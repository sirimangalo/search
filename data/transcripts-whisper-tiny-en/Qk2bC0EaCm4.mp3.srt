1
00:00:00,000 --> 00:00:03,000
Hello and welcome back to Ask a Month.

2
00:00:03,000 --> 00:00:14,000
Today, I will be answering the question as to what should we do when confronted by situations

3
00:00:14,000 --> 00:00:27,000
where we feel necessary to perform violent acts, specifically in terms of killing what we deem to be pests.

4
00:00:27,000 --> 00:00:37,000
Those sentient beings that are causing suffering for ourselves or those around us

5
00:00:37,000 --> 00:00:49,000
or are confronting us with a situation where we were put in some great physical difficulty.

6
00:00:49,000 --> 00:00:57,000
So the example that was given on the ask.ceremunglo.org was about rats.

7
00:00:57,000 --> 00:01:06,000
And in this case, it's not even a physical difficulty that they're giving.

8
00:01:06,000 --> 00:01:10,000
It's a problem with the landlord.

9
00:01:10,000 --> 00:01:15,000
So the landlord has given an ultimatum that, not an ultimatum,

10
00:01:15,000 --> 00:01:22,000
the requirement that the rats be taken care of.

11
00:01:22,000 --> 00:01:28,000
And so the question is what to do at this point.

12
00:01:28,000 --> 00:01:38,000
First of all, this question, really the core issue here,

13
00:01:38,000 --> 00:01:42,000
not in regards to specific instances,

14
00:01:42,000 --> 00:01:52,000
the core theory that we have to get through or come to understand

15
00:01:52,000 --> 00:01:59,000
is the difference between physical well-being and mental well-being.

16
00:01:59,000 --> 00:02:13,000
And so the problem is that we quite often think more of our physical well-being

17
00:02:13,000 --> 00:02:15,000
than of our mental well-being.

18
00:02:15,000 --> 00:02:20,000
And we'll often place our physical well-being ahead of our mental well-being,

19
00:02:20,000 --> 00:02:24,000
not realizing that this is actually the choice we're making.

20
00:02:24,000 --> 00:02:29,000
And that by performing violent acts towards other beings

21
00:02:29,000 --> 00:02:37,000
that we're actually hurting our mental health, our mental well-being.

22
00:02:37,000 --> 00:02:44,000
And so we'll commit egregious acts of violence against other living beings,

23
00:02:44,000 --> 00:02:51,000
seeking out some state of physical well-being for ourselves or for others.

24
00:02:51,000 --> 00:02:56,000
And so this goes for a great number of situations

25
00:02:56,000 --> 00:03:03,000
and this could even be extended into acts of war and acts of murder

26
00:03:03,000 --> 00:03:06,000
and assassination and so on.

27
00:03:06,000 --> 00:03:10,000
And so the question of whether it would have been right or wrong

28
00:03:10,000 --> 00:03:17,000
to murder someone like Adolf Hitler or Osama bin Laden if you had the chance.

29
00:03:17,000 --> 00:03:22,000
And so this isn't exactly what we're dealing with here,

30
00:03:22,000 --> 00:03:28,000
but the underlying issue is our physical well-being versus our mental well-being.

31
00:03:28,000 --> 00:03:33,000
And so in cases of pests, in cases of dangerous animals,

32
00:03:33,000 --> 00:03:37,000
even snakes and scorpions and so on,

33
00:03:37,000 --> 00:03:41,000
we often react,

34
00:03:41,000 --> 00:03:48,000
unmindfully not realizing that we're actually working towards our own detriment.

35
00:03:48,000 --> 00:03:52,000
And even though by acting in such a way,

36
00:03:52,000 --> 00:03:57,000
we might further our physical well-being for some time,

37
00:03:57,000 --> 00:04:07,000
we're actually causing a great amount of deterioration in our mental well-being.

38
00:04:07,000 --> 00:04:12,000
So we can ask ourselves, which is more important,

39
00:04:12,000 --> 00:04:18,000
whether it's better that we live a healthy and strong physical life

40
00:04:18,000 --> 00:04:24,000
with a corrupt and evil and unwholesome mind,

41
00:04:24,000 --> 00:04:27,000
or whether we die with a pure mind.

42
00:04:27,000 --> 00:04:35,000
And for most people, because of our inability to see beyond this human state,

43
00:04:35,000 --> 00:04:44,000
beyond this one life, what we call this life, this birth, this existence.

44
00:04:44,000 --> 00:04:47,000
Because this seems to be all that there is,

45
00:04:47,000 --> 00:04:52,000
and because we're so entrenched in this idea of the human status being the ultimate,

46
00:04:52,000 --> 00:04:57,000
that we see nothing wrong with committing egregious acts,

47
00:04:57,000 --> 00:05:05,000
and even with solving our minds for immediate pleasure,

48
00:05:05,000 --> 00:05:10,000
not realizing that we're accumulating these tendencies in our minds,

49
00:05:10,000 --> 00:05:13,000
and the mind doesn't go away, that it continues on,

50
00:05:13,000 --> 00:05:19,000
and there is no end that we might call death or so on.

51
00:05:19,000 --> 00:05:24,000
As long as we have these tendencies, they will increase,

52
00:05:24,000 --> 00:05:29,000
and they will ever, and again, lead us to conflict and suffering,

53
00:05:29,000 --> 00:05:33,000
and they're actually setting us up for greater and greater suffering,

54
00:05:33,000 --> 00:05:37,000
which is quite easy to see if one is practicing the meditation,

55
00:05:37,000 --> 00:05:41,000
that one will see that these acts are unwholesome unpleasant,

56
00:05:41,000 --> 00:05:45,000
not leading to positive circumstances.

57
00:05:45,000 --> 00:05:48,000
There was a question that was asked some time ago,

58
00:05:48,000 --> 00:05:55,000
on ask.ceremungalad.org, about whether it would be worth it to go to hell yourself,

59
00:05:55,000 --> 00:06:01,000
for people who believe in that the mind can become so solid that it goes to hell,

60
00:06:01,000 --> 00:06:10,000
in order to save other beings, in order to bring up to prevent great violence or so on.

61
00:06:10,000 --> 00:06:14,000
So if you were to kill Osama bin Laden, they'd all hit, there's someone like this,

62
00:06:14,000 --> 00:06:19,000
and therefore we're able to prevent great suffering,

63
00:06:19,000 --> 00:06:24,000
as the idea was to save the world, would you do it?

64
00:06:24,000 --> 00:06:30,000
And so the point here is that by creating more violence,

65
00:06:30,000 --> 00:06:34,000
we're in a state where there is a great amount of violence in the world,

66
00:06:34,000 --> 00:06:40,000
and there are beings in positions to inflict great violence on other beings,

67
00:06:40,000 --> 00:06:49,000
and this is a result of our accumulated tendencies to perform such violent acts on each other.

68
00:06:49,000 --> 00:06:55,000
Now by increasing those, you're not in any way helping things.

69
00:06:55,000 --> 00:07:00,000
The people who die, the people who kill, they all have these tendencies in them.

70
00:07:00,000 --> 00:07:03,000
What you do by, in this case, going to hell, or in any case,

71
00:07:03,000 --> 00:07:09,000
creating unwholesome states in the mind is just increasing the amount of unwholesomeness in the universe.

72
00:07:09,000 --> 00:07:14,000
Since the beings that die, they continue on the beings that kill, they continue on,

73
00:07:14,000 --> 00:07:20,000
and you yourself by adding to the pot, you only increase the tendencies.

74
00:07:20,000 --> 00:07:28,000
So the important solution, the solution, is to decrease the amount of killing,

75
00:07:28,000 --> 00:07:36,000
and to teach people to be patient with, you know, when confronted with revenge,

76
00:07:36,000 --> 00:07:42,000
when other beings would inflict violence on us, to be forbearing,

77
00:07:42,000 --> 00:07:45,000
and to put an end to it.

78
00:07:45,000 --> 00:07:52,000
If this means my death, then let that be the end, to not react, to not reply,

79
00:07:52,000 --> 00:07:59,000
and create the cycle of revenge, which actually spans lifetimes.

80
00:07:59,000 --> 00:08:04,000
A life is nothing in the face of our minds, and our minds continue on,

81
00:08:04,000 --> 00:08:07,000
and carry these states with them.

82
00:08:07,000 --> 00:08:12,000
So this is sort of the backdrop, the theory behind what I'm going to talk about here,

83
00:08:12,000 --> 00:08:17,000
and the basic answer that, no, it's not right,

84
00:08:17,000 --> 00:08:23,000
and it could never be a good thing to get rid of rats,

85
00:08:23,000 --> 00:08:26,000
to kill rats that are infesting your house,

86
00:08:26,000 --> 00:08:33,000
or even to kill parasites, or to kill those dangerous animals,

87
00:08:33,000 --> 00:08:41,000
a snake, or someone, an animal, or an intruder who is threatening your family, and so on.

88
00:08:41,000 --> 00:08:46,000
And this is because of the difference between physical health and mental health,

89
00:08:46,000 --> 00:08:50,000
and if it means that we have to, as a result, suffer physically,

90
00:08:50,000 --> 00:09:01,000
and not just our bodies, but our physical surrounding might be in perfect suboptimal.

91
00:09:01,000 --> 00:09:09,000
But that our minds should remain healthy is far more important,

92
00:09:09,000 --> 00:09:13,000
and if our minds can stay healthy and can stay pure,

93
00:09:13,000 --> 00:09:16,000
then it really doesn't matter where we are.

94
00:09:16,000 --> 00:09:21,000
And so this is the first thing that we have to get through,

95
00:09:21,000 --> 00:09:23,000
and we have to understand.

96
00:09:23,000 --> 00:09:26,000
So then the question is, well, then what do you do?

97
00:09:26,000 --> 00:09:35,000
If you are, if it is not in your best interest to perform acts of violence,

98
00:09:35,000 --> 00:09:38,000
well, then what do you do when confronted with this situation?

99
00:09:38,000 --> 00:09:39,000
You're here.

100
00:09:39,000 --> 00:09:40,000
It's not just our well-being.

101
00:09:40,000 --> 00:09:44,000
We have a conflict with our landlord, for example.

102
00:09:44,000 --> 00:09:50,000
And so, and I've gotten several questions in regards to this,

103
00:09:50,000 --> 00:09:55,000
even people asking about if you're confronted by a violent person,

104
00:09:55,000 --> 00:09:57,000
what would you do?

105
00:09:57,000 --> 00:10:01,000
So there are, I think, three methods that are in line with the boot decision.

106
00:10:01,000 --> 00:10:04,000
There's actually, you could break it up any number of ways,

107
00:10:04,000 --> 00:10:10,000
but briefly, I think there are three suitably Buddhist responses

108
00:10:10,000 --> 00:10:15,000
to this sort of situation, whether it be pests,

109
00:10:15,000 --> 00:10:21,000
whether it be criminals or murderers or however.

110
00:10:21,000 --> 00:10:24,000
The first is to avoid the situation.

111
00:10:24,000 --> 00:10:29,000
Now, the Buddha did condone avoiding those situations

112
00:10:29,000 --> 00:10:32,000
that would obviously get in the way of your meditation practice,

113
00:10:32,000 --> 00:10:34,000
in your mental development.

114
00:10:34,000 --> 00:10:39,000
And the examples he used were of dangerous, you know,

115
00:10:39,000 --> 00:10:44,000
Nicola, or a charging elephant.

116
00:10:44,000 --> 00:10:47,000
If an elephant is charging it, you avoid it.

117
00:10:47,000 --> 00:10:50,000
You don't stand there and seeing, you don't have to.

118
00:10:50,000 --> 00:10:53,000
You can move to the side or so on.

119
00:10:53,000 --> 00:10:56,000
But I think this extends to a lot of things,

120
00:10:56,000 --> 00:10:59,000
for instance, avoiding situations, avoiding,

121
00:10:59,000 --> 00:11:02,000
in the case of criminals or murderers,

122
00:11:02,000 --> 00:11:11,000
avoiding those areas that where you're likely to be confronted with those sorts of people.

123
00:11:11,000 --> 00:11:17,000
Now, in the case of rat infestations,

124
00:11:17,000 --> 00:11:20,000
this is probably not a useful solution,

125
00:11:20,000 --> 00:11:22,000
but it is one solution that we should all keep in mind

126
00:11:22,000 --> 00:11:26,000
is to avoid those situations where you have a landlord

127
00:11:26,000 --> 00:11:32,000
or be you're living in a house that is susceptible to these sorts of things.

128
00:11:32,000 --> 00:11:37,000
So, I mean, an ideal form of this would be to leave the home

129
00:11:37,000 --> 00:11:41,000
and live under a tree or live in a cave, live in the forest,

130
00:11:41,000 --> 00:11:44,000
where you don't have to deal with these situations,

131
00:11:44,000 --> 00:11:47,000
because obviously living in the household life,

132
00:11:47,000 --> 00:11:49,000
it's much more complicated.

133
00:11:49,000 --> 00:11:54,000
And the situation with rats is one that comes up common.

134
00:11:54,000 --> 00:11:56,000
Another one that people talk about is lice.

135
00:11:56,000 --> 00:11:58,000
Well, if you had lice, what would you do?

136
00:11:58,000 --> 00:12:00,000
Would you not want to kill them?

137
00:12:00,000 --> 00:12:04,000
And so one means of overcoming this is to shave your head,

138
00:12:04,000 --> 00:12:06,000
as I've heard that actually,

139
00:12:06,000 --> 00:12:08,000
I'm not sure if this is true or not,

140
00:12:08,000 --> 00:12:11,000
but I've heard that that will actually prevent the lice from reading.

141
00:12:11,000 --> 00:12:17,000
If you shave your head, they won't be able to stay there.

142
00:12:17,000 --> 00:12:20,000
They stay at the roots of the hair and so on.

143
00:12:20,000 --> 00:12:23,000
So, these are just wild examples.

144
00:12:23,000 --> 00:12:25,000
I mean, you could think of many different examples,

145
00:12:25,000 --> 00:12:29,000
but with in the case of rats,

146
00:12:29,000 --> 00:12:32,000
in the case of violence and so on,

147
00:12:32,000 --> 00:12:36,000
we should be careful to avoid those kinds of situations

148
00:12:36,000 --> 00:12:39,000
that would only give rise to an awesomeness.

149
00:12:39,000 --> 00:12:48,000
You know, if you're a young attractive woman, for example,

150
00:12:48,000 --> 00:12:52,000
you might want to avoid dark streets at night or so on.

151
00:12:52,000 --> 00:12:56,000
I mean, not even a woman, if you're a person who doesn't look fierce

152
00:12:56,000 --> 00:12:58,000
or doesn't like me or so on,

153
00:12:58,000 --> 00:13:01,000
you might want to avoid those situations

154
00:13:01,000 --> 00:13:04,000
where you might get into a conflict.

155
00:13:04,000 --> 00:13:09,000
Among, I sometimes try to avoid those areas

156
00:13:09,000 --> 00:13:14,000
where I might be confronted with prejudice, bigotry, and so on.

157
00:13:14,000 --> 00:13:17,000
I was arrested and put in jail a couple of years ago,

158
00:13:17,000 --> 00:13:20,000
simply because I look different.

159
00:13:20,000 --> 00:13:24,000
And some people do because of their fear.

160
00:13:24,000 --> 00:13:29,000
They either made some assumptions

161
00:13:29,000 --> 00:13:32,000
or else they were specifically trying to get rid of me.

162
00:13:32,000 --> 00:13:35,000
I don't know, but I was arrested and put in jail,

163
00:13:35,000 --> 00:13:37,000
and it was a big deal.

164
00:13:37,000 --> 00:13:39,000
So it would have probably been in my best interest

165
00:13:39,000 --> 00:13:42,000
to just avoid the whole issue and the whole situation

166
00:13:42,000 --> 00:13:49,000
and stay in a place that was more accepting.

167
00:13:49,000 --> 00:13:52,000
So this is the first answer, I think,

168
00:13:52,000 --> 00:13:54,000
and it will work in a variety of situations,

169
00:13:54,000 --> 00:13:56,000
and it can be employed in a variety of ways.

170
00:13:56,000 --> 00:13:59,000
Just avoid the problem, find some ways

171
00:13:59,000 --> 00:14:03,000
so that you don't have to be confronted with the situation.

172
00:14:03,000 --> 00:14:06,000
You don't have to be confronted with these difficult issues,

173
00:14:06,000 --> 00:14:12,000
or try to restructure your life so that you don't meet with this situation.

174
00:14:12,000 --> 00:14:17,000
The second way is to find an alternative,

175
00:14:17,000 --> 00:14:20,000
an alternative to violence,

176
00:14:20,000 --> 00:14:22,000
and there are many alternatives.

177
00:14:22,000 --> 00:14:25,000
And I think this is a point that I always try to raise with people

178
00:14:25,000 --> 00:14:33,000
is that killing and violence are not the only way out of a situation,

179
00:14:33,000 --> 00:14:36,000
whether it be with pests, whether it be with murderers

180
00:14:36,000 --> 00:14:38,000
or criminals or so on.

181
00:14:38,000 --> 00:14:40,000
Often, if someone wants your wallet,

182
00:14:40,000 --> 00:14:42,000
maybe you just give them your wallet,

183
00:14:42,000 --> 00:14:47,000
and that's a way of dealing with the situation mindfully,

184
00:14:47,000 --> 00:14:51,000
giving up, letting go in this sense.

185
00:14:51,000 --> 00:14:54,000
Dealing with the situation may be talking to the person,

186
00:14:54,000 --> 00:14:58,000
sometimes that can work with criminals and murders.

187
00:14:58,000 --> 00:15:00,000
It's probably not likely to.

188
00:15:00,000 --> 00:15:08,000
With pests, this is really something that we spend far too little time on.

189
00:15:08,000 --> 00:15:13,000
I was trying to learn about how to get rid of termites,

190
00:15:13,000 --> 00:15:15,000
and being the idea came up,

191
00:15:15,000 --> 00:15:17,000
well, when there are termites, you have to kill them.

192
00:15:17,000 --> 00:15:21,000
So I researched it, and I was trying to find some information

193
00:15:21,000 --> 00:15:27,000
on ways of getting rid of termites that don't require you to kill them.

194
00:15:27,000 --> 00:15:31,000
And no research has been done on this as far as I'm or maybe,

195
00:15:31,000 --> 00:15:34,000
but there's nothing on the internet anyway.

196
00:15:34,000 --> 00:15:38,000
And I'm betting that there's very little research being done on this

197
00:15:38,000 --> 00:15:40,000
because people don't think.

198
00:15:40,000 --> 00:15:42,000
Well, when you have termites, you just kill them.

199
00:15:42,000 --> 00:15:44,000
You find some way, and there are many ways.

200
00:15:44,000 --> 00:15:50,000
They have many need and ingenious ways of killing termites,

201
00:15:50,000 --> 00:15:55,000
but there's no one has put any of their ingenuity into finding other solutions

202
00:15:55,000 --> 00:15:59,000
because no one's ever thought that of the importance of it.

203
00:15:59,000 --> 00:16:02,000
When you can kill them, why would you find another solution?

204
00:16:02,000 --> 00:16:06,000
I think this is really tragic because in many cases,

205
00:16:06,000 --> 00:16:09,000
the solution does exist, and it's not very difficult.

206
00:16:09,000 --> 00:16:11,000
The solution might be with termites,

207
00:16:11,000 --> 00:16:14,000
might be simply finding a compound that they don't like.

208
00:16:14,000 --> 00:16:17,000
Now, who would have thought to try to find a compound

209
00:16:17,000 --> 00:16:20,000
that termites don't like when you can find a compound that kills them?

210
00:16:20,000 --> 00:16:23,000
Some chemical compound that drives them away.

211
00:16:23,000 --> 00:16:29,000
I don't know of any, but I haven't had that much experience with termites.

212
00:16:29,000 --> 00:16:33,000
I have had experience with other animals and with ants, for example,

213
00:16:33,000 --> 00:16:35,000
people will put out poison to kill the ants.

214
00:16:35,000 --> 00:16:43,000
Now, baby powder or talcum powder works not as well as poison, obviously,

215
00:16:43,000 --> 00:16:47,000
but from Buddhist point of view, it works much better than poison.

216
00:16:47,000 --> 00:16:52,000
You sweep the ants away, and then you put talcum powder down in their path,

217
00:16:52,000 --> 00:16:55,000
and I'm not sure if it's the scented talcum powder

218
00:16:55,000 --> 00:16:59,000
or if normal talcum powder will work as well, but somehow it stops them.

219
00:16:59,000 --> 00:17:03,000
The small ants aren't able to cross it, even the big ants don't like it

220
00:17:03,000 --> 00:17:08,000
because it removes their scented trails, and so they don't go across it.

221
00:17:08,000 --> 00:17:12,000
If you rub it into a plate, rub it across their trail,

222
00:17:12,000 --> 00:17:15,000
it'll remove their trail, and they won't be able to find their way,

223
00:17:15,000 --> 00:17:17,000
and they won't come back in that direction.

224
00:17:17,000 --> 00:17:22,000
I use this a lot with ants, but to a great success.

225
00:17:22,000 --> 00:17:27,000
If ants are coming along, telephone wires or clothes lines or so,

226
00:17:27,000 --> 00:17:30,000
and you can put butter on the clothes line on the telephone wire,

227
00:17:30,000 --> 00:17:32,000
and they won't cross and butter.

228
00:17:32,000 --> 00:17:34,000
Not all ants, anyway.

229
00:17:34,000 --> 00:17:38,000
I do believe there are some varieties of ants that eat the butter,

230
00:17:38,000 --> 00:17:41,000
but as an example, there are ways around this with rats.

231
00:17:41,000 --> 00:17:47,000
The example I gave on the forum was to use humane rat traps.

232
00:17:47,000 --> 00:17:49,000
Humane mice traps, these exist.

233
00:17:49,000 --> 00:17:54,000
It's a box, and you can buy them, and there's bait inside.

234
00:17:54,000 --> 00:17:57,000
The rat goes in the door closes, or the door is made in such a way

235
00:17:57,000 --> 00:18:00,000
that they can't get out again, and then you take it away to the forest,

236
00:18:00,000 --> 00:18:06,000
find someplace far, far away, and release the rat end of story.

237
00:18:06,000 --> 00:18:09,000
I do this with mosquitoes as well.

238
00:18:09,000 --> 00:18:14,000
You have mosquitoes in your home, in your tent,

239
00:18:14,000 --> 00:18:19,000
wherever you are, you take a cup, you put the mosquito in the cup,

240
00:18:19,000 --> 00:18:21,000
you take it outside, and you do this again, and again,

241
00:18:21,000 --> 00:18:25,000
if you have a closed-off space where the mosquitoes don't come.

242
00:18:25,000 --> 00:18:29,000
Finding intelligent ways to deal with pests,

243
00:18:29,000 --> 00:18:32,000
I think, is incredibly important.

244
00:18:32,000 --> 00:18:35,000
In fact, part of me would like to set up a wiki page.

245
00:18:35,000 --> 00:18:38,000
Probably I will end up doing this a wiki page for just this sort of thing,

246
00:18:38,000 --> 00:18:43,000
where people can post their good ideas for how to deal with

247
00:18:43,000 --> 00:18:46,000
difficult situations in a Buddhist way.

248
00:18:46,000 --> 00:18:51,000
Not just pests, but also how to deal with questions that you get,

249
00:18:51,000 --> 00:18:55,000
or how to deal with this sort of person or that sort of person,

250
00:18:55,000 --> 00:18:58,000
how to deal with this situation, how to deal with.

251
00:18:58,000 --> 00:19:01,000
So many of the issues that were confronted with that all people,

252
00:19:01,000 --> 00:19:07,000
and all religions are confronted with, and have to find some way to make it.

253
00:19:07,000 --> 00:19:12,000
Try with their understanding of ethics and practice.

254
00:19:12,000 --> 00:19:17,000
So this is the second method, is to deal with it.

255
00:19:17,000 --> 00:19:24,000
Find a way, an alternative means of dealing with the situation that doesn't require violence.

256
00:19:24,000 --> 00:19:29,000
And one note I'd make on that is that sometimes in self-defense,

257
00:19:29,000 --> 00:19:35,000
it is even according to the Buddha proper to resort to limited amounts of violence.

258
00:19:35,000 --> 00:19:39,000
So if someone is attacking you to push them out of the way,

259
00:19:39,000 --> 00:19:43,000
or to hit them enough so that you can run away,

260
00:19:43,000 --> 00:19:45,000
even monks are allowed to do this.

261
00:19:45,000 --> 00:19:50,000
We're not allowed to hit someone, but we're allowed to hit someone in self-defense in order to get away.

262
00:19:50,000 --> 00:19:57,000
So if it means that you have to perform some limited act of violence

263
00:19:57,000 --> 00:20:02,000
in order to escape, or in order to wake up the attacker,

264
00:20:02,000 --> 00:20:06,000
or so on, or to find a way to change the situation,

265
00:20:06,000 --> 00:20:10,000
even putting the attacker into an armlock,

266
00:20:10,000 --> 00:20:13,000
or whatever if you know karate, or you know kung fu,

267
00:20:13,000 --> 00:20:18,000
or some martial arts to be able to change the situation,

268
00:20:18,000 --> 00:20:22,000
and avoid, again, avoiding the greater act of violence,

269
00:20:22,000 --> 00:20:31,000
then to a limited extent, because it's not something that is designed to...

270
00:20:31,000 --> 00:20:35,000
It's not something that is designed to harm.

271
00:20:35,000 --> 00:20:41,000
It's in self-defense, and it's designed to allow you to escape the situation.

272
00:20:41,000 --> 00:20:51,000
And that is permitted, provided that it doesn't reflect fatal harm on the other person, or on the other being.

273
00:20:51,000 --> 00:20:57,000
So there might be a case where limited amounts of violence

274
00:20:57,000 --> 00:21:03,000
are not in the intention of hurting, or killing, or seriously harming the other person,

275
00:21:03,000 --> 00:21:08,000
but simply in the interests of self-defense, and for the purposes of escaping,

276
00:21:08,000 --> 00:21:12,000
might be, or are considered to be allowed.

277
00:21:12,000 --> 00:21:15,000
This is a way of dealing with the issue.

278
00:21:15,000 --> 00:21:20,000
The third answer, which I think we should also keep in mind,

279
00:21:20,000 --> 00:21:23,000
and this goes back to what I was saying in the beginning, is to accept,

280
00:21:23,000 --> 00:21:29,000
and to let go of the situation. And I think the issue of rats in the landlord

281
00:21:29,000 --> 00:21:37,000
is an interesting example of this, because sometimes we have to think outside the box,

282
00:21:37,000 --> 00:21:45,000
and we have to look outside of our situation, and not get confined to A or B mentality,

283
00:21:45,000 --> 00:21:48,000
where if I don't do this, that is going to happen.

284
00:21:48,000 --> 00:21:54,000
Often, when we let things go, when we're mindful, when we're aware of the situation,

285
00:21:54,000 --> 00:22:01,000
there's a C alternative arises, almost magically, and A and B disappear completely.

286
00:22:01,000 --> 00:22:11,000
So it may be in the case where we're confronted by someone assaulting us.

287
00:22:11,000 --> 00:22:17,000
Sometimes when it can happen, that when we're mindful, when we're aware,

288
00:22:17,000 --> 00:22:23,000
and when we're meditating on the situation, when we're taking it as a Buddhist practice,

289
00:22:23,000 --> 00:22:27,000
people are saying, what would you just say, pain, pain when someone's hitting you?

290
00:22:27,000 --> 00:22:34,000
I think, yes, that's a good, a perfectly reasonable response for this situation,

291
00:22:34,000 --> 00:22:44,000
and can often have magical results where people have found that suddenly the situation changed,

292
00:22:44,000 --> 00:22:49,000
and they were no longer the victim. Now, they were in control,

293
00:22:49,000 --> 00:22:54,000
and the other person was forced, really due to the power of presence,

294
00:22:54,000 --> 00:22:58,000
because the mind is such a powerful thing, much more powerful than the body,

295
00:22:58,000 --> 00:23:05,000
and simply the presence of someone who is mindful is really the greatest weapon there is,

296
00:23:05,000 --> 00:23:10,000
and it's something that can truly overcome these situations.

297
00:23:10,000 --> 00:23:16,000
So in the case of the rats in the landlord, it might be that simply by being mindful,

298
00:23:16,000 --> 00:23:21,000
and watching the situation unfold, and allowing the consequences

299
00:23:21,000 --> 00:23:24,000
if the landlords are going to throw you out,

300
00:23:24,000 --> 00:23:30,000
you simply say to the landlord, I'm Buddhist, and I don't kill, and so do what you will,

301
00:23:30,000 --> 00:23:35,000
and if it means that we have to do that, we have to come to some sort of conflict and so be it.

302
00:23:35,000 --> 00:23:41,000
Letting things go, holding on to what is really and truly important,

303
00:23:41,000 --> 00:23:45,000
which is your mental health and mental well-being, and the truth,

304
00:23:45,000 --> 00:23:50,000
because it's being untrue to yourself, to perform violence on other beings

305
00:23:50,000 --> 00:23:54,000
when you yourself don't want to feel such value.

306
00:23:54,000 --> 00:23:59,000
When you yourself don't or wish for that not to happen to you,

307
00:23:59,000 --> 00:24:04,000
so if someone's going to kill you, well, the only reason you'd kill them first is

308
00:24:04,000 --> 00:24:09,000
because you yourself don't want to die, and so you're being just as hypocritical as the other person.

309
00:24:09,000 --> 00:24:14,000
You're inflicting something on other beings that you yourself would not wish for,

310
00:24:14,000 --> 00:24:20,000
and so it's something that is against harmony and is against the truth, it's against reality,

311
00:24:20,000 --> 00:24:28,000
and it's going to create, as I said, corruption in the mind, something that we can do much better without in this,

312
00:24:28,000 --> 00:24:32,000
and we can do with much less than there is already in the world,

313
00:24:32,000 --> 00:24:38,000
so something that we should strive to do away with rather than increase.

314
00:24:38,000 --> 00:24:43,000
So simply by being mindful, by being aware of the situation, whether it be a violent situation,

315
00:24:43,000 --> 00:24:50,000
whether it be a difficult situation, whether it be a life or something,

316
00:24:50,000 --> 00:24:57,000
even just sticking with the itching and the pain of having life should be a part,

317
00:24:57,000 --> 00:25:01,000
not all of the answer, but should be at least a part of the answer.

318
00:25:01,000 --> 00:25:10,000
Of course, we can find other solutions and something that stops the life from or repels the life,

319
00:25:10,000 --> 00:25:19,000
and so it repels the assailant, but doesn't cause more suffering than is warranted,

320
00:25:19,000 --> 00:25:31,000
or isn't of the purpose of causing death or fatal or permanent physical damage or suffering to the other being.

321
00:25:31,000 --> 00:25:35,000
So I hope this has been helpful. I think this is an important subject,

322
00:25:35,000 --> 00:25:39,000
and I'm glad to talk about it. I've been meaning to make a video on this subject for a while,

323
00:25:39,000 --> 00:25:43,000
so there you have it. Thanks for tuning in and all the best.

