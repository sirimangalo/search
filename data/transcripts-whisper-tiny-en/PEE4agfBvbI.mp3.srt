1
00:00:00,000 --> 00:00:07,360
Hi, so in this video I will be discussing the number two reason why everyone should practice

2
00:00:07,360 --> 00:00:12,200
meditation and the number two in the top five reasons why everyone should practice meditation

3
00:00:12,200 --> 00:00:18,320
is that meditation puts one on the right path allows one to live one's life in a way

4
00:00:18,320 --> 00:00:23,640
which can be considered moral which can be considered noble which can be considered truly

5
00:00:23,640 --> 00:00:31,440
an honorable way of living. So we understand in the meditation tradition that there are many

6
00:00:31,440 --> 00:00:37,240
ways that we can live our lives. We can live our lives angry, we can live our lives greedy,

7
00:00:37,240 --> 00:00:42,320
we can live our lives in delusion or ignorance, we can live our lives as an ordinary human

8
00:00:42,320 --> 00:00:52,400
being, we can live our lives in a sort of a generous or a charitable way, doing good deeds,

9
00:00:52,400 --> 00:00:57,280
doing endeavors as you wish they could live or do unto you. We can even live our lives in

10
00:00:57,280 --> 00:01:08,520
some kind of meditative trends where we transcend ordinary reality. All of these are individual

11
00:01:08,520 --> 00:01:14,000
and unique ways of living one's life and unique paths which one can take. The meditation

12
00:01:14,000 --> 00:01:19,880
practice as we discuss it in on this channel is yet one other way which one can live

13
00:01:19,880 --> 00:01:24,520
one's life and we consider it to be the way to live correctly or the way to live properly

14
00:01:24,520 --> 00:01:30,000
and why this is so what we compare it to other ways of living. The first three ways of living

15
00:01:30,000 --> 00:01:37,960
ways of living in anger, in hate print, ways of living in greed or addiction and ways

16
00:01:37,960 --> 00:01:43,680
of living in delusion or ignorance. These are obviously ways which are unwholesome, ways

17
00:01:43,680 --> 00:01:48,880
which don't lead to happiness, don't lead to peace and the practice of meditation is clearly

18
00:01:48,880 --> 00:01:55,880
a way which as I've explained in other videos allows one to move away from these paths,

19
00:01:55,880 --> 00:02:02,080
to move away from states of being which put one into a state of real suffering. We can

20
00:02:02,080 --> 00:02:05,920
say that a person who is angry all the time, it's like a person living in hell, your

21
00:02:05,920 --> 00:02:13,280
life is hell. A person who is in greed and addiction all the time is just like a ghost,

22
00:02:13,280 --> 00:02:20,280
like someone who is always looking for things who is never satisfied and living in delusion

23
00:02:20,280 --> 00:02:25,960
we say it's like living as an animal, living as an ordinary dog or a cat or a pig or

24
00:02:25,960 --> 00:02:35,120
a goat or a cow or any of the life forms out there which we might think are perhaps less

25
00:02:35,120 --> 00:02:42,200
advanced or less able to develop themselves. Living as an ordinary human being is one other way

26
00:02:42,200 --> 00:02:47,040
that we can live our lives, it's a way which doesn't help the world, very much but perhaps

27
00:02:47,040 --> 00:02:51,280
we can say it doesn't hurt the world either. The practice of meditation is a way which

28
00:02:51,280 --> 00:02:57,000
is noble, it's not an ordinary way of living, though we focus and we're always paying attention

29
00:02:57,000 --> 00:03:01,680
to the ordinary reality. It actually is a way which brings one out of ordinary everyday

30
00:03:01,680 --> 00:03:07,480
reality, allows one to rise above the simple, dreary and mundane existence sort of going

31
00:03:07,480 --> 00:03:12,240
with the flow when things are good, you feel good when things are bad, you feel bad. It's

32
00:03:12,240 --> 00:03:16,320
a way of rising above all of this and coming to see it for what it is and living in

33
00:03:16,320 --> 00:03:21,320
a state of real and true peace and happiness. So it's not this path either, it's a path

34
00:03:21,320 --> 00:03:26,120
which we consider to be better than simply living. It's also not simply the path of

35
00:03:26,120 --> 00:03:31,160
doing good deeds because it's clear that people can do deeds with ulterior motives, people

36
00:03:31,160 --> 00:03:38,160
can do deeds out of delusion or simply do deeds because it hits them to do them at a certain

37
00:03:38,160 --> 00:03:42,720
time. They can do good deeds sometimes and then forget to do good deeds but when one's

38
00:03:42,720 --> 00:03:48,440
mind is pure, when one lives one most life in the present moment, one is able to approach

39
00:03:48,440 --> 00:03:53,360
everything with a certain amount of wisdom and can see the need, what is the need in

40
00:03:53,360 --> 00:03:58,960
every situation, is able to approach every situation, finding the way to bring help and

41
00:03:58,960 --> 00:04:03,880
to bring benefit to other people, to do good deeds. So it's not exactly the way of being

42
00:04:03,880 --> 00:04:10,720
a good person, just doing good deeds but in a sense it makes one truly, makes one's heart

43
00:04:10,720 --> 00:04:17,000
truly good and as a result everything one does then becomes good and it's sort of rather

44
00:04:17,000 --> 00:04:21,200
than intending to do good deeds one just does them as a part of one's nature because

45
00:04:21,200 --> 00:04:26,880
one's mind is pure. When someone asks for something there's no stinginess, if someone

46
00:04:26,880 --> 00:04:32,080
approaches you with anger, so there's no reaction and so we live our lives as you could

47
00:04:32,080 --> 00:04:39,200
say as good people simply because the mind is pure. It's also not the path of meditative

48
00:04:39,200 --> 00:04:44,160
trends or absorption. We're not looking for this heavy state of concentration where we

49
00:04:44,160 --> 00:04:49,120
forget about the world because this of course is temporary, you can work very very hard

50
00:04:49,120 --> 00:04:55,880
to take the mind away from reality but in the end it only lasts for as long as the power

51
00:04:55,880 --> 00:05:03,320
of the concentration. So if you work to X amount to bring your mind to a state of tranquility

52
00:05:03,320 --> 00:05:10,800
well it only lasts the very same amount of time. It's a very much very much dependent

53
00:05:10,800 --> 00:05:15,080
on how much work you put into it. The practice of meditation as we describe it here is

54
00:05:15,080 --> 00:05:20,360
not because it involves understanding and once you understand you can't say that the

55
00:05:20,360 --> 00:05:24,480
understanding is going to run out. This is understanding something you can use in every

56
00:05:24,480 --> 00:05:32,600
situation. When you understand something you can never eventually forget that it's such

57
00:05:32,600 --> 00:05:37,160
and such is the nature of it. You come to understand the nature of all of the things

58
00:05:37,160 --> 00:05:43,640
around you and inside of you and so you react to things with wisdom and with peace and

59
00:05:43,640 --> 00:05:49,520
with happiness at all times so your mind never falls into suffering. This is considered

60
00:05:49,520 --> 00:05:55,480
to be the right way and it's important to understand that here we don't mean it means everyone

61
00:05:55,480 --> 00:06:01,760
has to become a monk or everyone has to become this or that religious tradition or move

62
00:06:01,760 --> 00:06:06,080
here or move there. You can do whatever it is that you're doing right now as long as it's

63
00:06:06,080 --> 00:06:10,400
a moral wholesome thing to do and it's not creating states of suffering for yourself

64
00:06:10,400 --> 00:06:15,920
or others and still be on the right path. We can be doing all the living completely

65
00:06:15,920 --> 00:06:20,360
different lives and still all be on the right path because our minds are pure. Whatever

66
00:06:20,360 --> 00:06:24,960
we do we do it with a pure mind and this is considered to be the right path. This is the

67
00:06:24,960 --> 00:06:29,760
number two reason why everyone should practice meditation. So thanks for tuning in and

68
00:06:29,760 --> 00:06:50,840
please look forward to number one in the future.

