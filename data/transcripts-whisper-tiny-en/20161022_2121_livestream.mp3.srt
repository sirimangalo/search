1
00:00:00,000 --> 00:00:03,600
in a common area, but he had to promise that we wouldn't talk together.

2
00:00:03,600 --> 00:00:05,200
And it just went completely over our heads,

3
00:00:05,200 --> 00:00:07,400
we're like, oh yeah, that's interesting.

4
00:00:07,400 --> 00:00:10,040
And we started chatting with him, and he's just

5
00:00:10,040 --> 00:00:12,600
the glaring at us trying to,

6
00:00:12,600 --> 00:00:15,320
because in Thailand, they'll never tell you about rights.

7
00:00:15,320 --> 00:00:17,920
Hey, you guys have to be quiet and go back to your rooms.

8
00:00:17,920 --> 00:00:19,400
They just hint at it.

9
00:00:19,400 --> 00:00:22,080
And here we are, these dumb westerners who couldn't take the hint.

10
00:00:22,080 --> 00:00:31,280
And to a great extent, it made practice

11
00:00:31,280 --> 00:00:36,440
for that much more difficult, because I think there are only two of us

12
00:00:36,440 --> 00:00:37,800
who actually finished the course.

13
00:00:37,800 --> 00:00:41,160
And maybe I was the only one.

14
00:00:41,160 --> 00:00:45,720
I'm not sure about the others who, they had more difficulty.

15
00:00:45,720 --> 00:00:49,600
Harvey didn't finish the course.

16
00:00:49,600 --> 00:00:52,240
And I remember feeling quite bad about it,

17
00:00:52,240 --> 00:00:55,200
because near the end of the course,

18
00:00:55,200 --> 00:00:57,160
they have you do some fairly difficult.

19
00:00:57,160 --> 00:00:59,800
And the practice gets fairly difficult.

20
00:00:59,800 --> 00:01:00,840
And I couldn't take it.

21
00:01:00,840 --> 00:01:04,280
So I went, they gave us this exercise to do.

22
00:01:04,280 --> 00:01:10,200
And I went to see Harvey, and I said to Harvey, I said,

23
00:01:10,200 --> 00:01:11,200
man, are you doing this?

24
00:01:11,200 --> 00:01:13,520
I don't think I can do this.

25
00:01:13,520 --> 00:01:14,920
And he said, I don't know.

26
00:01:14,920 --> 00:01:16,760
I said, I said, yeah, I'm thinking I might leave.

27
00:01:16,760 --> 00:01:19,720
And then one of the teachers caught me talking to him,

28
00:01:19,720 --> 00:01:21,320
and said, hey, you can't talk to him.

29
00:01:21,320 --> 00:01:23,680
Go back to your room.

30
00:01:23,680 --> 00:01:25,920
So I went back to my room, and I strung up with it.

31
00:01:25,920 --> 00:01:28,600
And I actually ended up finishing, rather poorly,

32
00:01:28,600 --> 00:01:32,800
and not satisfactorily.

33
00:01:32,800 --> 00:01:35,120
But I finished.

34
00:01:35,120 --> 00:01:37,080
And then I went and talked to Harvey after I finished.

35
00:01:37,080 --> 00:01:38,800
And he said, yeah, I thought to my seaside.

36
00:01:38,800 --> 00:01:40,480
And finished, I thought to myself, no,

37
00:01:40,480 --> 00:01:41,360
it's not going to do it.

38
00:01:41,360 --> 00:01:42,920
I'm not going to do it.

39
00:01:42,920 --> 00:01:46,600
So I ruined his practice by talking to him.

40
00:01:46,600 --> 00:01:51,240
So my long, long-winded way of giving you an example

41
00:01:51,240 --> 00:01:55,960
of how speech causes all sorts of problems.

42
00:01:55,960 --> 00:02:01,240
Talking to other meditators is it can be a big danger

43
00:02:01,240 --> 00:02:06,640
of being kind of hindrance in the practice.

44
00:02:06,640 --> 00:02:08,080
Try to be careful.

45
00:02:08,080 --> 00:02:09,960
Don't share your conditions with others,

46
00:02:09,960 --> 00:02:12,520
because you affect each other's emotions.

47
00:02:12,520 --> 00:02:14,920
We're not quite vulnerable, right?

48
00:02:14,920 --> 00:02:17,880
You've dropped down all your guards,

49
00:02:17,880 --> 00:02:21,000
and you're trying to be honest and meditation.

50
00:02:21,000 --> 00:02:23,600
Sometimes really violent emotions come up,

51
00:02:23,600 --> 00:02:26,480
because you're allowing them to come up.

52
00:02:26,480 --> 00:02:28,200
When you're angry, you're no longer saying, OK,

53
00:02:28,200 --> 00:02:29,480
find some way to avoid that.

54
00:02:29,480 --> 00:02:33,760
Let's go out and pretend it's all good.

55
00:02:33,760 --> 00:02:36,280
When you're angry, you're letting the anger come up.

56
00:02:36,280 --> 00:02:37,800
You're angry.

57
00:02:37,800 --> 00:02:44,240
When you're greedy, whatever state your mind is,

58
00:02:44,240 --> 00:02:47,320
you're saying, OK, OK, let's look and work

59
00:02:47,320 --> 00:02:50,560
through this natch, and they're rather than repressing it.

60
00:02:50,560 --> 00:02:53,120
So if during that time, you're

61
00:02:53,120 --> 00:02:55,960
going to connect with someone else,

62
00:02:55,960 --> 00:02:59,960
you can ruin each other, because more harm to each other

63
00:02:59,960 --> 00:03:03,640
than you would in normal life.

64
00:03:03,640 --> 00:03:07,840
Meditation practice is a vulnerable sort of time.

65
00:03:07,840 --> 00:03:10,040
The meditators will leave here thinking

66
00:03:10,040 --> 00:03:15,000
they're quite more relaxed, and they've

67
00:03:15,000 --> 00:03:16,480
been able to let their guard down,

68
00:03:16,480 --> 00:03:28,720
and they feel stronger, because nothing's confronting them,

69
00:03:28,720 --> 00:03:31,320
they're not confronted by any challenges,

70
00:03:31,320 --> 00:03:33,840
so they feel much stronger, like they've gained a lot

71
00:03:33,840 --> 00:03:39,080
from the practice, more than they actually have in truth.

72
00:03:39,080 --> 00:03:42,600
But it's simply because living in the meditation center,

73
00:03:42,600 --> 00:03:47,160
there's nothing they're not confronted by any conflicts

74
00:03:47,160 --> 00:03:47,960
or challenges.

75
00:03:47,960 --> 00:03:50,160
And when they leave, because they've let their guard down,

76
00:03:50,160 --> 00:03:52,680
it can be quite difficult.

77
00:03:52,680 --> 00:03:56,840
So to say it, just to say that meditators are vulnerable

78
00:03:56,840 --> 00:04:01,720
and getting too involved with each other is a problem.

79
00:04:01,720 --> 00:04:04,600
So don't talk too much, just a point.

80
00:04:04,600 --> 00:04:09,760
Number two, number three, meet down on the sleeping.

81
00:04:09,760 --> 00:04:12,560
You get caught up in sleeping.

82
00:04:12,560 --> 00:04:15,040
You like to sleep.

83
00:04:15,040 --> 00:04:20,400
This is a real challenge, but it's kind of a liberating challenge.

84
00:04:20,400 --> 00:04:25,160
Meditators tend to find this a fairly

85
00:04:25,160 --> 00:04:29,440
on the whole of fairly reasonable challenge.

86
00:04:29,440 --> 00:04:35,080
Learning to see how attached we are to sleeping,

87
00:04:35,080 --> 00:04:43,720
how pleasurable it is to sleep, to fade into oblivion,

88
00:04:43,720 --> 00:04:47,960
to put our problems behind, and just let go.

89
00:04:47,960 --> 00:04:54,080
So comforting to sleep, comforting to be able to lie down.

90
00:04:54,080 --> 00:04:59,400
So we get attached to this state of oblivion,

91
00:04:59,400 --> 00:05:02,200
of course, what you start to notice in the practice

92
00:05:02,200 --> 00:05:03,680
is that when you wake up in the morning,

93
00:05:03,680 --> 00:05:06,520
you've lost a lot of your mindfulness.

94
00:05:06,520 --> 00:05:10,440
Because it's not nearly as peaceful as we think.

95
00:05:10,440 --> 00:05:13,120
It's actually all mixed up.

96
00:05:16,400 --> 00:05:20,560
If we sleep muddled, if we fall asleep with an agitated mind,

97
00:05:20,560 --> 00:05:22,080
we're agitated throughout the night.

98
00:05:22,080 --> 00:05:27,720
And as we don't have the mindfulness working,

99
00:05:27,720 --> 00:05:30,920
we wake up in a muddled state.

100
00:05:30,920 --> 00:05:35,160
And so we try to restrict our sleep to the bare minimum,

101
00:05:35,160 --> 00:05:38,600
what we need, times sleep more than six hours.

102
00:05:38,600 --> 00:05:42,840
And then we try to later cut it down to four hours.

103
00:05:42,840 --> 00:05:45,880
And you can see that if you're really fine-tuned,

104
00:05:45,880 --> 00:05:49,840
your practice four hours is actually plenty of sleep.

105
00:05:49,840 --> 00:05:51,320
It's quite surprising, actually.

106
00:05:51,320 --> 00:05:53,560
Most people wouldn't think that four hours of sleep

107
00:05:53,560 --> 00:05:59,520
wouldn't be enough, but when your mind is fine-tuned,

108
00:05:59,520 --> 00:06:01,840
you wouldn't want more.

109
00:06:01,840 --> 00:06:05,840
You wouldn't feel like more would be actually

110
00:06:05,840 --> 00:06:07,800
deleterious to your state of mind.

111
00:06:11,520 --> 00:06:12,880
We have to be careful about sleep.

112
00:06:12,880 --> 00:06:17,720
Sleep is an obstacle to your practice.

113
00:06:17,720 --> 00:06:20,880
So Mahasi Sai had undescribes this effort

114
00:06:20,880 --> 00:06:24,280
by the meditator to stay awake.

115
00:06:24,280 --> 00:06:27,120
You should look at sleep as your enemy.

116
00:06:27,120 --> 00:06:30,040
Sleep is this enemy waiting for you.

117
00:06:30,040 --> 00:06:31,320
It's like death.

118
00:06:31,320 --> 00:06:33,680
And you don't want to die soon.

119
00:06:33,680 --> 00:06:35,680
You don't want to give in to your enemy.

120
00:06:35,680 --> 00:06:38,640
You want to fight to the bitter end.

121
00:06:38,640 --> 00:06:41,600
So meditator should fight to the bitter end, not to fall asleep.

122
00:06:41,600 --> 00:06:44,920
But maybe not to that extent, but to some extent,

123
00:06:44,920 --> 00:06:47,600
you should look at sleep as your enemy.

124
00:06:47,600 --> 00:06:49,760
And when you do lie down and you can see

125
00:06:49,760 --> 00:06:54,200
that you can no longer sit up and your head's nodding

126
00:06:54,200 --> 00:06:56,600
and walking, isn't going to work anymore.

127
00:06:56,600 --> 00:06:58,000
Sitting isn't going to work anymore.

128
00:06:58,000 --> 00:07:01,040
Then you give in to sleep.

129
00:07:01,040 --> 00:07:02,000
But you don't give in.

130
00:07:02,000 --> 00:07:05,040
You lie down and give in to your fatigue.

131
00:07:05,040 --> 00:07:07,000
And say, I'm going to lie down.

132
00:07:07,000 --> 00:07:10,240
But you don't yet try to fall asleep.

133
00:07:10,240 --> 00:07:11,920
You don't wish for sleep to come.

134
00:07:11,920 --> 00:07:14,240
You don't wait for sleep to come.

135
00:07:14,240 --> 00:07:17,200
You try your best to be mindful watching the stomach

136
00:07:17,200 --> 00:07:23,480
rising and eventually, until eventually, fall asleep.

137
00:07:23,480 --> 00:07:25,200
If you think like that, it's not difficult.

138
00:07:25,200 --> 00:07:26,800
And the Buddha said, before you fall asleep,

139
00:07:26,800 --> 00:07:29,480
you make a note of when you're going to rise.

140
00:07:29,480 --> 00:07:32,120
And what that means is that the last thought in your mind

141
00:07:32,120 --> 00:07:36,360
or conscious in your mind to the very end

142
00:07:36,360 --> 00:07:41,640
is the sense of the boundaries of that sleep

143
00:07:41,640 --> 00:07:44,120
that you're not going to oversleep.

144
00:07:44,120 --> 00:07:46,360
And as a result, you wake up, generally, on time.

145
00:07:46,360 --> 00:07:52,440
It's quite interesting that how the mind can time six hours

146
00:07:52,440 --> 00:07:54,360
or four hours.

147
00:07:54,360 --> 00:07:56,440
And you find after a little bit of time,

148
00:07:56,440 --> 00:08:01,680
you wake up just before your alarm goes off.

149
00:08:01,680 --> 00:08:04,640
Every time, it's quite impressive

150
00:08:04,640 --> 00:08:07,080
that the mind is able to do that.

151
00:08:07,080 --> 00:08:08,320
I'm quite sure how that works.

152
00:08:11,080 --> 00:08:14,480
Nita Ramata, I don't get attached to sleep.

153
00:08:14,480 --> 00:08:20,120
And the sound number four, Nasanga Nita Ramata.

154
00:08:20,120 --> 00:08:22,440
This is like number two, or it's hand in hand.

155
00:08:22,440 --> 00:08:26,240
Sangha Nika is a community.

156
00:08:29,320 --> 00:08:31,960
Every one way of looking at this apart from speech

157
00:08:31,960 --> 00:08:34,160
is the need for community.

158
00:08:34,160 --> 00:08:38,240
A lot of meditators swear by meditation in groups.

159
00:08:38,240 --> 00:08:40,640
Our tradition doesn't really, and your meditators here

160
00:08:40,640 --> 00:08:45,640
will find they do a lot of work.

