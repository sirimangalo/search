1
00:00:00,000 --> 00:00:08,280
Okay, so welcome everyone to tonight's Munk Radio. It's April 1st, 2012

2
00:00:09,440 --> 00:00:11,600
but

3
00:00:11,600 --> 00:00:13,600
No jokes for us

4
00:00:13,600 --> 00:00:15,600
All seriousness today

5
00:00:17,560 --> 00:00:21,520
And first thing I'd like to introduce our panel for this evening

6
00:00:22,720 --> 00:00:24,720
I'm you to demo. I

7
00:00:24,720 --> 00:00:31,200
Most of you are familiar with me. This is Tarindu. Do you say hello Tarindu?

8
00:00:49,120 --> 00:00:51,120
This is good evening, good night

9
00:00:51,120 --> 00:00:56,080
This is Sumeda. You can introduce her

10
00:00:57,440 --> 00:01:00,320
Yeah, next to me is sitting Sumeda

11
00:01:03,840 --> 00:01:05,840
Hello

12
00:01:07,680 --> 00:01:11,280
And at the end is Pallanyani who is her

13
00:01:12,080 --> 00:01:15,840
Good evening or good morning first resident bikini

14
00:01:15,840 --> 00:01:17,840
I

15
00:01:18,800 --> 00:01:20,800
So announcement

16
00:01:22,960 --> 00:01:26,000
I guess for those of you who don't know and before someone asks a

17
00:01:26,800 --> 00:01:28,800
Nagasena left us

18
00:01:28,800 --> 00:01:30,800
I don't want to say too much, but

19
00:01:33,680 --> 00:01:37,120
We're happy to have him and now he's gone on to do other things

20
00:01:38,320 --> 00:01:40,560
Do you have any other announcements anything

21
00:01:40,560 --> 00:01:42,560
I

22
00:01:43,120 --> 00:01:45,120
Happening

23
00:01:46,720 --> 00:01:48,720
It was

24
00:01:48,720 --> 00:01:52,960
Might be going to Thailand in May when don't see how that's really

25
00:01:54,320 --> 00:01:58,800
I'm interested people here. Oh, yes. Yes. I know what the big announcement was is that

26
00:02:00,720 --> 00:02:02,560
Sumeda

27
00:02:02,560 --> 00:02:06,320
Actually, I think she's still referring to herself as as Meredith

28
00:02:06,320 --> 00:02:13,760
But we're not so she is is planning to ordain

29
00:02:14,800 --> 00:02:21,280
If all goes as planned and it's still not 100% sure the date is not 100% sure but on

30
00:02:22,880 --> 00:02:24,880
May 5th

31
00:02:24,880 --> 00:02:30,080
Which is there's anyone have any idea. We're gonna see if anyone knew

32
00:02:30,080 --> 00:02:38,000
We suck we suck is on the 5th of May which is the Buddha's birthday is in light and mint and is

33
00:02:38,800 --> 00:02:40,800
Bernie Banar is final passing away

34
00:02:41,360 --> 00:02:44,160
It's the celebration of that. This is the full moon celebration

35
00:02:44,400 --> 00:02:50,000
So many of you are probably familiar with it and I was just going to say that on the off chance that someone's

36
00:02:50,960 --> 00:02:52,640
Not busy that day

37
00:02:52,640 --> 00:02:56,560
You're all welcome to to come out. That's going to be May 5th

38
00:02:57,520 --> 00:02:58,720
2012 or

39
00:02:58,720 --> 00:03:03,120
2555 by this single is reckoning

40
00:03:04,160 --> 00:03:08,320
Her her parents or at least her father will come

41
00:03:08,960 --> 00:03:13,920
So oh, so yeah, just the father will come so everybody

42
00:03:14,960 --> 00:03:17,760
Can from America can join him

43
00:03:19,840 --> 00:03:21,840
It's just

44
00:03:21,840 --> 00:03:25,520
He's kind of he's the unknown right because he might not be available on the 5th of May

45
00:03:25,520 --> 00:03:30,800
So pending his availability, it'll be the 5th of May otherwise it might be

46
00:03:31,600 --> 00:03:34,160
A little later a little earlier. We'll have to talk about that

47
00:03:34,160 --> 00:03:53,040
That's all for announcements

