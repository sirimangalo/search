1
00:00:00,000 --> 00:00:05,000
Applying that your method of meditation is both samatha and vipasana.

2
00:00:09,000 --> 00:00:15,000
I wasn't, I don't think, and technically speaking, it's not.

3
00:00:15,000 --> 00:00:19,000
But practically speaking, yeah, it's both samatha and vipasana,

4
00:00:19,000 --> 00:00:24,000
because your mind becomes tranquil and new insight.

5
00:00:24,000 --> 00:00:28,000
But technically, no, it's just vipasana, because there's not enough samatha

6
00:00:28,000 --> 00:00:33,000
to enter into a stable jana.

7
00:00:33,000 --> 00:00:36,000
The jana that you enter into is momentary.

8
00:00:36,000 --> 00:00:41,000
It's a momentary jana called lakanupinijan,

9
00:00:41,000 --> 00:00:44,000
meditation on the characteristics,

10
00:00:44,000 --> 00:00:49,000
impermanent suffering, and so it occurs for a moment.

11
00:00:50,000 --> 00:00:55,000
But that's enough to tranquilize the mind, to suppress the five hindrances,

12
00:00:55,000 --> 00:00:58,000
and to allow you to see things clearly as they are.

13
00:00:58,000 --> 00:01:00,000
It's enough to become enlightened,

14
00:01:00,000 --> 00:01:03,000
but it's enough to tranquilize the mind,

15
00:01:03,000 --> 00:01:05,000
so it's also considered samatha.

16
00:01:05,000 --> 00:01:08,000
It can be considered samatha, just not technically speaking.

17
00:01:08,000 --> 00:01:16,000
Because samatha meditation is the moment when you are focused on a concept

18
00:01:16,000 --> 00:01:18,000
and tranquilizing your mind.

19
00:01:18,000 --> 00:01:22,000
If sana meditation is the moment when you are focusing on the characteristics

20
00:01:22,000 --> 00:01:27,000
and seeing impermanence suffering or not itself.

