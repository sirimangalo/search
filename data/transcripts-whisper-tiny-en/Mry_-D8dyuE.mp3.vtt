WEBVTT

00:00.000 --> 00:10.000
Okay, hello everyone, back with more questions and answers.

00:10.000 --> 00:19.000
So today's question, you can go and read it if you like, if you're watching this live.

00:19.000 --> 00:28.000
It's actually a rather complicated question, but I find two points in it.

00:28.000 --> 00:39.000
And about the noting practice or the use of what I call mantra, mindfulness mantra of sorts,

00:39.000 --> 00:45.000
using a word for making a note.

00:45.000 --> 00:51.000
And so the first part is about different ways of noting.

00:51.000 --> 00:54.000
This person has their own way of noting, I think.

00:54.000 --> 00:58.000
I don't quite understand.

00:58.000 --> 01:03.000
It's not not easy to tell exactly what this person is doing, how they're meditating,

01:03.000 --> 01:06.000
even quite what they're asking over the internet.

01:06.000 --> 01:09.000
So I'm just going to talk generally.

01:09.000 --> 01:15.000
First about that, and second is about whether noting can cause clinging.

01:15.000 --> 01:21.000
This is a common question, the second one, but it's too distinct.

01:21.000 --> 01:24.000
Points, though related.

01:24.000 --> 01:28.000
I think I can answer them sort of together.

01:28.000 --> 01:37.000
So this person talks about noting experiences as the five aggregates.

01:37.000 --> 01:42.000
So this one is the first aggregate, this one is form is physical.

01:42.000 --> 01:54.000
This one is feeling this is perception, this one is thought, this one is consciousness.

01:54.000 --> 01:59.000
And so there's a question, I'm not even sure that they're asking this question,

01:59.000 --> 02:05.000
but I have a question that I want to answer is whether this is a valid understanding of the technique

02:05.000 --> 02:08.000
that we do or even a valid meditation at all.

02:08.000 --> 02:15.000
So when we talk about meditation, this is a question I get sort of question I get is what is meditation

02:15.000 --> 02:21.000
or is ex-meditation, is this thing that I do meditation or so on?

02:21.000 --> 02:25.000
Meditation is some kind of mental practice.

02:25.000 --> 02:31.000
It's where you do something with your mind, cultivating habits, cultivating or dealing with habits.

02:31.000 --> 02:35.000
It can be about breaking down habits.

02:35.000 --> 02:40.000
And so when you have an experience, there's many different things you can do.

02:40.000 --> 02:44.000
You can think about it.

02:44.000 --> 02:46.000
You can react to it.

02:46.000 --> 02:54.000
You can try and suppress it or you can try and find a way to avoid it.

02:54.000 --> 03:08.000
You can go with it or engage with it, try and encourage it to continue.

03:08.000 --> 03:09.000
You can enjoy it.

03:09.000 --> 03:16.000
You can ignore it.

03:16.000 --> 03:24.000
And so what happens with some types of meditation or practice that appear to be meditation,

03:24.000 --> 03:32.000
things that become a meditation in the Western sense where you mull or ponder something.

03:32.000 --> 03:35.000
And I think this is what this person is talking about.

03:35.000 --> 03:42.000
And so I don't consider it to be a valid sort of meditation of the sort that we're doing.

03:42.000 --> 03:48.000
If you sit and when you have an experience, suppose you feel pain and you contemplate it from Buddhist perspective.

03:48.000 --> 03:51.000
If you think about it, hey, this is impermanent.

03:51.000 --> 03:53.000
Hey, this is suffering.

03:53.000 --> 03:57.000
Hey, this is non-soul for you to think, oh, this is just pain.

03:57.000 --> 04:01.000
And you try and tell yourself something about it.

04:01.000 --> 04:06.000
If you, well, not this is just pain, actually, that would be a little better, I think.

04:06.000 --> 04:14.000
But so I'll explain why, but if you make it out to be more than it is, you create abstract thought about it.

04:14.000 --> 04:18.000
You're not actually cultivating mindfulness.

04:18.000 --> 04:24.000
You're evoking the states of thought, states of contemplation.

04:24.000 --> 04:33.000
So you're actually not paying attention to the experience, which turns out to be quite important from a meditative or a mindfulness perspective.

04:33.000 --> 04:40.000
The difference between that and saying to yourself, this is pain or even just pain or the thought saying,

04:40.000 --> 04:51.000
this is a thought or thinking, is you're actually reminding yourself something very fundamental and basic about the experience.

04:51.000 --> 04:59.000
And what it evokes is a free understanding and a free consciousness.

04:59.000 --> 05:10.000
A consciousness that is free from any kind of judgment, any kind of abstraction, any kind of diversification, making more out of the experience than it actually is.

05:10.000 --> 05:28.000
So it's actually quite a special interaction in that it's meant to its design to, and it has the effect of creating an objective state of mind where you simply experience.

05:28.000 --> 05:31.000
The object as it is.

05:31.000 --> 05:43.000
When we talk about things like patience and equanimity, even peace and tranquility that are all wrapped up in this idea of not doing anything.

05:43.000 --> 05:56.000
So what we're trying to do with that is to find that state where we don't create, where we don't proliferate, or we don't make anything out of it.

05:56.000 --> 06:09.000
It's called Baba. Baba, if you're familiar with Buddhist theory, is the link in the chain that leads to suffering.

06:09.000 --> 06:19.000
When you make something out of something, so if someone says a simple worldly example, if someone says something to you like, you're a jerk, you're a dummy, you're this, you're that.

06:19.000 --> 06:22.000
If someone says you're fat, you're thin, or so on.

06:22.000 --> 06:28.000
If someone says something, maybe even something innocent to you, and you make something out of it, you get upset about it.

06:28.000 --> 06:36.000
You've created Baba means being or becoming, you make something, you cause something to become.

06:36.000 --> 06:43.000
You make a deal out of it when you make a big deal out of something, or any kind of deal.

06:43.000 --> 06:52.000
So the use of the mantra, when it's tautological in a sense of, it's not saying anything new about the experience.

06:52.000 --> 07:02.000
It's just reminding you, in the way of trying to experience the thing, simply for what it is.

07:02.000 --> 07:06.000
It's unique in that what it evokes again is nothing really.

07:06.000 --> 07:13.000
It's an absence, and it should feel like that. It should feel like you've got a respite.

07:13.000 --> 07:26.000
You've been given as a moment that is free from any of this thinking, or contemplating, or diversifying.

07:26.000 --> 07:35.000
So the first part of this question, this person who talks about applying Buddhist theory to the experience, and trying to, a common one that he wasn't mentioned,

07:35.000 --> 07:47.000
is saying to yourself, hey, that's impermanent. If you start to contemplate it as impermanent, it's a poor translation, or it's a misleading translation.

07:47.000 --> 07:52.000
When we read text where it says, one contemplates something as impermanent.

07:52.000 --> 08:02.000
It's generally better or more literal to say, seeds, or understands, or knows the thing as impermanent.

08:02.000 --> 08:09.000
That gets into our second part.

08:09.000 --> 08:14.000
But I'll just say a few more things about what else comes.

08:14.000 --> 08:19.000
I say when you note, you're really doing nothing, or you're creating nothing.

08:19.000 --> 08:23.000
The result is a state that is free, but there's much more to it than that.

08:23.000 --> 08:27.000
It's not just simply nothing.

08:27.000 --> 08:32.000
First of all, you're creating this objectivity, but second of all, you're creating a habit.

08:32.000 --> 08:38.000
You're creating a habitual awareness, or a habitual interaction with experience.

08:38.000 --> 08:51.000
So without meditating, a person who never has any idea to practice meditation is constantly engaging with experiences habitually.

08:51.000 --> 09:00.000
We think, and this person even mentions it, I think, in their questions about how when you experience something immediately you react to it.

09:00.000 --> 09:05.000
It's like this person explains it as being bound up in the experience.

09:05.000 --> 09:09.000
There's liking and disliking, and it's just a part of the experience.

09:09.000 --> 09:11.000
It's so immediate.

09:11.000 --> 09:15.000
So we would see this as habit.

09:15.000 --> 09:24.000
Our habits that we develop throughout our life in a kind of a similar way to the way we develop habits and meditation.

09:24.000 --> 09:31.000
But the habits and meditation, especially mindfulness meditation, are much more simple and singular.

09:31.000 --> 09:51.000
And so by saying to ourselves, pain, pain, or thinking, thinking, and evoking habit of experiencing things just as they are, it flows into our ordinary lives.

09:51.000 --> 10:03.000
And so we'll find ourselves walking down the street and experiencing things without stress, having thoughts, having interactions with people.

10:03.000 --> 10:13.000
We'll find ourselves at work in situations where we would be stressed, where we would be upset in family situations, where we would be reactive or reactionary.

10:13.000 --> 10:23.000
And finding ourselves far less reactionary because of the habits that we're developing. It's not magic. And it's not quick. It's not a quick fix.

10:23.000 --> 10:31.000
Because we're dealing with old habits that are years and years and even lifetimes perhaps old.

10:31.000 --> 10:44.000
But it happens quite clearly. And the evidence you can see for yourself, right? The Buddha said, teaching was something in the sea. For this very reason, it's not magic.

10:44.000 --> 10:50.000
It's not I'm talking about God or heaven or spirit or magical powers.

10:50.000 --> 10:56.000
I'm talking about simple principles of cultivating habits and their effect on it.

10:56.000 --> 11:08.000
The third thing it does is it breaks down bad habits. And that's, you know, as a part of building up good habits, is that all these reactions don't have the opportunity.

11:08.000 --> 11:19.000
So when you do nothing, we decrease and weaken our capacity, our potential to react to things.

11:19.000 --> 11:26.000
The fourth thing it does is it helps you to see more clearly your experience.

11:26.000 --> 11:32.000
So you'll see clearly how bad habits are bad. You'll see how bad anger is and greed is.

11:32.000 --> 11:39.000
And I can see it in arrogance and all that worry and restlessness and distraction and so on.

11:39.000 --> 11:47.000
But you'll also see more clearly the things that you like, the things that you normally like, the things that you normally dislike.

11:47.000 --> 12:05.000
You'll see clearly your thoughts and your rationalization or these investigations that go on in the mind.

12:05.000 --> 12:08.000
You'll see them clearly as well.

12:08.000 --> 12:21.000
And the great thing about that is that's where you see impermanence, suffering and nonself. You'll see that the things that you clung to or held onto or wanted are not worth wanting.

12:21.000 --> 12:31.000
Because the stability you saw in them, the satisfaction you saw in them, the control that you thought you had over them goes all too pot.

12:31.000 --> 12:39.000
It turns out to be an illusion. It turns out to be something you cooked up in your ignorance.

12:39.000 --> 12:49.000
And when you look more clearly at the experiences, you see that it's all unpredictable, in constant impermanence.

12:49.000 --> 12:58.000
It's unsatisfying and it's uncontrollable and there's no thing that you can control.

12:58.000 --> 13:05.000
So the nature of things is not about things that are possessible or controllable.

13:05.000 --> 13:11.000
And you'll see the chaos in your mind and in your body and so on.

13:11.000 --> 13:16.000
So these are the things that happen when you are mindful.

13:16.000 --> 13:20.000
When you just say to yourself, pain, pain, thinking, thinking.

13:20.000 --> 13:28.000
Question of whether noting could lead to clinging.

13:28.000 --> 13:39.000
It's often when someone first hears about this idea and we mentioned things like saying to yourself, angry, angry.

13:39.000 --> 13:44.000
It's often a question where the person asks, wouldn't that just make the anger worse?

13:44.000 --> 13:48.000
Wouldn't that just make me want the thing or create a thing more?

13:48.000 --> 14:01.000
And I suppose it's a reasonable or it's unsurprising that people will ask this, but for someone who's done meditation,

14:01.000 --> 14:10.000
it's hard to understand how someone could think that or how that could be possible.

14:10.000 --> 14:18.000
You might as well think that the sun might rise in the west of something.

14:18.000 --> 14:24.000
But to break it down intellectually, I'm going to give a talk, you know, talk a little bit intellectually.

14:24.000 --> 14:28.000
Just why it isn't so because if you practice meditation, you'll see that it isn't.

14:28.000 --> 14:35.000
So if you practice saying to yourself, pain, angry, angry or liking or whatever,

14:35.000 --> 14:41.000
if you say to yourself, pain, pain, why it doesn't lead to bad things?

14:41.000 --> 14:49.000
When we talk about why this is, it goes back to what I've just been talking about.

14:49.000 --> 14:58.000
But more particularly is talking about what it is that leads to clinging.

14:58.000 --> 15:07.000
Why is it that we cling to things? When we have something pleasant, why do we want it?

15:07.000 --> 15:13.000
And when we have wanting, what is it that leads us to go?

15:13.000 --> 15:20.000
And what is it that leads us to want more? When we have anger, what is it that leads to more anger?

15:20.000 --> 15:28.000
So it's first words noting that we can't actually be mindful in the present moment.

15:28.000 --> 15:37.000
And this is especially the case with anger and greed, for example, or unwholesome states.

15:37.000 --> 15:42.000
So when you have an experience of thought, for example, the thought has to come first,

15:42.000 --> 15:46.000
then you have to be mindful of it. You have to remind yourself that was thinking.

15:46.000 --> 15:50.000
So it's actually being mindful of something that happened in the past.

15:50.000 --> 15:58.000
And that's important because we have to understand what's going on here when we get angry or when we have clinging or so on.

15:58.000 --> 16:10.000
When we have an experience and then we cling to it, what is the process by which that occurs?

16:10.000 --> 16:19.000
So when we have anger, for example, anger and mindfulness can't exist in the same mindset.

16:19.000 --> 16:27.000
So what we're actually being aware of is the fact that we were angry.

16:27.000 --> 16:36.000
It's an acknowledgement of the fact that we're angry saying there was anger or there was anger and then saying to ourselves that was anger.

16:36.000 --> 16:55.000
So how that's different or how that's different from escalating or why that would in no way escalate is because escalating is when you say that something is this or that.

16:55.000 --> 16:58.000
When you have anger and you say that's bad.

16:58.000 --> 17:08.000
Or when the anger leads to a headache or attention or pain or sadness or so on, and you react to that, you say that's bad.

17:08.000 --> 17:18.000
So if someone says something to you and you get angry and the anger comes and then you think about what they said.

17:18.000 --> 17:22.000
You remember again what they said and this happens very quickly.

17:22.000 --> 17:33.000
You'll say, did you just say you're angry and angrily you'll say to them or to yourself, did you just say to me, I'm this or I'm bad.

17:33.000 --> 17:41.000
And that makes you more angry because you're evoking another experience after the anger and you're going to angry about that.

17:41.000 --> 17:51.000
And you're building up based on habits, you're escalating into more and more anger.

17:51.000 --> 18:01.000
And the same goes with this is similar to how I talk about talked about anxiety when you're anxious and then there's the physical feelings that come from anxiety and they make you more anxious.

18:01.000 --> 18:10.000
You get anxious because I'm anxious and you can feel it when it makes you more anxious and you can have a panic attack if it gets worse.

18:10.000 --> 18:21.000
When you say to yourself, for example, angry, angry or when you say to yourself, pain, pain, why it doesn't lead to bad things?

18:21.000 --> 18:30.000
Why the anger doesn't lead to more anger is because you're saying this is this rather than saying this is bad in creating a reaction, you're cutting the chain.

18:30.000 --> 18:41.000
It's quite simple and quite obvious. I think the question is based on our ordinary use of words.

18:41.000 --> 18:48.000
In an ordinary sense we might say, I'm so angry, but we don't mean it as an acceptance and an understanding that I am angry.

18:48.000 --> 18:54.000
We mean it as I'm getting angry or I want to be more angry.

18:54.000 --> 19:06.000
When you say I'm so angry, you're actually building more anger in the ordinary sense because you're not just being objective about it.

19:06.000 --> 19:16.000
So I don't know. A couple of interesting ideas about the noting technique about mindfulness in general.

19:16.000 --> 19:30.000
I think I take this approach because I think it's important to talk about this process of noting explicitly in detail.

19:30.000 --> 19:41.000
Sometimes I make videos about Buddhism and about meditation, talking about concepts and ideas without explicitly talking about the technique.

19:41.000 --> 19:51.000
It seems like sometimes people miss that and I've talked to people who are interested in my videos who have found them helpful.

19:51.000 --> 19:58.000
But you don't yet understand how it is repractives and so it stays on this intellectual level.

19:58.000 --> 20:10.000
I think it may have been a case with this person where they were engaged in analyzing the experiences which is different from being mindful of them.

20:10.000 --> 20:16.000
So there you go. I'm just a video for tonight. Thank you all for tuning in.

