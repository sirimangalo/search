1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhamabanda.

2
00:00:05,000 --> 00:00:12,000
Today we continue on with verse 130, which reads as follows.

3
00:00:12,000 --> 00:00:16,000
Sambait, tasanti, tandasa.

4
00:00:16,000 --> 00:00:20,000
Sambait, tasanti, vitangpyan.

5
00:00:20,000 --> 00:00:28,000
Atanang, pamangatwa, nahani, nagatani.

6
00:00:28,000 --> 00:00:32,000
Which is almost the same as the last one.

7
00:00:32,000 --> 00:00:38,000
It means all tremble at the rod.

8
00:00:38,000 --> 00:00:42,000
This is, this different is, giwitangpyan.

9
00:00:42,000 --> 00:00:43,000
Life is dear.

10
00:00:43,000 --> 00:00:46,000
Sambait, tasanti, tasanti.

11
00:00:46,000 --> 00:00:50,000
Life is dear to all.

12
00:00:50,000 --> 00:00:54,000
And then the same as the last one atanangpamangatwa.

13
00:00:54,000 --> 00:01:03,000
Having made a comparison with one's self, nahani, nagatani.

14
00:01:03,000 --> 00:01:09,000
And the story actually is said to be almost the same as the last one.

15
00:01:09,000 --> 00:01:12,000
The Buddha laid down two rules.

16
00:01:12,000 --> 00:01:20,000
And so the last story, it's about how he laid down the rule against hitting other monks.

17
00:01:20,000 --> 00:01:26,000
The group of six monks who were infamous for their escapades.

18
00:01:26,000 --> 00:01:37,000
Breaking in a lot of rules, and being the instigation for establishing many rules.

19
00:01:37,000 --> 00:01:46,000
They hit a group of 17 monks who had taken up residence.

20
00:01:46,000 --> 00:01:52,000
So this one doesn't say what actually happened, but for some reason or other they were angry at this.

21
00:01:52,000 --> 00:02:00,000
But in this case, it's interesting.

22
00:02:00,000 --> 00:02:03,000
The English translation, I think, gets it wrong.

23
00:02:03,000 --> 00:02:07,000
It's an English translation that says they hit them.

24
00:02:07,000 --> 00:02:14,000
And then this group of 17 monks held up their fist in response to scare them away.

25
00:02:14,000 --> 00:02:17,000
But I don't think that's what the Pali actually says.

26
00:02:17,000 --> 00:02:25,000
I can't quite get it for sure, but it certainly doesn't make it clear that it was in response.

27
00:02:25,000 --> 00:02:31,000
It appears to say that just like in the last one, where they actually hit the group of 17.

28
00:02:31,000 --> 00:02:38,000
In this one, the group of six monks just held up their fists to scare them.

29
00:02:38,000 --> 00:02:41,000
And again, the monks screamed.

30
00:02:41,000 --> 00:02:49,000
And again, the Buddha heard and asked, what's that scream about, and someone told him.

31
00:02:49,000 --> 00:02:55,000
And he said, nah, bikhui, ito, batai, a bikhui, nah, mam.

32
00:02:55,000 --> 00:02:58,000
Even got the bong.

33
00:02:58,000 --> 00:03:03,000
From here on monks, this is not to be done by monks.

34
00:03:03,000 --> 00:03:10,000
Whoever does it has broken a rule.

35
00:03:10,000 --> 00:03:12,000
And then he explains it.

36
00:03:12,000 --> 00:03:21,000
He says, bikhui, nah, mam, a yataa, hong, that they were a nay, bi, dandasa, tasanti.

37
00:03:21,000 --> 00:03:29,000
Just as I sow others tremble at the rod.

38
00:03:29,000 --> 00:03:34,000
Yataa, chammai, hong, tatay, wa nay, sangi, vidambhyam.

39
00:03:34,000 --> 00:03:44,000
Just as to me, and just as to me, sow to others, life is dear.

40
00:03:44,000 --> 00:03:51,000
Knowing this, nyaatwa, a yungyaatwa, knowing this, or iti nyaatwa.

41
00:03:51,000 --> 00:04:12,000
And so this is sort of the, as I already said, this is sort of the Buddhist version of the golden

42
00:04:12,000 --> 00:04:15,000
rule about doing unto others, as they were doing to you.

43
00:04:15,000 --> 00:04:22,000
But there's two interesting points that I can talk about for this one.

44
00:04:22,000 --> 00:04:24,000
The first is that they haven't actually done anything.

45
00:04:24,000 --> 00:04:28,000
You know, they didn't actually harm the other monks.

46
00:04:28,000 --> 00:04:36,000
And so often we place a lot of emphasis on the action.

47
00:04:36,000 --> 00:04:39,000
You know, when you actually harm someone.

48
00:04:39,000 --> 00:04:48,000
And it's not, it's not an in deeper esoteric teaching, but it's interesting to remind ourselves

49
00:04:48,000 --> 00:04:52,000
that it's not actually, it's not actually the acts itself.

50
00:04:52,000 --> 00:04:56,000
It's our own viciousness in the mind.

51
00:04:56,000 --> 00:05:00,000
When you raise your hand against someone, even just raising your hand.

52
00:05:00,000 --> 00:05:05,000
You know, a lot of abuse is, it's not even physical.

53
00:05:05,000 --> 00:05:09,000
It's emotional, it's the fear.

54
00:05:09,000 --> 00:05:22,000
And oftentimes it's the fear itself that is more harmful, that the actual bruises, the actual scars,

55
00:05:22,000 --> 00:05:30,000
heal much easier, much more completely than the mental scars.

56
00:05:30,000 --> 00:05:36,000
You hear stories about parents who tell their kids to go get them their belt, right?

57
00:05:36,000 --> 00:05:38,000
Go get me my belt.

58
00:05:38,000 --> 00:05:46,000
Because you imagine having knowing, like when you know what the meaning is, you know,

59
00:05:46,000 --> 00:05:53,000
you have to actually go and bring them the weapon they're going to use against you.

60
00:05:53,000 --> 00:05:58,000
And trauma.

61
00:05:58,000 --> 00:06:04,000
So that's one thing is remembering, especially for meditative purposes.

62
00:06:04,000 --> 00:06:10,000
You know, remembering to focus on our mind states, our intentions.

63
00:06:10,000 --> 00:06:14,000
It doesn't matter whether you actually kill or harm someone.

64
00:06:14,000 --> 00:06:27,000
It's the viciousness, the cruelty, the lack of compassion, the slavery to anger.

65
00:06:27,000 --> 00:06:33,000
You know, getting lost in our anger so we can't even see what we're doing.

66
00:06:33,000 --> 00:06:41,000
And that's the second thing to point out is that this isn't just an intellectual exercise where we say,

67
00:06:41,000 --> 00:06:46,000
yes, I don't want to be harmed and therefore it makes sense that I shouldn't harm others.

68
00:06:46,000 --> 00:06:47,000
It's not like that.

69
00:06:47,000 --> 00:06:56,000
It's the very fact that you don't want to be harmed yourself makes it perverse for you to harm others.

70
00:06:56,000 --> 00:07:05,000
And that's what makes it, or that's one way of explaining what makes it carmically active.

71
00:07:05,000 --> 00:07:11,000
That when you harm others, it comes back to harm you and you subject yourself to harm.

72
00:07:11,000 --> 00:07:15,000
Because you know in your mind that this is wrong.

73
00:07:15,000 --> 00:07:19,000
There's a sense that this is an evil thing.

74
00:07:19,000 --> 00:07:23,000
And by evil, I just mean something you wouldn't want to happen to you.

75
00:07:23,000 --> 00:07:37,000
And because you know that, when you do it to others, it leaves a scar on your mind.

76
00:07:37,000 --> 00:07:42,000
It perverts, your very reality.

77
00:07:42,000 --> 00:07:49,000
Because doing things that you want to happen to you is easy because there's a sense of the goodness of them.

78
00:07:49,000 --> 00:07:54,000
This is a good thing to do. So you do it for someone else. Why? Because it's a good thing.

79
00:07:54,000 --> 00:07:57,000
You don't have to feel the other person's happiness.

80
00:07:57,000 --> 00:07:59,000
You know that this is something that leads to happiness.

81
00:07:59,000 --> 00:08:02,000
So when you do it for them, it makes sense.

82
00:08:02,000 --> 00:08:07,000
To do something that causes harm to someone else actually is perverse.

83
00:08:07,000 --> 00:08:10,000
Because you know the harm in it.

84
00:08:10,000 --> 00:08:12,000
You know that this is an evil thing.

85
00:08:12,000 --> 00:08:14,000
And by evil again, this is a suffering thing.

86
00:08:14,000 --> 00:08:16,000
The thing that leads to suffering.

87
00:08:16,000 --> 00:08:24,000
So anyone who says that you should try and maximize your own happiness even at the expense of others.

88
00:08:24,000 --> 00:08:31,000
Or who argues that, you know, anyone who says look out for yourself first is being selfish.

89
00:08:31,000 --> 00:08:35,000
Because then they go and harm others doesn't really understand how it works.

90
00:08:35,000 --> 00:08:39,000
You can't be selfish and work for your own benefit.

91
00:08:39,000 --> 00:08:43,000
Anyone who is selfish is not working for their own benefit.

92
00:08:43,000 --> 00:08:46,000
Because they're doing things that they know are wrong.

93
00:08:46,000 --> 00:08:50,000
And again, wrong simply means things that lead to suffering.

94
00:08:50,000 --> 00:08:52,000
It's wrong because it leads to suffering.

95
00:08:52,000 --> 00:09:00,000
And there's no distinction that can be made between the suffering for oneself or the suffering for another.

96
00:09:00,000 --> 00:09:07,000
Because in your mind, there's an awareness, there's an understanding that this is a cause for suffering.

97
00:09:07,000 --> 00:09:21,000
There is that. So it necessarily involves anger, it involves unwholesome mind-state.

98
00:09:21,000 --> 00:09:26,000
And it leads you to be susceptible to harm yourself.

99
00:09:26,000 --> 00:09:39,000
When you harm others, the practical fallout is that you'll be afraid.

100
00:09:39,000 --> 00:09:48,000
You'll put yourself in the situation where you're paranoid about retribution.

101
00:09:48,000 --> 00:09:51,000
When you die, it will be an obsession in your mind.

102
00:09:51,000 --> 00:09:57,000
It's the kind of thing that comes back to haunt you when you harm the harm you've done to others.

103
00:09:57,000 --> 00:10:05,000
And so when you die, you end up with that as part of your rebirth, with that as part of who you are.

104
00:10:05,000 --> 00:10:12,000
Potentially leading you to hell even, but more likely just to lead you to a situation and abusive situation.

105
00:10:12,000 --> 00:10:23,000
Often this is what leads to these cycles of retribution where one person is one person harms another.

106
00:10:23,000 --> 00:10:30,000
They're both reborn and the other person, one person is the aggressor, the other is the victim, and it cycles like that.

107
00:10:30,000 --> 00:10:38,000
This kind of thing can happen. Because of the psychology of it, you harm others and you pervert your own situation.

108
00:10:38,000 --> 00:10:43,000
There's a sense that that is how Carnor works.

109
00:10:43,000 --> 00:10:56,000
The kind of thing that we discover through our practice, how these things affect our mind.

110
00:10:56,000 --> 00:11:02,000
It's not just a pretty saying, don't harm others because you wouldn't want that to happen to you.

111
00:11:02,000 --> 00:11:12,000
There are actually some depth to it that in fact that's very much a intrinsic part of how the mind works.

112
00:11:12,000 --> 00:11:20,000
So anyway, not too much to talk about because it's very similar, but we're continuing on.

113
00:11:20,000 --> 00:11:25,000
We've started the tense chapter. This is verse number two.

114
00:11:25,000 --> 00:11:32,000
So we're next to number 131. Thank you all for tuning in.

