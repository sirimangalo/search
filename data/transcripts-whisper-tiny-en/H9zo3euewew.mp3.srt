1
00:00:30,000 --> 00:00:50,480
Okay, good evening everyone, welcome to our weekly dumb session.

2
00:00:50,480 --> 00:00:58,280
These days we're going through the Maji Minikaya, the group of discourses of the Buddha

3
00:00:58,280 --> 00:01:05,400
that we call, we say, a middle length, so they're not too long and not too short.

4
00:01:05,400 --> 00:01:11,520
There are long ones that are also not too long, but they're longer, and they're also short

5
00:01:11,520 --> 00:01:19,960
ones which are not too short, but these are the middle length ones which, to put them in

6
00:01:19,960 --> 00:01:35,600
a special place is sort of just the right size for consumption, useful for our purposes,

7
00:01:35,600 --> 00:01:49,600
which are, our purposes are to provide inspiration and insight into the meditation practice,

8
00:01:49,600 --> 00:01:58,520
so we're trying to pick topics that will be useful and beneficial for our meditators to help

9
00:01:58,520 --> 00:02:08,920
us, help us free ourselves, we're in the our meditation as about changing the way we

10
00:02:08,920 --> 00:02:17,000
look at the world and opening up our hearts to the way things actually are, helping us

11
00:02:17,000 --> 00:02:25,640
to see that a lot of the way we look at the world is flawed and there's so many reasons

12
00:02:25,640 --> 00:02:39,160
for that, our reasons of culture, religion, also parents and role models, some of it's

13
00:02:39,160 --> 00:02:46,440
just due to chance, and then we culture evolves and sometimes just due to chance, the way

14
00:02:46,440 --> 00:03:01,720
beliefs evolve, anything goes, that story of any pennies and the sky is falling, something

15
00:03:01,720 --> 00:03:19,920
happens that it creates beliefs, that spread, being that much of our belief system and

16
00:03:19,920 --> 00:03:25,520
our way of looking at the world is mixture, we do the best we can, some of us do better

17
00:03:25,520 --> 00:03:32,080
than others, some do quite poorly and end up with views that cause them great suffering,

18
00:03:32,080 --> 00:03:39,560
stress, some do better, some are lucky enough, I would say luck is really the right word,

19
00:03:39,560 --> 00:03:51,880
but happen upon a way that is actually beneficial and needs to happiness, our intention

20
00:03:51,880 --> 00:04:00,000
and the Buddha's intention really in teaching was to clear all that up and provide some

21
00:04:00,000 --> 00:04:12,480
direction that if examined, followed and observed as far as results, one would be able

22
00:04:12,480 --> 00:04:18,080
to see that this is the way things actually are, much different than just the way I

23
00:04:18,080 --> 00:04:24,760
believe things to be, some of the suit has been skipping here, he goes over this again

24
00:04:24,760 --> 00:04:31,840
and again, belief turns out to be two, turns out one of two ways, right or wrong basically

25
00:04:31,840 --> 00:04:43,240
what he says, your religious books can say things and you say, well it says in this book,

26
00:04:43,240 --> 00:04:53,200
it can turn out one of two ways, it can be tradition, it can be, all sorts of, because

27
00:04:53,200 --> 00:05:06,520
your teacher, all these famous charismatic teachers say this, can turn out one of two ways.

28
00:05:06,520 --> 00:05:13,760
So today we're looking at the bunch at Diasuta, which is an interesting name, it means five

29
00:05:13,760 --> 00:05:18,120
and three, it doesn't really say what the five and the three are, but I think I have

30
00:05:18,120 --> 00:05:22,800
an idea, again I'm not going to go mainly through the suit, or completely through the

31
00:05:22,800 --> 00:05:29,640
suit, I'm encouraging to read it as with all of them, but again we're more interested

32
00:05:29,640 --> 00:05:35,280
in what sort of lesson it has for us, but this suit is really talking about the five

33
00:05:35,280 --> 00:05:44,360
is referring to five types of view, mainly about the self, and the three is talking about

34
00:05:44,360 --> 00:05:52,320
the three time periods, one gets caught up in views about the future, about the past,

35
00:05:52,320 --> 00:05:58,760
or one gets caught up about the present, that's what the suit does, it has a really interesting

36
00:05:58,760 --> 00:06:07,480
take on all three of these, that I think is useful for meditators, so there's some benefit

37
00:06:07,480 --> 00:06:10,720
here.

38
00:06:10,720 --> 00:06:16,200
So five, we don't really have to enumerate them, I mean there are four of them have

39
00:06:16,200 --> 00:06:25,040
to do itself, and really have to do with the future, we're talking about the three time

40
00:06:25,040 --> 00:06:33,920
periods, the three is the three time periods, and really what this sums up, I think quite

41
00:06:33,920 --> 00:06:45,760
well it sums up a lot if not all of the spirituality gone wrong, and that's, I mean it's

42
00:06:45,760 --> 00:07:01,440
harsh to say gone wrong, but you have to unpack this because, according, Buddhism sees the

43
00:07:01,440 --> 00:07:09,720
Buddha saw the whole of the universe, I'm not just talking about the physically universe,

44
00:07:09,720 --> 00:07:18,400
but all the realms of being, he called this samsara, we know this term is familiar, and

45
00:07:18,400 --> 00:07:23,160
in that context, in the context of calling this all samsara, anything that keeps you in

46
00:07:23,160 --> 00:07:32,400
samsara is wrong, and it's very specific sort of wrong, because someone can do right things

47
00:07:32,400 --> 00:07:36,920
in samsara, right, someone can be a good person and go to heaven, and you say well that's

48
00:07:36,920 --> 00:07:46,440
the right thing, he did the right thing, so we might not call it wrong and I don't want

49
00:07:46,440 --> 00:07:55,520
to be too harsh but limited, we call it spirituality that doesn't lean to a concrete goal

50
00:07:55,520 --> 00:08:05,680
or freedom, let's say, it's to samsara, to wandering around in circles, being born again

51
00:08:05,680 --> 00:08:20,640
is the idea, so the only thing in Buddhism that can be different is what he calls the cessation

52
00:08:20,640 --> 00:08:29,720
of formations, right, what does it call, the cessation of formations, but let's not, let's

53
00:08:29,720 --> 00:08:38,520
start, start, start, start, so people believe in self, and believe in self, let's make

54
00:08:38,520 --> 00:08:43,160
that out of this, because that's, pull that out of this, because that's a very important

55
00:08:43,160 --> 00:08:50,480
thing to pull out and talk about, clearly you don't have to, have the Buddha point out

56
00:08:50,480 --> 00:09:00,440
how important the self is, soul, I, I'm important that is, in western thought anyway,

57
00:09:00,440 --> 00:09:06,760
I don't know if you could argue that in other cultures, various cultures, there are various

58
00:09:06,760 --> 00:09:14,040
levels of this, but I think it's fair to say that most cultures, most people in the world

59
00:09:14,040 --> 00:09:19,560
have a strong sense of self, and that's very important to them, and the Buddha's time,

60
00:09:19,560 --> 00:09:26,360
it was very important, and it was disturbing for people that the Buddha would, would denounce

61
00:09:26,360 --> 00:09:35,880
their views, and so a lot of Buddhists have, have been quick to say that the Buddha thought

62
00:09:35,880 --> 00:09:41,360
there was no self, other Buddhists have been quick to say that the Buddha never said that,

63
00:09:41,360 --> 00:09:45,960
and the Buddha didn't teach, there was no self, the Buddha didn't deny the existence

64
00:09:45,960 --> 00:10:01,920
of self, and so we get all these different conflicting ideas and arguments, people believe

65
00:10:01,920 --> 00:10:09,520
in self, they have this idea that, particularly the Buddha refers to future, the idea

66
00:10:09,520 --> 00:10:22,480
that the soul continues on, the soul continues on, and is aware and unempaired after death,

67
00:10:22,480 --> 00:10:33,840
and then some other people, some people see that as limited, and you know, we're dwelling

68
00:10:33,840 --> 00:10:37,520
a little bit into the way they thought in India, because it might not seem that familiar,

69
00:10:37,520 --> 00:10:43,200
but I can show how it actually is quite familiar.

70
00:10:43,200 --> 00:10:55,000
If you think about the Hindu concept, the popular Hindu concept of Samsara, it's that

71
00:10:55,000 --> 00:11:08,320
the soul is born again and again, and it's part of this endless illusion of the world,

72
00:11:08,320 --> 00:11:14,520
and so someone would maybe be quick to say, that's not leaning anywhere, and that to

73
00:11:14,520 --> 00:11:21,000
do that again and again and again is missing something, and so there was a lot of talk

74
00:11:21,000 --> 00:11:28,440
about Moksha in India, and this idea of something beyond that, and so they would have

75
00:11:28,440 --> 00:11:34,880
these concepts of the soul becoming one with everything, one with God, you've probably heard

76
00:11:34,880 --> 00:11:42,240
of these concepts, and I sort of thought this was kind of an Indian concept, and you

77
00:11:42,240 --> 00:11:45,840
know, when you think of Christianity, take Christianity, because it's the other big one

78
00:11:45,840 --> 00:11:53,680
that talks about heaven, talks about the afterlife, Judaism, not so much, I guess Islam's

79
00:11:53,680 --> 00:11:58,920
well, but I don't know too much about it.

80
00:11:58,920 --> 00:12:07,640
So my concept of Christianity was that, you know, it's like fluffy clouds and people with

81
00:12:07,640 --> 00:12:16,000
rings around their heads and wings and stuff, the purlates and all that, but it was just

82
00:12:16,000 --> 00:12:24,440
reading today actually, Simone Wei is this Christian mystic, and someone said, well,

83
00:12:24,440 --> 00:12:30,040
if we don't see you in this life, I'll see you in the next, and she said, there will

84
00:12:30,040 --> 00:12:38,280
be no knowing each other in the next, something like that.

85
00:12:38,280 --> 00:12:45,320
So there's a deeper spirituality beyond this wandering one that sort of has the idea that

86
00:12:45,320 --> 00:12:52,520
there's some state, some higher state, to just being born again and again, or even going

87
00:12:52,520 --> 00:13:00,640
to heaven, it's becoming one with everything and so on.

88
00:13:00,640 --> 00:13:07,040
Now, other people who tried to jump even further and say, oh, no, no, that's still

89
00:13:07,040 --> 00:13:08,040
limited.

90
00:13:08,040 --> 00:13:11,440
You know, what is that state so what?

91
00:13:11,440 --> 00:13:21,080
So you're up in some oneness state, and they talk about something even, and I don't know,

92
00:13:21,080 --> 00:13:25,940
I don't want to go into this too detail, but they talk about deeper states that are

93
00:13:25,940 --> 00:13:32,040
neither percipient nor non-percipient.

94
00:13:32,040 --> 00:13:37,840
And the fourth state, of course, the annihilationist, the people who believe that when you

95
00:13:37,840 --> 00:13:41,920
die there's nothing, that should be more familiar.

96
00:13:41,920 --> 00:13:48,440
There's a lot of people in the West who in our culture that believe, who believe there's

97
00:13:48,440 --> 00:13:55,560
nothing after death, you know, we die, that's it.

98
00:13:55,560 --> 00:14:00,520
A lot of people have trouble with Buddhism, even though they want to, they agree with so

99
00:14:00,520 --> 00:14:05,320
much of it, but then they have trouble with the idea that there might be something after

100
00:14:05,320 --> 00:14:06,320
death.

101
00:14:06,320 --> 00:14:23,480
So fantastical and unsubstantiated, they would say, right, we don't see any afterlife here.

102
00:14:23,480 --> 00:14:29,680
What we see is people being born and then they die and they die, that's it.

103
00:14:29,680 --> 00:14:40,960
And the fifth type of person asserts nibana here and now, or the third it says, means

104
00:14:40,960 --> 00:14:45,000
they, the fifth type of person is special, and that's what we're going to talk about at

105
00:14:45,000 --> 00:14:52,320
the very end, but this type of person gets caught in the present, but the first four are

106
00:14:52,320 --> 00:14:53,320
really about the future.

107
00:14:53,320 --> 00:14:59,160
It's kind of a mixed up numbering system here, so I'm not really sorry if it sounds

108
00:14:59,160 --> 00:15:05,960
a bit confusing, because it kind of is.

109
00:15:05,960 --> 00:15:14,600
But first about the future, in particular about the self, so whether it be about our concepts

110
00:15:14,600 --> 00:15:21,200
of what happens after we die or our concept of the self, what the Buddha says about all

111
00:15:21,200 --> 00:15:31,720
of these is that they're gross, they're, they're coarse, which is important.

112
00:15:31,720 --> 00:15:36,040
The Buddha doesn't say it's true, and I've said this several times, the Buddha never

113
00:15:36,040 --> 00:15:38,880
says there is no self.

114
00:15:38,880 --> 00:15:43,280
People hear this about the Buddha that the Buddha taught, non-self, and so on, and they

115
00:15:43,280 --> 00:15:47,080
wonder, could really be no self?

116
00:15:47,080 --> 00:15:48,080
What would that even mean?

117
00:15:48,080 --> 00:15:52,320
They get very confused.

118
00:15:52,320 --> 00:15:56,400
And the confusion is, of course, the confusion is on this level, the same level of believing

119
00:15:56,400 --> 00:16:03,520
in a self, whether you believe a self or don't a self, the doubt about a self, even

120
00:16:03,520 --> 00:16:08,720
the realm of, of courseness of a mind state that is not useful, not helpful.

121
00:16:08,720 --> 00:16:14,960
It's very important that we understand how the Buddha replied that a person wanted to give

122
00:16:14,960 --> 00:16:20,280
the right answer, would not say no, no, wisdom teaches that there is no self, that's

123
00:16:20,280 --> 00:16:21,280
wrong.

124
00:16:21,280 --> 00:16:24,120
There, there, really, there is no self.

125
00:16:24,120 --> 00:16:28,400
Person would say, your beliefs and self are just pausing your stress and suffering, getting

126
00:16:28,400 --> 00:16:33,200
in your way, their course, and that's how the Buddha replied, he said, that's how the

127
00:16:33,200 --> 00:16:40,480
Buddha taught, he said, having found something better, having seen there's this, having

128
00:16:40,480 --> 00:16:49,360
known there is this seeing the escape from that, the tahgata has gone beyond that.

129
00:16:49,360 --> 00:16:57,360
So there is this, is referring to the freedom to the state of reality, really.

130
00:16:57,360 --> 00:17:04,880
If you want to paraphrase that, you could say, having seen reality, I'm going to shake

131
00:17:04,880 --> 00:17:07,880
his head at all these views that people have.

132
00:17:07,880 --> 00:17:13,920
Because if in your mind, you say to yourself, there is a self, what's going on in that moment,

133
00:17:13,920 --> 00:17:20,720
in that moment, the mind has some activity, there is some mental activity, in fact, even

134
00:17:20,720 --> 00:17:27,240
say the mind has would be just a convention, all that we know, all that really is happening

135
00:17:27,240 --> 00:17:33,720
is an experience of thought, the conception, there is a self.

136
00:17:33,720 --> 00:17:38,480
Same thing, if you say there isn't a self, same thing, if you say, I'm not sure, is there

137
00:17:38,480 --> 00:17:45,160
a self, or you worry about it, what happened to me, if there's no self, all of that

138
00:17:45,160 --> 00:17:51,440
is experience, and having seen this, the Buddha has no use for such views, Buddhists should

139
00:17:51,440 --> 00:18:00,360
have no use for such arguments or debates, because they're seeing experience, they're too

140
00:18:00,360 --> 00:18:12,840
busy with reality, they get caught up in all that stuff.

141
00:18:12,840 --> 00:18:22,440
And so it's a bit of a cop out, because it does an answer, the question that people often

142
00:18:22,440 --> 00:18:28,960
have is, prove it, what proof do you have, whatever it is, do you have that did anything

143
00:18:28,960 --> 00:18:31,000
after you die?

144
00:18:31,000 --> 00:18:35,840
The first three week needs of the discard by saying these people are just caught up in

145
00:18:35,840 --> 00:18:41,400
their views, but we say the same thing about the fourth person, a person who says, after

146
00:18:41,400 --> 00:18:42,400
death, there's nothing.

147
00:18:42,400 --> 00:18:47,720
He says, this person is also caught up in their views, which is sort of a cop out, because

148
00:18:47,720 --> 00:18:52,200
we're just, we don't have time, it's almost like we don't have time for that to entertain

149
00:18:52,200 --> 00:18:53,200
it.

150
00:18:53,200 --> 00:18:59,320
Let go of that, there's too much going on, and you ought to worry about the future, you've

151
00:18:59,320 --> 00:19:06,440
got to be concerned about where this is leading you, the Buddha says, such a person who gets

152
00:19:06,440 --> 00:19:12,320
caught up in this idea that there's nothing after death is still caught up, and they're

153
00:19:12,320 --> 00:19:18,960
going to go to a bad place when they die, so he doesn't give proof or evidence, which

154
00:19:18,960 --> 00:19:23,840
I think in the West, we often want this evidence that there is some life after death.

155
00:19:23,840 --> 00:19:31,440
I've talked about this before, it might take on it, the burden of proof lies on the people

156
00:19:31,440 --> 00:19:37,000
who say, experience stops at the moment of death.

157
00:19:37,000 --> 00:19:43,560
We know that experience exists, we have firsthand evidence, I can look and see, yes, there's

158
00:19:43,560 --> 00:19:48,880
experience, that's true, and yes, it continues on a moment after moment after moment.

159
00:19:48,880 --> 00:19:54,640
But person who says something happens, it's going to happen to you, that's going to stop

160
00:19:54,640 --> 00:19:55,640
this.

161
00:19:55,640 --> 00:20:00,640
I would say, well, prove it, provide me with evidence, and they show me the dead person

162
00:20:00,640 --> 00:20:05,720
and I'd say, that's nothing to do with experience, I don't know anything about that

163
00:20:05,720 --> 00:20:07,200
being's experience.

164
00:20:07,200 --> 00:20:12,640
All I know is that this experience and it continues on the period, if you start to look

165
00:20:12,640 --> 00:20:17,440
at reality in this way, it makes so much, it resonates with you.

166
00:20:17,440 --> 00:20:26,560
This way of looking at the world resonates because there's laws to it, if you do bad things,

167
00:20:26,560 --> 00:20:30,520
bad things happen to you, if you're mean and nasty, it corrupts your mind, if you're

168
00:20:30,520 --> 00:20:35,000
attached and greedy, if you become addicted, we can see how this works.

169
00:20:35,000 --> 00:20:41,480
The idea that a death that just stops is it creates a whole host of problems, that's probably

170
00:20:41,480 --> 00:20:47,360
where a lot of religion came from, the ideas of heaven and God and so on.

171
00:20:47,360 --> 00:20:51,440
Something of something higher, when, in fact, this is all there is, all there is is these

172
00:20:51,440 --> 00:21:01,240
experiences, death doesn't stop that, if you think it does, you've got to prove it.

173
00:21:01,240 --> 00:21:05,080
But this suit doesn't really about that question.

174
00:21:05,080 --> 00:21:10,480
It's about, it's about again, states of mind and state of a person's mind when they

175
00:21:10,480 --> 00:21:14,840
believe and hold on to any of these things, instead of being mindful and doing the work

176
00:21:14,840 --> 00:21:23,080
that needs to be done to purify their mind really, which is, according to Buddhism, the

177
00:21:23,080 --> 00:21:32,840
most important thing that we make our minds free from development, all these views and opinions

178
00:21:32,840 --> 00:21:34,680
don't really matter.

179
00:21:34,680 --> 00:21:41,520
So, as a second type of person has lots of views about the past, moving right along, we'll

180
00:21:41,520 --> 00:21:43,560
talk about that.

181
00:21:43,560 --> 00:21:48,720
That speculation about the past enables it as speculation about the past, but we could

182
00:21:48,720 --> 00:21:54,040
broaden it a little bit and talk about speculation about the world, because he says the

183
00:21:54,040 --> 00:21:59,640
self and world are eternal, not entirely about the past, you could say that I'm not

184
00:21:59,640 --> 00:22:04,480
going to argue with the Buddha, but just to keep it simple, we're talking about views

185
00:22:04,480 --> 00:22:14,880
about my metaphysics or no cosmology, views about, I guess it's called metaphysics.

186
00:22:14,880 --> 00:22:27,480
It's about the nature of the world, there's too many, there's a whole bunch, there's

187
00:22:27,480 --> 00:22:32,960
16 of them, I'm not going to win to them, but it's about the self and the world, it's

188
00:22:32,960 --> 00:22:40,040
about the nature of the world, and a lot about past life, I would guess, you know, if

189
00:22:40,040 --> 00:22:51,520
you're the idea of who you were before, he doesn't talk about that, though, and that's

190
00:22:51,520 --> 00:22:57,320
not because I think in West we're not so, I know a lot of our listeners have this sort

191
00:22:57,320 --> 00:23:01,600
of question, who was I in my past life, and the Buddha talks about that in other places,

192
00:23:01,600 --> 00:23:15,240
it's not talking about it here, there's speculation about the world really, what is the

193
00:23:15,240 --> 00:23:26,240
nature of the world, the speculate that we could turn maybe to the way religion looks

194
00:23:26,240 --> 00:23:33,960
at the world, religion has all sorts of views, the world was created in seven days, six

195
00:23:33,960 --> 00:23:47,920
days, six days, some people believe what's this belief that the world is on the back of

196
00:23:47,920 --> 00:23:59,160
the turtle, there's this funny belief, this kind of drissive, it's people, atheists

197
00:23:59,160 --> 00:24:07,280
or atheists will talk about, the earth is on the back of a turtle, then what's the turtle

198
00:24:07,280 --> 00:24:15,840
on the back of, and the person who is arguing, I know it's turtles all the way down, something

199
00:24:15,840 --> 00:24:28,880
like that, which they deride because it's not very well thought out, it doesn't answer

200
00:24:28,880 --> 00:24:39,040
the question, it's an infinite regress, but lots of views, views about the way things

201
00:24:39,040 --> 00:24:48,840
are, views about the self, views about the world, if you think of all the religious views,

202
00:24:48,840 --> 00:24:54,200
how Hindus believe that this is just an illusion, these things aren't actually here, this

203
00:24:54,200 --> 00:25:02,560
is God giving us something to play with, or God playing with us or something like that,

204
00:25:02,560 --> 00:25:08,520
they're not actually born and died, actually seeing, and none of these things are actually

205
00:25:08,520 --> 00:25:21,560
here, it's all this delusion, I hope people believe what leads to heaven, these terrorists

206
00:25:21,560 --> 00:25:28,200
who believe that if you kill people or kill yourself, you'll go to heaven, and Christians

207
00:25:28,200 --> 00:25:41,040
believe that if you put your faith in Jesus Christ, you'll go to heaven, lots of different

208
00:25:41,040 --> 00:25:53,800
beliefs, and what the Buddha says about his beliefs is that, it's important to distinguish,

209
00:25:53,800 --> 00:26:07,800
it's important because we fall into the habit of subsisting on belief, thinking that

210
00:26:07,800 --> 00:26:15,280
it's enough that I believe such and such, we gain comfort from our beliefs, I admit so

211
00:26:15,280 --> 00:26:22,000
many people in my time teaching so many people who have such strange beliefs, and you can

212
00:26:22,000 --> 00:26:27,880
hear it in their voice that they feel like it's enough, they shouldn't have to justify

213
00:26:27,880 --> 00:26:34,880
themselves or prove or provide evidence, it's satisfactory as human being, you kind of

214
00:26:34,880 --> 00:26:40,600
done your job as a spiritual being to believe something, right, we're talking about believing

215
00:26:40,600 --> 00:26:46,640
in something, and I hope it's clear that that's not really sufficient, you know, you can

216
00:26:46,640 --> 00:26:55,920
believe in the very wrong thing, having believe in something is by no means, support for

217
00:26:55,920 --> 00:27:05,640
it being useful, beneficial, not alone true, and so the Buddha says, I mean, the Buddha

218
00:27:05,640 --> 00:27:13,480
says, make a claim of knowing what's true, and that we all tend to, not just as Buddhists,

219
00:27:13,480 --> 00:27:21,120
we tend to make these claims because of how, because of our experiences, because of how

220
00:27:21,120 --> 00:27:30,280
empirical our practices, and that's important to remind ourselves, you know, I've seen

221
00:27:30,280 --> 00:27:40,720
what's real, this is real, real is experience, and so he says that anyone could know

222
00:27:40,720 --> 00:27:47,840
for themselves, basically put it this way, that anyone could investigate reality and find

223
00:27:47,840 --> 00:27:55,840
evidence for all this crazy views, is impossible, I mean, the Buddha knows that, knew that,

224
00:27:55,840 --> 00:28:01,680
all of us through our practice, if you practice, it's not, we don't have to call it practicing

225
00:28:01,680 --> 00:28:07,200
Buddhism, if you practice looking at reality, you'll see the same thing the Buddha did,

226
00:28:07,200 --> 00:28:15,560
I mean, you'll only see one thing, whatever it is, and it won't be all these crazy views,

227
00:28:15,560 --> 00:28:24,800
which come at a very, very different, many, many different things, they often, the most

228
00:28:24,800 --> 00:28:31,160
common is they come out of extrapolation, where you experience one thing and you extrapolate

229
00:28:31,160 --> 00:28:39,040
it to be something much, much more, like you feel bliss or peace, and every religious

230
00:28:39,040 --> 00:28:44,520
tradition out there will interpret it in a different way, the same experience of peace,

231
00:28:44,520 --> 00:28:50,160
except Buddhism would say, what's an experience of peace, and in fact he does, so let's

232
00:28:50,160 --> 00:28:57,240
move right along to the third section, when we get stuck in the present, some people

233
00:28:57,240 --> 00:29:04,880
are able to avoid these, what the Buddha calls future and past clings, clings to what

234
00:29:04,880 --> 00:29:10,240
happens to us in the future, and clings to about how the world has gotten the way it

235
00:29:10,240 --> 00:29:19,040
is, and I guess, how the way the world is, and they only focus on the present, and they

236
00:29:19,040 --> 00:29:27,400
become enlightened in the present, according to them, so some people find, as they call

237
00:29:27,400 --> 00:29:38,000
us, as they are saying, abides in the rapture of seclusion, and they give up views about

238
00:29:38,000 --> 00:29:44,920
the past, they give up sensual pleasure, and they enter into the rapture of seclusion,

239
00:29:44,920 --> 00:29:58,200
and the mind is secluded, peaceful, and they think, this is the peaceful, this is the sublime,

240
00:29:58,200 --> 00:30:11,160
this is Nimbana, they enter into rapture of seclusion, this rapture, this blissful state,

241
00:30:11,160 --> 00:30:18,040
and then that they then, and they scramble to get back, and when it doesn't come back,

242
00:30:18,040 --> 00:30:27,520
they experience grief, with the cessation of the rapture of seclusion grief arises, with

243
00:30:27,520 --> 00:30:43,520
the cessation of grief, the rapture of seclusion arises, and the Batagata, this is a word for

244
00:30:43,520 --> 00:30:44,280
the Buddha, understands, that is conditioned and gross, this is a refrain that goes through

245
00:30:44,280 --> 00:30:51,120
the Sutta, that is conditioned and gross, but there is, but there is, instead, the cessation

246
00:30:51,120 --> 00:30:56,760
of formations, having known there is this, seeing the escape from that, the Batagata has

247
00:30:56,760 --> 00:31:07,320
gone beyond that, so in this case, I mean, it's not bad to feel rapture of seclusion,

248
00:31:07,320 --> 00:31:19,280
but it's course and conditioned, it's limited, it's not, it's not an ultimate, good, it's

249
00:31:19,280 --> 00:31:28,360
problematic, because it's impermanence, because you can't control it, so another person

250
00:31:28,360 --> 00:31:34,000
goes beyond that and they experience our worldly pleasure, some people experience just

251
00:31:34,000 --> 00:31:40,840
complete calm and quiet, neither painful nor pleasant feeling, so they'll come and they'll

252
00:31:40,840 --> 00:31:47,040
explain it as this perfect tranquillity, stillness of mind, emptiness, there's lots of,

253
00:31:47,040 --> 00:31:55,200
the Buddha says, neither painful nor pleasant feeling, quite utilitarian description,

254
00:31:55,200 --> 00:31:59,320
which really cuts to the point, you can have all sorts of flowery, you can see this in

255
00:31:59,320 --> 00:32:11,920
oneness with God or oneness with the universe, and it's just a feeling, and the Buddha

256
00:32:11,920 --> 00:32:23,040
says, this is course, this is this is conditioned, what is conditioned means, it comes

257
00:32:23,040 --> 00:32:28,920
about because of causes, you did something and you got there, but the nature of conditioned

258
00:32:28,920 --> 00:32:44,840
things is they rely upon the supports, and when you stop supporting them, they disappear.

259
00:32:44,840 --> 00:33:10,800
And to someone even, and to these people who attain these states, and if they get deep

260
00:33:10,800 --> 00:33:16,560
into these states, they can think to themselves, I am at peace, I am attaining, I am

261
00:33:16,560 --> 00:33:25,720
without clinging, no, it's possible for someone to do attaining upon and have that sort

262
00:33:25,720 --> 00:33:31,560
of thought, attaining upon and I'm without clinging, but there are many, many people and

263
00:33:31,560 --> 00:33:35,920
many examples of people who have the idea that they're free, that they've attaining

264
00:33:35,920 --> 00:33:56,280
upon them, and when in fact they haven't, and of course the way to tell us, do they

265
00:33:56,280 --> 00:34:03,360
have clinging, is there some attachment, which really you can only find there being mindful

266
00:34:03,360 --> 00:34:10,000
and you can only see through strict and objective observation.

267
00:34:10,000 --> 00:34:14,840
If you take our practice, the practice that we do, none of this really is a problem, you're

268
00:34:14,840 --> 00:34:23,360
able to see the unglaringly problematic, all of this is and how at odds it is with the very

269
00:34:23,360 --> 00:34:37,040
simple truth of reality, you can see yourself holding on to view, holding on to your outlook

270
00:34:37,040 --> 00:34:39,840
and your beliefs.

271
00:34:39,840 --> 00:34:46,920
And so I'm going to set eye speech, the supreme state of sublime peace, which is liberation

272
00:34:46,920 --> 00:34:55,800
for not clinging, what they were trying to do is to work on their clinging, work on

273
00:34:55,800 --> 00:35:02,680
look at the things that we cling to, the things that we like and enjoy and want and strive

274
00:35:02,680 --> 00:35:10,840
for, and try to see them objectively, doesn't mean we're trying to see them without clinging,

275
00:35:10,840 --> 00:35:14,800
we're trying to look at them and answer the question of whether they're the worth clinging

276
00:35:14,800 --> 00:35:22,080
to, or not even asking that question, but look at them clearly to see the unfortunate

277
00:35:22,080 --> 00:35:26,280
truth that none of them are worth clinging to and that unfortunately we've been clinging

278
00:35:26,280 --> 00:35:32,440
to things that are not worth clinging to, that in fact nothing is worth clinging to, not

279
00:35:32,440 --> 00:35:43,120
even the view that I have attained Nibana, not even the idea of attained Nibana.

280
00:35:43,120 --> 00:35:53,600
And that is the bunch of tyacitor, some interesting insights, definitely with the read, but

281
00:35:53,600 --> 00:35:59,760
for now I have it's been somehow insightful and helpful to direct our attention and our

282
00:35:59,760 --> 00:36:13,760
meditation practice, that's the demo for tonight, thank you all for tuning in.

