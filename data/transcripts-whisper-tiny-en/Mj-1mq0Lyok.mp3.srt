1
00:00:00,000 --> 00:00:09,000
Hi, welcome back to Ask a Monk. Sorry I've been busy lately. I haven't had a chance to put any Ask a Monk videos up.

2
00:00:09,000 --> 00:00:22,000
I've been trying to get ready to go to Thailand, where I'll be spending the next two months, leaving at the end of this month, and I'll be there until around Christmas.

3
00:00:22,000 --> 00:00:47,000
Anyway, next question comes from inside is void. When I get set to meditate, should I try to seclude myself from things that might distract me, like keeping pets out of the room, or switching off devices that make sounds? Or should I allow those things in order to overcome them in meditation?

4
00:00:47,000 --> 00:00:58,000
Thanks. I think there are cases where you want to avoid certain things.

5
00:00:58,000 --> 00:01:15,000
Pets I think would be one of them because of the conscious interaction and the persistence of the phenomenon.

6
00:01:15,000 --> 00:01:28,000
The real point here is that it's how comfortable you are because in the end, there should be no difference between our formal practice and our practice and daily life.

7
00:01:28,000 --> 00:01:38,000
Formal practice is just that it's formal. It's a way of formalizing the act of being mindful.

8
00:01:38,000 --> 00:01:56,000
In the benefit of it, it's easier, it's more intense. It's like taking things out of the environment so that you can do laboratory experiments, and the experiment in the laboratory is much more pure.

9
00:01:56,000 --> 00:02:10,000
It's really the only way to train yourself in the principles that we're talking about here, is to take yourself away from those things.

10
00:02:10,000 --> 00:02:30,000
Now, that really only applies to those things that are going to be a distraction in your meditation. Sounds like a clock ticking, or air conditioning, or even external sounds outside.

11
00:02:30,000 --> 00:02:40,000
Most of the sounds that we have to deal with, cars going by, or whatever don't need to be a distraction. Obviously, they can become a part of the meditation.

12
00:02:40,000 --> 00:02:53,000
It's where it gets to the point where it's going to interrupt your meditation and become, it's going to take you away from your stomach rising.

13
00:02:53,000 --> 00:03:05,000
A cat crawling over you and clawing you can be an object of meditation, but the intensity of it and the conscious interaction will necessarily be a diversion.

14
00:03:05,000 --> 00:03:16,000
I think what you'll find more is that when you try to avoid certain phenomena that they actually distract you more in life.

15
00:03:16,000 --> 00:03:34,000
It becomes an obsession, and that becomes something that blocks you in the practice. For instance, people who try to take a clock out of the room, or remove certain sounds, will find that they then can never meditate with those sounds.

16
00:03:34,000 --> 00:03:49,000
When I first started, everyone, we were always trying to get the clock out of the room, put it under a pillow, or so on. I had an alarm clock, someone gave me, and I was just taking it under a pillow, and then I could still hear it, and there's a blanket on top of that.

17
00:03:49,000 --> 00:04:11,000
I had no longer hear it, because I found that I was walking, when I did my walking meditation, I'd be walking in sync with the clock all the time, and when I did my sitting meditation, I'd be breathing in sync, but then the problem came when I moved to another room with a couple of other meditators, and there was a clock in the room, and I had no opportunity to remove it.

18
00:04:11,000 --> 00:04:26,000
It drove me crazy, because it got in a gotten worse, because of my inability to accept it. Until finally, I just gave up, and if I was walking in sync with the clock, I was walking in sync with the clock, and eventually was able to overcome it.

19
00:04:26,000 --> 00:04:45,000
And that's obviously very important in the meditation practice to overcome our attachments as opposed to feeding them. So, things like pets, people, music, I would recommend all of these things should be removed from your meditation practice.

20
00:04:45,000 --> 00:05:01,000
So, if there are people talking in the room, that's incredibly difficult to meditate, because you're obviously thinking about what they're talking. If there's music, that's quite difficult to meditate, because your mind gets into this rhythm, and it's kind of like...

21
00:05:01,000 --> 00:05:18,000
It's a stimulus, and it keeps you happy, it keeps you calm, doesn't allow you to deal with the natural state of the mind, pets, obviously, as I said, can be a distraction as well.

22
00:05:18,000 --> 00:05:36,000
So, those are the few specific things, the more extreme stimulants, but as far as noises and finding a quiet place, I would recommend against it. In fact, I would recommend trying to practice in places where you wouldn't normally do.

23
00:05:36,000 --> 00:05:46,000
When I'm in an airport, I'll be going to Thailand when I'm in the airport. I'll do walking, meditation, and sitting meditation, even with incredibly loud noise, even with people talking.

24
00:05:46,000 --> 00:05:57,000
And you'll be vacuuming and vehicles going by or whatever, because you can pull these things into your meditation practice.

25
00:05:57,000 --> 00:06:09,000
So, in most cases, I would say, just let it be, let the noise become part of your meditation as opposed to letting it build up the aversion to it.

26
00:06:09,000 --> 00:06:18,000
So, hope that helps.

