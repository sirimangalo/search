1
00:00:00,000 --> 00:00:29,880
Okay, good evening everyone, welcome to our daily session, topic I wanted to focus on

2
00:00:29,880 --> 00:00:57,840
tonight is related to delusion or ignorance, so curious characteristic quality of

3
00:00:57,840 --> 00:01:21,240
samsara, that it may come to be in a state where we, where we don't see things as

4
00:01:21,240 --> 00:01:51,000
they are, I might sound insignificant, but it's actually incredibly important statement.

5
00:01:51,000 --> 00:02:00,080
We know this in science, modern material science, that's why they avoid the best of their

6
00:02:00,080 --> 00:02:13,320
ability, dependence on perception, and the realization that our perception can be wrong.

7
00:02:13,320 --> 00:02:20,200
It's deceiving how simple that statement is, and you might think, I get it, I understand

8
00:02:20,200 --> 00:02:29,440
that, but it's guaranteed that when you begin to explore the realm of perception through

9
00:02:29,440 --> 00:02:41,560
meditation practice, that you'll be surprised, that you'll be shocked with the realization

10
00:02:41,560 --> 00:02:56,440
that you're really just not even distorted reality, but completely misunderstood it, and

11
00:02:56,440 --> 00:03:12,480
totally, to the extent that black seemed like white, good seemed like bad seemed good.

12
00:03:12,480 --> 00:03:19,400
So in modern material science, they reject perception for this reason, you can't rely on

13
00:03:19,400 --> 00:03:31,800
your perception. They recognize this through studies, through experiment. I think in religion,

14
00:03:31,800 --> 00:03:44,200
of course, in spirituality, where we're prone to succumb to this. It's how you can come

15
00:03:44,200 --> 00:03:58,200
to be so sure of your beliefs, so sure of your convictions, and only have the internet

16
00:03:58,200 --> 00:04:26,360
to be wrong. Remember, once driving around and chin

17
00:04:26,360 --> 00:04:37,920
my, and we were headed somewhere, our whole troop, when I was teaching, we were heading

18
00:04:37,920 --> 00:04:43,800
somewhere to the, we had to get to the superhighway, and I told him to turn left, I said,

19
00:04:43,800 --> 00:04:49,240
you have to turn left, there's the super highway, the slouch and monkeys, no, no, that's

20
00:04:49,240 --> 00:04:55,560
not the superhighway, but I demanded, and finally they turned left, and then I looked and I said,

21
00:04:55,560 --> 00:05:16,640
oh, this isn't the superhighway. It's very easy to, it's very easy to be completely

22
00:05:16,640 --> 00:05:36,560
sure of yourself and be wrong. The problem, of course, with this stance of material science,

23
00:05:36,560 --> 00:05:50,120
is that it ignores a very terrible problem with, with our reality and our existence and attempts

24
00:05:50,120 --> 00:06:05,720
to sidestep the problem by strict reliance on, on third person, impersonal, not third

25
00:06:05,720 --> 00:06:19,360
person, but impersonal observations, find ways, double blind tests, but that doesn't solve

26
00:06:19,360 --> 00:06:24,920
the problem that we are really messed up in our perceptions, in more ways than science

27
00:06:24,920 --> 00:06:39,240
even is, I think, clear about, able to understand. And so Buddhism does something bold

28
00:06:39,240 --> 00:06:57,760
and perhaps seen as impossible by a material scientist, and that is to correct our perceptions,

29
00:06:57,760 --> 00:07:05,680
to find a way of being non, I don't know, I used to say objective, but I think that word

30
00:07:05,680 --> 00:07:08,840
is used differently by different people, I'm going to say objective, what I mean by that

31
00:07:08,840 --> 00:07:24,320
is impartial or unbiased, and even more than unbiased, clear, seeing clearly. We claim

32
00:07:24,320 --> 00:07:41,360
the ability to correct our misperceptions and to dispel the delusion and the ignorance.

33
00:07:41,360 --> 00:07:48,560
But I want to emphasize this point and it's, it's, it's both incredibly important

34
00:07:48,560 --> 00:08:00,840
to understand and incredibly important to understand as being the root of the problem that

35
00:08:00,840 --> 00:08:06,000
we can misunderstand, that we can be completely and utterly wrong about something, such

36
00:08:06,000 --> 00:08:13,360
that there's no hint of it being wrong, apart from the consequences of course. And this

37
00:08:13,360 --> 00:08:18,640
is the thing is, well, if we perceive things in a certain way, what's wrong with that, right?

38
00:08:18,640 --> 00:08:23,600
If I think ice cream is good, what's wrong with that? You like, I like chocolate ice cream,

39
00:08:23,600 --> 00:08:31,240
you like vanilla ice cream, right? We all like certain things, we all have certain partialities,

40
00:08:31,240 --> 00:08:35,640
perceptions, we all have certain, that's not even talk about partialities, but we all have

41
00:08:35,640 --> 00:08:43,280
certain character types. I have a short temper, I say, or I know I'm this or I'm that.

42
00:08:43,280 --> 00:08:52,360
I'm A-type personality, B-type personality, it's just who I am. And so you find people

43
00:08:52,360 --> 00:09:01,960
trying to perform mental gymnastics to try to make sure that everyone, whatever person

44
00:09:01,960 --> 00:09:13,800
type of person they are, is understood as being equally valid. So if our perception is different

45
00:09:13,800 --> 00:09:18,040
from someone else's perception, what's wrong with that? And the problem is that there is,

46
00:09:18,040 --> 00:09:24,320
as it turns out, an underlying reality, that doesn't change. You think, well, my reality

47
00:09:24,320 --> 00:09:34,240
is different from ours, well, actually, it's not. And there's no one in this world who can

48
00:09:34,240 --> 00:09:48,000
escape a certain fairly simple qualities or characteristics of reality. So greed, for example,

49
00:09:48,000 --> 00:09:57,520
greed and craving and desire and thirst and ambition, these are all very good examples of this.

50
00:09:57,520 --> 00:10:09,000
We wonder how a person can be, how a person can be driven to rape another person, for

51
00:10:09,000 --> 00:10:32,800
example. We study addiction, we study passion or desire, sexual desires, big one, we study

52
00:10:32,800 --> 00:10:45,040
addiction to food and so on. And we can't deny that there are negative consequences to desire.

53
00:10:45,040 --> 00:10:50,880
And so intellectually, this is easy to accept that, perhaps not clear to us, but should

54
00:10:50,880 --> 00:11:02,080
be easy to understand that there is an underlying reality that doesn't allow for addiction,

55
00:11:02,080 --> 00:11:10,120
that doesn't allow for a peaceful and a happy, addicted state. It's a reality whereby

56
00:11:10,120 --> 00:11:20,640
addiction causes suffering. But this idea of seeing things completely wrong helps us to understand

57
00:11:20,640 --> 00:11:28,640
why intellectual understanding isn't enough. Intellectual understanding, what is it? It's a thought.

58
00:11:28,640 --> 00:11:35,840
There's no such thing as intellectual understanding. It's a series of thoughts that lead to

59
00:11:35,840 --> 00:11:40,960
a single thought with a lot of conviction. So you have this thought, which is your premises,

60
00:11:40,960 --> 00:11:44,800
at least to this thought and this thought. And finally, you come up with a conclusion.

61
00:11:46,960 --> 00:11:52,000
And that has some power to it. It gives you conviction. So it will, to some extent,

62
00:11:52,000 --> 00:11:57,280
affect your behavior. But it doesn't change the fact that when you see cheesecake,

63
00:11:57,280 --> 00:12:02,720
you like it. You want it. You think you feel that that is going to bring you happiness.

64
00:12:03,360 --> 00:12:07,520
When you see the object of your desire, a beautiful man, a beautiful woman,

65
00:12:08,480 --> 00:12:18,800
when you see the body, when you see things, when you hear music, doesn't change the fact

66
00:12:18,800 --> 00:12:37,280
that we get attached, that we become addicted, that it blinds us to any suffering, that you

67
00:12:37,280 --> 00:12:45,120
be completely blind. You can rationalize, you can examine your life and see how terrible you are,

68
00:12:45,120 --> 00:12:51,280
are terribly, you are destroying your life, read Dostiovsky, some of his stuff,

69
00:12:52,400 --> 00:12:58,320
pretty hard core suffering. And he could write about it. He knew what he was doing to his life,

70
00:13:00,080 --> 00:13:08,640
to his mind. But the addict can't help themselves. It's on totally different,

71
00:13:09,600 --> 00:13:14,560
on a totally different level. This is how someone could write, how in the time of the Buddha,

72
00:13:14,560 --> 00:13:23,040
there was this novice, Samanera, who raped his cousin, who was a bikkhuni. She was in Harahan.

73
00:13:26,720 --> 00:13:33,040
And when he left her kutti, her hut, the earth swallowed him up, they say,

74
00:13:34,800 --> 00:13:40,800
or he died and went to hell. That a person can't see that that's wrong.

75
00:13:40,800 --> 00:13:46,000
And yet, there are many people in this world who can't see it's wrong and we want to call them

76
00:13:46,000 --> 00:13:57,360
monsters. We want to say that that is a person very different from us. That person is nothing

77
00:13:57,360 --> 00:14:09,280
like us. But we all have this in us. And to some extent, it's incredibly

78
00:14:09,280 --> 00:14:15,120
blameworthy, but it's also, in a sense, blameless, because according to their perception,

79
00:14:15,120 --> 00:14:21,920
it's not blameless, but it's, you understand what I mean? It's completely purely,

80
00:14:22,720 --> 00:14:28,880
this is going to make me happy. There's total ignorance and delusion and lack of clarity.

81
00:14:29,840 --> 00:14:38,240
They're unable to see is what I mean. At that moment, the mind is consumed, the mind is clear

82
00:14:38,240 --> 00:14:42,880
that this is going to make me happy. And then, oops, boy, was I wrong.

83
00:14:47,040 --> 00:14:52,320
That's how this, some sorrow works. That's how we get stuck and why we stay stuck.

84
00:14:54,000 --> 00:14:59,040
And that's how insight meditation works. That's why insight meditation works. That's why

85
00:15:00,320 --> 00:15:05,520
the answer isn't to force yourself to follow your intellectual understanding, but it's to see

86
00:15:05,520 --> 00:15:12,560
clearly. Anger is another one. Anger is, it's incredible what anger will do to you.

87
00:15:16,800 --> 00:15:27,600
There was time of the Buddha. There was this, his stepfather.

88
00:15:27,600 --> 00:15:34,800
There was so angry at him. So angry at the Buddha,

89
00:15:39,600 --> 00:15:46,080
for taking away his, taking for leaving his daughter first of all, when he left the home,

90
00:15:46,080 --> 00:15:52,080
and then for later on for ordaining her, and taking her away from him, as well, along with his

91
00:15:52,080 --> 00:16:00,000
grandson, Raoula, and he ended up going to hell as well. And Devadatta is another example.

92
00:16:00,000 --> 00:16:05,840
Devadatta is said to have spouted, vomited blood. He was so angry.

93
00:16:05,840 --> 00:16:23,920
We get angry and it blinds us. At that moment, we really think that anger is the best solution.

94
00:16:25,920 --> 00:16:30,320
We don't at that moment have conscious awareness that this is wrong.

95
00:16:30,320 --> 00:16:35,440
Even though intellectually we might know and we might try to be a nice, nice people.

96
00:16:38,080 --> 00:16:45,280
It's quite common for Buddhists. We are nice people, right? Why? Because we have all these

97
00:16:45,280 --> 00:16:50,000
teachings on how to be nice people. Christians, many Christians are nice people for similar

98
00:16:50,000 --> 00:16:56,400
reasons, because Christianity, I think, is particularly concerned with being nice. Turn the

99
00:16:56,400 --> 00:17:01,600
other cheek. It's very powerful saying Christians can be really nice. I've had some really nice

100
00:17:01,600 --> 00:17:08,240
Christians. Of course, they have lots of other crazy ideas, but the niceness is there. But as

101
00:17:08,240 --> 00:17:16,240
with Buddhists, we can be very nice, but don't get us upset. Don't poke the hornets nest,

102
00:17:16,240 --> 00:17:24,320
because Buddhists will get just as angry as anyone else. I've had monks yelling at me,

103
00:17:24,320 --> 00:17:32,800
remember early on I have to tell this story. I've come back from Canada. I spent some

104
00:17:32,800 --> 00:17:39,040
spent my first reigns retreat in Canada. I came back to Thailand and not hadn't been there two

105
00:17:39,040 --> 00:17:43,760
weeks and we're sitting in the dining hall. Suddenly there was a clatter beside us and I turn over

106
00:17:43,760 --> 00:17:48,480
all the monks sitting in the dining room. I turn and one of the monks has gotten up and he's just

107
00:17:48,480 --> 00:17:54,240
pounding on one of the other monks. His nose is broken. He's got a blood going everywhere.

108
00:17:58,800 --> 00:18:04,320
So I got up and started and grabbed this. I remember it of all the monks. I was the only one.

109
00:18:12,720 --> 00:18:17,440
At that moment, he thought punching this other monk was the right thing to do.

110
00:18:17,440 --> 00:18:22,640
I mean isn't it crazy how he can think, especially as a Buddhist monk, how he could come to the

111
00:18:22,640 --> 00:18:28,480
point where he thought that that was a good idea. He was totally blind and he knew it. He felt

112
00:18:28,480 --> 00:18:34,080
awful about it. He terribly ashamed. It's not a bad monk, he's just

113
00:18:34,080 --> 00:18:48,480
a strong anger inside. He's built up and delusion. I mean David, that doesn't

114
00:18:48,480 --> 00:18:57,040
good example of delusion. Imagine, this guy wanted to be a desire as well. He wanted to be

115
00:18:57,040 --> 00:19:05,040
the head of the leader. He wanted the Buddha. He had his arrogance. That's delusion. We hold

116
00:19:05,040 --> 00:19:12,160
ourselves up, how we puff ourselves up, how we think how important we are. Look at these people

117
00:19:12,160 --> 00:19:18,480
who are all puffed up. Anyone who has self-importance and that's kind of ridiculous really.

118
00:19:18,480 --> 00:19:30,560
They become less attractive, right? They become less impressive. They make everybody angry around

119
00:19:30,560 --> 00:19:38,320
them and they make everyone afraid of them and they create such suffering. There's no greatness

120
00:19:38,320 --> 00:19:51,120
to be had in arrogance and pride. It's unpleasant and people don't see this, right?

121
00:19:52,400 --> 00:19:58,080
And don't see how silly they look when they puff themselves up, like blowfish or like a peacock.

122
00:20:01,280 --> 00:20:08,240
We really think that there's good to be had by showing off. If you ever hear people

123
00:20:08,240 --> 00:20:13,360
it's a there's culture. I've been in cultures, I guess I won't say which, but I've been in

124
00:20:13,360 --> 00:20:20,400
cultures where some of the people are incredibly inclined to brag about themselves. It's become

125
00:20:20,400 --> 00:20:28,560
a culture. In fact, where they tell you how great they are, how people are intent upon

126
00:20:28,560 --> 00:20:39,680
impressing upon you, how great they are. Mostly it just frustrates us and it makes us feel jealous

127
00:20:39,680 --> 00:20:43,200
and you know, because we're caught up in all that. But if you step back and as a meditative do you

128
00:20:43,200 --> 00:20:50,160
think, you know, who could think that such a thing is a good idea, but of course at the moment

129
00:20:50,160 --> 00:20:56,240
for that person. I think showing off somehow impresses people. Somehow makes people like you more

130
00:20:56,240 --> 00:21:01,440
or something. I don't know. That's sort of what we think. We think we're going to feel good. It'll

131
00:21:01,440 --> 00:21:08,720
make us feel good, right? Because inside we usually hate ourselves. We have lots of self-esteem issues,

132
00:21:08,720 --> 00:21:15,920
so we compensate by see if everybody else thinks I'm great and then maybe I'll be great.

133
00:21:19,360 --> 00:21:24,880
It's kind of funny to watch. This is why this is why Buddhas and Arahant have a funny sense of humor.

134
00:21:24,880 --> 00:21:29,600
They smile at the oddest things because it's kind of, well they're not odd, really. It's just

135
00:21:29,600 --> 00:21:34,560
that all of us are too blind to see the humor in it.

136
00:21:34,560 --> 00:21:58,080
It's starting around like peacocks, fighting like roosters, and copy they didn't like dogs in

137
00:21:58,080 --> 00:22:05,920
heat wherever caught up in greed, anger, delusion. If you look at things like food, and if you

138
00:22:05,920 --> 00:22:10,880
ever watch people stuffing their face, if you look at even sexual activity. I mean, how ridiculous

139
00:22:10,880 --> 00:22:17,360
is it? How ridiculous is sexual sex? What a silly activity. Kissing, kissing this start with.

140
00:22:18,560 --> 00:22:22,880
What is kissing? Just smack our lips together for a while.

141
00:22:22,880 --> 00:22:32,320
Isn't it absurd? Yet when you're kissing it seems like the pinnacle, right? And people have

142
00:22:32,320 --> 00:22:43,440
written poems and songs and books about kissing, about sex, about food, recipe books. Doesn't it

143
00:22:43,440 --> 00:22:57,360
make you stop and scratch your head? We have books with pictures of fat and grains and oils and

144
00:22:57,360 --> 00:23:07,760
salts and sugars and stuff. All this stuff that eventually becomes urine and feces. I mean,

145
00:23:07,760 --> 00:23:14,640
that's what we're writing and that's what we're making books about and praising and poems and everything.

146
00:23:15,600 --> 00:23:25,120
So, I mean, it's ridiculous, but the key, the more important point is this understanding of how

147
00:23:25,120 --> 00:23:32,960
wrong we can be, of realizing that we don't see it as ridiculous. And it's not even that it's unclear

148
00:23:32,960 --> 00:23:46,080
to us. It's that it really undeniably seems meaningful. So, the key out of that is that we're wrong

149
00:23:47,040 --> 00:23:54,160
and that we can prove to ourselves that we're wrong and what we're wrong about, very easily, in fact.

150
00:23:54,160 --> 00:24:05,120
All you have to do is look when a person looks, when you look you see. When you look you see,

151
00:24:05,120 --> 00:24:10,080
when you see you know, if you want to know you have to look, if you want to know you have to see,

152
00:24:10,080 --> 00:24:20,000
if you want to see you have to look. Look, see, no. When you practice mindfulness, when you cultivate

153
00:24:20,000 --> 00:24:30,160
mindfulness, desire and the objects of your desire seem undesirable. They become undesirable. You

154
00:24:30,160 --> 00:24:38,640
can clearly see, geez, what am I doing, lusting after the human body? What am I doing, lusting

155
00:24:38,640 --> 00:24:52,320
after this taste in my mouth, sugar and salt? It's like you pull away the veil of ignorance.

156
00:24:52,320 --> 00:24:58,240
It's like you turn on a light at Jentong, this monk here. He said, once I caught it on tape,

157
00:24:58,240 --> 00:25:06,160
he said, mindfulness is like a light. All the evil things, it was just a really, really good way

158
00:25:06,160 --> 00:25:11,360
of putting all the evil things, they're all darkness. Mindfulness is like a light. When you turn on

159
00:25:11,360 --> 00:25:19,520
the light, the darkness disappears. That's really true. In that moment, when you're mindful,

160
00:25:19,520 --> 00:25:20,880
the darkness is disappeared.

161
00:25:26,960 --> 00:25:32,320
So important for us to understand, important and an important reason for us to be very careful

162
00:25:32,320 --> 00:25:43,760
about self-confidence, being sure of ourselves. I think one of the things a meditator

163
00:25:43,760 --> 00:25:48,400
learns early on is that you can't trust yourself. You can't trust intuition.

164
00:25:49,840 --> 00:25:59,280
You have no reason to trust yourself. But unlike material scientists, you can change that.

165
00:25:59,280 --> 00:26:08,960
You don't need to trust yourself, but that you can come to see through your delusion. You can

166
00:26:08,960 --> 00:26:20,240
come to see your misunderstandings. And you can come to clear them up. To the point that,

167
00:26:20,240 --> 00:26:23,440
I mean the argument might be made how do you know that then you're seeing things clearly,

168
00:26:23,440 --> 00:26:32,240
but it's the point that they then become in line with reality. So you might say,

169
00:26:32,240 --> 00:26:35,920
after your practice meditation, how do you know you're still not seeing things incorrectly?

170
00:26:38,080 --> 00:26:41,680
The difference is that now it's in light, now your understanding is in line with reality.

171
00:26:41,680 --> 00:26:47,520
And that's important. That's clear. The way we see things saw things before was not in line

172
00:26:47,520 --> 00:26:51,600
with reality. These things that couldn't bring us happiness, we thought they could bring us

173
00:26:51,600 --> 00:26:56,800
happiness. They can't. You see impermanence. You see suffering. You see non-self. You see these

174
00:26:56,800 --> 00:27:02,400
qualities of reality that nothing is stable. Because of that nothing can satisfy us. There's nothing

175
00:27:03,600 --> 00:27:08,400
that is mean that is mine. There's nothing that is under our control.

176
00:27:08,400 --> 00:27:24,320
So there you go. There's the dhamma for tonight. Thank you all for tuning in.

177
00:27:38,400 --> 00:28:03,040
Okay, so we have how's the volume on the live broadcast?

178
00:28:03,040 --> 00:28:10,320
Does anyone listening to the live audio broadcast?

179
00:28:15,760 --> 00:28:22,160
Noisey and slightly high pitched. Someone said it was two.

180
00:28:25,040 --> 00:28:31,440
Someone said it was two. Is the second life audio high pitched? Like two, two, two.

181
00:28:31,440 --> 00:28:41,200
Like my voice has been become. Okay, let me re-log.

