1
00:00:00,000 --> 00:00:04,340
Hello and welcome back to our study of the Damapada.

2
00:00:04,340 --> 00:00:13,780
Today we continue on with verses 2.49 and 2.50, which read as follows.

3
00:00:13,780 --> 00:00:20,560
Who in study is the chapter of Diwavar and the

4
00:00:20,560 --> 00:00:14,780
perfect Matthew of the

5
00:00:14,780 --> 00:00:37,740
your

6
00:00:37,740 --> 00:00:39,760
dz deserving

7
00:00:39,760 --> 00:01:09,700
which means people give according to their faith according to their faith

8
00:01:09,700 --> 00:01:29,860
to their conviction, whoever becomes disturbed or upset by that, by the giving of food

9
00:01:29,860 --> 00:01:36,840
and drink by others, or by the food and drink of others.

10
00:01:36,840 --> 00:01:45,020
Such a person, whether at night, by day or by night, does not attain samadhi, focus

11
00:01:45,020 --> 00:01:59,420
or concentration, whoever has removed this, cut it off, uprooted it completely, eradicated

12
00:01:59,420 --> 00:02:17,300
and that person, indeed, by day or by night, does attain to samadhi.

13
00:02:17,300 --> 00:02:36,180
These two verses were taught in response to the story of a certain monk, Anavas, who was apparently

14
00:02:36,180 --> 00:02:45,780
very critical of any gift given to the monastics.

15
00:02:45,780 --> 00:02:51,160
He was critical of, they say, Anatapindika's gifts, Anatapindika was the great supporter

16
00:02:51,160 --> 00:03:02,300
of the Buddha, who gave so much of his wealth and supported the Buddha quite intently, and

17
00:03:02,300 --> 00:03:10,080
he would criticize Risaka, the chief female, they disciple of the Buddha, who was equally

18
00:03:10,080 --> 00:03:18,400
kind and generous and supportive of the Buddha's religion.

19
00:03:18,400 --> 00:03:22,680
When they said, it says, when they got hot food, he would complain, it was too hot,

20
00:03:22,680 --> 00:03:27,680
when they got cold food, he would say it should be hot.

21
00:03:27,680 --> 00:03:34,560
No matter what people gave to the monks, he was always complaining, but his reason for complaining

22
00:03:34,560 --> 00:03:39,960
seems to have not been because he disliked what was given.

23
00:03:39,960 --> 00:03:47,680
It seems that he disliked or he felt threatened by the giving because he would always

24
00:03:47,680 --> 00:03:51,840
brag about his own family.

25
00:03:51,840 --> 00:04:00,040
And whenever anyone supported the monks, the story goes that he would always compare it

26
00:04:00,040 --> 00:04:07,760
to his family and say, my family gives so much better than this, my relatives, my kinsmen,

27
00:04:07,760 --> 00:04:08,760
my people.

28
00:04:08,760 --> 00:04:16,640
It's a peculiar story, but bear with me, there's a point to it.

29
00:04:16,640 --> 00:04:22,880
And so the monks thought, wow, this is really interesting, I mean, if true, this is a big

30
00:04:22,880 --> 00:04:31,000
deal, this guy's family is something special for him to disdain on any other person doing

31
00:04:31,000 --> 00:04:37,480
such great deeds as giving gifts to others or being charitable, you know?

32
00:04:37,480 --> 00:04:42,320
And so they went to the village where this novice was from, and they asked around about

33
00:04:42,320 --> 00:04:48,960
some great family who gave such great gifts and who was generous and charitable in this

34
00:04:48,960 --> 00:04:54,080
city, and the wealthy people of the city said, we don't know of such a person.

35
00:04:54,080 --> 00:05:03,840
And they said, but there is this gatekeeper, guy, the guy who guards the gate of the city.

36
00:05:03,840 --> 00:05:12,280
He had a son named Tissa, who had the same name as this novice who went forth as a novice.

37
00:05:12,280 --> 00:05:18,240
So they went to the family, they found out sure enough, this was his family, it was a very

38
00:05:18,240 --> 00:05:28,760
poor family who never didn't give anything or was never of any great charity.

39
00:05:28,760 --> 00:05:33,320
And so they went to the Buddha and they brought this up to the Buddha that this samanara

40
00:05:33,320 --> 00:05:39,640
was really, really talking without any basis.

41
00:05:39,640 --> 00:05:42,320
And the Buddha taught these verses in response.

42
00:05:42,320 --> 00:05:46,960
He said, oh, this samanara, this is just the way he's been from lifetime after lifetime.

43
00:05:46,960 --> 00:05:51,240
He talked about the samanara's past lives, and then he said, it's just how he always

44
00:05:51,240 --> 00:05:53,040
has been.

45
00:05:53,040 --> 00:05:59,600
And then he pivots, and he teaches these verses so to understand the importance of this

46
00:05:59,600 --> 00:06:07,640
subject and why it's an instructive story, you have to get an idea of the world that

47
00:06:07,640 --> 00:06:10,320
Buddhist monks live in.

48
00:06:10,320 --> 00:06:19,240
We live impoverished, we don't take salaries, we're not allowed to work for a living.

49
00:06:19,240 --> 00:06:26,800
For all intents and purposes, we're without, we are to be content with nothing.

50
00:06:26,800 --> 00:06:34,720
The allowance to receive gifts is just that, an allowance as an exception to stay alive,

51
00:06:34,720 --> 00:06:46,960
not to be sought out, not to be coerced or fast over, but to be accepted as an allowance.

52
00:06:46,960 --> 00:06:54,440
So when monks wake up in the morning in order to gain food to survive, they are allowed

53
00:06:54,440 --> 00:07:01,080
to walk mindfully without any begging or asking or even hinting, without even showing

54
00:07:01,080 --> 00:07:07,040
their bull, walk through the village, and if someone is giving arms or interested in giving

55
00:07:07,040 --> 00:07:09,680
arms, then they're allowed to take that spot.

56
00:07:09,680 --> 00:07:16,920
So the idea of giving food to the monks is it's the whole of the realm of economy for

57
00:07:16,920 --> 00:07:19,600
monks, of livelihood, you might say.

58
00:07:19,600 --> 00:07:26,480
And so it's equivalent to what we might think of in a worldly and in a non-monastic setting

59
00:07:26,480 --> 00:07:31,040
of livelihood, working for a living.

60
00:07:31,040 --> 00:07:35,400
And if you understand the parallel, if you see the data as the parallel, then you can

61
00:07:35,400 --> 00:07:36,880
get an idea where this is coming from.

62
00:07:36,880 --> 00:07:40,400
This is a guy who was obsessed over his livelihood.

63
00:07:40,400 --> 00:07:44,400
So for monks, because we don't have money, because we don't have any of the trappings of

64
00:07:44,400 --> 00:07:49,360
employment or society that lay people do, this is what monks obsess about.

65
00:07:49,360 --> 00:07:51,960
They obsess about who's giving what and so on.

66
00:07:51,960 --> 00:07:54,520
And usually it's on the other side.

67
00:07:54,520 --> 00:07:57,560
And I would find it's much more about, are they giving to me?

68
00:07:57,560 --> 00:07:58,560
What are they giving to me?

69
00:07:58,560 --> 00:08:03,160
And it seems that, you know, to this monk's credit, he wasn't greedy.

70
00:08:03,160 --> 00:08:08,920
He was just conceited and deceitful, really, which is bad enough.

71
00:08:08,920 --> 00:08:11,280
But he was boasting about something that wasn't true.

72
00:08:11,280 --> 00:08:24,480
Perhaps he felt a low self esteem or somehow trying to overcompensate for his lack of wealth

73
00:08:24,480 --> 00:08:26,440
the upbringing or something.

74
00:08:26,440 --> 00:08:27,920
It's a strange story, as I said.

75
00:08:27,920 --> 00:08:37,200
But it underlines this important concept of obsession and disturbance in the mind.

76
00:08:37,200 --> 00:08:48,960
And so it works on both sides of this issue, on the one side being disturbed by the activities

77
00:08:48,960 --> 00:08:50,120
of others.

78
00:08:50,120 --> 00:08:57,200
It's a very common thing to see where we get upset when we see others doing things we

79
00:08:57,200 --> 00:09:02,560
perceive as being improper, critical of others.

80
00:09:02,560 --> 00:09:06,760
So we're critical of other people when they try to do their best.

81
00:09:06,760 --> 00:09:14,000
When we see someone, there's this term that you hear called virtue signaling.

82
00:09:14,000 --> 00:09:20,960
And people who use this term, they're critical of people who appear to be professing

83
00:09:20,960 --> 00:09:24,040
virtues, professing good things.

84
00:09:24,040 --> 00:09:25,680
And they'll say, oh, that's just virtue signaling.

85
00:09:25,680 --> 00:09:27,360
I mean, that person really doesn't mean it.

86
00:09:27,360 --> 00:09:32,520
And so it seems to be a bit of an obsession for people to point out the inadequacies of

87
00:09:32,520 --> 00:09:34,640
others.

88
00:09:34,640 --> 00:09:43,120
This mind state is a very important one where we obsess about the idea, the activities

89
00:09:43,120 --> 00:09:44,120
of others.

90
00:09:44,120 --> 00:09:48,560
We obsess about the workings of other people.

91
00:09:48,560 --> 00:09:56,960
It's an essential pitfall of one's own mental development.

92
00:09:56,960 --> 00:10:04,960
It's something that will constantly obstruct and hinder our path towards true peace of

93
00:10:04,960 --> 00:10:08,920
mind because we are externalizing things.

94
00:10:08,920 --> 00:10:17,240
We're living in a realm where the answers to our problems are without any concrete basis

95
00:10:17,240 --> 00:10:19,520
in reality because they deal with other people.

96
00:10:19,520 --> 00:10:25,960
Is this person really virtuous or are they just pretending?

97
00:10:25,960 --> 00:10:29,760
You'll never find a true answer to that because you're dealing with people.

98
00:10:29,760 --> 00:10:34,360
You're looking at, is this person a good person when people aren't the things that really

99
00:10:34,360 --> 00:10:40,800
exist because it exists, of course, who's just experienced?

100
00:10:40,800 --> 00:10:47,120
And on the other side, as I mentioned, our obsession is often about our own gains.

101
00:10:47,120 --> 00:10:54,840
So when we see this verse, without the story, includes a broader picture of the obsession

102
00:10:54,840 --> 00:11:03,200
with any topic around livelihood, for example, as I said, what you gain for yourself.

103
00:11:03,200 --> 00:11:10,720
People who are obsessed with other people's salaries is this person getting more than me.

104
00:11:10,720 --> 00:11:21,800
The obsession with class warfare or class inequality, talking about demanding equality

105
00:11:21,800 --> 00:11:27,480
in the world of taxing the rich, eating the rich is something you hear, which I'm not

106
00:11:27,480 --> 00:11:30,960
sure what exactly that means.

107
00:11:30,960 --> 00:11:38,680
And blaming rich people, which the point here is not whether it's true or not whether anyone

108
00:11:38,680 --> 00:11:41,560
is truly to blame or not.

109
00:11:41,560 --> 00:11:46,480
The point is, and this is what the verse focuses on, holds in on.

110
00:11:46,480 --> 00:11:49,600
The verse doesn't even condemn the monk.

111
00:11:49,600 --> 00:11:57,760
It, in fact, perhaps implicitly condemns the monks who came to the Buddha by saying, when

112
00:11:57,760 --> 00:12:03,320
you're concerned with other people, when you're concerned with the doings of others, you'll

113
00:12:03,320 --> 00:12:04,880
never attain concentration.

114
00:12:04,880 --> 00:12:07,360
So there's even these monks who came to the Buddha.

115
00:12:07,360 --> 00:12:11,720
Maybe they did a good thing, but it's also quite possible to understand that they were

116
00:12:11,720 --> 00:12:16,440
barking up the wrong tree as well by concerning themselves with this monk.

117
00:12:16,440 --> 00:12:22,080
So the Buddha takes this story and he pivots to point out what's really important.

118
00:12:22,080 --> 00:12:27,400
Why is it wrong to be focused on the deeds of others?

119
00:12:27,400 --> 00:12:33,760
It's not a matter of whether they're doing wrong or they're doing right.

120
00:12:33,760 --> 00:12:37,960
It's not a matter of whether you're right to do it or not, right to criticize others

121
00:12:37,960 --> 00:12:40,720
or not.

122
00:12:40,720 --> 00:12:45,280
The point and the real important lesson is that it disturbs samadhi.

123
00:12:45,280 --> 00:12:56,520
It's a very simple thing, it disturbs your mind, it interrupts the clarity and the purity of

124
00:12:56,520 --> 00:13:05,360
your mind, it interrupts your process of mental development.

125
00:13:05,360 --> 00:13:08,600
So how it relates to our practice should be fairly obvious.

126
00:13:08,600 --> 00:13:14,400
And I think one way that it relates to our practice specifically is it makes the link between

127
00:13:14,400 --> 00:13:19,360
the worldly and the spiritual or the internal and the external.

128
00:13:19,360 --> 00:13:29,240
You can't live your life focused on externalities as an example, criticizing the activities

129
00:13:29,240 --> 00:13:38,800
of others and expect to really understand and come to peace with your own experience.

130
00:13:38,800 --> 00:13:44,800
There are two different ways of looking at the world.

131
00:13:44,800 --> 00:13:52,880
So you see there's a problem in many aspects of our life, husbands and wives critical

132
00:13:52,880 --> 00:14:00,840
of each other, parents, critical of children, children, critical of parents, where critical

133
00:14:00,840 --> 00:14:14,520
of other ethnic groups or other groups in society, class war, parents, on politics, where

134
00:14:14,520 --> 00:14:26,440
this party and that party sports team, sports teams, but we live our lives in externalities

135
00:14:26,440 --> 00:14:32,480
concerned with the workings of others.

136
00:14:32,480 --> 00:14:40,800
So our practice of meditation can well be described as the abandoning the uprooting of

137
00:14:40,800 --> 00:14:50,440
this whole perspective, this whole way of looking at reality in terms of what people are

138
00:14:50,440 --> 00:15:02,600
doing and the right and wrong of people's activities, the better and the worst of people.

139
00:15:02,600 --> 00:15:08,560
Through the practice of mindfulness, we come to see the good and the bad of experiences,

140
00:15:08,560 --> 00:15:26,040
of reactions of mind state, how the real issues that we're faced with are all related

141
00:15:26,040 --> 00:15:33,320
to our experience and our reactions to our experience are our internal process of cognition,

142
00:15:33,320 --> 00:15:40,520
of seeing, hearing, smelling, tasting, feeling and thinking.

143
00:15:40,520 --> 00:15:47,160
When we focus on these, any idea of what others might be doing, the right and the wrong

144
00:15:47,160 --> 00:16:00,280
of criticism feeds away and because it's real in a way that people and actions are not,

145
00:16:00,280 --> 00:16:13,400
it creates a energy, a life, a live liveliness because of the simplicity and the purity

146
00:16:13,400 --> 00:16:25,600
and the reality, the not needing to engage in abstract and complicated thoughts of people

147
00:16:25,600 --> 00:16:37,240
being right and positions and comparisons between positions and comparisons between actions.

148
00:16:37,240 --> 00:16:47,160
The Buddha's teaching, many of the Buddha's teachings are so very simple, they're often,

149
00:16:47,160 --> 00:16:54,000
the depth of them is often overlooked, they're often mistaken as simplistic teachings.

150
00:16:54,000 --> 00:17:02,520
But the simplicity of them is a huge part of the teaching because it creates a pure and

151
00:17:02,520 --> 00:17:10,080
a clear state of mind when you have a simple perspective of seeing things just as they

152
00:17:10,080 --> 00:17:11,080
are.

153
00:17:11,080 --> 00:17:17,800
When seeing is just seeing and hearing is just hearing, even if you're in a whole other

154
00:17:17,800 --> 00:17:23,240
universe, you're in a whole other dimension.

155
00:17:23,240 --> 00:17:32,400
There's no more comparisons with others, there's no more worrying about who gave what

156
00:17:32,400 --> 00:17:39,760
or to extrapolate for those who are not monastic, who gets what, who does what in society

157
00:17:39,760 --> 00:17:43,120
and work.

158
00:17:43,120 --> 00:17:49,880
Someone told me recently about having to compete in a job with another company and the other

159
00:17:49,880 --> 00:17:57,480
company not playing fair.

160
00:17:57,480 --> 00:18:04,760
If you live in that world, you're always going to be faced with worry and uncertainty.

161
00:18:04,760 --> 00:18:13,560
You can never really be certain because of the very nature of it, because people and

162
00:18:13,560 --> 00:18:24,960
society and employment and economy and so on are all conceptual.

163
00:18:24,960 --> 00:18:27,520
They're removed from what's actually going on behind the scenes.

164
00:18:27,520 --> 00:18:31,120
You can never really predict accurately what's going to happen.

165
00:18:31,120 --> 00:18:36,480
You can never be stable or sure because it's not real.

166
00:18:36,480 --> 00:18:43,000
When you finally come to focus on reality, on experience, while you come to see that experience

167
00:18:43,000 --> 00:18:49,640
is chaotic, but dependable, it will always just be seeing, hearing, smelling, tasting,

168
00:18:49,640 --> 00:18:55,400
feeling, thinking, when you live in that way, when you live in that realm, your foundation

169
00:18:55,400 --> 00:19:03,720
is secure, your interactions will be without any surprises.

170
00:19:03,720 --> 00:19:13,480
There will always be a familiarity, a certainty, an unshakable knowledge and understanding,

171
00:19:13,480 --> 00:19:22,000
because there can never be more than that simple, momentary experiences.

172
00:19:22,000 --> 00:19:28,880
So a simple lesson, one of the teachings in the Dhampada, but it gives us an important

173
00:19:28,880 --> 00:19:38,080
reminder, like many of the teachings of not only what's the right way and the wrong

174
00:19:38,080 --> 00:19:49,440
way to behave, but why it's important, not because it's right or just, but because it

175
00:19:49,440 --> 00:19:57,840
is supportive and because it has positive benefits to your psyche, because it is psychologically

176
00:19:57,840 --> 00:20:02,120
beneficial, so that's the Dhampada for today, thank you for listening.

