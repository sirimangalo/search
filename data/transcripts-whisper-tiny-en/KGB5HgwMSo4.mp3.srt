1
00:00:00,000 --> 00:00:06,720
I'm planning to do some at the end of the morning for energy during the day, right?

2
00:00:06,720 --> 00:00:12,560
And in the evening balance with Vipasana, is that okay or should I do it always in combination

3
00:00:12,560 --> 00:00:14,600
and completely apart?

4
00:00:14,600 --> 00:00:17,200
It's interesting, no?

5
00:00:17,200 --> 00:00:21,240
12 power to you, I mean, geez, the Buddha said,

6
00:00:21,240 --> 00:00:28,440
Samat and Vipasana are the two greatest, are just the greatest benefit, the most wonderful

7
00:00:28,440 --> 00:00:29,440
thing.

8
00:00:29,440 --> 00:00:34,080
So if you're doing both of them, wow, it's like good for you.

9
00:00:34,080 --> 00:00:38,920
I can think of far worse things you can be doing.

10
00:00:38,920 --> 00:00:42,160
But there are issues with doing two different techniques.

11
00:00:42,160 --> 00:00:46,480
If you're doing one technique in the morning and one technique in the evening, then you're

12
00:00:46,480 --> 00:00:50,960
going to twice as much to train your mind in because your mind has to become accustomed

13
00:00:50,960 --> 00:00:53,000
to the practice.

14
00:00:53,000 --> 00:00:58,560
So why not do something that has both Samat and Vipasana together?

15
00:00:58,560 --> 00:01:03,320
Why not practice both of them together?

16
00:01:03,320 --> 00:01:11,200
You say, Samat brings you energy, well that's interesting, that's a good reason I suppose.

17
00:01:11,200 --> 00:01:14,520
I never found a need to practice Samat of for energy.

18
00:01:14,520 --> 00:01:20,080
I remember doing tree planting, spent months doing tree planting and here look, I was

19
00:01:20,080 --> 00:01:27,120
not much bigger than I am now, now a little bit more muscular.

20
00:01:27,120 --> 00:01:30,080
And I just did Vipasana and it was incredible for energy.

21
00:01:30,080 --> 00:01:35,640
It was amazing that I was able to keep up with some of the best planters out there because

22
00:01:35,640 --> 00:01:44,800
I just had such dedication, you know, Vipasana gives you such ability to do without

23
00:01:44,800 --> 00:01:54,040
feeling, without flagging, to push yourself, physically, without worrying, without stopping,

24
00:01:54,040 --> 00:02:05,800
without giving in, I don't know, I'm not keen on the whole Samat in the morning for

25
00:02:05,800 --> 00:02:07,600
energy thing.

26
00:02:07,600 --> 00:02:15,440
Like Samat is something that you shouldn't do in the forest or, you know, in some sort of

27
00:02:15,440 --> 00:02:23,080
seclusion because if you can't get free from the, you know, Ajahn Chai even said, Samat

28
00:02:23,080 --> 00:02:24,680
is based on craving.

29
00:02:24,680 --> 00:02:29,400
I don't know if I'd go so far but he said this.

30
00:02:29,400 --> 00:02:34,480
Or based on clinging, there's one or the other, it's either based on craving or clinging.

31
00:02:34,480 --> 00:02:38,360
If you're only practicing it for energy, well, then it might actually be detrimental because

32
00:02:38,360 --> 00:02:45,280
it'll create a sort of addiction to it almost.

33
00:02:45,280 --> 00:02:50,080
If you don't get into the Jana, it's where you're free from that.

34
00:02:50,080 --> 00:02:53,520
And you know, even if you do, there's an argument to be made for it.

35
00:02:53,520 --> 00:03:00,160
The intellectual or the abstract clinging to the Jana's, when you come out of them, then

36
00:03:00,160 --> 00:03:05,600
your craving comes back, you can actually crave for them or become addicted or even give

37
00:03:05,600 --> 00:03:07,640
rice to views.

38
00:03:07,640 --> 00:03:17,520
I would say we're passing as much more, much more, much more potential value to someone

39
00:03:17,520 --> 00:03:24,760
who has to go to work, someone who's not a monk because it relates directly to your

40
00:03:24,760 --> 00:03:25,760
experience.

41
00:03:25,760 --> 00:03:28,160
Samat is an escape from your experience.

42
00:03:28,160 --> 00:03:42,680
I think that's valid, anybody else here, foreign disappeared.

43
00:03:42,680 --> 00:03:51,920
Yeah, I started off meditating, doing some other, and then I moved over to seeing insight

44
00:03:51,920 --> 00:03:57,640
meditation later.

45
00:03:57,640 --> 00:04:02,160
I think they both had that benefit, so I mean, I don't do some after at the moment.

46
00:04:02,160 --> 00:04:08,760
It could be very good for quads paying down the mind, you know, some after giving you

47
00:04:08,760 --> 00:04:12,840
a kind of calmness which you can build on.

48
00:04:12,840 --> 00:04:13,840
Yeah.

49
00:04:13,840 --> 00:04:27,600
Well, if you were to use the abdomen in a way for samata, for your samata practice,

50
00:04:27,600 --> 00:04:33,120
that's difficult because the abdomen is impermanent, it's changing, the samata you need

51
00:04:33,120 --> 00:04:42,040
an object that is stable, that you can rely upon, the abdomen can't be relied upon.

52
00:04:42,040 --> 00:04:47,040
Have you do some samata?

53
00:04:47,040 --> 00:04:48,440
It's not really bad.

54
00:04:48,440 --> 00:04:54,080
The bigger problem I think is with having two techniques, for me it seems too much, too

55
00:04:54,080 --> 00:04:55,080
much trouble.

56
00:04:55,080 --> 00:05:00,560
See, we always want to make things more complicated than they need to be.

57
00:05:00,560 --> 00:05:05,080
So having two different meditation techniques, think of what that's doing to your poor brain

58
00:05:05,080 --> 00:05:11,600
that has to now grapple with two different modes of mental behavior, maybe that's unfair

59
00:05:11,600 --> 00:05:16,360
because it's actually maybe good for the brain to have to work out.

60
00:05:16,360 --> 00:05:24,360
But if you're working, if you have to work during the day, it just seems better to keep

61
00:05:24,360 --> 00:05:25,360
it simple.

62
00:05:25,360 --> 00:05:31,160
I don't know.

63
00:05:31,160 --> 00:05:32,160
Nothing wrong with that.

64
00:05:32,160 --> 00:05:36,880
I know there's things samata in the past.

65
00:05:36,880 --> 00:05:43,160
If it gets to your routine where you do one every morning, one you do for sure, why not?

66
00:05:43,160 --> 00:05:58,360
Sorry about that.

