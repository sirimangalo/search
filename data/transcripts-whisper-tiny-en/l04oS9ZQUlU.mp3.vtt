WEBVTT

00:00.000 --> 00:10.640
Next question, when I experience a situation where I feel angry and try to be mindful,

00:10.640 --> 00:17.360
I feel that I'm repressing it by saying angry, angry, and trying to investigate it.

00:17.360 --> 00:23.000
After I feel frustrated, am I doing something wrong in the practice, am I catching the thought

00:23.000 --> 00:35.200
too late, or is it because I'm still not advanced enough?

00:35.200 --> 00:52.240
I guess the problem here is in the try to repressing in the...

00:52.240 --> 01:14.240
No, I'm not sure if I do understand the question correct.

01:14.240 --> 01:19.240
Well, the first part, I'm feeling I'm repressing it by saying angry, angry.

01:19.240 --> 01:32.040
It's not a repressing, when you repress by saying angry, angry, and when you then try to investigate,

01:32.040 --> 01:41.440
you are not really with the present moment, when you say angry, angry, then you should do so

01:41.440 --> 01:47.640
because you experience that anger in that present moment.

01:47.640 --> 02:00.640
When you are repressing, then you should say repressing and when you are trying to investigate,

02:00.640 --> 02:09.240
you should say to yourself investigating, investigating, and you should try to very clearly separate

02:09.240 --> 02:18.240
these things, because there are different moments, and in that time when you investigate,

02:18.240 --> 02:23.880
the anger is not there anymore, and when you are repressing, the investigation is not there

02:23.880 --> 02:35.800
anymore, and when you don't separate this clearly, everything gets mixed up, and that might

02:35.800 --> 02:52.920
lead to frustration in the end, because you try to be mindful and cannot make your understanding

02:52.920 --> 03:00.400
of the present moment, maybe a little bit shorter, say, don't say I'm angry for hours,

03:00.400 --> 03:09.720
but see the anger in that second, and know that when you say angry, it can't be there

03:09.720 --> 03:15.640
anymore, it can't be the same anger anymore.

03:15.640 --> 03:26.080
There might be another moment of anger, but there are all these other things in between,

03:26.080 --> 03:40.560
so my advice is to just go a little bit deeper and be a little bit more separating.

03:40.560 --> 03:46.080
I think from what I've seen the reason why people feel like they're repressing and

03:46.080 --> 03:51.800
by acknowledging anything, especially anger, anger is a very good example, and because

03:51.800 --> 03:57.480
what anger is, anger is a painful feeling, anger is associated with a painful feeling,

03:57.480 --> 04:06.240
so anger will often create a headache or will create a stress in the body, generally in

04:06.240 --> 04:12.280
the head, but it can also create tension and suffering throughout the body, and so when

04:12.280 --> 04:17.040
you say angry, the anger disappears, and you're left with the unpleasant feeling, which

04:17.040 --> 04:24.040
is actually a result of the anger, not a result of the mindfulness.

04:24.040 --> 04:26.400
The other example is when you're thinking a lot.

04:26.400 --> 04:30.320
When a person thinks and thinks and thinks and thinks and thinks, and then finally they

04:30.320 --> 04:33.160
realize and they say thinking, thinking, then they have this big headache and so they

04:33.160 --> 04:36.720
think, oh, being mindful causes a headache, but it's so not true.

04:36.720 --> 04:42.080
All that's thinking for ten minutes, that's what caused you the headache, but when

04:42.080 --> 04:45.880
you say thinking, thinking, thinking disappears and you're left with just a headache, so

04:45.880 --> 04:53.240
you think, well, that's useless, what did that do for me, that just gave me a headache?

04:53.240 --> 04:57.080
This is in regards to repressing it, it's actually, it's certainly not true.

04:57.080 --> 05:01.840
It can happen that at the moment when you say angry, anger, you're saying it with anger or

05:01.840 --> 05:06.880
you're saying it with frustration, and so instead of being mindful and having a good quality

05:06.880 --> 05:12.640
of mind, you're trying to push it away, you're trying to get rid of it or you're trying

05:12.640 --> 05:18.720
to repress it, it can also happen that you don't have faith in the practice, so you're

05:18.720 --> 05:23.240
doing it with doubt, you're saying, is this going to work and you're saying angry, but

05:23.240 --> 05:28.240
you don't really mean it, you're not really clearly aware this is anger, you're thinking,

05:28.240 --> 05:31.920
okay, I'll just throw something at it and see if it works, because you don't really

05:31.920 --> 05:35.600
believe in the meditation practice, so this can happen where people practice without

05:35.600 --> 05:44.840
full appreciation or full intention to see the object or to this, finally it can also

05:44.840 --> 05:51.520
happen that you're practicing correctly and your mind isn't working correctly, you're

05:51.520 --> 05:57.200
doing everything right and your mind isn't cooperating and your body isn't cooperating,

05:57.200 --> 06:00.800
which means you try to be mindful, you can't be mindful, you try to catch something,

06:00.800 --> 06:06.600
you catch it too late, you try to clearly see something and it's already gone and so on

06:06.600 --> 06:12.440
and so on, and so you get frustrated, the truth is all of that is perfectly fine, you're

06:12.440 --> 06:16.880
seeing impermanence that even your own mind is impermanence, sometimes you can be mindful,

06:16.880 --> 06:21.080
sometimes you can't, you're seeing suffering, but it's not the way you want it to be and

06:21.080 --> 06:27.080
you're seeing non-self that you can't control it, this leads to this leads to frustration

06:27.080 --> 06:32.160
and that's the problem, this is reality, but reality is something we don't like, something

06:32.160 --> 06:36.280
we cannot bear with, we can't bear with impermanence, we can't bear with suffering, we can't

06:36.280 --> 06:40.880
bear with non-self, even just watching the stomach will make you frustrated, right, oh

06:40.880 --> 06:45.160
it's so wonderful, smooth and then it changes and then it's not smooth, it's rough

06:45.160 --> 06:48.560
and you think what am I doing wrong, what's wrong and then you try again and again and

06:48.560 --> 06:52.720
you become frustrated and I can't do this, this is useless, but the truth is you're doing

06:52.720 --> 06:58.160
it, the problem is not the way the practice is going, the problem is the frustration,

06:58.160 --> 07:02.920
now you have frustration in your mind and you're forgetting to continue with the frustration,

07:02.920 --> 07:07.360
so at the moment that you have frustration, you should say to yourself, frustrated, frustrated

07:07.360 --> 07:11.760
and focus on it and you'll see that just like everything else, it disappears as well.

07:11.760 --> 07:15.360
Every time you get frustrated it's a chance for you to build patience and patience is

07:15.360 --> 07:20.320
what we're trying to cultivate, patience is involved with wisdom and they're associated,

07:20.320 --> 07:24.920
true, pace and patience means just seeing things as they are and not getting upset about

07:24.920 --> 07:35.320
them, so however your meditation is, the frustration about that meditation is the real problem,

07:35.320 --> 07:39.800
it's stopping you from digging out whatever problems are going on in your meditation,

07:39.800 --> 07:43.960
stopping you from adjusting your mind, once you get frustrated about it, your object of

07:43.960 --> 07:50.680
meditation should be the frustration, get rid of that and develop patience and the ability

07:50.680 --> 07:56.720
that's polynomial you said to see the moment to moment experiences, the frustration will

07:56.720 --> 08:01.880
stop, that frustration will stop and desire will stop it, liking will stop it, all of our

08:01.880 --> 08:07.400
judgements of our practice will stop us from cultivating our practice, so people think they're

08:07.400 --> 08:12.000
stuck in the practice, they're not getting anywhere with the practice, this is just wrong

08:12.000 --> 08:15.640
view, it's wrong thought, when you think that you're not getting anywhere, I've heard

08:15.640 --> 08:20.160
people tell me that they've been stuck in meditation for five years or ten years, well

08:20.160 --> 08:23.920
someone tell me something ridiculous like that, they've been spending the past five

08:23.920 --> 08:28.560
years feeling like they're stuck and really that's the first thing to get rid of is the

08:28.560 --> 08:33.080
idea that you could possibly be stuck because there's no you and there's no stuck, there's

08:33.080 --> 08:37.920
only an experience and it's amazing that people could waste five years thinking that they're

08:37.920 --> 08:45.240
stuck, when in fact any moment you can become enlightened if you're clearly aware of the

08:45.240 --> 08:51.240
moment, all we're trying to do is cultivate that clear awareness, so frustration and any kind

08:51.240 --> 08:58.280
of judgment about your practice will stop you from doing that, so be careful of the frustration

08:58.280 --> 09:03.160
because you can become frustrated about reality, we do become frustrated about the truth,

09:03.160 --> 09:08.480
you're seeing the truth, you're seeing impermanence, you're seeing suffering, it's the frustration

09:08.480 --> 09:15.200
that's the problem, your inability to accept those very important aspects of reality, once

09:15.200 --> 09:20.440
you can accept them and you can see that that's the truth of life, this is the path to enlightenment,

09:20.440 --> 09:30.040
as among all you sudia, this is the path to purification, may I add something, it sounded

09:30.040 --> 09:43.120
a little bit that you are upset that when you say anger, anger, that the anger is kind

09:43.120 --> 09:51.840
of subdued, you call it repress it, but it's not you repress in it willingly, probably or

09:51.840 --> 10:00.400
possibly, it is happening that your anger goes away because you say angry, angry and you

10:00.400 --> 10:09.280
should be happy about it, it probably you are accustomed or have that habit and a liking

10:09.280 --> 10:20.160
for being angry and an explode kind of and feel a relief because of that, because it

10:20.160 --> 10:28.120
is nice when you are so angry and then you yell at somebody or you bang doors or something

10:28.120 --> 10:35.560
like that, that is a relief and when you say anger, anger or angry, angry, that just

10:35.560 --> 10:41.240
don't happen anymore, so that's just a bunch of suffering.

10:41.240 --> 10:49.600
And then this might feel for you frustrating because you can't get that relief that you

10:49.600 --> 11:00.120
are used to and then so if this works for you, this saying angry, angry, be very happy

11:00.120 --> 11:08.160
about it that you don't have to react on your anger anymore and in that case you should

11:08.160 --> 11:23.840
acknowledge happy, happy of course.

