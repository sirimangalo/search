1
00:00:00,000 --> 00:00:05,480
Okay, good evening.

2
00:00:05,480 --> 00:00:12,840
So Wednesday is I think I'm going to start answering questions again, which may mean shorter

3
00:00:12,840 --> 00:00:20,520
videos sometimes, we'll see, then I might be a shorter talk, but there'll be questions

4
00:00:20,520 --> 00:00:26,880
that relate to practice, so again, you don't have to come for all the talks, but certainly

5
00:00:26,880 --> 00:00:32,560
welcome to come, listen.

6
00:00:32,560 --> 00:00:41,240
So tonight I'm going over a question, a couple of older questions that I think are related

7
00:00:41,240 --> 00:01:04,680
and have a general applicability to meditators, Buddhist people, to the problems that we face

8
00:01:04,680 --> 00:01:17,920
as sentient creatures, it's the topic of problems.

9
00:01:17,920 --> 00:01:27,560
More specifically, I suppose, what's of great interest is problems with meditating, problems

10
00:01:27,560 --> 00:01:52,440
that keep us from meditating, problems that cause us to struggle when meditating.

11
00:01:52,440 --> 00:02:00,040
And so the question comes for people who are interested in meditations, some people who

12
00:02:00,040 --> 00:02:12,320
are interested in meditation, that's how to deal with extreme conditions, problems.

13
00:02:12,320 --> 00:02:26,040
Some people have such problems that they seem incapable of practicing, and we're not

14
00:02:26,040 --> 00:02:32,440
just talking about struggle, we're talking about the incapacity to practice meditation,

15
00:02:32,440 --> 00:02:40,640
it could be because of a neurotic impulse or state of mind, it could be because of a physical

16
00:02:40,640 --> 00:02:48,480
ailment, things that get in the way of our practice.

17
00:02:48,480 --> 00:02:55,560
And so it's important to note that the bad news is there are certain things that will

18
00:02:55,560 --> 00:03:02,920
get in the way of your meditation practice, that will prevent you from practicing.

19
00:03:02,920 --> 00:03:10,680
Some people may not be able to practice meditation.

20
00:03:10,680 --> 00:03:19,840
The good news is that it's not so much in terms of them not being able to solve their

21
00:03:19,840 --> 00:03:25,240
problems.

22
00:03:25,240 --> 00:03:35,040
And you don't have to actually solve your problems.

23
00:03:35,040 --> 00:03:44,360
But it has to do with an incapacity, and this will be the case for some people that are

24
00:03:44,360 --> 00:03:49,360
incapable.

25
00:03:49,360 --> 00:03:56,320
When you come here, you're taking on a task, when you begin to practice meditation on

26
00:03:56,320 --> 00:04:03,240
your own as well, you're undertaking a task to change something, to do something, to gain

27
00:04:03,240 --> 00:04:05,760
something.

28
00:04:05,760 --> 00:04:08,680
And I'm here to help you do it, I'm here to guide you through it.

29
00:04:08,680 --> 00:04:12,000
But we may be incapable, we may fail.

30
00:04:12,000 --> 00:04:18,120
Each and every one of you when you come here, we're a partnership.

31
00:04:18,120 --> 00:04:23,960
I may fail in teaching you the right things, you may fail in practicing them, together

32
00:04:23,960 --> 00:04:25,000
we may fail.

33
00:04:25,000 --> 00:04:27,280
That's the bad news.

34
00:04:27,280 --> 00:04:35,160
But the good news again, it's not because you had problems that were insurmountable, but

35
00:04:35,160 --> 00:04:42,120
that we were unable to create.

36
00:04:42,120 --> 00:04:52,440
We were unable to do the task that needed to be done, which is not solving your problems.

37
00:04:52,440 --> 00:04:58,240
And so the question here about how to deal with these problems, someone had neurotic fear,

38
00:04:58,240 --> 00:05:04,680
another person had intense chronic pain, prevented them from meditating.

39
00:05:04,680 --> 00:05:09,360
All of you have, I'm sure, problems, not that necessarily prevent you from meditating,

40
00:05:09,360 --> 00:05:12,120
but make it a struggle.

41
00:05:12,120 --> 00:05:17,120
We all, when we come to meditate, we have problems, we get in our way.

42
00:05:17,120 --> 00:05:24,560
How do we deal with these?

43
00:05:24,560 --> 00:05:28,160
So there are two principles that we have to understand.

44
00:05:28,160 --> 00:05:32,560
Again, to make clear, I didn't quite make it clear.

45
00:05:32,560 --> 00:05:35,320
The good news is that you don't have to fix your problems.

46
00:05:35,320 --> 00:05:37,720
That's the wrong way of looking at it.

47
00:05:37,720 --> 00:05:48,960
So the good news is the way to solve this or fix this is to change the way we look at

48
00:05:48,960 --> 00:05:55,760
the problem, to change our way of looking at things.

49
00:05:55,760 --> 00:05:58,960
And there's two aspects to this.

50
00:05:58,960 --> 00:06:08,720
One is in terms of what we see and what is really there.

51
00:06:08,720 --> 00:06:13,760
There's a very powerful verse that I don't think is from the Buddha, but it's in one of

52
00:06:13,760 --> 00:06:14,760
the commentaries.

53
00:06:14,760 --> 00:06:20,080
It says, it's one of these verses where the commentator of the Buddha's teaching, they're

54
00:06:20,080 --> 00:06:26,360
commenting on the Buddha's teaching and they say, they now hope we're on it.

55
00:06:26,360 --> 00:06:31,320
As said, the ancients or the old ones.

56
00:06:31,320 --> 00:06:37,760
And it's understood that men have meant probably the aras in the time of the Buddha.

57
00:06:37,760 --> 00:06:44,960
The teachers who passed on the Buddha's teaching, that's what they would say when it wasn't

58
00:06:44,960 --> 00:06:45,960
something.

59
00:06:45,960 --> 00:06:52,320
The Buddha said it was something someone said, some Buddhists don't know who.

60
00:06:52,320 --> 00:07:14,280
And this verse goes, what is seen that he does not see.

61
00:07:14,280 --> 00:07:39,480
It's a bit of a riddle actually, what he sees that is not seen and if you were to take

62
00:07:39,480 --> 00:07:46,440
it out of the Buddhist context, it might seem well like a riddle.

63
00:07:46,440 --> 00:07:48,040
But it's an important and powerful statement.

64
00:07:48,040 --> 00:07:51,720
This is on the commentary of the Sati Patanas who have to be commentary on them, stood

65
00:07:51,720 --> 00:07:52,720
on mindfulness.

66
00:07:52,720 --> 00:07:57,600
So it's an important thing to think of.

67
00:07:57,600 --> 00:08:07,720
What is seen and what we see, what we see is not what is actually seen and we have a

68
00:08:07,720 --> 00:08:20,960
problem, meaning when we see problems, when we have problems, whether we have a problem,

69
00:08:20,960 --> 00:08:29,960
because the problems are not what is actually seen, they're not what is actually there.

70
00:08:29,960 --> 00:08:33,560
So the first part is in regards to what is seen and what is actually there.

71
00:08:33,560 --> 00:08:40,880
And the second part is in regards to our relationship to it and our response and our

72
00:08:40,880 --> 00:08:48,360
getcha, our work, our task in relation to it.

73
00:08:48,360 --> 00:08:50,560
So problems are not there.

74
00:08:50,560 --> 00:08:54,000
Problems are not what is there.

75
00:08:54,000 --> 00:09:00,200
What is there is experience, I think is probably the best way to put it.

76
00:09:00,200 --> 00:09:13,880
This is moments of experience and the quality of these two very different things.

77
00:09:13,880 --> 00:09:17,040
It's like night and day.

78
00:09:17,040 --> 00:09:19,480
A problem is constant.

79
00:09:19,480 --> 00:09:22,400
It's always there.

80
00:09:22,400 --> 00:09:24,400
It's a thing, right?

81
00:09:24,400 --> 00:09:30,480
It doesn't have a moment when it arises and when it ceases.

82
00:09:30,480 --> 00:09:47,360
A problem is to be fixed, it's yours, it's an entity, it's a thing.

83
00:09:47,360 --> 00:09:53,320
It has a life or a reality of its own.

84
00:09:53,320 --> 00:09:59,320
This is on the other hand or momentary and they aren't me or mine, they don't have any

85
00:09:59,320 --> 00:10:02,320
entity.

86
00:10:02,320 --> 00:10:11,200
There's no fixing or, and there's not even any vocabulary about fixing or changing, that's

87
00:10:11,200 --> 00:10:15,240
all.

88
00:10:15,240 --> 00:10:18,000
What would it mean to fix an experience, right?

89
00:10:18,000 --> 00:10:20,760
We fix a problem, can we fix an experience?

90
00:10:20,760 --> 00:10:22,400
No, the experience is gone.

91
00:10:22,400 --> 00:10:25,240
It's gone already.

92
00:10:25,240 --> 00:10:30,400
Why would you, even, you know, what would be the thing that you would be fixing, right?

93
00:10:30,400 --> 00:10:35,680
There's no vocabulary, there's no relationship there.

94
00:10:35,680 --> 00:10:42,160
So our perception of what's there has to change.

95
00:10:42,160 --> 00:10:46,520
That's the first aspect of the goal of meditation practice.

96
00:10:46,520 --> 00:10:50,440
The second is our relationship to it.

97
00:10:50,440 --> 00:10:57,240
So when we talk about fixing problems, we have to understand that this is the way we look

98
00:10:57,240 --> 00:10:58,240
at things.

99
00:10:58,240 --> 00:11:03,560
It's bound up in seeing problems, seeing entities and things.

100
00:11:03,560 --> 00:11:07,320
And it's bound up in a process by which we relate to things.

101
00:11:07,320 --> 00:11:10,640
This is a problem, I'm going to fix it, right?

102
00:11:10,640 --> 00:11:15,120
The process is about fixing.

103
00:11:15,120 --> 00:11:20,360
So we have two parts, the problem and the fixing.

104
00:11:20,360 --> 00:11:25,040
We have the thing and the task and that's a good way of breaking it down because that's

105
00:11:25,040 --> 00:11:29,320
how the Buddha talked about reality and the four number truths he talked about or the

106
00:11:29,320 --> 00:11:34,200
four number truths, he said, we have suffering and then we have something we're going

107
00:11:34,200 --> 00:11:35,200
to do about it.

108
00:11:35,200 --> 00:11:40,040
It's a problem and the task.

109
00:11:40,040 --> 00:11:46,880
That in Buddhism, it's not about problems and fixing them.

110
00:11:46,880 --> 00:11:52,480
It's about reality and seeing it, understanding it.

111
00:11:52,480 --> 00:11:57,640
So our relationship to reality has to change, not just seeing reality.

112
00:11:57,640 --> 00:11:59,880
I mean, it happens at the same time it's not.

113
00:11:59,880 --> 00:12:05,400
You do one and then the other, but it has two qualities to it.

114
00:12:05,400 --> 00:12:14,800
Not only do we have to look at things differently, but we have to then relate to them

115
00:12:14,800 --> 00:12:20,960
differently, so instead of trying to, and it really happens at the same time again, it's

116
00:12:20,960 --> 00:12:25,000
just two qualities, two parts of the same thing.

117
00:12:25,000 --> 00:12:32,960
The trying to fix things is not the task, when we talk about the truth of suffering,

118
00:12:32,960 --> 00:12:37,320
Buddhism is about escaping suffering, no, it's not.

119
00:12:37,320 --> 00:12:43,760
The task in regards to suffering, Buddha said, is Barinaya, we need to see it completely.

120
00:12:43,760 --> 00:12:47,480
I'm sorry, understand it completely.

121
00:12:47,480 --> 00:12:48,480
No way.

122
00:12:48,480 --> 00:12:59,120
Naya just means to know, Barinaya means to understand.

123
00:12:59,120 --> 00:13:05,160
So when we talk about problems, it's not to trivialize the great pain that some people

124
00:13:05,160 --> 00:13:15,240
might feel, or their incapacity to practice as a result of it, or the neurotic behavior,

125
00:13:15,240 --> 00:13:26,720
neurotic qualities of mind, the even psychotic episodes, things that get in the way.

126
00:13:26,720 --> 00:13:31,480
But in the solution, if there is one, and if it's going to be successful, which it's

127
00:13:31,480 --> 00:13:35,720
not going to be successful in all cases, but if it is to be successful, it's not in

128
00:13:35,720 --> 00:13:39,560
terms of solving problems.

129
00:13:39,560 --> 00:13:49,560
It's in terms of seeing reality, understanding reality.

130
00:13:49,560 --> 00:13:53,760
And so when you have pain, it's not about making the pain go away.

131
00:13:53,760 --> 00:13:59,760
It's about rather than back pain as a problem.

132
00:13:59,760 --> 00:14:03,840
It's experiences of pain and being able to see the moments of pain, and instead of trying

133
00:14:03,840 --> 00:14:08,320
to make the pain go away, try and understand your reactions to it.

134
00:14:08,320 --> 00:14:13,160
Oh, yes, it's because I want it to go away, then I'm suffering.

135
00:14:13,160 --> 00:14:23,080
Maybe you're bored, sitting in meditation, and you think, well, this is Barin.

136
00:14:23,080 --> 00:14:32,200
The problem is not sitting in meditation, the problem is that you are bored of it.

137
00:14:32,200 --> 00:14:44,400
Maybe you have an addiction problem, and the problem you think is food, or it's anyway.

138
00:14:44,400 --> 00:14:56,160
The problem is not the experience of food, or sex, or music, or art, beauty, the problem

139
00:14:56,160 --> 00:15:06,360
is our relationship to it, how we deal with it, our fix, getting our fix of the thing

140
00:15:06,360 --> 00:15:12,080
we desire.

141
00:15:12,080 --> 00:15:16,160
And so this is what we mean by talking about the three characteristics.

142
00:15:16,160 --> 00:15:24,760
The three characteristics sum up the change that occurs from seeing things as having these

143
00:15:24,760 --> 00:15:34,720
qualities of being a thing, an entity, a problem, or a solution, even.

144
00:15:34,720 --> 00:15:39,240
The solution, like something that we want when I get it, that will be the, that will make

145
00:15:39,240 --> 00:15:45,400
me happy, seeing things differently.

146
00:15:45,400 --> 00:15:56,560
The qualities of problems and solutions, our stability, satisfaction, control, that's how

147
00:15:56,560 --> 00:15:57,560
we're looking at it.

148
00:15:57,560 --> 00:16:05,720
We're looking at it as a thing that we can solve and be happy, fix and be satisfied

149
00:16:05,720 --> 00:16:09,280
and do by it.

150
00:16:09,280 --> 00:16:16,480
When you change that, and you look at things differently, and you see experiences, while

151
00:16:16,480 --> 00:16:21,840
they have three very different qualities, three opposite qualities, they are impermanent,

152
00:16:21,840 --> 00:16:28,080
they are changing all the time, there's nothing there, the idea that one would be a problem

153
00:16:28,080 --> 00:16:31,960
couldn't even arise.

154
00:16:31,960 --> 00:16:35,240
They are not able to satisfy.

155
00:16:35,240 --> 00:16:41,760
There's no solution, there's no way to solve them, to fix them, to keep them, right, to

156
00:16:41,760 --> 00:16:46,680
make them satisfy you.

157
00:16:46,680 --> 00:16:52,760
And they're not you, they're not you, they're not under your control.

158
00:16:52,760 --> 00:16:57,640
You can't change that, you can't make them be other than what they are.

159
00:16:57,640 --> 00:17:03,920
They're not, you can't force reality to be different, right?

160
00:17:03,920 --> 00:17:09,080
It's just not in the vocabulary of experience that it should be something you can control

161
00:17:09,080 --> 00:17:17,440
or manipulate.

162
00:17:17,440 --> 00:17:23,640
And so this is the change that has to occur.

163
00:17:23,640 --> 00:17:30,920
We have to change from looking at problems and solutions and really me and mine and all

164
00:17:30,920 --> 00:17:39,560
of this, and entities, like things, you know, and start to see in terms of the experiences

165
00:17:39,560 --> 00:17:51,160
what's going on, behind all of that, and it's not to say that this is easier, this

166
00:17:51,160 --> 00:17:59,800
is going to occur, but it's an important paradigm shift from struggling with our problems

167
00:17:59,800 --> 00:18:12,080
and finding a way out, and a way forward, and a way to change and free ourselves from

168
00:18:12,080 --> 00:18:25,560
suffering, requires us to change our whole goal, really, our whole direction, from solving

169
00:18:25,560 --> 00:18:34,480
things and fixing things to seeing things and understanding things.

170
00:18:34,480 --> 00:18:42,960
It's kind of very foreign, I think, I'm stressing this because it's very foreign.

171
00:18:42,960 --> 00:18:48,480
When you go to a doctor and ask about pain, I think you can be hard-pressed to find

172
00:18:48,480 --> 00:18:55,240
one that would encourage you to understand the pain, but that's basically what we're

173
00:18:55,240 --> 00:18:56,240
doing.

174
00:18:56,240 --> 00:19:03,120
We are, in a sense, acting in the capacity of, well, we're doctors for ourselves, we have

175
00:19:03,120 --> 00:19:04,120
pain.

176
00:19:04,120 --> 00:19:05,120
There's a good example.

177
00:19:05,120 --> 00:19:09,240
This isn't just mental, but these are physical problems.

178
00:19:09,240 --> 00:19:13,080
When you go to the doctor, it's a problem that has a solution to take a pill, for example,

179
00:19:13,080 --> 00:19:16,480
or exercise, or however.

180
00:19:16,480 --> 00:19:22,280
When you come here, it's an experience that has to be understood.

181
00:19:22,280 --> 00:19:28,960
When you understand it, understand your pain, that will solve your problems, and it won't

182
00:19:28,960 --> 00:19:29,960
solve your problems.

183
00:19:29,960 --> 00:19:32,040
It will change the way you look at things.

184
00:19:32,040 --> 00:19:40,200
It will change your reality to trying to solve, from trying to solve problems, to trying

185
00:19:40,200 --> 00:19:46,800
to understand and wake up, because solving problems doesn't, this is the point doesn't

186
00:19:46,800 --> 00:19:48,680
free you from suffering.

187
00:19:48,680 --> 00:20:00,280
It has to do with the quality of mind, where this isn't a philosophical topic, issue problem.

188
00:20:00,280 --> 00:20:03,080
It's a practical one.

189
00:20:03,080 --> 00:20:13,040
When you engage in the creation of problems and the fixing of problems, that quality of

190
00:20:13,040 --> 00:20:22,720
mind hits an impact, it is incapable of becoming free from suffering, why?

191
00:20:22,720 --> 00:20:25,360
Because a problem is an abstract concept.

192
00:20:25,360 --> 00:20:37,120
When you say, I have chronic pain, for example, your mind is not in tune with reality.

193
00:20:37,120 --> 00:20:42,960
He doesn't have these problems or solution.

194
00:20:42,960 --> 00:20:47,640
Reality has experiences, and so you're out of that, when you're thinking in terms of problems

195
00:20:47,640 --> 00:20:48,840
and solving them.

196
00:20:48,840 --> 00:20:59,040
You're out of touch with the mechanism, the mechanics of reality.

197
00:20:59,040 --> 00:21:06,400
When you instead engage in, this is real, this is this, that is that understanding,

198
00:21:06,400 --> 00:21:10,920
this means understanding it intellectually, it means really just seeing it, oh it's like this,

199
00:21:10,920 --> 00:21:17,400
oh yes, this is how the mind is working, this is what's going on in the mind, this is

200
00:21:17,400 --> 00:21:22,880
what's going on, this is what's really here.

201
00:21:22,880 --> 00:21:28,600
Then you're in touch with reality, and that's really all it takes, suffering comes because

202
00:21:28,600 --> 00:21:34,640
we're ignorant, this is the essence of Buddhism, this is why we could talk about the foreign

203
00:21:34,640 --> 00:21:38,640
humble truths, what are the foreign humble truths, they're wisdom, they're things we have

204
00:21:38,640 --> 00:21:49,440
to see, truth, we don't know the truth, we can't handle the truth sometimes.

205
00:21:49,440 --> 00:21:58,120
So here's the question for you, can you handle the truth, can you, well come to see

206
00:21:58,120 --> 00:22:04,120
the truth, and truth of course is a being word that becomes a buzzword, really it's

207
00:22:04,120 --> 00:22:09,880
not, this isn't some religious truth or doctrine, it's just the very simple truth, right,

208
00:22:09,880 --> 00:22:17,840
the simple, what is really true, it's not true that we have problems, that's an abstract

209
00:22:17,840 --> 00:22:23,080
way of looking at things, you say, I have a pain problem, I have a problem with this phobia

210
00:22:23,080 --> 00:22:28,680
or whatever, no you don't have a problem, there are experiences and they arise in

211
00:22:28,680 --> 00:22:34,920
things, it's much more accurate and because it's much more accurate it's much more powerful

212
00:22:34,920 --> 00:22:43,660
in terms of helping you to, for yourself from suffering, so in regards to, I think it's

213
00:22:43,660 --> 00:22:50,080
in regards to a lot of questions that come in about having this problem or that problem,

214
00:22:50,080 --> 00:22:58,520
I think it's a good way, a good part of the solution though again, the solution is to stop

215
00:22:58,520 --> 00:23:06,480
looking for solutions, stop looking in terms of problems and solutions, so that's the

216
00:23:06,480 --> 00:23:36,320
demo for tonight, thank you all for listening.

