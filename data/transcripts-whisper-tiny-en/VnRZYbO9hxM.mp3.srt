1
00:00:00,000 --> 00:00:28,380
Okay, good morning.

2
00:00:28,380 --> 00:00:41,880
This morning is a goal.

3
00:00:41,880 --> 00:00:58,040
In our life, we establish goals for ourselves to be part of our quest.

4
00:00:58,040 --> 00:01:09,840
To establish order and reason and purpose in our lives.

5
00:01:09,840 --> 00:01:23,280
When we do things, we relate them to our goals or we establish goals in relation to our activities.

6
00:01:23,280 --> 00:01:25,840
Why are we doing it?

7
00:01:25,840 --> 00:01:34,780
There must be a reason.

8
00:01:34,780 --> 00:01:41,140
Meditation is no different.

9
00:01:41,140 --> 00:01:52,020
We come to the practice of meditation for our reason.

10
00:01:52,020 --> 00:02:05,520
We have goals.

11
00:02:05,520 --> 00:02:18,680
It's true that the practice of meditation doesn't work well when we're focused on goals

12
00:02:18,680 --> 00:02:30,040
on results, that's not to say that there aren't goals of the meditation practice or reasons

13
00:02:30,040 --> 00:02:31,040
why we practice.

14
00:02:31,040 --> 00:02:40,800
Of course, it's just important to understand that as a practice, focusing on your goals

15
00:02:40,800 --> 00:02:51,460
is not incredibly helpful.

16
00:02:51,460 --> 00:02:57,400
If you don't have a sense of goals or reasons for doing something, it's hard to gain

17
00:02:57,400 --> 00:03:07,680
that the interest and appreciation and inclination towards what you're doing.

18
00:03:07,680 --> 00:03:14,440
There's that and that's a reason for talking about goals and reasons.

19
00:03:14,440 --> 00:03:21,040
If you're focused on the goals while you're practicing, it's very hard for you to be present.

20
00:03:21,040 --> 00:03:28,200
Unlikely, you're going to be focused on the actual quality of your practice, the quality

21
00:03:28,200 --> 00:03:36,960
of the mind, rather than having a clear mind that is present and engaged with reality,

22
00:03:36,960 --> 00:03:44,640
you're going to be wondering about the future, worried and doubting about whether there's

23
00:03:44,640 --> 00:03:51,360
some results going to come, a lot of worry and doubt and meditation can be attributed to

24
00:03:51,360 --> 00:04:04,960
goal seeking or result or end it's not the truth is, you can get results based on

25
00:04:04,960 --> 00:04:10,760
work that you put in and so if your mind is not focused on the present, if you're thinking

26
00:04:10,760 --> 00:04:16,320
about what results you're going to get, you'll never get any results because you're not

27
00:04:16,320 --> 00:04:19,000
doing any work.

28
00:04:19,000 --> 00:04:26,040
On the other hand, if you forget all about the results and focus on the work, at least

29
00:04:26,040 --> 00:04:37,040
while you're doing it, of course results will come and he doubt is just unreasonable.

30
00:04:37,040 --> 00:04:49,800
I always come from anything you do, you just have to figure out the right thing to do.

31
00:04:49,800 --> 00:05:04,920
Nonetheless, it gives us encouragement to know the results and to have goals and that can

32
00:05:04,920 --> 00:05:08,840
support our inclination to practice, that's it.

33
00:05:08,840 --> 00:05:09,840
So what are the goals?

34
00:05:09,840 --> 00:05:12,680
What are the reasons why we practice Satipatana?

35
00:05:12,680 --> 00:05:24,960
I'm going to give us five reasons, five goals.

36
00:05:24,960 --> 00:05:41,520
The first is for purification, we practice meditation for the purification of the mind.

37
00:05:41,520 --> 00:05:45,720
We talk about purification and Buddhism or purity.

38
00:05:45,720 --> 00:05:54,920
We're always talking about the mind, there's no way to purify the body.

39
00:05:54,920 --> 00:06:03,080
purification of the body really only means abstention from impure thoughts based on impure

40
00:06:03,080 --> 00:06:19,000
mind, mind-states or so actions based on impure mind-states and speech based on impure

41
00:06:19,000 --> 00:06:26,160
mind-states.

42
00:06:26,160 --> 00:06:35,440
But the purification of the mind, purity of mind, this is a goal.

43
00:06:35,440 --> 00:06:44,840
We take it as our goal to straighten our minds out.

44
00:06:44,840 --> 00:06:51,960
How many impure states of mind and by impure means states of mind that lead to suffering,

45
00:06:51,960 --> 00:07:02,960
that are conflicted, that are weakening, harmful to oneself, harmful to others, just

46
00:07:02,960 --> 00:07:22,360
unbeneficial, useless, harmful, greed, anger, delusion, everything they bring conceived, vanity,

47
00:07:22,360 --> 00:07:36,760
jealousy, stubbornness, arrogance, hatred, meanness, cruelty, all of these states of

48
00:07:36,760 --> 00:07:53,880
mind, addiction, fear, worry, doubt, lots, lots and lots, all of these are considered impurity

49
00:07:53,880 --> 00:08:05,040
if states of mind, really this is the most important one, this is the work, if you focus

50
00:08:05,040 --> 00:08:14,880
on this then you don't have to worry about results, if you identify all the problems

51
00:08:14,880 --> 00:08:22,200
and in the practice of Satyipatana you come to identify them as problems, in the sense

52
00:08:22,200 --> 00:08:27,520
of just seeing how problematic they are, that's it, that's all you need to do, you don't

53
00:08:27,520 --> 00:08:39,320
actually have to fix them, seeing them is enough, seeing them so clearly that they weaken

54
00:08:39,320 --> 00:08:49,720
and disappear by themselves, why because of the disintegration to cultivate them, of course

55
00:08:49,720 --> 00:08:59,160
if you see something as unbeneficial, as harmful, if you see it clearly you won't cultivate

56
00:08:59,160 --> 00:09:06,080
it, we don't realize that that's all it takes, that these things that we rail against

57
00:09:06,080 --> 00:09:21,040
and worry about and are upset by, are dependent on our ignorance to thrive, that we just

58
00:09:21,040 --> 00:09:29,320
don't understand them, that if we just spent some time looking at them, there's no way

59
00:09:29,320 --> 00:09:37,360
we would incline towards them, instead we try to sometimes stop ourselves or prevent ourselves

60
00:09:37,360 --> 00:09:46,320
to repress our emotions and so on, which is really a futile exercise and leads to more

61
00:09:46,320 --> 00:09:54,520
stress and disappointment, mindfulness allows us a way to look at and be with our emotions

62
00:09:54,520 --> 00:10:01,920
without being overwhelmed by them, so the purification of mine, this is a great goal, the

63
00:10:01,920 --> 00:10:07,240
second goal and they're all related, they're all really one and the same in the end,

64
00:10:07,240 --> 00:10:16,680
but the second aspect of it is for overcoming mental illness, mental illness is the way

65
00:10:16,680 --> 00:10:25,120
we describe it in the West, it's really any kind of mental upset or turmoil, because that's

66
00:10:25,120 --> 00:10:35,080
what the impurities in the mind bring, they bring turmoil, they form into habits and those

67
00:10:35,080 --> 00:10:43,360
habits overwhelm us and keep us from being happy, so all kinds of depression and anxiety

68
00:10:43,360 --> 00:10:56,320
and addiction, obsession, they're all just habits, habits based on ignorance, that are rooted

69
00:10:56,320 --> 00:11:09,080
in impurities of mind, states of mind that are causing us harm, so it's not to trivialise

70
00:11:09,080 --> 00:11:16,720
mental illness, what we call mental illness in modern society is generally acute versions

71
00:11:16,720 --> 00:11:23,040
of all of these things and for some people it's related to the way their brain works

72
00:11:23,040 --> 00:11:32,240
because of the brain's weakness in providing pleasurable stimuli and therefore leading

73
00:11:32,240 --> 00:11:43,280
to addiction, leading to aversion, leading to depression, so in Buddhism we don't focus

74
00:11:43,280 --> 00:11:50,280
on happiness, happy mind states because you have to recognise not everyone can experience

75
00:11:50,280 --> 00:11:58,520
the same sorts of happiness, if we praised happy states, some people would always feel disappointed

76
00:11:58,520 --> 00:12:07,080
and often do and meditation feel disappointed because they can't feel happy or even calm,

77
00:12:07,080 --> 00:12:15,400
we can't guarantee that not all the time, not immediately of course, but that's not the

78
00:12:15,400 --> 00:12:24,280
goal, the goal is to freer ourselves from the illness part, freer ourselves from the bad

79
00:12:24,280 --> 00:12:32,280
habits and the states that are causing us suffering, whatever else comes, be it peace or

80
00:12:32,280 --> 00:12:43,640
happiness or joy is not predictable, what's predictable is that you'll be free from suffering

81
00:12:43,640 --> 00:12:54,080
that you'll be at peace, at least we can predict, but you have to face,

82
00:12:54,080 --> 00:13:03,000
mindfulness is about facing these mental illnesses and bad habits, learning to react to them

83
00:13:03,000 --> 00:13:17,920
and interact with them differently without judgment or without encouraging them.

84
00:13:17,920 --> 00:13:26,960
The third goal is to freer ourselves from physical and mental suffering, which of course

85
00:13:26,960 --> 00:13:36,280
comes part and parcel with all of the mental illness and impurity.

86
00:13:36,280 --> 00:13:41,040
There's physical suffering and there's mental suffering, mental suffering is bound up in

87
00:13:41,040 --> 00:13:49,320
defilements and the mental turmoil.

88
00:13:49,320 --> 00:13:59,320
The physical suffering, physical suffering, we sometimes become discouraged by when we're

89
00:13:59,320 --> 00:14:12,840
not able to experience peace and pleasure, we're not able to enjoy the meditation

90
00:14:12,840 --> 00:14:24,680
so often we spend an inordinate amount of energy trying to be happy, trying to have a

91
00:14:24,680 --> 00:14:33,800
pleasurable and peaceful and enjoyable meditation session, but this doesn't emulate

92
00:14:33,800 --> 00:14:40,600
life, life is unpredictable and often unpleasant, and so if we really want to learn and

93
00:14:40,600 --> 00:14:47,000
understand, really the point is we can't escape physical suffering, we have to be able to

94
00:14:47,000 --> 00:14:59,720
separate mental and physical suffering out and so while during meditation we can

95
00:14:59,720 --> 00:15:07,080
see progress towards freedom from mental suffering, which is all the reactions to our

96
00:15:07,080 --> 00:15:13,960
experiences and disliking the upset and so on, and the physical suffering, which we can't

97
00:15:13,960 --> 00:15:20,280
really do much about, so when we talk about freeing ourselves from suffering both

98
00:15:20,280 --> 00:15:28,960
physical and mental, the first stage is the mental, the first stage is to not suffer

99
00:15:28,960 --> 00:15:38,000
from the physical suffering, and that's the challenge, but like everything, physical

100
00:15:38,000 --> 00:15:47,240
suffering is an experience, and rather than react to it or judge it or suffer from it,

101
00:15:47,240 --> 00:16:01,400
we can come to see it just as it is, and if we can come to see it just as pain, we've

102
00:16:01,400 --> 00:16:14,200
come to see it clearly, this is what we've again, we pass on that and come to see clearly.

103
00:16:14,200 --> 00:16:27,680
The fourth goal is to find the right path, so a path is another, it's an aspect of a goal,

104
00:16:27,680 --> 00:16:39,400
but it's like a goal, but a little bit more general, how are we going to live our lives,

105
00:16:39,400 --> 00:16:52,400
what path should we be on, related to goals, but also related to our identity, it's a

106
00:16:52,400 --> 00:17:05,160
big part of establishing a framework for our reality, what path am I on, what is my path?

107
00:17:05,160 --> 00:17:13,120
It often relates to views, and it often relates to goals, where am I going, if I'm going

108
00:17:13,120 --> 00:17:24,200
there, I have to get on this path, relating to views is, I believe this, so I'm doing

109
00:17:24,200 --> 00:17:38,880
this, I live my life by this or that, in Buddhism the path is a goal in itself, I think

110
00:17:38,880 --> 00:17:48,000
that's an important point, it's not so much about trying to get somewhere, as it is about

111
00:17:48,000 --> 00:18:08,000
a manner of travel, where very much actions are active, events, where much less static

112
00:18:08,000 --> 00:18:21,880
entities than we are dynamic, changing events, and so our way of life is a path, it's not

113
00:18:21,880 --> 00:18:32,760
static, we are always going somewhere, and so Buddhism focuses much more on the quality

114
00:18:32,760 --> 00:18:44,960
of our interactions with Samsara, than it does on which interactions we choose or how we

115
00:18:44,960 --> 00:18:55,840
decide what to do in our lives and on, direction comes from the clarity and the quality

116
00:18:55,840 --> 00:19:08,560
of the mind, we read about the Eightfold Noble Path, which is really the ultimate Buddhist

117
00:19:08,560 --> 00:19:17,800
guide to life, and this is what we aim for, ultimately the Eightfold Noble Path is just

118
00:19:17,800 --> 00:19:23,920
the final moment where we are perfect, where our minds are in a perfect state that allows

119
00:19:23,920 --> 00:19:37,200
us to see things perfectly and ignite this spark that frees us from suffering.

120
00:19:37,200 --> 00:19:46,560
The final goal is this freedom from suffering, this spark, so we talked already about freedom

121
00:19:46,560 --> 00:19:54,520
from suffering, but it's more of a general sense of relating to suffering.

122
00:19:54,520 --> 00:20:02,520
Nibana, we define as freedom from suffering, but Nibana or Nubana, it's something a little

123
00:20:02,520 --> 00:20:14,880
more than that, it's the cessation, it's an experience of cessation without any reference

124
00:20:14,880 --> 00:20:27,280
to experience whatsoever, it's an experience that's indescribable, it's not at all, it's

125
00:20:27,280 --> 00:20:34,520
nothing like an arisen experience of seeing, hearing, smelling, tasting, feeling, or even

126
00:20:34,520 --> 00:20:44,840
thinking, even cognizing, even remembering, it's not even something you can remember.

127
00:20:44,840 --> 00:20:56,080
It's hard to understand, but a person experiences and understands that it's freedom,

128
00:20:56,080 --> 00:21:04,480
they understand because they have the experience of it, not as a memory, not as a perception,

129
00:21:04,480 --> 00:21:16,480
not as an idea, but as a part of their reality.

130
00:21:16,480 --> 00:21:23,040
Nibana is something that changes a person, changes their perspective, it opens up a new

131
00:21:23,040 --> 00:21:39,280
door, an alternative to some sara, an alternative to suffering, and it's the true goal

132
00:21:39,280 --> 00:22:06,120
because it has a stable, a secure result, secure and stable state of mind that is a result

133
00:22:06,120 --> 00:22:25,840
of its experience, so many goals, ultimately all relating, as you can see, to suffering,

134
00:22:25,840 --> 00:22:30,880
the cause of suffering, the cessation of suffering, and the path leading to the cessation

135
00:22:30,880 --> 00:22:43,760
of suffering, it's a good indication of where the focus of Buddhism is, and how Buddhism

136
00:22:43,760 --> 00:22:53,080
focuses on the four noble truths, so reasons for practicing goals to keep in mind that's

137
00:22:53,080 --> 00:23:09,480
the demo for this morning, thank you all for listening.

