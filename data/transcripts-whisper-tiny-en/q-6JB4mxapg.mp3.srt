1
00:00:00,000 --> 00:00:25,200
Good evening everyone, good vibe, March 22nd.

2
00:00:25,200 --> 00:00:36,280
The nays quote is from the jot again, and with the jot again of course it's best to

3
00:00:36,280 --> 00:00:43,840
go with the call of translations, because they rhyme.

4
00:00:43,840 --> 00:00:50,040
But to get a sense of why it's important that they should rhyme, and it's like at the

5
00:00:50,040 --> 00:01:00,320
Poly first, because it's, well I mean important, it's actually not all that important,

6
00:01:00,320 --> 00:01:08,520
but you know there's some rhythm to it, and you could argue that that's a good thing because

7
00:01:08,520 --> 00:01:14,360
it makes it easy to remember, it makes it memorable, it has a certain power to it,

8
00:01:14,360 --> 00:01:25,240
to create confidence and appreciation, but on the other side it's possible to become attached

9
00:01:25,240 --> 00:01:34,040
to it, so which we should be careful, anyway, verses are quite powerful, so here they

10
00:01:34,040 --> 00:01:47,680
are.

11
00:01:47,680 --> 00:01:53,320
It's the first verse.

12
00:01:53,320 --> 00:02:08,160
Easy to understand are the cries of the jackal or the bird, but the cry of a human

13
00:02:08,160 --> 00:02:16,000
of a king is far harder to understand than those. That's my translation, but here's the

14
00:02:16,000 --> 00:02:25,440
cowl translation. The cry of jackals or a bird is understood with ease, yay, but the word

15
00:02:25,440 --> 00:02:35,080
of meno king is darker, far than these. So it's easy to understand the cry of an animal,

16
00:02:35,080 --> 00:02:40,600
most when they talk, they're pretty easy to understand. Do they like you? Do they dislike

17
00:02:40,600 --> 00:02:50,120
you? In not long time, you can get a sense of the emotion behind their cries for the most part

18
00:02:50,120 --> 00:02:59,160
easy to understand. The only animal that probably comes close that I can think of to humans is,

19
00:02:59,160 --> 00:03:11,240
of course, the monkey kind, who tend to be able to trick each other and humans as well.

20
00:03:11,240 --> 00:03:18,520
We are lived with monkeys. They can be pretty sneaky, but humans take the cake, you know.

21
00:03:18,520 --> 00:03:26,680
We can say one thing, do another thing, and think an entire different thing. We totally

22
00:03:26,680 --> 00:03:34,600
deceptive. In fact, for the most part, we spend much of our days being deceptive saying things

23
00:03:34,600 --> 00:03:45,800
that cover up rather than elucidate our feelings. We're very good as human beings to say things

24
00:03:45,800 --> 00:03:57,000
like someone asks, how are you, and we feel terrible is your own right? We're often going to be on

25
00:03:57,000 --> 00:04:03,960
that because we want to actually hide how we feel, how do you like my dress? Oh, it's beautiful.

26
00:04:06,760 --> 00:04:13,720
Do I look fat in this? No, that kind of thing. These little white lines we talk about.

27
00:04:13,720 --> 00:04:25,160
Remember that's going too far. I wasn't thinking of lies. When you're angry at someone but you

28
00:04:25,160 --> 00:04:34,040
want to be polite and so you hide it, you smile. Humans are very good at this. We're very good at

29
00:04:34,040 --> 00:04:40,200
hiding how we feel. When you're at school, it's quite incredible when you actually sit down and

30
00:04:40,200 --> 00:04:46,280
talk to some of the students, most of them have, well, many of them are in a lot of stress and

31
00:04:46,280 --> 00:04:49,960
they hide it and they smile and they choke and they pretend that they're happy because

32
00:04:49,960 --> 00:04:55,400
well, everyone else seems like they're happy, so if you'll guilty that they're not happy and

33
00:04:55,400 --> 00:04:58,840
so they pretend, a lot of pretending going on.

34
00:04:58,840 --> 00:05:12,040
Today I taught meditation. Today was a very busy day. I had a test in Latin to do very well.

35
00:05:16,040 --> 00:05:23,400
And then spent the rest of it most of the day sitting, not too many people, but a handful

36
00:05:23,400 --> 00:05:32,360
of people today came by and then in the afternoon I taught a group of meditators who, by special

37
00:05:32,360 --> 00:05:40,040
invitation invited me to give them a talk on meditation, de-stressor they called it, de-stressor

38
00:05:40,040 --> 00:05:47,880
meditation. So I had a half an hour with them and then I went back downstairs and sat at the

39
00:05:47,880 --> 00:05:57,560
five-minute meditation table. Then this afternoon I went to a meeting of the McMaster Indigenous

40
00:05:59,000 --> 00:06:04,280
student community alliance, no, association alliance, maybe.

41
00:06:06,920 --> 00:06:11,400
And it was a neat thing. First Nations people are the Indigenous people.

42
00:06:11,400 --> 00:06:17,320
They have, well, I mean more than that, and why it's appropriate for this talk is

43
00:06:19,720 --> 00:06:28,920
they were really crapped on by the Canadian people and continue to be.

44
00:06:28,920 --> 00:06:46,200
And it's not well publicized how poorly they've been treated. And I think how it ties into this

45
00:06:46,200 --> 00:06:58,120
is how messed up we are really. We do whatever we can to manipulate each other.

46
00:06:58,120 --> 00:07:03,400
I'm not talking actually individually, but more on a global level. This world is pretty messed up.

47
00:07:06,120 --> 00:07:11,000
I think trying to fix the situation is probably futile.

48
00:07:13,880 --> 00:07:20,520
And the worst is trying to change like political systems or trying to

49
00:07:20,520 --> 00:07:32,440
protest. I think if it's going to change, if there's some hope for humanity still,

50
00:07:33,400 --> 00:07:44,440
it's got to be through appeal to goodness. Trying to convince people of the truth that

51
00:07:45,160 --> 00:07:50,200
manipulating each other and hurting each other is not, doesn't lead to happiness, doesn't lead to

52
00:07:50,200 --> 00:08:00,200
peace, doesn't lead to anything good. To start, I guess, being more strained. So the part of this

53
00:08:00,200 --> 00:08:05,880
is how tricky humans can be. That's really what this quote is about. It has some curious twists

54
00:08:05,880 --> 00:08:11,800
and turns in it. It says a few things. So the first thing it says is that it's hard to know,

55
00:08:11,800 --> 00:08:15,560
that's really the point that's being made in this jot. The king, this is the backstory,

56
00:08:15,560 --> 00:08:24,440
is this king asks, a goose. What is that? There is a goose, an animal who can talk. And so they're

57
00:08:24,440 --> 00:08:31,240
talking. And he says to the goose, you know, stay with me. Live here with me. You're a king of the

58
00:08:31,240 --> 00:08:38,680
goose. A geese. I'm a king of humans. I can live together. He says, no, no, no. You'll get drunk. He

59
00:08:38,680 --> 00:08:45,720
says, probably you'll get drunk one day. And decide, you want to have roast duck, roast goose.

60
00:08:46,600 --> 00:08:51,240
And that would be it for me. And he says, then I'll stop. He says, no, no, I'll stop drinking. I

61
00:08:51,240 --> 00:08:56,920
would drink alcohol while you're here. Then he launches into this. And he says, oh, it's hard to

62
00:08:56,920 --> 00:09:06,360
know the words of men. If you were a jackal or a bird, then I'd be able to understand what you're

63
00:09:06,360 --> 00:09:15,080
saying. But Manusa was sittang Raj and Dubijan at Toronto. It's hard to understand, especially for a

64
00:09:15,080 --> 00:09:23,320
goose, I guess. Somehow he can understand the human. But he still doesn't trust him. And then he

65
00:09:23,320 --> 00:09:45,960
says, when they're versus a heart. See, okay, we think when may a man may think, yeah,

66
00:09:45,960 --> 00:09:57,080
this is a relative, a friend or a companion. But you will obey Sumino. This is a good quote.

67
00:09:57,080 --> 00:10:09,000
You will obey Sumino who put a pachas, a pachate, so when the past was kind-hearted or dear or

68
00:10:09,000 --> 00:10:21,480
well-disposed afterwards becomes an enemy. See what the English is. A man may think, this is my friend,

69
00:10:21,480 --> 00:10:28,200
my comrade or my kin, this is my comrade of my kin. But friendship goes and often hate and

70
00:10:28,200 --> 00:10:38,280
met he begin. Just read the English, who has your heart is near to you with you or error he be. But

71
00:10:38,280 --> 00:10:47,400
who dwells with you and your heart is strange of far as he. Who in your house of kindly heart shall

72
00:10:47,400 --> 00:10:55,000
be is kindly still the far across the sea. Who in your house shall hostile be of heart. Hostile

73
00:10:55,000 --> 00:11:05,000
he is the ocean wide apart. Thy foes, O Lord of chariots, so near the RFR, but foster of thy realm,

74
00:11:05,000 --> 00:11:13,000
the good and heart, close-link it are. Who stays too long, find oftentimes that friend is changed to

75
00:11:13,000 --> 00:11:20,760
foe. Then air I lose your friendship, I will take my leave and go. So he's making an argument

76
00:11:20,760 --> 00:11:30,360
for why he should leave because no friend and foe, well, human and goose, you know how the story ends,

77
00:11:31,880 --> 00:11:34,840
and often ends in dead goose.

78
00:11:34,840 --> 00:11:50,680
But that's the way of humans is we're hard to understand. So how does this relate to our

79
00:11:50,680 --> 00:11:58,200
meditation? How can I relate this to our meditator? We have to be careful in our minds,

80
00:11:58,200 --> 00:12:06,520
so you can fool others, but you can't fool yourself. Like you can, you can try and you can

81
00:12:07,320 --> 00:12:14,120
work at it. But the result isn't satisfied. I mean, so by trick others, you can trick others

82
00:12:15,080 --> 00:12:21,160
into doing what you want, but it doesn't work with the mind, but you don't get positive results

83
00:12:21,160 --> 00:12:29,560
by fooling yourself. You only get more and more messed up. So the inside, whether we tell the truth

84
00:12:29,560 --> 00:12:34,680
to others, you have to tell the truth to ourselves, it's very important. That's the essential

85
00:12:34,680 --> 00:12:52,440
invitation. So we have to stop pretending and often it comes down to denying our experiences.

86
00:12:53,160 --> 00:12:58,120
No, thinking, no, no, no, I can't do that, I can't be that, this is not right, this is not right.

87
00:12:58,120 --> 00:13:05,080
Instead of actually looking at the experience, instead of being honest with ourselves saying

88
00:13:05,080 --> 00:13:16,920
it's there, you know, it's under my control, and looking at it, honestly, giving it an honest

89
00:13:16,920 --> 00:13:31,160
and the analysis. We look at our emotions, look at our thoughts, and being honest with them,

90
00:13:31,160 --> 00:13:36,760
not trying to suppress them or say, no, no, no, it's not right. It doesn't work. You have to give

91
00:13:36,760 --> 00:13:45,800
them an honest appraisal, the good ones and the bad ones. It's the only way you can learn,

92
00:13:45,800 --> 00:13:53,240
it's the only way you can learn what's right and wrong. We have to be honest with ourselves.

93
00:13:58,040 --> 00:14:03,240
Should be honest with each other as well. It shouldn't be open and it would be great if we could

94
00:14:03,240 --> 00:14:09,560
all be straight and honest. I mean, sometimes it's not right to be open and honest. Sometimes you

95
00:14:09,560 --> 00:14:16,440
have to be a little bit deceptive. Should never lie, but there are times where you should be careful

96
00:14:16,440 --> 00:14:23,080
with the truth, because if you're too straight forward, people get the wrong idea, because we all

97
00:14:23,080 --> 00:14:32,120
have, you know, defilements. If you're too honest, you can think people are angry or so. Sometimes

98
00:14:32,120 --> 00:14:43,480
you have to use subterfuge. Not lying. Lying is problem. But other people sometimes you have to

99
00:14:43,480 --> 00:14:50,760
be human. I mean, it's actually, it can be useful thing at times, too. If you don't want to scare

100
00:14:50,760 --> 00:15:06,680
someone, you want to give them confidence. Or if you want someone to not dwell on their faults,

101
00:15:07,720 --> 00:15:13,560
you can encourage them and it can save. But with yourself, it doesn't work like that.

102
00:15:13,560 --> 00:15:24,920
With yourself, the best way is honesty. Be honest with yourself. That's what insight is. It's about

103
00:15:26,200 --> 00:15:34,360
looking honest and saying, this is the truth. The truth is this. I have good things and bad things,

104
00:15:34,360 --> 00:15:39,720
and good things seeing them. It's good for seeing them, for what they are. The bad things also

105
00:15:39,720 --> 00:15:47,000
seeing them, for what they are. Because the clearer you see things, the better it gets. Bad things

106
00:15:47,000 --> 00:15:52,680
don't get, don't seem good when you look at them. The closer you look at them, the more clearly

107
00:15:52,680 --> 00:15:58,440
they're there. The problem with them is, more clearly you can see they're in their problem,

108
00:15:58,440 --> 00:16:12,920
they're evil. How they hurt you, how they stress you. It's been a long day today, so

109
00:16:15,160 --> 00:16:20,440
I don't know. I think there's some interesting numbers, so I'm going to stop there.

110
00:16:20,440 --> 00:16:41,160
You know, open up the hangout, if anybody wants to come on and talk. Otherwise, so call it a night.

111
00:16:50,440 --> 00:17:12,440
Pretty busy, this time of year.

112
00:17:12,440 --> 00:17:21,800
I've already said I'm going to New York, so that's happening.

113
00:17:25,080 --> 00:17:33,960
Not sure about going to UK. They wanted me to go for a long time, I think, but

114
00:17:33,960 --> 00:17:41,560
it's not going to work. There's the idea to go to Thailand and Sri Lanka, that might

115
00:17:41,560 --> 00:17:51,240
be happening. But that's the thing, is how long can I stay away? What's the minimum amount of time

116
00:17:52,520 --> 00:17:57,480
to stay in Thailand and Sri Lanka as well? It gets to be a long trip.

117
00:17:57,480 --> 00:18:06,680
So I'm only thinking the UK would be viable if either for a short time or if there was really

118
00:18:06,680 --> 00:18:19,640
something substantial going on. Because we kind of agreed that I probably should go to Thailand

119
00:18:19,640 --> 00:18:28,360
to see my teacher. I'm getting calls. I'm trying to hang out with me.

120
00:18:37,960 --> 00:18:39,560
Who are you?

121
00:18:39,560 --> 00:18:44,680
Yeah, Bantere. It's Michael. I'm Michael. It's a one called Joseph trying to join the hangout.

122
00:18:44,680 --> 00:18:50,760
If you want to join the hangout, you have to go to meditation.surimungalow.org.

123
00:18:51,880 --> 00:18:54,600
That's where you find the link.

124
00:19:04,280 --> 00:19:05,320
Whoa, someone else.

125
00:19:05,320 --> 00:19:15,080
More than one person pinging me on hangout.

126
00:19:16,680 --> 00:19:22,600
I'm an atheist, but I find your talks really insightful. You keep it open for everyone

127
00:19:22,600 --> 00:19:26,120
and thank you. Well, I'm an atheist too, so don't be afraid.

128
00:19:26,120 --> 00:19:34,920
And here is Ken.

129
00:19:37,000 --> 00:19:38,920
So do you guys have questions or are you just?

130
00:19:39,560 --> 00:19:48,280
Yeah. I have a question about things that maybe are not really able to be mindful about.

131
00:19:49,640 --> 00:19:55,880
For instance, when I'm reading something, I feel like I really can't be mindful of

132
00:19:55,880 --> 00:20:00,280
it because once I'm noting reading reading, then I don't retain anything.

133
00:20:01,560 --> 00:20:04,440
Is there anything to be said about that kind of thing?

134
00:20:05,400 --> 00:20:11,240
There are just two different activities. Readings are different activity from meditating.

135
00:20:12,520 --> 00:20:17,000
In between reading, you know, when you're starting to get a headache or something,

136
00:20:17,000 --> 00:20:19,800
you'll be mindful of your, you can still be mindful in between reading.

137
00:20:19,800 --> 00:20:24,920
And reading is not as linear as you think, you know, there's time to process and then your mind

138
00:20:24,920 --> 00:20:31,560
wanders and you bring your mind back. So there is time in between readings to be mindful

139
00:20:31,560 --> 00:20:52,920
about the act itself, not so much. No. I mean, not everything is meditation.

140
00:20:53,800 --> 00:20:57,240
Hello. Hey, Ken? Yeah.

141
00:20:57,240 --> 00:21:01,880
Can you hear me? I can hear you.

142
00:21:02,680 --> 00:21:08,520
Okay. I just I just wanted to say hi, but also that's an interesting thing about when people say

143
00:21:09,800 --> 00:21:17,080
how are you? And then a lot of times I don't really know what to say. Like sometimes I just say,

144
00:21:17,080 --> 00:21:21,800
oh, not bad, but you know, there's kind of like a social convention to say you're doing fine,

145
00:21:21,800 --> 00:21:28,360
you know, and it's it's pretty subtle, but you know, it's always sometimes confusing.

146
00:21:29,000 --> 00:21:34,520
And sometimes for a while there, I used to just say sukaduka, because it sounds like superduper,

147
00:21:34,520 --> 00:21:38,760
but it's actually saying sukaduka. So you're good and bad. So I don't know.

148
00:21:40,120 --> 00:21:43,880
The Buddha would say the Buddha and the monks would say,

149
00:21:43,880 --> 00:21:52,360
comedy, young. In fact, this is what they ask each other is a comedy on Monday, yapanyang. Are you

150
00:21:52,360 --> 00:21:57,640
able to bear with it? Are you able to are you able to bear with it? Are you able to stand it?

151
00:21:59,400 --> 00:22:04,600
Let me say comedy on yes. I mean, I'm basically bearing up. I'm getting by and

152
00:22:04,600 --> 00:22:17,160
I'm able to bear with it. I don't know about this, pinging me on Hangouts, a bit.

153
00:22:21,080 --> 00:22:23,960
You think about all the questions? This is the problem now with questions.

154
00:22:23,960 --> 00:22:28,920
This is the answer to them off. And so, you know, I'm just getting questions that I've

155
00:22:28,920 --> 00:22:35,240
answered that question a thousand times. Someone's asking about meditating with music. I'm sure

156
00:22:35,240 --> 00:22:40,920
I've got two or three videos on meditating. I must. I must have answered that one, right?

157
00:22:43,800 --> 00:22:47,880
Anyway, I got a more probably a little deeper question.

158
00:22:47,880 --> 00:22:58,200
You know, on like the eightfold, a noble path, like there's the right thought. And then one is like

159
00:22:58,200 --> 00:23:13,240
of, you know, relinquishment. The one, the thought of renunciation. That's how I understood it

160
00:23:13,240 --> 00:23:21,400
anyways. And that seems like that seems like one that's sort of trying to cultivate that one.

161
00:23:22,280 --> 00:23:28,920
Seems like it takes a bit of finesse. Would you agree? Like right now, right? I'm just thinking

162
00:23:28,920 --> 00:23:34,120
like, well, what would it be like right now? All I got is like, well, what would it be right now

163
00:23:34,120 --> 00:23:41,880
right now if I didn't have any craving or I didn't have any aversion? Well, how would that be

164
00:23:41,880 --> 00:23:47,800
right now? And then I think, oh, that'd be positive. And that, but just, you know, to renounce,

165
00:23:47,800 --> 00:23:54,600
seems like a bit of a jump. Well, yeah, I mean, you're dealing with English translations

166
00:23:54,600 --> 00:24:01,320
and with superficial understandings. I mean, not in some of the insult, but you have to understand

167
00:24:01,320 --> 00:24:06,520
exactly what it's being said. They're all it is. It's the opposite of gamma-wetaka, which is

168
00:24:06,520 --> 00:24:12,280
a sensual desire, lustful thoughts, desire, and thought. And they come and just mean thoughts

169
00:24:12,280 --> 00:24:20,280
of thoughts that are free from from desire. So when you see something that you want,

170
00:24:20,280 --> 00:24:27,960
being able to see it without wanting it. And that's sort of, that's more or less right thought.

171
00:24:27,960 --> 00:24:36,840
Yeah. It's not, you're overthinking it, I think, in terms of,

172
00:24:38,280 --> 00:24:42,920
you know, it means you have to give up. So if it's not actually thought, it's, it's not actually

173
00:24:42,920 --> 00:24:46,920
thoughts like, oh, might you give this up or might you give that up? I mean, those are good thoughts,

174
00:24:46,920 --> 00:24:54,840
but right thought is just thought without without desiring and greed and greed. So it's just

175
00:24:54,840 --> 00:25:05,240
a more general that may come without, I mean, name means need not. So it's a comma. I'm actually

176
00:25:05,240 --> 00:25:09,480
not sure about that, but basically that sort of means without greed.

177
00:25:11,880 --> 00:25:18,360
So for a while, it might, there just might be a period of just kind of warming up to that idea of

178
00:25:18,360 --> 00:25:28,680
not thinking about things with, with desire or aversion. Well, it's more you look at the desire

179
00:25:28,680 --> 00:25:32,120
and you start to see, and you look at the things that you desire and you start to say that

180
00:25:32,840 --> 00:25:38,920
they're not worth desiring. That's what we pass in as all about. That's what the three characteristics

181
00:25:38,920 --> 00:25:43,240
are in permanent suffering and non-self. What does that mean? It means these things that you thought

182
00:25:43,240 --> 00:25:51,320
were stable, satisfying, and controllable are not actually. When you see that, the desire goes away,

183
00:25:51,320 --> 00:26:00,280
desire for them disappeared. That's where that's essentially a core we pass in it.

184
00:26:01,480 --> 00:26:05,880
Seeing the things that you thought were stable and stable and changing all the time,

185
00:26:05,880 --> 00:26:10,200
the things you thought were satisfying, not satisfying, mainly because they're unstable.

186
00:26:10,200 --> 00:26:16,360
And you can't change it, you can't control it, you can't fix it. Some things see

187
00:26:16,360 --> 00:26:21,720
how we're controllable and not controllable. So you give them up in this garbage,

188
00:26:23,880 --> 00:26:32,840
not for me. So slowly, slowly develop it. Let's just try to see things as they are. I mean,

189
00:26:32,840 --> 00:26:37,480
the truth is there. It's not, it's not about belief or changing your opinion. It's not about opinions.

190
00:26:37,480 --> 00:26:45,320
Look and see, and the claim is that this is what you'll see. You'll see in permanent suffering

191
00:26:45,320 --> 00:26:49,160
and on top, your desire will just disappear, and that's where right thought comes here.

192
00:26:49,160 --> 00:26:53,080
Right thought will naturally come through the practice.

193
00:26:56,440 --> 00:27:00,280
Now, you could, if you're wanting to be a little bit more conceptual about it and

194
00:27:00,280 --> 00:27:05,080
sort of daily life kind of thing, they're good thoughts, thoughts over an institution,

195
00:27:05,080 --> 00:27:10,680
or something, what sort of things can I give away? It's not right thought. I mean, that's not

196
00:27:10,680 --> 00:27:14,680
the definition of right thought. That's just a thought, and it can still be filled with greed,

197
00:27:14,680 --> 00:27:19,400
anger, and delusion. You can be thinking, I want to be a good Buddhist, so I'm going to do this,

198
00:27:22,040 --> 00:27:25,400
I hate all this suffering that I have, so I'm going to get rid of everything, and then I'll

199
00:27:25,400 --> 00:27:31,080
be free for this anger base. They're good thoughts, and they're the preliminary thoughts,

200
00:27:31,080 --> 00:27:38,680
but it's not right thought, or nobles, right thought. But why I bring it up is because that's

201
00:27:38,680 --> 00:27:42,280
a good thing. It's good to think about such things. I'm not trying to discount these thoughts of,

202
00:27:43,240 --> 00:27:48,200
you know, really, I could live without this, I could live without that. Those are good,

203
00:27:48,200 --> 00:27:56,360
conventionally good. It's good to have such thoughts, and to be thoughtful in general,

204
00:27:56,360 --> 00:28:01,720
in a conventional sense. Sometimes we put too much emphasis on meditation, and I don't want to do

205
00:28:01,720 --> 00:28:07,480
that. It's good to be thoughtful as a human being, and so social thoughtfulness is good as well.

206
00:28:08,520 --> 00:28:13,560
Worry about the environment, not worry, but be conscious of the environment, be conscious of

207
00:28:13,560 --> 00:28:21,000
like today we were talking about the TRC, the in Canada, we have this truth and reconciliation

208
00:28:21,000 --> 00:28:27,320
commission, which is really interesting as a Buddhist, because it talks about people respecting

209
00:28:27,320 --> 00:28:33,720
each other and learning to live in harmony, because the Canadian government really not as much

210
00:28:33,720 --> 00:28:42,600
as the American government I don't think, but they really crapped on the First Nations people,

211
00:28:42,600 --> 00:28:50,600
and continue to do so. It's getting maybe a little better, but not where it should be.

212
00:28:53,320 --> 00:28:55,560
Yeah, I know.

213
00:28:57,880 --> 00:29:02,280
So yeah, I'm thinking about these things and talking to people and appreciating people.

214
00:29:02,920 --> 00:29:07,160
I love that the university, there's so many good people doing good things. I mean,

215
00:29:07,160 --> 00:29:11,320
most people are just people, but there are groups that are doing this good thing, so I always

216
00:29:11,320 --> 00:29:17,080
take time as I walk by to look at all the all the club tables, because I have

217
00:29:18,200 --> 00:29:22,280
you know, sometimes I'll sit there with the meditation, but otherwise, just walk by and

218
00:29:23,160 --> 00:29:29,400
talk to them, and if they're doing something good like for AIDS or for the AIDS table was funny,

219
00:29:29,400 --> 00:29:34,600
because they're handing stuff out, and so they tried to hand me a package of candy and a condom.

220
00:29:34,600 --> 00:29:42,600
And I laughed at the condom, I said, no, thank you. And the first one was kind of a little bit,

221
00:29:42,600 --> 00:29:45,800
you know, why are you laughing kind of thing? And I said, I guess I shouldn't laugh, but

222
00:29:50,840 --> 00:29:52,520
not going to have much use for that.

223
00:29:52,520 --> 00:30:05,160
But yeah, but I said, I talked to them, I said, good job, I was, and there was a group raising

224
00:30:05,160 --> 00:30:12,600
for homeless and you get your thumbs up and raising money, and most of them raising money,

225
00:30:12,600 --> 00:30:18,920
and I don't ever have money obviously, so I'll just give them moral support.

226
00:30:18,920 --> 00:30:25,720
It's great to talk to people, you know, who have good thoughts. University is a place where a

227
00:30:25,720 --> 00:30:31,400
lot of good thoughts come about. Let me say there's a lot of problems at the university, but

228
00:30:32,680 --> 00:30:38,600
there is a segment that is a lot of goodness. So thinking about these things and being conscious,

229
00:30:38,600 --> 00:30:43,400
I think it's good. You shouldn't confuse it with meditation or right thought, but all right,

230
00:30:43,400 --> 00:30:49,160
thought is much deeper. Right, thought you need a pure mind that doesn't have any way to do a talk

231
00:30:49,160 --> 00:30:53,800
into people or thinking about issues or something. Thinking about your possessions is not that.

232
00:30:55,160 --> 00:31:03,240
Right, thought is, in general, and in totality in the end, to be a type of thoughts that are free

233
00:31:03,240 --> 00:31:08,040
from green anger, and it was from whatever those thoughts made me. So when you think about something

234
00:31:08,040 --> 00:31:17,640
that would normally make you angry, I'm not getting angry. So if you really have right thought,

235
00:31:17,640 --> 00:31:22,440
then you're pretty much, if you really have it, you're pretty much in your hands, right?

236
00:31:23,560 --> 00:31:29,400
Well, a soda banana attains it in the moment right before they realize the bander or

237
00:31:29,400 --> 00:31:40,680
the moment of realizing the bander. Technically, the noble aidful bat doesn't arise until

238
00:31:41,480 --> 00:31:48,920
the moment of enlightenment as a soda banana. So up until that point, you're practicing what's

239
00:31:48,920 --> 00:31:55,800
called the pubangamanga, which is developing, cultivating the path factors, but they don't ever

240
00:31:55,800 --> 00:32:07,800
come to be noble until the moment of enlightenment. But you put them up there as the goal,

241
00:32:07,800 --> 00:32:13,960
and then you think, well, we're working towards that. So my mind is more free from green anger

242
00:32:13,960 --> 00:32:26,040
and delusion, so I'm getting closer to that kind of thing. I always found that I could be mindful

243
00:32:26,040 --> 00:32:32,600
as long as I didn't start talking, but as soon as I started talking, I kind of lost my mindfulness,

244
00:32:33,880 --> 00:32:41,720
and then when I didn't really check out the fateful path, but then on right speech, it was

245
00:32:41,720 --> 00:32:46,920
like, oh, if you follow all these kind of rules, then you kind of got to remember them as you're

246
00:32:46,920 --> 00:32:49,560
talking, and then you can sort of get some mindfulness.

247
00:32:53,160 --> 00:33:00,360
Yeah. I mean, again, it's still fairly conventional because right speech is probably no speech

248
00:33:01,480 --> 00:33:09,560
in the end, because it has to have the other seven factors, and you can't really have

249
00:33:09,560 --> 00:33:17,800
right mindfulness, and so on when you're speaking. I mean, actually, it is possible. It's

250
00:33:17,800 --> 00:33:21,080
possible you could become enlightened while you're speaking, but you'd have to be watching your

251
00:33:21,080 --> 00:33:27,720
lips when you have to be aware of that feeling, and you have to be really present. Because speaking

252
00:33:27,720 --> 00:33:33,000
is actually, it's a process. There's the thought, and the thought leads to the speech, and you can

253
00:33:33,000 --> 00:33:36,760
feel that. You know, you have a thought, and then suddenly your lips start moving and your voice

254
00:33:36,760 --> 00:33:40,840
box starts rattling, and you can be mindful of all that as it's happening.

255
00:33:45,160 --> 00:33:49,240
I don't want to suddenly meditate and get very angry at me for this. You thought it was absurd

256
00:33:49,240 --> 00:33:55,800
that you could be mindful while you're talking. How would you go about noting that if you're

257
00:33:55,800 --> 00:34:08,360
feeling watching your lips and so on?

258
00:34:15,320 --> 00:34:16,920
That's those to have some discussion.

259
00:34:16,920 --> 00:34:27,880
Yeah, especially, especially like, well, where I am, pretty much any discussion I have

260
00:34:27,880 --> 00:34:35,800
is used to be on the internet. I don't really know tons of people who are practicing

261
00:34:35,800 --> 00:34:49,640
almost not actually. Well, that's what we're here for. It's nice to be able to bring people together.

262
00:34:54,280 --> 00:34:57,160
Okay, thank you very much. I got more than enough to go over here.

263
00:34:57,160 --> 00:35:05,240
Okay. I'm going to head out. Have a good night.

