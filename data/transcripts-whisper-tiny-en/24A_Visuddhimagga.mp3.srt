1
00:00:00,000 --> 00:00:08,800
Today, we're going to check the 11.

2
00:00:08,800 --> 00:00:16,160
In this chapter, two kinds of meditation are treated.

3
00:00:16,160 --> 00:00:24,360
And I'm afraid the first one may not be popular with the people in the West.

4
00:00:24,360 --> 00:00:33,120
It is the perception of repulsiveness in nutrient treatment.

5
00:00:33,120 --> 00:00:42,520
But we must understand that this meditation is not for not eating, not for refraining

6
00:00:42,520 --> 00:00:53,760
from eating, but it is eating with greed, eating with non-attachment, eating without

7
00:00:53,760 --> 00:00:54,760
greed.

8
00:00:54,760 --> 00:01:02,800
I mean, eating with non-attachment, because whether you are a labor center or a monk,

9
00:01:02,800 --> 00:01:10,440
you have to eat enough, say to give you healthy and to keep you in a good shape.

10
00:01:10,440 --> 00:01:14,080
So this is eating without attachment.

11
00:01:14,080 --> 00:01:23,440
And in order to get rid of attachment, we have to get the perception of repulsiveness

12
00:01:23,440 --> 00:01:29,120
in food.

13
00:01:29,120 --> 00:01:40,160
And this book is meant for monks, and so monks have to practice restraint in all areas.

14
00:01:40,160 --> 00:01:44,640
And so with regard to eating two days to be restrained.

15
00:01:44,640 --> 00:01:55,040
And especially in the teachings of tera water Buddhism, when we eat, what we eat is not

16
00:01:55,040 --> 00:01:59,400
so important as how we eat.

17
00:01:59,400 --> 00:02:06,320
So the way we eat is much more important than what we eat.

18
00:02:06,320 --> 00:02:13,320
So when we eat, we try to do arouse this perception of repulsiveness in food.

19
00:02:13,320 --> 00:02:18,640
And so we do not get too much attached to food.

20
00:02:18,640 --> 00:02:24,020
So now comes the description of the development of perception of repulsiveness in

21
00:02:24,020 --> 00:02:25,020
nutriman.

22
00:02:25,020 --> 00:02:34,760
Actually, I think we should translate this word as food, although the pali word ahaara

23
00:02:34,760 --> 00:02:39,000
can mean nutriman as well as food.

24
00:02:39,000 --> 00:02:48,520
We are according to abidema, and the word ahaara means nutriman, but in popular usage,

25
00:02:48,520 --> 00:02:50,760
the word ahaara means food.

26
00:02:50,760 --> 00:02:53,440
So repulsiveness in food.

27
00:02:53,440 --> 00:02:58,160
So we just listed as the one perception next to the immaterial states.

28
00:02:58,160 --> 00:03:05,720
And then the commentator defines the word ahaara, the pali word ahaara, it nourishes.

29
00:03:05,720 --> 00:03:12,760
Literally, it brings, it brings on, thus it is nutriman, so thus it is food.

30
00:03:12,760 --> 00:03:20,440
That is of food kinds as physical nutriman, nutriman consisting of contact, nutriman consisting

31
00:03:20,440 --> 00:03:25,520
of mental volition, and nutriman consisting of consciousness.

32
00:03:25,520 --> 00:03:34,000
Now these food, things are called by the name of ahaara in pali, by the name of food,

33
00:03:34,000 --> 00:03:38,000
by the name of nutriman.

34
00:03:38,000 --> 00:03:41,600
The first one is physical nutriman.

35
00:03:41,600 --> 00:03:47,400
In fact, it is not a direct translation of the pali word.

36
00:03:47,400 --> 00:03:57,000
The pali word is kabalikara, you will find the word down in the footnote too, kabalinkara,

37
00:03:57,000 --> 00:03:59,000
kabalinkara ahaara.

38
00:03:59,000 --> 00:04:03,960
Now, the word, in the word kabalinkara, there is the word kabalinkara.

39
00:04:03,960 --> 00:04:17,680
Kabalah means a handful, we do not call food a mouthful but we use the word handful

40
00:04:17,680 --> 00:04:24,200
because we eat with hands and when months eat, they pick up the food in their hands and

41
00:04:24,200 --> 00:04:30,000
then make it into something like a ball and then put it in their mouth.

42
00:04:30,000 --> 00:04:36,280
So this handful is called kabalah in pali.

43
00:04:36,280 --> 00:04:42,800
So the word comes to mean food in general.

44
00:04:42,800 --> 00:04:49,360
So kabalikara, it is translated as physical nutriman here.

45
00:04:49,360 --> 00:05:01,320
But according to abirama, what is ahaara is not the whole food itself but it is called

46
00:05:01,320 --> 00:05:11,000
oja, nutrative essence which is contained in the food and that is what is called ahaara

47
00:05:11,000 --> 00:05:16,000
according to abirama.

48
00:05:16,000 --> 00:05:22,480
Although here physical nutriman or pali word kabalinkara ahaara is used, we must understand

49
00:05:22,480 --> 00:05:27,720
that it means in nutrative essence in the food.

50
00:05:27,720 --> 00:05:34,800
So that is one food, nutriman consisting of contact means contact is also called food, contact

51
00:05:34,800 --> 00:05:44,240
is a mental factor and that is also called food and then the next is mental volition.

52
00:05:44,240 --> 00:05:49,640
So volition is also called food and the last one is consciousness.

53
00:05:49,640 --> 00:05:57,440
So in one of the soldiers Buddha said there are four kinds of food and these are mentioned.

54
00:05:57,440 --> 00:06:08,600
So they are called food because they nourish something, then the question is which nourishes

55
00:06:08,600 --> 00:06:13,280
what?

56
00:06:13,280 --> 00:06:19,680
So I think the first line in paragraph two should see like that but which nourishes what?

57
00:06:19,680 --> 00:06:21,720
Simply touch.

58
00:06:21,720 --> 00:06:28,680
Now physical nutriman, kabalinkara, ahaara nourishes or brings on the materiality of the

59
00:06:28,680 --> 00:06:36,120
object that has nutrative essence as eight.

60
00:06:36,120 --> 00:06:41,320
You need some abirama to understand this.

61
00:06:41,320 --> 00:06:47,200
There are material particles, I mean material qualities of material properties and there

62
00:06:47,200 --> 00:06:49,240
are set to be 28.

63
00:06:49,240 --> 00:06:59,720
And these 28 material properties arise in groups and some group has eight material properties

64
00:06:59,720 --> 00:07:06,360
in it, some have 9, some have 10, some have 11 and so on.

65
00:07:06,360 --> 00:07:15,720
And the least number of material properties contain in a group is eight.

66
00:07:15,720 --> 00:07:21,400
So anywhere I just said that according to Uvirama, anywhere we find these eight material

67
00:07:21,400 --> 00:07:26,560
properties and living beings or outside things.

68
00:07:26,560 --> 00:07:36,800
But here the the the eight is set to be one, nutriman, I mean material of the object that

69
00:07:36,800 --> 00:07:47,200
has nutrative essence as eight, that means you remember the eight, eight material qualities,

70
00:07:47,200 --> 00:08:02,160
the four primaries and then color, smell, taste and this one, this ahaara origin.

71
00:08:02,160 --> 00:08:12,960
So these are set to be found everywhere, where there is a meta material properties.

72
00:08:12,960 --> 00:08:24,000
And these eight are the result of cost by comma or sometimes cost by checker, I mean

73
00:08:24,000 --> 00:08:31,680
consciousness sometimes cost by temperature or climate and sometimes by ahaara nutriman.

74
00:08:31,680 --> 00:08:35,960
Now here that cost by nutriman is needed.

75
00:08:35,960 --> 00:08:51,320
So the cabblingara ahaara brings on or nourishes the eight material properties which

76
00:08:51,320 --> 00:08:59,400
are the which are cost by ahaara.

77
00:08:59,400 --> 00:09:08,160
So that is why the the nutrative essence is called ahaara or physical nutriman in in

78
00:09:08,160 --> 00:09:10,120
barley language.

79
00:09:10,120 --> 00:09:15,560
The second one is contact as nutriman nourishes the three kinds of filling.

80
00:09:15,560 --> 00:09:20,280
When there is contact there is filling right and in the in the 12th purpling, but it I

81
00:09:20,280 --> 00:09:33,240
mean dependent origination, filling is conditioned by what contact.

82
00:09:33,240 --> 00:09:40,800
So contact is said to nourish or said to bring the three kinds of filling plus and

83
00:09:40,800 --> 00:09:45,000
filling and and plus and filling and neutral filling.

84
00:09:45,000 --> 00:09:47,640
If there is no contact there, there will be no filling.

85
00:09:47,640 --> 00:09:53,440
So contact is said to to be the condition of filling.

86
00:09:53,440 --> 00:10:00,680
Mental volition as nutriman nourishes brings on rebut linking in the three kinds of becoming.

87
00:10:00,680 --> 00:10:11,040
Now mental volition means chaitana actually mental volition means comma or sankara in

88
00:10:11,040 --> 00:10:17,280
the dependent origination, the second link comma formations.

89
00:10:17,280 --> 00:10:19,760
So mental volition is comma formation.

90
00:10:19,760 --> 00:10:22,360
And so what what does it bring?

91
00:10:22,360 --> 00:10:30,220
It brings patisandhi, rebut linking in the three kinds of becoming that is sent sent

92
00:10:30,220 --> 00:10:39,760
shasbikami, find with a material becoming and immaterial becoming.

93
00:10:39,760 --> 00:10:46,480
Consciousness as nutriman nourishes mental materiality at the moment of rebut linking.

94
00:10:46,480 --> 00:10:52,760
This is also to be understood with reference to the dependent origination.

95
00:10:52,760 --> 00:10:59,840
So consciousness conditions, mentality, materiality or mind and matter.

96
00:10:59,840 --> 00:11:06,120
At the moment of rebut there is rebut consciousness and together with rebut consciousness

97
00:11:06,120 --> 00:11:14,360
there are mental mental factors and also there are material properties caused by kama in

98
00:11:14,360 --> 00:11:15,360
the first.

99
00:11:15,360 --> 00:11:29,000
So what we call rebut is the the rebut consciousness it's concomitant and kama bone

100
00:11:29,000 --> 00:11:31,000
matter.

101
00:11:31,000 --> 00:11:40,600
So consciousness or here actually rebut consciousness, nourishes or is the condition for mind

102
00:11:40,600 --> 00:11:46,600
and matter, mind and matter we are means the mental factors and material properties.

103
00:11:46,600 --> 00:11:47,600
So they are there.

104
00:11:47,600 --> 00:11:53,320
So it is called ahara or in English nutriman.

105
00:11:53,320 --> 00:11:57,640
Now when there is physical nutriman there is attachment which brings peril.

106
00:11:57,640 --> 00:12:08,520
That means the danger in physical nutriman is attachment.

107
00:12:08,520 --> 00:12:12,920
So because ah we are attached to food.

108
00:12:12,920 --> 00:12:15,360
So when we eat food we get attachment.

109
00:12:15,360 --> 00:12:20,320
So the danger in physical and nutriman is attachment.

110
00:12:20,320 --> 00:12:31,040
When there is nutriman as contact there is approaching coming together and that is that brings

111
00:12:31,040 --> 00:12:37,920
peril or that is the danger.

112
00:12:37,920 --> 00:12:50,640
And when there is nutriman as mental volition there is reappearance means rebut in different

113
00:12:50,640 --> 00:12:53,760
existences which brings peril.

114
00:12:53,760 --> 00:13:04,200
So ah mental volition ah I mean rebut is a danger in mental volition because it brings

115
00:13:04,200 --> 00:13:11,520
about rebirth and when there is nutriman as consciousness there is rebut linking.

116
00:13:11,520 --> 00:13:20,560
That means the mental volition gives rebirth.

117
00:13:20,560 --> 00:13:29,280
So actually ah something like throwing throwing down something that is the the function

118
00:13:29,280 --> 00:13:32,360
of mental volition.

119
00:13:32,360 --> 00:13:44,840
And the rebirth itself the rebirth consciousness itself is ah has a it is a danger in consciousness.

120
00:13:44,840 --> 00:13:52,360
So rebut linking is danger in consciousness.

121
00:13:52,360 --> 00:13:56,280
And to show how they bring fear thus physical nutriman should be illustrated by the

122
00:13:56,280 --> 00:14:03,280
seemingly of the child's flesh.

123
00:14:03,280 --> 00:14:12,600
That is kindred sayings volume 2 page 68 following ah in that sort of old Buddha said

124
00:14:12,600 --> 00:14:22,880
the the the real food should be regarded as a child's one's own child's flesh and the

125
00:14:22,880 --> 00:14:25,640
second one was contact.

126
00:14:25,640 --> 00:14:32,880
Should be regarded as heightless cow and mental volition should be regarded as a pit

127
00:14:32,880 --> 00:14:42,240
of life cold and consciousness should be regarded as is a hundred spheres yet to read

128
00:14:42,240 --> 00:14:46,280
that so that you understand this.

129
00:14:46,280 --> 00:14:54,720
So the first one is explain by way of a story not not an actual story but just just

130
00:14:54,720 --> 00:15:03,040
similarly so two people husband and wife with the child go go went on the journey and

131
00:15:03,040 --> 00:15:08,240
then they were they were going through a desert and they they have lost everything so they

132
00:15:08,240 --> 00:15:17,320
were thirsty they were hungry and so they decided to eat the flesh of their child just

133
00:15:17,320 --> 00:15:22,000
to get out of that wilderness.

134
00:15:22,000 --> 00:15:30,120
So when they eat the the flesh of their child they they do not they do not feel heavy

135
00:15:30,120 --> 00:15:37,960
but they they may they may eat the tears and so on and so in the same way the food should

136
00:15:37,960 --> 00:15:47,760
be eaten with that kind of perception as though you are eating the flesh of your own child.

137
00:15:47,760 --> 00:15:52,280
So when you have to eat the flesh of your own child you cannot have any attachment to

138
00:15:52,280 --> 00:15:58,920
that flesh and contact as new treatment by the seemingly of heightless cow.

139
00:15:58,920 --> 00:16:03,080
So a cow who's skin or height is taken off.

140
00:16:03,080 --> 00:16:09,720
So wherever it goes it would be bitten by flies mosquitoes and so on.

141
00:16:09,720 --> 00:16:17,640
So in the air or even it goes into the water wherever it goes there is this there are these

142
00:16:17,640 --> 00:16:23,520
flies and others to to to to to bite or to sting it.

143
00:16:23,520 --> 00:16:29,680
So in the same way when there is contact there is all is feeling so contact is accompanied

144
00:16:29,680 --> 00:16:31,520
always by feeling.

145
00:16:31,520 --> 00:16:42,520
So feeling is compared to a biting of insects and so on and less than feeling that's right

146
00:16:42,520 --> 00:16:48,760
and mental volition as new treatment by the seemingly of pit of life cold.

147
00:16:48,760 --> 00:16:58,120
So nobody wants to fall into the pit of life cold because it brings suffering so in the

148
00:16:58,120 --> 00:17:07,000
same way mental volition throws throws us into a new existence and when we are reborn

149
00:17:07,000 --> 00:17:13,960
in a new existence we will suffer the dukha in here and in that life.

150
00:17:13,960 --> 00:17:19,520
So it is to be regarded as a pit of life course and the last one is consciousness new

151
00:17:19,520 --> 00:17:28,840
treatment by the seemingly of a hundred spears that is a it the court and then send

152
00:17:28,840 --> 00:17:34,920
them to be executed and it would be carried through this space and then give a hundred

153
00:17:34,920 --> 00:17:43,160
blows here hundred blows there and so on and then some of them are explained I think

154
00:17:43,160 --> 00:18:01,600
of a footnote yeah yes now out of these four kinds of new treatment or what are calls

155
00:18:01,600 --> 00:18:09,960
new treatment only one the first one will be treated or is it the only the first one is relevant

156
00:18:09,960 --> 00:18:15,280
in this in this chapter or this kind of meditation now in the footnote the explanations

157
00:18:15,280 --> 00:18:23,640
are given and I hope you have read it about the middle of the footnote approaching is

158
00:18:23,640 --> 00:18:32,440
explained as meeting coinciding with an abandoned versions of perception due to an object

159
00:18:32,440 --> 00:18:44,520
being perceived as permanent etc. when it is not so I think that is a little incorrect.

160
00:18:44,520 --> 00:18:54,720
What is meant is the approaching is meeting coinciding or coming into contact not with

161
00:18:54,720 --> 00:19:02,440
an abandoned traversions but for those whose traversions and yet an abandoned so he

162
00:19:02,440 --> 00:19:16,520
misunderstood the poly compound word here poly compounds are very difficult to decide what

163
00:19:16,520 --> 00:19:24,720
they mean the same word they mean two different things so yeah what really is meant is

164
00:19:24,720 --> 00:19:33,400
for those whose perversions are yet an abandoned meeting with an object so when we meeting

165
00:19:33,400 --> 00:19:41,800
with an object at that meeting with an object is parallel since it is not free from the

166
00:19:41,800 --> 00:19:46,400
three kinds of suffering so when there is meeting with the object there is contact and

167
00:19:46,400 --> 00:19:58,520
feeling okay go back so out of these four it is only physical

168
00:19:58,520 --> 00:20:05,680
nuterman class at what is eaten drunk chewed and tasted and tasted here it will be explained

169
00:20:05,680 --> 00:20:14,480
later perception that is intended here as nuterman in this sense so the food is intended

170
00:20:14,480 --> 00:20:19,600
as ahara or nuterman in this chapter the perception arisen as the apprehension of the

171
00:20:19,600 --> 00:20:25,800
robustness aspect in that nuterman is perception of robustiveness in nuterman one who wants

172
00:20:25,800 --> 00:20:30,640
to develop the perception of robustiveness in nuterman should learn the meditation subject

173
00:20:30,640 --> 00:20:43,640
and all these things so we should develop or around the perception of robustiveness in

174
00:20:43,640 --> 00:20:56,320
10 ways that is what has to go in going for food I mean it is for months going for food

175
00:20:56,320 --> 00:21:06,320
seeking food using means eating food secretions receptacle what is undigested what is

176
00:21:06,320 --> 00:21:15,760
digested and then resort of fruit fruit outflow and then smearing so all these are designed

177
00:21:15,760 --> 00:21:24,580
to help us to get the sense of repulsiveness set towards food so nowadays people say

178
00:21:24,580 --> 00:21:31,840
we enjoy food please enjoy food here we don't enjoy but we eat we eat with this kind

179
00:21:31,840 --> 00:21:39,640
of reflection so that we don't get attached to food not the first one going so in

180
00:21:39,640 --> 00:21:48,280
month has to go out every day for arms in the morning so it is this percentage of months

181
00:21:48,280 --> 00:21:54,680
even when a man has gone for them so mighty dispensation still after he has perhaps spent

182
00:21:54,680 --> 00:22:00,120
all night reciting the enlightened ones were doing the aesthetics work that means doing

183
00:22:00,120 --> 00:22:06,160
meditation after he has risen early to do the duties connected with the shrine cherries

184
00:22:06,160 --> 00:22:11,240
and the enlightenment tree cherries to set out the water for drinking washing to strip

185
00:22:11,240 --> 00:22:15,760
the ground and to see the needs of the body after he has sat down on his seat and given

186
00:22:15,760 --> 00:22:24,760
attention to his meditation subject 20 or 30 times now 20 or 30 days it is not so clear

187
00:22:24,760 --> 00:22:33,040
and I think that the some commentary also did not know exactly what is meant because in

188
00:22:33,040 --> 00:22:39,040
the photo you will see different opinions are given now he has some say that the definition

189
00:22:39,040 --> 00:22:45,480
of the number of times is according to what is present by continuity that means one

190
00:22:45,480 --> 00:22:58,840
continuity after another now when you when you enter it enter a cell from light first

191
00:22:58,840 --> 00:23:07,360
you you cannot see clearly because you came from from light so after some some say seconds

192
00:23:07,360 --> 00:23:14,560
or minutes as you can see so that is called one continuity and if you go out the same thing

193
00:23:14,560 --> 00:23:20,800
will happen so go out from the dark into the light you will not you will not see clearly

194
00:23:20,800 --> 00:23:28,080
as soon as you go out you have to attach to the difference of how to go out light or

195
00:23:28,080 --> 00:23:38,920
elimination so in this way the times should be taken to me this continuity one continuity

196
00:23:38,920 --> 00:23:47,240
after another that is an opinion of some but others say that it is by way of warming

197
00:23:47,240 --> 00:23:54,120
up the seat so if you settle sometime then then the seat became becomes warm the development

198
00:23:54,120 --> 00:23:59,720
that does not reach at prehand suppression of hindrances does not remove the bodily discomfort

199
00:23:59,720 --> 00:24:04,240
in the act of sitting because of the lack of avoiding happiness so before you get good

200
00:24:04,240 --> 00:24:15,000
concentration you will still want to move may want to change a position every now and then

201
00:24:15,000 --> 00:24:24,520
so one one heating up of the position is what is meant by time here that is what the

202
00:24:24,520 --> 00:24:33,760
others say and then what about 20 and 30 now 20 and 30 is taken as the number already

203
00:24:33,760 --> 00:24:44,840
observed by the time of setting out on the arms wrong in fact it means taken as the number

204
00:24:44,840 --> 00:24:52,520
by the noted and by the formally noted time of setting up the I will set out for arms

205
00:24:52,520 --> 00:25:03,400
at such a time so that that is decided beforehand so the number means say by the

206
00:25:03,400 --> 00:25:12,040
status according to modern time we will say I will go on at 6 30 so before 6 30 until

207
00:25:12,040 --> 00:25:18,040
6 30 you will be practicing meditation something like that so this last one is the one

208
00:25:18,040 --> 00:25:27,240
that goes to things is most likely no another will come or alternatively from going to up

209
00:25:27,240 --> 00:25:33,000
to smelling is one turn one time so that means there are 10 10 reflection to be made

210
00:25:33,000 --> 00:25:38,720
so from the first reflection to get this chance reflection to get one round one turn

211
00:25:38,720 --> 00:25:50,440
one time so that that means the month contemplates on this by these 10 ways 20 times

212
00:25:50,440 --> 00:25:58,360
or 30 times before he gets up and go for arms actually not not clear which is meant

213
00:25:58,360 --> 00:26:08,400
so we can take the last one so 20 or 30 times and got up again then he must take his

214
00:26:08,400 --> 00:26:23,720
bow and outer open so on so that is the with respect to going food is so for instance

215
00:26:23,720 --> 00:26:40,200
four way to understand this maybe it's a mutation subject is the graph it could be no it could

216
00:26:40,200 --> 00:26:51,520
be say when you use spread it as an object you see in one how one in two out to entry

217
00:26:51,520 --> 00:27:00,040
out to 10 that would be one time there and it again in one out one into two and then

218
00:27:00,040 --> 00:27:14,240
out to and then two and so on and in paragraph seven line three what is keckles because

219
00:27:14,240 --> 00:27:25,360
there are little animals little they sometimes eat cockroaches and insects and lizards

220
00:27:25,360 --> 00:27:31,600
and how is it right yeah it's a small small house is it house lizards we don't see many

221
00:27:31,600 --> 00:27:37,280
of them here and here actually one has me here we discussed how to come take care of

222
00:27:37,280 --> 00:27:55,880
and then on page and next page for a graph eight in due course after standing in the

223
00:27:55,880 --> 00:28:04,840
debating lodge I don't know whether that can be called debating because it is a place where

224
00:28:04,840 --> 00:28:15,920
being the monk decides which which way to go for arms round so in a in a lodge for thinking

225
00:28:15,920 --> 00:28:25,040
in the way beginning where must I go for arms today so maybe there are two or three villages

226
00:28:25,040 --> 00:28:30,720
not far from the monastery so he may go to one village and one day and to another village

227
00:28:30,720 --> 00:28:38,000
the other day so he decides where which way to go or which village to go and standing

228
00:28:38,000 --> 00:28:47,720
and that lodge I'm in that place that is translated as debating lodge he may be debating

229
00:28:47,720 --> 00:28:59,080
internally which village to go so he did all these things and this is the robustiveness

230
00:28:59,080 --> 00:29:09,520
in a robustiveness of food with regard to going and then as to seeking after going to

231
00:29:09,520 --> 00:29:16,280
the village the among must go from one house to another so he where when he has enjoyed

232
00:29:16,280 --> 00:29:20,480
the robustiveness of going this way and has gone into the village and his clothing is

233
00:29:20,480 --> 00:29:25,080
cloak of patches he has to wander in the village street from house to house like a beggar

234
00:29:25,080 --> 00:29:35,880
with a dish in his hand and so on and it may not be so so bad nowadays and also in

235
00:29:35,880 --> 00:29:44,320
Buddhist countries but if not all of the people of Buddhist and some may just drive us away

236
00:29:44,320 --> 00:29:52,600
if we go and stand in front of their houses so you see here on page three hundred seventy

237
00:29:52,600 --> 00:30:02,880
six paragraph twelve others treating with hash word such as go away you bore head.

238
00:30:02,880 --> 00:30:11,640
When we were in Sri Lanka we do not know which house is a Buddhist and which is not

239
00:30:11,640 --> 00:30:17,920
so sometimes we stood in front of a house of a Christian or also the Muslim and when

240
00:30:17,920 --> 00:30:28,760
Buddhist so as if we just break us to be a house don't stand there but they they may

241
00:30:28,760 --> 00:30:38,920
not drive us away but it might have happened in the in the olden days when people are

242
00:30:38,920 --> 00:30:50,160
not all Buddhist so that is seeking number three the third is how as to using as to

243
00:30:50,160 --> 00:31:02,160
using really means as to eating sometimes to be literal may miss the real point so

244
00:31:02,160 --> 00:31:10,960
yeah using using a food means just eating so in eating to there is repulsiveness and because

245
00:31:10,960 --> 00:31:19,960
it is a India Sri Lanka and Burma troubled the countries so when you have to go from from

246
00:31:19,960 --> 00:31:25,120
your monastery to be village and then go on the rounds you may be sweating and so your

247
00:31:25,120 --> 00:31:32,480
hands may be smeared with sweat so that's why here it's said and sitting at this in a

248
00:31:32,480 --> 00:31:38,080
control place out of the village then so long as he has not dipped his hand into it he

249
00:31:38,080 --> 00:31:42,800
would be able to invite you respected be who or a decent person if you so want to share

250
00:31:42,800 --> 00:31:47,400
it but as soon as he has dipped his hand into it out of desire to eat he would be a

251
00:31:47,400 --> 00:31:57,520
change to say take some because now the food is made with his sweat or something so this

252
00:31:57,520 --> 00:32:06,560
is eating he put his in a mouth and then jaws doing the function of what pestle and the

253
00:32:06,560 --> 00:32:19,520
other a mortar and so on so we have to contemplate on this when we eat I don't know whether

254
00:32:19,520 --> 00:32:31,060
it would be good to eat thinking of this you may even want to form it now food not

255
00:32:31,060 --> 00:32:38,280
13 kamasad jelly usually rendered janket I don't know what janket is but the vineyard

256
00:32:38,280 --> 00:32:47,880
commander is given that's made of corn yowa yowa is not corn it is burly or wheat

257
00:32:47,880 --> 00:33:01,360
janket is a custard janket is a custard it should be a bit of burly or wheat some kind of

258
00:33:01,360 --> 00:33:15,240
something like rice not necessarily jelly then as to secretion yeah boaters and

259
00:33:15,240 --> 00:33:20,560
particular boaters and we'll turn in one acts of only one of the four secretions consisting

260
00:33:20,560 --> 00:33:26,720
of bile plant plus and black I don't know whether that is true and and I hear also

261
00:33:26,720 --> 00:33:33,720
which one do they have or it could be anyone anyone so the only is not not needed here so

262
00:33:33,720 --> 00:33:44,080
instead of only I want to say any have any one of them not only and I think we should say

263
00:33:44,080 --> 00:33:50,160
even boaters and particular boaters and we'll turning on as if any one of the four secretions

264
00:33:50,160 --> 00:33:55,920
consisting of bile plant plus and black but those with weak merits have all four so when

265
00:33:55,920 --> 00:34:02,720
the food has arrived arrived at the stage of being eaten and it enters inside then in one

266
00:34:02,720 --> 00:34:08,720
whose secretion of bile is in excess it becomes as utterly not sitting as if smeared with thick

267
00:34:08,720 --> 00:34:19,200
muduga oil so muduga is also difficult to translate a kind of seed which gives oil

268
00:34:19,200 --> 00:34:27,400
I'm sure it's not advertising no and it is thick oil so in one whose ingredient of

269
00:34:27,400 --> 00:34:33,840
flame is in excess it is as if smeared with the juice of an agabola leaves maybe a

270
00:34:33,840 --> 00:34:43,160
sticky sticky substance maybe you get a sticky substance from that kind of leaf maybe something

271
00:34:43,160 --> 00:34:53,560
like one called that ladies finger ok ok ok ok ok ok ok can be sticking with the special

272
00:34:53,560 --> 00:34:59,920
it's ok something like that in one whose equation of pus is in excess it is as if smeared

273
00:34:59,920 --> 00:35:05,760
with unsaid butter milk and in one whose section of blood is in excess it is as utterly

274
00:35:05,760 --> 00:35:10,720
not sitting as if smeared with the dye so that is how robust ifness should be reviewed

275
00:35:10,720 --> 00:35:17,320
as to secretion and then as to receptacle when it has gone inside the belly and is smeared

276
00:35:17,320 --> 00:35:23,000
with one of these secretions then the receptacle it goes into is no gold dish or crystal

277
00:35:23,000 --> 00:35:32,560
or silver dish and so on and so on and then 19 what as to what is undigested and cookie

278
00:35:32,560 --> 00:35:39,080
I mean undigested so there is we believe there is something like a fire in the stomach

279
00:35:39,080 --> 00:35:53,800
and that fire I mean that heat digest the food so it is not yet digested and the next one

280
00:35:53,800 --> 00:35:59,600
is what is cooked what is digested when it has been completely cooked there by the body

281
00:35:59,600 --> 00:36:08,400
fires it does not turn into gold silver etc as the pores of coal silver etc do through

282
00:36:08,400 --> 00:36:15,440
smelting instead giving of food and froth and bubbles it turns into experiment and fills

283
00:36:15,440 --> 00:36:22,720
the receptacle for digested food like brown clays squeezed with this smoothing trowel

284
00:36:22,720 --> 00:36:33,520
and packed into a tube so receptacle for digested food really means later part of the

285
00:36:33,520 --> 00:36:43,120
intestines and it turns into urine and fills the bladder this is how robust ifness should

286
00:36:43,120 --> 00:36:51,200
be reviewed as to what is cooked as be fruit when that means as to the result when it has

287
00:36:51,200 --> 00:36:58,720
been rightly cooked it is well digested it produces the various kinds of poger consisting

288
00:36:58,720 --> 00:37:06,320
of head hairs bodies hairs and so on that is when when it is digested well when wrongly

289
00:37:06,320 --> 00:37:12,280
cooked that is when it is not digested it produces a hundred diseases beginning with each

290
00:37:12,280 --> 00:37:18,840
spring warm small pores leprosy clay consumption cause flux and so on such is its fruit

291
00:37:18,840 --> 00:37:25,720
as how robust ifness should be reviewed as to the fruit and then as to outflow on being

292
00:37:25,720 --> 00:37:31,440
followed it enters by one door after which it flows out by several doors in the way beginning

293
00:37:31,440 --> 00:37:37,480
either from the eye either from the eye and so on and on being followed it was followed

294
00:37:37,480 --> 00:37:43,880
even in the company of last gathering but on flowing out now converted into excrement

295
00:37:43,880 --> 00:37:52,120
building etcetera it is exceeded only in solitude and so on so that is rebusiveness as

296
00:37:52,120 --> 00:38:01,600
to outflow and then smearing at the time of using it he smears his hands lifts tongue

297
00:38:01,600 --> 00:38:08,040
and pellet and be become repulsive by being smeared with it and even when washed they have

298
00:38:08,040 --> 00:38:14,200
to be washed again and again in order to remove the smell that is very true when you eat

299
00:38:14,200 --> 00:38:21,960
with your hands sometimes the dishes may be smelly and the smell sticks to the fingers

300
00:38:21,960 --> 00:38:28,440
for a long time and just as when rises being boiled the husk the red powder covering the

301
00:38:28,440 --> 00:38:35,960
grain etcetera rice up and smearing mouth rim and lid of the cauldron so too when eaten

302
00:38:35,960 --> 00:38:40,880
it rises up during its cooking and simmering by the body that covers the whole body

303
00:38:40,880 --> 00:38:46,760
it turns into tada which may as the teeth and it turns into spittles from etcetera which

304
00:38:46,760 --> 00:38:52,280
respectively smears the tongue by lid etcetera and so on so on now about three or four lines

305
00:38:52,280 --> 00:38:59,560
down and after one has washed is setting one of these the head has to be washed again and

306
00:38:59,560 --> 00:39:07,240
there is number 17 right I think that number should be moved to one line down and after

307
00:39:07,240 --> 00:39:12,640
one has washed is setting one of these second setting one of these seven inch should be

308
00:39:12,640 --> 00:39:23,240
there and here the first setting one of these means what you call urinal orifice and the

309
00:39:23,240 --> 00:39:45,720
second one means the urinal orifice so this is according to out through smearing so we

310
00:39:45,720 --> 00:39:58,160
have got 10 ways of developing the perception of reposiveness and if you use reposiveness

311
00:39:58,160 --> 00:40:02,280
and this way in 10 aspects and strikes at it with thought and a five thought physical

312
00:40:02,280 --> 00:40:07,880
new treatment becomes evident to him in its repulsive aspect he cultivates that sign again and

313
00:40:07,880 --> 00:40:17,240
again now that sign in other meditations how many signs do we have in other types of meditations

314
00:40:17,240 --> 00:40:29,480
for example casino meditation you remember the signs yeah two signs what is the first sign

315
00:40:29,480 --> 00:40:40,120
it is translated in this book is learning sign that means cross sign actually and the other

316
00:40:40,120 --> 00:40:50,120
one is counterpart sign and Bali the first one is yoga honey beta and the second one is petiba

317
00:40:50,120 --> 00:40:59,480
honey beta so there is there are two kinds of signs for example if you practice casino disk

318
00:40:59,480 --> 00:41:09,400
meditation when you can when you have memorized the disk and when you can see in your mind with

319
00:41:09,400 --> 00:41:17,560
your eyes closed yet you can see the disk then you have said you have called the the first kind

320
00:41:17,560 --> 00:41:26,680
of sign learning sign or cross sign and later on you you concentrate on that sign again and again

321
00:41:27,880 --> 00:41:35,960
and so it becomes very refined and at the time you have said you have got the second kind of

322
00:41:37,000 --> 00:41:46,200
sign but sign counterpart sign so counterpart sign is much more refined than the learning sign

323
00:41:46,200 --> 00:41:55,480
so there are these two signs but here there is no such sign but the sign here simply means the

324
00:41:55,480 --> 00:42:02,040
object of meditation the the footnote also means it was but it is very difficult to understand

325
00:42:02,040 --> 00:42:13,080
in the footnote because it is a translation of the the sub commentary and also it's not so

326
00:42:13,080 --> 00:42:22,200
understandable so the sign here means just the object of meditation the object of meditation is what

327
00:42:22,200 --> 00:42:30,680
here the nutrient although we call it physical nutrient actually it is what it is the or

328
00:42:30,680 --> 00:42:46,120
nutritive essence in the food now nutritive essence in the food is baramata ultimate reality

329
00:42:46,120 --> 00:42:53,320
it is one of the 28 material properties but the the aspect of

330
00:42:53,320 --> 00:43:06,040
reposiveness is concept so through through contemplating on the reposiveness

331
00:43:07,720 --> 00:43:15,720
you take the the real thing as object and that real thing is called subhavadhamma

332
00:43:15,720 --> 00:43:23,960
dhamma that have individual essence and dhamma that have individual essence are by nature

333
00:43:26,200 --> 00:43:36,680
difficult to understand by nature profound since in this meditation the object is profound

334
00:43:36,680 --> 00:43:49,480
there can be no jana somebody reach only the access stage so you cannot get jana by practicing

335
00:43:49,480 --> 00:43:58,120
this kind of meditation because the the object is and that the object belong to the dhammas

336
00:43:58,120 --> 00:44:08,280
which have individual essence or in other words the object belongs belongs to the paramata

337
00:44:08,280 --> 00:44:16,360
the ultimate reality so it is difficult to to see it is profound so the jana cannot reach

338
00:44:16,360 --> 00:44:23,800
absorption in this kind of meditation it will reach only to the access stage

339
00:44:23,800 --> 00:44:30,040
so that maybe what is meant by the footnote 18

340
00:44:36,440 --> 00:44:49,880
he did not re translate the whole the whole passage and here he gives the his translation

341
00:44:49,880 --> 00:44:59,320
as the translation of the whole passage but some some lines are omitted here so it doesn't seem

342
00:44:59,320 --> 00:45:10,280
really connected and they did a further down that is an inaccurate in the translations so

343
00:45:10,280 --> 00:45:21,800
the sub commentary gives true true reasons for the jana cannot reach absorption in this kind of

344
00:45:21,800 --> 00:45:29,160
meditation the first is it is because the occurrence of development is contingent only upon

345
00:45:29,720 --> 00:45:39,160
dhammas with an individual essence that is because this meditation has to take the dhamma with

346
00:45:39,160 --> 00:45:46,840
individual essence as object and that dhamma with an individual absence individual essence

347
00:45:46,840 --> 00:45:54,200
is by nature performed by nature difficult to see that is why when the somebody cannot reach

348
00:45:54,200 --> 00:46:13,160
to the jana stage in this meditation through there is only x

