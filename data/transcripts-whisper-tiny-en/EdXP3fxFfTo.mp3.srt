1
00:00:00,000 --> 00:00:07,680
Good evening YouTube. We are now broadcasting YouTube live and we can

2
00:00:10,800 --> 00:00:12,800
link the hangout to our chat.

3
00:00:15,840 --> 00:00:21,920
So if anybody wants to join the hangout, come on and show their face you're welcome to.

4
00:00:21,920 --> 00:00:28,240
If you want to hide behind the text, you can do so.

5
00:00:36,720 --> 00:00:40,960
So I have that. Do you remember how I was talking about, was I ranting on here about this

6
00:00:40,960 --> 00:00:54,480
um, the Muslim group on campus, maybe not on here. I told you guys about this, this interface

7
00:00:55,440 --> 00:00:58,080
cafe. Are you talking about that? Yes.

8
00:00:59,200 --> 00:01:03,360
And now everyone, they didn't really seem all that interested in peace. It was more like our religion

9
00:01:03,360 --> 00:01:13,360
is great. And then in follow up to that, after Paris, um, the Muslim association on campus has

10
00:01:14,000 --> 00:01:20,560
really aggressively sought to appease people's or, or, you know, um,

11
00:01:20,560 --> 00:01:34,400
um, I would you call improve the people's opinion of Islam. So today after class,

12
00:01:35,120 --> 00:01:44,000
I was in the student center and I was meeting up with my friend and her friend. And because she was,

13
00:01:44,000 --> 00:01:56,960
she had had a breakdown. So no, I was there to talk to her. But, um, in the, in the entryway,

14
00:01:56,960 --> 00:02:03,680
they've got all these tables for all the clubs. And I've seen in the big area, somehow they got

15
00:02:03,680 --> 00:02:08,160
these, they were able to put up all these banners. And one of the big banners is this big black

16
00:02:08,160 --> 00:02:15,360
banner with red lettering. You know, I'm at a black banner with red lettering saying, uh,

17
00:02:15,360 --> 00:02:24,720
Islam, uh, our Islam and the West, uh, incompatible or something like that. And then there is Islam

18
00:02:24,720 --> 00:02:32,880
is about justice, about equality and equality. And that's it. And so I've been, I've been complaining

19
00:02:32,880 --> 00:02:41,200
about it, saying that why isn't it about peace? And if they're trying to give a positive,

20
00:02:41,200 --> 00:02:50,640
um, a positive image, why are they using black and red? You know, is, and, and why have they

21
00:02:50,640 --> 00:02:56,080
worried about Islam being compatible? I mean, the wording of it is really strange. I was my father,

22
00:02:56,080 --> 00:03:02,560
I was, I was telling my father about this. They're not saying peace. And, and I felt like they weren't

23
00:03:02,560 --> 00:03:06,960
at this interfaith cafe. They also weren't. They took them, I know, not at the end of it. There was

24
00:03:06,960 --> 00:03:15,280
a rally for Beirut because there was a bombing in, or there was something in Beirut. And, um,

25
00:03:15,280 --> 00:03:20,880
and Paris. So, and this guy stood out who I guess was a Muslim Muslim and he was talking about

26
00:03:20,880 --> 00:03:27,280
Islam. And he never really said peace. It was like he was beating around the bush. And so I get

27
00:03:27,280 --> 00:03:32,480
the feeling these people aren't interested in peace. Exactly. And so this guy in their entryway,

28
00:03:32,480 --> 00:03:36,560
this guy said, can they, can I take 10 seconds of your time? And I had no idea what he was. I didn't

29
00:03:36,560 --> 00:03:43,200
see his table or what he was holding. And I looked at them and I said, one, there's going to start

30
00:03:43,200 --> 00:03:47,200
counting. And then he starts talking. He, and then he pulls at this little thing, which is a

31
00:03:47,200 --> 00:03:52,400
smaller version that says, Islam in the West. And I started ranting. I said, look you.

32
00:03:52,400 --> 00:03:59,440
You're, this is not helping. And I said, why doesn't it say peace? And he said, oh, well, Islam

33
00:03:59,440 --> 00:04:04,480
means peace. Islam doesn't mean peace. I'm pretty sure Islam means surrender. I think, right? The

34
00:04:04,480 --> 00:04:11,200
word. And I said, I said, well, um, well, why doesn't it say that? Why, why are you concerned

35
00:04:11,200 --> 00:04:16,000
with Islam in the West? Why, if it said, I said, if it said Islam in the peace, I, I go. And he said,

36
00:04:16,000 --> 00:04:19,680
well, if you come, you'll feel very peaceful vibes. I said, we're looking at that. I don't feel

37
00:04:19,680 --> 00:04:24,240
peaceful vibes. It's black and red. If you were looking, and he actually greeted me, he said, yeah,

38
00:04:24,240 --> 00:04:29,520
well, that's true. Because I really gave him a piece of my mind. And I think I really got through

39
00:04:29,520 --> 00:04:34,320
to him whether it's going to make any influence or not. But I'm not impressed. And I told him

40
00:04:34,320 --> 00:04:39,440
that. I said, you guys, you guys don't have your heads on straight. And I think it's the

41
00:04:39,440 --> 00:04:43,040
Christians as well for the most part, don't have their head on straight. And people are not

42
00:04:43,040 --> 00:04:49,280
really interested in peace. And that's, that's shameful on both sides. You know, if you want to spread,

43
00:04:49,280 --> 00:04:56,080
if you want to improve the image of your religion, come on, say it, don't be afraid to say it.

44
00:04:56,080 --> 00:05:06,240
My religion is about peace. I believe in peace. And that's it. I, I denounce and these kind,

45
00:05:06,240 --> 00:05:09,840
I don't denounce any kind of violence, I think, but they don't. I think for the most part,

46
00:05:09,840 --> 00:05:15,440
they're happy about violence and they want violence. Now, maybe that's going to fire. But they,

47
00:05:15,440 --> 00:05:20,400
you know, there's, if someone wrongs you, you hurt them, that kind of thing, the eye for the

48
00:05:20,400 --> 00:05:26,480
eye and that kind of thing. So this, this thing says justice and equality. And I was thinking

49
00:05:26,480 --> 00:05:32,000
about that, you know, because justice, justice doesn't do it for me. Because everyone has

50
00:05:32,000 --> 00:05:36,480
their own idea of justice, right? If your justice is like the Old Testament, an eye for an eye,

51
00:05:36,480 --> 00:05:50,720
well, that makes everybody blind in one eye. And, and equality as well. I mean, if your religion

52
00:05:50,720 --> 00:05:57,200
teaches horrible things, which I don't know, I mean, I think many of these religions, Christianity,

53
00:05:57,200 --> 00:06:03,360
Judaism, Islam do, then, you know, equality is meaningless. If everyone has to follow horrible

54
00:06:03,360 --> 00:06:10,800
teachings equally, I guess they're addressing the idea that women are, are unequal in

55
00:06:14,160 --> 00:06:17,840
most of these religions. I don't know about Christianity, I can't remember, but

56
00:06:18,880 --> 00:06:25,680
pretty sure that in Judaism, it's been pretty unequal in the texts and pretty sure that in Islam

57
00:06:25,680 --> 00:06:32,480
is the same. I don't know. Either way, I mean, equality is, that's good in the sentence to not

58
00:06:32,480 --> 00:06:37,280
be prejudiced against people. My point is, they're missing the whole point. Here you've,

59
00:06:37,280 --> 00:06:43,280
people have been violent and awful in the name of your religion. And now you want to say

60
00:06:43,280 --> 00:06:49,440
is that it's compatible with the West. I mean, who cares? Are you against these bombings? Are

61
00:06:49,440 --> 00:06:54,480
you against these shootings? Are you, what did they do? Are you against these atrocities or not?

62
00:06:54,480 --> 00:06:58,720
That's what we want to know. Do you believe that hurting people is good? And they're not addressing

63
00:06:58,720 --> 00:07:02,880
it. And then they put up these black and red signs and you think these are terrorists. Who

64
00:07:02,880 --> 00:07:11,760
puts up a black and red sign? Death metal signs are black and red. It was the kind of

65
00:07:11,760 --> 00:07:18,560
colors that I would go for when I was having metal listening to Ozzie Osborne and going to

66
00:07:18,560 --> 00:07:25,920
black Sabbath concerts. Black piece. Oh, there's my rant for the day. But I felt like I

67
00:07:25,920 --> 00:07:30,080
had done something because I really felt, I finally got a chance to talk to someone about this.

68
00:07:30,080 --> 00:07:34,480
I've just been shaking my head at these people. There's no idea. PR. They've got a bad

69
00:07:34,480 --> 00:07:43,200
PR person, that's for sure. But I think it's more than that. I don't know. I'm really not impressed

70
00:07:43,200 --> 00:07:48,160
with Islam so far. I'm not impressed with many religions actually.

71
00:07:48,160 --> 00:08:00,000
So this, then we had a discussion about religion with Katya's her name. She said, would

72
00:08:00,000 --> 00:08:07,280
you say that all religions or religions have been the cause of most conflict in the world?

73
00:08:07,280 --> 00:08:16,320
We just got talking. That was my story for today.

74
00:08:20,560 --> 00:08:25,360
Oh, and we meditated in the atrium. That's the other thing. Just a short meditation. There's only

75
00:08:25,360 --> 00:08:36,960
three of us. But it was nice. The atrium's a nice place. We're going to do atrium on Monday.

76
00:08:37,920 --> 00:08:44,480
The gym on Wednesday and the student center on Friday. So we'll have three meditations

77
00:08:44,480 --> 00:09:03,680
for the rest of the year, hopefully. And did I mention the book thing? Last night we talked

78
00:09:03,680 --> 00:09:10,400
about the books, right? Yes. Briefly, yes. We're trying to figure it out. Everyone's all confused.

79
00:09:10,400 --> 00:09:15,280
I don't remember who did it last time, but I just assumed it would be easy. It's all as the publisher

80
00:09:15,280 --> 00:09:19,680
certainly knows if we can somehow get through to the publisher. He's ready to print them, I'm sure.

81
00:09:20,400 --> 00:09:28,480
As long as he's not dead. Everyone dies. We'll be dead. But otherwise, it's just a matter of everyone's

82
00:09:28,480 --> 00:09:32,640
confused wondering, oh, you want send us, send us it and we'll print it out and we'll be like, no,

83
00:09:32,640 --> 00:09:40,560
no, it's already there. It just has to be printed. Is it color? No, you don't understand.

84
00:09:40,560 --> 00:09:44,000
Ask me these questions. Just contact the publisher. He knows.

85
00:09:44,000 --> 00:09:48,400
So it's a reprint, really? Yeah, it's a reprint. It's like the third or fourth reprint.

86
00:09:50,880 --> 00:09:54,640
Just keep printing more. That's a good thing.

87
00:09:54,640 --> 00:10:02,960
Yeah, and he's a Buddhist. He's a student of Gary Bengoda,

88
00:10:02,960 --> 00:10:15,600
Katukurunde Nyanananda and Mitri Nisa. Well, a damage even hunger, I don't remember where he's from.

89
00:10:15,600 --> 00:10:25,280
But damage even in Nyanananda is two months. I don't even need a guy. Seems like it.

90
00:10:25,840 --> 00:10:30,800
Nyanananda, when you get the books back, if people want one, can they send a self-addressed

91
00:10:30,800 --> 00:10:38,880
amp envelope or something to get one? Yeah, that one's for me. If you send a self-addressed stamped

92
00:10:38,880 --> 00:10:49,680
envelope, I can send it back. It's not really necessary because you can download it, right? Yes.

93
00:10:50,880 --> 00:10:58,080
If you want the nice shiny copy, don't have one here. We can also.

94
00:11:02,320 --> 00:11:06,400
Questions? Do we have any questions? We have questions. Okay.

95
00:11:06,400 --> 00:11:12,400
In the Western world and Western ancient studies, there are a number of people that study

96
00:11:12,400 --> 00:11:17,760
and even fluently speak the dead language Latin. Are there any people today that still study

97
00:11:17,760 --> 00:11:23,840
Polly to the point that they can speak it? If so, how would one do this? Would you ever consider

98
00:11:23,840 --> 00:11:30,640
taking a large amount of time to do this? We talked about it. In Burma, they do it. I think

99
00:11:30,640 --> 00:11:39,600
Burma is probably the only place. Maybe Sri Lanka, but not really. Not really. But in Burma,

100
00:11:39,600 --> 00:11:44,320
they still do conversational Polly. In Thailand, they've started to try. These monks who have

101
00:11:44,960 --> 00:11:50,400
studied in Burma have made textbooks. I've got one of the textbooks. That's also in English.

102
00:11:50,400 --> 00:11:57,200
It's English, Thai, and Polly. It's conversational Polly. It's really neat, but we never

103
00:11:57,200 --> 00:12:03,040
get off the ground. I'd like to get some people to know Polly together and actually do this,

104
00:12:03,040 --> 00:12:10,640
learn it. The only time I really got close was many years ago. There was a Polly group and

105
00:12:11,840 --> 00:12:18,800
I think it's still going, I don't really involve. But what we did is we set up this

106
00:12:18,800 --> 00:12:29,920
kind of like a wiki. It was just a page that we can all edit. We would take turns writing sentences.

107
00:12:29,920 --> 00:12:35,840
I would say something and the next person would reply. We were speaking in Polly and we could

108
00:12:35,840 --> 00:12:40,720
correct each other's grammar and we could ask questions about grammar and so on. But it never took

109
00:12:40,720 --> 00:12:45,920
off because I wasn't really all that good. I was the only one who really had

110
00:12:45,920 --> 00:12:51,680
and there weren't many people interested. But of the three or four people interested, I was the

111
00:12:51,680 --> 00:12:59,040
only one who could even come close to forming a sentence. So we need people who really know

112
00:12:59,040 --> 00:13:06,160
the language. But that would be something. It was really neat to do, to be able to type to each

113
00:13:06,160 --> 00:13:12,320
other in Polly to have a conversation. It's easier when you type because you've got more time.

114
00:13:12,320 --> 00:13:17,280
Do I actually have to speak it? I don't talk. Whenever he meets Burmese people, he says something

115
00:13:17,280 --> 00:13:22,880
in Polly to them. And Sri Lankans as well, he's tried, but Sri Lankan monks had brought us

116
00:13:22,880 --> 00:13:28,960
Sri Lankan monk to him and he... but Sri Lankan monk had no idea what he would do with sake.

117
00:13:28,960 --> 00:13:38,960
Sri Lankan, they don't really do it anymore. Some do, but it's not so common.

118
00:13:45,680 --> 00:13:50,480
I continually have this happen where I cannot seem to focus on one thing with noting.

119
00:13:51,120 --> 00:13:55,600
I will note rising and then my attention will go to my feeling of sitting. So I'll say sitting.

120
00:13:55,600 --> 00:14:01,440
But then my attention will have gone elsewhere. So what do I do to stop focusing on noting

121
00:14:01,440 --> 00:14:08,080
and let myself just experience what happens and note when possible? Is this acceptable practice?

122
00:14:08,080 --> 00:14:12,560
Do you understand what I'm trying to say? I'll be unfamiliar to clarify if not. Thank you,

123
00:14:12,560 --> 00:14:19,440
Bhante. Yeah, I mean, if it's many things distracting you, then you would say distracted or even

124
00:14:19,440 --> 00:14:25,440
overwhelmed to succumb. When you feel overwhelmed, you can say overwhelmed. There's nothing wrong with

125
00:14:25,440 --> 00:14:31,520
that per se, but you do want to try to come back to your base, back to the stomach rather than jumping

126
00:14:32,960 --> 00:14:37,840
from one unit. You know, you do one and then if something drops that, you say that you

127
00:14:37,840 --> 00:14:42,960
acknowledge that. But when there's nothing, try to come back to the stomach to start over.

128
00:14:42,960 --> 00:14:50,160
I wouldn't worry too much about that. You're seeing impermanence suffering in non-self, so that's good.

129
00:14:51,360 --> 00:14:52,160
Oops, you don't let go.

130
00:15:01,760 --> 00:15:06,240
How does one know when they are experiencing nabana and not just filled with aversion to

131
00:15:06,240 --> 00:15:12,320
life's activities? You don't know when you're experiencing nabana. There's no knowing in the

132
00:15:12,320 --> 00:15:21,920
nabana. There was no worry about that. Don't worry about nabana. It's not something you can miss.

133
00:15:23,600 --> 00:15:30,000
It's not something you can mistake for anything else.

134
00:15:30,000 --> 00:15:41,200
I think we're all caught up with questions.

135
00:15:51,200 --> 00:15:59,520
Oh, I'm sorry. I read it wrong. I apologize. He meant nabana. How does one know when they're

136
00:15:59,520 --> 00:16:05,200
experiencing nabana and not just filled with aversion to life's activities? My apologies.

137
00:16:06,640 --> 00:16:12,240
When nabana is not a version, nabana is losing your desire. So when you find that you don't

138
00:16:12,240 --> 00:16:17,280
have desire for things that you use to desire, that's nabana. When you look at things that you

139
00:16:17,280 --> 00:16:30,560
normally desire, it's just not worth it. That's nabita. Nee means out or not. Bida, it comes from, what does

140
00:16:30,560 --> 00:16:35,680
it come from? I think from desire or something. It has something to do with your excitement.

141
00:16:35,680 --> 00:16:51,600
Nee means not excitement. I'm not excited. Vida, I don't know.

142
00:16:59,920 --> 00:17:04,080
Any words on the project in Tampa, Ponte? No, no, no, we're good.

143
00:17:04,080 --> 00:17:06,800
But they said Monday or Tuesday.

144
00:17:06,800 --> 00:17:35,200
Good one. Okay, good night then everyone.

145
00:17:36,800 --> 00:17:46,800
Thanks Robin for your help. Thank you, Bonte. Good night.

