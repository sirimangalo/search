So that was my arms round.
Not a usual arms round, I suppose.
And I don't always have people stop me.
I don't always have police officers stop me.
And so this was definitely unusual.
It's almost as if I planned it.
Having talked this morning about one of the things I'm
going on quote unquote afraid of being police officers.
And then right after that, being stopped by police officer.
But he was really nice.
He probably has heard from people.
Or he's probably himself.
And he claimed that there are other people wondering or concerned
about who I am.
But it's probably more him interested.
And maybe even suspicious.
But he was very friendly, overtly ostensibly.
How do you say on the outside?
He seemed friendly.
And he just wanted to know what it is that I'm carrying around.
And so I showed him my ops ball.
And he said he didn't have a problem with what I'm doing.
But he said I have to understand that people are going to be
interested and knows anything new when people become curious.
And so I don't think it's a big problem.
I'm really going to write this letter to the local newspaper.
Hopefully I'll write it this afternoon.
And then you'll get a chance to.
I'll read it to you this afternoon.
Why we do arms round?
Well, it's something that came from the Buddhist time.
But without dragging up the past or talking about the tradition of it,
there are some really obvious reasons as to why it's a useful thing
or a beneficial thing to do.
Obviously, the benefits to the monk himself is it should be quite obvious.
There's incredible benefits to the recipient.
I don't have to work for food or I don't have to make money.
I don't touch money.
I don't have to go cooking.
I don't have to keep food.
I mean, it seems a lot like taking advantage of people in that sense.
And a lot of people think of it like that.
I've heard people say that monks are lazy or so on.
And so if we're just asking, what are the benefits?
Why do you do that?
And obviously, I don't think there's any question as to why one would do it
if they were just concerned about their own benefit.
I think the benefits for the people who are giving is probably less clear,
especially for those people who aren't accustomed to giving something
on a daily basis or giving to people that they might not know.
But you really have to look at it in terms of being a religion.
Because we see, and all other religions, people give exorbitant amounts of money
to, or seemingly, exorbitant amounts of money to priests and ministers and so on,
without a second thought.
There's people who think that's wasteful as well.
But if you compare the two, the difference between giving 10% of your salary
to a priest or a minister, and just giving food,
like a portion of the food that you make for yourself anyway,
or in the case of restaurants, a portion of the food that they have on hand,
which really doesn't cost that much to them anyway,
it's really putting it on that sort of a scale,
it makes it seem like it's not really of any burden to the people.
On the other hand, it's in fact a wonderful thing for them to do.
It's charity, and supporting someone who you believe in.
If you believe in a monk, if you believe that they're doing something good,
then to give them something is a wonderful thing for you.
I've done it before myself.
I do it often when I see monks, I put food on my birthday every year.
I try to give arms to a group of monks.
It's something that makes you feel good, giving to people.
The Buddha himself was very clear about this.
He said the benefits of getting food are one day of food,
but the benefits of giving food are immeasurable.
Hundreds of thousands of times better because the benefit that you gain,
you keep with you and you keep it in your heart.
When you do something good, when you help someone,
when you give something, you get a much greater benefit
in the person who receives.
The arms are something that I've done for my whole monks life.
I've done it everywhere, I've done it in forests, I've done it in cities,
I've done it on a mountain, going up and down the mountain.
I've walked many, many miles each day,
even today walking in the sun.
This is, I think it's hard to say that monks are lazy,
considering the rigid set of rules that we have to abide by.
That's part of the reason why we abide by the rules
because we acknowledge that we don't want to be caught up in the world.
Yet, we have no right to expect anything.
We have no right to expect the luxuries of people who live in the world as a result.
Sometimes it looks like I'm living in luxury even as a monk,
so you have a computer and so on.
But there are so many rules that I have to abide by
that make it really impossible for me to become caught up
in these things or to become,
to live a luxurious life.
The clothes I own are the clothes that you see on me.
I don't have a second set.
The bed I sleep on is the floor.
The food I eat, I eat one meal a day.
I really think it's not much that you could point a finger at
to say that, oh, look at the monkey's getting some free food every day.
Because that's the only food I eat and it's been given out of faith.
It's been given purely with no desire for anything in return.
Simply for the sheer pleasure of being able to give something,
being able to help someone, being able to support them for another day.
The Buddha said it's like a honey bee that goes and pollinates the flower
and takes the pollen, harms neither themselves nor the flower.
The arms round is not something exorbitant where we're asking for anything huge or out of hand.
But it's enough to keep us alive and it's something that keeps other people spiritually alive.
It's something that's a support for their own spiritual development and a support for our spiritual development.
So that's in brief what it means to go on arms round.
I often have my students follow me on arms round so they can understand it and appreciate it much more.
I've had people from countries around the world follow me on arms round and literally break down and cry when they see the wonderful
heartfelt gesture given by people, sometimes people who don't have much at all.
I've been to places where people are themselves living in difficult situations
and yet they always give something, give a little bit, something that's the food that they give is not a lot.
The food that they have is not a lot but they want to share.
They feel great and they feel happy and they're smiling.
It's really a wonderful thing so I wouldn't change it.
I myself am a generous person, I don't feel ashamed to say that.
So I don't feel bad when I receive the gifts from other people because I myself give gifts.
I don't think there's anything.
I just somehow think that people often misunderstand or it's a foreign thing.
This is why I'm being stopped by the police.
So I think it's good to explain these sort of things.
I hope that helped to clear some things up and make it clear a little bit more, a little bit more clear about what I'm doing and how I live my life.
So that's the latest in this life in the day of a month.
