1
00:00:00,000 --> 00:00:02,000
You

2
00:00:30,000 --> 00:00:49,840
Good evening, everyone. Welcome to our live broadcast.

3
00:01:00,000 --> 00:01:14,480
Today, we're looking at the Pasui hara suta.

4
00:01:14,480 --> 00:01:25,480
Pasui hara, Pasu. I put the title of this one as harmony, but that's not quite what it is.

5
00:01:25,480 --> 00:01:32,480
Pasui hara, we hara, we know this word. It means the dwelling.

6
00:01:32,480 --> 00:01:37,680
So we hara means dwelling. It could be a place or it could be a state.

7
00:01:37,680 --> 00:01:41,160
When you dwell in some, when you live in a certain way, that's called a

8
00:01:41,160 --> 00:01:47,320
wehara. When you live in a certain place, the place is called a wehara.

9
00:01:47,320 --> 00:01:58,960
We hara means if it's a place where you live or a living, a way of living or a place of living.

10
00:01:58,960 --> 00:02:07,360
Pasu, Pasu means comfortable or at ease.

11
00:02:07,360 --> 00:02:13,760
And it's used, I don't know if it's used in other ways, but in the dhamma, it's most often used

12
00:02:13,760 --> 00:02:24,760
to refer to a community that lives in ease. And so it's a quality of a community that is harmonious.

13
00:02:24,760 --> 00:02:31,240
So this is the idea of harmony. It involves the idea of harmony now.

14
00:02:31,240 --> 00:02:37,200
We're going to see these five again, I think, in the uncuturnic eye book of sixes.

15
00:02:37,200 --> 00:02:44,160
I know they're elsewhere, but I think they're assuming they're also in the book of sixes.

16
00:02:44,160 --> 00:02:52,000
It's going to add one. So we'll revisit these and we'll add one more quality.

17
00:02:52,000 --> 00:02:58,040
But these are the five qualities, and it's given a different name, but it's the same sort of idea.

18
00:02:58,040 --> 00:03:06,280
So the idea here is that these five are ways of dwelling with living with other people,

19
00:03:06,280 --> 00:03:15,400
living at ease in a community with relations.

20
00:03:15,400 --> 00:03:23,080
A way of living with other people at ease, at ease with others, and having healthy relationships.

21
00:03:23,080 --> 00:03:33,560
It's really what it's all about. And so one way of looking, this is a way to create communal harmony,

22
00:03:33,560 --> 00:03:39,160
create harmony in a community. And that's a good way of looking at it.

23
00:03:39,160 --> 00:03:46,680
It's important that we are mindful of our place in a community,

24
00:03:46,680 --> 00:03:53,080
and our place with other people, our roles, and our attitudes, and our involvement with other.

25
00:03:53,080 --> 00:04:00,040
And it'd be healthy that we cultivate wholesomely.

26
00:04:00,040 --> 00:04:06,680
Because relationships can be an incredible boon, support for the spiritual life,

27
00:04:06,680 --> 00:04:13,640
but they can, of course, also be a real drag on the spiritual life.

28
00:04:13,640 --> 00:04:20,760
So the last thing you want is to get stuck in a situation where you're with people who are dragging you down,

29
00:04:20,760 --> 00:04:28,280
who are preventing you from cultivating wholesome qualities, because life is short.

30
00:04:28,280 --> 00:04:33,640
And we want to make the best of it, the most of it,

31
00:04:33,640 --> 00:04:43,480
before we have to leave, have to slow off this mortal coil from there.

32
00:04:47,480 --> 00:04:52,040
So we have to be mindful of our living situation.

33
00:04:52,040 --> 00:04:59,400
Living at ease is an important quality. You don't want to be worried about your living situation,

34
00:04:59,400 --> 00:05:03,800
because it takes energy away from your practice.

35
00:05:03,800 --> 00:05:12,360
So the other way of looking at these is, as it means determining whether a community or a situation

36
00:05:12,360 --> 00:05:21,720
is beneficial for your practice. Sometimes leaving is preferable.

37
00:05:21,720 --> 00:05:28,200
Sometimes even the Buddha, the monks were fighting the Buddha up and left.

38
00:05:28,200 --> 00:05:34,280
You tried to talk to them. They wouldn't listen, so they just left.

39
00:05:38,600 --> 00:05:47,560
Sometimes when you have to see to your well, your own welfare,

40
00:05:47,560 --> 00:05:52,280
if you want to help others, even if you want to help others,

41
00:05:52,280 --> 00:06:04,840
you yourself have to be well. You yourself have to become favorable if you're in a situation that...

42
00:06:04,840 --> 00:06:13,000
I was talking to someone this morning about this, and here's how I put it.

43
00:06:13,000 --> 00:06:19,000
If you're living in a situation that is painful, if there's a lot of suffering in your living situation,

44
00:06:19,000 --> 00:06:27,800
that's not a problem in Buddhism. When those of us have taken up meditation could understand this,

45
00:06:27,800 --> 00:06:33,800
that pain isn't really a problem. Even hard work isn't really a problem.

46
00:06:33,800 --> 00:06:45,960
It can be problematic as it, it taxes your energy and so on, but it's not a reason necessarily to leave.

47
00:06:45,960 --> 00:06:51,640
But if you're in a situation with evil, evil and suffering are two very different things.

48
00:06:52,440 --> 00:06:57,560
So evil means if you're in a situation with other people who are abusive or

49
00:06:57,560 --> 00:07:11,320
immoral, alcoholic, strong addicts, this isn't just suffering anymore.

50
00:07:11,320 --> 00:07:16,360
So you say, well, I suffer with it or I live with it, but when other people who are engaging in

51
00:07:16,360 --> 00:07:25,560
evil deeds, it's more than just suffering. You may not engage in the same activities that they do,

52
00:07:25,560 --> 00:07:33,000
but evil cultivates evil nonetheless. So when a person is, say, a drug, a dick, or an alcoholic,

53
00:07:33,560 --> 00:07:38,680
so you may not engage in drugs and alcohol, but then you say, well, I'm not doing it, so I can live

54
00:07:38,680 --> 00:07:43,320
with them. But you see, the problem with these things is it makes them unmindful, so they will

55
00:07:43,320 --> 00:07:53,800
behave in ways and speak in ways that will instigate the violence in you. You'll get frustrated

56
00:07:53,800 --> 00:07:57,880
with them, you'll get upset at them, you'll get stressed, you'll get worried, you'll get afraid,

57
00:07:59,960 --> 00:08:04,760
which are all evil. Those are all other types of evil. They're evil in the sense that they cause suffering

58
00:08:04,760 --> 00:08:13,400
and they cultivate into bad habits. They become causes for you to do evil thing, to do bad things,

59
00:08:13,400 --> 00:08:20,360
maybe not take drugs or alcohol, but to say bad things to others, to do bad things,

60
00:08:20,360 --> 00:08:29,640
to do things that are regret, to evil. Evil can only come of evil. The only thing that can come

61
00:08:29,640 --> 00:08:39,400
of evil is more evil. So in those cases, it's often better to find another living situation.

62
00:08:40,440 --> 00:08:47,080
If you can't move these people to change their ways, then

63
00:08:47,080 --> 00:08:54,440
it's not just a matter of bearing with it. It's not like bearing with insects or

64
00:08:56,440 --> 00:09:03,960
bearing with heat or cold or pain or sickness. You're bearing with evil is much different.

65
00:09:06,040 --> 00:09:13,320
Because of how it affects you, but also because of not being a good situation. If you stay in a

66
00:09:13,320 --> 00:09:21,000
living situation where others are constantly engaged in evil, you're a part of it.

67
00:09:22,520 --> 00:09:27,640
You're staying is a support for them to keep doing what they're doing.

68
00:09:29,400 --> 00:09:35,080
If they are abusive towards you or unpleasant towards you, then you're just helping them.

69
00:09:37,880 --> 00:09:38,760
Involved in it.

70
00:09:38,760 --> 00:09:49,320
So you can stay completely mindful if you're an arrow hunt and not to be affected by it,

71
00:09:49,320 --> 00:09:54,600
but we have stories of our hunts when they found because this arrow hunt to realize the

72
00:09:54,600 --> 00:09:59,400
monk was living with was very jealous of him. So just up and left,

73
00:10:00,920 --> 00:10:06,280
it's a good story actually. This monk was afraid that this new monk looked so

74
00:10:06,280 --> 00:10:13,240
spiritually advanced and he was afraid he was going to lose his standing in this village,

75
00:10:13,240 --> 00:10:21,640
because everyone respected him and he got very jealous and greedy. He threw out this monk's

76
00:10:21,640 --> 00:10:26,200
food. In the morning, he said, okay, I'll ring the bell when we go for arms. In the morning,

77
00:10:26,840 --> 00:10:33,080
he tapped the bell with his fingernail. Then he went into the village and the villager said,

78
00:10:33,080 --> 00:10:39,080
where's that other monk that just came to visit? He said, oh, I don't know. I rang the bell and he

79
00:10:39,080 --> 00:10:48,760
didn't come. He must be sleeping in. So they said, oh, well, we'll give you two portions of

80
00:10:48,760 --> 00:10:54,280
food, then bring one back for him. He ate the first portion and then they gave him a second

81
00:10:54,280 --> 00:10:58,440
portion to bring back. He looked at the food and he said, this is really good food. If he sees

82
00:10:58,440 --> 00:11:02,760
this, he won't want to go anywhere and I can't have this monk staying here. So he'd actually

83
00:11:02,760 --> 00:11:12,920
dumped out with this other monk's food and went into a terrible deed as a result.

84
00:11:14,920 --> 00:11:19,720
In the hour hunt, the hour hunt in the morning got a sense of what was going on and he just

85
00:11:19,720 --> 00:11:29,000
left, just walked out. There's definitely, I mean, because people will say, well, as Buddhists,

86
00:11:29,000 --> 00:11:35,640
you have to just bear with it and be patient with it. But patience with evil is a touch and

87
00:11:35,640 --> 00:11:43,480
go thing. It's not something you should stick with, even if you're an hour hunt. Of course, you

88
00:11:43,480 --> 00:11:49,240
should be an hour hunt. It wouldn't have to tell you that. It's a great wisdom and understanding, but

89
00:11:49,240 --> 00:11:54,120
right and wrong. Okay, without further ado, let's get into what are these five things. What do you

90
00:11:54,120 --> 00:12:05,400
need? The first three are simple. So we have the Pali, Ida bikou no mehtangayakamang pajupati

91
00:12:05,400 --> 00:12:21,160
sabraamachari su avi jayvara hucha. Does that mean here monks buy a monk? No, not buy me a

92
00:12:21,160 --> 00:12:43,000
monk. Mehtangayakamang, the acts of, well, bodily acts of mehtang, are established. That's not it.

93
00:12:43,000 --> 00:12:59,800
By that monk, bodily activity, maybe, is established. Love with loving kindness. A bikou

94
00:12:59,800 --> 00:13:07,880
maintains bodily acts of loving kindness, works as well. A kabody makes an act. Towards his fellow,

95
00:13:07,880 --> 00:13:19,000
his or her fellow practitioners in the spiritual life, sabraamachari su. Brahmachari is the

96
00:13:20,760 --> 00:13:28,680
holy life. And sabraamachari is those who are living the holy life together with one. A

97
00:13:28,680 --> 00:13:39,000
bikou jayvara hucha in public and in private. So does this mean means the things you do,

98
00:13:39,000 --> 00:13:48,920
you act out of love both to their face and behind their backs. You don't smile to their face and

99
00:13:48,920 --> 00:13:59,960
then stab them in the back. And likewise, with speech, wa ji kamang and manu kamang. Mehtang kamang

100
00:13:59,960 --> 00:14:09,720
bajupatitamang. A bikou jayvara hucha in public and in private. Your speech towards that person

101
00:14:09,720 --> 00:14:16,120
and about that person and your thoughts. When with that person and your thoughts, when without

102
00:14:16,120 --> 00:14:24,280
that person, it should be filled with love, with friendliness. Mehta really means friendliness.

103
00:14:25,080 --> 00:14:30,600
You should have a friendly kind behavior, kind attitude, kind speech.

104
00:14:34,040 --> 00:14:42,280
Quite simple note, Mehta is the king. Mehta is a big part of communal harmony. Mehta can change,

105
00:14:42,280 --> 00:14:49,560
friendliness can change a situation, which is why it's a power that meditators have.

106
00:14:50,600 --> 00:14:56,760
The ability to change your mind and the power to say, I'm going to change my beat my attitude.

107
00:14:58,120 --> 00:15:04,440
Because you have a sort of a presence of mind and a centeredness of balance through the practice

108
00:15:04,440 --> 00:15:10,120
of mindfulness. Things like friendliness come easy and you can actually consciously cultivate it.

109
00:15:10,120 --> 00:15:14,440
You can say, look, I have anger towards this person, but that's just my anger.

110
00:15:15,720 --> 00:15:23,400
If I cultivate love, that will change. So you cultivate it with your actions, speech and thoughts.

111
00:15:23,400 --> 00:15:28,760
When you sit down and meditate, you take some time out of your insight meditation to

112
00:15:30,600 --> 00:15:32,760
send good thoughts to the people you live with.

113
00:15:32,760 --> 00:15:45,720
That's one, two, and three. Number four is, before and five are different and they're equally

114
00:15:45,720 --> 00:15:48,200
important. It's important that we get this straight.

115
00:15:48,200 --> 00:16:01,200
Nitaani si Helanne acandani a cnaany ah sambhamali a kamas ani boo jizanee tush.

116
00:16:01,200 --> 00:16:09,200
When you passatani, aparamatani, samadhi samatani, samadhi samadhi kani,

117
00:16:09,200 --> 00:16:12,700
it's a lot of the description of Sila.

118
00:16:12,700 --> 00:16:23,700
So one dwells in common, where is the tatarupi.

119
00:16:23,700 --> 00:16:28,700
So the tatarupi is to see the samanyagato.

120
00:16:28,700 --> 00:16:31,900
We had it.

121
00:16:31,900 --> 00:16:39,200
Sila samanyagato is an interesting word, and it's an interesting encounter with this word.

122
00:16:39,200 --> 00:16:50,900
I was in a community where I began to act in a...

123
00:16:50,900 --> 00:16:58,600
I began to keep a precept that the other monks in the community were not keeping.

124
00:16:58,600 --> 00:17:00,300
That's not true.

125
00:17:00,300 --> 00:17:06,300
I had gone elsewhere, and I had come to this community, and it turned out that I kept a rule that they didn't keep.

126
00:17:06,300 --> 00:17:11,100
And so they had a meeting about it, not directly.

127
00:17:11,100 --> 00:17:12,900
Everything in Thailand is all indirect.

128
00:17:12,900 --> 00:17:16,300
There's no one ever called me out on it.

129
00:17:16,300 --> 00:17:20,600
But they had a meeting to talk about harmony.

130
00:17:20,600 --> 00:17:27,500
And one of the monks says, yes, well, we need to have Sila samanyagato.

131
00:17:27,500 --> 00:17:34,900
Sila samanyagato means similarity.

132
00:17:34,900 --> 00:17:36,400
And so they quoted this teaching.

133
00:17:36,400 --> 00:17:42,500
This is the very teaching, or probably they took the sixfold version, the six part version.

134
00:17:42,500 --> 00:17:52,200
But they pulled out this one point and said, you know, so if we're all keeping the precepts in a certain way,

135
00:17:52,200 --> 00:17:58,200
then everyone has to keep them that way.

136
00:17:58,200 --> 00:18:02,200
I didn't get a chance, you know, because there was never at any point where I was called out,

137
00:18:02,200 --> 00:18:14,200
but it felt quite clear that it was because I had gotten some heat for doing this.

138
00:18:14,200 --> 00:18:19,200
But I bring it up because I want to be clear about what this means.

139
00:18:19,200 --> 00:18:25,200
It just doesn't mean that you have to match your behavior to those around you.

140
00:18:25,200 --> 00:18:27,200
And that's where all these adjectives come in.

141
00:18:27,200 --> 00:18:28,700
So let's look at the adjectives.

142
00:18:28,700 --> 00:18:39,200
Sila samanyagato means one's ethical qualities are the same as one's

143
00:18:39,200 --> 00:18:41,200
one's fellow residence.

144
00:18:41,200 --> 00:18:44,200
But let's look at the whole quote.

145
00:18:44,200 --> 00:18:46,200
So it was openly and privately, R.E.J.

146
00:18:46,200 --> 00:18:51,200
possessing and common with this fellow monk's virtuous behavior.

147
00:18:51,200 --> 00:18:54,200
So common virtuous behavior.

148
00:18:54,200 --> 00:18:57,200
But there's a key here.

149
00:18:57,200 --> 00:19:00,200
Because you can ask the question before we even go into it.

150
00:19:00,200 --> 00:19:03,200
Well, what about thieves?

151
00:19:03,200 --> 00:19:06,200
You can say, okay, well, sure, thieves can get along.

152
00:19:06,200 --> 00:19:11,200
But does that mean that the way to create communal harmonies to break precepts?

153
00:19:11,200 --> 00:19:14,200
If other people are breaking the means, if everyone else is drinking alcohol,

154
00:19:14,200 --> 00:19:19,200
you should, and that's how you create living in ease.

155
00:19:19,200 --> 00:19:20,200
You could argue that.

156
00:19:20,200 --> 00:19:24,200
And so you could argue as these monks did that for all breaking the precepts.

157
00:19:24,200 --> 00:19:27,200
And that leads to communal harmony.

158
00:19:27,200 --> 00:19:29,200
Except it doesn't.

159
00:19:29,200 --> 00:19:30,200
And that's the problem.

160
00:19:30,200 --> 00:19:35,200
One of the big reasons why I started keeping fairly strict precepts is because I saw

161
00:19:35,200 --> 00:19:40,200
how certain precepts, when broken, destroyed communities.

162
00:19:40,200 --> 00:19:43,200
That's why the Buddha instated them.

163
00:19:43,200 --> 00:19:46,200
There's many precepts that are mainly not entirely,

164
00:19:46,200 --> 00:19:52,200
but mainly for the purpose of cultivating communal harmony.

165
00:19:52,200 --> 00:20:00,200
And so what the Buddha says here is you have common virtuous behavior that is unbroken,

166
00:20:00,200 --> 00:20:06,200
flawless, unblemished, unblaged, liberating, praised by the wise,

167
00:20:06,200 --> 00:20:12,200
unclinging, and leading to concentration.

168
00:20:12,200 --> 00:20:14,200
It's a tall order, really.

169
00:20:14,200 --> 00:20:16,200
But we can draw something from this.

170
00:20:16,200 --> 00:20:20,200
That morality is important.

171
00:20:20,200 --> 00:20:32,200
And if someone is engaging in a breach of whatever type of morality that the community is cultivating.

172
00:20:32,200 --> 00:20:36,200
And by that, I mean the five precepts are the eight precepts,

173
00:20:36,200 --> 00:20:41,200
or the ten precepts, or the many, many monastic precepts.

174
00:20:41,200 --> 00:20:45,200
But at least a basic set of morality, right?

175
00:20:45,200 --> 00:20:49,200
When people start breaking, the community, the mental harmony breaks down.

176
00:20:49,200 --> 00:20:52,200
So obviously if everyone's stealing from each other,

177
00:20:52,200 --> 00:20:59,200
that doesn't create communal harmony just because you have the same view and grasp of ethics.

178
00:20:59,200 --> 00:21:07,200
That's a good behavior when everybody keeps it, creates communal harmony.

179
00:21:07,200 --> 00:21:13,200
But it was quite specific here that it's not just having the same as it were behaviors.

180
00:21:13,200 --> 00:21:18,200
And everyone has actually behavior that is ethical.

181
00:21:18,200 --> 00:21:20,200
That leads one to duality.

182
00:21:20,200 --> 00:21:25,200
If you've got a thief among, thief in your midst, we had one monk once in a place where I stayed,

183
00:21:25,200 --> 00:21:26,200
and he was a thief.

184
00:21:26,200 --> 00:21:28,200
He wasn't a monk.

185
00:21:28,200 --> 00:21:30,200
He certainly wasn't.

186
00:21:30,200 --> 00:21:33,200
I mean, honestly, I don't have any proof that he was a thief,

187
00:21:33,200 --> 00:21:38,200
but just everyone was saying that he was stealing this, stealing that, they saw him steal.

188
00:21:38,200 --> 00:21:40,200
So it's all just hearsay to me.

189
00:21:40,200 --> 00:21:49,200
Man, he was a cleptomaniac.

190
00:21:49,200 --> 00:21:52,200
And it tore up the community.

191
00:21:52,200 --> 00:21:56,200
Everyone was very angry all the time and worried all the time.

192
00:21:56,200 --> 00:22:02,200
The novice monks, their money just disappeared and that kind of thing.

193
00:22:02,200 --> 00:22:05,200
Not good.

194
00:22:05,200 --> 00:22:08,200
No, it's that number four.

195
00:22:08,200 --> 00:22:11,200
And number five is the same, but with views.

196
00:22:11,200 --> 00:22:17,200
So one has the same views as one's fellow monks.

197
00:22:17,200 --> 00:22:19,200
That's important.

198
00:22:19,200 --> 00:22:24,200
One's fellow meditators say one's fellow religious people.

199
00:22:24,200 --> 00:22:26,200
But it's also not the case.

200
00:22:26,200 --> 00:22:31,200
If you all have views, there's no benefit in helping others.

201
00:22:31,200 --> 00:22:37,200
If there's no wrong in hurting others, it's a bad view that's not going to lead to harmony.

202
00:22:37,200 --> 00:22:43,200
If you have the view that there's no harm in clinging and no harm in addiction,

203
00:22:43,200 --> 00:22:48,200
then everyone is always striving after their own greedy ambitions.

204
00:22:48,200 --> 00:22:52,200
And that doesn't lead to communal harmony.

205
00:22:52,200 --> 00:22:58,200
But right view, having right view, and cultivating right view.

206
00:22:58,200 --> 00:23:02,200
Yes, you have to have views in common with your fellows.

207
00:23:02,200 --> 00:23:14,200
And in a lot of ways that's equally important in regards to simple matters of the approach to the spiritual life.

208
00:23:14,200 --> 00:23:24,200
If everyone, so to take an example from a monastic community, if everyone is practicing certain special practices,

209
00:23:24,200 --> 00:23:31,200
like everyone's all decided to only eat from their arms bowl, or only eat one meal a day,

210
00:23:31,200 --> 00:23:35,200
these kind of things create communal harmony if everyone does it.

211
00:23:35,200 --> 00:23:39,200
But if someone doesn't, because there are different approaches to the monastic life,

212
00:23:39,200 --> 00:23:50,200
and if everyone does in different ways, you could argue that that may take away from communal harmony.

213
00:23:50,200 --> 00:23:57,200
Or there's certainly the Agen-Cha monasteries and the Dhamma Yudh monasteries really.

214
00:23:57,200 --> 00:23:59,200
They have this down good.

215
00:23:59,200 --> 00:24:02,200
Everyone of their monasteries doesn't a similar way.

216
00:24:02,200 --> 00:24:07,200
And every monk in the monastery eats food at the same time in a similar way.

217
00:24:07,200 --> 00:24:10,200
And you've got to admit, there's some power to that.

218
00:24:10,200 --> 00:24:12,200
But it's not that important.

219
00:24:12,200 --> 00:24:14,200
I don't really subscribe to that.

220
00:24:14,200 --> 00:24:19,200
And I generally, of the opinion, let everyone do the way they're going to do something.

221
00:24:19,200 --> 00:24:21,200
I think that was common in the Buddhist time as well.

222
00:24:21,200 --> 00:24:23,200
Different monks would practice in different ways.

223
00:24:23,200 --> 00:24:31,200
So I think it's important that we single out the views that were clear that everyone has right view,

224
00:24:31,200 --> 00:24:35,200
because people with wrong view disturb the community.

225
00:24:35,200 --> 00:24:38,200
It's hard to live in ease when you're with people.

226
00:24:38,200 --> 00:24:45,200
And so the greater point is that if there are members of the community,

227
00:24:45,200 --> 00:24:49,200
or people you live with, who are not following these five,

228
00:24:49,200 --> 00:24:57,200
this is what destroys the community, which makes it uncomfortable

229
00:24:57,200 --> 00:25:01,200
and unsuitable to live in that community.

230
00:25:01,200 --> 00:25:07,200
So we have cases where the Buddha left, where our hands would just leave.

231
00:25:07,200 --> 00:25:15,200
And you have people with wrong view, people with unethical behavior.

232
00:25:15,200 --> 00:25:20,200
People who are just unfriendly, people that are mean and nasty to live with.

233
00:25:20,200 --> 00:25:29,200
It's not a wholesome place, not conducive to meditation or mental development or goodness in general.

234
00:25:29,200 --> 00:25:33,200
So there you go, these five, good to remember.

235
00:25:33,200 --> 00:25:37,200
It's a good to remember for us that these are things we have to cultivate.

236
00:25:37,200 --> 00:25:45,200
But also good to keep in mind as things that you want to look for in the community.

237
00:25:45,200 --> 00:25:54,200
You want to look at your own living situation and decide whether it's to your benefit to stay or to go.

238
00:25:54,200 --> 00:25:56,200
So there you go.

239
00:25:56,200 --> 00:26:04,200
A little bit of dhamma.

240
00:26:04,200 --> 00:26:15,200
Now, if you will, if there are any questions, we can do some Q&A.

241
00:26:15,200 --> 00:26:23,200
I don't hear you, Robin.

242
00:26:23,200 --> 00:26:27,200
How's that, Bhante?

243
00:26:27,200 --> 00:26:28,200
Hello, Venerable Bhante.

244
00:26:28,200 --> 00:26:32,200
My question is about tenseness in the abdomen during walking.

245
00:26:32,200 --> 00:26:37,200
In walking meditation, I'll notice that the stomach is tense, so I loosen up.

246
00:26:37,200 --> 00:26:41,200
And when returning the mind to the walking, the stomach is tense again.

247
00:26:41,200 --> 00:26:44,200
Is tenseness related to clinging to something?

248
00:26:44,200 --> 00:26:47,200
Is there underlying fear that I need to work on?

249
00:26:47,200 --> 00:26:50,200
Or is there something else that results in the tenseness?

250
00:26:50,200 --> 00:26:54,200
It should be a goal to be as loose as possible in the body as I walk.

251
00:26:54,200 --> 00:26:55,200
Is this right?

252
00:26:55,200 --> 00:26:58,200
I should be releasing tension as I notice it.

253
00:26:58,200 --> 00:26:59,200
Is this right?

254
00:26:59,200 --> 00:27:03,200
Is tenseness something that will cause one to not go very deep?

255
00:27:03,200 --> 00:27:08,200
Should I include the abdomen in my mindfulness as I walk to ensure that it is relaxed?

256
00:27:08,200 --> 00:27:10,200
Thank you, Bhante.

257
00:27:10,200 --> 00:27:13,200
Anything, Robin?

258
00:27:13,200 --> 00:27:21,200
Well, you don't have control over whether you're tense or not, and when you're walking,

259
00:27:21,200 --> 00:27:24,200
more you shouldn't even try to control the tenseness.

260
00:27:24,200 --> 00:27:27,200
I go with that because you can try it.

261
00:27:27,200 --> 00:27:30,200
And you can, the mind can signal as he's doing.

262
00:27:30,200 --> 00:27:33,200
He actually is able to, to some extent, control.

263
00:27:33,200 --> 00:27:36,200
But I think the point is that's not the way we do things.

264
00:27:36,200 --> 00:27:43,200
So that part where I can answer right away, no, it's not, that's an important aspect.

265
00:27:43,200 --> 00:27:50,200
No, it's not, it's not the goal to be as loose as possible when you walk.

266
00:27:50,200 --> 00:27:52,200
Sorry, I had to drop to do what else we get.

267
00:27:52,200 --> 00:27:55,200
Oh no, that's, that's fine.

268
00:27:55,200 --> 00:28:01,200
And it's just going to say, you know, during walking, even though there are other things going on,

269
00:28:01,200 --> 00:28:06,200
we are trying to kind of stay with the feet and so.

270
00:28:06,200 --> 00:28:10,200
In this case, I would recommend stopping, don't do it while you're walking.

271
00:28:10,200 --> 00:28:13,200
So yeah, he's, he's asking that, don't do it while you're walking.

272
00:28:13,200 --> 00:28:18,200
Stop walking, even say to yourself, stop being, stop being.

273
00:28:18,200 --> 00:28:21,200
And then with your feet together, then focus on the stomach.

274
00:28:21,200 --> 00:28:22,200
But you're not trying to listen it.

275
00:28:22,200 --> 00:28:27,200
Then you would say to yourself, tens, tens, not trying to listen it.

276
00:28:27,200 --> 00:28:30,200
Because no tension isn't indicative of anything.

277
00:28:30,200 --> 00:28:33,200
It doesn't matter where it came from at all.

278
00:28:33,200 --> 00:28:37,200
It's important that you just see it as tension and so that you don't start to question,

279
00:28:37,200 --> 00:28:38,200
what does it mean?

280
00:28:38,200 --> 00:28:39,200
What could it mean?

281
00:28:39,200 --> 00:28:40,200
What does it signify?

282
00:28:40,200 --> 00:28:42,200
It doesn't signify anything.

283
00:28:42,200 --> 00:28:44,200
Signifies tension.

284
00:28:44,200 --> 00:28:49,200
That's the key to the meditation practice.

285
00:28:49,200 --> 00:28:56,200
See it as it is.

286
00:28:56,200 --> 00:28:59,200
Of course, if you're worried about it or concerned about it, that's something else.

287
00:28:59,200 --> 00:29:04,200
Don't worry, we're worried or concerned.

288
00:29:04,200 --> 00:29:07,200
Hello, Bantay.

289
00:29:07,200 --> 00:29:14,200
I'm your answers that meditation for beginners should probably not be a comfortable thing.

290
00:29:14,200 --> 00:29:21,200
Walking meditation seems to be maybe too easy to accomplish as opposed to sitting meditation,

291
00:29:21,200 --> 00:29:25,200
at which I'm still at my diapers.

292
00:29:25,200 --> 00:29:32,200
Any suggestions? It's not something that arise as an easiness of accomplishing walking meditation while doing it.

293
00:29:32,200 --> 00:29:35,200
Just that sitting is way harder than walking.

294
00:29:35,200 --> 00:29:41,200
That might change.

295
00:29:41,200 --> 00:29:46,200
And easy may be sort of all in how you look at it.

296
00:29:46,200 --> 00:29:50,200
So the question is during walking is your mind with your foot a hundred percent of the time.

297
00:29:50,200 --> 00:29:56,200
If your mind is not with the foot a hundred percent of the time, then it still needs work.

298
00:29:56,200 --> 00:30:01,200
But the other thing is we have a more detailed what walking step which can help.

299
00:30:01,200 --> 00:30:04,200
Stepping right, stepping left is pretty basic.

300
00:30:04,200 --> 00:30:09,200
There are some traditions that start you off of the third walking step lifting, moving place.

301
00:30:09,200 --> 00:30:10,200
We don't do that.

302
00:30:10,200 --> 00:30:15,200
We start you at stepping right, stepping left, and then as you go through the course,

303
00:30:15,200 --> 00:30:19,200
we will, the teacher will give you a higher walking step.

304
00:30:19,200 --> 00:30:22,200
So if had some people actually decide they're going to do higher walking steps,

305
00:30:22,200 --> 00:30:29,200
which is theoretically fine, it's just a problem when it comes to me actually trying to lead them through the course.

306
00:30:29,200 --> 00:30:36,200
The best way is if you can hold off and try and schedule a time to meet with me once a week.

307
00:30:36,200 --> 00:30:39,200
And I can give it to you properly.

308
00:30:39,200 --> 00:30:44,200
Once you get all the steps, you can choose which step you want to do.

309
00:30:44,200 --> 00:30:50,200
Third step, fourth step, fifth step, sixth step.

310
00:30:50,200 --> 00:30:54,200
The walking meditation is, I think, quite valid.

311
00:30:54,200 --> 00:30:59,200
It's about cultivating clarity of mind.

312
00:30:59,200 --> 00:31:05,200
It's also good for teaching you how to be mindful when you move.

313
00:31:05,200 --> 00:31:11,200
But it changes from day to day, week to week, month to month.

314
00:31:11,200 --> 00:31:15,200
Walking and sitting sometimes, one is easy, the other is difficult.

315
00:31:15,200 --> 00:31:17,200
Sometimes the other way around.

316
00:31:17,200 --> 00:31:23,200
It is sometimes it seems easy, and then the next day you can barely take two steps without stumbling.

317
00:31:23,200 --> 00:31:27,200
It's just the way it is.

318
00:31:27,200 --> 00:31:33,200
Hi, Bhante. Is it okay to lower your head when watching the rising and falling of the abdomen?

319
00:31:33,200 --> 00:31:37,200
Or is the head supposed to be even, not in a high or low position?

320
00:31:37,200 --> 00:31:38,200
Thank you.

321
00:31:38,200 --> 00:31:42,200
I mean, you would keep it looking straight ahead.

322
00:31:42,200 --> 00:31:45,200
You wouldn't consciously say, I think it's better.

323
00:31:45,200 --> 00:31:48,200
I want to lower my head and then do that.

324
00:31:48,200 --> 00:31:52,200
But if your head goes down because you're tired, that's fine.

325
00:31:52,200 --> 00:31:53,200
That happens.

326
00:31:53,200 --> 00:31:57,200
Usually you'd want to lift it back up when you realize it's a wanting to lift,

327
00:31:57,200 --> 00:32:01,200
or a wanting razor and raising razor.

328
00:32:01,200 --> 00:32:02,200
It goes down again.

329
00:32:02,200 --> 00:32:04,200
You can just pick it up.

330
00:32:04,200 --> 00:32:05,200
Sometimes you're so tired.

331
00:32:05,200 --> 00:32:09,200
Sometimes you fall asleep, that kind of thing.

332
00:32:09,200 --> 00:32:20,200
But generally it's better to consciously have your face looking straight at your eyes closed.

333
00:32:20,200 --> 00:32:23,200
But if you're mean, like actually with your head down watching the abdomen,

334
00:32:23,200 --> 00:32:34,200
that's a really bad idea because you're not using your eyes to watch or using your mind.

335
00:32:34,200 --> 00:32:36,200
That was it.

336
00:32:36,200 --> 00:32:38,200
You guys are too easy on me.

337
00:32:38,200 --> 00:32:39,200
Wow.

338
00:32:39,200 --> 00:32:42,200
And that was after two days.

339
00:32:42,200 --> 00:32:44,200
Disgraceful.

340
00:32:44,200 --> 00:32:45,200
No, that's good.

341
00:32:45,200 --> 00:32:47,200
That means everyone's meditating.

342
00:32:47,200 --> 00:32:48,200
Yes.

343
00:32:48,200 --> 00:32:51,200
Everyone's giving up.

344
00:32:51,200 --> 00:32:53,200
We have lots of meditators.

345
00:32:53,200 --> 00:32:56,200
What's our meditation this look like?

346
00:32:56,200 --> 00:33:01,200
Wow, that's pretty good.

347
00:33:01,200 --> 00:33:03,200
Good list.

348
00:33:03,200 --> 00:33:06,200
Sanka didn't have a question.

349
00:33:06,200 --> 00:33:07,200
Come on, Sanka.

350
00:33:07,200 --> 00:33:09,200
Hey, it's Timo.

351
00:33:09,200 --> 00:33:11,200
No, it's Timo from German.

352
00:33:11,200 --> 00:33:14,200
I know a Timo in Canada, but it's not him.

353
00:33:14,200 --> 00:33:17,200
Don't take it.

354
00:33:17,200 --> 00:33:26,200
All right then.

355
00:33:26,200 --> 00:33:27,200
Thanks everyone.

356
00:33:27,200 --> 00:33:29,200
Thanks Robin for your help.

357
00:33:29,200 --> 00:33:32,200
Tomorrow I'm meeting with the...

358
00:33:32,200 --> 00:33:37,200
It's got a big acronym, but it's something on campus some kind of interfaith group

359
00:33:37,200 --> 00:33:40,200
to see about working with the chaplaincy office.

360
00:33:40,200 --> 00:33:46,200
I think I'm not quite sure, but hopefully they'll let us do some chaplaincy work,

361
00:33:46,200 --> 00:33:51,200
all the other religions on campus.

362
00:33:51,200 --> 00:33:59,200
And I have a doctor's appointment to find out why I'm burping all the time.

363
00:33:59,200 --> 00:34:02,200
I think I'm just getting old.

364
00:34:02,200 --> 00:34:03,200
I think that's all it is.

365
00:34:03,200 --> 00:34:08,200
Body is saying...

366
00:34:08,200 --> 00:34:11,200
Body is laughing at me.

367
00:34:11,200 --> 00:34:13,200
It's playing tricks on me.

368
00:34:13,200 --> 00:34:16,200
It's breaking down.

369
00:34:16,200 --> 00:34:19,200
Body is like an old cart.

370
00:34:19,200 --> 00:34:25,200
Keep patching it up to finally just falls apart.

371
00:34:25,200 --> 00:34:28,200
I'm dying, Robin.

372
00:34:28,200 --> 00:34:29,200
What do they do?

373
00:34:29,200 --> 00:34:37,200
The doctors ever have anything pro or con to say about your practice of not eating after

374
00:34:37,200 --> 00:34:38,200
a new child?

375
00:34:38,200 --> 00:34:39,200
I didn't tell him that.

376
00:34:39,200 --> 00:34:43,200
I was just thinking I'm probably tomorrow I should tell him that I'll meet in the morning.

377
00:34:43,200 --> 00:34:48,200
I know he's going to say, you know, you should be eating something in the afternoon.

378
00:34:48,200 --> 00:34:49,200
Because it's a big...

379
00:34:49,200 --> 00:34:53,200
Apparently also you can get ulcers if you're not careful with your...

380
00:34:53,200 --> 00:34:54,200
I don't know.

381
00:34:54,200 --> 00:34:58,200
Somebody told me that some time and told me.

382
00:34:58,200 --> 00:35:03,200
And the Thai doctor told me monastic, monastic and diet is wrong.

383
00:35:03,200 --> 00:35:04,200
Well, I don't know.

384
00:35:04,200 --> 00:35:11,200
I mean actually Doug, one of our volunteers that posted a video in our Slack about what's

385
00:35:11,200 --> 00:35:14,200
kind of the new thing intermittent fasting.

386
00:35:14,200 --> 00:35:20,200
And there are similarities to the monastic diet with the sixth precept.

387
00:35:20,200 --> 00:35:24,200
Because it's apparently there are some benefits to going along periods of time without

388
00:35:24,200 --> 00:35:25,200
eating.

389
00:35:25,200 --> 00:35:26,200
But...

390
00:35:26,200 --> 00:35:29,200
It would just be interesting to...

391
00:35:29,200 --> 00:35:32,200
You probably should mention it to them that aren't they?

392
00:35:32,200 --> 00:35:38,200
Because if what you're experiencing is a digestive problem, they probably should know that, right?

393
00:35:38,200 --> 00:35:43,200
Yeah.

394
00:35:43,200 --> 00:35:55,200
So, about that and some French stuff and learning French.

395
00:35:55,200 --> 00:35:57,200
And we're going to...

396
00:35:57,200 --> 00:35:58,200
Oh, a neat thing today.

397
00:35:58,200 --> 00:35:59,200
I went to...

398
00:35:59,200 --> 00:36:06,200
We got this French conversation class and the tutor said he wants...

399
00:36:06,200 --> 00:36:10,200
Because he was asking what we're interested in that we want to talk about.

400
00:36:10,200 --> 00:36:13,200
And I said, of course, meditation.

401
00:36:13,200 --> 00:36:15,200
And I'm looking to find ways.

402
00:36:15,200 --> 00:36:20,200
I'm learning French for the purpose of being able to teach meditation to French people.

403
00:36:20,200 --> 00:36:25,200
And so today he said, you know, I want you to come in here and teach us...

404
00:36:25,200 --> 00:36:28,200
I'm trying to teach us meditation in French.

405
00:36:28,200 --> 00:36:30,200
And we'll try and work through that.

406
00:36:30,200 --> 00:36:33,200
He's interested in learning meditation.

407
00:36:33,200 --> 00:36:40,200
And the other woman who's also in my class is in this conversation class.

408
00:36:40,200 --> 00:36:43,200
She's open to it anyway.

409
00:36:43,200 --> 00:36:45,200
It's a lot of fun.

410
00:36:45,200 --> 00:36:47,200
So I'm going to pull off the French.

411
00:36:47,200 --> 00:36:50,200
Pull down the French translation of my booklet.

412
00:36:50,200 --> 00:36:53,200
And...

413
00:36:53,200 --> 00:36:55,200
Yeah.

414
00:36:55,200 --> 00:36:59,200
I'll try again out.

415
00:36:59,200 --> 00:37:00,200
Okay.

416
00:37:00,200 --> 00:37:02,200
Good night, everyone.

417
00:37:02,200 --> 00:37:04,200
Thank you, Dante. Good night.

418
00:37:04,200 --> 00:37:33,200
Thanks again, too.

