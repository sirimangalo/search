1
00:00:00,000 --> 00:00:06,340
Accepting that all views distract from the road to nibana, how tolerance should Buddhists

2
00:00:06,340 --> 00:00:11,260
be to other views and religions, which may just be different perspectives.

3
00:00:11,260 --> 00:00:16,900
I heard Buddhism is meant to be tolerant to other views, but how tolerant is tolerant.

4
00:00:16,900 --> 00:00:19,540
Good question.

5
00:00:19,540 --> 00:00:24,380
I don't know, Buddhism tolerant to other views, it depends what you mean by tolerant, and

6
00:00:24,380 --> 00:00:25,940
that's really what you're asking.

7
00:00:25,940 --> 00:00:28,580
What do we mean by tolerant?

8
00:00:28,580 --> 00:00:32,540
How should we be tolerant and what ways should we be tolerant?

9
00:00:32,540 --> 00:00:38,380
We should approach views and religions in the same way that we approach everything.

10
00:00:38,380 --> 00:00:49,780
I really, everything should be approached in the same way, with mindfulness, with clarity,

11
00:00:49,780 --> 00:00:58,460
with knowledge of the ultimate reality involved in the experience involved in the situation.

12
00:00:58,460 --> 00:01:01,940
So religions don't exist, views.

13
00:01:01,940 --> 00:01:11,620
May I add openness and open with an open, accepting mind?

14
00:01:11,620 --> 00:01:19,580
Yeah, level, balanced mind, because that's the question we're dealing with here is whether

15
00:01:19,580 --> 00:01:25,220
you should be fully open-minded to give the benefit of the doubt.

16
00:01:25,220 --> 00:01:29,780
We were talking about this earlier, you know, that there is a sense of openness, even

17
00:01:29,780 --> 00:01:31,820
in bad things, right?

18
00:01:31,820 --> 00:01:35,860
Because if it's bad, it's objectively bad, so yeah, open as well.

19
00:01:35,860 --> 00:01:39,220
You don't have to have prejudice even against bad things.

20
00:01:39,220 --> 00:01:44,140
So if it's a wrong view or so, and you shouldn't have the idea, this is wrong.

21
00:01:44,140 --> 00:01:46,260
You shouldn't have a version to it.

22
00:01:46,260 --> 00:01:52,860
There should be a openness, yes, so I agree.

23
00:01:52,860 --> 00:02:00,540
I just have to think about it, because if it's wrong, it's going to be objectively wrong.

24
00:02:00,540 --> 00:02:04,060
And by giving it a fair hearing, it's going to fail.

25
00:02:04,060 --> 00:02:08,220
It's going to fail the test of usefulness.

26
00:02:08,220 --> 00:02:12,700
And that's really the only way, the only litmus test, to tell you whether something is

27
00:02:12,700 --> 00:02:20,740
good or bad, is objective observation and nonjudgmental, I guess is really what is meant

28
00:02:20,740 --> 00:02:29,700
by open to give it the fair trial and let it say it's peace without any partiality,

29
00:02:29,700 --> 00:02:32,660
not being favorable towards it, not being unfound.

30
00:02:32,660 --> 00:02:49,740
So why I talk about this is because we don't aim to have, because our intellectual, I don't

31
00:02:49,740 --> 00:02:53,740
want you to think I want you to be intellectual, but the activity that should go on in

32
00:02:53,740 --> 00:02:57,420
the mind is that this is wrong view.

33
00:02:57,420 --> 00:02:59,220
We come to see this through the practice.

34
00:02:59,220 --> 00:03:04,460
When you practice, we pass in a meditation, you come to see how, as I said, how body

35
00:03:04,460 --> 00:03:08,980
and mind works, you come to see what our body and mind, what is reality, it's body

36
00:03:08,980 --> 00:03:14,380
and mind, how they work together, they work together in terms of cause and effect and so

37
00:03:14,380 --> 00:03:19,500
doing bad deeds, does lead to bad result, doing good deeds, does lead to good result.

38
00:03:19,500 --> 00:03:25,140
And things like God don't exist, the creator, God doesn't exist, there are things that

39
00:03:25,140 --> 00:03:31,700
cannot exist in the system of reality, they're impossibilities in the system of reality.

40
00:03:31,700 --> 00:03:38,700
So if someone tells you, God created the universe in seven days, has a plan for you and

41
00:03:38,700 --> 00:03:46,900
some point in the future he's going to give you eternal bliss in heaven if you believe

42
00:03:46,900 --> 00:03:49,700
in him.

43
00:03:49,700 --> 00:04:00,620
This is impossible for some, for this is impossible to, this is impossible, no, it can't

44
00:04:00,620 --> 00:04:07,420
occur and it's completely 100% unbelievable to someone who has understood reality.

45
00:04:07,420 --> 00:04:10,620
It can't be the case that this could be true.

46
00:04:10,620 --> 00:04:16,820
So one knows in one's mind, I would say intellectually, but what I mean by that is that's

47
00:04:16,820 --> 00:04:20,580
the thought that goes through one's mind, that's all I mean by intellectually, one doesn't

48
00:04:20,580 --> 00:04:25,780
come to that logically, but the mental activity that goes on in the mind will be this is

49
00:04:25,780 --> 00:04:27,140
wrong view.

50
00:04:27,140 --> 00:04:32,340
Whenever someone hears, whenever say an enlightened person or even just to be passed in

51
00:04:32,340 --> 00:04:38,180
a meditator, here's these things, this is correct for this to come into the mind, but

52
00:04:38,180 --> 00:04:42,460
that doesn't mean that one, that doesn't, that shouldn't have a great deal of impact

53
00:04:42,460 --> 00:04:46,060
on how one interacts with people of other religions.

54
00:04:46,060 --> 00:04:49,180
So this is where the tolerance comes in.

55
00:04:49,180 --> 00:04:55,860
In the mind, in one's intellectual mind, one has no tolerance for it, one can't have

56
00:04:55,860 --> 00:05:01,140
tolerance for it, one can't entertain this view and say, well, maybe they're right.

57
00:05:01,140 --> 00:05:05,220
One can say that in one's mind, but one can never have the feeling that this might be

58
00:05:05,220 --> 00:05:06,220
right at all.

59
00:05:06,220 --> 00:05:10,380
One can say, okay, well, let's look and see whether this is right, but it's impossible

60
00:05:10,380 --> 00:05:16,340
that one could come to any conclusion that this is right or even entertain it, if they

61
00:05:16,340 --> 00:05:17,340
have seen reality.

62
00:05:17,340 --> 00:05:19,940
I mean, that's really just the case.

63
00:05:19,940 --> 00:05:24,540
If I have something in my hand, as long as you've never seen what's in my hand, then

64
00:05:24,540 --> 00:05:30,460
you can believe me when I tell you there's a diamond in my hand or there's a frog

65
00:05:30,460 --> 00:05:33,980
in my hand or whatever I tell you, you can still entertain that.

66
00:05:33,980 --> 00:05:39,620
But once you've seen what's in my hand, then you can't believe me when I say there's

67
00:05:39,620 --> 00:05:40,300
this or that.

68
00:05:40,300 --> 00:05:44,700
But if I open it and show you there's nothing, then if I say there's a frog in my hand,

69
00:05:44,700 --> 00:05:47,020
you can't believe me.

70
00:05:47,020 --> 00:05:51,540
Once you've seen it for yourself, this is the way with anything and reality, of course,

71
00:05:51,540 --> 00:05:56,380
is the most absolute in that way.

72
00:05:56,380 --> 00:06:02,460
So you're not tolerant at all internally, externally is totally different.

73
00:06:02,460 --> 00:06:09,660
And I think that's how this should be understood that we don't entertain other religions

74
00:06:09,660 --> 00:06:12,420
and saying, well, you know, they're all teaching people to be good.

75
00:06:12,420 --> 00:06:16,180
So they're all equal or they're all equal or something.

76
00:06:16,180 --> 00:06:24,180
But we can, and we should, pick out what is good in all religions and say that that is

77
00:06:24,180 --> 00:06:25,180
good.

78
00:06:25,180 --> 00:06:28,900
But as the Buddha said, he said, you know, when a person says something that's in line

79
00:06:28,900 --> 00:06:32,660
with the truth, I agree with them, when a person that says something that's out of line

80
00:06:32,660 --> 00:06:35,420
with the truth, I disagree with them.

81
00:06:35,420 --> 00:06:39,380
So how you react to the person, how you react to other people and other religions and

82
00:06:39,380 --> 00:06:44,020
how you talk about them and speak about them, is a totally different matter.

83
00:06:44,020 --> 00:06:48,460
Because if you sit around saying, oh, these other religions are horrible, then that's

84
00:06:48,460 --> 00:06:51,300
not a terribly Buddhist way to behave.

85
00:06:51,300 --> 00:06:58,580
If you ridicule them or if you say mean and nasty things to them or about them, then what

86
00:06:58,580 --> 00:07:01,300
it shows is that you're lacking in Buddhist practice.

87
00:07:01,300 --> 00:07:07,300
A Buddhist, the person who is Buddhist will be, I think maybe the word isn't tolerant,

88
00:07:07,300 --> 00:07:12,820
but will be, you know, mindful is the best word and you could say respectful or you could

89
00:07:12,820 --> 00:07:19,860
say a cordial or so on, but the real word is mindful.

90
00:07:19,860 --> 00:07:23,860
And so because they act in a mindful way, it could be different for some people.

91
00:07:23,860 --> 00:07:27,500
Some people mindful might just be to not say anything, for some people mindful, mindful

92
00:07:27,500 --> 00:07:34,420
might be to interact with a person and speak cordially to them and agree with them when

93
00:07:34,420 --> 00:07:37,060
they talk about good things.

94
00:07:37,060 --> 00:07:40,940
So when a Christian says, you know, you reap what you sow, we say, yeah, yeah, that's

95
00:07:40,940 --> 00:07:41,940
so true.

96
00:07:41,940 --> 00:07:45,140
When they say blessed are the meek, you say, yes, yes, that's true.

97
00:07:45,140 --> 00:07:49,420
You can even say, you know, Jesus really had some good things to say for example.

98
00:07:49,420 --> 00:07:51,380
You can say that about all religions.

99
00:07:51,380 --> 00:07:56,940
That's, I think, a good thing to do on a practical level, if you're just looking for tips

100
00:07:56,940 --> 00:08:01,180
on how to deal with people from other religions, I think that's the best to find the

101
00:08:01,180 --> 00:08:08,180
good things in them and stick to that and try to do your best to be silent and avoid

102
00:08:08,180 --> 00:08:09,340
the controversial issues.

103
00:08:09,340 --> 00:08:15,140
But that has nothing to do with how tolerant you are, I think, except, you know, see

104
00:08:15,140 --> 00:08:19,660
it's a difficult word tolerant because you certainly tolerate them.

105
00:08:19,660 --> 00:08:22,100
And if they want to kill you, you tolerate that.

106
00:08:22,100 --> 00:08:26,260
If they say, she had time and they say, convert or die, you say, okay, go ahead, do

107
00:08:26,260 --> 00:08:31,660
you worst, that's tolerant, so in a sense you're completely tolerant, but you're not tolerant

108
00:08:31,660 --> 00:08:38,060
in the sense that they say, believe in our religion or, or, or will kill you or, or

109
00:08:38,060 --> 00:08:43,700
you should believe in our religion, you say, okay, I'll go with that, or our religion

110
00:08:43,700 --> 00:08:44,700
is equal.

111
00:08:44,700 --> 00:08:49,500
You, you will never agree with them on that, but I guess that, that is still within tolerance.

112
00:08:49,500 --> 00:08:56,460
So tolerant in the sense that you, you will never be, be angry about the other religion.

113
00:08:56,460 --> 00:09:03,740
You'll never reject it as a negative thing, but you will reject it 100% in the mind.

114
00:09:03,740 --> 00:09:07,180
And if they ask you your own opinion and you have to give it, you'll say, that's wrong

115
00:09:07,180 --> 00:09:08,180
view.

116
00:09:08,180 --> 00:09:11,580
You'll say that, that is not in line with reality or you might try to find some better

117
00:09:11,580 --> 00:09:14,300
way to say it.

118
00:09:14,300 --> 00:09:15,980
But it works on two levels.

119
00:09:15,980 --> 00:09:20,220
But you believe for yourself and how you interact with other people, which I think are

120
00:09:20,220 --> 00:09:22,700
two very, very different spheres of activity.

121
00:09:22,700 --> 00:09:27,740
Oh, you have nothing to add to that.

122
00:09:27,740 --> 00:09:53,900
It's really perfect.

