I'm trying to observe my emotions, but I'm somewhat out of touch with them and I control them a lot.
When an emotion goes away, how can I tell if it's subsided on its own or if I suppressed it?
I wouldn't be so concerned or so analytical about it because the analysis itself is probably a misunderstanding of what's going on.
The concern about suppressing things, the concern about causing yourself suffering, about not accepting the experience.
This concern that you're suppressing, that you're not accepting and learning about the experience is often a misunderstanding of what's going on.
The emotions themselves will cause stress and that's why they're stressed in the mind.
It's not because necessarily because you're suppressing them.
What happens is when you have certain emotions then because of the complications in the mind, we give rise to even more conflicting emotions.
That's where it starts to get confusing.
But the desire is stressful. This is something that you can see if your mind is quiet.
Anger is not only anger is stressful, but desire itself is stressful and will give you tension in the body and suffering in the mind.
It's probably what you're experiencing is quite a bit of suffering and try to be objective about that.
These thoughts that you're having, the worries and the confusion and the uncertainty is also arising.
Remember that part of this difficulty that you're having, the struggle that you're having, is the struggle to accept impermanent suffering and so on.
That the struggle to accept things just as they are, thinking that something's wrong with your practice, is actually kind of absurd that anything could be wrong with your practice.
There is nothing called practice and you're not doing it.
There is only experience and it's coming and going and once you realize that, then all of these knots become untied.
Every moment is a new knot. Every moment in your meditation is a new challenge and the suppression occurs when we fail to meet the challenge.
When at any given moment we stop the mindfulness, we stop the awareness or we cling to something.
It's because we have very one-track minds. We don't have the flexibility to be able to move from one challenge to the next.
This is one explanation of why enlightened people are so bright and so clear in mind because they're able to move from one challenge to the next.
You might get one challenge and you might get it and you unties some knot and then you get so caught up in that that the next challenge comes and you miss it and you try to react to the next moment in the same way.
For example, you focus on pain and finally you get it pain and finally your mind realizes it's just pain and then some kind of lust comes up.
The feeling or even going back to the pain and you're not able to deal with the new experience that you have to be dealt with in a totally different way.
The first shift of awareness was to convince yourself that the pain is rather than being bad that it is just pain.
Now the last, again you have to shift to see that it's just a feeling of pleasure and the chemicals arising in the body. You have to see it for what it is but it's coming from the other angle, the angle of it being a good thing and wanting to cling to it.
So you have to convince yourself in another way or you have to teach yourself in another way that this is the way of letting letting go pain is teaching yourself in the way of letting come.
For example, the difficulty is not in suppressing, the difficulty is in keeping up and sticking with the rhythm or keeping up with the rhythm in adapting to the situation.
And which in the end simply means no matter which angle the mind is taking straightened out in the same direction.
If it's crooked this way, straightened it back this way. If it's crooked this way straightened it back this way. Everything has to be straightened out. Whatever the mind is clinging to bring it back to center.
Everything is just about learning how to do that and teaching yourself to do that with everything, to be clearly aware and simply clearly aware of everything.
I think it's a red herring. It's a false problem that we come into thinking that we're suppressing things, thinking that we're controlling.
And when that feeling of the you identify with some kind of suppression comes up, acknowledge it just as a feeling, acknowledge it for what it is. And as you see clearer, you should be able to see that actually it's just arising based on the defilements, based on the emotions, based on, and you know, based on, so for example.
One thing that happens is people say, well, when I acknowledge thinking, thinking, it feels like I get a big headache. When I say to myself, thinking, thinking, I just get a big headache.
You know, when I'm thinking a lot, I don't have the headache. But as soon as I say, thinking, thinking, thinking, I get a big headache. So you start to think, well, the technology is really just causing me suffering.
Once you really get good at it, and once you really see what's going on, you say, no, no, no, that's not what's going on. Here I've been thinking for a half an hour, building up this huge huge headache without knowing it.
And because I don't want to face the headache, I just think more, you know, I just distract my attention further and further, and it's so much fun to do that.
But there's building, a building, a building of this huge headache, and then finally, I've been thinking, and you bring yourself back and say, thinking, thinking, and boom, suddenly you've got to deal with a headache.
And so you say to yourself, saying, thinking, thinking, thinking, that's just suppressing it. That's not really facing it.
You see, it's a misunderstanding of what's really going on.
When you have lust or desire, and you focus on, you say, wanting, wanting, or pleasure, then suddenly you feel this tension in the body.
You feel this tension, it's coming up, but it's this tension that was there because of the desire itself, or you feel the dullness of mind that comes with desire.
And you think, oh, this is coming from the meditation. You think this meditation I'm suppressing, so it's giving me this feeling of dullness.
But actually, the desire is what's bringing it. The emotions themselves are bringing these as a result. When you're mindful, you're just beginning to see objectively.
And you'll see that, you'll see that, actually, the moment when you have desire, you're not seeing things objectively. That's why it feels wonderful.
The desire feels wonderful until you stop, because suddenly you're objective again, and you're seeing both sides.
Desire is like, here you have the good, here you have the bad, running towards the good, and running away from the bad, so that you never really experience it.
It just builds up, builds up, builds up until you stop, and then you experience it, then you have to deal with it.
So I wouldn't worry too much, even if you are suppressing, the most important thing is to watch the suppression, not to stop suppressing.
It's just to watch and to see what's going on, because in the end, you'll see impermanence suffering, and once off, you'll see that you can't control it.
You'll see that it's, even the suppressing is something that happens sometimes, it doesn't happen sometimes, it's worth getting caught up in, not worth getting attached to.
