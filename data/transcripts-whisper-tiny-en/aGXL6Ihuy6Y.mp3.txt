Okay, good evening, everyone.
Good evening and welcome to our weekly questions and answer period.
It's normally every week, but I've been away, so the questions are piled up.
We have 30 questions. We had a lot more, but I maybe went a little further than normal in my pruning of them.
There were just too many.
Good rule of thumb is if one person is asking lots of questions, I'm going to be harder on them and delete more of them because
you don't get brownie points for asking more questions. If you're asking lots of questions, it's a sign that they might be a little bit off track.
So repeat offenders, get treated more harshly because the idea ideally you should be here to have fewer questions as time goes on, right?
As you gain more wisdom, if you're not gaining wisdom, anyway, no, the big deal is if someone, if there's a lot of questions to the point that it appears that
asking questions is being used as a substitute for or intellectual inquiry is being used as a substitute for practice, so I don't want to encourage that.
Then there are lots of questions that are, I think, arguably outside of the context of this forum, which is really supposed to be about meditation and really about our meditation.
Anyway, that all being said, we're going to dive right in.
So here's someone who has good results when watching the sensations of the breath without noting and noting whatever distractions take them away from the breath.
Provide samata but still lies within the category of insight meditation is all distractions away from the breath or noted. Is this okay in your view? I don't know what you want me to say here.
Not really much more I can say. You believe that it, or you feel that it is both samata and vipassana, so I mean anything that increases your calm can be called samata and anything that helps you see things more clearly can be called vipassana.
That's not a problem. I mean any sort of meditation generally is most or lots of meditation practices are helpful and are going to lead you closer to the goal.
Enlightenment is the ultimate goal is enlightenment, but it's very much dependent on your own level of development.
For some people, it doesn't matter what meditation they practice, it's going to take them a long time for some people.
It takes very little work and often just a little bit of correction through a teacher or through some advice or even through their own experience if they're really well advanced and they can become enlightened.
The only question is really what is going to get you there more quickly and unfortunately it is different for every person.
Each person requires different factors, requires different things to allow them to become enlightened.
Now the big problem of course is that none of us really know what those are for each individual person.
Only a Buddha can really, that's what we think is so great about a Buddha is they're able to see that.
Embiring that we have guidelines while the Buddha was quite adamant that everyone had to practice mindfulness and that seems like quite clearly the root of it all.
So we tend to teach mindfulness and in my tradition we have this one technique and this is the technique that we teach to people.
With the help of a teacher it's easy to adapt it to one to each individual's personal experience to some extent and of course depends on the ability of the teacher but still depends very much on the ability of the student.
But that's all we've got. If you want to practice something else, I don't really much to say about it.
But I didn't want to delete this one because I wanted to give that context about people asking can I practice like this? Can I practice like that?
Let's not get too dogmatic about things but be clear what exactly we're talking about. You can practice any old way you want. It's not how I teach and it's not how people in our tradition.
It's not the way that people in our tradition consider to be the best way to practice. Of course that opinion differs from tradition to tradition.
The only answer you're going to get from me is that that's not as good as what we do.
I feel what we do is the best. I mean I guess I'm not so strongly adamant about that but that's how I feel.
I'm not very good at this. Someone asked if there are any other books in our tradition.
And the adjunct on the traditions. The thing is adjunct on is not a scholar monk. So he didn't write any books.
I've written more books than he has. He has like a level of grades three education.
You know pretty good in poly I think but you know he's never he's never held himself up to be a scholar.
I mean he's studied and he keeps studying and he reads and but his manner is much more simple.
And he's always been much more focused on you know teaching people talking to people this dialogue kind of like Socrates in a way.
Is that sort of figure of helping people not don't think of Socrates that's not what he's like but this sort of much more conversational.
He's never or not in a long time anyway. I don't know if you ever taught classes of any sort.
I think he did actually I don't remember but like you know many many many years ago.
So there are books that are written based on his teachings and some of his students teachings and he once gave a talk that was transcribed and translated into English.
And that's a book is made out of that. So that's available. The translation is okay and the format of the book I'm not really impressed by it.
Personally.
And there have been that lately there have been some attempts to translate into English a lot of his talks but I gave up because a lot of his talks are are not professional enough to be transcribed you know.
They'd have to be polished and polished and fixed up and anyway I guess the short answer is I would recommend going more towards Mahasi Sayedah because he was a scholar monk and a lot of because Burma was used to be a British colony.
There's a lot of English translations of his tech his books.
So anything written by the Mahasi Sayedah I would look in that direction definitely a hundred percent recommend anything written by him as long as the translation is okay.
So in regards to noting, if I think of this I find it easier to find it hard.
So noting is just words so I find it hard to identify with the noting.
But if I think about what these labels actually are I think this is more helpful. I think of this I find E.G. pain pain this is really a signal sent to the brain registering the mind from.
Okay I think you've really got it all wrong what we're trying to do here. We're not trying to identify with or feel comfortable or have some kind of success as a result of noting.
It isn't or practice isn't meant to be some kind of resolution of anything.
Meditation is supposed to mess you up. It's supposed to make you uncomfortable. It's supposed to be very disturbing.
And this is why people have problems with noting. It's uncomfortable to note because when you do that there's stress and there's chaos and there's sort of this when there's uncontrollable experiences.
And the mind starts to freak out as a result. It's not what we want it to be. We want things to be stable satisfying controllable.
And generally that's how we want our meditation to be as well.
So we're not really interested in fact in observing the experience we're interested in reminding ourselves.
It means to remind yourself that it's just that. So we're trying to change the way we think of things.
Just remind ourselves, hey that's just pain that's just seeing. That's what the noting is for.
It's meant to remind you and to focus you on this bare experience of the thing as it is.
The whole signal sent to the brain, blah blah blah, is not at all what we're driving for aiming for.
So I think you've got some theories going on here but I'm not at all interested in them and I don't think I'm not going to entertain or argue about them.
We're doing one particular thing here and that's to see things as they are seen. Pain as pain and so the noting is meant to allow that.
It's meant to remind you of that. And it's not going to be comfortable. It's not going to be peaceful but it's going to expose our partialities how we'd rather chase after things and how we'd rather get caught up
in our experiences than simply observe them.
So I mean basically a lot of these questions. If it's not the technique that I teach in my booklet then no I'm not okay with it.
There's no question. I mean it doesn't mean that you're wrong. Maybe you think I'm wrong. You don't have to believe me.
But you're not going to get a different answer for me.
What do you recommend for a more advanced practice after practicing the insight meditation in your book how to meditate?
Well I recommend coming to practice or coming to do meditation course here if you see.
I think this camera's on. We got four meditators here.
Right now doing courses. We also have online courses so you're welcome to sign up for one of those and then we meet once a week.
But you know the booklet is really just the preliminary. It's a basic exercise.
There's a lot more to learn in terms of the technical practice that we teach.
The booklet isn't the complete manual.
We purposely didn't make a manual because we want to prescribe these as you go through the practice.
You know guided by a teacher.
Okay so here's a long question.
A lot of practices have to do a self-control like being able to control not indulging in food, waking up on time.
Lots of other things. How can we learn from our mistakes? How should we handle them without guilty feeling?
The way it's to reduce the limited self-control or the practices to increase the self-control.
I mean you're describing the difficulty of becoming enlightened.
If it were easy we'd all be enlightened but we've been going through lifetime after lifetime without it.
Now we have this glimpse of enlightenment of a way out because of people who have actually done it.
And so somehow we're aiming in that direction or we're pulled in that direction a little bit but for the most part we're still very much stuck in Samsara.
So the best answer really is that to be to ensure some sort of success and to be able to actually control your desires,
so to speak meaning to not be consumed by your desires is to take radical drastic measures.
You know to go off into the forest this is how the Buddha became Buddha.
He left society and spent as much of his time as he could in the forest learning about reality because he can't learn about,
he can't develop good things just by mucking around in society.
He did that too but eventually more and more he left the world.
And so as followers of the Buddha we have an accelerated process.
We don't have to go off and learn about reality.
We come to a Buddhist center and we follow the guidance of the Buddha.
And so we have meditation practices and theories and guidelines that allow us to progress more quickly.
But you know definitely if you really are serious about this question about trying to gain self-control,
leave home and go become a monk or at least go to a meditation center and work hard at becoming enlightened.
According to the Yuganada Sutta which one of the four methods does the Mahasi technique fall under.
It is insight preceded by tranquility.
I mean you have to if you're asking that question if we want the really technical answer,
tranquility here refers to the jhanas refers to actual absorption.
And depending on how you think of the jhanas, our practice only enters into the true jhana at the moment of enlightenment,
at the realization of nibhana.
But it's arguable because what does a jhana really mean?
Couldn't you say that the sankarupakinyana is the fourth jhana for example?
Anyway, this is one of those technical questions that I'm not really fond of.
Because who cares?
Sorry.
It's not real.
Well, maybe that's a bit too harsh but it's not as important as some other questions.
Technical questions are fine.
I'm not the one to answer them and this really isn't the context.
The point is who cares means it's not immediately going to help my meditators sitting here in front of me.
I'm not all that interested.
I want to help them.
Tonight is also for people here.
Someone asks me, tear coffee.
I choose tea.
How do I observe this thought tea without one of the label, my thought?
I have no idea.
Am I the one doing the choosing of the tea?
Well, there's no eye.
There's just the mind.
There's lots of reasons why you chose tea.
It's very organic.
There's this muck of body and mind conglomerate, a aggregate of body and mind.
For some reason or another, it could be a million different reasons came to choose tea instead of coffee.
You don't really understand the usefulness of that question.
A three-year-old has started asking me about death without any cause.
Everything that is alive dies and death is when the heart stops, breathing stops.
Everything is always changing.
That's interesting.
Why not teach them the truth?
Death is just the concept.
Death is when the body breaks down.
When we let go of this body and go on to find another one.
It sounds like we're talking about a soul traveling, but it's much better than saying we die.
We don't die.
The body breaks down.
You could say the body is like a car.
It's true.
It's not wrong to talk in ways that sound like there's a soul.
If you want to get technical about it, the mind arises and ceases every moment.
Nonetheless, the moment of death, all that you could say happens is the body breaks down.
They're much better to talk in terms of terms that sound to a child.
This is me.
When I die, it just means letting go of this body.
I don't think that wouldn't really scare them.
That's the sort of thing that I think would reassure a child.
I've talked full of the story I think before, but we took a Buddhism class in university
many years ago.
One of my friends was a Catholic and grew up as a Catholic and gone through many years of
Catholic school and Catholic church and everything.
She came up to me after the class and she said, what a concept to know the idea that
we get another chance.
How freeing that is.
I would think that would, I was freaked out as a child over death.
Death was just, yeah, it's this inconceivable thing and it's this unavoidable end loss.
But if you think in terms of rebirth, it's not loss.
All you lose is the body.
Body is not really all that good for anything.
You'll just go get another one.
You'll help her understand things in sort of a Hindu way, which is basically correct.
The only things the Hindus or the Brahmins got wrong is this idea of self and that there
was any self involved in the process.
Yeah, I mean, it'd be great if, because this is what they teach Asian kids and look
at how less fear there is of death.
And less of distress and existential crises, Asian kids or Buddhist kids know that.
It's all about karma and going through lifetime after lifetime, doing better and becoming
a better person eventually becoming enlightened.
It's really a good base for eventually actually taking up meditation practice.
It's why Buddhists do tend to have an easier meditation practice than non-Buddhist.
People who've never learned anything and grew up without this and come to practice
meditation have a harder time.
They don't have that base of stability in the mind.
There's lots of other factors, but I would think that would be a great thing to teach a child.
I'm pretty sure it is.
How can any Buddhist be content or happy if he believes all our existence is wretched?
Yeah, I mean, I want to be kind of smurmy and say that it's because they believe all of our existence is wretched that they're content and happy
because they're not chasing after it.
But what you're suggesting, what you're implying is that a Buddhist is upset, is disturbed by that wretchedness.
And that's not necessarily true.
See, the whole idea of seeing something is wretched.
It's just a manner of speaking.
What's more technically correct is that a Buddhist sees that everything that arises is just a momentary arising.
Reality is just everything experiences arise and sees moment after moment after moment.
But the sort of conventional interpretation of that is that that makes it like garbage, useless, bad.
Unbad in the sense of like rotten fruit or rotten meat.
Not something you want to have anything to do with.
It's useless.
But an enlightened being, not talking about Buddhists, but enlightened being is not disturbed by that.
I mean, in fact, I would say most Buddhists are more disturbed by the fact that they're still chasing after these things than they are disturbed by the fact that it's wretched.
So, I don't see that as being something that would cause people to.
I mean, the way that it can cause problems is if someone is looking for some meaning, looking for something that's not wretched,
or is expecting something not wretched, right?
Buddhist don't expect that.
Buddhists tend to have this understanding that everything is wretched.
But if you think, you get depressed because, oh, man, this is all wretched.
Where's the non-retched stuff?
And then you get depressed when you find out that everything's wretched.
Not because it's wretched, but because you want something that's not wretched, or you're expecting, you see it.
So, you know, most Buddhists don't think like that.
They don't say things, oh, all existence is wretched, but to some extent it's kind of true in the sense that nothing is worth clinging to.
Nothing is of any value.
Nothing that arises anyway, right?
Of course, peace and freedom.
It's worth it as value.
Does the Buddha think or is Buddhist perception one of pure awareness with no thoughts?
I kept this one because, even though it's speculative, the important aspect here is a lot of people think that
meditation is to stop you from thinking, and that's not true.
Yes, the Buddha did think absolutely thought is not a problem.
The problem is not that you're thinking, it's that you're reacting to the thought.
That you're judging them and that you're chasing them as that you're obsessing and extrapolating upon them.
So, when you meditate, be clear that we're not trying to blank the mind.
We're trying to gain objectivity, equanimity, struggling with the concept of not mine.
It's ironic that you say it like that.
It's kind of funny because, of course, it's because of the eye that we struggle with not mine.
But that's not quite what you mean.
So, when things last as an object of awareness, they seem to be the only thing present and no one else can experience them than me.
So, while they last, they should be able to take the ownership.
Oh, it was clear they're not worth clinging on to.
I mean, you're projecting.
What was really going on is there's an experience of what seeing, let's say.
So, you have an experience of seeing.
There's an experience of seeing, now there's the physical aspect which is the light and the eye.
Then there's the mental aspect which is the awareness of it.
So, it's not eye who is experiencing them.
There is the awareness of the object.
But that awareness is momentary.
You see, this isn't metaphysical or philosophical.
It's just trying to describe experience, describe reality from an experiential point of view, which doesn't admit to the self.
We're not trying to answer these questions.
Is there a self? Isn't there a self?
Is this mine? Is this not mine?
Not exactly.
We're trying to describe, or not even describe it.
We're trying to observe reality or change the way we look at reality.
So that we simply see experiences experiencing, seeing, seeing, hearing, hearing.
And we don't ask all these questions.
We don't cling because we have so much clinging of me and mine which doesn't have any place in what we define as reality.
And the idea is we define reality in very simple terms. Reality is experience.
That's a very useful paradigm to take on.
A very useful way of looking at the world because it stops all this ego and clinging.
There's no reason to...
If you don't have concepts of me and mine and all this garbage that has no meaning,
if you don't have any of that, then you don't suffer.
You don't cling. There's no whatever.
And so as you observe reality,
as you observe the moment after moment of experience of reality,
you lose all concept of self and me and mine.
And you see how out of sync those views are with reality that they actually create stress.
When you hold on to something wanting it to be this way, wanting it to be that way,
you create stress for yourself. It doesn't work.
It's not sustainable.
When you hold on to something as me, as mine, then when you lose it,
when you can't get it, then you suffer.
So you see that all of these views, self, soul, me, mine,
they all are part and parcel of the problem.
They are part of this stress and striving after things that don't make us happy and so on.
So the understanding of...
I mean, not mine is something that you come to through...
Come to through meditation.
You'll come to see that reality is just experience and experiences are...
The idea of mine doesn't even...
Again, it's like good or bad. They have no place in this.
It is what it is. It arises and ceases.
So not mine is in response to this idea of mine, which is wrong.
That's what not mine, not self.
Well, the three characteristics aren't something positive. They're negative, right?
It's not permanent or stable or predictable.
It's not satisfying and it's not yours.
They're in response to wrong views of thinking things are going to be stable or we can find stability and predictability and so on.
That we can find satisfaction and that we can control and that we can own and possess and it's good to own and possess.
What are some strategies for getting children involved in meditation?
Well, you could try... I've made some videos on how to meditate for kids.
I think that's the best I can do without going into too much detail.
And look up my how to meditate for children videos.
Give you some ideas.
Question and reference to women in Buddhism.
Long, long, long. How is it that women have come to be at such a disadvantage?
It wasn't the Buddha who instituted an oppressive framework.
So again, not really to do with meditation, but I think it's this kind of thing that's important.
Buddhism does have a bad... it's like most religions.
It doesn't have a good history in relation to women.
No, the Buddha didn't. Wasn't the one who thought up, hey, we should be mean to women,
but he did come out of a culture at the time in the Buddha.
Women were basically possessions of their husbands.
And if you read the Buddhist literature, this problem is really confronted.
But, you know, the Buddha wasn't out to change society.
It wasn't out to liberate women from... it was out to help people and a lot of women became enlightened.
But it was much more individual like every person, you know, take them as they are.
It wasn't out to change society. People say he was out to end the caste system.
He really wasn't. He spoke against it and he spoke against the idea that women were inferior to men and that sort of thing.
But one thing about women is he was cautious about it because, of course, it was a problem for his male students.
A lot of the most predominant... most of his students were men because they were the ones who had the power to come and become monks.
And so he was always, you know, sort of urging caution in relation to women.
And women did play a lesser role because most likely he had far fewer female disciples,
just because of the society. And women just weren't allowed to go off and become monks.
And for the most part, you know, of course that changed.
It's a complicated situation. But I think one thing I would say is that he wasn't so much interested in,
hey, let's make it fair for the women. Which, you know, maybe it doesn't sound like, you know,
makes people feel less, feel less about, less happy about the Buddha.
But he was much more about taking people as they came and, you know, working with people in their situations to become enlightened.
I don't know if that helps in any way.
But certainly there's never been any sense of discrimination in terms of practice.
How do you skillfully decide when to act and when to endure?
There's mindfulness. Don't worry about acting or enduring. Just worry about being mindful.
Come on, cultivate an addiction to meditation. Can this be a bad thing? Yes, one can become addicted to meditation.
It's probably a bad thing.
What you get addicted to is a concept, you know.
Again, what does it mean to have an addiction? There's, I don't know.
This seems fairly speculative. I'm not sure that's really useful.
Although one thing I would say is you have to be careful not to cling to any desire to meditate or wanting to meditate.
It can blow up in your face.
What you're clinging to is a concept and you have to understand meditation is really in the best sense.
It's about being mindful in the present moment, whatever you're doing.
It's about moments. You can't be addicted to those moments.
Because those moments are moments without addiction.
Don't cling to the concept of meditating.
Because it can even get worse like I'm a meditator and you feel proud.
It may even feel better than other people because of all the meditation you do.
It can get in the way.
Craving leads to volitional activities that can be used for duty.
So if I stop craving, how would I do duties to family?
That's not true.
It doesn't get in the way of your duties. It gets a way in the way of your attachment to your duties.
It means you don't want to help other people anymore.
You still do it because it's the right thing to do.
Is it helpful when meditating to have a statue of the Buddha to remind you to be mindful?
Yes, I think so.
I think it can be quite helpful.
If you think of the Buddha as a greatest meditation teacher, we consider ever.
So having him as a reminder, especially if you've read his teachings and have some faith and appreciation of him,
I think it's a great thing to have.
Is meditation contradictory to non-self?
Yeah, meditation isn't for the purpose of getting rid of self.
It's for the purpose of calming the mind and creating wholesome mind states of love, friendliness.
What are we for not the eye, the ego, the mind?
I mean, you're circular reasoning or your question is nonsense because you're asking,
what am I if not I?
Well, of course, I is I.
I mean, the question doesn't even work because there is no eye.
There's just experience seeing, hearing.
You're looking at it from the wrong point of view, which is fine, but you'll understand through practice.
It's important to let go of these kind of questions and worries and try and just look at the world based up from a point of view of experience.
Questions about me and mine and self are not useful.
Not helpful.
Does the notion of self protect us?
Yes, I mean, self is I is us.
So if you let go of it and the tiger shows up, yeah, you might die.
You might get eaten by the tiger.
But since there's no self, it's not really a problem.
Pain, pain.
When good things happen to me, I consume.
I feel happiness, when bad things happen to me, I feel sadness.
What should I do in my daily life so I can be more or consumed by happiness?
So I can be more an economist apart from meditation.
Well, try and be mindful in your daily life.
I talk about that in the sixth, sixth, sixth chapter of my booklet.
How to be mindful in daily life.
I mean, it's pretty simple and Mahasi Sayyada has lots of descriptions of it.
Basically trying to do the same thing that you do in practice, but outside.
When you walk around, you say walking, walking.
When you brush your teeth, you can say brushing.
When you eat your food, you can say chewing, chewing, swallowing.
When you brush your hair, when you scrub your hair in the shower, scrubbing.
When you're on the toilet, you can note all the feelings and that sort of thing.
Everything.
When you lie down to sleep, you can be mindful.
But don't worry about being an equanimous.
Equanimous isn't a practice, not that we're trying to do.
Don't worry about being mindful, so when you're sad, try and note the sadness.
Say sad.
Say when you're happy, note the happiness.
So, pain arises and ceases, but while it is there, it is a painful experience, even when
noting it is pain, I feel the aversion.
Well, you note the aversion.
I mean, that's not going to last.
If you're really proficient in meditation, the aversion doesn't arise.
It's not the same thing or they're not.
It's not a necessary result.
Until ignorance is erased, does every act have a karmic impact?
No.
I have observed a shift in my perception as a result of mindfulness.
It seems like I've lost empathy, a long question here, but basically feel kind of selfish.
I think that's fair, but there's a lot of...
It's debatable whether altruism, feeling wanting to help others is of any value at all,
or it's actually good in any way.
Why is it good to want to help others?
I'm not saying it's not good, but why is it good?
Who says it is good?
What does it mean to say that it's good to help others?
So, you know, even feel esophically or intellectually, there's no real basis for that.
And the Buddha talked a lot about helping others, but he put it in context.
You help others.
It can be a good for your own mind.
I mean, in the best sense, it makes you a better person.
What that means is it gives you a more stable mind,
and brings you, therefore, closer to peace and happiness and freedom from suffering.
I think something could be said, and I have said before,
is that sometimes if one is just intellectual, one can become quite cold,
and artificially detached.
And I think someone who is meditating a lot appears to be quite warm and compassionate.
You know, like my teacher is very warm and kind and helpful,
and he's just a wonderful person, but he'll drop you like,
you know, when you say drop you like a bad habit,
so I forget about you going to his room and he has no thought.
It can be completely, you know, it's completely detached,
but still very warm and compassionate.
So, I mean, it's important not to be intellectually detached
because then you get this idea that we're just walking around like zombies all the time.
Being detached doesn't mean you can't be compassionate or helpful
or sympathetic and understanding.
But in your mind, you know, there's no sense of wanting to help others
or worrying about or caring whether people benefit from your actions and that sort of thing.
There's just a sense of appropriateness.
You know, this person needs my help.
They need me to sympathize, oh, yes, that's terrible.
Does some extent in a light being will do that?
Now we're still not there.
You guys can go anytime.
You don't have to listen to all these questions.
Feel free to go and meditate.
This will next week, there'll be fewer, I think.
Let's try to go a little quicker here.
Okay, yeah, this person wants to become a doctor.
They've got lots of things.
Stress, examinations, urgency to choose.
Just be able to feel the specialization.
But then they study Buddhism and realize the uselessness of all this need of achieving,
taking care of other people's bodies and their attachment to it.
What do I do?
Well, you know, I would argue there are probably ways that you could be a Buddhist doctor.
I mean, no doubt there's lots of Buddhist doctors, but really technically,
something I'd encourage you to read up is about Jiwaka.
He was a Buddhist doctor.
He didn't become a monk.
And so you couldn't go really far if you really wanted to get through to become enlightened.
You'd have to really kind of give up being a doctor.
Because you couldn't, of course, have patience that weren't monks.
But, you know, a doctor who ordained could become, could help the monks,
help other monks to some extent.
And so you could use your position as a doctor, for example,
to teach people ways to be more meditative, more mindful, right?
You don't have to just as a doctor prescribed pills and medication.
Of course, being a general practitioner has got problems associated with it
for someone who's serious about Buddhist practice.
I mean, serious in terms of wants to become enlightened in this life.
But there are ways to allow you to be a wholesome doctor
and provide people with guidance that leads not to just become dependent on pills and medication,
but to actually develop ideas.
So there are lots of doctors who come to teach meditation and focus their attention
and their skills on meditative practices.
So it's really up to you.
I mean, if you want to, if you really want to,
I think Westerners sometimes think that it's all or nothing.
And sure, all is, of course, the best, but they realize quite,
they figure quite, they figure out quite quickly that, yeah, it's the best,
but I'm just not cut out for the best.
And we think we're all cut out for the best.
Go for the best.
You can try it.
You might get burnt.
But, you know, however far you can go, that's great.
Why is naming a collection of aggregates?
Five aggregates is I wrong.
Tree is a collection of impermanent roots, and they use why do we give it an identity as a tree?
Well, you're realizing something important here that tree is just a concept that's made up of
really experiences, but, you know, made up of roots and so on.
Also because the five aggregates are something that arises and ceases in a moment.
Five aggregates is five parts to an experience.
So at the moment of seeing there are the five aggregates,
and those five aggregates arise and cease.
Five aggregates is not this me thing.
This is not the five aggregates.
That's not what it means.
Five aggregates is the five aspects of parts of an experience.
In an ultimate sense anyway.
Arsenia vipalasa karma, yes.
Most if not all people can understand the logical argument for Anatanya in light and because logic has nothing to do with enlightenment.
Why should I consider parents and kids as Dukkha?
You should not consider parents and kids as Dukkha.
Nobody ever handles this question directly.
Oh, I'm missing one at the time.
Is this speaker enlightened? What's speaker me? Am I enlightened?
Well, I notice if I get enlightened, could I already be enlightened?
Yes, you would notice if you were an Arahan,
but say, you know, I think it's possible for you to not be sure what you are
if you're saying a soda pan or something.
But, you know, it's so dangerous to start talking about these things
who is enlightened because everybody wants, you know, a teacher is not enlightened.
The problem is it leads to so many abuses because then everyone tries to say that their teacher is enlightened
and there's a competition for being enlightened.
And it's, you know, in certain contexts, it's fine.
Like in the context among monks, for example.
But in regards to the broader society,
I would say you should never get into that sort of a conversation of who's enlightened.
It should only go on between really serious practitioners and preferably monks
there's no conflict of interest in terms of who is supporting people who are being supported.
Right? I'm being supported by all of you.
So if I start bragging about, you know, my level of enlightenment,
there's such a conflict of interest that's basically the answer.
So we don't talk about it.
Right, I think this one I already answered.
I know there is a difference between an intellectual understanding and an understanding of reality.
However, if over time a longer period of time, I guess,
it's possible for understanding of reality to change.
No, because reality is, I mean, it's experience.
So, I mean, it's not about what reality is.
It's not about whether it changes.
It's about taking the time to observe and to investigate what reality is.
So it's that practice.
The practice doesn't change.
There's no, it's never going to be, oh, hey, we should stop trying to understand reality.
There's a new way to become enlightened because things changed.
And now all of a sudden the way to become enlightened is
other than understanding reality, right?
It's always going to be that.
The problem is there's very little investigation into reality.
There's much more adherence to views and beliefs and opinions and theories and concepts and so on.
I'm very new to Buddhism. What are the basic rules outside the noble truths in the eight full noble path?
Well, right.
So there's not much outside the four noble truths in the eight full noble path,
but they're just very condensed versions of Buddhism.
A better way to look at it is the three trainings.
So we have morality, which is ethics, trying to keep moral precepts.
And then concentration, which are focus, which is meditation,
where you try to focus the mind on reality.
And then wisdom, the third one, wisdom, which is what comes from focusing your mind on reality.
Those are the three basic trainings of Buddhism.
So the basic rules are the precepts of not killing, stealing, cheating, lying,
taking drugs or alcohol.
But it's worth reading up on, I would recommend reading terabyte of Buddhism because that's the Buddhism that I follow.
I think you learn quickly about Buddhism is there's not Buddhism, there's Buddhism's plural.
There's many kinds of Buddhism.
When I know it, when I experience an unpleasant sensation, is it okay to know Dukkha Dukkha?
I wouldn't try to find fancy names, pick one that, unless you're a fluent native poly speaker,
I wouldn't say Dukkha Dukkha.
I would use the language that's most familiar to you.
It's not about finding special fancy names.
It's about finding a name that reminds you, keeps you centered on the fact that that's all it is.
So if you feel unpleasant, is it unpleasant or pain, or if you don't like it disliking, that sort of thing.
What happens to enlighten beings after death?
Enlighten being is just a concept, so you figure out concepts don't exist, they're just conceptual.
If the historical existence of the Buddha is conclusively disproved with Buddhism collabs, no.
Hi, I was once your student in Sri Lanka six years ago.
Hi, Jay.
You have to remind me who you are six years ago.
It's been that long.
Jay, don't remember Jay.
Sorry.
You don't have to jog my memory.
It's because my memory is getting bad as I grow old.
But I don't forget faces so easy.
Can I use only inquiry to directly remove taint of kamasu?
I don't want to answer that question.
Use my meditation technique, that's all I'm going to say.
Do parents, since parents and kids have existence and therefore I have a career,
trying to re-ask this question, aren't you?
Since parents and kids are sneaky.
No, you've asked too many questions and I'm not going to even entertain that one.
Sorry.
I flew in from the UK.
Sorry.
Need to send me a picture.
I need more than just Jay.
Jay has flown from UK.
Cool.
I think it was a good time until I got very sick and almost died.
Then I realized it was probably a bad place to have foreign meditators come too much sickness and death.
Okay.
That's it.
We got through them all.
62 people watching, great.
A lot of chat on YouTube.
I'll read through that after.
Okay.
That's all for tonight.
Thank you all for coming out.
Have a good night.
