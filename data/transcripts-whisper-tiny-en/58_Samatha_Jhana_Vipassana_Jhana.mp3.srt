1
00:00:00,000 --> 00:00:06,280
Samata Jana, Vipassana Jana, question.

2
00:00:06,280 --> 00:00:11,560
What is the difference between Vipassana Jana's and Samata Jana's?

3
00:00:11,560 --> 00:00:18,720
Is there an advantage of doing straight insight versus Samata?

4
00:00:18,720 --> 00:00:20,200
Answer?

5
00:00:20,200 --> 00:00:25,960
Ultimately, the goal of Buddhism is to see the four noble truths,

6
00:00:25,960 --> 00:00:33,920
which can only be seen through the practice of observing ultimate reality as it is.

7
00:00:33,920 --> 00:00:39,520
The ancient commentaries record different ways to go about it,

8
00:00:39,520 --> 00:00:44,880
but you must eventually focus on reality to see the truth.

9
00:00:44,880 --> 00:00:48,840
If you want to become a fully enlightened Buddha,

10
00:00:48,840 --> 00:00:52,080
then you have your work cut out for you.

11
00:00:52,080 --> 00:00:59,280
If you want to become a private Buddha, then you still have an incredible amount of work to do.

12
00:00:59,280 --> 00:01:04,240
If you want to become one of the Buddha's chief disciples,

13
00:01:04,240 --> 00:01:09,840
you can look to our Buddha's two chief disciples as examples.

14
00:01:09,840 --> 00:01:15,280
There was Mogulana who took seven days to become an arhant,

15
00:01:15,280 --> 00:01:22,640
and Sai Puta who took 14 days to become an arhant.

16
00:01:22,640 --> 00:01:27,760
Why did Sai Puta take longer to become enlightened than Mogulana

17
00:01:27,760 --> 00:01:33,840
when he was the most excellent in wisdom apart from the Buddha?

18
00:01:33,840 --> 00:01:39,200
And why did both of them take longer than those who became enlightened

19
00:01:39,200 --> 00:01:43,320
from simply listening to the Buddha teach?

20
00:01:43,320 --> 00:01:50,480
It took Sai Puta and Mogulana longer because they had higher aspirations.

21
00:01:50,480 --> 00:01:54,720
Sai Puta had exceptional aspiration,

22
00:01:54,720 --> 00:02:01,560
and it is amazing that in 14 days he could accomplish what he did.

23
00:02:01,560 --> 00:02:07,240
He came to understand reality in a very profound way.

24
00:02:07,240 --> 00:02:16,680
The Suta states that he was able to observe realities appearing in sequence one by one.

25
00:02:16,680 --> 00:02:20,560
You do not need to follow these examples.

26
00:02:20,560 --> 00:02:25,720
It is totally up to the individual's aspirations.

27
00:02:25,720 --> 00:02:30,320
Kondanya was the Buddha's first enlightened disciple,

28
00:02:30,320 --> 00:02:35,000
the first person to realize the truth of the Buddha's teaching

29
00:02:35,000 --> 00:02:41,080
had made a wish in the past life that he should be the first person to become

30
00:02:41,080 --> 00:02:45,800
enlightened in the Buddha's dispensation.

31
00:02:45,800 --> 00:02:53,880
To cement his aspiration, he would give the first of everything as charity.

32
00:02:53,880 --> 00:02:57,960
He gave away the first rise when it was on ripe.

33
00:02:57,960 --> 00:03:03,320
He gave away the first rise when it was ripe on the stock.

34
00:03:03,320 --> 00:03:07,480
Then he gave away the first harvest of the rise.

35
00:03:07,480 --> 00:03:12,280
Then he gave away the first thrashing of the rise

36
00:03:12,280 --> 00:03:17,560
and then the first polishing of the rise and so on.

37
00:03:17,560 --> 00:03:22,200
He always gave away the first and then he made a determination

38
00:03:22,200 --> 00:03:27,240
that he should therefore come to see the truth first.

39
00:03:27,240 --> 00:03:33,720
His brother at the same time decided to give away the last of the rise,

40
00:03:33,720 --> 00:03:36,760
the last thrashing, etc.

41
00:03:36,760 --> 00:03:43,720
And then he made a determination to become the last person to realize the Buddha's teaching.

42
00:03:43,720 --> 00:03:49,800
The second brother became Subaddha in the time of our Buddha,

43
00:03:49,800 --> 00:03:56,760
the last person to learn the Buddha's teaching from the Buddha's own mouth.

44
00:03:56,760 --> 00:04:03,240
You have to recognize that there are different ways of accomplishing the same goal.

45
00:04:03,240 --> 00:04:10,440
There is even a sutta that is often quoted in which some monks says.

46
00:04:10,440 --> 00:04:17,080
We do not have magical powers and we have not attained theuru pajanas.

47
00:04:17,080 --> 00:04:22,120
All we have are the janas required for attaining wisdom.

48
00:04:22,120 --> 00:04:25,240
We are panya we muti.

49
00:04:25,240 --> 00:04:32,360
We muti means liberated the Buddha and the later commentaries say

50
00:04:32,360 --> 00:04:40,440
there are two kinds of we muti, panya we muti and cheto we muti.

51
00:04:40,440 --> 00:04:49,080
Those who are cheto we muti practice some at the first and then continue with vipasana.

52
00:04:49,080 --> 00:04:56,680
Panya we muti practice only vipasana and the jana that they attain is the bare minimum

53
00:04:56,680 --> 00:04:58,920
to see clearly.

54
00:04:58,920 --> 00:05:06,760
The commentaries are very clear on this and there is really no controversy in the text.

55
00:05:07,800 --> 00:05:15,400
But in modern times some people do not accept this and they were interpret what the Buddha said

56
00:05:15,400 --> 00:05:23,080
in new ways. Here I will give my understanding about the samata and vipasana janas according

57
00:05:23,080 --> 00:05:29,640
to the commentaries. There are in total three kinds of jana.

58
00:05:29,640 --> 00:05:38,200
The first kind of jana is samata jana. The second kind is vipasana jana and the third

59
00:05:38,200 --> 00:05:48,600
kind is lokutara jana. To attain samata jana called a Ramana Upani jana in the commentaries

60
00:05:49,480 --> 00:05:57,880
one meditates on an a Ramana. A Ramana means object but here specifically it refers to a

61
00:05:57,880 --> 00:06:06,920
conceptual object because only a conceptual object can be made the single object of one's focus

62
00:06:06,920 --> 00:06:16,120
over a period of time. There are 40 standard objects of samata meditation outlined in the text

63
00:06:17,000 --> 00:06:27,640
the 10 casinas which are earth, water, fire, air, blue, yellow, red, white,

64
00:06:27,640 --> 00:06:42,200
enclosed space and consciousness. The 10 contemplations on loaththsonness which are a swollen corpse,

65
00:06:42,200 --> 00:06:53,800
a discolored corpse, a festering corpse, a fistured corpse, a mangled corpse, a dismembered corpse,

66
00:06:53,800 --> 00:07:04,600
a cut and dismembered corpse, a bleeding corpse, a warm infested corpse, a skeleton.

67
00:07:04,600 --> 00:07:22,360
The 10 Anusati, the Buddha, the Dharma, the Sangha, morality, generosity, heavenly beings, death, the body,

68
00:07:22,360 --> 00:07:38,600
the breath, peace. The four brahma wiharas, kindness, compassion, joy and equanimity. The four arupa

69
00:07:38,600 --> 00:07:49,720
kamatanas, limitless space, limitless consciousness, nothingness, neither perception nor non-perception.

70
00:07:49,720 --> 00:08:05,000
Aharepati kula sanya, perception of the repulsiveness of food, and cha tu da tu vava tana,

71
00:08:05,800 --> 00:08:12,760
differentiating the four primary elements of earth, fire, water and air.

72
00:08:12,760 --> 00:08:25,800
Focusing on any one of these 40 or something similar leads to samata chana. They are all based on

73
00:08:25,800 --> 00:08:35,480
concept, so meditating on these objects will not lead to insight directly. For example, you cannot

74
00:08:35,480 --> 00:08:44,520
expect to gain insight by thinking of the color white, because white is a concept apart from the

75
00:08:44,520 --> 00:08:53,880
experience of seeing. The only way you can enter into a samata chana is if you focus on something

76
00:08:53,880 --> 00:09:01,240
that is stable, and if you focus on something that is stable, you will never see impermanence

77
00:09:01,240 --> 00:09:10,600
suffering a non-cell. This is the distinction here. We pass on a jana is called by the commentaries,

78
00:09:11,400 --> 00:09:20,520
La kanupani jana, which means meditation on the characteristics. La kana means characteristic,

79
00:09:20,520 --> 00:09:30,840
referring to the three characteristics of all real, a reason phenomena, impermanence, suffering and

80
00:09:30,840 --> 00:09:41,000
non-cell. We pass on a jana is based on reality, focusing on a momentary experience that arises in

81
00:09:41,000 --> 00:09:51,880
ceases. We pass on a means to see clearly the nature of reality as being unsatisfying, stressful,

82
00:09:51,880 --> 00:10:00,040
and not worth clinging to. When you see this, you let go. When you let go, you are free,

83
00:10:00,760 --> 00:10:09,640
and when you are free, you can say, I am free. This jana is necessary to become enlightened,

84
00:10:09,640 --> 00:10:17,720
because it involves the wisdom that leads to letting go. It is the only way one can enter

85
00:10:17,720 --> 00:10:26,120
into nibana. Samata meditation was thought and practiced before the time of the Buddha,

86
00:10:26,120 --> 00:10:32,520
and yet there are Buddhist teachers who claim that it was discovered by the Buddha.

87
00:10:32,520 --> 00:10:41,640
It seems incredible to think that the sublime peace and tranquility described by ancient aesthetics

88
00:10:41,640 --> 00:10:50,840
was something other than jana. Hindu gurus are experts in jana's. The Buddha truly did discover

89
00:10:50,840 --> 00:10:59,240
vipassana, as can be seen by the absence of the three characteristics in the teachings of other

90
00:10:59,240 --> 00:11:09,560
religions, or even contradiction, especially regarding nonsa. Vipassana leads to the third type

91
00:11:09,560 --> 00:11:20,360
of jana, Lokutarajana, the monks who call themselves pioneer vimuti, specify that they had never

92
00:11:20,360 --> 00:11:30,520
developed former jana, Arupadjana, which implies that they did attain jana with form, Rupadjana.

93
00:11:31,320 --> 00:11:37,080
Rupa, here, refers to the physical nature of the object of meditation.

94
00:11:38,040 --> 00:11:45,560
Some say that means even pioneer vimuti must practice samata jana, but the real reason for

95
00:11:45,560 --> 00:11:54,360
specifying not attaining Arupadjana is because enlightenment in itself involves a specific sort

96
00:11:54,360 --> 00:12:05,400
of jana called Lokutarajana. Lokutarajana, or based on the level of jana, used to attain them.

97
00:12:05,400 --> 00:12:16,040
If one enters into Nibana from the first jana, this is the first Lokutarajana. If one enters

98
00:12:16,040 --> 00:12:45,880
from the second jana, it is the second Lokutarajana and so on.

