1
00:00:00,000 --> 00:00:05,800
Hi, the next question comes from House of Dow, near you to Damu.

2
00:00:05,800 --> 00:00:09,100
How much value do you put on the practice of Janna?

3
00:00:09,100 --> 00:00:12,700
Didn't the Buddha advocate Janna practice at least up to a point?

4
00:00:12,700 --> 00:00:16,200
And does it not have a use in deepening Vipasana practice?

5
00:00:16,200 --> 00:00:19,500
All the best.

6
00:00:19,500 --> 00:00:23,800
The word Janna, it's a highly debated word.

7
00:00:23,800 --> 00:00:31,100
Not the meaning, but the use of it or its place in Buddhism.

8
00:00:31,100 --> 00:00:33,300
I think a lot of the problem comes from the fact that we

9
00:00:33,300 --> 00:00:36,700
misunderstand or we apply too much meaning to the word.

10
00:00:36,700 --> 00:00:42,600
The word Janna means meditation or focusing or absorption.

11
00:00:42,600 --> 00:00:45,100
It means fixing the mind on an object.

12
00:00:45,100 --> 00:00:47,600
And really, in all meditation, that's what we do.

13
00:00:47,600 --> 00:00:50,800
This is why the Buddha said there is no wisdom without Janna.

14
00:00:50,800 --> 00:00:53,100
There is no Janna without wisdom.

15
00:00:53,100 --> 00:00:57,300
But putting these two together, then you come to release.

16
00:00:57,300 --> 00:01:01,400
You come to true understanding and freedom from suffering.

17
00:01:01,400 --> 00:01:06,100
So there's no need to concern or worry about,

18
00:01:06,100 --> 00:01:07,800
do we have to incorporate this?

19
00:01:07,800 --> 00:01:10,100
Do we have to incorporate that?

20
00:01:10,100 --> 00:01:13,600
The problem comes when we want to practice certain types of

21
00:01:13,600 --> 00:01:16,500
meditation that are not based on reality.

22
00:01:16,500 --> 00:01:21,900
And this is where a lot of the ancient texts will diverge.

23
00:01:21,900 --> 00:01:24,900
They split meditation up into two types.

24
00:01:24,900 --> 00:01:29,900
One type of meditation is a summer temptation which focuses on

25
00:01:29,900 --> 00:01:33,900
Janna or a type of Janna, a type of meditation where you focus

26
00:01:33,900 --> 00:01:35,600
on a single object.

27
00:01:35,600 --> 00:01:37,500
And that object is not a real object.

28
00:01:37,500 --> 00:01:39,000
It's something you create in your mind.

29
00:01:39,000 --> 00:01:40,500
You think about something.

30
00:01:40,500 --> 00:01:42,000
So it arises in the mind.

31
00:01:42,000 --> 00:01:43,300
It's a construct.

32
00:01:43,300 --> 00:01:44,800
It's not there in the first place.

33
00:01:44,800 --> 00:01:47,700
You think of the Buddha or you think of a color.

34
00:01:47,700 --> 00:01:51,800
A very simple one would be to imagine a white circle here.

35
00:01:51,800 --> 00:01:56,200
Inside your third eye or a red or a blue circle or something.

36
00:01:56,200 --> 00:01:57,200
You're creating something.

37
00:01:57,200 --> 00:01:58,200
It's not real.

38
00:01:58,200 --> 00:02:00,900
And as a result, it's not going to bring wisdom and

39
00:02:00,900 --> 00:02:03,100
understanding about reality.

40
00:02:03,100 --> 00:02:05,000
But it will bring great states of calm.

41
00:02:05,000 --> 00:02:07,500
That's why that meditation is called samadha.

42
00:02:07,500 --> 00:02:11,200
This meditation can be useful as a precursor to Vipasana.

43
00:02:11,200 --> 00:02:11,400
Why?

44
00:02:11,400 --> 00:02:12,800
Because it calms the mind down.

45
00:02:12,800 --> 00:02:14,800
It focuses the mind.

46
00:02:14,800 --> 00:02:17,800
It can also lead you to become attached to it.

47
00:02:17,800 --> 00:02:21,200
It can be a hindrance towards Vipasana in some cases.

48
00:02:21,200 --> 00:02:22,200
So you have to be careful.

49
00:02:22,200 --> 00:02:24,400
You use it to focus the mind.

50
00:02:24,400 --> 00:02:27,600
And you can also use it to gain very special and magical

51
00:02:27,600 --> 00:02:31,200
states of mind and even magical powers, so they say.

52
00:02:31,200 --> 00:02:34,400
But you can't use it directly to become free from suffering.

53
00:02:34,400 --> 00:02:36,500
To become free from suffering, you have to use a different

54
00:02:36,500 --> 00:02:37,300
type of jhana.

55
00:02:37,300 --> 00:02:39,800
It's actually called Vipasana jhana.

56
00:02:39,800 --> 00:02:44,500
And so the use of jhana in Vipasana is correct.

57
00:02:44,500 --> 00:02:48,600
It means meditating in Vipasana or meditating to see clearly.

58
00:02:48,600 --> 00:02:53,300
So when you start to practice Vipasana, you're going to

59
00:02:53,300 --> 00:02:54,800
focus on an object.

60
00:02:54,800 --> 00:02:55,800
And that is a jhana.

61
00:02:55,800 --> 00:02:57,900
Your mind is focused.

62
00:02:57,900 --> 00:03:00,100
It's clearly aware of only that object.

63
00:03:00,100 --> 00:03:03,600
When we say to ourselves, rising and we know that the rising,

64
00:03:03,600 --> 00:03:05,600
when we say falling, we know the falling.

65
00:03:05,600 --> 00:03:07,800
Slowly, our mind gives up the hindrance.

66
00:03:07,800 --> 00:03:11,800
It gives up liking, disliking, drowsiness, distraction,

67
00:03:11,800 --> 00:03:14,000
doubt, and it's fixed and focused.

68
00:03:14,000 --> 00:03:15,700
So you can say it enters jhana.

69
00:03:15,700 --> 00:03:18,300
It enters the Vipasana jhana.

70
00:03:18,300 --> 00:03:20,000
And this is the other type of meditation.

71
00:03:20,000 --> 00:03:24,100
Samata is tranquility meditation, focusing on a concept.

72
00:03:24,100 --> 00:03:27,900
Vipasana meditation, Vipasana meditation is meditation focused

73
00:03:27,900 --> 00:03:29,000
on ultimate reality.

74
00:03:29,000 --> 00:03:32,500
It means mundane reality, anything that arises in the present

75
00:03:32,500 --> 00:03:36,000
moment, whether it be in the body, our feelings, our

76
00:03:36,000 --> 00:03:40,100
thoughts, our emotions, and so on.

77
00:03:40,100 --> 00:03:44,800
So a lot of the argument and debate out there about jhana

78
00:03:44,800 --> 00:03:46,900
is, I think, really superfluous.

79
00:03:46,900 --> 00:03:49,500
We practice to understand ultimate reality.

80
00:03:49,500 --> 00:03:53,400
And I think that's pretty clear in the Buddha's teaching

81
00:03:53,400 --> 00:03:54,800
that that's what we're all about.

82
00:03:54,800 --> 00:04:00,300
So I would suggest to stick to trying to understand

83
00:04:00,300 --> 00:04:06,500
ultimate reality and not worry about terms and concepts.

84
00:04:06,500 --> 00:04:08,500
OK, so thanks for the question.

85
00:04:08,500 --> 00:04:24,700
I hope that helps.

