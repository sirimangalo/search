1
00:00:00,000 --> 00:00:29,420
In my talk, I said something about

2
00:00:29,420 --> 00:00:42,600
latency of defilements, latency of defilements in the objects, but I didn't

3
00:00:42,600 --> 00:00:54,240
explain it, so you may want to know more about that.

4
00:00:54,240 --> 00:01:02,440
There are two kinds of latency of mental defilements.

5
00:01:02,440 --> 00:01:14,280
The first one is latency in the mental continuum of beings, and the other is latency in

6
00:01:14,280 --> 00:01:25,940
the objects, understand the latency of mental defilements, we must understand the mental

7
00:01:25,940 --> 00:01:28,100
defilements first.

8
00:01:28,100 --> 00:01:34,300
So what are the mental defilements?

9
00:01:34,300 --> 00:01:42,820
They are called mental defilements because they smear the mind or they make our mind dirty,

10
00:01:42,820 --> 00:01:54,900
so they are called mental defilements, but the explanation given in the commentaries

11
00:01:54,900 --> 00:02:03,940
is that they are the ones that afflict the mind or that torment the mind, so they are

12
00:02:03,940 --> 00:02:13,660
tormentors of mind or those that afflict the mind, so they are called key lisa in

13
00:02:13,660 --> 00:02:32,100
Pali, those that defile or that are defiled, so this what mental defilement comes to be used

14
00:02:32,100 --> 00:02:36,980
for the what key lisa in Pali.

15
00:02:36,980 --> 00:02:51,060
Now there are ten mental defilements or ten key lisa's, so they begin with great head

16
00:02:51,060 --> 00:03:13,420
delusion, pride, wrong view, doubt, and then wrong view and doubt, so the other four

17
00:03:13,420 --> 00:03:33,740
are sloth and then restlessness and shamelessness in doing unwholesome deeds and fearlessness

18
00:03:33,740 --> 00:03:45,180
in doing unwholesome deeds, you don't need to know all these ten, one by one, so please

19
00:03:45,180 --> 00:03:56,300
know that they are just the mental states that make our mind dirty, so whenever one or

20
00:03:56,300 --> 00:04:04,340
more of these mental state arise in our minds, then our minds are said to be impure,

21
00:04:04,340 --> 00:04:15,820
we are said to have unwholesome, mind or unwholesome consciousness, and the aim of the

22
00:04:15,820 --> 00:04:24,420
ultimate aim of Vipasana is to get rid of these mental defilements.

23
00:04:24,420 --> 00:04:31,540
Now purification of beings of purification of mind simply means eradication of these

24
00:04:31,540 --> 00:04:37,140
mental defilements, once the mental defilements are eradicated and they no longer arise

25
00:04:37,140 --> 00:04:43,380
in the mind, then mind becomes totally pure.

26
00:04:43,380 --> 00:04:49,660
Sometimes these mental defilements really arise in our minds, for example, when we

27
00:04:49,660 --> 00:04:56,660
are angry, the anger arises in our minds, when we are attached to something, then there

28
00:04:56,660 --> 00:05:02,060
is greed or attachment in our minds and so on.

29
00:05:02,060 --> 00:05:11,500
When they get the necessary conditions, then they arise in our minds.

30
00:05:11,500 --> 00:05:21,780
When they arise in our minds, we can do something so that they just stay there at the

31
00:05:21,780 --> 00:05:35,700
rising stage and they do not proceed to the stage of breaking the precepts or transgressions.

32
00:05:35,700 --> 00:05:44,740
Both, I am angry with the person, so when I am angry with him, then anger has risen

33
00:05:44,740 --> 00:05:47,460
in my mind.

34
00:05:47,460 --> 00:05:54,940
I can control that anger and not let it to reach the stage of transgression.

35
00:05:54,940 --> 00:06:05,580
That means to quarrel with him, to hit him or even to kill him, so I can control

36
00:06:05,580 --> 00:06:10,540
my anger not to reach that stage.

37
00:06:10,540 --> 00:06:19,300
So there are now, we see two stages of mental defilements that arise in our mind and

38
00:06:19,300 --> 00:06:31,340
then reach the stage of action, doing something depending on that mental defilement.

39
00:06:31,340 --> 00:06:44,260
Do not live in our mind constantly, not always, only once in a while they arise in our

40
00:06:44,260 --> 00:06:46,220
minds.

41
00:06:46,220 --> 00:06:52,860
So when they do not arise in our minds, where do they live?

42
00:06:52,860 --> 00:07:12,740
I said to be lying dormant in our minds, but that does not mean that they have arisen

43
00:07:12,740 --> 00:07:18,540
and then they lie under the surface or something like that.

44
00:07:18,540 --> 00:07:29,340
What it means is there is a liability for them to arise, so there is a potential for them

45
00:07:29,340 --> 00:07:35,180
to arise when the conditions are met.

46
00:07:35,180 --> 00:07:48,500
Three levels of mental defilements, the latent level and then arising or coming

47
00:07:48,500 --> 00:07:56,180
up level, that means coming up to the surface and then acting level or transgression level.

48
00:07:56,180 --> 00:08:06,900
So there are three levels of mental defilements, the latent level is the most difficult

49
00:08:06,900 --> 00:08:14,140
to destroy or to eradicate.

50
00:08:14,140 --> 00:08:26,500
And there are three levels of mental defilements, both give us three weapons to deal with

51
00:08:26,500 --> 00:08:29,300
each one of these levels.

52
00:08:29,300 --> 00:08:39,260
And these three weapons are sealer, moral purity, samadhi, concentration and wisdom,

53
00:08:39,260 --> 00:08:50,780
I mean, Banya wisdom.

54
00:08:50,780 --> 00:08:57,380
When you take precepts and you really keep them, then you will not reach the transgression

55
00:08:57,380 --> 00:08:59,260
stage.

56
00:08:59,260 --> 00:09:05,540
Even though you are angry, you will not do anything out of anger and you will not kill

57
00:09:05,540 --> 00:09:13,860
or even if you are attached to something, if you really desire something, you will not

58
00:09:13,860 --> 00:09:19,980
take it forcefully, you will not steal it because you have taken the precepts.

59
00:09:19,980 --> 00:09:33,260
So taking the precepts helps you to deal with that mental defilements at transgression

60
00:09:33,260 --> 00:09:35,420
level.

61
00:09:35,420 --> 00:09:44,540
And actually that is the easiest of the three to get rid of, I mean, to prevent.

62
00:09:44,540 --> 00:09:50,780
You just take the precepts and you keep them and even though you are attached to something,

63
00:09:50,780 --> 00:09:56,700
you will not take it by force, you will not steal it, even though you are angry, you will

64
00:09:56,700 --> 00:10:04,220
not act upon anger and fight with the person or even kill him because you have taken upon

65
00:10:04,220 --> 00:10:11,340
yourself the precepts of not killing, not stealing and so on.

66
00:10:11,340 --> 00:10:25,620
So sealer can help us to avoid a transgression, but it cannot help us to prevent mental

67
00:10:25,620 --> 00:10:29,860
defilements from arising in our minds.

68
00:10:29,860 --> 00:10:40,780
Now please note that sealer is actually the control of bodily actions and vocal actions

69
00:10:40,780 --> 00:10:47,420
or sealer controls your body and your speech, not your mind.

70
00:10:47,420 --> 00:10:57,780
Although mine is involved when we keep precepts, when we try to avoid killing and so on,

71
00:10:57,780 --> 00:11:07,300
the sealer's task is to control the bodily actions and vocal actions, to prevent mental

72
00:11:07,300 --> 00:11:11,300
defilements from reaching the arising stage.

73
00:11:11,300 --> 00:11:21,980
We need to do some other thing and that is some hard concentration of meditation because

74
00:11:21,980 --> 00:11:25,780
meditation deals with our mind.

75
00:11:25,780 --> 00:11:31,620
Now when you meditate, you just sit and you try to control your mind or you try to be mindful

76
00:11:31,620 --> 00:11:35,300
of the object at the present moment.

77
00:11:35,300 --> 00:11:46,820
So by the practice of meditation, we will be able to prevent the mental defilements from

78
00:11:46,820 --> 00:11:56,740
reaching the arising stage, two kinds of meditation, samata or tranquility meditation and

79
00:11:56,740 --> 00:12:07,620
Vipasana meditation, so both meditations can help us to keep these mental defilements away

80
00:12:07,620 --> 00:12:12,460
from the stage of arising.

81
00:12:12,460 --> 00:12:25,780
But then keeping them away is not absolute or not total, not permanent.

82
00:12:25,780 --> 00:12:33,340
So by the practice of samata meditation or Vipasana meditation, we are able to keep them

83
00:12:33,340 --> 00:12:39,300
away for just for some time.

84
00:12:39,300 --> 00:12:43,620
We cannot keep them away all together from our minds.

85
00:12:43,620 --> 00:12:53,420
It was a person practices samata meditation and he catch jhanas and abinas or supernormal

86
00:12:53,420 --> 00:13:03,140
knowledge, so by the practice of samata and by the attainment of jhanas and supernormal

87
00:13:03,140 --> 00:13:11,420
knowledge, he is able to keep these mental defilements away from a longer period of times,

88
00:13:11,420 --> 00:13:17,380
maybe months or even years.

89
00:13:17,380 --> 00:13:26,060
But they are not eradicated, the mental defilements are not just right all together.

90
00:13:26,060 --> 00:13:34,820
And so when they meet with the conditions, necessary conditions, then these mental defilements

91
00:13:34,820 --> 00:13:38,020
will arise in their minds again.

92
00:13:38,020 --> 00:13:45,860
So in one of his first existences, Bodhisattva was a helmet.

93
00:13:45,860 --> 00:13:53,660
So as a helmet, he lived in the Himalayas and he practiced meditation and he caught jhanas

94
00:13:53,660 --> 00:13:56,940
and supernormal powers.

95
00:13:56,940 --> 00:14:03,420
So one day he went to the city and met the king and the king was pleased with him and

96
00:14:03,420 --> 00:14:08,180
so the king asked him to stay at the Royal Garden.

97
00:14:08,180 --> 00:14:13,380
And also the king offered food to him every day.

98
00:14:13,380 --> 00:14:28,460
So the helmet went to the palace every day to accept food and okay.

99
00:14:28,460 --> 00:14:37,860
One time there was a rebellion and the king had to go out and suppress it.

100
00:14:37,860 --> 00:14:46,500
So before he went out, he asked his queen to take care of the helmet.

101
00:14:46,500 --> 00:14:53,860
So in his absence, the helmet went to the palace to receive food as usual.

102
00:14:53,860 --> 00:15:00,620
And whenever he went, he went through the air and he entered through the window.

103
00:15:00,620 --> 00:15:16,260
So one day the queen bathed and then put on very fine and smooth clothes, garments.

104
00:15:16,260 --> 00:15:22,220
And she was waiting for the helmet to come, maybe she was lying down.

105
00:15:22,220 --> 00:15:28,620
Then the helmet came and he entered through the window.

106
00:15:28,620 --> 00:15:39,340
When the queen hurt the noise of his robes, she caught up and unfortunately the garment fell

107
00:15:39,340 --> 00:15:46,300
off and the helmet saw the exposed body of the queen.

108
00:15:46,300 --> 00:15:56,380
And so when he saw the queen in that condition, the mental defalments, long suppressed,

109
00:15:56,380 --> 00:16:10,020
came up to him, so he was not able to control himself.

110
00:16:10,020 --> 00:16:21,300
So by the help of some mathematician, one can keep the mental defalments away for some

111
00:16:21,300 --> 00:16:28,060
time only and not for all time.

112
00:16:28,060 --> 00:16:37,420
Like in this story, these mental defalments can just arise when there are conditions

113
00:16:37,420 --> 00:16:42,420
for them.

114
00:16:42,420 --> 00:16:52,700
This reminds me of a story that was told in Burma and that was a popular in Burma.

115
00:16:52,700 --> 00:17:04,380
Now about 200 years ago there was a king and he had a minister who was very clever.

116
00:17:04,380 --> 00:17:11,020
So that king had a cat.

117
00:17:11,020 --> 00:17:22,500
He trained his cat so so well that when he did the service to the Buddha statue, when

118
00:17:22,500 --> 00:17:33,740
he bought onto the statue and did the recitation, the cat hauled the candle for him in his

119
00:17:33,740 --> 00:17:35,380
hands.

120
00:17:35,380 --> 00:17:41,940
So he was very proud of that cat and so he told about the cat to the minister because

121
00:17:41,940 --> 00:17:46,020
he expected the minister to praise the cat.

122
00:17:46,020 --> 00:17:55,260
But the minister said, you are majesty, that is because the cat has not seen what he likes.

123
00:17:55,260 --> 00:18:09,420
So the king was displeased but at night, that minister caught a mouse during the day and

124
00:18:09,420 --> 00:18:17,460
he took it to where the king was doing his service to the Buddha statue and then he

125
00:18:17,460 --> 00:18:20,060
released the cat the mouse.

126
00:18:20,060 --> 00:18:25,220
So the mouse ran in front of the cat and as soon as they get saw the mouse, he throw away

127
00:18:25,220 --> 00:18:35,620
the candle and ran up to the mouse.

128
00:18:35,620 --> 00:18:43,620
We must nug and also prevent the mental defalments from reaching the stage of a rising.

129
00:18:43,620 --> 00:18:57,460
Now when you make notes of the objects or when you are mindful of the objects or when

130
00:18:57,460 --> 00:19:05,420
you practice mindfulness, mental defalments do not get chance to enter your mind.

131
00:19:05,420 --> 00:19:12,100
So when you practice Vipasana, meditation, you are able to keep the mental defalments away

132
00:19:12,100 --> 00:19:23,620
but just for a moment, the moment you stop practicing mindfulness, they can come in.

133
00:19:23,620 --> 00:19:32,820
So by the practice of the matama meditation or Vipasana meditation, people can keep these

134
00:19:32,820 --> 00:19:41,900
mental defalments away from them for some time only or for a moment only, not for good.

135
00:19:41,900 --> 00:19:54,180
Now the deepest level or the latent level of the mental defalments can be eradicated by

136
00:19:54,180 --> 00:20:00,740
enlightenment only or by the putt consciousness only.

137
00:20:00,740 --> 00:20:09,860
So when a person becomes enlightened or when a person attains enlightenment, a type of

138
00:20:09,860 --> 00:20:19,300
consciousness arises in that person, a type of consciousness that he has never experienced

139
00:20:19,300 --> 00:20:21,260
before.

140
00:20:21,260 --> 00:20:28,980
So that consciousness is called path consciousness and that consciousness has the power to

141
00:20:28,980 --> 00:20:37,060
destroy the mental defalments altogether so that they do not arise again in his mind.

142
00:20:37,060 --> 00:20:45,540
So that is total, total abandonment or eradication of mental defalments.

143
00:20:45,540 --> 00:20:53,700
So the eradication of mental defalments or the removal of the deepest level of the

144
00:20:53,700 --> 00:21:00,980
mental defalment can be achieved only through the putt consciousness.

145
00:21:00,980 --> 00:21:08,180
But in order to reach the putt consciousness, what must one do?

146
00:21:08,180 --> 00:21:10,460
Practice Vipasana.

147
00:21:10,460 --> 00:21:20,900
So Vipasana is strictly speaking Vipasana does not eradicate mental defalments altogether

148
00:21:20,900 --> 00:21:29,460
but the putt consciousness arises as an outcome of Vipasana practice.

149
00:21:29,460 --> 00:21:37,740
Without Vipasana, there can be no putt consciousness and so there can be no attainment

150
00:21:37,740 --> 00:21:40,220
of enlightenment.

151
00:21:40,220 --> 00:21:48,980
So in order to deal with the deepest level of mental defalments, in order to get rid of

152
00:21:48,980 --> 00:21:59,300
the deepest level of mental defalments, we practice Vipasana meditation.

153
00:21:59,300 --> 00:22:09,980
So the mental defalments and the deepest level or the latent level are the most difficult

154
00:22:09,980 --> 00:22:17,540
to get rid of and they are said to be latent in our mental continuum.

155
00:22:17,540 --> 00:22:24,060
So they are not, they are not the ones that has already arisen but they are something

156
00:22:24,060 --> 00:22:32,500
like a potential in our minds, say for them to arise.

157
00:22:32,500 --> 00:22:37,100
We can take an example of a much box of matches.

158
00:22:37,100 --> 00:22:45,100
You keep the box of matches from young children from small children, why?

159
00:22:45,100 --> 00:22:53,300
Because there is a possibility that they may play with it and they may get burnt or in other

160
00:22:53,300 --> 00:23:02,020
words there is potential fire in the box of matches.

161
00:23:02,020 --> 00:23:15,580
In the same way, although mental defalments are not always present, not always arising

162
00:23:15,580 --> 00:23:23,060
in our minds, there is a potential in our minds for them to arise.

163
00:23:23,060 --> 00:23:30,820
And that potential or that liability is what we call the latent stage or latent states.

164
00:23:30,820 --> 00:23:52,140
So they have not reached the stage of arising and continuing but there is a tendency or

165
00:23:52,140 --> 00:23:56,980
the potential for them to arise.

166
00:23:56,980 --> 00:24:02,660
And that is what we call the latent states.

167
00:24:02,660 --> 00:24:10,100
Can we say that fire exists in the match box?

168
00:24:10,100 --> 00:24:15,980
No, if it does, it will be burning.

169
00:24:15,980 --> 00:24:22,380
So fire is not existent in the match box but there is a liability that when someone strikes

170
00:24:22,380 --> 00:24:26,620
the match, the fire will be produced.

171
00:24:26,620 --> 00:24:37,740
So in the same way, mental defalments not always exist in our minds.

172
00:24:37,740 --> 00:24:44,860
But there is a liability that they will arise when the necessary conditions are met.

173
00:24:44,860 --> 00:24:55,220
Although they arise only when the conditions are met, we say that they are with us or we

174
00:24:55,220 --> 00:25:04,260
are with them because so long as we have not destroyed them all together, so long as we

175
00:25:04,260 --> 00:25:16,500
have not eradicated them, we are set to have these mental defalments, say it was in

176
00:25:16,500 --> 00:25:18,020
who smokes.

177
00:25:18,020 --> 00:25:24,700
So although at the moment he is not smoking, if you ask him, do you smoke?

178
00:25:24,700 --> 00:25:34,460
And he will say I do because he has this habit of smoking and he has not given up this smoking.

179
00:25:34,460 --> 00:25:37,500
So he will say I smoke.

180
00:25:37,500 --> 00:25:45,580
So in the same way, if somebody asks us whether we have mental defalments, we will say yes.

181
00:25:45,580 --> 00:25:53,820
Although at this very moment, I hope we have no mental defalments in our minds.

182
00:25:53,820 --> 00:25:59,300
Say we are practicing dharma, I am giving a dharma talk and you are listening to the

183
00:25:59,300 --> 00:26:04,500
dharma talk and so there are no mental defalments in our minds.

184
00:26:04,500 --> 00:26:11,220
But if somebody asks us are you free from mental defalments, we will say no.

185
00:26:11,220 --> 00:26:23,100
Same dormant in our minds is what we call latency in the mental continuum.

186
00:26:23,100 --> 00:26:34,620
So the mental defalments, although they do not reach the real stage of arising, they

187
00:26:34,620 --> 00:26:46,180
are set to be lying dormant in our minds and that lying dormant is what we call the level

188
00:26:46,180 --> 00:26:54,300
of latency or latency in the mental continuum of beings.

189
00:26:54,300 --> 00:27:05,260
Another kind of latency and that is latency in the objects.

190
00:27:05,260 --> 00:27:14,140
We encounter many kinds of objects, material objects as well as mental objects.

191
00:27:14,140 --> 00:27:30,340
So whatever the object is, if we are not mindful, we as it were put the mental defalments

192
00:27:30,340 --> 00:27:32,940
into those objects.

193
00:27:32,940 --> 00:27:40,700
That means suppose I see a beautiful object, so I like it and so I have a attachment

194
00:27:40,700 --> 00:27:43,300
to that object.

195
00:27:43,300 --> 00:27:53,620
So when I have a attachment to that object, it is actually I am injecting this mental

196
00:27:53,620 --> 00:27:56,420
defiments into that object.

197
00:27:56,420 --> 00:28:03,540
So that is what we call latency of mental defalments in the object.

198
00:28:03,540 --> 00:28:08,220
Actually mental defilements are in the mind and not in the object.

199
00:28:08,220 --> 00:28:14,500
But since I have taken this object as a desirable object and I have an attachment with

200
00:28:14,500 --> 00:28:25,300
this object, it is as though I have put or inject the mental defilements in this object.

201
00:28:25,300 --> 00:28:31,700
So once mental defilement is put in the object, it stays there always.

202
00:28:31,700 --> 00:28:38,180
That means every time you encounter this object, it comes along with the object.

203
00:28:38,180 --> 00:28:42,860
Along with that mental defilements you have put in it.

204
00:28:42,860 --> 00:28:47,780
So that is called latency in the objects.

205
00:28:47,780 --> 00:28:54,060
And when we have injected mental defilements into the objects, since they are mental

206
00:28:54,060 --> 00:28:59,540
defilements, they lead us to action.

207
00:28:59,540 --> 00:29:05,500
And mental defilements lead us to unholy some actions.

208
00:29:05,500 --> 00:29:20,140
Sometimes I am attached to this object, I will protect it and at the risk of my life,

209
00:29:20,140 --> 00:29:24,340
I will protect it, something like that.

210
00:29:24,340 --> 00:29:30,340
And if somebody comes and then I will fight with him or something like that, so it leads

211
00:29:30,340 --> 00:29:40,580
to action, the mental defilement or the attachment to the object or aversion to the object.

212
00:29:40,580 --> 00:29:46,020
So these lead to the action and these actions are called kama.

213
00:29:46,020 --> 00:29:54,900
So when there is kama, there is the result of kama as the rebirth in this case rebirth

214
00:29:54,900 --> 00:29:57,820
in waffle states.

215
00:29:57,820 --> 00:30:06,660
So every time we put the mental defilement in the object, we are prolonging our state

216
00:30:06,660 --> 00:30:10,100
in the samsara.

217
00:30:10,100 --> 00:30:19,380
We are creating our own suffering because the result of acoustalized always undesirable

218
00:30:19,380 --> 00:30:22,340
or unpleasant.

219
00:30:22,340 --> 00:30:31,500
So we must understand that every time we put a mental defilement in the object, we are

220
00:30:31,500 --> 00:30:38,700
prolonging our state in the samsara.

221
00:30:38,700 --> 00:30:47,320
The practice of mindfulness or the practice of vipassana can help us to prevent injecting

222
00:30:47,320 --> 00:30:54,340
mental defilements in the objects because when we are mindful and we say seeing, seeing,

223
00:30:54,340 --> 00:31:03,380
seeing, or hearing hearing and so on, we do not take this object as desirable or undesirable.

224
00:31:03,380 --> 00:31:09,260
So when we do not take this as desirable or undesirable, we will not be attached to it,

225
00:31:09,260 --> 00:31:13,180
we will not have aversion towards it.

226
00:31:13,180 --> 00:31:24,780
So the practice of mindfulness can help us not to put mental defilements into the objects.

227
00:31:24,780 --> 00:31:35,020
So when you practice vipassana meditation, you practice mindfulness and in the beginning,

228
00:31:35,020 --> 00:31:51,320
you may not even see the object clearly, but as your practice strengthens, as your concentration

229
00:31:51,320 --> 00:31:57,660
becomes stronger, you will be able to see the objects clearly and you will be able to

230
00:31:57,660 --> 00:32:06,780
see the conditionality of the objects and you will be able to see that these objects

231
00:32:06,780 --> 00:32:11,780
come and go and so you see their impermanence and so on.

232
00:32:11,780 --> 00:32:20,300
So when you see these characteristics of all conditions for nomena, that is, impermanence,

233
00:32:20,300 --> 00:32:28,460
suffering and non-so nature of things, you will not be attached to that object or you

234
00:32:28,460 --> 00:32:31,620
will not hate that object and so on.

235
00:32:31,620 --> 00:32:38,900
And so you are able to avoid putting mental defilements into that object.

236
00:32:38,900 --> 00:32:47,260
So when you do not put in mental defilements in the object, there will be no acting depending

237
00:32:47,260 --> 00:32:52,100
on the mental defilements, simply because there are no mental defilements.

238
00:32:52,100 --> 00:32:58,660
So when there are no acting, when there is no acting, that means when there is no action,

239
00:32:58,660 --> 00:33:03,140
when there is no comma, there will be no result of comma.

240
00:33:03,140 --> 00:33:18,660
And so the future lives or future aggregates, that would happen if you cannot prevent

241
00:33:18,660 --> 00:33:24,620
putting mental defilements in the objects will not arise.

242
00:33:24,620 --> 00:33:34,780
So that is, you are cutting the flow of rebirds.

243
00:33:34,780 --> 00:33:44,340
So you are cutting short, the Samsara, you are Samsara.

244
00:33:44,340 --> 00:33:55,060
So with the help of Vipasana, we can prevent putting mental defilements in the objects,

245
00:33:55,060 --> 00:34:03,620
we can prevent the latency of mental defilements in the objects.

246
00:34:03,620 --> 00:34:09,740
And as we go along, we will be able to prevent this more and more.

247
00:34:09,740 --> 00:34:18,940
And when our mental practice of Vipasana matures, then as a result of the practice of

248
00:34:18,940 --> 00:34:27,740
Vipasana or as the outcome of Vipasana, the path consciousness will arise.

249
00:34:27,740 --> 00:34:41,220
So when path consciousness arises, then it is able to do away with the even the deepest

250
00:34:41,220 --> 00:34:45,620
level of mental defilements.

251
00:34:45,620 --> 00:34:57,180
So only when path consciousness arises can all mental defilements be eradicated so that

252
00:34:57,180 --> 00:35:02,340
they do not arise again in that person.

253
00:35:02,340 --> 00:35:15,020
And when we see the mental defilements are just try to remove or eradicate it, we mean

254
00:35:15,020 --> 00:35:24,540
that they are rendered incapable of arising again, not that they are there and then we just

255
00:35:24,540 --> 00:35:28,460
try them.

256
00:35:28,460 --> 00:35:36,900
When mental defilements arise in our mind, there can be no Vipasana or there can be no

257
00:35:36,900 --> 00:35:38,940
path consciousness.

258
00:35:38,940 --> 00:35:46,860
So when there is no path consciousness, there can be no eradication of mental defilements.

259
00:35:46,860 --> 00:35:55,820
So the eradication of mental defilements really means rendering them incapable of arising

260
00:35:55,820 --> 00:35:58,620
again, arising in the future.

261
00:35:58,620 --> 00:36:05,540
So that is what we should understand by the expression removal of mental defilements or

262
00:36:05,540 --> 00:36:11,060
abandonment of mental defilements or eradication of mental defilements.

263
00:36:11,060 --> 00:36:22,100
Now when you are able to avoid putting mental defilements in the objects, it is because

264
00:36:22,100 --> 00:36:31,700
you are able to see the object as it is without any additions of your own.

265
00:36:31,700 --> 00:36:40,580
So you see the object and you take the object as it is and not taking it as it is beautiful

266
00:36:40,580 --> 00:36:46,380
object or it is an ugly object or this object I like or this object I don't like.

267
00:36:46,380 --> 00:36:54,900
So without putting anything onto the objects, you just take the object as it is.

268
00:36:54,900 --> 00:37:06,460
That is why you are able to avoid injecting mental defilements into the object.

269
00:37:06,460 --> 00:37:21,500
And that being able to take the object as it is described as an achievement of the yogis.

270
00:37:21,500 --> 00:37:35,660
Now Buddha taught that things are impermanent, but if we think an object is permanent,

271
00:37:35,660 --> 00:37:42,940
that means we are adding something unreal to that object.

272
00:37:42,940 --> 00:37:50,140
Although the object is impermanent, we are putting the permanency onto the object.

273
00:37:50,140 --> 00:38:01,980
So in that case, we are taking the object not as it is but as we like, as we like it

274
00:38:01,980 --> 00:38:12,780
to be. So when we take something impermanent to be permanent, we are adding this permanency

275
00:38:12,780 --> 00:38:16,580
which is unreal to the object.

276
00:38:16,580 --> 00:38:26,500
But when you are able to apply mindfulness to the object, you are able to avoid that.

277
00:38:26,500 --> 00:38:37,220
That ability to avoid adding anything unreal onto the object is described as a great achievement

278
00:38:37,220 --> 00:38:40,260
of a yogi.

279
00:38:40,260 --> 00:38:53,740
Now when we think an object would be permanent, which is really impermanent, that means

280
00:38:53,740 --> 00:38:56,740
we are not satisfied with this object.

281
00:38:56,740 --> 00:39:03,420
And so we are taking something that is real, taking away something real from the object.

282
00:39:03,420 --> 00:39:12,020
We want to take this impermanency out of the object and then in it place, we put in the

283
00:39:12,020 --> 00:39:13,940
permanency.

284
00:39:13,940 --> 00:39:26,420
So when we take some object to be an impermanent object to be permanent, we are in one

285
00:39:26,420 --> 00:39:34,940
way taking away something that is real from the object.

286
00:39:34,940 --> 00:39:46,020
So there are two things, adding something unreal to the object and taking away something

287
00:39:46,020 --> 00:39:50,500
real from the object.

288
00:39:50,500 --> 00:39:58,060
And I think that is what we always do.

289
00:39:58,060 --> 00:40:03,540
We always put something unreal onto the objects and we want to take away what is real

290
00:40:03,540 --> 00:40:11,660
from the objects because we want things to be beautiful, things to be impermanent and so

291
00:40:11,660 --> 00:40:13,740
on.

292
00:40:13,740 --> 00:40:22,940
And so to avoid adding something unreal onto the objects and to avoid taking something

293
00:40:22,940 --> 00:40:28,500
real away from the objects is a very difficult one.

294
00:40:28,500 --> 00:40:38,220
And it can be achieved only through mindfulness meditation, only through constant observation,

295
00:40:38,220 --> 00:40:40,940
only through Vipasana.

296
00:40:40,940 --> 00:40:53,580
We are able to prevent adding something unreal and to take away something real from the object.

297
00:40:53,580 --> 00:41:01,140
We are following the advice given by the Buddha.

298
00:41:01,140 --> 00:41:09,980
The advice given by the Buddha is that they are seeing with regard to what is seen and

299
00:41:09,980 --> 00:41:11,780
so on.

300
00:41:11,780 --> 00:41:19,500
So in other words, that means take the object as it is without any additions.

301
00:41:19,500 --> 00:41:31,900
Buddha gave this advice to two people, one on different occasions.

302
00:41:31,900 --> 00:41:43,780
So once an acetic came to the Buddha.

303
00:41:43,780 --> 00:41:53,740
He wanted to learn from the Buddha and so he said that he walked the whole night.

304
00:41:53,740 --> 00:42:00,420
When he reached the monastery, Buddha had already gone out to the village for arms.

305
00:42:00,420 --> 00:42:05,260
So he asked the monks where the Buddha was and when they told him that Buddha was on his

306
00:42:05,260 --> 00:42:12,580
arms round, he went after the Buddha and sought him out.

307
00:42:12,580 --> 00:42:17,860
And when he met the Buddha, he just said, Pandey, please teach me something in brief so

308
00:42:17,860 --> 00:42:20,260
that I can practice.

309
00:42:20,260 --> 00:42:25,940
And Buddha said, it is not the time to ask a question or to answer a question, I am on the

310
00:42:25,940 --> 00:42:27,500
arms round.

311
00:42:27,500 --> 00:42:29,980
So you just wait.

312
00:42:29,980 --> 00:42:32,780
But he was persistent.

313
00:42:32,780 --> 00:42:37,740
He insisted that Buddha gave him the answer for three times.

314
00:42:37,740 --> 00:42:46,340
So after three times, he said, Pandey, we do not know whether I will die next moment or

315
00:42:46,340 --> 00:42:49,300
not or you will die next moment or not.

316
00:42:49,300 --> 00:42:53,540
So please teach me now so that I can practice.

317
00:42:53,540 --> 00:42:59,980
So you know why when going on arms round, Buddha just gave this advice, very short advice.

318
00:42:59,980 --> 00:43:10,420
And that has become a very famous advice and every practitioner of Vipasana knows this

319
00:43:10,420 --> 00:43:12,140
advice given by the Buddha.

320
00:43:12,140 --> 00:43:17,780
And that is, let there be seeing with regard to what is seen, let there be hearing with

321
00:43:17,780 --> 00:43:22,980
regard to what is heard, let there be sensing with regard to what is sensed.

322
00:43:22,980 --> 00:43:35,460
That means what is smelled, what is tested, what is touched.

323
00:43:35,460 --> 00:43:41,900
And let there be just thinking with regard to what is thought.

324
00:43:41,900 --> 00:43:45,900
So this is a very short advice given by the Buddha.

325
00:43:45,900 --> 00:43:51,180
And it was enough for that aesthetic because he was a gifted person actually, although

326
00:43:51,180 --> 00:43:59,340
he was a, he belonged to another teaching.

327
00:43:59,340 --> 00:44:07,980
So he practiced following this short advice of the Buddha and it is said that before

328
00:44:07,980 --> 00:44:16,220
Buddha reached back to the monastery, he became an other hand and then he came to the

329
00:44:16,220 --> 00:44:24,100
Buddha and asked to ordain him, but Buddha knew that he could not become a monk in that

330
00:44:24,100 --> 00:44:25,580
life.

331
00:44:25,580 --> 00:44:31,980
So Buddha said, we do not ordain those who have no ropes and bowls and so you go and

332
00:44:31,980 --> 00:44:39,340
fetch the ropes and bowls and so he went out to fetch ropes and bowls and he was attacked

333
00:44:39,340 --> 00:44:44,660
by a cow and then he died.

334
00:44:44,660 --> 00:44:55,060
So when you see something and you stop at just seeing, you are putting an end to suffering

335
00:44:55,060 --> 00:44:58,540
regarding that object.

336
00:44:58,540 --> 00:45:09,900
So regarding that object, since you have no attachment or no aversion towards the object,

337
00:45:09,900 --> 00:45:20,700
you do not get any unwholesome state from that object and so you are able to avoid gamma

338
00:45:20,700 --> 00:45:30,860
which will send you to the full, full, waffle state and so the ability to take the object

339
00:45:30,860 --> 00:45:40,980
as it is is actually the ability to make an end of suffering.

340
00:45:40,980 --> 00:45:55,380
If we can take every object as it is, then the putting an end of suffering will be complete,

341
00:45:55,380 --> 00:46:03,620
but we are not able to do that now, so with regard to objects, we may be able to stop

342
00:46:03,620 --> 00:46:06,820
at just seeing, hearing and so on.

343
00:46:06,820 --> 00:46:14,340
But even I think that that is desirable than not getting anything at all.

344
00:46:14,340 --> 00:46:23,820
So this is the immediate result, say we get from the practice of Vipassana.

345
00:46:23,820 --> 00:46:34,220
People think that they do not get immediate results, nowadays people are very oriented towards

346
00:46:34,220 --> 00:46:37,620
getting results, but this is the immediate result.

347
00:46:37,620 --> 00:46:43,060
You get from the practice of Vipassana or the mindfulness meditation.

348
00:46:43,060 --> 00:46:50,500
So you do not get attached to the object or you do not have a version of anger towards

349
00:46:50,500 --> 00:46:51,580
the object.

350
00:46:51,580 --> 00:46:58,300
And so with regard to that particular object, there is no more suffering for you.

351
00:46:58,300 --> 00:47:02,620
So you are making an end of suffering regarding that object.

352
00:47:02,620 --> 00:47:16,620
So if you can make the end of suffering regarding every object, then your freedom from suffering

353
00:47:16,620 --> 00:47:20,100
will be complete.

354
00:47:20,100 --> 00:47:28,540
So please do not think that even if you do not get great results from the practice, you

355
00:47:28,540 --> 00:47:35,700
are getting this immediate result every time, every moment, you apply mindfulness to

356
00:47:35,700 --> 00:47:39,940
the objects.

357
00:47:39,940 --> 00:47:47,260
And to achieve this, you just need to pay attention to the object and take it as it is

358
00:47:47,260 --> 00:47:49,900
without any additions.

359
00:47:49,900 --> 00:48:09,900
So it sounds very simple, but it needs a determination and strenuous effort to achieve this.

360
00:48:09,900 --> 00:48:26,380
So by the practice of mindfulness or by constant observation, may we be able to take the

361
00:48:26,380 --> 00:48:38,580
objects as they are and avoid mental defilements regarding these objects so that in ultimately,

362
00:48:38,580 --> 00:48:55,140
say, we are able to realize the total purity of mind.

