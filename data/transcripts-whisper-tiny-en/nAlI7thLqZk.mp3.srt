1
00:00:00,000 --> 00:00:26,000
So, good evening everyone, broadcasting live, sometime in February, February 5th, 2016.

2
00:00:26,000 --> 00:00:42,000
So, a couple of announcements, one announcement at one big announcement.

3
00:00:42,000 --> 00:00:52,000
I'd say there is a fundraising campaign up, which is, well, I can't talk too much about it.

4
00:00:52,000 --> 00:01:02,000
I can't get involved with fundraising, except to say that, same to say what.

5
00:01:02,000 --> 00:01:08,000
That if we have the support, we'll keep doing this.

6
00:01:08,000 --> 00:01:16,000
If we don't have the support, well, I might have to head back to Stony Creek or Thailand,

7
00:01:16,000 --> 00:01:22,000
maybe I'll just go back to Sri Lanka, live in the cave somewhere.

8
00:01:22,000 --> 00:01:37,000
That was the other announcement, was we got some visitors, aliens, foreigners.

9
00:01:37,000 --> 00:01:47,000
That's Patrick, and that's Thomas.

10
00:01:47,000 --> 00:01:51,000
They're in meditation, that's why they look so somber.

11
00:01:51,000 --> 00:01:59,000
The meditation makes you suffer, don't they look like they're suffering?

12
00:01:59,000 --> 00:02:07,000
The meditation is a very terrible thing, brings out the worst in people.

13
00:02:07,000 --> 00:02:14,000
It actually does, you know, really brings out the worst in people, brings it out and throws it out.

14
00:02:14,000 --> 00:02:19,000
It gets rid of it.

15
00:02:19,000 --> 00:02:25,000
Apropos of the quote tonight.

16
00:02:25,000 --> 00:02:32,000
The quote is apropos, I don't know how that works.

17
00:02:32,000 --> 00:02:37,000
The night's quote is, it's a nice poly quote.

18
00:02:37,000 --> 00:02:39,000
It's very quotable.

19
00:02:39,000 --> 00:02:43,000
This is something you could quote the Buddha saying.

20
00:02:43,000 --> 00:02:44,000
Where is it?

21
00:02:44,000 --> 00:02:46,000
Let's find the poly.

22
00:02:46,000 --> 00:03:12,000
When the mind is defiled, beings are defiled, beings will be defiled.

23
00:03:12,000 --> 00:03:19,000
They are defiled.

24
00:03:19,000 --> 00:03:30,000
They don't know when the mind is cleansed, sata, we switch on, beings are purified.

25
00:03:30,000 --> 00:03:35,000
The purification is in the mind.

26
00:03:35,000 --> 00:03:49,000
It may seem kind of trite or obvious to a Buddhist, but there's a couple of important implications to this verse.

27
00:03:49,000 --> 00:04:04,000
The first is sort of a attack on the concept of purity and other religions.

28
00:04:04,000 --> 00:04:12,000
In the time of the Buddha, they would, well, even today, they bathe in the ganga river.

29
00:04:12,000 --> 00:04:19,000
I mean, yeah, thinking that it's going to purify them.

30
00:04:19,000 --> 00:04:22,000
That's the idea of purification and Hinduism.

31
00:04:22,000 --> 00:04:23,000
That's one idea.

32
00:04:23,000 --> 00:04:26,000
It's not the idea, but a lot of people think that.

33
00:04:26,000 --> 00:04:32,000
So they take tracks up to the mountains, the source of the ganga nadi,

34
00:04:32,000 --> 00:04:40,000
the river ganges as it's the Brits called it.

35
00:04:40,000 --> 00:04:43,000
Thinking that it would purify them.

36
00:04:43,000 --> 00:04:54,000
It was that way in the Buddhist time and even today, there's this idea that it purifies.

37
00:04:54,000 --> 00:05:02,000
In other religions, in many religions, there's the idea that purification comes from God.

38
00:05:02,000 --> 00:05:09,000
And purification comes through prayer.

39
00:05:09,000 --> 00:05:25,000
purification comes through ritual.

40
00:05:25,000 --> 00:05:44,000
So you have to perform to become purified.

41
00:05:44,000 --> 00:05:51,000
Some religions are about purification of water, purification of smoke, and many different kind of ways of purification.

42
00:05:51,000 --> 00:05:57,000
So this is a big thing that the Buddha was addressing was, no, it's the mind.

43
00:05:57,000 --> 00:06:00,000
The mind just would purify his being.

44
00:06:00,000 --> 00:06:02,000
So that's the obvious one.

45
00:06:02,000 --> 00:06:08,000
That's what we often hear talked about, but I think a more important implication of this,

46
00:06:08,000 --> 00:06:19,000
what it's saying, is that being the being is dependent on the mind, not the other way around.

47
00:06:19,000 --> 00:06:23,000
The mind doesn't come from the being, the being comes from the mind.

48
00:06:23,000 --> 00:06:27,000
Mark that well, that's important.

49
00:06:27,000 --> 00:06:37,000
So ordinary thinking is who you are determines your mind's status.

50
00:06:37,000 --> 00:06:43,000
An angry person gets angry. Why? Because they're an angry person.

51
00:06:43,000 --> 00:06:49,000
It's sloppy thinking, but we fall into it crazily.

52
00:06:49,000 --> 00:06:52,000
I'm an alcoholic.

53
00:06:52,000 --> 00:06:54,000
I'm an addict.

54
00:06:54,000 --> 00:06:56,000
I don't agree with it really.

55
00:06:56,000 --> 00:06:59,000
I just off the top of my hand it sounds wrong.

56
00:06:59,000 --> 00:07:03,000
I know alcoholics anonymous is big in this identifying yourself as an alcoholic.

57
00:07:03,000 --> 00:07:08,000
Kind of understand the rationale behind it, but there's something there that's a little bit.

58
00:07:08,000 --> 00:07:13,000
I don't know. I don't want to go attacking.

59
00:07:13,000 --> 00:07:20,000
What appears to be a really good addiction counseling service, but not convinced that it's entirely perfect.

60
00:07:20,000 --> 00:07:26,000
I mean, hey, I bet meditation would be a really good addiction service.

61
00:07:26,000 --> 00:07:29,000
Can't say that.

62
00:07:29,000 --> 00:07:36,000
I don't know off the top of my head of any alcoholics who came and gave it up for meditation.

63
00:07:36,000 --> 00:07:44,000
I do remember one guy after he finished his course wearing white clothes in Thailand.

64
00:07:44,000 --> 00:07:48,000
Went down into the village in order three beers.

65
00:07:48,000 --> 00:07:51,000
I mean, it seems kind of, you know, so what?

66
00:07:51,000 --> 00:07:56,000
But well, he just finished an entire meditation course and he was still wearing his white clothes.

67
00:07:56,000 --> 00:07:59,000
And so there I am up on the mountain in the meditation center.

68
00:07:59,000 --> 00:08:05,000
And someone comes to me and says, one year meditators snuck out to go get drunk.

69
00:08:05,000 --> 00:08:09,000
And he was still staying with us.

70
00:08:09,000 --> 00:08:13,000
The thing he had given up eight precepts, so he was on five precepts.

71
00:08:13,000 --> 00:08:19,000
And he didn't understand that five precepts is in the point being you're staying with us.

72
00:08:19,000 --> 00:08:23,000
And oh boy, did I tear into any notice?

73
00:08:23,000 --> 00:08:26,000
You know, it is response was, which is defense was.

74
00:08:26,000 --> 00:08:31,000
Well, I ordered six spirits, but I only drank three of them.

75
00:08:31,000 --> 00:08:39,000
He really thought that was that and he didn't realize that not drinking alcohol was part of the five precepts, which is probably my fault.

76
00:08:39,000 --> 00:08:43,000
Show this lack of instruction.

77
00:08:43,000 --> 00:08:45,000
Anyway, yeah.

78
00:08:45,000 --> 00:08:53,000
I'm hoping that that's not a, I mean, it's kind of shameful to know that someone who finishes our course is still going to go out and drink.

79
00:08:53,000 --> 00:08:55,000
Shouldn't happen like that.

80
00:08:55,000 --> 00:09:02,000
But I mean, I would defend, the defense would be some people slip to the cracks.

81
00:09:02,000 --> 00:09:08,000
But I would, I would argue that probably a pretty, you know, why wouldn't it be?

82
00:09:08,000 --> 00:09:10,000
But anyway, totally off track there.

83
00:09:10,000 --> 00:09:12,000
I'll apologize.

84
00:09:12,000 --> 00:09:18,000
The point I was trying to make was

85
00:09:18,000 --> 00:09:30,000
that our state of being who we are quote unquote is based simply into a solely on the habits that we form.

86
00:09:30,000 --> 00:09:40,000
Not on some predetermined genetic, et cetera, et cetera, a state of being, you know, it's all habitual.

87
00:09:40,000 --> 00:09:44,000
It's all habits, it's artificial.

88
00:09:44,000 --> 00:09:50,000
So if you say you're an angry person, what that means is you've developed over time a habit to get angry.

89
00:09:50,000 --> 00:10:04,000
If you're an addiction person while you've cultivated addiction and by that very nature, by that very fact,

90
00:10:04,000 --> 00:10:06,000
the habits can be unlearned.

91
00:10:06,000 --> 00:10:10,000
You can can head in the other direction.

92
00:10:10,000 --> 00:10:21,000
You can argue it gets more and more difficult, the more you get addicted or more deeply ingrained the habit becomes and I agree with that for sure.

93
00:10:21,000 --> 00:10:25,000
It doesn't mean it's who you are.

94
00:10:25,000 --> 00:10:32,000
The mind comes first, the mind precedes all things.

95
00:10:32,000 --> 00:10:37,000
It's just a matter of what you're working for.

96
00:10:37,000 --> 00:10:43,000
So if you want to talk about why we are the way we are, we start with the mind.

97
00:10:43,000 --> 00:10:46,000
We start with the habits that we cultivate.

98
00:10:46,000 --> 00:10:50,000
And so meditations is just another habit.

99
00:10:50,000 --> 00:10:57,000
We're cultivating a wholesome habit, a habit of objectivity, a habit of mental clarity.

100
00:10:57,000 --> 00:10:59,000
Let's say you should look at it.

101
00:10:59,000 --> 00:11:00,000
It's not magic.

102
00:11:00,000 --> 00:11:05,000
It's not like you can count up how many hours you've done and somehow that mean anything.

103
00:11:05,000 --> 00:11:07,000
It doesn't mean anything.

104
00:11:07,000 --> 00:11:22,000
All that means anything is how, how often how frequently and how clearly you're able to set your mind.

105
00:11:22,000 --> 00:11:30,000
So every moment that your mind is clear, you're cultivating that habit of clarity.

106
00:11:30,000 --> 00:11:36,000
Every moment you're objective, you're cultivating objectivity, every moment you're in reality.

107
00:11:36,000 --> 00:11:38,000
You're cultivating an awareness of reality.

108
00:11:38,000 --> 00:11:39,000
That's habitual.

109
00:11:39,000 --> 00:11:47,000
It, over time, begins to erode other habits, and begins to take their place.

110
00:11:47,000 --> 00:11:52,000
And the power of it leads to understanding.

111
00:11:52,000 --> 00:12:06,000
We start to see things clear, and that understanding works to erode to weaken bad habits that are based on misunderstanding.

112
00:12:06,000 --> 00:12:16,000
Because when you understand that something's wrong, when you used to think it was right, that changes everything.

113
00:12:16,000 --> 00:12:18,000
Many habits don't arise.

114
00:12:18,000 --> 00:12:24,000
When a person becomes a sodapan that there are certain habits that just get cut off from wisdom.

115
00:12:24,000 --> 00:12:27,000
But until that point we're building habits.

116
00:12:27,000 --> 00:12:32,000
We're building up purity of mind to the extent that we can let go.

117
00:12:32,000 --> 00:12:37,000
So the extent that we can slip through the cracks.

118
00:12:37,000 --> 00:12:39,000
So not clinging on to everything.

119
00:12:39,000 --> 00:12:45,000
Let go and out to Nirvana.

120
00:12:45,000 --> 00:12:55,000
Once you've seen Nirvana game change, that way habits disappear.

121
00:12:55,000 --> 00:13:05,000
The residual residue of them, physical residue and so on.

122
00:13:05,000 --> 00:13:08,000
Chemicals in the brain are associated with anger.

123
00:13:08,000 --> 00:13:16,000
The protection must have been there, but the mental aspect isn't there.

124
00:13:16,000 --> 00:13:27,000
So the second point is just that the being isn't the one that holds our defilements or our problems.

125
00:13:27,000 --> 00:13:30,000
It's the mind, and they're just mind-states.

126
00:13:30,000 --> 00:13:33,000
All problems, depression, anxiety.

127
00:13:33,000 --> 00:13:47,000
And all the distill down to mind-states, even schizophrenia, bipolar, all these things, can be in the end separated out into physical states and mental states that arise and cease.

128
00:13:47,000 --> 00:13:49,000
Often incessantly.

129
00:13:49,000 --> 00:13:54,000
But so people would argue that's not a habit.

130
00:13:54,000 --> 00:14:02,000
The only way you can say it's not a habit is if you follow sort of modern thinking that life starts at conception.

131
00:14:02,000 --> 00:14:09,000
That birth creates mind, not mind creates birth.

132
00:14:09,000 --> 00:14:12,000
So Buddhism doesn't think that.

133
00:14:12,000 --> 00:14:18,000
Buddhism claims that mind creates birth, not the other way around.

134
00:14:18,000 --> 00:14:26,000
And so something like, as I was thinking, I was just talking about last night, the mind creates schizophrenia that's a habit.

135
00:14:26,000 --> 00:14:35,000
So based on some bad habits somehow.

136
00:14:35,000 --> 00:14:37,000
Yeah.

137
00:14:37,000 --> 00:14:44,000
So that's all a little bit of, I wanted to get Robin on here, so she's on here now.

138
00:14:44,000 --> 00:14:53,000
Let's see if I can patch you into the audio.

139
00:14:53,000 --> 00:14:59,000
Let's see.

140
00:14:59,000 --> 00:15:09,000
Oh, some capture.

141
00:15:09,000 --> 00:15:12,000
Okay, Robin, are you there?

142
00:15:12,000 --> 00:15:15,000
I'm here, Soty Bante.

143
00:15:15,000 --> 00:15:16,000
Okay.

144
00:15:16,000 --> 00:15:24,000
So you're now on irresponsible for this new campaign, this new thing on new caring?

145
00:15:24,000 --> 00:15:25,000
Yes.

146
00:15:25,000 --> 00:15:28,000
Oh.

147
00:15:28,000 --> 00:15:33,000
I started working on the new campaign, but I didn't even release it yet.

148
00:15:33,000 --> 00:15:34,000
Oops.

149
00:15:34,000 --> 00:15:37,000
Well, I just released it for you.

150
00:15:37,000 --> 00:15:38,000
Oh, okay.

151
00:15:38,000 --> 00:15:39,000
It said it's live.

152
00:15:39,000 --> 00:15:42,000
I just got an email saying it's live.

153
00:15:42,000 --> 00:15:44,000
Oh, that's right.

154
00:15:44,000 --> 00:15:50,000
I guess that's why I wasn't informed about it because still in the works.

155
00:15:50,000 --> 00:15:53,000
It has, it has no pictures on it or anything yet.

156
00:15:53,000 --> 00:15:55,000
It doesn't have any pictures.

157
00:15:55,000 --> 00:15:56,000
It doesn't have any pictures.

158
00:15:56,000 --> 00:15:58,000
It's just them to put some pictures on it.

159
00:15:58,000 --> 00:16:03,000
It was really, I'm sorry, it was really just in progress, completely in progress.

160
00:16:03,000 --> 00:16:09,000
Well, 218 people have seen it on Facebook.

161
00:16:09,000 --> 00:16:12,000
One person's already shared it.

162
00:16:12,000 --> 00:16:13,000
So it's going viral.

163
00:16:13,000 --> 00:16:16,000
Okay.

164
00:16:16,000 --> 00:16:19,000
Well, we'll get some pictures up.

165
00:16:19,000 --> 00:16:22,000
Maybe release that.

166
00:16:22,000 --> 00:16:24,000
Well, I can talk about it anyway.

167
00:16:24,000 --> 00:16:28,000
I mean, it's sure.

168
00:16:28,000 --> 00:16:31,000
Sorry, it was just really unprepared for this.

169
00:16:31,000 --> 00:16:33,000
You know, we.

170
00:16:33,000 --> 00:16:37,000
The monastery has been up and running for six months now.

171
00:16:37,000 --> 00:16:39,000
And I mean, things are really going well.

172
00:16:39,000 --> 00:16:43,000
There's a lot of meditators coming.

173
00:16:43,000 --> 00:16:47,000
The meditation meditator schedule is booked for a couple of months out and advanced.

174
00:16:47,000 --> 00:16:48,000
It's great.

175
00:16:48,000 --> 00:16:51,000
There's a lot of things going on.

176
00:16:51,000 --> 00:16:57,000
And we're just we're looking to gauge support for whether this monastery is able to continue

177
00:16:57,000 --> 00:17:04,000
on well past, you know, well past when our lease ends, which is in August.

178
00:17:04,000 --> 00:17:09,000
So we're just looking to gauge support for that.

179
00:17:09,000 --> 00:17:12,000
Hopefully, you know, people are interested in supporting the monastery in the meditation center.

180
00:17:12,000 --> 00:17:17,000
Well, beyond what we already have the funds for, which is.

181
00:17:17,000 --> 00:17:21,000
This was another thing I needed to check on, but it's somewhere between June and August.

182
00:17:21,000 --> 00:17:22,000
So.

183
00:17:22,000 --> 00:17:28,000
Just looking to gauge support beyond that.

184
00:17:28,000 --> 00:17:30,000
Okay.

185
00:17:30,000 --> 00:17:32,000
Thank you for that.

186
00:17:32,000 --> 00:17:36,000
Thank you, Dante.

187
00:17:36,000 --> 00:17:40,000
And with that, I think we'll end for the night.

188
00:17:40,000 --> 00:17:42,000
So thank you all for tuning in.

189
00:17:42,000 --> 00:17:45,000
Have a good night.

190
00:17:45,000 --> 00:18:10,000
Thank you, Dante.

