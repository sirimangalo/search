1
00:00:00,000 --> 00:00:06,080
Welcome, everyone, to find Wednesday morning in the Deer Park.

2
00:00:09,920 --> 00:00:15,760
Today's talk will be about becoming Buddhist.

3
00:00:25,600 --> 00:00:27,600
And it's an interesting topic.

4
00:00:27,600 --> 00:00:35,040
There are actually many Buddhist teachers out there who recommend not to become Buddhist.

5
00:00:36,240 --> 00:00:40,800
And maybe not many, but there are some teachers out there who

6
00:00:46,880 --> 00:00:53,360
who believe that the word Buddhism is in itself a problem

7
00:00:53,360 --> 00:00:59,440
and is taking away from the purity of the teaching.

8
00:01:02,000 --> 00:01:08,160
So I'd like to qualify and I'm not going to deny that there's a danger in labels,

9
00:01:10,320 --> 00:01:16,160
but I'm going to qualify what I mean and try to explain why it might actually be a benefit to us

10
00:01:16,160 --> 00:01:23,920
to take on the label or the role or the identification of a Buddhist.

11
00:01:26,720 --> 00:01:30,960
First of all, what is meant by a Buddhist? There are three ways of approaching the Buddhist teaching.

12
00:01:33,520 --> 00:01:40,160
Three sorts of paths you can take when you approach what we understand to be Buddhism.

13
00:01:40,160 --> 00:01:47,040
And the first is to become a fully enlightened Buddha.

14
00:01:49,840 --> 00:01:58,880
This path is obviously the most difficult and it's also the most noble and exceptional.

15
00:01:58,880 --> 00:02:14,080
To become a Buddha takes an incredibly long time and it depends on one's skill and imperfections

16
00:02:14,080 --> 00:02:16,400
as to whether one will even come close to attaining it.

17
00:02:19,280 --> 00:02:21,920
They say that to become a perfectly enlightened Buddha,

18
00:02:21,920 --> 00:02:35,360
you have to be willing to swim through an ocean of red-hot coals.

19
00:02:36,800 --> 00:02:44,320
If suppose there were an ocean the size of the Pacific Ocean and instead of water it were full

20
00:02:44,320 --> 00:02:49,280
of red-hot coals and you knew that you could swim through it without dying but you'd still feel

21
00:02:49,280 --> 00:02:59,280
all the pain and suffering. If you had what it took to become a Buddha you wouldn't think twice

22
00:02:59,280 --> 00:03:03,760
to cross that ocean in order to become a Buddha.

23
00:03:06,800 --> 00:03:13,040
It takes an incredible determination and you might even say it takes an incredible amount of luck

24
00:03:13,040 --> 00:03:18,960
because it's something that takes such a long time that the chances of you maintaining your

25
00:03:18,960 --> 00:03:25,600
dedication for such an uncountable length of time is quite difficult. This isn't to discourage

26
00:03:25,600 --> 00:03:33,760
people from that path. It's maybe to instill some measure of realism because it seems like there's

27
00:03:33,760 --> 00:03:40,160
a lot of people who take on the vow to become a fully enlightened Buddha without really understanding

28
00:03:40,160 --> 00:03:48,400
what it entails and without really taking on those practices and that sort of dedication

29
00:03:48,400 --> 00:03:52,640
which is going to give one even a slight chance of getting there.

30
00:03:55,920 --> 00:04:02,400
The second path is to become enlightened for oneself but not teach.

31
00:04:05,520 --> 00:04:11,760
And so what makes a Buddha a fully enlightened Buddha special is that first of all he's able to

32
00:04:11,760 --> 00:04:21,040
realize here she is able to realize the truth for themselves without anyone having to explain it to

33
00:04:21,040 --> 00:04:30,640
them. This is the extent of their perfection. They don't need a teacher. They're so highly developed

34
00:04:30,640 --> 00:04:36,560
that they're able to look at reality and to see it for what it is without any instruction.

35
00:04:36,560 --> 00:04:42,960
But the second thing is that they're also then able to teach it to other people.

36
00:04:45,200 --> 00:04:50,880
Not only do they free themselves from all attachment, all clinging, all suffering,

37
00:04:51,600 --> 00:04:57,760
but they understand clearly how they did it and how anyone else can do it.

38
00:04:59,520 --> 00:05:06,240
And they're able to see the path of any other person as to what it's going to take to get them

39
00:05:06,240 --> 00:05:16,000
to that stage and so they can teach others and they can teach others perfect with perfect accuracy

40
00:05:16,000 --> 00:05:22,560
such that they know exactly what it's going to take to that person to develop.

41
00:05:25,760 --> 00:05:30,960
Now the second type here is called the Pachaykabuddha private Buddha or one who becomes enlightened

42
00:05:30,960 --> 00:05:37,200
for themselves. And this sort of person is special as well because they're still able to

43
00:05:37,760 --> 00:05:44,240
realize the truth by themselves. These are many of the great seers that have been in the past.

44
00:05:47,280 --> 00:05:55,200
A good example I think could be allowed to who was the founder of Taoism.

45
00:05:55,840 --> 00:06:00,240
But he wasn't the founder in the sense of actually going out and teaching anyone.

46
00:06:00,240 --> 00:06:06,880
He just left behind a text or a set of teachings which in the end aren't really teachings.

47
00:06:06,880 --> 00:06:10,720
They're just principles. And then he left the world.

48
00:06:13,040 --> 00:06:20,080
And so these beings can arise at any time and they go into the forest and they're able to

49
00:06:20,080 --> 00:06:27,520
realize the truth. But it's a different sort of realization. They let go. They see things

50
00:06:27,520 --> 00:06:39,040
clearly but they don't have this design in terms of understanding the method

51
00:06:40,400 --> 00:06:45,520
to bring people to this realization. Just like all of us when we practice, often we practice

52
00:06:45,520 --> 00:06:53,360
according to a teacher and we're even less able than a Pachaykabuddha to teach anyone else.

53
00:06:53,360 --> 00:06:58,800
We were able to practice by ourselves and we're able to follow the instruction of the teacher.

54
00:06:58,800 --> 00:07:03,760
But we're not able to lead other people to the same realization because we don't have the

55
00:07:03,760 --> 00:07:11,840
knowledge of the teacher. So Pachaykabuddha becomes enlightened but then doesn't teach others.

56
00:07:13,520 --> 00:07:19,840
The third type what I would identify with as a Buddhist or it's the type of Buddhists that I'm

57
00:07:19,840 --> 00:07:26,960
going to be talking about here is a person who is not able in their present condition to realize

58
00:07:26,960 --> 00:07:36,240
the truth for themselves. But they take on either the Buddha or one of his enlightened disciples

59
00:07:36,240 --> 00:07:44,480
or someone who is knowledgeable about the teachings of the Buddha as a teacher and as a result

60
00:07:44,480 --> 00:07:58,320
they're able to realize the truth having been instructed. This is the most common. The difference

61
00:07:59,280 --> 00:08:07,120
between the enlightened Buddha and the fully enlightened Buddha and one of his disciples

62
00:08:08,320 --> 00:08:13,680
it really boils down to this. A fully enlightened Buddha to become a fully enlightened Buddha,

63
00:08:13,680 --> 00:08:19,920
you not only have to give up everything, you also have to understand everything, know everything.

64
00:08:21,280 --> 00:08:25,120
You have to know everything, you have to know nothing, you know stone left unturned,

65
00:08:25,120 --> 00:08:31,440
you have to have come to understood the entire universe. To be a disciple of the Buddha,

66
00:08:31,440 --> 00:08:37,840
you don't have to know everything but you have to let go of everything. You have to understand

67
00:08:37,840 --> 00:08:45,440
enough about the universe to realize that there's nothing worth clinging to. When you do that,

68
00:08:45,440 --> 00:08:52,080
you become free from suffering. So in one sense the realization is the same on a practical level

69
00:08:53,360 --> 00:08:58,000
because you no longer subject to attachment and addiction and suffering.

70
00:09:00,000 --> 00:09:03,760
But the fully enlightened Buddha has the advantage in terms of being able to

71
00:09:03,760 --> 00:09:08,480
teach others and to the breadth of the knowledge is incomparable.

72
00:09:13,200 --> 00:09:17,840
So today I'm just going to be talking about this third type, how to become a follower of the Buddha.

73
00:09:20,640 --> 00:09:27,360
And the benefits, as I see it, of taking on this label, given that there are potential

74
00:09:27,360 --> 00:09:36,320
disadvantages of self identification and creating some sort of sense of self or so on.

75
00:09:38,720 --> 00:09:47,520
Are that by determining in your mind by making a determination that you're going to follow

76
00:09:47,520 --> 00:10:03,040
the Buddha's teaching that the Buddha and Buddhism is your path? It's something that protects you.

77
00:10:03,040 --> 00:10:19,360
You gain this both internal and external protection and you become protected from evil spirits,

78
00:10:19,360 --> 00:10:32,080
you become protected from misfortune, and you become protected by Buddhists, by the Buddhist

79
00:10:32,080 --> 00:10:41,360
religion. So you become protected internally and externally. Evil spirits, there are malevolent

80
00:10:41,360 --> 00:10:52,320
forces in the world. There are both humans and non-humans who, through mischief or through

81
00:10:53,440 --> 00:11:05,920
genuine malevolence, will try to harm us. Having taken the Buddha and his teachings and the

82
00:11:05,920 --> 00:11:15,360
Sangha as our refuge, having become a Buddhist or to one who follows the Buddha's teaching,

83
00:11:15,360 --> 00:11:26,320
we have this strength of mind and we have this, it's like entering the mafia, you have protection.

84
00:11:26,320 --> 00:11:38,720
They're sort of like to make a crude comparison because there are also benevolent forces

85
00:11:38,720 --> 00:11:46,640
in the universe, both humans and non-human. And the most benevolent are the Buddhists,

86
00:11:47,920 --> 00:11:53,040
those who follow the Buddha's teaching because of the purity of the teaching.

87
00:11:53,040 --> 00:12:00,160
The fact that the Buddha focused and stressed the purification of the mind.

88
00:12:01,760 --> 00:12:06,960
So there are many forces out there that protect the Buddha's teaching, seeing that as a beneficial

89
00:12:07,920 --> 00:12:17,440
force in the world. And not just argument spirits, but also human beings who see the benefit

90
00:12:17,440 --> 00:12:23,920
of the Buddha's teaching. And in fact, and many times non- Buddhists will protect Buddhists.

91
00:12:24,640 --> 00:12:32,480
And I've seen this. They're both sides, there are human beings who obviously, through their

92
00:12:32,480 --> 00:12:37,440
ignorance or through their bigotry, they're actually malevolent towards the Buddha's teaching,

93
00:12:37,440 --> 00:12:47,280
towards Buddhism. And they see monks, they feel hostility. I was arrested last year and put in jail,

94
00:12:47,280 --> 00:12:57,600
by someone who thought, or was either trying to put me in trouble or else genuinely thought

95
00:12:57,600 --> 00:13:04,080
I was something I wasn't thought I was a streaker or so on. And you get this. But you also get

96
00:13:05,360 --> 00:13:14,400
other people who are not Buddhist, but see the genuine sincerity, the purity, the love and

97
00:13:14,400 --> 00:13:21,120
compassion that is expressed by Buddhists. And wish to protect them.

98
00:13:28,880 --> 00:13:35,120
And this is one of the benefits I would say of identifying yourself as a Buddhist.

99
00:13:35,120 --> 00:13:41,120
It's one of those, maybe similar to identifying yourself as a Canadian. We always have this joke

100
00:13:41,120 --> 00:13:49,200
when we go traveling. As Canadians, we always put the Canadian flag on our backpack.

101
00:13:51,120 --> 00:13:58,080
And this is because Canadians were, I don't know anymore, but they were for a long time respected

102
00:13:58,880 --> 00:14:07,280
rightfully or not rightfully as nice people, good people. And so would generally be given

103
00:14:07,280 --> 00:14:18,240
an easier time if they were accosted by police or so on. So what happened then is Americans when

104
00:14:18,240 --> 00:14:24,720
they would go abroad, they would also put a Canadian flag on their backpacks. And if anyone

105
00:14:24,720 --> 00:14:32,080
asked they would tell them they were Canadian. Maybe something like that. This is sort of

106
00:14:32,080 --> 00:14:39,600
one of the lower more base benefits of becoming a Buddhist, but it certainly is there.

107
00:14:40,400 --> 00:14:48,560
And protection is quite useful being protected from danger. Obviously when we're trying to develop

108
00:14:48,560 --> 00:15:00,480
ourselves. They say that when you take the Buddha as your refuge, evil spirits will not dare

109
00:15:00,480 --> 00:15:06,480
to come near you because they know the danger of attacking someone who is a follower of the Buddha.

110
00:15:08,880 --> 00:15:15,680
There is an inherent danger for spirits because it's very easy for them to

111
00:15:17,520 --> 00:15:23,360
switch from one state to another. A human being doesn't evil deed and it's very slow to bring

112
00:15:23,360 --> 00:15:31,120
results, but a spirit does evil deeds or good deeds, it's very quick to bring results.

113
00:15:34,800 --> 00:15:38,080
The next benefit is that it brings happiness in this life.

114
00:15:38,080 --> 00:15:52,480
When we become Buddhist, it brings a great state of peace and surety confidence to us.

115
00:15:54,640 --> 00:16:02,080
It eases our minds. We don't have to think about what is right and what is wrong. We accept

116
00:16:02,080 --> 00:16:07,760
the teachings of the Buddha. We've studied them and we're sure in our minds and we have this

117
00:16:07,760 --> 00:16:16,560
great confidence. We know where we stand. So it's like when the Christians, they always tell you

118
00:16:16,560 --> 00:16:23,280
and when you have a problem, you just ask yourself, what would Jesus do? This is the Christian

119
00:16:23,280 --> 00:16:29,440
answer. For Buddhist we say, what would the Buddha do? What would a Buddhist do? What would a follower

120
00:16:29,440 --> 00:16:37,040
of the Buddha do? This is generally useful as well. As I said, it's something that brings us

121
00:16:37,040 --> 00:16:45,120
peace and happiness. It's something that calms and tranquilizes our minds. When we practice

122
00:16:45,120 --> 00:16:53,120
meditation, we feel somehow like we are a part of the group. We're walking with the Buddha.

123
00:16:53,120 --> 00:17:05,520
We're following after the Buddha. The next benefit related to that is it brings happiness

124
00:17:05,520 --> 00:17:13,520
in our next life. Because of the surety of mind, the confidence that we have and the peace

125
00:17:13,520 --> 00:17:23,920
and tranquility that exists in our minds from accepting the Buddha as our teacher. When we pass away,

126
00:17:27,200 --> 00:17:30,320
we can be sure to be born in a good place, in a good way.

127
00:17:36,480 --> 00:17:41,120
But the real reason I would say, and probably the best reason, the only sort of reason that is

128
00:17:41,120 --> 00:17:50,960
of immediate benefit or immediate purpose for us is that considering that the Buddha's teaching

129
00:17:50,960 --> 00:18:02,480
is so pure and that there's no part of it that is detrimental to us, that taking on the Buddha

130
00:18:02,480 --> 00:18:12,080
as our refuge, taking on Buddhism as our path leads to purity of mind, leads to purity of

131
00:18:12,080 --> 00:18:19,680
mind and a dedication to the practice. And this is obviously the most important because our

132
00:18:20,480 --> 00:18:24,480
intention, our path as Buddhists is to practice, is to develop our minds.

133
00:18:24,480 --> 00:18:33,520
And so taking the Buddha as our refuge and reminding ourselves of the qualities of the Buddha

134
00:18:34,640 --> 00:18:41,440
and determining that this is our path gives us the strength of mind to continue in the practice.

135
00:18:42,400 --> 00:18:49,440
When the going gets tough, we remember the example that the Buddha said and we reaffirm in

136
00:18:49,440 --> 00:18:55,520
our minds that we are followers of the Buddha, it gives us confidence, it gives us reassurance

137
00:18:55,520 --> 00:19:05,840
that we're a good person, that we're someone who has done a good thing in terms of

138
00:19:07,360 --> 00:19:12,320
you know, put placing our trust and our confidence in the Buddha. And by following this path,

139
00:19:12,320 --> 00:19:20,800
which leads us to freedom from suffering. So I would say it's generally a good thing, it's

140
00:19:20,800 --> 00:19:25,760
obviously not the best thing and it's not a replacement for meditation. Let's say there's a lot

141
00:19:25,760 --> 00:19:36,480
of people out there who are Buddhist, quote unquote, and don't practice and tend to get this

142
00:19:36,480 --> 00:19:45,200
over confidence that, you know, I'm Buddhist and I've been Buddhist from birth and that somehow

143
00:19:45,200 --> 00:19:53,360
that means something, somehow that is going to protect one. And I would say, you know, having

144
00:19:53,360 --> 00:19:59,520
said that it is a protection, it is a support, it's a fairly weak protection in the face of

145
00:19:59,520 --> 00:20:06,720
the defilements that exist in our minds. If that's our only protection is this identification

146
00:20:06,720 --> 00:20:15,200
as a Buddhist, then we're not likely to be protected in any way from the evils in our mind.

147
00:20:15,200 --> 00:20:21,520
It's simply a support, it's a basic practice that encourages us, that tranquilizes our minds.

148
00:20:21,520 --> 00:20:25,520
When I was practicing meditation, when I first went to practice meditation, I didn't

149
00:20:25,520 --> 00:20:31,680
wasn't even interested in Buddhism. I had no intention to become a Buddhist. But they made us do

150
00:20:31,680 --> 00:20:35,840
this ceremony where we, you know, we take the Buddha as our refuge and so I followed after and I

151
00:20:35,840 --> 00:20:42,800
was reading the words, reading the script again while I was meditating. They wouldn't let us read.

152
00:20:42,800 --> 00:20:47,680
So all we had is this little booklet and I just kept reading it over and over again in the

153
00:20:47,680 --> 00:20:57,360
chanting. And so when the practice got really difficult and suddenly I was faced with,

154
00:20:58,160 --> 00:21:10,000
you know, this overwhelming pain and suffering from just the realization of the insanity that

155
00:21:10,000 --> 00:21:20,160
existed in my mind and what I had done to myself and how I was so on the wrong path with drugs

156
00:21:20,160 --> 00:21:30,640
and alcohol and women and music and so much that was really destroying my piece of mind

157
00:21:30,640 --> 00:21:37,120
and have been doing so for many years. I remember waking up at three in the morning and

158
00:21:37,120 --> 00:21:41,280
you know, just saying, okay, I can't get ready to start. And as soon as I sat down to start meditating,

159
00:21:41,280 --> 00:21:46,240
everything came back again. Everything from the last day and I was right there again,

160
00:21:46,240 --> 00:21:52,320
waiting for me. It hadn't gone anywhere. It hadn't disappeared with my sleep. And just freaking out

161
00:21:52,320 --> 00:22:00,080
and realizing, you know, I can't do this anymore. And I had no way nowhere to go. I had no refuge,

162
00:22:00,080 --> 00:22:12,080
you know, nothing to hold on to, nothing to pull myself up with. And so I went walking out of my

163
00:22:12,080 --> 00:22:17,920
out of my hut looking, you know, I'm not looking actually just wandering aimlessly. And then I saw

164
00:22:17,920 --> 00:22:24,000
often this bamboo salad they had this Buddha image and the light was on, somebody left the light

165
00:22:24,000 --> 00:22:33,120
on on night. And as soon as I saw it, I just just drawn to it and I walked over and I knelt down

166
00:22:33,120 --> 00:22:38,160
in front of the Buddha and I prostrated down and I started chanting according to this booklet

167
00:22:38,160 --> 00:22:49,520
and taking refuge in the Buddha. I suppose that sounds a lot like a Christian tale of what

168
00:22:49,520 --> 00:22:57,840
they call a newborn, a born again, being born again. And, you know, I don't think it's that dissimilar

169
00:22:57,840 --> 00:23:03,200
and it's certainly not a state of enlightenment. But it was something that really helped me in my

170
00:23:03,200 --> 00:23:09,120
practice. It helped me to continue meditation. So if you wanted to meditate, if that was my intention

171
00:23:09,120 --> 00:23:14,400
is to meditate, then I would say that was a real benefit for me because it gave me this,

172
00:23:14,400 --> 00:23:22,400
you know, basic reassurance that I wasn't alone. I had someone, you know, to hold on to

173
00:23:22,400 --> 00:23:32,080
while I was like a child just learning to walk. And it's certainly not the final solution,

174
00:23:32,080 --> 00:23:40,320
but for newcomers it can be a very great thing to do, a great benefit and helps to

175
00:23:40,320 --> 00:23:47,680
calm and reassure your mind in the beginning. Okay, so the next thing is what does it mean to

176
00:23:47,680 --> 00:23:54,800
be a Buddhist and how do you become a Buddhist? And it's quite simple. As a Buddhist, we're not

177
00:23:56,640 --> 00:24:02,160
really interested in identifying ourselves as anything. But there are certain qualifications

178
00:24:02,160 --> 00:24:15,200
that one has to obtain, one has to take the three refuges. To be considered a Buddhist,

179
00:24:15,200 --> 00:24:20,240
you have to take the Buddha as your refuge. The idea is, as a Buddhist, we say that the

180
00:24:20,240 --> 00:24:28,560
Buddha is our leader. Though he's passed away long ago, we still understand that this person

181
00:24:28,560 --> 00:24:38,000
who taught all of these wonderful things is our leader. And we take him and singularly him as

182
00:24:38,000 --> 00:24:44,320
our leader. The second is the Dhamma, his teachings. We take his teachings as our teaching,

183
00:24:44,320 --> 00:24:49,520
as the teachings we're going to follow. We say that these are the teachings that we're going

184
00:24:49,520 --> 00:24:58,320
to follow. This is our path. And third, we take the teachers or the enlightened disciples of

185
00:24:58,320 --> 00:25:05,360
the Buddha as our refuge. The people who have passed on the Buddha's teaching up until this time.

186
00:25:16,880 --> 00:25:25,920
This is what I meant by having the protection, having a support. Because these things are

187
00:25:25,920 --> 00:25:30,880
something that we can always reflect on, something that we can always remember, something that we can

188
00:25:30,880 --> 00:25:36,240
use to support ourselves when we're going to get stuff, when we're in difficulty.

189
00:25:37,840 --> 00:25:42,800
It's not a central practice. Again, I don't want people to get this idea that we're like a

190
00:25:42,800 --> 00:25:49,520
faith-based religion where we focus on worshiping these things or something. But the support is

191
00:25:49,520 --> 00:26:03,120
undeniable. The support that these things give to our minds. And so my teacher, he said when you're

192
00:26:03,760 --> 00:26:12,000
when you're in danger, when you're traveling anywhere, when you're going on a trip or something,

193
00:26:12,000 --> 00:26:24,000
and you're worried and you're not sure whether there'll be danger or misfortune or so on,

194
00:26:24,720 --> 00:26:27,840
you can just remind yourself of the Buddha and say that the Buddha is your refuge,

195
00:26:27,840 --> 00:26:31,600
the Dhamma is your refuge, the Sangha is your refuge. And it strengthens your mind.

196
00:26:31,600 --> 00:26:43,360
And it can actually change the course of events because it creates some sort of strength and power

197
00:26:43,360 --> 00:26:52,000
in your mind. And perhaps even is a means of seeking protection from the angels and the guardian

198
00:26:52,000 --> 00:26:58,000
spirits, who may also be Buddhist or appreciate the Buddhist teaching.

199
00:26:58,000 --> 00:27:05,280
So we have this mantra actually that will say to ourselves that just as the Buddha is my refuge,

200
00:27:05,280 --> 00:27:09,760
the Dhamma is my refuge, the Sangha is my refuge, the Buddha may not, the Dhamma may not,

201
00:27:09,760 --> 00:27:13,920
the Sangha may not all. And we repeat that to ourselves.

202
00:27:13,920 --> 00:27:23,360
The second thing that's required as a Buddhist is that we keep the five precepts.

203
00:27:29,040 --> 00:27:35,360
And this is where people tend to have a lot of trouble. I would say most people are at least

204
00:27:35,360 --> 00:27:43,200
superficially able to accept the Buddha, the Dhamma and the Sangha as good things and as their

205
00:27:43,200 --> 00:27:48,240
refuge, even though they might not quite know what that means, most of many Buddhists don't

206
00:27:48,240 --> 00:27:55,280
really know what the Dhamma is, don't have a strong understanding of what is the teaching of the

207
00:27:55,280 --> 00:28:02,160
Buddha and also don't have a way of distinguishing between those teachers or those Buddhists

208
00:28:02,160 --> 00:28:06,400
who could be considered Sangha who actually practice according to the Buddha's teaching.

209
00:28:07,200 --> 00:28:11,600
So there are a lot of Buddhists out there who follow after teachers who maybe are

210
00:28:11,600 --> 00:28:20,800
fortune telling or offering, selling amulets or teaching public school or whatever,

211
00:28:21,600 --> 00:28:26,160
many things which are not in line with the Buddha's teaching. I would make one thing that this is

212
00:28:26,160 --> 00:28:35,200
not really a part of the Sangha. These people are not really followers or passing on the Buddha's

213
00:28:35,200 --> 00:28:44,080
teaching. But so people are able at least superficially to accept this. It's very difficult for people

214
00:28:44,080 --> 00:28:51,280
to accept the five precepts. And this makes it a little bit difficult to become a Buddhist.

215
00:28:54,160 --> 00:29:02,160
And obviously that makes sense because Buddhism is not an easy path. It's not an easy thing to

216
00:29:02,160 --> 00:29:07,600
become enlightened to become free from suffering. We're not talking about something. There's no

217
00:29:08,240 --> 00:29:14,080
okay, become a Buddhist, sign up in your set for life. This isn't one of those religions.

218
00:29:16,400 --> 00:29:22,640
It's a very difficult thing and this is perhaps why seeking out a support might be beneficial

219
00:29:22,640 --> 00:29:30,880
because we have no illusions about the difficulty of the task at hand. It's not something where

220
00:29:30,880 --> 00:29:38,080
everyone can be said to succeed. You might fail. You might end this life not having

221
00:29:40,080 --> 00:29:49,680
gained any lasting benefit or not having gained, not having become enlightened to any degree.

222
00:29:50,960 --> 00:29:54,320
It's possible that you could practice for some time and follow away from it.

223
00:29:54,320 --> 00:30:04,960
And so really we need all the help we can get. It's the benefit of becoming Buddhist.

224
00:30:10,080 --> 00:30:17,840
But we also have to take on some fairly difficult precepts. People always ask about the five

225
00:30:17,840 --> 00:30:23,360
precepts. They're really necessary, especially not drinking alcohol and taking drugs.

226
00:30:23,360 --> 00:30:28,560
And really I think this is a terrible question to ask. I think it's ridiculous that people

227
00:30:28,560 --> 00:30:33,600
always say, well, if you want to have one glass of wine, it's not really that bad. It's really,

228
00:30:33,600 --> 00:30:41,280
I mean, come on, people. If you can't give up such a simple thing in toxicants, drinking poison,

229
00:30:41,280 --> 00:30:55,040
drinking rotten grapes, is it really fair for us to wine and complain about such a small thing when

230
00:30:55,040 --> 00:31:03,920
what we're looking at here is such a profound and incredibly difficult path that if you can't even

231
00:31:03,920 --> 00:31:19,680
give up these basic acts, these basic immoral undertakings, it doesn't speak well for your ability

232
00:31:19,680 --> 00:31:22,320
to do away with the very subtle to found.

233
00:31:27,280 --> 00:31:31,760
And I think this is easy to see for people who really practice meditation. If you're serious

234
00:31:31,760 --> 00:31:36,560
about meditation, it's not really that difficult for you to give these things up. This is what

235
00:31:36,560 --> 00:31:44,000
is required. This is what the Buddha recommended, not even really recommended, but he was fairly

236
00:31:44,000 --> 00:31:48,960
categorical in terms of, if you don't keep these, you're not really going to progress in your

237
00:31:48,960 --> 00:31:59,840
practice. These are the basic moral principles of a Buddhist meditator, a Buddhist practitioner.

238
00:31:59,840 --> 00:32:06,400
If you can't even keep these, it's very difficult to see it any way for you to progress.

239
00:32:07,360 --> 00:32:25,680
And so it's often seen as making a concession or giving something up in favor for our practice.

240
00:32:25,680 --> 00:32:31,520
We want to go out drinking, we want to drink wine, we want to be a social drinker or so on.

241
00:32:32,160 --> 00:32:37,040
Maybe we want to kill, we want to steal, we want to cheat, we want to lie. And in some ways,

242
00:32:37,040 --> 00:32:46,880
we can verify, we can justify very small bad deeds. But this is a sacrifice that we make.

243
00:32:48,720 --> 00:32:54,320
And in the end, it isn't a sacrifice at all. We see that we were wrong to, once we practice,

244
00:32:54,320 --> 00:32:58,800
we were wrong to think that there was any good that could come from any of these things.

245
00:33:00,080 --> 00:33:05,280
Even just in terms of drinking, people who drink a little bit and say, oh, I'm just doing it to

246
00:33:05,280 --> 00:33:11,520
be social and to fit in and so on. Actually, what you're saying there is that you're doing it so

247
00:33:11,520 --> 00:33:17,360
that you don't have to challenge other people's beliefs. Suppose your friends are alcoholics or

248
00:33:17,360 --> 00:33:21,360
they really do get drunk. Well, you just drink just so that they don't feel bad. Basically,

249
00:33:21,360 --> 00:33:28,720
what you're saying, you drink so that they're able to retain their sense that drinking is okay,

250
00:33:29,360 --> 00:33:36,720
which of course it isn't. And as a Buddhist, it certainly isn't. As a Buddhist, we make this

251
00:33:36,720 --> 00:33:44,160
determination that this is the opposite of a clear and sober and pure state of mind.

252
00:33:44,160 --> 00:33:53,840
And so you're doing an incredible disservice to the Buddhist, to Buddhism by encouraging these

253
00:33:53,840 --> 00:34:05,440
people and you're wasting a very precious chance to change the world, to say to these people,

254
00:34:05,440 --> 00:34:16,320
I'm sorry, I don't drink. I believe that drinking is a cause for a mmm-hmm, a muddled state

255
00:34:16,320 --> 00:34:27,600
of mind, an enemy to clarity of mind. My practice is to purify my mind. So I don't do this.

256
00:34:27,600 --> 00:34:31,600
No, if that makes them angry, then here you go. This is something they have to look at

257
00:34:31,600 --> 00:34:36,080
themselves and something you have to look at. Why are you hanging out with people who get angry

258
00:34:36,080 --> 00:34:41,040
when you talk about clarity of mind and purity of mind? When you talk about meditation and all

259
00:34:41,040 --> 00:34:49,680
your friends start to, you know, get upset and get bored and disinterested looks on their faces.

260
00:34:49,680 --> 00:34:58,240
Why are you with these people? If you can somehow explain to them to the extent that they're

261
00:34:58,240 --> 00:35:05,920
able to accept and eventually come to practice good. But otherwise, it's not much of an excuse to

262
00:35:05,920 --> 00:35:11,760
say, well, my friends like to drink and therefore I should drink with them. It probably means

263
00:35:11,760 --> 00:35:20,160
that you're hanging out with the wrong people. So the five precepts, we don't kill, we don't steal,

264
00:35:20,160 --> 00:35:32,400
we don't cheat, meaning to commit adultery or break up other relationships in romantic affairs.

265
00:35:33,600 --> 00:35:38,640
We don't lie and we don't take drugs and alcohol and those things that intoxicate the mind.

266
00:35:42,080 --> 00:35:46,800
And this is basically what it means to become a Buddhist. When you take these two things,

267
00:35:46,800 --> 00:35:53,360
the three refuges, you accept that the Buddha, his teachings and the Sangha, the teachers who

268
00:35:53,360 --> 00:36:06,400
have passed on his teachings, are your refuge, are your point of reference. And when you take the

269
00:36:06,400 --> 00:36:20,000
five precepts as rules for your life, this is really what it means to become a Buddhist.

270
00:36:20,800 --> 00:36:31,440
And just as a side note, I made a video on YouTube recently that is a ceremony for taking

271
00:36:31,440 --> 00:36:40,640
the three refuge and the five precepts. So if in case anyone is interested in becoming a Buddhist,

272
00:36:40,640 --> 00:37:00,880
you're welcome to look it up. And here's the link.

273
00:37:10,640 --> 00:37:30,000
So I thought that was something useful to talk about. This ceremony on YouTube is a way of

274
00:37:30,000 --> 00:37:35,760
reaffirming these principles that I've talked about. It's a ceremony. It's a ritual.

275
00:37:35,760 --> 00:37:42,400
And what it does is it reaffirms in your mind. It's just like our meditation practice. When we

276
00:37:42,400 --> 00:37:46,960
watch the rising, we reaffirm for ourselves. This is the rising. This is the following of the

277
00:37:46,960 --> 00:37:52,800
abdomen. We feel pain. We reaffirm it for what it is. And it has a power. When we do this ceremony,

278
00:37:52,800 --> 00:37:59,360
it has a power in our minds. It reaffirms in our minds. It strengthens our vow, our intention.

279
00:37:59,360 --> 00:38:08,560
And it has a purpose. It has an effect. I would say the effect is beneficial because it strengthens

280
00:38:08,560 --> 00:38:15,360
our ability to carry out our practice. So that's what I wanted to talk about today. I think that

281
00:38:15,360 --> 00:38:20,720
should be something generally useful for people. And as a support for our meditation practice,

282
00:38:20,720 --> 00:38:24,960
I haven't really gotten into meditation these past couple of weeks. And I promised that I

283
00:38:24,960 --> 00:38:37,040
would, but maybe I'll say. I'd like to teach a session. Record a session on basic meditation

284
00:38:37,040 --> 00:38:45,120
practice. Just a 10-15-minute session. It's a guided meditation that I cannot use for people to

285
00:38:45,120 --> 00:38:50,400
use at home when they want to learn to meditate in a very short time. You can get a basic

286
00:38:50,400 --> 00:38:55,520
understanding on meditate. It's maybe I'll do that next week. Anyway, thank you all for coming.

287
00:38:55,520 --> 00:39:25,360
If you have any questions, I'm happy to answer them now.

