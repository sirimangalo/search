1
00:00:00,000 --> 00:00:23,000
Okay, good evening everyone, hopefully this is working properly.

2
00:00:23,000 --> 00:00:45,000
So we have another graduate here with us today, who's very happy to have finished the course.

3
00:00:45,000 --> 00:00:59,000
Or maybe he's just very happy to be done with it.

4
00:00:59,000 --> 00:01:02,000
And we're right.

5
00:01:02,000 --> 00:01:08,000
It's funny, I never call my meditators by their names, so I often even forget their names.

6
00:01:08,000 --> 00:01:14,000
I know them very, very well, but I don't even know their names so many of the times.

7
00:01:14,000 --> 00:01:20,000
And word, come say hello.

8
00:01:20,000 --> 00:01:23,000
Hopefully that might work.

9
00:01:23,000 --> 00:01:28,000
Hello.

10
00:01:28,000 --> 00:01:30,000
Say again, hello?

11
00:01:30,000 --> 00:01:34,000
Yeah, I think it worked.

12
00:01:34,000 --> 00:01:42,000
So, you've been here for how long have you been here three weeks now?

13
00:01:42,000 --> 00:01:45,000
Yeah, 21 days.

14
00:01:45,000 --> 00:01:47,000
How was the food?

15
00:01:47,000 --> 00:01:48,000
Good.

16
00:01:48,000 --> 00:01:50,000
It felt like home.

17
00:01:50,000 --> 00:01:51,000
It felt like home.

18
00:01:51,000 --> 00:01:52,000
It was good.

19
00:01:52,000 --> 00:01:57,000
Yeah.

20
00:01:57,000 --> 00:01:59,000
And the rooms were okay.

21
00:01:59,000 --> 00:02:01,000
Yeah, and everything was in order, everything was good.

22
00:02:01,000 --> 00:02:04,000
I didn't know what to expect.

23
00:02:04,000 --> 00:02:11,000
Yeah, it felt like home, so it had the comfort of being at home, but still the discipline of being.

24
00:02:11,000 --> 00:02:16,000
The discipline of being at an intense meditation course, and that was excellent.

25
00:02:16,000 --> 00:02:21,000
How was the meditation difficult?

26
00:02:21,000 --> 00:02:23,000
What did you learn?

27
00:02:23,000 --> 00:02:24,000
Wow.

28
00:02:24,000 --> 00:02:32,000
You're feeling before you came here and how you feel now and you're getting ready to leave.

29
00:02:32,000 --> 00:02:35,000
What's changed?

30
00:02:35,000 --> 00:02:42,000
Great question.

31
00:02:42,000 --> 00:02:45,000
I haven't thought about it.

32
00:02:45,000 --> 00:02:48,000
So my feeling coming in here was to learn a new technique.

33
00:02:48,000 --> 00:02:53,000
I've been meditating for a few years now, and I've read a lot.

34
00:02:53,000 --> 00:02:58,000
Yeah, I've heard and read a lot about this technique, so I was open to try something new.

35
00:02:58,000 --> 00:03:06,000
I didn't know much what to expect.

36
00:03:06,000 --> 00:03:16,000
Walking meditation was a new thing for me, and doing that, it really showed me how it complemented having this dynamic practice to the sitting, which I found very helpful.

37
00:03:16,000 --> 00:03:24,000
And in fact, I tried to sit now without walking, and I saw how much more difficult it is without the walking.

38
00:03:24,000 --> 00:03:32,000
So that was nice, it really does calm the mind and help the sitting.

39
00:03:32,000 --> 00:03:35,000
There's so much to say, I don't know.

40
00:03:35,000 --> 00:03:37,000
How do you feel today?

41
00:03:37,000 --> 00:03:43,000
You come out of the hardest part of the course, how do you feel today?

42
00:03:43,000 --> 00:03:50,000
Relieved that it's over at the same time, a part of me wants to keep going.

43
00:03:50,000 --> 00:03:57,000
But yeah, of course, we leave it, it's over, it was tough.

44
00:03:57,000 --> 00:04:05,000
But as they say, or as I've heard, you purify through fire in a sense.

45
00:04:05,000 --> 00:04:11,000
So you need to go through that difficulty to get through, to leave the side.

46
00:04:11,000 --> 00:04:15,000
But do you feel peaceful today?

47
00:04:15,000 --> 00:04:24,000
Yes, a lot more, I mean, I'm sensing it now, speaking on the mic, I'm very calm.

48
00:04:24,000 --> 00:04:29,000
I don't know if that comes across, but yeah, absolutely.

49
00:04:29,000 --> 00:04:37,000
And the meta part was very nice, very special.

50
00:04:37,000 --> 00:04:46,000
It takes time, of course, to really digest what a meditation course was all about.

51
00:04:46,000 --> 00:04:49,000
And it's not part of an ongoing process, right?

52
00:04:49,000 --> 00:04:52,000
This isn't the beginning for you in the certain days, at the end.

53
00:04:52,000 --> 00:04:53,000
Congratulations.

54
00:04:53,000 --> 00:04:55,000
Thank you very much for everything.

55
00:04:55,000 --> 00:05:01,000
You will swap it.

56
00:05:01,000 --> 00:05:06,000
Everyone says, congratulations.

57
00:05:06,000 --> 00:05:11,000
You're a star.

58
00:05:11,000 --> 00:05:12,000
How was that?

59
00:05:12,000 --> 00:05:14,000
Was the mic okay?

60
00:05:14,000 --> 00:05:15,000
Two mic set up here.

61
00:05:15,000 --> 00:05:18,000
That mic isn't as sensitive as this one.

62
00:05:18,000 --> 00:05:26,000
Should probably turn this mic down a little bit.

63
00:05:26,000 --> 00:05:35,000
Part of a process is sort of the theme for tonight.

64
00:05:35,000 --> 00:05:46,000
I have to think at questions about how long it's going to take to figure ourselves from

65
00:05:46,000 --> 00:05:51,000
bad habits.

66
00:05:51,000 --> 00:05:54,000
What can we do to free ourselves?

67
00:05:54,000 --> 00:06:02,000
What can we do sort of an underlying assumption that it's just going to suddenly

68
00:06:02,000 --> 00:06:12,000
find this just suddenly going to fix itself?

69
00:06:12,000 --> 00:06:26,000
But it would have compared the past to the ocean, great ocean.

70
00:06:26,000 --> 00:06:39,000
He was approached by this sort of the titans and Greek mythology and demigod being

71
00:06:39,000 --> 00:06:50,000
sort of like angels, but a little more coarse, a little more unrefined.

72
00:06:50,000 --> 00:07:00,000
Some form of angel and Buddhist cosmology or heavenly being, celestial being, but sort

73
00:07:00,000 --> 00:07:01,000
of associated with the earth.

74
00:07:01,000 --> 00:07:05,000
And this one was when they lived in the ocean.

75
00:07:05,000 --> 00:07:11,000
And I approached him and asked him, I approached him to listen to his teaching, then

76
00:07:11,000 --> 00:07:16,000
the Buddha asked him, I asked him about the ocean.

77
00:07:16,000 --> 00:07:19,000
He said, what do you think of the ocean?

78
00:07:19,000 --> 00:07:22,000
He said, do you delight in it?

79
00:07:22,000 --> 00:07:29,000
And they said, yes, yes, we asura, asura, this is the name of this type of being.

80
00:07:29,000 --> 00:07:33,000
So we delight in the ocean.

81
00:07:33,000 --> 00:07:37,000
So the Buddha asked, how many great qualities does the ocean have?

82
00:07:37,000 --> 00:07:46,000
He said, something up here he wants to compare his teaching to the great ocean.

83
00:07:46,000 --> 00:07:52,000
And so he asked, how many great qualities cause them to, cause you to delight, to make

84
00:07:52,000 --> 00:07:54,000
you delight in the ocean?

85
00:07:54,000 --> 00:07:57,000
What do you see in it?

86
00:07:57,000 --> 00:08:00,000
So this isn't even good during the guy book of AIDS.

87
00:08:00,000 --> 00:08:04,000
There are eight great qualities of the ocean.

88
00:08:04,000 --> 00:08:12,000
And the first, the first is that the ocean doesn't, as deep as it is, doesn't suddenly

89
00:08:12,000 --> 00:08:17,000
fall, doesn't suddenly become deep.

90
00:08:17,000 --> 00:08:34,000
It inclines slowly, so huge that so vast that there's huge depths, and it can

91
00:08:34,000 --> 00:08:43,000
slam, even so slightly.

92
00:08:43,000 --> 00:08:46,000
Because of how vast it is.

93
00:08:46,000 --> 00:08:48,000
Like not like a swimming pool.

94
00:08:48,000 --> 00:08:55,000
A swimming pool has to get deep really quick, because there's not enough room for it to get deep.

95
00:08:55,000 --> 00:08:56,000
But not the ocean.

96
00:08:56,000 --> 00:09:02,000
The ocean gets deep very slowly.

97
00:09:02,000 --> 00:09:12,000
Until it gets super deep.

98
00:09:12,000 --> 00:09:19,000
And then the ocean is, the second quality is that the ocean is stable.

99
00:09:19,000 --> 00:09:24,000
Doesn't overflow its boundaries.

100
00:09:24,000 --> 00:09:29,000
So the ocean doesn't flood, well that doesn't yet.

101
00:09:29,000 --> 00:09:36,000
As we know, the boundaries are probably soon going to change and expand.

102
00:09:36,000 --> 00:09:41,000
Well, for the longest time, the ocean has been kind of stable.

103
00:09:41,000 --> 00:09:48,000
It's not like a lake or a river that floods.

104
00:09:48,000 --> 00:10:01,000
And the third is that the ocean doesn't keep corpses, it doesn't keep refuse.

105
00:10:01,000 --> 00:10:05,000
Because we know that now even this isn't true.

106
00:10:05,000 --> 00:10:10,000
Now there's a big island of refuse in the middle of the ocean, because it's plastic.

107
00:10:10,000 --> 00:10:17,000
It's about the size of Texas, apparently.

108
00:10:17,000 --> 00:10:21,000
The great island of plastic.

109
00:10:21,000 --> 00:10:29,000
But the ocean generally, any refuse, a corpse, any corpses.

110
00:10:29,000 --> 00:10:34,000
The word here is the idea here is corpses.

111
00:10:34,000 --> 00:10:35,000
It's in India.

112
00:10:35,000 --> 00:10:42,000
They would cast off their bodies into the river and it would carry down to the ocean.

113
00:10:42,000 --> 00:10:44,000
But it would never stay in the ocean.

114
00:10:44,000 --> 00:10:47,000
The ocean would toss them out.

115
00:10:47,000 --> 00:10:54,000
The ocean is pure in that way.

116
00:10:54,000 --> 00:10:59,000
And number four is that the ocean is the ocean is one.

117
00:10:59,000 --> 00:11:08,000
Even though you have all these rivers reaching the great ocean, they're no longer separate.

118
00:11:08,000 --> 00:11:12,000
All water joins together in the ocean.

119
00:11:12,000 --> 00:11:26,000
All water eventually goes to reach the ocean and becomes simply the great ocean.

120
00:11:26,000 --> 00:11:33,000
Number five, no matter how much water flows into the ocean,

121
00:11:33,000 --> 00:11:40,000
there's either a decrease or an increase.

122
00:11:40,000 --> 00:11:44,000
Even though you constantly see water pouring in,

123
00:11:44,000 --> 00:11:48,000
then constantly water evaporating its stable.

124
00:11:48,000 --> 00:11:58,000
Again, not trespassing its boundaries, but also not decreasing or increasing.

125
00:11:58,000 --> 00:12:03,000
Number six, the ocean has, but one taste, the taste of salt.

126
00:12:03,000 --> 00:12:10,000
Where you go in the ocean, it doesn't taste differently.

127
00:12:10,000 --> 00:12:13,000
You don't get a different taste depending on which part of the ocean,

128
00:12:13,000 --> 00:12:19,000
which beach you go to swim on.

129
00:12:19,000 --> 00:12:24,000
Number seven, under the ocean, there's meant there are many precious substances.

130
00:12:24,000 --> 00:12:30,000
There is much of great magnificence in the ocean.

131
00:12:30,000 --> 00:12:35,000
The corals and the pearls and gems,

132
00:12:35,000 --> 00:12:39,000
even gold silver rubies apparently.

133
00:12:39,000 --> 00:12:50,000
And all these beautiful shells and beautiful fish and so on.

134
00:12:50,000 --> 00:12:56,000
Number eight, the great ocean is the abode of great being.

135
00:12:56,000 --> 00:13:01,000
We've got some names of great beings here,

136
00:13:01,000 --> 00:13:04,000
but they're apparently great spirits that live in the ocean.

137
00:13:04,000 --> 00:13:08,000
Apparently there are even just animals in the ocean.

138
00:13:08,000 --> 00:13:12,000
Some ocean animals live to be like 500 years.

139
00:13:12,000 --> 00:13:15,000
Apparently lobsters don't age.

140
00:13:15,000 --> 00:13:18,000
And lobsters could live for hundreds of years,

141
00:13:18,000 --> 00:13:23,000
except they get too big and then it's hard for them to survive.

142
00:13:23,000 --> 00:13:29,000
But other fish just live on and on for 500 years in the ocean.

143
00:13:29,000 --> 00:13:32,000
You think humans are long lived compared to animals,

144
00:13:32,000 --> 00:13:45,000
but there are some animals that have been around in the ocean for hundreds and hundreds of years.

145
00:13:45,000 --> 00:13:55,000
And then the Buddha compares this to the dhamma, he says,

146
00:13:55,000 --> 00:13:58,000
just as the great ocean slant slopes and inclines gradually not dropping off abruptly.

147
00:13:58,000 --> 00:14:04,000
So too in the dhamma vinya of the Buddha,

148
00:14:04,000 --> 00:14:09,000
the realization of the truth occurs gradually.

149
00:14:09,000 --> 00:14:13,000
And stages.

150
00:14:13,000 --> 00:14:16,000
So it's not something that you just get all at once.

151
00:14:16,000 --> 00:14:19,000
It's something that is open, I echo, it leads us on.

152
00:14:19,000 --> 00:14:24,000
The more you learn, the more you realize there is more to learn.

153
00:14:24,000 --> 00:14:31,000
The more you understand.

154
00:14:31,000 --> 00:14:36,000
You just get closer and closer to freedom.

155
00:14:36,000 --> 00:14:42,000
And the important thing here is to realize that it's not just going to all get better all at once.

156
00:14:42,000 --> 00:14:48,000
It's not going to happen overnight and it's certainly not a one-step process.

157
00:14:48,000 --> 00:14:50,000
It's someone tonight.

158
00:14:50,000 --> 00:14:56,000
It made me think of this because he said he'd been practicing for a few months

159
00:14:56,000 --> 00:15:03,000
and he still had these solis problems that they don't think in terms of months.

160
00:15:03,000 --> 00:15:07,000
They have to think in terms of lifetimes for some of it.

161
00:15:07,000 --> 00:15:12,000
How many lifetimes have we been building up the wrong habit?

162
00:15:12,000 --> 00:15:19,000
How much deeper and much more profound than we think?

163
00:15:19,000 --> 00:15:27,000
So what we look at is whether we're inclining in the right direction.

164
00:15:27,000 --> 00:15:32,000
Are we inclining in a wholesome direction or are we inclining in an unwholesome direction?

165
00:15:32,000 --> 00:15:36,000
And just be content with that and work in a way

166
00:15:36,000 --> 00:15:40,000
to bring about greater and greater understanding.

167
00:15:40,000 --> 00:15:44,000
Be reassured that we're, you know, it's getting deeper.

168
00:15:44,000 --> 00:15:48,000
We're heading towards the bottom of the ocean, the bottom of the dumber.

169
00:15:48,000 --> 00:15:51,000
We're getting to the bottom of it.

170
00:15:51,000 --> 00:15:55,000
It's just going to take time.

171
00:15:55,000 --> 00:16:02,000
And just as the ocean doesn't overflow its boundaries in the same way

172
00:16:02,000 --> 00:16:13,000
when the Buddha lays down precepts, the rules.

173
00:16:13,000 --> 00:16:18,000
What's amazing is that people don't transgress them.

174
00:16:18,000 --> 00:16:21,000
Especially a person who practices meditation.

175
00:16:21,000 --> 00:16:30,000
When we are unable to, we're unable to break with the moral code.

176
00:16:30,000 --> 00:16:41,000
There's such a power to the realization of the dumber that people are,

177
00:16:41,000 --> 00:16:50,000
are powerfully disinclined to perform unwholesome deeds.

178
00:16:50,000 --> 00:16:54,000
It goes both ways because without the practice of morality

179
00:16:54,000 --> 00:17:00,000
of course, meditation can succeed.

180
00:17:00,000 --> 00:17:04,000
I've been a bit lax, but we do these online courses

181
00:17:04,000 --> 00:17:09,000
and I've made one so weak with meditators.

182
00:17:09,000 --> 00:17:14,000
So we had one meditator who just explained to me that he's been killed.

183
00:17:14,000 --> 00:17:17,000
He's involved in killing animals.

184
00:17:17,000 --> 00:17:22,000
And I said, oh, well, we can continue with the course.

185
00:17:22,000 --> 00:17:26,000
I mean, there's too much friction involved.

186
00:17:26,000 --> 00:17:28,000
There's too much conflict.

187
00:17:28,000 --> 00:17:35,000
It just won't work.

188
00:17:35,000 --> 00:17:40,000
I have to be vigilant now to remember to make sure that people are keeping

189
00:17:40,000 --> 00:17:45,000
at least the five precepts.

190
00:17:45,000 --> 00:17:47,000
But it will come to a head.

191
00:17:47,000 --> 00:17:53,000
I mean, I've had meditators, I've had people practicing meditation before

192
00:17:53,000 --> 00:17:57,000
who came to realize that they couldn't continue with their livelihood

193
00:17:57,000 --> 00:18:00,000
and they just had to give it up.

194
00:18:00,000 --> 00:18:06,000
And one friend like this who eventually came to a head,

195
00:18:06,000 --> 00:18:09,000
he had to quit with his studies.

196
00:18:09,000 --> 00:18:16,000
He was studying in a lab working on rats.

197
00:18:16,000 --> 00:18:20,000
He was a biologist about to finish his degree in biology

198
00:18:20,000 --> 00:18:22,000
and he had to actually change his major.

199
00:18:22,000 --> 00:18:24,000
He just decided he couldn't continue.

200
00:18:24,000 --> 00:18:29,000
And now he works for the Canadian government, I think.

201
00:18:29,000 --> 00:18:35,000
In public, public, something public.

202
00:18:35,000 --> 00:18:37,000
I can't remember what it was.

203
00:18:37,000 --> 00:18:41,000
Something administrative, nothing to do with biology.

204
00:18:41,000 --> 00:18:48,000
This is amazing that people are through the practice.

205
00:18:48,000 --> 00:18:57,000
They're willing and they're inclined to change their whole lives

206
00:18:57,000 --> 00:19:12,000
in regards to keeping in moral and ethical behavior.

207
00:19:12,000 --> 00:19:18,000
Number three, just as the ocean doesn't associate with the corpse

208
00:19:18,000 --> 00:19:20,000
and thrusts it out.

209
00:19:20,000 --> 00:19:26,000
So too, the Sangha does not associate with a person who is immoral of bad character,

210
00:19:26,000 --> 00:19:33,000
impure of suspect behavior, secretive in his actions.

211
00:19:33,000 --> 00:19:40,000
So again, quite quickly, we expel those who are theoretically,

212
00:19:40,000 --> 00:19:59,000
and not theoretically, but in terms of the ultimate Sangha, the Sangha of meditators.

213
00:19:59,000 --> 00:20:06,000
Of course, even in Buddhism there are bad apples who come and go.

214
00:20:06,000 --> 00:20:10,000
What's amazing is how good people, people who have cultivated meditation,

215
00:20:10,000 --> 00:20:16,000
practice, want nothing to do with those who are not interested in meditation

216
00:20:16,000 --> 00:20:30,000
and those who are unable to see the benefit of cultivating wholesome mind-states.

217
00:20:30,000 --> 00:20:36,000
And it says, even though he is seated in the midst of the Sangha, yet he is far from the Sangha

218
00:20:36,000 --> 00:20:39,000
and the Sangha is far from him.

219
00:20:39,000 --> 00:20:54,000
How unsullied the mind and the world of the meditator is from the into-filed minds,

220
00:20:54,000 --> 00:21:10,000
or the ordinary minds of those who are inclined towards sensual gratification and indulgence in immoral activities.

221
00:21:10,000 --> 00:21:14,000
Number four, even when just as when the Great Rivers reach the ocean,

222
00:21:14,000 --> 00:21:17,000
they give up their former names and it all becomes the ocean.

223
00:21:17,000 --> 00:21:24,000
So too, everyone who comes to the Dhamma, it's amazing is that they all just become Buddhists.

224
00:21:24,000 --> 00:21:30,000
As we're talking about before, the idea that we give up a lot of who we are,

225
00:21:30,000 --> 00:21:35,000
not consciously, but just naturally.

226
00:21:35,000 --> 00:21:59,000
There's, we give up status and identity, personality, and we just become people, kind, compassionate, natural, peaceful people.

227
00:21:59,000 --> 00:22:17,000
We lose all the demarcations of groups and all the barriers of culture and race and class and gender.

228
00:22:17,000 --> 00:22:25,000
It all just disappears.

229
00:22:25,000 --> 00:22:32,000
Number five, even though just as when the ocean doesn't increase or decrease,

230
00:22:32,000 --> 00:22:46,000
even though no matter how much rain falls into it or is evaporated, how do it?

231
00:22:46,000 --> 00:23:02,000
Even when many people attain the truth, it just means that many, many people attain it and it never becomes full.

232
00:23:02,000 --> 00:23:09,000
We can always receive more meditators, receive more.

233
00:23:09,000 --> 00:23:11,000
There can never be too many, basically.

234
00:23:11,000 --> 00:23:17,000
The statement is that nibana doesn't increase, which is actually interesting.

235
00:23:17,000 --> 00:23:27,000
In a sense, it's not like heaven or something, or as heaven gets quite full when a Buddha comes to being.

236
00:23:27,000 --> 00:23:33,000
The angels all remark at how full heaven is becoming because all the Buddhists go up to heaven.

237
00:23:33,000 --> 00:23:39,000
So they come in, they come in the comment on this to the Buddha, but nibana doesn't like that.

238
00:23:39,000 --> 00:23:44,000
Nibana doesn't get more full.

239
00:23:44,000 --> 00:23:50,000
So I guess what you could say about this is how special the teaching is or the goal is.

240
00:23:50,000 --> 00:24:00,000
The goal of the Buddha's teaching is, and then it's infinite.

241
00:24:00,000 --> 00:24:11,000
There's no change. It is beyond samsara, it's really the point.

242
00:24:11,000 --> 00:24:18,000
Number six, just as the ocean has but one taste, so too the dhamma, the Buddha has only one taste.

243
00:24:18,000 --> 00:24:23,000
Well, the ocean is salty, that's the taste of the ocean.

244
00:24:23,000 --> 00:24:31,000
The dhamma is freedom, that's the taste of freedom, the taste of liberation.

245
00:24:31,000 --> 00:24:35,000
So that's what meditation, that's what Buddhism should feel like.

246
00:24:35,000 --> 00:24:38,000
How should it taste to you? What is the flavor of the dhamma?

247
00:24:38,000 --> 00:24:40,000
The flavor of the dhamma should be liberation.

248
00:24:40,000 --> 00:24:46,000
If you feel more trapped and more caught up, then you're not practicing the dhamma.

249
00:24:46,000 --> 00:24:59,000
If you feel more free and more released from your traps and your bonds, your bondage,

250
00:24:59,000 --> 00:25:11,000
more free from judgment and partiality and identification and ego, that's the taste of the dhamma.

251
00:25:11,000 --> 00:25:16,000
Number seven, just as the great ocean contains many precious substances.

252
00:25:16,000 --> 00:25:23,000
Lots of precious, beautiful, magnificent things.

253
00:25:23,000 --> 00:25:26,000
So too the dhamma of the Buddha contains great things.

254
00:25:26,000 --> 00:25:32,000
It contains the four foundations of mindfulness, the four great efforts.

255
00:25:32,000 --> 00:25:40,000
The four, excuse me, the four basis for power, the five faculties, the five powers,

256
00:25:40,000 --> 00:25:44,000
seven factors of enlightenment and the noble eightfold paths.

257
00:25:44,000 --> 00:25:47,000
A lot of jewels there.

258
00:25:47,000 --> 00:25:54,000
A lot of precious and magnificent things to be found in the dhamma.

259
00:25:54,000 --> 00:26:00,000
Number eight, just as the great ocean, is in a boat of great beings.

260
00:26:00,000 --> 00:26:10,000
Whales and sharks and wondrous ancient creatures.

261
00:26:10,000 --> 00:26:15,000
Apparently there are beings that live in the ocean that are 500 yojana, which is 500 leagues,

262
00:26:15,000 --> 00:26:19,000
which is about 8,000 miles.

263
00:26:19,000 --> 00:26:29,000
I don't know what's going on here, but very, very far too big to actually exist in the ocean as we understand it today.

264
00:26:29,000 --> 00:26:33,000
Maybe Sangha can shed some light on this.

265
00:26:33,000 --> 00:26:41,000
Because a yojana is 16 kilometers, 500 times 16 is 8,000 kilometers long.

266
00:26:41,000 --> 00:26:47,000
I don't think such beings could fit in the ocean as we understand it.

267
00:26:47,000 --> 00:26:51,000
Nonetheless, great beings do exist in the dhamma.

268
00:26:51,000 --> 00:26:58,000
They're not that big physically, but they're great and profound mentally.

269
00:26:58,000 --> 00:27:03,000
So, in the dhamma, we have the stream enter the Sotapana,

270
00:27:03,000 --> 00:27:11,000
a person who has freed themselves from a wrong view of self and attachment to rights and rituals

271
00:27:11,000 --> 00:27:13,000
or doubt about the practice.

272
00:27:13,000 --> 00:27:18,000
When we have the Sakadagami who has weakened, has very little greed or anger,

273
00:27:18,000 --> 00:27:22,000
such that they'll only ever be reborn one more time.

274
00:27:22,000 --> 00:27:29,000
And the anagami who has freed themselves from all central desire and all aversion.

275
00:27:29,000 --> 00:27:36,000
I mean, they are a hunt who has freed themselves from all delusion.

276
00:27:36,000 --> 00:27:45,000
And these great and profound beings.

277
00:27:45,000 --> 00:27:52,000
Yeah, maybe they're celestial, that would make sense.

278
00:27:52,000 --> 00:28:00,000
Then they could somehow be an extra dimensional sense of being so big.

279
00:28:00,000 --> 00:28:04,000
So, those are the eight astounding qualities of the dhamma.

280
00:28:04,000 --> 00:28:08,000
Dhamma is something great and profound.

281
00:28:08,000 --> 00:28:16,000
It's like, think of the great ocean.

282
00:28:16,000 --> 00:28:20,000
It's a useful simile because it allows the mind to grasp just how great this teaching is, how profound it is.

283
00:28:20,000 --> 00:28:26,000
Because when you compare the ocean to the dhamma, the ocean is actually quite insignificant.

284
00:28:26,000 --> 00:28:32,000
But we often minimalize or trivialize the dhamma.

285
00:28:32,000 --> 00:28:47,000
And so, by thinking about the great ocean and just how profound and deep, vast and complex, the ocean is.

286
00:28:47,000 --> 00:28:54,000
The dhamma is his vast, profound, deep.

287
00:28:54,000 --> 00:29:06,000
So, there you go. There's our dhamma for tonight.

288
00:29:06,000 --> 00:29:09,000
Today, I have two monitors going.

289
00:29:09,000 --> 00:29:16,000
I've got this little monitor here that's allowing me to...

290
00:29:16,000 --> 00:29:20,000
I can check questions here on the other monitor.

291
00:29:20,000 --> 00:29:34,000
So, hopefully, the recording went well. I think it's got the whole screen in it now.

292
00:29:34,000 --> 00:29:39,000
How does impermanent affect the yogi-mother in relation to her children?

293
00:29:39,000 --> 00:29:43,000
Does she become unattached to them? The question is from a real-life situation.

294
00:29:43,000 --> 00:29:47,000
Yes, absolutely. I had this happen to one of my meditators.

295
00:29:47,000 --> 00:29:54,000
It was great. This meditator, she came to me, a Thai woman.

296
00:29:54,000 --> 00:29:59,000
And her 30-year-old son was...

297
00:29:59,000 --> 00:30:03,000
That's probably not exactly what you're asking, but it's still a good example.

298
00:30:03,000 --> 00:30:06,000
So, he was 30 years old, he was quite old.

299
00:30:06,000 --> 00:30:09,000
But he was being terrible to her, and he had started...

300
00:30:09,000 --> 00:30:14,000
He had taken up Hinduism, but he had gotten involved in this guru,

301
00:30:14,000 --> 00:30:18,000
and he had gotten very egotistical about it, and was very nice to his mother.

302
00:30:18,000 --> 00:30:21,000
So, she was having real trouble with him.

303
00:30:21,000 --> 00:30:26,000
And she said, can you help? Can you help my son? Can you do something to help with this?

304
00:30:26,000 --> 00:30:31,000
And I said, sure, I'll help you come and meditate.

305
00:30:31,000 --> 00:30:39,000
And she was kind of unimpressed by that.

306
00:30:39,000 --> 00:30:44,000
But eventually, she did come and meditate, and she finished a meditation course with me.

307
00:30:44,000 --> 00:30:47,000
And once she finished, I asked her, you know, how's her son doing?

308
00:30:47,000 --> 00:30:50,000
And she said, oh, I don't know. Let him go his own way.

309
00:30:50,000 --> 00:30:55,000
And she had totally changed from being worried and attached to him,

310
00:30:55,000 --> 00:31:00,000
to being totally free and liberated from that.

311
00:31:00,000 --> 00:31:04,000
So, yes, certainly a mother would become...

312
00:31:04,000 --> 00:31:09,000
Question, I suppose, is what would they do in regards to a young child?

313
00:31:09,000 --> 00:31:14,000
I mean, just because you're unattached doesn't mean you don't care for

314
00:31:14,000 --> 00:31:17,000
do positive things for others.

315
00:31:17,000 --> 00:31:23,000
Actually, the opposite is the cases that you care and wish well for others,

316
00:31:23,000 --> 00:31:26,000
even more than before.

317
00:31:26,000 --> 00:31:31,000
Much more inclined to be supportive, so I doubt there would be much problem

318
00:31:31,000 --> 00:31:35,000
for one practicing the Dhamma, but they wouldn't be at all inclined to...

319
00:31:35,000 --> 00:31:42,000
So, another case is I had a meditator who became pregnant.

320
00:31:42,000 --> 00:31:46,000
She came to do a course with me, finished a meditation course,

321
00:31:46,000 --> 00:31:49,000
and then went back to meet her husband.

322
00:31:49,000 --> 00:31:57,000
And they slept together, and when she came back, she was pregnant.

323
00:31:57,000 --> 00:31:59,000
She came back to a second course.

324
00:31:59,000 --> 00:32:03,000
And went home, and her husband actually left her.

325
00:32:03,000 --> 00:32:06,000
He found it, and another wife.

326
00:32:06,000 --> 00:32:13,000
And she was forced now as a quite accomplished meditator

327
00:32:13,000 --> 00:32:16,000
to take care of this young girl.

328
00:32:16,000 --> 00:32:20,000
And wow, the trouble that it was for her,

329
00:32:20,000 --> 00:32:22,000
when the challenge it was for her.

330
00:32:22,000 --> 00:32:26,000
But how well she equipped she was to deal with it.

331
00:32:26,000 --> 00:32:31,000
But, you know, it's even more of a challenge for her

332
00:32:31,000 --> 00:32:34,000
because she's not at all inclined to be a mother.

333
00:32:34,000 --> 00:32:37,000
She was inclined to be a meditator, but...

334
00:32:37,000 --> 00:32:41,000
And then the last she took care of the child,

335
00:32:41,000 --> 00:32:46,000
and it's probably a better mother than most.

336
00:32:50,000 --> 00:32:55,000
I met her, and I met her child later on when she was about five years old.

337
00:32:55,000 --> 00:33:00,000
Crazy kid.

338
00:33:04,000 --> 00:33:09,000
I've heard of the technique of trying to study.

339
00:33:09,000 --> 00:33:13,000
I'm trying to be mindful of the last moment before falling asleep

340
00:33:13,000 --> 00:33:16,000
in the first moment after waking up.

341
00:33:16,000 --> 00:33:21,000
So something a non-intense meditator should try to do.

342
00:33:21,000 --> 00:33:23,000
Yeah, for sure.

343
00:33:23,000 --> 00:33:26,000
But as mindful as you can before you fall asleep.

344
00:33:26,000 --> 00:33:29,000
I mean, it's just a challenge.

345
00:33:29,000 --> 00:33:32,000
It's not like you get a prize when you do,

346
00:33:32,000 --> 00:33:36,000
but it's a sign that you're thinking in that way

347
00:33:36,000 --> 00:33:38,000
will keep you mindful.

348
00:33:38,000 --> 00:33:43,000
It will keep you from falling into a mindless state

349
00:33:43,000 --> 00:33:44,000
before you fall asleep.

350
00:33:44,000 --> 00:33:48,000
And you'll find that your sleep is actually improved because of it.

351
00:33:48,000 --> 00:33:52,000
If your mind will up into the moment before you sleep.

352
00:33:52,000 --> 00:33:57,000
It's a good idea to walking meditation right after waking up.

353
00:33:57,000 --> 00:34:00,000
Yeah, absolutely.

354
00:34:00,000 --> 00:34:01,000
It depends.

355
00:34:01,000 --> 00:34:04,000
If you're really tired walking, just might make you fall over.

356
00:34:04,000 --> 00:34:06,000
It might be too much.

357
00:34:06,000 --> 00:34:10,000
Sometimes sitting for a bit allows you to wake up.

358
00:34:10,000 --> 00:34:12,000
You have to decide.

359
00:34:12,000 --> 00:34:15,000
Sometimes sitting meditation is better right when you wake up,

360
00:34:15,000 --> 00:34:17,000
depending on how you feel.

361
00:34:17,000 --> 00:34:19,000
But again, if you're sleeping well, when you wake up,

362
00:34:19,000 --> 00:34:27,000
you should be refreshed.

363
00:34:27,000 --> 00:34:30,000
To reflect the nine virtues of the Buddha is to consider them

364
00:34:30,000 --> 00:34:36,000
in the terms given of the given explanations.

365
00:34:36,000 --> 00:34:38,000
It is difficult to memorize.

366
00:34:38,000 --> 00:34:40,000
So can Buddha necessarily be done with the guide

367
00:34:40,000 --> 00:34:44,000
or does it require memory?

368
00:34:44,000 --> 00:34:46,000
Well, I don't teach too much about Buddha,

369
00:34:46,000 --> 00:34:51,000
I'm not really going to give details about that.

370
00:34:51,000 --> 00:34:55,000
I mean, it's something that you should know about

371
00:34:55,000 --> 00:34:57,000
and you should be inclined towards,

372
00:34:57,000 --> 00:35:00,000
but you shouldn't put too much thought into it.

373
00:35:00,000 --> 00:35:02,000
It's good to memorize that.

374
00:35:02,000 --> 00:35:03,000
It's good to memorize that.

375
00:35:03,000 --> 00:35:05,000
It's good to memorize that.

376
00:35:05,000 --> 00:35:08,000
But if you can't, you can just say Buddha, Buddha.

377
00:35:08,000 --> 00:35:10,000
That's really enough.

378
00:35:10,000 --> 00:35:25,000
For meditation purposes, you don't need to memorize much.

379
00:35:25,000 --> 00:35:27,000
And that's all the questions.

380
00:35:27,000 --> 00:35:33,000
Are there any local questions here?

381
00:35:33,000 --> 00:35:36,000
It's possible to have a mind of a pick of

382
00:35:36,000 --> 00:35:40,000
when one sees a being of sufferers or our hands always move

383
00:35:40,000 --> 00:35:42,000
to help when they see beings who suffer,

384
00:35:42,000 --> 00:35:46,000
given that they are in a position to help.

385
00:35:46,000 --> 00:35:51,000
They're not really moved to help in that sense.

386
00:35:51,000 --> 00:35:54,000
I mean, often they don't help.

387
00:35:54,000 --> 00:36:02,000
But they're inclined to go with the flow for the most part.

388
00:36:02,000 --> 00:36:09,000
In the sense that if it's the right moment for them to help,

389
00:36:09,000 --> 00:36:14,000
they will act appropriately.

390
00:36:14,000 --> 00:36:17,000
I mean, I would think most people would look at

391
00:36:17,000 --> 00:36:19,000
an our hand and think of them as quite passive

392
00:36:19,000 --> 00:36:23,000
and uninterested, generally uninvolved,

393
00:36:23,000 --> 00:36:25,000
unless it's requested of them,

394
00:36:25,000 --> 00:36:30,000
they would generally not get involved with people's problems.

395
00:36:30,000 --> 00:36:32,000
Because they're free.

396
00:36:32,000 --> 00:36:36,000
I mean, they don't have any desire for it.

397
00:36:36,000 --> 00:36:45,000
They're a desire to help others or desire to change the world.

398
00:36:45,000 --> 00:36:50,000
But they'll never be mean or nasty and cruel.

399
00:36:50,000 --> 00:36:53,000
And if approached to help, they will generally,

400
00:36:53,000 --> 00:36:55,000
I mean, it's generally more if they're approached

401
00:36:55,000 --> 00:36:57,000
and asked to help.

402
00:36:57,000 --> 00:37:04,000
If there's some expectation that they have on them to help,

403
00:37:04,000 --> 00:37:07,000
constrainment will be attained through Cela alone.

404
00:37:07,000 --> 00:37:08,000
It depends what you mean by Cela.

405
00:37:08,000 --> 00:37:11,000
I mean, Cela means normal.

406
00:37:11,000 --> 00:37:15,000
So it can be attained by the proper,

407
00:37:15,000 --> 00:37:16,000
by normal here.

408
00:37:16,000 --> 00:37:19,000
It means what is one's ordinary way.

409
00:37:19,000 --> 00:37:30,000
If one has a is by nature means by Cela mindful.

410
00:37:30,000 --> 00:37:33,000
If by nature they are mindful,

411
00:37:33,000 --> 00:37:36,000
meaning they have as a Cela being mindful.

412
00:37:36,000 --> 00:37:39,000
That's not normally how we use the word in Buddhism,

413
00:37:39,000 --> 00:37:41,000
but it's how what the word actually means.

414
00:37:41,000 --> 00:37:44,000
What is your normal behavior?

415
00:37:44,000 --> 00:37:47,000
So if your normal behavior is good and mindful,

416
00:37:47,000 --> 00:37:52,000
yes, that's enough to become a so dependent,

417
00:37:52,000 --> 00:37:54,000
but not just from keeping precepts precepts,

418
00:37:54,000 --> 00:37:57,000
or one way of understanding the word Cela.

419
00:37:57,000 --> 00:38:01,000
That's not enough.

420
00:38:01,000 --> 00:38:03,000
If you're always guarding the senses,

421
00:38:03,000 --> 00:38:04,000
well, that's enough.

422
00:38:04,000 --> 00:38:06,000
That's being mindful.

423
00:38:06,000 --> 00:38:10,000
If you're watching the mind as things arise

424
00:38:10,000 --> 00:38:12,000
to keep from liking and disliking

425
00:38:12,000 --> 00:38:15,000
and learning to see things objectively,

426
00:38:15,000 --> 00:38:17,000
well, that's enough.

427
00:38:17,000 --> 00:38:20,000
That will lead to enlightenment to Iran.

428
00:38:32,000 --> 00:38:33,000
Okay.

429
00:38:33,000 --> 00:38:35,000
I guess that's all for tonight.

430
00:38:35,000 --> 00:38:38,000
Thank you all for coming out.

431
00:38:38,000 --> 00:38:45,000
I wish you all a good night.

