1
00:00:00,000 --> 00:00:07,000
Yeah, we're ready to start, so good afternoon everyone, we're ready to go, we see the manga,

2
00:00:07,000 --> 00:00:09,000
Robin you want to take it.

3
00:00:09,000 --> 00:00:18,000
Sure, we're on chapter 6, page 169, foundness as a meditation subject, and Aurora, can you start us off?

4
00:00:18,000 --> 00:00:25,000
Now, ten kinds of foundness is corpses without consciousness, we're listed next after the casinos thus.

5
00:00:25,000 --> 00:00:37,000
The bloated, the vivid, the festering, the cut up, the nod, the scattered, the hacked and scattered, the bleeding, the warm and pasted, a skeleton.

6
00:00:37,000 --> 00:00:46,000
The bloated, it is bloated because bloated by gradual dilation and swelling after the close of life as a bellows is with wind.

7
00:00:46,000 --> 00:00:57,000
What is bloated is the same as the bloated, or alternatively, what is bloated is vile because of repulsiveness, but it is the bloated.

8
00:00:57,000 --> 00:01:03,000
This is a term for corpse in that particular state.

9
00:01:03,000 --> 00:01:11,000
The livid, what has patchy discoloration is called livid, what is livid is the same as the livid.

10
00:01:11,000 --> 00:01:18,000
Or alternatively, what is livid is vile because of repulsiveness, thus it is the livid.

11
00:01:18,000 --> 00:01:32,000
This is a term for a corpse that is reddish colored in places where flesh is prominent, whitish colored in places where pus has collected, but mostly blue black as if draped with

12
00:01:32,000 --> 00:01:41,000
blue black cloth in the blue black places.

13
00:01:41,000 --> 00:01:46,000
The festering, what is trickling with pus in broken places is festering.

14
00:01:46,000 --> 00:01:56,000
What is festering is the same as the festering, or alternatively, what is festering is vile because of repulsiveness, thus it is the festering.

15
00:01:56,000 --> 00:02:08,000
This is the term for corpse in that particular state.

16
00:02:08,000 --> 00:02:28,000
So can you be the next one?

17
00:02:28,000 --> 00:02:47,000
This is the term for a corpse cut in the middle.

18
00:02:47,000 --> 00:03:01,000
No cannot, what has been chewed here and there in various way by dogs, jackals, etc., is what is not.

19
00:03:01,000 --> 00:03:22,000
What is not is the same as the gnote or alternatively, what is not is vile because of repulsiveness, thus it is the gnote.

20
00:03:22,000 --> 00:03:32,000
This is a term for a corpse in that particular state.

21
00:03:32,000 --> 00:03:40,000
The scattered, what is strewn about is scattered, what is scattered is the same as the scattered.

22
00:03:40,000 --> 00:03:47,000
Or alternatively, what is scattered is vile because of repulsiveness that it is the scattered.

23
00:03:47,000 --> 00:03:58,000
What is trying to do here is explain the cut on the end, which isn't really all that necessary.

24
00:03:58,000 --> 00:04:07,000
This is just a derivation, but I guess the point is we keep the, and so on, can stand on its own as an adjective.

25
00:04:07,000 --> 00:04:18,000
So the question is why do you have to add cut on the ending?

26
00:04:18,000 --> 00:04:44,000
When we keep the cut, it could be considered the noun, we keep it as an adjective meaning scattered in this example.

27
00:04:44,000 --> 00:04:53,000
This is a result of the nyka, nyka suffix, which becomes cut.

28
00:04:53,000 --> 00:05:00,000
I am not quite sure about that, but anyway, it comes to be a cut on the ending.

29
00:05:00,000 --> 00:05:04,000
It doesn't really have any to do with good chita.

30
00:05:04,000 --> 00:05:12,000
I think that is more of an instructive etymology than historical.

31
00:05:12,000 --> 00:05:18,000
It probably isn't where it comes from, it's just adding cut on the end, it makes it that much is.

32
00:05:18,000 --> 00:05:24,000
Anyway, this is a term for a corpse that is strewn here and there in this way.

33
00:05:24,000 --> 00:05:31,000
Here, hand, there are foot, there, the head.

34
00:05:31,000 --> 00:05:44,000
The half in scatter is hacked and it is scattered in the way just described, thus it is hacked and scattered.

35
00:05:44,000 --> 00:05:51,000
This is a term for a corpse scattered in the way just described after it has been hacked with a knife in a crow's foot pattern on every limb.

36
00:05:51,000 --> 00:06:02,000
The bleeding, its sprinkles, karate, scatters, blood, and it trickles here and there, thus it is the bleeding.

37
00:06:02,000 --> 00:06:12,000
This is a term for a corpse smeared with trickling blood.

38
00:06:12,000 --> 00:06:24,000
The worm invested, it is maggots that are called worms, its sprinkles worms, thus it is worm invested. This is the term for a corpse full of maggots.

39
00:06:24,000 --> 00:06:36,000
A skeleton, bone, at the, is the same as skeleton, at tikka, or alternately bone, at t, is wild, kochita, because of reposiveness.

40
00:06:36,000 --> 00:06:47,000
Thus it is a skeleton at tikka. This is a term for both, for a single bone and for a framework of bones.

41
00:06:47,000 --> 00:07:06,000
These names are also used both for the signs that arise with the loaded etc as their support and for the Janas obtained in the signs.

42
00:07:06,000 --> 00:07:26,000
Here in when a meditator wants to develop the Janas called Over the Bloated by arousing the sign of the bloated on a bloated body, he should in the way already described, approach a teacher of the kind mentioned under the earth casino, and learn the meditation, meditation subject from him.

43
00:07:26,000 --> 00:07:46,000
In explaining the meditation subject to him, the teacher should explain it all, that is the directions for going with the aim of acquiring the sign of foulness, the characterizing of the surrounding signs, the 11 ways of apprehending the sign, the reviewing of the path gone by and come by, concluding with the directions for absorption.

44
00:07:46,000 --> 00:08:15,000
And when the meditator has learned it all well, he should go to an abode of the kind already described and live there while seeking the sign of the bloated. So it's bypassing much, as I said it would, that was already described in the section on Patuikka senior, the rest are just going to be, in many ways they're similar, the development is similar.

45
00:08:15,000 --> 00:08:36,000
Meanwhile, when he hears people saying that at some village gate or in some road, or at some forest edge, or at the base of some rock, or at the root of some tree, or in some carnal ground, a bloated corpse is lying, he should not go there at once, like one who plunges into a river, where there is no forward.

46
00:08:36,000 --> 00:09:03,000
Why not? Because this foulness is beset by wild beasts and non-human beings, and he might risk his life there, or perhaps the way to go to it goes by a village gate or a bathing place or an irrigated field, and there are visible object of the opposite sex might come into focus, or perhaps the body is of the opposite sex for a female body is unsuitable for a man and a male body for a woman.

47
00:09:03,000 --> 00:09:19,000
If only recently did, it may even look beautiful, hence there might be danger to the life of purity, but if he judges himself, this is not difficult for one like me, then he can go there.

48
00:09:19,000 --> 00:09:35,000
And when he goes, and when he goes, he should do so only after he has spoken to the senior elder of the community, or to some well-known beaker.

49
00:09:35,000 --> 00:09:53,000
At the channel ground, or if his goat rises when he is confronted with disagreeable objects, such as visible forms and sounds of non-human beings, lions, tigers, etc., or something else afflicts him.

50
00:09:53,000 --> 00:10:07,000
Then he whom he told will have his bowl and drop well looked after in the monastery, or he will care for him by sending him because one of us has to him.

51
00:10:07,000 --> 00:10:28,000
Besides, robbers may mention this thinking, a channel ground, a safe place for them, whether or not they have done anything wrong, and when men chase them, they drop their goods near the beaker and wrong away.

52
00:10:28,000 --> 00:10:44,000
Perhaps the men sees the beaker saying, we have found the thief with the goods and only them, only him.

53
00:10:44,000 --> 00:11:01,000
And he whom he told will explain to the men, do not bother him. He went to do this special work after telling me, and he will rescue him.

54
00:11:01,000 --> 00:11:14,000
This is the advantage of going only after informing someone.

55
00:11:14,000 --> 00:11:35,000
Therefore, he should inform a beaker of the kind described, and then set out eager to see the sign, and as happy and joyful as a warrior noble on his way to the scene of anointing, as one going to offer libations at the hall of sacrifice, or as a popper on his way to unearth the hidden treasure.

56
00:11:35,000 --> 00:11:42,000
And he should go there in the way advised by the commentaries.

57
00:11:42,000 --> 00:12:01,000
For this is said, one who is learning the bloated sign of foulness goes along with no companion, with unremitting mindfulness established, with his sense faculties turned inwards, with his mind not turned outwards, reviewing the path gone by and come by.

58
00:12:01,000 --> 00:12:15,000
And the place where the bloated sign of foulness has been left, he notes any stone or termite mound or tree or bush or creeper, there each with its particular sign and in relation to the object.

59
00:12:15,000 --> 00:12:23,000
When he has done this, he characterizes the bloated sign of foulness by the fact of its having attained that particular individual essence.

60
00:12:23,000 --> 00:12:47,000
Then he sees that the sign is properly apprehended, and that it is properly remembered, that it is properly defined by its color, by its mark, by its shape, by its direction, by its location, by its delimitation, by its joints, by its openings, by its concavities, by its convexities, and all around.

61
00:12:47,000 --> 00:13:06,000
When he has properly apprehended the sign, properly remembered it, properly defined it, he goes alone with no companion, with unremitting mindfulness established with his sense faculties turned inwards, with his mind not turned outwards, reviewing the path gone by and come by.

62
00:13:06,000 --> 00:13:16,000
When he walks, he resolves, that his walk is oriented towards it, when he sits, he prepares a seat that is oriented towards it.

63
00:13:16,000 --> 00:13:21,000
What is the purpose? What is the advantage of characterizing the surrounding signs?

64
00:13:21,000 --> 00:13:28,000
Characterizing the surrounding signs has known the lotion for his purpose. It has known the lotion for his advantage.

65
00:13:28,000 --> 00:13:48,000
What is the purpose? What is the advantage of apprehending the sign in the other 11 ways?

66
00:13:48,000 --> 00:13:54,000
Apprehending the sign in the other 11 ways has anchoring the mind for his purpose.

67
00:13:54,000 --> 00:14:01,000
It has anchoring the mind for his advantage. What is the purpose? What is the advantage of reviewing the path gone by and come by?

68
00:14:01,000 --> 00:14:15,000
Reviewing the path gone by and come by has keeping the mind on the track for his purpose. It has keeping the mind on track for his advantage.

69
00:14:15,000 --> 00:14:25,000
When he has established reverence for it by seeing its advantages and by perceiving it as a treasure, and so come to love it, he anchors his mind upon that object.

70
00:14:25,000 --> 00:14:29,000
Surely in this way I shall be liberated from aging and death.

71
00:14:29,000 --> 00:14:37,000
Quite secluded from sense desires, secluded from unprofitable things, he enters upon and dwells in the first jhana, seclusion.

72
00:14:37,000 --> 00:14:50,000
He has arrived in the first jhana at the fine material sphere. His is a heavenly abiding and an instance of the meritorious action consisting and meditative development.

73
00:14:50,000 --> 00:15:19,000
So if he goes to the jhana ground to test his control of mind, let him do so or just strike in the gong or summoning a chapter. If he goes there mainly for developing that meditation or meditation subject, let him go learn with no companion without renouncing his basic meditation subject and keeping it always in mind, taking a walking stick or a staff to keep off attacks by dogs, etc.

74
00:15:19,000 --> 00:15:37,000
Ensuring, unremitting mindfulness by establishing it well, which is mine not turned out towards because he has ensured that his faculties of which his mind is the sixth are turned in words.

75
00:15:37,000 --> 00:15:54,000
As he goes out of the monastery, he should not negate. I have gone out in such a direction by such a gate. After that he should define a path by which he goes.

76
00:15:54,000 --> 00:16:23,000
This path goes in an easter really direction, westerly, north, northern, gnarly, south,rly, direction, or it goes in an intermediate direction.

77
00:16:23,000 --> 00:16:43,000
And in this place it goes to the left, in this place to the right, and in this place there is a stone, in this term, termite mount.

78
00:16:43,000 --> 00:17:00,000
In this tree, in this bush, in this scraper, he should go to the place where the sign is defined, making this way path by which he goes.

79
00:17:00,000 --> 00:17:19,000
The point here is to keep mindfulness and to set up a continuous mindfulness. It's kind of an easing into, so that by the time a meditator arrives at the journal ground, they are able to be objective because the idea is, in some ways, this is going to be a difficult subject.

80
00:17:19,000 --> 00:17:46,000
But this is sort of a common idea throughout the meditation teachings of continuity, the importance of continuity, so not ever giving up one's mindfulness, even when not doing formal practice.

81
00:17:46,000 --> 00:18:03,000
And he should not approach it upwind, for if he did so, and the smell of corpse is a sale, a sale of his nose, his brain might get upset, or he might throw up his food, or he might repent his coming thinking, what a place of corpses I have come to.

82
00:18:03,000 --> 00:18:28,000
So instead of approaching it upwind, he should go downwind. If he cannot go by a downwind path, if there is a mountain or a ravine or a rock or a fence or a patch of thorns, or a water or a bog in this way, then he should go stopping his nose with the corner of his robe. These are the duties in going.

83
00:18:28,000 --> 00:18:36,000
When he has gone there in this way, he should not at once look at the sign of foulness. He should make sure of the direction.

84
00:18:36,000 --> 00:18:43,000
For perhaps if he stands in a certain direction, the object does not appear clearly to him, and his mind is not willy.

85
00:18:43,000 --> 00:19:00,000
So rather than there, he should stand where the object appears clearly, and his mind is willy. And he should avoid standing to leeward or to winward of it. For if he stands to leeward, he is bothered by the corpse smell and his mind's trays.

86
00:19:00,000 --> 00:19:15,000
And if he stands to winward and non-human beings are dwelling there, they may get annoyed and do him a mischief. So he should move around a little and not stand too much to winward.

87
00:19:15,000 --> 00:19:36,000
Then he should stand not too far off or to near, or too much towards the feet or the head. If he stands too far off, the object is not clear to him, and if he stands to near, he may get frightened. If he stands too much toward the feet or the head, not all the foulness becomes manifest to her equally.

88
00:19:36,000 --> 00:19:46,000
So she should not stand too far off or to near, opposite the middle of the body, and a place convenient for her to look at.

89
00:19:46,000 --> 00:20:10,000
Then he should characterize the surrounding signs in the way it stated to us. In the place where the bloating sign of foulness has been left, he notes any stones or creeper there in its sign.

90
00:20:10,000 --> 00:20:22,000
In this way, this rock is high or low, small or large, brown or black, or white, long or round, after which he should observe the relative positions thus.

91
00:20:22,000 --> 00:20:33,000
In this place, this is a rock, this is the sign of foulness, this is the sign of foulness, this is a rock.

92
00:20:33,000 --> 00:20:49,000
If there is a termite mound, he should define it in this way, this is high or low, small or large, brown or black, or white, long or round, after which he should observe the relative positions thus.

93
00:20:49,000 --> 00:20:56,000
In this place, there is a termite mound, this is the sign of foulness.

94
00:20:56,000 --> 00:21:21,000
If there is a tree, he should define it in this way, this is a pipe, thick tree, or a banyan thick tree, or a chaga thick tree, or a teeta thick tree.

95
00:21:21,000 --> 00:21:37,000
It is tall or short, small or large, black or white, after which he should observe the relative positions thus.

96
00:21:37,000 --> 00:21:47,000
In this place, this is a tree, this is the sign of foulness.

97
00:21:47,000 --> 00:21:58,000
If there is a bush, he should define it in this way, this is a cindy bush, or a caramanda bush, or a caramira bush, or a caranda bush.

98
00:21:58,000 --> 00:22:05,000
It is tall or short, small or large, after which he should observe the relative positions thus.

99
00:22:05,000 --> 00:22:12,000
In this place, there is a bush, this is the sign of foulness.

100
00:22:12,000 --> 00:22:27,000
If there is a creeper, he should define it in this way, this is a pumpkin creeper, or a gourd keeper, creeper, or a brown creeper, or a black creeper, or a stinking creeper, after which he should observe the relative positions thus.

101
00:22:27,000 --> 00:22:34,000
In this place, this is a creeper, this is the sign of foulness, this is the sign of foulness, this is a creeper.

102
00:22:34,000 --> 00:22:43,000
The point here is to separate them. He is not saying that the creeper is foul, etc. He is saying, here is this corpse, and here is this other thing.

103
00:22:43,000 --> 00:22:59,000
A lot of this is incredibly, incredibly, pedantic, and in my everyday, overly rigid, but the point is to not leave anything to chance, and to make it totally scientific.

104
00:22:59,000 --> 00:23:07,000
It is like setting up a double-blind study, or a sterile lab, or something like that.

105
00:23:07,000 --> 00:23:15,000
I mean, this isn't nearly, you could go a lot worse than this, but this is taking extra precautions to be completely short.

106
00:23:15,000 --> 00:23:21,000
We should appreciate it. I think we shouldn't discard this sort of preparation as being unnecessary.

107
00:23:21,000 --> 00:23:39,000
We often are quick to just jump in and try something, which is really the lazy way. Setting it up like this is definitely this idea of setting things up in such a rich, structured fashion is definitely not to be underestimated.

108
00:23:39,000 --> 00:23:55,000
I think actually if I was approaching a dead body, I'd be just taking my time and observing everything and trying to buy a little time too.

109
00:23:55,000 --> 00:24:11,000
They are not the sign of foulness. They are other than the sign of foulness. The sign of foulness is referring to the corpse.

110
00:24:11,000 --> 00:24:29,000
They don't identify as them, right? They don't identify as them as what they are as being separate to the actual sign of foulness, which is the body. So he's defining different parts to his surroundings. Here's the body. Here's a brush. Here's the body. Here's a tree.

111
00:24:29,000 --> 00:24:45,000
I see. I can't thank you.

112
00:24:45,000 --> 00:24:59,000
Whenever I see them, I kind of am affected by that. I don't know if I've ever had a chance to actually go to a carnal ground, but that kind of is like an opportunistic carnal ground, I suppose. Not quite this detailed though.

113
00:24:59,000 --> 00:25:13,000
But it's actually, I think, quite different. That being affected by it is something you want to avoid. It's not actually, I don't think, considered beneficial to have that reaction. So you're trying to avoid that.

114
00:25:13,000 --> 00:25:32,000
You're trying to gain.

115
00:25:32,000 --> 00:25:48,000
You're trying to hear cultivate the objectivity in advance before you actually look at the body so that when you look at it, you're able to enter into actually the genre, the idea here, maybe not the genre, but we're trying to get to a very calm state. It might just be a good job.

116
00:25:48,000 --> 00:26:07,000
Thank you.

117
00:26:07,000 --> 00:26:27,000
You're not quite almost, but it's more, it would bring about agitation.

118
00:26:27,000 --> 00:26:48,000
That's probably more akin to the effectiveness that I feel when I see just a random dead animal or something.

119
00:26:48,000 --> 00:27:01,000
Yeah, being affected like that may not be whole, so I'm not sure.

120
00:27:01,000 --> 00:27:22,000
Yeah, I suppose, I mean, any experience like that can be usually helpful.

121
00:27:22,000 --> 00:27:32,000
Everything is perfect and dominant and lasting.

122
00:27:32,000 --> 00:27:49,000
That actually is, is that it may instigate the reflection. That is, oh, yes, I too will die, but often it's just propulsion, which is, I think negative. I think I'm also

123
00:27:49,000 --> 00:28:03,000
going to say, maybe it should be the same as Prince, what Prince he thought when he saw dead body.

124
00:28:03,000 --> 00:28:25,000
They're seeing, it was a surprise to him that such a thing existed. He didn't realize that it was possible to get all sick of died. You know, think of many people that when they see a dead body, they just don't want to see it and they run away from it and they totally shut it down and actually becomes a cause for increased aversion to even thinking about death.

125
00:28:25,000 --> 00:28:40,000
Well, many people to see a dead body, it just makes them hated with hate death even more, you know, be less prepared for death.

126
00:28:40,000 --> 00:28:48,000
So this seems like it's really just really taking your time and just making sure you're ready for it when you when you're finally get there.

127
00:28:48,000 --> 00:29:09,000
It's an argument for that being important that it's not always good. It's not it's often not good to see dead people or so on that if your mind's not in the right place, it can traumatize you and it can make you hate death even more and be less ready to accept and to let go at the moment of death.

128
00:29:09,000 --> 00:29:13,000
I think there's an argument for that.

129
00:29:13,000 --> 00:29:42,000
When I drive in a room and if I see animal body on the side, I usually say to myself, take refuge to Buddha, take refuge to Sangha, take refuge to Gama and wish them not to become animal.

130
00:29:42,000 --> 00:29:54,000
Well, the next life it's kind of blessing to them, come myself down.

131
00:29:54,000 --> 00:29:56,000
There you go.

132
00:29:56,000 --> 00:30:09,000
I think that actually adds to the point of how important it is to calm yourself down and how much better it is to enter into the calm state.

133
00:30:09,000 --> 00:30:22,000
But what we're talking about here is having approached it from that calm state where you can actually be objective because the point is that it's so shocking to us.

134
00:30:22,000 --> 00:30:50,000
But the key is to use that power of the experience from an objective point of view without any of the reactivity to let it open you up and to let the power just pull push you forward towards at least a calm state.

135
00:30:53,000 --> 00:30:58,000
One day, are these type of meditation subjects still done today still taught to monks and things?

136
00:30:58,000 --> 00:31:02,000
Oh, yeah, absolutely.

137
00:31:02,000 --> 00:31:09,000
This one in particular, there's lots of meditation on the dead bodies going on, especially in Sri Lanka.

138
00:31:09,000 --> 00:31:18,000
Not that much, a little bit in Thailand, but a lot in Sri Lanka from what I hear.

139
00:31:18,000 --> 00:31:25,000
Yeah, but I don't think it's easy to find a bloated body in Sri Lanka.

140
00:31:25,000 --> 00:31:30,000
Right, not mostly just dead and cut up, but in the more.

141
00:31:30,000 --> 00:31:40,000
Right.

142
00:31:40,000 --> 00:31:44,000
I think we're on number 34.

143
00:31:44,000 --> 00:31:50,000
Also, with its particular sign and in relation to the object was said.

144
00:31:50,000 --> 00:31:57,000
But that is included by what has just been said for he characterizes it with its particular sign.

145
00:31:57,000 --> 00:32:03,000
When he divines it again and again and he characterizes it in relation to the object.

146
00:32:03,000 --> 00:32:07,000
When he defines it by combining it each time in pairs thus.

147
00:32:07,000 --> 00:32:09,000
This is a rock.

148
00:32:09,000 --> 00:32:11,000
This is the sign of foulness.

149
00:32:11,000 --> 00:32:14,000
This is the sign of foulness.

150
00:32:14,000 --> 00:32:17,000
This is a rock.

151
00:32:17,000 --> 00:32:24,000
Having done this, again, he should bring to mind the fact that it has an individual essence.

152
00:32:24,000 --> 00:32:28,000
Its own state of being bloated, which is not common to anything else.

153
00:32:28,000 --> 00:32:37,000
Since it was said that he defines it by the fact of it having attained that particular individual essence.

154
00:32:37,000 --> 00:32:47,000
The meaning is that it should be defined according to individual essence, according to its own nature, as the inflated, the bloated.

155
00:32:47,000 --> 00:32:54,000
Having defined it in this way, he should apprehend the sign in the following six ways.

156
00:32:54,000 --> 00:33:06,000
That is to say by its color, by its mark, by its shape, by its direction, by its location, by its delineation.

157
00:33:06,000 --> 00:33:11,000
How?

158
00:33:11,000 --> 00:33:14,000
The meditator should define it by its color thus.

159
00:33:14,000 --> 00:33:20,000
This is the body of one who is black or white or yellow skinned.

160
00:33:20,000 --> 00:33:27,000
Instead of defining it by female mark or the male mark, he should define it by its mark thus.

161
00:33:27,000 --> 00:33:35,000
This is the body of one who was in the first phase of life, in the middle phase of, in the middle phase, in the last phase.

162
00:33:35,000 --> 00:33:45,000
By its shape, he should define it only by the shape of the bloated thus.

163
00:33:45,000 --> 00:33:57,000
This is the shape of its head, this is the shape of its neck, this is the shape of its pen, this is the shape of its chest.

164
00:33:57,000 --> 00:34:04,000
This is the shape of its belly, this is the shape of its novel.

165
00:34:04,000 --> 00:34:20,000
This is the shape of its hips, this is the shape of its thigh, this is the shape of its cough, this is the shape of its foot.

166
00:34:20,000 --> 00:34:24,000
He should define it by its direction thus.

167
00:34:24,000 --> 00:34:32,000
There are two directions in this body that is down from the navel as the lower direction, and up from it as the upper direction.

168
00:34:32,000 --> 00:34:35,000
For alternatively, he can define a thus.

169
00:34:35,000 --> 00:34:37,000
I am standing in this direction.

170
00:34:37,000 --> 00:34:41,000
The sign of foulness is in that direction.

171
00:34:43,000 --> 00:34:46,000
He should define it by its location thus.

172
00:34:46,000 --> 00:34:53,000
The hand is in this location, the foot in this, the head in this, the middle of the body in this.

173
00:34:53,000 --> 00:34:56,000
Or alternatively, he can define a thus.

174
00:34:56,000 --> 00:34:58,000
I am in this location.

175
00:34:58,000 --> 00:35:03,000
The sign of foulness is in that.

176
00:35:04,000 --> 00:35:07,000
He should define it by its delimitation thus.

177
00:35:07,000 --> 00:35:15,000
This body is delimited below by the soles of the feet, above by the tips of the hair, all around by the skin.

178
00:35:15,000 --> 00:35:20,000
The space so delimited is filled up with 32 pieces of corpse.

179
00:35:20,000 --> 00:35:23,000
Or alternatively, he can define a thus.

180
00:35:23,000 --> 00:35:26,000
This is the delimitation of its hand.

181
00:35:26,000 --> 00:35:29,000
This is the delimitation of its foot.

182
00:35:29,000 --> 00:35:31,000
This is the delimitation of its head.

183
00:35:31,000 --> 00:35:35,000
This is the delimitation of the middle part of its body.

184
00:35:35,000 --> 00:35:39,000
Or alternatively, he can delimit as much of it as he has apprehended thus.

185
00:35:39,000 --> 00:35:44,000
Just this much of the bloated is like this.

186
00:35:44,000 --> 00:35:55,000
Okay, I am going to suggest that we stop there where we started wailing, but some people probably have to leave, so I don't think we should go on too much longer.

187
00:35:55,000 --> 00:35:58,000
We almost got half done there.

188
00:35:58,000 --> 00:36:00,000
Hopefully next time we'll finish it off.

189
00:36:00,000 --> 00:36:02,000
So let's take a break.

190
00:36:02,000 --> 00:36:29,000
Five minutes or ten minutes and we'll come back and do poly.

