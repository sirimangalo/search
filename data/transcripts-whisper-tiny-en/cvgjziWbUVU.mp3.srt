1
00:00:00,000 --> 00:00:12,560
Welcome to Ask a Monk, I'm going to try to use my phone as a video camera, it's got

2
00:00:12,560 --> 00:00:15,280
a pretty good camera on it.

3
00:00:15,280 --> 00:00:21,240
So today I thought I would answer the question about alcohol, and I know I've talked

4
00:00:21,240 --> 00:00:26,880
about this before in other videos but never as a separate topic, so I'd just like to

5
00:00:26,880 --> 00:00:39,800
go over basically the reasons why our practice is to refrain from alcohol entirely.

6
00:00:39,800 --> 00:00:49,320
The question was whether it's necessary to abstain from alcohol 100 percent when taking

7
00:00:49,320 --> 00:00:54,800
alcohol once in a while can bring pleasure and the person even said it's a great pleasure

8
00:00:54,800 --> 00:00:56,720
for them to drink alcohol.

9
00:00:56,720 --> 00:01:00,000
So what's the problem?

10
00:01:00,000 --> 00:01:07,000
I think this comes from a, in general it comes from a lack of understanding of what the

11
00:01:07,000 --> 00:01:14,560
moral precepts are for and a lack of understanding of Buddhist morality because Buddhist

12
00:01:14,560 --> 00:01:22,280
morality is not like say Christian morality in a classical sense.

13
00:01:22,280 --> 00:01:28,760
I mean Christian morality is a diverse topic but the Judeo-Christian morality in general

14
00:01:28,760 --> 00:01:39,240
is thought to, in its classical form, is thought to be because of an injunction from God

15
00:01:39,240 --> 00:01:49,040
and because of the effect that it has on other people, it's bad to kill and so on.

16
00:01:49,040 --> 00:01:56,200
And our general understanding of morality is because of the effect that it has on other

17
00:01:56,200 --> 00:02:00,800
people, so something is immoral if it hurts someone else and so on.

18
00:02:00,800 --> 00:02:07,320
Buddhist morality, as I've said before, is totally the opposite, it's concerned entirely

19
00:02:07,320 --> 00:02:14,960
with the effect that it has on oneself because the idea being that if you do something

20
00:02:14,960 --> 00:02:19,720
based on greed, anger, and delusion, it hurts yourself and in fact the causing suffering

21
00:02:19,720 --> 00:02:27,200
for other people has a much greater impact on your own mind than on the other person.

22
00:02:27,200 --> 00:02:36,440
It's possible to be the victim of violence or theft or so on and escape without any

23
00:02:36,440 --> 00:02:42,280
damage whatsoever depending on a person's ability to deal with change and stress and so

24
00:02:42,280 --> 00:02:54,840
on, but a person who commits the act is in a much graver danger or has a much, isn't

25
00:02:54,840 --> 00:03:01,520
able to escape the effect that the act has on one's mind and because of the inherent

26
00:03:01,520 --> 00:03:04,880
greed, anger, and delusion involves.

27
00:03:04,880 --> 00:03:09,840
So I think if we understand morality in this way and if we really understand Buddhist

28
00:03:09,840 --> 00:03:17,080
morality then it's, I think, the fifth precept on not taking drugs in alcohol is actually

29
00:03:17,080 --> 00:03:22,800
I would say the easiest to understand because it has the most direct impact on one's

30
00:03:22,800 --> 00:03:27,360
own mind and in fact it's the opposite of meditation practice.

31
00:03:27,360 --> 00:03:32,040
So if you practice a little bit of meditation and if you're mindful a little bit, you gain

32
00:03:32,040 --> 00:03:34,120
benefit and clarity of mind.

33
00:03:34,120 --> 00:03:39,720
If you drink alcohol just a little bit, it has the opposite effect and you lose clarity

34
00:03:39,720 --> 00:03:42,840
of mind, you lose awareness and so on.

35
00:03:42,840 --> 00:03:49,120
So if you're very mindful it has a great effect, if you're very, if you consume a lot

36
00:03:49,120 --> 00:03:55,920
of alcohol it has the opposite effect, you become completely unaware and have no clarity

37
00:03:55,920 --> 00:03:58,280
of mind whatsoever.

38
00:03:58,280 --> 00:04:03,200
So the idea that first of all that it's harmless because it's not hurting other people

39
00:04:03,200 --> 00:04:10,320
is totally missing the point that it has a direct impact on one's own mind and also

40
00:04:10,320 --> 00:04:17,480
the idea that you could have a little bit of alcohol without getting drunk and so on

41
00:04:17,480 --> 00:04:25,440
is really, you know, there is no line that says now I'm drunk and now I'm not a person

42
00:04:25,440 --> 00:04:30,880
who drinks one glass of alcohol, I mean I'm not an angel, I've done all this before

43
00:04:30,880 --> 00:04:37,400
myself, a person who has a little bit of alcohol becomes a little bit less alert and

44
00:04:37,400 --> 00:04:38,400
aware.

45
00:04:38,400 --> 00:04:42,640
This is really why we do it and why there's so much pleasure, I mean you can ask yourself

46
00:04:42,640 --> 00:04:48,400
where is the pleasure coming from, there is no chemical in alcohol as far as I know

47
00:04:48,400 --> 00:04:51,200
that stimulates pleasure in the mind.

48
00:04:51,200 --> 00:04:57,360
So what's really happening is you're losing your sense of shame and your sense of morality,

49
00:04:57,360 --> 00:05:00,360
your sense of awareness of what you're doing.

50
00:05:00,360 --> 00:05:04,320
And once you're drunk, once you're intoxicated, you know everything seems funny, everything

51
00:05:04,320 --> 00:05:11,400
seems interesting, you become stupid, you become dumb, you feel like you're intelligent

52
00:05:11,400 --> 00:05:18,040
and wise and you're carrying on all these conversations and everyone must be really keen

53
00:05:18,040 --> 00:05:21,760
on you or everyone must think you're such a great person and in fact I think you're

54
00:05:21,760 --> 00:05:26,280
an idiot and you're just making a fool of yourself and so on.

55
00:05:26,280 --> 00:05:34,480
I mean I grew up in the boonies, you know, surrounded by, you know we lived out in the country

56
00:05:34,480 --> 00:05:40,200
so we would have bush parties and you know the things people did and you can't say

57
00:05:40,200 --> 00:05:48,520
that there is any honor or any nobility to be had from the pleasure that comes from alcohol.

58
00:05:48,520 --> 00:05:53,600
It doesn't bring pleasure, it brings the inability to discern what is right and wrong

59
00:05:53,600 --> 00:05:58,040
and so as a result we're able to do all sorts of wrong things without feeling guilty, without

60
00:05:58,040 --> 00:06:11,280
feeling upset about them and therefore you have this freedom, this sense of inhibition

61
00:06:11,280 --> 00:06:19,640
or lack of inhibition, so that doesn't change the fact that what you're doing is really,

62
00:06:19,640 --> 00:06:26,000
really dumb and really, really wrong and it's changing your mind and it's changing the

63
00:06:26,000 --> 00:06:32,240
nature of other people's minds, it's changing your environment and it's leading you towards

64
00:06:32,240 --> 00:06:39,600
a social setting and a lifestyle that is surrounded by people who are immoral, who are engaged

65
00:06:39,600 --> 00:06:44,000
in, you know, pretty much everything else and this is what Buddhist always say, you know

66
00:06:44,000 --> 00:06:47,760
alcohol leads to everything else and it really, really does.

67
00:06:47,760 --> 00:06:55,240
I mean, your stupidity of mind, the lack of clarity, the mind which is dull and so on

68
00:06:55,240 --> 00:07:02,600
and is able to get past its inhibitions against doing other immoral acts will lead you

69
00:07:02,600 --> 00:07:09,120
to surround yourself with immoral people and to engage in immoral acts without thought,

70
00:07:09,120 --> 00:07:13,280
without consciousness, I mean I've been there, I've done it all, I'm not, this isn't talking

71
00:07:13,280 --> 00:07:19,440
from theory and I think if people are honest with themselves, they really have to wake

72
00:07:19,440 --> 00:07:26,480
up and see that this is the case and in their own lives, this is what's happening for

73
00:07:26,480 --> 00:07:32,600
people who drink alcohol, there's a lot of, you know, it changes your mind so it changes

74
00:07:32,600 --> 00:07:39,200
your life and there's a great number of things that come along with it, it's the opposite

75
00:07:39,200 --> 00:07:45,400
of the meditation practice and so if the meditation practice brings clarity or the clarity

76
00:07:45,400 --> 00:07:51,840
from meditation brings goodness, brings happiness, brings peace then the lack of clarity,

77
00:07:51,840 --> 00:07:57,560
the dullness that comes from drinking alcohol even though it has some great pleasure when

78
00:07:57,560 --> 00:08:03,160
you're doing it is going to drag you down and down and down and turn you into a base

79
00:08:03,160 --> 00:08:12,040
and immoral person, it really does do that, I mean and again it's totally a matter

80
00:08:12,040 --> 00:08:21,760
of quality, so if you drink a little bit, it degrades your life, it degrades your mind

81
00:08:21,760 --> 00:08:25,920
a little bit, if you drink a lot, it degrades your life to a great extent so it's not

82
00:08:25,920 --> 00:08:29,200
saying that you drink once in a while, you're going to go to hell or you're going to

83
00:08:29,200 --> 00:08:35,000
have a terrible life or so but it's a bad thing to do, so which brings us to the point

84
00:08:35,000 --> 00:08:40,320
of well what's wrong with taking alcohol once in a while and so people will say well

85
00:08:40,320 --> 00:08:45,760
you know they have this rule but you know if I just take it once in a while well it's

86
00:08:45,760 --> 00:08:50,800
not going to make me an evil person and no of course it's not, a person can take a glass

87
00:08:50,800 --> 00:08:55,960
of alcohol and you know the effect that it has in the mind will wear off and one can develop

88
00:08:55,960 --> 00:09:01,280
one's mind again and get back on track and so on and as long as one stays in moderation

89
00:09:01,280 --> 00:09:08,640
one can theoretically get past it, the point is that as I said even one glass of alcohol

90
00:09:08,640 --> 00:09:15,640
is a bad thing, it affects your client, it's no good, it's poison, if you take a lot

91
00:09:15,640 --> 00:09:22,520
of it it's going to, it will have a great effect on your life, on your mind, on your environment

92
00:09:22,520 --> 00:09:31,120
so the reason, our reason for taking the rule strictly and why I would definitely 100% recommend

93
00:09:31,120 --> 00:09:38,560
that you keep the rule against taking drugs in alcohol 100% and not engage in it even

94
00:09:38,560 --> 00:09:47,360
in moderation is because of the precedent that it sets both for yourself and for others.

95
00:09:47,360 --> 00:09:51,400
First the fact that you're drinking will mean that you'll be surrounded by other people

96
00:09:51,400 --> 00:09:58,680
who are drinking, you're surrounded by people who and you yourself also have this idea

97
00:09:58,680 --> 00:10:04,680
that there's nothing wrong with this or have the acceptance of the taking alcohol, your

98
00:10:04,680 --> 00:10:11,840
environment and the places that you hang out and so on are all going to reflect that.

99
00:10:11,840 --> 00:10:18,160
The acceptance that you give to the alcohol that you give to taking it even in moderation

100
00:10:18,160 --> 00:10:30,080
is a lifestyle choice and is a way of life that you choose and that you take on and so what

101
00:10:30,080 --> 00:10:36,120
that does to your own mind is creates this acceptance of things that are unwholesome.

102
00:10:36,120 --> 00:10:42,200
I mean the story that they give is if you give a bowl of milk and a bowl of alcohol to

103
00:10:42,200 --> 00:10:45,600
a dog, what's the dog going to do?

104
00:10:45,600 --> 00:10:50,200
There's no dog that I know of, well you can train your dog apparently but an ordinary

105
00:10:50,200 --> 00:10:55,120
dog will not hesitate and will go straight for the milk and ignore the alcohol and they

106
00:10:55,120 --> 00:11:01,680
wouldn't touch it unless you can train your dog to drink alcohol and I don't know

107
00:11:01,680 --> 00:11:10,000
or beer anyway but you take just the alcohol from it and they know that it's poison.

108
00:11:10,000 --> 00:11:14,880
The fact that we're taking it shows that we have some problem in our mind and the fact

109
00:11:14,880 --> 00:11:20,000
that we have social structures set up that incorporate alcohol into them shows that

110
00:11:20,000 --> 00:11:24,640
there's a problem with their social structures and so the fact that we engage in it in

111
00:11:24,640 --> 00:11:30,640
moderation, so we say, I mean there really is no moderation with something that is poisonous

112
00:11:30,640 --> 00:11:33,640
with something that dulls your mind.

113
00:11:33,640 --> 00:11:38,840
To the extent that alcohol does, I mean you could make an argument for watching television

114
00:11:38,840 --> 00:11:41,360
and watching movies and so on.

115
00:11:41,360 --> 00:11:45,600
One could say also intoxicate the mind but the level is totally different.

116
00:11:45,600 --> 00:11:52,360
There's a direct intoxication effect from alcohol and the lack of ability to see things

117
00:11:52,360 --> 00:11:55,440
clearly that comes from taking alcohol.

118
00:11:55,440 --> 00:12:04,400
All you can do without problems when your drunk is asleep is to lie down and shut your

119
00:12:04,400 --> 00:12:12,000
mind off because the mind doesn't work, the brain isn't functioning properly, if you use the brain in this state,

120
00:12:12,000 --> 00:12:17,560
you're just going to, you're not going to have any ability to discern what is good and what is bad

121
00:12:17,560 --> 00:12:21,640
because your faculties are mixed up.

122
00:12:21,640 --> 00:12:29,280
So by creating this standard for ourselves that know we are not going to engage in something

123
00:12:29,280 --> 00:12:37,240
which the only effect of that substance is to intoxicate the mind, then we deny this lifestyle,

124
00:12:37,240 --> 00:12:44,080
we deny this sort of social structure and we change our lifestyles, what this does is it changes

125
00:12:44,080 --> 00:12:51,640
your environment, it changes your friendships and it sets you on a lifestyle that is conducive

126
00:12:51,640 --> 00:12:57,520
towards clarity and understanding and freedom from suffering and peace and happiness.

127
00:12:57,520 --> 00:13:03,960
And so for this is why, for people who are interested in practicing meditation, this precept

128
00:13:03,960 --> 00:13:11,600
and all five of the precepts are really a basic, basic requirement, something that by all

129
00:13:11,600 --> 00:13:18,440
means have to be kept in full for someone who is serious about taking on this sort of a lifestyle

130
00:13:18,440 --> 00:13:21,640
because they will have a direct impact on your life.

131
00:13:21,640 --> 00:13:28,960
If you break these five roots, it's going to lesser more, it's going to have an effect

132
00:13:28,960 --> 00:13:31,680
on your life and change who you are.

133
00:13:31,680 --> 00:13:38,120
So because of the effect that alcohol has on you, yes, we abstain from it 100%, we take

134
00:13:38,120 --> 00:13:41,000
this as a rule.

135
00:13:41,000 --> 00:13:47,360
It's not that you can't do it and then come back and meditate but you're doing the opposite,

136
00:13:47,360 --> 00:13:50,880
you're doing two different things that are going in the opposite direction.

137
00:13:50,880 --> 00:13:56,640
So as a rule, we take this on ourselves because we're trying to create an environment,

138
00:13:56,640 --> 00:14:03,560
this is a social structure, a way of life for ourselves that is conducive towards meditation

139
00:14:03,560 --> 00:14:11,000
and even a little bit of alcohol is going to set you in a situation or in situations where

140
00:14:11,000 --> 00:14:16,720
you're surrounded by such people as are unmindful and uninterested in meditation and that's

141
00:14:16,720 --> 00:14:22,440
a real problem even if it doesn't get you stinking drunk, the effect that it has on your

142
00:14:22,440 --> 00:14:31,200
mind and the effect it has on your surroundings is going to be a great hindrance to your

143
00:14:31,200 --> 00:14:33,920
meditation practice and progress.

144
00:14:33,920 --> 00:14:34,920
So I hope this helps.

145
00:14:34,920 --> 00:14:38,360
I'm sure there's people out there who are not going to agree with this and are going

146
00:14:38,360 --> 00:14:44,040
to continue to drink alcohol and well, I'm not the judge.

147
00:14:44,040 --> 00:14:48,760
I had an old friend who said, no one can escape their karma and he would always say this.

148
00:14:48,760 --> 00:14:52,120
Whenever there was an argument and people said, this is right, that's right, he said,

149
00:14:52,120 --> 00:14:55,840
well no one can escape their karma and if you do a good thing you'll get a good result,

150
00:14:55,840 --> 00:14:58,040
if you do a bad thing you'll get a bad result.

151
00:14:58,040 --> 00:15:03,120
So we'll see what the result is and you can see for yourself what the result is.

152
00:15:03,120 --> 00:15:07,000
You don't have to believe me if it's a good result, keep doing it.

153
00:15:07,000 --> 00:15:16,240
So hope that is helped and all the best, thanks for tuning in.

