1
00:00:00,000 --> 00:00:01,000
Go ahead.

2
00:00:01,000 --> 00:00:03,000
Nelson asks,

3
00:00:03,000 --> 00:00:06,000
Bante, I've been practicing for about three months,

4
00:00:06,000 --> 00:00:10,000
according to the method in Buddha Dasas on that panasati,

5
00:00:10,000 --> 00:00:13,000
20 minutes in the morning, 20 minutes in the evening.

6
00:00:13,000 --> 00:00:15,000
A few days ago, though, while meditating,

7
00:00:15,000 --> 00:00:17,000
I noticed my heartbeat.

8
00:00:17,000 --> 00:00:21,000
I stopped counting my breasts and began to breathe in and out

9
00:00:21,000 --> 00:00:22,000
to counting my heartbeats.

10
00:00:22,000 --> 00:00:25,000
I have been doing this sense and seemed to experience a deeper

11
00:00:25,000 --> 00:00:28,000
and more stable concentration.

12
00:00:28,000 --> 00:00:32,000
Is this useful as a method, or do you think I am putting myself

13
00:00:32,000 --> 00:00:36,000
in a trance?

14
00:00:36,000 --> 00:00:39,000
Well, trances are useful methods, though.

15
00:00:39,000 --> 00:00:46,000
I think, yeah, most likely, you could call it a trance.

16
00:00:46,000 --> 00:00:48,000
I mean, it's really just a term.

17
00:00:48,000 --> 00:00:50,000
No, you call it a trance.

18
00:00:50,000 --> 00:00:52,000
It's a type of meditation.

19
00:00:52,000 --> 00:00:54,000
I like this kind of idea of the word,

20
00:00:54,000 --> 00:00:56,000
this kind of definition of the word,

21
00:00:56,000 --> 00:00:58,000
jana, jana means meditation.

22
00:00:58,000 --> 00:01:02,000
And I think that's great because it gives us a very open mind

23
00:01:02,000 --> 00:01:05,000
and allows us to say, well, you're practicing meditation.

24
00:01:05,000 --> 00:01:07,000
And from the sounds of it, it's good meditation.

25
00:01:07,000 --> 00:01:08,000
How do we know that?

26
00:01:08,000 --> 00:01:11,000
Because you're gaining a good concentration.

27
00:01:11,000 --> 00:01:14,000
Your mind is probably free from liking, free from disliking,

28
00:01:14,000 --> 00:01:18,000
free from distract, drowsiness, free from distraction,

29
00:01:18,000 --> 00:01:21,000
and free from doubt at the time that you're in it.

30
00:01:21,000 --> 00:01:22,000
So for sure, it's a good method.

31
00:01:22,000 --> 00:01:25,000
The question is, what's it good for?

32
00:01:25,000 --> 00:01:28,000
And that's a little more difficult to answer.

33
00:01:28,000 --> 00:01:34,000
These sort of trance or even states of calm

34
00:01:34,000 --> 00:01:38,000
are good on different levels.

35
00:01:38,000 --> 00:01:40,000
If you're just staying in them,

36
00:01:40,000 --> 00:01:42,000
then they're only good for themselves.

37
00:01:42,000 --> 00:01:44,000
And they're good as a kind of an escape,

38
00:01:44,000 --> 00:01:47,000
something that you can go to when they're going to get stuff.

39
00:01:47,000 --> 00:01:50,000
You have some way to take a retreat,

40
00:01:50,000 --> 00:01:54,000
as they say, or take a fine escape.

41
00:01:54,000 --> 00:01:57,000
They can also be good for gaining special knowledges

42
00:01:57,000 --> 00:01:58,000
and magical powers.

43
00:01:58,000 --> 00:02:02,000
If you've ever really developed high states of concentration,

44
00:02:02,000 --> 00:02:07,000
you can see how your mind is able to expand

45
00:02:07,000 --> 00:02:14,000
and travel in different dimensions.

46
00:02:14,000 --> 00:02:16,000
You might say, I don't know what the right word is,

47
00:02:16,000 --> 00:02:19,000
but the mind is able to enter into states of reality

48
00:02:19,000 --> 00:02:23,000
that are inaccessible, otherwise.

49
00:02:23,000 --> 00:02:25,000
But because you're asking me,

50
00:02:25,000 --> 00:02:30,000
and from a hardcore Buddhist point of view,

51
00:02:30,000 --> 00:02:35,000
they're only useful as an object of insight meditation.

52
00:02:35,000 --> 00:02:38,000
They're useful because your mind is very clear at that moment.

53
00:02:38,000 --> 00:02:40,000
So if you start taking that state

54
00:02:40,000 --> 00:02:43,000
as an object of insight meditation,

55
00:02:43,000 --> 00:02:47,000
for example, as we said to you yourself, calm,

56
00:02:47,000 --> 00:02:49,000
reflecting on it just as a state of calm

57
00:02:49,000 --> 00:02:52,000
that has arisen, that arises and ceases

58
00:02:52,000 --> 00:02:55,000
and being aware of the mind that is aware of it,

59
00:02:55,000 --> 00:02:58,000
that also arises and ceases being aware of the impermanence of it,

60
00:02:58,000 --> 00:03:00,000
which you're probably not able to see

61
00:03:00,000 --> 00:03:06,000
if you're focusing on it as in terms of cultivating it.

62
00:03:06,000 --> 00:03:08,000
Focusing on the object of it,

63
00:03:08,000 --> 00:03:10,000
which is the breath or the heartbeat or so on,

64
00:03:10,000 --> 00:03:12,000
and focusing on that as impermanence,

65
00:03:12,000 --> 00:03:13,000
suffering and non-self.

66
00:03:13,000 --> 00:03:17,000
Seeing the letting go of your attachment to these things,

67
00:03:17,000 --> 00:03:22,000
the underlying detail, the views

68
00:03:22,000 --> 00:03:26,000
that it is self, the conceit that it is good,

69
00:03:26,000 --> 00:03:32,000
and the done that liking or disliking it,

70
00:03:32,000 --> 00:03:34,000
that it is your possession.

71
00:03:34,000 --> 00:03:37,000
These three things can't be cut simply

72
00:03:37,000 --> 00:03:40,000
by the development of concentration.

73
00:03:40,000 --> 00:03:42,000
So with the development of insight,

74
00:03:42,000 --> 00:03:43,000
you're able to overcome this,

75
00:03:43,000 --> 00:03:45,000
get rid of your views and your ideas

76
00:03:45,000 --> 00:03:48,000
about self and about control and so on.

77
00:03:48,000 --> 00:03:50,000
You're able to give up your conceit

78
00:03:50,000 --> 00:03:52,000
in terms of thinking yourself better because of it,

79
00:03:52,000 --> 00:03:55,000
or worse, outside of it,

80
00:03:55,000 --> 00:03:59,000
or conceit in terms of it being me and mine and so on,

81
00:03:59,000 --> 00:04:03,000
my attainment, and done the high your attachment to it,

82
00:04:03,000 --> 00:04:07,000
in terms of it being good, in terms of it being desirable.

83
00:04:07,000 --> 00:04:11,000
And so once you can do that about any one thing in the universe

84
00:04:11,000 --> 00:04:14,000
and see it clearly, see clearly that,

85
00:04:14,000 --> 00:04:17,000
this is something arises, it therefore has to cease.

86
00:04:17,000 --> 00:04:19,000
You can't see some would they have done,

87
00:04:19,000 --> 00:04:22,000
but you wrote it, everything that arises has to cease.

88
00:04:22,000 --> 00:04:25,000
Then by extrapolation, you know that about the whole world.

89
00:04:25,000 --> 00:04:30,000
The mind instantly is clear that that is the nature of reality.

90
00:04:30,000 --> 00:04:33,000
And when you get to that state,

91
00:04:33,000 --> 00:04:36,000
very briefly, this is a very brief explanation

92
00:04:36,000 --> 00:04:37,000
of the meditation practice.

93
00:04:37,000 --> 00:04:40,000
At that moment, the mind enters into the state of cessation.

94
00:04:40,000 --> 00:04:42,000
It leaves behind the world.

95
00:04:42,000 --> 00:04:45,000
Therefore, quite useful,

96
00:04:45,000 --> 00:04:48,000
in terms of allowing you to enter into the state of cessation

97
00:04:48,000 --> 00:04:51,000
if you take it as an objective insight meditation.

98
00:04:51,000 --> 00:04:55,000
I don't know, Paul, and you have anything to add.

99
00:04:55,000 --> 00:04:59,000
Yes, I don't have much to say about it,

100
00:04:59,000 --> 00:05:07,000
but I was told that to focus on the heartbeat

101
00:05:07,000 --> 00:05:15,000
is not beneficial because sometimes the heart can change its beat

102
00:05:15,000 --> 00:05:20,000
and that can be very, very disturbing for people

103
00:05:20,000 --> 00:05:28,000
and make them fear a lot

104
00:05:28,000 --> 00:05:31,000
and have more fears coming up

105
00:05:31,000 --> 00:05:36,000
and then lose all the calm and get very disturbed.

106
00:05:36,000 --> 00:05:38,000
I've heard that as well,

107
00:05:38,000 --> 00:05:40,000
but I don't know that I buy it.

108
00:05:40,000 --> 00:05:42,000
They say the same thing, some centers say the same thing

109
00:05:42,000 --> 00:05:44,000
about the headaches,

110
00:05:44,000 --> 00:05:47,000
that it will get worse if you focus on it.

111
00:05:47,000 --> 00:05:50,000
That sounds a little bit soft-core to me.

112
00:05:50,000 --> 00:05:52,000
So let me ask you.

113
00:05:52,000 --> 00:05:55,000
This is what you've heard from people.

114
00:05:55,000 --> 00:05:58,000
Would you not, as an experienced meditation,

115
00:05:58,000 --> 00:06:01,000
say that that's actually a good thing

116
00:06:01,000 --> 00:06:04,000
because it helps you to examine your fear

117
00:06:04,000 --> 00:06:06,000
and your attachment.

118
00:06:06,000 --> 00:06:07,000
I've heard that as well.

119
00:06:07,000 --> 00:06:09,000
That was one of the first things I went through in my mind,

120
00:06:09,000 --> 00:06:14,000
but I think one point you might want to consider

121
00:06:14,000 --> 00:06:16,000
is if the heartbeat changes,

122
00:06:16,000 --> 00:06:19,000
it could theoretically cause a heart attack.

123
00:06:19,000 --> 00:06:24,000
So it is something that you want to be a little bit soft-core

124
00:06:24,000 --> 00:06:27,000
about in the power of the mind.

125
00:06:27,000 --> 00:06:29,000
Sometimes the heart,

126
00:06:29,000 --> 00:06:34,000
just some people have in regular heartbeat

127
00:06:34,000 --> 00:06:37,000
or due to, I don't know what,

128
00:06:37,000 --> 00:06:40,000
it changes sometimes.

129
00:06:40,000 --> 00:06:43,000
Some heartbeat is missing.

130
00:06:43,000 --> 00:06:49,000
So when these people start to meditate without guidance

131
00:06:49,000 --> 00:06:53,000
and without knowing that heartbeat can be irregular

132
00:06:53,000 --> 00:06:58,000
or then it can be really fearful for them.

133
00:06:58,000 --> 00:07:00,000
I've raised it indirectly a very good point

134
00:07:00,000 --> 00:07:04,000
that the real problem is not which technique you're doing.

135
00:07:04,000 --> 00:07:05,000
The problem is not having a teacher

136
00:07:05,000 --> 00:07:07,000
because it's not just the heartbeat.

137
00:07:07,000 --> 00:07:10,000
Everything you do, the chances of you giving rise

138
00:07:10,000 --> 00:07:13,000
to great states of fear and worry

139
00:07:13,000 --> 00:07:16,000
and eventually just giving up the practice

140
00:07:16,000 --> 00:07:18,000
because you don't know what to do next

141
00:07:18,000 --> 00:07:21,000
and you feel like you hit an impasse something

142
00:07:21,000 --> 00:07:23,000
that is impossible to meditate.

143
00:07:23,000 --> 00:07:26,000
The smallest things that a meditation teacher would just say,

144
00:07:26,000 --> 00:07:29,000
it's that and change your condition

145
00:07:29,000 --> 00:07:31,000
and set you on the next.

146
00:07:31,000 --> 00:07:33,000
Just as nothing, brush it aside

147
00:07:33,000 --> 00:07:36,000
will stop a solitary meditator from continuing

148
00:07:36,000 --> 00:07:38,000
because we can't see our own eyes

149
00:07:38,000 --> 00:07:40,000
without a mirror.

150
00:07:40,000 --> 00:07:43,000
We don't have the wisdom that allows us to see our own mind

151
00:07:43,000 --> 00:07:46,000
to see what tricks we're playing on ourselves.

152
00:07:46,000 --> 00:07:50,000
So if you stop practicing Ajahn Buddha Das' teachings

153
00:07:50,000 --> 00:07:54,000
then you probably have a problem

154
00:07:54,000 --> 00:07:57,000
with following his books and his guidance

155
00:07:57,000 --> 00:07:59,000
or at least a small problem.

156
00:07:59,000 --> 00:08:02,000
And considering that you've developed a technique

157
00:08:02,000 --> 00:08:06,000
that you have no teacher in the short term

158
00:08:06,000 --> 00:08:10,000
but you have to consider the power of the mind

159
00:08:10,000 --> 00:08:12,000
and the power for it to lead us astray.

160
00:08:12,000 --> 00:08:16,000
This is one of the great things I was trying to bring out about.

161
00:08:16,000 --> 00:08:21,000
So made a story is that the importance of having a teacher

162
00:08:21,000 --> 00:08:25,000
because you can see how you trick yourself into many different things

163
00:08:25,000 --> 00:08:29,000
and you realize that the mind is, as you said,

164
00:08:29,000 --> 00:08:31,000
you can't see your own eyes

165
00:08:31,000 --> 00:08:55,000
and you can't see your own mind.

