1
00:00:00,000 --> 00:00:02,000
I'm not impressed.

2
00:00:05,600 --> 00:00:08,000
Okay, now we're really broadcasting live.

3
00:00:10,400 --> 00:00:12,960
November 7th, 2015.

4
00:00:14,080 --> 00:00:17,200
Now let's make sure we've got everything set up.

5
00:00:17,200 --> 00:00:29,200
Okay, and everything should be working properly.

6
00:00:32,320 --> 00:00:34,640
So

7
00:00:38,240 --> 00:00:40,720
we have a Yelp listing.

8
00:00:40,720 --> 00:00:43,200
We have a thanks to Anna.

9
00:00:43,200 --> 00:00:46,800
She created a listing for the meditation center on Yelp.

10
00:00:46,800 --> 00:00:52,400
So if you put in meditation in Hamilton, you do pop up.

11
00:00:54,000 --> 00:00:56,320
So it's a simple listing so far.

12
00:00:57,040 --> 00:01:02,560
You can probably put some pictures up there and make it a little fancier, but it's there.

13
00:01:04,400 --> 00:01:11,600
And shortly we should be getting confirmation to get everything tied up for Google as well.

14
00:01:11,600 --> 00:01:17,600
So that'll be a great thing when when you type it in in the driving directions, come up and all that kind of thing.

15
00:01:19,120 --> 00:01:20,960
Just a little more verification needed.

16
00:01:24,320 --> 00:01:26,480
Just letting people know about the meditation center.

17
00:01:28,240 --> 00:01:29,760
Awesome. Yeah.

18
00:01:31,200 --> 00:01:36,480
And tomorrow we have our volunteer meetings so we can maybe discuss that a little bit tomorrow.

19
00:01:36,480 --> 00:01:40,800
That's at one. Yes.

20
00:01:47,680 --> 00:01:49,280
Tonight we have an interesting verse.

21
00:01:51,200 --> 00:01:54,400
Very deep meditative one. I encourage everyone to read this.

22
00:01:55,280 --> 00:02:01,200
Not sure how much I can talk about it, but there is to be said, but it certainly was reading.

23
00:02:01,200 --> 00:02:15,200
It's the dark where you have a thorn in your side and then to get it out, you stick another thorn in your side.

24
00:02:20,000 --> 00:02:20,800
Doesn't really help.

25
00:02:20,800 --> 00:02:32,640
But it's interesting how he actually talks about when a person is inundated by painful feelings then no

26
00:02:34,640 --> 00:02:40,560
chase after pleasant feelings. Why? Because they don't know any better. They can't think of any

27
00:02:40,560 --> 00:02:50,880
better way to get rid of unpleasant feelings than to douse them, douse the fire with gasoline really

28
00:02:53,520 --> 00:03:00,560
to smother it with sensual desire and then lust arise. I mean, you read this. It's a very clear

29
00:03:01,440 --> 00:03:10,080
exposition on the nature of attachment and so it's a clear exposition of the problem that

30
00:03:10,080 --> 00:03:21,600
we face. The ordinary person knows no other escape from painful feelings.

31
00:03:21,600 --> 00:03:37,680
Now he is so big away Pajana, he is too far to jump, too, too shallow and yet there are way

32
00:03:37,680 --> 00:03:46,080
than I ever think. Come and tell me, you do cry away than I ever think I'll miss you.

33
00:03:46,080 --> 00:04:02,080
So I'm in the city, not here, but I'm not here. Other than Kamasoka, right now I read that

34
00:04:02,080 --> 00:04:17,280
no confused, there's reading the wrong way. It's hard to read because it's so wide.

35
00:04:19,840 --> 00:04:25,680
Now he's so big away, Pajana, he is too far apart to jump, an ordinary world thing who is

36
00:04:25,680 --> 00:04:37,360
uninstracted, doesn't know any other way than Kamasoka, the pleasure of sensuality

37
00:04:38,720 --> 00:04:50,880
for the dukai away, the nayani ceremony for the escape from suffering feelings.

38
00:04:50,880 --> 00:05:10,080
But they don't know any other way. And when they are abinanda, when they are rejoicing or indulging

39
00:05:10,080 --> 00:05:23,760
or engaging and rejoicing in central pleasure, the dukai away, the nayana, the nayana, the seyo,

40
00:05:23,760 --> 00:05:32,800
so I don't see it. So I knew say I was kind of like an obsession.

41
00:05:32,800 --> 00:05:41,920
You become obsessed with the obsession of Braga, because I'm a city, but I know it's the

42
00:05:41,920 --> 00:05:54,480
lie dormant. I know Satan is to lie down or to lie with the lie inside. It's an interesting

43
00:05:54,480 --> 00:06:05,840
and you say, how do you announce Satan? You know, become obsessed. I mean, the idea of it,

44
00:06:05,840 --> 00:06:11,200
and you say it's something deep, so obsession is kind of like a deep attachment,

45
00:06:11,200 --> 00:06:27,200
because deeply involved in it. So do we have questions today? We do.

46
00:06:28,720 --> 00:06:33,520
Mante, a homeless person asked for money so that he could get some food. I gave it to him

47
00:06:33,520 --> 00:06:38,960
and then drove home. On the drive home, I was very mindful the whole time. He took much less

48
00:06:38,960 --> 00:06:43,680
effort than usual than I thought that if I constantly gave a portion of the money I earned from

49
00:06:43,680 --> 00:06:48,720
my job to a charity, it might help me be more mindful at work as it's helping other people.

50
00:06:49,440 --> 00:06:55,040
Will this work? Is this why the Buddha recommended householders to donate a portion of

51
00:06:55,040 --> 00:06:59,680
a portion of their money to charity? I believe I heard you say this before. Thanks,

52
00:06:59,680 --> 00:07:10,320
Bante. Sorry for the unusually large question. I hope you can answer me.

53
00:07:10,320 --> 00:07:14,560
Yeah. Give me giving it a little matter. What?

54
00:07:17,600 --> 00:07:22,080
That's about it. Definitely if you can give it a give.

55
00:07:22,080 --> 00:07:29,120
It's something that sports your mind. The Buddha said,

56
00:07:29,120 --> 00:07:58,960
if a person does good deeds, they should do them again and again.

57
00:07:58,960 --> 00:08:08,160
Goodness is happiness. The building of goodness, the accumulating of goodness is happiness.

58
00:08:08,160 --> 00:08:24,800
It's a mābīkava yetāpungyanana. Don't be afraid because of pṛṣṇyā of goodness. Sūkhāsītān bīkava ya-dīwāgādīwāgādītān bṛṇyādī. Good deeds are another word for happiness.

59
00:08:27,360 --> 00:08:31,600
For happiness is another word for this. That is goodness.

60
00:08:31,600 --> 00:08:42,960
No, that is to say goodness is good needs.

61
00:08:42,960 --> 00:08:46,480
Should you be concerned with what people do, what the money?

62
00:08:46,480 --> 00:08:51,480
Because like sometimes, you know, people are asking for change in things and you think,

63
00:08:51,480 --> 00:08:56,200
well, if I give the money, you know, they may just use it to buy drugs or whatever.

64
00:08:56,200 --> 00:09:07,040
It can be, you have to ask yourself, how important it is to you, you know, like if it's something

65
00:09:07,040 --> 00:09:15,640
that you, I mean, it's not like I would rest my whole religious life on or spiritual development

66
00:09:15,640 --> 00:09:22,120
on charity to the poor or charity to a specific individual.

67
00:09:22,120 --> 00:09:31,440
You know, it's really not all that relevant, it's relevant to sort of person that you're giving

68
00:09:31,440 --> 00:09:32,440
it to.

69
00:09:32,440 --> 00:09:42,040
So if there's a homeless person who is really mean and nasty or really immoral, and again,

70
00:09:42,040 --> 00:09:51,200
giving money, as I said, a fairly low form of charity, giving money is, I mean, for that

71
00:09:51,200 --> 00:09:54,560
reason as well, because, you know, they have to do something with it.

72
00:09:54,560 --> 00:09:58,720
You're not actually doing them any favor directly, you're doing them favor, but you're

73
00:09:58,720 --> 00:10:03,360
not actually giving them anything useful, you're giving them something that they can

74
00:10:03,360 --> 00:10:06,480
use to get useful things or to get harmful things.

75
00:10:06,480 --> 00:10:12,240
So money is kind of, it's power, you're giving them power as opposed to something that

76
00:10:12,240 --> 00:10:22,800
will help them power doesn't always help, so money is not the best way to give.

77
00:10:22,800 --> 00:10:28,440
So it could be a concern then if you, you know, like when you're in the city and people

78
00:10:28,440 --> 00:10:32,800
are asking for change and things, and if you think they're potentially going to use it

79
00:10:32,800 --> 00:10:41,160
for drugs or alcohol, would that be a reason not to give a little, sorry, sorry that

80
00:10:41,160 --> 00:10:42,160
you finished.

81
00:10:42,160 --> 00:10:43,160
Go ahead.

82
00:10:43,160 --> 00:10:44,160
No, no, that was it.

83
00:10:44,160 --> 00:10:52,200
But you were going to say would that be, well, you know, it seems like a conflict, you

84
00:10:52,200 --> 00:10:57,000
know, because you can tell that that kind of behavior would harm people, of course you

85
00:10:57,000 --> 00:11:02,640
don't know what someone's going to do, but that's always the concern that, you know,

86
00:11:02,640 --> 00:11:08,760
when you get people money, that they're going to use it to just further harm themselves.

87
00:11:08,760 --> 00:11:15,640
But that's not really your intent when you're giving, it's not, I wouldn't worry too much

88
00:11:15,640 --> 00:11:20,200
about that, but on the other hand, as I said, I wouldn't really put too much into it.

89
00:11:20,200 --> 00:11:26,800
You consider the, the more pure part of the goodness is the giving up because they asked

90
00:11:26,800 --> 00:11:30,720
you, not because you want them to have something, but because they asked you if I ask

91
00:11:30,720 --> 00:11:36,800
you for something, you giving it to me is a relinquishing of the stinginess, right, of saying

92
00:11:36,800 --> 00:11:43,080
no to someone, so if someone asks you for something, unless it's immoral to give it to

93
00:11:43,080 --> 00:11:47,680
them, you give it to them not thinking, oh, this will be to their benefit, you say, I give,

94
00:11:47,680 --> 00:11:56,680
I give as a giving up as a not clinging, like, like we sent her, we sent her, I gave things

95
00:11:56,680 --> 00:12:02,960
away that you probably would have, well, that's not even fair to say, but we sent her

96
00:12:02,960 --> 00:12:07,000
to give up everything when people asked him to, he was just waiting for people to ask

97
00:12:07,000 --> 00:12:12,320
him, people asked him for things that they shouldn't have asked him for, and he gave

98
00:12:12,320 --> 00:12:18,800
because he had made a determination that if anyone asks him for something, he will give.

99
00:12:18,800 --> 00:12:26,040
So it's two aspects of giving, giving as a intentional good deed and giving when someone

100
00:12:26,040 --> 00:12:32,240
asks you, two different things, like if you go out of your way to give money to the

101
00:12:32,240 --> 00:12:39,840
homeless, then you can be, you can be rightly questioned as to, hey, do you know where your

102
00:12:39,840 --> 00:12:40,840
money is going?

103
00:12:40,840 --> 00:12:45,000
You know, that's a pretty dubious sort of spiritual practice because you don't really

104
00:12:45,000 --> 00:12:48,640
know what's going to happen to it, but if you're walking down the street and someone

105
00:12:48,640 --> 00:12:52,280
asks you for change, I don't think anyone's going to say, hey, don't give, well, people

106
00:12:52,280 --> 00:12:53,280
will.

107
00:12:53,280 --> 00:12:56,880
I don't think they rightly can say, hey, what are you doing giving him money?

108
00:12:56,880 --> 00:13:07,080
Say, well, he asked me, so, like one of my students had this was being, was being asked

109
00:13:07,080 --> 00:13:14,200
for money again and again for this and that from a person overseas, a person in another

110
00:13:14,200 --> 00:13:15,200
country.

111
00:13:15,200 --> 00:13:19,120
So they had no way of knowing whether this person was saying was true and it seemed kind

112
00:13:19,120 --> 00:13:25,360
of shady and I'd heard similar things, I was similar nature.

113
00:13:25,360 --> 00:13:30,440
So I said to him the same thing, I said, you know, give a little and it's a different kind

114
00:13:30,440 --> 00:13:31,440
of practice.

115
00:13:31,440 --> 00:13:32,440
It's about giving up.

116
00:13:32,440 --> 00:13:36,480
It's not the kind of thing where you'd want to give huge sums of money, pour your

117
00:13:36,480 --> 00:13:41,480
life savings into it or give them everything they want, but you give a little and that

118
00:13:41,480 --> 00:13:44,560
kind of is a way of giving a badge and tongue would do this.

119
00:13:44,560 --> 00:13:48,400
People would come and ask him for money and he would just give them a little.

120
00:13:48,400 --> 00:13:53,320
There were his relatives, this really funny couple, they claimed to be relatives of him.

121
00:13:53,320 --> 00:13:57,080
I guess they were relatives of him but they milked it for all it was worth so they would

122
00:13:57,080 --> 00:14:00,520
come back again and again asking for money.

123
00:14:00,520 --> 00:14:05,200
And finally, his secretary locked the doors, I was standing outside because I was waiting

124
00:14:05,200 --> 00:14:12,200
to come in for the listener in for reporting and they locked the doors and we had to

125
00:14:12,200 --> 00:14:18,680
stand outside for like half an hour while he yelled at these two people.

126
00:14:18,680 --> 00:14:27,560
He was a real bulldog.

127
00:14:27,560 --> 00:14:29,280
Thank you, buddy.

128
00:14:29,280 --> 00:14:33,400
Are comp feelings considered neutral feelings?

129
00:14:33,400 --> 00:14:35,160
Yes.

130
00:14:35,160 --> 00:14:44,760
That's it, I'm questions.

131
00:14:44,760 --> 00:14:46,800
Well, good.

132
00:14:46,800 --> 00:14:47,800
Yeah.

133
00:14:47,800 --> 00:14:52,320
That means fewer questions with lots of people, a lot of fewer people.

134
00:14:52,320 --> 00:14:56,640
No, lots of people meditating tonight.

135
00:14:56,640 --> 00:14:57,640
Yeah.

136
00:14:57,640 --> 00:15:02,160
Fewer people logged in.

137
00:15:02,160 --> 00:15:05,360
That's fine.

138
00:15:05,360 --> 00:15:09,800
Are you looking on the website?

139
00:15:09,800 --> 00:15:14,600
Yeah, we got the list of green and orange people.

140
00:15:14,600 --> 00:15:19,960
Yeah, I'm not sure that that list is always accurate.

141
00:15:19,960 --> 00:15:21,760
It seems like, I don't know.

142
00:15:21,760 --> 00:15:25,720
Well, it's only if they're looking at this page, if they're on YouTube, it won't show.

143
00:15:25,720 --> 00:15:28,680
If there's somewhere else, it's not even all the people on the list.

144
00:15:28,680 --> 00:15:34,040
It's just people who are actively have meditation.ceremungal.org open, but it should

145
00:15:34,040 --> 00:15:37,400
be everyone.

146
00:15:37,400 --> 00:15:39,880
Maybe it's not that good.

147
00:15:39,880 --> 00:15:41,880
Doesn't have Patrick, does it?

148
00:15:41,880 --> 00:15:44,880
Where did he go?

149
00:15:44,880 --> 00:15:50,680
Yeah, that's what I wonder, because sometimes it shows more people meditating than logged

150
00:15:50,680 --> 00:15:51,680
in there.

151
00:15:51,680 --> 00:15:52,680
So I don't know.

152
00:15:52,680 --> 00:15:56,240
Maybe people close out of the page or something.

153
00:15:56,240 --> 00:15:57,240
Could be maybe.

154
00:15:57,240 --> 00:16:03,600
Maybe it doesn't work as it should.

155
00:16:03,600 --> 00:16:09,320
I think finally, last night's, last night's, yes, last night's, Damapada was the video

156
00:16:09,320 --> 00:16:12,280
as well, well-made.

157
00:16:12,280 --> 00:16:20,000
Finally, I figured out the settings of this darn camera, how to get it set up to overexpose

158
00:16:20,000 --> 00:16:31,560
the background and yet maintain a good-looking picture for anyone's interested.

159
00:16:31,560 --> 00:16:38,520
I think the group here watches your Damapada videos no matter what.

160
00:16:38,520 --> 00:16:39,520
Good quality, bad quality.

161
00:16:39,520 --> 00:16:44,840
I mean, the talk is always wonderful, but sometimes the audio is off, but I think this group

162
00:16:44,840 --> 00:16:45,840
here.

163
00:16:45,840 --> 00:16:46,840
Oh, yeah, the audio last night was off.

164
00:16:46,840 --> 00:16:50,400
See, it's just not one thing, it's another.

165
00:16:50,400 --> 00:16:54,680
The audio is off, because there's this thing called, I think it's called a DC loop, which

166
00:16:54,680 --> 00:16:58,920
is weird, because, well, something like a some kind of loop.

167
00:16:58,920 --> 00:17:07,720
If you've got somehow two connections, then it builds up a, a sounds of you.

168
00:17:07,720 --> 00:17:11,800
Listen to last night's, Damapada, you can hear it in the background.

169
00:17:11,800 --> 00:17:12,800
Yes.

170
00:17:12,800 --> 00:17:16,800
I've had that before, and I had no idea what was going on, and it's because I had two things

171
00:17:16,800 --> 00:17:21,440
plugged in that were connected to the camera.

172
00:17:21,440 --> 00:17:25,600
Well, this group here, we, we like them no matter what, but it's great that they're coming

173
00:17:25,600 --> 00:17:35,880
out better for people who are newer to it, but anyway, so a good night then.

174
00:17:35,880 --> 00:17:37,560
Thank you, Robin, for your help.

175
00:17:37,560 --> 00:17:38,560
Thank you, Robin.

176
00:17:38,560 --> 00:17:39,560
Thank you.

177
00:17:39,560 --> 00:17:40,560
Good night.

178
00:17:40,560 --> 00:18:08,560
Good night.

