1
00:00:00,000 --> 00:00:05,160
Hello and welcome back to our study of the Dhamapada. Today we continue on with

2
00:00:05,160 --> 00:00:12,160
verses 104 and 105 which read as follows.

3
00:00:35,160 --> 00:00:46,160
Ta-da-ru-pasa-chan-dunu, which means, it's actually similar to the last one.

4
00:00:46,160 --> 00:00:55,160
Conquest of the self is better than the conquest of others, of other people.

5
00:00:55,160 --> 00:01:03,160
At the Dhampasa-pasa-santa-tara, you know, for one who has

6
00:01:03,160 --> 00:01:12,160
conquered themselves, and is constantly restrained.

7
00:01:12,160 --> 00:01:22,160
Sun yet that Jadino, one who fares, restrained, or controlled.

8
00:01:22,160 --> 00:01:36,160
In a sense of not giving rise to uncontrolled behavior, or activity that causes harm to others.

9
00:01:36,160 --> 00:01:45,160
Nae-wade-wona-gandam-bo, not an angel, nor a gandam-bo, which is also another type of angel, I guess.

10
00:01:45,160 --> 00:01:54,160
Nama-ro-saha-brah-muna-nam-mara, which is like the evil Satan, nor Brahma's gods.

11
00:01:54,160 --> 00:02:04,160
Jitanga-pajidanga-yra can turn their victory into defeat.

12
00:02:04,160 --> 00:02:08,160
Jitanga-pajitanga-pajita is defeat, I believe.

13
00:02:08,160 --> 00:02:17,160
Jitanga-pajidanga means turning the victory into defeat, they will not be able to.

14
00:02:17,160 --> 00:02:26,160
For such a person, they will not be able to turn the victory of such a person into defeat.

15
00:02:26,160 --> 00:02:34,160
So great verse shows the power of self-conquest.

16
00:02:34,160 --> 00:02:46,160
In both story, the story goes, there was a Brahmin who came to the Buddha, and asked him a question.

17
00:02:46,160 --> 00:02:56,160
I guess maybe he was a follower of the Buddha, or he understood how worthy the Buddha was, or how wise the Buddha was.

18
00:02:56,160 --> 00:03:08,160
And so he had this question, he wondered to himself whether the Buddha, whether Buddhas, whether those who are enlightened,

19
00:03:08,160 --> 00:03:16,160
know only what is a benefit, or do they also know what leads to detriment?

20
00:03:16,160 --> 00:03:23,160
Do they only know about gain or do they know about loss as well?

21
00:03:23,160 --> 00:03:32,160
Do they know atameva-jana-ti-udha-hu, anatame-pajisa, anatame-pajisa?

22
00:03:32,160 --> 00:03:41,160
Do they know only what is atame means, what is a benefit, or what is a value, or do they also know what is detriment?

23
00:03:41,160 --> 00:03:50,160
Anata, which is harmful, or leads one away from success, leads one away from what is useful, which is useless,

24
00:03:50,160 --> 00:03:54,160
or harmful, really.

25
00:03:54,160 --> 00:03:56,160
And so he went to ask the Buddha this.

26
00:03:56,160 --> 00:04:00,160
I guess he had some idea that the Buddha only taught people good things.

27
00:04:00,160 --> 00:04:06,160
He only taught, so everyone was praising how the Buddha's teachings would lead people to good things.

28
00:04:06,160 --> 00:04:11,160
And they were talking about how great the Buddha's teaching Buddha would teach you, do this to that.

29
00:04:11,160 --> 00:04:15,160
And so he hadn't heard the Buddha talk about, don't do this, don't do that.

30
00:04:15,160 --> 00:04:20,160
And so he went to the Buddha, and the Buddha gave him an interesting quote.

31
00:04:20,160 --> 00:04:25,160
And I was just having a terrible time with it, because the English is...

32
00:04:25,160 --> 00:04:27,160
It's incredible how sloppy they were with it.

33
00:04:27,160 --> 00:04:36,160
They translate fierceness, Chandikhan, as moonlight.

34
00:04:36,160 --> 00:04:41,160
Which is, well, I mean, it's understandable because Chanda would be the moon.

35
00:04:41,160 --> 00:04:45,160
But Chanda would die with the...

36
00:04:45,160 --> 00:04:49,160
I'll be a palatial, indeed, anyway.

37
00:04:49,160 --> 00:04:51,160
Doesn't mean moon at all.

38
00:04:51,160 --> 00:04:54,160
Unless there's another version that confuses the two.

39
00:04:54,160 --> 00:04:56,160
But it means ferocity.

40
00:04:56,160 --> 00:05:00,160
So the Buddha says it gives a list of six things.

41
00:05:00,160 --> 00:05:05,160
And I think I've got them all down.

42
00:05:05,160 --> 00:05:06,160
And he says...

43
00:05:06,160 --> 00:05:09,160
So he goes and asks the Buddha, do you know about lost as well?

44
00:05:09,160 --> 00:05:13,160
And the Buddha says, of course, and he tells him...

45
00:05:13,160 --> 00:05:16,160
He says...

46
00:05:16,160 --> 00:05:22,160
I know both that which is of benefit and that which is to detriment.

47
00:05:22,160 --> 00:05:24,160
And so he taught.

48
00:05:24,160 --> 00:05:28,160
He says, please then tell me about what is to detriment.

49
00:05:28,160 --> 00:05:31,160
And the Buddha says, well, then listen.

50
00:05:31,160 --> 00:05:35,160
And he gives this verse, which isn't the Dhamma Pandavas.

51
00:05:35,160 --> 00:05:36,160
But there's...

52
00:05:36,160 --> 00:05:40,160
In the case with these stories, there will be other verses that...

53
00:05:40,160 --> 00:05:42,160
Where they come from exactly.

54
00:05:42,160 --> 00:05:44,160
We don't know where they've been recorded.

55
00:05:44,160 --> 00:05:48,160
We don't know, but it's supposed to have been said by the Buddha as well.

56
00:05:48,160 --> 00:05:51,160
So he says, usura seyang, which means clear.

57
00:05:51,160 --> 00:05:52,160
That one's clear.

58
00:05:52,160 --> 00:05:54,160
It means sleeping beyond sunrise.

59
00:05:54,160 --> 00:05:56,160
So sleeping during the day.

60
00:05:56,160 --> 00:05:58,160
Well, it's time to get up and go to work.

61
00:05:58,160 --> 00:06:02,160
One instead stays in bed and doesn't do one's work.

62
00:06:02,160 --> 00:06:09,160
A la seyang, laziness, then one is just lazy and doesn't want to do good things.

63
00:06:09,160 --> 00:06:12,160
Tendi kang means, again, ferocity.

64
00:06:12,160 --> 00:06:17,160
So being mean or nasty or cruel, being...

65
00:06:17,160 --> 00:06:22,160
When people are overbearing and just constantly irritable.

66
00:06:22,160 --> 00:06:26,160
And the Diga Sondi young is an interesting one, but I think.

67
00:06:26,160 --> 00:06:29,160
Because Diga means long, but Sondi means...

68
00:06:29,160 --> 00:06:33,160
It means addiction to intoxication or addiction to drink.

69
00:06:33,160 --> 00:06:35,160
Usually refers to it.

70
00:06:35,160 --> 00:06:38,160
Maybe intoxication is better.

71
00:06:38,160 --> 00:06:42,160
So I think it says something to do with drinking and intoxicating drinks.

72
00:06:42,160 --> 00:06:46,160
And it may be a corruption, but I probably just don't know what it means.

73
00:06:46,160 --> 00:06:48,160
Anyway, but the English is terrible.

74
00:06:48,160 --> 00:06:50,160
It doesn't have any of that.

75
00:06:50,160 --> 00:06:51,160
What does it say?

76
00:06:51,160 --> 00:06:55,160
Long continued prosperity, which is ridiculous.

77
00:06:55,160 --> 00:07:01,160
Diga is long, but long continued prosperity is not a cause for ruin,

78
00:07:01,160 --> 00:07:03,160
except it can make you negligent, I suppose.

79
00:07:03,160 --> 00:07:06,160
But that's certainly not what's being said here.

80
00:07:06,160 --> 00:07:08,160
And then we've got...

81
00:07:08,160 --> 00:07:10,160
I think it's addiction to strong drink.

82
00:07:10,160 --> 00:07:13,160
Why? Because these come from the...

83
00:07:13,160 --> 00:07:16,160
Or these are echoed in the...

84
00:07:16,160 --> 00:07:25,160
And the Diga Nikaya Suta.

85
00:07:25,160 --> 00:07:29,160
The Siegelawada Suta.

86
00:07:29,160 --> 00:07:32,160
The discourse, the Siegelawada Suta.

87
00:07:32,160 --> 00:07:36,160
Siegelawada Suta talks about these various states that lead to loss.

88
00:07:36,160 --> 00:07:41,160
And addiction to drink is, of course, one of them.

89
00:07:41,160 --> 00:07:46,160
Ekasadhanangamanan.

90
00:07:46,160 --> 00:07:49,160
So I believe it means traveling alone.

91
00:07:49,160 --> 00:07:52,160
Which they just translate as going on long journeys,

92
00:07:52,160 --> 00:07:54,160
but going on journeys doesn't lead to loss.

93
00:07:54,160 --> 00:07:58,160
Going on long journeys alone can lead to problems.

94
00:07:58,160 --> 00:08:01,160
In the time of the Buddha, it certainly could because you could get robbed.

95
00:08:01,160 --> 00:08:04,160
It was a big deal if you traveled alone.

96
00:08:04,160 --> 00:08:08,160
It wasn't, it just wasn't a good idea.

97
00:08:08,160 --> 00:08:17,160
And finally, Bharadha Rupa Sivanam, which means chasing after other people's wives.

98
00:08:17,160 --> 00:08:19,160
Which was apparently a thing.

99
00:08:19,160 --> 00:08:20,160
Or is really a thing.

100
00:08:20,160 --> 00:08:23,160
People come in adultery all the time.

101
00:08:23,160 --> 00:08:31,160
Bad idea leads to loss, leads to disrespect, leads to loss of friendship, etc.

102
00:08:31,160 --> 00:08:34,160
So he taught these things and maybe he taught others.

103
00:08:34,160 --> 00:08:37,160
But that's the sort of things he taught.

104
00:08:37,160 --> 00:08:44,160
Certainly in the Siegelawadha Sivanam, he taught many other things that are to your detriment.

105
00:08:44,160 --> 00:08:50,160
And it's curious, you know, because he doesn't say gambling.

106
00:08:50,160 --> 00:08:55,160
And yet gambling is considered one of the things that leads to loss.

107
00:08:55,160 --> 00:09:00,160
I don't believe it's in here. If I've translated it correctly, it's not there.

108
00:09:00,160 --> 00:09:04,160
And in the Siegelawadha Sivanam, it's clearly there.

109
00:09:04,160 --> 00:09:12,160
And that's interesting because the Brahman is then impressed and it says very good.

110
00:09:12,160 --> 00:09:14,160
Very good. That's awesome. Well spoken.

111
00:09:14,160 --> 00:09:22,160
You're a really, really great teacher and you really know what is a benefit and what is a detriment.

112
00:09:22,160 --> 00:09:28,160
And then the Buddha asks him, so Brahman, how do you make your livelihood?

113
00:09:28,160 --> 00:09:34,160
And turns out the Brahman makes his livelihood through gambling.

114
00:09:34,160 --> 00:09:36,160
That's how he makes a living.

115
00:09:36,160 --> 00:09:47,160
I guess he must be sort of one of these people who are a con artist, right?

116
00:09:47,160 --> 00:09:49,160
Or something like that.

117
00:09:49,160 --> 00:09:53,160
To make, I guess, there are professional poker players nowadays.

118
00:09:53,160 --> 00:09:58,160
I think they're apparently the gamble online. They have people who tell me about that.

119
00:09:58,160 --> 00:10:04,160
How they do gambling online.

120
00:10:04,160 --> 00:10:17,160
And so it's interesting that the Buddha didn't include it, but that is common to be very careful not to attack a person directly.

121
00:10:17,160 --> 00:10:27,160
So if you had brought up gambling, there's a little bit, as a Buddhist, you're more curious than this Brahman probably realized.

122
00:10:27,160 --> 00:10:34,160
He doesn't bring it up, but yet he wants to make it clear that gambling is in there as well.

123
00:10:34,160 --> 00:10:38,160
Without actually saying all gambling is going to lead you to ruin.

124
00:10:38,160 --> 00:10:50,160
It's as though he purposefully left it out, or this story, part of the reason for telling this story is to,

125
00:10:50,160 --> 00:10:56,160
or part of the meaning behind the story is to show that he didn't attack him directly.

126
00:10:56,160 --> 00:10:59,160
It's quite curious.

127
00:10:59,160 --> 00:11:04,160
But can be a really important aspect of teaching, so anyway.

128
00:11:04,160 --> 00:11:17,160
Point is, he tells him, the Brahman says he makes his living by gambling, and this is the funny sort of inside joke that we can kind of smile,

129
00:11:17,160 --> 00:11:31,160
because we know it's kind of what's going through the Buddhist mind to some extent, that he knows that this Brahman is on a state of loss.

130
00:11:31,160 --> 00:11:35,160
He's heading in a bad way.

131
00:11:35,160 --> 00:11:43,160
And so then the Buddha asks him, so who wins when you gamble? Do you win, or does your opponents win?

132
00:11:43,160 --> 00:11:48,160
He says, oh, well, sometimes I win, sometimes my opponents win.

133
00:11:48,160 --> 00:11:56,160
And then the Buddha says, and Buddha lays down the law and says, Brahman, that's not a real victory.

134
00:11:56,160 --> 00:12:02,160
So as you can see, that's the nature of victory. Sometimes you win, sometimes you lose.

135
00:12:02,160 --> 00:12:05,160
Or we have this saying, you win some, you lose some.

136
00:12:05,160 --> 00:12:09,160
Because it's not a really, it means that on balance you come out with nothing, right?

137
00:12:09,160 --> 00:12:11,160
You never come out ahead.

138
00:12:11,160 --> 00:12:14,160
Or it's certainly not guaranteed that you're going to come out ahead.

139
00:12:14,160 --> 00:12:16,160
And there must be some other factors.

140
00:12:16,160 --> 00:12:21,160
Often people would say luck is all that allows you to come out ahead.

141
00:12:21,160 --> 00:12:25,160
And as we talked about, of course, it doesn't matter whether you come out ahead or not,

142
00:12:25,160 --> 00:12:29,160
when you come out ahead what happens while you begin to get enmity.

143
00:12:29,160 --> 00:12:32,160
Your opponents are upset because they've lost.

144
00:12:32,160 --> 00:12:35,160
When you win, someone has to lose.

145
00:12:35,160 --> 00:12:39,160
That kind of victory is not really, he says, a trifling.

146
00:12:39,160 --> 00:12:41,160
That's how the English translate it.

147
00:12:41,160 --> 00:12:42,160
You can't really trust the English.

148
00:12:42,160 --> 00:12:44,160
No, can we?

149
00:12:44,160 --> 00:12:47,160
Parajayo.

150
00:12:47,160 --> 00:12:56,160
That's a low sort of, I believe, J-O, no, J-O-P, won't be Parajayo-T.

151
00:12:56,160 --> 00:12:58,160
That's a question, sorry.

152
00:12:58,160 --> 00:13:01,160
But I'm going to upper medical, yeah, so it's a trifle.

153
00:13:01,160 --> 00:13:04,160
He doesn't say that's a bad kind of victory,

154
00:13:04,160 --> 00:13:07,160
or that kind of victory is actually leading you to ruin, which, in fact,

155
00:13:07,160 --> 00:13:08,160
it is.

156
00:13:08,160 --> 00:13:10,160
He says that's a trifling victory in years.

157
00:13:10,160 --> 00:13:13,160
So he, in a roundabout way,

158
00:13:13,160 --> 00:13:16,160
he's trying to get this Brahmin to see the error of his ways.

159
00:13:16,160 --> 00:13:19,160
He'll give a provide him with some friendly advice.

160
00:13:19,160 --> 00:13:22,160
The Brahmin did ask after all what leads to loss,

161
00:13:22,160 --> 00:13:25,160
and so he's trying to somehow tell him that his life's down.

162
00:13:25,160 --> 00:13:28,160
That's that.

163
00:13:28,160 --> 00:13:31,160
But instead he says, so to be very gentle,

164
00:13:31,160 --> 00:13:34,160
he says that's a trifling victory, a real victory is self-victory.

165
00:13:34,160 --> 00:13:38,160
Real conquest, that's conquering yourself.

166
00:13:38,160 --> 00:13:39,160
Why?

167
00:13:39,160 --> 00:13:43,160
Because that kind of conquest, you'll never lose.

168
00:13:43,160 --> 00:13:47,160
And so it's poignant in relation to the story,

169
00:13:47,160 --> 00:13:50,160
the idea of being invincible.

170
00:13:50,160 --> 00:13:57,160
Because the win of a gambler is always subject to threat.

171
00:13:57,160 --> 00:14:02,160
The next role of the dice, the next deal of the cards,

172
00:14:02,160 --> 00:14:06,160
could be, could change your fortune, could spell ruin.

173
00:14:06,160 --> 00:14:11,160
Whereas, and much victory is like this, victory in war,

174
00:14:11,160 --> 00:14:18,160
victory in business, victory in romance, victory of all kinds,

175
00:14:18,160 --> 00:14:21,160
is unpredictable.

176
00:14:21,160 --> 00:14:25,160
And it's, in stable, is impermanent.

177
00:14:25,160 --> 00:14:27,160
It doesn't last forever.

178
00:14:27,160 --> 00:14:35,160
It's always subject to the potential of being overdone, over thrown.

179
00:14:35,160 --> 00:14:38,160
But that's really the claim that's being made.

180
00:14:38,160 --> 00:14:41,160
So this is how it relates to our practice.

181
00:14:41,160 --> 00:14:44,160
It's an understanding of what, of the extent,

182
00:14:44,160 --> 00:14:48,160
and the magnitude of the practice that we're undertaking.

183
00:14:48,160 --> 00:14:51,160
I mean, this isn't just stress relief.

184
00:14:51,160 --> 00:14:56,160
I was thinking today about advertising the benefits of meditation.

185
00:14:56,160 --> 00:15:00,160
We're probably going to put up posters.

186
00:15:00,160 --> 00:15:04,160
And it made me think of, you know,

187
00:15:04,160 --> 00:15:09,160
people really, it's really catchy to say mindfulness-based stress reduction.

188
00:15:09,160 --> 00:15:12,160
Because it's a non-sectarian.

189
00:15:12,160 --> 00:15:13,160
It's non-religious.

190
00:15:13,160 --> 00:15:18,160
It's really something that people can sort of rolls off the tongue.

191
00:15:18,160 --> 00:15:21,160
But then I thought it's kind of, you know,

192
00:15:21,160 --> 00:15:23,160
petty to say stress reduction.

193
00:15:23,160 --> 00:15:25,160
Because that's not really what we're talking about, is it?

194
00:15:25,160 --> 00:15:28,160
In Buddhism we go quite, it's mindfulness-based,

195
00:15:28,160 --> 00:15:30,160
or it's based on satin.

196
00:15:30,160 --> 00:15:34,160
But it goes farther than just reducing stress.

197
00:15:34,160 --> 00:15:39,160
It uproots anything that could possibly cause you stress in the future.

198
00:15:39,160 --> 00:15:45,160
There's no potential for future stress.

199
00:15:45,160 --> 00:15:47,160
Meaning invincible.

200
00:15:47,160 --> 00:15:49,160
And it goes beyond heaven.

201
00:15:49,160 --> 00:15:51,160
It goes beyond God.

202
00:15:51,160 --> 00:15:54,160
It goes beyond the universe.

203
00:15:54,160 --> 00:16:00,160
It's something that goes beyond loss,

204
00:16:00,160 --> 00:16:04,160
something that can never be taken from you.

205
00:16:04,160 --> 00:16:08,160
We're talking about invincibility.

206
00:16:08,160 --> 00:16:11,160
How does this come about?

207
00:16:11,160 --> 00:16:19,160
It comes about by a person who is Atadanta, Bosa.

208
00:16:19,160 --> 00:16:28,160
The Neepjang Sanyatta Charino, who is constantly restrained.

209
00:16:28,160 --> 00:16:33,160
Meaning they never, when seeing a form with the eye,

210
00:16:33,160 --> 00:16:36,160
they never give rise to likes or dislikes.

211
00:16:36,160 --> 00:16:40,160
They see it as it is and they're objective.

212
00:16:40,160 --> 00:16:50,160
They free themselves from the potential for

213
00:16:50,160 --> 00:16:59,160
involvement in this game of gain and loss and happiness and suffering.

214
00:16:59,160 --> 00:17:06,160
It sounds actually quite fearsome for people who are addicted to pleasure

215
00:17:06,160 --> 00:17:12,160
and who are caught up in this game of gain and loss.

216
00:17:12,160 --> 00:17:19,160
It's something that you do have to kind of finesse when you talk to people.

217
00:17:19,160 --> 00:17:27,160
People often accuse, make the accusation that such a person would be someone like a zombie.

218
00:17:27,160 --> 00:17:32,160
When they are restrained,

219
00:17:32,160 --> 00:17:36,160
it means they are just suppressing their urges, which isn't at all the case.

220
00:17:36,160 --> 00:17:39,160
This is suppressing urges wouldn't work.

221
00:17:39,160 --> 00:17:42,160
It's not something that is sustainable.

222
00:17:42,160 --> 00:17:47,160
It's not the true victory.

223
00:17:47,160 --> 00:17:57,160
True victory is invincible, imperturbable.

224
00:17:57,160 --> 00:18:01,160
I was thinking about this today, the whole idea of the zombie, right?

225
00:18:01,160 --> 00:18:07,160
We have this idea that a person who doesn't like and dislike things is just like a zombie.

226
00:18:07,160 --> 00:18:10,160
Their life has no flavor, has no meaning.

227
00:18:10,160 --> 00:18:13,160
But then if you look at people who are caught up,

228
00:18:13,160 --> 00:18:17,160
if you think of the state of mind,

229
00:18:17,160 --> 00:18:21,160
a person who's caught up in chasing after things,

230
00:18:21,160 --> 00:18:23,160
the objects of their desire,

231
00:18:23,160 --> 00:18:25,160
how much like a zombie they are,

232
00:18:25,160 --> 00:18:31,160
how unaware they are of their own mind, of their own state,

233
00:18:31,160 --> 00:18:37,160
how inflamed the mind becomes with passion and hate.

234
00:18:37,160 --> 00:18:40,160
How the mind just in general becomes inflamed,

235
00:18:40,160 --> 00:18:45,160
and so is subject to greed, anger and delusion constantly.

236
00:18:45,160 --> 00:18:51,160
Sometimes a such intense greed that they will do anything to get what they want,

237
00:18:51,160 --> 00:18:59,160
even at the detriment to other people, even at the cost of friendship.

238
00:18:59,160 --> 00:19:09,160
Sometimes I'll give rise to such states of delusion that they alienate all of their friends

239
00:19:09,160 --> 00:19:16,160
due to their oppressive, arrogant, conceited nature.

240
00:19:16,160 --> 00:19:19,160
The level of conceit that were capable of the level of arrogance

241
00:19:19,160 --> 00:19:25,160
were capable of and how awful it is and how harmful it is.

242
00:19:25,160 --> 00:19:28,160
And anger, how we can be.

243
00:19:28,160 --> 00:19:35,160
So caught up with rage and hatred constantly

244
00:19:35,160 --> 00:19:45,160
depressed or angry, sad, afraid, all of these negative states.

245
00:19:45,160 --> 00:19:49,160
It's kind of like a zombie, like one of those zombie movies

246
00:19:49,160 --> 00:19:53,160
or zombie shows that everyone's watching.

247
00:19:53,160 --> 00:19:57,160
Evil zombies, how horrific they are.

248
00:19:57,160 --> 00:19:59,160
That's kind of what we're talking about.

249
00:19:59,160 --> 00:20:01,160
In light and people are nothing like that.

250
00:20:01,160 --> 00:20:04,160
In light and one is the opposite of a zombie.

251
00:20:04,160 --> 00:20:09,160
They are awake, they are alive, they never die.

252
00:20:09,160 --> 00:20:12,160
They're invincible.

253
00:20:12,160 --> 00:20:16,160
And so this is what we're striving for. It's something quite profound.

254
00:20:16,160 --> 00:20:21,160
I teach meditation, teaching at the university, and I teach to all sorts of people.

255
00:20:21,160 --> 00:20:27,160
And a lot of people are just in it for stress or reduction, stress or relief.

256
00:20:27,160 --> 00:20:29,160
And so it kind of pulls you in and you start to realize,

257
00:20:29,160 --> 00:20:31,160
oh, it goes a little bit deeper.

258
00:20:31,160 --> 00:20:34,160
And you end up down the rabbit hole, so to speak,

259
00:20:34,160 --> 00:20:39,160
where you don't recognize the world around you anymore,

260
00:20:39,160 --> 00:20:41,160
your whole world would change us.

261
00:20:41,160 --> 00:20:43,160
It's not like it's a trap or something.

262
00:20:43,160 --> 00:20:46,160
It's just the sweater starts to unravel.

263
00:20:46,160 --> 00:20:53,160
The whole illusion that you built up around yourself.

264
00:20:53,160 --> 00:20:57,160
And who you thought you were starts to unravel.

265
00:20:57,160 --> 00:20:59,160
And you see how asleep you've been.

266
00:20:59,160 --> 00:21:02,160
You're like a sleepwalker, like a zombie.

267
00:21:02,160 --> 00:21:04,160
That's how it should be.

268
00:21:04,160 --> 00:21:06,160
That's how it really is in meditation.

269
00:21:06,160 --> 00:21:09,160
You start to wake up and see, oh, I've got these problems.

270
00:21:09,160 --> 00:21:14,160
I'm going to find my mind as anger, my mind as delusion.

271
00:21:14,160 --> 00:21:16,160
Oh, boy, I'm in trouble.

272
00:21:16,160 --> 00:21:21,160
And so you start to do the hard work of changing that.

273
00:21:21,160 --> 00:21:24,160
That's what we do in meditation.

274
00:21:24,160 --> 00:21:27,160
So, and that is the highest victory.

275
00:21:27,160 --> 00:21:30,160
It's a victory that no one can take from you.

276
00:21:30,160 --> 00:21:33,160
That's the point of this verse, and it's very well said.

277
00:21:33,160 --> 00:21:36,160
As are all the things the Buddha said.

278
00:21:36,160 --> 00:21:40,160
Something that we should appreciate and remember to give us confidence

279
00:21:40,160 --> 00:21:43,160
that what we're aiming for is not something simple.

280
00:21:43,160 --> 00:21:49,160
It's not just a hobby that you can pick up and put down something

281
00:21:49,160 --> 00:21:53,160
that really and truly changes your life, transforms your way,

282
00:21:53,160 --> 00:21:58,160
wakes you up and helps you rise above your troubles.

283
00:21:58,160 --> 00:22:01,160
So that even though the situation may not change,

284
00:22:01,160 --> 00:22:04,160
no matter what the situation is,

285
00:22:04,160 --> 00:22:08,160
you are above it, you are beyond it.

286
00:22:08,160 --> 00:22:11,160
So, that's the teaching for tonight.

287
00:22:11,160 --> 00:22:14,160
This verse is 104 and 105.

288
00:22:14,160 --> 00:22:17,160
We're almost a quarter of the way through the Dhammapada.

289
00:22:17,160 --> 00:22:23,160
I think when we get to 108, that will be one quarter of the way.

290
00:22:23,160 --> 00:22:24,160
Thank you everyone.

291
00:22:24,160 --> 00:22:26,160
Thank you all for tuning in.

292
00:22:26,160 --> 00:22:30,160
We're wishing you all the best on your path to invincibility.

293
00:22:30,160 --> 00:22:35,160
Bye bye.

