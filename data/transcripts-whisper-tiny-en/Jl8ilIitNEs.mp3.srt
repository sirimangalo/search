1
00:00:00,000 --> 00:00:02,000
You

2
00:00:30,000 --> 00:00:32,000
You

3
00:01:00,000 --> 00:01:02,000
You

4
00:01:30,000 --> 00:01:32,000
You

5
00:02:00,000 --> 00:02:02,000
You

6
00:02:04,000 --> 00:02:07,000
Evening everyone. Welcome to our live broadcast

7
00:02:18,000 --> 00:02:20,000
Today we're looking at

8
00:02:20,000 --> 00:02:22,000
The

9
00:02:23,000 --> 00:02:25,000
I'm good during the guy a book of fours

10
00:02:27,000 --> 00:02:29,000
Starting with suit the 17

11
00:02:34,000 --> 00:02:37,000
Another fairly brief teaching but

12
00:02:40,000 --> 00:02:42,000
When I think it's important it's important to know

13
00:02:42,000 --> 00:02:44,000
I

14
00:02:46,000 --> 00:02:48,000
translate as inclinations

15
00:02:51,000 --> 00:03:01,000
But it's a sit this simple the word is simply going or coming

16
00:03:01,000 --> 00:03:19,000
a good thing a good thing a good thing is like partiality means being

17
00:03:19,000 --> 00:03:21,000
being

18
00:03:24,000 --> 00:03:27,000
predisposed for against something

19
00:03:31,000 --> 00:03:35,000
I think it's important because this sums up the

20
00:03:37,000 --> 00:03:41,000
concept of our reactions being most important

21
00:03:41,000 --> 00:03:52,000
That the big shift that has to come about is not in the experiences we

22
00:03:53,000 --> 00:03:56,000
encounter but in our reactions to them

23
00:03:59,000 --> 00:04:01,000
We don't come to meditate to try and

24
00:04:01,000 --> 00:04:08,000
Change the experiences we have

25
00:04:10,000 --> 00:04:13,000
Become to meditate to change the way we interact with them

26
00:04:14,000 --> 00:04:17,000
Change the way we look at them first and foremost

27
00:04:22,000 --> 00:04:25,000
Those things we think of as problems

28
00:04:25,000 --> 00:04:30,000
Those things we think of as desirable

29
00:04:33,000 --> 00:04:35,000
Our ambitions

30
00:04:36,000 --> 00:04:39,000
Our dearest possessions and

31
00:04:40,000 --> 00:04:43,000
Friends and enemies all these things we

32
00:04:43,000 --> 00:04:53,000
We're inclined in certain ways predisposed towards these things in certain ways

33
00:04:55,000 --> 00:05:01,000
Spiders so many people are strongly predisposed towards spiders

34
00:05:05,000 --> 00:05:09,000
Snakes on the other hand flowers

35
00:05:09,000 --> 00:05:13,000
Cigarettes

36
00:05:16,000 --> 00:05:18,000
The naked body

37
00:05:19,000 --> 00:05:23,000
You're in feces we're predisposed to so many different things

38
00:05:25,000 --> 00:05:29,000
And it's not even predisposed because we cultivate dispositions

39
00:05:30,000 --> 00:05:32,000
We cultivate inclinations

40
00:05:33,000 --> 00:05:35,000
We cultivate before a good deal

41
00:05:35,000 --> 00:05:38,000
They're habitual

42
00:05:38,000 --> 00:05:41,000
They're habits that we cultivate

43
00:05:41,000 --> 00:05:45,000
You might desire something a little bit or you might think oh that looks interesting

44
00:05:45,000 --> 00:05:49,000
And then when you obtain it sure enough it's pleasant

45
00:05:50,000 --> 00:05:56,000
And that reinflates the reinforcement of the desire for the option

46
00:05:57,000 --> 00:06:01,000
So the next time you have an actual substantial desire for it

47
00:06:01,000 --> 00:06:06,000
And then the next time and the next time it becomes an addiction

48
00:06:09,000 --> 00:06:15,000
So it's quite scary actually how strong these predispositions can become

49
00:06:15,000 --> 00:06:22,000
But it's quite encouraging on the other hand that they're simply constructs

50
00:06:22,000 --> 00:06:29,000
They can be done away with simply by changing your habits

51
00:06:34,000 --> 00:06:42,000
So the four Agatee are Chanda Chanda Agatee

52
00:06:43,000 --> 00:06:47,000
In inclination or being inclined

53
00:06:47,000 --> 00:06:52,000
Because of desire

54
00:06:53,000 --> 00:06:54,000
Dos Agatee

55
00:06:54,000 --> 00:06:59,000
Being inclined or predisposed because of anger

56
00:07:00,000 --> 00:07:01,000
Mohagatee

57
00:07:01,000 --> 00:07:03,000
Being inclined

58
00:07:05,000 --> 00:07:07,000
On a delusion

59
00:07:08,000 --> 00:07:09,000
And via Agatee

60
00:07:09,000 --> 00:07:11,000
Being disposed out of fear

61
00:07:11,000 --> 00:07:13,000
Predisposed

62
00:07:13,000 --> 00:07:18,000
Being inclined

63
00:07:19,000 --> 00:07:24,000
I mean is the most clear in their relation to our

64
00:07:24,000 --> 00:07:27,000
Relationships with other people

65
00:07:28,000 --> 00:07:33,000
Who are partial towards other people out of our attachment to them

66
00:07:35,000 --> 00:07:37,000
We

67
00:07:37,000 --> 00:07:42,000
This is how we distinguish between friends and enemies, we help our friends

68
00:07:42,000 --> 00:07:44,000
We try to hurt our enemies

69
00:07:46,000 --> 00:07:49,000
When we're partial to certain individuals

70
00:07:49,000 --> 00:07:52,000
When we're partial to certain experiences

71
00:07:53,000 --> 00:07:55,000
We treat them differently

72
00:07:55,000 --> 00:08:01,000
We treat them differently

73
00:08:02,000 --> 00:08:07,000
Favoriteism, this idea of favoring people of certain people

74
00:08:09,000 --> 00:08:15,000
Generally when you see people of the same race as you, the same color skin, for example

75
00:08:18,000 --> 00:08:21,000
This is where racism and bigotry come from

76
00:08:21,000 --> 00:08:26,000
When you see someone who's different color skin

77
00:08:26,000 --> 00:08:31,000
You're generally predisposed in certain ways towards them

78
00:08:31,000 --> 00:08:36,000
Maybe out of liking them, maybe out of hating them

79
00:08:36,000 --> 00:08:39,000
Maybe out of fear of them, mostly out of delusion

80
00:08:40,000 --> 00:08:43,000
Delusion because somehow you think it's meaningful

81
00:08:43,000 --> 00:08:50,000
On the other hand, you can be predisposed out of wisdom

82
00:08:51,000 --> 00:08:54,000
Not predisposed, but disposed

83
00:08:54,000 --> 00:08:56,000
Inclined out of wisdom

84
00:08:56,000 --> 00:08:59,000
You can be inclined to avoid certain things, for example, a snake

85
00:08:59,000 --> 00:09:02,000
You see a snake on the side of the road

86
00:09:02,000 --> 00:09:05,000
And you know that that snake is poisonous

87
00:09:05,000 --> 00:09:08,000
It's not wrong to be inclined to stay away from it

88
00:09:09,000 --> 00:09:12,000
But it's wrong to be inclined out of these four things

89
00:09:12,000 --> 00:09:14,000
It's not wrong to be inclined

90
00:09:14,000 --> 00:09:15,000
You can be inclined to meditate

91
00:09:15,000 --> 00:09:16,000
That's good

92
00:09:16,000 --> 00:09:18,000
Dhamma Chanda

93
00:09:18,000 --> 00:09:21,000
Desire to practice the Dhamma

94
00:09:24,000 --> 00:09:26,000
It's an inclination of service

95
00:09:26,000 --> 00:09:28,000
But when you're inclined out of

96
00:09:29,000 --> 00:09:32,000
Last or inclined out of hatred

97
00:09:32,000 --> 00:09:34,000
Inclined out of delusion

98
00:09:35,000 --> 00:09:37,000
Or fear

99
00:09:37,000 --> 00:09:43,000
You tend not to make the rational choices that would bring you peace and resolution

100
00:09:47,000 --> 00:09:53,000
We react hastily out of fear, out of anger, out of desire

101
00:09:53,000 --> 00:09:57,000
You have a hard time discerning what is in our best interest

102
00:10:00,000 --> 00:10:05,000
This is people eat too much or engage in constant entertainment

103
00:10:05,000 --> 00:10:10,000
Or they're constantly bickering or fighting

104
00:10:10,000 --> 00:10:14,000
Or harming others, complaining

105
00:10:17,000 --> 00:10:20,000
They create conflict and enmity

106
00:10:23,000 --> 00:10:28,000
Or they ruin things through delusion, confusion, arrogance, conceit

107
00:10:28,000 --> 00:10:36,000
You can think of it like being bent

108
00:10:36,000 --> 00:10:41,000
Inclined in a certain direction is like being bent out of shape

109
00:10:41,000 --> 00:10:44,000
Our minds are crooked

110
00:10:44,000 --> 00:10:47,000
We use this word in English, crooked

111
00:10:47,000 --> 00:10:49,000
Someone who's crooked is not honest

112
00:10:49,000 --> 00:10:52,000
It's not straight up, it's not upright

113
00:10:52,000 --> 00:10:55,000
There's something weasely about them

114
00:10:55,000 --> 00:11:00,000
And a weasel is slippery and not straight forward

115
00:11:04,000 --> 00:11:09,000
Meditation is about cultivating rectitude straightness

116
00:11:12,000 --> 00:11:14,000
Ooh-joo, there's actually a word for it

117
00:11:14,000 --> 00:11:17,000
The song I said, the Ooh-joo, straight forward

118
00:11:18,000 --> 00:11:23,000
They don't act out of desire, they don't act out of hatred

119
00:11:23,000 --> 00:11:28,000
They don't act out of delusion, they don't act out of fear

120
00:11:28,000 --> 00:11:33,000
So we try to be objective, this is really the key in the meditation practice

121
00:11:36,000 --> 00:11:40,000
Don't worry about what you experience, don't try to fix things

122
00:11:40,000 --> 00:11:42,000
Even your reactions, don't try to fix them

123
00:11:42,000 --> 00:11:45,000
Just try to be straight about everything

124
00:11:45,000 --> 00:11:47,000
If you're angry, just know that you're angry

125
00:11:47,000 --> 00:11:49,000
Say to yourself, angry, angry, remind yourself

126
00:11:49,000 --> 00:11:53,000
That's anger, to experience something that you like

127
00:11:53,000 --> 00:11:56,000
Or you would normally want, try to just see it forward again

128
00:11:56,000 --> 00:11:58,000
It's just seeing, just hearing

129
00:11:58,000 --> 00:12:02,000
If you feel happy about something, please do buy something

130
00:12:02,000 --> 00:12:05,000
Try and see it just as pleasure

131
00:12:05,000 --> 00:12:20,000
Don't be partial or cultivate favoritism

132
00:12:21,000 --> 00:12:26,000
Your mind becomes straight and like a straight tree, your mind becomes strong

133
00:12:26,000 --> 00:12:28,000
It's able to grow

134
00:12:28,000 --> 00:12:33,000
You're only able to really grow and reach the sky

135
00:12:33,000 --> 00:12:37,000
If you're upright

136
00:12:37,000 --> 00:12:41,000
When the mind is crooked, like a crooked tree, it can't grow

137
00:12:41,000 --> 00:12:45,000
And it fails and falters and winners and dies

138
00:12:52,000 --> 00:12:57,000
So we remember these four, it's kind of like the, it's actually just the first three

139
00:12:57,000 --> 00:13:03,000
Great anger and delusion, but they add fear in here, because it's specific on the Buddha's fear

140
00:13:03,000 --> 00:13:07,000
Because it's specifically, I think, relates to dealing with people

141
00:13:07,000 --> 00:13:11,000
This is applying to favoritism towards people

142
00:13:11,000 --> 00:13:15,000
But it's quite useful to know these types of partiality

143
00:13:15,000 --> 00:13:19,000
We're not straightforward and objective, how we treat people

144
00:13:19,000 --> 00:13:25,000
And the experience is differently based on our inclination

145
00:13:25,000 --> 00:13:29,000
These things color, our judgment, they color our experience

146
00:13:37,000 --> 00:13:40,000
And we have a polyverse here

147
00:13:40,000 --> 00:13:41,000
We have a little more

148
00:13:41,000 --> 00:13:57,000
We have a little more of a little more of a little more of a little more of a little more

149
00:13:57,000 --> 00:14:04,000
If through desire, hate, fear or delusion, one transgresses against the Dhamma

150
00:14:04,000 --> 00:14:09,000
One's famed diminishes like the moon in the dark fortnight, it's just the half of the moon

151
00:14:09,000 --> 00:14:16,000
Or the moon, the half of the month where the moon gets darker and darker every day

152
00:14:16,000 --> 00:14:25,000
So it's specifically relating to people, it's as to do with fame, your reputation

153
00:14:25,000 --> 00:14:35,000
And you have a reputation for favoritism, and you don't treat people objectively

154
00:14:35,000 --> 00:14:41,000
You don't think of what's most helpful to people, spending lots of time chatting with people

155
00:14:41,000 --> 00:14:46,000
When there's no benefit coming or dismissing people when they want to

156
00:14:46,000 --> 00:15:01,000
And they have questions and concerns about the Dhamma

157
00:15:01,000 --> 00:15:10,000
Partial to certain experiences, partial to certain mine states, partial to certain feelings

158
00:15:10,000 --> 00:15:22,000
For and against, basically the thing is don't relate to things, try to relate to things with greed or anger

159
00:15:22,000 --> 00:15:29,000
Don't let that be the defining feature or delusion

160
00:15:29,000 --> 00:15:36,000
Where you make decisions based on your emotions, rather than what's right

161
00:15:36,000 --> 00:15:42,000
And what's wrong, what's actually good for you, because these things won't tell you that

162
00:15:42,000 --> 00:15:48,000
These are habits, they're not based on reality

163
00:15:48,000 --> 00:15:54,000
Because when you start to see reality for what it is, your habits change

164
00:15:54,000 --> 00:16:01,000
You could develop habits based on wisdom, and then your inclination is all based on experience, knowledge

165
00:16:01,000 --> 00:16:07,000
Knowing that this is actually not good for me, this is good for me, that kind of thing

166
00:16:07,000 --> 00:16:32,000
Anyway, so that's our little bit of Dhamma for tonight

167
00:16:32,000 --> 00:16:39,000
How are we doing?

168
00:16:39,000 --> 00:16:46,000
Robin, you're way over on the left-hand side of your screen, why is that?

169
00:16:46,000 --> 00:16:49,000
Oh, you know why I ask?

170
00:16:49,000 --> 00:16:56,000
Because when I put you in our screen, I've cut off the sides

171
00:16:56,000 --> 00:17:03,000
Oh, okay, and then it cuts part of it, kind of cuts

172
00:17:03,000 --> 00:17:09,000
I probably just move the webcam, it's a separate webcam, is that

173
00:17:09,000 --> 00:17:17,000
That's better, sort of, I think I've actually adjusted, are you now in the middle?

174
00:17:17,000 --> 00:17:21,000
Yeah, I don't see what you see, so...

175
00:17:21,000 --> 00:17:22,000
Oh, you know?

176
00:17:22,000 --> 00:17:25,000
You're in the bottom right corner, if you click on your picture in the bottom right corner

177
00:17:25,000 --> 00:17:28,000
It should come up big, and you can click.

178
00:17:28,000 --> 00:17:29,000
Okay.

179
00:17:29,000 --> 00:17:32,000
Now you're centered, sort of.

180
00:17:32,000 --> 00:17:37,000
See, but now I've got your center.

181
00:17:37,000 --> 00:17:42,000
Okay.

182
00:17:42,000 --> 00:17:44,000
I'm ready for some questions.

183
00:17:44,000 --> 00:17:46,000
I'm ready for some questions.

184
00:17:46,000 --> 00:17:53,000
Yesterday, I asked about Kamoy Yoga, and practices outside Satyipakana to assist in

185
00:17:53,000 --> 00:17:57,000
furthering a wholesome state, and wasn't specific.

186
00:17:57,000 --> 00:18:01,000
What would your advice be for someone who has cut off a bond with a person,

187
00:18:01,000 --> 00:18:05,000
and despite the brightness of the decision, is struggling with the craving

188
00:18:05,000 --> 00:18:09,000
and attachment that comes from psychic casting and loss?

189
00:18:09,000 --> 00:18:11,000
Are there any additional practices?

190
00:18:11,000 --> 00:18:17,000
More specifically, despite the mindfulness, noting, and meditation, the mind isn't letting go.

191
00:18:17,000 --> 00:18:19,000
Were you kidding?

192
00:18:19,000 --> 00:18:26,000
Even more specific, it's mostly...

193
00:18:26,000 --> 00:18:39,000
I mean, you can't force your mind to let go, which is what you're seeing.

194
00:18:39,000 --> 00:18:43,000
It's not just simple like that where you say, okay, let go, let go.

195
00:18:43,000 --> 00:18:46,000
That's part of the... it's an important lesson for you,

196
00:18:46,000 --> 00:18:58,000
just to say that the mind doesn't work the way you think it does.

197
00:18:58,000 --> 00:19:02,000
I mean, sure, there's lots of things you can do to help yourself,

198
00:19:02,000 --> 00:19:04,000
but none of them are really going to be...

199
00:19:04,000 --> 00:19:18,000
I mean, they're really going to be sufficient.

200
00:19:18,000 --> 00:19:24,000
Surround yourself with good people, find the community,

201
00:19:24,000 --> 00:19:29,000
like-minded individuals that help you, get pagged over it,

202
00:19:29,000 --> 00:19:33,000
help you remind you of good things that kind of thing.

203
00:19:33,000 --> 00:19:36,000
But in the end, there's no trick.

204
00:19:36,000 --> 00:19:39,000
You just kind of slog through it.

205
00:19:39,000 --> 00:19:43,000
There's no way to make it easy or comfortable.

206
00:19:43,000 --> 00:19:46,000
It's like withdrawal, and it's way worse than any kind of drug,

207
00:19:46,000 --> 00:19:58,000
because it's something we held on to for a long time.

208
00:19:58,000 --> 00:20:07,000
In the end, it's just meditation, it's going to help you.

209
00:20:07,000 --> 00:20:09,000
But it won't suddenly turn these things off.

210
00:20:09,000 --> 00:20:15,000
It's just helping you to understand and to let go.

211
00:20:15,000 --> 00:20:18,000
And that go means, you know, let come as well.

212
00:20:18,000 --> 00:20:21,000
Let me don't react.

213
00:20:21,000 --> 00:20:27,000
Don't give it energy, don't feed it.

214
00:20:27,000 --> 00:20:32,000
I will cause that.

215
00:20:32,000 --> 00:20:35,000
Hi, Monkey.

216
00:20:35,000 --> 00:20:40,000
My introduction to meditation and Buddhist books was quite young in my teens.

217
00:20:40,000 --> 00:20:46,000
But I've come to think that it plays some detrimental effect on me.

218
00:20:46,000 --> 00:20:51,000
The reason when I was young, I have the impression that Buddhism taught that thinking is not good.

219
00:20:51,000 --> 00:20:55,000
So I feel I have not done enough reflecting and introspector critical thinking

220
00:20:55,000 --> 00:20:56,000
when it should be done.

221
00:20:56,000 --> 00:21:06,000
Even though I don't quite understand every time I read about Buddhism, it always says to note thinking, as thinking, as if we should reduce our thoughts in our lives.

222
00:21:06,000 --> 00:21:13,000
When I feel like this, if you want history, I know the loss of deep, deep contemplation in our lives. Thank you.

223
00:21:13,000 --> 00:21:28,000
Now, first of all, when you know something is thinking, when you know thinking is thinking, there's no part of that, that even hints or implies that you should reduce your thinking.

224
00:21:28,000 --> 00:21:33,000
That's not at all what it's saying, maybe you should read it again.

225
00:21:33,000 --> 00:21:40,000
But I guess the implication is, which is, you know, it's still part of what you're saying.

226
00:21:40,000 --> 00:21:52,000
It exposes that you're not reacting or acting out on it, you're not inclining your mind towards it.

227
00:21:52,000 --> 00:21:58,000
But as far as people in history, known for lots of deep contemplation,

228
00:21:58,000 --> 00:22:05,000
in the fact the idea that their wise is certainly up for debate.

229
00:22:05,000 --> 00:22:10,000
If you want to follow their lead, go ahead.

230
00:22:10,000 --> 00:22:18,000
But the Buddha didn't follow their lead on everything, on everything anyway.

231
00:22:18,000 --> 00:22:25,000
So you have to decide, if you think that it's better to think a lot, then go ahead.

232
00:22:25,000 --> 00:22:32,000
But you know how our Buddhism stands, that thinking is not, thinking is not the answer, thinking is in the solution.

233
00:22:32,000 --> 00:22:40,000
There's no reason why thinking should benefit you or enlighten you, not substantially.

234
00:22:40,000 --> 00:22:45,000
It's funny how people think and then somehow think that they're going to come up with an answer.

235
00:22:45,000 --> 00:22:48,000
If you read Descartes' meditations, it's quite interesting.

236
00:22:48,000 --> 00:22:52,000
I mean, obviously I don't believe with, I don't agree with many of the things Descartes said,

237
00:22:52,000 --> 00:22:56,000
but he really didn't meditate.

238
00:22:56,000 --> 00:23:04,000
He could say he thought, but he was really trying to observe his experience and see what was real.

239
00:23:04,000 --> 00:23:10,000
It was a valiant effort without the Buddha's wisdom, but he failed.

240
00:23:10,000 --> 00:23:16,000
He certainly tried, and it gives you an idea of the sort of difference.

241
00:23:16,000 --> 00:23:24,000
Because he wasn't really interested in his thoughts, he was interested in what was real about him or about reality.

242
00:23:24,000 --> 00:23:31,000
Of course, then he started thinking about it, and they came up with some good ideas.

243
00:23:31,000 --> 00:23:37,000
But I never became enlightened.

244
00:23:37,000 --> 00:23:42,000
Guess I mean to say that the best part of his meditations was when he was actually looking and saying,

245
00:23:42,000 --> 00:23:49,000
hey, this is all there is, once he started thinking about it, well, he got on and mixed up.

246
00:23:49,000 --> 00:23:55,000
I mean, it not mixed up, but he added things that weren't actually there.

247
00:23:55,000 --> 00:24:04,000
They say that he was under duress because anybody who, what he was really trying to do was be able to separate the body and the mind,

248
00:24:04,000 --> 00:24:13,000
because the mind was free, had free will, and the whole idea of God, he had to separate God out from the body.

249
00:24:13,000 --> 00:24:22,000
Because otherwise, he couldn't talk about the body as being deterministic, so he was just sort of, anyway.

250
00:24:22,000 --> 00:24:29,000
He still gotten quite a bit of trouble, I think, for expressing his views.

251
00:24:29,000 --> 00:24:34,000
So he had a kind of hand-in-gloom God, anyway.

252
00:24:34,000 --> 00:24:39,000
It's not your question, but it does relate to people in history because they're not as wise, I think,

253
00:24:39,000 --> 00:24:43,000
because you're making them up to me.

254
00:24:43,000 --> 00:24:56,000
They were conventionally wise, but I feel like a Buddha.

255
00:24:56,000 --> 00:25:02,000
I know all my emotions with feeling, feeling, any problems with that.

256
00:25:02,000 --> 00:25:09,000
You kind of had this question last night, and the problem is that you don't really experience them as feeling, feeling.

257
00:25:09,000 --> 00:25:12,000
If you're angry, you don't think of it as a feeling, you think of it as anger.

258
00:25:12,000 --> 00:25:18,000
If you want something, it's not a feeling, it's wanting.

259
00:25:18,000 --> 00:25:25,000
I mean, arguably it is kind of a feeling in a general sense of the word, but it's not all that helpful.

260
00:25:25,000 --> 00:25:32,000
I mean, you're not really going to be clear about what you're experiencing if you don't use the right word.

261
00:25:32,000 --> 00:25:43,000
So it's one thing to get obsessive about finding the right word, but it's another to be lazy.

262
00:25:43,000 --> 00:25:59,000
When I started to repel even to the thought of meditation, it's hard to be focused with annoyance and patience and wanting mind.

263
00:25:59,000 --> 00:26:04,000
I do know these feelings, but right now I might have to experience them a little longer.

264
00:26:04,000 --> 00:26:14,000
I have to get through this.

265
00:26:14,000 --> 00:26:16,000
Well, the thought of meditation is just a thought.

266
00:26:16,000 --> 00:26:21,000
I mean, it's interesting that you've gotten averse to it.

267
00:26:21,000 --> 00:26:25,000
It's common because meditation's tough, and the mind doesn't like tough.

268
00:26:25,000 --> 00:26:27,000
But meditation doesn't exist.

269
00:26:27,000 --> 00:26:32,000
It's just a thought you have.

270
00:26:32,000 --> 00:26:42,000
Now, the whole idea of insight meditation is to figure out how to overcome that, how to be free from the influence of your suffering.

271
00:26:42,000 --> 00:26:49,000
So it's stressful when you think about that meditation, but not because of the thought and the meditation are the problem.

272
00:26:49,000 --> 00:26:57,000
The thought is the annoyance, what you say, annoyance and patience and wanting mind.

273
00:26:57,000 --> 00:27:03,000
So that's what you're meditating on, and you're learning to see them clearly in that cause.

274
00:27:03,000 --> 00:27:09,000
If you don't like meditating, well, that's just one more thing you don't like, and that's what you should meditate on.

275
00:27:09,000 --> 00:27:23,000
The thing and the disliking, because otherwise you suffer, if you're always playing or following your emotions,

276
00:27:23,000 --> 00:27:38,000
you can be a slave to them, and you just reinforce them, and become stronger, and they cause you more stress than suffering.

277
00:27:38,000 --> 00:27:45,000
I have that tendency to escape into meditation and the download talks.

278
00:27:45,000 --> 00:28:13,000
For example, there's no trick to it, and as with everything else, there's no trick in them.

279
00:28:13,000 --> 00:28:19,000
You just have to do it. You have to learn to be mindful, learn to see things as they are.

280
00:28:19,000 --> 00:28:27,000
Nobody's going to do it for you. I can't fix your problems with my answers.

281
00:28:27,000 --> 00:28:36,000
I mean, there's much there. I mean, whether studying is actually beneficial, you might eventually decide that studying is not in your best interest,

282
00:28:36,000 --> 00:28:46,000
but when it's something you have to do, you have to look at your mind, and you have to learn how to let go of your attachment to,

283
00:28:46,000 --> 00:28:54,000
in this case, watching YouTube videos. If it's something that pleases you, you have to say, one thing, one thing,

284
00:28:54,000 --> 00:29:00,000
and pleasure, pleasure, happy, happy.

285
00:29:00,000 --> 00:29:07,000
You have to reverse the studying, there you go. Just reverse the meditating, meditate on the aversion.

286
00:29:30,000 --> 00:29:37,000
Is still there? Yes, but I couldn't hear you for a moment.

287
00:29:37,000 --> 00:29:45,000
I'm not sure if I disconnected or you did, or is this just an internal glitch there?

288
00:29:45,000 --> 00:29:55,000
Well, I heard myself. Okay. At the end of one of your download talks, maybe last night, the audio cut off completely.

289
00:29:55,000 --> 00:30:02,000
I could hear you, but it wasn't coming for on YouTube, so a little something going on with the audio.

290
00:30:02,000 --> 00:30:07,000
Something happened yesterday, and I felt the men's pity for myself.

291
00:30:07,000 --> 00:30:12,000
I tried to note that I was working with someone and found it difficult.

292
00:30:12,000 --> 00:30:17,000
About an hour later, I sat and meditated and noted the immense sadness for a while.

293
00:30:17,000 --> 00:30:20,000
After an intense earthquake, it went away.

294
00:30:20,000 --> 00:30:25,000
I was surprised, but you went. The sadness came back several times over the next hour or two.

295
00:30:25,000 --> 00:30:31,000
Each time I did a mini meditation, and each time I felt the sadness was less and less.

296
00:30:31,000 --> 00:30:36,000
But then things got worse. I acknowledged feelings over and over, but it got steadily worse.

297
00:30:36,000 --> 00:30:42,000
In the end, I was so distressed that I thought it would be good to die. Not going to, just thoughts.

298
00:30:42,000 --> 00:30:49,000
I stopped meditating until this morning. I'm still distressed, but I am now doing short bursts of meditation, getting on.

299
00:30:49,000 --> 00:30:56,000
Practicing and noticing, thinking, thinking, is there a way of better managing when you are overwhelming emotions?

300
00:30:56,000 --> 00:31:00,000
No, I mean, all of this is inside. This is habits.

301
00:31:00,000 --> 00:31:08,000
As soon as you start to try to change your habits, your mind rebels or reacts,

302
00:31:08,000 --> 00:31:14,000
because it's like trying to stem the tide and really can overwhelm you.

303
00:31:14,000 --> 00:31:21,000
You are trying to, it's not comfortable to change.

304
00:31:21,000 --> 00:31:28,000
That being said, you have to be careful not to get caught up in the disappearing of things.

305
00:31:28,000 --> 00:31:33,000
It's good that you see that sometimes when you know that your mind is clear for a moment,

306
00:31:33,000 --> 00:31:39,000
and the thing disappeared, the problem disappears, but it's important not to get stuck on that.

307
00:31:39,000 --> 00:31:46,000
You have to come back to the present moment and be mindful of what's next, because there might be stronger emotions.

308
00:31:46,000 --> 00:31:51,000
But the point is that no matter how strongly emotion gets, it's only an emotion.

309
00:31:51,000 --> 00:31:55,000
That's the difference between these two approaches practicing to get rid of things,

310
00:31:55,000 --> 00:32:01,000
and practicing to be invincible to them. It's quite different.

311
00:32:01,000 --> 00:32:04,000
When you focus on the pain, there's two approaches.

312
00:32:04,000 --> 00:32:06,000
One is thinking, how can I get rid of this?

313
00:32:06,000 --> 00:32:10,000
And then there's thinking, how can I stop reacting to it?

314
00:32:10,000 --> 00:32:13,000
So no matter how distressed you get, or no matter what kind of thoughts you have,

315
00:32:13,000 --> 00:32:18,000
thoughts of killing yourself, thoughts of killing other people, thoughts of doing the worst of worst things,

316
00:32:18,000 --> 00:32:25,000
are just thoughts. Even your emotions are just emotions.

317
00:32:25,000 --> 00:32:29,000
It's when you react to them and get upset about them,

318
00:32:29,000 --> 00:32:34,000
that you return them into a problem.

319
00:32:34,000 --> 00:32:40,000
So there's no aim to stress, to stress arises, just remind yourself, okay, this is now. Forget about the past.

320
00:32:40,000 --> 00:32:44,000
So no, I'm just say to yourself, that work before, why doesn't it work now?

321
00:32:44,000 --> 00:32:47,000
You start to doubt because, hey, it's getting worse.

322
00:32:47,000 --> 00:32:51,000
But getting worse is just an appeal to the past and the future.

323
00:32:51,000 --> 00:32:54,000
It's not being in the present. The present things can't get worse.

324
00:32:54,000 --> 00:32:57,000
They just are what they are.

325
00:32:57,000 --> 00:33:02,000
And then at that point, no matter how bad it gets, it is what it is.

326
00:33:02,000 --> 00:33:07,000
It's not better or worse, more or less, stronger, weaker.

327
00:33:07,000 --> 00:33:08,000
It's all relative.

328
00:33:08,000 --> 00:33:13,000
And relative is just past and future. It's not the present.

329
00:33:20,000 --> 00:33:24,000
I guess that the kids don't be overwhelmed by your emotions.

330
00:33:24,000 --> 00:33:36,000
Then they won't be overwhelmed by me.

331
00:33:36,000 --> 00:33:41,000
Someone wants the live stream.

332
00:33:41,000 --> 00:33:44,000
It's funny, this app was supposed to have the live stream.

333
00:33:44,000 --> 00:33:46,000
Did it? Click on live stream.

334
00:33:46,000 --> 00:33:49,000
Live stream currently offline.

335
00:33:49,000 --> 00:33:54,000
Oh. You know what? I wonder if I'm broadcasting to the AM.

336
00:33:54,000 --> 00:33:58,000
I'm broadcasting to the wrong place, I think.

337
00:33:58,000 --> 00:34:02,000
No. Where am I broadcasting to?

338
00:34:02,000 --> 00:34:04,000
Huh.

339
00:34:04,000 --> 00:34:14,000
I think I am broadcasting to the wrong place.

340
00:34:14,000 --> 00:34:19,000
Oh, I'm not broadcasting at all.

341
00:34:19,000 --> 00:34:27,000
Shoot.

342
00:34:27,000 --> 00:34:29,000
That's funny.

343
00:34:29,000 --> 00:34:31,000
This is just going nowhere.

344
00:34:31,000 --> 00:34:34,000
It's funny that it's not telling me a minute.

345
00:34:34,000 --> 00:34:36,000
Here's the second. Let me fix that.

346
00:34:36,000 --> 00:34:45,000
I have to rethink how the port.

347
00:35:06,000 --> 00:35:17,000
It sounds like everything YouTube.

348
00:35:17,000 --> 00:35:20,000
It sounds like everything was coming out.

349
00:35:20,000 --> 00:35:43,000
And then YouTube, though.

350
00:35:43,000 --> 00:35:51,000
That's it.

351
00:36:13,000 --> 00:36:37,000
Why did it turn off?

352
00:36:37,000 --> 00:36:39,000
That's interesting.

353
00:36:39,000 --> 00:36:44,000
I know what it is, when I type an M, I bet.

354
00:36:44,000 --> 00:36:48,000
No. Shift M.

355
00:36:48,000 --> 00:36:50,000
Is that a shortcut from the Earth?

356
00:36:50,000 --> 00:36:56,000
I think it is. I think there's things.

357
00:36:56,000 --> 00:36:58,000
Yeah.

358
00:36:58,000 --> 00:36:59,000
A and D.

359
00:36:59,000 --> 00:37:01,000
That's what's turning it off.

360
00:37:01,000 --> 00:37:03,000
That's probably what happened last night.

361
00:37:03,000 --> 00:37:08,000
I set up some shortcuts and it works even when I'm not in the window.

362
00:37:08,000 --> 00:37:11,000
Let's get rid of those.

363
00:37:11,000 --> 00:37:14,000
I don't need shortcuts.

364
00:37:14,000 --> 00:37:17,000
I was wondering, as early as well, I'm like, I thought I turned that on.

365
00:37:17,000 --> 00:37:23,000
Why did it turn off?

366
00:37:23,000 --> 00:37:24,000
Okay.

367
00:37:24,000 --> 00:37:25,000
So here's the thing.

368
00:37:25,000 --> 00:37:29,000
There is a live stream and it set a new location.

369
00:37:29,000 --> 00:37:34,000
So go to the app and the menu and then look on live stream.

370
00:37:34,000 --> 00:37:42,000
And then as to where it is being saved,

371
00:37:42,000 --> 00:37:47,000
it should be saved in our new subdomain.

372
00:37:47,000 --> 00:37:49,000
Yes.

373
00:37:49,000 --> 00:37:56,000
Share.ceremunglo.org, front slash broadcast.

374
00:37:56,000 --> 00:38:01,000
So here's where there should be the new...

375
00:38:01,000 --> 00:38:04,000
This recording right now, which just started a few minutes ago,

376
00:38:04,000 --> 00:38:07,000
shouldn't be here at this new address.

377
00:38:07,000 --> 00:38:12,000
Share.ceremunglo.org, front slash broadcast.

378
00:38:12,000 --> 00:38:15,000
We should add that link to the live stream page.

379
00:38:15,000 --> 00:38:26,000
Should actually say, here's where the archive is.

380
00:38:26,000 --> 00:38:27,000
I'll do that now.

381
00:38:27,000 --> 00:38:30,000
I'll open an issue.

382
00:38:30,000 --> 00:38:31,000
Any more questions?

383
00:38:31,000 --> 00:38:33,000
How are we doing?

384
00:38:33,000 --> 00:38:35,000
Sort of questions.

385
00:38:35,000 --> 00:38:40,000
Was it, did you intend to have no chat on the YouTube today?

386
00:38:40,000 --> 00:38:41,000
Yes.

387
00:38:41,000 --> 00:38:42,000
The chat was disabled.

388
00:38:42,000 --> 00:38:44,000
I was intentional.

389
00:38:44,000 --> 00:38:49,000
And then Meg was asking about the old talks.

390
00:38:49,000 --> 00:38:50,000
And I didn't...

391
00:38:50,000 --> 00:38:54,000
I know the old...

392
00:38:54,000 --> 00:38:59,000
There was a page on the old site that had called the old talks.

393
00:38:59,000 --> 00:39:03,000
And they should still be at the old location.

394
00:39:03,000 --> 00:39:07,000
I wasn't able to access them.

395
00:39:07,000 --> 00:39:10,000
Oh, because they're at meditation.

396
00:39:10,000 --> 00:39:15,000
I think it's the change over to the website.

397
00:39:15,000 --> 00:39:18,000
Right.

398
00:39:18,000 --> 00:39:20,000
See if I can fix that.

399
00:39:20,000 --> 00:39:22,000
Provide access to those again.

400
00:39:22,000 --> 00:39:24,000
But they can now be old ones now.

401
00:39:24,000 --> 00:39:27,000
The new ones aren't going to go in that place.

402
00:39:27,000 --> 00:39:30,000
See?

403
00:39:30,000 --> 00:39:35,000
Yeah.

404
00:39:35,000 --> 00:39:45,000
That's nothing there.

405
00:39:45,000 --> 00:39:55,000
Yeah, there we are.

406
00:39:55,000 --> 00:40:15,000
Now I just got to find a place to put these.

407
00:40:15,000 --> 00:40:16,000
Okay.

408
00:40:16,000 --> 00:40:21,000
I've just moved them.

409
00:40:21,000 --> 00:40:26,000
So they are now.

410
00:40:26,000 --> 00:40:31,000
Shouldn't be at audio.ceremungal.org.

411
00:40:31,000 --> 00:40:34,000
Let's see about this.

412
00:40:34,000 --> 00:40:35,000
I don't know.

413
00:40:35,000 --> 00:40:36,000
It didn't work.

414
00:40:36,000 --> 00:40:46,000
Why doesn't audio.ceremungal wait?

415
00:40:46,000 --> 00:40:51,000
Didn't we have an audio.ceremungal.org?

416
00:40:51,000 --> 00:40:56,000
It was static.ceremungal.org.

417
00:40:56,000 --> 00:40:59,000
Direct audio is that one.

418
00:40:59,000 --> 00:41:00,000
Yeah.

419
00:41:00,000 --> 00:41:02,000
But it wasn't there.

420
00:41:02,000 --> 00:41:03,000
It was.

421
00:41:03,000 --> 00:41:08,000
But it's gone now, I think.

422
00:41:08,000 --> 00:41:14,000
I thought I had an audio page with some PHP work.

423
00:41:14,000 --> 00:41:16,000
Oh, you're right.

424
00:41:16,000 --> 00:41:18,000
Yeah, you're right.

425
00:41:18,000 --> 00:41:20,000
Yeah, it's like on my web blog.

426
00:41:20,000 --> 00:41:22,000
There's a link to that.

427
00:41:22,000 --> 00:41:25,000
And of course, another number there.

428
00:41:25,000 --> 00:41:29,000
Did Maxa doesn't actually work anymore.

429
00:41:29,000 --> 00:41:33,000
Hmm.

430
00:41:33,000 --> 00:41:39,000
We're going to buy your cool Excel show.

431
00:41:39,000 --> 00:41:41,000
Can't buy this.

432
00:41:41,000 --> 00:41:44,000
But we do give them wait for free.

433
00:41:44,000 --> 00:41:47,000
And actually our volunteer group is right in the process of

434
00:41:47,000 --> 00:41:49,000
setting up a system right now.

435
00:41:49,000 --> 00:41:52,000
We have a lot of the extra booklets there in Sri Lanka,

436
00:41:52,000 --> 00:41:53,000
which is where they're printed.

437
00:41:53,000 --> 00:41:57,000
And they're being sent to a couple of well placed volunteers

438
00:41:57,000 --> 00:42:03,000
running Europe, one in India, and one in the United States.

439
00:42:03,000 --> 00:42:07,000
And from there, we'll have something on the website where you can request one.

440
00:42:07,000 --> 00:42:09,000
And our volunteers will send one out to you.

441
00:42:09,000 --> 00:42:12,000
A couple of weeks because I think the estimate was about four

442
00:42:12,000 --> 00:42:16,000
to six weeks from Sri Lanka to the destinations

443
00:42:16,000 --> 00:42:19,000
before the volunteers get them to mail them out.

444
00:42:19,000 --> 00:42:23,000
So check the website, maybe in the bottom on the front now.

445
00:42:31,000 --> 00:42:33,000
I don't know what's wrong there.

446
00:42:33,000 --> 00:42:38,000
I'm going to have time for that.

447
00:42:38,000 --> 00:42:40,000
So what are we going to do?

448
00:42:40,000 --> 00:42:42,000
We'll go here and stand as that one.

449
00:42:42,000 --> 00:43:06,000
Yes, it's time.

450
00:43:12,000 --> 00:43:36,000
Okay.

451
00:43:36,000 --> 00:43:41,000
That should work.

452
00:43:41,000 --> 00:43:51,000
Well, I deal, but where am I?

453
00:43:51,000 --> 00:43:59,000
So I put on my web blog a new link to the audio, which is here.

454
00:43:59,000 --> 00:44:04,000
So if you go to that, which we're all been pointed out,

455
00:44:04,000 --> 00:44:08,000
you'll find a directory, and you should find a new

456
00:44:08,000 --> 00:44:12,000
subdirectory called live.

457
00:44:12,000 --> 00:44:39,000
And the live subdirectory is where all the old live streams are now.

458
00:44:43,000 --> 00:44:46,000
Thank you, Buffet.

459
00:44:46,000 --> 00:44:48,000
Are we done?

460
00:44:48,000 --> 00:44:52,000
We're all quite a fun questions.

461
00:44:52,000 --> 00:44:54,000
And that's all for tonight, Penn.

462
00:44:54,000 --> 00:44:55,000
Thank you all for tuning in.

463
00:44:55,000 --> 00:44:57,000
Thank you, Robin, for your help.

464
00:44:57,000 --> 00:45:00,000
Have a good night, everyone.

465
00:45:00,000 --> 00:45:20,000
Thank you, Buffet.

