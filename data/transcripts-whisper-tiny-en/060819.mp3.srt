1
00:00:00,000 --> 00:00:20,560
Today we continue talking about the 38 blessings of the Lord Buddha.

2
00:00:20,560 --> 00:00:30,040
When they are talking about 6 and they will go on to 7 to 10.

3
00:00:30,040 --> 00:00:39,840
The next ten are the next ten are the next ten are close.

4
00:00:39,840 --> 00:00:58,400
The next ten are the next ten are the next ten are the next ten.

5
00:00:58,400 --> 00:01:18,680
The next ten are the next ten are the next ten.

6
00:01:18,680 --> 00:01:44,960
We have a discipline which is well trained to be well trained and disciplined.

7
00:01:44,960 --> 00:02:08,800
The next ten are the next ten are the next ten.

8
00:02:08,800 --> 00:02:23,240
The next ten are the next ten are the next ten.

9
00:02:23,240 --> 00:02:46,280
The next ten are the next ten are the next ten.

10
00:02:46,280 --> 00:02:57,560
Here we understand it to me in the teaching of the Lord Buddha, but actually it could even

11
00:02:57,560 --> 00:03:07,280
be considered a blessing just to have learned a lot in a worldly sense, to say knowledge

12
00:03:07,280 --> 00:03:35,200
is power and whatever realm we work in, we work in business, we work in education,

13
00:03:35,200 --> 00:03:50,080
in any field in the world, the more we have studied what we are doing, the greater

14
00:03:50,080 --> 00:03:58,720
would be our power and our ability to carry out our work in the domains the same, of

15
00:03:58,720 --> 00:04:04,920
course even much more of a blessing, because the domain is that which keeps us from falling

16
00:04:04,920 --> 00:04:09,280
into evil, falling into suffering.

17
00:04:09,280 --> 00:04:15,440
When we have great knowledge or learning about the dhamma, for sure we can much better

18
00:04:15,440 --> 00:04:26,040
put it in the practice, if we learn all 38 of the blessings of the Lord Buddha, we will

19
00:04:26,040 --> 00:04:34,120
very much better off to put them all into practice, better than we know one or two, we

20
00:04:34,120 --> 00:04:41,200
only know the very basics of the Lord Buddha's teaching, if we learn a lot, let the Buddha

21
00:04:41,200 --> 00:04:45,760
teaching it will be much easier for us to put it all into practice, to put the Buddha's

22
00:04:45,760 --> 00:04:50,960
teaching into practice, because we understand it on the more broad level, it's a great

23
00:04:50,960 --> 00:05:05,760
blessing to study, but with study you can also apply it according to the teachers, according

24
00:05:05,760 --> 00:05:13,360
to the teachers on the teachings on great knowledge, great learning, it can also apply

25
00:05:13,360 --> 00:05:23,360
to practice and it can also apply to the results, one gets from practice.

26
00:05:23,360 --> 00:05:32,480
Here we can find it only to book learning or listening to what other people have to say,

27
00:05:32,480 --> 00:05:40,400
but really it can also mean, because the knowledge which we gain from books and from hearing,

28
00:05:40,400 --> 00:05:48,240
is really a very, only very superficial knowledge, it doesn't constitute real understanding

29
00:05:48,240 --> 00:06:02,280
or real learning of a subject, until we put ourselves to the task, we really can't be

30
00:06:02,280 --> 00:06:09,120
expected to bring us any great fruit, so we'll combine it to this, because the next one

31
00:06:09,120 --> 00:06:18,920
is, it means handicraft or the ability to do the work, this supplies more to the realm

32
00:06:18,920 --> 00:06:27,440
of practice, we need the ability to, we need to have the ability to put into practice

33
00:06:27,440 --> 00:06:40,600
whatever we've learned, in the time of the Lord Buddha there were many stories of people

34
00:06:40,600 --> 00:06:49,560
who had studied, but had never put into practice what they had learned, one story is

35
00:06:49,560 --> 00:06:57,720
the story of empty book, the Lord Buddha called him empty book, his name was Pottila which

36
00:06:57,720 --> 00:07:07,920
meant the form of a book in the, the times are a, a palm leaf, a manuscript, something

37
00:07:07,920 --> 00:07:18,120
used to write on, and he had studied the whole of the Buddha's teaching, memorized the

38
00:07:18,120 --> 00:07:29,160
entire teaching of the Lord Buddha, and when he went to see the Lord Buddha, he, he thought

39
00:07:29,160 --> 00:07:37,400
that of course the Lord Buddha would, praise him for his attainment, he had students 500

40
00:07:37,400 --> 00:07:45,800
students or 1000 students, and he thought that the Lord Buddha would for sure have something

41
00:07:45,800 --> 00:07:50,040
good to say about him, so he went to see the Lord Buddha, they respect the Lord Buddha,

42
00:07:50,040 --> 00:07:58,720
the Lord Buddha, so I am coming and said, oh here comes the Lord empty book, there goes

43
00:07:58,720 --> 00:08:07,680
empty book, empty book sitting down, empty book is getting up, they call them by the

44
00:08:07,680 --> 00:08:23,360
name empty book, and Pottila was very ashamed to hear this from the Lord Buddha, and he

45
00:08:23,360 --> 00:08:29,560
realized that it was because he had studied but never practiced, that the study which

46
00:08:29,560 --> 00:08:35,920
he had gained was, was really nothing, was done something that really meant anything,

47
00:08:35,920 --> 00:08:46,720
he was still an empty book, so he went off and tried to find a teacher to teach him, but

48
00:08:46,720 --> 00:08:56,840
no one would teach him until finally he found a novice, a seven year old novice who taught

49
00:08:56,840 --> 00:09:05,000
him meditation, no one else would dare to teach him, they was quite, quite, would be

50
00:09:05,000 --> 00:09:09,640
quite difficult to teach because he knew everything already, people who know everything

51
00:09:09,640 --> 00:09:21,000
are very difficult to teach, but the novice that has had a way of breaking his attachment

52
00:09:21,000 --> 00:09:31,680
to his learning, and in the end he did attain to become an arahant through the practice

53
00:09:31,680 --> 00:09:41,920
of meditation, the sipancha means the ability to practice, the understanding, the skill

54
00:09:41,920 --> 00:09:56,200
and practice, in the world it means being skilled in, skilled in a trade, killed in a

55
00:09:56,200 --> 00:10:14,680
handicraft, skilled in whatever field which one applies oneself to, this is a great blessing

56
00:10:14,680 --> 00:10:24,280
in the world, something which is highly prized, someone who is skilled, someone who has

57
00:10:24,280 --> 00:10:33,680
a skill in what they do, so blessing is something you can take anywhere with us, you have

58
00:10:33,680 --> 00:10:37,080
a business, you can take the business with you, but if you have a skill you can take

59
00:10:37,080 --> 00:10:42,360
it anywhere, there are skills that we have, we can take them anywhere with us, but

60
00:10:42,360 --> 00:10:45,960
a set of business is a great blessing, something we should all strive for, we live in

61
00:10:45,960 --> 00:10:54,560
the world, we should strive to have some skill which is well trained in the blessing, the

62
00:10:54,560 --> 00:11:02,160
meditation practice, in the Buddhist teaching of course it means the skill and meditation

63
00:11:02,160 --> 00:11:10,120
practice, which would have skill and attaining to the states of calm, which are conducive

64
00:11:10,120 --> 00:11:18,680
for insight, meditation, insight, realization, if we really want to be skillful we should

65
00:11:18,680 --> 00:11:25,600
be skillful at attaining magical powers, reading people's minds, remembering past lives,

66
00:11:25,600 --> 00:11:41,640
flying through the air, shape, changing and so on, divine eye, divine ear always see things

67
00:11:41,640 --> 00:11:48,840
far away, hear things far away and so on, but whether we attain to those magical powers,

68
00:11:48,840 --> 00:11:54,640
magical states or not, most of the partners who become skillful and seeing the pre-current

69
00:11:54,640 --> 00:12:02,200
first thing, become skillful and watching the rising and falling, watching the feelings,

70
00:12:02,200 --> 00:12:08,840
watching the mind, watching the mirais and seeing that all the things around us which

71
00:12:08,840 --> 00:12:17,960
used to make us so upset, they really only body and mind, they're impermanent, they're

72
00:12:17,960 --> 00:12:25,240
unsatisfying and they're not under our control, and to let them go, gain skill and attaining

73
00:12:25,240 --> 00:12:33,160
to complete freedom from suffering, skill and attaining what we call peaceful cessation where

74
00:12:33,160 --> 00:12:46,800
the mind let's go where the mind is free, the mind doesn't fall into suffering, it's

75
00:12:46,800 --> 00:12:57,480
skill in talking about skill, skill in skill and practice being able to put into practice

76
00:12:57,480 --> 00:13:14,800
what we've learned, remember 9, so we know you're just to see people as a well-trained

77
00:13:14,800 --> 00:13:26,240
discipline, discipline is the most important foundation of the practice, it's a great

78
00:13:26,240 --> 00:13:34,240
blessing to be skilled, to be well-trained and disciplined, sometimes you even see it in the

79
00:13:34,240 --> 00:13:42,760
world among people who haven't practiced Buddhism, they have a rudimentary sort of discipline

80
00:13:42,760 --> 00:13:54,440
and you see it among soldiers, I even saw it once among an ordinary person in the

81
00:13:54,440 --> 00:14:05,920
world, it was a woman who was from Cambodia, and she was Buddhist, so you can describe

82
00:14:05,920 --> 00:14:14,760
it somewhat to her Buddhist background, but I'd never seen anyone like her, she had a

83
00:14:14,760 --> 00:14:21,280
teacher and she brought her teacher who was a Westerner to come to see us in the monastery,

84
00:14:21,280 --> 00:14:30,040
and her teacher was not very polite, her teacher was not mean but she was somewhat demanding

85
00:14:30,040 --> 00:14:42,280
and her discipline was not very strong, but this student who brought her teacher to see

86
00:14:42,280 --> 00:14:52,040
us in the monastery was probably the most well-discipline person I'd ever met, maybe

87
00:14:52,040 --> 00:15:03,320
ever have met still, she had some sort of training in manners and she was yes-miss, no-miss,

88
00:15:03,320 --> 00:15:11,200
and I helped you miss someone who was very well-trained, very disciplined and would not show

89
00:15:11,200 --> 00:15:19,400
any sort of anger or upset, the way she sat, the way she walked, if someone you could

90
00:15:19,400 --> 00:15:29,600
see would have received great training in discipline, discipline it means to have body

91
00:15:29,600 --> 00:15:37,240
and to have speech which is well-refined, which is well-trained, some people on the other

92
00:15:37,240 --> 00:15:46,720
hand are always moving, always fidgeting about, always saying useless things, always talking

93
00:15:46,720 --> 00:15:57,680
about, and the way they talk is not trained or not polite with discipline, and the practice

94
00:15:57,680 --> 00:16:09,720
of Buddhism, of course it takes on more specific meaning of refraining from evil deeds

95
00:16:09,720 --> 00:16:16,880
with the body and with the speech, because a person can be disciplined and still be able

96
00:16:16,880 --> 00:16:28,560
to be of do evil deeds, for instance soldiers or police officers, even thieves and villains

97
00:16:28,560 --> 00:16:35,240
are able to have certain type of discipline, when it comes to refraining from evil

98
00:16:35,240 --> 00:16:48,800
deeds they might not hesitate.

99
00:16:48,800 --> 00:16:52,800
So when we mean discipline in the Buddha's teaching we're talking about refraining from

100
00:16:52,800 --> 00:17:00,760
deeds which are inappropriate, when we're late people we have to keep five precepts

101
00:17:00,760 --> 00:17:08,160
or eight percent, these are rules which we have to train ourselves in for the purpose

102
00:17:08,160 --> 00:17:14,200
of the attainment of concentration, morality is always discipline is always for the purpose

103
00:17:14,200 --> 00:17:22,000
of attaining to concentration, for monks we have so many rules, but it's the truth is

104
00:17:22,000 --> 00:17:35,880
when we keep all of these rules we can realize that our concentration really improves

105
00:17:35,880 --> 00:17:41,920
when this year I stopped to touch money, as a monk we're not supposed to be touching

106
00:17:41,920 --> 00:17:50,200
money, or supposed to be buying things or involved with touching money so I started to

107
00:17:50,200 --> 00:17:56,400
train myself in this and since I started training you can see clearly that discipline certainly

108
00:17:56,400 --> 00:18:00,720
does lead to concentration, it leads your mind to be fixed and composed, you're going

109
00:18:00,720 --> 00:18:04,800
to have to be worrying or thinking about this or about that, all the things in the

110
00:18:04,800 --> 00:18:13,560
open, surround you in the world, you might become fixed and focused in one place, not restless

111
00:18:13,560 --> 00:18:25,400
or disturbed this before, but even in regards to all of the precepts of five of the

112
00:18:25,400 --> 00:18:37,560
eight or 227 rules that the monks have to keep, the most important, the more important

113
00:18:37,560 --> 00:18:46,360
discipline, is the discipline which comes from the mind which is well trained, when the

114
00:18:46,360 --> 00:18:53,440
mind is not flitting here or flitting there or falling into bad or unwholesome states, the

115
00:18:53,440 --> 00:19:00,040
discipline mind, we talk about disciplining the body in the speech, well people can force

116
00:19:00,040 --> 00:19:08,600
themselves to do that and still be evil people inside, but the discipline of the mind

117
00:19:08,600 --> 00:19:23,360
is just a much more important thing, you have a mind which is well disciplined, we don't

118
00:19:23,360 --> 00:19:30,680
let our mind get angry or we don't get our mind, let our mind become greedy, we don't

119
00:19:30,680 --> 00:19:36,040
let our mind become deluded, we come to see that these things are the cause of suffering

120
00:19:36,040 --> 00:19:45,600
or they're suffering themselves, let go of them, this comes of course from the practice

121
00:19:45,600 --> 00:19:52,360
of meditation which we are all undertaking, this is the most important thing for us is

122
00:19:52,360 --> 00:20:02,040
always to push ourselves to pull ourselves up to the level where we don't get angry

123
00:20:02,040 --> 00:20:09,560
even when people do bad things to us, we want to be better than them, we don't get angry

124
00:20:09,560 --> 00:20:17,440
at them, we send love to them, they may be happy, forgive them, they may be free from suffering

125
00:20:17,440 --> 00:20:33,520
and so on, where our minds don't become upset or even attached to good work, this is called

126
00:20:33,520 --> 00:20:44,000
being the only just to see people who have a well-trained discipline, discipline of mind

127
00:20:44,000 --> 00:20:51,320
and number 10, supa, supa supa, sita, jehwa, whatever words, whatever speech is well-spoken,

128
00:20:51,320 --> 00:21:00,860
this is the greatest, this is a blessing and the greatest, and highest, and number 10

129
00:21:00,860 --> 00:21:13,960
is whatever speech is well-spoken, this is a blessing in the world, this is kind of

130
00:21:13,960 --> 00:21:21,840
funny because sometimes you meet people who can, who's speech is so sweet, you're

131
00:21:21,840 --> 00:21:27,200
called the flatter, someone who says always saying good, you hear them saying good

132
00:21:27,200 --> 00:21:33,440
things. But sometimes it happens that you meet someone who, to your face, they say

133
00:21:33,440 --> 00:21:42,280
so many nice good things. But if you're perceptive, you know that right behind

134
00:21:42,280 --> 00:21:47,400
your back, for sure, they're not saying good things about you. You know why

135
00:21:47,400 --> 00:21:51,200
they're saying it, they're not saying it because they mean it or because of

136
00:21:51,200 --> 00:21:54,280
loving kindness which exists in their heart. They're saying it because they know

137
00:21:54,280 --> 00:22:03,440
when they say it. And it's a way of ingratiating themselves with you.

138
00:22:03,440 --> 00:22:14,480
Sometimes you see people smiling, but you know behind their smile they're

139
00:22:14,480 --> 00:22:29,600
really looking at you as though you're some kind of beast or so on. These kind of

140
00:22:29,600 --> 00:22:35,840
people, what I mean is that sometimes people have sweet words and you think

141
00:22:35,840 --> 00:22:41,360
all that's well said. But really the the intentions behind their speech are

142
00:22:41,360 --> 00:22:49,120
not not also not pure. For speech to really be truly well-spoken just to come

143
00:22:49,120 --> 00:22:55,960
from a pure heart. For instance there was in the Buddhist time there was one

144
00:22:55,960 --> 00:23:09,640
monk lived in the forest and all of his teaching was one story. He had heard

145
00:23:09,640 --> 00:23:14,040
many things, but all he remembered, he kept in mind only one one story of the

146
00:23:14,040 --> 00:23:20,720
Buddha, but he was so good at telling it that every time when he told the

147
00:23:20,720 --> 00:23:27,000
story, when he came to the verse at the end and finished up the story, that all

148
00:23:27,000 --> 00:23:32,440
of the angels and all of the all of them beings living in the forest they would

149
00:23:32,440 --> 00:23:40,200
all sad, who was sad, who in this great noise would ring through the forest.

150
00:23:40,200 --> 00:23:45,440
And this one one day a monk came to visit him and he saw this and he said,

151
00:23:45,440 --> 00:23:51,800
Oh, these people don't know good teaching when they hear it. I'll go up and

152
00:23:51,800 --> 00:23:56,800
give a talk and he talked and talked and talked and talked and gave a long

153
00:23:56,800 --> 00:24:02,280
talk about many different things. There's a monk who had great learning,

154
00:24:02,280 --> 00:24:14,000
vast learning. And with the end he finished this, he finished the talk and he

155
00:24:14,000 --> 00:24:21,200
sat and waited and he did, there was no noise, he didn't hear anything. And the other

156
00:24:21,200 --> 00:24:26,760
monk said yes, yes, the other monk got up on the chair and just said his one

157
00:24:26,760 --> 00:24:31,520
verse, the end verse of the story, immediately the whole forest shook with sad

158
00:24:31,520 --> 00:24:45,680
who's sad. The monk who, because he was given from his heart. When we say things

159
00:24:45,680 --> 00:24:49,280
we have to be sure that our intentions are pure. We can't just say things to

160
00:24:49,280 --> 00:24:55,160
flatter other people. And we can't just be like a parrot repeating the words of

161
00:24:55,160 --> 00:25:01,360
other people. And we wish people good goodness for themselves. We have

162
00:25:01,360 --> 00:25:07,040
to see, do we really wish them goodness. When we teach the teaching of the Lord

163
00:25:07,040 --> 00:25:11,920
Buddha, do we really mean it? Do we really understand the teaching? Otherwise we

164
00:25:11,920 --> 00:25:20,280
can just be like an empty book, like a parrot, like the Lord Buddha said,

165
00:25:20,280 --> 00:25:30,800
someone who feeds, feeds cows, someone who looks at what they call a shepherd,

166
00:25:30,800 --> 00:25:39,880
or a cow herd, someone who looks after, other people's cows. When you look after

167
00:25:39,880 --> 00:25:50,360
other people's cows, you see, you never get, you never get to possess the

168
00:25:50,360 --> 00:25:55,880
fruits of the, of looking after the cow, the fruits of having the milk or

169
00:25:55,880 --> 00:26:04,400
having cheese or butter, the products which come from the cow. If the cows are

170
00:26:04,400 --> 00:26:11,240
not your cows, they are someone else's cows. When you will never get to

171
00:26:11,240 --> 00:26:18,640
taste the fruits of the, the fruits that come from the cow. Buddha said in the

172
00:26:18,640 --> 00:26:22,760
same way, even though someone knows a lot and a lot of the teaching is able to

173
00:26:22,760 --> 00:26:29,280
teach it. If they haven't come to practice or understand the teachings, it's

174
00:26:29,280 --> 00:26:37,920
just like someone who looks after other people's cows. They won't get the true

175
00:26:37,920 --> 00:26:40,880
flavor of the dhamma.

176
00:26:40,880 --> 00:26:54,800
For something to be well spoken, it needs to have, it needs to be at the right

177
00:26:54,800 --> 00:27:02,280
time. Sometimes you say something good, but it's at the wrong time. It has to be

178
00:27:02,280 --> 00:27:10,400
spoken, it has to be the truth. Sometimes we speak nice things, but it's not

179
00:27:10,400 --> 00:27:34,440
true. It has to be appropriate thing to say. It can be something useless or

180
00:27:34,440 --> 00:27:46,560
something which is unhelpful. It has to be said with love, with loving kindness

181
00:27:46,560 --> 00:27:59,960
in one's heart. And so on. There are many factors which need to do something

182
00:27:59,960 --> 00:28:05,880
that lead for something to be well spoken. This is considered a great blessing. It's a blessing

183
00:28:05,880 --> 00:28:10,440
for the person who speaks. It's a blessing for the person who hears it. When we hear the

184
00:28:10,440 --> 00:28:17,840
teachings of the Lord Buddha, it's one of the greatest blessings. When we talk about the

185
00:28:17,840 --> 00:28:22,120
teachings of the Lord Buddha, when we talk about meditation practice, it's one of the

186
00:28:22,120 --> 00:28:28,680
greatest blessings that leads to peace and to happiness in the world. Even animals when they

187
00:28:28,680 --> 00:28:33,720
hear the teachings of the Lord Buddha, because it's so pure and there's nothing mean or

188
00:28:33,720 --> 00:28:39,440
evil being said, their minds can become pure. In the time of the Lord Buddha there's

189
00:28:39,440 --> 00:28:45,760
a story of a frog that was reborn in heaven simply from listening to the Lord Buddha's

190
00:28:45,760 --> 00:28:54,360
teaching. Didn't understand the word, but was sitting, absorbing the sound vibrations.

191
00:28:54,360 --> 00:29:02,240
And as a result of the wholesome mind which arose, as a result of the moment when the

192
00:29:02,240 --> 00:29:15,440
frog died, it was reborn in heaven. So it's considered a blessing. Whatever words are

193
00:29:15,440 --> 00:29:25,440
most spoken, it's considered a blessing as well. This is the blessings number 7 to the 10.

