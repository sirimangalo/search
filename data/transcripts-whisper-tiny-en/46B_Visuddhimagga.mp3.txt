Those defilements that have swab to grow in past or future or present, and the answer is,
they are simply those described as arisen by having swab to grow in.
But yeah, just that.
Are those defilements that have swab to grow in past or future or present?
And the answer is, they are simply those described as arisen by having swab to grow in.
But yeah, just that.
We cannot say that they are present or they are future or they are first.
But yeah, just those deaths have arisen by having swab to grow in.
Now, there are various meanings of arisen that is to say arisen as accurately operating and so on.
So, I know this is exactly simple.
Is this close to saying that they are empty?
No.
We will expect it out, right?
Okay.
Yeah.
So, then the commented brings in four kinds of, what I call arisen.
The part is open up.
So, on the sheet, I get the part in one also.
Four kinds of open up that features arisen.
Four things that are called open up in family.
And the one open up again means that that has arisen.
Now, the first is arisen as actually occurring.
That means all that is reckoned to possess the three moments of arising,
eating and dissolution.
That means it is the real present.
Things.
Because when we say something is present, we mean it has, it possesses three stages of arising
and staying and disappearing or arising, eating and dissolution.
So, this is the real present.
So, this real present is sometimes called open up that which has arisen.
The next one is arisen as being and gone.
That means they have arisen and now they are no more.
They are also called arisen.
And there are two of them.
Whole salmon and wholesome damas that these potatoes and potatoes exist,
which have experienced the stimulus of an object.
That means which have tasted the object, which has featured,
experienced the object, the taste of the object, and disappeared.
So, they are also called arisen as being and gone.
Now, here being means experienced.
The second is also anything conditioned.
That has reached the three moments beginning with arising and has seized.
So, something which has something conditioned, which has come into being and then disappeared.
So, that also is called arisen.
It is having been and gone.
Now, that there arisen by opportunity made.
Now, the past gamma is called arisen by opportunity made.
It was really past.
So, although it is past, it is called arisen.
That means it is still with us or something like that.
Because it has stood.
That is the direct translation.
It has existed inhibiting the result of other gamma,
and has made the opportunity for its own result to arise in the future.
When there is gamma and then it disappears, then it leaves something like potential
in the continuity of beings to give results in the future.
So, when it made the opportunity for its result to arise in the future,
then it inhibits the result of other cameras.
And the second is the future result.
The future result is also called upina, although it has not yet arisen.
So, although it has not yet arisen, it is called arisen.
Because when opportunity arises made, it is sure to arise in the future.
When gamma is, say, accumulated, I mean, gamma is done, then the fruit is sure to arise.
So, the future result can be called arisen, although it is, in fact, it has not yet arisen.
The fourth arisen, by having soil to grow in this, we are concerned with this, the fourth one.
So, arisen, by having soil to grow in.
That means, unholes some gamma, which has not been eradicated with respect to any given soil,
then what is soil here?
So, soil here means the five aggregates and the three planes of becoming, which are the object of Yipasana.
And what has soil means mental defilements, which are capable of arising with respect to those aggregates.
So, what the first element needs is this kind of defilements, arisen by having soil to grow in.
And here, soil means the five aggregates in these planes of becoming.
They are the object of Yipasana meditation.
And what has soil means mental defilements, which are capable of arising with respect to those aggregates.
That means, suppose I see an object, it is a variable object.
And I do not practice Yipasana towards that object.
So, I take it to be beautiful.
Although at that moment, I may have no attachment, but since I have taken it to be beautiful,
I can have attachment with regard to that thing in the future.
So, that kind of attachment or mental defilement is what is called, which is called, I mean,
which is a soil to grow in.
So, with regard to objects, we have to practice Yipasana meditation so that we see their true nature.
So, that we see that they are implemented and so on.
When we have seen that, when we have practiced Yipasana towards that, then the defilements are not said to be in here and in these objects.
But with regard to objects, which we feel to observe by Yipasana, there is always the possibility that defilements will arise.
With regard to those things.
Those defilements, which can arise through not having observed by Yipasana, are called the having soil to grow in.
Actually, what the eradicates is not the present mental defilements, not the past mental defilements, not the future mental defilements,
but it is something like future.
Because some liability in our continuity, that is, when there are conditions, they can arise.
So, that liability is what is eradicated by the per consciousness.
And such the liability is just called having those having the soil to grow in.
That means, in my continuity, let us see, in my continuity of consciousness, they can arise.
So, they have my continuity as a soil to grow in, because I have taken the object to be beautiful, to be permanent, to be present, to be, what about that, to be substantial or to be self.
So, since I have taken this thing to be permanent and so on at any time, the defilement can arise.
So, that liability, that those defilements which can occur through not being observed with Yipasana, protocol, those that have soil to grow in.
Because it has obtained, it has got my continuity, my mind, to grow in, to arise in.
And that is not, and that is not meant objectively.
That means, having the soil to grow in, having the soil means, having the soil not as an object.
But as a place to grow and as a bridge, that is also important.
Because if we say, it is by way of taking an object, that it has got the soil, then it can mean, say, there is an aha.
He said to be beautiful.
So, he meant so on the arghand, and then he had sexual desires for dead arghand.
I mean, he wanted dead arghand to be something of his wife or something like that.
And so, even with regard to the body of an arghand, mental defilements can arise in other persons.
By taking the body of an arghand as an object.
So, if we mean that having the soil means, having the soil as an object, then it will mean that an arghand can eradicate mental defilements of another person.
Because that person has taken the arghand as an object, and then he has attachment arise in his mind.
So, having the soil does not mean having taken as an object.
But having the soil means having got somewhere at some place to grow in or to grow out of.
So, yeah, like the elder who had obtained, like the defilements that arose in the elder Mahathasa,
for not yet. I think it's 806, right?
Like those that arose in the rich man's story with respect to the aggregates of Mahathata.
So, he had wrong desires for Mahakatana.
And in the Brahman student Nanda, with respect to Upalawana, Upalawana was an arghand in Nanda.
And Nanda was a girl.
Nanda fell in love with her. And so, one day when she came back from the sun, then he raped Nanda.
So, having soil means not having soil by way of taking as an object, but by way of having a place, having a base.
Because if we see the defilement which has the soil to grow in by way of taking objects, then it may mean the defilement in another person also.
And nobody can eradicate, even the Buddha cannot eradicate the defilement in another person.
So, I can eradicate defilements in my mind and not in another person's mind.
So, it is to be understood, having soil means having soil not as an object, not as taking as an object.
Having it as its location or something like that.
So, and if what is called arisen by having soil to grow in, no one could abandon the root of becoming because it would be an abandonable.
That means because it belongs to another person.
But arisen by having soil should be understood subjectively with respect to the basis for them in oneself.
That means they should be understood as having the place.
Having the place to live or to exist something like that.
For the defilements that are the root of the realm, in one's world, actually they not fully understood by inside.
So, if we do not practice Vipasana towards themes, then we do not fully understand these objects.
Fully understanding, having seen, they are arriving and disappearing, and they are characteristics.
And also, being able to get rid of matter of defilements with regard to them.
So, for the defilements that are the root of the realm of interior, that the realm in here and in one's own aggregate are not fully understood by inside.
From the instance, those aggregates arise, and that is what should be understood as arisen by having soil to grow in, in the sense of it being an abandoned.
So, in brief what the path abandons or eliminates is just the mental defilements which would arise when there are conditions, which would arise because one has not seen them.
So, seeing them correctly, one has not fully understood them.
So, that liability is what the path consciousness eradicates, and not the real mental defilements arising at the moment.
Because when there are mental defilements, there can be no path consciousness, and when there is path consciousness, there can be no mental defilements.
And then, if you understand this, the other passages are not difficult.
So, and then the other gives another set of four kinds of opina, paragraph 89.
Besides, these there are four other ways of crossing arisen, namely arisen is happening, arisen with apprehension of an object, arisen through non-suppression, and arisen through non-abolition.
So, arisen as happening is the same as one arisen as accurately occurring. That means, it is rightly in existence, with us in three moments.
Now, when an object has had some previous time come into focus in the eye, etc., and development did not arise then, but arose in full force leader on simply because the object had been apprehended.
That means, taken firmly as dominant and so on.
Then, that defilement is called arisen with apprehension of an object.
Like the defilement that arose in the elder Mahapists after seeing the form of a person of the opposite sex while wandering for arms in the village of Kalea and me, or not, not Kalea and me.
So, before there was no defilement in elder Mahapists, but he saw a person of the opposite sex, and then the defilement arose in his mind, so like that.
As long as the defilement is not suppressed by either serenity or insight, though it may not have actually entered the conscious continuity, it is nevertheless called arisen through non-suppression.
That means, it is not really arisen, but it is called arisen, because it has not been suppressed, because there is no cause to prevent its arising.
Even when they are suppressed by serenity or insight, they are still called arisen through non-abolition, because of the necessity for the arising, has not been transcended unless they have been cut off by the path.
That is, those that are not cut off or abandoned by the path consciousness.
So, they are called arisen through non-abolition.
Now, abortion and suppression are different here.
Suppression means keeping them at bay for some time, and abolition means eradicating altogether.
Like the elder who had obtained the attainment and the defilements that arose in him, while he was going to the air on his ceiling, the sound of a woman singing with the street voice as he was gathering flowers and a group of blossoming trees.
So, he had obtained the eight attainment, so that means he has suppressed the mental development by the eight attainment.
And the defilements that arose in him, how he was going to the air.
But he was going to be air, but he heard the woman singing, the flocking flowers, and so the defilements arose in him.
So, such defilements are called, and those that arisen through non-abolition, because they are not abolished, they are not eradicated, they may arise when they are conditioned.
So, there are these four kinds.
And the three kinds, namely arisen with apprehension of an object, arisen through non-suppression, and arisen through non-abolition should be understood as included by four arisen by having soil to grow in.
So, the fourth of the first list corresponds to three of the second list.
So, as regards to the kinds or reasons stated, the four kinds, namely, as actually occurring, as been and gone by opportunity made, and as happening, cannot be abandoned by any knowledge, because they cannot be eliminated by the thoughts.
But the four kinds of illusions, namely, by having soil to grow in with apprehension of an object through non-suppression, and through non-abolition, can all be abandoned, because if given Monday,
also from Monday, knowledge, which arises when it arises, nullifies a given one of these modes of the arisen.
So, when we see the prep abendance, then we mean this second four.
I mean, second four means here, mentioned one of the first and three of the second list.
So, by having soil and with apprehension of an object, through non-suppression, and through non-abolition.
So, what the first erotic is the inherent tendencies, or latent tendencies, and not the ones that have arisen in the consciousness.
Because when they are in the consciousness, then we cannot dedicate them, simply because they are there.
That is why, in the soldiers, or especially in the commonries, it is said, and no part in the rotor.
That means, non-arising in the future is called cessation.
So, cessation of mental defilements means non-arising of them in the future.
Because mental defilements arise and then they disappear by themselves.
You don't have to do anything about them.
They arise and they disappear.
So, the cessation of mental defilements really means not letting them arise again.
So, non-arising in the future is what is called cessation of mental defilements.
And here also, when there is path, it has the power to render them in active or particle.
Non-arising. Now, the full function, the full function is in the single moment.
It is said that the part does the full functions simultaneously.
And what are before?
Now, at the tenths of penetrating to the truth, each one of the full technology is said to exercise full functions in the single moment.
And these are full understanding of the first noble truth, abandoning the second noble truth, realizing the third noble truth, and developing the full noble truth.
So, at one moment, the part consciousness or part knowledge exercises these full functions.
Not one by one, but simultaneously, these full functions are done.
For this is said by the agent, just as the lamp performs full function and so on.
So, the similarly of the lamp is brought here.
And then another matter, 95.
That's the sun when it arises, performs full function and so on.
And then another matter, it both performs full functions.
So, these similes are given to illustrate the full functions, stand by fat consciousness and simultaneously.
And then in paragraph 97.
So, when his knowledge occurs with the full function in a single moment at the time of penetrating the full truth, then the full truth will be single by the present at the center of fullness in 16 ways, and it is said.
So, there are 16 ways or 16 meanings mentioned here, four for each truth.
So, how is there a single penetration of the full truth and the sense of trueness?
There is single penetration of the full truth and the sense of trueness and 16 aspects.
Suffering has the meaning of oppressing, meaning of being formed, meaning of burning or torment and meaning of change.
These are the four meanings of the first noble truth.
And then the second noble truth, the four meanings of second noble truth, meaning of accumulation, meaning of souls, meaning of bondage and meaning of impediment.
And the four meanings of the third is the meaning of escape, meaning of the crucial, meaning of being not formed, and meaning of gentlessness.
And the meaning of the fourth, noble truth is meaning of cause, meaning of outlet, meaning of cause, meaning of sin, and meaning of dominance.
So, these are called the 16 ways of the four noble truths.
And paragraph 98 raises a question here, it may be asked.
Since there are other meanings of suffering, etc. too, such as indices, human and so on.
Why then are only four mention for each?
Now, if you go back to chapter 20, paragraph 18, you will find that there are 40 ways of looking at things as impermanent and so on.
So, the first noble truth has more than four meanings, meaning of disease and the meaning of a tumor and so on.
So, why are they not taken and only before are taken?
Now, we answered that in this context, it is because of what is evident through seeing the other three truths in each case.
Firstly, in the passage beginning here, in what is knowledge of suffering, it is the understanding, the act of understanding that arises contingent upon suffering.
Knowledge of the truth is presented as having a single truth as its object individual.
But in the passage beginning, because he who sees suffering sees also its origin, it is presented as accomplishing its function with respect to the other three truths simultaneously,
that it is making one of them its object.
And it is described as seeing the one each at a time.
But in other passages, then the seeing is presented as occurring at the same time, I mean seeing of four at the same time.
Now, as regards these two contexts, when firstly knowledge makes each true its object seemingly, then
suffering has the characteristic of progressing and as its individual has done so.
In this paragraph, we should we should strike out those in the square brackets.
It is inserted by the translator and it is not warranted by the sub commentary.
Now, paragraph 99, 99, 100 red, 101, 102, almost for our kids.
Yeah, after the likewise, three, three paragraphs after likewise, and after I didn't just remember you.
But 99 also, after then, when suffering is made the object?
No.
If suffering is made the object then, the only thing it will see is the first meaning of the suffering and not the others.
But what the other is explaining here is that when you see the second noble suit of origin,
then the sense of being formed of the first noble suit becomes evident.
So when you see the second truth, then one meaning of the first two becomes evident,
and when you see the third noble truth, another meaning of the first noble suits becomes evident.
And when you see the fourth noble truth, yet another meaning of the first noble should become evident.
That is why these four are mentioned.
I mentioned here.
So when you see the first noble truth,
then the meaning of oppression
is evident.
When you see the second noble truth,
the meaning of the informed of the first noble truth
becomes evident.
When you see the sad number to the meaning
of burning or torment of the first noble truth
becomes evident, and when it is
change becomes evidence. That is why these four meanings are given.
So by their own seeing them individually and also through seeing other truths, the meanings
of the given truth becomes evidence. So these paragraphs explain at this meaning.
So we do not need when suffering is made the object, when such an origin is made the object,
when such an origin is made the object, when such an origin is made the object, when the
process is made the object. And then in paragraph 99, an example is given the S, the beauties
or soldiery's ugliness did to the venerable nanda through seeing the celestial names.
Now, somebody or beauty was said to be very beautiful. She was very light like a beauty
queen. So she was very beautiful and so nanda was so much in love of her. But Voda wanted
to teach him a lesson. So Voda took him to the celestial world and showed celestial names.
So after seeing the celestial names, who is more beautiful, sundry or celestial names,
then he said, or sundry is like a sheep monkey. We saw on our way here something like that.
After seeing the name, the sundry becomes ugly. But actually, the sundry was a beautiful woman.
So here also, cooling fat removes the burning of the defragments. And so suffering in
sense of burning becomes evident through seeing the fat. When you see the cool one,
then the burning of the another thing becomes evident. Okay, that should be the end of today.
