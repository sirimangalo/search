and welcome back to our study of the Dhamapada. Today we're continuing on with verse
252, which reads as follows.
sudha sang wa jaman ye sang at the nang panadhu dha sang, paraisan ye soh wa jani,
opunati yatah bhu sang, atan opanah chah diti, keling wa kita wa satho.
which means easy to see are the faults of others, hard to see our one's own faults.
The faults of others, one windows away, like chaff, but one's own faults. One covers up like a fraud
who covers up their deceit. So this verse was taught in response to a
story, very short story, related by a rich man named Mandika. And most of the
backstory to the verses relating to how Mandika became a rich man. But since it
doesn't really relate to the verse, I'm going to skip it. It's also quite a
fantastical story, I think probably exaggerated, it's to some extent. But the
gist of it is that he was very generous, very kind, very thoughtful, and he
was a great supporter of Buddhism in the time of the Buddha,
vipasi, I think, the time of another Buddha. One time there was a famine in one of his
past lives, and they stored as much rice as they could, but finally they were
down to one pot of rice for him, his wife, his son, his daughter, and his
servant. And so they split it up five ways, but then they saw an enlightened
being, some religious person who exhibited all the qualities of an enlightened
being, and they were so impressed by his countenance that they all decided to
give their meal, their last meal to this religious person, given as an
offering. Really, in a way, as an offering to dedicate to their own well-being,
they used it as a support for their own determination to never have to
starve again. And the story goes that as a result they didn't ever starve again.
So the lesson behind that is, of course, the sort of rudimentary Buddhist
lesson of being generous, and I think we shouldn't ignore that. It doesn't have
much to do with the verse at all, but it is worth mentioning that many of
these stories have some element relating to generosity. They remark upon
the well-being or the fortune of individuals as being a product of
generosity and kindness and goodness in general. And that's an important
Buddhist quality. I mean, it sounds somewhat awkward as a religious person myself
to talk about such things, especially when they relate to generosity towards
religious individuals, but I certainly, as I've said before, subscribe to this
idea just because I'm a monk doesn't mean I don't believe that giving to enlightened
beings, whether they be monastic or not, but even just giving to the
monastic community is a great thing to do, something I do whenever I have the
chance, something we did last weekend with the help of Jeff and with the
help of our organization. For my birthday, I gave food to the monks there,
and that's just a great thing to do. Generosity, people who are generous,
people who give and are kind, they know the power of it. They can appreciate
this importance of this. But the real story behind this verse, it was quite
simple, mendika one day went to see the Buddha. And on his way to see the Buddha,
he was confronted by someone. I can't remember some follower of a different
religion. And he asked, where are you going? And I'm going to see someone to go
to my, and he said, oh, why would you go to see him? You who are a kiriawada? Why
would you go see someone who is a kiriawada? You are a person who is kiriawada.
kiriawada means someone who, well, literally believes in the, believes in action.
And it means the efficacy of action. It means that there is a result to your
actions, or that there is such a thing as an action that is, that is potent. And
Akkiriawada is someone who doesn't believe in the potency of action. And mendika
didn't actually respond as far as we, we read. He just continued on his way to
see the Buddha, but he remarked to the Buddha about this, what this person had
said to him, maybe he wanted to get the Buddha's take on it, or the Buddha's
explanation of whether he was kiriawada or Akkiriawada. But the Buddha turned
around with this verse, focusing more on the, the nature of the conversation
as being confrontational as being somewhat accusative and being focused on
the faults of others. It may very well have been that mendika had a problem
with obsessing over the faults of these individuals, because of course when you
point out someone else's fault, that that's quite an ugly sort of thing to do.
And so it's a fault yourself. Perhaps he was waiting for the Buddha to say
something bad about those people, the Buddha as a result may have taught this
to remind him not to focus on the faults of others. But it's certainly an
indictment of these people who are very much focused on criticizing the Buddha
for being someone who didn't believe in karma, which is very egregious
indictment or claim accusation. So that's the, the first lesson that we can
derive from this story is really about this, this incidental usage of this
dual categorization of being Akkiriawada and kiriawada. So we can ask as a sort of
a preparation to talk about the actual verse, we can ask what is, what is the
Buddha was the Buddha, Akkiriawada is the Buddha kiriawada. So Akkiriawada can be
understood in two ways. And this sort of answers the question as to why these
people would think in the first place that the Buddha was Akkiriawada.
Didn't they think the Buddha didn't they hadn't they ever heard that the Buddha
taught about karma? Well, first of all, the Buddha didn't teach about karma. He
taught about intention or inclination, right? Because in the time of the Buddha
there was an, there was an belief that actual physical actions, ritual actions,
a very specific sort, had potency. If you perform a ritual in the proper way,
well, it wasn't even so much that that results would occur. It was that it was
what God wanted you to do or it's what was right for you to do. It was your duty to do.
If you were a religious person, your duty is to do the rituals because perhaps
because over the course of thousands and thousands years they'd forgotten why,
but they just knew it was their duty. So they did with their parents and grandparents and
ancestors and then if you were of the royal class, it was your duty to fund the rituals,
which is great teaching to perpetuate by the religious people and so on.
And so the Buddha didn't teach against this and this may have been what they meant,
but I assume more likely what they meant is that the Buddha taught against the concept of a soul,
right, because the Buddha taught non-sal. So this can be misunderstood as meaning that the Buddha,
as a result, thought that we are just physical, right, or that we are just automatons,
that there is determinism. So a curioadam might very well be an indictment of the concept of
non-sal, which is a very difficult concept to understand. So it's understandable that these people
made that misunderstanding. So it's quite likely that that's the case, because the curioadam
usually refers to a person who believes that there is a karaka, a self, a soul that acts,
like when we decide to do something, there is a soul that makes that decision.
But of course Buddhism denies not just that idea, but the very framework within which it rests.
So Buddhism, to understand non-sal, Buddhism looks at reality in a different way. We look at
reality from a point of view of experiences. It's not just about taking self out of the equation,
and then whatever's left, you've got just mindless actions. Rather, you have experiences,
and what we would think of as a self is actually just moments of experience and actions,
you know, in mental inclinations that are potent, and there is certainly a potency involved.
So that's the idea of a curioadam, curioadam, curioadam, the Buddha himself did answer the question
of whether he was curioadam, but he answered it in a different way. So in the sense that it's
usually meant the Buddha was curioadam in regards to the idea that there is a potency to our actions,
but he was not curioadam in the sense of believing in an entity. Simply because the Buddha didn't
see the world or the Buddha taught not in the framework of entities, but in the framework of
experiences, which are momentary, and they arise and they cease, and if you start looking at the
world that way, there's no room for the idea of a self. But when the Buddha answered, he said,
yes, there's a way you could say, I'm a curioadam, I don't believe in action. He said, because I teach
people to not do any evil actions. So I teach the non-doing of evil actions, the aqiriya,
the non-action in regards to evil, so a totally different interpretation. He denied being aqiriya
wadam in the ordinary sense. So that's enough of that. In regards to the actual lesson of the
verse, we've heard this lesson or similar lessons to this, the Buddha was often remarking on this
dichotomy of how we relate to the faults of others and how we relate to the faults of our
own faults. And more generally, how we relate to the external to others and how we relate to
ourselves or even more generally, how we relate to the external world and how we relate to the
experiential inner world. I mean, it is an inner world because it's only happening to us. We
own the experience, our own experiences. So I think we can take this on two levels. In regards to the
story, talking about how we approach the issue or the event when someone confronts us or accuses us,
criticizes us, points out our faults. And this verse helps us understand this. It gives us a sort of
a general basic Buddhist lesson in regards to dealing with criticism. It reminds us to
appreciate how prone we are to seek out the faults of others. The imagery the Buddha uses is quite
important as well. If you know anything about winnowing, chaff is the husks of rice. A person
who knows chaff is so obsessed, not with the rice, but with the chaff, that in a big pan of rice,
they will find a way to get the chaff out. When we were picking blueberries, we had these,
we rigged up tweezers because we would pick blueberries and there was the stems and we would
reach in with the tweezers and pull them out. When you're winnowing chaff, we saw them do it in
India, but the idea is you have a big fan and you have a pan of rice and you shake up the rice
and the rice goes up and falls back out of the pan, but when the chaff goes up, it's blown away by
the fan. A person who is skilled at winnowing chaff, they have to get all the chaff out and they
have to be very obsessed with it. When you liken that to a person, winnowing faults,
it's quite apt because it becomes a habit where we overlook all the good qualities of others
and hone in on their faults. This is basically what the Buddha is saying, it becomes a habit.
It's an interesting reminder of this bad habit of humanity that we fall into partiality
based on greed, based on anger, based on delusion, based on fear.
And we spend our time trying to find fault in others. We do it out of greed in order to manipulate
others, sometimes pointing out fault is a means of feeling better about yourself, sometimes it's a
means of ingratiating yourself with others. When you demean others, you can manipulate them to
the point that they rely on you. We do it out of anger because we want to hurt others with our
criticism. We do it out of delusion. For many kinds of delusion, we do it out of wrong view, the
belief that something this person is doing right is wrong, so we criticize them for it. Many people
criticize the Buddha for good things that he did, thinking that they were wrong. We criticize
out of arrogance and conceit. It makes us feel better about ourselves. We're able to pump ourselves up.
We do it out of low self-esteem. We hate ourselves, so we try to bring others down to our level,
try to feel better about ourselves by bringing others down. We do it out of fear, worry, feeling
like we're not adequate, and it assuages our fear by reminding ourselves that others have. Our
minds are perverse in their approach to others. Buddhism is very much about focusing on oneself.
Well, I don't think it's proper to open your faults. You shouldn't be spreading your faults
to others like chaff, showing them the bits of you that are faulty. I don't think it's proper
really good, because if you're constantly telling other people what faults you have, you're
encouraging them to criticize you. Knowing that others are prone to criticism, you really shouldn't
be too concerned or too interested in presenting your faults to others. But we spend far too much
energy trying to hide our faults, and it's really to no avail, because everyone's putting a microscope
on us, anyway. One interesting thing about this verse is, it seems to be contradicting itself,
because the faults of others are not easy, it shouldn't be easy to see, if we're all hiding them.
If everyone's hiding their faults, and we are, it actually is quite difficult to see the faults
of others. We don't know the extent of other people suffering, or they're evil, or they're unwholesomeness.
Someone going to look like a very wholesome, pleasant, good person, kind person, but inside
have great corruption and mental issues. They can be engaged in great evil in private and secret.
But what the verse means, I think, and what it does point out is that we're better able to see
the faults of others for two reasons. And this relates to how we should understand it as
meditators, I think. We're better able to see the faults of others because we're objective
about them. We're not so objective about our faults. We're not so objective about our faults,
because at the time when we are faulty, when we're overwhelmed by greed or anger or delusion,
our minds are not in a clear state. None of these states can arise with clarity of mind,
that's very definition or nature of evil, that it's always associated with darkness, with delusion.
And so because of that, at the time when we are doing something wrong, we really are not in the
position or a state of mind to be able to appreciate the wrongness of it, whereas with others,
because it's not us, because we're not engaged in unwholesome is quite often quite easily.
We can see all the little imperfections of other people's characters.
It is an important lesson to not do that, to change the way we engage with others.
And meditation helps us to see this. I think it's important to understand that enlightened
person doesn't spend much time trying to change others or criticizing others, and that
the vast majority of the times that we are inclined to criticize point out other people's faults
is based on unwholesomeness, based on bad habit, based on even just a desire to change others,
which may seem like a positive thing, but ends up being obsessive and controlling, and usually harmful,
usually our efforts are met with anger and arrogance. Who are you to tell me, it's hard.
It's very hard to rightfully criticize someone. It's very hard to having rightfully criticize
someone, have it actually benefit them. And so one more lesson is, conversely, knowing this,
when people do criticize us, and the Buddha taught this as well, we should try our best to appreciate
that criticism. When others do criticize us, we understand that that's what people are prone to do,
where our whole lives are always going to be criticized. But I said, no one is free from criticism.
And knowing that, we should be prepared for it, expect it, and understand how to deal with criticism.
You know, the many books and non-Buddhist teachers have taught about dealing with criticism.
Your therapist will tell you how to deal with criticism, your parents will tell you.
We talk about having a thick skin, don't be thin skin and so on. But it's important.
It's very important. It's embarrassing as a Buddhist to get angry when someone criticizes you.
As long as we have arrogance and self-worth, our attachment to self-eagle,
we're always going to get upset when others criticize us. So an important Buddhist practice is
to overcome that and to understand the wrongness of reacting negatively to criticism.
Sometimes we want to criticize someone back or so on.
But on a deeper level, in regards to our practice, this is a good example of how
mindfulness benefits both our own mental health and our relationships with others.
Because mindfulness helps you see things just as they are.
It helps you relieve you of this need to change others or change the world around you.
And so as a result, it makes people more comfortable around you. They feel like they can be
themselves. People will admit things to you that they wouldn't do others because they know you're
not judging them. Think of that. Think of that when you think of how good a district
decides and how good you are at pointing out the flaws of others. No one likes that. No one appreciates
that. And most astutely, it's right for someone to say, if you're prone to criticism,
that's a fault of yours. And so you are worthy of criticism when you're prone to criticize other.
But when we're mindful, our experience of others is just our experience of them.
When they say something, it's an experience of sound. When we think about what they've said,
it's an experience of thinking. When we think of who they are, that's all just concepts in our
mind and we see those concepts. So we focus much more on our experience of them.
It doesn't mean we don't know that they're there or we don't understand their situation.
But it does filter and purify that. Purify it because it relates it back to what is actually
happening, not how we interpret it, whether we like it or dislike it, what we want to do to change
it, so on. And so our relationships with others, this is a very good example of how they, again,
they are purified and harmonized. But internally, I think the most important lesson is how we
approach our own faults because we don't just hide them from others, which as I said,
doesn't have to be a negative thing. Now actively hiding them maybe, but
it could even be positive to not show your faults to others because of, again, how it can cause
problems. That's not the real problem. The real problem or the big problem is how we hide them
from ourselves. We hide them from ourselves by rationalizing them. We hide them from ourselves by
understanding them, misunderstanding them. So again, the idea that our faults are not actually
faults. We hide them from ourselves or we ignore them by hating ourselves. We render ourselves
ineffective at dealing with them by saying, yeah, there I go again doing this or doing that.
I'm just a terrible person, which is a useless thing to do. I mean, it's not only hateful. That's
not the biggest problem. The biggest problem is that it makes us ineffectual. It's like an answer.
It's a defense mechanism of sorts because as long as you remind yourself that you're not able to
deal not, you're, you're just such a bad, you're that sort of bad person. If you're that sort of
bad person, well, then yeah, you have no power to change because it's who you are. But it's not
who you are. It's just habit as everything is. So the way meditation changes this and especially
the mantra meditation is on three levels, on the perception level. When you note, for example,
let's look at one of the hindrances. So if you're angry, if you're an angry sort of person,
and you say to yourself, angry, angry, the word focuses you on the experience,
and you see the anger as something that arises and ceases. You see her ceases pretty quickly
because it can't exist with mindfulness. But you see it for what it is. And so that changes your
perception of it. Your perception of your faults changes you from, from maybe getting caught up
in the anger and thinking, yeah, you should do something with this anger. Maybe hurt others or
say things that are unpleasant to others. It changes it on the thought level. You see, the thing
about a noting, a mantra, is it is a thought. It's a mental activity. It's a mental construct. It's
conceptual. It's an act of conceptualizing. And it's meant to be a replacement for the ordinary
conceptualizing that we do. When you're angry, you conceptualize it as, I am angry. And this
person made me angry. And I don't deserve whatever they did to make me angry and so on. Our thought
process, this is the wrongness of our thought process. We are oblivious to the nature of the anger
because we're focused on maybe the other person or whatever it was that caused their anger or whatever
we're going to do about our anger, with our anger. And so when we say to ourselves,
angry, all of that is gone. There is no conceptualizing beyond anger. What is this? How do I
conceive of anger? It's anger, greed. When you want something, what you want, you know, how you're
going to get it gone because there's only the wanting. And finally, on the level of views,
the perversion of views is done away with by as a result of the process of noting,
because as you note and your clarity on the perception level where you perceive things as what they
are, you think of them as they are, your view becomes the observation, the conclusions that you
draw based on your observations as though we're a science experiment because your observations are
so clear. You're able to understand, yes, anger leads to this. This led to the anger. Because I
was unmindful, I allowed this experience to pervert my perception to corrupt my mind and to
delude me into thinking that somehow it was wrong and that I had to fix it and so on. I mean,
it's not even really a thought process. It's just your awareness of what's happening,
seeing the way things go and what are the results and how horrible it is to be an angry person
or a greedy person or a deluded person or so on. So this verse really reminds us of the importance
of meditation. It reminds us of a very important problem that we have of hiding our faults and focusing
too much on the external world, which includes and often is very much centered around our
relationship to others and how we are obsessed about how they're better than us, how they're
worse than us, what's wrong with them, what's right with them and not just people in general,
the world around us as opposed to our experience of the world around us. Again, our focus on
changing and fixing other people is mostly wrong-headed. It can be a very good thing to point out
the faults of others if you do it right, but again, very difficult thing to do. It's important for
friends to do, it's important for parents to do, but it sounds so very, very difficult to do with
any positive outcome both for yourself and the other person. So something not really to be
emphasized. They would have likened it to an accurate acrobat, so we can liken it simply to two
people walking together. If we're walking side by side, you don't say, well, you watch where I'm
walking and I'll watch where you're walking and that way we won't trip over anything would be
absurd to think such a thing. It would be inefficient and most likely ineffectual, whereas if we both
focus on our own feet, look, you focus on your feet, I'll focus on my feet, we'll walk together
in harmony, neither one of us will trip. And if the world would like this, if we all did our duty,
fixed our own problems, clear, cleansed and harmonized, purified our own minds,
the world would be in perfect harmony. We wouldn't have to work on our relationships with others,
our interactions with the world around us, people, places, things, animals, whatever
would be pure, would be free from all of the poisons that we inject into our
experiences. So some thoughts on this verse, a simple verse, a simple teaching, but an important
one, on many levels. So that's the teaching of the Dhamma Padafar tonight. Thank you all for
tuning in. I wish you all the best.
