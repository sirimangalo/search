1
00:00:00,000 --> 00:00:05,280
Difficulties and Meditation Wrong Mindfulness

2
00:00:05,280 --> 00:00:08,320
The bitter thought that mindfulness is always good.

3
00:00:08,320 --> 00:00:10,560
In the Abhidharma it is good.

4
00:00:10,560 --> 00:00:15,600
So Bana Chaita Sika, which means a beautiful mind state.

5
00:00:15,600 --> 00:00:20,800
The Buddha himself did, however, use the term wrong mindfulness at times.

6
00:00:20,800 --> 00:00:24,000
To answer the question of what wrong mindfulness means,

7
00:00:24,000 --> 00:00:28,160
we can begin with a technical definition of mindfulness.

8
00:00:28,160 --> 00:00:30,640
In the traditional terrified attacks,

9
00:00:30,640 --> 00:00:33,280
all Dhammas have four qualities.

10
00:00:33,280 --> 00:00:36,960
Each Dhamma will have a characteristic, a function,

11
00:00:36,960 --> 00:00:41,920
a manifestation, and a proximate or nearby cause.

12
00:00:41,920 --> 00:00:46,400
The characteristic of mindfulness is being unwavering.

13
00:00:46,400 --> 00:00:49,200
The ordinary mind, wobbles and waivers.

14
00:00:49,200 --> 00:00:52,560
It is not stable nor fixed on an object.

15
00:00:52,560 --> 00:00:57,440
Mindfulness enables grasping the object firmly without wavering.

16
00:00:57,440 --> 00:01:01,200
The function of mindfulness is to not forget.

17
00:01:01,200 --> 00:01:06,240
Ordinary mindfulness refers to the ability to recollect things that happened long ago.

18
00:01:06,240 --> 00:01:11,040
Then there is Sati Patana, the mindfulness meditation practice.

19
00:01:11,040 --> 00:01:16,320
The meaning of Sati Patana is to not forget the object in the present moment.

20
00:01:16,320 --> 00:01:19,200
Ordinarily, we experience something momentarily,

21
00:01:19,200 --> 00:01:22,000
and then get caught up in judgment and reaction,

22
00:01:22,000 --> 00:01:24,720
forgetting about the actual object.

23
00:01:24,720 --> 00:01:28,000
We see something, but we are not interested in the seeing,

24
00:01:28,000 --> 00:01:30,240
rather we are interested in what it means.

25
00:01:30,240 --> 00:01:35,680
Wondering, is it good, is it bad, is it me, is it mine?

26
00:01:35,680 --> 00:01:37,920
Mindfulness does not do that.

27
00:01:37,920 --> 00:01:42,000
Mindfulness sticks with the pure experience of the object.

28
00:01:42,000 --> 00:01:44,800
Mindfulness manifests in two ways,

29
00:01:44,800 --> 00:01:50,240
firstly as guarding and secondly as confronting the objective field.

30
00:01:50,240 --> 00:01:54,960
And no ordinary mind is unguarded and the farmers may enter easily.

31
00:01:54,960 --> 00:01:58,240
Mindfulness guards against these departments.

32
00:01:58,240 --> 00:02:01,440
Mindfulness also confronts the objective field.

33
00:02:01,440 --> 00:02:06,320
An ordinary mind is not always able to confront objects of experience.

34
00:02:06,320 --> 00:02:11,040
When an unpleasant experience comes, the ordinary mind shies away from it.

35
00:02:11,040 --> 00:02:16,560
And when a pleasant experience arises, the mind immediately chases after it.

36
00:02:16,560 --> 00:02:20,480
Carding the mind and confronting the object are signs of mindfulness,

37
00:02:20,480 --> 00:02:25,760
which enables one to have positive and negative experiences without reacting.

38
00:02:25,760 --> 00:02:30,480
The proximate cause that gives rise to mindfulness is strong perception.

39
00:02:30,480 --> 00:02:35,520
When you perceive that you are seeing, that perception is called sana.

40
00:02:35,520 --> 00:02:41,200
When you perceive hearing that cat meowing, that perception is called sana.

41
00:02:41,200 --> 00:02:46,960
Theta sama is when you reaffirm the perception and it becomes strengthened.

42
00:02:46,960 --> 00:02:50,400
This is accomplished by reminding yourself of the experience.

43
00:02:50,400 --> 00:02:57,600
As seeing, seeing or hearing, hearing, reminding yourself of what you are experiencing,

44
00:02:57,600 --> 00:03:02,560
strengthens the pure perception of the experience, giving rise to mindfulness.

45
00:03:02,560 --> 00:03:07,680
The text saying that mindfulness is like a pillar because it is firmly founded.

46
00:03:07,680 --> 00:03:13,280
The ordinary mind is like a bull floating on water that flits about here and there.

47
00:03:13,280 --> 00:03:17,280
Mindfulness is like a pillar sunk in the bottom of a lake.

48
00:03:17,280 --> 00:03:22,320
No matter how the wind or water buffets the pillar, it does not shake.

49
00:03:22,320 --> 00:03:25,200
Mindfulness is also said to be like a gatekeeper.

50
00:03:25,200 --> 00:03:31,520
It guards the eye, ear, and the other sense doors where all of our experience comes.

51
00:03:31,520 --> 00:03:36,320
Mindfulness lets experiences enter without letting in the defamments.

52
00:03:36,320 --> 00:03:39,040
What then is wrong mindfulness?

53
00:03:39,040 --> 00:03:42,560
There are four ways we might think of wrong mindfulness.

54
00:03:42,560 --> 00:03:50,720
Unmindfulness, misdirected mindfulness, lapsed mindfulness, and impotent mindfulness.

55
00:03:50,720 --> 00:03:54,240
Unmindfulness is just the opposite of mindfulness.

56
00:03:54,240 --> 00:03:58,720
The characteristic of unmindfulness is being wavering.

57
00:03:58,720 --> 00:04:04,240
Its function is forgetfulness, its manifestation is not confronting

58
00:04:04,240 --> 00:04:08,000
and its proximate cause is weak perception.

59
00:04:08,000 --> 00:04:13,120
If you never go to a meditation center or you never take up the practice of meditation,

60
00:04:13,120 --> 00:04:17,360
you are generally unmindful and therefore your mind wobbles.

61
00:04:17,360 --> 00:04:21,520
Your mind is also forgetful so you cannot remember things that happened yesterday

62
00:04:21,520 --> 00:04:24,240
and you can never remember the present moment.

63
00:04:24,240 --> 00:04:27,760
You experience something and immediately you react.

64
00:04:27,760 --> 00:04:32,000
You do not face objects of experience because of weak perception.

65
00:04:32,000 --> 00:04:36,320
You perceive something but your mind is not trained to stick with the simple perception

66
00:04:36,960 --> 00:04:38,560
so you get lost in your reactions.

67
00:04:39,680 --> 00:04:44,160
Misdirected mindfulness means mindfulness that is focused on the wrong objects.

68
00:04:44,880 --> 00:04:49,680
This type of mindfulness is not intrinsically wrong, just wrong for a specific practice.

69
00:04:50,720 --> 00:04:53,840
If you want to go somewhere you need to go on the right road.

70
00:04:53,840 --> 00:04:55,360
There is nothing wrong with the other roads.

71
00:04:55,920 --> 00:04:58,000
They just do not take you where you are trying to go.

72
00:04:58,000 --> 00:05:02,480
By the same token, if you practice mindfulness of the past,

73
00:05:02,480 --> 00:05:07,040
remembering past life for example, it is never going to allow you to attain enlightenment

74
00:05:07,760 --> 00:05:10,000
because it is focused on the wrong objects.

75
00:05:11,040 --> 00:05:16,240
There is nothing technically wrong with the past, it is just a mundane conceptual object.

76
00:05:17,120 --> 00:05:22,560
Likewise, mindfulness of the future, like when you plan ahead or have an experience

77
00:05:22,560 --> 00:05:25,520
of precognition where you see something before it happens,

78
00:05:25,520 --> 00:05:30,240
could also be considered a form of mindfulness, but it does not help you either

79
00:05:30,240 --> 00:05:34,000
because it is also not focused on actual reality as you experience it.

80
00:05:35,120 --> 00:05:38,400
If you focus on a concept, for example, a candle flame,

81
00:05:39,040 --> 00:05:42,560
eventually you are able to see this object in your mind conceptually.

82
00:05:43,440 --> 00:05:47,680
When this happens, the object is stable, satisfying and even controllable.

83
00:05:48,400 --> 00:05:50,560
You can expand and contract it in your mind.

84
00:05:50,560 --> 00:05:55,600
You can enter into very high states of calm taking a concept as an object,

85
00:05:55,600 --> 00:05:58,400
but it is not going to lead you to enlightenment.

86
00:05:59,200 --> 00:06:05,680
It is not any charm, impermanent, dukha, suffering, or anata, non-self.

87
00:06:06,640 --> 00:06:10,320
You will never see these three characteristics, three mindfulness of concepts,

88
00:06:11,040 --> 00:06:14,560
so for the purposes of enlightenment, it is misdirected.

89
00:06:14,560 --> 00:06:23,200
Lapse to mindfulness is like unmindfulness, except that it arises in someone who is practicing correctly.

90
00:06:24,640 --> 00:06:27,520
We, as meditators, are not robots or machines.

91
00:06:28,560 --> 00:06:32,960
This is not an assembly line where we pass people through and they all come out in light.

92
00:06:33,760 --> 00:06:36,400
Everyone has conditions in which they come to meditation,

93
00:06:37,040 --> 00:06:39,440
and every meditator has a different experience.

94
00:06:39,440 --> 00:06:44,000
Often, this shows itself in positive or negative states,

95
00:06:44,720 --> 00:06:46,640
or in deep states that may have been hidden.

96
00:06:47,920 --> 00:06:53,040
The ten imperfections of insight describe some of the positive states that result from proper practice.

97
00:06:54,080 --> 00:06:56,720
These positive states will not lead to enlightenment,

98
00:06:56,720 --> 00:07:00,800
although sometimes meditators start to think that they will and are led astring.

99
00:07:02,480 --> 00:07:07,600
Impetant mindfulness relates to an absence of the other factors of the noble eightfold path.

100
00:07:07,600 --> 00:07:12,560
If you have wrong with you, you will not see clearly no matter how mindful you are.

101
00:07:13,760 --> 00:07:16,400
If you believe any of the five kandas are the self,

102
00:07:16,960 --> 00:07:20,720
or if you do not believe in karma, or if you think that God created us,

103
00:07:21,600 --> 00:07:25,520
all of these views will prevent you from seeing ordinary experiences clearly.

104
00:07:26,320 --> 00:07:31,360
The same goes for wrong thought, bad intentions or ambitions and cruelty in the mind.

105
00:07:32,240 --> 00:07:34,560
These will all get in the way of mindfulness practice.

106
00:07:34,560 --> 00:07:40,800
If you lie, gossip, or just talk a lot, it is going to get in the way of mindfulness.

107
00:07:41,680 --> 00:07:46,080
The same goes if you are a murderer, or a thief, or if you take drugs or alcohol.

108
00:07:46,880 --> 00:07:51,280
You can try your best of being mindful, but if you are not keeping the five precepts,

109
00:07:51,280 --> 00:07:52,240
you will not see clearly.

110
00:07:53,360 --> 00:07:57,920
I would never guide someone through meditation course if they were not able to keep at least the

111
00:07:57,920 --> 00:08:00,400
five precepts. It is just futile.

112
00:08:00,400 --> 00:08:04,640
If you practice wrong livelihood, making a living from bad things,

113
00:08:05,120 --> 00:08:10,160
or if you practice wrong effort, being lazy, or directing effort towards the cultivation of

114
00:08:10,160 --> 00:08:14,480
unhoseness, all of these will prevent mindfulness from being effective.

115
00:08:15,520 --> 00:08:20,080
If you are unfocused, through wrong concentration, or if you are focused on the wrong things,

116
00:08:20,800 --> 00:08:23,280
it will make whatever mindfulness you have impotent.

117
00:08:24,880 --> 00:08:28,000
The eightfold path must work together with mindfulness.

118
00:08:28,000 --> 00:08:33,280
As mindfulness on his own is not enough, mindfulness is like the key that starts the engine.

119
00:08:33,280 --> 00:08:37,600
He gets everything going, but if your engine is broken, the key does not do much.

120
00:08:38,800 --> 00:08:41,520
So these are what would be considered wrong mindfulness.

121
00:08:42,080 --> 00:08:46,800
The consequences of wrong mindfulness are regression, stagnation, and complication.

122
00:08:47,840 --> 00:08:53,280
Regression happens when you get discouraged, and then instead of gaining wholesome states,

123
00:08:53,280 --> 00:08:58,080
you regress and become unwindful, afraid of, or upset with your practice,

124
00:08:58,960 --> 00:09:02,240
you may think the practice is useless because you are not practicing properly.

125
00:09:03,040 --> 00:09:07,120
This may cause one to leave the practice and Buddhism, and it may even lead one to

126
00:09:07,120 --> 00:09:14,160
disparage Buddhism as useless. Stagnation is another danger, and meditators may practice for years

127
00:09:14,160 --> 00:09:17,120
without progressing if their mindfulness is not well directed.

128
00:09:18,320 --> 00:09:21,760
Some meditators may experience great calm and peace in their practice,

129
00:09:21,760 --> 00:09:26,400
but never gain insight because they fail to cultivate the force at a botana.

130
00:09:27,920 --> 00:09:32,480
Complication is the third consequence of wrong mindfulness, and this is the most dangerous.

131
00:09:33,600 --> 00:09:36,000
Mindfulness is designed to simplify things.

132
00:09:36,560 --> 00:09:40,080
The Buddha said, when you see, let it just be seen.

133
00:09:40,880 --> 00:09:42,400
This is how you should train yourself.

134
00:09:43,200 --> 00:09:45,520
When you are hearing, let it just be hearing.

135
00:09:46,400 --> 00:09:49,680
When you experience something, let it just be the experience.

136
00:09:49,680 --> 00:09:55,840
When you are unmindful, or if you have a distorted state of mindfulness in which one of the path

137
00:09:55,840 --> 00:10:00,800
factors is missing, your perception is complicated rather than simplified.

138
00:10:01,680 --> 00:10:06,800
This can actually lead to mind states that are more tense, stressed, or poisonous,

139
00:10:07,440 --> 00:10:12,560
and through repeated and intensive practice, you may lose your mindfulness entirely and become

140
00:10:12,560 --> 00:10:15,440
temporarily insane, unable to control yourself.

141
00:10:15,440 --> 00:10:20,240
I have seen this happen to meditators, though I have never had it happen to someone

142
00:10:20,240 --> 00:10:21,600
practicing under my guidance.

143
00:10:22,480 --> 00:10:27,200
Without close and proper guidance, some meditators may lose their mindfulness,

144
00:10:27,200 --> 00:10:28,240
and that can be dangerous.

145
00:10:28,240 --> 00:10:45,920
Wrong mindfulness is something you will have to be concerned with.

