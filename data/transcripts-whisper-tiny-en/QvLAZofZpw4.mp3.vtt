WEBVTT

00:00.000 --> 00:15.000
Hi, so in this my final video on why everyone should practice meditation, I'm going to give the number one reason why everyone should practice meditation.

00:15.000 --> 00:28.000
And the number one reason is because meditation simply makes you free, and this is of course the most important reason why everyone should practice meditation.

00:28.000 --> 00:51.000
It's because in our everyday lives we sometimes we think we're free, but more often than not we have to admit to ourselves that we're trapped either in our emotions, our needs, our wants, trapped in our hatreds and our versions,

00:51.000 --> 01:08.000
or we're trapped by our actions, by the things we do, we've trapped ourselves by all of the things that we have chased after, all of the things that we have built up, we're trapped by our debts, we're trapped by the world around us.

01:08.000 --> 01:22.000
Or we're trapped by the results of our actions, we're now trapped in the suffering which comes from addiction, which comes from attachment, which comes from hatred, which comes from animosity and so on.

01:22.000 --> 01:39.000
And so, where we sometimes think that we're free to do whatever we want, we think we live in a free society for instance, that we have the freedom to pursue all of the things that we want to achieve, all of the things that we want to obtain.

01:39.000 --> 01:56.000
And we realize that actually in doing so simply going after, chasing after our hearts, following our hearts as they say, we have become slaves, we have become enslaved to our own mental

01:56.000 --> 02:09.000
defilements from the unwholesome emotions or mind states which we've cultivated, or the actions which come from them, or the results of those actions.

02:09.000 --> 02:25.000
And this is in the tradition of the meditation we say there's a wheel, you have first you have the mind state arise, these unpleasant or unwholesome mind states, and then they lead you to do things.

02:25.000 --> 02:33.000
They lead you to suffering, they lead you to bad deeds to do things which are cause for your own suffering or the suffering or others.

02:33.000 --> 02:35.000
So meditation makes you free.

02:35.000 --> 02:51.000
Once you purify your mind, once you stop doing, stop getting angry, once you're able to control and to see through the anger, see through the greed, see through the addiction to this and to that, then you stop doing these things, you stop chasing after things, your action becomes purified.

02:51.000 --> 03:00.000
And you become free from the need to do this, to do that, to say this, to say that, that you don't cause trouble or suffering for yourself or others.

03:00.000 --> 03:03.000
And as a result, you don't have to face the results of your actions.

03:03.000 --> 03:17.000
So it's this wheel, these three things which you cut off, you cut them off at the point where you stop creating these bad mind states, these unwholesome and skillful and useful mind states.

03:17.000 --> 03:21.000
And as a result, your action becomes pure and the results of your action become pure.

03:21.000 --> 03:33.000
Once the results of your action become pure and there's no more suffering, there's no more unpleasantness or the things that come, you don't see them as unpleasant and the cycle continues.

03:33.000 --> 03:38.000
And you're able to, you react to the results of your actions with mindfulness.

03:38.000 --> 03:46.000
And as a result, you're able to continue to purify and to become free from this cycle of suffering.

03:46.000 --> 04:03.000
So this is the number one reason meditation simply allows you to live your life in a pure, a simple and a peaceful fashion where whatever comes to you, you take it from what it is and you don't make more of it than what it really is.

04:03.000 --> 04:11.000
And as a result, this allows you to be free, free from all sufferings that can exist in the world.

04:11.000 --> 04:16.000
So thanks for tuning in and this is the end of my series on why everyone should meditate.

04:16.000 --> 04:31.000
I hope that you've taken some of these lessons to heart and that you are able to find the time and find the encouragement in your own heart to come to begin to practice.

04:31.000 --> 04:42.000
This teaching which truly and sincerely leads the people who practice it truly and sincerely to freedom from all sufferings that exist in the world.

04:42.000 --> 05:09.000
So thanks again and all the best.

