1
00:00:00,000 --> 00:00:08,000
How to avoid becoming too paranoid about all these rules and precepts.

2
00:00:08,000 --> 00:00:13,000
For example, thinking, did I create any bad karma at this situation today?

3
00:00:13,000 --> 00:00:19,000
According to the rules, this was wrong action, wrong speech, etc.

4
00:00:19,000 --> 00:00:29,000
And end up in an even worse mental state than before.

5
00:00:29,000 --> 00:00:33,000
Well, to cut really to the chase is to focus on the paranoia.

6
00:00:33,000 --> 00:00:34,000
It's great.

7
00:00:34,000 --> 00:00:39,000
I mean, this is a great way to state a question and to look at the problem.

8
00:00:39,000 --> 00:00:45,000
How do I deal with the paranoia rather than saying, how do I stop myself from doing any little bad karma?

9
00:00:45,000 --> 00:00:46,000
Do bad karma?

10
00:00:46,000 --> 00:00:49,000
How do I stop myself?

11
00:00:49,000 --> 00:00:56,000
So, like, actually approaching the question from a paranoid angle, right?

12
00:00:56,000 --> 00:00:57,000
That's horrible.

13
00:00:57,000 --> 00:00:58,000
I can't take it anymore.

14
00:00:58,000 --> 00:01:00,000
I'm committing so much bad action.

15
00:01:00,000 --> 00:01:05,000
Instead, you step back and you look and you say, how do I stop being so paranoid about it?

16
00:01:05,000 --> 00:01:07,000
The first thing, get rid of the first problem.

17
00:01:07,000 --> 00:01:12,000
How do I stop myself from being upset about it?

18
00:01:12,000 --> 00:01:15,000
Because when you're upset, you can't approach the problem.

19
00:01:15,000 --> 00:01:19,000
It gets back to the other question about when you're angry, you can't.

20
00:01:19,000 --> 00:01:25,000
When you're upset about not meditating, it becomes more difficult to get into meditation.

21
00:01:25,000 --> 00:01:43,000
So, yeah, the first step is to focus on the paranoia and to...

22
00:01:43,000 --> 00:01:51,000
And the other part of it is that that's not the entire answer because it's a true...

23
00:01:51,000 --> 00:01:57,000
It's a truism or it's a true part of reality that when you do bad deeds, you feel bad about them.

24
00:01:57,000 --> 00:02:05,000
The only way to avoid feeling bad is to repress the conscience, to repress the natural tendency to feel bad.

25
00:02:05,000 --> 00:02:12,000
This is a natural consequence of bad deeds, to feel uncomfortable.

26
00:02:12,000 --> 00:02:25,000
So, a very important part of stopping feeling bad about them is for sure to not do them.

27
00:02:25,000 --> 00:02:31,000
But I would say it goes in that order that first you deal with the paranoia so that you're able to accept what you're doing.

28
00:02:31,000 --> 00:02:33,000
And to say, yes, this is... I'm doing this.

29
00:02:33,000 --> 00:02:36,000
I'm acting in this way and to be clear about it.

30
00:02:36,000 --> 00:02:41,000
So, you're impartial about it because when you're impartial, you can look at it and you can see...

31
00:02:41,000 --> 00:02:45,000
Oh, yeah, now this is what I'm doing is something horrible, something...

32
00:02:45,000 --> 00:02:48,000
And you feel it for yourself that it's not good.

33
00:02:48,000 --> 00:02:57,000
And when you feel for yourself that it's not good, you naturally give it up.

34
00:02:57,000 --> 00:03:19,000
And that's...

