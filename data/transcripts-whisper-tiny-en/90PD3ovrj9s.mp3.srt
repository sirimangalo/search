1
00:00:00,000 --> 00:00:27,120
Good evening everyone, broadcasting live April, 28th, tonight's talk is about reverence.

2
00:00:27,120 --> 00:00:45,120
As a monk comes and asks the Buddha, I ask them two questions, actually, this quote only gives one.

3
00:00:45,120 --> 00:01:09,120
What is the cause? What is the reason?

4
00:01:09,120 --> 00:01:22,120
And by which, when the tatagata has entered into Parinibhana, the good dhamma doesn't last long, doesn't last.

5
00:01:22,120 --> 00:01:33,120
So this quote only asks, does last? The next question is, what is the cause? What is the reason why it does last?

6
00:01:33,120 --> 00:01:48,120
And the Buddha, this is from the Anguta Anikaya Book of Seven.

7
00:01:48,120 --> 00:02:01,120
And so it's all this to seven things. Seven things that when you pay respect to them reverence.

8
00:02:01,120 --> 00:02:08,120
And the word is gharava, sagarava.

9
00:02:08,120 --> 00:02:16,120
And gharava means heavy. And gharava means heavy.

10
00:02:16,120 --> 00:02:28,120
And gharava actually, the word guru in Sanskrit, guru means teacher.

11
00:02:28,120 --> 00:02:34,120
I believe the origin is from the word heavy.

12
00:02:34,120 --> 00:02:41,120
And gharava means to take something seriously.

13
00:02:41,120 --> 00:02:56,120
If we appreciate and think of as worth something, so we worship in a sentence.

14
00:02:56,120 --> 00:03:08,120
In a literal sentence, we place in a, we place worth on these things.

15
00:03:08,120 --> 00:03:13,120
Then the dhamma will last long after the Buddha goes, the Buddha disappears.

16
00:03:13,120 --> 00:03:22,120
Because when the Buddha is around, it's sattadeva, minasana means a teacher of all beings.

17
00:03:22,120 --> 00:03:29,120
They say, an excelled teacher, an excelled trainer.

18
00:03:29,120 --> 00:03:35,120
So when he's around, there's no question, the dhamma will last.

19
00:03:35,120 --> 00:03:42,120
When he's gone, however, we've got left is, well, the arahan disciples of the Buddha

20
00:03:42,120 --> 00:03:49,120
and then they pass away, and then they're hopefully more arahants come.

21
00:03:49,120 --> 00:03:55,120
The question is, what keeps the lineage going? What keeps the chain going?

22
00:03:55,120 --> 00:04:02,120
And so it's souptas like this that are of great interest to those of us in later generations.

23
00:04:02,120 --> 00:04:08,120
Because it gives us an idea of how we support not only our own practice,

24
00:04:08,120 --> 00:04:17,120
but how we carry on the legacy of the Buddha, pass on the legacy,

25
00:04:17,120 --> 00:04:20,120
the future generations as well.

26
00:04:20,120 --> 00:04:27,120
Now we take, take what was given to us and cherish it and nurture it,

27
00:04:27,120 --> 00:04:30,120
and keep it alive.

28
00:04:30,120 --> 00:04:34,120
It's like we're given a scene to a great tree,

29
00:04:34,120 --> 00:04:42,120
and it's up to us to plant it and to cultivate it for future generations.

30
00:04:42,120 --> 00:04:48,120
The dhamma is like that, it's something, or it's like a fire in ancient times in cave time,

31
00:04:48,120 --> 00:04:51,120
in ancient ancient times.

32
00:04:51,120 --> 00:05:00,120
They had to carry a coal with them from campfire to campfire.

33
00:05:00,120 --> 00:05:04,120
It's just a way to keep fire, it was to carry something.

34
00:05:04,120 --> 00:05:08,120
And if the fire went out and in trouble,

35
00:05:08,120 --> 00:05:14,120
it had a fire carrier. I read a story about it once anyway.

36
00:05:14,120 --> 00:05:21,120
Yeah, you have to, we have to care for dhamma.

37
00:05:21,120 --> 00:05:24,120
So how do we care for the dhamma?

38
00:05:24,120 --> 00:05:32,120
What are the ways by which we ensure that

39
00:05:32,120 --> 00:05:36,120
the stitching will continue?

40
00:05:36,120 --> 00:05:38,120
So the Buddha gives seven things.

41
00:05:38,120 --> 00:05:42,120
We take these things seriously and appreciate them,

42
00:05:42,120 --> 00:05:45,120
and in the sense worship them,

43
00:05:45,120 --> 00:05:47,120
but worship, not in the way we use it,

44
00:05:47,120 --> 00:05:52,120
just assign it some worth.

45
00:05:52,120 --> 00:05:55,120
And of course, the first three are the Buddha, the dhamma,

46
00:05:55,120 --> 00:05:57,120
the sanga. The first one is the sata.

47
00:05:57,120 --> 00:06:00,120
Sata means teacher, but in Buddhism,

48
00:06:00,120 --> 00:06:04,120
usually refers to the Buddha, and here that's what it refers to.

49
00:06:04,120 --> 00:06:12,120
It's a special word that is generally only used for the Buddha.

50
00:06:12,120 --> 00:06:23,120
It literally means teacher, something like teacher.

51
00:06:23,120 --> 00:06:38,120
And so if a monk or a bikho or a bikuni, a male or female monk,

52
00:06:38,120 --> 00:06:42,120
that's just anyone who's a man or a woman,

53
00:06:42,120 --> 00:06:46,120
and anyone who's in between, anyone at all,

54
00:06:46,120 --> 00:06:53,120
if they do not take the teacher seriously

55
00:06:53,120 --> 00:06:59,120
and assign worth and appreciation to the teacher,

56
00:06:59,120 --> 00:07:03,120
or the dhamma or the sanga.

57
00:07:03,120 --> 00:07:10,120
They do not dwell agaraw, agarawawarawaranti.

58
00:07:10,120 --> 00:07:18,120
They dwell without reverence, without appreciation,

59
00:07:18,120 --> 00:07:23,120
without taking seriously.

60
00:07:23,120 --> 00:07:32,120
And then the fourth is sika, the sika, the sika,

61
00:07:32,120 --> 00:07:37,120
the training, sika.

62
00:07:37,120 --> 00:07:44,120
Sika means the training, so the training in a general sense,

63
00:07:44,120 --> 00:07:59,120
the training, in giving up, and the training in morality,

64
00:07:59,120 --> 00:08:05,120
the training in concentration and wisdom.

65
00:08:05,120 --> 00:08:12,120
But here's specifically just the act of the things that we do,

66
00:08:12,120 --> 00:08:16,120
like practicing meditation and keeping preset,

67
00:08:16,120 --> 00:08:21,120
so the things that we do and the things that we don't do.

68
00:08:21,120 --> 00:08:27,120
We abstain from certain things and we take on certain behaviors,

69
00:08:27,120 --> 00:08:32,120
certain practices, just the training,

70
00:08:32,120 --> 00:08:36,120
the learning, studying, listening to the dhamma,

71
00:08:36,120 --> 00:08:40,120
remembering the dhamma, reciting the dhamma,

72
00:08:40,120 --> 00:08:44,120
thinking about the dhamma, all of these things.

73
00:08:44,120 --> 00:08:47,120
This is all the training that we do.

74
00:08:47,120 --> 00:08:48,120
Taking that seriously.

75
00:08:48,120 --> 00:08:51,120
We don't take that seriously.

76
00:08:51,120 --> 00:08:54,120
And number five is samadhi.

77
00:08:54,120 --> 00:08:57,120
We don't take concentration seriously.

78
00:08:57,120 --> 00:09:02,120
We go to spells it out explicitly, concentration.

79
00:09:02,120 --> 00:09:05,120
Samadhi is an interesting word.

80
00:09:05,120 --> 00:09:07,120
It could just be focused.

81
00:09:07,120 --> 00:09:13,120
Samas is like same.

82
00:09:13,120 --> 00:09:16,120
It means level or even.

83
00:09:16,120 --> 00:09:19,120
So not too much, not too little.

84
00:09:19,120 --> 00:09:22,120
When your mind gets perfectly focused,

85
00:09:22,120 --> 00:09:25,120
that's why it's kind of like focused rather than concentration,

86
00:09:25,120 --> 00:09:27,120
because it's like a camera lens.

87
00:09:27,120 --> 00:09:30,120
If you focus too much, it gets blurry.

88
00:09:30,120 --> 00:09:32,120
Too little also blurry.

89
00:09:32,120 --> 00:09:34,120
You need perfect focus.

90
00:09:34,120 --> 00:09:37,120
And then you can see.

91
00:09:37,120 --> 00:09:39,120
The mind has to be balanced.

92
00:09:39,120 --> 00:09:42,120
There's really what samadhi is all about.

93
00:09:42,120 --> 00:09:46,120
People think of samadhi as being concentrated.

94
00:09:46,120 --> 00:09:51,120
So no thoughts focused only on one thing.

95
00:09:51,120 --> 00:09:53,120
No distractions.

96
00:09:53,120 --> 00:09:55,120
But it's not necessarily that.

97
00:09:55,120 --> 00:09:59,120
It just means the mind that is seeing things clearly.

98
00:09:59,120 --> 00:10:03,120
The mind that is focused on something.

99
00:10:03,120 --> 00:10:04,120
As it is.

100
00:10:04,120 --> 00:10:06,120
In that moment.

101
00:10:06,120 --> 00:10:09,120
You don't have to block everything out

102
00:10:09,120 --> 00:10:11,120
and just concentrate on a single thing.

103
00:10:11,120 --> 00:10:15,120
You have to be focused on whatever arises.

104
00:10:15,120 --> 00:10:19,120
Clearly seeing it clearly.

105
00:10:19,120 --> 00:10:25,120
That's why we use this word when we say to ourselves seeing or hearing or rising.

106
00:10:25,120 --> 00:10:28,120
We're trying to focus.

107
00:10:28,120 --> 00:10:32,120
Focus means on this, the core of it.

108
00:10:32,120 --> 00:10:35,120
The core of this is rising.

109
00:10:35,120 --> 00:10:38,120
Whatever rising means is falling.

110
00:10:38,120 --> 00:10:41,120
The core of seeing is seeing, the core of pain is pain.

111
00:10:41,120 --> 00:10:46,120
But there's no judgments or reactions or anything.

112
00:10:46,120 --> 00:10:56,120
I'll get rid of all of those.

113
00:10:56,120 --> 00:11:02,120
Number six is a pamadhi.

114
00:11:02,120 --> 00:11:05,120
A pamadhi.

115
00:11:05,120 --> 00:11:10,120
A pamadhi.

116
00:11:10,120 --> 00:11:19,120
The last words of the Buddha is that we should cultivate a pamadhi.

117
00:11:19,120 --> 00:11:22,120
Pamadhi comes from the root mud.

118
00:11:22,120 --> 00:11:24,120
Mud means to be drunk.

119
00:11:24,120 --> 00:11:28,120
Pamadhi is like really drunk.

120
00:11:28,120 --> 00:11:33,120
But it's just as prefix that oneifies it that augments it.

121
00:11:33,120 --> 00:11:47,120
So pamadhi is like an negligent or intoxicated.

122
00:11:47,120 --> 00:11:50,120
Mixed up in the mind.

123
00:11:50,120 --> 00:11:58,120
A pamadhi is to be clear-minded, to be unentoxicated, to be sober.

124
00:11:58,120 --> 00:12:07,120
So drunk on lust, drunk on anger, drunk on delusion, or arrogance, or conceit.

125
00:12:07,120 --> 00:12:11,120
To not be drunk on any of these emotions.

126
00:12:11,120 --> 00:12:12,120
So we should take that seriously.

127
00:12:12,120 --> 00:12:14,120
We should appreciate that.

128
00:12:14,120 --> 00:12:16,120
We don't appreciate that.

129
00:12:16,120 --> 00:12:20,120
And so the Buddha says.

130
00:12:20,120 --> 00:12:31,120
And the seventh one is quite curious.

131
00:12:31,120 --> 00:12:33,120
Curious in a good way.

132
00:12:33,120 --> 00:12:36,120
It's just so surprising, I think.

133
00:12:36,120 --> 00:12:39,120
It's not what you'd expect as the last one.

134
00:12:39,120 --> 00:12:41,120
But the santara.

135
00:12:41,120 --> 00:12:44,120
Ajahn, my teacher, talked about it.

136
00:12:44,120 --> 00:12:50,120
I think he mentions this.

137
00:12:50,120 --> 00:12:56,120
Many times the Buddha talks about the santara.

138
00:12:56,120 --> 00:13:00,120
Pati santari and garawa.

139
00:13:00,120 --> 00:13:03,120
Pati santari, agarawa.

140
00:13:03,120 --> 00:13:07,120
If we don't take seriously or appreciate the santara.

141
00:13:07,120 --> 00:13:20,120
The santara is quite know the etymology, but it means hospitality.

142
00:13:20,120 --> 00:13:23,120
Or it could mean friendliness.

143
00:13:23,120 --> 00:13:26,120
It could mean goodwill, friendship.

144
00:13:26,120 --> 00:13:33,120
But it's really just hospitality when you welcome people.

145
00:13:33,120 --> 00:13:37,120
But in this context, it makes perfect sense.

146
00:13:37,120 --> 00:13:40,120
If you have a meditation center where you don't welcome people

147
00:13:40,120 --> 00:13:45,120
when someone walks in, they don't know.

148
00:13:45,120 --> 00:13:47,120
Everyone looks at them.

149
00:13:47,120 --> 00:13:50,120
What are you doing here?

150
00:13:50,120 --> 00:13:54,120
Sometimes you go to a monastery and no one wants to talk to you.

151
00:13:54,120 --> 00:13:56,120
Go to a meditation center.

152
00:13:56,120 --> 00:14:03,120
If people are kind of putting on errors,

153
00:14:03,120 --> 00:14:05,120
they're real meditators or something.

154
00:14:05,120 --> 00:14:07,120
Or if they just don't care.

155
00:14:07,120 --> 00:14:11,120
And they're just concerned about their own practice.

156
00:14:11,120 --> 00:14:17,120
If such a place existed, then how would you ever share?

157
00:14:17,120 --> 00:14:22,120
How would you ever lead to spreading the dhamma?

158
00:14:22,120 --> 00:14:28,120
Well, I've heard, there are places where you go into the office.

159
00:14:28,120 --> 00:14:32,120
The meditations center office and they just yell at you as well.

160
00:14:32,120 --> 00:14:44,120
They look down upon you and they don't know how to deal with people.

161
00:14:44,120 --> 00:14:46,120
And I've seen that I've talked with other people.

162
00:14:46,120 --> 00:14:50,120
In Chantang, the people in the office would rotate.

163
00:14:50,120 --> 00:14:54,120
And so you had to catch the right person in the office.

164
00:14:54,120 --> 00:14:56,120
You never know what was going to happen.

165
00:14:56,120 --> 00:15:00,120
You want to book a room for someone and maybe the yell at you.

166
00:15:00,120 --> 00:15:01,120
They're very nice.

167
00:15:01,120 --> 00:15:08,120
You have to find the right person and talk to them.

168
00:15:08,120 --> 00:15:09,120
It's very important.

169
00:15:09,120 --> 00:15:11,120
So it's something for us to remember.

170
00:15:11,120 --> 00:15:13,120
You have to welcome them.

171
00:15:13,120 --> 00:15:16,120
And in general, you could talk about this.

172
00:15:16,120 --> 00:15:21,120
The first thing I was thinking about is it'd be neat someday

173
00:15:21,120 --> 00:15:26,120
if our community grows.

174
00:15:26,120 --> 00:15:31,120
If we could have groups of people,

175
00:15:31,120 --> 00:15:35,120
like maybe once a week,

176
00:15:35,120 --> 00:15:40,120
we could have kind of a more formal online session.

177
00:15:40,120 --> 00:15:48,120
And if someone was organizing a group in their home,

178
00:15:48,120 --> 00:15:54,120
then they could join the hangout with their group.

179
00:15:54,120 --> 00:15:56,120
Wow, wouldn't that be neat?

180
00:15:56,120 --> 00:15:58,120
That's what we should do.

181
00:15:58,120 --> 00:16:04,120
Once a week, we could do once a month to start.

182
00:16:04,120 --> 00:16:11,120
We already have groups of people.

183
00:16:11,120 --> 00:16:14,120
And someone puts together a group in their home.

184
00:16:14,120 --> 00:16:17,120
And people come to their home.

185
00:16:17,120 --> 00:16:20,120
And they join the hangout.

186
00:16:20,120 --> 00:16:26,120
And we could have up to 10 groups.

187
00:16:26,120 --> 00:16:29,120
And we have to talk about that.

188
00:16:29,120 --> 00:16:32,120
So the idea is to welcome people even into your home.

189
00:16:32,120 --> 00:16:34,120
People do this.

190
00:16:34,120 --> 00:16:36,120
They have a dumb group in their home.

191
00:16:36,120 --> 00:16:41,120
They've got space.

192
00:16:41,120 --> 00:16:46,120
And then they set up.

193
00:16:46,120 --> 00:16:49,120
Sometimes they just meditate together.

194
00:16:49,120 --> 00:16:51,120
Sometimes they listen to a talk.

195
00:16:51,120 --> 00:16:55,120
I know dumb groups that just put on some CD or something,

196
00:16:55,120 --> 00:16:57,120
somebody giving a talk.

197
00:16:57,120 --> 00:17:01,120
And everybody listens and then they do meditation together.

198
00:17:01,120 --> 00:17:06,120
So but we could do something like that.

199
00:17:06,120 --> 00:17:08,120
But it could be live.

200
00:17:08,120 --> 00:17:10,120
Live from all over the world.

201
00:17:10,120 --> 00:17:12,120
10 different groups.

202
00:17:12,120 --> 00:17:16,120
That's the maximum you can have 10 people in the hangout.

203
00:17:16,120 --> 00:17:20,120
So if anybody's interested in setting up a dumb group,

204
00:17:20,120 --> 00:17:22,120
let me know.

205
00:17:22,120 --> 00:17:27,120
And we'll try and arrange something where we meet together like this,

206
00:17:27,120 --> 00:17:34,120
but we'll have a special day where I'll give a real talk.

207
00:17:34,120 --> 00:17:37,120
I'll give a longer talk.

208
00:17:37,120 --> 00:17:44,120
And then we'll connect.

209
00:17:44,120 --> 00:17:45,120
Right.

210
00:17:45,120 --> 00:17:47,120
So those are the seven.

211
00:17:47,120 --> 00:17:53,120
That's one list of seven things that need to lead to the dumb

212
00:17:53,120 --> 00:17:58,120
lasting. So if we don't spread it, if we don't share it,

213
00:17:58,120 --> 00:18:01,120
if we're not welcoming of people who want to learn.

214
00:18:01,120 --> 00:18:05,120
And the Buddha wasn't big on spreading the dominant in terms of

215
00:18:05,120 --> 00:18:08,120
going around teaching people.

216
00:18:08,120 --> 00:18:11,120
But much more about, as far as I can see,

217
00:18:11,120 --> 00:18:14,120
much more about welcoming people who wanted to learn.

218
00:18:14,120 --> 00:18:16,120
And people wanted to learn.

219
00:18:16,120 --> 00:18:20,120
It was all about finding ways to accommodate them.

220
00:18:20,120 --> 00:18:23,120
And if they do come, if they don't come, don't have to learn

221
00:18:23,120 --> 00:18:25,120
of our way looking for people.

222
00:18:25,120 --> 00:18:33,120
We're not trying to push it this on anybody.

223
00:18:33,120 --> 00:18:35,120
That's kind of how beautiful it is.

224
00:18:35,120 --> 00:18:36,120
We don't need students.

225
00:18:36,120 --> 00:18:38,120
We're not looking for students.

226
00:18:38,120 --> 00:18:42,120
And just people who are looking for it,

227
00:18:42,120 --> 00:18:46,120
we open the door for them, provide them the opportunity.

228
00:18:46,120 --> 00:18:51,120
Tonight a woman came to visit. She lived in Cambodia for nine years.

229
00:18:51,120 --> 00:18:55,120
And she just came and she's seen some of my videos.

230
00:18:55,120 --> 00:18:58,120
And I gave her the booklet.

231
00:18:58,120 --> 00:19:02,120
And then she just did meditation.

232
00:19:02,120 --> 00:19:03,120
I didn't meditate with her.

233
00:19:03,120 --> 00:19:05,120
I came upstairs and did my...

234
00:19:05,120 --> 00:19:06,120
Because I do walking.

235
00:19:06,120 --> 00:19:08,120
She didn't want to do walking.

236
00:19:08,120 --> 00:19:10,120
So...

237
00:19:10,120 --> 00:19:14,120
But it'd be nice if we could have a group here.

238
00:19:14,120 --> 00:19:16,120
Here as well.

239
00:19:16,120 --> 00:19:19,120
I think probably what we do is just have people come up here at nine.

240
00:19:19,120 --> 00:19:22,120
If someone wants to hear the Dhamma, they can come at nine.

241
00:19:22,120 --> 00:19:24,120
Come sit up with us here.

242
00:19:28,120 --> 00:19:30,120
For now anyway.

243
00:19:30,120 --> 00:19:34,120
We're looking to get a bigger place.

244
00:19:34,120 --> 00:19:37,120
Anyway, so these seven, the Buddha, the Dhamma, the Sangha,

245
00:19:37,120 --> 00:19:38,120
take them seriously.

246
00:19:38,120 --> 00:19:39,120
That's another thing.

247
00:19:39,120 --> 00:19:42,120
Often people don't take the Buddha too seriously.

248
00:19:42,120 --> 00:19:47,120
They hear these talks about people who spit on Buddha images

249
00:19:47,120 --> 00:19:52,120
and burn them and just treat them like rubbish.

250
00:19:52,120 --> 00:19:56,120
Thinking that's not the real Buddha.

251
00:19:56,120 --> 00:19:58,120
But there's something to it.

252
00:19:58,120 --> 00:20:00,120
If you don't take the Buddha image seriously,

253
00:20:00,120 --> 00:20:04,120
what does that say about how you feel about the Buddha?

254
00:20:04,120 --> 00:20:09,120
Yeah, people say it's just an image in the Buddha's and whatever.

255
00:20:09,120 --> 00:20:12,120
And there's something to images. They represent something.

256
00:20:12,120 --> 00:20:15,120
In ancient times, they wouldn't even make images

257
00:20:15,120 --> 00:20:17,120
because they revered the Buddha.

258
00:20:17,120 --> 00:20:20,120
It seems because they revered the Buddha so much.

259
00:20:22,120 --> 00:20:26,120
So when we have these images, we try to treat them quite carefully

260
00:20:26,120 --> 00:20:29,120
because we respect the Buddha so much.

261
00:20:29,120 --> 00:20:32,120
If you don't, I mean, this is the thing.

262
00:20:32,120 --> 00:20:35,120
It's the Dhamma won't last because there's no figure.

263
00:20:35,120 --> 00:20:40,120
There's none of this sort of religious feeling

264
00:20:40,120 --> 00:20:42,120
that keeps things together.

265
00:20:42,120 --> 00:20:47,120
The sense of urgency, the sense of zeal and interest.

266
00:20:47,120 --> 00:20:49,120
That's so powerful in any religion.

267
00:20:49,120 --> 00:20:52,120
It could be for the purposes of evil.

268
00:20:52,120 --> 00:20:54,120
It could be for the purposes of good.

269
00:20:54,120 --> 00:20:56,120
But it's a power.

270
00:20:56,120 --> 00:20:58,120
If you don't take these things seriously,

271
00:20:58,120 --> 00:21:01,120
I don't appreciate them, don't revere them.

272
00:21:01,120 --> 00:21:05,120
Very hard to keep going.

273
00:21:05,120 --> 00:21:07,120
They talk about secular Buddhism.

274
00:21:07,120 --> 00:21:09,120
Yeah, it's fine.

275
00:21:09,120 --> 00:21:13,120
But it's hard to get that feeling and appreciation.

276
00:21:13,120 --> 00:21:16,120
Part of religion is the feeling.

277
00:21:16,120 --> 00:21:21,120
The sense of reverence,

278
00:21:21,120 --> 00:21:23,120
appreciation.

279
00:21:23,120 --> 00:21:26,120
Not just taking something clinically in terms of,

280
00:21:26,120 --> 00:21:28,120
yes, this helps me relieve stress,

281
00:21:28,120 --> 00:21:33,120
but, yes, this is the teaching of the perfectly enlightened Buddha.

282
00:21:33,120 --> 00:21:36,120
It carries a lot more weight.

283
00:21:36,120 --> 00:21:44,120
And then the training, concentration,

284
00:21:44,120 --> 00:21:52,120
epamada, which is vigilance or sobriety,

285
00:21:52,120 --> 00:21:55,120
and hospitality.

286
00:21:55,120 --> 00:21:58,120
We have to take the training seriously,

287
00:21:58,120 --> 00:21:59,120
both study and practice.

288
00:21:59,120 --> 00:22:01,120
We have to take the concentration.

289
00:22:01,120 --> 00:22:04,120
Remember to try and be concentrated.

290
00:22:04,120 --> 00:22:06,120
Remember in practice.

291
00:22:06,120 --> 00:22:09,120
Not just study.

292
00:22:09,120 --> 00:22:12,120
And be mindful.

293
00:22:12,120 --> 00:22:14,120
Epamada, actually, being sober,

294
00:22:14,120 --> 00:22:17,120
it's the Buddha said it's a synonym for being mindful.

295
00:22:17,120 --> 00:22:20,120
So this really means using mindful.

296
00:22:20,120 --> 00:22:23,120
Seeing things clearly as they are.

297
00:22:23,120 --> 00:22:25,120
Grasping things as they are.

298
00:22:25,120 --> 00:22:29,120
Remembering things as they are.

299
00:22:29,120 --> 00:22:31,120
And finally, we have to be hospitable.

300
00:22:31,120 --> 00:22:34,120
So we have to welcome people to join.

301
00:22:34,120 --> 00:22:37,120
Not just practicing for ourselves,

302
00:22:37,120 --> 00:22:39,120
but providing the opportunity

303
00:22:39,120 --> 00:22:43,120
and being friendly and welcoming.

304
00:22:43,120 --> 00:22:45,120
Don't just shy away and say,

305
00:22:45,120 --> 00:22:46,120
oh, I'm not a teacher.

306
00:22:46,120 --> 00:22:47,120
I can't teach you the done.

307
00:22:47,120 --> 00:22:49,120
I only want to teach.

308
00:22:49,120 --> 00:22:52,120
You teach how you were taught, pass it on.

309
00:22:52,120 --> 00:22:55,120
It doesn't mean you have to answer all of their questions

310
00:22:55,120 --> 00:22:57,120
and problems and give them advice.

311
00:22:57,120 --> 00:22:59,120
It just means you have to explain to them

312
00:22:59,120 --> 00:23:03,120
how to do meditation, which is quite simple.

313
00:23:03,120 --> 00:23:06,120
And just reassure them that it has benefits,

314
00:23:06,120 --> 00:23:07,120
and if they try it,

315
00:23:07,120 --> 00:23:10,120
they will see the benefits for themselves.

316
00:23:10,120 --> 00:23:15,120
That's all you need to do.

317
00:23:15,120 --> 00:23:22,120
So, my mom, that's the demo for tonight.

318
00:23:22,120 --> 00:23:31,120
And you guys can go.

319
00:23:31,120 --> 00:23:49,120
Okay, Larry has a question.

320
00:23:49,120 --> 00:23:53,120
Well, noting posture, movements, intentions.

321
00:23:53,120 --> 00:23:58,120
I might lapse into contemplating death or contemplating my good fortune.

322
00:23:58,120 --> 00:24:01,120
Then I realize I'm contemplating and not noting.

323
00:24:01,120 --> 00:24:04,120
How should one's balance the process of noting

324
00:24:04,120 --> 00:24:08,120
and the process of some contemplation?

325
00:24:08,120 --> 00:24:12,120
Well, if you are, if that thought arises,

326
00:24:12,120 --> 00:24:13,120
that's fine.

327
00:24:13,120 --> 00:24:14,120
You can do both.

328
00:24:14,120 --> 00:24:15,120
You just have the thought,

329
00:24:15,120 --> 00:24:16,120
and then you say thinking,

330
00:24:16,120 --> 00:24:18,120
thinking, or if you feel happy,

331
00:24:18,120 --> 00:24:24,120
you can say happy, happy.

332
00:24:24,120 --> 00:24:29,120
I mean, the thought arises of it's on its own.

333
00:24:29,120 --> 00:24:31,120
We're just trying to be mindful of it,

334
00:24:31,120 --> 00:24:39,120
because even wholesomeness can be caught up in delusion.

335
00:24:39,120 --> 00:24:40,120
They're not directly,

336
00:24:40,120 --> 00:24:46,120
but you can become unwholesome about your wholesomeness.

337
00:24:46,120 --> 00:24:49,120
If you start to get attached to it,

338
00:24:49,120 --> 00:24:56,120
attach to the idea of it anyway.

339
00:24:56,120 --> 00:25:06,120
I mean, it won't lead to freedom,

340
00:25:06,120 --> 00:25:11,120
so you can switch back and forth.

341
00:25:11,120 --> 00:25:13,120
For the death one, especially,

342
00:25:13,120 --> 00:25:19,120
you know, if you mind full of death, that's a useful meditation.

343
00:25:19,120 --> 00:25:22,120
It's good to give you the impetus to practice.

344
00:25:22,120 --> 00:25:25,120
It gives you the religious feeling.

345
00:25:25,120 --> 00:25:29,120
Some way, we call it.

346
00:25:29,120 --> 00:25:31,120
So that's mindfulness, a different meditation.

347
00:25:31,120 --> 00:25:33,120
It's useful to practice that,

348
00:25:33,120 --> 00:25:36,120
and then go back to practicing mindfulness.

349
00:25:36,120 --> 00:25:42,120
As far as the contemplation of good fortune,

350
00:25:42,120 --> 00:25:45,120
let me be careful of that,

351
00:25:45,120 --> 00:25:50,120
because it can slip into complacency.

352
00:25:50,120 --> 00:25:52,120
The angels think like that.

353
00:25:52,120 --> 00:25:56,120
They think everything, they think they're there.

354
00:25:56,120 --> 00:26:00,120
They've got some safe tears on.

355
00:26:00,120 --> 00:26:02,120
Probably you're not having that problem,

356
00:26:02,120 --> 00:26:04,120
but you have to be careful,

357
00:26:04,120 --> 00:26:08,120
but I'm not convinced that it's wholesome.

358
00:26:08,120 --> 00:26:10,120
We talk about contentment.

359
00:26:10,120 --> 00:26:15,120
There's something in there that's probably associated with contentment,

360
00:26:15,120 --> 00:26:20,120
but appreciation is a lot like liking,

361
00:26:20,120 --> 00:26:23,120
you know, and clinging,

362
00:26:23,120 --> 00:26:26,120
because things can change at any time, right?

363
00:26:26,120 --> 00:26:30,120
Your safety is completely impermanent,

364
00:26:30,120 --> 00:26:32,120
so it's an illusion.

365
00:26:32,120 --> 00:26:38,120
There's no safety in this desire.

366
00:26:38,120 --> 00:26:42,120
Anything, everything can leave you in the moment.

367
00:26:42,120 --> 00:26:45,120
So in what way is it safe?

368
00:26:45,120 --> 00:26:48,120
In the end, it's all just seeing, hearing,

369
00:26:48,120 --> 00:26:51,120
smelling, tasting, feeling, thinking.

370
00:26:55,120 --> 00:26:59,120
I'll be continuing this series on the Dhammapada.

371
00:26:59,120 --> 00:27:02,120
It's probably the only reason I continue to miss

372
00:27:02,120 --> 00:27:04,120
because people ask me the questions like this,

373
00:27:04,120 --> 00:27:06,120
because I always think, oh, well,

374
00:27:06,120 --> 00:27:08,120
maybe nobody wants them anymore.

375
00:27:08,120 --> 00:27:09,120
I haven't heard.

376
00:27:09,120 --> 00:27:11,120
Nobody's asked about them in a while,

377
00:27:11,120 --> 00:27:14,120
so probably people are sick of them.

378
00:27:18,120 --> 00:27:20,120
Yeah, I was busy with finals,

379
00:27:20,120 --> 00:27:23,120
but it's kind of just, well, now I'm not.

380
00:27:23,120 --> 00:27:27,120
So, sure, I can start up the Dhammapada series again.

381
00:27:27,120 --> 00:27:34,120
I mean, it's not that I stopped.

382
00:27:34,120 --> 00:27:41,120
I can continue to do more Dhammapada if people want it.

383
00:27:45,120 --> 00:27:48,120
How is it that we bow down to and revere the meditation practice?

384
00:27:48,120 --> 00:27:50,120
I mean, the practice itself.

385
00:27:50,120 --> 00:27:56,120
Should we hold the triple gem or the practice in mind for reverence?

386
00:27:56,120 --> 00:28:00,120
Well, I don't think you need to hold anything in mind for reverence.

387
00:28:00,120 --> 00:28:03,120
It's just taking that serious thing.

388
00:28:03,120 --> 00:28:06,120
I mean, you can do meditations on the Buddha,

389
00:28:06,120 --> 00:28:08,120
the Dhammapada and the Sangha obviously.

390
00:28:08,120 --> 00:28:11,120
But that's not what I don't think what the Buddha is saying.

391
00:28:11,120 --> 00:28:14,120
It's just if they don't respect.

392
00:28:14,120 --> 00:28:16,120
Because there's a lot of disrespect.

393
00:28:16,120 --> 00:28:19,120
For the Sangha, for example,

394
00:28:19,120 --> 00:28:21,120
monks get a lot of disrespect.

395
00:28:21,120 --> 00:28:25,120
All teachers get a lot of disrespect.

396
00:28:25,120 --> 00:28:28,120
Not a lot, not mostly respect.

397
00:28:28,120 --> 00:28:33,120
But there's always people who have very little respect.

398
00:28:33,120 --> 00:28:39,120
The point is, well, that is harmful to the...

399
00:28:39,120 --> 00:28:44,120
You could say respect has to be earned, sure, fine.

400
00:28:44,120 --> 00:28:48,120
But there's something about being respectful.

401
00:28:48,120 --> 00:28:52,120
If you don't want to practice,

402
00:28:52,120 --> 00:28:53,120
you don't have to come practice.

403
00:28:53,120 --> 00:28:58,120
But if someone's teaching, there's a lot of...

404
00:28:58,120 --> 00:29:02,120
And then there's disrespect to the Buddha.

405
00:29:02,120 --> 00:29:08,120
Disrespect to the Dhammap.

406
00:29:08,120 --> 00:29:12,120
You know, not taking it seriously.

407
00:29:12,120 --> 00:29:14,120
Disrespect.

408
00:29:14,120 --> 00:29:16,120
And not necessarily disrespect, right?

409
00:29:16,120 --> 00:29:19,120
But not taking the practice seriously.

410
00:29:19,120 --> 00:29:24,120
You know, people who do walking meditation, talking on the phone.

411
00:29:24,120 --> 00:29:27,120
And it's not even...

412
00:29:27,120 --> 00:29:29,120
Not even how disrespectful that is.

413
00:29:29,120 --> 00:29:30,120
It's just...

414
00:29:30,120 --> 00:29:31,120
You know, if you don't take it seriously,

415
00:29:31,120 --> 00:29:33,120
you're not going to get any results.

416
00:29:33,120 --> 00:29:35,120
So the question, if you do an hour of meditation,

417
00:29:35,120 --> 00:29:37,120
how much are you really meditating?

418
00:29:37,120 --> 00:29:40,120
Are you really taking it seriously?

419
00:29:40,120 --> 00:29:42,120
Do you respect...

420
00:29:42,120 --> 00:29:45,120
And it's not respect in terms of disrespect.

421
00:29:45,120 --> 00:29:47,120
It's like, do you appreciate?

422
00:29:47,120 --> 00:29:48,120
That's the best.

423
00:29:48,120 --> 00:29:49,120
Do you appreciate?

424
00:29:49,120 --> 00:29:52,120
Gauru means heavy, so it's like taking seriously,

425
00:29:52,120 --> 00:29:54,120
seeing it as a weighty thing.

426
00:29:54,120 --> 00:29:55,120
Or do you see it?

427
00:29:55,120 --> 00:29:57,120
Do you take it lightly, right?

428
00:29:57,120 --> 00:30:00,120
It's the opposite of taking something lightly.

429
00:30:00,120 --> 00:30:02,120
If you take meditation lightly,

430
00:30:02,120 --> 00:30:05,120
it's how you won't get the results.

431
00:30:05,120 --> 00:30:06,120
If you don't get the results,

432
00:30:06,120 --> 00:30:20,120
but it's a move pathway.

433
00:30:20,120 --> 00:30:21,120
But yeah, if you want to do this,

434
00:30:21,120 --> 00:30:23,120
if you want to pay respect,

435
00:30:23,120 --> 00:30:27,120
there's a quick chant that we do in the opening ceremony.

436
00:30:27,120 --> 00:30:29,120
So I often do it as a...

437
00:30:29,120 --> 00:30:32,120
Kind of like a mantra, really.

438
00:30:32,120 --> 00:30:34,120
We pay respect to the five things.

439
00:30:34,120 --> 00:30:36,120
The Buddha, the Dhamma and the Sangha,

440
00:30:36,120 --> 00:30:38,120
the meditation practice,

441
00:30:38,120 --> 00:30:42,120
and the teacher who offers the meditation practice.

442
00:30:42,120 --> 00:30:46,120
Namami, Bhudhyangu, Nasagarantam.

443
00:30:46,120 --> 00:30:48,120
Nasagarantam.

444
00:30:48,120 --> 00:30:52,120
Namami, Dhamma, Muni Raja, Desitam.

445
00:30:52,120 --> 00:30:55,120
Namami, Sangha, Muni Rajasamakam.

446
00:30:55,120 --> 00:30:59,120
Namami, Kammatana, Nibbana, Dikamupaya.

447
00:30:59,120 --> 00:31:02,120
Namami, Kammatana, Dayakacharya, Nibbana,

448
00:31:02,120 --> 00:31:22,120
Nambagu, Desikhan.

449
00:31:22,120 --> 00:31:29,120
And there are?".

450
00:31:29,120 --> 00:31:34,680
It's in the opening ceremony, I'm not sure where you get the opening ceremony.

451
00:31:34,680 --> 00:31:42,400
I think it's in our, whereas the opening ceremony, maybe it's not even online.

452
00:31:42,400 --> 00:31:45,800
But it might be taken from the Visudhi manga.

453
00:31:45,800 --> 00:31:59,400
A lot of that is taken from Visudhi manga, maybe not.

454
00:31:59,400 --> 00:32:10,920
Yeah, respect for the five, and so we do before we start the minute.

455
00:32:10,920 --> 00:32:16,840
It's the first thing we do in the, in the opening ceremony, almost the first thing, first

456
00:32:16,840 --> 00:32:17,840
big thing.

457
00:32:17,840 --> 00:32:23,880
I don't know, this is the first thing, isn't it?

458
00:32:23,880 --> 00:32:24,880
Yeah.

459
00:32:24,880 --> 00:32:27,320
So we start the opening ceremony and the closing ceremony.

460
00:32:27,320 --> 00:32:33,600
So I don't do ceremonies here, not yet anyway, but normally we'd have a ceremony where

461
00:32:33,600 --> 00:32:36,600
we go through all of this in pollen.

462
00:32:36,600 --> 00:32:39,600
It's quite nice.

463
00:32:39,600 --> 00:32:45,320
It's the thing, it should take it seriously, no, if we were to take it seriously, we probably

464
00:32:45,320 --> 00:32:47,040
should do the ceremony.

465
00:32:47,040 --> 00:32:52,160
But, you know, in the West, sometimes, again, people don't take it seriously enough.

466
00:32:52,160 --> 00:32:57,440
It's hard to push that on them.

467
00:32:57,440 --> 00:32:59,080
It's not entirely necessary.

468
00:32:59,080 --> 00:33:03,400
It doesn't mean you do a ceremony, it's the thing, it's just a ceremony.

469
00:33:03,400 --> 00:33:08,680
But something to consider once we get established here, maybe I can teach Michael, because

470
00:33:08,680 --> 00:33:10,680
another thing is you need two people.

471
00:33:10,680 --> 00:33:15,200
I do part, but I need a lay person to lead the meditators.

472
00:33:15,200 --> 00:33:17,000
I need someone to lead the meditators.

473
00:33:17,000 --> 00:33:25,400
I'll teach Michael how to do it someday, if he sticks around, maybe he'll become a monk.

474
00:33:25,400 --> 00:33:31,920
If he does, I get to give him the name Mogaracha again, because it's the closest thing

475
00:33:31,920 --> 00:33:33,880
to Michael, right?

476
00:33:33,880 --> 00:33:40,000
So our last Michael got the name Mogaracha, and boy did that, because of the stir.

477
00:33:40,000 --> 00:33:45,840
You know, the Sri Lankan people said they didn't want to bow down to it, because the

478
00:33:45,840 --> 00:33:53,680
word Mogar means useless or bad or stupid, but Mogaracha was one of the Buddhist

479
00:33:53,680 --> 00:34:07,280
chief disciples, one of the 80 great disciples, shame really.

480
00:34:07,280 --> 00:34:16,600
I have the five references somewhere, not sure where, but probably buried away somewhere

481
00:34:16,600 --> 00:34:26,760
in my documents, folder, you copy them.

482
00:34:26,760 --> 00:34:27,760
Anyway.

483
00:34:27,760 --> 00:34:29,800
That's all for tonight.

484
00:34:29,800 --> 00:34:48,400
Thank you all for tuning in, have a good night.

