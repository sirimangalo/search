1
00:00:00,000 --> 00:00:05,000
I have not been using chairs lately and so I'm getting to sit longer.

2
00:00:05,000 --> 00:00:11,000
When the cross-legged position gets too painful, should we compensate with walking,

3
00:00:11,000 --> 00:00:18,000
or is there another sitting position?

4
00:00:18,000 --> 00:00:25,000
When you sit in a cross-legged position and it becomes painful,

5
00:00:25,000 --> 00:00:35,000
the first thing is that you shouldn't note pain, pain, pain.

6
00:00:35,000 --> 00:00:42,000
Eventually until it goes away, sometimes it goes away, sometimes it doesn't.

7
00:00:42,000 --> 00:00:46,000
You should not try to compensate with walking,

8
00:00:46,000 --> 00:00:55,000
and because this would get you out of your meditation,

9
00:00:55,000 --> 00:01:00,000
you don't want to compensate, you want to see the reality,

10
00:01:00,000 --> 00:01:05,000
you want to see clearly as things are, and if there is pain,

11
00:01:05,000 --> 00:01:11,000
then this is to be seen clearly, and if the pain rises,

12
00:01:11,000 --> 00:01:16,000
and you say pain, it might go away, and if it does not go away,

13
00:01:16,000 --> 00:01:20,000
it might well be that there arises frustration,

14
00:01:20,000 --> 00:01:23,000
and this should be then noted.

15
00:01:23,000 --> 00:01:28,000
And after the frustration, might anger arise,

16
00:01:28,000 --> 00:01:33,000
or something else you might have some hearing in between,

17
00:01:33,000 --> 00:01:37,000
and then pain comes again.

18
00:01:37,000 --> 00:01:46,000
So you would miss all that if you just start walking.

19
00:01:46,000 --> 00:01:55,000
If the pain becomes unbearable and does not go away after time of noting it,

20
00:01:55,000 --> 00:01:59,000
then you can, of course, change the position,

21
00:01:59,000 --> 00:02:16,000
but you should do every single phase of changing position with noting every single step.

22
00:02:16,000 --> 00:02:22,000
When you have the thought of, oh, I need to change my position,

23
00:02:22,000 --> 00:02:30,000
then notice thinking, thinking, notice the wanting to change the position,

24
00:02:30,000 --> 00:02:37,000
note the intention, note the impulse that goes from the mind to the foot,

25
00:02:37,000 --> 00:02:42,000
and to the arm that takes the foot to move the foot around,

26
00:02:42,000 --> 00:02:47,000
and every single step should be noted,

27
00:02:47,000 --> 00:02:54,000
because every single step is one single present moment,

28
00:02:54,000 --> 00:03:01,000
and in one single present moment of moving,

29
00:03:01,000 --> 00:03:04,000
moving your leg, for example,

30
00:03:04,000 --> 00:03:08,000
the pain is not there because there is moving.

31
00:03:08,000 --> 00:03:14,000
So you learn by doing the movements mindful,

32
00:03:14,000 --> 00:03:19,000
a great lesson, hopefully.

33
00:03:19,000 --> 00:03:48,000
You said it all.

