1
00:00:00,000 --> 00:00:16,080
Welcome back to Q&A series.

2
00:00:16,080 --> 00:00:24,600
Today's question is about dealing with intense emotions.

3
00:00:24,600 --> 00:00:36,680
So the question was specifically regarding longing, someone who is fiance, who they loved

4
00:00:36,680 --> 00:00:43,720
very much was a way for an extended period of time and they found it interrupted their

5
00:00:43,720 --> 00:00:51,800
meditation practice, intense states of longing that prevented them from having a peaceful

6
00:00:51,800 --> 00:01:06,360
meditation session and so the question is what to do about this.

7
00:01:06,360 --> 00:01:24,600
So I think first and foremost we have to be clear about how we look at peace from a meditative

8
00:01:24,600 --> 00:01:31,240
perspective, how does peace relate to mindfulness practice?

9
00:01:31,240 --> 00:01:45,960
This is the goal of mindfulness insight, meditation practice, it's the goal of Buddhism.

10
00:01:45,960 --> 00:01:50,880
But how peaceful our meditation practice is going to be, is going to depend very much

11
00:01:50,880 --> 00:02:03,640
upon our habits and the qualities of the states of mind that make up our habits.

12
00:02:03,640 --> 00:02:15,800
Mindfulness is about focusing on our objective reality, it's about confronting that reality.

13
00:02:15,800 --> 00:02:37,000
So if your reality is one of longing then that's the reality that you're going to face.

14
00:02:37,000 --> 00:02:48,320
Finding your meditation to be peaceful is incorrect, improper, unreasonable, mindfulness

15
00:02:48,320 --> 00:02:56,920
meditation is about creating specific, pleasant state, it's about dealing with the states

16
00:02:56,920 --> 00:03:02,320
that exist in your mind and learning about them and understanding them.

17
00:03:02,320 --> 00:03:08,880
So that's first is not to consider that your practice is bad because in fact the description

18
00:03:08,880 --> 00:03:15,040
that you're giving of your practice is something that's praiseworthy.

19
00:03:15,040 --> 00:03:19,560
From the sounds of it you're practicing quite well.

20
00:03:19,560 --> 00:03:30,840
What you're seeing is that the result of longing is stressful and what you're also seeing

21
00:03:30,840 --> 00:03:39,200
are able to see from this over time especially, is that the attachment you have for this

22
00:03:39,200 --> 00:03:47,920
person is leading to the longing which is leading to stress and unpleasantness.

23
00:03:47,920 --> 00:04:00,400
It's a fact, it's an aspect of clinging and what I didn't just make all up all up.

24
00:04:00,400 --> 00:04:07,040
It's stuff about craving being the cause of suffering.

25
00:04:07,040 --> 00:04:19,440
The pleasant sensations that we strive for that we crave for lead to addiction and decide

26
00:04:19,440 --> 00:04:28,520
inspection when we don't get what we want, when we are up the A, he's sample your goal, being associated

27
00:04:28,520 --> 00:04:35,680
with what is not pleasant, the A, we pay, we pay your goal, being separated from what is

28
00:04:35,680 --> 00:04:48,160
dear to us, this is suffering, this is stress, but important to note as well is that mindfulness

29
00:04:48,160 --> 00:04:58,320
is not about denying or rejecting the desire, the attachment of what you would say, the

30
00:04:58,320 --> 00:05:08,040
love for this person, it's for facing it and observing it, which is what you're doing and

31
00:05:08,040 --> 00:05:13,440
the practice that you're doing from the sounds of it, if continued, if you're patient

32
00:05:13,440 --> 00:05:23,440
and if you can face those emotions, it's going to lead to more contentment, more peace,

33
00:05:23,440 --> 00:05:32,000
less attachment, less stress and less suffering and this can be generalized to any sort

34
00:05:32,000 --> 00:05:38,120
of extreme state like this, you don't have to reject, first of all don't reject the stress

35
00:05:38,120 --> 00:05:45,880
and the suffering that comes from these states, so that appears in meditation, oh my

36
00:05:45,880 --> 00:05:53,080
meditation is bad because I'm stressed and disquieted during the practice, I'm no good at

37
00:05:53,080 --> 00:06:04,400
this meditation, I'm doing something wrong, there's one of my videos I think was quite

38
00:06:04,400 --> 00:06:09,800
complete in describing, I think it's called the experience of reality and I definitely

39
00:06:09,800 --> 00:06:16,000
recommend that video because it summed up pretty much everything I wanted to say about

40
00:06:16,000 --> 00:06:23,240
the sort of idea that we have bad practice because it's unpleasant, it's unpredictable

41
00:06:23,240 --> 00:06:29,680
because it's uncontrollable and so why that's actually a good sign of learning something

42
00:06:29,680 --> 00:06:43,720
about cause and effect the nature of reality and sort of, but also don't reject or fight

43
00:06:43,720 --> 00:06:57,560
against the negative, against the causes of the negative experiences, so desire, aversion,

44
00:06:57,560 --> 00:07:03,640
trying to reject our attachment and reject our love for other people and so on, it's not

45
00:07:03,640 --> 00:07:12,280
necessary, it's not really all that helpful or best served by studying and observing, then

46
00:07:12,280 --> 00:07:17,400
you don't have to believe me that I say oh well, this has to do with your intense attachment

47
00:07:17,400 --> 00:07:22,960
to this person and you'd be better off getting rid of it, naturally as you observe, you

48
00:07:22,960 --> 00:07:28,600
become less attached to people, you become less dependent on them and thus less susceptible

49
00:07:28,600 --> 00:07:34,080
to the suffering that comes from change, the suffering that comes not from change but from

50
00:07:34,080 --> 00:07:46,080
our inability to bear with and to deal with and to live with change in permanence.

51
00:07:46,080 --> 00:07:51,920
That's about the core of it in regards to intense states.

52
00:07:51,920 --> 00:07:57,280
Because they can get so intense that it's very difficult to practice, it's easy to say

53
00:07:57,280 --> 00:08:01,000
well, just try and bear with them, that's the theory.

54
00:08:01,000 --> 00:08:08,560
There are many ways that you can reduce extreme states, but a lot of them have to do with

55
00:08:08,560 --> 00:08:14,800
not having a fiancee, for example, living in the forest, being celibate, eating one meal

56
00:08:14,800 --> 00:08:24,160
a day, I mean one great thing that works even for people who have partners and children

57
00:08:24,160 --> 00:08:31,560
and material lives, worldly lives is to do a meditation course, you know, find some time

58
00:08:31,560 --> 00:08:39,520
to either do a course intensively or do some sort of, you know, we do courses where people

59
00:08:39,520 --> 00:08:45,240
meet once a week, it's far less effective and less profound, it's the change if you do

60
00:08:45,240 --> 00:08:51,280
a sort of one hour a day meditation course, but it's still helpful, you know, undergo

61
00:08:51,280 --> 00:08:57,040
some training and the guidance from the teacher, sort of the psychological support that

62
00:08:57,040 --> 00:09:02,400
comes from knowing that you've got someone in your corner who's guiding you and in fact

63
00:09:02,400 --> 00:09:07,800
the guidance that they give if they're qualified to give it can be quite helpful in terms

64
00:09:07,800 --> 00:09:15,280
of helping you to deal with the more extreme states especially, so there you go, that's

65
00:09:15,280 --> 00:09:44,360
an answer to that question, thank you all for tuning in, have a good day.

