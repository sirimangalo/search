1
00:00:00,000 --> 00:00:22,520
Okay, good evening everyone, welcome to our live broadcast November 24th, so this daily thing

2
00:00:22,520 --> 00:00:34,920
is working out well. Today is, excuse me, the shaving day in Thailand. Tomorrow is the full moon,

3
00:00:34,920 --> 00:00:42,520
I think, which means, or maybe today it depends on which there's two traditions in Thailand.

4
00:00:42,520 --> 00:00:49,760
One strict and one not so strict. One strict and one traditional, I think you could say,

5
00:00:49,760 --> 00:00:57,760
is technically sometimes the moon is full after midnight, sometimes it's full before midnight,

6
00:00:57,760 --> 00:01:10,080
but the traditional group goes more by calculations than by exact hour, so there's two ways of

7
00:01:10,080 --> 00:01:15,560
calculating. I don't know which one I'm saying tomorrow is, but many of the full

8
00:01:15,560 --> 00:01:29,440
months are still the same. Anyway, shaving today. Shave once a month. It's a big thing

9
00:01:29,440 --> 00:01:34,440
in Thailand. Some monks do shave more regularly. In Sri Lanka they shave seemingly every

10
00:01:34,440 --> 00:01:42,600
couple days or every week. It's arguable which one's better. If you shave like once

11
00:01:42,600 --> 00:01:48,440
a week, then once every few days you can shave everything, shave your beard and your head

12
00:01:48,440 --> 00:01:54,080
all at once, and then it never gets so long and it's a bit of a drag because every month, shaving

13
00:01:54,080 --> 00:02:00,720
once a month and it's a lot of payer to continue to shave. It seems like less fuss to shave

14
00:02:00,720 --> 00:02:10,280
once a month. Probably get the inevitable question, why do monks shave? I think it's cleaner.

15
00:02:10,280 --> 00:02:19,880
It's less fuss. There's no ego attachment to the hair and hairstyle because monks were styling

16
00:02:19,880 --> 00:02:25,840
their hair in the beginning. That was why they would have instated the rule not to let it

17
00:02:25,840 --> 00:02:38,160
get longer. It can get this long, two fingers long, or two months. If you haven't shaved

18
00:02:38,160 --> 00:02:43,320
in two months, you have to shave. If it gets longer than two fingers, then you have to

19
00:02:43,320 --> 00:02:50,920
shave. That also apparently goes for the beard or technically goes for the beard so that

20
00:02:50,920 --> 00:02:56,440
some monks were testing that. There are monks who let their beard grow out. It's still

21
00:02:56,440 --> 00:03:05,760
two months you have to shave your beard. It's quite a lot of beard. Mostly we don't go

22
00:03:05,760 --> 00:03:12,280
by that. If I go by that one in Thailand, I have a real thing against facial hair in

23
00:03:12,280 --> 00:03:19,160
Thailand because most Thai people have very little facial hair. But still, it's your

24
00:03:19,160 --> 00:03:37,640
rogue if you have facial hair. Big on appearances. Good evening. Today I taught two people

25
00:03:37,640 --> 00:03:43,800
how to meditate. Or one person, I guess. This friend I'm talking about, my university

26
00:03:43,800 --> 00:03:50,640
friend, got one of her friends to come and learn how to meditate. Who also is struggling.

27
00:03:50,640 --> 00:03:57,520
She suffers from many of the same issues as she does. But it was a very good meditator.

28
00:03:57,520 --> 00:04:04,080
This new person was picked it up like nothing. Most people, most of us, I mean I include

29
00:04:04,080 --> 00:04:08,680
myself and this aren't able to pick it up very quickly. The odd person gets it really

30
00:04:08,680 --> 00:04:15,840
quickly. Like her synchronicity. It's simple things. You can tell when they do the walking.

31
00:04:15,840 --> 00:04:22,520
She's able to say step being right, step. So she's in time. That's rare actually. For someone

32
00:04:22,520 --> 00:04:28,880
who hasn't done it, it's actually a bit of a skill. But I was impressed by just that

33
00:04:28,880 --> 00:04:36,320
simple thing. And then she was sat very still when I did my meditation and afterwards

34
00:04:36,320 --> 00:04:47,480
said she was able to do it. So there you go. One person at a time. I'd really like to

35
00:04:47,480 --> 00:04:57,920
see if she keeps it up. I'll have to bring her a book. About this charity thing that

36
00:04:57,920 --> 00:05:03,880
we're looking at. Send them all you got in touch with me and there's a kid, there's a

37
00:05:03,880 --> 00:05:13,120
children's home in Tampa that they've been involved in involved with. But Robin, you sent

38
00:05:13,120 --> 00:05:19,640
me something about dogs. Actually, if there were something about cats or just pets in general

39
00:05:19,640 --> 00:05:29,160
because my stepfather's a real cat person. I guess dogs have more trouble, right? Bigger,

40
00:05:29,160 --> 00:05:36,480
harder to take care of, suffer more. They can't take care of themselves at cats again,

41
00:05:36,480 --> 00:05:43,480
maybe wild cats. I'm not sure. There may be something for cats. I do look that far. I was

42
00:05:43,480 --> 00:05:48,520
thinking about dogs today and I just looked it up. I can check for cats too. I was really

43
00:05:48,520 --> 00:06:04,480
thinking about helping humans. I guess humans are important too. They have to look after

44
00:06:04,480 --> 00:06:15,600
the dogs after all. Yes. And the cats. They have to praise the cats. They have to slave

45
00:06:15,600 --> 00:06:32,080
over the cats. That's their duty, their role in life. So what are you thinking to do for

46
00:06:32,080 --> 00:06:39,640
the children's home? Shall we do a Lacananan campaign? Or what were you thinking? I don't

47
00:06:39,640 --> 00:06:49,520
know if I can say yes to that. Yeah. I guess so. I guess I can say that. So that's sort

48
00:06:49,520 --> 00:06:59,920
of a neat idea. But I just can't talk about money. Sure. Can you tell me the name of the

49
00:06:59,920 --> 00:07:09,320
organization? We haven't decided on one. But I guess we can just decide on this children's

50
00:07:09,320 --> 00:07:16,040
home. I hope they send them out. I'm sure she's watching or she watches this. She did

51
00:07:16,040 --> 00:07:26,520
watch last night. Children's Home.org. Children's Home.org and they have a visit

52
00:07:26,520 --> 00:07:39,640
us. They also have something over the holidays. It's a nice website. Since 1892 they're in Tampa.

53
00:07:39,640 --> 00:07:46,640
We could just go for a tour. I could give a gift and we could go for a tour. VIP tours. So

54
00:07:46,640 --> 00:08:16,320
it's a VIP tour. You have to give a certain amount to become a VIP.

55
00:08:16,320 --> 00:08:19,920
Looks like they're having a big event on Saturday, December 12th, but you'll be there

56
00:08:19,920 --> 00:08:28,320
a little after that. Hmm. Yeah. That's to be early. Though we could give, you know,

57
00:08:28,320 --> 00:08:35,720
if we, but that's when that's an open house. Make a donation from our list of most needed

58
00:08:35,720 --> 00:08:44,800
items. So they have like a, they have a wish list of some sort. There's a way to find

59
00:08:44,800 --> 00:08:54,800
out how to become a VIP like what's the criteria. Then we'd have a goal, right? Yeah. How you

60
00:08:54,800 --> 00:09:13,440
can help. Make a financial donation. Here, make a gift and honor someone you care about. Become

61
00:09:13,440 --> 00:09:34,400
part of a surrogate family. So a benefactor is $1,000 each year for five years. That's

62
00:09:34,400 --> 00:09:56,400
it. Pushing it probably. Well, they have, they have a list of their most urgent needs.

63
00:09:56,400 --> 00:10:03,520
So we, you know, we could ask people if they'd like to send these items. Um, although I don't

64
00:10:03,520 --> 00:10:07,680
think you probably want to have to carry them down there. So we'd be better if we could send

65
00:10:07,680 --> 00:10:16,800
them to someone in the area. But on their most urgent needs, they're looking for, um, women's

66
00:10:16,800 --> 00:10:33,840
and men's hoodies and personal care items like body wash and hair products and things. Maderies.

67
00:10:33,840 --> 00:10:39,960
They have a much longer list of ongoing needs, just all sorts of personal care items and

68
00:10:39,960 --> 00:10:52,120
recreational items. So we could either, um, you know, look for donations of items or do

69
00:10:52,120 --> 00:11:05,560
an online campaign or donate gift cards. You've got a lot of options here. I'm back to

70
00:11:05,560 --> 00:11:11,640
school supplies. I'm trying to think of something that could get my family involved or

71
00:11:11,640 --> 00:11:25,160
to be able to give us a gift to them. Or a gift on their behalf would be the best way to

72
00:11:25,160 --> 00:11:35,000
give on someone else. We had a bunch of supplies. Then we could, we could all together

73
00:11:35,000 --> 00:11:49,480
deliver it. Yeah, we would just need a place to send to send them down there. So you wouldn't

74
00:11:49,480 --> 00:12:01,560
have to try to carry them all with you. There's urgent needs in November.

75
00:12:01,560 --> 00:12:08,760
Batteries.

76
00:12:08,760 --> 00:12:31,360
It's actually served children and adults.

77
00:12:31,360 --> 00:12:37,640
They suggest having a donation drive, which kind of sounds like maybe it will work talking

78
00:12:37,640 --> 00:13:01,640
about maybe.

79
00:13:01,640 --> 00:13:04,640
When you're ready, Matthew, there are a couple of questions.

80
00:13:04,640 --> 00:13:07,640
Let's get back to the number.

81
00:13:07,640 --> 00:13:11,840
I'm starting to feel like we're meeting there for a moment.

82
00:13:11,840 --> 00:13:15,040
At the moment someone dies, how long do they stay near their body?

83
00:13:15,040 --> 00:13:17,840
Are they still aware of their former selves?

84
00:13:17,840 --> 00:13:20,440
How long before they are reborn?

85
00:13:20,440 --> 00:13:22,440
I don't have no idea.

86
00:13:22,440 --> 00:13:23,440
It's not.

87
00:13:23,440 --> 00:13:29,240
It's not qualified to answer that question.

88
00:13:29,240 --> 00:13:30,240
Those question.

89
00:13:30,240 --> 00:13:37,240
There are all good questions to find someone who knows.

90
00:13:37,240 --> 00:13:44,960
Some of it's on the Buddha has passed things down, but not really to that extent, not

91
00:13:44,960 --> 00:13:46,120
really in that detail.

92
00:13:46,120 --> 00:13:51,280
We have stories that have been passed down, spirits.

93
00:13:51,280 --> 00:13:57,960
There was one spirit hanging out by its dead body and among came and took the cloth

94
00:13:57,960 --> 00:14:02,640
off the body because they usually wrapped them up in a white cloth and throw them in the

95
00:14:02,640 --> 00:14:04,680
rubbish and the carnal ground.

96
00:14:04,680 --> 00:14:10,320
We pulled the cloth off thinking it would make a nice robe cloth and the spirit got really

97
00:14:10,320 --> 00:14:17,600
upset and went back into the body and stood up and chased them all the way back to

98
00:14:17,600 --> 00:14:24,840
his good day where he closed the door and the body fell down and it dropped down against

99
00:14:24,840 --> 00:14:29,840
the door and the spirit left.

100
00:14:29,840 --> 00:14:31,840
You want to just pop back?

101
00:14:31,840 --> 00:14:38,560
A real Buddhist zombie story and you wanted the cloth back.

102
00:14:38,560 --> 00:14:42,040
So there was something in the Buddha and stated to rule as a result of that.

103
00:14:42,040 --> 00:14:44,240
I can't remember what the rule was.

104
00:14:44,240 --> 00:14:51,520
I think there was a rule and stated like making sure the body is really dead or something

105
00:14:51,520 --> 00:14:53,560
like what was the rule.

106
00:14:53,560 --> 00:14:57,920
It was a simple, it wasn't about don't take, don't take cloths from dead bodies.

107
00:14:57,920 --> 00:15:02,560
It's really a neat thing to do and it's not considered stealing because really your

108
00:15:02,560 --> 00:15:11,280
dead dude can move on and get them over it or something, some sort of minor rule that

109
00:15:11,280 --> 00:15:18,840
he instated to sort of make sure he didn't get chased by zombies.

110
00:15:18,840 --> 00:15:26,560
You could be covered the body up with something else.

111
00:15:26,560 --> 00:15:32,200
Dear Bhante, the more I give up doubts, the more I give up doubts regarding the practice,

112
00:15:32,200 --> 00:15:36,640
the more I understand the practice, the more I dedicate myself to the practice, and the

113
00:15:36,640 --> 00:15:38,920
more I let go.

114
00:15:38,920 --> 00:15:42,800
The more they develop an issue that could probably be seen as relating to what you described

115
00:15:42,800 --> 00:15:47,400
as the second imperfection of insight, the imperfection of knowledge.

116
00:15:47,400 --> 00:15:52,280
What has been happening lately is a kind of circle or siswam motion or something.

117
00:15:52,280 --> 00:15:57,480
I attain more clarity regarding pleasure, dukka and amatha.

118
00:15:57,480 --> 00:16:01,400
I start to see with greater and greater clarity the uselessness and worthlessness of

119
00:16:01,400 --> 00:16:06,760
aspects of myself, of other people, of several activities I've been occupied with throughout

120
00:16:06,760 --> 00:16:12,760
my life, of the goals I've had in my life, of the goals other people have, etc.

121
00:16:12,760 --> 00:16:17,200
This makes me want to practice even more and dedicate myself even more and I do that.

122
00:16:17,200 --> 00:16:21,640
But soon afterwards, when indulging in the many activities as before and dealing with the

123
00:16:21,640 --> 00:16:26,560
same people as before, which tends to be bound to happen due to being a layperson living

124
00:16:26,560 --> 00:16:31,280
in the world, my experience with these activities and people is now different because

125
00:16:31,280 --> 00:16:34,360
of how the practice has affected the mind.

126
00:16:34,360 --> 00:16:38,920
Everything is now so much easier and smoother and sometimes it even feels like I could clearly

127
00:16:38,920 --> 00:16:40,840
accomplish anything.

128
00:16:40,840 --> 00:16:45,840
This tends to entice or alert very strongly to play the mundane game of life.

129
00:16:45,840 --> 00:16:50,280
To play this mundane game of life for at least a bit longer, as it feels like I'm now

130
00:16:50,280 --> 00:16:54,760
almost looking at these activities and interactions from the outside as some kind of

131
00:16:54,760 --> 00:16:56,600
super-user.

132
00:16:56,600 --> 00:17:01,000
Of course this doesn't last for very long, but it makes things difficult.

133
00:17:01,000 --> 00:17:06,160
Even when it happens, I still always kind of remember the importance of the practice

134
00:17:06,160 --> 00:17:10,680
and the worthlessness of the things I mentioned earlier, but at least this slows things

135
00:17:10,680 --> 00:17:12,440
down considerably.

136
00:17:12,440 --> 00:17:14,200
Can you talk about this?

137
00:17:14,200 --> 00:17:15,200
Thank you.

138
00:17:15,200 --> 00:17:18,200
I think Robin just didn't talk about that.

139
00:17:18,200 --> 00:17:20,200
What's the question?

140
00:17:20,200 --> 00:17:25,280
I'm sorry, Robin, that's not really fair, is it?

141
00:17:25,280 --> 00:17:31,880
I mean, it's neat to hear about, but I think we probably understand when he's talking

142
00:17:31,880 --> 00:17:33,920
about that big change.

143
00:17:33,920 --> 00:17:34,920
That's the question.

144
00:17:34,920 --> 00:17:35,920
Yeah.

145
00:17:35,920 --> 00:17:36,920
I think you need to talk about it.

146
00:17:36,920 --> 00:17:43,080
I mean, Robin, you can, but I have pity on Robin that she has to talk and has to just

147
00:17:43,080 --> 00:17:44,080
say it all.

148
00:17:44,080 --> 00:17:45,080
That's okay.

149
00:17:45,080 --> 00:17:49,120
I don't mind reading long questions, you know, if it's helpful.

150
00:17:49,120 --> 00:17:51,120
And in the end, there's no question.

151
00:17:51,120 --> 00:17:52,120
What's the question?

152
00:17:52,120 --> 00:17:53,680
I mean, good for you.

153
00:17:53,680 --> 00:17:59,200
It's great to hear about your practice, but no, we don't.

154
00:17:59,200 --> 00:18:00,200
It's not fair.

155
00:18:00,200 --> 00:18:01,200
You have to ask a question.

156
00:18:01,200 --> 00:18:04,000
And it should be short and concise and simple.

157
00:18:04,000 --> 00:18:05,720
You see, to understand.

158
00:18:05,720 --> 00:18:08,880
Is that person still around?

159
00:18:08,880 --> 00:18:09,880
Yes.

160
00:18:09,880 --> 00:18:14,560
Well, then ask a question and make it simple.

161
00:18:14,560 --> 00:18:16,600
Keep it simple.

162
00:18:16,600 --> 00:18:23,080
Those are the rules.

163
00:18:23,080 --> 00:18:28,480
I don't need so much background, really, because you'll find that when you do come up

164
00:18:28,480 --> 00:18:34,280
with a question, if there is one, that you didn't really need to give all the background

165
00:18:34,280 --> 00:18:35,280
details.

166
00:18:35,280 --> 00:18:41,120
So you have to learn to be mindful, and I don't think so much.

167
00:18:41,120 --> 00:18:44,920
It sounds like there's maybe too much mental activity.

168
00:18:44,920 --> 00:18:47,920
Have a question to ask it.

169
00:18:47,920 --> 00:18:51,960
Then things then you don't be hitting you with a stick.

170
00:18:51,960 --> 00:18:56,960
We'll have to implement the Twitter limitation there.

171
00:18:56,960 --> 00:18:57,960
What's that 120 character?

172
00:18:57,960 --> 00:19:03,320
Yeah, it was originally at something like what, 400 characters or something, 200 and then

173
00:19:03,320 --> 00:19:07,160
people, oh, no, it's too short.

174
00:19:07,160 --> 00:19:12,360
How many posts did that person use just to ask that, not a question?

175
00:19:12,360 --> 00:19:13,680
It was just two.

176
00:19:13,680 --> 00:19:17,320
Oh, Timo, this is the guy who's coming in December.

177
00:19:17,320 --> 00:19:19,320
OK.

178
00:19:19,320 --> 00:19:28,480
So he's going to put a little more information there, and I believe.

179
00:19:28,480 --> 00:19:32,320
No, there's no need for background.

180
00:19:32,320 --> 00:19:34,320
Let's ask a question.

181
00:19:34,320 --> 00:19:38,320
See, and if you can't formulate a question, then you've got a problem.

182
00:19:38,320 --> 00:19:39,320
That's the reason.

183
00:19:39,320 --> 00:19:45,080
It's a litmus test that you still have, it's still not clear in your mind, which means

184
00:19:45,080 --> 00:19:46,080
you had that's your problem.

185
00:19:46,080 --> 00:19:51,400
You have to start saying, thinking, thinking, looking at your mind, and maybe read my

186
00:19:51,400 --> 00:19:55,640
booklet again to figure out if you're actually meditating correctly, because it's

187
00:19:55,640 --> 00:20:04,080
easy to get meditating on the wrong path, maybe you have states of bliss or happiness.

188
00:20:04,080 --> 00:20:08,040
Look at the 10 imperfections of insight, and see if you have any of the other ones.

189
00:20:08,040 --> 00:20:13,360
If you're happy or calm, if you're powerful, well, that's a confidence, that's sun

190
00:20:13,360 --> 00:20:14,360
habits.

191
00:20:14,360 --> 00:20:27,360
And the more I think is the defining, so you have to say, happy, happy, confident,

192
00:20:27,360 --> 00:20:37,360
confident, knowing, knowing how to do it.

193
00:20:37,360 --> 00:20:45,320
I know just on a calendar, Dante, that tomorrow is on the final Saturday, is that a different

194
00:20:45,320 --> 00:20:50,320
tradition?

195
00:20:50,320 --> 00:20:59,760
I don't know, which it's the full moon day, I guess it's what they say, when they say

196
00:20:59,760 --> 00:21:08,480
the Buddha taught the Anapana-Sati-Sukta, so those people who are keen on Anapana-Sati, have

197
00:21:08,480 --> 00:21:15,320
created a day, okay.

198
00:21:15,320 --> 00:21:25,960
Which I saw, kind of grumpy there, I didn't mean to sound grumpy, it's a good thing,

199
00:21:25,960 --> 00:21:32,000
it's good to have that, I guess a little bit grumpy, it's like wow, really, it's not

200
00:21:32,000 --> 00:21:40,040
really a holiday, like Maga-Bhuja, or Misaka-Bhuja, or Asalahapuja, these are the three big ones,

201
00:21:40,040 --> 00:21:45,840
because they're eating their old and traditional and meaningful, to have a day just for

202
00:21:45,840 --> 00:21:50,720
the sutta, that's nice, it's nothing wrong with it, but it's not on the level of a real

203
00:21:50,720 --> 00:21:53,160
Buddhist holiday.

204
00:21:53,160 --> 00:21:56,520
Is there anything special that they do in Thailand on that day?

205
00:21:56,520 --> 00:22:01,640
No, I mean this is, I don't know who which group this is, I think it's Tani-Sara-Bhuja,

206
00:22:01,640 --> 00:22:10,200
popularized that, I don't know, maybe it's his group, maybe the Dhamma-Yu-Tani-Kai, I don't know.

207
00:22:10,200 --> 00:22:36,520
Certainly not a big thing in Thailand as far as I know.

208
00:22:36,520 --> 00:22:43,200
Do you think it is important to keep attention on body in your daily life?

209
00:22:43,200 --> 00:22:46,040
Well not meditating rather than thoughts.

210
00:22:46,040 --> 00:22:53,040
One can understand on each adhukha, anata, through thoughts too, is well not meditating.

211
00:22:53,040 --> 00:22:56,120
Question is, while not meditating, can one give importance to thoughts or should one

212
00:22:56,120 --> 00:23:11,720
confine oneself to the body?

213
00:23:11,720 --> 00:23:25,920
We tend to recommend doing the body, it's easier when you're standing, walking, sitting, and

214
00:23:25,920 --> 00:23:26,960
mind, and that's for most people, for some people the mind is easier, you know, some people

215
00:23:26,960 --> 00:23:35,080
benefit more from focusing on one or another at the Satipatana, so as they know, no, there's

216
00:23:35,080 --> 00:23:40,760
no rules like that, that should this should that.

217
00:23:40,760 --> 00:23:49,080
Satipatana are like for weapons that you have in your gorilla soldier fighting a messy

218
00:23:49,080 --> 00:23:56,720
war, and so it's not neat, it's messy.

219
00:23:56,720 --> 00:24:00,120
Sometimes like this, sometimes like that, you have to change tactics depending on the

220
00:24:00,120 --> 00:24:08,120
enemy, and so on, depending on the situation, you have to be clever like a boxer, to know

221
00:24:08,120 --> 00:24:16,760
when the duck can win, to weave, and when to punch, and when to jab, and you have to know

222
00:24:16,760 --> 00:24:29,960
the long game, you have to be patient, okay, expect a knockout in one punch.

223
00:24:29,960 --> 00:24:37,240
It's kind of like a war of attrition, it's not what it's like when it's having, you bleed

224
00:24:37,240 --> 00:24:42,920
each other, you bleed your enemies to death, starve your enemies to death, that's really

225
00:24:42,920 --> 00:25:11,480
what it is we're starving our defilements, we're going to note them to death, what is this

226
00:25:11,480 --> 00:25:16,520
going to be that I've answered all the questions, possible and no one has more questions,

227
00:25:16,520 --> 00:25:22,120
our viewership is down, right, fewer people are watching, which is fine, it's expected,

228
00:25:22,120 --> 00:25:30,840
you know, it's got time to watch some funny monk for every day, got things they need

229
00:25:30,840 --> 00:25:38,440
to be doing, that's fine, this is just sort of a hello, tomorrow we'll do another dumber

230
00:25:38,440 --> 00:25:46,520
panda, but again that's not going to be broadcast live, it'll be audio live, is tomorrow

231
00:25:46,520 --> 00:25:55,320
yet, tomorrow is another dumber panda, so that's enough, that's all, good night, thank you.

232
00:25:55,320 --> 00:26:00,520
There was one more question, if you had to type.

233
00:26:00,520 --> 00:26:07,480
Hi Bante, is an hour hand protected from physical harm only during meditating or always?

234
00:26:07,480 --> 00:26:13,000
They're not protected from physical harm even when meditating, now there's these stories

235
00:26:13,000 --> 00:26:22,840
about when you enter into John or Samapati of some sort, you're invincible, but that's

236
00:26:22,840 --> 00:26:28,200
nothing to do with being an hour hand to give us per se, it's to do with the state of attainment,

237
00:26:28,200 --> 00:26:36,440
the state of meditation, even non-hour hands could theoretically have that sort of imperviousness,

238
00:26:36,440 --> 00:26:42,840
it's not exactly or directly related to their state of being an hour hand, it's the state of

239
00:26:42,840 --> 00:26:51,080
meditation that they go into, didn't the Buddha hurt his foot or have some sort of an injury after

240
00:26:51,080 --> 00:26:57,640
he was enlightened? Yeah, Mogulana, the most powerful meditative monk who had the most powerful

241
00:26:57,640 --> 00:27:05,080
attainment was beaten almost to death, and all the bones in his body broke in the pyramid,

242
00:27:05,080 --> 00:27:10,760
he had horrible karma, he had tortured his parents, beaten his parents because he didn't want to

243
00:27:10,760 --> 00:27:20,040
take care of him, he went to hell for that. This is a follow-up question from Timo,

244
00:27:20,760 --> 00:27:25,320
I guess I was looking for help regarding the seesaw motion between the practice and the lay life,

245
00:27:25,320 --> 00:27:31,080
I think questions seemed kind of conceited, also sorry Robin for the long questions, don't be

246
00:27:31,080 --> 00:27:39,240
sorry, wasn't even a question, where's your question? The question is what sort of help can

247
00:27:39,240 --> 00:27:44,920
you give me for understanding, we're dealing with the seesaw motion between the practice and the lay

248
00:27:44,920 --> 00:27:57,080
life? I don't know, I don't like general help questions either, as they're too complicated,

249
00:27:57,080 --> 00:28:01,160
it's like you want me to, you know, you're not alone, many people ask these sorts of questions,

250
00:28:01,160 --> 00:28:06,760
but it's like you want me to plan your life out, you know, general advice, it's too difficult,

251
00:28:07,320 --> 00:28:12,440
I don't know you, I don't know how to read the seagullovada supta, give you some general advice,

252
00:28:14,040 --> 00:28:20,120
digany kai at 31, I think, seagullovada supta, really good supta, but there's a lot.

253
00:28:20,120 --> 00:28:26,280
You know, because it's not that simple, I can't just tell you something that's going to help

254
00:28:26,280 --> 00:28:32,040
you with that, you have to give specific examples, like, is this right, is that right?

255
00:28:33,080 --> 00:28:37,240
And I think maybe to some extent, you already know the answer to some of the questions that

256
00:28:37,240 --> 00:28:44,040
you might ask, so you're avoiding them, you have to just look at the individual's aspects of your

257
00:28:44,040 --> 00:28:51,320
life, dealing with people, feeling, egotistical, feeling ambitious, feeling, that's where

258
00:28:51,320 --> 00:28:55,400
not egotistical, I don't know, you said that, I'm even ambitious, it was kind of like you were

259
00:28:55,400 --> 00:29:03,800
saying, and you know that that's ambition, how to deal with things is always just meditate,

260
00:29:03,800 --> 00:29:07,000
it's always just going to be learned to meditate, learn how to see them clearly, learn how to

261
00:29:07,000 --> 00:29:17,960
let go, but the details of living their life don't, I mean the ultimate advice, it just comes

262
00:29:17,960 --> 00:29:34,920
down to become a monk, if you want real answers, not an easy thing to do, apologies, probably not

263
00:29:34,920 --> 00:29:51,560
satisfied, but he's very grateful for the how to meditate books, so good, okay, enough, good night,

264
00:29:54,040 --> 00:30:01,000
thank you Robin, thank you Ponte, good night, we know another question, do we?

265
00:30:01,000 --> 00:30:09,240
Are you not that you say? No, okay, okay, good night Ponte, thank you,

