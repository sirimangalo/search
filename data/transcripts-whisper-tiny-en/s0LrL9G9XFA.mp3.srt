1
00:00:00,000 --> 00:00:04,880
I am currently quite ill, which is making me depressive.

2
00:00:04,880 --> 00:00:10,880
I used to be strong like I accepted suffering, but this time it's too many negative thoughts.

3
00:00:10,880 --> 00:00:18,440
Well, except the negative thoughts, they're not your thoughts, it's the most reassuring

4
00:00:18,440 --> 00:00:19,440
part.

5
00:00:19,440 --> 00:00:24,280
You don't have to cry over them, you don't have to be depressed over them, they're just

6
00:00:24,280 --> 00:00:27,200
thoughts.

7
00:00:27,200 --> 00:00:34,040
Just let them come, let them be, let them go, don't worry about what you used to be, how

8
00:00:34,040 --> 00:00:37,680
you used to be strong.

9
00:00:37,680 --> 00:00:44,080
If it were real, if you really were strong, you wouldn't be weak now, so it's good to see

10
00:00:44,080 --> 00:00:46,080
how weak you are inside.

11
00:00:46,080 --> 00:00:54,360
If you feel like this weakness is there now while it was there before.

12
00:00:54,360 --> 00:00:57,720
The potential to become weak was there.

13
00:00:57,720 --> 00:01:04,600
The only way to be free from this sort of state is not to strive to have the same state

14
00:01:04,600 --> 00:01:11,080
you had before, but to give up both states, to be okay with this state, to be okay with

15
00:01:11,080 --> 00:01:18,040
the way things are, remember that it's not you, it's not yours, they're just thoughts,

16
00:01:18,040 --> 00:01:19,680
they're just states.

17
00:01:19,680 --> 00:01:26,880
They're not something to be controlled, they're not something to be concerned about, they're

18
00:01:26,880 --> 00:01:33,240
not something to be upset over.

19
00:01:33,240 --> 00:01:37,720
The illness itself is also not something that should lead you to depression, you shouldn't

20
00:01:37,720 --> 00:01:44,160
let the illness lead you to depression, there's no reason for it to make you depressed.

21
00:01:44,160 --> 00:01:52,160
illness is a great way to live your life, it's a unique opportunity that you have, because

22
00:01:52,160 --> 00:02:02,800
reality is always, no matter who you are, it's always only six things, seeing, hearing,

23
00:02:02,800 --> 00:02:09,680
smelling, tasting, feeling, thinking, that's reality.

24
00:02:09,680 --> 00:02:16,280
The biggest problem is when our partiality is in line with our reality, when we get what

25
00:02:16,280 --> 00:02:35,120
we want, because our understanding of reality is so biased, that we can't help, but it

26
00:02:35,120 --> 00:02:41,800
can't help, but lead us to suffering, when things are otherwise, in most cases, it can't

27
00:02:41,800 --> 00:02:46,720
help but lead us to suffering, and when it does that, then we're forced to re-examine

28
00:02:46,720 --> 00:02:53,080
the way we look at things, or else to just follow after it and become depressed and even

29
00:02:53,080 --> 00:03:01,120
whatever suicidal or so on.

30
00:03:01,120 --> 00:03:05,120
And we have to ask these questions, and we have to come and seek, and that's why people

31
00:03:05,120 --> 00:03:11,160
come to practice Buddhism, is because they get sick, or because they have problems in life.

32
00:03:11,160 --> 00:03:15,760
But when a person doesn't have problems, when a person is healthy, they become negligent,

33
00:03:15,760 --> 00:03:20,680
they forget this, and they say, yes, yes, this is the right way to live, partial, liking

34
00:03:20,680 --> 00:03:24,040
this, liking that, disliking this, disliking this, like like as when I dislike this, it goes

35
00:03:24,040 --> 00:03:29,720
away, when I like this, it comes, and so we think that's the nature of reality, nature

36
00:03:29,720 --> 00:03:33,600
of reality is when you want things, you get them, when you don't want things, you push

37
00:03:33,600 --> 00:03:42,800
them away, don't like sickness, go to the doctor, once the central pleasure, go find a

38
00:03:42,800 --> 00:03:49,800
partner, as well, you go find good food or music or so, and we think that's the nature

39
00:03:49,800 --> 00:03:56,640
of reality, but when we become sick, we have a unique experience, when we become really sick,

40
00:03:56,640 --> 00:04:02,920
when we can't escape it, then we get to see how ridiculous it all is, how absurd

41
00:04:02,920 --> 00:04:12,760
this belief is, how absurd this game that we're playing is, and we open up to the truth

42
00:04:12,760 --> 00:04:22,000
of life, the objective truth, that reality is simply experience, we lose the idea of

43
00:04:22,000 --> 00:04:32,360
my body, my human body, my male body, my female body, my beautiful body, my ugly body,

44
00:04:32,360 --> 00:04:42,680
my tall body, my time, you may just see a lump of flesh, which is really what it is,

45
00:04:42,680 --> 00:04:48,400
it's the lump of rotting flesh that's going to pass away, and then it doesn't make

46
00:04:48,400 --> 00:04:53,560
us trouble, it doesn't cause us problems, it just is what it is, even pain can cause us

47
00:04:53,560 --> 00:05:04,520
problem, what is it, it's just pain, it's not bad, when you can get to that, then nothing

48
00:05:04,520 --> 00:05:34,480
can cause you suffering.

