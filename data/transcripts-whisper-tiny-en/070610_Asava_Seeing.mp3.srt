1
00:00:00,000 --> 00:00:14,020
Today, I thought I would start a series of talks on the As

2
00:00:14,020 --> 00:00:21,200
of at the taint for the fermentations, which is just another way of saying the defilements,

3
00:00:21,200 --> 00:00:24,480
the bad things which exist in our hearts.

4
00:00:24,480 --> 00:00:31,160
That the Lord Buddha gave specific teaching on what he called the Asava.

5
00:00:31,160 --> 00:00:37,960
It's a classification of defilements into three parts, which are gamma, asava, the fermentations

6
00:00:37,960 --> 00:00:46,520
in our defilements in regards to assentuality, and then gamma, asava, the defilements in which

7
00:00:46,520 --> 00:00:52,520
have to do with becoming, being this or being that or not being this or not being that.

8
00:00:52,520 --> 00:00:59,920
And then Aweja, asava, the defilements would have to do with simple ignorance.

9
00:00:59,920 --> 00:01:05,400
So there are, in this classification, there are three kinds of defilements, ones which have to

10
00:01:05,400 --> 00:01:12,440
do with our attachment to sensuality, liking good sights and sounds and smells and disliking

11
00:01:12,440 --> 00:01:16,480
bad sights and sounds and smells and so on.

12
00:01:16,480 --> 00:01:21,880
And then the ones having to do one thing to be something, one thing to be this, one thing

13
00:01:21,880 --> 00:01:32,040
to be that, attachment to status or position and so on, attachment to not being this or

14
00:01:32,040 --> 00:01:39,640
not being that, something, not being something in particular.

15
00:01:39,640 --> 00:01:44,120
And then the defilements which have to do with simple ignorance, simply not knowing,

16
00:01:44,120 --> 00:01:49,360
so not knowing what is suffering, what is the truth of cause of suffering and so on.

17
00:01:49,360 --> 00:01:55,200
Through simple ignorance, we become defile, we do bad things, we say bad things, simply

18
00:01:55,200 --> 00:01:57,000
out of ignorance.

19
00:01:57,000 --> 00:02:04,440
So these three classifications, these are three reasons why bad things arise.

20
00:02:04,440 --> 00:02:10,240
And so the Lord would give a teaching on how to be free from these defilements, these

21
00:02:10,240 --> 00:02:15,960
things which, in the palliative terms, it means they pickle or they cause the mind

22
00:02:15,960 --> 00:02:23,920
to become useless, to become rotten, to think of it as it makes our mind become rotten,

23
00:02:23,920 --> 00:02:28,760
like a rotten apple and a rotten tomato.

24
00:02:28,760 --> 00:02:36,200
And the Lord would have gave seven distinct ways of overcoming these defilements, these

25
00:02:36,200 --> 00:02:44,160
things which cause our minds to become rotten, which are called the taints or the fermentations.

26
00:02:44,160 --> 00:02:52,840
And these seven ways are put together, they are the complete description of the meditators

27
00:02:52,840 --> 00:02:59,960
life, the life of a monk or a nun or simply a simple meditator.

28
00:02:59,960 --> 00:03:08,040
These seven practices are the way to become free from taints and defilements, free from

29
00:03:08,040 --> 00:03:12,560
all sorts of unwholesome things which exist inside of ourselves.

30
00:03:12,560 --> 00:03:17,880
We make our minds pure to make our hearts clean.

31
00:03:17,880 --> 00:03:24,040
So today I'll list off to seven, but I'll try to go into one of the specific teachings

32
00:03:24,040 --> 00:03:28,760
every day, one or two, depending on the time.

33
00:03:28,760 --> 00:03:38,800
So the first one is Tessanapa, Tapa, the Asava, the defilements, the taints which are

34
00:03:38,800 --> 00:03:42,880
to be removed through scene.

35
00:03:42,880 --> 00:03:51,600
The second one, but the samwara pahat pahat pahat, which the taints which are to be removed

36
00:03:51,600 --> 00:03:53,960
through guarding.

37
00:03:53,960 --> 00:04:03,440
So through seeing, through guarding, number three, patisaywana, through reflecting.

38
00:04:03,440 --> 00:04:17,520
Number four, Atiwasana, through bearing, or in during, number five, patisayana, through

39
00:04:17,520 --> 00:04:22,440
running away or through avoiding.

40
00:04:22,440 --> 00:04:32,760
Number six, we know it through abandoning, and number seven, pahana, through developing.

41
00:04:32,760 --> 00:04:40,120
These are the seven, they're verbs, there's seven actions which we undertake to rid ourselves

42
00:04:40,120 --> 00:04:45,120
of all unwholesome tendencies, to make our minds pure and bright and clear and calm

43
00:04:45,120 --> 00:04:49,920
and clean at all times.

44
00:04:49,920 --> 00:04:54,920
So the first one, Tessanapa, Tapa, the ones which are to be gotten rid of through seeing.

45
00:04:54,920 --> 00:05:00,200
The way to get rid of taints, that's simply by seeing.

46
00:05:00,200 --> 00:05:07,480
And so the Lord who goes into a long explanation of what it means to be someone who gets,

47
00:05:07,480 --> 00:05:14,120
who is able to remove the aswara, the taints which exist inside.

48
00:05:14,120 --> 00:05:16,080
And he compares two different types of people.

49
00:05:16,080 --> 00:05:21,800
The first type of person is someone who is untrained, who doesn't, isn't interested

50
00:05:21,800 --> 00:05:26,760
in going to see wise people to listen to their teachings.

51
00:05:26,760 --> 00:05:35,160
As an interested in meditation practice, is an ordinary worldling who is very much interested

52
00:05:35,160 --> 00:05:41,480
in the world and interested in things which bring sensual pleasure, things which are of

53
00:05:41,480 --> 00:05:46,680
course impermanent and suffering in non-self.

54
00:05:46,680 --> 00:05:52,560
And he says this type of person reflects on things which are not to be reflected upon.

55
00:05:52,560 --> 00:05:58,720
And fails to reflect on those things which are proper to be reflected upon.

56
00:05:58,720 --> 00:06:03,240
And the things which are not to be reflected upon are those things which, when you consider

57
00:06:03,240 --> 00:06:10,880
them, when you attend to them, when you develop them, it leads to the increasing of the

58
00:06:10,880 --> 00:06:18,000
aswara, increasing more desire, more sensual desire, more desire to be this, to be that

59
00:06:18,000 --> 00:06:25,240
ambition, come things, and more delusion, more ignorance, which develops our ignorance

60
00:06:25,240 --> 00:06:30,200
or create, for instance, taking drugs or alcohol or so on.

61
00:06:30,200 --> 00:06:37,280
So people who go around breaking the five precepts or even not breaking the five precepts,

62
00:06:37,280 --> 00:06:47,080
but being unrestrained in their activities, in terms of sensuality, be it in romance

63
00:06:47,080 --> 00:06:51,880
or be it in food or be it in entertainment or so on.

64
00:06:51,880 --> 00:06:56,280
Or people who are unrestrained in their ambitions or unrestrained in their delusions, in

65
00:06:56,280 --> 00:07:02,320
their ignorance, they consider the wrong things, they consider those things when they

66
00:07:02,320 --> 00:07:10,920
see something, they reach for it, they run after, and they're always looking for more

67
00:07:10,920 --> 00:07:18,360
looking for something which is pleasurable, something which will bring them sensual pleasure.

68
00:07:18,360 --> 00:07:25,600
The Lord Buddha's other type of person is a person who doesn't attend upon those things

69
00:07:25,600 --> 00:07:29,440
which are not to be attended upon, and it ends reflects upon those things which are to

70
00:07:29,440 --> 00:07:31,800
be reflected upon.

71
00:07:31,800 --> 00:07:36,760
And those things which should be reflected upon those things which decrease and do away

72
00:07:36,760 --> 00:07:39,360
with the aswara.

73
00:07:39,360 --> 00:07:48,240
So attending to mindfulness, developing morality and concentration in wisdom, developing

74
00:07:48,240 --> 00:07:54,560
the mind to become, to see clearly, to do away with sensuality, to see that happy feelings,

75
00:07:54,560 --> 00:07:58,480
neutral feelings, suffering feelings, they're all impermanent.

76
00:07:58,480 --> 00:08:07,160
To see that becoming this or becoming that, whatever we become, we will have to leave behind

77
00:08:07,160 --> 00:08:13,720
the end, and whatever we run away from will always in the end, catch up with us, come

78
00:08:13,720 --> 00:08:21,760
back to us again and again and again, will always be running away from that, and coming

79
00:08:21,760 --> 00:08:27,760
to see clearly means to do away with our ignorance, to see clearly the truth of suffering,

80
00:08:27,760 --> 00:08:34,560
to see why we suffer, to see the cessation and the path out of the way out of suffering.

81
00:08:34,560 --> 00:08:40,600
So the Lord Buddha compared these two types of people and then said, how is it that the

82
00:08:40,600 --> 00:08:47,520
person who attends carefully, who considers things in the right man or not dwelling

83
00:08:47,520 --> 00:08:50,000
in the past or worrying about the future?

84
00:08:50,000 --> 00:08:54,200
How is it that they come to be one who sees?

85
00:08:54,200 --> 00:08:58,640
So this is the first part of this through seeing.

86
00:08:58,640 --> 00:09:07,040
And here's the Lord Buddha taught the Four Noble Truths, and we can explain that the

87
00:09:07,040 --> 00:09:13,480
Four Noble Truths are something like the core of Buddhism or the very basis of what it

88
00:09:13,480 --> 00:09:20,520
means, what is meant by the word Buddhism or the Buddha's teaching, is the teaching with

89
00:09:20,520 --> 00:09:26,240
the Lord Buddha gave first, when he first said in motion the Wheel of Dhamma, after

90
00:09:26,240 --> 00:09:36,480
the Lord Buddha became enlightened under the tree, the Bodhi tree in Bhutakaya, in

91
00:09:36,480 --> 00:09:45,480
Makatai, in India, and then he walked the 120 kilometers or however far it was.

92
00:09:45,480 --> 00:09:55,320
He walked to Isipatana in Saranat, which is just outside of Varanasi, and he taught the

93
00:09:55,320 --> 00:09:56,320
five ascetics.

94
00:09:56,320 --> 00:10:02,920
He's the five meditators of the five ascetics who followed after the Lord Buddha.

95
00:10:02,920 --> 00:10:08,760
The Bodhisatta, when he left home, before he became Buddha, when he left home, he practiced

96
00:10:08,760 --> 00:10:16,400
for six years torturing himself, and these five monks went after him thinking that these

97
00:10:16,400 --> 00:10:20,800
five ascetics, went with him thinking that he would, if he became enlightened, he would

98
00:10:20,800 --> 00:10:25,880
then share the teaching with them, and they would be able to seek clearly, following

99
00:10:25,880 --> 00:10:26,880
after him.

100
00:10:26,880 --> 00:10:30,600
He would be his first disciples.

101
00:10:30,600 --> 00:10:35,360
But at that time, the accepted practice was to torture oneself, either to live in the

102
00:10:35,360 --> 00:10:41,360
household life in luxury or to go in the forest and torture oneself physically, and the

103
00:10:41,360 --> 00:10:45,560
Lord Buddha, after six years, realized that torturing himself physically, did nothing to

104
00:10:45,560 --> 00:10:50,920
bring him closer to wisdom and understanding, that had no casual relationship between

105
00:10:50,920 --> 00:10:58,720
casual relationship with the realization of enlightenment.

106
00:10:58,720 --> 00:11:02,360
And so he gave it up, and he gave up the practice of torturing himself and found what

107
00:11:02,360 --> 00:11:07,400
is called the middle way, which is not torturing oneself and not indulging in central

108
00:11:07,400 --> 00:11:08,400
pleasure.

109
00:11:08,400 --> 00:11:15,320
It's being mindful, being mindful, having creating morality, doing away with all

110
00:11:15,320 --> 00:11:21,840
on wholesome states, clearing the mind, calming the mind down, focused mind, concentration,

111
00:11:21,840 --> 00:11:30,280
and developing and understanding, which is simply in line with the reality around us.

112
00:11:30,280 --> 00:11:35,760
But when he accepted the middle, when he started to practice the middle way, these five

113
00:11:35,760 --> 00:11:41,280
ascetics thought he was going back to the his old practice of indulging in luxury, and

114
00:11:41,280 --> 00:11:46,320
they knew that this wasn't surely wasn't the way out of suffering, so they left him, and

115
00:11:46,320 --> 00:11:51,920
they went to Varanasi to Isipatana, so the Lord Buddha walked back there to find them,

116
00:11:51,920 --> 00:11:53,920
and to teach them the teaching.

117
00:11:53,920 --> 00:11:57,680
Now at first, of course, they weren't interested.

118
00:11:57,680 --> 00:12:00,080
This is an interesting point.

119
00:12:00,080 --> 00:12:06,640
Nowadays we can see that there's a parallel with first disciples of the Lord Buddha that

120
00:12:06,640 --> 00:12:09,960
nowadays most people don't want to see the Buddha's teaching.

121
00:12:09,960 --> 00:12:13,640
They don't want to hear teachings on suffering.

122
00:12:13,640 --> 00:12:19,120
They don't want to hear about how sensual pleasures are not going to satisfy them.

123
00:12:19,120 --> 00:12:23,600
They don't want to hear about how the world is not going to bring them happiness, and

124
00:12:23,600 --> 00:12:28,000
they don't want to hear about how they have to give up their attachment to sensuality

125
00:12:28,000 --> 00:12:29,800
in order to find happiness.

126
00:12:29,800 --> 00:12:36,320
Most people nowadays want to find happiness in these things, or else we see the opposite

127
00:12:36,320 --> 00:12:41,240
just like these ascetics, as people have gone out of their way to torture themselves in

128
00:12:41,240 --> 00:12:42,240
many different ways.

129
00:12:42,240 --> 00:12:47,440
Either the body or the mind, and people developing all sorts of wrong views, all sorts

130
00:12:47,440 --> 00:12:56,360
of views based on science, what they call Western science, or even Eastern science.

131
00:12:56,360 --> 00:13:03,000
They have the different sciences in the world, there's different views, different understandings

132
00:13:03,000 --> 00:13:04,000
of the world.

133
00:13:04,000 --> 00:13:08,560
When we develop these views and when it comes to the teaching of the Lord Buddha they're

134
00:13:08,560 --> 00:13:11,960
not really interested, they don't really see the point.

135
00:13:11,960 --> 00:13:16,320
Most people nowadays will believe that at the moment of death there's nothing more.

136
00:13:16,320 --> 00:13:22,400
At the time of the Buddha this view is prevalent as well, that death is the end of existence,

137
00:13:22,400 --> 00:13:26,160
at the moment of death there is nothing.

138
00:13:26,160 --> 00:13:32,200
And this of course comes with an understanding based on what is called science of the death

139
00:13:32,200 --> 00:13:40,320
of another person because of course none of us can empirically measure our own death until

140
00:13:40,320 --> 00:13:47,560
it comes, but we can measure someone else's death in all appearances.

141
00:13:47,560 --> 00:13:55,040
It looks as though the person in front of us has died, has ceased to exist.

142
00:13:55,040 --> 00:13:59,920
But empirically of course the things are quite different because empirically inside we

143
00:13:59,920 --> 00:14:05,640
can see that the objects around us are rising and ceasing, seeing, hearing, smelling, tasting,

144
00:14:05,640 --> 00:14:06,640
feeling and thinking.

145
00:14:06,640 --> 00:14:11,320
They're arising and ceasing all the time and there's no cessation, there's no ceasing

146
00:14:11,320 --> 00:14:13,200
to exist.

147
00:14:13,200 --> 00:14:19,960
Now at the moment of death as to what happens is up to speculation and that's all it really

148
00:14:19,960 --> 00:14:23,720
is, it's speculation that at the moment of death there is nothing more.

149
00:14:23,720 --> 00:14:31,280
Somehow this process of arising and ceasing somehow stops and of course we all have to

150
00:14:31,280 --> 00:14:35,240
see for ourselves when the time comes.

151
00:14:35,240 --> 00:14:42,720
But at any rate these five ascetics were an example of how these sort of wrong views

152
00:14:42,720 --> 00:14:45,600
can get in the way because at first they weren't willing to listen to the Lord Buddha

153
00:14:45,600 --> 00:14:48,720
and this is also very common nowadays.

154
00:14:48,720 --> 00:14:55,680
People not interested in hearing or not interested in taking on the teaching, not interested

155
00:14:55,680 --> 00:15:01,840
in trying it out because of their own views and their own way of looking at things.

156
00:15:01,840 --> 00:15:06,960
The Lord Buddha was able to convince the five ascetics that something was indeed different.

157
00:15:06,960 --> 00:15:12,400
At the very least he had changed in some way and perhaps they could listen to what he

158
00:15:12,400 --> 00:15:22,600
had to say and they became malleable and they were able to let go of their view long enough

159
00:15:22,600 --> 00:15:28,960
to listen, at least to consider what the Lord Buddha was going to say.

160
00:15:28,960 --> 00:15:31,960
And first he taught that there was a middle way and then he explained what were the four

161
00:15:31,960 --> 00:15:35,480
noble truths and this became the core of Buddhism.

162
00:15:35,480 --> 00:15:40,120
So this is what we have to see and when we practice the Buddha's teaching, what does

163
00:15:40,120 --> 00:15:44,840
it mean to do away with evil unwholesome states through seeing?

164
00:15:44,840 --> 00:15:51,320
It means to see four things, to see the cause of suffering, to see suffering, to see what

165
00:15:51,320 --> 00:15:55,520
is the truth of suffering, to see what is the cause of suffering, to see the cessation

166
00:15:55,520 --> 00:16:00,800
of suffering and to see the path, the practice which leads to the cessation of suffering.

167
00:16:00,800 --> 00:16:07,880
These four things means to do away with unwholesome states through seeing and how does it

168
00:16:07,880 --> 00:16:08,880
work.

169
00:16:08,880 --> 00:16:13,760
First of all, it's important to understand this being the core of Buddhism that it's clear

170
00:16:13,760 --> 00:16:20,320
that the Lord Buddha taught suffering and he taught all about suffering and even though

171
00:16:20,320 --> 00:16:24,120
the word suffering is only an English word, it really is an appropriate word.

172
00:16:24,120 --> 00:16:29,320
It's important to not to water down the Lord Buddha's teaching or try to find a way around

173
00:16:29,320 --> 00:16:34,760
it to make people feel happy, to make people feel comforted that actually the Buddha

174
00:16:34,760 --> 00:16:37,760
really taught happiness.

175
00:16:37,760 --> 00:16:43,200
The truth is the Lord Buddha taught the ultimate happiness, the ultimate freedom from suffering.

176
00:16:43,200 --> 00:16:49,360
He taught that real happiness is simply having no suffering, just like perfect health

177
00:16:49,360 --> 00:16:56,200
for a doctor, perfect health is freedom from sickness, so the doctor will never be concerned

178
00:16:56,200 --> 00:17:00,680
very much about perfect health, he'll always be concerned about the sickness, if a person

179
00:17:00,680 --> 00:17:06,480
is healthy then there's nothing that the doctor has to do to go in and fix, but for a person

180
00:17:06,480 --> 00:17:12,440
who is sick or whatever sickness is exist, it is the doctor's duty to go in and to explain

181
00:17:12,440 --> 00:17:20,320
and to give medicine and have the patient take medicine in order to be free from the sickness.

182
00:17:20,320 --> 00:17:26,320
So the Lord does exactly like a doctor and it will accept he had a much more difficult

183
00:17:26,320 --> 00:17:30,800
time than the doctor, doctors will have a difficult time with patients who don't want

184
00:17:30,800 --> 00:17:35,560
to believe that they are sick, who don't want to accept the fact that they have sickness

185
00:17:35,560 --> 00:17:42,520
but of course the Buddha had a terrible time because no one wanted to consider that they

186
00:17:42,520 --> 00:17:48,120
were sick, no one wanted to think that these things inside of them were wrong, that the views

187
00:17:48,120 --> 00:17:52,920
and the conceits and their attachments and their cravings and their one things and all these

188
00:17:52,920 --> 00:18:00,760
things were wrong and of course in those times as just as today people only want to follow

189
00:18:00,760 --> 00:18:08,960
after their desires and of course that's what they want desires wanting, so they have

190
00:18:08,960 --> 00:18:16,320
this wanting and as a result of this wanting any talk about giving up the things which

191
00:18:16,320 --> 00:18:22,400
they want is of course an unwelcome thing and so I find the people who are most able

192
00:18:22,400 --> 00:18:28,840
to understand the Buddhist teachings are those who have had their hopes dashed or had

193
00:18:28,840 --> 00:18:37,160
their love, their objects of affection disappear, they people or objects or so on, people

194
00:18:37,160 --> 00:18:45,800
who have seen what it means to be without the objects of our attachment, people who have

195
00:18:45,800 --> 00:18:51,680
lost loved ones, people who have gone through states of loss or suffering, people who

196
00:18:51,680 --> 00:18:56,960
have sicknesses, people who are dying, these people are the ones who are able to understand

197
00:18:56,960 --> 00:19:03,080
yes what the Buddha said was really true, there is great sickness here, even people who

198
00:19:03,080 --> 00:19:08,520
are in perfect health physically but when they lose a loved one they might kill themselves,

199
00:19:08,520 --> 00:19:12,920
simply because they can't stand to live without the other person or without this or without

200
00:19:12,920 --> 00:19:18,520
that and so these people are able to see that there is a great sickness which they

201
00:19:18,520 --> 00:19:26,680
are not able to which medicine which they by themselves are not able to overcome and

202
00:19:26,680 --> 00:19:33,440
so they come to say well who is it or what is it that will make us be happy now and of

203
00:19:33,440 --> 00:19:38,880
course the objects of the sense are not going to make them happy at that time, they can

204
00:19:38,880 --> 00:19:45,440
drown their sorrows in alcohol or in drugs for so long and if they are smart they will

205
00:19:45,440 --> 00:19:49,760
be able to realize that that is not going to leave them out, if they are not able to

206
00:19:49,760 --> 00:19:56,560
realize that then they will ground themselves until they die and go on to be reborn in a very

207
00:19:56,560 --> 00:20:08,720
terrible place but people in general in the world people who are successful, who have

208
00:20:08,720 --> 00:20:16,080
everything they want, who seem to have everything they want are very good at convincing

209
00:20:16,080 --> 00:20:24,240
themselves and trying to convince everyone around them and refusing any idea to the contrary

210
00:20:24,240 --> 00:20:30,720
that they themselves have great happiness, that they themselves are always happy,

211
00:20:30,720 --> 00:20:39,080
that they themselves are not moved, not affected by suffering and whenever suffering

212
00:20:39,080 --> 00:20:44,560
arises they quickly forget about it, they quickly put it behind them during the time

213
00:20:44,560 --> 00:20:51,960
it is there they cry and they moan and they wail and they do all sorts of measures to

214
00:20:51,960 --> 00:20:59,440
become free or to forget the suffering and then when it is gone they quickly put it behind

215
00:20:59,440 --> 00:21:04,600
them and pretend it did not happen and pretend that it forget that it might happen in

216
00:21:04,600 --> 00:21:12,920
the future or do not never stop to consider that it very well might come at any time again

217
00:21:12,920 --> 00:21:16,200
and they say again to themselves that they are in great happiness, so the Lord Buddha gave

218
00:21:16,200 --> 00:21:21,480
this teaching which is very powerful and it is important that it be powerful that it

219
00:21:21,480 --> 00:21:28,800
is fucking because otherwise people will never wake themselves up, will be lost and will

220
00:21:28,800 --> 00:21:34,480
continue to destroy this world and to destroy themselves and make it a lesson less harmonious

221
00:21:34,480 --> 00:21:39,400
and peaceful place to live until we can realize that we are destroying ourselves in this

222
00:21:39,400 --> 00:21:46,400
world around us, until we can realize that we will never become free from suffering and

223
00:21:46,400 --> 00:21:52,560
never be free from the threat of suffering in the future, so the Lord Buddha taught that

224
00:21:52,560 --> 00:22:03,600
there is suffering that there is a problem with simply living our lives in indolence,

225
00:22:03,600 --> 00:22:14,440
living our lives as though there were no tomorrow or there are no consequences of our actions

226
00:22:14,440 --> 00:22:19,680
and he taught this and he taught this so that people who were able to see would be able

227
00:22:19,680 --> 00:22:29,440
to have a way out, so people who had seen suffering or who could remember when the Lord

228
00:22:29,440 --> 00:22:33,560
Buddha talked about this being suffering and that being suffering, they would be able to recollect

229
00:22:33,560 --> 00:22:37,040
the suffering which they had in their lives and how they were truly unable to deal with

230
00:22:37,040 --> 00:22:41,520
it, or when they saw suffering in other people those people were truly unable to deal with

231
00:22:41,520 --> 00:22:51,280
it and he taught this giving people an understanding of the truth of this, the truth of

232
00:22:51,280 --> 00:22:57,200
this fact that there was a problem which had to be addressed very quickly before the

233
00:22:57,200 --> 00:23:05,120
problem came to us or became so desperate that we weren't able to cure it and then he

234
00:23:05,120 --> 00:23:12,360
taught the cure or he taught them the reason why the suffering exists and how we could

235
00:23:12,360 --> 00:23:20,960
cure it, how we could remove the cause and find the cure, so the Lord Buddha taught

236
00:23:20,960 --> 00:23:28,520
the cause of suffering is called donehau or craving and you can translate it as you will

237
00:23:28,520 --> 00:23:36,080
craving is probably the best translation but it means any sort of greed or wishing or longing

238
00:23:36,080 --> 00:23:42,680
for things to be like this or not be like that or wishing for sensual desires, sensual

239
00:23:42,680 --> 00:23:50,480
pleasures and we can see this when we practice meditation quite clearly and this is what

240
00:23:50,480 --> 00:23:57,080
the Lord Buddha meant by seeing, first of all seeing suffering when we sit and we're rising

241
00:23:57,080 --> 00:24:02,680
falling we can see our minds wandering here wandering there without any rhyme or reason

242
00:24:02,680 --> 00:24:10,880
and we can see how this creates stress in the body and stress and headaches and how it

243
00:24:10,880 --> 00:24:17,120
creates a general sense of unease or when we are wishing for things or wanting for things

244
00:24:17,120 --> 00:24:23,400
to be like this or when we hold on, when we have a having and we really enjoy it and

245
00:24:23,400 --> 00:24:28,440
really like the feeling how this then leads us to suffer when the feeling disappears and

246
00:24:28,440 --> 00:24:33,920
we find in the morning our practice was very peaceful but then in the afternoon when the

247
00:24:33,920 --> 00:24:40,480
feeling went away our practice was very horrible and we can see in this way how our craving

248
00:24:40,480 --> 00:24:44,760
our wanting for things to be like this or be like that is really and truly the cause

249
00:24:44,760 --> 00:24:55,760
we're suffering for and this is what the Lord Buddha meant by Tassanapahatapah to remove

250
00:24:55,760 --> 00:25:01,080
these things through seeing through simply seeing this next time we feel happy we won't

251
00:25:01,080 --> 00:25:07,880
be so quick to reach for it or grab for it or hold on to it we will be more wary and see

252
00:25:07,880 --> 00:25:12,760
the happy feeling simply for what it is is something that's impermanent and therefore unsatisfying

253
00:25:12,760 --> 00:25:19,600
and satisfying and we won't hold on to it as me is mine is under my control and when we see

254
00:25:19,600 --> 00:25:30,680
like this we will not have to then come in contact with the suffering we will then not

255
00:25:30,680 --> 00:25:40,200
have to become party to the suffering which results from craving we will be able to see through

256
00:25:40,200 --> 00:25:48,840
all pleasurable states all unpleasurable states all states which arise in our body or

257
00:25:48,840 --> 00:25:55,480
our mind will be able to see through them and not be moved by them or be pushed into

258
00:25:55,480 --> 00:26:03,080
craving and therefore clinging and therefore suffering and this is the cessation of suffering

259
00:26:03,080 --> 00:26:09,040
when there is no more craving when we are no longer reaching out for these things because

260
00:26:09,040 --> 00:26:14,240
we have seen how they are not truly happiness then this is the cessation of suffering there is

261
00:26:14,240 --> 00:26:19,680
no no matter how much pain we might be in because we have no attachment to happy feelings or

262
00:26:19,680 --> 00:26:26,960
unhappy feelings it can it can these feelings cannot bring suffering to us yeah wanting to be this

263
00:26:26,960 --> 00:26:32,800
wanting to be that holding on to the past to the future these things can no longer affect us

264
00:26:32,800 --> 00:26:38,720
there's no more holding on to either past or future good or back through simply seeing that

265
00:26:38,720 --> 00:26:45,360
this is this is suffering and there is a cause the causes that very holding on and the path the

266
00:26:45,360 --> 00:26:50,720
fourth noble truth which we see is the path which leads there to which is of course the path of

267
00:26:50,720 --> 00:26:56,800
mindfulness when we simply acknowledge rising falling or when we're walking and we're stepping

268
00:26:56,800 --> 00:27:03,040
right stepping left and we're able to see clearly the truth of the body and the truth of the

269
00:27:03,040 --> 00:27:09,120
mind we're able to see good states bad states suffering what is the causes of suffering we do

270
00:27:09,120 --> 00:27:15,200
this by simply watching there's a saying if you if you want to know you have to see if you

271
00:27:15,200 --> 00:27:21,520
want to see you have to look if you look you'll see if you see you'll know these three things

272
00:27:21,520 --> 00:27:27,200
always this is through seeing the first the first of the Lord Buddhist teachings on how to do

273
00:27:27,200 --> 00:27:35,200
away with the asthma and so that is the demo for today tomorrow we'll go on to the other

274
00:27:35,200 --> 00:28:02,640
parts of the teaching on how to do away with the asthma that's all for today

