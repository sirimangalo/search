1
00:00:00,000 --> 00:00:07,320
Okay, good evening everyone. Welcome to our live broadcast November 24th

2
00:00:10,360 --> 00:00:14,840
So this daily thing is working out well today is

3
00:00:17,880 --> 00:00:22,000
Excuse me the shaving day in Thailand tomorrow's the full moon

4
00:00:23,400 --> 00:00:25,400
I think

5
00:00:25,400 --> 00:00:33,080
Which means I may be today it depends on which there's two traditions in Thailand one strict and one not so strict

6
00:00:35,160 --> 00:00:39,640
One strict in one traditional, I think you could say because technically

7
00:00:41,280 --> 00:00:45,760
Sometimes the moon is full after midnight sometimes it's full before midnight

8
00:00:46,480 --> 00:00:49,640
but the traditional group goes more by

9
00:00:49,640 --> 00:00:53,960
Calculations, then my exact hour

10
00:01:00,960 --> 00:01:05,400
I don't know which one I'm saying tomorrow is but but many of the full months are still the same

11
00:01:09,520 --> 00:01:11,520
Shaving today

12
00:01:11,520 --> 00:01:13,520
I

13
00:01:15,320 --> 00:01:17,320
Want some months

14
00:01:17,560 --> 00:01:22,040
It's a big thing in Thailand some monks do shave more regularly and Sri Lanka they shave

15
00:01:22,600 --> 00:01:24,600
Same every couple days

16
00:01:27,640 --> 00:01:29,640
It's our kimon which one's better

17
00:01:30,320 --> 00:01:35,960
If you shave like once a week then or once every few days you can shave everything shave your beard and

18
00:01:35,960 --> 00:01:41,960
You head all at once and then it never gets so long and it's a bit of a drag because every month

19
00:01:42,680 --> 00:01:44,680
Even once a month, and it's a lot of hair

20
00:01:47,760 --> 00:01:50,400
Seems like less bust shave once a month

21
00:01:53,960 --> 00:01:56,960
Probably get the inevitable question why do months shame I

22
00:01:58,360 --> 00:02:00,360
Think it's cleaner

23
00:02:00,360 --> 00:02:09,320
And it's less bust no ego attachment to the hair and hairstyle because monks were styling their hair

24
00:02:10,320 --> 00:02:11,840
beginning sort of

25
00:02:11,840 --> 00:02:15,400
And that was why they would have been stated to rule not to let it get longer

26
00:02:16,720 --> 00:02:20,200
Longer we can get this long long to two fingers long

27
00:02:23,960 --> 00:02:28,480
Or or two months if you haven't shaved into months you have to shave

28
00:02:28,480 --> 00:02:32,400
And if it gets longer than the fingers then you have to

29
00:02:35,000 --> 00:02:43,800
So that that also apparently goes for the beard or technically goes for the beard so that some monks were testing that there are monks who that their beard grow out

30
00:02:44,760 --> 00:02:47,000
It's still two months. You have to shave your beard

31
00:02:48,000 --> 00:02:50,000
That's why the beard down after two months

32
00:02:50,000 --> 00:02:57,400
A lot of beard mostly we don't go by that if I go by that one in Thailand

33
00:02:58,640 --> 00:03:04,000
If a real thing against facial hair in Thailand because most type people have very little facial hair

34
00:03:06,600 --> 00:03:12,120
Still it's your rogue if you have facial hair big on appearances

35
00:03:12,120 --> 00:03:19,120
Fortunately

36
00:03:22,800 --> 00:03:25,280
So good evening

37
00:03:25,280 --> 00:03:29,600
They I taught two people how to meditate or one person I guess this

38
00:03:30,120 --> 00:03:34,880
Friend I'm talking about my my university friend got one of her friends

39
00:03:36,520 --> 00:03:41,560
Come and learn how to meditate who also is struggling actually said suffers from many of the same issues

40
00:03:41,560 --> 00:03:43,560
as she does

41
00:03:44,560 --> 00:03:46,560
What was a very good minute

42
00:03:46,560 --> 00:03:50,840
New person who's picked up like nothing some people most people

43
00:03:51,400 --> 00:03:55,480
Most of us and I include myself and this aren't able to pick it up very quickly

44
00:03:55,480 --> 00:03:58,800
But the other person gets it really quickly like

45
00:04:00,200 --> 00:04:05,840
Her synchronicity, you know, it's a simple thing to can tell when they do the walking she's able to say step

46
00:04:06,560 --> 00:04:07,720
Right

47
00:04:07,720 --> 00:04:13,720
Yep, so she's in time with that's rare actually for someone who hasn't done it. It's actually a bit of a skill

48
00:04:14,760 --> 00:04:18,320
But I was impressed by I just that simple

49
00:04:19,320 --> 00:04:22,360
And then we did you're sat very still when I did my

50
00:04:22,720 --> 00:04:25,120
Yeah, that was through the meditation after

51
00:04:25,120 --> 00:04:35,120
You go one person at a time

52
00:04:43,120 --> 00:04:45,120
I'm about this

53
00:04:46,120 --> 00:04:48,120
Charity thing that we're looking

54
00:04:48,120 --> 00:04:55,520
And some of the money got in touch with me and there's a kid. There's a children's home in Tampa that they've

55
00:04:57,000 --> 00:04:59,000
involved

56
00:04:59,000 --> 00:05:01,000
with

57
00:05:01,000 --> 00:05:03,680
But Robin you sent me something about dogs actually

58
00:05:04,720 --> 00:05:11,040
If there were something about cats or just pets in general because my stepmother fathers a real cat person

59
00:05:11,040 --> 00:05:20,840
Yeah, I guess dogs have had more trouble, right bigger harder to take care of stuff for more

60
00:05:22,240 --> 00:05:26,280
There may be but I can't take care of themselves like cats. Yeah, maybe I'll get

61
00:05:27,440 --> 00:05:31,840
I'm not sure there and there may be something for cats. I did I just I didn't look that far

62
00:05:31,840 --> 00:05:35,960
I was thinking about dogs today, and I just looked it up. I can check for cats too

63
00:05:35,960 --> 00:05:47,080
I was really I was really thinking about helping humans. Okay. Yeah, I guess humans are important too

64
00:05:52,240 --> 00:05:55,240
They have to look after the dogs after all yes

65
00:05:56,640 --> 00:05:58,640
And the cats well

66
00:06:01,040 --> 00:06:03,040
You have to praise the cats

67
00:06:03,040 --> 00:06:08,440
I have to slave over the cats. They're doing their role in life

68
00:06:18,280 --> 00:06:23,960
So what are you thinking to do for the children's home? Shall we do like an online campaign?

69
00:06:24,760 --> 00:06:26,760
Or what were you thinking?

70
00:06:26,760 --> 00:06:39,640
I don't know if I can say yes to that. I guess so. I guess I can say that's that sort of indeed I do because

71
00:06:41,400 --> 00:06:43,400
But I just can't talk about money

72
00:06:45,440 --> 00:06:46,720
Sure

73
00:06:46,720 --> 00:06:50,360
Can you tell me the name of the organization so I can

74
00:06:50,640 --> 00:06:53,880
So we can decide it on one, but I guess it's

75
00:06:53,880 --> 00:06:58,400
Send the money. I guess we can just decide on this children's home

76
00:07:00,400 --> 00:07:02,400
Send the money

77
00:07:02,400 --> 00:07:05,560
Okay, she watches this did watch last

78
00:07:08,840 --> 00:07:10,840
Children's home dot org

79
00:07:13,320 --> 00:07:18,320
Children's home dot or they never visit us. They also have something over the holidays. It's a nice website

80
00:07:18,320 --> 00:07:23,080
Oh, yes, it's 1892

81
00:07:23,640 --> 00:07:25,640
We're in Tampa

82
00:07:28,120 --> 00:07:32,240
We could just go for a tour. You know, I could give a gift and they could go for a tour

83
00:07:32,240 --> 00:07:47,360
VIP tours, so what's a VIP tour you have to give a certain amount to become a VIP

84
00:08:02,720 --> 00:08:09,880
We'll look like they're having a big event on Saturday December 12th, but you'll be there a little after that

85
00:08:15,120 --> 00:08:17,520
Though we could give you know, if we

86
00:08:18,520 --> 00:08:20,520
But that's when that's an open house

87
00:08:22,000 --> 00:08:26,240
Make a donation from our list of most needed items, so they have like a

88
00:08:26,240 --> 00:08:31,200
I think they have a wish list of some sort

89
00:08:32,480 --> 00:08:36,240
There's a way to find out how to become a VIP like what's the criteria?

90
00:08:36,920 --> 00:08:40,300
Then we'd have a goal, right? Yeah

91
00:08:40,300 --> 00:08:55,180
How you can help

92
00:08:55,180 --> 00:09:09,180
You're gonna make a gift and honor if someone you care about come part of a surrogate

93
00:09:09,180 --> 00:09:20,020
The benefactor is $1,000. It's here for five years

94
00:09:20,020 --> 00:09:42,820
I think it probably

95
00:09:42,820 --> 00:09:49,860
They're most urgent needs, so we you know, we could ask people if they'd like to send these items

96
00:09:51,460 --> 00:09:54,420
Although I don't think you probably want to have to carry them down there, so

97
00:09:55,140 --> 00:09:57,140
We'd be better if we could send them to someone

98
00:09:58,340 --> 00:10:00,340
in the area

99
00:10:00,740 --> 00:10:03,140
But on their most urgent needs they're looking for

100
00:10:05,140 --> 00:10:10,660
Women's and mentalties and personal care items like body wash and hair products and things

101
00:10:10,660 --> 00:10:12,660
and batteries

102
00:10:22,180 --> 00:10:29,860
Well, and they have a much longer list of ongoing needs just all sorts of personal care items and recreational items

103
00:10:33,460 --> 00:10:35,460
So we could either

104
00:10:35,460 --> 00:10:42,180
You know look for donations of items or do an online campaign or

105
00:10:46,020 --> 00:10:48,820
Donate gift cards. We've got a lot of options here

106
00:10:51,460 --> 00:10:53,460
Well

107
00:10:53,700 --> 00:10:55,700
Come back to school supplies

108
00:10:57,140 --> 00:10:59,940
I'm trying to think of something that could get my family involved

109
00:10:59,940 --> 00:11:04,900
To be able to give us a gift to them

110
00:11:10,580 --> 00:11:12,580
I'll get on there behalf

111
00:11:13,220 --> 00:11:15,220
The best way to give on someone

112
00:11:15,220 --> 00:11:24,340
If we had a bunch of supplies, then we could they could all together deliver it

113
00:11:24,340 --> 00:11:37,780
Hmm, yeah, we would just need a place to send them to send them down there

114
00:11:37,780 --> 00:11:39,780
So you wouldn't have to try to carry them all with you

115
00:11:46,580 --> 00:11:48,580
There's urgent needs in November

116
00:11:48,580 --> 00:11:50,580
You

117
00:11:52,980 --> 00:11:54,980
Batteries

118
00:11:54,980 --> 00:12:02,980
I

119
00:12:11,780 --> 00:12:13,780
Actually serve children and adults

120
00:12:19,780 --> 00:12:21,780
They suggest having a donation drive

121
00:12:21,780 --> 00:12:26,820
Which kind of sounds like maybe what we're talking about maybe

122
00:12:26,820 --> 00:12:28,820
I

123
00:12:49,940 --> 00:12:52,340
When you're ready, Bantil there are a couple of questions

124
00:12:52,340 --> 00:12:58,740
All right, back to the number. Okay. I know it was starting to feel like I'm on to your meeting there for a moment

125
00:13:00,260 --> 00:13:05,700
At the moment someone dies. How long do they stay near their body? Are they still aware of their former selves?

126
00:13:06,340 --> 00:13:08,340
How long before they are reborn?

127
00:13:08,900 --> 00:13:10,900
I love no idea

128
00:13:12,180 --> 00:13:15,460
It's not I'm not qualified to answer that question

129
00:13:17,860 --> 00:13:19,860
Those questions

130
00:13:19,860 --> 00:13:23,700
I have a good question to find someone who knows

131
00:13:25,780 --> 00:13:28,500
Some of it's on the Buddha this past things down

132
00:13:32,020 --> 00:13:34,020
But not really to that extent

133
00:13:35,620 --> 00:13:37,620
You'll have stories that have been passed on

134
00:13:39,140 --> 00:13:43,140
Spirits there was one spirit hanging out. I had a body and

135
00:13:43,140 --> 00:13:49,460
And a monk came in the took the cloth off the body because they usually wrapped them up in a white cloth

136
00:13:50,100 --> 00:13:53,300
Throw them in the rubbish in the carnal ground

137
00:13:54,100 --> 00:13:57,220
We pulled the cloth off thinking it would make a nice robe cloth

138
00:13:58,020 --> 00:14:02,580
And the spirit got really upset and went back into the body and stood up and chased

139
00:14:05,060 --> 00:14:07,060
Chase them all the way back to his goodby

140
00:14:07,060 --> 00:14:11,860
Where he closed the door and the body fell down it fell it dropped down

141
00:14:13,140 --> 00:14:15,140
Against the door

142
00:14:18,660 --> 00:14:22,020
He wanted his cloth back a real Buddhist zombie

143
00:14:24,020 --> 00:14:26,020
Wanted the cloth back

144
00:14:27,060 --> 00:14:32,260
So there was something the Buddha instated a rule as a result of that. I can't remember what the rule was

145
00:14:32,900 --> 00:14:34,900
I think there was a rule and stated like

146
00:14:34,900 --> 00:14:40,260
Making sure the body is really dead or something

147
00:14:42,260 --> 00:14:46,500
It was a simple assistant. It wasn't about don't take don't take class from dead bodies

148
00:14:46,900 --> 00:14:48,900
But then same to do

149
00:14:48,900 --> 00:14:53,700
And it's not considered stealing because really your dead dude can move on and get over it

150
00:14:53,700 --> 00:15:03,620
And there's something some sort of minor minor rule and then he instated to sort of make sure he didn't get chased by zombies

151
00:15:07,220 --> 00:15:09,220
May be cover the body up with something else

152
00:15:14,980 --> 00:15:19,220
Dear Bombay the more I give up doubts record the more I give up doubts

153
00:15:19,220 --> 00:15:25,940
Regarding the practice the more I understand the practice the more I dedicate myself to the practice and the more I let go

154
00:15:27,300 --> 00:15:33,220
The more they're develops an issue that could probably be seen as relating to what you describe as the second imperfection of insight

155
00:15:33,700 --> 00:15:35,700
The imperfection of knowledge

156
00:15:35,700 --> 00:15:40,100
What has been happening lately is a kind of circle or seesaw motion or something

157
00:15:40,660 --> 00:15:43,060
I attain more clarity regarding financial

158
00:15:43,060 --> 00:15:50,980
I start to see with greater and greater clarity the uselessness and worthlessness of aspects of myself

159
00:15:51,620 --> 00:15:55,780
Of other people of several activities. I've been occupied with throughout my life

160
00:15:56,500 --> 00:16:00,340
Of the goals. I've had in my life of the goals other people have etc

161
00:16:01,060 --> 00:16:05,140
This makes me want to practice even more and dedicate myself even more and I do that

162
00:16:05,540 --> 00:16:11,380
But soon afterwards when indulging in the many activities as before and dealing with the same people as before

163
00:16:11,380 --> 00:16:15,700
Which tends to be bound to happen due to being a layperson living in the world

164
00:16:16,180 --> 00:16:21,700
My experience with these activities and people is now different because of how the practice has affected the mind

165
00:16:22,500 --> 00:16:28,500
Everything is now so much easier and smoother and sometimes it even feels like I could clearly accomplish anything

166
00:16:29,220 --> 00:16:33,460
This tends to entice or alert very strongly to play the mundane game of life

167
00:16:34,260 --> 00:16:37,220
To play this mundane game of life for at least a bit longer

168
00:16:37,220 --> 00:16:44,180
As it feels like I'm now almost looking at these activities and interactions from the outside as some kind of super user

169
00:16:45,060 --> 00:16:48,660
Of course this doesn't last for very long, but it makes things difficult

170
00:16:49,300 --> 00:16:56,260
Even when it happens. I still always kind of remember the importance of the practice and the worthlessness of the things

171
00:16:56,260 --> 00:16:57,780
I mentioned earlier

172
00:16:57,780 --> 00:17:01,940
But at least this slows things down considerably. Can you talk about this?

173
00:17:02,580 --> 00:17:04,580
Thank you

174
00:17:04,580 --> 00:17:08,740
I think Robin just didn't talk about that. What's the question?

175
00:17:09,460 --> 00:17:11,460
I'm sorry Robin that you had

176
00:17:12,020 --> 00:17:14,020
That's not really fair is it?

177
00:17:14,580 --> 00:17:16,580
I mean it's needed to hear about but

178
00:17:17,140 --> 00:17:21,700
Yeah, I think we probably understand when he's talking about that that big change

179
00:17:22,260 --> 00:17:23,620
That's the question

180
00:17:23,620 --> 00:17:28,020
Yeah, you need to talk I mean Robin you're gonna but

181
00:17:28,820 --> 00:17:32,100
I have pity on Robin. She has to talk. It has to say it

182
00:17:32,100 --> 00:17:37,460
Oh, that's okay. I don't mind reading long questions. It's you know if it's helpful

183
00:17:37,460 --> 00:17:44,500
But in the end was no question. What's the question? I mean good for you. It's great to hear about your practice, but

184
00:17:46,820 --> 00:17:53,540
No, we don't it's not fair. You have to ask a question. It should be short and concise and simple. You see to understand

185
00:17:55,220 --> 00:17:57,220
Is that person still around?

186
00:17:57,220 --> 00:17:59,220
Yes

187
00:17:59,220 --> 00:18:03,860
Well, then ask a question and make it simple. Keep it simple

188
00:18:08,820 --> 00:18:10,820
Those are the rules

189
00:18:13,460 --> 00:18:18,660
We don't need so much background really because you'll find that when you do come up with a question if there is one

190
00:18:19,860 --> 00:18:23,140
And that you didn't really need to keep all the background details

191
00:18:23,140 --> 00:18:28,820
You have to learn to be mindful you don't think so much

192
00:18:30,340 --> 00:18:32,740
Sounds like there's maybe too much mental activity

193
00:18:33,940 --> 00:18:37,300
Do you have a question ask it then things then

194
00:18:38,500 --> 00:18:40,500
You don't be hit you with a stick

195
00:18:40,980 --> 00:18:43,300
We'll have to I implement the Twitter

196
00:18:44,340 --> 00:18:46,900
Limitation there. What's that 120 characters?

197
00:18:46,900 --> 00:18:50,980
Yeah, it was originally at something like what 400 characters or something

198
00:18:50,980 --> 00:18:53,620
200 and then people all know it's too short

199
00:18:55,460 --> 00:19:01,700
Now how many how many posts is that person used just to ask to ask that not a question? It was just too

200
00:19:02,500 --> 00:19:05,060
Oh Timo, this is the guy who's coming in December

201
00:19:07,700 --> 00:19:11,460
So he's gonna put a little more information there and I believe

202
00:19:17,060 --> 00:19:19,620
No, there's no no need for background

203
00:19:19,620 --> 00:19:21,620
Just

204
00:19:22,020 --> 00:19:28,660
Ask a question see and if you can't formulate a question then you got a problem. That's that's that's the reason it's a litmus test

205
00:19:29,460 --> 00:19:32,420
That you still have it's still not clear in your mind

206
00:19:33,140 --> 00:19:36,980
Which means you that's your problem. You have to start saying thinking thinking and

207
00:19:37,620 --> 00:19:43,300
Looking at your mind and and maybe read my booklet again to figure out if you're actually meditating correctly

208
00:19:43,300 --> 00:19:49,140
It's it's easy to get meditating on the wrong path

209
00:19:50,260 --> 00:19:54,500
Maybe you have states and bliss or happiness look at the ten imperfections of insight

210
00:19:55,060 --> 00:19:58,020
And see if you have any of the other ones you'll have be a calm

211
00:19:59,220 --> 00:20:05,380
If you're powerful, well, that's a confidence at sunrise and the mocha. I think is the

212
00:20:06,820 --> 00:20:08,820
Defound

213
00:20:08,820 --> 00:20:11,860
So you have to say

214
00:20:13,700 --> 00:20:15,700
Happy happy confident

215
00:20:16,180 --> 00:20:18,180
Knowing

216
00:20:25,860 --> 00:20:31,940
You know just on a calendar birthday that tomorrow is on a panasati day. Is that oh, I don't know

217
00:20:31,940 --> 00:20:40,180
Is that a different tradition? I don't know

218
00:20:42,180 --> 00:20:44,180
Which it's the full moon day

219
00:20:46,020 --> 00:20:48,900
I guess it's what they say that when they say the Buddha

220
00:20:49,540 --> 00:20:54,180
Taught in the Ana panasati sutta. So those people who are keen on Ana panasati

221
00:20:56,820 --> 00:20:58,820
Have created a day

222
00:20:58,820 --> 00:21:02,820
Okay

223
00:21:11,460 --> 00:21:15,780
Okay, so kind of grumpy there. I didn't mean to send grumpy. It's a good thing. It's good to have that

224
00:21:16,740 --> 00:21:19,300
It's a little bit grumpy. It's like wow really

225
00:21:20,260 --> 00:21:22,820
It's not really a holiday like maga budge or

226
00:21:22,820 --> 00:21:28,260
We sack a budge or a saddle hub, which are these are the three big ones

227
00:21:29,300 --> 00:21:31,300
Is there a they're old and traditional and

228
00:21:32,100 --> 00:21:36,740
Meaningful to have a day just for the sutta. Well, that's nice. It's not the wrong with it

229
00:21:37,700 --> 00:21:40,180
Not on the level of a real Buddhist holiday

230
00:21:41,460 --> 00:21:44,500
Is there anything special that they do in Thailand on that day

231
00:21:45,700 --> 00:21:49,620
No, I mean, this is I don't know who which group this is I think it's tell you Sarah be good at

232
00:21:49,620 --> 00:21:54,980
Popularized that. I don't know. Maybe it's his group. Maybe the dama you to Nikay

233
00:21:54,980 --> 00:22:12,500
Okay, certainly not a big thing in Thailand as far as I know

234
00:22:24,980 --> 00:22:28,740
Do you think it is important to keep attention on body in your daily life?

235
00:22:31,460 --> 00:22:33,460
Well, not meditating rather than thoughts

236
00:22:34,340 --> 00:22:37,940
One can understand on each adhukha anata through thoughts too

237
00:22:39,060 --> 00:22:41,060
Is well not medit?

238
00:22:41,060 --> 00:22:46,340
Question is while not meditating can one give importance to thoughts or should one combine oneself to the body?

239
00:22:46,980 --> 00:22:51,300
Well, the body's easier when you're not meditating the body's easier to do mind

240
00:22:51,300 --> 00:22:56,180
For thoughts can be a bit overwhelming, but certainly you should be mindful of all four

241
00:22:56,900 --> 00:22:58,900
So deep at home during your life

242
00:23:00,180 --> 00:23:03,220
We tend to recommend doing the body. It's easier

243
00:23:03,860 --> 00:23:07,700
When you're standing walking sitting in mind. That's just easier to be mindful of

244
00:23:10,100 --> 00:23:13,700
And that's for most people for some people the mind is easier

245
00:23:14,260 --> 00:23:16,580
You know some people benefit more from

246
00:23:16,580 --> 00:23:24,660
Focusing on one or another of the Sati Bhutan yourself. So as you know, no, there's no rules like that

247
00:23:25,300 --> 00:23:27,300
Should this should that

248
00:23:29,140 --> 00:23:34,180
Sati Bhutan are like and for weapons that you have in your guerilla

249
00:23:35,300 --> 00:23:36,820
soldier

250
00:23:36,820 --> 00:23:38,820
fighting a messy war

251
00:23:39,220 --> 00:23:40,740
And so it's not it's not

252
00:23:40,740 --> 00:23:49,060
It's not neat. It's messy. Sometimes like this. Sometimes like that. You have to change tactics depending on the enemy

253
00:23:49,860 --> 00:23:51,860
and so on

254
00:23:52,260 --> 00:23:54,260
Depending on the situation

255
00:23:54,260 --> 00:23:55,940
You have to be clever like a boxer

256
00:23:56,260 --> 00:24:00,260
To know when the duck and when to leave and when to punch and when to jab

257
00:24:04,580 --> 00:24:07,300
When you have to know the long game you have to be patient

258
00:24:07,300 --> 00:24:09,300
Okay

259
00:24:10,180 --> 00:24:12,180
Expect to knock out in one punch

260
00:24:20,260 --> 00:24:22,260
It's kind of like a war of a trishing

261
00:24:25,460 --> 00:24:27,860
Bleed each other to bleed your enemies to death

262
00:24:29,380 --> 00:24:33,940
Star of your enemies to death. That's really what it is. We're starving our defilements

263
00:24:33,940 --> 00:24:35,940
I

264
00:24:36,740 --> 00:24:38,740
Gonna note them to death

265
00:24:38,740 --> 00:24:44,740
Hmm

266
00:24:59,380 --> 00:25:03,940
What is this? Can it be that I've answered all the questions possible and no one has more question?

267
00:25:03,940 --> 00:25:08,740
Our viewership is down right fewer people are watching. That's just fine

268
00:25:09,460 --> 00:25:11,940
It's expected you know looks that time to

269
00:25:13,300 --> 00:25:15,300
watch some

270
00:25:15,540 --> 00:25:17,540
Funny monk for every day

271
00:25:18,420 --> 00:25:20,420
Cut things they need to be doing

272
00:25:21,780 --> 00:25:24,340
That's fine. This is just sort of a hello

273
00:25:25,700 --> 00:25:30,900
Tomorrow we'll do another dumber panda, but again, that's not gonna be broadcast by it'll be audio live

274
00:25:30,900 --> 00:25:32,900
And

275
00:25:34,420 --> 00:25:36,660
Just tomorrow yet tomorrow is another dumber panda

276
00:25:40,500 --> 00:25:45,540
That's enough. That's all good night. Thank you. There was one more question if you had time

277
00:25:48,820 --> 00:25:53,860
Hi, Bante is an hour had to protect it from physical harm only during meditating or always

278
00:25:53,860 --> 00:26:00,020
And they're not protected from physical harm even when meditating now

279
00:26:00,580 --> 00:26:03,780
There's these stories about when you enter into jana or

280
00:26:04,660 --> 00:26:06,660
or some apathy of some sort

281
00:26:07,620 --> 00:26:09,620
You you're invincible

282
00:26:10,900 --> 00:26:13,860
But that's nothing to do with being an arrow hunt first per se

283
00:26:14,660 --> 00:26:17,940
It's to do with the state of attainment the state of meditation

284
00:26:17,940 --> 00:26:22,740
Yeah, even non arrow hunts couldn't theoretically have that sort of

285
00:26:24,180 --> 00:26:26,500
imperviousness. It's not based. It's not the

286
00:26:27,380 --> 00:26:32,820
Exactly or directly related to their state of being an arrow hunt. It's the state of meditation that they go into

287
00:26:35,300 --> 00:26:40,900
Didn't the Buddha hurt his foot or have some sort of an injury after he was enlightened. Yeah

288
00:26:40,900 --> 00:26:48,900
Mogulana the most powerful meditative monk who had the most powerful attainment was beaten almost to death

289
00:26:50,580 --> 00:26:56,420
Had all the bones in his body broke in the pyramid yet horrible karma. He tortured his parents

290
00:26:57,300 --> 00:26:59,940
Beat in his parents because he didn't want to take care of him

291
00:27:02,180 --> 00:27:04,180
Went to hell for that

292
00:27:04,180 --> 00:27:13,380
This is a follow-up question from Timo. I guess I was looking for help regarding the seesaw motion between the practice and the lay life

293
00:27:13,860 --> 00:27:19,860
I think questions seemed kind of conceited. Also, sorry Robin for the long questions. Don't be sorry

294
00:27:19,860 --> 00:27:21,940
It wasn't even a question. Where's your question?

295
00:27:23,860 --> 00:27:25,860
The question is

296
00:27:26,100 --> 00:27:29,780
What sort of help can you give me for understanding?

297
00:27:29,780 --> 00:27:33,540
We're dealing with the seesaw motion between the practice and the lay life

298
00:27:39,220 --> 00:27:42,820
No, I don't know and I don't like general help questions either

299
00:27:44,340 --> 00:27:49,860
As they're too complicated. It's like you want me to write about you know, it's not you're not alone many people ask these sorts of questions

300
00:27:49,860 --> 00:27:51,860
It's like you want me to plan your life out

301
00:27:51,860 --> 00:28:00,980
Yeah, general advice. It's too difficult. I don't know you. I don't know how to read the signal of others who tell give you some general advice

302
00:28:02,580 --> 00:28:04,580
Digani kai at 31. I think

303
00:28:05,540 --> 00:28:08,740
Signal of others and I'm really good at it, but there's a lot

304
00:28:11,780 --> 00:28:15,380
Because it's not that simple. I can't just tell you something that's going to help you with that

305
00:28:16,980 --> 00:28:20,580
You have to give specific examples like is this right is that right?

306
00:28:20,580 --> 00:28:26,340
And I think maybe to some extent you already know the answer to some of the some of the questions that you might ask

307
00:28:26,580 --> 00:28:30,820
So that you're avoiding them. You have to just look at the individual's

308
00:28:31,700 --> 00:28:33,700
aspects of your life

309
00:28:34,260 --> 00:28:36,260
Dealing with people feeling

310
00:28:36,500 --> 00:28:38,660
Egotistical feeling ambitious feeling

311
00:28:39,460 --> 00:28:44,260
That's we're not egotistical. I don't know you said that even ambitious was kind of like you were saying

312
00:28:46,420 --> 00:28:48,420
And you know that that's ambition

313
00:28:48,420 --> 00:28:55,940
How to deal with things is always just meditate. It's always just going to be learned to meditate. I don't have to see them clearly and then have to let go

314
00:28:59,300 --> 00:29:01,540
That it details of living their life

315
00:29:03,940 --> 00:29:09,060
Don't I mean the ultimate advice it just comes down to become a monk if you want real answers

316
00:29:09,060 --> 00:29:17,060
I'm not an easy thing to do

317
00:29:21,620 --> 00:29:24,500
Apologies probably not satisfied but

318
00:29:29,060 --> 00:29:31,060
He's very grateful for the how to meditate books

319
00:29:31,060 --> 00:29:33,060
So

320
00:29:38,180 --> 00:29:40,180
Okay enough good night

321
00:29:42,500 --> 00:29:44,500
Thank you Robin

322
00:29:45,860 --> 00:29:49,460
Thank you, Dante good night. We're not another question. Do it

323
00:29:49,460 --> 00:29:59,460
Are you not that you say no, okay, okay, good night Dante. Thank you

