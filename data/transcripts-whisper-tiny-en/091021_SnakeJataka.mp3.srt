1
00:00:00,000 --> 00:00:09,760
And next the story of the Urughadjataka.

2
00:00:09,760 --> 00:00:13,400
Man quits his mortal frame, and so on.

3
00:00:13,400 --> 00:00:17,880
This story, the master, well-dwelling in Jatobana, told concerning a landowner, whose son

4
00:00:17,880 --> 00:00:19,880
had died.

5
00:00:19,880 --> 00:00:24,240
The introductory story is just the same as that of the man who lost both his wife and

6
00:00:24,240 --> 00:00:25,440
father.

7
00:00:25,440 --> 00:00:30,200
Here, too, the master in the same way went to the man's house, and after saluting him as he

8
00:00:30,200 --> 00:00:35,120
was seated, asked him saying, Praise Sir, are you grieving?

9
00:00:35,120 --> 00:00:40,480
And on his replying, yes, Reverend Sir, ever since my son's death I grieve.

10
00:00:40,480 --> 00:00:48,200
He said, Sir, verily, that which is subject to dissolution is dissolved, and that which

11
00:00:48,200 --> 00:00:53,920
is subject to destruction is destroyed, and this happens not to one man only.

12
00:00:53,920 --> 00:00:58,520
Never in one village merely, but in countless fears, and in the three modes of existence,

13
00:00:58,520 --> 00:01:03,200
there is no creature that is not subject to death, nor is there any existing that is

14
00:01:03,200 --> 00:01:13,000
not existing thing that is capable of abiding in the same condition, all beings are subject

15
00:01:13,000 --> 00:01:18,440
to death, and all compounds are subject to dissolution.

16
00:01:18,440 --> 00:01:24,040
That sages of old when they lost a son said, that which is subject to destruction is destroyed,

17
00:01:24,040 --> 00:01:26,080
and grieved not.

18
00:01:26,080 --> 00:01:31,520
And here, upon at the man's request, he related a story of the past.

19
00:01:31,520 --> 00:01:35,640
Once upon a time when Bramadatta was reigning in Beneras, the Bodhisattva was born in a

20
00:01:35,640 --> 00:01:41,280
Brahmin household in a village outside the gates of Beneras, and rearing a family he supported

21
00:01:41,280 --> 00:01:43,480
them by field labor.

22
00:01:43,480 --> 00:01:46,440
He had two children, a son and a daughter.

23
00:01:46,440 --> 00:01:51,000
When the son was grown up, the father brought a wife home for him, from a family of equal

24
00:01:51,000 --> 00:01:53,600
rank with his own.

25
00:01:53,600 --> 00:01:59,240
Thus with a female servant, they composed a household of six, the Bodhisattva and his wife,

26
00:01:59,240 --> 00:02:05,040
the son and daughter, the daughter-in-law and the female servant.

27
00:02:05,040 --> 00:02:07,520
They lived happily and affectionately together.

28
00:02:07,520 --> 00:02:11,680
The Bodhisattva thus admonished the five, the other five.

29
00:02:11,680 --> 00:02:18,080
According as you have received, give arms, observe holy days, keep the moral law, dwell

30
00:02:18,080 --> 00:02:22,160
on the thought of death, be mindful of your mortal state.

31
00:02:22,160 --> 00:02:27,680
For in the case of beings like ourselves, death is certain, life uncertain.

32
00:02:27,680 --> 00:02:32,080
All existing things are transitory and subject to decay, therefore take heed to your

33
00:02:32,080 --> 00:02:35,280
ways, day and night.

34
00:02:35,280 --> 00:02:41,840
They readily accepted his teaching, and dwell earnestly on the thought of death.

35
00:02:41,840 --> 00:02:46,080
Now one day the Bodhisattva went with his son to plough his field.

36
00:02:46,080 --> 00:02:50,240
The son gathered together the rubbish and set fire to it.

37
00:02:50,240 --> 00:02:53,880
Not far from where he was, lived a snake in an antil.

38
00:02:53,880 --> 00:03:00,080
The smoke hurt the snake's eyes, coming out of his hole in a rage at thought.

39
00:03:00,080 --> 00:03:05,400
This is all due to that fellow, and fastening upon him with its four teeth it bit him.

40
00:03:05,400 --> 00:03:08,440
The youth fell down dead.

41
00:03:08,440 --> 00:03:12,800
The Bodhisattva on seeing him fall left his oxen and came to him.

42
00:03:12,800 --> 00:03:17,280
And finding that he was dead, he took him up and laid him at the foot of a certain tree,

43
00:03:17,280 --> 00:03:22,000
and covering him up with his cloak, he neither wept nor lamented.

44
00:03:22,000 --> 00:03:27,080
He said to himself, that which is subject to dissolution is dissolved, and that which is

45
00:03:27,080 --> 00:03:29,680
subject to death is dead.

46
00:03:29,680 --> 00:03:36,120
All compound things are transitory and liable to death.

47
00:03:36,120 --> 00:03:41,560
And recognizing the transitory nature of things he went on with his ploughing.

48
00:03:41,560 --> 00:03:45,840
Seeing a neighbor pass close by the field, he asked friend, are you going home?

49
00:03:45,840 --> 00:03:51,840
And on his answering yes, the Bodhisattva said, please then, go to our house and say

50
00:03:51,840 --> 00:03:57,400
to the mitt mistress, you are not today as formerly to bring food for two, but to bring

51
00:03:57,400 --> 00:04:00,120
it for only one.

52
00:04:00,120 --> 00:04:05,560
And hitherto the female slave came alone, and brought the food, but today all four of you

53
00:04:05,560 --> 00:04:10,360
are to put on clean garments, and to come with perfumes and flowers in your hands.

54
00:04:10,360 --> 00:04:17,520
All right, he said, and when, and spoke those very words to the brahman's wife.

55
00:04:17,520 --> 00:04:24,720
She asked by whom sir was this message given, by the brahmanlady he replied.

56
00:04:24,720 --> 00:04:31,400
Then she understood that her son was dead, but she did not so much as tremble.

57
00:04:31,400 --> 00:04:35,640
That's showing perfect self-control and wearing white garments, and with perfumes and flowers

58
00:04:35,640 --> 00:04:39,920
in her hand, she bathed them bring food and accompanied the other members of the family

59
00:04:39,920 --> 00:04:42,080
to the field.

60
00:04:42,080 --> 00:04:47,440
But no one of them all either shed a tear or made lamentation.

61
00:04:47,440 --> 00:04:53,160
The Bodhisattva still sitting in the shade where the youth lay, ate his food, and when

62
00:04:53,160 --> 00:04:58,000
the meal was finished, they all took up firewood, and lifting the body onto the funeral

63
00:04:58,000 --> 00:04:59,000
pirate.

64
00:04:59,000 --> 00:05:03,960
They made offerings of perfumes and flowers, and then set fire to it, but not a single tear

65
00:05:03,960 --> 00:05:07,160
was shed by anyone.

66
00:05:07,160 --> 00:05:10,600
All were dwelling on the thought of death.

67
00:05:10,600 --> 00:05:17,040
Such was the efficacy of their virtue that the throne of Saka, the king of the angels,

68
00:05:17,040 --> 00:05:23,960
professed signs of heat, as it often did when someone unearthed a virtuous act.

69
00:05:23,960 --> 00:05:29,800
Saka said, who I wonder is anxious to bring me down from my throne, meaning who will take

70
00:05:29,800 --> 00:05:36,280
his place, because to be born as Saka, the king of the angels, one has to do virtue, and

71
00:05:36,280 --> 00:05:45,760
thus this throne becomes hot, because it no longer is deserved solely by Saka himself.

72
00:05:45,760 --> 00:05:49,520
On reflection he discovered that the heat was due to the force of virtue existing in

73
00:05:49,520 --> 00:05:54,520
these people, and being highly pleased he said I must go to them and utter a loud cry

74
00:05:54,520 --> 00:06:00,600
of exultation, like the roaring of a lion, and immediately go afterwards, to fill their

75
00:06:00,600 --> 00:06:05,000
dwelling places with place with the seven treasures.

76
00:06:05,000 --> 00:06:09,200
And going there in haste he stood by the side of the funeral pirate and said, what are

77
00:06:09,200 --> 00:06:12,000
you doing?

78
00:06:12,000 --> 00:06:16,240
We are burning the body of a man, my lord.

79
00:06:16,240 --> 00:06:17,520
There is no man that you are burning.

80
00:06:17,520 --> 00:06:22,200
He said, me thinks you are roasting the flesh of some beast that you have slain.

81
00:06:22,200 --> 00:06:24,120
Not so, my lord, they said.

82
00:06:24,120 --> 00:06:28,320
It is merely the body of a man that we are burning.

83
00:06:28,320 --> 00:06:32,120
And he said, it must have been some enemy.

84
00:06:32,120 --> 00:06:36,680
The bodies had to reply, it is our own true son and no enemy.

85
00:06:36,680 --> 00:06:41,120
Then he could not have been a very dear as a son to you.

86
00:06:41,120 --> 00:06:43,920
He was very dear, my lord.

87
00:06:43,920 --> 00:06:47,400
Then why do you not weep?

88
00:06:47,400 --> 00:06:54,360
Then the body sat that to explain the reason why he did not weep under the first stanza.

89
00:06:54,360 --> 00:06:59,360
Man quits his mortal frame when joy in life his past.

90
00:06:59,360 --> 00:07:04,440
Ian as a snake is want, it is worn out slaw to cast.

91
00:07:04,440 --> 00:07:07,840
No friends lament can touch the ashes of the dead.

92
00:07:07,840 --> 00:07:09,360
Why should I grieve?

93
00:07:09,360 --> 00:07:13,920
He fares the way he had to tread.

94
00:07:13,920 --> 00:07:20,960
Sakha on hearing the words of the body satva asked the brahman's wife, how lady did

95
00:07:20,960 --> 00:07:24,200
the dead man stand to you?

96
00:07:24,200 --> 00:07:29,360
I sheltered him ten months in my womb and suckled him at my breast and directed the movements

97
00:07:29,360 --> 00:07:35,120
of his hands and feet and he was my grown up son, my lord.

98
00:07:35,120 --> 00:07:39,280
Granted lady that a father from the nature of a man may not weep, a mother's heart

99
00:07:39,280 --> 00:07:44,080
surely is tender, why then do you not weep?

100
00:07:44,080 --> 00:07:50,800
And to explain why she did not weep, she uttered a couple of stanzas as well.

101
00:07:50,800 --> 00:07:55,600
Uncalled he hither came, unbidden soon to go.

102
00:07:55,600 --> 00:08:00,760
Ian as he came he went, what causes here for woe.

103
00:08:00,760 --> 00:08:04,480
No friends lament can touch the ashes of the dead.

104
00:08:04,480 --> 00:08:05,480
Why should I grieve?

105
00:08:05,480 --> 00:08:10,240
He fares the way he had to tread.

106
00:08:10,240 --> 00:08:15,640
On hearing the words of the brahman's wife, Sakha asked the sister, lady what was the

107
00:08:15,640 --> 00:08:17,880
dead man to you?

108
00:08:17,880 --> 00:08:20,480
He was my brother, my lord.

109
00:08:20,480 --> 00:08:25,680
Lady sisters surely are loving towards their brothers, why do you not weep?

110
00:08:25,680 --> 00:08:31,640
But she to explain the reason why she did not weep repeated a couple of stanzas as well.

111
00:08:31,640 --> 00:08:36,640
Though I should fast and weep, how would it profit me?

112
00:08:36,640 --> 00:08:40,680
My kith and kin alas would more unhappy be.

113
00:08:40,680 --> 00:08:44,000
No friends lament can touch the ashes of the dead.

114
00:08:44,000 --> 00:08:45,000
Why should I grieve?

115
00:08:45,000 --> 00:08:50,640
He fares the way he had to tread.

116
00:08:50,640 --> 00:08:56,240
Sakha on hearing the words of the sister asked his wife, the young man's wife.

117
00:08:56,240 --> 00:08:58,280
Lady what was he to you?

118
00:08:58,280 --> 00:09:01,600
He was my husband, my lord.

119
00:09:01,600 --> 00:09:07,360
Women surely when a husband dies as widows are helpless, why do you not weep?

120
00:09:07,360 --> 00:09:14,000
But she to explain the reason why she did not weep uttered two stanzas of her own.

121
00:09:14,000 --> 00:09:20,280
As children cry and vain to grasp the moon above, so mortals idly mourn the loss of those

122
00:09:20,280 --> 00:09:22,400
they love.

123
00:09:22,400 --> 00:09:24,800
No friends lament can touch the ashes of the dead.

124
00:09:24,800 --> 00:09:25,800
Why should I grieve?

125
00:09:25,800 --> 00:09:30,560
He fares the way he had to tread.

126
00:09:30,560 --> 00:09:34,920
Sakha on hearing the words of the wife asked the handmade, saying, woman, what was he

127
00:09:34,920 --> 00:09:37,080
to you?

128
00:09:37,080 --> 00:09:40,080
He was my master, my lord.

129
00:09:40,080 --> 00:09:44,760
No doubt you must have been abused and beaten and oppressed by him, and therefore thinking

130
00:09:44,760 --> 00:09:48,680
he is happily dead, you weep not.

131
00:09:48,680 --> 00:09:52,880
Speak not so, my lord, that this does not suit his case.

132
00:09:52,880 --> 00:09:58,040
My young master was full of long suffering and love and pity for me, and was as a foster

133
00:09:58,040 --> 00:10:00,720
child to me.

134
00:10:00,720 --> 00:10:03,680
Then why do you not weep?

135
00:10:03,680 --> 00:10:10,200
And she to explain why she did not weep uttered a couple of stanzas herself.

136
00:10:10,200 --> 00:10:14,200
A broken pot of earth, ah, who can peace again?

137
00:10:14,200 --> 00:10:19,440
So too to mourn the dead is not but labor vain.

138
00:10:19,440 --> 00:10:21,880
No friends lament can touch the ashes of the dead.

139
00:10:21,880 --> 00:10:22,880
Why should I grieve?

140
00:10:22,880 --> 00:10:26,880
He fares the way he had to tread.

141
00:10:26,880 --> 00:10:31,560
Sakha on hearing what they all had to say was greatly pleased and said, you have carefully

142
00:10:31,560 --> 00:10:33,920
dwelt on the thought of death.

143
00:10:33,920 --> 00:10:37,000
Henceforth ye are not to labor with your own hands.

144
00:10:37,000 --> 00:10:39,880
I am Sakha, king of heaven.

145
00:10:39,880 --> 00:10:45,120
I will create the seven treasures in counsel of countless abundance in your house.

146
00:10:45,120 --> 00:10:49,920
You are to give alms to keep the moral law to observe holy days and to take heed to

147
00:10:49,920 --> 00:10:55,080
your ways, and thus admonishing them he filled their house with countless wealth and so

148
00:10:55,080 --> 00:10:58,160
departed from them.

149
00:10:58,160 --> 00:11:03,200
The master, having finished his exposition of the truth, declared the four noble truths

150
00:11:03,200 --> 00:11:05,480
and identified the birth.

151
00:11:05,480 --> 00:11:10,080
At the conclusion of the truth, the landowner attained the fruit of the first path,

152
00:11:10,080 --> 00:11:14,680
which is the path of Sotapana, means he has seen the truth.

153
00:11:14,680 --> 00:11:21,440
At that time Kudjutta was the female slave, Upalawana was the daughter, Rahula was the

154
00:11:21,440 --> 00:11:29,280
son, Kama was the mother, and I myself was the brahman, the end.

155
00:11:29,280 --> 00:11:30,280
So that's all for today.

156
00:11:30,280 --> 00:12:00,120
Thank you for coming to listen.

