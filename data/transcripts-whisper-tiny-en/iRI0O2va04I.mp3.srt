1
00:00:00,000 --> 00:00:05,680
For a lay person, how do you realize your fetters?

2
00:00:05,680 --> 00:00:11,280
I'm going to take that as it sounds as the sentence and assume that it really is asking

3
00:00:11,280 --> 00:00:14,080
what it purports to be asking.

4
00:00:14,080 --> 00:00:20,480
Realize your fetters in the sense of, and realize what you're bound to.

5
00:00:20,480 --> 00:00:24,200
How do you come to understand what is holding you?

6
00:00:24,200 --> 00:00:29,560
Because that's a good question if that's the question you're asking.

7
00:00:29,560 --> 00:00:31,040
Very good question.

8
00:00:31,040 --> 00:00:34,280
Lay people have many fetters.

9
00:00:34,280 --> 00:00:35,480
Monks have fetters as well.

10
00:00:35,480 --> 00:00:44,000
But the fetters of a lay person are more difficult, are difficult to see, have their own difficulties

11
00:00:44,000 --> 00:00:53,600
to them, especially because society that you're living in tends to support them, tends

12
00:00:53,600 --> 00:01:10,840
to encourage them, tends to view them as being proper and responsible and beneficial even.

13
00:01:10,840 --> 00:01:16,600
So how do you go about realizing what you're clinging to?

14
00:01:16,600 --> 00:01:21,480
Besides the obvious one, meditating, right?

15
00:01:21,480 --> 00:01:30,800
Turn yourself, let's work outwards in, and surround yourself, let's even go even further.

16
00:01:30,800 --> 00:01:38,040
Place yourself in a location that is going to allow you to practice the Buddhist teaching.

17
00:01:38,040 --> 00:01:43,720
We're just going to allow you, let's not even use the word Buddhist teaching.

18
00:01:43,720 --> 00:01:49,320
That is going to allow you to look at reality objectively.

19
00:01:49,320 --> 00:01:58,360
This is difficult because, as I said, society, the things around us, the society in which

20
00:01:58,360 --> 00:02:05,880
we most of us live, is brainwashing us, is what has brainwashed us into believing these

21
00:02:05,880 --> 00:02:14,440
things to be not fetters, to be not, to be actually supports.

22
00:02:14,440 --> 00:02:19,080
So take yourself out of that, if possible, as much as possible.

23
00:02:19,080 --> 00:02:26,920
And then surround yourself with only those people and things that are going to be conducive

24
00:02:26,920 --> 00:02:41,400
for objective, introspection and analysis of reality, objective investigation of reality.

25
00:02:41,400 --> 00:02:45,840
And then begin to undertake practices that allow you to see things objectively because

26
00:02:45,840 --> 00:03:00,760
you see the meaning, the whole meaning of the word fetters, is that they are negative.

27
00:03:00,760 --> 00:03:02,760
They're bad things.

28
00:03:02,760 --> 00:03:08,680
So we have to ask ourselves the question, why would we engage in something that was

29
00:03:08,680 --> 00:03:12,000
bad for us?

30
00:03:12,000 --> 00:03:15,320
We understand by the meaning of the word fetters here, that we're talking about something

31
00:03:15,320 --> 00:03:20,360
that we actually are responsible for binding ourselves to.

32
00:03:20,360 --> 00:03:24,240
We're not talking about someone else chaining us to something, we understand in Buddhism

33
00:03:24,240 --> 00:03:33,800
that we are not, we're not under the control of any other being.

34
00:03:33,800 --> 00:03:40,480
We willingly bind ourselves to things, become fettered by things.

35
00:03:40,480 --> 00:03:42,360
And so the question is, why would we do such a thing?

36
00:03:42,360 --> 00:03:46,880
Why would anyone do something that was to their detriment?

37
00:03:46,880 --> 00:03:50,720
And the only answer that I think is logical is that without knowing, because they don't

38
00:03:50,720 --> 00:03:52,400
know that it's to their detriment.

39
00:03:52,400 --> 00:03:55,440
They don't see things as they are.

40
00:03:55,440 --> 00:04:02,160
So that becomes the real challenge on all levels of our practice.

41
00:04:02,160 --> 00:04:10,640
Having a place and an environment and a way of life, livelihood, and doing things that

42
00:04:10,640 --> 00:04:13,280
allow you to see things as they are.

43
00:04:13,280 --> 00:04:20,040
And I think the key there is objectivity, because our fetters are all partiality, subjectivity.

44
00:04:20,040 --> 00:04:25,760
We have favor certain things and we are averse to certain things.

45
00:04:25,760 --> 00:04:29,480
And this is what leads to us to be fettered.

46
00:04:29,480 --> 00:04:35,440
Once you can see things as they are, once you can become objective, and once you're

47
00:04:35,440 --> 00:04:44,480
able to pull yourself out of culture, society, family, religion, whatever, pull yourself

48
00:04:44,480 --> 00:04:50,600
out of the beliefs and the views and the customs and the brainwashing, turning off

49
00:04:50,600 --> 00:04:59,080
the television and even the internet, the rest of the internet, except for this internet.

50
00:04:59,080 --> 00:05:11,080
And begin to look at reality, just ask yourself this very simple question, what is reality?

51
00:05:11,080 --> 00:05:13,080
It's not philosophical.

52
00:05:13,080 --> 00:05:15,280
What it means is, what is this that's in front of us?

53
00:05:15,280 --> 00:05:19,640
What do we mean by this question is not some philosophical thing about what's out there?

54
00:05:19,640 --> 00:05:20,800
What's here?

55
00:05:20,800 --> 00:05:24,120
What is this that we're experiencing?

56
00:05:24,120 --> 00:05:28,920
And then you begin to investigate it, and this is really what the Buddha taught.

57
00:05:28,920 --> 00:05:32,440
And this is how you become to see how you're fettered, because the only way to see it

58
00:05:32,440 --> 00:05:41,760
is to pull yourself out of it, or to make it seem to put it in a position where it doesn't,

59
00:05:41,760 --> 00:05:43,960
where it isn't natural.

60
00:05:43,960 --> 00:05:49,600
You have to take the fetters, take yourself out of the position where it is natural

61
00:05:49,600 --> 00:05:54,760
to engage in the fetters, where it is comfortable to engage in the fetters, engage in those

62
00:05:54,760 --> 00:05:58,800
things that you cling to.

63
00:05:58,800 --> 00:06:04,880
Put yourself in a position where it becomes superfluous to do so, and then you'll be able

64
00:06:04,880 --> 00:06:32,800
to see the needlessness, the harm in clinging.

