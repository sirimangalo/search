1
00:00:00,000 --> 00:00:15,360
Good evening, everyone. Welcome to our live broadcast evening dumb session.

2
00:00:15,360 --> 00:00:22,560
So last night, we started talking about mindfulness, intent was to answer the question

3
00:00:22,560 --> 00:00:27,200
of what is wrong mindfulness, but I ended up spending all the time talking about what is

4
00:00:27,200 --> 00:00:33,760
mindfulness. So tonight, briefly talking about how mindfulness goes wrong. I mean, it should be

5
00:00:33,760 --> 00:00:39,440
quite interesting as meditators to know how the practice can go wrong.

6
00:00:47,680 --> 00:00:55,600
So the first thing I think to note is that mindfulness is never wrong. Mindfulness is a

7
00:00:55,600 --> 00:01:08,400
so benign to say sadhara. So I can't remember the word, but it's a beautiful

8
00:01:09,680 --> 00:01:18,000
quality of mind. Mindfulness is common to all wholesome states, I think.

9
00:01:18,000 --> 00:01:31,760
So mindfulness itself can never be wrong and it's always useful. So when we talk about wrong

10
00:01:31,760 --> 00:01:39,680
mindfulness, we're not actually talking about mindfulness being wrong. We're talking more

11
00:01:39,680 --> 00:01:46,480
about the practice of mindfulness going wrong. But furthermore, the practice of mindfulness,

12
00:01:46,480 --> 00:01:54,080
it is quite difficult for it to go wrong. And I want to stress that because with other types

13
00:01:54,080 --> 00:02:01,040
of meditation, it actually is quite easy for it to go wrong. I'll talk a little bit about that,

14
00:02:01,040 --> 00:02:09,680
maybe I'll wait and talk during the talk about it. But mindfulness is so simple. The first thing

15
00:02:09,680 --> 00:02:18,240
is it's difficult to misunderstand the instruction. It's difficult to be in doubt about what is

16
00:02:18,240 --> 00:02:32,320
being asked. And it's difficult to apply mindfulness and, well, it's really impossible to apply

17
00:02:32,320 --> 00:02:41,440
mindfulness and go too far, right? With other types of meditation, you have to be careful. If you

18
00:02:41,440 --> 00:02:47,760
go too far, you can get out of your depths. But with mindfulness, you don't really ever get out of your

19
00:02:47,760 --> 00:02:55,120
depths. It can seem overwhelming, but it's in a different way. The overwhelming nature of it is

20
00:02:55,120 --> 00:03:03,440
real, right? It's just your own mind, whereas other types of meditation, you can get lost. And because

21
00:03:03,440 --> 00:03:10,160
it's conceptual, we'll talk about that. The first way mindfulness can go wrong. Let's bring this up

22
00:03:10,160 --> 00:03:26,080
is the four ways mindfulness can go wrong is one, unmindfulness, two misdirected mindfulness,

23
00:03:26,080 --> 00:03:43,680
three lapsed mindfulness, and four impotent or ineffective, ineffectual mindfulness, impotent might

24
00:03:43,680 --> 00:03:54,160
be the best word. Let's see if I can get to this again. So, unmindfulness is, of course, the

25
00:03:54,160 --> 00:04:00,160
opposite of what we talked about last night, right? If we just go through the four qualities of

26
00:04:00,160 --> 00:04:05,920
mindfulness, the look of the chateuka, and we reverse them. When you get to unmindfulness, what

27
00:04:05,920 --> 00:04:11,680
is it like? There is the wobbling. It has the characteristic of wobbling, be lop in the look of

28
00:04:11,680 --> 00:04:23,680
that. So, when you practice meditation and you let your mind, if you're not mindful, your mind

29
00:04:23,680 --> 00:04:33,280
flits from object to object. And so this is, I mean, this is a way, we talk about wrong mindfulness,

30
00:04:33,280 --> 00:04:37,920
I mean, the worst type of mindfulness is if you're not mindful, if you're just do walking and

31
00:04:37,920 --> 00:04:47,760
sitting maybe for hours a day without actually applying the mind to the object. So, samosara

32
00:04:47,760 --> 00:04:54,000
said, it has forgetfulness, it forgets itself. How you know you're practicing incorrectly,

33
00:04:54,000 --> 00:05:00,000
as you've forgotten your experiences, your mind is not connected with the present moment in

34
00:05:00,000 --> 00:05:09,840
standards. It caught up in judging and planning and wishing and so on. It's caught up in

35
00:05:09,840 --> 00:05:14,640
its own perceptions of things rather than the actual experience of them.

36
00:05:20,080 --> 00:05:24,880
So, and then when you experience this throughout the course, of course, and this is what you have

37
00:05:24,880 --> 00:05:32,000
to watch out for, you know you're going wrong when you realize you've gotten lost in judging

38
00:05:32,000 --> 00:05:39,360
something or worrying or stressing or any number of emotions, any number of distractions when you

39
00:05:39,360 --> 00:05:57,520
get caught up in fantasy and so on, forget yourself. Manifest itself as, oh, I missed one, but yeah,

40
00:05:57,520 --> 00:06:04,960
it manifests itself as not guarding, being unguarded. So, you see things and you want them or

41
00:06:04,960 --> 00:06:11,680
you're upset by them and you hear sounds and you're annoyed by them or you're enamored of them,

42
00:06:13,440 --> 00:06:18,960
smells and tastes when you eat your food and you just enjoy it so much or you eat your food and it's

43
00:06:20,080 --> 00:06:27,600
unpleasant. You let your mind go, sometimes you experience something and it gets you lost

44
00:06:27,600 --> 00:06:36,000
because you weren't guarded and you don't confront, remember the other manifestation, it manifests

45
00:06:36,000 --> 00:06:41,040
itself as confronting. So, unmindfulness is when you don't confront when you run away from it

46
00:06:42,800 --> 00:06:45,840
and the proximate cause is atirasanya.

47
00:06:53,520 --> 00:06:55,760
I didn't go through this with you guys last night, did I?

48
00:06:55,760 --> 00:07:02,800
Remember this? Did I talk about this last night? Because I've been rehearsing it. No, no, this is

49
00:07:02,800 --> 00:07:09,920
new, huh? Good. I was rehearsing it in my room as well. I just want to make sure this is new stuff.

50
00:07:11,760 --> 00:07:15,680
So, atirasanya, which means when you have perception and

51
00:07:15,680 --> 00:07:27,600
it's not firm, right? So, this is you experience something and mindfulness doesn't arise

52
00:07:28,320 --> 00:07:34,960
when you allow your mind to react to it. So, you experience pain, for example.

53
00:07:35,920 --> 00:07:42,560
There's the pain and that's the sunya but then instead of reaffirming that instead of staying

54
00:07:42,560 --> 00:07:49,760
with the experience, you get lost. I don't like the pain and so on. So, this is the cause of not

55
00:07:49,760 --> 00:07:59,280
being mindful is when you fail to grasp the experience, make it firm, make it strong, your mind is

56
00:07:59,280 --> 00:08:14,320
not strong basically. So, that's the first way mindfulness can go wrong. The second way

57
00:08:16,400 --> 00:08:20,240
misdirected mindfulness. And so, this is one that I've sort of

58
00:08:20,240 --> 00:08:30,640
I mean, it's something that isn't really wrong but it's problematic in the sense that

59
00:08:30,640 --> 00:08:39,360
if mindfulness is directed at the wrong object, it's not dangerous but it's not fruitful either

60
00:08:40,000 --> 00:08:45,440
or it's not fruitful in an ultimate sense. So, for example, mindfulness is the past

61
00:08:45,440 --> 00:08:52,240
where you can be, you can remember everything that happened in your life if you practice

62
00:08:52,240 --> 00:09:01,520
meditation and enter into states of extreme tranquility, you can become very mindful of your whole

63
00:09:01,520 --> 00:09:08,160
past all the way back to the moment when you were born. Very easy in fact, it's quite remarkable

64
00:09:08,160 --> 00:09:16,480
if you practice those practices. You'll get to the point where you can remember all the way back,

65
00:09:16,480 --> 00:09:25,040
your memory clears up because you strengthen your strength and your reality and then even break

66
00:09:25,040 --> 00:09:33,040
through conception and go back and remember past lives. But that's misdirected, right? It's

67
00:09:33,040 --> 00:09:38,560
misdirected in the sense that that won't make you enlightened and while that is an interesting

68
00:09:38,560 --> 00:09:46,480
and quite powerful practice in itself, general mindfulness of the past isn't going to help

69
00:09:46,480 --> 00:09:51,200
you at all. Your ability to remember things that happened long ago, which is a type of

70
00:09:51,920 --> 00:09:59,920
mind what we call sati. What it means is when you have a very strong mind but it's not

71
00:09:59,920 --> 00:10:06,400
directed at the present in the same with the future. But the more common one that's often

72
00:10:06,400 --> 00:10:15,760
misunderstood as being proper mindfulness is mindfulness of concept. So I talk a lot about concepts

73
00:10:15,760 --> 00:10:20,560
and how they're different from ultimate reality. So it's important because if you're mindful of

74
00:10:20,560 --> 00:10:27,600
concepts, for example, if you say it wish for all beings to be happy, that's the kind of a

75
00:10:27,600 --> 00:10:35,120
mindfulness or if you think about the parts of the body, hair, nails, teeth, flesh, skin,

76
00:10:37,440 --> 00:10:41,360
repeat this yourself, skin, skin. Well, skin isn't real. It's an idea that you get

77
00:10:42,000 --> 00:10:46,240
based on your experiences of seeing, hearing, smelling, tasting, feeling, thinking. But

78
00:10:46,240 --> 00:10:52,000
the skin itself is just a concept. And the thing is, if you focus on that, well, skin is

79
00:10:52,000 --> 00:11:00,880
as a concept is as permanent. It's constant. It's always there. It's not

80
00:11:00,880 --> 00:11:06,400
inconsistent in the sense, in the same way experiences are experiences that arise in

81
00:11:06,400 --> 00:11:14,080
cease moment after moment. And so if your mindfulness is not focused on the present

82
00:11:14,080 --> 00:11:21,680
reality, you want to experience and you won't come to understand impermanence,

83
00:11:21,680 --> 00:11:28,960
suffering, and non-cell, you won't let go, right? Concepts are something that we cling to,

84
00:11:30,320 --> 00:11:36,960
even tranquility meditation, all kinds of good meditation. But if your mindfulness, if your

85
00:11:36,960 --> 00:11:43,040
strength of mind is focused on concepts, you'll never come to see, you never come to let go and

86
00:11:43,040 --> 00:11:50,560
become free. So that's the second type of wrong mindfulness. The third way mindfulness can be seen

87
00:11:50,560 --> 00:11:56,480
as wrong is when it lapses. And you might argue this is, you might argue this is a lot like

88
00:11:58,480 --> 00:12:04,640
unmindfulness. But it only occurs for people who practice correctly. When you practice properly,

89
00:12:06,240 --> 00:12:12,480
because mindfulness practice calms the mind down and focuses the mind quite

90
00:12:12,480 --> 00:12:19,680
powerfully, you'll often experience positive states of bliss and joy. There was one person

91
00:12:19,680 --> 00:12:24,160
asked a question a couple of nights ago about their experience of great joy.

92
00:12:25,520 --> 00:12:30,720
And I've talked, I've given talks on this before, what the meditation is not and in talking about

93
00:12:31,360 --> 00:12:37,680
various ways we get stuck in the meditation. So there are good things that come by products of

94
00:12:37,680 --> 00:12:43,120
mindfulness practice, like you'll see bright light sometimes, or you'll see images in the mind.

95
00:12:44,080 --> 00:12:52,720
And then you get distracted by them, or you feel very calm and you become complacent and settled

96
00:12:52,720 --> 00:13:01,040
in the calm and you stop being mindful. This is actually an important part of the teaching of

97
00:13:01,040 --> 00:13:06,560
the meditation practice is to get meditators to be mindful of the good things that they get

98
00:13:06,560 --> 00:13:12,880
from meditation. You might feel happy, you might gain great knowledge and wisdom,

99
00:13:14,080 --> 00:13:18,880
sort of understanding about problems that you have in your life and so on.

100
00:13:21,920 --> 00:13:28,400
You may gain great energy and effort, or you might just become an emmered with the mindfulness,

101
00:13:28,400 --> 00:13:34,720
mindfulness itself can become a hindrance. Because once you get very mindful, you become complacent,

102
00:13:34,720 --> 00:13:44,960
you become sort of proud, confident in yourself. Hey, look at how mindful I am. And when you do

103
00:13:44,960 --> 00:13:50,320
that, you're not mindful anymore. And so you can sit there for a long time thinking, God, I'm so

104
00:13:50,320 --> 00:13:57,440
good at this. Wow, I really, this practice is great. And a confidence is another one. You become

105
00:13:57,440 --> 00:14:04,080
very confident in your teacher, in the teaching, in your own practice. You want to become a

106
00:14:04,080 --> 00:14:08,800
monk or a nun, you want to leave home. You want to go home and tell everyone about the practice.

107
00:14:10,640 --> 00:14:14,320
Great confidence comes, and that's a good thing, but you get lost with it.

108
00:14:15,840 --> 00:14:21,680
So good things have to be mindful. These are called the Vipassanupa Kilesa. They're defilements of

109
00:14:21,680 --> 00:14:31,520
insight because they're not evil or bad, but they're not perfect either. They impede your progress,

110
00:14:31,520 --> 00:14:38,400
they hinder it. So you could say that's a way that mindfulness practice goes bad,

111
00:14:39,280 --> 00:14:42,400
but generally just gets people stalled. If they don't have a teacher,

112
00:14:43,120 --> 00:14:48,080
it's common to be stuck in these states for quite a long time. But with a good teacher,

113
00:14:48,080 --> 00:14:54,560
it's not that problematic. And the fourth and final way, which I think is really the most

114
00:14:54,560 --> 00:14:59,600
proper way we can talk about wrong mindfulness. I think it's really how the Buddha talked about

115
00:14:59,600 --> 00:15:08,320
Mijasati, wrong mindfulness. Wrong mindfulness, because I said mindfulness itself can't be wrong,

116
00:15:08,320 --> 00:15:15,520
is impotent, impotent. Wrong, any of the eight full paths that are wrong, they're wrong because

117
00:15:15,520 --> 00:15:20,640
they're not accompanied by the other seven qualities, the other seven path factors.

118
00:15:22,640 --> 00:15:27,360
So in Buddhism, we talk about the eight full noble paths. Well, you need all eight factors

119
00:15:27,360 --> 00:15:31,440
working in tandem. If you have mindfulness, but wrong view,

120
00:15:33,040 --> 00:15:39,120
meditators who come here and have strong belief in God or a God, their God,

121
00:15:40,400 --> 00:15:46,480
or other religious views, strong belief in a self, and one person who believed in a God

122
00:15:46,480 --> 00:15:53,920
self, whatever that is. All kinds. And this person actually did the whole course.

123
00:15:53,920 --> 00:16:00,000
And he got to the end and he said, I think I got the experience that you wanted me to get.

124
00:16:00,000 --> 00:16:03,200
I said, oh, how do you know? He said, because my God self told me.

125
00:16:05,600 --> 00:16:10,160
And immediately I knew he hadn't gotten anything out of it because something was really wrong,

126
00:16:12,640 --> 00:16:18,320
which goes to show you can really be practicing, apparently be practicing mindfulness. But

127
00:16:18,320 --> 00:16:26,800
without right view, the intent to practice isn't enough. You're going to wrong intention,

128
00:16:26,800 --> 00:16:32,080
wrong, you know, if you're angry, bitter person, mindfulness practices isn't going to work,

129
00:16:32,080 --> 00:16:37,360
it's going to be ineffectual. If you're a greedy, addicted person, mindfulness isn't really going

130
00:16:37,360 --> 00:16:45,840
to work. I think if you don't actually address these, if you encourage them, if you are okay

131
00:16:45,840 --> 00:16:50,400
with being angry and greedy and deluded and you encourage those states,

132
00:16:52,800 --> 00:16:56,960
if you have wrong speech, if you're always saying bad things about people or

133
00:16:58,160 --> 00:17:04,000
yelling or hurting other people with a speech or you're lying, lying is the worst. If you're

134
00:17:04,000 --> 00:17:09,760
a liar, very difficult to be minded to to progress in meditation practice.

135
00:17:09,760 --> 00:17:16,880
Yeah, wrong action, killing, stealing, cheating, you're taking drugs in alcohol,

136
00:17:18,400 --> 00:17:25,360
not going to happen. And let's get stoned in practice, mindfulness meditation, right?

137
00:17:26,240 --> 00:17:31,200
The philosophy of whether drugs can be a part of meditative practice, some meditations, sure,

138
00:17:31,200 --> 00:17:41,360
not mindfulness. Wrong effort, right? If you're working too hard,

139
00:17:42,640 --> 00:17:49,040
you're hard to be mindful. Wrong concentration, if you're unfocused or if you're focused on the

140
00:17:49,040 --> 00:17:56,080
wrong thing. So the other seven path factors are, if I mislived the head right after action is

141
00:17:56,080 --> 00:18:04,880
wrong livelihood, but that's basically wrong action and wrong speed. But if any of these are

142
00:18:04,880 --> 00:18:11,760
missing, and mindfulness won't be effective. And that's the Buddha's, and that's how you understand

143
00:18:11,760 --> 00:18:17,360
right mindfulness, right? The eightfold path factors are all right because they're accompanied

144
00:18:17,360 --> 00:18:24,960
by the other seven. So the right way is when all eight come together, meaning it's more

145
00:18:24,960 --> 00:18:30,160
than just mindfulness, you need to have right view. I mean, and basically you need to understand

146
00:18:31,280 --> 00:18:35,680
body and mind. You have to understand that reality is made up of experiences.

147
00:18:37,360 --> 00:18:43,120
It's not made up of things. You know, this room doesn't actually exist. This room is a part of

148
00:18:43,120 --> 00:18:56,240
our perception. It's an extrapolation of our experiences and so on. So that was still a lot of

149
00:18:56,240 --> 00:19:03,280
information to fit in, would have to eventually become a 20 minute talk, but I just keep working

150
00:19:03,280 --> 00:19:09,280
on it. I think that's interesting, very useful for all of us, of course, to talk about and think

151
00:19:09,280 --> 00:19:15,520
about these things. So I hope that was useful, interesting. Thank you all for coming out.

152
00:19:16,560 --> 00:19:17,840
That's the demo for tonight.

153
00:19:17,840 --> 00:19:33,760
I see if there's any questions.

154
00:19:47,840 --> 00:20:01,280
Why is suffering considered negative? Well, because it's considered negative. If you didn't suffer

155
00:20:01,280 --> 00:20:08,320
from it, it wouldn't be called suffering. You admit by that question that there is the potential

156
00:20:08,320 --> 00:20:12,640
for someone to consider something negative. Well, it's the considering something negative that

157
00:20:12,640 --> 00:20:19,120
makes it suffering. If you let go of that consideration, it would no longer be suffering.

158
00:20:21,120 --> 00:20:24,640
Does the meditation century started in Sri Lanka still exist?

159
00:20:25,840 --> 00:20:28,640
Go there and be taught meditation. Yes, I'm pretty sure you could be.

160
00:20:30,720 --> 00:20:36,400
You might want to go to meet the regalence dead, but the place I was at is actually,

161
00:20:36,400 --> 00:20:42,880
I think, doing quite well, and there are Sri Lankan monks and foreign monks there.

162
00:20:44,560 --> 00:20:50,480
We built a big, two-story, four-room building, really good rooms,

163
00:20:53,440 --> 00:20:55,360
and there's two caves and so on.

164
00:20:58,080 --> 00:21:03,920
Big problem there, of course, is that it's a dengue-infested area, and you're more than likely

165
00:21:03,920 --> 00:21:09,760
to get dengue by going there. Unfortunately, it's a hot bed.

166
00:21:15,360 --> 00:21:20,080
I have to slow my actions to be aware of the intentions as well as the actual act.

167
00:21:20,720 --> 00:21:25,440
Should I just go about my day at my normal pace or keep doing things slower? Either way,

168
00:21:25,440 --> 00:21:31,040
if you're in the meditation center, you can certainly go slower, but you shouldn't have to go

169
00:21:31,040 --> 00:21:35,600
that slow. I mean, the mind is quite quick. If you're having to slow down, then maybe there's

170
00:21:35,600 --> 00:21:42,880
something you're missing, something that's keeping you from being a little bit quicker about it,

171
00:21:42,880 --> 00:21:44,560
but certainly slow down a little bit.

172
00:21:44,560 --> 00:21:55,120
I'm aware that there's no coincidence, if there's no coincidence, then nothing has been linked.

173
00:22:02,000 --> 00:22:09,040
Oh, the talks coincide with my own practice. That's interesting. I don't know.

174
00:22:09,040 --> 00:22:13,520
Yeah, I don't really have an answer for that question.

175
00:22:20,000 --> 00:22:24,640
Does the act of being mindful include recognizing the beginning stages of a sense induced

176
00:22:24,640 --> 00:22:41,840
fabrication and not allowing it to proliferate? Yes. Yes. I mean, you might want to, I've talked

177
00:22:41,840 --> 00:22:47,280
a lot about mindfulness. In fact, last night, I talked quite a bit about mindfulness, but it's fairly

178
00:22:47,280 --> 00:22:55,840
close to what you're saying. I think, are you saying that all meditation on concepts is wrong,

179
00:22:55,840 --> 00:23:00,480
mindfulness? It's not wrong, mindfulness. The Buddha wouldn't have called that, but

180
00:23:00,480 --> 00:23:08,240
if you're practicing to become enlightened, meditating on concepts is wrong in the sense

181
00:23:08,240 --> 00:23:14,480
that you're not going to become enlightened based on it. So it could be considered technically

182
00:23:14,480 --> 00:23:23,040
wrong. In fact, I think that's quite valid. It's not wrong in a general sense, but in the context

183
00:23:23,040 --> 00:23:25,200
of becoming enlightened, it's wrong.

184
00:23:32,880 --> 00:23:38,000
But it's not wrong to practice tranquility if your goal is tranquility, but if you practice

185
00:23:38,000 --> 00:23:44,880
tranquility, thinking it's going to bring insight, then you're wrong.

186
00:23:46,880 --> 00:23:52,320
What is the difference between tira sunya and sunya like cat dog cat tree, etc?

187
00:23:53,680 --> 00:23:57,920
Well, this is where sunya is used in a difference in, there's two meanings of at least two

188
00:23:57,920 --> 00:24:10,240
meanings of the word. I mean, sunya of a dog, of a cat, of a tree is just one kind of perception

189
00:24:10,800 --> 00:24:17,440
when you perceive the concept of dog or cat or so on. I mean, that doesn't work because that is

190
00:24:17,440 --> 00:24:25,360
perception of a concept, and if so, if you reaffirm that, so your misunderstanding of sunya

191
00:24:25,360 --> 00:24:33,120
is what comes before mindfulness, but tira sunya, when you affirm that, that's how mindfulness

192
00:24:33,120 --> 00:24:41,920
exists. So if you say dog, dog, dog, that's tira sunya, and it leads to sati, but it's wrong,

193
00:24:42,560 --> 00:24:49,920
for us, because it's a concept. If you say seeing, seeing that's tira sunya, and it leads to

194
00:24:49,920 --> 00:24:55,760
mindfulness, but it's right because it, the object is reality.

195
00:25:01,200 --> 00:25:08,800
That's unsophoresynthetic mind. No, not. Well, it's not real. I mean, a synthetic mind

196
00:25:08,800 --> 00:25:15,520
we're fooling ourselves. You can't create a mind like that. You know, it's like, if you look at

197
00:25:15,520 --> 00:25:20,240
the history of computers, you see that all we're doing is just making it smaller and smaller. We're

198
00:25:20,240 --> 00:25:26,480
not actually adding mind to the mix. You can't just make something more complex and say poof

199
00:25:26,480 --> 00:25:38,080
it's, it's, it's God, right? You started out with, with, with rocks, you know. It doesn't,

200
00:25:38,080 --> 00:25:45,280
at one, at some point of complexity become mind. It's just more complex body, physical.

201
00:25:49,360 --> 00:25:53,520
How do you know what to attend to and be mindful of during meditation? Whatever is clearest,

202
00:25:53,520 --> 00:25:55,440
just be mindful of that. It doesn't really matter.

203
00:25:58,560 --> 00:26:03,120
I think that's addressed in my frequently asked questions, which is linked at the top.

204
00:26:03,120 --> 00:26:09,600
If you might want to read that. Okay, and that's all the questions.

205
00:26:09,600 --> 00:26:39,440
Thank you all for coming. Have a good night.

