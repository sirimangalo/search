So, neither brain nor pleasure described us, or the occasion on which a sense we have
profitable consciousness as a reason accompanied by equanimity.
I don't think we should use equanimity for filling.
Neutral filling.
I think it's a better word.
Equanimity about insight is a name for equanimity consisting a neutrality about investigation,
just like that.
It is more or less the same as equanimity about formations.
Because equanimity about formations has to do with re-personal meditation, just as it is,
with this equanimity about insight is.
What has become that he abandoned and he obtains equanimity?
Equanimity, especially neutrality.
This is the real equanimity which is one of the mental factors in Bali, it is called tatra
matcha tada.
The next one is equanimity of Jana.
It's a name for equanimity producing impartiality towards them.
This is also not filling upika, it is Jana upika, it is specific equanimity.
And the next one is purifying equanimity, it is a name for equanimity purified of all
oppositions and through consisting an uninterestedness in stealing oppositions, that's
quite does.
Since all oppositions are purified of all oppositions, there is no interest in stealing
them because he has achieved stealing the oppositions.
After this achievement, he has no longer interest in stealing them because they are still
already.
That is called purifying equanimity and this is also the specific equanimity.
Now, here in six factors, equanimity, equanimity as defined equanimity as an enlightenment
factor, equanimity as specific neutrality, equanimity of Jana and purifying equanimity
of one in meaning, one in meaning means one in reality, one in essence.
And that is equanimity as specific neutrality.
The difference over is one of position or occasion, like the difference in the single
being as a ball, a youth, and a general, a king, and so on.
Therefore, these should be understood that equanimity as an enlightenment factor, et cetera,
and not form where there is six-factor equanimity, they are mutually exclusive.
And that six-factor equanimity, et cetera, and not form where there is equanimity as
an enlightenment factor.
Just as this one meaning, that means just as these are the same in essence or in reality,
so also equanimity, what formations and equanimity about inside have one meaning too, for
they are simply understanding, punya.
So these two are not the specific neutrality, but they are really understanding of
punya, because they arise during vipassana meditation, for they are simply understanding
class and these two ways according to function, just as and so on.
Then, down to the end of correct, 170, equanimity of energy and equanimity of feeling are different
both from each other and from the rest.
That means equanimity of energy means just energy, and equanimity of feeling is the neutral
feeling.
How many of the equanimity we get in essence or in reality, specific neutrality, understanding
energy and feeling, so in essence there are full kinds of equanimity, in detail there
are 10 kinds of equanimity.
So whenever we see the word equanimity, we have to understand which equanimity is meant here.
Then in this phrase, he dwells in equanimity, what would be judgment, specific neutrality,
not feeling equanimity, because we are still in the third genre, in the third genre there
is still Sukha, which is not neutral feeling, Sukha is a pleasurable feeling.
So here equanimity means specific neutrality or impartiality, and he is mindful and fully aware
and experiences Sukha, both bodily and mental, with his mental body.
Now for a graph 176, it is better in translation than in the original part, in the original
part, but it is the sentences are very involved, and then with the commonity they used
many relative pronouns, and one relative of another, and then the other relative of that,
and so it goes on like 6-8, it is very difficult to understand.
I don't know why he wrote that way, but in the translation it is better, we can understand.
So now, he feels means he experiences, he experiences Sukha, what Sukha is used in the
party, and here Sukha means both bodily, Sukha and mental, Sukha.
Now, when this genre arises, there is this Sukha feeling, I mean pleasurable feeling.
So when there is pleasurable feeling, he cannot help but experience it, although he has
no intention that I will experience or I will enjoy this Sukha, since that Sukha feeling
is accompanying the genre, he just experiences it.
So here, with his mental body means other mental states.
Now, when the third genre arises, there are other changes he goes also arising with it.
So there is third genre consciousness or third genre changes, and then there are three
diseases.
Right?
So among these diseases, there are beauty, Sukha, and take care of that.
They are the three are genre factors, and actually they are called genres.
The other competence are here called mental body.
So there is one consciousness, and then, let me see, 35, 33, there are 33 diseases.
So there is one data, third genre data, and 33 diseases, and among them, and there is
there is feeling, there is pleasurable feeling, which is Sukha.
And since Sukha arises together with other mental competence, the person is said to experience
the Sukha with mental competence.
So mental competence are here called mental body, and partly the word kaya is used.
And then, when this third genre arises, it causes material properties to arise, because
meta is caused by, some meta is caused by kama, some by chita, I mean consciousness,
some by climate, and some by food.
So when the third genre ticked arises, it causes material properties to arise too.
And since it is very refined state of mind, state of consciousness, the material properties
it produces are also very, very fine and very subtle.
And then, very fine and very subtle material properties are the tangible objects.
And then, also, the meditator or one who has chana experiences, so they are feeling,
or they are feeling arises through contact of his body with other tangible things,
tangible objects, like sitting on a soft cushion or something like that.
So during the time when the third genre arises, he is said to experience mental sukha
with his mental body and physical sukha also with mental body.
Because what we call feeling of weird enough is mental in the mind.
What we call feeling of sensations generally is the material properties in the body.
So suppose there is pain here, and pain is a collection of material properties here.
And we feel that pain, and that feeling of pain is in our mind.
So while we have a good touch, so there is a feeling, so that that good touch is material
property, and that we feel in our mind as sukha, as a pleasure.
So here, also, when the person gets a third genre, his mind is so refined that it produces
very refined material properties, and that these material properties are all scattered
through his body, or through with his body, and so he feels very pleasant sensations
in the body, through to mind.
So that is why it describes his described as experiencing sukha with his mental body.
So mental body means a group of mental, mental concomitants, a group of mental factors.
And he enters upon and draws in the third genre, on the bottom of which the noble ones
are known, with regard to the person who has attained the third genre.
He has equanimity, he is mindful, and he draws in sukha.
And why do the noble ones recommend, or I think, praise is better than the water command
here.
In praise of, you see something in praise of something, right?
So here, in the noble ones, praise this person, who has attained the third genre, because
he can keep himself in pleasure, not attached to even the very high form of, high form
of pleasure, sukha.
And his mind is so, he is so mindful in such a way that he can keep PD from arising,
because at the third genre, PD does not arise.
That is why, and this person is body of praise.
He can keep in partiality, and he can keep a weak PD by being very mindful.
And he draws in sukha, which is, which is experienced or enjoyed by all noble persons.
That is why he is worthy of, worthy of praise.
So in order to praise him, they say, this person has equanimity, this person is mindful,
and this person dwells in sukha.
And in this genre, there are how many factors, 3, 1, 3 or 2, you have to, I just stand
this for full method, not 5 full methods.
So only 2, right, sukha, and take kakada, the unification of mind, so do fight us.
Yes, okay, this is a third genre, then after third genre, we go to full genre.
But when it says he has equanimity, his mind is full and well in sukha, that his mind
also is not the third factor of mindfulness.
Mindfulness is with all jalas, mindfulness can never be without jalas, but here his
mindfulness is so, so good that it can keep beauty from arising.
When you pick attention, the close attention to what's happening at the moment, and you
can keep even beauty away from you, or a beauty from arising in your mind.
That is why he is pleased as having mindfulness or being mindful, not just ordinary
mindfulness, he has, this is very powerful and refined kind of mindfulness.
And he dwells in sukha, this sukha is not an ordinary sukha, it's very un-un-for-the-go,
I'm mixed on solid and it is the one experienced or enjoyed by noble persons.
Now we go to the fold, now he has to do the same thing, to find fault with here what, sukha.
It is going to neutral feeling now, so in this paragraph also, about the middle of the
paragraph 181, you know, the knowing there, so it should not be knowing, but when the
food jalas is about to arise, they arise in him, mind away at wedding and so on.
Now, food jalas is different from other jalas, because the food jalas are accompanied by
neutral feeling, but the first, second, and third jalas are accompanied by pleasurable
feeling.
Now, when we talk about the different conditions in Bhatana, that there are 24 causal
revisions in Bhatana, we call them conditions here, so when we talk about repetition
condition, then, since it is repetition, it must be of the same nature.
That means, when moments of consciousness arise one after the other, the previous consciousness
or matter of confidence arising together with previous consciousness can be the condition
as repetition condition of the succeeding consciousness only when they are of the same
nature.
Sometimes, pleasurable must be followed by pleasurable, neutral must be followed by neutral.
So, if neutral followed pleasurable, there can be no relationship as repetition.
So, in this food jalas, food jalas, thought process, pleasant feeling is not a condition
for neither painful nor pleasant feeling, and the preliminary work must be aroused in the
case of the food jalas with neither painful nor pleasant feeling, consequently peace, consciousness
of preliminary work are associated with neither painful nor pleasant feeling, and here
happiness vanishes, simply owing to their association with equanimity, what's that?
Now, what the comedy they are saying is, the pleasurable feeling cannot be a repetition
condition for neutral feeling.
Now, in the food jalas, there is neutral feeling and not pleasurable feeling, and if you
look at the thought process, the jalas consciousness is preceded by four or three moments
of sense fear, sense fear, jalanas, or impersonations.
So, since in the first thought process, since number five is accompanied by neutral
feeling, the others must also be accompanied by neutral feeling.
So, that is what the comedy they are saying, that is the only difference.
In the previous jalas, the sense fear, impersonations are accompanied by pleasurable feeling,
and jalanas, jalanas movements are also accompanied by pleasurable feeling.
But in this thought process for food jalanas, since jalanas be accompanied by neutral
feeling, the preceding karma, which jalanas must also be preceded by a company by neutral
feeling.
So, that is the only difference, that is what is what the comedy they are saying.
And here, in this translation, there is some error, because I already did it again, but
there is this difference, blissful place and feeling is not in condition as repetition
condition for neither painful, nor pleasant feeling, and the preliminary work must be
aroused in the case of the food jalanas, with neither painful nor pleasant feeling.
Actually, it should say, and in the food jalanas, neither painful nor pleasant feeling
must arise.
It is not in the preliminary, but it is in the full jalanas.
The neutral feeling must arise, there must be neutral feeling.
Therefore, the consciousness of preliminary work, that means karma of jalanas preceding
the ruba of jalanas, must be accompanied by neither painful, nor pleasant feeling.
Here, happiness vanishes, simply owing to their association with equanimity, here equanimity
means neutral feeling, because neutral feeling is feeling, and happiness of sukha is feeling.
So, if it is happiness, it cannot be neutral, and if it is neutral, it cannot be happiness.
So, since the full jalanas is accompanied by neutral feeling, there can be no sukha
feeling there, there can be no pleasurable feeling.
So, here, happiness vanishes, simply owing to their association with equanimity.
It may be a turn off for many people, because it is happiness vanishes, for a way to do.
But, happiness here is pleasurable feeling.
So, the neutral feeling is considered to be higher than the pleasurable feeling.
So, in this full jalanas, pleasurable feeling vanishes, and in its place, neutral feeling
arises.
This neutral feeling is so refined, that it is almost a kind of sukha, although it is
not technically sukha, it is the kind of sukha, which such people enjoy.
So, the full jalanas, with the abandoning of sukha and dukha, and with the previous disappearance
of somenas and dominoes, he enters the born and dures in the full jalanas, which is neither
pain nor pleasure, and which has purity of mindfulness caused by equanimity.
Now, with the abandoning of sukha and dukha, and with the previous disappearance of somenas
and dominoes, sir, you know, there are hundreds of thousands of thousands of thousands
of years old.
They're five kinds of feeling.
Sukha feeling, dukha feeling, somalanas are feeling, dominoes are feeling, uqbikha
feeling.
Sukha feeling, and dukha feeling are connected with body.
It is experienced in the mind, but it depends on the, it depends on the physical thing
of body.
So they are all sukha and dokha.
And samanhasa and dongmanhasa are mental, mental feeling, feeling of pleasure in the mind
and feeling of anguish in the mind.
Now, when you hit your, your finger with something, there is a faint ear and then you
have the experiences of that faint or feeling of that faint in your mind.
And that is dokha feeling.
And when you, when you think of something and you are, you are sad, then there is dongmanhasa
feeling or when you are angry, there is dongmanhasa feeling.
So they are different like that.
They are abandoning, with the abandoning of sukha and dokha and with the previous disappearance
of samanhasa and dongmanhasa.
Now, when we read this, it would lead us to, to, to mean that the sukha, dokha, samanhasa and
dongmanhasa are abandoned at the moment of, or just before close to the moment of
sukha, sukha, dokha, samanhasa and dongmanhasa occur during the preliminary stage of the
fourth jhanas.
Now, during the preliminary, preliminary stage of the fourth jhanas, samanhasa is abandoned.
And during the preliminary stage of first jhanas, dokha is abandoned.
And of second jhanas, dongmanhasa is abandoned and of third jhanas, sukha is abandoned.
So actually, they are abandoned during the preliminary stage of these four jhanas.
Now, that is why they said, with the previous disappearance of samanhasa and dongmanhasa.
So sukha, dokha, samanhasa and dongmanhasa are abandoned or eliminated before the moment
of fourth jhanas.
And during these stages, that is, during the preliminary stages of first jhanas, second
jhanas, third jhanas and also fourth jhanas, these feelings, sukha, dongmanhasa are abandoned.
Now, there is one passage in the sukhasa where I just said that they are abandoned
during the jhanas stage.
So the commentator pointed to that passage and then tries to explain it.
Now, it is true that they are abandoned in the preliminary stages.
But they are abandoned and they are not intensive, let me see what is used here, or reinforced
physician.
So their physician in the preliminary stages is not strong or not reinforced.
But at the moment of jhanas, there is reinforced physician or reinforced abandonment of
these things, these feelings.
So in the passage, pointed out by the commentator in the sukha passage, only the reinforced
physician is meant and not just the physician.
So that is why it is said that during the stage of jhanas, these are abandoned.
So in fact, these two statements are not against each other.
Now, paragraph 187, for accordingly, during the first jhanas access, which has multiple
adversity, I don't like the word multiple, actually it is different adversity, not multiple.
Because in a thought process, there is only one adversity, either five cents to
your advertising or mind or advertising, there can be only one adversity.
So here, jhanas access, which has different adversity, means the preliminary stage before
reaching the jhanas thought process.
Now, when it wasn't tries to get jhanas, he practiced his meditation and he gained
for the preliminary concentration and then access concentration.
So during the stage of access concentration, there are moments of this preliminary work.
And these moments consist of the series of thought processes and each thought process,
there is only one adversity.
So first jhanas access, which has different adversity, means while it wasn't is trying
to attain first jhanas, but not yet reach the first jhanas thought process, then those
stages are called and the jhanas access with different adversity.
If it is with the same adversity, then it will mean the jhanas thought process itself.
Here it is not jhanas thought process is not meant, but those occurring before the jhanas
thought process.
And those thought processes each of those thought processes has one adversity, but they
are different in different thought processes.
Therefore, they can be many thought processes of access concentration before reaching
the jhanas thought process.
So the pari wad nana can mean many or different, so here it means different.
So which is different avoiding, there could be re-arising of the bodily pain cycle, the bodily
pain cycle, the simply means pain, bodily pain due to contact with the cat flash and
so on.
So on page paragraph 182 which is called multiple and replace with different, which has different
abandon during the preliminary stages, why are they mentioned here?
Here, all four of them are mentioned in this, such as describing the full jhanas and the
common leader explains that it is done so that they can be easily understood or easily
grasped for a graph 190.
It is done so that they can be easily grasped, easily grasped, grasped means easily understood.
For the neither painful nor pleasant feeling described here, by the words which has neither
been nor pleasure is subtle, hard to recognize and not readily grasped, not readily
understood.
Now, among the feelings, the neutral feeling is the most difficult to see, most difficult to understand.
Pain is obvious and also good feeling through, through guys obvious.
But the neutral feeling is difficult to see, although one experiences neutral feeling, one
is not aware that that is neutral feeling, it is so, so difficult to understand so subtle.
So here, and the Buddha wants us to understand that neutral feeling, maybe with, in comparison
with other feelings.
So it gives all five feelings here, and then this is the feeling which is the most difficult
to understand or something like that.
So just as one, it cattle hard wants to catch a refractory ox that cannot be caught, that
all by approaching it.
He collects all the cattle in one hand and lets them out one by one, and then, I would
say, and then saying that it gets it, and so, I would strike on and so towards.
So saying that is it gets it, gets it caught, as well, not it gets caught, gets it caught
as well.
That means, let other people catch that cattle, this is the one I want to catch, and
so catch it, so the other people catch it.
So it should be after gets, and so should go.
So the sandals will run like this, and lets them out one by one, and then saying that
it gets it, gets it caught, as well.
So do the blessed one as collected, all these five kinds of feelings together, so that
they can be crushed readily.
For one day, you should collect it together, and this way, then what is not bodily pleasure,
bodily pain, mental joy, or mental grief can still be crushed in this way.
So what is, what is not suka, doka, stormas, and domanas, must be you pick up.
So the fourth journal is accompanied by neither pain nor pleasure or neutral feeling,
and it has the purity of mindfulness caused by equanimity, and that is in a specific
neutrality, actually, not only mindfulness, but all mental factors, all mental things
are purified in this, in this jana, and so it is stated, the teaching is given under
the heading of mindfulness, that means, only mindfulness is mentioned, but we must understand
not mindfulness only, but all others, headed by mindfulness.
There is an expression about that, you understand what I mean, sometimes we do not say,
we do not say, all thought we want to say, but we let people know by saying something
different from what we really want to say, say, I want you to understand that certainty,
I mean, mindfulness and others are purified by equanimity, but what I really say is
mindfulness is purified.
So I name mindfulness only, but I mean, mindfulness and others, what you say is in English,
is a way of speaking, something that I figure of speaks.
So I do not say so, but I just say mindfulness, but you must understand that I mean,
mindfulness and others.
Sometimes we say, say, the president comes, but he does not come alone, he comes with
others, but we do not mention others, something like that, surely.
Sometimes we say, surely, and we say, we just say a little bit, but sometimes we
just understand the rest as well, and then we should understand the purity of mindfulness
and that not mean the purity of mindfulness only, but purity of mindfulness and other
transcendence, and then we say, I mean, I mean, I mean, I mean, I mean, I mean, I mean,
meaning of the, it means this, under the heading of mindfulness, that means taking mindfulness
at the head of something, you just say mindfulness, but you understand not only mindfulness,
but others as well.
So this is the food journal, and how many, how many factors are there in food journal?
Two, two, because instead of suka, there is uki ka, so uki ka and ikagara, unification
of man, two.
So the third has two, and the fourth has also two factors.
This is four food methods, there are four journals according to this, but if you want
to experience five journals, then you go about eliminating uki ka and which are separated.
First, you try to eliminate uki ka, and then you get a second journal, and then you try
to eliminate uki ka, and then you get a third journal, so in, in, in that way, there are
five journals, and not four, but actually they are the same, depending on whether a person
eliminates uki ka, uki ka, uki ka, and which are at the same time, or separately one at each
time.
So in the, let us see, there are four journals, on, on the left hand side, and five
journals on the right hand side.
So if the first, in full-full method is the same as first in five-full method, or let
us see, the first in five-full method is the same as first in full-full method, and the
and third in 5-fold method is the same as 2nd in 4-fold method and the 4th in
5-fold method is the same as 3rd in 4-fold method and the 5th in 5-fold method is
the same as 4-fold method but you'll find a mention of 4 genres in the
shoulders. You very rarely find mention of 5 genres in the
shoulders, only 4 genres are mentioned and many many
shoulders but in Abidama all the 4-fold method as well as the 5-fold method is
mentioned. So that is the end of chapter 4 and it'll be great for next week so the
week after next we will continue and let's see how many pages
have to start up in two weeks. So please week two chapters chapter 5 and
have a look at what two factors are there in with the 4th joint S and M of D and
