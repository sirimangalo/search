1
00:00:00,000 --> 00:00:06,000
Okay, good evening. Welcome back to our study of the Damapada.

2
00:00:06,000 --> 00:00:13,000
Today, we continue on with verse 152, which reads as follows.

3
00:00:13,000 --> 00:00:20,000
Apasutaya-yang-puri-zo, Bali-bandhu-wa-ji-rati,

4
00:00:20,000 --> 00:00:28,000
monksa-anita-sa-wa-dhan-ti, panya-tas-na-wa-dity,

5
00:00:28,000 --> 00:00:32,000
which means

6
00:00:32,000 --> 00:00:39,000
man, this person is of little learning,

7
00:00:39,000 --> 00:00:44,000
grows old like an ox.

8
00:00:44,000 --> 00:00:48,000
Their flesh increases,

9
00:00:48,000 --> 00:00:55,000
but their wisdom does not.

10
00:00:55,000 --> 00:01:00,000
This was taught in regards to a specific elder

11
00:01:00,000 --> 00:01:04,000
when he was an elder because he'd been a monk for a long time,

12
00:01:04,000 --> 00:01:07,000
but that was about it.

13
00:01:07,000 --> 00:01:12,000
He wasn't an elder in any other sense of the word.

14
00:01:12,000 --> 00:01:20,000
He hadn't reached any sort of higher wisdom or knowledge.

15
00:01:20,000 --> 00:01:22,000
His name was Laludahi.

16
00:01:22,000 --> 00:01:29,000
Some of you might be familiar Laludahi.

17
00:01:29,000 --> 00:01:34,000
And he always said the wrong thing.

18
00:01:34,000 --> 00:01:38,000
When it was an occasion for giving a talk,

19
00:01:38,000 --> 00:01:40,000
rejoicing in people's good deeds,

20
00:01:40,000 --> 00:01:44,000
he would give a talk for a funeral,

21
00:01:44,000 --> 00:01:48,000
talking about dealing with sorrow,

22
00:01:48,000 --> 00:01:51,000
and when it was a sorrowful occasion,

23
00:01:51,000 --> 00:01:54,000
instead of helping people deal with the sorrow,

24
00:01:54,000 --> 00:01:57,000
he would talk about joyful things,

25
00:01:57,000 --> 00:02:00,000
how to rejoice in good things,

26
00:02:00,000 --> 00:02:02,000
totally useless.

27
00:02:02,000 --> 00:02:04,000
In fact, whenever he gave a talk,

28
00:02:04,000 --> 00:02:09,000
he would always say the wrong thing.

29
00:02:09,000 --> 00:02:11,000
And the monk said,

30
00:02:11,000 --> 00:02:17,000
what use is there in bringing this guy anywhere?

31
00:02:17,000 --> 00:02:20,000
He can't teach.

32
00:02:20,000 --> 00:02:21,000
Everything he says is wrong,

33
00:02:21,000 --> 00:02:23,000
he's always saying the wrong thing,

34
00:02:23,000 --> 00:02:25,000
and Buddha heard about it and said,

35
00:02:25,000 --> 00:02:29,000
oh, this wasn't, it's not just in this life,

36
00:02:29,000 --> 00:02:32,000
that Laludahi always said the wrong thing.

37
00:02:32,000 --> 00:02:36,000
He said, he did this in this past life as well.

38
00:02:36,000 --> 00:02:41,000
And the story goes that once there was this Brahmin,

39
00:02:41,000 --> 00:02:44,000
his name was Agidata,

40
00:02:44,000 --> 00:02:47,000
and he had a son named Soma Datta.

41
00:02:47,000 --> 00:02:51,000
And Soma Datta was,

42
00:02:51,000 --> 00:02:53,000
Soma Datta was wise.

43
00:02:53,000 --> 00:02:56,000
Soma Datta was the Bodhisattva

44
00:02:56,000 --> 00:02:59,000
in this past life story.

45
00:02:59,000 --> 00:03:04,000
Laludahi was his father.

46
00:03:04,000 --> 00:03:07,000
And they had two ox that they used,

47
00:03:07,000 --> 00:03:09,000
I guess, to plow their fields,

48
00:03:09,000 --> 00:03:16,000
and one day one of the oxen died.

49
00:03:16,000 --> 00:03:19,000
And the son said, well,

50
00:03:19,000 --> 00:03:26,000
you better go and ask the king for another ox.

51
00:03:26,000 --> 00:03:28,000
See if he can get some help,

52
00:03:28,000 --> 00:03:32,000
because I guess they supported the king

53
00:03:32,000 --> 00:03:34,000
or the king, supported them,

54
00:03:34,000 --> 00:03:36,000
somehow connected with the king.

55
00:03:43,000 --> 00:03:45,000
But the Brahmin asked his son to go and ask.

56
00:03:45,000 --> 00:03:47,000
And the son said, look, I can't ask.

57
00:03:47,000 --> 00:03:49,000
How can I ask?

58
00:03:49,000 --> 00:03:50,000
I'm just a young boy.

59
00:03:50,000 --> 00:03:51,000
I'm just a son.

60
00:03:51,000 --> 00:03:53,000
This father was, of course, a simpleton,

61
00:03:53,000 --> 00:03:54,000
and the son was wise.

62
00:03:54,000 --> 00:03:56,000
So he was depending on his son to do it for him.

63
00:03:56,000 --> 00:03:59,000
And he said, look, all you got to do,

64
00:03:59,000 --> 00:04:01,000
I'll teach you these verses.

65
00:04:01,000 --> 00:04:03,000
And he teaches them a verse,

66
00:04:03,000 --> 00:04:05,000
which is basically a request.

67
00:04:05,000 --> 00:04:07,000
He teaches them a speech,

68
00:04:07,000 --> 00:04:09,000
he prepares a speech.

69
00:04:09,000 --> 00:04:12,000
And the speech goes,

70
00:04:12,000 --> 00:04:14,000
I had two oxen, mighty king,

71
00:04:14,000 --> 00:04:16,000
with which I plowed my field.

72
00:04:16,000 --> 00:04:18,000
But one of the two is dead.

73
00:04:18,000 --> 00:04:20,000
Pray, give me another.

74
00:04:20,000 --> 00:04:22,000
Simple, right?

75
00:04:22,000 --> 00:04:26,000
Please give me another king.

76
00:04:26,000 --> 00:04:28,000
And the story goes that the Brahmin spent

77
00:04:28,000 --> 00:04:31,000
a year perfecting this.

78
00:04:31,000 --> 00:04:33,000
It's a little bit hard to believe,

79
00:04:33,000 --> 00:04:37,000
but he spent a long time memorizing this verse,

80
00:04:37,000 --> 00:04:39,000
preparing it.

81
00:04:39,000 --> 00:04:42,000
Okay, when he finally learned it by heart,

82
00:04:42,000 --> 00:04:44,000
the boy said, okay, follow me,

83
00:04:44,000 --> 00:04:46,000
we'll bring a gift to the king,

84
00:04:46,000 --> 00:04:47,000
and we'll give him the gift,

85
00:04:47,000 --> 00:04:49,000
and you can recite the verse.

86
00:04:49,000 --> 00:04:51,000
You can recite your speech.

87
00:04:51,000 --> 00:04:55,000
Let him know that you need a new ox.

88
00:04:55,000 --> 00:04:57,000
So they get to the king,

89
00:04:57,000 --> 00:04:58,000
and give him the gift,

90
00:04:58,000 --> 00:04:59,000
and he says,

91
00:04:59,000 --> 00:05:01,000
the king says,

92
00:05:01,000 --> 00:05:03,000
what do you have need of?

93
00:05:03,000 --> 00:05:04,000
Let me know.

94
00:05:04,000 --> 00:05:06,000
What can I do for you?

95
00:05:06,000 --> 00:05:07,000
And the Brahmin says,

96
00:05:07,000 --> 00:05:10,000
I had two oxen, mighty king,

97
00:05:10,000 --> 00:05:12,000
with which I plowed my field.

98
00:05:12,000 --> 00:05:13,000
But one of my two is dead.

99
00:05:13,000 --> 00:05:17,000
Please take the other, okay.

100
00:05:17,000 --> 00:05:19,000
And the king goes,

101
00:05:19,000 --> 00:05:20,000
what are you saying?

102
00:05:20,000 --> 00:05:21,000
Say it again,

103
00:05:21,000 --> 00:05:23,000
and he repeats it again exactly,

104
00:05:23,000 --> 00:05:26,000
exactly the same.

105
00:05:26,000 --> 00:05:28,000
And the king looks at some of that,

106
00:05:28,000 --> 00:05:29,000
and says,

107
00:05:29,000 --> 00:05:30,000
some of that,

108
00:05:30,000 --> 00:05:32,000
I guess you've got a lot of oxen

109
00:05:32,000 --> 00:05:36,000
that you can spare.

110
00:05:36,000 --> 00:05:38,000
And some of that to being wise,

111
00:05:38,000 --> 00:05:41,000
he says, instead of denying,

112
00:05:41,000 --> 00:05:42,000
instead of denying it,

113
00:05:42,000 --> 00:05:43,000
he says,

114
00:05:43,000 --> 00:05:44,000
you're majesty,

115
00:05:44,000 --> 00:05:50,000
we have just as many as you've given us.

116
00:05:50,000 --> 00:05:55,000
And the king very pleased with him,

117
00:05:55,000 --> 00:05:57,000
ended up giving him 16 oxen,

118
00:05:57,000 --> 00:05:59,000
and bunch of other stuff.

119
00:05:59,000 --> 00:06:02,000
He was very pleased with some of that.

120
00:06:02,000 --> 00:06:03,000
So when they get home,

121
00:06:03,000 --> 00:06:04,000
it's not even in the story,

122
00:06:04,000 --> 00:06:06,000
but in the jotica version,

123
00:06:06,000 --> 00:06:07,000
he asks him,

124
00:06:07,000 --> 00:06:07,000
he says,

125
00:06:07,000 --> 00:06:08,000
look,

126
00:06:08,000 --> 00:06:09,000
he's spent a year on this verse,

127
00:06:09,000 --> 00:06:13,000
why'd you mess it up?

128
00:06:13,000 --> 00:06:15,000
And he has some weird logic

129
00:06:15,000 --> 00:06:17,000
that one should never presume

130
00:06:17,000 --> 00:06:19,000
to ask something from the king,

131
00:06:19,000 --> 00:06:21,000
because if he gets this pleased,

132
00:06:21,000 --> 00:06:23,000
you can wind up in jail,

133
00:06:23,000 --> 00:06:24,000
or with your head cut off,

134
00:06:24,000 --> 00:06:26,000
or something.

135
00:06:26,000 --> 00:06:28,000
Anyway,

136
00:06:28,000 --> 00:06:31,000
it's just a silly little story.

137
00:06:31,000 --> 00:06:35,000
A very important verse, though.

138
00:06:35,000 --> 00:06:44,000
The bent of it is that

139
00:06:44,000 --> 00:06:50,000
the gist of it is that

140
00:06:50,000 --> 00:06:53,000
it's easy to become

141
00:06:53,000 --> 00:06:55,000
conceited about your age,

142
00:06:55,000 --> 00:06:56,000
about your seniority.

143
00:06:56,000 --> 00:06:58,000
I mean, this happens with monks a lot.

144
00:06:58,000 --> 00:07:00,000
Been a monk for a long time,

145
00:07:00,000 --> 00:07:03,000
and you think of yourself as an elder,

146
00:07:03,000 --> 00:07:06,000
and what do you have respect?

147
00:07:06,000 --> 00:07:08,000
Old people can be like this,

148
00:07:08,000 --> 00:07:11,000
maybe you're old.

149
00:07:11,000 --> 00:07:13,000
And Buddha wasn't,

150
00:07:13,000 --> 00:07:14,000
and you'd think,

151
00:07:14,000 --> 00:07:19,000
oh, somehow that I have life experience,

152
00:07:19,000 --> 00:07:21,000
life experience.

153
00:07:21,000 --> 00:07:22,000
It's true.

154
00:07:22,000 --> 00:07:24,000
The age should make you wiser,

155
00:07:24,000 --> 00:07:26,000
but it doesn't always.

156
00:07:26,000 --> 00:07:29,000
So there's this qualifying factor

157
00:07:29,000 --> 00:07:30,000
that's required,

158
00:07:30,000 --> 00:07:32,000
if you don't have it,

159
00:07:32,000 --> 00:07:37,000
you can be as older as seniors as you like,

160
00:07:37,000 --> 00:07:40,000
and you're not at all worthy of respect.

161
00:07:40,000 --> 00:07:43,000
And that quality is learning.

162
00:07:43,000 --> 00:07:46,000
Have you learned from life experiences?

163
00:07:46,000 --> 00:07:50,000
Have you learned from experiences?

164
00:07:50,000 --> 00:07:57,000
Have you learned anything?

165
00:07:57,000 --> 00:07:58,000
And so the worst sort of person

166
00:07:58,000 --> 00:08:00,000
is one who hasn't done any learning,

167
00:08:00,000 --> 00:08:06,000
who hasn't done even the most basic listening.

168
00:08:06,000 --> 00:08:08,000
They haven't gone to find wise people.

169
00:08:08,000 --> 00:08:11,000
They haven't asked questions.

170
00:08:11,000 --> 00:08:13,000
They haven't read books nowadays.

171
00:08:13,000 --> 00:08:16,000
We haven't books with lots of things in them.

172
00:08:16,000 --> 00:08:18,000
And they haven't even done that much.

173
00:08:18,000 --> 00:08:20,000
And if they haven't even done that much,

174
00:08:20,000 --> 00:08:23,000
they're the worst of people.

175
00:08:23,000 --> 00:08:25,000
This is a person who's like an ox.

176
00:08:25,000 --> 00:08:28,000
They can be rich or powerful,

177
00:08:28,000 --> 00:08:31,000
and they can be old,

178
00:08:31,000 --> 00:08:33,000
and venerate,

179
00:08:33,000 --> 00:08:38,000
old anyway.

180
00:08:38,000 --> 00:08:41,000
They're not be worth very much at all.

181
00:08:41,000 --> 00:08:44,000
They don't have their life be worth anyway.

182
00:08:44,000 --> 00:08:46,000
You might say they haven't lived.

183
00:08:46,000 --> 00:08:55,000
They've only survived.

184
00:08:55,000 --> 00:08:58,000
I think of before I found Buddhism.

185
00:08:58,000 --> 00:09:02,000
Before I had this inclination,

186
00:09:02,000 --> 00:09:06,000
I was seeking out knowledge,

187
00:09:06,000 --> 00:09:12,000
but before finding teachings on wisdom.

188
00:09:12,000 --> 00:09:15,000
It's interesting how I don't know what it's like now,

189
00:09:15,000 --> 00:09:19,000
but so many years ago.

190
00:09:19,000 --> 00:09:21,000
There was no inclination,

191
00:09:21,000 --> 00:09:27,000
and there was no inkling of where wisdom was to be found,

192
00:09:27,000 --> 00:09:32,000
and that was the most to be found in worldly studies.

193
00:09:32,000 --> 00:09:37,000
Maybe if I study philosophy, I'll find wisdom.

194
00:09:37,000 --> 00:09:50,000
So not even knowing,

195
00:09:50,000 --> 00:09:52,000
not even having the information.

196
00:09:52,000 --> 00:09:54,000
It's just the worst.

197
00:09:54,000 --> 00:09:56,000
If you never learn how to practice meditation,

198
00:09:56,000 --> 00:10:01,000
if you never have any instruction on how to practice meditation,

199
00:10:01,000 --> 00:10:04,000
you can never gain any sort of higher wisdom.

200
00:10:04,000 --> 00:10:07,000
It's the way of opening the door.

201
00:10:07,000 --> 00:10:11,000
For this reason, the tradition is very clear

202
00:10:11,000 --> 00:10:13,000
on the importance of study.

203
00:10:13,000 --> 00:10:16,000
We often look down our noses at study,

204
00:10:16,000 --> 00:10:19,000
thinking, it's not real wisdom.

205
00:10:19,000 --> 00:10:22,000
Real wisdom comes from meditation.

206
00:10:22,000 --> 00:10:24,000
We see people who study a lot,

207
00:10:24,000 --> 00:10:27,000
and they don't really know anything.

208
00:10:27,000 --> 00:10:30,000
So that other day doesn't seem to be this first type.

209
00:10:30,000 --> 00:10:32,000
It seems like he has heard a lot.

210
00:10:32,000 --> 00:10:37,000
He can recite all these different teachings.

211
00:10:37,000 --> 00:10:39,000
He just recites them at the wrong time,

212
00:10:39,000 --> 00:10:43,000
so there's something else missing.

213
00:10:43,000 --> 00:10:45,000
He's heard a lot, but he hasn't learned anything.

214
00:10:45,000 --> 00:10:48,000
So the Buddha is clearly saying something different here.

215
00:10:48,000 --> 00:10:52,000
He's not saying a person who hasn't heard a lot,

216
00:10:52,000 --> 00:10:57,000
saying a person hasn't learned a lot.

217
00:10:57,000 --> 00:10:59,000
So there are other people who have learned a lot,

218
00:10:59,000 --> 00:11:02,000
say they can remember and they can recite,

219
00:11:02,000 --> 00:11:04,000
and they can even relate.

220
00:11:04,000 --> 00:11:08,000
Back much of the Buddha's teachings,

221
00:11:08,000 --> 00:11:10,000
they can teach other people how to meditate.

222
00:11:10,000 --> 00:11:15,000
They can give our long talks on the most difficult

223
00:11:15,000 --> 00:11:20,000
of subjects reciting and repeating the Buddha's words,

224
00:11:20,000 --> 00:11:35,000
explaining the Buddha's words.

225
00:11:35,000 --> 00:11:38,000
But if they haven't actually learned them,

226
00:11:38,000 --> 00:11:42,000
and see this is really what makes talking about teachers,

227
00:11:42,000 --> 00:11:44,000
looking at a teacher in particular,

228
00:11:44,000 --> 00:11:47,000
what makes a good teacher is not that they have lots of knowledge,

229
00:11:47,000 --> 00:11:52,000
but that they can apply it.

230
00:11:52,000 --> 00:11:57,000
So when we learn how to practice meditation,

231
00:11:57,000 --> 00:12:02,000
our ability to apply it,

232
00:12:02,000 --> 00:12:08,000
as a whole other level of understanding,

233
00:12:08,000 --> 00:12:10,000
some people when they hear about meditation teachings,

234
00:12:10,000 --> 00:12:12,000
it's nonsensical to them.

235
00:12:12,000 --> 00:12:15,000
They can't really understand how to put it into practice,

236
00:12:15,000 --> 00:12:18,000
maybe they don't put out effort.

237
00:12:18,000 --> 00:12:21,000
When people who read the Satyapatana-sut,

238
00:12:21,000 --> 00:12:24,000
and without proper guidance and inclination,

239
00:12:24,000 --> 00:12:28,000
it's all just philosophy to them.

240
00:12:28,000 --> 00:12:31,000
And they might speculate about it or categorize it,

241
00:12:31,000 --> 00:12:38,000
put it in its place in their catalog of knowledge,

242
00:12:38,000 --> 00:12:40,000
without an understanding of it.

243
00:12:40,000 --> 00:12:44,000
They can't really relate or explain,

244
00:12:44,000 --> 00:12:49,000
put it to any use.

245
00:12:49,000 --> 00:12:51,000
So the second type,

246
00:12:51,000 --> 00:12:53,000
I think this is the sort of person,

247
00:12:53,000 --> 00:12:54,000
although I was.

248
00:12:54,000 --> 00:12:55,000
He had lots of knowledge,

249
00:12:55,000 --> 00:12:57,000
but he didn't know what to do with it.

250
00:12:57,000 --> 00:12:59,000
He had never taken the time to,

251
00:12:59,000 --> 00:13:01,000
or he didn't have the ability, perhaps,

252
00:13:01,000 --> 00:13:04,000
to reflect more deeply and understand

253
00:13:04,000 --> 00:13:09,000
what the meaning of the teachings actually was.

254
00:13:09,000 --> 00:13:14,000
That's such a person, even such a person who has understood

255
00:13:14,000 --> 00:13:19,000
the teachings and does understand how to put them into practice.

256
00:13:19,000 --> 00:13:24,000
Of course, that's still probably not quite what,

257
00:13:24,000 --> 00:13:27,000
not quite what the Buddha was talking about still,

258
00:13:27,000 --> 00:13:31,000
because such a person still doesn't have an understanding.

259
00:13:31,000 --> 00:13:36,000
You can't really say they've learned anything from the teachings.

260
00:13:36,000 --> 00:13:41,000
The Buddha said a person who studies a lot,

261
00:13:41,000 --> 00:13:43,000
but doesn't put it into practice.

262
00:13:43,000 --> 00:13:46,000
It's like a person who looks after other people's cows.

263
00:13:46,000 --> 00:13:49,000
They never get the milk,

264
00:13:49,000 --> 00:13:53,000
they never get the products from the cows.

265
00:13:53,000 --> 00:13:55,000
They might get paid,

266
00:13:55,000 --> 00:13:59,000
but they never get the fruit of the labor.

267
00:13:59,000 --> 00:14:02,000
When you teach others,

268
00:14:02,000 --> 00:14:05,000
it's great to watch them become enlightened,

269
00:14:05,000 --> 00:14:08,000
and it doesn't say anything about your own state of enlightenment.

270
00:14:08,000 --> 00:14:12,000
When you tell other people about Buddhism,

271
00:14:12,000 --> 00:14:16,000
teach other people who are past the knowledge on,

272
00:14:16,000 --> 00:14:21,000
you can't really be said to have learned anything yourself.

273
00:14:21,000 --> 00:14:24,000
You're just acting like a parent.

274
00:14:28,000 --> 00:14:34,000
The most important aspect of any learning

275
00:14:34,000 --> 00:14:36,000
and any knowledge is, of course,

276
00:14:36,000 --> 00:14:39,000
being actually putting it into practice.

277
00:14:39,000 --> 00:14:42,000
The learning we get comes from application.

278
00:14:42,000 --> 00:14:45,000
This is what we call bhāvānāmāyāpānya.

279
00:14:47,000 --> 00:14:50,000
It comes from mental development.

280
00:14:56,000 --> 00:14:59,000
However, this is not a great amount of knowledge

281
00:14:59,000 --> 00:15:02,000
or information is needed,

282
00:15:02,000 --> 00:15:10,000
but an ability to organize

283
00:15:10,000 --> 00:15:16,000
and then apply the teaching is all that's necessary.

284
00:15:16,000 --> 00:15:23,000
A person who learns nearly about the four foundations of mindfulness,

285
00:15:23,000 --> 00:15:25,000
if they're given enough information

286
00:15:25,000 --> 00:15:30,000
and able to process it well enough that they can put it into practice,

287
00:15:30,000 --> 00:15:34,000
and gain much more learning

288
00:15:34,000 --> 00:15:37,000
than a person who studied all of the Buddha's teachings,

289
00:15:37,000 --> 00:15:39,000
which is considerable amount.

290
00:15:39,000 --> 00:15:46,000
I met a man once who had read all of the Buddha's teachings twice in Pali.

291
00:15:46,000 --> 00:15:50,000
His Pali was pretty good, and he still had the weirdest views.

292
00:15:52,000 --> 00:15:57,000
He had some very strange views,

293
00:15:57,000 --> 00:16:04,000
but it had to do with studying and not actually organizing it

294
00:16:04,000 --> 00:16:08,000
or really understanding it and learning nothing from it.

295
00:16:08,000 --> 00:16:12,000
He admitted that he couldn't actually practice meditation.

296
00:16:12,000 --> 00:16:17,000
He denied that meditation had anything to do with becoming enlightened.

297
00:16:17,000 --> 00:16:19,000
He just have to hear the Buddha's words

298
00:16:19,000 --> 00:16:21,000
and that's the only way to become enlightened.

299
00:16:21,000 --> 00:16:24,000
Somehow he got this from reading the Buddha's teaching.

300
00:16:24,000 --> 00:16:30,000
Sort of evidence to me that study itself doesn't lead to practice,

301
00:16:30,000 --> 00:16:33,000
doesn't lead to wisdom and understanding.

302
00:16:33,000 --> 00:16:38,000
Our learning has to come from a deeper place.

303
00:16:38,000 --> 00:16:48,000
So the true learning that the Buddha is talking about here.

304
00:16:48,000 --> 00:16:51,000
It comes not from actually any knowledge,

305
00:16:51,000 --> 00:16:56,000
which whatsoever you don't technically have to have heard anything from anyone.

306
00:16:56,000 --> 00:17:03,000
And all that words and instruction can do is help those of us who are lost

307
00:17:03,000 --> 00:17:11,000
come to find reality, come to see the nature of reality.

308
00:17:11,000 --> 00:17:18,000
We're not trying to gain any sort of philosophy or outlook or belief

309
00:17:18,000 --> 00:17:23,000
or knowledge even.

310
00:17:23,000 --> 00:17:28,000
We're trying to gain what we're trying to gain as vision

311
00:17:28,000 --> 00:17:32,000
what we call Vipassana.

312
00:17:32,000 --> 00:17:34,000
Vipassana is a word we use.

313
00:17:34,000 --> 00:17:42,000
Instead of wisdom, we don't often talk about panya so much as Vipassana.

314
00:17:42,000 --> 00:17:46,000
And there's the reason for that is because it's not really a knowledge.

315
00:17:46,000 --> 00:17:49,000
It's a vision.

316
00:17:49,000 --> 00:17:51,000
It's not literally seeing with your eyes,

317
00:17:51,000 --> 00:17:56,000
but it's experiencing experiential wisdom.

318
00:17:56,000 --> 00:17:59,000
When you see for yourself that everything inside of you

319
00:17:59,000 --> 00:18:03,000
and in the world around you is impermanent.

320
00:18:03,000 --> 00:18:08,000
If everything inside you and the world around you is unsatisfied.

321
00:18:08,000 --> 00:18:12,000
And when you see that everything is non-self,

322
00:18:12,000 --> 00:18:21,000
that there is no control or ownership to be found.

323
00:18:21,000 --> 00:18:24,000
When you see for yourself that our attachments and our

324
00:18:24,000 --> 00:18:29,000
versions and our delusions and our conceits and arrogance

325
00:18:29,000 --> 00:18:34,000
is that all of these are simply a cause for stress and suffering.

326
00:18:34,000 --> 00:18:41,000
That we heard ourselves and bring ourselves in trouble and upset

327
00:18:41,000 --> 00:18:45,000
and we can't blame, let me see that we can't blame suffering

328
00:18:45,000 --> 00:18:49,000
on the world around us or our experiences

329
00:18:49,000 --> 00:18:53,000
of blaming on our own minds.

330
00:18:53,000 --> 00:18:57,000
And when we learn the ways in which we can rectify this

331
00:18:57,000 --> 00:19:01,000
when we can learn to let go,

332
00:19:01,000 --> 00:19:08,000
learn to see clearly and objectively.

333
00:19:08,000 --> 00:19:13,000
When we change, when we open up,

334
00:19:13,000 --> 00:19:18,000
when we free ourselves from our own clinging,

335
00:19:18,000 --> 00:19:21,000
from our own bondage.

336
00:19:21,000 --> 00:19:27,000
When we realize nibana, when we experience this liberation

337
00:19:27,000 --> 00:19:30,000
that is free from suffering,

338
00:19:30,000 --> 00:19:41,000
that is what we mean by learning.

339
00:19:41,000 --> 00:19:44,000
So perhaps the best way to understand it is in stages.

340
00:19:44,000 --> 00:19:50,000
Of course, intellectual learning of the right sort is important

341
00:19:50,000 --> 00:19:55,000
without it as this first as wisdom can't grow.

342
00:19:55,000 --> 00:19:59,000
But even that sort of learning should be understood as

343
00:19:59,000 --> 00:20:07,000
insufficient, necessary but not sufficient.

344
00:20:07,000 --> 00:20:12,000
One must take one's knowledge, one's learning and understand

345
00:20:12,000 --> 00:20:18,000
that the plot and organize it in one's mind

346
00:20:18,000 --> 00:20:20,000
to be able to put it into practice.

347
00:20:20,000 --> 00:20:22,000
What are the four foundations of mindfulness?

348
00:20:22,000 --> 00:20:23,000
What do they mean?

349
00:20:23,000 --> 00:20:24,000
What is their purpose?

350
00:20:24,000 --> 00:20:26,000
What is their use?

351
00:20:26,000 --> 00:20:29,000
How will I accomplish this?

352
00:20:29,000 --> 00:20:33,000
How do I do walking meditation, how sitting meditation,

353
00:20:33,000 --> 00:20:38,000
organizing and implementing,

354
00:20:38,000 --> 00:20:44,000
and then applying the teachings

355
00:20:44,000 --> 00:20:47,000
and really and truly practicing.

356
00:20:47,000 --> 00:20:49,000
That's of course the most prayer.

357
00:20:49,000 --> 00:20:52,000
It's much easier to find people who know the teachings

358
00:20:52,000 --> 00:20:55,000
and who even maybe understand the teachings

359
00:20:55,000 --> 00:20:58,000
of practice than that's very rare.

360
00:20:58,000 --> 00:21:00,000
Among those who practice it's even more rare

361
00:21:00,000 --> 00:21:05,000
for to find those who practice wholeheartedly enough

362
00:21:05,000 --> 00:21:17,000
and intently enough to free themselves from suffering.

363
00:21:17,000 --> 00:21:22,000
Just one more thing we might say is this first is a clear

364
00:21:22,000 --> 00:21:25,000
indication of the emphasis and Buddhism.

365
00:21:25,000 --> 00:21:29,000
The emphasis is not in the living.

366
00:21:29,000 --> 00:21:34,000
It's not in believing.

367
00:21:34,000 --> 00:21:38,000
The emphasis is not even just in being a good person.

368
00:21:38,000 --> 00:21:41,000
The emphasis is on learning.

369
00:21:41,000 --> 00:21:43,000
The emphasis is on wisdom.

370
00:21:43,000 --> 00:21:48,000
The idea that we can't force ourselves to be good people.

371
00:21:48,000 --> 00:21:56,000
We can't force ourselves to be free from the foundment.

372
00:21:56,000 --> 00:22:01,000
But with wisdom goodness comes with goodness follows.

373
00:22:01,000 --> 00:22:04,000
With wisdom evil is eradicated.

374
00:22:04,000 --> 00:22:11,000
Simply through seeing clearly.

375
00:22:11,000 --> 00:22:13,000
So there you go.

376
00:22:13,000 --> 00:22:16,000
That's the number part of first for tonight.

377
00:22:16,000 --> 00:22:19,000
Thank you all for tuning in.

378
00:22:19,000 --> 00:22:47,000
Have a good night.

