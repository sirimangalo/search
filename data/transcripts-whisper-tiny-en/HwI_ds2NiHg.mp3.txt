Hello, welcome to our weekly Dhamma session.
This is our first session of the New Year.
First session of the decade I was reminded.
Depending on how you counted, this is the New Decade.
Happy New Year! Happy New Year! Isn't the statement a wish?
It's actually an expression of made of kindness, friendliness.
As such, it's a fairly Buddhist thing to say, or it's in line with Buddhism to say it.
May you all have a happy New Year.
The qualifier, of course, is that happiness and Buddhism
is understood a little bit differently, perhaps, than how most people would understand happiness.
Rather than means to attain happiness, a little bit different.
The standard for what constitutes happiness, a little different.
So we wish someone to have a happy New Year.
We're not expecting them to go out partying New Year's Eve, get drunk,
make a full out of themselves, perhaps,
endanger themselves in others, and end up regretting it,
end up miserable the next day.
No ordinary happy New Year might look quite happy at the time,
but I used to mention to my students that, in Thailand,
New Year's Eve is the second most dangerous day of the year.
The second most miserable day of the year you might say,
in the sense of number of fit of driving fatalities.
A lot of death New Year's Eve.
Number two, number one is the Thai New Year, which is very much associated with alcohol
and for many people and drunk driving.
Putting aside unforeseen circumstances,
or should have foreseen circumstances.
Even the happiness that comes from revelry on New Year's Eve,
it doesn't really suffice, doesn't really meet the,
so it doesn't really pass the bar on what can be considered happiness.
It's not that we don't acknowledge the pleasure.
There's pleasure there for sure.
But it's not enough.
There is something higher, and there's, in fact,
that which anyone who experienced it would agree is better.
Well, this is nicer than that.
Problem is, it's not as easy to experience,
but it's harder to attain.
It's more of a challenge.
Happiness turns out to be dependent on some skill.
Happiness turns out not to be something you do on your day off.
Something you take a break to fall into.
Turns out, in fact, that if you work,
if you train yourself, if you align yourself or incline yourself
in the right direction, you can actually improve upon happiness,
refine your state of happiness.
This is because of the nature of the world that
knows things that we cling to, those things that we crave for,
those things that please us.
No matter how pleasurable they are,
they are unpredictable.
They're uncontrollable.
So they change, or they fail to appear.
And more importantly, they leave us in their wake.
The way we relate to pleasure, to experiences,
to pleasant experiences, is based on addiction.
So the only difference between the central pleasure of things like music,
or food, or drink, or sex,
and drugs like heroin, cocaine,
no matter of degree, the addiction is the same.
Sugar, apparently sugar, is a highly addictive substance.
There's no difference between the addiction to sugar and addiction to heroin.
Did I read something like that?
I think I'm not right there, but there is still addiction.
I can get addicted even to such physical things as food and music.
So the reason why pleasure is not considered to be happiness,
is because, first of all, it can't last.
It's unpredictable, it can't fulfill our desires.
I'm second of all, because the nature of desire is contrary to happiness.
The nature of desire is inherently tied up with addiction.
You can't enjoy some central pleasure without cultivating addiction towards it.
And unfortunately, that addiction is stressful,
it's ultimately unpleasant. The equation continues to tilt in the wrong direction,
so the objects of your desire become less and less pleasing.
This is just the nature of things.
I'm neither, it keeps tilting in the wrong direction,
and we keep getting what we want,
or else we eventually fail to get what we want, and it collapses it,
falls entirely in the wrong direction of creating great suffering and stress for us.
It's the nature of addiction.
When we go through withdrawal, inevitably we have to sort out our attachments.
So it turns out that there is something higher than that.
There is a way to refine our experience,
to the extent that we are happier,
and we experience greater happiness.
And we free ourselves from the stress and the worry of losing our happiness.
The stress and the worry of having our happiness threatened because it's above
the changes of life. It's above the nature of impermanence.
It's independent, a needseed tool, doesn't depend on anything.
Just be happy, that kind of thing, where we can be happy not because of what we experience,
but because of how we experience, because of our frame of mind,
our state of mind, our quality and our strength of mind.
There's a story that I used to hear around this time,
that relates to this idea.
In Thai, the translation is happy New Year, so they say, so what do you mean by?
So what do you say is a very well-known Thai phrase,
if you know anything about Thai language or Thailand, you've heard the word,
so what do you mean?
So what do you say is actually a corruption of Sanskrit word Swasti,
which may sound familiar as well because of the word Swastika.
The word Swastika means the bringer Mika suffix, bringer of Swati, Swati, Swati.
That's a Hindu word that was co-opted by the Nazi Party in Germany and used to represent their ideals.
Actually, I guess used as a good luck charm, they apparently were into the occult and all sorts of superstitions.
But the sun, I guess, the sun, and the representation of Swastika was thought to bring well-being.
Swasti, Su, Swat, and Asti, Asti being Swat,
good or well. So when you say, so what do you or Swasti means well-being?
So what do you mean by well-being in the New Year?
And it happens to be a topic that the Buddha himself talked about.
There's a story of an angel in heaven, sort of a lord of a group of angels, Subrahma, Subrahma.
His name was Subrahma, and he was the lord of the large retino of angels.
And the daily activities of these angels was every day they would go out into their heavenly divine gardens that were beyond anything you might see on earth, truly divine, pleasant, enjoyable, agreeable,
pleasant sound, no mosquitoes, a wonderful place, a wonderful place to meditate if they had thought of it, but they didn't think of it.
Instead, what they did is they would take turns, some of them would go up, would climb up into the trees and pick flowers,
some of them would stay on the ground and dance and sing, and the ones up in the trees would throw down the flowers to the ones below.
So the thing you might expect angels to do, somewhat refined actually.
I mean, there was no drunken revelry, there was no debauchery, it was very innocent sounding, throwing the flowers, singing, dancing.
But incredibly pleasant, enjoyable, a refined sort of enjoyment. You don't get to go to heaven just by wanting pleasure, absolutely.
There's a sense of refinement, a high sense of refinement that's required, anyone that gets into heaven has to have a very pure and clean sort of mind.
The pleasures they engaged and were quite pure in that sense.
But they were still based on addiction, had nothing to do with goodness, had nothing to do with the qualities, in fact, that would lead one to go to heaven.
And that's the problem with attaining good things.
They might come from the best intentions by being a good person, by being kind, by being ethical, by being pure and hard.
Good things will come to you, but enjoying those good things is often has the opposite results.
It has a corrupting influence, to become addicted, to become lazy, and become complacent, negligent.
And that's what happens to a lot of divas, a lot of angels.
So one day they're up, half of them up in the trees, half of them down below.
And suddenly, Subrahma comes out and he walks through his garden, and he doesn't hear the ones up in the trees anymore.
And he asks the others where did they go, where are these, where are fellow angels?
And he sends out his mind because angels have this sort of special capacity to perceive things beyond humans.
And he perceives that they're no longer up there, there isn't one anyone up in the trees, they've just disappeared.
And he sends out his mind and he detects that they've all died from heaven.
His life span in heaven has ended, and they're all born in hell.
It's quite a shocker.
Born in hell because of their incapacity to cope with death.
It doesn't actually take evil deeds, it just takes an intense anger-based state of mind at the moment of death.
So I don't know whether these angels had done very bad things to provoke that or whether they had just cultivated such addiction that they got very, very afraid, angry, upset when they died.
But they probably weren't Buddhist.
And he certainly didn't practice in any way that would conform to the Buddha's teaching.
And this, of course, unsurprisingly, scared Subramma very much.
And he said, do you have gone to the Buddha and asked the Buddha? Is this recorded in the Sutras?
How does one find well-being?
Probably with Sauti, just a simplification of the word Sauti.
So if you hear I say Sauti, Sauti means well-being to you.
Sauti, the standard Buddhist greeting.
Sauti, Sauti, and the Buddha said, and it's a fairly well-known verse,
Nantra, Bodja, Tapasana, Nantrasana, Nantrasana, Nantrasana, Nantrasana, Nantrasana, Sauti,
Nantrasana, praninah.
up asami, I don't see sooty happiness or I don't see well-being,
bhaninang for beings, Anyatara, apart from four things,
apart from wisdom, bhodja, apart from effort, tapasa, exertion,
apart from injury asamwara,
so injury asamwara guarding the senses,
apart from sabhanisaga, apart from abandoning or letting go of everything,
and abandoning letting go, relinquishing everything.
It's a fairly extreme teaching.
It's a bit of a wake-up call to hard pill to swallow
the idea that any of these things might lead to happiness as foreign to most people who seek happiness.
Any pleasure, seeker?
Wisdom, of course, seems to, well, perhaps,
perhaps makes the most sense of the four that an unwise person would be unhappy,
but still there's quite a disconnect, the idea that somehow you need to apply
some high quality of mind in order to be happy seems
to some extent antithetical to the idea of just being happy of being,
of being able to put down your burden, and now we have to be wise.
But I don't think it's a hard stretch to understand the wisdom of this statement,
to this extent anyway, that an unwise person is going to be very unhappy,
we just don't appreciate how true that is,
what an important role wisdom plays in happiness.
Without wisdom, the Buddha said there is no, there is no well-being,
there is no sweat deep in mind, there is no true happiness,
there is no freedom from the miseries of change, loss,
and the unpredictable nature of life, there is no peace.
Without wisdom, we misinterpret, we misunderstand everything,
we come in contact with every experience, in fact,
moment by moment experiencing things, we misunderstand them,
we grasp them incorrectly as though a person grasping a knife by the blade,
we cut themselves, there's nothing wrong with a knife,
they pick it up correctly if you are wise enough to know how to use a knife,
can't hurt you, no one would think picking up a knife would cut you,
but certainly if you'd pick it up for the wrong end,
you're going to hurt yourself if you're not wise in the way of knives.
Meditation is very much about cultivating the wisdom
of how to pick up our experiences like picking up a knife,
how to interact with experience in a positive and beneficial in a stress-free way,
in a peaceful and happy way,
in a way that leads to well-being.
And it's not a hard to understand way,
it's a challenge, it's a difficult to accomplish practice,
it requires wisdom, but it's not esoteric or complicated,
it just means to experience things without all of the baggage that we normally
associate with experience. Let's see, just be seeing, let hearing,
just be hearing, when we have this purity of experience
that we try to cultivate in meditation, when we achieve that,
and we find true peace and happiness we're able to interact with our
experience, free from stress, free from suffering,
free from clinging,
free from Mara.
But the others are a little more difficult to appreciate,
won't just something we can understand, we can appreciate wisdom,
something we can appreciate the value of Papa Sa that well-being should
require effort, we don't appreciate how much work it takes,
and it's not work maybe per se, it's the
training, the energetic quality of mind,
we need energy, we need to energize our minds, we can't have
sluggish or lethargic minds, we won't be able to keep up with reality,
reality has a very quick pace, it's quick to change,
what we find in meditation is that we're sluggish, we're often left behind,
we're incapable of keeping up with the change,
meditation can be quite uncomfortable in the beginning for this reason,
we're not strong enough, we're not energetic enough, we're not keeping up,
so we keep tripping over our own experience,
falling into grasping again and again,
even in meditation, in meditation we're grasping at experience,
something good comes, we react to it, we get lost in it,
and we suffer when it changes, something bad comes, we get lost, we get caught up
in it, we find ourselves overwhelmed with anger or frustration,
fear even, worry, doubt, but if we train ourselves,
if we adjust our minds and we shift our focus into being energetic
and to keeping up with life, with our reality,
the changing nature of reality,
we find it's much more peaceful, much more pleasant, much more agreeable,
much more free from danger and stress and strife,
we find that we can live a life that is free from
the stress of addiction, of obsession, of clinging to
things that can satisfy us.
The third is restraint of the senses, which really goes very much contrary to
our ordinary perception of happiness,
that happiness should be the engaging in the senses,
when you see something really try and see it as much as you can and as intensely
as you can, when you hear a sound turn up the volume, if it's a pleasant sound,
turn it up and climb towards it,
refine the sound, we'd buy these expensive earphones
to appreciate music,
get more refined delicacies of food or more intense
taste, I'm in intense tasting foods,
junk food, they call junk food that exists only for the pleasure of its taste,
and in fact there is no happiness there, that in fact the pleasure that comes
from all these things is in no way sufficient to be considered happiness,
it's inferior and based,
it has no capacity to make us a happier person,
let's phrase it that way to really understand what we mean,
it might seem very much like happiness when you're enjoying it, but the outcome is
not a happier person, it's not a person who is as happy as they were before,
it's a person who is more inclined towards addiction,
a person who is less satisfied,
more needy or clean, more clingy,
so restraint of the senses is something that but I did talk about
quite often it's a sort of a standard practice of a Buddhist meditator or a monk,
to limit your interactions with essential desire, central pleasure,
try and guard your vision,
guard your physical interactions, your sensory interactions,
but ultimately that isn't precisely what is meant here,
or that isn't the highest sense in which guarding the senses is meant,
because simply guarding from pleasant experiences isn't going to make you happy,
but the true guarding of the senses or restraint of the senses
is actually something much more pure and peaceful,
and again comes back to the experiencing sense experience just as it is,
to be in line with reality,
because there's nothing about any experience that
is intrinsically desirable,
just like there's nothing about any sensory experience,
experiencing experience that is intrinsically
repulsive, undesirable,
there's nothing about sense experience that can make us unhappy,
just like there's nothing that can make us happy,
what makes us happy and unhappy is our perception of them.
We perceive certain things as being pleasant,
they remind us of other things that have made us happy,
when we see something we hear sound,
and so they trigger, they have the capacity to trigger
pleasure, stimuli in the brain,
there's nothing intrinsic about them that is pleasant,
they just have the capacity to trigger something in our brain,
which releases certain chemicals and those chemicals
we find enjoyable and ultimately addictive,
so you can't actually, through indulgence and sensually,
you can't actually be happy
because you're cultivating directly explicitly
unavoidably, undeniably, you're developing addiction.
Every moment that you enjoy some central pleasure,
brain science tells us this,
it's not a question, it's not in doubt by the even modern science,
or so restraint of the senses is a way of experiencing things in such a way
that they no longer create addiction or aversion,
they no longer create fear as well, they no longer create anger,
no longer cultivate the opposite of addiction, which is our revulsion, our opposition,
how many things trigger us to be afraid or angry or frustrated.
You might see certain things and immediately be plagued by
all sorts of negative emotions,
seeing things just as they are having a state of mind that is
completely in tune with the reality of the truth of the experience,
there's a refinement there that is well beyond any sort of
ordinary appreciation of central pleasure,
there's a happiness and a peace that is sustainable
as independent of our experience.
There's an incredible sense of well-being, of safety,
security, unshakableity, you can't be shaken, you can't be
you can't have it taken away from you,
you can't be denied this sort of happiness,
there's nothing that anyone can do, nothing that you can experience, that will
change your clarity of mind.
And finally letting go of everything,
letting go is and immediately thought of as the cause of happiness,
it's not on New Year's.
Suppose something about New Year's is very wholesome is the letting go of the past,
if we can do it, but I think it often has to do with
trying to forget the past, which I don't think is really what is meant by letting
go of the past. Surely we should let go of the past in the future,
but I don't think we should forget about it.
I don't think we should cease to learn from it.
Most importantly, I don't think we should ignore it.
In the sense of trying to get away from all the bad,
there's a lot of resentment or negative association with the past year.
But the past year could have been a very positive learning experience,
not to trivialize the terrible suffering of the year.
Of course, it was an incredibly suffering time for so many people.
For many people, it was fatal.
There was a lot of an inordinate amount of death and loss.
But here we are, here we've come, and our relationship with the year should not be
one of anger or disgust or resentment.
We should let go of the past,
just as we should let go of the future, just as we should let go of the present.
But we have to be able to let come as well.
And to the extent that we resent what has happened in the past year,
that resentment is going to lead us to further suffering.
Letting go means letting come.
Letting go means not getting caught up in the past or the future,
not dwelling dependent on the past or the future.
Letting go of everything is in fact the most crucial part
of what leads to true happiness and well-being.
If there is nothing you cling to, there is nothing that has any power over you.
There is nothing that can shake you from your state of peace and
contentment.
We often talk about contentment and Buddhism as precisely for this reason, contentment is
a very powerful state.
Because it is our discontent that makes us unhappy, that makes us need to strive for pleasant
experiences, our inability to
be content with our present reality anyway.
I am going to wrap it up there so we can get around to some questions.
I want to keep everyone here, all the people here to help me.
I want to keep them beyond the time, but just a special message for the new year.
But I said these four things lead to sooty, well-being or true happiness.
So wish everyone, all of us may have a happy new year by cultivating these four things.
Wisdom, energy, restraint, and freedom or renunciation, relinquishment, letting go.
All right, so now we'll move into the question period, if anyone has any questions.
Go ahead and I think, let's see, Chris was here to ask them and we have Ulup and Jim here to support
and Olivia is our moderator in the chat. So from now on, chat messages should be restricted to
questions only. If you don't have questions, just close your eyes and we'll meditate together.
Okay, let's begin.
I often suffer from loneliness and not having as many friends as I'd like,
any tips to help with suffering from this.
So it's a good example of the two approaches, two things. When you want something,
there are two ways to solve this problem. One is to get what you want and two is to stop wanting
for yourself from the wanting. So the way you phrase it, it's a very sort of Buddhist way to
phrase it is that we acknowledge that we have this state of mind and rather than try and solve it
by fulfilling our desire. Let me try to approach from a perspective of desire, of removing the
desire. So to be clear about how Buddhism sees desire and the objects of our desire,
there isn't any special work that needs to be done beyond what is plainly evident.
But as practice requires a stasis of sorts where you maintain your experience of things as they are.
So the tension involved with wanting something, the very nature of wanting something has attention
in it, it implies a lack, it wouldn't want if you had. And so well, there is the wanting,
there is the tension. And so stasis the staying with that state
is that which that's what that's what will lead to freedom from the state. There is no
there is no special need to remove oneself from the wanting or to remove the wanting from oneself,
simply seeing the tension, it unravels the entire problem just like a knot that is untied.
When you untie a knot, no matter how big or complicated the knot is, once it's untied,
it's as though the knot was never there, completely unravels into nothingness.
That's how meditation feels, that's how mindfulness feels, it's like a water pouring water over a
sticky substance, it washes it away. It acts as a solvent to remove all the
defilement and all the grime and all this sticky attachment.
So like any other sort of craving or wanting or yearning, there's nothing that needs to be done
besides simply observing it. That creates all these prerequisites of wisdom,
because it helps you see it more clearly. Energy, it becomes energetic, it becomes strong,
not tossed about by this desire, but fixed in your observation of it.
It becomes restraint, meaning restraint in the sense of just seeing it as it is with no extrapolation.
You limit your experience to what is actually there.
And you let go, you have no reactivity involved. You realign yourself to an experience
that is simply understanding it as it is. So basically it's something like saying to yourself
wanting or sad, noting all of the qualities involved with loneliness, without any inclination
to try and fix or change it in either direction of repressing the desire or seeking out the object
of the desire.
Can I say to others that my practice, which is based on your teachings, is to reach 100%
conformance with reality? That's a good way of putting it. I don't know if I've ever heard that.
It may not be quite like that because it's not like your 50%
in conformance with reality. I suppose, I mean, I'm not convinced about the percent part, but
it's not a matter of percent per se, but the idea of being perfectly in conformance, it's in
fact the word that is used to describe the moment where the mind lets go, or the moment right
before the mind lets go, that which triggers the letting go is called anuloma.
Loma means grain, the grain like wood has a grain, we've ever chopped logs,
we've ever chopped firewood, you see people chop firewood, if you don't know you can only
chop firewood a certain way, it's because wood has a grain, it has lines in it, rings on the tree,
you know, but those rings are in a cylinder and you have to cut along the line,
you can't cut on a piece of wood sideways, so when you chop down a tree, it doesn't chop right
through, it doesn't break, you have to chop chop chop, it's harder that way. Loma is that grain,
it's a simple poly word. Anu it means following, so going with the grain, I think it's right
the English expression, anuloma, anuloma nyana, it's the going with the grain, when you're in
conformity, meaning the grain of reality, so going with the grain means conforming, the mind is no
longer out of conformity, there's no longer out of touch with reality, let's put it that way,
which desire is, desire, clinging, reaction, all of that is out of touch with reality, it's like
not present with the experience as well as it could be.
Is it improper to intently focus only on unpleasant feelings in order to see their true nature,
or should one just note any feeling that arises? So trying to intently focus is where you
run into problems, because it's not done in a vacuum, when you intently focus specifically on one
factor of reality, you're cultivating certain mind states, it's kind of selective and the problem
with that is there's an ignorance, there's nothing wrong with focusing on one thing,
but when you don't focus on something else, something that when it arises, like say a pleasant
experience, there's a quality of mind cultivated there, ignorance, there's a lack of clarity.
So the reason for focusing on everything is because when something arises, there's a reaction,
regardless of what you decide, there is a reaction, and you ignoring things is a reaction in itself,
it's a reaction based on ignorance, I mean there's no wisdom involved, there's no clarity of
mind unless you cultivate it, if you're not cultivating it for whatever arises, you're going to run
into troubles, you're going to cultivate that sort of ignorance.
So there is room for some sort of focus, we do encourage people to try and focus on the main object,
the stomach for example, but you can't do that at the cost of ignoring something,
if you aren't vigilant about interacting and acknowledging the existence of things,
you're going to run into trouble.
I've struggled with depression my whole life, especially the last four to six years,
including a lot of destructive behavior. I just can't get out of this circuit. What advice can
you give to help break this cycle? Well, at first of all, it's going to take some time,
it's going to take some effort, it's not the kind of thing you can free yourself
quickly from, you have to acknowledge that that's a part of
your field of experience, it's going to be a part of your practice probably for a long time.
So don't think of meditation as allowing you to avoid or break the cycle of depression.
But rather, it's a means to redirect your attention, redirect your perception.
So it looks very much in the beginning like a different perspective on your depression.
You have to see it like that, it's not that it doesn't cure things like depression,
it's that cure, cure looks a lot like realignment,
and it's a gradual process, breaking down bad habits, deconstructing them, and
ultimately abandoning them takes time. That's the very nature of the word habit,
implies some sort of entrenchment that can't simply be removed or abandoned.
When walking, I feel more and more that the foot moves by itself is it a sign of progress in
the meditation, or does this have any significance?
It can be a good sign, certainly. But like anything, it's something that you should just try and
to be mindful of trying to acknowledge it as it is. When walking and my concentration is good,
I often experience a light pressure in my face. Should I note this every time it arises?
You should note anything that you experience. So when walking, you can, to some extent, limit.
I just said don't ignore things, but in walking, you have to kind of do the best you can.
You're ultimately going to be ignoring something, so you certainly can stop each time,
but you should stop walking and note it. If eventually you want to try to get to the point
where you're not distracted so much by things, and that you're able to stay with the foot without
letting every little thing distract you. So practically speaking, you do sometimes ignore things
if they're very fleeting. But certainly if there's a pressure on your face, stop,
note it. We're not trying to get anywhere with walking. A misunderstanding or
one problem that meditators deal with early on is the desire to continue walking out of a
sense of accomplishment, how many times you could walk back and forth or something.
Because it's sort of an unconscious need to perform to succeed when your goal is to walk or
your intention is to walk, stopping all the time seems counter to the goal. In fact, it's not
stopping and doing standing meditation is a very proper and real sort of meditation practice.
Are there benefits to meditating or chanting in large groups?
There's some small benefit. There's not any very deep benefit to it. In fact, it can be quite
distracting. It's often easier because of the distraction. But remember, easy isn't really what we're
striving for as you can maybe see from what I was saying earlier, happiness is not something that
is easy. I think this is something you require skill. I make music for a living, and I find that
noting can become more difficult while playing an instrument is it simply best to note playing
while I practice guitar and singing while I sing? Singing may be, but that's pretty abstract.
So yes, yes, you certainly can, but it won't be that productive as opposed to noting the movements of
the body and the feelings in the in the voice box, for example. Ultimately, when you're doing
anything in the world, you should just try and do the best you can. The most simplest to note
your postures of standing, walking, sitting, lying.
What level of impermanence should we see to realize Nibbana? For example, when moving,
is seeing the lifting, then the moving, then the placing enough? Or should we see the many moments
of those movements? To see Nibbana, it requires just one moment of pure experience of impermanence,
and it's not quite the experience of it, it's the appreciation of it. What's required is the
mind to let go, and the mind lets go when it sees impermanence and suffering and non-stop. It sees the
three characteristics repeatedly incessantly. So when there's anuloma, this conformity is like an
appreciation, a acceptance, when you accept it as reality and your mind conforms to the fact that
everything that arises ceases. So it's not quite what you're talking about, it's more just
when the mind becomes to accept and appreciate and understand the truth of impermanence.
And of course, that's not through seeing impermanence per se, it's not directly, it's through a
mindfulness, you see mindfulness is what lets us see those things, so actively trying to see
impermanence is never going to get you there. You actively try and see things as they are,
and that's enough, because as they are is impermanent, so our practice should never be
engaging in impermanence, it should be engaging in mindfulness, just trying to see things as they
are and free ourselves from all the clutter of experience, all of the reactions and reactivity.
Is the main goal of meditation to train our minds into observing everything as it happens,
such as something happening internally, regardless of any external circumstances?
It's not quite how I would put it, I mean the main goal of meditation is freedom from suffering,
it's happiness, it's peace. But on a more immediate level, the main goal is to see things as they are.
So it's not so much about observing things per se, it's about observing things in a specific
pure, simple way. So it's about observing, in fact just observing something, anything.
We don't have to pick and choose ultimately. But it's not so much about the thing, it's about
our quality of observation, to have a quality of observation that is pure, that's what the mantra
does for us, it reminds us, it creates the simplicity of experience, this is this, it is what it is.
Is it recommended to seek out unpleasant experiences for meditation in order to see them as they are?
No, seeking out is not the means by which to see things as they are. If you want to see things
as they are, you have to devote yourself to that. So seeking anything out is taking away from the
state of seeing things as they are.
How can the nonverbal awareness of mindfulness meditation help people develop a healthier philosophy
about themselves, others and life on a verbal level? I'm not sure what you mean. Why are you
focusing on nonverbal, I'm not sure. But mindfulness helps us develop a healthy philosophy about
ourselves and others because of our quality of interaction with things. When you refine your
perception of things to the extent that you're seeing them as they are, you become much more
in tune with reality. Someone who is in tune with reality has a much better time of things,
much, much more wise and beneficial interaction with others, as opposed to someone who doesn't
see things clearly, who has misperceptions about things and reacts to things in ways that only
cause more suffering. Of course, most of the time our interactions with others are plagued by
improper reactions based on our misunderstanding, based on our lack of clarity and strength and purity
in mind.
How do we note phenomena for which there is not yet a name or label?
Well, everything has a, there's nothing outside of the six senses, so ultimately it can
be seeing, hearing, smelling, tasting, feeling, or thinking.
I have struggled with back pain all my life, which I learned from a doctor that this can be due
to stress and emotions in my unconscious brain. Do you have advice on how to help with this?
So this is a key difference between, I guess, Buddhism and some, well,
a difference between our ordinary means of approaching problems and the Buddhist approach.
We're not concerned with what something is due to. It doesn't help you to know what something is
due to and the solution doesn't lie in what it, what something is caused by. The solution lies in
seeing the reality of things as they are and to see in a more general sense what leads to what.
So when you feel pain, Buddhist practices to try and see the pain just as it is,
because focusing on causes is always going to be,
as I was going to be plagued by the uncertainty of life, you can't predict
when and if you're going to be presented with that which you're trying to avoid or that
the cause of suffering. If you're always trying to prevent, for example, back injury or back pain,
then you're going to be susceptible to great stress and suffering when you can't prevent it.
And so rather than try and prevent the things that we don't like, we try and change our interaction
with them so that we free ourselves from the dislike.
Can we say one experience arises because another has ceased?
I wouldn't really say that. It's not really a place to go into that. Have we run out of
two or one questions? There are three more. How do I help someone that is suffering get into
meditation? Well, you could offer to encourage them to read our booklet on how to meditate,
if they want to do an at home course, they can do that. But really, the practice of helping others
come to meditation is limited success at best. I think there is a real
goodness in suggesting it, offering it as a suggestion, providing people with the connection to
it. If you talk about meditation and let someone know, hey, this option exists,
I think that's a great thing. But pushing people to meditate, trying to encourage them to meditate,
trying to find ways to entice them to meditate, it's always ever going to have only
limited success because meditation requires, as you can tell from what I've been saying probably,
it requires a certain amount of inclination or a conviction. It's not an easy thing.
Certainly meditation will help everyone, where everyone is going to seek out that
beneficence. It's a whole other story. It's unlikely most people will not.
A really the best way to encourage meditation and others is to practice yourself. When you
yourself are more mindful of others, we'll take your example, we'll be inclined to follow your
example more. You're much more likely to have influence over them by your own quality of practice.
While on the path of letting go, outside of noting inner resistance to thoughts or situations,
are there other things I need to do? Well, there are two things that you need to do before you start
practicing. Really, one is be ethical and the other is to have proper views. So study can help with
that, listening to talks and reading teachings, try and get a clarity of purpose, get some of the
background on the practice, and appreciation of the perspective to which we
have perspective with which we come to meditation. So our perspective on things is a bit different
trying to appreciate the experiential based nature of Buddhist practice. What does that look
trying to understand the experiential nature of things? But beyond that, there's not much you need
to do. Beyond that, we just cultivate clarity of mind every moment.
How do I deal with loneliness through meditation? I think we already had this question, didn't we?
Sometimes the questions are pretty similar, but the answers aren't.
If you think the answer is the same, we can go on.
Yeah, we're over time now, let's just go on. I tend to slouch when I relax during my
meditations. How important is posture when meditating? Or is it more about equality of mind?
It's more about equality of mind, posture doesn't play much part.
Try and note your posture, try and maintain a posture that's
maintainable, posture that's conducive to
to to per law to sustained practice.
But focus much more on your state of mind.
While I was meditating recently and minding the breath since nothing else was bothering me,
my mind went into almost devoid. Do you have advice on how to keep this from happening?
We don't try to keep things from happening, so it's not actually a problem. You just try and note
the feeling of being an avoid quiet, maybe it could even say something like void, but better to
focus what are the qualities of it? If it's quiet, try and quilt, try and note that.
Come, maybe you'll come.
Okay, we've come to the end of the top tier. Okay, well, thank you all. So I had to keep you on
longer and say it was a special for the new year. I'll try and keep my talks a little shorter,
maybe. But lots of good questions. It's never a bad thing. Hopefully we got through all the
ones that needed answers urgently. If you're interested in going further with this, please do
consider to read our booklet on how to meditate. Take up meditation practice.
And thank you all for coming. I wish for me we all
that's all wished together, not just I wish, but we wish together that we all have a happy new year,
that this new year would be one of increased well-being for all beings, both physically and mentally,
sad who, sad who. Thank you all for coming. Thank you, Chris, Olivia,
Bruno, Jim. Thank you about that.
