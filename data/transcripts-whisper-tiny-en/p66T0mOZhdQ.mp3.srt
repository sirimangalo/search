1
00:00:00,000 --> 00:00:11,000
I think it's the same person, says my point is you can't live without your desires.

2
00:00:11,000 --> 00:00:19,000
Can't do.

3
00:00:19,000 --> 00:00:22,000
I think you can.

4
00:00:22,000 --> 00:00:27,000
Well, the next question on there is there is someone's state.

5
00:00:27,000 --> 00:00:31,000
There is a difference between desire and need.

6
00:00:31,000 --> 00:00:35,000
For example, food is needed to live and so is water.

7
00:00:35,000 --> 00:00:38,000
So they are not desires.

8
00:00:38,000 --> 00:00:42,000
If you don't get carried away with them, don't.

9
00:00:42,000 --> 00:00:46,000
No, it's not really like that.

10
00:00:46,000 --> 00:00:51,000
An arahad eats not because they want to or need to eat.

11
00:00:51,000 --> 00:00:55,000
They eat because it's appropriate to eat at that time.

12
00:00:55,000 --> 00:01:00,000
They eat because somehow in their life

13
00:01:00,000 --> 00:01:04,000
that is the natural course of affairs.

14
00:01:04,000 --> 00:01:08,000
It's not even because of a need because an arahad can just start to death.

15
00:01:08,000 --> 00:01:12,000
It can happen that they just don't eat for whatever reason

16
00:01:12,000 --> 00:01:16,000
and pass away into Nirvana.

17
00:01:16,000 --> 00:01:20,000
It can be that they just don't do anything.

18
00:01:20,000 --> 00:01:26,000
So it can be that you could be in a certain,

19
00:01:26,000 --> 00:01:27,000
yes.

20
00:01:27,000 --> 00:01:30,000
Actually what you're saying is true in certain instances.

21
00:01:30,000 --> 00:01:34,000
What this guy is saying, it can be possible that you're in a situation

22
00:01:34,000 --> 00:01:38,000
where due to a lack of desire you don't live.

23
00:01:38,000 --> 00:01:41,000
Even people who are not enlightened can come to that.

24
00:01:41,000 --> 00:01:44,000
In light and being especially could come to the situation

25
00:01:44,000 --> 00:01:51,000
where not having desires leads them to pass away.

26
00:01:51,000 --> 00:01:55,000
But it doesn't follow what you're saying is that you can't live without your desires

27
00:01:55,000 --> 00:01:57,000
because you certainly can.

28
00:01:57,000 --> 00:02:00,000
You don't need desires to live, you don't need desires to eat,

29
00:02:00,000 --> 00:02:03,000
you don't need desires to act, teach, or so.

30
00:02:03,000 --> 00:02:05,000
The Buddha was a very good example.

31
00:02:05,000 --> 00:02:13,000
The Buddha had no desires and yet acted and taught and helped countless beings become enlightened.

32
00:02:13,000 --> 00:02:16,000
It's simply because it's what he did.

33
00:02:16,000 --> 00:02:17,000
It's where he was going.

34
00:02:17,000 --> 00:02:22,000
He was set in that direction and he was impaled in that direction

35
00:02:22,000 --> 00:02:24,000
by from every corner.

36
00:02:24,000 --> 00:02:29,000
People coming to ask him to teach, people coming to ordain with him

37
00:02:29,000 --> 00:02:31,000
and live with him and so on.

38
00:02:31,000 --> 00:02:34,000
People coming to criticize him and so on.

39
00:02:34,000 --> 00:02:41,000
So he responded naturally to all of this without desires.

40
00:02:41,000 --> 00:02:44,000
The Buddha needs.

