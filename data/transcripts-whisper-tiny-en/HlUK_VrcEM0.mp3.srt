1
00:00:00,000 --> 00:00:06,360
Hello everyone, just an update here to let you know that if you haven't seen already

2
00:00:06,360 --> 00:00:16,480
I've shut down the Ask a Monk series for the winter, meaning that you can still look

3
00:00:16,480 --> 00:00:23,360
at the questions and answers but you can't ask any more questions because there's still

4
00:00:23,360 --> 00:00:31,000
quite a few that I haven't answered and I'm hoping to get them all finished. Most of them

5
00:00:31,000 --> 00:00:39,200
finished by the time I go to Thailand with the exception of course of the question about

6
00:00:39,200 --> 00:00:49,000
my story of my monk which honestly I don't think is all that exciting anyway. It's going

7
00:00:49,000 --> 00:00:56,000
to be a little bit difficult to tell because of course it involves a lot of different people

8
00:00:56,000 --> 00:01:04,160
who still might have some interest in what I do and so it's a balance between getting

9
00:01:04,160 --> 00:01:12,880
the story told and saying too much. You have to involve other people in the story so

10
00:01:12,880 --> 00:01:21,960
I have to figure out how to tell it more in a dumbic sort of way that teaches lessons

11
00:01:21,960 --> 00:01:30,760
rather than just tell stories. So the rest of the questions I'll be answering I would

12
00:01:30,760 --> 00:01:39,680
hope and then I'll be gone. I'm off to Thailand on the 25th of October and hopefully

13
00:01:39,680 --> 00:01:51,840
I'll be back around the 21st of December heading off to Sri Lanka for 9 days or 10 days

14
00:01:51,840 --> 00:02:01,240
I can't remember exactly 9 days I think. To see the country, to attend a Buddhist conference

15
00:02:01,240 --> 00:02:06,560
they're having this yearly conference of some world Buddhist fellowship or fellowship

16
00:02:06,560 --> 00:02:13,360
of world Buddhists or something but it's a chance also to see the country and to anyone

17
00:02:13,360 --> 00:02:20,320
else maybe one day I'll wind up living there. But I'm still hoping that after that I'll

18
00:02:20,320 --> 00:02:26,640
be back here in America to try to make it work here hopefully we'll be moving closer to

19
00:02:26,640 --> 00:02:33,440
the goal of opening a forest monastery in a little bit of a disappointment there when it

20
00:02:33,440 --> 00:02:40,200
turns out that there's not many people among my supporters interested in in the remote forest

21
00:02:40,200 --> 00:02:47,800
monastery. So hopefully that does happen otherwise I'll be living here in the city which

22
00:02:47,800 --> 00:02:58,560
is fine too as long as I'm in a place that I can go for arms round. So what other updates

23
00:02:58,560 --> 00:03:09,720
I was going to yeah the other one is I've still only got seven replies to my video my

24
00:03:09,720 --> 00:03:16,080
request for videos which is a little bit surprising I thought there'd be more of you out there

25
00:03:16,080 --> 00:03:20,720
that sounds like there's a few people who still intend to make videos but to the rest

26
00:03:20,720 --> 00:03:26,880
of you I mean come on there's hundreds of people who watch these videos every day thousand

27
00:03:26,880 --> 00:03:32,040
over a thousand people watch my videos every day which is you know I don't expect most of

28
00:03:32,040 --> 00:03:35,840
them to be interesting but those of you who've been living comments about how great this

29
00:03:35,840 --> 00:03:41,640
is come on show me how great it is show me show us all I mean the thing is not just to show

30
00:03:41,640 --> 00:03:46,960
me although that's appreciated it's nice to see that people are actually practicing the

31
00:03:46,960 --> 00:03:54,520
point is to show the world to and not to show yourself but to show meditation to show that

32
00:03:54,520 --> 00:04:01,560
people are actually practicing to show people that this technique this tool is something

33
00:04:01,560 --> 00:04:10,600
that's of use and people are putting it to use it's proof it's a proof of content so yeah

34
00:04:10,600 --> 00:04:20,080
and the video is still on my channel it's going to be my channel video or is it no maybe I'll

35
00:04:20,080 --> 00:04:27,240
make this on my channel video in which case I'll put a link right here to the other video so

36
00:04:27,240 --> 00:04:32,560
you can go and leave your video responses to that video if you leave your video responses to

37
00:04:32,560 --> 00:04:36,960
this one fine but I'd rather you leave them to the other one it explains what I'm looking

38
00:04:36,960 --> 00:04:45,400
for and also the way they're all in one place so okay thanks everyone for your support your

39
00:04:45,400 --> 00:04:50,440
comments feedback and so on oh the other thing is I'm not obviously going to be checking my

40
00:04:50,440 --> 00:04:58,880
email and comments while I'm away but I'd also ask that regardless I don't leave I don't leave

41
00:04:58,880 --> 00:05:04,680
questions in the comments section I'm I think I'm not going to be able to answer them anymore

42
00:05:04,680 --> 00:05:09,600
sometimes I do go in there and try to answer sometimes it's just random which ones I happen to

43
00:05:09,600 --> 00:05:14,720
answer but it's really too much at this point if you really have a question ask it on ask a

44
00:05:14,720 --> 00:05:19,320
monk when I get back or send me a private message when I get back if you send it in the

45
00:05:19,320 --> 00:05:24,760
meantime I may just skim through it when I get back because I'm sure I'll have lots and lots

46
00:05:24,760 --> 00:05:31,920
of emails and stuff to answer when I get back but when I return you know send me a private

47
00:05:31,920 --> 00:05:39,920
message send me an email while I'm away you can go crazy watch all the old videos there's a

48
00:05:39,920 --> 00:05:46,240
whole bunch of ask a monk videos up there's videos on meditation there's my second life talks

49
00:05:46,240 --> 00:05:52,920
there's a whole bunch of old talks that I've given a while back which could be outdated you know

50
00:05:52,920 --> 00:05:58,440
a lot of them weren't during the time I was still relearning English because I'm coming back

51
00:05:58,440 --> 00:06:06,240
from Asia where we don't speak much English and the English reduce because it's quite

52
00:06:06,240 --> 00:06:10,520
informal because we not only have to deal with Thai people but people from all over the world

53
00:06:10,520 --> 00:06:18,240
who speak poor to little to no English so but there are old talks in there on my web blog

54
00:06:18,240 --> 00:06:23,320
if you go to my web blog you'll see some an audio list you can listen to those otherwise

55
00:06:23,320 --> 00:06:28,760
there's lots of stuff on the web that has nothing to do with me and I encourage you to check

56
00:06:28,760 --> 00:06:37,280
out and read and listen to and watch whenever whenever you're up there the other thing is if you

57
00:06:37,280 --> 00:06:41,720
do watch my videos and if you do like them hit the like button because what I figured out is

58
00:06:41,720 --> 00:06:47,640
that affects the popularity and then YouTube puts them at the top of the list and more people

59
00:06:47,640 --> 00:06:52,360
can watch them so good way if you want to support this work make sure to hit the like button make

60
00:06:52,360 --> 00:07:01,800
sure to subscribe these kind of things you know increase the the exposure exponentially as I'm

61
00:07:01,800 --> 00:07:07,240
told okay so thanks a lot and keep practicing I'm going to be away in the forest for two months

62
00:07:07,240 --> 00:07:12,840
I expect some serious practice from everyone who's been watching these videos okay and I'm using

63
00:07:12,840 --> 00:07:18,920
a new mic here hope the sound is okay so this is the mic I'm going to be taking to Thailand

64
00:07:18,920 --> 00:07:26,520
because it's on top of the camera and means I can take videos of my teacher and so on this is

65
00:07:26,520 --> 00:07:32,760
exciting if you have when I come back I'll be able to see videos of Thailand and Sri Lanka

66
00:07:32,760 --> 00:07:37,800
and I teacher in the forest and so on during the time I'm not meditating it can be sure I'll be

67
00:07:37,800 --> 00:07:53,720
taking some videos okay thanks that's all have a good night

