Good evening everyone, broadcasting live, February 24th, 2016.
Today, we have a quote from the Sanyatani Kaya, it's a pretty awesome quote, one of those
quoteables, so in Pali we have Saya, Tapi, Bhikhave, Kumbu, Anand, Haru, Suk, Poateo,
Hote, Sadharo, Du Poateo, Hote, Bhikhave, all because you have seen the Baya in Samsara, the
danger in Samsara, Saya, Tapi, Kumbu, just as a pot, Anand, Haru, Suk, Poateo,
Hote, Apat, Anand, Haru, without supports, Suk, Poateo, Hote is easily overturned,
Sadharo, Du Poateo, with supports, it is hard to turn off.
Bhikhave, Thang, Anand, Haru, Suk, Poateo, Hote, Sadharo, Du Poateo, Hote, Ava, Mavakho,
even so because, Thang, Anand, Haru, mind, without supports, Suk, Poateo, is easily
overturned, or upset, Sadharo, Du Poateo, Hote, with supports, hard to upset.
Go to Bhikhavejita, Sadharo, and what, or because, is a support for the mind, Ayameva,
Ariyova, Thang, Du Poateo, Mango, this very noble eightfold path, so the noble eightfold
path is a support for the mind.
The idea that the Buddha is getting out here is that the eightfold number path, the eight
path factors, work together to support the mind, and you can't have a pastor enlightenment
without all eight factors.
If one factor is wrong, if you have a wrong view or wrong thought, or wrong action, wrong
speech, wrong action, wrong livelihood, wrong effort, wrong mindfulness, wrong concentration,
took Poateo, easily overturned, mind is easily shaken.
This idea of being unshaken, this is the essence of the Buddhist path, there is this
idea of being invincible.
I mean, they are given, it's they are given given against staying in some sorrow, against
seeking out happiness in impermanent things, because that sort of thing is unstable.
It's sort of things where renders you unstable, vulnerable, invincible, I don't think
it's such a word, suppose the invincible probably comes from the Latin, Winko, Winkari,
which is to conquer, Winko, mean would mean to conquer, in Winko, or invincible, just
guessing, not subject to being conquered.
So the idea behind Buddhism is to find a unassailable, indisputable, you know, the kind
of state that you can't, that is free from danger, that has no opening for attack.
So these, and if this is, these are the, these are what the supports of the 8th full
noble path give, the idea, the claim is through the practice of the 8th full noble path,
for keeping to the 8th full noble path, if one perfects all aid of these factors, right
view, right thought, right speech, right action, right livelihood, right effort, right mindfulness,
right concentration, one that's invincible, this, this supports the mind in always, that
there's nothing that could break through the defenses, nothing could tip you over like
a pot, but the 8th full noble path you might ask, well can I just use some of them, some
of the supports, or can I work on one of the supports at once, it doesn't really work
that way, and the 8th full noble path is a lot more theoretical than it is practical,
in the sense that it's important to know about these things, but in terms of practice,
you don't just go about practicing one and then practicing the other or so on, perfecting
one and then okay, that one's perfect, let's go perfect the other, it's more that when
you practice, your practice has to incorporate all 8 of these, so if you're practicing
insight meditation and you're lacking one of those, then you can, you can know that your
practice is lacking, it's not that you actually have to practice all 8, but in some ways
one way of looking at it is all you need is right view, and once you have right view,
the other seven follow along, another way of looking at it is you start with mindfulness,
and when you have mindfulness, then effort and concentration build up, and based on that,
all the rest of the path factors, I mean practically speaking, it's not so simple as practicing
the full noble path, you have to understand each of the 8 and where they fit in, because
it's not like you can say, oh my livelihood is pure, so there I purify livelihood, it's
not really what it means, it's just as part of those things that I have to be abstained
from that are vulnerabilities, you need that support, all 8 are necessary, and they come
through proper practice, it's actually not something you have to worry too much about,
they go on my missing one, it's good to see if you are missing one, but if you practice
mindfulness, if you practice that day, if you cultivate the five faculties and balance them
and then strengthen them, the 8 full noble paths will come by itself, the 8 full noble
path is really at the moment when you see clearly, when you just see perfectly for the
first time, and it's one moment, the noble path, one moment cuts the defilements and opens
the door to nibana, everything after that is fruit, it's the fruition, but it's only
one moment is the path, it's when everything comes together perfectly and that one moment,
they are perfectly supported and the mind is able to break free, so anyway, we had a couple
visitors this evening, a woman and her father came by to practice meditation, her father,
I probably made a mistake by teaching the walking meditation first, because I demonstrated
walking meditation, her father said, oh no, he's got some kind of Scottish accent or
some things, that's not really what we were expecting now, he said, but she said, well
it's exactly what I was expecting, he wasn't having it, so I said okay you can just sit
there and you don't have to do it, probably should have taught the sitting meditation
first, would have been more palatable I think, it was kind of cute and he was hard of
hearing as well as a bit, but yeah, so people are coming out, some engineers or something,
some group, some class, they want to hold a meditation class, yeah, lots of interesting
things, of course today was Wednesday, so I taught meditation in the gym, three new people
and three or four old people, some people are coming back every week, which is great, we
had, we must have had eight people, nine people maybe there today, and on Mondays people
are coming, Friday's not so much, but then Friday's I do all day, this Friday I'll be
doing all day, five minute meditation lessons, as usual, so if you have questions, come
on, hang out, thank you Pandit, I have a question, alright, come for me, so we're talking
about the reflections, I was just thinking about the ten perfections, the Buddha mentioned
and how they relate to the moment of time, or are they different from these eight perfections,
which are not really cool, was the word perfect is wrong, he shouldn't have used the word
perfect, because it's confusing, it's not what the word Samma means, Samma doesn't mean
perfect, it means right, I'm not even sure, I don't know the etymology, but it's always
translated as right, perfect, I don't think it has the sense of perfect, no it means
right, there's no question, this monk is the only, he's a really good monk and as far
as I know, he's a good guy, but he's got some entranslation to that, so he's translating
Samma as perfect, the ten perfections that the word is Parami or Paramita, and it comes
from the word Parama, Parama means having no, nothing beyond, which means ultimate,
so highest, so completions may be perfections, it works really well for a translation
of Paramita, means that which is the highest, perfection really, thank you Paramita,
hi Larry, good evening, good to see you, good to see you, thanks, so I had an exam this
afternoon, and the lotus sutra had a chance to take it apart and criticize it, I was
not that critical, you can't be, I mean you've got to be fair, we're scholars, so we can't
be palimasists, but it's kind of disgusting, the lotus sutra kind of disgusting, it
just so obviously, I don't know, it's hard to see them putting words in the buddha's mouth,
I mean there's criticism that, well that that polydepitika, we don't know that the buddha
said all of it, and I'll agree that some of the commentaries may have taken liberties, but
it's not so blatant, so I can't say one way or the other, this is pretty blatant, I mean
they've made up all sorts of new doctrines and suddenly the buddha's this, I don't know,
some kind of angel up and have it, everybody in the class is confused and I'm like, yeah,
I understand your confusion, it's like this is Buddhism, no, this is for many of them,
this is their first introduction of Buddhism, which is a shame because it's all messed
up, but he's doing a good job of showing how it got messed up over the course of its movement
to East Asia, I mean I think he actually agrees with Mahayana principles, but Mahayana
grew up in India, and Mahayana in India is different from I think even the Lotus Sutra,
Lotus Sutra is Indian, but never really gained much ground in India, gained a lot more
ground in East Asia, but Buddhism in East Asia is an interesting phenomenon, they developed
this idea that the Buddha has three bodies, so he's actually up in heaven, and in Nibbana
end on earth, and he comes to earth as like it's just an illusion, his birth and his 45
years of teaching, and the 45 years of teaching is lies, that's what Uplayakusa is,
he lies in order to get us to the point where we can hear the truth, he tricks us into
becoming our Hans, because that's the best we can do, because we're too deluded to understand
that we all have to become Buddhas, that's what, I mean I'm not making this up and this
isn't actually what the Lotus Sutra says, so I'm probably making a lot of Buddhas to
angry by saying this on the internet, but you know, what has that ever stopped? I don't know,
I think the heart Sutra is better actually, isn't that the one where Rupan Shulnya
dang isn't that the heart Sutra? I really can't tell. I think the heart Sutra
doesn't, but the heart Sutra I don't like it because it's kind of suffistic, it's
softism, you know, softism, like just sounding intelligent, like it says, form is emptiness,
emptiness is form, what the heck does that mean? And of course they say, ah, you see, you're
not Mayana, so you don't get to understand, and I'm like, go away, I have no use for
yourself is them. It's just sounding, you sure anyone can sound, I should stop before I get
in trouble, this is bad. I'm sad.
I had an experience like that listening to talk. I don't know, if you practice this, and
then you go in here, people talk about emptiness, and they don't, all sorts of crazy
ness. Anyway, there shouldn't be too critical of other people's beliefs. I promise that
I, I always promise that I won't do this. I won't go on and on about other people's
practices and beliefs. Then I do, don't I, I've criticized Christianity, I think I've
gotten into Islam, Judaism, I just have a torn into Judaism yet.
They're all garbage, they're all wrong, just not the kind of thing you should say on
the internet. I mean, I like reading criticism and of course criticism of our own tradition
as well.
Yeah, we should criticize our own traditions. I'll never be open to it.
So this is, this is something I've been looking at, in case anyone was interested in
criticism of the traditions. I just, I found that interesting, there's something in
this picture.
You know, there's, you know something that I didn't hear at all, but I caught part of
a speech, and I want you all to know about this guy because I'm not supposed to be political,
it's another thing we're not supposed to be, but there's this guy running for president
of the United States. And he said something I think it was last night or the night before.
That was pretty awesome. I mean, maybe not terra lotta Buddhist awesome, but pretty
awesome in the last. Bernie Sanders, did anybody know this guy, Bernie Sanders?
He's very popular with a lot of the Democrats, very, maybe more so than Hillary Clinton.
I think they are the two that are by most closely for the Democratic nomination.
Right. He said, they asked him what his religion was, I think. And he says, my religion
is that we're all in this together. When you hurt, when your child hurts, I hurt.
That is something like that. Your child is my child. My child is your child. He totally
breaking down barriers about, you know, it's basically, I guess it's a lot like the song
I imagined by John Lennon. The idea that we're just people, I mean, I guess you could
argue could label him as a humanist. He believes in people. And he believes that when we
stand together, when we come together, when we work together, good things happen. When
we cooperate instead of, you know, so he's a socialist, which means he believes in cooperation.
I mean, that's absolutely the big problem with capitalism is it's about competition. And
it's based on the virtue of competing with each other as opposed to cooperating, as
opposed to working together for the greater good. And so he's all about breaking down barriers,
breaking down walls, divisions, bringing people together to, for goodness. I mean, if you
listen to him talk, he's honest. If you don't like what he says, you can't argue that
this guy is a liar or a shister, sorry, that's a bad word, but he has Jewish, I think,
you know, shisters not a good word, I apologize. But, you know, that kind of, he's honest
and sincere. You ask, does he want to help people or does he just want to become president?
You know, this is a guy who is in politics because he wants to help people. I mean, how
awesome is this? How many, how long have we wished or hoped for such a person who wants to lead
the people out of the goodness of his heart? I mean, this doesn't exist very often. I could
tell you that. It doesn't exist. Do you know who are you? Sorry, I just joined, that's how I said.
How do you know who Bernie Sanders is? Yeah, I'm from the UK and in my country. It's very
competitive based. So it's very capitalist in this country. They want to get rid of free health care
which is not great. We have free health care here in Canada. Yeah, they want to get rid of it
because it's... What's the thing? I mean, what's Bernie Sanders platform? He wants to, you know,
I don't know. I don't want to get into what he's giving people and so on. But to care about the
fact that there are millions of people who can't get adequate health care. I mean, there's people
who are... I get... These people are my students. I have one student who was telling me his mother
got sick and he was basically having to choose between food and medicine and rent. I mean,
living on a budget that you can't live off of. And the things... He talks about these things.
Who else is talking about these things and making them platform? There are people starving. There
are people... There are children living in poverty. In the richest country in the world, I guess,
United States is the richest country in the world. I don't know. And there's an obscene number
of people living in poverty who don't have health care who can't get an education. He wants
education to be free, which is really a no-brainer to pull a bit of a pun. But I mean,
education... Going back to school, I mean, I've been talking to students here and saying,
there are students here in Canada where it's heavily subsidized who still can't get an
education, who still have to work two jobs just to be able to take classes. And as a result,
aren't learning any... Well, aren't learning what they should be learning because they're too
tired because they're too stressed. This is... I mean, maybe this is all worldly talk, but if we
talk about helping people and people's spiritual well-being, goodness, remember Buddhism, all
about goodness. And there's a lot of badness happening. There's a lot of greed, corruption,
and just too much politicizing people who want to be who are politicians just for their own
benefit, you know. They get their bought by the rich elite and they make lots of money giving
speeches and holding these dinners and whatever. I mean, guaranteeing that the rich stay rich
and the poor stay poor. I mean, you can't argue with the facts. The fact is there's lots and lots
of people who are very, very poor and that hasn't changed and isn't changing. It's getting
worse. Here comes Robin to tell me I'm renting. Robin, you can talk for yourself. I won't say
anything you don't want me to say on the internet. No, I'm here to say, go Bernie. You're in
with Bernie. Absolutely. I actually changed my party affiliation today. I was going to say she's a
Republican and we were talking about Bernie. I changed so that I can vote for Bernie. He's
awesome. Well, I think if we're any, if you and I are any indication, he's got the Buddhist vote.
I think so. Not that I know. He comes about as close to being what used to be referred to as a
true statesman. So many politicians are just hey boys for corporate power and wealthy people. It's
amazing that he's been able to stay in office so long. You're in office by virtue of the votes
of a constituency. So he must have a very enlightened constituency. He has like 83% of approval
rating in his state or something. 80% more. It's just like, hey, yeah. In a poll that came out right
after the New Hampshire primary where he just won in a landslide, there was a poll where the question
was, are the candidates trustworthy? Well, the right button. It got like 5% and he had 95%. So no
matter what, you know, you think of his policies. People do believe that he's dead.
I mean, that's to me that's a very, very important point. As before we talk about what he's
promising, you know, what's his character? You know, because politicians promise everything.
All the politicians promise what they think will get them elected. I bet he's guilty of that a
little bit as well. He knows, well, I have to say this or I won't get elected even though
maybe he doesn't quite believe it. But there's not that that isn't a huge question in people's
minds because they know he's sincere. And they know that the majority of what he at least the
majority of not all of what he stands for, he really stands for. And that, I mean, that's important.
It's not tricking it. It's not deceptive. There seems to be very much in favor of placing
constraints on corporations and throttling corporations, which is so very much needed in this country
and corporate power and the very wealthy powerful people that are running corporations is just
horrendous. And he seems to be in favor of trying to deal with that, which would be really
difficult, you know, because presidents got to work with the Congress. And we've of course seen
the Obama's difficulties working with the Congress that we have had the last few years.
The other neat thing about him is he does work well with people. I mean, when you're that
honest, he gets things done. He apparently has a record people saying, well, he's how could he ever
work. He's got a 40 year record, a long record of working with people, actually getting strong,
which, I mean, that's it. You said it. He's a real statesman. The Republicans appreciate that.
Well, some of them, sure, but he's worked with, anyway. I just wanted to say, here's someone
who is worthy of support. We need someone out over here, every new kid. Didn't you guys
have liked some socialist as well? Well, we're like very capitalist over here, not very socialist. So, we've got
corporate businesses and street guidelines and such. It's not the kind of society that I enjoy
living in. The UK has a history of impasse competition. In Denmark, he actually, as soon as you leave
at what's it called, like, primary school. I mean, as soon as you're not really a kid anymore,
you're going to start getting paid to go to school, paid for business school and university.
Just enough to, if you have an apartment, a small apartment. I mean, it's really a no brainer.
What's the result of that? As the result, people who are unproductive and know you have people
who are intelligent. The problem is rich people are not rich, but rich people are not all bad,
but greedy, corporate, terrible people. One people to be stupid. It's really, this is,
it doesn't like also like selfism as well. It's not like selfism as well, where some people
hold to the knowledge and want to dumb down other people. That's not, I think, what selfism
means. Selfism is when you just try to sound intelligent. That's not something else entirely.
It's elitism or something, or I don't know. There's probably a word for it.
I think there's great, one of the great things about America is that rich, a lot of rich people do
do good things. Those people who have gained power, Bill Gates, for example. Apparently, there's
conspiracy surrounding him, so maybe he's a terrible person as well. I don't know. The ideal of a
person who gets very rich from something like Microsoft Windows or Microsoft, which is actually a
terrible operating system. Then to use that money to say wipe out malaria or polio or polio,
which is a really terrible terrible disease. So whether the facts, I don't know what the facts are
and criticisms of him, particularly inside, there is a sense of rich people being charitable.
I don't know. I mean, maybe I'm being charitable by saying that, I don't know, but
no, definitely. Even like athletes in celebrities in the United States, they're very, very
helped me open with their foundations. Many people have their own foundations. They do good works.
It's maybe it's a little bit for publicity, but someone benefits from it anyway.
And then you have to, I guess you have to ask whether it's systemic and whether it's as
whether it works as well, because I don't know. Some people argue that the government
can't. The government is full of waste and so on. Anyway, I don't want to get into all
the politics of it, just the goodness of it. And here, I wanted to shout, give a shout out
to Bernie Sanders for what seems to be awesome as far as I can see. He's hard as in the right
place and he walks the walk. He actually does do good things for people. He has done good things
for people. He's helped people. Maybe he's a Bodhisattva. Could be. Couldn't very well be.
I mean, I guess I please endorse me yesterday. Sorry. He got Spike Lee's endorsed me yesterday.
Who has Spike Lee? I've heard this name before. He's a filmmaker. Okay, right. I think I knew that.
I don't know any of his films.
Well, he's big on the rap scene as well. Just to add a little bit. Okay.
If I could endorse a political person, he'd have my endorsement as well.
As it is, he has my respect. How about spiritual endorsement? Is this such a thing?
He has my spiritual endorsement, absolutely. I endorse him. I should send him one of these books.
I'll get Aaron, I'll just send him a guide to how to meditate. Yeah, do that.
Maybe I'll read a letter to him as well.
Not a lot of people do that nowadays.
Sorry. Not a lot of people write hand written letters nowadays. It's an art form now.
Yeah. Well, not when I write chicken and scratch. You could also send in your lessons in practical
Buddhism. Yeah, I don't imagine he's got a lot of time or interest in reading. This one's kind
of practical, so maybe it'll be maybe his wife would read a interesting thing.
He's less busy. He's just so busy it seems, which is another awesome thing. I mean, he's tireless.
He really, I've seen him, you know, he's old.
I'm going very strong.
He pray, he's healthy.
He what? He probably eats healthy.
Right. Well, maybe maybe he would maybe he would like one of these.
I'll send him a few, maybe. Sure.
Okay, I'm going to say good night. Thank you all. Good. Good to come up with something
interesting to talk about every night. Good night. Good night. Good night. Good night. Good night.
