1
00:00:00,000 --> 00:00:02,000
I'm ready to go.

2
00:00:12,000 --> 00:00:14,000
Good evening, everyone.

3
00:00:14,000 --> 00:00:16,000
Oops.

4
00:00:16,000 --> 00:00:18,000
Good evening, everyone.

5
00:00:18,000 --> 00:00:22,000
Welcome to our live broadcast.

6
00:00:22,000 --> 00:00:28,000
Tonight, we're looking at a good tourniquet.

7
00:00:28,000 --> 00:00:30,000
I am a book of five,

8
00:00:30,000 --> 00:00:34,000
suit of 154.

9
00:00:34,000 --> 00:00:40,000
And the pectomus is a dhamma samosa.

10
00:00:40,000 --> 00:00:47,000
Samosa.

11
00:00:53,000 --> 00:00:59,000
It's funny, isn't it samosa an Indian food?

12
00:00:59,000 --> 00:01:01,000
I don't suppose that's where it comes from,

13
00:01:01,000 --> 00:01:05,000
because samosa here means it comes from samoha,

14
00:01:05,000 --> 00:01:12,000
versus probably a corruption moha,

15
00:01:12,000 --> 00:01:19,000
or related to the moha mua.

16
00:01:19,000 --> 00:01:29,000
So it means the

17
00:01:29,000 --> 00:01:36,000
corruption, perhaps, or the loss,

18
00:01:36,000 --> 00:01:47,000
the clouding of the good dhamma.

19
00:01:47,000 --> 00:01:52,000
Maybe it's the loss of the good dhamma, the lapse.

20
00:01:52,000 --> 00:02:15,000
And then samosa, anantra dhana, anantra dhana means the disappearance.

21
00:02:15,000 --> 00:02:20,000
So we're concerned about this.

22
00:02:20,000 --> 00:02:25,000
We're concerned about what causes the dhamma to become clouded

23
00:02:25,000 --> 00:02:32,000
and lost, to fade away, to not shine bright,

24
00:02:32,000 --> 00:02:40,000
to not appear in all its splendor in the world.

25
00:02:40,000 --> 00:02:45,000
We should be concerned about this for our own well-being,

26
00:02:45,000 --> 00:02:50,000
keeping the dhamma clear in our own minds, in our own lives.

27
00:02:50,000 --> 00:02:54,000
We should also be concerned, in general,

28
00:02:54,000 --> 00:02:57,000
with the dhamma remain in the world.

29
00:02:57,000 --> 00:03:00,000
The dhamma being that which is considered good,

30
00:03:00,000 --> 00:03:06,000
that which keeps people from falling into corrupt evil ways

31
00:03:06,000 --> 00:03:13,000
and hurting and killing each other and manipulating each other.

32
00:03:13,000 --> 00:03:37,000
And toward toward.

