1
00:00:00,000 --> 00:00:21,000
Okay, the audio should be light, if you go to the link, you should be able to hear me.

2
00:00:21,000 --> 00:00:26,000
I'm going to hear the birds as well, because I'm typing in.

3
00:00:26,000 --> 00:00:31,000
I'm typing in a second life sound as well.

4
00:00:31,000 --> 00:00:44,000
I might be in a place actually if you're hearing me in a scene as well.

5
00:00:44,000 --> 00:00:56,000
That also records, so it will be on our website later if I want to go back and listen to it.

6
00:00:56,000 --> 00:00:59,000
And that's at the meditation.

7
00:00:59,000 --> 00:01:11,000
Okay, which I should probably move over into the main audio archive at the end.

8
00:01:11,000 --> 00:01:26,000
Okay, just getting really disturbed.

9
00:01:26,000 --> 00:01:46,000
We should wait for four or five minutes.

10
00:01:46,000 --> 00:01:53,000
Maybe if it's in a few minutes asking everyone about themselves.

11
00:01:53,000 --> 00:02:13,000
I know that's going to be chaotic if I ask you to tell me about yourselves really, but maybe people could just in a word or two say what sort of religious traditions, spiritual tradition, Buddhist school, meditation practices.

12
00:02:13,000 --> 00:02:25,000
What thing you sort of follow away from coming from, just a word or two or three or five.

13
00:02:25,000 --> 00:02:36,000
I don't know it helps, and I can know my audience.

14
00:02:36,000 --> 00:03:00,000
It also helps kill a few minutes here while we wait.

15
00:03:00,000 --> 00:03:07,000
See, I'm interested in hearing more about Sikh religion.

16
00:03:07,000 --> 00:03:14,000
I taught a couple of Sikh people who had to meditate.

17
00:03:14,000 --> 00:03:16,000
They even had their mayas.

18
00:03:16,000 --> 00:03:18,000
Their Shakur official daggers.

19
00:03:18,000 --> 00:03:19,000
I guess they all have them.

20
00:03:19,000 --> 00:03:24,000
You've never seen them, but you've got a flash of one of their daggers.

21
00:03:24,000 --> 00:03:31,000
A ceremony of things.

22
00:03:31,000 --> 00:03:38,000
Okay, thank you.

23
00:03:38,000 --> 00:03:47,000
Here we go.

24
00:03:47,000 --> 00:03:57,000
Eight years have we met?

25
00:03:57,000 --> 00:04:16,000
Is this just all over the Internet?

26
00:04:16,000 --> 00:04:23,000
Okay, I think we're ready to start.

27
00:04:23,000 --> 00:04:30,000
What are you doing left?

28
00:04:30,000 --> 00:04:39,000
I have to do this to start recording.

29
00:04:39,000 --> 00:04:46,000
I'm going to be recording.

30
00:04:46,000 --> 00:04:53,000
I don't do any asking about recording from last week, but I haven't even looked at it.

31
00:04:53,000 --> 00:04:58,000
It was split up because I froze.

32
00:04:58,000 --> 00:05:05,000
I haven't even looked at it maybe, but it wasn't much of a talk.

33
00:05:05,000 --> 00:05:12,000
Okay, so we're going to start.

34
00:05:12,000 --> 00:05:19,000
So welcome everyone.

35
00:05:19,000 --> 00:05:22,000
Thanks for coming out.

36
00:05:22,000 --> 00:05:31,000
Let's get to see such a groove.

37
00:05:31,000 --> 00:05:36,000
It's awesome that this place is still running.

38
00:05:36,000 --> 00:05:44,000
I was surprised, pleasantly surprised, to see that it's still here, but to see that you

39
00:05:44,000 --> 00:05:53,000
really manage to keep this community going, really get it going.

40
00:05:53,000 --> 00:06:00,000
How well organized it is.

41
00:06:00,000 --> 00:06:04,000
Remember when I used to give talks and the first time they come in, they would get a few

42
00:06:04,000 --> 00:06:08,000
few people because no one heard about it, but now you're all organized when people are

43
00:06:08,000 --> 00:06:11,000
already ready to come out of the space.

44
00:06:11,000 --> 00:06:15,000
That shows me that.

45
00:06:15,000 --> 00:06:19,000
I don't have to build up an audience because you've already got a community.

46
00:06:19,000 --> 00:06:21,000
So I appreciate that.

47
00:06:21,000 --> 00:06:31,000
Well, it's invited me here and that your community is coming out to listen.

48
00:06:31,000 --> 00:06:37,000
And so that's sort of what I'd like to talk about today.

49
00:06:37,000 --> 00:06:47,000
And you see when the Buddha, there's a very famous discourse of the Buddha in my tradition.

50
00:06:47,000 --> 00:06:56,000
It talks about when the Buddha was close to passing away.

51
00:06:56,000 --> 00:07:02,000
And it's interesting how the sutas frame, because the sutas are, while they were,

52
00:07:02,000 --> 00:07:05,000
they seemed to have been put together after the Buddha passed away.

53
00:07:05,000 --> 00:07:11,000
They were recorded much later in books, but I think they were recited after they passed away.

54
00:07:11,000 --> 00:07:18,000
And so there's a lot of commentary in terms of the Buddha was staying here, the Buddha was staying there.

55
00:07:18,000 --> 00:07:24,000
So someone else has written that or spoken that in the beginning.

56
00:07:24,000 --> 00:07:28,000
And then the Buddha said this, but there's a lot of background to it.

57
00:07:28,000 --> 00:07:33,000
And then in how they put the discourses together, like, what did they start with?

58
00:07:33,000 --> 00:07:36,000
And so this discourse called the Mahapar in Ivanasutas.

59
00:07:36,000 --> 00:07:46,000
It's a very long discourse. It talks about the last period of the Buddha's life, the last months.

60
00:07:46,000 --> 00:08:00,000
And some of it is really like those thoughts of places where he said, it says, he taught here, he taught there, he taught, he taught.

61
00:08:00,000 --> 00:08:08,000
Orderly Dhamma, he taught the Dhamma in sequence here, and then there, and then he taught the form of the truth.

62
00:08:08,000 --> 00:08:13,000
So it abbreviates a lot just by describing what the Buddha did, where he went.

63
00:08:13,000 --> 00:08:22,000
But curious, and I think not without importance, is how the discourse starts.

64
00:08:22,000 --> 00:08:50,000
Now picture, the topic is going to be about the Buddha's passing away, and it starts with a visit from a minister, the famous king.

65
00:08:50,000 --> 00:09:00,000
So the story goes that King Ajata Saptu was a patricite, he killed his father.

66
00:09:00,000 --> 00:09:11,000
And so he wasn't the nicest of people, not only his father, not only was he the killer's father, but his father.

67
00:09:11,000 --> 00:09:20,000
His father was a very devout Buddhist, and he could say, someone enlightened being.

68
00:09:20,000 --> 00:09:25,000
And his son killed him in his long story, but that's not our story today.

69
00:09:25,000 --> 00:09:33,000
The story goes that he wanted to attack the Wajians, the people of Waji.

70
00:09:33,000 --> 00:09:44,000
So the Wajias were, the Wajias will come later in this discourse. The Wajias were an interesting group of people who didn't have a king.

71
00:09:44,000 --> 00:09:54,000
They were headed by the Lachu, the literally princess, the literally people who ran in a fairly democratic fashion.

72
00:09:54,000 --> 00:10:03,000
And they're the civilization very democratically.

73
00:10:03,000 --> 00:10:15,000
And so Ajata Saptu was, I guess, had his King William missions to take over India, after taking over the kingship from his father.

74
00:10:15,000 --> 00:10:21,000
He killed his father.

75
00:10:21,000 --> 00:10:26,000
And so he said to himself, I will strike the Wajians who are so powerful and strong.

76
00:10:26,000 --> 00:10:34,000
I will cut them off and destroy them. I will bring them to ruin and destruction.

77
00:10:34,000 --> 00:10:44,000
I may wonder what this has to do with this wonderful Sangha here in second bite.

78
00:10:44,000 --> 00:10:54,000
But, a good time, he sent one of us at Aras, he sent this minister of the king who went to see the Buddha.

79
00:10:54,000 --> 00:10:58,000
So he sent them to the Buddha to ask him a question.

80
00:10:58,000 --> 00:11:04,000
Because it's an interesting thing about the Buddha that he was apolitical.

81
00:11:04,000 --> 00:11:11,000
And this is something that Buddhists, I think, don't recognize enough or try to even brush his side.

82
00:11:11,000 --> 00:11:15,000
That the Buddha wasn't, didn't condemn people.

83
00:11:15,000 --> 00:11:23,000
Not in terms of denying them his teaching.

84
00:11:23,000 --> 00:11:27,000
If someone came to him and wanted to learn, he would teach.

85
00:11:27,000 --> 00:11:31,000
If a king and there was King Pascainadi, who was also not a very nice person,

86
00:11:31,000 --> 00:11:40,000
came to the Buddha and asked him questions. He would do his best to answer according to the Dhamma.

87
00:11:40,000 --> 00:11:49,000
But in this case, it's probably the height of absurdity. He goes to the Buddha to ask him,

88
00:11:49,000 --> 00:11:54,000
how would he ask him whether he's going to be victorious?

89
00:11:54,000 --> 00:11:58,000
He goes to him and to tell him exactly what he said.

90
00:11:58,000 --> 00:12:01,000
He says, King Pascainadi said, tell him that King Pascainadi says,

91
00:12:01,000 --> 00:12:05,000
I will strike the Vadgians who are so powerful and strong.

92
00:12:05,000 --> 00:12:09,000
I will cut them off and destroy them. I will bring them to ruin and destruction.

93
00:12:09,000 --> 00:12:14,000
He goes to the Buddha and he says, it sends them to the Buddha and says, tell this to the Buddha.

94
00:12:14,000 --> 00:12:20,000
And then whatever the Buddha says, come back and let me know,

95
00:12:20,000 --> 00:12:23,000
because the target doesn't ever want.

96
00:12:23,000 --> 00:12:30,000
Imagine someone going to ask the Buddha, it's not that the happy ending to this whole thing is,

97
00:12:30,000 --> 00:12:37,000
he eventually does convert to Buddhism, too late, of course, he kills his father.

98
00:12:37,000 --> 00:12:40,000
And he ends up going to hell. It's unavoidable.

99
00:12:40,000 --> 00:12:48,000
But he ends up being a sponsor for the first Buddhist council after the Buddha passes away.

100
00:12:48,000 --> 00:12:55,000
And he ends up becoming, you know, realizing how awful he had been.

101
00:12:55,000 --> 00:13:06,000
And I think there's some prophecy about how after he leaves hell, he'll eventually be able to learn the Buddha's teaching.

102
00:13:06,000 --> 00:13:12,000
But I'd imagine even right now he's most likely, you know, deep dark pit of hell, unfortunately.

103
00:13:12,000 --> 00:13:18,000
But sorry, so that was supposed to be the happy part is that he's set to come back.

104
00:13:18,000 --> 00:13:22,000
You know, he changed his ways later on.

105
00:13:22,000 --> 00:13:31,000
So sad story, this is one of his long story about him and Deva Datta and so not our story today.

106
00:13:31,000 --> 00:13:39,000
So this minister wasakara goes to see the Buddha and he says this to the Buddha.

107
00:13:39,000 --> 00:13:45,000
And so the Buddha turns to Anandai and says to Anandai.

108
00:13:45,000 --> 00:13:51,000
And he describes seven things. He asks Anandai seven questions.

109
00:13:51,000 --> 00:13:55,000
He asks whether they hold regular meetings.

110
00:13:55,000 --> 00:13:58,000
He asks whether they need harmony.

111
00:13:58,000 --> 00:14:02,000
He asks whether they hold to traditions.

112
00:14:02,000 --> 00:14:05,000
He asks whether they revere their elders.

113
00:14:05,000 --> 00:14:17,000
He asks whether they care for their women and not forcibly adopt them or compel them into marriage.

114
00:14:17,000 --> 00:14:32,000
And he asks whether they honor their ancestors.

115
00:14:32,000 --> 00:14:43,000
And finally he asks that he asks whether they, whether they respect and revere saints,

116
00:14:43,000 --> 00:14:48,000
or the requisites, people who are enlightened.

117
00:14:48,000 --> 00:14:58,000
So they have a policy of caring for religious monks and nuns and enlightenment.

118
00:14:58,000 --> 00:15:01,000
And Anandai answers him, yes, to the seven things.

119
00:15:01,000 --> 00:15:07,000
It's kind of an interesting discourse because this is fairly world that you might think.

120
00:15:07,000 --> 00:15:14,000
And he asks why is the Buddha asking about these things?

121
00:15:14,000 --> 00:15:15,000
But it becomes clear.

122
00:15:15,000 --> 00:15:17,000
And Anandai says yes.

123
00:15:17,000 --> 00:15:21,000
And he says, well, good, because I taught them those seven things.

124
00:15:21,000 --> 00:15:26,000
And he says as long as they keep those seven principles.

125
00:15:26,000 --> 00:15:31,000
As long as they keep these seven principles, they can be expected to prosper.

126
00:15:31,000 --> 00:15:41,000
Which is interesting, I think, for people who have to live in the world with questions about how to form a proper society.

127
00:15:41,000 --> 00:15:50,000
A Buddhist society, a society based on something that Buddha would at least nominally approve of.

128
00:15:50,000 --> 00:15:52,000
But it's actually not his point.

129
00:15:52,000 --> 00:15:55,000
It's more a way to kind of get rid of this guy.

130
00:15:55,000 --> 00:16:02,000
Because then when he leaves, the Buddha turns to Anandai and says, gather all the monks together.

131
00:16:02,000 --> 00:16:08,000
He says, when Buddha was in Rajakaha, so he says, gather all the monks in Rajakaha.

132
00:16:08,000 --> 00:16:15,000
Rajakaha was where I just accepted this.

133
00:16:15,000 --> 00:16:22,000
And then he explains to all these monks what he had said.

134
00:16:22,000 --> 00:16:28,000
And then he outlines what is really the whole reason for including this.

135
00:16:28,000 --> 00:16:35,000
He lays down seven principles, just like the seven that he had taught to the lungees.

136
00:16:35,000 --> 00:16:46,000
By which the monks, by which the Buddha, the Buddha's sangha, by which Buddha's.

137
00:16:46,000 --> 00:16:55,000
And Buddhism can be expected to prosper and not decline.

138
00:16:55,000 --> 00:17:12,000
So let's say seven that I wanted to talk about today in the interest of expressing appreciation for the community here

139
00:17:12,000 --> 00:17:17,000
and expressing the wish that this community prosper and not decline.

140
00:17:17,000 --> 00:17:26,000
And for all of us who are interested in community, whether it be this community or other communities that we're involved in.

141
00:17:26,000 --> 00:17:35,000
So the seven principles of harmony, or they're called the seven principles for non-decline.

142
00:17:35,000 --> 00:17:42,000
By which those who follow them are destined to not decline.

143
00:17:42,000 --> 00:17:44,000
And in fact, there's several sets of seven.

144
00:17:44,000 --> 00:17:47,000
This is the first set, probably the most vulnerable.

145
00:17:47,000 --> 00:17:55,000
He actually, it's a long suit, and in this first part, he goes over several different lists of seven.

146
00:17:55,000 --> 00:18:04,000
And they're all faithfully recorded, which is really great if you have a lot to read and learn about a Buddhist concept of.

147
00:18:04,000 --> 00:18:06,000
How to run the community.

148
00:18:06,000 --> 00:18:08,000
What are the important aspects?

149
00:18:08,000 --> 00:18:11,000
But the first ones, as I said, the most vulnerable.

150
00:18:11,000 --> 00:18:17,000
So the first principle is community.

151
00:18:17,000 --> 00:18:26,000
It's expressed by meeting often in the community that doesn't meet often.

152
00:18:26,000 --> 00:18:36,000
This is how cliques form misunderstandings arise and conflict and adversary arises.

153
00:18:36,000 --> 00:18:45,000
Meeting is a very important aspect that's not just meeting like this to listen to the Dhamma, but meeting to discuss.

154
00:18:45,000 --> 00:18:47,000
Meeting to discuss how to run the community.

155
00:18:47,000 --> 00:18:54,000
And remember when I was here on the board, we used to have Buddhist center board meetings.

156
00:18:54,000 --> 00:18:58,000
So we had our special little spot where we meet.

157
00:18:58,000 --> 00:19:09,000
And back when the load drill, I just named, it was involved in the four or five of us to sit down together.

158
00:19:09,000 --> 00:19:19,000
It's important. It's important to talk. It's important to work together.

159
00:19:19,000 --> 00:19:26,000
I have a sense of community.

160
00:19:26,000 --> 00:19:31,000
No, I'm interested.

161
00:19:31,000 --> 00:19:34,000
It's the whole point of a group, right?

162
00:19:34,000 --> 00:19:37,000
Or it's, you could even say it's why we have it.

163
00:19:37,000 --> 00:19:40,000
Yeah, it's the whole point.

164
00:19:40,000 --> 00:19:46,000
We asked what it means to have the community.

165
00:19:46,000 --> 00:19:51,000
So I suppose, in that sense, it goes without saying that we're going to meet.

166
00:19:51,000 --> 00:19:53,000
That's what it's of this place is all about.

167
00:19:53,000 --> 00:19:56,000
But part of it is respecting that.

168
00:19:56,000 --> 00:20:01,000
And understanding the importance of meeting together.

169
00:20:01,000 --> 00:20:03,000
Understanding the significance of it.

170
00:20:03,000 --> 00:20:04,000
That'll only come here.

171
00:20:04,000 --> 00:20:09,000
We're not here to show off or to look pretty.

172
00:20:09,000 --> 00:20:21,000
Or to argue that this is an important, important occasion for the significance of meeting together like this.

173
00:20:21,000 --> 00:20:23,000
Shouldn't be understated.

174
00:20:23,000 --> 00:20:26,000
This isn't just casual on a Saturday.

175
00:20:26,000 --> 00:20:30,000
This is a spiritual gathering.

176
00:20:30,000 --> 00:20:33,000
This is what it means to be really, really.

177
00:20:33,000 --> 00:20:35,000
This is the great person.

178
00:20:35,000 --> 00:20:38,000
It's the first principle.

179
00:20:38,000 --> 00:20:41,000
The second principle is harmony.

180
00:20:41,000 --> 00:20:43,000
So you might not only be thinking to yourself,

181
00:20:43,000 --> 00:20:46,000
yeah, well, meeting together is good, of course.

182
00:20:46,000 --> 00:20:49,000
But certainly not enough.

183
00:20:49,000 --> 00:20:59,000
Because obviously you can meet together and still argue and bicker and show off.

184
00:20:59,000 --> 00:21:07,000
And you go to stick on and meet for the wrong reasons.

185
00:21:07,000 --> 00:21:13,000
I was at a panel recently, a piece studies panel.

186
00:21:13,000 --> 00:21:20,000
And this guy stands up and starts criticizing one of the panel members and then a woman in the audience,

187
00:21:20,000 --> 00:21:23,000
called him a hatemonger.

188
00:21:23,000 --> 00:21:29,000
And there's mostly 20-year-old students.

189
00:21:29,000 --> 00:21:39,000
And this guy looking at each other like the room is just going to explode.

190
00:21:39,000 --> 00:21:43,000
But the moderator was really, really, really great.

191
00:21:43,000 --> 00:21:45,000
And it put faith in me.

192
00:21:45,000 --> 00:21:48,000
It reminded me of why we study piece.

193
00:21:48,000 --> 00:21:50,000
There's epic master there.

194
00:21:50,000 --> 00:21:51,000
The university I'm at there.

195
00:21:51,000 --> 00:21:53,000
There's a piece studies program.

196
00:21:53,000 --> 00:21:58,000
So undervalued, even when you hear it, it sounds like something fruity.

197
00:21:58,000 --> 00:22:03,000
I don't know, something kind of newygy or something.

198
00:22:03,000 --> 00:22:07,000
Piece studies is really tough, really challenging.

199
00:22:07,000 --> 00:22:09,000
And it's so useful.

200
00:22:09,000 --> 00:22:14,000
Like, we've ever seen a piece worker diffuse a situation.

201
00:22:14,000 --> 00:22:17,000
It's very Buddhist.

202
00:22:17,000 --> 00:22:20,000
It's not about who's right and who's wrong, right.

203
00:22:20,000 --> 00:22:23,000
One of the panelists, I think, Pro-Palestinian.

204
00:22:23,000 --> 00:22:28,000
And the audience guy, remember, guy, was probably bravely.

205
00:22:28,000 --> 00:22:33,000
And that is really a hot topic on the world, right?

206
00:22:33,000 --> 00:22:38,000
Maybe not an age or so much, but all over the western world.

207
00:22:38,000 --> 00:22:44,000
And then this other woman.

208
00:22:44,000 --> 00:22:47,000
So she, I think her heart was in the right place,

209
00:22:47,000 --> 00:22:52,000
but she estimated the purpose.

210
00:22:52,000 --> 00:22:55,000
I think many people in the room would have agreed with her sentiment.

211
00:22:55,000 --> 00:22:58,000
And this guy probably was an instigator.

212
00:22:58,000 --> 00:23:04,000
But, see, that's not how you deal with conflict.

213
00:23:04,000 --> 00:23:16,000
So we just bring it up as an example of how the dynamics of meaning of communities.

214
00:23:16,000 --> 00:23:23,000
And how important it is to diffuse the situation.

215
00:23:23,000 --> 00:23:27,000
So it's not about who's right.

216
00:23:27,000 --> 00:23:29,000
It's not so much about who's right and who's wrong.

217
00:23:29,000 --> 00:23:34,000
It's about diffusing situation.

218
00:23:34,000 --> 00:23:40,000
The more we try to, sometimes, the more we try to figure out who's right,

219
00:23:40,000 --> 00:23:46,000
the more, the more bitter the problem becomes.

220
00:23:46,000 --> 00:23:49,000
There's one way of, I mean, sometimes it's obvious.

221
00:23:49,000 --> 00:23:51,000
You can all agree, well, this person's wrong,

222
00:23:51,000 --> 00:23:53,000
and we can't deny that.

223
00:23:53,000 --> 00:23:59,000
We can't say, oh, well, let's just let it go.

224
00:23:59,000 --> 00:24:04,000
If someone has come into the crime or broken a rule,

225
00:24:04,000 --> 00:24:06,000
they have to be called out on it.

226
00:24:06,000 --> 00:24:08,000
But there is one way of solving conflicts.

227
00:24:08,000 --> 00:24:11,000
It's called Tina Waddaracle, which means Tina,

228
00:24:11,000 --> 00:24:14,000
or Tina, which means grass.

229
00:24:14,000 --> 00:24:17,000
Waddaraka means to cover over.

230
00:24:17,000 --> 00:24:19,000
Waddaraka.

231
00:24:19,000 --> 00:24:21,000
Waddaraka.

232
00:24:21,000 --> 00:24:23,000
Waddaraka.

233
00:24:23,000 --> 00:24:24,000
Waddaraka.

234
00:24:24,000 --> 00:24:25,000
What?

235
00:24:25,000 --> 00:24:26,000
What that?

236
00:24:26,000 --> 00:24:28,000
To cover.

237
00:24:28,000 --> 00:24:31,000
To cover over with grass.

238
00:24:31,000 --> 00:24:35,000
Which means you've got this problem, or it's like a fire, right?

239
00:24:35,000 --> 00:24:39,000
And you just smother it, or you've got cow done,

240
00:24:39,000 --> 00:24:41,000
or you've got mud or something.

241
00:24:41,000 --> 00:24:44,000
You just put down grass.

242
00:24:44,000 --> 00:24:46,000
Fresh grass.

243
00:24:46,000 --> 00:24:49,000
Cover it over.

244
00:24:49,000 --> 00:24:50,000
Sometimes you have to do that.

245
00:24:50,000 --> 00:24:52,000
It means put it behind this.

246
00:24:52,000 --> 00:24:58,000
I'm going to pass this past.

247
00:24:58,000 --> 00:25:00,000
It's just an example.

248
00:25:00,000 --> 00:25:09,000
And indeed, the importance is to diffuse situations,

249
00:25:09,000 --> 00:25:11,000
to create harmony.

250
00:25:11,000 --> 00:25:13,000
It's important that we meet in harmony.

251
00:25:13,000 --> 00:25:15,000
It's important that we conduct a business in harmony.

252
00:25:15,000 --> 00:25:17,000
We should respect that.

253
00:25:17,000 --> 00:25:19,000
Doesn't mean we should ignore grievance.

254
00:25:19,000 --> 00:25:21,000
It doesn't mean we shouldn't bring them up.

255
00:25:21,000 --> 00:25:25,000
But we should be aware that we should ask ourselves,

256
00:25:25,000 --> 00:25:30,000
is this for harmony, or is this for me, or is for pride?

257
00:25:30,000 --> 00:25:32,000
Is this done out of a desire for peace,

258
00:25:32,000 --> 00:25:39,000
or is this done out of a desire for conflict?

259
00:25:39,000 --> 00:25:44,000
Is this likely to bring peace, or is this likely to bring conflict?

260
00:25:44,000 --> 00:25:50,000
And remember, these are directly related to our existence,

261
00:25:50,000 --> 00:25:54,000
our continued existence, our fertility.

262
00:25:54,000 --> 00:25:58,000
If we don't recognize our escalating of conflict,

263
00:25:58,000 --> 00:26:02,000
and we'll mainly disagreeing with the way things are,

264
00:26:02,000 --> 00:26:05,000
where the community is running is on,

265
00:26:05,000 --> 00:26:11,000
it will cause the decline of our community.

266
00:26:11,000 --> 00:26:13,000
Number two.

267
00:26:13,000 --> 00:26:16,000
Number three is commitment.

268
00:26:16,000 --> 00:26:19,000
It means taking your community seriously.

269
00:26:19,000 --> 00:26:22,000
And Buddhism, this means following the rules.

270
00:26:22,000 --> 00:26:28,000
Don't showing it, don't show up to meetings drunk.

271
00:26:28,000 --> 00:26:31,000
Don't.

272
00:26:31,000 --> 00:26:36,000
I guess the one thing I often talk about is closing Facebook down.

273
00:26:36,000 --> 00:26:39,000
If you're sitting here, supposedly sitting here listening to me,

274
00:26:39,000 --> 00:26:43,000
but at the same time your Facebook opened up,

275
00:26:43,000 --> 00:26:45,000
well, what am I going to do?

276
00:26:45,000 --> 00:26:46,000
What am I going to do?

277
00:26:46,000 --> 00:26:49,000
But this is a danger.

278
00:26:49,000 --> 00:26:57,000
If we don't take seriously, we don't, if we aren't committed to the community,

279
00:26:57,000 --> 00:27:00,000
it could be that the speakers we have are dull and boring,

280
00:27:00,000 --> 00:27:02,000
like you may not be at all interested in what I'm saying,

281
00:27:02,000 --> 00:27:05,000
in that case you should not invite me back.

282
00:27:05,000 --> 00:27:07,000
Lightly tell me that you don't want me.

283
00:27:07,000 --> 00:27:09,000
I won't be hurt.

284
00:27:09,000 --> 00:27:13,000
But this is, it's not about assigning, you know,

285
00:27:13,000 --> 00:27:17,000
what was right as well, but taking it seriously.

286
00:27:17,000 --> 00:27:21,000
Something, it's an issue we have to look at to set it up

287
00:27:21,000 --> 00:27:24,000
so that we're doing the right thing, you know,

288
00:27:24,000 --> 00:27:27,000
that our speakers are productive,

289
00:27:27,000 --> 00:27:31,000
are useful and helpful, that they're teaching good things.

290
00:27:31,000 --> 00:27:33,000
But as an audience, we have to take it seriously.

291
00:27:33,000 --> 00:27:39,000
So you shouldn't show up naked, which actually has happened, right?

292
00:27:39,000 --> 00:27:40,000
Does happen.

293
00:27:40,000 --> 00:27:53,000
You shouldn't show up drunk or be engaged in anything else while you're listening while you're here.

294
00:27:53,000 --> 00:27:57,000
You shouldn't come here to dance and say they're not dance,

295
00:27:57,000 --> 00:28:00,000
but to play or to you.

296
00:28:00,000 --> 00:28:06,000
We used to have couples come here and lie down together in the hot springs.

297
00:28:06,000 --> 00:28:12,000
Not, I mean, hey, I mean, and it could be the thing that some communities do,

298
00:28:12,000 --> 00:28:17,000
but it means to take, to take to some extent, seriously,

299
00:28:17,000 --> 00:28:21,000
to watch your speech, to watch your actions.

300
00:28:21,000 --> 00:28:24,000
And Buddhism, it would mean not to kill, not to steal,

301
00:28:24,000 --> 00:28:26,000
but to rhyme.

302
00:28:26,000 --> 00:28:37,000
She's not to take drugs and alcohol.

303
00:28:37,000 --> 00:28:41,000
Not to get caught up in entertainment, just for fun,

304
00:28:41,000 --> 00:28:43,000
but to focus on, you know, if it was in my tradition, everyone would have to show up

305
00:28:43,000 --> 00:28:45,000
wearing white clothes traditionally.

306
00:28:45,000 --> 00:28:47,000
And all have to be wearing white.

307
00:28:47,000 --> 00:28:49,000
She probably wouldn't go over very well.

308
00:28:49,000 --> 00:28:53,000
I mean, it's not, this isn't a monastery, so I'm not suggesting

309
00:28:53,000 --> 00:28:58,000
that this place should be.

310
00:28:58,000 --> 00:29:06,000
But, yeah, I mean, there are certain etiquette to listening to the dynamic.

311
00:29:06,000 --> 00:29:11,000
Like in some, in very traditional places, in Thailand, anyway.

312
00:29:11,000 --> 00:29:15,000
Everyone would be holding their hands up during the entire speech,

313
00:29:15,000 --> 00:29:17,000
holding their hands up to their chest.

314
00:29:17,000 --> 00:29:20,000
We wouldn't sit cross-legged.

315
00:29:20,000 --> 00:29:25,000
You'd have to sit with one, at least one leg, one foot pointed back.

316
00:29:25,000 --> 00:29:27,000
It's a little bit of a different position.

317
00:29:27,000 --> 00:29:30,000
I think I have, I actually have a pose for the avatar.

318
00:29:30,000 --> 00:29:33,000
I think I can actually, you see, yeah.

319
00:29:33,000 --> 00:29:34,000
I can show you.

320
00:29:34,000 --> 00:29:35,000
Think?

321
00:29:35,000 --> 00:29:37,000
Is it going to change?

322
00:29:37,000 --> 00:29:38,000
It's not change.

323
00:29:38,000 --> 00:29:43,000
Hmm.

324
00:29:43,000 --> 00:29:47,000
I think I have to stand up first.

325
00:29:47,000 --> 00:29:57,000
It's supposed to change the way I look.

326
00:29:57,000 --> 00:30:00,000
And then, just an example.

327
00:30:00,000 --> 00:30:04,000
It's not what I would be suggesting, but, you know,

328
00:30:04,000 --> 00:30:12,000
these kind of ideas of taking things seriously.

329
00:30:12,000 --> 00:30:17,000
It wouldn't be, if it your hands up, but also not sitting cross-legged,

330
00:30:17,000 --> 00:30:19,000
there's a pose that it was.

331
00:30:19,000 --> 00:30:20,000
That's just in Thailand.

332
00:30:20,000 --> 00:30:21,000
In Sri Lanka, they don't follow that.

333
00:30:21,000 --> 00:30:25,000
They'll sit with their feet pointing towards the monk or the Buddha.

334
00:30:25,000 --> 00:30:26,000
They sit against them all.

335
00:30:26,000 --> 00:30:28,000
They sit comfortably.

336
00:30:28,000 --> 00:30:30,000
That's not really the point.

337
00:30:30,000 --> 00:30:37,000
But, you know, being, it's more being committed to tradition and rules

338
00:30:37,000 --> 00:30:42,000
and, you know, the etiquette of the community and I'll let our

339
00:30:42,000 --> 00:30:52,000
ways fall apart not to get lazy to be mindful of it.

340
00:30:52,000 --> 00:30:57,000
Number four is about its respect.

341
00:30:57,000 --> 00:31:04,000
Having this awesome respect that you all seem to have for the teachers,

342
00:31:04,000 --> 00:31:07,000
very impressed.

343
00:31:07,000 --> 00:31:10,000
Everyone seems very respectful.

344
00:31:10,000 --> 00:31:18,000
There's two kinds of respect.

345
00:31:18,000 --> 00:31:23,000
There's respect for someone who you consider to be.

346
00:31:23,000 --> 00:31:27,000
Sort of someone you...

347
00:31:27,000 --> 00:31:29,000
So, I don't want to say, look up to.

348
00:31:29,000 --> 00:31:33,000
I know some people are kind of touchy about the idea of people being

349
00:31:33,000 --> 00:31:37,000
bothered about each other and that's an important point in the business room.

350
00:31:37,000 --> 00:31:41,000
I'm not thinking of yourself as a bother below someone.

351
00:31:41,000 --> 00:31:48,000
But, yet there is some sense of being a student and someone being your teacher.

352
00:31:48,000 --> 00:31:51,000
So when someone has come to teach you something,

353
00:31:51,000 --> 00:31:55,000
like when a monk is teaching the dhamma, all the other monks are obliged to

354
00:31:55,000 --> 00:31:57,000
sit lower than that monk.

355
00:31:57,000 --> 00:32:01,000
They're obliged to respect that monk.

356
00:32:01,000 --> 00:32:04,000
Even if they don't respect the normally.

357
00:32:04,000 --> 00:32:07,000
When they're teaching the dhamma.

358
00:32:07,000 --> 00:32:09,000
And that...

359
00:32:09,000 --> 00:32:11,000
I mean, that's really where the focus goes.

360
00:32:11,000 --> 00:32:15,000
But, there's also a sense of elders.

361
00:32:15,000 --> 00:32:20,000
Someone who is senior, so monks have to be respectful to their seniors.

362
00:32:20,000 --> 00:32:25,000
In my tradition, there's a bit of a weird thing that a lot of people don't like.

363
00:32:25,000 --> 00:32:31,000
That the bikuni sangha has to pay respect to the bikhu sangha.

364
00:32:31,000 --> 00:32:36,000
If it seems sexist, well, I can't argue with that.

365
00:32:36,000 --> 00:32:41,000
But I think there's a defense, whether you agree with a defense or not,

366
00:32:41,000 --> 00:32:45,000
is that the bikuni sangha is junior to the bikhu sangha.

367
00:32:45,000 --> 00:32:54,000
But, you know, doesn't necessarily hold anybody that's part of the ancient tradition.

368
00:32:54,000 --> 00:32:58,000
And it wouldn't recommend that that is an necessary thing.

369
00:32:58,000 --> 00:33:05,000
But seniority is something that we do, when that's what's mentioned here,

370
00:33:05,000 --> 00:33:13,000
the Buddha says, whoever's senior is their respect for them.

371
00:33:13,000 --> 00:33:17,000
And the other kind of respect is respect for each other.

372
00:33:17,000 --> 00:33:19,000
So, we all respect.

373
00:33:19,000 --> 00:33:23,400
and the Buddhism they talk about, everyone having Buddha nature.

374
00:33:23,400 --> 00:33:27,160
I think it's a tension to become a Buddha, and you respect that and everyone,

375
00:33:27,160 --> 00:33:30,840
no matter how evil they might be, and I think that's something

376
00:33:30,840 --> 00:33:36,280
pterorada could. I'm sure it's there if you look at the text, but it's something we don't

377
00:33:36,280 --> 00:33:41,560
focus so much on. Respect for everyone.

378
00:33:41,560 --> 00:33:49,080
The Buddha was clear, just because someone does something evil like a jata satu.

379
00:33:49,080 --> 00:33:55,960
Doesn't mean you should shun them. It's only when they don't want to listen and they don't want to improve.

380
00:33:55,960 --> 00:34:03,160
The jata satu was, yeah, bad friends. That's why he, you know, this whole thing about good friendship

381
00:34:03,160 --> 00:34:08,200
being so important. A jata satu had the worst friend. They were that bad.

382
00:34:08,200 --> 00:34:14,040
But both Devadatta and a jata satu in the end, learned their error of their ways,

383
00:34:16,440 --> 00:34:22,120
and ended up paying respect to the Buddha. They're both in hell, no, unfortunately, but

384
00:34:23,320 --> 00:34:23,960
they'll be back.

385
00:34:30,120 --> 00:34:35,240
They respect, respecting each other, respecting the person who's teaching,

386
00:34:35,240 --> 00:34:40,040
respecting people who are senior to you. I mean, it's not even so much about

387
00:34:40,680 --> 00:34:46,920
do it because we tell you it's more like that's useful. Remember, these are for the preservation

388
00:34:46,920 --> 00:34:51,400
of our society. It actually goes further than just do it because we tell you to.

389
00:34:53,000 --> 00:34:58,680
It's a sense of appreciation of our elders, people who have experience, people who have been around

390
00:34:58,680 --> 00:35:03,800
here a long time. I mean, you could even have that here. Look, everyone here has a birthday,

391
00:35:03,800 --> 00:35:06,840
right? How old are you? How many years have you been born on second life?

392
00:35:08,600 --> 00:35:12,920
So when you see someone that has been on second life for a while, whoa, you respect that.

393
00:35:13,720 --> 00:35:19,720
That's good. It means that you'll be able to learn from them. They will be sure they're clearly

394
00:35:20,360 --> 00:35:27,240
newbie, even just in terms of second life in general. You shouldn't be too cocky.

395
00:35:27,240 --> 00:35:33,640
You can learn a lot from the people of the Buddha Center, of course, has to be even more so

396
00:35:34,520 --> 00:35:40,280
in the spiritual center, respecting your elders who's been around longer.

397
00:35:41,640 --> 00:35:46,040
Now, what it shouldn't get into, of course, is this ego trip where that's the other side.

398
00:35:46,680 --> 00:35:53,720
Respect isn't, isn't a two-way street in the sense that the people who are senior should expect

399
00:35:53,720 --> 00:35:59,160
it. Absolutely not. A more senior, you've got a true sign of seniority in Buddhism,

400
00:35:59,160 --> 00:36:04,520
like someone who's really earned their seniority as someone who has none of that, who has no ego

401
00:36:04,520 --> 00:36:10,600
about it. Never thinks of themselves as senior. That's a person who really deserves their respect.

402
00:36:12,040 --> 00:36:17,960
Nonetheless, having been a monk for many years, I can tell you, you're still better off paying

403
00:36:17,960 --> 00:36:25,320
respect to the crotch of the old monk who really gets a kick out of the down-down. That's just

404
00:36:25,320 --> 00:36:34,360
easier. These are the fighting with them. I'll respect useful good. Number five is renunciation.

405
00:36:35,800 --> 00:36:42,280
Let's sit, this speaks more into the actual practice. The Buddha said, we should, if you want,

406
00:36:42,280 --> 00:36:54,120
Buddhism to continue. Then, one of the principles that we need to follow is to give up.

407
00:36:56,520 --> 00:36:59,880
And he says specifically, give up those desires that would lead to reverb.

408
00:37:01,160 --> 00:37:06,760
That's what we have to look at. That's what our desires are leading to.

409
00:37:06,760 --> 00:37:14,680
Our desires will have consequences in the future. They're mapping out

410
00:37:16,760 --> 00:37:25,800
any type of direction for them. Not random. Your future is not determined by God.

411
00:37:26,440 --> 00:37:32,760
Well, it's not random. It's directly related to what you're cultivating now.

412
00:37:32,760 --> 00:37:43,800
And desires will be delivered. If they are strong desires, they grow course reverb. They're

413
00:37:44,840 --> 00:37:51,720
light and relatively blameless desires. They will lead to a good reverb.

414
00:37:53,000 --> 00:37:59,400
Desires for world peace desires, for harmony, these kind of things. Better still lead to reverb,

415
00:37:59,400 --> 00:38:03,640
but if you're interested in a harmonious society, it might go to heaven.

416
00:38:06,520 --> 00:38:11,960
If you're just interested in carnal pleasures, well, probably not going to heaven.

417
00:38:14,600 --> 00:38:17,960
I'm not sure if they engage in carnal activities in heaven, but

418
00:38:20,120 --> 00:38:25,000
I haven't gotten a word on that one, but certainly much more refined than the world.

419
00:38:25,000 --> 00:38:33,640
We consider the essential pleasures of humans versus the essential pleasures of animals,

420
00:38:33,640 --> 00:38:39,560
like dogs or cockroaches. It's a graded, it's a graded scale.

421
00:38:40,760 --> 00:38:43,560
Directly related to your desires, where you go.

422
00:38:48,360 --> 00:38:54,200
And it's something that disrupts communities. It would make you feel bored right now.

423
00:38:54,200 --> 00:38:59,640
If you're feeling bored, it could be that I'm running boring because I know I have good potential

424
00:38:59,640 --> 00:39:09,080
to be somewhat interesting, possible. But it can also be because of our intense attachment

425
00:39:10,200 --> 00:39:15,480
to pleasure. Yeah, it's a nice talk. It's interesting, but you know,

426
00:39:17,240 --> 00:39:23,800
where's the beauty? Where's the wonderful sound? I'd rather go listen to music. I'd rather go watch

427
00:39:23,800 --> 00:39:27,080
me. I don't remember what ever it was.

428
00:39:27,080 --> 00:39:53,480
I don't know what ever it was, but I don't know what ever it was.

