1
00:00:00,000 --> 00:00:07,960
Hi, so now I'll get into some of the issues that I talked about in the last video.

2
00:00:07,960 --> 00:00:15,200
The first one is the absolute nature of morals and morality, of course, is something that

3
00:00:15,200 --> 00:00:23,240
is heartily denounced by the materialists as something that is simply, generally, as something

4
00:00:23,240 --> 00:00:30,720
that is simply a product of the sort of the way it worked or the way it turned out.

5
00:00:30,720 --> 00:00:38,240
The reason that we have morality at all is because of evolution, so if there was a group

6
00:00:38,240 --> 00:00:47,440
of people who, a group of beings who had no morality, well, they may have existed, but

7
00:00:47,440 --> 00:00:54,560
they've become extinct because of their inability to work together, because of the non-desirability

8
00:00:54,560 --> 00:00:55,560
of these things.

9
00:00:55,560 --> 00:01:02,400
Now, okay, for sure agreed that our morality, but listen to what you're saying, our morality,

10
00:01:02,400 --> 00:01:09,120
as it exists today, is, exists that way because of the way it's been developed, and of

11
00:01:09,120 --> 00:01:10,600
course that's the case.

12
00:01:10,600 --> 00:01:16,640
That doesn't mean that the way we look at morality is in any way moral, so using that

13
00:01:16,640 --> 00:01:22,800
as an argument to say, well, that's why morality is relative, or that's why morality is simply

14
00:01:22,800 --> 00:01:24,600
a product of nature.

15
00:01:24,600 --> 00:01:27,440
It doesn't say anything at all.

16
00:01:27,440 --> 00:01:36,160
Now if you were to say that, well, our current state of being is perfect, or is the best

17
00:01:36,160 --> 00:01:40,280
that we could possibly be, then yes, then you would have to say, well, yeah, then the

18
00:01:40,280 --> 00:01:48,960
perfect morality is, or real morality is relative, or is based on this sort of, or is exactly

19
00:01:48,960 --> 00:01:54,200
the way we look at it, the way we carry it out, and it's very much relative, certain

20
00:01:54,200 --> 00:01:55,920
times we keep it, certain times.

21
00:01:55,920 --> 00:02:01,240
But the truth is, relative morality has not served us very well.

22
00:02:01,240 --> 00:02:04,240
Neither has absolute morality as we understand it.

23
00:02:04,240 --> 00:02:12,240
Now this doesn't disprove the fact, the idea that there might be absolute morality, and

24
00:02:12,240 --> 00:02:17,680
I'm going to discuss why it's possible to understand that there is absolute morality.

25
00:02:17,680 --> 00:02:21,000
Now what does it mean to say there's absolute morality?

26
00:02:21,000 --> 00:02:26,640
Saying that there's absolute morality is simply saying that there is a law which governs

27
00:02:26,640 --> 00:02:33,480
the nature of the mind, and this is exactly what we would expect if we were to come to

28
00:02:33,480 --> 00:02:38,920
a scientific understanding of the mind, if we were to admit in a scientific terms the existence

29
00:02:38,920 --> 00:02:47,800
of the mind, we should expect and not be suspicious when we find that it actually runs

30
00:02:47,800 --> 00:02:51,880
according to certain laws, just as the physical world does.

31
00:02:51,880 --> 00:02:55,200
So we look at the physical world and we say, well, gravity, yeah, it's universal.

32
00:02:55,200 --> 00:02:59,360
You know, it's not a relative thing where sometimes the body goes according to and sometimes

33
00:02:59,360 --> 00:03:06,240
the body, the physical doesn't, gravity is universal as our, you know, all physical laws

34
00:03:06,240 --> 00:03:09,960
as far as we can understand them.

35
00:03:09,960 --> 00:03:14,360
Now what we understand is that the mind runs according to certain laws as well, and these

36
00:03:14,360 --> 00:03:17,160
laws we could call morality.

37
00:03:17,160 --> 00:03:22,880
Now I'm going to give a very specific definition of morality or a different definition

38
00:03:22,880 --> 00:03:24,400
of good, I suppose.

39
00:03:24,400 --> 00:03:28,200
We consider something to be good when it brings happiness.

40
00:03:28,200 --> 00:03:33,640
And this is a view where this is sort of a judgment call.

41
00:03:33,640 --> 00:03:39,200
You could say that suffering or unhappiness is a good thing.

42
00:03:39,200 --> 00:03:45,720
I don't think that's a very good explanation of reality because, you know, by definition

43
00:03:45,720 --> 00:03:48,440
once we call it suffering, it's not a good thing.

44
00:03:48,440 --> 00:03:51,960
So we're going to say that a good thing it brings happiness.

45
00:03:51,960 --> 00:03:56,040
Therefore anything that is moral is something that brings happiness.

46
00:03:56,040 --> 00:04:02,560
We will say those things which bring true happiness, those things are truly moral.

47
00:04:02,560 --> 00:04:06,760
And when we look at it this way, we can see that they're actually, for sure, must be

48
00:04:06,760 --> 00:04:15,160
certain laws which bring about the, these states of happiness, this reality which we would

49
00:04:15,160 --> 00:04:21,560
then call happiness or freedom from suffering.

50
00:04:21,560 --> 00:04:28,080
This allows us to, at least form a basis of what we might call, we might call ultimate

51
00:04:28,080 --> 00:04:29,080
morality.

52
00:04:29,080 --> 00:04:34,040
And in fact, people who are generally call themselves relative moralists or people who

53
00:04:34,040 --> 00:04:39,960
say that morality is something that changes over time actually have a very rigid, very universal

54
00:04:39,960 --> 00:04:41,440
understanding of morality anyway.

55
00:04:41,440 --> 00:04:48,680
They would say that, for instance, in the case of dictators or terribly cruel people,

56
00:04:48,680 --> 00:04:52,800
you have a moral responsibility to kill them, for instance, or to do away with them.

57
00:04:52,800 --> 00:04:56,640
And so they say, for that reason, morality is relative.

58
00:04:56,640 --> 00:05:02,640
Well, this is really absurd because what you're saying is that we always have the moral,

59
00:05:02,640 --> 00:05:04,960
you know, it's moral.

60
00:05:04,960 --> 00:05:08,560
We have this moral principle that in order to create happiness, we have to kill someone.

61
00:05:08,560 --> 00:05:12,080
We have to do something that other people might say is a bad deed, but this is actually

62
00:05:12,080 --> 00:05:14,040
a good thing it actually brings happiness.

63
00:05:14,040 --> 00:05:15,760
And so this is a moral law.

64
00:05:15,760 --> 00:05:21,880
And they would say that always a cruise, that always holds.

65
00:05:21,880 --> 00:05:27,440
You couldn't say, well, you know, what if there was, you know, could you sometimes kill

66
00:05:27,440 --> 00:05:31,960
someone who's doing bad deeds or stop someone who's doing bad deeds and not sometimes

67
00:05:31,960 --> 00:05:34,440
and so on.

68
00:05:34,440 --> 00:05:39,680
And so what we're actually looking at is, you know, we're all trying to find this understanding

69
00:05:39,680 --> 00:05:45,880
of what are the laws of the mind, the ways that the mind can then experience peace, experience

70
00:05:45,880 --> 00:05:50,480
happiness, experience freedom and suffering, and yet we just approach them in different ways.

71
00:05:50,480 --> 00:05:57,040
So I don't think there's any reason for us to think that there should not be an absolute

72
00:05:57,040 --> 00:06:01,880
guide of what is right and what is wrong.

73
00:06:01,880 --> 00:06:07,200
I think in general, we're all looking for this, we're all sort of expecting that there

74
00:06:07,200 --> 00:06:10,960
will be some laws that govern the nature of the mind, some people deny the existence of

75
00:06:10,960 --> 00:06:17,040
the mind, but they still think that there is some guiding rules on how to find happiness

76
00:06:17,040 --> 00:06:18,040
in peace.

77
00:06:18,040 --> 00:06:24,160
And those rules, I would call moral.

78
00:06:24,160 --> 00:06:33,000
The addition, which I would give here, is that once we give the mind a real existence,

79
00:06:33,000 --> 00:06:38,960
we then have to realize that everything we do is going to then affect the nature of the

80
00:06:38,960 --> 00:06:39,960
mind.

81
00:06:39,960 --> 00:06:43,760
So every time we kill, it is going to affect our mind.

82
00:06:43,760 --> 00:06:48,880
Every time we do something because killing or stealing or all these things, which we generally

83
00:06:48,880 --> 00:06:53,240
consider to be immoral, we do have to understand that they are going to have an impact

84
00:06:53,240 --> 00:06:54,240
on our mind.

85
00:06:54,240 --> 00:06:56,240
They are going to change the way we think.

86
00:06:56,240 --> 00:07:00,480
This is why people feel guilt, this is why people feel upset, this is why people feel

87
00:07:00,480 --> 00:07:03,760
rage, this is why people have nightmares.

88
00:07:03,760 --> 00:07:10,280
That's why people who do bad things tend to have difficulty meditating, for instance.

89
00:07:10,280 --> 00:07:11,880
They find that they have mental instability.

90
00:07:11,880 --> 00:07:14,560
So this is the first of the things that I wanted to discuss.

91
00:07:14,560 --> 00:07:17,000
The idea that there might be an absolute morality.

92
00:07:17,000 --> 00:07:22,200
In fact, through meditation, it's very easy to see that there is a law which governs

93
00:07:22,200 --> 00:07:26,360
the mind, but without any meditation, I'm just going to leave it at that and give sort

94
00:07:26,360 --> 00:07:31,800
of this superficial explanation.

95
00:07:31,800 --> 00:07:36,480
As soon as you practice any kind of meditation, which allows you to see the reality or

96
00:07:36,480 --> 00:07:40,320
the nature of the mind, it's very easy to see that it is governed by very specific laws

97
00:07:40,320 --> 00:07:46,080
and these are an absolute morality.

98
00:07:46,080 --> 00:07:50,760
There are many other things I could say about morality, things like people talk about how

99
00:07:50,760 --> 00:07:54,080
absolute morality could lead to suffering and so on.

100
00:07:54,080 --> 00:08:00,240
So the idea is that absolute morality could never lead to suffering, otherwise it wouldn't

101
00:08:00,240 --> 00:08:05,240
be moral, since we say that morality leads to happiness.

102
00:08:05,240 --> 00:08:09,200
This is a very deep subject, perhaps I'll have to give another talk on exactly what is

103
00:08:09,200 --> 00:08:16,720
happiness and arguing some of the fine details, but I think this is sort of a good basic

104
00:08:16,720 --> 00:08:17,720
understanding.

105
00:08:17,720 --> 00:08:19,040
So thank you for watching.

106
00:08:19,040 --> 00:08:25,040
This is the first in my series of sort of corollaries on the existence of the mind.

107
00:08:25,040 --> 00:08:53,040
Thank you.

