1
00:00:00,000 --> 00:00:17,480
Hello good evening everyone broadcasting live May 17th day I'm in New York at

2
00:00:17,480 --> 00:00:38,240
Bicobodis Monastery with Venerable Gintock I even they had monk of the Chinese Monastery

3
00:00:38,240 --> 00:00:48,480
in Toronto so that they finally we got to meet Bicobodis he's staying in the house just

4
00:00:48,480 --> 00:00:54,120
down the way I didn't realize this was actually a barely big monastery it's like a hundred

5
00:00:54,120 --> 00:01:02,640
acres do we have to walk to his house we're staying in the one way in the complex area

6
00:01:02,640 --> 00:01:25,160
so today's quote is about the Buddha and we're in there's not much to say about it

7
00:01:25,160 --> 00:01:34,520
clearest interesting aspect of though this quote out of context sounds like the Buddha is

8
00:01:34,520 --> 00:01:47,080
somehow focusing on how much better he is than all of us because he was first it

9
00:01:47,080 --> 00:01:56,720
actually think the context and the feeling I got from it anyway is it's actually more that's

10
00:01:56,720 --> 00:02:03,480
the only difference so the difference between the Buddha and his followers is just that

11
00:02:03,480 --> 00:02:28,320
he came first the freedom from suffering the enlightenment is the same what kind of puts

12
00:02:28,320 --> 00:02:35,520
us all on even footing and it's it's I mean important part of it is how encouraging

13
00:02:35,520 --> 00:02:48,120
that is for all of us that we have inside of ourselves the potential for enlightenment

14
00:02:48,120 --> 00:03:01,360
it's great potential inside of us we can do so many things we can you can reach and

15
00:03:01,360 --> 00:03:20,480
famous you can go to heaven we can go to hell you can go to nibhana Simon I don't remember

16
00:03:20,480 --> 00:03:26,960
whether I actually advertised the fact that we had a Danish translation but you know

17
00:03:26,960 --> 00:03:43,280
thanks for translating it remember I may have just put it up and forgot to do we have

18
00:03:43,280 --> 00:03:56,200
one line today a bunch of people everyone I'll be back tomorrow

19
00:03:56,200 --> 00:04:05,400
we're gonna leave here after breakfast I think and so tomorrow evening I'll be back

20
00:04:05,400 --> 00:04:27,480
to should be back to video broadcast anyone has any questions and happy to stick around

21
00:04:27,480 --> 00:04:52,200
how y'all doing as your meditation going we just found out bad news that Michael is probably

22
00:04:52,200 --> 00:05:06,320
leaving us he has a dog that we didn't know about and he left his dog with his brother

23
00:05:06,320 --> 00:05:18,120
and his brother says he can't take care of the dog anymore so we're back in the back in

24
00:05:18,120 --> 00:05:29,960
the market for a steward anybody wants to come and stay with us long term and we're

25
00:05:29,960 --> 00:05:38,800
probably not gonna as a result rent a we were thinking of renting a larger place but that's

26
00:05:38,800 --> 00:05:45,440
probably not gonna happen because even taking care of a small place without a steward is

27
00:05:45,440 --> 00:05:51,880
difficult we have more meditators in a larger place I think it's too much without someone

28
00:05:51,880 --> 00:06:13,360
helping so that's you know impermanence and impermanence change if anybody wants to come

29
00:06:13,360 --> 00:06:19,280
and stay with us long term I guess preferably someone who lives in Canada so they don't

30
00:06:19,280 --> 00:06:25,480
have to worry about visas but we'll take whatever we can get didn't we have someone there

31
00:06:25,480 --> 00:06:35,840
were two people why do I want to say this was it was it was it Kathy was it steward and

32
00:06:35,840 --> 00:06:42,840
you'll look at this you know the first

33
00:07:05,840 --> 00:07:34,640
it was another book person seems to me it was a female person did you see the large

34
00:07:34,640 --> 00:07:47,960
Buddha statue Ivan Ivan did we see the big Buddha statue did we see the largest Buddha

35
00:07:47,960 --> 00:08:03,040
statue where is it it's here this is the largest Buddha statue in what center in this

36
00:08:03,040 --> 00:08:08,840
monastery this monastery has the largest Buddha image in in the North America or

37
00:08:08,840 --> 00:08:22,680
something I haven't actually seen the largest Buddha statue but now that I know it's here

38
00:08:22,680 --> 00:08:38,600
I'll have to maybe go take a look at when we need the new steward A.S. A.P. as soon

39
00:08:38,600 --> 00:08:53,880
as possible hey sunkers here I think I hope or clear I think we are that I think we're

40
00:08:53,880 --> 00:09:00,440
I don't know how to say this but we're not really taking seriously Seneca's plans for

41
00:09:00,440 --> 00:09:06,440
me and Sri Lanka I hope because it was a bit of a disaster last time I think we're on the

42
00:09:06,440 --> 00:09:17,520
same page with that just one I've got you on here he wants me to he's got like 20 places

43
00:09:17,520 --> 00:09:24,200
for me to give talks and after those people I'm I know I recognize some of them our

44
00:09:24,200 --> 00:09:28,360
places where I went last time and they didn't really understand why they were inviting

45
00:09:28,360 --> 00:09:36,120
me and in certain in some cases actually regretted inviting because of course not

46
00:09:36,120 --> 00:09:40,080
everyone is into the same things we are

47
00:09:46,640 --> 00:09:53,800
Seneca thank you and we'll be seeing you next month I was wondering whether we

48
00:09:53,800 --> 00:10:04,160
should do more publish more books booklets I know we're doing a lot but maybe we

49
00:10:04,160 --> 00:10:10,800
should consider if that's doable because I'll be going there right so I can

50
00:10:10,800 --> 00:10:16,240
bring some back maybe good idea they're not so like they're expensive I've

51
00:10:16,240 --> 00:10:21,760
still got probably 500 I haven't counted them but it seems like I've got

52
00:10:21,760 --> 00:10:29,200
about half left but I forget to fix them

53
00:10:29,200 --> 00:10:51,440
I mean if they are interested and if they know who I am and if they are really

54
00:10:51,440 --> 00:10:59,960
interested in inviting me and not just oh pseudo humble or I don't know meditation

55
00:10:59,960 --> 00:11:28,760
oh yeah print probably a thousand that's such a big deal

56
00:11:28,760 --> 00:11:38,280
if we print more and I can just bring a big empty suitcase to bring at least some

57
00:11:38,280 --> 00:11:44,640
of the backing I have to make sure I have luggage allowance but I can bring

58
00:11:44,640 --> 00:11:51,280
some back probably not all of them and we'll have to find people going back and

59
00:11:51,280 --> 00:11:57,880
forth from Seneca which is always happening so that should doable

60
00:11:57,880 --> 00:12:22,320
I can see there's a lag lag between what I'm saying what you guys are

61
00:12:22,320 --> 00:12:45,920
typing probably 10 seconds yeah remember last year with some people were like

62
00:12:45,920 --> 00:12:49,320
well I just got a call from Seneca and he said you have to invite this

63
00:12:49,320 --> 00:12:55,400
monk so you don't really know why you're inviting me or who I am or anything not

64
00:12:55,400 --> 00:13:22,120
really good best

65
00:13:22,120 --> 00:13:29,120
okay call in the night if I see anybody type anything I'll just type back

66
00:13:29,120 --> 00:13:34,560
but this can see there's a bit of a lag so I think this is enough of you to

67
00:13:34,560 --> 00:13:41,240
like regular regularly scheduled video broadcast should resume tomorrow if

68
00:13:41,240 --> 00:13:47,040
I'm still alive and if everything is as planned if the

69
00:13:47,040 --> 00:13:58,560
Canada still hasn't dissolved in the world still hasn't broken down if the

70
00:13:58,560 --> 00:14:05,520
Internet still exists tomorrow and I'll probably be up in broadcasting so

71
00:14:05,520 --> 00:14:22,520
that's all for audio good luck

