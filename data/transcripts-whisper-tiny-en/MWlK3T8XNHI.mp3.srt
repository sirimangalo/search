1
00:00:00,000 --> 00:00:07,880
Thinking certain ways and behave in certain ways. We we become accustomed and habituated to to you know

2
00:00:09,040 --> 00:00:13,340
to conceptualize certain things and and and sometimes

3
00:00:14,880 --> 00:00:16,880
they create stress they create

4
00:00:19,200 --> 00:00:23,860
Detriman in your lives and it's not until you actually gravitate towards

5
00:00:24,480 --> 00:00:26,480
spiritual practices such as

6
00:00:26,480 --> 00:00:34,240
Meditation and you realize that those things are truly dangerous to your mind. It's uh, you know as you start to

7
00:00:34,720 --> 00:00:40,880
Really calm down and sharpen your mind and really observe what you have been learning for for

8
00:00:41,520 --> 00:00:43,520
Your entire life

9
00:00:43,520 --> 00:00:48,520
You start to realize that you've been really just hurting yourself. So yeah, I would say that

10
00:00:49,600 --> 00:00:51,600
It is a process of learning

11
00:00:51,600 --> 00:00:56,080
What your society has taught you. I wouldn't say it's a process of

12
00:00:56,880 --> 00:00:58,880
Unlearning all of

13
00:00:59,520 --> 00:01:01,520
social reality

14
00:01:01,920 --> 00:01:03,920
Yeah

15
00:01:04,480 --> 00:01:10,000
So yeah, you you basically start to see clearly what your social

16
00:01:11,520 --> 00:01:16,240
Your your society has taught you that's detrimental. You might not just forget everything

17
00:01:16,240 --> 00:01:23,120
Yeah, it's being able to see what is useful. Yeah, exactly. Yeah, because you know

18
00:01:25,360 --> 00:01:27,360
Basically everything that we do has been

19
00:01:28,160 --> 00:01:31,840
socialized to some extent. I'm not saying that everything, you know

20
00:01:34,000 --> 00:01:36,000
Yeah, but um

21
00:01:36,320 --> 00:01:41,440
Yeah, and some some of them are beneficial and some of them are neutral and some of them are detrimental

22
00:01:41,440 --> 00:01:48,960
and part of the meditation practice is to see clearly on what is good for us that we should strengthen it more

23
00:01:49,360 --> 00:01:53,600
what is bad for us that we should you know minimize it and

24
00:01:55,040 --> 00:02:00,480
And basically just you know and it's a progressive practice the more we do it the more we see it clearly and

25
00:02:01,120 --> 00:02:03,600
The more peace we can maintain so

26
00:02:04,640 --> 00:02:07,840
Yeah, I guess in some ways you can deviate from

27
00:02:07,840 --> 00:02:13,680
Your society, but um, I guess that's sometimes you just have to do that to

28
00:02:14,240 --> 00:02:16,240
Maintain that sort of peace for yourself

29
00:02:22,000 --> 00:02:25,920
Isn't it great? It's not often that the teacher can show off his students

30
00:02:27,680 --> 00:02:33,840
I'm not showing up. It's just an interesting. I thought it would be interesting for everyone to see here. We have a real

31
00:02:33,840 --> 00:02:37,840
live meditator

32
00:02:37,840 --> 00:02:41,520
Someone and you know, he's not he doesn't look he smiles

33
00:02:43,360 --> 00:02:46,160
He's been practicing diligently for 21 days

34
00:02:46,800 --> 00:02:49,600
Excellent response. In fact, you know, it did that

35
00:02:49,600 --> 00:03:05,280
I learned something from it. Thank you

