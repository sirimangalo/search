1
00:00:00,000 --> 00:00:12,920
Okay, good evening, everyone.

2
00:00:12,920 --> 00:00:23,680
We're back, we have meditators here at our center.

3
00:00:23,680 --> 00:00:45,840
We're back on the internet broadcasting live, it's a time for beginning, so we go back to

4
00:00:45,840 --> 00:00:52,640
the beginning where it all began under the Bodhi tree.

5
00:00:52,640 --> 00:01:04,360
The Bodhisatta, this prince in India, left home, just like all of us leave home to come

6
00:01:04,360 --> 00:01:24,560
to better ourselves spiritually, and he went to a secluded place and he sat under a tree and

7
00:01:24,560 --> 00:01:27,960
he realized in a very important truth.

8
00:01:27,960 --> 00:01:33,440
He found the beginning, this was the beginning of Buddhism and he found the beginning.

9
00:01:33,440 --> 00:01:41,120
He found where it all starts.

10
00:01:41,120 --> 00:01:49,520
And so when we talk about meditation, it's important, it's really a crucial distinction

11
00:01:49,520 --> 00:01:58,040
that we make between, I mean really meditation that doesn't get to the root and meditation

12
00:01:58,040 --> 00:02:01,840
that gets to the root.

13
00:02:01,840 --> 00:02:17,440
Some people object to this separating meditation into two categories.

14
00:02:17,440 --> 00:02:30,440
But I think it's quite easy to see the difference if you understand it correctly.

15
00:02:30,440 --> 00:02:34,640
The Bodhisatta, because he had been practicing meditation, he had practiced various types

16
00:02:34,640 --> 00:02:40,120
of meditation and he realized that they didn't get to the root.

17
00:02:40,120 --> 00:02:49,160
They were great, they were the kind of meditation that would lead you to deep trance states,

18
00:02:49,160 --> 00:02:57,840
sort of transcendental meditative states, and even lead you to be reborn in the Brahma

19
00:02:57,840 --> 00:03:08,840
world as a God, this was how one became one with God in India at the time.

20
00:03:08,840 --> 00:03:13,000
And he still said this isn't the way, you know.

21
00:03:13,000 --> 00:03:19,200
He remembered or he had a sense that I've been to the Brahma world before, that's not

22
00:03:19,200 --> 00:03:24,880
the way out of suffering or the Brahma world's are not an end.

23
00:03:24,880 --> 00:03:29,280
It could sense that this wasn't getting to the root and it quite clearly isn't.

24
00:03:29,280 --> 00:03:36,200
I mean the problem that these sort of meditations have is explaining the connection.

25
00:03:36,200 --> 00:03:40,200
How does this connect with the real problem?

26
00:03:40,200 --> 00:03:45,920
I can enter into a trance state, I can experience great peace, but how does that relate

27
00:03:45,920 --> 00:03:58,120
to my problems, how does that relate to my anxiety, how does that relate to my addiction,

28
00:03:58,120 --> 00:04:02,880
how does that relate to my suffering?

29
00:04:02,880 --> 00:04:10,480
Because it appears to just be avoiding the problem, it appears to just be trying to escape

30
00:04:10,480 --> 00:04:19,560
your problems rather than assault them.

31
00:04:19,560 --> 00:04:34,240
And so what the Buddha realized was something that started with Aweenja Pacheasanga, ignorance

32
00:04:34,240 --> 00:04:44,200
with ignorance as the cause, their eyes formation, Sankara, it's a very important Buddhist

33
00:04:44,200 --> 00:04:45,200
word.

34
00:04:45,200 --> 00:04:56,960
Aweenja Pacheasanga, Aweenja means knowledge, Aweenja means lack of knowledge.

35
00:04:56,960 --> 00:05:08,520
He realized that lack of knowledge is the root, ignorance, which is a very important point

36
00:05:08,520 --> 00:05:20,720
because it's quite different from the idea that lack of effort, lack of control, those

37
00:05:20,720 --> 00:05:32,520
aren't the cause of our problems, Sankara is our problems, Sankara means we create things.

38
00:05:32,520 --> 00:05:39,040
We create things, we have ambitions, we have goals, we build up lives for ourselves, we're

39
00:05:39,040 --> 00:05:44,920
starting with birth, we did this, we decided when we died, hey, that looks nice, let's

40
00:05:44,920 --> 00:05:50,360
go that way and we clung to something, we clung to this and it created this, out of

41
00:05:50,360 --> 00:05:53,960
that.

42
00:05:53,960 --> 00:06:00,080
And then as we grow, as we're born and we grow, we clung and we clung and we make decisions

43
00:06:00,080 --> 00:06:07,160
and we cultivate ambitions and we become and we become and we form and we form.

44
00:06:07,160 --> 00:06:13,760
And we form habits.

45
00:06:13,760 --> 00:06:19,000
And our habits grow into patterns and they become ingrained and become very much a part

46
00:06:19,000 --> 00:06:25,520
of who we are.

47
00:06:25,520 --> 00:06:34,360
And so what the Buddha saw was very important that it was ignorance, it wasn't control,

48
00:06:34,360 --> 00:06:35,360
right?

49
00:06:35,360 --> 00:06:39,720
So in meditation we're not trying to actively fix our problems, you're not sitting there

50
00:06:39,720 --> 00:07:08,760
and saying out with you anger, out with you greed, out with you delusion, go away, get lost.

51
00:07:08,760 --> 00:07:23,680
You're not trying to control them, you're not trying to fix them.

52
00:07:23,680 --> 00:07:28,120
You're trying to understand them.

53
00:07:28,120 --> 00:07:37,200
I think it's quite novel, I mean it's actually quite incredible in its simplicity.

54
00:07:37,200 --> 00:07:41,800
When you look at Buddhism as a Buddhist metadata you think, it's amazing that I didn't

55
00:07:41,800 --> 00:07:48,320
see that, it's remarkable how blind we are, how much ignorance we truly have that we can't

56
00:07:48,320 --> 00:07:53,760
see these simple things.

57
00:07:53,760 --> 00:08:03,640
And the simple truth is we suffer because we don't understand, we suffer because we

58
00:08:03,640 --> 00:08:08,240
are ignorant.

59
00:08:08,240 --> 00:08:16,920
I mean you think that you know you're doing something wrong.

60
00:08:16,920 --> 00:08:20,280
We think we know that anger is bad for us, greed is bad for us.

61
00:08:20,280 --> 00:08:26,600
We think we know that we're in the wrong.

62
00:08:26,600 --> 00:08:31,080
We think to ourselves, I know I've got a problem, I know I'm, you know anxiety is bad for

63
00:08:31,080 --> 00:08:37,200
me.

64
00:08:37,200 --> 00:08:42,240
But the truth is that we don't know.

65
00:08:42,240 --> 00:08:52,480
The truth is that we have in our minds an almost unconscious reactivity and a stimulus

66
00:08:52,480 --> 00:08:59,360
arises and we react in our said ways.

67
00:08:59,360 --> 00:09:03,120
And you notice that we actually appear to think that that's the proper way to react.

68
00:09:03,120 --> 00:09:09,160
If someone says something nasty to you, you think it's good to get angry at them, right?

69
00:09:09,160 --> 00:09:17,800
Or if I say something that you think is rude or impolite or if I do something like if

70
00:09:17,800 --> 00:09:29,200
I cut you off in traffic or if I eat the last chocolate or something, if I made loud noise,

71
00:09:29,200 --> 00:09:32,960
we had one meditator here, it was complaining about us using people using the bathroom

72
00:09:32,960 --> 00:09:34,560
at the wrong time.

73
00:09:34,560 --> 00:09:42,240
I mean I think it's good to get angry and I think it's good to get upset about things.

74
00:09:42,240 --> 00:09:48,240
When we reflect we think, I know, I know I shouldn't get angry, but in reality we don't

75
00:09:48,240 --> 00:09:49,800
know.

76
00:09:49,800 --> 00:09:54,480
That's the simplicity of it.

77
00:09:54,480 --> 00:10:01,800
So when we talk about habits, meditation is really divided down this line, are you creating

78
00:10:01,800 --> 00:10:02,800
new habits?

79
00:10:02,800 --> 00:10:07,360
Because if you're trying to fix things, you're just creating a new habit of trying to fix

80
00:10:07,360 --> 00:10:08,360
things.

81
00:10:08,360 --> 00:10:15,160
You're creating the new habit of reacting, it can be quite wholesome, suppose if you

82
00:10:15,160 --> 00:10:20,560
have anger and you counter it with love, it can be quite a good thing.

83
00:10:20,560 --> 00:10:25,280
So good things are formations as well.

84
00:10:25,280 --> 00:10:32,680
Good things will take you to heaven or to the god realms or they'll make you very happy.

85
00:10:32,680 --> 00:10:40,640
They won't solve your problems, they don't get to the root, they don't cut off the ignorance.

86
00:10:40,640 --> 00:10:49,520
They have nothing to do with the cause, they have nothing to do with the problem.

87
00:10:49,520 --> 00:10:58,200
If you want to free yourself from greed, anger and delusion, the only way to do it is to

88
00:10:58,200 --> 00:11:07,400
look and to understand and to see that greed, anger and delusion are bad for you, they're

89
00:11:07,400 --> 00:11:11,760
problems.

90
00:11:11,760 --> 00:11:16,120
The Buddha figured all this out in one night, the Buddha sat there, so why we call him

91
00:11:16,120 --> 00:11:20,280
the Buddha because he figured this all out.

92
00:11:20,280 --> 00:11:28,760
He looked inside himself and he didn't fix himself, he understood himself.

93
00:11:28,760 --> 00:11:33,200
He came to understand how the mind works.

94
00:11:33,200 --> 00:11:38,840
He freed himself from the ignorance, because when you look at your sunkaras, when you look

95
00:11:38,840 --> 00:11:48,680
at how, I suppose you like food and you find it delicious and you like it.

96
00:11:48,680 --> 00:11:58,960
As you watch that, if you're mindful, if you eat as a meditation, you find that that

97
00:11:58,960 --> 00:12:02,280
desire, that attachment, that addiction drops away.

98
00:12:02,280 --> 00:12:05,600
There's no reason for it, there's no benefit to it.

99
00:12:05,600 --> 00:12:12,680
You find it stressful, find it uncomfortable, you realize that it's just going to lead to

100
00:12:12,680 --> 00:12:13,680
more problems.

101
00:12:13,680 --> 00:12:20,320
If you like the food, you're going to eat more of it, you're going to overeat, perhaps.

102
00:12:20,320 --> 00:12:26,000
Or if you don't like the food or you're going to starve yourself, etc., you're going

103
00:12:26,000 --> 00:12:27,000
to suffer.

104
00:12:27,000 --> 00:12:34,520
It's not going to be pleasant, even though you eat the food and you're uncomfortable, or

105
00:12:34,520 --> 00:12:42,640
you hear the noise, or you feel the heat or the cold, all of these things we can't control

106
00:12:42,640 --> 00:12:48,920
this house especially, sometimes it's too hot, sometimes it's too cold, or we try to

107
00:12:48,920 --> 00:12:54,640
adjust it, but in the end you can't fix it, it's going to be hot, sometimes it's going

108
00:12:54,640 --> 00:13:04,080
to be cold sometimes, but the liking and disliking that's up to us.

109
00:13:04,080 --> 00:13:08,160
So the other type of meditation is not about fixing our habits or changing our habits

110
00:13:08,160 --> 00:13:14,080
or building new habits, but understanding our habits.

111
00:13:14,080 --> 00:13:20,560
And through understanding, that's how real change comes about.

112
00:13:20,560 --> 00:13:25,080
Because as you watch, you come to understand.

113
00:13:25,080 --> 00:13:38,840
And part of the understanding is this brilliant light shining down showing you.

114
00:13:38,840 --> 00:13:50,440
Showing you what you're doing wrong, showing you the nature of your habits.

115
00:13:50,440 --> 00:13:55,520
Showing you where you're mixed up, showing you where you're going wrong, and helping

116
00:13:55,520 --> 00:13:59,480
you change.

117
00:13:59,480 --> 00:14:03,040
So this is why the Buddha taught what we call mindfulness.

118
00:14:03,040 --> 00:14:12,680
His idea was just to bring people back to the present moment, bring people back to reality.

119
00:14:12,680 --> 00:14:16,080
Mindfulness is not, it doesn't mean awareness.

120
00:14:16,080 --> 00:14:22,520
This is what comes from mindfulness, but mindfulness is the act, the activity.

121
00:14:22,520 --> 00:14:28,840
Our activity is not to change, it's not to fix, it's to return.

122
00:14:28,840 --> 00:14:37,760
Mindfulness really means to remember or to recollect, it has to do with memory.

123
00:14:37,760 --> 00:14:44,080
When you keep something in mind, when you grasp something properly.

124
00:14:44,080 --> 00:14:49,600
So our ordinary experience is fleeting, we experience something, and then we're gone.

125
00:14:49,600 --> 00:14:54,000
When you have food, you think you know that you're eating, but your knowledge of eating

126
00:14:54,000 --> 00:14:55,280
is only for a moment.

127
00:14:55,280 --> 00:14:59,200
You have the taste in that moment, you're present.

128
00:14:59,200 --> 00:15:02,960
But the next moment we're off thinking about how good it tastes and boy, I should make

129
00:15:02,960 --> 00:15:08,480
this for myself at home and whatever, it's a jumping off point to so many different things

130
00:15:08,480 --> 00:15:17,600
covered in delusion or blanketed by this ignorance, this darkness, that has no clear awareness

131
00:15:17,600 --> 00:15:20,000
of reality.

132
00:15:20,000 --> 00:15:26,560
So mindfulness is what creates this awareness when you remind yourself, hence the use of a mantra,

133
00:15:26,560 --> 00:15:34,440
a mantra is a very common, a very ancient meditation technique, and we apply it regularly,

134
00:15:34,440 --> 00:15:36,480
continuously, repeatedly.

135
00:15:36,480 --> 00:15:46,960
When the goal isn't the mantra itself, the mantra is a tool to cultivate objective awareness.

136
00:15:46,960 --> 00:15:53,080
It forces your mind into a state of clear awareness.

137
00:15:53,080 --> 00:15:59,360
So the habit that we're cultivating is clarity.

138
00:15:59,360 --> 00:16:03,120
When you see something and you say to yourself, seeing, when you're not, it's not magic,

139
00:16:03,120 --> 00:16:09,040
you're reminding yourself, hey, that's not good, that's not bad, that's seeing.

140
00:16:09,040 --> 00:16:15,840
When you have pain, you remind yourself, pain, pain, it's just pain, it allows you to

141
00:16:15,840 --> 00:16:16,840
see it clearly.

142
00:16:16,840 --> 00:16:23,440
And then, when you see yourself reacting, when you're angry, or when you want something,

143
00:16:23,440 --> 00:16:29,200
or when you're confused, or when you're anxious, and you focus on that, and then you

144
00:16:29,200 --> 00:16:34,280
can see that clearly, you say anxious, anxious, not running anymore, not trying to change

145
00:16:34,280 --> 00:16:38,080
even, just trying to understand.

146
00:16:38,080 --> 00:16:46,760
You straighten your mind out so that you see it clearly as it is, not the biased, right?

147
00:16:46,760 --> 00:16:52,440
Our ordinary outlook on reality is biased, that's the problem.

148
00:16:52,440 --> 00:16:57,040
So we can't let our biases guide us this way or that way.

149
00:16:57,040 --> 00:17:05,200
All we have to do is remove our biases and then let the clarity do its thing once you see

150
00:17:05,200 --> 00:17:09,160
clearly, you let go.

151
00:17:09,160 --> 00:17:17,120
Nothing is worth clinging to, this is the bold truth that the Buddha proclaimed, it's

152
00:17:17,120 --> 00:17:21,040
a claim that he made.

153
00:17:21,040 --> 00:17:28,480
So we have this bold claim that if you look clearly, if you really see things as they

154
00:17:28,480 --> 00:17:36,920
are, you won't cling to anything, you'll find there is nothing worth clinging to.

155
00:17:36,920 --> 00:17:44,960
And if that's true, which we claim it is, then that simply is the answer.

156
00:17:44,960 --> 00:17:48,240
You don't have to fix your problems, you don't have to change your life.

157
00:17:48,240 --> 00:17:54,400
All you have to do is look, and once you look, you'll see, once you see, you'll know.

158
00:17:54,400 --> 00:18:02,400
If you want to know, you have to see, if you want to see, you have to look.

159
00:18:02,400 --> 00:18:10,240
And that is where it all begins with the looking.

160
00:18:10,240 --> 00:18:14,160
So there you go.

161
00:18:14,160 --> 00:18:15,880
I'm going to be on here every night.

162
00:18:15,880 --> 00:18:20,800
So I'm not giving long dhamma talks every night.

163
00:18:20,800 --> 00:18:26,600
Just give you a little bit of dhamma, this is the beginning, and every day I'll be back.

164
00:18:26,600 --> 00:18:30,680
Sorry for the technical difficulties, I'm not sure that the internet is actually getting

165
00:18:30,680 --> 00:18:37,760
this, but most important is you here at home, so I hope this is a little bit of encouragement

166
00:18:37,760 --> 00:18:44,360
and appreciate very much what you guys are doing, the work that you're doing here.

167
00:18:44,360 --> 00:18:54,040
We have two French, Canadian, no Quebec, Canadian meditators, and one woman from Romania,

168
00:18:54,040 --> 00:18:57,560
and then Javan is still with us from America.

169
00:18:57,560 --> 00:19:02,080
And downstairs there's another American man who didn't come up today, so we're really

170
00:19:02,080 --> 00:19:03,600
a full house.

171
00:19:03,600 --> 00:19:12,240
Okay, so that's the dhamma talk for tonight, you're all welcome to go back to your meditation.

172
00:19:12,240 --> 00:19:22,520
And those of you on the internet, if you have questions, I can answer a few questions maybe.

173
00:19:22,520 --> 00:19:23,520
Are we doing the questions?

174
00:19:23,520 --> 00:19:25,400
Are we doing them on the site?

175
00:19:25,400 --> 00:19:32,520
Is that what I decided?

176
00:19:32,520 --> 00:19:52,400
We've got a bunch of questions here on the site, so I'll go through these then.

177
00:19:52,400 --> 00:19:56,480
A good friend recently ordained, it was somewhat unexpected, and she opted not to make

178
00:19:56,480 --> 00:20:12,040
a large announcement about it.

179
00:20:12,040 --> 00:20:15,320
If you're trying to practice right speech, what should you do if you have information

180
00:20:15,320 --> 00:20:22,000
that don't feel comfortable sharing it, or maybe even have been asked not to share it?

181
00:20:22,000 --> 00:20:24,560
I don't think it's that detailed, you know.

182
00:20:24,560 --> 00:20:30,600
I mean, that's the point where it gets complicated, and you have to look at your intentions.

183
00:20:30,600 --> 00:20:36,160
It's usually much less, but it's important to understand that that's not really where

184
00:20:36,160 --> 00:20:44,400
right speech deals with, meaning to some extent it is, or I guess in the details it is,

185
00:20:44,400 --> 00:20:47,600
but you don't want to overcomplicate it.

186
00:20:47,600 --> 00:20:55,600
Or wrong speech is if you outright lie, knowingly, or you try to, your intention in saying

187
00:20:55,600 --> 00:21:02,360
something is to divide people, or to hurt someone, or if it's just speech that is useless.

188
00:21:02,360 --> 00:21:07,120
I mean, useless speech, of course, is the most difficult, but to overcome, but it's still

189
00:21:07,120 --> 00:21:09,080
wrong speech.

190
00:21:09,080 --> 00:21:14,480
So with the speech you're talking about, I mean, it gets more, I mean, a lot of it is

191
00:21:14,480 --> 00:21:20,480
just functional wrong speech, right, like if I say something to someone with good intentions

192
00:21:20,480 --> 00:21:27,080
or pure intentions, then, and yet it makes someone else angry, like, hey, why did you

193
00:21:27,080 --> 00:21:32,560
tell that person that it may not be wrong speech at all?

194
00:21:32,560 --> 00:21:38,400
I think there are certain instances where the speech wasn't wrong, but you were unmindful

195
00:21:38,400 --> 00:21:42,160
in the thoughts that led you to the speech, and because of your lack of mindfulness you

196
00:21:42,160 --> 00:21:49,920
didn't consider the consequences of your actions, and that's wrong, right?

197
00:21:49,920 --> 00:21:55,880
So I mean, if you're in Iran, you're much less likely to say the wrong thing, but you

198
00:21:55,880 --> 00:22:00,400
can still can, and our hand still might say something to make someone upset.

199
00:22:00,400 --> 00:22:05,080
I think that's unavoidable, right?

200
00:22:05,080 --> 00:22:09,760
So the wrongness of it is purely functional, and then our hand would probably apologize

201
00:22:09,760 --> 00:22:16,200
for saying the wrong thing, if they had no bad intention, that's the point.

202
00:22:16,200 --> 00:22:21,320
So when it comes to things like there's questions about keeping promises, if you make

203
00:22:21,320 --> 00:22:24,520
a promise to someone and then you break it, is that immoral?

204
00:22:24,520 --> 00:22:27,600
Well, no, not really in the Buddhist sense.

205
00:22:27,600 --> 00:22:32,200
I would say, I mean, there's a question about that, but my thought on it is, no, it's not,

206
00:22:32,200 --> 00:22:42,120
because at the time, you're really intended to carry out that promise, but circumstance

207
00:22:42,120 --> 00:22:49,000
has changed, and maybe you even realize something afterwards, oh, I can't keep that promise.

208
00:22:49,000 --> 00:22:55,000
That wasn't in your mind at the time.

209
00:22:55,000 --> 00:23:01,720
And as far as complex examples, such as this one, where you're not sure whether you should

210
00:23:01,720 --> 00:23:07,720
disclose other people's personal information, I would say it's much more functional than

211
00:23:07,720 --> 00:23:14,600
actually immoral or immoral, you're talking about something that mindfulness will help give

212
00:23:14,600 --> 00:23:19,480
you a sort of a sense of what's right and what's wrong, but it's not going to solve the

213
00:23:19,480 --> 00:23:21,640
problem at all times.

214
00:23:21,640 --> 00:23:26,160
So I mean, I think the answer to your question here is just be mindful.

215
00:23:26,160 --> 00:23:31,160
Be mindful, and as with many cases like this, not just about speech, but about action as

216
00:23:31,160 --> 00:23:36,840
well, don't expect to always have the right answer, expect to be wrong sometimes.

217
00:23:36,840 --> 00:23:49,640
I mean, we're, we live and learn, even our hands continue to learn in these instances.

218
00:23:49,640 --> 00:23:54,320
Struggling to combine meditation practices with regular life when talking about a field that

219
00:23:54,320 --> 00:24:11,360
requires a lot of suffering, studying, right?

220
00:24:11,360 --> 00:24:16,400
So the idea that it gets in the way of your ambitions, it gets in the way of studying

221
00:24:16,400 --> 00:24:22,120
and so on, and I think that's fair, but I think you can compartmentalize, right?

222
00:24:22,120 --> 00:24:26,880
And meditation is a habit, so it's something you have to keep up and do regularly or

223
00:24:26,880 --> 00:24:27,880
you lose it.

224
00:24:27,880 --> 00:24:33,520
It starts to fade, but it doesn't mean you have to do it all the time, ideally, that

225
00:24:33,520 --> 00:24:34,520
would be great.

226
00:24:34,520 --> 00:24:39,160
But when you do other things, you're just taking a break and it's, the commentary says

227
00:24:39,160 --> 00:24:44,600
it's like you, you put your bags down or, or, or some maybe it was a modern monk who said

228
00:24:44,600 --> 00:24:47,800
you put your bags down and then you pick them up.

229
00:24:47,800 --> 00:24:53,640
I think, I think the set the botana commentary says something like that, like you're carrying

230
00:24:53,640 --> 00:24:57,600
a bag and you put it down.

231
00:24:57,600 --> 00:25:02,720
Put it down, just make sure you go back and pick it up again.

232
00:25:02,720 --> 00:25:08,520
So when you're studying or so on, yes, maybe you can't be mindful or not very mindful.

233
00:25:08,520 --> 00:25:15,920
There are ways, but they consider it a break and just be conscious in your mind that you're

234
00:25:15,920 --> 00:25:17,720
going to go back to it.

235
00:25:17,720 --> 00:25:23,640
As far as ambitions, well, yes, I mean, the reason that's happening is because all of

236
00:25:23,640 --> 00:25:24,840
these things are useless.

237
00:25:24,840 --> 00:25:28,880
It's useless to do pretty much anything in the world.

238
00:25:28,880 --> 00:25:33,240
There's not much use to most of the things that we engage in.

239
00:25:33,240 --> 00:25:36,520
And as you start to see that, you start to give it up.

240
00:25:36,520 --> 00:25:41,520
But the point is it's not, it's not the brainwashing, it's just learning that they're

241
00:25:41,520 --> 00:25:47,160
useless and if they're useless, you know, to worry about stopping to do them, you should

242
00:25:47,160 --> 00:25:50,240
be happy that you're stopping to do useless things.

243
00:25:50,240 --> 00:25:52,840
Beyond that, I can't give much advice.

244
00:25:52,840 --> 00:25:57,320
I know most people that puts them in quite a con, quandary because they made lives of doing

245
00:25:57,320 --> 00:26:01,720
useless things or things that Buddhism would consider useless or things that you've

246
00:26:01,720 --> 00:26:06,960
come to realize through meditation are not as useful as you thought to struggle that

247
00:26:06,960 --> 00:26:13,360
we all go through and eventually, hopefully, you work it out and you give up the things

248
00:26:13,360 --> 00:26:17,600
that are useless.

249
00:26:17,600 --> 00:26:21,040
Sometimes I feel there's too much going on at once and so I'm not sure which things to

250
00:26:21,040 --> 00:26:27,640
note, which you do in such a circumstance, just note whatever's clearest.

251
00:26:27,640 --> 00:26:28,640
It doesn't really matter.

252
00:26:28,640 --> 00:26:29,640
It's not magic.

253
00:26:29,640 --> 00:26:36,280
They have to catch everything, find something that's clear and be mindful of them.

254
00:26:36,280 --> 00:26:40,640
One must observe the thought itself rather than getting lost in the content.

255
00:26:40,640 --> 00:26:47,160
Never, I noticed the mind thinking that thought usually stops, so I don't think it's possible

256
00:26:47,160 --> 00:26:49,320
to observe it.

257
00:26:49,320 --> 00:26:57,840
Yes, I mean, noting is always right after, so maybe that language is imprecise that it's

258
00:26:57,840 --> 00:27:02,120
we're not actually observing it, we're reminding ourselves, hey, that was just a thought

259
00:27:02,120 --> 00:27:03,800
so that you don't get caught up in it.

260
00:27:03,800 --> 00:27:09,160
The fact that it's the moment after is not consequential, it's fine, I mean, it's the way

261
00:27:09,160 --> 00:27:13,920
it's supposed to be.

262
00:27:13,920 --> 00:27:17,760
Sending the mind out to each moment of rising and falling rather than repeating the

263
00:27:17,760 --> 00:27:21,680
man during the head of the mouth, foreigner people, the idea of sending the mind anywhere

264
00:27:21,680 --> 00:27:26,760
outside the head is a very foreign concept, my own meditation is very foreign, the mind

265
00:27:26,760 --> 00:27:33,040
seems to reside in the brain, well, you're just going to have to let go of that idea,

266
00:27:33,040 --> 00:27:39,480
then to mind out to the stomach, maybe watch, when that happens, you might want to watch

267
00:27:39,480 --> 00:27:49,160
your emotions, whether you're confused or frustrated or doubting or so on, but eventually

268
00:27:49,160 --> 00:27:54,160
you just have to give up all that analytical thought and just send the mind out to the

269
00:27:54,160 --> 00:28:02,480
stomach, let the Buddha say about love and how we should act.

270
00:28:02,480 --> 00:28:07,000
And we act with love or should remindfulness always be put before love, did the Buddha

271
00:28:07,000 --> 00:28:08,000
love?

272
00:28:08,000 --> 00:28:15,440
Yes, the Buddha had experiences of love.

273
00:28:15,440 --> 00:28:23,480
So it's kind of important to separate the mindfulness practice and mindfulness itself.

274
00:28:23,480 --> 00:28:29,200
I mean, technically mindfulness is in every wholesome consciousness, I think.

275
00:28:29,200 --> 00:28:36,800
What we're trying to do is cultivate a specific type of mindfulness or repeated mindfulness

276
00:28:36,800 --> 00:28:46,040
or many qualities that are associated with mindfulness that aren't in all mind states.

277
00:28:46,040 --> 00:28:49,040
But for an hour a hunt, for example, they don't have to do that.

278
00:28:49,040 --> 00:28:53,720
When they have love, there's a clarity of mind, I mean, love is a wholesome mind state

279
00:28:53,720 --> 00:28:57,280
in itself, so there's not a problem there.

280
00:28:57,280 --> 00:29:00,440
Should we cultivate it?

281
00:29:00,440 --> 00:29:03,200
It love itself isn't going to free you from suffering.

282
00:29:03,200 --> 00:29:08,320
I mean, the Buddha had it as an outpouring of as a habit, right?

283
00:29:08,320 --> 00:29:14,760
And we all have it to a certain extent as a habit and cultivating it will support the

284
00:29:14,760 --> 00:29:18,320
practice of insight.

285
00:29:18,320 --> 00:29:24,720
So that's the place that it takes as far as should you, it's maybe a little more complicated

286
00:29:24,720 --> 00:29:30,040
at something supportive, so there may be times where you should, I mean, there's no answer

287
00:29:30,040 --> 00:29:36,200
like, yes, everyone should practice it at this time or that time.

288
00:29:36,200 --> 00:29:41,680
But it can be useful and it should be understood as positive as beneficial, but it should

289
00:29:41,680 --> 00:29:49,880
be also understood as not freeing one from suffering.

290
00:29:49,880 --> 00:29:51,840
Not completely, right?

291
00:29:51,840 --> 00:29:53,920
It's only a support.

292
00:29:53,920 --> 00:29:58,720
The monastics engage in any form of media entertainment.

293
00:29:58,720 --> 00:30:03,360
Not foreign entertainment purposes, but some things that could be considered entertainment

294
00:30:03,360 --> 00:30:09,480
could also be considered information, so there's a gray area to some extent, or I mean,

295
00:30:09,480 --> 00:30:17,360
it's much more about your intentions and so on, because entertainment and information often

296
00:30:17,360 --> 00:30:23,840
come together, so yeah, so you just have to be discreet and on, not discreet to

297
00:30:23,840 --> 00:30:33,720
be, I don't know what the word is, you have to be careful.

298
00:30:33,720 --> 00:30:37,560
And what way can karma impact those are incapable of feeling remorse?

299
00:30:37,560 --> 00:30:41,280
I think no one is really incapable of feeling remorse.

300
00:30:41,280 --> 00:30:47,040
Those people who feel very little remorse are in a very bad way.

301
00:30:47,040 --> 00:30:53,760
That means they've gotten themselves in such a bad way that they no longer are in tune

302
00:30:53,760 --> 00:30:58,800
with reality hardly at all, but I would say it's not incapable.

303
00:30:58,800 --> 00:31:04,560
There may be, I mean, there may be a certain type of person who, for some time, or even

304
00:31:04,560 --> 00:31:08,360
for this life, it's incapable, but I think it's much more rare.

305
00:31:08,360 --> 00:31:13,680
I think those people who we think are incapable are just very much out of tune with it,

306
00:31:13,680 --> 00:31:17,880
it's going to take them a long time to really even be aware that they are suffering or

307
00:31:17,880 --> 00:31:24,160
that they are hurting themselves.

308
00:31:24,160 --> 00:31:28,840
But you know, they're in such a bad way that it's the worst sort of state and then their

309
00:31:28,840 --> 00:31:41,000
karma that they're the results of that are going to be generally horrific.

310
00:31:41,000 --> 00:31:43,880
When the sessernens no sound is heard, that's a technical issue.

311
00:31:43,880 --> 00:31:47,760
You might want to bring it up with the webmaster, the people running this.

312
00:31:47,760 --> 00:31:52,120
I think there's an email address you can contact.

313
00:31:52,120 --> 00:31:53,120
Call the yoga.

314
00:31:53,120 --> 00:31:54,120
I don't know what that is.

315
00:31:54,120 --> 00:31:55,120
I think it's a Tibetan thing.

316
00:31:55,120 --> 00:31:59,760
I'm not really clear on it.

317
00:31:59,760 --> 00:32:01,800
Okay.

318
00:32:01,800 --> 00:32:11,800
So, if you are trying to do meditation, this site is only for when you're actually doing

319
00:32:11,800 --> 00:32:12,800
it.

320
00:32:12,800 --> 00:32:18,560
You can always log it later if you want, but it's supposed to be ideally for when you're

321
00:32:18,560 --> 00:32:21,800
actually doing it.

322
00:32:21,800 --> 00:32:30,040
Now, we're doing five more questions.

323
00:32:30,040 --> 00:32:33,760
The Buddha completely denied the presence of self, or did he say that it is an unskill

324
00:32:33,760 --> 00:32:39,760
phone or it's a question since the presence?

325
00:32:39,760 --> 00:32:44,520
He didn't ever completely deny the presence of self.

326
00:32:44,520 --> 00:32:49,320
And I think less for it doesn't really have anything to do with the idea that he thought

327
00:32:49,320 --> 00:32:51,040
there was a self.

328
00:32:51,040 --> 00:32:57,560
It's for the fact that it's a sort of, you know, the idea.

329
00:32:57,560 --> 00:33:04,760
It confuses people when you say there is no self, because, well, they think of themselves

330
00:33:04,760 --> 00:33:06,600
as an individual, right?

331
00:33:06,600 --> 00:33:11,920
It's a confusing question, but self isn't the sort of thing that can exist.

332
00:33:11,920 --> 00:33:25,120
Self is what we come to see is that reality is non-self or experiences or non-self.

333
00:33:25,120 --> 00:33:30,640
And the problem with saying there is no self is that it's not an experiential answer or

334
00:33:30,640 --> 00:33:34,400
an experiential statement, right?

335
00:33:34,400 --> 00:33:37,440
Reality is not from a Buddhist perspective made up of entities.

336
00:33:37,440 --> 00:33:42,280
So the idea that some things should or shouldn't exist is in the wrong context.

337
00:33:42,280 --> 00:33:47,200
In the context of experience, it's not things, it's experience.

338
00:33:47,200 --> 00:33:49,280
And experience is not self.

339
00:33:49,280 --> 00:33:50,280
It's not entities.

340
00:33:50,280 --> 00:33:58,920
It's about getting away from the idea of entities entirely.

341
00:33:58,920 --> 00:34:04,360
When I arrive at access concentration, I get scared, I have new advice.

342
00:34:04,360 --> 00:34:08,520
Well, it sounds like you might be practicing a different type of meditation, because when

343
00:34:08,520 --> 00:34:12,600
you're scared, an artificial just say scared and let it go.

344
00:34:12,600 --> 00:34:22,520
We're not interested in getting into transism and so on.

345
00:34:22,520 --> 00:34:27,120
I get a warm feeling like a peace blanket.

346
00:34:27,120 --> 00:34:36,280
Can't seem to replicate that feeling, so there's a method to provoke or help this feeling.

347
00:34:36,280 --> 00:34:39,440
Yes, no, that's not our goal.

348
00:34:39,440 --> 00:34:43,600
You might want to read my booklet on how to meditate, might help you understand the sort

349
00:34:43,600 --> 00:34:49,280
of meditation that we practice.

350
00:34:49,280 --> 00:34:52,400
The point being that you're suffering because you're trying and what you're seeing is

351
00:34:52,400 --> 00:34:53,400
non-self.

352
00:34:53,400 --> 00:34:58,680
This is the idea that you can't control, you're not in charge, you, these things don't

353
00:34:58,680 --> 00:35:00,560
belong to you.

354
00:35:00,560 --> 00:35:04,560
And so clinging to them like you are clinging to it is just a cause for suffering.

355
00:35:04,560 --> 00:35:09,240
You're inevitably going to suffer if you have that sort of attitude, that sort of outlook

356
00:35:09,240 --> 00:35:10,720
or that sort of goal.

357
00:35:10,720 --> 00:35:16,360
On the other hand, you know, it is true, it's a reason that brings people to meditate

358
00:35:16,360 --> 00:35:17,360
in the beginning.

359
00:35:17,360 --> 00:35:24,440
So, in the interim, it can be useful to make people want to meditate as they see that

360
00:35:24,440 --> 00:35:28,680
there are states that they've never experienced before.

361
00:35:28,680 --> 00:35:30,880
But in the end, you have to let go of it.

362
00:35:30,880 --> 00:35:38,240
I do longer sessions, I faint, do you recommend to adjust posture, certain for straighten

363
00:35:38,240 --> 00:35:48,280
the back, yeah, no, you won't straighten the back, you're, you know, you might get a little

364
00:35:48,280 --> 00:35:56,320
bit slouched over time, but it's not going to hurt your back, not according to Dr.

365
00:35:56,320 --> 00:36:03,320
Me, I mean, my teacher has a slouched back and he slouched as when he sits, he's 90 something,

366
00:36:03,320 --> 00:36:15,000
but he's been sitting in meditation for 70, 80 years when his back's fine.

367
00:36:15,000 --> 00:36:16,160
Because you're relaxed, right?

368
00:36:16,160 --> 00:36:21,960
I mean, I think a big part of what strains the back and does a real damage to your body

369
00:36:21,960 --> 00:36:27,040
in general is the stress, the mental stress, the tension, tension will tie you in knots

370
00:36:27,040 --> 00:36:30,600
and will really warp you and cause a lot of suffering.

371
00:36:30,600 --> 00:36:35,080
But when you relax, your legs go down, you're able to sit across, leg it even in the

372
00:36:35,080 --> 00:36:46,640
lotus position quite easily, your back relaxes so you don't have back aches and so on.

373
00:36:46,640 --> 00:36:51,640
I feel like inhale fully while meditating is this or breath, is this or breath breathing

374
00:36:51,640 --> 00:36:53,680
a normal rate.

375
00:36:53,680 --> 00:37:02,200
I don't understand, but I think if I understand what you're saying, no, we try to just

376
00:37:02,200 --> 00:37:05,040
watch the breath, not control it, not change it.

377
00:37:05,040 --> 00:37:08,840
If you notice it a different, then you just note that you're saying, knowing, or feeling

378
00:37:08,840 --> 00:37:37,800
feeling, but we're not actively trying to control or condition the breath, but we've

379
00:37:37,800 --> 00:38:04,280
got someone really upset at me.

380
00:38:04,280 --> 00:38:12,280
Whatever, sorry, why am I not, here's a good question, why am I not answering new questions

381
00:38:12,280 --> 00:38:13,280
on YouTube?

382
00:38:13,280 --> 00:38:20,520
Well, because the YouTube audience tends to be a little bit rough and unfiltered if you're

383
00:38:20,520 --> 00:38:24,400
not able to take anyone can sign up on our website, if you're not willing to take the

384
00:38:24,400 --> 00:38:35,800
time to get a little bit more engaged and committed, then sorry, I could answer questions

385
00:38:35,800 --> 00:38:42,720
to the cows came home and there's still be more questions to answer, so yeah, I limit

386
00:38:42,720 --> 00:38:43,720
them.

387
00:38:43,720 --> 00:38:44,720
If you don't like it, that's fine.

388
00:38:44,720 --> 00:38:48,880
I mean, I'm not perfect and got lots of people who really don't like me or don't like

389
00:38:48,880 --> 00:38:55,320
what I do and that's fine too, wish you all the best, but no, I'm not answering questions

390
00:38:55,320 --> 00:38:56,320
on YouTube.

391
00:38:56,320 --> 00:39:02,240
You do have to go to our site, sorry about that, but hey, I'm not getting paid, you're

392
00:39:02,240 --> 00:39:06,120
not paying me, so you don't like it, go somewhere else.

393
00:39:06,120 --> 00:39:22,720
Okay, that's all for tonight, thank you all for tuning in, have a good night.

