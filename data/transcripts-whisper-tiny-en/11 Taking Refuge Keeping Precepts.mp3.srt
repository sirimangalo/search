1
00:00:00,000 --> 00:00:07,280
So today is the second part of the spiritual path.

2
00:00:07,280 --> 00:00:21,680
Now the first thing, the follower of the Buddha do is take, take precepts.

3
00:00:21,680 --> 00:00:34,680
So when a person takes precepts, he becomes a follower of the Buddha, or he becomes a Buddhist,

4
00:00:34,680 --> 00:00:44,780
and a person who follows the Buddha simply takes refuge in the Buddha, the Divine, the

5
00:00:44,780 --> 00:00:56,080
Buddha, and so when a person takes refuge in the three gems, he becomes an Upa-saka.

6
00:00:56,080 --> 00:01:11,040
If he is a man, he becomes an Upa-saka, and if he is a woman, he is called Upa-sika.

7
00:01:11,040 --> 00:01:20,640
And Upa-saka or Upa-sika has something to follow or to practice.

8
00:01:20,640 --> 00:01:31,240
So as the follower of the Buddha, a person who takes refuge in the three gems is expected

9
00:01:31,240 --> 00:01:40,240
to practice the teachings of the Buddha.

10
00:01:40,240 --> 00:01:58,400
And Buddha gives his followers a way of practice based on three stages, morality or

11
00:01:58,400 --> 00:02:05,920
seala, concentration or samadhi and wisdom or panya.

12
00:02:05,920 --> 00:02:14,560
So these three steps are to be taken one by one, and they cannot be skipped.

13
00:02:14,560 --> 00:02:22,920
So one must take the first step, first and then second step and that step.

14
00:02:22,920 --> 00:02:35,280
The first step in this spiritual path to eradication of mental defilements is seala, motor

15
00:02:35,280 --> 00:02:39,160
conduct or virtue.

16
00:02:39,160 --> 00:02:53,040
So an Upa-saka or Upa-sika is expected to possess pure seala or pure motor conduct.

17
00:02:53,040 --> 00:03:05,760
And once the Buddha said, by how much is an Upa-saka virtuous or an Upa-saka becomes morally

18
00:03:05,760 --> 00:03:10,240
disciplined.

19
00:03:10,240 --> 00:03:16,120
And Buddha said, when an Upa-saka abstains from destruction of life, from taking what is

20
00:03:16,120 --> 00:03:26,240
not given, from secular misconduct, from false speech and from taking intoxicants, by

21
00:03:26,240 --> 00:03:39,400
this much is an Upa-saka virtuous or an Upa-saka becomes morally clean person.

22
00:03:39,400 --> 00:03:55,080
So an Upa-saka or Upa-sika is expected to keep these five precepts.

23
00:03:55,080 --> 00:04:08,400
Before we understand the five precepts, we must first understand what seala means.

24
00:04:08,400 --> 00:04:17,440
Seala is translated as motor conduct, virtue, but we should understand how the word is

25
00:04:17,440 --> 00:04:21,400
defined in the ancient commentaries.

26
00:04:21,400 --> 00:04:31,000
So seala is defined as non-diffusion of bodily actions, etc.

27
00:04:31,000 --> 00:04:41,160
Now when people break these rules, when people kill steel and so on, the bodily actions

28
00:04:41,160 --> 00:04:45,480
and verbal actions are said to be scattered.

29
00:04:45,480 --> 00:04:55,520
Now seala keeps these actions, bodily actions and verbal actions together or coordinated.

30
00:04:55,520 --> 00:05:03,120
So they are not scattered when the person keeps seala and abstains from killing living

31
00:05:03,120 --> 00:05:05,120
beings and others.

32
00:05:05,120 --> 00:05:17,240
So seala is defined as non-diffusion or non-scattering of bodily actions, etc.

33
00:05:17,240 --> 00:05:30,080
Seala is also defined as a basis or a receptacle, a basis for what, a receptacle for what,

34
00:05:30,080 --> 00:05:36,600
a basis or a receptacle for wholesome states, kusala, dhammas.

35
00:05:36,600 --> 00:05:46,240
So long as we keep seala, we are holding the wholesome states in our minds.

36
00:05:46,240 --> 00:05:50,680
And so seala is like a receptacle of wholesome states.

37
00:05:50,680 --> 00:05:56,240
The moment we break our seala, we lose the wholesome states.

38
00:05:56,240 --> 00:06:12,680
So seala is defined as a basis or a receptacle for wholesome states and what is the

39
00:06:12,680 --> 00:06:14,960
function of seala.

40
00:06:14,960 --> 00:06:26,880
The function of seala is described as to destroy unvirtuousness or to destroy misconduct.

41
00:06:26,880 --> 00:06:37,400
When a person keeps seala, he is destroying or actually avoiding unvirtuousness or avoiding

42
00:06:37,400 --> 00:06:38,800
misconduct.

43
00:06:38,800 --> 00:06:50,080
So to destroy means to avoid, not let them not let unvirtuousness or misconduct happen.

44
00:06:50,080 --> 00:07:02,880
So the function of seala is to destroy or to do away with misconduct or unvirtuousness.

45
00:07:02,880 --> 00:07:11,760
We can describe it in another way and that is, the function of seala is to restrain or

46
00:07:11,760 --> 00:07:17,040
to control bodily and verbal actions.

47
00:07:17,040 --> 00:07:24,880
So long as we keep seala, our bodily actions are controlled and our verbal actions are controlled,

48
00:07:24,880 --> 00:07:28,520
our speech is controlled.

49
00:07:28,520 --> 00:07:44,480
What means we do not do what is wrong by bodily actions or by speech.

50
00:07:44,480 --> 00:07:52,960
And it is important to note here that seala is the control of bodily and verbal actions

51
00:07:52,960 --> 00:07:55,840
and not of mind.

52
00:07:55,840 --> 00:08:04,280
Now mind is involved in the control of bodily actions and verbal actions because without

53
00:08:04,280 --> 00:08:07,000
mind you cannot control.

54
00:08:07,000 --> 00:08:21,920
But what is meant here is seala is involved only with bodily actions and verbal actions.

55
00:08:21,920 --> 00:08:34,160
That means even though you may say kill in your mind some living being, so long as you

56
00:08:34,160 --> 00:08:46,840
do not kill by your bodily actions or by asking another to kill, your seala is intact.

57
00:08:46,840 --> 00:08:50,160
You do not break the precept.

58
00:08:50,160 --> 00:08:58,320
So by thinking only, you are not breaking any precepts.

59
00:08:58,320 --> 00:09:07,680
Only when you do something or when you see something, do you break the precept.

60
00:09:07,680 --> 00:09:18,760
I hope you remember the three levels of mental departments, the late and level coming up

61
00:09:18,760 --> 00:09:23,800
or coming to the surface level and transgression level.

62
00:09:23,800 --> 00:09:35,480
So among these three, the target of seala is defilements at transgression level.

63
00:09:35,480 --> 00:09:41,760
That means if you keep seala, you do not transgress anything.

64
00:09:41,760 --> 00:09:47,320
You do not break any of the moral precepts.

65
00:09:47,320 --> 00:09:58,040
And so seala is for avoiding or for destroying or for getting rid of defilements, mental

66
00:09:58,040 --> 00:10:10,160
defilements at transgression level.

67
00:10:10,160 --> 00:10:19,760
And this seala is described in many ways and for leave you will, there are different kinds

68
00:10:19,760 --> 00:10:23,440
of sealas that could keep.

69
00:10:23,440 --> 00:10:33,880
So there are five precepts or H-upa setup research or H precepts with livelihood as the

70
00:10:33,880 --> 00:10:39,720
eight and ten precepts.

71
00:10:39,720 --> 00:10:44,320
Now the first of them are the five precepts.

72
00:10:44,320 --> 00:10:51,240
So you all know the five precepts as actually you have been feeding these five precepts

73
00:10:51,240 --> 00:10:53,080
for many years.

74
00:10:53,080 --> 00:11:03,600
And these five precepts are intended to be kept by lay people all the time.

75
00:11:03,600 --> 00:11:15,600
These precepts are called the precepts of the people of Kuru country or the weighty or

76
00:11:15,600 --> 00:11:17,600
revered practice.

77
00:11:17,600 --> 00:11:28,720
In the Jataka stories, there is one Jataka number 276 where 11 people kept the five precepts

78
00:11:28,720 --> 00:11:35,480
to the utmost, it is very inspiring to read that story, I am sorry I cannot tell you

79
00:11:35,480 --> 00:11:38,040
this story tonight.

80
00:11:38,040 --> 00:11:48,600
So if you are interested you may read the Jataka, Jataka number 276.

81
00:11:48,600 --> 00:12:02,840
So it is said that in India that is a place called Kuru, it is somewhere around New Delhi

82
00:12:02,840 --> 00:12:04,720
in modern India.

83
00:12:04,720 --> 00:12:16,320
So people of that country kept these five precepts and so the five precepts are known as

84
00:12:16,320 --> 00:12:26,960
the precepts of Kuru country and also since they practice with reverence, they practice

85
00:12:26,960 --> 00:12:38,000
it seriously, the five precepts are also called weighty or revered practice.

86
00:12:38,000 --> 00:12:46,440
And these five precepts are described as permanent sealer that is to be kept permanently

87
00:12:46,440 --> 00:13:01,440
and also they are called natural sealer, they come naturally to all beings.

88
00:13:01,440 --> 00:13:14,920
And it is said that in Buddhist cosmology there are four great continents east, south,

89
00:13:14,920 --> 00:13:26,560
west and not continents and the inhabitants of not continent keep these five precepts

90
00:13:26,560 --> 00:13:37,480
by nature, so they do not have to make effort to keep the five precepts.

91
00:13:37,480 --> 00:13:43,040
So these five precepts come to them naturally and so they are called the five precepts

92
00:13:43,040 --> 00:13:51,040
are called naturally established sealer for them.

93
00:13:51,040 --> 00:13:59,280
And bhamadras is saying that you must keep the five precepts as you keep the lower garment

94
00:13:59,280 --> 00:14:06,320
always on your body.

95
00:14:06,320 --> 00:14:16,760
And these five precepts are never broken by those who have gained enlightenment.

96
00:14:16,760 --> 00:14:26,560
So those who have become Sotabanas, Sagadagamis, Anagamis and Arathans never break these

97
00:14:26,560 --> 00:14:32,240
five moral precepts even in another life.

98
00:14:32,240 --> 00:14:38,440
That means suppose a person becomes the Sotabana in this life and then he dies and he is

99
00:14:38,440 --> 00:14:43,440
reborn as a human being in another existence.

100
00:14:43,440 --> 00:14:52,680
So even in that existence, even if he does not know about his being a Sotabana, he just

101
00:14:52,680 --> 00:15:03,320
naturally keeps these five precepts, so he will never break any one of these precepts.

102
00:15:03,320 --> 00:15:12,320
So the five precepts are always kept by those who have gained enlightenment.

103
00:15:12,320 --> 00:15:20,600
So we can understand that if one is capable of breaking any one of these rules, one is

104
00:15:20,600 --> 00:15:26,000
not an enlightened person.

105
00:15:26,000 --> 00:15:32,280
So you all know the five precepts, abstentions from killing, stealing, sex, or misconduct,

106
00:15:32,280 --> 00:15:36,080
lying and taking intoxicants.

107
00:15:36,080 --> 00:15:46,800
Now with each precept, there are what are called factors or constituents or components.

108
00:15:46,800 --> 00:16:04,840
That means some conditions that the fulfillment of which makes one break the precepts.

109
00:16:04,840 --> 00:16:10,120
That means if you force these four conditions, then you break this precept.

110
00:16:10,120 --> 00:16:16,920
If anyone of the factors are missing, then you do not break the precept.

111
00:16:16,920 --> 00:16:23,800
So they they they they are called factors or constituents or components.

112
00:16:23,800 --> 00:16:36,360
And we should understand these these factors, the factors of the first precept abstentions

113
00:16:36,360 --> 00:16:39,200
from killing.

114
00:16:39,200 --> 00:16:42,000
One is living being.

115
00:16:42,000 --> 00:16:52,320
That means the one that is killed must be a living being and to knowing that it is a living

116
00:16:52,320 --> 00:16:53,320
being.

117
00:16:53,320 --> 00:16:58,280
So if you do not know that it is a living being and you happen to kill it and you do not

118
00:16:58,280 --> 00:17:00,200
break this rule.

119
00:17:00,200 --> 00:17:07,160
So you must know that this is a living being and the that is the thought of killing.

120
00:17:07,160 --> 00:17:11,200
That means the intention to kill.

121
00:17:11,200 --> 00:17:19,120
Sometimes and intentionally you may you may kill an insect.

122
00:17:19,120 --> 00:17:24,680
Sometimes you may step on an insect without knowing it.

123
00:17:24,680 --> 00:17:28,720
In that case you do not break that reset.

124
00:17:28,720 --> 00:17:37,080
So the thought of killing means the intention to kill and then the fourth one is the effort.

125
00:17:37,080 --> 00:17:38,800
You make an effort to kill.

126
00:17:38,800 --> 00:17:47,680
So the act of killing and then death of the being by that effort and that being must

127
00:17:47,680 --> 00:17:48,680
die.

128
00:17:48,680 --> 00:17:59,560
So if these five conditions are fulfilled then you break that that reset.

129
00:17:59,560 --> 00:18:13,280
In the commentaries regarding killing and also stealing the means of doing this act are

130
00:18:13,280 --> 00:18:14,600
mentioned.

131
00:18:14,600 --> 00:18:24,520
Again you can say you can kill a living being in how many ways.

132
00:18:24,520 --> 00:18:33,160
Now there are means many means to kill a living being.

133
00:18:33,160 --> 00:18:38,720
The first one is killing by yourself.

134
00:18:38,720 --> 00:18:45,320
It is in in Pali it is for killing with your own hands so that that means killing by yourself

135
00:18:45,320 --> 00:18:53,920
and then the second is killing by command you give command to another person to kill and

136
00:18:53,920 --> 00:19:07,360
also killing by somebody you can throw that means killing by missiles or arrows or bullets

137
00:19:07,360 --> 00:19:12,960
or guns so that is also killing.

138
00:19:12,960 --> 00:19:25,760
And also killing by stationary things that means setting traps and others and then killing

139
00:19:25,760 --> 00:19:34,680
by magical means and killing by psychic power.

140
00:19:34,680 --> 00:19:42,360
So there are many ways of killing and all these killing are included in this precept.

141
00:19:42,360 --> 00:19:45,840
So if you kill yourself you break this rule.

142
00:19:45,840 --> 00:19:51,160
If you are someone to kill and that that button kills it then you break the precept and

143
00:19:51,160 --> 00:19:54,480
so on.

144
00:19:54,480 --> 00:20:01,560
For the second precept abstentions from taking what is not given freely by the owner.

145
00:20:01,560 --> 00:20:08,840
Now there are how many five conditions or five factors and the first one is another property.

146
00:20:08,840 --> 00:20:19,960
So the thing must be another the property of another person and no in the second is knowing

147
00:20:19,960 --> 00:20:22,200
that it is another property.

148
00:20:22,200 --> 00:20:28,280
So when it must ensue let say when you steal and you must know that this is the property

149
00:20:28,280 --> 00:20:35,680
of another person that means belong to another person and then that that is the thought

150
00:20:35,680 --> 00:20:41,360
of stealing that is you you want to steal you have the intention to steal and the effort

151
00:20:41,360 --> 00:20:48,360
you make the effort and taking the property by that effort that means the removing of that

152
00:20:48,360 --> 00:20:51,000
property or something like that.

153
00:20:51,000 --> 00:21:01,560
So when these five conditions are fulfilled then the second precept is broken.

154
00:21:01,560 --> 00:21:10,800
Here also stealing by oneself giving command to another to steal and so on they are also

155
00:21:10,800 --> 00:21:23,720
involved here and the third the third reset abstentions from cyclone misconduct.

156
00:21:23,720 --> 00:21:40,360
Now there are four factors and the first is the forbidden object that means what about

157
00:21:40,360 --> 00:21:51,000
the inviolable person that means the person with whom you must not have sex so such a

158
00:21:51,000 --> 00:22:02,240
person is called the forbidden object and then the second factor is the thought of enjoying

159
00:22:02,240 --> 00:22:08,920
it you want to enjoy this relation and then the third is the effort to enjoy it you

160
00:22:08,920 --> 00:22:17,320
make the effort and the fourth is tolerating the engaging of one organ with another that

161
00:22:17,320 --> 00:22:34,200
means tolerance of union sexual union so these these are the four factors if if anybody

162
00:22:34,200 --> 00:22:44,840
for those these four conditions then he said to break this third precept the factors

163
00:22:44,840 --> 00:22:56,040
for the fourth abstentions from lying are four so the first one is untrue situation that

164
00:22:56,040 --> 00:23:06,200
means something say you lie about must be untrue and then the thought of deceiving you

165
00:23:06,200 --> 00:23:11,240
want other people to to understand wrongly you want you you you have the intention to

166
00:23:11,240 --> 00:23:26,640
deceive and then the effort caused by that thought that means accordingly you make an effort

167
00:23:26,640 --> 00:23:32,040
and forth the others understanding of the meaning and the other person understands the

168
00:23:32,040 --> 00:23:41,560
meaning immediately after you say the words so when you fulfill these four conditions you

169
00:23:41,560 --> 00:23:53,160
break the fourth precept and here what is really bad is when the welfare of another person

170
00:23:53,160 --> 00:24:05,240
is destroyed sometimes there is what is called white lie sometimes you have you you have

171
00:24:05,240 --> 00:24:14,560
to lie to make other person feel comfortable sometimes you do you don't want another person

172
00:24:14,560 --> 00:24:21,360
to feel sorry and so on and you have to say something that is not true so although that

173
00:24:21,360 --> 00:24:38,720
is that is a lie it may not be not be too bad and sometimes people use exaggerations like

174
00:24:38,720 --> 00:24:47,520
when a monk get some something in the village then he may come back to the monastery and

175
00:24:47,520 --> 00:24:54,720
tells other monks oh there is a there is a river of all in the village something like that

176
00:24:57,040 --> 00:25:09,040
so although those are same saying what is not true they are not not bad and the factors

177
00:25:09,040 --> 00:25:22,080
of the fifth precept which is abstention from taking intoxicants for fast is being one or

178
00:25:23,600 --> 00:25:30,720
other of the intoxicating drinks such as liquor so that means what you drink must be

179
00:25:30,720 --> 00:25:41,040
a kind of alcohol a kind of liquor and the second one is desire to drink the intoxicating

180
00:25:41,040 --> 00:25:48,320
drink so you want to drink and that that is effort caused by that desire that means following

181
00:25:48,320 --> 00:25:55,440
the desire you may get effort to drink that means you drink and for this the drink enters once

182
00:25:55,440 --> 00:26:05,520
throughout when drunk so you drink and the drink goes down your throat so when these four

183
00:26:05,520 --> 00:26:17,840
conditions are fulfilled and you are said to break the fifth precept so we can check our actions

184
00:26:17,840 --> 00:26:28,000
with these factors and conditions when we do something that might involve breaking these rules

185
00:26:31,680 --> 00:26:39,440
for example the fifth one there are some people who say I just drink and I don't do anything

186
00:26:39,440 --> 00:26:50,400
I go to the and do I take that rule it may not be a great acoustal alright you just bring it

187
00:26:50,400 --> 00:26:56,160
and you feel a little dizzy and then you go to sleep you don't do anything bad but still you break

188
00:26:56,160 --> 00:27:04,560
the rule because you look at the four four conditions it must be liquor you you want to drink

189
00:27:04,560 --> 00:27:10,560
and you drink and the liquor goes down your throat so when these four things are there then you

190
00:27:10,560 --> 00:27:18,160
break the rule so although it may not be too bad but still there is the breaking of the precept

191
00:27:18,160 --> 00:27:28,960
if you drink now the second kind of sealer or label is the eight two positive precepts

192
00:27:28,960 --> 00:27:38,800
and these eight uber set of precepts are kept generally on two positive days now

193
00:27:41,840 --> 00:27:48,560
in our countries I don't know whether it is true in in Vietnam the month is divided into two

194
00:27:48,560 --> 00:27:59,120
halves the bright half and the dark half and on the eighth of the bright half and dark half

195
00:28:00,640 --> 00:28:09,440
and on the full moon day and new moon day people keep each uber set of precepts so generally

196
00:28:09,440 --> 00:28:24,160
speaking you buzzed up research that kept four times a month and these eight research

197
00:28:26,720 --> 00:28:30,560
you are very familiar with these eight research you are going to take them tomorrow

198
00:28:30,560 --> 00:28:39,920
so among these among the eight research the first is the same as that in the five precepts the second

199
00:28:39,920 --> 00:28:47,040
also the same as that in five precepts but the third is different in the five precepts that that

200
00:28:47,040 --> 00:28:55,040
is abstention from sexual misconduct and here the third is abstention from sex altogether

201
00:28:55,040 --> 00:29:03,440
so that is a difference here and then the fourth is the same as the fourth in the five precepts

202
00:29:03,440 --> 00:29:12,160
and the fifth also the same as in the five precepts is abstention from eating at the wrong time

203
00:29:12,160 --> 00:29:19,120
at the wrong time means after midday so during the time of the Buddha it is supposed to be wrong

204
00:29:19,120 --> 00:29:28,080
for recruiters to eat in the afternoon so the afternoon is for for for for recluse that for months

205
00:29:28,080 --> 00:29:34,480
the wrong time for eating and then number seven is abstention from dancing singing playing

206
00:29:34,480 --> 00:29:42,800
musical instruments witnessing shows that are like spikes when you when you watch shows you get

207
00:29:42,800 --> 00:29:51,360
acusala so they are they are described as similar to spikes spears or something

208
00:29:52,400 --> 00:29:59,200
and then wearing garlands that means not just garlands it flowers using flowers

209
00:29:59,840 --> 00:30:07,440
embellishing oneself with sense actually sometimes you have some some place here

210
00:30:07,440 --> 00:30:16,480
like a whole how do you call that and then you want to fill it with some some face to something

211
00:30:19,680 --> 00:30:25,920
you call making organa and then you define one serve with ungrands that means using perfumes

212
00:30:25,920 --> 00:30:33,520
and then all those things so you are you are to abstain from all these things and number eight

213
00:30:33,520 --> 00:30:40,400
is abstention from high and luxurious seats and beds so high means more than one and a half

214
00:30:40,400 --> 00:30:54,240
cubits high that means about two feet high and luxurious means a very luxurious those used by

215
00:30:54,240 --> 00:31:09,840
are called people who who enjoy their central blushes so these eight eight pieces are kept only

216
00:31:10,880 --> 00:31:19,680
occasionally and not always because as maybe we're having a family people cannot keep these

217
00:31:19,680 --> 00:31:33,120
eight precepts always so they are kept only on boss at a days now as a lay person

218
00:31:34,560 --> 00:31:45,040
if he or she keeps five precepts then his or her moral contact is said to be pure and so

219
00:31:45,040 --> 00:32:03,600
such a person is trace worthy but if let me say he so if he can keep eight precepts it's so much

220
00:32:03,600 --> 00:32:11,520
better but you know eight precepts cannot be kept always but when you take eight precepts and keeps

221
00:32:11,520 --> 00:32:22,160
you keep them you get less acusala because when you when you eat there is acusala involved

222
00:32:22,160 --> 00:32:29,200
and when you dancing and so on or when you use their flowers and so on there is acusala involved

223
00:32:29,200 --> 00:32:36,240
so when you have abstain from these things you get less acusala and also more time

224
00:32:36,240 --> 00:32:45,360
to devote to and the practice so it is better to keep eight precepts then to keep five precepts

225
00:32:45,360 --> 00:32:51,200
but as lay persons if you can keep five precepts so much the better

226
00:32:51,200 --> 00:33:08,640
now there there is another kind of eight precepts which can be an alternative to and the eight

227
00:33:08,640 --> 00:33:18,480
two positive precepts now as you have as you may know these eight precepts do not involve

228
00:33:18,480 --> 00:33:28,640
abstain from eating after midday so these eight precepts are called eight precepts with livelihood

229
00:33:28,640 --> 00:33:41,360
as the eight so abstain from wrong livelihood is the eight number eight precept then there must

230
00:33:41,360 --> 00:33:50,960
be seven above the eight so these eight precepts are called livelihood as having livelihood food

231
00:33:50,960 --> 00:34:01,440
as the eight precepts now these precepts is mentioned in in the commentaries but they do not give

232
00:34:01,440 --> 00:34:12,880
how to take these eight precepts the formula for taking these precepts but in in Bama the teachers

233
00:34:12,880 --> 00:34:28,320
of old have the formula based on the formula for taking five precepts and eight precepts so these

234
00:34:28,320 --> 00:34:33,920
eight precepts are the first four are the same as the first four of the five precepts

235
00:34:37,680 --> 00:34:49,520
and number five is abstain from slender that means trying to make people

236
00:34:49,520 --> 00:34:59,120
divided trying to trying to break friendship among people by telling one thing to one person

237
00:34:59,120 --> 00:35:04,720
and another thing to another person so that is called slender here so abstention from slender

238
00:35:05,280 --> 00:35:11,760
and then the sixth one is abstention from harsh speech like using abusive words and so on

239
00:35:11,760 --> 00:35:20,160
and then number seven is abstention from frivolous talk and nonsense talk talk that does not

240
00:35:20,160 --> 00:35:32,240
confuse to wholesome states and number eight is abstention from wrong livelihood so here

241
00:35:32,240 --> 00:35:37,040
the abstention from wrong livelihood in the number eight that is why the best set of

242
00:35:37,040 --> 00:35:47,920
silly is called H precepts with livelihood SP eight or eighth member now there is an easier way to

243
00:35:47,920 --> 00:35:51,520
remember these eight precepts now you know

244
00:35:51,520 --> 00:36:06,240
my ask how many how many miss deeds are there are done by body reaction

245
00:36:10,160 --> 00:36:19,040
three and how many miss deeds by speech so you you are a friend from these three and these four

246
00:36:19,040 --> 00:36:27,040
seven the eight is abstention from wrong livelihood so you get the eight H precepts

247
00:36:30,880 --> 00:36:41,440
now there is another set this time nine precepts and they are called nine opposite of precepts

248
00:36:41,440 --> 00:36:51,440
and these precepts are also kept on opposite of days nine are the eight opposite of precepts

249
00:36:52,240 --> 00:37:02,800
plus living with mine associated with mitta pervaded on all beings it is called

250
00:37:02,800 --> 00:37:14,720
Bhagavada nine-limt seala so here the first eight are the real precepts but the nine

251
00:37:15,600 --> 00:37:22,160
living with mine associated with mitta pervaded on all beings that means living with the practice

252
00:37:22,160 --> 00:37:35,920
of loving kindness on all beings this is actually not a precept because the practice of loving

253
00:37:35,920 --> 00:37:47,760
kindness is I mean practice of loving kindness belongs to Bhavana development of mine and not to

254
00:37:47,760 --> 00:38:01,120
seal up but in order to embellish the eight opposite of precepts people try to put one more theme here

255
00:38:02,000 --> 00:38:13,360
mitta is one limb to to to to that set of seala and so it is called nine nine opposite of precepts

256
00:38:13,360 --> 00:38:21,840
but when you take these nine opposite of precepts you should be very careful because you have to

257
00:38:21,840 --> 00:38:29,360
say I will live with my mind associated with mitta pervaded on all beings that means you have to be

258
00:38:29,920 --> 00:38:40,080
practicing loving kindness all the time so if you get angry then you may you may you may lose this

259
00:38:40,080 --> 00:38:54,640
this one factor or this one limb so there is a kind of a dialogue whether the nine

260
00:38:54,640 --> 00:39:00,960
three steps should be kept or not but these nine three steps are mentioned in the text

261
00:39:00,960 --> 00:39:11,360
themselves in the uncle tranikaya and so they can be kept but some people say the precept

262
00:39:12,560 --> 00:39:19,440
not I will live with my mind associated with mitta pervaded on all beings instead they say

263
00:39:19,440 --> 00:39:29,920
I will live with my mind associated with mitta pervaded on all beings as much as I can

264
00:39:34,640 --> 00:39:36,080
how to the best of my ability

265
00:39:36,080 --> 00:39:47,920
now the last set of precepts that can be kept by lay people is the the ten precepts

266
00:39:50,320 --> 00:40:00,160
these precepts are very rarely kept because it is very difficult for lay people

267
00:40:00,160 --> 00:40:14,880
to live without handling money and so on but some some lay people may keep these ten precepts

268
00:40:16,080 --> 00:40:22,960
the first six of the same are the same as those of the eight opposite of precepts so up to not

269
00:40:22,960 --> 00:40:32,320
eating after midday the precepts are the same are the same but the seventh in the eight opposite

270
00:40:32,320 --> 00:40:44,800
of precepts are divided into two here so number seven in the eight opposite of precepts becomes

271
00:40:44,800 --> 00:40:52,400
number seven and number eight here in the ten precepts

272
00:40:54,960 --> 00:41:01,200
so when number seven in the eight opposite of precepts become number seven and eight in the ten

273
00:41:01,200 --> 00:41:12,800
precepts we get nine we already get nine precepts and the last one is extension from accepting

274
00:41:12,800 --> 00:41:21,920
code and civil accepting or handling code civil money and others so these are the ten precepts

275
00:41:22,400 --> 00:41:31,440
so ten precepts can be kept by lay people also but as I said before it is rarely kept

276
00:41:32,400 --> 00:41:40,640
now these ten precepts are kept by some leaders or no basis so for no basis it is a must a compulsory

277
00:41:40,640 --> 00:41:50,560
these set of ten precepts but these are not the only precepts they keep now there are many more for

278
00:41:50,560 --> 00:42:07,520
them now many people when we talk about or write about no basis they talk about they write

279
00:42:07,520 --> 00:42:19,680
that no basis keep ten precepts that is not not not complete no basis have to keep

280
00:42:19,680 --> 00:42:49,600
ten twenty seventy five ninety five ninety five precepts on ninety five rules these ten precepts and then the other ten and then the seventy five rules that are common with months and so no basis kept keep ninety five precepts and not just ten precepts

281
00:42:49,600 --> 00:42:58,080
you must clearly understand this sometimes they even write that months keep ten precepts

282
00:43:02,800 --> 00:43:12,800
the the the precepts monks keep how many two hundred and twenty seven so ten precepts may be

283
00:43:12,800 --> 00:43:19,200
among these two hundred and twenty seven but we you are not to say monks keep ten precepts

284
00:43:20,240 --> 00:43:28,000
I wish I that that was true I I only have to keep ten precepts instead of twenty seven

285
00:43:32,480 --> 00:43:39,200
now we come to an important point regarding the precepts and that is

286
00:43:39,200 --> 00:43:46,320
we cannot change or modify the precepts these pieces are taught by the Buddha

287
00:43:48,560 --> 00:43:57,840
and if Buddha our life now we might be able to argue with him but since Buddha is no more

288
00:43:59,200 --> 00:44:06,160
we cannot change them or we cannot modify them we have no right to to change or modify them

289
00:44:06,160 --> 00:44:17,760
so it is very important you keep them as they are taught say in the text or you may break them

290
00:44:18,640 --> 00:44:26,160
or you may not keep them at all that's okay but you cannot change them or you cannot modify them

291
00:44:26,160 --> 00:44:31,440
that is I think important because now it is there are people who who want to change

292
00:44:31,440 --> 00:44:37,520
say we these are laid down to two two thousand five hundred years ago and so they are out of

293
00:44:37,520 --> 00:44:45,360
take now so we must change this and then now people are talking about the fifth precepts use

294
00:44:45,360 --> 00:44:55,840
and abuse so use of alcohol may be permissible only abuse is to be avoided

295
00:44:55,840 --> 00:45:02,880
but there are no such such such such such saying say in the text or in the commentaries

296
00:45:04,320 --> 00:45:10,880
if you bring and it goes on your throat then you bring them so whether you use it for your

297
00:45:12,880 --> 00:45:20,400
for for the purpose of health or you drink because you want to drink the moment you drink

298
00:45:20,400 --> 00:45:28,320
and it goes on your throat you break the room so with these pieces we cannot argue with anybody

299
00:45:28,320 --> 00:45:36,560
we cannot make changes to them we cannot modify them if we don't want to we may not keep them

300
00:45:38,480 --> 00:45:44,800
when I went to Canada this time I met a lady a Burmese lady and she told me

301
00:45:44,800 --> 00:45:52,400
when she took research she said say I refrain from killing living beings to the best of my

302
00:45:52,400 --> 00:46:03,200
ability so I do not know this is not a preset you have already made a provision for breaking it

303
00:46:04,480 --> 00:46:14,080
so I will keep it only as much as I can if I cannot I keep it I will break it or something like that

304
00:46:14,080 --> 00:46:22,480
so research should not be like that so whether you you can keep it intact or not is one thing

305
00:46:22,480 --> 00:46:29,360
I'm saying that I will keep it as much as I can it's another it shows a what about the weakness

306
00:46:29,360 --> 00:46:41,120
of the determination or weakness of spirit so I don't think the preset is taken when you say

307
00:46:41,120 --> 00:46:54,240
to the best of my ability okay we come to livelihood and upasaka must have

308
00:46:55,520 --> 00:47:02,560
or must follow a right livelihood that means he must abstain from wrong livelihood

309
00:47:02,560 --> 00:47:14,720
and go there thought in Angutra Nikaya that there are five traits that should not be increased

310
00:47:14,720 --> 00:47:23,520
in by an uber seba so if you are a leaf all over the Buddha and you should avoid these five

311
00:47:23,520 --> 00:47:31,680
kinds of traits and they are trading in weapons trading in beams trading in meat trading in

312
00:47:31,680 --> 00:47:39,440
intoxicants and trading in poison not trading in weapons means making weapons and selling them

313
00:47:40,000 --> 00:47:46,080
that is the explanation given in the commentary and in the supplementary

314
00:47:48,160 --> 00:47:55,200
a little more is added selling weapons after making them oneself you make them and then you sell

315
00:47:55,200 --> 00:48:01,840
them or after having others made them or you ask others to make to make them and then you sell them

316
00:48:01,840 --> 00:48:09,200
or obtaining them already made you you you got already made weapons and then you sell them

317
00:48:09,760 --> 00:48:17,760
so that is trading in weapons trading in beans means selling human beings

318
00:48:17,760 --> 00:48:28,160
so selling human beings for slavery and others and trading in meat means raising

319
00:48:29,920 --> 00:48:38,560
animals and selling them here also the supplementary is that selling them after raising them

320
00:48:38,560 --> 00:48:48,000
and having their meat ready so it is prohibited the supplementary explained because it

321
00:48:48,000 --> 00:48:59,280
involves taking life and trading in intoxicants when making intoxicants and selling them

322
00:48:59,280 --> 00:49:08,800
and trading in poisons making poisons and selling them so these five kinds of phrases described as

323
00:49:08,800 --> 00:49:17,600
wrong livelihood by the Buddha and so I should be avoided should I say as much as possible

324
00:49:17,600 --> 00:49:34,240
so a person who takes refuge in the Buddha down on the Sangha is expected to keep five precepts

325
00:49:34,240 --> 00:49:48,960
always and then keep eight nine or ten yourselves when he or she can and also abstain from

326
00:49:51,680 --> 00:50:01,280
livelihood which are described as wrong and here the five five times of trades

327
00:50:01,280 --> 00:50:14,480
so this is the the the sealer of keeping moral conduct pure as the first step in the spiritual journey

328
00:50:15,280 --> 00:50:23,600
and since it is the first step it is the basis it is the the firm foundation on which

329
00:50:23,600 --> 00:50:32,960
we will build the structure of concentration and wisdom so without the duplication

330
00:50:33,680 --> 00:50:40,640
without the purity of sealer we cannot hope to get concentration and without concentration

331
00:50:41,760 --> 00:50:52,080
wisdom or penetration into the nature of things cannot arise so sealer is to be kept

332
00:50:52,080 --> 00:51:06,640
pure especially when you practice meditation and when you practice meditation you expect to get

333
00:51:06,640 --> 00:51:14,240
concentration and if your moral conduct is not pure if your sealer is not pure you cannot get

334
00:51:14,240 --> 00:51:24,480
concentration because when your sealer is not pure you suffer from the remorseful feelings

335
00:51:25,280 --> 00:51:32,000
and when there is remorse there can be no peace of mind and so no tranquility and when there

336
00:51:32,000 --> 00:51:41,520
is no tranquility there is no and no comfort of mind and when there is no comfort of mind there can

337
00:51:41,520 --> 00:51:55,840
be no concentration so sealer keeping moral conduct pure is an important fact is actually for

338
00:51:55,840 --> 00:52:05,200
all people especially for those who follow the teachings of the Buddha and who who want to

339
00:52:05,200 --> 00:52:14,960
reach this stage where all mental departments are eradicated

