1
00:00:00,000 --> 00:00:03,200
I've been looking for a good online, just updated, so I have a lot.

2
00:00:07,000 --> 00:00:08,700
I'm about Miss Claudia has one.

3
00:00:09,500 --> 00:00:11,300
All I ask, because I'm next, right?

4
00:00:12,100 --> 00:00:14,800
What is the difference between brain and mind?

5
00:00:15,700 --> 00:00:16,200
Yes.

6
00:00:21,200 --> 00:00:25,000
I've answered this before, but I don't think I've ever answered it on Ask a Monk.

7
00:00:25,100 --> 00:00:27,500
Maybe I did, maybe I did in the very beginning,

8
00:00:27,500 --> 00:00:30,500
because it's been so long since I answered this one.

9
00:00:32,500 --> 00:00:34,500
But I know I've answered it somewhere.

10
00:00:35,000 --> 00:00:37,000
I've answered it for people before.

11
00:00:38,500 --> 00:00:41,500
What the brain and the mind?

12
00:00:48,500 --> 00:00:52,000
The first thing we have to, there's a few things we have to understand.

13
00:00:52,000 --> 00:01:00,000
The first thing we have to understand is in regards to levels of reality.

14
00:01:01,000 --> 00:01:06,000
In Buddhism, in the tradition that I follow, there are understood to be two levels of reality.

15
00:01:07,000 --> 00:01:10,000
Conventional reality and ultimate reality.

16
00:01:11,000 --> 00:01:14,000
Yeah, no, this is really the only thing we need to understand.

17
00:01:14,000 --> 00:01:18,000
If you understand the difference between conventional and ultimate reality,

18
00:01:18,000 --> 00:01:24,000
then you can understand quite easily the difference between the brain and the mind.

19
00:01:26,000 --> 00:01:28,000
The brain is a concept.

20
00:01:29,000 --> 00:01:32,000
It doesn't really exist in ultimate reality.

21
00:01:35,000 --> 00:01:36,000
So how does this work?

22
00:01:36,000 --> 00:01:40,000
Conventional reality is referring to concepts that arise in the mind.

23
00:01:41,000 --> 00:01:45,000
When you think of a brain, the brain arises in your mind.

24
00:01:45,000 --> 00:01:49,000
When you experience the brain,

25
00:01:54,000 --> 00:01:57,000
what you're actually experiencing is not a brain at all.

26
00:01:58,000 --> 00:02:01,000
What you're experiencing is light touching the eye.

27
00:02:02,000 --> 00:02:06,000
Sound at the air if you squish a brain and hear it squishing.

28
00:02:07,000 --> 00:02:10,000
Smell if it's rotting, taste if you're eating brains.

29
00:02:10,000 --> 00:02:15,000
One can brain's people eat, I think.

30
00:02:16,000 --> 00:02:19,000
If you squish it and you feel it squishy, that's the feeling.

31
00:02:20,000 --> 00:02:22,000
And if you think about a brain, that's the thought.

32
00:02:23,000 --> 00:02:26,000
It's not possible to experience brain.

33
00:02:27,000 --> 00:02:30,000
What you're experiencing is sight, sound, smell, taste, feeling of thought.

34
00:02:31,000 --> 00:02:33,000
That's all you can experience. That's ultimate reality.

35
00:02:34,000 --> 00:02:37,000
What is ultimately real is six things.

36
00:02:37,000 --> 00:02:42,000
So this is as far as the brain goes.

37
00:02:43,000 --> 00:02:45,000
The brain is something that doesn't exist in ultimate reality.

38
00:02:46,000 --> 00:02:49,000
Ultimate reality is made up of experiences.

39
00:02:50,000 --> 00:02:57,000
Experiences have a maximum of two aspects to them.

40
00:02:58,000 --> 00:03:00,000
Or let's say they have two aspects to them.

41
00:03:01,000 --> 00:03:04,000
The observer and the observed.

42
00:03:04,000 --> 00:03:08,000
That which does the observing and that which is observed.

43
00:03:09,000 --> 00:03:13,000
So for example, in watching the stomach,

44
00:03:14,000 --> 00:03:17,000
when you sit in meditation and you say rising falling,

45
00:03:18,000 --> 00:03:19,000
there are two aspects to the experience of it.

46
00:03:20,000 --> 00:03:24,000
There's the rising sensation and there's the mind which is aware of it.

47
00:03:25,000 --> 00:03:27,000
There's the awareness of it, I can say.

48
00:03:28,000 --> 00:03:30,000
How do we know that there's two things?

49
00:03:30,000 --> 00:03:33,000
Well, the rising and the falling occurs always.

50
00:03:34,000 --> 00:03:37,000
It all times, sometimes we're aware of it, sometimes we're not.

51
00:03:38,000 --> 00:03:41,000
At the times when we're not aware of it, there's no experience of it.

52
00:03:42,000 --> 00:03:47,000
So without the mind, the experience cannot arise.

53
00:03:48,000 --> 00:03:51,000
But if the mind goes to the stomach and the stomach is not rising,

54
00:03:52,000 --> 00:03:54,000
then there's also no experience of the rising.

55
00:03:54,000 --> 00:04:00,000
It takes both things to occur at once for the experience of rising to occur.

56
00:04:01,000 --> 00:04:08,000
So if by mind you mean a momentary awareness,

57
00:04:09,000 --> 00:04:11,000
which arises and ceases with the object,

58
00:04:12,000 --> 00:04:17,000
then mind is something that exists in ultimate reality.

59
00:04:17,000 --> 00:04:26,000
If by mind you mean the mind in terms of something that exists for a moment to moment

60
00:04:27,000 --> 00:04:33,000
and knows this or that, knows this and then that,

61
00:04:34,000 --> 00:04:38,000
knows one thing after another and is responsible for all of our knowledge as an entity,

62
00:04:39,000 --> 00:04:40,000
then again you're dealing with a concept.

63
00:04:41,000 --> 00:04:43,000
Why? Because it's nothing to do with experience.

64
00:04:43,000 --> 00:04:52,000
It is not experienceable in ultimate reality, our experiences, our things that arise and see a moment after moment after moment.

65
00:04:53,000 --> 00:05:00,000
That part of the explanation is a little bit hard to see because ordinary experience seems to be so fast

66
00:05:01,000 --> 00:05:04,000
that it seems to be a continuous moment.

67
00:05:04,000 --> 00:05:07,000
It seems that the same mind is aware of one thing after another.

68
00:05:07,000 --> 00:05:15,000
If you don't undertake intensive meditation, it's very difficult to break it up into pieces.

69
00:05:16,000 --> 00:05:20,000
If you do undertake intensive meditation for any length of time,

70
00:05:21,000 --> 00:05:26,000
even after a few days we'll begin to see the arising and be able to break it up into pieces,

71
00:05:27,000 --> 00:05:34,000
and actually see that what we call mind is actually just a series of awareness.

72
00:05:34,000 --> 00:05:41,000
None of which are, none of which last from one moment to the next.

73
00:05:42,000 --> 00:05:53,000
So either there are two different concepts, one referring to a physical entity that is experienced as seeing, hearing, smelling, tasting, feeling or thinking.

74
00:05:53,000 --> 00:06:04,000
The other is an entity that experiences and is aware of all of our experiences.

75
00:06:05,000 --> 00:06:14,000
Or one is a concept, a brain, and the other one is the name that we give to the awareness that arises in any given experience.

76
00:06:15,000 --> 00:06:16,000
Either way they're two very different things.

77
00:06:16,000 --> 00:06:29,000
The difficulty with this question, and the reason why people will ask this question, is a misunderstanding or a different understanding of what is meant by reality.

78
00:06:30,000 --> 00:06:39,000
So if you start from not from first principles, but you start by projecting a three-dimensional space external from our experiences,

79
00:06:39,000 --> 00:06:49,000
which as far as I understand has totally been thrown out the window by theoretical physicists in general, I'm not sure about that.

80
00:06:50,000 --> 00:06:57,000
But if you start from that point of view, then yeah, you're looking at everything in terms of brain first, right?

81
00:06:58,000 --> 00:07:03,000
And so you say things like what many scientists say, mind is just a product of the brain.

82
00:07:03,000 --> 00:07:28,000
Mind is just the synapses firing in the brain, and people have even told me that the best scientific theory or science has come very far improving or has pretty much proven the state of the truth.

83
00:07:28,000 --> 00:07:39,000
Or the theory that the mind is just an epiphenomenon created by the brain, and actually doesn't have any active role to play in experience.

84
00:07:40,000 --> 00:07:51,000
That it's just something that like harmony that arises based on sound or sound that arises based on vibrations, which is totally false.

85
00:07:51,000 --> 00:08:07,000
There is no substantial body of evidence to prove such a theory, even though, again and again, scientists and non-scientists alike will tend to claim that there is.

86
00:08:08,000 --> 00:08:12,000
And any investigation into this object whatsoever will tell you otherwise.

87
00:08:12,000 --> 00:08:29,000
In fact, many, many studies and research papers show, and show otherwise, show that actually the mind is something very different from the brain.

88
00:08:30,000 --> 00:08:38,000
And show actually, for example, this study that was done on psilocybin.

89
00:08:38,000 --> 00:08:44,000
There was a Dutch group, and you know, it's just one study, so it may be refuted by the next study.

90
00:08:45,000 --> 00:08:57,000
But this study, they injected psilocybin, which is the active narcotic element in mushrooms, magic mushrooms.

91
00:08:57,000 --> 00:09:09,000
The injected this into their subjects, and then studied their brain waves, and what they expected to find was an increased brain activity.

92
00:09:10,000 --> 00:09:24,000
Of course, that's what you'd expect. What do mushrooms do? Anyone who's taken them? They give you all sorts of wild experiences. You start laughing and controllably. You have hallucinations of angels and gods and all sorts of fun stuff.

93
00:09:24,000 --> 00:09:31,000
So you'd think, well, gee, it must mean that there's something crazy going on in the brain, right? It's a chemical.

94
00:09:32,000 --> 00:09:40,000
And so it's stimulating the brain in some way. What they found, true or not, this is what they found, was exactly the opposite.

95
00:09:41,000 --> 00:09:45,000
The psilocybin actually inhibits the brain's ability to function.

96
00:09:45,000 --> 00:09:56,000
So the question is, well, if it inhibits the brain's ability to function, how is it that you actually have enhanced experiences when you're on this drug?

97
00:09:58,000 --> 00:10:09,000
Now, if you go back about 100 years or more, you have some idea of a theory that may explain this sort of thing.

98
00:10:09,000 --> 00:10:33,000
And that's actually been followed by groups of people who are a little more open-minded than your average materialist scientist, who explained the mind in terms of the only, in the only terms that they could explain it, based on their clinical data, which is quite impressive if it's all real data, that the brain is a filter for the mind.

99
00:10:33,000 --> 00:10:45,000
It's not the creator of the mind, but it squeezes and shapes the mind into all sorts, contorts the mind and all sorts of weird shapes, giving us all these crazy...

100
00:10:45,000 --> 00:11:00,000
All these crazy neuro-season psychosis and so on, and how people can allow scientists to tell you what chemical is causing you, what crazy activity and what brain problem.

101
00:11:00,000 --> 00:11:12,000
What mind problem is being caused by what part of the brain makes them think that somehow the brain is the product of the mind, which is actually not supported by the evidence.

102
00:11:12,000 --> 00:11:27,000
So what they found is it's a filter in this way. Why? Because when the brain shuts off, there are cases where people have out-of-body experiences, where it's not possible for the experience that they were having to have been caused by the brain.

103
00:11:27,000 --> 00:11:39,000
The brain was not in a functional state. It was not producing enough current to give rise to any, even the weakest thought, let alone the clear vivid images that people experience.

104
00:11:39,000 --> 00:11:50,000
There's this Jill Taylor video, I think it's a PhD who had a stroke, and she gives a vivid example of this.

105
00:11:50,000 --> 00:11:57,000
Of course, I think in her point of view, half of her brain was cut off, and that's just what happens when only half of your brain works.

106
00:11:57,000 --> 00:12:24,000
That your mind works based on the other half, totally, which is, I guess, what was happening. But it could be used to support the filter argument, the filter theory, that once you cut off half of the filtering, the filtration, then the mind becomes so much more free and alive and non-judgmental and so on.

107
00:12:24,000 --> 00:12:36,000
But there certainly is no consensus on the question in the scientific community, and from a Buddhist point of view, it's all silly anyway, because the brain doesn't exist.

108
00:12:36,000 --> 00:12:47,000
So what are you doing worrying about? How can you say that the brain creates experience when you have to use experience to even think about the brain?

109
00:12:47,000 --> 00:13:00,000
Which is a neat twist that is echoed by this PhD that I always refer to Henry Stapp, who's an theoretical physicist, and he explains it much this way.

110
00:13:00,000 --> 00:13:16,000
He explains quantum physics in terms of requiring the experience, which is the basic premise of quantum physics was that you couldn't begin to talk about three-dimensional space or any dimensional space.

111
00:13:16,000 --> 00:13:37,000
You couldn't begin to talk about physics until you took into account the experience, so science became reduced to a description of observations as opposed to a set of impersonal entities existing outside of experience.

112
00:13:37,000 --> 00:13:53,000
Very interesting, if you're interested in that kind of stuff, but point of view, point being that the experience is first principles you have to start from experience.

113
00:13:53,000 --> 00:14:11,000
When you understand this, it becomes kind of a meaningless question, or very easy to answer a question anyway, because you're easy to see the answer to the question, a very, very different mind is a part of experience, brain is not a part of experience.

