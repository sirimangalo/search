Good evening and welcome back to our study of the Dhamupada.
Today we continue on with verses 209 to 211, which read as follows.
A yogi yunjamatana, yoga sminja, a yujana, a yujang, a tang hitwa, piyagahi, piyatatana, yujinana.
Mahpiyayi samaganti, a piyagahi kudajana, piyanang dasanang dukang, a piyanang jandasanang.
Tasma bhyang nakaiyirata, piyapayuhi papaku, kantati sang nakrijanti, a sang nakti, piyapyang, which means
one who is involved with things, one shouldn't be involved in, and in those things that
one should be involved in, doesn't get involved.
Having abandoned what is useful at that and clinging to what is dear to them.
They will end up enveying those who were more involved or better involved, those who
were strenuous or got, got work done, don't, don't associate or one shouldn't
associate, one shouldn't hold on to what is dear, or what is dear whatsoever, what is
not dear whatsoever, for not to see what is dear, this is suffering, as well as seeing what
is not dear.
For that reason, make nothing dear, piyapayuhi papaku, for it is, for the loss of what
is dear is evil, and here evil just means bad or unpleasant, kantati sang nakrijanti,
there is no, there are no bonds, binds, there are no fetters, for those who hold nothing
dear or under, not dear, isang nakti piyapayu, for those who have nothing that is dear
or the opposite of dear, which is unpleasant, that dear is maybe not, it's an awkward translation,
it might be something you value, something you cherish, something you hold on to, something
you love, we might say, it's probably the most common usage of the word love, and the love
can mean many different things, we say we love cheesecake, or when you say you love your
partner, even when you say you love your parents, it's often, or love your children, it's
often associated with clinging, rather than appreciation or simply friendlyness, so that's
what piyapayuhi means, and we're in the piyavaga, we've just started, this is the first
story from the piyavaga, which means the chapter on dearness or holding things, on cherishing,
so this was, this verse is supposed to have been said in regards to a story about a
family, a family whose members were very dear to each other, we have a mother and a
father and their son, and they were living together in harmony, but then one day the Buddha
came to their, their area, and the son heard him speak and was so encouraged by the things
the Buddha said that he decided that he would, he wanted to become a monk, so he asked
his parents, his parents refused, they said no, he can't, and they tried to keep watch
on him because they figured he would sneak away and go become a monk if they didn't,
and so whenever the mother would go out the father, she would tell the father to look after
him and watch him and spy on him, or basically hold him prisoner, and the same when the
father went out, he would tell the mother, remind the mother to do the same, one day the
father went out and the mother was weaving or spinning wool or something, and so she would
sit in the doorway of their, their hut or their home, and just to make sure he never
didn't even leave the house, I don't, it doesn't say how old this boy was, but maybe
he was just young enough to become a novice, I don't know, they loved him so much that
they wanted to protect him and, and keep him prisoner even, and, but he's he asked
her if he could go outside and to use the, the facilities, maybe the outhouse or something,
he had to go, maybe water the bushes, I don't know, and he had to make a bowel movement,
some sort, and so he asked her to move out of the way, I could, I could just go outside
for a second, so she let him go outside and went back to her work, and as soon as he got
outside he ran to the monastery, asked the monks to ordain him, they ordained him, which
is not really allowed, but this may have been before it was required to have your parents
permission or, anyway, it's just a story that the details not so important, but the
parents, father came home and he said, went into the house and where's our son? Usually
he would greet the father when he came home, didn't do that, where did our son go?
Well, he went out to the washerman, he's he's not back yet, oh, and the father said,
oh, I know, if he's not here, he looked everywhere for him, he said, if he's not here,
he must have gone to the monastery, so the father went to the monastery, sure enough he
saw his son with a shaven head and wearing robes, and he was so aggrieved, so sad, thinking
to himself, how can I go home and live without my only dear son?
And so then and there he said, I'm not going home and he asked the monks to ordain him
as well, shaved his head, put on the robes, became a monk, the mother sitting at home,
wondering where the husband and son have gone, she said, well, I guess probably the son
moved on to the monastery and so she heads off to the monastery and sees them both and says,
wow, what the heck am I going to do at home if they're both at the monastery, I better ordain
as well and she went to the bikunis and they ordained her, shaved her head, put on the
robes, which sounds like a very heartening story, you know, the son led both his mother
and father to become monks, but it didn't work out the best, they, after they ordained
it says they did nothing but socialize, they were so accustomed and attached to each
other, accustomed to being in each other's present, attached to each other's presence
and company and interactions, but they did nothing but sit around and socialize and chat
and the monks got annoyed, upset or disturbed, at least the ones who weren't yet far advanced
in their meditation and so they went in the Buddha and they said, better both her, this
family kind of regret letting them become monks because now they do a sit around and chat
and socialize, they're not accomplishing anything, accepting, increasing their clinging
and disturbing the monastery and so the Buddha called them up and is this true and you
scolded them and said, this is not the way of, it's not the way to have real happiness
see what did the Buddha exactly say, he said, why do you do this, this isn't the proper
way for you to conduct yourself, but but then or most, sir, it's impossible for us to
live apart and he said once you leave home, having left home, this is very improper and
then you taught, taught the verse basically, verses.
So we have a lesson from the story, we have a lesson from the verses, from the story,
I guess they're two lessons, the first most obvious one is we don't approve chatting
and socializing, so in a monastery and the meditation center, it's why we have these sort
of rules, not just because the Buddha said not to and said it's improper, but he got
admit if the Buddha says that something is improper, it's probably good idea to follow
even blindly because well, we don't know many, many things and taking your teachers
advice is often a good thing, good sort of default until you have reasoned it out.
But we can find reasons easily, of course, socializing and chatting in specific are quite
distracting from our practice and reinforcing of our defilement, the bad things inside that
we're trying to change about ourselves bad habits are quite reinforced simply by engaging
in idle speech because it's very difficult to be mindful, it's very habitual, just the
act of speech is caught up in so much of our habits, our manners of speaking, the topics
of speech, you know, often we sit around gossiping and talking bad about other people
and so on, that sort of thing, teasing, manipulating, joking and all of this, it's just part
of our habits that are caught up with speech which are very difficult to change, it's
not that speaking can't be mindful, you can even be mindful as you're speaking mindful
of your lips, moving even mindful of the sound of your own voice, it's just mostly
we're not doing that, that's the reason why we sit and chatter, not as a meditation
exercise usually, but we shouldn't think that silence is necessary, it isn't, it never
was, it was something that the Buddha criticized, in fact, if you say I'm not going to
talk no matter what, you shouldn't ever go to that extreme, you can, for the most part
not talk and the remarks who did this, they didn't talk hardly at all, but then they didn't
talk at all actually, but then every five days or so they would get together and they
would talk all night, talk about the dhamma, but it was a means of getting feedback and
interacting with your fellows, it's just important, just like we talk every morning, so
you have a chance to interact with someone who has a little bit of background and experience,
but the deeper lesson of course is this idea of holding something dear, so the real lesson
this story gives to us and other stories of the Buddha, or of the Buddha's time is how strongly
people cling to each other, how strongly this habit becomes, how powerful it can be,
the strength of their attachment was preventing them from allowing this young man to do something
great, right, how often you hear about parents not allowing their children to become more
dain, I mean I do in Asian circles anyway, not because they think Buddhism is bad, they
generally Buddhists themselves, there are cases where parents don't want their kids to
ordain, especially in the West because they think Buddhism is evil and wrong and so on,
but that's not the case often in the East, in Buddhist countries, they just don't want
it because they're so attached to their kids, even attached and worried about their kids'
future, they want their kids to be rich and in society and carry on their name and have
kids, they want grandkids and that sort of thing, so it's quite horrific to think that
their kids might become monks and then how strong was the attachment once he had ordained
that they couldn't live without him, they couldn't continue on their lives, they were
so desperate that they decided to ordain even though they didn't seem to be all that
inclined towards Buddhist practice and then once they were ordained their attachment
was so strong that even the sun wasn't able to practice because he was so involved
with socializing with his parents, so it gives us this example of very strong attachment.
When the verses themselves provide sort of the lesson on why it's not a good idea
to attach and there's three lessons, each verse is actually a little bit different and
you can find a distinct lesson from each of them that describes a very important part
of the Buddhist teaching, so the first verse is mostly describing our acts, acts and
omissions, talks about getting involved with things you shouldn't get involved in, I use
the word involved, yoga, yoga means something like bound or attached, but not exactly
attached, tied to yoga is kind of like I've talked about before from yoke, a yoke in English,
where the yoke on the ox is this thing that ties it to the cart, so it can pull it.
So when you're a harnessed to a cart, you do the work of pulling the cart, as I understand
how the word evolved, so it's used often in Buddhism to talk about this person who is dedicated,
a dedicated practitioner, yoga, what Shara is a very common Buddhist word, someone who is
engaged in the work, but there's things that you shouldn't engage in, you shouldn't get involved
and you shouldn't work at, and someone who engages in those things and doesn't engage
in the things that you shouldn't engage in, and then he says it's going to be jealous
or envious, it's going to feel bad when they see everyone else in a monastic setting, especially
or in a meditation center, everyone else is becoming enlightened and there's still useless.
The Buddha gives an example, not here, but it's a famous simile of a cowherd or a shepherd.
You might think of a person who works a servant in a rich household or a person, cows
we don't think of so much because we don't think of cows as highly as they would have
in the time of the Buddha, but it's the equivalent of being surrounded by riches, but
having to take care of them like a banker, a person who can work in a bank, surrounded
by such wealth, but have no access to the bank, a person who shepherds or cowherds, cows
surrounded by very valuable livestock, but they're just working for a minimum wage, they
don't actually get to taste the milk from the cows or the cheese or the meat now as we
kill the cows, you don't get any of the benefit from the animals.
Person who works in a mansion can be surrounded by great luxury, but never have access
to it, they can't sleep in the beds or sit in the chairs or eat the food, they get a
minimum wage and then they go home, or they sleep in the servant's quarters.
So the lesson here is about acts and omissions.
In Buddhism, of course, it's democratic, or it's even democratic, there's equality in the
sense that it's accessible to everyone, it's not a case where you're set in your social
class, so it's different in that you have the option to become rich in Buddhism, you have
the option to be the person who owns the cows, to be the person who owns the money in
the bank, all you have to do is do the work, engage in what you should engage in, don't
engage in what you shouldn't engage in.
So this relates to actions, karma, really.
There's a story of a monk who is very much attached to his food, he lived alone, and this
is a common thing, I've seen this, it's discouraging how common it can be, monks live
alone because they're jealous of their gains, they live in a village and they're surrounded
by people who, for lack of a better monk, a better monastery, go always go and pay respect
and give offerings to this monk, so anytime another monk comes, they're very protective
of their gains, the things they hold dear, so they'll chase the other monk, so why
you've got, you have to be a little bit suspicious when you see a monastery with only one
monk, it's unfortunately common for that to be the case.
So this is a story in the Buddha's time, it was even the case, there was this monk who
was in this sort of situation and another monk came and this monk was so profoundly peaceful
in his demeanor, he would walk slowly when he talked, it was thoughtful and he was in
Arahan, he was enlightened as it turns out, this monk of course didn't know that, he
was just scared because this was clearly a powerful sort of spiritual individual and he
thought to himself, I'm ruined, if the people of the village see this monk, I'll never
get any support and so he arranged it, so in the morning he said he would ring the bell
and in the morning he went and touched the bell with his finger and said, oh I've rung
the bell, this monk hasn't come and he went to the village and they asked, oh where's
the other monk, we heard there's another monk living there and he said, oh I think he's
sleeping in, I rang the bell and he didn't come and there was one laywoman who was
taking care of him, who made very sort of luscious with the word, very delicate, superior
food, good food, delicious food, refined food, fine food and she said, oh well then take
back, you eat some food here and then take back food for him and your bowl and so he ate
the meal and she put food in his bowl for the other monk and he looked at it and he said,
I can't let him taste this or he'll never leave, he'll be so attached to the food that
he'll stick around and then I'm ruined and so on the way back to the monastery he dumped
the food out on the side of the road, the other monk, the arrow hunt had woken up of
course early in the morning he wasn't lazy or wasn't sleeping in and he knew what the other
monk had done I think he had some kind of psychic ability to see that the monk was doing
that and then he knew that the monk had not actually rang the bell and he thought to himself
I can't live with such a person and he went on this way. The monk who was very much attached
to the food and as a result getting engaging in practices he really shouldn't have been
engaging in and ended up being born in hell and ended up becoming a, I think he was, there
was this monk in the time of our Buddha, yes this was a monk, starts with an emendic of
me, maybe I can't remember, one monk who, not mendicah, look here maybe I can't remember,
he never got enough to eat, as a result of his bad karma of throwing out this food that
was dedicated and designated for this other monk, he never got enough food and all the
time he was ordained until finally Sariputta was the one who ordained him and may have been
mendic and not anything and I don't remember his name, someone's going to tell me what
the name was eventually, Sariputta had to feed him with a spoon and finally he was dying
and Sariputta on the last day, Sariputta went for arms, he went, they went for arms together and this
monk didn't get any food, he said and neither of them got any food because of the bad karma
this monk, the story is a very long story, when he was born he was born a beggar and as soon as he was
born, as soon as he was conceived the whole company of beggars didn't get anything to eat,
they kicked his parents out and his parents didn't get anything to eat and then once he was born
none of them got anything to eat so they kicked him out and he never got anything to eat until
finally Sariputta found him and ordained him and he still didn't get anything to eat, Sariputta took
him on arms, neither of them got anything to eat, Sariputta told them to stay at home, Sariputta went
on arms, sent food back with a novice to him, the novice ate the food on the way,
Sariputta went back to the monastery and asked him if he didn't, he said, I'm fine,
he was in Arahan at that point I think and Sariputta took his food, took his bowl and actually with
his own hand, hand fed into his mouth and just before he passed away he got a real meal,
he actually got food to eat and I can't remember his name.
But the story here is about his evil deeds because of his craving and this is of course
just one of many many stories of how clinging or holding something dear,
something silly like food, right, can lead us to do such evil, when we hold people dear, we
when we cherish other people the things we'll do to get what we want, that's why I think you
can extrapolate to why things like rape occur and that sort of things horrible things that happen,
that's why people go to war because they cherish what other people have or they cherish what they
have, we cling to our possessions, we cling to nationalities, we cling to
color and so many things wealth is something rich people clinging to wealth now we see
the rich getting richer, the poor getting poorer and this class warfare,
fighting and in the truth of it is from a Buddhist perspective that the rich are just trading
places with the poor every lifetime so if you're rich and stingy then you get poor and then you
realize how awful it is to be stingy and so you treat you start to be a little more generous and
then maybe you become rich again but we're seeing less and less generosity I think and that's
why the disparity, why people are so few people are rich, so few people actually have
wealth or affluence, luxury because while they've traded places they've done some good things
that give them that and probably when they die they're going to lose it all
so the first first deals with this, this idea that we hold things dear,
leads us to do evil things, it leads us to fail to do good things, it's why this family was
unable to practice meditation and in our meditation this is the important lesson that
it prevents us from being mindful when we start to get soft track and thinking about the past
or the future greedy about or clinging to things that we might get or things that we had or so
it leads us to fail to be mindful, it's why entertainment is something we should put aside during
a meditation course because it gets in the way of our mindfulness.
The second verse deals more directly with the suffering, it says seeing what is not dear,
that's suffering and seeing what you don't like, that's suffering and what you like when you
don't see that, that's also suffering and many stories like this, a story of course of Patachara
who lost her two children and her husband, she was traveling back to see her family and her
husband got bitten by a snake and then one of her babies was taken away by a bird, she had just
given birth and she placed, she left the older son by the side of the river, this river and she
crossed the river with the baby, left the baby on the side of the other side, started across
back the river and then she turned around and saw that this bird had seen her newborn baby and
thought it was a piece of meat and grabbed it and so she waved at the bird in the middle of the
river trying to chase, trying to scare it off and the older child on the other bank saw her waving
and thought, she's calling me and gotten to the river and got swept away by the river, she lost
both of her children and when she got home to her parents now completely alone, she found that
her parents had died in their house and burnt down in the storm and she went crazy, she lost her
mind, she was so distraught that she just was all completely overwhelmed.
Such suffering comes from, I mean the potential for such suffering, it's not that you can't
enjoy the things that you hold dear, it's just that you engage in this kind of happiness
that is fraught with uncertainty and is dependent. In mindfulness we try to become independent,
it's another approach to happiness, it's much more sustainable, it's much more happy really
because we think these things make us happy, they only give us greater and greater clinging,
the happiness disappears and we're left with craving and clinging.
Another story is centity, many stories but centity is another example, he was a minister to a king
and he got rewarded and he ended up living like a king for a while and his girlfriend just
danced her, he was very fond of, she dies, all of a sudden just in front of his eyes while
she's dancing because she was like a starving herself, it was a thing for dancers to do,
still as I suppose and she suddenly got nervous, some kind of nerve damage and died
and he had to go see the Buddha and the Buddha had to explain to him, oh yes,
this is what happens when you hold things dear in permanence.
So the second verse relates to the suffering that comes from clinging, the third verse
relates specifically to the clinging that comes from holding things dear, so the clinging aspect
itself, it says there are no binds, there are no fetters for those who have hold nothing dear
and that's really the point, we don't see how dangerous it is to get attached to things,
we don't see how much suffering is involved, how distraught we can become
when the things we hold dear, people who hold on to relatives and belief family is so important
are the first ones to grieve deeply and really in a way that they would never wish on anyone else
when they lose their relatives and even when their relatives are there, the more clinging we
have towards them we think one's good, hold on to your family but these are the ones that fight
the most with their relatives that argue and when the relatives are not the way they want them to be
if a stranger said something to you you might not react but when or a stranger did something you
didn't expect doesn't bother you but when your relatives change sometimes it's just their children
doing things they don't want like dying their hair or wearing different clothes, not getting good
marks on their tests, someone else's kid doesn't do well in school, maybe you're happy because
it means your child is superior to them, you feel proud of your children but when your children
do not well because of your attachment to them you're scared for their future and not just as
your duty as a parent to teach them and help them but you suffer, children suffer when their parents
say things to them right, if another person were to criticize you you might be able to shrug it
off but when your parents do often you get very angry when your parents school new or so on
because of attachment because we were so caught up in our desire for them to be a certain way
and why it's interesting to especially stress the fact that each verse specifies a different
aspect of the teaching is because these three together the clinging, the action, the karma
and the suffering that comes from it are the cycle of samsara, it's called kilesa kama vibaka,
these three are the circle of craving, circle of life I guess in Buddhism, when you have craving
then there is actions, you act down on it, we do bad things, evil things, things that hurt
ourselves that hurt others, we fail to do good things of course because we're so infatuated with
getting what we want and we suffer because the actions lead to suffering that's the whole point,
they wouldn't be bad, they wouldn't be considered evil if they didn't lead to suffering but they do
clinging to good things and our aversion to bad things leads us to suffer,
these things manipulate others hurt others and as a result we suffer, we suffer when we don't
get what we want, we suffer when we get what we don't want revenge and so on and of course the
suffering leads us to cling more, we cling to when bad things happen to us, when we suffer
we do whatever we can to fix it, to get a pleasant experience and we create more and more clinging
because of the clinging then there's more suffering, defilements lead to action, action leads to
results, results we react to them again and so we create this snowball effect, this is why even
in meditation you can see this happening, you can see how when you get upset about something
then you get upset about that and you build and build the upset, that's when it becomes a real
problem, same with craving, if you want something it's just one thing but when you obsess over it
and you like the fact that you're wanting, you feel the pleasure associated with wanting
then you increase it
and so the final part of the third verse gives of course the opposite the way out
that really a much better way, a much more peaceful and happy way to live is when you don't hold
anything dear or under, you change this whole philosophy, you abandon the idea that happiness
can come from cherishing things or being averse to certain things and you cultivate an open mind
you know ability to experience the whole spectrum of reality because we have two choices,
we can limit our experiences to those things that we like or we can stop liking and we can be
open to all experiences of course the former sounds good on paper but is fraught with dangers
and problems and is a cause is the reason why there's so much suffering in the world because there
is so much suffering in the world it's an undeniable fact as much as we want to ignore it the reality
is there is great suffering in the world and it's all because it's all because of our clinging
because of our attachments because of holding cherishing certain things and hating and despising
other things so that's what we try that's why mindfulness is so central the Buddhism it's
why we're so focused on mindfulness because mindfulness is that middle way it is the avoiding
into two extremes it is a third option where you don't have to chase after things or run away
from things where you simply experience things as they are there's no more good there's no more
bad there's not even any me or mine to cling to it there only is experience it is what it is
and so by simply reminding yourself of that every moment when you can see or hear or have
seeing just be seeing have hearing just be hearing you create a new perspective a new way of
looking at reality instead of things you have to fix and control to being things that you should
experience and understand so they're very good set of verses that's the Democratic
for tonight thank you for listening we'll show all the best
