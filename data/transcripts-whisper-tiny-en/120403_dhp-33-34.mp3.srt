1
00:00:00,000 --> 00:00:14,680
Yeah.

2
00:00:14,680 --> 00:00:32,760
Okay, hello and welcome back to our study of the Dhamabanda.

3
00:00:32,760 --> 00:00:40,320
Today, we continue on with the Chit Dalagat, the Chit Dalagat, and for Wagana chapter

4
00:00:40,320 --> 00:00:41,320
number three.

5
00:00:41,320 --> 00:00:46,760
As we finish chapters, one in chapters two already.

6
00:00:46,760 --> 00:00:53,280
And this starts with verses number 33 and 34, which are based on a story about a monk

7
00:00:53,280 --> 00:00:54,280
called Meghia.

8
00:00:54,280 --> 00:01:13,520
So, the verses go as follows,

9
00:01:13,520 --> 00:01:25,980
so the E

10
00:01:25,980 --> 00:01:50,860
So, the first number, the translations, verse number 33, is Pandanantapanantitam, the

11
00:01:50,860 --> 00:02:03,100
mind that is fickle and unsteady, durakhan, difficult to guard dunywara, young, difficult to

12
00:02:03,100 --> 00:02:09,120
forbid or to stop, to keep in check.

13
00:02:09,120 --> 00:02:20,300
Ujungkaroti made how a wise person makes straight, just as Usukar or just as a Fletcher makes an

14
00:02:20,300 --> 00:02:23,620
arrow straight.

15
00:02:23,620 --> 00:02:32,980
And verse 34, wahri to wah, just as a waterborne creature or a fish, talaikitos thrown

16
00:02:32,980 --> 00:02:43,900
upon the cast upon the bare ground, taken out from the water, ukamu, kata, ubedo, so cast upon

17
00:02:43,900 --> 00:02:53,660
the ground of having been taken out from the water, Pari, Pandati, dunyitam, so to the, does

18
00:02:53,660 --> 00:03:04,820
this mind waver or flail about, maradee, young, it is the realm of mara, pahatawe that

19
00:03:04,820 --> 00:03:11,380
and it should be discarded, or the realm of mara should be discarded, the mind that is fickle

20
00:03:11,380 --> 00:03:15,900
or the mind that is wavering.

21
00:03:15,900 --> 00:03:21,100
So two very important verses from a practical point of view, and we have a fairly brief

22
00:03:21,100 --> 00:03:24,260
story, because the story is actually told elsewhere.

23
00:03:24,260 --> 00:03:28,740
So in the Dhamapadhi here we have it in very, very short form, just mentioning what

24
00:03:28,740 --> 00:03:29,740
the story was about.

25
00:03:29,740 --> 00:03:35,820
So the story is about a monkey magia who in the time, at some time in the Buddha's life

26
00:03:35,820 --> 00:03:40,700
was the Buddha's attendant.

27
00:03:40,700 --> 00:03:44,180
So he would carry the Buddha's bowl when they went on Pindapadhi when they went on

28
00:03:44,180 --> 00:03:53,980
Amshran, and one time when they were going on Amshran or traveling, magia had suddenly

29
00:03:53,980 --> 00:04:01,220
this urge to go and practice meditation in this forest growth that he had seen by the side

30
00:04:01,220 --> 00:04:02,220
of the road.

31
00:04:02,220 --> 00:04:07,620
He couldn't explain it, but he felt like he had to go and practice meditation that

32
00:04:07,620 --> 00:04:12,500
there was something that just wonderful about this little, this forest growth.

33
00:04:12,500 --> 00:04:20,060
So he asked the Buddha to take his bowl, and the Buddha said, look, I'm without an attendant.

34
00:04:20,060 --> 00:04:25,380
You have to carry my bowl, you have to act as my attendant, and he said, no, no, please

35
00:04:25,380 --> 00:04:26,380
please take your bowl.

36
00:04:26,380 --> 00:04:31,620
Because he couldn't understand why the Buddha was so bad, that he should have to carry

37
00:04:31,620 --> 00:04:32,620
the Buddha's bowl.

38
00:04:32,620 --> 00:04:34,900
He said, well, why can't the Buddha carry his own bowl?

39
00:04:34,900 --> 00:04:40,540
No, no, because this would be the duty of an attendant monk to carry the bowl.

40
00:04:40,540 --> 00:04:44,380
But it seemed like a really silly thing, Magia tried to explain that he wanted to go and

41
00:04:44,380 --> 00:04:49,100
practice meditation, and normally the Buddha would say, fine, fine, go and practice, and

42
00:04:49,100 --> 00:04:54,260
take his bowl back, but at this time the Buddha refused.

43
00:04:54,260 --> 00:04:59,060
And for the third time, and finally the Buddha consented in silence, took his bowl back

44
00:04:59,060 --> 00:05:01,020
and continued on his way.

45
00:05:01,020 --> 00:05:06,860
Magia in the meantime went into this growth to practice meditation, and as soon as he sat

46
00:05:06,860 --> 00:05:15,020
down, he felt his mind full of lust, and just this vivid images and memories of the past

47
00:05:15,020 --> 00:05:20,980
came up, and he was filled with all sorts of unwholesome states.

48
00:05:20,980 --> 00:05:27,020
And so he quickly got up and went back to meet the Buddha, and he was sorry that he had

49
00:05:27,020 --> 00:05:28,900
left, and the Buddha said, I told you not to go.

50
00:05:28,900 --> 00:05:33,460
He said, and he told him actually a past life story, somehow it goes, then he said, you

51
00:05:33,460 --> 00:05:37,100
know, in a past life that that grow of the reason why it's so attractive to you is because

52
00:05:37,100 --> 00:05:44,900
it's a grow that you used to meet up with women in this one, something like that.

53
00:05:44,900 --> 00:05:51,020
And so the Buddha used this as an example to admonish the monks, that the mind is quite

54
00:05:51,020 --> 00:05:55,900
fickle, and you shouldn't follow after it, that the mind can lead you in so many different

55
00:05:55,900 --> 00:05:56,900
ways.

56
00:05:56,900 --> 00:06:02,380
In one moment, the mind can change and lead us off into defilement and leave us off, lead

57
00:06:02,380 --> 00:06:07,180
us off into the wrong direction, and we might be sitting in meditation, or we might be

58
00:06:07,180 --> 00:06:11,660
living our lives in a peaceful way, in a wholesome way.

59
00:06:11,660 --> 00:06:18,540
And suddenly something comes up and sets us off, and carries us away.

60
00:06:18,540 --> 00:06:22,740
So the Buddha gave this explanation that this is the nature of the mind.

61
00:06:22,740 --> 00:06:28,620
The mind is fickle, the mind is unsteady, the mind is crooked, and he uses this image

62
00:06:28,620 --> 00:06:31,860
of a Fletcher, straightening an arrow.

63
00:06:31,860 --> 00:06:38,420
So what he's saying is that the mind in its ordinary state is not straight, it's crooked.

64
00:06:38,420 --> 00:06:42,940
I'm going here and going there, the mind is clinging, like if you look at the hand when

65
00:06:42,940 --> 00:06:47,180
the hand clings to something, it's because it's not straight, if you straighten your

66
00:06:47,180 --> 00:06:52,180
hand out, you can't cling to anything, or a hook, for example, because in the next verse

67
00:06:52,180 --> 00:06:57,900
he's talking about fish and mara, the bait of mara, the only reason mara can catch us

68
00:06:57,900 --> 00:07:03,820
is because our mind is crooked, the mind has something that you can hook into, but when

69
00:07:03,820 --> 00:07:14,420
the mind is straight, then there's no entry for mara, and there's no way for us to get caught.

70
00:07:14,420 --> 00:07:21,140
So from the story is just one example of how this occurs, and it's a really good example

71
00:07:21,140 --> 00:07:27,820
for us to see how in meditation our mind can carry us away with it, that we can get carried

72
00:07:27,820 --> 00:07:32,260
away by the fickleness of the mind.

73
00:07:32,260 --> 00:07:36,980
The mind is difficult to guard, it's something that can fall into greed, it can fall into anger,

74
00:07:36,980 --> 00:07:39,860
it can fall into delusion very easily.

75
00:07:39,860 --> 00:07:47,220
It's difficult to guard, and it's difficult to forbid even to just stop the mind from

76
00:07:47,220 --> 00:07:52,580
thinking this or thinking that, from getting angry, from getting upset, and recently

77
00:07:52,580 --> 00:08:00,140
one meditative was explaining how she got very angry and had work, someone said something

78
00:08:00,140 --> 00:08:04,180
to her told it was disappointed in her work, and she was so upset.

79
00:08:04,180 --> 00:08:10,900
But the bad thing wasn't the upset, she said, the bad thing was this anger that she had

80
00:08:10,900 --> 00:08:17,860
been for herself because of how upset she was, so the problem wasn't what they had said

81
00:08:17,860 --> 00:08:23,340
to her, she wasn't so upset about what they had said, but she was upset that she had

82
00:08:23,340 --> 00:08:33,180
got so upset, and this became a huge problem, so she was berating herself, and it was

83
00:08:33,180 --> 00:08:45,700
very angry at herself, for allowing herself to become so unstable in mind.

84
00:08:45,700 --> 00:08:53,220
So in another sense, the Buddha is just giving us a teaching here to allow us to see this

85
00:08:53,220 --> 00:08:57,540
clearly, to not become upset when this occurs, because actually the truth is the problem

86
00:08:57,540 --> 00:09:01,660
isn't the upset, the problem is that you get upset about it.

87
00:09:01,660 --> 00:09:06,180
When you get angry, then you think there's some problem, or when you have lust arise,

88
00:09:06,180 --> 00:09:10,540
so this monkey had some feeling arise, and he didn't know what it was, but he thought

89
00:09:10,540 --> 00:09:12,340
of course that he should follow it.

90
00:09:12,340 --> 00:09:19,020
And this is really the problem, is the reactions to our reactions, the reactions to our emotions,

91
00:09:19,020 --> 00:09:21,020
when you want something.

92
00:09:21,020 --> 00:09:25,620
One thing isn't a real problem, until you chase after it, until you try to achieve

93
00:09:25,620 --> 00:09:27,500
your desires.

94
00:09:27,500 --> 00:09:31,300
Anger isn't a real problem, until you say, I can't take it anymore, I have to go

95
00:09:31,300 --> 00:09:37,860
and act upon it, I have to hurt someone, or I have to cause suffering.

96
00:09:37,860 --> 00:09:44,060
And so on the one hand here, he's just explaining that this is the way it is, and your

97
00:09:44,060 --> 00:09:47,900
job is not to change this.

98
00:09:47,900 --> 00:09:53,900
You can't do new iron on the Buddha, it's very difficult to prevent the duty is to make

99
00:09:53,900 --> 00:10:01,660
and create a clear mind, to create a straight mind, to change this habit of the mind.

100
00:10:01,660 --> 00:10:09,260
Going here and going there is taking us away, it's crooked, the mind is following all sorts

101
00:10:09,260 --> 00:10:18,380
of crooked paths, the goal of our practice is to straighten the mind.

102
00:10:18,380 --> 00:10:26,740
The crookedness here, it's quite interesting, because even in meditation practice, we can

103
00:10:26,740 --> 00:10:31,380
give rise to this sort of crookedness, or even as monks, even in a monastic setting.

104
00:10:31,380 --> 00:10:34,900
So for example, we know that we have to give a greed, we know that we have to give

105
00:10:34,900 --> 00:10:37,580
a panger, we know that we have to give a delusion.

106
00:10:37,580 --> 00:10:44,380
And yet we'll always find little ways, devious ways of justifying our defilements.

107
00:10:44,380 --> 00:10:49,780
So the defilements will not really be the problem, but the problem is this crooked mind

108
00:10:49,780 --> 00:10:52,300
that is quite devious.

109
00:10:52,300 --> 00:10:59,820
So the example here is Meghia, who perhaps even knew that he had lust arising, or knew

110
00:10:59,820 --> 00:11:04,140
that he was being impartial, but he tried to justify it and say, oh, but I want to go

111
00:11:04,140 --> 00:11:05,940
in practice meditation.

112
00:11:05,940 --> 00:11:10,260
And the truth was he was just somehow attracted to this, and he had some incredible desire

113
00:11:10,260 --> 00:11:16,700
to go into that, this growth, which had carried over apparently from his past life.

114
00:11:16,700 --> 00:11:22,740
But we see this with so many things in our lives, we're talking about food, for example.

115
00:11:22,740 --> 00:11:28,980
And when you want this, or want that food, and then you make excuses, well, like it's

116
00:11:28,980 --> 00:11:32,940
good for this, or it's good for that, or when you want to practice yoga, for example,

117
00:11:32,940 --> 00:11:37,140
having an argument about yoga, you want to practice yoga, because you say it's good for

118
00:11:37,140 --> 00:11:44,300
your health, or it's good for this, or it's good for that, but the mind will find many,

119
00:11:44,300 --> 00:11:50,820
many, many excuses to allow us to follow the things that we enjoy, to follow the things

120
00:11:50,820 --> 00:11:58,340
that bring us pleasure, we want, we're talking about even just getting this, we want to

121
00:11:58,340 --> 00:12:00,380
get this drink in the morning.

122
00:12:00,380 --> 00:12:03,180
And you can make ulcers of excuses about how good it is for you.

123
00:12:03,180 --> 00:12:09,060
And so we're arguing, or we're talking, we're saying, but it's so good for you, well,

124
00:12:09,060 --> 00:12:12,540
yes, it's good for us, but it's not good for us to want it.

125
00:12:12,540 --> 00:12:16,700
If you get it, that's great, it'd be good for your body and so on.

126
00:12:16,700 --> 00:12:22,220
And it would even be good for your meditation practice to allow you to continue.

127
00:12:22,220 --> 00:12:25,980
But the greed for it, the wanting for it, of course, that doesn't mean you're going to

128
00:12:25,980 --> 00:12:31,180
get it, and in fact, if you do get it, it's going to, it's come about in an unwholesome

129
00:12:31,180 --> 00:12:41,220
way, and that will be a far worse problem in your meditation, this greed for it.

130
00:12:41,220 --> 00:12:47,620
So we have to be quite patient with our defilements, and we shouldn't let our defilements

131
00:12:47,620 --> 00:12:52,180
define either how good our practice is or how bad our practice is.

132
00:12:52,180 --> 00:12:57,260
So we shouldn't take the defilements as being a sign that we're not practicing well,

133
00:12:57,260 --> 00:13:00,980
or a sign that we're a failure in the practice.

134
00:13:00,980 --> 00:13:07,140
But we also shouldn't take the defilements to, as a just to try to justify them and try

135
00:13:07,140 --> 00:13:12,180
to pretend that they're okay, so we can get what we want and so on.

136
00:13:12,180 --> 00:13:17,140
We do this with our robes, you know, we think, well, I need good robes for this or for

137
00:13:17,140 --> 00:13:19,260
that, or we do it with our lodging.

138
00:13:19,260 --> 00:13:24,660
We do it with all of our, all of our requisites, it's very easy to find excuses for having

139
00:13:24,660 --> 00:13:26,700
good things and so on.

140
00:13:26,700 --> 00:13:28,460
We use it for our meditation practice.

141
00:13:28,460 --> 00:13:32,740
So for example, yoga, I mean, there's nothing intrinsically wrong with yoga.

142
00:13:32,740 --> 00:13:39,140
Yoga can be a good source of concentration and it can create states of mind that are quite

143
00:13:39,140 --> 00:13:47,820
stable and quite powerful, but the justification for it, the idea that somehow it has this

144
00:13:47,820 --> 00:13:52,780
benefit or that benefit and that therefore the clinging to it, this is the problem.

145
00:13:52,780 --> 00:13:58,260
When we take these states as being somehow intrinsically meaningful, we find some just

146
00:13:58,260 --> 00:14:02,100
notification and develop a view on based on them because of course from a Buddhist point

147
00:14:02,100 --> 00:14:08,220
of view, yoga has associated views and the idea that yoga is of some intrinsic benefit

148
00:14:08,220 --> 00:14:15,220
and the samadhi that comes from it is actually enlightenment or so on, which is a philosophy

149
00:14:15,220 --> 00:14:18,060
and the Hindu religion.

150
00:14:18,060 --> 00:14:19,460
The same even goes for wisdom.

151
00:14:19,460 --> 00:14:25,740
We can, you know, when we look at the spectrum of Buddhist thought and Buddhist theory,

152
00:14:25,740 --> 00:14:29,220
there are many different theories and opinions and people even come to the opinion that

153
00:14:29,220 --> 00:14:34,260
an arahan, for example, can still have greed, can still have anger, can still have delusion.

154
00:14:34,260 --> 00:14:38,860
So it's a way of justifying our own defilements because we think, well, I'm an arahan,

155
00:14:38,860 --> 00:14:40,540
but I still have greed, anger and delusion.

156
00:14:40,540 --> 00:14:44,220
Well, then that means an arahan can have greed, anger and delusion.

157
00:14:44,220 --> 00:14:49,340
So we give rise to a very, very wrong view based on our misunderstanding and this is what

158
00:14:49,340 --> 00:14:56,940
we call the crookedness of the mind, the panda nang tapalang, the mind that is not stable,

159
00:14:56,940 --> 00:14:58,900
not straight.

160
00:14:58,900 --> 00:15:03,180
And so we have this very powerful teaching that the Buddha gave on making the mind straight.

161
00:15:03,180 --> 00:15:12,820
This explanation of the Buddha's teaching as being the ujukaro, ujukara, jitudjukama, the act

162
00:15:12,820 --> 00:15:15,940
of making the mind straight.

163
00:15:15,940 --> 00:15:22,980
And this is a really good explanation of why we practice, for example, to note things.

164
00:15:22,980 --> 00:15:27,340
So when you're walking, to note to yourself, walking as the Buddha said, good tantouwagatami

165
00:15:27,340 --> 00:15:28,340
tikatami.

166
00:15:28,340 --> 00:15:30,220
Well, why should you say, I am walking?

167
00:15:30,220 --> 00:15:32,260
You know that you're walking already.

168
00:15:32,260 --> 00:15:35,340
When you have pain, why should you remind yourself that you have pain?

169
00:15:35,340 --> 00:15:38,060
You know that there's pain, it's no good.

170
00:15:38,060 --> 00:15:39,860
It's a cause for suffering.

171
00:15:39,860 --> 00:15:43,500
What we're trying to do is straighten the mind out in regards to the pain.

172
00:15:43,500 --> 00:15:46,620
So instead of thinking, this is my pain, this is bad pain.

173
00:15:46,620 --> 00:15:50,380
Oh, I should move now and justifying yourself saying, wow, if I don't move, maybe I'll

174
00:15:50,380 --> 00:15:53,980
get an injury or so on.

175
00:15:53,980 --> 00:15:57,540
Instead of trying to find that justification, which is considered the crookedness of the mind,

176
00:15:57,540 --> 00:15:59,180
we straighten the mind.

177
00:15:59,180 --> 00:16:03,980
And the mind becomes straight in regards to the pain, and it's only aware of this is pain.

178
00:16:03,980 --> 00:16:07,100
When there's pain, the mind knows there's pain.

179
00:16:07,100 --> 00:16:09,740
When one thinks, then most there's thoughts.

180
00:16:09,740 --> 00:16:11,460
When one has emotions.

181
00:16:11,460 --> 00:16:15,900
When it knows this emotion, there's anger, there's greed, there's delusion, liking,

182
00:16:15,900 --> 00:16:20,740
disliking, drowsiness, and so on, whatever exists in the mind, we know that that exists.

183
00:16:20,740 --> 00:16:26,340
This is making the mind straight, because clearly that when the mind knows that this is

184
00:16:26,340 --> 00:16:32,220
seeing or hearing or pain or so on, when it knows this, this, this, this, this, that's

185
00:16:32,220 --> 00:16:34,860
the straightest the mind can possibly be.

186
00:16:34,860 --> 00:16:39,540
If the mind says, this is me, this is mine, this is good, this is bad.

187
00:16:39,540 --> 00:16:41,580
It's already subjective.

188
00:16:41,580 --> 00:16:46,460
It becomes a crookedness, an entangled, a wavering of the mind.

189
00:16:46,460 --> 00:16:50,020
The mind is no longer fixed on the object itself.

190
00:16:50,020 --> 00:16:54,540
So you say, when walking, I know I'm walking already, well, do you know you're walking?

191
00:16:54,540 --> 00:16:56,260
And how long do you know that you're walking?

192
00:16:56,260 --> 00:16:58,140
Maybe for one instant, you know you're walking.

193
00:16:58,140 --> 00:17:01,860
But then you think, well, it's me that's walking, and this is good walking, or this

194
00:17:01,860 --> 00:17:02,860
is bad walking.

195
00:17:02,860 --> 00:17:07,340
I don't want to walk anymore, I'm bored of walking, I'm tired of walking and so on.

196
00:17:07,340 --> 00:17:12,980
I give rise to all of the partiality, all of the defilements.

197
00:17:12,980 --> 00:17:17,460
So the straightness of the mind is even in regards to the defilements, when you want something,

198
00:17:17,460 --> 00:17:23,100
when you don't like something, when you, even when you believe something, you believe

199
00:17:23,100 --> 00:17:24,500
that this is the truth.

200
00:17:24,500 --> 00:17:28,740
When you say, I believe this is the truth, that's, that's, that's the truth.

201
00:17:28,740 --> 00:17:33,780
But the truth is that you believe it, but it doesn't mean that what you believe is true.

202
00:17:33,780 --> 00:17:37,780
So even when we believe something, I just say thinking, thinking, or believing, believing

203
00:17:37,780 --> 00:17:40,140
if you like.

204
00:17:40,140 --> 00:17:45,940
And this is what makes the mind straight, not the belief, not the idea, not the theory,

205
00:17:45,940 --> 00:17:49,420
not the logic, or so on, because all of that can be crooked.

206
00:17:49,420 --> 00:17:53,980
You can, you can justify just about anything based on the crookedness of the mind.

207
00:17:53,980 --> 00:17:58,060
So this is the first part, very important, and it's a very clear explanation of what we're

208
00:17:58,060 --> 00:18:04,460
trying to do, make the mind straight, when you know that you're seeing, when you know

209
00:18:04,460 --> 00:18:08,660
that seeing is seeing, when you know that you're hearing, that hearing is hearing and

210
00:18:08,660 --> 00:18:13,500
smelling and tasting, and when you know that pain is just pain, whatever arises, when

211
00:18:13,500 --> 00:18:18,140
you know it for what it is, this is making the mind straight, and eventually the mind

212
00:18:18,140 --> 00:18:21,220
naturally inclines towards this sort of experience.

213
00:18:21,220 --> 00:18:28,020
Pain is just pain, thinking is just thinking and so on.

214
00:18:28,020 --> 00:18:34,340
So that's verse number 33, verse number 34, the Buddha also taught for the same story as

215
00:18:34,340 --> 00:18:37,260
a continuation of his explanation.

216
00:18:37,260 --> 00:18:44,140
He uses now instead of the arrow, he uses the example of a fish, a fish out of water, which

217
00:18:44,140 --> 00:18:49,140
is quite interesting, because you think, well, in the natural state, the mind is fickle

218
00:18:49,140 --> 00:18:53,420
and agitated, but it's actually not the natural state.

219
00:18:53,420 --> 00:18:57,420
The natural state of the mind would be a mind at rest.

220
00:18:57,420 --> 00:19:01,500
It has to have some cause for it to become agitated, and this is what happens, this is

221
00:19:01,500 --> 00:19:05,620
what we do throughout our whole lives, because if you look at a child's mind relatively

222
00:19:05,620 --> 00:19:09,740
speaking, even though it's already very agitated, relatively speaking, it's quite simple

223
00:19:09,740 --> 00:19:14,340
and quite direct, if it wants something, it takes it, if it doesn't want something in

224
00:19:14,340 --> 00:19:16,100
cries and so on.

225
00:19:16,100 --> 00:19:21,380
But humans, we are very devious, and we have these stresses and these worries and these

226
00:19:21,380 --> 00:19:28,180
concerns, and these anger and hatred and grudges that children could never have.

227
00:19:28,180 --> 00:19:30,700
And so we've cultivated this.

228
00:19:30,700 --> 00:19:34,180
We've taken our mind out of its natural state.

229
00:19:34,180 --> 00:19:42,340
The Buddha said, pabasarangidang bhikavatitang, this mind among, is beautiful, it's radiant,

230
00:19:42,340 --> 00:19:47,940
but it becomes defiled based on the defilement, based on the greed and anger and delusion

231
00:19:47,940 --> 00:19:54,460
that we cultivate, that we build up in the mind, that we develop in the mind, causing

232
00:19:54,460 --> 00:20:01,780
us to not be stable, to not be straight in the mind.

233
00:20:01,780 --> 00:20:09,700
So actually this idea of the fishes is quite apt, in normal, the way things actually go

234
00:20:09,700 --> 00:20:14,620
for most of us is when we come to meditate, we feel like the meditation is taking us out

235
00:20:14,620 --> 00:20:19,580
of the water, but actually it's quite the opposite.

236
00:20:19,580 --> 00:20:25,820
The meditation is to help bring us back and calm us down and find the water again.

237
00:20:25,820 --> 00:20:32,740
But we become so used to this agitated state that we think when we come to meditate, we

238
00:20:32,740 --> 00:20:36,140
say, let me out here, get me back to my ordinary life because we think that's where

239
00:20:36,140 --> 00:20:38,260
the stability is.

240
00:20:38,260 --> 00:20:41,540
But the truth is that the mind is like this and at all times, the mind is fickle and

241
00:20:41,540 --> 00:20:46,620
wavering and all over, and it's only when we come to meditate that we can see this, so

242
00:20:46,620 --> 00:20:52,580
we think, oh, meditation and Buddhism, that takes us out of the water.

243
00:20:52,580 --> 00:21:01,180
But all it does is show us how to the water we are or how disturbed the mind actually

244
00:21:01,180 --> 00:21:07,140
is, just as a fish becomes disturbed out of water.

245
00:21:07,140 --> 00:21:12,100
And so through the practice, we're coming to let go of these things, we're coming to

246
00:21:12,100 --> 00:21:16,780
give up the hooks, it's like a fish that has been hooked by the fishermen and wriggles

247
00:21:16,780 --> 00:21:23,540
back and forth, so we take out the hooks, everything that we desire, everything that we cling

248
00:21:23,540 --> 00:21:30,300
to, everything that we crave, everything that we hate and avoid and so on.

249
00:21:30,300 --> 00:21:35,700
We take this out, we see the desire, we see the aversion for what it is and we straighten

250
00:21:35,700 --> 00:21:36,700
the mind.

251
00:21:36,700 --> 00:21:41,980
The mind is straight, it can no longer cling and when it doesn't cling anymore, all the hooks

252
00:21:41,980 --> 00:21:47,740
are removed and we're put back into the water in the sand.

253
00:21:47,740 --> 00:21:56,380
And the Buddha said, he explained how this is, or he remarked how this is the realm of

254
00:21:56,380 --> 00:21:59,700
Mara or in one sense, the bait of Mara.

255
00:21:59,700 --> 00:22:04,860
So these hooks are the hooks of Mara, you know, by Mara we mean of course evil, which

256
00:22:04,860 --> 00:22:09,140
is all of the greed and anger and delusion.

257
00:22:09,140 --> 00:22:13,340
So he says this is what we have to let go of and in fact in one way of reading this is

258
00:22:13,340 --> 00:22:18,780
that he's telling us to let go of the mind and if you like it means letting go of the

259
00:22:18,780 --> 00:22:23,420
mind and the spickle, letting go of these mind states when the mind wants something, you

260
00:22:23,420 --> 00:22:27,860
just see that it wants it, you know, the hook is your attachment to it.

261
00:22:27,860 --> 00:22:33,500
If you want something and then you think, yes, I'm going to take it, then you're hooked.

262
00:22:33,500 --> 00:22:38,620
Even if you say no, I'm not going to take it, you get angry and upset at it, no, no, no,

263
00:22:38,620 --> 00:22:44,420
how bad mind is something, it's still a clinging, there's still a hook, you're like, no,

264
00:22:44,420 --> 00:22:50,140
no, no, no, you will stop, stop, stop, you're pushing it down and as a result, you're stuck

265
00:22:50,140 --> 00:22:55,540
with it, you're stuck to it, even with anger, even with disliking it.

266
00:22:55,540 --> 00:23:00,180
So we never want people to give guilty or upset when they have addictions or when they

267
00:23:00,180 --> 00:23:06,460
have a version just to see them clearly, because with any kind of liking or just like

268
00:23:06,460 --> 00:23:11,420
and this is the hook, the clinging, once you make your mind straight and you just know

269
00:23:11,420 --> 00:23:15,540
that is that, that is that, that is that you, it feels exactly as though your mind is

270
00:23:15,540 --> 00:23:20,300
straight and you're no longer clinging to it, the mind no longer clinging, anyway, there's

271
00:23:20,300 --> 00:23:26,780
no way it can cling and the mind is straight, so you remove the hook, this is the hook

272
00:23:26,780 --> 00:23:33,020
of Mar, and this is the pahat, the way where we, the Buddha, it joins us to give it up

273
00:23:33,020 --> 00:23:35,220
and to let go of it.

274
00:23:35,220 --> 00:23:40,060
So that's the teaching for today, just a short story, but two very important verses, one

275
00:23:40,060 --> 00:23:48,100
on the straightening of the mind and another one on the, bringing the mind back to a

276
00:23:48,100 --> 00:24:00,420
state of natural purity, just as a fish in water, the mind that is free from suffering lives

277
00:24:00,420 --> 00:24:05,780
in the world smooth and goes just like a fish in water.

278
00:24:05,780 --> 00:24:09,780
Most of us, we feel like a fish in water out of water, we've gotten so comfortable

279
00:24:09,780 --> 00:24:15,740
with our, with our state of suffering and we think meditation is actually causing suffering,

280
00:24:15,740 --> 00:24:19,820
and it's because only when we come back to meditate, we see what we're doing to ourselves.

281
00:24:19,820 --> 00:24:24,620
And the truth is once you meditate, the more you meditate, the more you realize what you've

282
00:24:24,620 --> 00:24:29,580
been doing to yourself and you see, wow, all that suffering I had in my life, all those terrible

283
00:24:29,580 --> 00:24:33,860
things I was doing to other people, all of the problems I was causing, all of the conflicts

284
00:24:33,860 --> 00:24:43,220
that I had, indeed the Buddha was correct, the mind is fickle, even in our ordinary lives,

285
00:24:43,220 --> 00:24:48,820
in that meditation, the mind will run around, but because you think this is right, this

286
00:24:48,820 --> 00:24:53,300
is good, you follow after it, you don't ever notice it, only when you come to meditate

287
00:24:53,300 --> 00:24:58,340
and you come to try to bring the mind back to a natural state, do you notice it and you

288
00:24:58,340 --> 00:25:01,660
see how crazy the mind is.

289
00:25:01,660 --> 00:25:08,660
So for new meditators, this is a good teaching for us to, to learn, to realize that

290
00:25:08,660 --> 00:25:15,140
this is not a sign that the meditation is wrong or is bad and it shouldn't be a sign,

291
00:25:15,140 --> 00:25:21,300
it shouldn't be something for us to become discouraged about, in the beginning we're dealing

292
00:25:21,300 --> 00:25:29,420
with a mind that is unstable and it takes quite a bit of time and thinks you have many,

293
00:25:29,420 --> 00:25:36,500
many hooks to remove all the hooks slowly, slowly, slowly before you're going to be stable

294
00:25:36,500 --> 00:25:42,580
and at peace, so reminding us that this is the nature of the mind and not to feel guilty

295
00:25:42,580 --> 00:25:48,220
or not to feel angry or not to hate ourselves for it, but just to slowly, slowly see our

296
00:25:48,220 --> 00:25:54,740
defilements as they are, see the suffering as it is, see everything, just as it is and

297
00:25:54,740 --> 00:26:01,380
let go of any desire or aversion towards it, okay, so thank you for tuning in and that's

298
00:26:01,380 --> 00:26:08,380
all for today.

