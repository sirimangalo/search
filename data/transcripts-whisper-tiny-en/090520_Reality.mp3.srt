1
00:00:00,000 --> 00:00:20,760
So today, today I'd like to talk about the Buddha's conception of reality, what is teaching

2
00:00:20,760 --> 00:00:36,840
on reality? Reality is something that is very important for the practice of meditation,

3
00:00:36,840 --> 00:00:51,200
the practice of Yipasana meditation, because Yipasana meditation is for the sole purpose of

4
00:00:51,200 --> 00:01:01,480
coming to see clearly, to understand reality.

5
00:01:01,480 --> 00:01:10,560
So we understand that the reason that we have problems, the reason that we have suffering,

6
00:01:10,560 --> 00:01:19,080
is because we don't fully clearly understand reality.

7
00:01:19,080 --> 00:01:27,800
Because we don't fully understand reality, we can't accept it as it is.

8
00:01:27,800 --> 00:01:41,560
So we always try to change it, force it to keep it the way we want it.

9
00:01:41,560 --> 00:01:49,960
One thing is to be something other than it is, one thing is that are impossible to obtain,

10
00:01:49,960 --> 00:02:01,440
holding on to things which are unable to satisfy us by their very nature, becoming upset

11
00:02:01,440 --> 00:02:17,560
at things which are not in another themselves, stressful or painful or suffering, becoming upset

12
00:02:17,560 --> 00:02:39,880
and disappointed by things which are actually not intrinsically suffering themselves,

13
00:02:39,880 --> 00:02:50,360
which there is no reason to become upset and disappointed about these things.

14
00:02:50,360 --> 00:03:00,000
Because we don't see reality as it is.

15
00:03:00,000 --> 00:03:06,320
So the Lord Buddha, one of the most important parts of the Lord Buddha's teaching is the

16
00:03:06,320 --> 00:03:20,520
explanation, the description, the teaching on reality, the reality in the Lord Buddha's

17
00:03:20,520 --> 00:03:34,160
teaching is split into four parts, basically split into three parts, the first part

18
00:03:34,160 --> 00:03:45,040
is the mind and it is split into two parts which is the mind itself and the qualities

19
00:03:45,040 --> 00:03:52,640
of the mind.

20
00:03:52,640 --> 00:04:10,160
And then there is the body or form material and then there is nibhana or freedom from

21
00:04:10,160 --> 00:04:23,440
body, freedom from mind, freedom of mind, a state of release.

22
00:04:23,440 --> 00:04:37,320
So in basic reality is only three things, mind, matter and nibhana.

23
00:04:37,320 --> 00:04:50,200
If we don't practice meditation, we can't understand this, we can't see how this is true.

24
00:04:50,200 --> 00:05:02,960
But when we look at the generally accepted understanding of the nature of the world, we see

25
00:05:02,960 --> 00:05:11,000
that actually most people understand the world to be only one thing or many people anyway

26
00:05:11,000 --> 00:05:16,040
understand, people who are not religious, scientists and so on, understand the world to

27
00:05:16,040 --> 00:05:23,800
be only one thing and that is what we'd call matter or they would say energy.

28
00:05:23,800 --> 00:05:32,280
We would say the world has made up entirely of energy which can be concentrated in

29
00:05:32,280 --> 00:05:40,440
matter or it can come in the form of light or so on, but in the end there is only one

30
00:05:40,440 --> 00:05:48,720
thing in reality, it is just energy.

31
00:05:48,720 --> 00:05:52,400
And then the many spiritual traditions or religious traditions in the world, they add

32
00:05:52,400 --> 00:06:13,480
things like the soul, God, many, and then many different concepts.

33
00:06:13,480 --> 00:06:19,760
So we have all these concepts which science denies, but we as people we understand to

34
00:06:19,760 --> 00:06:30,920
be real, for instance we say, my house is real, my car is real, my family is real, my

35
00:06:30,920 --> 00:06:39,080
hand is a hand, my foot is a foot and we have all these names for things, but science

36
00:06:39,080 --> 00:06:45,440
also clearly shows that these are not really existing things, that these are simply

37
00:06:45,440 --> 00:06:53,120
names that we give to construct, names that we give to formations.

38
00:06:53,120 --> 00:06:59,480
For instance, a house is not a house until you take all the lumber and all of the bricks

39
00:06:59,480 --> 00:07:04,480
or the shingles and the wood and the metal and so on.

40
00:07:04,480 --> 00:07:12,920
And you put it all together in the right way and it becomes a house, but you can take

41
00:07:12,920 --> 00:07:25,840
the same wood and you can put it into together in another way and you can make a barn

42
00:07:25,840 --> 00:07:26,840
with the same wood.

43
00:07:26,840 --> 00:07:31,440
So it is clear that the house doesn't really exist, all that exists as the wood and

44
00:07:31,440 --> 00:07:36,200
then the wood doesn't even exist, it is atoms and then the atoms don't even exist

45
00:07:36,200 --> 00:07:51,560
in the end, they are just energy or in Buddhism we call matter.

46
00:07:51,560 --> 00:07:58,920
So there are many, many different ideas of what is real.

47
00:07:58,920 --> 00:08:04,280
Why reality is so important, what is important to be clear about what is real is because

48
00:08:04,280 --> 00:08:13,960
it is our misunderstanding of reality, our creation, our proliferation of constructs,

49
00:08:13,960 --> 00:08:26,840
concepts, ideas where we create the idea that this is a person or a place or a thing,

50
00:08:26,840 --> 00:08:39,120
me or mine, we give rise to all of these ideas and concepts and this is what causes

51
00:08:39,120 --> 00:08:49,720
us to attach to hold on, it causes us to suffer.

52
00:08:49,720 --> 00:08:57,360
So when we are engaged in the practice of meditation we simply see things as they are,

53
00:08:57,360 --> 00:09:03,840
we see things arising and seeing coming and changing all the time and so there is no reason

54
00:09:03,840 --> 00:09:06,800
for us to attach to anything.

55
00:09:06,800 --> 00:09:14,920
This is why when we practice meditation we feel so happy, so free, so pure because when

56
00:09:14,920 --> 00:09:19,200
we are really practicing meditation, there is no reason.

57
00:09:19,200 --> 00:09:26,960
We see no reason to hold on to anything, but as soon as we start meditating then we

58
00:09:26,960 --> 00:09:31,920
go back to our slip back into our old way of looking at things and we see things as

59
00:09:31,920 --> 00:09:44,640
me, as mine, as good as bad, we don't see things clearly as they are, it's like our

60
00:09:44,640 --> 00:09:50,680
mind is all bent out of shape and so we suffer our mind is confused, it's caught up in

61
00:09:50,680 --> 00:09:54,520
all sorts of wrong ways of looking at the world.

62
00:09:54,520 --> 00:10:06,680
So it does things like attach and hold on or run away or chase away, push away.

63
00:10:06,680 --> 00:10:11,840
When really this is simply because the mind is not straight the mind is not clear, the

64
00:10:11,840 --> 00:10:17,720
mind doesn't have a clear understanding of reality.

65
00:10:17,720 --> 00:10:21,880
So it's important for us intellectually as well to understand what is the meaning of reality,

66
00:10:21,880 --> 00:10:29,200
so we don't misunderstand when we practice, maybe some people, they can't understand

67
00:10:29,200 --> 00:10:34,480
that the pain is not mine and so they think I am in pain and then they get very angry

68
00:10:34,480 --> 00:10:40,480
and upset and they are not able to continue in the meditation because they are upset,

69
00:10:40,480 --> 00:10:47,120
they are distressed in the experience of the pain, but when we understand how it works

70
00:10:47,120 --> 00:10:54,360
then we can see this in our meditation, we are able to open our hearts to the practice

71
00:10:54,360 --> 00:10:58,840
and we are able to accept the reality of the situation.

72
00:10:58,840 --> 00:11:06,560
When we can control things to be the way we want, we think there is a problem because

73
00:11:06,560 --> 00:11:11,480
we don't understand why this is because we believe in a self, we believe I am the one

74
00:11:11,480 --> 00:11:12,480
controlling.

75
00:11:12,480 --> 00:11:16,000
But when we come to understand what is reality, we see that actually there is no reason

76
00:11:16,000 --> 00:11:23,400
to say me or mine or I and then we are able to accept reality, we are able to understand

77
00:11:23,400 --> 00:11:27,280
why it is not behaving the way we want it to.

78
00:11:27,280 --> 00:11:36,520
The problem isn't that it is going against nature, the problem is that nature is

79
00:11:36,520 --> 00:11:44,320
uncontrollable, the problem is that our one thing, the one thing arises, this is a misunderstanding

80
00:11:44,320 --> 00:11:51,160
of, it is based on a misunderstanding of the reality.

81
00:11:51,160 --> 00:11:56,360
So these things are reality, we have mine, we have body, we have new bananas, so what

82
00:11:56,360 --> 00:11:58,600
are these three things?

83
00:11:58,600 --> 00:12:01,560
What is the nature of these three things?

84
00:12:01,560 --> 00:12:10,440
And the meaning of mind is something which knows an object, this awareness which arises.

85
00:12:10,440 --> 00:12:15,520
So when the foot moves, there is an awareness of the foot moving, when the stomach rises,

86
00:12:15,520 --> 00:12:18,880
there is an awareness of the stomach rising, when there is a pain, there is an awareness

87
00:12:18,880 --> 00:12:27,760
of the pain, when there is a thought, then there is an awareness of the thought.

88
00:12:27,760 --> 00:12:33,200
Normally, this is why we think there is an eye, because we see this continuous knowing

89
00:12:33,200 --> 00:12:36,280
that at all times there is knowing.

90
00:12:36,280 --> 00:12:42,160
But actually the knowing itself is impermanent and if we practice carefully, we can see

91
00:12:42,160 --> 00:12:46,600
that actually it is different minds are rising, there is no reason to say this knowing

92
00:12:46,600 --> 00:12:52,540
is the same as that knowing, sometimes there is a liking of the object, sometimes it

93
00:12:52,540 --> 00:12:59,920
is lacking of the object, the mind is clearly, there is nothing, there is no evidence

94
00:12:59,920 --> 00:13:05,320
to say that this mind is the same mind, this mind is the same mind, this mind is another

95
00:13:05,320 --> 00:13:12,440
mind, it is continuously aware, because the awareness of the foot moving is one kind

96
00:13:12,440 --> 00:13:17,720
of awareness, the awareness of the stomach moving is another kind of awareness.

97
00:13:17,720 --> 00:13:22,120
We have no direct evidence of anything else.

98
00:13:22,120 --> 00:13:28,560
This is because there is nothing else, there is an awareness which arises and ceases,

99
00:13:28,560 --> 00:13:34,760
and then another awareness arises and ceases, and these go together in a chain of the

100
00:13:34,760 --> 00:13:39,200
kind of thing they go and they come and they go.

101
00:13:39,200 --> 00:13:42,960
Sometimes we will be focusing on the rising and the falling, only to suddenly realize

102
00:13:42,960 --> 00:13:47,720
that actually there is a new mind or reason, and even in the beginning we are not aware

103
00:13:47,720 --> 00:13:52,920
that there is a new mind or reason, the mind is upthinking and doesn't occur to us that

104
00:13:52,920 --> 00:13:56,560
we are thinking until another mind arises which says, hey wait, I mean that I am supposed

105
00:13:56,560 --> 00:14:02,240
to be with the stomach, I am supposed to be with the foot, and when this mind arises

106
00:14:02,240 --> 00:14:09,240
then it catches and it realizes that another mind was thinking and it is able to give rise

107
00:14:09,240 --> 00:14:14,200
to another mind which is back at the stomach and so on, it is stable in terms of cause

108
00:14:14,200 --> 00:14:21,560
and effect.

109
00:14:21,560 --> 00:14:25,840
So the mind has a very simple nature, simple knowing and object, this is what is meant

110
00:14:25,840 --> 00:14:34,320
by mind and this clearly exists, and this mind arises at the eyes, sometimes it arises

111
00:14:34,320 --> 00:14:40,640
at the ears, sometimes it arises at the nose, it arises at the tongue, it arises,

112
00:14:40,640 --> 00:14:47,360
in part of the body it arises at the in the mind, sometimes, sometimes we are of thought,

113
00:14:47,360 --> 00:14:57,600
so we are of other minds and so on.

114
00:14:57,600 --> 00:15:02,480
And so in one way we can break down reality into our experience, can be broken down

115
00:15:02,480 --> 00:15:10,600
into six different experiences, so obviously seeing is not the same as hearing,

116
00:15:10,600 --> 00:15:15,440
hearing is not the same as smelling, it is not the same as feeling, it is tasting,

117
00:15:15,440 --> 00:15:24,920
it is feeling and thinking, even though the tongue is a part of the body when the taste

118
00:15:24,920 --> 00:15:32,000
sensorizes, this isn't the same as a feeling, the feeling of the food touching the tongue

119
00:15:32,000 --> 00:15:37,840
is one thing and the taste of the food is a different kind of thing, so this is we break

120
00:15:37,840 --> 00:15:54,320
reality up into six part, and by doing this we have to break away much of our misunderstanding

121
00:15:54,320 --> 00:16:03,200
of the world around us, so that when we see things we are able to break away this idea

122
00:16:03,200 --> 00:16:10,200
of it being good or bad, we are able to break away from even concepts such as us and

123
00:16:10,200 --> 00:16:21,080
them, the person, the object or possession or so on, because we start to see that actually

124
00:16:21,080 --> 00:16:30,560
even no matter what we own as our own belongings, our enjoyment of these things is still

125
00:16:30,560 --> 00:16:37,360
limited to the senses, and really there is no reason why these things are any more enjoyable

126
00:16:37,360 --> 00:16:42,760
than anything else, so we have something beautiful that we see so beautiful and so we love

127
00:16:42,760 --> 00:16:47,440
it and we are attached to it, but when we come to practice meditation we can see that

128
00:16:47,440 --> 00:16:52,480
actually it is just light touching the eye, just like any other light touching the eye and

129
00:16:52,480 --> 00:16:59,400
there is no particular reason why this light should be better than this light, even when

130
00:16:59,400 --> 00:17:09,240
we experience pain in the body, we come to realize that actually it is only our own judgment

131
00:17:09,240 --> 00:17:16,520
that makes it unpleasant, only one other experience, it is the same as a pleasurable experience,

132
00:17:16,520 --> 00:17:24,120
there is nothing particular about it that makes it pleasurable, but why we like and dislike

133
00:17:24,120 --> 00:17:30,920
this is the mental quality, so qualities of mind, this is the second reality, so mind

134
00:17:30,920 --> 00:17:37,360
displayed in the two parts, first we say well, the mind is the characteristic of knowing,

135
00:17:37,360 --> 00:17:42,400
but the knowing can be colored by many different things, it can be colored by liking,

136
00:17:42,400 --> 00:17:50,200
it can be colored by disliking, and it has many different factors in it which make up the

137
00:17:50,200 --> 00:17:56,840
knowing or the process of knowing, so for instance there is the feeling of the object,

138
00:17:56,840 --> 00:18:04,160
maybe a neutral feeling or a happy feeling or a painful feeling, unhappy feeling, maybe

139
00:18:04,160 --> 00:18:15,880
there is a liking or disliking, then there is the recognition of the object as blue as yellow

140
00:18:15,880 --> 00:18:27,680
as a person, an animal, so there are many different qualities of mind which make up this

141
00:18:27,680 --> 00:18:34,880
reality which we call knowing, but in the end when we start to look at reality we start

142
00:18:34,880 --> 00:18:41,360
to see that it is simply the knowing and the thing being known, when we come to see this

143
00:18:41,360 --> 00:18:46,800
as a rising, see, see, see, see, see, see, we start to see that actually there is no mind,

144
00:18:46,800 --> 00:18:52,920
there is no soul, there is no self involved in the process, even the knowing is simply

145
00:18:52,920 --> 00:19:00,280
a rising, see, see, see, it is impermanent, when we see this we stop getting this attachment

146
00:19:00,280 --> 00:19:09,360
to me and to mine, we lose this attachment in wanting to get things and possessing, wanting

147
00:19:09,360 --> 00:19:17,680
to enjoy certain things, wanting to run away from certain things, we start to lose all

148
00:19:17,680 --> 00:19:23,960
interest in our old interest in experiencing certain things disappears because we see that

149
00:19:23,960 --> 00:19:30,560
we can control the awareness, we start to see that we cannot force it so that we only

150
00:19:30,560 --> 00:19:35,240
see good things and we don't see bad things, we can't force it so that we only hear

151
00:19:35,240 --> 00:19:41,040
good things and not hear bad things and smell, only good things, only good smells and

152
00:19:41,040 --> 00:19:48,240
not smell bad smells, taste only good taste but not taste bad taste, we see that we can't

153
00:19:48,240 --> 00:19:54,240
control these things, we can't control reality, there is nothing to do with us, there is

154
00:19:54,240 --> 00:19:59,240
nothing to do with any eye or me or mine, there is no eye or me or mine involved in the

155
00:19:59,240 --> 00:20:07,280
audience and so there arises a new mind, a new set of minds which start to look at things

156
00:20:07,280 --> 00:20:13,080
differently and these minds have a new quality to them, so the mind is the same and that

157
00:20:13,080 --> 00:20:18,600
it knows the object but the qualities of mind are different, since there is no liking

158
00:20:18,600 --> 00:20:25,240
or disliking which arises, this is the development of the progress of meditation, as when

159
00:20:25,240 --> 00:20:31,480
the progress is in meditation you can say that one becomes less intoxicated by the objects

160
00:20:31,480 --> 00:20:37,560
which one experiences, as a result the new minds are also full of much more peace, they're

161
00:20:37,560 --> 00:20:51,240
much more stable minds, the new awareness and the new experience is much more peaceful,

162
00:20:51,240 --> 00:20:56,600
so the most important part of reality is the mind, there is another part of reality which

163
00:20:56,600 --> 00:21:08,400
we accept in Buddhism and we accept it because it has a different quality from the

164
00:21:08,400 --> 00:21:14,920
mind, even though all of experience, all of the reality which we can experience is based

165
00:21:14,920 --> 00:21:23,440
on the mind, there is something else which allows us to complete our explanation of reality

166
00:21:23,440 --> 00:21:30,400
without the other factor that we couldn't explain experience perfectly and this other

167
00:21:30,400 --> 00:21:37,640
factor is matter and this is where science says is the only thing that exists because

168
00:21:37,640 --> 00:21:44,440
science of course is based on matter, it's based on external experiments and empirical

169
00:21:44,440 --> 00:21:56,200
data acquired through energy using matter using energy as the means of investigation,

170
00:21:56,200 --> 00:22:02,000
so when science says mind doesn't exist, it's only a certain type of energy or it's

171
00:22:02,000 --> 00:22:08,080
only part of matter, it's because science cannot by its very nature approach the mind

172
00:22:08,080 --> 00:22:16,640
of course because the mind is not physical, it's not material and so many scientists

173
00:22:16,640 --> 00:22:22,840
are very sure that the mind is just a concept, something which religion has created or

174
00:22:22,840 --> 00:22:31,080
our own ignorance has created, this is only because science has started from the basis

175
00:22:31,080 --> 00:22:38,720
of matter, it started from the basis of physical things and so of course because anything

176
00:22:38,720 --> 00:22:46,360
that falls outside of the physical realm it's impossible for science to approach or to

177
00:22:46,360 --> 00:22:55,320
experiment on it but it's actually quite a ridiculous sort of conclusion because when

178
00:22:55,320 --> 00:23:01,160
we close our eyes we can see that indeed mind does exist and no matter that would science

179
00:23:01,160 --> 00:23:07,200
tend to say that it's only because of matter that we have mind, we can say this is

180
00:23:07,200 --> 00:23:10,680
actually something quite different, mind is something quite different that doesn't

181
00:23:10,680 --> 00:23:21,120
take up space, that doesn't have any form, it's simply annoying which exists but Buddhism

182
00:23:21,120 --> 00:23:25,600
doesn't go to either extreme, doesn't say that only mind exists, it doesn't say that

183
00:23:25,600 --> 00:23:32,280
only matter exists, it accepts the existence of both of them because they occur together

184
00:23:32,280 --> 00:23:38,160
and so the mind which experiences matter is a different mind from the mind which experiences

185
00:23:38,160 --> 00:23:47,520
mind, it actually works in a different way so when mind experiences matter it's experiencing

186
00:23:47,520 --> 00:23:56,640
generally through the body, so when we feel hot this heat is a material, it's an energy

187
00:23:56,640 --> 00:24:05,920
where science can perform experiments on because science uses the physical, so we can say

188
00:24:05,920 --> 00:24:12,120
yes this is hot, what Buddhism says yes this is hot, so heat does exist, cold also

189
00:24:12,120 --> 00:24:18,400
exists, of course science explains that in one way it's saying that it's just the speed

190
00:24:18,400 --> 00:24:26,880
of the speed of the electrons or the speed of the molecules or something like that, when

191
00:24:26,880 --> 00:24:33,720
the molecules are moving rapidly this is heat, when the molecules are moving slowly this

192
00:24:33,720 --> 00:24:40,240
is cold, so science will actually say that actually cold and heat don't exist, in Buddhism

193
00:24:40,240 --> 00:24:47,040
we say they do exist, we say this is one of the four building blocks of reality and so

194
00:24:47,040 --> 00:24:55,640
I guess I suppose we're talking about what science would call energy or something, one

195
00:24:55,640 --> 00:25:02,520
of the energies that science admits exists but from Buddhism from Buddhist point of view

196
00:25:02,520 --> 00:25:08,640
we're looking at from the point of view of the experience of heat by the mind, when

197
00:25:08,640 --> 00:25:17,200
heat arises and the mind experiences as hot or as cold, we say this is heat, this is cold

198
00:25:17,200 --> 00:25:21,040
and actually, what does that mean, it doesn't separate them out either, it just says

199
00:25:21,040 --> 00:25:25,360
this is the element of heat, so you could say it's just the element of energy, does

200
00:25:25,360 --> 00:25:30,720
it have energy or does it not have energy, it's not that there's two realities, one

201
00:25:30,720 --> 00:25:38,000
is cold, one is heat, it's the same reality and it's just relative, and so it is in

202
00:25:38,000 --> 00:25:43,040
line with science, there is no discrepancy here in terms of Buddhism being subjective

203
00:25:43,040 --> 00:25:48,920
or something, Buddhism talks about energy, so it's what we experience, we say this is

204
00:25:48,920 --> 00:25:55,720
hot, we say this is cold, it's because there's an element of energy there, and we can

205
00:25:55,720 --> 00:26:00,480
say that for sure this exists, we can experience it, we'll be sitting, it would feel very

206
00:26:00,480 --> 00:26:08,920
hot, and then the mind can have different qualities of liking or disliking, and when we practice

207
00:26:08,920 --> 00:26:14,120
meditation we say hot, hot, hot, the mind has new qualities, there's a new mind that arises

208
00:26:14,120 --> 00:26:22,680
with new qualities, if we want to see how the mind is working by itself based on constant

209
00:26:22,680 --> 00:26:27,880
effect, we can just sit and say to ourselves hot, hot or cold, cold, and we can see

210
00:26:27,880 --> 00:26:33,840
the new minds which arise, they will arise by themselves, where they no longer dislike

211
00:26:33,840 --> 00:26:39,520
or they no longer upset by the object, and we have pain, it's the same thing, we can

212
00:26:39,520 --> 00:26:45,760
see how yesterday we were saying pain and it was very upsetting to us, but then suddenly

213
00:26:45,760 --> 00:26:53,640
today the mind is inexplicably changed, and the explanation for it is that the minds

214
00:26:53,640 --> 00:26:58,560
which were mindful of the object, which were clearly aware of the object, give rise to

215
00:26:58,560 --> 00:27:04,360
new minds which, as a result of that clear awareness were no longer upset by the pain,

216
00:27:04,360 --> 00:27:06,360
so on.

217
00:27:06,360 --> 00:27:16,920
So this is talking about, first of all, but he talking about matter, talking about heat,

218
00:27:16,920 --> 00:27:26,800
then we have the solidity or rigidity, the hardness, the stiffness of an object, this

219
00:27:26,800 --> 00:27:35,480
you might say is the, I can't remember the exact scientific term that you might equate

220
00:27:35,480 --> 00:27:46,480
it with, but it's something the rigidity or the force of an object, so when you feel

221
00:27:46,480 --> 00:27:56,680
something and it's hard or it feels soft, where it reacts stiff, where it reacts as solid

222
00:27:56,680 --> 00:28:08,680
or firm, or it reacts as malleable, this is the element of earth, let me say the earth

223
00:28:08,680 --> 00:28:15,480
element, but it just seems to mean rigidity or hard being hard of being soft.

224
00:28:15,480 --> 00:28:27,480
And then we have the element of air, but the element of air means the pressure, being

225
00:28:27,480 --> 00:28:36,600
stiff or being flustered, being strong or being weak, sort of thing.

226
00:28:36,600 --> 00:28:41,640
And then we have the water element which is an element which we can't actually experience,

227
00:28:41,640 --> 00:28:47,720
but we know it exists because it's what keeps the elements together, it's what keeps

228
00:28:47,720 --> 00:28:48,720
things together.

229
00:28:48,720 --> 00:28:57,200
It wouldn't be something to do with gravity or it's the pressure, the static, how it

230
00:28:57,200 --> 00:29:08,400
paints sticks to a wall or water sticks together, when we see water, we pour water out,

231
00:29:08,400 --> 00:29:17,360
let me see it sticking together, we see surface tension on a drop of water, the fact that

232
00:29:17,360 --> 00:29:22,880
things stick together, this we call the element of water, and so we say these four elements

233
00:29:22,880 --> 00:29:28,560
exist in reality, and so the western science has given these four elements up I think

234
00:29:28,560 --> 00:29:33,800
a long time ago as sort of antiquated something that the Greeks were very much interested

235
00:29:33,800 --> 00:29:34,800
in.

236
00:29:34,800 --> 00:29:40,240
But I think it's most likely that the Greeks caught this through their Buddhist contact

237
00:29:40,240 --> 00:29:47,600
in India, or through the may not have been the Buddhist directly, but through the same

238
00:29:47,600 --> 00:29:54,400
tradition of approaching reality from an experiential point of view.

239
00:29:54,400 --> 00:30:01,160
So science looks at this and says it's very quaint and very simplistic, not realizing

240
00:30:01,160 --> 00:30:10,880
that it is the very building blocks of reality as it can be experienced, as it exists

241
00:30:10,880 --> 00:30:15,720
according to our experience of it.

242
00:30:15,720 --> 00:30:19,760
So it's simply another way of looking at the world, when we look at the world from an

243
00:30:19,760 --> 00:30:25,280
experiential point of view, when we look at the world from an external point of view,

244
00:30:25,280 --> 00:30:29,160
when we look at the world from an external point of view, we're limited to many sorts

245
00:30:29,160 --> 00:30:36,440
of constructs and concepts, and we're limited to very physical reality.

246
00:30:36,440 --> 00:30:42,520
And in fact, this physical reality doesn't reflect the true nature of experience, and

247
00:30:42,520 --> 00:30:49,320
so we find ourselves becoming very physical, very material, very attached to the material,

248
00:30:49,320 --> 00:30:57,160
material pleasures, and material understandings, and so we come up with many, many theories

249
00:30:57,160 --> 00:30:59,640
even are scientific theories.

250
00:30:59,640 --> 00:31:04,720
In the end, they have very great trouble explaining reality, because they can only go

251
00:31:04,720 --> 00:31:10,720
so far and then they come to something they could get in, but they can't really understand

252
00:31:10,720 --> 00:31:15,720
reality, or it's not the way they thought, but we see that even three-dimensional reality

253
00:31:15,720 --> 00:31:23,400
is not sufficient, it's not as real as we thought it was.

254
00:31:23,400 --> 00:31:28,760
And to the point that I've said before, there are some physicists who say, actually, the

255
00:31:28,760 --> 00:31:37,840
universe is actually most likely just a single entity, it actually takes up no space.

256
00:31:37,840 --> 00:31:46,320
It's most likely permanent, it's most likely unchanging, it's most likely spaceless, and

257
00:31:46,320 --> 00:31:53,040
all that's happening is our experience of it is, it's like refraction or something,

258
00:31:53,040 --> 00:31:58,480
it's creating all of this, so in reality, actually, the universe is not existent, it's

259
00:31:58,480 --> 00:32:04,440
what they come to, and I think this is because many physicists have started to look at

260
00:32:04,440 --> 00:32:11,200
eastern philosophies like Hinduism and the horse Hindu philosophy says that the world is

261
00:32:11,200 --> 00:32:16,920
one, we are one, and this has become a very spiritual thing, most people think Buddhism

262
00:32:16,920 --> 00:32:24,040
teaches this as well, that we are one, and all is one, and so on, something like that.

263
00:32:24,040 --> 00:32:30,640
But Buddhism teaches reality as it exists, it doesn't have any theoretical idea about

264
00:32:30,640 --> 00:32:33,720
something beyond the experience.

265
00:32:33,720 --> 00:32:38,360
Buddhism doesn't have any, we don't have any interest, of course, because we're trying

266
00:32:38,360 --> 00:32:43,000
to cure something, we're trying to heal our minds, we're not trying to come to an absolute

267
00:32:43,000 --> 00:32:48,680
answer, you know, how did the world arise, or when is it going to cease, or how big is

268
00:32:48,680 --> 00:32:54,200
the world, or so on, we're trying to fix a problem, we're trying to cure, we're trying

269
00:32:54,200 --> 00:32:59,960
to do something that's useful, because it's terribly unuseful for us, it's completely

270
00:32:59,960 --> 00:33:05,480
unuseful for us to know when did the universe start, when will the universe cease, how

271
00:33:05,480 --> 00:33:14,360
big is the universe, these things are not helpful, but a scientific examination of experiences

272
00:33:14,360 --> 00:33:19,560
incredibly helpful, this is what we can see through the practice, we can see that coming

273
00:33:19,560 --> 00:33:23,880
to understand reality is the most useful thing, because this is what leads us to the

274
00:33:23,880 --> 00:33:31,080
third and the final reality, this is release or freedom, where the mind becomes straight,

275
00:33:31,080 --> 00:33:39,680
where the mind comes to see clearly, the reason that we give rise to more and more minds

276
00:33:39,680 --> 00:33:45,800
is because we hold on and we focus, we create friction, we could say, we're creating

277
00:33:45,800 --> 00:33:57,400
a kind of a stress, we're making energy, we're creating more, through our karma, through

278
00:33:57,400 --> 00:34:05,040
our intention, we create, we give rise to new minds, so when we get angry, for instance,

279
00:34:05,040 --> 00:34:09,560
we can say we create, we can see that we create many things, we create suffering here

280
00:34:09,560 --> 00:34:17,720
and now as we're angry, we create suffering for other people, we create feelings of guilt,

281
00:34:17,720 --> 00:34:23,240
we create many more minds into the future and much more attachment, as we get more and

282
00:34:23,240 --> 00:34:28,480
more angry, we start to do more and more things and our minds become more and more attached,

283
00:34:28,480 --> 00:34:33,960
more and more bent out of shape, more and more crazy.

284
00:34:33,960 --> 00:34:37,960
And as we start to practice mindfulness, we see our mind starting to let go, starting to

285
00:34:37,960 --> 00:34:42,200
give up, starting to become less and less and less and less and less.

286
00:34:42,200 --> 00:34:48,120
To the point where the mind no longer runs around, no longer reaches out and we find

287
00:34:48,120 --> 00:34:55,120
our lives becoming simpler and simpler, less and less complicated, less and less stressful,

288
00:34:55,120 --> 00:35:06,080
we're starting to find ourselves becoming more and more peaceful all the time.

289
00:35:06,080 --> 00:35:10,960
And as we do this more and more and more, our minds eventually just let go and we're

290
00:35:10,960 --> 00:35:15,440
able to give up this world, we're able to give up an essential experience, we're able

291
00:35:15,440 --> 00:35:27,440
to give up any sort of experience whatsoever, really, in terms of seeing, hearing, smelling,

292
00:35:27,440 --> 00:35:33,360
tasting, feeling, and thinking, any sort of impermanence is given up and so we become,

293
00:35:33,360 --> 00:35:39,840
or you can say, experience becomes a permanent, it becomes an enters into a state of being

294
00:35:39,840 --> 00:35:54,160
which is not impermanence, which is not stressful, which is free from suffering and so

295
00:35:54,160 --> 00:35:58,960
this is what we call nibana, nibana is free known or it means release, it's when we finally

296
00:35:58,960 --> 00:36:04,720
let go and we do this simply through understanding reality, we're coming to see that actually

297
00:36:04,720 --> 00:36:11,840
all that exists is the mind and the object of experience, which can be either the body

298
00:36:11,840 --> 00:36:16,680
or it can be another mind when we're thinking in front of the seat, when we realize the

299
00:36:16,680 --> 00:36:24,000
last mind the new mind arises saying, oh, that mind was this, it is, it is when the feeling

300
00:36:24,000 --> 00:36:30,480
arises in the body, then the new mind arises and says, there's a feeling and all we're

301
00:36:30,480 --> 00:36:33,960
doing is because we don't see this, because we don't understand this, it's like these

302
00:36:33,960 --> 00:36:42,600
minds that arise, they're like dark minds, like unclear minds, minds without light, and

303
00:36:42,600 --> 00:36:46,920
we can see this is how it really feels when we practice meditation, when we practice

304
00:36:46,920 --> 00:36:50,320
meditation, it's like shining a light on.

305
00:36:50,320 --> 00:36:56,680
Many people who practice tranquility meditation before are often shocked by the clarity

306
00:36:56,680 --> 00:37:01,200
of mind that comes through vipassana meditation, they may have practiced meditation

307
00:37:01,200 --> 00:37:07,480
for a long time, and only now are they shocked by seeing how clear the mind becomes when

308
00:37:07,480 --> 00:37:14,280
you practice vipassana meditation, how you have to see so much more of reality, how it's

309
00:37:14,280 --> 00:37:19,480
like waking up, of course even more for those people who have never practiced meditation,

310
00:37:19,480 --> 00:37:24,000
it's a real wake up call, but they come to see many things about them, they didn't realize

311
00:37:24,000 --> 00:37:30,880
that they have no clue, this is because the minds that were arising were like sleeping

312
00:37:30,880 --> 00:37:36,680
minds, minds that were only halfway, when we practice meditation, our mind becomes more

313
00:37:36,680 --> 00:37:43,520
and more awake, more alert, and we're able to see reality more and more clearly.

314
00:37:43,520 --> 00:37:48,280
So this is the teaching that I'd like to give today just to help us to understand what

315
00:37:48,280 --> 00:37:53,840
do we mean by reality, it's actually very, very little in regards to what is really

316
00:37:53,840 --> 00:38:00,000
and truly real, most of our reality is based on concept, most of it's based on our own

317
00:38:00,000 --> 00:38:03,120
evolution or misunderstanding of reality.

318
00:38:03,120 --> 00:38:09,360
So we create all these ideas of heaven, of hell, of God, of peace, of that, based on

319
00:38:09,360 --> 00:38:12,640
the misunderstanding of experience and misunderstanding of reality.

320
00:38:12,640 --> 00:38:19,960
When we come to see that reality is experiential, we come to sort out many of the misunderstandings

321
00:38:19,960 --> 00:38:23,640
and as a result, many of the attachments until finally we're able to do away with all

322
00:38:23,640 --> 00:38:28,440
of us understanding and all of our attachments, when we do away with all of these, then

323
00:38:28,440 --> 00:38:35,000
we live in peace and happiness and eventually realize, for ourselves, total freedom

324
00:38:35,000 --> 00:38:37,320
comes up here.

325
00:38:37,320 --> 00:38:42,360
So I'd like to encourage everyone to please continue on your path, practice, so that we

326
00:38:42,360 --> 00:38:48,200
can offer ourselves a real nice complete freedom of suffering, thank you, let's all

327
00:38:48,200 --> 00:39:03,760
get it.

