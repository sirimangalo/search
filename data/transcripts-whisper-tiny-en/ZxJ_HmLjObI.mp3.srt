1
00:00:00,000 --> 00:00:12,000
Hello everyone, so here I am, I've come to visit with my uncle, just a couple more days in Bangkok, and then back to Canada,

2
00:00:12,000 --> 00:00:18,000
and there'll be lots more good things to come.

3
00:00:18,000 --> 00:00:24,000
I just thought, you know, it's been a while since I've actually given any sort of teaching over YouTube,

4
00:00:24,000 --> 00:00:32,000
so I thought, good time to have something to fill in the gap.

5
00:00:32,000 --> 00:00:40,000
Well, people are waiting patiently for my Dhamma Padha video updates, and so on and so on.

6
00:00:40,000 --> 00:00:51,000
I've been thinking a lot lately about the solution to the big problems in the world.

7
00:00:51,000 --> 00:01:03,000
In Thailand, now they have big problems, and there's a lot of talk, even among meditators and Buddhists about how to fix the problems.

8
00:01:03,000 --> 00:01:11,000
And you've got me thinking about some of the bigger issues.

9
00:01:11,000 --> 00:01:25,000
The paths that people are on, like, what you want out of the world, out of life, out of existence, as well as the ways that you go about to get it, to achieve your goals.

10
00:01:25,000 --> 00:01:31,000
And I think, in the end, that's what it comes down to, the answers to these two questions.

11
00:01:31,000 --> 00:01:37,000
One, where are you going, and two, what are you doing, or how do you get there?

12
00:01:37,000 --> 00:01:39,000
What are you doing to get there? How do you get there?

13
00:01:39,000 --> 00:01:43,000
What do you do to achieve your goals?

14
00:01:43,000 --> 00:01:51,000
And so I thought I'd do a video to describe this, because it applies to the Thai situation.

15
00:01:51,000 --> 00:01:57,000
But that would be the reason why I've been thinking about it.

16
00:01:57,000 --> 00:02:03,000
This video, the idea here, is to apply the principles on a wider scale.

17
00:02:03,000 --> 00:02:17,000
The reason it's been on my mind, or the reason this video idea came to mind, is sort of a realization of how important it is to not only do good deeds for yourself,

18
00:02:17,000 --> 00:02:20,000
but to also encourage other people in good deeds.

19
00:02:20,000 --> 00:02:25,000
So it awoke this desire to help others.

20
00:02:25,000 --> 00:02:35,000
And again, I thought, this kind of thought is important to get out there. It's important to remind people and to encourage each other in the practice of goodness.

21
00:02:35,000 --> 00:02:39,000
So this video is for that purpose.

22
00:02:39,000 --> 00:02:44,000
I've thought also to do one in Thai, which I may undertake.

23
00:02:44,000 --> 00:02:51,000
As well, I don't know how many Thai followers I have, but the original impetus was to address the Thai situation.

24
00:02:51,000 --> 00:02:58,000
Because there's a lot of people trying to address the issue, so life throws us a problem.

25
00:02:58,000 --> 00:03:02,000
How do we solve it? What do we focus on?

26
00:03:02,000 --> 00:03:11,000
And in this case, many people are focusing on the political situation, trying to solve the political crisis,

27
00:03:11,000 --> 00:03:19,000
trying to arrange so that their favorite person can start running things, or their favorite people,

28
00:03:19,000 --> 00:03:25,000
so that things can be run in a way that they would like, so that the country can be run in a certain way.

29
00:03:25,000 --> 00:03:31,000
So we put our focus on a country, for example. Our focus goes on many different things.

30
00:03:31,000 --> 00:03:36,000
Our focus might be on our family, or it might be on our religion.

31
00:03:36,000 --> 00:03:48,000
It's often on the country, it might be on our job, or it might be on the environment, or on science.

32
00:03:48,000 --> 00:03:55,000
It might be on knowledge and learning and so on. It might be on gaining magical powers, wherever our focus might be.

33
00:03:55,000 --> 00:04:01,000
Expanding this idea, it's clear that everyone has, or in general, people have their focus.

34
00:04:01,000 --> 00:04:08,000
And so this is the answer in the first question is, what is our focus? What do we want out of life?

35
00:04:08,000 --> 00:04:15,000
And I think this is an important question that people have to be able to answer.

36
00:04:15,000 --> 00:04:23,000
And oftentimes we fall into a problem right from the start here, because we focus on the wrong things.

37
00:04:23,000 --> 00:04:29,000
So we focus on things that have a limited benefit or result.

38
00:04:29,000 --> 00:04:38,000
And so when we approach the first issue is to ask ourselves, why?

39
00:04:38,000 --> 00:04:46,000
Are we focusing on X, Y, or Z? And what is the result that comes from focusing on that?

40
00:04:46,000 --> 00:04:55,000
So for example, if your focus is on the political situation, the question is, by focusing on that, what comes?

41
00:04:55,000 --> 00:05:06,000
And for many people, all that is come is stress, violence, anger, frustration, sadness, a lot of bad things.

42
00:05:06,000 --> 00:05:10,000
And people in Thailand, for example, are quite stressed at this point.

43
00:05:10,000 --> 00:05:15,000
Even people who are generally strong meditators have given up their meditation,

44
00:05:15,000 --> 00:05:21,000
have found themselves unable to relax, unable to calm down, unable to focus, unable to see clearly.

45
00:05:21,000 --> 00:05:24,000
And so find themselves getting upset usually.

46
00:05:24,000 --> 00:05:31,000
So clearly we see here, in my mind, a improper focus.

47
00:05:31,000 --> 00:05:41,000
So either path or the direction that they're heading is problematic.

48
00:05:41,000 --> 00:05:46,000
It's causing problems for them and is instigating the problems in the world.

49
00:05:46,000 --> 00:05:50,000
So it's creating more conflict.

50
00:05:50,000 --> 00:05:54,000
In a general sense, we have this all over the world.

51
00:05:54,000 --> 00:05:58,000
If it's not politics, then people are focusing on worldly gain, for example.

52
00:05:58,000 --> 00:06:07,000
Many people, even spiritual people, find themselves focusing on how to acquire a material or worldly gain,

53
00:06:07,000 --> 00:06:09,000
or even heavenly gains.

54
00:06:09,000 --> 00:06:12,000
So people are always obsessed with how they can get more.

55
00:06:12,000 --> 00:06:18,000
A lot of spiritual practices in the West, especially, are particularly,

56
00:06:18,000 --> 00:06:26,000
even in the East, really all around the world here in Thailand as well, are particularly focused on what spiritual practices can I do

57
00:06:26,000 --> 00:06:34,000
in order to increase my affluence, in order to increase my wealth, my physical comfort.

58
00:06:34,000 --> 00:06:41,000
And not only does this seem quite crass, but if we go along with it, we can still ask,

59
00:06:41,000 --> 00:06:44,000
okay, so once you get the affluence, what really have you done?

60
00:06:44,000 --> 00:06:48,000
If you look at your own benefit, you've spoiled your mind, you've corrupted your mind,

61
00:06:48,000 --> 00:06:54,000
and all you have left is this incredible need for comfort and luxury that never satisfies you.

62
00:06:54,000 --> 00:07:04,000
And in the end, leaves you rotten inside, and spoiled inside, and whining and complaining about the smallest disruption.

63
00:07:04,000 --> 00:07:08,000
And when you die, you die less satisfied than most.

64
00:07:08,000 --> 00:07:14,000
And then for the world as well, the problem with affluence is that you take from other people.

65
00:07:14,000 --> 00:07:21,000
You end up stealing and or not even stealing, but using resources that other people could use,

66
00:07:21,000 --> 00:07:26,000
and you wind up destroying the environment, and you're including the world, and so on.

67
00:07:26,000 --> 00:07:32,000
Affluence is not sustainable, and it's not something that you can spread around to everyone.

68
00:07:32,000 --> 00:07:36,000
So meaning in general, this path is not something that leads to greatness.

69
00:07:36,000 --> 00:07:45,000
It's not something that leads to any purpose or benefit, so you have to ask yourself why.

70
00:07:45,000 --> 00:07:52,000
I think the closest that people get while still missing the point is, for example, here in Thailand,

71
00:07:52,000 --> 00:07:56,000
some people have talked to me about the idea of putting good people in charge,

72
00:07:56,000 --> 00:08:00,000
and how important it is to make sure bad people don't get in charge of, say, the country.

73
00:08:00,000 --> 00:08:03,000
I think that still misses the point.

74
00:08:03,000 --> 00:08:10,000
If we're trying to create systems, for example, we want to change capitalist systems to be socialist systems,

75
00:08:10,000 --> 00:08:14,000
or socialist systems to be capitalist systems, or whatever we believe in.

76
00:08:14,000 --> 00:08:22,000
We're still dealing with a entity that is, and here's the point, outside of our sphere of influence,

77
00:08:22,000 --> 00:08:24,000
outside of our universe.

78
00:08:24,000 --> 00:08:29,000
So when I think of Thailand, most of Thailand is nothing to me, and never will be anything to me.

79
00:08:29,000 --> 00:08:33,000
I will have nothing to do with and have no control over.

80
00:08:33,000 --> 00:08:35,000
I have no influence over even.

81
00:08:35,000 --> 00:08:38,000
Much of Thailand is just conceptual to me.

82
00:08:38,000 --> 00:08:47,000
So when I conceive of Thailand, I've extended myself out of reality into conceptualization.

83
00:08:47,000 --> 00:08:52,000
So I've got all these ideas about what Thailand is, and what's going on in this part, and that part,

84
00:08:52,000 --> 00:08:55,000
and so many variables that are outside of my control.

85
00:08:55,000 --> 00:09:06,000
For example, if I were to take Thailand as my focus, all I would end up with is stress and suffering,

86
00:09:06,000 --> 00:09:10,000
when things didn't go my way.

87
00:09:10,000 --> 00:09:14,000
I don't think it's proper for us to just say focus on yourself,

88
00:09:14,000 --> 00:09:19,000
which I think is the general Buddhist response, is that, okay, so you can't fix the world.

89
00:09:19,000 --> 00:09:21,000
You can't even fix the country.

90
00:09:21,000 --> 00:09:25,000
So instead, only focus on doing good deeds for yourself.

91
00:09:25,000 --> 00:09:31,000
And a lot of other people have said this to me and said, look, I've realized I can't help Thailand, for example.

92
00:09:31,000 --> 00:09:38,000
But I can do something for myself. So what I have to do is just make myself free myself from suffering,

93
00:09:38,000 --> 00:09:39,000
free myself from the problems.

94
00:09:39,000 --> 00:09:47,000
And I think not only is that you could say selfish, but even if you accept it and say, great, okay, go for it.

95
00:09:47,000 --> 00:09:56,000
I think it's unsustainable, and it doesn't work in practice.

96
00:09:56,000 --> 00:10:02,000
Why? Because you do have a universe that surrounds you. It won't be Thailand, or it won't be the world,

97
00:10:02,000 --> 00:10:08,000
or it won't even be necessarily your family, or your society, your town, whatever.

98
00:10:08,000 --> 00:10:13,000
But it will be a universe that surrounds you. It'll be certain people who come into your life.

99
00:10:13,000 --> 00:10:17,000
And, of course, when they leave your life, you consider them out of your universe.

100
00:10:17,000 --> 00:10:20,000
But it will be these people that surround you.

101
00:10:20,000 --> 00:10:25,000
And if you're not influencing them, I would say your own personal practice is, of course, bound to suffer.

102
00:10:25,000 --> 00:10:33,000
A bound to, in fact, fail because of the influence of these other people and other aspects of your universe.

103
00:10:33,000 --> 00:10:41,000
I see that's crucial, is that the path that we should take, if I'm going to give an answer to this first question, what should our focus be?

104
00:10:41,000 --> 00:10:45,000
It should be on our universe. And we should understand clearly what that means.

105
00:10:45,000 --> 00:10:52,000
It means first and foremost ourselves. But second, it means all of that which surrounds us, which changes.

106
00:10:52,000 --> 00:10:56,000
But also has some stability to it.

107
00:10:56,000 --> 00:11:02,000
For example, our friends and the people who we consider to be in line with our views,

108
00:11:02,000 --> 00:11:09,000
we should be not only improving our own situation, but we should also be improving their situation.

109
00:11:09,000 --> 00:11:17,000
And the means of improvement that I would suggest, and that I think is in line with the Buddhist teaching,

110
00:11:17,000 --> 00:11:21,000
is the cultivation of what we call goodness in its many forms.

111
00:11:21,000 --> 00:11:27,000
So it doesn't just mean meditation. It doesn't mean we just force everyone to push everyone or push ourselves to meditation.

112
00:11:27,000 --> 00:11:33,000
It means we cultivate goodness in ourselves and in the world around us.

113
00:11:33,000 --> 00:11:37,000
So we are generous, generous with ourselves, but generous with other people.

114
00:11:37,000 --> 00:11:47,000
We're kind, we are moral and ethical, we are considerate and alert and aware of our actions and our speech.

115
00:11:47,000 --> 00:11:58,000
We are present, we are mindful, we are wise, we are patient, we are cultivate all of these good qualities.

116
00:11:58,000 --> 00:12:07,000
Not just by meditation, but by acts and speech that are mindful, thoughtful, considerate and kind and so on.

117
00:12:07,000 --> 00:12:13,000
I think with that as our focus, there is so much good that we can do in the world.

118
00:12:13,000 --> 00:12:22,000
And the problem often is not that we don't have the power to do good, but that we waste a lot of our energy on things that are useless.

119
00:12:22,000 --> 00:12:33,000
For example, fighting, protesting, et cetera, et cetera, and of course, in central indulgence and so on.

120
00:12:33,000 --> 00:12:48,000
And if we were to just take time out of our days to cultivate goodness, new good deeds, practice meditation, cultivate morality, patience and all of these good qualities.

121
00:12:48,000 --> 00:12:53,000
I think there is so much change that could come apart from the world.

122
00:12:53,000 --> 00:12:55,000
I think immediately our universe would change.

123
00:12:55,000 --> 00:12:59,000
And I suppose that's really the point is that you can't change the world.

124
00:12:59,000 --> 00:13:04,000
You can't change the universe. Everything changes and comes and goes.

125
00:13:04,000 --> 00:13:06,000
This earth is going to be destroyed.

126
00:13:06,000 --> 00:13:22,000
Anyone who is clinging to the idea of helping this earth is in for great disappointment when eventually the earth burns to a crisp and is destroyed by the power of the sun, et cetera.

127
00:13:22,000 --> 00:13:32,000
And so I was living a short-sighted dream. And in fact, if we want to find an ultimate good, we have to look beyond the physical realm, beyond the physical earth.

128
00:13:32,000 --> 00:13:40,000
So this is the answer to the first question, sort of in brief, the practice of cultivation of goodness both for ourselves and others. That's my answer.

129
00:13:40,000 --> 00:13:45,000
Now the answer is to how we go about this. There's many, many ways of answering this.

130
00:13:45,000 --> 00:13:54,000
And I think probably my favorite one, because it's so general, is what the Buddha called the four roads to success or the four edipada.

131
00:13:54,000 --> 00:14:06,000
Which are the four paths to power or success or magic, even if you want edi, which means really just power.

132
00:14:06,000 --> 00:14:10,000
But success, the meaning is if you want to succeed at anything, you need these four things.

133
00:14:10,000 --> 00:14:18,000
So here we have this idea of choosing our path. And with any path you choose, in order to succeed, I would say this is the answer.

134
00:14:18,000 --> 00:14:26,000
How do you succeed? How do you go about cultivating your paths? Follow these four qualities.

135
00:14:26,000 --> 00:14:43,000
The first one is contentment or desire, I suppose, or contentment, somewhere between contentment and desire, meaning being happy with what you're doing, being content to do what you're doing.

136
00:14:43,000 --> 00:15:01,000
So if you're not content with goodness, if your contentment lies in sensuality, for example, your content to indulge and to eat and to to to greed, then the cultivation of goodness suffers as a result, because your contentment lies elsewhere.

137
00:15:01,000 --> 00:15:21,000
Your content to work really hard, your content, or your volition, your zealous towards politics, or science, or study, or whatever, then you'll succeed in that direction.

138
00:15:21,000 --> 00:15:32,000
So the first thing is for us to succeed in the cultivation of goodness, is to be content, to cultivate goodness. And I think this comes from the first discussion.

139
00:15:32,000 --> 00:15:43,000
So the start, this is where the start is to decide on the path. So when you discuss the virtues of various paths, it leads you to make a decision on the path, and therefore be content with it.

140
00:15:43,000 --> 00:15:53,000
As a result of deciding that this is the best path, you become content with that path. The second thing you need, then, or what even comes from contentment is effort.

141
00:15:53,000 --> 00:15:59,000
Once your content, you've chosen your path. The second road to success, the Buddha said, is you need to work.

142
00:15:59,000 --> 00:16:09,000
So it's not enough that you have decided this is the right path, and it's your philosophy and life. It has to become your religion, which means you have to take it seriously and actually practice it.

143
00:16:09,000 --> 00:16:18,000
This requires effort. So if we're going to, here today I'm coming to encourage you in this, and in order to encourage you, I have to encourage you to actually go out and do good things.

144
00:16:18,000 --> 00:16:31,000
It doesn't mean you have to go out and become Mother Teresa, or the Buddhist equivalent, or you have to go running around, finding people to meditate, or working all the time, stressing out about it.

145
00:16:31,000 --> 00:16:43,000
But it means that you have to actually do something. So if you want to do this, take the effort, take the decision, and go and do things. Be generous. Be kind. Share the meditation practice with others.

146
00:16:43,000 --> 00:16:52,000
Send links about meditation practice and Buddhism and goodness or any kind. All around the world. Do what you can to spread good things.

147
00:16:52,000 --> 00:17:02,000
Also to yourself. Have the effort to sit down and meditate, have the effort to change some things about yourself that you don't like, that you wish weren't there.

148
00:17:02,000 --> 00:17:12,000
Cultivate. Actually go about cultivating good things. The third thing that you need is what we call jitta, which means you have to keep it in mind.

149
00:17:12,000 --> 00:17:24,000
So if you want to do this, you are putting out the effort to do it. The effort has to be focused, and the third aspect is the focusing of the mind, reminding yourself.

150
00:17:24,000 --> 00:17:39,000
So you have this effort, you have this enthusiasm, but then the enthusiasm gets carried away with something else, or it gets diffused, or it doesn't find it outlet because you've forgotten all about what you're intending to do.

151
00:17:39,000 --> 00:17:51,000
You've gone off and gotten lost in something else. Jitta means keeping it in mind. In anything that you do in order to succeed, it has to be something that you are constantly in mind.

152
00:17:51,000 --> 00:18:04,000
It's at the forefront of your mind. There's various ways of doing this. You can have artificial means as far as surrounding yourself with good people, or writing down little reminders, or putting in your book.

153
00:18:04,000 --> 00:18:10,000
Business people will have a day planner that reminds them when they have to do certain things.

154
00:18:10,000 --> 00:18:22,000
So we can have a day planner for good things, reminding ourselves that you can have artificial means, but most important is just remind yourself, or have a determination in your mind to do these sorts of things.

155
00:18:22,000 --> 00:18:35,000
Once a day, or ask yourself, today, have I done anything good, or what sorts of good things am I going to do, and really think about it and consider keeping in mind in your daily life.

156
00:18:35,000 --> 00:18:46,000
Don't just live your life. The point is, don't just live your life for the attainment of central pleasures, or for the busyness of work and relationships, and so on and so on.

157
00:18:46,000 --> 00:18:54,000
Take some time out to actually change your world, to actually cultivate goodness inside of yourself, and goodness in the people around you.

158
00:18:54,000 --> 00:19:00,000
This is number three. And finally, number four, you have to be wise about what you do.

159
00:19:00,000 --> 00:19:05,000
So for some people, they set their minds on something, and they work really hard at it.

160
00:19:05,000 --> 00:19:23,000
Because they're not reflecting on whether they're doing it properly, or efficiently, or in the correct way correctly, they fail, even though they put out lots and lots of effort.

161
00:19:23,000 --> 00:19:34,000
So there's this picture of people pushing cubes around, and working really hard pushing these cubes around, and then somebody comes by with a sphere.

162
00:19:34,000 --> 00:19:40,000
And with even less effort, because he's turned it into a sphere, it's much easier. It's not about how hard you work.

163
00:19:40,000 --> 00:19:48,000
But the fourth one, we monks that, means considering the right ways to do things in the wrong ways to do things in adjusting your practice.

164
00:19:48,000 --> 00:19:57,000
So in the example of practicing goodness, people might work really hard to help poor people, for example.

165
00:19:57,000 --> 00:20:10,000
You go running around trying to find beggars on the streets, and you give them some money. But after a while, you see that by giving money to beggars, it doesn't really help the world, nor does it help me, or does it make my universe a better place?

166
00:20:10,000 --> 00:20:16,000
Because maybe these people go off, and just by alcohol, or maybe it just gives them food.

167
00:20:16,000 --> 00:20:20,000
Well, that's not really the most efficient use of my time. So maybe I could teach them meditation.

168
00:20:20,000 --> 00:20:29,000
Or maybe I could teach successful people how to meditate. And they could spread out, and teach others.

169
00:20:29,000 --> 00:20:33,000
So this is an example that I've taken, because I'm not really interested in teaching everyone how to meditate.

170
00:20:33,000 --> 00:20:40,000
But you pick the best people, this is how the Buddha looked at it. He said, well, I'll pick the very, very best people who are right ready to teach.

171
00:20:40,000 --> 00:20:49,000
And if I get all of them advanced in the meditation, then they will then teach the next level, and we'll teach the next level, and it will be much more efficient.

172
00:20:49,000 --> 00:20:55,000
The practice of goodness, it should be this way. Ask yourself, don't just go around doing random acts of kindness.

173
00:20:55,000 --> 00:21:00,000
It's a great thing. But in the end, you've been inefficient in your practice.

174
00:21:00,000 --> 00:21:07,000
You've done many, many good things, and still your world is full of hatred and full of anger and conflict.

175
00:21:07,000 --> 00:21:13,000
Because you haven't addressed the bigger issues. You haven't been efficient in your use of resources.

176
00:21:13,000 --> 00:21:16,000
You haven't really hit the core issues.

177
00:21:16,000 --> 00:21:26,000
So when you do your good deeds, focus, or in this aspect, means consider carefully.

178
00:21:26,000 --> 00:21:30,000
What is the best thing in the most efficient use of your time and your resources?

179
00:21:30,000 --> 00:21:35,000
How can you go about really influencing the world?

180
00:21:35,000 --> 00:21:40,000
And one example is the importance of first helping yourself before you help others.

181
00:21:40,000 --> 00:21:50,000
Some people, good deeds, goodness, means running around helping other people, or trying to change other people to make them better, or to make them conform to your idea of what is right.

182
00:21:50,000 --> 00:22:02,000
Without actually making yourself a better person, so you run around and get angry at people because they're not sympathetic, or because they're not in harmony, and you end up creating disharmony.

183
00:22:02,000 --> 00:22:10,000
And this happens quite often with NGOs, or with activists, and people who are trying to change the world in a good way.

184
00:22:10,000 --> 00:22:19,000
At the very least they get burnt out, but also they often create as much conflict as they try to appease.

185
00:22:19,000 --> 00:22:25,000
So consideration is important. To recap, you need to be content with what you do.

186
00:22:25,000 --> 00:22:29,000
You need to agree with me in this that we need to create goodness in the world.

187
00:22:29,000 --> 00:22:36,000
You can get this far that we want to create goodness in the world, and we've done much to set ourselves in the right direction.

188
00:22:36,000 --> 00:22:39,000
But that's not enough. The second thing you need to put out effort.

189
00:22:39,000 --> 00:22:45,000
You have to actually do these things, after actually work to create goodness in the world.

190
00:22:45,000 --> 00:22:47,000
Number three, you have to keep your mind on it.

191
00:22:47,000 --> 00:22:51,000
Remind yourself about it. Keep your focus on goodness.

192
00:22:51,000 --> 00:22:53,000
Don't focus on the bad things.

193
00:22:53,000 --> 00:22:56,000
Don't read the news about how the world is bad in this way.

194
00:22:56,000 --> 00:23:01,000
If you want to read and make sure, read the headlines and find out what's going on fine,

195
00:23:01,000 --> 00:23:12,000
but don't delve in deeper and try to find exactly who's responsible, who's at fault, or so on.

196
00:23:12,000 --> 00:23:15,000
We'll cut that out. Focus on the goodness.

197
00:23:15,000 --> 00:23:22,000
Focus on what can we do, what aspects of the world need help, and where can we apply our efforts in our world, in our universe?

198
00:23:22,000 --> 00:23:29,000
And number four, consider the best way to do it.

199
00:23:29,000 --> 00:23:38,000
Be introspective or analytical, analyze, study, and be discriminating about where you put your efforts.

200
00:23:38,000 --> 00:23:51,000
Try to find the best means, or the best place to put your effort in, and try to figure out the most efficient and the most correct.

201
00:23:51,000 --> 00:23:59,000
And useful means to achieve the end, which is goodness, peace, happiness, and freedom from suffering.

202
00:23:59,000 --> 00:24:06,000
So there you go. There's a new video for me, and I hope it's been interesting and informative and helpful.

203
00:24:06,000 --> 00:24:13,000
And I really, really hope that this is an impetus for people to go out there and do good deeds as far as spreading goodness,

204
00:24:13,000 --> 00:24:19,000
spreading the teachings that lead to goodness, and spreading peace, happiness, and freedom from suffering.

205
00:24:19,000 --> 00:24:26,000
So there you go, wishing you all the best, peace.

206
00:24:26,000 --> 00:24:52,000
Hope that took.

