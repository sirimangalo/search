1
00:00:00,000 --> 00:00:04,800
Okay, one rather simple question about eating and meditation.

2
00:00:04,800 --> 00:00:09,880
For example, I am doing something which will take me half an hour, or hour to finish.

3
00:00:09,880 --> 00:00:15,320
And after that I am thinking to do some meditation, but during this half hour I become hungry.

4
00:00:15,320 --> 00:00:20,920
Should I eat and when I finish doing what I am doing meditate or not eat and keep working,

5
00:00:20,920 --> 00:00:27,200
then do the meditation on hungry stomach, or maybe just have a snack.

6
00:00:27,200 --> 00:00:35,760
It is not exactly a simple question, there is a lot in there that is a bit, I am not quite

7
00:00:35,760 --> 00:00:43,840
clear on the whole of the question.

8
00:00:43,840 --> 00:00:50,000
But one thing I would say which is kind of smurmy is that your work should be meditation

9
00:00:50,000 --> 00:00:52,880
and your eating should be meditation.

10
00:00:52,880 --> 00:00:56,480
So that is really where your answer, that is the most important part of the answer, it is

11
00:00:56,480 --> 00:00:58,960
not really smurmy at all.

12
00:00:58,960 --> 00:01:09,600
It is probably not the answer that most people would expect, but the meditation starts now.

13
00:01:09,600 --> 00:01:14,840
It doesn't start 30 minutes from now when you are going to meditate, because then you are

14
00:01:14,840 --> 00:01:21,080
wasting 30 minutes thinking it is okay, in 30 minutes I will meditate or an hour I will meditate

15
00:01:21,080 --> 00:01:28,080
or after this I will meditate, and during all that 30 minutes or hour you are wasting

16
00:01:28,080 --> 00:01:30,120
good meditation time.

17
00:01:30,120 --> 00:01:34,840
So when you work you should try to be mindful, when you eat you should try to be mindful.

18
00:01:34,840 --> 00:01:38,040
You should work on that.

19
00:01:38,040 --> 00:01:45,640
Why it is not such a smurmy answer after all is that that will really help your formal meditation.

20
00:01:45,640 --> 00:01:50,240
If a person just does formal meditation it doesn't think at all about meditation during

21
00:01:50,240 --> 00:01:56,880
their daily life, the formal meditation becomes quite difficult and it is a constant uphill

22
00:01:56,880 --> 00:02:02,320
struggle, because it is out of tune with your ordinary reality.

23
00:02:02,320 --> 00:02:07,200
So you should try to constantly be bringing your mind back to attention, I mean even right

24
00:02:07,200 --> 00:02:15,160
now here we are sitting together, we should try to make it a mindful experience, being

25
00:02:15,160 --> 00:02:22,360
aware at least of our emotions, likes and dislikes, the distractions in the mind, the feelings

26
00:02:22,360 --> 00:02:25,600
that we have.

27
00:02:25,600 --> 00:02:29,840
If you are just listening here, because I am talking I have to do more action, but for

28
00:02:29,840 --> 00:02:36,680
some of you just listening you can just revert back to your meditation unless you have

29
00:02:36,680 --> 00:02:41,600
questions.

30
00:02:41,600 --> 00:02:45,480
But as far as me making decisions as to when you should do eating and when you should

31
00:02:45,480 --> 00:02:52,760
do meditating, meditating on an empty stomach is interesting, can be useful to help you

32
00:02:52,760 --> 00:02:59,040
to learn about hunger.

33
00:02:59,040 --> 00:03:05,600
But I'd say generally you need the food for the energy for the meditation, so better

34
00:03:05,600 --> 00:03:09,040
to have a little bit of food before you meditate.

35
00:03:09,040 --> 00:03:14,440
I don't meditate any time, whenever you meditate, meditate, get some time for it and do

36
00:03:14,440 --> 00:03:16,000
it.

37
00:03:16,000 --> 00:03:17,000
Just don't procrastinate.

38
00:03:17,000 --> 00:03:23,480
It kind of sounds like you might be talking about procrastinating, which is dangerous.

39
00:03:23,480 --> 00:03:29,720
So you know it goes do work for a half an hour and then I'll do, then I'll have a snack

40
00:03:29,720 --> 00:03:33,640
and then when I'm having a snack I think about oh then I have to go check Facebook or

41
00:03:33,640 --> 00:03:38,040
email and check email and then something comes up in email and I have to call this person

42
00:03:38,040 --> 00:03:42,960
or that person or do this or I have to go to the store and then you never meditate after

43
00:03:42,960 --> 00:03:44,960
all.

44
00:03:44,960 --> 00:03:53,440
That kind of thing is dangerous, so better to be clear with yourself that if you really need

45
00:03:53,440 --> 00:04:07,240
food and you're really hungry than eat or eat a little bit, but sometimes it's just procrastinating

46
00:04:07,240 --> 00:04:11,120
because meditation can be difficult and the mind will actually develop a version towards

47
00:04:11,120 --> 00:04:18,840
it if you're not careful and if you're not aware of the aversion, if you're not looking

48
00:04:18,840 --> 00:04:24,760
at it and contemplating it as well, it can become a great hindrance and more than a hindrance

49
00:04:24,760 --> 00:04:29,680
it can actually trick you into every time you think of meditation your mind says oh he's

50
00:04:29,680 --> 00:04:34,600
thinking of meditation again, let's divert his attention, hey look at that, that's nice

51
00:04:34,600 --> 00:04:40,680
or hey aren't you hungry, hey you want to go for it, want to go for burgers or something

52
00:04:40,680 --> 00:04:46,600
right the mind will do this to you because it doesn't want amenities like oh no don't

53
00:04:46,600 --> 00:05:08,600
put me back there, that's torture, so be careful of that one.

