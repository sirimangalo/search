Hi, so this is an answer to a question on whether women can enter the monk's life.
And the short answer is, of course, yes.
And without getting into too much detail about it, just to point out that it's still
quite difficult.
And I think the biggest difficulty is just that there are so few female monks in existence.
So not only is it difficult to ordain, but I think even more so it's difficult to find a place
to stay, because a lot of the female monks are being supported by a small group of people
and don't have the room or the facilities to accommodate more.
That being said, if you're really interested, there are ways to get there, and you should
understand that it's a long process, becoming a monk for anyone should be a slow and gradual
process to be sure that you're really ready and really, really know what you're getting
yourself into.
If you're really interested in it, the best and really the only thing that I can suggest
is that you can get in touch with me or with my group, and there is a possibility that once
we have a place established, I mean even in the next year or so, that I'll be having
females ordain as nuns as monks.
So I hope that it clears a little bit up, just to encourage you in this idea, the thought
of getting away from the sensual world and entering into a lifestyle that is more conducive
towards meditation and peace.
So thanks for the question and getting coming.
