1
00:00:00,000 --> 00:00:06,520
So, neither brain nor pleasure described us, or the occasion on which a sense we have

2
00:00:06,520 --> 00:00:10,960
profitable consciousness as a reason accompanied by equanimity.

3
00:00:10,960 --> 00:00:19,760
I don't think we should use equanimity for filling.

4
00:00:19,760 --> 00:00:20,760
Neutral filling.

5
00:00:20,760 --> 00:00:26,360
I think it's a better word.

6
00:00:26,360 --> 00:00:32,440
Equanimity about insight is a name for equanimity consisting a neutrality about investigation,

7
00:00:32,440 --> 00:00:33,920
just like that.

8
00:00:33,920 --> 00:00:41,200
It is more or less the same as equanimity about formations.

9
00:00:41,200 --> 00:00:47,320
Because equanimity about formations has to do with re-personal meditation, just as it is,

10
00:00:47,320 --> 00:00:50,880
with this equanimity about insight is.

11
00:00:50,880 --> 00:00:57,760
What has become that he abandoned and he obtains equanimity?

12
00:00:57,760 --> 00:00:59,960
Equanimity, especially neutrality.

13
00:00:59,960 --> 00:01:09,040
This is the real equanimity which is one of the mental factors in Bali, it is called tatra

14
00:01:09,040 --> 00:01:12,520
matcha tada.

15
00:01:12,520 --> 00:01:14,320
The next one is equanimity of Jana.

16
00:01:14,320 --> 00:01:19,920
It's a name for equanimity producing impartiality towards them.

17
00:01:19,920 --> 00:01:35,480
This is also not filling upika, it is Jana upika, it is specific equanimity.

18
00:01:35,480 --> 00:01:40,120
And the next one is purifying equanimity, it is a name for equanimity purified of all

19
00:01:40,120 --> 00:01:51,120
oppositions and through consisting an uninterestedness in stealing oppositions, that's

20
00:01:51,120 --> 00:01:53,120
quite does.

21
00:01:53,120 --> 00:02:05,640
Since all oppositions are purified of all oppositions, there is no interest in stealing

22
00:02:05,640 --> 00:02:10,320
them because he has achieved stealing the oppositions.

23
00:02:10,320 --> 00:02:18,320
After this achievement, he has no longer interest in stealing them because they are still

24
00:02:18,320 --> 00:02:19,320
already.

25
00:02:19,320 --> 00:02:30,760
That is called purifying equanimity and this is also the specific equanimity.

26
00:02:30,760 --> 00:02:38,840
Now, here in six factors, equanimity, equanimity as defined equanimity as an enlightenment

27
00:02:38,840 --> 00:02:44,840
factor, equanimity as specific neutrality, equanimity of Jana and purifying equanimity

28
00:02:44,840 --> 00:02:51,800
of one in meaning, one in meaning means one in reality, one in essence.

29
00:02:51,800 --> 00:02:58,040
And that is equanimity as specific neutrality.

30
00:02:58,040 --> 00:03:05,840
The difference over is one of position or occasion, like the difference in the single

31
00:03:05,840 --> 00:03:10,040
being as a ball, a youth, and a general, a king, and so on.

32
00:03:10,040 --> 00:03:14,640
Therefore, these should be understood that equanimity as an enlightenment factor, et cetera,

33
00:03:14,640 --> 00:03:20,360
and not form where there is six-factor equanimity, they are mutually exclusive.

34
00:03:20,360 --> 00:03:26,240
And that six-factor equanimity, et cetera, and not form where there is equanimity as

35
00:03:26,240 --> 00:03:30,160
an enlightenment factor.

36
00:03:30,160 --> 00:03:37,880
Just as this one meaning, that means just as these are the same in essence or in reality,

37
00:03:37,880 --> 00:03:44,760
so also equanimity, what formations and equanimity about inside have one meaning too, for

38
00:03:44,760 --> 00:03:48,000
they are simply understanding, punya.

39
00:03:48,000 --> 00:03:58,700
So these two are not the specific neutrality, but they are really understanding of

40
00:03:58,700 --> 00:04:06,240
punya, because they arise during vipassana meditation, for they are simply understanding

41
00:04:06,240 --> 00:04:11,440
class and these two ways according to function, just as and so on.

42
00:04:11,440 --> 00:04:22,680
Then, down to the end of correct, 170, equanimity of energy and equanimity of feeling are different

43
00:04:22,680 --> 00:04:28,760
both from each other and from the rest.

44
00:04:28,760 --> 00:04:36,360
That means equanimity of energy means just energy, and equanimity of feeling is the neutral

45
00:04:36,360 --> 00:04:41,600
feeling.

46
00:04:41,600 --> 00:04:56,160
How many of the equanimity we get in essence or in reality, specific neutrality, understanding

47
00:04:56,160 --> 00:05:07,280
energy and feeling, so in essence there are full kinds of equanimity, in detail there

48
00:05:07,280 --> 00:05:09,800
are 10 kinds of equanimity.

49
00:05:09,800 --> 00:05:20,640
So whenever we see the word equanimity, we have to understand which equanimity is meant here.

50
00:05:20,640 --> 00:05:35,720
Then in this phrase, he dwells in equanimity, what would be judgment, specific neutrality,

51
00:05:35,720 --> 00:05:43,960
not feeling equanimity, because we are still in the third genre, in the third genre there

52
00:05:43,960 --> 00:05:51,720
is still Sukha, which is not neutral feeling, Sukha is a pleasurable feeling.

53
00:05:51,720 --> 00:06:03,200
So here equanimity means specific neutrality or impartiality, and he is mindful and fully aware

54
00:06:03,200 --> 00:06:13,960
and experiences Sukha, both bodily and mental, with his mental body.

55
00:06:13,960 --> 00:06:27,640
Now for a graph 176, it is better in translation than in the original part, in the original

56
00:06:27,640 --> 00:06:42,920
part, but it is the sentences are very involved, and then with the commonity they used

57
00:06:42,920 --> 00:06:51,160
many relative pronouns, and one relative of another, and then the other relative of that,

58
00:06:51,160 --> 00:06:56,760
and so it goes on like 6-8, it is very difficult to understand.

59
00:06:56,760 --> 00:07:04,080
I don't know why he wrote that way, but in the translation it is better, we can understand.

60
00:07:04,080 --> 00:07:13,840
So now, he feels means he experiences, he experiences Sukha, what Sukha is used in the

61
00:07:13,840 --> 00:07:19,840
party, and here Sukha means both bodily, Sukha and mental, Sukha.

62
00:07:19,840 --> 00:07:30,480
Now, when this genre arises, there is this Sukha feeling, I mean pleasurable feeling.

63
00:07:30,480 --> 00:07:38,320
So when there is pleasurable feeling, he cannot help but experience it, although he has

64
00:07:38,320 --> 00:07:47,880
no intention that I will experience or I will enjoy this Sukha, since that Sukha feeling

65
00:07:47,880 --> 00:07:57,240
is accompanying the genre, he just experiences it.

66
00:07:57,240 --> 00:08:05,400
So here, with his mental body means other mental states.

67
00:08:05,400 --> 00:08:16,120
Now, when the third genre arises, there are other changes he goes also arising with it.

68
00:08:16,120 --> 00:08:22,360
So there is third genre consciousness or third genre changes, and then there are three

69
00:08:22,360 --> 00:08:23,360
diseases.

70
00:08:23,360 --> 00:08:24,360
Right?

71
00:08:24,360 --> 00:08:32,160
So among these diseases, there are beauty, Sukha, and take care of that.

72
00:08:32,160 --> 00:08:40,440
They are the three are genre factors, and actually they are called genres.

73
00:08:40,440 --> 00:08:46,200
The other competence are here called mental body.

74
00:08:46,200 --> 00:08:58,560
So there is one consciousness, and then, let me see, 35, 33, there are 33 diseases.

75
00:08:58,560 --> 00:09:11,600
So there is one data, third genre data, and 33 diseases, and among them, and there is

76
00:09:11,600 --> 00:09:17,000
there is feeling, there is pleasurable feeling, which is Sukha.

77
00:09:17,000 --> 00:09:26,800
And since Sukha arises together with other mental competence, the person is said to experience

78
00:09:26,800 --> 00:09:32,000
the Sukha with mental competence.

79
00:09:32,000 --> 00:09:40,360
So mental competence are here called mental body, and partly the word kaya is used.

80
00:09:40,360 --> 00:09:51,440
And then, when this third genre arises, it causes material properties to arise, because

81
00:09:51,440 --> 00:09:59,680
meta is caused by, some meta is caused by kama, some by chita, I mean consciousness,

82
00:09:59,680 --> 00:10:04,320
some by climate, and some by food.

83
00:10:04,320 --> 00:10:15,680
So when the third genre ticked arises, it causes material properties to arise too.

84
00:10:15,680 --> 00:10:23,640
And since it is very refined state of mind, state of consciousness, the material properties

85
00:10:23,640 --> 00:10:29,160
it produces are also very, very fine and very subtle.

86
00:10:29,160 --> 00:10:43,800
And then, very fine and very subtle material properties are the tangible objects.

87
00:10:43,800 --> 00:10:53,560
And then, also, the meditator or one who has chana experiences, so they are feeling,

88
00:10:53,560 --> 00:11:07,840
or they are feeling arises through contact of his body with other tangible things,

89
00:11:07,840 --> 00:11:13,760
tangible objects, like sitting on a soft cushion or something like that.

90
00:11:13,760 --> 00:11:26,040
So during the time when the third genre arises, he is said to experience mental sukha

91
00:11:26,040 --> 00:11:33,440
with his mental body and physical sukha also with mental body.

92
00:11:33,440 --> 00:11:45,000
Because what we call feeling of weird enough is mental in the mind.

93
00:11:45,000 --> 00:11:54,880
What we call feeling of sensations generally is the material properties in the body.

94
00:11:54,880 --> 00:12:04,200
So suppose there is pain here, and pain is a collection of material properties here.

95
00:12:04,200 --> 00:12:09,280
And we feel that pain, and that feeling of pain is in our mind.

96
00:12:09,280 --> 00:12:18,640
So while we have a good touch, so there is a feeling, so that that good touch is material

97
00:12:18,640 --> 00:12:26,880
property, and that we feel in our mind as sukha, as a pleasure.

98
00:12:26,880 --> 00:12:35,240
So here, also, when the person gets a third genre, his mind is so refined that it produces

99
00:12:35,240 --> 00:12:44,960
very refined material properties, and that these material properties are all scattered

100
00:12:44,960 --> 00:12:53,400
through his body, or through with his body, and so he feels very pleasant sensations

101
00:12:53,400 --> 00:12:57,080
in the body, through to mind.

102
00:12:57,080 --> 00:13:06,960
So that is why it describes his described as experiencing sukha with his mental body.

103
00:13:06,960 --> 00:13:22,840
So mental body means a group of mental, mental concomitants, a group of mental factors.

104
00:13:22,840 --> 00:13:26,600
And he enters upon and draws in the third genre, on the bottom of which the noble ones

105
00:13:26,600 --> 00:13:30,920
are known, with regard to the person who has attained the third genre.

106
00:13:30,920 --> 00:13:37,640
He has equanimity, he is mindful, and he draws in sukha.

107
00:13:37,640 --> 00:13:50,760
And why do the noble ones recommend, or I think, praise is better than the water command

108
00:13:50,760 --> 00:13:54,840
here.

109
00:13:54,840 --> 00:14:00,160
In praise of, you see something in praise of something, right?

110
00:14:00,160 --> 00:14:15,920
So here, in the noble ones, praise this person, who has attained the third genre, because

111
00:14:15,920 --> 00:14:28,320
he can keep himself in pleasure, not attached to even the very high form of, high form

112
00:14:28,320 --> 00:14:33,880
of pleasure, sukha.

113
00:14:33,880 --> 00:14:44,440
And his mind is so, he is so mindful in such a way that he can keep PD from arising,

114
00:14:44,440 --> 00:14:50,000
because at the third genre, PD does not arise.

115
00:14:50,000 --> 00:14:54,720
That is why, and this person is body of praise.

116
00:14:54,720 --> 00:15:07,160
He can keep in partiality, and he can keep a weak PD by being very mindful.

117
00:15:07,160 --> 00:15:16,320
And he draws in sukha, which is, which is experienced or enjoyed by all noble persons.

118
00:15:16,320 --> 00:15:23,560
That is why he is worthy of, worthy of praise.

119
00:15:23,560 --> 00:15:29,840
So in order to praise him, they say, this person has equanimity, this person is mindful,

120
00:15:29,840 --> 00:15:37,160
and this person dwells in sukha.

121
00:15:37,160 --> 00:16:05,640
And in this genre, there are how many factors, 3, 1, 3 or 2, you have to, I just stand

122
00:16:05,640 --> 00:16:11,160
this for full method, not 5 full methods.

123
00:16:11,160 --> 00:16:31,720
So only 2, right, sukha, and take kakada, the unification of mind, so do fight us.

124
00:16:31,720 --> 00:16:45,720
Yes, okay, this is a third genre, then after third genre, we go to full genre.

125
00:16:45,720 --> 00:16:53,320
But when it says he has equanimity, his mind is full and well in sukha, that his mind

126
00:16:53,320 --> 00:17:03,480
also is not the third factor of mindfulness.

127
00:17:03,480 --> 00:17:15,160
Mindfulness is with all jalas, mindfulness can never be without jalas, but here his

128
00:17:15,160 --> 00:17:25,840
mindfulness is so, so good that it can keep beauty from arising.

129
00:17:25,840 --> 00:17:32,920
When you pick attention, the close attention to what's happening at the moment, and you

130
00:17:32,920 --> 00:17:39,720
can keep even beauty away from you, or a beauty from arising in your mind.

131
00:17:39,720 --> 00:17:45,920
That is why he is pleased as having mindfulness or being mindful, not just ordinary

132
00:17:45,920 --> 00:17:58,000
mindfulness, he has, this is very powerful and refined kind of mindfulness.

133
00:17:58,000 --> 00:18:06,840
And he dwells in sukha, this sukha is not an ordinary sukha, it's very un-un-for-the-go,

134
00:18:06,840 --> 00:18:20,800
I'm mixed on solid and it is the one experienced or enjoyed by noble persons.

135
00:18:20,800 --> 00:18:43,120
Now we go to the fold, now he has to do the same thing, to find fault with here what, sukha.

136
00:18:43,120 --> 00:18:53,080
It is going to neutral feeling now, so in this paragraph also, about the middle of the

137
00:18:53,080 --> 00:18:59,160
paragraph 181, you know, the knowing there, so it should not be knowing, but when the

138
00:18:59,160 --> 00:19:05,320
food jalas is about to arise, they arise in him, mind away at wedding and so on.

139
00:19:05,320 --> 00:19:11,040
Now, food jalas is different from other jalas, because the food jalas are accompanied by

140
00:19:11,040 --> 00:19:19,800
neutral feeling, but the first, second, and third jalas are accompanied by pleasurable

141
00:19:19,800 --> 00:19:20,800
feeling.

142
00:19:20,800 --> 00:19:33,480
Now, when we talk about the different conditions in Bhatana, that there are 24 causal

143
00:19:33,480 --> 00:19:39,600
revisions in Bhatana, we call them conditions here, so when we talk about repetition

144
00:19:39,600 --> 00:19:48,400
condition, then, since it is repetition, it must be of the same nature.

145
00:19:48,400 --> 00:20:02,600
That means, when moments of consciousness arise one after the other, the previous consciousness

146
00:20:02,600 --> 00:20:13,560
or matter of confidence arising together with previous consciousness can be the condition

147
00:20:13,560 --> 00:20:23,240
as repetition condition of the succeeding consciousness only when they are of the same

148
00:20:23,240 --> 00:20:24,240
nature.

149
00:20:24,240 --> 00:20:32,560
Sometimes, pleasurable must be followed by pleasurable, neutral must be followed by neutral.

150
00:20:32,560 --> 00:20:42,760
So, if neutral followed pleasurable, there can be no relationship as repetition.

151
00:20:42,760 --> 00:20:56,160
So, in this food jalas, food jalas, thought process, pleasant feeling is not a condition

152
00:20:56,160 --> 00:21:03,240
for neither painful nor pleasant feeling, and the preliminary work must be aroused in the

153
00:21:03,240 --> 00:21:10,000
case of the food jalas with neither painful nor pleasant feeling, consequently peace, consciousness

154
00:21:10,000 --> 00:21:16,760
of preliminary work are associated with neither painful nor pleasant feeling, and here

155
00:21:16,760 --> 00:21:25,600
happiness vanishes, simply owing to their association with equanimity, what's that?

156
00:21:25,600 --> 00:21:43,680
Now, what the comedy they are saying is, the pleasurable feeling cannot be a repetition

157
00:21:43,680 --> 00:21:56,800
condition for neutral feeling.

158
00:21:56,800 --> 00:22:01,880
Now, in the food jalas, there is neutral feeling and not pleasurable feeling, and if you

159
00:22:01,880 --> 00:22:11,080
look at the thought process, the jalas consciousness is preceded by four or three moments

160
00:22:11,080 --> 00:22:21,360
of sense fear, sense fear, jalanas, or impersonations.

161
00:22:21,360 --> 00:22:33,280
So, since in the first thought process, since number five is accompanied by neutral

162
00:22:33,280 --> 00:22:37,960
feeling, the others must also be accompanied by neutral feeling.

163
00:22:37,960 --> 00:22:43,720
So, that is what the comedy they are saying, that is the only difference.

164
00:22:43,720 --> 00:22:54,400
In the previous jalas, the sense fear, impersonations are accompanied by pleasurable feeling,

165
00:22:54,400 --> 00:22:59,760
and jalanas, jalanas movements are also accompanied by pleasurable feeling.

166
00:22:59,760 --> 00:23:08,320
But in this thought process for food jalanas, since jalanas be accompanied by neutral

167
00:23:08,320 --> 00:23:17,320
feeling, the preceding karma, which jalanas must also be preceded by a company by neutral

168
00:23:17,320 --> 00:23:18,320
feeling.

169
00:23:18,320 --> 00:23:23,440
So, that is the only difference, that is what is what the comedy they are saying.

170
00:23:23,440 --> 00:23:33,880
And here, in this translation, there is some error, because I already did it again, but

171
00:23:33,880 --> 00:23:39,520
there is this difference, blissful place and feeling is not in condition as repetition

172
00:23:39,520 --> 00:23:50,280
condition for neither painful, nor pleasant feeling, and the preliminary work must be

173
00:23:50,280 --> 00:23:55,960
aroused in the case of the food jalanas, with neither painful nor pleasant feeling.

174
00:23:55,960 --> 00:24:02,240
Actually, it should say, and in the food jalanas, neither painful nor pleasant feeling

175
00:24:02,240 --> 00:24:05,560
must arise.

176
00:24:05,560 --> 00:24:10,840
It is not in the preliminary, but it is in the full jalanas.

177
00:24:10,840 --> 00:24:17,320
The neutral feeling must arise, there must be neutral feeling.

178
00:24:17,320 --> 00:24:28,920
Therefore, the consciousness of preliminary work, that means karma of jalanas preceding

179
00:24:28,920 --> 00:24:43,400
the ruba of jalanas, must be accompanied by neither painful, nor pleasant feeling.

180
00:24:43,400 --> 00:24:50,840
Here, happiness vanishes, simply owing to their association with equanimity, here equanimity

181
00:24:50,840 --> 00:24:58,960
means neutral feeling, because neutral feeling is feeling, and happiness of sukha is feeling.

182
00:24:58,960 --> 00:25:05,480
So, if it is happiness, it cannot be neutral, and if it is neutral, it cannot be happiness.

183
00:25:05,480 --> 00:25:11,560
So, since the full jalanas is accompanied by neutral feeling, there can be no sukha

184
00:25:11,560 --> 00:25:16,360
feeling there, there can be no pleasurable feeling.

185
00:25:16,360 --> 00:25:24,200
So, here, happiness vanishes, simply owing to their association with equanimity.

186
00:25:24,200 --> 00:25:38,880
It may be a turn off for many people, because it is happiness vanishes, for a way to do.

187
00:25:38,880 --> 00:25:44,880
But, happiness here is pleasurable feeling.

188
00:25:44,880 --> 00:25:57,360
So, the neutral feeling is considered to be higher than the pleasurable feeling.

189
00:25:57,360 --> 00:26:05,000
So, in this full jalanas, pleasurable feeling vanishes, and in its place, neutral feeling

190
00:26:05,000 --> 00:26:06,000
arises.

191
00:26:06,000 --> 00:26:13,280
This neutral feeling is so refined, that it is almost a kind of sukha, although it is

192
00:26:13,280 --> 00:26:20,800
not technically sukha, it is the kind of sukha, which such people enjoy.

193
00:26:20,800 --> 00:26:28,920
So, the full jalanas, with the abandoning of sukha and dukha, and with the previous disappearance

194
00:26:28,920 --> 00:26:34,880
of somenas and dominoes, he enters the born and dures in the full jalanas, which is neither

195
00:26:34,880 --> 00:26:41,880
pain nor pleasure, and which has purity of mindfulness caused by equanimity.

196
00:26:41,880 --> 00:26:48,640
Now, with the abandoning of sukha and dukha, and with the previous disappearance of somenas

197
00:26:48,640 --> 00:26:55,640
and dominoes, sir, you know, there are hundreds of thousands of thousands of thousands

198
00:26:55,640 --> 00:27:08,080
of years old.

199
00:27:08,080 --> 00:27:09,760
They're five kinds of feeling.

200
00:27:09,760 --> 00:27:14,420
Sukha feeling, dukha feeling, somalanas are feeling, dominoes are feeling, uqbikha

201
00:27:14,420 --> 00:27:15,420
feeling.

202
00:27:15,420 --> 00:27:19,180
Sukha feeling, and dukha feeling are connected with body.

203
00:27:19,180 --> 00:27:29,820
It is experienced in the mind, but it depends on the, it depends on the physical thing

204
00:27:29,820 --> 00:27:31,460
of body.

205
00:27:31,460 --> 00:27:33,540
So they are all sukha and dokha.

206
00:27:33,540 --> 00:27:41,660
And samanhasa and dongmanhasa are mental, mental feeling, feeling of pleasure in the mind

207
00:27:41,660 --> 00:27:45,860
and feeling of anguish in the mind.

208
00:27:45,860 --> 00:27:52,340
Now, when you hit your, your finger with something, there is a faint ear and then you

209
00:27:52,340 --> 00:27:58,140
have the experiences of that faint or feeling of that faint in your mind.

210
00:27:58,140 --> 00:28:02,340
And that is dokha feeling.

211
00:28:02,340 --> 00:28:08,940
And when you, when you think of something and you are, you are sad, then there is dongmanhasa

212
00:28:08,940 --> 00:28:12,620
feeling or when you are angry, there is dongmanhasa feeling.

213
00:28:12,620 --> 00:28:14,620
So they are different like that.

214
00:28:14,620 --> 00:28:19,700
They are abandoning, with the abandoning of sukha and dokha and with the previous disappearance

215
00:28:19,700 --> 00:28:21,900
of samanhasa and dongmanhasa.

216
00:28:21,900 --> 00:28:30,740
Now, when we read this, it would lead us to, to, to mean that the sukha, dokha, samanhasa and

217
00:28:30,740 --> 00:28:42,620
dongmanhasa are abandoned at the moment of, or just before close to the moment of

218
00:28:42,620 --> 00:29:08,260
sukha, sukha, dokha, samanhasa and dongmanhasa occur during the preliminary stage of the

219
00:29:08,260 --> 00:29:13,300
fourth jhanas.

220
00:29:13,300 --> 00:29:24,020
Now, during the preliminary, preliminary stage of the fourth jhanas, samanhasa is abandoned.

221
00:29:24,020 --> 00:29:31,380
And during the preliminary stage of first jhanas, dokha is abandoned.

222
00:29:31,380 --> 00:29:41,860
And of second jhanas, dongmanhasa is abandoned and of third jhanas, sukha is abandoned.

223
00:29:41,860 --> 00:29:52,660
So actually, they are abandoned during the preliminary stage of these four jhanas.

224
00:29:52,660 --> 00:30:06,700
Now, that is why they said, with the previous disappearance of samanhasa and dongmanhasa.

225
00:30:06,700 --> 00:30:17,300
So sukha, dokha, samanhasa and dongmanhasa are abandoned or eliminated before the moment

226
00:30:17,300 --> 00:30:24,300
of fourth jhanas.

227
00:30:24,300 --> 00:30:32,020
And during these stages, that is, during the preliminary stages of first jhanas, second

228
00:30:32,020 --> 00:30:42,740
jhanas, third jhanas and also fourth jhanas, these feelings, sukha, dongmanhasa are abandoned.

229
00:30:42,740 --> 00:30:51,540
Now, there is one passage in the sukhasa where I just said that they are abandoned

230
00:30:51,540 --> 00:30:54,660
during the jhanas stage.

231
00:30:54,660 --> 00:31:16,660
So the commentator pointed to that passage and then tries to explain it.

232
00:31:16,660 --> 00:31:25,980
Now, it is true that they are abandoned in the preliminary stages.

233
00:31:25,980 --> 00:31:44,700
But they are abandoned and they are not intensive, let me see what is used here, or reinforced

234
00:31:44,700 --> 00:31:45,700
physician.

235
00:31:45,700 --> 00:31:53,460
So their physician in the preliminary stages is not strong or not reinforced.

236
00:31:53,460 --> 00:32:00,780
But at the moment of jhanas, there is reinforced physician or reinforced abandonment of

237
00:32:00,780 --> 00:32:08,900
these things, these feelings.

238
00:32:08,900 --> 00:32:20,260
So in the passage, pointed out by the commentator in the sukha passage, only the reinforced

239
00:32:20,260 --> 00:32:26,420
physician is meant and not just the physician.

240
00:32:26,420 --> 00:32:32,700
So that is why it is said that during the stage of jhanas, these are abandoned.

241
00:32:32,700 --> 00:32:43,860
So in fact, these two statements are not against each other.

242
00:32:43,860 --> 00:32:55,020
Now, paragraph 187, for accordingly, during the first jhanas access, which has multiple

243
00:32:55,020 --> 00:33:04,980
adversity, I don't like the word multiple, actually it is different adversity, not multiple.

244
00:33:04,980 --> 00:33:13,660
Because in a thought process, there is only one adversity, either five cents to

245
00:33:13,660 --> 00:33:26,820
your advertising or mind or advertising, there can be only one adversity.

246
00:33:26,820 --> 00:33:34,420
So here, jhanas access, which has different adversity, means the preliminary stage before

247
00:33:34,420 --> 00:33:37,900
reaching the jhanas thought process.

248
00:33:37,900 --> 00:33:44,580
Now, when it wasn't tries to get jhanas, he practiced his meditation and he gained

249
00:33:44,580 --> 00:33:50,700
for the preliminary concentration and then access concentration.

250
00:33:50,700 --> 00:33:59,140
So during the stage of access concentration, there are moments of this preliminary work.

251
00:33:59,140 --> 00:34:10,660
And these moments consist of the series of thought processes and each thought process,

252
00:34:10,660 --> 00:34:14,100
there is only one adversity.

253
00:34:14,100 --> 00:34:25,060
So first jhanas access, which has different adversity, means while it wasn't is trying

254
00:34:25,060 --> 00:34:33,700
to attain first jhanas, but not yet reach the first jhanas thought process, then those

255
00:34:33,700 --> 00:34:42,220
stages are called and the jhanas access with different adversity.

256
00:34:42,220 --> 00:34:49,100
If it is with the same adversity, then it will mean the jhanas thought process itself.

257
00:34:49,100 --> 00:34:56,820
Here it is not jhanas thought process is not meant, but those occurring before the jhanas

258
00:34:56,820 --> 00:34:58,060
thought process.

259
00:34:58,060 --> 00:35:05,980
And those thought processes each of those thought processes has one adversity, but they

260
00:35:05,980 --> 00:35:08,620
are different in different thought processes.

261
00:35:08,620 --> 00:35:16,820
Therefore, they can be many thought processes of access concentration before reaching

262
00:35:16,820 --> 00:35:22,340
the jhanas thought process.

263
00:35:22,340 --> 00:35:32,780
So the pari wad nana can mean many or different, so here it means different.

264
00:35:32,780 --> 00:35:40,020
So which is different avoiding, there could be re-arising of the bodily pain cycle, the bodily

265
00:35:40,020 --> 00:35:45,980
pain cycle, the simply means pain, bodily pain due to contact with the cat flash and

266
00:35:45,980 --> 00:35:53,820
so on.

267
00:35:53,820 --> 00:36:23,500
So on page paragraph 182 which is called multiple and replace with different, which has different

268
00:36:23,500 --> 00:36:30,380
abandon during the preliminary stages, why are they mentioned here?

269
00:36:30,380 --> 00:36:39,140
Here, all four of them are mentioned in this, such as describing the full jhanas and the

270
00:36:39,140 --> 00:36:45,820
common leader explains that it is done so that they can be easily understood or easily

271
00:36:45,820 --> 00:36:50,420
grasped for a graph 190.

272
00:36:50,420 --> 00:36:58,220
It is done so that they can be easily grasped, easily grasped, grasped means easily understood.

273
00:36:58,220 --> 00:37:03,180
For the neither painful nor pleasant feeling described here, by the words which has neither

274
00:37:03,180 --> 00:37:10,380
been nor pleasure is subtle, hard to recognize and not readily grasped, not readily

275
00:37:10,380 --> 00:37:11,380
understood.

276
00:37:11,380 --> 00:37:22,660
Now, among the feelings, the neutral feeling is the most difficult to see, most difficult to understand.

277
00:37:22,660 --> 00:37:28,300
Pain is obvious and also good feeling through, through guys obvious.

278
00:37:28,300 --> 00:37:36,980
But the neutral feeling is difficult to see, although one experiences neutral feeling, one

279
00:37:36,980 --> 00:37:43,860
is not aware that that is neutral feeling, it is so, so difficult to understand so subtle.

280
00:37:43,860 --> 00:37:57,500
So here, and the Buddha wants us to understand that neutral feeling, maybe with, in comparison

281
00:37:57,500 --> 00:38:00,380
with other feelings.

282
00:38:00,380 --> 00:38:06,780
So it gives all five feelings here, and then this is the feeling which is the most difficult

283
00:38:06,780 --> 00:38:09,980
to understand or something like that.

284
00:38:09,980 --> 00:38:16,620
So just as one, it cattle hard wants to catch a refractory ox that cannot be caught, that

285
00:38:16,620 --> 00:38:18,420
all by approaching it.

286
00:38:18,420 --> 00:38:26,460
He collects all the cattle in one hand and lets them out one by one, and then, I would

287
00:38:26,460 --> 00:38:39,340
say, and then saying that it gets it, and so, I would strike on and so towards.

288
00:38:39,340 --> 00:38:50,980
So saying that is it gets it, gets it caught, as well, not it gets caught, gets it caught

289
00:38:50,980 --> 00:38:52,380
as well.

290
00:38:52,380 --> 00:39:02,220
That means, let other people catch that cattle, this is the one I want to catch, and

291
00:39:02,220 --> 00:39:06,540
so catch it, so the other people catch it.

292
00:39:06,540 --> 00:39:13,020
So it should be after gets, and so should go.

293
00:39:13,020 --> 00:39:21,100
So the sandals will run like this, and lets them out one by one, and then saying that

294
00:39:21,100 --> 00:39:26,500
it gets it, gets it caught, as well.

295
00:39:26,500 --> 00:39:30,620
So do the blessed one as collected, all these five kinds of feelings together, so that

296
00:39:30,620 --> 00:39:33,460
they can be crushed readily.

297
00:39:33,460 --> 00:39:40,220
For one day, you should collect it together, and this way, then what is not bodily pleasure,

298
00:39:40,220 --> 00:39:47,420
bodily pain, mental joy, or mental grief can still be crushed in this way.

299
00:39:47,420 --> 00:39:57,500
So what is, what is not suka, doka, stormas, and domanas, must be you pick up.

300
00:39:57,500 --> 00:40:06,940
So the fourth journal is accompanied by neither pain nor pleasure or neutral feeling,

301
00:40:06,940 --> 00:40:12,340
and it has the purity of mindfulness caused by equanimity, and that is in a specific

302
00:40:12,340 --> 00:40:20,060
neutrality, actually, not only mindfulness, but all mental factors, all mental things

303
00:40:20,060 --> 00:40:27,140
are purified in this, in this jana, and so it is stated, the teaching is given under

304
00:40:27,140 --> 00:40:36,260
the heading of mindfulness, that means, only mindfulness is mentioned, but we must understand

305
00:40:36,260 --> 00:40:43,620
not mindfulness only, but all others, headed by mindfulness.

306
00:40:43,620 --> 00:40:54,740
There is an expression about that, you understand what I mean, sometimes we do not say,

307
00:40:54,740 --> 00:41:03,700
we do not say, all thought we want to say, but we let people know by saying something

308
00:41:03,700 --> 00:41:11,860
different from what we really want to say, say, I want you to understand that certainty,

309
00:41:11,860 --> 00:41:20,740
I mean, mindfulness and others are purified by equanimity, but what I really say is

310
00:41:20,740 --> 00:41:24,700
mindfulness is purified.

311
00:41:24,700 --> 00:41:35,180
So I name mindfulness only, but I mean, mindfulness and others, what you say is in English,

312
00:41:35,180 --> 00:41:43,460
is a way of speaking, something that I figure of speaks.

313
00:41:43,460 --> 00:41:49,900
So I do not say so, but I just say mindfulness, but you must understand that I mean,

314
00:41:49,900 --> 00:41:54,420
mindfulness and others.

315
00:41:54,420 --> 00:42:03,140
Sometimes we say, say, the president comes, but he does not come alone, he comes with

316
00:42:03,140 --> 00:42:08,340
others, but we do not mention others, something like that, surely.

317
00:42:08,340 --> 00:42:12,620
Sometimes we say, surely, and we say, we just say a little bit, but sometimes we

318
00:42:12,620 --> 00:42:24,300
just understand the rest as well, and then we should understand the purity of mindfulness

319
00:42:24,300 --> 00:42:29,500
and that not mean the purity of mindfulness only, but purity of mindfulness and other

320
00:42:29,500 --> 00:42:55,580
transcendence, and then we say, I mean, I mean, I mean, I mean, I mean, I mean, I mean,

321
00:42:55,580 --> 00:43:03,500
meaning of the, it means this, under the heading of mindfulness, that means taking mindfulness

322
00:43:03,500 --> 00:43:08,540
at the head of something, you just say mindfulness, but you understand not only mindfulness,

323
00:43:08,540 --> 00:43:12,100
but others as well.

324
00:43:12,100 --> 00:43:21,740
So this is the food journal, and how many, how many factors are there in food journal?

325
00:43:21,740 --> 00:43:37,420
Two, two, because instead of suka, there is uki ka, so uki ka and ikagara, unification

326
00:43:37,420 --> 00:43:38,420
of man, two.

327
00:43:38,420 --> 00:43:46,780
So the third has two, and the fourth has also two factors.

328
00:43:46,780 --> 00:43:54,540
This is four food methods, there are four journals according to this, but if you want

329
00:43:54,540 --> 00:44:06,060
to experience five journals, then you go about eliminating uki ka and which are separated.

330
00:44:06,060 --> 00:44:11,460
First, you try to eliminate uki ka, and then you get a second journal, and then you try

331
00:44:11,460 --> 00:44:18,980
to eliminate uki ka, and then you get a third journal, so in, in, in that way, there are

332
00:44:18,980 --> 00:44:28,660
five journals, and not four, but actually they are the same, depending on whether a person

333
00:44:28,660 --> 00:44:35,580
eliminates uki ka, uki ka, uki ka, and which are at the same time, or separately one at each

334
00:44:35,580 --> 00:44:37,620
time.

335
00:44:37,620 --> 00:44:47,180
So in the, let us see, there are four journals, on, on the left hand side, and five

336
00:44:47,180 --> 00:44:49,420
journals on the right hand side.

337
00:44:49,420 --> 00:44:54,260
So if the first, in full-full method is the same as first in five-full method, or let

338
00:44:54,260 --> 00:45:00,300
us see, the first in five-full method is the same as first in full-full method, and the

339
00:45:00,300 --> 00:45:07,920
and third in 5-fold method is the same as 2nd in 4-fold method and the 4th in

340
00:45:07,920 --> 00:45:14,900
5-fold method is the same as 3rd in 4-fold method and the 5th in 5-fold method is

341
00:45:14,900 --> 00:45:25,160
the same as 4-fold method but you'll find a mention of 4 genres in the

342
00:45:25,160 --> 00:45:31,380
shoulders. You very rarely find mention of 5 genres in the

343
00:45:31,380 --> 00:45:34,540
shoulders, only 4 genres are mentioned and many many

344
00:45:34,540 --> 00:45:44,980
shoulders but in Abidama all the 4-fold method as well as the 5-fold method is

345
00:45:44,980 --> 00:46:05,260
mentioned. So that is the end of chapter 4 and it'll be great for next week so the

346
00:46:05,260 --> 00:46:18,140
week after next we will continue and let's see how many pages

347
00:46:18,140 --> 00:46:32,280
have to start up in two weeks. So please week two chapters chapter 5 and

348
00:46:32,280 --> 00:46:48,720
have a look at what two factors are there in with the 4th joint S and M of D and

