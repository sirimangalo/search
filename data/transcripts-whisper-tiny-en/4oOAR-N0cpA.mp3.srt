1
00:00:00,000 --> 00:00:08,320
tell us a bit about how he got there what took him to Buddhism and finally to be brave enough

2
00:00:08,320 --> 00:00:17,440
to one to ordain and Ian related to it asks Nagazena did you ever had doubt about going forth

3
00:00:17,440 --> 00:00:31,360
maybe thinking you wouldn't last long how I got here is easy enough kind of took the plane

4
00:00:32,720 --> 00:00:45,600
tax a year but I think uh I don't know I mean it was a long time coming pretty much I think

5
00:00:45,600 --> 00:00:56,080
I've always had a bit of inclination towards it I guess since I was young I got into

6
00:00:56,080 --> 00:01:06,640
uh spiritual things and religions and looking at different things and mainly Hinduism

7
00:01:06,640 --> 00:01:19,280
and sufism both of which have uh ascetics and I was always like oh that would be uh the best

8
00:01:20,080 --> 00:01:28,320
the best thing to do just because to me I always saw the uh well when you stay in the world

9
00:01:28,320 --> 00:01:33,920
it's just you just do the same thing to everyone does you go to school so you get a job you get

10
00:01:33,920 --> 00:01:39,600
the jobs that way you could live so you get married so you get this get that and then you just get

11
00:01:39,600 --> 00:01:46,560
old and then you die and none of it really matters you can't you can't do anything with it if you're

12
00:01:46,560 --> 00:01:53,760
gonna die you just hold in to have fun with it for a little bit of time so then it just becomes

13
00:01:53,760 --> 00:02:01,920
meaningless there's nothing really to it so it was always something that I was

14
00:02:01,920 --> 00:02:13,120
wanting to do I guess and then I got here the meditation courses and yeah there's doubt a lot

15
00:02:13,120 --> 00:02:21,280
of times there is during the meditation courses there's everything it's like a life in a

16
00:02:21,280 --> 00:02:33,680
shot glass you get uh you get big ups big dance and everywhere in between all this so yeah there's

17
00:02:34,640 --> 00:02:43,600
there's moments of incredible inspiration whereas just this is this is the only thing and then

18
00:02:43,600 --> 00:02:49,680
two hours later I'd just be thinking how I how can I get out of here but what can I do to leave

19
00:02:49,680 --> 00:03:04,320
all this so yeah but in the end there's just a matter of really having to look at in the face

20
00:03:04,320 --> 00:03:11,600
I guess and just feel like well there's all these various things happening in my mind I want

21
00:03:11,600 --> 00:03:19,040
to do this I want to do that but you know that I came all the way out here to Sri Lanka for

22
00:03:19,040 --> 00:03:26,480
this purpose because of the way things were in me following these wants and all this and they make

23
00:03:26,480 --> 00:03:32,880
they make a good argument when you're in a tough situation but when you just look at it and

24
00:03:34,240 --> 00:03:41,600
it's like uh it's like this thinking question again it's just when you look at it just thoughts

25
00:03:41,600 --> 00:03:50,640
is just wanting to kind of it loses its punch it's just you give it its importance if you find

26
00:03:50,640 --> 00:03:57,440
this or that important it's because you're you're making it important there's nothing important

27
00:03:57,440 --> 00:04:04,320
about wanting this or wanting that all your wants and cravings there's there's nothing of value

28
00:04:04,320 --> 00:04:09,840
in them it's just that you convince yourself that there is because that's the way the mind works

29
00:04:09,840 --> 00:04:15,760
so you just it's really a matter of letting go I think you just kind of have to

30
00:04:18,160 --> 00:04:24,720
just kind of have to do what you know is right and then it works out

31
00:04:26,800 --> 00:04:31,040
you didn't have other Buddhist teachers before you took the courses there

32
00:04:31,040 --> 00:04:43,520
um no but um yeah I don't know

33
00:04:46,480 --> 00:04:54,400
just have to uh do what you know is right without getting caught up in all the stories that

34
00:04:54,400 --> 00:05:01,040
you'll come up with so that's all that it is is stories is that you just create everything this means

35
00:05:01,040 --> 00:05:06,480
this and it has this importance and it's related to that in this way and it's all just stories

36
00:05:06,480 --> 00:05:12,800
that you make up it really doesn't matter you just have to see that and then it becomes easy

37
00:05:12,800 --> 00:05:22,160
then it's not so much a big thing anymore then you just have to uh memorize

38
00:05:22,160 --> 00:05:24,640
memorize the procedure

39
00:05:29,440 --> 00:05:34,320
um sorry to take my career from you but just to continue on with that

40
00:05:36,960 --> 00:05:41,200
he's not telling you the actual story about it happened when he finished the

41
00:05:42,960 --> 00:05:51,040
the advanced course and we were all sitting we were all sitting down around down here trying to

42
00:05:51,040 --> 00:05:59,200
decide what to do should we should we ordain him tomorrow or should we wait a week

43
00:06:00,000 --> 00:06:05,760
and it was debatable because if we set it for tomorrow then or sun or whatever it was

44
00:06:06,640 --> 00:06:13,040
then uh what happens if he decides he's not going to ordain then the people who were expecting

45
00:06:13,040 --> 00:06:17,040
to come they won't have time to change or so and so and we'd already told them that it was

46
00:06:17,040 --> 00:06:21,040
going to be a week later because we wanted to make sure that he wants to ordain and then we

47
00:06:21,040 --> 00:06:25,600
realized wait a minute if we give him a week he might he might really change his mind and he'll

48
00:06:25,600 --> 00:06:30,880
just keep doubting him more and more and more and uh then he just won't ordain and we had

49
00:06:31,600 --> 00:06:37,760
yeah better and and pollinating gave a good example is that while in her case when she was interested

50
00:06:37,760 --> 00:06:42,480
and then didn't ordain and then years later she came back to it and it could be years before he

51
00:06:42,480 --> 00:06:48,400
actually you know has this chance again he may never have this chance again hmm if we give him that

52
00:06:48,400 --> 00:06:55,760
week uh two dangers so we were and then Bante um Anuma was saying come on give the extra week will

53
00:06:55,760 --> 00:07:03,680
be good for him to get uh used to it or be sure about it as a maybe first-year Lincoln people but

54
00:07:03,680 --> 00:07:10,800
for Westerners never know so we decided that this is what we're going to do if he or

55
00:07:10,800 --> 00:07:17,840
dames he's going to ordain um what would enjoy the Monday or was a Monday no he's going to ordain

56
00:07:17,840 --> 00:07:24,000
Monday if he doesn't ordain Monday he's not going to ordain what do you like that I think that's

57
00:07:24,000 --> 00:07:31,600
when we decided on yeah and that was not the next day but the day after yeah so then the next

58
00:07:31,600 --> 00:07:38,640
morning I went to see him we had this beautiful sunny morning chat Sunday Sunday sunny Sunday morning

59
00:07:38,640 --> 00:07:46,960
chat and uh the thing I don't know is still and so we was back and forth and then he was saying

60
00:07:47,680 --> 00:07:51,200
I said well that's not a very good way to start your that doesn't sound like the start of a

61
00:07:51,920 --> 00:07:58,880
a set of a spiritual life you have to be more sure than that and then we talked about it some more

62
00:07:58,880 --> 00:08:05,520
and more and they said well and it was interesting how it came out because it made me see something

63
00:08:05,520 --> 00:08:09,920
that sometimes and it was what I said to him in the end is that sometimes you just have to take

64
00:08:09,920 --> 00:08:16,160
a take the leap because he wasn't sure if he would take the leap and I think that's good advice

65
00:08:16,160 --> 00:08:23,840
to anyone who's thinking about this course don't don't sit there and waffle about it just if

66
00:08:23,840 --> 00:08:29,600
if you're going to do it do it and when the time comes take the leap do it and

67
00:08:29,600 --> 00:08:39,120
um you get get that over with too often things that are worth doing don't get done because of

68
00:08:39,120 --> 00:08:44,800
hesitation this is that famous saying he who hesitates his lost forever and you lose the

69
00:08:44,800 --> 00:08:49,360
opportunity you only get an opportunity once if an opportunity comes later it's a different

70
00:08:49,360 --> 00:08:54,960
opportunity that opportunity it's always a one-time thing when you have the chance to do

71
00:08:54,960 --> 00:09:00,560
something that you know is right don't don't hesitate with it you know take the leap

72
00:09:00,560 --> 00:09:04,800
and then the other thing that we are talking about is that you won't regret it it's not like

73
00:09:05,760 --> 00:09:12,160
in this case you're doing something that you could possibly later on say oh I wasted my life

74
00:09:12,160 --> 00:09:19,280
sitting there in the forest when I could have been could have been doing something much more

75
00:09:19,280 --> 00:09:28,080
you know because there's nothing else and this is clear so you know overcoming and suppressing

76
00:09:28,080 --> 00:09:36,640
those doubts and just going for it and I guess the corollary question that we have to ask is

77
00:09:36,640 --> 00:09:39,120
how do you feel now did you did was it a mistake?

78
00:09:39,120 --> 00:09:52,960
Completely I'm happy with it I mean yeah the the talk was definitely helpful because then

79
00:09:52,960 --> 00:09:58,960
it's like you're seeing these problems that you're having them it's just other person

80
00:09:58,960 --> 00:10:09,360
like huh yeah well this or that and kind of making you just look at it look at it better

81
00:10:11,120 --> 00:10:24,720
yeah I'm definitely happy with the decision it's just yeah it's the hesitation such

82
00:10:24,720 --> 00:10:30,800
because then you hesitate about it oh I don't know and then more doubt comes then you start weaving

83
00:10:30,800 --> 00:10:34,560
more and more things you just have to do it and then you're you're happy with it

84
00:10:40,960 --> 00:10:49,760
yeah then it's just then it becomes easy so yeah I mean it's good it's better than

85
00:10:49,760 --> 00:10:57,600
anything else I could see myself doing it reminds me of a

86
00:11:00,080 --> 00:11:06,480
when you had the question on education said this is this is the best school

87
00:11:07,600 --> 00:11:14,160
that's what it makes me think of because I mean it really it really is like the best thing

88
00:11:14,160 --> 00:11:23,680
that you can be doing you just have time to to meditate to study yes wear some robes

89
00:11:24,960 --> 00:11:27,280
go walk around the village to get some food

90
00:11:28,800 --> 00:11:35,600
and you just have more time to do something that's actually useful for yourself instead of useful

91
00:11:35,600 --> 00:11:48,720
for continuing on all the various doings of the world

