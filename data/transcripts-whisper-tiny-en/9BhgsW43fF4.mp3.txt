Hello and welcome back to our study of the Dhamapada.
So today we continue on with verses 256 and 257, which read as follows.
The Dhamapada is one of the most important things in this study of Dhamapada.
Which means one is not a judge of the Dhamapada.
Simply because one judge is forcefully, one is not a judge, a Dhamapada, a righteous judge,
just because one judge is forcefully.
Indeed one who is able to judge both cases and not cases.
So if one judge is cases forcefully, one is not a Dhamapada.
But if one judges both what are not, what are cases and what are not cases wisely,
saying that without force, Dhammena, righteously, Samena,
evenly, naivete, but a one judge is others.
A judge without force, righteously evenly, the cases of others.
And thus one guards the Dhamapada must a good domain to be a wise one.
Gards the Dhamapada, T.
Thus one is a Dhamapada judge.
So this verse comes from a very simple story.
So the one with quite a good lesson, I think.
The monks were aware of judges in the society in which they lived.
So when they would go on arms round, they would pass by the court.
I don't know maybe it was an open-air court in ancient times.
And so it was a place where people would come with their grievances
to have them sorted out by the appointed judges, judges who would have been appointed by the king perhaps.
Judges who had great power.
And of course as normal, this institution would have been highly respected
as a matter of course by the populace.
It's given a reputation by teachers, by culture as being an institution of great importance,
of great esteem.
And so naturally the monks assumed that there were great things happening there.
But one day they stopped and observed the proceedings and realized that in fact the judges were not judging impartially.
In fact they were taking bribes from rich land holders in order to sway their decisions.
And often would decide in ways that ruined the less fortunate or the less affluent of plaintiffs or defendants,
causing ruin to a great number of people, unwarranted,
stealing basically stealing their property and giving it to others based on bribes and faulty decisions.
And they were shocked and very much disillusioned by the court in their area.
And so they were discussing this and went to see the Buddha I think or the Buddha found out
and as a result the Buddha remarked in this way.
So the story, the topic today is about judging, which is a good Buddhist topic to talk about.
First of all we can make a brief comment on society and remark that it seems to of course not gotten any better throughout the world.
Corruption of course is rife in governments, in legal institutions.
We hear stories of people using religious views to bias their decisions.
But of course also greed, anger and delusion.
But more importantly of course is how this relates to our own meditation practice.
Judging is a good way to describe a large portion of our activity as human beings.
We're of course constantly judging. Judgment describes much of our interaction with the world around us.
And so the Buddha uses this word forcefully to describe it which I confess I have a hard time translating.
I'm pretty confident in the translation but if you've ever read this verse in English you've probably noticed
they use the word arbitrary which I don't think fits and I don't really understand how it is a translation of this word that I don't really understand that I confess.
But the idea of being forceful is an interesting one simply because
it often is something that lends a sort of credence.
If someone is expressing an opinion forcefully it often is sufficient to convince your average person of the veracity of it.
If someone says I believe this is right or this is wrong and they're great conviction it can often sway people.
Often someone who is a great orator it often doesn't really matter what their stance is.
People who train in debate, politics, politicians can be quite good at swaying people based on emotional appeal.
But it goes even deeper than that and I think you could just use this word sahasa and it is used to describe any sort of bias, any sort of biased judgment where you force something.
And so the idea that I get from this word is like a forcing square pegs to fit in round holes.
You can pretend that it's going to fit or you can be convinced that it's going and you can use a lot of force to try to get in but it's not going to lead to a smooth or pleasant result.
Or like trying to force two puzzle pieces that don't fit together.
You have a puzzle and trying to fit a piece in where it doesn't belong.
These sorts of analogies I think aptly describe the situation where you try to pass off improper judgment as proper judgment.
And this example fits with a judge in the world who presents an opinion or a judgment that goes against reality.
They force it through.
But we do this as a way of describing what we do quite often.
We do this out of greed, we do this out of anger, we do this out of delusion.
It's an important point because for these judges and for anyone who makes these sorts of improper judgment, even in their own personal life, on a moment-to-moment basis, how we judge experience.
It has often a sense of rightness about it.
For a judge who is taking bribes, it's pretty clear they know they're doing something that is dangerous.
But to say they know that it's wrong is I think an oversimplification because yes, they know it is seen as wrong by others.
But a person who takes bribes like that or any sort of addict, someone who steals and this is basically what these judges were doing.
Someone who is addicted to anything and that causes them to act and speak in manipulative ways or even just chase after objects of desire.
It doesn't have a sense that it's wrong.
People who are addicted to smoking, addicted to drugs, addicted to pornography.
They might hate themselves for it, beat themselves up for it, but deep down they don't think it's wrong.
They are forcing through this, they are glossing over.
It's like a grinding of the gears that they miss because of the power of the desire.
They only see the good of it.
This is why it's quite difficult for people to give up sensual pleasure and give up the things that they are addicted to, the things they want, the things they crave.
Because their lack of clarity, the imprecision of their judgment, the wrongness of their judgment, or they judge something to be satisfying a source of pleasure.
It provides them with a conviction, a sense of the rightness of it.
And this is, of course, why a very important point, why it's not really possible for someone who is mindful to get caught up in this.
When you're mindful, your addictions seem to melt away at the moment when you're mindful.
When you try to mindfully smoke cigarettes, you find it very hard to be attached to it.
The desire seems to just melt away because there's clarity, because there's precision.
And you can describe it as you're no longer just forcing through based on your belief, based on your imperfect perception, your perception of it as right.
The greatness of mindfulness is that it gives you this clarity and the ability to perceive.
Atang Anatang, as the Buddha said, what is a case, those are hard words.
I had a hard time translating that.
Atang, and Atang can mean case like you judge a case, like a matter, literally means something to matter, a subject, a topic or something.
But it also means benefit. So Atang is that which has meaning, that which is meaningful.
An Atang, that which is not meaningful, but it's extrapolated here to mean something like what is right and wrong.
So if someone presents a case before a judge, the judge can see that it has merit.
This case has merit. So that would be Atang. It has Atang. It has meaning. It's meaningful.
If a case doesn't have merit, then they have to judge it against the person presenting the case. It's An Atang.
So this kind of language, it's just the good or bad, right or wrong kind of language.
So mindfulness provides you with that, the ability to see through, to see that taking bribes, there's no universe in which that is right.
Again, it actually seems right to these judges who know that they're doing something quote unquote wrong, but it seems right why?
Because money is right. Me getting money is right. That is the goal.
This is why people lie and cheat and steal the imprecision. In their mind, they are grinding the gears forcing through.
Their inability to see clearly is just providing them with a conviction that is able to just ignore all the stress and suffering and wretchedness that comes from addiction.
An addict is so blind to the consequences that it feels like bliss to engage in their addiction, but any objective observer outside, even not someone practicing mindfulness, looks at them and says, what a wretch.
And they're not wrong in saying that. It's quite objectively clear, this person is wretched, suffering terribly in their bliss.
This is, I think, the meaning of sahasa forcing.
And I think it's a really good word, a good way of describing what happens when you don't have the clarity of mindfulness.
So mindfulness, again, is just the simple act of reminding yourself. It's just one moment.
And in that moment, you adjust your perception, right?
Because this state of mind that is forceful, that is just pushing through your based on the narrative that you provide.
This is good for me, this is my goal, money, or pleasure, or whatever, or conversely anger, right?
Some people can feel righteous in their anger. I read recently that there are Buddhist monks actually defending war and killing and saying that somewhere is justifiable.
Based, I guess, on this attachment to society, right? If someone invades your country, the horror of losing your country, this thing that you cling to can lead you to great anger and the ability to kill in defense.
I think defense can be warranted, but certainly not killing. I think why someone would force through that opinion is they're not able to see clearly enough the distinction between perhaps a defending yourself and murdering someone else.
And so, as a result, I mean, personally, at least because of anger, the aversion to losing what you hold dear. Your country, your society, even Buddhist countries often go to war and are very passionate about protecting Buddhism, which is kind of horrific to think about.
On the other hand, the Buddha didn't spend a lot of time trying to fix society. I think wisely he didn't take sides, didn't pick parties to follow or to support.
He praised people who were praised worthy, but much more than people he praised qualities that were praised worthy to try to convince all sides to better themselves rather than taking sides and creating militaristic situations.
But these states where we are caught up in greed and anger and delusion are made up of moments and they're perpetuated by moments.
And so, it may seem insignificant to sit on your meditation, see it and repeat to yourself pain, pain, anger, liking, or any of these things.
But you have to understand the, again, this nature of how our habits form and how they reform every moment that these are not masses or things that we carry around with us.
Our processes, and our processes that only continue by the perpetuation, the feeding of them, we feed them through our continuation of the habit.
Our anger perpetuates more anger, our greed cultivates the habit of greed, we like our liking, our anger makes us more angry and so on.
And so every moment that we change this provides us with a alternative and it provides us with a clarity. So in that moment, it's like putting on the breaks and provides you with this self reflection that is so much a part of meditation that allows you to see how am I doing?
How am I doing this? What is the state of my mind as I pass this judgment as I engage in this activity? It allows you to see.
And that's the whole point, mindfulness leads to seeing, seeing clearly.
It's quite, it should be quite apparent you should be able to see from moment to moment that you're less inclined to engage in hasty judgment, irrational judgment, reactivity.
You start and said instead of reacting, going with it, to seeing your reactions, you start to be like you're watching yourself in horror as you get angry again and again as you cling again and again.
And you see it, it wash away because you slowly, slowly stop feeding it. When you replace this forceful quality with a much more or a similar sort of forcefulness as I think it possibly, although I don't know about this word, Saha, you can use it in a way that is not unwholesome.
If someone is clearly seeing the nature of their experience, then there is a forcefulness still to their actions, to their judgments, but the force is the force of knowledge, the force of understanding.
They simply see clearly, they see how smoking, of smokers see is how smoking is disgusting and really unpleasant and not useful or beneficial in any way, seeing that it is a cause for addiction, it is a cause for the stress and the suffering of withdrawal and so on.
And with anger, for example, with taking bribes apart from how it can lead to criminal prosecution and so on, vilification.
Simply the insanity of the cruelty of hurting other people, taking away their property, for your own benefit, that sort of thing. Just seeing all this, affects a great change in a person.
But again, there is a great strength and so the force, you gain a great force, a great inertia or what is the word, a great power in your mind that is based on a stronger conviction.
It is no longer grinding and gears, it is knowing that the circular peg fits in the circular hole and going for it without any doubt. Seeing that, oh wait, this one is square, this goes in the square hole, putting things in the right place, simply because you see more clearly.
That is the general principle of mindfulness and I will be passing that. So I think that is the lesson that we can take from these two verses. And that is the demo of that Padafar today. Thank you for listening.
