1
00:00:00,000 --> 00:00:14,500
So today we continue our study of the Dhamapada on with 1st number 24, which is as follows.

2
00:00:30,000 --> 00:00:51,000
which means through striving and heedfulness, restraint and training.

3
00:00:51,000 --> 00:00:55,740
One a wise man should make an island for themselves,

4
00:00:55,740 --> 00:00:59,240
the deep nang-gari-rata-ni-dha-mi-dha-mi.

5
00:00:59,240 --> 00:01:11,740
Young-o-gona-mi-ki-rati, that the flood will not overcome or will not wash away or will not cover over.

6
00:01:11,740 --> 00:01:15,440
I will not be carried away by the flood.

7
00:01:15,440 --> 00:01:26,440
So this was told in regards to a monk who became an Arahant.

8
00:01:26,440 --> 00:01:37,040
After failing to learn a single stanza.

9
00:01:37,040 --> 00:01:39,840
So how the story goes is he and his brother became monks.

10
00:01:39,840 --> 00:01:45,840
They had been raised by their grandparents.

11
00:01:45,840 --> 00:01:55,840
Their mother had run away with servant and had lost all of her repute with the family.

12
00:01:55,840 --> 00:01:59,840
So in the end she had no money and no where to live.

13
00:01:59,840 --> 00:02:02,840
So she had to send the children back to live with their grandparents.

14
00:02:02,840 --> 00:02:07,840
And so the grandfather was the disciple of the Buddha.

15
00:02:07,840 --> 00:02:11,840
So he would take his grandchildren to listen to the Buddha teach.

16
00:02:11,840 --> 00:02:17,840
And so the elder son Mahapantaka became a monk.

17
00:02:17,840 --> 00:02:22,840
And through his practice, the Buddha's teaching became an Arahant.

18
00:02:22,840 --> 00:02:26,840
And he thought to himself, wouldn't it be great if I could give this to my younger brother.

19
00:02:26,840 --> 00:02:34,840
So he convinced his grandfather to let his brother become a monk.

20
00:02:34,840 --> 00:02:38,840
And he taught his younger brother this stanza.

21
00:02:38,840 --> 00:02:40,840
He tried to teach in the stanza.

22
00:02:40,840 --> 00:02:43,840
That was basically praise of the Buddha.

23
00:02:43,840 --> 00:02:48,840
And for four months, every time he would learn the first line.

24
00:02:48,840 --> 00:02:50,840
Remember the first line.

25
00:02:50,840 --> 00:02:52,840
Then he would go on to memorize the second line.

26
00:02:52,840 --> 00:02:55,840
And as soon as he memorized the second line, he'd forgotten the first line.

27
00:02:55,840 --> 00:03:02,840
So four lines stanza, just like the one that I gave was a two-line stanza.

28
00:03:02,840 --> 00:03:04,840
So like, just twice as much of that.

29
00:03:04,840 --> 00:03:07,840
For four months, he couldn't memorize it.

30
00:03:07,840 --> 00:03:15,840
And story goes that in the past, in the past life, he had been a good monk.

31
00:03:15,840 --> 00:03:18,840
But he had always looked down upon those monks and just

32
00:03:18,840 --> 00:03:22,840
disparaged those monks who couldn't memorize things.

33
00:03:22,840 --> 00:03:26,840
And then laugh about them and so on.

34
00:03:26,840 --> 00:03:28,840
And brag about it.

35
00:03:28,840 --> 00:03:32,840
And so as a result, he became dumb in his past life.

36
00:03:32,840 --> 00:03:37,840
That's a monk.

37
00:03:37,840 --> 00:03:39,840
And so he became quite discouraged.

38
00:03:39,840 --> 00:03:43,840
And his older brother figured that this monk was a duller.

39
00:03:43,840 --> 00:03:44,840
It was useless.

40
00:03:44,840 --> 00:03:47,840
And so he said, he said, look, for four months,

41
00:03:47,840 --> 00:03:48,840
you've gained nothing.

42
00:03:48,840 --> 00:03:52,840
And he said, you're better off not to be a monk.

43
00:03:52,840 --> 00:03:54,840
And so the younger brother was quite dejected.

44
00:03:54,840 --> 00:03:56,840
But he didn't take his brother seriously.

45
00:03:56,840 --> 00:03:59,840
And until he heard that his brother was responsible for,

46
00:03:59,840 --> 00:04:02,440
his brother was responsible for organizing meals.

47
00:04:02,440 --> 00:04:07,040
So when they went to people's houses, the older brother

48
00:04:07,040 --> 00:04:10,040
would organize monks to go.

49
00:04:10,040 --> 00:04:12,440
And so one day, this layperson, I think,

50
00:04:12,440 --> 00:04:16,840
I'm not a Pindika, came to invite them to his house.

51
00:04:16,840 --> 00:04:21,640
And he said to the older brother, please, how many monks do you have?

52
00:04:21,640 --> 00:04:25,920
Bring all the monks together with the Buddha for tomorrow's meal.

53
00:04:25,920 --> 00:04:29,320
And the older brother said, well, my younger brother

54
00:04:29,320 --> 00:04:30,520
is not a real monk.

55
00:04:30,520 --> 00:04:34,360
So I'll accept the invitation for everybody but him.

56
00:04:34,360 --> 00:04:36,840
And the younger brother was sitting there and he heard

57
00:04:36,840 --> 00:04:37,320
there.

58
00:04:37,320 --> 00:04:39,520
And at that point, he became quite dejected.

59
00:04:39,520 --> 00:04:42,560
And said, my brother says, I'm not a real monk.

60
00:04:42,560 --> 00:04:46,680
And so he decided that he better quit, he better disrobe.

61
00:04:46,680 --> 00:04:48,520
But the truth is he was happy being a monk.

62
00:04:48,520 --> 00:04:51,400
He didn't have a problem.

63
00:04:51,400 --> 00:04:55,560
His brother had this idea that somehow you had to memorize.

64
00:04:55,560 --> 00:04:58,120
It may also have been that his older brother was thinking

65
00:04:58,120 --> 00:05:01,360
that this would go to see the Buddha

66
00:05:01,360 --> 00:05:03,640
and the Buddha would give him a meditation object.

67
00:05:03,640 --> 00:05:05,320
Because that's what happened.

68
00:05:05,320 --> 00:05:08,360
The Buddha found out about this and saw him walking around

69
00:05:08,360 --> 00:05:13,920
dejected or about to leave.

70
00:05:13,920 --> 00:05:15,960
And he went up to see him and he said, do much wrong.

71
00:05:15,960 --> 00:05:18,600
And he said, oh, I can't memorize even a single stanza.

72
00:05:18,600 --> 00:05:19,920
And my brother says, I'm useless.

73
00:05:19,920 --> 00:05:24,200
And I should leave the leave and disrobe.

74
00:05:24,200 --> 00:05:26,960
And the Buddha said, that's not what makes a real monk.

75
00:05:26,960 --> 00:05:29,840
He said, you don't become a monk just for memorizing.

76
00:05:29,840 --> 00:05:33,560
He said, here, let me help you.

77
00:05:33,560 --> 00:05:34,760
He took a cloth.

78
00:05:34,760 --> 00:05:38,880
He took this white cloth, clean pure white cloth,

79
00:05:38,880 --> 00:05:40,600
and he gave it over to him.

80
00:05:40,600 --> 00:05:44,240
And he said, go stand in the sun, hold this white cloth,

81
00:05:44,240 --> 00:05:48,600
and say to yourself, and rub it.

82
00:05:48,600 --> 00:05:52,040
Rub it between your hand and say, a red jolt,

83
00:05:52,040 --> 00:05:56,600
Haran and Rajo Haran on which means taking out the dirt,

84
00:05:56,600 --> 00:05:57,600
rubbing out the dirt.

85
00:06:00,960 --> 00:06:03,440
And he sent him off to practice this,

86
00:06:03,440 --> 00:06:05,680
meditations, repeating to himself.

87
00:06:05,680 --> 00:06:09,520
Rajo Haran on taking out the dirt, taking out the dirt.

88
00:06:09,520 --> 00:06:14,160
And then the Buddha went off to accept this invitation.

89
00:06:17,200 --> 00:06:20,760
Now, what happened was he went off into the sun.

90
00:06:20,760 --> 00:06:23,200
And he's standing there, and of course, the sun comes up,

91
00:06:23,200 --> 00:06:25,480
and it's getting hot and sweating.

92
00:06:25,480 --> 00:06:29,120
And as his hand start to sweat, the cloth becomes soiled.

93
00:06:29,120 --> 00:06:33,960
It becomes impure, it loses its white color.

94
00:06:33,960 --> 00:06:38,600
And he looks at this, and he starts to become, well,

95
00:06:38,600 --> 00:06:40,880
turned off, of course, because it's a beautiful white cloth,

96
00:06:40,880 --> 00:06:42,320
and suddenly it starts to get ugly.

97
00:06:42,320 --> 00:06:44,320
And so he's standing there, saying,

98
00:06:44,320 --> 00:06:46,640
Rajo, taking out the dirt, taking out the dirt.

99
00:06:46,640 --> 00:06:50,560
And here's this cloth getting dirty right before his eyes.

100
00:06:50,560 --> 00:06:56,680
And as a result, it starts to open up his mind and his attachment

101
00:06:56,680 --> 00:07:03,480
to the cloth and the idea of purity starts to be loosened.

102
00:07:03,480 --> 00:07:06,600
So his mind starts to calm down as a result,

103
00:07:06,600 --> 00:07:10,360
the cravings in his mind start to go away.

104
00:07:10,360 --> 00:07:11,600
And the Buddha finds out about this.

105
00:07:11,600 --> 00:07:17,240
And the Buddha sends him this telepathic projection.

106
00:07:17,240 --> 00:07:20,520
So he sees the Buddha, and the Buddha says,

107
00:07:20,520 --> 00:07:26,200
chula-pantaka, it is raga, raga, raja, raja, raja.

108
00:07:26,200 --> 00:07:31,240
He says, this is a play on words, raga, raja, raja.

109
00:07:31,240 --> 00:07:33,800
The dirt he's talking about is lust.

110
00:07:33,800 --> 00:07:37,600
Lust is another word for dirt.

111
00:07:37,600 --> 00:07:41,920
And he said, by understanding this, one should take out the dirt,

112
00:07:41,920 --> 00:07:43,680
take out the dirt that is lust.

113
00:07:43,680 --> 00:07:50,000
And then he says, do so, raja, dosa, anger is dirt.

114
00:07:50,000 --> 00:07:51,880
And Moha has dirt.

115
00:07:51,880 --> 00:07:57,320
So these three things, and he helps him to look.

116
00:07:57,320 --> 00:07:59,160
And then to look under the looks inside.

117
00:07:59,160 --> 00:08:02,680
And he becomes an raja by listening to this teaching in the Buddha.

118
00:08:02,680 --> 00:08:07,320
The Buddha said, it's not this dirt on the cloth that we're talking about.

119
00:08:07,320 --> 00:08:11,120
The dirt that we want to take out is our cravings, our attachment.

120
00:08:11,120 --> 00:08:13,280
So attachment to the beauty of the cloth,

121
00:08:13,280 --> 00:08:14,760
attachment to the beauty of the body.

122
00:08:14,760 --> 00:08:19,280
It's also helping to see the impurity in the body.

123
00:08:19,280 --> 00:08:23,680
And so as a result of seeing that, he became an raja.

124
00:08:23,680 --> 00:08:26,720
Now, the Buddha was off at this invitation,

125
00:08:26,720 --> 00:08:30,360
and the Nathapindika came to offer the food.

126
00:08:30,360 --> 00:08:36,160
And he came over and he comes to offer this ceremony of water

127
00:08:36,160 --> 00:08:39,920
that they pour when they give a gift.

128
00:08:39,920 --> 00:08:42,560
But the Buddha covered his bow with his hand

129
00:08:42,560 --> 00:08:49,040
and said, Nathapindika, the sangha is not complete.

130
00:08:49,040 --> 00:08:51,120
Not everyone is here.

131
00:08:51,120 --> 00:08:55,400
And Nathapindika looked at Mahapantika and said,

132
00:08:55,400 --> 00:08:56,880
are all the monks here?

133
00:08:56,880 --> 00:09:01,240
Mahapantika said, all the monks are here.

134
00:09:01,240 --> 00:09:05,440
And the Buddha said, there's still a monk left in the monastery.

135
00:09:05,440 --> 00:09:10,040
And so Nathapindika sent a man to the monastery to find out.

136
00:09:10,040 --> 00:09:14,000
Now, the story goes that Julepantika,

137
00:09:14,000 --> 00:09:16,440
when he became an raja, he also gained magical powers.

138
00:09:16,440 --> 00:09:20,680
So he had several, and also he gained knowledge of the Pitakas.

139
00:09:20,680 --> 00:09:24,080
So he gained quite an array of mental powers.

140
00:09:24,080 --> 00:09:29,680
He lost all of this cloudiness in the mind

141
00:09:29,680 --> 00:09:31,280
to the stupidity left him.

142
00:09:31,280 --> 00:09:34,160
He was no longer a duller than he was able to remember

143
00:09:34,160 --> 00:09:35,680
the whole of the Buddha's teaching.

144
00:09:35,680 --> 00:09:38,000
Everything that he had ever heard the Buddha teaching,

145
00:09:38,000 --> 00:09:40,680
Buddha teach, suddenly it came back to him.

146
00:09:40,680 --> 00:09:45,440
When he became enlightened, because his mind became clear.

147
00:09:45,440 --> 00:09:47,440
And also he had these magical powers.

148
00:09:47,440 --> 00:09:48,880
So he knew it, his brother was saying.

149
00:09:48,880 --> 00:09:50,840
And he said, my brother said, there's no monks here.

150
00:09:50,840 --> 00:09:54,600
And suddenly, he made a resolve that the whole monastery

151
00:09:54,600 --> 00:09:56,040
should be full of monks.

152
00:09:56,040 --> 00:09:59,000
And there were 1,000, suddenly there were 1,000 monks

153
00:09:59,000 --> 00:10:01,320
filling Jaitawana at this monastery.

154
00:10:01,320 --> 00:10:02,240
Some of them are dying.

155
00:10:02,240 --> 00:10:03,520
They're closed, some of them are washing.

156
00:10:03,520 --> 00:10:04,960
They're closed, some of them are sweeping,

157
00:10:04,960 --> 00:10:07,200
some of them are meditating, some of them

158
00:10:07,200 --> 00:10:10,840
are sitting, some of them are doing walking.

159
00:10:10,840 --> 00:10:12,520
And this man came and saw these monks.

160
00:10:12,520 --> 00:10:14,400
And he went back and he said, there's 1,000 monks

161
00:10:14,400 --> 00:10:17,160
in the monastery, and he went out to Pindika.

162
00:10:17,160 --> 00:10:18,640
And he went out to Pindika, told us to the Buddha.

163
00:10:18,640 --> 00:10:22,040
And the Buddha said, go and tell them

164
00:10:22,040 --> 00:10:24,640
that you want julapantika.

165
00:10:24,640 --> 00:10:26,240
And so the man went back to Jaitawana.

166
00:10:26,240 --> 00:10:30,000
And he said, where is julapantika?

167
00:10:30,000 --> 00:10:33,240
And all 1,000 of them said, I'm julapantika.

168
00:10:33,240 --> 00:10:35,840
I'm julapantika.

169
00:10:35,840 --> 00:10:36,920
So he went back again.

170
00:10:36,920 --> 00:10:39,520
And he told her, what to do?

171
00:10:39,520 --> 00:10:41,680
They're all julapantika.

172
00:10:41,680 --> 00:10:44,800
The whole point is to show his brother

173
00:10:44,800 --> 00:10:49,520
that he's not useless, or to make clear

174
00:10:49,520 --> 00:10:51,680
what the truth is.

175
00:10:54,360 --> 00:10:57,920
And the Buddha said, the Buddha says, no, no, go and say it again.

176
00:10:57,920 --> 00:10:58,560
Ask it again.

177
00:10:58,560 --> 00:11:01,160
And the first one who says, I am julapantika.

178
00:11:01,160 --> 00:11:04,520
Grab onto his robe.

179
00:11:04,520 --> 00:11:05,960
And so he went back and he grabbed.

180
00:11:05,960 --> 00:11:08,080
And he said, where's julapantika?

181
00:11:08,080 --> 00:11:09,960
And they said, I am julapantika.

182
00:11:09,960 --> 00:11:11,920
But he saw the one who said it first, and he grabbed him

183
00:11:11,920 --> 00:11:12,840
by the robe.

184
00:11:12,840 --> 00:11:14,200
And suddenly all the rest disappeared.

185
00:11:14,200 --> 00:11:15,240
And he said, please come.

186
00:11:15,240 --> 00:11:16,240
The Buddha is waiting for you.

187
00:11:19,200 --> 00:11:23,680
And so they invited him to go.

188
00:11:23,680 --> 00:11:30,160
And then after the meal, everyone was kind of not sure who

189
00:11:30,160 --> 00:11:34,320
is this guy, a real monk, or is he not a real monk?

190
00:11:34,320 --> 00:11:38,440
And the Buddha said, at the end of the meal,

191
00:11:38,440 --> 00:11:40,760
he said, julapantika, please, you give the,

192
00:11:40,760 --> 00:11:41,960
give the thanksgiving.

193
00:11:41,960 --> 00:11:43,480
So you give the talk after the meal.

194
00:11:43,480 --> 00:11:45,640
Because after they had a meal, they would always give a talk

195
00:11:45,640 --> 00:11:46,160
to the Lord.

196
00:11:49,120 --> 00:11:50,600
And julapantika, and so the older brother,

197
00:11:50,600 --> 00:11:52,880
and everyone was thinking, what's he going to talk about?

198
00:11:52,880 --> 00:11:54,760
He doesn't, you can't even remember a single stanza.

199
00:11:54,760 --> 00:11:56,760
And suddenly, he gives this talk about everything

200
00:11:56,760 --> 00:11:58,480
the Buddha taught, and he goes through the whole

201
00:11:58,480 --> 00:12:01,120
of the three to pick up.

202
00:12:01,120 --> 00:12:03,520
And everyone realizes, wow, something

203
00:12:03,520 --> 00:12:07,440
has changed that he has become the most.

204
00:12:07,440 --> 00:12:10,560
And the story goes on to explain why he became,

205
00:12:10,560 --> 00:12:12,320
like he was, the story goes in the past.

206
00:12:12,320 --> 00:12:13,640
He was a king.

207
00:12:13,640 --> 00:12:17,840
And he had this idea of impurity.

208
00:12:17,840 --> 00:12:23,400
Because he was going on a voyage to the city as the king,

209
00:12:23,400 --> 00:12:27,320
going on a tour to see what was going on in the city.

210
00:12:27,320 --> 00:12:28,160
And it was very hot.

211
00:12:28,160 --> 00:12:29,120
And so he started sweating.

212
00:12:29,120 --> 00:12:31,800
And he wiped his face with his pure cloth,

213
00:12:31,800 --> 00:12:33,520
and he saw it get dirty.

214
00:12:33,520 --> 00:12:35,560
And he realized that this is the way with everything.

215
00:12:35,560 --> 00:12:38,080
Nothing stays pure and perfect.

216
00:12:38,080 --> 00:12:41,360
Everything is subject to change.

217
00:12:41,360 --> 00:12:44,480
And so that's what the Buddha saw when he gave him this cloth.

218
00:12:44,480 --> 00:12:49,440
He saw that in his past he had already used this meditation

219
00:12:49,440 --> 00:12:49,840
something.

220
00:12:49,840 --> 00:12:52,240
So then the monks were talking about this.

221
00:12:52,240 --> 00:12:53,480
How could it be that he couldn't even

222
00:12:53,480 --> 00:12:55,680
memorize this single stanza?

223
00:12:55,680 --> 00:12:59,720
How could it be that he is able to make this refuge for himself

224
00:12:59,720 --> 00:13:03,640
and find the ultimate refuge and become free from suffering

225
00:13:03,640 --> 00:13:06,200
without even being able to memorize a single stanza?

226
00:13:06,200 --> 00:13:10,160
And the Buddha said, it's not by listening.

227
00:13:10,160 --> 00:13:11,240
It's not by memorizing.

228
00:13:11,240 --> 00:13:12,280
It's not by all this.

229
00:13:12,280 --> 00:13:15,280
The way a person is able to make a refuge for themselves

230
00:13:15,280 --> 00:13:19,960
and island for themselves is through four things,

231
00:13:19,960 --> 00:13:25,020
through effort, through heedfulness,

232
00:13:25,020 --> 00:13:28,680
through restraint and through training.

233
00:13:28,680 --> 00:13:31,320
And this is how a wise one makes an island for themselves.

234
00:13:31,320 --> 00:13:33,520
And then he said the verse.

235
00:13:33,520 --> 00:13:54,160
So that's briefly the story.

236
00:13:54,160 --> 00:13:57,960
As to the meaning of it, this is a really important verse

237
00:13:57,960 --> 00:14:02,520
and one that is wonderful for us if you can remember to remember it.

238
00:14:02,520 --> 00:14:05,160
Please keep in mind what are these four things

239
00:14:05,160 --> 00:14:10,680
because even just reminding yourself of this teaching

240
00:14:10,680 --> 00:14:15,600
is of great use for us as in meditation.

241
00:14:15,600 --> 00:14:19,400
The first one, Utanena, with effort.

242
00:14:19,400 --> 00:14:21,440
When we're able to put out the effort in the practice.

243
00:14:21,440 --> 00:14:23,680
But this is necessary.

244
00:14:23,680 --> 00:14:26,240
This is the way that a person makes a refuge for themselves

245
00:14:26,240 --> 00:14:27,600
or makes an island of themselves.

246
00:14:27,600 --> 00:14:34,920
When a person, first of all, what we mean by island here

247
00:14:34,920 --> 00:14:36,840
is we're talking about a person becoming enlightened.

248
00:14:36,840 --> 00:14:38,520
And why we mean island?

249
00:14:38,520 --> 00:14:42,880
And the Buddha uses this, similarly, of the island and the flood.

250
00:14:42,880 --> 00:14:48,840
So all beings, we understand samsara is like a flood.

251
00:14:48,840 --> 00:14:50,480
There is like an ocean.

252
00:14:50,480 --> 00:14:53,200
And the ocean is drowning us.

253
00:14:53,200 --> 00:14:57,040
We're swimming about in this ocean aimlessly.

254
00:14:57,040 --> 00:15:00,040
The whole of the universe, the whole of samsara,

255
00:15:00,040 --> 00:15:02,320
you can't find a purpose, you can't find an aim,

256
00:15:02,320 --> 00:15:05,840
whatever you aim for, whatever you go for, whatever you reach,

257
00:15:05,840 --> 00:15:08,160
whatever you attain, becomes meaningless.

258
00:15:08,160 --> 00:15:11,960
And it's like carried away by the flood, whatever you build up,

259
00:15:11,960 --> 00:15:17,280
whatever great things, cities or palaces or learning

260
00:15:17,280 --> 00:15:18,880
that you gain at all disappears.

261
00:15:18,880 --> 00:15:20,880
Anything you remember eventually you forget,

262
00:15:20,880 --> 00:15:23,640
everything you create eventually is destroyed.

263
00:15:23,640 --> 00:15:24,560
So it's like an ocean.

264
00:15:24,560 --> 00:15:26,800
And there's nothing to hold on to.

265
00:15:26,800 --> 00:15:28,800
There's nothing that is a secure refuge.

266
00:15:28,800 --> 00:15:31,800
You build up a refuge that disappears.

267
00:15:31,800 --> 00:15:35,480
It leaves you without.

268
00:15:35,480 --> 00:15:39,000
So the finding of the real refuge is like building an island

269
00:15:39,000 --> 00:15:43,200
or finding an island that the flood can't overcome.

270
00:15:43,200 --> 00:15:47,160
No matter how much the waves come or tsunami or any kind

271
00:15:47,160 --> 00:15:50,720
of tides of any sort can't overcome this island.

272
00:15:50,720 --> 00:15:52,440
This is what we're all looking for in life.

273
00:15:52,440 --> 00:15:55,400
We're trying to find this stability and the security.

274
00:15:55,400 --> 00:15:58,640
The Buddha said the way to find this is by these four things.

275
00:15:58,640 --> 00:16:04,760
So utana means effort that we have to work hard.

276
00:16:04,760 --> 00:16:07,200
That this is the work that we see that Tulipanta

277
00:16:07,200 --> 00:16:09,840
continued, that it wasn't effort and studying.

278
00:16:09,840 --> 00:16:17,720
It wasn't effort in art, in being a monk or so on.

279
00:16:17,720 --> 00:16:20,600
It was effort in seeing the truth and in cleaning out

280
00:16:20,600 --> 00:16:21,520
the stains inside.

281
00:16:21,520 --> 00:16:24,720
So just this practice of meditation.

282
00:16:24,720 --> 00:16:27,920
And then going through, moment by moment,

283
00:16:27,920 --> 00:16:31,520
going through his mind, seeing the lust, seeing the anger,

284
00:16:31,520 --> 00:16:33,680
seeing the delusion that was in his mind

285
00:16:33,680 --> 00:16:37,400
and weeding it out, rooting it out.

286
00:16:37,400 --> 00:16:39,960
This is the effort that he put out.

287
00:16:39,960 --> 00:16:41,720
This is the effort that we all need.

288
00:16:41,720 --> 00:16:44,720
We have to do the work.

289
00:16:44,720 --> 00:16:46,400
So this is what it means to meditate.

290
00:16:46,400 --> 00:16:49,200
When we talk about meditation, some people they say,

291
00:16:49,200 --> 00:16:52,120
being mindful doesn't require meditation.

292
00:16:52,120 --> 00:16:54,240
A person can just be mindful in daily life.

293
00:16:54,240 --> 00:16:56,400
And that's enough to become enlightened.

294
00:16:56,400 --> 00:17:03,440
And the truth is, it's possible, but it's very, very difficult.

295
00:17:03,440 --> 00:17:05,120
And it actually takes a lot of work

296
00:17:05,120 --> 00:17:07,600
and you have to actually do something

297
00:17:07,600 --> 00:17:09,440
to raise yourself up.

298
00:17:09,440 --> 00:17:11,360
People say that just naturally it comes.

299
00:17:11,360 --> 00:17:12,760
You can naturally become enlightened.

300
00:17:12,760 --> 00:17:14,160
And that's quite misleading.

301
00:17:14,160 --> 00:17:15,440
And I think quite inaccurate.

302
00:17:15,440 --> 00:17:17,080
The person has to work.

303
00:17:17,080 --> 00:17:19,520
This is what we mean by the word practicing meditation.

304
00:17:19,520 --> 00:17:23,160
The word meditation means to apply yourself

305
00:17:23,160 --> 00:17:24,440
to the practice.

306
00:17:24,440 --> 00:17:29,280
The second one, a pamada means the clarity of mind,

307
00:17:29,280 --> 00:17:30,600
the mindfulness.

308
00:17:30,600 --> 00:17:35,080
So not just pushing yourself to practice meditation,

309
00:17:35,080 --> 00:17:37,320
but actually bringing the mind back.

310
00:17:37,320 --> 00:17:41,920
And guarding the mind, applying the mind

311
00:17:41,920 --> 00:17:42,680
to the object.

312
00:17:42,680 --> 00:17:46,800
And when you see something clearly knowing that this is seeing,

313
00:17:46,800 --> 00:17:49,680
when you hear something, knowing this is hearing,

314
00:17:49,680 --> 00:17:52,600
being with reality from moment to moment,

315
00:17:52,600 --> 00:17:56,840
and not letting yourself step away, the mindfulness.

316
00:17:56,840 --> 00:18:01,240
Or what we call, what we call sati, reminding yourself,

317
00:18:01,240 --> 00:18:03,840
this act of reminding yourself of what's going on.

318
00:18:03,840 --> 00:18:05,880
This is what we mean by heatfulness.

319
00:18:05,880 --> 00:18:09,880
Being heatful, being aware of the object.

320
00:18:09,880 --> 00:18:14,120
The third one, sanyamena means keeping the mind

321
00:18:14,120 --> 00:18:15,680
from getting distracted.

322
00:18:15,680 --> 00:18:19,520
So at the same time that we're watching the object,

323
00:18:19,520 --> 00:18:22,200
we have to watch ourselves when we get distracted.

324
00:18:22,200 --> 00:18:29,320
So sanyamena means, or it means keeping the mind

325
00:18:29,320 --> 00:18:30,720
within boundaries.

326
00:18:30,720 --> 00:18:33,400
So not letting the mind wander, not following after things

327
00:18:33,400 --> 00:18:35,720
that you see, or things that you hear,

328
00:18:35,720 --> 00:18:37,520
not getting caught up in random thoughts,

329
00:18:37,520 --> 00:18:38,680
or daydreams or so on.

330
00:18:38,680 --> 00:18:43,480
This is sanyamena, keeping the mind restrained.

331
00:18:43,480 --> 00:18:46,560
And the fourth one that goes quite along with with sanyamena

332
00:18:46,560 --> 00:18:50,280
and means very much the same thing as dameen dame.

333
00:18:50,280 --> 00:18:52,640
But dame is more like training yourself

334
00:18:52,640 --> 00:18:54,920
so that the mind doesn't run away,

335
00:18:54,920 --> 00:18:56,520
so that the mind doesn't get distracted.

336
00:18:56,520 --> 00:18:59,440
Sanyamena is guarding it, so it doesn't get distracted.

337
00:18:59,440 --> 00:19:02,240
Dameen is the aspect of the practice

338
00:19:02,240 --> 00:19:04,600
that changes the mind, straightens the mind,

339
00:19:04,600 --> 00:19:07,600
so that the mind no longer wants to go away,

340
00:19:07,600 --> 00:19:09,880
wants to be distracted, no longer wants to follow

341
00:19:09,880 --> 00:19:14,240
after great daydreams or addictions or desires or so.

342
00:19:16,320 --> 00:19:18,600
Basically, this refers to the practice of meditation

343
00:19:18,600 --> 00:19:20,760
and put out the effort in the practice

344
00:19:20,760 --> 00:19:25,800
and then apply yourself correctly in the practice.

345
00:19:25,800 --> 00:19:28,320
You guard yourself from getting distracted

346
00:19:28,320 --> 00:19:30,560
and train yourself so that in the end

347
00:19:30,560 --> 00:19:33,160
you don't want to become distracted,

348
00:19:33,160 --> 00:19:38,040
so that your mind is focused and resonant

349
00:19:38,040 --> 00:19:40,680
and intent upon the object.

350
00:19:40,680 --> 00:19:42,720
This is how really how julapantika,

351
00:19:42,720 --> 00:19:44,320
even though the text doesn't go into detail,

352
00:19:44,320 --> 00:19:46,240
this is how he became enlightened.

353
00:19:46,240 --> 00:19:49,040
Because when the Buddha started explaining to him

354
00:19:49,040 --> 00:19:50,640
what he was seeing this change

355
00:19:50,640 --> 00:19:55,480
that was going on in the cloth,

356
00:19:55,480 --> 00:19:57,280
and also in his mind,

357
00:19:57,280 --> 00:19:59,000
when the mind started to lose its interest

358
00:19:59,000 --> 00:20:01,960
and its attachment to the beautiful cloth,

359
00:20:01,960 --> 00:20:05,720
was that, oh, it's the greed, it's the anger,

360
00:20:05,720 --> 00:20:09,160
it's the delusion that we're trying to change,

361
00:20:09,160 --> 00:20:12,440
that is changing here until eventually your mind

362
00:20:12,440 --> 00:20:14,720
simply knows the object what it is.

363
00:20:14,720 --> 00:20:17,280
And so, he applied himself to this

364
00:20:17,280 --> 00:20:20,200
with effort, with mindfulness,

365
00:20:20,200 --> 00:20:22,280
clearly where this is greed, this is anger,

366
00:20:22,280 --> 00:20:24,520
this is delusion, this is seeing,

367
00:20:24,520 --> 00:20:26,040
this is hearing, this is smelling,

368
00:20:26,040 --> 00:20:27,360
this is tasting, and so on.

369
00:20:29,680 --> 00:20:34,880
And guarding himself and training himself in this way,

370
00:20:34,880 --> 00:20:37,360
and as a result he became an hour.

371
00:20:37,360 --> 00:20:39,200
So, just another small teaching

372
00:20:39,200 --> 00:20:41,640
and one that's very important for us as meditators,

373
00:20:41,640 --> 00:20:44,320
something that we can all take to heart.

374
00:20:44,320 --> 00:20:46,920
So, that's our teaching on the Damapada for today.

375
00:20:46,920 --> 00:20:49,200
Thank you all for tuning in and listening,

376
00:20:49,200 --> 00:20:50,600
and for joining us today,

377
00:20:50,600 --> 00:21:15,040
so we can go back to our practice.

