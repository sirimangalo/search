1
00:00:00,000 --> 00:00:06,920
Bante, for those of us who live in areas that have never had a Buddhist monastic present,

2
00:00:06,920 --> 00:00:12,840
what is important to know when meeting, greeting, interacting with a bikhu or bikuni?

3
00:00:12,840 --> 00:00:16,960
Also what is meant when people say sad, who is sad, who is sad?

4
00:00:16,960 --> 00:00:19,360
See, easy question.

5
00:00:19,360 --> 00:00:24,480
Okay, how to deal with bikhu's bikuni is don't call them bikhu.

6
00:00:24,480 --> 00:00:27,040
I've had people call me bikhu.

7
00:00:27,040 --> 00:00:32,360
You can call me what you can call them what you like, but it's like saying monk.

8
00:00:32,360 --> 00:00:35,560
Excuse me monk, we don't say that.

9
00:00:35,560 --> 00:00:42,440
Well, it means it's an etiquette, it's not that they'll be necessarily offended, but there's

10
00:00:42,440 --> 00:00:43,920
etiquette for you.

11
00:00:43,920 --> 00:00:50,480
Just as you wouldn't say, hey monk, don't say a bikhu.

12
00:00:50,480 --> 00:00:59,440
You can call them bante or iya, iya, you would be the correct, I think, which is the female.

13
00:00:59,440 --> 00:01:03,360
But they, actually, bante would be more proper, but they don't seem to use that, or they

14
00:01:03,360 --> 00:01:04,360
do actually.

15
00:01:04,360 --> 00:01:05,360
But nowadays they don't.

16
00:01:05,360 --> 00:01:13,360
But in the text, it seems like bante and bante, but they was the female.

17
00:01:13,360 --> 00:01:24,400
But they seem to be bikuni seem to prefer iya, or iya, I would say iya is probably more common.

18
00:01:24,400 --> 00:01:31,840
So find a venerable also work, just like you would call a priest father, right?

19
00:01:31,840 --> 00:01:36,480
Even if you're not particularly religious out of respect and deference, you call a doctor,

20
00:01:36,480 --> 00:01:43,840
you call a priest father, you call a monk, venerable, or something like that.

21
00:01:43,840 --> 00:01:49,760
Even if you don't respect them, it's a courtesy, it's etiquette, so that's something.

22
00:01:49,760 --> 00:01:52,080
Don't try to get too close or friendly to them.

23
00:01:52,080 --> 00:01:57,840
The reason we've ordained is to get away, is to be in solitude, to have the time to

24
00:01:57,840 --> 00:01:58,840
ourselves.

25
00:01:58,840 --> 00:02:06,040
So, trying to be buddy, buddy with a monk and thinking of yourself as buddies or something

26
00:02:06,040 --> 00:02:07,040
like that.

27
00:02:07,040 --> 00:02:11,640
If you're not a monk, it's like an ordinary person and a soldier.

28
00:02:11,640 --> 00:02:18,280
The soldier has to remain aloof and has to have some separate, some seclusion.

29
00:02:18,280 --> 00:02:19,280
There is a difference.

30
00:02:19,280 --> 00:02:22,800
I mean, people don't realize that because they haven't experienced the life, but we

31
00:02:22,800 --> 00:02:30,520
have a very strict life with lots of rules, and so it can be harmful for the monk,

32
00:02:30,520 --> 00:02:37,360
and it can be a source of misunderstanding for the lay person, when the monk is, as a result

33
00:02:37,360 --> 00:02:44,960
of their interaction, they might break rules, and as a result of them refusing to break

34
00:02:44,960 --> 00:02:50,960
rules, the lay person might become upset as a cause for misunderstanding when it seems

35
00:02:50,960 --> 00:02:57,720
like the monk is acting a bit standoffish or whatever, actually it's usually just trying

36
00:02:57,720 --> 00:02:59,040
to keep the rules.

37
00:02:59,040 --> 00:03:03,560
You're concerned that that's very difficult to keep the rules with these people, so close

38
00:03:03,560 --> 00:03:11,320
to me, so buddy, buddy.

39
00:03:11,320 --> 00:03:13,040
So yeah, that's about it.

40
00:03:13,040 --> 00:03:19,440
Men shouldn't touch bikinis, women shouldn't touch bikinis, right?

41
00:03:19,440 --> 00:03:24,800
And you shouldn't really touch them in general, touching is not a good thing, or not touch

42
00:03:24,800 --> 00:03:33,160
you feel the sort of people, monks, give them some space, give them their own space.

43
00:03:33,160 --> 00:03:38,160
Don't be afraid of them, and don't be afraid to ask, you know, if you don't, if you're

44
00:03:38,160 --> 00:03:42,520
not sure about something, ask if this appropriate, is this inappropriate?

45
00:03:42,520 --> 00:03:47,920
Most bikinis like to be asked, like to have people ask questions, it's not really a

46
00:03:47,920 --> 00:03:55,240
greed thing, it's just they're bursting with knowledge and practice and you know, quite

47
00:03:55,240 --> 00:04:00,480
willing to answer people's questions, so ask them lots of questions, it's a good thing.

48
00:04:00,480 --> 00:04:05,000
It's a good way to just figure if they're a real monk or a real bikinis, because if they

49
00:04:05,000 --> 00:04:12,480
get angry at you, then you know, you know, they're like, they brush you off, if they

50
00:04:12,480 --> 00:04:16,920
give you silly answers, if a monk is very buddy buddy with you, then you should know

51
00:04:16,920 --> 00:04:26,840
that they're not really dedicated to their practice, if they just want to tell jokes and

52
00:04:26,840 --> 00:04:38,680
so on and this kind of thing, engage in useless things.

53
00:04:38,680 --> 00:04:46,360
What does sadu means, it's good, so it's an appreciation, it's an expression of appreciation.

54
00:04:46,360 --> 00:04:50,280
When you appreciate something someone has done, when you appreciate something someone

55
00:04:50,280 --> 00:04:58,400
has said, or when you appreciate a good deed, this is what we say, we say sadu, it's

56
00:04:58,400 --> 00:05:16,960
okay, three times it's just a tradition.

