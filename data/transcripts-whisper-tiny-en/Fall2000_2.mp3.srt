1
00:00:00,000 --> 00:00:15,280
Today, we continue our talk on the four foundations of mindfulness.

2
00:00:15,280 --> 00:00:30,720
Now, the benefit of the practice of mindfulness is shown by the words removing covetousness

3
00:00:30,720 --> 00:00:33,240
and grief in the world.

4
00:00:33,240 --> 00:00:37,280
So this is the benefit of the practice.

5
00:00:37,280 --> 00:00:51,160
So when a yogi practices mindfulness and he is able to remove covetousness and grief, he

6
00:00:51,160 --> 00:01:06,960
is able to prevent covetousness and grief from arising in his mind.

7
00:01:06,960 --> 00:01:20,200
With regard to removing, the commentaries mentioned three kinds of removing.

8
00:01:20,200 --> 00:01:27,240
On the first kind, we will call momentary removing.

9
00:01:27,240 --> 00:01:42,680
In the second, temporary removing and the third total removing.

10
00:01:42,680 --> 00:01:56,920
Momentary removing means removing just at the moment when the mindfulness is present.

11
00:01:56,920 --> 00:02:05,080
Now this removing is called momentary removing because it can remove only at the moment

12
00:02:05,080 --> 00:02:11,040
when the mindfulness is arising.

13
00:02:11,040 --> 00:02:21,080
Now, suppose there is darkness and then you turn on the switch and there is light.

14
00:02:21,080 --> 00:02:28,920
So the moment there is light, darkness is dispelled.

15
00:02:28,920 --> 00:02:33,960
But the moment you turn off the switch again, the darkness comes back.

16
00:02:33,960 --> 00:02:43,280
So in the same way, when the removing is just for a moment, like when you are mindful,

17
00:02:43,280 --> 00:02:51,920
the mental hindrance cannot arise, but when you are not mindful, there is no mindfulness

18
00:02:51,920 --> 00:02:55,800
in your mind, the mental defalments come back.

19
00:02:55,800 --> 00:03:08,800
So such removing by actually substituting the mental defalments with some wholesome mental

20
00:03:08,800 --> 00:03:18,520
states is called momentary removing.

21
00:03:18,520 --> 00:03:30,120
The second kind of removing, temporary removing is preventing the mental defalments from

22
00:03:30,120 --> 00:03:39,920
arising for some time or for a longer period of time, longer than a moment.

23
00:03:39,920 --> 00:03:51,000
Now when, let us see, there is a pond and in the water there are water plants.

24
00:03:51,000 --> 00:04:02,280
Now, it buffalo would wait through the water and through the water plants.

25
00:04:02,280 --> 00:04:15,080
So when the buffalo goes through the water, the plants, the water plants are divided.

26
00:04:15,080 --> 00:04:19,560
And they will not come back together again for some time.

27
00:04:19,560 --> 00:04:24,200
Usually after some time, they will come back slowly and meet together.

28
00:04:24,200 --> 00:04:33,260
So in the same way, the temporary abandonment is abandonment for some time so that the

29
00:04:33,260 --> 00:04:43,320
mental defalments will not come back again for some time.

30
00:04:43,320 --> 00:04:54,400
The third kind of removing is total removing means, removing the mental defalments once

31
00:04:54,400 --> 00:05:05,280
and for all so that they do not arise in the mind of a yogi again or anymore.

32
00:05:05,280 --> 00:05:18,080
The first kind of removing a yogi achieves when he practices mindfulness or wipasana meditation.

33
00:05:18,080 --> 00:05:25,760
Now when he practices mindfulness or wipasana meditation, he makes effort and he tries to

34
00:05:25,760 --> 00:05:40,120
be mindful of the object and as the mindfulness grows in strength, he begins to see the

35
00:05:40,120 --> 00:05:49,520
objects clearly and he begins to see the objects one after another coming and going and

36
00:05:49,520 --> 00:05:59,560
also he begins to see objects arising and disappearing and so he knows that the objects

37
00:05:59,560 --> 00:06:08,040
are impermanent and suffering in the sense that they are afflicted by arising and disappearing

38
00:06:08,040 --> 00:06:15,240
and he also knows that there is no control over these objects arising and disappearing

39
00:06:15,240 --> 00:06:18,400
and also there is no substance in it.

40
00:06:18,400 --> 00:06:29,840
So when he sees in this way, he is preventing the mental defalments from arising at that

41
00:06:29,840 --> 00:06:39,800
moment or when he is seeing in this way, mental defalments do not get chance to arise

42
00:06:39,800 --> 00:06:41,600
in his mind.

43
00:06:41,600 --> 00:06:53,480
So while he is practicing mindfulness and while he experiences the clear comprehension,

44
00:06:53,480 --> 00:07:05,280
he is said to be removing the mental defalments momentarily.

45
00:07:05,280 --> 00:07:15,680
So at every moment of mindfulness practice, a yogi is achieving this first kind of removal

46
00:07:15,680 --> 00:07:21,920
of mental defalments moment by moment.

47
00:07:21,920 --> 00:07:30,440
But as soon as a yogi stops the practice of mindfulness, these mental defalments will get

48
00:07:30,440 --> 00:07:36,280
chance to arise in his mind again.

49
00:07:36,280 --> 00:07:43,920
The second kind of removal, a yogi achieves when he gets Jana.

50
00:07:43,920 --> 00:07:54,840
Now Jana is the highest state of consciousness and with Jana, these mental defalments can

51
00:07:54,840 --> 00:08:02,520
be pushed away for some period of time.

52
00:08:02,520 --> 00:08:12,880
The mental defalments removed by a Jana experience may not come back to the yogi for some

53
00:08:12,880 --> 00:08:19,080
time.

54
00:08:19,080 --> 00:08:28,000
The third kind of removal is achieved when a yogi reaches enlightenment.

55
00:08:28,000 --> 00:08:36,800
Now, at the moment of enlightenment, as I said before, a type of consciousness called

56
00:08:36,800 --> 00:08:47,800
heart consciousness arises and that thought consciousness takes Nibana as object and it removes

57
00:08:47,800 --> 00:08:50,960
or eradicates mental defalments.

58
00:08:50,960 --> 00:09:00,960
Now, there are four stages of enlightenment and at the first stage, some mental defalments

59
00:09:00,960 --> 00:09:10,800
are eradicated and at the second stage, the remaining mental defalments are made weaker

60
00:09:10,800 --> 00:09:17,640
and at the third stage, some more mental defalments are eradicated or removed and at the

61
00:09:17,640 --> 00:09:25,120
fourth stage, all the remaining mental defalments are eradicated.

62
00:09:25,120 --> 00:09:32,120
So once it are eradicated, these mental defalments will not arise again in that person.

63
00:09:32,120 --> 00:09:43,040
So that removal is called the total removal.

64
00:09:43,040 --> 00:09:53,840
So when a yogi practices Vipasana meditation he achieves momentary removal of mental defalments

65
00:09:53,840 --> 00:10:08,600
and when a yogi gains Jana he achieves the temporary removing and when he gains enlightenment

66
00:10:08,600 --> 00:10:12,440
he achieves the total removing.

67
00:10:12,440 --> 00:10:25,920
Now, Mahasi Shiro explained here that during the practice of Vipasana meditation, the

68
00:10:25,920 --> 00:10:30,240
temporary removal can be experienced.

69
00:10:30,240 --> 00:10:38,680
So he explained in this way, first a yogi practices mindfulness on objects and then

70
00:10:38,680 --> 00:10:47,640
he, his mindfulness become stronger and he gains good samadhi and he begins to see the

71
00:10:47,640 --> 00:10:54,680
true nature of things that is he begins to see the impermanent nature, suffering nature

72
00:10:54,680 --> 00:10:58,040
and non-soul nature of things.

73
00:10:58,040 --> 00:11:10,280
So when he sees the objects clearly in this way, he is said to be achieving momentary

74
00:11:10,280 --> 00:11:13,640
removal of mental defilements.

75
00:11:13,640 --> 00:11:25,480
Now when he has gained experience in achieving momentary removal, his mind will become so

76
00:11:25,480 --> 00:11:44,280
refined, his samadhi, his concentration, so strong that even if he, even if he is not

77
00:11:44,280 --> 00:11:56,040
mindful of an object, the mental defilements do not arise on these objects.

78
00:11:56,040 --> 00:12:05,760
Normally if a yogi does not practice mindfulness on an object, then the mental defilements

79
00:12:05,760 --> 00:12:08,360
will arise regarding that object.

80
00:12:08,360 --> 00:12:19,600
But now this yogi has gained so much experience in momentary removal that even if he does

81
00:12:19,600 --> 00:12:29,840
not practice mindfulness on an object, no mental defilements will arise in him.

82
00:12:29,840 --> 00:12:44,240
Even if he stopped meditating, his mind is so pure that no mental defilements will arise

83
00:12:44,240 --> 00:12:45,800
in his mind.

84
00:12:45,800 --> 00:12:53,960
So a yogi may think that I have eradicated mental defilements or no gross mental defilements

85
00:12:53,960 --> 00:12:56,400
can arise in my mind now.

86
00:12:56,400 --> 00:13:14,480
So that the ability to prevent mental defilements from arising even when they are not

87
00:13:14,480 --> 00:13:25,680
made the object of mindfulness is what we call temporary abandonment or temporary removal

88
00:13:25,680 --> 00:13:31,760
in the practice of vipassana.

89
00:13:31,760 --> 00:13:44,280
Again please note that removing here means not giving the mental defilements chance to

90
00:13:44,280 --> 00:13:47,080
arise.

91
00:13:47,080 --> 00:14:00,920
And that is achieved when a yogi is mindful and when he clearly comprehends the objects.

92
00:14:00,920 --> 00:14:13,240
So being mindful and clearly comprehending a yogi is said to be removing the mental defilements.

93
00:14:13,240 --> 00:14:23,440
So that means he does not have to make special effort to remove a mental defilement.

94
00:14:23,440 --> 00:14:34,000
But when he is mindful of the object, when he comprehends the object clearly that means

95
00:14:34,000 --> 00:14:41,960
when he sees the true nature of things, then he is at the same time said to be removing

96
00:14:41,960 --> 00:14:49,080
the mental defilements.

97
00:14:49,080 --> 00:15:00,280
And the words in the world should be understood according to where they are mentioned.

98
00:15:00,280 --> 00:15:08,640
So in the number one sentence, in the world means in the body.

99
00:15:08,640 --> 00:15:14,520
And in number two sentence, in the world means in the feelings, in number three sentence,

100
00:15:14,520 --> 00:15:17,440
in the world means in the consciousness.

101
00:15:17,440 --> 00:15:29,320
And in number four, sentence in the world means in the Dharma objects.

102
00:15:29,320 --> 00:15:36,640
So in number one sentence, Buddha said, if he could dwells contemplating the body in

103
00:15:36,640 --> 00:15:37,840
the body.

104
00:15:37,840 --> 00:15:51,040
So the word body is repeated and that is for the observation to be precise.

105
00:15:51,040 --> 00:15:57,960
So the commentary explains that contemplating or observing the body in the body means, observing

106
00:15:57,960 --> 00:16:03,440
the body in the body and not observing and the feeling in the body, consciousness in the

107
00:16:03,440 --> 00:16:06,920
body and Dharma objects in the body.

108
00:16:06,920 --> 00:16:26,280
So it is to show the precision of observation that the word body is mentioned twice.

109
00:16:26,280 --> 00:16:33,240
In this sentence, also the word body is to be understood according to the context.

110
00:16:33,240 --> 00:16:42,920
Now in the discourse on the four foundations of mindfulness, the body contemplation is described

111
00:16:42,920 --> 00:16:48,720
in 14 different ways.

112
00:16:48,720 --> 00:16:55,240
The first section is the mindfulness of breathing.

113
00:16:55,240 --> 00:17:01,800
Now the second is the mindfulness of the postures of the body.

114
00:17:01,800 --> 00:17:09,800
And the third is called the section on clear comprehension that is mindfulness of smaller

115
00:17:09,800 --> 00:17:20,080
activities like going forward, going back, stretching, bending and so on.

116
00:17:20,080 --> 00:17:31,320
And then there is a section on repulsiveness of the body or 32 parts of the body.

117
00:17:31,320 --> 00:17:36,040
And then there is a section on the four material elements.

118
00:17:36,040 --> 00:17:44,720
And next there is section on actually nine sections on symmetry contemplations.

119
00:17:44,720 --> 00:17:50,760
So all together there are 14 kinds of contemplation of the body.

120
00:17:50,760 --> 00:18:01,720
And so regarding the first section which is a section on breathing, the body means breathing.

121
00:18:01,720 --> 00:18:09,760
In the second section where the postures of the body are mentioned, then the postures of

122
00:18:09,760 --> 00:18:13,400
the body are called body and so on.

123
00:18:13,400 --> 00:18:21,080
So the body may mean sometimes the whole body, sometimes just the breathing in and breathing

124
00:18:21,080 --> 00:18:29,280
out or stepping forward, going back and so on.

125
00:18:29,280 --> 00:18:37,440
So when you are practicing mindfulness of breathing in and out and you are doing the

126
00:18:37,440 --> 00:18:44,800
contemplation on the body and when you are mindful of the postures, sitting, standing,

127
00:18:44,800 --> 00:18:51,040
walking and lying down, you are practicing contemplation on the body.

128
00:18:51,040 --> 00:19:02,040
And when you are mindful of going forward, going back, stretching, bending, eating and

129
00:19:02,040 --> 00:19:06,960
so on, you are practicing contemplation in the body.

130
00:19:06,960 --> 00:19:17,880
So there are all together 14 kinds of contemplation of the body taught in this discourse.

131
00:19:17,880 --> 00:19:28,320
Now the last nine contemplations of the body are called symmetry contemplations.

132
00:19:28,320 --> 00:19:36,880
Actually that is the contemplation on the different conditions of the dead body.

133
00:19:36,880 --> 00:19:47,560
So first it becomes swollen or festered and then there are other conditions of the dead

134
00:19:47,560 --> 00:19:54,840
body until it is reduced to bones and powder.

135
00:19:54,840 --> 00:20:04,880
So the symmetry contemplations mean contemplation on the conditions of the dead body, say

136
00:20:04,880 --> 00:20:10,760
laughter after the person is dead.

137
00:20:10,760 --> 00:20:18,760
Now when you are mindful of the rising and falling movements of the abdomen, you are doing

138
00:20:18,760 --> 00:20:30,080
the contemplation on the body that is mindfulness of the full mindfulness of the air element.

139
00:20:30,080 --> 00:20:36,120
Now you all know that there are four material elements, the element of earth, the element

140
00:20:36,120 --> 00:20:43,120
of water, the element of fire and the element of air.

141
00:20:43,120 --> 00:20:52,000
Among these four, the rising and falling movements belong to the air element.

142
00:20:52,000 --> 00:21:00,560
Because they are actually caused by the breathing in and out and so they are called air

143
00:21:00,560 --> 00:21:14,880
element and air element has the characteristic of extending and it has a function of moving.

144
00:21:14,880 --> 00:21:25,960
So when you concentrate not on the abdomen actually but on the movements then you are concentrating

145
00:21:25,960 --> 00:21:32,600
on the air element, on the function of air element.

146
00:21:32,600 --> 00:21:42,160
So when you practice mindfulness of the rising and falling of the movement, you are actually

147
00:21:42,160 --> 00:22:00,920
practicing the mindfulness of the full element here, mindfulness of the air element.

148
00:22:00,920 --> 00:22:18,360
Now the second contemplation, contemplation of feelings is a mental state.

149
00:22:18,360 --> 00:22:27,120
Whenever we experience an object, whenever we see an object or we hear an object, we smell

150
00:22:27,120 --> 00:22:30,560
an object and so on.

151
00:22:30,560 --> 00:22:37,160
There is the mental state feeling arising along with the consciousness.

152
00:22:37,160 --> 00:22:47,440
So the mental state feeling has the characteristic of experiencing or enjoying the taste

153
00:22:47,440 --> 00:22:50,920
of the object.

154
00:22:50,920 --> 00:22:59,120
So please note that feeling here is a mental state.

155
00:22:59,120 --> 00:23:07,040
But whenever we talk about feeling, our mind is always good to the physical thing.

156
00:23:07,040 --> 00:23:17,560
Say when you say pain, then we always go to the material thing that is called pain.

157
00:23:17,560 --> 00:23:25,480
And when we are mindful of pain, actually we are mindful of the feeling of that pain.

158
00:23:25,480 --> 00:23:29,960
Pain is not mental state, pain is a physical state.

159
00:23:29,960 --> 00:23:36,880
But this contemplation on the feeling is called mental contemplation.

160
00:23:36,880 --> 00:23:47,240
So contemplation of feeling is the contemplation of a mental state which is called feeling.

161
00:23:47,240 --> 00:23:53,920
And that arises in our mind, not in the physical body.

162
00:23:53,920 --> 00:24:07,520
But it is so associated with, so connected with the sensations in the body that in practice,

163
00:24:07,520 --> 00:24:14,720
we just take these two as one.

164
00:24:14,720 --> 00:24:20,840
So when we say, when you have pain, concentrate on the pain and be mindful of it, and

165
00:24:20,840 --> 00:24:29,840
that means concentrate on the experience in your mind of that sensation and not that sensation

166
00:24:29,840 --> 00:24:31,480
actually.

167
00:24:31,480 --> 00:24:38,280
So the sensation I mean something that is in the body.

168
00:24:38,280 --> 00:24:49,760
Now when the material properties in the body are in good state doing fine, then we don't

169
00:24:49,760 --> 00:24:51,760
feel any pain.

170
00:24:51,760 --> 00:24:59,720
But when we feel pain, that means the material properties in the body have become deteriorated

171
00:24:59,720 --> 00:25:01,400
or they have changed.

172
00:25:01,400 --> 00:25:06,680
So that is what we call pain or sensation.

173
00:25:06,680 --> 00:25:15,440
And the experience of that pain in our mind, experience in our mind of that pain is what

174
00:25:15,440 --> 00:25:17,760
we call feeling.

175
00:25:17,760 --> 00:25:26,480
So contemplation on feeling is a mental contemplation or contemplation of a mental object

176
00:25:26,480 --> 00:25:34,080
and not of the physical object.

177
00:25:34,080 --> 00:25:44,200
Basically there are three kinds of feelings, pleasant and pleasant and neutral.

178
00:25:44,200 --> 00:25:51,680
And each of these three can be worldly and unworthy.

179
00:25:51,680 --> 00:25:58,560
So worldly feeling means feelings, you experience when you are doing worldly things, when

180
00:25:58,560 --> 00:26:03,040
you are enjoying good things and so on.

181
00:26:03,040 --> 00:26:09,440
And worldly feeling means the feelings that you experience when you are doing meritorious

182
00:26:09,440 --> 00:26:14,240
tea, when you are practicing meditation and so on.

183
00:26:14,240 --> 00:26:24,480
So with the three basic feelings, we say that there are nine kinds of feeling and there

184
00:26:24,480 --> 00:26:35,760
are nine kinds of feeling taught in the discourse on the foundations of mindfulness.

185
00:26:35,760 --> 00:26:43,320
And whatever feeling there is, we are to be mindful of that feeling.

186
00:26:43,320 --> 00:26:50,760
That is why when there is a feeling of pain, we try to be mindful, saying pain, pain,

187
00:26:50,760 --> 00:26:59,240
pain, when there is stiffness and we try to be mindful of that stiffness, saying stiffness,

188
00:26:59,240 --> 00:27:01,280
stiffness, stiffness and so on.

189
00:27:01,280 --> 00:27:06,960
Actually whatever feeling there is, whether it is a pleasant feeling or unpleasant feeling

190
00:27:06,960 --> 00:27:11,520
or a neutral feeling, we are to be mindful of that feeling.

191
00:27:11,520 --> 00:27:18,240
Because if we do not practice mindfulness on feeling, we may have wrong notions of the feeling,

192
00:27:18,240 --> 00:27:25,920
that feeling lies for some time, that feeling is good and so on.

193
00:27:25,920 --> 00:27:38,480
So in order to see the true nature of feeling, we have to be mindful of the feeling.

194
00:27:38,480 --> 00:27:44,000
And contemplation on feeling is not to get rid of these feelings.

195
00:27:44,000 --> 00:27:51,920
Now when there is pain, you are instructed to be mindful of that pain or to make mental

196
00:27:51,920 --> 00:27:55,520
notes as pain, pain and so on.

197
00:27:55,520 --> 00:28:01,800
And many yogis think that it is to get rid of that pain that you have to be mindful

198
00:28:01,800 --> 00:28:05,000
of it, saying pain, pain, pain.

199
00:28:05,000 --> 00:28:08,560
But what the Buddha taught is different.

200
00:28:08,560 --> 00:28:19,920
So Buddha said, for the full understanding of these three kinds of feelings, you practice

201
00:28:19,920 --> 00:28:22,880
the foundations of mindfulness.

202
00:28:22,880 --> 00:28:28,760
So when there is feeling and you try to be mindful of that feeling, that means you are

203
00:28:28,760 --> 00:28:34,560
trying to see the true nature of that feeling.

204
00:28:34,560 --> 00:28:42,240
You are trying to see what feeling is and you are trying to see that that feeling arises

205
00:28:42,240 --> 00:28:47,880
and disappears and so that feeling is implemented and so on.

206
00:28:47,880 --> 00:28:58,920
So it is in order to understand fully in this way that you have to be mindful of feelings,

207
00:28:58,920 --> 00:29:10,160
not to get rid of pain or some unpleasant sensations.

208
00:29:10,160 --> 00:29:16,840
So when you practice feeling contemplation, you make effort and you try to be mindful

209
00:29:16,840 --> 00:29:27,280
and you get concentration and you see the feelings clearly or you comprehend the feeling

210
00:29:27,280 --> 00:29:28,600
clearly.

211
00:29:28,600 --> 00:29:35,960
So when you are mindful of the feeling, when you are clearly comprehending the feeling, you

212
00:29:35,960 --> 00:29:47,320
are actually removing mental defilements regarding that feeling.

213
00:29:47,320 --> 00:29:53,880
When the feeling is good, coverlessness or craving can arise.

214
00:29:53,880 --> 00:29:58,760
So when you have a good feeling, you want it to last for a long time and you are attached

215
00:29:58,760 --> 00:30:00,000
to it.

216
00:30:00,000 --> 00:30:09,080
And when the feeling is painful and you have what is called grief, you have ill will

217
00:30:09,080 --> 00:30:17,960
or you have anger, that is if you do not practice mindfulness.

218
00:30:17,960 --> 00:30:26,800
So when you practice mindfulness, then they will not get chance to arise in your mind.

219
00:30:26,800 --> 00:30:38,920
Now even when the feeling is bad or unpleasant, craving can arise.

220
00:30:38,920 --> 00:30:48,200
Not with regard to that unpleasant feeling, but when there is an unpleasant feeling,

221
00:30:48,200 --> 00:30:55,400
you long for a pleasant feeling, you want it to go away and you want to have the pleasant

222
00:30:55,400 --> 00:30:57,440
feeling arise in you.

223
00:30:57,440 --> 00:31:05,000
So even when the feeling is unpleasant, the attachment or craving can arise.

224
00:31:05,000 --> 00:31:17,080
That is if you do not practice mindfulness.

225
00:31:17,080 --> 00:31:22,520
Now we come to the third foundation of mindfulness.

226
00:31:22,520 --> 00:31:30,920
Now you are practicing meditation and you try to be mindful of the breath or the movements

227
00:31:30,920 --> 00:31:34,120
of the abdomen.

228
00:31:34,120 --> 00:31:42,440
Then after some time, you are mind wanders and you are aware of it and so you make notes

229
00:31:42,440 --> 00:31:46,760
as wandering, wandering, wandering.

230
00:31:46,760 --> 00:31:54,840
Now, you are doing contemplation of consciousness when you are noting your mind like that,

231
00:31:54,840 --> 00:31:59,000
wandering, wandering, wandering.

232
00:31:59,000 --> 00:32:13,800
Now, in order to understand consciousness as thoughts in Buddhism, you need to know abidhamma.

233
00:32:13,800 --> 00:32:26,560
According to abidhamma, there are two things that are called namma or mind.

234
00:32:26,560 --> 00:32:39,840
And abidhamma is thought to be a combination of consciousness and mental factors.

235
00:32:39,840 --> 00:32:49,440
And it is defined as the awareness of the object.

236
00:32:49,440 --> 00:33:03,000
It is a very simple awareness of the object and not the awareness we use in our talk on

237
00:33:03,000 --> 00:33:10,920
Vipasana meditation, so when we talk about Vipasana meditation, we say we try to be aware

238
00:33:10,920 --> 00:33:16,040
of the object and that awareness is actually a mindfulness.

239
00:33:16,040 --> 00:33:23,040
But here, consciousness, when we say consciousness is the awareness of the object, it is

240
00:33:23,040 --> 00:33:32,840
just a mere awareness of the object, not actually at that moment, not actually knowing

241
00:33:32,840 --> 00:33:41,240
whether it is, it is white or black or whatever, but just the awareness of the object.

242
00:33:41,240 --> 00:33:50,960
It is a very simple awareness of the object and that is called consciousness or chita

243
00:33:50,960 --> 00:33:57,320
in pali and abidhamma.

244
00:33:57,320 --> 00:34:08,800
And this awareness of the object or consciousness is always accompanied by mental factors.

245
00:34:08,800 --> 00:34:17,480
Now, there are many mental factors, actually there are 52 that accompany different types

246
00:34:17,480 --> 00:34:20,040
of consciousness.

247
00:34:20,040 --> 00:34:31,080
Now, attachment is a mental factor, hate or anger is a mental factor, delusion or ignorance

248
00:34:31,080 --> 00:34:40,160
is a mental factor, jealousy is a mental factor.

249
00:34:40,160 --> 00:34:48,280
And on the good side, the confidence is a mental factor, mindfulness is mental factor,

250
00:34:48,280 --> 00:35:00,640
understanding is mental factor, mental factor, so there are many mental factors.

251
00:35:00,640 --> 00:35:07,600
Some are good mental factors, some are bad mental factors and some are common to both

252
00:35:07,600 --> 00:35:09,680
good and bad.

253
00:35:09,680 --> 00:35:18,280
So the consciousness is always accompanied by some of these mental factors.

254
00:35:18,280 --> 00:35:27,200
And when the consciousness is accompanied by good mental factors, then we call that consciousness,

255
00:35:27,200 --> 00:35:31,080
good consciousness or wholesome consciousness.

256
00:35:31,080 --> 00:35:37,840
When the consciousness is accompanied by bad mental factors and it is called unwholesome

257
00:35:37,840 --> 00:35:42,640
consciousness and so on.

258
00:35:42,640 --> 00:35:50,240
Although consciousness and mental factors arise at the same time, consciousness is said

259
00:35:50,240 --> 00:35:54,800
to be their leader.

260
00:35:54,800 --> 00:36:03,080
Consciousness is said to be the full run of these mental factors because when there is

261
00:36:03,080 --> 00:36:10,280
no consciousness there can be no mental factors, no mental factors can arise when there

262
00:36:10,280 --> 00:36:13,200
is no consciousness.

263
00:36:13,200 --> 00:36:21,160
So consciousness is said to be their full run or leader.

264
00:36:21,160 --> 00:36:32,000
This consciousness is always with us, so long as we are living, we are never without consciousness.

265
00:36:32,000 --> 00:36:42,920
Then when we are first asleep in a deep sleep there is still consciousness of some kind.

266
00:36:42,920 --> 00:37:00,760
So we can never be without consciousness as long as we are alive.

267
00:37:00,760 --> 00:37:09,560
So awareness of the consciousness of the consciousness is called contemplating contemplation

268
00:37:09,560 --> 00:37:22,520
on the consciousness or the third foundation of mindfulness.

269
00:37:22,520 --> 00:37:33,560
So when a yogi makes effort and tries to be mindful of the consciousness as his mindfulness

270
00:37:33,560 --> 00:37:43,160
becomes stronger and as he gets concentration he begins to see the consciousness clearly

271
00:37:43,160 --> 00:37:46,080
and its characteristics and so on.

272
00:37:46,080 --> 00:37:56,160
So when he is seeing the consciousness, the true nature of consciousness then he is said

273
00:37:56,160 --> 00:38:02,960
to be removing mental defilements regarding that consciousness.

274
00:38:02,960 --> 00:38:10,440
That means regarding that consciousness he will not have attachment to the consciousness

275
00:38:10,440 --> 00:38:15,640
or anger to the consciousness.

276
00:38:15,640 --> 00:38:31,880
Then fourth foundation of mindfulness is contemplation on the damas, on the damas objects.

277
00:38:31,880 --> 00:38:46,040
Now when we explain the discourses that are originally in Pali and when we explain them

278
00:38:46,040 --> 00:38:56,200
in English sometimes we have problems with the translation.

279
00:38:56,200 --> 00:39:02,760
Some words actually cannot be translated.

280
00:39:02,760 --> 00:39:06,160
One of such words is the word dhamma.

281
00:39:06,160 --> 00:39:09,000
The word dhamma has many meanings.

282
00:39:09,000 --> 00:39:13,000
It means differently in different places.

283
00:39:13,000 --> 00:39:17,840
So we cannot give just one meaning of the word dhamma.

284
00:39:17,840 --> 00:39:33,760
Now when you say dhamma saranan kachami I go to dhamma for refuge then dhamma means the

285
00:39:33,760 --> 00:39:42,360
Buddha realized when he became the Buddha and also his teachings.

286
00:39:42,360 --> 00:39:45,840
Sometimes dhamma means nature.

287
00:39:45,840 --> 00:39:48,760
Sometimes it means learning and so on.

288
00:39:48,760 --> 00:39:57,440
So there are many meanings of the word dhamma and so we cannot have just one English translation

289
00:39:57,440 --> 00:40:02,040
for the word dhamma in all places.

290
00:40:02,040 --> 00:40:10,520
And here also the word dhamma cannot be translated into English.

291
00:40:10,520 --> 00:40:21,840
Many translators translate it as mind object or mental objects but I think that is not

292
00:40:21,840 --> 00:40:26,120
accurate.

293
00:40:26,120 --> 00:40:34,680
Mind object because there are many things that are called dhamma and in this discourses

294
00:40:34,680 --> 00:40:42,880
on the four foundations of mindfulness if you go to the section on the contemplation

295
00:40:42,880 --> 00:40:49,600
of the dhamma you will find that the five hindrances are called dhamma, five aggregates

296
00:40:49,600 --> 00:40:58,040
of clinging are called dhamma and the six external bases and six internal bases are called

297
00:40:58,040 --> 00:41:03,600
dhamma, seven factors of enlightenment are called dhamma and the four noble troops are called

298
00:41:03,600 --> 00:41:04,720
dhamma.

299
00:41:04,720 --> 00:41:12,360
So to cover all these how can we translate the word dhamma?

300
00:41:12,360 --> 00:41:22,200
Some of them are mental and some of them are material so we cannot call them mental objects

301
00:41:22,200 --> 00:41:25,480
or mind objects.

302
00:41:25,480 --> 00:41:37,120
So it is better to leave it as it is and explain when we use the word dhamma in a certain

303
00:41:37,120 --> 00:41:38,360
context.

304
00:41:38,360 --> 00:41:44,160
So here also we cannot translate this word into any language.

305
00:41:44,160 --> 00:41:50,080
So we say contemplation on the dhamma in the dhamma objects.

306
00:41:50,080 --> 00:41:57,040
So if we want to understand what dhamma means here you go to the section on the contemplation

307
00:41:57,040 --> 00:42:02,480
of the dhamma and there we find those mental hindrances and so on.

308
00:42:02,480 --> 00:42:08,440
So in this discourses, mental hindrances and others there are all the five groups.

309
00:42:08,440 --> 00:42:22,680
They are called dhamma like the word dhamma.

310
00:42:22,680 --> 00:42:27,440
The word cheetah which is translated here as consciousness is another difficult word to translate.

311
00:42:27,440 --> 00:42:35,480
But here many others translate the word cheetah as consciousness.

312
00:42:35,480 --> 00:42:48,280
Now here also for one of a better word we have to use the word consciousness for cheetah.

313
00:42:48,280 --> 00:42:51,880
But again it is not accurate.

314
00:42:51,880 --> 00:42:59,200
Now as you now know we are with consciousness always even when we are asleep we are with

315
00:42:59,200 --> 00:43:02,120
consciousness according to dhamma.

316
00:43:02,120 --> 00:43:08,340
But people would say when you are in deep sleep you are unconscious or when you are

317
00:43:08,340 --> 00:43:10,800
fine that you are unconscious.

318
00:43:10,800 --> 00:43:20,160
So this word also is not actually accurate for the word cheetah in pali but we have to use

319
00:43:20,160 --> 00:43:26,200
some English word for this word and so the word consciousness is used.

320
00:43:26,200 --> 00:43:34,120
So when we use the word consciousness it is important that we define it.

321
00:43:34,120 --> 00:43:38,840
We tell people what we mean by the word consciousness.

322
00:43:38,840 --> 00:43:46,560
Just using the word consciousness will not be enough because people may misunderstand.

323
00:43:46,560 --> 00:43:57,920
So here in a bithamma the English word consciousness is used for the word cheetah because

324
00:43:57,920 --> 00:44:11,200
there is no other better word for the word cheetah.

325
00:44:11,200 --> 00:44:24,200
Now the objects represented by the word dhamma are many and the objects that are called

326
00:44:24,200 --> 00:44:35,120
dhamma in this discourse overlap with the other objects of body contemplation, feeling

327
00:44:35,120 --> 00:44:39,360
contemplation and consciousness contemplation.

328
00:44:39,360 --> 00:44:50,320
So suppose you are practicing meditation and somebody make a noise and you are angry.

329
00:44:50,320 --> 00:45:03,720
So how would you note angry or anger?

330
00:45:03,720 --> 00:45:09,880
If you are making notes angry angry I think you are doing the third foundation of mindfulness

331
00:45:09,880 --> 00:45:19,520
because I am angry means my mind is accompanied by anger so it is very delicate.

332
00:45:19,520 --> 00:45:28,320
But when you are contemplating making notes as anger, anger you are doing the dhamma object

333
00:45:28,320 --> 00:45:32,320
contemplation or the fourth foundation of mindfulness.

334
00:45:32,320 --> 00:45:38,160
Because now you are taking anger which is one of the mental hindrances as the object

335
00:45:38,160 --> 00:45:42,080
of your attention.

336
00:45:42,080 --> 00:45:50,720
Although it is not important to try out I mean to find out what contemplation you are doing

337
00:45:50,720 --> 00:45:53,600
at the moment.

338
00:45:53,600 --> 00:46:03,840
It is still good to know say what contemplation you are doing but please do not try to find

339
00:46:03,840 --> 00:46:07,480
out what contemplation you are doing at the moment.

340
00:46:07,480 --> 00:46:17,960
It will take you away from the real practice of mindfulness and drag you into thinking.

341
00:46:17,960 --> 00:46:27,760
So sometimes it is difficult to see what contemplation we are doing, contemplation of consciousness

342
00:46:27,760 --> 00:46:33,000
or contemplation of the dhamma objects.

343
00:46:33,000 --> 00:46:43,720
But whatever object it is so long as you are mindful then you are doing the practice

344
00:46:43,720 --> 00:46:46,240
of mindfulness meditation.

345
00:46:46,240 --> 00:46:52,240
So you do not have to find out what contemplation you are doing at the moment.

346
00:46:52,240 --> 00:47:09,680
Later on you may think about it later on means after the practice of meditation.

347
00:47:09,680 --> 00:47:20,840
Now this is the four foundations of mindfulness in brief so if you want to know more details

348
00:47:20,840 --> 00:47:28,600
I must refer you to the discourse on the foundations of mindfulness.

349
00:47:28,600 --> 00:47:41,880
But what we learn from this passage is that we must make effort, we must practice mindfulness

350
00:47:41,880 --> 00:47:48,520
so that we clearly comprehend or we clearly see the objects.

351
00:47:48,520 --> 00:47:56,840
That means we see the true nature of the objects.

352
00:47:56,840 --> 00:48:08,160
Only when we clearly comprehend the objects, only when we see the objects as impermanence,

353
00:48:08,160 --> 00:48:17,560
suffering and non-soul, will we be able to prevent covetousness and grief from arising

354
00:48:17,560 --> 00:48:20,600
in our minds.

355
00:48:20,600 --> 00:48:31,680
So in order to prevent them from arising in our minds we need to comprehend the objects

356
00:48:31,680 --> 00:48:40,240
clearly and in order to see the objects clearly we need to practice mindfulness and for

357
00:48:40,240 --> 00:48:44,800
the practice of mindfulness we must make an effort.

358
00:48:44,800 --> 00:48:53,600
So if we make effort and if we practice mindfulness we will not fail to comprehend the

359
00:48:53,600 --> 00:48:55,320
objects clearly.

360
00:48:55,320 --> 00:49:02,400
So when we comprehend the objects clearly then we will be removing covetousness and grief

361
00:49:02,400 --> 00:49:07,680
at the same time.

362
00:49:07,680 --> 00:49:23,880
So let us all make effort to be mindful, so as to comprehend the objects clearly and remove

363
00:49:23,880 --> 00:49:32,800
covetousness and grief in the world.

364
00:49:32,800 --> 00:49:40,560
The

