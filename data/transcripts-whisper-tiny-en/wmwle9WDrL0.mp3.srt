1
00:00:30,000 --> 00:00:57,120
There is some technical difficulties this evening, good evening, welcome to evening dumber.

2
00:00:57,120 --> 00:01:10,120
So tonight we are looking at the third section of the sabasa wasut.

3
00:01:10,120 --> 00:01:38,580
But the sabana means using, making use of, and so again these sections are not directly

4
00:01:38,580 --> 00:01:49,040
relating to our meditation practice necessarily, but our are still important as a support

5
00:01:49,040 --> 00:02:02,520
for our practice, but the sabanas referring to, making use of, the requisites

6
00:02:02,520 --> 00:02:11,740
with the acknowledgement that practically speaking for our practice to succeed, we need

7
00:02:11,740 --> 00:02:26,540
to make use of material possessions.

8
00:02:26,540 --> 00:02:33,100
And so there is this curious sort of dichotomy that we are stuck in where, on the one hand

9
00:02:33,100 --> 00:02:40,060
we want to see everything is just experiences, but on the other hand we clearly have

10
00:02:40,060 --> 00:02:59,060
to deal with concepts, with things, with entities, food, shelter, clothing, medicine, food,

11
00:02:59,060 --> 00:03:09,380
and put them in order, clothing, food, shelter in medicine, these are the four requisites.

12
00:03:09,380 --> 00:03:16,940
The first thing is to get clear about this distinction, and to make clear in our minds

13
00:03:16,940 --> 00:03:29,940
what is necessary, the Buddha laid out these four things, but they really weren't all

14
00:03:29,940 --> 00:03:35,820
that was necessary even for monks, even in the time of the Buddha, so in the time of the

15
00:03:35,820 --> 00:03:50,540
Buddha the monks would make use of various things, needles, for example, water containers,

16
00:03:50,540 --> 00:04:02,060
razor blades, chairs, tables, I think, I'm not sure about tables, probably, but I don't

17
00:04:02,060 --> 00:04:05,860
know that it mentions them.

18
00:04:05,860 --> 00:04:16,860
Even fans, fans are useful for doing away with mosquitoes, towels, for example, lots of

19
00:04:16,860 --> 00:04:37,700
different things, but this is a key topic, understanding of what is necessary, what we need

20
00:04:37,700 --> 00:04:51,100
and what we just like or want, even down to the point of what makes us comfortable.

21
00:04:51,100 --> 00:05:00,420
So on the extreme we have all sorts of indulgences, let's say drugs, let's look at drugs

22
00:05:00,420 --> 00:05:08,260
that there's an extreme for you, for some people drugs like cocaine or heroin seem very

23
00:05:08,260 --> 00:05:17,660
necessary, but they're not necessary, right, and there's a drug addict need heroin, it's

24
00:05:17,660 --> 00:05:24,300
a good question, and it's a good question because it glaringly shows us the difference,

25
00:05:24,300 --> 00:05:35,820
which shows us a very coarse example of want and like rather than need, and the drug addict

26
00:05:35,820 --> 00:05:44,380
is not going to die without heroin, but it's incredibly unpleasant, of course, to go without,

27
00:05:44,380 --> 00:06:00,660
I think that sets a good baseline or it neatly demarcates the line between like and need

28
00:06:00,660 --> 00:06:08,260
food, for example, food isn't just something we like, no question there, you don't get

29
00:06:08,260 --> 00:06:13,380
food, I mean, there is a question, of course, because if you don't get the food, you'll

30
00:06:13,380 --> 00:06:27,380
just die, so there's a question of need for what, right, and practically speaking, again,

31
00:06:27,380 --> 00:06:39,740
it shows that generally we're not strict about our adherence to ultimate reality, we can't

32
00:06:39,740 --> 00:06:46,140
be, right, you can say, okay, ultimate reality is it's just an experience if I die of hunger,

33
00:06:46,140 --> 00:06:51,900
well, that's a part of my meditation practice, but practically speaking, that's foolish,

34
00:06:51,900 --> 00:06:57,180
for most people, it's foolish because you'll die before you, before you really realize

35
00:06:57,180 --> 00:07:04,860
that, yes, it may be true that reality is just experience, but until you realize that,

36
00:07:04,860 --> 00:07:16,620
I do very careful to be careful to protect your meditation practice, and I think that points

37
00:07:16,620 --> 00:07:25,860
to something very important for those practicing at home, that there are many complications

38
00:07:25,860 --> 00:07:37,980
in life that you have to deal with, you know, having a job livelihood, relationship, social

39
00:07:37,980 --> 00:07:51,060
norms and necessity, and that sort of social responsibilities and expectations that it won't

40
00:07:51,060 --> 00:08:00,820
do for us to to to discard and to ignore, yes, ultimately relationships and so on are all

41
00:08:00,820 --> 00:08:09,060
meaningless, but like if you don't cut your lawn, people complain if you don't shave

42
00:08:09,060 --> 00:08:16,500
your beard, people, well, depends on some bad example perhaps, but if you don't wash your

43
00:08:16,500 --> 00:08:22,140
clothes, let's say there's a good example, if you don't shower, you know, shower and then

44
00:08:22,140 --> 00:08:27,620
you get on the bus, well, you can say, yeah, it's just smell, but practically speaking

45
00:08:27,620 --> 00:08:36,860
there will be problems, right, yes, if you're enlightened and you don't have any attachment,

46
00:08:36,860 --> 00:08:45,380
funny thing is enlightened beings quite clearly do a dear to social norms and concern

47
00:08:45,380 --> 00:08:53,780
themselves with the practicalities of life, you know, it's just practical, just reasonable.

48
00:08:53,780 --> 00:09:03,820
So this dichotomy of understanding the building blocks, but living in living as a human

49
00:09:03,820 --> 00:09:15,260
in concepts, right, people places things. So that's what Patisei want us about, it's about

50
00:09:15,260 --> 00:09:25,300
making use of things and understanding what we need and what we need isn't just to stay

51
00:09:25,300 --> 00:09:37,500
alive, we need is to maintain our practice. So let's be clear, yes, it's more than just

52
00:09:37,500 --> 00:09:45,780
staying alive, it's more than just a bare minimum, but it should never go beyond what is

53
00:09:45,780 --> 00:09:52,380
supportive of our practice, which is supportive of goodness, supportive of happiness and

54
00:09:52,380 --> 00:09:57,900
happiness being something very different from pleasure. A drug addict will tell you, yes,

55
00:09:57,900 --> 00:10:04,500
I need the drugs to be happy, but it's not true. Drugs don't actually need the happiness,

56
00:10:04,500 --> 00:10:13,940
they need to pleasure, which is something very different from happiness. I mean, clearly

57
00:10:13,940 --> 00:10:28,580
because it makes the miserable entertainment, entertainment is another one, entertainment's

58
00:10:28,580 --> 00:10:40,220
very easy to get caught up, something that's clearly not of any use, right, for the entertainment

59
00:10:40,220 --> 00:10:44,460
side of it. Sure, you could watch a movie and you could learn something from it, there

60
00:10:44,460 --> 00:10:51,620
could be educational value. I think that's important to recognize, but you're just watching

61
00:10:51,620 --> 00:10:56,940
something for entertainment value, that's not necessary or useful, it's actually detrimental,

62
00:10:56,940 --> 00:11:14,100
right? And so there are many things in life that are clearly not at all necessary, and

63
00:11:14,100 --> 00:11:20,020
that's where we draw the line, hopefully, I mean, reasonably, it's not likely to draw the

64
00:11:20,020 --> 00:11:27,900
line anywhere near that, but the closer we can get the better, right? I mean, speaking

65
00:11:27,900 --> 00:11:33,620
of people living in the world, there's a lot that we have, that is for our pleasure and

66
00:11:33,620 --> 00:11:43,900
for comfort, people who have nice, comfy, well, televisions, let's say, stereo systems,

67
00:11:43,900 --> 00:11:52,620
there's a lot of things that are just for pleasure, just for entertainment, coffee,

68
00:11:52,620 --> 00:12:01,100
maybe, no, coffee's complicated, junk food, but there we get, okay, so certain things

69
00:12:01,100 --> 00:12:07,340
that are clearly just for pleasure, but then there is the things that are necessary and

70
00:12:07,340 --> 00:12:13,100
it's a matter of using, and this is what the Buddha is really talking about here, using

71
00:12:13,100 --> 00:12:22,140
those things that are necessary for the right reasons and in the right way, using those

72
00:12:22,140 --> 00:12:27,220
things that are necessary for the right reasons and in the right way, which means just

73
00:12:27,220 --> 00:12:35,260
because something is necessary, you know, supportive of your practice, theoretically, doesn't

74
00:12:35,260 --> 00:12:45,420
mean you're using it that way, take clothing, for example, clothing is necessary and

75
00:12:45,420 --> 00:12:53,020
the very base of, it's necessary because if you don't wear your naked and practically speaking

76
00:12:53,020 --> 00:12:59,420
that's a problem, it's a problem, it's a distraction, can't walk around in the world naked,

77
00:12:59,420 --> 00:13:09,980
it's not in this day and age, and it's a problem because of the suffering that comes

78
00:13:09,980 --> 00:13:17,260
as well, it's a distraction, very hard to meditate when you're having flies all over your

79
00:13:17,260 --> 00:13:24,260
body and bugs and heat and cold and so on, and you're cold because you're naked

80
00:13:24,260 --> 00:13:34,220
and close our useful support for the practice, but of course we use clothes for so much

81
00:13:34,220 --> 00:13:40,220
more, we use them to look beautiful, to look handsome, to look attractive, to look cool,

82
00:13:40,220 --> 00:13:53,100
to feel good about ourselves, so on, and we use clothes to make us feel happy, we like

83
00:13:53,100 --> 00:14:03,900
buying and wearing or we don't like, we get obsessed with clothes and worried about clothes

84
00:14:03,900 --> 00:14:13,660
or food, food is maybe an even clearer one, mostly we don't, well, a lot of the times

85
00:14:13,660 --> 00:14:22,820
as a people, as we race, human race, we don't use food for the right reasons, right?

86
00:14:22,820 --> 00:14:30,540
We eat too much, we eat the wrong things, and quite clearly this is a problem, food food

87
00:14:30,540 --> 00:14:38,100
is a huge problem, so many ways, it's a problem for our physical health, it's a problem

88
00:14:38,100 --> 00:14:47,780
for our mental health, it's a problem for the environment, for our environmental health,

89
00:14:47,780 --> 00:14:55,540
our use of food is probably one of the biggest problems that we have, all around food

90
00:14:55,540 --> 00:15:04,740
is practically speaking, you know, not looking at deep meditation teachings, practically

91
00:15:04,740 --> 00:15:17,660
speaking food is one if not the worst biggest problem we have, it's a problem for the environment,

92
00:15:17,660 --> 00:15:27,260
we're destroying the environment mainly, maybe not mainly, but one of the biggest ways

93
00:15:27,260 --> 00:15:37,540
in which we're destroying the environment is through food, we, well, consumption, things

94
00:15:37,540 --> 00:15:43,900
we want to consume and we're cutting down the rainforests to grow cows, to raise cattle

95
00:15:43,900 --> 00:15:53,660
and that kind of thing, our misuse of resources, I suppose food isn't the biggest reason

96
00:15:53,660 --> 00:16:22,300
but it is a environmental problem, because we're not satisfied with simple, simple food,

97
00:16:22,300 --> 00:16:26,220
it's a problem physically, because this is one of the biggest problems we have physically

98
00:16:26,220 --> 00:16:33,060
is food, the sicknesses that come from food, overeating and eating the wrong foods, makes

99
00:16:33,060 --> 00:16:41,180
us physically quite sick, but of course, the big concern of the Buddha is mentally and

100
00:16:41,180 --> 00:16:55,700
food is huge, mental, it's a huge problem for us mentally, we obsess over food, we live

101
00:16:55,700 --> 00:17:07,500
to eat, eat to live, don't live to eat, we live to eat and it's a distraction, it's

102
00:17:07,500 --> 00:17:16,620
a way of distracting ourselves from our problems, avoiding, dealing with them, it's a way

103
00:17:16,620 --> 00:17:24,860
of avoiding the questions of life, it's a distraction from what's important, if you just

104
00:17:24,860 --> 00:17:31,940
keep eating, it's another one of our drug habits, makes us feel pleasure, please, makes

105
00:17:31,940 --> 00:17:43,900
us feel all warm and happy inside for a while, prevents us from really dealing with the,

106
00:17:43,900 --> 00:17:50,540
we're dealing with ourselves, dealing with our minds, this is what meditators, what you

107
00:17:50,540 --> 00:17:54,380
can see in the meditation course, if you feel hungry in the evening, you crave for this

108
00:17:54,380 --> 00:17:58,500
kind of food or that kind of food, thinking about what you're going to eat in the morning

109
00:17:58,500 --> 00:18:10,780
that kind of thing, and you can see how much stress and suffering comes from it, how much

110
00:18:10,780 --> 00:18:19,300
of an attachment it is, and how valuable it is really to deal with these things, to cut

111
00:18:19,300 --> 00:18:26,460
back and to learn to use the requisites for what they're meant, and it's like peeling

112
00:18:26,460 --> 00:18:32,300
back the layers and seeing what's underneath, you get to see how your mind works, you don't

113
00:18:32,300 --> 00:18:37,780
get to see that if you're always indulging, if you want to know, and this is why we come

114
00:18:37,780 --> 00:18:42,540
to meditate, if you want to know what's causing you suffering, you have to peel it back

115
00:18:42,540 --> 00:18:51,740
and look, and that requires objectivity, requires you to stop indulging in greed and anger,

116
00:18:51,740 --> 00:19:09,460
liking and disliking, it requires you to look, requires you to, requires you to detox, to

117
00:19:09,460 --> 00:19:16,140
take yourself out of your comfort zone, and that's what you see, right, as you meditate

118
00:19:16,140 --> 00:19:23,420
you, see how your mind works, good in the bad.

119
00:19:23,420 --> 00:19:30,220
With shelter, shelter is one of the big requisites, it's very important, shelter means

120
00:19:30,220 --> 00:19:35,180
many things, the same as the name means, the room that we live in, the building that we

121
00:19:35,180 --> 00:19:43,420
live in, it means the bed that we sleep on, it also means the seat that we sit on, and this

122
00:19:43,420 --> 00:19:50,460
is something that I thought of, here's a good example, the seat, the meditator's cushion,

123
00:19:50,460 --> 00:19:54,020
what is the meditator's cushion for, why do we have a cushion?

124
00:19:54,020 --> 00:20:02,460
It's a good question, right, is there a problem sitting on the bear concrete, what happens

125
00:20:02,460 --> 00:20:09,500
when you sit on bear concrete, well, yeah, that's probably, overly distracting and potentially

126
00:20:09,500 --> 00:20:15,460
harmful to the body, so sitting on a cushion makes sense, but that's the question is

127
00:20:15,460 --> 00:20:25,300
how, how far are we going to go, are we really trying to sit without pain, not that

128
00:20:25,300 --> 00:20:32,380
pain is necessary to meditate obviously, but is the purpose of the seat to free ourselves

129
00:20:32,380 --> 00:20:39,140
from pain, and I think the answer really is no, yes, sitting on bear concrete is not good

130
00:20:39,140 --> 00:20:48,140
for the body, I think, and it's overly painful, but besides that it's not good, so sitting

131
00:20:48,140 --> 00:20:56,060
on something soft is nice, but sitting on the carpet is soft enough, right, these questions

132
00:20:56,060 --> 00:21:01,220
that we ask ourselves, what is really necessary, and the beginning, of course, a meditator

133
00:21:01,220 --> 00:21:05,460
needs a lot of support sitting, a new meditator, someone who's not comfortable sitting

134
00:21:05,460 --> 00:21:12,260
across legged, they need a lot of support, maybe even to sit in a chair or some people,

135
00:21:12,260 --> 00:21:22,380
but they don't need that, and this is part of the challenge of being a meditator is to

136
00:21:22,380 --> 00:21:29,500
be able to discern, differentiate what I need and what I just want and like, yes, I don't

137
00:21:29,500 --> 00:21:37,420
like sitting on the carpet, but I don't need to sit on a comfortable cushion and so on.

138
00:21:37,420 --> 00:21:44,020
A lot of Western meditators make this mistake, and they have all these elaborate ways of

139
00:21:44,020 --> 00:21:49,700
sitting and special sitting cushions and so on, and in the beginning that's all fine,

140
00:21:49,700 --> 00:21:59,180
but the question is, what do you need, and you don't need any of it, so you can look

141
00:21:59,180 --> 00:22:05,940
at it as a goal, food is, you know, food is an example of that, we eat a lot, how much

142
00:22:05,940 --> 00:22:17,900
food do you need, we have very comfortable houses and dwellings, we have nice beds, I always

143
00:22:17,900 --> 00:22:24,340
tell my students one of the first things that goes whenever I move somewhere is the bed,

144
00:22:24,340 --> 00:22:34,660
what a useless instrument, I sleep on the carpet, actually, and if you become accustomed

145
00:22:34,660 --> 00:22:42,100
to it, well your room gets bigger, you save a lot of space in your room really, you

146
00:22:42,100 --> 00:22:48,620
don't need this big bulky thing, you don't need it, it's a challenge in many ways, food,

147
00:22:48,620 --> 00:22:55,060
shelter, clothing, medicine, you know, how quick we are to use medicine, the Buddha included

148
00:22:55,060 --> 00:23:01,580
medicine is one of those things, one of those things that we should reflect upon, why are

149
00:23:01,580 --> 00:23:06,460
we using the medicine, do we need it, there's a lot of medicine we do need to support

150
00:23:06,460 --> 00:23:14,420
our practice, but painkillers may not be one of them, yeah, it may be an extreme pain,

151
00:23:14,420 --> 00:23:20,740
it'll be overly distracting and drive you crazy, but for the most part painkillers are

152
00:23:20,740 --> 00:23:29,100
just something we like, something we want because we don't like the opposite and again

153
00:23:29,100 --> 00:23:35,060
meditation is about taking us out of our comfort zone, so the Buddha had monks reflect

154
00:23:35,060 --> 00:23:40,900
on the requisite daily, they said every day we should reflect and then there's reflecting

155
00:23:40,900 --> 00:23:49,740
before you use it, reflecting after you use them, and so monks nowadays tend to do chanting,

156
00:23:49,740 --> 00:23:55,300
my monastery we do this chanting in the morning and evening and right before we eat,

157
00:23:55,300 --> 00:24:01,300
we chant, which is good, pamasi Sayyada had something to say about that, he said, you

158
00:24:01,300 --> 00:24:10,540
know it's not really the point, it was meant just as an example, the true reflection

159
00:24:10,540 --> 00:24:14,140
isn't chanting, reflection is when you use, as you're using it, when you use the

160
00:24:14,140 --> 00:24:20,020
road, why are you using the road, do you ever think about that?

161
00:24:20,020 --> 00:24:25,460
And so this is a practice, it's not meditation, but it's a practice of sitting back and

162
00:24:25,460 --> 00:24:37,180
saying, hey, why am I using this, I'm using this to stay warm or to stay cool or to

163
00:24:37,180 --> 00:24:44,300
stay not naked, why am I using this food while to stay alive, so I can continue meditating.

164
00:24:44,300 --> 00:24:50,740
Makes me very mindful, it sobs you up and it's quite eye opening, new way of living

165
00:24:50,740 --> 00:24:56,340
our lives, things we take for granted, using them, seeing them in a new light.

166
00:24:56,340 --> 00:25:01,660
But he also says, Mahasi says something that I think is very important and that's, you

167
00:25:01,660 --> 00:25:08,700
don't have to reflect conceptually, that he, I think it's the commentator, commentaries

168
00:25:08,700 --> 00:25:18,460
that say, or maybe even the Buddha, that reflection can be through meditation and so we

169
00:25:18,460 --> 00:25:24,700
do another chant, which is, yata pajayang bwata ma nang dha to mata mai waitang yatitang

170
00:25:24,700 --> 00:25:31,580
ji wurang, which I won't translate, but it's quite in it, it's quite a good

171
00:25:31,580 --> 00:25:38,100
chant that we do, quite in line with meditation.

172
00:25:38,100 --> 00:25:50,980
This, this thing we call the robes, the clothes, the, the, the, the robe itself, and

173
00:25:50,980 --> 00:25:55,540
the person using a robe are just elements.

174
00:25:55,540 --> 00:26:04,780
There's no person who uses the robe, there's no, me, no mind, no self, and he says

175
00:26:04,780 --> 00:26:08,220
that that, that's really what happens when you practice meditation, when you use the

176
00:26:08,220 --> 00:26:13,540
robe mindfully, you put on your clothes mindfully, you are reflecting in a way that's

177
00:26:13,540 --> 00:26:16,700
totally in line with the Buddha's teaching obviously.

178
00:26:16,700 --> 00:26:22,140
You don't need to reflect on using this too, so that I can not be naked and using this

179
00:26:22,140 --> 00:26:31,860
to ward off cold and he, using it mindful mindfully, lifting, placing, feeling.

180
00:26:31,860 --> 00:26:39,460
When you eat your food chewing, chewing, swallowing, it's not seeing them as me, I'm

181
00:26:39,460 --> 00:26:47,780
eating and self and so on, not seeing them as entities as things, but as experiences.

182
00:26:47,780 --> 00:26:53,860
And as you sign up, it points out that, you know, that's a valid reflection.

183
00:26:53,860 --> 00:26:58,180
So in the use of the requisites, it, again, it ultimately comes down to mindfulness.

184
00:26:58,180 --> 00:27:02,900
If you really can be mindful, you don't need any of this, because your reflection

185
00:27:02,900 --> 00:27:12,580
is done for you, you are being mind, you are reflecting wisely as you use the, the

186
00:27:12,580 --> 00:27:18,780
requisites.

187
00:27:18,780 --> 00:27:24,220
Anyway, that's quite a bit about something quite simple and that's remembering things

188
00:27:24,220 --> 00:27:32,580
that we use because they will have an effect positively or negatively on our practice.

189
00:27:32,580 --> 00:27:34,980
Okay.

190
00:27:34,980 --> 00:27:40,580
So that's the demo for tonight.

191
00:27:40,580 --> 00:27:49,460
And we can look at the questions.

192
00:27:49,460 --> 00:27:55,700
When I am sleeping and start dreaming, I am unable to be mindful and dream.

193
00:27:55,700 --> 00:28:03,180
When I start seeing things which anger me, yeah, well, yeah, there's not much you can

194
00:28:03,180 --> 00:28:09,740
do about that except keep meditating and as you meditate more, your dreams will become

195
00:28:09,740 --> 00:28:15,860
more or less, you know, that will be less reaction in your dreams because you're not no

196
00:28:15,860 --> 00:28:19,300
longer as reactionary and your dreams will be less.

197
00:28:19,300 --> 00:28:25,260
Your mind is more efficient, so you'll sleep better.

198
00:28:25,260 --> 00:28:30,140
Is Ditiupadana only occur with meets identity or is it possible for someone to have clinging

199
00:28:30,140 --> 00:28:42,460
towards some meditating?

200
00:28:42,460 --> 00:28:53,260
You know, no, it's meets identity, Ditiupadana.

201
00:28:53,260 --> 00:29:00,500
Now, I guess you're right, I guess that's a good point.

202
00:29:00,500 --> 00:29:01,500
Probably can.

203
00:29:01,500 --> 00:29:02,500
I mean, I don't know.

204
00:29:02,500 --> 00:29:07,340
These questions you ask, are they really important?

205
00:29:07,340 --> 00:29:11,300
The possible of clinging towards, okay, so is it possible to have clinging towards some

206
00:29:11,300 --> 00:29:12,300
meditating?

207
00:29:12,300 --> 00:29:13,300
Yes, of course.

208
00:29:13,300 --> 00:29:22,620
You can cling to right view, you can be proud of it, you can, you can cling to it without

209
00:29:22,620 --> 00:29:24,420
knowing it, right?

210
00:29:24,420 --> 00:29:32,460
A person who doesn't know the truth, but here's it, can say, yes, yes, there is rebirth

211
00:29:32,460 --> 00:29:38,180
and they can go around proclaiming it.

212
00:29:38,180 --> 00:29:43,220
They can wish for it to be true, boy, I hope I'm reborn, I hope there is enough to life,

213
00:29:43,220 --> 00:29:50,420
hope I don't die, I will not annihilate it when I die, that's why I kind of think.

214
00:29:50,420 --> 00:29:56,380
Could you please explain the difference between Jita and Satipanya, or Sati and Panya are

215
00:29:56,380 --> 00:30:04,900
to Jita-Sika, their mind and their mental concomitants, their aspects of mind.

216
00:30:04,900 --> 00:30:06,660
Jita means mind.

217
00:30:06,660 --> 00:30:14,980
So Jita contains qualities and Sati as a quality, and Panya as a quality in some mind.

218
00:30:14,980 --> 00:30:26,100
If consciously send mind to a single breath, single, would it still have to strengthen the

219
00:30:26,100 --> 00:30:32,340
faculty of Sati, or is it long hours, or only count?

220
00:30:32,340 --> 00:30:38,780
No, every moment counts, but it has to be habitual, why long hours are good is because

221
00:30:38,780 --> 00:30:43,140
it becomes habitual as you do it again and again, but say the thing is, it's not about

222
00:30:43,140 --> 00:30:50,140
the hours, it's about how many times you're consciously sending the mind to the object,

223
00:30:50,140 --> 00:30:55,820
not just sending the mind to the object, but seeing the object clearly.

224
00:30:55,820 --> 00:30:59,780
As you can do that better, it becomes habitual.

225
00:30:59,780 --> 00:31:08,100
So yes, you absolutely can and should strengthen Sati through daily practice, not just

226
00:31:08,100 --> 00:31:10,020
in formal practice.

227
00:31:10,020 --> 00:31:14,060
But if you're not doing formal practice, it's quite difficult because you're developing

228
00:31:14,060 --> 00:31:23,060
all sorts of other habits and formal meditation forces you to focus on good habits.

229
00:31:23,060 --> 00:31:25,660
Can the foundation course be taken more than once?

230
00:31:25,660 --> 00:31:27,260
Yeah, potentially.

231
00:31:27,260 --> 00:31:32,340
We like for people who have taken the foundation course to move on to the review course,

232
00:31:32,340 --> 00:31:40,900
which is a review of the foundation course, but it's a little more than that.

233
00:31:40,900 --> 00:31:45,300
If you've successfully and done well in the foundation course, there should be no reason

234
00:31:45,300 --> 00:31:51,020
to retake and you should just do the streamlined advanced course.

235
00:31:51,020 --> 00:31:55,940
It's facing our fears a good thing to do for the practice.

236
00:31:55,940 --> 00:31:58,300
Could it make you more mindful?

237
00:31:58,300 --> 00:32:03,300
Well, you should face your fears mindfully.

238
00:32:03,300 --> 00:32:05,100
Yeah, I think facing your fears.

239
00:32:05,100 --> 00:32:14,460
I mean, part mindfulness, one aspect of mindfulness is we say abimukha, abimukha, which means

240
00:32:14,460 --> 00:32:20,140
facing your experiences.

241
00:32:20,140 --> 00:32:21,780
So it's an aspect of mindfulness.

242
00:32:21,780 --> 00:32:26,140
If you're not being mindful, but you're just like putting yourself in the presence of

243
00:32:26,140 --> 00:32:31,300
things to make you afraid, I guess it's, it's debatable, right?

244
00:32:31,300 --> 00:32:35,660
Because if you do that, you might just be reinforcing the fear.

245
00:32:35,660 --> 00:32:40,580
I don't know, I don't think that's incredibly valid, but I mean, the thing is if you're

246
00:32:40,580 --> 00:32:44,100
not mindful, there's not much I can say, it's not good anyway.

247
00:32:44,100 --> 00:32:50,220
So you're just developing the habit of putting yourself in situations that trigger reactions.

248
00:32:50,220 --> 00:32:52,860
If you're not being mindful.

249
00:32:52,860 --> 00:32:59,660
So yes, okay, in an abstract sense, that can help you, potentially, but if you're not

250
00:32:59,660 --> 00:33:07,660
being mindful, it's not going to get that far, but absolutely, if you're being mindful,

251
00:33:07,660 --> 00:33:15,500
put yourself in, put yourself in, you know, okay, here's the problem with that.

252
00:33:15,500 --> 00:33:21,060
If you're consciously putting yourself in the presence of things that cause reactions,

253
00:33:21,060 --> 00:33:26,740
then you're, you're artificially creating, you're creating this habit of artificial,

254
00:33:26,740 --> 00:33:32,980
artificially, uh, contriving situations, right?

255
00:33:32,980 --> 00:33:36,140
And that in general is not such a good thing.

256
00:33:36,140 --> 00:33:42,100
So we don't, we don't encourage facing in the sense of, hey, I'll go and find a spider

257
00:33:42,100 --> 00:33:48,260
if I'm afraid of spiders, it's not really necessary, it's not a very good habit because

258
00:33:48,260 --> 00:33:56,780
it becomes habitual, it, it, it taints your mindfulness, the, the, um, sort of objective

259
00:33:56,780 --> 00:34:00,900
let things let the chips fall as they may, right?

260
00:34:00,900 --> 00:34:05,900
If you start to contrive, then you're kind of trying to control, you see.

261
00:34:05,900 --> 00:34:12,020
So going on to be a way to find a spider is, it's not a big problem, but in general, that's

262
00:34:12,020 --> 00:34:20,740
not the best attitude to have. So no, not really facing your fears. In the sense of going

263
00:34:20,740 --> 00:34:25,500
out of your way to find them, yes, facing your fears is, is incredibly great, that's mindfulness,

264
00:34:25,500 --> 00:34:31,780
but going out of your way to find the things that make you afraid, probably not a good

265
00:34:31,780 --> 00:34:39,580
idea in the long term. Even if you are being mindful, what's the best way to practice

266
00:34:39,580 --> 00:34:47,740
self-love? It's interesting, you know, the commentaries are not very keen on self-love.

267
00:34:47,740 --> 00:34:57,180
And I think with good reason because it becomes egotistical or it can feed the ego. So

268
00:34:57,180 --> 00:35:02,100
the problem is getting rid of self-hatred, you don't need self-love to get rid of self-hatred,

269
00:35:02,100 --> 00:35:05,660
that's an important point. People think, you know, I don't like myself, I got to learn

270
00:35:05,660 --> 00:35:11,300
to love myself. Well, no, not really. You don't like yourself, you've got to learn to

271
00:35:11,300 --> 00:35:19,660
let go of the disliking. Because if you love yourself, it feeds ego in a way that loving

272
00:35:19,660 --> 00:35:27,260
other people doesn't think. I'm trying to understand. The commentary says it's not

273
00:35:27,260 --> 00:35:32,220
really fruitful to love yourself in the same way as it's fruitful to love someone else.

274
00:35:32,220 --> 00:35:39,220
And it makes sense because loving other people is about altruism, it's about letting go

275
00:35:39,220 --> 00:35:50,260
of self, right? It's about getting a healthy state of mind in terms of relationships

276
00:35:50,260 --> 00:36:00,700
and it's about expanding our minds. But self-love, I don't know, there's a lot of people

277
00:36:00,700 --> 00:36:07,180
talking positively about it and I'd caution being too concerned with self-love. Guilt

278
00:36:07,180 --> 00:36:16,660
and self-hatred, you know, work them out. I don't think, I don't think you have to worry

279
00:36:16,660 --> 00:36:23,140
too much about it. Commentaries certainly don't seem to think so. I would say we put too

280
00:36:23,140 --> 00:36:28,980
much emphasis on self-love when it's not really the point of love and made the point

281
00:36:28,980 --> 00:36:35,980
of made days to relate to other beings. You know, worry about compassion and love. I know

282
00:36:35,980 --> 00:36:40,660
there's a lot of Buddhist teachers who are very clear that you should, but commentaries

283
00:36:40,660 --> 00:36:46,300
and, well, I mean, I don't subscribe too much to the whole self-love, self-compassion

284
00:36:46,300 --> 00:36:55,060
thing. Which is probably going to get me some down votes and comments, but okay, I'm

285
00:36:55,060 --> 00:37:06,580
happy to have debate. Okay, wait, there's more to this. I would like to be a source of joy

286
00:37:06,580 --> 00:37:14,020
and goodness to others. Most of the time, I'm a grumpy, closed-off man. Right, well, that's

287
00:37:14,020 --> 00:37:19,340
what love and compassion to others is. You don't need self-love for that. If you're

288
00:37:19,340 --> 00:37:24,540
a grump and a closed-off man, I mean, someone being a grump and a closed-off man is not

289
00:37:24,540 --> 00:37:31,740
bad. Some of it is based on wisdom and minding your own business, right? Some people

290
00:37:31,740 --> 00:37:37,300
are too much in other people's business. And that's a big problem in the world. It's

291
00:37:37,300 --> 00:37:42,780
that we're too, I don't know. I found it a big problem in Asia. In some countries in Asia

292
00:37:42,780 --> 00:37:46,940
where everyone is so concerned about everyone else and always in your face and in your

293
00:37:46,940 --> 00:37:53,460
business. I don't think that's that healthy. I don't think that's a part of the thing

294
00:37:53,460 --> 00:38:00,420
that's a part of culture, Asian culture. It's one that I was never comfortable with.

295
00:38:00,420 --> 00:38:07,780
Having said that, I mean, there's a lot that is love and compassion and consideration

296
00:38:07,780 --> 00:38:14,580
for others. But there is a part of it that becomes very obsessive, right? Mothers and parents.

297
00:38:14,580 --> 00:38:20,220
Mothers, no, not just mothers. Parents can become incredibly obsessive about their children.

298
00:38:20,220 --> 00:38:27,140
So being closed-off and even grumpy to some extent, it's not a bad thing. Does some extent

299
00:38:27,140 --> 00:38:33,260
it's not a bad thing? You just have to look and see am I angry or are people just expecting

300
00:38:33,260 --> 00:38:39,500
things of me? Which is their problem, really? Expectations are a cause for suffering, not

301
00:38:39,500 --> 00:38:44,860
people who don't live up to your expectations. The people who don't live up to your expectations

302
00:38:44,860 --> 00:38:53,460
aren't the problem. But to be a source, you want to be a source of joy. Okay, so there's

303
00:38:53,460 --> 00:38:59,580
a stress and there's a certain attachment. Don't do that. Don't want to be a source of

304
00:38:59,580 --> 00:39:09,780
joy and goodness to others. But having an understanding of how goodness leads to happiness

305
00:39:09,780 --> 00:39:15,740
and how happiness is a good thing is a good thing. So that being said, if you're concerned

306
00:39:15,740 --> 00:39:19,740
that you have lots of defilements that are causing other people's stress and suffering,

307
00:39:19,740 --> 00:39:24,380
well, there are different ways to deal with that. Of course, mindfulness is the best way.

308
00:39:24,380 --> 00:39:30,340
But maita and karuna compassion are two very good ways as well. But you have them for

309
00:39:30,340 --> 00:39:38,580
others. Just wanting, in fact, just wanting to be a source of happiness to others is a

310
00:39:38,580 --> 00:39:47,420
kind of a maita. So cultivate that. At the same time, cultivate mindfulness. That would

311
00:39:47,420 --> 00:40:01,780
be great support for your maita practice. Alright, so we've gone to all of them. That's

312
00:40:01,780 --> 00:40:08,780
the questions for tonight. Thank you all for tuning in. Have a good night.

