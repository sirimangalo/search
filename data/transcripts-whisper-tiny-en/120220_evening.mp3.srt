1
00:00:00,000 --> 00:00:10,000
So, I'm going to make this a little more formal, look.

2
00:00:10,000 --> 00:00:20,000
This book is a part of the tepitika, and the tepitika is what you see behind me here in the middle

3
00:00:20,000 --> 00:00:27,000
and on the left side, different versions.

4
00:00:27,000 --> 00:00:30,000
This is the tepitika and the common areas and some common areas.

5
00:00:30,000 --> 00:00:32,000
What that means is the teaching of the Buddha.

6
00:00:32,000 --> 00:00:37,000
You know, in brief, this is the teaching of the Buddha.

7
00:00:37,000 --> 00:00:42,000
So, it may seem odd that in a meditation course, I should pull out a book because

8
00:00:42,000 --> 00:00:45,000
it seems like maybe we're going to study.

9
00:00:45,000 --> 00:00:48,000
And if you study, then you have to think.

10
00:00:48,000 --> 00:00:53,000
And thinking can get in the way of meditating.

11
00:00:53,000 --> 00:00:56,000
But what you'll see, I'm going to try to go through actually here.

12
00:00:56,000 --> 00:00:58,000
I think this is a good idea.

13
00:00:58,000 --> 00:01:02,000
We picked up the Angutturani Kai.

14
00:01:02,000 --> 00:01:08,000
Angutturani Kai is a book of teachings about lists of things.

15
00:01:08,000 --> 00:01:12,000
And so, there's not much thinking involved.

16
00:01:12,000 --> 00:01:16,000
And what you should see is that actually, for the most part,

17
00:01:16,000 --> 00:01:24,000
it's in direct meditation instruction and vice and exhortation.

18
00:01:24,000 --> 00:01:29,000
As listening to the Dhamma is not for the purpose of giving you wisdom.

19
00:01:29,000 --> 00:01:35,000
It's for the purpose of giving you encouragement and giving you the map

20
00:01:35,000 --> 00:01:39,000
by which you can, like, a treasure map.

21
00:01:39,000 --> 00:01:44,000
You can take it to find the goal, which is wisdom.

22
00:01:44,000 --> 00:01:55,000
As the Buddha said, I'll cut that over the tag.

23
00:01:55,000 --> 00:01:58,000
The Buddha's are only ones who show the way.

24
00:01:58,000 --> 00:02:01,000
We only point the way.

25
00:02:01,000 --> 00:02:04,000
We have to walk ourselves.

26
00:02:04,000 --> 00:02:06,000
So that's what this is going to be.

27
00:02:06,000 --> 00:02:11,000
It's a way of setting us in the right direction.

28
00:02:11,000 --> 00:02:17,000
Because you start to learn as you stay in the meditation center longer,

29
00:02:17,000 --> 00:02:19,000
because we're free from all the diversions.

30
00:02:19,000 --> 00:02:22,000
So you start to see how the mind plays tricks on you.

31
00:02:22,000 --> 00:02:27,000
It starts to build up defilements, attachment.

32
00:02:27,000 --> 00:02:31,000
It starts to build up a version of devusions.

33
00:02:31,000 --> 00:02:40,000
Very easy to become obsessed and overwhelmed by the emotions in the mind.

34
00:02:40,000 --> 00:02:43,000
This is why I said about Mara.

35
00:02:43,000 --> 00:02:45,000
Mara tries to trick you.

36
00:02:45,000 --> 00:02:50,000
I think Rachelle had quite a bit of Mara at this course.

37
00:02:50,000 --> 00:02:55,000
Sometimes it tricked her.

38
00:02:55,000 --> 00:02:58,000
Sometimes she managed to trick it.

39
00:02:58,000 --> 00:03:03,000
But I pointed out because this is really the goal of the practice.

40
00:03:03,000 --> 00:03:05,000
This is great to hear.

41
00:03:05,000 --> 00:03:07,000
It's a great thing for us to hear.

42
00:03:07,000 --> 00:03:09,000
I'm not worried about you.

43
00:03:09,000 --> 00:03:14,000
Because we hear this is the fight that the Buddha made.

44
00:03:14,000 --> 00:03:18,000
I've ever seen those movies where the Buddha fought Mara.

45
00:03:18,000 --> 00:03:21,000
Now you understand all kind of what it means.

46
00:03:21,000 --> 00:03:25,000
Fight Mara, fight Mara, what does it mean?

47
00:03:25,000 --> 00:03:32,000
And say, oh, these things I have inside.

48
00:03:32,000 --> 00:03:33,000
So we'll go through here.

49
00:03:33,000 --> 00:03:36,000
I'm just going to pick up the things that I think are most useful.

50
00:03:36,000 --> 00:03:38,000
Most pertinent.

51
00:03:38,000 --> 00:03:40,000
This is the list of ones.

52
00:03:40,000 --> 00:03:41,000
So these lists are short.

53
00:03:41,000 --> 00:03:44,000
Each of them has one entry.

54
00:03:44,000 --> 00:03:47,000
You see to remember that.

55
00:03:47,000 --> 00:03:50,000
So here the Buddha says, nahang pick away,

56
00:03:50,000 --> 00:03:54,000
Anyang aikadamamti.

57
00:03:54,000 --> 00:03:58,000
Oh, because the body is just the word be cool.

58
00:03:58,000 --> 00:04:01,000
Because be cool means a monk.

59
00:04:01,000 --> 00:04:03,000
Or a beggar even.

60
00:04:03,000 --> 00:04:11,000
But it's the word the Buddha used to mean to refer to the monk.

61
00:04:11,000 --> 00:04:18,000
And that's because in the audience there would be monks.

62
00:04:18,000 --> 00:04:20,000
And then there would be lay people.

63
00:04:20,000 --> 00:04:22,000
But he put the monks at the top of it.

64
00:04:22,000 --> 00:04:25,000
It's like what he means is every all of you.

65
00:04:25,000 --> 00:04:28,000
Monks and everyone else.

66
00:04:28,000 --> 00:04:36,000
But it also has some connotation in regards to the meaning of the word be cool.

67
00:04:36,000 --> 00:04:40,000
But it wasn't always just talking about monks.

68
00:04:40,000 --> 00:04:44,000
Because be cool can mean many different things.

69
00:04:44,000 --> 00:04:46,000
A monk is someone who stays alone.

70
00:04:46,000 --> 00:04:50,000
So the word monk in English doesn't have to mean someone who has robes.

71
00:04:50,000 --> 00:04:52,000
It could just mean someone who stays by themselves.

72
00:04:52,000 --> 00:04:59,000
The monk comes from mono to stay alone.

73
00:04:59,000 --> 00:05:02,000
Because they say, be cool can mean a beggar.

74
00:05:02,000 --> 00:05:04,000
One who begets for food.

75
00:05:04,000 --> 00:05:06,000
The same as the English word beg is beak.

76
00:05:06,000 --> 00:05:11,000
It's the same language.

77
00:05:11,000 --> 00:05:14,000
But he can also mean buy young aikatit.

78
00:05:14,000 --> 00:05:16,000
So they say, buy an aikatit.

79
00:05:16,000 --> 00:05:18,000
See, it's the fierceness.

80
00:05:18,000 --> 00:05:21,000
You see how fierceness in the mind can be.

81
00:05:21,000 --> 00:05:24,000
So he says, this is because this is important.

82
00:05:24,000 --> 00:05:27,000
He's going to talk about the mind.

83
00:05:27,000 --> 00:05:32,000
He says, do you all see how fierceness in the mind can be?

84
00:05:32,000 --> 00:05:36,000
You who have the potential to see how fierceness in the mind can be.

85
00:05:36,000 --> 00:05:41,000
He says, I tell you, nah, nah, nah, nah, nah, nah, nah, nah, nah, nah, nah, nah, nah, nah, nah, nah, nah, nah, nah, nah, nah, nah, nah.

86
00:05:41,000 --> 00:05:51,000
I don't see any single dhamma, single thing.

87
00:05:51,000 --> 00:05:55,000
Dhamma here just means something that really exists.

88
00:05:55,000 --> 00:06:01,000
It's a part of reality.

89
00:06:01,000 --> 00:06:04,000
I don't see any one thing.

90
00:06:04,000 --> 00:06:11,000
A young, a young, a bhavitan, a kamini, a hoti.

91
00:06:11,000 --> 00:06:20,000
That is, when you don't train it, is useless.

92
00:06:20,000 --> 00:06:24,000
A kamini can be used for anything.

93
00:06:24,000 --> 00:06:28,000
You can't do anything with it.

94
00:06:28,000 --> 00:06:40,000
When untrained, I don't know of anything that when untrained is as useless as the untrained mind,

95
00:06:40,000 --> 00:06:46,000
are undeveloped, a bhavitan is undeveloped.

96
00:06:46,000 --> 00:06:52,000
So we think if you don't develop your body, or if you don't develop your reflexes,

97
00:06:52,000 --> 00:07:02,000
you don't train yourself in this way or that way, let's use this, let's put this in.

98
00:07:02,000 --> 00:07:06,000
You have to eat a kamini.

99
00:07:06,000 --> 00:07:09,000
You have to eat a kamini.

100
00:07:09,000 --> 00:07:12,000
You have to eat a kamini.

101
00:07:12,000 --> 00:07:15,000
You have to eat a kamini.

102
00:07:15,000 --> 00:07:26,000
And that is undeveloped, is useless.

103
00:07:26,000 --> 00:07:28,000
So this is our first list.

104
00:07:28,000 --> 00:07:30,000
How many things in it?

105
00:07:30,000 --> 00:07:31,000
One.

106
00:07:31,000 --> 00:07:34,000
The mind.

107
00:07:34,000 --> 00:07:38,000
Then the Buddha goes on to say, I don't know of any one thing.

108
00:07:38,000 --> 00:07:40,000
This is the curious thing.

109
00:07:40,000 --> 00:07:41,000
Next list.

110
00:07:41,000 --> 00:07:55,000
You don't know any one thing that when developed is so useful as the mind.

111
00:07:55,000 --> 00:08:02,000
The developed mind, I say, is useful.

112
00:08:02,000 --> 00:08:15,000
The things that are useful and developed mind is useful.

113
00:08:15,000 --> 00:08:20,000
So this is very simple teaching.

114
00:08:20,000 --> 00:08:26,000
But it's the basis of our training here.

115
00:08:26,000 --> 00:08:31,000
The mind, the mind is that which knows an object, that part of our experience right here.

116
00:08:31,000 --> 00:08:37,000
And now that is aware of the sound of my voice.

117
00:08:37,000 --> 00:08:41,000
It's what lets you see, what lets you hear.

118
00:08:41,000 --> 00:08:44,000
Without the mind, you'll be like a dead person.

119
00:08:44,000 --> 00:08:49,000
A dead person has eyes and ears and nose and tongue and body.

120
00:08:49,000 --> 00:08:58,000
But they don't see ears and mouth taste, because they have no mind.

121
00:08:58,000 --> 00:09:05,000
But even people who have a mind would have an undeveloped mind.

122
00:09:05,000 --> 00:09:08,000
What's it like when your mind is undeveloped?

123
00:09:08,000 --> 00:09:18,000
When you live at home, when you work in the world, when you deal with other people.

124
00:09:18,000 --> 00:09:27,000
When you interact with society, we spend a lot of our lives training ourselves.

125
00:09:27,000 --> 00:09:31,000
Training our minds, whipping our minds into shape, you might say,

126
00:09:31,000 --> 00:09:36,000
forcing our minds to be in this shape or that shape.

127
00:09:36,000 --> 00:09:40,000
So we've really trained our minds even before we come here.

128
00:09:40,000 --> 00:09:53,000
We've trained our minds to fit into our idea of who we are.

129
00:09:53,000 --> 00:10:00,000
You know how sometimes when you look in the mirror after a long time, you don't recognize yourself.

130
00:10:00,000 --> 00:10:02,000
Because you don't realize who you really are.

131
00:10:02,000 --> 00:10:05,000
You don't realize what you really look like.

132
00:10:05,000 --> 00:10:10,000
You don't have to realize it's that really neat.

133
00:10:10,000 --> 00:10:16,000
So what we do with our mind is we build it up into this idea of who we are.

134
00:10:16,000 --> 00:10:21,000
I'm this person and everyone else is looking at us like, wow, that person is all stressed out.

135
00:10:21,000 --> 00:10:27,000
And Mr. Obama, we think, oh, everyone else thinks I'm a wonderful person and so on.

136
00:10:27,000 --> 00:10:32,000
Remember we can trick some people as well.

137
00:10:32,000 --> 00:10:38,000
But we train our mind by clinging, by forcing.

138
00:10:38,000 --> 00:10:44,000
We're trying to make our minds fit into the idea of who we should be.

139
00:10:44,000 --> 00:10:47,000
I'm not that kind of person, I don't.

140
00:10:47,000 --> 00:10:52,000
I don't accept this part of myself.

141
00:10:52,000 --> 00:11:01,000
But then when we come here, it's like we're looking in the mirror, open it up and it's like, oh, that's who I really am.

142
00:11:01,000 --> 00:11:06,000
I'm down underneath this and you realize you're just pretending.

143
00:11:06,000 --> 00:11:14,000
You realize that all the work you've done to train yourself isn't really, hasn't really brought you anywhere.

144
00:11:14,000 --> 00:11:17,000
You haven't met with much success.

145
00:11:17,000 --> 00:11:23,000
It's just a facade, it's just what you showed other people.

146
00:11:23,000 --> 00:11:34,000
So what the Buddha means by training here isn't the training that we do in the world, isn't the forcing the mind.

147
00:11:34,000 --> 00:11:36,000
To be this way or that way.

148
00:11:36,000 --> 00:11:40,000
But even in that sense, you can see how it becomes useful.

149
00:11:40,000 --> 00:11:51,000
If you're working in business, it's really admirable sometimes to see in business people know how clever they can be.

150
00:11:51,000 --> 00:11:53,000
How efficient they can be.

151
00:11:53,000 --> 00:11:56,000
How they've trained their minds in a certain way.

152
00:11:56,000 --> 00:12:02,000
Sometimes they can be very, very brutally efficient.

153
00:12:02,000 --> 00:12:12,000
That point where they cause great suffering for other beings, but they have great efficiency because their mind works like a computer.

154
00:12:12,000 --> 00:12:14,000
People who use computers as well.

155
00:12:14,000 --> 00:12:18,000
Computer programmers, when you learn to program the computer, you can.

156
00:12:18,000 --> 00:12:20,000
It's amazing what you can do.

157
00:12:20,000 --> 00:12:22,000
You can.

158
00:12:22,000 --> 00:12:26,000
The mind is incredible, even without meditation.

159
00:12:26,000 --> 00:12:29,000
You can train your mind.

160
00:12:29,000 --> 00:12:34,000
In Burma, there are monks who can remember the whole of the typical.

161
00:12:34,000 --> 00:12:38,000
They can remember it in their mind.

162
00:12:38,000 --> 00:12:42,000
I tried learning just very, very bits and pieces.

163
00:12:42,000 --> 00:12:43,000
I learned one.

164
00:12:43,000 --> 00:12:45,000
Started to learn one sort of stuff.

165
00:12:45,000 --> 00:12:47,000
It's amazing as you go through it.

166
00:12:47,000 --> 00:12:52,000
This one monkey looked at me and he said, where do you keep it?

167
00:12:52,000 --> 00:12:55,000
Where do you keep all that?

168
00:12:55,000 --> 00:12:59,000
He was actually a meditation teacher.

169
00:12:59,000 --> 00:13:01,000
He wasn't just a simple monk.

170
00:13:01,000 --> 00:13:07,000
But he found it very interesting that the mind can keep it because the mind isn't keeping.

171
00:13:07,000 --> 00:13:10,000
There's no room in the brain for all that.

172
00:13:10,000 --> 00:13:14,000
But the mind has the power to connect and make connections.

173
00:13:14,000 --> 00:13:21,000
Maybe there is room in the brain, but the brain has to store it in a certain way.

174
00:13:21,000 --> 00:13:30,000
And so the mind changes the layout of the brain in such a way that it can remember this thing.

175
00:13:30,000 --> 00:13:32,000
And it can be a great use.

176
00:13:32,000 --> 00:13:34,000
It can be of use in business.

177
00:13:34,000 --> 00:13:37,000
It can be of use in society.

178
00:13:37,000 --> 00:13:40,000
When you learn to teach, for example, I used to give talks.

179
00:13:40,000 --> 00:13:49,000
And when I gave talks, five years ago or six years ago, I think I deleted the mom that I used to have them on my website.

180
00:13:49,000 --> 00:13:55,000
I deleted them because they're quite dry.

181
00:13:55,000 --> 00:14:00,000
And now some people sometimes I give a talk and people say, oh, it's a good talk.

182
00:14:00,000 --> 00:14:03,000
This is the training of the mind.

183
00:14:03,000 --> 00:14:05,000
It's not bragging.

184
00:14:05,000 --> 00:14:15,000
After years and years of doing something, your mind becomes trained.

185
00:14:15,000 --> 00:14:27,000
One of the special qualities of what we're doing here is that it's reaching a very root of the mind,

186
00:14:27,000 --> 00:14:30,000
a very root of our experience.

187
00:14:30,000 --> 00:14:39,000
What I mean is that when you train in business or when you train in society or giving talks or so,

188
00:14:39,000 --> 00:14:49,000
you have to use the building blocks of your experience to do that, to undertake that act.

189
00:14:49,000 --> 00:14:53,000
For instance, when you're giving a talk, there's many things involved with it.

190
00:14:53,000 --> 00:14:56,000
When you're up on stage giving a talk that's not easy.

191
00:14:56,000 --> 00:15:00,000
I'm giving talks in front of very large crowds because they always want the Western,

192
00:15:00,000 --> 00:15:02,000
oh, look, you can speak Thai for instance.

193
00:15:02,000 --> 00:15:03,000
Let him give a talk.

194
00:15:03,000 --> 00:15:05,000
I once gave a talk.

195
00:15:05,000 --> 00:15:08,000
I've got pictures of it in front of the whole city.

196
00:15:08,000 --> 00:15:11,000
I've changed my time.

197
00:15:11,000 --> 00:15:13,000
Not the whole city, not many, many Buddhists.

198
00:15:13,000 --> 00:15:16,000
It was a big celebration.

199
00:15:16,000 --> 00:15:23,000
First, the two big monks gave talks and then I was the third one at midnight and everyone was asleep.

200
00:15:23,000 --> 00:15:25,000
When I got to get in, there's a big throne.

201
00:15:25,000 --> 00:15:32,000
They put me up on this dhamma throne and wasn't a very good talk because I was nervous.

202
00:15:32,000 --> 00:15:35,000
And I didn't speak Thai very well.

203
00:15:35,000 --> 00:15:39,000
But you have to deal with it when you do with the nervousness.

204
00:15:39,000 --> 00:15:45,000
And then you forget something and you feel embarrassed and you lose your train of thought or you save something.

205
00:15:45,000 --> 00:15:50,000
And it's the wrong thing where it sounds dhamma.

206
00:15:50,000 --> 00:15:56,000
And then the audience starts fidgeting and you think, oh, are they really listening to me?

207
00:15:56,000 --> 00:16:02,000
And there's all of this going on in your mind and remembering things in order.

208
00:16:02,000 --> 00:16:04,000
And see what happens.

209
00:16:04,000 --> 00:16:07,000
The biggest thing is that you do get distracted.

210
00:16:07,000 --> 00:16:15,000
You get distracted by these emotions, worry and maybe you get angry or frustrated.

211
00:16:15,000 --> 00:16:20,000
And so maybe you get off track and you start spouting nonsense.

212
00:16:20,000 --> 00:16:25,000
Can be based on delusion, can be based on your own laziness, the greed in the mind,

213
00:16:25,000 --> 00:16:30,000
just like having fun and goofing off and laughing and so on.

214
00:16:30,000 --> 00:16:32,000
And then you lose your train of thought.

215
00:16:32,000 --> 00:16:35,000
So the skills are all there.

216
00:16:35,000 --> 00:16:38,000
Meditation is working on those skills.

217
00:16:38,000 --> 00:16:43,000
It's working on the basis of the base of the mind.

218
00:16:43,000 --> 00:16:50,000
This is why the Buddha said, once the mind is trained, it's like a pure piece of cloth.

219
00:16:50,000 --> 00:16:57,000
Maybe you don't want a pure piece of white cloth, but the choice, the choice when you're using cloth

220
00:16:57,000 --> 00:17:02,000
is either start with a pure piece or start with a dirty piece.

221
00:17:02,000 --> 00:17:05,000
If you start with a dirty piece, you can make anything.

222
00:17:05,000 --> 00:17:10,000
You put it in red dye, yellow dye, blue dye, make a shirt, make a vest.

223
00:17:10,000 --> 00:17:13,000
Looks ugly no matter what.

224
00:17:13,000 --> 00:17:18,000
And the dye doesn't take sewing it as useless.

225
00:17:18,000 --> 00:17:21,000
But when you have a pure piece of cloth, you can put it in any color.

226
00:17:21,000 --> 00:17:23,000
Doesn't matter what color.

227
00:17:23,000 --> 00:17:26,000
You can make a dress, you can make a shirt, you can make a flag, whatever you want to make.

228
00:17:26,000 --> 00:17:29,000
You have to start with a pure piece of cloth.

229
00:17:29,000 --> 00:17:35,000
This is why he said it just because what we're doing is at the very basic level.

230
00:17:35,000 --> 00:17:41,000
And you don't have to be a monk to appreciate the benefits of meditation, the benefits of the Buddha's teaching.

231
00:17:41,000 --> 00:17:44,000
Time of the Buddha even the kings.

232
00:17:44,000 --> 00:17:46,000
His kings wouldn't hurt.

233
00:17:46,000 --> 00:17:50,000
Appreciate the benefit.

234
00:17:50,000 --> 00:17:58,000
He changed his whole way of being after the Buddha.

235
00:17:58,000 --> 00:18:03,000
The point where his son was under the influence of an evil monk,

236
00:18:03,000 --> 00:18:06,000
and his son asked him to give me the kingdom.

237
00:18:06,000 --> 00:18:07,000
No.

238
00:18:07,000 --> 00:18:10,000
His son was plotting to kill him, that sort of.

239
00:18:10,000 --> 00:18:12,000
Plotting to kill him to take him to the kingdom.

240
00:18:12,000 --> 00:18:14,000
He said, why does he want to kill me?

241
00:18:14,000 --> 00:18:17,000
He wants to kill you because he wants your kingdom.

242
00:18:17,000 --> 00:18:20,000
Well, give it to him.

243
00:18:20,000 --> 00:18:25,000
I said, give me a kingdom.

244
00:18:25,000 --> 00:18:26,000
Why?

245
00:18:26,000 --> 00:18:28,000
Because he had become a sort of panda.

246
00:18:28,000 --> 00:18:32,000
He understood the Buddha's teaching for himself.

247
00:18:32,000 --> 00:18:34,000
He understood that nothing is worth going into.

248
00:18:34,000 --> 00:18:38,000
His mind was trained at that.

249
00:18:38,000 --> 00:18:40,000
And so he was able to deal with anything.

250
00:18:40,000 --> 00:18:43,000
Even his own son wanted to kill him.

251
00:18:43,000 --> 00:18:49,000
This is what commony means.

252
00:18:49,000 --> 00:18:52,000
You can use it for anything.

253
00:18:52,000 --> 00:18:57,000
Because life is not easy, no?

254
00:18:57,000 --> 00:19:00,000
There's many difficult things we have to do.

255
00:19:00,000 --> 00:19:01,000
Studying is a good example.

256
00:19:01,000 --> 00:19:03,000
I'll give you a good anecdote.

257
00:19:03,000 --> 00:19:05,000
You don't believe me.

258
00:19:05,000 --> 00:19:08,000
I'm sure you all believe me in practicing.

259
00:19:08,000 --> 00:19:10,000
I don't mean my word on it.

260
00:19:10,000 --> 00:19:13,000
When I went back, after I finished my first meditation course,

261
00:19:13,000 --> 00:19:17,000
I went back and changed my whole life.

262
00:19:17,000 --> 00:19:22,000
I gave up all my music collections, my guitar,

263
00:19:22,000 --> 00:19:26,000
all my sports equipment.

264
00:19:26,000 --> 00:19:29,000
I went to live in a monastery.

265
00:19:29,000 --> 00:19:31,000
This little monastery.

266
00:19:31,000 --> 00:19:34,000
And I went to university.

267
00:19:34,000 --> 00:19:38,000
That year in university, I took 11 courses.

268
00:19:38,000 --> 00:19:45,000
At the end of the year, nine A plus is 1 A and an A minus.

269
00:19:45,000 --> 00:19:49,000
And the A minus is because the teacher didn't like Buddhism.

270
00:19:49,000 --> 00:19:53,000
We didn't get along very well.

271
00:19:53,000 --> 00:19:55,000
That's an example.

272
00:19:55,000 --> 00:19:58,000
Because every day, meditation, study and meditation,

273
00:19:58,000 --> 00:19:59,000
I learned how to study.

274
00:19:59,000 --> 00:20:01,000
I re-talked myself a distance.

275
00:20:01,000 --> 00:20:06,000
Before that, I would be, you know, party, party, party, cram.

276
00:20:06,000 --> 00:20:09,000
And as you can't do this, so you force it all in.

277
00:20:09,000 --> 00:20:11,000
You say, learn, learn, learn, learn.

278
00:20:11,000 --> 00:20:13,000
It doesn't work.

279
00:20:13,000 --> 00:20:18,000
When you study and meditate, you learn how the mind works.

280
00:20:18,000 --> 00:20:22,000
You start to see, oh, yep, my mind is full now.

281
00:20:22,000 --> 00:20:25,000
Your mind is fully stopped standing.

282
00:20:25,000 --> 00:20:27,000
Take a meditation like that.

283
00:20:27,000 --> 00:20:29,000
And you know exactly.

284
00:20:29,000 --> 00:20:32,000
You're so clear in the mind that you know exactly

285
00:20:32,000 --> 00:20:34,000
when it's time to start again.

286
00:20:34,000 --> 00:20:36,000
It's very easy actually.

287
00:20:36,000 --> 00:20:41,000
If studying becomes easy, world becomes easy, work becomes easy.

288
00:20:41,000 --> 00:20:43,000
Everything becomes easy.

289
00:20:43,000 --> 00:20:52,000
When the mind is trained, life becomes easy.

290
00:20:52,000 --> 00:20:59,000
In fact, if you train yourself to a great extent,

291
00:20:59,000 --> 00:21:04,000
eventually life becomes, they say, I'm blessing.

292
00:21:04,000 --> 00:21:06,000
For many people, life is a curse.

293
00:21:06,000 --> 00:21:09,000
Some people want to kill themselves.

294
00:21:09,000 --> 00:21:14,000
Your life is a curse for them because their mind is untrained.

295
00:21:14,000 --> 00:21:18,000
Anything comes in, oh, it's terrible.

296
00:21:18,000 --> 00:21:20,000
They can't handle it.

297
00:21:20,000 --> 00:21:21,000
They get upset.

298
00:21:21,000 --> 00:21:22,000
They don't know how to deal with it.

299
00:21:22,000 --> 00:21:27,000
They don't have a means of dealing with it.

300
00:21:27,000 --> 00:21:30,000
If you train yourself in meditation,

301
00:21:30,000 --> 00:21:33,000
your life becomes a blessing for the world.

302
00:21:33,000 --> 00:21:36,000
If you're seeing people who have been practicing

303
00:21:36,000 --> 00:21:39,000
for 10, 20, 30, 40, 50 years,

304
00:21:39,000 --> 00:21:42,000
they become a blessing for people.

305
00:21:42,000 --> 00:21:44,000
You see this in Thailand,

306
00:21:44,000 --> 00:21:47,000
some meditation teachers here,

307
00:21:47,000 --> 00:21:51,000
the meditation teachers have been for 20 years teaching,

308
00:21:51,000 --> 00:21:54,000
30 years on our teacher,

309
00:21:54,000 --> 00:21:56,000
60 years or something teaching.

310
00:21:56,000 --> 00:21:58,000
And now he's everywhere he goes,

311
00:21:58,000 --> 00:22:01,000
his life is just a blessing for himself

312
00:22:01,000 --> 00:22:03,000
and for other people.

313
00:22:03,000 --> 00:22:06,000
He's able to help from great people.

314
00:22:09,000 --> 00:22:11,000
And the world becomes,

315
00:22:11,000 --> 00:22:13,000
they say, world becomes your oyster.

316
00:22:13,000 --> 00:22:15,000
There's anything you can do.

317
00:22:15,000 --> 00:22:17,000
We'll be talking about it and saying,

318
00:22:17,000 --> 00:22:23,000
how we're talking about this course

319
00:22:23,000 --> 00:22:26,000
I'm conducting over Skype and everything.

320
00:22:26,000 --> 00:22:30,000
It's interesting how easy it all becomes,

321
00:22:30,000 --> 00:22:32,000
how easy life becomes.

322
00:22:32,000 --> 00:22:37,000
You have this challenge,

323
00:22:37,000 --> 00:22:38,000
some student,

324
00:22:38,000 --> 00:22:41,000
and he wants to practice things in this way.

325
00:22:41,000 --> 00:22:43,000
How do I deal with it?

326
00:22:43,000 --> 00:22:44,000
Whereas before you'd be afraid,

327
00:22:44,000 --> 00:22:46,000
if I say the wrong thing,

328
00:22:46,000 --> 00:22:49,000
maybe he'll think I'm not a good teacher,

329
00:22:49,000 --> 00:22:53,000
I'm sorry, when you practice meditation,

330
00:22:53,000 --> 00:22:57,000
when you train your mind.

331
00:22:57,000 --> 00:23:03,000
Whereas what if he thinks I'm a bad teacher?

332
00:23:03,000 --> 00:23:04,000
What if I give a talk tonight

333
00:23:04,000 --> 00:23:06,000
and you think I'm useless teacher?

334
00:23:06,000 --> 00:23:09,000
When you run away and you leave?

335
00:23:09,000 --> 00:23:11,000
I've had to do it when I was in Thailand,

336
00:23:11,000 --> 00:23:14,000
many students ran away and I was devastated.

337
00:23:14,000 --> 00:23:16,000
One time I told all the meditators

338
00:23:16,000 --> 00:23:20,000
because they said they couldn't control them.

339
00:23:20,000 --> 00:23:23,000
They tried, they would meet up and talk

340
00:23:23,000 --> 00:23:25,000
and sometimes a man and a woman

341
00:23:25,000 --> 00:23:28,000
in a cup on the view, like romantic view

342
00:23:28,000 --> 00:23:32,000
and you think, oh no, we're going to get in trouble for this.

343
00:23:32,000 --> 00:23:33,000
And so I would tell them,

344
00:23:33,000 --> 00:23:35,000
no talking, no talking,

345
00:23:35,000 --> 00:23:37,000
and I would see them and I would tell them to talk.

346
00:23:37,000 --> 00:23:39,000
Then they'll just run away.

347
00:23:39,000 --> 00:23:41,000
We don't want this.

348
00:23:41,000 --> 00:23:43,000
And I was devastated.

349
00:23:43,000 --> 00:23:48,000
I don't know, all my students ran away.

350
00:23:48,000 --> 00:23:51,000
I knew they were doing that time.

351
00:23:51,000 --> 00:23:53,000
She was that day's the day.

352
00:23:53,000 --> 00:23:57,000
Another view, sometimes we would have a man in the world

353
00:23:57,000 --> 00:24:00,000
sitting under the moon or something.

354
00:24:00,000 --> 00:24:02,000
Oh, do you think, oh, yeah,

355
00:24:02,000 --> 00:24:06,000
it's not going to be happy about this.

356
00:24:06,000 --> 00:24:08,000
You're one of the good ones,

357
00:24:08,000 --> 00:24:10,000
that's why I don't remember you so well.

358
00:24:10,000 --> 00:24:13,000
So I remember you in the morning.

359
00:24:13,000 --> 00:24:22,000
Kathy, I was just expecting someone else.

360
00:24:22,000 --> 00:24:23,000
So when you train the mind,

361
00:24:23,000 --> 00:24:25,000
you don't suffer from these things.

362
00:24:25,000 --> 00:24:29,000
You may don't suffer from the vicissitudes of life

363
00:24:29,000 --> 00:24:31,000
when things change.

364
00:24:31,000 --> 00:24:33,000
And life becomes easy.

365
00:24:33,000 --> 00:24:36,000
You deal with people and they don't like what you do

366
00:24:36,000 --> 00:24:39,000
and you make mistakes.

367
00:24:39,000 --> 00:24:41,000
Even an aura hand can make mistakes.

368
00:24:41,000 --> 00:24:43,000
I'm sure of it.

369
00:24:43,000 --> 00:24:45,000
But when an aura hand makes mistakes,

370
00:24:45,000 --> 00:24:48,000
I've seen Anjan and my teacher make mistakes.

371
00:24:48,000 --> 00:24:51,000
And it's wonderful to see him make a mistake.

372
00:24:51,000 --> 00:24:53,000
He doesn't care.

373
00:24:53,000 --> 00:24:56,000
He doesn't try to hide it,

374
00:24:56,000 --> 00:24:59,000
but he doesn't feel bad.

375
00:24:59,000 --> 00:25:02,000
He doesn't hate himself for it.

376
00:25:02,000 --> 00:25:07,000
He says, oh, okay, it's not like that.

377
00:25:07,000 --> 00:25:10,000
Or something.

378
00:25:10,000 --> 00:25:12,000
And he moves on.

379
00:25:12,000 --> 00:25:14,000
He's no attachment.

380
00:25:14,000 --> 00:25:16,000
He'll give a talk,

381
00:25:16,000 --> 00:25:20,000
and sometimes his talks are a mess, really.

382
00:25:20,000 --> 00:25:21,000
He'll give a talk,

383
00:25:21,000 --> 00:25:22,000
and then he'll forget where he was,

384
00:25:22,000 --> 00:25:24,000
and he'll start talking about something else.

385
00:25:24,000 --> 00:25:26,000
Because he's gotten old,

386
00:25:26,000 --> 00:25:29,000
but he's also very tired of the time.

387
00:25:29,000 --> 00:25:30,000
Too much work.

388
00:25:30,000 --> 00:25:32,000
So I've got recordings,

389
00:25:32,000 --> 00:25:34,000
I've got to talk confidently.

390
00:25:34,000 --> 00:25:36,000
But it was always good.

391
00:25:36,000 --> 00:25:39,000
Because he'd stopped halfway through,

392
00:25:39,000 --> 00:25:41,000
and if you're a study monk,

393
00:25:41,000 --> 00:25:42,000
he'd get fed up with him,

394
00:25:42,000 --> 00:25:43,000
you'd think,

395
00:25:43,000 --> 00:25:45,000
I can't even remember a list of five things.

396
00:25:45,000 --> 00:25:47,000
And the kind of a talk is this.

397
00:25:47,000 --> 00:25:48,000
He gets through three,

398
00:25:48,000 --> 00:25:50,000
and suddenly he's talking about something else.

399
00:25:50,000 --> 00:25:51,000
But it was all done.

400
00:25:51,000 --> 00:25:52,000
It's okay.

401
00:25:52,000 --> 00:25:53,000
He's at three,

402
00:25:53,000 --> 00:25:55,000
and boom, it's changing now.

403
00:25:55,000 --> 00:25:58,000
This is a sign that this person is still cleaning.

404
00:25:58,000 --> 00:25:59,000
And so, okay.

405
00:25:59,000 --> 00:26:01,000
So now he's talking about something else,

406
00:26:01,000 --> 00:26:02,000
and it always was good.

407
00:26:02,000 --> 00:26:03,000
And the end,

408
00:26:03,000 --> 00:26:05,000
that's practice meditation together.

409
00:26:05,000 --> 00:26:09,000
So this is what it means,

410
00:26:09,000 --> 00:26:11,000
that kamani, young kamani,

411
00:26:11,000 --> 00:26:13,000
and train your mind,

412
00:26:13,000 --> 00:26:14,000
it's what he's saying.

413
00:26:14,000 --> 00:26:17,000
Because when the mind is trained,

414
00:26:17,000 --> 00:26:21,000
this is what is truly useful.

415
00:26:21,000 --> 00:26:24,000
There's nothing more useful in the world

416
00:26:24,000 --> 00:26:26,000
than a trained mind.

417
00:26:26,000 --> 00:26:29,000
And the trained mind brings happiness.

418
00:26:29,000 --> 00:26:32,000
So this is very brief,

419
00:26:32,000 --> 00:26:34,000
and part of the idea is

420
00:26:34,000 --> 00:26:36,000
to just show you what is Buddhism.

421
00:26:36,000 --> 00:26:37,000
These people wonder,

422
00:26:37,000 --> 00:26:38,000
what is Buddhism?

423
00:26:38,000 --> 00:26:40,000
Now you see, wow, the Buddhism,

424
00:26:40,000 --> 00:26:41,000
the Buddha was,

425
00:26:41,000 --> 00:26:43,000
this is what we're practicing.

426
00:26:43,000 --> 00:26:46,000
This is, this comes from the Buddha.

427
00:26:46,000 --> 00:26:47,000
So now we know,

428
00:26:47,000 --> 00:26:49,000
because some people will say,

429
00:26:49,000 --> 00:26:50,000
um,

430
00:26:50,000 --> 00:26:52,000
meditation,

431
00:26:52,000 --> 00:26:53,000
and Buddhism.

432
00:26:53,000 --> 00:26:54,000
Some people even say,

433
00:26:54,000 --> 00:26:55,000
I don't want to be,

434
00:26:55,000 --> 00:26:57,000
I don't want to get into Buddhism.

435
00:26:57,000 --> 00:26:59,000
I'm just interested in meditation,

436
00:26:59,000 --> 00:27:00,000
you're saying.

437
00:27:00,000 --> 00:27:01,000
Some people even get to that,

438
00:27:01,000 --> 00:27:03,000
but they get to view the opposite view

439
00:27:03,000 --> 00:27:04,000
where they say,

440
00:27:04,000 --> 00:27:05,000
they're not interested in them,

441
00:27:05,000 --> 00:27:08,000
so they just want to practice meditation.

442
00:27:08,000 --> 00:27:13,000
And it's not that Buddhism is meditation,

443
00:27:13,000 --> 00:27:15,000
and meditation is Buddhism,

444
00:27:15,000 --> 00:27:18,000
but where they meet,

445
00:27:18,000 --> 00:27:19,000
well,

446
00:27:19,000 --> 00:27:20,000
that is,

447
00:27:20,000 --> 00:27:24,000
that is the core.

448
00:27:24,000 --> 00:27:25,000
That's what most,

449
00:27:25,000 --> 00:27:26,000
what's most important,

450
00:27:26,000 --> 00:27:28,000
what's most important is,

451
00:27:28,000 --> 00:27:30,000
not cultural Buddhism,

452
00:27:30,000 --> 00:27:31,000
or traditional Buddhism,

453
00:27:31,000 --> 00:27:32,000
or scholarly Buddhism,

454
00:27:32,000 --> 00:27:37,000
and all meditations that have nothing to do with,

455
00:27:37,000 --> 00:27:39,000
with this,

456
00:27:39,000 --> 00:27:43,000
with the training of the mind.

457
00:27:43,000 --> 00:27:47,000
If it's just where the purpose is a feeling peaceful for a while,

458
00:27:47,000 --> 00:27:50,000
or seeing special things,

459
00:27:50,000 --> 00:27:55,000
reading people's minds flying through the air,

460
00:27:55,000 --> 00:27:58,000
and it's not true meditation,

461
00:27:58,000 --> 00:28:01,000
not the most important meditation.

462
00:28:01,000 --> 00:28:04,000
So now you can see that,

463
00:28:04,000 --> 00:28:05,000
what did the Buddha teach?

464
00:28:05,000 --> 00:28:08,000
Well, we have at the very beginning of this book,

465
00:28:08,000 --> 00:28:10,000
Kamaniyya Akkamanya,

466
00:28:10,000 --> 00:28:14,000
what is most useful or what is most useless?

467
00:28:14,000 --> 00:28:16,000
The trained mind is most useful,

468
00:28:16,000 --> 00:28:18,000
the most useful thing in the world,

469
00:28:18,000 --> 00:28:21,000
and the untrained mind is the most useful.

470
00:28:21,000 --> 00:28:22,000
So when we practice,

471
00:28:22,000 --> 00:28:25,000
when our mind is in the present moment,

472
00:28:25,000 --> 00:28:28,000
seeing things clearly as they are,

473
00:28:28,000 --> 00:28:30,000
you can see for yourself,

474
00:28:30,000 --> 00:28:32,000
you don't need my word for it.

475
00:28:32,000 --> 00:28:36,000
You've all been practicing enough to know this is the truth.

476
00:28:36,000 --> 00:28:40,000
There's nothing more important than this training that we're doing.

477
00:28:40,000 --> 00:28:44,000
You can feel for yourself

478
00:28:44,000 --> 00:28:50,000
how awesome is the power of just one moment

479
00:28:50,000 --> 00:28:52,000
of seeing things as they are.

480
00:28:52,000 --> 00:28:54,000
The point that the Buddha said,

481
00:28:54,000 --> 00:28:58,000
one moment seeing things as they are,

482
00:28:58,000 --> 00:29:01,000
is better than a hundred years of not seeing things as they are.

483
00:29:01,000 --> 00:29:02,000
The person lives a hundred years,

484
00:29:02,000 --> 00:29:07,000
but never sees things as they are.

485
00:29:07,000 --> 00:29:08,000
In better day,

486
00:29:08,000 --> 00:29:11,000
they were the only of one day

487
00:29:11,000 --> 00:29:14,000
and see things as they are.

488
00:29:14,000 --> 00:29:17,000
So, that talk.

489
00:29:17,000 --> 00:29:20,000
Now we get onto the meditation practice,

490
00:29:20,000 --> 00:29:23,000
start with mindful frustration

491
00:29:23,000 --> 00:29:28,000
and then walking around today.

