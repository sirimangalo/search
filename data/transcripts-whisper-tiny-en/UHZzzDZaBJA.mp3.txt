Okay, good evening everyone. Welcome to our daily session.
The topic I wanted to focus on tonight is related to delusion or ignorance.
The curious characteristic quality of samsara,
that it may come to be in a state where we,
where we don't see things as they are.
It might sound insignificant, but it's actually incredibly important statement.
We know this in science, modern material science.
That's why they avoid the best of their ability dependent on perception.
The realization that our perception can be wrong.
It's deceiving how simple that statement is, and you might think,
I get it, I understand that.
It's guaranteed that when you begin to explore the realm of perception through meditation practice,
that you'll be surprised, that you'll be shocked with the realization that you're really just
not even distorted reality, but completely misunderstood it,
and totally.
To the extent that black seemed like white could seem like bad.
Bad seemed good.
So in modern material science, they reject perception for this reason.
You can't rely on your perception.
They recognize this through studies, through experiment.
I think in religion, of course, in spirituality,
where we're prone to succumb to this.
It's how you can come to be so sure of your beliefs,
so sure of your convictions, and only hasn't turned out to be wrong.
I remember once driving around in Shenmai,
and we had it somewhere, our whole troop, when I was teaching, we were heading somewhere to get to the super highway.
I told him to turn left.
I said, you have to turn left.
There's the super highway.
There's the slouch and monkeys.
No, no, that's not the super highway.
And finally, they turned left, and then I looked and I said, oh, this isn't the super highway.
It's very easy to be completely sure of yourself and be wrong.
The problem, of course, with this stance of material science,
is that it ignores a very terrible problem with
our reality and our existence, and attempts to sidestep the problem
by strict reliance on third person's impersonal, not third person, but impersonal observations.
Find ways, double-blind tests.
But that doesn't solve the problem that we are really messed up in our perceptions.
In more ways, the science, even, is, I think, clear about,
able to understand.
And so Buddhism does something bold and perhaps seen as impossible by a material scientist,
and that is to correct our perceptions.
I find a way of being non, I don't know, I used to say objective,
but I think that word is used differently by different people.
I'm going to say objective, but I mean by that is impartial or unbiased,
and even more than unbiased, clear, seeing clearly.
We claim the ability to correct our misperceptions and to dispel the delusion and the anger.
But I want to emphasize this point, and it's both incredibly important to understand
and incredibly important to understand as being the root of the problem
that we can misunderstand, that we can be completely and utterly wrong about something.
Such that there's no hint of it being wrong.
Apart from the consequences, of course.
And this is the thing is, well, if we perceive things in a certain way, what's wrong with that?
If I think ice cream is good, what's wrong with that?
I like chocolate ice cream, you like vanilla ice cream.
We all like certain things, we all have certain partialities, perceptions,
we all have certain, let's not even talk about partialities, but we all have certain character types.
I have a short temper, let's say, or I know I'm this, or I'm that.
I'm A-type personality, B-type personality.
It's just who I am.
And so you find people trying to perform mental gymnastics to try to make sure that everyone
whatever person type of person they are is understood as being equally valid.
Right?
So if our perception is different from someone else's perception, what's wrong with that?
And the problem is that there is, as it turns out, an underlying reality that doesn't change.
You think, well, my reality is different from there as well, actually, it's not.
And there's no one in this world who can escape a certain fairly simple qualities or characteristics of reality.
So greed, for example, greed and craving and desire and thirst and ambition, these are all very good examples of this.
We wonder how a person can be driven to rape another person, for example.
We study addiction, we study passion or desire, sexual desires, big one.
We study addiction to food and so on.
And then deny that there are negative consequences to desire.
And so intellectually, this is easy to accept that, perhaps not clear to us, but should be easy to understand that there is an underlying reality that doesn't allow for addiction.
It doesn't allow for a peaceful and a happy, addicted state.
It's a reality whereby addiction causes suffering.
But this idea of seeing things completely wrong helps us to understand why intellectual understanding isn't enough.
There's intellectual understanding, what is it? It's a thought.
There's no such thing as intellectual understanding.
It's a series of thoughts that lead to a single thought with a lot of conviction.
So you have this thought, which is your premises, at least to this thought, and this thought.
And finally, you come up with a conclusion.
And that has some power to it. It gives you conviction.
So it will, to some extent, affect your behavior.
But it doesn't change the fact that when you see cheese cake, you like it.
You want it. You think you feel that that is going to bring you happiness.
When you see the object of your desire, a beautiful man, a beautiful woman,
and you see the body when you see things.
When you hear music, it doesn't change the fact that we get attached.
And we become addicted.
That it blinds us through any suffering.
That you'll be completely blind.
You can rationalize it. You can examine your life and see how terrible you are.
You are terribly destroying your life.
Read Dostiovsky, some of his stuff.
Pretty hard core.
Suffering. And he could write about it.
He knew what he was doing to his life.
To his mind.
But the addict can't help themselves.
It's a totally different, totally different level.
This is how someone could write how, in the time of the Buddha,
there was this novice Samanera who raped his cousin,
who was a bikkhuni. She was an arahant.
And when he left her kuti, her hut, he Earth swallowed him up, they say.
And I went to hell.
That a person can't see that that's wrong, right?
And yet, there are many people in this world who can't see it's wrong,
and we want to call them monsters.
We want to say that that is the sort of a person very different from us.
That sort of person is nothing like us.
But we all have this in us.
And to some extent, it's incredibly blame-worthy,
but it's also, in a sense, blameless,
because according to their perception, it's not blameless,
but it's, in a sense, what I mean, it's completely purely.
This is going to make me happy.
There's a total ignorance and delusion and lack of clarity.
They're unable to see, as what I mean.
At that moment, the mind is consumed.
The mind is clear that this is going to make me happy.
And, oops, boy, was I wrong.
That's how this, some sorrow works.
That's how we get stuck and why we stay stuck.
And that's how insight meditation works.
That's why insight meditation works.
That's why the answer isn't to force yourself
to follow your intellectual understanding, but it's to see clearly.
Anger is another one.
Anger is, it's incredible what anger will do to you.
There was, uh, time of the Buddha, there was this, his stepfather.
He was so angry at him.
For taking away his, taking for leaving his daughter first of all
when he left home and then for later on for ordaining her.
And taking her away from him.
As well, along with his grandson, Rahul.
And he ended up going to hell as well.
And David Atlas, another example.
David Atlas said to have spouted, vomited blood.
He was so angry.
We get angry and it blinds us.
At that moment, we really think that anger is the best solution.
We don't at that moment have conscious awareness that this is wrong.
We're going to intellectually wait.
We might know and we might try to be a nice, nice people.
It's quite common for Buddhists, you know, Buddhists.
We're nice people, right?
Why? Because we have all these teachings on how to be nice people.
Christians, many Christians are nice people for similar reasons
because Christianity, I think, is particularly concerned with being nice.
You know, turning the other cheek is very powerful saying,
Christians can be really nice.
And that's some really nice Christians.
Of course, they have lots of other crazy ideas.
But the niceness is there.
But as with Buddhists, we can be very nice.
But don't get us upset.
You know, don't poke the hornet's nest
because Buddhists will get just as angry as anyone else.
You know, I've had monks yelling at me.
Remember early on, I often tell the story.
I've come back from Canada.
I spent my first reigns for freedom of Canada.
I came back to Thailand and I hadn't been there two weeks
and we're sitting in the dining hall and suddenly there's a clatter beside us.
And I turn over all the monks sitting in the dining room.
I turn, and one of the monks has gotten up and he's just pounding
on one of the other monks.
His nose is broken.
He's got a blood going everywhere.
So I got up and started and grabbed this.
And remember, of all the monks, I was the only one.
At that moment, he thought punching this other monk was the right thing to do.
Isn't it crazy how he can think, especially as a Buddhist monk,
how he could come to the point where he thought that that was a good idea?
He was totally blind.
And he knew it. He felt awful about anything.
Terribly ashamed.
It's not a bad monk.
He's just strong anger inside.
He's built up.
And delusion.
I mean, David, that does a good example of delusion.
Imagine, this guy wanted to be a desire as well.
He wanted to be a head of the leader.
He wanted the Buddha.
Yeah, it was arrogance.
That's delusion.
We hold ourselves up.
We puff ourselves up how we think how important we are.
You look at these people who are all puffed up.
Anyone who has self-important.
That's kind of ridiculous, really.
They become less attractive, right?
They become less impressive.
They make everybody angry around them.
They make everyone afraid of them.
They create such suffering.
There's no greatness to be had in arrogance and pride.
It's unpleasant.
And the people don't see this, right?
They don't see how silly they look when they puff themselves up.
They blow fish or like a peacock.
We really think that there's good to be had by showing off.
Now, if you ever hear people, there's culture.
I've been in cultures.
I guess I won't say which.
But I've been in cultures where some of the people are incredibly inclined to brag about themselves.
It's become a culture.
In fact, when I tell you how great they are, how people are intent upon
you, how great they are.
Mostly, it just frustrates us.
And it makes us feel jealous.
Because we're caught up and all that.
But if you step back and as a meditative, you think,
who could think that such a thing is a good idea?
But of course, at the moment, for that person,
I think showing off somehow impresses people.
Somehow makes people like you more or something.
That's sort of what we think.
We think we're going to feel good.
It'll make us feel good.
Because inside, we usually hate ourselves.
We have lots of self-esteem issues.
So we compensate by seeing if everybody else thinks I'm great.
And maybe I'll be great.
It's kind of funny to watch.
This is why this is why Buddhas and Arahant have a funny sense of humor.
They smile at the artist's things.
Because they're not odd, really.
It's just that all of us are too blind to see the humor in it.
It's starting around like peacocks, fighting like roosters.
And they don't like dogs in heat, or caught up in greed, anger, and delusion.
You look at things like food.
And if you ever watch people stuffing their face,
you look at even sexual activity.
I mean, how ridiculous is it?
How ridiculous is sexual sex?
What a silly activity.
Kissing.
Kissing the start line.
What is kissing?
Just smack our lips together for a while.
Isn't it absurd?
Yet when you're kissing, it seems like the pinnacle.
Right?
And people have written poems and songs and books about kissing,
about sex, about food.
Rest up in books.
It doesn't make you stop and scratch your head.
We have books with pictures of fat and grained oils and salts and sugars and stuff.
All this stuff that eventually becomes urine and feces.
I mean, that's what we're writing.
And that's what we're making books about and praising and poems and everything.
So, I mean, it's ridiculous.
But the key, the more important point is this understanding of how wrong we can be,
of realizing that we don't see it as ridiculous.
And it's not even that it's unclear to us, it's that it really undeniably seems meaningful.
So, the key out of that is that we're wrong.
And that we can prove to ourselves that we're wrong.
And what we're wrong about.
Very easily, in fact.
All you have to do is look when a person looks, when you look, you see.
When you look, you see, when you see, you know, if you want to know, you have to look.
If you want to know, you have to see, if you want to see, you have to look.
Look, see, you know.
When you practice mindfulness, when you cultivate mindfulness, desire,
and the objects of your desire seem undesirable, they become undesirable.
You can clearly see, geez.
What am I doing, lasting after the human body?
What am I doing, lasting after this taste in my mouth?
And sugar and salt.
It's like you pull away the veil of ignorance.
It's like you turn on the light.
At Jentong, he this monk here.
He said, once I caught it on tape, he said,
mindfulness is like a light.
All the evil things, it was just a really, really good way of putting
all the evil things, they're all dark.
Mindfulness is like a light.
When you turn on the light, the darkness disappear.
That's really true.
In that moment, when you're mindful, the darkness is disappeared.
So, important for us to understand.
And an important reason for us to be very careful about self-confidence being sure of ourselves.
I think one of the things a meditator learns early on is that you can't trust yourself.
You can't trust intuition.
You have no reason to trust yourself.
But unlike material scientists, you can change that.
You don't need to trust yourself.
But that you can come to see through your delusion.
You can come to see your misunderstanding.
You can come to clear them up.
To the point that, I mean, the argument might be made how do you know that then you're seeing things clearly.
But it's the point that they then become in line with reality.
So, you might say, after your practice meditation, how do you know you're still not seeing things incorrectly?
The difference is that now it's in light, now your understanding is in line with reality.
And that's important.
That's clear.
The way we see things, saw things before, was not in line with reality.
These things that couldn't bring us happiness, we thought they could bring us happiness.
They can't.
You see impermanence.
You see suffering.
You see non-self.
You see these qualities of reality that nothing is stable.
Because of that nothing can satisfy us.
There's nothing that is mean that is mine.
There's nothing that is under our control.
So, there you go.
There's the demo for tonight.
Thank you all for joining me.
