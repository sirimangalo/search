1
00:00:00,000 --> 00:00:05,400
Okay, next question from Mr. Chan Buddhist who says,

2
00:00:05,400 --> 00:00:13,600
can I find the true teachings of Buddhism by just book learning, reading and watching videos, reading things online?

3
00:00:13,600 --> 00:00:17,400
I don't have a sunken near me, so at all, so I can't visit one.

4
00:00:17,400 --> 00:00:21,600
I must learn by books and on the internet. Please help me find the answer.

5
00:00:21,600 --> 00:00:30,600
Yeah, I think you can learn things on the internet. Obviously otherwise I wouldn't be posting these videos.

6
00:00:30,600 --> 00:00:38,600
The caution always is that it's such a contrived medium.

7
00:00:38,600 --> 00:00:45,600
There's so much in between you and the teaching that it's always going to be limited.

8
00:00:45,600 --> 00:00:59,600
It's a very rigid medium and at the very least puts many layers between you and the teacher, between you and the Buddha, between you and the Dhamma.

9
00:00:59,600 --> 00:01:08,600
So for getting a basic sense and a way of living your life is a Buddhist, I think there's no problem.

10
00:01:08,600 --> 00:01:14,600
If you really want to take intensive meditation practice, it can be done.

11
00:01:14,600 --> 00:01:22,600
I've led someone through meditation on Skype, almost all the way through meditation course.

12
00:01:22,600 --> 00:01:43,600
I've done that before, but I've also done it and had it fail where the meditator loses interest and it's not the same as being in a meditation center where everything is provided for you where it's totally conducive

13
00:01:43,600 --> 00:02:01,600
to progress on the path that's a much more preferable state and will allow for a much greater realization of the Buddha's teaching.

14
00:02:01,600 --> 00:02:12,600
So, yeah, you can learn online and I think I'm sure you found ways of doing that. I think this is why I'm putting these things out.

15
00:02:12,600 --> 00:02:17,600
I've also made a PDF of my meditation videos, that's on my web blog.

16
00:02:17,600 --> 00:02:19,600
There's lots of Buddhist sites.

17
00:02:19,600 --> 00:02:32,600
One I would recommend is called dhammawheel.org, I think, or.com. I don't know, you can look up dhammawheel Buddhist forum.

18
00:02:32,600 --> 00:02:46,600
Good place. I've been on and off there. There's a lot of Buddhist to the tradition that I follow on there talking and discussing various subjects.

19
00:02:46,600 --> 00:03:04,600
But what I always want to say when I get questions like this or when we're talking about this subject is that it's really not as difficult as most people think to find a Buddhist center where you can practice some form of Buddhist meditation.

20
00:03:04,600 --> 00:03:23,600
I mean, maybe it means saving up a little bit of money, getting a passport and taking a flight somewhere, but it could also just mean getting on a bus and taking a risk and trying something out.

21
00:03:23,600 --> 00:03:36,600
There are many times there are ways that we're not working towards that and we're not putting it as a priority.

22
00:03:36,600 --> 00:03:46,600
I'm sure there are probably situations where it's not possible, where there is no way of getting a passport or no way of getting the money or whatever.

23
00:03:46,600 --> 00:03:54,600
I think if you work hard enough and if you are determined enough, where there's a will, there's a way.

24
00:03:54,600 --> 00:04:01,600
And I wouldn't be just satisfied with book learning or learning from the internet.

25
00:04:01,600 --> 00:04:09,600
I would make the effort because we've only got this life. We don't know where we're going when we die. We don't know what that's going to mean.

26
00:04:09,600 --> 00:04:28,600
And if you let your circumstances get in the way and turn something that's difficult into something that's impossible, then you've wasted this prime opportunity that you have as a human being living on this earth to do something.

27
00:04:28,600 --> 00:04:40,600
I mean, take a risk, go. We're so averse to doing something that's difficult or that's out of the ordinary or dangerous in any way.

28
00:04:40,600 --> 00:04:54,600
And I think I would encourage people to know, try the internet's a great place, not only to learn about Buddhism, but to find Buddhist centers when you find one that seems to be what you're looking for, go for it.

29
00:04:54,600 --> 00:04:58,600
There's lots of ways to ask people about what centers are good and so on.

30
00:04:58,600 --> 00:05:03,600
I would recommend taking that risk and going somewhere.

31
00:05:03,600 --> 00:05:15,600
At the very least, you're sure in any Buddhist center to gain a great amount of concentration and focus of mind, which will be very helpful for developing insight in the future.

32
00:05:15,600 --> 00:05:31,600
If you're really not able, then yeah, the internet is a good, the best substitute we have and there are ways to contact teachers by email or through things like Skype or YouTube or so on that can serve as a surrogate.

33
00:05:31,600 --> 00:05:47,600
So either way, all the best, good luck and hope you're able to find a solution.

