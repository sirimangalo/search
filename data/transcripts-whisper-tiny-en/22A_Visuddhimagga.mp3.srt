1
00:00:00,000 --> 00:00:15,800
Now, I thought I had finished loving kindness, but I think I'll go back a little.

2
00:00:15,800 --> 00:00:25,680
So when you practice loving kindness, say first you try to see danger in anger or hate

3
00:00:25,680 --> 00:00:32,080
and then try to see the advantages of patience. And then you practice loving kindness meditation

4
00:00:32,080 --> 00:00:41,080
first to watch yourself and then to a person who is here and then to a neutral person

5
00:00:41,080 --> 00:00:53,960
and last to a person to an enemy. And when you are able to break down the barriers, that

6
00:00:53,960 --> 00:01:01,800
means when you have the mental impartiality towards these four kinds of persons, you are

7
00:01:01,800 --> 00:01:13,680
set to have broken the barriers. So with the breaking of barriers come the excess concentration.

8
00:01:13,680 --> 00:01:23,560
So in other meditation, we can say that at this point you get the learning sign and then

9
00:01:23,560 --> 00:01:29,240
at another point you get the counterpart sign. But yeah, there is no learning sign and

10
00:01:29,240 --> 00:01:42,280
counterpart sign separately. So the sign is the counterpart sign and it is not a special

11
00:01:42,280 --> 00:01:49,960
sign. But when you are able to break the barriers and you are set to have obtained the counterpart

12
00:01:49,960 --> 00:02:01,120
sign and at that moment also the excess concentration. Then you dwell on the beings

13
00:02:01,120 --> 00:02:09,120
and cultivate further so that you are taking Jana. And with Mida, with the loving kindness

14
00:02:09,120 --> 00:02:19,680
meditation, one can get up to the third Jana in full-fold method and the fourth Jana in

15
00:02:19,680 --> 00:02:28,800
five-fold method. And then come the text and commentary. It is page 333. The text

16
00:02:28,800 --> 00:02:41,200
is given here is the stock free text found in many soldiers and also in Abidama. So that

17
00:02:41,200 --> 00:02:48,180
the reference given here is Vibanga, that is Abidama and D, that is Soda. So in Soda

18
00:02:48,180 --> 00:02:57,760
Vidaka and also in Abidama, you find this passage again and again, one of the loving kindness

19
00:02:57,760 --> 00:03:07,260
meditation is described. And then the explanation of the words. So down about the middle

20
00:03:07,260 --> 00:03:23,220
of the page in paragraph 47, that is about the fourth line. Just as to oneself, equality

21
00:03:23,220 --> 00:03:31,100
with oneself without making the distinction, this is another being a participant. Or alternatively,

22
00:03:31,100 --> 00:03:38,860
equally sub-bartataya is with the whole state of mind, not reserving even a little is what

23
00:03:38,860 --> 00:03:52,180
is meant. But here really means not being distracted outside, even a little. So with the

24
00:03:52,180 --> 00:03:59,060
whole state of mind, the practice loving kindness meditation, not to be distracted outside

25
00:03:59,060 --> 00:04:21,140
even a little. And then in paragraph 48, measureless through familiarity and through having

26
00:04:21,140 --> 00:04:29,940
measureless beings as its object. So it is called, measureless, because it is practiced

27
00:04:29,940 --> 00:04:37,220
again and again. And so you are very familiar with that meditation, that kind of chana.

28
00:04:37,220 --> 00:04:45,060
Or it can mean measureless through having measureless beings as its object. Since its object

29
00:04:45,060 --> 00:04:51,500
is measureless, it is also called measureless. It is in dude, right? Because I mean in

30
00:04:51,500 --> 00:05:16,860
dude, first of 48 in dude. And dude, yeah. I think it would mean the same thing. Something

31
00:05:16,860 --> 00:05:36,700
like end out with D with. But that is a correct word. And D with it. And then the description

32
00:05:36,700 --> 00:05:49,620
of C 528 and modes of loving kindness on that are explained last week. So at the bottom

33
00:05:49,620 --> 00:06:00,780
of page 335, the explanation of the word be in sata, they are held sata, grouped with

34
00:06:00,780 --> 00:06:06,980
sata by desire. The pali word is just to be attached. So they are attached to, they are

35
00:06:06,980 --> 00:06:14,300
very attached to, we sata means very attached to by way of desire. So they are called

36
00:06:14,300 --> 00:06:25,500
sata in pali. And those who have no greed are also called sata. Because it is the common

37
00:06:25,500 --> 00:06:38,940
term or common usage for beings actually. And the example given here is tala vanta in

38
00:06:38,940 --> 00:06:48,900
pali, tala vanta means pam fang. If n made of pam leaves, but later on anything which we

39
00:06:48,900 --> 00:06:56,860
use to, to, to fang as is called a fan, whether it is made by, made with pam, pam leaves

40
00:06:56,860 --> 00:07:08,420
or by feathers or whatever. But the same name is applied to that thing. So I, I wanted

41
00:07:08,420 --> 00:07:15,460
out the word pan. So pan really means a feather. But even though it is not feather, it

42
00:07:15,460 --> 00:07:30,260
is called pan. And then you see the word a kara-china-ga. However, in the world, etymologists

43
00:07:30,260 --> 00:07:37,540
a kara-china-ga who do not consider meaning, have it that it is a near name. Now a kara-china-ga

44
00:07:37,540 --> 00:07:45,860
a kara means letters and tina-ga means fingers. So thinkers of letters. So that means

45
00:07:45,860 --> 00:07:51,860
grammarians or etymologists. And there are two kinds of etymologists here. One kind of a

46
00:07:51,860 --> 00:07:59,780
demologist did not such for the meaning of the word, the literal meaning of the word. So

47
00:07:59,780 --> 00:08:05,380
they said it is just a name given to the being. So don't worry about whether it means to be

48
00:08:05,380 --> 00:08:12,260
attached to or whatever. So it is a near name for them. While those who do consider meaning,

49
00:08:12,260 --> 00:08:18,980
have it that there are other kinds of etymologists who said, oh, it has a meaning. And the word

50
00:08:18,980 --> 00:08:32,020
comes from sattva in Sanskrit s-a-t-t-v-a. And a being is called sattva because it has a bright

51
00:08:32,020 --> 00:08:42,260
principle sattva. And this refers to the teaching of sankya system in Indian philosophy.

52
00:08:42,260 --> 00:08:50,500
So sankya system teaches that there are three principles, the sattva,

53
00:08:50,500 --> 00:09:01,460
readjust and tamas. They are given in the footnote. So in Sanskrit this is sattva. Sattva. Yes. That's why your

54
00:09:01,460 --> 00:09:12,660
body sattva. Yes. In pari it is sattva. And then I think I talk about pugula last time, pugula,

55
00:09:12,660 --> 00:09:22,500
the poo and kala. The poo here means hair and kala means to fall. So falling to hair,

56
00:09:22,500 --> 00:09:28,580
those who will fall to hair are called pugula. And the Hindu belief is that you go to

57
00:09:28,580 --> 00:09:43,860
hair if you do not have any offspring. And then in the footnote number seven, when the aggregates

58
00:09:43,860 --> 00:09:51,780
are not fully understood, there is naming a bitana of them and of the consciousness. I think

59
00:09:51,780 --> 00:10:00,820
of this should go. And consciousness of them as self. That means if we do not understand the real

60
00:10:00,820 --> 00:10:07,460
nature of aggregates, we call these aggregates or we give a name to this aggregate are arta self.

61
00:10:09,060 --> 00:10:14,900
And we understand it as arta. So consciousness here really needs understanding. So we understand

62
00:10:14,900 --> 00:10:24,180
as we know it as we know them, we know these aggregates as arta. And that is to say the physical

63
00:10:24,180 --> 00:10:32,580
body or alternatively the five aggregates or not. The sub commentary is explaining the word arta bhava.

64
00:10:32,580 --> 00:10:45,780
You can find the word in paragraph 54 after personality, arta bhava. So the word arta bhava

65
00:10:45,780 --> 00:10:54,500
is explained here. Arta means arta and bhava means something arising from it.

66
00:10:54,500 --> 00:11:07,540
So something here being the naming and knowing. So naming and doing arise from it, therefore

67
00:11:07,540 --> 00:11:17,300
it is called bhava. And how naming and how knowing? Knowing as arta. So that is called arta

68
00:11:17,300 --> 00:11:28,420
bhava in value or in Sanskrit. And then on page next page, that is seven

69
00:11:30,740 --> 00:11:35,540
the first paragraph. And here may all beings be free from enmity is one absorption.

70
00:11:35,540 --> 00:11:46,740
That the sub commentary also said something about absorption or jhana here.

71
00:11:49,300 --> 00:11:57,620
The word use is apana. So apana is normally translated as absorption or tana. But I think if we

72
00:11:57,620 --> 00:12:03,620
translated as jhana but as something like application, I think it makes more sense.

73
00:12:03,620 --> 00:12:10,180
Now, when we practice loving kind of meditation, we practice in four ways.

74
00:12:10,980 --> 00:12:17,700
May all beings be free from enmity. That is one. May all beings be free from, say,

75
00:12:17,700 --> 00:12:24,740
affliction. That is two. May all beings be free from anxiety. That is three. May they read

76
00:12:24,740 --> 00:12:36,900
heavily. That is four. So there is one application. What was the first one? May all beings be free

77
00:12:36,900 --> 00:12:40,980
from enmity. That is on page three hundred and thirty seven.

78
00:12:44,740 --> 00:12:53,540
But the commentary now sub commentary explains that you can get jhana just by by by practicing

79
00:12:53,540 --> 00:13:02,100
this one, say, saying this one sentence. May all beings be free from enmity. May all beings be

80
00:13:02,100 --> 00:13:09,140
free from enmity and so on. So this is called one apana, one absorption or one application.

81
00:13:09,940 --> 00:13:19,060
And in the explanation of these, free from affliction means free from mental affliction.

82
00:13:19,060 --> 00:13:26,500
And free from anxiety, I think that what their translation is not not accurate.

83
00:13:27,380 --> 00:13:31,540
The poly word is anika and that means free from dukha.

84
00:13:33,620 --> 00:13:41,540
So you may have noticed in the in the sheet, I gave you, last last last week, I changed

85
00:13:41,540 --> 00:13:49,220
then. So the second application is translated as may all beings be free from displeasure.

86
00:13:50,900 --> 00:13:57,140
And the third free from affliction. So free from affliction means free from bodily suffering,

87
00:13:57,140 --> 00:14:01,940
free from pain, something like that. And free from displeasure means from dominance or mental

88
00:14:01,940 --> 00:14:12,180
affliction. And the, the foreborn is made they live heavily to literally, it means made the whole

89
00:14:12,180 --> 00:14:19,380
their selves happily or made it, maintain their bodies, made it, made it, meaning their selves happily.

90
00:14:20,180 --> 00:14:24,900
So that, that, that is all right. If we see just made they live happily. So there are four

91
00:14:24,900 --> 00:14:36,340
absorptions or four applications. And then the 11 benefits to be enjoyed from the practice of

92
00:14:37,380 --> 00:14:43,700
loving kindness meditation. And there's leaves and comfort, waste and comfort and so on.

93
00:14:43,700 --> 00:15:01,380
And on page 39, paragraph 71, the reference is given there for like five in the case of the

94
00:15:01,380 --> 00:15:09,860
live woman devotee utara. So you're referred to chapter 12, paragraph 34, we will come to that later.

95
00:15:09,860 --> 00:15:19,620
No, no, no, no, not today. And the H a means the commentary to demabara. So if you want to read

96
00:15:19,620 --> 00:15:31,060
a story in more detail, you can read the book Buddhist legends. I think you, you have

97
00:15:31,060 --> 00:15:43,780
to call it Buddhist legends, part three page 103 and following. And like the poison in the case

98
00:15:43,780 --> 00:15:53,940
of sin utara recited the elder chula siva. We don't know this story because the common,

99
00:15:53,940 --> 00:16:00,820
sub commentary also did not explain this. Maybe it was very well known at the time of writing

100
00:16:00,820 --> 00:16:10,740
down this commentary, but not nobody knows about this elder chula siva. And the last one, like

101
00:16:10,740 --> 00:16:18,740
the knife in the case of a novice sankeja and also demabara attack. So for this you may read the same

102
00:16:18,740 --> 00:16:37,380
book Buddhist legends, part two page 200 and 38 and following. Okay, now we call it compassion.

103
00:16:37,380 --> 00:16:48,020
Confession karuna. Now the word will be explained later. So one who wants to develop

104
00:16:48,020 --> 00:16:52,820
compassion, compassion should begin his task by reviewing the danger and lack of compassion

105
00:16:52,820 --> 00:16:58,500
and the advantage and compassion. And then he practices compassion. Now, compassion takes

106
00:16:58,500 --> 00:17:10,900
first, compassion takes beings in suffering of being in distress as object. So it is a little

107
00:17:10,900 --> 00:17:18,340
different from the object of loving kindness meditation. Loving kindness meditation takes all beings

108
00:17:18,340 --> 00:17:27,060
whether they are suffering or whether they are enjoying. So all beings. But the karuna takes

109
00:17:27,060 --> 00:17:35,380
beings who are in distress, who are in suffering first. So when we want to practice karuna

110
00:17:35,380 --> 00:17:43,620
meditation, we have to find some person who is in distress, who is suffering, mentally as well

111
00:17:43,620 --> 00:17:55,220
as physically. So we have to find such a person. Now, if we cannot find such a person, what should we do?

112
00:18:00,100 --> 00:18:05,860
Then we can arrange compassion for an evil doing person. So there will be some person who is doing

113
00:18:05,860 --> 00:18:15,380
evil. Even though he is happy right now, he is going to suffer in his next life. So he is the object

114
00:18:15,380 --> 00:18:20,820
of compassion also. So we can practice compassion to such a person.

115
00:18:20,820 --> 00:18:35,140
But there will be people so happy at that time, they could find someone who is suffering. So they may be

116
00:18:35,140 --> 00:18:42,340
having fun right now that doing evil things, but they are going to suffer the consequences of

117
00:18:42,340 --> 00:18:52,260
the wrong doing the consequences of the next life. And so you can think about that and take them

118
00:18:52,980 --> 00:19:06,100
as object of karuna. And then the comparison is given here. He is caught with stolen goods and

119
00:19:06,100 --> 00:19:11,620
then who is condemned to death, to death. And so he was given food and all these things, but he was

120
00:19:11,620 --> 00:19:22,660
not happy and so on. Now in that story, paragraph 79, 1, 2, 0, 4, 5th line,

121
00:19:23,460 --> 00:19:32,820
so giving him a hundred blows in sets of 4, actually not in sets of 4, but at every crossroad,

122
00:19:33,620 --> 00:19:40,740
crossroad are called a set of 4 because the 4, 4 roads meet there. So at every crossroad,

123
00:19:40,740 --> 00:19:49,220
he was given a hundred blows, not in sets of 4. So people get them things to eat,

124
00:19:50,180 --> 00:19:55,220
and although he may be eating and although he may look enjoying it,

125
00:19:55,220 --> 00:20:03,700
he is going to suffer death. So in the same way although he was doing evil, maybe enjoying himself

126
00:20:03,700 --> 00:20:07,860
now, he is going to suffer. So he can be the object of karuna.

127
00:20:07,860 --> 00:20:19,140
And then there may be some person who does not do evil. So who do profitable deeds,

128
00:20:20,260 --> 00:20:26,020
but he may be overtaken by one of the kinds of ruin beginning with the ruin of health,

129
00:20:27,700 --> 00:20:34,580
ruin of relatives or ruin of property and so on. So he can also be an object of karuna meditation.

130
00:20:34,580 --> 00:20:42,740
So he does not do evil things, but who does meritorious, they could things, but he may be

131
00:20:43,780 --> 00:20:50,260
poor in health, he may have disease, or he may have lost his relatives, or he may have lost his

132
00:20:50,260 --> 00:21:00,420
property and so on. So he can also be the object of karuna. Now in that paragraph, paragraph 81,

133
00:21:00,420 --> 00:21:13,300
second of that, and here too, the what too is misplaced. That should go after he

134
00:21:14,900 --> 00:21:24,180
down one to the food line, he too deserves the meditative compassion. Not only the person who

135
00:21:24,180 --> 00:21:31,940
does evil, he also deserves compassion. So he too deserves the meditative compassion.

136
00:21:39,380 --> 00:21:52,340
Then, even with no such ruin, thus in reality, he is happy.

137
00:21:52,340 --> 00:22:03,300
Now, there is a person who does good, and he has not been overtaken by anyone of the ruins.

138
00:22:03,300 --> 00:22:13,700
So he is well off in prosperity. What do you do with him? You can still practice karuna towards him

139
00:22:13,700 --> 00:22:21,460
again, because in reality, he is unhappy, because he is not exempt from suffering of the realm

140
00:22:21,460 --> 00:22:28,580
of becoming. So he is not exempt from suffering old age, suffering disease, suffering dead at the end,

141
00:22:28,580 --> 00:22:37,620
in the end. And so, such a person can also be the object of karuna meditation.

142
00:22:37,620 --> 00:22:49,780
So, in fact, any being, anybody can be the object of karuna meditation, depending on how

143
00:22:49,780 --> 00:22:58,740
you regard and how you forget it. But it is better to find a person who is in real distress,

144
00:22:58,740 --> 00:23:06,420
who is in real suffering, and practice karuna to him. If you cannot find such a person, then

145
00:23:06,420 --> 00:23:15,700
the other person. And here, also, there is a breaking tone of barriers, and then practicing

146
00:23:15,700 --> 00:23:24,100
meditation until one gets karuna. This meditation can also lead to the attainment of third

147
00:23:24,100 --> 00:23:32,980
karuna or fourth karuna. The next one is gladness, moodita. So, since it is gladness,

148
00:23:32,980 --> 00:23:40,100
its object must be people who are in prosperity, who are in heaviness. So, beings who are in

149
00:23:40,100 --> 00:23:51,860
heaviness are the objects of moodita meditation. And you practice this to

150
00:23:51,860 --> 00:24:01,780
be a person, and so on. Now, a boon companion, what is a boon companion?

151
00:24:01,780 --> 00:24:16,980
The part of what is santa sahaya, santa sahaya, may literally mean santa means

152
00:24:20,740 --> 00:24:28,580
it's heaven, it's drinking place. So, santa sahaya means it's drinking company or something,

153
00:24:28,580 --> 00:24:36,020
that means a very intimate and very kind of what's that person.

154
00:24:37,860 --> 00:24:44,340
Do you use this boon companion? Well, I don't think santa doon is something like lucky or a gift

155
00:24:44,340 --> 00:24:53,220
or something like that. But a boon companion is not a phrase, a boon is all English for something

156
00:24:53,220 --> 00:24:58,420
like lucky or a gift or something. The high racing would be a good one now, isn't it? You had a

157
00:24:58,420 --> 00:25:05,540
boon companion, you think it was a very secure fan. Is that another another friendship? Well,

158
00:25:05,540 --> 00:25:10,900
a French, I was thinking, a French is a bomb he wants or something or a high liver or a drink

159
00:25:10,900 --> 00:25:25,700
or something, that's the closest, but that's French. But actually, you had drinking companion mix,

160
00:25:25,700 --> 00:25:50,980
makes a lot of sense. And when you practice, okay, when you practice compression,

161
00:25:50,980 --> 00:25:59,380
what you do is this being has indeed been reduced to misery. If only he could be free from this

162
00:25:59,380 --> 00:26:07,540
suffering, that is how you practice compassion and that is on phase 340 and paragraph 78.

163
00:26:08,260 --> 00:26:16,740
So, when you practice corona, you say, actually, may he be free from suffering, may he get free

164
00:26:16,740 --> 00:26:24,180
from this suffering. So, that is the way of practicing compassion towards beings.

165
00:26:26,820 --> 00:26:36,580
And Mudita, what do you say? This being is indeed glad, how good, how excellent that is Mudita.

166
00:26:36,980 --> 00:26:42,420
So, when you practice Mudita, you say, just he is happy, he is in prosperity, he is, how good,

167
00:26:42,420 --> 00:26:48,980
how excellent. So, that is how you practice Mudita meditation. But there is, there is another

168
00:26:48,980 --> 00:26:59,700
sentence, I think we will meet sometime later. So, when you want to practice Mudita meditation,

169
00:26:59,700 --> 00:27:06,100
you choose a person who is in happiness, who is in prosperity and he is in happiness and

170
00:27:06,100 --> 00:27:14,500
prosperous, how good, how excellent, like that. And here, if he is going companion or the

171
00:27:14,500 --> 00:27:20,020
dear person was happy in the past, but it is now unlucky and unfortunate, then gladness can still

172
00:27:20,020 --> 00:27:28,500
be aroused by remembering his past happiness or in the future, you will again enjoy some

173
00:27:28,500 --> 00:27:38,980
of the success and so on. So, you can still practice Mudita towards that person and then

174
00:27:39,860 --> 00:27:46,180
and then breaking down of barriers and so on. So, this meditation also leads to third

175
00:27:46,180 --> 00:27:55,540
Jana or full Jana. The last one, equanimity, Upika. One who wants to develop equanimity

176
00:27:55,540 --> 00:28:01,860
must have already obtained the triple or quadruple Jana in loving kindness. That is, if you want

177
00:28:01,860 --> 00:28:11,380
to attain Jana through this meditation, you must have, you first attained three Janas or four Janas

178
00:28:11,380 --> 00:28:22,260
by the practice of loving kindness or compassion or gladness. He should emerge from the third Jana

179
00:28:22,260 --> 00:28:28,180
in the full full reckoning after he has made it familiar and he should see danger in the former

180
00:28:28,180 --> 00:28:33,860
three divine abidings because they are linked with attention given to beings enjoyment

181
00:28:34,740 --> 00:28:40,820
or given to attachment to beings in the way beginning, may, day, day, day and so on.

182
00:28:43,380 --> 00:28:49,780
And because resentment and approval are near and because their association with joy is growth,

183
00:28:49,780 --> 00:28:57,460
actually, because they are growth due to association with joy, not that association with joy

184
00:28:57,460 --> 00:29:03,460
is growth, but they are growth. They are said to be growth because they are associated with joy.

185
00:29:03,460 --> 00:29:13,540
There is Suka with them. So, they are said to be growth and he should also see the advantage

186
00:29:13,540 --> 00:29:21,620
in equanimity because it is peaceful and so on. So, how do you practice equanimity towards people?

187
00:29:21,620 --> 00:29:29,300
What do you say? It is not given here.

188
00:29:29,300 --> 00:29:44,340
We will need it later. You see just beings have come as they are property,

189
00:29:46,340 --> 00:29:55,780
just that. So, it is, yeah, it is karma. Yes. Because being suffering or

190
00:29:55,780 --> 00:30:01,140
enjoy according to their karma. So, it is a karma. beings have karma as they are property,

191
00:30:01,140 --> 00:30:14,340
as they are matrix, as they are relative and so on. So, thinking of the law of karma

192
00:30:14,340 --> 00:30:27,940
is one way of practicing upika meditation. Therefore, he should arise equanimity towards the neutral

193
00:30:27,940 --> 00:30:35,060
person and then we already started. Then through the neutral one, I think those should go.

194
00:30:35,700 --> 00:30:42,100
We do not need them. Through the neutral one. So, then he should break down the barriers

195
00:30:42,100 --> 00:30:48,020
in each case between the three people that is the dear person, then the boom confident and then

196
00:30:48,020 --> 00:30:55,060
the hostile person and lastly himself. And he should cultivate that sign, develop and repeatedly

197
00:30:55,060 --> 00:31:03,540
practice it. So, here, since he has attained three genres or four genres with the mitta,

198
00:31:03,540 --> 00:31:14,180
meditation and others, here he reaches the full genre or the fifth genre. But how

199
00:31:14,180 --> 00:31:18,900
then does this arise in one in whom the third genre has already arisen on the basis of the

200
00:31:18,900 --> 00:31:30,260
advocacy and etcetera, it does not. Now, not anybody who has gained genre with other meditation

201
00:31:31,140 --> 00:31:39,140
can attain the full genre of fifth genre through upika. So, he has to, he has to

202
00:31:39,140 --> 00:31:49,300
have gained genre through mitta or cruna or muddida meditation so that he can gain to the

203
00:31:49,300 --> 00:31:55,780
full genre or the fifth genre through the last one through upika meditation. So,

204
00:32:00,660 --> 00:32:05,380
does this arise in one in whom the third genre has already arisen on the basis of the

205
00:32:05,380 --> 00:32:11,300
advocacy in the etcetera? It does not. Why not? Because the dissimilarity of the object.

206
00:32:13,620 --> 00:32:18,820
If you get, say, first second third genre taking the earth-cassina as object,

207
00:32:20,660 --> 00:32:27,700
earth disk as object, then the object of your genre is the earth disk. But here, the object of your

208
00:32:27,700 --> 00:32:37,700
genre must be beings, the concept being. So, there is difference of object because of the dissimilarity

209
00:32:37,700 --> 00:32:44,340
of the object. It arises only in one in whom the third genre has arisen on the basis of loving

210
00:32:44,340 --> 00:32:57,700
kindness etcetera. So, that is the end of the four divine abidings and now comes the general

211
00:32:58,580 --> 00:33:07,460
exposition. The first is the meanings, meaning of the word mitta, cruna, muddida, and pupika.

212
00:33:07,460 --> 00:33:14,580
So, as to the meaning, firstly of loving kindness, compassion, gladness, and equanimity,

213
00:33:14,580 --> 00:33:22,740
it fattens, thus it is loving kindness. Now, I think it is better to say it is sticky,

214
00:33:24,740 --> 00:33:33,220
thus it is loving kindness. The pali word mittity really means it sticks. So, it is sticky,

215
00:33:33,220 --> 00:33:48,020
thus it is loving kindness mitta. It is glutinous, not solvent because when you have mitta,

216
00:33:48,020 --> 00:33:58,820
you are as it were true to the person. So, it is glutinous, it is convenient, not solvent.

217
00:33:58,820 --> 00:34:05,300
Also, it comes about with respect to a friend, mitta, or it is behavior towards a friend.

218
00:34:06,420 --> 00:34:17,620
So, the first explanation takes the word mitta to come from a root, meat, and a suffix tah,

219
00:34:18,180 --> 00:34:26,420
but the second one, beginning with also, it comes about. So, the second one takes the word mitta

220
00:34:26,420 --> 00:34:37,060
to come from the word mitta, M-I-T-T-E. So, M-I-T-T-E is changed to M-E-T-T-E by a grammatical rule.

221
00:34:38,740 --> 00:34:50,180
So, the word mitta is derived first from a root and a suffix, and then the second from a word

222
00:34:50,180 --> 00:34:59,700
and a suffix. The second one is karuna. So, karuna is explained here as suffering in others,

223
00:34:59,700 --> 00:35:05,300
it causes, when there is suffering in others, it causes good people's hearts to be moved.

224
00:35:06,660 --> 00:35:14,180
That is right. Thus, it is compassion. Or alternatively, it converts others suffering,

225
00:35:14,180 --> 00:35:23,540
attacks, and demolishes it. Thus, it is compassion. Now, here, whether you can really

226
00:35:25,220 --> 00:35:35,140
demolish the suffering of others, it is not important, not in material. But, when karuna arises in

227
00:35:35,140 --> 00:35:46,660
one's mind, it arises in this manner, in this mode, wanting to remove suffering from others.

228
00:35:48,980 --> 00:35:54,420
Or, alternatively, it is scattered upon those who suffer. It is extended to them by provision.

229
00:35:54,420 --> 00:36:04,580
Thus, it is compassion. So, it is a desire for beings to be free from suffering,

230
00:36:04,580 --> 00:36:10,820
to be free from distress. Next one, those endowed with it are glad. Or, itself,

231
00:36:10,820 --> 00:36:16,180
is glad. Or, it is the mere act of being glad. Thus, it is gladness. So, moody diaries,

232
00:36:17,460 --> 00:36:21,300
define as just gladness. And, then, upika, it looks on,

233
00:36:23,780 --> 00:36:30,100
edge, upika dii. A abandoning such an interestedness as thinking, may they be free from

234
00:36:30,100 --> 00:36:36,420
enmity and having recourse to a neutrality. Thus, it is equanimity. So, this equanimity is not in

235
00:36:36,420 --> 00:36:46,660
difference. Because, it takes the beings as object, and it is just impartiality.

236
00:36:50,180 --> 00:36:57,860
Not just regarding them altogether. And then, characteristics. The characteristic of loving

237
00:36:57,860 --> 00:37:09,380
kindness is promoting the aspect of welfare. That means, actually,

238
00:37:13,220 --> 00:37:23,460
occurring in the mode of desiring welfare. If function is to prefer welfare, it is manifested

239
00:37:23,460 --> 00:37:34,980
as the removal of annoyance. Or, I just want to say ill will. Its proximate cause is seeing

240
00:37:34,980 --> 00:37:42,580
lovableness in beings. It succeeds when it makes ill will subside. And, it fails when it reduces

241
00:37:42,580 --> 00:37:54,980
selfish affection. Suffrage is put in a parenthesis. It is the unselfish affection.

242
00:37:54,980 --> 00:38:10,820
But, here, what is mean is attachment. So, if you have attachment or lower than you feel.

243
00:38:13,700 --> 00:38:23,540
And, compassion is actually desire to remove suffering from others. So, promoting the aspect of

244
00:38:23,540 --> 00:38:28,660
a living suffering. Its function presides and not bearing others suffering. It is manifest that

245
00:38:28,660 --> 00:38:35,700
is non-quiality. Its proximate causes seeing helplessness in those overwhelmed by suffering. It

246
00:38:35,700 --> 00:38:41,460
succeeds when it makes cruelty subsides. And, it fails when it produces sorrow. Now, it is very important.

247
00:38:42,180 --> 00:38:48,420
It fails when it produces sorrow. So, when you practice confession, you just stop at

248
00:38:48,420 --> 00:38:59,940
compassion and you don't go into a feeling sorry. That is very important. Because, when we see

249
00:38:59,940 --> 00:39:08,500
some people say injured by some person, we not only feel sorry for them, but we are angry with

250
00:39:08,500 --> 00:39:18,420
the person who inflict pain to them. So, when we do that, we are going beyond the scope of

251
00:39:18,420 --> 00:39:25,540
compassion. So, we are getting into Akusala and wholesome mental states. So, but,

252
00:39:25,540 --> 00:39:30,420
compassion is wholesome mental state. They are highly developed wholesome mental state.

253
00:39:30,420 --> 00:39:40,500
So, when we practice compassion, we just have this desire for them to be free from suffering. But,

254
00:39:40,500 --> 00:39:51,620
we cannot say that we suffer with them. Because, if we suffer with them, we have un-wholesome

255
00:39:52,660 --> 00:39:59,300
mental states in our mind. So, we do not suffer with them, but we have compassion for them. We

256
00:39:59,300 --> 00:40:06,500
sympathize with them. So, that is important in Karuna. Because, many, many people confuse

257
00:40:07,460 --> 00:40:14,900
Karuna with this with this enemy of sorrow. Glateness is characterized as

258
00:40:14,900 --> 00:40:21,380
gladdening produced by other success. Its function resides in being an envious. It is manifested

259
00:40:21,380 --> 00:40:28,180
as the elimination of versioned border. So, we were talking about a versioned and border on our way

260
00:40:28,180 --> 00:40:38,660
here. The part we would use is R, R, T. So, R, T means delight and R means not. So, non-delight

261
00:40:38,660 --> 00:40:51,380
in other peoples, other beings, success, etc. And, I do not think we can call it border.

262
00:40:51,380 --> 00:41:04,900
And, a version also might have said it, they do not do here. Can you think of another word

263
00:41:04,900 --> 00:41:29,780
which means non-delight? It is the opposite of envy, actually. When you envy somebody,

264
00:41:29,780 --> 00:41:36,180
you are not pleased with his success or his prosperity. So, you do not think delight in him

265
00:41:37,140 --> 00:41:46,260
in his success. So, the word R, D here means debt, not taking delight in other peoples,

266
00:41:46,260 --> 00:41:54,740
success and so on. It succeeds when it makes non-delighted subsides and it fails in when it produces

267
00:41:54,740 --> 00:42:03,380
merriment, that is merriment with attachment. Incordimity is characterized as promoting

268
00:42:03,380 --> 00:42:19,700
things. Yes, sympathetic joy. Yes, sympathetic joy and also some translated as altruistic joy.

269
00:42:19,700 --> 00:42:31,780
That may be better sympathetic joy because gladness can mean any kind of gladness.

270
00:42:35,060 --> 00:42:44,260
But, maybe here the other wanted to be literal. So, Buddha just means gladness, but

271
00:42:44,260 --> 00:42:52,180
I think the sympathetic joy is a better word for it.

272
00:42:52,180 --> 00:42:57,060
Condemity is characterized as promoting the aspect of neutrality to what beings and so on.

273
00:42:57,060 --> 00:43:04,980
Now, it is proximate because it is seeing ownership of deeds, karma, thus beings are owners of their deeds.

274
00:43:04,980 --> 00:43:09,460
Whose, if not theirs, is the choice by which they will become happy.

275
00:43:09,460 --> 00:43:17,060
Or, we will get free from suffering or we will not fall away from the success they have reached.

276
00:43:17,060 --> 00:43:26,340
Now, we will get free from suffering refers to karuna, compassion and we will not fall away from

277
00:43:26,340 --> 00:43:33,460
the success they have reached, refers to mudita. So, when you practice mudita, you can say,

278
00:43:33,460 --> 00:43:39,540
may they not fall away from this prosperity, from this success or from this happiness.

279
00:43:39,540 --> 00:43:48,260
You can say like that too. On the other page, we might be saying, this being is indeed clear,

280
00:43:48,260 --> 00:43:56,340
how good, how excellent. So, you can see to yourself that we are, you can say, may he not fall away

281
00:43:56,340 --> 00:44:03,460
from the present prosperity or present happiness or success. It succeeds when it makes resentment and

282
00:44:03,460 --> 00:44:12,500
approvals subsides. Resentment is low bar and approval, sorry, resentment is dosa and approval

283
00:44:12,500 --> 00:44:17,380
is low bar. And it fails when it produces the equanimity of unknowing.

284
00:44:17,380 --> 00:44:26,100
Accanimity of unknowing really means indifference of ignorance, just ignorance. And the purpose,

285
00:44:27,060 --> 00:44:36,820
the general purpose of this will divine abiding is the bliss of insight and an excellent form of

286
00:44:36,820 --> 00:44:44,820
future existence. That peculiar to each is respectively the wording of of

287
00:44:44,820 --> 00:44:57,620
goodwill and so on. So, it is, this paragraph is not difficult to understand. So, next

288
00:44:57,620 --> 00:45:06,980
paragraph, the near and far enemies. Now, they have, each one has two kinds of enemies near enemy

289
00:45:06,980 --> 00:45:15,700
and far enemy. The divine abiding of loving kindness is greed as a near enemy. So, when you,

290
00:45:15,700 --> 00:45:20,180
that is why it is very important when you practice loving kindness, you do not have attachment

291
00:45:20,180 --> 00:45:28,580
or grief to that person. Sometimes, they are very similar. And so, you may take one for the other.

292
00:45:28,580 --> 00:45:40,980
So, the near enemy is greed or attachment. Since both share in saving, seeing good things,

293
00:45:40,980 --> 00:45:48,100
seeing virtues. Great behaves like a fool who keeps close by a man and it easily finds an

294
00:45:48,100 --> 00:45:52,900
opportunity. So, loving kindness should be well protected from it. And ill will, which is

295
00:45:52,900 --> 00:46:00,180
dissimilar to the similar greed. It is its far enemy, like a fool as calm. What is that? As

296
00:46:00,180 --> 00:46:06,500
constant, in a rock wilderness. So, loving kindness must be practiced free from fear of death or

297
00:46:06,500 --> 00:46:12,820
free from danger of death. For it is not possible to practice loving kindness and feel anger simultaneously.

298
00:46:14,660 --> 00:46:20,740
Confession has grief based on the home life as its near enemy, that is, feeling sorry,

299
00:46:20,740 --> 00:46:25,780
since both share and seeing fear. Such grief has been described in the way of beginning and so on.

300
00:46:25,780 --> 00:46:55,620
And cruelty, which is dissimilar to the similar grief is its far enemy. So, it is near enemy.

