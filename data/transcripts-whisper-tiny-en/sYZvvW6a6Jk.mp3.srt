1
00:00:00,000 --> 00:00:04,000
.

2
00:00:04,000 --> 00:00:08,000
..

3
00:00:08,000 --> 00:00:12,000
..

4
00:00:12,000 --> 00:00:16,000
..

5
00:00:16,000 --> 00:00:20,000
...

6
00:00:20,000 --> 00:00:24,000
...

7
00:00:24,000 --> 00:00:28,000
..

8
00:00:28,000 --> 00:00:32,000
..

9
00:00:32,000 --> 00:00:36,000
..

10
00:00:36,000 --> 00:00:38,000
..

11
00:00:38,000 --> 00:00:40,000
..

12
00:00:40,000 --> 00:00:44,000
..

13
00:00:44,000 --> 00:00:48,000
..

14
00:00:48,000 --> 00:00:52,000
..

15
00:00:52,000 --> 00:00:54,000
..

16
00:00:54,000 --> 00:00:56,000
..

17
00:00:56,000 --> 00:01:00,000
...

18
00:01:00,000 --> 00:01:00,000
..

19
00:01:00,000 --> 00:01:04,000
..

20
00:01:04,000 --> 00:01:12,000
..

21
00:01:12,000 --> 00:01:16,000
..

22
00:01:16,000 --> 00:01:20,000
..

23
00:01:20,000 --> 00:01:21,000
..

24
00:01:21,000 --> 00:01:22,000
..

25
00:01:22,000 --> 00:01:20,000
..

26
00:01:20,000 --> 00:01:22,000
..

27
00:01:22,000 --> 00:01:23,000
..

28
00:01:23,000 --> 00:01:24,000
..

29
00:01:24,000 --> 00:01:25,000
..

30
00:01:25,000 --> 00:01:25,000
..

31
00:01:25,000 --> 00:01:25,000
..

32
00:01:25,000 --> 00:01:25,000
..

33
00:01:25,000 --> 00:01:25,000
..

34
00:01:25,000 --> 00:01:18,000
.

35
00:01:25,000 --> 00:01:25,520
..

36
00:01:25,000 --> 00:01:25,000
..

37
00:01:25,000 --> 00:01:25,000
..

38
00:01:25,000 --> 00:01:25,960
..

39
00:01:25,000 --> 00:01:25,960
..

40
00:01:25,960 --> 00:01:35,960
And not just at, because they're all reasonably pertinent, but sort of unique and immediately applicable to our group.

41
00:01:39,960 --> 00:01:42,960
Hopefully both local and on the internet.

42
00:01:44,960 --> 00:01:50,960
But this one is, I think fairly unique.

43
00:01:50,960 --> 00:02:01,960
And it has, of course, it touches on some familiar themes, but it sets the mound in a fairly unique way that I think makes it important.

44
00:02:04,960 --> 00:02:13,960
So the story is, the story goes that there is this man, both of the, and the Buddha is dwelling in a place.

45
00:02:13,960 --> 00:02:20,960
I'm not really familiar with Kalda. Agutarap and Agutarap and Agutarap and...

46
00:02:22,960 --> 00:02:27,960
and there's a town named Aparna. Or Istang.

47
00:02:27,960 --> 00:02:40,760
And in the morning, he went for arms and came back after receiving whatever food people

48
00:02:40,760 --> 00:02:50,240
had thought to offer to religious people that day, as was the custom in India.

49
00:02:50,240 --> 00:02:57,880
And he went to a little grove, some wooded place and sat down at the root of a tree.

50
00:02:57,880 --> 00:03:06,360
And sat abiding peacefully.

51
00:03:06,360 --> 00:03:12,080
And then came this man, Pohtalia, which was the text called the householder.

52
00:03:12,080 --> 00:03:23,000
Pohtalia, the householder, which is important because it doesn't think of himself as

53
00:03:23,000 --> 00:03:28,760
a householder. But the text goes ahead and right away calls him a householder and says,

54
00:03:28,760 --> 00:03:37,040
he was walking and wandering for exercise. And he was in full dress, whatever that means.

55
00:03:37,040 --> 00:03:41,680
Let's see what the polly is.

56
00:03:41,680 --> 00:03:54,320
Upana, we saw Nupa, Wurano. His well-dressed. And the point is, he had householder dress on.

57
00:03:54,320 --> 00:04:04,600
And he was carrying a parasol, which is another no-no for monks. And was wearing sandals,

58
00:04:04,600 --> 00:04:12,640
which actually was a no-no against the rules. Although living in the forest, it eventually

59
00:04:12,640 --> 00:04:24,720
became a thing where even the Buddha allowed monks to wear sandals when living in the forest.

60
00:04:24,720 --> 00:04:34,160
And he came to where the Buddha was sitting. And the Buddha said, there are seats

61
00:04:34,160 --> 00:04:40,000
a householder, sit down if you like. It could sit down if you like. And it calls him

62
00:04:40,000 --> 00:04:44,440
a householder. And why this is important? Because Pohtalia doesn't think he's a householder.

63
00:04:44,440 --> 00:04:52,000
And he gets upset. Angry and displeased. He said, he's calling me a householder and he's

64
00:04:52,000 --> 00:05:00,120
stood there. And the second time the Buddha said, there are seats, a householder sit down

65
00:05:00,120 --> 00:05:11,080
if you like. Again, angry in the third time. The Buddha says, sit down if you like

66
00:05:11,080 --> 00:05:20,720
householder. I guess very angry. And he finally blows up and says, must or go to much.

67
00:05:20,720 --> 00:05:27,560
Neither fitting nor proper that you address me as householder. And the Buddha says, but

68
00:05:27,560 --> 00:05:41,160
you look like one. He doesn't say it. He says, he says it more politically or more

69
00:05:41,160 --> 00:05:50,080
elegantly than that. He says you have the aspects, marks, and signs of a householder. Basically

70
00:05:50,080 --> 00:05:55,760
you look like one. You've got all the characteristics of a householder. How am I supposed

71
00:05:55,760 --> 00:06:02,480
to address you, basically? And he says, well, that may be true. But I have given up all

72
00:06:02,480 --> 00:06:09,440
my works and cut off all my affairs. I've settled everything. So it's what people talk

73
00:06:09,440 --> 00:06:18,520
about in India. In that time, anyway, they talked about setting everything aside and going

74
00:06:18,520 --> 00:06:24,560
off and living the life of a recluse. It's not just in India. You hear about this sort

75
00:06:24,560 --> 00:06:34,560
of idea or ideal. Later on in life, you commit yourself to philosophy or religion or simple

76
00:06:34,560 --> 00:06:44,720
living. I mean, given up your work and call it retirement, right? It says, in more way,

77
00:06:44,720 --> 00:06:50,280
have you given up your works and cut off all your affairs? And Buddha is going to try

78
00:06:50,280 --> 00:06:55,240
and explain to him why that doesn't make him a recluse, someone who is left the home

79
00:06:55,240 --> 00:07:04,160
life. And he says, what he says is reasonable. He says, I've given up all my wealth, given

80
00:07:04,160 --> 00:07:10,960
it all to my children as they're inheritance, given up everything I have. Well, my wealth,

81
00:07:10,960 --> 00:07:17,160
anyway, I do not advise or blame them, but such matters, but merely live on food and clothing.

82
00:07:17,160 --> 00:07:26,240
So his kids look after him. And he's supported in whatever he needs, but he doesn't get

83
00:07:26,240 --> 00:07:34,160
involved in wealth and politics and society. So it's how I've given up all my works

84
00:07:34,160 --> 00:07:41,600
and cut off all my affairs. And the Buddha said, well, that's one thing. But in the

85
00:07:41,600 --> 00:07:51,000
area, they discipline of the noble ones. And he uses a fairly lofty term here, which

86
00:07:51,000 --> 00:08:02,240
he often uses to describe his community, or not just his community, but the ideal. When

87
00:08:02,240 --> 00:08:09,400
the Buddha talks about his community and his religion, he's really not thinking about

88
00:08:09,400 --> 00:08:14,320
people. I don't think well, he appears to be much more talking about trying to place it

89
00:08:14,320 --> 00:08:19,600
in terms of the ideal, not being exclusionary while you have to be Buddhist if you want

90
00:08:19,600 --> 00:08:27,160
to be a real religious person. But here he says, are you in a, I mean, it's not really

91
00:08:27,160 --> 00:08:35,680
noble, the noble ones, those who you can really call noble, their discipline is different.

92
00:08:35,680 --> 00:08:46,160
And so this peaks his interest. And he changes his tone and starts calling him venerable

93
00:08:46,160 --> 00:08:54,440
sir, but I think he says, so then what is the cutting off of affairs in the noble ones

94
00:08:54,440 --> 00:09:01,920
discipline? It's interesting. What more is there to do? What more can I do? Because this

95
00:09:01,920 --> 00:09:06,280
is really probably something that was of interest to him. He thought to himself, I get

96
00:09:06,280 --> 00:09:12,040
a cut off my affairs. And so he did what he could, but here was someone telling him that

97
00:09:12,040 --> 00:09:21,520
there's more to do, more that you can do to live a free and simple and peaceful life.

98
00:09:21,520 --> 00:09:25,960
And the Buddha said, well, there are eight things in the noble ones discipline that lead

99
00:09:25,960 --> 00:09:34,440
to the cutting off of affairs. You can really say you've cut off. And it's again, it should

100
00:09:34,440 --> 00:09:41,000
be familiar, the Buddha putting a different spin on things. So where it's not just external

101
00:09:41,000 --> 00:09:47,320
stuff. In fact, it's the kind of thing where you don't even have to necessarily leave

102
00:09:47,320 --> 00:09:54,320
the home, the household life. You can live at home and all these things, but there are

103
00:09:54,320 --> 00:10:01,440
many things that you haven't done yet that you haven't committed to anyway. And so they

104
00:10:01,440 --> 00:10:06,680
should be fairly familiar. The first one is non-killing. And abstains from killing living

105
00:10:06,680 --> 00:10:15,800
beings, abandoned that one takes only what is given, one does not take what is not given,

106
00:10:15,800 --> 00:10:26,680
one doesn't steal, one abandons, thevery. With the support of truthful speech, one abandons

107
00:10:26,680 --> 00:10:35,360
false speech. With the support of unmalicious speech, malicious speech is abandoned, one

108
00:10:35,360 --> 00:10:45,120
doesn't speak, trying to make someone upset, trying to harm someone with their speech.

109
00:10:45,120 --> 00:10:57,680
With the support of no capacity, rapacity, rapacious, rapacious nature, and greed, with

110
00:10:57,680 --> 00:11:08,960
the support of not being greedy, greed is abandoned. With the support of not being spiteful

111
00:11:08,960 --> 00:11:14,960
and scolding, spite and scolding are to be abandoned. With the support of no anger and

112
00:11:14,960 --> 00:11:20,960
irritation, anger and irritation are to be abandoned. With the support of non-arrogance

113
00:11:20,960 --> 00:11:29,520
arrogance is to be abandoned. These are the eight things stated in brief without being

114
00:11:29,520 --> 00:11:37,400
expounded in detail that lead to the cutting off of affairs in the noble ones discipline.

115
00:11:37,400 --> 00:11:48,360
So I think what he's mainly getting at is ethics or discipline, the foundation of spiritual

116
00:11:48,360 --> 00:11:59,920
life or holy life. But he's tailoring in a little bit to this guy. It seems because

117
00:11:59,920 --> 00:12:07,280
Portalea has gotten angry and he's been kind of arrogant and full of himself and stiff

118
00:12:07,280 --> 00:12:18,960
backed, been quite unrigid in his mindset. And so the Buddha seems to address that directly

119
00:12:18,960 --> 00:12:26,000
by mentioning anger and irritation and by giving those and suddenly pointing out in sort

120
00:12:26,000 --> 00:12:32,160
of a roundabout way, right, he starts with not killing and then he hits him with softly

121
00:12:32,160 --> 00:12:36,880
with it so that he feels ashamed and realizes because he couldn't just say, look, you're

122
00:12:36,880 --> 00:12:42,840
getting angry. That's bad. You're a bad person. You're not really reckless because you're

123
00:12:42,840 --> 00:12:49,160
getting angry because that would, of course, lead to further anger, more anger most likely.

124
00:12:49,160 --> 00:12:55,960
But in a soft way, a roundabout way, he's come at what was the problem and helped this man

125
00:12:55,960 --> 00:13:04,000
to realize him. Not only is he not a reckless, but he's also not really a spiritual person

126
00:13:04,000 --> 00:13:14,880
because he still gets angry. He's lacking in that. And so even more interested and quite

127
00:13:14,880 --> 00:13:22,640
impressed by the Buddha's enumeration of these eight things, he says, it would be good

128
00:13:22,640 --> 00:13:33,840
if you could explain these things to me. You've stated them in brief, but could you elaborate?

129
00:13:33,840 --> 00:13:40,520
And so the Buddha talks about them. He gives a fairly standard description. Basically,

130
00:13:40,520 --> 00:13:50,520
if you kill, this is a fetter. This is something that binds you to many things, binds

131
00:13:50,520 --> 00:14:02,600
you to the guilt of having killed and binds you to the obsession with cruelty and with

132
00:14:02,600 --> 00:14:10,400
evil, with the mind state that is discordant with happiness or disconnected from

133
00:14:10,400 --> 00:14:18,120
happiness and peace because it aims for stress and suffering. So it's a stressful mind

134
00:14:18,120 --> 00:14:30,240
state and it's a part of your personality that's going to be a cause for disturbance.

135
00:14:30,240 --> 00:14:37,080
So by abandoning it, all those taints, vexation, and fever are gone. They don't arise.

136
00:14:37,080 --> 00:14:46,760
They don't get born in hell because of it and so on. The same with taking what is given

137
00:14:46,760 --> 00:14:55,400
when you abandon taking what is not given. When you abandon trying to take things from others

138
00:14:55,400 --> 00:15:01,560
or even manipulating people to give you things, you only take what is really given of

139
00:15:01,560 --> 00:15:13,360
their own accord. It's really the epitome or the pinnacle of economy where he's basically

140
00:15:13,360 --> 00:15:20,800
distilling down the economy to the purest form and where there's no buying and selling,

141
00:15:20,800 --> 00:15:28,360
there's no coercing, there's no chasing after, there's no chasing whatsoever, even

142
00:15:28,360 --> 00:15:42,800
for requisites, things you need, you cease from seeking them out and you only, well,

143
00:15:42,800 --> 00:15:54,560
insofar as asking for them or trying to ingrati yourself with others, even working for them.

144
00:15:54,560 --> 00:15:59,480
So in the sense that it's not seeking them out at all, looking to see if they're easy

145
00:15:59,480 --> 00:16:06,920
to find, oh, here's some rags or rubbish I can take and I'll take that rubbish and live

146
00:16:06,920 --> 00:16:16,200
off of that and that's sufficient. And so there's a lot of vexation that's done away

147
00:16:16,200 --> 00:16:21,240
with when you give up economy and certainly when you give up money but more so when you

148
00:16:21,240 --> 00:16:31,280
give up needing and wanting things, of course, most extreme when you give up taking things

149
00:16:31,280 --> 00:16:40,000
that aren't given, stealing, so much trouble has gotten rid of. You give up lying with

150
00:16:40,000 --> 00:16:48,840
the giving up of lying so much distress and confusion and warped relationship with reality

151
00:16:48,840 --> 00:16:57,400
is given up. When you stop being malicious, when you stop being, right, and so here he's

152
00:16:57,400 --> 00:17:06,680
quite tailoring it to this land. When you, when you give up greed, like growing up greed,

153
00:17:06,680 --> 00:17:10,880
pointing out that just because you've given up your wealth doesn't mean you've given

154
00:17:10,880 --> 00:17:19,160
up greed, it's not so much about your situation in life because of course even a monk

155
00:17:19,160 --> 00:17:24,800
living off in the forest can be full of greed, full of desire, full of spite and anger

156
00:17:24,800 --> 00:17:29,760
and irritation and all these things. So giving them up, it's very much what we're all

157
00:17:29,760 --> 00:17:37,600
about here trying to free ourselves from craving, free ourselves from anger and irritation.

158
00:17:37,600 --> 00:17:45,520
That's what the meditation is for. In case it wasn't clear, that's what we're here for.

159
00:17:45,520 --> 00:17:53,240
Not just to live peacefully and live in a retirement but to work, to train ourselves,

160
00:17:53,240 --> 00:18:04,000
to free ourselves from evil and wholesome states. And nobody says, so these eight things

161
00:18:04,000 --> 00:18:14,240
are, are putting your affairs in order, cutting off the affairs, but I know these

162
00:18:14,240 --> 00:18:19,280
things, eight things, lead to the cutting off the affairs. This is the base, but at this

163
00:18:19,280 --> 00:18:30,680
point he says, one has sort of set one's behavior in order, cutting off of affairs hasn't

164
00:18:30,680 --> 00:18:37,040
been achieved in all ways. You're not there yet, of course. So it's just the base and

165
00:18:37,040 --> 00:18:46,040
they say, oh well then, how do you do that? And then the Buddha, of course, switches,

166
00:18:46,040 --> 00:18:51,520
at this point he's caught him and he's going to give him a deeper teaching. That's not

167
00:18:51,520 --> 00:18:55,720
relating to his behavior, but relating to something that's going to be important. And

168
00:18:55,720 --> 00:19:02,480
I think it's important to all of us. He gives a bunch of similes, similes of how we should

169
00:19:02,480 --> 00:19:11,680
understand sensual pleasure. Yes, they're all about sensual pleasure. So this is of interest,

170
00:19:11,680 --> 00:19:17,520
because sensual pleasure is of course the sticking point. So it keeps us from being

171
00:19:17,520 --> 00:19:29,720
religious, it keeps us from going the distance, we're held back by our obsession and

172
00:19:29,720 --> 00:19:47,320
our distraction by things we want, things we like, less than desired addiction.

173
00:19:47,320 --> 00:19:52,920
So he asks some questions, he says, suppose there were a dog and the dog is very hungry

174
00:19:52,920 --> 00:20:03,120
and weak and it's waiting by a butcher's shop, smells the meat, sees the meat. And then

175
00:20:03,120 --> 00:20:12,400
a butcher comes, comes out with a bone and the bone has blood smeared on it. There's no

176
00:20:12,400 --> 00:20:19,400
meat on it, but it's smeared with blood and throws it out the window, sees the dog there

177
00:20:19,400 --> 00:20:27,680
and throws it out. What's the dog going to do? It's going to leap, leap at the bone.

178
00:20:27,680 --> 00:20:41,840
No, I don't chew on it, but he asks, so do you think that dog will be satiated since

179
00:20:41,840 --> 00:20:46,560
hunger going to go away, is the weakness going to go away? I don't know, it's really

180
00:20:46,560 --> 00:20:54,040
not. It's just going to get weird and disappointed after a while and still be very hungry.

181
00:20:54,040 --> 00:21:02,560
And the Buddha said, in the same way someone who is a noble disciple thinks about this

182
00:21:02,560 --> 00:21:09,600
and remembers that the Buddha compared the blood smeared bone, sensuality to the blood

183
00:21:09,600 --> 00:21:14,600
smeared bone, something that might make you excited and happy, but will never feel it

184
00:21:14,600 --> 00:21:21,960
fulfill you. That's what it's like, it's like knowing on this bloody bone, trying to get

185
00:21:21,960 --> 00:21:31,520
full. And then he says, suppose a vulture, a hair in or a hawk, one of these vicious

186
00:21:31,520 --> 00:21:45,760
birds, birds are so, such honed killers, some birds, they're very vicious killers.

187
00:21:45,760 --> 00:21:54,600
And so one of these birds gets a piece of meat, grabs it and flies away. And it's so

188
00:21:54,600 --> 00:22:00,480
excited, but then the other vultures, herons and hawks pursue the bird with the piece

189
00:22:00,480 --> 00:22:08,160
of meat and peck it, then claw it, trying to get at the piece of meat, trying to make

190
00:22:08,160 --> 00:22:14,640
it drop the meat. What do you think if that vulture, heron or hawk does not quickly let

191
00:22:14,640 --> 00:22:19,040
go of that piece of meat, wouldn't it incur? And I wasn't going to be in big trouble,

192
00:22:19,040 --> 00:22:30,600
wouldn't die or suffer quite egregiously. And I was like, yes, for sure. And he said,

193
00:22:30,600 --> 00:22:38,080
the Buddha says, well, in the same way, a meditator really, a noble disciple remembers

194
00:22:38,080 --> 00:22:47,760
that the Buddha compared sensuality with a piece of meat held by the hawk, held by the

195
00:22:47,760 --> 00:22:53,000
bird. Because like that, it's something that leads to great stress and suffering. You

196
00:22:53,000 --> 00:22:58,560
think you've got something wonderful, but there's so much stress that comes along with

197
00:22:58,560 --> 00:23:03,880
it, the stress of having to protect it, the stress of keeping it, the stress of wanting

198
00:23:03,880 --> 00:23:15,000
it, fear of being afraid of losing it and so on. The stress of having it threatened,

199
00:23:15,000 --> 00:23:32,160
the stress of jealousy and stinginess, miserliness, the stress of wanting it, liking it.

200
00:23:32,160 --> 00:23:40,040
And so thinking about it like this, thinking about sensuality like this one, one gives

201
00:23:40,040 --> 00:23:53,360
it up. And begins to see the danger and the Buddha says he avoids the equanimity that

202
00:23:53,360 --> 00:24:00,920
is diversified based on diversity and develops the equanimity that is unified based on unity.

203
00:24:00,920 --> 00:24:15,560
So what he means here is that there's a sort of an equanimity that comes from getting

204
00:24:15,560 --> 00:24:23,440
what you want. But because it's based in the diversity of experience, meaning experience

205
00:24:23,440 --> 00:24:28,560
is so much more than just you getting what you want. There are times of not getting

206
00:24:28,560 --> 00:24:35,280
what you want, losing what you like. And because it's diverse like that, your equanimity

207
00:24:35,280 --> 00:24:42,520
means your contentment, your contentment when you have what you want is going to be threatened,

208
00:24:42,520 --> 00:24:50,080
it will be temporarily satisfied. But it's based in diversity means it's based in the

209
00:24:50,080 --> 00:24:56,600
diverse nature of experience. Now the other kind of equanimity is not dependent on the

210
00:24:56,600 --> 00:25:01,200
changing experience. It's not dependent on this experience or that experience. So it's based

211
00:25:01,200 --> 00:25:12,160
on unity. It's fixed. It has a singular range of outcomes. It means it's always equanimous.

212
00:25:12,160 --> 00:25:18,960
Once you're able to see reality as simply experience as arising and ceasing, then your

213
00:25:18,960 --> 00:25:26,560
equanimity is unified. I think what is the description here. It's quite a unique

214
00:25:26,560 --> 00:25:33,560
term. It's not something you hear the Buddha say often. And he gives more similes. He's

215
00:25:33,560 --> 00:25:41,760
not done yet each one. He says this again repeating this refrain. So the next one is a

216
00:25:41,760 --> 00:25:50,680
grass torch. Suppose one takes a blazing grass torch and walks into the wind. So if one

217
00:25:50,680 --> 00:25:58,680
doesn't let go of that grass torch quickly, wouldn't it burn their hand or their arm?

218
00:25:58,680 --> 00:26:05,000
So it's like a fuse, really. This torch is like a fuse walking into the wind. If you

219
00:26:05,000 --> 00:26:11,720
don't let go of it, it's going to burn your hand. If you don't let go of the things we desire,

220
00:26:11,720 --> 00:26:17,080
we're setting ourselves up for stress and suffering when things change when we're not

221
00:26:17,080 --> 00:26:26,160
able to get what we want. Then he says, suppose there were a charcoal pit, deeper than

222
00:26:26,160 --> 00:26:31,120
a man's full height glowing with coals without flame or smoke. It was so hot it wasn't

223
00:26:31,120 --> 00:26:41,000
even flaming, I think it's a plane. White hot. Then a man came who wanted to live and not

224
00:26:41,000 --> 00:26:48,120
to die. He wanted pleasure and recoil from pain and two strong men grabbed him and dragged

225
00:26:48,120 --> 00:26:57,040
him towards the charcoal pit. What do you think? Wouldn't he twist his body, trying to

226
00:26:57,040 --> 00:27:03,600
get away from these two strong men? But it's a sensuality like a sensuality of the charcoal

227
00:27:03,600 --> 00:27:09,560
pit. It's quite dismal, a dismal view of it. But it's about the effects of addiction.

228
00:27:09,560 --> 00:27:19,240
Right? Trying, struggling to get away from your disappointment, your withdrawal, being

229
00:27:19,240 --> 00:27:29,100
tormented, being tormented really by the fear of consequence, the fear of losing, constantly

230
00:27:29,100 --> 00:27:36,400
tormented by anything that gets anything that threatens your equanimity, which threatens

231
00:27:36,400 --> 00:27:43,360
your contentment. Yes, I have all the things that I want, but there's always a potential

232
00:27:43,360 --> 00:27:49,480
for losing them. Right? When your happiness depends on things, losing them is always something

233
00:27:49,480 --> 00:27:57,440
that's going to be worrisome. So the same way this charcoal pit is worrisome for this

234
00:27:57,440 --> 00:28:05,600
man to say the least. Suppose someone dreamt about lovely parks, lovely groves, lovely

235
00:28:05,600 --> 00:28:12,960
meadows and lovely lakes. But then they woke up and it was all gone. It wasn't real.

236
00:28:12,960 --> 00:28:19,760
And he says, central pleasures have been likened to a dream by the blessed one, by the

237
00:28:19,760 --> 00:28:27,200
Buddha. And seeing this, and this is really it. I mean, it is a dream. You think, wow,

238
00:28:27,200 --> 00:28:31,760
I'm so happy with it, I get what I want. And then something happens and you lose it

239
00:28:31,760 --> 00:28:37,560
and you realize that was not true happiness. I wasn't really a happy person. I was just

240
00:28:37,560 --> 00:28:43,960
happy while I got what I want. And when I woke up from it, I realized I'm no better often

241
00:28:43,960 --> 00:28:50,200
before I'm worse often, because now I'm wishing craving for those things I can't have,

242
00:28:50,200 --> 00:28:59,680
like a dream. Or suppose a man borrowed goods, a person borrowed some goods, a fancy carriage,

243
00:28:59,680 --> 00:29:10,120
fine jewelerings, and proceeded to parade around. It's like you lease a car, this nice

244
00:29:10,120 --> 00:29:16,280
car and borrow some, well, borrowed is even better. You borrow somebody else's car, borrow

245
00:29:16,280 --> 00:29:25,440
someone's nice fancy suit and you go to the market and go to the mall. And when people

246
00:29:25,440 --> 00:29:31,880
see them, wow, this is a rich man. This is how the rich enjoy their wealth. But then

247
00:29:31,880 --> 00:29:37,040
the owners, when they saw this, they would take back their things. When they take back

248
00:29:37,040 --> 00:29:41,680
their things, you realize, well, it wasn't real. But when it's the state wasn't real,

249
00:29:41,680 --> 00:29:51,160
it was an illusion. And it's a sensuality is like that. Sensuality is like borrowed goods,

250
00:29:51,160 --> 00:29:59,440
you borrow them. So you get to taste some kind of contentment. It's like a taste of enlightenment

251
00:29:59,440 --> 00:30:07,480
in a sense. You say, wow, I'm so free from one, free from need. And then from crashing

252
00:30:07,480 --> 00:30:18,680
down, because it's based not on, it could not on its wisdom, but it's based on satisfaction

253
00:30:18,680 --> 00:30:24,000
on getting what you want, which is, of course, not tenable. As long as you have wants,

254
00:30:24,000 --> 00:30:29,160
you'll always be subject to not getting what you want.

255
00:30:29,160 --> 00:30:52,800
Okay, and here's a more complicated one you have two men. Once there is a fruit tree,

256
00:30:52,800 --> 00:31:01,640
and somebody goes to the fruit tree looking for fruit, and sees, oh, here's this tree

257
00:31:01,640 --> 00:31:04,920
has so much fruit in it, but none of the fruit has fallen to the ground, so they climb

258
00:31:04,920 --> 00:31:11,960
the tree. And climbs up in the tree, eats some of the fruit, put some in his bag, and

259
00:31:11,960 --> 00:31:19,600
he's so happy up in the tree, thinking he's got himself in a situation that is just wonderful

260
00:31:19,600 --> 00:31:24,800
sitting up in this tree, eating the fruit. Then a second man comes, and he also wants

261
00:31:24,800 --> 00:31:29,360
fruit, and he looks up in the tree, doesn't see the man, but he sees the fruit, and he

262
00:31:29,360 --> 00:31:34,280
says, none of the fruit has fallen. What do I do? And he takes out an axe, and he starts

263
00:31:34,280 --> 00:31:39,440
to cut down the tree. I don't know that this sort of thing would happen in real life,

264
00:31:39,440 --> 00:31:47,200
but it's an important point here. What do you think if the second man doesn't come

265
00:31:47,200 --> 00:31:52,480
down out of his tree, when the tree falls, he was going to break his hand or his foot,

266
00:31:52,480 --> 00:31:57,240
or some other part of his body. It's kind of a silly situation that probably wouldn't

267
00:31:57,240 --> 00:32:03,520
happen in real life, but oh, it may, I suppose, but the point is not that. The point

268
00:32:03,520 --> 00:32:09,720
is that this is imagery is how we should think about sensuality. Here we are up in our

269
00:32:09,720 --> 00:32:16,880
trees, but we're up in a tree. We're not standing on firm ground. We're in a precarious

270
00:32:16,880 --> 00:32:31,680
situation, such that if it all crashes, if we can't get what we want, then if some situation

271
00:32:31,680 --> 00:32:36,120
comes, then we're not going to be able to get what we want, and our happiness, our happy

272
00:32:36,120 --> 00:32:44,880
state of being up in the tree is going to be threatened and lead to great fear and lost,

273
00:32:44,880 --> 00:32:51,880
it's going to come crashing down like the tree. It was said when one realizes this, this

274
00:32:51,880 --> 00:33:01,800
is a, I mean these similes are very powerful imagery, but it's not so much, I mean a part

275
00:33:01,800 --> 00:33:08,800
of it is hearing that the Buddha has said this and reflecting on it, but a bigger part

276
00:33:08,800 --> 00:33:14,800
is realizing that yes, indeed, realizing for yourself with wisdom, yes, indeed, that's how it

277
00:33:14,800 --> 00:33:26,400
is. And so then he talks about what happens when one realizes this, because sensuality,

278
00:33:26,400 --> 00:33:32,800
of course, is a very, again, a very big part of the problem. And in general craving, wanting

279
00:33:32,800 --> 00:33:39,720
for things is really the whole problem. So when one changes and becomes more mindful and

280
00:33:39,720 --> 00:33:45,560
gains equal inimity about these things, then many things can come, when he talks about magical

281
00:33:45,560 --> 00:33:57,040
powers, one can gain with a unified mind and eventually with this mindfulness whose purity

282
00:33:57,040 --> 00:34:05,960
is due to equanimity, one realizes for oneself with direct knowledge enters and abides

283
00:34:05,960 --> 00:34:13,400
in the deliverance of mind and deliverance of wisdom. So true freedom of mind and wisdom

284
00:34:13,400 --> 00:34:21,440
that are tameless with the destruction of the tanes. So by giving up, by giving up

285
00:34:21,440 --> 00:34:31,640
ones, one's really one's attachment or one's associated mind states, mind states that create

286
00:34:31,640 --> 00:34:43,440
association, meaning associations of wanting and liking and aversion and hatred and so on.

287
00:34:43,440 --> 00:34:48,760
By giving those up through seeing things clearly, seeing that there's no reason to give rise

288
00:34:48,760 --> 00:34:58,240
to that. There's no benefit to it and there's great harm and great danger. Then one can

289
00:34:58,240 --> 00:35:07,040
be said to have settled ones of fairs, he says. And then he says, what do you think

290
00:35:07,040 --> 00:35:14,200
householder? Do you see in yourself any cutting off of affairs like this, cutting off of affairs?

291
00:35:14,200 --> 00:35:19,640
When it is achieved entirely and always this cutting off of affairs, settling of fairs,

292
00:35:19,640 --> 00:35:28,240
I think we'd probably more say in English, settling of fairs in the noble ones discipline.

293
00:35:28,240 --> 00:35:40,400
And he says, one day, who am I that I should possess any cutting off of affairs? For I'm

294
00:35:40,400 --> 00:35:45,360
very far from that anyway. And then he expresses how impressed he is, which is a common

295
00:35:45,360 --> 00:35:51,280
thing after listening to the Buddha talk. And then he takes refuge in the Buddha and

296
00:35:51,280 --> 00:35:58,960
in his dispensation. So again, an interesting suit to something that I think is beneficial

297
00:35:58,960 --> 00:36:05,000
for us, especially those similes, the Buddha talks about the mouseware, but here is one

298
00:36:05,000 --> 00:36:15,960
place where they're all gathered together and illustrated quite nicely. There we go. Many

299
00:36:15,960 --> 00:36:23,400
ways we can understand sensuality. So that's the number for tonight. Thank you all for

300
00:36:23,400 --> 00:36:39,320
tuning in. Have a good night.

