1
00:00:00,000 --> 00:00:06,760
Okay, I'm thinking about teaching elementary students meditation especially those who have emotional

2
00:00:06,760 --> 00:00:11,720
disturbances such as strong anger, defiant hyperactivity.

3
00:00:11,720 --> 00:00:15,440
What is the best technique are they too young to learn?

4
00:00:15,440 --> 00:00:23,120
Okay, I probably should do a separate video just to say this but let me just say it again

5
00:00:23,120 --> 00:00:29,440
and again and all of them because it's something that I believe quite strongly and I've

6
00:00:29,440 --> 00:00:38,560
said it now tonight a few times is that don't go where the real problems are, don't.

7
00:00:38,560 --> 00:00:47,040
I would recommend against trying to deal with people who have serious problems.

8
00:00:47,040 --> 00:00:56,200
It's very altruistic to do so but it's missing one important, very important point.

9
00:00:56,200 --> 00:01:12,520
The amount of effort you put in to helping someone who is in a very bad state is compared

10
00:01:12,520 --> 00:01:17,360
with the benefit of helping them, the potential benefit.

11
00:01:17,360 --> 00:01:22,480
The effort level is incredibly high.

12
00:01:22,480 --> 00:01:27,880
The amount of benefit that you can give to them is very low.

13
00:01:27,880 --> 00:01:38,560
There's no miracles, a person who has a lot of mental trouble is not likely to in most cases

14
00:01:38,560 --> 00:01:43,920
become fully enlightened being in this life.

15
00:01:43,920 --> 00:01:47,880
It just doesn't happen this month that I was talking about as an example of that.

16
00:01:47,880 --> 00:01:52,160
He practiced meditation, he's been practicing, if he's still alive, he's been practicing for

17
00:01:52,160 --> 00:01:56,920
20 years or something and he's still taking baby steps.

18
00:01:56,920 --> 00:02:01,000
He's still blowing up, he's still got the same problems.

19
00:02:01,000 --> 00:02:03,200
It's a very, very slow path.

20
00:02:03,200 --> 00:02:09,840
Everyone who practices meditation is going to go through the same conditions again and

21
00:02:09,840 --> 00:02:17,840
again and again until finally they're worked out, worn down, broken apart and discarded.

22
00:02:17,840 --> 00:02:32,160
But until that point they will come back again and again and again until they become weaker

23
00:02:32,160 --> 00:02:38,560
and weaker until they disappear.

24
00:02:38,560 --> 00:02:43,960
This is, you get a far less return for your effort.

25
00:02:43,960 --> 00:02:49,840
If you take the same amount of effort and you apply it to people who are exceptional

26
00:02:49,840 --> 00:03:02,440
gifted, brilliant, focused, people who have potential and you teach them meditation.

27
00:03:02,440 --> 00:03:13,720
First of all, very little effort, second of all, incredible results that they will

28
00:03:13,720 --> 00:03:23,840
be, they will grasp it much easier, pick it up and in many cases it's like planting

29
00:03:23,840 --> 00:03:24,840
a seed.

30
00:03:24,840 --> 00:03:35,360
You don't have to make it grow, it grows by itself, it goes viral because they pick it

31
00:03:35,360 --> 00:03:37,480
up and they become you.

32
00:03:37,480 --> 00:03:43,760
They become the person who is spreading the teaching.

33
00:03:43,760 --> 00:03:55,400
You help such people who are teachers and those teachers go and help and teach.

34
00:03:55,400 --> 00:04:04,480
So at the very least what you'll get theoretically is a bunch of people who are very

35
00:04:04,480 --> 00:04:11,800
gifted who will then go out and pass along what you've passed along to people who are

36
00:04:11,800 --> 00:04:19,680
less gifted and on and on down the line and there will be people who are actually working

37
00:04:19,680 --> 00:04:31,280
with these kind of people who have serious problems and they will have based on the teachings

38
00:04:31,280 --> 00:04:36,600
that you have been giving on a higher level, they will do the grunt work of dealing with

39
00:04:36,600 --> 00:04:48,400
these people and it will create more benefit in the long run, even for those people

40
00:04:48,400 --> 00:04:57,200
who are in a bad way because you are helping people who are in a position to help others

41
00:04:57,200 --> 00:05:07,240
will create a chain, it's like a pyramid scheme, it will multiply and everyone will benefit.

42
00:05:07,240 --> 00:05:10,960
What I'm saying is that people who come to practice meditation with me end up becoming

43
00:05:10,960 --> 00:05:19,680
social workers or teachers and so I don't have to go out and teach those people, I teach

44
00:05:19,680 --> 00:05:26,040
the people who will go out and teach them which is to me far better use of one's time.

45
00:05:26,040 --> 00:05:32,120
The basic point being to try to pick the gifted, pick the ones who are you going to get

46
00:05:32,120 --> 00:05:38,000
the best return for your effort because not only it's not being selfish, it's not being

47
00:05:38,000 --> 00:05:44,680
discriminatory, it's being realistic and understanding that we're really like in a war

48
00:05:44,680 --> 00:05:53,200
here, we don't have the luxury to pick and choose, we have to go for the most benefit

49
00:05:53,200 --> 00:06:02,560
because death waits for no one and nothing waits, the universe will continue rolling on in

50
00:06:02,560 --> 00:06:07,000
its way and if we don't keep up, we're going to get left behind.

51
00:06:07,000 --> 00:06:11,280
This is why people get burnt out when they deal with such people, now this is only half

52
00:06:11,280 --> 00:06:18,840
of the answer, my answer is don't teach children, this is only in relation to the part

53
00:06:18,840 --> 00:06:24,480
of especially those who have emotional disturbances, you're welcome to do it, I'm not

54
00:06:24,480 --> 00:06:31,880
going to try to tell you what to do, just give you some idea maybe it will help to change

55
00:06:31,880 --> 00:06:37,920
your focus, I would think it would be much, from my point of view, much more inspiring

56
00:06:37,920 --> 00:06:44,600
to teach the gifted children and the way that you teach children, the question as to

57
00:06:44,600 --> 00:06:54,400
whether they're too young, the tradition goes seven years, everything's seven, so at seven

58
00:06:54,400 --> 00:07:00,080
years they're old enough to understand, which is most children already, but anything

59
00:07:00,080 --> 00:07:06,240
below seven years is considered to be too young, but at seven years this is the cutoff

60
00:07:06,240 --> 00:07:10,000
and then they can start learning.

61
00:07:10,000 --> 00:07:16,560
The way that teach children is to introduce to them the concept of mindfulness, the concept

62
00:07:16,560 --> 00:07:32,560
of recognition, teaching them to recognize and to clearly perceive the experience in

63
00:07:32,560 --> 00:07:41,760
front of them, or anything really, and I tried an experiment once with some kids in a

64
00:07:41,760 --> 00:07:50,560
Thai time monastery in Los Angeles, I had them focus on a cat, I had them think of a cat

65
00:07:50,560 --> 00:07:55,360
and say to themselves in their mind, cat, cat, because this was a room of like a hundred

66
00:07:55,360 --> 00:08:01,240
kids and they were unruly, these were kids who come to the temple on Saturdays and

67
00:08:01,240 --> 00:08:11,920
get whatever lesson that is possible to give them, but it's mainly, not whatever, it

68
00:08:11,920 --> 00:08:21,000
is what it is, and so they were like anything I would say, they would repeat it and

69
00:08:21,000 --> 00:08:27,920
they would make fun of it and laugh at it, and I wasn't upset by that, but I was prepared

70
00:08:27,920 --> 00:08:37,960
in an event because I was aware of the situation, so you can't lead these people into directly

71
00:08:37,960 --> 00:08:42,280
into meditation, you can't say start watching the stomach say rising, give them something

72
00:08:42,280 --> 00:08:47,360
that's going to appeal to them, so I thought well let's do some simple kid stuff, think

73
00:08:47,360 --> 00:09:03,320
of a cat and say to yourself cat, cat, cat, and then dog, dog, I had them do dog, dog,

74
00:09:03,320 --> 00:09:09,200
and it's kind of fun at first, it seems like I'm just playing a game with them, but actually

75
00:09:09,200 --> 00:09:15,920
they start to conceive and they start to focus and their mind starts to quiet down, and

76
00:09:15,920 --> 00:09:22,360
it did work, I think it worked quite well, it's something that you'd have to try long term,

77
00:09:22,360 --> 00:09:27,840
but the point of it is to get them around slowly to the point of being able to watch and

78
00:09:27,840 --> 00:09:33,560
observe their own experience, first a cat and then a dog, and then I had them focus on

79
00:09:33,560 --> 00:09:38,920
their parents, and this is important, and this really got me brownie points with the parents,

80
00:09:38,920 --> 00:09:49,080
the parents are all sitting listening as well, and as I said, think of your mother and

81
00:09:49,080 --> 00:09:59,480
say to yourself mother, mother, and then your father and say to yourself father and father,

82
00:09:59,480 --> 00:10:04,680
and then I came to what is actually the beginnings of Buddhist meditation, it's the focusing

83
00:10:04,680 --> 00:10:10,520
on the Buddha, right? You have this famous Buddhist meditation of saying yourself Buddha,

84
00:10:10,520 --> 00:10:16,240
Buddha, so I had them do that, I said, picture the Buddha now, because the Buddha is being

85
00:10:16,240 --> 00:10:21,920
golden Buddha image, so think of the Buddha and say Buddha, Buddha, Buddha, Buddha, Buddha,

86
00:10:21,920 --> 00:10:29,160
and so they did this, and then eventually I got around, I think after the Buddha, then it

87
00:10:29,160 --> 00:10:36,000
was right, the focusing on the body, and then it was watching the stomach, watching your

88
00:10:36,000 --> 00:10:43,960
breath. I brought it to them that way, I think that it's just a simple point there,

89
00:10:43,960 --> 00:10:52,920
the idea of starting with something cartoonish that they can relate to, and of course it

90
00:10:52,920 --> 00:11:05,560
depends on the age level, but the important point is to bring the concept to them, find

91
00:11:05,560 --> 00:11:11,600
a way to explain mindfulness to them, and I think that's clearly explained by using the

92
00:11:11,600 --> 00:11:15,840
cat example, the dog exam, because you're just focusing on something and you're seeing

93
00:11:15,840 --> 00:11:21,920
this as a cat, this is a dog, and then they're building, what you're doing is they're

94
00:11:21,920 --> 00:11:31,120
building up this ability, and all you do is you bring that ability, that proficiency

95
00:11:31,120 --> 00:11:59,000
or however, back to focus on reality.

