1
00:00:00,000 --> 00:00:17,920
Good evening everyone, broadcasting live in May 15, 2016, tonight's quote from the Etie Wood

2
00:00:17,920 --> 00:00:30,880
look at. It's actually interesting. The quote itself is fairly simple. It's talking about

3
00:00:30,880 --> 00:00:38,400
two spheres. The quote is actually mistranslated as usual, which may not be important. It's

4
00:00:38,400 --> 00:00:46,480
actually more of an interpretative translation. The word for spiritual is interesting. Those of

5
00:00:46,480 --> 00:00:52,320
you who know anything about Pauli or the Dhamma may be wondering, well, where does that word come

6
00:00:52,320 --> 00:00:58,560
from? Haven't seen that word before. And you'd be right. That's not the word that's being used.

7
00:01:01,360 --> 00:01:08,880
Just look at the Pauli. We've got a common area. Let's look at the original Pauli.

8
00:01:08,880 --> 00:01:33,200
There are these two types of gifts. What two types of gifts? Amis. Amis means an object or a physical

9
00:01:33,200 --> 00:01:45,680
thing. Something physical is Amis. And Dhamma is a gift of Dhamma. So you can see how he gets spiritual

10
00:01:45,680 --> 00:01:53,280
from that, but it's important to be precise and clear what we're talking about. A gift of truth.

11
00:01:53,280 --> 00:02:04,880
And Dhamma means truth. Where Dhamma is an interesting word. Originally, probably, I haven't studied

12
00:02:04,880 --> 00:02:15,120
the etymology, but it was used in Hinduism to mean something that someone holds. So a warrior,

13
00:02:15,120 --> 00:02:24,640
warrior Dhamma, because the root and daughter means to carry or to hold, to protect, to guard,

14
00:02:25,280 --> 00:02:33,200
to keep, keep lay be a good one. So a warrior's Dhamma wouldn't be the rules. They keep or the code

15
00:02:34,160 --> 00:02:42,400
that they kept. It was what they held. And we say that in English, when you hold something to be

16
00:02:42,400 --> 00:02:54,400
true, or you hold something to be right. So the Buddha twisted that. I mean, by the time the Buddha

17
00:02:54,400 --> 00:02:58,240
came around, I've talked about this before, by the time the Buddha came around, it would have meant

18
00:02:58,880 --> 00:03:04,480
anything that someone holds to be true. So a teacher's Dhamma, a spiritual teacher would have

19
00:03:04,480 --> 00:03:09,040
their Dhamma. There were many different kinds of teachers who taught many different things. And each

20
00:03:09,040 --> 00:03:17,280
one, what they taught was called their Dhamma. So the Buddha had his own Dhamma or Dhamma, as we say, in

21
00:03:17,280 --> 00:03:24,240
power. But the Buddha explained it a bit differently, and he talked about something that holds

22
00:03:25,360 --> 00:03:32,480
its own or something that holds. And it's another way we use in English, the word hold.

23
00:03:32,480 --> 00:03:40,640
This holds that hold. So it doesn't hold that. It holds that. So figure the way of saying

24
00:03:40,640 --> 00:03:50,640
something is true. Something is factual. Meaning it holds up to inspection. When you inspect it,

25
00:03:50,640 --> 00:03:56,800
it holds up. Meaning it doesn't break down. So when you inspect a view,

26
00:03:56,800 --> 00:04:03,120
some views, when you inspect them, when you investigate them, they break down. You see,

27
00:04:03,120 --> 00:04:13,040
they don't accord with reality. But some hold up. And so what that means is reality or truth.

28
00:04:13,040 --> 00:04:18,800
So the Buddha's Dhamma is the truth. Is the truth. Of course, everyone just

29
00:04:18,800 --> 00:04:27,520
teaches us that. But that's how he means it. So that's how I think we should understand it here, a gift of truth.

30
00:04:27,520 --> 00:04:49,680
And he says, and then he says, the same about some way Bhagavat means sharing. There are two kinds of

31
00:04:49,680 --> 00:04:59,600
sharing. There are sharing of material possessions. And there is sharing of truth. And Anuga,

32
00:04:59,600 --> 00:05:10,080
which means support, right, assistance help. There are two kinds of help helping someone

33
00:05:10,080 --> 00:05:20,080
materially giving the money or giving them food or giving them etc. And then helping them out with

34
00:05:20,080 --> 00:05:29,920
the truth. And he says, in each case, the truth is better. And the commentary has some interesting

35
00:05:29,920 --> 00:05:38,720
explanation of it. That's what you'd expect. Amisadana is the four basic records. It's giving

36
00:05:40,560 --> 00:05:50,320
physical things that are needed. Maybe it's not that interesting. And there are some interesting

37
00:05:50,320 --> 00:06:01,760
language like when you are not up a post so-called, which means lazy or inactive,

38
00:06:03,600 --> 00:06:13,120
you give up your life of ease to teach others. That's how it's a gift. Because the truth

39
00:06:13,120 --> 00:06:18,320
gives someone the truth. It's an interesting, it seems kind of strange as to how do you give

40
00:06:18,320 --> 00:06:23,840
someone the truth? Well, that means taking the time to teach them the truth. A post who go,

41
00:06:23,840 --> 00:06:34,480
a hoot, wow, not being lazy or not being focused only on your own well being. And then the second one

42
00:06:34,480 --> 00:06:44,480
is for the third one, giving assistance. It's paraisam Anuga, Anugan, Nugan, Anugan, Anugan, Anugan,

43
00:06:44,480 --> 00:06:57,200
Anugan, Anugan means compassionately. It means come, come, come, come, come.

44
00:06:58,800 --> 00:07:10,560
It means to be moved. Anugan, Anugan means being moved to compassion or moved in relation to

45
00:07:10,560 --> 00:07:19,040
someone, moved to want to help them. So you can help them in two ways with physical material

46
00:07:19,040 --> 00:07:31,600
possessions or etc. So this is of course a perennial subject where you've seen many quotes about

47
00:07:31,600 --> 00:07:44,080
giving, what does it mean to give and the importance of giving. Now often bring up the topic

48
00:07:44,080 --> 00:07:52,240
or the fact that it's a bit of an awkward thing and it sounds somewhat self-serving to teach

49
00:07:53,520 --> 00:08:00,960
generosity as a teacher because you know it's a monastic because we rely upon people's

50
00:08:00,960 --> 00:08:10,160
generosity but it's not something to be ashamed of or something to shy away from me because

51
00:08:11,120 --> 00:08:16,480
it's just a fact that in order to keep teaching, in order to keep sharing the number,

52
00:08:17,920 --> 00:08:25,200
we need to be fed, we need to be given, we need to have the requisites, the basics of life to

53
00:08:25,200 --> 00:08:34,560
continue our work. But this quote is for those who give what they should think about giving and

54
00:08:34,560 --> 00:08:42,800
it's important not to fixate on material possessions because yes, it's good to give physical

55
00:08:42,800 --> 00:08:48,960
things but it's not the best. So something we should focus on and so in the Buddhist time as well

56
00:08:48,960 --> 00:08:56,320
as in present it's easy to get caught up in Amisadana giving too much material

57
00:09:00,400 --> 00:09:08,000
and forgetting about the truth so a lot of people will support teachers or support Buddhist

58
00:09:08,000 --> 00:09:14,480
monks but they never give the dhamma so they're constantly giving material possessions but they

59
00:09:14,480 --> 00:09:22,240
never have any, they never work to give the dhamma and that fact kind of I think shows

60
00:09:23,360 --> 00:09:28,640
that in fact material whereas it seems to be that material possessions are more concrete right

61
00:09:28,640 --> 00:09:33,600
so they're more weighty and so immediately we think well this is good and if you think about

62
00:09:33,600 --> 00:09:39,360
giving the dhamma that sounds kind of meaning a kind of trivial just telling someone what to do

63
00:09:39,360 --> 00:09:45,680
a teacher you know that's easy when in fact the opposite is okay it's not easy to teach

64
00:09:47,680 --> 00:09:58,640
it's not easy to teach but it's something it's far better to give the dhamma than to give

65
00:09:58,640 --> 00:10:16,000
physical material material gift it's like if you give the giving physical giving material possessions

66
00:10:16,000 --> 00:10:24,960
material gift is actually much easier much more trivial you've ever tried to teach someone

67
00:10:24,960 --> 00:10:31,200
you ever tried to give someone true theory even tried to give advice to someone that's not easy

68
00:10:31,200 --> 00:10:38,000
it's easy to give advice it's not easy to give good advice anyone can give advice and mostly

69
00:10:38,000 --> 00:10:44,240
we do we're very good at giving advice we're not very good at giving good advice this is the

70
00:10:44,240 --> 00:10:57,120
problem to give good advice you need you need to work you need to train but I absolutely can't

71
00:10:58,560 --> 00:11:04,480
can't stress enough the importance of giving the dhamma of course giving to yourself first

72
00:11:05,440 --> 00:11:12,560
don't you should never teach without practicing yourself but the greatness of giving the dhamma

73
00:11:12,560 --> 00:11:18,240
just sharing what you've learned I mean that's how Buddhism will continue if people stop spreading

74
00:11:18,240 --> 00:11:26,960
the dhamma if we're content with giving physical gifts material gifts if our goodness

75
00:11:28,400 --> 00:11:33,760
focuses mostly as Buddhists we practice for ourselves and then we're generous

76
00:11:33,760 --> 00:11:39,120
with material possessions Buddhism will never survive and you can see this in Buddhist cultures

77
00:11:39,120 --> 00:11:46,240
where giving material possessions is stressed becomes corrupt quite easily amongst become corrupt

78
00:11:46,240 --> 00:11:54,720
the institution becomes corrupt but where people give dhamma more people there's more scrutiny

79
00:11:54,720 --> 00:12:02,880
and there's more interest and it's more public with more people practicing more people come in

80
00:12:02,880 --> 00:12:10,880
to learn to understand there's a greater understanding when we're talking about the dhamma we give

81
00:12:10,880 --> 00:12:19,680
the dhamma so it's wrong to think that I'm not a teacher I don't dare teach because I'm not a

82
00:12:19,680 --> 00:12:25,440
teacher that's the wrong way of looking at it but I didn't think of himself or I didn't it

83
00:12:25,440 --> 00:12:31,760
did but he often talked and talked about himself or our teacher is more of a good friend someone

84
00:12:31,760 --> 00:12:37,200
who gives something and so often he used this kind of language giving a gift of truth

85
00:12:39,440 --> 00:12:44,320
and that's what it is if you know the truth if you've been taught the truth share it

86
00:12:45,680 --> 00:12:51,120
it's like on Facebook we share all sorts of stuff because we think it's good

87
00:12:51,120 --> 00:13:01,680
in in Buddhist cultures they'll often print texts and they'll print books you know people who

88
00:13:01,680 --> 00:13:07,040
print help print this book and that's a good point is we're probably gonna if I'm going

89
00:13:07,040 --> 00:13:12,960
to Sri Lanka we might as well print another thousand of them right so we should have another

90
00:13:12,960 --> 00:13:22,640
here we go this is a good but you know even printing books it's secondary printing books is useful

91
00:13:22,640 --> 00:13:32,960
it's great but if you compare it to not to bolster it not but if you compare it to the the writing

92
00:13:32,960 --> 00:13:38,160
of the book the creating of the book and that's what we need we need not people writing more books

93
00:13:38,160 --> 00:13:45,040
but necessarily but actually teaching actually helping actually working

94
00:13:48,400 --> 00:13:54,880
so once we get a center if we ever get a thriving meditation center

95
00:13:55,520 --> 00:14:00,640
maybe we can start training teachers as well I've done that before it's not difficult

96
00:14:00,640 --> 00:14:08,960
it's not difficult to teach again it's just takes time to teach well you have to work at that

97
00:14:11,840 --> 00:14:20,400
anyway quite a simple quote look too much more to say except to encourage they should be

98
00:14:21,120 --> 00:14:26,160
encouraged to give gifts of dominant is printing books and giving you know because that's kind

99
00:14:26,160 --> 00:14:32,960
of actually just physical you know it's still just a material thing but actually giving someone

100
00:14:32,960 --> 00:14:38,080
the truth that's very difficult something we should work at it should be a part of our practice

101
00:14:39,360 --> 00:14:44,800
but it said are there commentary it says it reads it again

102
00:14:44,800 --> 00:15:01,200
it's easy as a meditator to just go off on your own and do nothing which is fine

103
00:15:03,040 --> 00:15:09,840
but it's much greater to share the dhamma, dhamma some we bah go the sharing of the dhamma

104
00:15:09,840 --> 00:15:16,320
we should share talk that in kindergarten

105
00:15:19,040 --> 00:15:27,040
anyway that's the quote for tonight another drop of dhamma you have any questions

106
00:15:33,440 --> 00:15:38,480
how can we give the dhamma if we are surrounded by uninterested people that's a good point

107
00:15:38,480 --> 00:15:44,320
if there's no one in and I mean it's not so much that interested it's people who are in need

108
00:15:44,320 --> 00:15:52,400
or are receptive so it doesn't mean people have to say hey I want to learn about meditation

109
00:15:52,400 --> 00:15:55,600
but if someone's suffering

110
00:16:00,320 --> 00:16:03,520
yeah mainly if someone's suffering and looking for a cure

111
00:16:03,520 --> 00:16:12,160
you know offering Buddhism as a cure is but that's a good question you know and it's true

112
00:16:12,160 --> 00:16:17,200
if you're surrounded by people who are uninterested well many of us find ourselves in that position

113
00:16:17,200 --> 00:16:25,600
but I think there's probably something to be said for well it would be a reason for moving to a

114
00:16:25,600 --> 00:16:34,480
place where there's more people interested surrounding yourself with people who are interested

115
00:16:34,480 --> 00:16:38,640
putting yourself in a position where you're with people who are interested but you know another argument

116
00:16:38,640 --> 00:16:44,480
can be because monks sometimes find themselves alone it doesn't mean they it's it's important

117
00:16:46,000 --> 00:16:49,920
it doesn't mean it's an intrinsic part of our practice in fact there's something I should

118
00:16:49,920 --> 00:16:57,120
mention is that Don and generosity isn't isn't an intrinsic part of the past it's not necessary

119
00:16:59,200 --> 00:17:03,360
and so this is more like if you're going to give a gift which sort of gives shouldn't give

120
00:17:03,360 --> 00:17:10,640
and what sort of gift is better because often we are in a position where we can give can help

121
00:17:10,640 --> 00:17:16,000
someone you know someone's upset we're giving them a box of chocolates is probably not

122
00:17:16,000 --> 00:17:20,160
giving them a candy or something it's probably not the best answer

123
00:17:29,200 --> 00:17:33,440
so you don't have to give gifts because there's no one interested there's truly no one

124
00:17:33,440 --> 00:17:40,640
interested or receptive then it's like if there's no one hungry well don't don't go around

125
00:17:40,640 --> 00:17:49,040
giving people food if they're not hungry why are some intelligent people not wise but wise people

126
00:17:49,040 --> 00:17:56,320
seem to be always intelligent I don't know that that's necessarily true I mean wisdom and

127
00:17:56,320 --> 00:18:06,000
Buddhism is involved with mindfulness and that creates a certain clarity of mind but you know like

128
00:18:06,000 --> 00:18:13,920
there's this there's this teacher in Israel and he's not very intelligent I don't think I mean

129
00:18:13,920 --> 00:18:19,120
he's always said that he's a Buddhist monk in my tradition ordained under the same teacher

130
00:18:24,560 --> 00:18:30,720
but he said you know he said his memory is very poor and I think it is from from what I've seen

131
00:18:30,720 --> 00:18:36,000
I memorized the Patimoka and he just shook his head he said I don't know I don't know how you do that

132
00:18:37,200 --> 00:18:42,160
but he's a he's a very powerful presence and very pure

133
00:18:46,320 --> 00:18:50,400
so it's not necessarily the case certain aspects of intelligent art necessary

134
00:18:52,320 --> 00:18:55,360
but don't let anyone fool you into thinking

135
00:18:55,360 --> 00:19:11,600
you know into dismissing their lack of clarity of mind as being an important if someone

136
00:19:11,600 --> 00:19:22,160
mind is not clear there's a clarity that's associated with wisdom so like there was this monk

137
00:19:22,160 --> 00:19:25,840
in the Visudhi manga talks about this monk who couldn't remember

138
00:19:33,520 --> 00:19:39,280
who had no who remember who could study they would study the Majimani Gai and then left it

139
00:19:39,280 --> 00:19:45,760
alone for 20 years and hadn't memorized the Majimani Gai and for 20 years he hadn't gone over it

140
00:19:45,760 --> 00:19:51,360
and some students came to see him and it become because he had become enlightened he was able to

141
00:19:51,360 --> 00:19:56,560
remember it all and he was able to to ever sighted back to them or explain it to them

142
00:19:58,000 --> 00:20:08,560
so the power of meditation and enlightenment is is definitely supported of intelligence

143
00:20:08,560 --> 00:20:19,760
there's the story of julapantaka who couldn't remember his single stand

144
00:20:19,760 --> 00:20:28,160
but then he became an arahant and I'm pretty sure after he became after he became an arahant

145
00:20:28,160 --> 00:20:31,680
Buddha asked him to give a talk and everyone was like well he couldn't even remember

146
00:20:31,680 --> 00:20:38,640
stanza but he was because he was an arahant he was able to teach all of the Buddha's teaching

147
00:20:39,280 --> 00:20:46,880
he was quite wise but I would say would say probably some

148
00:20:50,000 --> 00:20:57,120
there may be able to argue that certain aspects of intelligence don't have anything to do with wisdom

149
00:20:57,120 --> 00:21:03,680
so why are some people who are intelligent not wise

150
00:21:06,400 --> 00:21:13,280
I would argue that there's some basis for that a person who is who is more intelligent has some

151
00:21:13,280 --> 00:21:22,080
wholesome base which may not be associated with wisdom see intelligence is considered to be a wholesome

152
00:21:22,080 --> 00:21:28,960
result so wisdom is a wholesome karma so therefore it brings about that sort of result

153
00:21:30,720 --> 00:21:37,200
but intelligence can also be a result of worldly karma potentially not even wisdom

154
00:21:39,280 --> 00:21:42,480
because goodness can be done with wisdom or without wisdom

155
00:21:42,480 --> 00:21:53,920
you know some but you've done you know we paid but I'd probably guess that intelligence is more

156
00:21:53,920 --> 00:21:59,520
on the wisdom side I mean they're of course closely related to who's to say what is intelligence

157
00:21:59,520 --> 00:22:04,000
and what is wisdom they're just words but it's worth having a good memory and so on

158
00:22:04,000 --> 00:22:12,800
may not be associated with wisdom but probably more so more likely associated with wisdom but

159
00:22:12,800 --> 00:22:21,840
at any rate it's a result and so a person may not keep up the karma you know it's with everything

160
00:22:21,840 --> 00:22:26,880
a person can be rich because of their past generosity in the past life but then they're no longer

161
00:22:26,880 --> 00:22:34,320
generous because they're rich they no longer generous so they give that up our goodness our state

162
00:22:34,320 --> 00:22:42,160
so goodness change so but probably most intelligent people were wise at some point maybe not

163
00:22:42,960 --> 00:22:48,640
probably but it's easy for that to change the only thing that doesn't change is insight

164
00:22:48,640 --> 00:22:56,640
into nibana which which breaks through some sorrow

165
00:23:05,280 --> 00:23:09,600
how was he sudden but he wasn't maybe able to teach the whole teaching but he was able to

166
00:23:09,600 --> 00:23:15,760
give a very profound talk because he was in our hand he understood the truth

167
00:23:15,760 --> 00:23:23,680
but no maybe you know but you know probably a great amount of it and moreover I think

168
00:23:24,560 --> 00:23:29,280
the idea is that suddenly his memory improved suddenly his memory cleared up

169
00:23:31,520 --> 00:23:39,120
so I'm not convinced about this Israeli monk who it sounds reasonable but it also could

170
00:23:39,120 --> 00:23:44,240
be argued that the more you meditate the more clear your mind becomes the better your memory gets

171
00:23:44,240 --> 00:23:50,720
so like Ajahn Tang's memory I mean he forgets things and he mixes things up and he has for a

172
00:23:50,720 --> 00:23:58,880
long time because he's tired mostly let's see other thing is your brain the quality of your brain

173
00:23:58,880 --> 00:24:06,000
the state of your brain and but his memory you know the things you remember suddenly he just

174
00:24:06,000 --> 00:24:11,600
pulls something out and he mixes it up sometimes but always pulling out this or that

175
00:24:11,600 --> 00:24:22,320
you know it's mind there's such a clarity so imagine Julebantaka had heard all the Buddhist

176
00:24:22,320 --> 00:24:26,880
teaching but just couldn't remember it and actually it was because of bad karma he had called

177
00:24:27,760 --> 00:24:31,360
some a potato Buddha I think

178
00:24:34,400 --> 00:24:37,840
an imbecile or something or a Buddha I can read the living very bad

179
00:24:37,840 --> 00:24:45,360
I know there was a monk who was at a stutter I think and he made fun of it no a monk who couldn't

180
00:24:45,360 --> 00:24:51,200
remember anything I think and he made fun of this monk or something I can't remember see

181
00:24:51,200 --> 00:25:11,200
memory it's a funny thing not to look it up but yeah there is a connection anyway

182
00:25:11,200 --> 00:25:20,640
yeah left brain right brain I've heard that as well

183
00:25:25,360 --> 00:25:30,720
it's interesting there's I think it's more complicated than that I mean that's a generalization

184
00:25:30,720 --> 00:25:34,960
but there are centers in the brain I was reading a couple of books about that

185
00:25:34,960 --> 00:25:42,640
how different centers in the brain are used for different qualities like there's this the emotional

186
00:25:44,080 --> 00:25:49,200
this book we have it here the emotional life of your brain or something like that

187
00:25:49,200 --> 00:26:06,000
it's an interesting talks about the different spheres or different parts of the brain

188
00:26:06,000 --> 00:26:23,520
any more questions

189
00:26:23,520 --> 00:26:32,960
I don't know saying good night

190
00:26:32,960 --> 00:26:55,440
and everyone

