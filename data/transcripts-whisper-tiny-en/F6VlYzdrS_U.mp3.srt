1
00:00:00,000 --> 00:00:27,200
Okay, good evening, everyone.

2
00:00:27,200 --> 00:00:31,800
We practice meditation.

3
00:00:31,800 --> 00:00:40,800
We practice mindfulness.

4
00:00:40,800 --> 00:00:53,600
We practice with the purpose of

5
00:00:53,600 --> 00:01:03,600
the purpose of strengthening our minds.

6
00:01:03,600 --> 00:01:17,160
We don't practice to escape or to change, not to change our experiences anyway.

7
00:01:17,160 --> 00:01:21,600
You don't come here thinking, I'll go to the meditation center so I won't have to deal

8
00:01:21,600 --> 00:01:25,900
with my real-life problems.

9
00:01:25,900 --> 00:01:28,900
I hope you don't.

10
00:01:28,900 --> 00:01:29,900
I need a vacation.

11
00:01:29,900 --> 00:01:39,800
I'll go and stay in a room and a basement somewhere.

12
00:01:39,800 --> 00:01:53,600
We come here to become strong.

13
00:01:53,600 --> 00:02:04,720
It's important that we understand, we understand the weakness in

14
00:02:04,720 --> 00:02:11,320
defilement and the weakness in evil.

15
00:02:11,320 --> 00:02:17,600
Goodness is strength, not because it's goodness, but because that's the definition of

16
00:02:17,600 --> 00:02:18,600
goodness.

17
00:02:18,600 --> 00:02:25,360
It's a part of the definition of goodness is the strength.

18
00:02:25,360 --> 00:02:34,640
It's a characteristic of certain mind states that really deserve the title of goodness.

19
00:02:34,640 --> 00:02:41,600
There's a strength to them.

20
00:02:41,600 --> 00:02:57,400
And the reason why we denounce and reject certain other states, greed, anger, delusion,

21
00:02:57,400 --> 00:03:06,440
because they're weak, they're vulnerable, fragile.

22
00:03:06,440 --> 00:03:16,600
If you're a person easily angered, you're going to react easily, easily disturbed.

23
00:03:16,600 --> 00:03:17,600
What's wrong with anger?

24
00:03:17,600 --> 00:03:21,040
Well, it's anger's evil, but it's really evil about it.

25
00:03:21,040 --> 00:03:25,960
The worst thing about anger is you poke someone who's anger and they're disturbed.

26
00:03:25,960 --> 00:03:30,480
They suffer or greed.

27
00:03:30,480 --> 00:03:39,600
If you like things, if you want things, you're vulnerable, you're weak, easily manipulated.

28
00:03:39,600 --> 00:03:41,080
How do you manipulate someone?

29
00:03:41,080 --> 00:03:47,400
Well, there are different ways, but one of the easiest ways is take advantage of their

30
00:03:47,400 --> 00:03:55,880
addictions, their likes, their dislikes, which harder to manipulate someone who

31
00:03:55,880 --> 00:04:05,520
has no likes or dislikes, who has not moved, this is what the Buddha said, he said,

32
00:04:05,520 --> 00:04:29,080
what does the Lokadhammi, he jittangyasana kampati?

33
00:04:29,080 --> 00:04:31,160
There's the sort of person who went touched by the Lokadhamma, the ways of the world.

34
00:04:31,160 --> 00:04:40,720
He's dangyasana kampati, their mind, the mind of such a person, such a person, the mind

35
00:04:40,720 --> 00:04:57,600
of a person who is not shaken, not kampati, kampati is like this, waiver, no, a soul kang, because

36
00:04:57,600 --> 00:05:10,000
their mind is not shaken by the vicissitudes of life, the changes, the ways of the world.

37
00:05:10,000 --> 00:05:31,800
Where anun, which the word, unsaid, un-mournful, un-regretful, un-regretting, weirajang, stainless,

38
00:05:31,800 --> 00:05:48,120
pure, cameong, safe, safe, truly safe, ate among atamut among, this is the highest blessing,

39
00:05:48,120 --> 00:06:01,760
this is an ultimate blessing, a real and true blessing.

40
00:06:01,760 --> 00:06:05,680
So I thought it would be useful for us to talk a little bit about, I think I've talked

41
00:06:05,680 --> 00:06:16,920
about these before, about the Lokadhamma, the things in the world that disturb us, that

42
00:06:16,920 --> 00:06:32,800
causes us to waiver, in our minds, causes us to become upset, what are they?

43
00:06:32,800 --> 00:06:43,120
Gain and loss, were disturbed and upset by these things, disturbed positively in the sense

44
00:06:43,120 --> 00:06:55,800
of wanting gain, negatively in the sense of fearing and reacting negatively to loss.

45
00:06:55,800 --> 00:07:07,680
So much of what we do in the world is for gain, to get this, get that, get money.

46
00:07:07,680 --> 00:07:14,760
Money is really just a placeholders, I mean, money is not a good in and of itself.

47
00:07:14,760 --> 00:07:21,960
I argued with my philosophy professor about this last year, he was saying money isn't

48
00:07:21,960 --> 00:07:25,240
the people, because I think it was, who was it?

49
00:07:25,240 --> 00:07:33,080
Aristotle says something about people who are, who crave after money, who see money as

50
00:07:33,080 --> 00:07:39,440
the ultimate good, and so well, it's not, isn't it possible that someone could just

51
00:07:39,440 --> 00:07:45,200
really like money, because it's true, you know?

52
00:07:45,200 --> 00:07:50,760
As ridiculous as it seems, people are just happy to have a lot of money.

53
00:07:50,760 --> 00:07:59,400
I mean, if you think about it logically, well, it is just paper, but we get very proud.

54
00:07:59,400 --> 00:08:06,080
It's gotten to the point where money is no longer a placeholder, money is, I mean, it represents

55
00:08:06,080 --> 00:08:14,040
so much, it represents power, represents getting so many things, you can get what you want

56
00:08:14,040 --> 00:08:33,000
if you have money.

57
00:08:33,000 --> 00:08:39,920
Wanting to get possessions, a car, a house, so many things, but it's really, it's really

58
00:08:39,920 --> 00:08:56,320
deeper than that, isn't it, one thing to get food, craving food, very, very simple desire,

59
00:08:56,320 --> 00:09:00,440
one thing food that you don't have, coming to a meditation center where all the food

60
00:09:00,440 --> 00:09:10,240
is, I don't know, I think Javen makes good food, I don't want to disparage this food, but maybe

61
00:09:10,240 --> 00:09:19,760
it's not a whole couture, certainly we have, right now we have a Vietnamese student, and

62
00:09:19,760 --> 00:09:30,360
where are you from, South America, Central America, Meco, Mexican student?

63
00:09:30,360 --> 00:09:40,480
I don't have any Mexican food, I don't think, maybe some Vietnamese food, wanting it, wanting

64
00:09:40,480 --> 00:09:51,440
this, wanting that, the things we want, disturbed by it, no, I've disturbed the mind

65
00:09:51,440 --> 00:10:05,640
becomes, how weak the mind is, because of wanting, becomes an obsession where you think

66
00:10:05,640 --> 00:10:12,520
it's a lot, as soon as I get it, I'll be happy, right, it's okay to want, because once

67
00:10:12,520 --> 00:10:22,600
I get it, then I'll be happy, and if you don't challenge yourself, you'll never see

68
00:10:22,600 --> 00:10:29,160
the, it may not, may never, not never, but you may, may take time for you to actually see

69
00:10:29,160 --> 00:10:39,440
how precarious the situation is, as long as you can keep getting what you want, maybe

70
00:10:39,440 --> 00:10:49,760
tempering your desire sometimes, I never see the problem with it until one day, bam, lost,

71
00:10:49,760 --> 00:10:57,280
disturbed by lost, disturbed by gain, because you can't just sit here when you want something,

72
00:10:57,280 --> 00:11:06,160
you have to go and get it, when you do sit and meditate here, disturbed constantly by things

73
00:11:06,160 --> 00:11:20,800
you want, gain, loss, disturbed by not getting, by losing the things we want, not being

74
00:11:20,800 --> 00:11:33,200
able to get them, we're afraid of losing money, we're afraid of losing possessions, we're

75
00:11:33,200 --> 00:11:49,320
devastated by loss, loss of wealth, loss of health, loss of relatives, friends, we put

76
00:11:49,320 --> 00:12:03,960
our, our, put all of our eggs in one basket, I guess, put our eggs in the basket of,

77
00:12:03,960 --> 00:12:13,160
the world, we, we, we put all of our faith and all of our, we wager all of our happiness

78
00:12:13,160 --> 00:12:24,840
on, on externalities, our happiness depends upon things which are not dependable, the world

79
00:12:24,840 --> 00:12:32,720
is not dependable, being a part of what the Buddha tried to teach, what he saw and what

80
00:12:32,720 --> 00:12:41,920
he focused on, and what he tried to impress upon his students, impermanence, uncertainty,

81
00:12:41,920 --> 00:12:52,200
but the nature of the world is, is a full spectrum, as long as there's only part of what's

82
00:12:52,200 --> 00:13:02,240
possible and that is acceptable to you, you're always going to be weak, vulnerable until

83
00:13:02,240 --> 00:13:10,800
you can be at peace and at ease with the full spectrum of reality, whatever happens,

84
00:13:10,800 --> 00:13:23,760
independent, unshaken, right, don't ever truly be at peace until you come to that.

85
00:13:23,760 --> 00:13:34,160
Those are the first two local demos.

86
00:13:34,160 --> 00:13:44,680
The second one is, one extreme, we have fame, the other one we have, what's the opposite

87
00:13:44,680 --> 00:13:57,520
of fame, it's something about, I guess, obscurity, it's perhaps the best one, but it's,

88
00:13:57,520 --> 00:14:02,160
it's a bit misleading to say obscurity, everything means not having any friends, not having

89
00:14:02,160 --> 00:14:12,560
anyone know you, not having any connections, being alone, right, how, how distant

90
00:14:12,560 --> 00:14:23,120
anyone knows your name, to be alone in the world, we're disturbed by fame, by how many

91
00:14:23,120 --> 00:14:35,160
friends we have, how many people know who we are, removed by our desire for relations

92
00:14:35,160 --> 00:14:47,440
or desire to be well known, desire to be famous.

93
00:14:47,440 --> 00:14:55,440
On many levels, it refers simply to having friends, having associates and people surrounding

94
00:14:55,440 --> 00:15:03,720
you, can be quite disturbing to people when they don't have any friends or when no one

95
00:15:03,720 --> 00:15:10,520
knows who they are, being a stranger in a strange land when you travel to another country

96
00:15:10,520 --> 00:15:16,760
and really just feeling alone, if you've been surrounded by people who you know,

97
00:15:16,760 --> 00:15:22,240
you never realize this until you go to another country and realize I'm all alone, I don't

98
00:15:22,240 --> 00:15:33,080
have anyone here, but it gets worse when people really want to be famous, right, and how

99
00:15:33,080 --> 00:15:48,480
fickle it can be, you can be famous one day and no one knows who you are the next, but

100
00:15:48,480 --> 00:15:54,440
the most devastating, I think, is feeling friendless, feeling like you have no one, no one

101
00:15:54,440 --> 00:16:02,640
to support you, no one who cares, I think we feel this acutely as meditators, sometimes

102
00:16:02,640 --> 00:16:11,000
those people who talk to who really become keenly interested in Buddhism and find there's

103
00:16:11,000 --> 00:16:20,480
no one around, I always think back to this one woman who emailed me and we got into a discussion,

104
00:16:20,480 --> 00:16:29,040
she was living in a house and they literally wouldn't let her meditate, evil is satanic

105
00:16:29,040 --> 00:16:38,880
and so on, I don't think that's a unique situation, I'm being alone in the practice of the

106
00:16:38,880 --> 00:16:48,040
dumb might can be quite disturbing, it shouldn't be, I mean this shouldn't be, the problem

107
00:16:48,040 --> 00:16:55,240
isn't the experiences you see, gain and loss are not the problem, the problem is being

108
00:16:55,240 --> 00:17:03,400
disturbed by them, having lots of friends being alone, you practice meditation for a while

109
00:17:03,400 --> 00:17:12,240
and you'll just be perfectly at ease and prefer for the most part to be alone, which

110
00:17:12,240 --> 00:17:17,920
is quite amazing really, I think it's quite a surprise for people who just need it to

111
00:17:17,920 --> 00:17:29,160
be around others, you need it to support and connection, to just feel incredibly comfortable

112
00:17:29,160 --> 00:17:38,160
just being alone.

113
00:17:38,160 --> 00:17:59,440
The third set is associated but this is perhaps more poignant, praise and blame, this one

114
00:17:59,440 --> 00:18:19,640
certainly is a cause for great disturbance, talk about wavering, how people are disturbed,

115
00:18:19,640 --> 00:18:27,640
how we are disturbed by praise and blame, that's a good one, you know, it's a good one

116
00:18:27,640 --> 00:18:34,440
to talk about because I'd say it's a good way to test yourself when you're praised and when

117
00:18:34,440 --> 00:18:52,080
you're blamed, it's always been quite interesting for me being on YouTube and having

118
00:18:52,080 --> 00:18:56,720
YouTube comments where people will tell me I've changed their life and they think I'm

119
00:18:56,720 --> 00:19:04,800
just the cat's meow and then the next day get a message saying I'm a hustler, I'm

120
00:19:04,800 --> 00:19:12,880
a fraud, must be selling snake oil or something, you know, many, many different kinds

121
00:19:12,880 --> 00:19:18,440
of opinion, but that's nothing really, I mean I don't have to deal with half the things

122
00:19:18,440 --> 00:19:26,480
that people out there do, daily people can be confronted by other people telling them

123
00:19:26,480 --> 00:19:35,960
you're useless, you're ugly, you're dumb, you're hopeless and not in a good way that

124
00:19:35,960 --> 00:19:46,400
we talked about in a bad way, we're a loser, you're a bum, I had one, someone came recently

125
00:19:46,400 --> 00:19:54,720
and told me that her daughter was told she was brown, she shouldn't show her her legs,

126
00:19:54,720 --> 00:19:59,320
she shouldn't wear shorts because people would be turned off by her brown skin, apparently

127
00:19:59,320 --> 00:20:09,240
that actually happens still in this world.

128
00:20:09,240 --> 00:20:15,480
And how devastated we become, right, someone calls you fat and you're maybe a little bigger

129
00:20:15,480 --> 00:20:20,320
than most people, maybe you have more fat than on your body than most people that can

130
00:20:20,320 --> 00:20:30,120
be quite hurtful, there was this big movement now, people getting very angry at how sensitive

131
00:20:30,120 --> 00:20:37,200
people are, and this is an interesting discussion, I mean there's truth to it, the fact

132
00:20:37,200 --> 00:20:44,520
that we are over sensitive, if someone, if you're, if you feel, or you know, if you

133
00:20:44,520 --> 00:20:51,680
if, if you identify, don't feel sorry, if you identify as a say a female, and you put

134
00:20:51,680 --> 00:21:02,840
car, you hear because well, you happen to have y chromosomes, you get very upset by that

135
00:21:02,840 --> 00:21:09,680
and they would say, well, stop being such a snowflake, this is the word they use, snowflake

136
00:21:09,680 --> 00:21:15,320
meaning, anyway, I don't want to get into it, but if it becomes quite violent then these

137
00:21:15,320 --> 00:21:24,400
people can be quite vitriolic and hurtful, but there is a point there, that sensitivity

138
00:21:24,400 --> 00:21:31,080
is, you've only got yourself to blame, if you're sensitive, I mean, again trying to get

139
00:21:31,080 --> 00:21:40,600
everyone to say, say the right thing, use the right words, you know, black people are

140
00:21:40,600 --> 00:21:45,320
going to be called horrible things, you know, this word that no one wants to say, right,

141
00:21:45,320 --> 00:21:53,000
you're going to have that, Mexican people, Asian people, you're all, we're all called, you

142
00:21:53,000 --> 00:21:58,520
know, I get called white, I don't know, that's not nearly as harmful as hurtful, but

143
00:21:58,520 --> 00:22:03,480
people certainly mean it in a derogatory sense, I mean it in a harmful sense, you can't

144
00:22:03,480 --> 00:22:10,320
be a real Buddhist because you're white, and I was arrested in, I would not be when I was

145
00:22:10,320 --> 00:22:16,280
arrested, the police before I was arrested and after as well, the police in California

146
00:22:16,280 --> 00:22:24,920
were constantly on me, and one Hispanic man said to me, he said, you know, if you had

147
00:22:24,920 --> 00:22:31,320
Asian skin, if you were, if you looked Asian, they wouldn't bother you.

148
00:22:31,320 --> 00:22:42,080
So we get, I mean, I'm in a place of privilege, especially in this country, I think

149
00:22:42,080 --> 00:22:50,120
it far less of it than people of other, you know, I'm not fat, I'm not short, I'm not

150
00:22:50,120 --> 00:22:59,600
a lot of things that people get bullied for, but no, we have to, in Buddhism, this is

151
00:22:59,600 --> 00:23:00,600
our practice.

152
00:23:00,600 --> 00:23:07,920
What I mean to say is that it's all wrong, of course, anyone who says, honestly, I agree

153
00:23:07,920 --> 00:23:14,400
that, you know, if someone wants to be called, she, her, he, him, or something else entirely,

154
00:23:14,400 --> 00:23:21,840
you can go for, you know, the power, you know, fine, it's, it's, it should be our responsibility

155
00:23:21,840 --> 00:23:29,840
to, to accommodate them, I mean, it's not like this is something that puts us out.

156
00:23:29,840 --> 00:23:41,120
It's wrong to be angry and hateful towards such people who, who have this, you know, identity,

157
00:23:41,120 --> 00:23:51,040
that, you know, whatever side of the fence on that debate you fall on, wherever you fall.

158
00:23:51,040 --> 00:23:56,160
In the end, we only have ourselves to blame because we can't, we can't control other

159
00:23:56,160 --> 00:24:06,160
people, and we're vulnerable, we're sensitive, we're weak, and it's, it's not, it's

160
00:24:06,160 --> 00:24:13,360
not a put down or an insult. It's totally understandable because for the most part,

161
00:24:13,360 --> 00:24:24,200
we don't have any way of becoming strong. We don't have any way of resisting. You can't blame

162
00:24:24,200 --> 00:24:33,760
the victim. I mean, actually, ultimately, you can, but rather than exactly the victim,

163
00:24:33,760 --> 00:24:40,000
we blame the situation that they're in, you know, the, the state that they're in. We blame

164
00:24:40,000 --> 00:24:46,880
the state that we're in. We're all in wretched state. We're all vulnerable. We're all weak.

165
00:24:46,880 --> 00:24:55,760
We're not invincible. It's, it's, it's ultimately our fault. We get put into these situations.

166
00:24:55,760 --> 00:25:01,280
We, we, I have blaming the victim, aren't I? No, I mean, the reason we get put in these

167
00:25:01,280 --> 00:25:06,320
situations is really because we're the bullies. We've been the bullies. We bully. And because

168
00:25:06,320 --> 00:25:14,320
we bully, we, we get caught up in loops of, of revenge. We get caught up in, we live, we live in

169
00:25:14,320 --> 00:25:26,240
this world because of how we've become, you know, the reason a person perhaps strongly identifies

170
00:25:26,240 --> 00:25:31,760
as a female, even though they're in the body of a male or strongly identifies as a male,

171
00:25:31,760 --> 00:25:38,880
even though they're in the body of a female. There's much to do with this going back and forth,

172
00:25:38,880 --> 00:25:46,160
not being able to decide, decide from life to life, just probably most of us going back and forth

173
00:25:46,160 --> 00:25:50,240
from being male and female and not deciding, being a male and thinking, Oh, it'd be nice to

174
00:25:50,240 --> 00:25:58,000
be a female, being a female back and forth. I met a, there's this woman. I don't know if she still

175
00:25:58,000 --> 00:26:04,240
works there, but works at this meditation center. She was really helpful and really nice and

176
00:26:04,240 --> 00:26:12,000
kind and we worked well. And I was teaching meditation and she was, every time I went to this place

177
00:26:12,000 --> 00:26:18,320
or quite often, she would invite me to teach and I went to another section in this in Bangkok

178
00:26:18,320 --> 00:26:26,640
and what Mahatad and I went to another section and talking to the monk about, yeah, or she came

179
00:26:26,640 --> 00:26:32,480
to see them or something, came to see my teacher when he was there, I think. When she left,

180
00:26:32,480 --> 00:26:38,080
he said, Yeah, you know, that woman. I said, Yeah, I was, I was teaching there and she was helping me

181
00:26:38,080 --> 00:26:51,040
and what we used to be monks together, he said, I had no idea. But this is, this is how we find

182
00:26:51,040 --> 00:26:58,320
ourselves in these situations. Why we find ourselves? I'm living in Asia as a white person,

183
00:26:59,040 --> 00:27:05,040
subject to a lot of discrimination. So it's not that I don't know these situations.

184
00:27:05,040 --> 00:27:17,440
And we put ourselves in, we get into these situations where we're discriminated against,

185
00:27:18,480 --> 00:27:27,120
people saying nasty things to us. That's not really what I wanted to say because the whole

186
00:27:27,120 --> 00:27:32,720
idea of karma and how we got to where we are is not the most important. It's most important

187
00:27:32,720 --> 00:27:40,480
is how we react to it. Right. So the strength, it's a good example. It's a good thing to bring

188
00:27:40,480 --> 00:27:49,280
this up because strength and invulnerability doesn't come from changing your karma.

189
00:27:50,480 --> 00:27:57,280
Just really the takeaway from that should just be that there's no, there should be no doubt or

190
00:27:57,280 --> 00:28:03,040
no confusion about why we're in the situation we're in. But the answer isn't to change it.

191
00:28:04,160 --> 00:28:09,680
So the answer is not to say, Okay, well, if I'm really nice to people and really humble and really

192
00:28:09,680 --> 00:28:17,200
patient and so on, everyone will be nice to me. And that's the answer, right? Praise, I'll just

193
00:28:17,200 --> 00:28:22,320
get praise. I'll do whatever I can to just be praised. It's a very dangerous road, right?

194
00:28:22,320 --> 00:28:31,040
If you're constantly seeking praise, that's some even worse. And at the worst, of course,

195
00:28:31,040 --> 00:28:37,840
it sets you up for disappointment. People don't praise you. Even you don't get

196
00:28:39,040 --> 00:28:44,560
blamed for anything or insulted. Just not have you craving the praise.

197
00:28:44,560 --> 00:28:53,600
I mean, it's a great test because we're praised and blamed and we're not praised and not blamed.

198
00:28:57,600 --> 00:29:03,440
How we treat, how we react, how we respond is quite important.

199
00:29:06,960 --> 00:29:13,280
So our meditation, I mean, this is all important. It's important discussion for our meditation

200
00:29:13,280 --> 00:29:18,160
because this is what we're trying to do. We're not trying to change our situation,

201
00:29:18,160 --> 00:29:24,160
we're trying to become strong. When we like something or when we dislike something, when we're

202
00:29:24,160 --> 00:29:35,440
moved and upset by the world. The final pair, of course, is the most basic. And that's,

203
00:29:35,440 --> 00:29:42,240
it's really relates to the rest of them, I think, that's happiness and unhappiness or pleasure

204
00:29:42,240 --> 00:29:46,560
and pain. Pleasure and pain is probably the best way to understand it.

205
00:29:50,800 --> 00:29:56,080
Pleasure, we get pleasure out of all of the other things, right? Praise brings us pleasure,

206
00:29:57,360 --> 00:30:04,640
friends, fame brings us pleasure, gain brings us pleasure, but there's just pleasure,

207
00:30:04,640 --> 00:30:14,400
basic pleasure as well. Soft seats give us pleasure, nice bed, good food,

208
00:30:16,000 --> 00:30:21,840
food itself, just food and nourishment gives us this warm and full feeling. It certainly takes

209
00:30:21,840 --> 00:30:34,640
away the pain and anguish of being hungry. Pleasure seekers, it's really not a deep teaching,

210
00:30:34,640 --> 00:30:39,680
I mean, it's not, it shouldn't be hard to understand, but it is. It is because we're taught

211
00:30:39,680 --> 00:30:47,200
something quite different, but it shouldn't be hard to understand that pleasure seeking isn't

212
00:30:47,200 --> 00:30:54,880
and isn't a viable means to find happiness. Always getting what you want is not something anyone

213
00:30:54,880 --> 00:31:05,520
ever recommends, right? So we tend instead to live in mediocrity or in some kind of

214
00:31:05,520 --> 00:31:14,160
liminal state where we're neither happy nor unhappy or not. At peace with ourselves,

215
00:31:15,120 --> 00:31:25,680
we're able to sustain a low grade happiness for the most part by not trying to be too happy,

216
00:31:28,480 --> 00:31:34,480
especially as Buddhists we can, you see a lot of Buddhists who live simple lives,

217
00:31:34,480 --> 00:31:41,120
understanding that getting caught up in too much pleasure is going to lead to suffering and so

218
00:31:41,120 --> 00:31:47,680
they engage in pleasurable activities, but they don't obsess about them and they haven't

219
00:31:47,680 --> 00:31:55,440
awareness a mindfulness of how dangerous it can be to obsess about pleasure and seek it out.

220
00:31:55,440 --> 00:32:08,000
But they're still vulnerable. We're still vulnerable. It's a sort of a

221
00:32:09,360 --> 00:32:17,760
pleasure and I'll bear with some pain. It's not really a solution though. It's not strong. It's

222
00:32:17,760 --> 00:32:26,720
still weak. You're still moved by pleasure and pain. Most of us who are like this, most of us

223
00:32:26,720 --> 00:32:34,080
are not addicts in the strong sense of the word. We're still moved by pain. We don't like it.

224
00:32:34,880 --> 00:32:40,800
We're weak. We're susceptible to pain and pleasure as well. Hey, who doesn't want to feel

225
00:32:40,800 --> 00:32:51,360
pleasure, right? Weakness. This is weakness. It literally is. It's not trying to be

226
00:32:53,280 --> 00:33:00,160
strict or mean about it, but literally in a literal sense is weakness. That's the point

227
00:33:00,160 --> 00:33:11,280
that it would be stronger. We would be much stronger if we were at peace with pleasure and pain,

228
00:33:12,080 --> 00:33:21,360
unmoved by it. That when pleasure came, it was just another experience. When pain comes,

229
00:33:21,360 --> 00:33:30,320
it's just another experience. When our happiness, our peace, our tranquility, our stability of mind. It's

230
00:33:30,320 --> 00:33:41,760
not dependent. It's not influenced by our experiences. It's the only way because experiences are

231
00:33:41,760 --> 00:33:53,200
unpredictable. You can predict and you can control for short periods, but not forever. It's

232
00:33:53,200 --> 00:34:02,400
not a solution. This should help to understand a little clearer about why we're doing what we're

233
00:34:02,400 --> 00:34:09,840
doing. Why are we sitting, repeating to ourselves, rising, falling? I think we overlook how powerful

234
00:34:09,840 --> 00:34:16,560
just that noting to yourself, the stomach rising and falling can be. It's an exercise in strength,

235
00:34:17,200 --> 00:34:26,480
in invincibility, where you experience something without reacting to it, without judging it,

236
00:34:28,480 --> 00:34:32,000
without any kind of labels or value judgment of good or bad.

237
00:34:32,000 --> 00:34:43,840
You want to be at peace, you want to be safe, invincible, strong. This is the way to go,

238
00:34:43,840 --> 00:34:53,200
learn how to see things as they are, how to live your life, how to be alive and not be bogged

239
00:34:53,200 --> 00:35:04,080
down by judging, judging everything, after a moment, after a moment, judge everything.

240
00:35:08,160 --> 00:35:16,800
So look at them as they're always something useful to go over and the general teaching of

241
00:35:16,800 --> 00:35:29,680
the weakness, how they are a test for us, they test us constantly. You can judge and you can measure

242
00:35:29,680 --> 00:35:36,480
your strength against these things, against the vicissitudes of life. I want to know what it means

243
00:35:36,480 --> 00:35:43,120
to be enlightened, how enlightened am I? I am as enlightened as I can withstand the vicissitudes

244
00:35:43,120 --> 00:35:50,400
of life, without being moved by them, without being upset, for against them.

245
00:35:50,400 --> 00:36:11,120
So that's the, that's the demo for tonight. Thank you all for tuning in. Have a good night.

