WEBVTT

00:00.000 --> 00:07.000
Please give an overview on the state of the stream winner.

00:07.000 --> 00:10.000
Stream winner.

00:10.000 --> 00:14.500
The word is Sotapana.

00:14.500 --> 00:17.500
Sotapana.

00:17.500 --> 00:27.000
Sotapana means stream and appana means attained.

00:27.000 --> 00:31.000
So it means one who has attained this stream.

00:31.000 --> 00:35.000
The word Sotapana, generally, as used in the Sutas,

00:35.000 --> 00:38.000
refers to one specific thing.

00:38.000 --> 00:43.000
In the commentaries, there is actually another type of Sotapana.

00:43.000 --> 00:54.000
And then the commentary, like addition, helps us to understand the Sutta definition as well,

00:54.000 --> 00:58.000
because it helps us understand the way that word is used in different contexts.

00:58.000 --> 01:00.000
In the Sutas, it's only used in one way.

01:00.000 --> 01:07.000
It's used to refer to the person who has had a realization of the formable truths,

01:07.000 --> 01:14.000
has come to realize Nibana for the first time.

01:14.000 --> 01:21.000
They have entered into a kind of a cessation, which we call Palasamapati,

01:21.000 --> 01:30.000
where they have realized that all things cease, because they've seen the cessation of all phenomena without remainder.

01:30.000 --> 01:40.000
They've come to regard all phenomena that arise as impermanent, unsatisfying, and uncontrollable.

01:40.000 --> 01:45.000
And that realization is only a temporary realization.

01:45.000 --> 01:48.000
They may still later on cling to things.

01:48.000 --> 01:54.000
But because they've had that realization, at the moment of that realization, the mind goes out, the mind ceases.

01:54.000 --> 01:58.000
And there is this experience of cessation.

01:58.000 --> 02:03.000
It can last for a brief moment, it can last for a minute, it can last for an hour.

02:03.000 --> 02:13.000
It can be cultivated to last, cultivated in some sense of determining to enter into it for up to seven days, according to the texts.

02:13.000 --> 02:19.000
I had one student who claimed to be able to enter into it for 24 hours.

02:19.000 --> 02:23.000
That's what she said, believe it or not.

02:23.000 --> 02:26.000
But she certainly did seem to enter into it quite frequently.

02:26.000 --> 02:36.000
We'd be in the car, and once we were in one of these red trucks that they have in Chiang Mai,

02:36.000 --> 02:41.000
and we're sitting there, and she's not all rocking in the bumps.

02:41.000 --> 02:48.000
And she's hitting her head off the side of the truck and not feeling it, and not being jolted at all.

02:48.000 --> 02:51.000
And as soon as the truck stopped, she opened her eyes.

02:51.000 --> 02:54.000
Something like that, or else it was, I can't remember what it was.

02:54.000 --> 02:59.000
It was like, we're all trying to get out of the car or the truck, and she's still sitting there.

02:59.000 --> 03:06.000
There was another time where we were sitting and chanting, and we'd do chanting in the evening, and we finished chanting.

03:06.000 --> 03:08.000
We all got up, and she was still sitting there.

03:08.000 --> 03:11.000
Because in the middle of chanting, we do sitting meditation.

03:11.000 --> 03:13.000
After sitting meditation, she didn't stop.

03:13.000 --> 03:15.000
She didn't do the rest of the chanting.

03:15.000 --> 03:16.000
We all got up.

03:16.000 --> 03:20.000
And I sat there from my room looking through the hallway at her sitting in the hall for a while.

03:20.000 --> 03:22.000
And after about five minutes, she got up.

03:22.000 --> 03:26.000
She opened her eyes, got up, and walked away.

03:26.000 --> 03:33.000
So this experience, for the first time, where the meditator enters into cessation,

03:33.000 --> 03:40.000
and is able to see the difference between what one's thought was happiness.

03:40.000 --> 03:50.000
And this cessation of suffering, and realizes that all things that arise are unsatisfying,

03:50.000 --> 03:54.000
are not happiness, are not beneficial.

03:54.000 --> 04:00.000
What one loses one's interest in them, loses the attachment to them.

04:00.000 --> 04:06.000
The first realization of Nimbana doesn't free one from attachment to things.

04:06.000 --> 04:09.000
One will become less attached to things through this realization,

04:09.000 --> 04:11.000
because one now has something to compare it to.

04:11.000 --> 04:16.000
Before the only thing one could think of is happy, where a risen experience is.

04:16.000 --> 04:22.000
So maybe ice cream is happiness, or chocolate is happiness, or sex is happiness, or drugs are happiness,

04:22.000 --> 04:25.000
or heaviness is happiness, or so on.

04:25.000 --> 04:27.000
This is what one thought was happiness.

04:27.000 --> 04:31.000
Sometimes after them, finds no satisfaction, because everything is changing,

04:31.000 --> 04:35.000
and everything is impermanent, and finds that all one is gaining through this is more and more greed,

04:35.000 --> 04:39.000
more and more attachment, more and more addiction.

04:39.000 --> 04:44.000
And when one realizes Nimbana, for the first time,

04:44.000 --> 04:48.000
one has realized something that is totally free from any attachment,

04:48.000 --> 04:55.000
has no building up of craving, has no stress, no agitation of mind,

04:55.000 --> 04:57.000
it's pure peace.

04:57.000 --> 05:01.000
The next moment when one arises from this first experience

05:01.000 --> 05:05.000
is the happiest moment of the person's life.

05:05.000 --> 05:09.000
It will, of one's career and some sorrow, really,

05:09.000 --> 05:13.000
because before that one had never, in all of the rounds of some sorrow,

05:13.000 --> 05:15.000
never come in contact with the Buddhist teaching,

05:15.000 --> 05:21.000
never gotten to this point, where one practiced intensively this teaching,

05:21.000 --> 05:26.000
and had never before experienced the state,

05:26.000 --> 05:29.000
where all things disappear, where there's no seeing, no hearing,

05:29.000 --> 05:32.000
no smelling, no tasting, no feeling, no thinking.

05:32.000 --> 05:36.000
So regardless of how, from people who hadn't realized

05:36.000 --> 05:41.000
that this would sound more terrifying, or horrible,

05:41.000 --> 05:46.000
or undistotally undesirable, the cessation of seeing, hearing,

05:46.000 --> 05:49.000
smelling, tasting, feeling, and thinking.

05:49.000 --> 05:52.000
Because it's devoid of all of those things,

05:52.000 --> 05:55.000
which one thought would be a cause of happiness.

05:55.000 --> 06:00.000
When the Sotapana arises from the attainment of Sotapana,

06:00.000 --> 06:05.000
of Sotapatimanga, Sotapatimala,

06:05.000 --> 06:10.000
they have changed this opinion about reality.

06:10.000 --> 06:15.000
And they have changed the idea that anything might be permanent,

06:15.000 --> 06:17.000
or anything might be lasting.

06:17.000 --> 06:20.000
They lose three things as a result of this.

06:20.000 --> 06:23.000
As a result of changing their idea of what is happiness

06:23.000 --> 06:26.000
and realizing the true peace and true happiness,

06:26.000 --> 06:29.000
which leaves them with a clear and pure mind.

06:29.000 --> 06:34.000
A person who has entered into it can be in a state of bliss for days,

06:34.000 --> 06:40.000
just on cloud nine for a great period of time,

06:40.000 --> 06:42.000
because of the change in their existence.

06:42.000 --> 06:46.000
Talking to some people who seem to have gotten this experience,

06:46.000 --> 06:48.000
were in no position to judge,

06:48.000 --> 06:51.000
who their whole character changes.

06:51.000 --> 06:55.000
And they're just in complete bliss and contentment

06:55.000 --> 06:59.000
for a great number of days until the defilements that are left start coming back

06:59.000 --> 07:03.000
and start affecting their mind again.

07:07.000 --> 07:12.000
But at that moment of coming back, they're free of three things.

07:12.000 --> 07:14.000
They may still have defilements and the defilements

07:14.000 --> 07:16.000
and the defilements will slowly come back,

07:16.000 --> 07:22.000
but they know something new that is irreversible.

07:22.000 --> 07:24.000
You could think of it like you have this damn.

07:24.000 --> 07:28.000
And as long as the damn is fully intact,

07:28.000 --> 07:32.000
you know that it could last for a great period of time.

07:32.000 --> 07:35.000
But as soon as you see the first crack in the damn,

07:35.000 --> 07:37.000
this damn can never be fixed,

07:37.000 --> 07:39.000
and eventually the damn is going to break,

07:39.000 --> 07:41.000
and the water is going to be released.

07:41.000 --> 07:44.000
And the same way the Sotupana has entered into some kind of...

07:44.000 --> 07:46.000
has entered into the stream.

07:46.000 --> 07:49.000
This is what the meaning of the word Sotah means.

07:49.000 --> 07:51.000
They've changed something about themselves,

07:51.000 --> 07:55.000
and they have entered into a non-static state.

07:55.000 --> 07:58.000
This state is not something that is going to stay still.

07:58.000 --> 08:02.000
They're headed upstream, you might say,

08:02.000 --> 08:06.000
or they're headed in the direction of Nimana.

08:06.000 --> 08:08.000
It's kind of like the damn is cracked,

08:08.000 --> 08:12.000
and it's only a matter of time before it bursts loose.

08:12.000 --> 08:14.000
So they lose three things.

08:14.000 --> 08:18.000
They lose wrong view, wrong view of self-sakya-diti.

08:18.000 --> 08:21.000
They lose any idea that there is a permanent soul,

08:21.000 --> 08:26.000
because this is the profound thing about the cessation of all experience.

08:26.000 --> 08:29.000
You realize that all that exists is experience.

08:29.000 --> 08:31.000
Without experience, there's nothing.

08:31.000 --> 08:35.000
There is Nimana, there is the cessation,

08:35.000 --> 08:38.000
which we call Nimana.

08:38.000 --> 08:47.000
And so as a result, this idea of a self has no place,

08:47.000 --> 08:51.000
has no meaning for them anymore.

08:51.000 --> 08:54.000
They lose the attachment to rights and rituals,

08:54.000 --> 08:59.000
or practice that is useless.

08:59.000 --> 09:02.000
They lose the idea that practice that is unbeneficial

09:02.000 --> 09:06.000
or of no benefit in leading towards the freedom from suffering,

09:06.000 --> 09:08.000
thinking that it is a benefit.

09:08.000 --> 09:10.000
They now know what is the right path.

09:10.000 --> 09:13.000
So they're able to see that what is useless is useless.

09:13.000 --> 09:15.000
They can see that doing rights and rituals

09:15.000 --> 09:20.000
and performing ceremonies that it's not the way to Nimana.

09:20.000 --> 09:24.000
It doesn't mean that they will stop and refuse to perform these things,

09:24.000 --> 09:27.000
but when they perform them, they'll know that this is meaningless

09:27.000 --> 09:31.000
or it's only meaningful because of the state of mind of the person.

09:31.000 --> 09:34.000
They come to see that it's the state of the mind of one's intention

09:34.000 --> 09:36.000
and one's clarity of mind that's most important.

09:36.000 --> 09:39.000
So when they do chanting, they'll try to use it

09:39.000 --> 09:44.000
as an opportunity to reflect on the wholesome qualities of the Buddha or so on.

09:44.000 --> 09:47.000
Or as they progress, they'll use the chanting

09:47.000 --> 09:50.000
as a time to be aware of their lips moving

09:50.000 --> 09:54.000
or of the feelings in the body and so on.

09:54.000 --> 09:57.000
They won't have the idea that chanting in and of itself

09:57.000 --> 09:59.000
has some power that's going to lead them to enlightenment

09:59.000 --> 10:02.000
or freedom from suffering.

10:02.000 --> 10:05.000
And so on, they don't have the idea that keeping this rule

10:05.000 --> 10:06.000
or that rule is important.

10:06.000 --> 10:09.000
They don't have the idea that, for example,

10:09.000 --> 10:13.000
not touching money, for example, is necessary to become enlightened

10:13.000 --> 10:15.000
so they don't hold on to the precepts as tightly.

10:15.000 --> 10:17.000
If they break a precept, they're not worried

10:17.000 --> 10:19.000
or feeling guilty about it.

10:19.000 --> 10:23.000
They just reprimand themselves and say that was wrong

10:23.000 --> 10:26.000
and they make an effort to not do it again and they confess it.

10:26.000 --> 10:28.000
But they don't feel upset about it.

10:28.000 --> 10:30.000
They don't think that, oh, now I'm going to go to hell

10:30.000 --> 10:32.000
because it's a concept.

10:32.000 --> 10:34.000
It's a convention.

10:34.000 --> 10:40.000
And so they don't cling to rules as well or morality.

10:40.000 --> 10:44.000
They understand it in terms of the mind state.

10:44.000 --> 10:46.000
So they understand that killing is always wrong

10:46.000 --> 10:47.000
or stealing is always wrong.

10:47.000 --> 10:50.000
But they understand that certain things are only concepts

10:50.000 --> 10:53.000
not eating after 12 o'clock if they eat in the afternoon.

10:53.000 --> 10:55.000
As a monk, for example, they won't feel guilty,

10:55.000 --> 10:57.000
but they'll maybe say to themselves,

10:57.000 --> 10:59.000
you've got to be more mindful and so on.

10:59.000 --> 11:02.000
And they will make a note and try to correct their behavior.

11:02.000 --> 11:04.000
That's it.

11:04.000 --> 11:06.000
The third thing is they lose doubt.

11:06.000 --> 11:09.000
So they lose doubt about the Buddha, doubt about the Buddha's teaching

11:09.000 --> 11:15.000
and doubt about what makes a person a enlightened disciple of the Buddha.

11:15.000 --> 11:18.000
So they understand what are the meaning of these three things

11:18.000 --> 11:22.000
and they understand that what the Buddha taught is the truth

11:22.000 --> 11:27.000
because they've realized it for themselves.

11:32.000 --> 11:35.000
So that's what the sutta is called being a sotepan.

11:35.000 --> 11:39.000
It's just one more thing because the Buddha goes and mentions

11:39.000 --> 11:41.000
this other kind of stream enter.

11:41.000 --> 11:44.000
It's called the jula sotepan, sotepan.

11:44.000 --> 11:48.000
The jula sotepan is someone who realizes the second stage of knowledge,

11:48.000 --> 11:50.000
altogether there are 16 stages of knowledge.

11:50.000 --> 11:53.000
When a person realizes the second stage of knowledge,

11:53.000 --> 11:55.000
it means they have an understanding of karma.

11:55.000 --> 11:59.000
How this works is in the beginning they see the body in the mind functioning.

11:59.000 --> 12:04.000
They're able to see the stomach rising and that's a phenomenon of body

12:04.000 --> 12:06.000
and they see the mind that goes out to the stomach

12:06.000 --> 12:08.000
and is aware of the stomach rising.

12:08.000 --> 12:12.000
So only when there is the stomach rising and the mind aware of it

12:12.000 --> 12:14.000
is there the experience of the stomach rising.

12:14.000 --> 12:17.000
If the mind is somewhere else or not thinking about the stomach,

12:17.000 --> 12:21.000
there's also no experience. One sees that there's the body in the mind.

12:21.000 --> 12:23.000
That's the first stage of knowledge.

12:23.000 --> 12:26.000
The second stage of knowledge and this is not intellectual

12:26.000 --> 12:30.000
is one begins to see how the body and the mind work together.

12:30.000 --> 12:32.000
So one wants to stand up and then there's the standing.

12:32.000 --> 12:34.000
One wants to sit down and then there's the sitting.

12:34.000 --> 12:38.000
There arises pain in the body and as a result there arises disliking.

12:38.000 --> 12:42.000
There arises pleasure in the body and as a result there arises liking.

12:42.000 --> 12:46.000
Because of the disliking and because of the liking there arises

12:46.000 --> 12:52.000
clinging in there arises partiality in there arises becoming and acting

12:52.000 --> 12:55.000
and stress and as a result suffering.

12:55.000 --> 12:58.000
And so as a result they start to see the truth of karma.

12:58.000 --> 13:02.000
A person who gets to this stage of knowledge has an understanding

13:02.000 --> 13:04.000
non-intellectual understanding of karma.

13:04.000 --> 13:10.000
They understand how defilements really inevitably lead to stress and suffering.

13:10.000 --> 13:14.000
And so as a result they have entered what we call the little stream

13:14.000 --> 13:19.000
which means whereas a Sotapana, another thing about the Sotapana

13:19.000 --> 13:24.000
is they're not liable to be born in states of whoa

13:24.000 --> 13:26.000
or states of suffering.

13:26.000 --> 13:28.000
They're not liable to be born in hell.

13:28.000 --> 13:30.000
They're not liable to be born as an animal.

13:30.000 --> 13:32.000
They're not liable to be born as a ghost.

13:32.000 --> 13:36.000
They can only be born as a human or an angel or a god.

13:36.000 --> 13:41.000
And eventually some of them will in one lifetime become enlightened.

13:41.000 --> 13:44.000
Some of them in two or three or four or five or six.

13:44.000 --> 13:48.000
But the longest it will take them to become fully enlightened

13:48.000 --> 13:52.000
and free from some saris said to be seven lifetimes.

13:52.000 --> 13:54.000
That's what the texts say.

13:54.000 --> 13:57.000
A Tula Sotapana doesn't have this reassurance.

13:57.000 --> 14:01.000
Obviously they haven't realized the reassurance that they have

14:01.000 --> 14:06.000
is that on realizing the second stage of knowledge in this lifetime

14:06.000 --> 14:10.000
at the end of this lifetime with the breakup of the body

14:10.000 --> 14:13.000
for this one lifetime.

14:13.000 --> 14:17.000
They will not be reborn in any of the states of suffering.

14:17.000 --> 14:19.000
They will not be reborn in hell.

14:19.000 --> 14:21.000
They will not be reborn as an animal.

14:21.000 --> 14:24.000
They will not be reborn as a ghost.

14:24.000 --> 14:29.000
So they are also said to be a Sotapana for that reason.

14:29.000 --> 14:32.000
But it's a Tula Sotapana and it's just a designation to mean

14:32.000 --> 14:36.000
that this is the stage that one needs to reach to be safe for this lifetime.

14:36.000 --> 14:40.000
Next lifetime, they will be born as a human or in a good state.

14:40.000 --> 14:45.000
They might still forget all of that and begin to practice on wholesome deeds

14:45.000 --> 14:52.000
and be born in hell as a result without any difficulty.

14:52.000 --> 14:59.000
As for the follow-up question,

14:59.000 --> 15:04.000
many people who lived in the Buddha's time said to be a streamwinner for sure.

15:04.000 --> 15:08.000
Many, many people, there were...

15:08.000 --> 15:11.000
movie Sakha became a Sotapana when she was seven years old

15:11.000 --> 15:13.000
and at the Pindika became a Sotapana.

15:13.000 --> 15:18.000
King Gimbisara became a Sotapana.

15:18.000 --> 15:20.000
They say in the commentaries,

15:20.000 --> 15:24.000
there's many mentions of Sotapana how the Buddha would give a talk

15:24.000 --> 15:27.000
and the whole crowd would become a Sotapana or so on.

15:27.000 --> 15:32.000
They say in Sawati where the Buddha spent a lot of his lifetime,

15:32.000 --> 15:40.000
only like 26 or 24 years,

15:40.000 --> 15:45.000
that there were seven Goti of people.

15:45.000 --> 15:48.000
And five Goti were...

15:48.000 --> 15:55.000
were Arya Pughalam in Sotapana or Sakha Di Gamir and Gamir Arahan.

15:55.000 --> 16:02.000
And one Goti were Galiana Pughalam, which means they were people

16:02.000 --> 16:05.000
who practiced morality and practiced meditation,

16:05.000 --> 16:07.000
but hadn't become enlightened yet.

16:07.000 --> 16:10.000
And then there were one last Goti who were Pughalalam,

16:10.000 --> 16:12.000
which means they were full of defilements

16:12.000 --> 16:14.000
and had no interest in the Buddha's teaching.

16:14.000 --> 16:16.000
So there's a story of this pig butcher,

16:16.000 --> 16:20.000
Junda, who lived very close to Jata Wana near or in Sawati.

16:20.000 --> 16:23.000
And every day the monks would go past

16:23.000 --> 16:28.000
and they thought it was remarkable that this guy lived so close to Jata Wana

16:28.000 --> 16:32.000
and he never came to listen to the Dhamma once

16:32.000 --> 16:35.000
or do any good deeds of giving charity

16:35.000 --> 16:36.000
or keeping morality or so on.

16:36.000 --> 16:38.000
He just killed pigs for his whole life

16:38.000 --> 16:40.000
and it was horrible how he killed pigs.

16:40.000 --> 16:43.000
I think we did a video about that already.

16:43.000 --> 16:45.000
But just an example.

16:45.000 --> 16:49.000
So I hope that's a fairly comprehensive explanation of Sotapana.

16:49.000 --> 16:52.000
Is there anything else that I can't think of anything?

16:52.000 --> 16:55.000
I think of anything that should be added.

16:55.000 --> 16:59.000
I mean, you know, there's always more that can be said.

16:59.000 --> 17:02.000
Lots of theory involved.

17:02.000 --> 17:08.000
But a Sotapana is a person who has followed the meditation practice

17:08.000 --> 17:10.000
to its consummation.

17:10.000 --> 17:12.000
At that point they're safe.

17:12.000 --> 17:15.000
They have reached a state of safety.

17:15.000 --> 17:18.000
And this is because of the...

17:18.000 --> 17:21.000
It's now become a habit and it begins to snowball

17:21.000 --> 17:24.000
and so it's not that they become content or they can become content.

17:24.000 --> 17:26.000
They can't become content.

17:26.000 --> 17:30.000
They have developed a state of energy, the state of confidence

17:30.000 --> 17:34.000
in the practice that will lead them to always think about practicing.

17:34.000 --> 17:37.000
Even though they might fall into liking or disliking

17:37.000 --> 17:39.000
and even get married and have children,

17:39.000 --> 17:43.000
we saw Kaha had 20 children or something.

17:43.000 --> 17:47.000
And she was a Sotapana.

17:47.000 --> 17:49.000
But they will always be thinking about meditation.

17:49.000 --> 17:51.000
Always be trying to find the time to meditate.

17:51.000 --> 17:54.000
Always be thinking about how they can develop themselves

17:54.000 --> 17:58.000
and always be interested in good things and spiritual things

17:58.000 --> 18:00.000
because it's stuck in their minds.

18:00.000 --> 18:04.000
If there's a crack, then that crack can't be fixed.

18:04.000 --> 18:27.000
It will eventually overflow and make them free.

