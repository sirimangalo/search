1
00:00:00,000 --> 00:00:05,000
In the Visudhi Magha, there's talk of Nimita's access concentration and casinos.

2
00:00:05,000 --> 00:00:11,000
Are these things dubious concepts or can most monks and monasteries attest to their reality and usefulness?

3
00:00:11,000 --> 00:00:14,000
Most monks can't agree on anything.

4
00:00:14,000 --> 00:00:22,000
So the issues that you've raised are some of them, I would say them.

5
00:00:22,000 --> 00:00:25,000
Not the more, but some of the contentious ones.

6
00:00:25,000 --> 00:00:33,000
And there's prominent monks who denounce them and denounce the Visudhi Magha or denounce these sorts of teachings.

7
00:00:33,000 --> 00:00:36,000
The Buddha himself used the word Nimita.

8
00:00:36,000 --> 00:00:40,000
And so the Visudhi Magha is just explaining it and trying to standardize it.

9
00:00:40,000 --> 00:00:44,000
Visudhi Magha is not trying to give doctrine, it's trying to give practice.

10
00:00:44,000 --> 00:00:46,000
And it's a practical manual.

11
00:00:46,000 --> 00:00:59,000
If you practice the casinos, you should be able to use the Visudhi Magha as a practical guide to develop Janda.

12
00:00:59,000 --> 00:01:05,000
Whether or not it's going to be exactly as is shown in the Visudhi Magha is not really necessary.

13
00:01:05,000 --> 00:01:09,000
It's not really the most important thing, whether or not it's true or false.

14
00:01:09,000 --> 00:01:15,000
The point is whether it's practical and useful, which it is and which there are many monks.

15
00:01:15,000 --> 00:01:23,000
Most especially the Paak Sayada, who apparently uses the Visudhi Magha and goes through the whole Visudhi Magha.

16
00:01:23,000 --> 00:01:33,000
We tend to skip chapters in our tradition, but the Paak Sayada, apparently they can remember past lives and enter into Jana's with all sorts of casinos.

17
00:01:33,000 --> 00:01:39,000
I've heard many monks talk about entering into Jana based on casino practice.

18
00:01:39,000 --> 00:01:49,000
But the Nimita, the Bhatim Bhagavan Nimita, for example, some people say it doesn't arise, which seems silly to me.

19
00:01:49,000 --> 00:02:01,000
Access concentration, I mean, some of these concepts are so technical that it's really just argumentative to even discuss them.

20
00:02:01,000 --> 00:02:07,000
The best thing is to just accept them and say, well, does it work, does it enter into Jana if it does them?

21
00:02:07,000 --> 00:02:09,000
That's the problem.

22
00:02:09,000 --> 00:02:13,000
Or, for example, access neighborhood concentration, access concentration.

23
00:02:13,000 --> 00:02:20,000
Does it repress the defilements and if it hindrances?

24
00:02:20,000 --> 00:02:23,000
And if it does, then what's wrong with it?

25
00:02:23,000 --> 00:02:25,000
And who cares what you call it, really?

26
00:02:25,000 --> 00:02:36,000
Even if you can't say it's Jana or is Jana or can't determine is it access concentration or is it attainment concentration?

27
00:02:36,000 --> 00:02:38,000
Or is it momentary concentration?

28
00:02:38,000 --> 00:02:40,000
That's an even more contentious one.

29
00:02:40,000 --> 00:02:42,000
Does it work?

30
00:02:42,000 --> 00:02:45,000
Does it lead to peace happiness freedom from suffering?

31
00:02:45,000 --> 00:02:46,000
Does it lead to wisdom?

32
00:02:46,000 --> 00:03:09,000
Does it lead to enlightenment or not?

