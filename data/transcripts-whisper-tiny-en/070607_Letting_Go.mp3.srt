1
00:00:00,000 --> 00:00:13,000
The reason why we practice meditation is for the purpose of letting go.

2
00:00:13,000 --> 00:00:26,000
This is a very easy, clear way of explaining the teaching which I don't know what it gave to the world.

3
00:00:26,000 --> 00:00:30,000
This is the teaching on how to let go.

4
00:00:30,000 --> 00:00:34,000
This is a teaching which is very much prized.

5
00:00:34,000 --> 00:00:37,000
This is an idea which is very much prized in the world.

6
00:00:37,000 --> 00:00:39,000
The concept of letting go.

7
00:00:39,000 --> 00:00:42,000
We're being able to let go.

8
00:00:42,000 --> 00:00:50,000
So we go around telling each other that you just have to let go or you have to learn to let go.

9
00:00:50,000 --> 00:00:56,000
Why don't you just let go, let it go, or so on.

10
00:00:56,000 --> 00:01:10,000
But we find we try that the reality of it is that letting go is not as simple as just willing it or wishing it to be.

11
00:01:10,000 --> 00:01:15,000
So wishing ourselves not to hold on, not to attach.

12
00:01:15,000 --> 00:01:25,000
And so we look at the Lord Buddha's teaching as a real way to really and truly let go.

13
00:01:25,000 --> 00:01:33,000
A way to really and truly be free from our attachments and our addictions to things.

14
00:01:33,000 --> 00:01:36,000
Because it goes deeper than simply letting go.

15
00:01:36,000 --> 00:01:44,000
It gives an explanation of why it is that we hold on in the first place and how we do away with the causes of attachment.

16
00:01:44,000 --> 00:01:48,000
Because attachment is not the beginning.

17
00:01:48,000 --> 00:01:50,000
It doesn't just come a rise by itself.

18
00:01:50,000 --> 00:01:52,000
It doesn't just arise out of nothing.

19
00:01:52,000 --> 00:01:55,000
It arises out of causes and conditions.

20
00:01:55,000 --> 00:02:00,000
And so the Lord Buddha taught the causes and actions why we hold on, why we attach.

21
00:02:00,000 --> 00:02:03,000
And ultimately why we suffer.

22
00:02:03,000 --> 00:02:09,000
And so we have the core teaching of the Lord Buddha on dependent origination.

23
00:02:09,000 --> 00:02:14,000
And then probably it is Petitya Samupada.

24
00:02:14,000 --> 00:02:25,000
And it starts a vitya pateya sankara with ignorance as a condition that arises sankara or formations.

25
00:02:25,000 --> 00:02:30,000
And in this case it means mental formations or volitional tendencies,

26
00:02:30,000 --> 00:02:40,000
or volition, or in Buddhist terms karma as we've heard the word karma.

27
00:02:40,000 --> 00:02:45,000
It means an intention, either a good intention or a bad intention.

28
00:02:45,000 --> 00:02:51,000
Our intention to do things that we may get a result are which are desire for things to be so to be this way or be that way.

29
00:02:51,000 --> 00:02:57,000
A vitya sankara, so we see ignorance, a vitya means ignorance.

30
00:02:57,000 --> 00:03:07,000
Ignorance is the root cause of all karma, of all wishes and all desires for things to be that, so to be this or be that.

31
00:03:07,000 --> 00:03:20,000
And this is an explanation of what is the start, the very beginning of attachment comes from ignorance.

32
00:03:20,000 --> 00:03:28,000
And so the Buddha's teaching is all about learning to see clearly, because of course our attachments have taken object.

33
00:03:28,000 --> 00:03:31,000
When we attach, we are attaching to something.

34
00:03:31,000 --> 00:03:40,000
We add something in our mind or something in the world around us that we see here here as an aller taste or feel.

35
00:03:40,000 --> 00:03:45,000
And we hold on to these things because of, at the root, because of ignorance.

36
00:03:45,000 --> 00:04:08,000
Because ignorance leads us to desire for things, leads us to create our mental intentions and to do, to act out when our intentions by other people or hurting ourselves or putting out effort to get the things we want or to be free from the things which we don't want.

37
00:04:08,000 --> 00:04:16,000
But the actual explanation on how this then leads to attachment and to suffering, we have to continue on.

38
00:04:16,000 --> 00:04:24,000
So we have ignorance leads us to act out things, leads us to do things, leads us to create intention in our minds.

39
00:04:24,000 --> 00:04:30,000
But how does this intention work and how does it have to do with holding on?

40
00:04:30,000 --> 00:04:46,000
When we go on to Sankara, Bateya, Vinyan, Vinyan consciousness, with these things which we have done in the past, in say past lifetimes, then there arises consciousness.

41
00:04:46,000 --> 00:04:51,000
With these karma as a cause, then there arises consciousness.

42
00:04:51,000 --> 00:05:05,000
So in the beginning of when we're born there arises a consciousness, we don't have to think yet about past life. If we start about at the moment of conception when there arises consciousness in the womb, this is the start of everything.

43
00:05:05,000 --> 00:05:12,000
It comes as a cause of our intentions in the past life. But we start simply here from consciousness.

44
00:05:12,000 --> 00:05:17,000
And this is going to come back again to ignorance and to karma.

45
00:05:17,000 --> 00:05:23,000
And we start here at Vinyan, Vinyan consciousness.

46
00:05:23,000 --> 00:05:46,000
Then we have continuing on Vinyan up at the Namarupa, Namarupa, with consciousness as a cause, as a condition, there arises body and mind or object and subject, subject and object.

47
00:05:46,000 --> 00:05:50,000
So when there is consciousness, there is consciousness of something.

48
00:05:50,000 --> 00:06:02,000
And so when we are first a rise in the womb of our mother, we have consciousness as our body progresses and our eyes and ears and nose and tongue.

49
00:06:02,000 --> 00:06:06,000
In the body then we have the six senses.

50
00:06:06,000 --> 00:06:11,000
And the six senses are basically Namarupa to the object and the subject.

51
00:06:11,000 --> 00:06:16,000
So the subject is the Vinyan or the knowing of the object.

52
00:06:16,000 --> 00:06:22,000
And the object itself is a sight or a sound or a smile or a taste or a feeling or a thought.

53
00:06:22,000 --> 00:06:28,000
So with the object and subject, there comes the Salayatana and the six senses.

54
00:06:28,000 --> 00:06:35,000
And when we have consciousness then there arises body and mind and there arises the six senses.

55
00:06:35,000 --> 00:06:43,000
Now with the six senses as the cause, the seeing, hearing, smelling, tasting, feeling and thinking, there arises contact.

56
00:06:43,000 --> 00:06:49,000
And this is all part of the same experience.

57
00:06:49,000 --> 00:06:53,000
Contact means contact between the mind and the object.

58
00:06:53,000 --> 00:06:56,000
And contact is the important part.

59
00:06:56,000 --> 00:06:59,000
This is where we start getting back to ignorance.

60
00:06:59,000 --> 00:07:07,000
Because when there is contact, when the mind touches the eye and there arises seeing or when the mind touches the ear and there arises hearing,

61
00:07:07,000 --> 00:07:15,000
we can see that the eye and the ear and the nose and the tongue and the body and the heart, they are there all the time.

62
00:07:15,000 --> 00:07:19,000
Our eyes are sometimes open and yet we don't see the things in front of us.

63
00:07:19,000 --> 00:07:23,000
Why our mind is somewhere else, our mind hasn't come in contact with the eye.

64
00:07:23,000 --> 00:07:29,000
Where we have ears but where oblivious to the things people are saying around us.

65
00:07:29,000 --> 00:07:31,000
Why? Because our mind is occupied with something else.

66
00:07:31,000 --> 00:07:34,000
Or smelling or tasting or feeling.

67
00:07:34,000 --> 00:07:37,000
Or thinking sometimes we are not even aware of what we are thinking.

68
00:07:37,000 --> 00:07:44,000
Because the mind is preoccupied with something else or the mind is not aware of the thought.

69
00:07:44,000 --> 00:07:54,000
But when there does arise, the contact, when we see something in the mind, the mind goes out to the eye and there is seeing,

70
00:07:54,000 --> 00:08:05,000
then there arises weight and a salayat and a pasa pateya, weight and a contact as a condition arises feeling.

71
00:08:05,000 --> 00:08:08,000
And this is where everything starts.

72
00:08:08,000 --> 00:08:16,000
So when we see something, because of the mind and the eye and the light touching the eye, then there arises a feeling.

73
00:08:16,000 --> 00:08:18,000
A happy feeling if it is something we enjoy.

74
00:08:18,000 --> 00:08:21,000
An unhappy feeling if it is something that we don't enjoy.

75
00:08:21,000 --> 00:08:27,000
And if it is neither nor, then there is a neutral feeling.

76
00:08:27,000 --> 00:08:30,000
But at any rate there arises one of the three kinds of feelings.

77
00:08:30,000 --> 00:08:34,000
And this is where we come back again to what happened in our past life.

78
00:08:34,000 --> 00:08:42,000
And we are going to continue the same thing over in this life where a vitya, a vitya pateya sankala.

79
00:08:42,000 --> 00:08:50,000
At the time when we feel something, if we feel happy and we have ignorance, we are not clearly aware that this is simply a feeling there will arise tana.

80
00:08:50,000 --> 00:08:53,000
Tana, which is craving.

81
00:08:53,000 --> 00:09:01,000
So as vityana pateya tana, with feeling as a condition there arises craving.

82
00:09:01,000 --> 00:09:07,000
So when we see something that we like, we are sitting here and we see something beautiful.

83
00:09:07,000 --> 00:09:10,000
There arises craving, wanting for it.

84
00:09:10,000 --> 00:09:19,000
If something unpleasant arises, we see or we hear or we smell, then there arises a different type of craving, a craving for it not to be.

85
00:09:19,000 --> 00:09:27,000
I am craving to be rid of the object of our discomfort.

86
00:09:27,000 --> 00:09:31,000
And if we have a neutral feeling, then there arises a craving for it to continue.

87
00:09:31,000 --> 00:09:38,000
For the calm, for the peace to continue, there arises a attachment to being that in that way.

88
00:09:38,000 --> 00:09:46,000
And the craving towards being at peace and having the peace of feeling continue.

89
00:09:46,000 --> 00:09:54,000
And this arises again because of a vitya, but we have to go back to the beginning.

90
00:09:54,000 --> 00:10:00,000
At the moment when we're feeling arises, when we feel something happy feeling because of the vitya, we hear or we smell.

91
00:10:00,000 --> 00:10:03,000
We're unhappy feeling or neutral feeling.

92
00:10:03,000 --> 00:10:10,000
If we don't have mindfulness, if we're not simply aware that this is a feeling, this is where we start to hold on.

93
00:10:10,000 --> 00:10:13,000
Because craving leads to upatana.

94
00:10:13,000 --> 00:10:15,000
Upatana.

95
00:10:15,000 --> 00:10:25,000
Upatana with craving as a condition there arises clinging.

96
00:10:25,000 --> 00:10:29,000
So now we see where everything comes from.

97
00:10:29,000 --> 00:10:38,000
Ignorance at the moment when there is feeling, when we see or we hear or we smell, leads to craving wanting it to continue.

98
00:10:38,000 --> 00:10:45,000
Not realizing that it's impermanent, that it's unsatisfying, that it's not under our control.

99
00:10:45,000 --> 00:10:49,000
Not seeing it simply as a part of nature, as a natural phenomenon.

100
00:10:49,000 --> 00:10:58,000
But taking pleasure in it, or becoming angry about it, or upset, or frustrated by it.

101
00:10:58,000 --> 00:11:02,000
And simply taking it for what it is, we've come to be intoxicated by it.

102
00:11:02,000 --> 00:11:04,000
And this is called Dunhauer craving.

103
00:11:04,000 --> 00:11:08,000
With craving as a condition then there arises clinging.

104
00:11:08,000 --> 00:11:10,000
And this is where holding on comes from.

105
00:11:10,000 --> 00:11:15,000
If at the moment when someone says something bad to us and we find we can't let it go,

106
00:11:15,000 --> 00:11:19,000
it's because at the moment when first the sounder eye arose,

107
00:11:19,000 --> 00:11:23,000
we weren't able to see that it was simply a sounder rising at the ear.

108
00:11:23,000 --> 00:11:31,000
When the sound then got into our minds and there arose thoughts of hatred or thoughts of anger or thoughts of displeasure,

109
00:11:31,000 --> 00:11:35,000
then we weren't able to see that these were simply thoughts and emotions,

110
00:11:35,000 --> 00:11:39,000
and not under our control, not ours to hold on to,

111
00:11:39,000 --> 00:11:42,000
not something that we should cling to.

112
00:11:42,000 --> 00:11:46,000
When we didn't see it clearly in this way then there became craving,

113
00:11:46,000 --> 00:11:48,000
a wanting, a wish for it to be like this.

114
00:11:48,000 --> 00:11:52,000
And this gets back to a week's habit to us on cover.

115
00:11:52,000 --> 00:11:56,000
So it goes in circles, in this life we're just continuing what we've done in the past life.

116
00:11:56,000 --> 00:11:58,000
We're creating more karma.

117
00:11:58,000 --> 00:12:01,000
And when we die, when we pass away, then there will be another reunion,

118
00:12:01,000 --> 00:12:03,000
and there will be more consciousness.

119
00:12:03,000 --> 00:12:06,000
We'll be born again and again and again and again and again.

120
00:12:06,000 --> 00:12:10,000
The way to become free, the way to really let go is to create a Vicha,

121
00:12:10,000 --> 00:12:25,000
which is nascent or science or understanding or wisdom knowledge you could say.

122
00:12:25,000 --> 00:12:32,000
When there arises knowledge that the things which arise in front of us are impermanent,

123
00:12:32,000 --> 00:12:36,000
unsatisfying and are not ours or not under our control,

124
00:12:36,000 --> 00:12:39,000
then we're able to see them for what they are and let them come and let them go

125
00:12:39,000 --> 00:12:43,000
and not crave for them to be this way or that.

126
00:12:43,000 --> 00:12:46,000
We not need for it to be like this or like that.

127
00:12:46,000 --> 00:12:48,000
And so not cling to anything.

128
00:12:48,000 --> 00:12:57,000
Of course, when we cling then it leads us to seek out and to take action,

129
00:12:57,000 --> 00:13:01,000
needing things to be like this or be like that when it's a pleasant things,

130
00:13:01,000 --> 00:13:06,000
working and working hard to keep the objects of our pleasure.

131
00:13:06,000 --> 00:13:11,000
And of course, since they're impermanence and they're unsatisfying since they're not under our control,

132
00:13:11,000 --> 00:13:16,000
then we find ourselves often meeting with things which are undesirable

133
00:13:16,000 --> 00:13:20,000
or losing the things which are the object of our desire.

134
00:13:20,000 --> 00:13:25,000
And of course, this is the truth of suffering, the truth of what it means

135
00:13:25,000 --> 00:13:30,000
or the truth which we have to face, which is called suffering.

136
00:13:30,000 --> 00:13:37,000
And this is a detailed explanation of the cause of suffering that it comes from ignorance.

137
00:13:37,000 --> 00:13:40,000
Because we can't see things simply for what they are,

138
00:13:40,000 --> 00:13:44,000
then we crave for them either to continue or for them to disappear.

139
00:13:44,000 --> 00:13:47,000
And when we crave for them like this or like that,

140
00:13:47,000 --> 00:13:50,000
then it leads to a clinging, it leads to an addiction.

141
00:13:50,000 --> 00:13:54,000
When we get what we want or when we are able to do away with what we don't want,

142
00:13:54,000 --> 00:14:00,000
then it becomes a part of our habit that every time that phenomenon arises,

143
00:14:00,000 --> 00:14:06,000
we're forced into a position of clinging, having a needing for it to be this way or that way,

144
00:14:06,000 --> 00:14:10,000
not able to bear with things simply as they are.

145
00:14:10,000 --> 00:14:18,000
And this leads for us then to leads to us then making effort or putting out effort

146
00:14:18,000 --> 00:14:20,000
to keep things the way they are.

147
00:14:20,000 --> 00:14:24,000
Keep things in a certain way and set up and control our lives.

148
00:14:24,000 --> 00:14:29,000
And when of course, our lives in the whole world around us are not under our control.

149
00:14:29,000 --> 00:14:32,000
Or something which are much bigger than us involved in it,

150
00:14:32,000 --> 00:14:38,000
natural laws of impermanence, of unsatisfaction,

151
00:14:38,000 --> 00:14:43,000
a dissatisfaction and of uncontrollability,

152
00:14:43,000 --> 00:14:48,000
which are a part of nature, which are a part of the way things are.

153
00:14:48,000 --> 00:14:53,000
And so this leads us to suffer when we don't care what we want to suffer when we get things,

154
00:14:53,000 --> 00:15:00,000
which are unpleasant to us, when our wishes are unfulfilled and so on and so on.

155
00:15:00,000 --> 00:15:05,000
And this is an understanding of what it means to hold on.

156
00:15:05,000 --> 00:15:08,000
So of course what it means to let go is when we do see clearly.

157
00:15:08,000 --> 00:15:12,000
When we see clearly something which arises.

158
00:15:12,000 --> 00:15:17,000
So at the moment when we're doing walking meditation or sitting meditation and then emotion arises

159
00:15:17,000 --> 00:15:20,000
and we're able to catch the emotion and simply say to ourselves,

160
00:15:20,000 --> 00:15:25,000
I agree, angry or wanting, wanting and let it go.

161
00:15:25,000 --> 00:15:29,000
Not getting to the point where we need it to be this and we need it to be that

162
00:15:29,000 --> 00:15:34,000
or even to the point where it makes an impression on our minds

163
00:15:34,000 --> 00:15:37,000
where we like it or we are displeased by it.

164
00:15:37,000 --> 00:15:42,000
We simply let it arise and let it cease as it naturally does anyway,

165
00:15:42,000 --> 00:15:45,000
without having for it to be any other way.

166
00:15:45,000 --> 00:15:49,000
Then there arises no craving, there arises no clinging,

167
00:15:49,000 --> 00:15:53,000
there arises no suffering needing things to be in this way or that way.

168
00:15:53,000 --> 00:15:57,000
And this is what it means in Buddhism what it means to let go.

169
00:15:57,000 --> 00:16:01,000
It means to create wisdom, to see clearly.

170
00:16:01,000 --> 00:16:05,000
And so wisdom is held to be the highest virtue in Buddhism.

171
00:16:05,000 --> 00:16:09,000
Simply letting go by itself cannot occur without seeing clearly.

172
00:16:09,000 --> 00:16:15,000
You cannot force yourself to let go of the things which you like or the things which you dislike.

173
00:16:15,000 --> 00:16:20,000
This becomes some sort of repression forcing yourself into a position

174
00:16:20,000 --> 00:16:23,000
which is not in line with your own understanding.

175
00:16:23,000 --> 00:16:28,000
Where you force yourself to believe something which you don't believe.

176
00:16:28,000 --> 00:16:34,000
When something you enjoy arises, you force yourself to think that it's undesirable.

177
00:16:34,000 --> 00:16:37,000
When in fact in your own view it is desirable.

178
00:16:37,000 --> 00:16:40,000
When this creates repression and this creates more mental sickness

179
00:16:40,000 --> 00:16:43,000
and can even lead to bodily sickness.

180
00:16:43,000 --> 00:16:47,000
At the time when you see clearly that these things which we think are desirable

181
00:16:47,000 --> 00:16:53,000
are actually merely a cause for more suffering because of our later attachment to them.

182
00:16:53,000 --> 00:16:58,000
When we see clearly in this way then we don't wish for them we don't desire for them.

183
00:16:58,000 --> 00:17:00,000
And so we don't cling to them.

184
00:17:00,000 --> 00:17:10,000
And so we can see the true way of letting go is actually part of a larger process

185
00:17:10,000 --> 00:17:16,000
which comes from seeing clearly that these things are not truly desirable.

186
00:17:16,000 --> 00:17:20,000
And so not desiring them and not then clinging to them.

187
00:17:20,000 --> 00:17:25,000
Having an arrival not letting the attachment arise in the first place.

188
00:17:25,000 --> 00:17:30,000
And so this is an exposition of dependent origination as well.

189
00:17:30,000 --> 00:17:36,000
The Buddhist concept of where attachment comes from, where suffering comes from.

190
00:17:36,000 --> 00:17:41,000
And how letting go occurs and how suffering therefore ceases.

191
00:17:41,000 --> 00:17:45,000
And so this is the demo which I'll give today.

192
00:17:45,000 --> 00:17:47,000
And so now we practice together.

193
00:17:47,000 --> 00:17:53,000
And do mindful frustration and then walking and then sitting to according to your own time.

194
00:17:53,000 --> 00:18:18,000
And we'll be here until 2 p.m.

