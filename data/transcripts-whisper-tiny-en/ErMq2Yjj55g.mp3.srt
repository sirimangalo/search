1
00:00:00,000 --> 00:00:15,280
Okay, good evening, welcome to our live broadcast.

2
00:00:15,280 --> 00:00:23,060
Tonight we have yet another dump on the square, it's just a short one tonight.

3
00:00:23,060 --> 00:00:28,880
Tonight I will be heading back to Stony Creek and tomorrow morning, tomorrow morning I have

4
00:00:28,880 --> 00:00:36,280
an interview on the radio, the local McMaster radio to advertise our peace walk and to talk

5
00:00:36,280 --> 00:00:45,280
about the new Buddhism Association at McMaster, so good publicity there, and lots of other

6
00:00:45,280 --> 00:00:51,380
things going on, so we might end it a little early tonight, but we've got a very short

7
00:00:51,380 --> 00:01:16,300
dump upon us tonight, shouldn't take long, okay, we're ready, good evening and welcome back

8
00:01:16,300 --> 00:01:18,780
to our study of the dump upon that.

9
00:01:18,780 --> 00:01:26,460
Tonight we continue on with verse number 81, which reads as follows,

10
00:01:26,460 --> 00:01:52,380
which means just as a rock that is a single hunk in the sense of having no cracks or just

11
00:01:52,380 --> 00:02:03,700
solid piece of granite, the just as the winds don't stir the rock, don't disturb the

12
00:02:03,700 --> 00:02:16,980
rock, wind can never make it waver, even so a wong in regards to ninda, which is blame

13
00:02:16,980 --> 00:02:31,060
or criticism, insult, the basanal sat, which is praise, in the face of praise and blame,

14
00:02:31,060 --> 00:02:42,940
not some indian deep india, the wise do not quiver, the waiver, do not stir, this was given

15
00:02:42,940 --> 00:02:52,860
in regards to la kuntaka, buddha, a monk who was short, his name was buddha, or whether

16
00:02:52,860 --> 00:03:01,340
that was his name or not, buddha just means a royal or a high class person thing, but it

17
00:03:01,340 --> 00:03:02,340
could be a name.

18
00:03:02,340 --> 00:03:09,020
Yesterday, I think we had was also, no, yesterday was, sorry, the story that we had yesterday

19
00:03:09,020 --> 00:03:15,380
was similar to the one of buddha, Raja was a king named buddha, somebody is like, maybe

20
00:03:15,380 --> 00:03:21,100
a common name, maybe it's an abolition, we'll take it as a name, I guess, but they call

21
00:03:21,100 --> 00:03:30,860
them la kuntaka because he was a dwarf, he was short, to say, at least he was a dwarf.

22
00:03:30,860 --> 00:03:38,020
And like many dwarves have to go through, he suffered at the hands of the novice monks

23
00:03:38,020 --> 00:03:47,540
and the unconverted, they say, so those who hadn't yet realized the buddha's teaching,

24
00:03:47,540 --> 00:03:53,800
unconverted I think is an old form, the word would be the ordinary people, people who

25
00:03:53,800 --> 00:04:05,020
haven't come to see it on a deeper level, that things like taunting and poking fun at people

26
00:04:05,020 --> 00:04:10,420
because of their height, it's not a cool thing to do, so there are sorts of people.

27
00:04:10,420 --> 00:04:15,140
In fact, they had this sort of thing happened in Thailand when I was there, it's funny

28
00:04:15,140 --> 00:04:19,900
the sort of things that happened, it's a different culture, I guess, like if someone had

29
00:04:19,900 --> 00:04:24,860
dark skin, they'd make jokes about them being very black and we were up on a mountain once

30
00:04:24,860 --> 00:04:29,140
and one of the monks said, oh, if we leave you here, when we come back, the clouds will

31
00:04:29,140 --> 00:04:32,140
be all dark.

32
00:04:32,140 --> 00:04:37,900
But sort of things wouldn't fly in the West and things like calling people fat, they

33
00:04:37,900 --> 00:04:43,260
don't really have a problem with talking about people being fat or that kind of thing.

34
00:04:43,260 --> 00:04:48,740
But so there was one in Thailand, there was one monk who was very short and he was constantly

35
00:04:48,740 --> 00:04:53,180
being harassed by the other monks, just joking and picking on him.

36
00:04:53,180 --> 00:05:00,060
And I think another thing is in Thailand, they have thinner thicker skinned, which means

37
00:05:00,060 --> 00:05:06,180
they don't respond so strongly to insults or this isn't, I don't know.

38
00:05:06,180 --> 00:05:14,620
On the other hand, they've also seen monks get up and start fist fights, so yeah, different

39
00:05:14,620 --> 00:05:17,140
culture.

40
00:05:17,140 --> 00:05:21,900
But in the time of the Buddha, this was also a thing.

41
00:05:21,900 --> 00:05:27,220
But the story is, it's just a simple expression of the fact that he didn't respond.

42
00:05:27,220 --> 00:05:35,940
He didn't get upset, he didn't lash out at them, probably because he was an Arahan.

43
00:05:35,940 --> 00:05:45,420
But it's one of those stories that shows the expression, it's an example of the expression

44
00:05:45,420 --> 00:05:49,780
of an Arahan, how an Arahan deals with things.

45
00:05:49,780 --> 00:05:52,260
And it's good for us to compare with how we deal with things.

46
00:05:52,260 --> 00:05:58,420
If someone picked on your height or your weight or your color of your skin, that kind of

47
00:05:58,420 --> 00:05:59,540
thing, how would you feel?

48
00:05:59,540 --> 00:06:05,420
How do you handle it when someone points out flaws like your teeth are not straight or your

49
00:06:05,420 --> 00:06:07,580
head's going bald?

50
00:06:07,580 --> 00:06:13,540
Someone actually said something really funny today, I don't know, I don't know, I don't

51
00:06:13,540 --> 00:06:20,140
want to poke fun, it was really a nice someone, I mean, everyone's so young here, it's

52
00:06:20,140 --> 00:06:21,140
funny.

53
00:06:21,140 --> 00:06:25,500
That kind of surprises me that these people are actually third and fourth year university

54
00:06:25,500 --> 00:06:29,060
students because they seem very, very young all of a sudden.

55
00:06:29,060 --> 00:06:33,580
And it's like I'm in a time warp coming back so many years later.

56
00:06:33,580 --> 00:06:38,660
But this woman came up to me and the student came up to me and said, I wanted to know

57
00:06:38,660 --> 00:06:45,180
about Buddhism, what I didn't know why I was wearing robes actually, that's how people

58
00:06:45,180 --> 00:06:48,060
start the conversation, but she was interested and she's probably coming to the piece

59
00:06:48,060 --> 00:06:53,220
walk to her. But she said, how old are you? And I said 36 and she said, oh, you look

60
00:06:53,220 --> 00:07:03,740
really good for 36. That's why 36 is really is old and funny. I mean, that was actually

61
00:07:03,740 --> 00:07:08,620
a compliment and you can easily go the other way. I've heard that and people say, oh,

62
00:07:08,620 --> 00:07:15,900
you're only 36 in Thailand, it was like that. When people were surprised that I wasn't

63
00:07:15,900 --> 00:07:21,220
much older. And actually, when I was like 25, I was already going bald, I think, like

64
00:07:21,220 --> 00:07:28,980
you're only 25, which is of course wouldn't be good for the ego. But we're very, very

65
00:07:28,980 --> 00:07:33,460
much against holding on to the ego. So it's good to have these kind of things happen.

66
00:07:33,460 --> 00:07:45,540
It could be tested. But it's also good for us to see an example like this, who wasn't

67
00:07:45,540 --> 00:07:55,900
affected by praise or blame. Praise and blame are two of the eight, what we call Loki and

68
00:07:55,900 --> 00:08:02,340
them are Loki, the demons of the world, their truths in the world. And they say you can't,

69
00:08:02,340 --> 00:08:08,220
you can't be free from praise and blame. You can't say you're always going to be praised.

70
00:08:08,220 --> 00:08:14,620
There also can be translated as the vicissitudes of life. As in the Mongols, it says

71
00:08:14,620 --> 00:08:19,340
put as a local dummy, jittang yasana company. It says basically the same thing as this

72
00:08:19,340 --> 00:08:26,380
verse. The greatest blessing eight among the moon among is when someone who is touched

73
00:08:26,380 --> 00:08:35,180
by the dumbness of the world, the worldly dumbness, worldly truths, the characteristics

74
00:08:35,180 --> 00:08:41,780
of the world, the aspects of existence, jittang yasana company, whoever's mind doesn't

75
00:08:41,780 --> 00:08:49,940
hear. Basically the same thing. This is the greatest blessing. And it's really a good

76
00:08:49,940 --> 00:08:57,380
expression of our practice. So to jump right into what this verse means to our practice,

77
00:08:57,380 --> 00:09:02,420
this is really what it's all about. Because if you've started practicing in this tradition,

78
00:09:02,420 --> 00:09:12,940
you can see that the mind is very apt or, you know, very apt, very quick to waver, quick

79
00:09:12,940 --> 00:09:18,620
to respond, quick to react. And this is what we mean by wavering. Forget about praise

80
00:09:18,620 --> 00:09:25,820
and blame, just sitting still, we react to pain, we react to itching, we react to heat

81
00:09:25,820 --> 00:09:31,780
and cold and noise if we're trying to meditate and there's noise, we react to our thoughts,

82
00:09:31,780 --> 00:09:38,900
we react to our emotions, we react just about everything. So our mind is constantly wavering.

83
00:09:38,900 --> 00:09:45,460
And then it extends into our life as well. Throughout our life, throughout our daily life,

84
00:09:45,460 --> 00:09:51,380
we're worried about this, afraid of that, angry about this, wanting that tossed and

85
00:09:51,380 --> 00:09:58,740
turned by the vicissitudes of life. And so the answer to a lot of life's problems are simply

86
00:09:58,740 --> 00:10:09,220
stop wavering, stop reacting, stand firm like a mountain, like a stone, stand firm like a rock,

87
00:10:11,460 --> 00:10:24,420
unwavering, unmoved, unshaken. It's, I guess it's a little bit, we have a little bit of a wrong

88
00:10:24,420 --> 00:10:28,580
idea, usually, of what it means to not waver. We think that somehow you have to force yourself,

89
00:10:28,580 --> 00:10:37,540
not to react or so. And so we, in ordinary usage, we talk about people who talk about

90
00:10:37,540 --> 00:10:45,940
being strong, stay strong, like as though you have to repress your feelings and grit and

91
00:10:45,940 --> 00:10:55,780
merit or grin and merit or something, bite your teeth. Don't, don't react, keep a stiff up

92
00:10:55,780 --> 00:10:59,700
or lip, that kind of thing. But that's not what it means at all. And that's what's so great about

93
00:10:59,700 --> 00:11:04,900
insight meditation is, it's, it's an a total different category from any kind of

94
00:11:06,340 --> 00:11:14,740
forced repression of reactions. And, and that's important because what you realize after some time

95
00:11:14,740 --> 00:11:19,460
is repression doesn't really work, forcing yourself to be a good person, forcing yourself to

96
00:11:19,460 --> 00:11:28,980
to be strong, forcing yourself not to react, forcing yourself. In general, is unsustainable.

97
00:11:28,980 --> 00:11:34,180
So we start to get the idea, well, just forget that. It's just drink and be merry because

98
00:11:34,180 --> 00:11:40,340
there's no way to repression is not good. So unknowingly, in modern times, we've come to

99
00:11:41,220 --> 00:11:46,260
see what the profound thinkers in the Buddhist time, we're also seeing that there's only two

100
00:11:46,260 --> 00:11:51,300
extremes and you have to choose between them. And nobody, very few people are able to see the

101
00:11:51,300 --> 00:12:00,420
middle way between this tort self torture and indulgence. Usually it's one doesn't work,

102
00:12:00,420 --> 00:12:03,460
so we can try the other one. The other one didn't work, so we try the first one.

103
00:12:05,540 --> 00:12:09,700
The Buddha taught the middle way and that's why it's a, that's why this middle way idea is so

104
00:12:09,700 --> 00:12:16,260
important. It's the middle way between these two extremes. It's this not wavering. It's not

105
00:12:16,260 --> 00:12:23,700
repressing at all. It's seeing so clearly that you have nothing, nothing to react about. It's like

106
00:12:23,700 --> 00:12:29,300
you already knew you already expecting it. It doesn't surprise you. It doesn't impact on your mind

107
00:12:29,300 --> 00:12:35,860
at all. It comes totally known and understood into your mind when it arises. You understand it

108
00:12:35,860 --> 00:12:41,460
when it ceases. You understand it. And that's all that happens. You were at peace before it arose,

109
00:12:41,460 --> 00:12:48,900
you're at peace after it leaves. This is what we strive for. Not repression, not forcing,

110
00:12:48,900 --> 00:12:54,980
not going somewhere or becoming something, but giving up and letting go and seeing clearly

111
00:12:55,700 --> 00:13:00,340
and understanding the nature of our experience, the nature of reality.

112
00:13:00,340 --> 00:13:07,220
And then when someone calls you a buffalo, you know, I don't got it. I don't have a tail. How

113
00:13:07,220 --> 00:13:12,340
can I be a buffalo? Someone calls you short? Well, I'm short. I knew that already.

114
00:13:13,460 --> 00:13:19,620
So if you're fat and someone calls you fat, well, yeah, I'm fat. There's no reason to get upset.

115
00:13:19,620 --> 00:13:28,260
We're getting upset doesn't help, but it's a flawed reaction that hurts you. It causes

116
00:13:28,260 --> 00:13:40,420
friction between you and the other party. It doesn't make you any taller. It doesn't change

117
00:13:40,420 --> 00:13:55,620
reality. We get so incensed by these things. Sometimes we're reasonably so. For example,

118
00:13:55,620 --> 00:13:59,540
like judging people by the color of their skin is no small thing. It's obviously

119
00:13:59,540 --> 00:14:06,180
an incredible amount of suffering for large numbers of people. And so the same goes for

120
00:14:08,100 --> 00:14:12,180
prejudice against shorter, tall people and fat people, that kind of thing.

121
00:14:13,220 --> 00:14:19,780
There's no question that these novices were wrong. What's just amazing is that in the face of

122
00:14:19,780 --> 00:14:28,100
such wrong, there was no anger. There was no, there was no reaction. There was no backlash.

123
00:14:29,060 --> 00:14:36,100
There was no enmity from the part of la kum taka. But yeah, and that's what's great about

124
00:14:36,100 --> 00:14:42,180
anara hunt is they've let go. They've risen above this. They have no quarrel with anyone unless

125
00:14:42,180 --> 00:14:49,380
they're able to live with and be at peace with everyone. And everything no matter what

126
00:14:49,380 --> 00:14:57,220
dhamma's arise, whether it be praise or blame or fame or infamy or wealth or poverty,

127
00:14:58,820 --> 00:15:05,940
happiness or suffering, put us a little kandami. He did dungus in a company. Their mind doesn't

128
00:15:05,940 --> 00:15:15,540
wait for a soul come. They are unsaddened. They are free from sorrow, sorrowless,

129
00:15:15,540 --> 00:15:25,700
without any stains or stainless, means without any evil in their hearts. Came among the safe,

130
00:15:26,340 --> 00:15:31,700
true safety. In fact, invincibility, because if nothing can affect you, then no one can ever hurt

131
00:15:31,700 --> 00:15:41,940
you and no circumstance can ever cause you suffering because you don't react. You don't waiver

132
00:15:41,940 --> 00:15:49,380
or it's shaken. Unshaken. Eight among all among them. This is the highest blessing.

133
00:15:51,300 --> 00:16:01,220
And this is why ninda pasan sasu nna samin janti bandita. In regards to praise and blame,

134
00:16:02,180 --> 00:16:06,660
the wise do not waiver. Are not unshaken, just like a rock,

135
00:16:06,660 --> 00:16:16,020
like a solid rock. It's not shaken by the wind. It's not moved by the wind. Samirati,

136
00:16:17,940 --> 00:16:25,300
it's not tossed about by the wind. So that's the dhamma panda for tonight. Thank you all for

137
00:16:25,300 --> 00:16:28,820
tuning in. Keep practicing and waiver.

138
00:16:28,820 --> 00:16:40,340
Okay, we're early, so that's good. So we can answer some questions, but I'm going to have to stop

139
00:16:40,340 --> 00:16:48,660
it at some point because I got to get ready to go to stormy creek and prepare for other things.

140
00:16:50,660 --> 00:16:58,260
Oh, I think we have a bunch of questions. First of all, before we get started, if you're looking at

141
00:16:58,260 --> 00:17:05,860
the archive of live stream talks, you'll notice that it's empty. You can thank me because I cleaned

142
00:17:05,860 --> 00:17:14,020
it out last night. So you're welcome. No, the truth is I deleted accidentally deleted all of the

143
00:17:14,020 --> 00:17:21,860
live stream archive about two gigabytes. So bye bye. That's what happens when we put a monk in

144
00:17:21,860 --> 00:17:28,580
charge of the web server. Yeah, they're gone in permanence.

145
00:17:32,580 --> 00:17:39,540
Yeah, the volume is low, huh? I don't know how to, but I guess I know how to fix that.

146
00:17:39,540 --> 00:17:52,500
It's just, well, I'm quiet anyway. It's my fault. Have to learn to speak up.

147
00:18:00,020 --> 00:18:07,140
So questions? We have questions. When it comes to animals, how is one to approach the subject of

148
00:18:07,140 --> 00:18:10,980
euthanasia or even giving them medication to ease their pain?

149
00:18:13,220 --> 00:18:20,020
Medication is the pain is reasonable to a certain extent, but euthanasia is not

150
00:18:21,700 --> 00:18:26,020
because death is a part of life, suffering is a part of life, and dealing with suffering

151
00:18:26,980 --> 00:18:32,820
is an important part of life. Cutting it off is not dealing with the pain. It's

152
00:18:32,820 --> 00:18:40,500
adding something to their problem, especially sending them on their way when they're in pain

153
00:18:40,500 --> 00:18:46,340
is a really bad idea, because any being that dies when they're in pain is not going to be in a

154
00:18:46,340 --> 00:18:50,340
good state. If they're suffering from the pain, that's a real reason to keep them around

155
00:18:50,980 --> 00:18:53,780
until they're ready to deal with, until they're at peace with the pain.

156
00:18:56,020 --> 00:18:59,620
But even still, when you murder, murder is a cause for a disruption of the mind.

157
00:18:59,620 --> 00:19:02,900
It's also unwholesome in your own mind.

158
00:19:06,420 --> 00:19:08,900
Is it possible to reincarnate in the past?

159
00:19:09,860 --> 00:19:11,140
I have no idea what that means.

160
00:19:15,380 --> 00:19:21,220
What is the difference between Jita and Vinaya? Vinaya?

161
00:19:21,220 --> 00:19:26,020
Vinaya, Vinaya, since both are translated as consciousness?

162
00:19:26,020 --> 00:19:31,140
One has four letters, and the other has seven.

163
00:19:33,220 --> 00:19:34,180
Do you mean the same thing?

164
00:19:35,220 --> 00:19:39,620
They can. Jita can also mean all four of the namakanda, I think.

165
00:19:41,060 --> 00:19:41,700
Are you sad?

166
00:19:42,740 --> 00:19:47,460
Yeah, because Jita is in Abidama, Jita is the real one, Vinaya is just a

167
00:19:47,460 --> 00:19:55,300
I'll sweep the word, but Jita means all four of the khandas, I think.

168
00:19:58,020 --> 00:20:00,740
Again, but usually it's just they're used interchangeably.

169
00:20:02,180 --> 00:20:06,020
You always amazes me how many polywords there are for the same thing and the

170
00:20:07,620 --> 00:20:13,220
it's a little confusing. Is it common that after we begin meditation, we start to notice how

171
00:20:13,220 --> 00:20:18,500
more how other people are feeling? Yeah, well, you're more alert to a lot of things.

172
00:20:19,540 --> 00:20:23,940
You can pick up on people's body language, you can pick up on people's emotions,

173
00:20:24,740 --> 00:20:30,020
you're more objective, so you have you're more there for them, you're less obsessed with your

174
00:20:30,020 --> 00:20:31,700
own problems, so for sure.

175
00:20:34,420 --> 00:20:38,980
And you can also learn to read people's minds or read people's hearts, so there are people who

176
00:20:38,980 --> 00:20:45,300
can actually sense other people's emotions. That can be probably pretty distracting.

177
00:20:48,980 --> 00:20:52,660
And someone wanted to know the demo about a verse you'd be teaching tonight, was that number

178
00:20:52,660 --> 00:20:55,300
81? 81, 81.

179
00:21:01,380 --> 00:21:04,500
What do you think of Ian Stevenson's research on reincarnation?

180
00:21:04,500 --> 00:21:09,540
I haven't looked too much into it. I think he did a good job.

181
00:21:11,300 --> 00:21:17,380
I think it's useful research. I'm sorry that it was disregarded by so many people.

182
00:21:18,420 --> 00:21:22,420
I think if you're interested in more thorough research, you should check out the people who

183
00:21:23,460 --> 00:21:28,100
continued his work on the people at the University of Georgia, I think,

184
00:21:28,100 --> 00:21:36,180
and Kelly and the other guy's names. There's two Kelly's and they wrote a book called Irreducible

185
00:21:36,180 --> 00:21:46,580
Mind. It's a really good book. Long, it's like a textbook, but it's worth reading. Irreducible Mind.

186
00:21:48,020 --> 00:21:52,340
But they've got lots of articles on the University of Georgia, the Division of Perceptual Studies,

187
00:21:52,340 --> 00:21:56,580
and these are all the people who came after Ian Stevenson in their research pretty neat.

188
00:21:56,580 --> 00:22:01,540
It's a little more advanced because Ian Stevenson was just a pioneer,

189
00:22:02,180 --> 00:22:06,820
but the people who came after were able to systemize, systematize the sort of teaching,

190
00:22:07,780 --> 00:22:09,940
the sort of research that he did.

191
00:22:12,340 --> 00:22:16,740
One day, is it possible that meditation could cause one's past deeds to be brought up in the

192
00:22:16,740 --> 00:22:21,780
present mind more often and more strongly than they would be otherwise? Thank you.

193
00:22:21,780 --> 00:22:26,260
Didn't we just have this question a couple of days ago?

194
00:22:28,420 --> 00:22:30,740
I think we actually did or something very similar.

195
00:22:33,460 --> 00:22:38,100
What's our rule on answering the same question to three days in a row?

196
00:22:41,460 --> 00:22:46,420
Is it typical to start to care less in terms of having an emotional reaction about what happens

197
00:22:46,420 --> 00:22:53,060
to others and to oneself after meditating for a while?

198
00:22:53,860 --> 00:22:59,700
Sorry, say again. Is it typical to start to care less in terms of having an emotional reaction

199
00:22:59,700 --> 00:23:03,220
about what happens to others and oneself after meditating for a while?

200
00:23:05,220 --> 00:23:12,660
Yeah, yeah, I mean you're more objective than you deal with things based on wisdom rather than

201
00:23:12,660 --> 00:23:18,500
emotion, so it doesn't mean you want to help people less, but you're less moved by it. You're

202
00:23:18,500 --> 00:23:25,940
less shaken by it. So like an arrow hand seems to be very much moved by people's suffering,

203
00:23:26,580 --> 00:23:33,300
but it's just functional. They're not actually upset about it. They just are immediately moved to

204
00:23:33,300 --> 00:23:43,540
help if they can. So if you find yourself not carrying someone falls and you have no thought

205
00:23:43,540 --> 00:23:48,100
that you should pick them up or someone wants to practice meditation and you think,

206
00:23:48,100 --> 00:23:52,980
I don't care why should I teach them, then maybe that you're doing something wrong.

207
00:23:54,820 --> 00:24:00,020
And just to follow up, I'm asking because non meditators seem to get mad when one does not react

208
00:24:00,020 --> 00:24:11,380
is expected. Well, yes, sometimes we can be a little hard-headed, but some we talked about this

209
00:24:11,380 --> 00:24:19,300
before. This is the problem people's inability to deal with impermanence. So when you change,

210
00:24:19,300 --> 00:24:24,180
they're unable to deal with it. That's more to do with their prior attachments to you or their

211
00:24:24,180 --> 00:24:30,980
attachments to the way people should react. So there's two different things. One, people who you've

212
00:24:30,980 --> 00:24:37,780
known before and two people who have an idea of the way a human being should act. They're both

213
00:24:37,780 --> 00:24:43,540
problems from meditators. It's true. And sometimes you have to just put on an act to some extent,

214
00:24:43,540 --> 00:24:47,700
you know, like go through the motions of being an ordinary person, fitting into your society.

215
00:24:47,700 --> 00:24:53,460
I think that's reasonable. You can consider it a functional thing where it's your duty to act like

216
00:24:53,460 --> 00:25:00,180
everybody else. I don't think that's wrong. It's a burden on you, but that's the burden of living

217
00:25:00,180 --> 00:25:05,220
in society. Any society, even monastics society, there are burdens like this. You have to act

218
00:25:06,260 --> 00:25:14,020
for the community. So that's advice I would give is to try to fit in. But it's not to say that

219
00:25:14,020 --> 00:25:23,860
they're right. It's just that, you know, that's how you deal with non-meditators.

220
00:25:23,860 --> 00:25:27,940
Hello, Bante. Why do we have questions? Why can't we just go and meditate? Thank you.

221
00:25:30,180 --> 00:25:33,300
That's not a real question, is it? I don't think that's a real question.

222
00:25:34,740 --> 00:25:37,620
Because you can go and just meditate if you want to.

223
00:25:37,620 --> 00:25:43,940
When I note thoughts, memories and feelings and meditation, I hesitate and think about the

224
00:25:43,940 --> 00:25:49,620
word or phrase I want to describe the specific thought. Is this common with some students and

225
00:25:49,620 --> 00:25:57,620
will hold back my practice? Sorry, I'm just doing something even that's not working.

226
00:26:00,660 --> 00:26:05,860
So the person is, when they note thoughts, memories and feelings, they hesitate and think about

227
00:26:05,860 --> 00:26:11,140
which word or phrase they want to describe the specific thought. It's common and will hold back my

228
00:26:11,140 --> 00:26:18,580
practice. It's common. It just means you're learning how to do the practice. It takes time to get

229
00:26:18,580 --> 00:26:26,500
good at it. It gets easier as you go along just like anything. Just be patient. But the other thing

230
00:26:26,500 --> 00:26:31,300
is it's helping you see that your mind is not totally under your control. So it's helping

231
00:26:31,300 --> 00:26:38,660
you to let go in and of itself. It's a good thing. Just showing you just to let go, teaching you

232
00:26:38,660 --> 00:26:47,140
how to, how things really work and forcing you to be more patient and more accepting or understanding.

233
00:26:50,100 --> 00:26:54,980
Hello, Ponte. When I find myself drifting off into thought and before I know it,

234
00:26:54,980 --> 00:27:00,340
I haven't been noting for maybe two or three minutes. It's frustrating, so I note frustrated.

235
00:27:00,340 --> 00:27:05,300
It doesn't seem to be getting better with time. I'm doing around one hour per day. Any advice

236
00:27:05,300 --> 00:27:13,060
or just keep practicing? Well, one hour a day is not a lot. It'll help you, but it's

237
00:27:13,060 --> 00:27:16,980
dropping the bucket really because then you've got 23 hours where you're not meditating.

238
00:27:18,180 --> 00:27:22,260
It's not to say that it's a bad thing. It's actually a great thing. If you can do an hour a day,

239
00:27:22,260 --> 00:27:27,220
that's pretty awesome. Just don't expect, in a week, you're going to be Superman.

240
00:27:27,220 --> 00:27:32,900
One thing you can do is try to incorporate meditation into your daily life,

241
00:27:32,900 --> 00:27:37,780
try to be mindful during the day. That should help, but certainly don't expect.

242
00:27:38,980 --> 00:27:44,980
I mean, another thing is that's not, you're perhaps looking in the wrong place. The benefit you

243
00:27:44,980 --> 00:27:49,460
should find is in your frustration. The fact that you can catch the frustration you wouldn't have

244
00:27:49,460 --> 00:27:53,780
caught it before, and that's a benefit of the practice. It's more important than catching your thoughts.

245
00:27:53,780 --> 00:28:00,260
You should also catch the desire to not think, the desire to not be distracted. That's also a

246
00:28:00,260 --> 00:28:05,300
bigger problem. Thoughts are just thoughts. You say, well, it gets in the way of my meditation,

247
00:28:05,300 --> 00:28:12,900
well, the meditation is your reactions to it. So when you like it or you dislike it, when you

248
00:28:12,900 --> 00:28:18,420
want something or you dislike what you get, that's where the meditation is really supposed to work.

249
00:28:18,420 --> 00:28:23,940
It's much more important. Don't worry so much about the distraction. That's natural if you're

250
00:28:23,940 --> 00:28:28,580
working during the day or doing whatever and only doing an hour of meditation in the day. It's

251
00:28:28,580 --> 00:28:48,420
natural for thoughts to write. I'll cut up on questions.

252
00:28:48,420 --> 00:29:06,340
All right. There was one more question that just slipped in if you have time.

253
00:29:06,340 --> 00:29:20,100
All right. Is it clear when you've become a Sothapana? It's clear, but you may not

254
00:29:20,100 --> 00:29:25,780
be able to identify it as Sothapana, but what's happened is clear. Something is clear. It's clear

255
00:29:25,780 --> 00:29:30,900
that something happens. That's about it. But there's nothing. There's no sign that says,

256
00:29:30,900 --> 00:29:39,380
congratulations, you are Sothapana. You've leveled up. There's nothing like that.

257
00:29:43,620 --> 00:29:46,820
What happens when all of your egoic desires have ceased to be?

258
00:29:48,340 --> 00:29:54,500
Okay, okay. Enough for today. Okay. Thank you, Bante. Thank you, Robin, for helping out.

259
00:29:54,500 --> 00:30:00,740
Good night, everyone.

