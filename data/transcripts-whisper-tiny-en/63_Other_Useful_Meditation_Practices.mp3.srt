1
00:00:00,000 --> 00:00:05,760
other useful meditation practices. You said in your ask among series,

2
00:00:05,760 --> 00:00:11,200
for people caught up in lust, it is good for them to contemplate the unpleasant aspects of the

3
00:00:11,200 --> 00:00:19,440
body. What is the proper way to do that? I did mention this and I think I mentioned as well that

4
00:00:19,440 --> 00:00:25,600
for people caught up in anger, it is good to practice friendliness, metta. Both belong to a set

5
00:00:25,600 --> 00:00:33,280
of four meditations called the Chaturaraka, kamatana. Araka means God. So these four meditations

6
00:00:33,280 --> 00:00:38,240
are meant to God, the person's mind, or they are something that protects you during the time that

7
00:00:38,240 --> 00:00:44,000
you are practicing the pastana meditation to see clearly. They are a sort of protection that will

8
00:00:44,000 --> 00:00:49,360
help keep the mind in a condition where it is able to see clearly, stopping it from falling into

9
00:00:49,360 --> 00:00:57,600
great states of lust, anger, and so on. The four kamatana meditations are mindfulness of the Buddha,

10
00:00:57,600 --> 00:01:03,600
mindfulness of the absence of beauty in the body, friendliness, and death. Each has different

11
00:01:03,600 --> 00:01:09,200
qualities and although none of them are necessary to attain enlightenment, they can be of practical

12
00:01:09,200 --> 00:01:15,360
value and so are recommended as generally supportive meditation practices as a complement to the

13
00:01:15,360 --> 00:01:22,480
practice of the pastana meditation. Mindfulness of the Buddha is most practical for Buddhists,

14
00:01:22,480 --> 00:01:28,640
people who consider the Buddha and his teachings as their guide, as means of bolstering our courage

15
00:01:28,640 --> 00:01:34,240
to undertake the difficult path towards purity and enlightenment. We practice recollecting the

16
00:01:34,240 --> 00:01:41,040
Buddha as an example to us, someone who was the perfect teacher, free from defilement and delusion,

17
00:01:41,040 --> 00:01:47,200
perfect in knowledge and conduct. We reflect on the characteristics of the Buddha, for instance,

18
00:01:47,200 --> 00:01:53,760
that he had great wisdom. We hear his teachings and gain great faith, reaffirming in our minds

19
00:01:53,760 --> 00:01:59,680
that yes indeed, he is one who had great wisdom, and so we should follow his teachings because

20
00:01:59,680 --> 00:02:06,640
they will certainly lead to the result he proclaims. We reflect on the great purity of the Buddha.

21
00:02:06,640 --> 00:02:13,840
Both he and his teachings are incredibly pure, free of dharma or superstition. The Buddha was pure

22
00:02:13,840 --> 00:02:19,760
in that he simply gave something to the world that was of use to all beings as opposed to looking

23
00:02:19,760 --> 00:02:25,920
for students or trying to become famous. We reflect on his great compassion, deciding to teach

24
00:02:25,920 --> 00:02:30,720
for the whole of his enlightened life. He could have sat in the forest and meditated and

25
00:02:30,720 --> 00:02:36,720
passed away, undisturbed, but when he was asked to teach, he gave up his own peaceful abiding to

26
00:02:36,720 --> 00:02:42,320
bring peace to others. There are many qualities of the Buddha that we recite and reflect upon,

27
00:02:42,320 --> 00:02:47,760
and although this is obviously something that is most useful for the Buddhists, I would encourage

28
00:02:47,760 --> 00:02:53,600
everyone to study about the qualities of the Buddha. One simple way to do this is to recite the

29
00:02:53,600 --> 00:03:01,440
Pali verse, recollecting his virtues and consider each one individually. So we say, vidya chiranasampa

30
00:03:01,440 --> 00:03:07,200
nu, which means, endured with knowledge and conduct, and reflect on the Buddha's greatness

31
00:03:07,200 --> 00:03:13,440
of knowledge and conduct and so on. The second meditation is the one you asked about,

32
00:03:13,440 --> 00:03:18,320
mindfulness of the unpleasant or literally not beautiful nature of the body.

33
00:03:18,320 --> 00:03:22,960
The view that the body is something beautiful is wrong. There is nothing intrinsically

34
00:03:22,960 --> 00:03:28,080
beautiful about the body. It could be argued that there is nothing intrinsically ugly about

35
00:03:28,080 --> 00:03:32,400
the body either, but in comparison to the perception of the body as beautiful,

36
00:03:32,400 --> 00:03:37,840
is generally due to not paying close attention to reality, whereas the perception of the body

37
00:03:37,840 --> 00:03:44,560
as ugly is generally as a result of paying attention. If you compare the body to gold or diamonds,

38
00:03:44,560 --> 00:03:51,200
or flower, the human body is in very high on any objective scale of beauty. This is made clear by

39
00:03:51,200 --> 00:03:56,000
the practice of reviewing the parts of the body. Starting with the hair on your head,

40
00:03:56,000 --> 00:04:01,440
its nature is like grass that has been planted in this skull, and if you do not wash it,

41
00:04:01,440 --> 00:04:06,800
smells and looks repulsive. Focusing your attention on hair as a meditation object,

42
00:04:06,800 --> 00:04:12,560
you quickly come to see that hair is not the attractive thing. We ordinarily perceive it as,

43
00:04:12,560 --> 00:04:18,480
we often perceive a person's hair as beautiful, but if a strand of hair falls into our food,

44
00:04:18,480 --> 00:04:24,800
we might become nauseous. When hair is cut off, it looks unappealing in clumps on the floor.

45
00:04:24,800 --> 00:04:30,160
People who keep their hair after cutting it are often met with revulsion. We go through all the

46
00:04:30,160 --> 00:04:38,800
various parts of the body as a reflection. Head hair, body hair, nails, teeth, skin, flesh, blood,

47
00:04:38,800 --> 00:04:47,040
bones, bone marrow, feces, urine, liver, spleen, heart and so on. Traditionally, one would learn

48
00:04:47,040 --> 00:04:55,920
and recite the bali names for each. Keisa, loma, naka, ventat, tako, mamsam, haru, ati,

49
00:04:55,920 --> 00:05:03,360
atimingum, etc. Through all 32 parts of the body, then break them up into groups or reflect on

50
00:05:03,360 --> 00:05:09,760
them individually, saying Keisa, Keisa, Keisa, and just repeat that word over and over,

51
00:05:09,760 --> 00:05:14,800
focusing on the concept of head hair in much the same way as we focus on experience in

52
00:05:14,800 --> 00:05:21,680
a bit past in a meditation. Except in this meditation, one focuses on a concept, the concept of

53
00:05:21,680 --> 00:05:26,880
head hair. You do not have to convince yourself that the body is ludsen. You just reflect

54
00:05:26,880 --> 00:05:31,520
mindfully about the parts of the body and slowly the delusion of beauty fades as you become

55
00:05:31,520 --> 00:05:37,280
more familiar with the true nature of the body. In some cases, it can happen that one does become

56
00:05:37,280 --> 00:05:42,640
repulsed as a result of this sort of practice. So it is best practiced by one who has great

57
00:05:42,640 --> 00:05:48,720
last for physical beauty to help free them of that misperception. The third meditation is

58
00:05:48,720 --> 00:05:53,920
friendiness, metta, which is often used as a means of dedicating the goodness gained through

59
00:05:53,920 --> 00:06:00,240
cultivating mindfulness to those we care for. After a formal meditation session, you can channel

60
00:06:00,240 --> 00:06:06,560
this strength and clarity of mind gained from meditation to express appreciation for all beings

61
00:06:06,560 --> 00:06:12,000
and to commit yourself to overcoming any conflict you might have with others. This practice

62
00:06:12,000 --> 00:06:17,040
can be of great benefit to the practice of the past in a meditation, helping to straighten out

63
00:06:17,040 --> 00:06:22,640
the crookedness we have in our minds. There are many ways to practice metta. You can extend good

64
00:06:22,640 --> 00:06:28,480
will consecutively in terms of proximity, wishing first for your parents to be happy, then for

65
00:06:28,480 --> 00:06:34,960
relatives, family, people nearby, people in your city, country, the whole world, and finally,

66
00:06:34,960 --> 00:06:40,640
the whole universe. Even a few minutes of this practice after finishing with the past in a meditation

67
00:06:40,640 --> 00:06:46,560
is useful for overcoming anger, strengthening relationships with others and straightening the mind.

68
00:06:46,560 --> 00:06:53,200
The fourth meditation is mindfulness of death, which is useful for cultivating a sense of urgency,

69
00:06:53,200 --> 00:06:57,840
reminding us that we cannot just go on with our lives as though they will know tomorrow.

70
00:06:58,480 --> 00:07:03,760
Eventually, they come the day of reckoning when our physical body dies. At the time of one

71
00:07:03,760 --> 00:07:09,760
step, they say your whole life flashes before your eyes and you will cling to whatever has the

72
00:07:09,760 --> 00:07:15,840
strongest emotional hold on your mind, leading you to be born again based on that experience.

73
00:07:15,840 --> 00:07:21,280
If it is a bad experience, your next life will be unpleasant. Remembering this possibility

74
00:07:21,280 --> 00:07:28,080
is an important practice to stop us from being negligent. There are many ways to cultivate mindfulness

75
00:07:28,080 --> 00:07:34,400
of death. The traditional method is to simply say to yourself something like life is uncertain,

76
00:07:34,400 --> 00:07:41,760
death is certain, or all beings have to die and I too will have to die one day. There is a story

77
00:07:41,760 --> 00:07:47,680
of the Bodhisattva, teaching his whole family mindfulness of death, so they were never complacent and when

78
00:07:47,680 --> 00:07:54,480
his son died, no one in the family was consumed by suffering at the loss. You can and should make

79
00:07:54,480 --> 00:08:00,640
use of all these meditations from time to time as appropriate. They are all beneficial and can be

80
00:08:00,640 --> 00:08:06,080
used together with vipassana meditation to great benefit.

