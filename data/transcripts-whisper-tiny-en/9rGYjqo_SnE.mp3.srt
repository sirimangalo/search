1
00:00:00,000 --> 00:00:05,000
Good evening and welcome back to our study of the Damapada.

2
00:00:05,000 --> 00:00:13,000
Today we continue on with first number 150, which reads as follows.

3
00:00:35,000 --> 00:00:50,000
And he made of bones, bong salohita nipanong that is plastered with flesh and blood.

4
00:00:50,000 --> 00:01:10,000
And here we are in all the age and death, mano makou, a conceit and deceit, ohito dwell, or are launched.

5
00:01:10,000 --> 00:01:29,600
And ascend upon and not sure about ohito, are lie, were in lie, old age and death, conceit

6
00:01:29,600 --> 00:01:38,200
and deceit, conceit and treachery or crookedness.

7
00:01:38,200 --> 00:01:45,200
So remember we are in the Jarawaga, I think, in Jarawaga, which means old age.

8
00:01:45,200 --> 00:01:50,200
So these are all going to be about old age, death, that kind of thing, the body.

9
00:01:50,200 --> 00:01:53,200
They're not going to be very pleasant, most likely.

10
00:01:53,200 --> 00:01:56,200
They haven't been so far.

11
00:01:56,200 --> 00:02:02,200
This story, one of those memorable stories, most of them are fairly memorable.

12
00:02:02,200 --> 00:02:12,200
And somewhat of fantastical stories, so if you're not fan of the miraculous bear with it,

13
00:02:12,200 --> 00:02:19,200
because it's not so much in the story, it's much more in the lesson that it has.

14
00:02:19,200 --> 00:02:48,200
And anyway, the story goes that this Buddhist nun will look upon and death, who is supposed to be the Buddha's cousin.

15
00:02:48,200 --> 00:02:52,200
Or her step-step sister, sister-in-law, something like that.

16
00:02:52,200 --> 00:02:55,200
Some relative in the Buddha, Rupananda.

17
00:02:55,200 --> 00:03:03,200
Rupananda, who was very beautiful.

18
00:03:03,200 --> 00:03:16,200
She was called genopanda kalyani, which means the beauty of the land, the most beautiful girl in the country.

19
00:03:16,200 --> 00:03:28,200
And she heard that, well, like many of the Buddha's relatives, everybody's becoming monks, and nun monks, female monks, male monks.

20
00:03:28,200 --> 00:03:33,200
And so she said, like many of them, fall that's follow along.

21
00:03:33,200 --> 00:03:35,200
You know, I'll do it too.

22
00:03:35,200 --> 00:03:43,200
Not really knowing or having any conception of what exactly the Buddha taught.

23
00:03:43,200 --> 00:03:52,200
And quite quickly, she became aware of the fact that the Buddha's teaching

24
00:03:52,200 --> 00:03:58,200
wasn't to have much good and disabled physical beauty.

25
00:03:58,200 --> 00:04:03,200
That physical beauty is impermanent.

26
00:04:03,200 --> 00:04:13,200
And it's impermanent and satisfying and controllable to cause for suffering.

27
00:04:13,200 --> 00:04:19,200
Something you shouldn't think of as me or mine, it's a cause for ego, conceit.

28
00:04:19,200 --> 00:04:25,200
Which wasn't a very pleasing sort of teaching for her.

29
00:04:25,200 --> 00:04:29,200
And moreover, it made her feel kind of nervous.

30
00:04:29,200 --> 00:04:34,200
She misunderstood that somehow the Buddha would look down upon her.

31
00:04:34,200 --> 00:04:40,200
And criticize her for being beautiful, right?

32
00:04:40,200 --> 00:04:46,200
Because beauty is, she thought, she understood beauty to be a bit of a problem in the Buddha's teaching.

33
00:04:46,200 --> 00:04:54,200
But at any rate, well, perhaps, but at any rate, the whole idea that the body,

34
00:04:54,200 --> 00:05:00,200
it's impermanent and unsatisfying and controllable, cause for suffering, the cause for ego, and so on.

35
00:05:00,200 --> 00:05:08,200
It's not really, not really comfortable for her because she was probably quite pleased with her beauty.

36
00:05:08,200 --> 00:05:14,200
And had received praise from an early age about her beauty.

37
00:05:14,200 --> 00:05:20,200
As a result, I'm most likely gained quite a bit of conceit and crookedness.

38
00:05:20,200 --> 00:05:30,200
Let's see where this is going.

39
00:05:30,200 --> 00:05:36,200
So, she thought, rather than have the Buddha criticize me for being beautiful or,

40
00:05:36,200 --> 00:05:43,200
whether being, having my beauty diminished in some way, or the importance of my beauty diminished.

41
00:05:43,200 --> 00:05:46,200
She thought, I'll just never go to see the Buddha.

42
00:05:46,200 --> 00:05:54,200
And so she did whatever she could to avoid having to see the Buddha for quite some time.

43
00:05:54,200 --> 00:06:05,200
Fortunately for her, living in the monastery and with lay people coming and going from the female monk monastery,

44
00:06:05,200 --> 00:06:09,200
she ended up hearing a good report about the Buddha.

45
00:06:09,200 --> 00:06:20,200
To say that, to put it mildly, she began to hear all these wonderful things people were saying about the Buddha.

46
00:06:20,200 --> 00:06:28,200
How impressive he was, how profound his teachings, how enlightening it was, how amazing and marvelous it was,

47
00:06:28,200 --> 00:06:36,200
just to see and to hear him teach, to receive instruction and to practice in the presence of the Buddha.

48
00:06:36,200 --> 00:06:45,200
To the point where, reasonably, understandably, imagine being so close to the Buddha, but having never gone to see him.

49
00:06:45,200 --> 00:06:54,200
She became curious, interested, and quite keen actually to see the Buddha.

50
00:06:54,200 --> 00:07:05,200
And it was she wrestled with this because it was quite serious for her that what he would say about her or the kind of things he would say would embarrass or humiliate her for being beautiful.

51
00:07:05,200 --> 00:07:12,200
Or at the very least, diminish the value of her beauty.

52
00:07:12,200 --> 00:07:23,200
But she said to herself, well, suppose he were to talk all day about how awful beauty is or how useless it is or how impermanent it is.

53
00:07:23,200 --> 00:07:26,200
How much could he possibly say, right?

54
00:07:26,200 --> 00:07:28,200
It's just words.

55
00:07:28,200 --> 00:07:37,200
As she said, in the end, she said, I got to go see him because she finally made a reminder that she wouldn't go to see him.

56
00:07:37,200 --> 00:07:43,200
We learned from the Buddha, listen to what he had to say and hopefully gained some understanding.

57
00:07:43,200 --> 00:07:47,200
Everyone heard, of course, and they were all excited because, oh, something special.

58
00:07:47,200 --> 00:07:53,200
Rupananda, because they knew she'd never gone to see the Buddha, and they said, he's going to give a special teaching for her.

59
00:07:53,200 --> 00:07:59,200
This is going to be a special occasion for the first time she'll get to see the Buddha and the Buddha himself.

60
00:07:59,200 --> 00:08:06,200
Using his special powers was able to think, or not able to think thought to himself,

61
00:08:06,200 --> 00:08:09,200
I'll have to do something special for her.

62
00:08:09,200 --> 00:08:15,200
And so she sat at the back kind of out of the way where I don't know where she sat at the back,

63
00:08:15,200 --> 00:08:24,200
where she wouldn't be seen by the Buddha.

64
00:08:24,200 --> 00:08:36,200
She said, I will not let him see who I am, and so she covered herself up instead of the back, but there's no fooling the Buddha.

65
00:08:36,200 --> 00:08:52,200
And so when the Buddha taught, before he taught, he used his magical, his special powers to create or to control her mind in some way,

66
00:08:52,200 --> 00:09:00,200
to create an impression on her mind that there was a beautiful woman behind the Buddha, fanning him.

67
00:09:00,200 --> 00:09:20,200
Nobody else could see it, they didn't actually create this vision, but he somehow made her to see this exceedingly beautiful woman, fanning the Buddha.

68
00:09:20,200 --> 00:09:23,200
In front of the Buddha, fanning the Buddha.

69
00:09:23,200 --> 00:09:28,200
This was a common thing, I mean you're thinking, talking in India where in the hot season it can get quite hot,

70
00:09:28,200 --> 00:09:34,200
and they didn't have fans for electric fans, obviously for the teacher.

71
00:09:34,200 --> 00:09:39,200
It was quite common for someone to stand, one of the monks often to stand in the fan.

72
00:09:39,200 --> 00:09:47,200
I've done it before, I did it for a adjunct, would take his fan and fan him when he was an outdoor ceremony,

73
00:09:47,200 --> 00:09:51,200
and he had to sit for a long time when he was a fan.

74
00:09:51,200 --> 00:09:56,200
It's one of those great gifts to the teacher.

75
00:09:56,200 --> 00:09:59,200
So it was a common thing.

76
00:09:59,200 --> 00:10:15,200
And Rupa Nandasar and was shocked, because this woman was actually her beauty, it has made Rupa Nandar look like a crow.

77
00:10:15,200 --> 00:10:21,200
A crow is an ugly thing, I guess the idea is something that's quite ugly.

78
00:10:21,200 --> 00:10:28,200
A competitor is off to a crow standing before a royal goose.

79
00:10:28,200 --> 00:10:37,200
And, you know, interested in her, because while he was another beautiful woman who appeared to be quite close to the Buddha,

80
00:10:37,200 --> 00:10:39,200
she seemed happy.

81
00:10:39,200 --> 00:10:43,200
And so suddenly she started to pay attention, and she lost her nervousness,

82
00:10:43,200 --> 00:10:48,200
and started listening to what the Buddha said, and watching this woman.

83
00:10:48,200 --> 00:10:51,200
And so the Buddha taught.

84
00:10:51,200 --> 00:10:59,200
But as he taught, she would look back at this beautiful woman in trance by her beauty.

85
00:10:59,200 --> 00:11:02,200
And she looked and suddenly the woman in age.

86
00:11:02,200 --> 00:11:08,200
A longer girl of 16, she was now a woman, perhaps a woman who had been through childbirth.

87
00:11:08,200 --> 00:11:19,200
He says, you know, lost a little bit of her youthful figure.

88
00:11:19,200 --> 00:11:26,200
That's not the same as it was before.

89
00:11:26,200 --> 00:11:30,200
And so she was kind of confused, and she kept watching.

90
00:11:30,200 --> 00:11:37,200
And as she watched, the Buddha caused the image to change her to a middle-aged woman.

91
00:11:37,200 --> 00:11:46,200
And then slowly, slowly, the middle-aged woman became old and decrepit and bent like an A-frame.

92
00:11:46,200 --> 00:11:54,200
And she said to herself, in the Pali, he dumped me, and there he dumped me.

93
00:11:54,200 --> 00:11:56,200
This has disappeared.

94
00:11:56,200 --> 00:11:58,200
That's disappeared.

95
00:11:58,200 --> 00:11:59,200
Realizing.

96
00:11:59,200 --> 00:12:03,200
The point that the text was making, she was able to see that.

97
00:12:03,200 --> 00:12:11,200
And this thing that she was clinging to, beautiful, beautiful God, beautiful God, ugly.

98
00:12:11,200 --> 00:12:13,200
Realizing impermanence.

99
00:12:13,200 --> 00:12:19,200
It's a very useful sort of conventional means of helping someone cultivate impermanence.

100
00:12:19,200 --> 00:12:25,200
It's a construct or an artifice.

101
00:12:25,200 --> 00:12:32,200
And finally, as you imagine this cripple old woman, suddenly the Buddha had her collapse.

102
00:12:32,200 --> 00:12:37,200
She gives out a large whale or moon or something.

103
00:12:37,200 --> 00:12:45,200
And collapses on the floor and starts dying, basically, in great pain and suffering.

104
00:12:45,200 --> 00:12:49,200
And it says rolling around in her urine and feces.

105
00:12:49,200 --> 00:12:51,200
Imagine the sight.

106
00:12:51,200 --> 00:12:55,200
And then lying still dead.

107
00:12:55,200 --> 00:13:02,200
Her group, Ananda, is just watching it in utter amazement.

108
00:13:02,200 --> 00:13:13,800
And watches as the corpse, now a corpse, begins to quite much more quickly than it would

109
00:13:13,800 --> 00:13:14,800
in the naturally.

110
00:13:14,800 --> 00:13:20,200
Of course, starts to bloat and break.

111
00:13:20,200 --> 00:13:25,200
And the quid starts to pour out a volley, open all the orifices.

112
00:13:25,200 --> 00:13:27,200
And then it breaks apart.

113
00:13:27,200 --> 00:13:32,200
And pus and blood comes out to maggots and so on.

114
00:13:32,200 --> 00:13:44,200
And according to the progression of the corpse.

115
00:13:44,200 --> 00:13:50,200
And you think, well, a nice show, but what does it mean?

116
00:13:50,200 --> 00:13:57,200
But I think it will be quite clear that the profundity of this is that this is us.

117
00:13:57,200 --> 00:14:01,200
This is what she's seeing as a story of life.

118
00:14:01,200 --> 00:14:06,200
This isn't some hypothetical.

119
00:14:06,200 --> 00:14:12,200
It's perhaps the vision that resonates the strongest with a human being.

120
00:14:12,200 --> 00:14:17,200
It's the vision of old age sickness and death.

121
00:14:17,200 --> 00:14:19,200
And it resonated with her.

122
00:14:19,200 --> 00:14:23,200
And she realized this beauty that she had.

123
00:14:23,200 --> 00:14:27,200
I mean, obviously she's not going to age that quickly, but it's going to come to her as well.

124
00:14:27,200 --> 00:14:35,200
This is exactly the fate that she will, can one day hope for or expect for.

125
00:14:35,200 --> 00:14:41,200
And so I mean, this isn't be passing exactly, but it's a great summit of practice that we use often

126
00:14:41,200 --> 00:14:43,200
while looking at dead corpses.

127
00:14:43,200 --> 00:14:47,200
But the idea is that it really sets the stage.

128
00:14:47,200 --> 00:14:54,200
It really prepares one for an understanding of true and permanent suffering in non-self.

129
00:14:54,200 --> 00:14:57,200
Because it makes you a lot more objective about the body.

130
00:14:57,200 --> 00:15:02,200
You lose a lot of the conceit, all of the crookedness surrounding the body.

131
00:15:02,200 --> 00:15:10,200
Using your body, thinking that people are attracted to your body and finding ways to make up your body.

132
00:15:10,200 --> 00:15:12,200
Make up your body.

133
00:15:12,200 --> 00:15:14,200
Clean your body.

134
00:15:14,200 --> 00:15:18,200
Print your body.

135
00:15:18,200 --> 00:15:29,200
If you're strong in the body, you need physical sports and exercises to try and make the body more competent, more able, more potent.

136
00:15:29,200 --> 00:15:32,200
We eat foods and so on.

137
00:15:32,200 --> 00:15:33,200
Or maybe the opposite.

138
00:15:33,200 --> 00:15:35,200
Maybe we load our body and we feel discussed.

139
00:15:35,200 --> 00:15:42,200
We feel self-conscious of it. We're fat, we're ugly, we're tall, we're short, we're thin.

140
00:15:42,200 --> 00:15:44,200
And so on.

141
00:15:44,200 --> 00:15:48,200
We have crooked nose, crooked teeth.

142
00:15:48,200 --> 00:15:53,200
Our hairs, the wrong color, our hairs, and so on.

143
00:15:53,200 --> 00:15:54,200
It's on.

144
00:15:54,200 --> 00:15:57,200
Our ears are too big, whatever.

145
00:15:57,200 --> 00:15:59,200
Anything and everything.

146
00:15:59,200 --> 00:16:11,200
And so looking at watching a body die, imagine watching a body decompose in front of your eyes.

147
00:16:11,200 --> 00:16:14,200
Kind of puts a damper on all that.

148
00:16:14,200 --> 00:16:23,200
It puts it all in perspective when you start to say, really, why am I so obsessed with this useless body that's going to get old sick and die.

149
00:16:23,200 --> 00:16:32,200
That's like a burden, like a corpse around one's neck.

150
00:16:32,200 --> 00:16:35,200
And so the Buddha began to teach her any thought.

151
00:16:35,200 --> 00:16:41,200
A verse that is not the verse that we have, but it's a bunch of verses actually.

152
00:16:41,200 --> 00:16:42,200
Let's read.

153
00:16:42,200 --> 00:16:47,200
Let's look at the, the poly's quite beautiful, but I won't bore you with it.

154
00:16:47,200 --> 00:16:49,200
Oh, it's actually quite good.

155
00:16:49,200 --> 00:16:54,200
As is this body, the verse he talks about, the body is diseased in pure putrid.

156
00:16:54,200 --> 00:17:00,200
It oozes and leaks, yet it is desired of simple terms.

157
00:17:00,200 --> 00:17:05,200
As is the, this body so also was that.

158
00:17:05,200 --> 00:17:21,200
That body, as that body, as, as this body, so is that body, as that body, so is this body.

159
00:17:21,200 --> 00:17:23,200
We are like them.

160
00:17:23,200 --> 00:17:25,200
This is what the Buddha was teaching to her.

161
00:17:25,200 --> 00:17:30,200
Basically what I sort of think I was just saying, but in much more words,

162
00:17:30,200 --> 00:17:35,200
we look quite as well as the Buddha.

163
00:17:35,200 --> 00:17:41,200
We hold the elements in their emptiness.

164
00:17:41,200 --> 00:17:44,200
Dha-tutu-sunya-tul-pasa.

165
00:17:44,200 --> 00:17:49,200
Look at these elements that are empty.

166
00:17:49,200 --> 00:17:53,200
We hold the emptiness of the elements.

167
00:17:53,200 --> 00:17:54,200
What does that mean?

168
00:17:54,200 --> 00:17:58,200
The, the, the emptiness of the elements means that there is no, there is no essence.

169
00:17:58,200 --> 00:18:03,200
There is nothing, there's nothing, there's nothing permanent, nothing.

170
00:18:03,200 --> 00:18:06,200
That is a thing, right?

171
00:18:06,200 --> 00:18:10,200
Because change means it's not actually, it can't actually be a real thing.

172
00:18:10,200 --> 00:18:13,200
What would it mean to say that something changes?

173
00:18:13,200 --> 00:18:14,200
It's not actually a thing anymore.

174
00:18:14,200 --> 00:18:17,200
It's just a process.

175
00:18:17,200 --> 00:18:18,200
And that's what the body is.

176
00:18:18,200 --> 00:18:23,200
This body is not a thing, it's a process.

177
00:18:23,200 --> 00:18:30,200
And the Buddha says as much, he says,

178
00:18:30,200 --> 00:18:36,200
whether it's one more malo-kang-punara-gami.

179
00:18:36,200 --> 00:18:39,200
Go not back to the world.

180
00:18:39,200 --> 00:18:47,200
Cast away desire for existence, and you shall walk in tranquility.

181
00:18:47,200 --> 00:18:53,200
Bawai-jandang-vira-ji-tua-pasa-nto-jari-sati.

182
00:18:53,200 --> 00:18:55,200
Don't know where this verse comes from.

183
00:18:55,200 --> 00:18:59,200
This is apparently something the Buddha said, but it's not canonical, I don't think.

184
00:18:59,200 --> 00:19:05,200
So, tell you right, then the Buddha,

185
00:19:05,200 --> 00:19:08,200
Buddha says with the idea that there's no sara.

186
00:19:08,200 --> 00:19:14,200
You must mean sara-yare-sara-ti-mas-sanyan-kari.

187
00:19:14,200 --> 00:19:18,200
Don't create the conception in your mind that there is anything of any,

188
00:19:18,200 --> 00:19:25,200
that there is any essence or importance or value to the body.

189
00:19:25,200 --> 00:19:30,200
Up atakopi-hi-ai-ta-sara-nati,

190
00:19:30,200 --> 00:19:41,200
the smallest bit of essence or importance in this body

191
00:19:41,200 --> 00:19:46,200
does not exist, nakti-ta.

192
00:19:46,200 --> 00:19:51,200
It is made up of 300 bones,

193
00:19:51,200 --> 00:19:57,200
smeared and plastered with blood and flesh and skin and so on,

194
00:19:57,200 --> 00:20:00,200
and then he gave the verse.

195
00:20:00,200 --> 00:20:05,200
So, how this relates to our practice should be fairly clear in two ways.

196
00:20:05,200 --> 00:20:09,200
The first is, the second part of the verse here

197
00:20:09,200 --> 00:20:12,200
it says, mano-makho-ji-ji-ta-jara.

198
00:20:12,200 --> 00:20:15,200
Actually, it's, you know, even the verse,

199
00:20:15,200 --> 00:20:18,200
what the verse explicitly states,

200
00:20:18,200 --> 00:20:21,200
that we become conceited and attached to the body.

201
00:20:21,200 --> 00:20:23,200
It was an important lesson,

202
00:20:23,200 --> 00:20:28,200
to the important delusion to dispel,

203
00:20:28,200 --> 00:20:33,200
because the body is certainly not worth clinging to being

204
00:20:33,200 --> 00:20:39,200
conceited about attached to desire and so on.

205
00:20:39,200 --> 00:20:41,200
Being something that's not going to last,

206
00:20:41,200 --> 00:20:43,200
that it's not going to, that it's not stable,

207
00:20:43,200 --> 00:20:47,200
that could never be a source of true happiness.

208
00:20:47,200 --> 00:20:49,200
But the more important lesson,

209
00:20:49,200 --> 00:20:52,200
I think actually surprisingly really comes from the story,

210
00:20:52,200 --> 00:20:54,200
not from the verse,

211
00:20:54,200 --> 00:20:56,200
but it's implicit in the verse,

212
00:20:56,200 --> 00:21:06,200
and it's the more ultimate reality of impermanent suffering

213
00:21:06,200 --> 00:21:08,200
and non-self.

214
00:21:08,200 --> 00:21:11,200
This idea of seeing things arise and sees that comes

215
00:21:11,200 --> 00:21:14,200
on a conceptual level,

216
00:21:14,200 --> 00:21:18,200
from seeing all day sickness and death,

217
00:21:18,200 --> 00:21:20,200
but it can also be seen,

218
00:21:20,200 --> 00:21:25,200
or it means eventually to be seen on a momentary level.

219
00:21:25,200 --> 00:21:31,200
And the true way to understand the nature of the body

220
00:21:31,200 --> 00:21:34,200
into free or self from any kind of attachment

221
00:21:34,200 --> 00:21:37,200
to it really is the practice of insight meditation.

222
00:21:37,200 --> 00:21:39,200
It's all fine and good to look at a corpse

223
00:21:39,200 --> 00:21:47,200
and to gain the profound seeming appreciation

224
00:21:47,200 --> 00:21:49,200
for the impermanence of the body,

225
00:21:49,200 --> 00:21:52,200
but it's quite another thing to actually see

226
00:21:52,200 --> 00:21:55,200
a momentary arising and see-saving experience

227
00:21:55,200 --> 00:21:58,200
and to realize that the body doesn't exist in the first place,

228
00:21:58,200 --> 00:22:00,200
and all that there is is this process.

229
00:22:00,200 --> 00:22:05,200
And to see how disconnected from that reality is our perception

230
00:22:05,200 --> 00:22:08,200
where we perceive the body as being beautiful,

231
00:22:08,200 --> 00:22:11,200
it could never be a part of the process.

232
00:22:11,200 --> 00:22:14,200
How we see it conceive the body as being strong,

233
00:22:14,200 --> 00:22:18,200
as being solid as being me and mine,

234
00:22:18,200 --> 00:22:20,200
and all that has no place in reality

235
00:22:20,200 --> 00:22:25,200
which is changing constantly in moments and moments of experience.

236
00:22:33,200 --> 00:22:36,200
It's a challenge to us,

237
00:22:36,200 --> 00:22:40,200
and it's a part of it's an important subject

238
00:22:40,200 --> 00:22:42,200
to talk about, to realize,

239
00:22:42,200 --> 00:22:45,200
and because as we talk about we are able to see,

240
00:22:45,200 --> 00:22:49,200
we're able to see and experience our attachments

241
00:22:49,200 --> 00:22:54,200
to the body, and it's a challenge

242
00:22:56,200 --> 00:23:01,200
to strive to understand this,

243
00:23:01,200 --> 00:23:04,200
where it's why the disconnect,

244
00:23:04,200 --> 00:23:07,200
to see through the disconnect,

245
00:23:07,200 --> 00:23:10,200
whereby we think of something as beautiful,

246
00:23:10,200 --> 00:23:13,200
attractive, me, mine,

247
00:23:13,200 --> 00:23:16,200
worthy of possessing or possessed by me,

248
00:23:16,200 --> 00:23:19,200
controlled by me, et cetera,

249
00:23:19,200 --> 00:23:23,200
with the disconnect between that and the reality,

250
00:23:23,200 --> 00:23:25,200
whereby it's not of that,

251
00:23:25,200 --> 00:23:29,200
where it's quite clearly doesn't even exist.

252
00:23:30,200 --> 00:23:32,200
A lot of our practices like this,

253
00:23:32,200 --> 00:23:35,200
it's about bridging the disconnect

254
00:23:35,200 --> 00:23:38,200
between our perception and reality,

255
00:23:38,200 --> 00:23:41,200
or eventually our view of things

256
00:23:41,200 --> 00:23:47,200
comes in line and can change radically as a result,

257
00:23:47,200 --> 00:23:52,200
comes in line as it comes in line with the truth,

258
00:23:52,200 --> 00:23:55,200
with what's really going on.

259
00:23:55,200 --> 00:23:58,200
And of course, that's very much why we suffer.

260
00:23:58,200 --> 00:24:00,200
We suffer because of the disconnect.

261
00:24:00,200 --> 00:24:03,200
If you understood reality clearly,

262
00:24:03,200 --> 00:24:05,200
there'd be no reason to suffer.

263
00:24:05,200 --> 00:24:11,200
Opportunity for suffering to arise.

264
00:24:11,200 --> 00:24:16,200
So it's not something to me,

265
00:24:16,200 --> 00:24:18,200
even though there's a sort of conceptual teaching,

266
00:24:18,200 --> 00:24:22,200
it's not something to be disregarded or discounted,

267
00:24:22,200 --> 00:24:24,200
because we have, and this is what we're born with.

268
00:24:24,200 --> 00:24:25,200
We're born with conceit,

269
00:24:25,200 --> 00:24:32,200
and we're born into a life of conceit and attachment to the body.

270
00:24:32,200 --> 00:24:38,200
And so that's one of the more important topics to talk about.

271
00:24:38,200 --> 00:24:39,200
The true nature of the body,

272
00:24:39,200 --> 00:24:42,200
both conceptually and in an ultimate sense,

273
00:24:42,200 --> 00:24:45,200
conceptually is getting old, sick and dying.

274
00:24:45,200 --> 00:24:47,200
And in an ultimate sense,

275
00:24:47,200 --> 00:24:51,200
as it's not really existing in the first place.

276
00:24:51,200 --> 00:24:54,200
So that's the demo panel for tonight.

277
00:24:54,200 --> 00:24:56,200
Thank you all for tuning in,

278
00:24:56,200 --> 00:25:03,200
I appreciate you all. Good practice.

