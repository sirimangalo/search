WEBVTT

00:00.000 --> 00:07.000
When wanting arises, when I meditate, I start meditating on the wanting, by saying wanting and wanting.

00:07.000 --> 00:09.000
The wanting becomes stronger.

00:09.000 --> 00:14.000
It seems like I am only suppressing the wanting rather than getting rid of it completely.

00:14.000 --> 00:17.000
How do I overcome this?

00:17.000 --> 00:27.000
I'm not, I'm not quite clear on the logic that you're using there, but when something becomes stronger,

00:27.000 --> 00:30.000
I'm not generally a sign of suppression.

00:30.000 --> 00:34.000
The suppression is when you make it weaker by pushing it down,

00:34.000 --> 00:40.000
and I would suggest that that's the root problem,

00:40.000 --> 00:43.000
because let's put it simply.

00:43.000 --> 00:48.000
Our negative mind states are not simple.

00:48.000 --> 00:53.000
The negativity that we have in our mind is not simply a drawer full of greed,

00:53.000 --> 00:56.000
a drawer full of anger, and a drawer full of illusion.

00:56.000 --> 01:00.000
It's so insanely mixed up,

01:00.000 --> 01:05.000
and I mean think about the constructs that you've created in your life, right?

01:05.000 --> 01:10.000
It's not difficult to understand how complex these habits can become,

01:10.000 --> 01:16.000
and these negative tendencies to cling and to hate,

01:16.000 --> 01:21.000
and to dislike how they can be to become, to be conceded, to be arrogant,

01:21.000 --> 01:26.000
to be arrogant, all of the various different formations.

01:26.000 --> 01:36.000
Now, as a result, you can never deal with a single emotion,

01:36.000 --> 01:38.000
and say, I'm just going to focus on this one.

01:38.000 --> 01:40.000
Poof, it's going to disappear.

01:40.000 --> 01:44.000
You've got more issues going on there.

01:44.000 --> 01:47.000
The pleasure that you're experiencing,

01:47.000 --> 01:49.000
if it's becoming stronger in meditation,

01:49.000 --> 01:53.000
and that's a clear sign that in your daily life, you're suppressing it,

01:53.000 --> 01:55.000
and you're probably suppressing it with all sorts of things,

01:55.000 --> 02:02.000
probably with guilt, probably with anger,

02:02.000 --> 02:06.000
which is a part and parcel of guilt with delusion,

02:06.000 --> 02:10.000
the idea that you are in control of your desires,

02:10.000 --> 02:12.000
so when I want to turn it on, it's on,

02:12.000 --> 02:14.000
when I want to turn it off, it's off.

02:14.000 --> 02:18.000
I love this person, but I won't go near that person, right?

02:18.000 --> 02:21.000
I love my wife, but these angels in heaven,

02:21.000 --> 02:25.000
when I see these nymphs in heaven, I won't become attacked to them.

02:25.000 --> 02:33.000
So there's delusion, there's many things that are suppressing the wanting.

02:33.000 --> 02:36.000
When you're practicing meditation,

02:36.000 --> 02:38.000
what you're experiencing is actually a good thing

02:38.000 --> 02:40.000
because you've stopped suppressing it.

02:40.000 --> 02:41.000
That's why it's getting stronger.

02:41.000 --> 02:45.000
It's not because you're meditating on it, not directly.

02:45.000 --> 02:48.000
It's like meditation is growing more desire,

02:48.000 --> 02:52.000
but you've got a huge lump of desire inside.

02:52.000 --> 02:58.000
You're just a massive desire, and that means you have massive...

02:58.000 --> 03:01.000
Well, you have the tendency or the propensity

03:01.000 --> 03:05.000
to give rise to massive amounts of desire,

03:05.000 --> 03:13.000
continuous succession of mind-states of desire-based mind-states.

03:13.000 --> 03:17.000
That's the habit that you've developed over the years,

03:17.000 --> 03:19.000
rather than dealing with it, you've been suppressing it,

03:19.000 --> 03:23.000
and that's why you don't experience it.

03:23.000 --> 03:26.000
You get some garbage, and you throw it in your closet,

03:26.000 --> 03:31.000
and you get some more garbage, and you just get this habit of throwing garbage into your closet.

03:31.000 --> 03:33.000
Well, guess what?

03:33.000 --> 03:35.000
You end up with a closet full of garbage,

03:35.000 --> 03:37.000
and then meditation is like you open the closet,

03:37.000 --> 03:40.000
and you get to have it all fall down on you,

03:40.000 --> 03:42.000
and then you're like, oh, I'll close that up again,

03:42.000 --> 03:45.000
and then you think, what's wrong with this meditation?

03:45.000 --> 03:49.000
It's bringing out all this bad stuff.

03:49.000 --> 03:50.000
So you have a choice.

03:50.000 --> 03:53.000
You can keep it in the closet, and in fact,

03:53.000 --> 03:56.000
keep throwing more in, or you can train yourself first of all

03:56.000 --> 04:00.000
to stop doing that, and start to empty out the closet,

04:00.000 --> 04:03.000
deal with it when it arises.

04:03.000 --> 04:07.000
In that sense, there's nothing really wrong with

04:07.000 --> 04:10.000
the experiences that you're having,

04:10.000 --> 04:13.000
and another thing I would note is that

04:13.000 --> 04:19.000
it's arguable that the desire is not which is increasing.

04:19.000 --> 04:24.000
What is increasing is the physical manifestations of the desire

04:24.000 --> 04:26.000
in the brain, in the body.

04:26.000 --> 04:31.000
So a good example is with lust, with physical attachment to the body.

04:31.000 --> 04:34.000
99% of it is not the desire.

04:34.000 --> 04:38.000
The chemical reactions, the bodily changes,

04:38.000 --> 04:41.000
the hormones, and so on, this is all totally physical.

04:41.000 --> 04:45.000
It's not the desire, and this is why it's so important

04:45.000 --> 04:53.000
in regards to attachment to sexuality, and that sort of thing,

04:53.000 --> 04:57.000
to be able to break up the experience into its component parts.

04:57.000 --> 05:01.000
When you meditate on the desire, you're allowing your body

05:01.000 --> 05:04.000
to give rise to these states that normally you would suppress.

05:04.000 --> 05:07.000
You would create a great tension in the body

05:07.000 --> 05:10.000
that would suppress so many of the systems

05:10.000 --> 05:14.000
that are now given the chance to release.

05:14.000 --> 05:18.000
And so the chemicals will arise and you'll have great pleasure.

05:18.000 --> 05:20.000
And then there arises the desire.

05:20.000 --> 05:23.000
But it can be that when you're meditating,

05:23.000 --> 05:27.000
the delusion and the guilt and so on is preventing you from seeing this.

05:27.000 --> 05:32.000
But when you're meditating, the desire can actually disappear,

05:32.000 --> 05:34.000
and you can feel such great pleasure

05:34.000 --> 05:36.000
that would normally have been associated with desire.

05:36.000 --> 05:40.000
But because of you're saying wanting or more important,

05:40.000 --> 05:42.000
saying happy, happy or pleasure, pleasure,

05:42.000 --> 05:46.000
or even feeling, feeling, and so on,

05:46.000 --> 05:51.000
is that what's occurring is this release, this bodily release,

05:51.000 --> 05:53.000
and by bodily we also mean the brain,

05:53.000 --> 05:55.000
so the chemical releases and so on,

05:55.000 --> 05:59.000
that are now given the chance because there's less tension in the body.

05:59.000 --> 06:01.000
That's one really good way of understanding this,

06:01.000 --> 06:14.000
is that you've been suppressing or restricting the flow of bodily systems.

06:14.000 --> 06:16.000
And that's what's coming out desire.

06:16.000 --> 06:21.000
Arguably is only a static mind state.

06:21.000 --> 06:24.000
So say we have more desire, less desire.

06:24.000 --> 06:27.000
I'm not sure that such a thing exists in Buddhist thought.

06:27.000 --> 06:30.000
Desire is the moment where you like something.

06:30.000 --> 06:33.000
Now, if you like something continuously,

06:33.000 --> 06:35.000
then you could call it strong desire,

06:35.000 --> 06:42.000
but it's still only moment to moment to moment desire.

06:42.000 --> 06:44.000
And that's only a very small part.

06:44.000 --> 06:46.000
That's a very mental part of it.

06:46.000 --> 06:49.000
It's where the mind creates partiality.

06:49.000 --> 06:52.000
And it says that this is positive identify something,

06:52.000 --> 06:55.000
categorizes it as a positive experience.

06:55.000 --> 06:57.000
It sounds like at that moment,

06:57.000 --> 06:59.000
you're probably having a lot of negative experiences

06:59.000 --> 07:02.000
because you have the suppression tendencies.

07:02.000 --> 07:03.000
Oh, no, more greed.

07:03.000 --> 07:05.000
And you think that's a bad thing, right?

07:05.000 --> 07:06.000
These feelings are coming out more.

07:06.000 --> 07:07.000
And that's a bad thing.

07:07.000 --> 07:09.000
So actually, you don't like it.

07:09.000 --> 07:12.000
You're not giving rise to so much greed.

07:12.000 --> 07:14.000
You're giving rise to a lot of anger,

07:14.000 --> 07:16.000
which is even worse because now you're mixing them up

07:16.000 --> 07:19.000
and creating more complications.

07:19.000 --> 07:21.000
So it's very important.

07:21.000 --> 07:24.000
And this is in the video that this very famous video,

07:24.000 --> 07:26.000
it's only famous because of the title

07:26.000 --> 07:29.000
on pornography and masturbation.

07:29.000 --> 07:32.000
I said, it's important to let the lust come up

07:32.000 --> 07:33.000
and to focus on it.

07:33.000 --> 07:36.000
Mostly it has nothing to do with the desire.

07:36.000 --> 07:38.000
It's just chemical reactions in the body.

07:38.000 --> 07:41.000
And to be able to see that, it's a profound,

07:41.000 --> 07:44.000
it can actually lead to great physical and mental pleasure.

07:44.000 --> 07:46.000
It can lead to great happiness.

07:46.000 --> 07:49.000
It's a release because you don't have to feel guilty anymore.

07:49.000 --> 07:51.000
It's just an experience.

07:51.000 --> 07:57.000
And it has nothing to do directly with the desire.

07:57.000 --> 07:59.000
The desire is that little moment where you say,

07:59.000 --> 08:00.000
this is good.

08:00.000 --> 08:03.000
If you're not saying that, if you're just experiencing it,

08:03.000 --> 08:07.000
this happiness, this pleasure, this chemicals, whatever,

08:07.000 --> 08:10.000
then you become free.

08:10.000 --> 08:12.000
And when it's gone, it's gone.

08:12.000 --> 08:17.000
It doesn't trouble you.

08:17.000 --> 08:22.000
So you could watch that video on pornography and masturbation.

08:22.000 --> 08:25.000
I think because it's also about addiction in general.

08:25.000 --> 08:27.000
It talks about some of these things.

08:27.000 --> 08:31.000
And hopefully what I've said now is basically along the same lines

08:31.000 --> 08:34.000
and useful as well.

08:34.000 --> 08:35.000
Go ahead.

08:35.000 --> 08:39.000
I think you already said it in a way, in another way.

08:39.000 --> 08:45.000
But my first thought on this was that when the wanting

08:45.000 --> 08:50.000
seems to become stronger, it can as well be

08:50.000 --> 08:56.000
that you just see it more clearly.

08:56.000 --> 08:58.000
That's the other thing, right?

08:58.000 --> 09:02.000
So it's actually, it has been there all the time,

09:02.000 --> 09:04.000
but you didn't see it.

09:04.000 --> 09:09.000
You covered it up and you thought it is whatever.

09:09.000 --> 09:13.000
But now you see it as wanting as what it really is.

09:13.000 --> 09:17.000
So that's important to know, I think.

09:17.000 --> 09:25.000
Yeah, because normally we have this self-created

09:25.000 --> 09:26.000
amnesia, right?

09:26.000 --> 09:27.000
We don't want to think about it.

09:27.000 --> 09:30.000
So when anger comes up, no, not supposed to get angry.

09:30.000 --> 09:33.000
When greed comes, no, not so.

09:33.000 --> 09:35.000
We don't keep that in mind.

09:35.000 --> 09:37.000
Oh, I was angry there.

09:37.000 --> 09:39.000
But we can get angry a million times a day

09:39.000 --> 09:41.000
and not really remember it.

09:41.000 --> 09:45.000
Because we're constantly throwing it under the cut.

09:45.000 --> 09:46.000
No, I didn't get angry.

09:46.000 --> 09:48.000
No, I'm not going to get angry.

09:48.000 --> 09:50.000
But we are already angry.

09:50.000 --> 09:51.000
It's just our reaction.

09:51.000 --> 09:57.000
That is, that is sort of switching the subject,

09:57.000 --> 09:59.000
trying to avoid the subject.

09:59.000 --> 10:02.000
So in meditation, it's not that we're getting angry more.

10:02.000 --> 10:04.000
It's that we're letting it come up.

10:04.000 --> 10:06.000
We're like, okay, let's see what anger's like.

10:06.000 --> 10:08.000
And boom, it comes up.

10:08.000 --> 10:11.000
And boom, then there's the physical.

10:11.000 --> 10:12.000
The same with us.

10:12.000 --> 10:14.000
Okay, let it come up and look at it.

10:14.000 --> 10:17.000
And this is very clear that most,

10:17.000 --> 10:20.000
or many people miss about the Buddha's teaching.

10:20.000 --> 10:22.000
If you read the Satyapatana Sutta,

10:22.000 --> 10:23.000
what did the Buddha say about these things?

10:23.000 --> 10:25.000
When you have sensual desire,

10:25.000 --> 10:28.000
you know to yourself, they're sensual.

10:28.000 --> 10:29.000
Or I have sensual desire.

10:29.000 --> 10:31.000
They're sensual desire in the mind.

10:31.000 --> 10:33.000
When you don't, when it's not present,

10:33.000 --> 10:35.000
you know it's not present.

10:35.000 --> 10:36.000
This is very important.

10:36.000 --> 10:40.000
This creates the equanimity that we're talking about.

10:40.000 --> 10:43.000
It allows you to see the progression of state.

10:43.000 --> 10:48.000
And so it allows you to see how greed is,

10:48.000 --> 10:52.000
how greed works and anger and so on,

10:52.000 --> 10:56.000
and how the mind works, to be objective about it.

10:56.000 --> 10:57.000
So yeah, very clear.

10:57.000 --> 11:00.000
It may not be that it's coming more.

11:00.000 --> 11:04.000
It's just that you're actually not only more aware of it,

11:04.000 --> 11:08.000
but I would say even further, you're admitting it.

11:08.000 --> 11:11.000
Because we pretend we weren't.

11:11.000 --> 11:15.000
I'm not an angry person, but it's because we suppress it all.

11:15.000 --> 11:17.000
And then meditation makes us angry people again,

11:17.000 --> 11:19.000
and we can fed up about the meditation.

11:19.000 --> 11:38.000
There is this.

