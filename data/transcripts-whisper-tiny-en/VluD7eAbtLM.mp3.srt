1
00:00:00,000 --> 00:00:09,840
okay hello everyone back answering questions I'm actually going to

2
00:00:09,840 --> 00:00:15,000
re-answer question so some of you may have already caught the last broadcast

3
00:00:15,000 --> 00:00:29,600
last month but I want to re-answer the question about about depression so I'll

4
00:00:29,600 --> 00:00:35,360
try and be brief this won't be a long video and there are a couple of things

5
00:00:35,360 --> 00:00:46,400
announcements at the end as well so the first thing to say about depression and

6
00:00:46,400 --> 00:00:54,800
anxiety question was about depression but depression and anxiety I think are

7
00:00:54,800 --> 00:01:04,200
two good examples of common mental illnesses there are others but they follow the

8
00:01:04,200 --> 00:01:13,120
similar ideas and principles so I don't think it's necessary to talk about or

9
00:01:13,120 --> 00:01:20,440
to go into detail with all of them we'll take these two as

10
00:01:20,440 --> 00:01:29,240
exemplary but the first thing to say about them is that in principle mindfulness

11
00:01:29,240 --> 00:01:35,320
meditation helps us deal with and overcome them mindfulness meditation I think

12
00:01:35,320 --> 00:01:43,280
it's stronger than that it not only helps us deal with them but if practiced

13
00:01:43,280 --> 00:01:49,760
properly of course that's a big caveat has to be practiced and has to be

14
00:01:49,760 --> 00:02:00,400
practiced properly but if so it really is enough to overcome and free

15
00:02:00,400 --> 00:02:09,200
oneself from issues of anxiety and depression and a lot of other mental

16
00:02:09,200 --> 00:02:15,720
illnesses I've talked about things like schizophrenia

17
00:02:15,720 --> 00:02:22,120
schizophrenia is another really good example it's very different but what

18
00:02:22,120 --> 00:02:29,080
little I know about schizophrenia from reading and listening is also

19
00:02:29,080 --> 00:02:36,360
similarly appropriate and I think surprisingly similar or this video is

20
00:02:36,360 --> 00:02:42,000
surprisingly applicable to schizophrenia as well that's not really what I want to

21
00:02:42,000 --> 00:02:46,840
talk about in this video how because what that means is I usually don't

22
00:02:46,840 --> 00:02:52,320
answer questions about mental illness if someone asks me how do I think the

23
00:02:52,320 --> 00:02:56,920
question was simply how do I free myself from depression how does one free

24
00:02:56,920 --> 00:03:02,400
oneself from depression because the answer is quite simply I mean it's very

25
00:03:02,400 --> 00:03:09,840
much that's the purpose of what I do of what we teach and practice and so

26
00:03:09,840 --> 00:03:19,800
making a video on it it's not really going to say anything in that sense beyond

27
00:03:19,800 --> 00:03:24,640
just talking about well practice meditation it would be an explanation of how

28
00:03:24,640 --> 00:03:32,640
to do mindfulness meditation but there I think two related factors which I do

29
00:03:32,640 --> 00:03:39,800
want to talk about that one would argue get in the way of practicing mindful

30
00:03:39,800 --> 00:03:47,080
illness properly for one who has a mental illness like anxiety or depression

31
00:03:47,080 --> 00:03:54,960
so this is what I want to talk about the first factor issue and these are

32
00:03:54,960 --> 00:04:02,960
really issues that I think it wouldn't be too far fetched to criticize modern

33
00:04:02,960 --> 00:04:12,880
medicine for falling prey to or getting wrong I think it's too much to have a

34
00:04:12,880 --> 00:04:17,600
blanket statement that all of modern medicine gets it wrong but I think in

35
00:04:17,600 --> 00:04:26,840
general not just medicine but you know our popular understanding of these

36
00:04:26,840 --> 00:04:35,680
things what we get wrong and the first one is to to reify them this word

37
00:04:35,680 --> 00:04:41,600
reify has very important applications and Buddhism to reify means from my

38
00:04:41,600 --> 00:04:49,000
understanding my use of the word here means to create something out of

39
00:04:49,000 --> 00:04:58,720
out of less really to to give something existence so when we say I have an

40
00:04:58,720 --> 00:05:07,560
anxiety problem we've created that concept we've created an entity and it's

41
00:05:07,560 --> 00:05:13,440
monolithic it's it's how do you it's atomic in the sense of being indivisible

42
00:05:13,440 --> 00:05:18,960
that's what the word atomic means as well how do you how do you deal with it I

43
00:05:18,960 --> 00:05:24,000
mean it then becomes a thing that you have to get rid of you have to get rid of

44
00:05:24,000 --> 00:05:30,960
the thing now because it's it's it's atomic or it's monolithic you've created

45
00:05:30,960 --> 00:05:40,320
something and this is key in in Buddhism in there was this very ancient

46
00:05:40,320 --> 00:05:48,160
this we have a story in ancient times after the Buddha passed away and the monks

47
00:05:48,160 --> 00:05:55,720
were all gathered to trying to figure out what was the Buddha's actual

48
00:05:55,720 --> 00:06:04,280
teaching that Buddhism had split into a number of different schools and a lot of

49
00:06:04,280 --> 00:06:08,480
them were teaching things that certainly didn't bear any resemblance to the

50
00:06:08,480 --> 00:06:18,240
original teaching quite far-fetched ideas and the king asked this one monk

51
00:06:18,240 --> 00:06:23,960
what did the Buddha teach what sort of a teacher was the Buddha and the

52
00:06:23,960 --> 00:06:28,320
answer was and this was the correct answer the Buddha was a wee budge of

53
00:06:28,320 --> 00:06:39,000
adding we budget means one who dissects one who splits apart one who splits

54
00:06:39,000 --> 00:06:44,400
the atom kind of so if you have this atom called anxiety or I have

55
00:06:44,400 --> 00:06:54,880
clinical depression I'm depressed you split this atom this is what

56
00:06:54,880 --> 00:07:04,080
Buddhism means to do and it's about separating the condition the problem the

57
00:07:04,080 --> 00:07:10,680
illness into its constituent parts into realities because depression

58
00:07:10,680 --> 00:07:16,600
doesn't exist anxiety doesn't exist what exists are moments of experience

59
00:07:16,600 --> 00:07:22,560
at an ultimate level that's what we're dealing with for anxiety there will be

60
00:07:22,560 --> 00:07:28,920
moments there will be the moment of experiencing that suppose I have to get up

61
00:07:28,920 --> 00:07:32,120
and run a stage in front of a hundred people or a thousand people and give a

62
00:07:32,120 --> 00:07:38,040
talk that experience the awareness of that even just the thought of it I'll be

63
00:07:38,040 --> 00:07:43,040
sitting backstage or something or in the audience waiting for my turn and I

64
00:07:43,040 --> 00:07:48,280
think about going up on stage and that thought is an experience and of course

65
00:07:48,280 --> 00:07:53,320
immediately after that there might arise anxiety which is another experience

66
00:07:53,320 --> 00:07:59,960
after that experience of anxiety there will be other phenomena that arise

67
00:07:59,960 --> 00:08:04,200
there may be butterflies in the stomach or heart beating fast or tension in

68
00:08:04,200 --> 00:08:09,160
the shoulders and based on those experiences other experiences will arise

69
00:08:09,160 --> 00:08:13,200
I'll feel that butterflies growing in my stomach and it will make me more

70
00:08:13,200 --> 00:08:18,040
anxious or my heart beating fast and I'll get afraid or I'll become worried

71
00:08:18,040 --> 00:08:28,360
that maybe I'll give a poor talk or maybe people will notice my my shaky voice

72
00:08:28,360 --> 00:08:32,680
that makes me more anxious and and this is how people get into panic attacks

73
00:08:32,680 --> 00:08:41,400
they build themselves up and entered the sort of feedback that based on

74
00:08:41,400 --> 00:08:47,080
moments of experience so when someone has a panic attack this is what I

75
00:08:47,080 --> 00:08:51,120
imagine it to be when they've just spiraled so far out of control and the habit

76
00:08:51,120 --> 00:08:58,480
is so strong the cycle is so strong the causal chain so strong and so quick so

77
00:08:58,480 --> 00:09:04,000
ingrained in them that they're unable to get themselves out of it

78
00:09:05,520 --> 00:09:12,440
with depression similar there's experiences and then there's maybe something

79
00:09:12,440 --> 00:09:19,360
bad happened to you what could be one thing and you're you're maybe just get

80
00:09:19,360 --> 00:09:26,520
sad but then it becomes a habit and you start to get more sad when you think

81
00:09:26,520 --> 00:09:30,640
about it and and you try to get rid of the thought and so the thought

82
00:09:30,640 --> 00:09:40,680
becomes a source of aversion and of course because you're giving it this

83
00:09:40,680 --> 00:09:45,200
energy and there's this energy involved with avoiding things or just liking

84
00:09:45,200 --> 00:09:51,120
things that you create more thoughts and so on so all of this is just to say

85
00:09:51,120 --> 00:09:58,520
that the solution from Buddhist Buddhist perspective is to break things into

86
00:09:58,520 --> 00:10:04,880
its constituent parts that's the first important part of things the second

87
00:10:04,880 --> 00:10:11,480
aspect of this and it's involved in the same sort of practice and it

88
00:10:11,480 --> 00:10:14,880
involves another thing that we get wrong about things like anxiety and

89
00:10:14,880 --> 00:10:23,480
depression is to fall into a negative relationship with them to call

90
00:10:23,480 --> 00:10:32,440
something a mental illness even to call something bad isn't it in and of

91
00:10:32,440 --> 00:10:45,440
itself a problem but I don't think we're well capable or ready to or

92
00:10:45,440 --> 00:10:52,320
familiar with the difference between intellectually calling something bad

93
00:10:52,320 --> 00:10:58,240
and feeling bad about it so we feel bad about our depression we feel a

94
00:10:58,240 --> 00:11:04,640
verse to it we feel bad about our anxiety we want to get rid of it I think

95
00:11:04,640 --> 00:11:11,320
these two things together taking a thing as a monolithic problem and seeing

96
00:11:11,320 --> 00:11:15,600
it as a problem you know feeling bad about it are the sorts of things that

97
00:11:15,600 --> 00:11:23,280
lead us to take medication to try to get rid of it to try to not have to deal

98
00:11:23,280 --> 00:11:28,720
with it you know from my perspective taking medication for for mental illness

99
00:11:28,720 --> 00:11:34,360
which should never have that I understand why it does but I think these are

100
00:11:34,360 --> 00:11:41,840
two reasons why we tend to first of all think about taking medication to to

101
00:11:41,840 --> 00:11:46,560
cover it up really you know flood our system with serotonin and so on with

102
00:11:46,560 --> 00:11:53,720
brain chemicals that prevent us from from following the habits that we

103
00:11:53,720 --> 00:12:02,360
normally would right getting into these cycles because we see it as a

104
00:12:02,360 --> 00:12:10,600
problem a problem and because we see experiences because experience is

105
00:12:10,600 --> 00:12:19,280
trigger negative reactions in us so for the first thing about breaking

106
00:12:19,280 --> 00:12:23,960
breaking a problem up into his experiences I mean that's useful to help us see

107
00:12:23,960 --> 00:12:29,560
what's really going on but the second part the idea that we see we have a

108
00:12:29,560 --> 00:12:37,040
negative reaction to things a negative mind states that arise based on our

109
00:12:37,040 --> 00:12:41,680
problems that's where the real issue is and that's what we really come to

110
00:12:41,680 --> 00:12:47,600
see and come to change I mean intellectually I think we have to grasp this idea

111
00:12:47,600 --> 00:12:55,960
of of not reacting negatively of the fact that our reactions are what causes

112
00:12:55,960 --> 00:13:04,520
us suffering so all of the physical aspects of anxiety for example the

113
00:13:04,520 --> 00:13:08,800
butterflies nurse that make the heart beating fast attention and headaches

114
00:13:08,800 --> 00:13:12,800
and whatever they're not the first of all they're distinct from the actual

115
00:13:12,800 --> 00:13:17,160
anxiety they're not anxiety they're physical the second of all they're not a

116
00:13:17,160 --> 00:13:22,200
problem and if we can see them as just experiences that we won't react to

117
00:13:22,200 --> 00:13:30,760
them first of all second of all the actual anxiety depression whatever fear

118
00:13:30,760 --> 00:13:37,320
could be any kind of quote unquote mental illness well on the one hand there's

119
00:13:37,320 --> 00:13:42,320
no denying the fact that they are bad what's really bad about them is that

120
00:13:42,320 --> 00:13:47,960
their reactions they're bad they're a bad reaction and so reacting to them

121
00:13:47,960 --> 00:13:54,360
badly if you don't like your anxiety if you don't like being depressed guess

122
00:13:54,360 --> 00:13:58,960
what you're doing you're you're feeding it you're creating more anxiety more

123
00:13:58,960 --> 00:14:10,480
depression I think this is extremely easy to see in schizophrenia for example

124
00:14:10,480 --> 00:14:13,760
I mentioned schizophrenia I just want to mention it here because my

125
00:14:13,760 --> 00:14:20,000
understanding of schizophrenia is that it involves hallucinations what we call

126
00:14:20,000 --> 00:14:25,360
hallucinations but you see in Buddhism in Buddhist meditation practice we

127
00:14:25,360 --> 00:14:30,720
would call them experiences so maybe you hear voices maybe you see things maybe

128
00:14:30,720 --> 00:14:37,480
you get thoughts that you think come from spirits or or whatever all of those

129
00:14:37,480 --> 00:14:41,640
it's not really important that they are understood as hallucinations it's

130
00:14:41,640 --> 00:14:45,440
important that they're understood as experiences and seen for what they are

131
00:14:45,440 --> 00:14:50,080
and that we don't react to them so if someone's telling you to kill

132
00:14:50,080 --> 00:14:54,760
yourself there was a monk once I knew who apparently had voices telling him to

133
00:14:54,760 --> 00:14:59,640
kill himself no they're just voices if I tell you to kill yourself it doesn't

134
00:14:59,640 --> 00:15:04,880
mean you should right even if it were a real person even if God tells you to

135
00:15:04,880 --> 00:15:12,760
do it it's really actually not sufficient reason to do it so being able to

136
00:15:12,760 --> 00:15:16,720
see our experiences as experiences that's essentially what we do in

137
00:15:16,720 --> 00:15:23,440
mindfulness meditation and so I think these two factors together our ability to

138
00:15:23,440 --> 00:15:27,400
break things apart and and just at least conceptually before we start to

139
00:15:27,400 --> 00:15:32,640
meditate to understand this distinction between turning things into entities

140
00:15:32,640 --> 00:15:38,960
like a depression I have depression and just seeing instead the basic

141
00:15:38,960 --> 00:15:44,080
building blocks of our experience and secondly to take those building blocks

142
00:15:44,080 --> 00:15:50,400
including the problem and seeing them objectively neutrally it's a very

143
00:15:50,400 --> 00:15:57,440
important aspect let it just be what it is and try to create this ability to

144
00:15:57,440 --> 00:16:01,720
see things objectively it's really what allows for wisdom to arise it's

145
00:16:01,720 --> 00:16:06,720
what allows us to see clearly and it's what allows us to let go because our

146
00:16:06,720 --> 00:16:13,360
attachments involve really delusion ignorance and and a lack of

147
00:16:13,360 --> 00:16:20,040
understanding of what really real happiness and peace are that happiness and

148
00:16:20,040 --> 00:16:27,840
peace can't come from fixing things it can't come from experiences it has to

149
00:16:27,840 --> 00:16:32,040
come from the way we look at it and the way we perceive and the way we relate

150
00:16:32,040 --> 00:16:42,040
to experiences peacefully happily and free from suffering so there you go

151
00:16:42,040 --> 00:16:49,280
well a lot of people listening watching 76 viewers which is really that's

152
00:16:49,280 --> 00:16:54,480
about as high as it gets no comments because I disabled comments I think

153
00:16:54,480 --> 00:16:59,760
will keep comments disabled it's a much more peaceful experience that way I'm

154
00:16:59,760 --> 00:17:06,160
actually thinking well I think comments on videos is okay I'm thinking about

155
00:17:06,160 --> 00:17:11,440
that though so I have at least one announcement one that I can remember and

156
00:17:11,440 --> 00:17:17,040
that's that we're moving so here I am in our backyard this was the house

157
00:17:17,040 --> 00:17:22,520
that we're living in real but the house we're living in is a bit small the

158
00:17:22,520 --> 00:17:26,560
meditators don't have a great experience with their accommodations they

159
00:17:26,560 --> 00:17:31,240
don't seem to complain which is great it's a testament to how mindful one

160
00:17:31,240 --> 00:17:37,840
becomes that one can live but they're living in the basement and the rooms are

161
00:17:37,840 --> 00:17:42,360
not real that real bedrooms they're they're makeshift for student accommodation

162
00:17:42,360 --> 00:17:48,320
for the university so the place we're moving to is bigger it has five

163
00:17:48,320 --> 00:17:52,480
bedrooms for meditators and they're they're actual real bedrooms they're it's a

164
00:17:52,480 --> 00:18:02,120
nice new cookie cutter a home and I will have my own apartment so in the same

165
00:18:02,120 --> 00:18:08,840
building but a separate living space with you know I don't have to bump

166
00:18:08,840 --> 00:18:13,480
around with meditators going to the washroom or using the washing dishes and

167
00:18:13,480 --> 00:18:20,480
so on so all in all it's going to be a much better sort of situation for us to

168
00:18:20,480 --> 00:18:27,520
live in I think and we're moving this month so next month we will be in the

169
00:18:27,520 --> 00:18:32,480
new place so for that reason I may be I mean I haven't been that constant but

170
00:18:32,480 --> 00:18:37,360
I may not yet get back into a very constant video routine we'll have to see

171
00:18:37,360 --> 00:18:45,280
how the move goes but next week should be more constant I think that's it I

172
00:18:45,280 --> 00:18:50,640
have more announcements so save them for next time I thank you all for tuning in

173
00:18:50,640 --> 00:19:10,560
I wish you all a good day and see you all soon

