1
00:00:00,000 --> 00:00:03,000
This two will change, this two will pass.

2
00:00:09,000 --> 00:00:15,000
It talks about what happens when you reflect upon these things that leads you to.

3
00:00:15,000 --> 00:00:22,000
It leaves you to the path, it leaves you, these things to help you get closer to the goal.

4
00:00:22,000 --> 00:00:25,000
And when you get closer to the goal, you become free from attachment.

5
00:00:25,000 --> 00:00:31,000
And then he gives a poem, which is often poetry at the end of these suttas.

6
00:00:31,000 --> 00:00:40,000
So whether he actually spoke these or these were meant as a summary of what was said by someone later author, I don't know.

7
00:00:40,000 --> 00:00:45,000
But we like to think that the Buddha spoke these verses as well.

8
00:00:45,000 --> 00:00:50,000
And we're going to basically a summary.

