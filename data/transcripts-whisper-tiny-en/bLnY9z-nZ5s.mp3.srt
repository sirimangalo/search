1
00:00:00,000 --> 00:00:09,600
Are there sings of the Buddha written in modern, everyday English, without the repetition?

2
00:00:09,600 --> 00:00:14,960
Many writings are difficult to comprehend because of the use of foreign words and praises.

3
00:00:14,960 --> 00:00:18,640
Yeah, there's a lot.

4
00:00:18,640 --> 00:00:27,400
The Bhikkhu Bodhi translations and the translation of the Diga Nikaya, all the ones from

5
00:00:27,400 --> 00:00:31,400
wisdom publications.

6
00:00:31,400 --> 00:00:40,640
They do still have some repetition, but for a lot of the denser areas, they just use the

7
00:00:40,640 --> 00:00:52,360
dot, dot, dot, the lepthesis, I'm not sure what it's called, and then for foreign words

8
00:00:52,360 --> 00:01:04,680
and praises, that's not in it much, it still uses some things like tatagata and just

9
00:01:04,680 --> 00:01:10,120
basic words like referring to the Buddha and things like that, I don't think it goes into

10
00:01:10,120 --> 00:01:15,360
any complex poly words.

11
00:01:15,360 --> 00:01:28,720
Yeah, you could kind of acknowledge the aversion towards the repetition, I like the repetition

12
00:01:28,720 --> 00:01:34,760
actually, I think it's kind of fun to have it repeated over and over and over again.

13
00:01:34,760 --> 00:01:39,680
It's important because it's only to drive in certain points.

14
00:01:39,680 --> 00:01:43,520
This is what we talked about in the Sutta study, that the repetition, and you'll see the

15
00:01:43,520 --> 00:01:48,840
Buddha using the same word in many different ways as well.

16
00:01:48,840 --> 00:01:53,400
This is a different technique, but the technique is a rhetorical technique that you'll

17
00:01:53,400 --> 00:01:59,360
see politicians use, they don't just say something, they say it was bad, it was horrible,

18
00:01:59,360 --> 00:02:05,960
it was wicked, it was vile or so on, they'll use lots and lots of adjectives.

19
00:02:05,960 --> 00:02:10,000
That's the same sort of technique being used to, the repetition, because remember it's

20
00:02:10,000 --> 00:02:15,240
a meditation teaching, it's not a theory, if it was just theory, yeah, you could list

21
00:02:15,240 --> 00:02:20,440
them, you say, one, the eye, two, the ear, three, remember these six, and that would

22
00:02:20,440 --> 00:02:28,840
be it, but it's not, the Buddha is sitting there teaching people meditation, so it's

23
00:02:28,840 --> 00:02:34,280
important to appreciate that, obviously, and it's useful to read it all the way through

24
00:02:34,280 --> 00:02:40,040
with all the repetitions, pausing, you know, this is why you should do it, read one, pause

25
00:02:40,040 --> 00:02:44,960
and practice it, read the second one, pause and practice it, read the third one, pause

26
00:02:44,960 --> 00:02:52,840
and practice it, or reflect on it, at least, reflect on each one, each one of the repetitions,

27
00:02:52,840 --> 00:02:56,880
because that's what it's for, it's not so that up here you have, okay, now I know there

28
00:02:56,880 --> 00:03:01,800
are six senses, the eye, the ear, the nose, so that you actually see, ah, this is the

29
00:03:01,800 --> 00:03:10,480
eye, see, light, this is the ear, sound, hearing, this is the nose, this is the tongue

30
00:03:10,480 --> 00:03:21,320
into it, and that you're actually here and now mindful, that's in regards to repetition,

31
00:03:21,320 --> 00:03:30,080
the difficult to comprehend is, for sure, there's a lot of modern translations, but nothing

32
00:03:30,080 --> 00:03:36,600
beats learning poly, you can come to our poly forum and start, we start doing translations

33
00:03:36,600 --> 00:03:44,640
at poly.ceremungalode.org, front slash forum, where we have a poly forum, you can come out

34
00:03:44,640 --> 00:03:51,200
and start to read the polysuitas with it in poly, and we're doing the commentaries right

35
00:03:51,200 --> 00:03:58,920
now, the dumbapada stories, but if you have questions about poly, you can come and learn,

36
00:03:58,920 --> 00:04:07,080
that's the best, really, because any translation is by necessity, make shift, and it misses

37
00:04:07,080 --> 00:04:15,200
some of the clarity and precision of the original language, maybe someone else knows the

38
00:04:15,200 --> 00:04:44,560
form address, poly.ceremungalode.org, front slash forum.

