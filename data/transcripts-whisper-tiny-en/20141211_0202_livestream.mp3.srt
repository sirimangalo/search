1
00:00:00,000 --> 00:00:16,320
Okay, good morning, broadcasting live from Chiang Mai again, heading off to Bangkok this

2
00:00:16,320 --> 00:00:31,920
evening and then Sri Lanka on the 20th, be back in Thailand, January 19th, and then staying,

3
00:00:31,920 --> 00:00:48,320
giving courses somewhere in the forest until the 19th of February and back to Canada.

4
00:00:48,320 --> 00:00:59,960
I was, I've heard a couple of things recently that made me think about the issue of truth

5
00:00:59,960 --> 00:01:09,480
and right view subjectivity, objectivity.

6
00:01:09,480 --> 00:01:21,760
There's a common theme in society, in our dealings with our fellow human beings, which

7
00:01:21,760 --> 00:01:46,280
concerns, the, I believe, is a postmodern concept of subjective truth, relative truth and

8
00:01:46,280 --> 00:01:51,880
circumstantial truth or right.

9
00:01:51,880 --> 00:02:06,680
It's expressed by the idea to each their own, or, I believe, the idiom in the West,

10
00:02:06,680 --> 00:02:15,280
which doesn't sound, I'm not sure exactly what it's referring to, but different folks,

11
00:02:15,280 --> 00:02:19,560
different strokes, or different strokes, different folks, I'm not quite sure exactly

12
00:02:19,560 --> 00:02:30,920
what that's referring to, it doesn't sound, anyway, won't touch that one, and that kind

13
00:02:30,920 --> 00:02:38,960
of thing to each their own, meaning, you mean different things, the sense I get is people

14
00:02:38,960 --> 00:02:50,600
use this as a means of avoiding conflict where views are concerned, lay people, ordinary

15
00:02:50,600 --> 00:02:55,960
people in the world tend to be very good at avoiding conflict, they have to be otherwise

16
00:02:55,960 --> 00:03:03,800
they can't get along in society, so the conflict doesn't occur at the level of views

17
00:03:03,800 --> 00:03:08,600
because people keep their views to themselves, it's not worth it, and they don't hold

18
00:03:08,600 --> 00:03:17,440
the views very strongly because they're more concerned with material advancement and

19
00:03:17,440 --> 00:03:28,400
sufficiency, so where they conflict is more in the realm of sensuality, as they get caught

20
00:03:28,400 --> 00:03:35,920
up in, tend to get caught up in sensual pleasures, and as a result this leads to conflict

21
00:03:35,920 --> 00:03:43,920
as a matter of course, fighting and bickering and arguing and wrangling for competing

22
00:03:43,920 --> 00:03:54,040
for resources and so on, but for religious people views are much more important and defined

23
00:03:54,040 --> 00:04:00,920
one's life, and for those of us who tend to be a little bit antisocial sticking to ourselves

24
00:04:00,920 --> 00:04:06,440
they can become a sticking point when we get together because we are much more absorbed

25
00:04:06,440 --> 00:04:13,800
in our own views and our own paths, so when we meet someone with different paths, they

26
00:04:13,800 --> 00:04:23,960
say that religious people tend to fight moral for views and opinions, and even are expected

27
00:04:23,960 --> 00:04:35,720
to debate and stick to their views, views are more important, beliefs and so on, and so

28
00:04:35,720 --> 00:04:43,080
it's interesting to hear lay people talk about these things entirely, say things like

29
00:04:43,080 --> 00:04:56,200
tau kong kau, it means it's right to them, it's right, it's true for them, right for them,

30
00:04:56,200 --> 00:05:06,520
or kong kong kau, that's where they find happiness, or that's their happiness, meaning

31
00:05:06,520 --> 00:05:17,160
you shouldn't judge them because to each their own, you'll find happiness in different ways.

32
00:05:17,160 --> 00:05:25,800
And so the interesting thing here from a Buddhist point of view is that it's at once philosophically

33
00:05:25,800 --> 00:05:35,280
useful and dangerous, it's useful in the sense of reminding us that it's not exactly

34
00:05:35,280 --> 00:05:45,400
that Buddhism tells you what you should do, or even what is right, not exactly, even though

35
00:05:45,400 --> 00:05:54,600
the Buddha used the words right and true and truth, or right anyway that's not

36
00:05:54,600 --> 00:06:01,040
worry about truth yet, but talking about what's right, the Buddha seems to be quite open

37
00:06:01,040 --> 00:06:08,400
about the idea of there being different paths that lead to different goals, and in a sense

38
00:06:08,400 --> 00:06:13,480
that I can't think of a place where he actually said, he seems to have been quite open

39
00:06:13,480 --> 00:06:19,080
to the idea of to each their own, in the sense that he would often just explain to people,

40
00:06:19,080 --> 00:06:25,840
you know, this path, and it leads here, there's that path and it leads there, and most

41
00:06:25,840 --> 00:06:32,600
often he wouldn't even say, this path is better, that path is better, that times he did,

42
00:06:32,600 --> 00:06:39,920
but for the most part he was quite open and quite happy to just leave it at to each

43
00:06:39,920 --> 00:06:43,680
their own, you know, which would you choose, this path you do, it leads you to hell,

44
00:06:43,680 --> 00:06:51,160
this path you do at least you to heaven, this path you practice, it leads you to freedom,

45
00:06:51,160 --> 00:07:00,880
and then just leave it up to the person, but the useful side is in terms of not pushing

46
00:07:00,880 --> 00:07:16,320
or not judging, but simply laying out a framework of what is true and what is possible,

47
00:07:16,320 --> 00:07:24,920
but it's of course dangerous because it most often slips into right for them, something

48
00:07:24,920 --> 00:07:37,400
that can be right or true or real for a certain individual, and so it leads to the idea

49
00:07:37,400 --> 00:07:49,960
that anything goes, and it misses the fact that there are laws of nature, especially beyond

50
00:07:49,960 --> 00:08:03,280
the laws that physics and biology and so on, the material sciences, except to recognize,

51
00:08:03,280 --> 00:08:12,280
is even with physical laws of gravity and evolution and natural selections, even there,

52
00:08:12,280 --> 00:08:23,400
it just doesn't yet say anything about ethics, not directly, meaning one can take it, take

53
00:08:23,400 --> 00:08:38,880
these laws and use them as one likes, and in fact potentially act unethically for one's

54
00:08:38,880 --> 00:08:46,760
own benefit, according to the physical, the laws of physics, the laws of biology, chemistry,

55
00:08:46,760 --> 00:08:55,320
one can be a real jerk or a real conniving manipulative individual and still potentially have

56
00:08:55,320 --> 00:09:06,520
it be right for them, but on a less extreme level people can do things like take drugs

57
00:09:06,520 --> 00:09:15,840
or alcohol, and the laws of the material sciences don't negate the idea, don't say anything

58
00:09:15,840 --> 00:09:24,120
necessarily about this being a bad thing, if some people want to drink alcohol all day

59
00:09:24,120 --> 00:09:29,640
every day, well that leads to each their own, that's what we would say, what they would

60
00:09:29,640 --> 00:09:34,000
say, not what we would say.

61
00:09:34,000 --> 00:09:44,160
So the problem comes, what's missing here is the fact that there is the claim made

62
00:09:44,160 --> 00:09:52,520
by Buddhism, that there are laws of nature that go beyond simply the physical, that the

63
00:09:52,520 --> 00:09:58,840
mind as well follows the laws of nature, which of course material sciences should accept

64
00:09:58,840 --> 00:10:10,240
because for a materialist or a physicalist, the mind is simply a product of the matter,

65
00:10:10,240 --> 00:10:18,400
which of course we disagree with, but to some extent, the mind can be, can arise based

66
00:10:18,400 --> 00:10:24,560
on matter, but it usually arises based on the mind.

67
00:10:24,560 --> 00:10:29,880
I think the, now I'm not even sure if the physical can create the mind, but it can definitely

68
00:10:29,880 --> 00:10:40,120
influence it anyway, but so the idea that the mind should follow laws of certain laws

69
00:10:40,120 --> 00:10:48,400
of its own, or laws of nature shouldn't be that foreign to scientific thinking people,

70
00:10:48,400 --> 00:10:59,320
it makes more sense than the alternative of it being arbitrary or unimpeded in the sense

71
00:10:59,320 --> 00:11:09,400
of being able to be in complete control of its reality, which is how we normally take

72
00:11:09,400 --> 00:11:16,640
it, we have this dichotomous sort of outlook on why the physical sciences are completely

73
00:11:16,640 --> 00:11:27,920
deterministic, and we, I am completely non-deterministic, can't remember what the

74
00:11:27,920 --> 00:11:39,680
problem is, but we have freedom of will, for most of us, practically speaking, this is how

75
00:11:39,680 --> 00:11:43,200
we approach life.

76
00:11:43,200 --> 00:11:46,800
So therefore we get to come to the idea to each their own, these are the physical laws,

77
00:11:46,800 --> 00:11:52,160
what you do with them is up to you, you want to be a drug addict, maybe you'll find

78
00:11:52,160 --> 00:12:05,560
happiness that way, you want to be a hedonistic, a sex maniac, or you want to engage in

79
00:12:05,560 --> 00:12:15,280
free love and whatever, so it is free, that's fine for you, if you want to practice

80
00:12:15,280 --> 00:12:27,120
abstinence, see celibacy, then to each their own, and it usually devolves into a sort

81
00:12:27,120 --> 00:12:32,960
of grabbing at whatever pleasure you can in the here and now, and things like celibacy

82
00:12:32,960 --> 00:12:42,160
are kind of frowned upon because it assumes, it starts to assume sort of laws of nature,

83
00:12:42,160 --> 00:12:47,520
often Christian, Judeo-Christian in the sense of God doesn't want you to do that, so you

84
00:12:47,520 --> 00:12:55,280
can't just do whatever you want to this idea of sort of a universal right and wrong, which

85
00:12:55,280 --> 00:13:01,200
is sort of being discarded or by many people has been discarded in favor of grasping

86
00:13:01,200 --> 00:13:08,760
at whatever makes you happy, whatever makes you happy, but Buddhism cuts through all of

87
00:13:08,760 --> 00:13:15,600
this by saying that it can't make you happy, yes, indeed to each their own, do what you

88
00:13:15,600 --> 00:13:27,920
want, but no, no, no to, it still in the sense of whatever makes you happy, but there's

89
00:13:27,920 --> 00:13:33,160
only one set of things that's going to make you happy, there's only a certain subset

90
00:13:33,160 --> 00:13:40,800
of reality that will make, that will lead to happiness, and this chafeset people I think

91
00:13:40,800 --> 00:13:48,480
because we like to think of our ability to find happiness wherever we want, I want to

92
00:13:48,480 --> 00:13:54,840
be happy as a business person, I want to be happy as a teacher, I want to be happy as a husband

93
00:13:54,840 --> 00:14:02,800
or a wife, I want to be happy doing this or doing that, I want to be happy in heaven, and

94
00:14:02,800 --> 00:14:08,440
people want to be able to find their happiness, whatever makes them happy, I'm happy eating

95
00:14:08,440 --> 00:14:18,720
this food or that food, I'm happy drinking alcohol and going to parties and bars, etcetera.

96
00:14:18,720 --> 00:14:31,120
So Buddhism denies this, denounces this idea, rejects this idea as wrong, as untrue

97
00:14:31,120 --> 00:14:38,840
as delusional, but you can't find happiness wherever you want, these things do not lead

98
00:14:38,840 --> 00:14:45,080
to happiness, that's the claim.

99
00:14:45,080 --> 00:14:50,360
And so there's nothing wrong with trying to find, they're trying to be sort of open to

100
00:14:50,360 --> 00:14:57,160
other people doing whatever they want, but we should never just accept, we should never

101
00:14:57,160 --> 00:15:08,280
go the next step and say, if it makes you happy or I'm glad you're happy doing this

102
00:15:08,280 --> 00:15:15,560
or that, I mean, even saying these things isn't exactly wrong because it often is an

103
00:15:15,560 --> 00:15:23,600
around about way of reminding people, so if someone's an alcoholic or let's not say

104
00:15:23,600 --> 00:15:29,320
an alcoholic or someone who has the firm belief that drinking alcohol and regular basis

105
00:15:29,320 --> 00:15:36,000
for pleasure, for the feeling of freedom that it gives to you, so feeling that it has

106
00:15:36,000 --> 00:15:42,440
the sense that this makes them happy, reminding them, saying to them, well, as long as

107
00:15:42,440 --> 00:15:47,640
it makes you happy, can actually be a good lesson for these people because it doesn't

108
00:15:47,640 --> 00:15:49,200
make them happy.

109
00:15:49,200 --> 00:15:53,560
And when you remind people of why they're doing something, they say they're doing it

110
00:15:53,560 --> 00:15:57,320
for happiness, they say, you know, as long as it makes you happy, as long as it really

111
00:15:57,320 --> 00:16:06,680
brings you happiness, not even really, you know, it has to be open and casual about it,

112
00:16:06,680 --> 00:16:12,520
but this kind of lesson can actually be useful, but we should not hold the, we have to

113
00:16:12,520 --> 00:16:19,600
be clear that this is not actually possible, that it's not possible for these things to

114
00:16:19,600 --> 00:16:29,000
lead to happiness. Happiness doesn't come from such a place, such a thing. It's because

115
00:16:29,000 --> 00:16:37,160
of the laws of nature. So before I was talking about Mara, how Mara gets in our way,

116
00:16:37,160 --> 00:16:42,160
we want to be happy in the world, but that kind of teaching of how evil is what is causing

117
00:16:42,160 --> 00:16:48,760
us, preventing us from being happy. That kind of teaching is kind of foundational. It's

118
00:16:48,760 --> 00:16:53,440
not really a deep teaching about the ultimate nature, it doesn't say much about the ultimate

119
00:16:53,440 --> 00:17:02,000
nature of reality, not in, not in terms of the general idea of evil. There's another

120
00:17:02,000 --> 00:17:08,560
teaching that that explains the fundamental nature of reality and why it's not possible

121
00:17:08,560 --> 00:17:25,880
to find happiness forever. You want, why the mind is unable to, unable to delight in whatever

122
00:17:25,880 --> 00:17:37,920
path it follows, to be happy with whatever it does. Be happy with the things that it comes

123
00:17:37,920 --> 00:17:51,080
in contact with. And this is the core teaching. I, a deep core teaching of the Buddha

124
00:17:51,080 --> 00:18:00,800
on the three characteristics, which most many of us are familiar. The three characteristics

125
00:18:00,800 --> 00:18:10,600
of all are risen phenomena, all phenomena in the world in Samsaya. And these are the characteristics

126
00:18:10,600 --> 00:18:19,680
of impermanence, suffering in non-self, or impermanence. Dukkha, no, how do you translate

127
00:18:19,680 --> 00:18:23,960
dukkha? Suffering I guess is the best, but it's not, there's not the best, it's just

128
00:18:23,960 --> 00:18:43,400
all we've got. And non-self, maybe dukkha would be best translated as dis-ease or unpleasing,

129
00:18:43,400 --> 00:18:53,240
nonpleasing. Unpleasing might be a good one, just because of how, what it means, unpleasing.

130
00:18:53,240 --> 00:18:59,120
I wouldn't say unpleasant. Maybe unpleasant also works, but unpleasing. I don't even think

131
00:18:59,120 --> 00:19:06,280
it's a word, but I just made up that word. It's a good one. Dukkha means unpleasing, in

132
00:19:06,280 --> 00:19:15,400
sense of not pleasing. Maybe that's not even true, because when you define please, it's

133
00:19:15,400 --> 00:19:27,760
something that does make you delight temporarily, unsatisfying. Anyway, once we explain

134
00:19:27,760 --> 00:19:32,480
it, it becomes clear. So suffering is fine as long as we explain it. What is meant, it's

135
00:19:32,480 --> 00:19:43,520
not actually the literal meaning. It's just the word. So the problem, the reason why we

136
00:19:43,520 --> 00:19:49,440
can't find happiness in the world, why we can't just do whatever we want, and why it

137
00:19:49,440 --> 00:19:58,000
doesn't work to say whatever makes you happy. There's no thing will make you happy. There's

138
00:19:58,000 --> 00:20:04,280
no thing in the world that can possibly make you happy, because there are laws of nature

139
00:20:04,280 --> 00:20:11,320
that affect the mind, that everything inside of us, you're in the mind of the body and

140
00:20:11,320 --> 00:20:23,320
the whole world around us is impermanent. The reason why we seek things out, we misunderstand

141
00:20:23,320 --> 00:20:38,520
them to be permanent, to be stable, to be lasting. The nature of pleasure or desire is

142
00:20:38,520 --> 00:20:55,400
to cling, is to pull together moments in time and hold on to them, which is sort of

143
00:20:55,400 --> 00:21:05,400
the equivalent of, when I was young, I probably was about 12, and one of the first times

144
00:21:05,400 --> 00:21:16,640
I went to I'm not 12, it was young, and maybe maybe 8, 10. We went to a mall in Sudbury,

145
00:21:16,640 --> 00:21:23,080
this town in Northern Ontario, Sydney in Northern Ontario, and they had these escalators,

146
00:21:23,080 --> 00:21:28,000
you know, for me growing up in the forest, an escalator was kind of a big deal, so you

147
00:21:28,000 --> 00:21:34,400
know, playing on the escalators while my parents shopped. I remember grabbing onto the

148
00:21:34,400 --> 00:21:41,680
side of the escalator, another hand rail, I was at the top, and it was an up escalator,

149
00:21:41,680 --> 00:21:51,640
and I held up my arm and I tried to push back on the arm rail, and I pushed really hard

150
00:21:51,640 --> 00:21:59,960
in my elbow snapped, and I've had 10 to the night ever since. It was exacerbated later

151
00:21:59,960 --> 00:22:08,880
on, but that was the point, I think I've still got it. I would exert my right arm anyway.

152
00:22:08,880 --> 00:22:14,840
It's kind of like that, trying to stop an immovable forest, and you only end up hurting

153
00:22:14,840 --> 00:22:23,960
yourself. It's kind of dumb, but it's the nature of desire to do that, to grab on and

154
00:22:23,960 --> 00:22:35,960
try to stop this unstoppable forest. The power that you actually exert over things is

155
00:22:35,960 --> 00:22:48,440
so minuscule, you can affect the outcome, the effect is much smaller than we think, much

156
00:22:48,440 --> 00:22:54,840
smaller than we usually understand it to be. And so as a result, we end up hurting ourselves

157
00:22:54,840 --> 00:23:06,840
and suffering, because change is built into the system. You can hold on to little things

158
00:23:06,840 --> 00:23:16,040
in life for some time, but the desire that you have will inevitably go counter to, it

159
00:23:16,040 --> 00:23:23,440
will try to make things last, things that don't last, and it builds up and builds up until

160
00:23:23,440 --> 00:23:31,200
there's a larger and larger expectation of that is further and further outside of what

161
00:23:31,200 --> 00:23:40,440
is actually possible, and then it crashes and something changes significantly and you suffer.

162
00:23:40,440 --> 00:23:45,960
So you can build up this sense of stability in life, you have a nice house and a car and a job

163
00:23:45,960 --> 00:23:53,320
and everything, and it can all come crashing down in a moment. And if you have great desire

164
00:23:53,320 --> 00:24:18,600
in your mind, of course, if your mind is pure, then all of this is no problem.

