1
00:00:00,000 --> 00:00:07,080
proper noting procedure. Can you cover proper noting during meditation? Do you

2
00:00:07,080 --> 00:00:13,720
always come back to the breath? If you are asking about the tradition that we

3
00:00:13,720 --> 00:00:19,160
follow, we are not technically practicing mindfulness of the breath. Anapanas

4
00:00:19,160 --> 00:00:25,440
sati mindfulness of the in breaths and the out breaths is technically samata

5
00:00:25,440 --> 00:00:31,480
meditation. Vipasana cannot arise when focused on the breath because breath is

6
00:00:31,480 --> 00:00:37,280
not an ultimate reality. The ultimate reality is the sensations that lead to

7
00:00:37,280 --> 00:00:43,120
the concept of breath, whether at the nose or the stomach. At the nose there

8
00:00:43,120 --> 00:00:48,400
will be sensations of heat and cold. At the stomach there will be sensations

9
00:00:48,400 --> 00:00:53,880
of tension and release. You don't have to focus on sensations related to the

10
00:00:53,880 --> 00:00:59,880
breath at all. You can focus on the bodily sensations related to sitting, saying

11
00:00:59,880 --> 00:01:07,520
to yourself sitting, sitting or when walking, walking, walking. When practicing

12
00:01:07,520 --> 00:01:12,360
walking meditation, you would not focus on the breath. You would focus on the

13
00:01:12,360 --> 00:01:17,520
movements of the feet. There is even frustration meditation where you focus on

14
00:01:17,520 --> 00:01:22,840
the movements of the hands, the bending of the back and so on. When eating, you

15
00:01:22,840 --> 00:01:29,600
can practice eating meditation, being mindful of chewing, chewing and swallowing.

16
00:01:29,600 --> 00:01:35,600
The easiest object to be mindful of, the most coarse and most obvious is the

17
00:01:35,600 --> 00:01:40,440
body. The mind is more difficult to observe because it is much quicker than the

18
00:01:40,440 --> 00:01:45,840
body. The mind is likened to be a wild animal. When a hunter wishes to catch a

19
00:01:45,840 --> 00:01:50,120
wild animal, they don't run around the jungle chasing after it. They wait

20
00:01:50,120 --> 00:01:54,960
quietly by the water, knowing that the wild animal will come to them. The body

21
00:01:54,960 --> 00:02:00,080
is like the water. The place where the mind comes and can be caught. In the

22
00:02:00,080 --> 00:02:06,480
Visutimaga, Venerable Bhutagosa says that as the body becomes clearly understood,

23
00:02:06,480 --> 00:02:12,080
the mind becomes clearly understood along with it. By focusing on the body, you

24
00:02:12,080 --> 00:02:16,880
are indirectly learning about the mind, which should make perfect sense because

25
00:02:16,880 --> 00:02:22,120
the mind is what is doing the focusing. As you observe the stomach, for instance,

26
00:02:22,120 --> 00:02:26,440
the rising and falling, you learn all sorts of things about the mind. You will

27
00:02:26,440 --> 00:02:31,960
see liking and disliking, boredom and aversion, controlling and stressing. The

28
00:02:31,960 --> 00:02:36,440
whole of the practice really is using the body in order to see the mind and

29
00:02:36,440 --> 00:02:42,240
when you see the mind, whether it be feelings, thoughts or emotions, you focus

30
00:02:42,240 --> 00:02:46,960
on those and when they are gone, you come back again to the body because it is

31
00:02:46,960 --> 00:03:14,080
still there.

