1
00:00:00,000 --> 00:00:04,080
Cool, could you really relate to this question of the art or I don't feel like

2
00:00:04,440 --> 00:00:07,760
Okay, let's quit this one in art. I don't know. I don't I am

3
00:00:08,160 --> 00:00:13,600
How art is interpreted in the for me art is a subjective manifestation of self

4
00:00:13,600 --> 00:00:16,000
But what about art is Buddhism?

5
00:00:17,160 --> 00:00:19,160
Yeah, I wish

6
00:00:20,760 --> 00:00:24,560
All right, well, let's answer you ask it all answered go ahead. Ask again

7
00:00:25,680 --> 00:00:28,080
Okay, slowly and carefully

8
00:00:28,080 --> 00:00:31,760
How art I'm going to use this same question

9
00:00:33,680 --> 00:00:35,680
That's the idea go for it

10
00:00:35,680 --> 00:00:43,360
Yeah, how art how art is interpreted in Buddhism for me art is a subjective manifestation of the cell

11
00:00:43,680 --> 00:00:48,720
But what about art in Buddhism? Okay, I know I'm a question. There is no cell, right?

12
00:00:48,720 --> 00:01:00,640
Hmm, I don't know. That doesn't work. It's not a very good. Not a very good question. That's all

13
00:01:03,760 --> 00:01:08,240
You know experience is not self and everything that arises is not self so

14
00:01:09,600 --> 00:01:11,600
You could say

15
00:01:11,840 --> 00:01:16,400
But you know there is no anything entities are not a part of reality

16
00:01:16,400 --> 00:01:20,720
Something arises there for it exists for there for it's really well

17
00:01:20,720 --> 00:01:26,800
I know what the author meant really because you have the own personality and you have to express

18
00:01:27,520 --> 00:01:29,920
Your words become your expression

19
00:01:30,720 --> 00:01:35,120
Yes, it's a manifestation of one's self in that sense

20
00:01:36,000 --> 00:01:40,400
one's being or one's state at that time and it can be

21
00:01:40,400 --> 00:01:45,920
In that sense it can it can show you things about yourself that you didn't know and it can show people things

22
00:01:46,480 --> 00:01:53,280
That are far more profound and just say looking in someone's eyes or watching someone's super official behavior

23
00:01:53,840 --> 00:01:59,440
Especially since we put on such errors, right? We all look like ordinary people, but who knows what's going on in everybody

24
00:02:00,160 --> 00:02:02,800
Owen can be a serial killer for all of you know

25
00:02:04,640 --> 00:02:06,640
On the Buddha

26
00:02:06,640 --> 00:02:12,560
Can be a Buddha for all. I think we don't know what's going on inside people's minds

27
00:02:14,400 --> 00:02:19,200
I think art is of course art can bring out some of these

28
00:02:20,240 --> 00:02:21,600
inner

29
00:02:21,600 --> 00:02:24,800
Working so certainly I would I would give it that

30
00:02:27,840 --> 00:02:29,840
Yeah expression music as well

31
00:02:32,080 --> 00:02:34,800
But you know there's that as by other aspect of it

32
00:02:34,800 --> 00:02:38,960
That is the the addiction aspect or the

33
00:02:40,160 --> 00:02:42,160
Attraction aspect

34
00:02:42,160 --> 00:02:44,160
So you become addicted to these states

35
00:02:44,720 --> 00:02:48,880
The the the work itself is interesting especially from the point of view

36
00:02:49,200 --> 00:02:55,040
You have a Buddhist or someone is looking objectively at it to see what's going on in someone's mind for example

37
00:02:55,120 --> 00:02:58,320
Psychologists use art art therapy. My aunt is

38
00:03:00,000 --> 00:03:02,000
Leader in art therapy in the world apparently

39
00:03:02,000 --> 00:03:08,720
She's a PhD at Loyola Mary Mountain. She was one of the pioneers in art therapy

40
00:03:11,920 --> 00:03:14,640
And so you can use it in that way

41
00:03:14,960 --> 00:03:19,840
But the act of making art on the other hand making music for example

42
00:03:19,840 --> 00:03:25,440
But art in general even if it's not just the addiction to pleasure as it is generally in music

43
00:03:25,440 --> 00:03:33,840
Or the addiction to the strong motion. It's addiction to to the states to addiction to self to whatever it is that makes it yourself

44
00:03:33,840 --> 00:03:38,000
Whatever is going on inside you become intoxicated by it made easily

45
00:03:39,120 --> 00:03:42,560
So and and so you know artists can be fairly

46
00:03:42,960 --> 00:03:45,840
Egotistical and and self you know self-absorbed

47
00:03:46,640 --> 00:03:48,720
Not necessarily in the way you think of it

48
00:03:49,360 --> 00:03:50,480
Very much

49
00:03:50,480 --> 00:03:56,000
Interested in the self and interested in who they are and I am this sort of person that I am that sort of person

50
00:03:57,040 --> 00:03:59,040
Maybe that's a gross generalization

51
00:03:59,040 --> 00:04:04,960
Well because all artists look at the answer who are who am I they want to define themselves

52
00:04:09,840 --> 00:04:11,840
and

53
00:04:11,840 --> 00:04:17,360
So for this person to clarify what because this is not

54
00:04:17,360 --> 00:04:21,120
The the stop the work you see it's

55
00:04:22,240 --> 00:04:25,040
It's a mirror of your mind. It's not yourself

56
00:04:26,720 --> 00:04:35,520
There's nothing wrong with the art itself and it can be interesting for other people, but the act of creating art is

57
00:04:38,400 --> 00:04:40,400
Suspect

58
00:04:40,880 --> 00:04:43,360
Yeah, so the act of creating

59
00:04:43,360 --> 00:04:48,560
It's like a day visit but it's danger. It's it's subject to

60
00:04:49,600 --> 00:04:50,800
to

61
00:04:50,800 --> 00:04:54,640
Problems, I mean obviously taking a paintbrush an elephant can paint them

62
00:04:55,040 --> 00:04:58,720
No, in Thailand the elephants create pictures make pictures of flowers

63
00:04:59,520 --> 00:05:01,280
With their trunk so

64
00:05:01,280 --> 00:05:05,360
There's nothing intrinsically wrong with with making art

65
00:05:06,000 --> 00:05:10,400
But when it becomes art there's something there, right? You're you're putting something into it

66
00:05:10,400 --> 00:05:16,800
You're putting in emotive state into it or something into it, which is generally based on addiction attachment

67
00:05:18,960 --> 00:05:21,520
To either self or emotion

68
00:05:23,760 --> 00:05:25,760
There's the danger there

69
00:05:27,040 --> 00:05:29,040
Thing you might say is it's an inexact

70
00:05:29,920 --> 00:05:33,440
Science, you know art is not a objective

71
00:05:34,400 --> 00:05:36,000
It's very very subjective, right? So

72
00:05:36,000 --> 00:05:45,280
Don't say what do you think about using art as a tool and your subject as a

73
00:05:46,160 --> 00:05:48,160
Use reality as a subject

74
00:05:48,160 --> 00:05:50,160
For your art to

75
00:05:51,600 --> 00:05:57,360
Understand that I just think it's it's inexact. I mean, why not use mindfulness much more reliable

76
00:05:58,240 --> 00:06:02,000
That's what I was trying to get at just knows that the inexactitude of it is

77
00:06:02,000 --> 00:06:06,240
Is means that it's very easy to get off track. You don't really know where it's leading you

78
00:06:06,240 --> 00:06:12,160
You can be all touchy feely about it, but you don't really know what it's and you get a sense of what it's doing to you

79
00:06:12,160 --> 00:06:14,160
but it's

80
00:06:14,160 --> 00:06:16,800
You know the word art is loaded with

81
00:06:17,280 --> 00:06:20,160
subjectivity and and inexactitude

82
00:06:20,880 --> 00:06:25,440
So to get some to get into going in a specific direction

83
00:06:26,320 --> 00:06:28,320
that's like driving

84
00:06:28,320 --> 00:06:32,960
Driving the car with poor alignment or something. No, it's like

85
00:06:35,680 --> 00:06:37,840
Yeah, driving a car with a touching the steering wheel

86
00:06:38,480 --> 00:06:44,320
If you are driving a car with your knees or something, it's not you know, you're not not very easy to stay on the road

87
00:06:45,680 --> 00:06:51,600
Yeah, I understand that we don't have many examples for art, which is quite

88
00:06:51,600 --> 00:07:00,400
Now seriously, we don't have much example that we could say it's actually beneficial or

89
00:07:01,280 --> 00:07:07,440
Well, they do use it for you know psychologists use it and therapists use it and they seem to have good results

90
00:07:08,640 --> 00:07:11,920
But there was you know good results is subjective as well

91
00:07:12,080 --> 00:07:15,680
What do you mean by good results to people becoming light and making art? I would say no

92
00:07:17,440 --> 00:07:19,440
Or rarely if they do

93
00:07:19,440 --> 00:07:21,440
I haven't heard

94
00:07:23,360 --> 00:07:25,360
It's like it has become to

95
00:07:26,000 --> 00:07:29,680
Intune with their selves and become you know if anything become more

96
00:07:30,800 --> 00:07:32,320
self

97
00:07:32,320 --> 00:07:33,200
more

98
00:07:33,200 --> 00:07:37,600
Center not centered but self assured more

99
00:07:37,920 --> 00:07:40,800
You know people are all on about being confident and so on

100
00:07:40,800 --> 00:07:47,680
Confident can be very dangerous as we were talking about earlier. It's something that is better to not be sure of yourself like Socrates for example

101
00:07:51,200 --> 00:07:53,200
Yeah, that's right

102
00:07:53,840 --> 00:07:59,040
Someone says art is the root of artificial which from Buddhist point is not a very beneficial thing

103
00:07:59,840 --> 00:08:01,840
So art is actually creating illusion

104
00:08:02,800 --> 00:08:04,880
Right, I would say art is more

105
00:08:05,600 --> 00:08:07,600
Not much more to do

106
00:08:07,600 --> 00:08:12,240
Not necessarily. It's it's like writing a letter for example. If I write a paper

107
00:08:12,960 --> 00:08:20,640
I'm could be talking about ultimate reality if I make a piece of art. I could be expressing a feeling or or a part of myself

108
00:08:21,680 --> 00:08:26,320
It's a communication really art is communication. It's a medium

109
00:08:27,360 --> 00:08:29,920
Yeah, I think expression is a matter of yeah

110
00:08:29,920 --> 00:08:36,080
I think expression is a matter of your personal feelings. What you're gonna express you or you're gonna

111
00:08:37,280 --> 00:08:39,280
If

112
00:08:39,280 --> 00:08:43,040
Gonna set this satisfaction gonna express yourself

113
00:08:44,240 --> 00:08:46,240
Because you know

114
00:08:46,240 --> 00:08:49,600
like if you if you're looking for satisfaction

115
00:08:52,560 --> 00:08:54,160
Then

116
00:08:54,160 --> 00:09:00,800
That's the one part of expression. The other is like just to show your emotions like anger

117
00:09:01,040 --> 00:09:08,000
That's another but you know if you angry or something you throw and you press yourself someone says you can

118
00:09:08,960 --> 00:09:11,040
Show a manifestation of the Buddha

119
00:09:12,000 --> 00:09:16,800
Art in art you can show a manifestation of Buddha to help someone realize something

120
00:09:17,760 --> 00:09:21,440
Yeah, that's I think that's the point. I was making it is a communication

121
00:09:21,440 --> 00:09:24,080
but I'm skeptical

122
00:09:25,600 --> 00:09:27,600
because

123
00:09:29,120 --> 00:09:32,320
It's it's it's not really just a communication the medium is

124
00:09:33,360 --> 00:09:35,360
so overwhelming

125
00:09:37,680 --> 00:09:45,840
Because you've defined it as art if you write you know, why aren't you using letters something that is very exact and this communicating a meaning?

126
00:09:47,280 --> 00:09:49,280
Art the whole

127
00:09:49,280 --> 00:09:51,280
definition of art is

128
00:09:52,000 --> 00:09:54,000
to evoke

129
00:09:54,800 --> 00:09:56,800
Something beyond just

130
00:09:57,600 --> 00:09:59,600
An understanding of what you're trying to say

131
00:10:00,640 --> 00:10:02,640
If you want to get the point across

132
00:10:04,080 --> 00:10:07,840
No, I am suspicious that there that inherent in art is some

133
00:10:10,320 --> 00:10:12,320
emotive

134
00:10:14,960 --> 00:10:16,960
Intent to evoke a response

135
00:10:16,960 --> 00:10:18,960
which

136
00:10:21,360 --> 00:10:27,840
No, it tend to think it's is a negative thing as opposed to just evoke understanding

137
00:10:28,720 --> 00:10:32,080
I'm skeptical that you could look at a painting and realize wow

138
00:10:32,800 --> 00:10:34,800
That's what the Buddha meant or

139
00:10:36,160 --> 00:10:40,560
I would say it could no longer be called art and would have to be called simply communication

140
00:10:41,280 --> 00:10:45,280
I don't think art is capable in my meaning of the word of a

141
00:10:45,280 --> 00:10:47,280
I'm objective communication

142
00:10:48,160 --> 00:10:49,520
but

143
00:10:49,520 --> 00:10:56,400
No, I'm sure many people just yeah, I don't know what kind of artist person talking about their any question

144
00:10:57,040 --> 00:10:59,040
well

145
00:10:59,040 --> 00:11:04,800
They really are it wouldn't be art if it was if it was really a communication

146
00:11:05,920 --> 00:11:09,920
But no, I think it's everything but not communication really I mean

147
00:11:09,920 --> 00:11:14,560
If you want if that's something to say just say it

148
00:11:16,240 --> 00:11:18,240
Why are you gonna like

149
00:11:18,240 --> 00:11:24,080
The fact that it becomes art is is not a functioning thing from a Buddhist point of view, but no

150
00:11:24,800 --> 00:11:28,320
There is if you study reality like

151
00:11:28,960 --> 00:11:30,960
Yeah, I mean an anatomy

152
00:11:30,960 --> 00:11:33,280
From an insight meditation point of view. It's

153
00:11:33,280 --> 00:11:40,560
Well, what you might say is that you know when you're dealing with with complex individuals who aren't meditating yet

154
00:11:41,200 --> 00:11:43,200
Then sure it can be incredibly

155
00:11:43,760 --> 00:11:48,400
Beneficial on a worldly level dealing with kids for example who've been abused

156
00:11:49,280 --> 00:11:55,920
No using art to help them to cope with their emotions is great. It's very helpful very useful

157
00:11:57,120 --> 00:12:00,320
So I was just thinking technically deep deep down

158
00:12:00,320 --> 00:12:06,400
It's got some problem to it, but I think you'd have to admit that

159
00:12:08,880 --> 00:12:14,720
On a you know even in the example of the Buddha image Buddha images are could be considered to be a type of art

160
00:12:15,840 --> 00:12:23,360
Or they're very closely related to I think what we're talking about is art and when you see a Buddha image when a meditator for example sees a Buddha image

161
00:12:23,360 --> 00:12:31,360
It can be incredibly inspiring and

162
00:12:32,960 --> 00:12:34,960
Maybe that's not such a good example

163
00:12:34,960 --> 00:12:37,760
Yeah, but I wouldn't you know, I wouldn't talk about how people

164
00:12:38,880 --> 00:12:46,560
React and art. It's just not as what art is how we like to his art. There's people doesn't matter. Really. It's just

165
00:12:50,560 --> 00:12:52,560
Yeah, it's just

166
00:12:52,560 --> 00:12:56,000
It's a marketing thing that you go to museum

167
00:12:59,200 --> 00:13:02,640
Be careful. There's artists out there who are going to tear you apart. Of course

168
00:13:03,360 --> 00:13:07,040
Everybody who knows history of art. They know that the

169
00:13:09,520 --> 00:13:11,520
Cut Church founded

170
00:13:12,240 --> 00:13:16,320
Most of the artists just they were doing they were making the art for money

171
00:13:16,320 --> 00:13:21,920
All right, so that was just a marketing thing

172
00:13:24,720 --> 00:13:29,840
Artists will tell you they don't do it for the money or they do it. No, let's stop. We're talking about Buddhist artists here

173
00:13:29,840 --> 00:13:34,640
We're asking the question of whether a Buddhist artist is actually

174
00:13:35,840 --> 00:13:39,840
Capable of teaching Buddhism through art basically

175
00:13:40,640 --> 00:13:42,640
No, I think

176
00:13:42,640 --> 00:13:44,640
Really

177
00:13:46,640 --> 00:13:52,560
I think it's a bad idea, but what about for kids? You know, what about kids who have been abused? How do you bring them to both?

178
00:13:53,120 --> 00:13:55,120
Maybe my cartoons, right?

179
00:13:55,680 --> 00:13:59,760
Well, what's what's better about cartoons than art? Why isn't art a valid

180
00:14:00,880 --> 00:14:05,440
Form of communication as well. Yeah, I notice the box downstairs. They're different

181
00:14:07,280 --> 00:14:09,280
Okay

182
00:14:09,280 --> 00:14:13,360
Especially for the kids themselves if they create art. Sometimes it helps them to

183
00:14:14,080 --> 00:14:18,000
Recognize with I mean, that's another thing. It can help you to

184
00:14:19,440 --> 00:14:24,560
I remember writing writing poetry as a means of

185
00:14:27,200 --> 00:14:31,200
Of understanding what was it, you know, understanding my feelings on something

186
00:14:31,840 --> 00:14:34,240
I just don't think that in the long term

187
00:14:34,240 --> 00:14:39,760
You know on the past to enlightenment. I think it is something that you have to give up eventually

188
00:14:40,400 --> 00:14:42,400
but in the beginning

189
00:14:42,480 --> 00:14:44,720
There's some you know, it can still be used

190
00:14:45,200 --> 00:14:51,440
Especially for people who are addicted to that so who are no like children who are attracted to that sort of thing

191
00:14:52,240 --> 00:14:57,120
and who would react to it would be engaged in like if you got kids to just sit down and and

192
00:14:58,720 --> 00:15:01,680
Practice meditation very difficult most of the time

193
00:15:01,680 --> 00:15:04,320
But if you have some pain to picture about

194
00:15:05,280 --> 00:15:08,400
Their feelings for example, or whenever our therapist do

195
00:15:09,360 --> 00:15:13,120
You probably haven't go a lot further to help them to understand their feelings

196
00:15:13,840 --> 00:15:15,840
So I think

197
00:15:17,440 --> 00:15:21,280
Are you see one thing when you are if you are kind of like

198
00:15:22,240 --> 00:15:28,640
The creating art that's a painting or drawing that a you kind of like your mind comes into state

199
00:15:28,640 --> 00:15:31,680
of sort of concentration

200
00:15:34,880 --> 00:15:36,880
And freedom

201
00:15:37,520 --> 00:15:40,720
So how are you it's a delusion?

202
00:15:41,760 --> 00:15:45,520
It is actually like is it bad? Does it positive?

203
00:15:49,200 --> 00:15:51,200
Sorry, is what possible

204
00:15:52,000 --> 00:15:54,000
Yeah, it's like

205
00:15:54,000 --> 00:16:01,200
People well, you can describe the state when you create it in your mind that creating if you are creating an art

206
00:16:04,000 --> 00:16:06,400
That you are in sort of the state of

207
00:16:07,520 --> 00:16:12,240
freedom like you can like express yourself, right?

208
00:16:14,320 --> 00:16:16,320
Well, yeah, what do you think?

209
00:16:16,640 --> 00:16:18,640
Pressing yourself is not the path

210
00:16:18,640 --> 00:16:26,800
This is a this is a fallacy of modern psychology that we don't agree with you think of think of the basic principle of quantum physics that

211
00:16:26,800 --> 00:16:28,800
Or of the highs and you know

212
00:16:28,800 --> 00:16:36,800
Let's take quantum physics or the Heisenberg uncertainty principle when you act you're already changing it when you express your feeling you're already

213
00:16:38,240 --> 00:16:46,160
Changing that feeling or you're you're creating that feeling how often how do we know that when we put together this piece of art based on our feelings

214
00:16:46,160 --> 00:16:49,920
We're not actually augmenting and reinforcing and

215
00:16:52,080 --> 00:16:54,400
You know actually even

216
00:16:54,400 --> 00:16:58,560
molding and shaping that that feeling how do we know that that's really

217
00:16:59,680 --> 00:17:04,560
We don't worry because it's not an exact investigation. It's it's art

218
00:17:05,360 --> 00:17:07,360
I mean the words as it already

219
00:17:08,160 --> 00:17:11,600
Yeah, I say the wrong question. It's like maybe not expression

220
00:17:11,600 --> 00:17:15,520
It's expression is something like you creating your mind

221
00:17:17,440 --> 00:17:19,440
But what I meant is

222
00:17:21,120 --> 00:17:28,240
That you are in sort of harmony. You know me like you feel like you're in harmony

223
00:17:29,760 --> 00:17:32,800
And what is it? Is it delusion? I mean, I heard

224
00:17:34,080 --> 00:17:40,240
I experienced it myself. I heard about other people like if you if you kind of like

225
00:17:40,240 --> 00:17:42,240
They call it

226
00:17:42,560 --> 00:17:44,560
they call it some sort of

227
00:17:46,640 --> 00:17:53,040
You know that you have to find a point in your life. It's gonna give you some sort of happiness or something like that

228
00:17:54,960 --> 00:17:56,960
Why are you thinking about it?

229
00:17:57,600 --> 00:18:02,160
You're talking about the harmony that comes from from art. Do people feel like they get something?

230
00:18:02,480 --> 00:18:07,920
Yeah, like the the process of making art is the out the the output

231
00:18:07,920 --> 00:18:09,920
It's not really

232
00:18:12,400 --> 00:18:14,400
The process

233
00:18:16,160 --> 00:18:21,920
Well, but see that's the thing you're as soon as you say that you're placing a value judgment on an experience

234
00:18:22,960 --> 00:18:30,160
if you know that to me that's the whole problem with art is art the word art to me is so loaded with value judgment and

235
00:18:30,160 --> 00:18:39,120
And you know subjectivity as opposed to seeing experiences for what they are that I can't help but feel like it's

236
00:18:39,680 --> 00:18:45,280
It's got its own got its inherent problems if you say it can be used as useful as a tool or fine

237
00:18:46,000 --> 00:18:51,840
Yeah, there can be benefit to it, but that's how it is karma is not always white or black sometimes. It's white and black

238
00:18:51,840 --> 00:19:00,560
So someone asks do you think we should all give up all forms of art and music? I mean

239
00:19:02,640 --> 00:19:08,800
On a level of one to ten art and music are not very high on the list of things that you have to give up

240
00:19:10,960 --> 00:19:18,080
But if you can power to you. It's a wonderful liberating experience. It's an empowering thing to be able to be free to not need such

241
00:19:18,080 --> 00:19:25,520
Crutches, you know most people even when they hear that that the kind of things that we're saying

242
00:19:27,120 --> 00:19:29,680
Become quite upset as a result. They're their

243
00:19:30,640 --> 00:19:32,880
Idea of what is important is offended

244
00:19:33,600 --> 00:19:37,840
The idea that music might be unbeneficial can make people

245
00:19:39,520 --> 00:19:41,520
Can put people quite quite

246
00:19:43,280 --> 00:19:45,280
Make people quite upset

247
00:19:45,280 --> 00:19:50,320
which of course is a sign that there is attachment there that there is a

248
00:19:52,960 --> 00:19:54,960
Eliminating factor there or

249
00:19:55,600 --> 00:19:57,600
crippling factor there

250
00:19:57,600 --> 00:20:00,960
Because when you when someone talks against the things that you like

251
00:20:01,760 --> 00:20:04,080
You become upset now for a person who doesn't

252
00:20:05,040 --> 00:20:07,040
Need music to make them happy

253
00:20:07,520 --> 00:20:12,320
They don't doesn't really matter what people say about music people can say it's greater. It's horrible. It doesn't bother them

254
00:20:12,320 --> 00:20:19,360
If if someone if you've if you've given up music and someone's his music is a horrible horrible thing

255
00:20:19,360 --> 00:20:21,360
It's the work of Satan

256
00:20:21,360 --> 00:20:25,200
That doesn't bother because you don't have anything to do with it anymore as with everything else

257
00:20:26,080 --> 00:20:32,000
So if you can do it. It's great when I finished my first meditation course one of the hardest things I did was give up music

258
00:20:32,400 --> 00:20:34,400
I had to stop playing the piano

259
00:20:34,400 --> 00:20:43,440
No more classical guitar. I had to give away my lead Zeppelin CD collection

260
00:20:44,800 --> 00:20:48,640
But I did it and it was empowering but

261
00:20:49,360 --> 00:20:53,520
Certainly it's you know ask ask more important things. Can you give up sex?

262
00:20:54,560 --> 00:20:59,840
Can you give up killing killing mosquitoes and insects? Can you give up drinking alcohol taking drugs?

263
00:21:00,400 --> 00:21:02,400
These are more important questions

264
00:21:02,400 --> 00:21:08,480
Like art and music are not such no go for it not a big deal

