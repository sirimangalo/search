1
00:00:00,000 --> 00:00:04,000
Hello, good afternoon.

2
00:00:04,000 --> 00:00:14,000
So this is going to be the beginning of restarting my Q&A videos.

3
00:00:14,000 --> 00:00:22,000
Today I'm going to be answering one of the questions from our meditation site.

4
00:00:22,000 --> 00:00:36,000
And the question today is, what is the validity of short meditation sessions?

5
00:00:36,000 --> 00:00:51,000
So the question here is, whether doing a 10 minute specific example of the answer will be try to be more general.

6
00:00:51,000 --> 00:01:04,000
Specifically, whether doing 10 minutes of meditation and then 4 minutes break in sequence,

7
00:01:04,000 --> 00:01:10,000
or 10 minutes meditation, 4 minutes break, 10 minutes meditation, 4 minutes break,

8
00:01:10,000 --> 00:01:13,000
whether that's acceptable.

9
00:01:13,000 --> 00:01:33,000
And so in general, the question becomes, how long should your meditation sessions be?

10
00:01:33,000 --> 00:01:50,000
The first thing to talk about then is, the importance of a meditation session at all, right?

11
00:01:50,000 --> 00:01:59,000
Meditation isn't something that involves minutes or hours or days.

12
00:01:59,000 --> 00:02:06,000
Meditation, as I've said, involves moments.

13
00:02:06,000 --> 00:02:11,000
It's because our experiences are momentary.

14
00:02:11,000 --> 00:02:17,000
Seeing lasts a moment, hearing lasts a moment.

15
00:02:17,000 --> 00:02:19,000
Pain is momentary.

16
00:02:19,000 --> 00:02:22,000
I mean, the experience of it is.

17
00:02:22,000 --> 00:02:30,000
As a result, and as you can fairly easily see with a little bit of meditation practice or even without it,

18
00:02:30,000 --> 00:02:34,000
our reactions to experiences are also momentary.

19
00:02:34,000 --> 00:02:36,000
It's only a moment.

20
00:02:36,000 --> 00:02:40,000
It only takes a moment to get angry about something.

21
00:02:40,000 --> 00:02:43,000
The anger appears quite quickly.

22
00:02:43,000 --> 00:02:48,000
You don't have to spend time saying, okay, let's get ready to be angry now.

23
00:02:48,000 --> 00:02:53,000
Here it comes, okay, altogether now, anger.

24
00:02:53,000 --> 00:02:59,000
The reaction is momentary.

25
00:02:59,000 --> 00:03:07,000
As a result, the habit forming occurs based on these moments and changing our habits.

26
00:03:07,000 --> 00:03:14,000
Learning even about our habits as a start involves the observation of these moments.

27
00:03:14,000 --> 00:03:19,000
Any moment that you can do that is great.

28
00:03:19,000 --> 00:03:29,000
For this reason, and also because this process of observation can take place, no matter what you're doing,

29
00:03:29,000 --> 00:03:38,000
a so-called session of meditation where you're actually sitting still isn't necessary at all.

30
00:03:38,000 --> 00:03:50,000
And if that's what you mean, then technically speaking, there's no reason to have any sort of roots about times of meditation sessions.

31
00:03:50,000 --> 00:03:56,000
So if you say 10 minutes of meditation and then 4 minutes of break, I would say no breaks.

32
00:03:56,000 --> 00:04:04,000
The only breaks should be when you're asleep because, well, you can't really very easily be mindful when you're sleeping.

33
00:04:04,000 --> 00:04:07,000
No breaks would be ideal.

34
00:04:07,000 --> 00:04:13,000
And there's really no excuse for a meditator to take a break in that sense.

35
00:04:13,000 --> 00:04:20,000
Now, for people living in the world, they're going to take breaks because they have other things to do.

36
00:04:20,000 --> 00:04:24,000
They want to learn something, they have to read a book.

37
00:04:24,000 --> 00:04:31,000
You're reading the book, you're not meditating, not consistently, not constantly.

38
00:04:31,000 --> 00:04:41,000
Because some of your time is engaged in other sorts of mental activity.

39
00:04:41,000 --> 00:04:43,000
But that's not really what's being asked here.

40
00:04:43,000 --> 00:04:44,000
It's an important point.

41
00:04:44,000 --> 00:04:51,000
It's important that we understand that and understand that all of our rules and guidelines and schedules are all convention.

42
00:04:51,000 --> 00:04:57,000
And they don't get at the underlying principle of meditation being about moments.

43
00:04:57,000 --> 00:05:04,000
You can meditate for hours and say, I did hours, meditation days, weeks, months, years, and get nothing out of it.

44
00:05:04,000 --> 00:05:11,000
Or never become enlightened for sure, simply because you never really meditated.

45
00:05:11,000 --> 00:05:18,000
You never really saw things clearly.

46
00:05:18,000 --> 00:05:26,000
So then practically speaking, what's the importance of, is there an importance of sitting still for 10 minutes?

47
00:05:26,000 --> 00:05:33,000
So the first thing I would say is that doing only sitting meditation all day is problematic.

48
00:05:33,000 --> 00:05:40,000
You know, sitting all day is something we become accustomed to in modern times, but it's not very healthy.

49
00:05:40,000 --> 00:05:44,000
It's not very natural, and it's not optimum.

50
00:05:44,000 --> 00:05:48,000
Optimal, it's not optimal.

51
00:05:48,000 --> 00:05:56,000
Walking meditation has, or walking movement has benefit psychologically as well, not just physically.

52
00:05:56,000 --> 00:06:03,000
There's an energy that's created through moving meditation, which is why we do walking meditation.

53
00:06:03,000 --> 00:06:09,000
So the first thing I would recommend is the first thing to switch would be to add 10 minutes of walking first.

54
00:06:09,000 --> 00:06:13,000
Do 10 minutes of walking, and then 10 minutes of sitting, and then four minutes break.

55
00:06:13,000 --> 00:06:14,000
Is that good?

56
00:06:14,000 --> 00:06:17,000
So that's the next question. Is that good?

57
00:06:17,000 --> 00:06:26,000
The other problem about short meditations, and this really, I think, gets to the heart of this question.

58
00:06:26,000 --> 00:06:28,000
They're great to start with.

59
00:06:28,000 --> 00:06:37,000
If anyone can do just starting to meditate, can do 10 minutes of walking and 10 minutes of sitting.

60
00:06:37,000 --> 00:06:38,000
Wow.

61
00:06:38,000 --> 00:06:39,000
Congratulations.

62
00:06:39,000 --> 00:06:41,000
You've done something very special.

63
00:06:41,000 --> 00:06:50,000
The Buddha said even one moment of meditation is very special, but 10 minutes of it, good for you.

64
00:06:50,000 --> 00:06:59,000
But once you've gone beyond that, there are limitations to such a practice of only 10 minutes.

65
00:06:59,000 --> 00:07:13,000
So mindfulness at its very core is about facing your problems, but not just problems facing everything, facing reality.

66
00:07:13,000 --> 00:07:22,000
Because we run away from it, we react to it, we forget it, and sati, which means to remember it.

67
00:07:22,000 --> 00:07:30,000
But sati has one of its important qualities is...

68
00:07:30,000 --> 00:07:35,000
I can't remember the word now.

69
00:07:35,000 --> 00:07:38,000
To face. It means to face.

70
00:07:38,000 --> 00:07:44,000
To face head-on.

71
00:07:44,000 --> 00:07:51,000
Your experience is rather than running away, being afraid of them, or reacting to them anyway.

72
00:07:51,000 --> 00:07:53,000
Just being with them.

73
00:07:53,000 --> 00:08:00,000
Patients is one way of describing the quality of an insight meditator.

74
00:08:00,000 --> 00:08:02,000
Someone who has...

75
00:08:02,000 --> 00:08:04,000
It's called...

76
00:08:04,000 --> 00:08:07,000
Analomika kanti.

77
00:08:07,000 --> 00:08:08,000
Analoma means...

78
00:08:08,000 --> 00:08:11,000
Analoma is when you finally get things.

79
00:08:11,000 --> 00:08:14,000
When you're going with the grain.

80
00:08:14,000 --> 00:08:16,000
Analoma means with the grain.

81
00:08:16,000 --> 00:08:20,000
Finally, you get it.

82
00:08:20,000 --> 00:08:24,000
And patience is the kanti means patience.

83
00:08:24,000 --> 00:08:28,000
Analomika kanti means patience that gets it.

84
00:08:28,000 --> 00:08:32,000
You're no longer impatient because you're together.

85
00:08:32,000 --> 00:08:35,000
You see things as they are.

86
00:08:35,000 --> 00:08:36,000
And you see things as they are.

87
00:08:36,000 --> 00:08:38,000
There's no stress in the basement.

88
00:08:38,000 --> 00:08:40,000
Ten minutes of meditation.

89
00:08:40,000 --> 00:08:46,000
First of all, doesn't let the average meditator face their problems.

90
00:08:46,000 --> 00:08:50,000
Face reality. After ten minutes.

91
00:08:50,000 --> 00:08:52,000
Things are just starting to get interesting.

92
00:08:52,000 --> 00:08:58,000
To put it to the colloquial terms.

93
00:08:58,000 --> 00:09:04,000
You're just starting to observe challenging mind states.

94
00:09:04,000 --> 00:09:06,000
The boredom would be a good one.

95
00:09:06,000 --> 00:09:10,000
Pain will start to come up.

96
00:09:10,000 --> 00:09:14,000
Once desires will come up.

97
00:09:14,000 --> 00:09:19,000
Ten minutes is fine to help you understand reality.

98
00:09:19,000 --> 00:09:21,000
Because reality is all there.

99
00:09:21,000 --> 00:09:23,000
But for the average meditator.

100
00:09:23,000 --> 00:09:26,000
Everything is all there.

101
00:09:26,000 --> 00:09:29,000
Mind states are still too subtle.

102
00:09:29,000 --> 00:09:32,000
So for an average meditator,

103
00:09:32,000 --> 00:09:34,000
conditions are going to build.

104
00:09:34,000 --> 00:09:37,000
And after an hour, it can get quite difficult.

105
00:09:37,000 --> 00:09:41,000
You'll have to face quite difficult conditions.

106
00:09:41,000 --> 00:09:44,000
Consider an hour to be about good.

107
00:09:44,000 --> 00:09:46,000
You don't have to go beyond an hour.

108
00:09:46,000 --> 00:09:48,000
Sometimes meditators do.

109
00:09:48,000 --> 00:09:52,000
But it's just an artificial limit that we tend to set.

110
00:09:52,000 --> 00:09:56,000
We say, well, new maximum one hour walking whenever sitting.

111
00:09:56,000 --> 00:09:59,000
That's what we try to get our meditators to do.

112
00:09:59,000 --> 00:10:04,000
Again, that's just no compelling reason to stick to one hour beyond.

113
00:10:04,000 --> 00:10:07,000
It seems about right.

114
00:10:07,000 --> 00:10:12,000
And it's convenient because you can count hours and so on.

115
00:10:12,000 --> 00:10:16,000
But most important is it allows you to face your problems.

116
00:10:16,000 --> 00:10:18,000
It allows you to face your problems.

117
00:10:18,000 --> 00:10:21,000
And it allows subtle problems to become more evident.

118
00:10:21,000 --> 00:10:22,000
Because they'll build.

119
00:10:22,000 --> 00:10:24,000
If you're not mindful of something,

120
00:10:24,000 --> 00:10:27,000
suppose during those ten minutes something is happening,

121
00:10:27,000 --> 00:10:29,000
like you're getting bored.

122
00:10:29,000 --> 00:10:30,000
But it's too subtle.

123
00:10:30,000 --> 00:10:32,000
And you don't catch it.

124
00:10:32,000 --> 00:10:36,000
You're not mindful of it because you're not aware of it.

125
00:10:36,000 --> 00:10:38,000
Well, after ten minutes, if you continue sitting,

126
00:10:38,000 --> 00:10:40,000
it'll become more apparent.

127
00:10:40,000 --> 00:10:43,000
It'll get more severe.

128
00:10:43,000 --> 00:10:45,000
And you'll be able to note it.

129
00:10:45,000 --> 00:10:48,000
So it's profitable in that way.

130
00:10:48,000 --> 00:10:52,000
Practically speaking longer meditation sessions are more profitable.

131
00:10:52,000 --> 00:10:56,000
That being said, there's another related question.

132
00:10:56,000 --> 00:11:02,000
And it's the question of whether I should do one big meditation session once a day.

133
00:11:02,000 --> 00:11:08,000
Or, for example, split it in half and do one in the morning and one in the evening.

134
00:11:08,000 --> 00:11:11,000
And I usually tell meditators, again,

135
00:11:11,000 --> 00:11:13,000
keeping in mind this isn't magic.

136
00:11:13,000 --> 00:11:15,000
I don't have any rulebook or I say,

137
00:11:15,000 --> 00:11:16,000
I have you do this.

138
00:11:16,000 --> 00:11:17,000
You'll become enlightened.

139
00:11:17,000 --> 00:11:19,000
If you do that, you won't become enlightened.

140
00:11:19,000 --> 00:11:24,000
It's important that we rise above magical thinking that somehow

141
00:11:24,000 --> 00:11:28,000
there's some magic way formula that I can use that's going to get me enlightened.

142
00:11:28,000 --> 00:11:30,000
We have to be smarter than that.

143
00:11:30,000 --> 00:11:33,000
Meditation is like war.

144
00:11:33,000 --> 00:11:37,000
It's like a battlefield.

145
00:11:37,000 --> 00:11:39,000
And you've done all this training.

146
00:11:39,000 --> 00:11:41,000
I don't know how they teach soldiers,

147
00:11:41,000 --> 00:11:43,000
but I would imagine they would tell them you're doing all this training

148
00:11:43,000 --> 00:11:45,000
and it's not coming crap when you're out in the field

149
00:11:45,000 --> 00:11:47,000
because everything's going to go crazy.

150
00:11:47,000 --> 00:11:48,000
I imagine that's how it is.

151
00:11:48,000 --> 00:11:53,000
I can imagine a battlefield being just all your training goes out the window

152
00:11:53,000 --> 00:11:57,000
and you try to use what you can and use it as best you can.

153
00:11:57,000 --> 00:12:00,000
It's kind of how meditation works.

154
00:12:00,000 --> 00:12:01,000
It's messy.

155
00:12:01,000 --> 00:12:06,000
And you try to do your best to learn about your mind and learn about reality

156
00:12:06,000 --> 00:12:08,000
using the tools that you've been given.

157
00:12:08,000 --> 00:12:12,000
And the one thing you should never mess with is the technique.

158
00:12:12,000 --> 00:12:15,000
But the technique is you can adapt to any situation

159
00:12:15,000 --> 00:12:20,000
and it doesn't involve minutes or hours or days.

160
00:12:20,000 --> 00:12:26,000
But so this question of whether you should do two shorter meditations,

161
00:12:26,000 --> 00:12:30,000
I recommend more because the other question here,

162
00:12:30,000 --> 00:12:35,000
or the other aspect of this question is the idea of continuity.

163
00:12:35,000 --> 00:12:40,000
And the Buddha talked about something or it's at least described

164
00:12:40,000 --> 00:12:43,000
to the Buddha as talking about something called satanchek.

165
00:12:43,000 --> 00:12:54,000
Satanchek Giri Oasena, which means staying in the same condition continuously,

166
00:12:54,000 --> 00:12:57,000
not changing in your condition.

167
00:12:57,000 --> 00:13:12,000
Again, to allow this sort of the accumulation of difficulty to really test you.

168
00:13:12,000 --> 00:13:16,000
It's easy to sit ten minutes and then, okay, because I can take a break.

169
00:13:16,000 --> 00:13:19,000
But what if you have to sit for an hour and you have to really bear with things.

170
00:13:19,000 --> 00:13:29,000
To be patient with the ability, the potential to cultivate patience is higher.

171
00:13:29,000 --> 00:13:34,000
And so, doing shorter meditations is not better because it's shorter.

172
00:13:34,000 --> 00:13:37,000
It's better because it's more often, it's more frequent.

173
00:13:37,000 --> 00:13:42,000
And it's more likely to encourage you to be mindful when you're not meditating.

174
00:13:42,000 --> 00:13:48,000
And if you do one meditation session, even a long one, it's good, good that you did it.

175
00:13:48,000 --> 00:13:51,000
But then there's 24 hours where you're not doing any formal meditation.

176
00:13:51,000 --> 00:13:57,000
And so, your ability to be mindful during the time you're not meditating.

177
00:13:57,000 --> 00:14:00,000
I would say, inhibited, reduced.

178
00:14:00,000 --> 00:14:03,000
Whereas if you do two a day, you're thinking more about meditation.

179
00:14:03,000 --> 00:14:05,000
It's more continuous.

180
00:14:05,000 --> 00:14:11,000
And so, the 12 hours between each or however many hours between each session,

181
00:14:11,000 --> 00:14:13,000
you're more likely to be mindful.

182
00:14:13,000 --> 00:14:22,000
So, doing more sessions is good, but doing lots of little sessions gets problematic for the reasons.

183
00:14:22,000 --> 00:14:24,000
So, there you go.

184
00:14:24,000 --> 00:14:25,000
Shorter videos now.

185
00:14:25,000 --> 00:14:26,000
That's the idea.

186
00:14:26,000 --> 00:14:29,000
One question, one answer per video.

187
00:14:29,000 --> 00:14:37,000
If you want to ask questions, you can go to our meditation site, meditation.ceremungalow.org.

188
00:14:37,000 --> 00:14:43,000
There's not going to be, it's not going to be in the description of this video or not right away.

189
00:14:43,000 --> 00:14:45,000
I don't think.

190
00:14:45,000 --> 00:14:49,000
But you can find it on my channel, I think.

191
00:14:49,000 --> 00:14:51,000
I think.

192
00:14:51,000 --> 00:14:54,000
And if you think, I don't know.

193
00:14:54,000 --> 00:15:00,000
If you can't find it, well, that's part of the part of your spiritual journey is to find things like this.

194
00:15:00,000 --> 00:15:02,000
The teacher doesn't seek out the students.

195
00:15:02,000 --> 00:15:09,000
So, good luck.

196
00:15:09,000 --> 00:15:09,000
I wish you all the best.

197
00:15:09,000 --> 00:15:10,000
74 viewers.

198
00:15:10,000 --> 00:15:11,000
Wonderful.

199
00:15:11,000 --> 00:15:20,000
Have a good day, everyone.

