1
00:00:00,000 --> 00:00:04,000
How long will it take before my Evo start shrinking for meditation?

2
00:00:10,000 --> 00:00:12,000
How long will it take?

3
00:00:17,000 --> 00:00:18,000
Evo's really...

4
00:00:19,000 --> 00:00:20,000
What?

5
00:00:20,000 --> 00:00:23,000
Some say there are certain amount of time that's supposed to.

6
00:00:23,000 --> 00:00:25,000
It's supposed to take.

7
00:00:25,000 --> 00:00:30,000
One week and three days.

8
00:00:34,000 --> 00:00:38,000
No, but Evo is a difficult one.

9
00:00:38,000 --> 00:00:40,000
Evo is difficult.

10
00:00:40,000 --> 00:00:43,000
You have to be repeatedly crushed.

11
00:00:43,000 --> 00:00:45,000
You have to crush...

12
00:00:45,000 --> 00:00:48,000
Evo has to be crushed by reality.

13
00:00:48,000 --> 00:00:53,000
Reality has to get in the way of the Evo.

14
00:00:53,000 --> 00:00:58,000
It's to show the Evo it's wrong.

15
00:01:00,000 --> 00:01:04,000
That's with everything and it comes gradually.

16
00:01:04,000 --> 00:01:07,000
It should come from the very beginning of meditation.

17
00:01:07,000 --> 00:01:11,000
If you're practicing Vipassana, it should come gradually.

18
00:01:11,000 --> 00:01:16,000
Every moment that you practice Vipassana, it should be attacking the Evo.

19
00:01:16,000 --> 00:01:26,000
But there are moments of incredible revelation where you just realize what an equitistical jerk you are.

20
00:01:26,000 --> 00:01:33,000
Are you realized how useless you are?

21
00:01:33,000 --> 00:01:40,000
This is realizing how devoid of any value you are.

22
00:01:40,000 --> 00:01:42,000
That's horrible.

23
00:01:42,000 --> 00:01:48,000
It's the most liberating thing is to realize that you're useless, that you're valueless, that you're meaningless.

24
00:01:48,000 --> 00:01:49,000
It's all liberating.

25
00:01:49,000 --> 00:01:58,000
Because you have no stresses, you have no expectations put on you.

26
00:02:00,000 --> 00:02:06,000
When you're comfortable with being useless with being valueless,

27
00:02:06,000 --> 00:02:08,000
that's the liberation of the Evo.

28
00:02:08,000 --> 00:02:10,000
It's a part of it.

29
00:02:10,000 --> 00:02:14,000
You have to come to this realization of how useless you are.

30
00:02:14,000 --> 00:02:18,000
Realizing that you're really you have no purpose.

31
00:02:18,000 --> 00:02:21,000
It's true though.

32
00:02:23,000 --> 00:02:27,000
It's wonderful to be useless, to be valueless, to be meaningful.

33
00:02:30,000 --> 00:02:34,000
I don't know, maybe useless is going too hard, useless isn't it?

34
00:02:34,000 --> 00:02:36,000
Because we are useless to ours.

35
00:02:36,000 --> 00:02:42,000
You're useful to ourselves, useless to each other, useless is maybe not the right word.

36
00:02:44,000 --> 00:02:47,000
Pointless, valueless, worthless.

37
00:02:47,000 --> 00:02:50,000
That was it, worthless is the word.

38
00:02:51,000 --> 00:02:53,000
Intrinsically worthless.

39
00:02:53,000 --> 00:02:57,000
You realize that you let go, you have no expectations.

40
00:02:57,000 --> 00:03:00,000
You can act with it, expectations without worrying that

41
00:03:00,000 --> 00:03:03,000
are you really going to help people?

42
00:03:03,000 --> 00:03:06,000
Are your activities really going to be right?

43
00:03:06,000 --> 00:03:08,000
Really going to be any good?

44
00:03:09,000 --> 00:03:12,000
What does it mean to them to be any good?

45
00:03:14,000 --> 00:03:16,000
You just see that they are what they are.

46
00:03:16,000 --> 00:03:18,000
And then you're really useful.

47
00:03:18,000 --> 00:03:20,000
You're real help to yourself and others.

48
00:03:20,000 --> 00:03:25,000
Acting in such a way that brings about benefit for yourself and for others.

49
00:03:25,000 --> 00:03:35,000
Definitely a version sets in and you look at why you don't want something.

50
00:03:35,000 --> 00:03:38,000
When you see I, you see the ego.

51
00:03:38,000 --> 00:03:41,000
It's usually a good one.

52
00:03:41,000 --> 00:03:44,000
Every now and then you see why you suffer.

53
00:03:44,000 --> 00:03:54,000
Over and over again.

