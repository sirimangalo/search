1
00:00:00,000 --> 00:00:01,000
Go ahead.

2
00:00:01,000 --> 00:00:08,880
Gamma asks, are there alternate routes to enlightenment in your tradition?

3
00:00:08,880 --> 00:00:13,560
I've heard that the contemplation of the mind and body feelings are practicing Jhana

4
00:00:13,560 --> 00:00:19,760
can lead one to cessation as quote unquote separate paths, and that each person should

5
00:00:19,760 --> 00:00:22,480
practice according to his or her temperament.

6
00:00:22,480 --> 00:00:23,960
Is this true?

7
00:00:23,960 --> 00:00:28,000
Without a teacher, how can I know which way is the best for me?

8
00:00:28,000 --> 00:00:45,680
To answer the last question first, without a teacher, it's very difficult to know which

9
00:00:45,680 --> 00:00:52,880
way is the best for you.

10
00:00:52,880 --> 00:01:00,200
Might have a certain feeling, and you might have a certain tendency, but you might still

11
00:01:00,200 --> 00:01:09,920
have a lot of defilements, and even though if there's a little knowledge about which

12
00:01:09,920 --> 00:01:18,640
would be good, which way would be the good for you, your defilements could just be so strong

13
00:01:18,640 --> 00:01:28,880
that you can't see the right way properly, and that you assume something is the right

14
00:01:28,880 --> 00:01:38,200
way for you, which is in reality not, because it's just more convenient or easier.

15
00:01:38,200 --> 00:01:45,520
So if many people without teacher think that, oh, this is easy, I can do that easy,

16
00:01:45,520 --> 00:01:48,040
so this must be the right way for me.

17
00:01:48,040 --> 00:01:53,760
And that is mistaken most of the times.

18
00:01:53,760 --> 00:02:01,840
In general, you can say, when it's too easy, then it's first not challenging enough for

19
00:02:01,840 --> 00:02:09,600
you for your mind, and it doesn't develop, no?

20
00:02:09,600 --> 00:02:19,400
You don't develop as much, and you might just give into your defilements, then you might,

21
00:02:19,400 --> 00:02:28,400
oh, this is very easy, great, that's my way, and you just do that, and don't look any further,

22
00:02:28,400 --> 00:02:42,120
and might miss the best, then, so that's the way that there are separate paths.

23
00:02:42,120 --> 00:02:51,360
Yeah, to the separate path, routes to enlightenment, you call them in our tradition, in

24
00:02:51,360 --> 00:03:00,760
our tradition, there's only one, and that is the four foundations of mindfulness, which

25
00:03:00,760 --> 00:03:09,800
Bante mentioned, but I can repeat them, body, feelings, mind, and dama, or also called

26
00:03:09,800 --> 00:03:27,040
mind objects, and the contemplation or the meditation to gain insight about the three characteristics

27
00:03:27,040 --> 00:03:44,120
of everything, the impermanence, unsatisfactoriness and uncontrollability of everything.

28
00:03:44,120 --> 00:03:53,640
This is the route to enlightenment in our tradition.

29
00:03:53,640 --> 00:03:59,320
There are, as you mentioned, separate path, they are now separate, but they weren't so

30
00:03:59,320 --> 00:04:03,240
much separate in the Buddha's time.

31
00:04:03,240 --> 00:04:09,920
Now we have Samatam, meditation, and Vipasana, meditation.

32
00:04:09,920 --> 00:04:19,400
One is more developing calm and peace and concentration, and the other is developing

33
00:04:19,400 --> 00:04:25,200
insight.

34
00:04:25,200 --> 00:04:30,880
In fact, you need to have concentration and mindfulness.

35
00:04:30,880 --> 00:04:41,960
You can't really separate the path, but actually, when you practice Vipasana meditation,

36
00:04:41,960 --> 00:04:48,960
you develop concentration, so you don't need to go separate path.

37
00:04:48,960 --> 00:05:01,800
You can just do Vipasana meditation and develop enough concentration, and at the same time

38
00:05:01,800 --> 00:05:11,680
enough mindfulness to get to your goal, to go the route to enlightenment.

39
00:05:11,680 --> 00:05:19,000
I mean, the whole character type thing is only for Samatam meditation, I think I mentioned

40
00:05:19,000 --> 00:05:22,920
that in an earlier talk.

41
00:05:22,920 --> 00:05:30,120
People always talk about character types, but in the practice of insight, meditation,

42
00:05:30,120 --> 00:05:34,400
character types are all encompassed by the four foundations of mindfulness.

43
00:05:34,400 --> 00:05:42,400
To give you a little bit of the actual theoretical structure that we have, is that there

44
00:05:42,400 --> 00:05:48,040
are two paths, and one of them is the practice of Samatam first, and then Vipasana.

45
00:05:48,040 --> 00:05:51,920
The other one is the practice of just Vipasana without Samatam.

46
00:05:51,920 --> 00:05:57,400
But the only reason they're separated in that way is because one of them does many things

47
00:05:57,400 --> 00:06:01,440
that are not core or essential to the Buddha's teaching.

48
00:06:01,440 --> 00:06:06,200
For example, remembering past lives, reading people's minds, flying through the air,

49
00:06:06,200 --> 00:06:12,840
these benefits that come only from a person who develops, what we call, Samatam meditation.

50
00:06:12,840 --> 00:06:18,040
The difference between Samatam Vipasana is not the qualities of mind, the meaning of the

51
00:06:18,040 --> 00:06:25,600
word, tranquility meditation is not that it's a tranquil practice, it's that the end

52
00:06:25,600 --> 00:06:28,480
goal is tranquility.

53
00:06:28,480 --> 00:06:33,600
So when someone was mentioning earlier of this practice of watching the heart or breathing

54
00:06:33,600 --> 00:06:42,120
according to the heart and getting into a state of great stability of mind and concentration,

55
00:06:42,120 --> 00:06:44,440
well that's the goal of that practice.

56
00:06:44,440 --> 00:06:48,880
There are many, many, many, there are an infinite number of practices you can dream up

57
00:06:48,880 --> 00:06:50,520
that lead to that.

58
00:06:50,520 --> 00:06:54,560
If you look at my videos on how to meditate for kids, we've only gotten that far.

59
00:06:54,560 --> 00:06:59,800
I've only gotten through teaching kids how to watch colors and if you watch a color,

60
00:06:59,800 --> 00:07:05,600
it's just one example of how your mind can become focused.

61
00:07:05,600 --> 00:07:13,200
So why it's called tranquility meditation is because it can't lead to insight, that focusing

62
00:07:13,200 --> 00:07:16,560
on the color, you can focus on it as much as you like, you'll never become enlightened

63
00:07:16,560 --> 00:07:20,720
until you start to look at it as impermanence, suffering and non-self.

64
00:07:20,720 --> 00:07:28,800
But so what we call insight meditation is not, not for the reason that it has only insight

65
00:07:28,800 --> 00:07:33,120
that it's not semi-dominant, it's not tranquility meditation because it has no tranquility,

66
00:07:33,120 --> 00:07:38,440
but because the goal of it is insight and the result of it is insight into reality.

67
00:07:38,440 --> 00:07:41,960
So it's focusing on the reality of the experience.

68
00:07:41,960 --> 00:07:48,160
If you're experiencing a color that's not blue or yellow, that's seeing, it's the experience

69
00:07:48,160 --> 00:07:54,560
of light touching the eye and the blue only arises in the mind as a concept, as a recognition

70
00:07:54,560 --> 00:08:00,840
because there's no difference qualitatively between blue, red and yellow, there are just

71
00:08:00,840 --> 00:08:09,760
different waves of the same light in the same spectrum.

72
00:08:09,760 --> 00:08:16,720
So it's really how it comes down to technique rather than path, there is only one path

73
00:08:16,720 --> 00:08:22,760
to enlightenment and that is, as Paulini said, it's the practice of insight meditation

74
00:08:22,760 --> 00:08:24,960
in line with the four foundations of mindfulness.

75
00:08:24,960 --> 00:08:31,160
You practice mindfulness of some part of your experience, whether it be physical or mental

76
00:08:31,160 --> 00:08:34,080
or both physical and mental.

77
00:08:34,080 --> 00:08:41,960
And as a result, you develop insight into impermanence, this word that I'm suffering, I'm

78
00:08:41,960 --> 00:08:45,280
just going to impermanence, suffering and uncontrollability.

79
00:08:45,280 --> 00:08:51,520
And suffering really does mean that you can't, it's unsatisfying and unsatisfiable.

80
00:08:51,520 --> 00:08:57,440
And non-self means not only that it's non-self, but basically that it's not controllable.

81
00:08:57,440 --> 00:09:02,440
And with the clear realization of any one of these three impermanence, suffering or non-self,

82
00:09:02,440 --> 00:09:07,040
any tongue, dukang or anata, the mind enters into cessation, it gives up.

83
00:09:07,040 --> 00:09:15,080
It says, now I see everything is not worth clinging to and it gives up its clinging and

84
00:09:15,080 --> 00:09:21,440
becomes, or enters into a state of freedom, at least temporarily.

85
00:09:21,440 --> 00:09:23,840
But there are many techniques that you can use to get there.

86
00:09:23,840 --> 00:09:29,520
So if you want to develop strong concentration first before developing insight, you can do

87
00:09:29,520 --> 00:09:30,520
that.

88
00:09:30,520 --> 00:09:31,520
Look at the color of these.

89
00:09:31,520 --> 00:09:33,840
This is a practical example.

90
00:09:33,840 --> 00:09:39,240
Problem is that certain, certain of these techniques are not suitable for certain individuals.

91
00:09:39,240 --> 00:09:44,360
If you develop loving kindness, for example, but you're a person with great lust.

92
00:09:44,360 --> 00:09:51,200
So you start, you know, pick an attractive person and start sending a love for them.

93
00:09:51,200 --> 00:09:55,320
Well, it can be quite distracting because you're going back and forth between lust and

94
00:09:55,320 --> 00:09:56,960
are love and lust.

95
00:09:56,960 --> 00:10:02,280
If you're a person who is full of anger, then you shouldn't practice mindfulness of

96
00:10:02,280 --> 00:10:03,280
load sameness.

97
00:10:03,280 --> 00:10:07,000
So some people will watch their body and in order to get rid of lust, they will look

98
00:10:07,000 --> 00:10:14,080
at the parts of the body, the hair, the fur, the body hair, the nails, the teeth, the

99
00:10:14,080 --> 00:10:17,840
skin, the flesh, the bones and so on.

100
00:10:17,840 --> 00:10:21,640
All of the parts of the body, they'll focus on them one by one to see the truth of them

101
00:10:21,640 --> 00:10:23,320
and get rid of the lust.

102
00:10:23,320 --> 00:10:25,320
If you're an angry person, this doesn't do you any good.

103
00:10:25,320 --> 00:10:29,600
It in fact is harmful because it makes you repulsed by your own body and you want to kill

104
00:10:29,600 --> 00:10:30,600
yourself.

105
00:10:30,600 --> 00:10:32,680
Apparently this, this sort of thing happens.

106
00:10:32,680 --> 00:10:37,520
So that's where the different quote unquote paths come, but they're just different techniques

107
00:10:37,520 --> 00:10:43,760
suitable for different people because in order to gain that state of calm, you need to

108
00:10:43,760 --> 00:10:46,880
counteract whatever it is that is making you distracted.

109
00:10:46,880 --> 00:10:50,720
If you're an agitated person, you should just sitting or lying meditation.

110
00:10:50,720 --> 00:10:59,120
If you're a lazy person, you should do walking or standing meditation, for example.

111
00:10:59,120 --> 00:11:05,440
But when it comes to actually developing insight and understanding, there is only one, there's

112
00:11:05,440 --> 00:11:13,960
really only one object and one technique, the object is reality, the technique is seeing

113
00:11:13,960 --> 00:11:25,680
reality clearly, how could there possibly be, so people often, we have this postmodern mindset

114
00:11:25,680 --> 00:11:27,920
and people have been calling me out for postmodernism.

115
00:11:27,920 --> 00:11:29,600
Look it up on Wikipedia.

116
00:11:29,600 --> 00:11:36,400
The base definition of postmodernism is relative truth, that truth is not objective, that

117
00:11:36,400 --> 00:11:39,080
there is no one way or one truth or one.

118
00:11:39,080 --> 00:11:44,560
And this doesn't jive with Buddhism and there's nothing intrinsically more logical about

119
00:11:44,560 --> 00:11:47,320
that than about there being an absolute truth.

120
00:11:47,320 --> 00:11:51,600
The problem is we don't know the ultimate truth, so we've come to start to say, well,

121
00:11:51,600 --> 00:11:52,600
maybe truth is subjective.

122
00:11:52,600 --> 00:11:55,320
You're away is your way, my way is my way.

123
00:11:55,320 --> 00:12:01,120
But what we mean by the one path is not that you have to become a monk or you have

124
00:12:01,120 --> 00:12:07,400
to practice our tradition even or you have to say what you're doing is we passana or

125
00:12:07,400 --> 00:12:12,200
it's samadha or it's this or it's that or it's jana, no, you have to practice a certain

126
00:12:12,200 --> 00:12:14,920
type of jana.

127
00:12:14,920 --> 00:12:18,960
What it means is you have to see reality for what it is, reality is your object seeing

128
00:12:18,960 --> 00:12:23,560
it for what it is, is the technique, there's no other technique, there's no other way.

129
00:12:23,560 --> 00:12:26,200
If you don't see reality, this is the Buddhist theory.

130
00:12:26,200 --> 00:12:28,800
If you don't see reality for what it is, you'll never become enlightened.

131
00:12:28,800 --> 00:12:32,760
The only way to become enlightened is to see reality for what it is.

132
00:12:32,760 --> 00:12:37,000
The Buddha said, the truth of suffering is to be understood thoroughly.

133
00:12:37,000 --> 00:12:40,960
If you understand the truth of suffering thoroughly, you'll give up the cause of suffering.

134
00:12:40,960 --> 00:12:43,840
Once you give up the cause of suffering, there's the cessation of suffering.

135
00:12:43,840 --> 00:12:48,560
This is the path to the cessation of suffering.

136
00:12:48,560 --> 00:13:01,560
You're okay?

