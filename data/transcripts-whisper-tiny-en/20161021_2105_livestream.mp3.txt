The Buddha said that patience can't be put among the Bodhika patients is the greatest form
of torture, maybe not torture, but a sadicism or tapa means heat, but it was a word
that was used for self-mortification or discipline, maybe discipline is the right word.
Patience is the highest form of discipline, that we have all these other ways of forcing ourselves
or tricking ourselves into bearing with unpleasantness, but the things surpasses true
patience. And this I think really what you feel directly when you meditate, maybe this
is one of the, for all those people that they're looking for, the benefits of meditation
and find themselves in having a hard time figuring out the bad, the actual benefits
of meditation. I think you can see this quite clearly and quite quickly is the patience
that which you were unable to bear previously, you're now able to bear. I think the
problem is when we look at benefits of meditation where we're often looking in the wrong
place or expected.
