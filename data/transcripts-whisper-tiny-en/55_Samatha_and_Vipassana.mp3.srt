1
00:00:00,000 --> 00:00:07,040
other types of meditation, kune, samata and vipassana.

2
00:00:08,960 --> 00:00:16,240
Question, I started with samata meditation as I read a lot that you have to call me a mind before

3
00:00:16,240 --> 00:00:24,480
starting to practice vipassana. What is your interpretation of the differences between these two

4
00:00:24,480 --> 00:00:37,040
and which should be practiced first? Answer, samata means tranquility, vipassana means seeing clearly.

5
00:00:38,080 --> 00:00:44,240
The Buddha used these terms together and individually as qualities of mind.

6
00:00:45,040 --> 00:00:51,600
When you talk about samata meditation as something separate from vipassana meditation,

7
00:00:51,600 --> 00:01:00,400
you are taking the discussion into a post-canonical realm. You are taking it out of the realm of what

8
00:01:00,400 --> 00:01:09,840
the Buddha actually said and into the realm of interpretation. I follow the orthodox interpretation

9
00:01:09,840 --> 00:01:18,160
that does talk about samata meditation and vipassana meditation, so I do not mind talking about this.

10
00:01:18,160 --> 00:01:27,600
For those people who advise to practice coming your mind first, samata meditation and then practicing

11
00:01:27,600 --> 00:01:37,440
to see clearly after vipassana meditation, this means first entering into a trance wherein you can see

12
00:01:37,440 --> 00:01:47,520
one thing very clearly. This is only possible if that one thing is a concept. Since concepts

13
00:01:48,080 --> 00:01:56,800
are lasting entities, the mind can focus on a slang. Understanding the distinction between

14
00:01:56,800 --> 00:02:06,000
reality and concept is important. It allows us to determine whether a meditation practice will

15
00:02:06,000 --> 00:02:15,600
lead to a trance state or a clear understanding of reality. Focusing on concepts will not help you

16
00:02:15,600 --> 00:02:22,800
understand reality because you are focusing on something you have created in your mind.

17
00:02:23,680 --> 00:02:30,480
It is not real. It does not have any of the qualities of ultimate reality.

18
00:02:30,480 --> 00:02:39,440
It is not the same as watching your stomach rise and fall because when you watch your stomach rise

19
00:02:39,440 --> 00:02:48,160
and fall, it changes constantly. The movements of the stomach are impermanent on satisfying

20
00:02:48,160 --> 00:02:57,520
and uncontrollable. Watching the stomach helps you to see reality as it is and that is difficult.

21
00:02:57,520 --> 00:03:07,200
It is much easier and pleasant to create a concept in your mind. For example, you can close your eyes

22
00:03:07,760 --> 00:03:16,400
and utter your third eye in the center of your forehead. Imagine a collar and you say to yourself

23
00:03:16,400 --> 00:03:26,480
blue, blue, or red, red, or white, white. Just focusing on the collar you have imagined.

24
00:03:27,760 --> 00:03:33,680
Once you can fix your mind, only on the collar, thinking of nothing else,

25
00:03:34,880 --> 00:03:40,560
that is considered to be an attainment of the fruit of samata meditation.

26
00:03:40,560 --> 00:03:50,400
Many religious traditions, especially in India, make use of this sort of practice for the purpose

27
00:03:50,400 --> 00:03:59,120
of gaining spiritual powers like remembering past lives, reading people's minds, seeing

28
00:03:59,120 --> 00:04:09,040
heaven and hell, seeing the future, and leaving your body. To make use of samata meditation

29
00:04:09,040 --> 00:04:16,720
for the subsequent development of Vipassana, you simply apply the principles of

30
00:04:16,720 --> 00:04:26,640
Satipatana practice to the samata experiences. For example, you can note the focused mind

31
00:04:26,640 --> 00:04:40,240
as focused, focused. If the experience is pleasurable, you can note happy, happy. If you feel calm,

32
00:04:40,240 --> 00:04:49,760
calm, calm. If the experience involves lights or colors, you note seeing, seeing,

33
00:04:49,760 --> 00:05:00,000
etc. and other results of the mindful interaction, the experience will change from seeing the stable,

34
00:05:00,640 --> 00:05:08,720
satisfying and controllable concept to the impermanent, unsatisfying and uncontrollable reality.

35
00:05:10,320 --> 00:05:15,680
Because samata meditation involves the cultivation of poor,

36
00:05:15,680 --> 00:05:21,440
fundamental faculties, when you switch to Vipassana meditation,

37
00:05:22,480 --> 00:05:28,560
your mind is readily able to see clearly the nature of reality.

38
00:05:30,000 --> 00:05:35,440
For someone who does not take the time to practice samata meditation first,

39
00:05:36,160 --> 00:05:43,920
Vipassana meditation can be a challenge because the mind does not have the prior training

40
00:05:43,920 --> 00:05:51,360
or on more pleasurable objects. You have to face pain in the body,

41
00:05:52,240 --> 00:06:00,720
distractions in the mind and all kinds of emotions without any of the peace and calm of samata

42
00:06:00,720 --> 00:06:10,880
meditation. For the most part, this means only that the meditation practice is less pleasurable,

43
00:06:10,880 --> 00:06:18,560
not slower or less effective. In fact, by starting with Vipassana meditation,

44
00:06:19,440 --> 00:06:27,120
one avoids the potential pitfalls of becoming complacent or considered about,

45
00:06:27,120 --> 00:06:36,080
once samata-based meditative attainment, as their training of the mind begins with ultimate

46
00:06:36,080 --> 00:06:44,160
reality directly. The difference between the Vipassana meditation and samata meditation is quite

47
00:06:44,160 --> 00:06:53,440
profound. Samata meditation is for the purpose of calming the mind where as Vipassana meditation

48
00:06:53,440 --> 00:07:01,840
is for the purpose of seeing clearly. This does not mean that Vipassana meditation does not lead

49
00:07:01,840 --> 00:07:09,600
to tranquility. You should find that through the practice of Vipassana meditation,

50
00:07:09,600 --> 00:07:19,920
you are much calmer in general. Vipassana meditation is differentiated from samata meditation not

51
00:07:19,920 --> 00:07:29,280
because it does not involve tranquility, but because tranquility is not the goal. Whether you feel calm

52
00:07:29,280 --> 00:07:38,560
or not is not practically important. All that is important is that you see your state of calm

53
00:07:38,560 --> 00:07:47,520
or distress clearly. Samata meditation cannot lead directly to enlightenment

54
00:07:48,240 --> 00:07:55,680
because it is not focused on reality. The only way that it can be used to attain enlightenment

55
00:07:55,680 --> 00:08:05,360
is if you follow it up as stated with Vipassana meditation. The strength of mind gained from

56
00:08:05,360 --> 00:08:12,640
practicing samata meditation allows you to see clearer than you would have otherwise.

57
00:08:14,160 --> 00:08:22,160
Since the same can be said of developing mental fortitude, starting with Vipassana meditation directly,

58
00:08:22,160 --> 00:08:30,400
the only real benefit to practicing samata meditation first is the state of peace and calm

59
00:08:30,400 --> 00:08:36,480
and maybe spiritual powers if you dedicate yourself to it to a great degree.

60
00:08:39,120 --> 00:08:46,320
It is in fact clear from the text that starting with Vipassana is the quicker option of the two.

61
00:08:46,320 --> 00:08:50,480
Among one's approach to Buddha and said,

62
00:08:51,280 --> 00:09:02,320
I am old. I do not have much time left and my memory is not good. I am not able to learn everything.

63
00:09:03,040 --> 00:09:11,840
Please teach me the basics of the path. The Buddha told him that first he should establish himself

64
00:09:11,840 --> 00:09:20,960
in an intellectual acceptance of right view and purify his morality to make sure he is ethical

65
00:09:20,960 --> 00:09:29,040
and moral in this behavior. Once he has done that, he has done enough to begin practicing

66
00:09:29,040 --> 00:09:37,440
the four foundations of mindfulness. There is no talk about first focusing the mind or

67
00:09:37,440 --> 00:09:47,280
entering into any meditative state before focusing on reality. In mindfulness, one

68
00:09:47,280 --> 00:09:56,640
accesses meditative states naturally, entering into them based on reality. Even just focusing

69
00:09:56,640 --> 00:10:04,480
under rising falling can be considered an absorbed state for the moment that one is observing it.

70
00:10:04,480 --> 00:10:10,960
When you say stepping right, stepping up, invoking meditation,

71
00:10:12,000 --> 00:10:19,760
if your mind is clearly aware of the movement, you are in an absorbed state for that moment.

72
00:10:20,960 --> 00:10:31,840
So you are cultivating both samata and Vipassana at once. You are tranquil and you are also seeing

73
00:10:31,840 --> 00:10:40,880
flirty.

