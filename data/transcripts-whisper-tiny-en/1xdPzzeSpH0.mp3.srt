1
00:00:30,000 --> 00:00:43,320
Okay, welcome everyone to another episode of Mongrady.

2
00:00:43,320 --> 00:00:56,560
As usual, I don't have a partner in, I don't have a fellow conspirator in the Hangout

3
00:00:56,560 --> 00:01:11,480
today, because I haven't been, I think because I haven't been advertising in advance.

4
00:01:11,480 --> 00:01:21,760
The part of the not advertising in advance thing was because I have to say it's not really

5
00:01:21,760 --> 00:01:28,760
that big of a deal to me. I'm not, I'm not the one looking for questions that people have

6
00:01:28,760 --> 00:01:39,160
questions I'm here to answer to. I think, why advertise? Except that it means I get someone

7
00:01:39,160 --> 00:01:45,440
here to help me. But that's okay, there's not so many questions, so if you're looking

8
00:01:45,440 --> 00:01:52,720
to ask a question, go to radio.ceremungaloon.org and you can check out the questions there

9
00:01:52,720 --> 00:02:01,040
down near the bottom or on the Google Moderator page and go on them and ask your own questions

10
00:02:01,040 --> 00:02:09,840
and so well. So I've got seven questions and I guess I'm just going to start in and who knows

11
00:02:09,840 --> 00:02:18,040
we'll see if someone else shows up here, but I'm not going to wait around. Maybe before

12
00:02:18,040 --> 00:02:28,120
I started, I should do some announcements and stuff. This is a good opportunity to say

13
00:02:28,120 --> 00:02:38,320
some things about what's going on, let people know who are following my activities.

14
00:02:38,320 --> 00:02:47,560
So there's not a lot going on. Sounds like I will be going to, I'll be going to Toronto

15
00:02:47,560 --> 00:02:57,040
and Rochester and I'll be a few days in Hamilton on a Stony Creek Ontario before heading

16
00:02:57,040 --> 00:03:04,760
over to Rochester to visit my mother and that will be the end of May. Right before that

17
00:03:04,760 --> 00:03:12,120
on the 24th, 25th, 26th of May we'll be having advertised this yet, so I was supposed

18
00:03:12,120 --> 00:03:21,560
to do for Pat. But we'll be having a weekend retreat, so we won't, we're not going to make

19
00:03:21,560 --> 00:03:29,960
too big of a deal out of it, but if anyone's in the area, the reason is it's the, we

20
00:03:29,960 --> 00:03:37,720
saw Capuchin which is the big Buddhist holiday of the year, so let me take it as an excuse

21
00:03:37,720 --> 00:03:52,760
to undertake intensive practice or the Buddhist teaching. That's about it really, I can't

22
00:03:52,760 --> 00:04:02,160
think of anything else. Oh, the snow is almost gone finally. What you know isn't, isn't

23
00:04:02,160 --> 00:04:06,640
necessarily a good thing because now the mosquitoes start to come up, but the snow is great.

24
00:04:06,640 --> 00:04:11,520
This actually noticed this part of the year is probably the best because it's cold enough

25
00:04:11,520 --> 00:04:18,080
to keep away mosquitoes and it's warm enough to be outside. I was doing walking meditation

26
00:04:18,080 --> 00:04:24,800
barefoot outside today for the first time and of course sitting meditation in, outside

27
00:04:24,800 --> 00:04:42,040
is quite wonderful or refreshing anyway. It's nice to have this chance. So that, that's

28
00:04:42,040 --> 00:04:47,720
what's going on here and I think that's it. Now we'll go on to answering people's

29
00:04:47,720 --> 00:04:58,920
questions. Okay, so I'm going to start at the top. Is there a benefit to meditating outside

30
00:04:58,920 --> 00:05:04,760
rather than inside? I have heard that meditating indoors is not very synergistic with the

31
00:05:04,760 --> 00:05:10,160
practice. You have to use smaller words. I don't have a clue. It's synergistic. It means

32
00:05:10,160 --> 00:05:19,640
I want to clue. I could look it up, but yes, I think there is potentially a benefit from

33
00:05:19,640 --> 00:05:24,200
time to time to meditating outside. It's funny how I was just talking about this and the

34
00:05:24,200 --> 00:05:28,480
last few days I've been trying to meditate outside as often as possible. It gives you

35
00:05:28,480 --> 00:05:37,560
energy. I mean, the air is, there's more oxygen in the air. It's something very mundane.

36
00:05:37,560 --> 00:05:49,360
It's lighter outside. So there's more, it wakes you up. It's more less distracting because

37
00:05:49,360 --> 00:05:56,120
of all the things inside. It's more natural. So the mind becomes calm through the lack

38
00:05:56,120 --> 00:06:05,720
of chaos that we have inside. These are mundane benefits. They aren't really going to

39
00:06:05,720 --> 00:06:10,480
help you become enlightened, but they aren't going to make you become enlightened, but they

40
00:06:10,480 --> 00:06:16,040
can help you to get your practice jump started or get you out of a rut. If you're practicing

41
00:06:16,040 --> 00:06:20,040
inside on it and you're falling asleep all the time, going outside can pull you out of

42
00:06:20,040 --> 00:06:27,680
that. It can give you energy. It can give you encouragement in your practice. But in the

43
00:06:27,680 --> 00:06:34,360
end, it's also important not to become attached to either type of practice, to be able

44
00:06:34,360 --> 00:06:39,800
to practice in any environment. And for that reason, you know, practicing outside is a challenge

45
00:06:39,800 --> 00:06:45,920
for some people who would rather become through both sitting outside. And so on the other

46
00:06:45,920 --> 00:06:51,240
hand, you can use it as a test for yourself. Go sit under a tree and you don't really even

47
00:06:51,240 --> 00:06:58,680
put up with some mosquitoes, for example, as a means of helping you to let go, helping

48
00:06:58,680 --> 00:07:11,240
you to give up your attachment or get dang in that kind of thing. No. But in general, practicing

49
00:07:11,240 --> 00:07:19,000
in different locations can be a means of opening you up and helping you to let go of your

50
00:07:19,000 --> 00:07:25,320
comfort zone as another reason. But I wouldn't put too much stress on, oh, I have to

51
00:07:25,320 --> 00:07:30,160
meditate outside. I can't meditate in my room. It's to this. It's to that. That is a reason

52
00:07:30,160 --> 00:07:34,120
to practice in your room to help you get over that. For example, if you say, I can't

53
00:07:34,120 --> 00:07:38,120
practice outside, it's to this, it's to that. Now then you maybe you should try practicing

54
00:07:38,120 --> 00:07:52,600
outside to challenge yourself. That the Buddha has at times mentioned, no. No, in the

55
00:07:52,600 --> 00:07:57,320
reason you do manga, it talks about the benefits of sitting outside. The Buddha talked

56
00:07:57,320 --> 00:08:03,960
about this as an ascetic practice with only living outside, never living in a good thing,

57
00:08:03,960 --> 00:08:13,560
as being a useful practice to undertake. I mean, it keeps you from getting lazy, because

58
00:08:13,560 --> 00:08:17,120
you don't have a bed. If you go and sit under a tree all day, you can't just lie down

59
00:08:17,120 --> 00:08:21,640
when you're in your room. If you're sitting on your bed, where is you for it to become

60
00:08:21,640 --> 00:08:30,240
lying on your bed? In Advaita and other schools, the practitioner is often told to identify

61
00:08:30,240 --> 00:08:34,280
with the awareness of the moment and not its content. In other words, when it's told to dwell

62
00:08:34,280 --> 00:08:39,920
as the knower, not the known, is this useful or antithetical to the Buddha. Again, it's

63
00:08:39,920 --> 00:08:45,280
one of these asking me to comment on the practices of other religions. Advaita Vedanta is

64
00:08:45,280 --> 00:08:51,320
a completely different religion. It's not Buddhism. I don't know. I mean, maybe it's

65
00:08:51,320 --> 00:08:59,520
a dog and a bigoted or I don't know. Prejudice, but I read this before this question earlier

66
00:08:59,520 --> 00:09:07,360
and I was thinking, I thought, right away that it's a good example of why one shouldn't

67
00:09:07,360 --> 00:09:13,160
mix different practices, because no, that's not what Buddhism teaches. I'm not exactly

68
00:09:13,160 --> 00:09:17,800
sure it's an odd sort of concept that you're bringing up, but it's certainly not how

69
00:09:17,800 --> 00:09:24,000
the Buddha taught. Whether it's somehow useful or somehow beneficial or not isn't really

70
00:09:24,000 --> 00:09:31,460
the question, it's not part of the course. I'm talking to take this medicine and you

71
00:09:31,460 --> 00:09:36,400
say, what about that medicine? If I take it, will it be helpful as well and I say, well,

72
00:09:36,400 --> 00:09:39,200
if you want to take that medicine, go take that medicine. You shouldn't be taking them

73
00:09:39,200 --> 00:09:44,240
both together and that's exactly what it'll be here. If you start mixing practices, even

74
00:09:44,240 --> 00:09:50,800
in Buddhism, if you mix two different Buddhist practices, you're going to have confusion

75
00:09:50,800 --> 00:09:58,600
and difficulty, but when you start including practices of other religions, why engage in

76
00:09:58,600 --> 00:10:03,520
a practice from Advaita Vedanta? When you've got quite clear practices in Buddhism, if

77
00:10:03,520 --> 00:10:07,040
that's what you want, then really you should be going to practice Advaita Vedanta. There's

78
00:10:07,040 --> 00:10:16,160
no, it's not a pick and choose kind of thing. There's this in the buffet. Even in Buddhism,

79
00:10:16,160 --> 00:10:20,720
pick one practice and stick to it. That's when everyone will tell you, but certainly be

80
00:10:20,720 --> 00:10:26,560
careful about practices outside of Buddhism because they might lead you in entirely other

81
00:10:26,560 --> 00:10:33,440
direction. Sorry, I mean, some people are not going to like that answer, I think, but you

82
00:10:33,440 --> 00:10:54,720
ask me, that's what I say. Let me do it. Just a second. Is it okay to listen to songs about

83
00:10:54,720 --> 00:11:00,240
the Dhamma or that have chance as their lyrics or is this sacrilegious? I listen to such

84
00:11:00,240 --> 00:11:05,040
songs sometimes, but only as a replacement for listening to secular music and not as a replacement

85
00:11:05,040 --> 00:11:15,440
for chanting. Sacrilegious. That's a tough one. The only sense that we endorse the avoiding

86
00:11:15,440 --> 00:11:20,400
avoidance of sacrilegious in the sense of, is it disrespectful to the Buddha, the Dhamma and

87
00:11:20,400 --> 00:11:26,640
the Sangha? I think there's an argument, I don't argue this, but I might, if I thought about it

88
00:11:26,640 --> 00:11:33,280
enough, that you are kind of disrespecting the Buddha and the Dhamma and the Sangha because

89
00:11:34,480 --> 00:11:41,360
singing, for example, is considered a type of wailing in Buddhism or crying or hysterics.

90
00:11:42,480 --> 00:11:46,160
Singing is considered to be a form of hysteric. Sorry, this is the Buddha's teaching.

91
00:11:46,160 --> 00:12:01,120
Singing is considered a form of insanity. So, when anyone puts Buddhism or the Buddha's teaching

92
00:12:01,120 --> 00:12:08,960
to a melody and starts singing it, then you're kind of missing the whole point. It no longer

93
00:12:08,960 --> 00:12:14,640
becomes the Buddha's teaching, it becomes kind of like mockery, you can say. I don't know

94
00:12:14,640 --> 00:12:19,120
many people who argue that, but it seems quite arguable that that is the case.

95
00:12:21,600 --> 00:12:26,240
But having said that, there's no one going to come down and punish you obviously. There's no

96
00:12:26,240 --> 00:12:31,280
Buddha God who's going to, well, maybe the gods wouldn't like it, maybe the angels wouldn't like it,

97
00:12:31,280 --> 00:12:39,840
or maybe they would like it, I don't know. But it might to encourage and to spread that kind of

98
00:12:39,840 --> 00:12:44,960
thing might actually hurt your practice because it's kind of perverting one's understanding of the

99
00:12:44,960 --> 00:12:51,760
Dhamma potentially. And even though you say, well, it's only for the Dhamma, but it's not really,

100
00:12:51,760 --> 00:12:57,120
it's for the sensual enjoyment. There's a pleasure that comes from it and that pleasure

101
00:12:57,120 --> 00:13:05,200
is associated with craving, not dissociated with craving. It's addictive, not otherwise.

102
00:13:05,200 --> 00:13:14,720
Whenever anyone says, believe it or not, this is according to us, the case that it is

103
00:13:17,360 --> 00:13:20,720
is addictive and therefore antithetically, it's to the Buddha's teaching.

104
00:13:22,480 --> 00:13:29,520
But what I was going to say is, there's a case in the Dheganikaya, I think, yeah,

105
00:13:29,520 --> 00:13:36,800
this, I believe it's the Sakapunha Sutta, where this angel comes down and starts giving the Buddha

106
00:13:36,800 --> 00:13:43,200
serenade on the Buddha, the Dhamma, and love. This is his song. It's about the Buddha, the Dhamma,

107
00:13:43,200 --> 00:13:48,960
and love because he's in love with this angel. And so he composed this song for her about the

108
00:13:48,960 --> 00:13:56,000
Buddha, the Dhamma, and love. And the Buddha at the end of his song, the Buddha says,

109
00:13:56,000 --> 00:14:03,200
well, your voice is in tune with your, he says something quite technical about it saying,

110
00:14:03,200 --> 00:14:09,520
how it's technically a very, he's very proficient at this. You're trying to be polite,

111
00:14:09,520 --> 00:14:17,680
but it's quite clear what he means. It's not to say something good about singing in general,

112
00:14:17,680 --> 00:14:26,480
but as far as singing and playing music goes, you have a talent kind of thing. So,

113
00:14:26,480 --> 00:14:31,840
you know, the Buddha wasn't upset by it, obviously. And I think that's probably the best way

114
00:14:31,840 --> 00:14:37,280
to take such things. But as far as encouraging them, even among Buddhist lay people,

115
00:14:37,280 --> 00:14:47,280
and say it's easy. Now, on the other hand, if we take this as a kind of a drug thing, where you

116
00:14:47,280 --> 00:14:54,320
have a drug addiction to music, and you're trying to win yourself off of it, but you can't

117
00:14:54,320 --> 00:15:00,240
seem to overcome it, then like any drug addiction, you should take, you know, taking halfway steps

118
00:15:00,240 --> 00:15:08,560
might be going, so that my music might be a legitimate means of softening with draw symptoms,

119
00:15:08,560 --> 00:15:16,320
that kind of thing. I don't know. It's dangerous. I don't know. Well, I've given my opinion,

120
00:15:16,320 --> 00:15:23,200
I think there's something, if you about it, though I'm not condemning it. We Buddhist,

121
00:15:23,200 --> 00:15:36,720
we don't condemn things. We just tell you what to expect from doing them. Sometimes, in sitting

122
00:15:36,720 --> 00:15:42,800
meditation, I get nauseous, even to the point of having to stop for being close to getting sick,

123
00:15:42,800 --> 00:15:49,280
stopping helps. This is sometimes associated with beauty, but not always. Is there any explanation

124
00:15:49,280 --> 00:15:55,600
to this rather strange phenomenon? I think if you ask my teacher, he would probably say it's

125
00:15:55,600 --> 00:16:04,720
always associated with beauty by his very nature, that nausea, unless it's totally unrelated to

126
00:16:04,720 --> 00:16:13,120
meditation, like in the sense of some food you ate or some sickness you have, then when it comes

127
00:16:13,120 --> 00:16:17,920
from meditation, that is, consider the form of beauty in a technical sense. It's just because

128
00:16:17,920 --> 00:16:23,280
beauty is this kind of catch-all where laughing, crying, yawning, all of these things. When they

129
00:16:23,280 --> 00:16:28,960
come from the meditation, they're considered to be a part of beauty. It would generally be based on

130
00:16:28,960 --> 00:16:38,960
strong concentration, combined with some fairly specific conditions in the body, like in the brain,

131
00:16:38,960 --> 00:16:48,240
in whatever it is that is triggering this nausea. There's nothing wrong with it. That would be

132
00:16:48,240 --> 00:16:56,080
my explanation that it comes from strong concentration, repetitive activity and conditioning. A lot

133
00:16:56,080 --> 00:16:59,920
of these things have nothing to do with the meditation you're doing. They have a lot to do with

134
00:16:59,920 --> 00:17:07,520
what you're bringing into the meditation. Kind of like how the chemicals in your body interact

135
00:17:07,520 --> 00:17:15,600
with ordinary medication or medicine. You're having some kind of side effect, not from the

136
00:17:15,600 --> 00:17:22,480
meditation itself, but from it's mixing with your past conditions until those past conditions get

137
00:17:22,480 --> 00:17:31,200
worked out. This is why things like rapture arise for individuals. They don't arise for everyone.

138
00:17:31,200 --> 00:17:36,160
Another reason it can arise is because you're practicing incorrectly. Are you practicing

139
00:17:36,160 --> 00:17:43,040
according to the force that you put on? If you are, it should get better, not worse. If you're

140
00:17:43,040 --> 00:17:48,880
clearly aware of the nausea, saying you're obnoxious, nauseous, and just seeing it for what it is,

141
00:17:49,440 --> 00:17:55,680
feeling, feeling, or whatever, and being clear about all the things that lead up to it. Instead of

142
00:17:55,680 --> 00:18:02,080
encouraging them and getting in this rut, which is so much a part of what beauty, what rapture is.

143
00:18:02,080 --> 00:18:12,560
If you are able to be clearly aware of it, you'll find it should definitely fade away

144
00:18:13,840 --> 00:18:18,240
until it's no longer a problem. But be careful that you're not encouraging it and that you're

145
00:18:18,240 --> 00:18:21,680
not getting upset about it, that you're not clinging to it, that you're not giving it power and

146
00:18:21,680 --> 00:18:26,720
so on. Try not to stop. Try to just be mindful of it and let it go. If you have to change posture,

147
00:18:26,720 --> 00:18:32,880
change posture, stand that walk, do walking meditation or lie down, do lying meditation, see if

148
00:18:32,880 --> 00:18:37,040
that helps you to let go of it. It's kind of things.

149
00:18:43,920 --> 00:18:50,400
Can you explain the difference between contact and feeling? Using sight as an example,

150
00:18:50,400 --> 00:18:53,280
is contact the moment of seeing and is feeling the pleasure,

151
00:18:53,280 --> 00:19:00,000
or neutrality associated with the sight? Well, first let's start with the second part,

152
00:19:01,440 --> 00:19:06,800
because technically seeing doesn't have any feeling, it doesn't have, seeing always has a

153
00:19:06,800 --> 00:19:13,040
neutral feeling associated with it. This is necessarily true because when you see something,

154
00:19:14,320 --> 00:19:21,120
the light touching the eye doesn't cause pleasure or pain. The contact between the two

155
00:19:21,120 --> 00:19:27,760
elements, the eye and the light, or even as it's experienced in the brain, has no

156
00:19:29,840 --> 00:19:34,480
positive or negative feeling associated with it. There's a neutral feeling associated with that

157
00:19:34,480 --> 00:19:39,440
mind and that moment of seeing. Now, the pleasure or displeasure comes in the next moment,

158
00:19:42,160 --> 00:19:48,640
or the next series of moments where you judge it, where you recognize it, it reminds you of

159
00:19:48,640 --> 00:19:52,560
something that that is pleasant or reminds you of something that is unpleasant,

160
00:19:52,560 --> 00:19:55,440
but it's caused your pleasure, it's caused your pain in the past, and that gets right to

161
00:19:55,440 --> 00:19:59,600
pleasure and pain. Then there's the liking or disliking, which is also associated with

162
00:20:00,800 --> 00:20:09,040
pleasure or pain. Now, those would come together actually. The light king and the disliking

163
00:20:09,040 --> 00:20:16,480
would come together at that moment of judging, of having pleasure or being pleased or displeased

164
00:20:16,480 --> 00:20:24,800
by it, having pleasure or suffering based on it. So it's kind of actually a bad example,

165
00:20:24,800 --> 00:20:31,120
but already it kind of actually shows the difference between pasta and weight in the

166
00:20:31,120 --> 00:20:35,600
pasta is that moment of contact, but in fact, pasta is not just the moment of contact.

167
00:20:35,600 --> 00:20:42,800
It is the contact, pasta doesn't exist, or maybe it does exist as a jate, I can't remember,

168
00:20:42,800 --> 00:20:49,760
but pasta is the contact, the contact between the eye, the light, and the consciousness.

169
00:20:49,760 --> 00:20:56,080
When those three things come together, there is a scene. The contact is the

170
00:20:59,600 --> 00:21:04,960
meeting of these three things, that's what we call pasta. Because of that meeting,

171
00:21:04,960 --> 00:21:08,720
what it means is because of that meeting, their arises weighed and their arises are feeling,

172
00:21:08,720 --> 00:21:13,920
but actually their arises consciousness first, and then their arises are feeling, according to

173
00:21:13,920 --> 00:21:22,320
the heavy damage. But in practical, in practical terms, when there is the sensation, as a result

174
00:21:22,320 --> 00:21:27,600
of that, there will arise a feeling, and that feeling will be pleasant or unpleasant, because of

175
00:21:27,600 --> 00:21:41,360
that there will be liking or disliking, and there will be clinging and so on.

176
00:21:41,360 --> 00:21:45,360
So they are quite different, in a way denies the feeling that arises. Pasta is the contact,

177
00:21:45,360 --> 00:21:49,840
I mean, I don't really understand what the confusion between the two would be.

178
00:21:49,840 --> 00:21:56,240
That seems quite difficult to confuse them. I wonder if I'm too loud here.

179
00:21:56,240 --> 00:22:04,400
It might doesn't look like, I wonder if I'm learning out my speakers.

180
00:22:10,640 --> 00:22:15,360
Okay. Yes, so next.

181
00:22:18,240 --> 00:22:23,760
Please, I need your help. I have chronic insomnia. I want to stop taking medications,

182
00:22:23,760 --> 00:22:28,000
I'm learning how to meditate, but I don't know if I'm doing the correct way. Please,

183
00:22:28,000 --> 00:22:30,960
I want to communicate with you, and I don't know how I'm new with all of this.

184
00:22:31,840 --> 00:22:35,680
Well, this isn't the best way to communicate with me, but it's maybe the best way to start,

185
00:22:35,680 --> 00:22:40,960
the best way is to send me a message. If you need my help, I'm happy to help,

186
00:22:42,000 --> 00:22:49,600
go to my web blog, and send me a message there, and I'll email you back if it's urgent,

187
00:22:49,600 --> 00:22:57,200
in this case, it sounds kind of urgent. But to start, and another thing is,

188
00:22:57,200 --> 00:23:04,400
read my booklet on how to meditate, because that is my understanding of the correct way to meditate

189
00:23:04,400 --> 00:23:11,280
in my tradition. So, I think that booklet should answer a lot of your questions,

190
00:23:11,280 --> 00:23:18,320
and should help you deal with insomnia. I mean, basically, or do a search on YouTube for my videos

191
00:23:18,320 --> 00:23:23,760
on insomnia, I think. I wonder if I have them. I wonder if they're title. I know,

192
00:23:23,760 --> 00:23:26,880
I've talked about it so many times, and it's one of the big ones that I use, because it's one of

193
00:23:26,880 --> 00:23:34,160
the easiest to cure with meditation. So, you can google me, and you have to demo, and insomnia.

194
00:23:35,280 --> 00:23:41,760
Hopefully, there's a video that I've done that I've titled insomnia. But otherwise, it's

195
00:23:41,760 --> 00:23:47,600
quite simple. I mean, the first thing to do is to stop trying to fall asleep. Stay awake all night,

196
00:23:47,600 --> 00:23:54,240
meditate all night. Don't try to stay awake, but allow yourself to stay awake. That's the first

197
00:23:54,240 --> 00:23:59,680
step. The biggest problem with insomnia is the need to sleep, or the perceived need to sleep.

198
00:24:00,880 --> 00:24:05,840
Obsession with sleeping, which is unhealthy. You shouldn't be obsessed with sleep. You shouldn't

199
00:24:05,840 --> 00:24:09,600
be worried about sleeping at all. If you can stay awake all night and meditate, it's actually

200
00:24:09,600 --> 00:24:16,480
much better for you than sleeping. But what you'll find is, as you allow yourself to stay awake,

201
00:24:16,480 --> 00:24:24,400
and as you slip in, you know, fall into the moment, you know, and are present with the phenomena

202
00:24:24,400 --> 00:24:32,560
that arise, with the experience of, you know, in the beginning, the stress and the worry, and the

203
00:24:32,560 --> 00:24:39,200
anger, the frustration, and not being able to sleep. But once you start to accept and examine and

204
00:24:39,200 --> 00:24:44,160
be with these emotions, you find it's actually much easier to fall asleep, but actually you fall

205
00:24:44,160 --> 00:24:52,000
asleep without even thinking about it. So first step, get this clear in your mind that you don't

206
00:24:52,000 --> 00:24:58,400
need to sleep. You really don't. Just be awake. Do meditation, lying meditation. Start with

207
00:24:59,040 --> 00:25:04,640
watching the stomach. When you lie there, lie in your back or something, and watch the stomach rise,

208
00:25:04,640 --> 00:25:09,440
saying, falling when it rises, it gives a rise, saying when it falls, it gives a fall,

209
00:25:09,440 --> 00:25:17,680
a rise, and finally, and until you, you know, until you theoretically fall asleep, if you feel

210
00:25:17,680 --> 00:25:20,800
angry, frustrated, frustrated, frustrated, frustrated, frustrated, frustrated, frustrated, stressed,

211
00:25:20,800 --> 00:25:29,520
sad yourself, stressed, stressed, just remind yourself or repeat these observations to yourself

212
00:25:29,520 --> 00:25:40,720
as you observe something, repeat it, worry, worry or afraid or angry, and do that with everything

213
00:25:40,720 --> 00:25:46,160
that arises. And if there's nothing come back to the rise, and this is how I teach meditation,

214
00:25:46,160 --> 00:25:51,680
you should be able to find some of the resources I've got up there, just by googling my name,

215
00:25:53,440 --> 00:25:57,200
best one is the booklet I wrote on how to meditate, so look up, how to meditate,

216
00:25:57,200 --> 00:26:03,840
you don't know when you'll find it. That's probably the best place to start. And I guarantee

217
00:26:04,400 --> 00:26:10,720
if you do it correctly, insomnia will never be a problem for you again. There's a hundred percent

218
00:26:10,720 --> 00:26:20,800
money back guarantee. Now, and if you need to ask questions, just send me an email with it.

219
00:26:20,800 --> 00:26:28,560
What should we do in the situations where we can't be mindful rising and falling? Can we use

220
00:26:28,560 --> 00:26:36,800
other eightfold paths at that moment? At times, when I can't be mindful at some situation,

221
00:26:36,800 --> 00:26:41,840
I get frustrated and hence suffer a lot. Should I lower my expectations, thanks.

222
00:26:44,720 --> 00:26:49,120
Is it really, this is totally unrelated, is it really easier to type the ad symbol than to write

223
00:26:49,120 --> 00:26:53,440
ad? That's quite interesting. That's a stupid question.

224
00:27:03,200 --> 00:27:06,080
You can't, you can't just pick and choose eightfold paths

225
00:27:09,840 --> 00:27:14,720
members, not exactly. I mean, it does help to sometimes cultivate one or the other,

226
00:27:14,720 --> 00:27:22,160
but it doesn't get you very far. There's no, the Buddha said, Satin Chaka, a hung bekeway,

227
00:27:22,160 --> 00:27:28,800
sabatika, and wadami. Mindfulness is useful at all moments. There's no moment, except when you're

228
00:27:28,800 --> 00:27:34,160
asleep, I suppose, where mindfulness is, where you can't be mindful.

229
00:27:39,200 --> 00:27:44,560
So unless you're talking about when you sleep, but then you couldn't use any of the path factors,

230
00:27:44,560 --> 00:27:51,200
anyway, there's no, there's no moment where you couldn't be mindful. And so mindfulness isn't

231
00:27:51,200 --> 00:27:55,360
rising and falling, those aren't equal. Rising and falling is an example of how to be mindful.

232
00:27:56,240 --> 00:28:04,640
When you get frustrated, that's an incredible opportunity to cultivate mindfulness.

233
00:28:04,640 --> 00:28:12,400
Say to yourself, frustrated, frustrated. When you suffer, say to yourself, suffering, suffering,

234
00:28:12,400 --> 00:28:20,640
or pain, pain, unhappy, unhappy, depressed, depressed, just expecting, don't lower your expectations,

235
00:28:20,640 --> 00:28:26,080
just learn about them, study them, and you'll find yourself letting them go. Naturally,

236
00:28:28,480 --> 00:28:33,520
read my booklet, probably you haven't read my booklet all the way through. That's what I

237
00:28:33,520 --> 00:28:43,760
would recommend. And that's it. That was easy. See, when I don't advertise, it's much easier on my

238
00:28:43,760 --> 00:28:52,400
part. So I think probably things will come together once. I think everyone's just, oh, he didn't

239
00:28:52,400 --> 00:28:58,320
advertise, so maybe he's not doing it, or else they're just like, ah, I've heard all the questions

240
00:28:58,320 --> 00:29:11,920
already. Why bother with that? We have 19 viewers, which is not bad. And if you, again, if you

241
00:29:11,920 --> 00:29:18,560
have questions, you should be visiting radio.ceremungalode.org. That's where you should go to

242
00:29:18,560 --> 00:29:24,320
ask questions. So for example, for next week, you could ask questions there. If you want to

243
00:29:24,320 --> 00:29:34,640
join the hangout and sit here with me and have your little, have your video down below,

244
00:29:35,280 --> 00:29:42,560
right beside my video, and then ask the questions and I'll answer them. So help me to do the asking,

245
00:29:43,520 --> 00:29:48,640
and maybe even help to answer them. Then you have to send me a message, get in my circles,

246
00:29:48,640 --> 00:29:55,360
unless you're already in my circle, and you have, well, that's not true. You have to be part

247
00:29:55,360 --> 00:30:02,240
of our community. So if you are someone who is, has been following what I do and is dedicated to

248
00:30:02,240 --> 00:30:15,120
this practice, and what therefore be a harmonious companion to my hangout group, send me a message,

249
00:30:15,120 --> 00:30:24,240
saying that, and I'll try to add you to the group. Okay, so that's all. Thank you for tuning in.

250
00:30:24,240 --> 00:30:30,720
We'll cut it there at half an hour, and please tune in next time. It should be the same time

251
00:30:31,440 --> 00:30:38,240
next Saturday. So every Saturday at 5 p.m. my time, if you want to find out your time,

252
00:30:38,240 --> 00:30:48,560
go to radio.ceremungal.org. It's 2200 UTC, I think, but it should give you local times if it's not broken.

253
00:30:48,560 --> 00:31:18,400
Anyway, have a good night for the singing.

