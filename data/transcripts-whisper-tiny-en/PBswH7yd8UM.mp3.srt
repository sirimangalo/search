1
00:00:00,000 --> 00:00:11,540
Hello and welcome back to our study of Vidamapana. Today we look at verse 172 which reads

2
00:00:11,540 --> 00:00:21,820
Yopube, Yocha, Pube, Pimenti, Tua, Phachaso, Nampamatiti. So Manglokang, Pabhavaseti,

3
00:00:21,820 --> 00:00:36,980
Abhom, Tova, Tandima, which means he who was whoever was heedless before,

4
00:00:36,980 --> 00:00:49,940
negligent before. But afterwards, it's no longer heedless, no longer negligent.

5
00:00:49,940 --> 00:01:00,140
Such a person, so Manglokang, Pabhavaseti, such a person lights up this world,

6
00:01:00,140 --> 00:01:11,380
like the moon when it comes out from behind a cloud. So this famous, another famous

7
00:01:11,380 --> 00:01:22,780
Dhammapada verse, was taught in relation to a monk with a broom. It's also

8
00:01:22,780 --> 00:01:32,700
mentioned, I think, in the next one, which is more famous. The story coming up, or not the

9
00:01:32,700 --> 00:01:36,700
next one, but a couple from now, there's a story that's more famous that we'll look at.

10
00:01:36,700 --> 00:01:49,220
And I think the verses mention there, but it's not the verse of the story. Anyway, this story,

11
00:01:49,220 --> 00:02:04,980
the story is about a monk with a broom. And so the story goes that there was one monk

12
00:02:04,980 --> 00:02:14,620
called Samunjani, who was always sweeping. He would sweep in the morning, he would go for

13
00:02:14,620 --> 00:02:23,100
arms when he came back, he would sweep, sweep, sweep the monastery. There are these sorts of monks

14
00:02:23,100 --> 00:02:28,780
I've seen, these sorts of monks and various monasteries that I've lived in or visited,

15
00:02:28,780 --> 00:02:45,260
monks and nuns who sweep a lot, or who work a lot, who do this or that work and are always busy.

16
00:02:45,260 --> 00:02:51,340
And not only did he do this himself, but one day he went and saw Rehwata, Rehwata,

17
00:02:51,340 --> 00:03:00,860
who was the foremost in terms of seclusion. Among the Buddhist disciples, I think that's what it

18
00:03:00,860 --> 00:03:05,260
was. Anyway, he was a monk who delighted in seclusion and never wanted to be around people,

19
00:03:06,940 --> 00:03:16,220
just wanted to be off on his own. So he lived in a, he normally lived in a very dense and

20
00:03:16,220 --> 00:03:24,540
impenetrable jungle. It's a kind of place where nobody else would want to go. He was

21
00:03:25,740 --> 00:03:30,860
interesting that way, but here he seems to have been in Jitaana, just probably to visit the Buddha.

22
00:03:32,380 --> 00:03:38,780
And he saw Rehwata, this monk, Samunjani saw Rehwata sitting there with his eyes closed.

23
00:03:40,460 --> 00:03:45,820
And he noticed that Rehwata would just sit around all day and not do any work.

24
00:03:45,820 --> 00:03:53,980
And he thought to himself, this monk, and he goes and takes the food, takes people's food,

25
00:03:53,980 --> 00:03:58,220
and then he comes back and he doesn't do any sweeping. He could at least sweep out one room.

26
00:04:00,860 --> 00:04:05,660
And Rehwata, I think, had some kind of power because he seemed to have known what was going on

27
00:04:05,660 --> 00:04:12,780
in this monk, this other monk's mind. He never called him, Samunjani called him a great

28
00:04:12,780 --> 00:04:38,460
idol, he was idol, and so Rehwata says, I should teach him, I should teach this monk something.

29
00:04:38,460 --> 00:04:44,220
And so he called the monk over and he explained to him, he said, look,

30
00:04:46,140 --> 00:04:49,740
of course you should be sweeping, you should sweep first thing in the morning,

31
00:04:50,700 --> 00:04:58,060
and go for arms and then come back and practice meditation, basically, a very simple teaching.

32
00:04:59,420 --> 00:05:05,260
But he gave him a specific meditation practice, and then in the evening then you can go and

33
00:05:05,260 --> 00:05:10,540
sweep again, so you sweep in the morning, sweep in the evening, but you really need to give yourself

34
00:05:10,540 --> 00:05:18,540
a chance. He says, oh, kas, oh, kas, oh, kas, oh, kata, or something like kata, bo, or something like

35
00:05:18,540 --> 00:05:24,540
I wanted to give oneself the opportunity to do something more meaningful.

36
00:05:25,740 --> 00:05:31,420
And so this monk took his admonishment and as a result became enlightened. Now when he was

37
00:05:31,420 --> 00:05:37,580
after he became enlightened, he no longer swept. He swept a little in the morning and some in the

38
00:05:37,580 --> 00:05:46,700
evening. The monk saw that this sweeping monk no longer did his work, and so the rooms got a little

39
00:05:46,700 --> 00:05:53,580
bit dingy and dusty. They saw that, near the monk saw that the monastery was no longer

40
00:05:53,580 --> 00:06:01,020
spic and span like it used to be. And so they came up to him and they said, well, why aren't you,

41
00:06:01,020 --> 00:06:11,420
why aren't you sweeping anymore? And he said, oh, but I've been able to say, I used to do that

42
00:06:11,420 --> 00:06:15,340
when I was heedless. Now, however, I've become heedful.

43
00:06:15,340 --> 00:06:24,780
And the monks heard this and they were indignant because they thought, look at this into this monk.

44
00:06:24,780 --> 00:06:29,420
We're tending to be something enlightened or something like that. So they went to see the Buddha

45
00:06:29,420 --> 00:06:34,700
and they said, they told the Buddha and they said, they're just monkos. He used to work,

46
00:06:34,700 --> 00:06:41,260
but now just in order to lay around, he claims to be an aran. He claims to be somehow enlightened.

47
00:06:41,260 --> 00:06:51,260
And the Buddha said, oh, no, it's true. My son, it calls it my son. My son used to be heedful.

48
00:06:51,260 --> 00:06:55,740
When he was heedless, he used to be heedless. When he was heedless, he spent all his time sweeping.

49
00:06:57,100 --> 00:07:04,380
But now he spends his time in the bliss of enlightenment and therefore sweeps no more.

50
00:07:04,380 --> 00:07:12,940
And then he said, you open me, put my jidpa on. Who's heedless before? He'd list no more.

51
00:07:12,940 --> 00:07:16,860
Illuminate, illumines this world as does the moon freed from a cloud.

52
00:07:22,700 --> 00:07:27,820
So I think, again, we separate the story and the verse, the verse isn't

53
00:07:29,260 --> 00:07:33,580
really all that related to the story. But the story has an interesting lesson.

54
00:07:33,580 --> 00:07:42,300
It's the question of work and more generally how we spend our time.

55
00:07:43,820 --> 00:07:51,660
And so you can apply this to ordinary life and all aspects of our life where we occupy ourselves

56
00:07:51,660 --> 00:08:01,020
with the various tasks. Sometimes caught up in ambition where we have some kind of

57
00:08:01,020 --> 00:08:12,460
drive that drives us to work, very hard. Sometimes it's a drive for money or power or status

58
00:08:12,460 --> 00:08:24,780
or respect. Often there's a sense of pride and righteousness.

59
00:08:24,780 --> 00:08:31,580
You know, I'm a hard worker. I worked hard and look what I've got to show for it.

60
00:08:33,980 --> 00:08:40,780
I met some Americans when I was living in America. I met people who were quite indignant

61
00:08:40,780 --> 00:08:50,540
that monks should not work. So there was something noble or right and many people truly feel this

62
00:08:50,540 --> 00:08:58,620
way. There's something noble and right about working. Thoreau said to give a modern example,

63
00:08:58,620 --> 00:09:07,180
he commented on this and he said, these people, they're like, they're like oxen.

64
00:09:09,340 --> 00:09:13,340
They'll work, work, work very hard, but what's the point? And they seem

65
00:09:13,340 --> 00:09:20,540
somehow to feel like it's right. There's a rightness to the work that we do.

66
00:09:22,540 --> 00:09:30,300
We're taught. We're taught that this is the right way to live. It's right to have a job.

67
00:09:31,820 --> 00:09:40,540
Anyone who doesn't have a job is a bum, an unemployed bum. And so you had this movement in America

68
00:09:40,540 --> 00:09:47,180
called the Dharma Bums. And I'm assuming it was somehow related to this sort of as a counterculture

69
00:09:47,180 --> 00:09:54,220
of people who realized, well, you don't really need to work. You know, there's no imperative

70
00:09:54,220 --> 00:09:59,980
to be a hardworking individual in the world. I imagine they were probably a little bit lazy

71
00:09:59,980 --> 00:10:11,420
in the sense I sort of get from it. In fact, I think work is quite valuable. It's just a question

72
00:10:11,420 --> 00:10:18,540
of what is the right work. And what makes clear in this story that it's not that one should

73
00:10:18,540 --> 00:10:27,500
never work in terms of physical labor or cleanliness or so on. That shouldn't be one's

74
00:10:27,500 --> 00:10:39,580
ultimate concern. There are mundane necessities. We have to eat. We have to live. We have to

75
00:10:39,580 --> 00:10:50,140
care for our possessions. But the point is to not let them own us, right? To not let our work

76
00:10:50,140 --> 00:10:59,580
consume us. To not let our possessions control us. And we can see as as we're mindful

77
00:11:01,500 --> 00:11:08,540
to what extent this is the case. And we naturally inclined towards simplifying our life as we see

78
00:11:08,540 --> 00:11:22,140
that the complexity and the busyness, the overall lack of opportunity as Raimata says,

79
00:11:23,260 --> 00:11:35,100
opportunity to perform the more meaningful work. I think it's a reminder to us. So the story is

80
00:11:35,100 --> 00:11:46,140
an important reminder that busyness is not in and of itself valuable. But work and

81
00:11:48,060 --> 00:11:59,180
energetic effort, this is valuable. As for the story, as for the verse,

82
00:11:59,180 --> 00:12:09,660
this verse is quite famous. I think it's both inspiring and instructive. It's a clear reminder

83
00:12:09,660 --> 00:12:16,620
of what is important. This word, a pamanda. Let me talk about so often. You don't hear about

84
00:12:16,620 --> 00:12:23,500
so much in English, I think, but the poly word, a pamanda. It's hard to translate because pamanda

85
00:12:23,500 --> 00:12:32,300
means pamanda really means intoxication of some sort, but it's not intoxication with alcohol,

86
00:12:33,740 --> 00:12:47,340
emotional intoxication or mental intoxication, mental drunkenness. When you're drunk on greed or

87
00:12:47,340 --> 00:13:00,060
anger or delusion or all three, pamanda is not being mindful. The Buddha said again and again,

88
00:13:00,940 --> 00:13:05,340
it's mindfulness. When you're mindful, that's what it means to be

89
00:13:05,340 --> 00:13:16,860
unheadless and unnegligent, non-negligent,

90
00:13:19,500 --> 00:13:30,380
to be sober. Mindfulness is waking up. This is why this imagery is so powerful,

91
00:13:30,380 --> 00:13:36,780
because it really is like coming out from behind a cloud, whether you light up the world

92
00:13:37,420 --> 00:13:43,900
when you're sitting at night and the moon comes out in the whole world. This is like

93
00:13:44,620 --> 00:13:49,900
when mindfulness arises in the mind, it's like you were asleep. It's like you were in a dark room,

94
00:13:49,900 --> 00:14:00,060
and you can only now see things as the light is turned on.

95
00:14:04,060 --> 00:14:14,220
It speaks of the condition of being in the wrong in the past that we shouldn't be too discouraged.

96
00:14:14,220 --> 00:14:19,260
My teacher would often recite this first to people who were discouraged about

97
00:14:20,700 --> 00:14:26,540
their past, bad things they had done, feeling guilty about them, feeling perhaps

98
00:14:31,180 --> 00:14:39,580
unqualified for the task at hand because of their past. He would say,

99
00:14:39,580 --> 00:14:45,420
well, that's all in the past. When you were in the past, it's not important to know and doing

100
00:14:47,020 --> 00:14:50,860
the right thing and then he would recite this first.

101
00:14:54,220 --> 00:15:00,380
We shouldn't dwell in the past. We should acknowledge that we're all coming out of darkness.

102
00:15:01,100 --> 00:15:07,820
We're all coming out of delusion and be clear of what takes us out of delusion.

103
00:15:07,820 --> 00:15:16,300
It's more important to be inspired by how profound and wonderful

104
00:15:17,900 --> 00:15:21,900
is the change that comes when one is mindful, how it lights up your mind,

105
00:15:23,660 --> 00:15:29,980
it lights up the earth, how it relates to the story and the way that it is the most important work,

106
00:15:29,980 --> 00:15:37,500
the most valuable work. The Buddha doesn't just say that this person brings light to themselves.

107
00:15:37,500 --> 00:15:46,300
He says this person lights up the world and so in a way it's an implication that they do the

108
00:15:46,300 --> 00:15:54,620
most important work because they bring light to the world. A person who is mindful,

109
00:15:54,620 --> 00:16:02,220
you know, if you think about what is accomplished through the work that we do, you ask yourself,

110
00:16:02,220 --> 00:16:08,460
these people who are proud of anyone who is proud of their job or they're there,

111
00:16:09,660 --> 00:16:18,380
for statelessness, their effort that they put into worldly affairs. Ask them, what is the result?

112
00:16:18,380 --> 00:16:33,340
If I make things or if I just crunch numbers or if I sell things or so and ask yourself,

113
00:16:33,340 --> 00:16:38,380
what are you accomplishing with your work? Even for yourself, you're only gaining money,

114
00:16:38,380 --> 00:16:43,980
but you ask, what are you doing for other people? What are you doing for the world?

115
00:16:43,980 --> 00:16:51,980
I mean, it's quite useful for those people living in the world to ask, what is it that I'm doing?

116
00:16:52,620 --> 00:16:59,820
What effect does my job have on the world? Does my work have on the world?

117
00:17:00,540 --> 00:17:06,300
I think that really frames things in a more favorable light for Buddhist monk or Buddhist

118
00:17:06,300 --> 00:17:13,660
meditator because the effect of our work, the work that we do is as meditators

119
00:17:15,820 --> 00:17:21,660
by cultivating mindfulness. It brings about the best result.

120
00:17:23,180 --> 00:17:28,540
You know, it reduces at the very least the amount of greed, anger, and delusion,

121
00:17:28,540 --> 00:17:39,500
the amount of defilements in the world. But moreover, it provides a source and a resource

122
00:17:41,180 --> 00:17:57,340
for the spread of mindfulness, of enlightenment, of peace, of purity, of an absence of all those things

123
00:17:57,340 --> 00:18:06,460
that are causing the problems. It's like this illness, this virus that spreads.

124
00:18:08,300 --> 00:18:15,180
The human race is like an organism that is crippled by a debilitating illness,

125
00:18:16,540 --> 00:18:21,420
living and getting by. There's a lot of good that happens in the world, but

126
00:18:21,420 --> 00:18:27,820
it can always see that it's sick. The human race is sick, and it's ill,

127
00:18:30,140 --> 00:18:33,500
and so we're spreading the cure, the antivirus,

128
00:18:36,060 --> 00:18:41,100
to the practice of meditation, curing the human race. Now, whether it will ever be cured,

129
00:18:41,100 --> 00:18:48,540
I mean, I think it ebbs and flows, but there's no question or no doubt about

130
00:18:48,540 --> 00:18:54,620
the result of the practice of meditation and mindfulness when it does occur.

131
00:19:00,540 --> 00:19:07,980
So, it speaks of the result of mindfulness. When a person is mindful and a person is no longer

132
00:19:09,420 --> 00:19:15,580
caught up in heedlessness, caught up in indulgence, and suddenly the world, it's like everything

133
00:19:15,580 --> 00:19:21,500
is set right. Suddenly you're here, you're present, you're aware, you're awake,

134
00:19:23,740 --> 00:19:24,700
and mind is clear.

135
00:19:28,780 --> 00:19:32,300
Just like the moon when it comes up from behind the cloud, it's just good imagery.

136
00:19:32,300 --> 00:19:48,220
It's a good verse. So, that's the Dhamupanda for this week. Thank you all for tuning in. Have a good night.

