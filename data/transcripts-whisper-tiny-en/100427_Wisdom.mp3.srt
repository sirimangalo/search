1
00:00:00,000 --> 00:00:15,120
okay so welcome everyone to our weekly deer park session and because we only

2
00:00:15,120 --> 00:00:24,180
have a session once a week I thought it's probably useful not just to give

3
00:00:24,180 --> 00:00:31,840
talks but to also have some sort of guided introduction to actual meditation

4
00:00:31,840 --> 00:00:39,480
practice so this talk is going to be a little bit more hands-on so to speak

5
00:00:39,480 --> 00:00:57,460
today's talk the topic is wisdom because of course wisdom and Buddhism is the

6
00:00:57,460 --> 00:01:06,120
highest virtue the highest quality which we're hoping to attain hoping to gain

7
00:01:06,120 --> 00:01:12,920
from our practice the Buddha said compared wisdom like the moon amongst

8
00:01:12,920 --> 00:01:29,920
the stars when you look at the night sky you see a lot of bright lights and you

9
00:01:29,920 --> 00:01:37,800
say it it's amazing how bright the stars are but then when the moon comes up

10
00:01:37,800 --> 00:01:47,500
you can see that it's it's on a whole other level it's a whole other volume of

11
00:01:47,500 --> 00:01:52,540
brightness in comparison to the stars and so we should keep this in mind when

12
00:01:52,540 --> 00:01:57,220
we think of what is it that we're trying to gain trying to get crying to attain

13
00:01:57,220 --> 00:02:01,900
in our practice and we should take this as an understanding that what we're

14
00:02:01,900 --> 00:02:09,460
really hoping to gain is is wisdom and so this is of course a very important

15
00:02:09,460 --> 00:02:14,580
topic of discussion and I think it's something that perhaps many people who

16
00:02:14,580 --> 00:02:20,680
surf the internet and virtual reality and so on are not thinking to

17
00:02:20,680 --> 00:02:31,220
to obtain in fact often the internet and these sort of game-like worlds are

18
00:02:31,220 --> 00:02:37,760
considered to be sort of like a past time or a break from our spiritual

19
00:02:37,760 --> 00:02:46,560
quest and and many people come here because perhaps they're though they know

20
00:02:46,560 --> 00:02:50,280
they're looking for something they don't realize the importance of wisdom and

21
00:02:50,280 --> 00:02:58,200
so it may have never entered their minds that wisdom was of any benefit but we

22
00:02:58,200 --> 00:03:02,040
can see even in our daily lives how important wisdom is how important it is

23
00:03:02,040 --> 00:03:14,280
simply to live our lives simply to be successful if we want to succeed in our

24
00:03:14,280 --> 00:03:20,760
work we need a certain amount of wisdom we need wisdom having having learned

25
00:03:20,760 --> 00:03:26,840
things we have to have gone to school and we have to have heard and and seen

26
00:03:26,840 --> 00:03:33,200
and read many things which are appropriate we have to kept it in mind and we

27
00:03:33,200 --> 00:03:38,160
have to have thought about it and put it into practice and we have to have

28
00:03:38,160 --> 00:03:44,480
had experience and all of these things are part of what it means by the

29
00:03:44,480 --> 00:03:53,600
word wisdom if we want to be successful in relationships if we want to have

30
00:03:53,600 --> 00:04:00,320
good friends if we want to be surrounded by kind and caring people then we

31
00:04:00,320 --> 00:04:03,480
have to know how to relate to them and we have to have social skills and we

32
00:04:03,480 --> 00:04:08,280
have to have interpersonal skills and we have to know how to tell the difference

33
00:04:08,280 --> 00:04:17,240
between a person who is looking for good things and a person who is headed

34
00:04:17,240 --> 00:04:22,440
towards bad things or unwholesome things a person who is going to lead us in a

35
00:04:22,440 --> 00:04:29,520
bad path and a person who is going to lead us to improvement is going to have

36
00:04:29,520 --> 00:04:33,280
all the qualities of a good friend we have to know how to deal with these people

37
00:04:33,280 --> 00:04:40,760
if we're kind of a person who doesn't have a certain level of wisdom then

38
00:04:40,760 --> 00:04:47,040
it's quite likely that people who are somehow high-minded are not going to be

39
00:04:47,040 --> 00:04:53,120
at all interested in associating with that person so that really everything

40
00:04:53,120 --> 00:04:59,160
everything we do and all of our achievements in this world all of the people

41
00:04:59,160 --> 00:05:08,080
who we esteem as the greats it really all has to do with some sort of level of

42
00:05:08,080 --> 00:05:18,960
wisdom we think of Socrates or we think of Einstein or any of the great even the

43
00:05:18,960 --> 00:05:26,600
great authors or the great scientists or philosophers we can think of what makes

44
00:05:26,600 --> 00:05:33,520
a person great and we can see how wisdom is actually very important now so

45
00:05:33,520 --> 00:05:42,960
what we're dealing with in Buddhism is a wisdom teaching a teaching on some sort

46
00:05:42,960 --> 00:05:48,160
of knowledge or some sort of understanding similar you could think of in terms

47
00:05:48,160 --> 00:05:54,440
of Plato or or or Socrates where they had some kind of philosophy and way of

48
00:05:54,440 --> 00:05:58,920
looking at the world and they had teachings and they had a outlook and a method

49
00:05:58,920 --> 00:06:05,240
and so on when we talk about Buddhism we're not talking about a faith-based

50
00:06:05,240 --> 00:06:14,640
religion or something based on magic or based on superstition or or even based on

51
00:06:14,640 --> 00:06:22,440
super mundane states we're talking about a practice for understanding that

52
00:06:22,440 --> 00:06:25,320
teaches us more about ourselves and more about the world around this

53
00:06:25,320 --> 00:06:29,440
helps us to understand those things that we're confronted with on a daily

54
00:06:29,440 --> 00:06:34,400
basis helps us to understand all of those huge big questions which in the

55
00:06:34,400 --> 00:06:39,800
end turn out to be very very mundane such as what is the meaning of life what

56
00:06:39,800 --> 00:06:49,200
is the meaning of the universe what is the meaning of reality so on and so this is

57
00:06:49,200 --> 00:06:52,560
really where we start in Buddhism and it's where we finish where we start in

58
00:06:52,560 --> 00:06:59,000
Buddhism as we start by teaching we start by by learning by gaining that sort

59
00:06:59,000 --> 00:07:06,960
of wisdom which is theoretical similar to going to school and studying or

60
00:07:06,960 --> 00:07:16,000
listening to people talk or reading books or so on all wisdom all attainment of

61
00:07:16,000 --> 00:07:22,360
understanding comes or starts from this basis of theoretical knowledge some

62
00:07:22,360 --> 00:07:28,880
people some many Buddhists in the beginning they can mistake this and they

63
00:07:28,880 --> 00:07:36,880
think that theoretical knowledge is useless it's unnecessary that the

64
00:07:36,880 --> 00:07:45,960
only wisdom that's worth anything is experiential wisdom and then

65
00:07:45,960 --> 00:07:53,000
this sort of you can seem kind of naive from someone who has really put

66
00:07:53,000 --> 00:07:58,200
into practice these teachings when you realize how how subtle and how

67
00:07:58,200 --> 00:08:04,840
difficult it is to to get on sort of this this path of understanding to

68
00:08:04,840 --> 00:08:13,320
avoid all and to avoid all the pitfalls of some sort of delusion or

69
00:08:13,320 --> 00:08:19,920
practice which which is leading one down a dead into a dead end or so on

70
00:08:19,920 --> 00:08:26,280
that without without the theory without a teaching it's basically like having

71
00:08:26,280 --> 00:08:35,760
to to reinvent the wheel or reinvent the entire path and so it's possible and

72
00:08:35,760 --> 00:08:39,120
it's shown the fact that it's possible is shown by the fact that the Buddha

73
00:08:39,120 --> 00:08:46,720
actually did it he had no one to teach him but it's very very difficult and

74
00:08:46,720 --> 00:08:52,080
it it's actually impossible you can say that the Buddha had no teacher but he

75
00:08:52,080 --> 00:08:59,160
did a lot of theoretical study he did a lot of laying down the axioms and

76
00:08:59,160 --> 00:09:04,760
the basics talking about what is the basics of reality and through

77
00:09:04,760 --> 00:09:10,160
experience experiment he was able to come up with this theory and so unless

78
00:09:10,160 --> 00:09:13,160
we're willing to do that we shouldn't think that we can just go and sit and

79
00:09:13,160 --> 00:09:16,960
somehow enlightenment is going to come to us that you have to do all of this

80
00:09:16,960 --> 00:09:21,720
preparatory work at least with certain extent so so what I'm going to try to

81
00:09:21,720 --> 00:09:26,440
give today is a little bit of theory that's important when we talk about

82
00:09:26,440 --> 00:09:32,680
wisdom teachings we're generally talking about vast amounts of information so

83
00:09:32,680 --> 00:09:37,960
whereas people might think the Buddhism is all about just sitting still it's

84
00:09:37,960 --> 00:09:44,840
that's certainly very big part of it but the theory is so profound and the

85
00:09:44,840 --> 00:09:50,800
path is so subtle that there has accumulated an incredible body of

86
00:09:50,800 --> 00:09:57,720
literature even just speaking of the Buddha's teachings himself and the Buddha

87
00:09:57,720 --> 00:10:01,600
didn't just say go and sit though he said that many times he also gave many

88
00:10:01,600 --> 00:10:08,760
teachings that were useful to help people to sit and to meditate in a proper

89
00:10:08,760 --> 00:10:12,520
manner and in a manner that would allow them to see things clearly and not get

90
00:10:12,520 --> 00:10:21,240
lost and distracted so it's it's perhaps a little naive to think by any

91
00:10:21,240 --> 00:10:26,320
stretch of the imagination on a single day or a half an hour in the next 10

92
00:10:26,320 --> 00:10:30,520
minutes I'm somehow going to give you an overview of the Buddha's teaching but

93
00:10:30,520 --> 00:10:36,960
we can come back a little ways towards this idea that Buddhism is just a

94
00:10:36,960 --> 00:10:43,760
teaching a practical teaching it's a teaching of getting experiential wisdom

95
00:10:43,760 --> 00:10:48,840
we don't want to give up theoretical knowledge altogether and in fact the

96
00:10:48,840 --> 00:10:56,400
more theoretical knowledge we have the better equipped we are but we have to

97
00:10:56,400 --> 00:11:01,360
find some sort of core and some sort of basis which we can then say okay if I

98
00:11:01,360 --> 00:11:04,840
practice according to these principles I'm practicing according to the

99
00:11:04,840 --> 00:11:09,200
Buddha's teaching and luckily for us of course this does exist there are a few

100
00:11:09,200 --> 00:11:13,040
ways of approaching this and one of the best ways is laid down in the Buddha's

101
00:11:13,040 --> 00:11:18,840
own words in his teachings on the four foundations of mindfulness which is a

102
00:11:18,840 --> 00:11:23,000
very common teaching that you'll find both in the Buddha's teachings and in

103
00:11:23,000 --> 00:11:28,160
those who even today spread the Buddha's teachings so not only are many

104
00:11:28,160 --> 00:11:31,240
Buddhist practicing this but you can actually find it in the Buddha's

105
00:11:31,240 --> 00:11:36,280
teachings as one of the core practical teachings where many times the Buddha

106
00:11:36,280 --> 00:11:42,760
said that if you want a very condensed version of his teachings or if you don't

107
00:11:42,760 --> 00:11:47,520
have a lot of time suppose your person who has a busy schedule or is is

108
00:11:47,520 --> 00:11:57,480
already grown up and gotten beyond the time where they could put great amounts of

109
00:11:57,480 --> 00:12:06,080
time to study then what you should do to focus your efforts is to set

110
00:12:06,080 --> 00:12:10,480
yourself up in morality first and then start practicing the four foundations of

111
00:12:10,480 --> 00:12:16,760
mindfulness to get a basic understanding of what it means to be moral sorry

112
00:12:16,760 --> 00:12:26,800
end to and get an understanding of of the truth you know get this basic

113
00:12:26,800 --> 00:12:32,560
doctrinal teaching you know what shouldn't I do and what is the ultimate goal

114
00:12:32,560 --> 00:12:37,800
or what is what is the truth just from a theoretical level then start practicing

115
00:12:37,800 --> 00:12:43,240
the four foundations of mindfulness so this is the the basics that we have to

116
00:12:43,240 --> 00:12:47,160
give a person the first theoretical thing we have to give is to teach them what

117
00:12:47,160 --> 00:12:52,360
it is to be moral and basically this means the kind of morality that you would

118
00:12:52,360 --> 00:12:59,120
find in most religious systems at least in theory if not in practice not to

119
00:12:59,120 --> 00:13:06,920
kill not to steal not to commit adultery or cheat not to lie and particularly

120
00:13:06,920 --> 00:13:11,800
the meditation teaching not to take drugs or alcohol because of course this is

121
00:13:11,800 --> 00:13:16,360
something that clouds the mind it's the heading in the opposite direction

122
00:13:16,360 --> 00:13:22,120
from mindfulness so these basic rules these are called the five precepts or the

123
00:13:22,120 --> 00:13:27,040
five training rules in Buddhism if someone can keep them then they've got the

124
00:13:27,040 --> 00:13:32,680
basics of morality so from a theoretical level it's important if we're talking

125
00:13:32,680 --> 00:13:40,480
about gaining wisdom that we start here why because these are things that the

126
00:13:40,480 --> 00:13:44,040
reason that they're considered immoral is because of the effect that they

127
00:13:44,040 --> 00:13:47,880
have on one's mind the effect that they have on one's peace of mind the

128
00:13:47,880 --> 00:13:54,160
effect they have on one's clarity of mind people who betrayed these precepts

129
00:13:54,160 --> 00:13:59,320
though it may not seem to to us readily if we're not if we're not looking

130
00:13:59,320 --> 00:14:03,480
carefully they they haven't profound effect on your mind if your person

131
00:14:03,480 --> 00:14:08,920
who's ever killed a substantial being large animals or or even worse killed a

132
00:14:08,920 --> 00:14:13,840
human being for these people they can generally readily appreciate the

133
00:14:13,840 --> 00:14:18,520
effect that it has on their mind people who steal people who cheat people who

134
00:14:18,520 --> 00:14:24,520
lie and people of course take drugs and alcohol if you're objective about it

135
00:14:24,520 --> 00:14:28,240
you can see how the the effect they have in your mind and so this is very

136
00:14:28,240 --> 00:14:35,040
important in the beginning the second thing is this understanding of

137
00:14:35,040 --> 00:14:38,480
reality or the truth from a theoretical perspective so we skip all the way to

138
00:14:38,480 --> 00:14:41,160
the end and we say what is it that we're trying to achieve or what is it that

139
00:14:41,160 --> 00:14:46,880
we're trying to gain what is the truth and of course this is something that

140
00:14:46,880 --> 00:14:52,000
you know you can you can explain on various levels of complexity but the

141
00:14:52,000 --> 00:14:59,520
easiest way to understand that the Buddha gave is that all things are indeed not

142
00:14:59,520 --> 00:15:03,280
worth clinging to that there is nothing in the world worth clinging to nothing in

143
00:15:03,280 --> 00:15:08,120
existence worth clinging to this is what we should understand is the truth

144
00:15:08,120 --> 00:15:14,280
this is something that we've if we want to say that we understand the Buddha's

145
00:15:14,280 --> 00:15:20,320
teaching the Buddha said understand this point and then you have this basic

146
00:15:20,320 --> 00:15:24,680
understanding of the Buddha's teaching you don't have to go studying the 84,000

147
00:15:24,680 --> 00:15:30,120
teachings of the Buddha understand that there is no thing and it's only an

148
00:15:30,120 --> 00:15:32,840
intellectual or it's only a theoretical understanding you don't yet

149
00:15:32,840 --> 00:15:35,720
understand if this is true or not but you understand this is what the Buddha

150
00:15:35,720 --> 00:15:39,480
taught and that's enough and you say okay this is going to be the mantra or

151
00:15:39,480 --> 00:15:43,960
the the motto you can say for the rest of my practice if I keep this in mind

152
00:15:43,960 --> 00:15:48,560
it's going to lead me along the path of the Buddha if at some point I say

153
00:15:48,560 --> 00:15:52,520
no this path is not for me then fine but at least I know this is what the Buddha

154
00:15:52,520 --> 00:15:59,840
taught that all things no thing is worth clinging to once we have these two

155
00:15:59,840 --> 00:16:03,960
foundations this is where the Buddha said okay now go on to practice the four

156
00:16:03,960 --> 00:16:08,400
foundations of mindfulness so theoretically once we have these these

157
00:16:08,400 --> 00:16:14,480
foundation teachings in our minds all we have to do is learn what are the four

158
00:16:14,480 --> 00:16:20,120
foundations of mindfulness and then start practicing so I'm going to give

159
00:16:20,120 --> 00:16:23,880
you give everyone a teaching now you're welcome to follow along you're

160
00:16:23,880 --> 00:16:29,080
welcome to just listen but the idea would be for now for all of us to stop

161
00:16:29,080 --> 00:16:36,280
texting and maybe if you can close your eyes preferably close your eyes you

162
00:16:36,280 --> 00:16:43,640
can put your avatar on busy if you get lots of texts or I am and let's start

163
00:16:43,640 --> 00:16:49,360
to learn about what is it that the Buddha taught or the four foundations of

164
00:16:49,360 --> 00:16:57,360
mindfulness and what is it that we're taught to be mindful of the first

165
00:16:57,360 --> 00:17:00,920
foundation that the Lord Buddha taught is the foundation of mindfulness of the

166
00:17:00,920 --> 00:17:07,600
body and this is the reason it's first or perhaps the best explanation is to

167
00:17:07,600 --> 00:17:13,680
why it's first is because it's the easiest to to grasp to see and this is

168
00:17:13,680 --> 00:17:18,760
agreed upon by the whole long tradition of Buddhist meditation teachers

169
00:17:18,760 --> 00:17:26,720
that the easiest and most course most obvious object of attention in terms of

170
00:17:26,720 --> 00:17:32,680
what's really there is the body and so the Buddha taught us that we should

171
00:17:32,680 --> 00:17:43,360
know the body for the body know the body as body know the body for what it is

172
00:17:43,360 --> 00:17:46,640
because as with everything else this is something that we're going to cling

173
00:17:46,640 --> 00:17:50,600
to as the Buddha said nothing in the world is worth clinging to and what we're

174
00:17:50,600 --> 00:17:56,880
going to at least test we're going to test this theory we're going to do a

175
00:17:56,880 --> 00:18:01,840
series of tests and experiments to see whether this is true and so we're going

176
00:18:01,840 --> 00:18:06,440
to learn about how we cling to things like the body and we're going to see

177
00:18:06,440 --> 00:18:12,000
whether that's actually of any benefit to us and with the idea that

178
00:18:12,000 --> 00:18:17,280
according to the Lord Buddha there was no benefit from clinging so we're going

179
00:18:17,280 --> 00:18:24,320
to watch the body how we how we do this how we practice all four foundations

180
00:18:24,320 --> 00:18:30,000
of mindfulness according to the teaching that I profess and of course

181
00:18:30,000 --> 00:18:34,800
there are many interpretations of what is meant by being mindful of these four

182
00:18:34,800 --> 00:18:40,240
things but according to the teaching that that I have we use a mantra which is

183
00:18:40,240 --> 00:18:45,120
similar to any ancient Indian meditation technique where you have a special

184
00:18:45,120 --> 00:18:51,240
mantra which focuses your attention on the object or on the thing on the

185
00:18:51,240 --> 00:18:56,160
phenomenon which is summed up by the mantra so it's usually something like God

186
00:18:56,160 --> 00:19:05,040
or some special experience or special state or special object in Buddhist

187
00:19:05,040 --> 00:19:10,600
meditation of course our object is reality because we want to see things as

188
00:19:10,600 --> 00:19:16,000
they are and as I said we want to learn about our clinging to things and we want

189
00:19:16,000 --> 00:19:19,840
to come to see the truth that nothing is worth clinging to because when we see

190
00:19:19,840 --> 00:19:23,560
that of course we won't cling to anything when we don't cling we'll be free

191
00:19:23,560 --> 00:19:32,560
we'll be like a bird flying and attached to anything so the our use of the

192
00:19:32,560 --> 00:19:36,200
mantra is the same same technique but a different object it's going to be a

193
00:19:36,200 --> 00:19:41,120
very ordinary object and the first one is the body so the technique that we

194
00:19:41,120 --> 00:19:44,640
often give to people is to start watching at the stomach because the most

195
00:19:44,640 --> 00:19:48,880
obvious part of the body when you're sitting still and not doing anything is the

196
00:19:48,880 --> 00:19:54,760
movement of the stomach that rises and falls as the breath goes in and out

197
00:19:54,760 --> 00:20:00,240
for many people nowadays this isn't that obvious because nowadays we have a lot

198
00:20:00,240 --> 00:20:04,840
of stresses that probably weren't apparent in the time of the Buddha or at

199
00:20:04,840 --> 00:20:09,200
least for monks meditating off in the forest but if it's not readily apparent

200
00:20:09,200 --> 00:20:13,440
in the beginning I guarantee you that it will be as you go on as you get more

201
00:20:13,440 --> 00:20:19,640
proficient in the practice as your breath calms down as your body loosens up

202
00:20:19,640 --> 00:20:24,760
it will become very very obvious because your breath will become more natural if

203
00:20:24,760 --> 00:20:28,720
you want to see this is true you can lie in your back if you lie in your back and

204
00:20:28,720 --> 00:20:33,920
just feel your your breath you'll see that you're just like a baby again and your

205
00:20:33,920 --> 00:20:38,440
belly is indeed rising and falling naturally but the problem is when we sit up

206
00:20:38,440 --> 00:20:43,240
we become all tense and stressed and get back into this forest unnatural

207
00:20:43,240 --> 00:20:48,600
state and so our breath becomes shallow and and and based in the chest and so

208
00:20:48,600 --> 00:20:55,640
if you want the best way to to bridge the gap is to put your hand on your

209
00:20:55,640 --> 00:21:01,960
stomach and just watch it or feel it rising and falling with your hand as it

210
00:21:01,960 --> 00:21:08,240
rises we're just going to use this mantra rising we just say to ourselves

211
00:21:08,240 --> 00:21:15,440
rising and when it falls we say falling

212
00:21:15,440 --> 00:21:22,080
rising

213
00:21:22,080 --> 00:21:25,880
falling and we don't say it out loud we're just saying it in our mind and

214
00:21:25,880 --> 00:21:33,520
our mind is at the stomach as we do this we're getting into the second type of

215
00:21:33,520 --> 00:21:39,320
wisdom which is wisdom sort of the mental exercise putting into practice the

216
00:21:39,320 --> 00:21:44,360
things that we've learned and and trying to understand how they work so as we

217
00:21:44,360 --> 00:21:49,480
say to ourselves rising it's a sort of wisdom it's a affirmation of the

218
00:21:49,480 --> 00:21:53,800
truth of that experience it's not the highest sort of wisdom yet but it's a

219
00:21:53,800 --> 00:22:01,160
understanding of things as they are this is rising it's putting into practice

220
00:22:01,160 --> 00:22:07,840
the theory that we've learned testing or thinking about or experimenting on

221
00:22:07,840 --> 00:22:26,680
that theory

222
00:22:37,840 --> 00:22:45,480
now as we start to watch the body as I said they're all together four

223
00:22:45,480 --> 00:22:49,320
foundations and the reason there are is because our mind doesn't stay with the

224
00:22:49,320 --> 00:22:56,600
body all the time it wonders it goes here and there and everywhere another

225
00:22:56,600 --> 00:23:00,840
thing about meditation is this sort of meditation is according to the

226
00:23:00,840 --> 00:23:04,120
teaching that nothing is worth clinging to we're also not trying to cling to

227
00:23:04,120 --> 00:23:07,800
the meditation object or trying to cling to the idea that our mind has to be

228
00:23:07,800 --> 00:23:13,280
stationary we're going to watch the mind as it is and we're going to learn and

229
00:23:13,280 --> 00:23:17,480
not cling and because we're going to start to see that clinging is the reason

230
00:23:17,480 --> 00:23:24,600
our mind is so flustered and confused and and scattered as it is so when our

231
00:23:24,600 --> 00:23:28,800
mind does wonder we're going to focus on the other various objects of its

232
00:23:28,800 --> 00:23:37,560
attention the next one being feelings that arise so when we're sitting watching

233
00:23:37,560 --> 00:23:42,640
the rising and falling we find our mind wandering away to certain sensations

234
00:23:42,640 --> 00:23:48,240
that arise in the body and in the mind pleasant sensations unpleasant sensations

235
00:23:48,240 --> 00:23:59,280
or neutral comms sensations many times sitting cross-legged in meditation

236
00:23:59,280 --> 00:24:04,960
we'll find pain arising in the back or in the legs sometimes we'll find pain in

237
00:24:04,960 --> 00:24:12,640
the head if we're if we think a lot of work hard every day so instead of

238
00:24:12,640 --> 00:24:18,000
seeing these as a distraction or an obstacle towards our practice we should

239
00:24:18,000 --> 00:24:21,880
focus them on them and use them as as our meditation practice they're part of

240
00:24:21,880 --> 00:24:26,240
reality and there's something that it's very common for us to cling to it's

241
00:24:26,240 --> 00:24:28,680
generally pretty obvious that we're clinging to them we're saying this is

242
00:24:28,680 --> 00:24:37,440
bad don't want this want this to leave me to go away so here instead of doing

243
00:24:37,440 --> 00:24:43,360
that we're going to say to ourselves pain or aching or sore or however just

244
00:24:43,360 --> 00:24:48,120
reminding ourselves of that of what it is we use this mantra it's going to focus

245
00:24:48,120 --> 00:24:52,960
objectively on the object not seeing it as good or bad just seeing it for what

246
00:24:52,960 --> 00:25:05,680
it is with wisdom so we say to ourselves pain pain pain pain and till it goes

247
00:25:05,680 --> 00:25:13,320
away if we feel happy we do the same we don't want to cling to happy

248
00:25:13,320 --> 00:25:18,200
ness either there's nothing wrong with happiness in fact most people would

249
00:25:18,200 --> 00:25:21,880
agree that happiness is a good thing but what's what's really not a good thing is

250
00:25:21,880 --> 00:25:26,440
our clinging to it we're saying oh I hope this stays for a long time I hope

251
00:25:26,440 --> 00:25:31,800
this is permanent because it doesn't stay for a long time it isn't permanent it

252
00:25:31,800 --> 00:25:37,680
isn't subject to our control so we should just know that we're happy and just

253
00:25:37,680 --> 00:25:43,960
be happy not clinging to it not judging it not giving rise to anything beyond

254
00:25:43,960 --> 00:26:10,040
just a knowledge that this is happiness say to ourselves happy happy happy

255
00:26:13,960 --> 00:26:31,520
if we feel calm the same thing many people as they practice as their

256
00:26:31,520 --> 00:26:36,840
practice progresses they'll feel calm they'll feel quiet and they'll find that

257
00:26:36,840 --> 00:26:39,960
they aren't able to meditate any longer because all that's left is a sense of

258
00:26:39,960 --> 00:26:47,920
quiet and peace but the Buddha was very clever and was very clear in pointing

259
00:26:47,920 --> 00:26:53,160
absolutely everything that we were to be mindful of that that could arise that

260
00:26:53,160 --> 00:26:58,240
includes peaceful feelings and so when we feel peaceful we should say to

261
00:26:58,240 --> 00:27:04,000
ourselves as well calm calm or peaceful peaceful

262
00:27:04,000 --> 00:27:12,280
this is the second foundation the third foundation is the foundation of the

263
00:27:12,280 --> 00:27:17,960
mind focusing on the mind itself as we practice we'll see the next thing the next

264
00:27:17,960 --> 00:27:23,160
problem is that we're thinking and we say oh this is a big problem because we're

265
00:27:23,160 --> 00:27:28,280
no longer focusing on our object now instead we're thinking remind us wandering

266
00:27:28,280 --> 00:27:34,800
away from the object but the great thing here as I said is that that can be

267
00:27:34,800 --> 00:27:39,920
that as well can be the object the mind can whatever can be the object whatever

268
00:27:39,920 --> 00:27:44,200
arises can be the object of our intention so when we think we can say to ourselves

269
00:27:44,200 --> 00:27:48,760
thinking thinking whatever we're thinking about instead of turning it into a

270
00:27:48,760 --> 00:27:55,520
huge issue a big deal making more of it than than just what it is we simply see

271
00:27:55,520 --> 00:28:07,080
it as thinking thinking thinking and the final one is our emotions and the final

272
00:28:07,080 --> 00:28:11,000
one actually as many many things but I'm just gonna simplify it down to our

273
00:28:11,000 --> 00:28:15,800
emotions it's many of the various other teachings of the Buddha which we

274
00:28:15,800 --> 00:28:20,280
should start to focus in on actually the first three are the main foundations

275
00:28:20,280 --> 00:28:24,240
the fourth one is things that we're going to have to focus in on our practice

276
00:28:24,240 --> 00:28:31,680
becomes more focused then simply anything we're going to focus on the

277
00:28:31,680 --> 00:28:36,720
important the salient points of our existence the first one is our hindrances

278
00:28:36,720 --> 00:28:42,760
the the things that you're going to get in the way of our practice when we

279
00:28:42,760 --> 00:28:49,280
like something when we don't like something when we feel angry or bored or

280
00:28:49,280 --> 00:28:55,760
frustrated or sad or depressed all of these negative emotions even including

281
00:28:55,760 --> 00:29:03,240
liking or or enjoy desire or lust or so and we consider them to be negative in

282
00:29:03,240 --> 00:29:09,920
the sense that they are judgments they're clinging and we're not going to judge

283
00:29:09,920 --> 00:29:13,800
these things we're just going we should understand that these are five things

284
00:29:13,800 --> 00:29:18,720
which are actually going to change our state of mind and take us out of the

285
00:29:18,720 --> 00:29:26,640
meditative state if we give power to these emotions then we'll find our

286
00:29:26,640 --> 00:29:31,680
mindfulness dissipating so it's important that not that we judge these not

287
00:29:31,680 --> 00:29:35,520
that we feel guilty about them when we say bad bad bad but that is with

288
00:29:35,520 --> 00:29:39,560
everything else we we pick up these as well as I sort of especially pick these

289
00:29:39,560 --> 00:29:45,000
up keep them in our radar when they arise be very quick to catch them just

290
00:29:45,000 --> 00:29:50,520
like everything else so when you like something say to yourself liking liking or

291
00:29:50,520 --> 00:29:58,640
wanting when you don't like something say disliking disliking simply knowing

292
00:29:58,640 --> 00:30:01,680
that now you're disliking we're going to see it for what it is if it's a good

293
00:30:01,680 --> 00:30:06,040
thing to like or dislike something then we'll know it we'll have wisdom we'll

294
00:30:06,040 --> 00:30:14,320
have understanding if we feel bored if we feel distracted

295
00:30:14,320 --> 00:30:21,840
if we feel sad or depressed we should just say to ourselves sad sad depressed

296
00:30:21,840 --> 00:30:24,440
depressed

297
00:30:27,960 --> 00:30:33,920
if we if we feel drowsy or lazy or tired we should simply know what for what it

298
00:30:33,920 --> 00:30:40,560
is just see if what it is not getting upset about it but just saying okay now we

299
00:30:40,560 --> 00:30:46,360
have this emotion in our minds now we have this state of mind we say to ourselves

300
00:30:46,360 --> 00:30:51,920
drowsy drowsy drowsy tired tired these are things which are going to get in the

301
00:30:51,920 --> 00:30:56,560
way of our practice very clearly and from the very very beginning so they're

302
00:30:56,560 --> 00:31:02,640
important for especially beginner meditators and the final one that the Buddha

303
00:31:02,640 --> 00:31:07,520
mentioned was doubt if you have doubt in your minds doubt about yourself whether

304
00:31:07,520 --> 00:31:10,360
you can do this practice doubt about the practice itself whether it's

305
00:31:10,360 --> 00:31:15,400
right then his advice for you is to practice doubt practice focusing on the

306
00:31:15,400 --> 00:31:20,200
doubt practice learning about the doubt itself give up the idea that you're

307
00:31:20,200 --> 00:31:23,040
practicing anything and just look at the doubt and say to yourself what is

308
00:31:23,040 --> 00:31:28,200
doubt make that your practice because once you learn about doubt and give up the

309
00:31:28,200 --> 00:31:32,840
doubt then there's nothing that you need to practice this is really in one

310
00:31:32,840 --> 00:31:38,240
sense this is the teaching of the Buddha that you come to see you come to give up

311
00:31:38,240 --> 00:31:42,080
your doubt you come to see everything for what it is and not have anything left

312
00:31:42,080 --> 00:31:46,640
to doubt there's no other practice beside that there's nothing else that you

313
00:31:46,640 --> 00:31:50,120
have to achieve

314
00:31:50,120 --> 00:32:02,560
when there's nothing left to be mindful of we just always come back to our

315
00:32:02,560 --> 00:32:06,880
base meditation of being mindful of the rising and the falling because it's

316
00:32:06,880 --> 00:32:12,360
always there the stomach rising and falling rising and falling we don't have to

317
00:32:12,360 --> 00:32:16,360
go looking for other objects when they come up we pay attention to them when

318
00:32:16,360 --> 00:32:20,720
they're not there we come back to the body it can really be any part of the

319
00:32:20,720 --> 00:32:24,120
body you can focus on the walking when you're walking around saying to yourself

320
00:32:24,120 --> 00:32:30,600
walking walking or someone when you're sitting still it's generally easiest to

321
00:32:30,600 --> 00:32:39,160
focus on the stomach because it's very clear it's always there so the

322
00:32:39,160 --> 00:32:42,680
practice is another type of wisdom it's it's a putting into practice it's

323
00:32:42,680 --> 00:32:48,160
getting the skills it's an intellectual exercise but what comes from the

324
00:32:48,160 --> 00:32:51,280
practice this is true is that this is really what we're getting we're trying to

325
00:32:51,280 --> 00:32:54,880
gain we're going to start seeing things about ourselves that we didn't see

326
00:32:54,880 --> 00:32:58,880
before we're going to learn things about ourselves that we didn't know before

327
00:32:58,880 --> 00:33:04,760
we're going to give up things that we thought were inherent or intrinsic to

328
00:33:04,760 --> 00:33:08,080
who we were and we'll give them up because we'll see that they're useless

329
00:33:08,080 --> 00:33:13,120
they're unhelpful they're unbeneficial they're not under our control and they

330
00:33:13,120 --> 00:33:16,960
have no use whatsoever and we're going to see that we're better off without

331
00:33:16,960 --> 00:33:24,040
them when we see that we're better off without them this is true wisdom and

332
00:33:24,040 --> 00:33:31,920
it's it's a fact it's a fact of life that's simply knowing that it's wrong for

333
00:33:31,920 --> 00:33:37,680
you when you really know that something is unhelpful that you do give it up the

334
00:33:37,680 --> 00:33:40,520
problem is we always say this is bad for me that's bad for me we don't

335
00:33:40,520 --> 00:33:45,920
really know we actually still feel pleasure and attachment to it we still

336
00:33:45,920 --> 00:33:50,360
think have this idea that it's somehow good for us when we see deeply that

337
00:33:50,360 --> 00:33:56,160
things are not worth clinging to then we actually don't cling to them we do

338
00:33:56,160 --> 00:34:01,280
follow our wisdom we do follow our knowledge it's just that most of our

339
00:34:01,280 --> 00:34:05,600
knowledge is only theoretical it's a sort of false knowledge we say this is

340
00:34:05,600 --> 00:34:09,360
good and this is bad but we don't really have any understanding of wisdom

341
00:34:09,360 --> 00:34:16,920
that is able to tell us that this is good or this is bad so here I've I've given

342
00:34:16,920 --> 00:34:21,680
a talk a little bit longer than I normally would but I sort of intended to do

343
00:34:21,680 --> 00:34:26,920
so thinking that it was going to also be a meditation practice and therefore

344
00:34:26,920 --> 00:34:31,760
cut into some of our question and answer time but now of course we have time

345
00:34:31,760 --> 00:34:38,000
for discussion questions and answer and probably I'm I'm I'm happy to be here

346
00:34:38,000 --> 00:34:46,560
after one one o'clock if there's still people liking or hanging on so I'll stop

347
00:34:46,560 --> 00:34:50,600
there and I'd like to thank you all for coming if you have any questions I'm

348
00:34:50,600 --> 00:35:10,000
happy to answer them via text from here on

