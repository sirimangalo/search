1
00:00:00,000 --> 00:00:10,160
Okay, so today we continue our study of the Damapanda with verse number 16, which reads

2
00:00:10,160 --> 00:00:21,400
as far as, in the mode of the page, a mode of the theme, kata pungyo, uba yata, mode of the theme.

3
00:00:21,400 --> 00:00:35,800
So the mode of the theme, this wa kama, visudimatana, which means here he rejoices, here

4
00:00:35,800 --> 00:00:44,320
after he rejoices, and the kata pungyo, the person the doer of good deeds, rejoices

5
00:00:44,320 --> 00:00:46,560
in both places.

6
00:00:46,560 --> 00:00:58,800
He thoroughly rejoices, having seen the purity of his own actions, and this is the companion

7
00:00:58,800 --> 00:01:04,400
verse 2, our last verse 15, but the story is different.

8
00:01:04,400 --> 00:01:15,280
The story here is about a good doer, a story of Damika Ubasika, Ubasika, who was Damika

9
00:01:15,280 --> 00:01:22,720
means one who lives by the number, Ubasika is a person who follows the teaching, who takes

10
00:01:22,720 --> 00:01:25,720
refuge.

11
00:01:25,720 --> 00:01:33,680
And Damika Ubasika was a follower of the Buddha, who was very much involved with the Buddha

12
00:01:33,680 --> 00:01:34,680
teaching.

13
00:01:34,680 --> 00:01:38,760
He was very keen on it and interested in it, and so he would always go to listen to

14
00:01:38,760 --> 00:01:43,760
the Buddha's teaching, listen to the sermons again and again, invite monks to his house

15
00:01:43,760 --> 00:01:49,720
to give pre-chings, and he would be always giving alms to the monks, giving food to the monks

16
00:01:49,720 --> 00:01:55,840
when they came, and he was very much looking after them, so always doing good deeds.

17
00:01:55,840 --> 00:02:01,960
He was established in morality, and I believe he was also a meditator.

18
00:02:01,960 --> 00:02:08,160
He must have very well been because he was very much interested in the Buddha's teaching

19
00:02:08,160 --> 00:02:12,320
on Satipatana, as the story goes.

20
00:02:12,320 --> 00:02:16,240
So as I understand, he may have even been a sotipana, but I'm not sure about that.

21
00:02:16,240 --> 00:02:17,240
I can't quite remember.

22
00:02:17,240 --> 00:02:27,720
Anyway, we can consider him to be a very good Buddhist in all senses of the word.

23
00:02:27,720 --> 00:02:34,360
And so it happened that he as well, after many years, he became ill and was on his deathbed,

24
00:02:34,360 --> 00:02:41,040
just like Junda, the poor butcher, but when he was on his deathbed, rather than becoming

25
00:02:41,040 --> 00:02:49,280
deranged or having his bad past deeds overwhelming, his good deeds came into him.

26
00:02:49,280 --> 00:02:52,480
And right away, he thought to do more good deeds.

27
00:02:52,480 --> 00:02:58,920
So when he knew that he was probably at his last moment, he sent a message to the Buddha

28
00:02:58,920 --> 00:03:00,920
asking for monks to come.

29
00:03:00,920 --> 00:03:05,200
They sent some monks, I would like to hear the dhamma figure, the best way to go out is

30
00:03:05,200 --> 00:03:10,240
listening to the dhamma and practicing according to the teaching.

31
00:03:10,240 --> 00:03:14,440
So the Buddha right away sent monks, and they came and they sat around his bed, and they

32
00:03:14,440 --> 00:03:19,560
asked him, Obasaka, Obasaka, what teaching would you like to hear?

33
00:03:19,560 --> 00:03:24,680
And he said, please recite for me the Satipatana sutra, because it's the teaching on all

34
00:03:24,680 --> 00:03:25,680
the Buddha's.

35
00:03:25,680 --> 00:03:31,520
So it's clear that it's very much clear, very much interested in meditation practice.

36
00:03:31,520 --> 00:03:38,600
Satipatana sutra is what our meditation practice is based on, it talks about the four foundations

37
00:03:38,600 --> 00:03:43,480
of mindfulness, the body, the feelings, the mind, and the number.

38
00:03:43,480 --> 00:03:49,480
So if you haven't read it, I think you probably all have read it, but if you haven't

39
00:03:49,480 --> 00:03:53,480
read it, just explain it, talks about the body.

40
00:03:53,480 --> 00:03:56,960
So how when you're walking, you know you're walking when the breath comes in, you know

41
00:03:56,960 --> 00:04:02,880
the breath is coming in, for instance, rising and falling, being aware of the physical aspects

42
00:04:02,880 --> 00:04:07,800
of the body, the movements of the body, when the stomach moves, when the foot moves, when

43
00:04:07,800 --> 00:04:13,880
the hand moves, even when you're going to the washroom and you're eating, whatever you're

44
00:04:13,880 --> 00:04:17,800
doing during the day, it talks about being mindful of the body, knowing it for what it

45
00:04:17,800 --> 00:04:18,800
is.

46
00:04:18,800 --> 00:04:23,440
When you have feelings, you know the feelings for what it is, pain, aching or soreness

47
00:04:23,440 --> 00:04:26,640
when you have thoughts.

48
00:04:26,640 --> 00:04:30,480
And when you have emotions, the dumb mother liking, just liking and so on, this is what

49
00:04:30,480 --> 00:04:31,480
it talks about.

50
00:04:31,480 --> 00:04:32,920
So they began to recite this.

51
00:04:32,920 --> 00:04:38,560
They started with the beginning, it was a cayenneau yong bikho a mungu.

52
00:04:38,560 --> 00:04:44,400
This is the only way among, so this is the one way among, for the purification of beings

53
00:04:44,400 --> 00:04:51,200
for the overcoming of sorrow, lamentation, despair, for the destruction of bodily and mental

54
00:04:51,200 --> 00:04:57,600
suffering, for attaining the right path and for realizing freedom.

55
00:04:57,600 --> 00:05:02,880
And so it's a very profound discourse, it's one that even today they will often recite

56
00:05:02,880 --> 00:05:08,640
for people who are dying or recite just in general in meditation centers to remind them

57
00:05:08,640 --> 00:05:10,600
of teachings.

58
00:05:10,600 --> 00:05:14,280
So at this time he would be listening to me, I will also be practicing.

59
00:05:14,280 --> 00:05:24,360
Now as he was on his way out, and because he was actually such a pure and wonderful person,

60
00:05:24,360 --> 00:05:28,760
the story goes, and this is where many people get turned off because some of these stories

61
00:05:28,760 --> 00:05:31,040
are a little bit fantastical.

62
00:05:31,040 --> 00:05:34,120
So whether they're true or not, I'll leave it up to you to decide.

63
00:05:34,120 --> 00:05:38,840
With the point that the truth of this, the fantastical parts doesn't really affect the

64
00:05:38,840 --> 00:05:46,480
message at all, and it's our meditation that verifies or denies the message, which in

65
00:05:46,480 --> 00:05:50,880
this case is that a good person has good things happen to me.

66
00:05:50,880 --> 00:05:57,800
But it said that he had visions, suddenly had visions of angels coming from all of the

67
00:05:57,800 --> 00:06:04,920
six levels of essential heaven when they came in chariots, and they all began to call

68
00:06:04,920 --> 00:06:05,920
to him.

69
00:06:05,920 --> 00:06:10,120
Please be born in my realm, be born in and because they all wanted this guy, I was kind

70
00:06:10,120 --> 00:06:18,520
of like a baseball draft or something, you know, wanted this guy on their team.

71
00:06:18,520 --> 00:06:25,320
And so they were all extolling the virtues of their heaven, and then he should be

72
00:06:25,320 --> 00:06:28,360
born there.

73
00:06:28,360 --> 00:06:31,640
And nobody else could hear or see them, it was only a vision that he had, and so he

74
00:06:31,640 --> 00:06:41,320
said to them, wait, wait, and his children heard this wait, wait, and he's kind of distracted

75
00:06:41,320 --> 00:06:47,040
look like he was talking to himself that he was saying, wait, wait, and they thought he

76
00:06:47,040 --> 00:06:50,600
must be telling the monks to stop the monks where they're chanting and suddenly he interrupts

77
00:06:50,600 --> 00:06:55,680
them and says, wait, wait, wait, and so the monks suddenly stop chanting, and they listen

78
00:06:55,680 --> 00:07:01,800
to me, okay, he wants us to wait, let's wait, let's be respected this guy, and the children

79
00:07:01,800 --> 00:07:07,640
started crying and they were horrified because they had been taught by him to that,

80
00:07:07,640 --> 00:07:14,320
you know, the importance of listening to the dhamma, and so they thought, well he must

81
00:07:14,320 --> 00:07:19,360
be totally deranged or he must be experiencing something terrible and it would be afraid

82
00:07:19,360 --> 00:07:24,480
or so on, and he doesn't want the monks to teach the dhamma, and so they began crying,

83
00:07:24,480 --> 00:07:31,640
they said, truly no one is immune to this, even our own father who we thought was so mindful,

84
00:07:31,640 --> 00:07:37,280
and so the monks saw them crying and the old men saying, wait, wait, they didn't, they

85
00:07:37,280 --> 00:07:44,200
said nothing for us to do so, they all left, and after they left, the old men said, where

86
00:07:44,200 --> 00:07:48,080
did the monks go, why did they stop chanting, and they said, they said, you told them

87
00:07:48,080 --> 00:07:57,520
to wait, and we were all horrified that we thought you were, we realized that you also

88
00:07:57,520 --> 00:08:02,160
are becoming a little bit deranged here, and he said, that's not why, I wasn't telling

89
00:08:02,160 --> 00:08:06,280
the monks to me, and I was telling these angels here to wait, don't come from me yet,

90
00:08:06,280 --> 00:08:12,280
I'm still listening to the dhamma, and they said, what, chariots, we don't see them, and

91
00:08:12,280 --> 00:08:16,960
then the story goes that he said he had them take a wreath of flowers and throw it up

92
00:08:16,960 --> 00:08:22,240
in the air, and it landed on one of the chariots, and it just hung there in the air, and

93
00:08:22,240 --> 00:08:26,760
he said, that's the heaven that I'm going to be born in, the two seat that heaven, and

94
00:08:26,760 --> 00:08:30,920
then he passed away, and when he passed away he entered into the chariot and went to

95
00:08:30,920 --> 00:08:34,920
heaven, this is how the story fell.

96
00:08:34,920 --> 00:08:39,000
So you can imagine that it may have been fancified, it may not have been, maybe true that

97
00:08:39,000 --> 00:08:44,160
this kind of thing happens, I'm open-minded about it, but it's not really important,

98
00:08:44,160 --> 00:08:50,960
but we can understand, even based on the last one is on two levels, a person who does

99
00:08:50,960 --> 00:08:56,560
good deeds, has good things happen to them, and a person who does bad things, has bad

100
00:08:56,560 --> 00:09:02,120
things happen, or becomes sufferers, sochity, and modity, sochity means sorrows, and

101
00:09:02,120 --> 00:09:08,720
modity means rejoices, or feels great pleasure for them.

102
00:09:08,720 --> 00:09:11,920
So, and when the monks went back to the monastery, they said to the Buddha, they said,

103
00:09:11,920 --> 00:09:16,440
you know, we tried, but sad thing is, he got deranged at the last moment, and he actually

104
00:09:16,440 --> 00:09:21,320
told us to stop teaching the dhamma, and the Buddha said, that's not why he, the Buddha

105
00:09:21,320 --> 00:09:28,880
explained from exactly what happened, and then he said, and then they said, wow, he was

106
00:09:28,880 --> 00:09:33,240
so happy here on earth, it's amazing, he was so happy here on earth, and now he's even

107
00:09:33,240 --> 00:09:37,600
happier up in heaven, how wonderful, and the Buddha said, this is the way it goes, a person

108
00:09:37,600 --> 00:09:43,480
who does good deeds, when he recited the verse.

109
00:09:43,480 --> 00:09:50,560
So this is another thing, it's also something that is often invisible to us, because

110
00:09:50,560 --> 00:09:54,560
we take it in a very shallow sense, so you'll often see people who have bad lives, who

111
00:09:54,560 --> 00:10:00,680
have bad things happening to them, because of the nature of their lives, and they want

112
00:10:00,680 --> 00:10:04,280
to take to do good deeds, thinking that it's going to get rid of all of their suffering,

113
00:10:04,280 --> 00:10:07,720
and then they're disappointed that it doesn't, and they feel like they've been betrayed

114
00:10:07,720 --> 00:10:14,360
and so on, because they have this incredibly shallow understanding of how good deeds work,

115
00:10:14,360 --> 00:10:19,480
but you do have those people who understand how good deeds work, and who really feel the

116
00:10:19,480 --> 00:10:26,640
benefit of them, and understand how wonderful it is to, for instance, give a gift, I

117
00:10:26,640 --> 00:10:34,560
have a story of a woman I knew, a girl who were teenagers, and we were walking down the

118
00:10:34,560 --> 00:10:40,480
street in the middle of winter, and suddenly she was walking side by side, suddenly she

119
00:10:40,480 --> 00:10:45,200
turned and went into a McDonald's, and she said, wait here, so I'm waiting outside,

120
00:10:45,200 --> 00:10:51,280
and there's this beggar right in front of this guy on the, living on a piece of cardboard

121
00:10:51,280 --> 00:10:52,840
outside the McDonald's.

122
00:10:52,840 --> 00:10:57,680
She comes out with a hamburger, and I know she doesn't eat meat, and she hands it to this,

123
00:10:57,680 --> 00:11:03,040
she bends down and hands it to this guy sitting on the cardboard and says, here you go,

124
00:11:03,040 --> 00:11:06,240
and I'm standing there kind of feeling ashamed of myself, I didn't even see the guy until

125
00:11:06,240 --> 00:11:11,360
she went in, didn't think anything of it, but right away, and then we started walking

126
00:11:11,360 --> 00:11:18,120
and getting right away, she said to me, she said, I don't give to people, I don't give

127
00:11:18,120 --> 00:11:25,240
charity because I want them to feel happy, I do it because I'm selfish, I give gifts

128
00:11:25,240 --> 00:11:27,720
because it makes me feel happy.

129
00:11:27,720 --> 00:11:31,320
This is a person who isn't religious at all, and I don't know where she is now, but

130
00:11:31,320 --> 00:11:37,800
she's just a person who has really felt the goodness of giving, the goodness of charity,

131
00:11:37,800 --> 00:11:43,040
and I think people who are engaged in giving do feel this, because it's a pleasure that

132
00:11:43,040 --> 00:11:49,720
is unadulterated, it's a pleasure that when you have this pleasure, there's nothing

133
00:11:49,720 --> 00:11:55,040
tainting it or contaminating it, and it's also something that lasts with you forever,

134
00:11:55,040 --> 00:11:59,720
it's something that's never going to be taken away, whereas pleasure based on sensuality

135
00:11:59,720 --> 00:12:03,320
is totally dependent on the object of the sense.

136
00:12:03,320 --> 00:12:08,160
When a person remembers about the good deeds that they have done, they can repeatedly gain

137
00:12:08,160 --> 00:12:16,480
this happiness and this peace, so it's something that people I think misunderstand and

138
00:12:16,480 --> 00:12:20,200
it's a part of how people misunderstand karma as being some magical thing that you can

139
00:12:20,200 --> 00:12:29,920
just give money in a box and magically become rich or so on, or give gifts and suddenly

140
00:12:29,920 --> 00:12:35,880
you are happy, but people who do it continuously and who engage with it as a practice,

141
00:12:35,880 --> 00:12:41,840
giving or morality, morality is another one, people feel like morality is a waste of time

142
00:12:41,840 --> 00:12:46,320
or it's just a lot of suffering to not do things that you want to do, you have a mosquito,

143
00:12:46,320 --> 00:12:48,760
why not just kill it and so on.

144
00:12:48,760 --> 00:12:53,080
The people who do it and most importantly, people who engage in meditation are able to

145
00:12:53,080 --> 00:12:57,840
see the difference, and so this is what you should all be seeing right now, that the good

146
00:12:57,840 --> 00:13:02,680
and the bad things in your mind have really very much contributed to how you now relate

147
00:13:02,680 --> 00:13:08,320
to the present moment, how you react to things, and through the meditation you begin

148
00:13:08,320 --> 00:13:13,200
to see the importance of doing good deeds, the importance of being generous, the importance

149
00:13:13,200 --> 00:13:19,040
of being moral, and the importance of practicing of developing your mind, of strengthening

150
00:13:19,040 --> 00:13:23,840
your mind so that you can see things as they are, as you can see the weaknesses in our

151
00:13:23,840 --> 00:13:29,320
mind, the greed, the anger, the delusion, how it's causing us great suffering, our

152
00:13:29,320 --> 00:13:38,640
skin, our stinginess, our jealousy, and so on, so it's actually easier to see how this

153
00:13:38,640 --> 00:13:43,320
works in the present life, even though most people don't see it, but through a little

154
00:13:43,320 --> 00:13:45,880
bit of meditation you can see how it happens.

155
00:13:45,880 --> 00:13:50,240
Now we have to kind of, people say take a leap of faith to think about how it's going

156
00:13:50,240 --> 00:13:53,920
to happen in the next night, just like with the last one, is it really true that this

157
00:13:53,920 --> 00:14:00,240
man went to heaven and not man went to hell and so on, but it's ameliorated and it's

158
00:14:00,240 --> 00:14:05,480
actually really done away with this idea of taking a leap of faith in regards to what

159
00:14:05,480 --> 00:14:12,440
happens when we die, by understanding, by really appreciating the experience that we're

160
00:14:12,440 --> 00:14:17,960
having in meditation and internalizing it, then realizing that this is reality, when we

161
00:14:17,960 --> 00:14:22,760
look around us and we see there's this building, there's the trees and so on, we see other

162
00:14:22,760 --> 00:14:26,680
people, we're actually just building these constructs up in our mind.

163
00:14:26,680 --> 00:14:30,360
The truth is that there's experience, now you're hearing, now you're seeing, now you're

164
00:14:30,360 --> 00:14:34,360
smelling, now you're tasting, now you're feeling and now you're thinking.

165
00:14:34,360 --> 00:14:39,800
So at the moment of death, the point is that this doesn't change, the theory is that

166
00:14:39,800 --> 00:14:44,120
that we subscribe to is that this doesn't change because there's no reason for it to

167
00:14:44,120 --> 00:14:45,120
change.

168
00:14:45,120 --> 00:14:50,840
The physical aspect of our reality is really just a part of experience, moment and moment

169
00:14:50,840 --> 00:14:55,040
and moment of experience, that continues on.

170
00:14:55,040 --> 00:15:00,120
It really helps to make sense of why the universe is the way it is, it's very much based

171
00:15:00,120 --> 00:15:05,640
on past causes and conditions and so the future as well will be based on these same causes

172
00:15:05,640 --> 00:15:06,640
and conditions.

173
00:15:06,640 --> 00:15:13,600
If our mind is impure, as the Buddha says, suffering follows us, if our mind is pure

174
00:15:13,600 --> 00:15:15,560
then happiness follows us.

175
00:15:15,560 --> 00:15:21,480
So this is how we understand that a person does good deeds, benefits both in this life

176
00:15:21,480 --> 00:15:26,680
and in the next and it's very much a part of our practice of meditation, the practice of

177
00:15:26,680 --> 00:15:32,120
meditation, we're purifying our minds, overcoming greed and desire to gain this and that

178
00:15:32,120 --> 00:15:36,360
because we're understanding that it's a cause for suffering and we're purifying, the word

179
00:15:36,360 --> 00:15:42,120
he uses is, come up with sudhing up the more, we sudhing in purity, so it's the purity

180
00:15:42,120 --> 00:15:48,920
of our acts, an act as pure or as defiled as, I think you all are aware, based on the intentions

181
00:15:48,920 --> 00:15:49,920
of the mind.

182
00:15:49,920 --> 00:15:55,520
So even if you walk, you can walk with an impure mind, you can speak, when you speak,

183
00:15:55,520 --> 00:16:01,000
you can speak with a pure and impure mind and obviously the practice of meditation is what's

184
00:16:01,000 --> 00:16:07,160
allowing us to see purity and impurity and to refine our behavior so that our reactions

185
00:16:07,160 --> 00:16:11,440
to things, this is really what you're doing because again and again you're reacting to things

186
00:16:11,440 --> 00:16:16,160
and you'll see your reaction to things and you'll slowly refine those reactions until

187
00:16:16,160 --> 00:16:21,840
it just becomes an interaction, you're not liking things or disliking things, you're just

188
00:16:21,840 --> 00:16:27,840
dealing with them, okay now I'm seeing now I'm hearing now I have pain, now I have pleasure

189
00:16:27,840 --> 00:16:31,920
and so on and you're not clinging to anything, this is how the Buddha said in the Satipatana

190
00:16:31,920 --> 00:16:38,480
suta, Aniseeto, Aniseeto, Aniseeto, we're not clinging to anything, so it's just another

191
00:16:38,480 --> 00:16:43,920
brief teaching, something more for us to keep in mind when we do our practice that this is

192
00:16:43,920 --> 00:16:50,960
really what we're doing is purifying our thoughts and our speech and our deeds so thanks for

193
00:16:50,960 --> 00:17:20,800
listening and back to meditation.

