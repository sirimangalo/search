1
00:00:00,000 --> 00:00:09,000
Another question related to the former question we answer already, is from Maranasati?

2
00:00:09,000 --> 00:00:14,000
Is there an age limit for ordaining an upper limit I mean?

3
00:00:14,000 --> 00:00:21,000
No, not in general, not in the Vinaya, not from the Buddha.

4
00:00:21,000 --> 00:00:34,000
There are some sangas, age limits. I know from the Ticnatan Plum village that there is an upper age limit of 60 years.

5
00:00:34,000 --> 00:00:48,000
I know from Bhikkhuni sangas in Taiwan that there is an upper limit of 35 because there are many, many Bhikkhuni ordaining

6
00:00:48,000 --> 00:00:56,000
and they have to stop it and have to kind of regulate it, so that's why they made an upper age limit.

7
00:00:56,000 --> 00:01:04,000
But in general there shouldn't be one in all the monasteries.

8
00:01:04,000 --> 00:01:10,000
I know someone was asking, because they were asking this question because they are 42 years old.

9
00:01:10,000 --> 00:01:17,000
I think 42 is well within the age limits in most cases except, in the sounds of it, if you want to become a Bhikkhuni in Taiwan,

10
00:01:17,000 --> 00:01:22,000
because they have too many Bhikkhuni in Taiwan, I guess.

11
00:01:22,000 --> 00:01:25,000
I don't know, I guess. That's what you're saying.

12
00:01:25,000 --> 00:01:26,000
Yes.

13
00:01:26,000 --> 00:01:55,000
I should send them to Japan. Japan needs more Bhikkhuni, because otherwise they have to open this dating service for monastics.

