1
00:00:00,000 --> 00:00:02,000
You

2
00:00:30,000 --> 00:00:32,000
You

3
00:01:00,000 --> 00:01:02,000
You

4
00:01:30,000 --> 00:01:32,000
You

5
00:01:43,000 --> 00:01:46,760
Good evening, everyone and welcome to our live broadcast

6
00:01:49,680 --> 00:01:54,000
It's kind of impressive how many buttons I have to push to get this up and going on the computer

7
00:01:54,000 --> 00:02:05,000
We're very complicated set up here. So I can remember if I got on the right missed something

8
00:02:07,000 --> 00:02:10,000
And I probably complain about it. No way of automating that process

9
00:02:12,000 --> 00:02:14,000
Maybe there would be

10
00:02:14,000 --> 00:02:16,000
It wouldn't probably be worth it

11
00:02:16,000 --> 00:02:18,000
Some of it's in Firefox

12
00:02:18,000 --> 00:02:20,000
Some of it's on the command line

13
00:02:20,000 --> 00:02:25,000
Some of it's in another app. Probably I should have a little sticky note with a checklist

14
00:02:27,000 --> 00:02:29,000
Like I have to put in the title of the video

15
00:02:30,000 --> 00:02:36,000
I have to start to tell the web our website that we've started broadcasting

16
00:02:36,000 --> 00:02:40,000
I have to start the audio feed to start the video feed

17
00:02:42,000 --> 00:02:44,000
Anyway

18
00:02:44,000 --> 00:02:46,000
Not complaining really. It's just

19
00:02:46,000 --> 00:02:48,000
Need a checklist

20
00:02:50,000 --> 00:02:54,000
Okay, so today we're looking at and good to run a guy a book of five's

21
00:02:56,000 --> 00:02:58,000
So to two sixteen

22
00:03:00,000 --> 00:03:02,000
The Duttiak and D Sutta

23
00:03:03,000 --> 00:03:05,000
The second Sutta on someone who is

24
00:03:06,000 --> 00:03:08,000
impatient

25
00:03:08,000 --> 00:03:15,000
Hmm, why didn't I pick the first one? I don't know

26
00:03:20,000 --> 00:03:22,000
Well, that's looking for both

27
00:03:27,000 --> 00:03:30,000
Right, we'll look at two fifteen and two sixteen

28
00:03:30,000 --> 00:03:32,000
Those are some good things, but

29
00:03:32,000 --> 00:03:37,000
The reason, the real reason I chose this is because I wanted to talk about patience

30
00:03:38,000 --> 00:03:40,000
I think we're

31
00:03:42,000 --> 00:03:48,000
Well, these two Suttas talk about the dangers of being impatient

32
00:03:49,000 --> 00:03:51,000
It's useful to know

33
00:03:51,000 --> 00:03:55,000
But I just wanted to talk in general about patience

34
00:03:55,000 --> 00:03:59,000
First, at least

35
00:03:59,000 --> 00:04:01,000
Patience is a virtue

36
00:04:03,000 --> 00:04:05,000
We hear a lot about patience

37
00:04:06,000 --> 00:04:08,000
When you think about patience, no one says

38
00:04:09,000 --> 00:04:15,000
I don't ever hear anyone say, man, that person is too patient for me, I can't be around them

39
00:04:17,000 --> 00:04:19,000
I hate that guy, he's so impatient

40
00:04:19,000 --> 00:04:24,000
You don't find it criticized, that's what I mean

41
00:04:25,000 --> 00:04:28,000
It's universally acclaimed, I would say

42
00:04:29,000 --> 00:04:30,000
Patience is

43
00:04:32,000 --> 00:04:35,000
It's not often recognized, we don't

44
00:04:38,000 --> 00:04:42,000
Hold it up as a cardinal virtue, perhaps often enough

45
00:04:42,000 --> 00:04:48,000
That would say it deserves more attention

46
00:04:49,000 --> 00:04:51,000
More appreciation

47
00:04:52,000 --> 00:04:54,000
But it's a curious one as well

48
00:04:55,000 --> 00:05:00,000
Because I'm not even sure if patience exists, I can't remember whether it's one of the holes in Jay D'Sica's or not

49
00:05:01,000 --> 00:05:02,000
Might be

50
00:05:03,000 --> 00:05:08,000
But patience is a curious word, what do we mean by being patient?

51
00:05:08,000 --> 00:05:19,000
How do you describe or how do you determine whether someone is actually being patient?

52
00:05:20,000 --> 00:05:23,000
What does it mean? What is the essence of patience?

53
00:05:26,000 --> 00:05:32,000
It's possible to repress your impatience

54
00:05:33,000 --> 00:05:35,000
If you're waiting for something

55
00:05:35,000 --> 00:05:40,000
Waiting for your alarm, your meditation timer to ring

56
00:05:41,000 --> 00:05:46,000
You can suppress that and force yourself to sit

57
00:05:47,000 --> 00:05:51,000
But pretend to agree, well that's not very patient, that's a person who's impatient

58
00:05:52,000 --> 00:05:59,000
But forcing them to themselves to bear with their impatience

59
00:05:59,000 --> 00:06:04,000
Like a kid who's restless in sitting and saying, are we there yet, are we there yet?

60
00:06:05,000 --> 00:06:08,000
You wouldn't call them patient

61
00:06:09,000 --> 00:06:11,000
So we talk about waiting patiently

62
00:06:12,000 --> 00:06:15,000
And we obviously then have waiting impatient

63
00:06:16,000 --> 00:06:20,000
So what it means to wait patiently, it seems, is

64
00:06:20,000 --> 00:06:32,000
To be free from that angst, that agitation, that desire for something to occur or for something to cease

65
00:06:34,000 --> 00:06:43,000
And so it appears that what we call patience is actually just another word for freedom from defilement, really, or enlightenment

66
00:06:43,000 --> 00:06:51,000
And so here we have a really good word to describe directly what it means to be enlightened

67
00:06:52,000 --> 00:06:54,000
An enlightened person is patient

68
00:06:55,000 --> 00:07:02,000
A person who is patient, it's a truly patient to sign that they're on their way to enlightenment

69
00:07:02,000 --> 00:07:14,000
Or maybe that's going a little too far, it means to sign that they are able to create mind states that are free from defilement

70
00:07:15,000 --> 00:07:19,000
Because you can do this with summit temptation, people who practice summit to meditation

71
00:07:20,000 --> 00:07:23,000
Or meditation outside the Buddha's teaching can be quite patient

72
00:07:24,000 --> 00:07:27,000
People have cultivated strat, cultivated strategies

73
00:07:27,000 --> 00:07:32,000
To minimize their impatience

74
00:07:37,000 --> 00:07:42,000
But it's the same thing, the same people have cultivated strategies to minimize their defilements

75
00:07:43,000 --> 00:07:52,000
As it appears that, that what makes us impatient is the greed and anger and delusion

76
00:07:52,000 --> 00:07:57,000
Basically what we're talking about here is someone who acts out of turn

77
00:07:58,000 --> 00:08:03,000
The qualities of an impatient person is that they act at inappropriate times

78
00:08:04,000 --> 00:08:09,000
So a patient person will not react when an impatient person will

79
00:08:11,000 --> 00:08:16,000
In conversation, people who interrupt other people

80
00:08:16,000 --> 00:08:21,000
They do sometimes because they're impatient

81
00:08:24,000 --> 00:08:33,000
When you're forced to wait in line or wait at the airport

82
00:08:33,000 --> 00:08:46,000
When you're forced to bear with unpleasant situations or long drawn out events

83
00:08:47,000 --> 00:08:50,000
When you're forced to sit still, stand still

84
00:08:53,000 --> 00:08:57,000
When you're forced to listen to someone drowning on and on

85
00:08:58,000 --> 00:09:01,000
When you're forced to bear with heat and cold

86
00:09:01,000 --> 00:09:06,000
The impatient person will react to all of this

87
00:09:07,000 --> 00:09:11,000
They will react if you have the patient person and the impatient person

88
00:09:12,000 --> 00:09:14,000
The impatient person will react first

89
00:09:15,000 --> 00:09:20,000
And they will react to in a way so as to change their situation

90
00:09:20,000 --> 00:09:29,000
The Buddha said that patients can't be, but among the poor they take out

91
00:09:30,000 --> 00:09:33,000
Patients is the greatest form of torture

92
00:09:34,000 --> 00:09:38,000
Maybe not torture, but asceticism or tapa

93
00:09:39,000 --> 00:09:45,000
It means heat, but it was a word that was used for self-mortification or discipline

94
00:09:45,000 --> 00:09:50,000
It means discipline is the right word

95
00:09:51,000 --> 00:09:54,000
Patients is the highest form of discipline

96
00:09:55,000 --> 00:10:05,000
We have all these other ways of forcing ourselves or tricking ourselves into bearing with unpleasantness

97
00:10:06,000 --> 00:10:10,000
But the things surpasses true patience

98
00:10:10,000 --> 00:10:14,000
And this I think really what you feel directly when you meditate

99
00:10:14,000 --> 00:10:18,000
Maybe this is one of the, for all those people that they're looking for

100
00:10:19,000 --> 00:10:23,000
The benefits of meditation and find themselves

101
00:10:24,000 --> 00:10:29,000
In having a hard time figuring out the actual benefits of meditation

102
00:10:29,000 --> 00:10:31,000
I think you can see this quite clearly

103
00:10:32,000 --> 00:10:35,000
And quite quickly is the patience

104
00:10:35,000 --> 00:10:40,000
That which you were unable to bear previously, you're now able to bear

105
00:10:43,000 --> 00:10:47,000
I think the problem is when we look at benefits of meditation

106
00:10:48,000 --> 00:10:51,000
We're often looking in the wrong place or expecting the wrong things

107
00:10:52,000 --> 00:10:55,000
We miss what we really get on a meditation

108
00:10:56,000 --> 00:10:58,000
Patients is one of the big ones

109
00:10:58,000 --> 00:11:06,000
We're looking for wisdom and by wisdom we think it's some kind of profound thought

110
00:11:07,000 --> 00:11:09,000
Or insight into reality

111
00:11:09,000 --> 00:11:12,000
It is quite profound, but perhaps not in the way we think

112
00:11:12,000 --> 00:11:15,000
I mean, patience itself is quite profound

113
00:11:16,000 --> 00:11:22,000
I think of the profound difference between a person who is unable to

114
00:11:22,000 --> 00:11:28,000
I'm unable to bear with or becomes really

115
00:11:29,000 --> 00:11:35,000
A given situation and a person who is a different person

116
00:11:35,000 --> 00:11:37,000
Who is unable to sleep at night

117
00:11:37,000 --> 00:11:41,000
And a person who is able to sleep sound no matter what happens to them

118
00:11:43,000 --> 00:11:46,000
People who are quick to anger

119
00:11:46,000 --> 00:11:49,000
And people who are not quick to anger

120
00:11:49,000 --> 00:11:52,000
Or unshakable

121
00:11:53,000 --> 00:11:58,000
People who are addicted and chasing after pleasures

122
00:11:59,000 --> 00:12:03,000
And those who are at peace and content with their lives

123
00:12:04,000 --> 00:12:06,000
Quite a difference

124
00:12:07,000 --> 00:12:12,000
So this is what you should see in the meditation quite quickly, I would think

125
00:12:12,000 --> 00:12:21,000
And certainly as you get better at it, patience is the clear benefit of the practice

126
00:12:21,000 --> 00:12:25,000
So when you feel pain and you say to yourself pain, pain suddenly, the pain is not a problem

127
00:12:25,000 --> 00:12:27,000
Your reactions disappear

128
00:12:29,000 --> 00:12:33,000
Loud noises, you're kind of surprised

129
00:12:33,000 --> 00:12:37,000
I'm amazed thinking back at how they used the body

130
00:12:37,000 --> 00:12:43,000
Heat and cold, amazed by how patient you can be with heat and cold

131
00:12:43,000 --> 00:12:46,000
If you just say to yourself, oh, it's hard to cold

132
00:12:49,000 --> 00:12:52,000
How much more patient you are with other people

133
00:12:53,000 --> 00:12:57,000
How much more patient you are with your work, how work is no longer arduous

134
00:12:57,000 --> 00:13:02,000
It's no longer torturous, it's just walking, standing, sitting, lying

135
00:13:02,000 --> 00:13:09,000
If they're not lying, but a lot of movements of the body and activity in the mind

136
00:13:09,000 --> 00:13:11,000
And it comes and it goes and that's it

137
00:13:18,000 --> 00:13:20,000
So that's about patience

138
00:13:20,000 --> 00:13:23,000
Let's look now at these two suitors because they talk about

139
00:13:23,000 --> 00:13:30,000
Give some concrete examples of kind of benefits that come from being patience

140
00:13:30,000 --> 00:13:33,000
If it's not yet clear

141
00:13:33,000 --> 00:13:41,000
So first of all, a person who is impatient, bah-noh-jana-sa-appi-o-ho-ti-amana-bo

142
00:13:41,000 --> 00:13:45,000
The person who is very, it was impatient

143
00:13:45,000 --> 00:13:55,000
It's going to be, what's the opposite of dear, under disagreeable

144
00:13:55,000 --> 00:14:00,000
To up-be-o, it means not dear to people

145
00:14:00,000 --> 00:14:03,000
It will be disagreeable to many people

146
00:14:03,000 --> 00:14:05,000
This is what you do find

147
00:14:05,000 --> 00:14:07,000
That person is very impatient

148
00:14:07,000 --> 00:14:09,000
It's hard to be around that person

149
00:14:09,000 --> 00:14:11,000
They're so impatient, oh, you're so impatient

150
00:14:11,000 --> 00:14:14,000
People will scold each other in this way

151
00:14:14,000 --> 00:14:17,000
It's hard to have good friends if you're not patient

152
00:14:17,000 --> 00:14:19,000
And the opposite is of course true

153
00:14:19,000 --> 00:14:22,000
The person who is patient is loved by all

154
00:14:22,000 --> 00:14:26,000
And is easy as able to be friends

155
00:14:26,000 --> 00:14:31,000
Some of the most rewarding friendships come from listening to each other

156
00:14:31,000 --> 00:14:35,000
From being there for each other

157
00:14:35,000 --> 00:14:40,000
A friend doesn't always want advice and conversation

158
00:14:40,000 --> 00:14:49,000
Sometimes they just want a patient, a patient friend

159
00:14:49,000 --> 00:14:52,000
To listen and to be there

160
00:14:52,000 --> 00:14:55,000
And to support them

161
00:14:58,000 --> 00:15:03,000
An impatient person, wah-ra-bah-lo-jah-ho-ti

162
00:15:03,000 --> 00:15:06,000
Has much wah-ra, wah-ra means

163
00:15:06,000 --> 00:15:10,000
Enmity, vengeance

164
00:15:10,000 --> 00:15:15,000
Wain, wah-ra, wah-ra is a word that at least in Thailand

165
00:15:15,000 --> 00:15:22,000
They focus on the concept of the cycle of revenge

166
00:15:22,000 --> 00:15:27,000
When you are in a relationship with someone

167
00:15:27,000 --> 00:15:30,000
That is vengeful

168
00:15:30,000 --> 00:15:32,000
When you have a grudge against someone

169
00:15:32,000 --> 00:15:36,000
In a rivalry, feud conflict with someone

170
00:15:36,000 --> 00:15:39,000
That's what wain, wah-ra means

171
00:15:39,000 --> 00:15:42,000
And it has sort of spiritual connotations

172
00:15:42,000 --> 00:15:47,000
The idea that it's a thing that goes from life to life

173
00:15:47,000 --> 00:15:50,000
So if you kill someone in this life, you have wain

174
00:15:50,000 --> 00:15:52,000
They say in Thailand, I mean

175
00:15:52,000 --> 00:15:54,000
In the next life, they'll come back and kill you

176
00:15:54,000 --> 00:15:56,000
Something like that

177
00:15:56,000 --> 00:15:58,000
It would be anger

178
00:15:58,000 --> 00:16:02,000
So if you meet someone and just you don't just can't hit it off

179
00:16:02,000 --> 00:16:05,000
They call it wain

180
00:16:05,000 --> 00:16:07,000
They actually do they really

181
00:16:07,000 --> 00:16:09,000
Wain, you come, you wain, god

182
00:16:09,000 --> 00:16:13,000
Wain is this idea of vengeance

183
00:16:13,000 --> 00:16:17,000
People get back at you because you have wain with them

184
00:16:17,000 --> 00:16:20,000
So this is what happens if you're impatient

185
00:16:20,000 --> 00:16:24,000
You cultivate conflict

186
00:16:24,000 --> 00:16:26,000
Of course if you're patient

187
00:16:26,000 --> 00:16:29,000
And even if people get angry at you, even if they hit you

188
00:16:29,000 --> 00:16:31,000
And even if they scold you

189
00:16:31,000 --> 00:16:35,000
And do things, say things behind your back

190
00:16:35,000 --> 00:16:38,000
Whenever they do, or say

191
00:16:38,000 --> 00:16:40,000
Or think

192
00:16:40,000 --> 00:16:44,000
Your patience, your clarity of mind, your objectivity

193
00:16:44,000 --> 00:16:48,000
You see, patience is just such a good word

194
00:16:48,000 --> 00:16:51,000
It keeps you from

195
00:16:53,000 --> 00:16:55,000
From continuing the conflict

196
00:16:55,000 --> 00:16:57,000
I'm going to say what they're going to do

197
00:16:57,000 --> 00:17:01,000
It's a worst evil

198
00:17:01,000 --> 00:17:04,000
The one who responds with anger

199
00:17:04,000 --> 00:17:06,000
To the one who is angry

200
00:17:06,000 --> 00:17:09,000
The one who responds with anger is worse

201
00:17:09,000 --> 00:17:11,000
Because they're the ones who

202
00:17:11,000 --> 00:17:12,000
Everyone gets angry

203
00:17:12,000 --> 00:17:14,000
But if we can't be patient with each other

204
00:17:14,000 --> 00:17:16,000
That's how conflict starts

205
00:17:16,000 --> 00:17:18,000
Conflict doesn't start with the first person

206
00:17:18,000 --> 00:17:22,000
You just see someone getting angry

207
00:17:22,000 --> 00:17:24,000
You just let it go

208
00:17:24,000 --> 00:17:26,000
We allow each other to be angry

209
00:17:26,000 --> 00:17:28,000
But if you get angry back

210
00:17:28,000 --> 00:17:30,000
You start at something

211
00:17:30,000 --> 00:17:32,000
You created a problem

212
00:17:32,000 --> 00:17:34,000
Because then it's a cycle

213
00:17:34,000 --> 00:17:37,000
When one person is angry

214
00:17:37,000 --> 00:17:39,000
Everyone else should

215
00:17:39,000 --> 00:17:43,000
Make effort to not be angry

216
00:17:43,000 --> 00:17:46,000
Number three, what Jabaholo

217
00:17:46,000 --> 00:17:50,000
Blame, much blame

218
00:17:50,000 --> 00:17:56,000
Yes, people blame you

219
00:17:56,000 --> 00:17:59,000
Or say bad things about you

220
00:17:59,000 --> 00:18:02,000
You'll do many things wrong

221
00:18:02,000 --> 00:18:05,000
As well

222
00:18:05,000 --> 00:18:09,000
An impatient person is the one who kills the mosquito

223
00:18:09,000 --> 00:18:12,000
An impatient person is the one who speaks out of turn

224
00:18:12,000 --> 00:18:17,000
An impatient person is the one who steals

225
00:18:17,000 --> 00:18:20,000
When they can't wait for their turn

226
00:18:20,000 --> 00:18:22,000
Or they can't wait for to get their own

227
00:18:22,000 --> 00:18:26,000
They can't bear with thought of not having something

228
00:18:26,000 --> 00:18:31,000
The impatient one is the one who kills and hurts and steals and lies and cheat

229
00:18:31,000 --> 00:18:35,000
Because they're impatient

230
00:18:35,000 --> 00:18:37,000
Do bad things

231
00:18:37,000 --> 00:18:40,000
Us are the number three

232
00:18:40,000 --> 00:18:45,000
Number four, Samuulho Kalang Karuti

233
00:18:45,000 --> 00:18:47,000
They die

234
00:18:47,000 --> 00:18:51,000
I was a bewildered mind

235
00:18:51,000 --> 00:18:53,000
Confused mind

236
00:18:53,000 --> 00:18:57,000
They do their time Kalang Karuti

237
00:18:57,000 --> 00:18:59,000
Kalang Karuti means

238
00:18:59,000 --> 00:19:01,000
They die

239
00:19:01,000 --> 00:19:05,000
That really means they do their time

240
00:19:05,000 --> 00:19:08,000
As Samuulho which means

241
00:19:08,000 --> 00:19:11,000
Confused be bewildered

242
00:19:11,000 --> 00:19:13,000
If you die confused, you don't know where you're going to go

243
00:19:13,000 --> 00:19:15,000
You just grab onto anything

244
00:19:15,000 --> 00:19:17,000
Because of your impatience

245
00:19:17,000 --> 00:19:19,000
You're not able to ponder

246
00:19:19,000 --> 00:19:23,000
You're not able to consider things rationally

247
00:19:23,000 --> 00:19:25,000
You're not able to see things clearly

248
00:19:25,000 --> 00:19:27,000
You're just quick to react

249
00:19:27,000 --> 00:19:29,000
So what's really going on?

250
00:19:29,000 --> 00:19:31,000
So when you die, you're very quick

251
00:19:31,000 --> 00:19:33,000
To be reborn

252
00:19:33,000 --> 00:19:35,000
On a more base

253
00:19:35,000 --> 00:19:39,000
Of course, realms of existence

254
00:19:41,000 --> 00:19:43,000
So that's number five

255
00:19:43,000 --> 00:19:46,000
Gaiyasabhiyadha, Bharongmana, Apayyang

256
00:19:46,000 --> 00:19:50,000
Do good thing, vinipadha, vinirya, vinipadha

257
00:19:50,000 --> 00:19:53,000
Just a fancy way of saying

258
00:19:53,000 --> 00:19:55,000
When you die, you're going to hell

259
00:19:55,000 --> 00:19:58,000
Or you're born in one of the states of loss

260
00:19:58,000 --> 00:20:00,000
Could be as an animal or so

261
00:20:00,000 --> 00:20:02,000
Well, it's here, it says vinirya, so

262
00:20:02,000 --> 00:20:05,000
It's quite possible that out of impatience

263
00:20:05,000 --> 00:20:08,000
It gets so bad that you

264
00:20:08,000 --> 00:20:11,000
When you die, you go to hell

265
00:20:13,000 --> 00:20:16,000
Whereas if you're patient, a patient person

266
00:20:16,000 --> 00:20:18,000
Because they don't grasp onto anything

267
00:20:18,000 --> 00:20:22,000
Their mind when they die becomes very light

268
00:20:22,000 --> 00:20:24,000
It's very little clinging, right?

269
00:20:24,000 --> 00:20:26,000
Because there's a very little clinging

270
00:20:26,000 --> 00:20:27,000
They're very bright and light

271
00:20:27,000 --> 00:20:30,000
And so they go to a bright and light place

272
00:20:30,000 --> 00:20:33,000
Which we know of as heaven

273
00:20:35,000 --> 00:20:37,000
That's 215, 216

274
00:20:37,000 --> 00:20:39,000
It's almost the same

275
00:20:39,000 --> 00:20:41,000
Just as one difference, I think

276
00:20:41,000 --> 00:20:42,000
Oh, it's a couple of different

277
00:20:42,000 --> 00:20:44,000
So the second one is

278
00:20:47,000 --> 00:20:49,000
Ludoho

279
00:20:49,000 --> 00:20:53,000
The person who is impatient is cruel

280
00:20:53,000 --> 00:20:59,000
Fierce, it's an awful person

281
00:20:59,000 --> 00:21:01,000
People who are impatient are

282
00:21:01,000 --> 00:21:04,000
ones who are who do things

283
00:21:04,000 --> 00:21:06,000
Who are or are others

284
00:21:06,000 --> 00:21:09,000
Who hurt even the ones they love

285
00:21:09,000 --> 00:21:10,000
They hurt

286
00:21:10,000 --> 00:21:12,000
Out of impatience, right?

287
00:21:12,000 --> 00:21:13,000
They say something

288
00:21:13,000 --> 00:21:15,000
And we don't mean it, but we're just impatient

289
00:21:15,000 --> 00:21:17,000
We're not able to

290
00:21:19,000 --> 00:21:21,000
Maintain our equanimity

291
00:21:21,000 --> 00:21:23,000
I'm able to see things without

292
00:21:23,000 --> 00:21:25,000
Experience things without reacting

293
00:21:29,000 --> 00:21:31,000
Number three is also different

294
00:21:31,000 --> 00:21:33,000
Number three, we put the Saudi

295
00:21:33,000 --> 00:21:35,000
They are remorseful

296
00:21:35,000 --> 00:21:37,000
The person who is impatient is remorseful

297
00:21:37,000 --> 00:21:39,000
Goes with that

298
00:21:39,000 --> 00:21:41,000
We hurt others, even though we might love them

299
00:21:41,000 --> 00:21:45,000
And then we feel bad about it after

300
00:21:45,000 --> 00:21:46,000
Will we do anything?

301
00:21:46,000 --> 00:21:49,000
We might engage in our addictions

302
00:21:49,000 --> 00:21:52,000
We feel bad about it after

303
00:21:54,000 --> 00:21:56,000
We put the Saudi

304
00:21:56,000 --> 00:21:58,000
Because of our impatience

305
00:21:58,000 --> 00:22:00,000
Because of our inability to

306
00:22:00,000 --> 00:22:03,000
Have this elusive quality of patience

307
00:22:03,000 --> 00:22:06,000
Which turns out to just be

308
00:22:06,000 --> 00:22:08,000
In its best form anyway

309
00:22:08,000 --> 00:22:11,000
Wisdom, freedom from defilement

310
00:22:11,000 --> 00:22:13,000
In any way

311
00:22:15,000 --> 00:22:17,000
The rest of them are insane

312
00:22:17,000 --> 00:22:21,000
That's the two

313
00:22:21,000 --> 00:22:23,000
Archandias

314
00:22:23,000 --> 00:22:25,000
A contestant

315
00:22:25,000 --> 00:22:29,000
Talking about patience and impatience

316
00:22:29,000 --> 00:22:33,000
That's our demo for tonight

317
00:22:33,000 --> 00:22:51,000
Yes, we have at least one question hanging on there

318
00:22:51,000 --> 00:22:57,000
You guys can go

319
00:22:57,000 --> 00:23:05,000
Sorry, I should fix my screen before

320
00:23:05,000 --> 00:23:07,000
This is a long time to let up

321
00:23:07,000 --> 00:23:09,000
Yeah, freezes

322
00:23:09,000 --> 00:23:35,000
Mine's probably frozen as well, probably

323
00:23:35,000 --> 00:23:49,000
I'm sorry about that

324
00:23:49,000 --> 00:23:53,000
It leads

325
00:23:53,000 --> 00:23:57,000
In the chakakakan

326
00:23:57,000 --> 00:23:59,000
It talks about the eye

327
00:23:59,000 --> 00:24:01,000
And visible forms not being

328
00:24:01,000 --> 00:24:03,000
Self because they rise and cease

329
00:24:03,000 --> 00:24:06,000
I can see how eye consciousness arises

330
00:24:06,000 --> 00:24:08,000
And ceases, but it seems to me

331
00:24:08,000 --> 00:24:11,000
My eye arose while I was in the womb

332
00:24:11,000 --> 00:24:14,000
And will not likely cease until after I die

333
00:24:14,000 --> 00:24:16,000
And some visible forms say a rock

334
00:24:16,000 --> 00:24:18,000
Been around many thousands of years

335
00:24:18,000 --> 00:24:20,000
Many material objects

336
00:24:20,000 --> 00:24:23,000
I won't observe arising and ceasing in my lifetime

337
00:24:23,000 --> 00:24:26,000
So how is it possible to see these things as impermanent

338
00:24:26,000 --> 00:24:28,000
And not self-then

339
00:24:28,000 --> 00:24:30,000
And sort of related to this

340
00:24:30,000 --> 00:24:32,000
Are the eye an internal eye based

341
00:24:32,000 --> 00:24:35,000
The same thing, you mentioned they were different

342
00:24:35,000 --> 00:24:40,000
And a previous answer, but I didn't understand that difference

343
00:24:40,000 --> 00:24:42,000
I mean, it's all just very technical

344
00:24:42,000 --> 00:24:45,000
I wouldn't overthink this

345
00:24:45,000 --> 00:24:51,000
It means that at the moment of seeing

346
00:24:51,000 --> 00:24:57,000
There is the contact

347
00:24:57,000 --> 00:25:02,000
The point is in Buddhism we don't acknowledge the existence of the eye

348
00:25:02,000 --> 00:25:06,000
The eye doesn't exist, this thing here

349
00:25:06,000 --> 00:25:10,000
But seeing does exist

350
00:25:10,000 --> 00:25:13,000
What we can experience as being real is seeing

351
00:25:13,000 --> 00:25:16,000
So what we're trying to do is describe

352
00:25:16,000 --> 00:25:20,000
What's responsible for this experience of seeing

353
00:25:20,000 --> 00:25:25,000
And it's clear that to see external objects

354
00:25:25,000 --> 00:25:32,000
You require something internal, an eye

355
00:25:32,000 --> 00:25:43,000
But that eye is considered to be simply the physical aspect of the experience

356
00:25:43,000 --> 00:25:48,000
Which arises in seizes

357
00:25:48,000 --> 00:25:51,000
So what it means is the experience, since the experience arises

358
00:25:51,000 --> 00:25:53,000
In seizes in a moment

359
00:25:53,000 --> 00:25:56,000
Every part of it, whether the physical part and the mental part

360
00:25:56,000 --> 00:25:59,000
All of them arise in seizes in a moment

361
00:25:59,000 --> 00:26:01,000
It's just quite technical

362
00:26:01,000 --> 00:26:03,000
I mean, you think it's actually kind of pedantic

363
00:26:03,000 --> 00:26:05,000
Why don't we just say the eye exists

364
00:26:05,000 --> 00:26:10,000
Because we're only talking about reality

365
00:26:10,000 --> 00:26:12,000
From the point of view of experience

366
00:26:12,000 --> 00:26:15,000
From the point of view of experience

367
00:26:15,000 --> 00:26:19,000
None of these things really practically speaking exist

368
00:26:19,000 --> 00:26:24,000
There are just aspects of this one thing that we call an experience of seeing

369
00:26:24,000 --> 00:26:27,000
So in the experience of seeing there, which is momentary

370
00:26:27,000 --> 00:26:33,000
There is the physical aspect, there is the physical internal aspect

371
00:26:33,000 --> 00:26:35,000
There is the physical external aspect

372
00:26:35,000 --> 00:26:37,000
And there is the mental aspect

373
00:26:37,000 --> 00:26:39,000
And those three coming together

374
00:26:39,000 --> 00:26:43,000
Are those three arising together

375
00:26:43,000 --> 00:26:46,000
Are the experience of seeing

376
00:26:46,000 --> 00:26:51,000
But in a way, you could think of it as just

377
00:26:51,000 --> 00:26:55,000
Just a convention

378
00:26:55,000 --> 00:26:58,000
Because you can also see things

379
00:26:58,000 --> 00:27:02,000
You can also see things in the mind

380
00:27:02,000 --> 00:27:06,000
Without using the eye, without relying on light

381
00:27:06,000 --> 00:27:10,000
So we're just describing, hey, it appears

382
00:27:10,000 --> 00:27:14,000
That certain things are physical

383
00:27:14,000 --> 00:27:18,000
So you see actual light

384
00:27:18,000 --> 00:27:22,000
And it's touching an actual eye

385
00:27:22,000 --> 00:27:26,000
But in reality, when you see, there's only the experience

386
00:27:26,000 --> 00:27:29,000
We're kind of extrapolating the fact

387
00:27:29,000 --> 00:27:35,000
That there is the physical base, physical eye base

388
00:27:35,000 --> 00:27:38,000
So as I said, I wouldn't really overthink this

389
00:27:38,000 --> 00:27:41,000
You're not trying to see that

390
00:27:41,000 --> 00:27:45,000
And practically speaking, trying to see each and every one of them individually

391
00:27:45,000 --> 00:27:46,000
It's impermanent

392
00:27:46,000 --> 00:27:48,000
We're certainly not talking about this physical eye

393
00:27:48,000 --> 00:27:53,000
The physical eye, as you say, is lasting

394
00:27:53,000 --> 00:27:55,000
It's stable

395
00:27:55,000 --> 00:27:58,000
So that's because it doesn't exist

396
00:27:58,000 --> 00:28:00,000
It's not real

397
00:28:00,000 --> 00:28:02,000
Real is the experience that arises

398
00:28:02,000 --> 00:28:05,000
It's safe to focus more on that

399
00:28:05,000 --> 00:28:08,000
So it's just that the text often

400
00:28:08,000 --> 00:28:11,000
You're going to break everything down, and they say, they take each aspect

401
00:28:11,000 --> 00:28:13,000
And say, well, each one of them is impermanent

402
00:28:13,000 --> 00:28:15,000
But it's not a new experience

403
00:28:15,000 --> 00:28:17,000
The experience that it's seeing is impermanent

404
00:28:17,000 --> 00:28:19,000
See seeing arises, and see some stats

405
00:28:19,000 --> 00:28:22,000
How we practically speaking see it

406
00:28:22,000 --> 00:28:24,000
It's my understanding

407
00:28:31,000 --> 00:28:34,000
And when you talk about physical forms being around

408
00:28:34,000 --> 00:28:36,000
For many thousands of years, no

409
00:28:36,000 --> 00:28:40,000
We're talking about the physical form that it's a part of that experience

410
00:28:40,000 --> 00:28:43,000
The rock itself doesn't exist

411
00:28:43,000 --> 00:28:46,000
Not from the point of view of experience

412
00:28:46,000 --> 00:28:50,000
Which is the paradigm that we use in Buddhism, as you can see

413
00:28:53,000 --> 00:28:56,000
Well, that was all the questions for tonight

414
00:28:58,000 --> 00:29:00,000
How was it?

415
00:29:00,000 --> 00:29:02,000
All right

416
00:29:02,000 --> 00:29:05,000
I don't even have a good night, everyone

417
00:29:05,000 --> 00:29:07,000
Thanks, Robin, for your help

418
00:29:07,000 --> 00:29:34,000
Thank you, Dante, good night

