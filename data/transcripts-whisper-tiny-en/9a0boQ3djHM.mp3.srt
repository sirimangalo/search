1
00:00:00,000 --> 00:00:06,440
Masaki asked, how does one describe that which is indescribable, discovered through

2
00:00:06,440 --> 00:00:11,720
meditation? How, for example, do I describe things which extend beyond the

3
00:00:11,720 --> 00:00:17,640
barrier of language, art and visuals to people? I wish to help people, even though

4
00:00:17,640 --> 00:00:22,360
I'm suffering. I see many people who are suffering. I wish you correct your

5
00:00:22,360 --> 00:00:28,080
underneath. I see many people who are suffering and it pains me that there are

6
00:00:28,080 --> 00:00:32,280
things I have learned in meditation that I cannot describe. I just want to

7
00:00:32,280 --> 00:00:35,240
alleviate suffering.

8
00:00:42,600 --> 00:00:53,520
By example, if you can't put it in words, do it be an example that is probably

9
00:00:53,520 --> 00:01:04,640
anyway, in any case, better than words. So what you have experienced is not

10
00:01:04,640 --> 00:01:10,360
really important to be told to other people. They don't need to know the words,

11
00:01:10,360 --> 00:01:18,560
they don't need to know what exactly you have experienced, but what would help

12
00:01:18,560 --> 00:01:25,040
them is that if your mind has become clear and if you have become more

13
00:01:25,040 --> 00:01:31,640
peaceful, then to share that with the people, then to share your clearness

14
00:01:31,640 --> 00:01:38,280
and maybe your joy that you gained, to share that be generous with it and spread

15
00:01:38,280 --> 00:01:52,400
it around the world, and just don't use words. Very good. Very well said, and

16
00:01:52,400 --> 00:01:57,400
another thing to share with them is there's this funny children's documentary of

17
00:01:57,400 --> 00:02:05,760
Buddhism and it shows the Buddha coming back to the five aesthetics and at first

18
00:02:05,760 --> 00:02:12,200
of course they don't want to take his room but then they do it and it really

19
00:02:12,200 --> 00:02:20,600
made me smile to watch this because he says, oh monks, I have realized the truth

20
00:02:20,600 --> 00:02:27,000
and found, you know, he gives this profound exposition of I have realized the

21
00:02:27,000 --> 00:02:31,640
Four Noble Truths and I have come to be free from all suffering and then they

22
00:02:31,640 --> 00:02:39,760
reply, how did you do it? Just kind of like he was saying, he had found a new,

23
00:02:39,760 --> 00:02:45,040
he had learned some new skill or something very simple, how did you do it

24
00:02:45,040 --> 00:02:51,480
venerable sir? But this is really the, this is really a good point because it's

25
00:02:51,480 --> 00:02:55,880
really what people want, what you need to give them is not as spontaneous and

26
00:02:55,880 --> 00:03:01,040
what you've gained from the practice but by showing them that it's an excellent

27
00:03:01,040 --> 00:03:08,280
example, once they see the example what they want is how can I get that and you

28
00:03:08,280 --> 00:03:12,000
should at least be able to explain the practices that you've done. If your

29
00:03:12,000 --> 00:03:16,640
practices have led you to peace, happiness and freedom from suffering, then

30
00:03:16,640 --> 00:03:21,920
explain to them the practice. This is, I mean if you read that it was my thinking

31
00:03:21,920 --> 00:03:25,200
when I made this booklet, you know, this booklet on how to meditate, it doesn't

32
00:03:25,200 --> 00:03:32,640
teach much theory at all. It gives you the basic how-to and the reason why I

33
00:03:32,640 --> 00:03:37,200
started making YouTube videos at all was not to put teachings out there

34
00:03:37,200 --> 00:03:41,280
actually but to put a how-to guide out there something so that you yourself

35
00:03:41,280 --> 00:03:47,720
don't have to learn theory, don't have to learn what is nibana like or what

36
00:03:47,720 --> 00:03:54,680
does it mean to be enlightened, you have to learn what are the practical steps that I

37
00:03:54,680 --> 00:04:00,200
can take to reduce myself, foring to increase my peace and to become free from

38
00:04:00,200 --> 00:04:22,360
defilement in mind. So the practice I think is something that you should share to them, share with them.

