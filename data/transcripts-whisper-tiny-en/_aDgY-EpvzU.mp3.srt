1
00:00:00,000 --> 00:00:14,880
What's in the background?

2
00:00:14,880 --> 00:00:19,900
No we are.

3
00:00:19,900 --> 00:00:28,440
Good evening.

4
00:00:28,440 --> 00:00:30,440
tonight's talk will be about

5
00:00:32,920 --> 00:00:34,920
Satipatana vipassana

6
00:00:40,680 --> 00:00:42,680
to

7
00:00:43,480 --> 00:00:45,480
pali words

8
00:00:50,920 --> 00:00:53,640
I bring up the pali I don't want to

9
00:00:54,760 --> 00:00:56,760
convert in

10
00:00:56,760 --> 00:00:58,760
you with

11
00:00:58,760 --> 00:01:00,760
foreign words but

12
00:01:00,760 --> 00:01:02,760
bring it up because they're hard to translate

13
00:01:04,280 --> 00:01:06,280
and they're often mistranslated

14
00:01:10,760 --> 00:01:13,720
We call the meditation that we practice vipassana

15
00:01:13,720 --> 00:01:15,720
you

16
00:01:22,680 --> 00:01:24,680
but sometimes you call something

17
00:01:26,920 --> 00:01:28,920
you call what you do

18
00:01:30,280 --> 00:01:32,280
you give it a name because of

19
00:01:33,160 --> 00:01:35,160
it describes what you do

20
00:01:36,840 --> 00:01:39,080
in this case we give it this name not

21
00:01:39,080 --> 00:01:41,560
not because it's what we do but because it's what we

22
00:01:41,560 --> 00:01:46,920
aspire for aspire towards aim for

23
00:01:53,240 --> 00:01:55,240
like when we say

24
00:01:57,080 --> 00:01:59,080
car travel

25
00:02:01,880 --> 00:02:04,200
you're traveling in a car or even

26
00:02:04,200 --> 00:02:06,200
you

27
00:02:08,600 --> 00:02:10,600
you might say

28
00:02:12,920 --> 00:02:15,960
you're going going on a trip a car trip

29
00:02:18,600 --> 00:02:21,640
but that's not what you're actually doing you're actually driving the car

30
00:02:23,160 --> 00:02:25,560
the driving is one thing and traveling is another

31
00:02:28,680 --> 00:02:30,360
vipassana is like the traveling

32
00:02:30,360 --> 00:02:32,920
the getting somewhere

33
00:02:38,600 --> 00:02:44,280
so getting somewhere is in our goal or sorry traveling is in our goal

34
00:02:45,880 --> 00:02:50,680
but it's the reason why we drive we drive to go somewhere

35
00:02:52,200 --> 00:02:54,280
the goal is is get it is the somewhere

36
00:02:54,280 --> 00:03:01,320
we pass in as like the driving that there's like the travel

37
00:03:02,760 --> 00:03:04,760
but Satyipatana is the driving

38
00:03:06,680 --> 00:03:08,200
Satyipatana is what we do

39
00:03:12,120 --> 00:03:14,760
we call it vipassana because our aim

40
00:03:18,520 --> 00:03:20,440
our aim is to see clearly

41
00:03:20,440 --> 00:03:28,120
vip means clearly

42
00:03:31,640 --> 00:03:33,880
our aim is seeing clearly

43
00:03:37,880 --> 00:03:39,880
so it's often translated as insight

44
00:03:42,280 --> 00:03:46,120
and that can be misleading

45
00:03:46,120 --> 00:03:50,920
I'm actually thinking it might not be a very good translation

46
00:03:52,840 --> 00:03:57,560
because insight tends to give the impression that

47
00:03:59,320 --> 00:04:03,960
it's meant to be somehow intellectual or rational or have to do with thought

48
00:04:07,080 --> 00:04:11,960
so when we hear it described about what vipassana means we think well this is something that

49
00:04:11,960 --> 00:04:18,040
I have to I have to understand

50
00:04:18,040 --> 00:04:22,600
what it's like understanding it's funny because it's not really

51
00:04:25,720 --> 00:04:28,520
understanding isn't really a great word either

52
00:04:32,760 --> 00:04:37,480
it can can be given proper meaning but it can also again

53
00:04:37,480 --> 00:04:42,600
refer to some kind of intellectual understanding

54
00:04:43,880 --> 00:04:49,960
so it's not about insight so much it is a clear sight again we means clear

55
00:04:53,400 --> 00:04:57,800
and this is made clear in the in the Buddhist forearms what is teaching himself

56
00:04:59,400 --> 00:05:02,520
the Buddhist teachings himself when when he says

57
00:05:02,520 --> 00:05:07,400
it's like polishing a lens

58
00:05:10,840 --> 00:05:13,480
wiping away all the grimes so you can see through it

59
00:05:14,520 --> 00:05:20,440
it's like polishing a glass or cleaning the glass so you can see clearly

60
00:05:20,440 --> 00:05:28,840
so what is it that we're trying to see clearly we're trying to see clearly three things

61
00:05:33,240 --> 00:05:36,760
we want to see clearly the inside of ourselves and in the world around us

62
00:05:39,240 --> 00:05:44,280
everything is uncertain and predictable in constant

63
00:05:44,280 --> 00:05:50,120
impermanent

64
00:05:50,680 --> 00:05:55,400
we want to see clearly that inside of ourselves and in the world around us

65
00:05:56,840 --> 00:05:58,520
nothing is satisfying

66
00:06:02,840 --> 00:06:05,960
nothing is happiness

67
00:06:05,960 --> 00:06:13,560
nothing is going to make us happy by taking it as an object

68
00:06:19,240 --> 00:06:25,000
and the three that inside of ourselves and the world around us and really every single thing

69
00:06:26,600 --> 00:06:30,280
including freedom from suffering is it's non-self

70
00:06:30,280 --> 00:06:38,280
so me it's about mine it doesn't have a self it doesn't have a soul it doesn't have an entity

71
00:06:39,640 --> 00:06:41,800
doesn't have a core

72
00:06:50,520 --> 00:06:56,920
and so if you took these as insights then you'd have to understand what these mean

73
00:06:56,920 --> 00:07:01,480
you'd have to have it explained to you and you'd have to

74
00:07:02,760 --> 00:07:08,200
sit and think about whether this was what you were experiencing am I really experiencing these

75
00:07:08,200 --> 00:07:19,960
three things do I even want to do I accept these three things is true sounds like a somewhat

76
00:07:19,960 --> 00:07:28,680
dubious framework into set your sights and aspiration

77
00:07:33,080 --> 00:07:38,360
but that's not what we best in the means we best in the means to see these things clearly

78
00:07:41,640 --> 00:07:45,560
it's not the understanding you don't even really have to understand them

79
00:07:45,560 --> 00:07:55,480
you don't need intellectual understanding when you practice mindfulness this is what you'll see

80
00:07:57,160 --> 00:08:04,920
how it should be understood is that in our blindness in our lack of clarity

81
00:08:06,040 --> 00:08:09,880
we cultivate all sorts of wrong ideas about things

82
00:08:09,880 --> 00:08:14,760
and more importantly we have a wrong relationship

83
00:08:16,680 --> 00:08:23,320
our perception of things as stable and as predictable as satisfying

84
00:08:25,480 --> 00:08:28,200
our perception of things as me and mine

85
00:08:28,200 --> 00:08:40,840
there's a practice of mindfulness we've come to see that unfortunately this isn't true

86
00:08:40,840 --> 00:08:56,760
the things that we cling to as stable and predictable or not

87
00:08:59,640 --> 00:09:04,200
the things we think of as satisfying actually don't satisfy us they just create more

88
00:09:04,200 --> 00:09:15,480
craving and more suffering eventually don't get what we want

89
00:09:22,200 --> 00:09:28,760
because it's not static because it's a process of cultivation and

90
00:09:28,760 --> 00:09:40,360
habit forming it's always changing always building and being torn down

91
00:09:40,360 --> 00:09:57,160
when we look at things as me and mine and even looking at things as having a reality to them

92
00:09:59,720 --> 00:10:02,120
when we practice mindfulness we see that's not the case

93
00:10:02,120 --> 00:10:12,840
we see that not that it's not the case that it's the wrong way of looking at things that

94
00:10:12,840 --> 00:10:21,880
leads to suffering really it leads to clinging and craving and leads to views and ideas and

95
00:10:21,880 --> 00:10:33,160
leads to perceptions that have no basis in reality

96
00:10:33,160 --> 00:10:41,400
possessiveness all of these things can only come in darkness they can't come through

97
00:10:41,400 --> 00:10:51,480
seeing clearly and it may seem dreary to suggest that the everything it sounds like is bad

98
00:10:53,400 --> 00:10:58,680
it actually is exactly the opposite none of this would be necessary if we weren't suffering if

99
00:10:58,680 --> 00:11:05,640
we weren't stressed if we didn't have anguish and depression and anxiety and fear

100
00:11:05,640 --> 00:11:14,760
all sorts of horrible things in our lives that have nothing to do with meditation practice or

101
00:11:14,760 --> 00:11:21,320
Buddhism you don't need Buddhism to tell you that life can be painful

102
00:11:23,400 --> 00:11:28,280
that we can be overwhelmed by suffering to the point where we don't have a way out

103
00:11:28,280 --> 00:11:43,160
so the claim in Buddhism is that other simple reason and quite a simple practice not easy but

104
00:11:43,160 --> 00:11:50,760
simple practice by which you can change that by which you can free yourself from the stress of

105
00:11:50,760 --> 00:12:04,440
suffering, anguish but ultimately comes from not seeing clearly that when you stop clinging to

106
00:12:04,440 --> 00:12:14,200
anything you stop suffering it's nothing more than that

107
00:12:14,200 --> 00:12:23,880
so I've said before that I talked before about we passing them being a goal and I want to

108
00:12:23,880 --> 00:12:34,200
be granted it's not legal but it's the immediate goal being mindful or mindful for the purpose

109
00:12:34,200 --> 00:12:40,280
of seeing clearly seeing clearly of course as its own purpose and that's the ultimate purpose

110
00:12:40,280 --> 00:12:54,920
but in an immediate sense our goal is to see more clean and continuously increasing our mental

111
00:12:54,920 --> 00:13:11,400
clarity so we're in darkness this is the claim being made even with all of our science and all of our

112
00:13:11,400 --> 00:13:26,600
intellect we're in darkness we're in darkness just means we've never really not even really looked

113
00:13:27,640 --> 00:13:35,080
and even when we look we're not equipped we don't have the faculties we don't have the mental

114
00:13:35,080 --> 00:13:44,600
fortitude and the mental faculties to see things as they are you notice this when you start to

115
00:13:44,600 --> 00:13:53,000
meditate you sit down to meditate and you'll find that immediately you're distracted immediately

116
00:13:53,000 --> 00:14:02,920
you're pulled away you're caught up trying to see things as they are but suddenly an emotion

117
00:14:02,920 --> 00:14:09,240
grips you desire for something else aversion to what you're doing aversion to the experiences

118
00:14:10,680 --> 00:14:12,840
meditation in the beginning isn't very meditative

119
00:14:17,640 --> 00:14:25,480
that's the darkness the inability the incapacity to see clearly something that we have to

120
00:14:25,480 --> 00:14:40,360
cultivate and to understand how we cultivate we all have to understand what it is that we're

121
00:14:40,360 --> 00:14:46,040
trying to see clearly what is it that is impermanent what is it that unsatisfying what is it that's

122
00:14:46,040 --> 00:14:59,160
non-self or uncontrollable and the answer is experience but what we're trying to see clearly is

123
00:15:01,160 --> 00:15:07,560
it may have been sound somewhat I mean doing to us as meditators it sounds kind of obvious but

124
00:15:09,240 --> 00:15:15,800
there's a shift that you undergo in meditation practice our ordinary way of seeing things is

125
00:15:15,800 --> 00:15:25,320
all conceptual we don't realize this but the people the places the things are all conceptual

126
00:15:27,080 --> 00:15:36,280
and our way of seeing is so much outside so much about the world how we see the world what we

127
00:15:36,280 --> 00:15:46,280
think about the world how the world sees us how other people see us it's all of them concepts

128
00:15:48,920 --> 00:15:57,080
who am I what do I look like when is my personality who are you what is that

129
00:15:57,080 --> 00:16:10,040
and so our mental activity is caught up constantly with this concept

130
00:16:13,640 --> 00:16:18,120
when you begin to cultivate mindfulness or set the set the pattern of

131
00:16:18,120 --> 00:16:34,680
the equation your perception shifts and you stop looking gradually move away from

132
00:16:37,080 --> 00:16:44,760
the realm of people in places and things and you start to feel the experience you start to

133
00:16:44,760 --> 00:16:55,080
be present you're aware of the sensations in the body

134
00:16:58,280 --> 00:17:06,120
you're aware of the feelings of pleasure and pain you're aware of the thoughts and the mind

135
00:17:06,120 --> 00:17:14,520
and the emotions and you pull back and you're able to taste this very basic very

136
00:17:14,520 --> 00:17:23,800
prime primary aspect of reality that is experienced before the conceptualizing before

137
00:17:24,360 --> 00:17:31,400
the recognizing of things as people in places and things as good and bad and me and mine before

138
00:17:31,400 --> 00:17:43,400
all that there's experience everything in our lives if you think about it it's the one thing that

139
00:17:43,400 --> 00:17:50,600
that is you can't do without to have even life or anything all of our science is dependent on

140
00:17:50,600 --> 00:17:55,480
the fact that we can see and hear and smell and taste and feel and of course think

141
00:17:55,480 --> 00:18:12,280
cognizant our relationships our ambitions our goals our philosophies all dependent on experience

142
00:18:12,280 --> 00:18:32,440
it's a very most basic aspect of reality and so Satyipatana is the cultivation of the ability to see

143
00:18:32,440 --> 00:18:41,000
this clearly because the problem is we don't see clearly we see we get glimpses of this

144
00:18:41,000 --> 00:18:49,240
in the beginning especially when we begin to meditate but we're quickly taken away as I said by our

145
00:18:49,240 --> 00:19:00,520
emotions by our distractions by our lack of attention our lack of focus our lack of patience

146
00:19:00,520 --> 00:19:19,880
and so Satyipatana is Sati Sati is the Sati is the practice of

147
00:19:19,880 --> 00:19:31,480
the practice by which we come to see clearly Sati literally means to remember or remembering

148
00:19:36,760 --> 00:19:44,760
and it sounds like an odd word to choose but the idea behind it is it's not about remembering

149
00:19:44,760 --> 00:19:53,400
the past to remembering something in the future it's like when we say remember yourself

150
00:19:53,400 --> 00:20:00,600
remember who you are that's not what we're practicing but when we say in English

151
00:20:03,720 --> 00:20:08,600
do you not remember if you've forgotten that you're in the library for example

152
00:20:08,600 --> 00:20:15,320
when someone is yelling you say have you forgotten remember where you are

153
00:20:16,360 --> 00:20:22,200
sometimes say when we're arrogant we might say remember your place

154
00:20:25,080 --> 00:20:26,440
put someone in their place

155
00:20:29,480 --> 00:20:36,520
but all of it points to this concept of losing track of the present moment

156
00:20:36,520 --> 00:20:39,720
that we have in our modern language as well

157
00:20:48,040 --> 00:20:52,280
so this is the linguistic and the reason why we use the word remember

158
00:20:53,720 --> 00:21:00,600
because it's an apt way of looking at the the reality of the situation that we forget

159
00:21:00,600 --> 00:21:14,200
we lose track of the experience in favor of our reactions our judgments our emotions

160
00:21:16,200 --> 00:21:18,520
our views and beliefs all of these things

161
00:21:22,120 --> 00:21:28,920
and so Satyya is about remembering it's about grasping the object facing the object facing

162
00:21:28,920 --> 00:21:33,720
reality and experience right facing experience

163
00:21:34,680 --> 00:21:41,480
Risaya Bhimukkapamu in this state of confronting

164
00:21:43,160 --> 00:21:46,360
confronting experience

165
00:21:46,360 --> 00:21:56,920
so the essence of Satyapatana the essence of what we've come to call mindfulness

166
00:21:59,640 --> 00:22:08,200
is confronting you want to understand it it's not about manufacturing

167
00:22:08,200 --> 00:22:17,560
much meditation is about manufacturing creating new habits

168
00:22:17,560 --> 00:22:30,920
Satyya mindfulness is about confronting our habits confronting our own selves our minds

169
00:22:30,920 --> 00:22:41,400
vatana means the establishment or the foundation

170
00:22:44,600 --> 00:22:45,880
and it refers to this

171
00:22:45,880 --> 00:22:59,400
this aspect of reality we call experience you use experience to cultivate Satyya

172
00:23:01,960 --> 00:23:09,880
so the Buddha outlined four categories of experience there's the physical aspect we call

173
00:23:09,880 --> 00:23:16,600
kai as the body but it means all well it's the physical as we experience it through the body

174
00:23:20,360 --> 00:23:26,760
hardness and softness stiffness heat and cold

175
00:23:32,840 --> 00:23:38,040
so all what when we talk about being mindful of walking walking we're actually being mindful of

176
00:23:38,040 --> 00:23:45,640
the experiences of the stiffness even the heat and the cold as we move our limbs and the hardness

177
00:23:45,640 --> 00:23:51,880
and the softness all of that is just being mindful of the body when we state ourselves

178
00:23:51,880 --> 00:23:57,320
rising falling watching the stomach it's a stiffness it's a physical experience

179
00:23:57,320 --> 00:24:09,240
and what this does is it allows us to see clearly it creates this mental fortitude or mental

180
00:24:09,240 --> 00:24:22,520
capacity to see clearly and to break through the delusion the habits that we have of clinging to

181
00:24:22,520 --> 00:24:33,160
things when we start to see that this thing that I hold on to is really only moments of experience

182
00:24:33,160 --> 00:24:37,160
how can you cling to a moment of experience

183
00:24:43,080 --> 00:24:49,560
how can you want it what is wanting what what are the things that you can want you can't want

184
00:24:49,560 --> 00:24:57,480
a moment of seeing want to get only a rise when there's recognition and this is something that

185
00:24:57,480 --> 00:25:09,880
brought me pleasure in the past it relies upon this stability relies upon controllability

186
00:25:09,880 --> 00:25:25,400
relies upon concept and likewise how can you be averse to something how can you possibly

187
00:25:26,840 --> 00:25:36,840
be angry about a moment of seeing a moment of hearing why this when you hear loud noises and

188
00:25:36,840 --> 00:25:46,600
there again and again why do you get angry it's kind of seems it's it's a maybe not question

189
00:25:46,600 --> 00:25:53,240
but it does seem kind of strange that sound could make us angry and the answers of course sound

190
00:25:53,240 --> 00:26:00,040
can't make you angry there's much more involved there sites and sounds

191
00:26:00,040 --> 00:26:16,280
they only give rise to suffering or well to delusions and mental misconceptions

192
00:26:16,280 --> 00:26:30,520
and therefore suffering they only give rise to them because we don't see clearly there's

193
00:26:30,520 --> 00:26:36,040
no way it in fact seems kind of like a silly thing to do to say to yourself walking walking

194
00:26:36,040 --> 00:26:42,360
why do I care about walking that's not the walking isn't a problem that's exactly the point

195
00:26:42,360 --> 00:26:49,880
walking isn't a problem but seeing also isn't a problem hearing isn't a problem reality isn't a

196
00:26:49,880 --> 00:26:59,640
problem it's a reaction reality are clinging to misconception of it

197
00:26:59,640 --> 00:27:13,400
sometimes an incomprehensible or hard to understand from meditators why we

198
00:27:14,040 --> 00:27:21,480
what say to ourselves I'll see what's the point none of these things are problem why do I care

199
00:27:21,480 --> 00:27:30,520
about this if you can live just by seeing and hearing and spelling and tasting and feeling

200
00:27:30,520 --> 00:27:36,120
and thinking the way we all think we are right I think of ourselves first and foremost as being

201
00:27:36,120 --> 00:27:45,960
here and now but our minds are most often otherwise occupied and judging and reacting and liking

202
00:27:45,960 --> 00:27:59,320
is constantly throughout our lives through our days and every moment and if we could learn

203
00:27:59,320 --> 00:28:04,360
which is what we're trying to do through mindfulness if we could learn to just be here now

204
00:28:04,360 --> 00:28:15,320
that seeing just be seeing there would be no problem

205
00:28:20,760 --> 00:28:26,120
it's not that there's some profound insight to be had from saying to yourself seeing

206
00:28:26,120 --> 00:28:34,280
the scene if you're waiting for that insight you can tell you no stop save yourself in the torture

207
00:28:37,400 --> 00:28:45,800
it's not inside it's clear sight you think you when you say seeing seeing that you see it

208
00:28:45,800 --> 00:28:56,520
as seeing a lot when you first start but you will get catch glimpses right and you will see

209
00:28:57,560 --> 00:29:04,040
now I am knowing this is this right for the movie yes you will see many things along the way

210
00:29:04,040 --> 00:29:10,040
that I was clinging to this and that was why it was causing itself and banging like that

211
00:29:10,040 --> 00:29:19,960
I'm attached to this all of the low bad dosamoha greed anger and delusion all of the many kinds

212
00:29:19,960 --> 00:29:29,480
of these one thing things liking things needing things and not liking things disliking things

213
00:29:29,480 --> 00:29:41,640
boredom sadness depression anxiety blood anxiety boredom sadness fear frustration and then

214
00:29:41,640 --> 00:29:57,800
delusion things like anxiety or restlessness doubt confusion ego arrogance conceit low self-esteem

215
00:29:57,800 --> 00:30:00,760
high self-esteem

216
00:30:10,520 --> 00:30:16,200
there's not other than anything really deep or profound to be had and I don't want to

217
00:30:16,200 --> 00:30:21,160
trivialize the Buddhist teacher I don't mean to but it's really just to get rid of all this stuff

218
00:30:21,160 --> 00:30:27,400
once all that stuff's gone and all the Buddha said deep day deep to mutton boi sit there let's

219
00:30:27,400 --> 00:30:34,760
seeing just be seeing that's really the path that's really the way we're going there's nothing

220
00:30:34,760 --> 00:30:47,320
behind that there's no deep intellectual conversation that needs to be had about what's behind the

221
00:30:47,320 --> 00:30:53,960
sea what does impermanence mean we don't need to really have intellectual conversation about it

222
00:30:58,200 --> 00:31:06,920
when you cultivate mindfulness you'll if anyone ever asks you so what's reality like is it's

223
00:31:06,920 --> 00:31:17,080
stable and constant you'll be able to tell them it's chaotic it's unpredictable very complicated

224
00:31:17,080 --> 00:31:24,360
there's so many factors and it's all mushing together but ultimately it's just moments

225
00:31:25,640 --> 00:31:34,280
seeing just less than hearing less so if you found something that's going to satisfy you if you

226
00:31:34,280 --> 00:31:39,800
hold on to it there's anything that's you know this is dukka which is translated as suffering but

227
00:31:39,800 --> 00:31:46,920
you have to understand if we are too strong on the suffering side people get kind of afraid really

228
00:31:49,400 --> 00:31:56,200
I'm going to use the word suffering because it's used in the sense of causing suffering

229
00:31:58,280 --> 00:32:01,640
like a fire if you say oh that fire over there is really hot huh

230
00:32:01,640 --> 00:32:09,880
well it's not hot unless you feel the heat like hot is of course relative it's it's actually

231
00:32:09,880 --> 00:32:14,680
an aspect of experience without someone to experience there's no such thing as hot

232
00:32:16,920 --> 00:32:19,480
and so the same goes with reality it's suffering

233
00:32:19,480 --> 00:32:31,960
and when you cling to it and when you hold on to meaning it's it's not going to satisfy

234
00:32:31,960 --> 00:32:54,040
that's why we say not satisfy and so you know when you hold on to a person and they're very

235
00:32:54,040 --> 00:33:01,400
common ones right people we cling to is a person I love this person so much and they make me so

236
00:33:01,400 --> 00:33:10,600
happy and then weigh on something to do something you don't like it's unpredictable we don't

237
00:33:10,600 --> 00:33:16,840
think of it that way but that's the truth of it even when a person dies the truth of it is that

238
00:33:18,200 --> 00:33:25,480
experience has changed we don't stop seeing or hearing or smelling or tasting or feeling and

239
00:33:25,480 --> 00:33:34,920
thinking but the concept which we clung to has stable and satisfying as controllable predictable

240
00:33:36,360 --> 00:33:48,360
it's not so it's someone as curious is there something that's going to satisfy is there something

241
00:33:48,360 --> 00:34:01,880
that you can cling to is only clinging it's not good what about self is there any good is

242
00:34:01,880 --> 00:34:06,440
there any reason or any rationale to cling to thing to hold on to something to hold something

243
00:34:06,440 --> 00:34:16,040
as self or to try to possess things to really conceive of things as me and mine it's an interesting

244
00:34:16,040 --> 00:34:26,120
question because me and mine is such an important part of our lives so it was quite profound

245
00:34:26,120 --> 00:34:32,840
and this is the profundity so it's very simple but it was such a blow and it still sets your

246
00:34:32,840 --> 00:34:41,080
shock when people hear that the Buddha taught he taught against this he criticized he said this

247
00:34:41,080 --> 00:34:53,480
is a hunger mamanga I'm making my making to actually question that question possession

248
00:34:55,480 --> 00:35:04,440
my mamanga but there's nothing that's me or mine it doesn't even have a place

249
00:35:04,440 --> 00:35:12,360
there's nothing by which a moment of experiencing seeing doesn't have anything to do with me

250
00:35:14,440 --> 00:35:19,240
that I've except in the intellectual sense of it's me who's experiencing it but there's no

251
00:35:19,240 --> 00:35:27,080
me involved it's not like my soul suddenly jumps out and sees things it's just a moment of experience

252
00:35:27,080 --> 00:35:36,440
it's gone there's no mind to it either it comes and it goes

253
00:35:45,160 --> 00:35:50,120
and because reality is made up of these moments of experience our attachments the thing

254
00:35:50,120 --> 00:35:56,760
is it's always going to be fraught with problems because they come and they go and they break the

255
00:35:56,760 --> 00:36:08,120
change they're not real they're not a good description of what is real reality is moment

256
00:36:13,240 --> 00:36:19,640
so I think a lot of this is familiar familiar territory but I wanted to start at the beginning

257
00:36:19,640 --> 00:36:25,880
normally when every time you come to do a course with my teacher you're always cool with it's

258
00:36:25,880 --> 00:36:37,800
again so in reference to that I think instead of where we're we we don't do a formal ceremony

259
00:36:37,800 --> 00:36:53,960
here we may we could we have before but I've taken it I've taken on the the benefit and the

260
00:36:53,960 --> 00:37:04,520
goodness and the virtue of virtue of informality I think there is a virtue to it there can be a

261
00:37:04,520 --> 00:37:12,920
virtue to ceremony and formality but informality helps remind us that this is just like this is

262
00:37:12,920 --> 00:37:22,920
not some religious or mystical or magical thing it really isn't doing the dishes and

263
00:37:22,920 --> 00:37:29,720
sweeping the floor and eating and drinking and urinating and defecating and showering

264
00:37:29,720 --> 00:37:37,240
and we shouldn't separate our practice or your life here I don't want this to become a

265
00:37:38,120 --> 00:37:41,560
you know we don't want to reify the meditation course experience

266
00:37:44,760 --> 00:37:49,160
shouldn't be reifying meaning make it into a thing so you come here wow I did a meditation

267
00:37:49,160 --> 00:37:53,000
course then you go home you're no longer meditating

268
00:37:53,000 --> 00:38:04,920
that being said I think we could there is benefit to the formality of a ceremony and

269
00:38:06,280 --> 00:38:13,320
subsequent talks but consider this to be our introduction talk because we've started fresh

270
00:38:13,320 --> 00:38:24,600
it's the new year and you're all freshly come here to the center you know thank you all for

271
00:38:24,600 --> 00:38:31,400
being patient and listening it's a great thing to come together to listen to the dhamma whoever

272
00:38:31,400 --> 00:38:42,760
is teaching it so appreciate and appreciate all your practice continuing practice here so have a good

273
00:38:42,760 --> 00:38:48,920
night let's see you all in the morning

274
00:38:48,920 --> 00:38:59,960
I also recorded this on the internet I don't know if they can actually hear

