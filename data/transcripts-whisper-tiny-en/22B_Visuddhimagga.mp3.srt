1
00:00:00,000 --> 00:00:06,000
And sympathetic joy or cleanness, Mujita.

2
00:00:06,000 --> 00:00:09,000
It's near enemy, what?

3
00:00:09,000 --> 00:00:13,000
It's joy based on the home life.

4
00:00:13,000 --> 00:00:22,000
That means becoming married, joyful with attachment.

5
00:00:22,000 --> 00:00:26,000
Such joy has been described.

6
00:00:26,000 --> 00:00:33,000
And a version of Buddha or non-delight, which is dissimilar to the similar joy, it's far enemy.

7
00:00:33,000 --> 00:00:37,000
So something like, Henry, it's far enemy.

8
00:00:37,000 --> 00:00:43,000
So gladness will be practiced free from fear or danger of that.

9
00:00:43,000 --> 00:00:50,000
And then equanimity has the equanimity of unknowing based on the home life as it's near enemy.

10
00:00:50,000 --> 00:00:53,000
That is not knowing, ignorance.

11
00:00:53,000 --> 00:01:05,000
The ignorance, which is not accompanied by pleasurable feeling or displaceable feeling.

12
00:01:05,000 --> 00:01:10,000
So accompanied by indifferent feeling and it is ignorance.

13
00:01:10,000 --> 00:01:15,000
Since Buddha shared and ignoring false and virtues.

14
00:01:15,000 --> 00:01:26,000
And then it's, and greed and resentment, which are dissimilar to similar unknowing, are it's far enemies.

15
00:01:26,000 --> 00:01:32,000
So it's far enemies are greed and resentment.

16
00:01:32,000 --> 00:01:37,000
You can either fall into greed or into resentment.

17
00:01:37,000 --> 00:01:39,000
And they are far enemies.

18
00:01:39,000 --> 00:01:47,000
Therefore, equanimity must be practiced free from fear or danger of that.

19
00:01:47,000 --> 00:01:55,000
So each of these has two kinds of enemies, near enemies and far enemies.

20
00:01:55,000 --> 00:02:00,000
So we should be careful both about mere enemies and far enemies, right?

21
00:02:00,000 --> 00:02:03,000
We practiced.

22
00:02:03,000 --> 00:02:05,000
And the beginning, middle, and end.

23
00:02:05,000 --> 00:02:12,000
Now, see, see is the Pali Chanda consisting and desire to act is the beginning of all these things.

24
00:02:12,000 --> 00:02:16,000
Suppression of the hindrance, etc. is the middle, absorption is the end.

25
00:02:16,000 --> 00:02:18,000
So Jana is the end.

26
00:02:18,000 --> 00:02:26,000
Their object is a single living being or many living beings has a mental object consisting in a concept.

27
00:02:26,000 --> 00:02:30,000
Because beings are concept, it is called concept.

28
00:02:30,000 --> 00:02:32,000
Then the order of extension.

29
00:02:32,000 --> 00:02:41,000
Now, here in the order, the extension of the object takes place either in excess or in absorption.

30
00:02:41,000 --> 00:02:43,000
Here is the order of it.

31
00:02:43,000 --> 00:02:47,000
Just as a skilled problem and first delimits an area and then just is flying.

32
00:02:47,000 --> 00:02:52,000
So first, a single dwelling should be delimited and loving kindness developed to watch all beings.

33
00:02:52,000 --> 00:02:57,000
They are in the way beginning, and this dwelling may all beings be free from energy.

34
00:02:57,000 --> 00:03:15,000
And then when you have gained experience with that, then you can extend your range of loving kindness by taking in two dwellings, then three dwellings, full dwellings, and so on.

35
00:03:15,000 --> 00:03:21,000
And the whole village, the district, the kingdom, one direction, and so on.

36
00:03:21,000 --> 00:03:30,000
To one wall sphere, or even beyond that, and develop loving kindness towards the beings in such areas.

37
00:03:30,000 --> 00:03:35,000
That is why I want to teach loving kindness, I teach in two ways.

38
00:03:35,000 --> 00:03:46,000
The first is by way of persons, may I be well happy and peaceful, may my teach as we were happy and peaceful, may my parents be well happy and peaceful, and so on.

39
00:03:46,000 --> 00:04:04,000
And the other way is, may all beings in this building we were happy and peaceful, and may all beings in this area, in this city, in this county, in this state, in this country, in this world we were happy and peaceful, in this universe, and then we all beings in general we were happy and peaceful.

40
00:04:04,000 --> 00:04:10,000
So this is extending a little by little.

41
00:04:10,000 --> 00:04:22,000
Now the outcome, what results can you get from the practice of these four kinds of divine abidings.

42
00:04:22,000 --> 00:04:39,000
Just as the immaterial states are the outcome of the casinos, and the base consisting of neither perception or non-perception is the outcome of concentration, and there should be a corner there.

43
00:04:39,000 --> 00:04:48,000
And fruition attainment is the outcome of insight, and the attainment of cessation is the outcome of serenity coupled with insight.

44
00:04:48,000 --> 00:04:54,000
So the divine abidings of equanimity is the outcome of the first three divine abidings.

45
00:04:54,000 --> 00:05:06,000
Because in order to be successful, in the fourth one you have to be successful first in the three receding divine abidings.

46
00:05:06,000 --> 00:05:22,000
For just as the gable raptors cannot be placed in the air without having first set up this scaffolding and built the framework of beings, so it is not possible to develop the fourth genre in these without having already developed the third genre in there.

47
00:05:22,000 --> 00:05:29,000
Now in this paragraph it is said that immaterial states are the outcome of the casinos.

48
00:05:29,000 --> 00:05:45,000
The arena really means genres developed on casinos. You take casinos as the object of meditation, and then you practice my decision and you get genre.

49
00:05:45,000 --> 00:06:03,000
If you do not get root power with a genre, then you do not get a root power with a genre. That is why it is said that the casinos are and the immaterial states are the outcome of casino genres.

50
00:06:03,000 --> 00:06:16,000
The base of the and the base consisting of neither perception or non-perception is the outcome of concentration.

51
00:06:16,000 --> 00:06:21,000
The fourth are root power genre. Is the outcome of the third are root power genre genre concentration.

52
00:06:21,000 --> 00:06:24,000
And fruition attainment is the outcome of insight.

53
00:06:24,000 --> 00:06:31,000
Now when you practice, we must remember the decision and you become enlightened.

54
00:06:31,000 --> 00:06:35,000
When you gain enlightenment, you gain fruition attainment.

55
00:06:35,000 --> 00:06:39,000
So fruition attainment is the outcome of insight meditation.

56
00:06:39,000 --> 00:06:44,000
And the attainment of cessation is the outcome of salinity coupled with insight.

57
00:06:44,000 --> 00:06:48,000
Now there is an attainment called cessation attainment.

58
00:06:48,000 --> 00:06:54,000
That means cessation of mental activities.

59
00:06:54,000 --> 00:07:02,000
It is a very highly developed type of genre.

60
00:07:02,000 --> 00:07:13,000
And in order to get into that genre, you must first have all the eight or nine genres.

61
00:07:13,000 --> 00:07:27,000
So you enter into first genre and then imagine from it and then enter into the second genre genre and so on, until you reach the full root power genre genre.

62
00:07:27,000 --> 00:07:38,000
And from first genre after, after emerging from the first genre, you practice with person out there.

63
00:07:38,000 --> 00:07:43,000
And then you enter into the second genre that is samata salinity meditation.

64
00:07:43,000 --> 00:07:48,000
And then you practice with person out on that genre or on the factors of genre genre.

65
00:07:48,000 --> 00:07:53,000
So we person now in samata go in a coupled way.

66
00:07:53,000 --> 00:07:59,000
So it is stated here that outcome of salinity coupled with insight.

67
00:07:59,000 --> 00:08:12,000
The explanation of this attainment will be given at the end of the book.

68
00:08:12,000 --> 00:08:17,000
So the divine abiding of equanimity is the outcome of the first three divine abiding and so on.

69
00:08:17,000 --> 00:08:19,000
Now questions.

70
00:08:19,000 --> 00:08:26,000
But why are the loving kind of combustion gladness and equanimity called divine abiding and so on?

71
00:08:26,000 --> 00:08:34,000
Now, the word, the body word for divine abiding is Brahma Vihara.

72
00:08:34,000 --> 00:08:36,000
Vihara means living.

73
00:08:36,000 --> 00:08:40,000
So it is here translated as abiding.

74
00:08:40,000 --> 00:08:45,000
And Brahma, the word Brahma has two meanings here.

75
00:08:45,000 --> 00:08:47,000
One is the best.

76
00:08:47,000 --> 00:08:50,000
And the other is faultless.

77
00:08:50,000 --> 00:08:52,000
So immaculate.

78
00:08:52,000 --> 00:09:00,000
And these abiding are called the best because they are the right attitude towards beings.

79
00:09:00,000 --> 00:09:02,000
So these are the right attitudes we have.

80
00:09:02,000 --> 00:09:04,000
We should have towards beings.

81
00:09:04,000 --> 00:09:07,000
And so they are called the best.

82
00:09:07,000 --> 00:09:11,000
And just as Brahma got abide with immaculate minds,

83
00:09:11,000 --> 00:09:17,000
so the metadata, so associate themselves with these abiding's abide on an equal footing with Brahma gods.

84
00:09:17,000 --> 00:09:23,000
So the Braamas immaculate, immaculate minds.

85
00:09:23,000 --> 00:09:32,000
So their minds are not contaminated by some mental defalments.

86
00:09:32,000 --> 00:09:41,000
And the person who practices any one of these meditations is like a Brahma.

87
00:09:41,000 --> 00:09:48,000
Because the Brahma gods in the world of the Brahma,

88
00:09:48,000 --> 00:09:55,000
always fight these full divine abiding.

89
00:09:55,000 --> 00:10:00,000
Because they live with these full divine abiding, they have no other thing to do.

90
00:10:00,000 --> 00:10:04,000
So these are called Brahma Vihara.

91
00:10:04,000 --> 00:10:12,000
The abiding like that of the Braamas, we can call that too.

92
00:10:12,000 --> 00:10:13,000
We can see that too.

93
00:10:13,000 --> 00:10:18,000
So they are called divine abiding in the sense of best and in the sense of immaculate.

94
00:10:18,000 --> 00:10:27,000
So best way of abiding or false less way of abiding.

95
00:10:27,000 --> 00:10:29,000
Why are they only four?

96
00:10:29,000 --> 00:10:38,000
And then the answer given is because the way to purity is full-fold.

97
00:10:38,000 --> 00:10:44,000
Loving kindness is the way to purity for one who has much ill will.

98
00:10:44,000 --> 00:10:47,000
Compassion is that for one who has much cruelty.

99
00:10:47,000 --> 00:10:53,000
Blackness is that for one who has much non-delight.

100
00:10:53,000 --> 00:11:02,000
And equality is that for one who has much greed.

101
00:11:02,000 --> 00:11:06,000
Crownity?

102
00:11:06,000 --> 00:11:12,000
And attention given to beings is only full-fold.

103
00:11:12,000 --> 00:11:20,000
That is to say, as bringing welfare, as removing suffering, as being glad that their success and as unconcerned.

104
00:11:20,000 --> 00:11:23,000
That is to say, in partial neutrality.

105
00:11:23,000 --> 00:11:25,000
That is the second reason.

106
00:11:25,000 --> 00:11:30,000
The third reason is one abiding in measureless states should practice loving kindness and

107
00:11:30,000 --> 00:11:35,000
the rest like a mother with full sons, namely a child and invalid one in the flesh of

108
00:11:35,000 --> 00:11:38,000
you than one busy with his own affairs.

109
00:11:38,000 --> 00:11:42,000
For she wants the child to grow up, wants the invalid to get well.

110
00:11:42,000 --> 00:11:46,000
Once the one in the flesh of you to enjoy for long the benefits of you.

111
00:11:46,000 --> 00:11:50,000
And it is not at all but that about the one who is busy with his own affairs.

112
00:11:50,000 --> 00:11:52,000
So this food from where we are at.

113
00:11:52,000 --> 00:11:56,000
That is why the measureless states are only full.

114
00:11:56,000 --> 00:11:59,000
You do first purity and other sets of full.

115
00:11:59,000 --> 00:12:03,000
Now the order.

116
00:12:03,000 --> 00:12:10,000
The order is just loving kindness, compassion, gladness, and anonymity.

117
00:12:10,000 --> 00:12:17,000
Now why are they called measureless?

118
00:12:17,000 --> 00:12:19,000
For their scope is measureless beings.

119
00:12:19,000 --> 00:12:27,000
That means their object is beings without measure, without exception.

120
00:12:27,000 --> 00:12:31,000
So all of them occur with a measureless scope.

121
00:12:31,000 --> 00:12:33,000
For their scope is measureless beings.

122
00:12:33,000 --> 00:12:37,000
And instead of assuming a measure such as loving kindness, etc.

123
00:12:37,000 --> 00:12:42,000
The trust will be developed only to what is single being or in an area of such an extent

124
00:12:42,000 --> 00:12:45,000
the occur with universal provision.

125
00:12:45,000 --> 00:12:50,000
What does that mean?

126
00:12:50,000 --> 00:13:06,000
Then when you practice loving kindness to a person, it cannot be called measureless.

127
00:13:06,000 --> 00:13:19,000
Now the translation is little off the mark.

128
00:13:19,000 --> 00:13:27,000
What is meant here or what is written by the original order is even to watch one person.

129
00:13:27,000 --> 00:13:32,000
Even when you are practicing to watch one person it can be called measureless.

130
00:13:32,000 --> 00:13:41,000
Because you do not limit your loving kindness to one part of his body.

131
00:13:41,000 --> 00:13:43,000
That is what is meant here.

132
00:13:43,000 --> 00:13:52,000
So even in regard to a single being the occur with universal provision, without assuming a measure

133
00:13:52,000 --> 00:13:57,000
as loving kindness should be developed in this much part of him.

134
00:13:57,000 --> 00:14:03,000
See, something like, he has had to be aware of him being beautiful now.

135
00:14:03,000 --> 00:14:07,000
Maybe the upper part of his body will have been peaceful or so.

136
00:14:07,000 --> 00:14:12,000
So you take the whole being.

137
00:14:12,000 --> 00:14:18,000
So even with regard to a single person it can be called measureless.

138
00:14:18,000 --> 00:14:22,000
That is what is meant here.

139
00:14:22,000 --> 00:14:27,000
And then producing three genres and four genres.

140
00:14:27,000 --> 00:14:32,000
So the first three can produce how many genres?

141
00:14:32,000 --> 00:14:35,000
Three or four.

142
00:14:35,000 --> 00:14:40,000
The last one can produce the folds or the fifth.

143
00:14:40,000 --> 00:14:49,000
Only one genre.

144
00:14:49,000 --> 00:14:56,000
Now there is a discussion here with an opponent.

145
00:14:56,000 --> 00:15:04,000
Now from paragraph 112 the argument and counter argument something like that.

146
00:15:04,000 --> 00:15:11,000
So the opponent finds it to a soda.

147
00:15:11,000 --> 00:15:21,000
And he took the soda to mean that the all four or five genres can be obtained by the practice of Mita,

148
00:15:21,000 --> 00:15:29,000
Karuna and Mudita also.

149
00:15:29,000 --> 00:15:40,000
The form we see in that soda may point to that.

150
00:15:40,000 --> 00:15:44,000
If you are not really familiar with Abidama.

151
00:15:44,000 --> 00:15:50,000
Buddha said, see after you practice loving kindness then you should go on

152
00:15:50,000 --> 00:15:56,000
by making this loving kindness with Vittaka and Vittaka and Vittaka and Vittaka,

153
00:15:56,000 --> 00:15:59,000
but with Vittaka and so on.

154
00:15:59,000 --> 00:16:09,000
So when you read it superficially you may also think that you can get all four or five genres through Mita,

155
00:16:09,000 --> 00:16:11,000
Mita, meditation and so on.

156
00:16:11,000 --> 00:16:13,000
And that is not so.

157
00:16:13,000 --> 00:16:18,000
So the other said, he should be told, do not put it like that.

158
00:16:18,000 --> 00:16:22,000
For if that was so then contemplation of the body etc.

159
00:16:22,000 --> 00:16:26,000
But also have quadruple and quadruple genre.

160
00:16:26,000 --> 00:16:32,000
Because Buddha taught about contemplation of body also in the same way.

161
00:16:32,000 --> 00:16:36,000
But contemplation of the body can produce only the first genre,

162
00:16:36,000 --> 00:16:38,000
not the other genres.

163
00:16:38,000 --> 00:16:41,000
It is mentioned in the back.

164
00:16:41,000 --> 00:16:47,000
And there is not even the first genre in the contemplation of feeling

165
00:16:47,000 --> 00:16:50,000
or in the other two.

166
00:16:50,000 --> 00:16:55,000
Now, or in the other two is strong.

167
00:16:55,000 --> 00:17:05,000
So in its place we should have not even the first genre in the contemplation of feeling.

168
00:17:05,000 --> 00:17:08,000
Let alone the second and so on.

169
00:17:08,000 --> 00:17:10,000
So please collect that.

170
00:17:10,000 --> 00:17:13,000
Let alone the second and so on.

171
00:17:13,000 --> 00:17:16,000
So when you practice feeling contemplation,

172
00:17:16,000 --> 00:17:20,000
you cannot get even the first genre.

173
00:17:20,000 --> 00:17:24,000
Let alone second, third and fifth.

174
00:17:24,000 --> 00:17:28,000
So do not misinterpret.

175
00:17:28,000 --> 00:17:31,000
I have misrepresented the Buddha, blessed one,

176
00:17:31,000 --> 00:17:36,000
but at here let's do the letter and then he explained this.

177
00:17:36,000 --> 00:17:43,000
Now, about five lines down,

178
00:17:43,000 --> 00:17:45,000
about 114.

179
00:17:45,000 --> 00:17:50,000
But the blessed one had no confidence yet in that vehicle.

180
00:17:50,000 --> 00:17:54,000
That was the word I was talking to you about.

181
00:17:54,000 --> 00:17:58,000
So actually the Buddha scolded that monk

182
00:17:58,000 --> 00:18:02,000
by saying these people,

183
00:18:02,000 --> 00:18:05,000
I think that is some misguided men.

184
00:18:05,000 --> 00:18:08,000
We are requesting me and when the dumbass expounded to them,

185
00:18:08,000 --> 00:18:11,000
they still fancy that they need,

186
00:18:11,000 --> 00:18:17,000
they still fancy that they need to follow me, something like that.

187
00:18:17,000 --> 00:18:19,000
So that monk asked the Buddha to teach him

188
00:18:19,000 --> 00:18:21,000
and Buddha taught him and he didn't practice.

189
00:18:21,000 --> 00:18:25,000
And then he again asked Buddha to teach him.

190
00:18:25,000 --> 00:18:29,000
But Buddha saw that he had capability to become an aran.

191
00:18:29,000 --> 00:18:35,000
So this time the Buddha taught him and this.

192
00:18:35,000 --> 00:18:44,000
So school, can you give me a more appropriate word?

193
00:18:44,000 --> 00:18:48,000
Dismissed with something.

194
00:18:48,000 --> 00:18:57,000
School that he rejected.

195
00:18:57,000 --> 00:19:00,000
Chestace, chestace, chestace.

196
00:19:00,000 --> 00:19:04,000
Very good, yes.

197
00:19:04,000 --> 00:19:10,000
So Buddha chastised him and not had no confidence yet in him.

198
00:19:10,000 --> 00:19:14,000
He misread the word, the right word,

199
00:19:14,000 --> 00:19:20,000
with a long division.

200
00:19:20,000 --> 00:19:27,000
So Buddha chastised the monk.

201
00:19:27,000 --> 00:19:33,000
And the meaning here is, although when you read the father,

202
00:19:33,000 --> 00:19:38,000
it seemed to me that you can practice loving kindness,

203
00:19:38,000 --> 00:19:42,000
meditation, and then you can get first again that full fifth

204
00:19:42,000 --> 00:19:47,000
from the practice of loving kindness meditation.

205
00:19:47,000 --> 00:19:52,000
But what Buddha meant there was that you practice meditation,

206
00:19:52,000 --> 00:19:56,000
midterm meditation, and you may get, say, first journal,

207
00:19:56,000 --> 00:19:58,000
second journal, third journal.

208
00:19:58,000 --> 00:20:00,000
But do not be content with that.

209
00:20:00,000 --> 00:20:06,000
Try to get other higher journals by taking other subjects of meditation.

210
00:20:06,000 --> 00:20:08,000
Not midterm meditation.

211
00:20:08,000 --> 00:20:12,000
If you want to get mid-fourth of fifth journal,

212
00:20:12,000 --> 00:20:16,000
then you have to practice other kind of meditation,

213
00:20:16,000 --> 00:20:18,000
not midterm meditation.

214
00:20:18,000 --> 00:20:21,000
Because by the practice of midterm meditation,

215
00:20:21,000 --> 00:20:25,000
you can get only to the third or the fourth journal.

216
00:20:25,000 --> 00:20:35,000
That is what is meant.

217
00:20:35,000 --> 00:20:42,000
In the footnote on page 349,

218
00:20:42,000 --> 00:20:47,000
mere unification of the mind, the kind of concentration,

219
00:20:47,000 --> 00:20:49,000
that is anti-velo.

220
00:20:49,000 --> 00:20:52,000
I think that is not too developed.

221
00:20:52,000 --> 00:20:54,000
Not anti-velo.

222
00:20:54,000 --> 00:20:59,000
Because it is a little developed, but not developed enough.

223
00:20:59,000 --> 00:21:03,000
So that is not too developed and just obtained by one

224
00:21:03,000 --> 00:21:16,000
in pursuit of development and so on.

225
00:21:16,000 --> 00:21:25,000
Now, next we will go to paragraph 119.

226
00:21:25,000 --> 00:21:30,000
The highest limit of each.

227
00:21:30,000 --> 00:21:35,000
While they are too full by weight of the triple quadruple

228
00:21:35,000 --> 00:21:37,000
Jana and the single remaining Jana,

229
00:21:37,000 --> 00:21:40,000
still they should be understood to be distinguishable

230
00:21:40,000 --> 00:21:42,000
in each case by different efficacy,

231
00:21:42,000 --> 00:21:45,000
consisting and having beauty as the highest,

232
00:21:45,000 --> 00:21:47,000
et cetera, for the SO describing,

233
00:21:47,000 --> 00:21:49,000
the halibbean was an asoda and so on.

234
00:21:49,000 --> 00:21:56,000
Now, beauty as the highest is not a good translation for the watch.

235
00:21:56,000 --> 00:22:06,000
That is liberation with the beautiful as object.

236
00:22:06,000 --> 00:22:14,000
Liberation with the beautiful as object.

237
00:22:14,000 --> 00:22:31,000
And here liberation means Jana's, Jana's which take pleasant things as objects, something like that.

238
00:22:31,000 --> 00:22:43,000
So, loving kind is the basic example for the liberation with the beautiful as object.

239
00:22:43,000 --> 00:22:48,000
And one who abides in compression has come to know thoroughly

240
00:22:48,000 --> 00:22:52,000
that danger in materiality since compression is aroused in him

241
00:22:52,000 --> 00:22:56,000
when he sees the suffering of beings that has its,

242
00:22:56,000 --> 00:22:59,000
I think, material centered core.

243
00:22:59,000 --> 00:23:07,000
That is, as its cause, form, be done with sticks and so on.

244
00:23:07,000 --> 00:23:13,000
You get it, send us that.

245
00:23:13,000 --> 00:23:16,000
One, twenty-one.

246
00:23:16,000 --> 00:23:22,000
It has its cause, form, be done with sticks.

247
00:23:22,000 --> 00:23:25,000
Forming the body.

248
00:23:25,000 --> 00:23:30,000
So it should be one who abides in compassion has come to know thoroughly the danger.

249
00:23:30,000 --> 00:23:33,000
In materiality is clear as fuck is not so good.

250
00:23:33,000 --> 00:23:41,000
In materiality, since compression is aroused in him when he sees the suffering of beings that has its cause,

251
00:23:41,000 --> 00:23:45,000
form, be done with sticks and so on.

252
00:23:45,000 --> 00:23:59,000
It has as its cause, form, be done with sticks and so on.

253
00:23:59,000 --> 00:24:10,000
And then, one hundred and twenty-three.

254
00:24:10,000 --> 00:24:16,000
There is a lot of existence and non-existent things in that passage.

255
00:24:16,000 --> 00:24:25,000
So what you should keep in mind is happiness and pain,

256
00:24:25,000 --> 00:24:35,000
sukha and dukha are said to be existent because they belong to paramata.

257
00:24:35,000 --> 00:24:48,000
They belong to ultimate reality because sukha, as well as dukha are among the mental factors.

258
00:24:48,000 --> 00:24:55,000
So sukha is a pleasurable feeling and dukha is a pleasurable feeling.

259
00:24:55,000 --> 00:25:00,000
And so they are among the fifty-two mental states, mental factors.

260
00:25:00,000 --> 00:25:04,000
And so they are said to be existent things.

261
00:25:04,000 --> 00:25:11,000
But a being is non-existent according to ultimate reality or in the ultimate sense.

262
00:25:11,000 --> 00:25:18,000
A being is just a combination of mind and matter or combination of five aggregates.

263
00:25:18,000 --> 00:25:22,000
And so when we say a being, it is non-existent.

264
00:25:22,000 --> 00:25:29,000
And when we say sukha and dukha, then sukha and dukha existent.

265
00:25:29,000 --> 00:25:37,000
So if you keep that in mind, then I think you can understand this paragraph 123.

266
00:25:37,000 --> 00:25:47,000
So upika is not interested in the heaviness of beings or in suffering of beings.

267
00:25:47,000 --> 00:25:54,000
It takes just the beings as an object, not the heaviness or suffering of beings,

268
00:25:54,000 --> 00:25:57,000
not the sukha or dukha of beings.

269
00:25:57,000 --> 00:26:08,000
So a person who practices upika meditation becomes skilled in taking what is non-existent

270
00:26:08,000 --> 00:26:12,000
that is beings as object.

271
00:26:12,000 --> 00:26:20,000
So it is easier for him for such a person to take the non-existent,

272
00:26:20,000 --> 00:26:25,000
the other kind of non-existent as object.

273
00:26:25,000 --> 00:26:35,000
And that is the biggest consisting of, which is non-existent to the individual essence of consciousness,

274
00:26:35,000 --> 00:26:38,000
which is the reality.

275
00:26:38,000 --> 00:26:48,000
Now, the third alubabhajara, the third immaterial jhana is what?

276
00:26:48,000 --> 00:26:53,000
Non-existence of the first alubabhajara jhana.

277
00:26:53,000 --> 00:27:00,000
So non-existence, non-existence is itself non-existent.

278
00:27:00,000 --> 00:27:05,000
Non-existence is a concept, it is not reality.

279
00:27:05,000 --> 00:27:14,000
So the person who is skilled in taking concepts as objects,

280
00:27:14,000 --> 00:27:22,000
when he practices with upika meditation, will find it very easy to take non-existence

281
00:27:22,000 --> 00:27:27,000
or nothingness of the first consciousness as object,

282
00:27:27,000 --> 00:27:34,000
when he tries to get the immaterial jhana's.

283
00:27:34,000 --> 00:27:40,000
Now, in order to get the third alubabhajara jhana,

284
00:27:40,000 --> 00:27:49,000
you have to take the absence of fast alubabhajara jhana as object.

285
00:27:49,000 --> 00:27:54,000
You know, alubabhajara jhana, it's immaterial jhana's.

286
00:27:54,000 --> 00:28:01,000
Most of the jhana's we are talking about are the material jhana's.

287
00:28:01,000 --> 00:28:08,000
So to get to the immaterial jhana's, we describe it in the coming chapter.

288
00:28:08,000 --> 00:28:11,000
Do you have the same as formulas?

289
00:28:11,000 --> 00:28:15,000
Oh yes, formulas, right, formulas jhana.

290
00:28:15,000 --> 00:28:20,000
So in order to get the third formulas jhana,

291
00:28:20,000 --> 00:28:29,000
you have to meditate on the absence of the first alubabhajara jhana.

292
00:28:29,000 --> 00:28:32,000
So that absence is nothing.

293
00:28:32,000 --> 00:28:35,000
So that is just a concept.

294
00:28:35,000 --> 00:28:40,000
So if you are skilled in taking concepts as object,

295
00:28:40,000 --> 00:28:45,000
you practice this alubabhajara jhana, then you will find it easier here

296
00:28:45,000 --> 00:28:48,000
to take the nothingness as object.

297
00:28:48,000 --> 00:28:56,000
So that is what is explained in paragraph 123.

298
00:28:56,000 --> 00:29:00,000
The sentences are long and involved.

299
00:29:00,000 --> 00:29:03,000
So it's not so easy to understand.

300
00:29:03,000 --> 00:29:07,000
So please keep in mind that,

301
00:29:07,000 --> 00:29:12,000
when it's a existent, it means sukhan tokatya.

302
00:29:12,000 --> 00:29:16,000
And when it says non existent, that means the beings.

303
00:29:16,000 --> 00:29:26,000
Because the beings are non existent in the ultimate sense.

304
00:29:26,000 --> 00:29:34,000
Okay, the others are not difficult to understand.

305
00:29:34,000 --> 00:29:38,000
The footnote number 20 is very good.

306
00:29:38,000 --> 00:29:45,000
Because even in the Burmese edition,

307
00:29:45,000 --> 00:29:47,000
there is a mistake.

308
00:29:47,000 --> 00:29:50,000
And in the singleist edition, the reading.

309
00:29:50,000 --> 00:29:52,000
I think the reading is right,

310
00:29:52,000 --> 00:29:54,000
and he took the singleist edition here.

311
00:29:54,000 --> 00:29:57,000
You may not understand this,

312
00:29:57,000 --> 00:30:00,000
but here he says reading in both cases,

313
00:30:00,000 --> 00:30:04,000
how is your monogana, dark khan?

314
00:30:04,000 --> 00:30:07,000
And not dukkhan?

315
00:30:07,000 --> 00:30:10,000
Dark khan and dukkhan?

316
00:30:10,000 --> 00:30:13,000
They have two very different meanings.

317
00:30:13,000 --> 00:30:16,000
Dark khan, dark khan means skilled

318
00:30:16,000 --> 00:30:18,000
and dukkhan means suffering.

319
00:30:18,000 --> 00:30:22,000
So here the correct reading should be dark khan and not dukkhan.

320
00:30:22,000 --> 00:30:27,000
Just the difference of A and U.

321
00:30:27,000 --> 00:30:36,000
So he detected this, and very good.

322
00:30:36,000 --> 00:30:41,000
And then on page 353,

323
00:30:41,000 --> 00:30:43,000
it is interesting here.

324
00:30:43,000 --> 00:30:46,000
Because by the practice of these,

325
00:30:46,000 --> 00:30:50,000
you also fulfill the ten parameters.

326
00:30:50,000 --> 00:30:52,000
In the Theravada Buddhism,

327
00:30:52,000 --> 00:30:55,000
there are ten parameters to be fulfilled,

328
00:30:55,000 --> 00:30:57,000
especially by bodhisattas,

329
00:30:57,000 --> 00:31:01,000
and also by any person.

330
00:31:01,000 --> 00:31:03,000
So these ten parameters,

331
00:31:03,000 --> 00:31:05,000
you mentioned,

332
00:31:05,000 --> 00:31:07,000
and to all beings,

333
00:31:07,000 --> 00:31:09,000
they give gifts.

334
00:31:09,000 --> 00:31:11,000
That means giving.

335
00:31:11,000 --> 00:31:16,000
That is the first parameter.

336
00:31:16,000 --> 00:31:19,000
And in order to avoid doing harm to beings,

337
00:31:19,000 --> 00:31:22,000
they undertake the precepts of virtue.

338
00:31:22,000 --> 00:31:24,000
That is the second parameter.

339
00:31:24,000 --> 00:31:26,000
Because the first is Sila,

340
00:31:26,000 --> 00:31:28,000
and the second, I mean,

341
00:31:28,000 --> 00:31:29,000
the first is Dana,

342
00:31:29,000 --> 00:31:31,000
and the second Sila.

343
00:31:31,000 --> 00:31:33,000
They practice renunciation.

344
00:31:33,000 --> 00:31:35,000
That is the third parameter

345
00:31:35,000 --> 00:31:37,000
for the purpose of perfecting their virtue.

346
00:31:37,000 --> 00:31:39,000
They cleanse their understanding.

347
00:31:39,000 --> 00:31:40,000
That is a fold.

348
00:31:40,000 --> 00:31:41,000
Panya.

349
00:31:41,000 --> 00:31:43,000
For the purpose of non-confusion

350
00:31:43,000 --> 00:31:45,000
about what is good and bad for beings.

351
00:31:45,000 --> 00:31:47,000
They constantly arise energy.

352
00:31:47,000 --> 00:31:48,000
That is a fifth.

353
00:31:48,000 --> 00:31:49,000
Virya.

354
00:31:49,000 --> 00:31:52,000
Having beings welfare and happiness attack,

355
00:31:52,000 --> 00:31:54,000
when they have acquired heroic fortitude

356
00:31:54,000 --> 00:31:57,000
through supreme energy,

357
00:31:57,000 --> 00:31:59,000
they become patient,

358
00:31:59,000 --> 00:32:00,000
canti,

359
00:32:00,000 --> 00:32:02,000
seventh,

360
00:32:02,000 --> 00:32:04,000
fourth, fifth,

361
00:32:04,000 --> 00:32:06,000
sixth.

362
00:32:06,000 --> 00:32:08,000
They do not deceive.

363
00:32:08,000 --> 00:32:10,000
That means truthfulness.

364
00:32:10,000 --> 00:32:12,000
That is seventh.

365
00:32:12,000 --> 00:32:13,000
Yes.

366
00:32:13,000 --> 00:32:14,000
They do not deceive.

367
00:32:14,000 --> 00:32:15,000
When promising,

368
00:32:15,000 --> 00:32:16,000
we shall give you this.

369
00:32:16,000 --> 00:32:18,000
We shall do this for you.

370
00:32:18,000 --> 00:32:21,000
They are unshakeably resolute upon beings.

371
00:32:21,000 --> 00:32:23,000
So resoluteness,

372
00:32:23,000 --> 00:32:25,000
the eighth parameter

373
00:32:25,000 --> 00:32:27,000
in Bali it is called a deep tana,

374
00:32:27,000 --> 00:32:30,000
firmness of resolution.

375
00:32:30,000 --> 00:32:33,000
Through unshakeable loving kindness,

376
00:32:33,000 --> 00:32:35,000
this is the ninth.

377
00:32:35,000 --> 00:32:38,000
They place them first before themselves.

378
00:32:38,000 --> 00:32:40,000
Actually, here, what is mean is

379
00:32:40,000 --> 00:32:42,000
they do favor first.

380
00:32:42,000 --> 00:32:45,000
Now Bali were Pupa Kari.

381
00:32:45,000 --> 00:32:46,000
Pupa means first,

382
00:32:46,000 --> 00:32:48,000
and Kari means doing.

383
00:32:48,000 --> 00:32:50,000
So they do first means,

384
00:32:50,000 --> 00:32:53,000
they don't wait for you to do favor to them.

385
00:32:53,000 --> 00:32:56,000
So they do first.

386
00:32:56,000 --> 00:32:59,000
So, through unshakeable loving kindness,

387
00:32:59,000 --> 00:33:02,000
they do favor first.

388
00:33:02,000 --> 00:32:57,000
And then through

389
00:32:57,000 --> 00:33:05,000
equanimity, the tenth.

390
00:33:05,000 --> 00:33:08,000
They expect no reward.

391
00:33:08,000 --> 00:33:11,000
Having thus fulfilled to the tenth perfections,

392
00:33:11,000 --> 00:33:13,000
these divine abidings,

393
00:33:13,000 --> 00:33:16,000
then perfect all the good states

394
00:33:16,000 --> 00:33:18,000
classed as the tenth power,

395
00:33:18,000 --> 00:33:20,000
the four kinds of fearlessness,

396
00:33:20,000 --> 00:33:22,000
and the six kinds of knowledge

397
00:33:22,000 --> 00:33:24,000
not shared by disciples,

398
00:33:24,000 --> 00:33:27,000
and the 18 states of the enlightened one.

399
00:33:27,000 --> 00:33:29,000
This is how they bring to perfection all

400
00:33:29,000 --> 00:33:31,000
the good states beginning with giving.

401
00:33:31,000 --> 00:33:35,000
So these 10 parameters can be fulfilled

402
00:33:35,000 --> 00:33:39,000
by the practice of full divine abidings.

403
00:33:44,000 --> 00:33:46,000
So much to toggle off.

404
00:33:46,000 --> 00:33:49,000
I intend to be good

405
00:33:49,000 --> 00:33:51,000
than next,

406
00:33:51,000 --> 00:33:53,000
but I could not.

407
00:33:53,000 --> 00:33:55,000
I cannot.

408
00:33:55,000 --> 00:33:58,000
Good next week.

409
00:33:58,000 --> 00:34:02,000
So we should lead through,

410
00:34:02,000 --> 00:34:04,000
let me see.

411
00:34:04,000 --> 00:34:07,000
Oh, it does.

412
00:34:07,000 --> 00:34:10,000
Maybe more than five abidings.

413
00:34:10,000 --> 00:34:12,000
Full throttle.

414
00:34:12,000 --> 00:34:17,000
Three hundred, full check.

415
00:34:17,000 --> 00:34:20,000
Yes.

416
00:34:20,000 --> 00:34:25,000
T-M-H-7-T.

417
00:34:25,000 --> 00:34:30,000
That is true.

418
00:34:34,000 --> 00:34:35,000
18 pages.

419
00:34:35,000 --> 00:34:37,000
18 pages?

420
00:34:37,000 --> 00:34:42,000
18 pages?

421
00:34:42,000 --> 00:34:47,000
Yeah.

422
00:34:47,000 --> 00:34:52,000
Okay.

423
00:34:52,000 --> 00:34:54,000
18 pages?

424
00:34:54,000 --> 00:34:55,000
Yes, it is.

425
00:34:55,000 --> 00:34:56,000
It is a sharpener time.

426
00:34:56,000 --> 00:34:57,000
The next chapter.

427
00:34:57,000 --> 00:35:14,000
18 pages?

428
00:35:14,000 --> 00:35:37,000
18 pages?

429
00:35:37,000 --> 00:36:00,000
18 pages?

430
00:36:00,000 --> 00:36:23,000
18 pages?

