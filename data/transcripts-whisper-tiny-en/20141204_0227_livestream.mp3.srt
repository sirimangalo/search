1
00:00:00,000 --> 00:00:13,520
Okay, good morning, broadcasting today from Chiang Mai City, so the sounds here are a little

2
00:00:13,520 --> 00:00:16,200
bit different.

3
00:00:16,200 --> 00:00:25,280
We still have the birds, and we still have the trees and the leaves, but competing with

4
00:00:25,280 --> 00:00:35,560
the sound of the motorcycles and the cars and the people, which is fine, it's all just

5
00:00:35,560 --> 00:00:46,280
sound, this sound that sound.

6
00:00:46,280 --> 00:00:54,280
This morning we were talking, I was talking with the monks here and we were talking about

7
00:00:54,280 --> 00:01:14,880
some of the morality problems that go on in monasteries, which is interesting because

8
00:01:14,880 --> 00:01:20,160
it was my intention to talk this morning about, on the radio, about here on the internet

9
00:01:20,160 --> 00:01:26,240
about morality.

10
00:01:26,240 --> 00:01:29,960
I believe that I'm where the Buddha said, see, long, low, key, and utarangu.

11
00:01:29,960 --> 00:01:32,920
I think I'm pretty sure it was the Buddha.

12
00:01:32,920 --> 00:01:41,680
We have in the Pali anyway, see, long, yeah, maybe it's in the jata car or something, but

13
00:01:41,680 --> 00:01:45,440
see, long, low, key, and utarangu.

14
00:01:45,440 --> 00:01:51,880
It is a utarangu, that means has something more greater than it, a utarangu has nothing

15
00:01:51,880 --> 00:01:59,720
greater than it, low, key, in the world, see, long, morality has nothing greater than

16
00:01:59,720 --> 00:02:10,400
it in itself in the world, there's nothing greater than morality in the world.

17
00:02:10,400 --> 00:02:15,400
Because in the world we're talking about, there's different, low, can refer to different

18
00:02:15,400 --> 00:02:22,160
things, but in this case it most likely refers to sataloka, sataloka means the world

19
00:02:22,160 --> 00:02:39,000
of beings, world in terms of realms, realms that beings exist in the realm of beings

20
00:02:39,000 --> 00:02:44,680
you're dealing with relationships and interactions between beings.

21
00:02:44,680 --> 00:02:55,000
The morality refers to actions and speech, which are the most important in our relationships.

22
00:02:55,000 --> 00:03:06,640
One thing, all governments and all structures, organizations have to consider, is morality.

23
00:03:06,640 --> 00:03:16,920
For really the first thing, the most important thing that any organization should consider,

24
00:03:16,920 --> 00:03:23,160
is ethics, morality, how to be good to each other, how to not harm one another, how to

25
00:03:23,160 --> 00:03:35,600
not create chaos and people who have morality are special because of their moral and ethical

26
00:03:35,600 --> 00:03:42,480
event, leads them to be surrounded by good people and leads them to cultivate a sort of

27
00:03:42,480 --> 00:03:56,160
a protection because people will protect them and support them and defend them from physical

28
00:03:56,160 --> 00:04:06,000
and verbal harm.

29
00:04:06,000 --> 00:04:11,720
Morality is really, it's all you need to live in peace in this world, it's all we need

30
00:04:11,720 --> 00:04:17,280
in the world to live in peace if we have morality, everything else should fall into

31
00:04:17,280 --> 00:04:34,800
the place because we wouldn't have people stealing or hoarding, it's a lead to absence

32
00:04:34,800 --> 00:04:40,120
of corruption, people wouldn't take more than their share or take that much.

33
00:04:40,120 --> 00:04:52,920
There's desire, wouldn't hurt or put people into each other in danger.

34
00:04:52,920 --> 00:04:59,800
The catch here, of course, is that morality relies upon something, morality isn't something

35
00:04:59,800 --> 00:05:06,040
that you can just create in the world, in fact the interesting thing is morality actually

36
00:05:06,040 --> 00:05:15,480
relies upon wisdom, you need wisdom to have morality, right, person has to understand

37
00:05:15,480 --> 00:05:20,840
what is right and what is wrong and why it's right and why it's wrong, the problems

38
00:05:20,840 --> 00:05:27,360
with acting in moral, problems with acting in moral, this is a big part of what we mean

39
00:05:27,360 --> 00:05:33,480
by wisdom and why we say wisdom instead of intelligence because morality doesn't rely

40
00:05:33,480 --> 00:05:43,120
upon book knowledge, it relies on something, most of us vaguely understand or imprecise

41
00:05:43,120 --> 00:05:50,280
the understand as a Muslim, because someone wise when they know what's right and not

42
00:05:50,280 --> 00:05:56,760
just from book knowledge but they really have wisdom, they really know when they can explain

43
00:05:56,760 --> 00:06:01,200
and when it's from the heart.

44
00:06:01,200 --> 00:06:08,240
So it's a tough thing actually to be truly moral, you need to be a developed person, you

45
00:06:08,240 --> 00:06:14,400
need to have cultivated wisdom and then the problem is that wisdom itself depends on

46
00:06:14,400 --> 00:06:24,440
something, you can't just cultivate wisdom, wisdom depends on concentration, focus, maybe

47
00:06:24,440 --> 00:06:30,920
not concentration so much but focus and you have to understand deeply about the word

48
00:06:30,920 --> 00:06:39,520
focus, it doesn't just mean to grasp something and not let go, focus means to have

49
00:06:39,520 --> 00:06:52,360
a clear picture, to not have a blurry mind, a fuzzy picture, a fuzzy mind, to sharpen

50
00:06:52,360 --> 00:07:01,880
the mind, to straighten the mind, to clear the land needs of the mind, so we're not

51
00:07:01,880 --> 00:07:12,360
partial or ignorant or arrogant or any of the many things.

52
00:07:12,360 --> 00:07:16,840
This is required to bring wisdom because once you're only when your mind is clear, can

53
00:07:16,840 --> 00:07:21,640
you understand these things, what is right and what is wrong, what is good, what is

54
00:07:21,640 --> 00:07:33,080
bad, what is a benefit, what is not a benefit, what is a detriment, but then the problem

55
00:07:33,080 --> 00:07:40,840
comes full circle because focus depends upon something and focus doesn't come into being

56
00:07:40,840 --> 00:07:49,240
by itself, focus depends on what depends on morality and so you have this problem and you

57
00:07:49,240 --> 00:07:53,880
find that these three things actually depend on each other, they arise based on each

58
00:07:53,880 --> 00:08:03,280
other, so what do you do, how do you bring about one or the other, it's actually not

59
00:08:03,280 --> 00:08:08,640
a problem in the way I stated it, it just seems like a problem so this is a question

60
00:08:08,640 --> 00:08:15,280
that comes up, but the truth is the eightfold number path, these three things are the three

61
00:08:15,280 --> 00:08:21,200
trainings they compromise, they make up the eightfold number path, if you know the eightfold

62
00:08:21,200 --> 00:08:28,440
number path, the first two are morality, the first two are wisdom, the right view and

63
00:08:28,440 --> 00:08:40,680
right thought, the next three are morality, right speech, right action, right livelihood

64
00:08:40,680 --> 00:08:53,640
and the last three are wisdom, are concentration, right effort, right mindfulness and

65
00:08:53,640 --> 00:09:01,800
right concentration and they go in a circle, these eight things can be made in a circle

66
00:09:01,800 --> 00:09:07,200
because once you have right concentration it improves your view, right focus, it improves

67
00:09:07,200 --> 00:09:14,840
your view which improves your thoughts and so on, so the question is where do you start,

68
00:09:14,840 --> 00:09:20,800
the answer is actually you can start anywhere, in truth, if the eightfold number path is

69
00:09:20,800 --> 00:09:27,880
like a wheel then the practice is like one of those you know the in the olden days children

70
00:09:27,880 --> 00:09:37,480
would take a stick and they would hit the wheel, right hit the wheel and make it turn

71
00:09:37,480 --> 00:09:43,080
and this was a game they'd run along after the wheel, the eightfold number path is like

72
00:09:43,080 --> 00:09:49,120
that, it doesn't matter where you hit it, you start it turning, the important thing is

73
00:09:49,120 --> 00:09:54,720
to start it turning, to start the wheel turning otherwise it won't go anywhere, but once

74
00:09:54,720 --> 00:10:02,200
the eightfold number path starts turning, faster and faster and faster, this is called

75
00:10:02,200 --> 00:10:13,120
pubangamanga, pubangamanga means pubanganga means part and pubam means the prior or previous,

76
00:10:13,120 --> 00:10:22,080
so this is the precursor path, the part of the path that is comes before the noble path,

77
00:10:22,080 --> 00:10:27,560
so to cultivate the noble path you have to do the pubangamanga, pubangamanga just means speeding

78
00:10:27,560 --> 00:10:34,760
up the wheel until it gets going so, so perfectly that we call it the noble path and that

79
00:10:34,760 --> 00:10:48,560
moment when it's just, just right, just perfect then it's able to cut the defoundments,

80
00:10:48,560 --> 00:10:53,640
but this means that to start you start anywhere, it doesn't matter which place you start

81
00:10:53,640 --> 00:10:59,160
because the whole wheel turns, or another way of looking at it is you have to turn the

82
00:10:59,160 --> 00:11:03,120
whole wheel, you can't just pick one or the other, you can't say today I'm going to work

83
00:11:03,120 --> 00:11:07,320
on right speech and only right speech or today I'm going to work on right effort and

84
00:11:07,320 --> 00:11:12,240
only right effort, it's actually much more complicated than that, if you look at some

85
00:11:12,240 --> 00:11:17,640
of the Buddha's deep teachings on the full noble path and talk about different parts surrounding

86
00:11:17,640 --> 00:11:26,720
different parts and what is the basis, but the two, the two places where it seems that

87
00:11:26,720 --> 00:11:38,320
the Buddha placed the most emphasis on starting on our right view, right view and right

88
00:11:38,320 --> 00:11:50,440
mindfulness, right view you need because that gives you an idea of morality, so you have

89
00:11:50,440 --> 00:11:56,440
right view and then morality, so right view means you have to learn something first, you

90
00:11:56,440 --> 00:12:05,600
have to get a right idea whether it's in the beginning just from listening, usually

91
00:12:05,600 --> 00:12:13,000
when a person comes to meditate for the first time you have to explain and sometimes

92
00:12:13,000 --> 00:12:19,920
answer their questions even once in a while you have to debate or argue with them until

93
00:12:19,920 --> 00:12:25,640
they can straighten out their view because otherwise when they practice they'll practice

94
00:12:25,640 --> 00:12:33,760
wrong, their practice will be fruitless, maybe even harmful because without right view

95
00:12:33,760 --> 00:12:41,240
it's not right practice, do things that they shouldn't do, thinking it's what they

96
00:12:41,240 --> 00:12:44,520
should do, they won't do things that they shouldn't do, thinking that it's something

97
00:12:44,520 --> 00:12:52,840
they shouldn't do, that kind of thing and right mindfulness because mindfulness really

98
00:12:52,840 --> 00:13:00,440
cuts through the whole painful noble path, if you have mindfulness then it creates a different

99
00:13:00,440 --> 00:13:07,440
type of right view, it creates a deeper right view in the sense of really understanding

100
00:13:07,440 --> 00:13:15,280
what is right and what is wrong, really understanding, seeing the formable truth for yourself,

101
00:13:15,280 --> 00:13:20,520
it creates morality because you're aware of what you're doing, what you're saying, it creates

102
00:13:20,520 --> 00:13:28,680
concentration, so often the Buddha would focus on mindfulness, so you could practice mindfulness

103
00:13:28,680 --> 00:13:33,720
first, I guess the point is when you're really mindful you're not, there's not an opportunity

104
00:13:33,720 --> 00:13:40,000
to be immoral and there's no chance for the arising of wrong view, if you really are mindful

105
00:13:40,000 --> 00:13:49,600
but other times it's even not even so clear because many times the Buddha did just say

106
00:13:49,600 --> 00:13:56,880
to start with morality, you know, I'm quite often we talk about starting with morality

107
00:13:56,880 --> 00:14:05,440
before you practice meditation, practice morality, so in his teachings he emphasized

108
00:14:05,440 --> 00:14:12,080
seems he emphasized right view and right mindfulness most often but at the same time you

109
00:14:12,080 --> 00:14:17,360
could start with morality, the point is you have to cover them all, you have to cover the

110
00:14:17,360 --> 00:14:23,000
whole hateful noble path and the best way to do this is to understand it as morality,

111
00:14:23,000 --> 00:14:33,920
concentration and wisdom, wherever you start, you can start anywhere because all eight, all

112
00:14:33,920 --> 00:14:39,000
eight come together, on the other hand if you're missing one of them, missing one part of

113
00:14:39,000 --> 00:14:47,080
it you can't roll the wheel, the wheel won't turn unless the wheel is complete, so there

114
00:14:47,080 --> 00:14:53,040
you go and just some thoughts about, well thoughts about the eight full noble path, that's

115
00:14:53,040 --> 00:15:00,760
about morality but more deeply about the eight full noble path, it's like a wheel, some

116
00:15:00,760 --> 00:15:06,520
more food for thought, it's the dhamma for today, thank you all for tuning in and for

117
00:15:06,520 --> 00:15:18,240
practicing together, may you all be well, and to our practice may we purify our minds

118
00:15:18,240 --> 00:15:28,760
and attain enlightenment, in a way conducive for the happiness and well-being of all beings,

119
00:15:28,760 --> 00:15:43,760
may all beings be happy and well, it's our tool.

