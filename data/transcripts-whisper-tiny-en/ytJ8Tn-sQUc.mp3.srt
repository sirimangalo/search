1
00:00:00,000 --> 00:00:20,460
Good evening broadcasting live, August 6th from Stony Creek, Ontario.

2
00:00:20,460 --> 00:00:33,200
We're live both on YouTube and on Meditation Plus.

3
00:00:33,200 --> 00:00:42,620
I'm not taking questions on YouTube anymore, it's too, a little bit too chaotic, too open.

4
00:00:42,620 --> 00:00:50,400
So if you want to ask questions, you have to go to meditation.serimungalow.org.

5
00:00:50,400 --> 00:01:02,940
And you have to ask questions live, unfortunately, for now, let's see on the way to ask questions here.

6
00:01:02,940 --> 00:01:12,580
So here we've been doing our meditation, and now we come together to look at this quote.

7
00:01:12,580 --> 00:01:29,300
And we have a quote on the carpenter.

8
00:01:29,300 --> 00:01:54,620
So it starts off not just talking about the carpenter, but he says, first of all, suppose there

9
00:01:54,620 --> 00:02:07,220
were a hand with eight, ten, twelve eggs, especially had these eggs, and she was looking

10
00:02:07,220 --> 00:02:17,300
to hatch, but she hadn't nurtured them, she hadn't sat on them, she had abandoned them, basically.

11
00:02:17,300 --> 00:02:27,260
And then she might later on, look, see them and wonder to herself when they're going to hatch.

12
00:02:27,260 --> 00:02:33,380
And she might wish, only they may, may my chicks be able to hatch and come out of their

13
00:02:33,380 --> 00:02:41,740
eggs, come out of their shells, but no matter how much she wished it wouldn't happen.

14
00:02:41,740 --> 00:02:43,740
They would have already been dead.

15
00:02:43,740 --> 00:02:49,220
That's not what the zoo just says, the zoo just says, you can't expect them to come

16
00:02:49,220 --> 00:02:55,700
out of their shells because they haven't been converted, invaded, or nurtured.

17
00:02:55,700 --> 00:03:01,580
And likewise, when a bikhu does not dwell, devoted to development, even though he might

18
00:03:01,580 --> 00:03:08,500
wish that he should become liberated, if you don't actually practice wishing he isn't

19
00:03:08,500 --> 00:03:15,660
of any use of any value.

20
00:03:15,660 --> 00:03:25,220
On the other hand, when you're dwelling devoted in development, when you devote yourself

21
00:03:25,220 --> 00:03:32,380
to the development of the mind, what it says, even though even though no such wish, even

22
00:03:32,380 --> 00:03:39,340
though he might not wish, you're she, might not wish, oh, may I be free from suffering,

23
00:03:39,340 --> 00:03:50,620
or may I be freed from the defilement, but still, because they're devoted to development,

24
00:03:50,620 --> 00:03:58,300
they become free, may it's not a difficult teaching to understand, it's quite simple,

25
00:03:58,300 --> 00:04:04,700
but it's important that something's a reminder to us, that no, it's not good enough to wish,

26
00:04:04,700 --> 00:04:10,580
because we find ourselves wishing, I wish I could meditate more, I wish I were better at

27
00:04:10,580 --> 00:04:16,860
meditation, I wish I could be freed from suffering, oh, I wish I could be such a, I wish

28
00:04:16,860 --> 00:04:22,620
it could be a better person, alcoholics, drug addicts, wish these things, but wishing

29
00:04:22,620 --> 00:04:31,380
has no benefit, no value, clearly, regardless of whether one, regardless of whether you wish

30
00:04:31,380 --> 00:04:39,180
or not, makes no difference, which is interesting, there's this question that goes around

31
00:04:39,180 --> 00:04:45,700
and people ask, ask again and again, if Buddhism is all about giving up wanting, what's

32
00:04:45,700 --> 00:04:52,180
this about wanting, what about wanting to meditate, how can you possibly progress without

33
00:04:52,180 --> 00:05:00,060
wanting, this is the key, you see, there's no, meditation isn't based on desire, in fact,

34
00:05:00,060 --> 00:05:07,300
the more you meditate, the less you have any desire, a goal or intention to become anything,

35
00:05:07,300 --> 00:05:14,260
you can do it more as a matter of course, and out of wisdom, out of an understanding that

36
00:05:14,260 --> 00:05:28,100
it's the right thing to do, just like the hand, the hand sits on the eggs without any

37
00:05:28,100 --> 00:05:34,940
wish that they might hatch, just doing it because that's the duty of the hand, and regardless

38
00:05:34,940 --> 00:05:45,220
of the fact that she has made no wish in due time, in due time, the eggs hatch, and

39
00:05:45,220 --> 00:05:51,500
then he says, when a carpenter or a carpenter's apprentice, looks at the handle of his

40
00:05:51,500 --> 00:05:59,420
ads, he sees the impressions of his fingers in his thumb, but he does not know so much

41
00:05:59,420 --> 00:06:03,900
of the ads handle has been worn away today, so much yesterday, so much earlier.

42
00:06:03,900 --> 00:06:13,180
He wears away whatever this ad does, it's one of these carpenters tools, it's worn away

43
00:06:13,180 --> 00:06:19,380
and so he looks at it one day and he sees that it's worn away, but he can't use it for

44
00:06:19,380 --> 00:06:35,180
a day and say, wow, look today it was worn away, so the point being made here is how minute

45
00:06:35,180 --> 00:06:42,860
are the changes, how fine are the changes that come about through meditation, that from

46
00:06:42,860 --> 00:06:49,860
one day to the next you might not see the difference, but you'll wake up one day and

47
00:06:49,860 --> 00:06:57,820
or you'll be one day in a conflict situation and you'll realize you're handling yourself

48
00:06:57,820 --> 00:07:02,500
better than you were before, you'll realize that as a result of the benefits of the

49
00:07:02,500 --> 00:07:07,380
meditation, you've changed, but you couldn't say when the change, you know, it was so

50
00:07:07,380 --> 00:07:15,740
gradual, that's the idea being passed on here, but when it has worn away, the knowledge

51
00:07:15,740 --> 00:07:22,380
occurs to him and it has worn away, so too because when a beaker dwells devoted to development

52
00:07:22,380 --> 00:07:26,780
even though no such knowledge occurs to him, so much of my teens have been worn away

53
00:07:26,780 --> 00:07:31,980
today, so much yesterday, so much earlier, when they are worn away, the knowledge occurs

54
00:07:31,980 --> 00:07:38,880
to him that they've been worn away, and more, there's more to this it actually, this

55
00:07:38,880 --> 00:07:46,540
is from the Senyutani Kaya, he says, I'm reading beaker bodies translations, I've got

56
00:07:46,540 --> 00:07:52,580
them on this, I'm on the computer, suppose because there were a seafaring ship down with

57
00:07:52,580 --> 00:07:59,020
rigging that had been worn away in the water for six months, it would be hauled up on dry

58
00:07:59,020 --> 00:08:04,980
land during the cold season, and its rigging would be further attacked by wind and sun, inundated

59
00:08:04,980 --> 00:08:17,380
by rain, from a rain cloud, the rigging would eat rigging would easily collapse and rot away,

60
00:08:17,380 --> 00:08:21,580
so too because when a beaker dwells devoted to development, his feathers easily collapse

61
00:08:21,580 --> 00:08:27,100
and rot away, so there's another part to the process, it's not just of wearing away like

62
00:08:27,100 --> 00:08:35,220
the ads handle, it's a eroding, an eroding until there's a collapse, because there is actually

63
00:08:35,220 --> 00:08:42,500
a moment of collapse in the practice, you get to a point where you're in defilements

64
00:08:42,500 --> 00:08:52,620
by you enter into Nibana, it's such a weakness that suddenly is a collapse, so just like

65
00:08:52,620 --> 00:09:03,220
a ship that rots when it's hauled up onto dry land, eventually rots and collapses, so

66
00:09:03,220 --> 00:09:10,180
to this frame, there's some sara that we find ourselves in, if we stop, if we take it

67
00:09:10,180 --> 00:09:16,780
out of the of its element, then we take away the fuel that keeps it going, eventually

68
00:09:16,780 --> 00:09:35,820
it will collapse and we'll be free from it, so it's a teaching to perhaps not be so impatient

69
00:09:35,820 --> 00:09:44,980
or not be so goal-oriented as we might otherwise, to remind us not to be focused on results

70
00:09:44,980 --> 00:09:55,100
or demanding to have some change like a magical pill, this isn't a game or it isn't

71
00:09:55,100 --> 00:10:06,540
a machine, this is reality, the reality here where you're sitting, your experience

72
00:10:06,540 --> 00:10:14,580
is real, it's not just going to change because you want it to, and it's not just going

73
00:10:14,580 --> 00:10:22,060
to magically go, go at the moment when you first start to meditate, it's not like unlocking

74
00:10:22,060 --> 00:10:30,100
a door and then it's open, it's about reality, and reality is, you know, there's so many

75
00:10:30,100 --> 00:10:37,900
conglomerate, like aggregates of habits, we have to tear them down and change them and

76
00:10:37,900 --> 00:10:43,900
it's natural, it's real, it's happening right here now, we're developing habits, we're

77
00:10:43,900 --> 00:10:53,740
changing our habits, some meditation is about a specific type of habit, of habit cultivation,

78
00:10:53,740 --> 00:10:58,780
we're cultivating a habit of clarity, without habit is going to conflict with a habit

79
00:10:58,780 --> 00:11:11,700
of delusion, of ignorance, to found it, so it also I think it can be a little bit discouraging

80
00:11:11,700 --> 00:11:17,540
in this case because then you ask, well, how do I know that I'm progressing on the path,

81
00:11:17,540 --> 00:11:24,780
this is another common question, and well, you may not know moment to moment that you're

82
00:11:24,780 --> 00:11:33,740
progressing, you may not be able to see the repercussions of the practice, don't let that

83
00:11:33,740 --> 00:11:42,140
blind you to the fact that you are, of the greatness of the practice in the moment, because

84
00:11:42,140 --> 00:11:49,380
what you can see, moment to moment, is that in the moment when you are truly mindful, your

85
00:11:49,380 --> 00:11:56,900
mind becomes clear, you change, you feel that that's where you feel like a lock, like

86
00:11:56,900 --> 00:12:04,980
a key in the lock, you catch it and you've broken a chain, you've released the chain by

87
00:12:04,980 --> 00:12:11,460
freeing yourself from the lock, it's like untying a knot, it's figuring out a puzzle,

88
00:12:11,460 --> 00:12:18,780
like one of those puzzles where you turn things just right and then it opens, and that

89
00:12:18,780 --> 00:12:24,900
you can see moment, moment, you get something, every time you meditate, if you don't learn

90
00:12:24,900 --> 00:12:30,140
something or gain something, change something about yourself, if you don't unlock something

91
00:12:30,140 --> 00:12:35,940
there, you weren't really meditating, I think I can safely say that, so it's not to say

92
00:12:35,940 --> 00:12:41,380
this is going to happen every time you sit, but because it does, the reason it doesn't,

93
00:12:41,380 --> 00:12:47,140
for this, the mind is, you haven't really meditating, when you really get it, when you

94
00:12:47,140 --> 00:12:56,820
really mindful, then clarity come, I can become, I tell this story often, I was watching

95
00:12:56,820 --> 00:13:05,460
a monk, teach once, very, very venerable monk, and he was explaining, when you, when

96
00:13:05,460 --> 00:13:11,500
you see, you see, he's seeing, he doesn't, this is just giving the same teaching, normally

97
00:13:11,500 --> 00:13:24,780
it, when you see, when you hear, say hearing, and then suddenly he stopped, and it was

98
00:13:24,780 --> 00:13:34,220
like, it was, it looked like he had fallen asleep, but he hadn't fallen asleep, so it can

99
00:13:34,220 --> 00:13:42,060
happen any time, enlightenment can come, if you really, mind is really that clear, one moment

100
00:13:42,060 --> 00:13:49,220
is all it takes, anyway, so that's the time of our today, thank you all for tuning in,

101
00:13:49,220 --> 00:13:57,060
now I suppose we have some questions, if you're on YouTube, you shouldn't be on YouTube

102
00:13:57,060 --> 00:14:04,020
to ask questions, go over to meditation.serimongolow.org, the idea is that you're

103
00:14:04,020 --> 00:14:13,780
meditating with us, now suspicious, you see, we've got this neat feature that, if your,

104
00:14:13,780 --> 00:14:19,820
if your username is in yellow, it means you didn't actually meditate with us, that's

105
00:14:19,820 --> 00:14:24,980
it, or an orange, it says logged in users, there's a list of us, and only those in green

106
00:14:24,980 --> 00:14:30,220
have actually meditated, so we can tell who hasn't meditated, I think there's like, if you

107
00:14:30,220 --> 00:14:35,100
haven't meditated in the past three hours or something, you're not, you're in, or you

108
00:14:35,100 --> 00:14:49,220
get to be an orange, so we have questions, after practicing for some months, the meditation

109
00:14:49,220 --> 00:14:54,940
is described in the booklet, awesome, now I keep asking myself, is this it, I can see

110
00:14:54,940 --> 00:15:01,020
mental and physical phenomena come and go, and then what I ask myself, should I just keep

111
00:15:01,020 --> 00:15:05,780
recognising these questions as thinking, you should look at them as, as probably based

112
00:15:05,780 --> 00:15:12,500
on some sort of development that you are, have a doubt or desire for something more wanting

113
00:15:12,500 --> 00:15:27,500
something special to happen, and it's based on your, your ordinal, your original state

114
00:15:27,500 --> 00:15:32,340
or original inclination, the meditation isn't going to do anything for you, it's going

115
00:15:32,340 --> 00:15:37,580
to untie the knots, so this is kind of a knot really when you have doubt or when you have

116
00:15:37,580 --> 00:15:49,100
a desire or whatever, whatever it stems from, desire for something more than you have

117
00:15:49,100 --> 00:15:57,580
to, that's a knot that you have to untie, so that's what I would recommend at that point,

118
00:15:57,580 --> 00:16:02,420
because yeah, this is it, it's not going to take you anywhere, it's not going to help

119
00:16:02,420 --> 00:16:06,980
you realise your desires or bring you some wonderful thing, it's just going to untie

120
00:16:06,980 --> 00:16:18,780
you're not, yeah, there's no shame in being orange, that's okay, well there's a little

121
00:16:18,780 --> 00:16:27,060
shame, but it's okay, we'll allow, we'll allow, we don't discriminate, but now you know,

122
00:16:27,060 --> 00:16:39,700
you can't pretend, you can't come here and pretend that you're a meditator, how do

123
00:16:39,700 --> 00:16:45,060
you view people with more defilements than you also is the use of incense necessary

124
00:16:45,060 --> 00:16:50,620
for meditation, those are two very different questions, how do you view people with more

125
00:16:50,620 --> 00:16:57,980
defilements than you, I guess, as unfortunate, but you don't really, I mean, I don't really

126
00:16:57,980 --> 00:17:04,780
view people, you know, I mean, I think as a meditator you start to give up your desire

127
00:17:04,780 --> 00:17:12,020
to judge others, it's one thing I learned so much from my teacher, because even as even

128
00:17:12,020 --> 00:17:20,020
as, beginning the meditator, it's still always, I think of ourselves as superior to others

129
00:17:20,020 --> 00:17:27,380
or want to judge each other and compare with each other and want to stay away from people

130
00:17:27,380 --> 00:17:39,060
who are problematic or so on, but he showed me that you correct ways to never judge anyone,

131
00:17:39,060 --> 00:17:45,340
you don't have to be people's judge, you don't have to fix them, it's not your job

132
00:17:45,340 --> 00:17:53,900
to change other people, so you don't really judge people and the most interesting thing

133
00:17:53,900 --> 00:18:00,420
I found from him is that he wasn't really all that interested in teaching people, like

134
00:18:00,420 --> 00:18:03,900
I thought, hey, why aren't we out there helping the world, right?

135
00:18:03,900 --> 00:18:08,020
When people came to him and they still come to him and drove, he's in over 90 years,

136
00:18:08,020 --> 00:18:14,020
so he doesn't teach all that much anymore because he's very tired and old, but people

137
00:18:14,020 --> 00:18:20,620
still come, he's, you know, famous, famous for what, not for giving, not for magic or

138
00:18:20,620 --> 00:18:29,500
giving cures, but for telling people, they're seeing, you know, very simple practice,

139
00:18:29,500 --> 00:18:35,100
but he doesn't go looking for students, he doesn't, he's not really all that interested

140
00:18:35,100 --> 00:18:44,380
in, you know, people come to him and it's almost like you have to make the initiative.

141
00:18:44,380 --> 00:18:49,900
Almost, it's not quite, I mean, he's very open and very kind and very willing to help,

142
00:18:49,900 --> 00:18:56,260
but not desiring to change people and not looking at you.

143
00:18:56,260 --> 00:19:00,660
The other thing I noticed because I had had experience with teachers who were delved and

144
00:19:00,660 --> 00:19:07,220
would poke and prod and look for your problems and say, when I first started, I keep

145
00:19:07,220 --> 00:19:15,660
you not, my teacher said the teacher who I was with, not my, my first teacher said, because

146
00:19:15,660 --> 00:19:20,300
I was having trouble, I was having real trouble in my, my foundation course.

147
00:19:20,300 --> 00:19:25,980
And he said, I don't know what's wrong, I think you must, and another thing, but he was

148
00:19:25,980 --> 00:19:31,020
sure he said, there must be something, he must be repressing, it was like, there must

149
00:19:31,020 --> 00:19:36,740
have something that happened when you were young and, and he was trying to get to figure

150
00:19:36,740 --> 00:19:45,380
out why I was so, I don't know, I don't, I wasn't that bad, in fact, I, I was okay,

151
00:19:45,380 --> 00:19:50,420
but I was, I was in great trouble because I was realizing things about myself, but I was

152
00:19:50,420 --> 00:20:01,180
on a really wrong path, and, but he tried to, so I, I spent all this time trying to figure

153
00:20:01,180 --> 00:20:06,940
out what it was that I was repressing, what was it inside that was, what was this block?

154
00:20:06,940 --> 00:20:10,980
And I suppose a lot of it was my own interpretation because I was a little bit, I was

155
00:20:10,980 --> 00:20:19,140
again, kid who didn't know anything, and so, but I, it really, it made things worse trying

156
00:20:19,140 --> 00:20:26,380
to find the root cause, the problem, the cause of all my problems.

157
00:20:26,380 --> 00:20:31,660
And so, but, but I developed that, and so for a couple of years, I was, you know, working

158
00:20:31,660 --> 00:20:36,580
in this way on this assumption that you have to delve and you have to figure out people's

159
00:20:36,580 --> 00:20:37,980
problems.

160
00:20:37,980 --> 00:20:44,260
And then I came to practice with Ajahn Tang, and she wasn't doing that, they didn't

161
00:20:44,260 --> 00:20:45,260
care.

162
00:20:45,260 --> 00:20:50,220
They come to him and, and it'd be all angry, and it would be, it would be like, I mean,

163
00:20:50,220 --> 00:20:55,740
it was pretty clear that he was just basically pretending not to notice, and not exactly

164
00:20:55,740 --> 00:20:58,500
pretending, but not at all interested.

165
00:20:58,500 --> 00:21:02,700
So you'd come angry and totally, and you wouldn't even talk about your anger, okay?

166
00:21:02,700 --> 00:21:09,140
And it, because it works, you know, the, the person comes to you with anger, and they expect

167
00:21:09,140 --> 00:21:15,700
you to respond to the anger, they expect you to, to, to react to it.

168
00:21:15,700 --> 00:21:18,620
And, and so the person might be really, really angry, and it would only be once they

169
00:21:18,620 --> 00:21:22,380
would, at least, and then they would say to him, you know, I feel really angry.

170
00:21:22,380 --> 00:21:27,020
And they would, he would say almost as though it was, it was a, of little consequence,

171
00:21:27,020 --> 00:21:32,380
he would like, like he hadn't thought of me like, oh, well, we'll say angry, angry.

172
00:21:32,380 --> 00:21:36,900
And then he'd continue on with something else, not even, see, because the person is making

173
00:21:36,900 --> 00:21:41,300
a big deal out of it, then you want them to let, they're holding on to it, and you want

174
00:21:41,300 --> 00:21:43,180
them to let go of it.

175
00:21:43,180 --> 00:21:49,860
But then my point here, and how it relates to your question is, you don't get anywhere

176
00:21:49,860 --> 00:21:58,860
by, by relating to people, by getting involved with the, getting caught up in others.

177
00:21:58,860 --> 00:22:09,860
I think in a sense, Buddhism is self-centered, I think you, you become less concerned about

178
00:22:09,860 --> 00:22:15,980
really other people's state.

179
00:22:15,980 --> 00:22:23,140
And it works, because it's like a tree, a, a junky, I think, said he thought of himself,

180
00:22:23,140 --> 00:22:29,620
I thought of the Arahand as a tree, the tree doesn't want people to sit under it.

181
00:22:29,620 --> 00:22:35,180
But the tree is so perfect with its branches and with its leaves, that people look at it

182
00:22:35,180 --> 00:22:42,580
and they say, I want to sit under that tree, because if it's very nature, this is where

183
00:22:42,580 --> 00:22:46,860
true benefit comes from, it doesn't come from wanting to help others, it doesn't come

184
00:22:46,860 --> 00:22:54,620
from seeking out students, seeking out beneficiaries.

185
00:22:54,620 --> 00:22:58,340
It comes from being beneficial, from having a beneficial nature.

186
00:22:58,340 --> 00:23:02,060
And you'll find that there are more people than you can help.

187
00:23:02,060 --> 00:23:07,580
When you really have something that you can give, that's, that's much more important.

188
00:23:07,580 --> 00:23:12,940
That's what's most important, because in the end my teacher said many times, there's

189
00:23:12,940 --> 00:23:21,220
no end, you know, he said, yeah, there's no end, you know, it never, the number of

190
00:23:21,220 --> 00:23:26,140
people, meditators, is never going to end, so you just help those you can help.

191
00:23:26,140 --> 00:23:30,100
What does it matter if you help 10 people or 100 people?

192
00:23:30,100 --> 00:23:36,420
What matters most is that you're in the right path, and you know, a little bit beyond

193
00:23:36,420 --> 00:23:41,900
your question, but basically, wouldn't, wouldn't think too much of other meditators,

194
00:23:41,900 --> 00:23:46,140
if they want your help, then they'll ask for it, and then you can give them basic information,

195
00:23:46,140 --> 00:23:50,820
but you try to be as objective and uninvolved as possible.

196
00:23:50,820 --> 00:23:57,260
You don't try to dissociate yourself, but you don't delve either too often, new meditators

197
00:23:57,260 --> 00:24:02,420
go back and try to fix everyone else, it's not how it works, it'll be so nice if everyone

198
00:24:02,420 --> 00:24:06,820
else got it the way you've gotten it, what you've realized wouldn't it be great if everyone

199
00:24:06,820 --> 00:24:13,820
else got it, but it's like hitting your head against a brick wall, eventually you realize

200
00:24:13,820 --> 00:24:18,620
that's not the path, the path is for me to go.

201
00:24:18,620 --> 00:24:21,540
If they want to follow the path, they're going to have to follow their path because you

202
00:24:21,540 --> 00:24:27,660
see it's not working, I think that's to do with your first question.

203
00:24:27,660 --> 00:24:34,260
The second question is, I don't use incense, I haven't let incense in quite a long time,

204
00:24:34,260 --> 00:24:37,220
so no, I don't think it's necessary.

205
00:24:37,220 --> 00:24:44,980
What expectations should a mentally ill person have about meditation?

206
00:24:44,980 --> 00:24:48,100
Do you expect it to be difficult?

207
00:24:48,100 --> 00:24:53,300
I mean we're all mentally ill to some extent, it's just a matter of degree, and it can

208
00:24:53,300 --> 00:24:57,820
be such a great degree that you've been born with mental illness, so it's people who

209
00:24:57,820 --> 00:25:03,820
have organic mental illness where the brain is damaged, there's chemical imbalance or whatever.

210
00:25:03,820 --> 00:25:10,540
So that might take more than one lifetime, but I think we should be clear that for most

211
00:25:10,540 --> 00:25:12,940
of us it's going to take more than one lifetime.

212
00:25:12,940 --> 00:25:17,900
Most people aren't going to become free from suffering in this life, so we do what we

213
00:25:17,900 --> 00:25:23,580
can, and if you find that you have a mental illness to the extent that you're not able

214
00:25:23,580 --> 00:25:30,140
to free yourself from it in this life, then don't think of that as a limit, death is not

215
00:25:30,140 --> 00:25:39,260
the end, work now, and better yourself, change your habits, it's a long path, work what

216
00:25:39,260 --> 00:25:40,260
you can.

217
00:25:40,260 --> 00:25:44,780
I mean even if you don't believe in the afterlife, do the best you can do is the best you

218
00:25:44,780 --> 00:25:49,780
can do, before you die, be ready for death.

219
00:25:49,780 --> 00:25:54,140
But there are things you can do, and meditation is one of them, it can't help.

220
00:25:54,140 --> 00:25:58,700
Don't expect too much, none of us should expect too much from meditation, but definitely

221
00:25:58,700 --> 00:26:03,700
someone who has mental illness should be have low expectations and not try to raise their

222
00:26:03,700 --> 00:26:11,580
expectations, but work step by step, think of it as the ads, wearing away a bit, that's

223
00:26:11,580 --> 00:26:37,820
how all of us are, bit by bit.

224
00:26:37,820 --> 00:26:42,740
So now you know, you'll have to go and do meditation, so you can get a green tag, the green

225
00:26:42,740 --> 00:26:52,940
tag doesn't last, and then you have to come back and meditate again.

226
00:26:52,940 --> 00:27:05,180
Anyway, you're concerned, I'm concerned I'm not building the right kind of attention.

227
00:27:05,180 --> 00:27:09,420
Don't feel anything during the sitting, well you don't, the idea isn't to feel anything,

228
00:27:09,420 --> 00:27:15,020
it's not the point, if you feel something that's a feeling, you're not supposed to feel

229
00:27:15,020 --> 00:27:16,020
anything.

230
00:27:16,020 --> 00:27:21,860
You're supposed to have a clear mind, I guess what you would feel is lack or sort of this,

231
00:27:21,860 --> 00:27:28,020
like a fog lifting, and in a lift for just a moment, but it's so momentary that you really

232
00:27:28,020 --> 00:27:31,420
have to work with it.

233
00:27:31,420 --> 00:27:36,180
Nothing is more difficult, because it's more intense, there's no distractions, but it's

234
00:27:36,180 --> 00:27:42,060
going to be more rewarding, and you're learning something new, you're sitting is probably

235
00:27:42,060 --> 00:27:46,860
done completely wrong, so just get moments where you're doing it right, try and find the

236
00:27:46,860 --> 00:27:52,900
moments out of the half hour where you can do it right, and those little moments are what

237
00:27:52,900 --> 00:27:57,580
is going to change your habit, it's going to build a new habit, both in this life and

238
00:27:57,580 --> 00:28:07,140
in the future, future life, it's going to build, it's these little slivers, slowly, slowly

239
00:28:07,140 --> 00:28:13,260
wear it away, but most of your meditation is going to be rubbish in the beginning, you

240
00:28:13,260 --> 00:28:16,820
don't worry about, how am I going to be mindful for a half an hour, how am I going to

241
00:28:16,820 --> 00:28:22,060
feel for a half an hour, get a moment where you can be mindful, start with one moment,

242
00:28:22,060 --> 00:28:28,260
and then another moment, in the more moments you can string together and gather it together,

243
00:28:28,260 --> 00:28:43,580
it's like raindrops, eventually it can flood a whole village, all right, I think that's

244
00:28:43,580 --> 00:28:49,900
enough for today, thank you all for tuning in, check out sungadotceremongalo.org, it's

245
00:28:49,900 --> 00:28:55,660
their new social network still under construction, and I got to tweak it a little bit,

246
00:28:55,660 --> 00:29:02,260
but soon it's going to show all sorts of stuff when everyone has things to comment and post

247
00:29:02,260 --> 00:29:06,860
and I don't know, I don't know what we'll use it for, but it's a social network, so it's

248
00:29:06,860 --> 00:29:16,620
place for us to gather and be not overly social, but at least to join together, anyway, see

249
00:29:16,620 --> 00:29:21,140
you tomorrow.

