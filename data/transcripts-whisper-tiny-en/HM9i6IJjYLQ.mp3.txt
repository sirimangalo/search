Hey, good evening.
Hi, Robin.
Can you hear me?
Don't hear me.
Okay, we're ready to start.
So good evening, everyone.
Broadcasting live from Stony Creek, Ontario, August 9th.
I heard you there.
I hear you.
You hear me?
I heard you.
I heard something.
We haven't joined by Robin tonight.
If you'd like to join the Hangout, well, you have to get special
permission.
But it's not hard to get just asking.
Well, actually, we probably wouldn't.
This isn't something I'm going to open up to everyone.
The idea here is to talk about this verse and answer people's questions.
But tonight, we have something special as well that I'd like to bring up.
And I was hoping Robin could maybe talk about it, but she's muted.
I don't hear her.
No.
I see your lips are moving.
No sound is coming up.
Was for a second. I heard something. I think it might be a cable or something.
How about that? Can you hear me now?
Now I can hear you, but this sounds pretty awful.
But that might be in my end.
That might be on my end, because that was happening before just a second.
That was when you were on Firefox.
Okay.
Go again.
How is it now?
Nope.
Yeah, it's at my end.
Are you able to go on Google at all and Google Chrome for this?
Because I think last time the problem was when you were on Firefox.
Now I've got a problem.
I'm uploading it.
I'm uploading it.
Okay, how about now?
Oh, how is that?
No, you're still there.
But that's just at my end. I don't think anyone else is bothered by it.
I just got a deal with it.
Anyway, enough of this. Good evening, as I said.
Welcome.
Today we're looking at a quote by Buddha.
Actually a conversation between him and a Brahman.
The Brahman says again with these views.
Brahman has his view.
So again we have this clear distinction between the Buddha and others.
But it's not interested in views.
So again we have someone coming to the Buddha saying,
all right, here's what I believe.
Keeping his views all bottled up is the thing about views.
Because they're hard to hold on to.
It takes energy and effort.
So you have to exercise.
Because you're constantly a swaghed by your own doubt.
So it's a view. It's not based on empirical evidence.
So in order to maintain it, you have to push.
And the push is in trying to get other people to believe the same things as you.
This is why people proselytize.
They may say it's for whatever reason.
But it's actually to reaffirm their own belief.
Because belief is difficult to take, difficult to keep.
So this Brahman he can't keep it.
He's holding onto this view and he's like,
believes it's so much.
But in order to maintain that belief, whenever it starts to slip,
he would have to go in.
I mean, just assuming he would have to go and tell others.
That's why he comes to the Buddha.
That's why people would come to the Buddha.
It would become to me and with these sort of things, all their views.
It's nice to have the example of the Buddha to say,
yeah, well, that's your view.
There was an article someone posted recently
and I was discussing it with them about this fact.
And the idea that people think that their view,
the idea of an opinion where people will come to you and say,
well, my opinion, in my opinion, X.
And it would be something like, in my opinion,
vaccines give you Alzheimer's.
You know, something like that.
I mean, I don't know about vaccines giving you Alzheimer's,
but the trust of the article is that,
well, in many cases, it's not even really an opinion.
It's just a mistake.
It's an erroneous statement.
It's just wrong.
An opinion is I like onions or I like lavender or something.
But there's this idea that you could hide behind something
because it's your opinion.
And also, if something is just my opinion,
like everyone's entitled to their opinions.
I mean, well, yeah, but it doesn't mean anything
that you have an opinion.
And so we were talking about whether there's
any meaning to having an opinion.
And besides how it affects your own mind,
there's really nothing.
It means nothing.
When you tell me, I believe X, it's really meaningless.
You know, if 90% of humans believed something,
it would still be meaningless.
You would still have no bearing on the truth.
If 99% of humans believed something
would still have no bearing on the truth.
So belief in and of itself is of no import.
Anyway, people seem to think it is important,
but it's important that they tell you.
So he came to go to the Buddha and said,
OK, this is my opinion.
There's no harm in telling people the truth.
It's an interesting statement.
It reminds one of the question of whether telling the truth
means not lying means always telling the truth.
Does not lying always mean telling the truth?
And from a Buddhist point of view,
obviously, especially here, the answer is no.
This isn't what we mean by not telling a lie.
Not telling a lie is actually lying.
If you omit the truth, and as a result,
someone has a mistaken understanding,
that's not the same as lying.
If you do anything to intentionally mislead someone,
well, that could be considered lying.
But withholding the truth is not considered lying,
because withholding the truth can often be quite beneficial.
The problem with lying is that it's a distortion.
It's a perversion of reality.
What you're doing is you're intentionally taking someone away
from the truth.
And so inherently, it's antithetical
to the practice of Buddhism and attainment of the truth.
But the other thing about this statement
that the Buddha gives race is, I don't say that.
I don't hold on to such a view.
Instead, this is the Buddha looks at things
from a completely different point of view,
and it's in one sense utilitarian,
if it's beneficial.
If it helps, if saying something helps,
then he will say it.
If saying something doesn't help, then he won't say it.
Now, the question then comes,
what could you lie if it was beneficial?
Or could you kill if it was beneficial?
And the point is that, of course, that's not possible.
If it's a lie is problematic, is inherently harmful.
And so, no.
Doing something that is harmful is not in the capability
actually, have been enlightened being capable of intentional harm
to themselves or others.
So, intentionally telling a lie is not possible.
Intentionally stealing, intentionally killing,
intentionally doing something that harms others.
They're intended for the intention to harm.
Or oneself.
I mean, intent having the unwholesome mind state
that would give rise to killing or stealing is not possible.
But the point here is in the difference
in the Buddha's approach to reality and it's in regards to
not whether it's this way or that way,
but whether it's helpful.
So, the Buddha is quite malleable and quite flexible
in regards to a lot of the views that arose.
So, as far as telling truth,
that's quite a simple teaching.
I mean, the actual teaching here is just that
it's not always useful to tell the truth,
but the premise behind it is more interesting.
And it's that in regards to views,
they're not really of any important opinions
are not useful.
The Buddha doesn't hold on to them,
doesn't give them any weight.
What he gave way to is truth, value, benefit.
So, that's the demo for tonight.
Does anybody have any questions?
What if somebody can handle the truth?
I don't understand.
No, such as the non-existence.
I mean, cannot handle the truth, is that what you're asking?
I think we've got some grammar issues.
If someone can't handle the truth,
like they believe that God exists,
so you shouldn't tell them that he doesn't exist.
That God doesn't exist.
I mean, this is another, you don't have to.
It's important to always be right.
It's not always important.
It's not always wise to try to convince other people
that you're right.
And there's a big difference.
It's not our duty to fix other people.
It's not our role, our goal doesn't help to try.
It's our role and our duty to be clear in the mind,
to be mindful, to be beneficial,
to be helpful, to be kind,
to be pure.
I have, so we have something that we wanted to talk about.
If Robin is amenable, maybe I could ask her to say something about our project.
Sure.
Can you still hear me?
Yes, I can.
And about tomorrow.
So if there's anybody who wants to join us tomorrow,
they can, we're at seven o'clock.
Just let them know.
They can contact and I'm allowed them to hang up.
Sure.
So what we're meeting on tomorrow at seven
for anyone who's interested is we're putting together
an online campaign.
Bunte is interested in establishing a monastery up in Canada
in the area of McMaster University,
which is a really exciting project.
It's going to be great for the students at McMaster
to have a Buddhist monastery,
a place to go and learn meditation.
But it's also going to be a benefit to people
who may never make it to Canada.
Although it's not that far, but even for people that are far away,
it's a really interesting concept.
What Bunte does now with broadcasting things that are going on on the internet.
It's just a wonderful idea of having local meditation courses
and local daily Dhamma and broadcasting it as well to us.
So it's really, really exciting and definitely want to support him in that.
So we have a campaign like the type that you see.
It's through a website called YouCaring
and we should have it ready tomorrow to roll out.
And hopefully people will be interested in supporting it
and sharing it with other people who might be interested in supporting it.
Awesome. Thank you.
Yeah. I mean, part of it is having a support group here
to do these sorts of things.
I mean, right now I'm doing everything on my own.
If you see, I just recorded the video for the project here.
There's the camera, right?
And then over there there's a place to...
Anyway, I'm not going to show you my room. It's all missing.
There's a station where that's currently
or just finished transcoding the video.
So it goes from this station to the end.
But I'm doing it all here in this small, small room.
And so the idea is that that probably should change.
So we're going to be starting at the place,
but we need help in organizing and supporting making it happen.
So tomorrow we're going to have a meeting.
And if anyone would like to join the meeting,
it's going to be online.
Just let us know and we'll add you to the Hangout at 7 p.m. Eastern.
And other than that, just wait for the YouTube video to come out.
Shouldn't have it. I mean, it's ready now.
I just have to upload it.
And then there's going to be links and everything there.
So I'm going to get involved.
Actually, maybe we can kind of premiere it here with this group
tomorrow before or at what point will it go live on YouTube?
Tonight. Yeah.
I mean, I can leave it private tonight, right?
Yeah, maybe premiere it here tomorrow with the with the script here.
That'd be nice.
Sure.
Anyway, back to some questions.
When a viewer opinion arises, I find myself verbalizing in the head instead,
along with my noting, if they're a way to not do so.
Well, you can't control. You see, this is the thing that you're learning is that we're not in control exactly.
We can change. We can steer ourselves in a different direction,
but it's got to be moment to moment.
And so for something to just not happen, it's like turning a switch.
It's not, it doesn't work that way.
Right now you're going heading in a,
or you're in a certain bend or a certain direction that includes that sort of verbal
verbalization, mentalization.
But you can steer away from that if you start to cultivate different habits.
You have to identify which habits or which activities are promoting the habit.
And you start to change as you see that those are un,
unbeneficial and they're leading to harmful habits, et cetera, et cetera.
So if a father wants their children to be Catholic and they don't want.
Yeah, I mean, sometimes you might just go along with something, right?
But you don't go along to it to the extent that it means you're lying.
But you just might not say, Father, I'm not a Catholic.
You know, you don't have to tell them that you're not a Catholic.
It doesn't really help him, not likely.
I mean, if you think it could, this is the thing.
But I said, if it's beneficial, the problem is it's hard for us to tell what is truly beneficial.
So that's where uncertainty comes in.
There are no certain answers, but there are certain answers.
We just don't have them.
So it's more about us trying in our muddled way to discern what is right.
It's not just, yes, always.
It'll be much easier if the answer was always tell people the truth.
When the opportunity arises.
But clearly that's not the right way.
So normally normal that labeling things seems more distracting than just noting them,
noticing them, but not picking word.
Yeah.
It's not distracting, but it's disturbing and it's meant to be.
It's meant to disturb your habitual stream.
You know, their habitual reactions.
So we have this, we're comfortable in the way that we deal with things.
But that being comfortable, that comfortable involves defilements.
And so we're trying to disrupt it.
The other thing is it's showing you how chaotic things really are.
So we have a way of surfing over the waves, the disturbances.
Sort of just superficial investigation, keeping it superficial enough so that it doesn't bother us.
This meditation is bringing you right in there.
And that's not easy.
And so as a result, you're seeing how difficult it is and how chaotic it is.
The third is that it's just a part of the beginner stages in the practice.
For an advanced meditator, they're much better able to have a greater than superficial involvement with reality.
And so as a result, it's not so disturbing.
But in the beginning, you're doing it a lot wrong or it's clumsy, it's unskillful.
And so yeah, as a result, it's going to be chaotic.
But no, it's not actually distracting.
That maybe how it appears because you have this idea that it's a better meditation somehow to just go with it.
But it's actually not a disturbing.
It's not distracting.
It's just disturbing.
And it's disturbing for these reasons.
First of all, reality is kind of disturbing.
Second of all, you're not in the beginning very good at approaching reality.
And third, it's a part of the actual practice to be disturbed.
Because the superficial ability to surf over the experiences,
belies the fact that it's actually not a way to find happiness.
That this is not sustainable, it's not conducive to true and lasting happiness.
In truth, once you see reality clearly, you reject it entirely and you leave it behind.
And in favor of freedom, you leave behind everything step by step in your life.
You start letting go of the more complicated aspects of your life,
identifying until eventually you just let go of everything and have no interest in it being involved at all.
So some questions about this project that we kind of vaguely mentioned.
I mean, I have to be a little vague because I can't say, but we need support.
To make this happen, we need people to help.
So if you want to help organize, there's a meeting.
But we're going to set up something on a crowdsourcing site.
So how many of you want to explain?
Sure.
It's a nice site called U-Caring.
And there's a ton of crowdsourcing, crowdsourced funding sites.
But we're able to find one that doesn't charge a percentage other than the PayPal percentage,
which everyone has to charge. That's a PayPal policy.
So in other words, Bontay's organization will receive the maximum amount of donations that are given to support this project.
So we have the amount that we are hoping to raise, which is initially $6,000 to pay for some very basic expenses.
The first and last months are into utilities and just real basic things like that.
So that will be available tomorrow.
And it will be on YouTube.
And, you know, the basic kind of thing that you can share on your Facebook if you so choose or if you know particular people that you know might be interested.
You know, please share it with them. They can be shared by email, many different methods of just getting the word out and be a great way to support.
And as far as the seven o'clock meeting tomorrow, Bontay, a couple of people are asking where to go.
That's actually on Google Hangouts, right?
Yeah, you have to get in light.
So you have to just send us an email or a message and send one of us a message and that's an organizational meeting.
It's only for if you want to be involved in organizing.
And we don't need, I suppose, we're going to need a few people involved in spreading the word and coming brainstorming and working out logistics and so on.
We have to find a property and we've been looking at rental properties and have to find the right one.
We have to go take a look at it.
Hopefully when I go to McMaster, we're going to find a community there who some people might be able to help out with these sorts of things.
I just put my email in the chat window if anyone is interested in helping out and being a volunteer or, you know, even just showing up just to kind of give some feedback.
We'll be looking at the video and everything for the first time together and just making sure we didn't overlook anything.
So if anyone's interested in attending or helping out, just send me an email and I'll make sure Bante has that information.
Okay, another question.
Is Buddhism or religion that focuses on worship towards Buddha rather than your own well-being?
I would say Jacob, no.
Simon says Jacob, no.
Absolutely. When pretty much the opposite, right, it's much more about your own well-being and very little about any sort of worship of any kind.
I mean, we do kind of worship the Buddha in the sense that we think he is someone worthy of our reverence, but it's more like reverence than worship.
The common word worship, I think, has become too much of sort of devoted towards the simple ritual as a God or something, but it wasn't certainly a God.
What would be your specific qualifications for our donation? Rule number one, don't ask me about our donation. Qualification number one, stop asking about our donation.
And that's not really each tongue tongue in cheek because, I mean, it is, but it isn't because anyone who comes to me looking to ordain at this point, it's just a red flag.
I'm sorry, and that sounds probably pretty bad. And it's probably in your case, not true, which is why I feel comfortable saying it because focus on meditation.
And I'm not chastising you or anything. I'm saying, you want it to happen. You're that serious.
Then it has to be, I'm so serious about meditation. I want to put on some rectangle of cloth and keep doing it. It has to be that.
And it can't be about, I want to become a monk because I think it's cool to wear a toga.
Right. So think of it like that, that 80s Kung Fu movie that I always referenced, where he sat in the courtyard.
And they wouldn't let him in. They said, no, no, a fight club as well. One of the last movies I saw before I came a monk fight club where they make you stand outside.
But there was an earlier movie this Shaolin movie where they made he sat outside and they said, no, no, we don't accept foreigners and they just ignored him and sat in front of the monastery until they let him in.
And of course he ended up causing a ruckus and big upset in the monastery.
So yeah, there's that. But let's say you have to approach it. You want, you come and do meditation course and then we'll talk about whether you can do a second meditation course.
If you've done that, then we can talk about, well, maybe a third meditation course. Let me kind of get the point and get the picture.
Because, you know, for that you stay, then you can stay.
I think you've answered all the questions, Dante.
That's what I was thinking. Yeah, there were like 200 questions piled up on Google moderator. So this is nice.
All caught up.
Awesome. Thank you, Robin, for being there to help. Oh, thank you.
All right, let's call it a night. See you all tomorrow.
Thank you, Dante.
