1
00:00:00,000 --> 00:00:04,000
Hello and welcome back to our study of the Damapanda.

2
00:00:04,000 --> 00:00:12,000
Today we continue on with verse 114, which reads as follows.

3
00:00:12,000 --> 00:00:20,000
Yocha was a satan, the way a person, a madan, badan.

4
00:00:20,000 --> 00:00:28,000
Aikahan, the Yivitan, seyo, passato, a madan, badan.

5
00:00:28,000 --> 00:00:32,000
Which, again, is very similar to the verses that we've been looking at.

6
00:00:32,000 --> 00:00:41,000
It means better than to live 100 years, not seeing the past to the deathless.

7
00:00:41,000 --> 00:01:05,000
So this verse was taught in response to the story of Kisa go to me.

8
00:01:05,000 --> 00:01:14,000
Kisa go to me was, there's some idea that she was a relative of the Buddha, though I'm not sure.

9
00:01:14,000 --> 00:01:24,000
Because she comes up in an earlier story of when Siddhartha was going forth.

10
00:01:24,000 --> 00:01:29,000
And one of the commentaries, I think.

11
00:01:29,000 --> 00:01:30,000
One of the other commentaries.

12
00:01:30,000 --> 00:01:38,000
But at any rate, we have Kisa means thin, so she was known as thin go to me.

13
00:01:38,000 --> 00:01:39,000
Which apparently is a problem.

14
00:01:39,000 --> 00:01:42,000
It was a problem at the time.

15
00:01:42,000 --> 00:01:53,000
The measure of beauty at the time would have been plumpness and a nice full belly to be able to belly dancing.

16
00:01:53,000 --> 00:01:56,000
It was a big thing.

17
00:01:56,000 --> 00:02:02,000
So thin people, thin women were not attractive, probably not thin men either.

18
00:02:02,000 --> 00:02:06,000
And so she had trouble getting a husband.

19
00:02:06,000 --> 00:02:11,000
But finally, there was some very, very strange thing that allowed her to get a husband.

20
00:02:11,000 --> 00:02:14,000
And I'm not really sure how we should interpret the story.

21
00:02:14,000 --> 00:02:17,000
It doesn't have anything to do with the story, so I'm not going to go into detail.

22
00:02:17,000 --> 00:02:26,000
But something about gold or... I don't really get it.

23
00:02:26,000 --> 00:02:31,000
So I'm not going to try and relate it.

24
00:02:31,000 --> 00:02:36,000
But suffice to say, she was seen as being somehow good luck.

25
00:02:36,000 --> 00:02:44,000
Because she was able to see charcoal as gold or something like that.

26
00:02:44,000 --> 00:02:48,000
So she finally got a husband as the point.

27
00:02:48,000 --> 00:02:54,000
And a big deal for her was having a child.

28
00:02:54,000 --> 00:02:58,000
And so she became pregnant.

29
00:02:58,000 --> 00:03:01,000
And that this was a really big deal for her.

30
00:03:01,000 --> 00:03:05,000
She was very keen to be a mother.

31
00:03:05,000 --> 00:03:17,000
And after 10 months, pregnancy she gave birth to a son.

32
00:03:17,000 --> 00:03:20,000
And this was such a joy to her.

33
00:03:20,000 --> 00:03:22,000
Going through labor was not a chore.

34
00:03:22,000 --> 00:03:26,000
She was very keen for this to occur.

35
00:03:26,000 --> 00:03:33,000
As I would say most people would be having carried a child around for 10 months.

36
00:03:33,000 --> 00:03:45,000
He would grow rather fond and attached to this being that's growing inside of you.

37
00:03:45,000 --> 00:03:52,000
And so once it was born, she was very much attached to this son.

38
00:03:52,000 --> 00:03:57,000
Very much in love with her child.

39
00:03:57,000 --> 00:04:03,000
And he lived his first year growing up as children do.

40
00:04:03,000 --> 00:04:08,000
But around the time you learned to walk, he contracted a sickness.

41
00:04:08,000 --> 00:04:15,000
And because medicine was at an unadvanced stage back then,

42
00:04:15,000 --> 00:04:19,000
he quickly died.

43
00:04:19,000 --> 00:04:30,000
And it's a place to say she's just a quesago to me was devastated because the son of hers was pretty much her whole world.

44
00:04:30,000 --> 00:04:38,000
At the same time, she seems to have led a rather sheltered life because she had never seen death before.

45
00:04:38,000 --> 00:04:41,000
And didn't really understand what death meant.

46
00:04:41,000 --> 00:04:48,000
And as a result, she picked up her dead son when they tried to burn him, burn his body.

47
00:04:48,000 --> 00:04:53,000
She wouldn't let them and she picked them up and carried him on her hip.

48
00:04:53,000 --> 00:05:04,000
And went off to find a doctor, someone to cure her son.

49
00:05:04,000 --> 00:05:09,000
And so she went from house to house trying to find someone who would have medicine who could cure her son.

50
00:05:09,000 --> 00:05:12,000
There must be a way to bring him back.

51
00:05:12,000 --> 00:05:20,000
She had no concept of death of how it's final.

52
00:05:20,000 --> 00:05:30,000
And so like any mother would do anything to find a cure if there were some way.

53
00:05:30,000 --> 00:05:40,000
And everyone turned her away, cursing her and ridiculing her and calling her rightfully

54
00:05:40,000 --> 00:05:45,000
crazy or foolish.

55
00:05:45,000 --> 00:05:48,000
But she didn't listen. She wouldn't listen.

56
00:05:48,000 --> 00:05:56,000
And she stubbornly refused to accept the fact that death is one way true.

57
00:05:56,000 --> 00:05:59,000
And then in fact what she was carrying around wasn't actually her son.

58
00:05:59,000 --> 00:06:05,000
It was just the worn out skin, worn out like a worn out snake skin.

59
00:06:05,000 --> 00:06:13,000
It was the leftover.

60
00:06:13,000 --> 00:06:26,000
And so finally there happened to be a rather wise person who heard her asking again and again for medicine.

61
00:06:26,000 --> 00:06:32,000
And correctly, correctly deduced that this was just a matter of not understanding death.

62
00:06:32,000 --> 00:06:44,000
And so he realized she needed someone to explain to her to help her to understand and gently and wisely find a way to lean her.

63
00:06:44,000 --> 00:06:46,000
Because it's not that people hadn't tried.

64
00:06:46,000 --> 00:06:53,000
It seems that people had been trying to explain to her death is one way.

65
00:06:53,000 --> 00:06:58,000
But she wouldn't listen to them. She was very, very difficult to teach.

66
00:06:58,000 --> 00:07:05,000
It seemed quite impossible to get through to her.

67
00:07:05,000 --> 00:07:11,000
And so this man said, well, I don't know of any medicine that can heal your child.

68
00:07:11,000 --> 00:07:19,000
But I know someone and he knows the cure.

69
00:07:19,000 --> 00:07:23,000
And she was elated, ecstatic. She said, who could that be?

70
00:07:23,000 --> 00:07:33,000
Oh, he lives in, he lives in, he lives in, I see they use very, very archaic language in these stories.

71
00:07:33,000 --> 00:07:41,000
So I fall into it. Now the, let me solve these words.

72
00:07:41,000 --> 00:07:47,000
In Jetamana, in Jetas Grove, go outside of the city and into the forest.

73
00:07:47,000 --> 00:07:51,000
And there's the, you know, the forest of Jetah.

74
00:07:51,000 --> 00:07:57,000
And go there. And that's where this, this doctor is residing.

75
00:07:57,000 --> 00:08:02,000
And so she immediately got up. Thank you. So thank you.

76
00:08:02,000 --> 00:08:10,000
Went off on her way to find this person who could heal her son and restore meaning to her life.

77
00:08:10,000 --> 00:08:19,000
Because for her meaning was her whole attention, you know, how, how attached we can become to, to,

78
00:08:19,000 --> 00:08:26,000
especially people, but to anything really.

79
00:08:26,000 --> 00:08:43,000
And so she went to Jetahana and met with this doctor who, of course, was our beloved teacher and leader, the Buddha.

80
00:08:43,000 --> 00:08:48,000
And she asked him, she said, look, I've heard,

81
00:08:48,000 --> 00:08:52,000
people tell me that you have a way to cure my child.

82
00:08:52,000 --> 00:08:58,000
Is this true? Is it, could it be possible that you have some medicine?

83
00:08:58,000 --> 00:09:04,000
And he said, well, I know, yeah, I, I have to cure.

84
00:09:04,000 --> 00:09:09,000
And she says, what, what is it, what is the cure?

85
00:09:09,000 --> 00:09:16,000
So well, you have to bring to me some mustard seed.

86
00:09:16,000 --> 00:09:19,000
And she said, mustard seed, well, that's easy.

87
00:09:19,000 --> 00:09:24,000
Well, where, where is it like special or, or some special mustard seed?

88
00:09:24,000 --> 00:09:28,000
It can't be just that where, where it will sort of mustard seed you want me to get.

89
00:09:28,000 --> 00:09:31,000
He said, well, no, just any old mustard seed will do.

90
00:09:31,000 --> 00:09:39,000
But you have to get me the mustard seed from a house where no one has ever died.

91
00:09:39,000 --> 00:09:45,000
No son or daughter has ever died.

92
00:09:45,000 --> 00:09:47,000
And she said, oh, well, that's fine.

93
00:09:47,000 --> 00:09:50,000
I'm going to do that right away.

94
00:09:50,000 --> 00:09:54,000
And so she wandered around from house to house.

95
00:09:54,000 --> 00:09:57,000
But everyone she talked to, they all had mustard seed.

96
00:09:57,000 --> 00:10:02,000
It was a common spice in India, still is.

97
00:10:02,000 --> 00:10:05,000
But there was no house where no one had died.

98
00:10:05,000 --> 00:10:10,000
You see, everyone was somebody's son or daughter and everyone's son or daughter.

99
00:10:10,000 --> 00:10:16,000
Every, every house, somebody's son or daughter, of course, and died.

100
00:10:16,000 --> 00:10:25,000
And especially with, with medicine and, and I'm unable to cure very simple sicknesses,

101
00:10:25,000 --> 00:10:27,000
death would have been a common thing.

102
00:10:27,000 --> 00:10:35,000
And this, they would say most, most people that, you know, just, just for miscarriage alone.

103
00:10:35,000 --> 00:10:43,000
Apparently, this is a, one of the, one of the arguments against God

104
00:10:43,000 --> 00:10:53,000
is that a woman's pelvic bone is to, is, or something to do,

105
00:10:53,000 --> 00:10:55,000
something about the pelvic region is the wrong size.

106
00:10:55,000 --> 00:10:58,000
And it's not really meant for childbirth,

107
00:10:58,000 --> 00:11:01,000
or it's that the human head has grown too big.

108
00:11:01,000 --> 00:11:04,000
Because childbirth shouldn't really happen.

109
00:11:04,000 --> 00:11:08,000
It's not really where the body isn't really made for childbirth.

110
00:11:08,000 --> 00:11:11,000
You know, even though it is kind of made for childbirth,

111
00:11:11,000 --> 00:11:15,000
it's, it's a very, very poorly built system.

112
00:11:15,000 --> 00:11:17,000
So the head is, is too big.

113
00:11:17,000 --> 00:11:23,000
And oftentimes requires caesarian section, or else both the mother and the,

114
00:11:23,000 --> 00:11:25,000
and the child can die.

115
00:11:25,000 --> 00:11:31,000
So back when they didn't have this, death in childbirth was, was common.

116
00:11:31,000 --> 00:11:37,000
You know, and even when born still birth, that's what's common.

117
00:11:37,000 --> 00:11:39,000
Miscarriage common.

118
00:11:39,000 --> 00:11:43,000
Birth of a child, you know, under, under five years old would have been common

119
00:11:43,000 --> 00:11:52,000
with all the sicknesses and children's susceptibility to disease.

120
00:11:52,000 --> 00:11:57,000
So death, death is everywhere.

121
00:11:57,000 --> 00:12:01,000
Death takes us all, the death.

122
00:12:01,000 --> 00:12:04,000
We live in the realm of death, somebody said,

123
00:12:04,000 --> 00:12:09,000
because we're all, it's like we are on death row,

124
00:12:09,000 --> 00:12:11,000
just waiting for our sentence.

125
00:12:11,000 --> 00:12:14,000
Of course we've come to accept it and we think of it as natural,

126
00:12:14,000 --> 00:12:17,000
but we are a lot like prisoners on death row,

127
00:12:17,000 --> 00:12:19,000
just waiting for our turn.

128
00:12:19,000 --> 00:12:22,000
And in fact, no one knows when their turn is going to be.

129
00:12:22,000 --> 00:12:24,000
We haven't been told when the execution is.

130
00:12:24,000 --> 00:12:28,000
We haven't even been told what sort of execution it's going to be.

131
00:12:28,000 --> 00:12:32,000
It could be slow, it could be quick, it could be tortuous.

132
00:12:32,000 --> 00:12:35,000
You know, it's not death by lethal injection,

133
00:12:35,000 --> 00:12:37,000
not most likely, not for most of us.

134
00:12:37,000 --> 00:12:41,000
For many of us it's going to be terribly painful.

135
00:12:41,000 --> 00:12:44,000
Fear some.

136
00:12:44,000 --> 00:12:52,000
We're like prisoners in the dungeon waiting to be beheaded or worse.

137
00:12:52,000 --> 00:12:55,000
The realm of the deathless is where we live.

138
00:12:55,000 --> 00:13:01,000
Not to be a total downer.

139
00:13:01,000 --> 00:13:04,000
Whatever house she went to would say this,

140
00:13:04,000 --> 00:13:08,000
and so she would have accepted their mustard seed

141
00:13:08,000 --> 00:13:10,000
and she'd have to give it back to them and say,

142
00:13:10,000 --> 00:13:12,000
no, this mustard seed won't do.

143
00:13:12,000 --> 00:13:15,000
And from house to house and sometimes it was fresh

144
00:13:15,000 --> 00:13:18,000
and people's minds and so they would be sad.

145
00:13:18,000 --> 00:13:23,000
And maybe even cry.

146
00:13:23,000 --> 00:13:28,000
And she felt, she saw this sadness reflected in other people

147
00:13:28,000 --> 00:13:31,000
and it really opened her eyes.

148
00:13:31,000 --> 00:13:33,000
And so this was the way of teaching her

149
00:13:33,000 --> 00:13:35,000
without actually having to tell her,

150
00:13:35,000 --> 00:13:37,000
without trying to argue with her,

151
00:13:37,000 --> 00:13:39,000
was to see that this was a fruitless,

152
00:13:39,000 --> 00:13:40,000
a hopeless task.

153
00:13:40,000 --> 00:13:44,000
And slowly this corpse on her side

154
00:13:44,000 --> 00:13:53,000
lost its attraction, lost its appeal.

155
00:13:53,000 --> 00:13:55,000
And she began to see it for what it was.

156
00:13:55,000 --> 00:13:57,000
It's just an empty husk.

157
00:13:57,000 --> 00:14:02,000
Started to see that death is just really a part of life.

158
00:14:02,000 --> 00:14:04,000
It is natural.

159
00:14:04,000 --> 00:14:13,000
Not a good thing, but certainly natural.

160
00:14:13,000 --> 00:14:16,000
And so time after time going to house after house,

161
00:14:16,000 --> 00:14:22,000
she eventually lost all of her attachment to this quest.

162
00:14:22,000 --> 00:14:25,000
And she looked down at the corpse.

163
00:14:25,000 --> 00:14:28,000
And she made a decision,

164
00:14:28,000 --> 00:14:32,000
went into the forest and dug a grave

165
00:14:32,000 --> 00:14:34,000
and buried her child,

166
00:14:34,000 --> 00:14:36,000
or placed the child in the forest.

167
00:14:36,000 --> 00:14:38,000
Maybe probably didn't bury it.

168
00:14:38,000 --> 00:14:42,000
Just laid it down in the forest.

169
00:14:42,000 --> 00:14:45,000
And then she went into the Buddha

170
00:14:45,000 --> 00:14:48,000
and bowed down to him.

171
00:14:48,000 --> 00:14:50,000
They'd respect and he said,

172
00:14:50,000 --> 00:14:53,000
oh, so did you get the mustard seed?

173
00:14:53,000 --> 00:14:55,000
And she said, no, I didn't.

174
00:14:55,000 --> 00:15:00,000
You never village the dead or far greater than the living.

175
00:15:00,000 --> 00:15:03,000
And the Buddha said, indeed,

176
00:15:03,000 --> 00:15:06,000
it was foolish of you to think

177
00:15:06,000 --> 00:15:08,000
that only you have lost.

178
00:15:08,000 --> 00:15:10,000
So I think that death has only come to you

179
00:15:10,000 --> 00:15:14,000
all beings are subject to death.

180
00:15:14,000 --> 00:15:15,000
And he said, the prince of death,

181
00:15:15,000 --> 00:15:17,000
like a raging torrent,

182
00:15:17,000 --> 00:15:19,000
sweeps away into the sea of ruin,

183
00:15:19,000 --> 00:15:21,000
all living beings,

184
00:15:21,000 --> 00:15:24,000
still are their longings unfulfilled.

185
00:15:24,000 --> 00:15:27,000
And then he again taught a verse

186
00:15:27,000 --> 00:15:30,000
that comes in the later chapter, 287.

187
00:15:30,000 --> 00:15:31,000
But we'll get to that later,

188
00:15:31,000 --> 00:15:34,000
so I'm not going to recite that.

189
00:15:34,000 --> 00:15:36,000
And as he taught her this,

190
00:15:36,000 --> 00:15:41,000
and based on just this ordeal that he had she'd been through

191
00:15:41,000 --> 00:15:43,000
and then the teaching of the Buddha,

192
00:15:43,000 --> 00:15:46,000
she was actually able then and there to apply it

193
00:15:46,000 --> 00:15:49,000
and to become a sotupana through his teachings.

194
00:15:49,000 --> 00:15:53,000
Which is quite remarkable.

195
00:15:53,000 --> 00:15:57,000
And as a result, she requested to become a biquini

196
00:15:57,000 --> 00:15:59,000
and she became a biquini

197
00:15:59,000 --> 00:16:02,000
and fulfilled her duties and practiced meditation.

198
00:16:02,000 --> 00:16:07,000
And then the story goes that one day

199
00:16:07,000 --> 00:16:11,000
every two weeks they'd have a gathering

200
00:16:11,000 --> 00:16:13,000
on the full moon in the empty moon,

201
00:16:13,000 --> 00:16:15,000
all the monks gathered together.

202
00:16:15,000 --> 00:16:17,000
And so if the gathering is done at night,

203
00:16:17,000 --> 00:16:19,000
which in some monasteries it is,

204
00:16:19,000 --> 00:16:20,000
you have to lay the lamp.

205
00:16:20,000 --> 00:16:22,000
So they would go and put the oil

206
00:16:22,000 --> 00:16:25,000
and the wick and light the lamp.

207
00:16:25,000 --> 00:16:28,000
And one day it was her turn to do this.

208
00:16:28,000 --> 00:16:30,000
And so she went and she lit the lamp

209
00:16:30,000 --> 00:16:32,000
and of course she had been meditating.

210
00:16:32,000 --> 00:16:36,000
And so she stood there watching this flame flicker.

211
00:16:36,000 --> 00:16:40,000
And she saw the different flames come up from the lamp

212
00:16:40,000 --> 00:16:44,000
and some flames would last a while

213
00:16:44,000 --> 00:16:46,000
and some would flicker out.

214
00:16:46,000 --> 00:16:49,000
And she watched this arising and ceasing.

215
00:16:49,000 --> 00:16:53,000
And because she was so in tune with this aspect

216
00:16:53,000 --> 00:16:57,000
of cessation and birth

217
00:16:57,000 --> 00:17:01,000
and true birth and death of every moment,

218
00:17:01,000 --> 00:17:05,000
it really hit her that that's really how our lives are.

219
00:17:05,000 --> 00:17:09,000
And she said our lives are just like these flames.

220
00:17:09,000 --> 00:17:13,000
We come into existence and poof

221
00:17:13,000 --> 00:17:14,000
when we're gone, that's it.

222
00:17:14,000 --> 00:17:17,000
There's no getting the flame back once it's gone.

223
00:17:17,000 --> 00:17:28,000
And the Buddha got a sense of this

224
00:17:28,000 --> 00:17:32,000
and came to see her as with the last story.

225
00:17:32,000 --> 00:17:34,000
And that's when he taught her this verse.

226
00:17:34,000 --> 00:17:36,000
He said,

227
00:17:36,000 --> 00:17:38,000
even as it is with these flames.

228
00:17:38,000 --> 00:17:41,000
So also it is with living beings here in the world.

229
00:17:41,000 --> 00:17:44,000
Some flare up whether there's flicker out.

230
00:17:44,000 --> 00:17:48,000
They only that have reached nibana or no more seem.

231
00:17:48,000 --> 00:17:50,000
So our lives are like a flame.

232
00:17:50,000 --> 00:17:53,000
We burn and then out.

233
00:17:53,000 --> 00:17:55,000
Then we come back again and again.

234
00:17:55,000 --> 00:17:57,000
If the flame is still lit,

235
00:17:57,000 --> 00:17:59,000
if the weak is still lit,

236
00:17:59,000 --> 00:18:01,000
the flame will come back again and again,

237
00:18:01,000 --> 00:18:03,000
fades out, comes back.

238
00:18:03,000 --> 00:18:06,000
Some birth and death, birth and death,

239
00:18:06,000 --> 00:18:08,000
incessant.

240
00:18:08,000 --> 00:18:10,000
But when you blow out,

241
00:18:10,000 --> 00:18:12,000
when the fire gets blown out,

242
00:18:12,000 --> 00:18:17,000
then there's no more flame.

243
00:18:17,000 --> 00:18:19,000
And so then he teaches the death lesson.

244
00:18:19,000 --> 00:18:23,000
So this is the idea that this verse is appropriate

245
00:18:23,000 --> 00:18:26,000
because it talks about death.

246
00:18:26,000 --> 00:18:29,000
It's appropriate because the story is all about death.

247
00:18:29,000 --> 00:18:34,000
So the key here was this realization of the prevalence

248
00:18:34,000 --> 00:18:39,000
and the ubiquitousness of death.

249
00:18:39,000 --> 00:18:43,000
And death is everywhere for everyone

250
00:18:43,000 --> 00:18:45,000
that we live in the realm of death.

251
00:18:45,000 --> 00:18:50,000
And so the Buddha taught specifically about the deathless.

252
00:18:50,000 --> 00:18:52,000
Because nibana or freedom are enlightened,

253
00:18:52,000 --> 00:18:56,000
this is the deathless because you never come back.

254
00:18:56,000 --> 00:18:59,000
You don't flicker up and die out.

255
00:18:59,000 --> 00:19:08,000
There's no more flame than become free.

256
00:19:08,000 --> 00:19:10,000
So this relates to our practice.

257
00:19:10,000 --> 00:19:12,000
This really relates to the ultimate goal.

258
00:19:12,000 --> 00:19:14,000
And for many people, it's kind of a foreign concept.

259
00:19:14,000 --> 00:19:16,000
I think many people practice meditation

260
00:19:16,000 --> 00:19:18,000
without much concept of nibana,

261
00:19:18,000 --> 00:19:20,000
even without much desire for it,

262
00:19:20,000 --> 00:19:24,000
because it goes so contrary to our desires.

263
00:19:24,000 --> 00:19:28,000
We can see how some things are causing a suffering

264
00:19:28,000 --> 00:19:31,000
and we do wish to be free from suffering.

265
00:19:31,000 --> 00:19:34,000
But it's a whole other thing to take it to its extreme

266
00:19:34,000 --> 00:19:38,000
to take it to its logical conclusion, though.

267
00:19:38,000 --> 00:19:42,000
And think about a complete cessation.

268
00:19:42,000 --> 00:19:48,000
If I cease, then what's left of all the things that I love and enjoy?

269
00:19:48,000 --> 00:19:52,000
To how this relates directly to our practice,

270
00:19:52,000 --> 00:19:55,000
it's something that you have to, it's on the horizon.

271
00:19:55,000 --> 00:19:57,000
But it's the logical outcome.

272
00:19:57,000 --> 00:20:00,000
It's not like it's something you fall into.

273
00:20:00,000 --> 00:20:03,000
It's something that eventually a person comes to.

274
00:20:03,000 --> 00:20:07,000
Because as long as we have desire for something,

275
00:20:07,000 --> 00:20:11,000
we're going to be reborn again and again and again and again.

276
00:20:11,000 --> 00:20:12,000
There's no question there.

277
00:20:12,000 --> 00:20:15,000
It's just that once you start to look clearly,

278
00:20:15,000 --> 00:20:18,000
and it may take lifetimes to do so,

279
00:20:18,000 --> 00:20:22,000
but eventually you start to become disenchanted.

280
00:20:22,000 --> 00:20:24,000
You see that it's the same thing again and again.

281
00:20:24,000 --> 00:20:28,000
And it's not really as pleasant or as comfortable or as happy

282
00:20:28,000 --> 00:20:30,000
as we think it's going to be.

283
00:20:30,000 --> 00:20:34,000
We just end up wanting more and more and more and just getting disappointed.

284
00:20:34,000 --> 00:20:38,000
We have to learn all these lessons about how we can't ever get what we want,

285
00:20:38,000 --> 00:20:39,000
and that's really unpleasant.

286
00:20:39,000 --> 00:20:42,000
We have to go through many, much unpleasantness.

287
00:20:42,000 --> 00:20:47,000
So eventually, you realize that life after life after life,

288
00:20:47,000 --> 00:20:50,000
it's not really all that it's cracked out to be.

289
00:20:50,000 --> 00:20:56,000
And I think if we looked at this life as just one in a string of countless lives,

290
00:20:56,000 --> 00:21:00,000
we'd come to see that, well, actually, if I have to do this all again,

291
00:21:00,000 --> 00:21:02,000
if the learning isn't cumulative,

292
00:21:02,000 --> 00:21:06,000
I'm not going to forget all these lessons I've learned and have to start over.

293
00:21:06,000 --> 00:21:10,000
It's not really that great of a thing to do again and again.

294
00:21:10,000 --> 00:21:13,000
It's quite stressful and unpleasant.

295
00:21:13,000 --> 00:21:15,000
But again, as long as we cling,

296
00:21:15,000 --> 00:21:18,000
as long as we desire it, as long as we're happy about being alive,

297
00:21:18,000 --> 00:21:21,000
there's no worry.

298
00:21:21,000 --> 00:21:23,000
We're still going to die.

299
00:21:23,000 --> 00:21:26,000
We're going to die again and again and again.

300
00:21:26,000 --> 00:21:33,000
It's only when we become free that we liberate ourselves from death as well.

301
00:21:33,000 --> 00:21:38,000
So that's the path of the deathless,

302
00:21:38,000 --> 00:21:46,000
path to the deathless is to give up our attachments and our desires.

303
00:21:46,000 --> 00:21:49,000
Our attachments to our own life,

304
00:21:49,000 --> 00:21:52,000
our attachment to the lives of others,

305
00:21:52,000 --> 00:21:56,000
our attachment to becoming in general to any kind of a rising.

306
00:21:56,000 --> 00:22:00,000
This is very much what we're into in meditation.

307
00:22:00,000 --> 00:22:02,000
So it goes in stages.

308
00:22:02,000 --> 00:22:08,000
In the beginning, we're only interested in the arising of real suffering,

309
00:22:08,000 --> 00:22:14,000
pain and unpleasantness and our own immorality and that kind of thing.

310
00:22:14,000 --> 00:22:16,000
Once we do away with that,

311
00:22:16,000 --> 00:22:19,000
then it's only a matter of refining it.

312
00:22:19,000 --> 00:22:24,000
So that eventually we come to see that suffering is really much more refined than that,

313
00:22:24,000 --> 00:22:26,000
and it's in everything.

314
00:22:26,000 --> 00:22:32,000
Eventually we give up more and more and we see more and more of our activities are based on greed,

315
00:22:32,000 --> 00:22:36,000
anger and delusion and arrogance and conceit and all that.

316
00:22:36,000 --> 00:22:40,000
So we give up more and more and more until eventually we're free.

317
00:22:40,000 --> 00:22:42,000
We give up everything.

318
00:22:42,000 --> 00:22:48,000
Let go of everything and then we can fly away and never have to come back.

319
00:22:48,000 --> 00:22:53,000
So that's the dhamma for tonight.

320
00:22:53,000 --> 00:22:56,000
It's the teaching and the deathless.

321
00:22:56,000 --> 00:22:58,000
One more verse.

322
00:22:58,000 --> 00:23:00,000
After this we have one more verse.

323
00:23:00,000 --> 00:23:01,000
That's very, very similar.

324
00:23:01,000 --> 00:23:03,000
And then we'll be finished with this chapter.

325
00:23:03,000 --> 00:23:05,000
But these are important stories.

326
00:23:05,000 --> 00:23:15,000
This is a very well-known story of how Kistel got to me found medicine for her son, dead son.

327
00:23:15,000 --> 00:23:21,000
Medicine to help her fix this problem of having a dead son.

328
00:23:21,000 --> 00:23:25,000
And she ended up fixing it by learning to let go.

329
00:23:25,000 --> 00:23:30,000
We're learning to be more flexible because her son went on his own journey.

330
00:23:30,000 --> 00:23:34,000
It's not like she's betraying his memory.

331
00:23:34,000 --> 00:23:42,000
She's just accepting reality that he was gone and her path led elsewhere.

332
00:23:42,000 --> 00:23:49,000
So that's the dhamma path of her tonight.

333
00:23:49,000 --> 00:23:52,000
Thank you all for tuning in.

334
00:23:52,000 --> 00:24:20,000
Good practicing.

