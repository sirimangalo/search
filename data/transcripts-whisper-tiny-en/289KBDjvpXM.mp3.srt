1
00:00:00,000 --> 00:00:13,800
Hello. Welcome back to Ask Monk. Today's question is on whether I think that there is more

2
00:00:13,800 --> 00:00:23,240
suffering in America, this is the question, where it is almost impossible not to find food

3
00:00:23,240 --> 00:00:32,800
and I guess because they people in America have developed their society, or more suffering

4
00:00:32,800 --> 00:00:44,320
in a tribal society. This is the question. So first of all, I'd like to sort of expand the

5
00:00:44,320 --> 00:00:52,440
question because I think the important point is whether not whether America now or America

6
00:00:52,440 --> 00:01:01,680
when it was owned by the Native American or First Nations people is better. The question

7
00:01:01,680 --> 00:01:11,200
is whether there's more suffering to be found in a society with technological advancements

8
00:01:11,200 --> 00:01:23,560
and politics and government and roads and cars and computers and iPods and so on. Or whether

9
00:01:23,560 --> 00:01:39,400
there is more suffering in a lifestyle that is simple, unrefined, unadvanced and

10
00:01:39,400 --> 00:01:49,800
unorganized on a level similar to a tribe. It's a difficult question, but why it's difficult

11
00:01:49,800 --> 00:01:56,400
is because I think there's merits to both sides. I want to be able to say that material advancement

12
00:01:56,400 --> 00:02:10,320
in some way heightens people's ability or interest in things like science, things like

13
00:02:10,320 --> 00:02:18,680
objective investigation and analysis and so on. The education side of things, I want to

14
00:02:18,680 --> 00:02:26,160
be able to say that that's a good thing into some degree. But I think given the choice,

15
00:02:26,160 --> 00:02:34,160
I would have to say that there's probably less suffering to be found in a less advanced

16
00:02:34,160 --> 00:02:42,320
society. And I think we have to advance this beyond the tribal societies of, say, North

17
00:02:42,320 --> 00:02:51,280
America in old times now. There are still some tribal societies, but not really to the extent

18
00:02:51,280 --> 00:02:57,120
that you would have found some time ago. Because I think this is a rather limited sort

19
00:02:57,120 --> 00:03:03,760
of way of looking at things. There are many societies or cultures or groups of individuals,

20
00:03:03,760 --> 00:03:09,600
groups of people living in the world who are living in limited technological advancement

21
00:03:09,600 --> 00:03:19,080
and so on. I think living in Thailand and living in Sri Lanka, I've been able to see some

22
00:03:19,080 --> 00:03:27,360
of these groups. Thailand, for example, is quite advanced in many ways. But in the villages,

23
00:03:27,360 --> 00:03:33,160
in the countryside, you'll still find people who are very technologically behind and living

24
00:03:33,160 --> 00:03:42,720
in fairly unorganized manners, in terms of political structure and so on. And you're in

25
00:03:42,720 --> 00:03:52,360
Sri Lanka even more so. You don't see the impact of modern society to such a great

26
00:03:52,360 --> 00:04:03,520
extent as you would in the West. And I think, beneficially, at least to some extent, that

27
00:04:03,520 --> 00:04:09,800
it has allowed people to be free from some of the suffering. So, stacking up the sufferings,

28
00:04:09,800 --> 00:04:16,120
I think the obvious suffering that was pointed out in the question is living in a tribal

29
00:04:16,120 --> 00:04:23,560
society or an uncivilized quote unquote society. It's going to be more difficult to find

30
00:04:23,560 --> 00:04:29,280
food. And I don't really think this is that big of a deal. In fact, I probably would

31
00:04:29,280 --> 00:04:36,400
care to wager that North America, it was a lot easier to find food before technology came

32
00:04:36,400 --> 00:04:42,440
into effect. And now the food quality has gone down and people have to work hard for it

33
00:04:42,440 --> 00:04:51,160
and so on. I would say, but anyway, that's maybe a bit speculative. But the real suffering,

34
00:04:51,160 --> 00:05:00,640
I think, that comes with being pointed here, pointed to here, is the lack of amenities,

35
00:05:00,640 --> 00:05:08,760
living in rough conditions, having to put up with insects, having to put up with leeches,

36
00:05:08,760 --> 00:05:13,400
having to put up with snakes and scorpions, having to put up with mosquitoes because

37
00:05:13,400 --> 00:05:20,080
you don't have screens on your windows or you don't have the technology to prevent these

38
00:05:20,080 --> 00:05:26,040
things, having to put up with limited medical facilities and so on. There are a lot

39
00:05:26,040 --> 00:05:32,960
of sufferings that come with living in a simple manner. And I think it's fair to point

40
00:05:32,960 --> 00:05:41,240
out that the monastic life is, by its nature, the Buddhist monastic life is of a sort

41
00:05:41,240 --> 00:05:47,960
of simple life. And as a result, there is a certain amount of suffering. This is why I think

42
00:05:47,960 --> 00:05:52,320
it was an interesting question and why I'd like to take some time to answer it. Because

43
00:05:52,320 --> 00:05:58,080
I have to personally go through some of the sufferings that the villagers here even have

44
00:05:58,080 --> 00:06:03,520
to go through. Perhaps even more so than they have to go through. I might be going through

45
00:06:03,520 --> 00:06:12,520
some of the sufferings that you might expect from a tribal society. Because for me food

46
00:06:12,520 --> 00:06:18,200
is limited, I only eat one meal a day. So you could say that somehow similar to the suffering

47
00:06:18,200 --> 00:06:22,800
that is brought about by having to hunt for your food, not knowing where you're going

48
00:06:22,800 --> 00:06:28,920
to get your next meal, not having a farm land or a grocery store or money that you can

49
00:06:28,920 --> 00:06:37,360
easily access these things. And having to put up with limited medical facilities. But

50
00:06:37,360 --> 00:06:46,760
I think the real key here is that, or the real good way to answer this question is to

51
00:06:46,760 --> 00:06:55,200
point out the fact that so many people undertake this lifestyle on purpose and voluntarily.

52
00:06:55,200 --> 00:06:58,400
And why would they do it if it was just going to lead to their suffering? And I think

53
00:06:58,400 --> 00:07:03,600
that's a question that a lot of people ask, why do people decide to live in caves and

54
00:07:03,600 --> 00:07:12,880
where robes, a single set of robes, et cetera, et cetera, eating only one meal a day, putting

55
00:07:12,880 --> 00:07:21,760
up with all of these difficulties. And I think it's because it's a tradeoff. If you give

56
00:07:21,760 --> 00:07:30,920
up, or if you want to be free from all of the stresses and all of the sufferings that

57
00:07:30,920 --> 00:07:39,160
are on the other side of the fence, then you can't expect to have all the amenities.

58
00:07:39,160 --> 00:07:43,840
I mean, they go hand in hand, the sufferings on the other side of a technologically advanced

59
00:07:43,840 --> 00:07:50,600
society are the stresses of having to work a job, you know, nine to five job, the stresses

60
00:07:50,600 --> 00:07:55,600
of having to think a lot of having to deal with people, crowds of people having to deal

61
00:07:55,600 --> 00:08:01,200
with traffic, having to deal with the complications that come with technology, like fixing

62
00:08:01,200 --> 00:08:10,280
your car and your house and all of the laws and having to deal with laws and lawyers

63
00:08:10,280 --> 00:08:16,640
and people suing you and so on, there's a huge burden of stress having to be worried

64
00:08:16,640 --> 00:08:24,800
about thieves, having to be worried about crooks, not crook but people cheating you, having

65
00:08:24,800 --> 00:08:31,880
to be worried about your boss, your co-workers, your clients and so on and a million other

66
00:08:31,880 --> 00:08:37,200
things. I mean, no matter what work you apply yourself to in an advanced society, there

67
00:08:37,200 --> 00:08:44,320
are more complications. And as a result, more stress and as a result, more suffering.

68
00:08:44,320 --> 00:08:53,040
So I think the tradeoff there is quite worth it in my mind because I'd like to, you

69
00:08:53,040 --> 00:08:57,280
know, the answer for me is quite clear because having put up with all these sufferings

70
00:08:57,280 --> 00:09:04,000
of having to have mosquitoes bite me and who knows what bite me and having to put up

71
00:09:04,000 --> 00:09:10,080
with leeches and the worries that I have to put up with worrying about stepping on a

72
00:09:10,080 --> 00:09:16,200
snake, worrying about that forefoot lizard down below me and the monkey scrapping on my

73
00:09:16,200 --> 00:09:22,800
head and so on. I really don't feel upset or affected by these things.

74
00:09:22,800 --> 00:09:28,480
They're physical, a physical reality and they're much more physical than they are mental.

75
00:09:28,480 --> 00:09:34,680
I feel so much more at peace here living in the jungle which, you know, might look peaceful

76
00:09:34,680 --> 00:09:41,280
but it's actually quite dangerous and it keeps you on your toes in many ways. Even during

77
00:09:41,280 --> 00:09:45,520
meditation, you have to do these little bugs that you can't even see them. They're

78
00:09:45,520 --> 00:09:53,280
worse than mosquitoes and they bite you. But it's not a stress for me, not in the same

79
00:09:53,280 --> 00:09:59,600
way as living in, say, the big cities of Colombo, Bangkok, Los Angeles. The stress is

80
00:09:59,600 --> 00:10:06,600
that's involved there. You know, it's an order of magnitude greater. So I'm not sure if

81
00:10:06,600 --> 00:10:10,640
this exactly answers the question but this is how I would prefer to approach it. I

82
00:10:10,640 --> 00:10:22,920
think there's far less stress to be had from a simple lifestyle because the stress is

83
00:10:22,920 --> 00:10:30,760
much more physical than they are mental and the mental stress is far less.

84
00:10:30,760 --> 00:10:38,200
Another thing about living in a simple society in a simple lifestyle is that it's much

85
00:10:38,200 --> 00:10:43,200
more in tune with our programming physically. I mean, we're still very much related

86
00:10:43,200 --> 00:10:51,360
to the monkeys. And so this kind of atmosphere jives with us, seeing the trees, why people

87
00:10:51,360 --> 00:10:55,440
go to nature and they feel peaceful of a sudden is because of how it's not because there's

88
00:10:55,440 --> 00:11:00,760
anything special there, it's because of what's not there. There's nothing jarring with

89
00:11:00,760 --> 00:11:06,520
your experience, no horns blasting, no bright lights, no beautiful pictures catching your

90
00:11:06,520 --> 00:11:14,600
attention, no ugly sights and so on. It's very peaceful, very calm, very natural, right?

91
00:11:14,600 --> 00:11:22,440
I mean, that's why we're here. And I think you'll find that in a traditional society.

92
00:11:22,440 --> 00:11:27,440
So I think, and I think this is borne out by the Buddhist teaching. It's borne out by

93
00:11:27,440 --> 00:11:33,560
the examples, obviously, of those people who have ordained its monks. There was a story

94
00:11:33,560 --> 00:11:37,040
in the typical in the Buddhist teaching that we always go back to. I think in the

95
00:11:37,040 --> 00:11:43,120
commentaries, actually, I'm not sure, well at least it's in the commentaries, about

96
00:11:43,120 --> 00:11:53,600
this king, relative of the king who became a monk, Mahakapina. And after he became a monk,

97
00:11:53,600 --> 00:11:58,440
he would sit under this tree, used to be a king, right? So after he became a monk, he would

98
00:11:58,440 --> 00:12:06,760
sit under a tree and go, oh, sukhan, oh, sukhan, oh, sukhan, which means, oh, what happiness,

99
00:12:06,760 --> 00:12:13,320
oh, what bliss, oh, what happiness. So you might say he was actually contemplating the

100
00:12:13,320 --> 00:12:17,080
happiness, he's looking at it and examining it in the much in the same way that we say

101
00:12:17,080 --> 00:12:24,200
to ourselves, happy, happy. But the monks heard him saying this and remarked on this.

102
00:12:24,200 --> 00:12:28,800
And they took it the wrong way. They took it to mean he was, for some reason, they took

103
00:12:28,800 --> 00:12:33,760
it that he must be thinking about his kingly happiness. He was remembering, oh, what

104
00:12:33,760 --> 00:12:38,440
happiness it was to be a king. And so they took him to the Buddha and the Buddha asked

105
00:12:38,440 --> 00:12:45,440
him, you know, what was it? He said, no, when I was a king, I had to worry about all

106
00:12:45,440 --> 00:12:50,440
of these stresses and concerns. My life was always in danger of assassination. I had

107
00:12:50,440 --> 00:12:57,000
all this treasure that I had to guard, all these ministers that I had to manage and to stop

108
00:12:57,000 --> 00:13:00,640
from cheating me and all the people who would come to me with their problems and all

109
00:13:00,640 --> 00:13:06,200
the laws I had to enforce and so on and so on. And I have none of that now. Now I have

110
00:13:06,200 --> 00:13:13,200
this tree, three robes, my bull, my tree. And this is the greatest happiness I've ever

111
00:13:13,200 --> 00:13:22,000
known in my life, he said. So there's a story to go with this question. I think one final

112
00:13:22,000 --> 00:13:28,640
note is that regardless of the answer to this question, whether there's more suffering

113
00:13:28,640 --> 00:13:35,280
in either one, I think the point is that whatever lifestyle you can undertake, not that

114
00:13:35,280 --> 00:13:41,840
has the least amount of suffering, but which has the least amount of unwholesomeness involved.

115
00:13:41,840 --> 00:13:47,280
That is the best lifestyle. So for those people who might be discouraged and by the fact

116
00:13:47,280 --> 00:13:52,000
that they have to live in a technologically advanced society as a Buddhist and they feel

117
00:13:52,000 --> 00:13:56,600
like they should be living in the forest, I don't mean to say that there's anything wrong

118
00:13:56,600 --> 00:14:00,480
with living in the city. I've lived in Los Angeles, I've gone on arms around in North

119
00:14:00,480 --> 00:14:09,480
Hollywood in places in Bangkok, in Colombo. I've lived in these places in fairly chaotic

120
00:14:09,480 --> 00:14:15,920
and stressful situations. But the point is to live your life in such a way that it's free

121
00:14:15,920 --> 00:14:21,120
from unwholesomeness. And this is where the question gets interesting because many tribal

122
00:14:21,120 --> 00:14:27,440
societies are involved in what Buddhists would call great unwholesomeness. Their way of lifestyle

123
00:14:27,440 --> 00:14:34,280
is hunting, as an example. And though they do it to serve, you could say it to survive,

124
00:14:34,280 --> 00:14:46,080
by nature, caught up with these states of cruelty, states of anger and delusion. And

125
00:14:46,080 --> 00:14:51,960
as a result, it keeps them bound to the wheel of life that they're going to have to take

126
00:14:51,960 --> 00:14:59,080
turns with the deer. This is what the First Nations people believe. They still believe

127
00:14:59,080 --> 00:15:04,200
that you take turns. I hunt the deer this life and in the next life I'm the deer

128
00:15:04,200 --> 00:15:09,120
and they hunt me and so on. There are these kinds of beliefs. And this is really in line

129
00:15:09,120 --> 00:15:13,320
with the Buddhist teaching. So, I mean, if you're fine with that, having to be hunted

130
00:15:13,320 --> 00:15:22,040
every other lifetime or a few lifetimes, then so be it. But this isn't the, from a Buddhist

131
00:15:22,040 --> 00:15:26,320
point of view, this isn't the way out of suffering. It's because there's a lot of suffering.

132
00:15:26,320 --> 00:15:30,560
I mean, tribes, of course, then there's tribal warfare and, you know, they're not free

133
00:15:30,560 --> 00:15:36,080
from all of this. And we're not free from it on the other side as living in technologically

134
00:15:36,080 --> 00:15:41,880
advanced societies. Living in Canada, I wasn't free from these things. I was hunting as

135
00:15:41,880 --> 00:15:47,320
well. I didn't need to, but I went hunting with my father. We engage in a lot of unholesumness

136
00:15:47,320 --> 00:15:54,200
in advanced societies. So, that's the most important. You can live in a city, you can live

137
00:15:54,200 --> 00:16:00,640
in nature. The reason why we choose to live in nature is, I think, overall, you know,

138
00:16:00,640 --> 00:16:05,600
the amount of peace that you have is the one side. But there's also a lot less unholesumness

139
00:16:05,600 --> 00:16:11,880
that you're free from having to deal with, you know, other people's unholesumness and

140
00:16:11,880 --> 00:16:17,440
having to see all these beautiful, attractive sites wanting to get this wanting to get

141
00:16:17,440 --> 00:16:24,080
that, being annoyed by people being upset by people. So, you're able to focus much

142
00:16:24,080 --> 00:16:30,400
clearer on the very core aspects of existence, you know, what really is reality without

143
00:16:30,400 --> 00:16:37,920
having to be, get caught up in unholesum minds states, stress and addiction and so on.

144
00:16:37,920 --> 00:16:42,560
And I think that's clear. I think being here in the forest is much better for my meditation

145
00:16:42,560 --> 00:16:49,560
practice than being in the city. But in the end, it's not the biggest concern. And I'm

146
00:16:49,560 --> 00:16:55,480
not afraid to live in the city if I have to. Because, for me, the most important is

147
00:16:55,480 --> 00:17:00,840
to continue the meditation practice and to learn more about my reactions and my interactions

148
00:17:00,840 --> 00:17:07,000
with the world around me. And sometimes that can be tested by suffering. Again, the Buddha's

149
00:17:07,000 --> 00:17:12,200
teaching is not to run away from suffering. It's to learn and to come to understand suffering.

150
00:17:12,200 --> 00:17:16,080
When you understand suffering, you'll understand what is really causing suffering. That

151
00:17:16,080 --> 00:17:20,960
it's not the objects. It's your relationship and your reactions to them. Your attachment

152
00:17:20,960 --> 00:17:25,680
to things being in a certain way and not being in another way. Your attachment to the

153
00:17:25,680 --> 00:17:29,720
objects of the sense and so on. When you can be free from that, it doesn't matter where

154
00:17:29,720 --> 00:17:35,560
you live, whether it be in a cave or in an apartment complex. So, there's the answer

155
00:17:35,560 --> 00:17:54,160
to your question, thanks, and have a good day.

