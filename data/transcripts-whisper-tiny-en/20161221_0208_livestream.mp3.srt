1
00:00:00,000 --> 00:00:02,000
You

2
00:00:30,000 --> 00:00:32,000
.

3
00:00:32,000 --> 00:00:34,000
..

4
00:00:34,000 --> 00:00:36,000
..

5
00:00:36,000 --> 00:00:38,000
..

6
00:00:38,000 --> 00:00:40,000
..

7
00:00:40,000 --> 00:00:44,000
..

8
00:00:44,000 --> 00:00:46,000
..

9
00:00:46,000 --> 00:00:48,000
..

10
00:00:48,000 --> 00:00:50,000
..

11
00:00:50,000 --> 00:00:52,000
..

12
00:00:52,000 --> 00:00:54,000
..

13
00:00:54,000 --> 00:00:56,000
..

14
00:00:56,000 --> 00:00:58,000
..

15
00:00:58,000 --> 00:01:01,000
...

16
00:01:01,000 --> 00:01:02,000
..

17
00:01:02,000 --> 00:01:04,000
..

18
00:01:04,000 --> 00:01:06,000
..

19
00:01:06,000 --> 00:01:08,000
..

20
00:01:08,000 --> 00:01:10,000
...

21
00:01:10,000 --> 00:01:14,000
..

22
00:01:14,000 --> 00:01:16,000
...

23
00:01:16,000 --> 00:01:18,420
..

24
00:01:18,420 --> 00:01:24,440
...

25
00:01:24,440 --> 00:01:25,440
..

26
00:01:25,440 --> 00:01:26,440
..

27
00:01:26,440 --> 00:01:27,440
...

28
00:01:27,440 --> 00:01:49,720
So good evening everyone, welcome to our evening demonstration.

29
00:01:49,720 --> 00:02:00,120
Thank you.

30
00:02:00,120 --> 00:02:13,160
Can

31
00:02:13,160 --> 00:02:35,660
Remember this Buddha taught Vinja, Charuna, Sampano, Soseto, Devamanu, Soseto, Devamanu,

32
00:02:35,660 --> 00:02:36,660
Soseto.

33
00:02:36,660 --> 00:03:02,960
And among the angels and the humans, something wrong with my voice,

34
00:03:02,960 --> 00:03:27,960
how can that be, how does that happen?

35
00:03:27,960 --> 00:03:35,960
How does it happen?

36
00:03:35,960 --> 00:03:46,960
That one's voice becomes high pitched as there's some,

37
00:03:46,960 --> 00:03:51,960
this internet thing is just not, not what it's got out to be.

38
00:03:51,960 --> 00:04:16,960
I can go back to teaching and analog testing testing.

39
00:04:16,960 --> 00:04:18,960
Maybe it was second life.

40
00:04:18,960 --> 00:04:27,960
It's a better note.

41
00:04:27,960 --> 00:04:32,960
Maybe it's been a problem with second life.

42
00:04:32,960 --> 00:04:33,960
All right.

43
00:04:33,960 --> 00:04:39,960
We start again, apologies to our local meditators who are so patient.

44
00:04:39,960 --> 00:04:44,960
I thought this unnecessary technology.

45
00:04:44,960 --> 00:04:54,960
We have a virtual deer park full of listeners.

46
00:04:54,960 --> 00:05:00,960
So the Buddha said, Vidja, Charuna, Sampano, Soseto, Devamanu, Soseto.

47
00:05:00,960 --> 00:05:07,960
And among all angels and humans,

48
00:05:07,960 --> 00:05:17,960
the highest, the highest, the most exalted,

49
00:05:17,960 --> 00:05:20,960
is the one who is endowed with Vidja and Charuna.

50
00:05:20,960 --> 00:05:33,960
Vidja meaning knowledge and Charuna meaning conduct.

51
00:05:33,960 --> 00:05:39,960
Vidja, Charuna, these two go together.

52
00:05:39,960 --> 00:05:43,960
Vidja, Charuna, Sampano is an epithet of the Buddha.

53
00:05:43,960 --> 00:05:56,960
It's something that is understood to be one of the core virtues of the Buddha.

54
00:05:56,960 --> 00:06:21,960
That he indeed was endowed with both knowledge and practice, knowledge and conduct.

55
00:06:21,960 --> 00:06:36,960
It comes back to the core Buddhist concept or dogma or doctrine that it's wisdom that was set one free.

56
00:06:36,960 --> 00:06:41,960
It's wisdom that leads to purification.

57
00:06:41,960 --> 00:06:50,960
Wisdom is the most holy, the highest holiness is wisdom, the greatest goodness is wisdom.

58
00:06:50,960 --> 00:06:55,960
Vidja, Dukama, J. Deepanyaya, Parisu Jiti.

59
00:06:55,960 --> 00:06:59,960
It is through effort that one over comes suffering.

60
00:06:59,960 --> 00:07:06,960
It is through wisdom that one is purified.

61
00:07:06,960 --> 00:07:20,560
And so again and again we have to impress upon the followers of the Buddha's

62
00:07:20,560 --> 00:07:30,560
teaching that it's not about creating a specific state or becoming something.

63
00:07:30,560 --> 00:07:36,560
Buddhism is all about wisdom, knowledge, Vidja.

64
00:07:36,560 --> 00:07:38,560
Vidja is an interesting word.

65
00:07:38,560 --> 00:07:41,560
Vidja comes from with the root.

66
00:07:41,560 --> 00:07:51,560
Vidja is where the word Vidja now comes from, so it means to actually experience it.

67
00:07:51,560 --> 00:07:55,560
There's a sense of it having to do with experiential knowledge.

68
00:07:55,560 --> 00:07:58,560
Because there's different kinds of wisdom right there.

69
00:07:58,560 --> 00:08:01,560
One can know so much.

70
00:08:01,560 --> 00:08:06,560
And there are many people who call themselves Buddha who know so much.

71
00:08:06,560 --> 00:08:14,560
They have knowledge of all the Buddha's teaching.

72
00:08:14,560 --> 00:08:20,560
But they don't have karan.

73
00:08:20,560 --> 00:08:26,560
A person might even have external behavior that's appropriate.

74
00:08:26,560 --> 00:08:31,560
They might keep the five precepts.

75
00:08:31,560 --> 00:08:40,560
Stigiously keeping the ethical precepts of body and speech.

76
00:08:40,560 --> 00:08:43,560
But karan isn't just that, isn't it?

77
00:08:43,560 --> 00:08:49,560
Karan is very much to do with our state of mind.

78
00:08:49,560 --> 00:08:56,560
It even has to do with our thoughts, karan.

79
00:08:56,560 --> 00:09:02,560
All of this is informed by wisdom.

80
00:09:02,560 --> 00:09:14,560
It's not informed by knowledge or it's not informed by the intentional control of the mind,

81
00:09:14,560 --> 00:09:24,560
the control of the body and control of the speech to repress and to prevent the arising of our inner,

82
00:09:24,560 --> 00:09:27,560
the most tendencies and desires.

83
00:09:27,560 --> 00:09:29,560
It's not about control.

84
00:09:29,560 --> 00:09:32,560
It's not about stopping the problems from arising.

85
00:09:32,560 --> 00:09:36,560
It's about understanding them.

86
00:09:36,560 --> 00:09:45,560
Understanding them to the point where they no longer have any hold over you.

87
00:09:45,560 --> 00:09:52,560
The source of suffering is not our experience.

88
00:09:52,560 --> 00:09:57,560
The source of all of our suffering is certainly not external phenomena.

89
00:09:57,560 --> 00:10:03,560
It's not even our actions or our speech or our thoughts.

90
00:10:03,560 --> 00:10:09,560
It's our reactions to all these things.

91
00:10:09,560 --> 00:10:14,560
Our reactions based on what, based on ignorance.

92
00:10:14,560 --> 00:10:22,560
Our weenja pachea sankara, based on ignorance that we do good and evil.

93
00:10:22,560 --> 00:10:25,560
Not just evil, but we do good as well.

94
00:10:25,560 --> 00:10:27,560
All based on ignorance.

95
00:10:27,560 --> 00:10:29,560
Why? Because we want some results.

96
00:10:29,560 --> 00:10:33,560
We want to be born in heaven or we want to be rich.

97
00:10:33,560 --> 00:10:36,560
We want to be this or that.

98
00:10:36,560 --> 00:10:44,560
We want this or we want that.

99
00:10:44,560 --> 00:10:46,560
And so the core in Buddhism is just wisdom.

100
00:10:46,560 --> 00:10:48,560
It's just gaining and understanding.

101
00:10:48,560 --> 00:10:51,560
But the hint here is with chatternut,

102
00:10:51,560 --> 00:10:55,560
that the understanding that we try to gain is nothing to do with intellectual knowledge.

103
00:10:55,560 --> 00:11:03,560
It has to do with true and lasting change and the eradication of ignorance.

104
00:11:03,560 --> 00:11:07,560
We jaya teweewa sesa wiragany rhoda.

105
00:11:07,560 --> 00:11:12,560
With the complete and utter feeling away and cessation of ignorance,

106
00:11:12,560 --> 00:11:15,560
all suffering ceases.

107
00:11:15,560 --> 00:11:19,560
We may tasakewa lasandukkandasany rhoda.

108
00:11:19,560 --> 00:11:26,560
This is the cessation of this entire mass of suffering.

109
00:11:26,560 --> 00:11:30,560
All through wisdom.

110
00:11:30,560 --> 00:11:34,560
It's quite unintuitive, I think.

111
00:11:34,560 --> 00:11:39,560
Intuitively, we want to control.

112
00:11:39,560 --> 00:11:41,560
We want to fix.

113
00:11:41,560 --> 00:11:44,560
We want a solution to our problems.

114
00:11:44,560 --> 00:11:48,560
But the solution to our problems is very quite much different.

115
00:11:48,560 --> 00:11:51,560
It's not about seeking a solution.

116
00:11:51,560 --> 00:11:57,560
The solution is simply to understand.

117
00:11:57,560 --> 00:12:00,560
To see them in a new way.

118
00:12:00,560 --> 00:12:01,560
To see them as they really are.

119
00:12:01,560 --> 00:12:03,560
Because a problem can't exist.

120
00:12:03,560 --> 00:12:04,560
There's not such thing as a problem.

121
00:12:04,560 --> 00:12:06,560
They don't exist.

122
00:12:06,560 --> 00:12:12,560
There's no such thing in ultimate reality as a problem.

123
00:12:12,560 --> 00:12:13,560
Problems are conceptual.

124
00:12:13,560 --> 00:12:15,560
It has to do with our reactions.

125
00:12:15,560 --> 00:12:20,560
Our judgements are identification with what does exist.

126
00:12:20,560 --> 00:12:24,560
Our clinging to that which exists.

127
00:12:24,560 --> 00:12:27,560
The Buddha said, the only thing you have to know.

128
00:12:27,560 --> 00:12:30,560
The only thing you really need to understand about Buddhism

129
00:12:30,560 --> 00:12:32,560
even before you come to meditate.

130
00:12:32,560 --> 00:12:35,560
This means understanding intellectually.

131
00:12:35,560 --> 00:12:38,560
I said, sub-bait dhamma nalangamini wasi.

132
00:12:38,560 --> 00:12:40,560
No dhamma is worth clinging to.

133
00:12:40,560 --> 00:12:43,560
All dhammas are not worth clinging to.

134
00:12:43,560 --> 00:12:48,560
Nothing.

135
00:12:48,560 --> 00:12:52,560
Once you understand that, that's what you understand going in

136
00:12:52,560 --> 00:12:55,560
terms of this is what I'm getting myself involved in.

137
00:12:55,560 --> 00:12:57,560
This is what Buddhism is all about.

138
00:12:57,560 --> 00:12:59,560
So I understand it intellectually.

139
00:12:59,560 --> 00:13:02,560
It helps focus your mind and direct your mind and keep you on path.

140
00:13:02,560 --> 00:13:04,560
What am I doing here?

141
00:13:04,560 --> 00:13:06,560
Am I trying to control the mind?

142
00:13:06,560 --> 00:13:09,560
I was thinking this word meditation is a little bit deceptive.

143
00:13:09,560 --> 00:13:13,560
I guess it's taken on so many improper connotations.

144
00:13:13,560 --> 00:13:17,560
Meditation is this thing you do with your eyes closed and your legs crossed.

145
00:13:17,560 --> 00:13:19,560
But that's not meditation.

146
00:13:19,560 --> 00:13:28,560
The practice of meditation is something that has to be the default filter

147
00:13:28,560 --> 00:13:35,560
with which we view experience, which we encounter and interact with experience.

148
00:13:35,560 --> 00:13:36,560
Meditation.

149
00:13:36,560 --> 00:13:37,560
We have to meditate.

150
00:13:37,560 --> 00:13:43,560
We have to be meditative in our existence.

151
00:13:43,560 --> 00:13:47,560
And so this is where we get the idea of mindfulness, of sati.

152
00:13:47,560 --> 00:13:51,560
That you apply mindfulness, you apply sati.

153
00:13:51,560 --> 00:13:53,560
And you start to see.

154
00:13:53,560 --> 00:13:57,560
So how do you come to know how do you get rid of ignorance through knowledge?

155
00:13:57,560 --> 00:14:00,560
How do you cultivate knowledge by seeing?

156
00:14:00,560 --> 00:14:04,560
How do you see by looking?

157
00:14:04,560 --> 00:14:07,560
How do you look while you focus?

158
00:14:07,560 --> 00:14:10,560
You straighten out your mind.

159
00:14:10,560 --> 00:14:18,560
You grasp properly grasp the object as it is.

160
00:14:18,560 --> 00:14:20,560
Don't let it slip away.

161
00:14:20,560 --> 00:14:25,560
Don't have any sort of superficial awareness of experience.

162
00:14:25,560 --> 00:14:31,560
You actually have to impress upon your mind this is this.

163
00:14:31,560 --> 00:14:41,560
It is just seeing.

164
00:14:41,560 --> 00:14:47,560
Hearing is hearing.

165
00:14:47,560 --> 00:14:48,560
So wisdom.

166
00:14:48,560 --> 00:14:51,560
Wisdom isn't something esoteric or hard to understand.

167
00:14:51,560 --> 00:14:52,560
Wisdom is really just this.

168
00:14:52,560 --> 00:14:53,560
That's another thing.

169
00:14:53,560 --> 00:14:56,560
Wisdom becomes terribly misunderstood.

170
00:14:56,560 --> 00:14:58,560
And so you have people who study all of them,

171
00:14:58,560 --> 00:15:04,560
but it's teachings and things that they're somehow wise.

172
00:15:04,560 --> 00:15:06,560
I used to give talks when I was in Thailand.

173
00:15:06,560 --> 00:15:08,560
I learned to give talks in Thai.

174
00:15:08,560 --> 00:15:10,560
I really practiced Thai.

175
00:15:10,560 --> 00:15:13,560
And when I learned to recite the Bhatimoka,

176
00:15:13,560 --> 00:15:17,560
I had Jantongi called me a parrot.

177
00:15:17,560 --> 00:15:21,560
I said it's just like a parrot.

178
00:15:21,560 --> 00:15:24,560
Because I don't speak Thai and I'm not Thai and so on.

179
00:15:24,560 --> 00:15:29,560
So I was like, look at this, you can train a foreigner.

180
00:15:29,560 --> 00:15:31,560
I always thought, yeah.

181
00:15:31,560 --> 00:15:38,560
Someone can speak, someone can learn so much.

182
00:15:38,560 --> 00:15:39,560
I mean learning how to teach.

183
00:15:39,560 --> 00:15:41,560
How to teach meditation.

184
00:15:41,560 --> 00:15:42,560
How to teach Buddhism.

185
00:15:42,560 --> 00:15:43,560
How to give a talk.

186
00:15:43,560 --> 00:15:45,560
It's just a skill.

187
00:15:45,560 --> 00:15:46,560
It's like being a parrot.

188
00:15:46,560 --> 00:15:49,560
So tonight I parrot it back a whole bunch of the Buddhist teaching.

189
00:15:49,560 --> 00:15:51,560
You might think, wow, he's wise.

190
00:15:51,560 --> 00:15:53,560
That's not wisdom.

191
00:15:53,560 --> 00:15:54,560
It's good memory.

192
00:15:54,560 --> 00:15:59,560
I've got a very good memory for what it's worth.

193
00:15:59,560 --> 00:16:01,560
But it's not wisdom.

194
00:16:01,560 --> 00:16:05,560
None of this is really valuable.

195
00:16:05,560 --> 00:16:08,560
In an ultimate sense, not without wisdom.

196
00:16:08,560 --> 00:16:14,560
Without wisdom intelligence can create much evil.

197
00:16:14,560 --> 00:16:16,560
Wisdom is something quite simple.

198
00:16:16,560 --> 00:16:18,560
It has all to do with Jadarna.

199
00:16:18,560 --> 00:16:21,560
And it's not Jadarna that other people see this is the problem.

200
00:16:21,560 --> 00:16:23,560
As you look at someone in their behavior,

201
00:16:23,560 --> 00:16:26,560
it might be quite good.

202
00:16:26,560 --> 00:16:29,560
And they might see, wow, that person is very calm and enlightened.

203
00:16:29,560 --> 00:16:33,560
They speak well, they act well.

204
00:16:33,560 --> 00:16:38,560
But it's possible to fake it.

205
00:16:38,560 --> 00:16:43,560
There are many unenlightened teachers who are very well versed in

206
00:16:43,560 --> 00:16:47,560
putting on a good show.

207
00:16:47,560 --> 00:16:54,560
But no Jadarna is how your mind and your body line up.

208
00:16:54,560 --> 00:16:57,560
If you want to know, you can only really know for yourself,

209
00:16:57,560 --> 00:16:59,560
for the most part.

210
00:16:59,560 --> 00:17:01,560
You want to know that you really have wisdom.

211
00:17:01,560 --> 00:17:05,560
You have to see how it plays out in your body and your speech

212
00:17:05,560 --> 00:17:09,560
and your thoughts.

213
00:17:09,560 --> 00:17:11,560
Wisdom is something that will change the way you think,

214
00:17:11,560 --> 00:17:13,560
the way you act, the way you speak.

215
00:17:13,560 --> 00:17:16,560
It will change your whole outlook on life.

216
00:17:16,560 --> 00:17:20,560
You will see the changes less greed, less anger,

217
00:17:20,560 --> 00:17:24,560
less delusion, less inclined to do unhold some things,

218
00:17:24,560 --> 00:17:29,560
more inclined to say or do the right thing,

219
00:17:29,560 --> 00:17:32,560
more ability and capability.

220
00:17:32,560 --> 00:17:33,560
It's such a power.

221
00:17:33,560 --> 00:17:36,560
Wisdom is this great power that changes our Jadarna,

222
00:17:36,560 --> 00:17:41,560
changes our behavior, changes our whole personality.

223
00:17:41,560 --> 00:17:46,560
Best be full of wisdom.

224
00:17:46,560 --> 00:17:49,560
Best be full of wisdom.

225
00:17:49,560 --> 00:17:51,560
Best be full of wisdom.

226
00:17:51,560 --> 00:17:54,560
There's no lust can set on fire.

227
00:17:54,560 --> 00:17:57,560
Never a one with wisdom filled could be a slave

228
00:17:57,560 --> 00:17:59,560
unto a desire.

229
00:17:59,560 --> 00:18:01,560
It's a Jadika verse.

230
00:18:01,560 --> 00:18:04,560
One of those well-translated,

231
00:18:04,560 --> 00:18:10,560
rhyming English couplet.

232
00:18:10,560 --> 00:18:12,560
So some food for thought tonight,

233
00:18:12,560 --> 00:18:14,560
a little bit of thought on wisdom.

234
00:18:14,560 --> 00:18:16,560
Get it clear in our minds that this is what we're aiming for.

235
00:18:16,560 --> 00:18:19,560
We're not trying to fix things or change things.

236
00:18:19,560 --> 00:18:21,560
Not directly.

237
00:18:21,560 --> 00:18:25,560
The fix and the change they come from understanding.

238
00:18:25,560 --> 00:18:30,560
Understanding changes us, changes who we are.

239
00:18:30,560 --> 00:18:31,560
So there you go.

240
00:18:31,560 --> 00:18:33,560
There's the dhamma for tonight.

241
00:18:33,560 --> 00:18:35,560
Successful, even with technical difficulties.

242
00:18:35,560 --> 00:18:37,560
Thank you all for tuning in

243
00:18:37,560 --> 00:18:40,560
and to my meditators for being patient.

244
00:19:07,560 --> 00:19:16,560
You didn't mention insights.

245
00:19:16,560 --> 00:19:22,560
How do they relate to wisdom?

246
00:19:22,560 --> 00:19:25,560
The insights are some, to some extent,

247
00:19:25,560 --> 00:19:30,560
more about the change that comes.

248
00:19:30,560 --> 00:19:33,560
They are wisdom.

249
00:19:33,560 --> 00:19:34,560
They are the stages there,

250
00:19:34,560 --> 00:19:36,560
and it's an in-depth understanding of wisdom.

251
00:19:36,560 --> 00:19:39,560
But they have more to do with how you change.

252
00:19:39,560 --> 00:19:42,560
So they're a pattern of wisdom.

253
00:19:42,560 --> 00:19:44,560
Pattern of understanding.

254
00:19:44,560 --> 00:19:47,560
Or a progress of what I was describing.

255
00:19:47,560 --> 00:19:49,560
This seeing that nothing was worth clinging to.

256
00:19:49,560 --> 00:19:53,560
Well, there are kind of stages to that.

257
00:19:53,560 --> 00:19:54,560
You know, it doesn't just,

258
00:19:54,560 --> 00:19:56,560
it's not just immediately you get an epiphany.

259
00:19:56,560 --> 00:19:57,560
I think it was clinging.

260
00:19:57,560 --> 00:19:58,560
That comes at the end.

261
00:19:58,560 --> 00:20:00,560
But leading up to that epiphany.

262
00:20:00,560 --> 00:20:05,560
There's quite a bit of prep work as you start to sort things out.

263
00:20:05,560 --> 00:20:10,560
And so there's a general pattern for how your thoughts change.

264
00:20:10,560 --> 00:20:12,560
The way you look at things change.

265
00:20:12,560 --> 00:20:15,560
Based on starting to see things more clearly.

266
00:20:15,560 --> 00:20:16,560
The practice is quite simple.

267
00:20:16,560 --> 00:20:17,560
It's just looking and seeing.

268
00:20:17,560 --> 00:20:22,560
But the stages of insight are much more useful for a teacher to

269
00:20:22,560 --> 00:20:26,560
judge and to assess where the meditator is.

270
00:20:26,560 --> 00:20:29,560
It's very dangerous for meditators to get caught up in them

271
00:20:29,560 --> 00:20:31,560
because then they miss the point.

272
00:20:31,560 --> 00:20:34,560
And the point is just to see things clearly.

273
00:20:34,560 --> 00:20:40,560
I think it's masculine.

274
00:20:40,560 --> 00:20:43,560
It says, never a man with wisdom filled could be a slave

275
00:20:43,560 --> 00:20:44,560
unto desire.

276
00:20:44,560 --> 00:20:48,560
Unto desire.

277
00:20:48,560 --> 00:20:49,560
You can Google it.

278
00:20:49,560 --> 00:20:50,560
Look it up.

279
00:20:50,560 --> 00:20:51,560
The Jataka's are all online.

280
00:20:51,560 --> 00:20:55,560
I recommend reading all 547 of them.

281
00:20:55,560 --> 00:20:58,560
But I've got them searchable on my computer.

282
00:20:58,560 --> 00:21:03,560
So I just run a search if I need to find something.

283
00:21:03,560 --> 00:21:09,560
But there's a lot of rhymes like that.

284
00:21:09,560 --> 00:21:11,560
Best be full of wisdom.

285
00:21:11,560 --> 00:21:13,560
This no lust can set on fire.

286
00:21:13,560 --> 00:21:22,560
Never a man with wisdom filled could be a slave unto desire.

287
00:21:22,560 --> 00:21:31,560
I have an online question.

288
00:21:31,560 --> 00:21:33,560
I tend to meditate right when I get up.

289
00:21:33,560 --> 00:21:36,560
But we'll meditate at different times before sleep.

290
00:21:36,560 --> 00:21:38,560
Is this okay?

291
00:21:38,560 --> 00:21:41,560
Again, meditation has to be ongoing.

292
00:21:41,560 --> 00:21:45,560
It's very much a part of your life.

293
00:21:45,560 --> 00:21:48,560
So don't fret too much.

294
00:21:48,560 --> 00:21:52,560
I like to remind people this is kind of like war.

295
00:21:52,560 --> 00:21:55,560
You're kind of like in a war zone.

296
00:21:55,560 --> 00:21:57,560
It's an important.

297
00:21:57,560 --> 00:22:00,560
If you're here in a meditation center, it's not quite like that.

298
00:22:00,560 --> 00:22:03,560
It's much more direct.

299
00:22:03,560 --> 00:22:06,560
But out there, it's like you have to think like a guerrilla warrior.

300
00:22:06,560 --> 00:22:11,560
It's all about getting in the shots when you can.

301
00:22:11,560 --> 00:22:13,560
Be flexible.

302
00:22:13,560 --> 00:22:14,560
Yeah, for sure.

303
00:22:14,560 --> 00:22:15,560
Different times that's fine.

304
00:22:15,560 --> 00:22:19,560
Don't be too concerned with a strict schedule.

305
00:22:19,560 --> 00:22:21,560
If you can, it's great.

306
00:22:21,560 --> 00:22:23,560
But don't be upset when you can't.

307
00:22:23,560 --> 00:22:30,560
And be flexible.

308
00:22:30,560 --> 00:22:34,560
Be clever.

309
00:22:34,560 --> 00:22:38,560
And it's much more about you than it is about formal.

310
00:22:38,560 --> 00:22:42,560
I did this much meditation at this time and so on.

311
00:22:42,560 --> 00:22:46,560
Of course, sticking to a scandal is good if you can.

312
00:22:46,560 --> 00:22:49,560
Because it gives you this routine.

313
00:22:49,560 --> 00:22:58,560
The actual ritual is useful to keep your attention.

314
00:22:58,560 --> 00:23:01,560
Can't help getting caught up with progress though.

315
00:23:01,560 --> 00:23:03,560
So one should just note that, right?

316
00:23:03,560 --> 00:23:04,560
Yes.

317
00:23:04,560 --> 00:23:07,560
If you're worried or doubt, that's all a part of the practice.

318
00:23:07,560 --> 00:23:08,560
It's a part of the challenge.

319
00:23:08,560 --> 00:23:12,560
If you're worried or doubt, those are part of the problem.

320
00:23:12,560 --> 00:23:16,560
Or if you want to progress, you want this, want that.

321
00:23:16,560 --> 00:23:19,560
It's usually ego based.

322
00:23:19,560 --> 00:23:40,560
Just note it.

323
00:23:40,560 --> 00:23:43,560
So these talks are also going to be on YouTube.

324
00:23:43,560 --> 00:23:45,560
Some of you are watching there, I'm sure.

325
00:23:45,560 --> 00:23:48,560
And we're also audio live.

326
00:23:48,560 --> 00:23:54,560
Now, someone, it's one of our people here, sent me a message or email or something.

327
00:23:54,560 --> 00:23:56,560
I've just been, I've got an exam tomorrow.

328
00:23:56,560 --> 00:24:00,560
So I've been a little bit preoccupied.

329
00:24:00,560 --> 00:24:03,560
She said she tried to download Second Life.

330
00:24:03,560 --> 00:24:05,560
She found it on the internet.

331
00:24:05,560 --> 00:24:09,560
But she got a problem and it just wouldn't load the Buddha Center.

332
00:24:09,560 --> 00:24:13,560
And she tried it both on her Android and on her iPad.

333
00:24:13,560 --> 00:24:18,560
I think, oh no.

334
00:24:18,560 --> 00:24:23,560
I don't think I think we're missing something here.

335
00:24:23,560 --> 00:24:30,560
So if you're trying to wonder how to get in Second Life, you really need a desktop computer or a strong laptop.

336
00:24:30,560 --> 00:24:33,560
It won't work on Android or iPad.

337
00:24:33,560 --> 00:24:34,560
It just doesn't.

338
00:24:34,560 --> 00:24:35,560
There's no software.

339
00:24:35,560 --> 00:24:41,560
You need special software that you download for PC, Mac, Linux.

340
00:24:41,560 --> 00:24:44,560
I think Mac works, doesn't it?

341
00:24:44,560 --> 00:24:47,560
And yeah, it's a little bit involved.

342
00:24:47,560 --> 00:24:52,560
So hopefully we'll have, I was thinking of at least probably what I should have done before anyway.

343
00:24:52,560 --> 00:25:00,560
I will soon, January when I come back, set up the video camera and record actual.

344
00:25:00,560 --> 00:25:01,560
And so don't have to screencast.

345
00:25:01,560 --> 00:25:04,560
Don't have to do this screen record.

346
00:25:04,560 --> 00:25:08,560
We'll record a video, a proper video, and then post that on YouTube.

347
00:25:08,560 --> 00:25:11,560
The problem is I got this big mic in front of my face now.

348
00:25:11,560 --> 00:25:13,560
It's such a nice microphone, but you can't.

349
00:25:13,560 --> 00:25:15,560
It doesn't work well for videos.

350
00:25:23,560 --> 00:25:27,560
Anyway, that's all for the questions, I guess.

351
00:25:27,560 --> 00:25:30,560
If so, then wish you all a good night.

352
00:25:30,560 --> 00:25:39,560
Yeah, only 20 people, we can only have 20 people on here at once.

353
00:25:39,560 --> 00:25:44,560
Let those seats, if you look over to my right, your left.

354
00:25:44,560 --> 00:25:47,560
There's a bunch of seats off on the gray part.

355
00:25:47,560 --> 00:25:50,560
That's actually in a different sim.

356
00:25:50,560 --> 00:25:51,560
So that's for overflow.

357
00:25:51,560 --> 00:25:56,560
People can go and sit there, and then they can listen in on the audio stream.

358
00:25:56,560 --> 00:26:01,560
I've done that before, but I don't think, if so, if any of you ever come late, then it's full.

359
00:26:01,560 --> 00:26:09,560
Go sit over there and turn on the audio stream or ask one of us where the audio stream is.

360
00:26:13,560 --> 00:26:16,560
I don't know if any of you have ever been caught out to have ever been really full.

361
00:26:16,560 --> 00:26:30,560
It just won't let you enter into the, once there's 20 people on the sim, you can't.

362
00:26:30,560 --> 00:26:32,560
Okay, I'm gonna quit there.

363
00:26:32,560 --> 00:26:54,560
Thank you, I'll have a good night.

