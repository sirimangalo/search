1
00:00:00,000 --> 00:00:28,920
Okay welcome everyone. Thank you all for coming out to our first ever Winnipeg

2
00:00:28,920 --> 00:00:34,080
meditation flash mob at the Forks.

3
00:00:41,200 --> 00:00:56,640
Yeah we've come together today to engage in something that might seem a

4
00:00:56,640 --> 00:01:07,080
little bit radical I think a lot of the the Buddhists and Buddhist meditators

5
00:01:07,080 --> 00:01:09,920
and meditators in the Winnipeg area when they heard about this were a little

6
00:01:09,920 --> 00:01:17,040
bit hesitant to get involved thinking that it might be something radical and

7
00:01:17,040 --> 00:01:33,760
even outside of or inappropriate but surprisingly enough meditation flash mobs

8
00:01:33,760 --> 00:01:40,160
are going on around the world they're being conducted in apparently over 250

9
00:01:40,160 --> 00:01:45,000
cities around the world and they've got a website where they're coordinating

10
00:01:45,000 --> 00:01:50,200
all these efforts so in fact in Winnipeg we're kind of behind the game so this

11
00:01:50,200 --> 00:01:57,200
is that's catching up and showing our support for this movement actually

12
00:01:57,200 --> 00:02:04,960
it has become a movement of course flash mobs are something familiar to most

13
00:02:04,960 --> 00:02:09,960
people who don't live in Buddhist monasteries or forests or

14
00:02:09,960 --> 00:02:17,000
uncaves or so on so most of you are aware what a flash mob is but the idea of a

15
00:02:17,000 --> 00:02:22,360
meditation flash mob might seem a little bit odd and actually I think has

16
00:02:22,360 --> 00:02:26,400
got some people a little bit hesitant about this so we'll have to use this as

17
00:02:26,400 --> 00:02:33,040
an example of how how much good can come from it I think we've all agreed

18
00:02:33,040 --> 00:02:40,400
that even just this first attempt at a flash mob has been quite successful but

19
00:02:40,400 --> 00:02:44,800
the question is successful how the question is why why a meditation flash

20
00:02:44,800 --> 00:02:51,520
mod and so I'd like to talk a little bit about why we're doing what we're

21
00:02:51,520 --> 00:02:59,560
doing the first reason and the real reason for deciding to do a meditation

22
00:02:59,560 --> 00:03:06,160
flash mob was for of course the effect that it would have on the people who

23
00:03:06,160 --> 00:03:16,920
witnessed it the idea behind a flash mob is to inspire to provide a performance

24
00:03:16,920 --> 00:03:20,800
in a sense something that allows people to see something out of the ordinary and

25
00:03:20,800 --> 00:03:33,240
to be inspired to be encouraged and to provide joy and happiness to the

26
00:03:33,240 --> 00:03:38,200
people who see it specifically of course what we're trying to do here is

27
00:03:38,200 --> 00:03:44,400
encourage people in the practice of meditation this morning we went into a

28
00:03:44,400 --> 00:03:51,760
store in here in Winnipeg and we we got there and it looked like the store was

29
00:03:51,760 --> 00:03:54,320
closed and then this guy came to the door just as we were getting to it and

30
00:03:54,320 --> 00:03:58,160
unlocked the door and let us in and while we were conducting our business this

31
00:03:58,160 --> 00:04:01,080
woman came up and asked to talk to the manager asked to them what the manager's

32
00:04:01,080 --> 00:04:04,920
name was and the guy said oh he's not here can I help you and she just tore into

33
00:04:04,920 --> 00:04:11,360
him saying you were you were away for an hour and he said what was away for

34
00:04:11,360 --> 00:04:14,520
half an hour and they got in this argument and he said I'm allowed to take a

35
00:04:14,520 --> 00:04:17,080
lunch break and she's I want to talk to your manager what you're not all

36
00:04:17,080 --> 00:04:20,760
you're it's not appropriate for you it was a huge deal for this woman that this

37
00:04:20,760 --> 00:04:25,360
guy had been away for even an hour and and to which he denied and said he'd

38
00:04:25,360 --> 00:04:30,280
been gone for half an hour and I thought well that's just a perfect example of

39
00:04:30,280 --> 00:04:38,320
what we're trying to address here that on a Saturday morning people should

40
00:04:38,320 --> 00:04:48,360
be so stressed that they can't wait in a parking lot for a store clerk to

41
00:04:48,360 --> 00:04:53,320
take his lunch break so we're trying to if you think about the audience the

42
00:04:53,320 --> 00:04:58,560
people around us of course they all seem fairly normal besides the guys with

43
00:04:58,560 --> 00:05:03,960
the punk haircuts and so on but everyone looks like they're living ordinary

44
00:05:03,960 --> 00:05:09,080
lives and of course if we think a little bit deeper than that we all know

45
00:05:09,080 --> 00:05:14,040
that that's not the case if you look at statistics pretty much everyone has

46
00:05:14,040 --> 00:05:18,960
their their story to tell and of course everyone in this group we all have our

47
00:05:18,960 --> 00:05:23,080
stories to tell things that we're keeping inside and that we have to deal with

48
00:05:23,080 --> 00:05:31,040
on a regular basis our difficulties but at the very least all of us have this

49
00:05:31,040 --> 00:05:34,080
practice of meditation now these people that were addressing they don't have

50
00:05:34,080 --> 00:05:44,600
this and so providing them just this vision this image this idea that there

51
00:05:44,600 --> 00:05:50,960
might be a aspect of life that they're missing a part of life that they they

52
00:05:50,960 --> 00:05:58,400
could use that would would benefit them that they're not that they're

53
00:05:58,400 --> 00:06:04,960
missing from their lives this image is is is I think a very important very

54
00:06:04,960 --> 00:06:08,600
useful thing to provide this is what we're doing and this is I think why it

55
00:06:08,600 --> 00:06:12,880
feels so exciting when we finish this and for those of you who were peeking and

56
00:06:12,880 --> 00:06:17,040
I wasn't peeking so I don't all have to look at the video afterwards and see

57
00:06:17,040 --> 00:06:22,480
but for those of you who saw everyone was excited and encouraged I think

58
00:06:22,480 --> 00:06:27,160
especially by younger children of course because they're far more open than

59
00:06:27,160 --> 00:06:32,400
adults who might be even concerned about this or upset about this thinking

60
00:06:32,400 --> 00:06:37,680
that it's some kind of cult activity or so on but especially for the

61
00:06:37,680 --> 00:06:47,260
children providing them with this image as the Buddha said

62
00:06:47,260 --> 00:06:51,440
samanan and jedasanang etamang alamutamang one of the highest blessings of

63
00:06:51,440 --> 00:06:58,880
life is to see people who are of quiet minds or of peaceful minds and so if

64
00:06:58,880 --> 00:07:02,200
you think back to when you were most of us were children unless we grew up in a

65
00:07:02,200 --> 00:07:05,920
Buddhist society for most of us this would you know I would have I would have

66
00:07:05,920 --> 00:07:10,520
been ecstatic to see people meditating when I was young I grew up in a small

67
00:07:10,520 --> 00:07:16,080
town in northern Ontario or the idea of meditation was something of course

68
00:07:16,080 --> 00:07:21,600
completely foreign to the culture and the society so to have this and to have the

69
00:07:21,600 --> 00:07:29,040
children asked their mom their parents what are they doing mom and when I

70
00:07:29,040 --> 00:07:31,880
hear their mothers explaining to their children to be quiet and they're

71
00:07:31,880 --> 00:07:37,160
meditating and so on this opens up a whole new university people so the

72
00:07:37,160 --> 00:07:41,360
inspiration that we give this is what a flash mob is supposed to do and I think

73
00:07:41,360 --> 00:07:46,840
what could be more inspirational than people sitting there I'm doing

74
00:07:46,840 --> 00:07:52,880
nothing it's it's quite inspiring because you know the Buddhist or the

75
00:07:52,880 --> 00:07:58,160
meditator the meditators motto don't just do something sit there this is our

76
00:07:58,160 --> 00:08:03,640
we are the opposite of what people do and people can't as Blaise Baskill said

77
00:08:03,640 --> 00:08:09,320
all of our problems come from not being able to sit alone in a room for

78
00:08:09,320 --> 00:08:13,440
any length of time all of our problems come from our need to be active our

79
00:08:13,440 --> 00:08:22,120
need to consume our need to see and to do and so on you see that's in Thailand

80
00:08:22,120 --> 00:08:25,960
or in Asia in these Buddhist countries my teacher said he said look at these

81
00:08:25,960 --> 00:08:32,400
foreign people who come here and they just look look look look I'll never

82
00:08:32,400 --> 00:08:35,640
have an end that keeps looking and looking for something and they're never

83
00:08:35,640 --> 00:08:41,400
going to find it and it's true when we go to even when people come here to

84
00:08:41,400 --> 00:08:45,040
the forks they're coming here to see something the question is what are they

85
00:08:45,040 --> 00:08:49,800
going to see and we provide them with something new this is the first reason

86
00:08:49,800 --> 00:08:54,560
the second reason the second purpose to what we're doing is to inspire

87
00:08:54,560 --> 00:09:00,440
ourselves and to come together one thing I realized as we are doing this is

88
00:09:00,440 --> 00:09:06,160
this is a actually a group activity I've met new people today which I didn't

89
00:09:06,160 --> 00:09:10,880
really think of as going to be a part of this I I met all of you and we

90
00:09:10,880 --> 00:09:15,640
become closer and talking and and just being together as a group we've

91
00:09:15,640 --> 00:09:20,000
brought together I think at least three four Buddhist communities in

92
00:09:20,000 --> 00:09:28,040
Winnipeg just with our small group here and with the idea that probably if we

93
00:09:28,040 --> 00:09:36,040
do this again and again it will only increase so it's a chance for us to come

94
00:09:36,040 --> 00:09:40,120
out of our shells I was saying this is kind of a coming out experience for

95
00:09:40,120 --> 00:09:46,760
all of us for all of us closet meditators it's a chance for us to show our

96
00:09:46,760 --> 00:09:53,920
meditator pride and and show that we are meditator not pride but I'm making a

97
00:09:53,920 --> 00:10:03,400
play on the whole gay pride no so we're gay in a different way happiness comes

98
00:10:03,400 --> 00:10:10,880
from not from sexuality but it comes from from pronunciation comes from letting

99
00:10:10,880 --> 00:10:18,680
go comes from from within so we're expressing this and we are bringing

100
00:10:18,680 --> 00:10:24,200
meditation into our society this is something that I've always felt very

101
00:10:24,200 --> 00:10:30,640
important that it's one thing to bring happiness to yourself to try to to

102
00:10:30,640 --> 00:10:40,000
cultivate your own happiness and peace and goodness inside but no one is an

103
00:10:40,000 --> 00:10:44,520
island right we you you can't expect to find this peace and happiness without

104
00:10:44,520 --> 00:10:48,560
also impacting the world around you and this is what you sometimes find

105
00:10:48,560 --> 00:10:53,080
in Buddhist societies that were quite insular and even here in the West I've

106
00:10:53,080 --> 00:10:58,880
talked to Buddhist Western Buddhist societies where they would never want to

107
00:10:58,880 --> 00:11:02,360
encourage other people to practice meditation and they feel like it's somehow

108
00:11:02,360 --> 00:11:08,720
just something they do they've decided to do not something that they think is

109
00:11:08,720 --> 00:11:13,480
couldn't better the world it's just like a club that they've joined and so

110
00:11:13,480 --> 00:11:19,480
they don't ever think to not to proselytize or to be missionaries or so on but

111
00:11:19,480 --> 00:11:25,320
to give the opportunity to others or to present this to other people which is

112
00:11:25,320 --> 00:11:32,360
so there's what we're doing we're not handing out flyers we're presenting

113
00:11:32,360 --> 00:11:35,760
some opportunity for people and if they ask about it well we have flyers that

114
00:11:35,760 --> 00:11:39,720
we can give out and we have books on how to practice meditation and we invite

115
00:11:39,720 --> 00:11:44,080
people to join us but the point is that we're impacting our society for our

116
00:11:44,080 --> 00:11:51,800
own benefit we're trying to make our society a peaceful harmonious society

117
00:11:51,800 --> 00:11:55,320
that we can practice meditation and where people don't scream in yell at us

118
00:11:55,320 --> 00:12:00,720
because we're half an hour late or so on this is something that tends to

119
00:12:00,720 --> 00:12:06,200
disrupt your meditation number two number three the final reason I think that

120
00:12:06,200 --> 00:12:14,200
I can think of why we're doing this is to inspire or to as I said to show our

121
00:12:14,200 --> 00:12:20,400
support for the meditation mob movement and so this will be going up on the

122
00:12:20,400 --> 00:12:27,840
internet and we'll have will hopefully inspire other people to perform such

123
00:12:27,840 --> 00:12:32,720
activities along with all the other groups that are doing this and we'll try to

124
00:12:32,720 --> 00:12:35,960
coordinate with them and there's going to be one where in September where the

125
00:12:35,960 --> 00:12:41,200
whole world will be doing this all on one day all the meditation mobs around

126
00:12:41,200 --> 00:12:47,440
the world will synchronize and do a world meditation mob so that's why we're

127
00:12:47,440 --> 00:12:51,880
doing this now I just wanted to talk a little bit about what we're doing this

128
00:12:51,880 --> 00:12:56,600
is something that most of you have your own meditation practice but just for

129
00:12:56,600 --> 00:13:02,120
those people who are watching and and for the the general audience and to remind

130
00:13:02,120 --> 00:13:11,240
us of basically what we're doing here or what we do in the in the Buddhist

131
00:13:11,240 --> 00:13:15,520
tradition to in regards to the practice of meditation what does it mean to

132
00:13:15,520 --> 00:13:18,480
practice meditation

133
00:13:18,480 --> 00:13:26,560
in in and I'm going to go by the tradition I fall but I'll try to be fairly

134
00:13:26,560 --> 00:13:30,400
general and just give some basic outlines as to what we mean by meditation

135
00:13:30,400 --> 00:13:33,400
because the word meditation of course means different things to different

136
00:13:33,400 --> 00:13:39,800
people Renee Descartes had this idea of the word meditation and in the West

137
00:13:39,800 --> 00:13:49,240
pre Buddhist pre Buddhism or pre Hinduism before meditation became meditation of

138
00:13:49,240 --> 00:13:54,600
this sort became popular in the West and it referred more to contemplation so

139
00:13:54,600 --> 00:14:01,000
thinking about mulling over some topic and so the the word meditation of

140
00:14:01,000 --> 00:14:05,120
course is very broad for most people even today who do practice meditation

141
00:14:05,120 --> 00:14:14,240
it simply means an escape from reality but meditation in the Buddhist

142
00:14:14,240 --> 00:14:19,600
teaching as I understand it is not for an escape and certainly not to contemplate

143
00:14:19,600 --> 00:14:24,240
or to think about anything meditation in the in the Buddhist sense is to

144
00:14:24,240 --> 00:14:30,160
create understanding to create specifically wisdom and understanding and the

145
00:14:30,160 --> 00:14:38,280
theory that lies behind all of the Buddhist teaching is that understanding or

146
00:14:38,280 --> 00:14:45,160
wisdom is enough to lead to complete freedom from suffering is enough to

147
00:14:45,160 --> 00:14:49,920
solve all your problems that all of our problems all of the difficulties all

148
00:14:49,920 --> 00:14:54,560
of our suffering can be overcome with by one quality of mind alone and that

149
00:14:54,560 --> 00:15:00,120
is wisdom so our meditation practice is for the purpose of seeing clearly

150
00:15:00,120 --> 00:15:05,360
simply creating understanding this is what we're trying to do the idea is

151
00:15:05,360 --> 00:15:20,760
that a person who has understanding who has wisdom will thereby be in tune

152
00:15:20,760 --> 00:15:26,520
with all other good qualities so love and compassion and joy and kindness all

153
00:15:26,520 --> 00:15:31,120
of these qualities of of mind that we all aspire to will come from

154
00:15:31,120 --> 00:15:41,080
understanding and and they falter due to our misunderstandings so any

155
00:15:41,080 --> 00:15:44,280
problem that we have any difficulty that we come with in our come to in our

156
00:15:44,280 --> 00:15:48,520
life the idea is that it's simply our misunderstanding that is causing us

157
00:15:48,520 --> 00:15:54,440
causing it making it cause us bring out suffering if we come to

158
00:15:54,440 --> 00:16:02,600
understand it we will be free from suffering so the the thrust of the

159
00:16:02,600 --> 00:16:10,000
Buddhist teaching is to create understanding about our experience and

160
00:16:10,000 --> 00:16:15,840
about in general every situation that we come to to be able to understand it

161
00:16:15,840 --> 00:16:22,880
in terms of its ultimate reality the general types of misunderstanding that

162
00:16:22,880 --> 00:16:29,960
the Buddha the Buddha talked about that that that the core of the Buddhist

163
00:16:29,960 --> 00:16:35,760
practice are the misunderstandings of permanence the misunderstanding of

164
00:16:35,760 --> 00:16:42,240
satisfaction and the misunderstanding of controllability or self so

165
00:16:42,240 --> 00:16:49,680
permanence happiness or satisfaction and self and the idea that we have the

166
00:16:49,680 --> 00:16:54,280
idea behind these misunderstood that makes the misunderstandings is that

167
00:16:54,280 --> 00:16:59,520
nothing in the world nothing outside of ourselves is permanent nothing

168
00:16:59,520 --> 00:17:06,200
and out outside of our nothing in our experience is can provide the

169
00:17:06,200 --> 00:17:11,400
stability that we need to be truly happy that there's nothing in the world that

170
00:17:11,400 --> 00:17:17,400
we can rely upon to make us happy it's impermanent there everything is

171
00:17:17,400 --> 00:17:24,000
impermanent is unstable is therefore unsatisfying and I'll cause for

172
00:17:24,000 --> 00:17:33,600
suffering and is uncontrollable and not amenable to our our will our

173
00:17:33,600 --> 00:17:45,520
mastery and can't be truly called a possession so the idea being that we focus

174
00:17:45,520 --> 00:17:50,800
on the objects of our experience or we we misunderstand the objects of our

175
00:17:50,800 --> 00:17:57,000
experience as being as being stable we think of the people that are in our

176
00:17:57,000 --> 00:18:00,200
lives or we think of the situations in our lives we think of even the

177
00:18:00,200 --> 00:18:05,200
experiences that we have as being somehow stable as somehow lasting we think

178
00:18:05,200 --> 00:18:08,200
that when we get what we want it's the happiness that comes is going to

179
00:18:08,200 --> 00:18:15,440
somehow provide some lasting stability for us we think of the things that

180
00:18:15,440 --> 00:18:20,200
we strive and and so we cling to them we cling to the objects of the sense

181
00:18:20,200 --> 00:18:23,400
we cling to people we cling to places we cling to things we cling to the

182
00:18:23,400 --> 00:18:28,120
positions in our lives and we have expectations about them we expect for them

183
00:18:28,120 --> 00:18:34,800
to provide us with this stability we misunderstand that the objects of

184
00:18:34,800 --> 00:18:40,480
experience are are satisfying so we think that central pleasure is somehow

185
00:18:40,480 --> 00:18:47,880
going to satisfy us because we think of it as stable lasting we we think

186
00:18:47,880 --> 00:18:56,720
that acquiring possessions and status and position all of these things are

187
00:18:56,720 --> 00:19:00,440
going to somehow satisfy us and we so we strive for them we think that we

188
00:19:00,440 --> 00:19:06,720
can find happiness in our experiences and in our acquisitions and so we

189
00:19:06,720 --> 00:19:14,560
strive and and and we create this ambition and and desire for

190
00:19:14,560 --> 00:19:22,960
sensuality for for positions and and for ideas and concepts and we

191
00:19:22,960 --> 00:19:29,960
being rich being famous having a secure and stable position in life and so on

192
00:19:29,960 --> 00:19:37,680
and we we misunderstand things as being permanent as being self as being

193
00:19:37,680 --> 00:19:42,320
controllable and so we try to force things we try to force our lives we

194
00:19:42,320 --> 00:19:46,080
try to force our minds we try to force the people around us to conform to

195
00:19:46,080 --> 00:19:52,120
our idea of of what is right of what is proper of what is good of what is

196
00:19:52,120 --> 00:19:58,640
somehow going to bring us happiness someone is going to provide stability so

197
00:19:58,640 --> 00:20:02,440
the Buddhist teaching is for the purpose of destroying these and helping us

198
00:20:02,440 --> 00:20:08,280
to see impermanence to set a impermanence suffering in non-self to see that

199
00:20:08,280 --> 00:20:13,640
these things are not capable of bringing us this stability of not bringing us

200
00:20:13,640 --> 00:20:18,640
this satisfaction or and are not amenable to our control point being to try

201
00:20:18,640 --> 00:20:24,080
to find happiness within ourselves and to let go of our expectations and to

202
00:20:24,080 --> 00:20:32,040
be comfortable with the world or with experience no matter what might

203
00:20:32,040 --> 00:20:38,600
come our way and what presents itself to be able to roll with the punches and

204
00:20:38,600 --> 00:20:46,560
to be to be able to dance with life instead of fight within and the way that

205
00:20:46,560 --> 00:20:50,560
we do this is to try to focus on ultimate reality to try to focus on the

206
00:20:50,560 --> 00:20:54,360
experience itself and to try to get out of these head game in mind games of

207
00:20:54,360 --> 00:21:03,120
creating ideas and expectations and and and concepts like duties or

208
00:21:03,120 --> 00:21:08,760
responsibilities in terms I mean in terms of unrealistic expectations and

209
00:21:08,760 --> 00:21:16,800
to live dynamically or flexibly so to be able to when someone's 30 minutes

210
00:21:16,800 --> 00:21:20,480
late to be able to deal with it I was saying this woman wouldn't survive the

211
00:21:20,480 --> 00:21:24,760
woman who came in this morning wouldn't have survived a day in many of the

212
00:21:24,760 --> 00:21:29,400
countries that I've been to because they have a different work ethic and not

213
00:21:29,400 --> 00:21:33,680
necessarily in fact in some ways a much better work ethic because there's

214
00:21:33,680 --> 00:21:38,480
far less stress and and suffering when there are far less expectation and

215
00:21:38,480 --> 00:21:44,480
our need for things to be in a certain way or our expectation or clinging to

216
00:21:44,480 --> 00:21:49,160
things being a certain way is is of course the cause for great suffering it

217
00:21:49,160 --> 00:21:53,240
creates conflict when other people have other expectations or some

218
00:21:53,240 --> 00:21:58,280
experience so we're trying to create a sort of flexibility by focusing on what's

219
00:21:58,280 --> 00:22:03,960
real and on the way things are instead of the way we'd like them to be and so

220
00:22:03,960 --> 00:22:09,360
to do this we practice what the Buddha called mindfulness and I think

221
00:22:09,360 --> 00:22:15,320
mindfulness is certainly the most common Buddhist meditation practice so most

222
00:22:15,320 --> 00:22:19,520
people are quite familiar with the concept I'm just gonna go briefly briefly

223
00:22:19,520 --> 00:22:26,000
through what it means and discuss the basic technique and practicing

224
00:22:26,000 --> 00:22:34,440
mindfulness so when we talk about mindfulness we're referring to a ability to

225
00:22:34,440 --> 00:22:42,760
recognize reality as it is so when you experience a phenomenon whether it be

226
00:22:42,760 --> 00:22:46,800
seeing or hearing or smelling or tasting or feeling or thinking when you

227
00:22:46,800 --> 00:22:51,600
have an experience to experience it just as it is without all sort without all

228
00:22:51,600 --> 00:22:59,600
the ordinary baggage or judgments without liking it or disliking it or wanting

229
00:22:59,600 --> 00:23:06,200
more of it or wanting it to go away or wishing it hadn't come or being afraid

230
00:23:06,200 --> 00:23:12,480
that it might of what might come in the future to simply accept it for what it

231
00:23:12,480 --> 00:23:16,960
is to be here and now when you experience pains to experience them just as

232
00:23:16,960 --> 00:23:20,720
pain when you experience happiness to experience it just as happiness when you

233
00:23:20,720 --> 00:23:25,120
experience liking or disliking to experience it just as it is and not let it

234
00:23:25,120 --> 00:23:32,640
become something more a lot of our problems even the negative qualities of

235
00:23:32,640 --> 00:23:41,440
mind are only negative because we extrapolate upon them or we we cause them

236
00:23:41,440 --> 00:23:45,680
to snowball so we're afraid of our fear we're worried about our worry we

237
00:23:45,680 --> 00:23:49,720
become anxious about our anxiety we depressed about our depression we're

238
00:23:49,720 --> 00:23:57,680
angry about our anger we we perpetuate all of our negative emotions by not

239
00:23:57,680 --> 00:24:00,520
being able to experience them just as they are and so this is what we're trying

240
00:24:00,520 --> 00:24:05,600
to do experience reality objectively reminding ourselves the word mindfulness

241
00:24:05,600 --> 00:24:11,800
set deep means to remind yourself the the original word is simply this

242
00:24:11,800 --> 00:24:15,960
ability to remind oneself so that we don't forget ourselves we don't

243
00:24:15,960 --> 00:24:20,440
forget what's really going on when you experience pain for example to not

244
00:24:20,440 --> 00:24:24,880
forget that it's just pain do not get lost and caught up in the worries and the

245
00:24:24,880 --> 00:24:29,000
fears about what it might become and how it might would it may be and the

246
00:24:29,000 --> 00:24:32,360
aversion towards it and how to get rid of it and so on to simply see it as

247
00:24:32,360 --> 00:24:37,480
pain and let it go and to be able to continue on and be flexible to be able

248
00:24:37,480 --> 00:24:41,960
to experience pain to be able to experience happiness to experience the whole

249
00:24:41,960 --> 00:24:50,160
spectrum of reality without requiring it to be something else and the method

250
00:24:50,160 --> 00:24:54,200
that we use in the tradition that I follow is to literally remind yourself so

251
00:24:54,200 --> 00:24:58,280
when you feel pain you would remind yourself pain when you would feel happiness

252
00:24:58,280 --> 00:25:01,880
you would remind yourself happy when you hear something you would remind yourself

253
00:25:01,880 --> 00:25:05,600
hearing when you see something you would remind yourself hearing just in your

254
00:25:05,600 --> 00:25:12,880
mind create this clear thought the objective reminder it's just that it is

255
00:25:12,880 --> 00:25:17,600
what it is it's nothing more it's nothing less and to do this we break our

256
00:25:17,600 --> 00:25:21,520
experience up into four parts we have what are called the four foundations of

257
00:25:21,520 --> 00:25:26,320
mindfulness the body the feelings the mind and the dummies

258
00:25:26,320 --> 00:25:31,640
dummies being a sort of a miscellaneous group miscellaneous category well we

259
00:25:31,640 --> 00:25:35,360
start with the body and so we'll focus on the bodily experiences when when you

260
00:25:35,360 --> 00:25:40,200
're sitting we just remind ourselves sitting just knowing that we're sitting if

261
00:25:40,200 --> 00:25:44,880
we're watching the breath we'd watch the the effect that the breath has on the

262
00:25:44,880 --> 00:25:50,120
body when the stomach or the chest rises we would say to ourselves rising when

263
00:25:50,120 --> 00:25:55,280
it falls we would say to ourselves falling just being aware as it happens

264
00:25:55,280 --> 00:25:59,280
we're already aware of it just reminding ourselves keeping ourselves with

265
00:25:59,280 --> 00:26:03,160
that bare awareness so that it doesn't become something more it's not there's no

266
00:26:03,160 --> 00:26:11,440
identification or or attraction or aversion it's just the experience for the

267
00:26:11,440 --> 00:26:15,360
feelings when we feel happy or when we feel pain as I said when we feel pain

268
00:26:15,360 --> 00:26:20,760
we'll say to ourselves pain pain just meditating on the pain really not

269
00:26:20,760 --> 00:26:24,160
letting it give rise to disliking in a version because actually the pain isn't

270
00:26:24,160 --> 00:26:28,400
the problem it's the aversion that's the real problem when we feel happy likewise

271
00:26:28,400 --> 00:26:32,480
there's nothing wrong with happiness of course but once you get into liking and

272
00:26:32,480 --> 00:26:38,080
wanting it creates desire and addiction and this habitual need for specific

273
00:26:38,080 --> 00:26:44,120
types of experience which can never be fully satisfied so to stay at just being

274
00:26:44,120 --> 00:26:48,360
happy of course we experience the happiness just as we experience the pain and we

275
00:26:48,360 --> 00:26:53,640
say to ourselves happy happy just staying with it when we feel calm likewise we

276
00:26:53,640 --> 00:27:01,840
say to ourselves calm and for the mind when we're thinking about the past or

277
00:27:01,840 --> 00:27:05,280
the future whether it be good thoughts or bad thoughts again we're not even

278
00:27:05,280 --> 00:27:10,440
trying to stop ourselves from thinking we're not trying to control we're

279
00:27:10,440 --> 00:27:14,760
trying to understand and you can't understand something if you don't let it

280
00:27:14,760 --> 00:27:17,840
happen you can't even understand your negative qualities of mind if you're

281
00:27:17,840 --> 00:27:20,920
always running away from them or pretending they don't exist or feeling guilty

282
00:27:20,920 --> 00:27:25,720
about them everything we have to experience we have to let it arise we have to

283
00:27:25,720 --> 00:27:31,800
let it come so when you're thinking don't try to quash the thought but just

284
00:27:31,800 --> 00:27:36,200
remind yourself that's thinking so that you don't get caught up in whatever the

285
00:27:36,200 --> 00:27:41,040
content of the thought is and get lost in ideas and concepts which don't really

286
00:27:41,040 --> 00:27:45,720
exist anywhere except in your mind what really exists at that moment is a

287
00:27:45,720 --> 00:27:49,200
thinking there's an experience of thinking so we remind ourselves thinking

288
00:27:49,200 --> 00:27:57,800
just meditating on it and with the dumbas while there's many different kinds of

289
00:27:57,800 --> 00:28:01,960
dumbas that will come across the first one is the hindrances these are the most

290
00:28:01,960 --> 00:28:05,880
important to talk about right off the bat because they're what will attack us

291
00:28:05,880 --> 00:28:10,680
and keep us from ever seeing clearly and from ever finding peace happiness or

292
00:28:10,680 --> 00:28:17,000
freedom from suffering they're what hold us back in our lives and in our our

293
00:28:17,000 --> 00:28:22,840
goals in both in the world and spiritually so they're very important to address

294
00:28:22,840 --> 00:28:27,000
right away and throughout our meditation practice but again we're not judging

295
00:28:27,000 --> 00:28:30,920
them even though I've said now that they're bad things I want you to practice

296
00:28:30,920 --> 00:28:34,200
as though they were just objects maybe paying a little more attention to them

297
00:28:34,200 --> 00:28:38,120
because they will be the thorn in your side throughout your practice if you

298
00:28:38,120 --> 00:28:42,120
don't but again trying to be as objective about them as you can not letting

299
00:28:42,120 --> 00:28:47,160
them snowball into some big problem because that's where the problem arises not

300
00:28:47,160 --> 00:28:51,240
from these emotions themselves but from when we let them get to us when you

301
00:28:51,240 --> 00:28:57,480
like like something dislike something when you're feel tired or drowsy when you

302
00:28:57,480 --> 00:29:02,440
feel distracted or worried and when you have doubt or confusion these are the

303
00:29:02,440 --> 00:29:07,160
states of mind that are going to cause us problems so when we like something

304
00:29:07,160 --> 00:29:11,880
or when we want something we'll simply try to stay with it and not let it

305
00:29:11,880 --> 00:29:17,000
become something more not not react to it like we say to ourselves like

306
00:29:17,000 --> 00:29:20,920
King like King or wanting want to we dislike something we'll say dislike King

307
00:29:20,920 --> 00:29:27,560
dislike in everyone we feel angry or sad or depressed or bored or frustrated

308
00:29:27,560 --> 00:29:33,800
afraid we just say to ourselves just like angry angry or bored bored or so on

309
00:29:33,800 --> 00:29:38,040
according to the experience when we feel tired as a tired

310
00:29:38,040 --> 00:29:44,200
just reminding ourselves and in some way removing the imbalance that is

311
00:29:44,200 --> 00:29:48,920
causing these these things to arise they can only come when you're off

312
00:29:48,920 --> 00:29:52,520
balance if your mind is pure and clear in the present moment none of these

313
00:29:52,520 --> 00:29:56,680
things will arise so there's something wrong and just by bringing your

314
00:29:56,680 --> 00:30:00,840
mind back to the present moment back to unawareness of it it will disappear

315
00:30:00,840 --> 00:30:05,400
when you say to yourself tired tired miraculously you'll find that the

316
00:30:05,400 --> 00:30:10,680
fatigue will dissipate because it's an imbalance in the mind

317
00:30:10,680 --> 00:30:16,280
either that or you'll fall asleep the fourth one is distraction and worries

318
00:30:16,280 --> 00:30:18,760
so if you're worried or distracted as well that they're distracted

319
00:30:18,760 --> 00:30:21,480
distracted or worried and if you have doubt or confusion say doubting

320
00:30:21,480 --> 00:30:26,120
down to your country's confused and the only other dumb

321
00:30:26,120 --> 00:30:29,560
group that's worth thinking about at least

322
00:30:29,560 --> 00:30:33,880
on a basic level is the senses so right away when you see or hear

323
00:30:33,880 --> 00:30:37,560
smell or taste or feel or think something you can be mindful of that as

324
00:30:37,560 --> 00:30:40,920
well saying to yourself seeing or hearing hearing

325
00:30:40,920 --> 00:30:45,640
smelling smelling tasting feeling as I said already thinking

326
00:30:45,640 --> 00:30:50,360
being aware of the experience this is basically how one might go about

327
00:30:50,360 --> 00:30:52,920
trying to understand reality and of course there are other techniques

328
00:30:52,920 --> 00:30:57,240
that are similar to these or even perhaps somewhat different

329
00:30:57,240 --> 00:31:01,080
but all in all we're talking about a objective awareness the

330
00:31:01,080 --> 00:31:04,760
reminder the ability to remember things as they are

331
00:31:04,760 --> 00:31:09,960
this is what we mean by the practice of meditation so this is

332
00:31:09,960 --> 00:31:16,920
well just something to provide some some instruction

333
00:31:16,920 --> 00:31:21,320
on this day of mindfulness and we'll try to

334
00:31:21,800 --> 00:31:26,680
maybe coordinate this conduct this sort of this experience on a weekly

335
00:31:26,680 --> 00:31:34,280
basis and we have we have the space here so in the future we could have

336
00:31:34,280 --> 00:31:40,360
discussion we can have different sort of activities going on

337
00:31:40,360 --> 00:31:43,560
maybe we can do the medit do the talk first and have the meditation afterwards

338
00:31:43,560 --> 00:31:49,640
sometimes today was our first test run so I think this was a great success

339
00:31:49,640 --> 00:31:56,680
everyone's already talked about listening to people talking about how they

340
00:31:56,680 --> 00:32:01,080
they think people react and positively to it so it's great to see

341
00:32:01,080 --> 00:32:05,720
great to hear and thank you all for coming out now as a last

342
00:32:05,720 --> 00:32:11,240
sort of a closing activity I'd like to ask everyone to

343
00:32:11,240 --> 00:32:17,080
close our eyes and send our thoughts of good will

344
00:32:17,080 --> 00:32:23,720
and kindness and compassion to first the spirits and the people that

345
00:32:23,720 --> 00:32:29,560
are here and at the forex and then to our families and loved ones and

346
00:32:29,560 --> 00:32:34,600
their enemies and to the whole world just spend about five minutes now

347
00:32:34,600 --> 00:33:00,600
sending good thoughts to the world

