1
00:00:00,000 --> 00:00:08,000
Miriam says, I've heard of married monks. Do you think people should listen to the talks these monks give?

2
00:00:08,000 --> 00:00:11,000
I don't mean marry monks, do you?

3
00:00:11,000 --> 00:00:18,000
But Tyrand, it was telling me earlier that he thinks the monks, she, my body, are married.

4
00:00:18,000 --> 00:00:25,000
No, I think it's remarboity, it's those, those rishis, the guys that look like monks

5
00:00:25,000 --> 00:00:31,000
but have this thing on their Hindu or something. So some of those and I'm not a poor man.

6
00:00:31,000 --> 00:00:38,000
And they might very well be married.

7
00:00:38,000 --> 00:00:41,000
Yeah, please.

8
00:00:41,000 --> 00:00:48,000
In Zen, you are allowed to marry, so I know Japanese monks and they're married.

9
00:00:48,000 --> 00:00:53,000
They're very good monks, so you can't generalize.

10
00:00:53,000 --> 00:00:58,000
So that tradition's in Buddhism when they are allowed to marry, so even.

11
00:00:58,000 --> 00:01:03,000
But there is, I mean, the fact that they're married does give.

12
00:01:03,000 --> 00:01:08,000
I would say it does kind of take away from the whole, leaving some sorrow behind the thing.

13
00:01:08,000 --> 00:01:11,000
Well, then you have to question Zen as such.

14
00:01:11,000 --> 00:01:16,000
Yeah, but Zen, sometimes they want to become Buddhas, I don't know.

15
00:01:16,000 --> 00:01:19,000
And then they have to go through everything in you.

16
00:01:19,000 --> 00:01:23,000
No, yeah, so it's all different. I mean, what route are they going?

17
00:01:23,000 --> 00:01:26,000
What are they on? What are they doing that they have become married?

18
00:01:26,000 --> 00:01:30,000
But it seems that it would dilute the teachings to have a life.

19
00:01:30,000 --> 00:01:34,000
I mean, there's dating service that they have in Japan for monks.

20
00:01:34,000 --> 00:01:41,000
Because they need, but it seems like it's just because they need more monks.

21
00:01:41,000 --> 00:01:45,000
So they have babies and the babies become monks.

22
00:01:45,000 --> 00:01:48,000
That's what the article said.

23
00:01:48,000 --> 00:01:51,000
But, okay, if you want to go...

24
00:01:51,000 --> 00:01:54,000
I mean, I know even in our tradition there are married people who are married.

25
00:01:54,000 --> 00:01:56,000
S.M. Gwenke is married.

26
00:01:56,000 --> 00:02:00,000
And he's a very well respected person.

27
00:02:00,000 --> 00:02:04,000
So yeah, I guess the marriage itself doesn't mean much.

28
00:02:04,000 --> 00:02:10,000
But the whole concept of marrying as a part of Buddhism,

29
00:02:10,000 --> 00:02:19,000
I think of Rob's question as diluting the teaching because what is it to marry someone?

30
00:02:19,000 --> 00:02:27,000
Is there a function to it? I mean, is there a Darmic benefit to marrying?

31
00:02:27,000 --> 00:02:32,000
Seeing the side, seeing the suffering of life.

32
00:02:32,000 --> 00:02:36,000
What we have to make is that we don't want to go into that.

33
00:02:36,000 --> 00:02:41,000
We don't want to go into that example.

34
00:02:41,000 --> 00:02:46,000
We have a meditator here right now that we don't want to go into that.

35
00:02:46,000 --> 00:02:53,000
Well, she's thinking of letting go and giving up the whole idea of getting married.

36
00:02:53,000 --> 00:02:58,000
So we were just talking about marriage.

37
00:02:58,000 --> 00:03:08,000
Yeah, let's stop there.

