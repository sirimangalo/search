1
00:00:00,000 --> 00:00:03,840
So, suggestions on increasing mindfulness throughout a busy day.

2
00:00:06,560 --> 00:00:10,000
I don't know. I mean, read my booklet, it's the first thing because in the booklet,

3
00:00:10,000 --> 00:00:17,200
it does give suggestions, specifically relating to a busy day, you know?

4
00:00:18,960 --> 00:00:21,200
You know, when I call those. Go for it.

5
00:00:22,480 --> 00:00:23,840
I call those mini meds.

6
00:00:25,360 --> 00:00:26,640
I like your mini meds.

7
00:00:26,640 --> 00:00:36,640
Many meditations are, you know, it's mini medication, mental medications.

8
00:00:38,640 --> 00:00:41,360
But I can find those at various times during the day.

9
00:00:42,240 --> 00:00:47,680
If you're able to take your mind away from your physical work, whatever it is you're

10
00:00:48,320 --> 00:00:54,080
actually doing, you don't want to be doing a dangerous job or driving or something,

11
00:00:54,080 --> 00:00:57,280
that even though we were driving, as everyone knows,

12
00:00:58,880 --> 00:01:00,000
you'll be aware of driving.

13
00:01:01,200 --> 00:01:07,120
And the next moment you're aware of driving, you may have covered a mile or two.

14
00:01:08,240 --> 00:01:11,200
And you really think about it. You think, well, what happened?

15
00:01:11,920 --> 00:01:18,080
No, I actually drove and I didn't really realize I was driving, but still with mini meditations,

16
00:01:18,080 --> 00:01:27,760
I find it's helpful to me if I become frustrated or if I become confused or if I become

17
00:01:30,240 --> 00:01:33,600
any change in my mental, mental attitudes.

18
00:01:36,720 --> 00:01:43,040
Just take a few moments a minute or two or five minutes, not just shut down completely,

19
00:01:43,040 --> 00:01:50,400
but become aware, become centered, become aware of the fact that you have a brain and what your

20
00:01:50,400 --> 00:01:56,160
brain is doing, whether or not you need to slow down and leave that you wait the moment,

21
00:01:56,880 --> 00:02:02,960
change the moment, and look at it from a perspective that what's out of word about something that

22
00:02:02,960 --> 00:02:12,240
has just happened in the past or am I projecting too far into the future and worrying about things

23
00:02:12,240 --> 00:02:22,560
that may or may not occur in the future. Once I sit in myself, then I become calmer and more rational

24
00:02:22,560 --> 00:02:33,280
than I think those I've used for a long time. If I don't want to sit, take a 15 or 20 or 30 minute

25
00:02:33,280 --> 00:02:42,880
period for a formal meditation session, then I can use what I call mini meds to just re-center

26
00:02:42,880 --> 00:02:53,680
myself and start over. For sure, taking stock of your situation, I think one other thing to do

27
00:02:53,680 --> 00:03:04,640
is to get this in your mind that you can do it and be ready for it. A busy, busy days generally

28
00:03:04,640 --> 00:03:10,080
tend to, unless you're talking about one busy day or something, you're probably talking about

29
00:03:10,080 --> 00:03:15,680
five days of your week or something like that. In that case, it's something that you can prepare for,

30
00:03:15,680 --> 00:03:19,360
because you know what tomorrow is probably going to be like, what sort of events it's going to

31
00:03:19,360 --> 00:03:23,680
include, you're probably going to have to deal with this situation, that situation, and so you can

32
00:03:23,680 --> 00:03:29,920
prepare mentally and think how you're going to be mindful in this situation, looking back at how

33
00:03:29,920 --> 00:03:37,520
you weren't mindful in that situation and so on, and can develop a routine and not necessarily

34
00:03:37,520 --> 00:03:47,760
a routine, but at least you can be ready to respond to certain situations. So when you get angry,

35
00:03:47,760 --> 00:03:51,680
you're ready to remind yourself, yes, this is the time to take a mini med.

36
00:03:53,360 --> 00:03:59,520
But we're not thinking in terms, we're not talking in terms of not thinking about the future,

37
00:03:59,520 --> 00:04:05,360
not planning for the future at all, we don't even mean that, I don't suggest anything in those

38
00:04:05,360 --> 00:04:12,320
terms, but what I do think of, when you're thinking of the future and more than I would say

39
00:04:12,320 --> 00:04:20,160
fantasizing, making up things about the future that may negatively affect me in the present,

40
00:04:20,160 --> 00:04:27,200
those are the type of things that I feel should not be entertained when you're thinking about

41
00:04:27,200 --> 00:04:32,320
the future, sure, but you can find them, you've got to do that in the society, you know,

42
00:04:32,320 --> 00:04:38,320
you know what around it, whether it's family concerns or work concerns or the future of your

43
00:04:38,320 --> 00:04:42,960
frontances or whatever it is, you're going to have to plan for the future, and I don't think you

44
00:04:42,960 --> 00:04:49,040
need to not plan for the future in that respect, but just the fact that watch what you are thinking

45
00:04:49,040 --> 00:04:54,880
about concerning the future, whether or not it's productive at all, or if it's negative,

46
00:04:56,160 --> 00:05:01,920
a negative thought projected into the future, then I think we should learn ourselves directly

47
00:05:01,920 --> 00:05:05,680
to the realities of the present moment and start over again.

48
00:05:05,680 --> 00:05:12,480
The point is, think of, think with wisdom, if you're thinking with wisdom, you think with clarity

49
00:05:12,480 --> 00:05:20,720
of mind, if you have those qualities, then they take the anger out, they take the negative

50
00:05:20,720 --> 00:05:25,680
negativity out, they take the attachment out, they take the partiality out, you want to be

51
00:05:25,680 --> 00:05:32,960
objective and clear in mind when you plan, right? No one wants to be planning with partial,

52
00:05:32,960 --> 00:05:39,760
imbalanced thoughts, and yet this is what we often do. We're hoping for this, we're hoping for

53
00:05:39,760 --> 00:05:47,040
that, and just scheming for this, and scheming for that, and so this corrupts us and it corrupts

54
00:05:47,040 --> 00:05:47,680
our future.

55
00:05:47,680 --> 00:06:03,600
When there's a question on history, I will hear about Buddhism, I guess, as someone else.

