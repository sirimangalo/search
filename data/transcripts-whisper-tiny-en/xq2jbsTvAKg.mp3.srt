1
00:00:00,000 --> 00:00:15,680
Okay, this is my first phone video. This is a pile of gravel and this is a pile of sand.

2
00:00:15,680 --> 00:00:26,040
And these have to be carried down to our caves and this is our first construction project

3
00:00:26,040 --> 00:00:34,400
for my first construction project here. I'm going to make two bathrooms for the two caves

4
00:00:34,400 --> 00:00:40,840
but they're going to be communal bathrooms so that the people using the meditation area there

5
00:00:40,840 --> 00:00:50,760
can also use the washrooms. This is the first step. We're not going to do any accommodations

6
00:00:50,760 --> 00:01:01,200
right now. First we're going to build some bathrooms. This has been my first phone video

7
00:01:01,200 --> 00:01:08,320
just to everybody know that things are coming along. We're slowly but surely getting

8
00:01:08,320 --> 00:01:19,760
into some construction and repairs to make this place a proper meditation center.

9
00:01:19,760 --> 00:01:25,840
And this is the Bodhi tree. If you see this little tree here, this is the little Bodhi

10
00:01:25,840 --> 00:01:30,600
tree. I showed the big Bodhi tree already. This is the little one and there's the

11
00:01:30,600 --> 00:01:36,320
Bihar in the background. So here's a phone video.

