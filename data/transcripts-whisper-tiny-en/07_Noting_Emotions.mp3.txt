noting emotions. In the case of emotions, is it okay to just note feeling or is it better
to label the specific emotion? Emotions are complex entities, they generally contain
fleeting mental component and a more lasting physical effect. It is important to separate
these two respects in order to clearly see the reality of the experience. For example,
anxiety exists only momentarily in the mind, but sometimes appears to persist due to the physical
reaction triggered by it. The actual emotion, like anxiety, disliking, liking, fear,
worry, etc, should be noted by name for the brief moment that arises. The physical aspect of
each should be distinguished as being simply feeling or in certain cases pain, pleasure or calm.
