1
00:00:00,000 --> 00:00:09,000
I remember you mentioning that sometimes people view themselves as a martyr when they go out of the way of helping others such as the poor sick and homeless.

2
00:00:09,000 --> 00:00:17,000
Do you think it's important to go out of the way to do these things like Christian missionaries because it's important to accumulate fields of merit?

3
00:00:17,000 --> 00:00:23,000
If we don't become Arahand, it's better to become a prince in the next life rather than a beggar.

4
00:00:23,000 --> 00:00:42,000
I got the video this evening. If you go on my Google Plus profiling, you'll see this video that doesn't relate to this quite, but it's a video of a beggar.

5
00:00:42,000 --> 00:00:50,000
Sorry, it actually relates perfectly to this question. Worth watching that video.

6
00:00:50,000 --> 00:00:56,000
I'm not going to tell you what's in it. You have to go watch it.

7
00:00:56,000 --> 00:01:03,000
So is it important? Well, the Buddha did say helping others you help yourself, helping yourself you help others.

8
00:01:03,000 --> 00:01:10,000
I've pointed out several times, and I think I would just reiterate that we are like a river.

9
00:01:10,000 --> 00:01:20,000
We have to purify the source, so you should never forego the practice of purifying the source and just making the water available.

10
00:01:20,000 --> 00:01:27,000
If you think that it's good that you have a water source and water available, that's only half the answer.

11
00:01:27,000 --> 00:01:33,000
The other half is that the source has to be pure. Your heart has to be pure if you go out of your way to help others.

12
00:01:33,000 --> 00:01:49,000
Look at how it goes down in reality. We put so much of our defilement into our help for others that we get stressed out, burnt out, other people suffer as a result.

13
00:01:49,000 --> 00:02:14,000
We wind up with doing some good, but we wind up often with a lot of superficial good and a fair amount of deep harm to our own minds and even the minds of others with conflicts and protests and arguments and so on.

14
00:02:14,000 --> 00:02:32,000
So, doing good deeds for others is useful. I think from a point, from my point of view, from the point of view, I probably speak for all of us when we say, going out of your way to accumulate merit is not really the Buddha's teaching.

15
00:02:32,000 --> 00:02:44,000
Something the Buddha would often encourage and people when he saw that that was the best he could give them and when it would help them to understand the relationship between good and evil.

16
00:02:44,000 --> 00:02:53,000
He would often point out that relationship, but ultimately the idea is to overcome and to be free.

17
00:02:53,000 --> 00:03:04,000
So, the goodness that we should do should be a part of our path. This is where I didn't do a video on this asking whether you should meditate or help people.

18
00:03:04,000 --> 00:03:12,000
And we should look at helping people as a part of our path. It's not that we're going out of our way. It's that they're a part of our way. They're in our way.

19
00:03:12,000 --> 00:03:23,000
And if we don't help them, we're just going to create conflict and we're going to get stuck at this point in our path. We have to work this out as a part of our life.

20
00:03:23,000 --> 00:03:30,000
That's why I said to my teacher when he meets with these people, he doesn't get rid of them. He doesn't kick them out. He deals with them.

21
00:03:30,000 --> 00:03:48,000
This is the person in front of me now and going to work this out mindfully.

22
00:03:48,000 --> 00:04:11,000
Yeah, I think there is this Bodhisattva ideal. For some people, it's probably a good way. And the Buddha has been a Bodhisattva many, many thousands of lifetimes before he became the Buddha.

23
00:04:11,000 --> 00:04:29,000
And helping people helps our mind to purify and to become a better being and to understand not only what is going on inside ourselves, but what is going on externally.

24
00:04:29,000 --> 00:04:48,000
So it is helpful to do so, but I think it's not helpful to go and look for where can I be or felt? What can I do like? You mentioned the missionaries, the Christian missionaries.

25
00:04:48,000 --> 00:05:03,000
And that is, as I understand, is getting too much involved in things that is not really your business.

26
00:05:03,000 --> 00:05:21,000
That sounds cruel, I think, is the word. That may sound cruel, but sometimes it's important to help and sometimes the better help is to just leave it as it is and not to get involved in it.

27
00:05:21,000 --> 00:05:36,000
So often the help is cultivating attachment. We have expectations about this and we become frustrated. And the other thing I didn't say, but often we're impressing our views on people.

28
00:05:36,000 --> 00:05:56,000
Exactly. Which is what religions do. It may not be to the point of handing out Bibles, but it often is, but it can often be more subtle than that. Trying to change people's lives, saying, let's get you out of this squalor and put you in apartment buildings, for example.

29
00:05:56,000 --> 00:06:22,000
They had something else to say, I can't remember. The other thing I wanted to say is that we talk about the Bodhisattva and the Buddha, but many people will talk with it and say, yes, there's the Bodhisattva way of going out of dedicating yourself to helping others, but it's not actually the case.

30
00:06:22,000 --> 00:06:40,000
That the Bodhisattva went out of his way to help others. The Bodhisattva spent, if you read what he did during those lifetimes, he didn't really go out of his way to help people. He was trying to develop perfection of mind, which is still a personal thing.

31
00:06:40,000 --> 00:06:48,000
He spent at least as much time often before his meditating as a Bodhisattva, as he did, for example, as a king.

32
00:06:48,000 --> 00:07:12,000
The point is, the Buddha has to do everything. He has to come to know everything. His reach has to be so much broader than a person who's going by his experience and going by his guidance, because the Buddha had to go everywhere before he could put it together himself for himself.

33
00:07:12,000 --> 00:07:32,000
But he didn't go out of his way and there are several examples. There was one example that I thought of when you were talking where Sariputa, the person who was to be Sariputa in his last life, they were both ascetics together.

34
00:07:32,000 --> 00:07:51,000
Sariputa had gotten in his mind that he was going to get everyone to keep the precepts. He would go around to people and say, look, come, come, come. You're not keeping the precepts. I'm going to teach you about these five precepts, the rules or ethics or morality.

35
00:07:51,000 --> 00:08:06,000
The Bodhisattva watched him for a while and he thought, man, this guy is really turning people off. These people aren't doing it from their heart.

36
00:08:06,000 --> 00:08:33,000
He said, I'm going to teach him a lesson. One day, Sariputa came back or the ascetic who was later to be Sariputa came back. He found the Bodhisattva kicking the earth and pulling rocks out of the ground, uprooting trees, throwing dirt here, just running around like a crazy person like he was trying to pull the earth apart.

37
00:08:33,000 --> 00:08:49,000
The ascetic, the Sariputa said, what are you doing? He said, I know what my goal in life is. I've decided that I'm going to level the earth. I'm going to turn the earth, make the earth flat.

38
00:08:49,000 --> 00:09:10,000
Sariputa said, how could you possibly flatten the earth? Are you out of your mind? The Bodhisattva looked at him and said, well, look at you, you're trying to level out, make all people the same and follow the same precepts.

39
00:09:10,000 --> 00:09:20,000
Because he was saying, no, there are mountains, there are valleys. There are good people, there are bad people. You expect them all to be able to follow.

40
00:09:20,000 --> 00:09:32,000
My quest of leveling out the earth is far more achievable than your quest in leveling out people's characters.

41
00:09:32,000 --> 00:09:44,000
And the Sariputa realized how silly he had been from then on. At least from then on, he never gave precepts to anyone who didn't ask.

42
00:09:44,000 --> 00:09:54,000
Something about how that's the tradition of people asking for the precepts. So tonight you asked for the precepts. We don't give them to people unless they ask. That's what that tradition came from.

43
00:09:54,000 --> 00:10:09,000
So it's not true to say that the Bodhisattva's path was to go out of his way to help people. It's not really carried out in the text. He helped an incredible number of people and sacrificed so much.

44
00:10:09,000 --> 00:10:14,000
But it seems to have more and more come natural. And that's of course the path that we're aiming for.

45
00:10:14,000 --> 00:10:29,000
If he hadn't gone out of his way to help people, he would think he would be developing more attachment, more views and opinions and so on. And this idea of trying to level out, make the earth flat.

46
00:10:29,000 --> 00:10:52,000
Okay.

