1
00:00:00,000 --> 00:00:28,440
Okay, good evening, welcome to, welcome back to evening

2
00:00:28,440 --> 00:00:46,040
I'm tonight we are, tonight we're going to skip ahead to mindfulness of feelings.

3
00:00:46,040 --> 00:00:55,520
There's a few more sections in mindfulness of the body, but they're more related to some

4
00:00:55,520 --> 00:01:04,040
other meditation, they're useful, but they're not immediately related to our mindfulness practice,

5
00:01:04,040 --> 00:01:11,960
so I'm just going to stay on track and go through those parts that are directly related

6
00:01:11,960 --> 00:01:23,000
to be bhasana, it's nothing against the other parts, it's just for the purpose of our practice.

7
00:01:23,000 --> 00:01:37,720
We're not, if you're interested in those other practices, mindfulness of the body, mindfulness

8
00:01:37,720 --> 00:01:46,640
of the elements, and mindfulness of the cemetery, contemplation of the dead body, you're welcome

9
00:01:46,640 --> 00:01:52,880
to look them up, quite interesting, but we're moving on to feelings,

10
00:01:52,880 --> 00:02:03,120
so we've talked about the body quite simply talking about being mindful of the experiences,

11
00:02:03,120 --> 00:02:08,800
the physical aspects of experience.

12
00:02:08,800 --> 00:02:14,280
With weight and now with feelings we move on to the mind, something that's mental, the body

13
00:02:14,280 --> 00:02:21,600
doesn't have feelings, feelings aren't physical, let's be clear, the body is not something

14
00:02:21,600 --> 00:02:30,080
that exists, so when we talk about the physical, we're referring to a specific aspect of

15
00:02:30,080 --> 00:02:41,120
experience or certain experiences that involves stiffness and hardness and heat and cold

16
00:02:41,120 --> 00:02:50,200
and that kind of thing, a very limited set of realities as the physical.

17
00:02:50,200 --> 00:03:00,040
So weight and not feeling is something that involves the mind, it's a taste of the experience,

18
00:03:00,040 --> 00:03:09,520
it's what the mind gets from the experience, sometimes it's pleasure, sometimes it's pain,

19
00:03:09,520 --> 00:03:19,040
and sometimes it's neither pleasure nor pain, neutral feelings, and the commentary says

20
00:03:19,040 --> 00:03:30,520
something about neutral feelings, how they're not easy to discern, to be mindful of,

21
00:03:30,520 --> 00:03:40,000
because it's not anything really, and it says, but you discern them because at the time

22
00:03:40,000 --> 00:03:43,680
of a neutral feeling there is no pleasure and there is no pain, that's why the Buddha

23
00:03:43,680 --> 00:03:53,080
called neutral feelings often adukamasu kang, not happiness and not suffering, it's

24
00:03:53,080 --> 00:03:58,640
to find by what it's not, because there's neither pleasure nor pain there, well that's

25
00:03:58,640 --> 00:04:07,160
what we mean by a neutral feeling, at that time there is what one would call a neutral

26
00:04:07,160 --> 00:04:21,760
feeling, who pick how we do not, so the text is quite simple, the Buddha says, when experiencing

27
00:04:21,760 --> 00:04:28,840
happy feeling one knows I'm experiencing happy feeling, however you go about that, go about

28
00:04:28,840 --> 00:04:36,080
doing that, when experiencing a suffering feeling one knows I'm experiencing a suffering

29
00:04:36,080 --> 00:04:41,720
feeling, painful feeling, when experiencing neither a pleasant nor painful feeling one

30
00:04:41,720 --> 00:04:47,520
knows I'm experiencing a neither pleasant nor painful feeling, I mean it sounds kind of

31
00:04:47,520 --> 00:04:50,520
simplistic, right?

32
00:04:50,520 --> 00:04:56,640
The commentary makes clear, we're not just talking about the way a baby knows, when

33
00:04:56,640 --> 00:05:06,040
when it's taking milk from its mother, baby knows it's happy, but that kind of knowledge

34
00:05:06,040 --> 00:05:17,640
involves self, it involves possession, it's defiled, it's a knowledge that is not clear

35
00:05:17,640 --> 00:05:26,600
knowledge, it's not clear, some pajamas, not full and complete awareness, it's awareness

36
00:05:26,600 --> 00:05:37,840
that preface is a judgment, a reaction, a attachment, so mindfulness doesn't change our

37
00:05:37,840 --> 00:05:46,760
experiences, it just changes our reactions to them, it prevents the reaction to the feeling

38
00:05:46,760 --> 00:05:55,280
and this is important, feeling is at a place of a huge part in Buddhism, their Buddha

39
00:05:55,280 --> 00:06:02,280
gave whole suitors just on feeling, on Vedana, because Vedana is the bait, Vedana is

40
00:06:02,280 --> 00:06:13,320
the last moment, or the last bastion, I don't know, it's the last defense, it's the

41
00:06:13,320 --> 00:06:21,840
line, Vedana is still neutral, if you experience a pleasant feeling, it's not a bad or a

42
00:06:21,840 --> 00:06:28,120
good thing, you experience a painful feeling also, not a wholesome or unwholesome thing,

43
00:06:28,120 --> 00:06:34,040
these are not good or evil, there's nothing evil about pleasure, there's nothing evil

44
00:06:34,040 --> 00:06:39,000
about pain, likewise there's nothing good or evil about either, there's nothing good about

45
00:06:39,000 --> 00:06:51,760
either, it's what happens the next moment, Vedana is the bait, that catches us up,

46
00:06:51,760 --> 00:06:59,440
and so the next thing the Buddha says in this section he talks about, Samhisa, and

47
00:06:59,440 --> 00:07:06,840
Niramisa, when experiencing something that is Samhisa and something that is Niramisa,

48
00:07:06,840 --> 00:07:19,840
Amisa is like the carnal or the world leader, the corporeal course physical, Niramisa's

49
00:07:19,840 --> 00:07:28,640
immaterial, spiritual, it's translated as, but they're just opposite, so Samhisa refers,

50
00:07:28,640 --> 00:07:37,880
I think, to defile happiness, and this is because this is important to the Buddha in regards

51
00:07:37,880 --> 00:07:46,480
to understanding that Vedana isn't the problem, you come to see that Vedana can exist

52
00:07:46,480 --> 00:07:53,640
with defilement and without defilement, if it's with defilement it involves desires and

53
00:07:53,640 --> 00:08:02,280
diversions, and painful feeling is associated with disliking of the pain, so there's physical

54
00:08:02,280 --> 00:08:19,200
and mental discomfort, pleasure is associated with desire, so there's a wish for an increase

55
00:08:19,200 --> 00:08:24,480
in attachment and addiction to the pleasure, but the pleasure in the pain themselves are

56
00:08:24,480 --> 00:08:29,520
not the problem, and you're able to see the difference, but the difference has nothing

57
00:08:29,520 --> 00:08:36,000
to do with the feeling, and you start to see that your mind starts to grasp that feeling

58
00:08:36,000 --> 00:08:44,600
isn't really the issue at all, because most of our lives is based around this issue

59
00:08:44,600 --> 00:08:52,440
of feeling, what do we do in our lives, we try our best to chase after certain types

60
00:08:52,440 --> 00:08:57,680
of feelings, that's what it comes down to, everything ends up being in drug addiction

61
00:08:57,680 --> 00:09:04,360
into the chemicals in our brain that can give us happiness and give us pleasure, and

62
00:09:04,360 --> 00:09:21,040
run away and chase away and live in an object-fear of painful feelings, or with neutral

63
00:09:21,040 --> 00:09:27,680
feelings be attached to them or be identified with them and that kind of thing, we're

64
00:09:27,680 --> 00:09:38,880
very much caught up in our reactions to feelings, how we would sort of feelings come

65
00:09:38,880 --> 00:09:49,280
to us, we're quite particular about them, and so we think that feeling is the issue,

66
00:09:49,280 --> 00:09:56,360
this is when we talk about suffering, most people see suffering in terms of feelings, suffering

67
00:09:56,360 --> 00:10:04,120
is a specific type of feeling, and if I can just avoid that feeling, that's happiness,

68
00:10:04,120 --> 00:10:10,240
and it's problematic, of course, because you can't avoid any sort of feelings, feelings

69
00:10:10,240 --> 00:10:19,200
are not under your control, they're not where the freedom comes, freedom doesn't come

70
00:10:19,200 --> 00:10:28,360
from freedom from feelings, it's not possible, it can't guarantee absence of certain

71
00:10:28,360 --> 00:10:34,160
types of feelings, we're not in control of reality, even if we were, our control would

72
00:10:34,160 --> 00:10:41,080
never be permanent.

73
00:10:41,080 --> 00:10:53,320
The commentary brings up an interesting sutta, the vedenaparigaha sutta, it's the sutta that

74
00:10:53,320 --> 00:10:58,200
allowed, sorry, put it to become enlightened, and it brings up another interesting issue

75
00:10:58,200 --> 00:11:06,920
in regards to vedena, or in regards to all of this, when we talk about knowing, a big

76
00:11:06,920 --> 00:11:15,920
part of it is knowing, impermanence is knowing that, I mean, when we talk about knowing,

77
00:11:15,920 --> 00:11:20,000
when experiencing a pleasant feeling, I'm experiencing a pleasant feeling, is knowing that

78
00:11:20,000 --> 00:11:24,480
at that moment, there's only the pleasant feeling, and the painful feeling from before

79
00:11:24,480 --> 00:11:35,200
is gone, and coming to see that all of our feelings, all of our experiences are momentary,

80
00:11:35,200 --> 00:11:42,160
and to see that there is no me or mine, and there's no eye that carries on for moment

81
00:11:42,160 --> 00:11:46,360
to moment.

82
00:11:46,360 --> 00:11:52,760
Because when you see that, it becomes a simple matter of experience, it's no longer objects

83
00:11:52,760 --> 00:12:03,120
of desire, it's no longer attachment or possession, there's nothing to possess when experiences

84
00:12:03,120 --> 00:12:09,520
arise and cease, all of the baggage that comes after feeling, that has nothing to do

85
00:12:09,520 --> 00:12:21,160
with feeling, but is caused by our reaction to feeling, all of that is washed away, when

86
00:12:21,160 --> 00:12:29,760
you're able to see feeling as feeling, when there is pleasant or painful, coming to see

87
00:12:29,760 --> 00:12:33,960
that painful feeling isn't the painful feelings aren't the problem, the problem is that

88
00:12:33,960 --> 00:12:39,480
we don't like the painful feelings, that's great liberation, I mean, that's really one

89
00:12:39,480 --> 00:12:51,240
of the classic ways one becomes enlightened is through changing from fighting with the

90
00:12:51,240 --> 00:12:58,200
pain, to standing with the pain, staying with the pain, experiencing the pain without

91
00:12:58,200 --> 00:13:04,040
the defilement, without the baggage, and being able to see the difference, that's nothing

92
00:13:04,040 --> 00:13:10,320
to do with the pain at all, that wasn't the problem, realizing that the problem was never

93
00:13:10,320 --> 00:13:19,160
the pain, and realizing that pain is never a problem as such a liberation, I mean, to see

94
00:13:19,160 --> 00:13:25,720
that the problem is that we want certain types of happiness and certain types of feeling,

95
00:13:25,720 --> 00:13:32,000
so it's quite a short section, but I'm always good to remember how important we

96
00:13:32,000 --> 00:13:39,280
are in the night for becoming enlightened, it's a very simple teaching, but we're dealing

97
00:13:39,280 --> 00:13:48,000
with very strong medicine, when you take this medicine, it makes great change, brings

98
00:13:48,000 --> 00:13:56,480
about great change to your mind, to your life, to your existence, but it frees you from

99
00:13:56,480 --> 00:14:07,360
so much, so there you go, there's our little bit of dhamma for tonight, contemplation

100
00:14:07,360 --> 00:14:13,400
of feeling, of course, when we practice, we just say feeling, feeling, obviously happy,

101
00:14:13,400 --> 00:14:18,960
or pain, pain, you don't have to go all out, the Buddha here, I don't think is talking

102
00:14:18,960 --> 00:14:23,440
about an actual practice, I don't think you would actually sit there and say, I'm experiencing

103
00:14:23,440 --> 00:14:28,560
a happy feeling, I'm experiencing a happy feeling, the point is what can you do to know

104
00:14:28,560 --> 00:14:32,760
that you're experiencing a happy feeling, it's not a mantra in the sense that you just

105
00:14:32,760 --> 00:14:39,840
repeat it blindly to yourself, it's a mantra in the sense that it reminds you, it brings

106
00:14:39,840 --> 00:14:45,200
about that awareness, that I'm experiencing a happy feeling, so by saying to yourself, happy,

107
00:14:45,200 --> 00:14:59,880
happy, that's the experience that comes, okay, so there you go, let's look and see if

108
00:14:59,880 --> 00:15:16,600
there are questions, three questions tonight, looks like, I'm overwhelmed with the information

109
00:15:16,600 --> 00:15:20,720
I'm reading and I'm finding it difficult to see the path I should take in my meditation

110
00:15:20,720 --> 00:15:25,640
practice, there's we pass in a summit that John is loving, kindness, hindrances, fetters,

111
00:15:25,640 --> 00:15:32,440
and appreciative joy, equanimity, et cetera, I don't know where to start, what to focus

112
00:15:32,440 --> 00:15:37,120
on when to move on, et cetera, do you have any guidance for that besides obtaining a

113
00:15:37,120 --> 00:15:41,520
teacher, are your two meditation books on your website enough or do they just outline

114
00:15:41,520 --> 00:15:46,880
meditation in the beginning stage when you look elsewhere, right, so the second booklet

115
00:15:46,880 --> 00:15:55,560
is actually more for people who have done a course and so the first booklet just gives

116
00:15:55,560 --> 00:16:02,680
the very first step in undertaking a meditation course, I wouldn't go, okay, I've read

117
00:16:02,680 --> 00:16:06,720
the first book done, what it says now the next thing to do is read the second book, I would

118
00:16:06,720 --> 00:16:12,080
have to read the first book, you should try and make time to do a meditation course,

119
00:16:12,080 --> 00:16:14,960
and once you've done that then you can start to read the second book, I mean there's

120
00:16:14,960 --> 00:16:22,400
not that much in the second book, but it's more useful when it's accompanying actual

121
00:16:22,400 --> 00:16:25,720
intensive practice and progress.

122
00:16:25,720 --> 00:16:29,400
So all those other types of meditation, I mean yes Buddhism as you're starting to see

123
00:16:29,400 --> 00:16:37,760
is a big thing, even teravada Buddhism is a very big thing, but beyond that there are other

124
00:16:37,760 --> 00:16:46,400
kinds of Buddhism, but even in teravada Buddhism there are even in the orthodoxy, people

125
00:16:46,400 --> 00:16:53,200
like me would consider actually kosher and not heuradic or radical, it's quite diverse,

126
00:16:53,200 --> 00:16:59,000
if you read the Visudhi manga which is a huge book, you start to see how complex it

127
00:16:59,000 --> 00:17:06,720
always, so there's a lot of attempts to boil it down to something manageable and I think

128
00:17:06,720 --> 00:17:13,040
our tradition does that, it makes it quite manageable and offers it in a way that it's probably

129
00:17:13,040 --> 00:17:21,760
the most manageable, the least amount of baggage or complication, so ultimately I would say

130
00:17:21,760 --> 00:17:26,280
mindfulness is where you start and you don't have to go anywhere else, everything else

131
00:17:26,280 --> 00:17:33,720
is sort of ancillary, useful in certain cases, but not always useful, mindfulness is

132
00:17:33,720 --> 00:17:40,680
the only thing that is always useful, which should guide your practice and help you

133
00:17:40,680 --> 00:17:45,240
to see what you need, if it shows you that you're very angry then maybe because you see

134
00:17:45,240 --> 00:17:50,520
that through mindfulness then you start to practice, loving kindness for example, mindfulness

135
00:17:50,520 --> 00:17:59,360
should show you what sort of things might help, but mindfulness is ultimately the main

136
00:17:59,360 --> 00:18:07,640
practice, if we consider on the breath in the same way we're concerned and are distracting

137
00:18:07,640 --> 00:18:14,920
thought as it arises until it disappears, why would one object go away and not the other?

138
00:18:14,920 --> 00:18:22,640
Everything goes away, the breath does go away, it rises and ceases, arises and ceases,

139
00:18:22,640 --> 00:18:29,400
the rising arises and ceases, the falling arises and ceases, same with thoughts, thoughts

140
00:18:29,400 --> 00:18:36,000
arise and cease, I'm not sure if I'm really understanding your question but it's not

141
00:18:36,000 --> 00:18:40,720
about things going away, everything goes away, we're just trying to see that, we're trying

142
00:18:40,720 --> 00:18:46,040
to see things arising and ceasing, because then it starts to show that there's really

143
00:18:46,040 --> 00:18:52,720
nothing particular that we should be clinging to over anything else, why be partial to

144
00:18:52,720 --> 00:18:56,560
certain states over others when everything is just coming and going, it's quite a simple

145
00:18:56,560 --> 00:19:04,560
truth, but it's very profound in its implications, because it teaches us to let go and

146
00:19:04,560 --> 00:19:10,920
to stop trying to force things or cling to things.

147
00:19:10,920 --> 00:19:15,800
I understand one should separate anxiety from feeling that anxiety produces, anxiety

148
00:19:15,800 --> 00:19:21,480
anxiety, then subsequently observe the feelings, you say feelings aren't actually bad

149
00:19:21,480 --> 00:19:25,560
and we should observe them objectively, I disagree.

150
00:19:25,560 --> 00:19:31,880
Those feelings produce a shaky voice and inhibit communication when phrases, ah yes, this

151
00:19:31,880 --> 00:19:35,280
is interesting, I've talked about this before, you may say that it's the anxious thoughts

152
00:19:35,280 --> 00:19:38,880
that create this feelings not the feeling of self, but even if that's the case, the feelings

153
00:19:38,880 --> 00:19:43,120
are still noticeable by those you interact with, they affect the interaction as people

154
00:19:43,120 --> 00:19:48,120
see you sweating, blushing, shaky voice, poor communication and become uncomfortable speaking

155
00:19:48,120 --> 00:19:52,400
with you, since these interactions are affected your relationships are affected, how are

156
00:19:52,400 --> 00:19:56,440
these feelings then not a problem, can one get rid of them if so, more to the general

157
00:19:56,440 --> 00:20:03,560
stages, it's a good well said, but let's be clear here, what is giving rise to those

158
00:20:03,560 --> 00:20:11,200
feelings, you agree okay, it's the anxiety, if you weren't anxious you wouldn't have

159
00:20:11,200 --> 00:20:18,480
the trembling and so on, so that shows to you that anxiety is a problem, you know, I'm

160
00:20:18,480 --> 00:20:25,920
supposed you're very cold and you're shivering and you start to talk and you're shivering,

161
00:20:25,920 --> 00:20:32,160
no one's going to say hi, look at that person, he's so useless, he's shivering, I mean

162
00:20:32,160 --> 00:20:36,760
it's because you'll say well it's nothing to do with me, it's not like I was anxious

163
00:20:36,760 --> 00:20:43,120
and then I'm shivering, right, so the physical experiences are only a problem because they

164
00:20:43,120 --> 00:20:54,320
manifest your anxiety to others and it's embarrassing, but the point is, and the most

165
00:20:54,320 --> 00:21:01,560
important point for a practical perspective is that once you experience these sensations

166
00:21:01,560 --> 00:21:08,360
which are actually neutral, they're neutral in the sense that they are not going to,

167
00:21:08,360 --> 00:21:16,640
they are not going to lean to more shaky feelings, they are not going to lean to more suffering,

168
00:21:16,640 --> 00:21:21,840
let's be clear here, you have the anxiety, it leads to suffering, why?

169
00:21:21,840 --> 00:21:25,760
Because you start shaking and people can say look at that guy, he's anxious and that's

170
00:21:25,760 --> 00:21:32,520
very embarrassing and affects your relationships and makes people annoyed and look down upon

171
00:21:32,520 --> 00:21:36,880
you and so on, but it's because of the anxiety, you don't have to say oh if only I didn't

172
00:21:36,880 --> 00:21:43,920
have those feelings, why so you could hide your anxiety, that wouldn't be the answer,

173
00:21:43,920 --> 00:21:47,640
the answer is not to get anxious in the first place and if you weren't anxious the feelings

174
00:21:47,640 --> 00:21:52,880
wouldn't arise, the feelings themselves don't lean to more feelings and that's what you

175
00:21:52,880 --> 00:21:58,800
have to see is that once you're experiencing these feelings, the answer, these solution

176
00:21:58,800 --> 00:22:03,360
is to be mindful of them, is to not say how can I get rid of these feelings because

177
00:22:03,360 --> 00:22:09,000
that involves anxiety and that's what happens, we have the feelings and we think oh these

178
00:22:09,000 --> 00:22:13,840
are feelings are a problem and that makes us anxious and it becomes a loop because then

179
00:22:13,840 --> 00:22:20,840
they get worse, right? You feed into your emotion, you're experiencing anxiety, it leads

180
00:22:20,840 --> 00:22:25,840
to these feelings, the feelings feed your anxiety and so on and so on and so on.

181
00:22:25,840 --> 00:22:29,480
If you can catch the feelings and be mindful of them, I've done that, you know I used

182
00:22:29,480 --> 00:22:34,920
to be very anxious when I got up to give talks, I've been giving talks now, I started

183
00:22:34,920 --> 00:22:40,080
giving meditation talks just because I thought well this is a useful skill to learn,

184
00:22:40,080 --> 00:22:50,600
I was, I don't know, it's 2,000 and 4 or 5 or something like that and some of them

185
00:22:50,600 --> 00:22:55,280
were awful and then I started getting better at it and people started inviting me places

186
00:22:55,280 --> 00:23:01,800
and I had big audiences and sometimes I was shaking, you know? And so I looked at this

187
00:23:01,800 --> 00:23:05,840
and applied mindfulness to it and I started to see it's quite remarkable, you can be

188
00:23:05,840 --> 00:23:11,080
sitting there shaking and yeah people are aware that you're a little bit shaky but you're

189
00:23:11,080 --> 00:23:17,000
so full of confidence and you're so sure of yourself, even though your voice is shaking,

190
00:23:17,000 --> 00:23:24,800
I such a wonderful feeling and people are quite impressed actually and it shows you

191
00:23:24,800 --> 00:23:31,680
that mindfulness really does solve everything, it really is this solvent that removes

192
00:23:31,680 --> 00:23:41,080
the poison because if you're mindful of your nervous reactions, it somehow changes the

193
00:23:41,080 --> 00:23:52,640
experience and like magic almost, you're not knowing people can tell that you're shaking

194
00:23:52,640 --> 00:23:56,400
and they can see that this person was anxious but they can also see now that this person

195
00:23:56,400 --> 00:24:07,960
has a very strong handle on their experience and they are clear and alert and not anxious.

196
00:24:07,960 --> 00:24:14,920
If you can separate the physical and mental like that it's such a power, so I hope

197
00:24:14,920 --> 00:24:21,200
that makes sense but I think you really have to put it into practice and be clear, it's

198
00:24:21,200 --> 00:24:27,640
a very important part of the solution is to learn to see that the feelings are the problem

199
00:24:27,640 --> 00:24:31,200
because when you do then you stop reacting to them, you stop letting them get you

200
00:24:31,200 --> 00:24:34,560
anxious and make you more anxious, oh no, now I'm having these feelings, what are people

201
00:24:34,560 --> 00:24:47,760
going to think, that's the wrong path, how do we cope with loved ones dying, well loved

202
00:24:47,760 --> 00:24:55,040
ones means you're attached to them and attachment is caused by ignorance and delusion

203
00:24:55,040 --> 00:25:04,200
unfortunately so learning to just love them and not have attachment to them, we're like

204
00:25:04,200 --> 00:25:14,080
ships passing in the night, we can't, we didn't invite, we didn't invite these loved ones

205
00:25:14,080 --> 00:25:20,760
into our lives, they just came and they didn't ask our permission when they leave, when

206
00:25:20,760 --> 00:25:31,280
they die, they just go and that'll always be how it happens and until we learn that and

207
00:25:31,280 --> 00:25:36,480
come to terms with it we'll always cry, we'll always suffer a lifetime after life, time

208
00:25:36,480 --> 00:25:50,440
in the same old way, because of these attachments, so short answer mindfulness, if you

209
00:25:50,440 --> 00:25:54,800
become mindful you'll start to free yourself from those attachments and see the difference

210
00:25:54,800 --> 00:25:59,520
between love and attachment, they're not the same thing or there are two different things

211
00:25:59,520 --> 00:26:05,960
that you can call them both love if you want but it's not very useful, it's more useful

212
00:26:05,960 --> 00:26:21,160
to think of love as something separate from attachment, okay, that's the questions for

213
00:26:21,160 --> 00:26:42,960
tonight, thank you all for tuning in.

214
00:26:42,960 --> 00:26:58,520
Thank you.

