1
00:00:00,000 --> 00:00:15,000
Okay. Good morning. If all is gone as planned and I'm broadcasting live here from

2
00:00:15,000 --> 00:00:36,000
Ching my Bangkok, sorry. I'm just, yes, the live video is going. Okay. So this morning or for a while

3
00:00:36,000 --> 00:00:54,000
now I've been interested in looking at something called the mula casuta, which is a, it's not

4
00:00:54,000 --> 00:00:59,000
really a suta as we understand it in the West. We hear about these sutas or sutras and

5
00:00:59,000 --> 00:01:10,000
I think of the long things, but there are many, many, many little sutras, some of which are incredibly

6
00:01:10,000 --> 00:01:23,000
valuable, but terse simply say what they have to say and that's it. And so there they've

7
00:01:23,000 --> 00:01:32,000
been recorded in their bare essence. I'm keeping a teaching so it'll, they weren't, it wasn't even

8
00:01:32,000 --> 00:01:38,000
recorded where it was given. Usually they are understood to be given, have been given in

9
00:01:38,000 --> 00:01:51,000
sawati. So sometimes they start with just the word sawati nidana means it took place in sawati, but

10
00:01:51,000 --> 00:01:56,000
most of them don't even go that far. They just assume that it's understood unless otherwise

11
00:01:56,000 --> 00:02:13,000
stated. So this one starts with the Buddha saying, let's get the Pali up. It's a ji bikhoi

12
00:02:13,000 --> 00:02:27,500
tia, padi bajaka, yivang pujayu. It's an interesting, interesting start. What's useful or

13
00:02:27,500 --> 00:02:41,000
important about this is the Buddha is telling them that if an Anyatitia, someone who holds

14
00:02:41,000 --> 00:02:49,000
another view, Anyatitia, pri bajaka, someone who has gone forth a recluse in another religion.

15
00:02:49,000 --> 00:02:55,000
If so, if we can translate it generally in modern terms, if someone from another religion

16
00:02:55,000 --> 00:03:07,000
or an ordained, someone who has ordained another religion asks you, sort of a test or a debate.

17
00:03:07,000 --> 00:03:20,000
King Moolak, King Moolak, I also have made a friend having what as their root are all dumbers.

18
00:03:20,000 --> 00:03:28,000
That's the, the pali way of expressing things. What, what do all dumbers have as their root?

19
00:03:28,000 --> 00:03:40,000
Or what is the root of all dumbers? King Moolak, having what as their root?

20
00:03:40,000 --> 00:03:48,000
So right away we have a important question and the answer is going to be decisive.

21
00:03:48,000 --> 00:03:55,000
So this is the sutta you can always come back to. If you want to know what is the root of all dumbers

22
00:03:55,000 --> 00:04:03,000
according to the Buddha, there were dhamma here means all realities.

23
00:04:03,000 --> 00:04:14,000
Or you could potentially say the understanding here is its results or outcomes.

24
00:04:14,000 --> 00:04:31,000
All situations or states including, or yes, out of everything, which one is the root?

25
00:04:31,000 --> 00:04:43,000
And here the root would be, the root of happiness and suffering, the root of good and evil, etc.

26
00:04:43,000 --> 00:04:51,000
Root of all things. And then he goes on, that's only the first one.

27
00:04:51,000 --> 00:05:01,000
There's nine more he says, yes, what if they ask you, King sambhava?

28
00:05:01,000 --> 00:05:23,000
What is the, just a second.

29
00:05:23,000 --> 00:05:38,000
Sambhava, what is the, that which causes them into being?

30
00:05:38,000 --> 00:05:46,000
What is their arising?

31
00:05:46,000 --> 00:05:57,000
King sambhadaya, what do they arise from? Or what is their cause? Sambhadaya.

32
00:05:57,000 --> 00:06:10,000
King sambhava saranya, what do they have is their meeting place or convergence?

33
00:06:10,000 --> 00:06:15,000
Where do they converge? Where do they come together? Sambhava saranya.

34
00:06:15,000 --> 00:06:36,000
King bhamukha, what is their leader? King adipateya, what is their master?

35
00:06:36,000 --> 00:06:55,000
King utra, what is the pinnacle or the highest? What is their lord?

36
00:06:55,000 --> 00:07:17,000
What is higher than all them? What is above them? King sara, King sara sambhada, what is the essence or the core?

37
00:07:17,000 --> 00:07:40,000
What of all them is which one is core? King o gada, o gada, sambhada, what is the,

38
00:07:40,000 --> 00:07:57,000
that which they become immersed in or what is the complete immersion or sort of like the resting place,

39
00:07:57,000 --> 00:08:20,000
what the word here is, that which they become immersed in? King periosana, what is their ending,

40
00:08:20,000 --> 00:08:37,000
complete and final ending, not all done much. So he asks ten very pertinent and potentially difficult questions.

41
00:08:37,000 --> 00:08:47,000
And he says if they ask you these ten questions, how are you going to answer? How would you answer?

42
00:08:47,000 --> 00:08:53,000
The monks are, they play it safe as usual. This is the standard response, they say,

43
00:08:53,000 --> 00:09:10,000
Bhagavang moolaka no bande dhamma. We, the blessed one is, or all dhammas are, the dhamma is rooted in the blessed one,

44
00:09:10,000 --> 00:09:20,000
it comes from the blessed one. The blessed one is our guide nithika, blessed one is our refuge.

45
00:09:20,000 --> 00:09:27,000
Sattva, dhamma, bhakti, bhakti, bhatu would be good if the blessed one were to tell us.

46
00:09:27,000 --> 00:09:41,000
Don't make us guess, please. Tell us the meaning, it does a bhasita sattva. Tell us the meaning of this bhasita, this saying.

47
00:09:41,000 --> 00:09:53,000
Bhagavat also to our bhikkhu, daraisanti. Having heard the blessed one, the bhikkhu will carry it, will hold it, hold that to be true.

48
00:09:53,000 --> 00:10:04,000
Daraisanti, this is, and daraati is, daraati, sorry. Daraati means to hold, it's actually from the same root as the word dhamma.

49
00:10:04,000 --> 00:10:11,000
That's what darama means. They will hold it as darama, whatever the Buddha says.

50
00:10:11,000 --> 00:10:17,000
So they play and say, if they say, wouldn't Lord Buddha, please tell us.

51
00:10:17,000 --> 00:10:26,000
Day in the Himi kueh sonata, sadhu kongmanasikarota. In that case, bhikkhu's, listen well, keep it in mind.

52
00:10:26,000 --> 00:10:35,000
I will speak bhasisami. Ahuang vante, they say, yes, miserable, sir.

53
00:10:35,000 --> 00:10:48,000
Very, let it be, that's miserable, sir. Bhagavahi tada vuacha, the blessed ones, spoke thus.

54
00:10:48,000 --> 00:11:01,000
If sajevi kueh nitya, bhagavati kueh, if requices of other religions or other views, ask you these ten questions,

55
00:11:01,000 --> 00:11:11,000
you should answer as follows. Chandamulaka, I was also a beta. Chanda is the root of all them.

56
00:11:11,000 --> 00:11:24,000
Chandam means contentment or interest desire, you could even say, that interest is probably a safer word here.

57
00:11:24,000 --> 00:11:35,000
Or contentment in the sense of inclining towards inclination maybe.

58
00:11:35,000 --> 00:11:42,000
Maybe we'll just go with desire, even though it's a tough one.

59
00:11:42,000 --> 00:11:52,000
Sambhava, what is the sambhava? What is their coming into being?

60
00:11:52,000 --> 00:11:58,000
It's very tough, a couple of these I don't quite understand, and even the commentary is not clear on,

61
00:11:58,000 --> 00:12:09,000
because the pali is not quite terse. Sambhava means coming into being so Manasika, Manasikara.

62
00:12:09,000 --> 00:12:15,000
It's the sambhava, so Manasikara, keeping in mind or attention.

63
00:12:15,000 --> 00:12:23,000
Attention is what allows, it's like the catalyst, I guess, that allows things to arise.

64
00:12:23,000 --> 00:12:30,000
Because the next one, pasa-sambhadeya, pasa is the, that which causes things to arise.

65
00:12:30,000 --> 00:12:39,000
Contact, contact is the sambhadeya.

66
00:12:39,000 --> 00:12:48,000
Waidana sambhavasarana, waidana, feelings are the feeling of waidana is a bit more than feeling.

67
00:12:48,000 --> 00:12:54,000
It's the actual sensation of the tasting of the experience.

68
00:12:54,000 --> 00:13:01,000
So it's where everything gathers, it's the meeting place, the convergence, everything converges on waidana.

69
00:13:01,000 --> 00:13:09,000
All the other aspects of experience, mr.a. upon waidana, are come together at waidana.

70
00:13:09,000 --> 00:13:22,000
Samadhi pamukha, Samidama, concentration or focus is the leader of all damas.

71
00:13:22,000 --> 00:13:28,000
Satadhi pateya, sambhadeya, Samidama.

72
00:13:28,000 --> 00:13:35,000
All damas have sati, mindfulness as their master.

73
00:13:35,000 --> 00:13:53,000
But Nuttara sambhadeya, wisdom is the, is the highest start, that which is above, that which, the peak of all damas.

74
00:13:53,000 --> 00:14:03,000
We muttisara sambhadeya, freedom or release is the goal or the essence.

75
00:14:03,000 --> 00:14:12,000
But the word essence here is incorrect, it's the, that which is essential or that which has true meaning or benefit.

76
00:14:12,000 --> 00:14:21,000
That which is the goal is maybe the best, not not a real translation here.

77
00:14:21,000 --> 00:14:25,000
Amato gata sambhadem, amata.

78
00:14:25,000 --> 00:14:30,000
The deathless is that which everything plunges into.

79
00:14:30,000 --> 00:14:34,000
Nibana periosana sambhadem.

80
00:14:34,000 --> 00:14:45,000
And Nibana is the ending, the complete and total ending of all damas.

81
00:14:45,000 --> 00:14:52,000
So I think this is an interesting sort of worth a little bit of investigation.

82
00:14:52,000 --> 00:14:59,000
Definitely these are ten of the most important damas there are and here they are classified.

83
00:14:59,000 --> 00:15:04,000
It's understood the importance of these ten.

84
00:15:04,000 --> 00:15:10,000
So Chandamoolakha is, is interesting.

85
00:15:10,000 --> 00:15:17,000
It's an important fact aspect of the Buddha's teaching that whatever you would climb your mind towards.

86
00:15:17,000 --> 00:15:22,000
Whatever you're interested in, that's what comes to me.

87
00:15:22,000 --> 00:15:26,000
So if you inclined your mind in a good way, good things come.

88
00:15:26,000 --> 00:15:30,000
You inclined your mind in a bad way, bad things come.

89
00:15:30,000 --> 00:15:37,000
Whatever you inclined your mind towards, that's where your future lies.

90
00:15:37,000 --> 00:15:42,000
Whatever you're interested in, well that's what gives rise to it.

91
00:15:42,000 --> 00:15:53,000
You can't blame external, you can only blame external factors so far.

92
00:15:53,000 --> 00:16:05,000
In the end it comes down to our own reactions to them and how we change and how we work with what we've got in the present moment to change.

93
00:16:05,000 --> 00:16:09,000
Because everything comes from the mind.

94
00:16:09,000 --> 00:16:25,000
Manopabangam i damas comes from the mind specifically here, from our interest, our desires, our inclinations.

95
00:16:25,000 --> 00:16:28,000
Manasika Rama means attention.

96
00:16:28,000 --> 00:16:34,000
So whatever you give attention to that causes things to happen or that.

97
00:16:34,000 --> 00:16:41,280
or that's, they're the instigator. That's what instigates the next one which is

98
00:16:41,280 --> 00:16:50,480
Pasa. So these are, these two are interesting. It's a reminder of the

99
00:16:50,480 --> 00:17:00,520
importance of, or a reminder of the paradigm in the Buddha's teaching that

100
00:17:00,520 --> 00:17:06,320
it's experiential. So nothing exists until it's experienced or except for

101
00:17:06,320 --> 00:17:20,720
experience. Experience is, it arises in it ceases. All dhammas come into

102
00:17:20,720 --> 00:17:26,120
being with contact. Contact means contact, not physical contact is

103
00:17:26,120 --> 00:17:32,360
contact with the mind. When the mind contacts based on Manasika, or based on

104
00:17:32,360 --> 00:17:37,920
our attention, when we, when we're interested in something, then we

105
00:17:37,920 --> 00:17:43,760
climb our minds and that inclination which gives rise to attention, which gives

106
00:17:43,760 --> 00:17:50,240
rise to contact. Contact is at the eye, the ear, the nose, the tongue, the body,

107
00:17:50,240 --> 00:17:56,600
and the heart or the mind. At any one of these six states, then the five aggregates

108
00:17:56,600 --> 00:18:02,360
arise and there's experience. That experience arises and ceases. That's

109
00:18:02,360 --> 00:18:09,920
reality. So and this is where our meditation takes us. It helps us to see these

110
00:18:09,920 --> 00:18:16,400
experiences one by one by one and see many interesting things about them and to

111
00:18:16,400 --> 00:18:27,400
see how we are relating to these experiences, whether for profit or for ill,

112
00:18:27,400 --> 00:18:33,520
helps us to adjust our habits. This is because it's the

113
00:18:33,520 --> 00:18:39,840
buildings, the building blocks of our lives, of our reality. And so as we

114
00:18:39,840 --> 00:18:49,640
understand them, we can rearrange them based on our understanding. Kings are

115
00:18:49,640 --> 00:18:57,120
most utterness, so we didn't ask the most utterness. We didn't ask the key, we

116
00:18:57,120 --> 00:19:00,880
didn't, we didn't, we're done, right? So based on we didn't, it comes

117
00:19:00,880 --> 00:19:10,480
time time. So it comes real desires or thirst or craving based on we didn't

118
00:19:10,480 --> 00:19:15,000
know. So we didn't know is the key, it's the crux, it's where everything comes

119
00:19:15,000 --> 00:19:19,120
together, but it's also the meaning here is it's the essence of experience.

120
00:19:19,120 --> 00:19:24,360
That's what the word we didn't already means. We didn't mean to experience, we

121
00:19:24,360 --> 00:19:28,520
didn't mean to experience the experience of it. So generally understood to

122
00:19:28,520 --> 00:19:34,440
refer to the pleasant or painful or neutral aspect of experience, how it's

123
00:19:34,440 --> 00:19:46,360
how it's received. We had to nice understood like to be like the king, when the

124
00:19:46,360 --> 00:19:52,480
king is eating, so many people have to prepare the food and bring it to the

125
00:19:52,480 --> 00:19:59,320
king, only the king himself gets to taste it. It's a way to know is the one that

126
00:19:59,320 --> 00:20:14,520
tastes the experience. Samadhi Pamokha focuses the leader, this is I think the

127
00:20:14,520 --> 00:20:18,960
most useful part in my mind of the suit is the difference between Samadhi

128
00:20:18,960 --> 00:20:25,440
and Sati, because I use this aspect of this, this, these two together often

129
00:20:25,440 --> 00:20:32,440
I'll remind people of these things. Samadhi is the leader, but mindfulness is

130
00:20:32,440 --> 00:20:41,520
the master or the Lord. What does this mean? It means that Samadhi is, is it the

131
00:20:41,520 --> 00:20:50,760
front? Samadhi leads you. But mindfulness is the guide, mindfulness is the

132
00:20:50,760 --> 00:20:59,080
controller. So based so if you have, the important point here is that if you

133
00:20:59,080 --> 00:21:06,720
have Samadhi, but no Sati, you see, then you go, but you don't know where you're

134
00:21:06,720 --> 00:21:12,160
going. It's like a boat without a rudder or a boat without a captain. The boat

135
00:21:12,160 --> 00:21:17,680
just drifts, even though there's a great wind and it's going to be quite

136
00:21:17,680 --> 00:21:25,320
powerful. It's like a car without a driver. It's going to go. So concentration

137
00:21:25,320 --> 00:21:29,880
is this way. If you practice meditation without mindfulness, just focusing the

138
00:21:29,880 --> 00:21:34,400
mind. Many people do this, they think meditation is just, the goal is just focus

139
00:21:34,400 --> 00:21:40,520
your mind. Don't realize that there's a quality to it. And if that's a good

140
00:21:40,520 --> 00:21:43,880
quality, if your mind is in a good place, then the concentration will be

141
00:21:43,880 --> 00:21:50,200
doing a good way. But if your mind comes to be in a bad way, the concentration

142
00:21:50,200 --> 00:21:56,360
will lead you to can drive you crazy even without mindfulness. So in meditation

143
00:21:56,360 --> 00:22:00,880
we focus more on mindfulness, cultivating mindfulness and letting the

144
00:22:00,880 --> 00:22:12,120
concentration develop naturally. You want the concentration to develop

145
00:22:12,120 --> 00:22:17,040
slowly, it develops quickly without mindfulness and its dangers. If you're

146
00:22:17,040 --> 00:22:23,000
developing mindfulness and we could say concentration together, then you have

147
00:22:23,000 --> 00:22:29,880
the power and you have the control. Mindfulness is what keeps the mind from.

148
00:22:29,880 --> 00:22:33,800
Going off track.

149
00:22:47,680 --> 00:22:51,520
But without samadhi, the other thing is if you have just sati, but no samadhi,

150
00:22:51,520 --> 00:22:58,600
you also can't, you don't go anywhere at all. You mind is too distracted and

151
00:22:58,600 --> 00:23:09,640
you need both samadhi and sati. Pan-nutarasamidhama, wisdom is the highest of the

152
00:23:09,640 --> 00:23:15,680
pinnacle of the peak. That's above all dumbness.

153
00:23:15,680 --> 00:23:30,000
This is another very important aspect of the Buddha's. I mean, this is to

154
00:23:30,000 --> 00:23:37,200
really outline Buddhism in a very compact manner. We're giving this list of

155
00:23:37,200 --> 00:23:41,440
ten things. It really sets out. This is why he asks. He says, if others ask you,

156
00:23:41,440 --> 00:23:46,000
they can be clear. This is what Buddhism believes. These are the understandings of

157
00:23:46,000 --> 00:23:49,840
Buddhism. Buddhism with wisdom is the highest. This is another clear feature of

158
00:23:49,840 --> 00:23:56,840
Buddhism. It's not compassion. It's not love. It's not mindfulness. It's not

159
00:23:56,840 --> 00:24:04,560
concentration. Not happiness. It's not peace. It's wisdom. That is the highest

160
00:24:04,560 --> 00:24:15,000
wisdom that we put up at the top. It rises above all dumbness. Wisdom is like

161
00:24:15,000 --> 00:24:21,520
being at the top of a mountain looking down and you can see everything. But

162
00:24:21,520 --> 00:24:29,160
you don't get caught up in everything. So our goal in meditation is always

163
00:24:29,160 --> 00:24:34,960
understanding, learning more about ourselves, understanding how we work,

164
00:24:34,960 --> 00:24:38,280
understanding our defilements, understanding our suffering, understanding

165
00:24:38,280 --> 00:24:51,020
happiness, understanding goodness. We mutisara, the essence or the goal of all

166
00:24:51,020 --> 00:24:55,960
dumbness. Out of all dumbness, what is the goal? The goal is freedom. The goal is to

167
00:24:55,960 --> 00:25:07,040
be free. To not be bound to anything. To not be imprisoned by anything. To not

168
00:25:07,040 --> 00:25:13,000
be imprisoned by our defilements. To not be imprisoned by pretty things or

169
00:25:13,000 --> 00:25:20,080
attractive things. To not be imprisoned by hatred or aversion. These are all

170
00:25:20,080 --> 00:25:26,800
prisons. To not be imprisoned by ignorance and delusion and confusion. Doubt, worry.

171
00:25:26,800 --> 00:25:36,240
To not become a slave to our minds, our emotions and so on. To be free. To not

172
00:25:36,240 --> 00:25:53,880
have to jump. Every time we wonder whether we don't want. I'm a dog-a-da-da-da.

173
00:25:53,880 --> 00:26:04,560
I'm a mata is the, at which things plunge into. The meaning here, if you unpack

174
00:26:04,560 --> 00:26:12,280
it is that it's a resistance plunging into the deathless. Nobody uses this

175
00:26:12,280 --> 00:26:31,800
phrase. Plunge into deathless. Deathless. Deathlessness. Because all dumbness

176
00:26:31,800 --> 00:26:38,920
are death. All dumbness. All other dumbness. Besides the Obama are mortal. They

177
00:26:38,920 --> 00:26:48,120
arise and they cease. But when they cease, that cessation is deathless. There

178
00:26:48,120 --> 00:26:58,520
are lack of the lack of arising or the cessation, the voidness that is beyond

179
00:26:58,520 --> 00:27:13,360
arising, that is eternal, doesn't die because it doesn't arise. So when someone

180
00:27:13,360 --> 00:27:28,280
attains Maghapala, Nibhana, when plunges into this state, plunges into

181
00:27:28,280 --> 00:27:36,920
deathlessness. Because all the other, all the things that die have died. There's

182
00:27:36,920 --> 00:27:45,920
no more birth. No more experience is arising. So what's left is called the deathless.

183
00:27:45,920 --> 00:27:52,440
It's like the ultimate. You take a fire and you plunge it into the water and the

184
00:27:52,440 --> 00:28:03,360
fire all goes out. And finally, Nibhana Paryosa means, well, finally, it's Nibhana.

185
00:28:03,360 --> 00:28:09,720
Paryosa, that means the end, the total complete end. That all dumbness, the only

186
00:28:09,720 --> 00:28:16,400
end, right, is Nibhana. Everything else is just a cycle. So we have this cycle of

187
00:28:16,400 --> 00:28:23,000
some sara that we go around and around and around in until we realize the

188
00:28:23,000 --> 00:28:36,320
Nibhana, we realize the Nibhana, that's the Paryosa, the ending. No more becoming.

189
00:28:36,320 --> 00:28:44,920
So just a little food for that. Quite a profound and important suit. That's the

190
00:28:44,920 --> 00:28:53,760
dhamma for today. Thank you for tuning in. Hope that got broadcast. And this will

191
00:28:53,760 --> 00:28:58,480
all, if you missed part of it, it's going to be recorded hopefully and it'll be on

192
00:28:58,480 --> 00:29:08,160
the website. It's that where is it. I don't know where it is. Go to meditation.streamungalow.org

193
00:29:08,160 --> 00:29:15,160
and there's a folder there. There's a link there. Okay, thanks for tuning in.

