1
00:00:00,000 --> 00:00:07,620
Okay, so the day I went to the University of the Dandabada, today we're on to

2
00:00:07,620 --> 00:00:19,480
verse 17, verse 17 and 18 are actually very similar to 15 and 16. So there's

3
00:00:19,480 --> 00:00:24,560
not going to be much to say about the verses themselves that we do have some

4
00:00:24,560 --> 00:00:29,200
interesting stories to tell. So hopefully these will be stories that have

5
00:00:29,200 --> 00:00:40,360
meaning for us. So this verse goes in the tapati, pachatapati, papakari,

6
00:00:40,360 --> 00:00:57,560
ubayatatapati, papam-me-katam-ti, tapati, piyo tapati, dukateng-kato. So meaning is very

7
00:00:57,560 --> 00:01:11,160
similar. Here he burns or boils inside. Here after he burns, he is burnt. The

8
00:01:11,160 --> 00:01:19,280
evil one is burnt in both places. Thinking of thinking, papam-me-katam, I've

9
00:01:19,280 --> 00:01:28,560
done evil. Evil has been done by me, he burns. And here and even more, more so,

10
00:01:28,560 --> 00:01:35,440
be you. He burns, having gone to suffering, having gone to bougatoo, which in

11
00:01:35,440 --> 00:01:42,440
this case is a prayer to places like Collins. In this case referring to hell

12
00:01:42,440 --> 00:01:47,020
because this is talking about the Dandabada. The Dandabada was a cousin of the

13
00:01:47,020 --> 00:01:55,240
buddhas who along with several of the buddhas relatives went forth. And the

14
00:01:55,240 --> 00:01:59,960
story goes that the rest of the people who went forth had, because they had

15
00:01:59,960 --> 00:02:05,560
done great merit and they were the pune-kati or the katapun-yos, as the verses say.

16
00:02:05,560 --> 00:02:10,960
They were ones who have done good deeds in the past. So their minds were in

17
00:02:10,960 --> 00:02:15,000
a good way and they were actually ordaining for the very reason. When they

18
00:02:15,000 --> 00:02:20,240
became monks, they put their hearts into realizations. And they were

19
00:02:20,240 --> 00:02:24,800
actually quite different. For instance, Ananda took a lot of time to become a

20
00:02:24,800 --> 00:02:31,280
monk because his intentions were different. He was interested in remembering

21
00:02:31,280 --> 00:02:34,680
the buddhas teaching and looking after the buddhas. So he didn't have as much time

22
00:02:34,680 --> 00:02:40,120
for meditation. And there was Anorudha who took time as well because he was

23
00:02:40,120 --> 00:02:46,120
caught up in magical powers. But Dandabada was so caught up in his own

24
00:02:46,120 --> 00:02:52,680
baseness of mind that he wasn't able to gain anything except magical powers or

25
00:02:52,680 --> 00:02:58,960
spiritual power from the practice. So as the rest of them eventually made their

26
00:02:58,960 --> 00:03:04,800
way to become at least so Dapana and eventually Arahan. Dandabada only got this

27
00:03:04,800 --> 00:03:11,240
because he was practicing tranquility only. He wasn't interested in inside or

28
00:03:11,240 --> 00:03:15,480
he wasn't putting his heart into it or he wasn't able to because of the evil

29
00:03:15,480 --> 00:03:26,480
in his heart. So as a result, he gained only this peace in the common

30
00:03:26,480 --> 00:03:31,480
mind that allowed him to fly through the air and read people's minds or

31
00:03:31,480 --> 00:03:37,080
whatever. He had some magical powers and he could fly through the air and so he

32
00:03:37,080 --> 00:03:43,720
used these powers to impress the king's son because all the other monks were

33
00:03:43,720 --> 00:03:49,480
being praised and supported by the people. But Dandabada also because he was

34
00:03:49,480 --> 00:03:57,760
having never done anything really good. He didn't have the kind of radiance

35
00:03:57,760 --> 00:04:02,400
that people liked to praise him. So there were no lay people looking after him

36
00:04:02,400 --> 00:04:07,920
and also he had none of the wisdom or attainment as far as enlightenment or

37
00:04:07,920 --> 00:04:12,160
so none of the God was looking for him to teach and so he felt quite jealous.

38
00:04:12,160 --> 00:04:20,880
So he tried to think of who could be that he could bring under his sway and

39
00:04:20,880 --> 00:04:26,400
again power over. And so he thought of the king's son, the king he couldn't touch

40
00:04:26,400 --> 00:04:31,560
because the king was a sotepana and he decided. So he he went after the king's son

41
00:04:31,560 --> 00:04:40,280
and he used his magical powers to make this the prince. We really didn't know

42
00:04:40,280 --> 00:04:45,000
the difference between Kulin evil. I make the prince think that he was enlightened

43
00:04:45,000 --> 00:04:51,360
and he said I am D. Waddad and he brought and he made a picture or he made an

44
00:04:51,360 --> 00:04:58,920
image of himself with snakes and flying through the air and this prince

45
00:04:58,920 --> 00:05:04,520
thought oh this must be a real holy man. And so they were Dandabada was able to get

46
00:05:04,520 --> 00:05:09,840
him under his sway and get him to kill his father and get him to help him to

47
00:05:09,840 --> 00:05:14,960
kill with Dandabada. And so he he did many many evil things. They were Dandabada if

48
00:05:14,960 --> 00:05:20,120
you don't know the story they were Dandabada was a pretty bad guy. And if you read

49
00:05:20,120 --> 00:05:23,160
the jot because you can see that it's something that he's been doing for a long

50
00:05:23,160 --> 00:05:29,040
time. So he tried he he released this wild elephant to kill the Buddha and that

51
00:05:29,040 --> 00:05:34,240
didn't work. The Buddha just saw this wild elephant coming and held up his hand

52
00:05:34,240 --> 00:05:40,600
and sent out loving kindness to the elephant. It's something that you can you

53
00:05:40,600 --> 00:05:44,600
can find if you're practicing meditation you're able to radiate nothing

54
00:05:44,600 --> 00:05:49,480
kindness to animals. It doesn't always work because if you're not as powerful as

55
00:05:49,480 --> 00:05:56,400
the Buddha wouldn't try it on a dog on a rabbit dog that you can feel the

56
00:05:56,400 --> 00:06:04,240
power and so the Buddha was able to extend great power in this regard. And then

57
00:06:04,240 --> 00:06:07,800
he tried to send some of these robbers after the Buddha he tried to drop a rock

58
00:06:07,800 --> 00:06:13,400
on the Buddha. And none of this was working and finally he when he released

59
00:06:13,400 --> 00:06:18,600
this elephant everyone was so scared and so and realized that they were Dandabada

60
00:06:18,600 --> 00:06:24,560
was just a really evil guy. That suddenly suddenly he lost all respect even from

61
00:06:24,560 --> 00:06:32,160
the prince. And so from that point on he had great trouble and so he had to

62
00:06:32,160 --> 00:06:37,200
think of some other way to gain power. And what he thought of was that he would

63
00:06:37,200 --> 00:06:42,960
break the Buddha's Sangha. So he devised this plan that he would ask the

64
00:06:42,960 --> 00:06:48,520
Buddha to instate these rules that were really strict. He said he said to the

65
00:06:48,520 --> 00:06:54,640
Buddha when everyone was in attendance. He came up to the Buddha and said I asked

66
00:06:54,640 --> 00:06:58,640
that you instate these five rules and one of them was that monks should

67
00:06:58,640 --> 00:07:02,840
always have to live in the forest. The monks should always go on arms that monks

68
00:07:02,840 --> 00:07:09,120
should only wear ragrobes and so on. And these were rules that the Buddha had

69
00:07:09,120 --> 00:07:14,320
already stated that certain people could undertake them and others might not

70
00:07:14,320 --> 00:07:19,720
undertake them because they're not intrinsically necessary for the practice

71
00:07:19,720 --> 00:07:26,360
and depending on a person's situation they might actually inhibit progress.

72
00:07:26,360 --> 00:07:30,880
So the Buddha refused to lay these down and David that the winter on saying

73
00:07:30,880 --> 00:07:38,720
all look who's real strict and said it can look who's the lazy one. And so he started

74
00:07:38,720 --> 00:07:44,360
speaking badly of the Buddha. At some point he also asked the Buddha to step

75
00:07:44,360 --> 00:07:49,480
down and said oh you're old now let me run the Sangha and the Buddha said I

76
00:07:49,480 --> 00:07:53,280
wouldn't even give the Sangha to. Sorry put there are mobile on that

77
00:07:53,280 --> 00:08:01,280
alone. A lick spittle like you. A lick spittle I guess is someone who licks spittle.

78
00:08:01,280 --> 00:08:05,480
And he said it's really called David that and it was it was really for the

79
00:08:05,480 --> 00:08:10,160
purpose because the whole the whole way along the Buddha knew that David that

80
00:08:10,160 --> 00:08:13,560
was not a good person. So the question always comes up well why did he let

81
00:08:13,560 --> 00:08:18,160
David that over day and he knew this? And the answer is that he saw from the

82
00:08:18,160 --> 00:08:21,440
beginning where it would lead to and he knew that David that that had been

83
00:08:21,440 --> 00:08:24,800
following him around and there was no way he could escape. If he had let him

84
00:08:24,800 --> 00:08:28,200
be a lay person maybe he would have aroused an army to wipe on all the monks

85
00:08:28,200 --> 00:08:35,040
or so on. Just the pure evil that was in his heart and to the Buddha thought

86
00:08:35,040 --> 00:08:39,280
that he would be a refuge for David that time. So he led all of this happen.

87
00:08:39,280 --> 00:08:43,120
And the reason why he called him a lickspill was to make it very very clear

88
00:08:43,120 --> 00:08:48,960
today with that end to the whole world to bring out to the light finally.

89
00:08:48,960 --> 00:08:52,880
It wasn't beating around the bush because this was his last life. He couldn't play

90
00:08:52,880 --> 00:08:57,280
games anymore. So he finally and he told everyone after David that they got very

91
00:08:57,280 --> 00:09:02,320
angry and left he told everyone he said David that is evil and on the wrong path

92
00:09:02,320 --> 00:09:08,400
and so on. So it of course this kind of when you say this about someone it

93
00:09:08,400 --> 00:09:14,560
brings about a conflict in the heart. So he got very all of his anger and hatred

94
00:09:14,560 --> 00:09:18,920
towards the Buddha that he had built up. After following the Buddha from life after

95
00:09:18,920 --> 00:09:25,600
life it just came boiling up inside and so this is where his burning started.

96
00:09:25,600 --> 00:09:31,400
It was from the point where he decided that he was going to take over the

97
00:09:31,400 --> 00:09:39,680
Sangha and that he lost all of his magical powers. So when he

98
00:09:39,680 --> 00:09:47,360
decided to take over the Sangha he went around asking all the monks or he

99
00:09:47,360 --> 00:09:50,280
called all the monks you know and then all the monks were assembled he said

100
00:09:50,280 --> 00:09:56,320
whoever agrees with these five rules take a ticket so they had these

101
00:09:56,320 --> 00:10:02,160
tickets and some of the new monks who didn't know it right for wrong with that

102
00:10:02,160 --> 00:10:06,120
hey well this guy is really serious really there maybe the

103
00:10:06,120 --> 00:10:11,240
Buddha is not really serious and so they followed after David after went up

104
00:10:11,240 --> 00:10:17,000
the guy as he said and taught them. The Buddha sent Sariputa after him and Mughalana

105
00:10:17,000 --> 00:10:21,320
and Sariputa and Mughalana went up. So it's really kind of funny how it happened

106
00:10:21,320 --> 00:10:26,880
when Sariputa saw when David at the saw Sariputa and Mughalana come if I

107
00:10:26,880 --> 00:10:31,440
know here come the Buddha is too cheap to cypos. See I told you eventually the

108
00:10:31,440 --> 00:10:35,200
whole Sangha will come around and so David at the Sariputa Mughalana didn't

109
00:10:35,200 --> 00:10:39,360
say anything but David at that old they've come to join me and look and then

110
00:10:39,360 --> 00:10:43,320
David at the he said himself up like the Buddha and he said he said I'm going to

111
00:10:43,320 --> 00:10:49,000
lie down now Sariputa Mughalana you teach on my behalf so he lay down and he

112
00:10:49,000 --> 00:10:53,520
because the Buddha would lie down and listen mindfully. David at the

113
00:10:53,520 --> 00:10:59,360
lay down without sleep and so Sariputa Mughalana saw him fall asleep and so

114
00:10:59,360 --> 00:11:02,840
they turned to the monks and they started teaching them Mughalana had also

115
00:11:02,840 --> 00:11:09,400
psychic powers that he used to impress them and Sariputa had some incredible wisdom

116
00:11:09,400 --> 00:11:16,600
and so as a result they were able to hear me able to teach and to convert these

117
00:11:16,600 --> 00:11:22,640
followers that they were that time and as a result allow them to see the truth

118
00:11:22,640 --> 00:11:27,720
and they actually became enlightened. As enlightened beings they all started

119
00:11:27,720 --> 00:11:32,840
but they said okay time but let's go back to where the real teaching is and so

120
00:11:32,840 --> 00:11:39,720
he brought all the monks back to the Buddha. At which point David at his

121
00:11:39,720 --> 00:11:45,920
assistant we'll give it up kicking him in the chest and saying look I told you

122
00:11:45,920 --> 00:11:50,960
to Sariputa Mughalana no good what are you doing trusting them and he kicked

123
00:11:50,960 --> 00:11:57,320
them in the in the chest and at that point David at the got very very sick he was

124
00:11:57,320 --> 00:12:02,960
quite injured by that and as a result of the evil in this heart. At the point

125
00:12:02,960 --> 00:12:08,840
when he got very very sick as often happens he started to realize the evil

126
00:12:08,840 --> 00:12:12,480
that was in his heart. Right if this is where it starts to frighten you when

127
00:12:12,480 --> 00:12:17,320
you can no longer overpower it when you're no longer in charge and you can no

128
00:12:17,320 --> 00:12:21,840
longer run away from the evil deeds. When a person is sick this is often

129
00:12:21,840 --> 00:12:26,680
when good and their bad deeds become their their refuge or their good

130
00:12:26,680 --> 00:12:32,200
refuge or their prison. So in this case it became a prison and this is where he

131
00:12:32,200 --> 00:12:37,840
finally caved in and realized all the evil that he had done. He became quite sick

132
00:12:37,840 --> 00:12:41,840
probably even more so when he realized that the monks and deserted him he was

133
00:12:41,840 --> 00:12:51,040
now all alone but only with a couple of his cronies. And so he asked he

134
00:12:51,040 --> 00:12:55,520
desired he asked his followers that he should like to go and see the Buddha he

135
00:12:55,520 --> 00:13:00,240
was he couldn't even stand up and so he said please bring me to see the Buddha

136
00:13:00,240 --> 00:13:03,280
and they said what are you talking about you only wanted to destroy him when

137
00:13:03,280 --> 00:13:08,800
you were well and leave it at the said it's true that I wanted to destroy him

138
00:13:08,800 --> 00:13:14,080
that the Buddha never harbored any evil towards me and I'm sure he will see me

139
00:13:14,080 --> 00:13:21,320
and so he requested please bring me and so they picked him up in his bed and

140
00:13:21,320 --> 00:13:27,360
they brought him to Jaitalana. Now the story goes that because of how evil he

141
00:13:27,360 --> 00:13:31,360
had been and because of his his his his treachery and breaking apart the

142
00:13:31,360 --> 00:13:34,880
sangha there was no way that he could possibly see the Buddha the Buddha

143
00:13:34,880 --> 00:13:40,400
said they he was told by the monks David that the wants to come to see you

144
00:13:40,400 --> 00:13:45,360
and the Buddha said let him come he'll never see me again and they brought him

145
00:13:45,360 --> 00:13:49,520
and as they were bringing him people said oh now he's he's only a league away

146
00:13:49,520 --> 00:13:55,480
or he's only a mile away and the Buddha said let him come right up to my

147
00:13:55,480 --> 00:14:01,240
doorstep he'll never see me and so they brought him on his chariot all the way to

148
00:14:01,240 --> 00:14:07,480
Jaitalana and they stopped inside the gate to take a break into to wash

149
00:14:07,480 --> 00:14:14,840
and in the pool and David out there saw them washing and so he sat up on his

150
00:14:14,840 --> 00:14:19,200
bed put his feet on the ground and when he put his feet on the ground I guess

151
00:14:19,200 --> 00:14:25,600
thinking that he was going to walk to where the Buddha was his feet sunk into

152
00:14:25,600 --> 00:14:29,000
the earth the earth couldn't even hold his weight and so they say he sunk right

153
00:14:29,000 --> 00:14:36,120
into the earth and all the way to hell or whatever and he died there and the

154
00:14:36,120 --> 00:14:41,760
fires of hell came up and he was told he was purged and and arose in hell in

155
00:14:41,760 --> 00:14:48,000
hell this how destroy goes so something like that when he rose in hell they

156
00:14:48,000 --> 00:14:51,640
see the story of David that to where he has to know because he threat tried to

157
00:14:51,640 --> 00:14:57,400
attack the Buddha and tried to attack the Dhamma which is which is impossible to

158
00:14:57,400 --> 00:15:04,200
shake which is immovable and then the Dhamma of course is internal to

159
00:15:04,200 --> 00:15:07,960
trying to attack that truth and perverted in the form of the Buddha and

160
00:15:07,960 --> 00:15:14,840
the Buddha's teaching he's now transfixed himself and immovable so what that

161
00:15:14,840 --> 00:15:21,200
means is he's encased an iron with a spike going from his head out through his

162
00:15:21,200 --> 00:15:28,320
bottom from the right side out through the left side and and just constant fire

163
00:15:28,320 --> 00:15:36,080
and burning transpixed and immovable for a young, young but the plus sign is

164
00:15:36,080 --> 00:15:41,120
that the Buddha said it would have been worse for him if he hadn't come in

165
00:15:41,120 --> 00:15:45,080
contact with the Buddha hadn't woke him up in this way because at the very

166
00:15:45,080 --> 00:15:51,560
end he he realized and to the end wisdom and understanding so the story goes

167
00:15:51,560 --> 00:15:57,000
that after all of the time that he spends in hell he will be reborn as a

168
00:15:57,000 --> 00:16:02,120
private Buddha particularly so it's a long story it's actually much longer than

169
00:16:02,120 --> 00:16:08,200
that I tried my best to embrace it but it's a story of great importance and

170
00:16:08,200 --> 00:16:14,720
one that we always talk about and remember the story of what not to do if

171
00:16:14,720 --> 00:16:20,560
you become a monk it's a sort of monk that you don't want to do so the lesson

172
00:16:20,560 --> 00:16:27,040
is very much in really in accord with the verses before the person who does

173
00:16:27,040 --> 00:16:34,920
bad deeds bad things happen to me actually burn the word tappati they are

174
00:16:34,920 --> 00:16:43,040
vexed here and vexed in the future they burn really so the idea of burning in

175
00:16:43,040 --> 00:16:46,880
hell has to do with all of the anger that's built up and and the result of

176
00:16:46,880 --> 00:16:52,280
having so much anger inside but the anger inside itself of course is burning

177
00:16:52,280 --> 00:16:59,240
I just another story in regards to how evil people burned inside there was

178
00:16:59,240 --> 00:17:05,200
once a teacher one of one of my teachers teachers in Thailand I read a book

179
00:17:05,200 --> 00:17:13,480
about it he was very much involved in bringing the past and a meditation to

180
00:17:13,480 --> 00:17:20,200
Thailand and also the Abidham mind and many things that Thailand wasn't

181
00:17:20,200 --> 00:17:25,160
wasn't well equipped with so from Burma they brought a lot because in Burma of

182
00:17:25,160 --> 00:17:28,400
course they will study directly study the typical these books come from

183
00:17:28,400 --> 00:17:33,240
Burma that we get here and they would actually read through and study these

184
00:17:33,240 --> 00:17:37,200
words in Thailand very little study of the typical stand so he brought it all

185
00:17:37,200 --> 00:17:41,960
over and that's why now in Thailand they will actually teach Abidham for

186
00:17:41,960 --> 00:17:46,520
example that's where this meditation comes from my teacher was sent as part of

187
00:17:46,520 --> 00:17:54,320
a group to go to Burma to Myanmar to learn meditation but of course there were

188
00:17:54,320 --> 00:18:00,920
people who didn't like this and and actually said things like isn't you know

189
00:18:00,920 --> 00:18:07,120
isn't the Buddha isn't Buddhism in Thailand good enough for you and really

190
00:18:07,120 --> 00:18:10,640
people who didn't like Burma because Burma had fun to attack Thailand anyway

191
00:18:10,640 --> 00:18:15,760
actually the real core of it is this monk was quite popular and what he was

192
00:18:15,760 --> 00:18:19,200
doing was quite popular with the people because he would teach meditation to

193
00:18:19,200 --> 00:18:23,440
ordinary people or he was involved with it and they were teaching Abidham

194
00:18:23,440 --> 00:18:26,480
mind and they were teaching so many new things so people were quite

195
00:18:26,480 --> 00:18:31,760
interested and it was all about jealousy because he started getting power and

196
00:18:31,760 --> 00:18:37,120
he had a powerful position and the other monks who were lying for the same

197
00:18:37,120 --> 00:18:41,080
position or were trying to get to the top position and thought that he might be

198
00:18:41,080 --> 00:18:49,840
getting for it getting it they began to attack him and he and eventually a group

199
00:18:49,840 --> 00:18:57,320
of them apparently including the Sangaraj and a very supreme patriarch although

200
00:18:57,320 --> 00:19:04,560
his his involvementism isn't quite clear they accused him of having sexual

201
00:19:04,560 --> 00:19:08,360
intercourse with a woman and they actually found a woman who was willing to say

202
00:19:08,360 --> 00:19:13,080
that he had sexual intercourse with him and I was reading this book it was a

203
00:19:13,080 --> 00:19:17,600
wonderful very thick book and I'm trying to read through it in Thai and when I came

204
00:19:17,600 --> 00:19:23,280
to this part with the woman you know he they and then they sent him a letter

205
00:19:23,280 --> 00:19:27,920
and said please you should disrupt you've done this terrible thing and we

206
00:19:27,920 --> 00:19:32,880
have a witness and we've gone through the motions of investigating and found

207
00:19:32,880 --> 00:19:37,160
that you're guilty you should disrupt and leave and he said if I if I'd actually

208
00:19:37,160 --> 00:19:41,440
done it why should I disrupt or they said you should disrupt means you should

209
00:19:41,440 --> 00:19:46,640
cease to become a monk and they even called him Num Tha or something like that

210
00:19:46,640 --> 00:19:51,600
much means a monk they still called him by a monk and he said well how

211
00:19:51,600 --> 00:19:55,520
could I be a monk if I've done this why should I why should I stop becoming a

212
00:19:55,520 --> 00:19:59,920
monk because of the wording said you should you should cease to become a

213
00:19:59,920 --> 00:20:03,760
monk and he said well I did it I would obviously it's just a sham that they're

214
00:20:03,760 --> 00:20:08,800
making it but the interesting point that I wanted to make this is in regard to

215
00:20:08,800 --> 00:20:13,840
this woman as you turn the pages and you come to he said eventually they

216
00:20:13,840 --> 00:20:20,080
found the woman and they they talked with her and and after repeated trying

217
00:20:20,080 --> 00:20:25,840
to to get her to tell the truth eventually she came forth and she wrote

218
00:20:25,840 --> 00:20:30,640
this letter and she she came forth by her she came over by herself and she

219
00:20:30,640 --> 00:20:36,800
said I just want to sleep at night she said all this she said no I'm sorry

220
00:20:36,800 --> 00:20:42,240
for what I did I I realized now that a person who does evil deeds

221
00:20:42,240 --> 00:20:48,080
get can can never can never prosper and what I've done is a horrible thing

222
00:20:48,080 --> 00:20:52,640
and it it's given me nightmares ever since I haven't been able to sleep I

223
00:20:52,640 --> 00:21:00,560
just you read it it was such a hard-rending a letter and she said

224
00:21:00,560 --> 00:21:05,120
that monk isn't guilty of anything and I'm writing this letter because I

225
00:21:05,120 --> 00:21:10,800
just want to sleep I don't want to go to hell so this kind of thing in a

226
00:21:10,800 --> 00:21:16,480
person who actually has some wits about them and does not totally

227
00:21:16,480 --> 00:21:22,960
corrupt as David Attalus when you do an evil deed it can really

228
00:21:22,960 --> 00:21:26,480
really hit you very hard and the realization

229
00:21:26,480 --> 00:21:32,240
can realization comes that evil evil really does exist

230
00:21:32,240 --> 00:21:36,080
for people like David Attate they're able to use their force of will as I

231
00:21:36,080 --> 00:21:41,760
said to cover it up most people are not able to do that and as a result

232
00:21:41,760 --> 00:21:46,560
have some semblance of morality they realize when the deed the deed

233
00:21:46,560 --> 00:21:52,320
but eventually it comes to you at any rate for David Attate came when he was

234
00:21:52,320 --> 00:21:57,440
very sick in his mind was weakened by the sickness and so as a result he wasn't

235
00:21:57,440 --> 00:22:02,880
able to cover up the evil deeds and he was forced to see them and then he

236
00:22:02,880 --> 00:22:06,880
realized for himself the badness unfortunately they

237
00:22:06,880 --> 00:22:11,760
what's done is done it's very difficult to do away with evil

238
00:22:11,760 --> 00:22:16,320
deeds you can mitigate the results by doing good deeds

239
00:22:16,320 --> 00:22:20,400
but he certainly didn't have the time to do that even though he tried

240
00:22:20,400 --> 00:22:24,720
so he was forced to burn in this life and burn also in the next time

241
00:22:24,720 --> 00:22:28,560
but because of his goodness the goodness doesn't disappear as well

242
00:22:28,560 --> 00:22:32,880
doesn't disappear either and just like the goodness that we do here even if you

243
00:22:32,880 --> 00:22:36,560
practice meditation and eventually you decide that you can't take it any

244
00:22:36,560 --> 00:22:40,160
more and you can go home the goodness doesn't leave you

245
00:22:40,160 --> 00:22:44,160
and all of the goodness that you do it's like as the Buddha said

246
00:22:44,160 --> 00:22:47,760
then we'll come to that verse it's like putting water into the cup

247
00:22:47,760 --> 00:22:52,400
and so even for David Attate the goodness that he did do

248
00:22:52,400 --> 00:22:55,600
and especially the goodness he did at the end of his life where he

249
00:22:55,600 --> 00:22:59,520
right before he died they say he raised his hands

250
00:22:59,520 --> 00:23:03,680
in homage to the Buddha and said I have clique refuge in the Buddha

251
00:23:03,680 --> 00:23:07,040
place for the Buddha scene maybe the Buddha

252
00:23:07,040 --> 00:23:10,240
forgive me and so on or for the

253
00:23:10,240 --> 00:23:14,880
Buddha take me as someone who has taken refuge

254
00:23:14,880 --> 00:23:19,440
and so as a result of that his mind is actually set in the right way now

255
00:23:19,440 --> 00:23:21,600
it's just going to take time for the

256
00:23:21,600 --> 00:23:25,680
way that we evil beings to work themselves out

257
00:23:25,680 --> 00:23:29,200
and then they say eventually he'll come back and

258
00:23:29,200 --> 00:23:32,160
be able to practice and become free from suffering

259
00:23:32,160 --> 00:23:36,160
so not much to say there and the next verse is going to be the same

260
00:23:36,160 --> 00:23:38,880
just more to think about in another nice story

261
00:23:38,880 --> 00:23:43,280
I'm just going through these in order so there will be a lot more

262
00:23:43,280 --> 00:23:59,200
but more wisdom to come thanks for listening and back to meditation

