1
00:00:00,000 --> 00:00:05,000
Welcome back to our study of the Dhamapanda.

2
00:00:05,000 --> 00:00:10,000
Today we continue on with verse number 65, which reads as follows.

3
00:00:10,000 --> 00:00:16,000
Muhu Tampi, J. We knew Panditang by Yuru Bhasati.

4
00:00:16,000 --> 00:00:26,000
Kipang dhamang wijana ti jyuhasu bharasang daa yatta.

5
00:00:26,000 --> 00:00:29,000
Which means very similar to the last one.

6
00:00:29,000 --> 00:00:34,000
But here we're talking about the opposite.

7
00:00:34,000 --> 00:00:44,000
When you a wise person, Muhu Tampi ti, if a wise person, even for a moment Panditang by Yuru Bhasati,

8
00:00:44,000 --> 00:00:51,000
should attend upon or hang out with a wise person.

9
00:00:51,000 --> 00:00:59,000
Kipang dhamang wijana ti, they're quick to know the, to understand the dhamma.

10
00:00:59,000 --> 00:01:11,000
Jyuhasu bharasang yatta, just as the tongue is quick to know the taste of the soup.

11
00:01:11,000 --> 00:01:20,000
Just as the tongue to the soup of a wise person is able to quickly understand the dhamma of a wise person,

12
00:01:20,000 --> 00:01:26,000
by hanging out with a wise person, even for a short time.

13
00:01:26,000 --> 00:01:29,000
So this, again, a rather short story.

14
00:01:29,000 --> 00:01:31,000
But we can provide a little bit of background.

15
00:01:31,000 --> 00:01:38,000
This is in relation to these, a group of 30 friends,

16
00:01:38,000 --> 00:01:41,000
who the story goes were out,

17
00:01:41,000 --> 00:01:46,000
cavorting in the forest with their wives.

18
00:01:46,000 --> 00:01:55,000
And one of them didn't have a wife, so they hired a courtesan for him, an escort, I guess.

19
00:01:55,000 --> 00:02:04,000
Contact and looked in the yellow pages and found an escort service.

20
00:02:04,000 --> 00:02:09,000
Something like the equivalent of the yellow pages would be to find an escort in those times.

21
00:02:09,000 --> 00:02:16,000
The court is in and invited her along to be his wife for the day.

22
00:02:16,000 --> 00:02:20,000
So they went into the forest and they're playing in the water.

23
00:02:20,000 --> 00:02:28,000
And they took off all their clothes, jump in the water and are sporting around.

24
00:02:28,000 --> 00:02:33,000
And weather in the water, if I'm not making this up, I didn't even know the water.

25
00:02:33,000 --> 00:02:34,000
They were asleep.

26
00:02:34,000 --> 00:02:35,000
I can't remember what it was.

27
00:02:35,000 --> 00:02:37,000
I can't remember why they were negligent.

28
00:02:37,000 --> 00:02:40,000
This courtesan runs off with all their jewels.

29
00:02:40,000 --> 00:02:46,000
It goes picking through their clothes and gathers up all their valuables and runs off into the forest.

30
00:02:46,000 --> 00:02:52,000
When they get back and find their clothes, they're missing all the jewels.

31
00:02:52,000 --> 00:02:57,000
They realize right away what happened and they start.

32
00:02:57,000 --> 00:02:58,000
They send their wives home, I guess.

33
00:02:58,000 --> 00:03:01,000
It doesn't say about that, but they go off into the forest.

34
00:03:01,000 --> 00:03:09,000
These 30 men trying to find this courtesan and get back their jewels.

35
00:03:09,000 --> 00:03:18,000
It just so happens that the Buddha having taught the group of five ascetics in Isipatana,

36
00:03:18,000 --> 00:03:28,000
having turned the wheel in Dhamma, and then having enlightened Yasa and all of his 54 friends.

37
00:03:28,000 --> 00:03:34,000
And having sent the total of 60, Arahan's off into the world to spread the teaching.

38
00:03:34,000 --> 00:03:36,000
No two of them going in one direction.

39
00:03:36,000 --> 00:03:44,000
So he sent them all through India. He went himself off to Rajagaha to spread the Dhamma there.

40
00:03:44,000 --> 00:03:45,000
Oruila.

41
00:03:45,000 --> 00:03:52,000
There he would find Isipat, Oruila Kasaband, the other two Kasapabrasas and eventually go off and teach

42
00:03:52,000 --> 00:03:54,000
Bimbisar and spread his teaching in Rajagaha.

43
00:03:54,000 --> 00:04:02,000
But first, he did something that appears in the texts.

44
00:04:02,000 --> 00:04:10,000
He stopped off where these 30 men were running around the forest trying to find this courtesan.

45
00:04:10,000 --> 00:04:16,000
And he stopped under a tree, which is sat there, and meditation.

46
00:04:16,000 --> 00:04:21,000
And these 30 men wandered upon him.

47
00:04:21,000 --> 00:04:23,000
And they looked and they said, oh, he looks like a wise person.

48
00:04:23,000 --> 00:04:26,000
They bet he can give us some good advice.

49
00:04:26,000 --> 00:04:32,000
And so they wandered up to him and paid respect, gave him respect.

50
00:04:32,000 --> 00:04:35,000
And then asked him, a vendor will serve.

51
00:04:35,000 --> 00:04:38,000
You see, it looked like he'd been sitting here in the forest for a while.

52
00:04:38,000 --> 00:04:42,000
And we'd wonder if you happen to have seen this woman.

53
00:04:42,000 --> 00:04:49,000
And we're looking for a woman who stole our jewels.

54
00:04:49,000 --> 00:04:54,000
I'm not exactly the sort of advice that most people would ask the Buddha for.

55
00:04:54,000 --> 00:04:57,000
But the Buddha gives them something much more valuable.

56
00:04:57,000 --> 00:05:00,000
He looks at them.

57
00:05:00,000 --> 00:05:10,000
And he knows, the reason he's there is because he knows the ability or the qualities of these 30 men

58
00:05:10,000 --> 00:05:17,000
that are highly cultivated individuals, even though they don't realize it.

59
00:05:17,000 --> 00:05:27,000
And so he asks them, he says, what do you think is better that you should search after a woman?

60
00:05:27,000 --> 00:05:31,000
I think you should search after yourself.

61
00:05:31,000 --> 00:05:39,000
Search after yourselves.

62
00:05:39,000 --> 00:05:42,000
I'm not exactly the advice they were looking for.

63
00:05:42,000 --> 00:05:47,000
And of course, this is the Buddha.

64
00:05:47,000 --> 00:05:50,000
And so they take in what he's saying.

65
00:05:50,000 --> 00:05:59,000
And of course, it's much more important, really, even all the jewels that we lost are not really all that valuable in comparison to finding oneself.

66
00:05:59,000 --> 00:06:03,000
Which actually sounds kind of non-Buddhist if you think about it.

67
00:06:03,000 --> 00:06:07,000
But it's all about giving up yourself, right?

68
00:06:07,000 --> 00:06:09,000
Giving up the idea of self.

69
00:06:09,000 --> 00:06:19,000
But it's a lot just made of this passage, actually, for those people who think that the Buddha actually taught that there is a self or had some idea of there being self.

70
00:06:19,000 --> 00:06:23,000
But it's quite clear from the Buddha's teaching that's not the case.

71
00:06:23,000 --> 00:06:28,000
And this is simply a teaching instrument.

72
00:06:28,000 --> 00:06:32,000
How to get the people, how to get them to look for something that's truly valuable.

73
00:06:32,000 --> 00:06:39,000
And they're probably working news, or whatever would have been, they were Brahmin religion.

74
00:06:39,000 --> 00:06:41,000
So they would have believed in a self.

75
00:06:41,000 --> 00:06:47,000
And so it's a good way to get people's attention to say, well, why don't you seek out what is truly important?

76
00:06:47,000 --> 00:06:49,000
Seek out yourselves.

77
00:06:49,000 --> 00:06:54,000
Come to know yourselves and learn about yourselves.

78
00:06:54,000 --> 00:06:59,000
And they say, well, let's sit down and I'll help you find yourself.

79
00:06:59,000 --> 00:07:06,000
And have them sit down and he teaches them the dhamma, and they all became enlightened right there.

80
00:07:06,000 --> 00:07:09,000
I don't know how many people have ever become enlightened listening to my dhamma.

81
00:07:09,000 --> 00:07:14,000
I'm probably not the same category as both.

82
00:07:14,000 --> 00:07:15,000
But it can't happen.

83
00:07:15,000 --> 00:07:24,000
You know, sometimes listening to a talk, if you're clearly aware of the experience,

84
00:07:24,000 --> 00:07:30,000
and the time that you're listening to the talk, it's useful because one, you can't go anywhere.

85
00:07:30,000 --> 00:07:34,000
And two, the teaching constantly reminds you of good things.

86
00:07:34,000 --> 00:07:47,000
Three, the company of the fellow audience members are all ideally on the same path.

87
00:07:47,000 --> 00:07:57,000
And so there are good qualities to the experience of listening to the dhamma that you don't get elsewhere.

88
00:07:57,000 --> 00:08:04,000
I mean, that timing is actually quite, can be quite easy to realize the teachings,

89
00:08:04,000 --> 00:08:13,000
realize the truth, and to find the truth, to understand reality.

90
00:08:13,000 --> 00:08:22,000
And so they did these 30 young men, became our hunts, and decided to become,

91
00:08:22,000 --> 00:08:28,000
and obviously decided to become monks and proceeded along with the Buddha.

92
00:08:28,000 --> 00:08:33,000
I'm not sure where they went, actually, maybe they went off on their own somewhere.

93
00:08:33,000 --> 00:08:39,000
But the other monks were talking about this later, and this is how this verse came out.

94
00:08:39,000 --> 00:08:43,000
They said, well, this is kind of crazy. You know, all of us would take so much work,

95
00:08:43,000 --> 00:08:47,000
and maybe these monks weren't yet our hunts, but they were, or maybe they were,

96
00:08:47,000 --> 00:08:54,000
but how long it took them to practice over the days and weeks and months and even years to become our hunt.

97
00:08:54,000 --> 00:09:00,000
But these 30 young men who were earlier in the same day,

98
00:09:00,000 --> 00:09:11,000
are frolicking in negligence, you know, in indolence.

99
00:09:11,000 --> 00:09:16,000
And then suddenly they can become our hunts, and how is that?

100
00:09:16,000 --> 00:09:19,000
And the Buddha came in and asked them, what are you talking about?

101
00:09:19,000 --> 00:09:23,000
And they said, I'm talking about this, and then the Buddha said, well,

102
00:09:23,000 --> 00:09:31,000
and then he actually told one of the Jatakas, based on this.

103
00:09:31,000 --> 00:09:34,000
He reminded them of Jatakas once in a past life.

104
00:09:34,000 --> 00:09:41,000
He had taught these 30 men were coming to kill him, and he was able to

105
00:09:41,000 --> 00:09:49,000
help them realize the ways, and as a result they gave up their evil

106
00:09:49,000 --> 00:09:53,000
and became disciples of it.

107
00:09:53,000 --> 00:10:00,000
But the meaning behind it is that they had a lot coming into this.

108
00:10:00,000 --> 00:10:03,000
They called Upanishaya. They brought from their past lives.

109
00:10:03,000 --> 00:10:05,000
They all bring this from our past lives.

110
00:10:05,000 --> 00:10:09,000
We come into this life with qualities, both positive and negative.

111
00:10:09,000 --> 00:10:15,000
The fact that we're born human means we definitely still are born with

112
00:10:15,000 --> 00:10:20,000
unwholesome qualities. Everyone, even the Bodhisattva is born as a human being.

113
00:10:20,000 --> 00:10:24,000
I still have greed, still have anger, still have delusion.

114
00:10:24,000 --> 00:10:28,000
Without those we wouldn't be born again as a human.

115
00:10:28,000 --> 00:10:32,000
We have negative qualities, but we also carry all the positive qualities,

116
00:10:32,000 --> 00:10:36,000
all that we've cultivated in our past lives, all the good work that we've done

117
00:10:36,000 --> 00:10:41,000
in Buddhism and outside of Buddhism, helping other people,

118
00:10:41,000 --> 00:10:50,000
being moral and ethical and a high-minded and spiritually-minded

119
00:10:50,000 --> 00:10:55,000
and being inclined toward the spiritual life inside Buddhism, within Buddhism

120
00:10:55,000 --> 00:10:59,000
and outside of it as well, all the wisdom that we've gained,

121
00:10:59,000 --> 00:11:01,000
all the learning that we've gained from lifetime to lifetime.

122
00:11:01,000 --> 00:11:05,000
It doesn't leave us except insofar as our memories go.

123
00:11:05,000 --> 00:11:11,000
The memories, of course, fade away as they always do over time,

124
00:11:11,000 --> 00:11:15,000
but the quality of the mind changes.

125
00:11:15,000 --> 00:11:19,000
Yes, we're all aware of it, but it's...

126
00:11:19,000 --> 00:11:22,000
And so the Bodhisattva said, it's because of we knew,

127
00:11:22,000 --> 00:11:25,000
because these men are we knew, they're people who understand.

128
00:11:25,000 --> 00:11:28,000
People who have this resonance in them.

129
00:11:28,000 --> 00:11:32,000
They're the potential to resonate with the dhamma.

130
00:11:32,000 --> 00:11:37,000
Just as the tongue has the potential, a person without their tongue damaged,

131
00:11:37,000 --> 00:11:42,000
their sense of taste damaged, unless their sense of taste is damaged,

132
00:11:42,000 --> 00:11:49,000
they have the potential to receive whatever the taste of the soup is.

133
00:11:49,000 --> 00:11:54,000
Quite quickly, without any effort, unlike the spoon which we talked about last week,

134
00:11:54,000 --> 00:11:57,000
the spoon no matter how long it touches the soup, it never tastes anything.

135
00:11:57,000 --> 00:11:59,000
It's all about one's potential.

136
00:11:59,000 --> 00:12:06,000
So the point is that the dhamma is good in and of itself.

137
00:12:06,000 --> 00:12:12,000
There's nothing wrong with the dhamma just because we can't become enlightened off of it.

138
00:12:12,000 --> 00:12:16,000
It's up to our quality, since we talked about last week.

139
00:12:16,000 --> 00:12:22,000
If we don't have those qualities, we spend all the time we want with enlightened beings.

140
00:12:22,000 --> 00:12:30,000
And if our deans and our speech and our thoughts are unwholesome,

141
00:12:30,000 --> 00:12:34,000
and they are not inclined towards enlightenment, they will never get there.

142
00:12:34,000 --> 00:12:40,000
And seriously, it's like being the spoon with the soup.

143
00:12:40,000 --> 00:12:51,000
But the quality of a wise person is that they quickly understand the truth.

144
00:12:51,000 --> 00:12:55,000
Even when they just taught the Buddha's teachings from the books,

145
00:12:55,000 --> 00:12:57,000
they immediately get the meaning of it.

146
00:12:57,000 --> 00:12:59,000
They very quickly get the meaning of it.

147
00:12:59,000 --> 00:13:04,000
Some people when they hear the Buddha's teaching are confused and disinterested,

148
00:13:04,000 --> 00:13:07,000
turned off even repelled by the Buddha's teaching.

149
00:13:07,000 --> 00:13:11,000
When they hear about it being for the giving up,

150
00:13:11,000 --> 00:13:17,000
the discarding of central desire and giving up of the whole world.

151
00:13:17,000 --> 00:13:21,000
I think of this as a ridiculous, horrible thing, a bad teaching.

152
00:13:21,000 --> 00:13:26,000
Teaching that is built on repression and so on and so on.

153
00:13:26,000 --> 00:13:30,000
They're the misunderstandings that arise.

154
00:13:30,000 --> 00:13:34,000
Come from a different sort of person.

155
00:13:34,000 --> 00:13:36,000
Divas are a personality.

156
00:13:36,000 --> 00:13:39,000
Those people who are inclined towards the world.

157
00:13:39,000 --> 00:13:42,000
They're inclined towards craving and attachment.

158
00:13:42,000 --> 00:13:48,000
They aren't able to understand the teaching.

159
00:13:48,000 --> 00:13:51,000
And so that's a common question.

160
00:13:51,000 --> 00:13:54,000
How do these monks in the time of the Buddha hear the Buddha giving a talk?

161
00:13:54,000 --> 00:13:56,000
And some people just immediately became around.

162
00:13:56,000 --> 00:13:58,000
Very quickly, like, sorry, Buddha.

163
00:13:58,000 --> 00:14:01,000
So he did not even have to hear from the Buddha.

164
00:14:01,000 --> 00:14:08,000
He heard from Asaji this one of the first five monks within the five ascetics.

165
00:14:08,000 --> 00:14:12,000
And he just heard half of a stanza.

166
00:14:12,000 --> 00:14:14,000
And he became a Swatapana.

167
00:14:14,000 --> 00:14:17,000
Just from hearing half of the stanza.

168
00:14:17,000 --> 00:14:41,000
He was a Swatapana.

169
00:14:41,000 --> 00:14:44,000
He was ready for it.

170
00:14:44,000 --> 00:14:47,000
Very sensitive tongue.

171
00:14:47,000 --> 00:14:49,000
Quickly, he was able to realize the teaching.

172
00:14:49,000 --> 00:14:54,000
So what does this mean for all of us?

173
00:14:54,000 --> 00:14:59,000
It means that realistically, first of all, we should never fault the teaching.

174
00:14:59,000 --> 00:15:01,000
It doesn't bring us anything.

175
00:15:01,000 --> 00:15:07,000
If we can find fault with the teaching in an intellectual sense,

176
00:15:07,000 --> 00:15:09,000
this is what we should give up the teaching.

177
00:15:09,000 --> 00:15:13,000
But when we find the teaching doesn't work, we have to ask ourselves one of two things.

178
00:15:13,000 --> 00:15:17,000
The teaching is effective, or are we unable to understand the teaching?

179
00:15:17,000 --> 00:15:20,000
Are we unable to understand the Buddha and the practice?

180
00:15:20,000 --> 00:15:22,000
There's these two ways.

181
00:15:22,000 --> 00:15:26,000
When you see enlightened beings, and this is the key,

182
00:15:26,000 --> 00:15:32,000
if your teacher is full of greed and full of anger and full of delusion,

183
00:15:32,000 --> 00:15:36,000
maybe it's my teacher's fault, or the teaching is impure.

184
00:15:36,000 --> 00:15:39,000
But when you know that your teacher lives like the Buddha,

185
00:15:39,000 --> 00:15:45,000
we know that we have the Buddha, and when we see that people who practice meditation

186
00:15:45,000 --> 00:15:50,000
are able to free themselves from these things,

187
00:15:50,000 --> 00:15:54,000
then we have to understand that it's most likely our own fault.

188
00:15:54,000 --> 00:15:59,000
We ourselves are just lacking in the requisites to quickly understand the teaching.

189
00:15:59,000 --> 00:16:01,000
The second part is not to be discouraged.

190
00:16:01,000 --> 00:16:04,000
Not to be quickly discouraged because after a week of practicing,

191
00:16:04,000 --> 00:16:06,000
I'm still not enlightened.

192
00:16:06,000 --> 00:16:10,000
Here I have been sitting for ten minutes, and I don't feel anything yet.

193
00:16:10,000 --> 00:16:12,000
They're kind of saying.

194
00:16:12,000 --> 00:16:15,000
To understand that part of this is the cultivation.

195
00:16:15,000 --> 00:16:17,000
Tolkien, Tolkien, the Buddha said,

196
00:16:17,000 --> 00:16:20,000
dropped by drop, or little by little.

197
00:16:20,000 --> 00:16:22,000
The goodness doesn't come to you all at once.

198
00:16:22,000 --> 00:16:25,000
It's something that you cultivate little by little every moment.

199
00:16:25,000 --> 00:16:27,000
Every moment that we're mindful.

200
00:16:27,000 --> 00:16:30,000
We're more that we're aware of something that we're clearly aware of.

201
00:16:30,000 --> 00:16:33,000
Experience, just as it is without liking,

202
00:16:33,000 --> 00:16:36,000
or judgment, or attachment, or conceit, or views,

203
00:16:36,000 --> 00:16:38,000
that moment is blessed.

204
00:16:38,000 --> 00:16:40,000
That moment is great.

205
00:16:40,000 --> 00:16:44,000
If we can cultivate those moments again and again continuously,

206
00:16:44,000 --> 00:16:46,000
eventually becomes habitual,

207
00:16:46,000 --> 00:16:49,000
and that's when we're able to understand the number.

208
00:16:49,000 --> 00:16:52,000
For example, when you learn about Buddhism,

209
00:16:52,000 --> 00:16:55,000
before you practice meditation, it's a lot less clear

210
00:16:55,000 --> 00:16:58,000
than after you actually sit down and practice the teaching

211
00:16:58,000 --> 00:17:00,000
and then approach the same text.

212
00:17:00,000 --> 00:17:03,000
You find that you understand them so much better and so much clearer

213
00:17:03,000 --> 00:17:06,000
and so much more fruitfully.

214
00:17:06,000 --> 00:17:13,000
And your studies are far greater fruit because of your practice.

215
00:17:13,000 --> 00:17:15,000
So it's not to be discouraged.

216
00:17:15,000 --> 00:17:21,000
We shouldn't think that we're useless.

217
00:17:21,000 --> 00:17:25,000
We shouldn't think we're the spoon.

218
00:17:25,000 --> 00:17:28,000
We should always look at our own attitude as the point

219
00:17:28,000 --> 00:17:33,000
that we shouldn't be like Udayi, this guy who just hung out

220
00:17:33,000 --> 00:17:35,000
with the Buddha and pretended to be wise,

221
00:17:35,000 --> 00:17:37,000
but never really listened to what the Buddha had to say

222
00:17:37,000 --> 00:17:41,000
or put it into practice or tried to understand it even.

223
00:17:41,000 --> 00:17:43,000
We should always try to put the teachings into practice,

224
00:17:43,000 --> 00:17:46,000
try to understand the more and try to apply them to our own life,

225
00:17:46,000 --> 00:17:48,000
not to just keep them up in our head,

226
00:17:48,000 --> 00:17:51,000
or chant them or teach them to others

227
00:17:51,000 --> 00:17:52,000
or think about them,

228
00:17:52,000 --> 00:17:55,000
but we should actually try every day

229
00:17:55,000 --> 00:17:58,000
and in every aspect of our lives to incorporate them into our lives.

230
00:17:58,000 --> 00:17:59,000
How can we be more generous?

231
00:17:59,000 --> 00:18:01,000
How can we be more moral?

232
00:18:01,000 --> 00:18:03,000
How can we be more focused and concentrated

233
00:18:03,000 --> 00:18:05,000
and clear in our minds?

234
00:18:05,000 --> 00:18:07,000
We're cultivating goodness,

235
00:18:07,000 --> 00:18:09,000
the three types of goodness

236
00:18:09,000 --> 00:18:13,000
and cultivating morality and concentration in wisdom.

237
00:18:13,000 --> 00:18:16,000
Most importantly, using mindfulness to cultivate it all

238
00:18:16,000 --> 00:18:18,000
because mindfulness is what clears the mind

239
00:18:18,000 --> 00:18:21,000
and allows you to see the difference between wholesome and unwholesome

240
00:18:21,000 --> 00:18:28,000
and allows you to keep the mind from wandering in unwholesome ways,

241
00:18:28,000 --> 00:18:31,000
focus the mind and see things clearly.

242
00:18:31,000 --> 00:18:35,000
It's what leads us to niband it, freedom from suffering.

243
00:18:35,000 --> 00:18:38,000
It's just the meaning is it's all on us.

244
00:18:38,000 --> 00:18:41,000
Up to us whether we're going to be the spoon or the tongue.

245
00:18:41,000 --> 00:18:42,000
So the analogy ends there.

246
00:18:42,000 --> 00:18:44,000
It doesn't mean that if you're a spoon, you're always going to be a spoon.

247
00:18:44,000 --> 00:18:46,000
It just means don't be a spoon.

248
00:18:46,000 --> 00:18:50,000
Be a tongue and try to receive the teachings.

249
00:18:50,000 --> 00:18:52,000
Cultivate knowledge and understanding,

250
00:18:52,000 --> 00:18:54,000
so you can understand the teachings.

251
00:18:54,000 --> 00:18:58,000
And don't think that just because you've memorized all the Buddha's teachings

252
00:18:58,000 --> 00:19:00,000
or you know how to practice meditation,

253
00:19:00,000 --> 00:19:04,000
that you're a Buddhist or that you're practicing the Buddha's teaching.

254
00:19:04,000 --> 00:19:06,000
You should ever take that for granted.

255
00:19:06,000 --> 00:19:09,000
You find them the more you practice and the longer that you.

256
00:19:15,000 --> 00:19:18,000
Associate with Buddhism with the Buddha's teaching.

257
00:19:18,000 --> 00:19:21,000
The deeper your understanding of the same teachings gets.

258
00:19:21,000 --> 00:19:24,000
And you realize that there are still things that you're missing.

259
00:19:24,000 --> 00:19:31,000
It gets more and more refined and your sense of taste becomes more and more refined

260
00:19:31,000 --> 00:19:35,000
until you can taste the dhamma, which has the taste of freedom.

261
00:19:35,000 --> 00:19:40,000
So simple verse, simple story, simple meaning.

262
00:19:40,000 --> 00:19:45,000
You have to cultivate knowledge and understanding.

263
00:19:45,000 --> 00:19:48,000
Understanding of the Buddha's teaching requires,

264
00:19:48,000 --> 00:19:53,000
has to be ready for it or has to be in its condition to understand the truth.

265
00:19:53,000 --> 00:19:56,000
So that's the story for tonight.

266
00:19:56,000 --> 00:19:58,000
Teaching on the dhamma pada.

267
00:19:58,000 --> 00:20:01,000
Thank you for tuning in and for coming out.

268
00:20:01,000 --> 00:20:03,000
All those who are here locally.

269
00:20:03,000 --> 00:20:19,000
I wish you all to find true peace, happiness and freedom from suffering.

