1
00:00:00,000 --> 00:00:24,400
just

2
00:00:24,400 --> 00:00:27,400
I really shouldn't be this.

3
00:00:33,400 --> 00:00:36,400
Okay, I'm going to try to read me.

4
00:00:36,400 --> 00:00:38,400
I'm going to read me again.

5
00:00:38,400 --> 00:00:42,400
Turn the video quality down a little bit.

6
00:00:43,400 --> 00:00:46,400
I just pretend that that was the problem.

7
00:00:46,400 --> 00:01:13,400
Thank you.

8
00:01:13,400 --> 00:01:27,100
So, yes, so this Mahasi Sayadam.

9
00:01:27,100 --> 00:01:35,500
He was talking about how sometimes it's difficult for us to listen to profound teachings

10
00:01:35,500 --> 00:01:47,460
as we are attached to excitement to entertainment becomes a chore to have to listen to the

11
00:01:47,460 --> 00:01:54,460
Mahasi Sayadam, we are not careful.

12
00:01:54,460 --> 00:02:03,460
We are going to learn part of our success and our continuation of our ability to be patient,

13
00:02:03,460 --> 00:02:08,460
not only with our everything, but with our desire.

14
00:02:08,460 --> 00:02:14,460
It's all about me, me, I, I, that I need, of course, to be able to bring us down.

15
00:02:14,460 --> 00:02:19,460
It is often about giving up your own desires and favor of the community.

16
00:02:19,460 --> 00:02:24,460
Favor of the progress is the goal to the community.

17
00:02:24,460 --> 00:02:27,460
That's number five.

18
00:02:27,460 --> 00:02:32,460
Number six is seclusion.

19
00:02:32,460 --> 00:02:40,460
So, the Buddhist in the context of the monastic system, he said the monks should live in the forest.

20
00:02:40,460 --> 00:02:50,460
There's something specifically about being in the forest and it's, of course, appealing.

21
00:02:50,460 --> 00:03:01,460
The natural setting that calms the mind down keeps the mind peaceful.

22
00:03:01,460 --> 00:03:05,460
But I think probably more important is the seclusion.

23
00:03:05,460 --> 00:03:12,460
That doesn't mean seclusion necessarily from each other, although that is part of it.

24
00:03:12,460 --> 00:03:18,460
I mean seclusion mostly from outside influences.

25
00:03:18,460 --> 00:03:27,460
This is again shutting down Facebook, closing your door, finding a quiet spot.

26
00:03:27,460 --> 00:03:37,460
You can have it in this case, a digital community, just finding a way to isolate yourself

27
00:03:37,460 --> 00:03:42,460
from the world, keep the community isolated.

28
00:03:42,460 --> 00:03:51,460
In the sense that you're not at the same time engaging in the world, not bringing the world into the community.

29
00:03:51,460 --> 00:04:05,460
I mean, having some, having a system set up to keep the community focused.

30
00:04:05,460 --> 00:04:09,460
That also means seclusion as a community that we shouldn't be.

31
00:04:09,460 --> 00:04:17,460
The one thing about Second Life that I don't know how many of you have noticed, but it's really, if you're not mindful,

32
00:04:17,460 --> 00:04:24,460
it's really easy to lose time chatting with other people, just in a chat conversation.

33
00:04:24,460 --> 00:04:31,460
And it's almost as though you can't get out of it.

34
00:04:31,460 --> 00:04:38,460
And you find yourself chatting.

35
00:04:38,460 --> 00:05:01,460
I mean, it's just an example of how easy it is to get caught up in socializing.

36
00:05:01,460 --> 00:05:10,460
And we somehow feel that there's an expectation of the social.

37
00:05:10,460 --> 00:05:17,460
It's great to come here to the Buddha Center sometimes and just see someone with their avatar sitting on the mat.

38
00:05:17,460 --> 00:05:24,460
It's kind of funny because they're not actually sitting on the mat, but you know, the symbol of it.

39
00:05:24,460 --> 00:05:30,460
A person sitting alone on the mat is here up in the hall.

40
00:05:30,460 --> 00:05:38,460
It's almost as though saying to yourself, committing to yourself to sit there.

41
00:05:38,460 --> 00:05:40,460
So that's an important perspective.

42
00:05:40,460 --> 00:05:45,460
And the community, ironically, is the ability to give each other space.

43
00:05:45,460 --> 00:05:52,460
If the community promotes giving people their space, they're trying to be alone.

44
00:05:52,460 --> 00:06:05,460
It's the time to be in a meeting, in a community helping each other, but recognizing that true change has to be affected by oneself.

45
00:06:05,460 --> 00:06:13,460
And it's best affected once when there's the tools and the moment I try to best affect it alone.

46
00:06:13,460 --> 00:06:33,460
Number seven, I put down this mindfulness, but it actually is a specific type of mindfulness.

47
00:06:33,460 --> 00:06:37,460
It's mindfulness of other people.

48
00:06:37,460 --> 00:06:51,460
It's mindfulness of the needs of others, mindfulness of the comfort and the hospitality.

49
00:06:51,460 --> 00:07:05,460
So another way of thinking of it is hospitality, but what it really means is acting in such a way as to allow people to make use of the needs.

50
00:07:05,460 --> 00:07:09,460
Acting in such a way that good people want to join the community.

51
00:07:09,460 --> 00:07:11,460
This is what the Buddha said.

52
00:07:11,460 --> 00:07:16,460
He said, so that those who are of good behaviors who haven't yet come to the community,

53
00:07:16,460 --> 00:07:21,460
who want to come to a comfortable coming, who climbs the road.

54
00:07:21,460 --> 00:07:30,460
And acting in such a way being mindful, to the extent that those who are already a part of the community,

55
00:07:30,460 --> 00:07:36,460
and are properly behaved and practicing well, that they want to stay.

56
00:07:36,460 --> 00:07:41,460
If they don't run away or get bored, I'll be fatigued.

57
00:07:41,460 --> 00:07:48,460
I've left this community for many years, because I have nothing to do with the community that it is much more to do with.

58
00:07:48,460 --> 00:07:57,460
My own going off and doing otherworldly things.

59
00:07:57,460 --> 00:08:07,460
But it's a very important aspect of the community that will mind full of each other.

60
00:08:07,460 --> 00:08:15,460
That we act in such a way as to be considerate of others.

61
00:08:15,460 --> 00:08:29,460
That we are mindful of the structure of the community, mindful of the setup of our center.

62
00:08:29,460 --> 00:08:38,460
The physical setup, mindful of the institution, the organization, the management, the office being mindful of all these things.

63
00:08:38,460 --> 00:08:45,460
Mindful of our attitudes, mindful of who is the representative who is the industry of M.

64
00:08:45,460 --> 00:09:01,460
They call ambassadors, emissaries, the people who greet and provide information with the facilities that we have.

65
00:09:01,460 --> 00:09:09,460
The teachers that we have, et cetera, are they beneficial?

66
00:09:09,460 --> 00:09:17,460
Do they need the needs of a membership? Are they welcoming? Are they open?

67
00:09:17,460 --> 00:09:21,460
Of interest, those who might want to try.

68
00:09:21,460 --> 00:09:27,460
And I think you could even, that why I kept it as mindfulness is because I think you could easily argue that

69
00:09:27,460 --> 00:09:35,460
the most important factor in this is our own actual true awareness, meditative awareness.

70
00:09:35,460 --> 00:09:42,460
So if we don't have that, no Buddhist worth their salt is going to be at all interested in such a community.

71
00:09:42,460 --> 00:09:56,460
If we don't have the mindfulness and the clarity of my nature, we understand ourselves our emotions and our interactions with the world around us.

72
00:09:56,460 --> 00:10:00,460
People will quickly come to see that it's just a superficial community.

73
00:10:00,460 --> 00:10:07,460
People will look like this, but they're still in the match, but actually they're not in the same place.

74
00:10:07,460 --> 00:10:17,460
Our mindfulness is important in terms of cultivating communities, in terms of the preservation of our community.

75
00:10:17,460 --> 00:10:27,460
And our actual quality of our mind, the quality, the quality that we're developing and focusing on.

76
00:10:27,460 --> 00:10:37,460
So there you go. Those are seven things that the Buddha said, lead to the preservation of the community.

77
00:10:37,460 --> 00:10:40,460
Not it's the crime.

78
00:10:40,460 --> 00:10:52,460
And that's my talk for today. Thank you all for tuning in. If you have questions or comments, criticisms, and happy to hear them all.

79
00:11:10,460 --> 00:11:39,460
Thank you for having me.

80
00:11:39,460 --> 00:12:02,460
Thank you.

81
00:12:02,460 --> 00:12:27,460
Yeah, I should be back like Saturday. This is, I'm up for it.

82
00:12:27,460 --> 00:12:50,460
I think I'll turn off the video or stop it there, but if you have questions, I'm happy to answer.

83
00:12:50,460 --> 00:13:15,460
Yeah, the Buddha said, people are like cloth, monks, these robes that we wear.

84
00:13:15,460 --> 00:13:24,460
Traditionally, we would go around garbage piles, finding cloth, and cloth was like plastic today.

85
00:13:24,460 --> 00:13:39,460
The plastic of today is like the cloth at that time, so there's lots of cloth around, so they'd take strips of cloth, but if the cloth was part of the cloth was rotten, you wouldn't throw away the whole cloth.

86
00:13:39,460 --> 00:13:47,460
He would tear off that part, and keep the rest. That's a good way to think of people.

87
00:13:47,460 --> 00:14:03,460
All the bad in them tear off. It might seem kind of dishonest in terms of ignoring people's bad habits, but to some extent it's important to ignore people's people.

88
00:14:03,460 --> 00:14:26,460
It's important to maintain harmony, maybe not ignore exactly, but be able to put it aside.

89
00:14:26,460 --> 00:14:52,460
Thanks for watching.

90
00:16:26,460 --> 00:16:54,460
So I'm going to head off and I'm going to put this here all next week.

91
00:16:54,460 --> 00:16:58,460
So, I'm going to go down and see what happens.

92
00:16:58,460 --> 00:17:03,460
So, we're going to go down and see what happens.

93
00:17:03,460 --> 00:17:10,460
So, we're going to go down and see what happens.

94
00:17:10,460 --> 00:17:34,460
So, we're going to go down and see what happens.

