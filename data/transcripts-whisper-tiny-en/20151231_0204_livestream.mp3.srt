1
00:00:00,000 --> 00:00:16,800
Okay, good evening everyone, broadcasting live, December 30th, audio first, because tonight

2
00:00:16,800 --> 00:00:31,600
we will be studying the Damapada again, so we'll do a video recording and then we'll switch

3
00:00:31,600 --> 00:00:46,400
to live video broadcast test, hello and welcome back to our study of the Damapada.

4
00:00:46,400 --> 00:00:53,400
Tonight we continue on with verse 122, which reads as follows,

5
00:01:16,400 --> 00:01:27,400
which is almost exactly the same as the last verse, which if you'll remember was in regards

6
00:01:27,400 --> 00:01:39,400
to not thinking little evil deeds, thinking it won't come to me, so here is the opposite.

7
00:01:39,400 --> 00:01:50,400
Ma wa mani eta, pune yasa, don't look down upon or don't underestimate.

8
00:01:50,400 --> 00:02:01,400
Pune ya, which is goodness, thinking namandang agamesity, it won't come to me.

9
00:02:01,400 --> 00:02:20,400
For uda vindunipati na uda kumbu pipurati, a water pot becomes full drop by drop, with drops of water, with drops of water falling.

10
00:02:20,400 --> 00:02:46,400
Thiroturati pune yasa, a wise person becomes full of goodness, token to can be agino.

11
00:02:46,400 --> 00:03:00,400
Even though it might be drop by drop, one becomes full with goodness, or by through goodness, full of goodness.

12
00:03:00,400 --> 00:03:08,400
Anyway, here we have a story.

13
00:03:08,400 --> 00:03:16,400
The story goes that the Buddha was talking about different types of giving.

14
00:03:16,400 --> 00:03:30,400
It seems there are many stories of giving, giving being a fundamental religious practice that you might say gets up.

15
00:03:30,400 --> 00:03:39,400
Some people might say gets a little too much coverage in Buddhist circles.

16
00:03:39,400 --> 00:03:46,400
I think it's fair to say that often it's because there's a concern for getting.

17
00:03:46,400 --> 00:03:50,400
People talk about giving because they're concerned with getting.

18
00:03:50,400 --> 00:03:59,400
Often monasteries or meditation centers or monks would like to get things or any need of things.

19
00:03:59,400 --> 00:04:11,400
So there's kind of attention there because you have to accept that while there is a need.

20
00:04:11,400 --> 00:04:16,400
It seems a little bit suspicious and I've often commented on this.

21
00:04:16,400 --> 00:04:25,400
A little bit, not suspicious, a little bit.

22
00:04:25,400 --> 00:04:42,400
Uncomfortable to talk about how good it is to give when you actually mean it would be good if we could receive, for example.

23
00:04:42,400 --> 00:04:46,400
Anyway, giving is a spiritual practice.

24
00:04:46,400 --> 00:04:48,400
It's a good spiritual practice.

25
00:04:48,400 --> 00:04:50,400
There's nothing wrong with giving.

26
00:04:50,400 --> 00:05:05,400
We recently gave a large donation, our group, many of you who are watching this video perhaps, to a children's home in Florida.

27
00:05:05,400 --> 00:05:08,400
That was so good.

28
00:05:08,400 --> 00:05:22,400
My stepfather got involved, and my mother got involved, and everyone was so happy.

29
00:05:22,400 --> 00:05:25,400
It was a way to really bring people together and it has the potential to bring people together again.

30
00:05:25,400 --> 00:05:27,400
We do this again next time.

31
00:05:27,400 --> 00:05:29,400
We'll even bring more people together.

32
00:05:29,400 --> 00:05:32,400
That's what this story in the Dhampa is about.

33
00:05:32,400 --> 00:05:41,400
The Buddha talks about how some people give themselves are charitable or kind, but don't encourage other people to do good things.

34
00:05:41,400 --> 00:05:45,400
It just doesn't just have to do with giving any good deed.

35
00:05:45,400 --> 00:05:48,400
You could say the same about meditation or morality.

36
00:05:48,400 --> 00:06:01,400
If you're at moral yourself, but you don't encourage people or let people, other people know about your views on ethical acts or ethical matters issues.

37
00:06:01,400 --> 00:06:13,400
Another person might encourage others, but not do good deeds themselves, and then one person does neither, and another person does both.

38
00:06:13,400 --> 00:06:20,400
Both does good deeds and encourage others.

39
00:06:20,400 --> 00:06:24,400
His teaching is something that we've talked about before.

40
00:06:24,400 --> 00:06:36,400
If you do good deeds, but don't encourage others to do good deeds, then you'll get great reward yourself.

41
00:06:36,400 --> 00:06:40,400
Your life will get better, but you'll be alone.

42
00:06:40,400 --> 00:06:42,400
You won't be surrounded by other people.

43
00:06:42,400 --> 00:06:52,400
If you become a good person yourself, but you haven't made friends with or worked together with other people to do good deeds.

44
00:06:52,400 --> 00:07:03,400
You'll be lacking in friendship and companionship, which is very important, both in the world and in spiritual practice.

45
00:07:03,400 --> 00:07:25,400
So, as the Buddha was teaching, it turns out there was a certain Pandita Puri said, a wise man who, having heard this dhamma desa na, thought to himself, well, then, that's what I should do, rather than just be generous and kind and do good deeds on my own.

46
00:07:25,400 --> 00:07:33,400
I should bring people together to do good deeds. This is what we did for Florida. This is why we do these things. Bring people together.

47
00:07:33,400 --> 00:07:38,400
Don't just do good deeds yourself. Do them together as a group.

48
00:07:38,400 --> 00:07:59,400
So, he went to the Buddha, and he asked, or maybe he went to the Buddha, and said to Bhante, I would like to invite monks for lunch.

49
00:07:59,400 --> 00:08:26,400
Now, how many monks were there? There were many, many monks. I don't know if it says you're how many monks, but it was a lot.

50
00:08:26,400 --> 00:08:39,400
It was the number of monks. This has been Jaitavana, so there were probably hundreds, if not, more thousands, even maybe. I don't know. Of course, they always offer etc.

51
00:08:39,400 --> 00:08:49,400
But lots, lots of monks, more than anyone poor, family, or even ordinary family, would be able to care for it.

52
00:08:49,400 --> 00:08:59,400
So, he did this completely on faith that he could get people to join him in this great deed.

53
00:08:59,400 --> 00:09:07,400
And it was really sort of a powerful determination on his part to be so bold.

54
00:09:07,400 --> 00:09:25,400
And so, he went into the village, into the city, actually. It's how many went, maybe banging a drum, or he went to...

55
00:09:25,400 --> 00:09:37,400
He went to his own village, maybe not the city, but he went around saying, everyone, maybe banging a drum, or something.

56
00:09:37,400 --> 00:09:43,400
I have invited the congregation of monks, presided over by the Buddha for the meal tomorrow.

57
00:09:43,400 --> 00:09:53,400
Come, give what you can, provide as much as your means permits. Let us do all the cooking in one place, and give arms in common.

58
00:09:53,400 --> 00:10:00,400
So, rather than the ordinary way, which would be, oh, well, I'll invite a couple of monks to my house.

59
00:10:00,400 --> 00:10:03,400
Do this good deed or that good deed.

60
00:10:03,400 --> 00:10:07,400
The idea was, let's all do one good deed together.

61
00:10:07,400 --> 00:10:16,400
So, that we share in the deed, and that our karma is intertwined in the sense that we have similar futures together.

62
00:10:16,400 --> 00:10:23,400
We move forward and upward together.

63
00:10:23,400 --> 00:10:31,400
And so, the people were, for the most part, overjoyed, but there was one rich guy who was not pleased.

64
00:10:31,400 --> 00:10:41,400
And hearing what this guy was saying, preaching, talking about his intentions, became quite angry.

65
00:10:41,400 --> 00:10:53,400
With the thought thus, rather than look at this guy, rather than invite people, invite monks, based on his own means.

66
00:10:53,400 --> 00:11:06,400
How dare he assume that other people should help him fulfill his wishes, fulfill his intentions.

67
00:11:06,400 --> 00:11:18,400
I mean, how dare he just assume or push us or try to persuade us all to do something that really is on him.

68
00:11:18,400 --> 00:11:27,400
It's his burden. He's the idiot who bit off more than he can chew, and now he's coming and begging and pleading us.

69
00:11:27,400 --> 00:11:30,400
He is very upset about this.

70
00:11:30,400 --> 00:11:43,400
A rich man, this guy was rich as well. So, when the guy came to his door, he took, it's funny, he took two fingers in his thumb, I guess.

71
00:11:43,400 --> 00:11:48,400
Or maybe three fingers in his thumb. Maybe it was like this.

72
00:11:48,400 --> 00:11:55,400
And grabbed just as much rice would fit there and dropped it in this guy's ball.

73
00:11:55,400 --> 00:12:04,400
And did the same with up beans, and juggery, or sugar, and salt, or whatever whatever he gave.

74
00:12:04,400 --> 00:12:10,400
He gave just a little bit because he was giving ingredients.

75
00:12:10,400 --> 00:12:19,400
And so, that's significant because he came to be known as the cat foot, rich man of the cat foot.

76
00:12:19,400 --> 00:12:27,400
The guy, because I think, because this looks like a cat foot, cats have three paws, maybe.

77
00:12:27,400 --> 00:12:34,400
The three paws, and the thumb. Somehow they'd had a meaning at the time.

78
00:12:34,400 --> 00:12:44,400
So, his name was, he became known as cat foot or belalapata.

79
00:12:44,400 --> 00:12:56,400
And so, the wise man, the guy who was organizing this, took the offerings, and he placed them apart.

80
00:12:56,400 --> 00:13:05,400
So, he took the other, everyone else's offerings and just put them all together, but the rich man's offerings, he kept them separate.

81
00:13:05,400 --> 00:13:15,400
And, and guarded them individually. And the rich man was sort of watching this guy.

82
00:13:15,400 --> 00:13:23,400
And as he saw this behavior that he hadn't mixed them in with everything else, that he had kept them separate.

83
00:13:23,400 --> 00:13:28,400
He asked one of his servants, he told him on a service to go and spy on him and see what he was doing.

84
00:13:28,400 --> 00:13:34,400
What was up with all this?

85
00:13:34,400 --> 00:13:41,400
And the man followed after this guy, the organizer, and saw that what he was doing,

86
00:13:41,400 --> 00:13:55,400
is he was taking like one grain or a few grains of the rich guy's rice, and putting a little bit in each and every thing that they were cooking.

87
00:13:55,400 --> 00:14:07,400
So, distributing it evenly among the product.

88
00:14:07,400 --> 00:14:12,400
And so, this guy goes back and tells the rich man who's quite puzzled and doesn't know what's going on.

89
00:14:12,400 --> 00:14:17,400
He can't see what angle this guy is playing, but he starts to get suspicious.

90
00:14:17,400 --> 00:14:30,400
Because some, his, his, this guy has, this guy deliberately paid special attention to the rich man's offering,

91
00:14:30,400 --> 00:14:35,400
which was, was ridiculously small.

92
00:14:35,400 --> 00:14:40,400
And so, he starts to get, he starts to get this thought, and this might, he knows.

93
00:14:40,400 --> 00:14:48,400
That guy knows that my offering was, was miniscule, was meaningless, was an insult.

94
00:14:48,400 --> 00:14:53,400
And he's going to tell, he's going to tell the Buddha, or he's going to announce everyone,

95
00:14:53,400 --> 00:15:00,400
and he's going to announce that I gave such and such, that I gave very, very little.

96
00:15:00,400 --> 00:15:07,400
So, he takes, what he does, he takes a knife, because it gets like a sword or a knife or something,

97
00:15:07,400 --> 00:15:12,400
he takes it in his robe, and he goes to the monastery.

98
00:15:12,400 --> 00:15:16,400
The next morning, or no, he goes to the place where they're going to feed the monks.

99
00:15:16,400 --> 00:15:23,400
The next morning with the thought, if he does, as soon as he opens his mouth, if he starts to talk about me,

100
00:15:23,400 --> 00:15:25,400
I'm going to kill him.

101
00:15:25,400 --> 00:15:32,400
That's a good, that's what, how, how awful this guy really was.

102
00:15:32,400 --> 00:15:45,400
So, he's quite odd that he, because later on he actually comes to realize the dhamma, but,

103
00:15:45,400 --> 00:15:47,400
is this the story?

104
00:15:47,400 --> 00:15:52,400
It's hard to know what's going on underneath, but he gets very, very angry,

105
00:15:52,400 --> 00:15:56,400
and he must really think that this guy is out to get him.

106
00:15:56,400 --> 00:16:09,400
That this guy has set him up and is kind of disappointed or upset,

107
00:16:09,400 --> 00:16:16,400
or feels self-righteous about the fact that the rich man didn't give what he could give.

108
00:16:16,400 --> 00:16:22,400
Anyway, so he's there with his knife, sort of standing up to the side,

109
00:16:22,400 --> 00:16:27,400
and what does this guy, this organizer do?

110
00:16:27,400 --> 00:16:31,400
He proclaims to the Buddha that all the people who gathered there

111
00:16:31,400 --> 00:16:38,400
had given something for each of them, for every bit of the food,

112
00:16:38,400 --> 00:16:45,400
and he said, please, Venerable, sir.

113
00:16:45,400 --> 00:16:51,400
May everyone here receive a rich reward, because they all,

114
00:16:51,400 --> 00:16:55,400
they all gave according to their ability.

115
00:16:55,400 --> 00:17:03,400
The rich man heard this, and he was, he was quite moved by this,

116
00:17:03,400 --> 00:17:09,400
by the generous, and he realized that he had had this whole thing misunderstood,

117
00:17:09,400 --> 00:17:17,400
that he misunderstood this man's intentions, and started to realize that actually this guy was a really good person.

118
00:17:17,400 --> 00:17:23,400
And it's funny, the translation says, if I don't ask him to pardon me,

119
00:17:23,400 --> 00:17:29,400
punishment from the king will fall up on my head, but I'm pretty sure that's not what it says.

120
00:17:29,400 --> 00:17:33,400
You see, there's something called Deva Danda,

121
00:17:33,400 --> 00:17:39,400
which Deva means angel, but it can also mean king.

122
00:17:39,400 --> 00:17:44,400
The king is considered to be a god among men, or a deity among men,

123
00:17:44,400 --> 00:17:52,400
so it could mean king, but it really doesn't, because he's talking about actually talking about his head,

124
00:17:52,400 --> 00:17:56,400
which, well, there's no reason that the king would punish him,

125
00:17:56,400 --> 00:18:02,400
but it's the angels that will meet out punishment by splitting his head into pieces,

126
00:18:02,400 --> 00:18:04,400
which is a common phrase.

127
00:18:04,400 --> 00:18:10,400
There was this belief that if you did a terribly heinous crime,

128
00:18:10,400 --> 00:18:19,400
the angels would meet out punishment on your head by splitting it into pieces.

129
00:18:19,400 --> 00:18:24,400
And so he, actually, this rich man, who was very arrogant and conceded,

130
00:18:24,400 --> 00:18:29,400
and was very much on the wrong path, meant it his way is just standing there,

131
00:18:29,400 --> 00:18:32,400
listening and watching and looking and seeing.

132
00:18:32,400 --> 00:18:38,400
What a wonderful thing he had missed out on, because of his selfishness and his greed,

133
00:18:38,400 --> 00:18:41,400
and his stinginess.

134
00:18:41,400 --> 00:18:47,400
And so he bowed down at the feet of this organizing fellow,

135
00:18:47,400 --> 00:18:53,400
and said, please forgive me, sir.

136
00:18:53,400 --> 00:18:57,400
And the man looked down at him and said, well, what's,

137
00:18:57,400 --> 00:19:00,400
why are you asking me forgiveness?

138
00:19:00,400 --> 00:19:02,400
Totally.

139
00:19:02,400 --> 00:19:11,400
I'm oblivious and having never thought anything bad about the man.

140
00:19:11,400 --> 00:19:15,400
So he was totally surprised as to what was wrong.

141
00:19:15,400 --> 00:19:21,400
And the treasure explained to him, sorry, the rich man explained to him that,

142
00:19:21,400 --> 00:19:23,400
you know, really he was angry.

143
00:19:23,400 --> 00:19:29,400
He gave that gift out of anger, and he purposely gave very, very little.

144
00:19:29,400 --> 00:19:36,400
He actually at this point felt kind of sad that he hadn't given something significant.

145
00:19:36,400 --> 00:19:41,400
And the Buddha saw this, standing right there, sitting there, probably eating.

146
00:19:41,400 --> 00:19:46,400
And asked what was happening, what was this, what was going on.

147
00:19:46,400 --> 00:19:53,400
And so they told him, and the rich man said, well, I was awful, really,

148
00:19:53,400 --> 00:20:01,400
and I feel like I missed a really good opportunity to take part in something wholesome.

149
00:20:01,400 --> 00:20:04,400
And the Buddha said, oh no, you didn't, actually.

150
00:20:04,400 --> 00:20:09,400
Well, the Buddha basically said, don't ever think of a good deed as a trifle.

151
00:20:09,400 --> 00:20:14,400
In the end you gave, in the end you were, you relented.

152
00:20:14,400 --> 00:20:18,400
You could have said no, you could have given nothing.

153
00:20:18,400 --> 00:20:23,400
But in the end you gave something, you were kind, you were generous.

154
00:20:23,400 --> 00:20:25,400
You gave something that belonged to you.

155
00:20:25,400 --> 00:20:31,400
You didn't have no duty or no obligation to you.

156
00:20:31,400 --> 00:20:34,400
So even that is a good deed, the Buddha said.

157
00:20:34,400 --> 00:20:38,400
And he used it as an opportunity to tell this person.

158
00:20:38,400 --> 00:20:50,400
It's kind of, I would bet that there's the fact that this was a rich and sort of well-to-do,

159
00:20:50,400 --> 00:20:56,400
affluent individual played a part in being very fairly positive about this.

160
00:20:56,400 --> 00:20:59,400
Because you could have warned the guy, you know, don't do evil.

161
00:20:59,400 --> 00:21:01,400
Be careful, and yes, you've done a bad deed.

162
00:21:01,400 --> 00:21:09,400
But rich people don't tend, rich people who have wealth, who have power,

163
00:21:09,400 --> 00:21:14,400
what a attract more flies with honey is the same.

164
00:21:14,400 --> 00:21:17,400
Such people you often have to be careful with.

165
00:21:17,400 --> 00:21:24,400
It's funny, the people who are most virtuous and spiritually uplifted,

166
00:21:24,400 --> 00:21:28,400
you tend to be able to treat them the harshest.

167
00:21:28,400 --> 00:21:31,400
They tend to be able to be the hardest on them, and they can take it,

168
00:21:31,400 --> 00:21:34,400
and it's useful for them and helpful for them.

169
00:21:34,400 --> 00:21:37,400
But for people who are rich, you're better off to go,

170
00:21:37,400 --> 00:21:44,400
and people who are spoiled perhaps, you're better off to go positive.

171
00:21:44,400 --> 00:21:55,400
Nonetheless, there is a point here that any amount of goodness should not be disregarded.

172
00:21:55,400 --> 00:22:05,400
Should not be underestimated.

173
00:22:05,400 --> 00:22:09,400
And so then he taught this verse.

174
00:22:09,400 --> 00:22:12,400
And this is like the last one, well even more so than the last one.

175
00:22:12,400 --> 00:22:16,400
I think this is really an awesome verse for us to remember as meditators,

176
00:22:16,400 --> 00:22:21,400
as Buddhists, as spiritual practitioners.

177
00:22:21,400 --> 00:22:26,400
Because it's easy to get discouraged when you think of your problems,

178
00:22:26,400 --> 00:22:33,400
when you think of your goals, when you think of spiritual goals.

179
00:22:33,400 --> 00:22:36,400
It's easy to think, well I'll never be like that, I'll never get there.

180
00:22:36,400 --> 00:22:40,400
I'll never free myself from this.

181
00:22:40,400 --> 00:22:47,400
And we become discouraged before we've been tried, because it seems like a mountain.

182
00:22:47,400 --> 00:22:51,400
But even a mountain, when you get up close, it's made of rock.

183
00:22:51,400 --> 00:22:58,400
It's something that you can take apart.

184
00:22:58,400 --> 00:23:01,400
All good things come step by step, right?

185
00:23:01,400 --> 00:23:04,400
The journey of a thousand miles starts with the first step.

186
00:23:04,400 --> 00:23:08,400
It's a very important concept, because it's really how it works.

187
00:23:08,400 --> 00:23:10,400
Goodness doesn't disappear.

188
00:23:10,400 --> 00:23:13,400
Our good deeds don't just disappear.

189
00:23:13,400 --> 00:23:23,400
If every good success has to come from the deeds, from individual good deeds.

190
00:23:23,400 --> 00:23:31,400
And so whether it be generosity, and therefore the result being an affluent sort,

191
00:23:31,400 --> 00:23:40,400
or high status, all the good mundane things that come from being generous,

192
00:23:40,400 --> 00:23:45,400
whether it be good for having good friends, all these good results,

193
00:23:45,400 --> 00:23:54,400
they all come from small deeds, from small acts of kindness.

194
00:23:54,400 --> 00:23:57,400
Because our lives are this way.

195
00:23:57,400 --> 00:24:04,400
Our lives work moment by moment, and every moment we can do wholesomeness,

196
00:24:04,400 --> 00:24:08,400
we can do a good deed, we can do a bad deed.

197
00:24:08,400 --> 00:24:13,400
Every moment there's an opportunity, whether it be through generosity, or morality,

198
00:24:13,400 --> 00:24:17,400
or simply through meditation.

199
00:24:17,400 --> 00:24:21,400
So our ethics, we don't have to have lofty ethics.

200
00:24:21,400 --> 00:24:25,400
Ethics aren't about the principles themselves.

201
00:24:25,400 --> 00:24:29,400
It's about the acts, it's about our state of mind,

202
00:24:29,400 --> 00:24:33,400
and our momentary interaction with the world around it.

203
00:24:33,400 --> 00:24:41,400
And meditation likewise, enlightenment isn't something that you jump to,

204
00:24:41,400 --> 00:24:45,400
or you fall into, or you break open.

205
00:24:45,400 --> 00:24:48,400
It's not an instantaneous thing.

206
00:24:48,400 --> 00:24:53,400
It comes step by step and moment by moment.

207
00:24:53,400 --> 00:24:56,400
Every moment we have the opportunity to do good deeds,

208
00:24:56,400 --> 00:25:00,400
every moment that we're mindful, when you're mindful of the footlifting,

209
00:25:00,400 --> 00:25:06,400
when you're mindful of the foot moving, when you're mindful of the stomach rising.

210
00:25:06,400 --> 00:25:09,400
That one moment is a wholesome mind.

211
00:25:09,400 --> 00:25:12,400
When you see that you're angry and you're mind yourself,

212
00:25:12,400 --> 00:25:16,400
this is anger, it's not me, it's not mind, it's just anger.

213
00:25:16,400 --> 00:25:20,400
When you see something and you're reminders of it, see it.

214
00:25:20,400 --> 00:25:23,400
Every time you do a good deed like this,

215
00:25:23,400 --> 00:25:26,400
when you're kind to someone, when you're generous,

216
00:25:26,400 --> 00:25:29,400
when you're compassionate, when you're helpful,

217
00:25:29,400 --> 00:25:33,400
when you're patient, when you're at good frustrated,

218
00:25:33,400 --> 00:25:35,400
and impatient with people,

219
00:25:35,400 --> 00:25:39,400
and then you remind yourself, impatient or frustrated.

220
00:25:39,400 --> 00:25:43,400
And as a result, become more patient.

221
00:25:43,400 --> 00:25:47,400
The power of this is not to be underestimated,

222
00:25:47,400 --> 00:25:50,400
because this is how true power comes about,

223
00:25:50,400 --> 00:25:52,400
not in a single balance,

224
00:25:52,400 --> 00:25:55,400
not by one great act.

225
00:25:55,400 --> 00:25:58,400
You don't break through to become a good person.

226
00:25:58,400 --> 00:26:01,400
A goodness comes moment by moment,

227
00:26:01,400 --> 00:26:04,400
if something anyone can do, because it's here and now,

228
00:26:04,400 --> 00:26:06,400
and it's moment by moment.

229
00:26:06,400 --> 00:26:09,400
This is what's so awesome about this path,

230
00:26:09,400 --> 00:26:12,400
this practice, Buddhism, meditation.

231
00:26:12,400 --> 00:26:15,400
It's not something locked here or difficult.

232
00:26:15,400 --> 00:26:18,400
As difficult as it is, it's just moments.

233
00:26:18,400 --> 00:26:22,400
Something you can do any moment, every moment,

234
00:26:22,400 --> 00:26:26,400
that you're doing a good deed.

235
00:26:26,400 --> 00:26:29,400
It's one step closer to the goal.

236
00:26:29,400 --> 00:26:33,400
It's one step higher, step up on the ladder,

237
00:26:33,400 --> 00:26:35,400
bringing you higher, making you happier,

238
00:26:35,400 --> 00:26:42,400
bringing peace and clarity and freedom to you,

239
00:26:42,400 --> 00:26:44,400
and to those around you.

240
00:26:44,400 --> 00:26:46,400
The main thing about this story that isn't really,

241
00:26:46,400 --> 00:26:48,400
doesn't really come out in the verse,

242
00:26:48,400 --> 00:26:50,400
is about encouraging others,

243
00:26:50,400 --> 00:26:54,400
so not only should we encourage others to be generous,

244
00:26:54,400 --> 00:26:56,400
but even more importantly,

245
00:26:56,400 --> 00:26:58,400
we should encourage others to be ethical,

246
00:26:58,400 --> 00:27:01,400
and spiritual, quantum contemplative,

247
00:27:01,400 --> 00:27:04,400
should encourage others to meditate,

248
00:27:04,400 --> 00:27:06,400
to better themselves,

249
00:27:06,400 --> 00:27:09,400
to not just be content, to be an ordinary person,

250
00:27:09,400 --> 00:27:11,400
to get old sick and die,

251
00:27:11,400 --> 00:27:13,400
and have the world forget about them,

252
00:27:13,400 --> 00:27:16,400
but to move forward and upward,

253
00:27:16,400 --> 00:27:19,400
and to make use of the time that they have,

254
00:27:19,400 --> 00:27:22,400
and the energy that they have,

255
00:27:22,400 --> 00:27:25,400
and the life that they have,

256
00:27:25,400 --> 00:27:32,400
to become, to put it to some use,

257
00:27:32,400 --> 00:27:35,400
to make it, to have, and bring it,

258
00:27:35,400 --> 00:27:39,400
to have value of true value.

259
00:27:39,400 --> 00:27:42,400
So, in encouraging others,

260
00:27:42,400 --> 00:27:47,400
we gain this extra support

261
00:27:47,400 --> 00:27:50,400
of having good people around this.

262
00:27:50,400 --> 00:27:52,400
We don't encourage others in goodness,

263
00:27:52,400 --> 00:27:54,400
while you can be as good as you want.

264
00:27:54,400 --> 00:27:56,400
It's not sure who you'll be surrounded by.

265
00:27:56,400 --> 00:27:59,400
You'll have to be surrounded by people who complain about it,

266
00:27:59,400 --> 00:28:03,400
because they don't have the sense of the importance of goodness,

267
00:28:03,400 --> 00:28:10,400
and they haven't had the taste of the fruit of goodness.

268
00:28:10,400 --> 00:28:14,400
So you end up often being surrounded by people who don't understand goodness,

269
00:28:14,400 --> 00:28:16,400
who don't appreciate goodness,

270
00:28:16,400 --> 00:28:18,400
who don't have the clarity and the happiness

271
00:28:18,400 --> 00:28:20,400
that comes from goodness,

272
00:28:20,400 --> 00:28:24,400
and therefore surrounded often by miserable people.

273
00:28:24,400 --> 00:28:26,400
We don't want that.

274
00:28:26,400 --> 00:28:29,400
So, it's very important to what we do good deeds,

275
00:28:29,400 --> 00:28:32,400
to encourage other people in it,

276
00:28:32,400 --> 00:28:37,400
and to work together to perform good deeds.

277
00:28:37,400 --> 00:28:39,400
So, anyway,

278
00:28:39,400 --> 00:28:42,400
that's the Dhammapada verse tonight.

279
00:28:42,400 --> 00:28:46,400
One more teaching on good things, bad things,

280
00:28:46,400 --> 00:28:48,400
spiritual things.

281
00:28:48,400 --> 00:28:50,400
Thank you all for tuning in,

282
00:28:50,400 --> 00:28:52,400
pushing you off.

283
00:28:52,400 --> 00:28:54,400
Good practice,

284
00:28:54,400 --> 00:28:58,400
and success in practicing together in a good way.

285
00:28:58,400 --> 00:29:00,400
Thank you. Good night.

286
00:29:08,400 --> 00:29:10,400
Okay.

287
00:29:10,400 --> 00:29:14,400
So, now,

288
00:29:14,400 --> 00:29:18,400
we can move to a live broadcast.

289
00:29:21,400 --> 00:29:23,400
Try to do some stuff.

290
00:29:23,400 --> 00:29:37,400
Okay, we're going to go live video.

291
00:29:42,400 --> 00:29:45,400
Good evening, everyone.

292
00:29:45,400 --> 00:29:51,400
For those of you who are watching this

293
00:29:51,400 --> 00:29:55,400
or joining us live on YouTube,

294
00:29:55,400 --> 00:29:59,400
we've just recorded a Dhammapada video,

295
00:29:59,400 --> 00:30:04,400
and now we can answer some questions

296
00:30:04,400 --> 00:30:06,400
if anybody has them,

297
00:30:06,400 --> 00:30:17,400
or give us a good night.

298
00:30:17,400 --> 00:30:19,400
I didn't mention that.

299
00:30:19,400 --> 00:30:22,400
Did I mention about the digital poly reader?

300
00:30:22,400 --> 00:30:23,400
I don't think I did.

301
00:30:23,400 --> 00:30:24,400
No.

302
00:30:24,400 --> 00:30:27,400
The digital poly reader doesn't really work anymore.

303
00:30:27,400 --> 00:30:31,400
It does, but it may not soon.

304
00:30:31,400 --> 00:30:33,400
What changed?

305
00:30:33,400 --> 00:30:37,400
Firefox Mozilla started being

306
00:30:37,400 --> 00:30:40,400
somewhat

307
00:30:40,400 --> 00:30:44,400
a little fascist dictatorial.

308
00:30:44,400 --> 00:30:47,400
Not there.

309
00:30:47,400 --> 00:30:49,400
They're add-on policy.

310
00:30:49,400 --> 00:30:53,400
So, now we have to actually submit our add-ons

311
00:30:53,400 --> 00:30:54,400
for their review,

312
00:30:54,400 --> 00:30:56,400
or Firefox won't even install them.

313
00:30:56,400 --> 00:31:00,400
It's kind of ridiculous.

314
00:31:00,400 --> 00:31:05,400
I had to upload all my add-ons to their site.

315
00:31:05,400 --> 00:31:10,400
Many people have contacted me.

316
00:31:10,400 --> 00:31:13,400
Don't blame me.

317
00:31:13,400 --> 00:31:15,400
But there's a way to get around it.

318
00:31:15,400 --> 00:31:17,400
For those of you who are wondering,

319
00:31:17,400 --> 00:31:19,400
I just had to, just tonight,

320
00:31:19,400 --> 00:31:22,400
I had to get around it again.

321
00:31:22,400 --> 00:31:25,400
I think you have to go into your configuration

322
00:31:25,400 --> 00:31:28,400
and switch something to false.

323
00:31:28,400 --> 00:31:30,400
It's something about signatures.

324
00:31:30,400 --> 00:31:31,400
You can read about it.

325
00:31:31,400 --> 00:31:39,400
There's sites that tell you how to do it.

326
00:31:39,400 --> 00:31:44,400
Would it be any easier to use Google instead of Firefox?

327
00:31:44,400 --> 00:31:45,400
No.

328
00:31:45,400 --> 00:31:47,400
It's integrated with Firefox.

329
00:31:47,400 --> 00:31:54,400
I made a decision because of how open Mozilla was.

330
00:31:54,400 --> 00:31:58,400
That Firefox was easier and more powerful.

331
00:31:58,400 --> 00:32:02,400
But now it seems like that was a bit of a mistake.

332
00:32:14,400 --> 00:32:17,400
I don't see any questions just to comment

333
00:32:17,400 --> 00:32:21,400
that someone's having trouble with the indentation timer.

334
00:32:21,400 --> 00:32:23,400
Not working.

335
00:32:23,400 --> 00:32:28,400
Well, if anyone wants to troubleshoot the website

336
00:32:28,400 --> 00:32:31,400
or reprogram it, feel free.

337
00:32:31,400 --> 00:32:35,400
I think it's at this point as is

338
00:32:35,400 --> 00:32:38,400
until we get some real developers.

339
00:32:38,400 --> 00:32:40,400
Yeah.

340
00:32:40,400 --> 00:32:43,400
I wonder if there was someone who was working on

341
00:32:43,400 --> 00:32:45,400
a meditation app for iPhone for you.

342
00:32:45,400 --> 00:32:48,400
Did that ever be finished?

343
00:32:48,400 --> 00:32:49,400
No.

344
00:32:49,400 --> 00:32:54,400
I didn't hear back at all about that.

345
00:32:54,400 --> 00:32:57,400
It was kind of slick, but they were doing it.

346
00:32:57,400 --> 00:32:58,400
Yeah.

347
00:32:58,400 --> 00:33:07,400
You remember seeing the little prettier?

348
00:33:07,400 --> 00:33:08,400
Okay.

349
00:33:08,400 --> 00:33:12,400
Well, maybe we'll call it a night.

350
00:33:12,400 --> 00:33:14,400
Thank you all for tuning in.

351
00:33:14,400 --> 00:33:19,400
I'm sorry, there's not much to see here.

352
00:33:19,400 --> 00:33:22,400
We'll have a demo part of the video up soon.

353
00:33:22,400 --> 00:33:25,400
Let's see how tomorrow.

354
00:33:25,400 --> 00:33:27,400
Tomorrow is the, oh, tomorrow here is something.

355
00:33:27,400 --> 00:33:30,400
We could have a meditate into the new year.

356
00:33:30,400 --> 00:33:31,400
How about that?

357
00:33:31,400 --> 00:33:33,400
That would be nice.

358
00:33:33,400 --> 00:33:36,400
Tomorrow we'll have broadcast at nine,

359
00:33:36,400 --> 00:33:38,400
and then after the broadcast,

360
00:33:38,400 --> 00:33:43,400
we'll just meditate until midnight together.

361
00:33:43,400 --> 00:33:46,400
That sounds good.

362
00:33:46,400 --> 00:33:48,400
Thank you, Dante.

363
00:33:48,400 --> 00:33:49,400
Okay.

364
00:33:49,400 --> 00:33:51,400
So everyone come on tomorrow.

365
00:33:51,400 --> 00:33:54,400
9.30 or 9.

366
00:33:54,400 --> 00:33:55,400
We'll have the broadcast.

367
00:33:55,400 --> 00:33:57,400
We'll go all the way to midnight.

368
00:33:57,400 --> 00:34:05,400
We won't broadcast till midnight, but we'll do it on our meditation site.

369
00:34:05,400 --> 00:34:07,400
You don't have to meditate for all the hours,

370
00:34:07,400 --> 00:34:09,400
but you can meditate on and off.

371
00:34:09,400 --> 00:34:14,400
And at midnight, we'll all come back on the meditation site

372
00:34:14,400 --> 00:34:16,400
and just say, happy new year.

373
00:34:16,400 --> 00:34:19,400
Wish each other happy new year.

374
00:34:19,400 --> 00:34:22,400
Just an excuse to get together.

375
00:34:22,400 --> 00:34:26,400
Get together and do good deals, so.

376
00:34:26,400 --> 00:34:28,400
Okay, good night.

377
00:34:28,400 --> 00:34:29,400
Good night, Dante.

378
00:34:29,400 --> 00:34:30,400
Thank you.

379
00:34:30,400 --> 00:34:40,400
Thank you.

