1
00:00:00,000 --> 00:00:10,000
So good evening, everyone. Welcome to our live broadcast, November 21st, 2015.

2
00:00:16,000 --> 00:00:20,000
Apparently, last night's Damapada video is too long.

3
00:00:20,000 --> 00:00:25,000
We told that no one has time to sit through a 47-minute video.

4
00:00:25,000 --> 00:00:31,000
And normally, comments like this will go unanswered and discussed.

5
00:00:31,000 --> 00:00:36,000
I'm not reading all comments, so please don't expect me to reply to your comments.

6
00:00:36,000 --> 00:00:41,000
But I do read the first couple on each video just to make sure the video got uploaded correctly

7
00:00:41,000 --> 00:00:43,000
and no one's having problems.

8
00:00:43,000 --> 00:00:50,000
Sometimes the first couple of comments tell me that the audio was no good or was missing or something like this.

9
00:00:50,000 --> 00:00:52,000
But no, it was too long.

10
00:00:52,000 --> 00:01:00,000
And I just thought it interesting to comment because a 47-minute Damataq is usually considered to be too short.

11
00:01:00,000 --> 00:01:04,000
So if people are having trouble, and I know this is a thing, really, it's true.

12
00:01:04,000 --> 00:01:09,000
YouTube actually tells you, no one's going to watch a 47-minute video.

13
00:01:09,000 --> 00:01:13,000
You shouldn't upload them. You should upload short videos.

14
00:01:13,000 --> 00:01:19,000
When you learn about what YouTube has to say, they know from experience.

15
00:01:19,000 --> 00:01:25,000
I think I've even got a tool that lets me see how many minutes of my video people watch on average.

16
00:01:25,000 --> 00:01:30,000
It says, out of a 47-minute video, average number of minutes watched.

17
00:01:30,000 --> 00:01:33,000
It's probably about three.

18
00:01:33,000 --> 00:01:37,000
I don't know, I don't use the tool, but I can check.

19
00:01:37,000 --> 00:01:42,000
But it's a bit of a shame, really, because, as I said,

20
00:01:42,000 --> 00:01:49,000
and for good reason, Damataq's 47-minutes, that's short.

21
00:01:49,000 --> 00:01:54,000
Given a couple of hours, I think, two-hour Damataq ones, I think.

22
00:01:54,000 --> 00:01:58,000
People have to two hours, people start to complain a bit.

23
00:01:58,000 --> 00:02:02,000
But two or three hours is not unheard of.

24
00:02:02,000 --> 00:02:05,000
One hour is a pretty good length for a talk.

25
00:02:05,000 --> 00:02:08,000
That's usually what we aim for.

26
00:02:08,000 --> 00:02:13,000
Now, I know that Damataq videos have been shorter, and some of the videos don't require it,

27
00:02:13,000 --> 00:02:17,000
and I'm probably going more in-depth and more off-track.

28
00:02:17,000 --> 00:02:21,000
And so I appreciate that. I will try to stay on track.

29
00:02:21,000 --> 00:02:25,000
Maybe a little more on track than before.

30
00:02:25,000 --> 00:02:31,000
But being said, shouldn't be the length.

31
00:02:31,000 --> 00:02:33,000
It should be the content.

32
00:02:33,000 --> 00:02:37,000
If the content is objectionable, then that's a problem.

33
00:02:37,000 --> 00:02:46,000
Anyway, not a big deal. I just thought it was interesting.

34
00:02:46,000 --> 00:02:49,000
We have a quote from the Buddha's last moments.

35
00:02:49,000 --> 00:02:51,000
That's actually a really good quote.

36
00:02:51,000 --> 00:03:00,000
It's one that our Gen Tong brings up a lot.

37
00:03:00,000 --> 00:03:07,000
We're far to fit anyway.

38
00:03:30,000 --> 00:03:35,000
It's a good quote.

39
00:03:35,000 --> 00:03:40,000
But it's a big quote.

40
00:03:40,000 --> 00:03:42,000
It's a huge quote.

41
00:03:42,000 --> 00:03:46,000
It's a big quote.

42
00:03:46,000 --> 00:03:49,000
It's a big quote.

43
00:03:49,000 --> 00:03:53,000
It's a big quote.

44
00:03:53,000 --> 00:04:06,440
And there's a word that's not normally there.

45
00:04:06,440 --> 00:04:13,320
He doesn't put down with his photo case in the city.

46
00:04:13,320 --> 00:04:17,240
His word isn't in all of the person.

47
00:04:17,240 --> 00:04:33,800
It means any monk, female or male, or lead disciple, male or female, who practices the

48
00:04:33,800 --> 00:04:44,360
dhamma in line with the dhamma leading to the dhamma, who dwells practicing the dhamma

49
00:04:44,360 --> 00:04:46,800
in order to attain the dhamma.

50
00:04:46,800 --> 00:04:54,280
Sami, Ji, Bhatipana, who practices properly.

51
00:04:54,280 --> 00:05:01,040
Anudamata, the one who fares according to the dhamma, so that hag dhamma sataruti, such

52
00:05:01,040 --> 00:05:07,640
a person does right by the Buddha, sakaruti.

53
00:05:07,640 --> 00:05:14,640
He does make reverence to the Buddha, makes homage.

54
00:05:14,640 --> 00:05:25,200
Maneti holds up high, holds in their mind, honors things highly of, who J.D., reverents

55
00:05:25,200 --> 00:05:34,080
as Bhati ati, no highly of a Bhati ati is, not in all of our other versions.

56
00:05:34,080 --> 00:05:41,080
Paramaya pujaya with the highest form of reverents.

57
00:05:41,080 --> 00:05:49,880
Tasma, Thihan and dhamma, Nudhamma, Bhatipana, Vihanis, Sami, Ji, Bhatipana, Anudhamma,

58
00:05:49,880 --> 00:06:00,720
Janino, therefore Ananda, you all should dwell practicing the dhamma in order to

59
00:06:00,720 --> 00:06:05,560
attain the dhamma.

60
00:06:05,560 --> 00:06:10,960
Should practice properly, should fare according to the dhamma,

61
00:06:10,960 --> 00:06:17,960
thus you should train yourselves.

62
00:06:17,960 --> 00:06:42,520
Ah, yes, you have that, actually, in English.

63
00:06:42,520 --> 00:06:48,480
So, and we have any questions for tonight?

64
00:06:48,480 --> 00:06:50,640
We do.

65
00:06:50,640 --> 00:06:56,560
Thinking about self and non-self, or what is not, I came to the idea that there is just

66
00:06:56,560 --> 00:06:58,280
happening.

67
00:06:58,280 --> 00:07:03,600
Is this idea of just happening in line with Buddhist teaching moments of happening, actions,

68
00:07:03,600 --> 00:07:06,560
acting upon actions?

69
00:07:06,560 --> 00:07:16,280
Yeah, I mean, that sort of insight is kind of the objectivity poking through.

70
00:07:16,280 --> 00:07:20,920
It's just starting to become more objective, sounds good, sounds like.

71
00:07:20,920 --> 00:07:27,920
Don't hold on to it, that's a raft, a raft changed your mind, so it got you across something,

72
00:07:27,920 --> 00:07:32,480
that's through the raft away and get going.

73
00:07:32,480 --> 00:07:39,440
Hello, Banthay, can you talk about body scanning?

74
00:07:39,440 --> 00:07:42,760
It should always be categorized as samata, right?

75
00:07:42,760 --> 00:07:48,960
Many people seem to associate practicing mindfulness with doing body scanning, or even associate

76
00:07:48,960 --> 00:07:53,840
the term we pass into with body scanning, maybe due to goenka.

77
00:07:53,840 --> 00:07:59,000
The more I have practiced in sight meditation, the less I have done body scanning, should

78
00:07:59,000 --> 00:08:00,840
I stop it completely.

79
00:08:00,840 --> 00:08:04,560
I've noticed it's easy to get attached to the tranquility.

80
00:08:04,560 --> 00:08:08,960
How do you think about this and how to talk about it to the people that have the practicing

81
00:08:08,960 --> 00:08:12,560
mindfulness equals body scanning view?

82
00:08:12,560 --> 00:08:19,000
If one wants to practice repassana intensively, and advance one's understanding of reality,

83
00:08:19,000 --> 00:08:23,840
is it counterproductive to do body scanning once in a while, or can it be useful somehow?

84
00:08:23,840 --> 00:08:28,840
Sorry for the poor articulation, I'm tired or something, thank you.

85
00:08:28,840 --> 00:08:38,200
The word body scanning, I mean words are really problematic, and people have catch

86
00:08:38,200 --> 00:08:42,720
traces or latent labels for things.

87
00:08:42,720 --> 00:08:50,320
Like our, for example, our technique is often talked about as labeling, or noting, acknowledging

88
00:08:50,320 --> 00:08:57,680
what's the big one, acknowledging, but acknowledging is it problematic term anyway, a little

89
00:08:57,680 --> 00:09:03,680
bit of track, the point being body scanning, in order to understand it, we really have

90
00:09:03,680 --> 00:09:09,440
to break it down and see what you're doing, because body scanning is not, when you say

91
00:09:09,440 --> 00:09:14,160
body scanning, it is not immediately mean that it's samata, no, that's, I don't think

92
00:09:14,160 --> 00:09:16,080
that's expected to say.

93
00:09:16,080 --> 00:09:24,080
As to whether it is proper meditation, and whether it is repassana, right, the question

94
00:09:24,080 --> 00:09:27,600
would be whether it's proper meditation, because repassana depends on the object.

95
00:09:27,600 --> 00:09:42,720
And body scanning, body scanning could be problematic if it, if it deals with a body, like

96
00:09:42,720 --> 00:09:47,880
moving through the body.

97
00:09:47,880 --> 00:09:52,040
First of all, I'll say there's no reason to suggest that one should practice in this

98
00:09:52,040 --> 00:09:53,040
way.

99
00:09:53,040 --> 00:09:54,040
I can't think of.

100
00:09:54,040 --> 00:10:03,120
I can't think of a single, even say that we see the manga, passage, which encourages

101
00:10:03,120 --> 00:10:09,120
body scanning, there may be, but I don't, I can't think of anything that hints at it.

102
00:10:09,120 --> 00:10:16,880
I can't even, I don't even, now maybe ladies say it or something about this, and maybe

103
00:10:16,880 --> 00:10:22,760
it is in the commentary somewhere, but I would imagine that even ladies say it, I

104
00:10:22,760 --> 00:10:23,760
didn't.

105
00:10:23,760 --> 00:10:29,120
I could question, did ladies say it, or who was supposed to be the teacher of the teacher

106
00:10:29,120 --> 00:10:34,640
of Goenka, and ladies say it, I was usually who they, they go back to.

107
00:10:34,640 --> 00:10:37,960
So the question would be whether ladies say it, or Wibu say it, or who I think is another

108
00:10:37,960 --> 00:10:42,600
one, a student of ladies say it, either these guys talked about body scanning and where

109
00:10:42,600 --> 00:10:45,600
they got it from.

110
00:10:45,600 --> 00:10:51,600
So I mean, that is something, when there's no, when there's no long standing tradition,

111
00:10:51,600 --> 00:10:56,160
or you can't bring it back to the Buddha, you can't bring it back to the sort of things

112
00:10:56,160 --> 00:10:58,640
that the Buddha taught.

113
00:10:58,640 --> 00:11:03,640
If you contrast that with what we do, as I've been pointing out, especially in our study

114
00:11:03,640 --> 00:11:08,560
of the Wisudimanga, there is very strong precedent for this kind of practice.

115
00:11:08,560 --> 00:11:16,560
In the Wisudimanga, it literally, right, it directly explains to practice in this way.

116
00:11:16,560 --> 00:11:18,520
This is how you practice meditation.

117
00:11:18,520 --> 00:11:25,920
In the Satipatana Sutta, the Buddha himself appears to be, at least subject to interpretation,

118
00:11:25,920 --> 00:11:31,720
but the grammar itself supports directly, saying to yourself things like walking, walking

119
00:11:31,720 --> 00:11:39,080
or angry, angry, pain, pain, and so on.

120
00:11:39,080 --> 00:11:44,760
It's, it's pretty easy to read that into the, I would have, why I'm saying some people

121
00:11:44,760 --> 00:11:47,720
deny this, some people say no, that's not what it says, because it depends what you mean

122
00:11:47,720 --> 00:11:48,720
by the grammar.

123
00:11:48,720 --> 00:11:53,960
But it literally says, the Chantua, the Chami, deep agenda, the Chami tea means I am walking

124
00:11:53,960 --> 00:11:55,320
and tea is a quote.

125
00:11:55,320 --> 00:12:01,720
So it is a quote, no, but the word, the verb is pajana tea, which means no is clearly.

126
00:12:01,720 --> 00:12:10,200
So one knows clearly, quote, unquote, I am walking, that, that coupled with the tradition

127
00:12:10,200 --> 00:12:15,960
of how meditation was practiced is, is a clear indication that what we're doing is not

128
00:12:15,960 --> 00:12:19,680
something newer or off the wall or far-fetched.

129
00:12:19,680 --> 00:12:25,520
The body scanning, before, just as a precursor, I want to say that, that I don't think

130
00:12:25,520 --> 00:12:31,080
it's well supported by the text, couldn't be wrong, and it would be interesting to talk

131
00:12:31,080 --> 00:12:33,600
to them about that.

132
00:12:33,600 --> 00:12:38,000
But that having been said, that's not, we shouldn't be dogmatic as, so dogmatic as to

133
00:12:38,000 --> 00:12:43,160
discard it as a result of that, but as I said, we have a bit of a problem because it may

134
00:12:43,160 --> 00:12:46,280
encourage a concept of a body.

135
00:12:46,280 --> 00:12:58,360
The other thing it may encourage is self, in the sense of actively seeking out.

136
00:12:58,360 --> 00:13:02,880
Now, there's a little bit of that in what we do, but we're careful to, this is a sensitive

137
00:13:02,880 --> 00:13:07,640
subject, it's something that we have to be sensitive to, that we're not actually forcing

138
00:13:07,640 --> 00:13:09,880
or seeking.

139
00:13:09,880 --> 00:13:13,760
And so we tell you to focus on the stomach, rising and falling, and you could argue that

140
00:13:13,760 --> 00:13:20,240
that's pushing yourself to stay with the stomach, could be, and it can become somewhat

141
00:13:20,240 --> 00:13:25,480
forceful and based on concepts of control.

142
00:13:25,480 --> 00:13:30,440
But we're fairly careful not to let it become that we say that's where we start, but

143
00:13:30,440 --> 00:13:36,040
we're completely open to letting the mind go into what is often called choiceless awareness.

144
00:13:36,040 --> 00:13:43,040
It is pretty choiceless, we're not exactly choosing, even though we do choose the stomach,

145
00:13:43,040 --> 00:13:47,400
we do it as a base more than anything.

146
00:13:47,400 --> 00:13:51,400
I mean, it's, there's still that, that potential criticism.

147
00:13:51,400 --> 00:13:57,880
I think some people would say, well, if you're going to be totally, we're going to be

148
00:13:57,880 --> 00:14:00,480
sincere about this, it should be completely choiceless.

149
00:14:00,480 --> 00:14:04,160
Of course, that's a problem, and that doesn't really work, you can't do that.

150
00:14:04,160 --> 00:14:06,840
And so as a result, we do use some control.

151
00:14:06,840 --> 00:14:11,440
And I think the biggest argument for either practice, whether it's the body scan or whether

152
00:14:11,440 --> 00:14:18,040
it's staying with the stomach, rising and falling, is that you need to start from a point

153
00:14:18,040 --> 00:14:25,840
of control in order to just get your feet, in order to just get balanced enough to start

154
00:14:25,840 --> 00:14:29,400
to see and to see how control breaks down.

155
00:14:29,400 --> 00:14:36,200
So it's not exactly control, but there is the potential.

156
00:14:36,200 --> 00:14:43,000
And I think the good thing about the body scan is that it's, it's, it's not partial.

157
00:14:43,000 --> 00:14:48,480
So the idea is you're, if you scan from head to feet, you're not choosing this part

158
00:14:48,480 --> 00:14:50,360
of that part based on your preference.

159
00:14:50,360 --> 00:14:54,080
You're not going to certain things and avoiding other things, you, you, you're forced to

160
00:14:54,080 --> 00:14:56,920
go through the entire body, I think, I've never practiced it.

161
00:14:56,920 --> 00:15:00,600
I think they did it from head to feet.

162
00:15:00,600 --> 00:15:06,280
But that being said, apart from those minor thoughts about it, I don't think about it too

163
00:15:06,280 --> 00:15:07,280
much.

164
00:15:07,280 --> 00:15:11,280
And probably this is probably a shouldn't have said even that much because it's not our technique.

165
00:15:11,280 --> 00:15:18,200
And I don't like to talk about things that don't concern us, but it allowed me to talk

166
00:15:18,200 --> 00:15:19,200
about our technique.

167
00:15:19,200 --> 00:15:23,880
And I think rather than, than attack other people's techniques, I would say that about

168
00:15:23,880 --> 00:15:27,400
our technique, that it's well represented in the texts.

169
00:15:27,400 --> 00:15:30,400
And so that's why we practice this way.

170
00:15:30,400 --> 00:15:35,240
Oh, it's not evident.

171
00:15:35,240 --> 00:15:40,280
There was a lot of question as to what you should do.

172
00:15:40,280 --> 00:15:42,640
Should you stop it completely?

173
00:15:42,640 --> 00:15:44,040
You should practice one way or the other.

174
00:15:44,040 --> 00:15:46,400
If you're practicing our technique, practice our technique.

175
00:15:46,400 --> 00:15:50,200
If you're practicing that technique, practice that technique, shouldn't mix.

176
00:15:50,200 --> 00:15:56,120
It shouldn't, because that becomes a waste based on your partiality mixing, apart from

177
00:15:56,120 --> 00:15:59,040
being confusing, it's also usually based on preference.

178
00:15:59,040 --> 00:16:01,560
I like to do this sometimes and that sometimes.

179
00:16:01,560 --> 00:16:05,480
So when I feel like doing this on this, when I feel like doing that, I'll do that.

180
00:16:05,480 --> 00:16:13,320
And that's hugely problematic.

181
00:16:13,320 --> 00:16:19,280
How about the idea of talking about this two people who have the body scanning view?

182
00:16:19,280 --> 00:16:22,040
We're not, we're not about changing people's views.

183
00:16:22,040 --> 00:16:24,640
If they have that view, then power to them.

184
00:16:24,640 --> 00:16:26,600
If they're looking to change, then let them change.

185
00:16:26,600 --> 00:16:30,520
I'm not trying to convince the world of our practice.

186
00:16:30,520 --> 00:16:35,160
Usually if people even want us to convince them, we say, you know, it's too much trouble

187
00:16:35,160 --> 00:16:36,160
for me.

188
00:16:36,160 --> 00:16:42,640
I mean, that's what sort of the example we get from the our hands in the texts, for

189
00:16:42,640 --> 00:16:47,160
the most part, they would be, you know, teaching you would be too much trouble is the

190
00:16:47,160 --> 00:16:48,640
kind of thing they would say.

191
00:16:48,640 --> 00:16:49,640
It's important.

192
00:16:49,640 --> 00:16:58,800
It's important not to pander, you know, we're not, and not to, not to obsess over changing

193
00:16:58,800 --> 00:17:00,600
other people.

194
00:17:00,600 --> 00:17:06,000
So look at yourself, at that point, you should look at your own thoughts, you know, do

195
00:17:06,000 --> 00:17:07,000
you understand this?

196
00:17:07,000 --> 00:17:11,720
Okay, do it, do it the way you understand it, and then you ask yourself, maybe I'm doing

197
00:17:11,720 --> 00:17:12,720
it wrong.

198
00:17:12,720 --> 00:17:15,400
That's what you have to figure out.

199
00:17:15,400 --> 00:17:18,480
Everyone should look at their own feet when they walk.

200
00:17:18,480 --> 00:17:21,600
Not literally, when we do walking meditation, don't look at you, see?

201
00:17:21,600 --> 00:17:35,040
That's the Buddha said we watch our own paths, we don't worry about the footsteps of others.

202
00:17:35,040 --> 00:17:37,960
Question about the meditator list, excuse me.

203
00:17:37,960 --> 00:17:41,960
What does the plus one and the number shown between the two hands into K?

204
00:17:41,960 --> 00:17:46,000
Mine has changed from five to two, and fairly new to the layout.

205
00:17:46,000 --> 00:17:48,280
I don't think that's possible.

206
00:17:48,280 --> 00:17:52,880
Unless you're using the Android app, it does weird things, I don't know how to fix it.

207
00:17:52,880 --> 00:17:57,960
Yeah, the Android app sometimes shows the, shows the in green meaning that you've already

208
00:17:57,960 --> 00:18:00,120
clicked on it when you happen.

209
00:18:00,120 --> 00:18:01,520
So that's a little strange.

210
00:18:01,520 --> 00:18:06,000
If you don't move the list, it fixes itself on the next update, but if it's not all

211
00:18:06,000 --> 00:18:10,960
looking down, something goes kind of funny when, because there's something wrong with it,

212
00:18:10,960 --> 00:18:12,960
it's kind of fixed.

213
00:18:12,960 --> 00:18:18,520
I mean, someone who's smart.

214
00:18:18,520 --> 00:18:24,080
The plus one means plus one, it means you've done something good and I'm out and I'm

215
00:18:24,080 --> 00:18:33,240
like, it's like, it's like, it's like a Facebook like, so it should, I don't think

216
00:18:33,240 --> 00:18:42,240
they can go down, but I don't know, is that, yeah, technical difficulties.

217
00:18:42,240 --> 00:18:44,920
In weekends, I try to practice the egg precepts.

218
00:18:44,920 --> 00:18:49,960
I've noticed strong craving, but there doesn't seem to be any object for the craving.

219
00:18:49,960 --> 00:18:52,400
It seems like the mind is just hungry.

220
00:18:52,400 --> 00:18:56,800
I can watch this hungry mind for only so long, then I end up feeding it.

221
00:18:56,800 --> 00:19:00,800
Is the idea to feed it, but in the most wholesome way possible?

222
00:19:00,800 --> 00:19:11,880
I think by feed it, you're talking sort of a figurative feeding, I hope, talking about feeding

223
00:19:11,880 --> 00:19:13,640
the mind, right?

224
00:19:13,640 --> 00:19:22,480
I mean, the best is if you can learn to overcome and let go of it.

225
00:19:22,480 --> 00:19:27,160
But yeah, if you're right, so you're trying to keep the precepts and then you end up

226
00:19:27,160 --> 00:19:34,280
breaking the egg precepts, maybe, by listening music or something or watching movie or something.

227
00:19:34,280 --> 00:19:39,560
I mean, there are ways to do it without breaking the precepts, so it's sort of distracting,

228
00:19:39,560 --> 00:19:44,840
but it's kind of beside the point and then you have to learn to overcome it.

229
00:19:44,840 --> 00:19:48,840
If you keep an egg precepts, you should keep an egg precepts.

230
00:19:48,840 --> 00:19:50,880
It's only temporary.

231
00:19:50,880 --> 00:19:54,720
So you learn to keep teaching yourself control of nothing else.

232
00:19:54,720 --> 00:19:59,680
But I mean, that's part of the reason for keeping the precepts is to learn about your desires

233
00:19:59,680 --> 00:20:09,560
and to have a chance, because if you constantly have the ability to indulge, to satisfy

234
00:20:09,560 --> 00:20:14,560
your desires, you'll never get to see what this desire is like, you'll never really get

235
00:20:14,560 --> 00:20:20,480
to understand what desire is, you'll never have a chance to challenge this idea that

236
00:20:20,480 --> 00:20:26,160
desire is worth having and that desire is a sign that you should chase after what you're

237
00:20:26,160 --> 00:20:27,160
looking for.

238
00:20:27,160 --> 00:20:34,080
So as you watch the desire without indulging it, you're able to see a middle way, you know,

239
00:20:34,080 --> 00:20:39,120
a way of just being with desire without acting on it or without repressing it.

240
00:20:39,120 --> 00:20:41,920
That's what we're looking for.

241
00:20:41,920 --> 00:20:44,960
That's so what you're talking about is not the idea, the idea is not defeated.

242
00:20:44,960 --> 00:20:50,080
But if you feel that then just feel as mindful as you can, of course, if you're keeping

243
00:20:50,080 --> 00:20:52,840
a precepts, you shouldn't feel it.

244
00:20:52,840 --> 00:21:02,760
Hello, Bhante, could you please tell whether it is true that one has to cycle through

245
00:21:02,760 --> 00:21:07,280
the insight knowledge as many, many times after third path before actually arriving at

246
00:21:07,280 --> 00:21:12,240
fourth path, or does one only have to get through one more progress of insight cycle after

247
00:21:12,240 --> 00:21:15,120
third path to attain fourth path?

248
00:21:15,120 --> 00:21:16,120
Thanks.

249
00:21:16,120 --> 00:21:26,120
It only requires one cycle, but that's technically, realistically it usually requires many.

250
00:21:26,120 --> 00:21:28,560
But there's two different cycles.

251
00:21:28,560 --> 00:21:38,280
One cycle is only going to be a review, so it's not going to be all 16 stages of knowledge.

252
00:21:38,280 --> 00:21:42,280
You can actually only go through the stages of knowledge four times.

253
00:21:42,280 --> 00:21:49,280
So you're not actually going through all the stages of knowledge, not technically, because

254
00:21:49,280 --> 00:21:51,920
it's missing one until you reach the next path.

255
00:21:51,920 --> 00:21:56,600
But I think that's really a technicality.

256
00:21:56,600 --> 00:22:01,120
Every time you go through the knowledge, you reduce the number of, you reduce your

257
00:22:01,120 --> 00:22:02,120
defamance.

258
00:22:02,120 --> 00:22:05,040
So I'm going to cut off piece by piece by piece.

259
00:22:05,040 --> 00:22:08,760
When you reach a certain point, that's called anangami and you reach another and you reach

260
00:22:08,760 --> 00:22:28,760
the final point where there's none left, that's R1.

261
00:22:28,760 --> 00:22:30,160
There is one more.

262
00:22:30,160 --> 00:22:34,280
Could you please talk about how anangami progresses to our hardship?

263
00:22:34,280 --> 00:22:39,080
I am at high equanimity after third path.

264
00:22:39,080 --> 00:22:40,080
Any advice?

265
00:22:40,080 --> 00:22:43,960
I got frustrated after practicing a long time.

266
00:22:43,960 --> 00:22:51,560
Advice, here's a person with yellow orange, who hasn't done any meditation with

267
00:22:51,560 --> 00:22:52,560
this.

268
00:22:52,560 --> 00:22:54,600
Anangami don't get frustrated.

269
00:22:54,600 --> 00:22:56,600
I'm not going to answer your question.

270
00:22:56,600 --> 00:23:00,720
If you start meditating with this, I think I'm not going to answer these sorts of questions

271
00:23:00,720 --> 00:23:06,360
and it's either you want to meditate with us, you should start logging your meditation.

272
00:23:06,360 --> 00:23:10,960
Read my booklet, start practicing according to our technique, start logging your meditation

273
00:23:10,960 --> 00:23:14,080
and if you have questions about your meditation, I'm happy to answer them.

274
00:23:14,080 --> 00:23:18,000
I'm not going to talk about these.

275
00:23:18,000 --> 00:23:22,960
It's not something I can verify and the sounds of it, there's a misunderstanding of what

276
00:23:22,960 --> 00:23:26,960
anangami is.

277
00:23:26,960 --> 00:23:34,480
Sorry.

278
00:23:34,480 --> 00:23:36,400
And with that, we are caught up.

279
00:23:36,400 --> 00:23:38,680
Nobody clicks on people's hands anymore, do they?

280
00:23:38,680 --> 00:23:39,680
I used to.

281
00:23:39,680 --> 00:23:41,680
I do sometimes.

282
00:23:41,680 --> 00:23:46,040
There's a lot of them click, click, click.

283
00:23:46,040 --> 00:23:56,600
What we don't do very often is up on the top part, click to like questions and comments

284
00:23:56,600 --> 00:23:57,600
and things.

285
00:23:57,600 --> 00:24:12,520
I forget to do that.

286
00:24:12,520 --> 00:24:14,640
Hm.

287
00:24:14,640 --> 00:24:22,280
Okay, that's all done for tonight.

288
00:24:22,280 --> 00:24:25,600
Thank you all for tuning in and good night.

289
00:24:25,600 --> 00:24:26,600
Thank you.

290
00:24:26,600 --> 00:24:27,600
Thank you, Bonte.

291
00:24:27,600 --> 00:24:53,600
Good night.

