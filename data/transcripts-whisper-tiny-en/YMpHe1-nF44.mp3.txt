Okay, we have five minutes left, so I'm going to ask that our three panel, the three of us say our last words and then call it quits for the year.
So, Bill, can you give us your last words, what you'd like to say, but this evening and what you'd like to wish for the people out there?
This has been great, you know, that we can, we're doing this this year, people that can come on and take part in your, what you're doing, separate from the comment areas that you would just kind of answer as best you could and get to as many as you could.
And I was just glad to be part of it and learned a lot, and I hope everybody has progressed from it.
Thank you very much too.
Thank you for being here. Paul, are you still with us?
I'm still here, Bante.
Can I ask your last word? Okay.
I've really enjoyed coming to these meetings and I wish you lots of like your travels.
And on a more general kind of note, I just wish everyone to be peaceful, happy and well.
Thank you.
Yeah, so thanks everyone. This has been great. It's lots of fun. It's encouraging.
It's a nice way of spreading the dhamma. It really helps.
The sort of thing helps my work doesn't make more work for me.
This kind of thing is what makes the world easier to live in and makes it easier to spread the Buddhist teaching because there's far more this sort of thing.
For me, it's amazing the reach that you can gain and how you can actually feel the potential for making a small change even on a global level that you're actually able to participate in the global conversation
to the extent that it may actually have change and affect the world.
What that means, I don't know, but it's the work that we do and it's certainly a good work.
So I'm going to get cut off here in a second, but I'd like to wish everyone good end of the year and a happy new year.
And that you only put into practice these the teachings of the Buddha don't be content with just learning about them.
Try your best to put them in practice and may everyone through their practice find true peace, happiness and freedom from suffering.
Peace.
Goodbye.
Good bye.
