1
00:00:00,000 --> 00:00:09,000
Okay, this is in regards to feeling angry towards my mom, because of how she treated me when I was growing up as a child.

2
00:00:10,000 --> 00:00:19,000
I'm thinking about having a conversation telling her what I needed as a child from her, but wasn't receiving,

3
00:00:19,000 --> 00:00:21,000
which has now affected me as an adult.

4
00:00:21,000 --> 00:00:23,000
What do you think about this?

5
00:00:23,000 --> 00:00:31,000
Does this create even more tension and negativity, or would it be the start of a healing process for both of us?

6
00:00:31,000 --> 00:00:32,000
Hmm.

7
00:00:32,000 --> 00:00:44,000
In the first place, it would show you very clearly the defilements that you have.

8
00:00:44,000 --> 00:00:56,000
And it shows you the wanting that there is something that you want from your mother, that you wanted then, and that you still want now.

9
00:00:56,000 --> 00:01:11,000
And it is very, very good that you see this, that you have the chance now to observe your defilements as they arise.

10
00:01:11,000 --> 00:01:17,000
If you talk with her or not, it's really not so important.

11
00:01:17,000 --> 00:01:26,000
What is important is that you see that what has happened when you was a child is the past.

12
00:01:26,000 --> 00:01:32,000
It is no more. It has gone. It is past.

13
00:01:32,000 --> 00:01:41,000
So now you see, I wanted this and that, and I didn't get it. And I'm angry.

14
00:01:41,000 --> 00:01:50,000
So note it and be happy, be grateful that you have the chance to learn this now.

15
00:01:50,000 --> 00:01:56,000
And don't get deeper into it. Just try to let go of it.

16
00:01:56,000 --> 00:02:04,000
Learn to let go of it. Your mother is not the one she was. I don't know what she did.

17
00:02:04,000 --> 00:02:14,000
And it really doesn't matter because the point is that now is now past is past.

18
00:02:14,000 --> 00:02:24,000
And you create now the karma that you will have to deal with in future.

19
00:02:24,000 --> 00:02:34,000
So when you don't stop to be angry with her now, you will be angry with her in future.

20
00:02:34,000 --> 00:02:42,000
And the only chance to change it is to stop being angry with her now.

21
00:02:42,000 --> 00:02:48,000
And you can do so by seeing very clearly what is going on in your mind.

22
00:02:48,000 --> 00:02:56,000
And to do what Bhantayutadamu said in regards to equanimity.

23
00:02:56,000 --> 00:03:08,000
Yeah, that's just to put it into kind of a more general format.

24
00:03:08,000 --> 00:03:17,000
In general, going back to the past and worrying about what has happened in the past is not recommended.

25
00:03:17,000 --> 00:03:23,000
Going back to the past is connecting who you are now with who you were in the past.

26
00:03:23,000 --> 00:03:28,000
And that's Paulinean. He says the past is past and you were a different person.

27
00:03:28,000 --> 00:03:39,000
And the point is that it reinforces the ego. It reinforces the idea of I and the whole idea of needs as well.

28
00:03:39,000 --> 00:03:44,000
But the idea of having problems in the present based on the past,

29
00:03:44,000 --> 00:03:53,000
I mean, people who are victims of abuse will often fall into that kind of a mentality and a really a trap.

30
00:03:53,000 --> 00:03:58,000
I had one meditated that I always bring up as an example when I was in Doy Soutep,

31
00:03:58,000 --> 00:04:06,000
who was abused as a child. And she came to me and she was 40. She came to our meditation center.

32
00:04:06,000 --> 00:04:09,000
She was 40 something. But she looked like a ghost.

33
00:04:09,000 --> 00:04:17,000
If you look at her, her hair was in her face and just this horrified expression that she was like as though she was seeing a ghost,

34
00:04:17,000 --> 00:04:22,000
she had been raped by her father when she was young.

35
00:04:22,000 --> 00:04:27,000
And now at 40 years old, she was still dealing with it. I mean, I don't know how long ago it was.

36
00:04:27,000 --> 00:04:32,000
But I think she did tell me later.

37
00:04:32,000 --> 00:04:36,000
But the point I want to make is that it was clear that something was wrong.

38
00:04:36,000 --> 00:04:39,000
Clearly that something was going on, but I never asked her about it.

39
00:04:39,000 --> 00:04:49,000
And I think this really puzzled her because her whole life, the therapy that she had always gone through again and again,

40
00:04:49,000 --> 00:05:02,000
before this was to go into it and to become, you know, to somehow delve into these problems and to connect what's going on now with what was going on in the past.

41
00:05:02,000 --> 00:05:09,000
And we don't do that and I didn't do it with her. And as a result, she started to, at first, it was like,

42
00:05:09,000 --> 00:05:14,000
well, don't you want to know why I'm crying? Don't you know, what's wrong with me?

43
00:05:14,000 --> 00:05:21,000
Until eventually she realized that, you know, based on the simple teachings that had nothing to do with any problem in the past.

44
00:05:21,000 --> 00:05:28,000
And you know, I said, when you're crying, just say to yourself, crying, crying, don't worry about it's not a problem that you're crying.

45
00:05:28,000 --> 00:05:33,000
There's nothing to be concerned about, just say to yourself, crying, crying.

46
00:05:33,000 --> 00:05:36,000
And it totally changed her outlook on the problems.

47
00:05:36,000 --> 00:05:43,000
She was able to finally cope with what was the real problem and what's occurring right here and now.

48
00:05:43,000 --> 00:05:48,000
That really should have nothing to do with the past.

49
00:05:48,000 --> 00:05:52,000
The past has created where you are now, for sure. But now that's meaningless.

50
00:05:52,000 --> 00:06:05,000
The past, what's in the past has gone and, you know, trying to have people make amends for it or to, you know, to cling to the idea that someone else has heard you.

51
00:06:05,000 --> 00:06:09,000
This is in the Dhamapada, the Buddha was quite clear about it.

52
00:06:09,000 --> 00:06:12,000
He beat me, he robbed me, he...

53
00:06:12,000 --> 00:06:21,000
a kochimang, a haasimang, a genimang, however it goes. He scolded me, he abused me and so on.

54
00:06:21,000 --> 00:06:24,000
For a person who harbors these thoughts, suffering never ceases.

55
00:06:24,000 --> 00:06:27,000
Now, I know you're aware of this and you don't want to be angry at her.

56
00:06:27,000 --> 00:06:35,000
But just to say that going back to the past only reaffirms the idea of self, of eye and what I need.

57
00:06:35,000 --> 00:06:40,000
And so we don't do it. It's not part of our therapy. Our therapy is to deal with the here and the now.

58
00:06:40,000 --> 00:06:44,000
And to learn to dissociate, stop making associations.

59
00:06:44,000 --> 00:06:53,000
It is what it is. Hence, when you feel pain, to say to yourself pain, do not think about why you feel pain or it's because of an accident or this or that.

60
00:06:53,000 --> 00:06:58,000
When you feel anger, do not think about why you feel anger or even who you feel angry at.

61
00:06:58,000 --> 00:07:03,000
You're just feeling angry and to focus on the anger, to focus on the experience as it is.

62
00:07:03,000 --> 00:07:10,000
Because the whole point is to lose the object. When you say to yourself, angry, angry, angry.

63
00:07:10,000 --> 00:07:14,000
You're breaking the connection with the object of your anger.

64
00:07:14,000 --> 00:07:22,000
This is incredibly important. As long as there's the connection with the object, there's going to be the object arises.

65
00:07:22,000 --> 00:07:26,000
Thinking about the object arises. Anger arises based on the thinking.

66
00:07:26,000 --> 00:07:33,000
Based on the anger, there's a returning to the object. Thinking about what you can do to hurt them.

67
00:07:33,000 --> 00:07:37,000
Thinking about what they've done to you and getting angry again. It's a feedback loop.

68
00:07:37,000 --> 00:07:44,000
As long as you have the connection with the object, you're going to be creating more and more anger every time you think about that object.

69
00:07:44,000 --> 00:07:53,000
When you say to yourself, angry, angry, angry, you change the habit in the mind that when anger arises, there's just anger.

70
00:07:53,000 --> 00:08:00,000
It doesn't go back to the object, thinking about creating difficulties or problems.

71
00:08:00,000 --> 00:08:06,000
The point is the anger has no fuel. Once the anger has no fuel, it stays for a while.

72
00:08:06,000 --> 00:08:12,000
It might even stay quite strong based on the fuel that it already has from your past or from your habits.

73
00:08:12,000 --> 00:08:19,000
But eventually it disappears. The key to dealing with these things is to see them for what they are and nothing more.

74
00:08:19,000 --> 00:08:30,000
You may have intense states of anger or intense states of greed, and that's not the biggest problem.

75
00:08:30,000 --> 00:08:34,000
It's not the real problem. The real problem is when you follow after them.

76
00:08:34,000 --> 00:08:44,000
If you're able to be mindful of wanting, wanting, or angry, angry, and see them for what they are, you see that they're really harmless.

77
00:08:44,000 --> 00:08:49,000
The only thing that they're doing is bringing you more stress in the mind.

78
00:08:49,000 --> 00:08:55,000
So you quickly do away with them. That's really how we deal with these things.

79
00:08:55,000 --> 00:09:18,000
So just meant to sort of generalize what you'd already say.

