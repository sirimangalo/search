1
00:00:00,000 --> 00:00:05,000
Hello, welcome back to Askamunk.

2
00:00:05,000 --> 00:00:11,000
Some time ago, I did a video on what meditation is not,

3
00:00:11,000 --> 00:00:16,000
or specifically those experiences that are not insight meditation,

4
00:00:16,000 --> 00:00:22,000
or not meditation into the nature of reality.

5
00:00:22,000 --> 00:00:27,000
So these are sort of experiences and meditation,

6
00:00:27,000 --> 00:00:31,000
which on the surface appear to be positive beneficial,

7
00:00:31,000 --> 00:00:35,000
and are generally regarded to be good experiences,

8
00:00:35,000 --> 00:00:38,000
but when the meditator follows after them,

9
00:00:38,000 --> 00:00:43,000
and clings to them and becomes attached to them,

10
00:00:43,000 --> 00:00:46,000
they take one away from the purpose,

11
00:00:46,000 --> 00:00:48,000
which is to see things as they are.

12
00:00:48,000 --> 00:00:51,000
So these are experiences like states of happiness,

13
00:00:51,000 --> 00:00:53,000
states of peace, states of calm,

14
00:00:53,000 --> 00:00:59,000
states of supernatural knowledge of things far away,

15
00:00:59,000 --> 00:01:02,000
or knowledge of the past, knowledge of the future,

16
00:01:02,000 --> 00:01:04,000
and so on.

17
00:01:04,000 --> 00:01:08,000
Many supposedly good experiences,

18
00:01:08,000 --> 00:01:11,000
and in many types of meditation,

19
00:01:11,000 --> 00:01:13,000
these experiences are considered to be a goal,

20
00:01:13,000 --> 00:01:16,000
or a purpose for practicing meditation.

21
00:01:16,000 --> 00:01:18,000
Now in insight meditation,

22
00:01:18,000 --> 00:01:22,000
our intention is to see reality and understand reality,

23
00:01:22,000 --> 00:01:26,000
which is our experience of the world for what it is,

24
00:01:26,000 --> 00:01:28,000
and to do away with our misconceptions

25
00:01:28,000 --> 00:01:31,000
and to do away with our misunderstanding.

26
00:01:31,000 --> 00:01:36,000
So it's important to understand the truth of these experiences

27
00:01:36,000 --> 00:01:38,000
and to see them for what they are,

28
00:01:38,000 --> 00:01:43,000
as simply states of emotion or feeling or knowledge

29
00:01:43,000 --> 00:01:46,000
or experience that come and go,

30
00:01:46,000 --> 00:01:51,000
and there's nothing intrinsically special about them,

31
00:01:51,000 --> 00:01:54,000
and if one can take them as simply an experience

32
00:01:54,000 --> 00:01:58,000
and let them go and move on, then there'll be no problem.

33
00:01:58,000 --> 00:02:02,000
Now in this video, I wanted to talk about those experiences,

34
00:02:02,000 --> 00:02:05,000
which are insight meditation,

35
00:02:05,000 --> 00:02:07,000
or are a part of insight meditation,

36
00:02:07,000 --> 00:02:11,000
but are conversely often understood by the meditator

37
00:02:11,000 --> 00:02:13,000
to be wrong practice,

38
00:02:13,000 --> 00:02:16,000
or to be a sign that things are going wrong,

39
00:02:16,000 --> 00:02:18,000
or that one is practicing incorrectly.

40
00:02:18,000 --> 00:02:21,000
So we have both sides.

41
00:02:21,000 --> 00:02:23,000
The other video talked about those things,

42
00:02:23,000 --> 00:02:28,000
which the meditator misconceived to be beneficial,

43
00:02:28,000 --> 00:02:32,000
misconceived to be good practice, proper practice,

44
00:02:32,000 --> 00:02:34,000
and here I'd like to talk about those things

45
00:02:34,000 --> 00:02:37,000
that the meditator will often understand,

46
00:02:37,000 --> 00:02:41,000
misunderstand to be unbeneficial or improper practice,

47
00:02:41,000 --> 00:02:43,000
or a sign that practice is going wrong.

48
00:02:43,000 --> 00:02:47,000
When in fact, these things are a sign that one is practicing correctly.

49
00:02:47,000 --> 00:02:51,000
Now to understand this, it's important for us to understand

50
00:02:51,000 --> 00:02:56,000
what we mean by meditation, specifically insight meditation.

51
00:02:56,000 --> 00:03:02,000
The teaching of the Buddha is based on the fact that inside of all of us,

52
00:03:02,000 --> 00:03:06,000
we have three things which we would be better off without,

53
00:03:06,000 --> 00:03:12,000
which exist in our mind and our cause for all of our suffering.

54
00:03:12,000 --> 00:03:18,000
These we, in English, we translate them as greed, anger,

55
00:03:18,000 --> 00:03:19,000
and delusion.

56
00:03:19,000 --> 00:03:24,000
These are sort of approximate names for these states.

57
00:03:24,000 --> 00:03:28,000
Greed as any partiality for something,

58
00:03:28,000 --> 00:03:34,000
anger is a name for any partiality against something.

59
00:03:34,000 --> 00:03:39,000
And delusion is the misunderstanding that leads us

60
00:03:39,000 --> 00:03:44,000
to be partial towards or against something.

61
00:03:44,000 --> 00:03:51,000
So these three things are the problem that we are trying to address

62
00:03:51,000 --> 00:03:55,000
in the Buddhist teaching or in the insight meditation tradition.

63
00:03:55,000 --> 00:03:57,000
Because obviously, if you misunderstand something,

64
00:03:57,000 --> 00:03:59,000
that's not a good thing, but the point is,

65
00:03:59,000 --> 00:04:04,000
once you misunderstand something, it's going to give rise to attachment

66
00:04:04,000 --> 00:04:09,000
to things that are to certain things,

67
00:04:09,000 --> 00:04:12,000
and repulsion from certain other things.

68
00:04:12,000 --> 00:04:15,000
And this is how we often will live our lives,

69
00:04:15,000 --> 00:04:19,000
being attached to certain experiences and repulsed by other experiences,

70
00:04:19,000 --> 00:04:23,000
and so we're constantly in a state of chasing after some things

71
00:04:23,000 --> 00:04:26,000
and running away from certain other things.

72
00:04:26,000 --> 00:04:32,000
Now this is a problem because as the Buddha pointed out,

73
00:04:32,000 --> 00:04:37,000
the nature of reality is that nothing is going to satisfy us.

74
00:04:37,000 --> 00:04:41,000
There is nothing which we are going to chase after

75
00:04:41,000 --> 00:04:45,000
and it's going to bring us peace, happiness, and freedom from suffering

76
00:04:45,000 --> 00:04:47,000
once we get it, once we attain it.

77
00:04:47,000 --> 00:04:50,000
The reason for this is because we have another three things,

78
00:04:50,000 --> 00:04:54,000
and this ties into the experiences that are going to come up in meditation.

79
00:04:54,000 --> 00:04:57,000
When we practice meditation correctly,

80
00:04:57,000 --> 00:04:59,000
we're going to see reality for what it is.

81
00:04:59,000 --> 00:05:03,000
Now, the reason we get attached to certain things

82
00:05:03,000 --> 00:05:05,000
and repulsed by other certain things,

83
00:05:05,000 --> 00:05:08,000
because we don't see the nature of reality,

84
00:05:08,000 --> 00:05:12,000
we become attracted to certain things based on three characteristics.

85
00:05:12,000 --> 00:05:16,000
We perceive certain things in the world to be permanent,

86
00:05:16,000 --> 00:05:19,000
we perceive certain things to be satisfying,

87
00:05:19,000 --> 00:05:23,000
and we perceive certain things to be under our control.

88
00:05:23,000 --> 00:05:25,000
We expect certain things to be this way.

89
00:05:25,000 --> 00:05:27,000
We hope that certain things will be this way,

90
00:05:27,000 --> 00:05:31,000
and we hope to attain states that are this way,

91
00:05:31,000 --> 00:05:33,000
that are permanent, that are satisfying,

92
00:05:33,000 --> 00:05:35,000
and that are under our control.

93
00:05:35,000 --> 00:05:38,000
Now, the reason why nothing is going to satisfy

94
00:05:38,000 --> 00:05:41,000
is going to ever bring us peace, happiness, and freedom from suffering

95
00:05:41,000 --> 00:05:43,000
is because there's nothing in the world,

96
00:05:43,000 --> 00:05:45,000
which is any of these three things.

97
00:05:45,000 --> 00:05:48,000
And this is where our misunderstanding lies.

98
00:05:48,000 --> 00:05:50,000
When we talk about the delusion part,

99
00:05:50,000 --> 00:05:53,000
the misunderstanding part, it's the misunderstanding

100
00:05:53,000 --> 00:05:56,000
that we're going to somehow find something that is permanent.

101
00:05:56,000 --> 00:05:59,000
We're going to somehow find something that is satisfying.

102
00:05:59,000 --> 00:06:03,000
We're going to somehow find something that is controllable,

103
00:06:03,000 --> 00:06:08,000
that we can say belongs to us, or is ours.

104
00:06:08,000 --> 00:06:11,000
So this is why we give rise to greed.

105
00:06:11,000 --> 00:06:14,000
When we see certain things,

106
00:06:14,000 --> 00:06:17,000
when we are confronted by certain experiences,

107
00:06:17,000 --> 00:06:22,000
we conceive of that object to be a source of stability,

108
00:06:22,000 --> 00:06:26,000
permanence, reliability.

109
00:06:26,000 --> 00:06:30,000
We conceive it to be therefore satisfying, and pleasant,

110
00:06:30,000 --> 00:06:37,000
and cause for us to be truly happy, and truly at peace with ourselves.

111
00:06:37,000 --> 00:06:39,000
And something that we can control,

112
00:06:39,000 --> 00:06:42,000
something that we can predict,

113
00:06:42,000 --> 00:06:47,000
that will listen and obey our wishes when we want it to be this way.

114
00:06:47,000 --> 00:06:50,000
It will be this way when we want it to be that way.

115
00:06:50,000 --> 00:06:54,000
When we want it to be it will be when we want it to not be, it will not be.

116
00:06:54,000 --> 00:06:58,000
Now, the reason, this is the reason why we become attached to things,

117
00:06:58,000 --> 00:07:01,000
and we chase after things, because we conceive them to be this way.

118
00:07:01,000 --> 00:07:04,000
We gain certain states of pleasure,

119
00:07:04,000 --> 00:07:08,000
and because we're not looking closely at it,

120
00:07:08,000 --> 00:07:11,000
we think, oh, this pleasure is going to last a long time,

121
00:07:11,000 --> 00:07:14,000
or this pleasure, if I work,

122
00:07:14,000 --> 00:07:18,000
it will last longer and longer, and eventually it will last forever.

123
00:07:18,000 --> 00:07:24,000
And so we work and work and work, and eventually we are dissatisfied.

124
00:07:24,000 --> 00:07:26,000
We give rise to the other side.

125
00:07:26,000 --> 00:07:31,000
So the reason we give rise to greed is because we conceive things to be,

126
00:07:31,000 --> 00:07:35,000
to be permanent, satisfying, and controllable.

127
00:07:35,000 --> 00:07:40,000
But when they are not, when, on, in fact,

128
00:07:40,000 --> 00:07:44,000
we realize the truth, we're confronted with the truth.

129
00:07:44,000 --> 00:07:49,000
We're confronted with what we would expect to be a source of wisdom and understanding.

130
00:07:49,000 --> 00:07:52,000
So that is, we see things change.

131
00:07:52,000 --> 00:07:57,000
We see things disappear and fall apart.

132
00:07:57,000 --> 00:08:04,000
We see things cause stress and be uncomfortably experienced discomfort.

133
00:08:04,000 --> 00:08:07,000
We experience suffering and pain and so on.

134
00:08:07,000 --> 00:08:10,000
And when we aren't able to control things,

135
00:08:10,000 --> 00:08:12,000
when things are out of our control,

136
00:08:12,000 --> 00:08:16,000
when experiences, when the reality, when our life goes out of our control,

137
00:08:16,000 --> 00:08:18,000
the things around us, even our own bodies,

138
00:08:18,000 --> 00:08:23,000
they disobey us, and we lose control of our faculties, our mind,

139
00:08:23,000 --> 00:08:26,000
our memory, and so on.

140
00:08:26,000 --> 00:08:30,000
Just about everything, it gives rise to suffering.

141
00:08:30,000 --> 00:08:33,000
It gives rise to anger, which is the other side.

142
00:08:33,000 --> 00:08:38,000
So the reason why we get angry is because of our experience,

143
00:08:38,000 --> 00:08:39,000
our disappointment.

144
00:08:39,000 --> 00:08:42,000
We thought things would be permanent and so on.

145
00:08:42,000 --> 00:08:45,000
And when they're not, we become angry and upset,

146
00:08:45,000 --> 00:08:53,000
also based on our expectation that we should be able to find our expectation of finding

147
00:08:53,000 --> 00:09:01,000
permanent satisfaction and control or mastery or control over our lives,

148
00:09:01,000 --> 00:09:04,000
over our reality.

149
00:09:04,000 --> 00:09:11,000
So these two states are caused by these expectations.

150
00:09:11,000 --> 00:09:13,000
Now the expectation is the delusion.

151
00:09:13,000 --> 00:09:15,000
So these are how the three things work.

152
00:09:15,000 --> 00:09:19,000
If we can remove the delusion, the misunderstanding,

153
00:09:19,000 --> 00:09:23,000
if we can see things and understand the truth,

154
00:09:23,000 --> 00:09:26,000
that all of reality is impermanent,

155
00:09:26,000 --> 00:09:29,000
is unsatisfying, is uncontrollable.

156
00:09:29,000 --> 00:09:31,000
If we're able to see this, then there would be no problem.

157
00:09:31,000 --> 00:09:36,000
It's possible to live in this world in peace, happiness, and freedom from suffering.

158
00:09:36,000 --> 00:09:40,000
The point is not to expect things to be other than what they are,

159
00:09:40,000 --> 00:09:44,000
because in fact life is like a dance, or it has a rhythm to it,

160
00:09:44,000 --> 00:09:45,000
and everything is changing.

161
00:09:45,000 --> 00:09:50,000
And if you can accept that rhythm, and if you can accept reality for what it is,

162
00:09:50,000 --> 00:09:54,000
whenever or whatever it is at any given time,

163
00:09:54,000 --> 00:09:58,000
if you can give up these expectations that things should be this way,

164
00:09:58,000 --> 00:10:01,000
or that way that there should somehow be some stability,

165
00:10:01,000 --> 00:10:07,000
that there should somehow be some satisfaction in an experience,

166
00:10:07,000 --> 00:10:11,000
and that somehow experience should be under your control.

167
00:10:11,000 --> 00:10:16,000
If you can give this up, then you can live in peace, happiness, and freedom from suffering.

168
00:10:16,000 --> 00:10:20,000
It means you have to give up any expectation,

169
00:10:20,000 --> 00:10:25,000
and any attachment to this experience or that experience.

170
00:10:25,000 --> 00:10:29,000
So this is what we're trying to achieve in inside meditation,

171
00:10:29,000 --> 00:10:32,000
and so we're therefore trying to come to understand,

172
00:10:32,000 --> 00:10:36,000
simply to see reality for what it is, to strengthen our minds,

173
00:10:36,000 --> 00:10:41,000
and to create this understanding and this wisdom,

174
00:10:41,000 --> 00:10:47,000
which allows us to anticipate change, and to expect change,

175
00:10:47,000 --> 00:10:51,000
so that when change occurs, to be ready for it.

176
00:10:51,000 --> 00:10:58,000
So once we've seen an experience change and come to realize that this is the core of reality,

177
00:10:58,000 --> 00:11:01,000
is that whatever arises ceases,

178
00:11:01,000 --> 00:11:04,000
everything that comes goes, everything is constantly changing,

179
00:11:04,000 --> 00:11:07,000
and therefore no experience can satisfy us,

180
00:11:07,000 --> 00:11:11,000
and you can't expect something to really bring you happiness,

181
00:11:11,000 --> 00:11:14,000
and you can't expect to find control,

182
00:11:14,000 --> 00:11:23,000
or to be able to control and manipulate and dominate, and be in charge of even your own life.

183
00:11:23,000 --> 00:11:26,000
If we can let go of all this, then we'll be truly peaceful and happy.

184
00:11:26,000 --> 00:11:30,000
This is what we're trying to gain, this understanding.

185
00:11:30,000 --> 00:11:35,000
So the problem is, when we come to practice any type of meditation,

186
00:11:35,000 --> 00:11:40,000
we bring all of these ideas, these misunderstandings with us.

187
00:11:40,000 --> 00:11:48,000
So involuntarily or unconsciously, we expect the meditation to be permanent or stable,

188
00:11:48,000 --> 00:11:54,000
to be pleasant and satisfying, and to be controllable.

189
00:11:54,000 --> 00:11:59,000
So we expect that our object of meditation should be all of these things.

190
00:11:59,000 --> 00:12:04,000
When we focus on the stomach, we expect that with some work and with some practice,

191
00:12:04,000 --> 00:12:09,000
it's going to be constant, so it will be rising and falling,

192
00:12:09,000 --> 00:12:14,000
constantly and smoothly, and therefore it will be pleasant,

193
00:12:14,000 --> 00:12:19,000
it will be satisfying, and the meditation will bring us a great amount of pleasure and happiness.

194
00:12:19,000 --> 00:12:21,000
And we'll be able to control it.

195
00:12:21,000 --> 00:12:26,000
We'll be the one saying, now rise, now fall, and our minds will never think,

196
00:12:26,000 --> 00:12:31,000
and will never wander, and will never give rise to any unpleasant situation.

197
00:12:31,000 --> 00:12:35,000
Eventually the pain will leave, and we'll be able to sit comfortably, and so on.

198
00:12:35,000 --> 00:12:39,000
And so on, this is how we look at it, this is what we expect.

199
00:12:39,000 --> 00:12:44,000
But we have to understand that this is not the purpose of meditation,

200
00:12:44,000 --> 00:12:53,000
this is not the proper goal, the true way to find peace, happiness, and freedom from suffering.

201
00:12:53,000 --> 00:12:56,000
It's just the baggage and the misunderstandings that we're bringing with us.

202
00:12:56,000 --> 00:12:59,000
What we're actually trying to do is do away with these expectations,

203
00:12:59,000 --> 00:13:02,000
the need for some kind of stability,

204
00:13:02,000 --> 00:13:07,000
and the need for some kind of pleasure, and the need to be able to control.

205
00:13:07,000 --> 00:13:11,000
What we're trying to do is be able to experience and interact,

206
00:13:11,000 --> 00:13:14,000
and be with reality whatever it is.

207
00:13:14,000 --> 00:13:23,000
And so what we're going to try to do is to give up these expectations

208
00:13:23,000 --> 00:13:26,000
to come to see that reality is not what we think it is.

209
00:13:26,000 --> 00:13:31,000
And this is what's going to happen when, for instance, as an example, you watch the stomach.

210
00:13:31,000 --> 00:13:35,000
If you say to yourself, rising, falling, and just watch the stomach rising,

211
00:13:35,000 --> 00:13:39,000
when it rises rising, when it falls falling,

212
00:13:39,000 --> 00:13:41,000
you're going to see all three of these things.

213
00:13:41,000 --> 00:13:43,000
You're going to see impermanence.

214
00:13:43,000 --> 00:13:47,000
So sometimes it will be deep, sometimes it will be shallow,

215
00:13:47,000 --> 00:13:52,000
sometimes it will be smooth, sometimes it will be in pieces, and so on.

216
00:13:52,000 --> 00:13:59,000
And you're going to see, basically, that it's changing.

217
00:13:59,000 --> 00:14:02,000
You're also going to see sometimes that it's uncomfortable,

218
00:14:02,000 --> 00:14:05,000
that there's a sense of physical discomfort,

219
00:14:05,000 --> 00:14:09,000
that the rising and the falling, because of our clinging to it,

220
00:14:09,000 --> 00:14:16,000
because we like certain states, and because we see it as being ours,

221
00:14:16,000 --> 00:14:19,000
it leads us to a state of discomfort.

222
00:14:19,000 --> 00:14:23,000
There's the stress in the stomach and the tension,

223
00:14:23,000 --> 00:14:27,000
and sometimes breath is not smooth and not comfortable.

224
00:14:27,000 --> 00:14:30,000
Because it's not according to our wishes.

225
00:14:30,000 --> 00:14:34,000
And we're going to also see that it's not according to our,

226
00:14:34,000 --> 00:14:37,000
it's not under our control.

227
00:14:37,000 --> 00:14:44,000
So sometimes it will go by itself, eventually,

228
00:14:44,000 --> 00:14:48,000
in higher stages, when the meditator gets more clear and inside,

229
00:14:48,000 --> 00:14:51,000
the rising and falling will appear to go by itself.

230
00:14:51,000 --> 00:14:54,000
But in the beginning, this is experienced by actually the feeling

231
00:14:54,000 --> 00:14:58,000
that we are controlling our breath, the feeling that we are making it,

232
00:14:58,000 --> 00:15:02,000
this misunderstanding that somehow we are controlling it,

233
00:15:02,000 --> 00:15:06,000
and that idea of control or the habit of trying to control things

234
00:15:06,000 --> 00:15:09,000
is going to create a great amount of suffering.

235
00:15:09,000 --> 00:15:12,000
This is how we see that it's not under our control.

236
00:15:12,000 --> 00:15:17,000
Because when we do exert our control, trying to make it rise and fall smoothly,

237
00:15:17,000 --> 00:15:21,000
I'm saying, okay, now rise, now fall, now rise, now rise.

238
00:15:21,000 --> 00:15:23,000
It's going to create stress and discomfort.

239
00:15:23,000 --> 00:15:27,000
You'll find that when you force it, you'll conceive,

240
00:15:27,000 --> 00:15:29,000
it feels like you're forcing it.

241
00:15:29,000 --> 00:15:32,000
You'll see that this is a great amount of stress and suffering.

242
00:15:32,000 --> 00:15:35,000
So this is seeing that actually it's not under your control

243
00:15:35,000 --> 00:15:38,000
and trying to control things is the real,

244
00:15:38,000 --> 00:15:43,000
a real source of stress and suffering.

245
00:15:43,000 --> 00:15:45,000
Now the problem is when these things arise,

246
00:15:45,000 --> 00:15:47,000
the first thing the meditator thinks,

247
00:15:47,000 --> 00:15:50,000
most meditators think is something's going wrong.

248
00:15:50,000 --> 00:15:52,000
They think that either A, this meditation is no good

249
00:15:52,000 --> 00:15:55,000
or B, they're not practicing it correctly.

250
00:15:55,000 --> 00:16:00,000
Now it's important to understand that actually a person who realizes these things

251
00:16:00,000 --> 00:16:05,000
who sees the changing, who sees the discomfort,

252
00:16:05,000 --> 00:16:09,000
who sees the uncontrollability of the experience.

253
00:16:09,000 --> 00:16:12,000
This is a person who is starting to understand reality,

254
00:16:12,000 --> 00:16:14,000
to see that things the way they are,

255
00:16:14,000 --> 00:16:17,000
and to change their misperceptions about reality,

256
00:16:17,000 --> 00:16:21,000
to open up and to free themselves of these expectations

257
00:16:21,000 --> 00:16:24,000
of the dependency on a specific type of experience

258
00:16:24,000 --> 00:16:29,000
and the inability to bear with other types of experience.

259
00:16:29,000 --> 00:16:32,000
Once we can experience this,

260
00:16:32,000 --> 00:16:34,000
which is actually quite a difficult thing to do,

261
00:16:34,000 --> 00:16:37,000
simply watch the stomach and all of its changes

262
00:16:37,000 --> 00:16:41,000
and all of its variations without becoming upset

263
00:16:41,000 --> 00:16:47,000
without becoming frustrated and angry and fallen to suffering.

264
00:16:47,000 --> 00:16:50,000
If once we can do this, then we can experience anything.

265
00:16:50,000 --> 00:16:54,000
We'll be able to find that simply watching the stomach

266
00:16:54,000 --> 00:16:58,000
for some time you'll be able to deal with all sorts of difficulty

267
00:16:58,000 --> 00:17:01,000
that appear in your life much better.

268
00:17:01,000 --> 00:17:04,000
You'll find much greater peace, happiness and freedom from suffering

269
00:17:04,000 --> 00:17:07,000
in your daily life, no matter what arises,

270
00:17:07,000 --> 00:17:09,000
no matter what problems you have in your life.

271
00:17:09,000 --> 00:17:13,000
So this is the true reason why we're practicing.

272
00:17:13,000 --> 00:17:18,000
Now this talk I'm giving, I wanted to give it for a while,

273
00:17:18,000 --> 00:17:21,000
but this is specifically in response to someone

274
00:17:21,000 --> 00:17:27,000
who had a specific experience in after meditation.

275
00:17:27,000 --> 00:17:30,000
And this deals with these,

276
00:17:30,000 --> 00:17:35,000
or it has to do with this idea of certain experiences arising,

277
00:17:35,000 --> 00:17:40,000
because what's going to happen once you begin to see

278
00:17:40,000 --> 00:17:45,000
this impermanence, this suffering nature of things

279
00:17:45,000 --> 00:17:48,000
and the non-self, the inability to control things.

280
00:17:48,000 --> 00:17:51,000
Once you start to see this, you're going to start to let go,

281
00:17:51,000 --> 00:17:54,000
and you're going to start to let go of any concept of things.

282
00:17:54,000 --> 00:17:59,000
So instead of seeing people as a person,

283
00:17:59,000 --> 00:18:04,000
you're going to experience them as a set of experiences,

284
00:18:04,000 --> 00:18:06,000
a set of phenomenon that arise.

285
00:18:06,000 --> 00:18:09,000
So you hear their voice and you'll take that as hearing, as a sound.

286
00:18:09,000 --> 00:18:12,000
When you see them, you'll take that as seeing.

287
00:18:12,000 --> 00:18:14,000
So the idea of a specific person falls apart

288
00:18:14,000 --> 00:18:17,000
and you wind up taking people as they are.

289
00:18:17,000 --> 00:18:20,000
And instead of holding grudges or having expectations

290
00:18:20,000 --> 00:18:23,000
where this person has to behave in this way,

291
00:18:23,000 --> 00:18:25,000
or this person is a bad person, and so on,

292
00:18:25,000 --> 00:18:27,000
you'll take them moment by moment.

293
00:18:27,000 --> 00:18:29,000
And so whatever they are in that moment,

294
00:18:29,000 --> 00:18:31,000
you'll respond to that.

295
00:18:31,000 --> 00:18:37,000
And you'll be able to deal with them in a wise and impartial manner.

296
00:18:37,000 --> 00:18:42,000
You'll find yourself free from all of the baggage

297
00:18:42,000 --> 00:18:45,000
that we carry around with us.

298
00:18:45,000 --> 00:18:52,000
These cycles of vengeance and the feuding and so on,

299
00:18:52,000 --> 00:18:56,000
all of the problems that we have with people will disappear.

300
00:18:56,000 --> 00:18:58,000
All the problems that we have with everything,

301
00:18:58,000 --> 00:19:00,000
when you see something that, before, would scare you,

302
00:19:00,000 --> 00:19:02,000
when you experience something that, before,

303
00:19:02,000 --> 00:19:06,000
would make you afraid or stressful or stressed or worried.

304
00:19:06,000 --> 00:19:08,000
You'll find that you don't take it in that way anymore.

305
00:19:08,000 --> 00:19:09,000
You take it for what it is.

306
00:19:09,000 --> 00:19:11,000
When you're, for instance, in the airplane,

307
00:19:11,000 --> 00:19:14,000
when people are afraid of flying and so on,

308
00:19:14,000 --> 00:19:17,000
it's because they can see it to be much more than it is.

309
00:19:17,000 --> 00:19:19,000
Once you start to see things,

310
00:19:19,000 --> 00:19:23,000
simply for what they are, you give up any attachment to them.

311
00:19:23,000 --> 00:19:27,000
You see that they're impermanent, unsatisfying and uncontrollable.

312
00:19:27,000 --> 00:19:29,000
Because you're not clinging to them and saying,

313
00:19:29,000 --> 00:19:31,000
oh, this is going to make me happy,

314
00:19:31,000 --> 00:19:34,000
or how can I fix this to make it better,

315
00:19:34,000 --> 00:19:37,000
you're going to see it simply for what it is.

316
00:19:37,000 --> 00:19:40,000
And you're going to see the, for instance, the experience of flying

317
00:19:40,000 --> 00:19:43,000
as a series of experiences, a series of phenomenon,

318
00:19:43,000 --> 00:19:46,000
a phenomenon that you experience as seeing or hearing,

319
00:19:46,000 --> 00:19:48,000
or so on, and sitting in a plane

320
00:19:48,000 --> 00:19:50,000
is just going to be exactly what it is.

321
00:19:50,000 --> 00:19:52,000
There's going to be no baggage attached to it,

322
00:19:52,000 --> 00:19:57,000
and people are able to overcome fear of flying as an example.

323
00:19:57,000 --> 00:20:00,000
Arguments that we have when someone's yelling at you

324
00:20:00,000 --> 00:20:02,000
and you say to yourself, hearing, hearing,

325
00:20:02,000 --> 00:20:05,000
and you're simply aware of it as sound.

326
00:20:05,000 --> 00:20:09,000
You don't process the sound as good or bad.

327
00:20:09,000 --> 00:20:13,000
You simply know what's being said, and you're aware of the conversation,

328
00:20:13,000 --> 00:20:15,000
aware of the things that are being said,

329
00:20:15,000 --> 00:20:17,000
and you take it for what it is.

330
00:20:17,000 --> 00:20:21,000
Once you process it, you process it based on the meaning of the words,

331
00:20:21,000 --> 00:20:24,000
and you're able to see it impartially.

332
00:20:24,000 --> 00:20:26,000
Because, again, you don't have any idea

333
00:20:26,000 --> 00:20:28,000
that you're going to be able to fix this and make it better

334
00:20:28,000 --> 00:20:30,000
when bad things occur.

335
00:20:30,000 --> 00:20:32,000
You're just going to take it for it.

336
00:20:32,000 --> 00:20:34,000
You don't have any conception that it's bad.

337
00:20:34,000 --> 00:20:36,000
You simply see it as it is.

338
00:20:36,000 --> 00:20:39,000
But the side effect here, that was brought up,

339
00:20:39,000 --> 00:20:42,000
is that this person said they would look in the mirror

340
00:20:42,000 --> 00:20:45,000
and they didn't recognize themselves.

341
00:20:45,000 --> 00:20:50,000
And I think this is a good indicator

342
00:20:50,000 --> 00:20:53,000
that this person is actually practicing properly.

343
00:20:53,000 --> 00:20:55,000
They have good concentrations.

344
00:20:55,000 --> 00:20:58,000
So what they're seeing in the mirror is just light,

345
00:20:58,000 --> 00:20:59,000
and it takes a little bit of time,

346
00:20:59,000 --> 00:21:02,000
or it may not even occur sometimes.

347
00:21:02,000 --> 00:21:05,000
That this is mean that this is a person.

348
00:21:05,000 --> 00:21:08,000
The recognition disappears, or not exactly the recognition,

349
00:21:08,000 --> 00:21:11,000
but the seeing occurs in a different manner.

350
00:21:11,000 --> 00:21:14,000
Whereas for most of us, when we see something,

351
00:21:14,000 --> 00:21:16,000
there's so much baggage that occurs immediately.

352
00:21:16,000 --> 00:21:17,000
We like it.

353
00:21:17,000 --> 00:21:19,000
When we first recognize it, then we like it,

354
00:21:19,000 --> 00:21:22,000
or we dislike it, our minds move so quickly.

355
00:21:22,000 --> 00:21:24,000
And in a chaotic state most of the time,

356
00:21:24,000 --> 00:21:27,000
so we don't even have time to stop and realize

357
00:21:27,000 --> 00:21:29,000
that what we're seeing is light,

358
00:21:29,000 --> 00:21:31,000
and that it's an experience,

359
00:21:31,000 --> 00:21:34,000
and to catch what's going on.

360
00:21:34,000 --> 00:21:37,000
Once we practice meditation and we develop concentration

361
00:21:37,000 --> 00:21:39,000
and we're able to see things as they are,

362
00:21:39,000 --> 00:21:43,000
we're simply going to experience it for light touching my eye

363
00:21:43,000 --> 00:21:47,000
and a seeing experience that arises.

364
00:21:47,000 --> 00:21:49,000
It's not something out there.

365
00:21:49,000 --> 00:21:52,000
When you look in the mirror, you project and think there's a person.

366
00:21:52,000 --> 00:21:54,000
But actually, it's not.

367
00:21:54,000 --> 00:22:02,000
It's light touching the mirror and coming back and touching your face.

368
00:22:02,000 --> 00:22:05,000
You're eye, coming from the face to the mirror,

369
00:22:05,000 --> 00:22:08,000
then back to your eye, or however.

370
00:22:08,000 --> 00:22:13,000
And so it's important in this case,

371
00:22:13,000 --> 00:22:16,000
I think this explanation is important to help us

372
00:22:16,000 --> 00:22:22,000
to avoid this concern that somehow something's wrong.

373
00:22:22,000 --> 00:22:25,000
People will go home to their after meditation courses,

374
00:22:25,000 --> 00:22:29,000
to their families, and their families will become quite upset

375
00:22:29,000 --> 00:22:32,000
because their children are no longer clinging

376
00:22:32,000 --> 00:22:34,000
or no longer attaching to them,

377
00:22:34,000 --> 00:22:37,000
so as a result, there's not as much joy.

378
00:22:37,000 --> 00:22:41,000
It seems not as much affection and so on.

379
00:22:41,000 --> 00:22:45,000
But this is simply because these people don't realize

380
00:22:45,000 --> 00:22:48,000
that this form of joy is associated with clinging,

381
00:22:48,000 --> 00:22:51,000
and it only leads to expectations,

382
00:22:51,000 --> 00:22:53,000
and when those expectations are not met,

383
00:22:53,000 --> 00:22:56,000
it leads to anger and fighting and frustration,

384
00:22:56,000 --> 00:22:59,000
and eventually leads to sadness and despair

385
00:22:59,000 --> 00:23:02,000
when there's a breakup.

386
00:23:02,000 --> 00:23:05,000
So this is natural.

387
00:23:05,000 --> 00:23:07,000
The person doesn't make a decision.

388
00:23:07,000 --> 00:23:10,000
I'm not going to attach to you anymore because that's bad.

389
00:23:10,000 --> 00:23:13,000
They realize for themselves that there's no reason,

390
00:23:13,000 --> 00:23:16,000
there's no good that comes from attachment from clinging,

391
00:23:16,000 --> 00:23:19,000
and that in fact taking things as they are

392
00:23:19,000 --> 00:23:23,000
is a much more peaceful state of experience,

393
00:23:23,000 --> 00:23:24,000
a state of reality.

394
00:23:24,000 --> 00:23:26,000
And so they change their way of behaving

395
00:23:26,000 --> 00:23:29,000
and their way of approaching their lives

396
00:23:29,000 --> 00:23:31,000
and the world around them.

397
00:23:31,000 --> 00:23:34,000
So just to point out,

398
00:23:34,000 --> 00:23:36,000
just to point out, in this video,

399
00:23:36,000 --> 00:23:39,000
the idea that I like to point out is,

400
00:23:39,000 --> 00:23:43,000
there are these certain experiences that we will come across,

401
00:23:43,000 --> 00:23:45,000
and we should be open to them,

402
00:23:45,000 --> 00:23:48,000
and we should learn to be able to open up

403
00:23:48,000 --> 00:23:50,000
to unpleasant situations.

404
00:23:50,000 --> 00:23:53,000
It doesn't mean that our meditation will always be unpleasant.

405
00:23:53,000 --> 00:23:55,000
People can practice in sight meditation

406
00:23:55,000 --> 00:23:58,000
and have great pleasure and happiness.

407
00:23:58,000 --> 00:24:01,000
You can even have great states of calm and so on.

408
00:24:01,000 --> 00:24:03,000
But as I said in the other video,

409
00:24:03,000 --> 00:24:05,000
and I'll say it again here,

410
00:24:05,000 --> 00:24:08,000
we shouldn't cling to the good ones.

411
00:24:08,000 --> 00:24:10,000
We should also not cling to the bad ones,

412
00:24:10,000 --> 00:24:13,000
and we should not pay any attention or put any weight

413
00:24:13,000 --> 00:24:17,000
on the fact that we might have unpleasant experiences.

414
00:24:17,000 --> 00:24:21,000
And that in fact what we're going to realize is that there's no benefit

415
00:24:21,000 --> 00:24:26,000
or there's no true happiness in any experience

416
00:24:26,000 --> 00:24:29,000
that no experience can bring you happiness.

417
00:24:29,000 --> 00:24:32,000
If you're not already happy, if you're not already at peace with yourself,

418
00:24:32,000 --> 00:24:35,000
there's nothing that you will experience

419
00:24:35,000 --> 00:24:37,000
that is going to bring that to you.

420
00:24:37,000 --> 00:24:40,000
It's only your own freedom of mind,

421
00:24:40,000 --> 00:24:45,000
your own ability to accept ability to live with,

422
00:24:45,000 --> 00:24:52,000
to live with change, to live with discomfort,

423
00:24:52,000 --> 00:24:56,000
to live with the uncontrollability of the universe,

424
00:24:56,000 --> 00:24:59,000
in peace and happiness and freedom from suffering.

425
00:24:59,000 --> 00:25:02,000
Once you come to see these things

426
00:25:02,000 --> 00:25:04,000
and once you come to understand them

427
00:25:04,000 --> 00:25:08,000
and appreciate and accept and realize that this is the nature of reality,

428
00:25:08,000 --> 00:25:12,000
then you will find truth is happiness and freedom from suffering.

429
00:25:12,000 --> 00:25:14,000
When these things come up in your meditation,

430
00:25:14,000 --> 00:25:18,000
the important thing is to not be discouraged and not be frustrated,

431
00:25:18,000 --> 00:25:23,000
to appreciate it and to learn to acknowledge and accept it.

432
00:25:23,000 --> 00:25:25,000
So when you have pain and pain,

433
00:25:25,000 --> 00:25:27,000
when the stomach is uncomfortable,

434
00:25:27,000 --> 00:25:33,000
say to uncomfortable or discomfort or disliking or angry or upset and so on,

435
00:25:33,000 --> 00:25:37,000
when things are not the way you are, when it gives rise to frustration,

436
00:25:37,000 --> 00:25:41,000
to be aware of the frustration and to be aware of this situation.

437
00:25:41,000 --> 00:25:44,000
And if you do this and continue on in this way,

438
00:25:44,000 --> 00:25:47,000
eventually the frustration will disappear.

439
00:25:47,000 --> 00:25:50,000
Your mind will be forced to accept.

440
00:25:50,000 --> 00:25:56,000
Your mind will be forced to give up its misunderstandings.

441
00:25:56,000 --> 00:26:01,000
Once you see reality, you see that it's like this again and again

442
00:26:01,000 --> 00:26:05,000
and again, eventually the mind is forced to give up this delusion,

443
00:26:05,000 --> 00:26:08,000
this idea that somehow it's not like that.

444
00:26:08,000 --> 00:26:10,000
The idea that it can be different

445
00:26:10,000 --> 00:26:14,000
once you try and try again, your mind tries to find happiness,

446
00:26:14,000 --> 00:26:19,000
cling things and realizes that it's only leading again and again to suffering,

447
00:26:19,000 --> 00:26:23,000
then the mind will give up and the mind will stop looking for happiness,

448
00:26:23,000 --> 00:26:29,000
in external objects and things that have no ability to bring true happiness.

449
00:26:29,000 --> 00:26:34,000
So this is a video on basically what insight meditation is,

450
00:26:34,000 --> 00:26:39,000
if you practice in this way, it is the path which leads us to see clearly

451
00:26:39,000 --> 00:26:45,000
and become free from all suffering and stress.

452
00:26:45,000 --> 00:26:50,000
And so I'd like to wish everyone all the best and good luck on your path

453
00:26:50,000 --> 00:26:56,000
and hope that you two are able to find true insight into the nature of reality

454
00:26:56,000 --> 00:27:01,000
and thereby true peace, happiness and freedom from suffering.

455
00:27:01,000 --> 00:27:11,000
Thanks for tuning in and all the best.

