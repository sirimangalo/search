1
00:00:00,000 --> 00:00:12,820
Okay, good evening everyone. Welcome to our evening broadcast evening down the

2
00:00:12,820 --> 00:00:24,260
talk. Tonight's topic is a challenging one. It's not going to be long. There's

3
00:00:24,260 --> 00:00:34,140
not too much to say but I mean sometimes we sugarcoat the truth. I think I'm

4
00:00:34,140 --> 00:00:41,200
guilty of that making Buddhism sound more pleasant or more accommodating than it

5
00:00:41,200 --> 00:00:53,580
actually is and the truth is the Buddha really saw our situation as human beings

6
00:00:53,580 --> 00:01:01,740
was quite wretched. There's no escaping that. So when we talk about Buddhism as

7
00:01:01,740 --> 00:01:08,940
being pessimistic we can deny it all we want but to some extent it's not

8
00:01:08,940 --> 00:01:13,060
pessimistic. I've talked about that. That word has too much associated with it

9
00:01:13,060 --> 00:01:18,820
but it definitely doesn't look favorable upon it. It makes a claim that you can

10
00:01:18,820 --> 00:01:26,340
argue you could say it's pessimistic because you might disagree with it but

11
00:01:26,340 --> 00:01:39,700
the argument is that our existence as human beings is quite wretched and it has

12
00:01:39,700 --> 00:01:49,340
very much to do with the fact that we're dependent. We are as slaves as sick

13
00:01:49,340 --> 00:02:00,020
people in ways that we don't realize. We think that we're happy sometimes. We don't

14
00:02:00,020 --> 00:02:07,300
really understand what happiness is. We think we do but when we think of as

15
00:02:07,300 --> 00:02:17,260
happiness it turns out to be a cause for great suffering. It turns out to be a

16
00:02:17,260 --> 00:02:30,180
inherent problem with our way of looking at the world based on the fact that

17
00:02:30,180 --> 00:02:37,780
we are very much dependent and therefore at the mercy of and the

18
00:02:37,780 --> 00:02:46,820
vicissitudes of life. So this particular sutat it's in the Sanyuta Nikaya

19
00:02:46,820 --> 00:02:53,780
Nidana Sanyuta which is a wonderful Sanyuta it's a Sanyuta just means

20
00:02:53,780 --> 00:03:00,300
collection. So it's this collection of discourses based around Petitya Samupada. So

21
00:03:00,300 --> 00:03:04,460
if you want to learn about dependent origination this is the chapter to read

22
00:03:04,460 --> 00:03:09,540
the collection of sutas to read. Sanyuta Nikaya is really wonderful it's just

23
00:03:09,540 --> 00:03:16,660
very very big so it's not an easy book to really go through. That doesn't mean

24
00:03:16,660 --> 00:03:23,660
you shouldn't. But this is number 63 that's what's it called. Puta

25
00:03:23,660 --> 00:03:32,420
mang supa masut. Upa mai is a simile. Mang samin's flesh and puta means sun or

26
00:03:32,420 --> 00:03:40,300
child. So the simile of the flesh of the child. It's actually the

27
00:03:40,300 --> 00:03:46,780
sutas actually about food. Remember a few days ago I think or some some days

28
00:03:46,780 --> 00:03:57,020
ago. I mentioned this as the first of the ten darakapana kumarapana. The

29
00:03:57,020 --> 00:04:05,460
ten questions of the of the young child. The first one is Samba yasata

30
00:04:05,460 --> 00:04:11,380
harititika. All beings subsist on food and I said there are four types of food.

31
00:04:11,380 --> 00:04:18,540
When the sutas lays them out the four types of food are Kabalinka. Kabalinka

32
00:04:18,540 --> 00:04:23,540
are. It's a material food.

33
00:04:23,540 --> 00:04:34,420
Passahara which is or nutriment is a better word. The nutriment of contact. The

34
00:04:34,420 --> 00:04:42,940
third is mano sanjaitana. We are mano sanjaitana. The nutriment of mental

35
00:04:42,940 --> 00:04:52,740
intention or volition. And the fourth is vinyana hara which is the food of

36
00:04:52,740 --> 00:04:57,980
consciousness, the nourishment of consciousness and nutriment. And so what it

37
00:04:57,980 --> 00:05:05,020
means by nutriment is again this dependency. It's the cause. I mean the whole

38
00:05:05,020 --> 00:05:09,820
of this collection and really all of the Buddha's teaching is about seeing

39
00:05:09,820 --> 00:05:15,660
cause and effect, about learning how all of who we are and what we experience

40
00:05:15,660 --> 00:05:25,860
is it's dependent on causality. We got the way we are for a reason and we

41
00:05:25,860 --> 00:05:30,980
learn about those reasons and we realize how we're still becoming more and

42
00:05:30,980 --> 00:05:38,420
more based on the food that we take in nourishment, the nutriment. If we take in

43
00:05:38,420 --> 00:05:44,340
bad and if we take on bad and if we cultivate the bad then bad is the result.

44
00:05:44,340 --> 00:05:51,460
We cultivate the good and good is the result. It's quite simple.

45
00:05:51,460 --> 00:05:56,020
And so these four the Buddha laid out as sort of an outline of the kinds of

46
00:05:56,020 --> 00:06:03,620
things that have an effect on us. The kind of things, the different kind of

47
00:06:03,620 --> 00:06:14,580
things that change us, that feed who we are. And I said in the beginning he

48
00:06:14,580 --> 00:06:19,500
sees it as quite wretched, the situation of dealing with these these types of

49
00:06:19,500 --> 00:06:24,380
food and they don't sound bad at all I suppose when you when you think about

50
00:06:24,380 --> 00:06:30,620
when we hear about them because we're quite enamored with them. But the Buddha in

51
00:06:30,620 --> 00:06:34,300
the suta he lays out a description of these four. So he said how should you

52
00:06:34,300 --> 00:06:39,820
think of physical nutriment when we think of the food we

53
00:06:39,820 --> 00:06:44,140
how should we think of it? And he gives the simile of the

54
00:06:44,140 --> 00:06:50,060
flesh of the child. The simile is this or the example is this. The analogy I

55
00:06:50,060 --> 00:06:56,140
guess is this. It's a allegory I don't know the story. He says imagine there

56
00:06:56,140 --> 00:07:02,540
were there was this couple a husband and wife a wife and husband and a woman

57
00:07:02,540 --> 00:07:08,380
and a man with their son traveling through the desert. They had to escape a

58
00:07:08,380 --> 00:07:12,940
plague or something and they hadn't brought much food with them and they didn't

59
00:07:12,940 --> 00:07:17,100
realize how long it was and they get halfway through the desert and they run

60
00:07:17,100 --> 00:07:23,340
out of food and so they're dying and they're both dying and their son is

61
00:07:23,340 --> 00:07:27,340
their child is dying and they can no longer nurse him because they when

62
00:07:27,340 --> 00:07:31,580
there's breast milk dies out and dries out pro whatever.

63
00:07:34,140 --> 00:07:37,820
And he's not he's certainly not advocating this but with this the husband and

64
00:07:37,820 --> 00:07:44,620
wife thinks of themselves is that they're all going to die and he's not

65
00:07:44,620 --> 00:07:48,380
abdicating this. This is not the teaching but he says imagine that these two

66
00:07:48,380 --> 00:07:52,780
people who are ordinary people who have wrong ideas about things

67
00:07:52,780 --> 00:07:58,940
decide that they're going to kill their child and eat his his his flesh

68
00:07:58,940 --> 00:08:05,420
his or her flesh which is a horrific image really but but he purposefully

69
00:08:05,420 --> 00:08:10,780
makes it as horrific as possible and he's not saying this is not horrific

70
00:08:10,780 --> 00:08:17,420
but what he says is he says do you think that this man and this woman who

71
00:08:17,420 --> 00:08:24,300
loved their child dearly would eat that food for for entertainment do you

72
00:08:24,300 --> 00:08:29,340
think they'd they'd sit around remarking on how delicious it is

73
00:08:29,340 --> 00:08:33,900
he asks the monks would they would they eat it for enjoyment or I mean it's

74
00:08:33,900 --> 00:08:40,860
just the most awful example but he says would they only eat it for

75
00:08:40,860 --> 00:08:45,740
for crossing this desert and they say yes that

76
00:08:45,740 --> 00:08:49,900
in this wretched state that's what they would eat it for and he says this is how

77
00:08:49,900 --> 00:08:57,340
you should see food this is how you should see food and and what is what it

78
00:08:57,340 --> 00:09:01,420
does is it points out how wretched we are

79
00:09:01,420 --> 00:09:06,860
that we don't see this our dependency on food

80
00:09:07,980 --> 00:09:11,980
we think it's great that we can eat

81
00:09:12,220 --> 00:09:18,140
not seeing how ensnared we are how caught up we are

82
00:09:21,500 --> 00:09:28,700
so it's a wake-up call for us to see using this food for pleasure is like

83
00:09:28,700 --> 00:09:34,140
it's like chickens clocking about in the pen

84
00:09:34,140 --> 00:09:38,300
having no clue that they're going to be slaughtered

85
00:09:43,260 --> 00:09:47,900
and it's the same because we we we go through our lives

86
00:09:47,900 --> 00:09:53,260
enjoying things like food or all the physical pleasure

87
00:09:53,260 --> 00:09:59,820
sight sound smells not just tastes feelings

88
00:10:01,820 --> 00:10:07,500
and we suffer terribly for this because we become ensnared we become

89
00:10:07,500 --> 00:10:11,820
well at the very least we're we're

90
00:10:12,700 --> 00:10:18,780
hopelessly ignorant of the reality or situation and so unable to deal with

91
00:10:18,780 --> 00:10:22,700
stress and suffering unable to deal with all the age sickness that

92
00:10:22,700 --> 00:10:27,820
loss trauma

93
00:10:27,820 --> 00:10:31,260
instead we live our lives praying and hoping that these

94
00:10:31,260 --> 00:10:34,620
sorts of things don't happen to us

95
00:10:34,620 --> 00:10:39,100
gonna live forever right or at the very least I won't be

96
00:10:39,100 --> 00:10:44,140
I won't meet with loss or trauma or

97
00:10:44,140 --> 00:10:53,820
trouble difficulty

98
00:10:53,820 --> 00:10:57,180
well we are like this as it is woman and man crossing the desert

99
00:10:57,180 --> 00:11:00,540
we're in a wretched situation where we need these things where we are not

100
00:11:00,540 --> 00:11:05,100
independent of them and moreover we are intoxicated by them

101
00:11:05,100 --> 00:11:08,540
we're caught up in in food how wonderful it is

102
00:11:08,540 --> 00:11:12,220
how much happiness it brings us

103
00:11:12,220 --> 00:11:15,660
when in fact we can see that it's just bringing we can see it's as we

104
00:11:15,660 --> 00:11:19,580
meditate that it's just bringing us stress and

105
00:11:19,580 --> 00:11:25,180
like in like so many other physical things it's

106
00:11:25,180 --> 00:11:29,420
if we don't it's something that we just need

107
00:11:29,420 --> 00:11:32,460
we can't be without

108
00:11:37,420 --> 00:11:39,740
and he says that I mean it's an optimistic

109
00:11:39,740 --> 00:11:43,660
teaching though what many people might not agree the optimism is

110
00:11:43,660 --> 00:11:46,380
when you realize there's you free yourself

111
00:11:46,380 --> 00:11:49,340
if you really see food in this way

112
00:11:49,340 --> 00:11:53,180
I'm not something to enjoy something that's fraught with

113
00:11:53,180 --> 00:11:58,940
problems and caught up in the suffering of some sorrow

114
00:11:59,900 --> 00:12:03,820
and you have no attachment to it

115
00:12:03,820 --> 00:12:07,260
I would have said if you understand this about food and you understand all of the

116
00:12:07,260 --> 00:12:10,460
senses

117
00:12:10,460 --> 00:12:13,180
and he says then there is no fetter

118
00:12:13,180 --> 00:12:17,660
and this is the key is that happiness is freedom from the fetters

119
00:12:17,660 --> 00:12:22,860
freedom from any fetter freedom from any bondage

120
00:12:24,380 --> 00:12:27,580
so that whether you get food or don't get food you're

121
00:12:27,580 --> 00:12:38,140
here at peace

122
00:12:38,140 --> 00:12:41,740
there was someone was asking me why wouldn't an enlightened being just

123
00:12:41,740 --> 00:12:43,580
starve themselves and in fact

124
00:12:43,580 --> 00:12:46,940
but apparently it's something that's in the text there are enlightened

125
00:12:46,940 --> 00:12:53,580
beings who just wouldn't eat not purposefully perhaps but

126
00:12:53,580 --> 00:12:57,740
would just not be concerned about going to get food

127
00:13:01,500 --> 00:13:05,020
and it's not like certainly not the way of all enlightened beings

128
00:13:05,020 --> 00:13:07,420
but there's nothing wrong with this

129
00:13:07,420 --> 00:13:10,300
I mean the point is when you're enlightened you're free

130
00:13:10,300 --> 00:13:16,700
you don't require food to be happy to satiate you to satisfy you to

131
00:13:16,700 --> 00:13:24,700
console you

132
00:13:28,780 --> 00:13:31,580
not sure how this teaching tonight is going to be taken it doesn't get any

133
00:13:31,580 --> 00:13:34,620
better from here

134
00:13:34,620 --> 00:13:36,940
I don't care

135
00:13:36,940 --> 00:13:43,180
number two this is this is the real stuff no this is the Buddha

136
00:13:43,180 --> 00:13:48,940
number two is contact so what is the food of contact?

137
00:13:48,940 --> 00:13:52,860
Buddha says suppose there was a flayed cow

138
00:13:52,860 --> 00:13:59,980
there's a word that might not be familiar to everyone FL AYED flayed means

139
00:13:59,980 --> 00:14:05,340
whipped right but flayed actually means the skin is broken

140
00:14:05,340 --> 00:14:09,260
so it's a cow that's someone whipped or or maybe flayed to see use a

141
00:14:09,260 --> 00:14:13,900
different instrument I'm not quite sure but to the point where

142
00:14:13,900 --> 00:14:21,100
it starts to bleed and he says if if this cow were to stand

143
00:14:21,100 --> 00:14:27,180
exposed to a wall the creature's dwelling in the wall would nibble at her

144
00:14:27,180 --> 00:14:31,900
if it's still exposed to a tree then the creatures in the tree would nibble at

145
00:14:31,900 --> 00:14:36,060
her if she stands exposed to water the creature's

146
00:14:36,060 --> 00:14:39,820
dwelling in the water wouldn't nibble at her if she extends exposed to open

147
00:14:39,820 --> 00:14:45,020
air the flies in the air wouldn't nibble at her and

148
00:14:45,020 --> 00:14:49,660
whatever that flayed cloud stands exposed to the creatures dwelling there

149
00:14:49,660 --> 00:14:52,140
wouldn't nibble at her

150
00:14:52,140 --> 00:14:57,100
so he's setting up the the most wretched example of contact

151
00:14:57,100 --> 00:15:03,180
this wretchedness of being susceptible to contact

152
00:15:03,180 --> 00:15:09,260
he doesn't see contact he didn't see contact as positive in any way

153
00:15:09,260 --> 00:15:12,540
we normally think of contact as a great thing you know when you come in

154
00:15:12,540 --> 00:15:17,580
contact with pleasant pleasant sounds and sights

155
00:15:17,580 --> 00:15:24,700
taste smells feelings particularly feels it feelings

156
00:15:24,700 --> 00:15:28,380
contact is all about feelings when you come in contact with something it

157
00:15:28,380 --> 00:15:33,100
makes you happy we think we think of this as pleasure in that as

158
00:15:33,100 --> 00:15:40,380
pleasure or this pleasure that pleasure as happiness

159
00:15:40,380 --> 00:15:45,980
so the Buddha really did see all of this as as tortuous

160
00:15:45,980 --> 00:15:53,580
as a cause for all of our suffering and and the real deep reason

161
00:15:53,580 --> 00:15:57,500
you can you can try to explain it philosophically like oh because it changes

162
00:15:57,500 --> 00:16:02,540
and so on but the deep reason is simply because it arises in

163
00:16:02,540 --> 00:16:08,060
seizes it's incessant is what you'll see in your practice as

164
00:16:08,060 --> 00:16:12,860
as you get to these later stages you'll see things arising and

165
00:16:12,860 --> 00:16:18,700
ceasing and and become uh equanimous towards them

166
00:16:18,700 --> 00:16:22,380
see that that's all it is it's a bunch of arising and ceasing or

167
00:16:22,380 --> 00:16:29,340
it never stops you're incessantly hounded by

168
00:16:29,340 --> 00:16:33,500
experience and and thereby contact

169
00:16:36,060 --> 00:16:39,660
this is what causes one to let go

170
00:16:39,660 --> 00:16:42,700
and the point being that it can only it only hurts us because we have

171
00:16:42,700 --> 00:16:48,220
expectations and attachment to it once we see it as this as

172
00:16:48,220 --> 00:16:55,900
as worthless and we can experience then we can experience it for just

173
00:16:55,900 --> 00:17:02,380
fine you know the incessant the incessant nature of it the

174
00:17:02,380 --> 00:17:07,820
hounding of experience there's no longer hounding us because we no longer

175
00:17:07,820 --> 00:17:15,020
are susceptible to it no longer enamored by it or caught up with

176
00:17:15,020 --> 00:17:23,740
by it all contact should be seen clearly as not this is not

177
00:17:23,740 --> 00:17:28,380
a or the way to find happiness

178
00:17:31,420 --> 00:17:36,780
it's number two number three is mental volition

179
00:17:36,780 --> 00:17:42,140
mental volition this is karma our intentions

180
00:17:42,140 --> 00:17:47,980
when we want to kill someone or want to hurt someone or when we want to be

181
00:17:47,980 --> 00:17:56,220
happy when we want food when we want sex when we want romance

182
00:17:56,220 --> 00:18:03,980
music anything when we want to be free from suffering

183
00:18:03,980 --> 00:18:09,340
mental volition when we inclined towards something in the Buddha

184
00:18:09,340 --> 00:18:15,180
again saw this as wretched even our volitions our desires our ambitions

185
00:18:15,180 --> 00:18:18,780
wretcheding saw them

186
00:18:18,780 --> 00:18:23,740
he said some imagine here's another allegory

187
00:18:23,740 --> 00:18:28,620
is imagine there was a charcoal pit a big pit with

188
00:18:28,620 --> 00:18:35,820
blazing hot embers glowing coals we've ever seen one of these pits where they

189
00:18:35,820 --> 00:18:41,180
put coals and it's just a pit of very hot

190
00:18:41,180 --> 00:18:48,140
and then a man came along and then someone who didn't want to die

191
00:18:48,140 --> 00:18:52,860
desiring happiness of an averse to suffering

192
00:18:53,900 --> 00:18:59,580
then two strong men would grab him by one arm

193
00:18:59,580 --> 00:19:04,380
and drag him towards the charcoal pit

194
00:19:04,380 --> 00:19:08,140
what do you think that man's volition would be

195
00:19:08,140 --> 00:19:11,660
that man's volition would be to get far away

196
00:19:11,660 --> 00:19:15,420
his longing would be to get far away he would wish

197
00:19:15,420 --> 00:19:21,980
it his wish would be to get far away from the charcoal pit

198
00:19:21,980 --> 00:19:25,020
and he said this is how you should for what reason either

199
00:19:25,020 --> 00:19:29,100
because if I fall into this charcoal pit anyway that's not so far

200
00:19:29,100 --> 00:19:34,300
he says this is how you should see mental volition

201
00:19:34,300 --> 00:19:37,260
it's the most wretched you think of this and this is wretched this man

202
00:19:37,260 --> 00:19:42,380
wants something so bad right

203
00:19:42,380 --> 00:19:45,340
and in fact the funny thing is it's

204
00:19:45,340 --> 00:19:50,140
I mean it's a hard to believe if you've never really studied or of course

205
00:19:50,140 --> 00:19:52,380
practiced

206
00:19:52,380 --> 00:19:56,540
but it's teaching but it's not actually the charcoal pit that's going to make

207
00:19:56,540 --> 00:20:00,940
him suffer if he was okay about being dragged somehow

208
00:20:00,940 --> 00:20:08,380
amazingly okay with being dragged towards the charcoal pit and okay with

209
00:20:08,380 --> 00:20:13,020
having his skin burnt up and okay with the pain

210
00:20:13,020 --> 00:20:17,660
and okay with the seizures and whatever else would it would happen

211
00:20:17,660 --> 00:20:23,660
okay with dying then he wouldn't suffer

212
00:20:23,660 --> 00:20:29,020
but it's a great example of how it's it's an extreme example of how volition

213
00:20:29,020 --> 00:20:33,660
causes such great suffering I mean if you wake up

214
00:20:33,660 --> 00:20:36,300
if he were to if they were to knock him out and throw him in the

215
00:20:36,300 --> 00:20:41,340
charcoal pit and then he wakes up in it he would suffer less right

216
00:20:41,340 --> 00:20:44,540
could you imagine being dragged towards the charcoal pit

217
00:20:44,540 --> 00:20:47,500
how much suffering even before he gets there

218
00:20:47,500 --> 00:20:51,660
the anguish the horror

219
00:20:51,660 --> 00:20:54,780
right and he has to be not even there yet it's like why are you

220
00:20:54,780 --> 00:21:01,580
suffering so much you're not even feeling it yet because of volition

221
00:21:01,580 --> 00:21:05,100
and he said this is how we should see volition because it's all it's all just

222
00:21:05,100 --> 00:21:09,340
trying to escape suffering and find happiness where

223
00:21:09,340 --> 00:21:15,180
you run around like chickens with our heads cut off looking for

224
00:21:15,180 --> 00:21:20,220
satisfaction and we heard ourselves and we heard other people in

225
00:21:20,220 --> 00:21:25,500
my cause also I mean look at all the problems in society in the world

226
00:21:25,500 --> 00:21:31,180
how many of them are caused by this greed and ambition and our

227
00:21:31,180 --> 00:21:38,140
conflicting desires so much stress and suffering

228
00:21:38,140 --> 00:21:42,380
I think of all the stress and suffering in this world

229
00:21:42,380 --> 00:21:45,660
all of you come here and and you tell them we talk about

230
00:21:45,660 --> 00:21:52,060
the stress and suffering that you have it it gives you perspective to think

231
00:21:52,060 --> 00:21:57,740
that the world is full of people like us suffering tortured

232
00:21:58,700 --> 00:22:03,180
wretched

233
00:22:04,460 --> 00:22:07,340
yeah well and if you don't like this you welcome there's lots of other

234
00:22:07,340 --> 00:22:13,980
you YouTube stuff out there and for those of you staying with me well

235
00:22:13,980 --> 00:22:17,500
I think you kind of get what I'm saying

236
00:22:17,500 --> 00:22:23,100
this is deep stuff it's hard to swallow I think

237
00:22:23,100 --> 00:22:26,300
if you've never really practiced it just sounds bad pessimistic it's really

238
00:22:26,300 --> 00:22:31,980
not it's realistic and it's optimistic

239
00:22:31,980 --> 00:22:36,860
because it talks about how to find real happiness

240
00:22:36,860 --> 00:22:43,020
but it wasn't afraid of seeing it of calling it like it is

241
00:22:43,020 --> 00:22:47,980
how how how this would be received by the world how it will be received

242
00:22:47,980 --> 00:22:51,580
tonight by YouTube I don't know

243
00:22:52,780 --> 00:22:57,180
doesn't really matter truth is the truth

244
00:22:58,060 --> 00:23:02,700
the fourth one is consciousness and he says how should you understand

245
00:23:02,700 --> 00:23:06,380
consciousness yeah it's not any better

246
00:23:06,380 --> 00:23:10,380
so we have consciousness we're always conscious right

247
00:23:10,380 --> 00:23:14,540
maybe except when we're sleeping or

248
00:23:15,500 --> 00:23:19,900
in between experiences maybe but consciousness

249
00:23:19,900 --> 00:23:23,980
how should we see consciousness he says well

250
00:23:23,980 --> 00:23:32,140
suppose there were a bandit a criminal and they brought him before the king

251
00:23:32,140 --> 00:23:36,220
and said this is this man here is a bandit a criminal he says

252
00:23:36,220 --> 00:23:40,860
they say king tell us what his punishment should be

253
00:23:40,860 --> 00:23:47,500
and the king says well go and go and in the morning

254
00:23:47,500 --> 00:23:53,500
strike him with a hundred spears

255
00:23:53,500 --> 00:23:57,020
all morning hundred spears I don't know what it means maybe

256
00:23:57,020 --> 00:24:00,300
poke him with the tip of it bleed a little bit

257
00:24:00,300 --> 00:24:05,980
hundred times and so in the morning they go and they strike him with a

258
00:24:05,980 --> 00:24:10,060
spear a hundred they poke him with a spear a hundred time

259
00:24:10,060 --> 00:24:14,940
and the king asks that noon man how's that man

260
00:24:14,940 --> 00:24:21,420
and they say still alive sire remarkably

261
00:24:21,420 --> 00:24:28,300
and then he says then go at noon now and strike him with a hundred spears

262
00:24:28,300 --> 00:24:31,020
and so they strike him with a hundred spears at noon

263
00:24:31,020 --> 00:24:35,820
piercing with a hundred spears in the evening he asks again how is that

264
00:24:35,820 --> 00:24:37,900
man

265
00:24:37,900 --> 00:24:42,700
still alive sire and he says then go in the evening and strike him with another

266
00:24:42,700 --> 00:24:44,860
hundred spears

267
00:24:44,860 --> 00:24:47,340
and so in the evening they strike him with another hundred spears and the

268
00:24:47,340 --> 00:24:51,500
Buddha says what do you think that man being struck with 300 spears would he

269
00:24:51,500 --> 00:24:53,180
experience pain

270
00:24:53,180 --> 00:24:56,620
and displeasure on that account

271
00:24:56,620 --> 00:25:00,060
and they're like venerable sir even if he were struck with one

272
00:25:00,060 --> 00:25:03,740
spear he would experience pain and displeasure on that account not to speak of

273
00:25:03,740 --> 00:25:06,300
300 spear

274
00:25:06,300 --> 00:25:09,260
and Buddha said in this way the new treatment of consciousness should be

275
00:25:09,260 --> 00:25:13,020
understood should be seen

276
00:25:13,020 --> 00:25:19,740
wretched again all consciousness

277
00:25:19,740 --> 00:25:25,900
all consciousness you see the incessant arising and ceasing of it

278
00:25:25,900 --> 00:25:29,980
you see that consciousness is just like a dart it's an invasion

279
00:25:29,980 --> 00:25:34,860
how hard is that to see right consciousness is an invasion

280
00:25:34,860 --> 00:25:41,340
it's very hard to see the only way you can really appreciate that is if you'd

281
00:25:41,340 --> 00:25:45,180
seen the banana you've seen something better

282
00:25:45,180 --> 00:25:50,060
experience something better

283
00:25:50,060 --> 00:25:55,740
something better than consciousness

284
00:25:55,740 --> 00:26:02,300
let me say unconscious as well suppose i mean it's one way of describing it

285
00:26:02,300 --> 00:26:04,940
it's not like sleep

286
00:26:04,940 --> 00:26:08,620
it's like freedom

287
00:26:08,620 --> 00:26:13,180
i don't know it's a very deep teaching i'm afraid it will be misunderstood or

288
00:26:13,180 --> 00:26:15,740
misappreciate and

289
00:26:15,740 --> 00:26:20,540
by the general audience but i think it's also important that we do

290
00:26:20,540 --> 00:26:23,900
at least come to terms and we don't lie to ourselves but what we're dealing

291
00:26:23,900 --> 00:26:27,020
with if you don't like it and you have to

292
00:26:27,020 --> 00:26:31,020
I mean the best thing about all this is if you don't like you can disprove it

293
00:26:31,020 --> 00:26:36,860
problem is it's true so it may be nice if some sorrow is a wonderful place and if

294
00:26:36,860 --> 00:26:37,980
we could

295
00:26:37,980 --> 00:26:43,580
work this out so we all live together in harmony for the rest of eternity but

296
00:26:43,580 --> 00:26:46,620
you know those religions that teach people about this eternal heaven

297
00:26:46,620 --> 00:26:50,380
where you sing hallelujah forever

298
00:26:50,380 --> 00:26:53,580
it's a nice thought it's a wonderful dream

299
00:26:53,580 --> 00:26:59,660
but it's not reality but the great thing is there's

300
00:26:59,660 --> 00:27:04,460
we can be free we can find peace

301
00:27:04,460 --> 00:27:08,140
i mean whether you like this or not ask yourself if you're happy

302
00:27:08,140 --> 00:27:13,340
i mean the reason people come to me is because they're generally not happy

303
00:27:13,340 --> 00:27:18,540
some are fairly happy it's not all the case but people come because

304
00:27:18,540 --> 00:27:21,900
they see they want to improve themselves i mean the best people

305
00:27:21,900 --> 00:27:24,380
some people come thinking they're just going to have fun and that's

306
00:27:24,380 --> 00:27:29,500
problem because they don't see the the the the challenges and the problems

307
00:27:29,500 --> 00:27:32,780
and the danger of clinging

308
00:27:32,780 --> 00:27:36,380
the best people the ones who really get from this

309
00:27:36,380 --> 00:27:42,380
are the ones who need help and see that they need help

310
00:27:42,380 --> 00:27:47,500
because they're able to find help they're able to help themselves

311
00:27:47,500 --> 00:27:54,140
so there you go a little food for thought

312
00:27:54,140 --> 00:27:58,300
there's the demo for tonight thank you all for tuning in

313
00:27:58,300 --> 00:28:07,340
thank you all for coming

314
00:28:07,340 --> 00:28:19,980
so do we have questions

315
00:28:19,980 --> 00:28:38,460
we can't even log in

316
00:28:45,180 --> 00:28:48,220
okay i'm going to save the questions for another day

317
00:28:48,220 --> 00:28:50,220
thank you all for coming out have a good night

