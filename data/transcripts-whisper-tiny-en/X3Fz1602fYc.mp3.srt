1
00:00:00,000 --> 00:00:06,420
and welcome back to our study of the Dhamapada. Today we're continuing on with verse

2
00:00:06,420 --> 00:00:11,060
252, which reads as follows.

3
00:00:11,060 --> 00:00:27,020
sudha sang wa jaman ye sang at the nang panadhu dha sang, paraisan ye soh wa jani,

4
00:00:27,020 --> 00:00:39,500
opunati yatah bhu sang, atan opanah chah diti, keling wa kita wa satho.

5
00:00:39,500 --> 00:00:57,260
which means easy to see are the faults of others, hard to see our one's own faults.

6
00:00:57,260 --> 00:01:12,140
The faults of others, one windows away, like chaff, but one's own faults. One covers up like a fraud

7
00:01:12,140 --> 00:01:27,220
who covers up their deceit. So this verse was taught in response to a

8
00:01:27,220 --> 00:01:37,020
story, very short story, related by a rich man named Mandika. And most of the

9
00:01:37,020 --> 00:01:43,860
backstory to the verses relating to how Mandika became a rich man. But since it

10
00:01:43,860 --> 00:01:50,900
doesn't really relate to the verse, I'm going to skip it. It's also quite a

11
00:01:50,900 --> 00:01:56,220
fantastical story, I think probably exaggerated, it's to some extent. But the

12
00:01:56,220 --> 00:02:05,220
gist of it is that he was very generous, very kind, very thoughtful, and he

13
00:02:05,220 --> 00:02:10,120
was a great supporter of Buddhism in the time of the Buddha,

14
00:02:10,120 --> 00:02:20,820
vipasi, I think, the time of another Buddha. One time there was a famine in one of his

15
00:02:20,820 --> 00:02:27,580
past lives, and they stored as much rice as they could, but finally they were

16
00:02:27,580 --> 00:02:33,980
down to one pot of rice for him, his wife, his son, his daughter, and his

17
00:02:33,980 --> 00:02:41,260
servant. And so they split it up five ways, but then they saw an enlightened

18
00:02:41,260 --> 00:02:51,660
being, some religious person who exhibited all the qualities of an enlightened

19
00:02:51,660 --> 00:02:59,100
being, and they were so impressed by his countenance that they all decided to

20
00:02:59,100 --> 00:03:09,660
give their meal, their last meal to this religious person, given as an

21
00:03:09,660 --> 00:03:15,900
offering. Really, in a way, as an offering to dedicate to their own well-being,

22
00:03:15,900 --> 00:03:21,340
they used it as a support for their own determination to never have to

23
00:03:21,340 --> 00:03:28,900
starve again. And the story goes that as a result they didn't ever starve again.

24
00:03:31,100 --> 00:03:37,860
So the lesson behind that is, of course, the sort of rudimentary Buddhist

25
00:03:37,860 --> 00:03:42,220
lesson of being generous, and I think we shouldn't ignore that. It doesn't have

26
00:03:42,220 --> 00:03:51,140
much to do with the verse at all, but it is worth mentioning that many of

27
00:03:51,140 --> 00:03:56,900
these stories have some element relating to generosity. They remark upon

28
00:03:56,900 --> 00:04:03,300
the well-being or the fortune of individuals as being a product of

29
00:04:03,300 --> 00:04:08,700
generosity and kindness and goodness in general. And that's an important

30
00:04:08,700 --> 00:04:13,300
Buddhist quality. I mean, it sounds somewhat awkward as a religious person myself

31
00:04:13,300 --> 00:04:17,340
to talk about such things, especially when they relate to generosity towards

32
00:04:17,340 --> 00:04:21,420
religious individuals, but I certainly, as I've said before, subscribe to this

33
00:04:21,420 --> 00:04:26,540
idea just because I'm a monk doesn't mean I don't believe that giving to enlightened

34
00:04:26,540 --> 00:04:30,260
beings, whether they be monastic or not, but even just giving to the

35
00:04:30,260 --> 00:04:33,660
monastic community is a great thing to do, something I do whenever I have the

36
00:04:33,660 --> 00:04:40,260
chance, something we did last weekend with the help of Jeff and with the

37
00:04:40,260 --> 00:04:46,620
help of our organization. For my birthday, I gave food to the monks there,

38
00:04:46,620 --> 00:04:55,500
and that's just a great thing to do. Generosity, people who are generous,

39
00:04:55,500 --> 00:04:59,620
people who give and are kind, they know the power of it. They can appreciate

40
00:04:59,620 --> 00:05:06,580
this importance of this. But the real story behind this verse, it was quite

41
00:05:06,580 --> 00:05:13,540
simple, mendika one day went to see the Buddha. And on his way to see the Buddha,

42
00:05:13,540 --> 00:05:24,660
he was confronted by someone. I can't remember some follower of a different

43
00:05:24,660 --> 00:05:28,780
religion. And he asked, where are you going? And I'm going to see someone to go

44
00:05:28,780 --> 00:05:36,860
to my, and he said, oh, why would you go to see him? You who are a kiriawada? Why

45
00:05:36,860 --> 00:05:44,700
would you go see someone who is a kiriawada? You are a person who is kiriawada.

46
00:05:44,700 --> 00:05:51,700
kiriawada means someone who, well, literally believes in the, believes in action.

47
00:05:51,700 --> 00:05:57,780
And it means the efficacy of action. It means that there is a result to your

48
00:05:57,780 --> 00:06:05,620
actions, or that there is such a thing as an action that is, that is potent. And

49
00:06:05,620 --> 00:06:15,660
Akkiriawada is someone who doesn't believe in the potency of action. And mendika

50
00:06:15,660 --> 00:06:20,340
didn't actually respond as far as we, we read. He just continued on his way to

51
00:06:20,340 --> 00:06:23,460
see the Buddha, but he remarked to the Buddha about this, what this person had

52
00:06:23,460 --> 00:06:29,900
said to him, maybe he wanted to get the Buddha's take on it, or the Buddha's

53
00:06:29,900 --> 00:06:36,700
explanation of whether he was kiriawada or Akkiriawada. But the Buddha turned

54
00:06:36,700 --> 00:06:46,020
around with this verse, focusing more on the, the nature of the conversation

55
00:06:46,020 --> 00:06:51,180
as being confrontational as being somewhat accusative and being focused on

56
00:06:51,180 --> 00:06:58,340
the faults of others. It may very well have been that mendika had a problem

57
00:06:58,340 --> 00:07:05,580
with obsessing over the faults of these individuals, because of course when you

58
00:07:05,580 --> 00:07:10,180
point out someone else's fault, that that's quite an ugly sort of thing to do.

59
00:07:10,180 --> 00:07:13,420
And so it's a fault yourself. Perhaps he was waiting for the Buddha to say

60
00:07:13,420 --> 00:07:18,620
something bad about those people, the Buddha as a result may have taught this

61
00:07:18,620 --> 00:07:24,380
to remind him not to focus on the faults of others. But it's certainly an

62
00:07:24,380 --> 00:07:28,700
indictment of these people who are very much focused on criticizing the Buddha

63
00:07:28,700 --> 00:07:35,500
for being someone who didn't believe in karma, which is very egregious

64
00:07:35,500 --> 00:07:44,860
indictment or claim accusation. So that's the, the first lesson that we can

65
00:07:44,860 --> 00:07:53,260
derive from this story is really about this, this incidental usage of this

66
00:07:53,260 --> 00:07:59,900
dual categorization of being Akkiriawada and kiriawada. So we can ask as a sort of

67
00:07:59,900 --> 00:08:07,820
a preparation to talk about the actual verse, we can ask what is, what is the

68
00:08:07,820 --> 00:08:15,820
Buddha was the Buddha, Akkiriawada is the Buddha kiriawada. So Akkiriawada can be

69
00:08:15,820 --> 00:08:23,420
understood in two ways. And this sort of answers the question as to why these

70
00:08:23,420 --> 00:08:29,260
people would think in the first place that the Buddha was Akkiriawada.

71
00:08:29,260 --> 00:08:32,220
Didn't they think the Buddha didn't they hadn't they ever heard that the Buddha

72
00:08:32,220 --> 00:08:37,660
taught about karma? Well, first of all, the Buddha didn't teach about karma. He

73
00:08:37,660 --> 00:08:43,020
taught about intention or inclination, right? Because in the time of the Buddha

74
00:08:43,020 --> 00:08:52,140
there was an, there was an belief that actual physical actions, ritual actions,

75
00:08:52,140 --> 00:08:59,100
a very specific sort, had potency. If you perform a ritual in the proper way,

76
00:08:59,100 --> 00:09:06,780
well, it wasn't even so much that that results would occur. It was that it was

77
00:09:06,780 --> 00:09:11,580
what God wanted you to do or it's what was right for you to do. It was your duty to do.

78
00:09:11,580 --> 00:09:17,500
If you were a religious person, your duty is to do the rituals because perhaps

79
00:09:17,500 --> 00:09:21,260
because over the course of thousands and thousands years they'd forgotten why,

80
00:09:21,260 --> 00:09:25,260
but they just knew it was their duty. So they did with their parents and grandparents and

81
00:09:25,260 --> 00:09:32,460
ancestors and then if you were of the royal class, it was your duty to fund the rituals,

82
00:09:32,460 --> 00:09:39,500
which is great teaching to perpetuate by the religious people and so on.

83
00:09:42,140 --> 00:09:45,660
And so the Buddha didn't teach against this and this may have been what they meant,

84
00:09:45,660 --> 00:09:51,500
but I assume more likely what they meant is that the Buddha taught against the concept of a soul,

85
00:09:51,500 --> 00:09:58,540
right, because the Buddha taught non-sal. So this can be misunderstood as meaning that the Buddha,

86
00:09:59,900 --> 00:10:04,860
as a result, thought that we are just physical, right, or that we are just automatons,

87
00:10:04,860 --> 00:10:11,980
that there is determinism. So a curioadam might very well be an indictment of the concept of

88
00:10:11,980 --> 00:10:17,820
non-sal, which is a very difficult concept to understand. So it's understandable that these people

89
00:10:17,820 --> 00:10:24,380
made that misunderstanding. So it's quite likely that that's the case, because the curioadam

90
00:10:24,380 --> 00:10:31,660
usually refers to a person who believes that there is a karaka, a self, a soul that acts,

91
00:10:32,300 --> 00:10:37,420
like when we decide to do something, there is a soul that makes that decision.

92
00:10:37,420 --> 00:10:51,500
But of course Buddhism denies not just that idea, but the very framework within which it rests.

93
00:10:51,500 --> 00:10:58,380
So Buddhism, to understand non-sal, Buddhism looks at reality in a different way. We look at

94
00:10:58,380 --> 00:11:03,500
reality from a point of view of experiences. It's not just about taking self out of the equation,

95
00:11:03,500 --> 00:11:11,580
and then whatever's left, you've got just mindless actions. Rather, you have experiences,

96
00:11:11,580 --> 00:11:18,380
and what we would think of as a self is actually just moments of experience and actions,

97
00:11:18,380 --> 00:11:23,980
you know, in mental inclinations that are potent, and there is certainly a potency involved.

98
00:11:23,980 --> 00:11:41,340
So that's the idea of a curioadam, curioadam, curioadam, the Buddha himself did answer the question

99
00:11:41,340 --> 00:11:47,420
of whether he was curioadam, but he answered it in a different way. So in the sense that it's

100
00:11:47,420 --> 00:11:54,700
usually meant the Buddha was curioadam in regards to the idea that there is a potency to our actions,

101
00:11:54,700 --> 00:12:01,500
but he was not curioadam in the sense of believing in an entity. Simply because the Buddha didn't

102
00:12:01,500 --> 00:12:13,180
see the world or the Buddha taught not in the framework of entities, but in the framework of

103
00:12:13,180 --> 00:12:17,420
experiences, which are momentary, and they arise and they cease, and if you start looking at the

104
00:12:17,420 --> 00:12:23,420
world that way, there's no room for the idea of a self. But when the Buddha answered, he said,

105
00:12:23,420 --> 00:12:29,740
yes, there's a way you could say, I'm a curioadam, I don't believe in action. He said, because I teach

106
00:12:29,740 --> 00:12:36,220
people to not do any evil actions. So I teach the non-doing of evil actions, the aqiriya,

107
00:12:36,220 --> 00:12:42,700
the non-action in regards to evil, so a totally different interpretation. He denied being aqiriya

108
00:12:42,700 --> 00:12:51,660
wadam in the ordinary sense. So that's enough of that. In regards to the actual lesson of the

109
00:12:51,660 --> 00:13:00,780
verse, we've heard this lesson or similar lessons to this, the Buddha was often remarking on this

110
00:13:00,780 --> 00:13:08,700
dichotomy of how we relate to the faults of others and how we relate to the faults of our

111
00:13:08,700 --> 00:13:17,580
own faults. And more generally, how we relate to the external to others and how we relate to

112
00:13:17,580 --> 00:13:22,940
ourselves or even more generally, how we relate to the external world and how we relate to the

113
00:13:22,940 --> 00:13:29,420
experiential inner world. I mean, it is an inner world because it's only happening to us. We

114
00:13:29,420 --> 00:13:38,620
own the experience, our own experiences. So I think we can take this on two levels. In regards to the

115
00:13:38,620 --> 00:13:53,260
story, talking about how we approach the issue or the event when someone confronts us or accuses us,

116
00:13:53,260 --> 00:14:01,100
criticizes us, points out our faults. And this verse helps us understand this. It gives us a sort of

117
00:14:01,100 --> 00:14:07,660
a general basic Buddhist lesson in regards to dealing with criticism. It reminds us to

118
00:14:11,900 --> 00:14:19,660
appreciate how prone we are to seek out the faults of others. The imagery the Buddha uses is quite

119
00:14:19,660 --> 00:14:31,820
important as well. If you know anything about winnowing, chaff is the husks of rice. A person

120
00:14:31,820 --> 00:14:39,500
who knows chaff is so obsessed, not with the rice, but with the chaff, that in a big pan of rice,

121
00:14:40,780 --> 00:14:48,380
they will find a way to get the chaff out. When we were picking blueberries, we had these,

122
00:14:48,380 --> 00:14:54,700
we rigged up tweezers because we would pick blueberries and there was the stems and we would

123
00:14:54,700 --> 00:14:59,900
reach in with the tweezers and pull them out. When you're winnowing chaff, we saw them do it in

124
00:14:59,900 --> 00:15:06,940
India, but the idea is you have a big fan and you have a pan of rice and you shake up the rice

125
00:15:06,940 --> 00:15:12,300
and the rice goes up and falls back out of the pan, but when the chaff goes up, it's blown away by

126
00:15:12,300 --> 00:15:19,660
the fan. A person who is skilled at winnowing chaff, they have to get all the chaff out and they

127
00:15:19,660 --> 00:15:26,540
have to be very obsessed with it. When you liken that to a person, winnowing faults,

128
00:15:26,540 --> 00:15:40,860
it's quite apt because it becomes a habit where we overlook all the good qualities of others

129
00:15:41,580 --> 00:15:48,300
and hone in on their faults. This is basically what the Buddha is saying, it becomes a habit.

130
00:15:48,300 --> 00:15:58,060
It's an interesting reminder of this bad habit of humanity that we fall into partiality

131
00:15:58,860 --> 00:16:03,820
based on greed, based on anger, based on delusion, based on fear.

132
00:16:06,380 --> 00:16:12,780
And we spend our time trying to find fault in others. We do it out of greed in order to manipulate

133
00:16:12,780 --> 00:16:17,820
others, sometimes pointing out fault is a means of feeling better about yourself, sometimes it's a

134
00:16:17,820 --> 00:16:24,700
means of ingratiating yourself with others. When you demean others, you can manipulate them to

135
00:16:24,700 --> 00:16:29,420
the point that they rely on you. We do it out of anger because we want to hurt others with our

136
00:16:29,420 --> 00:16:35,740
criticism. We do it out of delusion. For many kinds of delusion, we do it out of wrong view, the

137
00:16:35,740 --> 00:16:41,740
belief that something this person is doing right is wrong, so we criticize them for it. Many people

138
00:16:41,740 --> 00:16:50,860
criticize the Buddha for good things that he did, thinking that they were wrong. We criticize

139
00:16:50,860 --> 00:16:57,420
out of arrogance and conceit. It makes us feel better about ourselves. We're able to pump ourselves up.

140
00:16:57,420 --> 00:17:05,180
We do it out of low self-esteem. We hate ourselves, so we try to bring others down to our level,

141
00:17:05,180 --> 00:17:13,500
try to feel better about ourselves by bringing others down. We do it out of fear, worry, feeling

142
00:17:13,500 --> 00:17:21,020
like we're not adequate, and it assuages our fear by reminding ourselves that others have. Our

143
00:17:21,020 --> 00:17:30,380
minds are perverse in their approach to others. Buddhism is very much about focusing on oneself.

144
00:17:30,380 --> 00:17:42,220
Well, I don't think it's proper to open your faults. You shouldn't be spreading your faults

145
00:17:42,220 --> 00:17:48,620
to others like chaff, showing them the bits of you that are faulty. I don't think it's proper

146
00:17:48,620 --> 00:17:54,380
really good, because if you're constantly telling other people what faults you have, you're

147
00:17:54,380 --> 00:18:01,980
encouraging them to criticize you. Knowing that others are prone to criticism, you really shouldn't

148
00:18:01,980 --> 00:18:10,140
be too concerned or too interested in presenting your faults to others. But we spend far too much

149
00:18:10,140 --> 00:18:17,420
energy trying to hide our faults, and it's really to no avail, because everyone's putting a microscope

150
00:18:17,420 --> 00:18:25,820
on us, anyway. One interesting thing about this verse is, it seems to be contradicting itself,

151
00:18:25,820 --> 00:18:35,980
because the faults of others are not easy, it shouldn't be easy to see, if we're all hiding them.

152
00:18:35,980 --> 00:18:41,660
If everyone's hiding their faults, and we are, it actually is quite difficult to see the faults

153
00:18:41,660 --> 00:18:47,420
of others. We don't know the extent of other people suffering, or they're evil, or they're unwholesomeness.

154
00:18:47,420 --> 00:18:56,140
Someone going to look like a very wholesome, pleasant, good person, kind person, but inside

155
00:18:56,940 --> 00:19:06,540
have great corruption and mental issues. They can be engaged in great evil in private and secret.

156
00:19:06,540 --> 00:19:16,140
But what the verse means, I think, and what it does point out is that we're better able to see

157
00:19:16,140 --> 00:19:23,020
the faults of others for two reasons. And this relates to how we should understand it as

158
00:19:23,020 --> 00:19:29,500
meditators, I think. We're better able to see the faults of others because we're objective

159
00:19:29,500 --> 00:19:36,780
about them. We're not so objective about our faults. We're not so objective about our faults,

160
00:19:36,780 --> 00:19:46,060
because at the time when we are faulty, when we're overwhelmed by greed or anger or delusion,

161
00:19:47,340 --> 00:19:52,540
our minds are not in a clear state. None of these states can arise with clarity of mind,

162
00:19:52,540 --> 00:20:04,780
that's very definition or nature of evil, that it's always associated with darkness, with delusion.

163
00:20:06,860 --> 00:20:11,900
And so because of that, at the time when we are doing something wrong, we really are not in the

164
00:20:11,900 --> 00:20:20,220
position or a state of mind to be able to appreciate the wrongness of it, whereas with others,

165
00:20:20,220 --> 00:20:26,460
because it's not us, because we're not engaged in unwholesome is quite often quite easily.

166
00:20:27,100 --> 00:20:34,780
We can see all the little imperfections of other people's characters.

167
00:20:37,580 --> 00:20:45,100
It is an important lesson to not do that, to change the way we engage with others.

168
00:20:45,100 --> 00:20:50,940
And meditation helps us to see this. I think it's important to understand that enlightened

169
00:20:50,940 --> 00:20:57,180
person doesn't spend much time trying to change others or criticizing others, and that

170
00:20:58,300 --> 00:21:03,900
the vast majority of the times that we are inclined to criticize point out other people's faults

171
00:21:04,860 --> 00:21:11,020
is based on unwholesomeness, based on bad habit, based on even just a desire to change others,

172
00:21:11,020 --> 00:21:20,860
which may seem like a positive thing, but ends up being obsessive and controlling, and usually harmful,

173
00:21:20,860 --> 00:21:28,300
usually our efforts are met with anger and arrogance. Who are you to tell me, it's hard.

174
00:21:29,020 --> 00:21:35,260
It's very hard to rightfully criticize someone. It's very hard to having rightfully criticize

175
00:21:35,260 --> 00:21:43,580
someone, have it actually benefit them. And so one more lesson is, conversely, knowing this,

176
00:21:44,620 --> 00:21:50,860
when people do criticize us, and the Buddha taught this as well, we should try our best to appreciate

177
00:21:52,140 --> 00:21:59,740
that criticism. When others do criticize us, we understand that that's what people are prone to do,

178
00:21:59,740 --> 00:22:05,740
where our whole lives are always going to be criticized. But I said, no one is free from criticism.

179
00:22:06,860 --> 00:22:12,860
And knowing that, we should be prepared for it, expect it, and understand how to deal with criticism.

180
00:22:13,820 --> 00:22:18,780
You know, the many books and non-Buddhist teachers have taught about dealing with criticism.

181
00:22:19,340 --> 00:22:22,860
Your therapist will tell you how to deal with criticism, your parents will tell you.

182
00:22:22,860 --> 00:22:30,060
We talk about having a thick skin, don't be thin skin and so on. But it's important.

183
00:22:30,060 --> 00:22:34,540
It's very important. It's embarrassing as a Buddhist to get angry when someone criticizes you.

184
00:22:35,500 --> 00:22:42,620
As long as we have arrogance and self-worth, our attachment to self-eagle,

185
00:22:43,500 --> 00:22:48,060
we're always going to get upset when others criticize us. So an important Buddhist practice is

186
00:22:48,060 --> 00:22:55,260
to overcome that and to understand the wrongness of reacting negatively to criticism.

187
00:22:55,820 --> 00:23:00,620
Sometimes we want to criticize someone back or so on.

188
00:23:02,540 --> 00:23:08,860
But on a deeper level, in regards to our practice, this is a good example of how

189
00:23:09,660 --> 00:23:15,900
mindfulness benefits both our own mental health and our relationships with others.

190
00:23:15,900 --> 00:23:20,060
Because mindfulness helps you see things just as they are.

191
00:23:21,100 --> 00:23:27,100
It helps you relieve you of this need to change others or change the world around you.

192
00:23:27,820 --> 00:23:31,740
And so as a result, it makes people more comfortable around you. They feel like they can be

193
00:23:31,740 --> 00:23:37,740
themselves. People will admit things to you that they wouldn't do others because they know you're

194
00:23:37,740 --> 00:23:42,220
not judging them. Think of that. Think of that when you think of how good a district

195
00:23:42,220 --> 00:23:49,660
decides and how good you are at pointing out the flaws of others. No one likes that. No one appreciates

196
00:23:49,660 --> 00:23:57,900
that. And most astutely, it's right for someone to say, if you're prone to criticism,

197
00:23:57,900 --> 00:24:03,580
that's a fault of yours. And so you are worthy of criticism when you're prone to criticize other.

198
00:24:03,580 --> 00:24:12,940
But when we're mindful, our experience of others is just our experience of them.

199
00:24:13,900 --> 00:24:17,820
When they say something, it's an experience of sound. When we think about what they've said,

200
00:24:17,820 --> 00:24:23,020
it's an experience of thinking. When we think of who they are, that's all just concepts in our

201
00:24:23,020 --> 00:24:28,140
mind and we see those concepts. So we focus much more on our experience of them.

202
00:24:29,100 --> 00:24:32,620
It doesn't mean we don't know that they're there or we don't understand their situation.

203
00:24:32,620 --> 00:24:39,740
But it does filter and purify that. Purify it because it relates it back to what is actually

204
00:24:39,740 --> 00:24:45,740
happening, not how we interpret it, whether we like it or dislike it, what we want to do to change

205
00:24:45,740 --> 00:24:52,140
it, so on. And so our relationships with others, this is a very good example of how they, again,

206
00:24:52,140 --> 00:25:03,660
they are purified and harmonized. But internally, I think the most important lesson is how we

207
00:25:03,660 --> 00:25:08,220
approach our own faults because we don't just hide them from others, which as I said,

208
00:25:08,220 --> 00:25:12,300
doesn't have to be a negative thing. Now actively hiding them maybe, but

209
00:25:12,300 --> 00:25:22,220
it could even be positive to not show your faults to others because of, again, how it can cause

210
00:25:22,220 --> 00:25:27,660
problems. That's not the real problem. The real problem or the big problem is how we hide them

211
00:25:27,660 --> 00:25:35,900
from ourselves. We hide them from ourselves by rationalizing them. We hide them from ourselves by

212
00:25:35,900 --> 00:25:43,740
understanding them, misunderstanding them. So again, the idea that our faults are not actually

213
00:25:43,740 --> 00:25:56,060
faults. We hide them from ourselves or we ignore them by hating ourselves. We render ourselves

214
00:25:56,060 --> 00:26:01,340
ineffective at dealing with them by saying, yeah, there I go again doing this or doing that.

215
00:26:01,340 --> 00:26:06,780
I'm just a terrible person, which is a useless thing to do. I mean, it's not only hateful. That's

216
00:26:06,780 --> 00:26:12,380
not the biggest problem. The biggest problem is that it makes us ineffectual. It's like an answer.

217
00:26:12,380 --> 00:26:17,500
It's a defense mechanism of sorts because as long as you remind yourself that you're not able to

218
00:26:17,500 --> 00:26:24,300
deal not, you're, you're just such a bad, you're that sort of bad person. If you're that sort of

219
00:26:24,300 --> 00:26:29,100
bad person, well, then yeah, you have no power to change because it's who you are. But it's not

220
00:26:29,100 --> 00:26:37,020
who you are. It's just habit as everything is. So the way meditation changes this and especially

221
00:26:37,020 --> 00:26:44,700
the mantra meditation is on three levels, on the perception level. When you note, for example,

222
00:26:45,660 --> 00:26:50,780
let's look at one of the hindrances. So if you're angry, if you're an angry sort of person,

223
00:26:50,780 --> 00:26:57,260
and you say to yourself, angry, angry, the word focuses you on the experience,

224
00:26:57,260 --> 00:27:02,940
and you see the anger as something that arises and ceases. You see her ceases pretty quickly

225
00:27:03,660 --> 00:27:10,380
because it can't exist with mindfulness. But you see it for what it is. And so that changes your

226
00:27:10,380 --> 00:27:16,780
perception of it. Your perception of your faults changes you from, from maybe getting caught up

227
00:27:16,780 --> 00:27:21,740
in the anger and thinking, yeah, you should do something with this anger. Maybe hurt others or

228
00:27:21,740 --> 00:27:27,500
say things that are unpleasant to others. It changes it on the thought level. You see, the thing

229
00:27:27,500 --> 00:27:36,540
about a noting, a mantra, is it is a thought. It's a mental activity. It's a mental construct. It's

230
00:27:36,540 --> 00:27:44,620
conceptual. It's an act of conceptualizing. And it's meant to be a replacement for the ordinary

231
00:27:44,620 --> 00:27:53,260
conceptualizing that we do. When you're angry, you conceptualize it as, I am angry. And this

232
00:27:53,260 --> 00:28:00,540
person made me angry. And I don't deserve whatever they did to make me angry and so on. Our thought

233
00:28:00,540 --> 00:28:09,660
process, this is the wrongness of our thought process. We are oblivious to the nature of the anger

234
00:28:09,660 --> 00:28:13,660
because we're focused on maybe the other person or whatever it was that caused their anger or whatever

235
00:28:13,660 --> 00:28:21,260
we're going to do about our anger, with our anger. And so when we say to ourselves,

236
00:28:21,260 --> 00:28:29,500
angry, all of that is gone. There is no conceptualizing beyond anger. What is this? How do I

237
00:28:29,500 --> 00:28:36,380
conceive of anger? It's anger, greed. When you want something, what you want, you know, how you're

238
00:28:36,380 --> 00:28:45,100
going to get it gone because there's only the wanting. And finally, on the level of views,

239
00:28:47,020 --> 00:28:53,740
the perversion of views is done away with by as a result of the process of noting,

240
00:28:54,460 --> 00:29:00,460
because as you note and your clarity on the perception level where you perceive things as what they

241
00:29:00,460 --> 00:29:07,420
are, you think of them as they are, your view becomes the observation, the conclusions that you

242
00:29:07,420 --> 00:29:12,860
draw based on your observations as though we're a science experiment because your observations are

243
00:29:12,860 --> 00:29:19,980
so clear. You're able to understand, yes, anger leads to this. This led to the anger. Because I

244
00:29:19,980 --> 00:29:30,300
was unmindful, I allowed this experience to pervert my perception to corrupt my mind and to

245
00:29:30,300 --> 00:29:35,020
delude me into thinking that somehow it was wrong and that I had to fix it and so on. I mean,

246
00:29:35,020 --> 00:29:40,540
it's not even really a thought process. It's just your awareness of what's happening,

247
00:29:40,540 --> 00:29:45,500
seeing the way things go and what are the results and how horrible it is to be an angry person

248
00:29:45,500 --> 00:29:53,180
or a greedy person or a deluded person or so on. So this verse really reminds us of the importance

249
00:29:53,180 --> 00:30:01,020
of meditation. It reminds us of a very important problem that we have of hiding our faults and focusing

250
00:30:01,020 --> 00:30:07,900
too much on the external world, which includes and often is very much centered around our

251
00:30:07,900 --> 00:30:13,100
relationship to others and how we are obsessed about how they're better than us, how they're

252
00:30:13,100 --> 00:30:20,780
worse than us, what's wrong with them, what's right with them and not just people in general,

253
00:30:20,780 --> 00:30:28,860
the world around us as opposed to our experience of the world around us. Again, our focus on

254
00:30:28,860 --> 00:30:34,860
changing and fixing other people is mostly wrong-headed. It can be a very good thing to point out

255
00:30:34,860 --> 00:30:40,940
the faults of others if you do it right, but again, very difficult thing to do. It's important for

256
00:30:40,940 --> 00:30:47,420
friends to do, it's important for parents to do, but it sounds so very, very difficult to do with

257
00:30:47,420 --> 00:30:54,460
any positive outcome both for yourself and the other person. So something not really to be

258
00:30:55,500 --> 00:31:01,580
emphasized. They would have likened it to an accurate acrobat, so we can liken it simply to two

259
00:31:01,580 --> 00:31:07,420
people walking together. If we're walking side by side, you don't say, well, you watch where I'm

260
00:31:07,420 --> 00:31:12,940
walking and I'll watch where you're walking and that way we won't trip over anything would be

261
00:31:12,940 --> 00:31:20,540
absurd to think such a thing. It would be inefficient and most likely ineffectual, whereas if we both

262
00:31:20,540 --> 00:31:24,540
focus on our own feet, look, you focus on your feet, I'll focus on my feet, we'll walk together

263
00:31:24,540 --> 00:31:29,660
in harmony, neither one of us will trip. And if the world would like this, if we all did our duty,

264
00:31:29,660 --> 00:31:37,900
fixed our own problems, clear, cleansed and harmonized, purified our own minds,

265
00:31:39,420 --> 00:31:43,900
the world would be in perfect harmony. We wouldn't have to work on our relationships with others,

266
00:31:44,700 --> 00:31:49,980
our interactions with the world around us, people, places, things, animals, whatever

267
00:31:51,420 --> 00:31:57,820
would be pure, would be free from all of the poisons that we inject into our

268
00:31:57,820 --> 00:32:05,900
experiences. So some thoughts on this verse, a simple verse, a simple teaching, but an important

269
00:32:05,900 --> 00:32:11,740
one, on many levels. So that's the teaching of the Dhamma Padafar tonight. Thank you all for

270
00:32:11,740 --> 00:32:41,580
tuning in. I wish you all the best.

