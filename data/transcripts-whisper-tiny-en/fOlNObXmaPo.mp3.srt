1
00:00:00,000 --> 00:00:09,560
I lost it. What do we do when family members think we have been brainwashed as a result

2
00:00:09,560 --> 00:00:13,760
of the Buddha's teaching? They think we've gone crazy.

3
00:00:13,760 --> 00:00:17,280
You want to start?

4
00:00:17,280 --> 00:00:27,080
Well, I don't know what to say, but my mother was at one point. I gave her a book because

5
00:00:27,080 --> 00:00:38,320
I wanted to introduce her to Buddhism and she found that's brainwashing and I said to her,

6
00:00:38,320 --> 00:00:53,960
yeah, that's right. Afterwards, it's purified and so we started to look at it from a different

7
00:00:53,960 --> 00:01:06,720
angle and she was kind of in that war generation when people were afraid and then the

8
00:01:06,720 --> 00:01:19,000
Russians came and people were sending to how you say that.

9
00:01:19,000 --> 00:01:32,280
There's a sick joke that we have about, I don't know if I should say it, but you know

10
00:01:32,280 --> 00:01:39,800
the word concentration camp. There's a play on words there because there's this one

11
00:01:39,800 --> 00:01:44,680
place and I'm not going to say where it was, but this place where the monk who designed

12
00:01:44,680 --> 00:01:49,400
it is obviously not a meditator and it's the saddest piece of meditation construction

13
00:01:49,400 --> 00:01:54,400
that I've ever seen. It's not really a good joke because the Holocaust was a serious

14
00:01:54,400 --> 00:02:01,480
thing and it's something that people hold very seriously. But it's such a terrible joke.

15
00:02:01,480 --> 00:02:06,440
I shouldn't even tell it, but you kind of get where I'm going.

16
00:02:06,440 --> 00:02:14,880
So I don't know if I can tell it, but it was really funny because the, you know, just a disclaimer

17
00:02:14,880 --> 00:02:19,160
that I don't, I'm not a sort of person who looks down on the Holocaust. One thing I would

18
00:02:19,160 --> 00:02:23,840
say is there have been many Holocausts in history and it's kind of sad that only the

19
00:02:23,840 --> 00:02:29,080
one in Germany is remembered so strongly, but Germany, there are a lot of terrible things

20
00:02:29,080 --> 00:02:37,600
that went on based on this one man and his crazy followers, whoever, who killed a lot

21
00:02:37,600 --> 00:02:47,080
of people in horrific ways. I don't know, I'm not going to go there. Let's just move.

22
00:02:47,080 --> 00:02:53,160
Okay. So yeah, then back to my mom. The question was really important.

23
00:02:53,160 --> 00:03:04,480
Sorry. So there is that really deep sitting fear in people of that generation to be brainwashed

24
00:03:04,480 --> 00:03:14,400
and not to be able to think what they want. And I could convince her that it is sometimes

25
00:03:14,400 --> 00:03:22,480
really good not to think what you want and that it is really beneficial to be brainwashed

26
00:03:22,480 --> 00:03:30,360
and that is something that is not something that is obstructed on upon you from outside,

27
00:03:30,360 --> 00:03:36,600
but is that it is something that you decide that you want to do for yourself and for the

28
00:03:36,600 --> 00:03:46,800
benefit for you and for the others. So it's a whole different story than what they fear

29
00:03:46,800 --> 00:03:58,080
actually. And the result is purification of mind and is liberation. And once she understood

30
00:03:58,080 --> 00:04:08,840
that, she gave up the whole concept and she lost her fear about that her daughter could

31
00:04:08,840 --> 00:04:14,000
be brainwashed. It's an interesting, I would say that all brainwashing is in a sense

32
00:04:14,000 --> 00:04:19,120
a kind of a purification. And that's what makes it so dangerous because when the mind

33
00:04:19,120 --> 00:04:26,600
is pure, pure in this sense, it's not in a Buddhist sense, but when the mind is focused

34
00:04:26,600 --> 00:04:32,360
and bright, you can put all sorts of crazy ideas in there when it's a clean slate, right?

35
00:04:32,360 --> 00:04:40,600
You can write on it. When the mind is all distracted, it's very difficult for an authoritarian

36
00:04:40,600 --> 00:04:48,400
body to impose its rules. That's why you have models and chanting, you know, or ceremonies

37
00:04:48,400 --> 00:04:54,200
and you know, you have these marching. This is why armies will do drills, right? And they

38
00:04:54,200 --> 00:04:59,840
get into this concentration in many different ways. It's how you teach people how to

39
00:04:59,840 --> 00:05:08,080
kill, kill others. How you train murderers, you have to brainwash them. But the brainwashing

40
00:05:08,080 --> 00:05:14,320
itself is actually, I think, in many ways similar to meditation. It's just, so what I wanted

41
00:05:14,320 --> 00:05:19,040
to say in regards to that is that you shouldn't look at it in terms of, you know, what

42
00:05:19,040 --> 00:05:24,160
is brainwashing, or use this word, brainwashing as to say it's bad. You should look

43
00:05:24,160 --> 00:05:32,800
at what is being taught here because there is in a sense of sort of indoctrination only

44
00:05:32,800 --> 00:05:38,400
in the sense, in the very limited sense that you're taking on some doctrine, right?

45
00:05:38,400 --> 00:05:44,800
So rather than say, you're being indoctrinated, we should look at what is the doctrine?

46
00:05:44,800 --> 00:05:49,280
What is it that's being taught? Because there was this criticism in the Buddhist time,

47
00:05:49,280 --> 00:05:57,920
all the people of some area were all becoming monks and everyone started making an uproar.

48
00:05:57,920 --> 00:06:03,040
And this psalmana got to my, he's taking away our children from us. And they went to the

49
00:06:03,040 --> 00:06:06,800
Buddha and told them. The Buddha said, we'll just tell them that they were dating according

50
00:06:06,800 --> 00:06:13,280
to the dhamma, the dharma, the truth, the righteousness, because in India dharma was an important

51
00:06:13,280 --> 00:06:16,480
thing. And so that's what they started doing. They said, well, we are dating people according

52
00:06:16,480 --> 00:06:22,200
to the, we are dating people, we teach people according to the dhamma, the truth. And then

53
00:06:22,200 --> 00:06:26,920
they were like, oh, wow, you know, what can you say about that? Oh, it said, they say that

54
00:06:26,920 --> 00:06:33,480
psalmana got to my, teaches them according to the dhamma. They're like, okay, well, that's,

55
00:06:33,480 --> 00:06:38,120
I mean, that's really the point. It's, it's what is being taught. It's not that something is being

56
00:06:38,120 --> 00:06:45,000
taught in a religious sort of way. I don't know if that's, well, at least my part isn't

57
00:06:45,000 --> 00:06:53,400
perhaps so useful. Where do we lose? But we have talked before about how to deal with family

58
00:06:53,400 --> 00:06:59,840
members. And one of the things I said was just being an example, because I mean, give

59
00:06:59,840 --> 00:07:05,360
you an example, when I went back home and my parents started criticizing me and eventually,

60
00:07:05,360 --> 00:07:10,800
you know, using fairly strong words about my, my state of mind, I would just close my

61
00:07:10,800 --> 00:07:15,680
eyes and go rise, see, falling, but they were yelling at me, right? Which was not a good

62
00:07:15,680 --> 00:07:21,480
thing to do in retrospect, because that, that's, that's how you show someone that you've

63
00:07:21,480 --> 00:07:26,600
been brainwashed, right? I hadn't, I mean, from my point of view, it was like, oh, I don't

64
00:07:26,600 --> 00:07:30,600
want to deal with this. Let's go back to my meditation rising. Probably I should have done

65
00:07:30,600 --> 00:07:39,000
hearing hearing hearing. I didn't really have a good sense of what to do. But, you know,

66
00:07:39,000 --> 00:07:46,720
you have to be, you have to be able to show them the, the benefits of your practice.

67
00:07:46,720 --> 00:07:55,720
And I think eventually, eventually that's what would, would wins them over. That's sort

68
00:07:55,720 --> 00:08:01,480
of how my parents have come at this point. They can't, at least can't deny the goodness

69
00:08:01,480 --> 00:08:07,560
of it that I'm doing something good for myself and for other people that I've actually

70
00:08:07,560 --> 00:08:11,840
developed myself and gained something positive out of it.

71
00:08:11,840 --> 00:08:20,520
Until that comes, may you have to understand a, a beginner meditator is often, I was

72
00:08:20,520 --> 00:08:26,120
reading this, this article recently, being meditators can be the most of egotistical people

73
00:08:26,120 --> 00:08:33,280
that exists. The most can, can, arrogant, most conceded individuals in the world because

74
00:08:33,280 --> 00:08:39,320
like any other religious person, they've, they've got the truth, right? And, and the

75
00:08:39,320 --> 00:08:43,000
Buddhist teaching is a very powerful truth. And, when you see it, you say, wow, this

76
00:08:43,000 --> 00:08:50,000
is the truth. You, you become self righteous. It's very easy for a beginner meditator to become

77
00:08:50,000 --> 00:08:56,040
self righteous. That, that is a huge turn off, obviously, for all the people around you

78
00:08:56,040 --> 00:09:01,400
because you maybe weren't that sort of person before. You can actually become a worse person

79
00:09:01,400 --> 00:09:05,560
from their point of view. Because, the point being because you're still struggling with

80
00:09:05,560 --> 00:09:11,680
it yourself. Until you get to the point where it's comfortable for you and you're comfortable

81
00:09:11,680 --> 00:09:18,920
in it and able to express yourself and, and, and really actually experience the truth

82
00:09:18,920 --> 00:09:23,840
instead of just reading and intellectualizing about it. Once you gain this experience

83
00:09:23,840 --> 00:09:32,520
for yourself, you, you become, you know, a living, living proof that it's not brainwashing

84
00:09:32,520 --> 00:09:37,280
in the sense that they think it is in terms of being, brainwashing doesn't just

85
00:09:37,280 --> 00:09:40,960
be washing your mind. That's the, what I said to everyone. Yeah, brainwashing, it's

86
00:09:40,960 --> 00:09:50,240
washing my brain. But the word brainwashing is used to mean taking on a doctrine without

87
00:09:50,240 --> 00:09:56,560
any purpose, without any reason being indoctrinated by something as I understand it, although

88
00:09:56,560 --> 00:10:08,920
it's not a word that I use often. Just maybe a short, when, when the meditation is advancing

89
00:10:08,920 --> 00:10:19,160
and going good and you develop the skills of, of a meditator, then your, your family won't

90
00:10:19,160 --> 00:10:26,080
think anymore that you're going crazy, I would say. They would, they would like what

91
00:10:26,080 --> 00:10:29,760
you do, even if they don't understand what it is.

92
00:10:29,760 --> 00:10:34,400
For sure, people will often say that they think they have to admit that you're a better

93
00:10:34,400 --> 00:10:40,520
person. And so the problem is in the beginning, you're taking the first steps on a very

94
00:10:40,520 --> 00:10:44,840
long journey. And that can be very, the other thing is that can be really disconcerting

95
00:10:44,840 --> 00:10:51,760
to you. So it can upset. I mean, for me, it totally changed my life around before meditating.

96
00:10:51,760 --> 00:10:57,400
I've said this many times. I was into a lot of things Nick can talk about this as well.

97
00:10:57,400 --> 00:11:06,640
We had some, some shared experiences of how we were before. But so, so imagine having

98
00:11:06,640 --> 00:11:12,560
to do a total U-turn in your life. Basically, I gave up my CD collection. I gave, I

99
00:11:12,560 --> 00:11:21,760
sold my Les Paul guitar. I gave up everything, rock climbing, piano, guitar, everything that

100
00:11:21,760 --> 00:11:31,600
I, that I was into. I gave up drugs, gave up alcohol, gave up women, gave up everything

101
00:11:31,600 --> 00:11:39,640
that really made me who I was, honestly, most of it anyway. I had all of these external

102
00:11:39,640 --> 00:11:44,880
things that, that everyone knew this as who I was. So that's disconcerting to yourself.

103
00:11:44,880 --> 00:11:49,640
And without, because without saying, it's disconcerting to other people, but most important

104
00:11:49,640 --> 00:11:59,920
that you, you are in a state of tension. And until you get comfortable in it and part of

105
00:11:59,920 --> 00:12:04,640
getting comfortable in it is actually changing your life because the truth about the, if

106
00:12:04,640 --> 00:12:10,240
you become, suppose you became an Aran, you would have no choice but to ordain immediately

107
00:12:10,240 --> 00:12:17,960
or pass away. You, you are so out of whack with, with an ordinary life or a worldly life

108
00:12:17,960 --> 00:12:25,000
that, that you, you can't even survive there. So for all of us, it's, it's, it's that

109
00:12:25,000 --> 00:12:29,800
sort of experience on a much more limited scale. When you begin to practice, your life

110
00:12:29,800 --> 00:12:35,440
has to change. You might no longer be living with this, the people that you lived with

111
00:12:35,440 --> 00:12:40,920
before. Your, your friends might all disappear. Your, your family members, you might even

112
00:12:40,920 --> 00:12:46,080
become distant from them because you're becoming a part of the universe. You're becoming

113
00:12:46,080 --> 00:12:51,600
a child of the universe. That may seem kind of cheesy to say so, but you're coming to realize

114
00:12:51,600 --> 00:12:56,960
that the truth in a much more universal way than this than just, I am this human who was

115
00:12:56,960 --> 00:13:02,440
born 32 years ago and have these people as my friends and these people as my family

116
00:13:02,440 --> 00:13:08,960
and have this name and so on. You, you rise so far above that that your family is really

117
00:13:08,960 --> 00:13:21,640
all beings, all beings become your family. You, you, you open up to the whole spectrum

118
00:13:21,640 --> 00:13:26,040
of experience. And so some people find that they're, they're whisked away to the other

119
00:13:26,040 --> 00:13:30,800
side of the world and suddenly their family is people they've never met before. And the

120
00:13:30,800 --> 00:13:35,400
people they come in contact with, it's just bizarre. When I was in Thailand, I would come,

121
00:13:35,400 --> 00:13:39,480
I've suddenly, I was in contact with people from all over the world. I met people, I've

122
00:13:39,480 --> 00:13:45,880
met people from countries that I never even knew existed. I met a man, I taught a man from

123
00:13:45,880 --> 00:13:55,560
Nigeria. I taught a man from Surinam, you know, people from all the countries in Europe

124
00:13:55,560 --> 00:14:00,320
and I didn't know much about Europe at all and then learning cultures and languages and

125
00:14:00,320 --> 00:14:09,440
everything. So this change has to go on in the very beginning of your meditation. It's

126
00:14:09,440 --> 00:14:14,120
something that will be a part of your minute. It might not be so extreme, especially if

127
00:14:14,120 --> 00:14:21,600
you're practicing on a day-to-day basis, but there is going to be a conflict there and living

128
00:14:21,600 --> 00:14:25,840
in the world, I think it goes without saying that you have to live with some conflict,

129
00:14:25,840 --> 00:14:31,120
that your life in the secular, in the worldly sense and your life in the dhamma are not

130
00:14:31,120 --> 00:14:36,760
going to always jive. And that relates, I think, to your relationship with your family,

131
00:14:36,760 --> 00:14:41,920
which in the beginning may be quite strained until they become come. I mean, the other thing

132
00:14:41,920 --> 00:14:47,040
is whether you're getting something good out of it or not, you're not who they expect

133
00:14:47,040 --> 00:14:52,560
you to be. And this is the Buddha's teaching on suffering. Suffering comes from change. We cling

134
00:14:52,560 --> 00:14:58,720
to a person, they are like this. This is my son, my daughter, my husband, my wife. And then

135
00:14:58,720 --> 00:15:09,200
when they change, it's like the foundation was pulled out from underneath us. It's like this

136
00:15:09,200 --> 00:15:13,680
with anything. You cling to something and then you think it's stable. You think this person is

137
00:15:13,680 --> 00:15:17,680
going to always be there. And then when they pass away, when they leave, when they change,

138
00:15:17,680 --> 00:15:22,080
when they act in a way that you didn't expect, that wasn't according to what you thought

139
00:15:22,080 --> 00:15:29,200
they were, then you suffer. So for me, it was part of it was just that, that it was things that

140
00:15:29,200 --> 00:15:34,400
I couldn't satisfy. I couldn't go back to playing. I couldn't, but I decided not to go back to

141
00:15:34,400 --> 00:15:43,120
playing guitar. I decided not to go back to joking and doing theater. I was in theater as well.

142
00:15:43,120 --> 00:15:48,160
And these were all things that my family loved about me. You were so much fun before. Now,

143
00:15:48,160 --> 00:15:54,320
you're a zombie. And the things like this, that I just couldn't be there for them in the way

144
00:15:54,320 --> 00:15:58,960
that I was before. I couldn't be the person that they thought I was, that I used to be.

145
00:15:59,600 --> 00:16:07,680
It wasn't my life path anymore. And so that takes time for them to realize that's not the most

146
00:16:07,680 --> 00:16:14,320
important part thing in the world. And it's not really a measure of a person's goodness or sanity

147
00:16:14,320 --> 00:16:25,200
or so on. And you have to understand also that they are not there yet. They don't have these

148
00:16:27,120 --> 00:16:32,880
realizations that you've theoretically gained in the meditation. And so they can't appreciate it.

149
00:16:32,880 --> 00:16:38,560
They can't see the world in the universal sense for them. It's still, you're my son. I'm your father,

150
00:16:38,560 --> 00:16:45,600
I'm your mother. But through the meditation, that changes for you. And you come to see,

151
00:16:46,240 --> 00:16:51,280
you come to feel that way about everyone, really, that you have love for everyone. You wish

152
00:16:51,280 --> 00:17:05,920
all beings to be good, to be well, but have no clinging to any or partiality towards anyone.

153
00:17:05,920 --> 00:17:21,680
I think the biggest thing is just that you're changing, because you have to think

154
00:17:23,040 --> 00:17:32,160
for everybody, it's all in how you relate to other people, like you're saying it's

155
00:17:32,160 --> 00:17:37,280
oh, you're a fun person, you play guitar, we could do this. And all these things, it's like

156
00:17:38,640 --> 00:17:44,880
there's things about you that are being taken away from them. So it's no longer, oh, I could

157
00:17:45,680 --> 00:17:50,560
call him up and we could talk about this, because you won't be interested in that. And all these

158
00:17:50,560 --> 00:17:57,600
different things, so it's like your, your change is taking something away from them that

159
00:17:57,600 --> 00:18:05,200
they weren't expecting. But that you were open to, but like you're saying that they aren't

160
00:18:05,200 --> 00:18:10,240
open to you. That's the problem. And I think that you just have to kind of be okay with that

161
00:18:11,440 --> 00:18:15,280
and just go with it.

162
00:18:16,240 --> 00:18:21,520
Man, another part of that is you have to understand where we're coming from. Are you with these people

163
00:18:21,520 --> 00:18:34,080
because you share a spiritual, you're not with these people, because you share a spiritual goal.

164
00:18:35,120 --> 00:18:39,040
The reason why your friends with all of these people is because you're all into drugs,

165
00:18:39,040 --> 00:18:47,200
or you're all into music, or you're all into whatever. The reason with your families is even more

166
00:18:47,200 --> 00:18:52,720
arbitrary. According to Buddhism, it's karmic and there's a relation there, but it certainly doesn't

167
00:18:52,720 --> 00:19:00,800
have anything to do with what you're now into. So if you've gotten into, it's like if you get into

168
00:19:00,800 --> 00:19:07,200
sports, you start to hang out with sports people. And if you get into academics, you start hanging

169
00:19:07,200 --> 00:19:14,480
out with the academic people. And if you suddenly change, then your friends obviously change.

170
00:19:14,480 --> 00:19:24,880
This is what I think is an important realization, and it's an important decision that one has to

171
00:19:24,880 --> 00:19:35,520
come to, that the reasons for us being around and being close to these certain people

172
00:19:36,560 --> 00:19:44,240
are perhaps not the best reasons for being close to people. So yeah, you may lose all of your

173
00:19:44,240 --> 00:19:48,560
friends, but that shouldn't be surprising, because your friends, you weren't friends with them

174
00:19:48,560 --> 00:19:53,600
because they were spiritual people, you're friends with them because you let each other down

175
00:19:53,600 --> 00:20:00,240
the path to destruction, the path to self-destruction. For example, or you weren't family members

176
00:20:00,240 --> 00:20:04,480
with these people because you shared a spiritual path, and maybe that some of your family members

177
00:20:04,480 --> 00:20:10,880
may never be interested in what you do. You can't bring them with you because your relationship,

178
00:20:10,880 --> 00:20:17,680
your similarity may have nothing to do with spirituality. The reason why you were born into a

179
00:20:17,680 --> 00:20:23,280
certain family may have nothing to do with your interest in spirituality and maybe totally unrelated.

180
00:20:23,920 --> 00:20:29,040
So you have to be open to that and decide what's important for you. I think for most people,

181
00:20:29,040 --> 00:20:36,720
family is very important, and that attachment is near impossible to do away with. But

182
00:20:36,720 --> 00:20:47,840
when you practice intensively and in a protracted manner, if you practice over the long term,

183
00:20:48,800 --> 00:20:55,360
and eventually you come to this realization that your true family are the people who practice

184
00:20:56,320 --> 00:21:01,200
the Buddhist teaching, who practice to understand reality for what it is.

185
00:21:01,200 --> 00:21:07,760
You have to practice, develop goodness inside of themselves, who practice to help the world

186
00:21:07,760 --> 00:21:16,160
and to make the world a better place. Those people who do good are your closest relatives,

187
00:21:16,160 --> 00:21:22,240
and the ones that you consider to be yourself to be close to. You associate yourself with.

188
00:21:22,240 --> 00:21:29,040
I'll finish that one.

