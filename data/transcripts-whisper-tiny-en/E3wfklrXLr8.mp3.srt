1
00:00:00,000 --> 00:00:07,520
When you think of the Buddha's appearance, do you think of him as in the iconic iconography

2
00:00:07,520 --> 00:00:10,720
or in another way?

3
00:00:10,720 --> 00:00:15,600
And what's your view on the 32 signs of a great man?

4
00:00:15,600 --> 00:00:25,200
No, I don't think of him really like most of the iconography.

5
00:00:25,200 --> 00:00:43,520
I don't think of him as made of brass or stone or with all the weird colors that they put him in.

6
00:00:43,520 --> 00:00:48,560
And I think of him in a robe, usually I think of him with me bowing down in his feet.

7
00:00:48,560 --> 00:00:50,560
I get the truth.

8
00:00:50,560 --> 00:00:56,080
When I think of the Buddha or the Sangha, either way, I think of myself.

9
00:00:56,080 --> 00:01:06,480
I see their feet first, really, and I see them in robes and it's just a vision that comes.

10
00:01:06,480 --> 00:01:17,680
The 32 signs of a great man are a source of a lot of difficulty, I think, for westerners.

11
00:01:17,680 --> 00:01:23,600
I was asking about them, I mean, just let's be open-minded, I was arguing, playing devil's advocate,

12
00:01:23,600 --> 00:01:30,640
and I said, well, how is it possible for someone's mouth as if someone's tongue took over their forehead?

13
00:01:30,640 --> 00:01:40,720
I mean, really, getting your tongue to your ears is, well, or getting your tongue to your ears, how is that possible?

14
00:01:40,720 --> 00:01:50,640
So we argued it, you know, my feeling was just really just meant that he had a long tongue.

15
00:01:50,640 --> 00:01:59,840
But no, people take it literally, that his tongue could cover his whole forehead and touch both ears.

16
00:01:59,840 --> 00:02:10,640
But the argument that they use is that there are very strange sorts of beings that there are people with very strange body parts.

17
00:02:10,640 --> 00:02:18,160
For example, one interesting thing I found was I was recently alerted to the fact that

18
00:02:18,160 --> 00:02:26,320
Mogulana is said to have had a blue skin, so that he's shown with blue skin in Sri Lanka,

19
00:02:26,320 --> 00:02:28,880
whenever he has the picture of Mogulana, he's blue.

20
00:02:28,880 --> 00:02:32,400
So I asked, why is that Mogulana, that it's Mogulana?

21
00:02:32,400 --> 00:02:37,760
And they explained it's because he just came from hell, and the fear of hell was so sharp

22
00:02:37,760 --> 00:02:41,520
in him that he was born with blue skin.

23
00:02:41,520 --> 00:02:47,440
And so that's hard to believe, but then I read in this book that someone in England a few hundred years ago

24
00:02:47,440 --> 00:02:50,560
was actually born with blue skin and had blue skin through their life.

25
00:02:50,560 --> 00:02:59,840
So that's an example of how you really can't say for sure.

26
00:02:59,840 --> 00:03:08,560
What I would say is that with anything like this, it's really just a matter of fact,

27
00:03:08,560 --> 00:03:15,600
was it true that the Buddha had a tongue that could touch his ears and many other,

28
00:03:15,600 --> 00:03:21,120
mostly, that's really the biggest one that poses the biggest problem for me.

29
00:03:21,120 --> 00:03:27,920
I think there was another one, but the tongue one is probably the biggest.

30
00:03:27,920 --> 00:03:30,240
So did he have it such a tongue or did he not?

31
00:03:30,240 --> 00:03:34,720
And once you pose that question, you can of course ask yourself, does it matter?

32
00:03:37,360 --> 00:03:40,880
And of course it doesn't. There's so many questions like that.

33
00:03:40,880 --> 00:03:43,920
Was it true that the Buddha did this? Was it true that the Buddha did that?

34
00:03:43,920 --> 00:03:46,320
Is it factually correct?

35
00:03:46,320 --> 00:03:52,240
You know, all these stories actually true that we hear about Devos coming down and this happening

36
00:03:52,240 --> 00:03:58,720
and that happening. Did the Buddha really perform this miracle or that miracle?

37
00:03:58,720 --> 00:04:02,320
It's just a question of historical fact. Did it happen or did it not?

38
00:04:02,320 --> 00:04:09,200
And then does it matter whether it did or not? Because in Buddhism, it doesn't matter whether

39
00:04:09,200 --> 00:04:16,880
the Buddha performed any miracles or not. It doesn't matter whether he was a dwarf or a hunch

40
00:04:16,880 --> 00:04:23,840
baggage. I'm sorry, I don't mean to be disrespectful. I'm not trying to disrespect, but it has no

41
00:04:23,840 --> 00:04:28,880
impact on the Dhamma. It has some impact on our faith. And if the Buddha had been

42
00:04:30,400 --> 00:04:36,560
perished the thought, a dwarf or a hunchback, we would think, well, how could such a perfect person

43
00:04:36,560 --> 00:04:42,720
be born in such a state? Based on the teachings of the Buddha, you'd have to think of him as being

44
00:04:42,720 --> 00:04:54,240
born in a much, much more perfect form. But it doesn't have an impact on our practice.

45
00:04:57,120 --> 00:05:08,480
For me, there are certain suitors that I read. I don't look at the whole canon as the

46
00:05:08,480 --> 00:05:17,440
word of the Buddha. Certain suitors just stand out as truth to me, to my inner core, and some

47
00:05:18,000 --> 00:05:24,400
do not. And so, you know, I will look at them and just put them on a back burner and don't

48
00:05:24,400 --> 00:05:31,920
get too caught up to it. I realize how old these texts are. And there's that one. And a few others

49
00:05:31,920 --> 00:05:40,320
that just, when I read them, jump out at me, like it's possibly a later edition. I don't know,

50
00:05:41,760 --> 00:05:45,600
but I don't let a bother me because the ones that ring true

51
00:05:46,960 --> 00:05:57,360
ring deeply true to me. And the others I just

52
00:05:57,360 --> 00:06:04,560
read and I appreciate if I did there there. But I'll realize that I don't think that this whole

53
00:06:04,560 --> 00:06:08,320
canon is without error.

54
00:06:08,320 --> 00:06:17,760
Yeah, but the only thing that I would say in defense of it is that we are not perfect either.

55
00:06:17,760 --> 00:06:23,680
I mean, this example, I'm not going to spend too much time defending it. But

56
00:06:23,680 --> 00:06:30,160
I have had examples of that sort of thing. And you have to realize that everyone,

57
00:06:30,720 --> 00:06:39,040
we come to the Dhamma with prejudices. That's why it's so important for us. And those prejudices

58
00:06:39,040 --> 00:06:46,560
are going to color what we find valid and what we find invalid. So I've had arguments with

59
00:06:46,560 --> 00:06:52,080
monks about this suit to being invalid or that. And once you point something out to them,

60
00:06:52,080 --> 00:07:02,400
they have to concede the fact that their whole reason for doubting it was in

61
00:07:02,400 --> 00:07:06,880
trouble. So for example, just give us impulse. So the example, the fact that

62
00:07:06,880 --> 00:07:14,560
Mogulana had blue skinned. And as I said, it isn't without presidents even in recorded history.

63
00:07:14,560 --> 00:07:24,080
So who knows? We aren't we aren't perfect that we know everything that's possible. We don't

64
00:07:24,080 --> 00:07:28,640
know whether it could have been the fact that the Buddha did have such a big tongue. And of

65
00:07:28,640 --> 00:07:33,040
course, we don't know that it was maybe just an exaggeration of of course it might not be.

66
00:07:33,840 --> 00:07:40,080
The point I think being that it's not really a core Buddhist teaching. I mean, it's not going to

67
00:07:40,080 --> 00:07:45,520
shake the foundations of Buddhism if we find out that the Buddha didn't really have

68
00:07:46,160 --> 00:07:55,600
such a tongue. But definitely in this case, I'd have to say that it's not one of the

69
00:07:55,600 --> 00:08:03,440
suitors that I reread as inspiration for my own practice. Although there are some aspects of

70
00:08:03,440 --> 00:08:08,080
the, I believe it's the Mahaprasalakana Sutta. No, or the, I don't know what it's called,

71
00:08:08,080 --> 00:08:14,640
the one in the digany kaya with the takanas. It has some really interesting things like how the

72
00:08:14,640 --> 00:08:21,600
Buddha would chew every rice grain. No rice grain was, was swallowed without having been chewed.

73
00:08:22,640 --> 00:08:29,360
Now that is inspiring. The way he sat, the way he walked, the way he talked that those parts of

74
00:08:29,360 --> 00:08:37,120
his mannerism and then how he says, and that's, that's a small fragment of the virtues of the Buddha.

75
00:08:37,120 --> 00:08:43,600
And if you read that, it's just an incredibly inspiring passage. Of course, you kind of spoils it

76
00:08:43,600 --> 00:08:51,280
when you, you're still thinking, but what about the tongue? But no, I think that there, there's

77
00:08:51,280 --> 00:08:56,880
something really valuable in that the one particular Sutta that I'm thinking.

78
00:08:59,600 --> 00:09:05,920
But I want to question, sorry, it doesn't mean that they don't exist yet.

79
00:09:05,920 --> 00:09:12,320
No. No. Like magical powers. If you don't have them, it doesn't mean that they don't

80
00:09:12,320 --> 00:09:18,320
exist. Exactly. Many people are, are totally, they will throw at all the magical powers, which

81
00:09:18,320 --> 00:09:25,040
is ridiculous. There are many people out there who have had experiences that are magical,

82
00:09:25,040 --> 00:09:32,640
that can only be described as out of body experiences, whatever. And I'm, I'm convinced that that

83
00:09:32,640 --> 00:09:38,480
is the, that is reality. You know, anyone who practices meditation sees that things aren't

84
00:09:38,480 --> 00:09:44,800
exactly as they seem here. One day, maybe one day, physics will catch up and start to realize,

85
00:09:44,800 --> 00:09:50,720
look at quantum physics, how it's totally shattered our concept of what reality is. We can't

86
00:09:50,720 --> 00:09:57,520
really describe reality anymore. So no one's in a position to say this candy. So another point

87
00:09:57,520 --> 00:10:04,080
that you might make is, from our understanding of magical powers, if someone wants their tongue

88
00:10:04,080 --> 00:10:11,360
to touch their, you know, touch their toes, it's possible to develop a power, you know, in some

89
00:10:11,360 --> 00:10:17,360
radical way to do that. There are, there are magic, there are magical things that can be done

90
00:10:17,920 --> 00:10:24,640
with extreme concentration and focus. Of course, and this is totally radical, radical talk,

91
00:10:24,640 --> 00:10:30,560
and most people won't accept it, which is why in short, the best answers put them on the back

92
00:10:30,560 --> 00:10:36,640
earner as Alfred says, don't pay too much attention. You know, it shouldn't be a sticking point

93
00:10:36,640 --> 00:10:41,840
where you say, you know, I can't believe in Buddhism because I can't believe that the Buddha had

94
00:10:41,840 --> 00:10:49,520
such a tongue. No, that would be a shame. I think the best thing to do would be to say,

95
00:10:49,520 --> 00:10:54,960
oh, that's interesting. Buddha had a tongue like that. Wow. Because with pairs, I mean,

96
00:10:55,840 --> 00:11:02,400
what is it about us that we have to deny such things? No, I'm not rest until I find the truth of

97
00:11:02,400 --> 00:11:08,000
this one. I won't rest until I can prove that the Buddha didn't have such a tongue, right?

98
00:11:09,600 --> 00:11:15,920
So why not just say, okay, Buddha had such a tongue. That's cool. Can I get back to meditation now?

99
00:11:15,920 --> 00:11:20,720
I mean, why not? Why not? There was a teacher in Thailand who said,

100
00:11:21,440 --> 00:11:28,320
Buddhism puts the magic back in life, which, you know, I don't, I'm just quoting off the top of my

101
00:11:28,320 --> 00:11:35,520
head. I don't particularly hold to such a quote, but what she was trying to say is, is this exactly

102
00:11:35,520 --> 00:11:40,960
exact thought that I wouldn't say we should try to put the magic back into life particularly,

103
00:11:40,960 --> 00:11:48,800
but to some extent, we have to let go of this, this obsession with skepticism and the desire

104
00:11:48,800 --> 00:11:55,120
to disprove and doubt things, basically the desire to doubt things. So it's actually kind of a

105
00:11:55,120 --> 00:11:59,680
useful teaching in that sense. It catches you and you start doubting and then the teacher can say,

106
00:11:59,680 --> 00:12:07,120
look at the doubt. It's useful. Get people thinking. It's like a Zen Kohan. The Buddha had a

107
00:12:07,120 --> 00:12:13,680
tongue that could touch his ears. Think about that until you let go of your doubt. Maybe it could

108
00:12:13,680 --> 00:12:23,600
become enlightened on the deep. Anyway, great. I'm glad we've got some conversation going on with

109
00:12:23,600 --> 00:12:39,520
this. We're not going to get through many questions this way though, which is okay.

