Here's a good one. Thank you for your erratic question.
Question is, it's not better to suffer in future lives.
It's not better to suffer in future lives instead of reaching enlightenment and not live anymore.
I hear you. It's quite clear and I think this is on a lot of people's minds.
There was a queen once in the Buddha's time and she became an Arahant.
And the Buddha said to the king, well either you,
either you let her become a bikunin or she's going to pass away within seven days.
Because there's no way she could survive. There's no way she could continue.
It's not really possible.
So she would pass away and he was like,
oh no, then an ordainer, ordainer right away.
Because he couldn't bear to have her disappear.
He's not bear to have her gone.
He said, enough of this talk of very nibana.
The question, sorry, I'm sorry to find it funny, but it really answers itself.
What you're saying is you want to suffer in future lives.
So there's nothing really to worry about because you have no chance.
With that mind-state remaining, you have no chance of not living anymore.
You will have to come back because there is still this cause for future rebirth.
At the moment of death, you don't say, okay enough.
You say more or more or more. What about this? What about that?
So don't you don't have to wrestle with this one.
You don't have to think, mmm, should I practice Buddhism?
Because if I practice Buddhism maybe I won't live anymore.
It's not possible.
The only way that you could not come back and live again to suffer more and more
is if you decided for yourself through what I would understand
to be an incredible realization of the truth, but it depends who you ask.
That there were no reason to come back.
You will come to realize that there was no benefit.
And you come to realize that in fact there is no one living at all.
There is only actually these experiences.
None of which are of any benefit to you.
Or of any benefit in general.
None of which have no intrinsic value.
And when you realize that you're not born anymore,
it just happens.
So for yeah, for as long as you,
you still think that there's some benefit to existence.
Don't worry about it.
You got lots of time to come back again and again.
Which a lot of people like about Buddhism.
They think, well great, I'll just come back again and again and again.
Eventually I'll learn everything and become enlightened.
All in good time, but first let me explore this kind of this novel idea
that I can actually do good deeds and go to heaven and so on.
Which is fine.
It's kind of unshaky ground because you never know where you're going to go.
And maybe this life, next life you go to heaven,
but once you forget all of the stuff that you've learned in regards to goodness and badness,
you might come back as one of those evil people and go to hell as well.
So yeah, but really that's the point is that
to answer your question directly, it's because there's nothing good about any piece of existence.
It's actually a delusion that we have.
It's not understanding reality as it is.
But for as long as you understand reality to have some intrinsic benefit or realities,
as long as you understand experience,
seeing, hearing, smelling, tasting, feeling, thinking,
to have some intrinsic value, you come back and there's no need to worry about that.
The other thing I'd say is that it's kind of like,
it kind of like works like pulling the thread of a sweater.
Because you've got this loose thread and you say, well, that's no good.
I've got to get rid of that.
But as you pull it, you realize that it's attached to so much more.
And then you keep pulling and pulling and pulling and eventually you're left without a sweater.
Suffering is really in that vein.
We come to practice not because we want to become enlightened,
but because we've got a lot of problems.
So we figure, get rid of those problems and life will be fine.
Then I won't need to come embedded.
Then I won't have any interest embedded.
That's enough.
Let me get that far and then quit.
Problem is, as you explore these questions or these problems,
deeper and deeper into you get to the root of the problem.
You find that the root is actually the root of who you are.
It's the root of your identification with existence.
And once that's gone, then you have no interest.
Either way, there's no interest in coming back as this or that.
There's no aversion to existing.
And so there's just peace.
And then at the end there's freedom.
So I hope that answers your question.
