1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Damapanda.

2
00:00:05,000 --> 00:00:12,000
Today we continue on with verse number 84, which reads as follows.

3
00:00:36,000 --> 00:00:41,000
Nata he do, napara sahid, not for,

4
00:00:41,000 --> 00:00:48,000
not with oneself as the cause, not with another as the cause.

5
00:00:48,000 --> 00:00:52,000
Not because of oneself, not for the cause of oneself,

6
00:00:52,000 --> 00:00:57,000
not for, not for one's own cause,

7
00:00:57,000 --> 00:00:59,000
for one's own benefit is the meaning,

8
00:00:59,000 --> 00:01:05,000
or for the benefit of another.

9
00:01:05,000 --> 00:01:09,000
Nata put the michae, not wanting,

10
00:01:09,000 --> 00:01:13,000
not for desires in regards to children,

11
00:01:13,000 --> 00:01:17,000
nada nang, or wealth,

12
00:01:17,000 --> 00:01:21,000
nata rakthang, or even a kingdom.

13
00:01:21,000 --> 00:01:24,000
Nata ichae, nada minas amidimatana,

14
00:01:24,000 --> 00:01:29,000
one should not wish for success,

15
00:01:29,000 --> 00:01:34,000
for worldly, for one's own prosperity,

16
00:01:34,000 --> 00:01:41,000
by means of adhamma, by unrighteous means.

17
00:01:41,000 --> 00:01:46,000
Such a one should be ciloa, have morality,

18
00:01:46,000 --> 00:01:50,000
or ethics, panewa should be wives.

19
00:01:50,000 --> 00:01:57,000
Damiko sia, thus they should be righteous.

20
00:01:57,000 --> 00:02:01,000
Damika, damika, damika is one who has damika,

21
00:02:01,000 --> 00:02:09,000
I mean, one who has.

22
00:02:09,000 --> 00:02:14,000
So this verse was spoken in regards to

23
00:02:14,000 --> 00:02:21,000
Damika. Damika Tira was a monk who had a very good name,

24
00:02:21,000 --> 00:02:28,000
but he also lived up to his name.

25
00:02:28,000 --> 00:02:32,000
It seems that he was living well,

26
00:02:32,000 --> 00:02:34,000
lived according to the dhamma,

27
00:02:34,000 --> 00:02:37,000
so maybe this is how he got the name,

28
00:02:37,000 --> 00:02:38,000
maybe it wasn't his real name,

29
00:02:38,000 --> 00:02:43,000
but it's what they called him because he was righteous.

30
00:02:43,000 --> 00:02:49,000
He could have also just been his name.

31
00:02:49,000 --> 00:02:53,000
Sometimes names do give power in that way.

32
00:02:53,000 --> 00:02:57,000
Name someone something and they feel like they have to live up to it,

33
00:02:57,000 --> 00:03:00,000
or it guides them through their lives,

34
00:03:00,000 --> 00:03:07,000
so naming someone Tiffany or something might not be as meaningful.

35
00:03:07,000 --> 00:03:11,000
Noah is interesting, it's got an interesting meaning.

36
00:03:11,000 --> 00:03:16,000
It's certainly affected me, no? You've got these names?

37
00:03:16,000 --> 00:03:20,000
You know, on the other hand, what's in a name right?

38
00:03:20,000 --> 00:03:25,000
There was another story of a man named Arya,

39
00:03:25,000 --> 00:03:30,000
and he was, I think he was a fisherman.

40
00:03:30,000 --> 00:03:33,000
And so the Buddha went.

41
00:03:33,000 --> 00:03:37,000
He was walking along one day and they came across this man named Arya,

42
00:03:37,000 --> 00:03:42,000
who was really quite proud of his name and thought he had such a great name.

43
00:03:42,000 --> 00:03:44,000
And the Buddha, I think we'll have this.

44
00:03:44,000 --> 00:03:47,000
I think pretty sure it's in the dhamma pod, so we'll come to it.

45
00:03:47,000 --> 00:03:50,000
So just a little spoiler.

46
00:03:50,000 --> 00:03:52,000
He turns to all the monks and he asks them,

47
00:03:52,000 --> 00:03:56,000
what's your name and all the monks are telling them their names?

48
00:03:56,000 --> 00:03:58,000
And Arya, this man who's sitting there,

49
00:03:58,000 --> 00:04:02,000
maybe he came to listen to the Buddha teach or something.

50
00:04:02,000 --> 00:04:05,000
He's like, oh, I hope the Buddha asks me.

51
00:04:05,000 --> 00:04:08,000
Maybe my name and finally the Buddha gets around and says,

52
00:04:08,000 --> 00:04:09,000
so what's your name?

53
00:04:09,000 --> 00:04:14,000
He says, my name's Arya and he says, it's not a very good name for you.

54
00:04:14,000 --> 00:04:18,000
You don't call someone an Arya who kills.

55
00:04:18,000 --> 00:04:24,000
Anyway, so this guy whose name was Dhammika.

56
00:04:24,000 --> 00:04:31,000
Having listened to the Buddha's teaching often became desires of ordaining.

57
00:04:31,000 --> 00:04:38,000
And so he said to his wife, look, I want to ordain,

58
00:04:38,000 --> 00:04:41,000
but his wife was pregnant.

59
00:04:41,000 --> 00:04:44,000
And so she said, wait, please.

60
00:04:44,000 --> 00:04:51,000
If you must go forth, please wait until our son is our baby is born.

61
00:04:51,000 --> 00:04:55,000
And so he waited and sure enough,

62
00:04:55,000 --> 00:04:59,000
baby was born and then he waited until the baby was old enough to walk.

63
00:04:59,000 --> 00:05:04,000
And then he said to his wife again, I'm really ready.

64
00:05:04,000 --> 00:05:08,000
I've been patient, but I really would like to become a monk.

65
00:05:08,000 --> 00:05:13,000
And this wife says, wait, at least until he's come of age.

66
00:05:13,000 --> 00:05:18,000
So he's into our son and his left home.

67
00:05:18,000 --> 00:05:21,000
And the man was silent, but then he thought to himself,

68
00:05:21,000 --> 00:05:26,000
what good is it to try and wait for her permission?

69
00:05:26,000 --> 00:05:35,000
And he just went off and became a monk.

70
00:05:35,000 --> 00:05:37,000
Kind of reminiscent of the Bodhisattva,

71
00:05:37,000 --> 00:05:41,000
when he left home and everyone's always critical of this.

72
00:05:41,000 --> 00:05:47,000
How can these Buddhists allow for, allow their students,

73
00:05:47,000 --> 00:05:51,000
allow their people to just go forth,

74
00:05:51,000 --> 00:05:55,000
leaving behind wife and child?

75
00:05:55,000 --> 00:05:57,000
How could the Buddha set that kind of example?

76
00:05:57,000 --> 00:05:59,000
I'm not taking care of his family,

77
00:05:59,000 --> 00:06:04,000
so they like to criticize in this way.

78
00:06:04,000 --> 00:06:06,000
When we talk about this person,

79
00:06:06,000 --> 00:06:08,000
there should be something to say on that.

80
00:06:08,000 --> 00:06:17,000
So I'll wait.

81
00:06:17,000 --> 00:06:22,000
But yeah, so he just left home

82
00:06:22,000 --> 00:06:28,000
and became an aura hunt, practiced at a obtained meditation subject

83
00:06:28,000 --> 00:06:33,000
and strived and struggled and became an aura hunt.

84
00:06:37,000 --> 00:06:40,000
After becoming an aura hunt, he went back to Sabati

85
00:06:40,000 --> 00:06:42,000
to where his wife and child were living,

86
00:06:42,000 --> 00:06:44,000
and they gave him arms, food.

87
00:06:44,000 --> 00:06:46,000
And he taught the Dhamma to them.

88
00:06:46,000 --> 00:06:49,000
He would go and sit in their house and teach them the Dhamma,

89
00:06:49,000 --> 00:06:53,000
and sure enough, his son became interested,

90
00:06:53,000 --> 00:06:56,000
while that's followed the family line,

91
00:06:56,000 --> 00:06:59,000
and went forth and became a monk himself.

92
00:06:59,000 --> 00:07:04,000
And in no long time, became an aura hunt himself.

93
00:07:04,000 --> 00:07:09,000
And last but not least, the wife realized that thinking to herself,

94
00:07:09,000 --> 00:07:12,000
what is their left for me in the home life?

95
00:07:12,000 --> 00:07:19,000
He was a monk, a monk, and became an aura hunt herself.

96
00:07:19,000 --> 00:07:24,000
So this is one example of the greatness of ordaining.

97
00:07:24,000 --> 00:07:29,000
And in fact, it's a good example to think about.

98
00:07:29,000 --> 00:07:34,000
They would criticize in the time in the Buddha,

99
00:07:34,000 --> 00:07:39,000
when the Buddha went back to Kapi Nawatu,

100
00:07:39,000 --> 00:07:44,000
all the saki and men followed him,

101
00:07:44,000 --> 00:07:47,000
and even the women eventually followed him

102
00:07:47,000 --> 00:07:50,000
and became the first bhikunis.

103
00:07:50,000 --> 00:07:52,000
And everyone was up at arms saying,

104
00:07:52,000 --> 00:07:56,000
he's taking away our children.

105
00:07:56,000 --> 00:07:59,000
How can he just...

106
00:07:59,000 --> 00:08:04,000
How can these people just take our children away from us?

107
00:08:04,000 --> 00:08:06,000
And so they told the Buddha this, and the Buddha said,

108
00:08:06,000 --> 00:08:09,000
tell them, and this was when Buddhism was new,

109
00:08:09,000 --> 00:08:10,000
you have to remember.

110
00:08:10,000 --> 00:08:13,000
So the word dhama wasn't very well understood,

111
00:08:13,000 --> 00:08:14,000
but he said, tell them,

112
00:08:14,000 --> 00:08:17,000
they were day and according to the dhama.

113
00:08:17,000 --> 00:08:19,000
And that worked somehow.

114
00:08:19,000 --> 00:08:21,000
So they went, and when people...

115
00:08:21,000 --> 00:08:23,000
I think it's a critic,

116
00:08:23,000 --> 00:08:25,000
the criticizer scolded them for this.

117
00:08:25,000 --> 00:08:27,000
They said, we are day and a court...

118
00:08:27,000 --> 00:08:30,000
People are day and under us according to the dhama,

119
00:08:30,000 --> 00:08:33,000
according to the truth, according to righteousness.

120
00:08:33,000 --> 00:08:35,000
And so at that point, if anyone wanted to criticize,

121
00:08:35,000 --> 00:08:38,000
the point was they would have to criticize the teachings.

122
00:08:38,000 --> 00:08:41,000
It made people think and say, well, that's true.

123
00:08:41,000 --> 00:08:45,000
If you become a monk, if it's for a good reason,

124
00:08:45,000 --> 00:08:46,000
if it's according to the truth,

125
00:08:46,000 --> 00:08:49,000
and if there's truth behind the teachings,

126
00:08:49,000 --> 00:08:51,000
then how can you criticize that?

127
00:08:55,000 --> 00:08:58,000
And to some extent, how can you even criticize

128
00:08:58,000 --> 00:09:00,000
leaving behind wife and child,

129
00:09:00,000 --> 00:09:02,000
but we'll get into them.

130
00:09:02,000 --> 00:09:05,000
That's what this verse really addresses.

131
00:09:05,000 --> 00:09:07,000
So there's not much more to this story.

132
00:09:07,000 --> 00:09:09,000
They all became more day and then the monks were talking about it

133
00:09:09,000 --> 00:09:11,000
and thought it was exceptional,

134
00:09:11,000 --> 00:09:14,000
that he was able to set an example for his son

135
00:09:14,000 --> 00:09:17,000
and eventually even his wife followed him.

136
00:09:17,000 --> 00:09:20,000
And the Buddha used this as an example and said,

137
00:09:20,000 --> 00:09:25,000
this is really the way we practice in Buddhism.

138
00:09:25,000 --> 00:09:28,000
This is the direction in which we're headed

139
00:09:28,000 --> 00:09:30,000
and he gave this verse teaching.

140
00:09:30,000 --> 00:09:33,000
And it's an important verse, I think.

141
00:09:33,000 --> 00:09:37,000
One that I always think about,

142
00:09:37,000 --> 00:09:39,000
not atahay to naprasahay to.

143
00:09:39,000 --> 00:09:41,000
Usually I can't remember the rest of it,

144
00:09:41,000 --> 00:09:43,000
but at least that part.

145
00:09:43,000 --> 00:09:46,000
Not for one's own benefit or for the benefit of another.

146
00:09:46,000 --> 00:09:48,000
Should one do evil?

147
00:09:48,000 --> 00:09:50,000
Is the first part of this?

148
00:09:50,000 --> 00:09:53,000
So the most extreme aspect of it is,

149
00:09:53,000 --> 00:09:59,000
there's never any reason to do something

150
00:09:59,000 --> 00:10:00,000
against the dhamma.

151
00:10:00,000 --> 00:10:05,000
Atahay, that's a medimatana.

152
00:10:05,000 --> 00:10:10,000
One should not wish for success in an evil way,

153
00:10:10,000 --> 00:10:16,000
success for oneself in a way that goes against the dhamma.

154
00:10:16,000 --> 00:10:19,000
But there's more to that.

155
00:10:19,000 --> 00:10:23,000
Because as the Buddha says,

156
00:10:23,000 --> 00:10:28,000
the wise man, he says more directly

157
00:10:28,000 --> 00:10:31,000
one should never desire success at all,

158
00:10:31,000 --> 00:10:35,000
really, for one's own sake or for another.

159
00:10:35,000 --> 00:10:37,000
Meaning worldly success.

160
00:10:37,000 --> 00:10:41,000
And this is sort of the idea in the Buddha's teaching

161
00:10:41,000 --> 00:10:47,000
that all of the all of what we're taught

162
00:10:47,000 --> 00:10:53,000
and all that we have our culture pushes us towards.

163
00:10:53,000 --> 00:10:55,000
Is suspected best?

164
00:10:55,000 --> 00:10:57,000
We would consider us to be faulty.

165
00:10:57,000 --> 00:11:00,000
It's based on delusion.

166
00:11:00,000 --> 00:11:03,000
The idea that there is something meaningful

167
00:11:03,000 --> 00:11:06,000
about worldly success.

168
00:11:06,000 --> 00:11:09,000
The Buddha said, as we'll read later on,

169
00:11:09,000 --> 00:11:14,000
better than kingship, better than

170
00:11:14,000 --> 00:11:18,000
lordship over the angels or earth,

171
00:11:18,000 --> 00:11:21,000
better than to be the emperor of the world,

172
00:11:21,000 --> 00:11:24,000
is the attainment of sotapanda.

173
00:11:24,000 --> 00:11:32,000
Sotapati Palangwarang.

174
00:11:32,000 --> 00:11:36,000
Because really, what purpose can there be

175
00:11:36,000 --> 00:11:40,000
in worldly things, in samsara at all?

176
00:11:40,000 --> 00:11:42,000
People ask, what is the meaning of life?

177
00:11:42,000 --> 00:11:44,000
What is the purpose of life?

178
00:11:44,000 --> 00:11:46,000
There's no reason to think that the world

179
00:11:46,000 --> 00:11:49,000
admits of the universe admits of any purpose

180
00:11:49,000 --> 00:11:52,000
that there can be found any purpose in samsara.

181
00:11:52,000 --> 00:11:54,000
The only way you could think of such a purpose

182
00:11:54,000 --> 00:11:56,000
is if you had a God, a divine being,

183
00:11:56,000 --> 00:11:58,000
who was running everything.

184
00:11:58,000 --> 00:12:02,000
In which case, it's more like a prison than anything.

185
00:12:02,000 --> 00:12:05,000
There's no perfection to be found in the world.

186
00:12:05,000 --> 00:12:07,000
It's just always changing.

187
00:12:07,000 --> 00:12:09,000
It's quite natural.

188
00:12:09,000 --> 00:12:13,000
It appears to be quite as one would expect it to appear

189
00:12:13,000 --> 00:12:16,000
to be, just random and chaotic.

190
00:12:16,000 --> 00:12:20,000
Organize according to laws and rules and patterns.

191
00:12:20,000 --> 00:12:30,000
But in the end, there's no protection from suffering.

192
00:12:30,000 --> 00:12:32,000
There's no protection from laws.

193
00:12:32,000 --> 00:12:35,000
There's no protection from the dissolution

194
00:12:35,000 --> 00:12:40,000
of all the things that we strive for.

195
00:12:40,000 --> 00:12:45,000
So there's more to just, more to it than just not doing evil.

196
00:12:45,000 --> 00:12:50,000
Ha ha ha ha ha ha, ha ha ha ha ha ha a performance.

197
00:12:50,000 --> 00:12:53,000
And putamichayen....

198
00:12:53,000 --> 00:12:57,000
Putamichayen one should not wish for sams,

199
00:12:57,000 --> 00:13:00,000
wealth or even a kingdom,

200
00:13:00,000 --> 00:13:05,000
it doesn't you can do nothing by energy?

201
00:13:05,000 --> 00:13:07,000
Just means.

202
00:13:07,000 --> 00:13:10,000
Which will not seek these things out.

203
00:13:10,000 --> 00:13:12,000
Certainly, I mean there's 2 things being said here,

204
00:13:12,000 --> 00:13:17,040
really there's the and the Buddha is kind of is often like this in the sense of

205
00:13:17,040 --> 00:13:20,800
couching it because he doesn't want to alienate people whereas I'm not so

206
00:13:20,800 --> 00:13:26,720
careful perhaps when the commentary may be also not but the Buddha does say two

207
00:13:26,720 --> 00:13:31,120
things here he says you're not desire these things but then he also couches it

208
00:13:31,120 --> 00:13:34,440
and says at the very least I mean that's what it seems like he's saying

209
00:13:34,440 --> 00:13:40,560
because in the next part he says by unjust means ada main where so you could

210
00:13:40,560 --> 00:13:47,040
also interpret this I think to mean one should not seek one should not seek

211
00:13:47,040 --> 00:13:52,840
well outside of the dhamma and in this sense could be outside of the path

212
00:13:52,840 --> 00:13:58,720
outside of the path to freedom so if you're looking for success it should not

213
00:13:58,720 --> 00:14:02,040
be outside of the dhamma which is basically what's being said here of course

214
00:14:02,040 --> 00:14:09,120
the translation usually is ada vatamma is is unjust unrighteous so evil but in

215
00:14:09,120 --> 00:14:13,000
the end Buddha's boil down to the same thing because we will consider on a very

216
00:14:13,000 --> 00:14:20,280
very ultimate if you're going to nitpick on an ultimate level any desire for

217
00:14:20,280 --> 00:14:28,080
sons for wealth or even for a kingdom for anything any desire for anything is

218
00:14:28,080 --> 00:14:34,080
by Buddhist definition evil evil in the sense that it's going to lead to

219
00:14:34,080 --> 00:14:40,160
stress and suffering it has an unpleasant result a result that is not going to

220
00:14:40,160 --> 00:14:44,400
favor us there's not going to make us happy it's going to keep us being

221
00:14:44,400 --> 00:14:51,200
reborn again and again and and keep us in the in the prison of some

222
00:14:51,200 --> 00:15:00,520
Zara so these are things we should look at in the practice it's something we

223
00:15:00,520 --> 00:15:04,440
should check in ourselves because these are views that are deeply ingrained by

224
00:15:04,440 --> 00:15:08,520
society that we've been taught since we were born right as you grow up your

225
00:15:08,520 --> 00:15:13,040
taught by example of your parents so great thing about this story is here we

226
00:15:13,040 --> 00:15:17,840
have someone who is actually an example for their their family going back to his

227
00:15:17,840 --> 00:15:21,840
family could you imagine having a parent who was a monk so in Thailand I hear

228
00:15:21,840 --> 00:15:28,400
about this they someone in their family was a monk and he was actually a role

229
00:15:28,400 --> 00:15:32,920
model for his his child who said I want to be like my father when I grow up and

230
00:15:32,920 --> 00:15:38,800
even for his wife in the end followed along it points to the greatness of

231
00:15:38,800 --> 00:15:45,560
ordaining how it can affect your family and again that the the defense is

232
00:15:45,560 --> 00:15:50,280
that it's it's based on the dhamma so you know leaving your family behind to go

233
00:15:50,280 --> 00:16:00,520
and live a negligent life would be would be definitely a blame worthy but to go

234
00:16:00,520 --> 00:16:07,200
and to live it in a spiritually enlightening way that can only help both

235
00:16:07,200 --> 00:16:18,440
oneself and the people close but most of us get a different sort of role model

236
00:16:18,440 --> 00:16:26,640
right we're taught to be physically attractive we're taught to be eloquent and

237
00:16:26,640 --> 00:16:32,480
charismatic we're taught to be powerful confident I mean confident is

238
00:16:32,480 --> 00:16:36,000
bad even power even eloquent these aren't bad things but we're taught to be

239
00:16:36,000 --> 00:16:40,320
things that are going in a way that's going to help us in the world we're

240
00:16:40,320 --> 00:16:46,480
taught of the value of money the value of of power over others about value of

241
00:16:46,480 --> 00:16:53,960
social status that so many things the value of stability which is totally

242
00:16:53,960 --> 00:17:00,200
totally overrated but it's so seriously ingrained that we're so afraid of

243
00:17:00,200 --> 00:17:07,240
laws or of being shaken up we become so inflexible in our lives we need to

244
00:17:07,240 --> 00:17:12,360
have a stable job and a stable stable income and we often set ourselves up in

245
00:17:12,360 --> 00:17:17,320
these castles that become prisons prisons of debt prisons of

246
00:17:17,320 --> 00:17:23,920
responsibility for the prison of ownership it's an interesting thing we think

247
00:17:23,920 --> 00:17:27,320
it's so great to have possessions but our possessions end up owning us because

248
00:17:27,320 --> 00:17:31,640
then we need locks to keep them safe and we need to protect them and to guard

249
00:17:31,640 --> 00:17:37,600
them and to take care to care for them and so on and we can't just we can't move

250
00:17:37,600 --> 00:17:43,760
busy so they we can't pick up and go anywhere we want and so on so all of these

251
00:17:43,760 --> 00:17:53,160
things and then we're taught about progress you know getting education for the

252
00:17:53,160 --> 00:17:57,560
means of getting a good job getting a good job for a means of having a

253
00:17:57,560 --> 00:18:01,880
retirement plan and so on and in the end it leads to the same death that

254
00:18:01,880 --> 00:18:07,880
the end doesn't lead anywhere different in fact it doesn't lead to anything greater

255
00:18:07,880 --> 00:18:12,640
something maybe we feel proud of at the end of our life sure but even that

256
00:18:12,640 --> 00:18:18,440
pride is a it's in and of itself a problem nothing we should feel proud of

257
00:18:18,440 --> 00:18:24,600
nothing that is of any use on the other hand when we live our lives focused on

258
00:18:24,600 --> 00:18:29,800
the dumb of focused on being moral ethical charitable generous kind

259
00:18:29,800 --> 00:18:37,640
compassionate to be calm and collected and clear-minded and wise you know who

260
00:18:37,640 --> 00:18:43,120
wouldn't be proud of such things as this right of course not be proud

261
00:18:43,120 --> 00:18:48,680
exactly but feel good and feel content feel confident these are things that we

262
00:18:48,680 --> 00:18:56,600
should feel confident about so these are things that we have to look at in

263
00:18:56,600 --> 00:19:00,400
our meditation that we have to look at not just one more meditating but in our

264
00:19:00,400 --> 00:19:05,040
lives and and try and identify them because they can get in the way of

265
00:19:05,040 --> 00:19:10,360
meditation when you want to do meditation but at the same time your keen on

266
00:19:10,360 --> 00:19:15,280
getting a raise or getting a better job or a bigger house or a nicer car getting

267
00:19:15,280 --> 00:19:24,520
married some students who are keen to keen for romance you know it's an

268
00:19:24,520 --> 00:19:30,280
obsession for some people and all of these things we have to look at and

269
00:19:30,280 --> 00:19:37,800
really remind ourselves scold ourselves even this is not really going to make

270
00:19:37,800 --> 00:19:42,360
me happy there's really no benefit in those people who have strived

271
00:19:42,360 --> 00:19:52,560
striven strived for these things I've never ended up content and happy as a

272
00:19:52,560 --> 00:19:56,200
result of them their contentment and happiness is is dependent on other

273
00:19:56,200 --> 00:20:00,480
things it's dependent on their state of mind their clarity and their purity of

274
00:20:00,480 --> 00:20:07,840
mind so these are things that the Buddha reminds us to stay away from and it

275
00:20:07,840 --> 00:20:13,280
puts a paints a clear picture of the difference between the world the way of

276
00:20:13,280 --> 00:20:18,600
the world and the way of the dhamma which go are diametrically opposed Mahasi

277
00:20:18,600 --> 00:20:24,960
Adha gives a great brief talk about this in one of his books he says most

278
00:20:24,960 --> 00:20:29,400
people don't like to hear the dhamma because it's diametrically opposed to

279
00:20:29,400 --> 00:20:34,880
their their way of life for most people these things that I'm talking about are

280
00:20:34,880 --> 00:20:40,160
the right way you know whenever I talk about things like exercise and what

281
00:20:40,160 --> 00:20:46,440
music art beauty there's always someone who's going to be disappointed and upset

282
00:20:46,440 --> 00:20:56,600
and turned off so it's important for us to choose sides in in a sense which

283
00:20:56,600 --> 00:21:01,440
which should choose directions not sides but directions because they don't

284
00:21:01,440 --> 00:21:06,720
go in the same direction nahi dhamma nahi dhamma nahi dhamma nahi dhamma

285
00:21:06,720 --> 00:21:17,960
a dhammaom challenges all boats are in they many places outside of the dhamma

286
00:21:17,960 --> 00:21:20,480
don't go in the same direction

287
00:21:20,480 --> 00:21:24,760
that the more near-youngly deta mo-papi desu-gutting,

288
00:21:24,760 --> 00:21:29,240
the dhamma leads to suffering, the hell actually.

289
00:21:29,240 --> 00:21:34,440
Dhamma leads to heaven, or a good way, leads in a good way,

290
00:21:34,440 --> 00:21:38,400
leads to a good becoming, or a good state.

291
00:21:38,400 --> 00:21:40,880
Eventually, it needs to nibana.

292
00:21:40,880 --> 00:21:43,880
So, that's the dhamma panda for this evening.

293
00:21:43,880 --> 00:21:50,880
Thank you all for tuning in. Keep practicing.

