1
00:00:00,000 --> 00:00:07,360
Welcome back to Ask A Monk. Here I have a couple more days before I have to arrange for

2
00:00:07,360 --> 00:00:19,200
my travel to America and I'm going to take some time out to go off and practice alone.

3
00:00:19,200 --> 00:00:25,960
But in the meantime, I've got some time to answer a couple of questions, so I'm going back.

4
00:00:25,960 --> 00:00:33,680
I'm not going to open Ask A Monk up for new questions because it looks like there's over

5
00:00:33,680 --> 00:00:38,600
a hundred. I can't tell exactly how many questions haven't been answered, but it looks

6
00:00:38,600 --> 00:00:44,760
like there's over a hundred that I haven't answered, so I think that's enough for a while.

7
00:00:44,760 --> 00:00:51,160
And hopefully I'll get through some of them in the next little while.

8
00:00:51,160 --> 00:00:57,560
So the next question I'm going to answer is a question I'm currently reading a book titled,

9
00:00:57,560 --> 00:01:04,400
if you meet the Buddha on the road, kill him. My question is, do you agree or disagree

10
00:01:04,400 --> 00:01:16,400
that enlightenment already exists in all of us without having it taught by an outside influence?

11
00:01:16,400 --> 00:01:26,560
As with many of the things that we talk about in Buddhism, we have to overcome this idea of

12
00:01:26,560 --> 00:01:37,600
re-ifying things or giving an identity to things or an entity, turning things into an entity,

13
00:01:37,600 --> 00:01:44,000
let's put it that way. So when you talk about enlightenment existing inside of us, it kind of sounds

14
00:01:44,000 --> 00:01:50,720
like, and I'm not trying to criticize you, but this is what we often do. We hear about these things

15
00:01:50,720 --> 00:01:57,600
so much that we start to look at them as things, as entities in and of themselves, like you could

16
00:01:57,600 --> 00:02:02,720
cut yourself open and you'd find enlightenment, which obviously isn't what you meant, but that's kind

17
00:02:02,720 --> 00:02:14,640
of what the implication is or what the concept that's really what you're getting at is that the

18
00:02:14,640 --> 00:02:21,360
idea is that there's something inside of us. And what that would be, I'm not sure, it's like the

19
00:02:21,360 --> 00:02:27,440
idea that people say of a soul, that there is a soul inside of us and the soul that goes from one

20
00:02:27,440 --> 00:02:34,640
body to the next, the self, the ideas of God and so on, because in Buddhism this is a core concept,

21
00:02:34,640 --> 00:02:45,360
the idea of deconstructing identities and entities and understanding things scientifically.

22
00:02:46,640 --> 00:02:52,640
So this is important in this question particularly because what do we mean by the word enlightenment,

23
00:02:52,640 --> 00:03:00,400
and there's the word nirvana, and nirvana is something that we often look at as an entity as well.

24
00:03:00,400 --> 00:03:06,880
We think of it as a state or a realm. There's a question coming up about the realm of nirvana,

25
00:03:07,760 --> 00:03:16,960
which can really be misleading because it's really just a name for an experience or

26
00:03:16,960 --> 00:03:26,080
a change that occurs. It's not in and of itself anything. Enlightenment, the word enlightenment,

27
00:03:26,080 --> 00:03:34,960
even if you just look it up in the dictionary, there's nothing secret about what the word enlightenment

28
00:03:34,960 --> 00:03:45,520
means. It simply means wisdom or the awakening to some realization. So then in answer to

29
00:03:45,520 --> 00:03:51,360
your question, it doesn't enlightenment exist inside of us. Well, no, it doesn't because realization

30
00:03:51,360 --> 00:03:55,680
is something that has to come to you. It's something that has to arise. It's not something that

31
00:03:55,680 --> 00:04:00,400
is already inside of you. Either you've realized it or you haven't. If you've already realized

32
00:04:00,400 --> 00:04:05,120
it, then you're enlightened. If you haven't realized it, then you're not enlightened. So for

33
00:04:05,120 --> 00:04:11,520
all of those people who are identified themselves as being unenlightened and who could be identified

34
00:04:11,520 --> 00:04:16,080
as being unenlightened, it's because they haven't realized the certain things. So that realization

35
00:04:16,080 --> 00:04:24,560
doesn't exist inside of them. Unless you think of it as some kind of entity, realization is

36
00:04:24,560 --> 00:04:29,040
something that occurs in the mind. It's something that comes to you. There's no secret hidden spot

37
00:04:29,040 --> 00:04:35,680
inside of us where it exists. Either you realize something or you don't. So this is one part of the

38
00:04:35,680 --> 00:04:41,440
question. The other part is whether you need someone else to enlighten you. And of course the

39
00:04:41,440 --> 00:04:49,520
answer is no. You can realize the truth obviously by yourself. There's no rules. There's no rules

40
00:04:49,520 --> 00:04:57,680
to enlightenment. There's no rules to existence. The universe has certain nature. When you come

41
00:04:57,680 --> 00:05:03,200
to understand this nature, when you come to understand how the universe works, that's enlightenment.

42
00:05:03,200 --> 00:05:10,880
And it can come to anyone. It's a thousand or a million times easier when you're talking to someone

43
00:05:10,880 --> 00:05:16,800
who's already become enlightened or is on the path to becoming enlightened. There are many stages

44
00:05:16,800 --> 00:05:25,760
whereby you are able to let go, where you're able to see freedom from suffering and get a glimpse

45
00:05:25,760 --> 00:05:30,720
of it and start to open yourself up to this realization. To learn from such a person

46
00:05:30,720 --> 00:05:40,960
is obviously far, more beneficial, far easier than trying to find it on your own. But there are

47
00:05:40,960 --> 00:05:45,520
many different paths and many different people. And there's a lot of theories and philosophies

48
00:05:45,520 --> 00:05:51,440
surrounding this. Some people say that you should work until you're able to realize it for yourself.

49
00:05:51,440 --> 00:05:57,840
They want to be able to understand it for themselves without any help from other beings.

50
00:05:57,840 --> 00:06:03,040
And then there's the whole issue of what it is that you have to realize in order to become enlightened.

51
00:06:03,040 --> 00:06:10,080
So I'm not going to go into the details or the various ideas people have about what is enlightenment.

52
00:06:10,080 --> 00:06:16,640
Let's just leave it at that where enlightenment is realizing and a realization of the nature

53
00:06:16,640 --> 00:06:23,920
of the universe and the nature of reality. And in Buddhism or as I understand it,

54
00:06:23,920 --> 00:06:30,560
this is directly related to an understanding of happiness and suffering. What is it that causes

55
00:06:30,560 --> 00:06:38,000
suffering and understanding of an understanding that leads us to become free from suffering?

56
00:06:38,000 --> 00:06:42,960
It leads us to give up those things, those attitudes and behaviors that are causing us suffering.

57
00:06:42,960 --> 00:06:49,840
So it's not simply a realization about the nature of the cosmos or galaxies and solar

58
00:06:49,840 --> 00:06:56,320
systems of physics and the big bang and so on. It's specifically a realization which

59
00:06:56,320 --> 00:07:01,520
frees you from suffering because there are a lot of realizations and understandings that may not

60
00:07:01,520 --> 00:07:10,880
do that. So this is what I understand by enlightenment. And so basically the answer is first of

61
00:07:10,880 --> 00:07:17,120
all that, no, it doesn't exist inside of all of us, but yes, it can be attained by oneself

62
00:07:17,120 --> 00:07:25,360
without another person to guide one. But the point is that it's a lot easier when you have people,

63
00:07:25,360 --> 00:07:30,480
friends. It's not, it doesn't have to be just a teacher-student relationship, friends who are

64
00:07:30,480 --> 00:07:37,600
also striving for enlightenment or friends who have gone farther along the path than oneself

65
00:07:38,400 --> 00:07:44,080
can be a great help. And so that's a good question because I think it's important that we talk

66
00:07:44,080 --> 00:07:50,160
and understand what we mean by enlightenment so that we're not looking around for it or sitting

67
00:07:50,160 --> 00:07:57,120
around waiting for it to suddenly appear or so on. It arise out of its cavity inside of us or

68
00:07:57,120 --> 00:08:04,800
something to open up like a flower or something. It's a realization and it comes from study and

69
00:08:04,800 --> 00:08:10,640
practice and study and meaning, study of oneself and study of reality as you look at reality

70
00:08:10,640 --> 00:08:15,360
and look at your emotions and your addictions and so on, and come to understand them simply

71
00:08:15,360 --> 00:08:20,160
seeing them for what they are, then it's a realization that comes. This is enlightenment. It's

72
00:08:20,160 --> 00:08:27,760
a realization that there's nothing worth clinging to that all of our attachments are not leading us

73
00:08:27,760 --> 00:08:34,000
anywhere closer to happiness than leading us to misery, dissatisfaction and further and further

74
00:08:34,000 --> 00:08:44,000
addiction, onward and onward forever. So I hope that helps. Thanks for the question.

