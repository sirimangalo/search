1
00:00:00,000 --> 00:00:05,000
Okay, so what is the difference between metta and compassion?

2
00:00:05,000 --> 00:00:09,000
Simple question. It does have a simple answer.

3
00:00:09,000 --> 00:00:13,000
I think people are not so clear on the difference between these two.

4
00:00:13,000 --> 00:00:19,000
Not realizing that they are actually different sides of the same coin.

5
00:00:19,000 --> 00:00:23,000
And there is quite a bit of over, there is some sense of overlap,

6
00:00:23,000 --> 00:00:26,000
but they are opposites.

7
00:00:26,000 --> 00:00:28,000
They are opposites sides of the same coin.

8
00:00:28,000 --> 00:00:32,000
So metta, which we translate as actually friendliness.

9
00:00:32,000 --> 00:00:36,000
Metta comes from the word metta, metta, and then you strengthen the eye,

10
00:00:36,000 --> 00:00:40,000
and it becomes an E, and so you get metta.

11
00:00:40,000 --> 00:00:44,000
Metta means friend, metta means friendliness.

12
00:00:44,000 --> 00:00:50,000
What it refers to is the intention, the mind state,

13
00:00:50,000 --> 00:00:55,000
which is intent upon bringing happiness to other beings.

14
00:00:55,000 --> 00:00:57,000
Or not exactly bringing happiness.

15
00:00:57,000 --> 00:01:04,000
Intent upon a wish, or a desire, or an inclination,

16
00:01:04,000 --> 00:01:07,000
because it doesn't have anything to do with desire or attachment,

17
00:01:07,000 --> 00:01:09,000
not necessarily.

18
00:01:09,000 --> 00:01:14,000
But an inclination that beings should be happy.

19
00:01:14,000 --> 00:01:17,000
So you have a being who is in a neutral state,

20
00:01:17,000 --> 00:01:26,000
that it is the wish, the volition, the mind set,

21
00:01:26,000 --> 00:01:31,000
that that person, or people, or that all beings,

22
00:01:31,000 --> 00:01:33,000
should be happy.

23
00:01:33,000 --> 00:01:35,000
So it's a positive one.

24
00:01:35,000 --> 00:01:41,000
Compassion, carona, which we translate as compassion,

25
00:01:41,000 --> 00:01:42,000
is the opposite.

26
00:01:42,000 --> 00:01:47,000
You have a neutral person, and you wish for them

27
00:01:47,000 --> 00:01:50,000
not to fall into suffering.

28
00:01:50,000 --> 00:01:53,000
You may this person be free from suffering,

29
00:01:53,000 --> 00:01:57,000
or, sorry, conversely, you have a person who is in suffering,

30
00:01:57,000 --> 00:02:01,000
and the wish for them to be free from suffering.

31
00:02:01,000 --> 00:02:10,000
The intention, the mind set that being should be free from suffering.

32
00:02:10,000 --> 00:02:13,000
So it doesn't actually require you to do anything.

33
00:02:13,000 --> 00:02:16,000
Neither one of these actually requires actions.

34
00:02:16,000 --> 00:02:21,000
It's these mind sets that will inform your actions.

35
00:02:21,000 --> 00:02:24,000
So as a result of a desire for beings to be happy,

36
00:02:24,000 --> 00:02:29,000
or in a mind set that is good for beings to be happy,

37
00:02:29,000 --> 00:02:31,000
may beings be happy.

38
00:02:31,000 --> 00:02:35,000
As a result, you will then act in such,

39
00:02:35,000 --> 00:02:36,000
or act accordingly.

40
00:02:36,000 --> 00:02:38,000
So you'll do things to bring happiness and peace

41
00:02:38,000 --> 00:02:42,000
and freedom from happiness and peace to people.

42
00:02:42,000 --> 00:02:48,000
Now, compassion, when you have this mind set,

43
00:02:48,000 --> 00:02:52,000
it will inform your actions in such a way that you work

44
00:02:52,000 --> 00:02:54,000
to relieve suffering.

45
00:02:54,000 --> 00:02:58,000
So that when someone is in pain,

46
00:02:58,000 --> 00:03:01,000
you will immediately act to try to free them from that

47
00:03:01,000 --> 00:03:04,000
when they're in mental anguish or despair.

48
00:03:04,000 --> 00:03:09,000
You'll incline towards freeing them from that.

49
00:03:09,000 --> 00:03:12,000
So the problem is that in the end,

50
00:03:12,000 --> 00:03:14,000
it all comes down to the same thing.

51
00:03:14,000 --> 00:03:16,000
That true happiness is freedom from suffering.

52
00:03:16,000 --> 00:03:18,000
They're one and the same.

53
00:03:18,000 --> 00:03:25,000
But the feeling in one's mind is different.

54
00:03:25,000 --> 00:03:28,000
So when one has love for beings,

55
00:03:28,000 --> 00:03:31,000
as friendliness, it's a wish, it's a positive wish.

56
00:03:31,000 --> 00:03:33,000
When one has compassion, it's a wish to take away.

57
00:03:33,000 --> 00:03:36,000
It's a wish to take away someone's suffering.

58
00:03:36,000 --> 00:03:40,000
But the kind of two sides of the same coin.

59
00:03:40,000 --> 00:03:42,000
And so it's an interesting question.

60
00:03:42,000 --> 00:03:47,000
But I think it's a clear answer that compassion

61
00:03:47,000 --> 00:03:50,000
is desire to take away suffering.

62
00:03:50,000 --> 00:04:05,000
Friendliness, or mid-day, is desire to bestow happiness.

