1
00:00:00,000 --> 00:00:09,120
Hello everyone. Today is January 1st 2014. I just like to take this opportunity to

2
00:00:09,120 --> 00:00:16,280
wish everyone a happy new year. A new year that is filled with good things

3
00:00:16,280 --> 00:00:23,440
most especially well-being. You may all find well-being in the new year. We

4
00:00:23,440 --> 00:00:29,840
consider well-being to be more important than what we might call happiness. Because

5
00:00:29,840 --> 00:00:40,360
the word happiness implies some sort of a feeling that is ephemeral subject to the

6
00:00:40,360 --> 00:00:47,960
changes in life. So get it sometimes without it's most of the time or much of the

7
00:00:47,960 --> 00:00:53,040
time. Whereas well-being though it implies happiness it's a sort of

8
00:00:53,040 --> 00:01:01,320
happiness that is lasting. It's a sense of contentment. And so we tend to

9
00:01:01,320 --> 00:01:09,720
wish each other well-being in the new year. The Buddha gave a specific

10
00:01:09,720 --> 00:01:15,080
teaching on well-being. He used this word to describe what he thought was the

11
00:01:15,080 --> 00:01:22,200
goal for beings in the world. You'd be the humans or any type of being that

12
00:01:22,200 --> 00:01:26,320
might exist. What we're all looking for is well-being. You could say we're looking

13
00:01:26,320 --> 00:01:32,040
for happiness but the word he used was well-being. He said there are four things

14
00:01:32,040 --> 00:01:38,720
that you need to find well-being in this universe. And so I just like to

15
00:01:38,720 --> 00:01:44,560
pass these on as a sort of a new year dumber. The first is that we need wisdom.

16
00:01:44,560 --> 00:01:47,760
Obviously this is the most important Buddhism. Without wisdom you can't be

17
00:01:47,760 --> 00:01:54,800
expected to understand what is for your benefit and to your detriment. Wisdom is

18
00:01:54,800 --> 00:01:58,800
what allows you to make decisions that lead to your own happiness and

19
00:01:58,800 --> 00:02:03,920
happiness of others, your own well-being and the well-being of others. It's what

20
00:02:03,920 --> 00:02:09,400
allows you to find the right path and to find the right way for you in the

21
00:02:09,400 --> 00:02:14,100
world, to make the right decisions and so on. Because it isn't to be the

22
00:02:14,100 --> 00:02:19,960
highest that all other qualities of mind, whether they be compassion or love

23
00:02:19,960 --> 00:02:25,640
or joy or contentment, equanimity, these kind of things, humility. They all

24
00:02:25,640 --> 00:02:31,720
require wisdom. If they don't have wisdom it's just a fake, artificial construct.

25
00:02:31,720 --> 00:02:34,800
They don't come from the heart. There's something to come from the heart. It

26
00:02:34,800 --> 00:02:40,600
requires a true understanding of its benefit. An innate understanding of the

27
00:02:40,600 --> 00:02:46,480
benefit of it. And likewise to remove unwholesomeness, requires understanding. To

28
00:02:46,480 --> 00:02:52,880
abstain from anger and greed and delusion requires wisdom. So that's the first

29
00:02:52,880 --> 00:03:00,120
one. The second quality is effort, exertion. Nothing is to be gained through

30
00:03:00,120 --> 00:03:08,560
losing around. What has said, we are able to overcome suffering through effort.

31
00:03:08,560 --> 00:03:14,240
It's not something that can be done without putting out effort. To be moral, to

32
00:03:14,240 --> 00:03:20,280
avoid unwholesomeness, to avoid killing and stealing. It's easy to tell people not

33
00:03:20,280 --> 00:03:24,000
to get angry and want to hurt each other and to greedy and want to steal from

34
00:03:24,000 --> 00:03:29,520
each other and deluded and want to take drugs in alcohol and so on. But our

35
00:03:29,520 --> 00:03:35,320
addictions and our habits lead us back over and again to these things. So the

36
00:03:35,320 --> 00:03:41,080
breaking of habits requires effort. And the cultivation of wisdom also requires

37
00:03:41,080 --> 00:03:47,240
effort. Effort in meditation, abstaining from bad unwholesomeness deeds, cultivating

38
00:03:47,240 --> 00:03:52,640
good deeds, both of these require effort. And the purification of the mind

39
00:03:52,640 --> 00:03:58,280
requires effort. We're going to be a good person and help others. We're going to be

40
00:03:58,280 --> 00:04:03,480
charitable and generous and kind. We have to pull ourselves out of our inertia,

41
00:04:03,480 --> 00:04:12,920
our habits of being lazy and indulgent and so on. Number three, we need to guard

42
00:04:12,920 --> 00:04:19,160
our senses. The eye, the ear, the nose, the tongue, the body and the mind. Guard

43
00:04:19,160 --> 00:04:22,560
here doesn't mean to, we have to avoid seeing and hearing and smelling and

44
00:04:22,560 --> 00:04:26,520
tasting and feeling thinking. The point is not to avoid experience. The point is

45
00:04:26,520 --> 00:04:35,080
to guard the extrapolation of experience. So the judgment, the partiality.

46
00:04:35,080 --> 00:04:38,680
In what we're trying to limit our experience, we're just trying to

47
00:04:38,680 --> 00:04:44,280
limit our reaction to our experience. The one that is objective and wise. So

48
00:04:44,280 --> 00:04:47,200
when you see, you should know that you're seeing. You shouldn't know that

49
00:04:47,200 --> 00:04:51,920
that's bad, that's good. And me and mine and get angry and upset or

50
00:04:51,920 --> 00:04:56,760
addicted and attached to it. When you hear, when you smell, it's not that we

51
00:04:56,760 --> 00:05:01,960
can't experience the whole range of experience. It's that we have to be very

52
00:05:01,960 --> 00:05:05,960
careful to stay objective so that we don't cultivate addiction. We don't

53
00:05:05,960 --> 00:05:10,520
cultivate partiality. We don't cultivate these memories in the mind. That

54
00:05:10,520 --> 00:05:13,600
brought me pleasure. That was a good thing. That brought me suffering. That was a

55
00:05:13,600 --> 00:05:21,600
bad thing. And therefore, cultivate a version and attachment, which of course

56
00:05:21,600 --> 00:05:27,320
leads to pushing and pulling and eventually suffering. They're inevitably

57
00:05:27,320 --> 00:05:31,680
suffering. So guarding our senses is very important. It's not that we

58
00:05:31,680 --> 00:05:38,920
repress our experiences, but filtering is so that the dirt stays out in the

59
00:05:38,920 --> 00:05:43,000
experience alone comes in. And seeing is just seeing, hearing is just hearing

60
00:05:43,000 --> 00:05:48,800
is fine. And number four is we have to give up everything. This is the claim

61
00:05:48,800 --> 00:05:54,200
in Buddhism is that happiness doesn't come from clinging. The way of the world

62
00:05:54,200 --> 00:05:59,320
is that the more you get, the more you hold on, the more happiness you get, the

63
00:05:59,320 --> 00:06:04,240
more the more pleasure you attain. And the Buddha noticed that this isn't

64
00:06:04,240 --> 00:06:07,440
the case. Many religious people have noticed that this isn't the case, that

65
00:06:07,440 --> 00:06:11,640
it's through letting go and giving up, that you find true happiness. So in

66
00:06:11,640 --> 00:06:16,040
Buddhism, this is the claim that only through letting go can you be truly happy

67
00:06:16,040 --> 00:06:20,200
and truly free? Because true happiness has to be what we call an nimisa

68
00:06:20,200 --> 00:06:32,360
suka. An namasa means a namasa means a niramasa.

69
00:06:32,360 --> 00:06:37,520
Niramisa, sorry, niramisa, which means having nothing to do with an object,

70
00:06:37,520 --> 00:06:41,280
not taking an object. Happiness that it is dependent on something,

71
00:06:41,280 --> 00:06:47,000
it's dependent on what is impermanent. Happiness should be this well-being,

72
00:06:47,000 --> 00:06:53,360
should be our contentment with the sister's of life. If your happiness

73
00:06:53,360 --> 00:06:58,960
depends on something, unless that thing is eternal as permanent, then it's

74
00:06:58,960 --> 00:07:03,520
going to create some instability and the potential for loss. Loss is a common

75
00:07:03,520 --> 00:07:09,720
experience in the world for people who cling to things, for people who hold on to

76
00:07:09,720 --> 00:07:13,200
things. They will inevitably have to suffer loss if not in this life than in

77
00:07:13,200 --> 00:07:18,160
future lives. Because of impermanence, you can't hold on to anything forever.

78
00:07:18,160 --> 00:07:23,200
So true happiness, the way to go to find true happiness is not clinging, it's

79
00:07:23,200 --> 00:07:27,800
not depending, it's not hedging your bet that something is going to last and

80
00:07:27,800 --> 00:07:31,280
hoping that you get through life without losing the things that you love. True

81
00:07:31,280 --> 00:07:35,560
happiness is through giving that out, giving up the whole worry and concern

82
00:07:35,560 --> 00:07:41,800
about losing this or that thing that's finding true contentment. And so the

83
00:07:41,800 --> 00:07:46,040
similarly is like the difference between someone who clings to something, being

84
00:07:46,040 --> 00:07:49,440
afraid that they're going to fall, to be cling to the side of a cliff, being afraid

85
00:07:49,440 --> 00:07:52,400
that you're going to fall. This is how most people of their lives always worry

86
00:07:52,400 --> 00:07:56,440
about what the consequences of letting go are going to be. As opposed to a bird

87
00:07:56,440 --> 00:08:02,280
that let's go of something knowing that they're going to fly or realizes that

88
00:08:02,280 --> 00:08:06,280
they're able to fly and so they let go. And this is the change that we have to

89
00:08:06,280 --> 00:08:12,600
come about and we have to realize that to be truly happy, to fly, to not be

90
00:08:12,600 --> 00:08:18,760
stuck to anything. We have to let go, to find true happiness, we have to give up

91
00:08:18,760 --> 00:08:24,000
our attachment. And that happiness that comes from letting go, we call well-being.

92
00:08:24,000 --> 00:08:31,920
You know, it's a state of being that is content and that piece with itself for

93
00:08:31,920 --> 00:08:37,360
eternity. So that is the Buddhist teaching on well-being and that's the

94
00:08:37,360 --> 00:08:41,120
dhamma for the new year that we may all find well-being. If we want to find

95
00:08:41,120 --> 00:08:45,120
well-being we should cultivate these four things. It's the kind of framework for

96
00:08:45,120 --> 00:08:48,880
resolution that we should have in the new year. So some food for that. Thank you

97
00:08:48,880 --> 00:08:59,680
all for tuning in and wishing you all a happy and well-new year, 2014 or

98
00:08:59,680 --> 00:09:02,960
whatever year it might be in your calendar. So wishing you all the best

99
00:09:02,960 --> 00:09:20,720
peace and happiness.

