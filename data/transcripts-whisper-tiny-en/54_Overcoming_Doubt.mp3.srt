1
00:00:00,000 --> 00:00:06,160
overcoming doubt. Why is it so important to overcome doubt early in the practice?

2
00:00:07,520 --> 00:00:11,440
Doubt is one of the five hindrances. If your mind is consumed by doubt,

3
00:00:11,440 --> 00:00:16,880
it will be impossible to commit yourself wholeheartedly to the practice. So overcoming doubt is

4
00:00:16,880 --> 00:00:23,520
very important. Purification by overcoming doubt is one of the seven purifications in the path of

5
00:00:23,520 --> 00:00:30,800
purification, coming after purification of view. Purification of view means changing the way

6
00:00:30,800 --> 00:00:35,360
you look at the world, moving from a conceptual view of reality to one based on direct experience.

7
00:00:36,480 --> 00:00:42,240
Purification by overcoming doubt relates to doubt about how reality works, so it is logical that

8
00:00:42,240 --> 00:00:48,320
it will arise only after you have a proper perspective on reality. Doubt is a quality of mind

9
00:00:48,320 --> 00:00:53,120
that most people deal with on a regular basis, trying to decide what to do with their lives,

10
00:00:53,120 --> 00:00:57,920
finding the right course of action in every situation. We doubt about our lives,

11
00:00:57,920 --> 00:01:02,480
about what we should do and about the nature of the world. Much of science was created to help

12
00:01:02,480 --> 00:01:06,800
answer questions about the nature of our world and to help us understand reality so that we might

13
00:01:06,800 --> 00:01:12,720
live better lives. Religion was created for the same sort of purpose, both religion and science

14
00:01:12,720 --> 00:01:17,440
try to provide us with answers to subdue our doubts, though what we call religion has often been

15
00:01:17,440 --> 00:01:23,040
more about belief without much basis in evidence, while science has been much more about evidence-based

16
00:01:23,040 --> 00:01:28,480
knowledge. Psychologically, there is quite a bit of overlap when it comes to believing something

17
00:01:28,480 --> 00:01:33,280
and knowing it. What is important is that you are provided with answers that quell your doubt.

18
00:01:33,280 --> 00:01:39,280
Science provides answers based on evidence, and so it is very good at overcoming doubt conventionally.

19
00:01:39,280 --> 00:01:43,680
If you rely on science, if you believe and put your faith in science, then it is very good at

20
00:01:43,680 --> 00:01:49,600
overcoming intellectual doubt. Very good at helping one find ways to live one's life. Science

21
00:01:49,600 --> 00:01:55,200
cannot, however, free one from doubt completely for two reasons. First, because science does not

22
00:01:55,200 --> 00:02:00,480
know everything, and scientists have not found answers to all questions. And secondly, because the

23
00:02:00,480 --> 00:02:05,840
answers science gives are not one's own answers. Science cannot help you know the truth of your

24
00:02:05,840 --> 00:02:10,480
reality. If you look at depression, for example, science can help you understand the technical

25
00:02:10,480 --> 00:02:15,200
aspects of depression, and this understanding may help you deal with depression on a superficial

26
00:02:15,200 --> 00:02:19,840
level, but it cannot provide you with the direct understanding required to free you from it.

27
00:02:19,840 --> 00:02:23,600
Likewise, for anxiety, fear, and all other sorts of mental suffering.

28
00:02:24,800 --> 00:02:28,960
Science cannot tell you how to live your life or how to react to situations.

29
00:02:28,960 --> 00:02:33,760
It cannot bring true wisdom. This is what we commonly understand as the difference between wisdom

30
00:02:33,760 --> 00:02:39,440
and intelligence. Religion, on the other hand, tries to provide wisdom, making claims on topics like

31
00:02:39,440 --> 00:02:44,000
ethics, happiness, and the nature of reality. Most religion, however, is more about providing

32
00:02:44,000 --> 00:02:49,920
psychological support than actual understanding. Belief in God, for example, is best understood

33
00:02:49,920 --> 00:02:55,680
as a coping mechanism of sorts. People use God to reassure themselves that there is meaning to life,

34
00:02:55,680 --> 00:03:00,640
no matter how hard things get. From a point of view of psychology, God and most of the other

35
00:03:00,640 --> 00:03:05,600
concepts we associate with religion are just tools that help cope with the difficulties of life.

36
00:03:05,600 --> 00:03:10,080
Most of religion is like this, Buddhists around the world perform all sorts of rituals,

37
00:03:10,080 --> 00:03:15,760
like chanting, ringing bells, bowing, and so on. Psychologically, these are useful practices

38
00:03:15,760 --> 00:03:20,880
that help promote positive states of mind, but none are really enough for us to completely overcome

39
00:03:20,880 --> 00:03:26,560
doubt. Overcoming doubt is the stage on the path where the meditator begins to actually

40
00:03:26,560 --> 00:03:32,080
understand not only the nature and building blocks of reality, but how it works. Specifically,

41
00:03:32,080 --> 00:03:37,120
how the physical and mental aspects of experience work and interact, and even more specifically,

42
00:03:37,120 --> 00:03:42,320
how our experiential problems come to be. Whatever biggest problems is that we do not even

43
00:03:42,320 --> 00:03:48,240
understand our problems. When we feel anxious or depressed, we usually do not even know where it

44
00:03:48,240 --> 00:03:53,520
comes from. When you go to the doctor for anxiety or depression, they can explain the cause in terms

45
00:03:53,520 --> 00:03:58,560
of the physical state of your brain, which may be conceptually correct, but it is wrong in the sense

46
00:03:58,560 --> 00:04:03,920
that this sort of answer cannot for you from depression. It is the wrong answer because it is the

47
00:04:03,920 --> 00:04:10,080
wrong type of answer. The right type of answer is one that we see for ourselves, one in which

48
00:04:10,080 --> 00:04:15,200
we comprehend our disorders for what they really are. After becoming familiar with the nature

49
00:04:15,200 --> 00:04:20,080
of experience, the meditator begins to see how experiences work in terms of cause and effect.

50
00:04:20,640 --> 00:04:27,760
This is called the second of the 16 stages of knowledge. This second stage, or the knowledge

51
00:04:27,760 --> 00:04:34,640
that grasps conditionality, is true understanding of the law of karma. In Buddhism, karma means

52
00:04:34,640 --> 00:04:40,160
mental inclination, intention, or volition. At this stage, the meditator observes through direct

53
00:04:40,160 --> 00:04:44,560
experience that inclination leads to resultant actions, which lead to resulting effects.

54
00:04:45,360 --> 00:04:49,920
There is no need for belief, as the meditator sees directly that certain mind states lead to

55
00:04:49,920 --> 00:04:56,400
certain results, which is a crucial part of the mechanics of reality. Knowledge that grasps

56
00:04:56,400 --> 00:05:02,080
conditionality, involves seeing how craving is a cause for suffering, which often causes some

57
00:05:02,080 --> 00:05:08,000
conflict as one wrestles with one's prior perceptions of the benefits of craving. Some meditators

58
00:05:08,000 --> 00:05:12,240
first instinct is to think that something is wrong with their meditation, because they cannot

59
00:05:12,240 --> 00:05:17,200
believe that craving leads to suffering. When they want things, they get them, so they see the

60
00:05:17,200 --> 00:05:22,160
problem as meditation preventing them from getting what they want. They often see a lot of suffering

61
00:05:22,160 --> 00:05:26,800
and stress in their minds at this stage as a result of their attachments. This stage of the

62
00:05:26,800 --> 00:05:31,600
meditation can be quite stressful, as one is forced to choose between clinging to one's desires

63
00:05:31,600 --> 00:05:37,440
or letting them go. Both seem to be potential ways to free oneself from suffering. Your two options

64
00:05:37,440 --> 00:05:42,800
are, get what you want, or give up the wanting. Either method theoretically frees you from the

65
00:05:42,800 --> 00:05:49,280
wanting and suffering. Purification by overcoming doubt leads to the knowledge that the things

66
00:05:49,280 --> 00:05:54,560
that we cling to are not stable or predictable, not satisfying and not controllable. These are

67
00:05:54,560 --> 00:06:00,560
called the three characteristics of all or risen phenomena. Seeing how phenomena interact with

68
00:06:00,560 --> 00:06:05,520
each other on a moment-to-moment basis leads one to realize that the problem is not the failure to

69
00:06:05,520 --> 00:06:11,520
acquire the objects of our desire but the desire itself. One realizes that if one were content,

70
00:06:11,520 --> 00:06:17,280
one would not suffer. The things we cling to are impermanent on satisfying and uncontrollable.

71
00:06:17,280 --> 00:06:22,960
They cannot possibly provide real satisfaction. They are impermanent, uncertain, and unstable.

72
00:06:22,960 --> 00:06:28,080
They are suffering in the sense that they are unsatisfying. And if one clings to them, one will

73
00:06:28,080 --> 00:06:34,160
suffer because they cannot last. They are not oneself. They do not belong to oneself and they

74
00:06:34,160 --> 00:06:39,920
have no self of their own to cling to. They are experiences that arise and cease based on causes

75
00:06:39,920 --> 00:06:45,200
and conditions. The realization of the three characteristics can only come about

76
00:06:45,200 --> 00:06:50,640
after purification by overcoming doubt. Since one must first observe the causal relationships

77
00:06:50,640 --> 00:06:56,080
between phenomena before one can understand their value. Freedom from doubt means certainty

78
00:06:56,080 --> 00:07:00,560
afforded by direct knowledge, which provides the mind the fortitude required to attain the

79
00:07:00,560 --> 00:07:05,680
higher stages of knowledge and freedom from suffering. Before attaining this freedom from doubt,

80
00:07:05,680 --> 00:07:10,800
you might think that the meditation is causing the impermanence, suffering and non-self-characteristics

81
00:07:10,800 --> 00:07:16,080
that you observe in your meditative experiences. You might be inclined to stop meditating,

82
00:07:16,080 --> 00:07:20,960
so as to return to ordinary reality, where things are pleasant, satisfying, and uncontrollable.

83
00:07:22,000 --> 00:07:25,680
This is because meditation forces you to confront the nature of reality,

84
00:07:25,680 --> 00:07:30,400
whereas ordinarily we are constantly avoiding it. Once you see clearly the causal

85
00:07:30,400 --> 00:07:35,680
relationship between experiences you have no need to avoid or manipulate reality as you are able

86
00:07:35,680 --> 00:07:42,240
to skillfully respond to any experience without reaction or partiality. The state of mind before

87
00:07:42,240 --> 00:07:46,960
attaining freedom from doubt is like beginning as a carpenter. Before you can build a cabinet,

88
00:07:46,960 --> 00:07:51,680
you have to learn about your tools. In the early stages, you are still learning how reality works.

89
00:07:51,680 --> 00:07:56,320
Once you do that, and the doubt disappears, your mind becomes a client in wielding like a tool

90
00:07:56,320 --> 00:08:01,760
in the hands of a skilled carpenter. Such a mind is a tool you can apply towards overcoming

91
00:08:01,760 --> 00:08:11,760
suffering. That is why it is important to overcome doubt.

