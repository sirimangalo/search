1
00:00:00,000 --> 00:00:08,000
Hello fellow youtubers. Just thought I'd give a shout to let everyone know that I'll be

2
00:00:08,000 --> 00:00:13,520
starting to give talks in second life, which is this online virtual reality that you

3
00:00:13,520 --> 00:00:23,400
can look up at secondlife.com. I know it sounds a little bit strange. I myself was not

4
00:00:23,400 --> 00:00:28,920
really interested until I found out that there was a Buddha Center in second life and

5
00:00:28,920 --> 00:00:33,160
they have teachers come and give talks there and they've asked me to give talks, they're

6
00:00:33,160 --> 00:00:40,680
starting this Thursday. So I'll be giving talks every almost every day, every day except

7
00:00:40,680 --> 00:00:49,280
for Tuesdays, no, every day except for Mondays and Wednesdays at 12 noon. So if you want

8
00:00:49,280 --> 00:00:56,480
you can check me out, come and listen to the talk at second life at the Buddha Center,

9
00:00:56,480 --> 00:01:04,920
just look up Buddha Center under the places in second life. And I'm also around there giving

10
00:01:04,920 --> 00:01:09,120
instruction on meditation. If you'd like to talk about your practice, get some advice,

11
00:01:09,120 --> 00:01:14,920
we can sit and have a chat around a virtual fireplace or under virtual trees or so on.

12
00:01:14,920 --> 00:01:21,760
It's actually a pretty good setting and quite a useful tool for teaching and for spreading

13
00:01:21,760 --> 00:01:28,720
the meditation teaching. So hope to see you there. If you have any trouble, please let

14
00:01:28,720 --> 00:01:33,360
me know or if you like more information, just give me a shout. Okay, thanks for tuning

15
00:01:33,360 --> 00:01:56,480
in and hope to see you there.

