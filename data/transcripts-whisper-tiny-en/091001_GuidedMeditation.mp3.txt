Namato. Namata, yes, sir. So welcome, everyone, to today's talk on insight meditation.
And I thought that it would be good to sort of frame these talks as a sort of a meditation session.
So I'd like to invite everyone, if you would, to set your animator to busy, if you're afraid, many of us might get.
I am instant messages during this time, so we'll just set our avatar as to busy, if you like.
And that way we can focus better on what's the task at hand.
If you feel comfortable, you can try to sit on the floor, you can sit comfortably in a chair.
And you can just close your eyes and listen to the talk, or keep your eyes open if it's more comfortable.
But we're going to try to use this time as a sort of a guided meditation.
It's just really how we should look at a Dharma talk anyway.
We can see in the time of the Buddha, when he taught meditation, when he taught Buddhism,
it was always with an emphasis on the practical, and very often after the Lord Buddha finished talking.
By the time he finished his talk.
And people who listen would have gained some realization through their practice during the time they were listening.
So this is sort of the kind of thing that we hope for on maybe a smaller scale during the time of listening to talk on the Buddha's teaching,
especially one about meditation.
So I thought I'd take the opportunity today to sort of go over some meditation basics,
and kind of take it as a guided meditation, sort of meditating as we go.
So sit comfortably, try to get into a meditation position, and I'll just start explaining some things.
For those of you who already have your own meditation practice, you're welcome to just meditate.
Take up that practice. You can disregard whatever instructions seem contrary to the method that you follow.
So meditation, the word meditation can mean different things to different people.
For some people it means simply an escape, a retreat, a vacation from the troubles and difficulties that we encounter in our lives.
But the way I like to look at meditation, and if you really look at the word meditation, it means to consider, to reflect, to examine,
and to find a solution to the problems that exist in the mind.
So it's not like running away from your problems, it's like facing them and learning more about them and coming to understand
how the problem works, how the problem situation works, to create suffering for you.
So this is on a very phenomenological level, so in the present moment, even right now, there are many things that have the potential to bring us suffering.
Maybe sounds around you, maybe the feeling of the hardness of the chair or the stiffness in your back, or even pain in the head or joints, pain in the stomach, pain in different parts of the body.
Memories that are coming back again and again, they can be fears or worries or stresses.
It can be both physical and mental, things which bring us suffering.
And the reason these things can bring us suffering is because we don't really understand them, we don't really see them for what they are.
We mistake them for me, for mine, or something that I have to react to.
And as a result, we do react, trying to change, trying to alter, trying to modify things to be the way we want them to.
Meditation seeks to remedy that, to allow us to see things simply for what they are and not judge or stress out about, obsess over the things that we experience.
And in classical Buddhism, the objects of meditation for which we're going to examine reality are called the four foundations of mindfulness.
These are four parts of reality or four classifications of reality, as it pertains to meditation practice.
So the first one is the body, and this is always considered to be the base meditation object.
We always return to the body because it's always prominent.
It's almost always easy to examine, easy to focus on.
And as I've said before, as a result of looking at the body, we're going to understand the mind better.
So here we're going to start focusing on the body in the tradition that I follow.
We focus on the stomach because when we're sitting still, it's for the only thing that moves.
For beginners, it might mean more in the chest.
The stomach might not actually seem to move that much.
If you want to become more comfortable with it, you can place your hand on the stomach until you can get a feel for its movement.
Just place your hand there, and you'll feel the stomach rising in the body.
Now the technique that we're going to use to remedy things is we're trying to straighten our minds out.
The Lord Buddha's words is that a wise person straightens their mind just as a Fletcher straightens the arrow shaft,
someone who plumbers on direct water or as a carpenter straightens wood, or so on.
The wise straighten their mind.
So the idea is that our minds are crooked in regards to these things are bent out of shame.
And all we're going to do is straighten our minds out. Instead of seeing it as rising, we see it as good or bad as comfortable or uncomfortable as me as mine and so on.
Trying to make it more comfortable, trying to make it more deep, more shallow.
Trying to force it, trying to control it.
So the tool we're going to use to straighten our minds is the mantra.
And mantra is often understood to be something for focusing the mind on a concept.
But here we're going to use it to focus the mind on reality.
So when the stomach rises, we're going to use the mantra to remind ourselves of the reality of the rising,
keeping our minds simply fixed on the reality of it.
So our minds don't wander into evolution.
When it rises, we say to ourselves rising.
When it falls, we say to ourselves falling, simply reminding ourselves of what's going on.
Not letting our minds make more of things than they actually are.
And what we notice when we do this,
then you can just keep doing it, keep acknowledging it as I speak.
Or you can focus on the words being spoken.
You can acknowledge them as well.
You can acknowledge the sound is hearing, hearing, and so on.
What we're going to notice is that we can't focus on the rising and falling not easily.
Because there are many other things which take our attention away.
And the first thought is to try to do away with these distractions from the breathing.
And the force our minds to stay with the one object.
But in insight meditation, we're not trying to do this.
We're trying to understand reality and watch reality and not judge it.
Not judge one thing is better than another.
So we're going to take objects in sequence, whatever comes up, we're going to look at that.
And so we have three other foundations of mindfulness, not just the body.
When we're focusing on the rising and falling, this is one part.
When we're focusing on the sound of the ear, this is body, and this is physical.
But then there's also a second one which is feelings or sensations.
So sometimes when we're sitting, we might feel pain in the legs or pain in the back or pain in the hand or pain in the stomach,
pain in the chest, pain wherever.
And this is a totally valid meditation object.
It's not less valid than the rising and the falling of the abdomen.
So whereas normally we'd let it disturb us, bring us stress and suffering.
Now we're going to come to see it for what it is and remind ourselves that it's only pain.
It's only a sensation, it's only a part of reality.
There's no reason to judge it, no reason to get upset by it, no reason to obsess over it.
And so we say to ourselves, pain, pain, pain staying with it no matter how strong it gets, no matter how bad it gets.
Because when we're aware that it simply is pain, then it's neither good nor bad.
It has no power over us.
It simply is what it is.
And we're with reality.
We're with the reality of the object.
We don't know anything besides the reality of it.
There's no room in our minds.
We're mindful, our minds are full of the reality.
And we can just say that as long as there's pain there.
And when it's gone, we just come back to the rising and the following.
Come back to the body.
But we also see that there's other things.
The third one is the mind.
So we're going to see that our mind is thinking a lot.
Often the mind is wandering into the past, wandering into the future.
The beginning of all diversification, where we begin to make more of something than it actually is,
is with the mind where we start thinking about things.
We start thinking about remembering things or worrying about the future or thinking about what's happening right now
and starting to form judgments and views and ideas about it.
So we can catch these as well.
If we get better at this, we can start to catch the thoughts.
And this is where mindfulness of the body is very useful
because it's like we're waiting for the mind ready to catch it when it wanders away
and we're retraining it, training it out of this obsession with distracted thought.
And so we say to ourselves thinking, thinking, just reminding ourselves that we're thinking.
And what happens then is that we're able to see it as something in the present
and not follow it into the past or the future into delusion or end illusion.
And then it disappears and we can come back to the rising of the following.
This is without forcing it, without pushing it away.
We're simply acknowledging it's existence.
And simply by acknowledging it, it loses all power over us.
And it disappears by itself.
And the final one on a basic level is our emotions when we have anger, liking or disliking,
greed or anger, liking, wanting, disliking anger, sadness, fear or worry, sadness.
This is what comes from thinking, comes from, and this becomes from distracted thought.
When we make more of something than it is, we have all these emotions that come up, judging things as good or bad and liking or disliking.
And this is the real cause for suffering.
This is where suffering really comes into existence through our judgment.
Our judgment of things which are just arising and ceasing which have no intrinsic positive or negative value.
So in order that these things should not grow and should not be able to feed on the experience, we focus on them.
We look at them, we acknowledge them, we keep our minds full of the reality of them.
So what happens then is they can't go back and find fuel in the object of our attention.
They are the object of our attention.
We focus on the anger, we say to ourselves, angry, angry.
When we want something wanting, wanting, when we like something liking, when we feel sad, sad, when we feel depressed,
depressed, depressed, depressed.
Just keeping these things in mind and not letting ourselves then judge them thinking,
oh now I have to do something about it.
I am angry so I have to hurt someone or say something nasty.
I want something so now I have to go after it.
I feel depressed so now I have to take medication or go see a doctor.
Really you don't have to do anything even about these emotions.
Simply see them for what they are and not to disappear by themselves.
It is because of our mind that always makes more of things than they are.
This is why we get these emotions in the first place.
But not seeing things as they are.
So we focus on these as well.
We focus on our thoughts and we focus on our emotions.
We can try this now together just for a couple of minutes.
Thank you very much.
Thank you very much.
Thank you very much.
Thank you very much.
And when there is nothing left to acknowledge we can then go back again to the rising of the falling of the stomach.
Whenever something comes up again we go out and let it be our object of meditation.
Just naturally taking whatever is present.
When there is nothing left to come back again to the rising of the Carbon deficiency.
Some other things that we have to watch out for in meditation are besides liking and disliking.
Everyone is drowziness, meditating we can often feel tired, often feel like our minds are drowsy,
feel like we're falling asleep.
So this isn't something that we have to worry about, it's something that we use as a meditation
object as well, and we come to just see it for what it is, it's an amazing thing about
all of these things, including drowziness, is that once you start focusing on them.
And if you're really good at catching them and seeing them for what they are, they just disappear,
they don't have any power over you anymore.
It's like a straightening out of the mind, it's like the only reason it was there in the
first place, is because the mind was bent out of shape, and as soon as you can catch it up,
it disappears, but you have to be good and you have to be consistent, and you have to stay
with it, just saying to yourself, drowsy, drowsy, trying to catch up the drowsyness in your attention.
The opposite, of course, is when the mind is too awake, when the mind is too alert,
is too active, distracted, thinking a lot, thinking so many different things, not just
thinking, but it means the mind is unfocused, it's a general state of mind.
This can be a real hindrance to meditation.
So in this happens, we just put everything aside and just focus on the state of being distracted,
watching our mind run hither and thither, saying to ourselves, distracted and distracted.
Thank you for watching.
Thank you for watching.
Thank you for watching.
The final hindrance in meditation is probably the worst one, the one which causes the most
trouble for, especially for beginner meditators, is doubt.
When you have doubt about yourselves or doubt about the meditation practice, doubt about
which way is right, which way is wrong, doubt about the things you're experiencing, confusion
about what they might be, not being sure.
When the mind is not sure, it's like a fork in the road and you don't know which one
to take, you don't get anywhere.
And often this is very difficult for meditators to know how to deal with doubt.
Sometimes we go the wrong way and stop meditating.
We think that the doubt has some significance and that means what we're doing is probably
wrong.
If it were right, we would never doubt it and that's not certainly not the case.
We can be doing the perfectly right thing and still doubt it.
And why that is, because doubt is just a quality of mind, it's something that we have
and we carry around with this and we end up doubting just about everything.
We don't give things a fair chance.
We don't really try to understand the phenomenon and so we just doubt about it.
So it's a real hindrance in our practice, we can't get anywhere.
Instead of examining it and trying to see the truth about it, because after we see the
truth, of course, we wouldn't need any doubt, whether it's good or bad.
So for this reason, we don't give doubt any weight, any importance.
We simply say to ourselves, doubting, doubting, just focusing on the doubting, put everything
aside, put aside the practice, put aside the phenomenon, put aside the experience, just focus
on the doubt itself.
When you do this, you see how much better you feel when you give up doubt.
When you just don't care right or wrong, you just be.
You're not trying to prove anything, you're not trying to be anything.
You're just here and now.
And when the doubt disappears, you feel much more common, much more peaceful.
So you realize there's really nothing, no need to doubt whatsoever, there's nothing
doubt dubious about simply being with reality, and when these things are gone, then we
can focus back on the practice, focus back on reality.
And what we're going to do is we practice this, what's going to happen is our mind's
going to straighten out and these hindrance has come less and less.
There's less liking, less wanting for things.
There's also less disliking or discomfort, depression, sadness, these things become less
and less frequently.
And they still come back again and again in waves because of all the many years we've
developed and accumulated and encouraged them, but they come less and less frequently.
There's less laziness or drowsiness, there's less mental distraction, there's less doubt.
And so our mind is much calmer, much more peaceful than before.
At this point, many meditators can think they've reached the ultimate goal and they become
lazy.
And as a result of the laziness, they slip back down and things get bad again, and when
things get bad, they aren't ready to deal with it because they become lazy.
The mind's were so peaceful, they thought that it was somehow permanent.
And so what we do is even when the meditation starts to work, starts to bring bare
fruit.
We don't stop there, we focus on the fruit as well.
When we feel calm, we say to ourselves, calm.
When we feel happy, we say to ourselves, happy, happy taking it up as our meditation
object, not making more of it than it is, not saying to ourselves, oh, this happiness
means x, this peace means y.
When you're happy, it's just happiness.
When you're at peace, it's just peace.
And you start to see that actually it's impermanent as well.
It's also changing, also subject to the laws of nature.
It comes because of some cause, generally the meditation practice, and when you stop
practicing it disappears, it's not the goal, it's not the result which we're looking for.
So we put it aside and come back to where we let it go when it's gone, it's gone, when
it's there, when it's gone, it's gone.
When we let it go according to nature, we don't mourn when it's not there, or seek
after it when it's missing, because we don't attach to it when it's present.
In this way, we can be comfortable with all experience, with all of reality, the whole
diversity of experience, not segmenting our experience, or segmenting in reality into good
and bad.
Everything is what it is, and our happiness has nothing to do with experiential phenomenon.
Second, Our
So, I will stop there.
As far as the teaching for today, if people have any questions, you're welcome to ask.
Thank you all for coming.
Is there a difference between a spoken mantra and concentration on concentrating on breath?
Yes, I would say there is, because concentration is not the same as mindfulness.
The word mindfulness has its own meaning, and as I've said, it's a fairly poor translation.
Mindfulness is the remembering of something for what it is.
So when we say to ourselves, rising, we're reminding ourselves of what it is, not allowing our minds to attach to it, to stay bent out of shape.
The nature of the mind in its ordinary state is bent.
This is how we come into this world. We don't come into the world pure.
So what we're trying to do is train ourselves to straighten it out. It takes something beyond simply concentration.
It takes work. It takes mindfulness.
Concentrating on breath, generally uses a mantra anyway in Buddhist circles.
Often they'll say Buddha, Buddha is the name of the Buddha. They'll use it for the breath.
In the traditional text, which is probably more proper, they would use in-out-in-out, or bhassasami, sasami, breathing in, breathing out.
Or they would count it, saying in one-out-one, in two-out-two, in three-out-three, in four-out-four, or just counting one, two-three, four up to ten and back down again.
But this is an insight meditation because it's not awareness of the reality of it.
It's just a concept which is used to calm the mind.
One-out-one, Two-out-one in the world, are doing a lot of possibilities.
welcome
.
..
..
..
..
..
..
..
..
..
..
..
..
..
There's no questions.
There's no questions.
Please let me know otherwise.
We'll just stop it there.
Ah, here.
Must one have a one pointed mind before conducting rebass in there?
No, one must one need not have a one-pointed mind before conducting me pass in a.
If that were the case, the Buddha would not have encouraged us to take the five hindrances
as a meditation object in Vipassana because when one starts practicing the four foundations
of mindfulness, one is including the five hindrances in one's awareness in one's attention.
So, this is clear that at the beginning of meditation one is not one-pointed.
Now, the reason I hesitated in answering is because according to the Abhidhamma, the mind is always one-pointed.
Even the very distracted mind is still one-pointed.
It's always fixed on a single object.
Another reason why one might hesitate in answering this is because even though one begins to practice Vipassana meditation
or begins to practice meditation without a single point, without a mind which is focused, which has great concentration
that until through the practice of Vipassana, until concentration arises, Vipassana insight will not come.
So, I guess it's just that the question is a little bit, could be a little bit misleading,
I guess I understand.
I understand where you're coming from and I imagine that my answer is direct,
but really it is true that one must have a one-pointed mind before Vipassana insight arises.
One need not have a one-pointed mind before beginning to practice to see clearly.
Otherwise, you know, where would you start?
And I guess the idea then would be that you'd have to practice meditation based on a concept.
Meditation based on something that was not real before you could practice Vipassana, which certainly isn't the case.
But what happens in Vipassana meditation is that the concentration comes first and then the insight surely,
but the object is the same.
So, that's the first question.
The second question, are you going to your house and I'll have to the meditation?
I could.
I have to work on the chanting book next, but I can do that anytime.
Gotcha.
So, we can say that Vipassana helps to produce one-pointedness.
Again, we have to ask what you mean by Vipassana.
Do you mean the practice of meditation too see clearly or the experience of seeing clearly?
And I guess the answer is yes in both cases.
The meditation which we practice for the purpose of seeing clearly does bring a state of mind,
which is more focused on single objects.
But as I said, the mind is always one-pointed anyway.
But in classical Buddhist doctrine, including the Buddhist speech himself,
what do you mean by one-pointed is yes focused very strongly on a single object.
And yes, the practice does lead to that because that's the only way we could see things clearly.
The only way Vipassana can arise is through this single-pointed state of mind based on the phenomenon.
But that's a Vipassana meditation focusing on the rising and the falling brings one-pointedness based on the rising and based on the falling.
Once you gain insight, then of course your mind becomes even more one-pointed.
Through the practice of through the gaining of insight, the mind becomes one-pointed on Nirvana or Nirvana.
And that's the ultimate one-pointedness where the mind is no longer giving rise to any experience whatsoever,
where there is only the cessation of suffering.
And that's ultimate one-pointedness.
And so the answer is yes in both cases.
Though I'm not quite sure which one you're referring to.
You're welcome.
I guess it was one of the two.
Yes.
Thank you.
Oh, and one thing I meant to mention is that people who are interested in this sort of meditation are welcome to look further at some of the other things I've said
and some more techniques on how to practice this type of Vipassana meditation.
On my YouTube channel, youtube.com.com.
Thank you.
Thank you.
Okay, well thanks for coming everyone.
I hope it's been useful.
See you again next time.
