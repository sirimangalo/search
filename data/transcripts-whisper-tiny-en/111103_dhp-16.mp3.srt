1
00:00:00,000 --> 00:00:10,080
Okay, so today we continue our study of the Damabanda with verse number 16, which reads

2
00:00:10,080 --> 00:00:26,000
as far as, in Damodati, Pachamodati, Katapunyo Ubiata, Modati, so Modati, so Damodati, this wa-kamma

3
00:00:26,000 --> 00:00:41,000
Risudimatanoh, which means, here he rejoices, here after he rejoices, the Katapunyo,

4
00:00:41,000 --> 00:00:50,600
the person that doer of good deeds, rejoices in both places, he rejoices, he thoroughly rejoices,

5
00:00:50,600 --> 00:01:00,800
seeing the purity of his own actions, and this is the companion verse 2, our last

6
00:01:00,800 --> 00:01:08,300
verse 15, but the story is different, the story here is about a good doer, and so

7
00:01:08,300 --> 00:01:19,100
story of Damika Ubiata, who was Damika means one who lives by the Damma, Ubiata is a person

8
00:01:19,100 --> 00:01:28,300
who follows the teaching, who takes refuge, and Damika Ubiata was a follower of the Buddha,

9
00:01:28,300 --> 00:01:36,200
who was very much involved with the Buddha's teaching, he was very keen on it and interested

10
00:01:36,200 --> 00:01:40,700
in it, and so he would always go to listen to the Buddha's teaching, listen to the sermons

11
00:01:40,700 --> 00:01:47,700
again and again, invite monks to his house to give pre-chings, and he would be always giving

12
00:01:47,700 --> 00:01:52,800
arms to the monks, giving food to the monks when they came, and he was very much looking

13
00:01:52,800 --> 00:02:00,200
after them, so always doing good deeds, he was established in morality, and I believe he was

14
00:02:00,200 --> 00:02:07,200
also a meditator, he must have very well been because he was very much interested in the

15
00:02:07,200 --> 00:02:13,800
Buddha's teaching on Satyipatana, as the story goes, so as I understand he may have even

16
00:02:13,800 --> 00:02:17,600
been a sort upon that, but I'm not sure about that, I can't quite remember, anyway

17
00:02:17,600 --> 00:02:28,600
we can consider him to be a very good Buddhist in all senses of the word, and so it

18
00:02:28,600 --> 00:02:34,360
happened that he as well, after many years, he became ill and was on his deathbed, just

19
00:02:34,360 --> 00:02:41,040
like Judah, the poor butcher, but when he was on his deathbed, rather than becoming

20
00:02:41,040 --> 00:02:49,400
deranged or having his bad past deeds overwhelming, his good deeds came to him, and right

21
00:02:49,400 --> 00:02:55,280
away he thought to do more good deeds, so when he knew that he was probably at his last

22
00:02:55,280 --> 00:03:01,600
moment, he sent a message to the Buddha asking for monks to come, please send some monks,

23
00:03:01,600 --> 00:03:05,680
I would like to hear the dumb knife, figure the best way to go out is listening to the

24
00:03:05,680 --> 00:03:11,880
dumbman practicing according to the teaching, so the Buddha right away sent monks and

25
00:03:11,880 --> 00:03:17,880
they came and they sat around his bed, and they asked him, what teaching would you

26
00:03:17,880 --> 00:03:24,200
like to hear, and he said, please recite from me the Satyipatana, because it's the teaching

27
00:03:24,200 --> 00:03:29,960
on all the Buddha's, that's clear that he was very much clear, very much interested

28
00:03:29,960 --> 00:03:36,280
in meditation practice, Satyipatana, so it is what our meditation practice is based on

29
00:03:36,280 --> 00:03:41,880
and talks about the four foundations of mindfulness, the body, the feelings, the mind,

30
00:03:41,880 --> 00:03:49,120
and the number, so if you haven't read it, I think you probably all have read it, but

31
00:03:49,120 --> 00:03:54,960
if you haven't read it, just explain it, talks about the body, so how when you're walking,

32
00:03:54,960 --> 00:03:58,400
you know you're walking, when the breath comes in, you know the breath is coming in,

33
00:03:58,400 --> 00:04:03,440
for instance, rising and falling, being aware of the physical aspects of the body, the

34
00:04:03,440 --> 00:04:09,080
movements of the body, when the stomach moves, when the foot moves, when the hand moves,

35
00:04:09,080 --> 00:04:13,720
you know when you're going to the washroom, when you're eating, when you're doing whatever

36
00:04:13,720 --> 00:04:17,160
you're doing during the day, when the talks about being mindful of the body, knowing

37
00:04:17,160 --> 00:04:22,120
it for what it is, when you have feelings, you know the feelings for what it is, pain,

38
00:04:22,120 --> 00:04:28,200
or aching, or soreness, when you have thoughts, and when you have emotions, the dumb mother

39
00:04:28,200 --> 00:04:32,840
liking, just liking, so on, this is what it talks about, so they began to recite this,

40
00:04:32,840 --> 00:04:39,360
they started with the beginning, that says, aka yanoyang bikho yamamu, this is the only way

41
00:04:39,360 --> 00:04:46,120
among, so this is the one way amongst, for the purification of beings, for the overcoming

42
00:04:46,120 --> 00:04:52,160
of sorrow, lamentation, despair, for the destruction of bodily and mental suffering, for

43
00:04:52,160 --> 00:04:59,080
attaining the right path, and for realizing freedom, and so it's a very profound discourse,

44
00:04:59,080 --> 00:05:05,320
it's one that even today they will often recite for people who are dying, or recite

45
00:05:05,320 --> 00:05:10,960
just in general, in meditation centers, to remind them of teachings, so at this time

46
00:05:10,960 --> 00:05:16,160
he would be listening to me and also be practicing, now as he was on his way out, and because

47
00:05:16,160 --> 00:05:26,640
he was actually such a pure and wonderful person, the story goes, and this is where many

48
00:05:26,640 --> 00:05:31,160
people get turned off, because some of these stories are a little bit fantastical, so whether

49
00:05:31,160 --> 00:05:35,520
they're true or not, they'll leave it up to you to decide, with the point that the truth

50
00:05:35,520 --> 00:05:41,600
of this, the fantastical parts doesn't really affect the message at all, and it's our meditation

51
00:05:41,600 --> 00:05:48,520
meant that verifies or denies the message, which in this case is that a good person has

52
00:05:48,520 --> 00:05:55,240
good things happen to him, but it said that he had visions, suddenly had visions of angels

53
00:05:55,240 --> 00:06:04,080
coming from all of the six levels of essential heaven when they came in chariot, then they

54
00:06:04,080 --> 00:06:09,040
all began to call to him, please be born in my realm, be born in, because they all wanted

55
00:06:09,040 --> 00:06:18,480
this guy, I was kind of like a baseball draft or something, wanted this guy on their team,

56
00:06:18,480 --> 00:06:25,600
and so they were all extolling the virtues of their heaven, and then he should be born

57
00:06:25,600 --> 00:06:31,360
there, and nobody else could hear or see them, it was only a vision that he had, and

58
00:06:31,360 --> 00:06:40,400
so he said to them, wait, wait, and his children heard this wait, wait, and he's kind

59
00:06:40,400 --> 00:06:45,840
of distracted, look like he was talking to himself, that he was saying wait, wait, and

60
00:06:45,840 --> 00:06:49,880
they thought he must be telling the monks to stop the monks where they're chanting, and suddenly

61
00:06:49,880 --> 00:06:55,520
he interrupts them, and says wait, wait, and so the monks suddenly stop chanting, and they

62
00:06:55,520 --> 00:07:01,200
listen to me, okay, he wants us to wait, that's me, there's a good respect to this guy,

63
00:07:01,200 --> 00:07:06,560
and the children started crying, and they were horrified, because they had been taught by

64
00:07:06,560 --> 00:07:14,360
him to that, the importance of listening to the dhamma, and so they thought he must be

65
00:07:14,360 --> 00:07:20,080
totally deranged, or he must be experiencing something terrible, and be afraid, and so on,

66
00:07:20,080 --> 00:07:24,680
and he doesn't want the monks to teach the dhamma, and so they began crying, they said,

67
00:07:24,680 --> 00:07:31,600
and truly no one is immune to this, even our own father who we thought was so mindful,

68
00:07:31,600 --> 00:07:37,720
and so the monks saw them crying, and the old men saying wait, wait, and they said nothing

69
00:07:37,720 --> 00:07:44,640
for us to do, so they all left, and after they left, the old men said, where did the monks

70
00:07:44,640 --> 00:07:50,680
go, why did they stop chanting, and they said, they said, you told them to wait,

71
00:07:50,680 --> 00:07:57,920
and we were all horrified that we thought you were, we realized that you also are becoming

72
00:07:57,920 --> 00:08:02,440
a little bit deranged here, and he said, that's not why, I wasn't telling the monks

73
00:08:02,440 --> 00:08:06,840
to me, I was telling these angels here to wait, don't come from me yet, I'm still listening

74
00:08:06,840 --> 00:08:13,000
to the dhamma, and they said, what, chariots, we don't see them, and then the story goes

75
00:08:13,000 --> 00:08:18,680
that he said he had them take a wreath of flowers, and throw it up in the air, and it landed

76
00:08:18,680 --> 00:08:23,040
on one of the chariots, and it just hung there in the air, and he said, that's the heaven

77
00:08:23,040 --> 00:08:27,480
that I'm going to be born, and the two seat that happened, and then he passed away, and

78
00:08:27,480 --> 00:08:32,600
when he passed away he entered into the chariot and went to heaven, this is how the story

79
00:08:32,600 --> 00:08:38,560
goes. So you can imagine that it may have been fancified, it may not have been, maybe

80
00:08:38,560 --> 00:08:43,200
true that this kind of thing happens, I'm open minded about it, but it's not really

81
00:08:43,200 --> 00:08:50,680
important, but we can't understand, based on the last one is on two levels, a person who

82
00:08:50,680 --> 00:08:56,320
does good deeds, has good things happen to them, and a person who does bad things, has

83
00:08:56,320 --> 00:09:02,040
bad things happen, or becomes sufferers, sochity, and modity, sochity means sorrows and

84
00:09:02,040 --> 00:09:08,600
modity means rejoices, or feels great pleasure for them.

85
00:09:08,600 --> 00:09:11,840
So when the monks went back to the monastery, they said to the Buddha, they said, you

86
00:09:11,840 --> 00:09:16,400
know, we tried, but sad thing is, he got deranged at the last moment, and he actually

87
00:09:16,400 --> 00:09:21,320
told us to stop teaching the dhamma, and the Buddha said, that's not why he, the Buddha

88
00:09:21,320 --> 00:09:28,120
explained exactly what happened, and then he said, and then they said, wow, you mean,

89
00:09:28,120 --> 00:09:33,000
he was so happy here on earth, it's amazing, he was so happy here on earth, and now he's

90
00:09:33,000 --> 00:09:37,520
even happier up in heaven, how wonderful, and the Buddha said, this is the way it goes, a person

91
00:09:37,520 --> 00:09:43,440
who does good deeds, you know, he recited the verse.

92
00:09:43,440 --> 00:09:50,520
So this is another thing, it's also something that is often invisible to us, because

93
00:09:50,520 --> 00:09:54,520
we take it in a very shallow sense, so you'll often see people who have bad lives, who

94
00:09:54,520 --> 00:10:00,600
have bad things happening to them, because of the nature of their lives, and they want

95
00:10:00,600 --> 00:10:03,800
to take to do good deeds, thinking that it's going to get rid of all of their suffering,

96
00:10:03,800 --> 00:10:04,800
right?

97
00:10:04,800 --> 00:10:07,680
And they're disappointed that it doesn't, and they feel like they've been betrayed

98
00:10:07,680 --> 00:10:14,320
and so on, because they have this incredibly shallow understanding of how good deeds work.

99
00:10:14,320 --> 00:10:19,320
But you do have those people who understand how good deeds work, and who really feel

100
00:10:19,320 --> 00:10:26,440
the benefit of them, and understand how wonderful it is to, for instance, give a gift,

101
00:10:26,440 --> 00:10:34,520
I have a story of a woman I knew, a girl who were teenagers, and we were walking down the

102
00:10:34,520 --> 00:10:40,400
street in the middle of winter, and suddenly she walked inside me and said, suddenly, she

103
00:10:40,400 --> 00:10:45,000
turned and went into a McDonald's, and she said, wait here, and so I'm waiting outside

104
00:10:45,000 --> 00:10:51,200
and there's this beggar right in front of this guy on the living on a piece of cardboard

105
00:10:51,200 --> 00:10:52,760
outside the McDonald's.

106
00:10:52,760 --> 00:10:57,600
She comes out with a hamburger, and I know she doesn't eat meat, and she hands it to this,

107
00:10:57,600 --> 00:11:02,960
she bends down and hands it to this guy sitting on the cardboard and says, here you go,

108
00:11:02,960 --> 00:11:06,160
sometimes I mean I'm kind of feeling ashamed of myself, I didn't even see the guy until

109
00:11:06,160 --> 00:11:11,280
she went in, I didn't think anything other, but right away, and then we started walking

110
00:11:11,280 --> 00:11:18,560
and then right away she said to me, she said, I don't give to people, I don't give charity

111
00:11:18,560 --> 00:11:25,600
because I want them to feel happy, I do it because I'm selfish, I give gifts because it

112
00:11:25,600 --> 00:11:27,640
makes me feel happy.

113
00:11:27,640 --> 00:11:31,440
This is a person who isn't religious at all, and I don't know where she is now, but she's

114
00:11:31,440 --> 00:11:37,760
just a person who has really felt the goodness of giving and goodness of charity.

115
00:11:37,760 --> 00:11:43,000
And I think people who are engaged in giving do feel this, because it's a pleasure that

116
00:11:43,000 --> 00:11:49,640
is unadulterated, it's a pleasure that when you have this pleasure, there's nothing

117
00:11:49,640 --> 00:11:55,040
tainting it or contaminating it, and it's also something that lasts with you forever,

118
00:11:55,040 --> 00:11:59,640
it's something that's never going to be taken away, whereas pleasure based on sensuality

119
00:11:59,640 --> 00:12:03,240
is totally dependent on the object of the sense.

120
00:12:03,240 --> 00:12:08,120
When a person remembers about the good deeds that they have done, they can repeatedly gain

121
00:12:08,120 --> 00:12:13,320
this happiness and this peace.

122
00:12:13,320 --> 00:12:18,040
So it's something that people I think misunderstand and it's a part of how people misunderstand

123
00:12:18,040 --> 00:12:25,160
karma is being some magical thing that you can just give money in a box and magically

124
00:12:25,160 --> 00:12:31,640
you become rich or so on, or give gifts and suddenly you are happy.

125
00:12:31,640 --> 00:12:36,480
But people who do it continuously and who get engaged in it as a practice, giving or

126
00:12:36,480 --> 00:12:42,080
morality, morality is another one, people feel like morality is a waste of time or it's

127
00:12:42,080 --> 00:12:46,360
just a lot of suffering to not do things that you want to do, you have a mosquito why

128
00:12:46,360 --> 00:12:48,680
not just kill it and so on.

129
00:12:48,680 --> 00:12:53,000
The people who do it and most importantly, the people who engage in meditation are able to

130
00:12:53,000 --> 00:12:54,000
see the difference.

131
00:12:54,000 --> 00:12:58,480
And so this is what you should all be seeing right now, that the good and the bad things

132
00:12:58,480 --> 00:13:03,720
in your mind have really very much contributed to how you now relate to the present moment,

133
00:13:03,720 --> 00:13:06,440
how you react to things.

134
00:13:06,440 --> 00:13:10,760
And through the meditation you begin to see the importance of doing good deeds, the importance

135
00:13:10,760 --> 00:13:17,320
of being generous, the importance of being moral and the importance of practicing of developing

136
00:13:17,320 --> 00:13:22,440
your mind, of strengthening your mind so that you can see things as they are.

137
00:13:22,440 --> 00:13:27,400
So you can see the weaknesses in our mind, the greed, the anger, the delusion, how it's

138
00:13:27,400 --> 00:13:35,400
causing us great suffering, our stinginess, our jealousy and so on.

139
00:13:35,400 --> 00:13:42,240
So it's actually easier to see how this works in the present life, even though most people

140
00:13:42,240 --> 00:13:45,800
don't see it, but through a little bit of meditation you can see how it happens.

141
00:13:45,800 --> 00:13:50,160
Now we have to kind of, people say take a leap of faith to think about how it's going

142
00:13:50,160 --> 00:13:53,880
to happen in the next life, just like with the last one, is it really true that this

143
00:13:53,880 --> 00:13:57,600
man went to heaven and that man went to hell and so on.

144
00:13:57,600 --> 00:14:04,120
But it's ameliorated and it's actually really done away with this idea of taking a leap

145
00:14:04,120 --> 00:14:11,400
of faith in regards to what happens when we die by understanding, by really appreciating

146
00:14:11,400 --> 00:14:16,200
the experience that we're having in meditation and internalizing it and realizing that

147
00:14:16,200 --> 00:14:17,200
this is reality.

148
00:14:17,200 --> 00:14:22,400
When we look around us and we see there's this building, there's the trees and so on, we

149
00:14:22,400 --> 00:14:26,640
see other people, we're actually just building these constructs up in our mind.

150
00:14:26,640 --> 00:14:28,520
The truth is that there's experience.

151
00:14:28,520 --> 00:14:32,240
Now you're hearing, now you're seeing, now you're smelling, now you're tasting, now

152
00:14:32,240 --> 00:14:34,320
you're feeling, now you're thinking.

153
00:14:34,320 --> 00:14:38,000
So at the moment of death, the point is that this doesn't change.

154
00:14:38,000 --> 00:14:42,760
The theory is that that we subscribe to is that this doesn't change because there's

155
00:14:42,760 --> 00:14:51,200
no reason for it to change.

156
00:14:51,200 --> 00:14:50,680
The physical aspect of our reality is really just a part of experience, moment to moment

157
00:14:50,680 --> 00:14:55,000
to moment to moment experience, that continues on.

158
00:14:55,000 --> 00:14:59,160
It really helps to make sense of why the universe is the way it is.

159
00:14:59,160 --> 00:15:04,640
It's very much based on past causes and conditions and so the future as well will be based

160
00:15:04,640 --> 00:15:06,360
on the same causes and conditions.

161
00:15:06,360 --> 00:15:13,640
If our mind is impure, as the Buddha says, suffering follows us, if our mind is pure,

162
00:15:13,640 --> 00:15:15,480
then happiness follows us.

163
00:15:15,480 --> 00:15:21,400
So this is how we understand that a person does good deeds and benefits both in this life

164
00:15:21,400 --> 00:15:22,400
and in the next.

165
00:15:22,400 --> 00:15:25,600
And it's very much a part of our practice of meditation.

166
00:15:25,600 --> 00:15:31,400
We've practiced in meditation, we're purifying our minds, overcoming greed and desire to gain

167
00:15:31,400 --> 00:15:34,800
this and that because we're understanding that it's a cause for suffering.

168
00:15:34,800 --> 00:15:41,400
And we're purifying the word he uses is kamma we suiting up the more, we suiting in purity.

169
00:15:41,400 --> 00:15:43,200
So it's the purity of our acts.

170
00:15:43,200 --> 00:15:48,880
An act as pure or is defiled as, I think you all are aware, based on the intentions

171
00:15:48,880 --> 00:15:49,880
of the mind.

172
00:15:49,880 --> 00:15:54,840
So when you, even if you walk, you can walk with an impure mind, you can speak.

173
00:15:54,840 --> 00:15:59,000
When you speak, you can speak with a pure and impure mind.

174
00:15:59,000 --> 00:16:03,560
And obviously the practice of meditation is what's allowing us to see purity and impurity

175
00:16:03,560 --> 00:16:07,600
and to refine our behavior so that our reactions to things.

176
00:16:07,600 --> 00:16:11,560
This is really what you're doing, because again and again you're reacting to things and

177
00:16:11,560 --> 00:16:13,960
you'll see your reaction to things.

178
00:16:13,960 --> 00:16:18,280
And you'll slowly refine those reactions until it just becomes an interaction.

179
00:16:18,280 --> 00:16:21,000
You're not liking things or disliking things.

180
00:16:21,000 --> 00:16:23,000
You're just dealing with them.

181
00:16:23,000 --> 00:16:28,240
Okay, now I'm seeing now and hearing now I have pain, now I have pleasure and so on.

182
00:16:28,240 --> 00:16:30,200
And you're not clinging to anything.

183
00:16:30,200 --> 00:16:34,280
This is how the Buddha said in the Satipatana Sutta, Anisita and Julie had a deal.

184
00:16:34,280 --> 00:16:37,360
We're not clinging to anything.

185
00:16:37,360 --> 00:16:42,960
So there's another brief teaching, something more for us to keep in mind when we do our

186
00:16:42,960 --> 00:16:48,360
practice that this is really what we're doing is purifying our thoughts and our speech

187
00:16:48,360 --> 00:16:49,360
and our deeds.

188
00:16:49,360 --> 00:17:08,160
So thanks for listening and back to meditation.

