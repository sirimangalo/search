1
00:00:00,000 --> 00:00:13,040
How do I know I am being mindful enough and practicing properly?

2
00:00:13,040 --> 00:00:20,760
It's a difficult question to answer because you really have to understand where your

3
00:00:20,760 --> 00:00:24,000
question is coming from.

4
00:00:24,000 --> 00:00:28,480
I mean you, I mean when you ask this question, the important thing for the teacher to

5
00:00:28,480 --> 00:00:35,760
point out is what's giving rise to this question.

6
00:00:35,760 --> 00:00:39,840
And what's giving rise to it may be what's giving rise to it.

7
00:00:39,840 --> 00:00:45,960
I would venture a guess is some kind of doubt or anxiety about your practice, about who

8
00:00:45,960 --> 00:00:52,760
you are and about your status as a meditator and so on.

9
00:00:52,760 --> 00:00:59,640
It could come from doubts or uncertainty or even can come simply from curiosity.

10
00:00:59,640 --> 00:01:06,280
But all of these are mind states and all of those are what you should be focusing on rather

11
00:01:06,280 --> 00:01:08,640
than asking such questions.

12
00:01:08,640 --> 00:01:11,520
That's really avoiding the question, isn't it?

13
00:01:11,520 --> 00:01:15,280
I don't think it's a very good question to ask and I'm not criticizing you because it's

14
00:01:15,280 --> 00:01:26,400
a question that I get a lot but the way the way to answer it is to divert the focus back

15
00:01:26,400 --> 00:01:35,000
to where it should be and that is on the states of mind when they arise rather than worrying

16
00:01:35,000 --> 00:01:41,040
about how your practice is doing or how mindful you are.

17
00:01:41,040 --> 00:01:56,760
There's a general confusion that occurs in the meditator's mind because they are indoctrinated

18
00:01:56,760 --> 00:02:03,000
culturally into the idea that meditation is about concentration.

19
00:02:03,000 --> 00:02:09,040
So they use the word mindfulness but their brain is still thinking concentration.

20
00:02:09,040 --> 00:02:14,760
So they ask questions and this is a very common question, am I being mindful enough?

21
00:02:14,760 --> 00:02:26,880
You can't possibly qualify, there's no quality to mindfulness, you either are mindfulness

22
00:02:26,880 --> 00:02:34,640
or you are not and you can only be mindful one moment at a time.

23
00:02:34,640 --> 00:02:40,080
This is what is confirmed by our use of this word, the word is the recognition but that

24
00:02:40,080 --> 00:02:47,360
recognition occurs, the word mindfulness which really means to recognize and to fully grasp

25
00:02:47,360 --> 00:02:50,920
the object as it is.

26
00:02:50,920 --> 00:02:57,640
It occurs for one moment and if you're not practicing the next moment then that goes away.

27
00:02:57,640 --> 00:03:15,480
So if you're not quick and you're not catching the next one then you miss the whole point.

28
00:03:15,480 --> 00:03:27,160
So the answer here is to try and see a moment to moment.

29
00:03:27,160 --> 00:03:34,560
Don't worry about your quality of practice, don't worry about your quality of mind.

30
00:03:34,560 --> 00:03:39,800
People will always come to me, meditators will always come to me and say I'm not concentrated

31
00:03:39,800 --> 00:03:51,760
enough and I will remind them again and again and again that it's not really what we're

32
00:03:51,760 --> 00:03:58,600
looking for and I didn't ask how concentrated are you, how is your practice and then

33
00:03:58,600 --> 00:04:04,280
I was like oh I'm not very concentrated today, well I didn't ask that, I mean basically

34
00:04:04,280 --> 00:04:12,480
the meaning is that's not how I would judge your practice, by how concentrated you are.

35
00:04:12,480 --> 00:04:18,160
But when you're not concentrated as the Buddha said in the Satyipatanas, you should

36
00:04:18,160 --> 00:04:26,360
know when I'm unfocused, he knows I'm unfocused, when he's focused and he knows he's

37
00:04:26,360 --> 00:04:32,680
focused.

38
00:04:32,680 --> 00:04:42,040
I think that I'll help you and I'm happy not only to progress but to hopefully avoid

39
00:04:42,040 --> 00:04:50,680
what may be and I'm assuming it likely is doubts about yourself and your practice and

40
00:04:50,680 --> 00:05:06,280
worrying about the practice and so on, okay, glad that helped.

