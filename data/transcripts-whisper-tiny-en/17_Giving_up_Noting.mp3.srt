1
00:00:00,000 --> 00:00:03,360
Giving up noting.

2
00:00:03,360 --> 00:00:09,240
In the tradition I have been practicing, Bhante Gunna Ratana has told me not to note

3
00:00:09,240 --> 00:00:16,000
mental phenomena such as thinking, itching, et cetera, because it is likely you will become

4
00:00:16,000 --> 00:00:22,520
fixated on the words and fixated on the mental noting, but I have noticed you teach this

5
00:00:22,520 --> 00:00:23,680
practice.

6
00:00:23,680 --> 00:00:29,680
Would you eventually have people deviate from this practice to examine the mental phenomena

7
00:00:29,680 --> 00:00:32,240
and the sensation without notation?

8
00:00:32,240 --> 00:00:33,240
No.

9
00:00:33,240 --> 00:00:38,640
People often ask if there will be a point when I tell them to stop noting, and the answer

10
00:00:38,640 --> 00:00:39,640
is no.

11
00:00:39,640 --> 00:00:41,120
There is no such point.

12
00:00:41,120 --> 00:00:45,000
There are so many different opinions about noting meditation.

13
00:00:45,000 --> 00:00:48,720
Some people say noting is samata meditation.

14
00:00:48,720 --> 00:00:56,800
Not noting is vipassana.

15
00:00:56,800 --> 00:01:02,760
I have heard people say noting inhibits or prevents you from developing concentration.

16
00:01:02,760 --> 00:01:07,280
I have heard people say noting gives you too much concentration.

17
00:01:07,280 --> 00:01:12,240
Everyone has an opinion about it, and it is quite funny to listen to all these opinions.

18
00:01:12,240 --> 00:01:14,200
The Buddha's teaching is A.E.

19
00:01:14,200 --> 00:01:23,080
If it does cause you to fixate on the words, then you have come and sing, and you should

20
00:01:23,080 --> 00:01:25,400
go and see another teacher instead.

21
00:01:25,400 --> 00:01:28,960
I have never seen that for myself or with my students.

22
00:01:28,960 --> 00:01:34,600
If you are fixating on the words, then you are not practicing meditation, and you should

23
00:01:34,600 --> 00:01:36,520
acknowledge the fixating.

24
00:01:36,520 --> 00:01:39,800
If you are investigating, say investigating.

25
00:01:39,800 --> 00:01:45,280
In situations where you think you are fixating, or you feel you are fixating, it is more

26
00:01:45,280 --> 00:01:49,200
important to look at the feeling and the judgment of the practice.

27
00:01:49,200 --> 00:01:51,720
That is almost always the problem.

28
00:01:51,720 --> 00:01:57,280
It may be that your practice is fine, but then doubt arises because we are generally prone

29
00:01:57,280 --> 00:01:58,560
to doubt things.

30
00:01:58,560 --> 00:02:03,120
When the doubt comes up, acknowledge the doubting, and you will see that just like everything

31
00:02:03,120 --> 00:02:04,920
else, it disappears.

32
00:02:04,920 --> 00:02:09,480
How can you fixate on the words if you are watching the doubting, and you are saying to

33
00:02:09,480 --> 00:02:12,200
yourself, doubting, doubting.

34
00:02:12,200 --> 00:02:15,840
After noting, you will notice that the doubting has ceased.

35
00:02:15,840 --> 00:02:19,240
Isn't that impermanence what we are trying to see?

36
00:02:19,240 --> 00:02:24,280
If all you can see is words, you have a problem with your understanding of the practice.

37
00:02:24,280 --> 00:02:30,080
I remember when I first started, all I saw were words in my head as a very visual person.

38
00:02:30,080 --> 00:02:35,760
If I noted, seeing, seeing, there would be those words in my head, my initial practice

39
00:02:35,760 --> 00:02:37,240
was crazy practice.

40
00:02:37,240 --> 00:02:41,880
I heard them talking about the middle way, and so I tried to envision this line down

41
00:02:41,880 --> 00:02:45,040
the middle of my body, and I was trying to open this up.

42
00:02:45,040 --> 00:02:50,040
I saw this white line, and I was like trying to get into the middle way somehow.

43
00:02:50,040 --> 00:02:55,880
The teacher at the time said, practice is like a hammer, and then I got this big headache,

44
00:02:55,880 --> 00:03:01,000
and so I started to visualize my brain, and I was imagining hitting it with the hammer

45
00:03:01,000 --> 00:03:03,160
trying to break it open.

46
00:03:03,160 --> 00:03:06,600
Everything the teacher said to me, I was just visualizing it.

47
00:03:06,600 --> 00:03:10,640
It was pretty terrible, but I had nothing to do with the noting practice, and everything

48
00:03:10,640 --> 00:03:13,800
to do with my understanding or lack thereof.

49
00:03:13,800 --> 00:03:19,520
Once I started appreciating the noting as simply a reminder meant to prevent reactions,

50
00:03:19,520 --> 00:03:24,320
I was able to separate the craziness of my head from the practice that was leading me

51
00:03:24,320 --> 00:03:25,400
to tame it.

52
00:03:25,400 --> 00:03:30,280
If it were the case that noting made you fixated on the words themselves, then mantra

53
00:03:30,280 --> 00:03:35,480
meditation in general would be useless, and the word mantra would never have come into

54
00:03:35,480 --> 00:03:36,920
popular usage.

55
00:03:36,920 --> 00:03:43,520
In reality, however, a mantra does have power and does focus your mind on an object,

56
00:03:43,520 --> 00:03:45,240
whatever the object might be.

57
00:03:45,240 --> 00:03:50,080
If noting caused you to fix the same on the words, then the fisudimaga would be wrong

58
00:03:50,080 --> 00:03:53,680
when it describes it again and again, the use of these words.

59
00:03:53,680 --> 00:03:59,000
According to the fisudimaga, when you practice the earth kasina, you focus on a disc

60
00:03:59,000 --> 00:04:05,680
of earth and repeat one of the various names of earth like fat devi or rumi, repeating it a hundred

61
00:04:05,680 --> 00:04:08,200
times or a thousand times.

62
00:04:08,200 --> 00:04:12,920
The fisudimaga is absolutely clear about the use of a mantra.

63
00:04:12,920 --> 00:04:19,200
When you practice the white kasina, you repeat to yourself white, white, white.

64
00:04:19,200 --> 00:04:23,720
When you practice meta meditation, you repeat a mantra of kindness.

65
00:04:23,720 --> 00:04:29,720
When you practice the 32 constituent parts, you say the name of each part of the body

66
00:04:29,720 --> 00:04:35,240
in order, or you pick one like focusing on the hair and saying, hair, hair.

67
00:04:35,240 --> 00:04:40,200
This is accepted meditation practice since ancient times, and with good reason because

68
00:04:40,200 --> 00:04:43,440
mantras have great power and benefit to the mind.

69
00:04:43,440 --> 00:04:49,360
When the Buddha said, get chanto, bhagga, chami, di, bhagga, nati, which means when

70
00:04:49,360 --> 00:04:56,360
going, when knows I am going, it is not when knows the going, when knows fully and completely

71
00:04:56,360 --> 00:04:57,760
I am going.

72
00:04:57,760 --> 00:05:01,280
So there arises a clear thought about the experience.

73
00:05:01,280 --> 00:05:02,960
This is not something new.

74
00:05:02,960 --> 00:05:09,360
The Buddha taught such practice, and the fisudimaga describes explicitly that this is what

75
00:05:09,360 --> 00:05:11,160
is meant by meditating.

76
00:05:11,160 --> 00:05:16,640
Meditation means to focus one's attention on the object, just as it is without diversity

77
00:05:16,640 --> 00:05:22,320
of thought, to see white as white, to see blue as blue, to see the earth as earth, and so

78
00:05:22,320 --> 00:05:23,320
on.

79
00:05:23,320 --> 00:05:53,160
Nature is the tool used to see things this way.

