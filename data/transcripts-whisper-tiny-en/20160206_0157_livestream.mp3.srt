1
00:00:00,000 --> 00:00:26,000
So, good evening everyone, broadcasting live, sometime in February, February 5th, 2016.

2
00:00:26,000 --> 00:00:41,000
So, a couple of announcements, one announcement, that one big announcement.

3
00:00:41,000 --> 00:00:51,000
I'd say there is a fundraising campaign up, which is, well, I can't talk too much about it.

4
00:00:51,000 --> 00:01:01,000
I can't get involved with fundraising, except to say that, except to say what.

5
00:01:01,000 --> 00:01:06,000
That if we have the support, we'll keep doing this.

6
00:01:06,000 --> 00:01:15,000
If we don't have the support, well, I might have to head back to Stony Creek or Thailand,

7
00:01:15,000 --> 00:01:21,000
maybe I'll just go back to Sri Lanka, live in the cave somewhere.

8
00:01:21,000 --> 00:01:36,000
That was the other announcement, was we got some visitors, aliens, foreigners.

9
00:01:36,000 --> 00:01:46,000
And that's Patrick, and that's Thomas.

10
00:01:46,000 --> 00:01:50,000
They're in meditation, that's why they look so somber.

11
00:01:50,000 --> 00:01:54,000
This meditation makes you suffer.

12
00:01:54,000 --> 00:01:58,000
Don't they look like they're suffering?

13
00:01:58,000 --> 00:02:06,000
The meditation is a very terrible thing, brings out the worst in people.

14
00:02:06,000 --> 00:02:08,000
It actually does, you know.

15
00:02:08,000 --> 00:02:18,000
It really brings out the worst in people, brings it out, and throws it out, and gets rid of it.

16
00:02:18,000 --> 00:02:24,000
Apropos of the quote tonight.

17
00:02:24,000 --> 00:02:31,000
The quote is apropos, I don't know how that works.

18
00:02:31,000 --> 00:02:36,000
The night's quote is, it's a nice poly quote.

19
00:02:36,000 --> 00:02:38,000
It's very quotable.

20
00:02:38,000 --> 00:02:41,000
This is something you could quote the Buddha saying.

21
00:02:41,000 --> 00:02:42,000
Where is it?

22
00:02:42,000 --> 00:02:45,000
Let's find the poly.

23
00:02:45,000 --> 00:03:11,000
When the mind is defiled, beings are defiled, beings will be defiled.

24
00:03:11,000 --> 00:03:16,000
It is Sunday.

25
00:03:16,000 --> 00:03:21,000
I don't know.

26
00:03:21,000 --> 00:03:29,000
The mind is cleansed, sata, resulgenti, beings are purified.

27
00:03:29,000 --> 00:03:34,000
Purification is in the mind.

28
00:03:34,000 --> 00:03:44,000
It may seem kind of trite or obvious to the Buddhist, but there's a couple of important

29
00:03:44,000 --> 00:03:48,000
implications to this verse.

30
00:03:48,000 --> 00:04:03,000
The first is sort of a attack on the concepts of purity and other religions.

31
00:04:03,000 --> 00:04:11,000
In the time of the Buddha, they would, well, even today, they bathe in the Ganga River,

32
00:04:11,000 --> 00:04:18,000
meaning, yeah, thinking that it's going to purify them.

33
00:04:18,000 --> 00:04:21,000
That's the idea of purification in Hinduism.

34
00:04:21,000 --> 00:04:27,000
There was one idea, not the idea, but a lot of people think that, so they take tracks up to the mountains,

35
00:04:27,000 --> 00:04:39,000
the source of the Ganga Nadi, the river Ganges, as the Brits called it.

36
00:04:39,000 --> 00:04:42,000
Thinking that it would purify them.

37
00:04:42,000 --> 00:04:53,000
It was that way in the Buddhist time, and even today, there's this idea that it purifies.

38
00:04:53,000 --> 00:05:01,000
Again, other religions and many religions, there's the idea that purification comes from God.

39
00:05:01,000 --> 00:05:08,000
And purification comes through prayer.

40
00:05:08,000 --> 00:05:26,000
Purification comes through ritual, the idea that there are certain acts you have to perform to be

41
00:05:26,000 --> 00:05:40,000
kind.

42
00:05:40,000 --> 00:05:42,000
Many different kind of ways of purification.

43
00:05:42,000 --> 00:05:46,000
Some religions are religious movements, beliefs are torturing yourself.

44
00:05:46,000 --> 00:05:49,000
That's what leads to purity.

45
00:05:49,000 --> 00:05:51,000
All these different ideas.

46
00:05:51,000 --> 00:05:56,000
So this is a big thing that the Buddha was addressing, no, it's the mind.

47
00:05:56,000 --> 00:05:59,000
The mind just would purify his views.

48
00:05:59,000 --> 00:06:00,000
So that's the obvious one.

49
00:06:00,000 --> 00:06:02,000
That's what we often hear talked about.

50
00:06:02,000 --> 00:06:14,000
But I think a more important implication of this, what it's saying, is that being, the being,

51
00:06:14,000 --> 00:06:18,000
is dependent on the mind, not the other way around.

52
00:06:18,000 --> 00:06:22,000
The mind doesn't come from the being, the being comes from the mind.

53
00:06:22,000 --> 00:06:26,000
Mark that well, that's important.

54
00:06:26,000 --> 00:06:36,000
So ordinary thinking is who you are determines your mind's status.

55
00:06:36,000 --> 00:06:38,000
An angry person gets angry.

56
00:06:38,000 --> 00:06:42,000
Why? Because they're an angry person.

57
00:06:42,000 --> 00:06:47,000
It's sloppy thinking, but we fall into it quite easily.

58
00:06:47,000 --> 00:06:51,000
I'm an alcoholic.

59
00:06:51,000 --> 00:06:53,000
I'm an addict.

60
00:06:53,000 --> 00:06:55,000
I don't agree with it, really.

61
00:06:55,000 --> 00:06:57,000
I'm just off the top of my head.

62
00:06:57,000 --> 00:06:58,000
It sounds wrong.

63
00:06:58,000 --> 00:07:00,000
I know alcoholics anonymous is big in this.

64
00:07:00,000 --> 00:07:02,000
Identifying yourself is an alcoholic.

65
00:07:02,000 --> 00:07:04,000
Kind of understand the rationale behind it.

66
00:07:04,000 --> 00:07:07,000
But there's something there that's a little bit.

67
00:07:07,000 --> 00:07:08,000
I don't know.

68
00:07:08,000 --> 00:07:10,000
I don't want to go attacking.

69
00:07:10,000 --> 00:07:14,000
What appears to be a really good addiction counseling service,

70
00:07:14,000 --> 00:07:19,000
but not convinced that it's entirely perfect.

71
00:07:19,000 --> 00:07:25,000
I mean, hey, a bit meditation would be a really good addiction service.

72
00:07:25,000 --> 00:07:30,000
I can't say that.

73
00:07:30,000 --> 00:07:35,000
I don't know off the top of my head of any alcoholics who came and gave it up through meditation.

74
00:07:35,000 --> 00:07:40,000
I do remember one guy after he finished his course,

75
00:07:40,000 --> 00:07:47,000
wearing white clothes in Thailand, went down into the village in order to three beers.

76
00:07:47,000 --> 00:07:50,000
I mean, it seems kind of, you know, so what?

77
00:07:50,000 --> 00:07:53,000
But he just finished an entire meditation course,

78
00:07:53,000 --> 00:07:55,000
and he was still wearing his white clothes.

79
00:07:55,000 --> 00:07:58,000
And so there I am up on the mountain in the meditation center.

80
00:07:58,000 --> 00:08:06,000
And someone comes to me and says, one of your meditators snuck out to go get drunk.

81
00:08:06,000 --> 00:08:10,000
And he was still staying with us the thing.

82
00:08:10,000 --> 00:08:12,000
He had given up eight precepts, so he was on five precepts,

83
00:08:12,000 --> 00:08:18,000
and he didn't understand that five precepts is the point being you're staying with us,

84
00:08:18,000 --> 00:08:21,000
and oh boy, did I tear into him?

85
00:08:21,000 --> 00:08:25,000
And he noticed his response was, which is defense was.

86
00:08:25,000 --> 00:08:30,000
Well, I ordered six beers, but I only drank three of them.

87
00:08:30,000 --> 00:08:32,000
I really thought that was that,

88
00:08:32,000 --> 00:08:35,000
and he didn't realize that not drinking alcohol was part of the five precepts,

89
00:08:35,000 --> 00:08:37,000
and the three precepts, which is probably my fault,

90
00:08:37,000 --> 00:08:42,000
you know, show this lack of instruction.

91
00:08:42,000 --> 00:08:46,000
Anyway, yeah, I'm hoping that that's not a,

92
00:08:46,000 --> 00:08:50,000
I mean, it's kind of shameful to know that someone who finishes our course

93
00:08:50,000 --> 00:08:52,000
is still going to go out and drink.

94
00:08:52,000 --> 00:08:54,000
It shouldn't happen like that.

95
00:08:54,000 --> 00:09:01,000
But I mean, I would defend, the defense would be some people slip to the cracks.

96
00:09:01,000 --> 00:09:07,000
But I would, I would argue that probably a pretty, you know, why wouldn't it be?

97
00:09:07,000 --> 00:09:11,000
But anyway, totally off track there, apologize.

98
00:09:11,000 --> 00:09:21,000
The point I was trying to make was that our state of being who we are,

99
00:09:21,000 --> 00:09:25,000
quote unquote,

100
00:09:25,000 --> 00:09:29,000
is based simply into a solely on the habits that we form,

101
00:09:29,000 --> 00:09:34,000
not on some predetermined genetic, et cetera, et cetera,

102
00:09:34,000 --> 00:09:39,000
a state of being, you know, it's all habitual.

103
00:09:39,000 --> 00:09:42,000
It's all habits, it's artificial.

104
00:09:42,000 --> 00:09:45,000
So if you say you're an angry person,

105
00:09:45,000 --> 00:09:49,000
what that means is you've developed over time a habit to get angry.

106
00:09:49,000 --> 00:09:53,000
If you're an addiction person while you've cultivated addiction,

107
00:09:53,000 --> 00:10:03,000
and by that very nature, by that very fact,

108
00:10:03,000 --> 00:10:05,000
the habits can be unlearned.

109
00:10:05,000 --> 00:10:09,000
You can can head in the other direction.

110
00:10:09,000 --> 00:10:11,000
You could argue it gets more and more difficult,

111
00:10:11,000 --> 00:10:14,000
the more you get addicted,

112
00:10:14,000 --> 00:10:17,000
or more deeply ingrained the habit becomes,

113
00:10:17,000 --> 00:10:20,000
and I agree with that for sure.

114
00:10:20,000 --> 00:10:24,000
It doesn't mean it's who you are.

115
00:10:24,000 --> 00:10:26,000
The mind comes first.

116
00:10:26,000 --> 00:10:31,000
The mind proceeds all things.

117
00:10:31,000 --> 00:10:36,000
It's just a matter of what you're working for.

118
00:10:36,000 --> 00:10:40,000
So if you want to talk about why we are the way we are,

119
00:10:40,000 --> 00:10:42,000
we start with the mind.

120
00:10:42,000 --> 00:10:45,000
We start with the habits that we cultivate.

121
00:10:45,000 --> 00:10:49,000
And so meditation is just another habit.

122
00:10:49,000 --> 00:10:52,000
We're cultivating a wholesome habit,

123
00:10:52,000 --> 00:10:54,000
a habit of objectivity,

124
00:10:54,000 --> 00:10:56,000
a habit of mental clarity.

125
00:10:56,000 --> 00:10:58,000
Let's say you should look at it.

126
00:10:58,000 --> 00:10:59,000
It's not magic.

127
00:10:59,000 --> 00:11:01,000
It's not like you can count up how many hours you've done

128
00:11:01,000 --> 00:11:03,000
and somehow have that mean anything.

129
00:11:03,000 --> 00:11:05,000
It doesn't mean anything.

130
00:11:05,000 --> 00:11:08,000
All that means anything is how,

131
00:11:08,000 --> 00:11:16,000
and how often how frequently

132
00:11:16,000 --> 00:11:20,000
and how clearly you're able to set your mind.

133
00:11:20,000 --> 00:11:24,000
So every moment that your mind is clear,

134
00:11:24,000 --> 00:11:28,000
you're cultivating that habit of clarity.

135
00:11:28,000 --> 00:11:30,000
Every moment you're objective,

136
00:11:30,000 --> 00:11:32,000
you're cultivating objectivity.

137
00:11:32,000 --> 00:11:35,000
Every moment you're in reality,

138
00:11:35,000 --> 00:11:37,000
you're cultivating an awareness of reality.

139
00:11:37,000 --> 00:11:38,000
And that's a bit short.

140
00:11:38,000 --> 00:11:40,000
Over time,

141
00:11:40,000 --> 00:11:42,000
begins to erode other habits,

142
00:11:42,000 --> 00:11:46,000
begins to take their place,

143
00:11:46,000 --> 00:11:50,000
and the power of it leads to understanding.

144
00:11:50,000 --> 00:11:53,000
We start to see things clear,

145
00:11:53,000 --> 00:11:59,000
and that understanding works to erode

146
00:11:59,000 --> 00:12:03,000
to weaken bad habits

147
00:12:03,000 --> 00:12:05,000
that are based on misunderstanding.

148
00:12:05,000 --> 00:12:08,000
Because when you understand that something's wrong,

149
00:12:08,000 --> 00:12:13,000
when you used to think it was right,

150
00:12:13,000 --> 00:12:15,000
that changes everything.

151
00:12:15,000 --> 00:12:17,000
Many habits don't arise.

152
00:12:17,000 --> 00:12:19,000
When a person becomes a sort-up on it,

153
00:12:19,000 --> 00:12:23,000
there are certain habits that just get cut off from wisdom.

154
00:12:23,000 --> 00:12:24,000
But until that point,

155
00:12:24,000 --> 00:12:25,000
we're building habits.

156
00:12:25,000 --> 00:12:28,000
We're building a purity of mind

157
00:12:28,000 --> 00:12:31,000
to the extent that we can let go.

158
00:12:31,000 --> 00:12:35,000
So the extent that we can slip through the cracks,

159
00:12:35,000 --> 00:12:38,000
so not clinging on to everything,

160
00:12:38,000 --> 00:12:45,000
let go and out to Nirvana.

161
00:12:45,000 --> 00:12:48,000
Once you've seen Nirvana game change,

162
00:12:48,000 --> 00:12:51,000
at that point,

163
00:12:51,000 --> 00:12:54,000
habits disappear.

164
00:12:54,000 --> 00:13:03,000
The residual residue of the physical residue,

165
00:13:03,000 --> 00:13:05,000
and so on chemicals in the brain,

166
00:13:05,000 --> 00:13:07,000
are associated with anger or addiction,

167
00:13:07,000 --> 00:13:09,000
must it be there?

168
00:13:09,000 --> 00:13:14,000
But the mental aspect isn't there.

169
00:13:14,000 --> 00:13:18,000
So the second point is just that

170
00:13:18,000 --> 00:13:21,000
the being isn't the one that holds

171
00:13:21,000 --> 00:13:25,000
our defilements or our problems.

172
00:13:25,000 --> 00:13:27,000
It's the mind,

173
00:13:27,000 --> 00:13:28,000
and they're just mind-states.

174
00:13:28,000 --> 00:13:31,000
All problems, depression, anxiety.

175
00:13:31,000 --> 00:13:35,000
It can all be distilled down to mind-states.

176
00:13:35,000 --> 00:13:37,000
Even schizophrenia,

177
00:13:37,000 --> 00:13:40,000
bipolar, all these things can be,

178
00:13:40,000 --> 00:13:42,000
in the end, separated out into physical states

179
00:13:42,000 --> 00:13:45,000
and mental states that arise and cease.

180
00:13:45,000 --> 00:13:47,000
Often incessantly,

181
00:13:47,000 --> 00:13:51,000
but so people would argue that's not a habit.

182
00:13:51,000 --> 00:13:54,000
The only way you can say it's not a habit

183
00:13:54,000 --> 00:13:57,000
is if you follow a sort of modern thinking

184
00:13:57,000 --> 00:14:02,000
that life starts at conception,

185
00:14:02,000 --> 00:14:05,000
that birth creates mind.

186
00:14:05,000 --> 00:14:08,000
Not mind creates birth.

187
00:14:08,000 --> 00:14:11,000
So Buddhism doesn't think that.

188
00:14:11,000 --> 00:14:14,000
Buddhism claims that mind creates birth,

189
00:14:14,000 --> 00:14:17,000
not the other way around.

190
00:14:17,000 --> 00:14:19,000
And so something like,

191
00:14:19,000 --> 00:14:20,000
as I was thinking,

192
00:14:20,000 --> 00:14:22,000
just thinking I was just talking about last night,

193
00:14:22,000 --> 00:14:25,000
the mind creates schizophrenia to have it.

194
00:14:25,000 --> 00:14:28,000
So based on some bad habits,

195
00:14:28,000 --> 00:14:34,000
it's a mouth.

196
00:14:34,000 --> 00:14:36,000
Yeah.

197
00:14:36,000 --> 00:14:38,000
So that's all a little bit of,

198
00:14:38,000 --> 00:14:40,000
I wanted to get Robin on here,

199
00:14:40,000 --> 00:14:43,000
so she's on here now.

200
00:14:43,000 --> 00:14:52,000
Let's see if I can patch you into the audio.

201
00:14:52,000 --> 00:14:58,000
Let's see.

202
00:14:58,000 --> 00:15:08,000
Oh, so capture.

203
00:15:08,000 --> 00:15:11,000
Okay, Robin, are you there?

204
00:15:11,000 --> 00:15:13,000
I'm here, Soty Bante.

205
00:15:13,000 --> 00:15:15,000
Okay.

206
00:15:15,000 --> 00:15:18,000
So you are now on,

207
00:15:18,000 --> 00:15:20,000
you're responsible for this new campaign,

208
00:15:20,000 --> 00:15:22,000
there's new thing on you caring?

209
00:15:22,000 --> 00:15:23,000
Yes.

210
00:15:23,000 --> 00:15:26,000
Oh.

211
00:15:26,000 --> 00:15:29,000
I started working on the new campaign,

212
00:15:29,000 --> 00:15:31,000
but I didn't even really see it.

213
00:15:31,000 --> 00:15:33,000
Oh, oops.

214
00:15:33,000 --> 00:15:35,000
Well, I just released it for you.

215
00:15:35,000 --> 00:15:37,000
Oh, okay.

216
00:15:37,000 --> 00:15:38,000
It said it's live.

217
00:15:38,000 --> 00:15:41,000
I just got an email saying it's live.

218
00:15:41,000 --> 00:15:43,000
Oh, that's right.

219
00:15:43,000 --> 00:15:45,000
I guess that's why I wasn't informed about it

220
00:15:45,000 --> 00:15:47,000
because still in the works.

221
00:15:47,000 --> 00:15:52,000
Well, it has no pictures on it or anything yet.

222
00:15:52,000 --> 00:15:53,000
It doesn't have any pictures.

223
00:15:53,000 --> 00:15:54,000
I was going to say anything.

224
00:15:54,000 --> 00:15:57,000
I suggest them to put some pictures on it.

225
00:15:57,000 --> 00:16:00,000
It was really just in progress,

226
00:16:00,000 --> 00:16:02,000
completely in progress.

227
00:16:02,000 --> 00:16:07,000
Well, 218 people have seen it on Facebook.

228
00:16:07,000 --> 00:16:10,000
One person's already shared it.

229
00:16:10,000 --> 00:16:12,000
So it's going viral.

230
00:16:12,000 --> 00:16:15,000
Okay.

231
00:16:15,000 --> 00:16:18,000
Well, we'll get some pictures up.

232
00:16:18,000 --> 00:16:21,000
Maybe release it.

233
00:16:21,000 --> 00:16:23,000
Well, I can talk about it anyway.

234
00:16:23,000 --> 00:16:24,000
Yeah, go ahead.

235
00:16:24,000 --> 00:16:25,000
Sure.

236
00:16:25,000 --> 00:16:31,000
Sorry, I'm just really unprepared for this.

237
00:16:31,000 --> 00:16:36,000
You know, the monster has been up and running for six months now.

238
00:16:36,000 --> 00:16:38,000
And things are really going well.

239
00:16:38,000 --> 00:16:40,000
There's a lot of meditators coming.

240
00:16:40,000 --> 00:16:44,000
The meditator schedule is booked for a couple of months out.

241
00:16:44,000 --> 00:16:46,000
And the advances is great.

242
00:16:46,000 --> 00:16:48,000
There's a lot of things going on.

243
00:16:48,000 --> 00:16:53,000
And we're just, we're looking to gauge support for whether this

244
00:16:53,000 --> 00:16:58,000
monastery is able to continue on well past, you know, well past

245
00:16:58,000 --> 00:17:02,000
when our lease ends, which is in August.

246
00:17:02,000 --> 00:17:05,000
So we're just looking to the gauge support for that.

247
00:17:05,000 --> 00:17:08,000
Hopefully, you know, people are interested in supporting the

248
00:17:08,000 --> 00:17:10,000
monastery in the meditation center.

249
00:17:10,000 --> 00:17:15,000
Well, beyond what we already have the funds for, which is,

250
00:17:15,000 --> 00:17:18,000
actually, this was another thing I needed to check on.

251
00:17:18,000 --> 00:17:20,000
But it's somewhere between June and August.

252
00:17:20,000 --> 00:17:24,000
So just looking to gauge support beyond that.

253
00:17:24,000 --> 00:17:29,000
Okay.

254
00:17:29,000 --> 00:17:31,000
Thank you for that.

255
00:17:31,000 --> 00:17:35,000
Thank you, Dante.

256
00:17:35,000 --> 00:17:39,000
And with that, I think we'll end for the night.

257
00:17:39,000 --> 00:17:42,000
So thank you all for tuning in.

258
00:17:42,000 --> 00:17:45,000
Have a good night.

259
00:17:45,000 --> 00:17:47,000
Thank you, Dante.

260
00:17:53,000 --> 00:17:54,000
Oops.

261
00:17:54,000 --> 00:18:18,000
Really, it wasn't quite ready yet.

