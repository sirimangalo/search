1
00:00:00,000 --> 00:00:02,300
Okay, welcome back to Ask a Monk.

2
00:00:02,300 --> 00:00:10,620
Next question is from 1, morea, 1, 9, 9, 1, 0, 2, 0, 9.

3
00:00:10,620 --> 00:00:16,860
Can you talk of concepts of destiny, fate, and time?

4
00:00:16,860 --> 00:00:20,460
Is time linear and how does the concept of time pertain to rebirth?

5
00:00:20,460 --> 00:00:21,860
Does time loop on itself?

6
00:00:21,860 --> 00:00:23,960
Do we live in all realities simultaneously?

7
00:00:23,960 --> 00:00:26,100
Can we remember past lives?

8
00:00:26,100 --> 00:00:29,940
Are we predisposed to live certain experiences?

9
00:00:29,940 --> 00:00:40,120
The question of time has come up a few times, and I've declined to answer it.

10
00:00:40,120 --> 00:00:46,380
I don't think time has a lot to do with Buddhist teachings, but I did talk about time

11
00:00:46,380 --> 00:00:54,920
quite a bit, but basically to say, forget about the past, or don't dwell on the past,

12
00:00:54,920 --> 00:01:01,280
don't live in the future, try to see what's in the present moment clearly.

13
00:01:01,280 --> 00:01:09,440
My understanding is that time is here and now, that's what's real, and that's all that's

14
00:01:09,440 --> 00:01:11,080
real.

15
00:01:11,080 --> 00:01:15,720
Time is not linear in the sense of the past and the future existing.

16
00:01:15,720 --> 00:01:20,680
There's only the present moment, and the truth about time is that it's the present moment

17
00:01:20,680 --> 00:01:21,680
changing.

18
00:01:21,680 --> 00:01:29,520
The reality changing time is a concept used to make measurements and predictions about

19
00:01:29,520 --> 00:01:37,880
these changes, but that's all it is, it's a tool.

20
00:01:37,880 --> 00:01:42,840
The truth is that existence is only here and now.

21
00:01:42,840 --> 00:01:51,280
Things like destiny and fate, well, as I understand from a Buddhist point of view and

22
00:01:51,280 --> 00:01:59,440
for what I've been trying to read about quantum physics and how it relates to our understanding

23
00:01:59,440 --> 00:02:05,280
of reality, there are two parts to reality.

24
00:02:05,280 --> 00:02:10,960
There's the physical world around us and then there's the mental world inside of us, and

25
00:02:10,960 --> 00:02:18,120
the physical world is in many ways deterministic, so you're not going to be able to easily

26
00:02:18,120 --> 00:02:24,280
escape some of your future, there are many things that you're stuck with, like in this

27
00:02:24,280 --> 00:02:27,360
life, I'm not going to be a basketball player.

28
00:02:27,360 --> 00:02:34,840
For example, there are many things that were stuck in based on our fate and our destiny

29
00:02:34,840 --> 00:02:38,040
because of the deterministic nature of the physical realm.

30
00:02:38,040 --> 00:02:43,640
Now the mental realm is not deterministic in this way, though it is dependent and it's

31
00:02:43,640 --> 00:02:54,080
reacting in regards to the physical realm that they're hitting back and forth.

32
00:02:54,080 --> 00:03:01,400
The mental realm is this choice that we have at every moment and this choice is dictated

33
00:03:01,400 --> 00:03:09,760
by our physical nature sometimes, but includes the ability to make decisions.

34
00:03:09,760 --> 00:03:18,000
So we have the ability to decide to change, to work in a certain direction.

35
00:03:18,000 --> 00:03:29,920
We can say that we're going to go against the way things appear to us.

36
00:03:29,920 --> 00:03:37,400
We can train our minds so that we understand something in a contrary way to before.

37
00:03:37,400 --> 00:03:47,120
There were experiments that were done about this where they train people to change their

38
00:03:47,120 --> 00:03:51,360
perceptions of things and we can see that this is possible with effort from a mental

39
00:03:51,360 --> 00:03:54,480
point of view, you can change your future.

40
00:03:54,480 --> 00:03:58,920
We can see this in meditation, how we can change the way our minds work, where we were

41
00:03:58,920 --> 00:04:04,720
headed in one direction once we start to meditate, we change our minds and go in another

42
00:04:04,720 --> 00:04:05,720
direction.

43
00:04:05,720 --> 00:04:23,520
I wouldn't say that fate and destiny are an ultimate truth, there is inertia of both physical

44
00:04:23,520 --> 00:04:29,000
and mental realities that propels us on in certain directions, so even in the mind we

45
00:04:29,000 --> 00:04:35,080
might have built up much anger, much aversion to something and that even with meditation

46
00:04:35,080 --> 00:04:37,720
can be quite difficult to change.

47
00:04:37,720 --> 00:04:48,680
But we have this ability to decide at every moment how we're going to adjust and if we adjust

48
00:04:48,680 --> 00:04:53,200
ourselves enough in one direction we can change who we are.

49
00:04:53,200 --> 00:04:59,200
That's the other part of your question, can we remember rebirth apparently we can?

50
00:04:59,200 --> 00:05:04,960
Or even there is even the ability to predict the future, people who practice meditation

51
00:05:04,960 --> 00:05:11,440
can see things that haven't happened yet and this is verifiable in meditation.

52
00:05:11,440 --> 00:05:17,040
So yeah, I think I've answered most of your questions.

53
00:05:17,040 --> 00:05:30,200
Same time as I said is just here and now but as far as the future and the past go, there

54
00:05:30,200 --> 00:05:33,000
is quite a difference obviously between the past and the future, I don't think I'm

55
00:05:33,000 --> 00:05:39,800
saying anything new by expressing the Buddhist understanding that the past is set, the past

56
00:05:39,800 --> 00:05:44,680
and the past is one realm, the future is another realm, the present is another realm.

57
00:05:44,680 --> 00:05:50,800
The past is the nature of the past is things that are set, you can't change the past and

58
00:05:50,800 --> 00:05:56,640
this is an important reality for us to realize that no matter how much we wish things

59
00:05:56,640 --> 00:06:01,280
had been different, wish things were different now, wish that we had done things differently

60
00:06:01,280 --> 00:06:07,080
and that they hadn't happened this way or that way, it's past and it's set, that's

61
00:06:07,080 --> 00:06:08,360
the nature of the past.

62
00:06:08,360 --> 00:06:13,600
The nature of the future is uncertain, unpredictable, you can see certain things into the

63
00:06:13,600 --> 00:06:21,360
near future but unless you develop your mind to an extreme degree, it gets quite hazy

64
00:06:21,360 --> 00:06:31,080
very quickly and so it's also not worth making plans without having this kind of supernatural

65
00:06:31,080 --> 00:06:35,160
ability to predict what's going to happen.

66
00:06:35,160 --> 00:06:40,680
The best thing is to come to understand the present moment because the nature of the present

67
00:06:40,680 --> 00:06:46,680
moment is to be dynamic, is to be the time when change occurs, the time when changes

68
00:06:46,680 --> 00:06:52,560
can be enacted here and now you can change your life, you can't go from who you are now

69
00:06:52,560 --> 00:06:57,280
to being immediately someone else but you can turn in one direction or the other, you

70
00:06:57,280 --> 00:07:04,040
can at every moment affect a change in your direction which can eventually change who

71
00:07:04,040 --> 00:07:07,680
you are quite dramatically.

72
00:07:07,680 --> 00:07:15,440
So that's about all I'd like to say about those things, time and defeat and so on.

73
00:07:15,440 --> 00:07:19,400
As far as time looping on itself, I don't think I answered that and I don't think I

74
00:07:19,400 --> 00:07:46,480
quite understand it but I would say no probably not, okay, thanks for the questions.

