Hi, and welcome back to our study of the Dhamapada.
Today we continue on with verse number 83, which reads as follows.
The Dhamapada way, Sapurisa, Jajanti, Sapurisa, good people, are always renowned, are everywhere.
In every instance, on every occasion, renouncing, giving up, giving up.
Nacama kama la playantisanto, they don't mutter or blather or chatter, as though they were
nacama kamasanto, as though they were enamored by sensuality, in love with love or lusting after lust,
lusting after lustful things, desires of the desire and diasirables.
Sukayana puta at dawadukayna, when touched by either happiness or suffering, no jawajana
bandita dasayanti, they don't exhibit the wise, don't exhibit highs or lows.
So ordinary people when touched by Sukha or dukha, happiness or suffering, they are affected,
they are elated, they rejoice about their good fortune, they become somehow dependent
on it, they step into it and they come to expect it, they went touched by suffering,
and then they lament and bemoaned their state. But asapurisaya's ever chaganti is ever
renouncing, letting go. So that's the verse, in regards to a story about, it says 500
banks and 500 beggars. So in the time of the Buddha, right after the Buddha was recently
enlightened, he was staying in Wairanja, he lived there for the rains, he had been invited
to stay in Wairanja, if you read the beginning of the vinaya, day in Akopanai, Pandasama yi
in the Buddha, Bhakava, Wairanja, and we had a tea, the Buddha was dwelling in Wairanja.
And this Brahman invited him to stay there and then forgot about him. And so the Buddha
and his 500 monks dwelt in great hardship and they only were able to survive because
of some horse traders who gave them horse food, gave them oats, basically, or something
like oats, some kind of grain and they were able to live off this grain. But it was great
hardship, so they went on arms but I guess didn't get much. And this Brahman who invited
them to this area where they weren't able to get arms, totally forgot about them until
the end of the rains. But it says here that there were a bunch of beggars living with them.
By the kindness of the monks, 500 eaters of refuse lived within the monastery enclosure.
Or it doesn't even say that, but what it does say is that the point is these beggars
were before, so they weren't living. They weren't living with the Buddha and Wairanja.
But in Wairanja the monks, these 500 monks were all at least so dependent, maybe all
are hands. And they were all living with the Buddha and they were content with their
poor fare. They were at peace, even in great hardship. Later on they came to great wealth
and prosperity, not wealth in terms of money, but they were very well cared for. They
moved to Sawati and they lived there. But they were still the same, they were still
at peace. So whether they lived in hardship or
whether they lived in great comfort, they were the same. And then you had these 500 beggars
who appeared after there was great wealth and because of their own hardship decided
that they would hang out at the monastery and so on, through the kindness of the monks,
these 500 beggars sort of lived off the leftover food because everyone was always bringing
food to the monks and coming to hear the monks at teach and supporting the monks in
different ways. And so there was lots of leftovers and so these beggars would come.
And it says they would eat the choice food left over by the monks and then lie down
and sleep. And then when they got up from their sleep, they would be full of energy and
they would go and wrestle. They would shout and jump and wrestle and play, misbehaved both
within and without the monastery. They did nothing but misbehaved. And so the monks
were discussing this and they noted the difference because ostensibly a part of the
problem for these beggars was the opulence. Once they got great food, they got strength
and energy and they just started acting, rumunctious. Whereas the monks, whether they were
in hardship or in opulence or affluence, were unchanged. And so for that reason, he
told a jotic historian about horses, which is actually quite interesting because it points
out that even, it's a story about some donkeys and horses. And these thoroughbred horses
were fed, it was an accident or something, they were fed wine of some sort or some kind
of alcohol and were unaffected by it. But these donkeys who were hanging out with the
thoroughbred horses drank the same wine and started praying and carrying on the point
being that the alcohol, even alcohol, even though we rail against it as being a terrible
terrible thing, all it has the power to do is remove your inhibitions. So if you don't
have any harmful intentions in your mind, it doesn't actually hurt you. Unfortunately,
that for most of us, that includes delusion, it includes ego and so on. So if you have
none of that and you're fine, but anyway, he makes the point that this is a difference
between people, therefore an ordinary person. It's actually an interesting point because
for ordinary people, it's sometimes the other way around. Sometimes when things are fine,
people can be quite moral and ethical, right? When you don't need to steal, when you don't
need to kill, when there's no adversity, when there's no danger. But when the going gets
tough, many people will abandon their ethics, abandon their goodness, their generosity,
or rather people that stay the way around in hardship, they are forced to behave themselves.
They have to act in such a way that other people respect them and so on. But when things
are good, they get lazy, right? Or for monastics, this is very much the case. When things
are rough, you know, you're forced to be mindful because there's lots of physical pain
and physical discomfort and hunger and thirst and heat and cold that you can escape. But
when you have the pleasure and the luxury that it's very easy to get lazy, it's very easy
to become indulgent and so on. It's actually quite an impressive thing, impressive feat
that these monks having been in such hardship, going to such opulence and not being affected
by it. But the point still stands that it works both ways and it seems more to me about
change than anything. That after a while you become sort of stable in your situation and
if things don't change, you become, you may become complacent thinking that everything
is okay, only when things change. You see what you couldn't see because of the stability
and the part of it points out the nature of impermanence or the importance of impermanence,
the harm and the danger of change to one psyche. And also the usefulness of impermanence
for that very reason when things change, you're able to better see your defilement when
they have the rug pulled out from under you. When everything's going stable, whether
it's difficult, if it's difficult, you plot along, if it's good, then you feel calm
and at peace. But when things change, you can be quite upset. The person who is living
in opulence, if they fall into hardship, would have a very hard time coping mentally
with it. So it goes both ways and this is continuing this theme in this verse of the, we've
seen this recently quite a bit. We talk about how wise people touched by their happiness
or unhappiness, happiness or suffering show no change.
So the experience, no highs or lows. Which is interesting, people often think of that
as being a terribly dull and an interesting state. Certainly not something to be striped
for, the highs and the lows or the spice of life they would say. It gives life meaning,
I guess, it gives meaning to your happiness. But it's actually quite the opposite. We find
people who are stuck unhappiness and suffering that these are the ones who are like zombies.
They can be very depressed and colorless in their constant aversion to suffering or
in their constant obsession with happiness. A zombie is someone who is constantly seeking
out pleasure. It's by seeking out pleasure that you become this zombie that consumes
and consumes. Or you're a zombie working, working just to survive in great hardship. Just
to cope with suffering. The person who has had great loss becomes a zombie. A person who
exhibits neither highs or lows is actually quite a piece, has quite a calm and uplifted
mind, a light mind, a mind that is flexible, a mind that is kind and compassionate because
they don't have, they aren't caught up in their own emotions. So they're able to approach
life with great zest and in finger and clarity and goodness as well. So their intentions
to help others and that kind of thing are pronounced. Their happiness and their peace are
much pronounced. So they're anything but a zombie. The zombie is the one who has caught
up in legs and dislikes. They become tired and they get into this trance-like state, zombie-like
state of consuming pleasure and coping with suffering. It's a warning for all of us not
to become complacent. It's easy to think that you're moral and ethical. If you don't look
closely, it's easy to think, well I'm not a bad person, I'm living fine. But so much of
that is often dependent on our pleasure. When you start to meditate, you start to see,
actually I'm quite intoxicated by this pleasure. And many of the things that I thought were
harmless are actually building me up to take a fall because when I'm not able to indulge
in central pleasures anymore, I'm going to get angry and upset and frustrated. It's what
leads us to fight and quarrel and manipulate others are indulgence and sensuality.
It's really the crux of it. Our reactions to pleasant experiences and our reactions to
unpleasant experiences is about sums up all of the problems that we have or the problems of the
mind that we're working to overcome. We have these two. There's a third one that's not really
explicitly mentioned here and that's a delusion. We consider these to be the three evils
in the mind, the three things that lead us to suffer. But they're not equal. Happiness and
unhappiness lead to greed and anger, liking and disliking. Our reactions to these things,
these are, you could say, the surface problem, but the underlying problem. The third one is the
underlying problem. Our delusion is how we approach these things. The reason we get angry about
unpleasant things, the reason we become attached to pleasant things, is because of our delusion,
because of our ignorance, our perception, that this one's going to make me happy. This one's
something that I, the way to be happy is to avoid it. If I chase after this one, I'll be happy,
if I push this one away, I'll also be happy. Not realizing that more we push this one away,
more unhappy will become. The more we cling to this one, the more needy we will become and so
the more unhappy we will become. And shameful we will become. If you ask yourself, who would you
rather have as a friend, who would you rather associate with? Someone who wants a lot, or someone
who doesn't, who has great wanting, or someone who has little wanting, who has great desires,
strong desires, or someone who has simple desires, or is free from desire. I don't know, some
people might say they like people who have great desires, but they think about it when people
have great desires, then they end up wanting a lot from you. Friends who have great desires can
be great burdens. Friends who are hard to please, or friends who have great people who have great
aversion to suffering. So this is the sort of situation we have here where part of the implication
here is that these guys are acting improper or acting in an unsuitable way of having
eaten as this food that was meant for the monks and living in close quarters with these spiritual
people. They're acting like well like ordinary individuals. You get that a lot in monasteries when
they get big in Thailand. You can see this a lot where the workers and the monasteries
are not so very moral. Someone, I don't know, I was talking, talking to one of the workers about
cockroaches and she, I think she pulled out a can of cockroach spray and was trying to hand it
to me. And yeah, I remember going, we went on this parade once. The monks, we were up in this boat,
they had us up on this float with a boat. It was like a boat and people were putting food in the
boat. It was the means of supporting, I don't know actually what it was, what the food was going
to be for, but it was a parade with monks, which was kind of nice. But then the board of directors
for the monastery was surrounding it and going on with us and then they started pulling, they
pulled out alcohol and started drinking whiskey. Well, you know, all around us, they were holding
onto the edges of the boat and riding the float. It's a shame really, that that sort of thing
that living so close to such goodness and such spiritual teachings that they don't bother to put
them into practice. And part of it is this problem of having things too good when things are good,
people may tend to misbehave. It's part of the reason why monks are encouraged to go off
into the forest and live in a simplest, simple accommodations to live more or less in hardship
because it does, it can keep you honest. It forces you to be mindful.
In terms of the monastic life, hardship is quite often important for spiritual development
because it's very easy to get lazy as we can see here. Anyway, so a simple teaching,
the verse itself has some interesting points. The idea that what makes a good person is their
practice of renunciation everywhere, whether they're happy or unhappy. And they never blather on
lupianity. It's an interesting word. They've chatter. The implication is that people who are
intoxicated with sensuality will blather on about it. Something we should catch ourselves
in talking about food or clothing or music or art or this kind of thing.
But one who is wise, when touched by other happiness or by sorrow, is that peace,
is content. Their happiness, their peace, their contentment is not dependent.
Anamisasoka is a happiness that is not based on an object.
Doesn't require this or that. It isn't affected. It isn't disturbed.
It isn't disturbed and it isn't dependent on external sense and external
experiences or phenomena. This is why meditation is so important. It's important to understand
change and to become familiar with change and to see that change is a part of life so that
changes don't affect us, so that our happiness can be our peace of mind. It can be undisturbed by
change. Anyway, that's the teaching of this demo by the verse. That's all for now. Thank you for
tuning in. Keep practicing and be...
