1
00:00:00,000 --> 00:00:16,000
conditional relations, delivered by Willaro, Ocela Nanda, at the Tagata Meditation Center, April 25, 1997.

2
00:00:16,000 --> 00:00:36,000
Today, we come to the third variety, the third aspect of law of cause and effect.

3
00:00:36,000 --> 00:00:43,000
Now, there are three aspects of law of cause and effect.

4
00:00:43,000 --> 00:00:52,000
So, the first two we have already studied and they are the law of gamma and its results

5
00:00:52,000 --> 00:00:57,000
and the law of dependent origination.

6
00:00:57,000 --> 00:01:17,000
Today, we come to the law of Patana, conditional relations.

7
00:01:17,000 --> 00:01:26,920
Law of Patana, conditional relations is like the law of gamma and the law of

8
00:01:26,920 --> 00:01:30,920
dependent origination in natural law.

9
00:01:30,920 --> 00:01:38,920
So, it was not created by anybody or not created by the Buddha,

10
00:01:38,920 --> 00:01:47,920
but this law was hidden from beings when there were no Buddhas in the world.

11
00:01:47,920 --> 00:02:04,920
And then, a Buddha appears in the world and discovers this law and then he teaches,

12
00:02:04,920 --> 00:02:11,920
or he reveals this law to the world.

13
00:02:11,920 --> 00:02:18,920
Now, there is a few intertext about the law of dependent origination.

14
00:02:18,920 --> 00:02:24,920
And that is, whether the Buddhas arise or they do not arise,

15
00:02:24,920 --> 00:02:33,920
yet there remains that elements, that relatedness of stage,

16
00:02:33,920 --> 00:02:37,920
or regularity of stage, specific conditionality.

17
00:02:37,920 --> 00:02:41,920
That means, the law of dependent origination.

18
00:02:41,920 --> 00:02:44,920
So, the Buddha discovers it, penetrators to it,

19
00:02:44,920 --> 00:02:48,920
and then having discovered it and penetrated to it,

20
00:02:48,920 --> 00:02:52,920
he announces, he teaches and so on to the world.

21
00:02:52,920 --> 00:02:58,920
So, like the dependent, the law of dependent origination,

22
00:02:58,920 --> 00:03:12,920
the law of Patana is also a natural law discovered by the Buddha and revealed to the world.

23
00:03:12,920 --> 00:03:18,920
The law of Patana is more comprehensive than the other two,

24
00:03:18,920 --> 00:03:22,920
the law of gamma and the law of dependent origination,

25
00:03:22,920 --> 00:03:33,920
because the law of gamma explains only because the reality regarding living being,

26
00:03:33,920 --> 00:03:38,920
and the law of dependent origination also deals with living beings,

27
00:03:38,920 --> 00:03:48,920
and not with outside things, not with non-living things.

28
00:03:48,920 --> 00:03:56,920
But Patana covers all both living beings and non-living things.

29
00:03:56,920 --> 00:04:10,920
So, Patana can explain how one material property is related to another material property outside living beings.

30
00:04:10,920 --> 00:04:19,920
And like the law of gamma and Patana, dependent origination,

31
00:04:19,920 --> 00:04:31,920
it explains also how the Nama and Ruba, Mylan, Medha are related in living beings.

32
00:04:31,920 --> 00:04:40,920
But only is it comprehensive with regard to subject Medha,

33
00:04:40,920 --> 00:04:48,920
but it is comprehensive also in the method of treatment,

34
00:04:48,920 --> 00:05:10,920
because in the law of dependent origination, we know only that some phenomenon is conditioned by another phenomenon.

35
00:05:10,920 --> 00:05:23,920
But we are not given to know or it is not explained that how these two things are related,

36
00:05:23,920 --> 00:05:26,920
in what way they are related.

37
00:05:26,920 --> 00:05:37,920
But in Patana, the way they are related is also explained.

38
00:05:37,920 --> 00:05:49,920
So, I always compare this to saying dependent origination,

39
00:05:49,920 --> 00:05:53,920
saying these two persons are related.

40
00:05:53,920 --> 00:06:02,920
But Patana, according to Patana, we would say these two people are related as a father and son,

41
00:06:02,920 --> 00:06:07,920
or as brothers or as friends.

42
00:06:07,920 --> 00:06:14,920
So, in Patana, that is called, in Pauli, it is called Patayasati,

43
00:06:14,920 --> 00:06:21,920
the force of the conditionality, and that means just how these are related.

44
00:06:21,920 --> 00:06:32,920
So, in Patana, not only is it explained that two things are related,

45
00:06:32,920 --> 00:06:38,920
but it is explained that they are related in such a way.

46
00:06:38,920 --> 00:06:43,920
So, it is more comprehensive in the method of treatment also,

47
00:06:43,920 --> 00:06:51,920
than the other two varieties of law of course, and effect.

48
00:06:51,920 --> 00:07:03,920
And the law of Patana is the most profound of the teachings in Abitama.

49
00:07:03,920 --> 00:07:14,920
And it is also the most voluminous, or the biggest of the books of Abitama.

50
00:07:14,920 --> 00:07:18,920
Now, as you know, there are seven books in Abitama.

51
00:07:18,920 --> 00:07:30,920
And in the sixth council Buddhist council edition, there are 12 books, 12 volumes,

52
00:07:30,920 --> 00:07:42,920
and five big volumes belong to Patana, why seven smaller volumes belong to other six books.

53
00:07:42,920 --> 00:07:52,920
So, Patana is, by volume, the biggest of the teachings in Abitama,

54
00:07:52,920 --> 00:08:09,920
and also it is the most difficult, the most profound of the Abitama teachings.

55
00:08:09,920 --> 00:08:25,920
So, four weeks after his enlightenment, Buddha contemplated on the teachings of Abitama.

56
00:08:25,920 --> 00:08:34,920
Now, you know that Buddha spent eight weeks after his enlightenment,

57
00:08:34,920 --> 00:08:39,920
under the body tree, and near the body tree.

58
00:08:39,920 --> 00:08:45,920
He did not preach anything during these eight weeks.

59
00:08:45,920 --> 00:08:52,920
But during the third week, he contemplated on the teachings of Abitama.

60
00:08:52,920 --> 00:09:00,920
Now, there are several books in Abitama, and so he went through these books one after another.

61
00:09:00,920 --> 00:09:10,920
When he was contemplating on the first six books, and nothing special,

62
00:09:10,920 --> 00:09:14,920
or nothing unusual happened.

63
00:09:14,920 --> 00:09:28,920
But when he got into the seventh book that is got into the law of Patana,

64
00:09:28,920 --> 00:09:39,920
he was of six colors, came out of his body, and was spread to the end of the world.

65
00:09:39,920 --> 00:09:52,920
But that was because when he was contemplating the first six books of Abitama,

66
00:09:52,920 --> 00:10:01,920
his supreme wisdom, his omniscience, did not get sufficient room to move about.

67
00:10:01,920 --> 00:10:09,920
Now, for us, the first six books are also profound and difficult and deep.

68
00:10:09,920 --> 00:10:14,920
But for the Buddha, for the mind of the Buddha, they are not so deep.

69
00:10:14,920 --> 00:10:23,920
And so his supreme wisdom did not get enough room to move about.

70
00:10:23,920 --> 00:10:29,920
But when he got into the seventh book in Patana,

71
00:10:29,920 --> 00:10:37,920
Patana was deep and Patana was comprehensive.

72
00:10:37,920 --> 00:10:43,920
So his supreme wisdom got sufficient room to move about.

73
00:10:43,920 --> 00:10:53,920
So when he was able to contemplate as much as he liked,

74
00:10:53,920 --> 00:10:58,920
on Patana, he became very happy.

75
00:10:58,920 --> 00:11:03,920
So when he became happy, when his mind became happy,

76
00:11:03,920 --> 00:11:17,920
then the physical body, which is the beast of the mind also becomes clear.

77
00:11:17,920 --> 00:11:23,920
And so as a result, the rays came out of his body,

78
00:11:23,920 --> 00:11:28,920
and they were spread to the end of the world.

79
00:11:28,920 --> 00:11:35,920
So when Buddha was contemplating on the first six books,

80
00:11:35,920 --> 00:11:42,920
his mind was like a fool, a big fool, put into a small tank.

81
00:11:42,920 --> 00:11:53,920
So the fool was in the tank, but there is not enough room for him to play or to swim.

82
00:11:53,920 --> 00:12:00,920
But when he got into the seventh book, it was like the fool released into the ocean.

83
00:12:00,920 --> 00:12:06,920
So when the whole gets into the ocean, it can go anywhere, it lies.

84
00:12:06,920 --> 00:12:10,920
And so it becomes happy.

85
00:12:10,920 --> 00:12:18,920
So in the same room, when he got into contemplating the seventh book of the Buddha,

86
00:12:18,920 --> 00:12:25,920
he's supreme wisdom, got enough room to move about.

87
00:12:25,920 --> 00:12:38,920
And so he was happy, and the six rays of six colors came out of his body.

88
00:12:38,920 --> 00:12:47,920
So now you know that Patana explains the conditionality

89
00:12:47,920 --> 00:13:02,920
of mental and physical phenomena more comprehensively than the law of dependent origination.

90
00:13:02,920 --> 00:13:09,920
In Patana, there are taught twenty-four kinds of conditions.

91
00:13:09,920 --> 00:13:17,920
Or twenty-four modes of conditionality.

92
00:13:17,920 --> 00:13:25,920
So since this is just an introductory introduction and fundamentals,

93
00:13:25,920 --> 00:13:29,920
I will not go through them all one by one.

94
00:13:29,920 --> 00:13:38,920
If you are interested, then you can read some books, but it may not be so easy

95
00:13:38,920 --> 00:13:45,920
for an uninitiated in a bit of what you understand.

96
00:13:45,920 --> 00:13:54,920
So there are twenty-four kinds of conditional relations.

97
00:13:54,920 --> 00:14:03,920
Now we will divide these into some categories.

98
00:14:03,920 --> 00:14:18,920
So in this teaching, there is one phenomenon which conditions.

99
00:14:18,920 --> 00:14:24,920
And there is another phenomenon which is conditioned.

100
00:14:24,920 --> 00:14:32,920
So the first one we may call cause, and the second one we may call effect.

101
00:14:32,920 --> 00:14:38,920
But sometimes the word cause means something that produces some other thing.

102
00:14:38,920 --> 00:14:47,920
And so I want to avoid these words cause an effect when I explain any Patana.

103
00:14:47,920 --> 00:14:56,920
So I will use conditionally, factor, and condition factor.

104
00:14:56,920 --> 00:15:04,920
But which conditions that which have some other thing to arise?

105
00:15:04,920 --> 00:15:07,920
And then that which is conditioned.

106
00:15:07,920 --> 00:15:11,920
So these two.

107
00:15:11,920 --> 00:15:20,920
And in Patana, it is explained that these two are related.

108
00:15:20,920 --> 00:15:32,920
And they are related in such a way, in some way, like they are related as root,

109
00:15:32,920 --> 00:15:41,920
related by root cause, or related by way of object and so on.

110
00:15:41,920 --> 00:15:54,920
The first category is the condition factor producing conditions factor.

111
00:15:54,920 --> 00:16:04,920
Now this is a real relationship of real cause and effect.

112
00:16:04,920 --> 00:16:07,920
And in this case, Kama is an example.

113
00:16:07,920 --> 00:16:09,920
The law of Kama.

114
00:16:09,920 --> 00:16:24,920
Now suppose you do a Kusala Kama, when will you get the result of that Kusala Kama?

115
00:16:24,920 --> 00:16:35,920
You will get the result of Kusala Kama in this life, or in the next life, or in the life after next.

116
00:16:35,920 --> 00:16:44,920
So Kama is the conditioning factor, or Kama is the cause, and the results are the effect.

117
00:16:44,920 --> 00:16:57,920
Kama belongs to one time, and the results belong to a different time, in this life, or in future lives.

118
00:16:57,920 --> 00:17:07,920
So Kama and its results are related by way of Kama condition.

119
00:17:07,920 --> 00:17:24,920
Now here, the relationship is between the producer and the produced.

120
00:17:24,920 --> 00:17:37,920
The second category is that of conditioning factor arising prior to the conditioned factor, and then they become related.

121
00:17:37,920 --> 00:17:45,920
So here, first, the conditioning factor arises, and then it exists for some time.

122
00:17:45,920 --> 00:17:49,920
And then the conditioned factor arises.

123
00:17:49,920 --> 00:17:57,920
And so it is related to the conditioning factor, or the conditioned factor is related to the conditioned factor.

124
00:17:57,920 --> 00:18:07,920
By way of what is called pu-ri-jada, it is by way of three nations.

125
00:18:07,920 --> 00:18:16,920
Now, let us say, you see something.

126
00:18:16,920 --> 00:18:25,920
So there is a visible object, and then the seeing consciousness.

127
00:18:25,920 --> 00:18:40,920
So the visible object arises, at least three moments before the seeing consciousness arises.

128
00:18:40,920 --> 00:18:47,920
It is very, very detailed in a bit of a.

129
00:18:47,920 --> 00:18:51,920
So when you see something, seeing consciousness arises.

130
00:18:51,920 --> 00:18:57,920
And before that seeing consciousness arises, that something arose.

131
00:18:57,920 --> 00:19:09,920
So the object which is seen arises, about three moments before the seeing consciousness arises.

132
00:19:09,920 --> 00:19:26,920
So when seeing consciousness arises, then the seeing consciousness is related to the visible object by way of pre-masons.

133
00:19:26,920 --> 00:19:38,920
So in that case, the conditioning factor does not produce the conditioned factor, but it supports.

134
00:19:38,920 --> 00:19:47,920
It helps the conditioned factor, to arise and to exist for some time.

135
00:19:47,920 --> 00:19:52,920
You see something.

136
00:19:52,920 --> 00:19:55,920
There arises seeing consciousness.

137
00:19:55,920 --> 00:20:06,920
And that seeing consciousness is said to depend on the visible object.

138
00:20:06,920 --> 00:20:15,920
But seeing consciousness depends on another thing.

139
00:20:15,920 --> 00:20:18,920
You know what?

140
00:20:18,920 --> 00:20:20,920
The eye, right?

141
00:20:20,920 --> 00:20:23,920
If you have no eyes, you do not see.

142
00:20:23,920 --> 00:20:36,920
So the eye is also a condition for the seeing consciousness to arise.

143
00:20:36,920 --> 00:20:48,920
Now the eye has been existing for a long time before the seeing consciousness arises.

144
00:20:48,920 --> 00:21:02,920
So in this case also, the relationship between the seeing consciousness and the eye, the physical eye, is by way of pre-masons.

145
00:21:02,920 --> 00:21:12,920
That means the eye existed, I mean the eye arises before the seeing consciousness.

146
00:21:12,920 --> 00:21:17,920
And when seeing consciousness arises, the eye is still existing.

147
00:21:17,920 --> 00:21:29,920
And so the relationship is said to be by way of pre-masons.

148
00:21:29,920 --> 00:21:43,920
If you understand this kind of relationship between seeing consciousness and visible object, you understand the relationship between hearing consciousness and sound and so on.

149
00:21:43,920 --> 00:21:54,920
So when you hear a sound, the moment you hear the sound, the sound has already existed three short moments.

150
00:21:54,920 --> 00:22:06,920
So you may think that you hear the sound as soon as it arises, but actually three moments have passed when you really hear the sound.

151
00:22:06,920 --> 00:22:15,920
So the sound is related to the hearing consciousness by way of pre-masons.

152
00:22:15,920 --> 00:22:28,920
And also the seeing, I mean hearing consciousness, relationship between hearing consciousness and the eye.

153
00:22:28,920 --> 00:22:42,920
The category is that of conditions factor arising prior to the conditioning factor.

154
00:22:42,920 --> 00:22:45,920
It is strange.

155
00:22:45,920 --> 00:23:07,920
Now in this relationship, the conditions factor arises before the conditioning factor arises.

156
00:23:07,920 --> 00:23:17,920
When the relationship occurs, it is at the moment when these two exist.

157
00:23:17,920 --> 00:23:38,920
Not that the conditions factor arises prior to the conditioning factor is related before it arises.

158
00:23:38,920 --> 00:23:49,920
The relationship is only when it arises and then it exists for some time.

159
00:23:49,920 --> 00:24:10,920
Now when you have the expression of your face is one thing and when you are set, it changes and you have another expression.

160
00:24:10,920 --> 00:24:23,920
And that is actually conditioned by the mind which arises later.

161
00:24:23,920 --> 00:24:32,920
So the material properties in this phase has been existing for some time.

162
00:24:32,920 --> 00:24:43,920
And then your mind arises and then your mind conditions this phase is becoming a happy or happy phase or set phase.

163
00:24:43,920 --> 00:25:01,920
So in this case, the conditioning, the conditions factor arises prior to the conditioning.

164
00:25:01,920 --> 00:25:12,920
Here, the mind is that we conditioning and the material properties are said to be conditioned.

165
00:25:12,920 --> 00:25:20,920
Not that they are conditioned before the mind arises, but when it arises, it conditions the existing material properties.

166
00:25:20,920 --> 00:25:33,920
So this is the relationship of post-masons.

167
00:25:33,920 --> 00:25:38,920
Sometimes the relationship is between those that arise at the same time.

168
00:25:38,920 --> 00:25:57,920
So in this relationship, both the conditioning factor and conditions factor arise simultaneously and one is said to be conditioning and the other conditions.

169
00:25:57,920 --> 00:26:14,920
That is, conditioning at the same time, suppose you have low buffer for something.

170
00:26:14,920 --> 00:26:17,920
You have desire for something.

171
00:26:17,920 --> 00:26:28,920
So along with desire, there is what is called consciousness and also other mental factors.

172
00:26:28,920 --> 00:26:39,920
Desire is one mental factor and along with desire, there are other mental factors and along with these mental factors, there is consciousness.

173
00:26:39,920 --> 00:26:50,920
And also, mind can produce material properties.

174
00:26:50,920 --> 00:26:58,920
So at that moment, some material properties are also produced.

175
00:26:58,920 --> 00:27:16,920
So in this moment of desire, desire is something that is desire, other mental factors, consciousness and then material properties caused by mind.

176
00:27:16,920 --> 00:27:34,920
So here, desire is related to the others by way of coonsons, by way of arising at the same time.

177
00:27:34,920 --> 00:27:44,920
So they arise at the same time, but one is said to be conditioning and the others are conditioned.

178
00:27:44,920 --> 00:27:56,920
To be a relationship is by way of coonsons arising at the same time.

179
00:27:56,920 --> 00:28:05,920
The same with when some type of consciousness arises and then mental factors and material properties.

180
00:28:05,920 --> 00:28:16,920
Now when Jana arises, there is what is called Jana, that means a group of mental factors.

181
00:28:16,920 --> 00:28:28,920
And then Jana consciousness and then other mental factors and then material properties caused by Jana consciousness.

182
00:28:28,920 --> 00:28:45,920
In that case also, that the Jana is said to be conditioning factor and the other associated phenomena are conditioned factors.

183
00:28:45,920 --> 00:28:58,920
So they are related. I mean, the Jana's are related to other mental factors, consciousness and material properties by way of coonsonsons.

184
00:28:58,920 --> 00:29:10,920
So in this kind of relationship, both conditioning factor and conditions factor must be existing at that moment.

185
00:29:10,920 --> 00:29:16,920
If one is not existing, there can be no such relationship.

186
00:29:16,920 --> 00:29:24,920
And also at the moment of enlightenment, they arises what is called path consciousness.

187
00:29:24,920 --> 00:29:34,920
And along with path consciousness, there are eight factors of path

188
00:29:34,920 --> 00:29:44,920
and also other mental factors and material properties caused by path consciousness.

189
00:29:44,920 --> 00:29:53,920
In that case, to the eight path factors are conditioning factors and the others are conditioned.

190
00:29:53,920 --> 00:30:01,920
And they are related by way of arising at the same time or conditions.

191
00:30:01,920 --> 00:30:19,920
The next category is that of again, conditioning factor and conditions factor arising together and the condition each other.

192
00:30:19,920 --> 00:30:22,920
So it is called reciprocal conditioning.

193
00:30:22,920 --> 00:30:32,920
They arise together and then they support each other.

194
00:30:32,920 --> 00:30:39,920
So again, when desire arises, there are mental factors, there is consciousness.

195
00:30:39,920 --> 00:30:45,920
And they are said to be conditioned by each other.

196
00:30:45,920 --> 00:30:55,920
So when you take one as a conditioning, the others are conditioned and when you take another as conditioning, then the remaining are conditioned.

197
00:30:55,920 --> 00:31:09,920
So there can be reciprocal conditioning, between consciousness and mental factors.

198
00:31:09,920 --> 00:31:21,920
So in the fourth category of relationship, one is always the conditioning.

199
00:31:21,920 --> 00:31:27,920
One is always dependent upon and the others always depend upon that.

200
00:31:27,920 --> 00:31:35,920
But in the fifth kind of relationship, they are reciprocal.

201
00:31:35,920 --> 00:31:44,920
And so everyone can be the conditioning and also conditioned.

202
00:31:44,920 --> 00:31:54,920
And the reciprocal conditioning is compared to three sticks standing when they are put together a tripod.

203
00:31:54,920 --> 00:32:04,920
So they come together and they support each other and they stand.

204
00:32:04,920 --> 00:32:07,920
Oh, six categories.

205
00:32:07,920 --> 00:32:13,920
There is a area in numbers.

206
00:32:13,920 --> 00:32:22,920
Now here, conditioning factor is the object of the conditioned factor.

207
00:32:22,920 --> 00:32:36,920
So when we see something and the thing we see is called an object and we are the consciousness that sees is called subject.

208
00:32:36,920 --> 00:32:41,920
So here the relationship is as object and subject.

209
00:32:41,920 --> 00:32:54,920
So when we see something, the object is related to our seeing consciousness by way of object.

210
00:32:54,920 --> 00:33:09,920
And when we hear something the same, the sound is related to hearing consciousness by way of object relationship.

211
00:33:09,920 --> 00:33:18,920
Seven one is conditioning by disappearing.

212
00:33:18,920 --> 00:33:24,920
Something disappears so that some other thing can take its place.

213
00:33:24,920 --> 00:33:29,920
And that is also called conditioning.

214
00:33:29,920 --> 00:33:38,920
So in order for another man to sit here at this place, I forget this place.

215
00:33:38,920 --> 00:33:44,920
So my regression of this place is a condition for that must be in here.

216
00:33:44,920 --> 00:33:52,920
So such a given place is also called a condition.

217
00:33:52,920 --> 00:34:01,920
So when something disappears to some other thing, that first thing disappears.

218
00:34:01,920 --> 00:34:05,920
That first thing is that there are no more.

219
00:34:05,920 --> 00:34:22,920
So it is by way of immediate your proximity or by way of not absence.

220
00:34:22,920 --> 00:34:33,920
When this given place is of the same kind of phenomenon, it is called repetition.

221
00:34:33,920 --> 00:34:48,920
So one type of consciousness arises and then it disappears and then the another type of consciousness of the same kind arises.

222
00:34:48,920 --> 00:34:54,920
And then it disappears and then another kind of the same kind of consciousness.

223
00:34:54,920 --> 00:35:04,920
So in that case it is called repetition condition.

224
00:35:04,920 --> 00:35:09,920
It is proximity or disappearing condition and it is also repetition condition.

225
00:35:09,920 --> 00:35:21,920
Now if you know a bit of you and you know that when you experience Kusala, wholesome consciousness

226
00:35:21,920 --> 00:35:35,920
the repeats of seven times, the wholesome consciousness arises and then disappears and then arises and disappears.

227
00:35:35,920 --> 00:35:39,920
So it repeats seven times.

228
00:35:39,920 --> 00:35:58,920
So when it repeats seven times then it is said to be the first one is said to be related to the second one by way of repetition also.

229
00:35:58,920 --> 00:36:07,920
And the second one is related to the third one by way of repetition also and so on.

230
00:36:07,920 --> 00:36:11,920
Now let us go to the first category.

231
00:36:11,920 --> 00:36:19,920
So in the first conditioning factor produces the conditioned factor.

232
00:36:19,920 --> 00:36:24,920
So that is the real cause effect relationship.

233
00:36:24,920 --> 00:36:34,920
But the others are not producing and produced some arise before, some arise after,

234
00:36:34,920 --> 00:36:38,920
some arise at the same time and then they support each other.

235
00:36:38,920 --> 00:36:46,920
So here conditioning means just supporting as a dependence.

236
00:36:46,920 --> 00:36:55,920
And then in the last it is just recruiting its place.

237
00:36:55,920 --> 00:37:05,920
So just by disappearing, a sudden phenomenon gives chance to another phenomenon to arise.

238
00:37:05,920 --> 00:37:15,920
So in pattern, different kinds of, or different types of relationships are taught.

239
00:37:15,920 --> 00:37:31,920
And so who did knowledge of pattern, we can understand the law of cause and effect more, more fully and also more correctly.

240
00:37:31,920 --> 00:37:48,920
The last one, to understand this is very important, the relationship of giving place or disappearing.

241
00:37:48,920 --> 00:38:02,920
Now many others wrote that debt consciousness, conditions and rebirth consciousness.

242
00:38:02,920 --> 00:38:06,920
Or the rebirth consciousness is conditioned by debt consciousness.

243
00:38:06,920 --> 00:38:14,920
Is that right or wrong?

244
00:38:14,920 --> 00:38:23,920
Debt consciousness, conditions, rebirth consciousness.

245
00:38:23,920 --> 00:38:29,920
We must be careful.

246
00:38:29,920 --> 00:38:35,920
Here conditions mean just for creating its place, just disappearing.

247
00:38:35,920 --> 00:38:44,920
Not causing the rebirth consciousness to arise.

248
00:38:44,920 --> 00:39:02,920
So when we say rebirth consciousness is conditioned by debt consciousness, we just mean that debt consciousness disappears.

249
00:39:02,920 --> 00:39:07,920
And in each place, rebirth consciousness arises.

250
00:39:07,920 --> 00:39:14,920
But rebirth consciousness is not caused by, or not produced by debt consciousness.

251
00:39:14,920 --> 00:39:24,920
Because rebirth consciousness is a result in consciousness, then it is the result of some coma in the past.

252
00:39:24,920 --> 00:39:34,920
So here the relationship is just for creating its place or disappearing, not causing, not producing.

253
00:39:34,920 --> 00:39:40,920
So if we take that to main causal of producing, we will be wrong.

254
00:39:40,920 --> 00:39:51,920
So it is very important that you understand these different kinds of relations and then understand the statements correctly,

255
00:39:51,920 --> 00:39:58,920
which reference to these relationships.

256
00:39:58,920 --> 00:40:03,920
Okay, what does a law of pattern that teach us?

257
00:40:03,920 --> 00:40:15,920
Now it teaches us that all beings and all inanimate things in the world arise dependents are born condition

258
00:40:15,920 --> 00:40:19,920
and not without conditions.

259
00:40:19,920 --> 00:40:28,920
So whatever arises mind or matter, there must be a condition for its arising.

260
00:40:28,920 --> 00:40:43,920
So in the teachings of the Buddha, there is nothing which arises out of nothing, which arises without conditions that is in the world.

261
00:40:43,920 --> 00:40:50,920
But there is one thing taught by the Buddha which arises, which has no conditions.

262
00:40:50,920 --> 00:40:52,920
And that is nibhana.

263
00:40:52,920 --> 00:40:56,920
No, nibhana is said to be unconditioned.

264
00:40:56,920 --> 00:41:06,920
And there are always these two things, just be conditioning, factor and condition factor.

265
00:41:06,920 --> 00:41:14,920
Sometimes they belong to different times and sometimes they belong to at the same time.

266
00:41:14,920 --> 00:41:20,920
But there are only these two, the conditioning factor and condition factor.

267
00:41:20,920 --> 00:41:30,920
And apart from these two, there is no ata or no agent or no god or no Brahma to create them.

268
00:41:30,920 --> 00:41:46,920
So the Buddhist teaching of the law of course in effect is that there is always a condition for something in the world to arise,

269
00:41:46,920 --> 00:41:50,920
whether living being or non-living being.

270
00:41:50,920 --> 00:41:56,920
So there is always a condition or cause for something to arise.

271
00:41:56,920 --> 00:42:01,920
And so nothing arises without cause.

272
00:42:01,920 --> 00:42:12,920
And that cause or condition is not the imaginary ata soul or god or Brahma or whatever.

273
00:42:12,920 --> 00:42:22,920
But the conditions that are mine and matter.

274
00:42:22,920 --> 00:42:33,920
So the Buddhism access that all things in the world are conditioned.

275
00:42:33,920 --> 00:42:47,920
And that condition is not the imaginary agent or ata soul or ata or god or Brahma.

276
00:42:47,920 --> 00:43:03,920
And so these conditioning and condition factors arise and disappear, arise and disappear, arise and disappear.

277
00:43:03,920 --> 00:43:13,920
Or by their own accord and there is no way of control over them.

278
00:43:13,920 --> 00:43:20,920
And so in the end, what battle of teachers is that there is no atma.

279
00:43:20,920 --> 00:43:25,920
They just arise and disappear according to the conditions.

280
00:43:25,920 --> 00:43:28,920
And when there are no conditions, nothing arises.

281
00:43:28,920 --> 00:43:32,920
So you see me here because I am here.

282
00:43:32,920 --> 00:43:34,920
If I am not here, you do not see.

283
00:43:34,920 --> 00:43:38,920
So your seeing consciousness is conditioned by me.

284
00:43:38,920 --> 00:43:45,920
I am not created by some Brahma or some agent.

285
00:43:45,920 --> 00:43:52,920
So because there is a sound, I am making the sound, you hear the sound, you hear my voice.

286
00:43:52,920 --> 00:43:58,920
And so you are hearing consciousness is conditioned by my voice.

287
00:43:58,920 --> 00:44:03,920
Now I stop talking and then you don't hear any sound.

288
00:44:03,920 --> 00:44:06,920
So everything is conditioned.

289
00:44:06,920 --> 00:44:12,920
And when there are conditions, can there be, I think, there are conditions.

290
00:44:12,920 --> 00:44:21,920
And so this is, there is always this relationship between conditions, teams and conditioning

291
00:44:21,920 --> 00:44:24,920
things in different modes.

292
00:44:24,920 --> 00:44:32,920
And according to Patana, there are 24 modes of relationships.

293
00:44:32,920 --> 00:44:51,920
Now you understand that it is important to understand different kinds of relationships as conditioning

294
00:44:51,920 --> 00:44:54,920
and conditions.

295
00:44:54,920 --> 00:45:06,920
And only when you understand the relationship according to the Patana conditions, do you understand

296
00:45:06,920 --> 00:45:16,920
the law of dependent origination correctly and fully?

297
00:45:16,920 --> 00:45:22,920
So it is important that when you study the dependent origination,

298
00:45:22,920 --> 00:45:30,920
law of dependent origination, you should not forget about the law of Patana.

299
00:45:30,920 --> 00:45:38,920
You always have to apply the law of Patana to the law of dependent origination.

300
00:45:38,920 --> 00:45:41,920
So these two cannot be separated actually.

301
00:45:41,920 --> 00:45:52,920
They should be understood together.

302
00:45:52,920 --> 00:46:01,920
Now for example, dependent upon consciousness, nama and rupa arise.

303
00:46:01,920 --> 00:46:04,920
Now there is one link in the dependent origination.

304
00:46:04,920 --> 00:46:15,920
So dependent upon consciousness, nama and rupa arise.

305
00:46:15,920 --> 00:46:24,920
There, consciousness means that's a rebut consciousness.

306
00:46:24,920 --> 00:46:29,920
And nama means the mental factors arising along with it.

307
00:46:29,920 --> 00:46:35,920
And rupa means rupa born of kama.

308
00:46:35,920 --> 00:46:38,920
They arise at the same time.

309
00:46:38,920 --> 00:46:47,920
But the formula is dependent upon consciousness, nama and rupa arise.

310
00:46:47,920 --> 00:46:56,920
So if you do not apply the law of Patana to that link,

311
00:46:56,920 --> 00:47:04,920
you may wrongly take that when you're not produces.

312
00:47:04,920 --> 00:47:09,920
I mean, consciousness produces nama and rupa, which is wrong.

313
00:47:09,920 --> 00:47:11,920
They arise at the same time.

314
00:47:11,920 --> 00:47:16,920
And so they are related by root of what konis says.

315
00:47:16,920 --> 00:47:17,920
Right?

316
00:47:17,920 --> 00:47:18,920
Yeah.

317
00:47:18,920 --> 00:47:27,920
We are going to send an origination correctly only when you understand the law of Patana.

318
00:47:27,920 --> 00:47:29,920
Relations in the law of Patana.

319
00:47:29,920 --> 00:47:37,920
Therefore, it is very important that when you study dependent origination,

320
00:47:37,920 --> 00:47:41,920
you also study the Patana relations.

321
00:47:41,920 --> 00:47:48,920
It may not be easy to study both at the same time,

322
00:47:48,920 --> 00:47:58,920
because you need to understand some abitama.

323
00:47:58,920 --> 00:48:05,920
But it is rewarding if you have patient enough,

324
00:48:05,920 --> 00:48:08,920
say, to go through the study of abitama.

325
00:48:08,920 --> 00:48:14,920
And when you understand it, you understand very clearly

326
00:48:14,920 --> 00:48:18,920
you do not make mistakes in understanding.

327
00:48:18,920 --> 00:48:28,920
And so it is important that you study both the law of dependent origination

328
00:48:28,920 --> 00:48:36,920
and the law of Patana, and you study these two together.

329
00:48:36,920 --> 00:48:39,920
Okay, one more example.

330
00:48:39,920 --> 00:48:47,920
There is a link which is dependent upon contact filling arises,

331
00:48:47,920 --> 00:48:52,920
Pasa and Widana.

332
00:48:52,920 --> 00:48:55,920
How do you understand it?

333
00:48:55,920 --> 00:49:03,920
Contact arises first, and then leader Widana arises.

334
00:49:03,920 --> 00:49:10,920
So, contact and filling arise at the same time.

335
00:49:10,920 --> 00:49:18,920
But one is said to be the condition, and the other conditions, right?

336
00:49:18,920 --> 00:49:25,920
So, contact is said to be conditioning and filling is said to be conditioned,

337
00:49:25,920 --> 00:49:33,920
because it is, they are alluded by way of rising together, cognizance.

338
00:49:33,920 --> 00:49:41,920
So, even between things arising together, there is a relationship

339
00:49:41,920 --> 00:49:44,920
as conditioning and conditions.

340
00:49:44,920 --> 00:49:53,920
So, only when you apply the law of Patana condition to the law of dependent

341
00:49:53,920 --> 00:49:59,920
origination, do you understand dependent origination correctly?

342
00:49:59,920 --> 00:50:06,920
So, it is important that you study Patana conditions,

343
00:50:06,920 --> 00:50:13,920
and then study the dependent, the law of dependent origination

344
00:50:13,920 --> 00:50:18,920
along with the Patana conditions.

345
00:50:18,920 --> 00:50:25,920
Okay, I think by now we understand the law of course and effect

346
00:50:25,920 --> 00:50:35,920
fairly comprehensively in its three aspects.

347
00:50:35,920 --> 00:50:42,920
The law of comma and its results, the law of dependent origination

348
00:50:42,920 --> 00:50:47,920
and the law of Patana relations.

349
00:50:47,920 --> 00:51:07,920
Okay, Patana condition.

350
00:51:07,920 --> 00:51:31,920
Tana means a place for literally standing.

351
00:51:31,920 --> 00:51:38,920
So, something on which some other being stand.

