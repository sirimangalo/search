1
00:00:00,000 --> 00:00:07,000
I'm good evening, everyone.

2
00:00:07,000 --> 00:00:14,000
I'm not sure if that's any better.

3
00:00:14,000 --> 00:00:24,000
I'm broadcasting live in the seventh.

4
00:00:24,000 --> 00:00:33,000
This quote is about water.

5
00:00:33,000 --> 00:00:44,000
I'm the wind up on ham, which is probably, I would say, an underappreciated source of insight.

6
00:00:44,000 --> 00:00:50,000
It's much later, many years after the Buddha passed away.

7
00:00:50,000 --> 00:00:58,000
I think at least a few hundred, maybe more.

8
00:00:58,000 --> 00:01:03,000
But it's very well-written, very well-organized.

9
00:01:03,000 --> 00:01:08,000
And it's got much that is insightful about the Buddha's teaching.

10
00:01:08,000 --> 00:01:10,000
It's a lot of questions.

11
00:01:10,000 --> 00:01:15,000
It's the questions of King Melinda or Menyander, I think.

12
00:01:15,000 --> 00:01:25,000
It's a Greek king who is identified with a Greek king who conquered India or something.

13
00:01:25,000 --> 00:01:33,000
He came in contact with his Buddhist monks and asked them all sorts of questions,

14
00:01:33,000 --> 00:01:36,000
and most of them couldn't answer the question.

15
00:01:36,000 --> 00:01:49,000
And so they directed him to Nagasena, a very well-known monk who was skilled in debate

16
00:01:49,000 --> 00:01:57,000
and skilled in duration, skilled in answering questions.

17
00:01:57,000 --> 00:01:59,000
So it's many, many questions.

18
00:01:59,000 --> 00:02:01,000
It's quite a large volume.

19
00:02:01,000 --> 00:02:08,000
It's available in the internet, an old translation, but pretty good.

20
00:02:08,000 --> 00:02:13,000
You can find it at sacredtextsacrid-texts.org.

21
00:02:13,000 --> 00:02:16,000
It's worth reading.

22
00:02:16,000 --> 00:02:19,000
But this is about water.

23
00:02:19,000 --> 00:02:21,000
It's a comparison.

24
00:02:21,000 --> 00:02:24,000
It makes a lot of comparisons.

25
00:02:24,000 --> 00:02:32,000
He talks about nibana, nirvana, using a lot of different comparisons with similes.

26
00:02:32,000 --> 00:02:36,000
But he was talking about a meditator.

27
00:02:36,000 --> 00:02:43,000
Let's see if we can find the word for student of meditation.

28
00:02:43,000 --> 00:02:45,000
So the question is,

29
00:02:45,000 --> 00:03:01,000
it's a part of a larger text.

30
00:03:01,000 --> 00:03:23,000
It's asking one of the five ways, five things factors of water that one should grasp.

31
00:03:23,000 --> 00:03:27,000
It should take up.

32
00:03:27,000 --> 00:03:33,000
So how should one be like water?

33
00:03:33,000 --> 00:03:35,000
I just want to find the word,

34
00:03:35,000 --> 00:03:37,000
yoga, yoga, whatjara?

35
00:03:37,000 --> 00:03:39,000
Yoga, whatjara?

36
00:03:39,000 --> 00:03:44,000
Someone who is practicing yoga, basically.

37
00:03:44,000 --> 00:03:47,000
So this is what yoga was how the word yoga was used.

38
00:03:47,000 --> 00:03:50,000
Yoga was used for to mean exertion.

39
00:03:50,000 --> 00:03:54,000
But yoga, whatjara is the word that came to mean.

40
00:03:54,000 --> 00:03:57,000
It's probably adopted from Hinduism.

41
00:03:57,000 --> 00:04:04,000
But it was used in Buddhism to mean someone who is on the spiritual path.

42
00:04:04,000 --> 00:04:12,000
Someone who is exerting themselves.

43
00:04:12,000 --> 00:04:14,000
We also call them yogi.

44
00:04:14,000 --> 00:04:17,000
So we entirely use this word a lot.

45
00:04:17,000 --> 00:04:19,000
They call meditators yogis.

46
00:04:19,000 --> 00:04:24,000
Yogi, they change it, put the word.

47
00:04:24,000 --> 00:04:28,000
Someone who is practicing yoga is a yogi.

48
00:04:28,000 --> 00:04:31,000
But it doesn't mean yoga in modern sense.

49
00:04:31,000 --> 00:04:33,000
Yoga just means one.

50
00:04:33,000 --> 00:04:36,000
Because the control of a yogi means you,

51
00:04:36,000 --> 00:04:38,000
which means to be connected.

52
00:04:38,000 --> 00:04:42,000
It's actually where I think the word uta comes from my name.

53
00:04:42,000 --> 00:04:52,000
I mean a yoke, so the ox would have a yoke and would be tied to the work.

54
00:04:52,000 --> 00:04:54,000
So we're comparing water to such a person.

55
00:04:54,000 --> 00:04:59,000
How should such a person, someone who is undertaking the training of the Buddha,

56
00:04:59,000 --> 00:05:02,000
morality, concentration, wisdom?

57
00:05:02,000 --> 00:05:06,000
How should they be like water?

58
00:05:06,000 --> 00:05:11,000
Water is a good object for comparison.

59
00:05:11,000 --> 00:05:17,000
Why? Because water is naturally calm.

60
00:05:17,000 --> 00:05:38,000
Water is something that is still smooth as a reflection in the sense of being perfectly smooth and unshaken.

61
00:05:38,000 --> 00:05:41,000
In the same way a meditator should be like a pool of water,

62
00:05:41,000 --> 00:05:46,000
dispelling trickery, conjulary insinuation and dissembling.

63
00:05:46,000 --> 00:05:53,000
Should be well poised, unshaken and troubled and quite pure by nature.

64
00:05:53,000 --> 00:05:59,000
Water to a human's water is like the purest substance.

65
00:05:59,000 --> 00:06:01,000
That's what we survive on.

66
00:06:01,000 --> 00:06:07,000
We need water to survive.

67
00:06:07,000 --> 00:06:10,000
We're just funny, we're just talking about water today.

68
00:06:10,000 --> 00:06:16,000
I was at McMaster University at the Peace Studies booth.

69
00:06:16,000 --> 00:06:26,000
We were talking to a perspective students about the Peace Studies program.

70
00:06:26,000 --> 00:06:36,000
And I was talking about water because the Peace Studies department has gotten interested in the water situation.

71
00:06:36,000 --> 00:06:41,000
And it's about social, it's the social justice aspect of Peace Studies.

72
00:06:41,000 --> 00:06:44,000
I'm not really involved in it.

73
00:06:44,000 --> 00:06:49,000
So they're trying to address the issue of like in Flint, Michigan.

74
00:06:49,000 --> 00:06:53,000
The issue with the First Nations people here.

75
00:06:53,000 --> 00:06:58,000
And not having clean water to drink and to use.

76
00:06:58,000 --> 00:07:05,000
Having polluted water, having their water contaminated with chemicals.

77
00:07:05,000 --> 00:07:13,000
Toxins.

78
00:07:13,000 --> 00:07:15,000
And we're talking about Taoism.

79
00:07:15,000 --> 00:07:22,000
And one of the people working at the booth had quite a conversation with her about many things.

80
00:07:22,000 --> 00:07:26,000
But one of the most water, how water is.

81
00:07:26,000 --> 00:07:28,000
Water is basically this.

82
00:07:28,000 --> 00:07:32,000
So I think she'd be interested in this quote, really send it to her.

83
00:07:32,000 --> 00:07:37,000
Water is pure, water is unshaken, water is calm.

84
00:07:37,000 --> 00:07:42,000
Water is nurturing, I think she would say, because she's into this idea of nurturing.

85
00:07:42,000 --> 00:07:47,000
She sees it as sort of a feminine quality.

86
00:07:47,000 --> 00:07:52,000
So we're talking about feminism and feminine qualities and masculine qualities.

87
00:07:52,000 --> 00:07:59,000
Compassion being masculine, feminine, I mean traditionally being understood.

88
00:07:59,000 --> 00:08:09,000
You know, we both were trying to avoid the words feminine and masculine, but talking about the different energies in the world.

89
00:08:09,000 --> 00:08:15,000
And we live in a very quote unquote masculine world.

90
00:08:15,000 --> 00:08:19,000
And so there's a certain type of energy that one might call masculine.

91
00:08:19,000 --> 00:08:20,000
Hard.

92
00:08:20,000 --> 00:08:24,000
And the end we use the words hard and soft.

93
00:08:24,000 --> 00:08:27,000
Aggressive maybe.

94
00:08:27,000 --> 00:08:28,000
Water is not tasting.

95
00:08:28,000 --> 00:08:33,000
So fire and watery, let's say.

96
00:08:33,000 --> 00:08:37,000
But here it's water is purity.

97
00:08:37,000 --> 00:08:39,000
Water is clarity.

98
00:08:39,000 --> 00:08:41,000
Water is calm.

99
00:08:41,000 --> 00:08:43,000
So the first one is this unshaken.

100
00:08:43,000 --> 00:08:48,000
As a meditator, we should be unshaken by the vicissitudes of life.

101
00:08:48,000 --> 00:08:50,000
By the experiences of our meditation.

102
00:08:50,000 --> 00:08:53,000
Because they'll try to shake you.

103
00:08:53,000 --> 00:09:02,000
And then they'll shake you again and they'll shake you in different ways every all the time.

104
00:09:02,000 --> 00:09:05,000
The things will change.

105
00:09:05,000 --> 00:09:09,000
And then that way that they change will change.

106
00:09:09,000 --> 00:09:11,000
You have to be constantly.

107
00:09:11,000 --> 00:09:13,000
You have to be adaptive and flexible.

108
00:09:13,000 --> 00:09:15,000
You have to be clever.

109
00:09:15,000 --> 00:09:17,000
And you have to be present.

110
00:09:17,000 --> 00:09:24,000
As soon as you become static, you get carried away.

111
00:09:24,000 --> 00:09:32,000
And by the vicissitudes of life.

112
00:09:32,000 --> 00:09:39,000
The second one is water is poised and naturally cool.

113
00:09:39,000 --> 00:09:47,000
So yoga watch that I should be compassionate, should be possessed of patience.

114
00:09:47,000 --> 00:09:50,000
Should be cool in this way.

115
00:09:50,000 --> 00:09:53,000
Patients love and mercy.

116
00:09:53,000 --> 00:09:57,000
And they should not be hotheaded.

117
00:09:57,000 --> 00:09:59,000
They should not be quick to anger.

118
00:09:59,000 --> 00:10:01,000
They should not be irritable.

119
00:10:01,000 --> 00:10:02,000
Should not be impatient.

120
00:10:02,000 --> 00:10:05,000
Patients is very important.

121
00:10:05,000 --> 00:10:10,000
Patients with yourself, patients with other people, patients.

122
00:10:10,000 --> 00:10:12,000
And patients with things you want as well.

123
00:10:12,000 --> 00:10:17,000
Not just immediately giving up to help.

124
00:10:17,000 --> 00:10:26,000
Being controlled by your desires.

125
00:10:26,000 --> 00:10:36,000
As water makes the impure pure, even so one should be pure.

126
00:10:36,000 --> 00:10:41,000
One should have no faults.

127
00:10:41,000 --> 00:10:45,000
One should not have any reason to be reprimanded by the wise.

128
00:10:45,000 --> 00:10:48,000
Wise sheep, people should not be able to sense your one.

129
00:10:48,000 --> 00:10:52,000
No matter where they are in private or in public.

130
00:10:52,000 --> 00:11:00,000
They should be pure in their deeds and their speech.

131
00:11:00,000 --> 00:11:06,000
As water is wanted by everyone, even so a meditator should be.

132
00:11:06,000 --> 00:11:09,000
Should be the kind of person who is appreciated.

133
00:11:09,000 --> 00:11:19,000
It should work to make yourself not a burden, not a source of irritation for others.

134
00:11:19,000 --> 00:11:26,000
Because this is problematic for others and for yourself to create suffering.

135
00:11:26,000 --> 00:11:29,000
Obviously, you should not be such a person.

136
00:11:29,000 --> 00:11:38,000
You should think of water as being pure.

137
00:11:38,000 --> 00:11:45,000
Whenever someone sees pure water.

138
00:11:45,000 --> 00:11:51,000
There is nothing negative about water.

139
00:11:51,000 --> 00:11:57,000
And it is very much desired and appreciated.

140
00:11:57,000 --> 00:12:04,000
We should be like water in that way.

141
00:12:04,000 --> 00:12:07,000
And finally, water troubles no one.

142
00:12:07,000 --> 00:12:09,000
It is kind of the same.

143
00:12:09,000 --> 00:12:16,000
So the first one is people think positively and the next one is people should not think negatively.

144
00:12:16,000 --> 00:12:20,000
Don't think negatively of water they can positively.

145
00:12:20,000 --> 00:12:25,000
So you will go out and I should not do anything wrong that makes others unhappy.

146
00:12:25,000 --> 00:12:28,000
Water doesn't hurt people.

147
00:12:28,000 --> 00:12:32,000
Well, still water doesn't.

148
00:12:32,000 --> 00:12:36,000
Water actually can be quite troublesome.

149
00:12:36,000 --> 00:12:40,000
And certain instances can drown with water.

150
00:12:40,000 --> 00:12:48,000
Water can scald you if it is too hot.

151
00:12:48,000 --> 00:12:57,000
But here is the connection that the comparison is still forest pool, calm water.

152
00:12:57,000 --> 00:13:02,000
Pure water is valuable and appreciated.

153
00:13:02,000 --> 00:13:06,000
It is a good imagery.

154
00:13:06,000 --> 00:13:11,000
It is a kind of thing that we can think of when you want to think of what kind of a person you would like to be.

155
00:13:11,000 --> 00:13:13,000
As a meditator, what should I be?

156
00:13:13,000 --> 00:13:16,000
And water is something.

157
00:13:16,000 --> 00:13:21,000
It comes easily to mind, of course, because it is so much a part of our lives.

158
00:13:21,000 --> 00:13:27,000
And when you think about the purity of water and the stillness of a pool of water.

159
00:13:27,000 --> 00:13:32,000
The smoothness of the surface.

160
00:13:32,000 --> 00:13:35,000
You think about the cleansing nature of water.

161
00:13:35,000 --> 00:13:38,000
You think about how precious it is.

162
00:13:38,000 --> 00:13:41,000
So perfect it is.

163
00:13:41,000 --> 00:13:48,000
It is something to aspire for, aspire towards.

164
00:13:48,000 --> 00:13:54,000
So, a little bit of dhamma.

165
00:13:54,000 --> 00:13:58,000
Today, as I said, we were at the McMaster.

166
00:13:58,000 --> 00:14:00,000
And then we went to see a new house.

167
00:14:00,000 --> 00:14:02,000
Thinking about moving to a new house.

168
00:14:02,000 --> 00:14:09,000
Just to get more rooms because we have many people wanting to come and meditate here.

169
00:14:09,000 --> 00:14:14,000
So we are looking at how we are talking about.

170
00:14:14,000 --> 00:14:20,000
We are going to try to look at several different houses and see which one is most suitable.

171
00:14:20,000 --> 00:14:25,000
One today was good.

172
00:14:25,000 --> 00:14:30,000
Actually, maybe a little smaller than this house, but more rooms.

173
00:14:30,000 --> 00:14:36,000
So, I have to think about whether it feels a little claustrophobic because it is small.

174
00:14:36,000 --> 00:14:42,000
People have to stay here for a long time. Maybe they need more space.

175
00:14:42,000 --> 00:14:46,000
You know, you pack a lot of people into one small house.

176
00:14:46,000 --> 00:14:50,000
I want to think about that.

177
00:14:50,000 --> 00:14:56,000
Anyway, any questions?

178
00:14:56,000 --> 00:15:20,000
What are your thoughts on the new kadam bhudis?

179
00:15:20,000 --> 00:15:25,000
I kind of like to shy away and I am happy that I don't know things about many things because otherwise,

180
00:15:25,000 --> 00:15:33,000
it has happened with the lotus which I tend to be somewhat opinionated.

181
00:15:33,000 --> 00:15:38,000
So, I kind of try to avoid questions like, what do you think of X?

182
00:15:38,000 --> 00:15:48,000
And thus X is something within her tradition.

183
00:15:48,000 --> 00:15:59,000
Pretty sure it is Tibetan, but yeah, if a Google search returns many results claiming that something is a cult,

184
00:15:59,000 --> 00:16:03,000
it is a good reason to be wary.

185
00:16:03,000 --> 00:16:05,000
You should start your own center.

186
00:16:05,000 --> 00:16:06,000
Start your own group.

187
00:16:06,000 --> 00:16:10,000
Have people invite people over to your house, start a meetup.

188
00:16:10,000 --> 00:16:14,000
Go to meetup.com and start a meditation group.

189
00:16:14,000 --> 00:16:20,000
And then you can come on our hangout. You can broadcast and I can lead you.

190
00:16:20,000 --> 00:16:23,000
You can lead you in meditation or something.

191
00:16:23,000 --> 00:16:26,000
You can give a talk to your group.

192
00:16:26,000 --> 00:16:28,000
Everyone should start groups in their own area.

193
00:16:28,000 --> 00:16:31,000
We can have it as big network.

194
00:16:31,000 --> 00:16:36,000
We all meditate together.

195
00:16:36,000 --> 00:16:46,000
We can call it the new Siri Mangalo.

196
00:16:46,000 --> 00:17:15,000
We are doing walking meditation with our kids, as effective they like to walk in a much faster pace.

197
00:17:15,000 --> 00:17:18,000
Probably not.

198
00:17:18,000 --> 00:17:23,000
I would say walking meditation for young kids is difficult,

199
00:17:23,000 --> 00:17:30,000
as they tend to be impatient.

200
00:17:30,000 --> 00:17:35,000
If you want to teach kids how to meditate, it depends on their age, I suppose.

201
00:17:35,000 --> 00:17:38,000
But absolutely, they shouldn't walk fast.

202
00:17:38,000 --> 00:17:43,000
It's not about walking. It's about being aware of the movements of the feet.

203
00:17:43,000 --> 00:17:49,000
It's not neat.

204
00:17:49,000 --> 00:17:52,000
When kids tend to be very excited and full of energy,

205
00:17:52,000 --> 00:17:55,000
they have a hard time being patient.

206
00:17:55,000 --> 00:17:59,000
If you can teach them patience, that'd be awesome.

207
00:17:59,000 --> 00:18:02,000
But walking meditation requires patience.

208
00:18:02,000 --> 00:18:06,000
That's really the question, can your kids develop patience?

209
00:18:06,000 --> 00:18:09,000
Because you have to walk slowly.

210
00:18:09,000 --> 00:18:12,000
You need to develop patience.

211
00:18:12,000 --> 00:18:22,000
They're walking fast because they're impatient.

212
00:18:22,000 --> 00:18:31,000
I would think.

213
00:18:31,000 --> 00:18:46,000
When I meditate, I feel claustrophobic.

214
00:18:46,000 --> 00:18:50,000
Well, have you read my book that on how to meditate?

215
00:18:50,000 --> 00:18:53,000
You haven't, and that's where I recommend you start.

216
00:18:53,000 --> 00:18:55,000
It might help with your claustrophobia.

217
00:18:55,000 --> 00:19:01,000
If you feel that, it might be it's fear, right, phobia.

218
00:19:01,000 --> 00:19:06,000
So then you would say afraid, afraid, or worried, or anxious, anxious.

219
00:19:06,000 --> 00:19:13,000
But if you haven't read my book that I recommend that.

220
00:19:13,000 --> 00:19:15,000
Why avoid this kind?

221
00:19:15,000 --> 00:19:19,000
I haven't really done so much to answer your question.

222
00:19:19,000 --> 00:19:20,000
I haven't talked about it.

223
00:19:20,000 --> 00:19:25,000
I've talked about it in details because it's actually quite simple to fix.

224
00:19:25,000 --> 00:19:31,000
If you're practicing the meditation as I teach, as we teach.

225
00:19:31,000 --> 00:19:37,000
So there's not like any special tips for things like claustrophobia.

226
00:19:37,000 --> 00:19:40,000
The thing is, these conditions don't exist.

227
00:19:40,000 --> 00:19:43,000
You feel fear.

228
00:19:43,000 --> 00:19:47,000
And that's the key, is that there's a feeling of fear that arises.

229
00:19:47,000 --> 00:19:51,000
And so the practice that we follow would be to say to yourself,

230
00:19:51,000 --> 00:19:53,000
afraid, afraid.

231
00:19:53,000 --> 00:19:58,000
But to read my booklet, sort of, I hope.

232
00:19:58,000 --> 00:20:27,000
I think that will answer that in fairly good detail.

233
00:20:27,000 --> 00:20:37,000
Any other questions?

234
00:20:37,000 --> 00:20:40,000
I'm really serious about this starting demo groups in your area.

235
00:20:40,000 --> 00:20:41,000
I think it'd be great.

236
00:20:41,000 --> 00:20:44,000
And we can do it like once a week.

237
00:20:44,000 --> 00:20:53,000
We could all meet online, and I could give a talk.

238
00:20:53,000 --> 00:20:58,000
Or you could just do whatever day of the week and just come on at 9 p.m. with your group.

239
00:20:58,000 --> 00:21:03,000
And join our hangout.

240
00:21:03,000 --> 00:21:08,000
Of course, I suppose some people don't want the meditation broadcast on the internet.

241
00:21:08,000 --> 00:21:10,000
I suppose there's that.

242
00:21:10,000 --> 00:21:15,000
Well, one day a week we could do a private hangout that wouldn't have to be broadcast on the internet.

243
00:21:15,000 --> 00:21:20,000
Although it's nice to have the internet community involved.

244
00:21:20,000 --> 00:21:23,000
If something to think about.

245
00:21:23,000 --> 00:21:25,000
I haven't done that though.

246
00:21:25,000 --> 00:21:34,000
Private Skype calls to groups like there was a group in Texas who wanted me to lead them in meditation once a week.

247
00:21:34,000 --> 00:21:39,000
And I've done online stuff like that.

248
00:21:39,000 --> 00:21:43,000
That's what I'm thinking about.

249
00:21:43,000 --> 00:21:44,000
Okay.

250
00:21:44,000 --> 00:21:46,000
And then I'll say good night.

251
00:21:46,000 --> 00:21:53,000
Tomorrow, Mother's Day, we are going to Mississauga for the Sri Lankan.

252
00:21:53,000 --> 00:21:55,000
We sack a celebration.

253
00:21:55,000 --> 00:21:58,000
We sack a food, just celebration.

254
00:21:58,000 --> 00:22:04,000
We sack a punami, the full moon of the month we sack.

255
00:22:04,000 --> 00:22:18,000
Anyway, have a good night.

256
00:22:34,000 --> 00:22:36,000
Okay.

