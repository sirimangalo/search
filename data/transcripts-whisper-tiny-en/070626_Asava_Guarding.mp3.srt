1
00:00:00,000 --> 00:00:10,600
And today I will be talking about the second in this series of talks on the second in the

2
00:00:10,600 --> 00:00:21,680
series of groups of assholes of defalments which create, cause the mind to, ferment or cause

3
00:00:21,680 --> 00:00:23,720
the mind to go sour.

4
00:00:23,720 --> 00:00:29,900
It's just the poetic way of saying they make the mind dirty or they destroy the

5
00:00:29,900 --> 00:00:32,600
mind.

6
00:00:32,600 --> 00:00:39,000
And the second group of assholes, or the second method, really it's a method to overcome

7
00:00:39,000 --> 00:00:45,600
the assholes or a certain of mental defalment or mental pollution.

8
00:00:45,600 --> 00:00:53,680
And the second type is called the somewhat apahatapah, those assholes, or those mental

9
00:00:53,680 --> 00:01:02,320
pollutions which should be removed through guarding or through restraining.

10
00:01:02,320 --> 00:01:07,280
And in this case, it means restraining the six senses, so today I'll be talking about

11
00:01:07,280 --> 00:01:11,680
guarding the six senses.

12
00:01:11,680 --> 00:01:17,320
First a little bit about, a little bit more about the assova or about mental pollution's

13
00:01:17,320 --> 00:01:29,660
or things which make the mind rotten, make the mind go sour.

14
00:01:29,660 --> 00:01:34,440
It's well known that the Lord Buddha's teaching is for the purpose of the purification

15
00:01:34,440 --> 00:01:37,900
of the mind, of course, in the Satipatana Sutets.

16
00:01:37,900 --> 00:01:43,860
The first goal of the practice is satananguis vitya, the purification of beings, in this

17
00:01:43,860 --> 00:01:51,840
case the purification of the minds of beings.

18
00:01:51,840 --> 00:01:58,800
And in the Satipatana Sutets, of course, it's very clear how this comes about, how it comes

19
00:01:58,800 --> 00:02:01,440
about that our mind becomes pure.

20
00:02:01,440 --> 00:02:06,320
We all want our minds to be pure, but we can figure out how it is that we can do away

21
00:02:06,320 --> 00:02:14,000
with these things which cause stress and which cause suffering and which cause strife and quarrel

22
00:02:14,000 --> 00:02:24,440
inside of ourselves and among us and other people.

23
00:02:24,440 --> 00:02:32,080
So the Lord Buddha laid down a course of practice which leads us to make our minds pure.

24
00:02:32,080 --> 00:02:40,000
And in the sabbasa was that the Lord Buddha gave several methods or several directions

25
00:02:40,000 --> 00:02:41,560
we can take in order to overcome.

26
00:02:41,560 --> 00:02:47,640
In the end, it all comes down to, comes all back to the four foundations of mindfulness

27
00:02:47,640 --> 00:02:52,160
which were taught in the Satipatana Sutets with the Lord Buddha said, kachantho-wag its hammiti

28
00:02:52,160 --> 00:02:54,960
pachanthi and so on.

29
00:02:54,960 --> 00:03:04,440
When going he knows, I go, I'm going, when standing, he knows I'm standing or she, when

30
00:03:04,440 --> 00:03:08,720
sitting one knows when he's sitting, when lying, when he's lying, when one has a painful

31
00:03:08,720 --> 00:03:13,760
feeling, when one knows, I'm experiencing a painful feeling.

32
00:03:13,760 --> 00:03:19,320
One one has a thought of greed, when he knows this is a thought of greed, when one's

33
00:03:19,320 --> 00:03:24,600
mind is full of anger, when he knows this mind is full of anger.

34
00:03:24,600 --> 00:03:33,520
And the key here is one knows, one knows the bare truth of the phenomenon.

35
00:03:33,520 --> 00:03:38,920
And this comes back to the very essence of what it means to be mindful, the very essence

36
00:03:38,920 --> 00:03:42,640
of the word sati which means really to remember.

37
00:03:42,640 --> 00:03:50,240
And I mean simply to remember, to bring about remembrance in the mind of the pure and clear

38
00:03:50,240 --> 00:03:53,560
nature of the reality.

39
00:03:53,560 --> 00:03:58,160
And it's something which I've started to call in English in order to explain to Westerners

40
00:03:58,160 --> 00:04:02,320
clear thought.

41
00:04:02,320 --> 00:04:05,720
In the West, it's sometimes very difficult to understand why we would go around all they

42
00:04:05,720 --> 00:04:12,080
sing to ourselves rising, falling, why we would, when we walk we say walking, walking

43
00:04:12,080 --> 00:04:16,840
when we're sitting sitting sitting, when we say seeing, hearing, smelling, tasting, feeling

44
00:04:16,840 --> 00:04:22,800
why we say to ourselves again and again, as though we were robots.

45
00:04:22,800 --> 00:04:29,720
And so sometimes it comes across in the wrong way, because of how it's explained and

46
00:04:29,720 --> 00:04:36,760
because it's explained in a very robotic and a very mechanical fashion.

47
00:04:36,760 --> 00:04:40,960
But if we come back and explain why it is that we're doing this, it's easy to see that

48
00:04:40,960 --> 00:04:46,800
there is a real purpose to it, it's for the purpose of creating clear thought.

49
00:04:46,800 --> 00:04:49,840
Because in our minds it's not true that if we don't do this, our minds are not going

50
00:04:49,840 --> 00:04:52,600
to think anything.

51
00:04:52,600 --> 00:04:56,240
And this is maybe a problem which we come to the meditation with.

52
00:04:56,240 --> 00:05:01,640
We think the meditation is for the purpose of not thinking anything anymore, that maybe

53
00:05:01,640 --> 00:05:04,920
somehow we can just stop thinking.

54
00:05:04,920 --> 00:05:08,600
And then here we give the meditator a new set of thoughts.

55
00:05:08,600 --> 00:05:14,560
When you see, think seeing, when you hear, think hearing and so on.

56
00:05:14,560 --> 00:05:16,960
And I think this isn't really what they expect.

57
00:05:16,960 --> 00:05:26,360
But the truth is, any practice you do that blocks out all thought, you might find a way

58
00:05:26,360 --> 00:05:34,880
to block out your thought for a minute or an hour or even a day or so on, it can't possibly

59
00:05:34,880 --> 00:05:42,560
bring about that state of being forever as soon as the power of our concentration which

60
00:05:42,560 --> 00:05:47,800
blocked out the thought disappears, the thoughts will come back.

61
00:05:47,800 --> 00:06:02,280
And so we have to come to terms with the thoughts themselves, not actually, instead

62
00:06:02,280 --> 00:06:07,520
of trying to destroy all thoughts, we have to be no thoughts, instead we have to come to

63
00:06:07,520 --> 00:06:09,520
terms with the nature of the thoughts.

64
00:06:09,520 --> 00:06:15,040
Well, we have to actually be responsible for our thoughts, that we can't let our thoughts

65
00:06:15,040 --> 00:06:20,440
wander off into greed, into anger, into delusion, because our minds will continue to think

66
00:06:20,440 --> 00:06:24,640
and they'll think about the past and the future and create all sorts of mental constructs

67
00:06:24,640 --> 00:06:28,000
inside.

68
00:06:28,000 --> 00:06:34,240
And based on these constructs, constructs will create views and conceit and stinginess and

69
00:06:34,240 --> 00:06:44,920
jealousy and so on and so on, cruelty, deceit and so on, all sorts of developments, things

70
00:06:44,920 --> 00:06:52,840
which cause the mind to ferment, to go rotten, keep the mind from being pure and being

71
00:06:52,840 --> 00:07:02,640
clean, being fresh, which make the mind pickled, pickled the mind.

72
00:07:02,640 --> 00:07:12,800
So the purpose of meditation is not to do away with our natural state of being, as in

73
00:07:12,800 --> 00:07:18,640
thinking or any other state of being, any sort of pain in the body or state in the body,

74
00:07:18,640 --> 00:07:28,800
but to purify, to make it pure, to make our actions, our words and our thoughts pure.

75
00:07:28,800 --> 00:07:34,600
So at the time when we say to ourselves, rising, falling, all we're trying to do is come

76
00:07:34,600 --> 00:07:38,360
to terms with something which is really and truly there.

77
00:07:38,360 --> 00:07:42,320
When we're walking while we're walking anyway, but instead of letting our minds think

78
00:07:42,320 --> 00:07:49,760
while we're going to think anyway, instead of letting our minds think about how wonderful

79
00:07:49,760 --> 00:07:58,280
our body is, or how awful our body is, or create all sorts of delusions about what we're

80
00:07:58,280 --> 00:08:05,200
doing at this moment, instead we simply come to terms with the bare reality of the action.

81
00:08:05,200 --> 00:08:09,840
So when we walk, walking, walking, and when we see things, when we hear things, when

82
00:08:09,840 --> 00:08:14,840
we smell things, when we taste, when we feel when we think anything, we come to terms

83
00:08:14,840 --> 00:08:16,760
with the bare reality.

84
00:08:16,760 --> 00:08:22,240
And so this is today's topic on specifically the six senses.

85
00:08:22,240 --> 00:08:31,000
The seeing, hearing, smelling, tasting, feeling and thinking.

86
00:08:31,000 --> 00:08:36,360
And this is another very important core concept in the Buddhist teaching, the teaching

87
00:08:36,360 --> 00:08:46,840
and the senses, because for most people in the world, these are a reality which is overlooked

88
00:08:46,840 --> 00:08:51,320
when we interact with the world around us.

89
00:08:51,320 --> 00:09:00,280
Everything we see is simply a sight, simply a vision, simply light touching the eye.

90
00:09:00,280 --> 00:09:05,280
Everything we hear is simply a sound touching the ear, everything we smell is simply

91
00:09:05,280 --> 00:09:14,360
a scent touching the nose, taste touching the tongue, thoughts touching the body, a sensation

92
00:09:14,360 --> 00:09:19,560
touching the body and thoughts coming in contact with the mind, and thoughts arising in

93
00:09:19,560 --> 00:09:23,920
the mind.

94
00:09:23,920 --> 00:09:28,320
But what happens for the average person is when we see something, it becomes something

95
00:09:28,320 --> 00:09:37,960
either desirable or something undesirable, something pleasant or something unpleasant.

96
00:09:37,960 --> 00:09:41,720
Based on our past experiences in the past, this is something which has brought pleasure

97
00:09:41,720 --> 00:09:42,800
to us.

98
00:09:42,800 --> 00:09:49,440
In the past, this is something brought pain to us and we're afraid or we're excited about

99
00:09:49,440 --> 00:09:53,480
the fruit which it will bring this time as well.

100
00:09:53,480 --> 00:09:58,560
And based on this memory we then create our thoughts about it, and this is where our

101
00:09:58,560 --> 00:10:00,520
impure thoughts arise.

102
00:10:00,520 --> 00:10:04,800
So it has brought us happiness in the past, we want to get it.

103
00:10:04,800 --> 00:10:11,360
We seek out for it, we seek out for a way to come to possess it.

104
00:10:11,360 --> 00:10:19,680
And we find ourselves leaving behind our duties and our obligations in search, instead

105
00:10:19,680 --> 00:10:26,200
of instead in search for pleasure in search for the objects of our desire.

106
00:10:26,200 --> 00:10:31,080
And we find ourselves unable to do without these things.

107
00:10:31,080 --> 00:10:36,480
And on the other hand, when bad things unpleasant things come, things which we don't

108
00:10:36,480 --> 00:10:41,240
like, we will do anything to be free from them, sometimes we will say bad things or do

109
00:10:41,240 --> 00:10:46,680
bad things, simply do not have to see and hear and smell and taste and feel and think

110
00:10:46,680 --> 00:10:50,480
these bad things anymore.

111
00:10:50,480 --> 00:10:55,040
Now somewhere the Lord Buddha's teaching on how to destroy these defilements which arise

112
00:10:55,040 --> 00:11:02,080
at the eye, the ear, the nose, the tongue, the body and the heart is simply the same

113
00:11:02,080 --> 00:11:05,080
Lord Buddha's same teaching on mindfulness.

114
00:11:05,080 --> 00:11:12,080
People remember this, reminding ourselves that when we see this is only seen, so what

115
00:11:12,080 --> 00:11:15,280
is it that we do when we see something?

116
00:11:15,280 --> 00:11:21,040
Why is it that we say to ourselves seeing, we are creating a bare remembrance in our mind,

117
00:11:21,040 --> 00:11:24,880
a clear thought of what is the reality of the experience.

118
00:11:24,880 --> 00:11:29,640
So it is no longer a bad thing or a good thing, something which is going to lead us to

119
00:11:29,640 --> 00:11:37,800
get angry or get greedy or lead to all sorts of views and conceits and ideas instead

120
00:11:37,800 --> 00:11:41,880
it is simply going to be light touching the eye.

121
00:11:41,880 --> 00:11:48,000
And not only does this prevent the bad things from arising, it also creates a deeper fruit

122
00:11:48,000 --> 00:11:51,440
as well which is the fruit of insight.

123
00:11:51,440 --> 00:11:56,600
It helps us to see that really the whole world around us is simply this, it is simply

124
00:11:56,600 --> 00:12:02,400
16 sites, sounds, smells, tastes, feelings and thoughts.

125
00:12:02,400 --> 00:12:10,880
And the problem is 90% of what we think is real is simply thought, when we see something

126
00:12:10,880 --> 00:12:19,120
it becomes trees, it becomes people, it becomes beautiful, it becomes ugly, it becomes

127
00:12:19,120 --> 00:12:27,320
me and mine and you and them and theirs and we create all sorts of views and leaps about

128
00:12:27,320 --> 00:12:32,160
the things which we see when actually it is simply light touching the eye.

129
00:12:32,160 --> 00:12:41,600
We start to see that the reality of the whole world around us is simply a very, a very

130
00:12:41,600 --> 00:12:48,400
simple, very ordinary thing which is sight and then sound and smell and taste and feeling

131
00:12:48,400 --> 00:12:52,080
and thought and these things arise and cease and arise and cease and this is where we

132
00:12:52,080 --> 00:12:56,760
come to see the truth of what the Lord would have called impermanence, suffering and

133
00:12:56,760 --> 00:12:57,760
oneself.

134
00:12:57,760 --> 00:13:07,440
First we see that there is the mind and there is the body and the body is the light and

135
00:13:07,440 --> 00:13:14,320
the eye or the sound and the ear and there is the mind which goes to touch, touch the

136
00:13:14,320 --> 00:13:17,400
eye, touch the ear.

137
00:13:17,400 --> 00:13:21,360
As we can see we can be looking at something with our eyes but our mind is somewhere else

138
00:13:21,360 --> 00:13:25,720
so we don't even see what our eyes are looking at.

139
00:13:25,720 --> 00:13:31,920
Sometimes we are, a sound comes to the ear, someone calls us but because our mind is

140
00:13:31,920 --> 00:13:38,880
preoccupied with something else we don't even hear the sound or a smell or a taste.

141
00:13:38,880 --> 00:13:42,320
Sometimes we are eating food and it is very tasty but our minds are thinking about something

142
00:13:42,320 --> 00:13:43,320
else.

143
00:13:43,320 --> 00:13:46,480
We don't even taste the food which we are eating.

144
00:13:46,480 --> 00:13:51,240
So when we practice we start to see this rhetoric, it is called body and mind and unless

145
00:13:51,240 --> 00:13:56,880
the two are together we know arising of the sight, the sound, the smell, the taste of

146
00:13:56,880 --> 00:14:04,800
feeling about but when they come together these things, one of these six things will arise.

147
00:14:04,800 --> 00:14:09,760
And then we start to see this deeper truth that these things, this seeing, this hearing,

148
00:14:09,760 --> 00:14:15,040
this smelling, this tasting, this feeling and this thinking is not me, it is not mine,

149
00:14:15,040 --> 00:14:21,280
it is not permanent and it is not under my control, it is not something which is subject

150
00:14:21,280 --> 00:14:26,000
to being the way I want it to be all the time.

151
00:14:26,000 --> 00:14:31,240
It is something which will often be the way I be in a certain way that I don't wish it

152
00:14:31,240 --> 00:14:32,240
for it to be.

153
00:14:32,240 --> 00:14:37,320
So I will often see things which are unpleasant, hear things which are unpleasant, smell

154
00:14:37,320 --> 00:14:41,120
things which are unpleasant and so on.

155
00:14:41,120 --> 00:14:43,400
And this is something which I can't avoid.

156
00:14:43,400 --> 00:14:49,360
So we see these three, the lone Buddha called the three characteristics, the three natural

157
00:14:49,360 --> 00:14:54,280
realities, just like in science we talk about the characteristics of matter and the characteristics

158
00:14:54,280 --> 00:14:56,480
of energy and so on.

159
00:14:56,480 --> 00:15:02,680
When the Buddha was a very pure scientist and he said the reality is three characteristics,

160
00:15:02,680 --> 00:15:09,920
it is impermanent and it is unsatisfying and it is not under our control.

161
00:15:09,920 --> 00:15:15,880
When we see this, this is what changes our mind so that we don't want to hold on to these

162
00:15:15,880 --> 00:15:21,760
things, we don't want to seek out, seek after these things which we use to find pleasant

163
00:15:21,760 --> 00:15:24,720
which we use to think we are going to bring us happiness, we can see that these are

164
00:15:24,720 --> 00:15:31,720
things which actually pickle the mind which actually cause the mind to go rotten, which

165
00:15:31,720 --> 00:15:39,520
create addiction, which create all sorts of displeasure, all sorts of frustration and anger

166
00:15:39,520 --> 00:15:40,520
and hatred.

167
00:15:40,520 --> 00:15:50,680
When we start to let go of all of our illusions, all of our old habits, all of our old ways

168
00:15:50,680 --> 00:15:52,800
of reacting to things.

169
00:15:52,800 --> 00:15:56,840
And we find that that is simply all that there is in this body and in this mind is

170
00:15:56,840 --> 00:16:03,680
old habits, old ways of reacting to things which are really no longer applicable now

171
00:16:03,680 --> 00:16:08,160
that we can see that these are only seeing, hearing, smelling, tasting, feeling and thinking

172
00:16:08,160 --> 00:16:16,120
and we start to find these, become bored, become disenchanted with these old ways of reacting.

173
00:16:16,120 --> 00:16:21,200
We come to see where true peace and true happiness lies and that isn't simply having

174
00:16:21,200 --> 00:16:27,360
a clear thought about the present reality, to know simply what is this that we are experiencing

175
00:16:27,360 --> 00:16:29,360
right here and now.

176
00:16:29,360 --> 00:16:34,400
As we sit here what is happening around us, there are sights, there are sounds, there

177
00:16:34,400 --> 00:16:38,960
are smells, there are tastes, there are feelings and there are thoughts and these make up

178
00:16:38,960 --> 00:16:48,000
the universe and when we see in this way when we practice in this way, we will come to

179
00:16:48,000 --> 00:16:55,120
reach what is the goal of life, the goal of Buddhism, the goal of the meditation practice

180
00:16:55,120 --> 00:17:02,640
and that is of course the path and the fruition and the nibana or freedom and what

181
00:17:02,640 --> 00:17:10,400
it simply means to become free, we will become unbound, we will become free as a bird

182
00:17:10,400 --> 00:17:16,800
from a cage and we no longer improve in prison ourselves, we will no longer be imprisoned

183
00:17:16,800 --> 00:17:24,160
by our addictions and by our aversion, we will be able to live in this world, to go anywhere

184
00:17:24,160 --> 00:17:31,400
in peace and in freedom without any sort of better, without any sort of bond, without any

185
00:17:31,400 --> 00:17:38,480
fear or any angst or any worry or any stress, taking everything as it comes because we

186
00:17:38,480 --> 00:17:42,160
see it as it for what it is.

187
00:17:42,160 --> 00:17:46,440
This is the Lord Buddha's teaching on samura pahata pahata.

188
00:17:46,440 --> 00:17:55,720
The method of overcoming the bad things in the mind by guarding or by restraining the

189
00:17:55,720 --> 00:18:01,320
senses, by not letting our minds fly out, follow out after the objects of the

190
00:18:01,320 --> 00:18:05,920
senses and that is all for today and I will practice together.

