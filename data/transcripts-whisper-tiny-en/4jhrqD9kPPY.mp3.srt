1
00:00:00,000 --> 00:00:06,000
Hello, and welcome back to Ask a Monk. Today's question is about drowsiness.

2
00:00:06,000 --> 00:00:11,000
So I was asked how to deal with falling asleep when meditating.

3
00:00:11,000 --> 00:00:17,000
Someone said they would like to be able to meditate, and they've tried, but they find themselves falling asleep every time.

4
00:00:17,000 --> 00:00:21,000
I think I've addressed this before, but not comprehensively.

5
00:00:21,000 --> 00:00:29,000
So I'd like to do that now, sort of referring back to the Buddha's teaching.

6
00:00:29,000 --> 00:00:44,000
Now, the person who asked the question this time said that he or she would like to use the meditation for the purpose of feeling rested.

7
00:00:44,000 --> 00:00:48,000
So they want to be able to find rest, but still be alert.

8
00:00:48,000 --> 00:00:55,000
And I'd like to caution that that may be the reason why you're falling asleep when you meditate,

9
00:00:55,000 --> 00:01:00,000
because you've got the wrong understanding of what meditation should be.

10
00:01:00,000 --> 00:01:08,000
If your intention is to simply rest, then falling asleep is probably the best way to do it.

11
00:01:08,000 --> 00:01:16,000
To an extent, I mean, I guess if that's the only, the point is if that's the only intention that you go into the meditation with,

12
00:01:16,000 --> 00:01:21,000
and that's the direction that your mind is going to go.

13
00:01:21,000 --> 00:01:27,000
If you sit down and meditate and say, okay, I want to feel rested, your mind is not going to develop energy.

14
00:01:27,000 --> 00:01:31,000
It's not going to be awake and alert.

15
00:01:31,000 --> 00:01:38,000
It's going to start to tend towards the only way that it knows how to rest, and that is too falling asleep.

16
00:01:38,000 --> 00:01:40,000
There are better ways in meditation.

17
00:01:40,000 --> 00:01:47,000
I think certainly does lead to much more of a rested mind state, but the mind won't go in that direction.

18
00:01:47,000 --> 00:01:53,000
Unless your intention in going to the meditation is to use it for the purpose of meditating,

19
00:01:53,000 --> 00:01:59,000
and that is to understand reality and to be able to deal with the difficulties and the problems,

20
00:01:59,000 --> 00:02:06,000
and to understand the difficulties and the problems that you have in your mind and in your life.

21
00:02:06,000 --> 00:02:15,000
If you go into the meditation with this mindset, this sort of mindset, then your mind is more likely to be alert and to be interested,

22
00:02:15,000 --> 00:02:19,000
and you're more likely to be able to pay attention, less likely to fall asleep.

23
00:02:19,000 --> 00:02:26,000
That's the first bit of advice I would give this to try to reevaluate what you expect to get out of meditation.

24
00:02:26,000 --> 00:02:33,000
Resting is certainly not the purpose of practicing meditation, not the best aim.

25
00:02:33,000 --> 00:02:36,000
If that's the aim going into it, as I said,

26
00:02:36,000 --> 00:02:48,000
then going to have these sorts of repercussions causing you to fall asleep. Nonetheless, for those meditators who have made up their mind and come to understand what it is that meditation is for,

27
00:02:48,000 --> 00:02:54,000
and are using it for the purpose of coming to understand reality as it is,

28
00:02:54,000 --> 00:03:01,000
there are times, especially when doing intensive meditation, that you find yourself feeling drowsy and even falling asleep.

29
00:03:01,000 --> 00:03:03,000
So how do you deal with this?

30
00:03:03,000 --> 00:03:11,000
Well, the Buddha gave seven specific techniques that one could use to overcome drowsiness,

31
00:03:11,000 --> 00:03:20,000
and there are actually seven different types of drowsiness, or at least a few different types of drowsiness.

32
00:03:20,000 --> 00:03:28,000
Each one is unique in its approach, and so I'd like to in this case refer directly back to this.

33
00:03:28,000 --> 00:03:36,000
It's the Pajalayana Sutta, or Pajalasutta, or Chapala Sutta, it's in the Angutranikaya Book of Seven.

34
00:03:36,000 --> 00:03:41,000
So we have seven ways of dealing with drowsiness, that's pretty.

35
00:03:41,000 --> 00:03:51,000
So the first way that the Buddha said is to change your object or to examine the object of your attention,

36
00:03:51,000 --> 00:03:59,000
because one of the most obvious reasons why someone might become drowsies,

37
00:03:59,000 --> 00:04:01,000
because their mind has begun to wander.

38
00:04:01,000 --> 00:04:06,000
When you're sitting in meditation, you begin by focusing on a specific object,

39
00:04:06,000 --> 00:04:16,000
but your mind starts to wander and slowly fall into a more trance-like state that is bordering on sleep,

40
00:04:16,000 --> 00:04:19,000
and eventually will lead one to fall asleep.

41
00:04:19,000 --> 00:04:29,000
Now, when that is the case, it's important to think about what it was that you were considering when you were feeling drowsy,

42
00:04:29,000 --> 00:04:43,000
and to change that, to go back to focusing on the original object and to be careful not to let yourself fall into the reflection on these unspeculative thoughts

43
00:04:43,000 --> 00:04:46,000
that are going to lead your mind to wander.

44
00:04:46,000 --> 00:04:51,000
Often, it's the case that we'll start to remember things that we're worried about or concerned about,

45
00:04:51,000 --> 00:04:59,000
and that will lead our minds to wander and speculate and eventually get tired and fall into fall asleep.

46
00:04:59,000 --> 00:05:08,000
So the Buddha said, be careful not to give those sorts of thoughts any ground, whatever thought it was that was causing you to feel tired,

47
00:05:08,000 --> 00:05:19,000
to avoid that thought and not develop that train of thought or state of mind.

48
00:05:19,000 --> 00:05:24,000
Of course, the best way to do this is to come back to your meditation technique,

49
00:05:24,000 --> 00:05:31,000
and especially to deal with, as the Buddha said, the drowsiness itself,

50
00:05:31,000 --> 00:05:37,000
instead of allowing the thoughts to build the drowsiness, focus on the drowsiness and look at it.

51
00:05:37,000 --> 00:05:43,000
When you do that, you're letting go of the cause of the drowsiness, and the drowsiness will disappear by itself.

52
00:05:43,000 --> 00:05:51,000
Also, the attention and the alert awareness of the drowsiness is the opposite,

53
00:05:51,000 --> 00:05:58,000
and you'll find that the drowsiness, as you watch it, because of the change in the mind state, the drowsiness itself goes away.

54
00:05:58,000 --> 00:06:02,000
So you can try to focus on the drowsiness itself, which is quite useful,

55
00:06:02,000 --> 00:06:08,000
but the most important is to avoid the thoughts that were causing you to become drowsy.

56
00:06:08,000 --> 00:06:17,000
The second way is to, if this practical method doesn't work of reverting back to the practice and adjusting your practice,

57
00:06:17,000 --> 00:06:24,000
you can go back to the teachings that the Buddha taught or that your teacher gave us on,

58
00:06:24,000 --> 00:06:32,000
and to go over them in your mind, to think about, for instance, the four foundations of mindfulness,

59
00:06:32,000 --> 00:06:40,000
the body, the feelings, the mind, the various dummies, the hindrances of the emotions or so on.

60
00:06:40,000 --> 00:06:45,000
Focus on what it was that the Buddha taught or what it is that your meditation teacher taught,

61
00:06:45,000 --> 00:06:49,000
and go over them also, the body, how do we focus on the body?

62
00:06:49,000 --> 00:06:59,000
Think about maybe ways that you are lacking and things that you are missing in terms of awareness of the body,

63
00:06:59,000 --> 00:07:08,000
and are you actually able to watch the movements of the stomach, or be aware of the sitting position, or the breath, or whatever is your object?

64
00:07:08,000 --> 00:07:13,000
Are you actually aware of the feelings when there's a painful feeling? Are you actually paying attention to it?

65
00:07:13,000 --> 00:07:20,000
For instance, saying to yourself, pain, pain, or if you feel happy or calm, are you actually paying attention?

66
00:07:20,000 --> 00:07:29,000
Or are you letting it drag you down into a state of lethargy and a state of fatigue and drowsiness?

67
00:07:29,000 --> 00:07:32,000
And the same with the mind and the mind.

68
00:07:32,000 --> 00:07:42,000
So refer back to the teachings and think about them in your mind, examine them, and relate them back to your practice, and compare your practice.

69
00:07:42,000 --> 00:07:45,000
Compare your practice to the teachings.

70
00:07:45,000 --> 00:07:53,000
If you do that, first of all, just thinking about these good things will wake you up and remind you of what you should be doing.

71
00:07:53,000 --> 00:07:57,000
Second of all, it will allow you to adjust your practice.

72
00:07:57,000 --> 00:08:03,000
The third way, if that doesn't work, or another way to deal with drowsiness, is to actually recite the teachings.

73
00:08:03,000 --> 00:08:13,000
And I know from experience that this is quite useful, for example, when you're driving.

74
00:08:13,000 --> 00:08:24,000
I remember when I would be driving somewhere at night and trying to be mindful, watching the road, and steering, turning, seeing, and emotions that arise, and so on.

75
00:08:24,000 --> 00:08:33,000
That I would find myself drifting, because sometimes there would be excess concentration, and not enough effort, and maybe falling asleep.

76
00:08:33,000 --> 00:08:41,000
When that happened, I would actually recite the Buddha's teachings, and we'd do these chance that we have the Buddha's teachings.

77
00:08:41,000 --> 00:08:53,000
We actually recite them, so it's not really an intellectual exercise, but it's something akin to singing songs when people turn on the radio and sing along when they're driving late at night.

78
00:08:53,000 --> 00:08:59,000
It's something that will wake you up, but also, of course, because it's the Buddha's teachings.

79
00:08:59,000 --> 00:09:15,000
It's something that will invigorate you and give you effort and give you the encouragement that you might need, thinking about the Buddha, thinking about his teachings, thinking about the meditation practice, and so on.

80
00:09:15,000 --> 00:09:30,000
If that still doesn't work, the fourth method the Buddha said is to start to get physical, and the Buddha said, you pull your ears, the technique of waking you up, kind of stretching your cranium.

81
00:09:30,000 --> 00:09:43,000
Rub your arms, rub your body, massage yourself, and maybe stretch a little bit to kind of wake you up, to get the blood flowing, and to give yourself a little bit of physical energy.

82
00:09:43,000 --> 00:09:51,000
It might be the cause, your body might be tired, or so on, your body might be stiff, for instance.

83
00:09:51,000 --> 00:09:54,000
Some people might even go so far as to practice yoga.

84
00:09:54,000 --> 00:10:02,000
I think it could be a very useful technique to waking you up, because it's a different technique, and actually might lead in a different direction.

85
00:10:02,000 --> 00:10:24,000
I wouldn't recommend extensive practice of yoga in combination with insight meditation, but there's certainly nothing harmful in practicing it, and certainly in moderation for the purposes of building energy necessary to practice meditation.

86
00:10:24,000 --> 00:10:32,000
But at any rate, the Buddha said massaging, and rubbing, and pulling your ears, and so on.

87
00:10:32,000 --> 00:10:45,000
If that doesn't work, another way to deal with drowsiness, the Buddha said, is to actually stand up, and then go get some water, pour water on your face, rub water into your eyes, and look to all directions.

88
00:10:45,000 --> 00:10:56,000
The Buddha said, take a look around, this is a way of waking you up, and stimulating your mind to get rid of this heavy state of concentration.

89
00:10:56,000 --> 00:11:01,000
Look in all directions, look all around, you go and look around, see what's going on.

90
00:11:01,000 --> 00:11:21,000
Look up at the stars, he said, you should be drowsiness comes at night, so go and look up at the sky, look up at the stars, look up at the constellations, is a way of breaking up this heavy concentration, giving yourself more flexible state of mind to break, to throw off the lethargy, the drowsiness.

91
00:11:21,000 --> 00:11:31,000
He said, if you do that, then it's possible that the drowsiness will disappear, and you'll be able to continue with your practice.

92
00:11:31,000 --> 00:11:36,000
You can do walking meditation, the Buddha said, this is a part of this, is to switch to doing walking meditation.

93
00:11:36,000 --> 00:11:40,000
That's one of the reasons for walking meditation as well.

94
00:11:40,000 --> 00:11:43,000
It allows you to develop effort and energy.

95
00:11:43,000 --> 00:11:52,000
If you sit all the time, it's easy to fall asleep, if you do walking as well, you'll find that when you do the sitting, you feel charged afterwards.

96
00:11:52,000 --> 00:11:55,000
So this is the fifth method.

97
00:11:55,000 --> 00:12:03,000
The sixth method is a specific meditation technique, and it's probably not applicable for most beginner meditators,

98
00:12:03,000 --> 00:12:11,000
but if you've been practicing meditation for a while, and I suppose even as a beginner you could attempt it.

99
00:12:11,000 --> 00:12:25,000
The technique is to think of day as night, to resolve on the meditation, or the awareness of light, even though it's dark at night, because of course night is the darkness.

100
00:12:25,000 --> 00:12:38,000
It's something that triggers our recollection of, oh, now it's time to fall asleep, and causes us to start to feel drowsy, our mind triggers the drowsiness routine, based on the fact that it's dark.

101
00:12:38,000 --> 00:12:48,000
If you trick yourself, you tell yourself that it's light, or you envision, you can close with your eyes closed, you can imagine light, or become aware of light.

102
00:12:48,000 --> 00:12:55,000
I know there are monks who told me that you should do meditation with a bright light on, and I suppose this could help as well.

103
00:12:55,000 --> 00:13:07,000
I've tried it, it doesn't seem that useful, but I suppose it could be, at least to some extent, but here the point is to actually get it in your mind that it's light.

104
00:13:07,000 --> 00:13:12,000
That there is light, because that will trigger the energy in the mind.

105
00:13:12,000 --> 00:13:14,000
It's a way of stimulating energy.

106
00:13:14,000 --> 00:13:20,000
He said, thinking of though it's night to think of it as day, and to get an idea in your mind.

107
00:13:20,000 --> 00:13:28,000
If you're actually been practicing meditation for a while, you can actually begin to envision bright lights.

108
00:13:28,000 --> 00:13:33,000
Some people even get very distracted by these lights, or colors, or pictures, or so on.

109
00:13:33,000 --> 00:13:41,000
And we have to remind meditators not to get off track, and to acknowledge them as seeing, to just remind yourself this is a visual stimulus.

110
00:13:41,000 --> 00:13:48,000
It's not a magical, magical experience, or certainly not the path that leads to enlightenment.

111
00:13:48,000 --> 00:13:50,000
It's the wrong path.

112
00:13:50,000 --> 00:13:54,000
But in this case, it has a limited benefit of bringing about energy and effort.

113
00:13:54,000 --> 00:13:59,000
It has its use, even though it's not the path.

114
00:13:59,000 --> 00:14:01,000
And that's the sixth method.

115
00:14:01,000 --> 00:14:04,000
If that doesn't work, the Buddha said, lie down.

116
00:14:04,000 --> 00:14:08,000
And the Buddha's way of lying down was to lie down on your side.

117
00:14:08,000 --> 00:14:12,000
Not in your stomach, not in your back, but to lie down on one side.

118
00:14:12,000 --> 00:14:17,000
And I'm not sure you can prop your head up, or I don't think that's mentioned.

119
00:14:17,000 --> 00:14:22,000
The way I would often do it is to actually prop my head up on my elbow.

120
00:14:22,000 --> 00:14:27,000
And it's a technique that I've seen other people use in monks' use.

121
00:14:27,000 --> 00:14:31,000
And it seems to very much keep you awake.

122
00:14:31,000 --> 00:14:36,000
If you start to fall asleep, your head starts to fall off your arm, and you wake up quite well.

123
00:14:36,000 --> 00:14:38,000
It's also quite painful in the beginning.

124
00:14:38,000 --> 00:14:40,000
It's something you have to develop.

125
00:14:40,000 --> 00:14:45,000
It's a physical technique that you have to develop, just like sitting cross-legged.

126
00:14:45,000 --> 00:14:48,000
But at any rate, lie down on one side.

127
00:14:48,000 --> 00:14:54,000
And perhaps not propping the head up, but at the very least, resolving on getting up.

128
00:14:54,000 --> 00:14:59,000
The Buddha said, you're lying down, and you know, you're kind of given up at this point.

129
00:14:59,000 --> 00:15:05,000
Because you feel like you can't overcome the drowsiness, so you're going to accept that you might fall asleep.

130
00:15:05,000 --> 00:15:12,000
So you lie down, and you say, I'm going to get up in so many minutes, or so many hours.

131
00:15:12,000 --> 00:15:17,000
If it's time to sleep, you say, I'm going to sleep for four hours, or however long you're going to sleep.

132
00:15:17,000 --> 00:15:20,000
And I'm going to get up at such and such a time.

133
00:15:20,000 --> 00:15:26,000
When you resolve, think about, before you go to sleep, think only about that, the fact that you're going to get up.

134
00:15:26,000 --> 00:15:39,000
So that when that time comes, you'll find that if you resolve in this way, your mind is able to somehow wake you up at that time.

135
00:15:39,000 --> 00:15:49,000
It's amazing how advanced the mind really is that you'll find yourself waking up a few minutes before the hour that you're going to wake up.

136
00:15:49,000 --> 00:15:54,000
Or how many minutes you were going to sleep or so on.

137
00:15:54,000 --> 00:15:56,000
And then the Buddha said, and then get up.

138
00:15:56,000 --> 00:16:05,000
He said, resolve in your mind, I will not give into drowsiness, and I will not become attached to the pleasure that comes from lying down.

139
00:16:05,000 --> 00:16:13,000
Because one of the things that we miss, one of the great addictions that we miss, is the addiction to sleep.

140
00:16:13,000 --> 00:16:23,000
Many people like to lie down for many, many hours and sleep a lot, and it's because of the physical pleasure that comes from it.

141
00:16:23,000 --> 00:16:29,000
But the problem with this physical pleasure is, like all physical pleasures, it's not permanent, it's not lasting.

142
00:16:29,000 --> 00:16:33,000
And it's not the case that the more you sleep, the more happy you feel.

143
00:16:33,000 --> 00:16:50,000
And even fact, feel more depressed and more lethargic and less energetic, less bright, and at peace with yourself, you feel more distracted, less awake, and less alert.

144
00:16:50,000 --> 00:17:01,000
So, the Buddha said, this is the last one you have to make it. You can't just lie down and say, I'm going to go to sleep and fall asleep.

145
00:17:01,000 --> 00:17:11,000
You have to be strict about it that I'm going to set myself, how long I'm going to sleep, and then when I wake up I'm going to get up and go on with my practice.

146
00:17:11,000 --> 00:17:31,000
The Buddha said, this is the way that a person should deal with drowsiness. So, rather than give my own thoughts on it, or something that's vaguely based on the Buddha's teaching, there you have directly as best as I could translate and paraphrase and expand upon it.

147
00:17:31,000 --> 00:17:45,000
The Buddha's teaching on how to deal with drowsiness. So, thanks for the question, hope that helps.

