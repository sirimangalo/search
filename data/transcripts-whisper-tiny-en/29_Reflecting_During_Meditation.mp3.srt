1
00:00:00,000 --> 00:00:04,000
Reflecting during meditation.

2
00:00:04,000 --> 00:00:05,000
Question.

3
00:00:05,000 --> 00:00:08,000
Sometimes I am facing an issue

4
00:00:08,000 --> 00:00:12,000
and let you to lead to by using my meditation time

5
00:00:12,000 --> 00:00:14,000
to reflect on it.

6
00:00:14,000 --> 00:00:17,000
I try to break it down to clearly understand it

7
00:00:17,000 --> 00:00:22,000
and understand why it is affecting me negatively or positively.

8
00:00:22,000 --> 00:00:24,000
Is this a valid meditation?

9
00:00:24,000 --> 00:00:26,000
If I just repeat,

10
00:00:26,000 --> 00:00:30,000
this liking, the psyche, the issue keeps arising

11
00:00:30,000 --> 00:00:34,000
and it seems like my mind is screaming for an interpretation

12
00:00:34,000 --> 00:00:38,000
compared to a passive observation.

13
00:00:38,000 --> 00:00:42,000
And so, valid meditation does not mean anything

14
00:00:42,000 --> 00:00:45,000
as meditation would mean anything.

15
00:00:45,000 --> 00:00:48,000
Meditation is such a vague word.

16
00:00:48,000 --> 00:00:50,000
Do you mean the cut is meditation?

17
00:00:50,000 --> 00:00:55,000
There are many different kinds of activity for meditation.

18
00:00:55,000 --> 00:00:58,000
Meditating and reflecting on your problems

19
00:00:58,000 --> 00:01:02,000
is very different from what we teach and practice.

20
00:01:02,000 --> 00:01:04,000
If you want an answer like,

21
00:01:04,000 --> 00:01:07,000
what is such a meditation like it leads to?

22
00:01:07,000 --> 00:01:10,000
As it is not particularly based on mindfulness,

23
00:01:10,000 --> 00:01:14,000
I would say that you are more likely to be played by judgements

24
00:01:14,000 --> 00:01:17,000
and reactions when you reflect upon your thoughts.

25
00:01:17,000 --> 00:01:20,000
It can be a use of practice at times,

26
00:01:20,000 --> 00:01:23,000
but it cannot replace mindfulness.

27
00:01:23,000 --> 00:01:27,000
Understand this when we talk about the second part of your question.

28
00:01:27,000 --> 00:01:30,000
You say that when you just repeat the mantra,

29
00:01:30,000 --> 00:01:33,000
your mind is screaming for an interpretation.

30
00:01:33,000 --> 00:01:36,000
This is what we expect to see.

31
00:01:36,000 --> 00:01:39,000
This is not the sign that your practice is going wrong.

32
00:01:39,000 --> 00:01:43,000
This is a sign that reality is not under your control.

33
00:01:43,000 --> 00:01:45,000
When you notice this liking,

34
00:01:45,000 --> 00:01:48,000
disliking and the issue keeps arising,

35
00:01:48,000 --> 00:01:52,000
what you are seeing is that it is not under your control

36
00:01:52,000 --> 00:01:54,000
but you are not in charge,

37
00:01:54,000 --> 00:01:58,000
and that it does not turn off just because you want it to go away.

38
00:01:58,000 --> 00:02:01,000
This is what we are trying to see

39
00:02:01,000 --> 00:02:05,000
if the issue keeps coming back again and again

40
00:02:05,000 --> 00:02:07,000
and you keep staying objective,

41
00:02:07,000 --> 00:02:08,000
or as you say,

42
00:02:08,000 --> 00:02:10,000
passively observing it,

43
00:02:10,000 --> 00:02:13,000
eventually you will be fed up with it saying,

44
00:02:13,000 --> 00:02:18,000
look, this is just making me suffer again and again and again.

45
00:02:18,000 --> 00:02:21,000
It is necessary for our issues

46
00:02:21,000 --> 00:02:23,000
to come up again and again.

47
00:02:23,000 --> 00:02:26,000
We are not trying to get rid of problems.

48
00:02:26,000 --> 00:02:30,000
We are trying to understand them as experiences.

49
00:02:30,000 --> 00:02:34,000
When you understand your experiences clearly enough,

50
00:02:34,000 --> 00:02:38,000
your mind will cease to react in the ways you ordinarily react

51
00:02:38,000 --> 00:02:43,000
because you will see they just cause you suffering again and again.

52
00:02:43,000 --> 00:02:47,000
The unfortunate truth is that to free yourself from something

53
00:02:47,000 --> 00:02:49,000
you really have to go through it,

54
00:02:49,000 --> 00:02:52,000
experiencing it again and again.

55
00:02:52,000 --> 00:02:54,000
There is no quick fix.

56
00:02:54,000 --> 00:02:57,000
When you talk about the more proactive practice of trying

57
00:02:57,000 --> 00:02:59,000
to find out what is wrong,

58
00:02:59,000 --> 00:03:03,000
it just encourages the habit of trying to fix things.

59
00:03:03,000 --> 00:03:06,000
That habit is problematic.

60
00:03:06,000 --> 00:03:09,000
With it, you will never become disenchanted turn away

61
00:03:09,000 --> 00:03:23,000
or let's go because you will always be trying to make things better.

