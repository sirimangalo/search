1
00:00:00,000 --> 00:00:10,960
Hello, so this is the second in my series on why everyone should practice meditation and

2
00:00:10,960 --> 00:00:15,880
this is going to give the number four reason in the top five reasons why everyone should

3
00:00:15,880 --> 00:00:16,880
meditate.

4
00:00:16,880 --> 00:00:24,920
The number four reason is because meditation is something which allows us to do away with

5
00:00:24,920 --> 00:00:34,960
sorrow, lamentation and despair and this includes all kinds of depression and all sorts of

6
00:00:34,960 --> 00:00:42,040
mourning and sadness and any kind of upset in the mind, all of our these mental conditions

7
00:00:42,040 --> 00:00:48,040
which for the most part we are unable to do away with, we try to find the way out to

8
00:00:48,040 --> 00:00:56,120
meditation, we think of it as perhaps an imbalance in the mind or we try to do away with it

9
00:00:56,120 --> 00:01:03,520
through drugs, through alcohol, through entertainment, through diversion and so we find ourselves

10
00:01:03,520 --> 00:01:11,700
really covering over the cause or covering over the experience of the suffering, the

11
00:01:11,700 --> 00:01:16,680
experience of the unpleasantness of the situation and as a result we are never really able

12
00:01:16,680 --> 00:01:26,840
to do away with the unpleasantness, we are never really able to fully overcome these difficult

13
00:01:26,840 --> 00:01:27,840
experiences.

14
00:01:27,840 --> 00:01:32,480
The practice of meditation at the time when we practice and our mind is clear and our mind

15
00:01:32,480 --> 00:01:38,600
is fully aware of the situation as it is occurring and aware of the reality around us.

16
00:01:38,600 --> 00:01:43,160
We come to see that really all of the things which are upsetting us, whether they be

17
00:01:43,160 --> 00:01:51,320
loss or whether they be lack of achievement, not able to attain the things that we want

18
00:01:51,320 --> 00:01:56,320
to attain, not able to get the things that we want to get or being confronted by things

19
00:01:56,320 --> 00:02:01,200
which are unpleasant, being confronted by things which are very difficult to bear or

20
00:02:01,200 --> 00:02:06,600
just overall feelings of sadness and depression.

21
00:02:06,600 --> 00:02:10,440
Actually these things are very easy to overcome at the time when we practice these

22
00:02:10,440 --> 00:02:19,600
things or we can see that they have really no basis in reality, they take all sorts

23
00:02:19,600 --> 00:02:25,720
of conceptual objects, so it is either a person who is passed away or who is left or

24
00:02:25,720 --> 00:02:33,400
who is gone, who is no longer with us or it is a thing or it is a concept of some sort,

25
00:02:33,400 --> 00:02:36,800
something which we create in our mind and actually it is nothing to do with the reality

26
00:02:36,800 --> 00:02:37,800
around us.

27
00:02:37,800 --> 00:02:44,320
At the time when we are mindful, we can see this and we can bring our minds back to

28
00:02:44,320 --> 00:02:48,720
the present moment and we are able very quickly to do away with these things.

29
00:02:48,720 --> 00:02:55,720
People are able to do with great terrible states of stress and depression in a very

30
00:02:55,720 --> 00:02:56,720
short time.

31
00:02:56,720 --> 00:03:04,720
In fact, these are things which take very little time to do away with states of sadness

32
00:03:04,720 --> 00:03:12,520
of mourning, depression, stress or anxiety, fear or worry, even insomnia, many people

33
00:03:12,520 --> 00:03:18,120
come to practice meditation who are unable to sleep adequately or feel like they are not

34
00:03:18,120 --> 00:03:24,280
able to sleep as much or as soundly as they would like and they find that through the

35
00:03:24,280 --> 00:03:27,600
practice of meditation they are very quickly able to overcome this and their mind

36
00:03:27,600 --> 00:03:34,960
relaxes and there is a result they are able to sleep where they are able to relax and

37
00:03:34,960 --> 00:03:38,200
they are able to live their lives peaceful and happily.

38
00:03:38,200 --> 00:03:42,680
So this is something to keep in mind, if there is anyone out there who knows someone

39
00:03:42,680 --> 00:03:47,560
who has a condition where maybe they are taking medication for trying to cure it through

40
00:03:47,560 --> 00:03:53,080
a physical means, that actually there is a cure, a mental cure using the mind as the

41
00:03:53,080 --> 00:04:01,920
cure using the training of the mind trying to bring about a cure on a mental basis and

42
00:04:01,920 --> 00:04:08,560
this is the practice of meditation if you come and you start to use this clear thought

43
00:04:08,560 --> 00:04:11,760
and bring your mind back to the present reality, you start to see that all of these things

44
00:04:11,760 --> 00:04:16,880
really have no meaning, have no basis in reality in the reality that we are experiencing

45
00:04:16,880 --> 00:04:17,880
right now.

46
00:04:17,880 --> 00:04:22,680
It is often something from the past or something in the future and it really has nothing

47
00:04:22,680 --> 00:04:28,840
to do with reality which is the phenomena which arise and which are experienced at every

48
00:04:28,840 --> 00:04:29,840
moment.

49
00:04:29,840 --> 00:04:33,640
So this is number four, the number four reason why everyone should practice meditation.

50
00:04:33,640 --> 00:04:39,280
Thanks for tuning in and look forward to seeing number three in the near or far future.

51
00:04:39,280 --> 00:04:54,240
Thank you.

