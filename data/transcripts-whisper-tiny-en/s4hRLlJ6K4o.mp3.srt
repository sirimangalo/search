1
00:00:00,000 --> 00:00:12,000
So, in regards to God, and in regards to avoiding a confrontation with theists,

2
00:00:12,000 --> 00:00:21,000
I was in Thailand in Deoisu Tap. I think I've told this story before on record.

3
00:00:21,000 --> 00:00:26,000
And I was sitting with one of my students, because we had this thing where we would meet people.

4
00:00:26,000 --> 00:00:32,000
People, visitors would come. I didn't do it so much, but I was trying to, some of my students were doing it,

5
00:00:32,000 --> 00:00:35,000
because I was responsible for teaching meditators.

6
00:00:35,000 --> 00:00:39,000
But visitors would come, and they'd talk to them about Buddhism and give them an introduction.

7
00:00:39,000 --> 00:00:42,000
I answered their questions, like this.

8
00:00:42,000 --> 00:00:47,000
And we got a lot of visitors, because we were at a popular tourist location,

9
00:00:47,000 --> 00:00:51,000
which is part of the reason why I left in the end.

10
00:00:51,000 --> 00:00:56,000
But, you know, it was fruitful, because we helped a lot of people, and so these two missionaries came,

11
00:00:56,000 --> 00:01:00,000
and they said, well, we're looking for information, and so we sat down with them.

12
00:01:00,000 --> 00:01:04,000
And I talked about the Eightfold Noble Path, or something.

13
00:01:12,000 --> 00:01:14,000
Sorry, I'm getting something in Thai.

14
00:01:14,000 --> 00:01:21,000
What but had sizjamtang? What in jamtang?

15
00:01:21,000 --> 00:01:25,000
What in jamtang?

16
00:01:25,000 --> 00:01:27,000
가세요.

17
00:01:27,000 --> 00:01:28,000
가세요.

18
00:01:28,000 --> 00:01:29,000
What?

19
00:01:29,000 --> 00:01:30,000
가세요.

20
00:01:30,000 --> 00:01:31,000
가세요.

21
00:01:31,000 --> 00:01:34,000
Sorry, I'm interrupting a recorded video.

22
00:01:34,000 --> 00:01:37,000
I should go in order, right?

23
00:01:37,000 --> 00:01:40,000
Where was I?

24
00:01:40,000 --> 00:01:44,000
What was I talking about?

25
00:01:44,000 --> 00:01:46,000
Talking about doy suthep?

26
00:01:46,000 --> 00:01:48,000
What but had, doy suthep?

27
00:01:48,000 --> 00:01:50,000
Here we were with these missionaries,

28
00:01:50,000 --> 00:01:55,000
told them, talked about the Eightfold Noble Path.

29
00:01:55,000 --> 00:01:59,000
And then, you know, I was just giving them the basics of Buddhism,

30
00:01:59,000 --> 00:02:03,000
and then one of them said to me, well, you know, what you say is interesting,

31
00:02:03,000 --> 00:02:09,000
but I still believe that Jesus Christ is the only path to God.

32
00:02:09,000 --> 00:02:12,000
And I said, I looked at him, and I said,

33
00:02:12,000 --> 00:02:15,000
I said, you're absolutely right.

34
00:02:15,000 --> 00:02:18,000
In my students, she's sitting in the chair next to me.

35
00:02:18,000 --> 00:02:20,000
She goes like this.

36
00:02:20,000 --> 00:02:22,000
I didn't blink.

37
00:02:22,000 --> 00:02:24,000
I looked at him, and I said,

38
00:02:24,000 --> 00:02:27,000
but you have to understand what is God.

39
00:02:27,000 --> 00:02:33,000
And you have to understand what is Jesus Christ.

40
00:02:33,000 --> 00:02:36,000
What is God? God is love.

41
00:02:36,000 --> 00:02:41,000
And there's a saying in the Bible that God is love.

42
00:02:41,000 --> 00:02:43,000
So what is the path to love?

43
00:02:43,000 --> 00:02:48,000
If you believe that God is some guy with a beard sitting up on a throne

44
00:02:48,000 --> 00:02:50,000
and heaven, you're wrong.

45
00:02:50,000 --> 00:02:52,000
The Bible says it itself.

46
00:02:52,000 --> 00:02:54,000
God is love.

47
00:02:54,000 --> 00:02:56,000
And I said, I actually extrapolated.

48
00:02:56,000 --> 00:02:58,000
I said, God is love.

49
00:02:58,000 --> 00:02:59,000
God is compassion.

50
00:02:59,000 --> 00:03:01,000
God is joy.

51
00:03:01,000 --> 00:03:03,000
And God is equanimity.

52
00:03:03,000 --> 00:03:05,000
This is what God is.

53
00:03:05,000 --> 00:03:09,000
And if you think that Jesus was a man who was born on earth to parents

54
00:03:09,000 --> 00:03:15,000
and lived as a Jew, then you're also wrong.

55
00:03:15,000 --> 00:03:19,000
Jesus Christ is the path to God.

56
00:03:19,000 --> 00:03:21,000
And what is the path to these things?

57
00:03:21,000 --> 00:03:25,000
Well, the path to these things is actually love, compassion, joy,

58
00:03:25,000 --> 00:03:26,000
and equanimity.

59
00:03:26,000 --> 00:03:28,000
And this is what Jesus Christ was.

60
00:03:28,000 --> 00:03:30,000
They didn't really buy it.

61
00:03:30,000 --> 00:03:33,000
But it was anything to say, I think,

62
00:03:33,000 --> 00:03:34,000
because it's very Buddhist.

63
00:03:34,000 --> 00:03:37,000
And if you learn anything about Brahman in Buddhism,

64
00:03:37,000 --> 00:03:40,000
you learn about the Brahman Viharas.

65
00:03:40,000 --> 00:03:43,000
There's a couple of good suitors,

66
00:03:43,000 --> 00:03:46,000
but in general, the talking about the Brahman Viharas

67
00:03:46,000 --> 00:03:50,000
are the dwellings of Brahman or God.

68
00:03:50,000 --> 00:03:53,000
And these are the four Brahman Viharas.

69
00:03:53,000 --> 00:03:57,000
Love, compassion, joy, and equanimity.

70
00:03:57,000 --> 00:04:02,000
So that's an example of how we avoid the confrontation with them.

71
00:04:02,000 --> 00:04:06,000
Just find a way to agree with them.

72
00:04:06,000 --> 00:04:07,000
Or twist it.

73
00:04:07,000 --> 00:04:10,000
I mean, the Buddha was all about this.

74
00:04:10,000 --> 00:04:12,000
He talked about how he's a real Brahman.

75
00:04:12,000 --> 00:04:14,000
He's a real shaman.

76
00:04:14,000 --> 00:04:19,000
How, in so many ways, he was able to twist things around

77
00:04:19,000 --> 00:04:23,000
and show people how it was their own misunderstanding.

78
00:04:23,000 --> 00:04:30,000
Because the path from theism to atheism

79
00:04:30,000 --> 00:04:35,000
is a path from assumption to understanding.

80
00:04:35,000 --> 00:04:38,000
So theists have theories about reality.

81
00:04:38,000 --> 00:04:40,000
And when they say the word God,

82
00:04:40,000 --> 00:04:43,000
they're actually talking about reality.

83
00:04:43,000 --> 00:04:45,000
They just misunderstand.

84
00:04:45,000 --> 00:04:47,000
They have the misunderstanding,

85
00:04:47,000 --> 00:04:50,000
interpretation and extrapolation.

86
00:04:50,000 --> 00:04:52,000
And it's just a path.

87
00:04:52,000 --> 00:04:55,000
If they can get from there to a clear understanding

88
00:04:55,000 --> 00:04:57,000
of what they're talking about,

89
00:04:57,000 --> 00:05:02,000
they'll be, I guess they'll be atheist.

90
00:05:02,000 --> 00:05:04,000
I mean, you don't even have to say it like that,

91
00:05:04,000 --> 00:05:06,000
but they'll be at the truth.

92
00:05:06,000 --> 00:05:09,000
And whether you call it God or not,

93
00:05:09,000 --> 00:05:10,000
it's just reality.

94
00:05:10,000 --> 00:05:35,000
It's the experience of the world around us.

