1
00:00:00,000 --> 00:00:11,880
Good evening, everyone, broadcasting live October 26th, 2015.

2
00:00:11,880 --> 00:00:18,840
Tonight, we're not going to do the Dhammapadna, it's feeling a little bit under the weather.

3
00:00:18,840 --> 00:00:25,520
I don't think I'm sick, but I didn't get around to looking at the Dhammapadna and probably

4
00:00:25,520 --> 00:00:27,720
going to call it an early night.

5
00:00:27,720 --> 00:00:33,760
And there's a couple of people visiting here and downstairs, so yeah, nothing tonight.

6
00:00:33,760 --> 00:00:41,680
And tomorrow night as well, tomorrow is the full moon, so officially end of the rains.

7
00:00:41,680 --> 00:00:50,480
We enter into the cold season, officially, and tomorrow evening, I will be at Stony Creek

8
00:00:50,480 --> 00:00:58,320
doing an official ceremony with the other monks there to leave the rains.

9
00:00:58,320 --> 00:01:02,920
It's not even a ceremony to leave the rains, it's just we ask forgiveness from each

10
00:01:02,920 --> 00:01:03,920
other, basically.

11
00:01:03,920 --> 00:01:12,520
We give a permission to the others to criticize us, it's kind of a nice ceremony.

12
00:01:12,520 --> 00:01:18,120
Maybe I'll take them up on it and start criticizing them, something you never do.

13
00:01:18,120 --> 00:01:22,640
No one ever actually goes ahead and criticize us, but I think I'll maybe joke about it

14
00:01:22,640 --> 00:01:26,320
or something with them.

15
00:01:26,320 --> 00:01:31,120
Start telling them all their faults, we're talking all this talk about criticism.

16
00:01:31,120 --> 00:01:36,400
Tomorrow is the one day of the year where it's open season on everybody, you're supposed

17
00:01:36,400 --> 00:01:40,520
to be allowed to criticize each other, because you invite each other to criticize.

18
00:01:40,520 --> 00:01:45,720
But no, they're good guys, there's nothing.

19
00:01:45,720 --> 00:01:55,720
We'll take some questions, I don't think there's that many, we've got a few here.

20
00:01:55,720 --> 00:02:07,040
I have a question on the, you know, where you ask forgiveness, is it actually, what

21
00:02:07,040 --> 00:02:19,760
you're using open speakers as well?

22
00:02:19,760 --> 00:02:26,320
That's right, I didn't really catch what you said, but I need to have it on because I need

23
00:02:26,320 --> 00:02:28,800
to record your asking.

24
00:02:28,800 --> 00:02:38,960
Yeah, I'm just wondering what the intention is when you're asking for, when you're

25
00:02:38,960 --> 00:02:44,480
asking for, when you're asking for, when you're asking for, you know, what is the intention

26
00:02:44,480 --> 00:02:45,480
that that?

27
00:02:45,480 --> 00:02:53,840
Well, to end, to end the reins on a good note so that there's no hard feelings and no

28
00:02:53,840 --> 00:03:03,480
bad resentment, just to be accountable to each other and to clear the air, because you've

29
00:03:03,480 --> 00:03:08,360
spent three months together, theoretically, I actually haven't, I've spent a lot of

30
00:03:08,360 --> 00:03:15,360
it away, but having spent three months together usually, a lot of stuff can happen, you

31
00:03:15,360 --> 00:03:24,160
know, you can get, you can build up animosity, so in the end, you invite each other to

32
00:03:24,160 --> 00:03:28,680
clear the air, you accept other people, the problems other people have had, you listen

33
00:03:28,680 --> 00:03:50,200
and you accept, and let it go.

34
00:03:50,200 --> 00:04:08,760
You can, I think you can get hangouts for iOS, so you don't use Chrome, you'll use hangouts,

35
00:04:08,760 --> 00:04:14,720
Google hangouts, so as long as they have that for Chrome, which I think there is, and

36
00:04:14,720 --> 00:04:28,720
I think that's a good idea, but I didn't talk to her, so that worked out fine.

37
00:04:28,720 --> 00:04:57,840
It's not normal, but we're not concerned so much about normal, it's real, and that's all

38
00:04:57,840 --> 00:05:03,480
that's important, so it's a sound, and you should say hearing hearing, there's not really

39
00:05:03,480 --> 00:05:07,320
a normal, you know, what would that mean?

40
00:05:07,320 --> 00:05:14,200
Everyone gets it, everyone has different conditions, and so everyone will ask that sort of

41
00:05:14,200 --> 00:05:17,960
question, is this normal, there really is no normal, there's you, and there's what you're

42
00:05:17,960 --> 00:05:25,320
experiencing, so it doesn't matter whether it's normal, a normal, abnormal is just a

43
00:05:25,320 --> 00:05:33,600
judgment call, when you hear, you should say hearing, hearing, just let it go, knowledge

44
00:05:33,600 --> 00:05:37,840
it for some time, if it doesn't go away, then just ignore it, and then if it drags your

45
00:05:37,840 --> 00:05:45,400
attention back later, you can always go back and acknowledge it again.

46
00:05:45,400 --> 00:05:50,600
If you dislike it, or you like it, or whatever, worry about it afraid of it, should acknowledge

47
00:05:50,600 --> 00:05:56,520
all of that as well.

48
00:05:56,520 --> 00:06:01,720
Could the monks who live in the forest would be angry animals attack?

49
00:06:01,720 --> 00:06:10,520
Yeah, pretty obvious, isn't it?

50
00:06:10,520 --> 00:06:18,640
I guess, is there more to that, because the monk, since the monks, their state of mind

51
00:06:18,640 --> 00:06:24,840
is more calm, so they're less liable to be attacked by animals, but it certainly happens.

52
00:06:24,840 --> 00:06:41,200
Crushed by an elephant, gored by a wild pig, eaten by a lion or a tiger, killed by mosquitoes,

53
00:06:41,200 --> 00:06:50,120
yeah, a lot of death by mosquitoes.

54
00:06:50,120 --> 00:06:57,840
There aren't that many forest monks, but there's also not that many wild animals anymore,

55
00:06:57,840 --> 00:07:06,120
so depends which countries, Sri Lanka still had some, but snakes would probably be mosquitoes,

56
00:07:06,120 --> 00:07:12,280
no, I don't know about death from mosquitoes, but yeah, there's probably death by mosquitoes,

57
00:07:12,280 --> 00:07:17,560
death by snake by it, and death by disease, probably the worst.

58
00:07:17,560 --> 00:07:24,240
There's a lot of bacteria in the rainforests, and maybe not the most, but it is up there,

59
00:07:24,240 --> 00:07:38,120
and I can infection and fungus, and just really awful stuff in the rainforests.

60
00:07:38,120 --> 00:07:51,120
One day at T

61
00:07:51,120 --> 00:07:56,560
and study groups, since they go to the list of the participants,

62
00:07:56,560 --> 00:07:59,120
how do we go through that and not let them know?

63
00:07:59,120 --> 00:08:05,600
Is this a group that members are themselves and do not adhere to a year or two or so?

64
00:08:05,600 --> 00:08:11,040
Also, what if people would be able to bring the live stream live stream public so they can listen to them

65
00:08:11,040 --> 00:08:15,680
or in their lives and thank you, my thank you, my thank you, my thank you.

66
00:08:15,680 --> 00:08:23,680
I don't really think we want people from other traditions coming to meditate here.

67
00:08:23,680 --> 00:08:29,680
It really should be our tradition. That's my feeling.

68
00:08:29,680 --> 00:08:35,680
I think I'd rather stick to our tradition.

69
00:08:35,680 --> 00:08:41,680
But if they want to listen to live stream, that's absolutely welcome to.

70
00:08:41,680 --> 00:08:49,680
I mean, we haven't required it, but I think we'd encourage that if you're coming here, you're practicing our tradition.

71
00:08:49,680 --> 00:08:53,680
That seems, I don't see a reason to go outside of that.

72
00:08:57,680 --> 00:09:01,680
Because it will just get weird otherwise, especially in the chat box.

73
00:09:01,680 --> 00:09:09,680
Yeah.

74
00:09:09,680 --> 00:09:15,680
The students who are watching, the students who are watching.

75
00:09:15,680 --> 00:09:21,680
Next tradition is not weird stuff out of the pattern.

76
00:09:21,680 --> 00:09:23,680
Can you talk about it?

77
00:09:23,680 --> 00:09:27,680
Yeah.

78
00:09:27,680 --> 00:09:29,680
Sorry, I gotta go.

79
00:09:29,680 --> 00:09:33,680
I just came on to say hello tonight.

80
00:09:33,680 --> 00:09:35,680
Thank you, bye.

81
00:09:35,680 --> 00:09:36,680
Thank you, bye.

82
00:09:36,680 --> 00:09:38,680
Yeah, sorry, guys.

83
00:09:38,680 --> 00:09:41,680
Thanks for showing up. Thanks for meditating with us.

84
00:09:41,680 --> 00:09:43,680
Keep up the good work.

85
00:09:43,680 --> 00:09:47,680
Darwin, I'll try tomorrow, maybe tomorrow.

86
00:09:47,680 --> 00:09:49,680
You have a session, right?

87
00:09:49,680 --> 00:09:53,680
The time is that.

88
00:09:53,680 --> 00:09:59,680
Everyone go to Darwin session in second life.

89
00:10:07,680 --> 00:10:13,680
If you can post it in the chat, there are a lot of snow. I'll try to make it.

90
00:10:13,680 --> 00:10:23,680
Okay, anyway, good night, everyone.

