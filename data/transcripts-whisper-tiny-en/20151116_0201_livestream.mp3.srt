1
00:00:00,000 --> 00:00:20,000
Hello, evening, broadcasting while November 15, 2015.

2
00:00:20,000 --> 00:00:32,000
Hello, everyone.

3
00:00:32,000 --> 00:00:34,000
Whoever is easy to speak to because of respecting,

4
00:00:34,000 --> 00:00:37,000
we're veering and honoring the government.

5
00:00:37,000 --> 00:01:05,000
These features gracious and I call them easy to speak to.

6
00:01:05,000 --> 00:01:22,000
What was that Dante, was that the call?

7
00:01:22,000 --> 00:01:28,000
Gentle kind, humble, sorry, sorry, I mean, humility.

8
00:01:28,000 --> 00:01:34,000
Sorech is an interesting word, it's one we don't hear about often enough.

9
00:01:34,000 --> 00:01:41,000
But there are two, the Buddha said there are two dhammas that make one beautiful.

10
00:01:41,000 --> 00:01:52,000
And Sorech, thamtham, taittham, hyen, we don't listen in first year,

11
00:01:52,000 --> 00:01:58,000
Dhammas studies in Thailand.

12
00:01:58,000 --> 00:02:02,000
Thamthi mi ubakara, ma'ak, the dhammas that are of great support, sati kaht,

13
00:02:02,000 --> 00:02:08,000
sampajanya, mindfulness and clear comprehension.

14
00:02:08,000 --> 00:02:11,520
Tham-ti-tham hai, no, that's not over a second.

15
00:02:11,520 --> 00:02:15,280
Second one, pukon-ti-ha-da-ya,

16
00:02:15,280 --> 00:02:17,360
people who are hard to find in the world.

17
00:02:24,400 --> 00:02:30,000
Pubakavi, the one who does good deeds to others, unprompted.

18
00:02:30,000 --> 00:02:36,400
So who has done, which has does good deeds without having any reason to

19
00:02:36,400 --> 00:02:38,800
win, without being owed.

20
00:02:38,800 --> 00:02:43,040
It does good deeds for others without being owed them.

21
00:02:43,040 --> 00:02:48,960
And Katan-yukatawidi, the one who knows the good deeds to others have done

22
00:02:48,960 --> 00:02:53,120
and tries to pay them back.

23
00:02:54,240 --> 00:02:59,680
And then there's four, the third one.

24
00:02:59,680 --> 00:03:05,200
Tham-ti-tham-ti-kum-kong-lo,

25
00:03:05,200 --> 00:03:11,360
Tham-ti-lulai, the dumbers that protect the world.

26
00:03:13,360 --> 00:03:21,040
Thir-ti-kum-od-pa, the fear of shame of wrongdoing

27
00:03:21,040 --> 00:03:24,080
and fear of the results of wrongdoing.

28
00:03:24,080 --> 00:03:28,320
And number four, Tham-ti-tham hai-na-ya,

29
00:03:30,160 --> 00:03:34,400
and Katan-ti-kum-so-ra-ta, Katan-ti-ya's patience.

30
00:03:35,360 --> 00:03:38,960
And Sore-ra-ja, Sore-ra-ja, means humility.

31
00:03:40,560 --> 00:03:44,000
Clumsinium, they call, I don't really know what Samyam is,

32
00:03:44,000 --> 00:03:46,320
but that's how the Thai people try and say that.

33
00:03:46,320 --> 00:03:53,360
And Samyam is kind of like a meekness, I think.

34
00:03:55,360 --> 00:03:59,120
Gen-to-thi, being gentle.

35
00:04:04,800 --> 00:04:10,240
Sore-ra-ta-taw-ti-kum-ni-wa-ta, ni-wa-ta, ni-wa-ta, ni-wa-ta, ni-wa-ta is an interesting word.

36
00:04:10,240 --> 00:04:17,120
And wa-ta means wind, wa-ta is like,

37
00:04:17,120 --> 00:04:23,680
because wa-ta, wa-ta means wind, ni-wa-ta means having the wind out,

38
00:04:25,440 --> 00:04:29,920
or having, with wind removed, kind of thing, like ni-bana,

39
00:04:29,920 --> 00:04:33,680
or having unbound, so ni-wa-ta is unwinded.

40
00:04:33,680 --> 00:04:41,920
And what it means is, not being puffed up,

41
00:04:41,920 --> 00:04:44,160
because that's humility.

42
00:04:49,280 --> 00:04:53,520
Without going on, that's funny that the bilingual

43
00:04:53,520 --> 00:04:55,920
dictionary just says, with the wind gone down,

44
00:04:55,920 --> 00:05:00,880
it doesn't make mention of the fact that it's often used to talk about a person

45
00:05:00,880 --> 00:05:06,560
that's pain, without, without being off, not being off of them.

46
00:05:08,720 --> 00:05:11,040
Wupa-san-taw-pa-san-taw-taw-ti.

47
00:05:17,360 --> 00:05:22,800
Wupa-san-taw means calm, or at peace, wupa-pa means

48
00:05:22,800 --> 00:05:28,880
a, gone to a fully, really.

49
00:05:32,880 --> 00:05:34,880
And santa comes from some,

50
00:05:35,920 --> 00:05:39,680
what it means to, to be quiet or peaceful.

51
00:05:39,680 --> 00:05:42,640
Santa-san-taw means wupa-san-taw,

52
00:05:42,640 --> 00:05:55,280
with someone who is gone to peace, become peaceful.

53
00:06:01,920 --> 00:06:06,000
Such a person who is these things when,

54
00:06:06,000 --> 00:06:10,880
and, yeah, when the makna, no way.

55
00:06:10,880 --> 00:06:40,240
And they are touched, interestsful indeed.

56
00:06:40,240 --> 00:06:45,580
It's interesting, I think what he's saying is they're only these, some people are only

57
00:06:45,580 --> 00:06:51,920
this way when they're not touched, y'all are not a mana, what's a mana, a tab, who's

58
00:06:51,920 --> 00:06:52,920
something.

59
00:06:52,920 --> 00:07:00,560
They're only this way, in so long as they are not touched by speech that is under

60
00:07:00,560 --> 00:07:06,320
speech that is unendearing, so harsh speech.

61
00:07:06,320 --> 00:07:30,000
And I don't consider them to be easy to talk to, because sometimes you have to talk to

62
00:07:30,000 --> 00:07:33,840
people about things that they don't want to hear, and if they get to take it, then they're

63
00:07:33,840 --> 00:07:35,880
not easy to talk to.

64
00:07:35,880 --> 00:07:45,240
It's being easy to talk to, it's important, it's in the mingle, it's a sugato, it's an important

65
00:07:45,240 --> 00:07:55,800
quality of students, it's an important quality of a meditator, a practitioner, right, so

66
00:07:55,800 --> 00:08:03,360
this is why when people accuse us or attack us, we have to remember the teaching, and that's

67
00:08:03,360 --> 00:08:18,920
what we have, not good, fed up, is it me as a mind blurring, is my picture of a blurring

68
00:08:18,920 --> 00:08:19,920
to your oven?

69
00:08:19,920 --> 00:08:24,640
A little blurring, where you going?

70
00:08:24,640 --> 00:08:29,680
Are you too far back?

71
00:08:29,680 --> 00:08:36,680
I shouldn't be, I can't focus on it, maybe it's because down here is not blurring, maybe

72
00:08:36,680 --> 00:08:42,680
it's focusing on your mouth, on the floor there or something, because that's closer,

73
00:08:42,680 --> 00:08:58,480
maybe it's just, maybe it's just failing to do its job, I'm not bad, I'm not bad, I'm

74
00:08:58,480 --> 00:09:05,480
not bad, so you know, this is a kind of exciting thing that you're going to handle,

75
00:09:05,480 --> 00:09:13,480
you're going to handle it, you're going to handle it, and if you do, you're going to

76
00:09:13,480 --> 00:09:20,480
handle it, and that would be not understanding, so.

77
00:09:20,480 --> 00:09:27,480
On the bright side, I think the audio is coming in well, as far as I can tell.

78
00:09:27,480 --> 00:09:33,480
We're going to consider a big, big opportunity for us to test ourselves.

79
00:09:33,480 --> 00:09:37,480
Oh yes, it's easy to get from success.

80
00:09:37,480 --> 00:09:45,480
Yes, they don't work well for two days in a row, there's always something.

81
00:09:45,480 --> 00:09:51,480
It's even worse when you can't really, you know, do much with your own computer, and you have

82
00:09:51,480 --> 00:10:03,480
to depend on other people, that's the worst.

83
00:10:03,480 --> 00:10:09,480
So, yes.

84
00:10:09,480 --> 00:10:12,480
What does it mean when you know, for example, rising?

85
00:10:12,480 --> 00:10:21,480
Do you mean is rising as a word, or as a verb, or you mean rising as happiness as a noun?

86
00:10:21,480 --> 00:10:26,480
Similar thing with feelings, when you know happy, does it refer to me as I am happy?

87
00:10:26,480 --> 00:10:32,480
It doesn't go with the non-self concept, or to the feeling as there is now happy feeling

88
00:10:32,480 --> 00:10:33,480
in me.

89
00:10:33,480 --> 00:10:38,480
So my question is, what is the meaning of the English labels used to a noting?

90
00:10:38,480 --> 00:10:43,480
Please explain that, I meditate 20 to 40 minutes a day, but not on web.

91
00:10:43,480 --> 00:10:44,480
So answer, please.

92
00:10:44,480 --> 00:10:45,480
Thanks.

93
00:10:45,480 --> 00:10:50,480
Okay, this, first of all, I want to address this, doesn't go to the non-self concept.

94
00:10:50,480 --> 00:10:58,480
I probably should not put this too much on that, but this is an example of this whole idea of

95
00:10:58,480 --> 00:11:02,480
non-self, meaning there is no self.

96
00:11:02,480 --> 00:11:09,480
The Buddha didn't ever say there is a self, didn't give us any indication to believe that there is a self.

97
00:11:09,480 --> 00:11:16,480
We can sort of wrong view to think that something exists from one moment to the next,

98
00:11:16,480 --> 00:11:23,480
that something continues on from one life to the next, that it's our spirit, that it's reborn.

99
00:11:23,480 --> 00:11:26,480
But he never said there is no self.

100
00:11:26,480 --> 00:11:32,480
When asked point blank, he refused to answer, and there's a reason for it because it confuses you,

101
00:11:32,480 --> 00:11:39,480
and you get these kind of semantic problems where you say, I can't say I am happy because that doesn't go as non-self.

102
00:11:39,480 --> 00:11:41,480
That's not understanding non-self.

103
00:11:41,480 --> 00:11:46,480
Non-self is when your camera doesn't focus on you and you get angry.

104
00:11:46,480 --> 00:11:49,480
You get angry because you don't understand that it's not self.

105
00:11:49,480 --> 00:11:52,480
You can't under your control, it doesn't belong to you.

106
00:11:52,480 --> 00:11:56,480
There's no sense of the universe working according to your command.

107
00:11:56,480 --> 00:11:59,480
That's just not the way the universe works.

108
00:11:59,480 --> 00:12:02,480
It doesn't mean that I don't exist.

109
00:12:02,480 --> 00:12:03,480
That's not what it means.

110
00:12:03,480 --> 00:12:05,480
It's not that we're denying that.

111
00:12:05,480 --> 00:12:07,480
It's not what we're saying, I do exist.

112
00:12:07,480 --> 00:12:09,480
We're not saying that either.

113
00:12:09,480 --> 00:12:17,480
But you have to understand the context and the direction in which we meant to understand non-self.

114
00:12:17,480 --> 00:12:22,480
That sounds like a attacking image.

115
00:12:22,480 --> 00:12:29,480
It's a very common misconception that we have to clear out because it's not how we should be thinking.

116
00:12:29,480 --> 00:12:32,480
We shouldn't be thinking in terms of I exist, I don't exist.

117
00:12:32,480 --> 00:12:37,480
That's very confusing because of course I exist.

118
00:12:37,480 --> 00:12:41,480
An ultimate reality that question doesn't even make sense.

119
00:12:41,480 --> 00:12:48,480
The idea of self doesn't make sense.

120
00:12:48,480 --> 00:12:54,480
It doesn't have any place in a conversation about ultimate reality.

121
00:12:54,480 --> 00:13:00,480
So when you say I like it, it's just a convention where you're saying what I just want.

122
00:13:00,480 --> 00:13:02,480
It's like you want me to work.

123
00:13:02,480 --> 00:13:06,480
I'm not just saying this myself, the Buddha himself said,

124
00:13:06,480 --> 00:13:12,480
the temple where the tami can be done.

125
00:13:12,480 --> 00:13:17,480
When walking, I lose, I walk.

126
00:13:17,480 --> 00:13:21,480
You see, no, he means, I'm here.

127
00:13:21,480 --> 00:13:24,480
He means I am the first person.

128
00:13:24,480 --> 00:13:27,480
You see, no, sad.

129
00:13:27,480 --> 00:13:33,480
I am sad, just how you say, I'm sad.

130
00:13:33,480 --> 00:13:36,480
The Buddha used to be in I.

131
00:13:36,480 --> 00:13:42,480
Na Hana and Bakave, I don't speak of the Buddha.

132
00:13:42,480 --> 00:13:45,480
A hundred.

133
00:13:45,480 --> 00:13:46,480
A hundred.

134
00:13:46,480 --> 00:13:47,480
A hundred.

135
00:13:47,480 --> 00:13:48,480
A hundred.

136
00:13:48,480 --> 00:13:49,480
What do you want?

137
00:13:49,480 --> 00:13:50,480
I say this.

138
00:13:50,480 --> 00:13:55,480
That's not your question.

139
00:13:55,480 --> 00:13:59,480
But, it does relate to the question in terms of words.

140
00:13:59,480 --> 00:14:02,480
It's just being words.

141
00:14:02,480 --> 00:14:06,880
that's not really the most important point here. We were just talking this morning

142
00:14:06,880 --> 00:14:14,560
or noon or one, but we did the Risudimaga today. I was pointing this out. You said the mantra

143
00:14:14,560 --> 00:14:21,120
of how ancient it is. It really is very much a part of the practice. And so it's

144
00:14:21,120 --> 00:14:25,120
important to use the mantra. It's something, it's not a new concept that we just thought

145
00:14:25,120 --> 00:14:30,160
out. It's not something that Mahasi signed off that out. It's what you get when you read

146
00:14:30,160 --> 00:14:36,960
the ancient texts. It's how they practice. In the time of the Risudimaga at least, it's over 1500 years old.

147
00:14:42,400 --> 00:14:51,120
So, but it's the actual mantra that you use, it's not so important. Whether it's a verb or

148
00:14:51,120 --> 00:14:56,560
noun, for example, it has to relate and it should relate quite closely to the experience.

149
00:14:56,560 --> 00:15:04,080
And that's all that's important. How close it relates and how it relates to reality.

150
00:15:05,760 --> 00:15:11,600
So, you say, I am walking and you're focused on the eye. Well, that's what I'm saying. I am walking.

151
00:15:11,600 --> 00:15:18,560
I am walking. I mean, it's so that puts yourself on your feet, not someone else's feet.

152
00:15:19,760 --> 00:15:24,480
I am walking. That's useful. In English, it's not so useful because it makes a separate

153
00:15:24,480 --> 00:15:30,000
brain. It's separate words. But in certain languages, I am walking. It's one word. It took a

154
00:15:30,000 --> 00:15:34,960
chow me. I go. So, then it makes perfect sense to use it. The booty is done. So,

155
00:15:35,840 --> 00:15:37,680
I'm going to just can't criticize him.

156
00:15:41,040 --> 00:15:43,120
Glad to hear your meditating every day. That's great.

157
00:15:43,120 --> 00:15:53,360
Yeah, Rob, and I think you're echoing me. That's not to be on your end.

158
00:15:55,200 --> 00:15:58,480
So, I don't know what this is. Again, more than a term.

159
00:16:00,240 --> 00:16:03,200
So, maybe turn your speakers out. Okay.

160
00:16:03,200 --> 00:16:06,880
If it's quiet, it shouldn't be a problem. It's too loud, problem.

161
00:16:06,880 --> 00:16:11,280
I'm going to head so I don't be right back.

162
00:16:14,960 --> 00:16:16,000
What about the question?

163
00:16:26,000 --> 00:16:27,680
She's actually alone with all of you.

164
00:16:27,680 --> 00:16:36,960
Hello, everyone. Who do we have here today? We have someone named Amadou, who is orange.

165
00:16:38,160 --> 00:16:44,560
We have someone called Amy Amy, who is also orange. I don't know why there are those people I don't

166
00:16:44,560 --> 00:16:49,440
think. I don't know those things. Hey, man, that sounds familiar. That's his Sri Lankan name.

167
00:16:50,160 --> 00:16:53,520
Joshua, I know he was busy. He visited here. He gave us some paint.

168
00:16:53,520 --> 00:17:04,320
Thanks for the paint, Joshua. But now that I'm out here, I've lost this strong desire to paint

169
00:17:04,320 --> 00:17:11,280
my room. I have to see where that goes. Why? Because it's an ordeal to move all the computer stuff

170
00:17:11,280 --> 00:17:18,880
in the desk. Matthew S, I don't know. Maybe I do, but it doesn't ring a bell.

171
00:17:18,880 --> 00:17:22,800
Then we have a word that I can't pronounce.

172
00:17:22,800 --> 00:17:32,160
Green, king, shani, friend of the lattice. Tina, I think I know Tina, haitina, can't have

173
00:17:33,920 --> 00:17:41,920
Anna Figueroa. She's an RBCD Madhu class, right?

174
00:17:41,920 --> 00:17:48,000
And in our volunteer group and coordinating the translations, the outstanding translations.

175
00:17:48,000 --> 00:17:53,920
Right. She's a volunteer. Way to go ahead. Yes. Yes. Aurora is an old meditator.

176
00:17:53,920 --> 00:18:00,720
She's going to Burma. Wow. She's got a Burma this month. I don't know if she wants me to tell everyone,

177
00:18:00,720 --> 00:18:06,560
but it's pretty awesome. She's going to Paditarana, so we can all say sad and

178
00:18:06,560 --> 00:18:12,000
I'm no more than that of her. I do. That's wonderful. I'm no more than a

179
00:18:13,680 --> 00:18:19,440
For now. Sorry. Go ahead. Oh, Aurora and Fernando are an RBCD Maga group,

180
00:18:21,920 --> 00:18:26,560
which has been around for, I think we've been around for about nine, 10 months now.

181
00:18:26,560 --> 00:18:38,320
Quite a while. And Fernando's in our group, he's also one of our weekly meditators.

182
00:18:39,280 --> 00:18:46,800
Kevin, I think, I don't know, first name is Tina. Patrick, oh, I probably know all these people.

183
00:18:47,920 --> 00:18:52,320
Someone named Robine, who's that? No idea. I was like,

184
00:18:52,320 --> 00:19:03,840
middle eastern name, Robine, from Dubai or something. Say hi. He's from Sri Lanka. He's visited me

185
00:19:03,840 --> 00:19:07,440
several times. They'll be coming here in his wife. We'll be coming next week, I think.

186
00:19:10,080 --> 00:19:13,840
Simon, Simon, I think is an old Simon. He's been around for a while.

187
00:19:15,360 --> 00:19:18,640
Simon is in Denmark, I think. I know that Simon

188
00:19:18,640 --> 00:19:24,400
He's been around for a while, I think. Yeah, Jim and Wayne. Those are our two

189
00:19:27,440 --> 00:19:29,440
There are two

190
00:19:34,400 --> 00:19:40,800
High level. How do you say the higher up? Senior meditators.

191
00:19:42,400 --> 00:19:44,320
Senior because they're all

192
00:19:44,320 --> 00:19:51,200
right. Tim's been around from the very beginning. Tim and Wayne are awesome. I got the right

193
00:19:51,200 --> 00:20:03,120
right Wayne and that's Tim and the other one. No, Wayne and Tim, yeah. They they meet one

194
00:20:03,120 --> 00:20:10,000
after the other. So I get to talk about them. They're doing a great job. There's that

195
00:20:10,000 --> 00:20:17,440
with the damn old guy. I'm going to break. And I got Michael, don't know Michael. Do I know Mike?

196
00:20:18,800 --> 00:20:22,720
And us fans don't know that, but trust that's been here for a while, I think.

197
00:20:23,840 --> 00:20:31,040
Ask questions. So by the, is one of my meditators getting into which one that's, is that some

198
00:20:31,040 --> 00:20:39,920
month? I know. No, Sabadi is in Thailand, I believe. Oh, no, I'm sorry. I'm, I'm wrong. New Zealand.

199
00:20:44,000 --> 00:20:47,520
Okay, remember that, but I think I just talked to them this morning or yesterday. I thought it was

200
00:20:47,520 --> 00:20:53,840
the month. No, Sabadi, I believe is a woman hand from New Zealand.

201
00:20:53,840 --> 00:21:02,160
Yeah. Oh, especially. Sorry. I'm confusing. I do. Also, I didn't talk this the month. I'm sorry.

202
00:21:02,720 --> 00:21:08,480
I thought this is especially confusing. Oh, Samantha is also on our list.

203
00:21:09,680 --> 00:21:11,600
Yes, she. Mr. Twice. No.

204
00:21:13,280 --> 00:21:20,800
Yes, she signed up for a meeting spot. I'm sorry. Yesterday was yesterday was, they came to get me

205
00:21:20,800 --> 00:21:29,200
early earlier than I expected because we had to be in Toronto by 10. But, you know, they got me

206
00:21:29,200 --> 00:21:36,640
really early. So I ended up leaving before. And then, yeah, sorry, such a kind, I confused you

207
00:21:36,640 --> 00:21:51,920
as someone to Sri Lankan or in Sri Lankan national. So I can warn people. I knew where I just

208
00:21:54,320 --> 00:22:00,400
can see the names, you know, the names and the faces trying to keep track of them. So

209
00:22:00,400 --> 00:22:09,360
Samosanti is Sharon's wife. So they too will be coming next week. And I think I'm leaving.

210
00:22:11,520 --> 00:22:12,240
Very nice.

211
00:22:16,240 --> 00:22:21,680
Sabadi is Sashika. She clarified in in chat.

212
00:22:21,680 --> 00:22:29,920
I think we have a question.

213
00:22:31,520 --> 00:22:35,360
One day, lately, I've been doing meditation for about two to three days a week.

214
00:22:38,480 --> 00:22:39,200
Oh, I'm sorry.

215
00:22:41,760 --> 00:22:48,960
I apologize for Islam. Our monks mostly at least so tapana plus level. I'm asking because it

216
00:22:48,960 --> 00:22:52,400
seems that keeping eight or more precepts would be really hard otherwise.

217
00:22:54,320 --> 00:22:58,160
Well, it's easier when you're in a monastery, when you're off in the forest. I mean,

218
00:22:58,960 --> 00:23:06,000
when you're living on a straw mat in the forest, not that hard to keep eight percent.

219
00:23:10,160 --> 00:23:13,600
But no, you don't have to be. I mean, that's what you're striving for.

220
00:23:13,600 --> 00:23:20,400
And yeah, if you'll never get to that level, it can be what I just thought. I mean, even a

221
00:23:20,400 --> 00:23:23,520
sota panic and still have attachment desires for those things.

222
00:23:27,200 --> 00:23:28,800
Now, that's why you have to keep training.

223
00:23:28,800 --> 00:23:38,640
I don't put most up on too high a pedestal. You'll be disappointed otherwise.

224
00:23:44,720 --> 00:23:48,960
And also, they're not supposed to talk about that, right? So it's not like the late person can

225
00:23:48,960 --> 00:23:53,280
go up and ask a monk if they're a sota pana. Yeah, I mean, that's not something you should really

226
00:23:53,280 --> 00:23:59,680
worry about, but look at your own practice. Do the teachings work if they do? Someone

227
00:23:59,680 --> 00:24:04,800
reason they contacted me and point like asking me and they're afraid to take me as a teacher unless

228
00:24:04,800 --> 00:24:15,280
I'm, unless I let them know you're on this or not. She made a good point that she's had

229
00:24:15,280 --> 00:24:22,160
teachers who, you know, someone, one of her teachers told her that she has to find someone and

230
00:24:22,160 --> 00:24:27,680
make sure that they are realized. Because in Hinduism, they're very keen to tell you how

231
00:24:27,680 --> 00:24:32,800
realized they are. And I think in Mayana as well as some Mayana schools, and we're very important

232
00:24:32,800 --> 00:24:37,520
to talk about it. We've been talking about it, a lot of people like to talk about how their

233
00:24:37,520 --> 00:24:43,520
teachers were like, and it's good to do that. But it's really not proper. It's not the way you should be

234
00:24:43,520 --> 00:24:48,720
not doing anything. It's not always going to be focused on which we've focused on the teaching

235
00:24:48,720 --> 00:24:54,800
to do their work. Because that question you have to and you can answer yourself. That's a much

236
00:24:54,800 --> 00:25:01,200
more pertinent question. Do the teachings work? If you ask that question, then you know what you

237
00:25:01,200 --> 00:25:06,560
have to do. If I turn you guys my teachers and I don't have to go practice with him. I'm going

238
00:25:06,560 --> 00:25:12,000
to do the deal and I don't know every teacher, every person says that about their teacher.

239
00:25:12,000 --> 00:25:17,760
So many people get it wrong.

240
00:25:23,360 --> 00:25:28,480
But lately I've been doing meditation for about two to three days a week. How many days of

241
00:25:28,480 --> 00:25:34,320
constant consistent daily meditation should I do before I set up a formal meditation course with

242
00:25:34,320 --> 00:25:47,200
Dante? Seven days. We've done it for a week and we can meet. Yeah. We have to wait till January when

243
00:25:47,200 --> 00:25:57,760
I had more. It's looking good for adding 28 more slots for January, which is awesome, you know,

244
00:25:57,760 --> 00:26:05,360
and then I'm meeting with 56 people a week. 56 different people doing courses. I mean,

245
00:26:05,360 --> 00:26:11,840
they're not hardcore courses. It's not as great as awesome as people doing it coming and doing

246
00:26:11,840 --> 00:26:17,360
intensive course because that's tough. On the other hand, it's great.

247
00:26:19,200 --> 00:26:23,440
Well, the other side of it is, you know, doesn't happen sometimes that people go and do an

248
00:26:23,440 --> 00:26:27,200
intensive course and everything is great while they're there. But then they go back to their life

249
00:26:27,200 --> 00:26:32,320
and, you know, they don't continue with it. And here are people that are finding time to meditate

250
00:26:32,320 --> 00:26:41,120
consistently in their daily life. So maybe I don't think so. I mean, we consider we believe that for

251
00:26:41,120 --> 00:26:48,400
many people the course changes them. Not sure that this course will be as life changing. It won't be

252
00:26:49,200 --> 00:26:55,360
because especially we're not going to the end. Won't be as life changing, I don't think.

253
00:26:55,360 --> 00:27:02,720
And those people decide to take the online course to another level and come and do in-person

254
00:27:02,720 --> 00:27:07,920
meditation as well. Yeah, that's a good product for anything. Not a complete product because

255
00:27:07,920 --> 00:27:14,320
it's a big wake-up called to come and realize, oh, you have to do eight or ten or twelve hours a day

256
00:27:14,320 --> 00:27:15,760
as opposed to two.

257
00:27:19,360 --> 00:27:23,920
But they've had a little bit of, you know, experience with having the reporting session talking

258
00:27:23,920 --> 00:27:32,720
to you knowing what the, you know, how the steps go. And yeah, I mean, this was kind of the point

259
00:27:32,720 --> 00:27:37,200
was a lot of people are meditating for a long time without going through the steps, which is

260
00:27:37,200 --> 00:27:43,440
it. And they think, well, this is it. It's just kind of a simple little technique. Now,

261
00:27:43,440 --> 00:27:50,000
there's a little bit of a mental game or a mind training. Quite involved.

262
00:27:50,000 --> 00:27:54,000
Always something new.

263
00:28:11,360 --> 00:28:18,000
Sometimes I thought wondering how long I've been with. Sorry. Sometimes I thought wondering

264
00:28:18,000 --> 00:28:23,280
how long I've been meditating and how long I've left comes to me. And it causes me to be restless

265
00:28:23,280 --> 00:28:28,320
and makes it harder to continue. Is there anything I can do beyond noting it and letting it pass?

266
00:28:30,560 --> 00:28:38,080
Well, you should know the aspect of it. It is a, is a conglomerate of things. There's the thought

267
00:28:38,800 --> 00:28:44,320
and then there's the reason for the thought. Usually it's nagging because you're bored,

268
00:28:44,320 --> 00:28:54,080
you know, adverse to meditating longer. You want to do something else more that kind of thing.

269
00:28:55,280 --> 00:28:58,960
You know, it's often some reason for it. If there's not, if it's just a simple thought, then it

270
00:28:58,960 --> 00:29:03,920
would be innocuous. Probably wouldn't be asking me about it. And most probably these feelings

271
00:29:03,920 --> 00:29:09,760
come up when there's a, when they are persistent and they're persistent because we really want to

272
00:29:09,760 --> 00:29:20,160
stop. We don't want to continue with it. I wouldn't say it causes you restless. I mean,

273
00:29:22,160 --> 00:29:27,440
I would, I would suggest that probably the thought comes up. But wondering comes up because you're

274
00:29:27,440 --> 00:29:34,400
already immersed in meditating. So you have to find that emotion, disliking or wanting or whatever

275
00:29:34,400 --> 00:29:43,200
is boredom and note it as well. The thought as well, the emotion as well. And the experiences,

276
00:29:43,200 --> 00:29:49,200
if you feel pain or if you feel, you know, some physical discomfort that's causing it, then you have

277
00:29:49,200 --> 00:29:50,160
to note that as well.

278
00:29:55,760 --> 00:30:02,720
A friend of mine said Buddhists believe in monism or that we all are one. He said the Dalai Lama

279
00:30:02,720 --> 00:30:08,640
said this as well as an academic. I said it might be a Mahayana belief, but I have not come across

280
00:30:08,640 --> 00:30:15,200
this in Teravada, although all beings are composed of the same thing, aggregates. I said it as

281
00:30:15,200 --> 00:30:16,880
view. Have you heard this?

282
00:30:18,480 --> 00:30:23,200
Yeah, I mean, many Mahayana teachers seem to believe that it's a very Hindu concept.

283
00:30:23,200 --> 00:30:38,640
We are one, it's not a very Buddhist concept. I don't think even the Mahayana teaches monism

284
00:30:38,640 --> 00:30:56,960
at Vaita, they would call it Hinduism. Advaita means non-dualism, but non-dualism is something else.

285
00:30:58,880 --> 00:31:04,640
You know, in Hinduism there is the sense of oneness and everything. It's very much we are

286
00:31:04,640 --> 00:31:12,640
we are, I am God, you are God. The difference is, I know it, you don't. That's the kind of thing

287
00:31:12,640 --> 00:31:20,960
they do. We don't teach that. We're not all one. We're not all God. But yes, I think you did a

288
00:31:20,960 --> 00:31:30,960
service to Buddhism by stating the truth. We are made up the same stuff. We made it. One kind of stuff.

289
00:31:30,960 --> 00:31:40,160
Experience. There's no room for that. One thing to exist. It's not a part of the framework of reality.

290
00:31:40,800 --> 00:31:47,760
Reality has experiences arising and ceasing. Each experience is one. So two different experiences

291
00:31:47,760 --> 00:31:53,840
aren't even one anymore. It's just a view. It's just a view, it's just a view. It's just a belief.

292
00:31:53,840 --> 00:31:59,440
It's meaningless in terms of reality. It says nothing about ultimate reality.

293
00:32:08,800 --> 00:32:13,280
We all cut up. I think we're all cut up on questions, Bantay. All right.

294
00:32:13,280 --> 00:32:24,080
That's all then. If you have more questions, come back tomorrow. Tomorrow, probably have a

295
00:32:24,080 --> 00:32:30,640
demo product. I know. Tomorrow I have to skip again. Tomorrow there's an interfaith coffee house

296
00:32:30,640 --> 00:32:38,480
at 8 p.m. And I promised the Muslim Students Association that I'd go. So let's see how that

297
00:32:38,480 --> 00:32:43,680
happens. I think I have five to seven minutes to present. In shorter and shorter. On Saturday

298
00:32:43,680 --> 00:32:48,640
yesterday, I had to present in 20 minutes and I thought that was short. What am I going to say in

299
00:32:48,640 --> 00:32:59,120
five to seven minutes? Well, that sounds important. So I get to skip two demo products in the room.

300
00:33:00,000 --> 00:33:04,800
And this is the good when you've been telling us what this is. One of the most well-known

301
00:33:04,800 --> 00:33:10,800
demo patterns. It's a good idea. It's interesting. It's not actually that deep in the

302
00:33:10,800 --> 00:33:18,160
teachings or not actually that profound but interesting story. And it's well known. So we'll

303
00:33:18,160 --> 00:33:25,200
shoot for Wednesday. We will. Sounds good. Thank you, Bantay. Thanks, Robin. Thanks everyone for

304
00:33:25,200 --> 00:33:42,000
tuning in. Have a good night. Good night all.

