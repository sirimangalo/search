1
00:00:00,000 --> 00:00:08,080
All right, we're continuing our study of the Visudhi Mankha from when you want to tell

2
00:00:08,080 --> 00:00:09,640
us where we're at.

3
00:00:09,640 --> 00:00:21,880
Yes, we are just starting chapter 9, the Divine Abidings, which is on page 291.

4
00:00:21,880 --> 00:00:29,840
Anko, would you be able to start us off?

5
00:00:51,880 --> 00:01:15,000
Yes, I can, loving kindness.

6
00:01:15,000 --> 00:01:19,640
The four Divine Abidings were mentioned next to the recollections as meditation subjects.

7
00:01:19,640 --> 00:01:24,840
There are loving kindness, compassion, gladness and equanimity.

8
00:01:24,840 --> 00:01:30,600
A meditator who wants to develop, firstly loving kindness among these, if he is a beginner

9
00:01:30,600 --> 00:01:36,360
should sever the impediments and learn the meditation subject, then when he has done the

10
00:01:36,360 --> 00:01:42,280
work enacted with the meal and got rid of any dizziness due to it, should see himself comfortably

11
00:01:42,280 --> 00:01:45,960
on a well-prepared seat in his occluded place.

12
00:01:45,960 --> 00:01:52,720
To start with, he should review the danger in hate and the advantage in patience.

13
00:01:52,720 --> 00:01:53,720
Why?

14
00:01:53,720 --> 00:02:01,200
Because hate has to be abandoned and patience attained in the development of this meditation

15
00:02:01,200 --> 00:02:09,640
subject, and he cannot abandon unseen dangers and attain unknown advantages.

16
00:02:09,640 --> 00:02:16,000
Now the danger in hate should be seen in accordance with touch with us as this.

17
00:02:16,000 --> 00:02:23,320
Prince, when a man hates, he is afraid to hate and his mind is obsessed by hate.

18
00:02:23,320 --> 00:02:28,040
He kills living things and etc.

19
00:02:28,040 --> 00:02:35,320
And the advantage in patience should be understood according to such suitors as this.

20
00:02:35,320 --> 00:02:43,280
No higher rule, the Buddha say, than patience, and no nibana higher than forbearance.

21
00:02:43,280 --> 00:02:49,160
Patience in force, in strong array, this he might call a brown.

22
00:02:49,160 --> 00:02:58,280
No greater thing exists than patience.

23
00:02:58,280 --> 00:03:03,280
Thereupon he should embark upon the development of loving kindness for the purpose of

24
00:03:03,280 --> 00:03:09,040
secluding the mind from hate, seen as a danger and introducing it to patience, known as

25
00:03:09,040 --> 00:03:10,880
an advantage.

26
00:03:10,880 --> 00:03:17,160
But when he begins, he must know that some persons are of the wrong sort, at the very beginning

27
00:03:17,160 --> 00:03:22,040
and that loving kindness should be developed towards certain kinds of persons and not towards

28
00:03:22,040 --> 00:03:24,560
certain other kinds at first.

29
00:03:24,560 --> 00:03:33,280
For loving kindness, it should not be developed at first towards the following four kinds of

30
00:03:33,280 --> 00:03:34,280
persons.

31
00:03:34,280 --> 00:03:42,960
An antipatetic person, a very dearly loved friend, an ultra person, and a hostile person.

32
00:03:42,960 --> 00:03:53,360
Also, it should not be developed specifically towards the opposite sex or towards a dead person.

33
00:03:53,360 --> 00:04:01,720
What is the reason why it should not be developed at first towards an antipatetic person

34
00:04:01,720 --> 00:04:04,080
on the others?

35
00:04:04,080 --> 00:04:13,120
To put an antipatetic person in a dear one, in a dear one's place, is fatigue.

36
00:04:13,120 --> 00:04:20,200
To put a very dearly loved friend in a neutral place is fatigue.

37
00:04:20,200 --> 00:04:27,280
What is the latest mission before the friend?

38
00:04:27,280 --> 00:04:29,680
He feels like sweeping.

39
00:04:29,680 --> 00:04:39,000
To put an ultra person in a respected one's or a dear one's place is fatigue.

40
00:04:39,000 --> 00:04:45,560
Anger springs up in him if he recollects a hostile person.

41
00:04:45,560 --> 00:04:56,160
That's why it should not be developed at first towards an antipatetic person and the rest.

42
00:04:56,160 --> 00:05:01,760
Then if he develops it specifically towards the opposite sex, lust inspired by that person

43
00:05:01,760 --> 00:05:03,600
springs up in him.

44
00:05:03,600 --> 00:05:09,240
An elder supported by a family was asked, it seems, by a friend's son, venerable sir,

45
00:05:09,240 --> 00:05:11,880
toward whom should loving kindness be developed.

46
00:05:11,880 --> 00:05:15,080
The elder told him, toward a person one loves.

47
00:05:15,080 --> 00:05:16,840
He loved his own wife.

48
00:05:16,840 --> 00:05:21,280
Through developing loving kindness toward her, he was fighting against the wall all night,

49
00:05:21,280 --> 00:05:22,760
all night.

50
00:05:22,760 --> 00:05:26,600
That is why it should not be developed specifically toward the opposite sex.

51
00:05:26,600 --> 00:05:32,320
I should read the foot, but it's quite interesting what fighting against the wall means.

52
00:05:32,320 --> 00:05:36,920
I'll read it out loud for the benefit of the recording.

53
00:05:36,920 --> 00:05:42,840
Fighting against the wall, having undertaken the precepts of virtue, sat down on the sea

54
00:05:42,840 --> 00:05:45,440
in his room with the door locked.

55
00:05:45,440 --> 00:05:47,680
He was developing loving kindness.

56
00:05:47,680 --> 00:05:51,640
Blinded by lust, arisen under cover of the loving kindness, he wanted to go to his wife

57
00:05:51,640 --> 00:05:55,960
and without noticing the door, he beat on the wall and his desire to get out even by

58
00:05:55,960 --> 00:06:00,520
breaking the wall down.

59
00:06:00,520 --> 00:06:06,120
That is interesting.

60
00:06:06,120 --> 00:06:10,240
Blinded by lust.

61
00:06:10,240 --> 00:06:19,440
How do you explain that to the contractor, this kind of fix the wall?

62
00:06:19,440 --> 00:06:24,800
But if he develops it towards the dead person, he reaches neither absorption nor access.

63
00:06:24,800 --> 00:06:31,040
A young Biko, it seems, had started loving kindness inspired by his teacher.

64
00:06:31,040 --> 00:06:34,880
His knowledge made no head to a atoll.

65
00:06:34,880 --> 00:06:40,960
He went to a senior atoll, and told him, ''I'm quite familiar with any genre through

66
00:06:40,960 --> 00:06:44,480
loving kindness, and yet I cannot attain it.

67
00:06:44,480 --> 00:06:45,480
What is the matter?

68
00:06:45,480 --> 00:06:50,480
The other said, seek the sign frame to object of your meditation.

69
00:06:50,480 --> 00:06:54,920
He did so, finding that his teacher had died.

70
00:06:54,920 --> 00:07:00,960
He proceeded with developing loving kindness, inspired by another and attained absorption.

71
00:07:00,960 --> 00:07:06,280
That is why it should not be developed towards one who is dead.

72
00:07:06,280 --> 00:07:16,120
But they have a question, what is the difference between meta and ados?

73
00:07:16,120 --> 00:07:33,240
Are they the same or are they the same or are they the same or are they the same or are

74
00:07:33,240 --> 00:07:34,240
the same?

75
00:07:34,240 --> 00:07:35,240
I can't remember.

76
00:07:35,240 --> 00:07:38,440
I think they are different, but I can't remember.

77
00:07:38,440 --> 00:07:41,160
Actually, I think they might be the same.

78
00:07:41,160 --> 00:07:42,920
There are two of them are the same, you see.

79
00:07:42,920 --> 00:07:48,280
There are only two upper manias, upper mania and jaytasika.

80
00:07:48,280 --> 00:07:51,520
I think they are karuna and moodita.

81
00:07:51,520 --> 00:07:56,880
Because who pick is your pick and jaytasika, it's not, it's already been discussed elsewhere.

82
00:07:56,880 --> 00:08:03,920
And I think you're right, I think meta isn't a jaytasika, it's considered called adosajaytasika.

83
00:08:03,920 --> 00:08:13,080
But you and you pick, I don't have hatred towards anyone right now, so we can technically

84
00:08:13,080 --> 00:08:22,520
say it's not a hateful thought or lack of hate, I mean it is lacking hate, which

85
00:08:22,520 --> 00:08:25,920
is what ados amines right now.

86
00:08:25,920 --> 00:08:47,920
Yeah, I mean, it depends whether you're talking about the abidama or the sutas, because it does seem to be a little bit underwhelming to say that love is simply the absence of anger, that doesn't seem to be what is being talked about here, seems to be more of a positive aspect, although it's hard to say exactly what is meant.

87
00:08:56,920 --> 00:09:21,920
First of all, it should be developed only towards oneself, doing it repeatedly, thus may I be happy and free from suffering, or may I keep myself free from enemy of election and anxiety and live happily.

88
00:09:21,920 --> 00:09:33,920
If that is so, does it not conflict with what is said in the texts, where there is no mention of any development of it, towards oneself, in what is said in the way banga.

89
00:09:33,920 --> 00:09:46,920
And how does a be-good well-pervading one direction with this heartfelt loving kindness, just as he would feel loving kindness on seeing a dearly-loved person, so it pervades all beings with loving kindness.

90
00:09:46,920 --> 00:10:00,920
And in what is said in the Patisambida, in what five ways is the mind-deliverance of loving kindness practiced with unspecified provision, may all beings be free from enmity of fiction and anxiety and live happily.

91
00:10:00,920 --> 00:10:12,920
May all breathing things, all who are born, all persons, all those who have a personality be free from enmity of fiction and anxiety and live happily.

92
00:10:12,920 --> 00:10:19,920
And in what is said in the Medisuta, in joy and safety, may all beings be joyful and hurt.

93
00:10:19,920 --> 00:10:23,920
Does it not conflict with those texts?

94
00:10:23,920 --> 00:10:29,920
It does not conflict. Why not? Because that refers to absorption.

95
00:10:29,920 --> 00:10:45,920
But this initial development, towards oneself, refers to making oneself an example. But even if he develops developing kindness for a hundred or a thousand years in this way, I am happy, and so on.

96
00:10:45,920 --> 00:11:01,920
Of social fusion would never arise. But if he develops it in this way, I am happy, just as I want to be happy and dread-paying, and say I want to live and not to die, so to other beings too.

97
00:11:01,920 --> 00:11:22,920
Making himself an example, then decide for other beings, welfare and wealth and happiness arises in him. And this method is indicated by the blessed one saying, I will defeat all quarters with my mind, not find I any dearer than myself.

98
00:11:22,920 --> 00:11:35,920
Selfies like wise to every other idea, who loves himself and never harm others.

99
00:11:35,920 --> 00:11:40,920
So he should first as example, pervade himself with loving kindness.

100
00:11:40,920 --> 00:12:03,920
Next after that, in order to proceed easily, he can recollect such gifts, kind words, etc., as inspire love and endearment, such virtue, learning, etc., as inspire respect and reverence met within a teacher or his equivalent or preceptor of his equivalent, developing loving kindness toward him in the way beginning.

101
00:12:03,920 --> 00:12:12,920
May this good man be happy and free from suffering. What's such a person of course, he attains absorption.

102
00:12:12,920 --> 00:12:32,920
But if this be who does not rest content with just that much, and wants to break down the barriers, he should next, after that, develop loving kindness towards a very dearly loved friend, then towards a neutral person as a very dearly loved friend.

103
00:12:32,920 --> 00:12:48,920
Then towards a hostile person as neutral. And while he does so, he should make his mind manageable and will be in each instance before passing on to the next.

104
00:12:48,920 --> 00:13:03,920
But if he has no enemy, or he is of the type of a great man who does not perceive another as an enemy, even when the other does harm, he should not interest himself as follows.

105
00:13:03,920 --> 00:13:27,920
Now that my consciousness of loving kindness has become wielding towards a neutral person as she'll apply it to a hostile one. Rather, it was about one who actually has an enemy that it was said above that he should develop loving kindness towards a hostile person as neutral.

106
00:13:27,920 --> 00:13:47,920
Getting rid of her name. If resentment arises in him when he applies his mind to a hostile person, because he remembers wrongs down by the person by that person, he should get rid of the resentment by entering repeatedly into loving kindness.

107
00:13:47,920 --> 00:14:03,920
The words, any of the first mentioned person, and then after he has emerged each time, directing loving kindness towards the first.

108
00:14:03,920 --> 00:14:17,920
But if he does not die out in spite of his efforts, then letting reflect upon the soul with other few years of such kind.

109
00:14:17,920 --> 00:14:33,920
Try and stir repeatedly to leave resentment far behind. He should have managed himself in this way.

110
00:14:33,920 --> 00:15:02,920
Now, Jew who gets angry has not the blessed one said this, because even if bandits virtually served me from being with a two hundred soul, he who entrained and entertained hate in his head on that account, could not be one who carried out my teaching.

111
00:15:02,920 --> 00:15:26,920
This, to repay angry men in kind is worse than to be angry faced. Repaying no angry men in kind, and when a barrel had to win, the will of God, he does promote his own and then the others too.

112
00:15:26,920 --> 00:15:34,920
Coocha, another's anger now, and mindful maintain his peace.

113
00:15:34,920 --> 00:15:45,920
And this because there are seven things gratifying and helpful to an enemy that happened to one who is angry.

114
00:15:45,920 --> 00:16:03,920
Where the woman or men, what seven here because an enemy which starts for his enemy, letting be ugly, where is that, and any need does not delight in an enemy beauty.

115
00:16:03,920 --> 00:16:29,920
Now, this angry person is afraid to anger, ruled by anger, thought, well-bated, well, and no, anointed with hair and beard, trimmed and closed in white, yet he saw me being afraid to anger.

116
00:16:29,920 --> 00:16:38,920
This is the first thing gratifying and helpful to an enemy that befalls one who is angry.

117
00:16:38,920 --> 00:16:57,920
Where the woman or men, furthermore, an enemy which starts for his enemy, letting play in pain, letting have no fortune, letting not to be wealthy, letting not to be families,

118
00:16:57,920 --> 00:17:23,920
letting have no friends, letting not break up of the body after death appeared in happy destiny in a humanly world.

119
00:17:23,920 --> 00:17:32,920
Why is that? An enemy does not delight in an enemy who's going to a happy destiny.

120
00:17:32,920 --> 00:17:43,920
Now, this angry person is afraid to anger, ruled by anger, his misconduct himself in the body's speech and mind.

121
00:17:43,920 --> 00:18:03,920
On the breakup of the body, after death, hero appears in a state of loss, in a happy destiny, in perdition, in hell, being afraid to anger.

122
00:18:03,920 --> 00:18:20,920
And this, as love from a prior birth, a vote, and a vote in the middle, serves neither for timber in the village nor for timber in the forest.

123
00:18:20,920 --> 00:18:35,920
So, if such a person, as this, I say, if you are angry now, you will be one who does not carry out the blessed ones teaching.

124
00:18:35,920 --> 00:18:51,920
By repaying an angry man in kind you will be cause and the angry man, and do not win the pearl had to win. You will yourself to do yourself the things that have your enemy.

125
00:18:51,920 --> 00:19:00,920
And you will be like a prior love, prior love.

126
00:19:00,920 --> 00:19:06,920
If his resentment subsides, drives, and makes effort in this way, it is good.

127
00:19:06,920 --> 00:19:18,920
If not, then he should remove irritation by remembering some controlled and purified state in that person, which expires confidence when remembered.

128
00:19:18,920 --> 00:19:31,920
For one person may be controlled in his body behavior with his control in doing an extensive cause of duty, known to all, though his verbal and mental behavior are controlled.

129
00:19:31,920 --> 00:19:41,920
Then the latter should be ignored and controlled in his body behavior, remember.

130
00:19:41,920 --> 00:19:58,920
Another way may be controlled in his verbal behavior, and his control known to all, he may naturally be clever at welcoming kindly, easy to talk with.

131
00:19:58,920 --> 00:20:26,920
Conjunior, open, countenanced, deferential in speech, and he may expound the dharma with a sweet voice and give explanations of dharma with well-rounded phrases and details.

132
00:20:26,920 --> 00:20:35,920
Through his bodily and mental behavior are not controlled.

133
00:20:35,920 --> 00:20:48,920
Then the latter should be ignored and controlled in his verbal behavior, remembered.

134
00:20:48,920 --> 00:20:58,920
Another way may be controlled in his mental behavior, and his control in worship at shrines, etc., evident to all.

135
00:20:58,920 --> 00:21:07,920
For one one who is uncontrolled in mind, plays homage at a shrine, or at an enlightenment tree, or to elders, it is not to it carefully.

136
00:21:07,920 --> 00:21:18,920
And he sits in the dharma preaching pavilion with mind astray or nodding, what one whose mind is controlled, plays homage carefully and deliberately.

137
00:21:18,920 --> 00:21:28,920
Listen to the dharma tentatively remembering it, and if it's in the confidence in his mind through his body or his speech.

138
00:21:28,920 --> 00:21:38,920
So another may be controlled in his mental behavior, though his body and verbal behavior are not controlled.

139
00:21:38,920 --> 00:21:42,920
Then the latter should be ignored and the control in his mental behavior, remembered.

140
00:21:42,920 --> 00:22:11,920
But there may be another whom, not even one of these three things, is controlled. Then compassion for that person should be about this. Though he's going about in the human world now, nevertheless after a certain number of days, he will find himself in one of the eight great hell, all the 16 prominent hells, where irritations subsides to through compassion.

141
00:22:11,920 --> 00:22:30,920
And yet another all three may be controlled. Then he can remember any of these three in that person, whichever he likes, for the development of loving kindness for such a person is eating.

142
00:22:30,920 --> 00:22:48,920
And in order to make the meaning of this clear, the following suit are from the book of five should be cited in full. Because there are five ways of dispelling annoyance, whereby annoyance, a reason and a beaker can be entirely dispelled.

143
00:22:48,920 --> 00:23:07,920
But if irritation still arises in spite of his effort, then he should admonish himself thus. Suppose an enemy has hurt you now in what is his domain. Why try yourself as well to hurt your mind? This is not his domain.

144
00:23:07,920 --> 00:23:22,920
In tears you left your family. They had been kind and helpful to you. So why not leave your enemy? The anger that brings harm to you.

145
00:23:22,920 --> 00:23:42,920
This anger that you entertain in a new wing at the very roots of all the virtues that you guard, who is there such a fool as you. Another does ignoble deeds, so you're angry. How is this?

146
00:23:42,920 --> 00:23:58,920
Do you then want to copy to the sort of act that he commits? Suppose another to annoy, provokes you with some odious act. Why suffer anger to spring up and do as he would have you do?

147
00:23:58,920 --> 00:24:22,920
If you get angry, then maybe you make him suffer, maybe not. Though with the hurt that anger brings, you certainly are punished now. If anger blinded enemies set out to tread the path of who? Do you buy getting angry too, intend to follow you to toe?

148
00:24:22,920 --> 00:24:34,920
If hurt is done you buy a foe because of anger on your part, then put your anger down for why should you be harassed groundlessly.

149
00:24:34,920 --> 00:25:01,920
Since states, since states, last but the moments time, those aggregates by which down the odious act have seized. So now what is it you're angry with? Who should be hurt? Who seeks to hurt another? In the others absence.

150
00:25:01,920 --> 00:25:09,920
Your presence is the cause of hurt. Why are you angry then with him?

151
00:25:09,920 --> 00:25:13,920
That's pretty awesome.

152
00:25:13,920 --> 00:25:23,920
I like the idea that you're to blame for the anger because you're there.

153
00:25:23,920 --> 00:25:32,920
It makes you think really.

154
00:25:32,920 --> 00:25:39,920
It's like that novice for being there and getting his eye poked out.

155
00:25:39,920 --> 00:25:43,920
But he wasn't angry.

156
00:25:43,920 --> 00:25:55,920
But why should his argument be why should he blame the elder when he was to blame for being in the way?

157
00:25:55,920 --> 00:26:16,920
I remember he said it's the thought of some sort.

158
00:26:16,920 --> 00:26:34,920
Is that character reoccurring different stories or is it just that one story?

159
00:26:34,920 --> 00:26:48,920
I don't think his name is given in the story.

160
00:26:48,920 --> 00:26:56,920
Okay. Shall I continue or you guys want to keep on talking about the verse?

161
00:26:56,920 --> 00:27:05,920
I'll go ahead, please.

162
00:27:05,920 --> 00:27:29,920
Thank you.

163
00:27:29,920 --> 00:27:50,920
Thank you.

164
00:27:50,920 --> 00:28:17,920
Thank you.

165
00:28:17,920 --> 00:28:34,920
Thank you.

166
00:28:34,920 --> 00:28:55,920
Thank you.

167
00:28:55,920 --> 00:29:19,920
Thank you.

168
00:29:19,920 --> 00:29:46,920
Thank you.

169
00:29:46,920 --> 00:30:11,920
Thank you.

170
00:30:11,920 --> 00:30:38,920
Thank you.

171
00:30:38,920 --> 00:31:02,920
Thank you.

172
00:31:02,920 --> 00:31:29,920
Thank you.

173
00:31:29,920 --> 00:31:55,920
Thank you.

174
00:31:55,920 --> 00:32:22,920
Thank you.

175
00:32:22,920 --> 00:32:48,920
Thank you.

176
00:32:48,920 --> 00:33:14,920
Thank you.

177
00:33:14,920 --> 00:33:40,920
Thank you.

178
00:33:40,920 --> 00:34:06,920
Thank you.

179
00:34:06,920 --> 00:34:32,920
Thank you.

180
00:34:32,920 --> 00:34:58,920
Thank you.

181
00:34:58,920 --> 00:35:24,920
Thank you.

182
00:35:24,920 --> 00:35:50,920
Thank you.

183
00:35:50,920 --> 00:36:16,920
Thank you.

184
00:36:16,920 --> 00:36:42,920
Thank you.

185
00:36:42,920 --> 00:37:09,920
Thank you.

186
00:37:09,920 --> 00:37:35,920
Thank you.

187
00:37:35,920 --> 00:38:01,920
Thank you.

188
00:38:01,920 --> 00:38:27,920
Thank you.

189
00:38:27,920 --> 00:38:53,920
Thank you.

190
00:38:53,920 --> 00:39:19,920
Thank you.

191
00:39:19,920 --> 00:39:45,920
Thank you.

192
00:39:45,920 --> 00:40:11,920
Thank you.

193
00:40:11,920 --> 00:40:37,920
Thank you.

194
00:40:37,920 --> 00:41:03,920
Thank you.

195
00:41:03,920 --> 00:41:29,920
Thank you.

196
00:41:29,920 --> 00:41:55,920
Thank you.

197
00:41:55,920 --> 00:42:21,920
Thank you.

198
00:42:21,920 --> 00:42:47,920
Thank you.

199
00:42:47,920 --> 00:43:13,920
Thank you.

200
00:43:13,920 --> 00:43:39,920
Thank you.

201
00:43:39,920 --> 00:44:05,920
Thank you.

202
00:44:05,920 --> 00:44:31,920
Thank you.

203
00:44:31,920 --> 00:44:57,920
Thank you.

204
00:44:57,920 --> 00:45:23,920
Thank you.

205
00:45:23,920 --> 00:45:49,920
Thank you.

206
00:45:49,920 --> 00:46:15,920
Thank you.

207
00:46:15,920 --> 00:46:41,920
Thank you.

208
00:46:41,920 --> 00:47:07,920
Thank you.

209
00:47:07,920 --> 00:47:33,920
Thank you.

210
00:47:33,920 --> 00:47:59,920
Thank you.

211
00:47:59,920 --> 00:48:25,920
Thank you.

212
00:48:25,920 --> 00:48:51,920
Thank you.

213
00:48:51,920 --> 00:49:17,920
Thank you.

214
00:49:17,920 --> 00:49:43,920
Thank you.

215
00:49:43,920 --> 00:50:09,920
Thank you.

216
00:50:09,920 --> 00:50:35,920
Thank you.

217
00:50:35,920 --> 00:51:01,920
Thank you.

218
00:51:01,920 --> 00:51:27,920
Thank you.

219
00:51:27,920 --> 00:51:53,920
Thank you.

220
00:51:53,920 --> 00:52:19,920
Thank you.

221
00:52:19,920 --> 00:52:47,920
Thank you.

222
00:52:47,920 --> 00:53:13,920
Thank you.

223
00:53:13,920 --> 00:53:39,920
Thank you.

224
00:53:39,920 --> 00:54:05,920
Thank you.

