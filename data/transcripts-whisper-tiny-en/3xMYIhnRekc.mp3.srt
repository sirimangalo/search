1
00:00:00,000 --> 00:00:07,360
Venerable Taylor, when I tried to bring a Lord Buddha's little statue to my room in Japan,

2
00:00:07,360 --> 00:00:15,400
my wife opposed to it crazily, and she suggested to divorce and worship Lord Buddha.

3
00:00:15,400 --> 00:00:18,240
She's a socag and gakai member.

4
00:00:18,240 --> 00:00:24,080
Ah, the problems with marriage, no?

5
00:00:24,080 --> 00:00:29,560
That's not really a question now, is it?

6
00:00:29,560 --> 00:00:33,400
I guess the question is what to do.

7
00:00:33,400 --> 00:00:39,040
You know, I'm not really for marriage and for these reasons.

8
00:00:39,040 --> 00:00:47,680
If there's some need to be married, then I would just say put up with it.

9
00:00:47,680 --> 00:00:58,920
But otherwise, divorce become a monk, if you really are keen on doing the right,

10
00:00:58,920 --> 00:01:06,600
you know, engaging in good things, you know, engaging in what is a real benefit.

11
00:01:06,600 --> 00:01:07,600
That's the advice that we give.

12
00:01:07,600 --> 00:01:13,000
Now, the question is how far you can go towards putting that sort of thing into effect.

13
00:01:13,000 --> 00:01:19,400
If probably it's not the case that you're probably not in a position to ordain, you just

14
00:01:19,400 --> 00:01:27,200
want to do some practice and probably you love your wife and are very much attached to her.

15
00:01:27,200 --> 00:01:29,600
But you know, that's the reality of living in the world.

16
00:01:29,600 --> 00:01:36,000
It's doubly so in terms of being married, this is the difficulty that we have that we face

17
00:01:36,000 --> 00:01:39,960
because you're not married to your wife because the reason you're married to your wife

18
00:01:39,960 --> 00:01:45,360
is not because of your religious views, obviously.

19
00:01:45,360 --> 00:01:52,840
So you have to therefore balance which is more important to your religious views or your

20
00:01:52,840 --> 00:01:55,960
attraction to your wife.

21
00:01:55,960 --> 00:02:00,400
And if they're both important, then you've got a conflict that you have to resolve.

22
00:02:00,400 --> 00:02:10,480
You have to juggle to conflicting conflicting aspects of your life.

23
00:02:10,480 --> 00:02:18,680
Now, there are ways to, you know, to ameliorate it.

24
00:02:18,680 --> 00:02:28,160
I mean, obviously what you want is a way to keep them both, you know, which is not impossible.

25
00:02:28,160 --> 00:02:35,200
That's just, you know, it's so much simpler to just give it up and go your own way, right?

26
00:02:35,200 --> 00:02:42,760
Once you find out that you're not so compatible as you thought.

27
00:02:42,760 --> 00:02:52,080
And it depends, the real key question here that has to be answered through test is whether

28
00:02:52,080 --> 00:03:00,400
your wife has the potential to come to understand the truth, I mean, as we understand

29
00:03:00,400 --> 00:03:01,400
it as Buddhist.

30
00:03:01,400 --> 00:03:06,720
So as Buddhist, we understand certain things to be the truth is your wife capable of

31
00:03:06,720 --> 00:03:13,720
understanding those things and therefore letting go and, you know, actually appreciating the

32
00:03:13,720 --> 00:03:14,720
Buddha.

33
00:03:14,720 --> 00:03:18,120
Yeah, okay, that's, that's actually just one question.

34
00:03:18,120 --> 00:03:20,320
Let's deal with that one first.

35
00:03:20,320 --> 00:03:36,680
So the, the, an important thing to do is to begin to help her to, to understand

36
00:03:36,680 --> 00:03:50,600
and, you know, concepts like letting go and concepts like purity and enlightenment and so on.

37
00:03:50,600 --> 00:03:57,160
To help her to, to see things in the same way as you see them, you know, okay.

38
00:03:57,160 --> 00:04:02,520
And if she has, you know, good qualities of mind, then she will come around.

39
00:04:02,520 --> 00:04:07,880
But the other thing is, as I, as I thought about this is that, you know, you're, you're

40
00:04:07,880 --> 00:04:13,760
talking about a Buddha image, which is not really intrinsically important to your Buddhist

41
00:04:13,760 --> 00:04:14,760
practice.

42
00:04:14,760 --> 00:04:20,400
You can do away with the Buddha statue or you can put it in a place and just stop worshiping

43
00:04:20,400 --> 00:04:21,400
it.

44
00:04:21,400 --> 00:04:25,280
You know, not do Buddha puja or offering it food or so on.

45
00:04:25,280 --> 00:04:28,680
There's no reason for you to do those things.

46
00:04:28,680 --> 00:04:35,320
There's, there's, sorry, those, there's no intrinsic duty for you to do those things.

47
00:04:35,320 --> 00:04:42,440
So if it's causing conflict and just put the Buddha statue away and keep practicing.

48
00:04:42,440 --> 00:04:53,720
Yeah, so, you know, it, it, it's not really even a sign that she's on the wrong path.

49
00:04:53,720 --> 00:05:01,480
You may even be trying to warn you away from Sila Vatapara masa, Sila Vatapara masa,

50
00:05:01,480 --> 00:05:06,240
which is clinging to rituals, so if you give food to the Buddha every day and you think

51
00:05:06,240 --> 00:05:10,640
that that's the most important thing and so on or it's intrinsically important, then you

52
00:05:10,640 --> 00:05:14,320
may be missing something that she's trying to teach you.

53
00:05:14,320 --> 00:05:19,440
But from the sounds of it, it's just, you know, people clinging to dogmas and opposing

54
00:05:19,440 --> 00:05:25,840
other people's beliefs and so on and then that's difficult to deal with.

55
00:05:25,840 --> 00:05:32,920
So why it's very important to don't just be blind, they say love is blind, you know, love

56
00:05:32,920 --> 00:05:39,560
looks not with its heart, with its eyes, but with its mind and therefore is we can

57
00:05:39,560 --> 00:05:48,760
keep it painted blind, nor hats loves mind of any reason, taste, wings and noise, figure

58
00:05:48,760 --> 00:05:52,680
on Hindi haste, something like that.

59
00:05:52,680 --> 00:05:59,000
We rush into things, not based on love actually based on attachment and that's a problem.

60
00:05:59,000 --> 00:06:05,000
If you're going to get married, you should be very, very sure that you're compatible,

61
00:06:05,000 --> 00:06:09,160
that you're both going to be, you're going to be helping each other, supporting each

62
00:06:09,160 --> 00:06:10,160
other in your practice.

63
00:06:10,160 --> 00:06:16,000
Now, it may be that she is, it may be that she's actually helping you teach you something

64
00:06:16,000 --> 00:06:20,400
to help you to open up, but from the sounds of it is probably not, and it's probably

65
00:06:20,400 --> 00:06:25,640
something that you'll have to help her to let go and to see the beauty of the Buddha

66
00:06:25,640 --> 00:06:28,080
and the greatness of the Buddha.

67
00:06:28,080 --> 00:06:32,320
I don't really know what Soka Gaka is, I forgot, I think that's the ones that they do,

68
00:06:32,320 --> 00:06:41,560
and I'm your holy Gikyo, and want to manifest things and so on.

69
00:06:41,560 --> 00:06:45,480
So yeah, you have to help her, in that case, you'd have to help her give up materialism

70
00:06:45,480 --> 00:06:54,600
and thinking about the future and so on, help her to understand what is true Buddhism,

71
00:06:54,600 --> 00:07:09,880
what the Buddha truly taught and so on.

