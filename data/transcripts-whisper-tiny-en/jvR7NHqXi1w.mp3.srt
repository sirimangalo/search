1
00:00:00,000 --> 00:00:13,800
So everyone, those of you on YouTube, we're broadcasting late because we've just done

2
00:00:13,800 --> 00:00:18,800
the Dhamapada and I was going to turn all this off first.

3
00:00:18,800 --> 00:00:28,000
So excuse me, many things to turn off.

4
00:00:28,000 --> 00:00:39,000
With me as Robin, thank you for joining me, Robin, as usual.

5
00:00:39,000 --> 00:00:44,000
Thank you, Budday.

6
00:00:44,000 --> 00:00:47,000
And I think we have some questions.

7
00:00:47,000 --> 00:00:49,000
We do.

8
00:00:49,000 --> 00:00:54,000
This one goes back to last night just before we signed off.

9
00:00:54,000 --> 00:00:58,000
It is easy to stay mindful during the gross sensations.

10
00:00:58,000 --> 00:01:03,000
But when subtle sensations come, desire to let them continue arises.

11
00:01:03,000 --> 00:01:14,000
How to understand Dukka in such situations?

12
00:01:14,000 --> 00:01:25,000
So is that the suffering there is that they can't satisfy you?

13
00:01:25,000 --> 00:01:32,000
Suffering there is that they are not happiness.

14
00:01:32,000 --> 00:01:40,000
Truly the suffering there is in the awareness of them as it's not, it's not a positive

15
00:01:40,000 --> 00:01:43,800
thing, it's a not happiness thing.

16
00:01:43,800 --> 00:01:52,800
So when you're mindful of them, you'll see instantly that they won't even have, it's not an intellectual thing.

17
00:01:52,800 --> 00:01:56,800
You'll just feel the peace and you won't see it as happiness.

18
00:01:56,800 --> 00:02:02,800
Or you'll see the happiness and won't see it as happiness.

19
00:02:02,800 --> 00:02:09,800
I mean, as pleasant, you won't have any liking for it.

20
00:02:09,800 --> 00:02:13,800
Remember, the three characteristics are negatives.

21
00:02:13,800 --> 00:02:15,800
They're not positive qualities.

22
00:02:15,800 --> 00:02:18,800
I need jam means it's not permanent.

23
00:02:18,800 --> 00:02:21,800
Dukka means it's not happiness.

24
00:02:21,800 --> 00:02:26,800
Or even if you want to go in more detail, you can't fix it.

25
00:02:26,800 --> 00:02:32,800
There's nothing to be, it's not a refuge.

26
00:02:32,800 --> 00:02:35,800
It's not a base.

27
00:02:35,800 --> 00:02:41,800
It's not something you can use to satisfy you, for example.

28
00:02:41,800 --> 00:02:44,800
But in general, it's not happiness, isn't it?

29
00:02:44,800 --> 00:02:46,800
And Anata means it's not self.

30
00:02:46,800 --> 00:02:50,800
It's not controllable, it's not you, it's not yours.

31
00:02:50,800 --> 00:02:53,800
That kind of thing.

32
00:02:53,800 --> 00:02:58,800
So if you say to yourself liking, liking, or feeling, feeling, or happy, happy,

33
00:02:58,800 --> 00:03:01,800
you'll see, you'll just let go of it immediately.

34
00:03:01,800 --> 00:03:13,800
Because once you see it clearly, you see there's really nothing inherently positive about it.

35
00:03:13,800 --> 00:03:19,800
Bante Sujato of the Ajahn Chag group once mentioned that the biggest eater in the world is time,

36
00:03:19,800 --> 00:03:22,800
but the Arahat eats time itself.

37
00:03:22,800 --> 00:03:24,800
In what sense does he eat it?

38
00:03:24,800 --> 00:03:33,800
Thank you, Bante.

39
00:03:33,800 --> 00:03:44,800
Anata has Bante Sujato, I don't know.

40
00:03:44,800 --> 00:04:00,800
I mean, Arahat has attained enlightenment and Nibana is out of time, is timeless, so that's probably what he's referring to.

41
00:04:00,800 --> 00:04:06,800
If the Buddha was omniscient, why didn't he make scientific discoveries like discovering the law of

42
00:04:06,800 --> 00:04:12,800
electromagnetism to make technology and to improve people's lives?

43
00:04:12,800 --> 00:04:18,800
I really think electromagnetism has improved people's lives.

44
00:04:18,800 --> 00:04:25,800
I think there's a lot of...

45
00:04:25,800 --> 00:04:28,800
It's funny, I mean, it's not really...

46
00:04:28,800 --> 00:04:35,800
I'm not really impressed by the sense of awful.

47
00:04:35,800 --> 00:04:39,800
It's not a great question.

48
00:04:39,800 --> 00:04:45,800
Sorry to say.

49
00:04:45,800 --> 00:04:52,800
Because why...

50
00:04:52,800 --> 00:04:56,800
There are so many things that you could say help people, right?

51
00:04:56,800 --> 00:05:04,800
I mean, even, I think better than that example would be medicinal discoveries, right?

52
00:05:04,800 --> 00:05:10,800
Why didn't the Buddha tell us about penicillin, for example, right?

53
00:05:10,800 --> 00:05:13,800
Or give any number of things?

54
00:05:13,800 --> 00:05:16,800
I mean, there's an argument to be made there.

55
00:05:16,800 --> 00:05:20,800
But even that is not a very...

56
00:05:20,800 --> 00:05:32,800
It's not a very deep question or the answer is because those are not very deep, profound benefits.

57
00:05:32,800 --> 00:05:42,800
It doesn't really benefit people to have computers for...

58
00:05:42,800 --> 00:05:43,800
And so on.

59
00:05:43,800 --> 00:05:50,800
It would benefit people as meditation, and so that's what the Buddha taught.

60
00:05:50,800 --> 00:05:52,800
It's a reasonable question.

61
00:05:52,800 --> 00:05:53,800
I shouldn't...

62
00:05:53,800 --> 00:05:59,800
I'm not really trying to discount it, but you have to understand...

63
00:05:59,800 --> 00:06:08,800
It's not really...

64
00:06:08,800 --> 00:06:10,800
It's on a whole other level.

65
00:06:10,800 --> 00:06:16,800
The Buddha's teaching is something quite different.

66
00:06:16,800 --> 00:06:18,800
It's like why didn't the Buddha...

67
00:06:18,800 --> 00:06:23,800
Like if I were to ask you, why didn't the Buddha act as a doctor?

68
00:06:23,800 --> 00:06:28,800
Why didn't he cure people's sicknesses, right?

69
00:06:28,800 --> 00:06:36,800
Why didn't he help a nathapindika get his money back?

70
00:06:36,800 --> 00:06:40,800
Why didn't he go around telling the kings how to run...

71
00:06:40,800 --> 00:06:43,800
Why didn't he run for government?

72
00:06:43,800 --> 00:06:48,800
Why didn't the Buddha stay as a prince to rule his kingdom?

73
00:06:48,800 --> 00:06:53,800
And the answer is all the same.

74
00:06:53,800 --> 00:07:07,800
So I mean, the answer is, I think, fairly obvious.

75
00:07:07,800 --> 00:07:12,800
I guess there's something a little bit more to your question.

76
00:07:12,800 --> 00:07:21,800
And it's more the simplicity of just telling people, right?

77
00:07:21,800 --> 00:07:31,800
But the answer is still stands, and it's that...

78
00:07:31,800 --> 00:07:33,800
I suppose twofold.

79
00:07:33,800 --> 00:07:43,800
First of all, it's a waste of time that could be better used teaching.

80
00:07:43,800 --> 00:07:52,800
But it's also a distraction.

81
00:07:52,800 --> 00:07:56,800
So not only did he have better things to teach,

82
00:07:56,800 --> 00:08:00,800
but if you start teaching people about technology,

83
00:08:00,800 --> 00:08:02,800
I mean, really, it's kind of ridiculous.

84
00:08:02,800 --> 00:08:06,800
I mean, suppose the Buddha had taught about electromagnetism.

85
00:08:06,800 --> 00:08:08,800
I mean, this is assuming he is omniscient,

86
00:08:08,800 --> 00:08:12,800
and I know there's a lot of criticism of that claim.

87
00:08:12,800 --> 00:08:16,800
I mean, I'm not in a place to say either way,

88
00:08:16,800 --> 00:08:19,800
but assuming he was omniscient,

89
00:08:19,800 --> 00:08:21,800
and it's not really omniscient either.

90
00:08:21,800 --> 00:08:24,800
You have to understand what omniscience means.

91
00:08:24,800 --> 00:08:27,800
Omniscience means in Buddhism,

92
00:08:27,800 --> 00:08:30,800
it's not possible to be omniscient.

93
00:08:30,800 --> 00:08:35,800
But what is possible is that any question you put to your mind,

94
00:08:35,800 --> 00:08:37,800
the answer comes immediately.

95
00:08:37,800 --> 00:08:42,800
So I mean, probably a simpler question to answer to your question

96
00:08:42,800 --> 00:08:44,800
would be he would have never thought to ask...

97
00:08:44,800 --> 00:08:48,800
He would never have thought to ask the question.

98
00:08:48,800 --> 00:08:51,800
But not only would he have never thought to himself,

99
00:08:51,800 --> 00:08:54,800
hey, what is electromagnetism?

100
00:08:54,800 --> 00:09:00,800
He also wouldn't have had any interest in going there,

101
00:09:00,800 --> 00:09:03,800
because he had clear knowledge of what was of benefit.

102
00:09:03,800 --> 00:09:09,800
And so if he had gone out of his way to try to figure out,

103
00:09:09,800 --> 00:09:13,800
you know, how to make an iPad,

104
00:09:13,800 --> 00:09:15,800
imagine what that would have done.

105
00:09:15,800 --> 00:09:18,800
What a distraction that would have been,

106
00:09:18,800 --> 00:09:23,800
if he had explained about electromagnetism

107
00:09:23,800 --> 00:09:26,800
or even penicillin, even penicillin,

108
00:09:26,800 --> 00:09:30,800
which would have saved people and helped people.

109
00:09:30,800 --> 00:09:33,800
The distraction that it would have provided.

110
00:09:33,800 --> 00:09:35,800
Give you an example.

111
00:09:35,800 --> 00:09:39,800
There's this monk who lives in California,

112
00:09:39,800 --> 00:09:45,800
who's spent a lot of his time giving people,

113
00:09:45,800 --> 00:09:49,800
providing people with cures to their sicknesses.

114
00:09:49,800 --> 00:09:52,800
And he apparently has magical, some kind of magical power.

115
00:09:52,800 --> 00:09:55,800
I mean, this is all, he's very famous.

116
00:09:55,800 --> 00:09:59,800
And he was able to build a very beautiful monastery in California.

117
00:09:59,800 --> 00:10:03,800
Just because of his renown.

118
00:10:03,800 --> 00:10:10,800
And I've been critical of him because it's not really proper

119
00:10:10,800 --> 00:10:13,800
for monks to provide medical advice.

120
00:10:13,800 --> 00:10:15,800
It's where I actually forbidden to do it.

121
00:10:15,800 --> 00:10:17,800
And it's for this sort of reason.

122
00:10:17,800 --> 00:10:22,800
And also for the reason that then people expect it for monks.

123
00:10:22,800 --> 00:10:26,800
And other monks who are practicing in teaching meditation have difficulty,

124
00:10:26,800 --> 00:10:31,800
but mostly because it just distracts people away from meditation.

125
00:10:31,800 --> 00:10:33,800
And that's exactly what's happened.

126
00:10:33,800 --> 00:10:35,800
Apparently someone came to him and asked him,

127
00:10:35,800 --> 00:10:36,800
why don't you teach meditation?

128
00:10:36,800 --> 00:10:38,800
And he said, well, I would like to.

129
00:10:38,800 --> 00:10:41,800
But I have so many people coming to me now that

130
00:10:41,800 --> 00:10:45,800
and everyone's just coming because they want their sicknesses cured.

131
00:10:45,800 --> 00:10:48,800
And they have people who are always coming from medicine.

132
00:10:48,800 --> 00:10:50,800
And so he has no time to teach.

133
00:10:50,800 --> 00:10:55,800
And so even curing people's sicknesses

134
00:10:55,800 --> 00:11:00,800
would have a deleterious effect on one's ability to teach.

135
00:11:04,800 --> 00:11:09,800
It would make things worse, not better, ironically.

136
00:11:12,800 --> 00:11:17,800
So I guess it was an interesting question of some interesting discussion.

137
00:11:17,800 --> 00:11:20,800
I hope that helped to answer.

138
00:11:20,800 --> 00:11:21,800
To apologize.

139
00:11:21,800 --> 00:11:26,800
I don't mean I often feel like I'm quite negative about these questions

140
00:11:26,800 --> 00:11:31,800
and it's important to be sensitive to the fact that there are people out there who are.

141
00:11:33,800 --> 00:11:36,800
Who hear you say, that's a stupid question.

142
00:11:36,800 --> 00:11:38,800
Not really stupid.

143
00:11:38,800 --> 00:11:45,800
It's just, we have to be, we have to be strict about this.

144
00:11:45,800 --> 00:11:48,800
We have to get to the truth of it.

145
00:11:48,800 --> 00:11:54,800
So I apologize if sometimes it sounds like I'm being hard on you.

146
00:11:58,800 --> 00:12:00,800
Thank you, London.

147
00:12:00,800 --> 00:12:04,800
Is it normal to separate your mind from your body when you meditate?

148
00:12:04,800 --> 00:12:07,800
I ask because today something very odd happened.

149
00:12:07,800 --> 00:12:10,800
I viewed myself after meditation as someone else.

150
00:12:10,800 --> 00:12:12,800
In other words, I existed in my mind,

151
00:12:12,800 --> 00:12:15,800
but I viewed my body as something completely different.

152
00:12:15,800 --> 00:12:20,800
I can only describe it like my body is a suit that I wear each day.

153
00:12:20,800 --> 00:12:23,800
For the record I don't drink or take drugs.

154
00:12:23,800 --> 00:12:25,800
And as far as I know, I'm completely sane.

155
00:12:25,800 --> 00:12:27,800
It felt really peculiar.

156
00:12:27,800 --> 00:12:29,800
Is this normal?

157
00:12:29,800 --> 00:12:32,800
Insane people think they're completely sane.

158
00:12:32,800 --> 00:12:41,800
Which is interesting because I would say we're all a little bit insane.

159
00:12:41,800 --> 00:12:48,800
Just different degrees of insanity until you become an arahant who would be completely sane.

160
00:12:48,800 --> 00:12:50,800
But that's, I think, neither here nor there.

161
00:12:50,800 --> 00:12:51,800
It is quite common.

162
00:12:51,800 --> 00:12:55,800
Relatively speaking, which, you know, common would be there.

163
00:12:55,800 --> 00:12:59,800
If I had to guess maybe one in a hundred meditators,

164
00:12:59,800 --> 00:13:03,800
this is one in a hundred people who do serious meditation.

165
00:13:03,800 --> 00:13:07,800
Which I think is pretty common.

166
00:13:07,800 --> 00:13:13,800
It's probably even more common than that.

167
00:13:13,800 --> 00:13:16,800
But for the rest of you who are thinking, why doesn't that happen to me?

168
00:13:16,800 --> 00:13:19,800
Well, it's not that common.

169
00:13:19,800 --> 00:13:21,800
Depends on the individual.

170
00:13:21,800 --> 00:13:25,800
But I've heard many stories like this.

171
00:13:25,800 --> 00:13:30,800
Yeah, it happens.

172
00:13:30,800 --> 00:13:37,800
But my teacher once said it told a story about this monk who left his body.

173
00:13:37,800 --> 00:13:40,800
And he was, he was want to do this.

174
00:13:40,800 --> 00:13:44,800
So his students didn't know it.

175
00:13:44,800 --> 00:13:50,800
And one day they came in to see him and found that, you know, he wasn't responsive.

176
00:13:50,800 --> 00:13:54,800
And they called to him and he didn't move.

177
00:13:54,800 --> 00:14:01,800
And so they, and he didn't, he didn't, he didn't extrapolate on it.

178
00:14:01,800 --> 00:14:04,800
But they gave him an injection.

179
00:14:04,800 --> 00:14:07,800
And I'm assuming it wouldn't have been an injection of formaldehyde.

180
00:14:07,800 --> 00:14:09,800
Like they thought he was dead.

181
00:14:09,800 --> 00:14:13,800
But they gave him some kind of injection.

182
00:14:13,800 --> 00:14:16,800
And because of the injection, he couldn't come back.

183
00:14:16,800 --> 00:14:18,800
I think they must have felt the pulse, right?

184
00:14:18,800 --> 00:14:19,800
He wasn't dead.

185
00:14:19,800 --> 00:14:22,800
So there would have still been a pulse.

186
00:14:22,800 --> 00:14:25,800
And they thought he was, he was in a coma or something.

187
00:14:25,800 --> 00:14:28,800
And so they took the, they brought the doctor and the doctor gave him an injection.

188
00:14:28,800 --> 00:14:32,800
And because of whatever the injection was, he wasn't able to come back.

189
00:14:32,800 --> 00:14:34,800
And so he was trying to get back into his body.

190
00:14:34,800 --> 00:14:37,800
And he wasn't able to connect again.

191
00:14:37,800 --> 00:14:39,800
Which was interesting.

192
00:14:39,800 --> 00:14:42,800
And so Ajahn said, you know, it's better not to play with it.

193
00:14:42,800 --> 00:14:45,800
But if you have to, you need to put a sign up or something.

194
00:14:45,800 --> 00:14:52,800
And still alive, leave me alone.

195
00:14:52,800 --> 00:14:56,800
But he said, maybe Ajahn said, when it happens, you should come.

196
00:14:56,800 --> 00:15:00,800
Just coming back, coming, coming back and make yourself come back.

197
00:15:00,800 --> 00:15:04,800
And then it's dangerous, he said.

198
00:15:04,800 --> 00:15:14,800
It's probably a good lesson too if you come upon someone meditating.

199
00:15:14,800 --> 00:15:16,800
Don't inject them with anything.

200
00:15:16,800 --> 00:15:17,800
Yeah, he mentioned that.

201
00:15:17,800 --> 00:15:24,800
I mean, that happens in our tradition because we know there's the entering into cessation.

202
00:15:24,800 --> 00:15:29,800
And people can do that for hours as well.

203
00:15:29,800 --> 00:15:31,800
There's one nun who was very good at it.

204
00:15:31,800 --> 00:15:35,800
But to this day, I don't think it really was what she thought it was.

205
00:15:35,800 --> 00:15:40,800
Because she was one of the most foul most things I've ever missed.

206
00:15:40,800 --> 00:15:43,800
She would yell at people and scold them.

207
00:15:43,800 --> 00:15:46,800
It's our killer mosquito once as well.

208
00:15:46,800 --> 00:15:49,800
So I don't think she was actually enlightened in any way.

209
00:15:49,800 --> 00:15:54,800
But she was able to enter into these trance states on will just in a few seconds.

210
00:15:54,800 --> 00:15:59,800
And she thought it was enlightenment.

211
00:15:59,800 --> 00:16:05,800
But they could pick her up when she was in this day.

212
00:16:05,800 --> 00:16:07,800
You could pick her up and turn her around.

213
00:16:07,800 --> 00:16:12,800
And she would be stiff and you could pick her up.

214
00:16:12,800 --> 00:16:15,800
When she came back, she wouldn't have any recollection of it, I think.

215
00:16:25,800 --> 00:16:28,800
And with that, we're all caught up on questions.

216
00:16:28,800 --> 00:16:29,800
OK.

217
00:16:29,800 --> 00:16:32,800
Good night.

218
00:16:32,800 --> 00:16:33,800
Thanks, everyone.

219
00:16:33,800 --> 00:16:34,800
Thanks.

220
00:16:34,800 --> 00:16:35,800
Robin.

221
00:16:35,800 --> 00:16:37,800
Thank you.

222
00:16:37,800 --> 00:16:38,800
Good night.

223
00:16:38,800 --> 00:16:43,800
Good night.

