1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhamapanda.

2
00:00:05,000 --> 00:00:15,000
Today we continue with verse 216, which reads as follows.

3
00:00:15,000 --> 00:00:44,000
From first comes sorrow.

4
00:00:44,000 --> 00:00:53,000
Sorrow from thirst comes danger.

5
00:00:53,000 --> 00:01:02,000
For one who is liberated from thirst, from thirsting, from craving,

6
00:01:02,000 --> 00:01:14,000
there is no sorrow, when it's fear, or when it's danger.

7
00:01:14,000 --> 00:01:23,000
And this verse was supposed to have been taught in response to a story about a

8
00:01:23,000 --> 00:01:29,000
Brahmin householder in the time of the Buddha.

9
00:01:29,000 --> 00:01:34,000
The story goes that in Sawati there was a Brahmin householder.

10
00:01:34,000 --> 00:01:43,000
Brahmans were the upper class or are the upper class in India, one of the upper classes.

11
00:01:43,000 --> 00:01:53,000
And he had some farmland, and he spent his days farming.

12
00:01:53,000 --> 00:02:01,000
And so one story tells us that he was a holder of false views.

13
00:02:01,000 --> 00:02:05,000
So as many people in India at the time of the Buddha,

14
00:02:05,000 --> 00:02:13,000
he had views that were unspecified but contrary to reality.

15
00:02:13,000 --> 00:02:22,000
So probably views about God and views about sacrifice and ritual and all these sorts of things.

16
00:02:22,000 --> 00:02:35,000
And views that there was no result of karma, views that God would protect him, that sort of thing.

17
00:02:35,000 --> 00:02:41,000
And he went out one day to clear his field.

18
00:02:41,000 --> 00:02:45,000
So I guess probably he had some help, maybe, maybe not, maybe he was not wealthy.

19
00:02:45,000 --> 00:02:55,000
But he was spending his day clearing the field, maybe he had a horse and an ox and a plow or something.

20
00:02:55,000 --> 00:03:00,000
And the Buddha was walking for arms round, and he saw the Brahmin.

21
00:03:00,000 --> 00:03:02,000
And it's kind of an interesting story.

22
00:03:02,000 --> 00:03:12,000
The Buddha engaged with people in quite unique ways, not ever the same.

23
00:03:12,000 --> 00:03:21,000
But one thing that he often did when he saw potential in someone,

24
00:03:21,000 --> 00:03:27,000
which the story says that he did in this Brahmin, is he would ask them questions?

25
00:03:27,000 --> 00:03:30,000
Simple questions, not even questions really about the Dhamma,

26
00:03:30,000 --> 00:03:34,000
but he would often ask people questions about what they were doing.

27
00:03:34,000 --> 00:03:38,000
And so he asked the Brahmin, what are you doing Brahmin?

28
00:03:38,000 --> 00:03:41,000
And he said, I'm clearing my field.

29
00:03:41,000 --> 00:03:49,000
And the Buddha would ask these questions, obviously not because he was ignorant of what they were doing.

30
00:03:49,000 --> 00:03:54,000
But it's a means of not only breaking the ice and creating a connection,

31
00:03:54,000 --> 00:03:59,000
which the Buddha seemed to want to do.

32
00:03:59,000 --> 00:04:05,000
But it's also a means of having the Brahmin get a deeper understanding of what he himself was doing.

33
00:04:05,000 --> 00:04:09,000
It's a very good way to get people to become aware of what they're doing.

34
00:04:09,000 --> 00:04:12,000
To be more mindful, right?

35
00:04:12,000 --> 00:04:16,000
It's a great way to start a relationship in the Dhamma,

36
00:04:16,000 --> 00:04:19,000
to ask people what they're doing.

37
00:04:19,000 --> 00:04:28,000
And as a teacher, like the Buddha, to impart this sense of presence.

38
00:04:28,000 --> 00:04:31,000
He said, I'm clearing my field.

39
00:04:31,000 --> 00:04:35,000
And the Buddha continued on his way for homes.

40
00:04:35,000 --> 00:04:40,000
And the next day he came back and the Brahmin was maybe he was still clearing his field,

41
00:04:40,000 --> 00:04:48,000
but eventually he started to plant the rice.

42
00:04:48,000 --> 00:04:50,000
And the Buddha asked him, what are you doing?

43
00:04:50,000 --> 00:04:52,000
He said, I'm planting my rice.

44
00:04:52,000 --> 00:04:56,000
And every day the Buddha would go buy and ask him what he's doing.

45
00:04:56,000 --> 00:04:58,000
He said, I'm planting the rice.

46
00:04:58,000 --> 00:05:01,000
Now I'm weeding the field.

47
00:05:01,000 --> 00:05:05,000
Now I'm maybe it wasn't rice. I don't think you weed rice,

48
00:05:05,000 --> 00:05:09,000
but he was weeding the field and whatever it was.

49
00:05:09,000 --> 00:05:11,000
Tending it, guarding it.

50
00:05:11,000 --> 00:05:13,000
Every day he told him what he was doing.

51
00:05:13,000 --> 00:05:18,000
Till eventually he got a sort of a friendship with the Buddha.

52
00:05:18,000 --> 00:05:21,000
And he said to the Buddha one day he said,

53
00:05:21,000 --> 00:05:33,000
bow, go to my bow is not very respectful, so he didn't really call him venerable,

54
00:05:33,000 --> 00:05:37,000
but he said, Mr. Go to my kind of thing.

55
00:05:37,000 --> 00:05:40,000
Go to my Buddha's name.

56
00:05:40,000 --> 00:05:46,000
He said, every day you come by and you ask me about my rice field.

57
00:05:46,000 --> 00:05:50,000
I think I've decided that you and I should be partners in this.

58
00:05:50,000 --> 00:05:53,000
And he said, even I will be partners in this.

59
00:05:53,000 --> 00:05:57,000
And he said, when the rice is, when that's another rice,

60
00:05:57,000 --> 00:06:01,000
when the crop is ripe, I will share it with you.

61
00:06:01,000 --> 00:06:06,000
I won't eat any of my crops without sharing with you.

62
00:06:06,000 --> 00:06:10,000
He said, you'll be my partner in this.

63
00:06:10,000 --> 00:06:15,000
And the Buddha was silent and continued on an arms route.

64
00:06:15,000 --> 00:06:19,000
Eventually the crops grew and it was the night before

65
00:06:19,000 --> 00:06:21,000
the crops were due to be harvested.

66
00:06:21,000 --> 00:06:25,000
And he said, tomorrow I'm going to harvest like crops.

67
00:06:25,000 --> 00:06:30,000
And he was, I guess, quite excited about this and anticipating

68
00:06:30,000 --> 00:06:36,000
not only enjoying it himself, but sharing it with his partner.

69
00:06:36,000 --> 00:06:41,000
And that night there was a great rainstorm of flood.

70
00:06:41,000 --> 00:06:48,000
And it flooded away his entire crop, lost it all.

71
00:06:48,000 --> 00:06:53,000
And in the morning he woke up and went to see his crop,

72
00:06:53,000 --> 00:06:59,000
how it fared and found that it was gone.

73
00:06:59,000 --> 00:07:02,000
And he was devastated.

74
00:07:02,000 --> 00:07:08,000
He thought immediately, here I promised to share it with my friend,

75
00:07:08,000 --> 00:07:09,000
go to mine.

76
00:07:09,000 --> 00:07:11,000
I can't share any of it.

77
00:07:11,000 --> 00:07:15,000
And I can't, of course, enjoy it myself.

78
00:07:15,000 --> 00:07:23,000
And he was depressed and wouldn't eat and just lay in his bed all day.

79
00:07:23,000 --> 00:07:30,000
And the Buddha noticed that he was absent and went to his door.

80
00:07:30,000 --> 00:07:33,000
And the Brahmin servants told him that the Buddha was here.

81
00:07:33,000 --> 00:07:35,000
And he said, let him in and give him a seat.

82
00:07:35,000 --> 00:07:38,000
And the Buddha came in and sat down and said, where's the Brahmin?

83
00:07:38,000 --> 00:07:42,000
He said, oh, he's in bed.

84
00:07:42,000 --> 00:07:46,000
And the Buddha said, tell him to come out.

85
00:07:46,000 --> 00:07:50,000
And the Brahmin came out and sat down and paid.

86
00:07:50,000 --> 00:07:55,000
He said, converse that the Buddha gave friendly greetings.

87
00:07:55,000 --> 00:07:57,000
And the Buddha asked him, what's wrong?

88
00:07:57,000 --> 00:08:01,000
And then he said, Brahmin explained that the crop had disappeared.

89
00:08:01,000 --> 00:08:05,000
And because of that, he was devastated.

90
00:08:05,000 --> 00:08:06,000
He was sad.

91
00:08:06,000 --> 00:08:10,000
He was depressed.

92
00:08:10,000 --> 00:08:15,000
And the Buddha said, do you know really why you're depressed?

93
00:08:15,000 --> 00:08:17,000
Why you're sad?

94
00:08:17,000 --> 00:08:23,000
That's an interesting question because the common answer, of course,

95
00:08:23,000 --> 00:08:25,000
is, well, I'm sad because I lost my crop.

96
00:08:25,000 --> 00:08:30,000
I'm sad because what I had hoped to happen didn't happen.

97
00:08:30,000 --> 00:08:34,000
I didn't get what I wanted to put it simply.

98
00:08:34,000 --> 00:08:36,000
And that's why I'm sad.

99
00:08:36,000 --> 00:08:39,000
So why would the Buddha ask such a thing?

100
00:08:39,000 --> 00:08:42,000
And the Brahmin, when he heard this, he thought,

101
00:08:42,000 --> 00:08:44,000
he said, he responded.

102
00:08:44,000 --> 00:08:47,000
He said, he admitted that.

103
00:08:47,000 --> 00:08:49,000
No, actually, I don't know.

104
00:08:49,000 --> 00:08:52,000
He realized it's deeper than just that.

105
00:08:52,000 --> 00:08:54,000
And he said, but you know.

106
00:08:54,000 --> 00:08:59,000
And the Buddha said, I do.

107
00:08:59,000 --> 00:09:05,000
It's because of Tana craving or thirst.

108
00:09:05,000 --> 00:09:08,000
The literal translation is thirst, but it just means craving.

109
00:09:08,000 --> 00:09:13,000
Because of craving, it comes fear.

110
00:09:13,000 --> 00:09:19,000
If fear arises, it arises because of, sorry,

111
00:09:19,000 --> 00:09:25,000
if sorrow arises, it arises because of craving.

112
00:09:25,000 --> 00:09:33,000
And then he thought this first.

113
00:09:33,000 --> 00:09:38,000
So the lesson here is about that distinction.

114
00:09:38,000 --> 00:09:41,000
This is our final verse in this series of verses

115
00:09:41,000 --> 00:09:43,000
that are all the same.

116
00:09:43,000 --> 00:09:45,000
It's the same verse, just a different word,

117
00:09:45,000 --> 00:09:47,000
and a different story.

118
00:09:47,000 --> 00:09:49,000
And you can see it's actually quite remarkable.

119
00:09:49,000 --> 00:09:51,000
If you just read the verses, you don't realize it.

120
00:09:51,000 --> 00:09:53,000
If you're reading through the verses,

121
00:09:53,000 --> 00:09:56,000
it seems like, oh boy, it's the same verse again.

122
00:09:56,000 --> 00:09:57,000
And again, I get it the first time.

123
00:09:57,000 --> 00:09:59,000
Oh, it's just a different word.

124
00:09:59,000 --> 00:10:01,000
But each story gives us a different facet

125
00:10:01,000 --> 00:10:03,000
to the dangers of craving.

126
00:10:03,000 --> 00:10:06,000
The dangers of what is ultimately the same emotional

127
00:10:06,000 --> 00:10:09,000
mind state, but different circumstances,

128
00:10:09,000 --> 00:10:11,000
different qualities to it.

129
00:10:11,000 --> 00:10:14,000
So this one is in regards to ambition,

130
00:10:14,000 --> 00:10:15,000
really.

131
00:10:15,000 --> 00:10:22,000
Ambition meaning in the expectation of worldly success.

132
00:10:22,000 --> 00:10:27,000
In the sense of engaging in an activity

133
00:10:27,000 --> 00:10:29,000
to get a result in the world,

134
00:10:29,000 --> 00:10:32,000
usually in economic activity.

135
00:10:32,000 --> 00:10:35,000
So in this case, he engaged in,

136
00:10:35,000 --> 00:10:40,000
sort of, farming,

137
00:10:40,000 --> 00:10:42,000
agriculture, that's the word,

138
00:10:42,000 --> 00:10:44,000
engaged in agriculture,

139
00:10:44,000 --> 00:10:46,000
in order to make a profit,

140
00:10:46,000 --> 00:10:48,000
profit for himself and profit

141
00:10:48,000 --> 00:10:52,000
to be able to share it with his friend, his partner.

142
00:10:52,000 --> 00:11:02,000
And it's a very common experience in life

143
00:11:02,000 --> 00:11:04,000
to have our ambitions,

144
00:11:04,000 --> 00:11:08,000
our undertaking,

145
00:11:08,000 --> 00:11:12,000
not bear the fruit that we had hoped them to.

146
00:11:12,000 --> 00:11:15,000
This is something we see acutely

147
00:11:15,000 --> 00:11:19,000
in these days with people who are suffering

148
00:11:19,000 --> 00:11:22,000
from losing their jobs

149
00:11:22,000 --> 00:11:25,000
from lack of employment,

150
00:11:25,000 --> 00:11:30,000
from having their savings

151
00:11:30,000 --> 00:11:33,000
depleted and their savings

152
00:11:33,000 --> 00:11:37,000
in stocks and retirement funds and so on,

153
00:11:37,000 --> 00:11:40,000
all reduced.

154
00:11:40,000 --> 00:11:43,000
I mean, that's actually the least of it,

155
00:11:43,000 --> 00:11:45,000
but the loss of jobs,

156
00:11:45,000 --> 00:11:48,000
the loss of economic

157
00:11:48,000 --> 00:11:51,000
ability, capacity to feed yourself even,

158
00:11:51,000 --> 00:11:54,000
to pay the bills,

159
00:11:54,000 --> 00:11:58,000
is devastating in these times.

160
00:11:58,000 --> 00:12:01,000
With so many people,

161
00:12:01,000 --> 00:12:05,000
afraid by a danger has arisen,

162
00:12:05,000 --> 00:12:08,000
fear has arisen,

163
00:12:08,000 --> 00:12:13,000
unable to make ends meet

164
00:12:13,000 --> 00:12:17,000
and unable to cope with

165
00:12:17,000 --> 00:12:21,000
their circumstance.

166
00:12:21,000 --> 00:12:23,000
This is a reality.

167
00:12:23,000 --> 00:12:24,000
This is something you can't avoid.

168
00:12:24,000 --> 00:12:27,000
This is something Buddhism doesn't

169
00:12:27,000 --> 00:12:29,000
really hope to avoid,

170
00:12:29,000 --> 00:12:32,000
although there are some guidelines

171
00:12:32,000 --> 00:12:34,000
for why it happens.

172
00:12:34,000 --> 00:12:36,000
It doesn't just happen randomly.

173
00:12:36,000 --> 00:12:39,000
And this is a wrong view

174
00:12:39,000 --> 00:12:41,000
and it's a problematic view

175
00:12:41,000 --> 00:12:42,000
that we see in the world

176
00:12:42,000 --> 00:12:45,000
where people believe that

177
00:12:45,000 --> 00:12:47,000
or not even believe perhaps

178
00:12:47,000 --> 00:12:48,000
or just have a sense

179
00:12:48,000 --> 00:12:52,000
that what happens be it pleasant

180
00:12:52,000 --> 00:12:54,000
or unpleasant is by chance.

181
00:12:54,000 --> 00:12:56,000
This is a wrong view.

182
00:12:56,000 --> 00:12:58,000
It's not random,

183
00:12:58,000 --> 00:13:01,000
but for all intents and purposes,

184
00:13:01,000 --> 00:13:02,000
practically speaking,

185
00:13:02,000 --> 00:13:03,000
it is random.

186
00:13:03,000 --> 00:13:05,000
In the sense that we don't know

187
00:13:05,000 --> 00:13:07,000
what's going to happen,

188
00:13:07,000 --> 00:13:10,000
we can't control what's going to happen.

189
00:13:10,000 --> 00:13:12,000
It's not because I want myself

190
00:13:12,000 --> 00:13:14,000
to only experience good things

191
00:13:14,000 --> 00:13:18,000
that good things happen.

192
00:13:18,000 --> 00:13:22,000
The sense that there is some non-random

193
00:13:22,000 --> 00:13:24,000
or non-random quality to it

194
00:13:24,000 --> 00:13:26,000
is in the sense that it relates

195
00:13:26,000 --> 00:13:28,000
to our relationship with the world,

196
00:13:28,000 --> 00:13:30,000
karma, our relationship with others,

197
00:13:30,000 --> 00:13:32,000
with the world in general,

198
00:13:32,000 --> 00:13:35,000
our relationship with reality.

199
00:13:35,000 --> 00:13:36,000
Therefore, out of touch with reality,

200
00:13:36,000 --> 00:13:38,000
we're very much liable to do things

201
00:13:38,000 --> 00:13:40,000
that are going to cause us problems

202
00:13:40,000 --> 00:13:42,000
in the future, not just in this life,

203
00:13:42,000 --> 00:13:45,000
but throughout the moment by moment,

204
00:13:45,000 --> 00:13:50,000
existence in Samsara.

205
00:13:58,000 --> 00:14:00,000
But be that as it may,

206
00:14:00,000 --> 00:14:02,000
at this moment in time,

207
00:14:02,000 --> 00:14:05,000
we don't know what's going to happen.

208
00:14:05,000 --> 00:14:07,000
And so for some people,

209
00:14:07,000 --> 00:14:10,000
it's simply a matter of

210
00:14:10,000 --> 00:14:12,000
hedging your bets.

211
00:14:12,000 --> 00:14:14,000
People have a general sense

212
00:14:14,000 --> 00:14:17,000
in their life that bad things have happened to me,

213
00:14:17,000 --> 00:14:21,000
but I have pretty good luck.

214
00:14:21,000 --> 00:14:22,000
And we get this sense.

215
00:14:22,000 --> 00:14:24,000
We may not express it verbally,

216
00:14:24,000 --> 00:14:25,000
vocally,

217
00:14:25,000 --> 00:14:26,000
though some people do.

218
00:14:26,000 --> 00:14:28,000
I've been pretty lucky in my life here

219
00:14:28,000 --> 00:14:31,000
that's quite common.

220
00:14:31,000 --> 00:14:32,000
But we have a sense that,

221
00:14:32,000 --> 00:14:36,000
okay, I'm doing okay.

222
00:14:36,000 --> 00:14:37,000
And this is the exact,

223
00:14:37,000 --> 00:14:39,000
many of the people who had this,

224
00:14:39,000 --> 00:14:42,000
had this exact same sense

225
00:14:42,000 --> 00:14:45,000
right up until devastation hit.

226
00:14:45,000 --> 00:14:46,000
There's Brahmin,

227
00:14:46,000 --> 00:14:49,000
I was thinking everything was going fine.

228
00:14:49,000 --> 00:14:52,000
And then he was utterly devastated.

229
00:14:52,000 --> 00:14:54,000
We can't avoid.

230
00:14:54,000 --> 00:14:56,000
And thinking that we can avoid

231
00:14:56,000 --> 00:14:58,000
is negligent,

232
00:14:58,000 --> 00:15:00,000
is negligent in the extreme,

233
00:15:00,000 --> 00:15:03,000
thinking that we're going to get lucky

234
00:15:03,000 --> 00:15:06,000
is what everyone who ever experiences

235
00:15:06,000 --> 00:15:08,000
devastation thinks

236
00:15:08,000 --> 00:15:11,000
they weren't prepared for it.

237
00:15:11,000 --> 00:15:12,000
Not everyone.

238
00:15:12,000 --> 00:15:14,000
Some people are pessimistic by nature.

239
00:15:14,000 --> 00:15:16,000
They think very bad things are going to happen,

240
00:15:16,000 --> 00:15:18,000
but for those people,

241
00:15:18,000 --> 00:15:19,000
no, interestingly,

242
00:15:19,000 --> 00:15:21,000
I would say it's less devastating

243
00:15:21,000 --> 00:15:23,000
because they weren't surprised

244
00:15:23,000 --> 00:15:24,000
by it.

245
00:15:24,000 --> 00:15:27,000
They weren't taken off guard by it.

246
00:15:27,000 --> 00:15:29,000
So there's an advantage to being pessimistic.

247
00:15:29,000 --> 00:15:30,000
Of course,

248
00:15:30,000 --> 00:15:33,000
the disadvantage is quite glaring

249
00:15:33,000 --> 00:15:35,000
as that you're all constantly unhappy

250
00:15:35,000 --> 00:15:37,000
if you're pessimistic.

251
00:15:37,000 --> 00:15:43,000
So clearly there is some better way

252
00:15:43,000 --> 00:15:45,000
to engage with reality.

253
00:15:45,000 --> 00:15:47,000
The first point

254
00:15:47,000 --> 00:15:50,000
is that

255
00:15:50,000 --> 00:15:53,000
we have to accept

256
00:15:53,000 --> 00:15:57,000
disappointment as a part of life.

257
00:15:57,000 --> 00:16:00,000
That disappointment is a part of life.

258
00:16:00,000 --> 00:16:02,000
And this is an important lesson

259
00:16:02,000 --> 00:16:04,000
because we often make

260
00:16:04,000 --> 00:16:06,000
like it's not a part of life.

261
00:16:06,000 --> 00:16:09,000
It may not be a part of your life.

262
00:16:09,000 --> 00:16:11,000
Acting like it's not going to be a part of your life

263
00:16:11,000 --> 00:16:14,000
is negligent in the extreme.

264
00:16:14,000 --> 00:16:16,000
It's what causes devastation.

265
00:16:16,000 --> 00:16:18,000
That is the cause of devastation.

266
00:16:18,000 --> 00:16:22,000
That is the root cause of devastation

267
00:16:22,000 --> 00:16:25,000
or it's what's preventing us

268
00:16:25,000 --> 00:16:28,000
from preventing devastation.

269
00:16:28,000 --> 00:16:31,000
If you act

270
00:16:31,000 --> 00:16:34,000
as though it will not come to you,

271
00:16:34,000 --> 00:16:37,000
it will never work

272
00:16:37,000 --> 00:16:40,000
to ensure that it doesn't come to you.

273
00:16:40,000 --> 00:16:41,000
It's not going to come

274
00:16:41,000 --> 00:16:43,000
why work to solve something

275
00:16:43,000 --> 00:16:46,000
that's not going to be a problem.

276
00:16:46,000 --> 00:16:48,000
So understanding

277
00:16:48,000 --> 00:16:52,000
is a big part of what is understanding

278
00:16:52,000 --> 00:16:55,000
that disappointment

279
00:16:55,000 --> 00:16:57,000
is a part of life.

280
00:16:57,000 --> 00:16:59,000
Understanding

281
00:16:59,000 --> 00:17:02,000
that it is a potential

282
00:17:02,000 --> 00:17:05,000
to be part of answering the question

283
00:17:05,000 --> 00:17:07,000
because you need to ask the question first

284
00:17:07,000 --> 00:17:09,000
how do I prepare for it?

285
00:17:09,000 --> 00:17:11,000
And if you don't think you have to

286
00:17:11,000 --> 00:17:14,000
you'll never ask the question.

287
00:17:14,000 --> 00:17:16,000
But the bigger lesson

288
00:17:16,000 --> 00:17:18,000
is the lesson that the Buddha gives

289
00:17:18,000 --> 00:17:21,000
and that is

290
00:17:21,000 --> 00:17:25,000
that it is not that disappointment

291
00:17:25,000 --> 00:17:29,000
the physical reality of disappointment

292
00:17:29,000 --> 00:17:32,000
that is the cause of suffering.

293
00:17:32,000 --> 00:17:34,000
You don't suffer because

294
00:17:34,000 --> 00:17:37,000
you didn't get what you want.

295
00:17:37,000 --> 00:17:39,000
You suffer because you wanted it

296
00:17:39,000 --> 00:17:41,000
in the first place.

297
00:17:41,000 --> 00:17:43,000
And so this Brahman

298
00:17:43,000 --> 00:17:45,000
was quite excited

299
00:17:45,000 --> 00:17:46,000
and you might even say

300
00:17:46,000 --> 00:17:48,000
that the Buddha set him up.

301
00:17:48,000 --> 00:17:50,000
It may very well be.

302
00:17:50,000 --> 00:17:51,000
You could say that.

303
00:17:51,000 --> 00:17:53,000
The Buddha made him more excited, right?

304
00:17:53,000 --> 00:17:55,000
Because the Buddha kept asking him

305
00:17:55,000 --> 00:17:57,000
and kept making him think,

306
00:17:57,000 --> 00:18:02,000
oh, yes, yes, I am going to enjoy this.

307
00:18:02,000 --> 00:18:03,000
I don't know if that's true.

308
00:18:03,000 --> 00:18:05,000
I don't want to pin that on the Buddha,

309
00:18:05,000 --> 00:18:08,000
but it would be a useful lesson tool

310
00:18:08,000 --> 00:18:10,000
if you think that way.

311
00:18:10,000 --> 00:18:13,000
Take people's natural ignorance

312
00:18:13,000 --> 00:18:14,000
and obliviousness

313
00:18:14,000 --> 00:18:17,000
to potential suffering

314
00:18:17,000 --> 00:18:19,000
when you know that something bad

315
00:18:19,000 --> 00:18:21,000
is never going to happen

316
00:18:21,000 --> 00:18:25,000
and using it to teach them something.

317
00:18:25,000 --> 00:18:33,000
It's a bit cruel, I think.

318
00:18:33,000 --> 00:18:36,000
But this Brahman

319
00:18:36,000 --> 00:18:39,000
was not, he didn't suffer

320
00:18:39,000 --> 00:18:41,000
because he didn't wasn't able

321
00:18:41,000 --> 00:18:43,000
to harvest his crop.

322
00:18:43,000 --> 00:18:45,000
He was suffering because of this build-up.

323
00:18:45,000 --> 00:18:47,000
Build-up and build-up and build-up.

324
00:18:47,000 --> 00:18:53,000
He built-up such excitement

325
00:18:53,000 --> 00:18:56,000
that has nothing to do with the reality

326
00:18:56,000 --> 00:18:59,000
of cultivating a crop

327
00:18:59,000 --> 00:19:01,000
and then harvesting it

328
00:19:01,000 --> 00:19:02,000
or cultivating a crop

329
00:19:02,000 --> 00:19:04,000
and then not being able to harvest it.

330
00:19:04,000 --> 00:19:06,000
I mean, theoretically

331
00:19:06,000 --> 00:19:08,000
one can do all the work

332
00:19:08,000 --> 00:19:10,000
and get no results from it.

333
00:19:10,000 --> 00:19:12,000
No, not because

334
00:19:12,000 --> 00:19:14,000
it was never going to bring result

335
00:19:14,000 --> 00:19:16,000
but because of some accident

336
00:19:16,000 --> 00:19:20,000
or some fortuitous event,

337
00:19:20,000 --> 00:19:25,000
and then not suffer from it.

338
00:19:25,000 --> 00:19:27,000
It's possible to have all that happen

339
00:19:27,000 --> 00:19:30,000
and be completely at peace

340
00:19:30,000 --> 00:19:32,000
or at least indifferent,

341
00:19:32,000 --> 00:19:35,000
for many reasons.

342
00:19:35,000 --> 00:19:37,000
For example, if it wasn't your crop,

343
00:19:37,000 --> 00:19:38,000
if it was someone else's

344
00:19:38,000 --> 00:19:39,000
and you were just doing the work

345
00:19:39,000 --> 00:19:41,000
and you got paid anyway,

346
00:19:41,000 --> 00:19:42,000
you wouldn't care at all

347
00:19:42,000 --> 00:19:46,000
whether the crop could be harvested or not.

348
00:19:46,000 --> 00:19:48,000
But his identification,

349
00:19:48,000 --> 00:19:53,000
his investment in the results devastated him.

350
00:19:53,000 --> 00:19:55,000
It's not to say

351
00:19:55,000 --> 00:19:57,000
we can dismiss

352
00:19:57,000 --> 00:19:59,000
the great tragedy

353
00:19:59,000 --> 00:20:01,000
that this brahman suffered.

354
00:20:01,000 --> 00:20:03,000
I mean, it sounds like he still had servant

355
00:20:03,000 --> 00:20:05,000
so he probably wasn't destitute

356
00:20:05,000 --> 00:20:07,000
but there was a loss of livelihood

357
00:20:07,000 --> 00:20:09,000
on his part.

358
00:20:09,000 --> 00:20:12,000
It was expecting

359
00:20:12,000 --> 00:20:15,000
and it probably impacted his lifestyle

360
00:20:15,000 --> 00:20:18,000
in people in today's world

361
00:20:18,000 --> 00:20:19,000
and throughout history,

362
00:20:19,000 --> 00:20:21,000
but what we see today

363
00:20:21,000 --> 00:20:24,000
in the circumstances of the world is facing now.

364
00:20:24,000 --> 00:20:30,000
A lot of people are in fairly dire straits.

365
00:20:30,000 --> 00:20:32,000
And there's no dismissing that.

366
00:20:32,000 --> 00:20:35,000
It's true that it's impact.

367
00:20:35,000 --> 00:20:37,000
There's a profound impact.

368
00:20:37,000 --> 00:20:39,000
That is not the reason

369
00:20:39,000 --> 00:20:44,000
why people are sad, depressed,

370
00:20:44,000 --> 00:20:48,000
fearful, worried about the future,

371
00:20:48,000 --> 00:20:54,000
worried about the present, angry, frustrated,

372
00:20:54,000 --> 00:20:56,000
and so on.

373
00:20:56,000 --> 00:20:59,000
All of that is

374
00:20:59,000 --> 00:21:01,000
for a very different reason.

375
00:21:01,000 --> 00:21:04,000
It's because of our attachment

376
00:21:04,000 --> 00:21:07,000
to results and our attachment to pleasure

377
00:21:07,000 --> 00:21:12,000
because it's not just the anticipation of pleasure.

378
00:21:12,000 --> 00:21:15,000
It's the pleasure in anticipating

379
00:21:15,000 --> 00:21:17,000
this brahman,

380
00:21:17,000 --> 00:21:19,000
as with all the other stories,

381
00:21:19,000 --> 00:21:20,000
as with the last story,

382
00:21:20,000 --> 00:21:25,000
the story of this man who made the beautiful golden image

383
00:21:25,000 --> 00:21:27,000
of the woman.

384
00:21:27,000 --> 00:21:29,000
It wasn't just the anticipation,

385
00:21:29,000 --> 00:21:30,000
though that was a big part of it.

386
00:21:30,000 --> 00:21:31,000
It was also the pleasure.

387
00:21:31,000 --> 00:21:33,000
Every time he thought

388
00:21:33,000 --> 00:21:36,000
of being married to such a beautiful woman.

389
00:21:36,000 --> 00:21:39,000
Every time this brahman talked with the Buddha

390
00:21:39,000 --> 00:21:43,000
and thought about what he was going to experience,

391
00:21:43,000 --> 00:21:45,000
there was pleasure associated with it.

392
00:21:45,000 --> 00:21:50,000
That's what causes greed to be such a craving

393
00:21:50,000 --> 00:21:54,000
to be such a pernicious mind state,

394
00:21:54,000 --> 00:21:57,000
such a difficult mind state to overcome.

395
00:21:57,000 --> 00:21:59,000
As it's pleasant,

396
00:21:59,000 --> 00:22:01,000
it's enjoyable.

397
00:22:01,000 --> 00:22:03,000
It's not just enjoyable getting what you want.

398
00:22:03,000 --> 00:22:06,000
It's also enjoyable wanting it.

399
00:22:06,000 --> 00:22:08,000
There's a pleasure involved.

400
00:22:08,000 --> 00:22:09,000
Not always.

401
00:22:09,000 --> 00:22:13,000
It can be neutral, especially if you're quite mindful.

402
00:22:13,000 --> 00:22:20,000
But there's often quite a pleasure that encourages the desire.

403
00:22:20,000 --> 00:22:27,000
It's why it's very lightly blamed.

404
00:22:27,000 --> 00:22:30,000
We don't blame people for wanting things.

405
00:22:30,000 --> 00:22:32,000
Not the way we blame people for getting angry

406
00:22:32,000 --> 00:22:35,000
or being conceited and arrogant and so on.

407
00:22:35,000 --> 00:22:38,000
In the world, it's rare.

408
00:22:38,000 --> 00:22:44,000
More rare to scold or blame someone for being greedy.

409
00:22:44,000 --> 00:22:45,000
We do.

410
00:22:45,000 --> 00:22:47,000
It happens if someone is very greedy,

411
00:22:47,000 --> 00:22:48,000
excessively.

412
00:22:48,000 --> 00:22:49,000
But just for wanting things,

413
00:22:49,000 --> 00:22:55,000
no, not the same way as when people get angry or so on.

414
00:22:55,000 --> 00:22:57,000
We don't blame it in ourselves either.

415
00:22:57,000 --> 00:23:00,000
When we get angry, we don't like that.

416
00:23:00,000 --> 00:23:02,000
But we don't say, oh, I don't like being so.

417
00:23:02,000 --> 00:23:04,000
I don't like liking things.

418
00:23:04,000 --> 00:23:05,000
We like liking things.

419
00:23:05,000 --> 00:23:06,000
We enjoy them.

420
00:23:06,000 --> 00:23:09,000
Unless you're perhaps you've heard Buddhist teachings

421
00:23:09,000 --> 00:23:12,000
and you realize, oh, there's danger and so you get upset

422
00:23:12,000 --> 00:23:14,000
with yourself.

423
00:23:14,000 --> 00:23:19,000
It's not very wholesome either, but in general,

424
00:23:19,000 --> 00:23:21,000
we enjoy liking it.

425
00:23:21,000 --> 00:23:24,000
And this is what the Brahman was cultivating.

426
00:23:24,000 --> 00:23:33,000
This joy, this excitement that was going to come into fruition.

427
00:23:33,000 --> 00:23:38,000
Now, it's not to praise greed or craving because it's pleasant.

428
00:23:38,000 --> 00:23:43,000
It actually makes it worse because the pleasant feeling doesn't

429
00:23:43,000 --> 00:23:47,000
assuage or doesn't, doesn't appease the craving.

430
00:23:47,000 --> 00:23:52,000
It makes it worse, makes it stronger.

431
00:23:52,000 --> 00:23:57,000
It's not the kind of pleasure that is content.

432
00:23:57,000 --> 00:24:01,000
The pleasure involved with craving reinforces the craving.

433
00:24:01,000 --> 00:24:06,000
You want something and you're excited or you're happy about what you're going to get.

434
00:24:06,000 --> 00:24:10,000
You only want it more.

435
00:24:10,000 --> 00:24:12,000
The devastation increases.

436
00:24:12,000 --> 00:24:23,000
This is the danger in craving, the danger in the pleasure of craving.

437
00:24:23,000 --> 00:24:32,000
And so this is the lesson, this sort of mid-level lesson of how it is craving that

438
00:24:32,000 --> 00:24:33,000
leads to suffering.

439
00:24:33,000 --> 00:24:39,000
Of course, on a deeper level and on a meditative level,

440
00:24:39,000 --> 00:24:44,000
this is sort of the lesson that I've been repeating throughout these verses.

441
00:24:44,000 --> 00:24:49,000
It's that even deeper than this teaching, which is an important teaching

442
00:24:49,000 --> 00:24:55,000
on the disadvantages of craving is the teaching on how even pleasure,

443
00:24:55,000 --> 00:24:58,000
even getting what you want.

444
00:24:58,000 --> 00:25:01,000
And of course, the craving for getting what you want.

445
00:25:01,000 --> 00:25:08,000
All of it is unsatisfying.

446
00:25:08,000 --> 00:25:13,000
So the pleasure of wanting the pleasure of getting what you want,

447
00:25:13,000 --> 00:25:19,000
the experience of it, when you experience it as it is,

448
00:25:19,000 --> 00:25:21,000
you see that it's not worth clinging to.

449
00:25:21,000 --> 00:25:31,000
But there's nothing special or concrete about it.

450
00:25:31,000 --> 00:25:33,000
It's not a real thing.

451
00:25:33,000 --> 00:25:37,000
It's real in the sense that it's not real in the sense of

452
00:25:37,000 --> 00:25:43,000
this lasting, in the sense of being something you can hold on to,

453
00:25:43,000 --> 00:25:47,000
when craving arises, it arises temporarily, momentarily.

454
00:25:47,000 --> 00:25:51,000
When pleasure arises, it arises momentarily.

455
00:25:51,000 --> 00:25:54,000
It arises like everything else.

456
00:25:54,000 --> 00:25:57,000
It ceases, everything that arises ceases.

457
00:25:57,000 --> 00:25:58,000
And this isn't theory.

458
00:25:58,000 --> 00:26:00,000
This is what you see when you practice.

459
00:26:00,000 --> 00:26:03,000
As you watch, if you like something and you say liking,

460
00:26:03,000 --> 00:26:07,000
if you want something and you say wanting wanting,

461
00:26:07,000 --> 00:26:11,000
if you feel happy and you say happy, happy,

462
00:26:11,000 --> 00:26:13,000
you don't learn anything profound about it.

463
00:26:13,000 --> 00:26:15,000
You don't suddenly get this billboard that tells you

464
00:26:15,000 --> 00:26:18,000
that's not worth clinging to.

465
00:26:18,000 --> 00:26:19,000
Nothing happens.

466
00:26:19,000 --> 00:26:21,000
You see there's nothing to it.

467
00:26:21,000 --> 00:26:23,000
And that it takes time.

468
00:26:23,000 --> 00:26:25,000
But eventually you realize that's what insight is.

469
00:26:25,000 --> 00:26:28,000
That's what we pass in the means.

470
00:26:28,000 --> 00:26:30,000
We pass in as seeing that there's nothing to it.

471
00:26:30,000 --> 00:26:32,000
There's nothing special.

472
00:26:32,000 --> 00:26:35,000
There's no billboard that will suddenly flash

473
00:26:35,000 --> 00:26:37,000
and tell you what this pleasure is,

474
00:26:37,000 --> 00:26:39,000
what this craving is.

475
00:26:39,000 --> 00:26:41,000
You'll see that craving is just craving.

476
00:26:41,000 --> 00:26:44,000
It's an experience that arises and ceases.

477
00:26:44,000 --> 00:26:45,000
It's very simple.

478
00:26:45,000 --> 00:26:47,000
It's not much to it.

479
00:26:47,000 --> 00:26:48,000
But that's how you'll see it.

480
00:26:48,000 --> 00:26:51,000
And when you start to see it that way,

481
00:26:51,000 --> 00:26:55,000
you'll see no reason to cling to anything.

482
00:26:55,000 --> 00:26:58,000
No one will tell you nothing will tell you not to cling.

483
00:26:58,000 --> 00:27:02,000
It's not this lesson of, oh yeah, that might lead to problems.

484
00:27:02,000 --> 00:27:05,000
It's the lesson of, this is a thing that arose in ceased.

485
00:27:05,000 --> 00:27:08,000
And so craving doesn't have any place.

486
00:27:08,000 --> 00:27:10,000
When you see things just as they are,

487
00:27:10,000 --> 00:27:11,000
there's no room for clinging.

488
00:27:11,000 --> 00:27:16,000
There's no room for craving.

489
00:27:16,000 --> 00:27:21,000
So this is the last verse in this series of verses.

490
00:27:21,000 --> 00:27:25,000
We're still in the Piawaga, I think.

491
00:27:25,000 --> 00:27:27,000
So there's a couple of more about the word Pia,

492
00:27:27,000 --> 00:27:30,000
but they're on different topics.

493
00:27:30,000 --> 00:27:33,000
And so this ends a series of talks that I think are,

494
00:27:33,000 --> 00:27:36,000
the teachings are very important.

495
00:27:36,000 --> 00:27:39,000
They give us all the many facets.

496
00:27:39,000 --> 00:27:40,000
As I said, if you read the verses,

497
00:27:40,000 --> 00:27:42,000
you don't see this, but by looking at all these stories,

498
00:27:42,000 --> 00:27:46,000
you see, oh yes, many different ways that are very, very common.

499
00:27:46,000 --> 00:27:49,000
We see them every day.

500
00:27:49,000 --> 00:27:56,000
There's such a prevalent reality in our lives.

501
00:27:56,000 --> 00:28:02,000
So now we have losing a child, losing a loved one,

502
00:28:02,000 --> 00:28:05,000
losing the potential for a loved one,

503
00:28:05,000 --> 00:28:07,000
fighting over a romantic interest.

504
00:28:07,000 --> 00:28:11,000
We have the loss of livelihood,

505
00:28:11,000 --> 00:28:13,000
or the loss of expected livelihood,

506
00:28:13,000 --> 00:28:16,000
not getting what you want in the world,

507
00:28:16,000 --> 00:28:20,000
or not getting perhaps what you need to survive.

508
00:28:20,000 --> 00:28:23,000
Again, this isn't to belittle that.

509
00:28:23,000 --> 00:28:25,000
It's not to say that it's not devastating.

510
00:28:25,000 --> 00:28:28,000
It's not significant.

511
00:28:28,000 --> 00:28:33,000
It's just to say that you don't have to be devastated by

512
00:28:33,000 --> 00:28:42,000
the very real consequences or results of events that are outside of your control.

513
00:28:42,000 --> 00:28:43,000
You have two choices.

514
00:28:43,000 --> 00:28:49,000
You can experience it mindfully and suffer physically,

515
00:28:49,000 --> 00:28:53,000
or you can experience it unmindfully and suffer physically,

516
00:28:53,000 --> 00:28:56,000
and mentally. That's the choice.

517
00:28:56,000 --> 00:29:04,000
You can't choose not very readily how you're going to live physically,

518
00:29:04,000 --> 00:29:07,000
but you can choose more readily.

519
00:29:07,000 --> 00:29:15,000
You can work anyway to live in such a way that is free from mental suffering.

520
00:29:15,000 --> 00:29:18,000
No, it takes work. It's not to belittle it,

521
00:29:18,000 --> 00:29:21,000
but it's much more stable and much more powerful

522
00:29:21,000 --> 00:29:24,000
than trying to control the world around you,

523
00:29:24,000 --> 00:29:30,000
so that you don't ever experience disappointment.

524
00:29:30,000 --> 00:29:34,000
So, verse 216, that's the teaching for tonight.

525
00:29:34,000 --> 00:29:59,000
Thank you all for listening.

