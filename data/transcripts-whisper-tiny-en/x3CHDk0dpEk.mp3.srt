1
00:00:00,000 --> 00:00:23,960
Good evening everyone, broadcasting live, February 24, 2016, today we have a quote from the

2
00:00:23,960 --> 00:00:31,720
Sanyatana Kaya, it's a pretty awesome quote, one of those quoteables, so in

3
00:00:31,720 --> 00:00:42,280
Paoli we have Saya, Tapi, Bhikhwa, Kumbu, Anand, Haru, Supa, Tiyo, Hote, Sadharo,

4
00:00:42,280 --> 00:00:53,240
Bhikhwa, Oh Bhikhwa, Oh Bhikhwa, Oh Bhikhwa, Oh Bhikhwa, Oh Bhikhwa, Oh Bhikhwa, You have seen

5
00:00:53,240 --> 00:01:07,440
Bhaya in Samsara, the danger in Samsara, Saya, Tapi, Kumbu, Just as a part, Anand, Haru,

6
00:01:07,440 --> 00:01:16,680
Supa, Tiyo, Hote, Apat, Anand, Haru, Without supports, Supa, Bavat, Tiyo, Hote is easily

7
00:01:16,680 --> 00:01:30,920
overturned, Sadharo, Lu, Bhavat, Tiyo, Tiyo, Tiyo, With supports, that is hard to turn off.

8
00:01:30,920 --> 00:01:42,880
Bhikhwa, Maywakho, Bhikhwa, Jitang, Anand, Haru, Supa, Vatiyang, Hote, Sadharo, Dupa, Vatiyang, Hote,

9
00:01:42,880 --> 00:01:52,080
Ava, Maywakho, even so, because Jitang, Anand, Haru, the mind, without supports,

10
00:01:52,080 --> 00:02:04,040
Supa, Vatiyang, Hote is easily overturned, or upset. Sadharo, Nupa, Vatiyang, Hote, With supports,

11
00:02:04,040 --> 00:02:17,200
Hard to upset. Koj, Vikhwa, Jitasa, Adharo, And what, Oh Bhikhwa's, Is a support for the mind?

12
00:02:17,200 --> 00:02:27,520
Ayameva, Aru, Atang, Gikomongo, this very noble, hateful path. So the noble, hateful

13
00:02:27,520 --> 00:02:34,020
path is a support for the mind. The idea that what is getting out here is that the

14
00:02:34,020 --> 00:02:42,440
hateful, noble path, the eight path factors work together to support the mind. You can't

15
00:02:42,440 --> 00:02:52,400
have a pasture enlightenment without all eight factors. If one factor is wrong, if you

16
00:02:52,400 --> 00:03:03,000
have a wrong view, a wrong thought, or wrong action, wrong speech, wrong action, wrong livelihood,

17
00:03:03,000 --> 00:03:13,840
wrong effort, wrong mindfulness, wrong concentration. Supa Vatiyang, easily overturned,

18
00:03:13,840 --> 00:03:32,560
mind is easily shaken. This idea of being unshaken, this is the essence of the Buddhist

19
00:03:32,560 --> 00:03:40,520
path. There's this idea of being invincible. I mean, they are given, it's the argument

20
00:03:40,520 --> 00:03:55,880
given against staying in some sorrow, against seeking out happiness in in impermanent things,

21
00:03:55,880 --> 00:04:06,600
that sort of thing is, it's unstable. And that sort of thing is very renders you unstable,

22
00:04:06,600 --> 00:04:19,080
vulnerable, invincible. I don't think it's such a word. Suppose the invincible probably

23
00:04:19,080 --> 00:04:27,280
comes from the lad in Winko, Winkery, which is to conquer. Winkable mean would mean to

24
00:04:27,280 --> 00:04:39,240
conquerable, in Winkable, or invincible, would mean to, I'm just guessing, not subject

25
00:04:39,240 --> 00:05:01,800
to being conquered. So, the idea behind Buddhism is to find an unassailable, indisputable,

26
00:05:01,800 --> 00:05:18,640
the kind of state that you can't, that that is free from danger, that has no opening

27
00:05:18,640 --> 00:05:25,560
for attack. So, these, and this is, these are the, these are what the supports of the

28
00:05:25,560 --> 00:05:32,100
8th of normal path give. And the, the claim is, through the practice of the 8th of

29
00:05:32,100 --> 00:05:37,920
normal path, through keeping to the 8th of normal path, if one perfects all 8 of these

30
00:05:37,920 --> 00:05:44,200
factors, right, view, right, thought, right, speech, right, action, right livelihood,

31
00:05:44,200 --> 00:05:52,360
right effort, right mindfulness, right concentration, one is invincible. This, this supports

32
00:05:52,360 --> 00:05:58,800
the mind in all waves, that there's nothing that could break through the defenses, nothing

33
00:05:58,800 --> 00:06:14,280
could tip you over, like a pot. But the 8th of normal path, you might ask, well, can I

34
00:06:14,280 --> 00:06:18,720
just use some of them, some of the supports, or can I work on one of the supports at once,

35
00:06:18,720 --> 00:06:25,400
it doesn't really work that way. And the 8th of normal path, there's a lot more theoretical

36
00:06:25,400 --> 00:06:33,400
than it is practical. In the sense that it's important to know about these things, but

37
00:06:33,400 --> 00:06:37,000
in terms of practice, you don't just go about practicing one and then practicing the

38
00:06:37,000 --> 00:06:41,040
other or so on, perfecting one and then okay, that one's perfect, let's go perfect the

39
00:06:41,040 --> 00:06:46,480
other. It's more that when you practice, your practice has to incorporate all 8 of these.

40
00:06:46,480 --> 00:06:51,920
So if you're practicing insight meditation, and you're lacking one of those, then you can

41
00:06:51,920 --> 00:06:57,800
even know that your practice is lacking. It's not that you actually have to practice all

42
00:06:57,800 --> 00:07:06,800
8, right? In some ways, one way of looking at it is all you need is right view. And once

43
00:07:06,800 --> 00:07:12,080
you have right view, the other seven follow along, another way of looking at it is you start

44
00:07:12,080 --> 00:07:16,400
with mindfulness. And when you have mindfulness, an effort in concentration buildup,

45
00:07:16,400 --> 00:07:23,520
and based on that, all the rest of the path factors, I mean, practically speaking, it's

46
00:07:23,520 --> 00:07:28,880
not so simple as practicing the full number of paths. You have to understand each of the

47
00:07:28,880 --> 00:07:35,480
8 and where they fit in, because it's not like you can say, oh, my livelihood is pure,

48
00:07:35,480 --> 00:07:39,800
so there I pure, if I had livelihood, it's not really what it means. It's just as part

49
00:07:39,800 --> 00:07:51,640
of those things that I have to be abstained from that are vulnerabilities. You need that

50
00:07:51,640 --> 00:07:58,600
support, all 8 are necessary. And they come through proper practice. It's actually not something

51
00:07:58,600 --> 00:08:03,200
you have to worry too much about, like, oh, my missing one is good to see if you are missing

52
00:08:03,200 --> 00:08:08,840
one. But if you practice mindfulness, if you practice at the, if you cultivate the five

53
00:08:08,840 --> 00:08:14,200
faculties and balance them and then strengthen them, the 8 full number of paths will come

54
00:08:14,200 --> 00:08:19,880
by itself. The 8 full number of paths is really at the moment when you see clearly, when

55
00:08:19,880 --> 00:08:27,680
you just see perfectly for the first time. And it's one moment, the noble path, one moment,

56
00:08:27,680 --> 00:08:36,520
cuts the defilement and opens the door to Nirvana. Everything after that is fruit. It's

57
00:08:36,520 --> 00:08:42,480
the fruition, but it's only one moment is the path. So when everything comes together perfectly

58
00:08:42,480 --> 00:08:49,960
and that one moment, they are perfectly supported and the mind is able to break free.

59
00:08:49,960 --> 00:09:01,960
Anyway, we had a couple of visitors this evening, a woman and her father came by to practice

60
00:09:01,960 --> 00:09:09,080
meditation. Her father, I probably made a mistake by teaching the walking meditation first

61
00:09:09,080 --> 00:09:16,800
because I demonstrated walking meditation and father said, oh, now he's got to have some kind

62
00:09:16,800 --> 00:09:22,040
of Scottish accent or some things that that's not really what we were expecting. Now is it?

63
00:09:22,040 --> 00:09:30,880
She said, well, it's exactly what I was expecting. He wasn't having it. So I said, okay,

64
00:09:30,880 --> 00:09:34,360
you can just sit there and you don't have to do it. Probably should have taught the

65
00:09:34,360 --> 00:09:48,680
sitting meditation first. Would have been more palatable, I think. It was kind of cute.

66
00:09:48,680 --> 00:10:00,600
And it was hard of hearing as well. But yeah, so people are coming out. This, um,

67
00:10:00,600 --> 00:10:12,000
some engineers or something, some group, some class they want us to take. They want to

68
00:10:12,000 --> 00:10:21,720
hold a meditation class. Yeah, lots of interesting things. Of course, today was Wednesday.

69
00:10:21,720 --> 00:10:30,520
So I taught meditation in the gym, three new people and three or four old people. So people

70
00:10:30,520 --> 00:10:35,600
coming back every week, which is great. We had, we must have had eight people, nine people

71
00:10:35,600 --> 00:10:43,320
maybe that day. And on Mondays, people are coming Friday is not so much. But then Friday

72
00:10:43,320 --> 00:10:53,080
is I do all day. It's Friday. I'll be doing all day. Five minute meditation lessons. And

73
00:10:53,080 --> 00:11:07,920
usual. So if you have questions, come on the hangout. Thank you, Pandit. I have a question.

74
00:11:07,920 --> 00:11:13,200
All right, go for it. So talking about the perfections, I was just thinking about the ten

75
00:11:13,200 --> 00:11:21,000
perfections, the Buddha mentioned, and how they relate to the moment of time or are they

76
00:11:21,000 --> 00:11:26,760
different from these eight perfections, which are not really called perfections.

77
00:11:26,760 --> 00:11:30,960
Well, these, the word perfect is wrong. You shouldn't have used the word perfect because

78
00:11:30,960 --> 00:11:34,640
it's confusing. It's not with the word, some, I mean, some, that doesn't mean perfect.

79
00:11:34,640 --> 00:11:41,800
It means right. I'm not even sure I don't know the technology, but it's always, it's

80
00:11:41,800 --> 00:11:47,400
always translated as right. Perfect. I don't think it has the sense of perfect. No,

81
00:11:47,400 --> 00:11:54,040
it's means right. It's no question. This monk is the only, he's a really good monk.

82
00:11:54,040 --> 00:12:01,360
And as far as I know, he's on the right, he's a good guy. But he's got some odd translation

83
00:12:01,360 --> 00:12:11,720
down. So yeah, he's translating Samma as perfect. The ten perfections, the word is

84
00:12:11,720 --> 00:12:21,480
Parami or Paramita. And it comes from the word Barah or Parama. Parama means having, having

85
00:12:21,480 --> 00:12:31,560
no, nothing beyond, which means ultimate. So highest. So completions, maybe perfections works

86
00:12:31,560 --> 00:12:39,160
really well for a translation of Paramita. Means that which is the highest. Perfection,

87
00:12:39,160 --> 00:12:44,240
really. Okay, thank you, Rantir.

88
00:12:44,240 --> 00:13:00,320
All right, Larry, good evening. Good evening. Good to see you. Good to see you, thanks.

89
00:13:00,320 --> 00:13:13,000
So I had an exam this afternoon, and the lotus sutra had a chance to take it apart

90
00:13:13,000 --> 00:13:22,880
and criticize. I love to have heard that. I wasn't that critical. You can't be, I mean,

91
00:13:22,880 --> 00:13:34,480
you got to be fair, we're, we're scholars. So we can be polymorphists. But it's kind

92
00:13:34,480 --> 00:13:41,520
of disgusting. A lotus sutra kind of discussed. I'm sorry, I haven't read it myself.

93
00:13:41,520 --> 00:13:53,760
Maybe I could look at it. It's just so obviously, I don't know. It's hard to see them

94
00:13:53,760 --> 00:14:01,000
putting words in the Buddha's mouth. I mean, there's criticism that, well, that polydipitica,

95
00:14:01,000 --> 00:14:04,960
we don't know that the Buddha said all of it, and I'll agree that some of the commentaries

96
00:14:04,960 --> 00:14:11,440
may have taken liberties, but it's not so blatant. So I can't say one way or the other.

97
00:14:11,440 --> 00:14:17,920
This is pretty blatant. I mean, they've made up all sorts of new doctrines, and suddenly

98
00:14:17,920 --> 00:14:24,400
the Buddha's this, I don't know, some kind of angel up in heaven. Everybody in the class

99
00:14:24,400 --> 00:14:28,560
is confused, and I'm like, yeah, I understand your confusion. I'm like, this is Buddhism. No.

100
00:14:32,080 --> 00:14:37,520
So many of them, this is their first introduction of Buddhism, which is a shame because it's

101
00:14:37,520 --> 00:14:43,440
all messed up. But he's doing a good job of showing how it got messed up over the course of

102
00:14:43,440 --> 00:14:50,720
its movement. I mean, I think he actually agrees with Mahayana principles, but Mahayana grew up in

103
00:14:50,720 --> 00:14:59,120
India, and Mahayana in India is different from, I think, even the lotus sutra, lotus sutra is

104
00:14:59,120 --> 00:15:05,920
Indian, but never really gained much ground in India, gained a lot more ground in East Asia. But

105
00:15:06,800 --> 00:15:13,440
Buddhism, in East Asia, is an interesting phenomenon. They developed this idea that the Buddha has

106
00:15:13,440 --> 00:15:20,720
three bodies, so he's actually up in heaven, and in Nirvana, end on earth, and he comes to earth

107
00:15:20,720 --> 00:15:26,960
as a, like, it's just an illusion. His birth and his 45 years of teaching, and the 45 years of

108
00:15:26,960 --> 00:15:40,320
teaching is lies. Wow. That's what Kupayakusa is. He lies in order to get us to the point where we

109
00:15:40,320 --> 00:15:48,960
can hear the truth. He tricks us into becoming our hunts, because that's the best we can do

110
00:15:48,960 --> 00:15:53,280
because we're too, we're too deluded to understand that we all have to become Buddhas.

111
00:15:53,280 --> 00:15:59,600
I mean, I'm not making this up, and this isn't actually what the lotus sutra says, so

112
00:16:00,640 --> 00:16:05,600
I'm probably making a lot of Buddhas to hangry by saying this on the internet. But, you know,

113
00:16:06,480 --> 00:16:12,560
what has that ever stopped? I'm sorry. Is this the same thing as the heart sutra?

114
00:16:13,360 --> 00:16:18,240
Oh, I don't know. Okay. Heart sutra is better actually, isn't that the one where

115
00:16:18,240 --> 00:16:25,120
Rubbang shu nyatan is in path of heart sutra. Really, I can't tell, I'm sorry.

116
00:16:25,120 --> 00:16:29,280
I think the heart sutra is a... But the heart sutra, I don't like it because it's kind of

117
00:16:29,280 --> 00:16:36,560
suphistic. It's soft. You know, softism, like, just sounding intelligent, like it says,

118
00:16:37,200 --> 00:16:42,560
form is emptiness, emptiness is form. What the heck does that mean? And of course they say,

119
00:16:42,560 --> 00:16:46,560
ah, you see, you're not lying on the show, you don't get to understand, and I'm like, go away.

120
00:16:46,560 --> 00:16:49,200
I have no use for yourself, isn't it?

121
00:16:51,200 --> 00:16:58,400
Okay. If you're just sounding... Are you sure anyone can sound... I should stop before I get into trouble.

122
00:16:59,280 --> 00:17:06,480
This is bad. I'm not an experience like that listening to a talk, I think.

123
00:17:06,960 --> 00:17:11,440
I don't know when... If you practice this and then you go in here,

124
00:17:12,320 --> 00:17:13,760
people talk about emptiness.

125
00:17:13,760 --> 00:17:18,640
I don't know if they're all sorts of craziness.

126
00:17:19,600 --> 00:17:25,440
Anyway, it's been a bit too critical of other people's beliefs. I promise that I always

127
00:17:25,440 --> 00:17:34,080
promise that I won't do this. I won't go on and on about other people's practices and beliefs.

128
00:17:34,080 --> 00:17:39,040
Then I do, don't. I have criticized Christianity. I think I've gotten into Islam.

129
00:17:39,040 --> 00:17:44,080
Judaism, have I torn into Judaism yet?

130
00:17:47,120 --> 00:17:49,920
They're all wrong.

131
00:17:51,120 --> 00:17:53,200
It's just not the kind of thing you should say on the internet.

132
00:17:54,800 --> 00:18:00,000
I mean, I like reading criticism and of course, criticism of our own tradition as well.

133
00:18:00,400 --> 00:18:01,680
I just put it out of the internet.

134
00:18:01,680 --> 00:18:03,600
Yeah, I should criticize our own traditions.

135
00:18:03,600 --> 00:18:10,400
Father may open to it. So this is something I've been looking at in case anyone was interested

136
00:18:10,400 --> 00:18:16,160
in criticism of the traditions. I just found that interesting.

137
00:18:17,360 --> 00:18:18,800
It's a Danish picture.

138
00:18:18,800 --> 00:18:27,680
You know, you know something that I didn't hear at all, but I caught part of a speech.

139
00:18:28,880 --> 00:18:33,280
And I want you all to know about this guy because I'm not supposed to be political.

140
00:18:33,280 --> 00:18:36,800
It's another thing we're not supposed to be, but there's this guy running for president of the

141
00:18:36,800 --> 00:18:41,920
United States and he said something I think it was last night or the night before.

142
00:18:43,760 --> 00:18:50,880
That was pretty awesome. I mean, maybe not terra lotta Buddhist awesome, but pretty awesome

143
00:18:50,880 --> 00:18:55,040
in the way. Bernie Sanders, did anybody know this guy, Bernie Sanders?

144
00:18:55,040 --> 00:19:02,000
He's very popular with a lot of the Democrats.

145
00:19:03,440 --> 00:19:07,760
Very, maybe more so than Hillary Clinton.

146
00:19:08,720 --> 00:19:13,920
I think they are the two that are vying most closely for the Democratic nomination.

147
00:19:16,320 --> 00:19:20,800
He said that they asked him what his religion was, I think.

148
00:19:20,800 --> 00:19:27,760
And he says, my religion is that we're all in this together.

149
00:19:29,680 --> 00:19:33,120
When you hurt, when your child hurts, I hurt.

150
00:19:34,080 --> 00:19:39,040
That is something like that. Your child is my child. My child is your child.

151
00:19:41,600 --> 00:19:50,000
Totally breaking down barriers about, you know, it's basically, I guess it's a lot like the song

152
00:19:50,000 --> 00:19:58,320
imagined by John Lennon, right? The idea that we're just people. I mean, I guess you could

153
00:19:58,320 --> 00:20:04,400
argue could label him as a humanist. He believes in people and he believes that when we stand

154
00:20:04,400 --> 00:20:11,680
together, when we come together, when we work together, good things happen. When we cooperate,

155
00:20:11,680 --> 00:20:16,880
instead of, you know, so he's a socialist, which means he believes in cooperation. I mean, that's

156
00:20:16,880 --> 00:20:25,200
absolutely the big problem with capitalism is it's about competition. And it's based on

157
00:20:26,160 --> 00:20:35,680
the virtue of competing with each other as opposed to cooperating, as opposed to working together

158
00:20:35,680 --> 00:20:43,200
for the greater good. And so he's all about breaking down barriers, breaking down walls, divisions,

159
00:20:43,200 --> 00:20:50,880
bringing people together to, for goodness. I mean, if you listen to him talk, he's honest.

160
00:20:50,880 --> 00:20:57,760
If you don't like what he says, you can't argue that this guy is a liar or a shister or

161
00:20:57,760 --> 00:21:04,480
sorry, that's a bad word. He has Jewish, I think. He has shisters on the cover. I apologize.

162
00:21:04,480 --> 00:21:15,680
But you know, that kind of, yeah, he's honest and sincere. You ask, does he want to help

163
00:21:15,680 --> 00:21:20,800
people? Did you just want to become president? You know, this is a guy who is in politics,

164
00:21:20,800 --> 00:21:27,360
because he wants to help me. I mean, how, how, how awesome is this? How many, how long have we wished

165
00:21:27,360 --> 00:21:36,960
or, or hope for such a person who, who wants to lead the people out of the goodness of his heart?

166
00:21:36,960 --> 00:21:43,360
You know, he might as well exist very often. I can tell you that. You know, who are you?

167
00:21:43,360 --> 00:21:49,280
Sorry, I just joined. That's how he said. Do you know who Bernie Sanders is?

168
00:21:49,280 --> 00:21:58,640
Yeah, I'm from the UK and in my country, it's very competitive based. So it's, it's very capitalist

169
00:21:58,640 --> 00:22:06,560
in this country. They, they want to get rid of free health care, which is not great.

170
00:22:08,080 --> 00:22:13,680
So if he helps carry her in Canada, yeah, they want to get rid of it because it's

171
00:22:13,680 --> 00:22:22,400
what's Bernie Sanders platform he wants, he wants to, you know, I don't know. I don't want to get

172
00:22:22,400 --> 00:22:28,960
into what he's giving, but to care about the fact that there are millions of people who can't

173
00:22:28,960 --> 00:22:33,200
get adequate health care. I mean, there's people who are, I get, these people are my student.

174
00:22:34,080 --> 00:22:40,880
I have one student who was telling me his mother got sick and he was basically having to choose

175
00:22:40,880 --> 00:22:47,440
between food and medicine and, and rent. I mean, living on a budget that you can't live off of.

176
00:22:47,440 --> 00:22:55,280
And the things he talks about these things, who else is talking about these things and making

177
00:22:55,280 --> 00:23:00,240
them platform? There are people starving. There are people, there are, you know, children living

178
00:23:00,240 --> 00:23:06,080
in poverty in the richest country in the world. I guess United States is the richest country in

179
00:23:06,080 --> 00:23:12,640
the world. I don't know. And, and there's an obscene number of people living in poverty who don't

180
00:23:12,640 --> 00:23:19,520
have health care who can't get an education. He wants education to be free, which is really a no

181
00:23:19,520 --> 00:23:28,240
brainer and to pull a bit of a pun. But I mean, education, I'm being going back to school,

182
00:23:28,240 --> 00:23:33,680
I may have been talking to students here and saying, there are students here in Canada where

183
00:23:33,680 --> 00:23:39,920
it's heavily subsidized who still can't get an education, who still have to work two jobs

184
00:23:42,240 --> 00:23:50,720
just to be able to take classes. And, and as a result, aren't learning any, well, aren't learning

185
00:23:50,720 --> 00:23:54,320
what they should be learning because they're too tired because they're too stressed.

186
00:23:56,240 --> 00:24:00,160
This is, I mean, maybe this is all worldly talk. But if we talk about helping people and,

187
00:24:00,160 --> 00:24:06,560
and, you know, people's spiritual well being goodness, remember Buddhism, all about goodness.

188
00:24:08,640 --> 00:24:16,560
There's a lot of badness happening. There's a lot of greed, corruption, and just too much

189
00:24:16,560 --> 00:24:22,240
politicizing people who want to be who are politicians just, for their own benefit, you know,

190
00:24:22,240 --> 00:24:31,360
they, they get their bought by the rich elite and, you know, they make lots of money giving

191
00:24:31,360 --> 00:24:39,600
speeches and holding these dinners and whatever. I mean, guaranteeing that the rich stay rich

192
00:24:39,600 --> 00:24:44,560
and of course, they pour. I mean, you can't argue with the facts. The fact is there's lots

193
00:24:44,560 --> 00:24:49,760
and lots of people who are very, very poor and that hasn't changed and isn't changing. It's getting

194
00:24:49,760 --> 00:24:58,800
worse. Sorry. Here comes Robin to tell me I'm renting. I know. Okay, okay. Robin, Robin can,

195
00:24:58,800 --> 00:25:03,520
you can talk for yourself. I won't, I won't say anything you don't want me to say on the internet.

196
00:25:03,520 --> 00:25:09,440
No, I'm here to say, go Bernie. You're in, you're in with Bernie. Absolutely. I actually

197
00:25:09,440 --> 00:25:15,040
changed my, my party affiliation to the American Republican. I was going to say she's a Republican

198
00:25:15,040 --> 00:25:20,720
and we were talking about Bernie. I changed so that I can vote for Bernie. He's awesome. Awesome.

199
00:25:21,600 --> 00:25:28,080
Well, it comes about a, if we're any, if you and I are any indication, he's got the Buddhist

200
00:25:28,080 --> 00:25:39,040
vote. I think so. Not the way. It comes about as close to being what used to be referred to as a

201
00:25:39,040 --> 00:25:47,040
true statesman. So many politicians are just table boys for corporate power and wealthy people.

202
00:25:48,000 --> 00:25:55,200
And he, it's amazing that he's been able to stay in office so long. I mean, you, you're in office

203
00:25:55,200 --> 00:26:02,800
by virtue of the vote of a constituency. So he must have a very enlightened constituency.

204
00:26:02,800 --> 00:26:10,880
He, he has like 83% of approval rating in his state or something. It's more, it's just like,

205
00:26:12,880 --> 00:26:18,400
yeah. In a poll that came out right after the New Hampshire primary where she, he just won in a landslide,

206
00:26:19,200 --> 00:26:25,440
there was a poll where the question was, are the candidates trustworthy?

207
00:26:25,440 --> 00:26:32,240
That's right. That got like 5% and he had 95%. So no matter what, you know, you think of his

208
00:26:32,240 --> 00:26:38,640
policies, people do believe that he's telling me that that's, to me, that's a very, very important

209
00:26:38,640 --> 00:26:44,080
point is before we talk about what he's promising, you know, what's his character?

210
00:26:44,960 --> 00:26:49,520
You know, because politicians promise everything. All the politicians promise what they think

211
00:26:49,520 --> 00:26:54,880
will get them elected. I bet he's guilty of that a little bit as well. He knows what I have to

212
00:26:54,880 --> 00:27:01,440
say this or I won't get elected even though maybe he doesn't quite believe it. But, you know,

213
00:27:01,440 --> 00:27:10,160
there's not that, that, that isn't a huge question in people's minds because they know he's

214
00:27:10,160 --> 00:27:16,160
sincere. And they know that the majority of what he, he, he, at least the majority of not all of

215
00:27:16,160 --> 00:27:22,000
what he stands for, he really stands for. And that, I mean, that's important. It's not tricking

216
00:27:22,000 --> 00:27:29,360
it. He's not, it's not deceptive. There seems to be very much in favor of placing

217
00:27:30,720 --> 00:27:40,400
constraints on corporations and throttling corporations, which is so very much needed in this

218
00:27:40,400 --> 00:27:50,800
country. The corporate power and the, the, the, the very wealthy, powerful people that are running

219
00:27:50,800 --> 00:28:01,680
corporations is, is just horrendous. And he's, he seems to be in favor of trying to deal with that,

220
00:28:01,680 --> 00:28:08,720
which would be really difficult, you know, with, with the, because presidents got to work with the

221
00:28:08,720 --> 00:28:17,040
Congress and we've of course seen the, the, the Obama's difficulties working with the Congress

222
00:28:17,040 --> 00:28:26,000
that we have had the last few years. And it's just, the other neat thing about him is he does work

223
00:28:26,000 --> 00:28:31,360
well with people. I mean, when you're that honest, he gets things done. He apparently has a record

224
00:28:32,400 --> 00:28:37,760
people saying, well, he's how could he ever work. He's got a 40 year record, a long record of

225
00:28:37,760 --> 00:28:44,320
working with people actually getting sure, which, I mean, that's it. He, you said it. He's a real

226
00:28:44,320 --> 00:28:52,000
statesman. People, the Republicans appreciate that. Well, some of them, well, some of them, sure,

227
00:28:52,000 --> 00:29:00,640
but, you know, he's done work with. Anyway, I don't know, I just wanted to say is here's someone

228
00:29:00,640 --> 00:29:09,040
who is worthy of support. We need someone out over here in the UK. You guys are like some

229
00:29:09,040 --> 00:29:16,080
socialist as well. Well, we're like very capitalist over here, not very socialist. So we've got

230
00:29:16,880 --> 00:29:24,800
corporate businesses and, you know, strict guidelines and such. So it's not the kind of

231
00:29:24,800 --> 00:29:34,720
society that I enjoy living in. We have a history of, yeah, media's competition.

232
00:29:36,640 --> 00:29:41,040
You know, in Denmark, he actually, as soon as you leave at a, what's it called, like,

233
00:29:41,040 --> 00:29:45,760
primary school. I mean, as soon as you're not really a kid anymore, you're, you're going to start

234
00:29:45,760 --> 00:29:51,520
getting paid to go to school, paying like before business school and university. And just enough

235
00:29:51,520 --> 00:29:57,520
to, to, if you have an apartment, it's more important than to, to kids. And it's, it's really

236
00:29:57,520 --> 00:30:04,080
a no brainer. What's the result of that? You know, as the result, people who, who are unproductive

237
00:30:04,080 --> 00:30:11,280
and know you have people who are intelligent. And the problem is rich people, well, they're not rich,

238
00:30:11,280 --> 00:30:17,520
but not rich people are not all bad, but greedy, corporate, terrible people,

239
00:30:17,520 --> 00:30:26,000
want, want, want people to be stupid. It's really, this is, isn't that like also like

240
00:30:26,000 --> 00:30:35,120
selfism as well? Sorry, isn't that like a selfism as well, where some people hold to the knowledge

241
00:30:35,120 --> 00:30:40,720
and want to dumb down other people? That's not, I think, what's selfism means,

242
00:30:40,720 --> 00:30:50,160
selfism is when you just try to sound intelligent. It's elitism or something or, I don't know,

243
00:30:50,160 --> 00:30:58,080
there's probably a word for it. But, you know, I think there's great, one of the great things

244
00:30:58,080 --> 00:31:04,400
about America is that rich people, a lot of rich people do do good things, right? I mean,

245
00:31:04,400 --> 00:31:10,480
those people who have gained power, like Bill Gates, for example, apparently he's, there's conspiracy

246
00:31:10,480 --> 00:31:17,360
surrounding him, so maybe he's a terrible person as well. I don't know. But the ideal of a person

247
00:31:17,360 --> 00:31:23,760
who gets very rich from something like Microsoft Windows or Microsoft is actually a terrible

248
00:31:23,760 --> 00:31:34,400
operating system. But then to use that money to say wipe out malaria or polio or, you know,

249
00:31:35,040 --> 00:31:40,160
polio, which is really terrible, terrible disease. So whether the facts, I don't know what the facts

250
00:31:40,160 --> 00:31:49,920
are and criticisms of him, particularly at the side, there is a sense of rich people being charitable,

251
00:31:49,920 --> 00:31:53,680
I don't know. I mean, maybe I'm being charitable by saying that, I don't know, but.

252
00:31:53,680 --> 00:31:58,720
No, definitely, even like athletes and celebrities in the United States, they're very, very

253
00:31:58,720 --> 00:32:04,080
out in the open, you know, with their foundations, for many, many people have their own foundations,

254
00:32:04,080 --> 00:32:09,200
they do good works. You know, maybe it's a little bit for publicity, but someone benefits from it

255
00:32:09,200 --> 00:32:14,640
anyway. And then you have to, I guess you have to ask whether it's systemic and whether it's as,

256
00:32:14,640 --> 00:32:22,960
you know, whether it works as well, because I don't know. Some people argue that the government

257
00:32:22,960 --> 00:32:30,720
can't, the government is full of waste and so on. Anyway, I don't want to get into all the

258
00:32:30,720 --> 00:32:36,960
politics of it, just goodness of it. And here, you know, supporting, I wanted to, wanted to

259
00:32:36,960 --> 00:32:44,480
shout, give a shout out to Bernie Sanders for, for his, what seems to be awesome as far as I can see,

260
00:32:45,200 --> 00:32:51,040
his heart is in the right place. And he walks the walk, he actually does do good things for people.

261
00:32:51,040 --> 00:32:56,720
He has some good things for people. It's helped people. Maybe he's a bodhisattva.

262
00:32:56,720 --> 00:33:06,880
I guess I please endorse me yesterday. That was all right. He got Spike Lee's endorse me

263
00:33:06,880 --> 00:33:12,960
yesterday. Who is Spike Lee? I've heard this name before. He's a filmmaker. Okay, right. I think I

264
00:33:12,960 --> 00:33:20,480
knew that. I don't know any of his films. Well, I was big in the rap scene as well, just sad

265
00:33:20,480 --> 00:33:29,360
and a little bit. Okay. If I, if I couldn't endorse a political person, he'd have my endorsement as

266
00:33:29,360 --> 00:33:37,200
well. As it is, he has my respect. How about spiritual endorsement? This is such a thing.

267
00:33:39,360 --> 00:33:44,320
He has my spiritual endorsement, absolutely. I endorse become, I should send them one of these books.

268
00:33:44,320 --> 00:33:51,120
Get Aaron, I just send him a guide to how to meditate. Yeah, do that.

269
00:33:53,120 --> 00:33:58,880
Maybe I'll read a letter to him as well. Not a lot of people do that nowadays.

270
00:34:00,560 --> 00:34:07,600
Sorry. A lot of people write hand written letters nowadays. It's a quite a, it's an art form now.

271
00:34:07,600 --> 00:34:20,400
Yeah. Well, not when I write chicken and scratch. You could also send him your lessons in practical

272
00:34:20,400 --> 00:34:25,840
Buddhism. Yeah, I don't imagine he's got a lot of time or interests in reading. This one's kind

273
00:34:25,840 --> 00:34:34,640
of practical. So maybe, maybe, maybe, maybe his wife will read interesting. He's less busy. He's

274
00:34:34,640 --> 00:34:39,920
just so busy, it seems, which is another awesome thing. I mean, he's tireless. He really,

275
00:34:41,120 --> 00:34:53,920
it seems, you know, he's old. He probably eats healthy. He probably eats healthy.

276
00:34:53,920 --> 00:35:03,600
Right. Well, maybe, maybe he would, maybe you would like one of these. I'll send them a few

277
00:35:03,600 --> 00:35:12,720
maybe. Sure. Okay, I'm going to say good night. Thank you all. Good, thank you.

278
00:35:13,920 --> 00:35:16,160
Something interesting to talk about every night.

279
00:35:16,160 --> 00:35:23,040
Good night. Thank you, man. Good night. Good night.

