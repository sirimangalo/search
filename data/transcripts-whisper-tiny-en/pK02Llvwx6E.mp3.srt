1
00:00:00,000 --> 00:00:07,000
Today we are going to see the Dhamma at the Leo C. in Permino.

2
00:00:07,000 --> 00:00:09,000
See the Dhamma?

3
00:00:09,000 --> 00:00:15,000
We are going to see this parrot.

4
00:00:15,000 --> 00:00:19,000
We are going to see the parrot.

5
00:00:19,000 --> 00:00:24,000
We are going to see the parrot.

6
00:00:24,000 --> 00:00:28,000
We are going to see the parrot.

7
00:00:28,000 --> 00:00:34,000
I can read your friends, but I probably can't reply in friends.

8
00:00:34,000 --> 00:00:37,000
Paulina, do you know a friend?

9
00:00:37,000 --> 00:00:39,000
No, I am sorry.

10
00:00:39,000 --> 00:00:44,000
Well, if we have a Polish person, I know who to turn to.

11
00:00:44,000 --> 00:00:46,000
Only sponging Swedish.

12
00:00:46,000 --> 00:00:47,000
Okay.

13
00:00:47,000 --> 00:00:51,000
So I can help indicate this.

14
00:00:51,000 --> 00:00:52,000
I think I get it.

15
00:00:52,000 --> 00:00:53,000
I mean, I get it.

16
00:00:53,000 --> 00:00:57,000
Someone else also tried to translate it as well.

17
00:00:57,000 --> 00:00:59,000
Yes.

18
00:00:59,000 --> 00:01:02,000
I think that was my question.

19
00:01:02,000 --> 00:01:04,000
Yes.

20
00:01:04,000 --> 00:01:08,000
No, this is a question of whether the Dhamma is in Perminant.

21
00:01:08,000 --> 00:01:10,000
Oh, sorry.

22
00:01:10,000 --> 00:01:11,000
Yes.

23
00:01:11,000 --> 00:01:13,000
Whether the Dhamma is a good question.

24
00:01:13,000 --> 00:01:20,000
It is a very interesting tricky question that has an interesting answer to it, I think.

25
00:01:20,000 --> 00:01:24,000
Helps us to understand what exactly the Dhamma is.

26
00:01:24,000 --> 00:01:27,000
Dhamma, the word Dhamma means many different things.

27
00:01:27,000 --> 00:01:37,000
So if you focus, if you do it in Petit, you see no precise.

28
00:01:37,000 --> 00:01:38,000
I don't know.

29
00:01:38,000 --> 00:01:39,000
You have to be a little bit.

30
00:01:39,000 --> 00:01:41,000
Boku, Petit, Claab.

31
00:01:41,000 --> 00:01:42,000
I don't know.

32
00:01:42,000 --> 00:01:44,000
It's a little bit unclear.

33
00:01:44,000 --> 00:01:47,000
But I'm going to give a comprehensive answer.

34
00:01:47,000 --> 00:01:49,000
So hopefully, it will answer your question.

35
00:01:49,000 --> 00:01:52,000
And unfortunately, it's going to be in English.

36
00:01:52,000 --> 00:01:58,000
I'm not a very good Canadian in my friend's skills.

37
00:01:58,000 --> 00:02:01,000
I should just see Canadian.

38
00:02:01,000 --> 00:02:04,000
But not so good French.

39
00:02:04,000 --> 00:02:06,000
Because I don't use it.

40
00:02:06,000 --> 00:02:11,000
So it will be nice to have a little bit more input in French.

41
00:02:11,000 --> 00:02:14,000
But Dhamma means many different things.

42
00:02:14,000 --> 00:02:20,000
If you're talking about the Sadhamma, the Sadhamma means the Dhamma of the Buddha.

43
00:02:20,000 --> 00:02:22,000
It's the teaching of the Buddha.

44
00:02:22,000 --> 00:02:25,000
In the triple gem, the three refuges.

45
00:02:25,000 --> 00:02:30,000
But Dhamma is a little bit more specific.

46
00:02:30,000 --> 00:02:43,000
If that's the Dhamma that you're referring to, that we go for refuge to the Buddha, the Dhamma, and the Sanga.

47
00:02:43,000 --> 00:02:50,000
Then I think we have to be a little bit more, still a little bit more specific.

48
00:02:50,000 --> 00:03:04,000
If you're talking about the teachings of the Buddha, that part of the Dhamma that is particularly referring to the concepts that have been employed by the Buddha,

49
00:03:04,000 --> 00:03:10,000
in order to allow people to understand the truth.

50
00:03:10,000 --> 00:03:19,000
So, remember, teaching uses words which are inherently conceptual.

51
00:03:19,000 --> 00:03:24,000
To, in this case, to help us to understand ultimate reality.

52
00:03:24,000 --> 00:03:29,000
So when we say to ourselves rising, falling, the word rising is a concept.

53
00:03:29,000 --> 00:03:33,000
But it's the name of an experience which is real in an ultimate sense.

54
00:03:33,000 --> 00:03:38,000
So we use concepts to understand ultimate reality.

55
00:03:38,000 --> 00:03:42,000
Those particular formed concepts are, of course, impermanent.

56
00:03:42,000 --> 00:03:46,000
The teachings of the Buddha are going to be forgotten one day.

57
00:03:46,000 --> 00:03:52,000
Eventually, there will be only one half of a verse or something.

58
00:03:52,000 --> 00:03:57,000
People will be able to remember half of a stanza of Dhamma, I think, is at the very end.

59
00:03:57,000 --> 00:03:59,000
That's when the Dhamma disappears.

60
00:03:59,000 --> 00:04:03,000
So right now we have the teachings of the Buddha one day.

61
00:04:03,000 --> 00:04:05,000
They will be forgotten.

62
00:04:05,000 --> 00:04:15,000
Except, you know, people will remember bits and pieces like half of a verse that doesn't have any meaning, any significant profundity.

63
00:04:15,000 --> 00:04:17,000
So that's going to disappear.

64
00:04:17,000 --> 00:04:21,000
But that's not the whole meaning of the word Dhamma.

65
00:04:21,000 --> 00:04:32,000
Impermanence, suffering and non-self are three Dhammas, three or three aspects of reality.

66
00:04:32,000 --> 00:04:36,000
That the Buddha said, are permanent.

67
00:04:36,000 --> 00:04:38,000
This is the ironic thing.

68
00:04:38,000 --> 00:04:42,000
Impermanence is permanent.

69
00:04:42,000 --> 00:04:45,000
Maybe that's not really a good thing to say.

70
00:04:45,000 --> 00:04:48,000
It's permanent or not, but the Buddha said,

71
00:04:48,000 --> 00:05:15,000
I forget that it's as suited that way.

72
00:05:15,000 --> 00:05:19,000
And often chanting in Thailand, the Dhamma niyama sutra.

73
00:05:19,000 --> 00:05:25,000
Niyama means the certainty or the opposite of Impermanence,

74
00:05:25,000 --> 00:05:32,000
the absolute certainty and consistency.

75
00:05:32,000 --> 00:05:47,000
So the Buddha arises, or not, a Nupada one.

76
00:05:47,000 --> 00:05:50,000
I forget exactly what the phrase is, Dhamma, or something.

77
00:05:50,000 --> 00:06:03,000
This element of Dhamma stays Dita.

78
00:06:03,000 --> 00:06:10,000
The consistency of Dhammas or the certainty stays.

79
00:06:10,000 --> 00:06:13,000
All Sankara's, all formations are impermanent.

80
00:06:13,000 --> 00:06:17,000
So whether Buddha arises or not, this Dhamma stays.

81
00:06:17,000 --> 00:06:23,000
This truth is, it persists.

82
00:06:23,000 --> 00:06:30,000
But then he says, what happens is a Buddha,

83
00:06:30,000 --> 00:06:54,000
and he says, what happens to the Buddha?

84
00:06:54,000 --> 00:07:00,000
Is it conceivable or helps people to conceive of it?

85
00:07:00,000 --> 00:07:05,000
Establish it as a teaching.

86
00:07:05,000 --> 00:07:07,000
But the reality exists.

87
00:07:07,000 --> 00:07:15,000
So the four noble truths, for example, are truths no matter what period of time you exist.

88
00:07:15,000 --> 00:07:18,000
And that will never change.

89
00:07:18,000 --> 00:07:26,000
But everything that Yankin Tisam would be a Dhamma, that all arisen phenomena will,

90
00:07:26,000 --> 00:07:34,000
will, or of nature, to see if it actually sees after a simple moment.

91
00:07:34,000 --> 00:07:38,000
So if that's the Dhamma that you're referring to, the truth of the Buddha realized,

92
00:07:38,000 --> 00:07:45,000
that truth is persists eternally.

93
00:07:45,000 --> 00:07:50,000
Nothing can, it's not possible for something to arise, that does not cease.

94
00:07:50,000 --> 00:07:54,000
It's not possible for some formation to be permanent.

95
00:07:54,000 --> 00:07:57,000
It's not possible for some formation to be satisfying.

96
00:07:57,000 --> 00:08:02,000
It's not possible for any Dhamma to be self, to be at that.

97
00:08:02,000 --> 00:08:08,000
So two aspects of the Dhamma, that which is taught and the teaching itself,

98
00:08:08,000 --> 00:08:11,000
that which is taught is, is always there.

99
00:08:11,000 --> 00:08:16,000
But the teaching of it comes and goes with the Buddha's.

