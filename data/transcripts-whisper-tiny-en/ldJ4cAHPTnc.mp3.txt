T.J. has said that we can judge things based on our wisdom. When something is going on
in the world that we judge is wrong or bad, for example, what is going on in Tibet, how
do we know when and how to properly intervene, or is it better to just let things exist
as they are not trying to influence them, even if we know it's wrong and may hurt someone?
Well, judging based on wisdom, I personally, when I said that probably wasn't thinking
of things like the Chinese occupation of the territory or the state of Tibet, in fact, I don't
know too much weight on this at all, really, and wisdom doesn't judge so much. I was
just probably just giving you, if I, the only time I think that I've talked about this
is when, yeah, this abidama teacher told me that judging can be based on wisdom. And I have
to concur that you can judge that certain things are wrong, but I don't think it's the judging
in the sense that you're thinking of it. You wouldn't judge in the sense of thinking,
I got to do something about that, or that's a real problem, because you've given up
any thought of problems, and enlightened being would be able to judge in the sense of
saying, that's bad, that's good in the sense of, or not even that's bad, that's good.
This will lead to this, and that will lead to that. So this will lead to suffering, and
that will lead to happiness, or this will lead to nothing, and this will lead to something,
and so on, out of knowledge. They will be aware of how defilements lead to suffering, how
craving leads to suffering. And in fact, I would say they would give up, and this was
maybe a controversial thing to say. I would say they would give up such causes like freeing
a nation from another nation, and so on. I'll tell a story that I told. It's not
in my story, but the connection that I made in regards to Thailand, when there were difficulties,
and I don't know if there's still art with Muslim groups, or trying to occupy or take over parts
of Southern Thailand. And there was debate over whether to fight, or whether to give some
control over to the Muslim people. And I looked at the Jataka, because if you want the ultimate
example, we have the Bodhisatta, who, as a king, he gave up his kingdom. He was besieged
by a foreign power. This other king came to, to besiege that his city to attack and conquer
his kingdom. And so the minister said, it came and said, should we fight? Should we attack them?
And he said, fight, attack. What are you talking about? If they want the kingdom, let them have it.
And it's quite a difficult teaching, and some people would say, well, that just applies to the
Bodhisatta, but I'm not sure that it does. The only thing that comes from animosity, from
enmities, is more enmity, and more struggle, and more suffering. The issue in Tibet, or in any
case of occupation, or territorialism, conquering, or power struggles, war, and so on.
From a organizational point of view, you could say, this is wrong, this is right, this is good,
this is bad, but it really has nothing to do with the Buddha's teaching, or with the path to
the freedom from suffering. Freedom from suffering is not an organizational thing. It's something
that you undertake on your own, that you undertake for your own self. And so the only judging
that should go on in terms of the practice is what mine states in your mind are wholesome,
and what mine states are unwholesome. You shouldn't, even in your practice, in terms of the practice,
for freedom from suffering. Even consider the mine states of other people, whether they are wholesome,
or unwholesome, which is said, nah pray san vilomani nah pray san katagatana. We should not concern
ourselves with the faults of others, or the deeds, or undone, the deeds done are undone by others.
We should concern ourselves with our own. So to put it simply, talking about these things as
good or bad issues in the world as good or bad is only on a conceptual level. It has nothing to
do with the Buddha's teaching. And Arahan, of course, does away with the world, or any interest in
the world. And so they wouldn't fight for such causes. If you want, all we can say from a Buddhist
point of view is that, well, if you get angry about it, that's an unwholesome thing. And if you let
go of something, then that's a wholesome thing. If you help people to become to find peace,
then that's a good thing. But if you cause conflict with other people, then that's a bad thing.
On an experiential level. So the judging that we concern ourselves with is experiential moment-to-moment.
The problem here is that we ourselves have defiled minds. But mentioned it already. So sometimes
we want to get involved and we want to intervene because of our own defilements and then we get
entangled and then we get angry and things get out of control. So it is really important to
consider very carefully why do we get involved there. And what is what we can do? What is the good
thing that we can do? I heard of monks burning themselves or people burning themselves. So
this is killing. And killing can only happen when there is a huge amount of aggression and anger.
So it might be understandable that people whose country has been intruded and is violated all
the time that there is that anger. But especially when there is not the possibility as it was
earlier to practice meditation and live a more spiritual life. So all these actions come from
defiled minds and I don't think it helps the case. It makes it worse because no peace can
can arise out of this because this is just hatred. So when one gets involved and when one wants
to intervene, it is very important to check on the intentions and to check on the
the the the defilements that there's usually there's some wanting or not wanting.
