1
00:00:00,000 --> 00:00:07,000
Miriam asked, what if you're not able to go on retreats?

2
00:00:07,000 --> 00:00:32,000
Then try to do a retreat at home, maybe with Bante, with a Skype teaching, or with the booklet and the videos, that's possible.

3
00:00:32,000 --> 00:00:49,000
And if you don't have that time to do a retreat at home, then at least try to meditate every day, according to the instructions.

4
00:00:49,000 --> 00:01:01,000
And try to ask as many questions as possible that you don't have so many questions arise during your meditation practice.

5
00:01:01,000 --> 00:01:14,000
So make sure that you have kind of feedback that is helping you over your hardships or over your problems.

6
00:01:14,000 --> 00:01:23,000
And another thing as well, there has to be a background to your question.

7
00:01:23,000 --> 00:01:27,000
Well, many people are not able to go on retreats, but that doesn't bother them.

8
00:01:27,000 --> 00:01:35,000
So the question is, what is your goal and what is your goal in life?

9
00:01:35,000 --> 00:01:40,000
If your goal in life is to become free from suffering and to become enlightened,

10
00:01:40,000 --> 00:01:45,000
if your goal in this life is enlightenment or in however many lifetimes it takes,

11
00:01:45,000 --> 00:01:53,000
then I would say really the answer if you're not able at all is to start changing some things,

12
00:01:53,000 --> 00:02:02,000
start moving in that direction, start making a determination, whether it be in this life or in the next,

13
00:02:02,000 --> 00:02:06,000
to get yourself in a situation where you can go on a retreat.

14
00:02:06,000 --> 00:02:10,000
It's not really as difficult, but as we think it is, but it takes a lot of letting go.

15
00:02:10,000 --> 00:02:18,000
Sometimes it takes a change of lifestyle.

16
00:02:18,000 --> 00:02:26,000
If you really think that if you really have an appreciation of the Buddha's teaching or appreciation of the truth,

17
00:02:26,000 --> 00:02:31,000
the simple truth, for example, that nothing is worth clinging to, nothing in the world.

18
00:02:31,000 --> 00:02:36,000
Nothing at all is worth clinging to.

19
00:02:36,000 --> 00:02:49,000
Then you'll start to let go of any concern for physical or social welfare.

20
00:02:49,000 --> 00:02:57,000
You'll come to realize that no matter what or how you live your life or what you experience in life,

21
00:02:57,000 --> 00:03:02,000
in the end it's only seeing, hearing, smelling, tasting, feeling and thinking.

22
00:03:02,000 --> 00:03:14,000
Whereas before you would have thought living without a job or even going without food for a day is unthinkable.

23
00:03:14,000 --> 00:03:20,000
Living in poverty is impossible.

24
00:03:20,000 --> 00:03:24,000
It's just unfathomable.

25
00:03:24,000 --> 00:03:30,000
Maybe not living in poverty, but adjusting your life so that you're more hard up,

26
00:03:30,000 --> 00:03:39,000
and you don't have the luxuries and the ability, the lifestyle that you have.

27
00:03:39,000 --> 00:03:44,000
But when you realize that in the end it's still the same set of experiences as before,

28
00:03:44,000 --> 00:03:49,000
then you're able to give up much of your life and much of your comfort

29
00:03:49,000 --> 00:03:57,000
and able to raise really not that much money to, for example, take a plane to

30
00:03:57,000 --> 00:04:05,000
to Sri Lanka or at the very least to arrange your affairs and your family members in such a way

31
00:04:05,000 --> 00:04:12,000
so that you have time to take a retreat in closer to home.

32
00:04:12,000 --> 00:04:16,000
What one way or the other finds some way, and if it's not in this life,

33
00:04:16,000 --> 00:04:21,000
if you have children or if you have a family and you have responsibilities or you have debts or so on,

34
00:04:21,000 --> 00:04:30,000
then put your effort into coming to a situation where not only can you join a retreat,

35
00:04:30,000 --> 00:04:36,000
but you can actually dedicate your life to this because that's really what it's a sliding scale.

36
00:04:36,000 --> 00:04:40,000
Or it's a matter of degree.

37
00:04:40,000 --> 00:04:49,000
In the end, the best thing you can do is to go off into the forest and come off to our forest and practice or meditation.

38
00:04:49,000 --> 00:04:53,000
But it is a matter of degrees how far you're willing to take it.

39
00:04:53,000 --> 00:05:00,000
If your question is coming from the basis of how can I live my life just as I'm...

40
00:05:00,000 --> 00:05:07,000
What can I do if my current life doesn't let me practice meditation,

41
00:05:07,000 --> 00:05:13,000
or how can I find a way to practice in my current life without changing anything about my life?

42
00:05:13,000 --> 00:05:19,000
If that life doesn't let me go under retreats, I mean if it were that kind of question,

43
00:05:19,000 --> 00:05:21,000
then you would have a real problem.

44
00:05:21,000 --> 00:05:25,000
You would have to say what polynities I wouldn't do as much as you can,

45
00:05:25,000 --> 00:05:34,000
but with the qualifier that you shouldn't then expect quite profound results.

46
00:05:34,000 --> 00:05:37,000
It's quite likely that you will only gain superficial results,

47
00:05:37,000 --> 00:05:47,000
or results in a very short, a very...

48
00:05:47,000 --> 00:05:55,000
I'm a minor, a gradual result.

49
00:05:55,000 --> 00:06:04,000
And it will take a long time to really significantly change anything about your life.

50
00:06:04,000 --> 00:06:08,000
Maybe not to be so negative about it,

51
00:06:08,000 --> 00:06:14,000
because even just a little bit of practice can have a significant impact on your life.

52
00:06:14,000 --> 00:06:18,000
But understanding that it's really up to you,

53
00:06:18,000 --> 00:06:20,000
if you're not able to take a retreat,

54
00:06:20,000 --> 00:06:25,000
well if a retreat is something that you want to do, then make it happen,

55
00:06:25,000 --> 00:06:28,000
because the universe has endless potential.

56
00:06:28,000 --> 00:06:32,000
This is what it means to say that there is no purpose in life.

57
00:06:32,000 --> 00:06:34,000
You choose your purpose.

58
00:06:34,000 --> 00:06:36,000
If you want to go under retreat and make it happen,

59
00:06:36,000 --> 00:06:38,000
it may not happen in this life,

60
00:06:38,000 --> 00:06:44,000
but it's pretty hard to imagine it not happening for someone who,

61
00:06:44,000 --> 00:06:47,000
in some life, to come or in some time in the future,

62
00:06:47,000 --> 00:06:53,000
is dedicated enough to do anything really.

63
00:06:53,000 --> 00:06:55,000
So you have to keep that in mind, make it happen.

64
00:06:55,000 --> 00:06:59,000
If you want to go under retreat, make it happen.

65
00:06:59,000 --> 00:07:01,000
It all depends what you want.

66
00:07:01,000 --> 00:07:07,000
I think she was asking because Jan's mentioned a little to go.

67
00:07:07,000 --> 00:07:12,000
But this should be flushed out, I think.

68
00:07:12,000 --> 00:07:17,000
Okay. Yeah.

