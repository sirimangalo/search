1
00:00:00,000 --> 00:00:09,900
Hello and welcome back to our study in the Damanpanda. Today, we continue on with verse 164,

2
00:00:09,900 --> 00:00:39,400
which reads as follows.

3
00:00:39,400 --> 00:00:58,900
Yo, Patikosati, yo-dumindo, Patikosati, whatever fool, whatever person, whatever person insults,

4
00:00:58,900 --> 00:01:17,900
the religion of the enlightened ones, the noble religion, noble teaching, the Dhamma-ji-wi-wi-nada,

5
00:01:17,900 --> 00:01:32,900
the religion of those who live the holy life, the recording of righteousness, the Aryans.

6
00:01:32,900 --> 00:01:47,100
Whoever insults it, the fool, Dhamma-dumindo, the teaching Messiah, the teaching Messiah

7
00:01:47,100 --> 00:01:58,000
the power, because of the evil views, dependent on their evil views, so they have evil views

8
00:01:58,000 --> 00:02:13,100
on the insult, the noble way. Just like the fruit of the katakareed, palani katakaseva,

9
00:02:13,100 --> 00:02:27,100
atta-ghatai, they bear fruit, the bearing of fruit, the fruit that comes from their act destroys themselves.

10
00:02:27,100 --> 00:02:36,600
So there's this read, the katakareed, so when it bears fruit, the fruit is so heavy that the read snaps,

11
00:02:36,600 --> 00:02:48,600
such a person who insults the reviles the good way, the noble way, and has evil views,

12
00:02:48,600 --> 00:02:56,600
destroys themselves, just like that, the fruit of their evil.

13
00:02:56,600 --> 00:03:07,600
So this was taught in regards to the elder Kala, Kala was a monk who lived in Sabhati,

14
00:03:07,600 --> 00:03:20,600
and the story goes that there was a certain woman who, maybe an elderly woman who looked after him,

15
00:03:20,600 --> 00:03:27,600
with the tenderness of a mother for a son, so she took great care of him.

16
00:03:27,600 --> 00:03:35,600
And one day, her neighbors went to hear the Buddha, this teach the Dhamma, and they said,

17
00:03:35,600 --> 00:03:42,600
oh, it's how wonderful is it to hear the teachings of the Buddha, how great is it?

18
00:03:42,600 --> 00:03:48,600
And so hearing this, she says to the elders, what do you think, should I go and listen to the Buddha teach someday?

19
00:03:48,600 --> 00:03:53,600
They said, no, no, you shouldn't.

20
00:03:53,600 --> 00:03:57,600
And again, she started thinking about this, and so she asked him again, you know, why not?

21
00:03:57,600 --> 00:03:59,600
He said, oh, no, no, it's not worth your time.

22
00:03:59,600 --> 00:04:05,600
Wouldn't give her a good reason, but he kept just waiting her from it.

23
00:04:05,600 --> 00:04:13,600
And we're told that the reason, his reasoning was that if she went to hear the Buddha teach,

24
00:04:13,600 --> 00:04:20,600
she would have no use for him, here he was providing her with her only source of the Dhamma.

25
00:04:20,600 --> 00:04:24,600
Well, she went to the Buddha, he knew it, he saw what happened then.

26
00:04:24,600 --> 00:04:29,600
When they heard the Buddha teach, they got great faith from the Buddha.

27
00:04:29,600 --> 00:04:35,600
And then out of game comparison, they would look at the other monks and say these other monks.

28
00:04:35,600 --> 00:04:39,600
They would only have eyes for the Buddha.

29
00:04:39,600 --> 00:04:44,600
It's happened a lot in my monastery, it's kind of funny.

30
00:04:44,600 --> 00:04:53,600
Because our headmaster is just so esteemed that most of the other monks actually starve to our god.

31
00:04:53,600 --> 00:04:55,600
We're very poorly taken care of.

32
00:04:55,600 --> 00:05:05,600
It was very hard to survive in some ways as a monk in my monastery because everybody was a very rich monastery,

33
00:05:05,600 --> 00:05:11,600
but all the money and support when only to the top.

34
00:05:11,600 --> 00:05:26,600
So for those monks looking for gain, it was very hard to be well off as a monk there.

35
00:05:26,600 --> 00:05:33,600
I mean, that might sound strange because of course monks are supposed to be very content with little or nothing.

36
00:05:33,600 --> 00:05:42,600
But in some places in Thailand, monks were able to be very well taken care of.

37
00:05:42,600 --> 00:05:48,600
So this monk was quite well taken care of and didn't want to lose this.

38
00:05:48,600 --> 00:05:54,600
It's also reason why sometimes monks would leave a monastery and go off and start their own.

39
00:05:54,600 --> 00:06:00,600
Because when you're alone, while you're the only monk and the only religious teacher, it's very easy to get along.

40
00:06:00,600 --> 00:06:07,600
I mean, this is all certainly, I don't want to give you the idea that I'm condoning this or encouraging it.

41
00:06:07,600 --> 00:06:14,600
It's great danger and it's very wrong to be even thinking in this sort of way.

42
00:06:14,600 --> 00:06:23,600
This monk was very much in the wrong for his thought and much more for his actions in dissuading this woman.

43
00:06:23,600 --> 00:06:31,600
So finally, she had enough and she had her daughter.

44
00:06:31,600 --> 00:06:37,600
She had her daughter bring food to the monk and take care of this monk.

45
00:06:37,600 --> 00:06:42,600
Do whatever you can to make sure he's happy.

46
00:06:42,600 --> 00:06:49,600
I'm going to go to Jetohana and hear the Buddha teach.

47
00:06:49,600 --> 00:06:55,600
And so the elder was in this, I guess he was staying in the monastery in the city.

48
00:06:55,600 --> 00:06:58,600
There would be, I guess, places to stay in the city.

49
00:06:58,600 --> 00:07:05,600
When this monk saw him coming, when this monk saw her coming, bringing him food, he asked her,

50
00:07:05,600 --> 00:07:07,600
where's your mother?

51
00:07:07,600 --> 00:07:14,600
She's gone to hear the Buddha teach.

52
00:07:14,600 --> 00:07:27,600
And as soon as the elder heard the monk heard this, he got consumed with anger and fear that she had disregarded his words.

53
00:07:27,600 --> 00:07:33,600
Disregarded his evil advice at the rim or what went quite quickly to Jetohana.

54
00:07:33,600 --> 00:07:40,600
When he saw this woman listening to the Buddha teach the dhamma,

55
00:07:40,600 --> 00:07:49,600
his anger and just blind range because can you imagine having the audacity to do what he did,

56
00:07:49,600 --> 00:07:53,600
which he goes to the Buddha and says to the Buddha,

57
00:07:53,600 --> 00:08:01,600
this stupid woman does not understand your subtle discourse on the dhamma.

58
00:08:01,600 --> 00:08:13,600
How awful to, in front of this woman, scold her like that and discouraged the Buddha from teaching.

59
00:08:13,600 --> 00:08:19,600
As though the Buddha didn't know how to teach people to their own level, right?

60
00:08:19,600 --> 00:08:25,600
And he says, teacher instead, he has the audacity to tell the Buddha what to teach her.

61
00:08:25,600 --> 00:08:32,600
Teacher, the duty of arms giving and moral precepts dhamna and seala.

62
00:08:32,600 --> 00:08:36,600
Basically, what he's saying is, don't teach her anything deep.

63
00:08:36,600 --> 00:08:43,600
Why? Because I can't teach her anything deep because I'm an evil, vicious, foolish monk.

64
00:08:43,600 --> 00:08:48,600
And I'm basically useless.

65
00:08:48,600 --> 00:08:55,600
So, if you teach her the great dhamma, what you saw she have for me.

66
00:08:55,600 --> 00:08:59,600
I mean, it doesn't say this, but this is, of course, the unspoken reason

67
00:08:59,600 --> 00:09:04,600
because he's really a terrible person and he doesn't want the comparison.

68
00:09:04,600 --> 00:09:10,600
So, if the Buddha just teaches her simple things, things that won't actually make her enlightened.

69
00:09:10,600 --> 00:09:13,600
Of course, the worst for him wouldn't be if she would have become enlightened

70
00:09:13,600 --> 00:09:18,600
and she'd turn around and look at him and say, why are you teaching me?

71
00:09:18,600 --> 00:09:23,600
You're useless, man.

72
00:09:23,600 --> 00:09:29,600
But if he teaches, if the Buddha would only teach her on charity and morality,

73
00:09:29,600 --> 00:09:34,600
then he'd have a chance because those are simple things that simple people can teach.

74
00:09:34,600 --> 00:09:38,600
It's not easy to teach about me passing our insight meditation.

75
00:09:38,600 --> 00:09:42,600
It's not easy to teach people mindfulness, you know?

76
00:09:42,600 --> 00:09:47,600
It's easy to say the word mindfulness be mindful, but to actually understand it.

77
00:09:47,600 --> 00:09:53,600
And, you know, all these things that we talk about, the paradigm shift and the way of looking at the world

78
00:09:53,600 --> 00:09:59,600
in terms of ultimate reality, seeing experiential reality and understanding.

79
00:09:59,600 --> 00:10:02,600
It's not just intelligence.

80
00:10:02,600 --> 00:10:05,600
It takes vision, takes insight.

81
00:10:05,600 --> 00:10:13,600
It takes concentration or focus, which means your mind has to be, to some extent, pure.

82
00:10:13,600 --> 00:10:19,600
If your mind is full of evil, unwholesome desires, it's very difficult to think,

83
00:10:19,600 --> 00:10:26,600
even to resonate with concepts like impermanence, suffering, noncell.

84
00:10:26,600 --> 00:10:29,600
Not easy.

85
00:10:29,600 --> 00:10:36,600
So, he compounds his evil by not only dissuading this woman from hearing the Buddha,

86
00:10:36,600 --> 00:10:41,600
but now reviling her in front of the Buddha and ordering the Buddha,

87
00:10:41,600 --> 00:10:44,600
instructing the Buddha on how to teach.

88
00:10:44,600 --> 00:10:48,600
Very evil.

89
00:10:48,600 --> 00:10:52,600
So, the Buddha looks at him and says,

90
00:10:52,600 --> 00:10:59,600
mongapuris, I'm assuming is the word he uses, let's say.

91
00:10:59,600 --> 00:11:01,600
Oh, he says, dupanyo, you stupid person.

92
00:11:01,600 --> 00:11:02,600
Pandya is wisdom.

93
00:11:02,600 --> 00:11:05,600
Dupanyam means someone who has no wisdom.

94
00:11:05,600 --> 00:11:11,600
Bhapikanditi nisaya, budhanang sasana, nipatikositi.

95
00:11:11,600 --> 00:11:13,600
It's curious that the Buddha uses this word.

96
00:11:13,600 --> 00:11:20,600
He says, based on your evil views, you revile the religion of the Buddha.

97
00:11:20,600 --> 00:11:22,600
Because he's not actually.

98
00:11:22,600 --> 00:11:25,600
That's one thing he's not explicitly doing.

99
00:11:25,600 --> 00:11:31,600
And so, this speech is meant to connect it to the verse.

100
00:11:31,600 --> 00:11:33,600
And if you look at this, you might think,

101
00:11:33,600 --> 00:11:36,600
well, this is just a commentator trying to fit a different story

102
00:11:36,600 --> 00:11:39,600
and with this verse, which is totally unrelated.

103
00:11:39,600 --> 00:11:42,600
You might think that way.

104
00:11:42,600 --> 00:11:45,600
Because the verse is about reviling the dhamma,

105
00:11:45,600 --> 00:11:47,600
which is an important concept that we'll talk about.

106
00:11:47,600 --> 00:11:55,600
But the story is about evil and the fear of losing one's gain,

107
00:11:55,600 --> 00:12:00,600
really, losing one's income, the fear of the dhamma.

108
00:12:00,600 --> 00:12:06,600
But we can be charitable and to the commentator.

109
00:12:06,600 --> 00:12:09,600
And there's an easy way to understand this.

110
00:12:09,600 --> 00:12:12,600
I mean, that the Buddha is trying to,

111
00:12:12,600 --> 00:12:15,600
and in many cases it appears that this is the way it was.

112
00:12:15,600 --> 00:12:19,600
The Buddha was trying to redirect things.

113
00:12:19,600 --> 00:12:22,600
You know, often the Buddha's response to a problem

114
00:12:22,600 --> 00:12:24,600
is not to address it head on.

115
00:12:24,600 --> 00:12:28,600
Because just arguing with someone

116
00:12:28,600 --> 00:12:32,600
or debating or fighting with someone

117
00:12:32,600 --> 00:12:37,600
may mean that in the end you're right in their wrong.

118
00:12:37,600 --> 00:12:39,600
But it may not fix the problem.

119
00:12:39,600 --> 00:12:41,600
What is the true problem here?

120
00:12:41,600 --> 00:12:44,600
And so the Buddha hits the heart of the matter

121
00:12:44,600 --> 00:12:51,600
by saying, in doing this, you are insulting the dhamma.

122
00:12:51,600 --> 00:12:53,600
You are reviling it.

123
00:12:53,600 --> 00:12:59,600
You are in some way attacking the dhamma.

124
00:12:59,600 --> 00:13:02,600
So rather than actually point out

125
00:13:02,600 --> 00:13:06,600
that he's just afraid of losing his gain,

126
00:13:06,600 --> 00:13:09,600
he says, really what's at stake,

127
00:13:09,600 --> 00:13:14,600
what's at work here, that you can play at work here.

128
00:13:14,600 --> 00:13:18,600
It's your evil outlook,

129
00:13:18,600 --> 00:13:23,600
your corrupt way of looking at things.

130
00:13:23,600 --> 00:13:25,600
And you're only going to hurt yourself.

131
00:13:25,600 --> 00:13:28,600
You think this is to your gain

132
00:13:28,600 --> 00:13:31,600
to prevent this woman from hearing the dhamma.

133
00:13:31,600 --> 00:13:36,600
But it's only to your loss.

134
00:13:36,600 --> 00:13:41,600
So he then attacks the dhamma.

135
00:13:41,600 --> 00:13:43,600
Because that's what's going on,

136
00:13:43,600 --> 00:13:45,600
this woman is hearing the dhamma.

137
00:13:45,600 --> 00:13:47,600
This is the transmission of the dhamma,

138
00:13:47,600 --> 00:13:50,600
spreading the arising, the growth,

139
00:13:50,600 --> 00:13:53,600
the planting of the seed of the dhamma.

140
00:13:53,600 --> 00:13:55,600
This is what's happening.

141
00:13:55,600 --> 00:13:58,600
And he's attacking it,

142
00:13:58,600 --> 00:14:04,600
like a soldier attacking a fortress.

143
00:14:04,600 --> 00:14:08,600
He's attacking the spreading of the dhamma,

144
00:14:08,600 --> 00:14:12,600
the dhammundis and the teaching and the dhamma.

145
00:14:12,600 --> 00:14:15,600
So you can say that he's attacking,

146
00:14:15,600 --> 00:14:19,600
that he's still hard to see how he's insulting the dhamma.

147
00:14:19,600 --> 00:14:22,600
But you can think of it as an insult to,

148
00:14:22,600 --> 00:14:24,600
well, first off,

149
00:14:24,600 --> 00:14:27,600
suggest that the Buddha doesn't know how to teach.

150
00:14:27,600 --> 00:14:35,600
Second off to focus or to insist on the teachings,

151
00:14:35,600 --> 00:14:39,600
only of morality and charity.

152
00:14:39,600 --> 00:14:42,600
Much though useful and quite Buddhist,

153
00:14:42,600 --> 00:14:46,600
are by no means deep or profound

154
00:14:46,600 --> 00:14:52,600
or conducive or leading them in and of themselves to enlightenment.

155
00:14:52,600 --> 00:14:57,600
And then the Buddha teaches this verse.

156
00:14:57,600 --> 00:15:00,600
So that's the story.

157
00:15:00,600 --> 00:15:02,600
So what lessons we can gain?

158
00:15:02,600 --> 00:15:06,600
Again, I think we can look at the two different sides of this,

159
00:15:06,600 --> 00:15:08,600
the story side and the verse side.

160
00:15:08,600 --> 00:15:10,600
And in reference to the story,

161
00:15:10,600 --> 00:15:14,600
it's more related to evil views

162
00:15:14,600 --> 00:15:19,600
and how people use make use of Buddhism.

163
00:15:19,600 --> 00:15:26,600
This monk is clearly not only greedy,

164
00:15:26,600 --> 00:15:29,600
but allowing his greed.

165
00:15:29,600 --> 00:15:31,600
I know that's the wrong word,

166
00:15:31,600 --> 00:15:32,600
but let's use it for now.

167
00:15:32,600 --> 00:15:39,600
Allowing his greed to inform his actions.

168
00:15:39,600 --> 00:15:42,600
So why I say allowing is probably the wrong word

169
00:15:42,600 --> 00:15:48,600
is because it's more like based on his desire.

170
00:15:48,600 --> 00:15:50,600
And this is the key point.

171
00:15:50,600 --> 00:15:53,600
This is an important point to talk about.

172
00:15:53,600 --> 00:15:57,600
It's common to think.

173
00:15:57,600 --> 00:16:00,600
I mean, an ordinary understanding of desire

174
00:16:00,600 --> 00:16:06,600
is that it exists independent of our other emotions.

175
00:16:06,600 --> 00:16:11,600
You want something and that's it.

176
00:16:11,600 --> 00:16:16,600
You want it and either you get it or you don't get it.

177
00:16:16,600 --> 00:16:19,600
We don't connect that we don't connect that the wanting

178
00:16:19,600 --> 00:16:22,600
that our desires are very much related

179
00:16:22,600 --> 00:16:27,600
to our aversion and our fear

180
00:16:27,600 --> 00:16:34,600
and our jealousy, arrogance, conceit.

181
00:16:34,600 --> 00:16:41,600
That our desires corrupt our minds.

182
00:16:41,600 --> 00:16:44,600
We have this idea that I can one things

183
00:16:44,600 --> 00:16:48,600
and still be a kind and nice and generous person of others.

184
00:16:48,600 --> 00:16:52,600
That it doesn't actually diminish my purity of mind.

185
00:16:52,600 --> 00:16:54,600
I don't connect.

186
00:16:54,600 --> 00:16:57,600
I don't make a connection generally speaking.

187
00:16:57,600 --> 00:17:00,600
The fact that I want things, the fact that I like certain things,

188
00:17:00,600 --> 00:17:05,600
what does that have to do with whether I'm an evil or a good person?

189
00:17:05,600 --> 00:17:09,600
What does that have to do with how I treat others?

190
00:17:09,600 --> 00:17:12,600
And this is an important example.

191
00:17:12,600 --> 00:17:15,600
It's a good example of this important concept

192
00:17:15,600 --> 00:17:17,600
that there is a connection.

193
00:17:17,600 --> 00:17:21,600
That as with all things, this is the reason

194
00:17:21,600 --> 00:17:25,600
for the Buddha to take up the concept of karma

195
00:17:25,600 --> 00:17:29,600
and apply the word karma in a Buddhist sense

196
00:17:29,600 --> 00:17:32,600
to show that there are consequences.

197
00:17:32,600 --> 00:17:35,600
And the consequences that we didn't realize

198
00:17:35,600 --> 00:17:37,600
there are connections, causal connections

199
00:17:37,600 --> 00:17:41,600
between things like desire and cruelty.

200
00:17:41,600 --> 00:17:46,600
How awful this man is to this woman.

201
00:17:46,600 --> 00:17:50,600
You can imagine knowing what we know about how great

202
00:17:50,600 --> 00:17:53,600
Buddha's teaching is.

203
00:17:53,600 --> 00:17:57,600
What a horrible thing to do to prevent,

204
00:17:57,600 --> 00:18:00,600
to do whatever you can to prevent someone from hearing it.

205
00:18:00,600 --> 00:18:02,600
And how blind you have to be.

206
00:18:02,600 --> 00:18:04,600
It's easy to see.

207
00:18:04,600 --> 00:18:08,600
This man's focus was so strongly

208
00:18:08,600 --> 00:18:11,600
on the things that he liked, right?

209
00:18:11,600 --> 00:18:14,600
He had cultivated such an addiction

210
00:18:14,600 --> 00:18:15,600
that it blinded him.

211
00:18:15,600 --> 00:18:17,600
But he probably didn't even think about whether it was

212
00:18:17,600 --> 00:18:18,600
good or evil.

213
00:18:18,600 --> 00:18:21,600
His mind was so clouded.

214
00:18:21,600 --> 00:18:23,600
That's what you see.

215
00:18:23,600 --> 00:18:26,600
It's one thing to want something.

216
00:18:26,600 --> 00:18:30,600
And then to know that you're feeling

217
00:18:30,600 --> 00:18:34,600
and that you don't want to share it with someone else.

218
00:18:34,600 --> 00:18:39,600
That you don't want to lose it for someone else's gain.

219
00:18:39,600 --> 00:18:44,600
And that's basically to put it in general terms.

220
00:18:44,600 --> 00:18:47,600
If this woman gains something,

221
00:18:47,600 --> 00:18:50,600
then he's going to lose something.

222
00:18:50,600 --> 00:18:53,600
So to an ordinary, ordinarily,

223
00:18:53,600 --> 00:18:55,600
we weigh these things, right?

224
00:18:55,600 --> 00:18:58,600
We think, oh, if this woman goes to the Buddha,

225
00:18:58,600 --> 00:19:02,600
I'm going to lose my support.

226
00:19:02,600 --> 00:19:05,600
But of course, an ordinary human being

227
00:19:05,600 --> 00:19:10,600
with even an ounce of our shred of decency

228
00:19:10,600 --> 00:19:15,600
would feel ashamed and acknowledge

229
00:19:15,600 --> 00:19:18,600
that it's much more important that this woman goes

230
00:19:18,600 --> 00:19:21,600
to talk to the Buddha.

231
00:19:21,600 --> 00:19:23,600
But you see this.

232
00:19:23,600 --> 00:19:27,600
It gets to the point where it blinds you.

233
00:19:27,600 --> 00:19:30,600
I think you have to say that it's on another level.

234
00:19:30,600 --> 00:19:33,600
This is why the Buddha brings up the idea of views.

235
00:19:33,600 --> 00:19:36,600
Because your view is blind you.

236
00:19:36,600 --> 00:19:39,600
This person's view, and it's an odd word.

237
00:19:39,600 --> 00:19:43,600
But if we think of it more as inability to see his blindness,

238
00:19:43,600 --> 00:19:45,600
he is blinded.

239
00:19:45,600 --> 00:19:46,600
It can't see.

240
00:19:46,600 --> 00:19:48,600
His view is this.

241
00:19:48,600 --> 00:19:50,600
This is his view.

242
00:19:50,600 --> 00:19:53,600
And by being in being blind,

243
00:19:53,600 --> 00:19:56,600
because of his blindness,

244
00:19:56,600 --> 00:20:01,600
he does whatever he can to stop this woman

245
00:20:01,600 --> 00:20:06,600
from benefiting.

246
00:20:06,600 --> 00:20:16,600
But the point for us, this is a good lesson for us

247
00:20:16,600 --> 00:20:17,600
to think of this man.

248
00:20:17,600 --> 00:20:23,600
But a much better lesson is to pondering consider

249
00:20:23,600 --> 00:20:34,600
that the immediate relationship between things

250
00:20:34,600 --> 00:20:43,600
like desire and our lack of compassion,

251
00:20:43,600 --> 00:20:47,600
stinginess, jealousy, avarice, envy,

252
00:20:47,600 --> 00:20:53,600
everything, and cruelty.

253
00:20:53,600 --> 00:20:56,600
You can't just go ahead and want and want and want

254
00:20:56,600 --> 00:20:59,600
and still be a kind and gentle person.

255
00:20:59,600 --> 00:21:02,600
Actually, the way it is is more you want.

256
00:21:02,600 --> 00:21:04,600
It starts to suck away.

257
00:21:04,600 --> 00:21:07,600
It starts to eat away at your goodness.

258
00:21:07,600 --> 00:21:12,600
You can be a good person and start to want and to like things.

259
00:21:12,600 --> 00:21:15,600
But your wants and your needs.

260
00:21:15,600 --> 00:21:19,600
The reason why we don't see this is because ordinary life

261
00:21:19,600 --> 00:21:22,600
is full of evil.

262
00:21:22,600 --> 00:21:26,600
Ordinary human life, we don't think of it this way.

263
00:21:26,600 --> 00:21:29,600
And it only really makes sense if you actually take the time

264
00:21:29,600 --> 00:21:32,600
to meditate and become sensitive.

265
00:21:32,600 --> 00:21:34,600
Because that's what it is.

266
00:21:34,600 --> 00:21:36,600
If you're living in a cesspool,

267
00:21:36,600 --> 00:21:43,600
you aren't sensitive to dirt, to filth.

268
00:21:43,600 --> 00:21:48,600
But once you begin to purify your mind,

269
00:21:48,600 --> 00:21:51,600
like a person who's left a cesspool,

270
00:21:51,600 --> 00:21:53,600
taking the shower.

271
00:21:53,600 --> 00:21:55,600
And when they go anywhere near the cesspool,

272
00:21:55,600 --> 00:21:58,600
they're completely revolted.

273
00:21:58,600 --> 00:22:01,600
And become more sensitive to it.

274
00:22:01,600 --> 00:22:03,600
I mean, good people are more.

275
00:22:03,600 --> 00:22:05,600
This is what we talked about last time.

276
00:22:05,600 --> 00:22:08,600
Good people are quite sensitive to evil.

277
00:22:08,600 --> 00:22:10,600
Evil people are not sensitive to you.

278
00:22:10,600 --> 00:22:12,600
They're quite sensitive to good.

279
00:22:12,600 --> 00:22:17,600
And allergic to it, I might say.

280
00:22:17,600 --> 00:22:22,600
And this is a really important point with craving.

281
00:22:22,600 --> 00:22:28,600
Craving isn't so much about the consequences,

282
00:22:28,600 --> 00:22:33,600
in terms of the later you might not get what you want.

283
00:22:33,600 --> 00:22:36,600
And the upset, that's important.

284
00:22:36,600 --> 00:22:40,600
But much more important is the very nature of the act

285
00:22:40,600 --> 00:22:44,600
or the state of wanting something.

286
00:22:44,600 --> 00:22:47,600
That it corrupts the mind.

287
00:22:47,600 --> 00:22:49,600
It has an immediate effect.

288
00:22:49,600 --> 00:22:52,600
And it has a direct relationship

289
00:22:52,600 --> 00:22:54,600
with your clarity, your purity,

290
00:22:54,600 --> 00:22:57,600
your goodness of heart.

291
00:22:57,600 --> 00:23:00,600
As a part of what you see in meditation,

292
00:23:00,600 --> 00:23:02,600
you can see the right.

293
00:23:02,600 --> 00:23:05,600
You can see and feel directly how wrong it is

294
00:23:05,600 --> 00:23:07,600
to cling to things, to desire things,

295
00:23:07,600 --> 00:23:10,600
which seems so foreign to most people.

296
00:23:10,600 --> 00:23:13,600
There's this idea that it's part of being human.

297
00:23:13,600 --> 00:23:15,600
And it's more static, right?

298
00:23:15,600 --> 00:23:16,600
We think of it as static.

299
00:23:16,600 --> 00:23:19,600
I like eggs, as though you've always liked eggs

300
00:23:19,600 --> 00:23:21,600
and you always will like eggs.

301
00:23:21,600 --> 00:23:23,600
And it's just part of who you are.

302
00:23:23,600 --> 00:23:25,600
Which is not at all the case.

303
00:23:25,600 --> 00:23:26,600
It's a habit.

304
00:23:26,600 --> 00:23:29,600
And by reinforcing it, you're strengthening it.

305
00:23:29,600 --> 00:23:32,600
And it's pulling on everything else.

306
00:23:32,600 --> 00:23:35,600
It's taking energy away from good things you can be doing.

307
00:23:35,600 --> 00:23:37,600
And it's corrupting them.

308
00:23:37,600 --> 00:23:38,600
It's lowering.

309
00:23:38,600 --> 00:23:43,600
It's reducing your interest in being a good person.

310
00:23:43,600 --> 00:23:49,600
So this is the sorts of things that are interesting

311
00:23:49,600 --> 00:23:52,600
in regards to the story.

312
00:23:52,600 --> 00:23:55,600
The verse, again, related though it is,

313
00:23:55,600 --> 00:24:03,600
does clearly teach a different, a different lesson.

314
00:24:03,600 --> 00:24:05,600
But related.

315
00:24:05,600 --> 00:24:09,600
And it's related to the idea of karma.

316
00:24:09,600 --> 00:24:20,600
And it offers an interesting idea that

317
00:24:20,600 --> 00:24:25,600
there's something especially bad about

318
00:24:25,600 --> 00:24:31,600
evil done in relation to Buddhism.

319
00:24:31,600 --> 00:24:35,600
I mean, that's how it's the superficial expression.

320
00:24:35,600 --> 00:24:45,600
But what it's really saying is that when you oppose

321
00:24:45,600 --> 00:24:49,600
good things, your opposition to good things

322
00:24:49,600 --> 00:24:57,600
is resulting results in evil proportionate

323
00:24:57,600 --> 00:25:00,600
to the goodness of the thing that you're opposing.

324
00:25:00,600 --> 00:25:05,600
That's really the Buddhist dogma or doctrine.

325
00:25:05,600 --> 00:25:09,600
In Buddhism, what's the most pure and beautiful and wonderful

326
00:25:09,600 --> 00:25:11,600
and good thing?

327
00:25:11,600 --> 00:25:15,600
Of course, it's the teaching of the Buddha,

328
00:25:15,600 --> 00:25:18,600
not just in its theoretical state,

329
00:25:18,600 --> 00:25:20,600
but in terms of the dissemination.

330
00:25:20,600 --> 00:25:24,600
Anyone who gets in the way of someone hearing

331
00:25:24,600 --> 00:25:26,600
the Buddha's teaching,

332
00:25:26,600 --> 00:25:29,600
I mean, it's understandably thought to be doing

333
00:25:29,600 --> 00:25:31,600
one of the most horrific things possible.

334
00:25:31,600 --> 00:25:35,600
Not the most, but it's got to be up there.

335
00:25:35,600 --> 00:25:37,600
To cut someone off.

336
00:25:37,600 --> 00:25:43,600
To purposefully prevent someone from such a powerful good.

337
00:25:43,600 --> 00:25:48,600
It's a special sort of evil.

338
00:25:48,600 --> 00:25:53,600
It takes for this man to allow his desires

339
00:25:53,600 --> 00:25:58,600
to lead him to actually

340
00:25:58,600 --> 00:26:01,600
ignore this woman's good or prevent her.

341
00:26:01,600 --> 00:26:03,600
It'll be so blind.

342
00:26:03,600 --> 00:26:07,600
It takes blindness as the point.

343
00:26:07,600 --> 00:26:09,600
Beyond ordinary blindness.

344
00:26:09,600 --> 00:26:12,600
It takes a mindset.

345
00:26:12,600 --> 00:26:14,600
It really would continue.

346
00:26:14,600 --> 00:26:16,600
It was the word view, and that's what it means.

347
00:26:16,600 --> 00:26:18,600
But it relates to mindset.

348
00:26:18,600 --> 00:26:22,600
This person just is perverse, really.

349
00:26:22,600 --> 00:26:27,600
They're mindless corrupt or perverse twisted as the point.

350
00:26:27,600 --> 00:26:34,600
The way they look at the world is so mixed up, messed up.

351
00:26:34,600 --> 00:26:36,600
That's really what view is all about.

352
00:26:36,600 --> 00:26:38,600
So that's what this is teaching.

353
00:26:38,600 --> 00:26:41,600
It's teaching about views.

354
00:26:41,600 --> 00:26:43,600
So it's more than one thing.

355
00:26:43,600 --> 00:26:46,600
It's teaching about the special evil of reviling the Buddha,

356
00:26:46,600 --> 00:26:50,600
the reviling the Dhamma, the Sangha,

357
00:26:50,600 --> 00:26:55,600
of attacking them.

358
00:26:55,600 --> 00:26:59,600
And it's talking about the evil that's required to do that.

359
00:26:59,600 --> 00:27:05,600
The evil of views out twisted you have to be.

360
00:27:05,600 --> 00:27:11,600
And so that's how it's all relates to meditation and how views

361
00:27:11,600 --> 00:27:12,600
come into play.

362
00:27:12,600 --> 00:27:14,600
And what views really mean in Buddhism,

363
00:27:14,600 --> 00:27:16,600
and why they're such an important thing.

364
00:27:16,600 --> 00:27:19,600
The views that we're concerned about,

365
00:27:19,600 --> 00:27:22,600
the concept of views, is the twisted way of looking at the world.

366
00:27:22,600 --> 00:27:24,600
I read this question about what is right view.

367
00:27:24,600 --> 00:27:30,600
Well, theoretically, right view is anything that's in line with reality.

368
00:27:30,600 --> 00:27:33,600
But that's not really what we're concerned about.

369
00:27:33,600 --> 00:27:36,600
We're concerned with this obtaining noble view,

370
00:27:36,600 --> 00:27:40,600
which really means a straight relationship with reality,

371
00:27:40,600 --> 00:27:43,600
where you see things, you connect with reality,

372
00:27:43,600 --> 00:27:46,600
this is this, it is what it is.

373
00:27:46,600 --> 00:27:47,600
It sounds simple.

374
00:27:47,600 --> 00:27:49,600
That is right view.

375
00:27:49,600 --> 00:27:52,600
So hard.

376
00:27:52,600 --> 00:27:56,600
We think, well, it must be something more to it than that.

377
00:27:56,600 --> 00:27:58,600
The problem is there's so much more to it than that,

378
00:27:58,600 --> 00:28:02,600
and all that more to it is what's wrong.

379
00:28:02,600 --> 00:28:06,600
It makes so much much out of everything and twist things.

380
00:28:06,600 --> 00:28:08,600
It's really two different things.

381
00:28:08,600 --> 00:28:10,600
You can make more of something than it is.

382
00:28:10,600 --> 00:28:12,600
Like, this is good, this is bad.

383
00:28:12,600 --> 00:28:15,600
And that's already twisted, never mind.

384
00:28:15,600 --> 00:28:19,600
As soon as you see something as good or bad, it's twisted.

385
00:28:19,600 --> 00:28:23,600
You can make something more out of something like, say,

386
00:28:23,600 --> 00:28:24,600
this is a hand.

387
00:28:24,600 --> 00:28:26,600
It's not actually a hand.

388
00:28:26,600 --> 00:28:27,600
It's light touching your eye.

389
00:28:27,600 --> 00:28:29,600
And even that is making a little more out of it.

390
00:28:29,600 --> 00:28:31,600
It's seeing.

391
00:28:31,600 --> 00:28:33,600
This is seeing.

392
00:28:33,600 --> 00:28:35,600
But you think it's a hand.

393
00:28:35,600 --> 00:28:37,600
That's making more out of it than it actually is.

394
00:28:37,600 --> 00:28:39,600
That's not really a problem.

395
00:28:39,600 --> 00:28:42,600
It's potentially leading to problems.

396
00:28:42,600 --> 00:28:45,600
But the real problem is when you get twisted and you say,

397
00:28:45,600 --> 00:28:49,600
oh, would a beautiful hand, would an ugly hand?

398
00:28:49,600 --> 00:28:51,600
Would a stupid thing for him to do to raise his hand?

399
00:28:51,600 --> 00:28:54,600
Or would I really like that?

400
00:28:54,600 --> 00:28:55,600
He's doing that.

401
00:28:55,600 --> 00:28:56,600
Maybe you think it's funny.

402
00:28:56,600 --> 00:28:58,600
Or maybe you think it's clever.

403
00:28:58,600 --> 00:29:01,600
And you start to twist it.

404
00:29:01,600 --> 00:29:04,600
Maybe you think you're clever because you understand that.

405
00:29:04,600 --> 00:29:06,600
Maybe you think you're more clever because you could have thought

406
00:29:06,600 --> 00:29:09,600
of a better way to explain it, right?

407
00:29:09,600 --> 00:29:15,600
So much twisting and turning.

408
00:29:15,600 --> 00:29:19,600
Maybe we can make another, we can tie this into the idea of the read.

409
00:29:19,600 --> 00:29:20,600
It's like you have this read.

410
00:29:20,600 --> 00:29:24,600
And if the read is straight, it can bear all that fruit.

411
00:29:24,600 --> 00:29:28,600
But when it's twisted, it snaps.

412
00:29:28,600 --> 00:29:33,600
The twisting of the mind, what the twisting of the mind really does is it

413
00:29:33,600 --> 00:29:39,600
makes you twisted and you can't see the body.

414
00:29:39,600 --> 00:29:41,600
It's like you become mentally a hunchback.

415
00:29:41,600 --> 00:29:43,600
That's what I'm trying to say.

416
00:29:43,600 --> 00:29:47,600
It's like you look at someone whose body is back.

417
00:29:47,600 --> 00:29:51,600
It's not straight.

418
00:29:51,600 --> 00:29:53,600
This is the way the mind goes.

419
00:29:53,600 --> 00:29:54,600
The mind becomes...

420
00:29:54,600 --> 00:29:56,600
It coils in on itself.

421
00:29:56,600 --> 00:30:01,600
It requires evil sights.

422
00:30:01,600 --> 00:30:04,600
The way it's heavily on the mind is what the Buddha says.

423
00:30:04,600 --> 00:30:09,600
The way it's so heavily on the mind that it breaks the mind.

424
00:30:09,600 --> 00:30:10,600
That's a good example.

425
00:30:10,600 --> 00:30:14,600
This man was clearly so consumed by evil.

426
00:30:14,600 --> 00:30:18,600
That he did the unthinkable.

427
00:30:18,600 --> 00:30:20,600
I mean, there are worse things he could have done.

428
00:30:20,600 --> 00:30:23,600
Like he could have come and tried to kill the Buddha or something.

429
00:30:23,600 --> 00:30:28,600
Oh no, if I kill the Buddha she won't be able to hear his teachings.

430
00:30:28,600 --> 00:30:30,600
That would have been worse.

431
00:30:30,600 --> 00:30:37,600
But it's still, it's hard to fathom the gall of this man.

432
00:30:37,600 --> 00:30:39,600
I'm clearly caused by evil.

433
00:30:39,600 --> 00:30:43,600
This isn't just a fairy tale.

434
00:30:43,600 --> 00:30:46,600
This is happening all the time.

435
00:30:46,600 --> 00:30:52,600
Today there was a mass shooting in Las Vegas.

436
00:30:52,600 --> 00:30:58,600
And I think it had something to do with that apparently.

437
00:30:58,600 --> 00:31:03,600
I don't really follow the news, but something to do with the gambling debt.

438
00:31:03,600 --> 00:31:06,600
It's just very apropos to this sort of story.

439
00:31:06,600 --> 00:31:08,600
Can you imagine?

440
00:31:08,600 --> 00:31:09,600
Incredible.

441
00:31:09,600 --> 00:31:11,600
Incredible to think.

442
00:31:11,600 --> 00:31:18,600
If assuming that this is what I heard or what I read was what really happened.

443
00:31:18,600 --> 00:31:27,600
Someone became so consumed by greed and anger at their loss.

444
00:31:27,600 --> 00:31:35,600
That they went out to shot hundreds of people or something.

445
00:31:35,600 --> 00:31:37,600
It's just a fairy tale.

446
00:31:37,600 --> 00:31:41,600
It's just reality.

447
00:31:41,600 --> 00:31:45,600
It had a very real consequences to our evil.

448
00:31:45,600 --> 00:31:48,600
And you think, wow, you're over stating it.

449
00:31:48,600 --> 00:31:53,600
My evil is not like that man's evil.

450
00:31:53,600 --> 00:31:58,600
It's only a matter of degree.

451
00:31:58,600 --> 00:31:59,600
That's why we call it evil.

452
00:31:59,600 --> 00:32:02,600
We don't beat around the bush and say, it's okay.

453
00:32:02,600 --> 00:32:03,600
Everyone likes things.

454
00:32:03,600 --> 00:32:04,600
It's not really evil.

455
00:32:04,600 --> 00:32:05,600
It is.

456
00:32:05,600 --> 00:32:06,600
It really is evil.

457
00:32:06,600 --> 00:32:08,600
It's evil to want anything.

458
00:32:08,600 --> 00:32:10,600
It's just a matter of degree.

459
00:32:10,600 --> 00:32:12,600
But what we don't get.

460
00:32:12,600 --> 00:32:15,600
We think it's okay to like, just don't get out of hand.

461
00:32:15,600 --> 00:32:17,600
It's all out of hand.

462
00:32:17,600 --> 00:32:18,600
It's all wrong.

463
00:32:18,600 --> 00:32:21,600
There's no good that comes from wanting or liking things.

464
00:32:21,600 --> 00:32:24,600
It's a hard build to swallow.

465
00:32:24,600 --> 00:32:28,600
If someone is mindful, there's no other way.

466
00:32:28,600 --> 00:32:34,600
You can't see things that way anymore.

467
00:32:34,600 --> 00:32:37,600
That wanting could be in any way good.

468
00:32:37,600 --> 00:32:40,600
You can't deny the fact.

469
00:32:40,600 --> 00:32:42,600
You don't want to deny the fact.

470
00:32:42,600 --> 00:32:47,600
You free yourself from the blindness of thinking that there's some good to be had

471
00:32:47,600 --> 00:32:52,600
from liking or wanting.

472
00:32:52,600 --> 00:32:56,600
So that's the Dhamapanda for tonight.

473
00:32:56,600 --> 00:33:24,600
Thank you all for tuning in.

