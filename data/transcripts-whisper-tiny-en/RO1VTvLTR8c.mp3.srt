1
00:00:00,000 --> 00:00:07,000
But what if the Buddha was merely highly advanced in a particular spiritual technique?

2
00:00:07,000 --> 00:00:12,000
It seems in order to reap the benefits of training you have to put everything into it.

3
00:00:12,000 --> 00:00:19,000
Wouldn't this same amount of effort bring success in any field?

4
00:00:19,000 --> 00:00:28,000
Yeah, in one sense, yes, the Buddha taught the four Itipada, which are the roads or the paths to success.

5
00:00:28,000 --> 00:00:30,000
One way of translating them.

6
00:00:30,000 --> 00:00:36,000
So, whatever you want to do, you have to practice these four Itipada.

7
00:00:36,000 --> 00:00:37,000
You're welcome.

8
00:00:37,000 --> 00:00:41,000
Obviously, you're welcome to have that sort of a theory.

9
00:00:41,000 --> 00:00:46,000
The Buddha was just someone who was highly developed in a specific path.

10
00:00:46,000 --> 00:00:48,000
So, we had this question on the ask form.

11
00:00:48,000 --> 00:00:50,000
Someone was talking about the pointlessness.

12
00:00:50,000 --> 00:00:53,000
And this isn't really neat concept to explore.

13
00:00:53,000 --> 00:00:58,000
Because, of course, it's very volatile to start talking about life as being meaningless and pointless.

14
00:00:58,000 --> 00:01:04,000
But I was thinking about, and I do stand by my answer for what it's worth,

15
00:01:04,000 --> 00:01:07,000
that it's the meaningless that gives us power.

16
00:01:07,000 --> 00:01:08,000
You can do anything you want.

17
00:01:08,000 --> 00:01:10,000
You want to follow this path, follow this path.

18
00:01:10,000 --> 00:01:12,000
You want to follow that path, follow that path.

19
00:01:12,000 --> 00:01:15,000
You want to become king of the world, follow the path to become king of the world.

20
00:01:15,000 --> 00:01:18,000
You want to follow the path to become god.

21
00:01:18,000 --> 00:01:20,000
You want to become god, follow the path to become god.

22
00:01:20,000 --> 00:01:24,000
You want to go to hell, follow the path to go to hell.

23
00:01:24,000 --> 00:01:25,000
There's paths for everything.

24
00:01:25,000 --> 00:01:26,000
There's no purpose.

25
00:01:26,000 --> 00:01:27,000
You make your purpose.

26
00:01:27,000 --> 00:01:28,000
You want to be this, be this.

27
00:01:28,000 --> 00:01:31,000
You want to be that, be that.

28
00:01:31,000 --> 00:01:38,000
So, it is fair in one sense to say that the Buddha was just the teacher of another path.

29
00:01:38,000 --> 00:01:46,000
The problem is that it's all meaningless, that all paths are meaningless.

30
00:01:46,000 --> 00:01:48,000
How could they have meaning?

31
00:01:48,000 --> 00:01:50,000
You've become god, okay, you're god.

32
00:01:50,000 --> 00:01:53,000
There's no intrinsic meaning in that, right?

33
00:01:53,000 --> 00:01:58,000
You've become king of the world, okay, you're king of the world, right?

34
00:01:58,000 --> 00:02:00,000
And this is the truth.

35
00:02:00,000 --> 00:02:02,000
This is an unmitigatable truth.

36
00:02:02,000 --> 00:02:06,000
This is objective for all the paths that they all are meaning.

37
00:02:06,000 --> 00:02:11,000
You could even say that, I don't want to quite go there, but now I'm not going to say it.

38
00:02:11,000 --> 00:02:16,000
I think the one exception, I would say the one exception is the Buddhist path, right?

39
00:02:16,000 --> 00:02:19,000
And of course everyone says it about their path, but hear me out.

40
00:02:19,000 --> 00:02:23,000
Because once you realize that all paths are meaningless, do let go.

41
00:02:23,000 --> 00:02:29,000
You stop striving, you stop looking for a path, you stop creating karma.

42
00:02:29,000 --> 00:02:33,000
And that is the teach, in one way is the teaching of the Buddha.

43
00:02:33,000 --> 00:02:45,000
So this is a theory that the preposition is that it is a special, that the Buddha was not just another teacher in one sense.

44
00:02:45,000 --> 00:02:49,000
And that is the sense that he taught the giving up of all paths.

45
00:02:49,000 --> 00:02:58,000
His path was the giving up of striving for anything, for any state of being.

46
00:02:58,000 --> 00:03:04,000
And so, if we talk about the Buddha's path as being meaningless or the goal, which is nibana, as being meaningless,

47
00:03:04,000 --> 00:03:13,000
I think it's very difficult and it's probably inaccurate to say, because it's a non-path.

48
00:03:13,000 --> 00:03:18,000
It's not going, it's not coming, it's not creating anything.

49
00:03:18,000 --> 00:03:23,000
As Sumena said, it's not even remembering.

50
00:03:23,000 --> 00:03:29,000
So you'd be hard pressed to say that it's meaningless.

51
00:03:29,000 --> 00:03:32,000
Because there's nothing that you could grasp on and say, look, you see?

52
00:03:32,000 --> 00:03:36,000
No meaning to that, or what is this for or so on.

53
00:03:36,000 --> 00:03:42,000
There's no handle by which you could grasp and say, this is meaningless.

54
00:03:42,000 --> 00:03:44,000
So they actually do say it's meaningful.

55
00:03:44,000 --> 00:03:48,000
It has benefited as purpose.

56
00:03:48,000 --> 00:03:55,000
I don't know if I'd go so far, but the texts do say that.

57
00:03:55,000 --> 00:04:19,000
You got anything?

