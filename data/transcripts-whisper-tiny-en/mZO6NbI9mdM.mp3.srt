1
00:00:00,000 --> 00:00:05,000
Okay, the next question comes from Ginger Catney.

2
00:00:05,000 --> 00:00:12,000
Is it okay to practice Uposita once a month instead of the usual weekly times?

3
00:00:12,000 --> 00:00:17,000
I have a mild blood pressure problem, and fasting seems to really cause my blood pressure to drop,

4
00:00:17,000 --> 00:00:19,000
to the point I feel lightheaded.

5
00:00:19,000 --> 00:00:35,000
Yes, the word Uposita means to observe or to undertake the observance.

6
00:00:35,000 --> 00:00:38,000
And so it's another word for holiday.

7
00:00:38,000 --> 00:00:41,000
So in Buddhism, we have a weekly holiday.

8
00:00:41,000 --> 00:00:46,000
It's a lunar week, so on the full moon, the empty moon and the half moons.

9
00:00:46,000 --> 00:00:52,000
We will undertake a day of holiday.

10
00:00:52,000 --> 00:00:57,000
And what we do on the holidays will keep the meditators preset.

11
00:00:57,000 --> 00:01:04,000
So for late people, for monks it's different, but late people will go from keeping basic morality

12
00:01:04,000 --> 00:01:11,000
to keeping meditators morality, meaning they'll abstain from sexual activity.

13
00:01:11,000 --> 00:01:16,000
They'll abstain from eating in the afternoon.

14
00:01:16,000 --> 00:01:20,000
They'll abstain from entertainment and beautification.

15
00:01:20,000 --> 00:01:23,000
And they will sleep on the floor.

16
00:01:23,000 --> 00:01:27,000
So they'll keep eight rules altogether of conduct.

17
00:01:27,000 --> 00:01:29,000
And these are the meditators presets.

18
00:01:29,000 --> 00:01:34,000
They're the state of morality for someone who's practicing meditation.

19
00:01:34,000 --> 00:01:36,000
Normally we do that every week.

20
00:01:36,000 --> 00:01:39,000
Yes, it's fine if you just want to do it once a month.

21
00:01:39,000 --> 00:01:44,000
What I would say about feeling lightheaded is that it's not a problem.

22
00:01:44,000 --> 00:01:46,000
It's not going to kill you.

23
00:01:46,000 --> 00:01:52,000
And I wouldn't think that it's going to cause any serious health side effects,

24
00:01:52,000 --> 00:01:54,000
if your doctor says otherwise.

25
00:01:54,000 --> 00:01:56,000
You're welcome to listen to them.

26
00:01:56,000 --> 00:02:03,000
I felt lightheaded before when I didn't have as much food as usual.

27
00:02:03,000 --> 00:02:08,000
But I think you'll find that meditating on the lightheadedness helps you

28
00:02:08,000 --> 00:02:10,000
to see that it's not really an issue.

29
00:02:10,000 --> 00:02:11,000
It's not really a problem.

30
00:02:11,000 --> 00:02:13,000
It's just another state of being.

31
00:02:13,000 --> 00:02:21,000
And in fact, it helps you to learn to deal with difficult situations.

32
00:02:21,000 --> 00:02:25,000
The fasting is in some ways, at least in the beginning,

33
00:02:25,000 --> 00:02:33,000
it's meant to do that for us, to open us up to this idea that we can live with less

34
00:02:33,000 --> 00:02:39,000
than optimal states of body and mind that we can live with

35
00:02:39,000 --> 00:02:48,000
states of fatigue, states of various states of suffering

36
00:02:48,000 --> 00:02:55,000
that we don't have to always be in a state of perfect pleasure and happiness.

37
00:02:55,000 --> 00:03:05,000
And so I would say go for it every week.

38
00:03:05,000 --> 00:03:09,000
I would say don't let that be a hindrance to your

39
00:03:09,000 --> 00:03:10,000
minute to your practice.

40
00:03:10,000 --> 00:03:14,000
And I would like to encourage people who haven't heard of this practice before

41
00:03:14,000 --> 00:03:16,000
to take it as well.

42
00:03:16,000 --> 00:03:19,000
If you are interested in the Buddhist teaching,

43
00:03:19,000 --> 00:03:24,000
you can take that as a weekly marker

44
00:03:24,000 --> 00:03:26,000
that today I'm going to keep these precepts.

45
00:03:26,000 --> 00:03:28,000
I'm not going to eat in the afternoon.

46
00:03:28,000 --> 00:03:30,000
I'm not going to engage in sexuality.

47
00:03:30,000 --> 00:03:36,000
I'm not going to engage in entertainment or beautification.

48
00:03:36,000 --> 00:03:39,000
And I'm not going to sleep on the bed.

49
00:03:39,000 --> 00:03:42,000
I'll sleep on the floor like meditators do.

50
00:03:42,000 --> 00:04:02,000
So good luck with that.

