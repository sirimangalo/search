1
00:00:00,000 --> 00:00:29,440
Good evening everyone.

2
00:00:29,440 --> 00:00:32,980
Good.

3
00:00:32,980 --> 00:00:39,680
Good afternoon, good afternoon.

4
00:00:39,680 --> 00:00:50,240
From 2020 Thursday to 2016.

5
00:00:50,240 --> 00:00:53,440
annulai sayth bodha pannal

6
00:00:52,960 --> 00:00:56,440
samap fapas beginner river

7
00:00:55,600 --> 00:00:58,280
ku tala s fthappada

8
00:00:58,200 --> 00:01:02,560
sadhita picked

9
00:01:04,600 --> 00:01:08,800
�nta payi o got panarita

10
00:01:08,800 --> 00:01:23,800
And we have quotes for quotes about the teaching of the Buddhas.

11
00:01:23,800 --> 00:01:33,800
It's interesting he's pulled together quotes from three different places and he's skipped

12
00:01:33,800 --> 00:01:34,800
one.

13
00:01:34,800 --> 00:01:39,800
I could have added a fifth, I'm not sure why he didn't.

14
00:01:39,800 --> 00:02:07,800
But once we have our

15
00:02:07,800 --> 00:02:14,800
he's to do evil.

16
00:02:14,800 --> 00:02:17,800
He's not doing a good translation.

17
00:02:17,800 --> 00:02:20,800
He's to do evil means.

18
00:02:20,800 --> 00:02:23,800
He's not doing all evil.

19
00:02:23,800 --> 00:02:30,800
He's a little person, but I'll be coming full of or coming accomplished.

20
00:02:30,800 --> 00:02:40,800
In regards to awesomeness or goodness or skillfulness.

21
00:02:40,800 --> 00:02:47,800
Skillful things, good things.

22
00:02:47,800 --> 00:02:52,800
Satitapriyodhapalang and the purification of one's own mind.

23
00:02:52,800 --> 00:02:59,800
Satitapriyodhana.

24
00:02:59,800 --> 00:03:08,800
Satitapriyodhana.

25
00:03:08,800 --> 00:03:15,800
Satitapriyodhana.

26
00:03:15,800 --> 00:03:24,800
Satitapriyodhana.

27
00:03:24,800 --> 00:03:35,800
Satitapriyodhana.

28
00:03:35,800 --> 00:03:42,800
Moderation in regards to food.

29
00:03:42,800 --> 00:03:45,800
Bandanjatsayunasana.

30
00:03:45,800 --> 00:03:52,800
Having a dwelling that is included.

31
00:03:52,800 --> 00:04:01,800
Satitapriyodhana.

32
00:04:01,800 --> 00:04:08,800
Higher mind.

33
00:04:08,800 --> 00:04:15,800
Satitapriyodhana.

34
00:04:15,800 --> 00:04:28,800
Satitapriyodhana.

35
00:04:28,800 --> 00:04:39,800
Satitapriyodhana.

36
00:04:39,800 --> 00:04:47,800
Satitapriyodhana.

37
00:04:47,800 --> 00:04:54,800
Satitapriyodhana.

38
00:04:54,800 --> 00:04:57,520
quoting what the text had said about

39
00:04:57,520 --> 00:05:00,920
why the Buddha teaches in parables

40
00:05:00,920 --> 00:05:04,320
then I redid it because I went over it again and I had to

41
00:05:04,320 --> 00:05:08,720
edit it to say, well the text says that the Buddha says this

42
00:05:08,720 --> 00:05:13,280
the Buddha is quoted as saying or

43
00:05:13,280 --> 00:05:20,280
is claim to have said or something like that

44
00:05:20,280 --> 00:05:30,200
is I don't believe that the Buddha said the stuff that they will notice

45
00:05:30,200 --> 00:05:33,960
through traffic, I don't think that many people

46
00:05:33,960 --> 00:05:40,120
don't besides those who are developed Mahayana Buddhists

47
00:05:40,120 --> 00:05:43,560
the question is what did the Buddha teach and what is the Buddha teaching

48
00:05:43,560 --> 00:05:52,760
I think most of us agree that the four normal truths through the Buddha is teaching

49
00:05:52,760 --> 00:05:57,280
really the teaching of all Buddhas

50
00:05:57,280 --> 00:06:01,680
so it's a good reason for us to start there

51
00:06:01,680 --> 00:06:08,040
because I love that in Mahayana Buddhism they also talk about the four

52
00:06:08,040 --> 00:06:13,920
number of truths why we know it's the teaching of the Buddha

53
00:06:13,920 --> 00:06:25,200
we don't find it anywhere else someone had to come up with this

54
00:06:25,200 --> 00:06:29,760
this is something unique to the Buddha's teaching

55
00:06:29,760 --> 00:06:45,680
have the idea that suffering is caused by craving

56
00:06:45,680 --> 00:06:51,840
suppose it's not even that unique if you think about it

57
00:06:51,840 --> 00:06:57,800
maybe the eight full noble path because you know in India at the time

58
00:06:57,800 --> 00:07:02,800
a lot of people thought the desire was

59
00:07:02,800 --> 00:07:11,880
there was a cause of suffering

60
00:07:11,880 --> 00:07:15,160
and people were clear about birth being suffering old age being suffering

61
00:07:15,160 --> 00:07:17,160
death being suffering

62
00:07:17,160 --> 00:07:20,840
a lot of ascetics knew this sort of thing

63
00:07:20,840 --> 00:07:25,520
what they didn't know is how to be free from desire

64
00:07:25,520 --> 00:07:32,520
talked about desires and they would torture themselves to try to free them from desire

65
00:07:32,520 --> 00:07:37,880
so one of the last things the Buddha said before he passed away was that

66
00:07:37,880 --> 00:07:45,120
it's the eight full noble path that determines the religion

67
00:07:45,120 --> 00:07:49,600
and whatever he said in whatever religion

68
00:07:49,600 --> 00:07:56,000
you find the eight full noble path

69
00:07:56,000 --> 00:07:57,800
in that religion you're going to find

70
00:07:57,800 --> 00:08:01,880
sort upon the sake of the kamiana kamiara hunt

71
00:08:01,880 --> 00:08:05,880
you're going to find the mind being

72
00:08:05,880 --> 00:08:08,000
any religion

73
00:08:08,000 --> 00:08:13,960
it doesn't have the eight full noble path

74
00:08:13,960 --> 00:08:17,440
but if you want to talk about what the Buddha teach

75
00:08:17,440 --> 00:08:21,280
thinking the three trainings are the eight full noble path

76
00:08:21,280 --> 00:08:24,880
morality concentration and wisdom

77
00:08:24,880 --> 00:08:29,520
this is probably the best way to explain to someone in that nutshell what

78
00:08:29,520 --> 00:08:31,240
Buddhism is

79
00:08:31,240 --> 00:08:35,760
the eight full noble path

80
00:08:35,760 --> 00:08:42,920
so starting with morality

81
00:08:42,920 --> 00:08:46,960
cultivating right that right action right speech

82
00:08:46,960 --> 00:08:50,400
right livelihood

83
00:08:50,400 --> 00:08:58,040
then right constant then concentration which is

84
00:08:58,040 --> 00:09:02,880
red effort right mindfulness and right

85
00:09:02,880 --> 00:09:05,040
right focus

86
00:09:05,040 --> 00:09:08,280
right concentration

87
00:09:08,280 --> 00:09:10,320
and then right wisdom

88
00:09:10,320 --> 00:09:11,960
or wisdom which is

89
00:09:11,960 --> 00:09:18,960
right thought and right view right view right thought

90
00:09:24,560 --> 00:09:30,440
and I had a meeting today of the Hamilton Interfaith Peace Group

91
00:09:30,440 --> 00:09:33,360
and uh...

92
00:09:33,360 --> 00:09:34,640
it's always been

93
00:09:34,640 --> 00:09:37,800
since I've joined this group

94
00:09:37,800 --> 00:09:43,320
I guess I joined in

95
00:09:43,320 --> 00:09:48,120
and I don't know

96
00:09:48,120 --> 00:09:51,120
the idea of faith is

97
00:09:51,120 --> 00:09:53,840
it's very much

98
00:09:53,840 --> 00:09:57,240
theistic concept right

99
00:09:57,240 --> 00:10:02,480
not to say that Buddhists don't have faith and talked about this before

100
00:10:02,480 --> 00:10:09,480
and it's not exactly and it's not a core concept in Buddhism

101
00:10:09,480 --> 00:10:14,280
it's nowhere near a core content is faith useful yeah faith is a

102
00:10:14,280 --> 00:10:16,320
is a wholesome quality

103
00:10:16,320 --> 00:10:19,360
also in the sense that it gives you strength

104
00:10:19,360 --> 00:10:22,680
but it's possible of course to have faith in something that is unjustify

105
00:10:22,680 --> 00:10:25,720
it's possible for faith to lead you to

106
00:10:25,720 --> 00:10:28,120
danger to suffering

107
00:10:28,120 --> 00:10:31,320
not the faith itself but the thing that you have

108
00:10:31,320 --> 00:10:38,320
faith in

109
00:10:40,840 --> 00:10:44,040
in the face of propels you towards

110
00:10:44,040 --> 00:10:48,000
danger

111
00:10:48,000 --> 00:10:50,040
so it's not

112
00:10:50,040 --> 00:10:52,640
just not considered to be

113
00:10:52,640 --> 00:10:54,440
essential

114
00:10:54,440 --> 00:10:56,640
sense or

115
00:10:56,640 --> 00:11:00,200
it's not central

116
00:11:00,200 --> 00:11:04,960
they post you could say it's essential is one of the faculties but

117
00:11:04,960 --> 00:11:09,960
not in the sense of

118
00:11:09,960 --> 00:11:11,080
you know i mean

119
00:11:11,080 --> 00:11:15,040
faith is

120
00:11:15,040 --> 00:11:18,440
when people talk about faith they're talking about something that they don't

121
00:11:18,440 --> 00:11:20,800
understand

122
00:11:20,800 --> 00:11:24,520
because you can't see the science is faith based

123
00:11:24,520 --> 00:11:28,240
and yet scientists are often very confident of their

124
00:11:28,240 --> 00:11:32,040
their theories

125
00:11:32,040 --> 00:11:35,200
but it's because they have reason to be confident they have faith in their

126
00:11:35,200 --> 00:11:38,840
theories they have faith in their instruments but it's based on experience

127
00:11:38,840 --> 00:11:40,120
it's based

128
00:11:40,120 --> 00:11:42,760
on observations it's based on

129
00:11:42,760 --> 00:11:45,320
knowledge and so on

130
00:11:45,320 --> 00:11:48,360
that's not what is meant by a faith

131
00:11:48,360 --> 00:11:51,400
my faith your faith

132
00:11:51,400 --> 00:11:54,400
religious faith

133
00:11:54,400 --> 00:11:58,440
anyway today and we were talking about this

134
00:11:58,440 --> 00:12:02,800
event that we're going to be holding a nape for storytelling event

135
00:12:02,800 --> 00:12:06,600
we're trying to find the name for it and someone suggested we should put the word

136
00:12:06,600 --> 00:12:09,680
faith in the title

137
00:12:09,680 --> 00:12:12,520
and i said well you know

138
00:12:12,520 --> 00:12:17,960
it wasn't really all that keen to bring it up but i said

139
00:12:17,960 --> 00:12:20,400
if you're inviting a Buddhist cause i'll be

140
00:12:20,400 --> 00:12:23,720
i'll be telling one of the stories if there's a Buddhist and

141
00:12:23,720 --> 00:12:26,760
there it doesn't really

142
00:12:26,760 --> 00:12:30,400
you know it doesn't really fit with Buddhism or not

143
00:12:30,400 --> 00:12:34,880
based we don't talk about faith it's not a word that we use

144
00:12:34,880 --> 00:12:40,600
and caused a little bit of that

145
00:12:40,600 --> 00:12:44,240
there's a little bit of contention there because

146
00:12:44,240 --> 00:12:47,240
the group itself is all about faith

147
00:12:47,240 --> 00:12:57,040
it's called the Hamiltonian Interfaith Peace Group

148
00:12:57,040 --> 00:12:59,640
but the reason i think of that is

149
00:12:59,640 --> 00:13:05,640
because that's really not what the Buddha's teaching is about

150
00:13:05,640 --> 00:13:10,720
no matter how useful you say faith is

151
00:13:10,720 --> 00:13:11,560
i keep

152
00:13:11,560 --> 00:13:15,720
i expected the Buddha's teaching is definitely to move beyond faith

153
00:13:15,720 --> 00:13:20,600
beyond belief in things that you don't understand

154
00:13:20,600 --> 00:13:26,080
the idea is to come to understand

155
00:13:26,080 --> 00:13:30,920
focuses on understanding things on in believing things

156
00:13:30,920 --> 00:13:36,760
and interestingly that's something that distinguishes teravada Buddhism from

157
00:13:36,760 --> 00:13:40,720
what i'm reading about my and a Buddhist without because

158
00:13:40,720 --> 00:13:45,680
at least in the lotus sutra the starts to become an emphasis on faith

159
00:13:45,680 --> 00:13:51,080
the Buddha says everyone is going to become a Buddha and he says you just have to believe me

160
00:13:51,080 --> 00:13:53,480
you're going to have to believe me in this one

161
00:13:53,480 --> 00:13:56,160
believe in me and then

162
00:13:56,160 --> 00:13:59,520
you know it's by the time the lotus sutra came about there was

163
00:13:59,520 --> 00:14:03,680
a lot of funny stuff going on so they started believing that the Buddha

164
00:14:03,680 --> 00:14:06,120
there were heavens full of Buddha's

165
00:14:06,120 --> 00:14:09,120
lands full of Buddha's called Buddha lands or whatever

166
00:14:09,120 --> 00:14:12,320
there's the pure land

167
00:14:12,320 --> 00:14:16,240
that's brought in this but there's the idea that

168
00:14:16,240 --> 00:14:20,960
Buddha's were constantly

169
00:14:20,960 --> 00:14:25,760
working to enlighten beings for eternity

170
00:14:25,760 --> 00:14:29,280
they never really leave samsara it's a much more complicated system

171
00:14:29,280 --> 00:14:32,560
that they have

172
00:14:32,560 --> 00:14:37,920
and so no wonder that people don't really get a sense of what the Buddha taught

173
00:14:37,920 --> 00:14:43,360
because according to who I am

174
00:14:43,360 --> 00:14:47,280
so I submit to you all

175
00:14:47,280 --> 00:14:51,680
with the Buddha taught us through things morality concentration

176
00:14:51,680 --> 00:14:59,440
and wisdom and when you develop concentration

177
00:14:59,440 --> 00:15:03,600
it leads to when you develop morality the concentration that comes from

178
00:15:03,600 --> 00:15:08,240
morality as a great fruit

179
00:15:08,240 --> 00:15:11,600
when you have strong concentration that leads to wisdom and

180
00:15:11,600 --> 00:15:14,000
you have wisdom

181
00:15:14,000 --> 00:15:17,360
it leads to as we hit it down we move to

182
00:15:17,360 --> 00:15:24,000
the the mind becomes free from

183
00:15:24,000 --> 00:15:34,000
as of the form of confidence

184
00:15:34,000 --> 00:15:56,280
So, see everyone's active on second life, I don't know I haven't been making videos,

185
00:15:56,280 --> 00:16:01,720
I'll try to make some this week, we'll see, I've just been a little bit swamped,

186
00:16:01,720 --> 00:16:14,760
I don't know things, the ten talk, I was supposed to do a ten talk, and as I said I

187
00:16:14,760 --> 00:16:25,880
kind of bombed the trial run, but they invited me to another interview, so there were

188
00:16:25,880 --> 00:16:33,480
16 semi-finalists, and none we had the semi-final finalists, eight of us, so they cut

189
00:16:33,480 --> 00:16:38,240
it down to eight and I was still in it, so I wrote them back and said I really don't

190
00:16:38,240 --> 00:16:47,080
think I can give it my all, I realized in the interview, I just don't have the, I don't

191
00:16:47,080 --> 00:16:54,440
have the heart to put my heart into it, there's something, there's need to give a talk

192
00:16:54,440 --> 00:17:02,280
about, talk to so many people and such a prestigious organization, but they don't want

193
00:17:02,280 --> 00:17:11,480
people like me, I don't think so anyway, I mean if they want me to just get up and teach

194
00:17:11,480 --> 00:17:32,040
me the taste and that's something I'd be passionate about, but I do have to find ways

195
00:17:32,040 --> 00:17:55,360
to teach, it's a little bit cumbersome, I'm using the internet to teach, I mean not just

196
00:17:55,360 --> 00:18:06,800
to teach, but to build a community, I shouldn't drink, shouldn't minimize the importance

197
00:18:06,800 --> 00:18:16,560
of a local community, hopefully that'll build up as well, and people to keep this face

198
00:18:16,560 --> 00:18:31,640
going, to keep me alive, to keep putting out the trash, cleaning the bathrooms, making

199
00:18:31,640 --> 00:18:40,640
sure meditators have food, but like on the internet the problem is building a community

200
00:18:40,640 --> 00:18:46,440
right, so we've, I think this side has done quite a bit, it's likely that a whole list

201
00:18:46,440 --> 00:18:59,520
of people on this evening, but there's always more that can be done in one end, so it's

202
00:18:59,520 --> 00:19:14,840
not, it's not a perfect community, this is nice, I'm trying to just get a sense of the

203
00:19:14,840 --> 00:19:23,520
community, because it's nice coming on at nine and giving a little bit of a chance to

204
00:19:23,520 --> 00:19:39,440
give a little bit of dumber, and we have a community sway, and we can practice together,

205
00:19:39,440 --> 00:20:00,800
it's been a long day, we had a meeting this afternoon, really interesting meeting, interesting

206
00:20:00,800 --> 00:20:22,800
group of people working on this event, but yeah, I guess that's all for this evening,

207
00:20:22,800 --> 00:20:37,800
and thank you all for tuning in, have a good night.

