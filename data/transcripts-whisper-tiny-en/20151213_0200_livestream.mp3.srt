1
00:00:00,000 --> 00:00:12,560
Good evening, everyone broadcasting live December 12, 2015.

2
00:00:12,560 --> 00:00:19,760
Maybe a short broadcast tonight.

3
00:00:19,760 --> 00:00:24,040
I'm realizing that I have quite a bit of studying left to do for the next two exams.

4
00:00:24,040 --> 00:00:28,840
I haven't taken these classes, probably as seriously as I should have.

5
00:00:28,840 --> 00:00:38,200
I had to do a lot of reading, probably not going to do that while I'm either of these

6
00:00:38,200 --> 00:00:39,200
exams.

7
00:00:39,200 --> 00:00:42,760
What subject's birthday?

8
00:00:42,760 --> 00:00:50,000
Linguistics and philosophy.

9
00:00:50,000 --> 00:00:51,960
Monday we're having a study session.

10
00:00:51,960 --> 00:00:54,600
Two of my classmates are coming over.

11
00:00:54,600 --> 00:00:59,600
I'm going to sit down for the linguistics exam.

12
00:00:59,600 --> 00:01:02,520
Not really worried about it because that's the first year class, I don't think it really

13
00:01:02,520 --> 00:01:14,120
matters either way, as long as I pass it, but I was talking with another student about

14
00:01:14,120 --> 00:01:15,120
this.

15
00:01:15,120 --> 00:01:19,960
For the light and the exam, I said it kind of, or I was talking to someone about how it feels

16
00:01:19,960 --> 00:01:27,720
kind of wrong to not do your best at something, you know?

17
00:01:27,720 --> 00:01:31,640
Yeah, it's straight.

18
00:01:31,640 --> 00:01:40,400
They're still an intention to get 100% in everything, and if I don't get 100% well,

19
00:01:40,400 --> 00:01:50,560
that lesson has to be learned to try to get 100% next time, which is a difficult position

20
00:01:50,560 --> 00:01:52,600
to be in.

21
00:01:52,600 --> 00:01:57,600
We'll see if I get 100% in that, wouldn't that be funny?

22
00:01:57,600 --> 00:02:05,040
Got 100% going into the exam, probably got a 95 list.

23
00:02:05,040 --> 00:02:07,520
So Latin was last week's?

24
00:02:07,520 --> 00:02:09,480
Yeah.

25
00:02:09,480 --> 00:02:10,480
There was a bonus question.

26
00:02:10,480 --> 00:02:14,680
I asked if there was going to be a bonus section because he puts bonus on all the quizzes

27
00:02:14,680 --> 00:02:17,120
and they're actually quite generous.

28
00:02:17,120 --> 00:02:24,200
So I actually, on my quizzes, each of my quizzes, I got over 100% because of the bonus.

29
00:02:24,200 --> 00:02:28,720
I think average was like 103%.

30
00:02:28,720 --> 00:02:34,720
And then, so there was a bonus on the exam, but the bonus was actually quite cheap.

31
00:02:34,720 --> 00:02:39,120
Out of 200 marks, it was a three mark bonus question, but why I bring it up?

32
00:02:39,120 --> 00:02:49,200
And this is actually, it seems, it is related, well, it was an interesting quote, interesting

33
00:02:49,200 --> 00:02:57,600
probably to many of us, because he brought up this quote that had just come up recently

34
00:02:57,600 --> 00:03:06,280
in the media, a Latin quote, and in regards to one of the biggest stories of the past

35
00:03:06,280 --> 00:03:07,280
month.

36
00:03:07,280 --> 00:03:17,440
If anyone knows what that quote was, because it's actually quite a famous quote, I hadn't

37
00:03:17,440 --> 00:03:22,880
not being a news person, I hadn't seen it, but he brought up a picture and it's now

38
00:03:22,880 --> 00:03:29,040
on the wiki page, wiki, PDF page for that quote.

39
00:03:29,040 --> 00:03:41,680
The quote is, fluctuate, neck, mortgage tour, doesn't ring any bells in it, unless you're

40
00:03:41,680 --> 00:03:46,920
really keeping up with a news or unless you happen to be from France, because it's a very

41
00:03:46,920 --> 00:03:56,760
famous quote in France, fluctuate means, tossed about the fluctuate, right, fluctuate means

42
00:03:56,760 --> 00:04:11,320
that to be shaken, disturbed, it's using the imagery of a boat, fluctuate means shaken

43
00:04:11,320 --> 00:04:26,440
about, tossed about, by the way, neck means and not, and mergy tour, mergy tour means, mergy

44
00:04:26,440 --> 00:04:36,200
tour, mergy tour, mergy is not merged, it's from sub-merge, so mergy tour means sunk, so

45
00:04:36,200 --> 00:04:48,320
a boat, a successful ship is one that is tossed about, but not sub-merge, not sunk, and

46
00:04:48,320 --> 00:04:53,040
I just barely remembered him bringing up and forgot totally what it was about, but I kind

47
00:04:53,040 --> 00:05:01,240
of guessed, and I actually think I got it right, but tossed about and not submerged, I

48
00:05:01,240 --> 00:05:07,720
think it's more like tossed about and not sunk, but basically that.

49
00:05:07,720 --> 00:05:16,200
Anyway, it was used, they used it for many occasions in France to describe, to talk

50
00:05:16,200 --> 00:05:21,760
about how they are resilient, it's a quote about resiliency, but they also used it for the

51
00:05:21,760 --> 00:05:28,920
Paris bombings, to show that they weren't going to be, they weren't going to be defeated,

52
00:05:28,920 --> 00:05:34,880
they weren't going to let this defeat them, it's a quote about resiliency, but I think

53
00:05:34,880 --> 00:05:42,720
it's a very good quote from Buddhism, from Buddhist practice, and it's a very good sort

54
00:05:42,720 --> 00:05:52,760
of Buddhist response to things like terrorist attacks, and it's a difficult use in life,

55
00:05:52,760 --> 00:06:02,760
and yes, never sunk.

56
00:06:02,760 --> 00:06:08,200
There's always another chance, you always get another chance.

57
00:06:08,200 --> 00:06:14,520
We have the Buddha, as our example of that, the Buddha who took four uncountable eons and

58
00:06:14,520 --> 00:06:28,280
one hundred thousand countable eons, great eons, to become a Buddha, all the time it took

59
00:06:28,280 --> 00:06:39,720
him, all the mistakes he had to make to get there, makes you think that our one little

60
00:06:39,720 --> 00:06:54,280
life is not really worth all the concern we give it, like there's that Zen book that

61
00:06:54,280 --> 00:07:13,000
my mother got and showed to me, don't sweat the small stuff, and it's all small stuff.

62
00:07:13,000 --> 00:07:14,000
No questions tonight?

63
00:07:14,000 --> 00:07:25,360
I don't think so, yeah, they are a quote about an acrobat, right?

64
00:07:25,360 --> 00:07:31,880
Yes, I like that quote, just protect, but protect is a poor translation, it is a literal

65
00:07:31,880 --> 00:07:38,440
translation, but it means guard, guard would probably be better, watch out for, you know,

66
00:07:38,440 --> 00:08:05,200
a cup, but rockhead and guard is better, I will, he does use guard, he watched and guarded

67
00:08:05,200 --> 00:08:14,120
by himself in the first part, but then in the second part he switched, no master, with any

68
00:08:14,120 --> 00:08:43,960
switches to protect, which is weird, guard, so how do we guard ourselves, and we

69
00:08:43,960 --> 00:08:48,800
practice the four foundations of mindfulness, and by protecting ourselves we also protect

70
00:08:48,800 --> 00:08:53,880
others, this is another interesting part of this, not only is it about everyone doing

71
00:08:53,880 --> 00:09:03,440
their own duty, you know, and being good people, but it addresses the idea that when you

72
00:09:03,440 --> 00:09:14,640
do that you also help others, why, because of the leading to patience, forbearance,

73
00:09:14,640 --> 00:09:33,720
armlessness, love and compassion, by practicing mindfulness, by practicing, practicing for

74
00:09:33,720 --> 00:09:46,000
one's own betterment, one protects others, which is in contrast to those people who think

75
00:09:46,000 --> 00:09:53,120
the way to be a good person, the way to do the right thing is to help others, it's not

76
00:09:53,120 --> 00:10:03,200
really, because a person who helps themselves is naturally inclined to help others, it's

77
00:10:03,200 --> 00:10:10,560
not what should be the focus, right, the focus is unhelping others, you neglect the source,

78
00:10:10,560 --> 00:10:17,920
if your focus is unhelping yourself then you radiate goodness too, in all things that you do.

79
00:10:17,920 --> 00:10:42,680
If you don't have any questions we can just call it a night, say hello, thank you all

80
00:10:42,680 --> 00:11:02,400
for meditating with us, tomorrow we have meetings and Misudimaga, Miskip, Miskip some of the

81
00:11:02,400 --> 00:11:11,520
meetings, maybe I'll do my studying what you guys are having the meeting and all this

82
00:11:11,520 --> 00:11:21,400
and I can study and be there at the same time I think, yeah we can just kind of call

83
00:11:21,400 --> 00:11:37,200
out to you if we need your input on something, sure, that sounds good, okay then, good

84
00:11:37,200 --> 00:11:44,200
night everyone, thank you Robin for joining me, thank you Dante, good night, good night

