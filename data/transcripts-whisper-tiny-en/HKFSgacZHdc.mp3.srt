1
00:00:00,000 --> 00:00:08,080
Okay, when I meditate I always want to investigate my thought why I'm feeling that what is the reason of my fear

2
00:00:08,280 --> 00:00:11,520
What is the cause of this? Is it a good thing to do?

3
00:00:11,520 --> 00:00:37,520
You have to understand what the purpose of meditation is, that's finding out the cause of things is not the true purpose of meditation, it's interesting

4
00:00:37,520 --> 00:00:42,800
But you have to see the flaw in what you're saying

5
00:00:47,680 --> 00:00:56,560
The reality is like a river everything goes in order, right? But on the other hand, it's quite complex. So

6
00:00:57,680 --> 00:01:00,800
The say X is the cause of Y is a

7
00:01:00,800 --> 00:01:08,880
It is a simplistic understanding of how things work. If you sit there and what you're talking about is saying, okay

8
00:01:08,880 --> 00:01:15,040
I'm angry this morning, and the cause is that I

9
00:01:17,560 --> 00:01:22,200
Was unmindful, and I allowed my

10
00:01:22,200 --> 00:01:35,720
To the anger to the processing of an experience to lead to anger. That's that's one part of the story

11
00:01:35,720 --> 00:01:42,960
The question is why did why were you unmindful in the first place? And what happened in between the time when you were unmindful in the time when you got angry

12
00:01:42,960 --> 00:01:48,600
There's many things going on pinpointing one specific thing as saying this is a cause

13
00:01:48,600 --> 00:01:56,080
Intellectually and it has some value then because you say oh well, then I should be more mindful

14
00:01:56,920 --> 00:02:05,200
And that sort of thing does happen in meditation, but that's not meditation. It doesn't solve the problem

15
00:02:08,040 --> 00:02:10,040
In fact meditation does

16
00:02:10,960 --> 00:02:12,960
begin by

17
00:02:12,960 --> 00:02:20,640
Identifying momentary causes. So from one moment to the next seeing what what mind precedes what

18
00:02:21,840 --> 00:02:27,000
What what cause precedes what result? But it's a very it's still a very limited

19
00:02:27,600 --> 00:02:29,440
a very preliminary

20
00:02:29,440 --> 00:02:35,800
Basic understanding that understanding is also not enough. It's not the true reason for practicing

21
00:02:35,800 --> 00:02:40,240
It's not the highest insight highest insight is to see that things arise

22
00:02:40,240 --> 00:02:42,600
Based on a cause to see things

23
00:02:43,720 --> 00:02:45,640
Are dependently risen

24
00:02:45,640 --> 00:02:48,680
That's it to not what was the cause?

25
00:02:49,080 --> 00:02:53,360
But to see the things arising and ceasing it comes and then it goes

26
00:02:53,800 --> 00:02:58,000
So that's to see impermanence. It's also to see a doo-kau. It means

27
00:02:59,000 --> 00:03:02,320
inability to satisfy because it's impermanent and

28
00:03:02,920 --> 00:03:09,080
Nonself to see that there's no essence to it. It's about the dumb myself about the experience itself

29
00:03:09,080 --> 00:03:11,080
That's what we're most interested in

30
00:03:12,320 --> 00:03:14,320
We are

31
00:03:15,320 --> 00:03:20,560
Because the cause of suffering is simply attachment to things

32
00:03:20,760 --> 00:03:22,920
So the cause and effect doesn't

33
00:03:23,720 --> 00:03:28,560
Directly is indirectly can be used so but directly does not tie into that to

34
00:03:29,800 --> 00:03:33,000
Craving it's simply caused by seeing that things are

35
00:03:34,120 --> 00:03:35,200
stable

36
00:03:35,200 --> 00:03:42,760
Satisfying and controllable once you see that things are not thus then you let go of them and when you let go of them

37
00:03:42,760 --> 00:03:44,120
You become free

38
00:03:44,120 --> 00:03:46,120
When you're free you say I'm free

39
00:03:46,560 --> 00:03:48,560
Nothing more needs to be done

40
00:03:48,560 --> 00:03:50,720
So is it useful? Yes

41
00:03:51,040 --> 00:03:58,000
Arguably that has some intellectual benefit because it's going to say well, then I shouldn't do that or I should adjust myself

42
00:03:58,000 --> 00:04:01,080
It allows you to be a sort of a meta analysis

43
00:04:01,080 --> 00:04:08,480
M-E-T-A that allows you to adjust your practice. That's what we would call we monks and it allows you to succeed

44
00:04:09,000 --> 00:04:15,120
So there's benefit to it. Don't think that it's a replacement to meditation. It's not it's it's it's a

45
00:04:16,200 --> 00:04:19,360
Non-meditative process at that moment. You're actually

46
00:04:20,040 --> 00:04:25,160
Not meditating because you're not observing things simply as they are you're reflecting on them

47
00:04:25,160 --> 00:04:28,520
You're intellectualizing about them you're reflecting

48
00:04:28,520 --> 00:04:32,760
Processing your memories of things which in fact can be

49
00:04:33,240 --> 00:04:36,360
Flawed, but that's not the biggest problem the biggest problem is

50
00:04:36,840 --> 00:04:44,240
It's memories. It's okay. This happened like this happened like this which is on the past and is not there is not

51
00:04:46,320 --> 00:04:49,840
Cannot be a replacement for true insight. So

52
00:04:49,840 --> 00:04:59,840
Not wrong, not bad, but not insignificant

