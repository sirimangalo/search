WEBVTT

00:00.000 --> 00:02.800
Hi, and welcome back to Ask a Monk.

00:02.800 --> 00:08.900
Today, we have a question from German 1184.

00:08.900 --> 00:12.800
Yutadama, how much emphasis does your tradition put on book learning?

00:12.800 --> 00:18.900
In Tibet, the practice seems to have much memorization of root texts and commentaries

00:18.900 --> 00:20.900
and the debating upon them.

00:20.900 --> 00:23.100
Some people approach the practice in this way.

00:23.100 --> 00:27.500
What do you think?

00:27.500 --> 00:37.000
Well, I think that no one would disagree that book learning has its place.

00:37.000 --> 00:45.600
I've often complained that even in the tradition that I follow, in many of the traditions

00:45.600 --> 00:53.300
of Buddhism, there is too much of an emphasis on textual study as an end more than

00:53.300 --> 00:59.300
a means.

00:59.300 --> 01:08.100
Ideally, we see textual study as similar to studying a roadmap, learning about the

01:08.100 --> 01:18.000
paths, about the way we're going to go, or a modern example would be learning how to drive

01:18.000 --> 01:19.000
a car.

01:19.000 --> 01:24.300
So, you can study a roadmap as much as you want, you can learn how to drive, you can learn

01:24.300 --> 01:29.700
everything, all the tricks of the road, everything there is to know about traveling,

01:29.700 --> 01:34.700
but if you never take the journey, you'll never get anywhere.

01:34.700 --> 01:38.700
I think this has to be said about textual study.

01:38.700 --> 01:50.200
The other is some argument to be made for the intellectual appreciation of the teachings,

01:50.200 --> 02:01.200
the understanding of the teaching intellectually, and the ability to use the teachings directly

02:01.200 --> 02:08.500
as you're studying to examine yourself, and there is, to a certain level, this is possible.

02:08.500 --> 02:14.200
It's possible that as you're reading a teaching or as you're listening to a talk that

02:14.200 --> 02:20.600
you're actually also reflecting on the teachings and using them practically.

02:20.600 --> 02:27.000
But as I said, I think there are many traditions and not exactly traditions.

02:27.000 --> 02:33.000
I would say monasteries, teachers, being more specific than talking about any specific

02:33.000 --> 02:40.400
type of Buddhism, but there are many Buddhists who like to get together in a group and debate

02:40.400 --> 02:53.000
or study or discuss a teaching, and think of that as a fundamental part of their practice.

02:53.000 --> 03:02.000
It's useful to an extent, but if you find a group or a teacher who is only going so

03:02.000 --> 03:06.800
far as to give textual study, then you have to say that there is something missing

03:06.800 --> 03:09.600
from the teaching.

03:09.600 --> 03:16.200
In the end, studying, as an end, simply makes you better at studying.

03:16.200 --> 03:23.100
The more you study, the better you are memorizing, the better you are, and logic, being

03:23.100 --> 03:31.200
able to assimilate teachings and make sense of words and syntax and so on.

03:31.200 --> 03:35.400
You assimilate it into your brain and get the concepts, and it can make you a good teacher,

03:35.400 --> 03:43.800
it can make you very confident of yourself because of your learning.

03:43.800 --> 03:48.400
If you're good at debate, when you practice debate, it just makes you better at debate,

03:48.400 --> 03:55.400
it makes you more skilled at argument, at winning people over, at explaining things to people,

03:55.400 --> 04:00.200
at being able to reply to criticisms and so on.

04:00.200 --> 04:07.900
But until you practice meditation, the only way you're going to get better at understanding

04:07.900 --> 04:09.800
reality is to look at reality.

04:09.800 --> 04:16.200
Neither of these things study, debate, or any of the other associated activities helps

04:16.200 --> 04:18.400
you to understand reality.

04:18.400 --> 04:23.000
The only way you can get better at seeing reality for what it is, is to look at it, just

04:23.000 --> 04:24.600
to train yourself.

04:24.600 --> 04:29.200
So the meditation is a very specific action where we're training ourselves to see things

04:29.200 --> 04:30.400
as they are.

04:30.400 --> 04:31.400
It's a training.

04:31.400 --> 04:38.480
It's not intellectual, it's not a realization that comes to you.

04:38.480 --> 04:42.600
It's a change in the way you look at things, so instead of seeing things as me as mine

04:42.600 --> 04:50.200
is good as bad, you actually see them, and you're actually able to experience something

04:50.200 --> 04:55.000
simply as it is, when you see something, you only see it, when you hear something, you

04:55.000 --> 05:00.600
only hear it, when you feel pain, there's only the pain, when you feel happy, there's

05:00.600 --> 05:03.200
only the happiness and so on.

05:03.200 --> 05:08.000
And so it's a skill rather than, it's nothing that can come from someone else, it's nothing

05:08.000 --> 05:09.700
someone else can teach you.

05:09.700 --> 05:13.800
They can only teach you the way of developing the skill and it's up to you to develop

05:13.800 --> 05:17.600
this skill until you're able to do it for yourself.

05:17.600 --> 05:26.600
Once you're able to see reality for what it is, only then can you say that you'll actually

05:26.600 --> 05:32.800
be free from suffering, there's only one way to realize the truth of the Buddha's teaching

05:32.800 --> 05:35.500
and that's through the practice.

05:35.500 --> 05:37.400
So I hope that makes sense.

05:37.400 --> 05:42.200
I think it's generally understood that this is the case, that there are three parts to

05:42.200 --> 05:43.200
the Buddha's teaching.

05:43.200 --> 05:48.200
There's study and you study for the purpose of practicing and you practice for the purpose

05:48.200 --> 05:49.200
of realization.

05:49.200 --> 05:54.800
So there's the study, the practice and the realization.

05:54.800 --> 06:04.100
So I would say there's no place for the accumulation of book knowledge, knowledge is for

06:04.100 --> 06:05.100
the purpose of practice.

06:05.100 --> 06:11.080
If it's not practical, if it's not something that you intend to use either or to help

06:11.080 --> 06:17.080
someone else, then it really isn't the Buddhist learning.

06:17.080 --> 06:22.080
Buddhist learning is for the purpose of putting it into practice, for the purpose of

06:22.080 --> 06:23.780
realizing the truth.

06:23.780 --> 06:27.960
If it's not something that leads you to realize the truth, then it's also not considered

06:27.960 --> 06:30.880
to be proper learning in a Buddhist sense.

06:30.880 --> 06:31.880
Okay, so I hope that helps.

06:31.880 --> 06:48.680
Thanks for the question.

