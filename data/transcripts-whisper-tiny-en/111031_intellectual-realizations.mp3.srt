1
00:00:00,000 --> 00:00:18,000
Okay, so do the realizations of impermanence, suffering, and non-self have to be, or are they supposed to be intellectual?

2
00:00:18,000 --> 00:00:41,000
No, and in fact, in a sense, the experience of impermanence, suffering, and non-self is the absence, because they're all negative aspects or negative characteristics.

3
00:00:41,000 --> 00:00:59,000
It's not like non-self comes out and hits you in the face, but what it is is it's the giving up, the belief that something is permanent.

4
00:00:59,000 --> 00:01:11,000
It may come in your meditation that you intellectualize, or not exactly intellectualize, but the thought arises. Wow, this is all impermanent.

5
00:01:11,000 --> 00:01:35,000
There's this thought arises. I don't like a realization, but that's not the insight. That's not the moment of insight. It often follows. It's preceded by a moment of insight.

6
00:01:35,000 --> 00:01:45,000
The moment of insight is the seeing for yourself, for instance, in the case of impermanence, it's seeing something cease.

7
00:01:45,000 --> 00:02:09,000
It's the continued realization, the continued observation of the cessation of phenomena. For suffering, it's seeing the intolerability of the phenomenon, basically in terms of seeing that the things you thought that were going to make you happy are not going to make you happy.

8
00:02:09,000 --> 00:02:18,000
When you see things as they are, you give up the belief in permanence in satisfaction, or that things can be pleasant.

9
00:02:18,000 --> 00:02:43,000
When you cling to something that it can bring you happiness.

10
00:02:43,000 --> 00:02:54,000
In the beginning, you're just going to keep controlling the stomach no matter what you do.

11
00:02:54,000 --> 00:03:04,000
You'll be practicing rising, falling, and here you are forcing the breath at every in breath, every out breath, forcing the stomach.

12
00:03:04,000 --> 00:03:20,000
You start to see that it's not really forcing the breath at all. There's this conception of the forcing, but eventually what you'll see is that all you're doing is creating stress.

13
00:03:20,000 --> 00:03:44,000
You're not actually controlling the breath, you're just creating more stress. The breath is arising based on many conditions, only one of which is the mind, but the mind is responsible for creating stress, which is why when you try to control it, you just feel more and more terrible, more and more unpleasant.

14
00:03:44,000 --> 00:04:05,000
Until finally you realize that it's not really, you can't just say, because think about it, this is what we don't realize, if you could make it, if it was under your control, then why can't you just say, okay, my breath is just going to rise smoothly, it's going to fall smoothly.

15
00:04:05,000 --> 00:04:24,000
That's what we think we can do, but totally off the wall, this idea, this idea that we can do that, because it's totally unrelated to the facts, and eventually you realize this.

16
00:04:24,000 --> 00:04:36,000
It's not intellectualizing, like I just did, I just had you intellectualize and say, as the Buddha did well, if you could, then why can't you control it to be like this or like that, it's not like that at all.

17
00:04:36,000 --> 00:04:53,000
The realization is, is visceral, it's actually seeing me breath a rise and see sun its own, and the clear perception of non-self is when you actually see like that, it actually appears to you that the body is moving on its own.

18
00:04:53,000 --> 00:05:05,000
It appears to you that thoughts are arising on the road, you're able to see this, and you're able to realize that really that's all there is, is phenomena arising and ceasing, and you give up any desire to control.

19
00:05:05,000 --> 00:05:15,000
The whole idea of control just goes out the window, and that's when you do the realization of non-self.

20
00:05:15,000 --> 00:05:43,000
So the mind lets go, and the mind lets go, then there's freedom from suffering, that's how it should be.

