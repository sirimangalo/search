1
00:00:00,000 --> 00:00:21,600
Okay welcome everyone, tonight we'll have an English session, first of all, a short reminder

2
00:00:21,600 --> 00:00:35,280
about the Buddhist teaching and then we'll move into the meditation practice.

3
00:00:35,280 --> 00:00:46,600
One of the easiest ways to adjust our practice is to think about the results that we're hoping

4
00:00:46,600 --> 00:00:56,960
to attain from the practice, think about why we're practicing, think about what are the

5
00:00:56,960 --> 00:01:12,040
benefits and the goals, the results that we hope to achieve from the practice.

6
00:01:12,040 --> 00:01:21,200
Because when we look at these, look at what's important about the practice, it helps

7
00:01:21,200 --> 00:01:28,040
us to shape our practice, it makes it clear what we have to do.

8
00:01:28,040 --> 00:01:36,240
If we want to understand ourselves, then we have to, we know what we have to do.

9
00:01:36,240 --> 00:01:41,440
We know what it's going to take to come to understand ourselves.

10
00:01:41,440 --> 00:01:48,200
We have to look at ourselves, we have to look at who we are, what we are.

11
00:01:48,200 --> 00:01:51,480
We can't let our mind wander.

12
00:01:51,480 --> 00:01:58,880
We know what it means to be meditating and not meditating, likewise if we want to feel peaceful

13
00:01:58,880 --> 00:02:04,280
and calm, if we want to quiet our minds, then we can't wander off and think about other

14
00:02:04,280 --> 00:02:14,720
things.

15
00:02:14,720 --> 00:02:20,520
When we want to straighten our minds, we want to be free from addiction, aversion, greed,

16
00:02:20,520 --> 00:02:21,520
anger, delusion.

17
00:02:21,520 --> 00:02:30,120
We want to rid ourselves of all the negative emotions that we have in our minds.

18
00:02:30,120 --> 00:02:40,400
Just worry, fear, depression, and so on.

19
00:02:40,400 --> 00:02:49,560
Then we know what we have to do, it makes it clear the quality of our practice.

20
00:02:49,560 --> 00:02:56,840
So often it's sufficient to just talk about what we will hope to gain from the practice.

21
00:02:56,840 --> 00:03:02,520
And that gives us an idea of where we should be in our practice.

22
00:03:02,520 --> 00:03:05,760
So what are the things that we get from the practice, the things that we should hope for

23
00:03:05,760 --> 00:03:08,600
from it in the practice?

24
00:03:08,600 --> 00:03:16,960
Well, first of all, we can split the practice up into its two component parts, the walking

25
00:03:16,960 --> 00:03:21,080
meditation and the sitting meditation.

26
00:03:21,080 --> 00:03:29,600
As though they have a lot in common, they obviously have some things not in common.

27
00:03:29,600 --> 00:03:34,400
They differ in certain respects.

28
00:03:34,400 --> 00:03:38,920
Walking meditation is more active.

29
00:03:38,920 --> 00:03:41,440
On the one hand, easier and on the other hand, more difficult.

30
00:03:41,440 --> 00:03:43,640
It's easier to do.

31
00:03:43,640 --> 00:03:55,040
It feels easier, it feels less intense, but on the other hand, it's much more difficult

32
00:03:55,040 --> 00:04:03,200
to keep your mind still because ordinarily, when we're active, we're used to thinking,

33
00:04:03,200 --> 00:04:06,880
we're used to letting the mind be active as well.

34
00:04:06,880 --> 00:04:13,080
It's when we sit still or when we lie down, when we quiet the body that we're used

35
00:04:13,080 --> 00:04:22,760
to quieting the mind, the walking meditation is a very important example of how to quiet

36
00:04:22,760 --> 00:04:29,720
the mind, focus the mind well, we're active, to bring the meditation into our daily life

37
00:04:29,720 --> 00:04:38,480
so that our whole life becomes quiet and calm and clear, focused, that our mind is strong

38
00:04:38,480 --> 00:04:49,440
and present at all times.

39
00:04:49,440 --> 00:04:57,560
The walking has several benefits for the body that it's worth noting about this.

40
00:04:57,560 --> 00:05:02,680
These are not the main reason why we practice meditation.

41
00:05:02,680 --> 00:05:08,680
The reason why we split up into walking and sitting partially is because it supports good

42
00:05:08,680 --> 00:05:10,480
bodily health.

43
00:05:10,480 --> 00:05:20,640
When we're doing long meditation retreats, walking meditation serves as an intermission or a

44
00:05:20,640 --> 00:05:31,080
break from the sitting meditation where we're able to give the body a chance to stretch.

45
00:05:31,080 --> 00:05:42,200
And as a result, this has benefits like digesting our food better, helping us to overcome

46
00:05:42,200 --> 00:05:50,200
sicknesses, and helping us to be free from sickness and disease, supporting immune systems

47
00:05:50,200 --> 00:05:51,200
and so on.

48
00:05:51,200 --> 00:05:58,280
The other thing walking meditation does is build patience because it's very repetitive, it's

49
00:05:58,280 --> 00:06:13,240
a repetitive action that can become very boring if we use to excitement if we use to

50
00:06:13,240 --> 00:06:23,720
use to activity or very activity we use to following our hearts desire and so on.

51
00:06:23,720 --> 00:06:30,240
Going back and forth since it's over repetitive, it creates great patience and it's much

52
00:06:30,240 --> 00:06:37,160
like any repetitive work so it gives us this ability to work more efficiently in the

53
00:06:37,160 --> 00:06:48,040
tasks that we have to perform that are repetitive, that are onerous, that are even unpleasant,

54
00:06:48,040 --> 00:06:55,680
they can lose their unpleasantness.

55
00:06:55,680 --> 00:07:00,520
And of course walking meditation helps us to walk, helps us to become strong in our

56
00:07:00,520 --> 00:07:08,080
walkings because of the movements of the body and the endurance and the patience that

57
00:07:08,080 --> 00:07:14,640
it brings, it teaches us to be in tune with our body when we walk and allows us to walk

58
00:07:14,640 --> 00:07:19,520
more efficiently which can be useful if we intend to walk, if we're a kind of person who

59
00:07:19,520 --> 00:07:28,840
would walk long distances, of course for monks this is an important skill.

60
00:07:28,840 --> 00:07:33,120
So being in tune with our body is an important part here, but the biggest benefit of walking

61
00:07:33,120 --> 00:07:40,320
meditation that's most important for our purposes is that it acts as a segue between

62
00:07:40,320 --> 00:07:45,200
non-meditative state and the sitting meditation.

63
00:07:45,200 --> 00:07:57,880
Because when we start to meditate our minds are generally unfocused and we have to force

64
00:07:57,880 --> 00:08:04,960
them to sit still, just as we have to optimize our bodies to sit still, we also have to

65
00:08:04,960 --> 00:08:11,120
force the mind to sit still because of the intensity of the sitting meditation, we have

66
00:08:11,120 --> 00:08:18,360
to sit still, close our eyes and we can't get up, we can scratch our nose, we can't

67
00:08:18,360 --> 00:08:24,800
shift our about, it's quite difficult because our minds are, when we first start meditating

68
00:08:24,800 --> 00:08:28,880
are still in the state of distraction.

69
00:08:28,880 --> 00:08:35,840
So walking meditation as I said it's easier in this regard in that we're able to move

70
00:08:35,840 --> 00:08:42,120
the body, it's the halfway, so we don't have the complete freedom that we would

71
00:08:42,120 --> 00:08:49,760
normal in a normal state but we're not yet in a meditative state where we have to keep

72
00:08:49,760 --> 00:08:55,440
the body in the mind fixed and focused.

73
00:08:55,440 --> 00:09:01,600
And so the benefit of the walking meditation Buddha said is that it lasts into the sitting

74
00:09:01,600 --> 00:09:07,360
meditation, the concentration that you gain as an effect for the sitting meditation.

75
00:09:07,360 --> 00:09:14,880
It's useful to practice beforehand because the concentration will then last, then continue

76
00:09:14,880 --> 00:09:19,560
on into the sitting.

77
00:09:19,560 --> 00:09:24,040
So these are the reasons why we do walking meditation, but the real benefits that we

78
00:09:24,040 --> 00:09:31,160
hope to gain from meditation which are generally associated with sitting meditation but

79
00:09:31,160 --> 00:09:34,880
can be equally found in the walking meditation.

80
00:09:34,880 --> 00:09:43,360
There are four reasons why we practice our benefits to the practice.

81
00:09:43,360 --> 00:09:47,480
So once we've done the walking meditation we're sitting down or even now when we haven't

82
00:09:47,480 --> 00:09:52,600
done the walking meditation and we're trying to watch our minds, watch our bodies, watch

83
00:09:52,600 --> 00:09:59,240
our thoughts and our emotions and the feelings that arise.

84
00:09:59,240 --> 00:10:04,800
What are the benefits that we can gain from doing this?

85
00:10:04,800 --> 00:10:11,080
The first one is that we gain happiness and peace in the here and now.

86
00:10:11,080 --> 00:10:18,200
Last night I went to teach at the Metro Detention Center in downtown Los Angeles teaching

87
00:10:18,200 --> 00:10:29,400
federal inmates and you sort of get a sense of their life.

88
00:10:29,400 --> 00:10:35,360
We were able to go in and see them in their living quarters and they have an area living

89
00:10:35,360 --> 00:10:43,280
area and then cells all around and when they came down to meditate after we finished

90
00:10:43,280 --> 00:10:51,720
asked them how was it and they just said what an incredible relief from the chaos of being

91
00:10:51,720 --> 00:10:57,000
in such cramped quarters with so many people having to live your life in such cramped

92
00:10:57,000 --> 00:11:06,960
quarters where your mind is constantly stressed and worried and unsure about the future

93
00:11:06,960 --> 00:11:23,360
and surrounded by people who are often very degrees of sanity and morality and so on.

94
00:11:23,360 --> 00:11:30,440
And so there was a group, a group, a group of women that came down to practice meditation

95
00:11:30,440 --> 00:11:41,160
at the end they were in total agreement, unanimous agreement that it was just such a relief

96
00:11:41,160 --> 00:11:55,000
such a good break from the daily stress and the difficulty of being in prison.

97
00:11:55,000 --> 00:12:02,000
I think that's a good example of this one.

98
00:12:02,000 --> 00:12:05,680
When we first sit down the first thing we gained from the practice is this peace and

99
00:12:05,680 --> 00:12:06,680
calm.

100
00:12:06,680 --> 00:12:14,120
So really we're able to put aside all of our cares and troubles for a short time.

101
00:12:14,120 --> 00:12:22,760
We're able to have this quiet time alone, it's a chance to sort out many things in the

102
00:12:22,760 --> 00:12:29,960
mind, all of the things that we're still worrying about, stressing about, frustrated

103
00:12:29,960 --> 00:12:36,720
about during the day, feeling guilty about all of our negative emotions, the things that

104
00:12:36,720 --> 00:12:38,760
we're still clinging to.

105
00:12:38,760 --> 00:12:43,680
We have a chance to sort them out to acknowledge them, see them for what they are and

106
00:12:43,680 --> 00:12:56,240
give them their spot light for their time and then let them go and so we're able to

107
00:12:56,240 --> 00:13:02,120
sort out many things and quiet down our mind in the practice.

108
00:13:02,120 --> 00:13:08,360
This is not the ultimate goal of the practice, but it's a good enough reason for most

109
00:13:08,360 --> 00:13:14,960
people. For most of us, meditation is just a chance to quiet the mind, this is why many

110
00:13:14,960 --> 00:13:20,560
people get into meditation because our minds are not quiet.

111
00:13:20,560 --> 00:13:28,000
The meditation, it really is, it's the first thing you gain from the meditation and

112
00:13:28,000 --> 00:13:34,120
it's also the last thing that you gain, but in the sense of being the first thing, it's

113
00:13:34,120 --> 00:13:39,840
not really the goal that we're looking for because it's something that's going to disappear

114
00:13:39,840 --> 00:13:41,680
once you go back out into the world.

115
00:13:41,680 --> 00:13:48,520
What we're looking for is this end piece, the piece that comes at the end once you understand,

116
00:13:48,520 --> 00:13:55,360
once you really get it and you learn how to deal with the difficulties of daily life

117
00:13:55,360 --> 00:14:01,400
and are able to take it out there into the world and carry the peace and the happiness

118
00:14:01,400 --> 00:14:07,720
and freedom from suffering with you.

119
00:14:07,720 --> 00:14:08,720
But this is the first one.

120
00:14:08,720 --> 00:14:17,600
This is what we are immediately able to see from the practice and it's a relief.

121
00:14:17,600 --> 00:14:25,680
The second benefit that we gain is knowledge and insight.

122
00:14:25,680 --> 00:14:31,080
We come to understand things about ourselves and about the world around us that we couldn't

123
00:14:31,080 --> 00:14:42,000
understand before, why do we suffer, why do we obsess over things and how do we do away

124
00:14:42,000 --> 00:14:49,720
with stress, how do we do away with suffering, how do we stop ourselves from getting angry,

125
00:14:49,720 --> 00:14:57,160
how do we break the cycle of addiction, how do we become free from guilt and worry?

126
00:14:57,160 --> 00:15:07,000
We learn how these things work, we come to understand how our mind works, and we come

127
00:15:07,000 --> 00:15:14,760
to see how our mind works in much more detail than before.

128
00:15:14,760 --> 00:15:21,560
When our minds don't work the way we'd like, in these just suffering for us, but we don't

129
00:15:21,560 --> 00:15:24,400
really understand what happened there, what did I do wrong?

130
00:15:24,400 --> 00:15:30,400
We don't really understand what the chain of events was.

131
00:15:30,400 --> 00:15:34,320
We know that maybe we got angry, but that's all we know about it.

132
00:15:34,320 --> 00:15:38,400
I got angry, I couldn't stop myself from getting angry.

133
00:15:38,400 --> 00:15:44,640
When we look closer, we see really what happened, there's the seeing something or hearing

134
00:15:44,640 --> 00:15:49,680
something, and then there's the thinking it over and then there's the getting angry and

135
00:15:49,680 --> 00:15:53,840
then there's the thinking it about it again and then the getting angry again and thinking

136
00:15:53,840 --> 00:15:58,760
and then starting to think about what you're going to do to get back at the person,

137
00:15:58,760 --> 00:16:03,920
thoughts of revenge, the more anger, the more thoughts of revenge and so on.

138
00:16:03,920 --> 00:16:10,480
And it actually snowballs, it's an entire cycle and chain of events that finally leads

139
00:16:10,480 --> 00:16:24,400
us to do things that we regret.

140
00:16:24,400 --> 00:16:32,280
How meditation accomplishes this is because we're looking at the objects as they arise.

141
00:16:32,280 --> 00:16:34,880
The reason why we can't see it normally is because we're not really looking.

142
00:16:34,880 --> 00:16:42,640
When we see, we right away start to process it, it's good, it's bad, it's me, it's mine,

143
00:16:42,640 --> 00:16:47,120
and we've lost sight of the object.

144
00:16:47,120 --> 00:16:51,520
We've gone into the next stage of figuring out what we're going to do to how we're going

145
00:16:51,520 --> 00:16:54,680
to react to it, very reactionary.

146
00:16:54,680 --> 00:17:02,680
When we meditate, the seeing is seeing, so we say to ourselves seeing, when you hear

147
00:17:02,680 --> 00:17:12,600
something hearing and you're watching it and you're watching it, you can see how the

148
00:17:12,600 --> 00:17:17,360
mind reacts to it, you can see what happens in the mind, seeing, seeing you get to see

149
00:17:17,360 --> 00:17:23,320
that first mind that starts to get angry about it or attract it to it, you shift your

150
00:17:23,320 --> 00:17:30,120
focus to the anger or the attraction, angry, angry, liking, liking.

151
00:17:30,120 --> 00:17:36,040
When you can see what happens next, you get to see the whole series of events, you get

152
00:17:36,040 --> 00:17:54,360
to really see how your mind works.

153
00:17:54,360 --> 00:17:59,520
You come to understand how to fix all of your problems, you come to understand how

154
00:17:59,520 --> 00:18:09,920
to, or why things go, occur the way they do, you're able to see clearly every situation

155
00:18:09,920 --> 00:18:15,880
and someone comes to you and they're upset and they don't know what to do, and you just

156
00:18:15,880 --> 00:18:21,360
mindfully listen and you watch your own emotions and your own reactions, and then you

157
00:18:21,360 --> 00:18:29,360
think about what they're saying, clearly aware of the situation, you're able to fix

158
00:18:29,360 --> 00:18:33,800
not only your own problems but other people's as well, you're able to see things clearly,

159
00:18:33,800 --> 00:18:39,800
knowledge and vision and so on, it's very important that you get to, you're able to see

160
00:18:39,800 --> 00:18:51,000
reality, you see things much clearer than before, the things don't upset you and don't

161
00:18:51,000 --> 00:18:57,000
attract the other way they used to, so you don't be afraid to addiction and hatred and

162
00:18:57,000 --> 00:18:59,440
so on.

163
00:18:59,440 --> 00:19:12,560
The third benefit is mindfulness itself, mindfulness and clear awareness.

164
00:19:12,560 --> 00:19:18,560
The great thing about my meditation is not only the knowledge that you gain from knowing

165
00:19:18,560 --> 00:19:23,040
what you're doing wrong, but it's also the ability to stop yourself from doing things

166
00:19:23,040 --> 00:19:28,920
wrong, what's great about the acknowledgement, this mantra that we use, it's not only

167
00:19:28,920 --> 00:19:32,080
that you learn about, oh I'm doing this wrong, I'm doing that wrong, but it actually

168
00:19:32,080 --> 00:19:43,080
changes, so when you say this is good or this is bad, you're already falling into greed

169
00:19:43,080 --> 00:19:49,160
and anger and you're setting yourself up for suffering, but when you rewind it back and

170
00:19:49,160 --> 00:19:58,800
say, loop it back and say, this is this, this is seeing, hearing, smelling, pain or

171
00:19:58,800 --> 00:20:11,400
waking or happiness or calm or so on, it is what it is and this cuts off our emotions,

172
00:20:11,400 --> 00:20:16,680
it cuts off the anger, it cuts off the greed and the addiction and you can feel, you feel

173
00:20:16,680 --> 00:20:22,480
clear when you do it, you really feel like you've totally simplified reality, you've

174
00:20:22,480 --> 00:20:36,760
simplified your whole experience of reality, now you see things for what they are.

175
00:20:36,760 --> 00:20:42,080
The fourth benefit is the final benefit, the one that we're really hoping for, really

176
00:20:42,080 --> 00:20:49,080
working towards and that's the cutting off, the defoundment, the cutting off of the negative

177
00:20:49,080 --> 00:20:55,920
mind sting, the things that are the cause of all of our suffering, the truth about suffering

178
00:20:55,920 --> 00:21:01,760
is it's not caused by external objects, no one else can hurt you, nobody can cause you

179
00:21:01,760 --> 00:21:08,680
suffering for you, when someone says something nasty to you, it's up to you to process

180
00:21:08,680 --> 00:21:15,200
that, it's up to you to say that's a bad thing, it's a mean and nasty and evil thing

181
00:21:15,200 --> 00:21:27,800
and then get angry and hurt yourself, feel sad about it, feel scared, feel stressed,

182
00:21:27,800 --> 00:21:40,240
feel worried, depressed, you lose your job, you lose your home, your family, your friends,

183
00:21:40,240 --> 00:21:46,160
these external things cannot make you suffer but we often think it's the proper reaction

184
00:21:46,160 --> 00:21:52,840
is to suffer when someone yells at me, I should get angry and then why should you get

185
00:21:52,840 --> 00:21:57,720
angry, so you can hurt yourself, so you can be in pain, so you can suffer.

186
00:21:57,720 --> 00:22:02,880
So you can make them suffer.

187
00:22:02,880 --> 00:22:09,600
When I lose my possession, my belonging, my job, my friend, my family, it's proper to

188
00:22:09,600 --> 00:22:16,920
feel depressed, it's proper to cry, to feel sad, you ask yourself, does it get you your

189
00:22:16,920 --> 00:22:22,840
job back, does it get you your friend back, does it get you your family back, your house,

190
00:22:22,840 --> 00:22:29,320
your belonging, whatever, you know, does it benefit the dead person if someone's passed

191
00:22:29,320 --> 00:22:43,560
away, no, does it benefit you, no, there's really no benefit to any of these emotions,

192
00:22:43,560 --> 00:22:52,960
it'll save, on a limited extent you can say things like crying, but useful, because it's

193
00:22:52,960 --> 00:22:58,880
a chemical, it creates a chemical reaction or whatever's something in the brain and makes

194
00:22:58,880 --> 00:23:07,920
you feel happy, oftentimes people cry for this reason, but in the end none of this is

195
00:23:07,920 --> 00:23:13,920
of any benefit, none of this can really make us happy or can really make us unhappy, it's

196
00:23:13,920 --> 00:23:21,760
up to us to, it's up to our own minds to interpret things and make ourselves happy and happy,

197
00:23:21,760 --> 00:23:28,480
when we become addicted to things, I think it's good to be attached to the things that

198
00:23:28,480 --> 00:23:36,760
you love, that you like, that you enjoy, only we find that for people who really love

199
00:23:36,760 --> 00:23:43,640
and really enjoy things, they become addicted, it's a cycle, you never really have enough

200
00:23:43,640 --> 00:23:51,360
and people who are more passionate or are much more subject to the anger and frustration

201
00:23:51,360 --> 00:23:57,360
when they don't get what they want, and it's only a matter of degree for all of that.

202
00:23:57,360 --> 00:24:00,920
So how meditation does away with these, because it helps us to understand, it changes

203
00:24:00,920 --> 00:24:07,280
the way we think of these objects of the world around us, changes the way we think about

204
00:24:07,280 --> 00:24:14,040
our own body and our own mind, when we see these things objectively, we see them for what

205
00:24:14,040 --> 00:24:26,800
they are, it causes a shift in our way of looking at things, we can no longer see addictive

206
00:24:26,800 --> 00:24:36,160
things as attractive, we can no longer see and experience unpleasant things as unpleasant.

207
00:24:36,160 --> 00:24:41,680
We can because we look at them differently, we look at them as they really are, when we hear

208
00:24:41,680 --> 00:24:47,320
something, we look at it as being a sound, when we think about something, we look at

209
00:24:47,320 --> 00:24:52,840
it as being a thought, we don't judge, we don't say this is good, this is bad, this is

210
00:24:52,840 --> 00:25:00,120
me, this is mine, it creates a paradigm shift, whereas before we look at things in terms

211
00:25:00,120 --> 00:25:06,120
of what benefit they can bring me in the shortest time possible, were they good or they

212
00:25:06,120 --> 00:25:12,760
bad filtering reality, compartmentalizing reality, this part of reality is acceptable,

213
00:25:12,760 --> 00:25:21,080
this part is unacceptable, tolerate, I can tolerate this, I can't tolerate this part,

214
00:25:21,080 --> 00:25:27,160
this comes, I have to keep it, I have to hold on to it and cling to it and make sure

215
00:25:27,160 --> 00:25:34,360
it doesn't go away, and if this part arises I have to chase it away, destroy it, and

216
00:25:34,360 --> 00:25:54,480
stop it, get rid of it, and so we don't really live, live our full and natural and harmonious

217
00:25:54,480 --> 00:26:03,920
life, meditation we change our way of looking at things, we are able to experience

218
00:26:03,920 --> 00:26:10,600
all the full spectrum of reality, we are in total harmony with the present moment, being

219
00:26:10,600 --> 00:26:18,480
able to accept this right here and right now, so here we are sitting still, you can ask

220
00:26:18,480 --> 00:26:27,040
yourself, am I able to accept this, am I able to bear with the experience of this reality,

221
00:26:27,040 --> 00:26:34,520
all the many facets of acceptable to me, and then we can see that our ordinary state

222
00:26:34,520 --> 00:26:40,400
of living is generally suffering, not being able to accept reality, needing for it to be

223
00:26:40,400 --> 00:26:45,760
other than what it is, and when we need for that, because there is no way to make it

224
00:26:45,760 --> 00:26:56,400
other than what it is, we suffer, we build up these thoughts of how I can change it and

225
00:26:56,400 --> 00:26:59,520
what I am going to do to change it, and that is just suffering for us, once we are able

226
00:26:59,520 --> 00:27:05,160
to change it, and we go on to the next one, and the next bad thing comes when we try

227
00:27:05,160 --> 00:27:10,760
to change it as well, when we keep running and running, when we build up this compartmentalization,

228
00:27:10,760 --> 00:27:21,160
this is tolerable, this is intolerant, so meditation has these very profound benefits, very

229
00:27:21,160 --> 00:27:28,520
simple, but very profound, meditation practice is a very simple concept, it is not that

230
00:27:28,520 --> 00:27:35,280
it is hard to understand, it is just very, very hard to practice, and that is simply

231
00:27:35,280 --> 00:27:47,640
because we are used to the wrong way, what we have lived our lives focused on the wrong

232
00:27:47,640 --> 00:27:57,220
way of living, and not just this life, but where we have come from is a way of living,

233
00:27:57,220 --> 00:28:08,200
a way of understanding the world and reacting to the world that is bound up in suffering,

234
00:28:08,200 --> 00:28:16,760
bound up in clinging and craving and wanting and needing, and addiction and aversion and

235
00:28:16,760 --> 00:28:22,920
anger and frustration and boredom and so on, liking and disliking, this is our way of approaching

236
00:28:22,920 --> 00:28:30,640
reality, so when we understand this, I think this is very useful for us in the meditation,

237
00:28:30,640 --> 00:28:36,320
we can see what are we trying to do in the meditation, when we walk, or when we see it,

238
00:28:36,320 --> 00:28:40,960
we are just trying to look and to see things as they are, we are trying to understand,

239
00:28:40,960 --> 00:28:45,600
we are trying to change the way we look at things, we are saying to ourselves, no, the way

240
00:28:45,600 --> 00:28:50,520
I react to things is not proper, the way I interact with the world around me, it is not

241
00:28:50,520 --> 00:28:59,080
bringing me happiness, I am not really content and happy with things as they are right now,

242
00:28:59,080 --> 00:29:04,800
so I am going to change that, I am going to try to understand things better, try to understand

243
00:29:04,800 --> 00:29:11,160
reality better so that when I see things, when reality comes to me, I will be able to

244
00:29:11,160 --> 00:29:19,280
react, be able to answer the questions that reality poses to me, what to do next, I will

245
00:29:19,280 --> 00:29:22,880
have a clear understanding of what to do next, that is all we are trying to do, we are

246
00:29:22,880 --> 00:29:29,120
not trying to change things, we are not trying to leave our bodies, leave reality and

247
00:29:29,120 --> 00:29:39,880
go to some exotic place, we are trying to understand my life, what it is, clearer and

248
00:29:39,880 --> 00:29:51,080
more correct, more exact, so that is the demo for today, and now we will continue on with

249
00:29:51,080 --> 00:30:18,320
our meditation, we will do the mindful frustration and then walk in and then see.

