1
00:00:00,000 --> 00:00:22,800
Good evening, everyone. Welcome to our daily live broadcast.

2
00:00:22,800 --> 00:00:34,200
Today's quote is about contentment. So, one of our famous quotes are the Buddha.

3
00:00:34,200 --> 00:00:38,400
I'm regarding contentment.

4
00:00:38,400 --> 00:00:45,920
So, if we told not many of the Buddha's quotes are all that famous. Even the most famous

5
00:00:45,920 --> 00:00:52,000
quotes are not that famous. The most famous Buddha quotes are the quotes that the Buddha

6
00:00:52,000 --> 00:01:02,080
never actually said, but I've been propagated around the internet. Also, it's an interesting

7
00:01:02,080 --> 00:01:15,800
phenomenon. But I think the most misquoted person on the internet may be besides Albertai and

8
00:01:15,800 --> 00:01:28,080
Stein. Probably up there, right-wing Albertai and Stein. There are very few quotes on the

9
00:01:28,080 --> 00:01:39,200
internet, relatively, that are actually. It's not very quotable because his teaching

10
00:01:39,200 --> 00:01:53,040
is somewhat deep, and it challenges. It doesn't really ring true. Not in a conventional

11
00:01:53,040 --> 00:02:08,200
sentence. It challenges. There are quotable quotes like, no dhamma is worth clinging

12
00:02:08,200 --> 00:02:22,040
to. Nothing is worth clinging to. It doesn't have the same ring to it as the fake Buddha

13
00:02:22,040 --> 00:02:31,440
quotes. This is a good one. This one is one that you could quote. The most quotable

14
00:02:31,440 --> 00:02:37,480
of the Buddha that I can think of is, don't let the moment pass you by. It's actually

15
00:02:37,480 --> 00:02:43,720
word to the Buddha, but it's such a generic saying that it probably wasn't the first

16
00:02:43,720 --> 00:02:54,680
thing he certainly wasn't the last to say. But here, this quote is basically that, like

17
00:02:54,680 --> 00:03:11,240
the wings of a bird. Like the wings of a bird. Like a bird. Just as a bird carries

18
00:03:11,240 --> 00:03:29,800
only its wings, carries its wings as its only bird. So, two, one is satisfied with a

19
00:03:29,800 --> 00:03:41,440
road to protect the road to sustain the body and food to satisfy to sustain the stomach.

20
00:03:41,440 --> 00:04:04,360
See, it's done all that quotable. This is how one is content. Let's say this is a

21
00:04:04,360 --> 00:04:21,880
motto to live by for a Buddhist monk. We can't get what we want. We can't live our lives

22
00:04:21,880 --> 00:04:38,120
in poverty, in orientation. If we begin to covet, which has happened, covet possession

23
00:04:38,120 --> 00:04:49,000
of belongings, things. We're inevitably disappointed unless we have a large following

24
00:04:49,000 --> 00:04:56,360
in our great teacher. The time a monk becomes a great teacher. A great, not great, but

25
00:04:56,360 --> 00:05:01,840
like a well-known teacher, then they get lots of support and are able to get whatever they

26
00:05:01,840 --> 00:05:19,960
want within reason. During training and living and traveling, one has to be content. No

27
00:05:19,960 --> 00:05:26,860
matter what one has to be content, you know, even a great teacher who has great relative

28
00:05:26,860 --> 00:05:38,960
wealth and fame and support. To follow the Buddha's teaching, one need to be content,

29
00:05:38,960 --> 00:05:45,440
and which, how to what extent should one be content, one should be content to the extent

30
00:05:45,440 --> 00:05:55,340
that one only plings to food and roads. The only two things one actually needs. So,

31
00:05:55,340 --> 00:06:05,600
clings to mean, seeks out as a requirement, doesn't go without. Everything else one is able

32
00:06:05,600 --> 00:06:13,120
to go without, but food one seeks out food and roads one seeks out or clothes one seeks

33
00:06:13,120 --> 00:06:22,880
out clothing. Besides these two wings, one seeks out nothing. This is how one is content.

34
00:06:22,880 --> 00:06:32,960
It's all aiming for all of us monks for now. This is a good, good measure, measuring

35
00:06:32,960 --> 00:06:44,560
post allows us to measure up and see what sort of things are we clinging to. The smart

36
00:06:44,560 --> 00:06:52,520
phone is not included in there. A nice house is not even included. Notice he doesn't

37
00:06:52,520 --> 00:06:57,920
include the other requisites dwelling and medicines or the other two requisites, but he doesn't

38
00:06:57,920 --> 00:07:04,000
include those because you don't actually need them. Do you want to be truly content? Doesn't

39
00:07:04,000 --> 00:07:11,480
matter where you sleep, sleep on the floor, sleep on the forest, sleep under a park bench.

40
00:07:11,480 --> 00:07:16,040
So, this story of how I went and went and slept under a park bench. Actually, I've done

41
00:07:16,040 --> 00:07:23,640
it more than once when I was in Sri Lanka. I got into Colombo. I was, I guess I was going

42
00:07:23,640 --> 00:07:32,600
to give a talk, but I got in the night before and the monastery that I was going to was

43
00:07:32,600 --> 00:07:40,000
already closed and so rather than bother them, they'll wake them up. I just walked in

44
00:07:40,000 --> 00:07:44,760
nearby park and I actually kind of snuck in. I think I snuck into the park because it

45
00:07:44,760 --> 00:08:00,320
was closed as well. I went and slept on a park bench. This is, you can, you have

46
00:08:00,320 --> 00:08:07,360
really lived until you've gone to, until you've completely let go and lived in this kind

47
00:08:07,360 --> 00:08:19,160
of poverty. Since those who are homeless are enviable in terms of their pronunciation, they've

48
00:08:19,160 --> 00:08:29,480
gotten to the bottom. Oscar Wilde, I think he is Oscar. No, George Orwell. George Orwell,

49
00:08:29,480 --> 00:08:38,280
if you know this book about his time in Paris and London when he was, when he had no money

50
00:08:38,280 --> 00:08:43,800
and he said, when you get to the bottom, there's a curious feeling that you can't get

51
00:08:43,800 --> 00:08:51,440
any, you can't fall any further. You're suddenly, it's this feeling that we get as

52
00:08:51,440 --> 00:08:58,760
ordinances. You can't do anything more to me. I've gotten to the, I've fallen as far as I can

53
00:08:58,760 --> 00:09:09,920
fall. I remember the small things like I realized one day that I wouldn't, I just couldn't

54
00:09:09,920 --> 00:09:17,960
get laundry so I didn't have any. In the early days, I was using money, monks in Thailand

55
00:09:17,960 --> 00:09:24,120
often use money. And I didn't have any. It was the thing about using money is there was

56
00:09:24,120 --> 00:09:30,800
never enough. And so I just realized that wasn't going to be able to buy, didn't have

57
00:09:30,800 --> 00:09:41,800
any money to buy laundry. And I really panicked. I wouldn't be able to get soap to wash

58
00:09:41,800 --> 00:09:47,880
my clothes and knowing it was soap to wash my body as well. What am I going to do?

59
00:09:47,880 --> 00:09:57,520
And then it just hit me. I'm like, well, wash without soap. I knew it was just liberating

60
00:09:57,520 --> 00:10:03,800
to realize that you can just go without. Of course, the funny thing is once I started keeping

61
00:10:03,800 --> 00:10:12,640
the rules and not using money, there's a lot more well supported people appreciated. There's

62
00:10:12,640 --> 00:10:27,800
an appreciation for them. I'm just doing an appreciation for supporting my practice.

63
00:10:27,800 --> 00:10:48,720
So yeah, monks get support. We're supported and looked after. It's not like we're extravagant

64
00:10:48,720 --> 00:11:01,680
or hard to look after. It's not like providing food and clothing. Basic represents.

65
00:11:01,680 --> 00:11:16,360
It's like it's a great burden. And it's out of appreciation for someone who is cultivating

66
00:11:16,360 --> 00:11:25,880
spiritual truth and cultivating peace and goodness, making themselves a better person. If

67
00:11:25,880 --> 00:11:32,040
you want to put it like that, when you see someone trying to become a better person, wouldn't

68
00:11:32,040 --> 00:11:40,880
you want to help them? Being a monk is all about that and becoming a better person. It's

69
00:11:40,880 --> 00:11:46,240
one thing I support. It's why we support how many people who come here who do meditation

70
00:11:46,240 --> 00:11:55,120
courses. Free food, free shelter, free teaching. I can then you talk about the teaching

71
00:11:55,120 --> 00:12:12,800
that Buddhist monk can provide. Because of the time they have to practice and study and teach.

72
00:12:12,800 --> 00:12:18,720
But we should all be content. And this is the greatest content. It's to be free like

73
00:12:18,720 --> 00:12:31,640
a bird. There's only two things to cling to and hold on to. Everything else let it go and

74
00:12:31,640 --> 00:12:48,040
fly away. So we have some questions. I believe we're all caught up. We have from today

75
00:12:48,040 --> 00:12:58,320
you know even the smallest of feelings in meditation are only noted if it's a big distraction.

76
00:12:58,320 --> 00:13:07,960
This type of meditation, let's get this clear that there's no such thing as a distraction

77
00:13:07,960 --> 00:13:12,760
that which you experience is the object, is the proper object of meditation that which

78
00:13:12,760 --> 00:13:22,560
is present. So if a feeling is present it's not a distraction from something. It's an

79
00:13:22,560 --> 00:13:28,720
experience of something. So it's that something that you want to be clear about. So if a feeling

80
00:13:28,720 --> 00:13:44,320
arises smaller being that feeling is now there. So that's what should be your object of meditation.

81
00:13:44,320 --> 00:13:53,200
So you should note it. How to practice gratitude without being

82
00:13:53,200 --> 00:14:01,200
think gratitude. Truly gratitude coming from the bottom of the heart. Well that's

83
00:14:01,200 --> 00:14:14,960
what anything sincerity is a product of purity. So you can't just, I mean it's kind of

84
00:14:14,960 --> 00:14:23,120
a funny question because you're asking how do you manufacture sincerity? You can't

85
00:14:23,120 --> 00:14:30,320
manufacture it. It's you have to be sincere. You can't manufacture it. What can I do

86
00:14:30,320 --> 00:14:37,360
that will make me be sincere? Okay, because I work that way. The thing is to learn to

87
00:14:37,360 --> 00:14:44,640
be sincere. To stop clinging. What is it that's making the gratitude

88
00:14:44,640 --> 00:14:55,840
fame, delusion, crooked, crookedness, pretense. So if you give up pretense

89
00:14:55,840 --> 00:15:06,320
then you only have sincerity. It's like love, you know, you don't have to cultivate it. If you

90
00:15:06,320 --> 00:15:14,560
cultivate it, it's sort of just artificial. But to get true and pure and real love or gratitude

91
00:15:14,560 --> 00:15:22,800
or any of these things. Just be sincere. Learn to let go and claim

92
00:15:22,800 --> 00:15:30,960
and there'll be a natural sense of appreciation and gratitude. The sensitivity

93
00:15:30,960 --> 00:15:47,040
really becomes sensitive to important things, important events, important actions.

94
00:15:49,120 --> 00:15:53,920
Whatever happened to that second book you were working on. Do you think you might finish it?

95
00:15:53,920 --> 00:15:59,200
There's only one more chapter to write. I've been thinking of writing the last chapter. It's

96
00:15:59,200 --> 00:16:07,600
not the most important. I mean, it is the most important topic, but in a sense it's

97
00:16:08,720 --> 00:16:16,000
it's not really the kind of chapter that you have to write about. Or it's a little bit sensitive

98
00:16:16,000 --> 00:16:21,360
to write about because you can't talk about any band that you can't describe it. Because I'm

99
00:16:21,360 --> 00:16:30,240
trying to make these books fairly open to non-butists. I don't just want to talk about me

100
00:16:30,240 --> 00:16:40,240
behind that. I have to explain how it comes out of the practice and still not going to too much

101
00:16:40,240 --> 00:16:51,280
detail. Because it's so much to be experienced on your limits. If anything is esoteric in

102
00:16:51,280 --> 00:16:57,760
terabyte of Buddhism in Bhanas, I think fairly esoteric. It's the kind of thing you really have to

103
00:16:59,520 --> 00:17:01,680
kind of experience to appreciate.

104
00:17:06,480 --> 00:17:08,560
So I mean, it's not like

105
00:17:11,360 --> 00:17:18,560
it's all like the other chapters in that sense. I had a plan on finishing it, so it's just a matter

106
00:17:18,560 --> 00:17:23,600
of getting around to it. Well, the rest of it's up. The rest of it's all you really need.

107
00:17:32,720 --> 00:17:38,800
Very beautiful quotes. What scripture is it from? This is from the Deganikaya.

108
00:17:38,800 --> 00:17:48,640
The Samanipala Sutta. The fruits of recluse ship, the fruits of renunciation.

109
00:17:52,080 --> 00:17:58,480
This was taught to Ajita Satu. The son of King Bimbisara, Ajita Satu killed this father

110
00:17:59,120 --> 00:18:06,480
at the behest of Davidata. And he was trying to kill the Buddha. Ajita Satu tried to kill his father.

111
00:18:06,480 --> 00:18:14,320
And actually succeeded in killing his father, which was just about the worst thing

112
00:18:18,160 --> 00:18:21,360
up there, among the worst things a person can do.

113
00:18:23,520 --> 00:18:27,440
As a result, even through teaching the sutta, one of the greatest sutta

114
00:18:29,360 --> 00:18:34,000
Ajita Satu was unable to become enlightened as he was destined for help.

115
00:18:34,000 --> 00:18:44,160
But it's an important sutta. Ajita Satu wanted to get a teaching from one of the wise men

116
00:18:45,120 --> 00:18:50,080
around, but he actually wanted to go in here the Buddha taught.

117
00:18:52,720 --> 00:18:57,280
But he knew that if he were to say that, you know, it's going to talk to the Buddha,

118
00:18:57,280 --> 00:19:08,400
it wouldn't really, it would be kind of hypocritical because he plotted with Davidata to kill

119
00:19:08,400 --> 00:19:13,200
the Buddha and he killed one of the Buddha's greatest disciples as father.

120
00:19:14,400 --> 00:19:20,960
So he wanted somebody else to suggest that they go and see the Buddha. He felt awkward,

121
00:19:20,960 --> 00:19:26,160
felt embarrassed like he wasn't worthy. I mean, this is what karma does to you. If you do

122
00:19:26,160 --> 00:19:31,680
very bad things, it tears you away from good people. It makes you feel ashamed and

123
00:19:33,440 --> 00:19:39,840
unable to associate with good people. So he was ashamed for a good reason.

124
00:19:42,000 --> 00:19:49,120
And so he asked all of his advisors who they should go, who we should go see. And they also

125
00:19:49,120 --> 00:19:54,800
just in various teachers, whatever contemporaries of the Buddha, probably these are the teachers

126
00:19:54,800 --> 00:20:05,840
that you find in the Upanishads. And then he turns to Jiwaka, Jiwaka's this doctor who looked

127
00:20:05,840 --> 00:20:12,560
after the Buddha and he was a Buddhist, sutta pan, I think. And he said, what about you? Why are

128
00:20:12,560 --> 00:20:19,920
you quietly to them? I mean, you should go see the Buddha. And so he decides to go see the Buddha.

129
00:20:23,520 --> 00:20:24,640
And he gets there because

130
00:20:30,720 --> 00:20:36,800
he has asked all these other teachers this question and they couldn't really give him an answer.

131
00:20:36,800 --> 00:20:44,640
And so he comes and asks the Buddha, what is the fruit of a relationship, of pronunciation?

132
00:20:44,640 --> 00:20:52,400
What good is it to go off in the forest and live with just a Roman bull? Because he says I have

133
00:20:52,400 --> 00:20:56,800
all these workers working for me and I see the benefit of what they do. They do their work,

134
00:20:56,800 --> 00:21:04,240
they get paid. Whatever they do, they have the satisfaction and gratification and the reward.

135
00:21:04,240 --> 00:21:12,800
That comes with their occupation. What do the monks get? What do they do? What do the Hermit?

136
00:21:12,800 --> 00:21:17,520
It's been reckless. What do people get from the religion?

137
00:21:21,040 --> 00:21:24,880
It's the whole sutta, it's about all the benefits. It's one of the longest sutta's.

138
00:21:24,880 --> 00:21:34,560
The diga means long, so it's one of the few long sutta's of the best, very long. Many,

139
00:21:37,120 --> 00:21:39,920
many, many teachings in it. This is one of them.

140
00:21:43,520 --> 00:21:49,280
Contentment. This is later on in the sutta when he starts to talk about the real benefits.

141
00:21:49,280 --> 00:22:00,800
One of the things that supports, one, to find the greatest benefit, which is freedom.

142
00:22:05,920 --> 00:22:11,120
We're reading someone in Palasu. I believe there's a translation on the internet, probably by

143
00:22:11,120 --> 00:22:17,360
tennis. I don't recommend his translations as the best. Unfortunately, it's a great monk.

144
00:22:17,360 --> 00:22:22,640
He's done a great service for Buddhism with his Vinaya teachings.

145
00:22:25,040 --> 00:22:31,280
But his translations are not... I don't know. I think you have to be one of his followers in his

146
00:22:31,280 --> 00:22:36,000
tradition to really appreciate him. He's not a very good translator, I don't think.

147
00:22:36,000 --> 00:22:48,560
21-day meditation course question. He gives a traditional Zen cushion for sitting

148
00:22:48,560 --> 00:22:52,000
meditation, to get all of it, sitting bones for proper posture.

149
00:22:52,000 --> 00:23:06,720
We have small cushions for you to sit on, put your butt under your bottom.

150
00:23:08,800 --> 00:23:13,120
You can raise your pelvis, but I'm going to concern about proper posture.

151
00:23:14,320 --> 00:23:18,000
There's no real proper posture, not for the best in the practice.

152
00:23:18,000 --> 00:23:27,840
For some a time, it's useful. I don't know. I mean, in Thailand, we sit on these flat cushions.

153
00:23:28,880 --> 00:23:36,000
Everybody does. It's very rare to find someone who tries to sit in any sort of proper posture.

154
00:23:38,400 --> 00:23:42,000
Even if they do, they still sit flat. They don't put anything under their bottom.

155
00:23:42,000 --> 00:23:48,240
That's a fairly Western concept, or maybe Hindu, I don't know, yoga.

156
00:23:48,960 --> 00:23:53,680
But in Buddhism, there are a lot of Buddhism, and there are traditional Buddhism, not just

157
00:23:53,680 --> 00:23:57,520
there, but traditional Buddhism. There were none of these cushions.

158
00:23:59,840 --> 00:24:03,840
There's one teacher who's always complaining, who's complaining to me,

159
00:24:03,840 --> 00:24:06,480
is that, oh, I went to America. These Westerners are so soft.

160
00:24:06,480 --> 00:24:12,640
I like this monk like to rent, likes to rent, I'm sure he's still alive.

161
00:24:15,920 --> 00:24:21,280
He's the teacher at what my dad didn't bank on. He said, I've been all over America and

162
00:24:21,280 --> 00:24:28,480
they're so soft. They sit on these big cushions. It's funny, because yeah, we sit on the floor.

163
00:24:28,480 --> 00:24:36,800
Just a little cloth. Put your cloth down on the carpet, or even the wooden floor.

164
00:24:38,080 --> 00:24:43,120
That's why you sit, a good reason to sit in full lotus is so that you're in the balls of your feet.

165
00:24:43,120 --> 00:24:45,840
The balls of your ankles don't touch the floor.

166
00:24:47,840 --> 00:24:54,480
If you've ever sat flat on the wooden floor, your ankles touch them, and that's quite uncomfortable.

167
00:24:54,480 --> 00:24:58,720
If you sit full lotus, they don't.

168
00:25:05,840 --> 00:25:07,440
The link to my book is at the top.

169
00:25:14,000 --> 00:25:17,280
So maybe the second version isn't there either.

170
00:25:17,280 --> 00:25:30,880
Let me see, let me see. Let me see. It's on our site. There's a link to it in the menus.

171
00:25:30,880 --> 00:25:59,120
Somewhat buried, I admit, but you're welcome to bring your own question if you want.

172
00:25:59,120 --> 00:26:02,320
We've got cushions, if you need one.

173
00:26:03,920 --> 00:26:07,440
Tina made some nice little cushions about this thing that you can put under.

174
00:26:13,920 --> 00:26:17,920
Can we say that in meta-meditation, we're sincere about wanting to some do

175
00:26:17,920 --> 00:26:23,280
anger instead of letting it cause harm. Otherwise, being sincere would mean letting the anger

176
00:26:23,280 --> 00:26:25,200
take control of your words and actions.

177
00:26:25,200 --> 00:26:37,120
I mean, sincerity is just not sure, not sure, rather than forced or artificial.

178
00:26:38,240 --> 00:26:41,680
Sensarity means coming from the heart, so in anything you do.

179
00:26:41,680 --> 00:26:55,520
Yes, in meta, if you are sincere, there will be a sincere version to anger,

180
00:26:55,520 --> 00:27:01,040
not a version, but you know what I mean? Here you ought to put towards anger and its results.

181
00:27:01,040 --> 00:27:11,200
By the way, since here, it would be artificial or fake, because it comes from insight.

182
00:27:14,080 --> 00:27:18,400
We're reading in the Buddha's words by Bikubodian, I still find it fascinating.

183
00:27:19,760 --> 00:27:27,200
So I'm sure it's a very fascinating compilation. One of the verses said that after enlightenment

184
00:27:27,200 --> 00:27:32,480
one uses its insight to direct it to the destruction of other taint. No.

185
00:27:35,280 --> 00:27:39,920
No, and after enlightenment in Arahant does not have greed, anger, delusion.

186
00:27:39,920 --> 00:27:45,280
So you'll have to give me that quote, and I'll explain it to you how you're

187
00:27:46,080 --> 00:27:50,240
misreading it or it's being mistranslated. I'm pretty sure he didn't

188
00:27:50,240 --> 00:27:56,640
mistranslate it, he's a pretty good translator. So probably there's a misinterpretation of the

189
00:27:56,640 --> 00:28:02,800
environment. So this thing is after enlightenment. It's probably not after enlightenment,

190
00:28:02,800 --> 00:28:08,720
it's maybe after the attainment of Sotupana. Sotupana still has the balance.

191
00:28:12,480 --> 00:28:16,720
What is the first sutta I should read to begin a formal study of Buddhism?

192
00:28:16,720 --> 00:28:20,960
Mostly studied Buddhism through secondary sources in the past.

193
00:28:24,000 --> 00:28:29,920
But there's no one sutta, but there are compilations that you can read.

194
00:28:31,120 --> 00:28:35,840
Read the Majimani Kaya. I think that's pretty standard for people to suggest.

195
00:28:35,840 --> 00:28:41,600
The middle length discourses. Pikubodian translations, you can get them, you have to buy them,

196
00:28:41,600 --> 00:28:48,400
or you have to get one of these pirated versions, which are actually available.

197
00:28:51,920 --> 00:28:57,680
There's an app for Android called Teravada Buddhist texts.

198
00:29:00,080 --> 00:29:06,400
And if you read through the description, it talks about such texts.

199
00:29:06,400 --> 00:29:17,280
And if you can find an archive, such texts, you could read them on the Android.

200
00:29:18,160 --> 00:29:24,400
But of course, that would be a prominently in violation of copyright law, although Bikubodian

201
00:29:24,400 --> 00:29:31,360
was fine with it. So read that as we will.

202
00:29:31,360 --> 00:29:38,800
But yeah, reading the Majimani Kaya is a good Majimani Kaya digani Kaya.

203
00:29:38,800 --> 00:29:44,480
These two, if you read these, you should get a pretty good feeling for what the Buddha taught.

204
00:29:45,200 --> 00:29:53,200
Not a perfect one. You still need lots more study and even practice and practice for sure,

205
00:29:53,200 --> 00:29:58,960
but also guidance from the commentary I would argue.

206
00:29:58,960 --> 00:30:04,960
And so it would help you to go here and send it to all the different teachings.

207
00:30:28,960 --> 00:30:39,680
I'm scheduled for the 21-day meditation cards. Will I be sleeping on a pad on the floor?

208
00:30:41,840 --> 00:30:49,920
We have mats that are about an inch or two thick, and you can put a couple of them together.

209
00:30:49,920 --> 00:30:52,720
If you need, we're probably need to get more to think of it.

210
00:30:54,000 --> 00:30:56,480
That's what we should do right away, because we don't have enough.

211
00:30:56,480 --> 00:31:02,000
I don't think for four meditators, I don't think. You may have to look into that and get more

212
00:31:02,000 --> 00:31:09,920
in the next couple of days. We have bedding, we have sheets, pillows, blankets,

213
00:31:12,560 --> 00:31:16,720
bring your own towel, we probably have them here, but that's something you should bring on your

214
00:31:16,720 --> 00:31:22,880
own washcloth and your own all your toiletries. You can wear socks during the day and bear

215
00:31:22,880 --> 00:31:29,600
feet. Bear feet, I like that. There's no rule, but I appreciate bear feet more. That's

216
00:31:29,600 --> 00:31:40,160
just me. I won't look down on you if you wear socks. If you run out of something, we can help you

217
00:31:40,160 --> 00:31:46,480
again. If you run out of something, don't run out of something. Bring everything you need,

218
00:31:46,480 --> 00:31:52,400
or else just live without it. Yeah, we don't have anybody. You're going to be looking after

219
00:31:52,400 --> 00:31:58,800
yourselves in the monastery. We probably can help you. May we've got a Thai woman who can

220
00:32:00,000 --> 00:32:05,840
local supporter who brings food to the monastery. She can probably help get anything you need.

221
00:32:05,840 --> 00:32:23,280
But remember scouts, boy scouts, be prepared, be prepared.

222
00:32:23,280 --> 00:32:38,560
Be prepared to do it without it in some cases.

223
00:33:54,080 --> 00:34:00,480
You have to put a little question right before them. I know for the new people,

224
00:34:02,240 --> 00:34:08,480
might not be clear. Yeah, it's not easy to understand. We don't even explain that.

225
00:34:10,400 --> 00:34:15,920
But it's harder for us to find what your questions and what your comments unless you put the

226
00:34:15,920 --> 00:34:24,240
question right before. There's a little button there that you use to find the question button.

227
00:34:26,960 --> 00:34:34,400
Chanting has different benefits. What are your thoughts on the benefits of chanting?

228
00:34:38,320 --> 00:34:38,640
Yeah.

229
00:34:38,640 --> 00:34:46,640
Yeah, but it doesn't have anything to do with concentrating on mindfulness.

230
00:34:50,400 --> 00:34:55,360
I don't know how chanting would relate to mindfulness, but there's certainly concentration,

231
00:34:55,360 --> 00:35:09,840
and intention, and reiteration of basic teaching. I don't have too many thoughts on it.

232
00:35:15,120 --> 00:35:19,840
I'm dissociated since starting meditation and having a rough time getting past this.

233
00:35:19,840 --> 00:35:26,400
I'm depersonalized and you realize, boy, for someone who's depersonalized, you use the word

234
00:35:26,400 --> 00:35:27,040
I a lot.

235
00:35:31,760 --> 00:35:38,160
How can I get past this disconnect between myself and reality? I'm obsessing over yourself.

236
00:35:40,400 --> 00:35:42,320
Stop worrying about yourself. That will help.

237
00:35:42,320 --> 00:35:51,360
If you're worried, say, worried, worried. If you're upset, say, upset.

238
00:35:55,520 --> 00:35:56,960
Spoiler, there is no self.

239
00:36:01,600 --> 00:36:07,440
There's no self there. There's only experience. There's experiences arise. It's none of them

240
00:36:07,440 --> 00:36:12,320
or self. Let's go of them. They feel better.

241
00:36:26,800 --> 00:36:32,240
What languages would be best to learn for terra lada studies and travel?

242
00:36:32,240 --> 00:36:36,640
English would be a good one.

243
00:36:42,640 --> 00:36:48,160
If you've got a head start in there.

244
00:36:48,160 --> 00:37:04,480
Would it not be beneficial to add a request to use the question button? Yes, it certainly would.

245
00:37:04,480 --> 00:37:17,120
We're changing the website. We've just finally got the website online.

246
00:37:19,280 --> 00:37:22,160
Should I tell you all the address? No, I don't have it here.

247
00:37:23,680 --> 00:37:33,280
We've got a test version of it up. It's live. So it's just a matter of tweaking it,

248
00:37:33,280 --> 00:37:38,880
fine-tuning it, making sure it works. I mean, heck, I'm sure it works better than this website

249
00:37:38,880 --> 00:37:43,920
already. So probably fairly soon we'll just switch over to it.

250
00:37:45,520 --> 00:37:50,720
May not be as full featured yet. Maybe a few things about it that I'm not.

251
00:37:52,480 --> 00:37:59,600
I just looked at it briefly today for the first time, but we'll be moving over to that too.

252
00:37:59,600 --> 00:38:04,800
So once it gets there, then we can add all of them. We can tweak it as necessary.

253
00:38:05,520 --> 00:38:15,520
I don't want to tweak this one anymore. We won't be using it soon.

254
00:38:15,520 --> 00:38:31,840
But you're right, I can't scold you for not using the question mark when you have it

255
00:38:31,840 --> 00:38:51,040
installed. How often do you do these live shows? Sorry, I'm new here.

256
00:38:53,520 --> 00:38:57,840
Right now I'm doing them every day, but that doesn't always indicate.

257
00:38:57,840 --> 00:39:04,080
They're scheduled to happen every day, but I don't do them every day.

258
00:39:05,680 --> 00:39:12,480
I do try to keep everyone on our toes, not being too complacent,

259
00:39:14,640 --> 00:39:18,640
but sometimes we don't broadcast. But otherwise, mostly every day.

260
00:39:18,640 --> 00:39:28,400
All right, sounds like we're kind of winding down now.

261
00:39:30,320 --> 00:39:38,640
We'll end it there. Thank you all for tuning in and pick it up again tomorrow night.

262
00:39:40,000 --> 00:39:45,280
We're moving this weekend, so may not broadcast every day this weekend.

263
00:39:45,280 --> 00:39:52,720
But we'll see. I'll try to. We should be okay. And then there's going to be a switch over

264
00:39:54,240 --> 00:39:59,680
the internet that the new place isn't up until Wednesday, but that just probably means I'll

265
00:39:59,680 --> 00:40:04,080
stay here until Wednesday and then Wednesday morning off to the final moveover.

266
00:40:05,760 --> 00:40:12,720
Robbins here, Robin got here this morning. She's not here right now if she went to Toronto.

267
00:40:12,720 --> 00:40:18,080
But she's already started packing stuff up and we got more people coming to help.

268
00:40:19,120 --> 00:40:23,040
Here in the area and want to come help us move. Great. Come on in.

269
00:40:25,200 --> 00:40:31,920
Then we start moving to start actually moving Saturday. That's when we finally get to go to the new place.

270
00:40:31,920 --> 00:40:41,920
Really, you can name everyone.

