1
00:00:00,000 --> 00:00:10,760
And speaking about the reality as it is, how is it, you think about something or experience

2
00:00:10,760 --> 00:00:17,200
something in your life and you make a talk about it, I mean you come to the point that

3
00:00:17,200 --> 00:00:24,080
you create a talk in your mind which you don't know if it's real or it's not real, that

4
00:00:24,080 --> 00:00:32,240
you don't really, you can't find the reality. Well reality is what it is, if it's arisen in

5
00:00:32,240 --> 00:00:37,920
your mind then from my point of view it's real and I'm pretty sure that the Buddha, that's

6
00:00:37,920 --> 00:00:46,560
what the Buddha taught. There's no, you don't think about what's going on in your mind when

7
00:00:46,560 --> 00:00:56,520
you ask this, when you grapple with the idea of reality, there's no definition, there's

8
00:00:56,520 --> 00:01:04,600
no, there's no train of thought that could arise to give you the definition of reality.

9
00:01:04,600 --> 00:01:12,000
It makes no sense to even hypothesize about what is, what is not real, apart from what

10
00:01:12,000 --> 00:01:19,880
it is arisen, so if it is real, so if a thought arises in your mind then to that extent

11
00:01:19,880 --> 00:01:25,720
to the extent that it is written arisen then to that extent it's real. Other than that it

12
00:01:25,720 --> 00:01:34,440
really, I think it's, it's, it's, it's, it's irrational to, to talk about this being

13
00:01:34,440 --> 00:01:38,480
written or that being written, either that or it's just using logic and reason like how

14
00:01:38,480 --> 00:01:45,000
materialists point at that you can see things, so therefore they must exist and of course

15
00:01:45,000 --> 00:01:51,040
quantum physics and modern physics has grown that out of the water to some theories

16
00:01:51,040 --> 00:01:52,040
have seen.

17
00:01:52,040 --> 00:02:01,560
Given that the experiences are made by mind produced by mind and led by mind produced by

18
00:02:01,560 --> 00:02:12,440
mind, this seems like one. Right. That's from, okay. So, so if you imagine something and

19
00:02:12,440 --> 00:02:20,040
you really believe in it, that becomes something that actually happens or maybe in the

20
00:02:20,040 --> 00:02:24,400
sense that you're thinking, it just means that they come in that sequence, doesn't make

21
00:02:24,400 --> 00:02:30,280
them any less real just because the mind was the instigator or the mind allowed you

22
00:02:30,280 --> 00:02:32,280
to allow the experience to write.

