1
00:00:00,000 --> 00:00:16,800
Okay. Good evening. Good evening, everyone. I don't know when exactly it starts recording,

2
00:00:16,800 --> 00:00:34,320
but broadcasting live January 6th, 2016. Back on with our weekly meditations at McMaster and we're

3
00:00:34,320 --> 00:00:41,360
going to keep trying with them with three times a week actually. And next week we'll have our five

4
00:00:41,360 --> 00:00:50,480
minute meditation lesson. I've got someone helping me out with that, hopefully. I have to test

5
00:00:50,480 --> 00:00:55,360
it. I haven't done a five minute meditation. I've never taught meditation in five minutes. Not

6
00:00:55,360 --> 00:01:00,640
formally. So I'll have to figure out what's my five minute. What's my elevator pitch? Because

7
00:01:00,640 --> 00:01:12,400
what they call it. So we're going to set up. We're going to set up mats and sorry I didn't hear you.

8
00:01:12,400 --> 00:01:21,440
You can practice with us. It's a five minute. Good idea.

9
00:01:21,440 --> 00:01:35,840
Okay. So the basis of meditation is about objectivity. I don't know if I want to get too

10
00:01:35,840 --> 00:01:43,760
philosophical because they shut out. But it's we're trying to see things as they are.

11
00:01:43,760 --> 00:01:50,480
We're trying to see things objectively without reacting to them. So what we're going to do

12
00:01:50,480 --> 00:02:01,200
is we're going to remind ourselves, in general, this is this. So pain is pain. Thoughts are

13
00:02:01,200 --> 00:02:07,440
thoughts. Anger is anger. Depression is depression. And we're not going to react to anything.

14
00:02:07,440 --> 00:02:16,720
Because you see when you react to things, this multiplies them and continues them and they

15
00:02:16,720 --> 00:02:21,200
snowball out of control. So when you experience pain, if you could just experience an

16
00:02:21,200 --> 00:02:28,320
ex pain, it doesn't end up bothering you. If you, when you feel angry and then you say angry,

17
00:02:28,320 --> 00:02:34,560
you remind yourself that you're angry and it doesn't turn into a fight or a problem. It doesn't

18
00:02:34,560 --> 00:02:41,680
make you want to do bad things or say bad things. If you have a thought or someone shouting at you

19
00:02:41,680 --> 00:02:49,600
and you just see it as a thought or as a sound, just as the experience of it, then it won't hurt

20
00:02:49,600 --> 00:02:56,880
you. How many minutes is that? I'm sorry, I was hoping. Well, the last 10 years had a counter on

21
00:02:56,880 --> 00:03:01,920
your side of the hangout. I'm sorry. That was about a minute, I think, right? A couple minutes.

22
00:03:02,720 --> 00:03:06,080
Okay, so then, so here's what we're going to do. Close your eyes.

23
00:03:06,080 --> 00:03:12,240
And maybe I'm going to tell them to close your eyes. First, I have to say how we do it. So

24
00:03:12,800 --> 00:03:20,320
how we do this is just simply reminding ourselves. You pick a word that reminds you what's going on.

25
00:03:20,320 --> 00:03:24,720
Like for instance, if you feel pain, you remind yourself just with one word, pain.

26
00:03:25,440 --> 00:03:32,240
And that becomes your meditation. You'd say to yourself, pain, pain. I don't think I'm having

27
00:03:32,240 --> 00:03:37,600
a close to your eyes yet. I think this is still eyes open. I made a mistake. So is it pain, pain?

28
00:03:38,800 --> 00:03:42,880
If you have a thought, you just say thinking, thinking, if you hear a sound like all there's

29
00:03:42,880 --> 00:03:46,320
noise of these people, because we're right in the middle of the hallway, really. It's kind of funny.

30
00:03:47,440 --> 00:03:48,960
Is it hearing hearing?

31
00:03:51,280 --> 00:03:56,080
And so this is the skill that we're trying to cultivate. So the technique that I'm going to show

32
00:03:56,080 --> 00:04:02,080
you to do this is to train you to do this. So we pick something that's neutral. We'll pick our breath.

33
00:04:02,080 --> 00:04:06,160
Focus on the breath. When your breath comes into your body, your stomach will rise. When your breath

34
00:04:06,160 --> 00:04:10,080
goes out of the body, your stomach will fall. If you don't feel it, you can put your hand there.

35
00:04:11,120 --> 00:04:20,400
But you just say to yourself, rise, see, fall. You can close your eyes and do it. You don't say it out loud.

36
00:04:20,400 --> 00:04:31,920
You just remind yourself in your mind, rise. And that trains you to do that. Once you can do that,

37
00:04:33,200 --> 00:04:41,440
then you begin to extrapolate it into, apply it in the rest of your life. So let's try that now.

38
00:04:44,000 --> 00:04:48,560
And then we'll have to do that. After a minute or so, probably have one minute left,

39
00:04:48,560 --> 00:04:58,320
then I'll say, okay, and then, and now, apply the same concept of seeing your stomach move clearly.

40
00:04:58,320 --> 00:05:03,600
Apply it to things that really matter. Like when you feel pain, remind yourself pain,

41
00:05:03,600 --> 00:05:08,240
when you have thoughts, remind yourself thought thinking thinking. When you have emotions,

42
00:05:08,240 --> 00:05:13,600
remind yourself angry, frustrated, bored, sad, worried. When you want something wanting,

43
00:05:13,600 --> 00:05:21,840
when you like something like kings. When you hear sounds, hearing, hearing, seeing, seeing.

44
00:05:24,160 --> 00:05:27,840
And see, the thing is, we don't have a time limit. It's whatever they want to get up. If they

45
00:05:27,840 --> 00:05:33,440
want to sit there and talk to me for a while, well, I think that's fine. But I'm not sure how many

46
00:05:33,440 --> 00:05:40,000
mats to have. I think we'll have to experiment because I think it's going to be too noisy for

47
00:05:40,000 --> 00:05:46,080
people to be too far away. So it might be like me and two other people at a time. Any more than

48
00:05:46,080 --> 00:05:52,560
that, and it might get too hard for them to hear. But I think we're going to set up a nice carpet,

49
00:05:54,000 --> 00:06:00,800
rug or something. And, and real sitting mats, and then tell people to take their shoes off.

50
00:06:02,000 --> 00:06:06,800
Are we going to do that? I don't think there's any way around getting to take their shoes or boots off.

51
00:06:06,800 --> 00:06:12,240
Or maybe the yoga mats, if a carpet is hard to come by.

52
00:06:12,240 --> 00:06:19,440
Right. We could just put out a few yoga mats and just leave it at that, right?

53
00:06:19,440 --> 00:06:26,640
Yeah, we'd be some for four yoga mats together or something. The other thing is how to get

54
00:06:26,640 --> 00:06:33,680
all this to the university. You know, the sign. I guess it's not really

55
00:06:33,680 --> 00:06:38,960
carryable at that point. See, we're supposed to have a locker. I have to tomorrow I'm going to

56
00:06:38,960 --> 00:06:43,360
find out about our locker. Why we don't have a locker for our club? Because every club should have

57
00:06:43,360 --> 00:06:55,040
a locker. But we don't. Yet that I know. Anyway, that's future plans here. So that seems

58
00:06:55,040 --> 00:07:00,480
doable, no? Yes, and I'm your timing after I realized that I wasn't timing it for you.

59
00:07:00,480 --> 00:07:04,400
That was two minutes. That was only two minutes. So what was two minutes?

60
00:07:05,280 --> 00:07:10,560
The from the point that I realized that from the point that I hadn't been timing it

61
00:07:11,120 --> 00:07:15,280
from there to the end, it was two minutes. So I think you were under five minutes altogether

62
00:07:15,920 --> 00:07:21,840
because the whole I can have was only two minutes. Good. Did I leave anything out? No?

63
00:07:21,840 --> 00:07:29,120
All right. People will have questions probably about any of the changes. It's misleading.

64
00:07:29,120 --> 00:07:35,440
The sign is misleading. False advertising. It's only a two minute meditation.

65
00:07:37,120 --> 00:07:42,400
No, that gets the more time to actually meditate. Yeah. Yeah, it's part of the lesson.

66
00:07:44,240 --> 00:07:47,120
That's going to be great. Funny helps. I got to get someone to take pictures.

67
00:07:47,120 --> 00:07:53,200
Maybe we'll make a video of it. Put it on YouTube.

68
00:08:00,400 --> 00:08:05,680
I should go on the silhouette actually. I should talk to the silhouette the McMaster University

69
00:08:05,680 --> 00:08:10,400
newspaper and the radio as well. Put on a press release. That's what I'll do.

70
00:08:10,400 --> 00:08:14,960
We have to put on a press release. We did this for the meditation. We did this for the

71
00:08:14,960 --> 00:08:18,720
piecewalk. And they interviewed me on radio.

72
00:08:27,760 --> 00:08:35,680
So questions huh? We have questions. This one wasn't exactly directed to you but it's probably

73
00:08:35,680 --> 00:08:41,120
a good one to answer anyway. What I don't know what I don't know yet is. Why do you call him

74
00:08:41,120 --> 00:08:45,280
Bante instead of you to Dama? Yeah, but don't answer this one like every week.

75
00:08:46,720 --> 00:08:56,800
No, it just seems that way. I have answered it for this new people all the time. Sorry.

76
00:08:57,520 --> 00:09:02,000
There are only people all the time. New people joining this group all the time.

77
00:09:02,000 --> 00:09:04,880
But those questions you think people can answer amongst each other.

78
00:09:04,880 --> 00:09:12,800
Someone else can answer it. Okay. Bante, I was very glad to hear that you get to go see your

79
00:09:12,800 --> 00:09:17,680
teacher again. Would it be possible for us to come together as a community and put something

80
00:09:17,680 --> 00:09:23,520
together for you to offer to Ajahn Tom? And if so, what would be appropriate? Thank you, Bante.

81
00:09:23,520 --> 00:09:30,320
That'd be great. I'm not really thinking that I'm going to go. It was, you know, it's been bugging

82
00:09:30,320 --> 00:09:37,280
me that I haven't talked to him about this new monastery but it's not really worth flying across

83
00:09:37,280 --> 00:09:47,280
the world to do it. It's not, you know, it's not like I've got a free ticket to go so it would

84
00:09:47,280 --> 00:09:51,120
have to be paid for which is a considerable expense.

85
00:09:51,120 --> 00:10:01,440
So yeah, absolutely. If I go, let's, for sure, let's keep that in mind but it probably won't be.

86
00:10:02,320 --> 00:10:08,000
I don't know. We'll see because there's talk that they might support summer or some of the ticket

87
00:10:08,000 --> 00:10:15,120
anyway. I'll see if it's really, there's a legitimate conference going on as well.

88
00:10:15,120 --> 00:10:26,160
But the only real reason to go would be to talk to my teacher and he already knows about the place.

89
00:10:26,160 --> 00:10:31,120
I just haven't told him myself, haven't talked to him, haven't gotten his instructions in his

90
00:10:31,120 --> 00:10:48,400
blessing and all that.

91
00:10:48,400 --> 00:10:50,480
Did the book in now, why do you never exist?

92
00:10:50,480 --> 00:11:02,720
I think the word, I think why is problematic. It's a question that doesn't, it's like asking an

93
00:11:02,720 --> 00:11:09,280
innocent man why why why he beats his wife. It's like asking why does a tree exist?

94
00:11:10,720 --> 00:11:16,720
What is the purpose of a tree? It's a ridiculous question. It's a sort of a

95
00:11:16,720 --> 00:11:20,320
theistic question really because the only answer is God.

96
00:11:25,200 --> 00:11:29,120
It's a very human thing to want to find a purpose for things.

97
00:11:30,800 --> 00:11:39,520
We differentiated ourselves from ordinary animals by our ability to cultivate tools, right?

98
00:11:40,160 --> 00:11:44,560
So the idea of things having a purpose and if things don't have a purpose we destroy them or we

99
00:11:44,560 --> 00:11:52,960
get rid of them, we remove them. We've had a very utilitarian bent to us in the sense.

100
00:11:54,240 --> 00:11:58,160
So there's no reason to think that anything has a purpose.

101
00:12:04,240 --> 00:12:08,800
I mean I guess if the question were instead, how is it that the universe exists?

102
00:12:08,800 --> 00:12:19,120
I don't know whether the Buddha knew or not, but again I think it's probably a bad question.

103
00:12:23,760 --> 00:12:31,280
It's incomprehensible to us that there shouldn't be an answer to that, but I think that's our

104
00:12:31,280 --> 00:12:43,040
problem. I think it's just the way we think. I have anxiety issues and once I've become too anxious

105
00:12:43,040 --> 00:12:48,000
about something for a few days continuously, it will get harder for me to meditate and eventually

106
00:12:48,000 --> 00:12:53,120
I stop meditating until the thing I'm worried about is subsided. How can I make sure I don't

107
00:12:53,120 --> 00:12:58,480
stop meditating even when I'm anxious about something? I should meditate on the anxiety. Look up

108
00:12:58,480 --> 00:13:04,720
some of the videos I've done on anxiety particularly because it's an interesting one. Anxiety

109
00:13:04,720 --> 00:13:10,560
it's fairly easy to see that a lot of it's just physical and physical aspects of anxiety aren't

110
00:13:10,560 --> 00:13:16,320
anxiety. If you go to video.series it is a video, what is the website?

111
00:13:16,320 --> 00:13:23,280
Yes. Video.surimonglo.org we got a whole bunch of my videos categorized and I think anxiety is

112
00:13:23,280 --> 00:13:36,000
under mental problems or something. Yes and the other thing you can do is lie down to lying meditation

113
00:13:36,800 --> 00:13:41,840
but definitely meditate on all the aspects of what you call anxiety because much of it is physical

114
00:13:43,760 --> 00:13:48,560
in the anxiety only lasts a moment and if you're patient and keep noting anxiety then you're

115
00:13:48,560 --> 00:13:54,960
doing very well. You're learning a lot, you're studying the anxiety and you're meditating so it

116
00:13:54,960 --> 00:13:58,320
doesn't actually interrupt your meditation if it becomes a part of your meditation.

117
00:14:01,680 --> 00:14:08,640
The thing that you're anxious about, note that thing, thinking, thinking over there.

118
00:14:08,640 --> 00:14:18,720
How do I balance my mind between light terms and higher states of awareness through meditation?

119
00:14:19,360 --> 00:14:24,320
I look around me and see people unaware of life itself and they seem to be in a permanent state

120
00:14:24,320 --> 00:14:30,320
of sleep. I'm finding this very difficult. People seem to be more focused on self gain rather than

121
00:14:30,320 --> 00:14:38,640
local gain. Well there's no balancing them, they're their opposites. The dhamma goes in the opposite

122
00:14:38,640 --> 00:14:50,480
direction of material gain. So the conflict that you face is to choose in the end one or the other.

123
00:14:50,480 --> 00:14:58,000
But I mean that's in the end if you decide that you want to become a monk or something.

124
00:15:06,560 --> 00:15:11,120
What exactly does reflection of an action mean in the core of today? Thanks.

125
00:15:11,120 --> 00:15:21,760
We have to read the whole suit there. Reflection here means before you do something,

126
00:15:25,600 --> 00:15:30,880
before you do something you should reflect. Will this be to my detriment or to the detriment of others?

127
00:15:31,840 --> 00:15:38,320
Will this be a harmful thing that I'm about to do? So then you shouldn't do it. If not then go ahead

128
00:15:38,320 --> 00:15:43,200
and do it. The same well you were doing it and the same after you have done it was that thing

129
00:15:43,200 --> 00:15:50,640
that I did. Harmful or beneficial and if it is harmful then you should not do it again.

130
00:15:51,360 --> 00:15:57,200
Well you're doing it if you realize it's harmful you should stop. So reflecting that way.

131
00:15:57,200 --> 00:16:09,120
I mean it's a this is a talk to his son who is a young boy and it's very fatherly.

132
00:16:09,120 --> 00:16:14,320
It's a unique dialogue that these two have father and son.

133
00:16:14,320 --> 00:16:27,520
If you are mindful of what is, I'm sorry, if you are mindful of what it is neutral and with no

134
00:16:27,520 --> 00:16:33,360
desire, if you accept things for what they are, is there any reason to help others? Is there any

135
00:16:33,360 --> 00:16:42,960
reason to change things? Not really. Except that it's the right thing to do. It's the natural

136
00:16:42,960 --> 00:16:50,160
thing to do. We're not stones. We don't just sit around doing nothing. So part of your equanimity

137
00:16:50,160 --> 00:16:55,040
is what appears to be compassionate and I think you could even argue that it is compassionate

138
00:16:55,040 --> 00:17:02,080
but it's just a word. I mean you act in the right way. So enlightened beings appear to be

139
00:17:02,080 --> 00:17:06,480
very compassionate but some people think well they're not really compassionate because they don't

140
00:17:06,480 --> 00:17:13,600
really care. They don't really care but the natural thing to do is to help others.

141
00:17:25,040 --> 00:17:30,000
Just as a Christian prays the Lord's prayer on a daily basis. Do you have any Buddhist texts

142
00:17:30,000 --> 00:17:39,440
which you read reflect on daily aside from your YouTube dialogue videos? Yeah there's

143
00:17:43,040 --> 00:17:46,880
reflect on the Buddha, the Dhamma and the Sangha. That's the most common.

144
00:17:48,400 --> 00:17:52,560
So we have reflections about the qualities of the Buddha, the qualities of the Dhamma, the qualities

145
00:17:52,560 --> 00:18:04,640
of the Sangha. And then there are other Dhammas that are to be reflected upon daily. There's a list

146
00:18:04,640 --> 00:18:12,480
of five for lay people. There's a list of ten for monks. They're called the Abinha, Pachawek, and a Dhamma.

147
00:18:12,480 --> 00:18:28,240
So what are they? They are. I will get old, I will get sick, I will die, I will lose everything

148
00:18:28,240 --> 00:18:38,960
I have, I can't quite remember. Oh and I'm heir to my Dhamma. Look at the chanting guy. This is

149
00:18:38,960 --> 00:18:55,200
Taravada chanting guy in the house. I believe a person who wishes to ordain must have the

150
00:18:55,200 --> 00:19:01,200
approval of parents. Is that necessary for a grown adult? Would if the parents don't agree?

151
00:19:01,200 --> 00:19:09,280
Yes, technically that's necessary of a grown adult. If the parents don't agree, then you

152
00:19:10,160 --> 00:19:19,520
starve yourself until they give in. Or threaten to throw yourself from a clip or something like that.

153
00:19:21,840 --> 00:19:26,320
Maybe that's a bit of a bit far. But there's something about that.

154
00:19:26,320 --> 00:19:33,280
Monks who actually did that, he threatened to kill themselves. But they're starving yourself.

155
00:19:33,280 --> 00:19:33,840
They work.

156
00:19:39,600 --> 00:19:51,120
I don't know. Technically you can't become a monk. Sorry.

157
00:19:51,120 --> 00:20:05,520
And you can go and stand in front of their front door, sit on their porch until they give in.

158
00:20:09,360 --> 00:20:14,960
Does that happen a lot? Because people ask that question a lot. The parents don't agree.

159
00:20:14,960 --> 00:20:21,680
If your parents are evangelist Christians, they don't.

160
00:20:24,720 --> 00:20:33,520
I mean, many, many people who are not Buddhist would forbid their children even when they get older.

161
00:20:35,760 --> 00:20:42,880
My grandmother would never give consent. Although sometimes it's interesting,

162
00:20:42,880 --> 00:20:49,840
they'll just say, do whatever you want kind of thing. And they're like, ooh.

163
00:20:56,160 --> 00:21:01,360
Is there much difference between a monk from Thailand and the teaching they learn from

164
00:21:01,360 --> 00:21:14,640
a monk from Nepal? Well, every monk teaches differently. But there's also different kinds of Buddhism.

165
00:21:17,760 --> 00:21:21,040
If the parent is deceased, I assume their approval is not needed.

166
00:21:22,560 --> 00:21:28,160
But what if that was just a comment? But what if you knew the parents did not approve and then they

167
00:21:28,160 --> 00:21:32,640
passed away? And you knew that that was not understood? As long as they're dead.

168
00:21:33,520 --> 00:21:36,160
As long as they're dead, it's okay if they never would have gone for it.

169
00:21:36,160 --> 00:21:40,560
They're alive. Yeah. No, I mean, it's not about disrespecting them. It's about making them feel bad.

170
00:21:41,520 --> 00:21:47,600
Okay. Dead. They're gone. In fact, it's kind of an interesting rule because the Buddha

171
00:21:47,600 --> 00:21:58,880
only gave the rule out of respect for his father, who then passed away. But he instated it as a

172
00:21:58,880 --> 00:22:04,480
rule. And a lot of the times the reason the Buddha instated it isn't just the only reason he

173
00:22:04,480 --> 00:22:18,480
instated it, like the impetus. So it's not to say that he didn't agree with his father.

174
00:22:18,480 --> 00:22:23,920
I think you could argue that they're extenuating circumstances, but sorry, put it in,

175
00:22:23,920 --> 00:22:29,920
sorry, put the ordained his nephew without his nephew's parents consent. He said,

176
00:22:29,920 --> 00:22:38,320
they have wronged you. I'm his father.

177
00:22:46,240 --> 00:22:51,360
The suggestion, that a question, but a suggestion to find a way to remind how you do

178
00:22:51,360 --> 00:22:58,960
relate to this page. I think you have like a template set up on for YouTube videos. Don't you

179
00:22:58,960 --> 00:23:08,720
want that? I mean, really, I'm going to add it to every video. No, but isn't there a template

180
00:23:08,720 --> 00:23:16,880
that you can use for your videos? I don't know. Maybe. I mean, I just copy paste copy paste.

181
00:23:16,880 --> 00:23:21,680
I do it for all the down the part of videos, but yeah, I'm not going to do it for every day

182
00:23:21,680 --> 00:23:27,280
in the down the video. No, I thought there because I thought with your older videos they

183
00:23:27,280 --> 00:23:33,200
have so much information. I know why don't think that you probably were typing that in every

184
00:23:33,200 --> 00:23:38,080
time. I just assumed there was some sort of a template. But I mean, you were just copying

185
00:23:38,080 --> 00:23:45,280
pasting from. Well, I haven't saved in the Firefox extension, but it's not like it does it for

186
00:23:45,280 --> 00:23:54,480
me itself. I have to copy it in. I see. Again, I can try to put that in as a comment so people

187
00:23:54,480 --> 00:24:01,920
know because people still do ask questions on the YouTube. I need to comment. There you go.

188
00:24:01,920 --> 00:24:07,680
Someone just say, Hey, if you want to ask questions, go here. Yeah, I do put that in one

189
00:24:07,680 --> 00:24:27,360
of this. Okay, we don't want this place overrun. Small is okay. We'll grow up too soon as it is.

190
00:24:27,360 --> 00:24:46,880
Okay. Yeah, the East Buddhism in East Asia is, it looks really good. It had just had a class today.

191
00:24:47,440 --> 00:24:54,240
The professors, his specialty is Indian Buddhism, which is neat because that's my interest, of

192
00:24:54,240 --> 00:25:07,680
course. But in Indian monasticism, his thesis he's doing is PhD on. So I feel better about it

193
00:25:07,680 --> 00:25:12,880
than before because I wasn't really that keen on East Asian Buddhism, but he sounds like someone

194
00:25:12,880 --> 00:25:22,960
who will be interesting to talk to. And he's a really gregarious sort of person. So he certainly

195
00:25:22,960 --> 00:25:31,280
will keep it interesting. Some people can't teach. Nice people, but they're too timid or they're too

196
00:25:31,280 --> 00:25:38,240
anxious or too anxious to please. So sometimes they, it's all, the people make these, the

197
00:25:38,240 --> 00:25:42,720
professor will make these silly jokes and they just don't know how to be funny and they try to be

198
00:25:42,720 --> 00:25:53,200
funny. And it's just, just teach, you know, it's cool or we don't need to be entertained. Some

199
00:25:53,200 --> 00:25:58,000
people do I suppose. And it's intimidating. You get up there and all these students with glassy eyes

200
00:25:58,000 --> 00:26:04,720
and slouching or right on their phones. It's really intimidating when you're in a

201
00:26:04,720 --> 00:26:11,840
front of a room and everyone looks bored about what you're saying. So if you're not hard, if you're not

202
00:26:11,840 --> 00:26:22,400
strong, you really just have to ignore your audience sometime. I do large classes? Yeah, pretty large.

203
00:26:22,400 --> 00:26:27,520
This one is a third year class, but it's a cross-listed with arts and science, which is neat

204
00:26:27,520 --> 00:26:32,560
because arts and science, which was what I was in years ago and it's this really special program

205
00:26:32,560 --> 00:26:39,520
that McMaster for Keners, for people who are just looking to learn. So we got a bunch of upper-year

206
00:26:39,520 --> 00:26:50,480
art side people, which is awesome. And tomorrow I think I'm switching into a third, another third

207
00:26:50,480 --> 00:26:57,120
year piece studies class because today I had a class on piece and popular culture, but I'm afraid

208
00:26:57,120 --> 00:27:03,680
it sounds like it's going to be a lot of music and movies and I think that's not really appropriate.

209
00:27:06,880 --> 00:27:12,480
Trying to think, could I, could I justify that? Yeah, I don't think so.

210
00:27:15,440 --> 00:27:20,960
It's also new. It's also a newspaper. So I was thinking, well maybe

211
00:27:20,960 --> 00:27:30,400
we're looking at newspapers and newspaper articles. I don't think so. I think piece studies is

212
00:27:30,400 --> 00:27:33,680
going to turn out to be not as interesting as I thought it was. It's very much about

213
00:27:35,040 --> 00:27:39,280
external piece, you know? Not so much about inner piece.

214
00:27:39,280 --> 00:27:48,160
Like people that go to protesting, it's really angry protesting about piece. But too,

215
00:27:48,160 --> 00:27:53,040
and also like brokering piece, piece talks, there's all that kind of stuff.

216
00:27:54,960 --> 00:27:58,880
Like piece corpse, whatever that is, piece corpse, piece corpse.

217
00:27:58,880 --> 00:27:59,760
Piece corpse, yeah.

218
00:28:00,640 --> 00:28:04,560
Like maybe we have the blue, we used to have the blueberries, the piece, piece,

219
00:28:04,560 --> 00:28:09,440
piece, and they called, piecekeepers.

220
00:28:14,400 --> 00:28:18,000
I'm, I'm, I'm thinking religious studies actually be more interesting for me.

221
00:28:18,640 --> 00:28:26,160
I thought it wouldn't, but I'm kind of keen on, I'm religious studies now.

222
00:28:26,160 --> 00:28:34,880
Is that all our questions?

223
00:28:37,520 --> 00:28:40,480
Just one more. Did you hear that from Ted Explante?

224
00:28:41,680 --> 00:28:48,880
No, they, let me see. They just closed applications on January 4th, so

225
00:28:48,880 --> 00:28:56,400
after two days ago. So I think it's still early. I'm not holding my breath. I mean,

226
00:28:56,400 --> 00:29:03,200
not that it's a big deal, but I'm not really hopeful because I don't probably fit the mold of

227
00:29:03,200 --> 00:29:08,720
a student and it's supposed to be student based. So I don't know, maybe they like it, maybe they

228
00:29:08,720 --> 00:29:12,880
don't. The other thing I was supposed to be about change and I didn't talk about change in my

229
00:29:12,880 --> 00:29:20,000
application. I realized that afterwards. I forgot that I should try to tie it into their, the paradox

230
00:29:20,000 --> 00:29:30,320
of change. It'll be neat. I've got lots of work to do as it is.

231
00:29:31,280 --> 00:29:36,000
Yeah, it certainly is about change. I mean, introducing technology to ancient teachings is,

232
00:29:36,000 --> 00:29:41,760
that's all changed. But yeah, that's right. I think it's close enough that they can imply it,

233
00:29:41,760 --> 00:29:44,080
but I probably should at least use the word change.

234
00:29:49,440 --> 00:29:54,000
Okay, that's all done. Good night. Thanks for joining us.

235
00:29:54,000 --> 00:30:11,920
Thank you. Thank you, Robin. Good night.

