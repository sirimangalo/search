1
00:00:00,000 --> 00:00:03,040
Noting technique Q&A.

2
00:00:03,040 --> 00:00:05,440
Awareness or noting?

3
00:00:05,440 --> 00:00:06,720
Question.

4
00:00:06,720 --> 00:00:13,280
Why is it not enough to just be aware of the phenomena that arise without noting them?

5
00:00:13,280 --> 00:00:14,480
Answer.

6
00:00:14,480 --> 00:00:19,120
More important than what is enough is an answer to the question,

7
00:00:19,120 --> 00:00:20,880
what is better?

8
00:00:20,880 --> 00:00:25,360
It may be enough to simply be aware of a phenomenon,

9
00:00:25,360 --> 00:00:28,640
but it is better to apply mindfulness to it.

10
00:00:28,640 --> 00:00:35,200
In our tradition, mindfulness is defined as the grasping of the object as it is,

11
00:00:35,200 --> 00:00:39,600
a quality that may or may not be present in ordinary awareness.

12
00:00:39,600 --> 00:00:46,160
The real question is, without reminding oneself of the objective nature of the phenomenon,

13
00:00:46,160 --> 00:00:53,440
i.e. without noting, how can one be sure one is observing the phenomenon objectively?

14
00:00:53,440 --> 00:00:58,640
Commonly, new meditators have a natural dissinclination to note.

15
00:00:58,640 --> 00:01:03,040
It is more comfortable to allow ones awareness to stand unimpeded,

16
00:01:03,040 --> 00:01:08,160
rather than to train the mind in the precision of identifying the object.

17
00:01:08,160 --> 00:01:11,600
The question of which is better is open for debate,

18
00:01:11,600 --> 00:01:16,400
but one should not let their own prejudice to be the deciding factor either way.

19
00:01:16,400 --> 00:01:23,840
The essential quality of vipassana meditation is sati, sati means to remember, recollect,

20
00:01:23,840 --> 00:01:26,160
or remind oneself of something.

21
00:01:26,160 --> 00:01:31,600
According to the visudimaga, the proximate cause is tirasana,

22
00:01:31,600 --> 00:01:39,600
which means firm recognition, recognition, or saana is present in ordinary experience,

23
00:01:40,320 --> 00:01:44,560
and noting is understood to augment and stabilize this recognition,

24
00:01:44,560 --> 00:01:48,080
preventing the mind from wavering in its objective observation.

25
00:01:48,800 --> 00:01:53,680
While reminding oneself of the nature of phenomenon in this way may not always be comfortable

26
00:01:53,680 --> 00:01:58,080
or feel natural, it is understood to be more reliably objective.

27
00:01:58,720 --> 00:02:05,200
The mental activity involved in producing a label for the object prevents any alternative judgment

28
00:02:05,200 --> 00:02:06,960
or extrapolation of the object.

29
00:02:07,840 --> 00:02:13,120
Put another way, it is inevitable that the mind will give rise to some sort of label for every

30
00:02:13,120 --> 00:02:14,160
experience anyway.

31
00:02:14,960 --> 00:02:18,480
Noting is a means of ensuring that the label is objective.

32
00:02:19,200 --> 00:02:24,800
If one experiences a version or doubt about the technique, it is enough to note these mind states.

33
00:02:25,680 --> 00:02:29,040
They are undeniably hindrances to meditation practice,

34
00:02:29,760 --> 00:02:31,040
whereas noting is not.

35
00:02:31,680 --> 00:02:35,440
If one is able to overcome one's aversion and doubt by noting,

36
00:02:36,160 --> 00:02:43,040
disliking, disliking, or doubting, doubting, it can be assured that one will become

37
00:02:43,040 --> 00:02:47,680
comfortable with it, and see the benefit in the noting technique for oneself.

38
00:02:47,680 --> 00:03:17,520
If one is unable to do so, one is welcome to seek out other techniques and other traditions.

