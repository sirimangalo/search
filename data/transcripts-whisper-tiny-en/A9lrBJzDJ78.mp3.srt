1
00:00:00,000 --> 00:00:15,700
Okay, trying again, welcome everyone to ask a monk, where I answer questions, answering

2
00:00:15,700 --> 00:00:21,240
questions from our website, so if you're interested in asking a question, you can go

3
00:00:21,240 --> 00:00:25,240
visit our website, the link should be in the comments.

4
00:00:25,240 --> 00:00:32,280
If it's a question about meditation, it will probably get answered, it's a question about

5
00:00:32,280 --> 00:00:39,200
Buddhism, it might get answered, no promises.

6
00:00:39,200 --> 00:00:46,080
So today's question is about the relationship between mindfulness and noting, which is

7
00:00:46,080 --> 00:00:51,160
the sort of question I get after, and this one is a bit different.

8
00:00:51,160 --> 00:01:01,840
The question is sort of on the general topic of the relationship between the noting and

9
00:01:01,840 --> 00:01:14,720
the object, so it relates to issues of what does it mean to note something, and what

10
00:01:14,720 --> 00:01:24,920
is the thing that you're noting, and the difference between noting and being mindful if

11
00:01:24,920 --> 00:01:32,080
there is any, or the relationship between mind.

12
00:01:32,080 --> 00:01:39,200
So first just a little bit about mindfulness, the word mindfulness is a translation of the

13
00:01:39,200 --> 00:01:48,560
word sati, it's not a perfect translation, and so it's important, a little bit important

14
00:01:48,560 --> 00:01:57,160
that we understand the word sati, the word that we all use to mean the state of mind,

15
00:01:57,160 --> 00:02:05,280
the state of mind that has clarity, and rightly grasps the object, or grasps the object

16
00:02:05,280 --> 00:02:11,280
in such a way that it can be seen clearly, right, because mindfulness is what leads us to

17
00:02:11,280 --> 00:02:18,280
see clearly, mindfulness is the grasping, or sati is the grasping of the object, that's

18
00:02:18,280 --> 00:02:25,640
how it looks.

19
00:02:25,640 --> 00:02:39,800
So sati is used in colloquial terms, in a conventional sense, to refer to being able,

20
00:02:39,800 --> 00:02:44,920
when you were able to remember things that happened a long time ago, and this concept

21
00:02:44,920 --> 00:02:53,120
of memory I think is why the Buddha chose it, because in meditation it means to remember

22
00:02:53,120 --> 00:03:04,080
the present moment, right, it's this sort of general meditative characteristic that we

23
00:03:04,080 --> 00:03:08,320
use when we're talking about mindfulness, when we're talking about meditation in general,

24
00:03:08,320 --> 00:03:15,960
that means you're not distracted, means that you're not dwelling in abstraction, this

25
00:03:15,960 --> 00:03:24,160
concept of abstraction I think is very important, and it becomes important when we talk

26
00:03:24,160 --> 00:03:30,160
about the difference between mindfulness and noting, as it's called.

27
00:03:30,160 --> 00:03:37,000
So often our minds are stuck in abstraction, which means all of you who are commenting

28
00:03:37,000 --> 00:03:43,880
right now, you're having to type, so you're having to come up with conceptual thought,

29
00:03:43,880 --> 00:03:50,320
because the thought is not evil, it's not bad, but it's not mindful.

30
00:03:50,320 --> 00:03:56,440
You find it's very hard to type a question when you're trying to be mindful at the

31
00:03:56,440 --> 00:04:01,720
same time when you're present, right, because at least in the beginning being present

32
00:04:01,720 --> 00:04:13,280
means you can be able to be aware of the fingers moving, and so you lose the abstract

33
00:04:13,280 --> 00:04:22,160
thought, it's hard to maintain sort of a stream of conceptual thought when you're

34
00:04:22,160 --> 00:04:29,160
mindful, so it's two different sort of ideas, right, the idea is that when we dwell

35
00:04:29,160 --> 00:04:35,360
in abstract thought we've forgotten about the reality, I mean a simple one would be being

36
00:04:35,360 --> 00:04:42,040
aware that you're sitting, this awareness is lost on most of us even though we're probably

37
00:04:42,040 --> 00:04:52,920
most of us sitting, so if you're caught up in writing or reading or watching, you might

38
00:04:52,920 --> 00:05:00,600
forget that if you're listening to me, you might forget that you're sitting, but when

39
00:05:00,600 --> 00:05:08,600
I say that to you, it reminds you, I say, hey, did you know that you're sitting and suddenly

40
00:05:08,600 --> 00:05:13,960
you're reminded that you're sitting? This is, I think, why the Buddha chose this word

41
00:05:13,960 --> 00:05:22,080
setting, why it's how he described this sort of not just a state but an activity, right,

42
00:05:22,080 --> 00:05:28,280
because mindfulness is a leads to clarity, and the act of cultivating mindfulness is this

43
00:05:28,280 --> 00:05:35,480
process by which you rightfully grasp, or rightly, not rightfully, rightly grasp an object,

44
00:05:35,480 --> 00:05:44,400
it's a wrong way grasping it, talk a little bit about that as well.

45
00:05:44,400 --> 00:05:59,160
But abstraction is, it is useful, it's really the only way we can understand and change

46
00:05:59,160 --> 00:06:08,280
our view, right, abstraction is the basis of language, so for someone else to remind us

47
00:06:08,280 --> 00:06:15,960
of something, they need to use this abstract, these concepts, words, words are all concepts,

48
00:06:15,960 --> 00:06:21,120
the word set doesn't mean anything, it's just a sound, right, but it has meaning conceptually

49
00:06:21,120 --> 00:06:27,680
to us, and that allows us to change, based on how our mind works, it allows us to conceive

50
00:06:27,680 --> 00:06:33,560
of something, we know what set the refers to, or what mindfulness refers to in it, based

51
00:06:33,560 --> 00:06:39,960
on our memory and our recognition and our ability to associate, we associate that word with

52
00:06:39,960 --> 00:06:45,160
certain mind states, and those are evoked, and the mind states are all real, right,

53
00:06:45,160 --> 00:06:51,960
mindfulness as a reality is real, but the word mindfulness is just a concept, when I say

54
00:06:51,960 --> 00:06:58,760
mindfulness, it evokes in your certain real mind states that are an abstract, and so they're

55
00:06:58,760 --> 00:07:05,840
useful, teaching is useful me talking, all of this talking, some of you I think find

56
00:07:05,840 --> 00:07:15,280
it useful, and it's useful as an abstraction, as a way of changing the way you look at

57
00:07:15,280 --> 00:07:24,920
things, and allowing you to see reality in a different way, but it will never be a replacement,

58
00:07:24,920 --> 00:07:29,080
it will never be enough, I could give talks, and you could listen to all of my talks,

59
00:07:29,080 --> 00:07:36,400
and it will never be enough, if it wasn't associated with the practice that it's trying

60
00:07:36,400 --> 00:07:45,120
to evoke, right, the states of mind that it's hopefully trying to evoke in the listeners,

61
00:07:45,120 --> 00:07:50,840
and that's where noting sort of comes in, it's sort of a nexus between the two, noting,

62
00:07:50,840 --> 00:07:59,680
the word noting, I don't use a lot, I just usually tell people to say to themselves,

63
00:07:59,680 --> 00:08:06,720
because the word noting is, it's analytical, it's sort of dry, to me it has specific

64
00:08:06,720 --> 00:08:12,560
connotations that aren't necessarily helpful, it's not a bad word, I just, and so people

65
00:08:12,560 --> 00:08:17,800
use it, I'm not critical of them, but I like to tell people to say to themselves, right,

66
00:08:17,800 --> 00:08:23,800
because it's more practical, you know, it's not clinical anymore, it's just say to yourself,

67
00:08:23,800 --> 00:08:30,600
there's no doubt about what I'm telling you to do, and that to me relates back to this,

68
00:08:30,600 --> 00:08:36,800
what's really going on, what this really is, is the use of a mantra, mantra meditation

69
00:08:36,800 --> 00:08:42,320
is very old, I think before the Buddha, and it's very broad and widespread, and well

70
00:08:42,320 --> 00:08:50,680
broadly, widely recognized as an effective meditation tool, because it's the simplest means

71
00:08:50,680 --> 00:08:56,640
of creating the appropriate mind state, whatever the appropriate mind state is, I mean

72
00:08:56,640 --> 00:09:06,000
it's similar to when we yell at someone, if I yell at someone and say, you hurt me, that's

73
00:09:06,000 --> 00:09:15,880
actually a means of reinforcing the anger, of creating confidence, and I mean maybe not

74
00:09:15,880 --> 00:09:20,520
consciously, this isn't, you're not doing it in order to do that, but it has the effect,

75
00:09:20,520 --> 00:09:27,680
words are very powerfully of the effect of augmenting our emotions, often to our detriment,

76
00:09:27,680 --> 00:09:28,680
right?

77
00:09:28,680 --> 00:09:33,480
If you start thinking to yourself, like you're up on a stage, or if I'm here on YouTube

78
00:09:33,480 --> 00:09:42,600
and I look and I say, oh, there's 30, okay, I don't know what's going to happen there,

79
00:09:42,600 --> 00:09:50,560
there'll probably be some minutes of just blankness, but I am back, hopefully.

80
00:09:50,560 --> 00:09:59,800
I tried to talk, I asked someone about it, they don't know what's going on.

81
00:09:59,800 --> 00:10:14,520
Technology, well, the truth is, we shouldn't be too spoiled, because you think about the

82
00:10:14,520 --> 00:10:20,440
difficulty you would have finding a teacher, not to say I'm some great teacher, but any

83
00:10:20,440 --> 00:10:22,960
teacher, right?

84
00:10:22,960 --> 00:10:26,920
You would have to go off and you would have to probably buy a plane to get, and then

85
00:10:26,920 --> 00:10:35,600
you would have to go off into the jungle, and we might have to wait many hours before the

86
00:10:35,600 --> 00:10:44,680
teacher comes out of their hut, because they only come out to teach at certain times.

87
00:10:44,680 --> 00:10:51,920
So we can consider ourselves spoiled, being able to hear this at all.

88
00:10:51,920 --> 00:10:57,240
So a few minutes, having to wait for it to buffer, I think I know what I can do.

89
00:10:57,240 --> 00:11:04,360
I can just crash the YouTube app, and if I reload it, it asks me if I want to reconnect,

90
00:11:04,360 --> 00:11:05,360
that seems to work.

91
00:11:05,360 --> 00:11:16,480
So as soon as it buffers, I'll just kill the app, and start off.

92
00:11:16,480 --> 00:11:19,360
I don't know where I was at in the talk.

93
00:11:19,360 --> 00:11:24,800
So we're talking about mindfulness, apologies if I miss something here, or repeat something.

94
00:11:24,800 --> 00:11:29,760
We're talking about mindfulness and noting, and how noting is sort of the nexus between

95
00:11:29,760 --> 00:11:36,400
abstraction and reality, and right.

96
00:11:36,400 --> 00:11:38,720
So I was talking about the mantra, how I talk about it.

97
00:11:38,720 --> 00:11:43,200
I usually use the language of a mantra, because it's really where this comes from.

98
00:11:43,200 --> 00:11:45,280
Why do we use this word?

99
00:11:45,280 --> 00:11:49,560
Well, this isn't very old technique, it's not something new that someone said, hey,

100
00:11:49,560 --> 00:11:52,080
why don't we repeat a word to ourselves?

101
00:11:52,080 --> 00:11:55,360
It didn't come out of nowhere, it wasn't a new creation.

102
00:11:55,360 --> 00:11:59,520
It's not like there was an old way of meditating, and suddenly someone thought, boy, that

103
00:11:59,520 --> 00:12:02,000
she was words.

104
00:12:02,000 --> 00:12:03,000
And why?

105
00:12:03,000 --> 00:12:05,840
Because we're always using words.

106
00:12:05,840 --> 00:12:08,920
When you like something, you say, boy, that's great.

107
00:12:08,920 --> 00:12:12,920
You have this concept in your mind of, I should get that.

108
00:12:12,920 --> 00:12:19,040
We have, it may not even be words, but it's a conceptualization.

109
00:12:19,040 --> 00:12:25,880
You see something, you say, that's a banana, or that's an apple, or that's a piece of cheesecake.

110
00:12:25,880 --> 00:12:33,520
And that concept reminds you of what it tastes like to eat a piece of cheesecake, and

111
00:12:33,520 --> 00:12:37,080
creates all sorts of problems, right?

112
00:12:37,080 --> 00:12:41,080
And mantras often play with that.

113
00:12:41,080 --> 00:12:46,360
So you might have a mantra that helps you create certain states of mind, stimulate this.

114
00:12:46,360 --> 00:12:51,720
Hopefully, you don't use a mantra that stimulates greed or anger, but we often use words

115
00:12:51,720 --> 00:12:54,040
in those ways.

116
00:12:54,040 --> 00:12:55,040
Meditation, good meditation.

117
00:12:55,040 --> 00:13:00,600
And this is why meditation can be dangerous, because it can evoke quite conceivably.

118
00:13:00,600 --> 00:13:05,240
You could have a meditation that would make you more angry, more greedy, more deluded,

119
00:13:05,240 --> 00:13:13,400
especially, because, of course, any religious teaching that has wrong view, you know,

120
00:13:13,400 --> 00:13:24,360
it often involves teachings and even sayings, mottos, even mantras that support delusion.

121
00:13:24,360 --> 00:13:31,280
But a good meditation will be something that, or can be something that evokes good

122
00:13:31,280 --> 00:13:36,120
positive emotions, one that comes to mind, I think, should come to mind to many people

123
00:13:36,120 --> 00:13:38,600
who is loving kindness.

124
00:13:38,600 --> 00:13:44,120
We use a mantra, we'll say to ourselves, may I be happy, may all beings be happy.

125
00:13:44,120 --> 00:13:47,760
And it's meant to, it's not lip service, right?

126
00:13:47,760 --> 00:13:54,160
The words themselves are mean or useless, they're just words, but they have the potential,

127
00:13:54,160 --> 00:14:05,360
especially over time, to evoke positive mind states of love, of compassion, of kindness.

128
00:14:05,360 --> 00:14:11,160
Other meditations aren't designed to create an emotion, but they're designed to similarly

129
00:14:11,160 --> 00:14:12,840
cultivate certain mind states.

130
00:14:12,840 --> 00:14:21,880
So, casino meditation is a good example where a casino is an object that you use to create

131
00:14:21,880 --> 00:14:29,320
a sense of totality, where your whole universe is one thing, like a color.

132
00:14:29,320 --> 00:14:36,080
So you start by focusing on a circle of white, say, a color white, and you just repeat

133
00:14:36,080 --> 00:14:47,240
you look at it and you repeat yourself, white, white, white in the mantra helps evoke a singularity,

134
00:14:47,240 --> 00:14:51,280
let's say, or a sense of single-pointedness.

135
00:14:51,280 --> 00:14:59,200
So white isn't a special mind state, but because it's a simple concept, and it's very

136
00:14:59,200 --> 00:15:07,840
singular, it pulls in all of the associated mind states, like white isn't something that's

137
00:15:07,840 --> 00:15:10,680
going to make you angry or greedy or deluded or so on.

138
00:15:10,680 --> 00:15:13,320
It's not going to be an evoke bad thing.

139
00:15:13,320 --> 00:15:20,920
So it will create, it has the potential to help you create a lot of very powerful and

140
00:15:20,920 --> 00:15:27,760
pure states of mind, eventually so that all you see when you close your eyes is white,

141
00:15:27,760 --> 00:15:32,120
and the benefits there are this very strong focus.

142
00:15:32,120 --> 00:15:42,000
So with mindfulness, the intention is to help us bring our minds back to focus not on a

143
00:15:42,000 --> 00:15:44,600
concept, but on reality.

144
00:15:44,600 --> 00:15:49,880
So we're using abstraction to bring the mind back to reality, and that's what's special

145
00:15:49,880 --> 00:15:53,800
and unique about mindfulness, that's why there's such a buzzable mindfulness, is it different

146
00:15:53,800 --> 00:15:55,000
from other types of meditation?

147
00:15:55,000 --> 00:15:56,520
Yes.

148
00:15:56,520 --> 00:16:01,360
It is unique in that sense, now you might say it's similar in the same in other ways, but

149
00:16:01,360 --> 00:16:06,240
in that sense mindfulness is unique, because rather than focusing your attention on some

150
00:16:06,240 --> 00:16:14,880
other abstraction, or on an abstraction at all, the object of the mantra is real, is experiential.

151
00:16:14,880 --> 00:16:18,240
The word real is slippery because it means different things to other people, to different

152
00:16:18,240 --> 00:16:24,720
people, but experiential, it's a part of experience, which in Buddhism is what's real.

153
00:16:24,720 --> 00:16:30,240
Like seeing, when you say to yourself seeing, seeing, it helps bring your mind back to

154
00:16:30,240 --> 00:16:31,240
the experience.

155
00:16:31,240 --> 00:16:40,640
Oh, yes, I'm seeing at this moment, there's this sense and this awareness, this presence.

156
00:16:40,640 --> 00:16:47,320
Which is also objective, because our ordinary awareness and experience of things is steeped

157
00:16:47,320 --> 00:16:52,440
and habit, we remember that something brought us pleasure, so we like it, and we conceive

158
00:16:52,440 --> 00:17:02,400
of it as positive as pleasant, or we remember, we associate it with a past suffering, and

159
00:17:02,400 --> 00:17:04,560
so we dislike it, and so on.

160
00:17:04,560 --> 00:17:10,720
Or we associate it with some view and delusion arises, like we look in the mirror and

161
00:17:10,720 --> 00:17:16,200
we see ourselves, boy, I'm so handsome or boy, I'm so ugly.

162
00:17:16,200 --> 00:17:21,120
Or we have this conceit arises, or so on, or even just the view of self.

163
00:17:21,120 --> 00:17:27,720
This is me, this is mine, this I am, this is mine.

164
00:17:27,720 --> 00:17:34,280
And so mindfulness, or this practice that we talk about where you remind yourself of the

165
00:17:34,280 --> 00:17:45,080
experience, has the potential to remove all of that, it reminds you, and it evokes objective

166
00:17:45,080 --> 00:17:46,080
states, right?

167
00:17:46,080 --> 00:17:49,640
Instead of saying, this is bad, this is good, this is me, this is mine, you say this

168
00:17:49,640 --> 00:17:55,080
is this, seeing, seeing, hearing, is hearing, thinking, thinking.

169
00:17:55,080 --> 00:18:00,840
This is what the Buddha taught, and this is what you'll see him teach again and again,

170
00:18:00,840 --> 00:18:07,440
and so I want to stop here just for a moment to point this out, that we have shelves

171
00:18:07,440 --> 00:18:10,000
of the Buddha's teaching.

172
00:18:10,000 --> 00:18:19,560
And often what is missed is this very basic core teaching, that is really the bulk of it.

173
00:18:19,560 --> 00:18:24,040
It's the bulk of it, but it's so simple and so ordinary that it gets overlooked of it.

174
00:18:24,040 --> 00:18:25,040
What do we know about Buddhism?

175
00:18:25,040 --> 00:18:26,040
What is Buddhism?

176
00:18:26,040 --> 00:18:29,880
Well, we have things like the Four Noble Truths, because they're very powerful to

177
00:18:29,880 --> 00:18:36,720
talk about, we have mindfulness, but we also have lots of stories, like the life of the

178
00:18:36,720 --> 00:18:39,760
Buddha examples and so on.

179
00:18:39,760 --> 00:18:45,360
And even in Buddhist culture, you'll find this a lot, but it's still rare to have people

180
00:18:45,360 --> 00:18:47,520
talk about the six senses, right?

181
00:18:47,520 --> 00:18:48,520
Seeing, what is Buddhism?

182
00:18:48,520 --> 00:18:53,600
Buddhism is about seeing, hearing, smelling, tasting, feeling, thinking, but it's really

183
00:18:53,600 --> 00:18:54,600
the core of it.

184
00:18:54,600 --> 00:18:57,640
The five aggregates, what are the five aggregates?

185
00:18:57,640 --> 00:19:05,800
They're five things, five aspects of seeing, five aspects of hearing, and this is, why

186
00:19:05,800 --> 00:19:10,560
this is so core, this is the Buddha taught us to understand and to see reality, and that's

187
00:19:10,560 --> 00:19:13,800
what's going to happen when you use mindfulness.

188
00:19:13,800 --> 00:19:20,960
So the question, this is all just preparatory to help us understand the context of this

189
00:19:20,960 --> 00:19:21,960
question.

190
00:19:21,960 --> 00:19:30,200
This person was asking whether you can note many things in one word, right?

191
00:19:30,200 --> 00:19:37,880
And I think my answer to this is to say, don't confuse the act of noting with the states

192
00:19:37,880 --> 00:19:40,240
of mind that it evokes.

193
00:19:40,240 --> 00:19:49,440
Mindfulness is a tool, it's an artificial tool used to bring the mind closer to closer

194
00:19:49,440 --> 00:19:55,280
to reality and more objective about that reality, about the experience.

195
00:19:55,280 --> 00:20:03,640
But it is not the objectivity, it is not the awareness, and so don't get too concerned

196
00:20:03,640 --> 00:20:05,280
about the details, right?

197
00:20:05,280 --> 00:20:10,360
In a general, I want this to be a general message to people that mindfulness shouldn't

198
00:20:10,360 --> 00:20:15,760
be seen as the states of mind, mindfulness is the tool that you use, or this noting the

199
00:20:15,760 --> 00:20:20,120
mantra is the tool that we use, that evokes certain minds to it.

200
00:20:20,120 --> 00:20:25,840
So sure, if you're anxious, and you know that anxiety is composed of different physical

201
00:20:25,840 --> 00:20:29,720
and mental states, you can still just say anxious, anxious.

202
00:20:29,720 --> 00:20:37,600
The idea is that it helps you to see your experiences more clearly.

203
00:20:37,600 --> 00:20:49,320
But not to see the mantra as some kind of pill or some kind of magical wand that

204
00:20:49,320 --> 00:20:55,280
you wave and it affects reality.

205
00:20:55,280 --> 00:21:00,040
It doesn't have to be one to one, you don't have to be mindful of everything.

206
00:21:00,040 --> 00:21:06,840
I mean, I think in terms of anxiety, because I think what the person was asking about

207
00:21:06,840 --> 00:21:08,320
anxiety is a really good example.

208
00:21:08,320 --> 00:21:16,280
What I like to talk about, because it's one you can often see very quick results in meditation.

209
00:21:16,280 --> 00:21:20,160
And when you separate the different experiences.

210
00:21:20,160 --> 00:21:27,880
So on the one hand, the whole experience is anxiety.

211
00:21:27,880 --> 00:21:34,720
But on the other hand, some aspects of it are physical, and some aspects of it are mental.

212
00:21:34,720 --> 00:21:40,680
So the butterflies that you feel in your stomach, the heart beating quickly, the tension

213
00:21:40,680 --> 00:21:45,000
in your shoulders, all of those can be noted individually, right?

214
00:21:45,000 --> 00:21:51,160
And they help you to come closer to the object.

215
00:21:51,160 --> 00:21:55,240
And you'll see quite quickly what I mean by there's a difference between the noting and

216
00:21:55,240 --> 00:22:01,360
the mindfulness, because simply saying to yourself, when your tense and the shoulders

217
00:22:01,360 --> 00:22:05,640
say the tense, tense, or you feel bubbling in your stomach, or there's butterflies in

218
00:22:05,640 --> 00:22:11,880
your stomach because your anxious is feeling, feeling, it doesn't go away.

219
00:22:11,880 --> 00:22:15,880
It doesn't go away, but it doesn't lead to more anxiety.

220
00:22:15,880 --> 00:22:23,200
And you'll see that it evokes states of neutrality, states of objectivity, that aren't

221
00:22:23,200 --> 00:22:24,200
reactive.

222
00:22:24,200 --> 00:22:26,200
Because that's how anxiety works.

223
00:22:26,200 --> 00:22:32,400
Anxiety you start with a thought, there's 50 people now listening to me talk here.

224
00:22:32,400 --> 00:22:34,560
And that makes me self-conscious, right?

225
00:22:34,560 --> 00:22:41,680
It evokes that thought, evokes certain states in the mind of anxiety, which then creates

226
00:22:41,680 --> 00:22:45,880
these feelings in the body, and this is where the terrible part happens, is those feelings

227
00:22:45,880 --> 00:22:51,040
in the body make me more anxious, oh, I'm anxious.

228
00:22:51,040 --> 00:22:55,680
And they bounce back and forth.

229
00:22:55,680 --> 00:23:01,760
So the states of the physical states lead to more anxiety, more anxiety, he means stronger

230
00:23:01,760 --> 00:23:03,000
physical reactions.

231
00:23:03,000 --> 00:23:08,320
And it becomes, it's snowballs into this sort of feedback loop, bigger and bigger and bigger

232
00:23:08,320 --> 00:23:10,840
until we can have a panic attack.

233
00:23:10,840 --> 00:23:14,240
Yeah, this is, I think, I've never had a panic attack, but I think this is what people

234
00:23:14,240 --> 00:23:16,520
talk about when they talk about a panic attack.

235
00:23:16,520 --> 00:23:22,440
It's just gotten overwhelming, and they have no way of dealing with that.

236
00:23:22,440 --> 00:23:27,000
But you can see that each moment, there's a process going on, there's abstraction.

237
00:23:27,000 --> 00:23:35,320
You experience something, and then you conceptualize it, and you react to it.

238
00:23:35,320 --> 00:23:44,520
And all of that can be replaced and can be changed if you use a different mantra, because

239
00:23:44,520 --> 00:23:48,680
that's kind of a mantra, I'm anxious, oh no, this is a problem.

240
00:23:48,680 --> 00:23:56,000
You change it to just, this is anxiety, or the physical, this is feeling, right?

241
00:23:56,000 --> 00:24:01,640
And this evokes very different states of mind, as the potential to, it's not magic, it's

242
00:24:01,640 --> 00:24:07,240
not in and of itself mindfulness, but if used consistently, properly, and with the right

243
00:24:07,240 --> 00:24:15,000
idea of why you're doing it, it can evoke states of objectivity of calm, that counteract

244
00:24:15,000 --> 00:24:21,120
the buildup of things like anxiety.

245
00:24:21,120 --> 00:24:32,560
So the person was asking about things like numbers, and I don't really want to get into

246
00:24:32,560 --> 00:24:40,480
the details of their other question, but it was the idea of the mind can only take one

247
00:24:40,480 --> 00:24:41,640
object.

248
00:24:41,640 --> 00:24:44,400
So does that mean the number one is the only real number?

249
00:24:44,400 --> 00:24:52,400
But why I think that it's related is, again, this confusion of the difference between

250
00:24:52,400 --> 00:25:00,000
concepts and reality, abstraction, again, is useful, and the mantra is a useful abstraction

251
00:25:00,000 --> 00:25:09,160
that allows us to evoke certain states and direct our attention to the object of our

252
00:25:09,160 --> 00:25:14,960
experience, or the object of maybe not even experience in other meditations, it's a concept,

253
00:25:14,960 --> 00:25:15,960
right?

254
00:25:15,960 --> 00:25:20,480
So the color white, you just evoke it in your mind, right?

255
00:25:20,480 --> 00:25:24,960
But in mindfulness, the difference, the only difference is the object, and it's an important

256
00:25:24,960 --> 00:25:25,960
difference.

257
00:25:25,960 --> 00:25:31,320
It's important because it allows us to see certain things that aren't present in concepts,

258
00:25:31,320 --> 00:25:41,560
humans, our inability to, or our lack of familiarity with impermanence is what leads us to

259
00:25:41,560 --> 00:25:42,560
disappointment.

260
00:25:42,560 --> 00:25:45,800
Things don't go as expected, and why didn't they go as expected?

261
00:25:45,800 --> 00:25:50,880
But for a meditator, it's like, oh yeah, I don't know what's going to happen next.

262
00:25:50,880 --> 00:25:51,880
And that's okay.

263
00:25:51,880 --> 00:25:57,240
I mean, that's, you become familiar with reality, and the idea, the whole idea of expectation

264
00:25:57,240 --> 00:26:05,200
becomes absurd, you can't know, and any kind of expectation that you do have is just going

265
00:26:05,200 --> 00:26:13,080
to wind you up for potential disappointment, and it doesn't have any benefit to expect

266
00:26:13,080 --> 00:26:14,080
things, right?

267
00:26:14,080 --> 00:26:17,320
I want things to be this way, but what does that mean, right?

268
00:26:17,320 --> 00:26:21,800
All that means is the cultivation of one thing, doesn't have any bearing on how things

269
00:26:21,800 --> 00:26:29,280
are going to be, and knowledge of suffering, that when you try to fix things, you try to

270
00:26:29,280 --> 00:26:35,840
control things, you just suffer, you get stressed, and you're met with disappointment,

271
00:26:35,840 --> 00:26:40,720
or even you get what you want, and you just want it more, and so on.

272
00:26:40,720 --> 00:26:46,480
And a non-self, you can't control the desire to control the looking at things in terms

273
00:26:46,480 --> 00:26:52,640
of controlling them, forcing, being in charge, looking at things as me, as mine, is what

274
00:26:52,640 --> 00:26:59,440
leads to suffering in the first place, we don't get, we don't suffer when someone else

275
00:26:59,440 --> 00:27:06,800
loses a possession, we suffer when we lose something, we suffer when we can't control because

276
00:27:06,800 --> 00:27:10,040
of our attachment to things being me in mind.

277
00:27:10,040 --> 00:27:15,160
So I mean, all these things are what we see from reality, our lack of familiarity with

278
00:27:15,160 --> 00:27:21,720
these fundamental aspects of nature leads us to seek out certain experiences, right?

279
00:27:21,720 --> 00:27:27,240
We conceive of experiences as being people, as being things, and those things we conceive

280
00:27:27,240 --> 00:27:32,520
of as bringing us pleasure, bringing us pain, and so we react positively and negatively

281
00:27:32,520 --> 00:27:43,760
towards them, through meditation, that changes, we start to see things just as experience.

282
00:27:43,760 --> 00:27:53,000
And we see more clearly what is the reality of things, that's what's unique about mindfulness.

283
00:27:53,000 --> 00:27:56,440
The noting is just a tool.

284
00:27:56,440 --> 00:28:03,160
So it was a hard question to really answer directly, I mean I could have given a simple answer,

285
00:28:03,160 --> 00:28:07,400
but it wouldn't suit the purpose I don't think.

286
00:28:07,400 --> 00:28:14,400
Most important is to understand that mindfulness is a tool, or the noting, the mantra is a tool

287
00:28:14,400 --> 00:28:20,080
that evokes potentially positive mind-stings, not to worry too much about the details

288
00:28:20,080 --> 00:28:24,880
of am I noting everything, do I have to note this, you don't have to note everything,

289
00:28:24,880 --> 00:28:28,280
you don't have to be exact.

290
00:28:28,280 --> 00:28:33,760
Like Mahasi Sayada talks about this, people are critical when you say to yourself sitting,

291
00:28:33,760 --> 00:28:37,320
sitting, sitting isn't real, right?

292
00:28:37,320 --> 00:28:42,920
But it's not even the sitting, it's not even conceiving of it as sitting, it's that sitting

293
00:28:42,920 --> 00:28:46,840
when you say to yourself, it brings you back to the experience, what is the experience,

294
00:28:46,840 --> 00:28:52,320
whether it's pressure on your bottom, intention in the back, in the light, and all of

295
00:28:52,320 --> 00:28:57,560
that is real, and so saying to yourself, sitting evokes this, sitting isn't saying this

296
00:28:57,560 --> 00:29:01,840
is good or this is bad, it's a very objective sort of thing to say.

297
00:29:01,840 --> 00:29:07,120
And so even though it's conceptual, and sitting itself is conceptual, then the mantra

298
00:29:07,120 --> 00:29:12,160
is one that helps you relate back to reality, and anyone who's critical of it should

299
00:29:12,160 --> 00:29:18,520
look at what the Buddha himself said, he said, when sitting, no clearly to yourself, sitting,

300
00:29:18,520 --> 00:29:22,040
I'm sitting, and you see no, and you see no, and you see no, and you see no, and you

301
00:29:22,040 --> 00:29:25,640
see, and so on.

302
00:29:25,640 --> 00:29:33,920
So this is theory, and this is me giving a talk on this, the result, and the benefit,

303
00:29:33,920 --> 00:29:42,200
and the real, real important part is that we all put this into practice, right here

304
00:29:42,200 --> 00:29:47,160
and now you can be aware that you're sitting, and you can use this mantra as a very useful

305
00:29:47,160 --> 00:29:48,160
tool.

306
00:29:48,160 --> 00:29:56,280
Don't let the tool, what's the, is there an adage or an idian about this, don't let

307
00:29:56,280 --> 00:29:58,880
the tool become the work, right?

308
00:29:58,880 --> 00:30:01,360
Maybe I just coined it.

309
00:30:01,360 --> 00:30:09,720
Don't let the work, don't let the tool become the work, the tool is what you use, don't

310
00:30:09,720 --> 00:30:17,880
let the tool become the, the object, so don't worry too much, is it working?

311
00:30:17,880 --> 00:30:18,880
Why isn't it working?

312
00:30:18,880 --> 00:30:22,360
Why do I say pain, pain, and the pain doesn't go away or so on?

313
00:30:22,360 --> 00:30:29,360
Why am I not getting what I expected, what I expect results, why am I not getting them,

314
00:30:29,360 --> 00:30:35,920
and not getting good results because of expectation, that kind of thing.

315
00:30:35,920 --> 00:30:43,520
Use mindfulness as a tool, this is what we should do, and you'll find that this is what

316
00:30:43,520 --> 00:30:46,880
it, this is what it does, and this is what it's meant to do, so it's important to understand

317
00:30:46,880 --> 00:30:47,880
this.

318
00:30:47,880 --> 00:30:54,840
All this talk, you might say, oh, this monk talks a lot, it's all just meant to help you

319
00:30:54,840 --> 00:30:59,920
understand, that's what abstraction is for, it's to help you change the way you might

320
00:30:59,920 --> 00:31:05,120
look at things or give you a new way of looking at things, a new perspective, new tools,

321
00:31:05,120 --> 00:31:06,120
right?

322
00:31:06,120 --> 00:31:13,880
To deal with reality, and I teach things that lead to more abstraction are generally not

323
00:31:13,880 --> 00:31:19,000
what we're trying to pass on, I'm not trying to make you think more about this or take

324
00:31:19,000 --> 00:31:26,000
your soul and ponder it and debate it or question it philosophically.

325
00:31:26,000 --> 00:31:32,400
I'm really just trying to say, you know, when you say to yourself, pain, pain, it helps

326
00:31:32,400 --> 00:31:36,680
you see the pain more clearly, so you do that, don't overthink it.

327
00:31:36,680 --> 00:31:43,760
So there you go, there's my video, I'm in Florida, I think you can see that, so I'll have

328
00:31:43,760 --> 00:31:50,760
some time to do more regular videos, but this month I'm traveling, so there will be days

329
00:31:50,760 --> 00:31:57,480
and even periods where I won't be able to, where the internet might not be good enough.

330
00:31:57,480 --> 00:32:05,320
I do plan, as far as one can plan in the future, to do more videos, and to keep this up,

331
00:32:05,320 --> 00:32:11,960
but anyone with expectations of videos out there, so something you should be mindful of,

332
00:32:11,960 --> 00:32:17,120
something one thing should feel, annoyed at this month's crew, doesn't have a regular

333
00:32:17,120 --> 00:32:23,280
schedule, well, maybe that's in a way a good thing, because it helps you see your

334
00:32:23,280 --> 00:32:42,880
catching.

