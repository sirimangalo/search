1
00:00:00,000 --> 00:00:09,440
Good evening everyone broadcasting live October 26th, 2015.

2
00:00:09,440 --> 00:00:16,440
Tonight we're not going to do the Damapada, it's feeling a little bit under the weather.

3
00:00:16,440 --> 00:00:23,120
I don't think I'm sick, but I didn't get around to looking at the Damapada and probably

4
00:00:23,120 --> 00:00:29,840
going to call it an early night and there's a couple of people visiting here and downstairs.

5
00:00:29,840 --> 00:00:35,440
Yeah, nothing tonight and tomorrow night as well, tomorrow is the full moon.

6
00:00:35,440 --> 00:00:39,280
So officially end of the rains.

7
00:00:39,280 --> 00:00:48,040
We enter into the cold season officially and tomorrow evening I will be at Stony Creek

8
00:00:48,040 --> 00:00:56,000
doing an official ceremony to with the other monks there to leave the rains.

9
00:00:56,000 --> 00:01:00,560
It's not even a ceremony to leave the rains, it's just we ask forgiveness from each

10
00:01:00,560 --> 00:01:01,560
other basically.

11
00:01:01,560 --> 00:01:10,120
We give a permission to the others to criticize us, it's kind of a nice ceremony.

12
00:01:10,120 --> 00:01:15,720
Maybe I'll take them up on it and start criticizing them, something you never do.

13
00:01:15,720 --> 00:01:20,240
No one ever actually goes ahead and criticize us, but I think I'll maybe joke about it

14
00:01:20,240 --> 00:01:26,680
or something with them, start telling them all their faults.

15
00:01:26,680 --> 00:01:31,680
We're talking all this talk about criticism, tomorrow's the one day of the year where it's

16
00:01:31,680 --> 00:01:35,920
open season on everybody, you're supposed to be allowed to criticize each other because

17
00:01:35,920 --> 00:01:47,120
you invite each other to criticize, but no they're good guys, there's nothing major to criticize.

18
00:01:47,120 --> 00:01:52,560
So we'll take some questions, I don't think there's that many, we've got a few here and

19
00:01:52,560 --> 00:01:53,560
then yeah.

20
00:01:53,560 --> 00:02:04,080
I have a question on the, you know, where you ask forgiveness, is it, is it actually

21
00:02:04,080 --> 00:02:11,880
what your forgiveness or is it more important for the monk to put that on your criticism?

22
00:02:11,880 --> 00:02:15,360
We've got a lot of rebirth.

23
00:02:15,360 --> 00:02:22,480
You're using open speakers as well, I'm sorry, I didn't generally catch what you said,

24
00:02:22,480 --> 00:02:26,480
but I need to have it on because I need to record what you're asking.

25
00:02:26,480 --> 00:02:29,000
Let me turn my depth.

26
00:02:29,000 --> 00:02:36,960
Yeah, I'm just wondering what the, what the intention is with when you're asking, when

27
00:02:36,960 --> 00:02:44,280
you're opening yourself up your criticism is it, you know, what is the intention of that?

28
00:02:44,280 --> 00:02:51,640
I'll do and to end the reins on a good note so that there's no hard feelings and no pent-up

29
00:02:51,640 --> 00:03:01,400
resentment, just to be accountable to each other and to clear the air, because you've spent

30
00:03:01,400 --> 00:03:06,520
three months together, theoretically, I actually haven't, I've spent a lot of it away,

31
00:03:06,520 --> 00:03:13,360
but having spent three months together usually, a lot of stuff can happen, you know you

32
00:03:13,360 --> 00:03:22,120
can get, you can build up animosity, so in the end you invite each other to clear the

33
00:03:22,120 --> 00:03:27,360
air, you accept, other people, the problems other people have had, you listen and you accept

34
00:03:27,360 --> 00:03:31,360
and let it go.

35
00:03:31,360 --> 00:03:46,360
Do you have some questions, I only have you have an Apple iPad, but you do have Chrome,

36
00:03:46,360 --> 00:03:57,040
would I equal to you, would I be able to meet with this, what I also need you?

37
00:03:57,040 --> 00:04:06,520
You can, I think you can get Hangouts for iOS, so you don't use Chrome, you'll use Hangouts,

38
00:04:06,520 --> 00:04:12,400
Google Hangouts, so as long as they have that for Chrome, which I think there is, and

39
00:04:12,400 --> 00:04:28,800
I'm going to talk to her, but I didn't talk to her, so that worked out fine.

40
00:04:28,800 --> 00:04:55,520
It's not normal, but we're not concerned so much about normal, it's real, and that's all

41
00:04:55,520 --> 00:05:01,120
that's important, so it's a sound, and you should say hearing, hearing, there's not really

42
00:05:01,120 --> 00:05:10,960
a normal, what would that mean, everyone gets it, everyone has different conditions, so everyone

43
00:05:10,960 --> 00:05:15,160
will ask that sort of question, is this normal, there really is no normal, there's you and

44
00:05:15,160 --> 00:05:20,880
there's what you're experiencing, so it doesn't matter whether it's normal, a normal

45
00:05:20,880 --> 00:05:29,000
abnormal is just a judgment call, when you hear, you should say hearing, hearing, just

46
00:05:29,000 --> 00:05:34,720
let it go, knowledge it for some time, if it doesn't go away, then just ignore it, and

47
00:05:34,720 --> 00:05:43,000
then if it drags your attention back later, you can always go back and acknowledge it again.

48
00:05:43,000 --> 00:05:48,240
If you dislike it, or you like it, or whatever, worry about it afraid of it, should acknowledge

49
00:05:48,240 --> 00:05:54,640
all of that as well.

50
00:05:54,640 --> 00:05:59,480
Could the monks who live in the forest feed in your animals attend?

51
00:05:59,480 --> 00:06:14,720
Yeah, pretty obvious, isn't it, I guess, is there more to that, because the monk, since

52
00:06:14,720 --> 00:06:19,600
the monks, their state of mind is more calm, so they're less liable to be attacked by animals,

53
00:06:19,600 --> 00:06:33,000
but it certainly happens, crushed by an elephant, gored by a wild pig eaten by a lion or

54
00:06:33,000 --> 00:06:45,440
a tiger, killed by mosquitoes, a lot of death by mosquitoes, just about half an inch

55
00:06:45,440 --> 00:06:48,120
of an inch of an eight, four sponges, is it happening?

56
00:06:48,120 --> 00:06:55,480
There aren't that many forest monks, but there's also not that many wild animals anymore,

57
00:06:55,480 --> 00:07:02,800
so depends which country, Sri Lanka still had some, but snakes would probably be

58
00:07:02,800 --> 00:07:08,640
mosquitoes, I don't know about death through mosquitoes, but yeah, there's probably death

59
00:07:08,640 --> 00:07:15,480
by mosquitoes, death by snakebite, and death by disease, probably the worst.

60
00:07:15,480 --> 00:07:21,920
There's a lot of bacteria in the rainforests, maybe not the most, but it is up there,

61
00:07:21,920 --> 00:07:34,720
like infection and fungus, it's just really awful stuff in the rainforests.

62
00:07:34,720 --> 00:07:58,760
They go to the Buddhist, it's not going to be good to let them know, is this a group of

63
00:07:58,760 --> 00:08:25,480
people from other traditions coming to meditate here, it really should be our tradition,

64
00:08:25,480 --> 00:08:27,520
that's my feeling.

65
00:08:27,520 --> 00:08:35,080
I think I'd rather stick to our tradition, but if they want to listen to the livestream

66
00:08:35,080 --> 00:08:43,280
that's absolutely welcome to, I mean we haven't required it, but I think we'd encourage

67
00:08:43,280 --> 00:08:50,040
that if you're coming here you're practicing our tradition, that seems, I don't see a reason

68
00:08:50,040 --> 00:08:58,840
to go outside of that, because it will just get weird otherwise, especially in the chat

69
00:08:58,840 --> 00:09:28,040
in the next few weeks, you'd have to be the students who are watching here, with a current

70
00:09:28,040 --> 00:09:29,040
video.

71
00:09:29,040 --> 00:09:38,000
I just came on to say hello tonight, thank you, guys, but thanks for showing up, thanks

72
00:09:38,000 --> 00:09:42,320
for meditating with us, keep up the good work.

73
00:09:42,320 --> 00:09:53,200
Darwin, I'll try tomorrow, maybe tomorrow, you have a session, what time is that?

74
00:09:53,200 --> 00:10:08,160
I'm going to go to Darwin session in second life, I mean if you can post it in the chat

75
00:10:08,160 --> 00:10:37,880
there let us know, I'll try to make it, okay, anyway, goodnight everyone, goodnight

