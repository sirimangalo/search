1
00:00:00,000 --> 00:00:16,080
Okay, good evening, everyone.

2
00:00:16,080 --> 00:00:33,360
I'm casting my mark for a minute.

3
00:00:33,360 --> 00:00:45,440
Today's quote is about sand on a fingernail.

4
00:00:45,440 --> 00:00:58,400
So I'm going to like to not like to do the Buddha, was accustomed to or was...

5
00:00:58,400 --> 00:01:12,480
He was really good at finding examples in nature, and coming up with comparisons between

6
00:01:12,480 --> 00:01:32,360
the nature of the world, the natural world, and the nature of reality, so I'm using simple

7
00:01:32,360 --> 00:01:40,000
examples in nature, so it's a very common tool.

8
00:01:40,000 --> 00:01:53,760
So one day he picked up a few grains of sand on his fingernail, and he asked Ananda,

9
00:01:53,760 --> 00:02:00,800
Ananda, he asked a bunch of bikhus.

10
00:02:00,800 --> 00:02:25,880
He asked a bunch of bikhus.

11
00:02:25,880 --> 00:02:43,320
The little bit of sand that I've put on my fingernail, lah, or ayeongmaha, but the...or the scrader.

12
00:02:43,320 --> 00:02:52,200
He used the simile for different things, in one place he talks about, not exactly the simile

13
00:02:52,200 --> 00:03:12,520
but he says this, even a little bit of...there's one place where he talks about a little

14
00:03:12,520 --> 00:03:21,320
bit of feces, a little bit of manure cow down, or human down, excrement, a little bit

15
00:03:21,320 --> 00:03:22,320
of excrement.

16
00:03:22,320 --> 00:03:28,000
You say, if you have a little bit of excrement, is it any better than a lot of excrement?

17
00:03:28,000 --> 00:03:35,040
It's a little bit of excrement, suddenly beautiful, suddenly wonderful, and I said, no, even

18
00:03:35,040 --> 00:03:42,280
a little bit of excrement is a bad thing, and the Buddha said, oh, in the same way, even

19
00:03:42,280 --> 00:03:52,480
a little bit of suffering, a little bit of samsar, a little bit of rebirth, and it's

20
00:03:52,480 --> 00:03:59,480
still bad, still not the way, still not a goal, or something you should look for, should

21
00:03:59,480 --> 00:04:00,480
seek for.

22
00:04:00,480 --> 00:04:08,400
But here he uses this little bit to say that's like the number of beings that are born

23
00:04:08,400 --> 00:04:15,240
as humans, being born as a human is a rare thing, the number of beings that are born

24
00:04:15,240 --> 00:04:35,400
as animals, as insects, or rodents, birds, and even the higher mammals, fish, all these

25
00:04:35,400 --> 00:04:42,640
various types of animals, it's like the great earth, it's far, far more than the only

26
00:04:42,640 --> 00:04:54,560
a few humans.

27
00:04:54,560 --> 00:05:03,440
And even what he doesn't say here, what he says elsewhere is, apakad-doma-nusi-sul-yi-jena-pa-raganu.

28
00:05:03,440 --> 00:05:11,440
You are a few of the humans who actually make it to a good destination, who actually

29
00:05:11,440 --> 00:05:14,640
had it in the right direction.

30
00:05:14,640 --> 00:05:20,200
So being born as a human being is like winning a lottery, and then most of us just

31
00:05:20,200 --> 00:05:33,920
wander it, wasted, it's like just a coincidence, and then it's gone, back into the, back

32
00:05:33,920 --> 00:05:42,600
into the pool of some sara.

33
00:05:42,600 --> 00:05:53,720
So there are many ways that we have to remember to take advantage of being born as a human

34
00:05:53,720 --> 00:05:54,720
being.

35
00:05:54,720 --> 00:05:56,960
We have to act like human beings.

36
00:05:56,960 --> 00:06:05,520
There are human beings that act like hell beings, there are human beings that act like

37
00:06:05,520 --> 00:06:11,360
ghosts, there are human beings that act like animals, and that's where they're going,

38
00:06:11,360 --> 00:06:14,040
that's where they're heading.

39
00:06:14,040 --> 00:06:26,040
Someone who is full of anger and hatred, it's like there are a demon on earth, a person

40
00:06:26,040 --> 00:06:34,120
who is full of greed, wanting this, wanting that can overcome their wanting, it's like

41
00:06:34,120 --> 00:06:50,120
a ghost, a hungry ghost, wailing, wanting a never satisfied, like in the Christmas

42
00:06:50,120 --> 00:07:04,200
Carol, Marily Strudges partner, destined to walk here, never satisfied, because he was full

43
00:07:04,200 --> 00:07:18,920
of greed, when he was alive, beings are full of delusion, arrogant and conceited, just

44
00:07:18,920 --> 00:07:25,600
like cats, you know, cats are so arrogant, they can see that there are many of them, and

45
00:07:25,600 --> 00:07:31,760
dogs are so dire, just ignorant and kind of dumb.

46
00:07:31,760 --> 00:07:42,040
So if we're keen on ignorance and arrogance and conceit and all these things, if we're

47
00:07:42,040 --> 00:07:51,040
caught up in drugs and alcohol, the looting and the fuddling our minds, it's just like

48
00:07:51,040 --> 00:08:01,000
being an animal, it's like being an animal on a human, a human animal, but then there

49
00:08:01,000 --> 00:08:07,720
are other humans that are like angels, humans like that are like gods, there are other

50
00:08:07,720 --> 00:08:17,520
humans that are like good Buddhas, these are the ones that we aim for, and there are ways

51
00:08:17,520 --> 00:08:23,760
as a human being to be like an angel or a god or even a Buddha, we may not be a Buddha,

52
00:08:23,760 --> 00:08:34,800
but we can be like one, we follow the Buddha's example, we cultivate mindfulness, clarity,

53
00:08:34,800 --> 00:08:45,280
wisdom, we have to act, we have to be mindful of our position as we think, I've been

54
00:08:45,280 --> 00:08:53,120
getting calls lately, and I have to say something, it's really, religion really does

55
00:08:53,120 --> 00:09:03,040
strange things to people, spirituality, sometimes it's just kind of funny, because people

56
00:09:03,040 --> 00:09:10,080
come up to me all the time and say the wackiest things, and the emails again, and now

57
00:09:10,080 --> 00:09:19,680
the phone calls I'm getting, but sometimes it's, you have to be mindful of yourself,

58
00:09:19,680 --> 00:09:26,280
if someone called me up today, not to really, not to get upset or anything, but just

59
00:09:26,280 --> 00:09:33,000
to talk about this, called me up today, and just asked if this was the minute, the international

60
00:09:33,000 --> 00:09:44,040
meditation center, I think, and without introducing himself, without asking how I was,

61
00:09:44,040 --> 00:09:50,080
if I've got time to talk, can I answer a question, just started asking me questions about

62
00:09:50,080 --> 00:09:59,440
monks and why monks weren't keeping the rules and why I'm not doing my videos, I didn't

63
00:09:59,440 --> 00:10:09,040
want to answer him, I didn't feel comfortable, because I didn't feel like he was mindful,

64
00:10:09,040 --> 00:10:18,200
that's not how you start a conversation, when people come up, when someone comes to you,

65
00:10:18,200 --> 00:10:23,600
when you approach someone, you should ask how they are, you should introduce yourself,

66
00:10:23,600 --> 00:10:27,840
and you should be mindful of the fact that you don't know this person, and so to ask

67
00:10:27,840 --> 00:10:35,000
them deep questions about this, you know, not deep, but personal questions really about

68
00:10:35,000 --> 00:10:43,640
monastic life and why monks this and that, without ever even, without even telling me your

69
00:10:43,640 --> 00:10:51,240
name, and I asked these guys, do I know you or so, and anyway, it wasn't a big, I just,

70
00:10:51,240 --> 00:10:57,240
I thought afterwards, because I didn't answer, and I was just quite quiet, and he started

71
00:10:57,240 --> 00:11:09,680
laughing at me, and you know, I just, that's not, I'm not trying to come on your

72
00:11:09,680 --> 00:11:18,760
event, but I know that this kind of attitude, sometimes we're not mindful, and we have

73
00:11:18,760 --> 00:11:25,040
to be mindful, because I think it's because it's out of our realm, you know, we get good

74
00:11:25,040 --> 00:11:32,680
at, we're good at being human in our, in the frame of reference that we have, so when

75
00:11:32,680 --> 00:11:40,680
we go to work or school, we get good at playing the game, so we get good at saying the

76
00:11:40,680 --> 00:11:47,120
right things, and as a monk, I, I'm proficient at being a monk and proficient at it, you

77
00:11:47,120 --> 00:11:53,920
know, when you're taken out of your, your comfort zone, it's familiar, and you don't

78
00:11:53,920 --> 00:12:02,320
have, you can't rely upon habit, I think this goes for a lot of situations, you know, when

79
00:12:02,320 --> 00:12:10,040
you're sick, people can be very irritable and, and unmindful when they're sick, because

80
00:12:10,040 --> 00:12:16,000
it's a situation that they're not accustomed to, when we're surrounded by people, or

81
00:12:16,000 --> 00:12:27,280
we're in public, this is the, by the importance of understanding mindfulness, it's not,

82
00:12:27,280 --> 00:12:31,120
it's quite different from habit, it's different from concentration, it's not something

83
00:12:31,120 --> 00:12:35,240
you can build up, something you have to be, or you can build it up, but you build

84
00:12:35,240 --> 00:12:41,120
it as a skill, and then you use it, you have to be used it, you have to be mindful,

85
00:12:41,120 --> 00:12:50,960
you have to remember yourself, and so this is, let this be a lesson, it's an interesting

86
00:12:50,960 --> 00:13:03,080
lesson, acting, acting appropriately, when you talk to people, you engage with people.

87
00:13:03,080 --> 00:13:09,760
This is kind of, kind of a thing that's not related to being, not related to meditation,

88
00:13:09,760 --> 00:13:15,600
but also very much related to meditation, it means how we behave in the world around us,

89
00:13:15,600 --> 00:13:23,800
the things we do, the things we say, it's not meditation, and people often ask me questions

90
00:13:23,800 --> 00:13:31,160
about, what should I do in this situation, how should I behave as a human being in this situation,

91
00:13:31,160 --> 00:13:38,920
and a situation, and it's, these are difficult questions to answer, people are expecting

92
00:13:38,920 --> 00:13:45,560
a solution to their problems, but it's very much, you know, so it's unrelated to the

93
00:13:45,560 --> 00:13:51,000
meditation, but very much related to meditation, because I can't give you the answer, that's

94
00:13:51,000 --> 00:14:01,440
not how it works, the, the, the, the requirement is a foundation that is capable of dealing

95
00:14:01,440 --> 00:14:06,000
with your own problems, you know, you have to find the solution for yourself, that's the

96
00:14:06,000 --> 00:14:11,480
only way it works, right, tell you the answer, and then you go and do this or do that,

97
00:14:11,480 --> 00:14:17,720
it's, it's artificial, you see, and whatever you say or whatever you do, it's not from

98
00:14:17,720 --> 00:14:27,520
the heart, it's not, you know, so it requires mindfulness as a basis, it's why it's so important,

99
00:14:27,520 --> 00:14:35,000
you want to truly be a human, truly live as a human being, you need mindfulness as the

100
00:14:35,000 --> 00:14:42,000
day, you always, always come back to mind, or set the mindfulness may not be the best

101
00:14:42,000 --> 00:14:57,000
word, right, I was, I meditated earlier and then somehow the timer didn't work, so I

102
00:14:57,000 --> 00:15:03,600
clicked it late, so I think it says I'm just finishing meditation, I did my hour, if you're

103
00:15:03,600 --> 00:15:09,320
wondering why I'm supposed to be meditating when I'm doing it, because we've got everybody

104
00:15:09,320 --> 00:15:16,760
signs in from when they're going to meditate, and I was doing meditation, I got an hour

105
00:15:16,760 --> 00:15:24,360
or over an hour ago, and then I didn't click for some reason, so I, after I did walking,

106
00:15:24,360 --> 00:15:32,680
I checked it, and then I clicked it, so it's a half an hour late, I'm not just clicking

107
00:15:32,680 --> 00:15:44,200
in the not meditating, that's the danger with this, I can't tell whether people are actually

108
00:15:44,200 --> 00:15:58,720
meditating, I don't know why anyone would, anyway, so yeah, that's the quote for this

109
00:15:58,720 --> 00:16:10,840
evening, it's given up the power, it's the power, apakade, sata, yamanu, sisupa, jaya,

110
00:16:10,840 --> 00:16:23,320
even so amongst few of those beings that are reborn as humans, at the go, et, y wa

111
00:16:23,320 --> 00:16:32,160
bho, dara, sata, yi, an yatra, mnosei, but jaya, and far more of those beings that are

112
00:16:32,160 --> 00:16:42,840
born as something other, you know, tasmati, habikabe, evang sikitabung, therefore amongst, here

113
00:16:42,840 --> 00:17:01,480
you should train das, apakmata, yhadesama, we will dwell with apamada, apamata, not intoxicated,

114
00:17:01,480 --> 00:17:16,120
not confused in the mind, mindful basically, a one evil bikabe sikitabung, that should you train

115
00:17:16,120 --> 00:17:32,800
nakasikasupa, the fingernail, nakasika, what is sikka, nakka, nakka is fingernail, sikka, on the top the

116
00:17:32,800 --> 00:17:47,880
tip, nakasikas, the tip of the fingernail, so that's the dhamma for tonight, and if people

117
00:17:47,880 --> 00:17:56,960
have questions, I am going to now post the hangout, so you welcome to come and ask questions

118
00:17:56,960 --> 00:18:04,200
there, and I expect anyone who comes on to be mindful and to introduce themselves and

119
00:18:04,200 --> 00:18:11,760
to be respectful and mindful of the sorts of questions that should be asked, I'm not

120
00:18:11,760 --> 00:18:28,120
sure, but I've scared everyone away, everyone there's come on, I'm sure, it's just strange

121
00:18:28,120 --> 00:18:31,600
that people, there's a certain familiarity where people would just come up to me and start

122
00:18:31,600 --> 00:18:43,160
talking, someone at university, I think I mentioned this, came up to me and said, look,

123
00:18:43,160 --> 00:18:47,200
you can't walk around wearing that kind of clothes and not have people want to come up

124
00:18:47,200 --> 00:18:51,800
and ask you about it, and expect people to not come up and ask you, and I turned to

125
00:18:51,800 --> 00:19:03,120
me, and why not, I think, mind your own business, with that I turned out to be quite

126
00:19:03,120 --> 00:19:14,760
nice, he was actually quite respectful, recently another person called us from a radio station

127
00:19:14,760 --> 00:19:20,760
and they want to come by tomorrow, they're going to come by at 11 tomorrow, which is

128
00:19:20,760 --> 00:19:29,280
a bad timing, it's right when we're eating, but when they want to ask us about our program,

129
00:19:29,280 --> 00:19:38,600
you can basically give us free publicity, which is awesome, very kind of, so there are people

130
00:19:38,600 --> 00:19:44,920
who are starting to find out about us, they just have to be ready for it, and I think

131
00:19:44,920 --> 00:19:51,360
I just start screening my calls, because this person wasn't very happy with me in the end,

132
00:19:51,360 --> 00:19:58,800
I don't think, it wasn't answering, you see, as common for, it was common, there's stories

133
00:19:58,800 --> 00:20:05,760
of monks, or aunts, even, who wouldn't answer questions, they just wouldn't say anything,

134
00:20:05,760 --> 00:20:13,360
and a question, or when someone was saying something to them, that just wasn't, it's because

135
00:20:13,360 --> 00:20:22,600
of indifference, monks are not like the Buddha, so I've done this before, and people can

136
00:20:22,600 --> 00:20:27,960
be very upset at you for not answering their questions, because they're so used to, I mean,

137
00:20:27,960 --> 00:20:35,440
social, the social way is to have someone, when you talk to someone, you should answer

138
00:20:35,440 --> 00:20:43,320
them, and so this person was saying today, it was just an interesting point, he was

139
00:20:43,320 --> 00:20:51,760
saying, I've talked to a lot of people, and I've never had someone not answer, just

140
00:20:51,760 --> 00:20:58,160
not say anything, and I didn't reply to that, and then he said, I'm not sure it's

141
00:20:58,160 --> 00:21:07,360
what the Buddha would have done, or basically would have wanted or something, basically

142
00:21:07,360 --> 00:21:13,200
criticizing and saying, this is what you're doing is wrong, but the funny thing is

143
00:21:13,200 --> 00:21:14,200
why do I care?

144
00:21:14,200 --> 00:21:17,120
You know, why does it matter to me what you think, it doesn't?

145
00:21:17,120 --> 00:21:24,360
I mean, something about being a monk, that we're very much outside of this, I didn't

146
00:21:24,360 --> 00:21:32,240
come to you, you called me, so it's an interesting point for me as a monk to be in this

147
00:21:32,240 --> 00:21:38,280
sort of situation, but one interesting aspect of it is the power that we have, we're not

148
00:21:38,280 --> 00:21:47,760
required to, you know, how sometimes you play into other people's games, bait, you know,

149
00:21:47,760 --> 00:21:53,240
bait and switch, you've heard of this idea of bait and switch, or someone baits you with

150
00:21:53,240 --> 00:21:59,000
something, and then if you get into it with them, then they start on something else,

151
00:21:59,000 --> 00:22:08,960
and they drag you on, and into something that's often quite unwholesome, and useless,

152
00:22:08,960 --> 00:22:19,000
speed, and not have, we don't have the responsibility to play other people's games, I guess

153
00:22:19,000 --> 00:22:22,920
is the general point that I'm making, it's interesting to me.

154
00:22:22,920 --> 00:22:32,080
I think as a monk, I have that luxury not having to play into people's games, and it's

155
00:22:32,080 --> 00:22:40,160
really a power of having left the world, because there's something you can do to me.

156
00:22:40,160 --> 00:22:48,040
I'm happy to help, I don't know this guy, if you want to learn meditation, I'm happy

157
00:22:48,040 --> 00:23:01,560
to help, but that was really interesting to me.

158
00:23:01,560 --> 00:23:12,800
We have a great power in mindfulness in not worrying, not clinging, not needing anything

159
00:23:12,800 --> 00:23:27,320
from other people, anyway, enough talk, thank you all, I guess there's no question, so

160
00:23:27,320 --> 00:23:47,320
that's all for this evening, I'm sure you all have a good practice, good night.

