1
00:00:00,000 --> 00:00:07,000
The main difference is between meditation and prayer.

2
00:00:07,000 --> 00:00:14,000
Anybody?

3
00:00:14,000 --> 00:00:21,000
Meditation you're trying to quiet your mind in prayer?

4
00:00:21,000 --> 00:00:28,000
I'm talking about comma by meditation.

5
00:00:28,000 --> 00:00:35,000
The goal is to quiet your mind, not so much converse.

6
00:00:35,000 --> 00:00:42,000
I had the Christi Christians, evangelists trying to explain to me that in Christianity

7
00:00:42,000 --> 00:00:46,000
Christianity is objective.

8
00:00:46,000 --> 00:00:53,000
I'm assuming this is the meaning in terms of prayer because you're focusing on something

9
00:00:53,000 --> 00:01:00,000
outside of yourself, which is objective, so God is objective.

10
00:01:00,000 --> 00:01:04,000
Of course, I turned the tables on them and said, no, he's not.

11
00:01:04,000 --> 00:01:10,000
They said, you're subjective because you're focusing on yourselves, which is subjective.

12
00:01:10,000 --> 00:01:13,000
But no, it's not.

13
00:01:13,000 --> 00:01:19,000
It's interesting how you can totally see reality from different perspectives.

14
00:01:19,000 --> 00:01:31,000
This evangelist who was telling me God is focusing on something outside of yourself is objective

15
00:01:31,000 --> 00:01:35,000
and focusing on something inside is subjective.

16
00:01:35,000 --> 00:01:41,000
And in Buddhism, of course, we believe the opposite that focusing on your own experience

17
00:01:41,000 --> 00:01:47,000
is objective, focusing on concept outside of yourself that you aren't experiencing

18
00:01:47,000 --> 00:01:50,000
is subjective.

19
00:01:50,000 --> 00:01:54,000
I said, you know, your concept of God is totally different from the Hindu concept of God,

20
00:01:54,000 --> 00:01:55,000
for example.

21
00:01:55,000 --> 00:01:57,000
So how objective is that?

22
00:01:57,000 --> 00:01:58,000
It's actually quite subjective.

23
00:01:58,000 --> 00:02:03,000
You believe God to be this and this and this and so on, and other people believe otherwise.

24
00:02:03,000 --> 00:02:06,000
But experience is what it is.

25
00:02:06,000 --> 00:02:11,000
You see, I see seeing as seeing no matter who sees it.

26
00:02:11,000 --> 00:02:19,000
So maybe that's a good answer to this question, how prayer is subjective or objective,

27
00:02:19,000 --> 00:02:22,000
depending on who you ask.

28
00:02:22,000 --> 00:02:27,000
And meditation is the opposite, again, depending on who you ask.

29
00:02:27,000 --> 00:02:31,000
And I guess how you pray, you know, is your type of prayer,

30
00:02:31,000 --> 00:02:35,000
one where you're just trying to be quiet and sense God,

31
00:02:35,000 --> 00:02:41,000
or trying to converse with God, and ask things of God.

32
00:02:41,000 --> 00:02:47,000
Because I have read accounts of Christians,

33
00:02:47,000 --> 00:02:52,000
reaching the states that I would describe as a genre,

34
00:02:52,000 --> 00:02:57,000
the way they describe it, just from being quiet,

35
00:02:57,000 --> 00:03:04,000
in their mind, got quiet and they had, and they experienced what seems to be

36
00:03:04,000 --> 00:03:06,000
like a genotype state.

37
00:03:06,000 --> 00:03:10,000
You know, they would have their mythology behind that.

38
00:03:10,000 --> 00:03:17,000
But I think it's certainly possible for a Christian to enter some of these

39
00:03:17,000 --> 00:03:24,000
common-bodied states that they get in their mind still enough.

40
00:03:24,000 --> 00:03:26,000
Indeed.

41
00:03:26,000 --> 00:03:33,000
So on a deeper level, it's a question of where your mind is.

42
00:03:33,000 --> 00:03:37,000
Prayer on a superficial level is quite different from meditation,

43
00:03:37,000 --> 00:03:41,000
because I think the word prayer means to ask or something,

44
00:03:41,000 --> 00:03:44,000
not to request something.

45
00:03:44,000 --> 00:03:46,000
Please, please that.

46
00:03:46,000 --> 00:03:49,000
That's, I think, a definition in the dictionary of prayer.

47
00:03:49,000 --> 00:03:54,000
But what is the purpose of communicating with God?

48
00:03:54,000 --> 00:03:59,000
If it's, as you say, in some cases, it's best not called prayer.

49
00:03:59,000 --> 00:04:02,000
It's better called meditation, because it quiets the mind.

50
00:04:02,000 --> 00:04:07,000
Of course, insight meditation is, I would still say,

51
00:04:07,000 --> 00:04:13,000
I don't think it could ever be compared to prayer.

52
00:04:13,000 --> 00:04:20,000
No, because, again, it's not, it's a different level.

53
00:04:20,000 --> 00:04:24,000
How would you describe the difference between insight meditation and prayer?

54
00:04:24,000 --> 00:04:27,000
Insight meditation specifically?

55
00:04:27,000 --> 00:04:32,000
The difference between insight meditation and prayer?

56
00:04:32,000 --> 00:04:38,000
Why prayer could never be considered insight meditation and vice versa?

57
00:04:38,000 --> 00:04:44,000
Because insight meditation, you're just looking at the bare experience of what is happening,

58
00:04:44,000 --> 00:04:55,000
you're not trying to communicate to God, you're just getting into

59
00:04:55,000 --> 00:04:58,000
the bare experience.

60
00:04:58,000 --> 00:05:01,000
And those are just two different things.

61
00:05:01,000 --> 00:05:06,000
Yeah.

62
00:05:06,000 --> 00:05:09,000
Yeah.

63
00:05:09,000 --> 00:05:15,000
Prayer, I think, in its essence, has a concept as its object,

64
00:05:15,000 --> 00:05:20,000
and by concept, if we don't want to be, use it in a pejorative sense,

65
00:05:20,000 --> 00:05:23,000
something that you're not experiencing.

66
00:05:23,000 --> 00:05:32,000
It's, you try to connect with something beyond your experience.

67
00:05:32,000 --> 00:05:38,000
And so from Buddhist point of view, it's essentially conceptual.

68
00:05:38,000 --> 00:05:41,000
I think that's the essence of the answer.

69
00:05:41,000 --> 00:05:48,000
In insight meditation, you focus on experience and only experience.

70
00:05:48,000 --> 00:05:54,000
For example, if you see God, it's just seeing, it's not really God.

71
00:05:54,000 --> 00:05:56,000
So that, there's a good example.

72
00:05:56,000 --> 00:05:58,000
If you see God, prayer would be to talk to him.

73
00:05:58,000 --> 00:06:01,000
Insight meditation would be to say seeing, seeing, seeing.

74
00:06:01,000 --> 00:06:23,000
It's great to write.

