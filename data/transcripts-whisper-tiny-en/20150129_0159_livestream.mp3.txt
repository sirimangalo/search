Okay, good morning everyone, broadcasting lives from Mayongsong, from the mountain, come
near to undertake more intensive practice in the forest, something that I think is not
well understood about Buddhist meditation, is that it's designed to change a person in a very
fundamental way, it's not meant to merely return you to an ordinary state where you can go
back and live your life as you would like, in many ways, will change the way you look at
the world, in other words it's not designed to make you an ordinary person, it's designed
to make you a somewhat special person, and so it can be quite shocking to realize the sort
of the depths and the difficulty of the practice, difficult not only in the resistance to the
practice but difficult in the resistance to change, a resistance to accept the magnitude
of our misunderstandings about the world, magnitude of the depth of our attachments, that
deep we have to go to actually become free from suffering, but on the other hand it's encouraging
to realize how I'll profound the practice really is, and how powerful it is, it would
have taught that it makes you, he said that the practice makes you a great being, you have
this word Mahantata, in Sanskrit it would be Mahatma, Mahatma in this word in India that
was used to describe Gandhi, but it was used to describe any great being, so if you think
of it in those terms, there's very few people who don't admire the image of God's
Gandhi or what, the ideal that he was supposed to represent, the greatness of mind, of
being Mahatma.
I think many people come, most people don't come to meditation thinking they're going
to become someone great or someone famous or powerful or just want to become, I'd best
want to become good people, but greatness doesn't have anything to do of course with
power or fame or renown, you can be a great person living off in the forest, but it's
important I think to think in this way because we shouldn't settle for mediocrity, meditation
won't allow you to, we should see the benefit of being a great person, a great being
of greatness, we should see the importance of true change and true spiritual enlightenment
as opposed to just meditating to feel good, to avoid our problems or to escape our problems
temporarily, if you want to succeed in meditation, you have to be open to change and true
change, true transformation, so the Buddha described what it takes to become a great person
and these are qualities that as meditators, we will do well to espouse and cultivate,
said anyone who has these qualities is sure to become a great person, we'll hunt that
as a great person, we'll become a great being a profound being, the first one is Anoka
One has to be full of wisdom, and number two yoga bahu no, only full of yoga.
Yoga here means energy or effort, and number three, weigh the bahu no.
That means feeling will be full of feeling of zero or zest or interest.
Asan tutti bahu no, will be full of discontent.
Kita dulones, and not give up one's effort or duties, not give up the work or the task.
Then we put it down.
Then Kuzaleh su-dhammeh su-tari-jah-pah-daliti, to always strive to attain greater and greater
whole su-das.
These six things are very dumbest that we want to become a great being.
So briefly, a lokabahu no means wisdom in the meditation practice we have to always be
clear in our minds, to use the wisdom that we gain to continue, to be wise and clever, to
be clear that the things that you experience are impermanent and unsatisfying uncontrollable,
because this is what you're going to see when you practice.
And if you can't recognize that, it becomes a source of stress and suffering and difficulty
in the practice.
When pain arises, the inability to see it as impermanent and unsatisfying, in the sense
that you can't fix it, you can't make it better, it doesn't belong to you.
You can't see this then it leads you to suffer and doubt about the practice and doubt
about your ability to practice.
The same goes with pleasant things if you experience pleasant sensations, pleasant experiences
and if you can't see them as transient, as unsatisfying, because they're transient,
because when they end and they're gone and all you're left with is your desire, the
desire builds but the pleasure doesn't, the pleasure fades, you can't see this then you become
discontent, more and more, wanting for pleasant sensations, pleasant experiences, less
able to maintain and sustain them to a level that satisfies you.
Aloka Maholo, you have to cultivate as much light, aloka means light, but it is light of wisdom,
your mind has to be bright and clear, to cultivate this clarity of mind.
The beginning when we practice our minds are very clouded and murky, we don't see so clearly.
It's hard to tell what's right, what's wrong, it's hard to tell what is the point of the
practice, but as you practice your mind becomes clearer, as your mind clears, you're able
to see the difference, you're able to see the benefits of clarity and you're able to
see the dangers in attachment and clinging and aversion, you see the causes and effect,
that leads to what, you start to see how your mind works, and you can adjust accordingly.
Yoga Maholo, you have to work hard, meditation doesn't meant to be easy, it's not something
you can do when you feel like it, you should try to be persistent in the practice, you
can do in a sense, have to push yourself to practice, otherwise laziness takes over
and your mind falls back into all the comfortable patterns, which is much more comfortable
and pleasant, but it doesn't stir up, it doesn't dig deep and uproot or bad habits.
You have to sit really in a dukama jati, you overcome suffering through effort, to work at it,
to work at it and you have to overcome your aversion to work, it makes us cranky when
we have to work, it's unpleasant, you have to overcome that, you have to become strong,
and natural, perfect state is actually quite energetic, but our ordinary state is usually
much more lazy, and so it feels somehow stressful to have to work, because we'd much rather
just lay us around and enjoy ourselves, but it's a trap you become more and more obsessed
with the pleasantness of lying down and sitting in comfortable chairs and seats, a great
person is able to work at an optimum level, it's very little rest, very little, very
little comfort, here's the point is to be objective, if you're truly objective then
you don't mind to be a state of comfort or discomfort, just embody you either way, but
our ordinary state is only content with comfort and pleasure, and when we don't have it
then we get upset and angry and frustrated, so clearly we're not objective, to be truly
objective you have to actually work hard, work hard to change that, to bring your mind
to a truly neutral ground, when you're able to be happy no matter what the situation,
whether you have comfort or discomfort, wait about to work hard to overcome unwholesomeness
in the mind, you have to work hard to cultivate wholesomeness, working yoga baholos,
or yoga baholos, wait a baholomines, you have to wait a means feeling, it means putting
your heart into something, you have to practice with your whole hardness, you can't just
force yourself to practice, you can't just be here because someone told you that you
should or because you think it's the right thing to do, if you don't really want to
do it, it's very difficult to meditate, to become great, you have to be passionate about
what you do, to some extent passionate about meditation, it just means you have to be clear
in your mind about the benefits of meditation, to be very careful because the mind will
trick you into thinking that you don't need to meditate or that it's too much work or that
it's too stressful or so, and trick you into thinking that meditation is not the right
way to go, you have to be clear in your mind the benefits, meditation helps to clear the
mind that meditation helps you to root out all mental, upset mental illness or mental disturbances,
it helps you overcome suffering, meditation clears up, the attachments that lead us to suffering
that causes us to fall into stress and suffering, meditation leads you to the right path,
allows you to live your life however you like without worry or stress or fear or anxiety
without doubt gives you a clear mind and meditation leads you to freedom, we have to put
our hearts into it, you can't do this half-hearted, you have to be very careful to
uproot doubt and uncertainty and bevelance, the uncaring mind that is uncommitted to
be committed to the practice, be in a battle, as unto the Baha'u'llow means you have to be
full of discontent, so discontent here, there's two kinds of contentment, we have to
of course cultivate contentment with our experiences, but it's a discontent, this means
in regards to goodness, it should never be content with what you have with who you are,
we content with, we content to keep the unwholesomeness of the existence of the mind saying
oh it's not so bad, or the goodness saying oh it's good enough, if you want to become
a great person and really succeed in the meditation you have to be willing to give up all
unwholesomeness, be ready to cultivate perfect wholesomeness, the great thing is that it is
possible, you can become a great person, it's there for us to do, it's there for us to
attain, so characteristic of a great person is that they don't rest on their laurels,
they continue to, a great person is recognized by their ability to see the smallest faults,
an ordinary person ignores their great faults, and dismisses them as being inconsequential,
as a great person is even disturbed by their small fault, the smallest fault, is a difference
between an ordinary person and a great person, Anikita duro means not to ever put down
the task, so to not take a break, in meditation there really is no need to take a break,
there's no benefit in taking a break, because it doesn't mean that you have to be doing
formal meditation, but it means there's no reason to give up mindfulness, because even
when you're not meditating, no matter what you're doing you can always work at cultivating
clarity of mind, when you're eating you can work to have a clear mind, you're taking
a shower or brushing your teeth, when you're on the toilet you can be working to clear
your mind, even when you lie down to sleep at night, you can be working to cultivate clarity
of mind, there's no excuse, there's no reason, there's no benefit to stopping there.
So at another, it's another quality of a great person that they don't, they don't give
up, and they don't take breaks, they're constantly working to improve themselves.
And the final one, Qousalaisu damesu dari chapat vaisi means not only are they working,
it's not a linear progress, it's just one important point to understand about meditation
that it's not something that you can just put your head down and plow on, you have to
be adaptive, to be able to adapt, adaptable, to be ready to adapt, to new situations,
utari chapat vaisi means working to further yourself, to greater goodness, always looking
for new ways to improve yourself, and in meditation this is a special, take special importance
because your conditions change, it's common for a meditator to become complacent thinking
they've figured it out, they've come to understand how to meditate and so they work
out a problem in the mind and then they try to apply the same technique to a new
problem and find that the results are different, or become complacent, you find that meditation
is like layers of an onion every, every time you figure something out you learn something
new about yourself, there's something new and a little bit different to learn, there's
always new and greater understanding to begin, and so we have to approach every experience,
every meditation state with an open mind, with a beginner's mind they've seen, ready
to learn something new, this quality of mind is important in meditation, the ability to adapt
to a new situation, to not take it for granted, because it's very easy for a meditator
to get into a rut, where they are meditating, but aren't progressing, because they're
ignoring new stimuli, new information, they'll be ignoring, they'll be very confident
in dealing with certain situations, certain emotions, but other emotions they've ignored
and they've never really learned how to deal with, so it just takes, it'll be among
the ability to discern and discriminate, not just work hard, but to be able to see, to
adjust your work, according to the task at hand, working to reach a new level of understanding,
it's not just effort, but you need wisdom and discernment, so some good demos, I think
these are useful for all meditators, whether we're here in the forest, practicing intensive
video, for people in the world to think about, and to cultivate, because in the end
that's the goal, that's the way we're headed to greatness, something that is well worth
appreciation, greatly appreciated to see so many people practice, to become better people,
so this is, these are some of the demos that help us do it, help us become not only good
people, but great, deep profoundly.
