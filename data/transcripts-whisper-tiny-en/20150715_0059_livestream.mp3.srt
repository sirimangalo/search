1
00:00:00,000 --> 00:00:14,500
Okay, in broadcasting life, today from Sunderland, Ontario, where I'm visiting with a family.

2
00:00:14,500 --> 00:00:23,360
Be here till Friday and then back to Stony Creek, Ontario, and then on Monday, the

3
00:00:23,360 --> 00:00:35,160
morning, 20th, we're planning on taking a road trip, maybe, up to a minute to an island,

4
00:00:35,160 --> 00:00:37,160
where I was born.

5
00:00:37,160 --> 00:00:51,840
I spent a few days revisiting, and when I just wanted to show the monks where I live, because

6
00:00:51,840 --> 00:00:57,920
they're always telling me about their homeland in southern Vietnam, but used to be a part

7
00:00:57,920 --> 00:00:58,920
of Cambodia.

8
00:00:58,920 --> 00:01:07,000
And I said, I should show you where I grew up, because they talk about how beautiful

9
00:01:07,000 --> 00:01:14,560
Ontario is, and this is nothing to see in my home.

10
00:01:14,560 --> 00:01:25,800
I want to show them something that's wrong, chance to get away, go off into the forest,

11
00:01:25,800 --> 00:01:28,960
and do some meditation up on the cliffs.

12
00:01:28,960 --> 00:01:31,960
Let's see.

13
00:01:31,960 --> 00:01:41,600
Anyway, just details of life, and everyone know what's happening with me.

14
00:01:41,600 --> 00:01:48,880
Today's reading, again, we're reading from the words, words of the Buddha, Buddha Vatsana

15
00:01:48,880 --> 00:01:57,680
by Damika, and today's reading is about livelihood, really.

16
00:01:57,680 --> 00:02:06,080
Well, I'm sorry we missed that yesterday, I missed yesterday, wasn't it?

17
00:02:06,080 --> 00:02:14,480
I'll try not to miss, try to be here every day, but I can't always promise.

18
00:02:14,480 --> 00:02:25,440
The way today's is about livelihood, someone's accusing, sorry, put the improper consumption

19
00:02:25,440 --> 00:02:38,400
of goods, and he says that he refrains from basically refrains from what would be considered

20
00:02:38,400 --> 00:02:55,440
a wrong livelihood for a monk, and therefore he is properly.

21
00:02:55,440 --> 00:03:08,400
It's obviously, this is a specific teaching for monks, but the general sense is clearly

22
00:03:08,400 --> 00:03:12,600
applicable in the playlist or anyone there, to everyone.

23
00:03:12,600 --> 00:03:22,560
Right livelihood is something livelihood in general, it's an unavoidable consequence

24
00:03:22,560 --> 00:03:27,640
of living in the world, you have to stay alive.

25
00:03:27,640 --> 00:03:36,520
In modern times this means a lot more than simply survival means taking part in systems

26
00:03:36,520 --> 00:03:50,480
and structures, organizations, getting a job, going to school, learning skills on this,

27
00:03:50,480 --> 00:03:59,680
engaging with society, voting, renew news, livelihood can be quite complex, just to stay

28
00:03:59,680 --> 00:04:12,600
alive to society, and so it is even more important that we understand the ethical

29
00:04:12,600 --> 00:04:22,760
ins and outs of livelihood, it's a part of our lives, a big part of our lives.

30
00:04:22,760 --> 00:04:30,760
So the Buddha gave instruction on the sorts of livelihood that are improper, and we've

31
00:04:30,760 --> 00:04:36,200
talked about this a bit on the internet, for example the Buddha said like selling weapons

32
00:04:36,200 --> 00:04:45,200
is considered wrong livelihood, but then what about working in a factory that makes

33
00:04:45,200 --> 00:04:59,320
weapons, and if hunting is wrong, what about this wife who, this woman who was married

34
00:04:59,320 --> 00:05:09,360
to a man who hunted and she cleaned all of his implements for it and cleaned his snares

35
00:05:09,360 --> 00:05:18,600
and so on, is that wrong livelihood?

36
00:05:18,600 --> 00:05:28,360
Of course, ultimately when we talk about right or wrong, we deal with intention, it isn't

37
00:05:28,360 --> 00:05:32,520
that, and there is no gray area, it's not hard to understand, if your mind is full of

38
00:05:32,520 --> 00:05:39,640
greed, anger, delusion, that's wrong, doesn't have anything to do with the actual acts.

39
00:05:39,640 --> 00:05:50,120
So this is why it's possible to work in a factory or clean implements of murder-based

40
00:05:50,120 --> 00:05:59,760
ism, without impure thoughts, without unholesing this, because your mind is simply performing

41
00:05:59,760 --> 00:06:02,800
an action.

42
00:06:02,800 --> 00:06:09,080
But when you sell something, you undertake, you make a decision what to sell, you make

43
00:06:09,080 --> 00:06:12,800
a decision to sell weapons, for example.

44
00:06:12,800 --> 00:06:19,840
So there is something there that's undeniably unholesing, working in a store, if you work

45
00:06:19,840 --> 00:06:25,680
in a store, as a clerk selling things, it's not the same.

46
00:06:25,680 --> 00:06:33,520
But if you open a liquor store or make a decision in a restaurant to sell alcohol, I think

47
00:06:33,520 --> 00:06:35,200
you can be considered comfortable.

48
00:06:35,200 --> 00:06:40,920
At this point of view, you've made a decision to sell things to people.

49
00:06:40,920 --> 00:06:55,000
If you work in a store, you could argue that you could argue that you're simply responding

50
00:06:55,000 --> 00:06:56,400
to orders.

51
00:06:56,400 --> 00:07:03,080
Someone says, I'd like to buy this, and if you just bring them up, give them, take

52
00:07:03,080 --> 00:07:07,640
their money, give their money, you're not actually handling or getting involved with

53
00:07:07,640 --> 00:07:12,160
the sale, and you're just facilitating.

54
00:07:12,160 --> 00:07:20,200
So we could say facilitating is also wrong, but I think it's possible for the intention

55
00:07:20,200 --> 00:07:26,400
to be pure in that case, for the person who's manufacturing weapons, they're simply

56
00:07:26,400 --> 00:07:28,000
putting stuff together.

57
00:07:28,000 --> 00:07:36,440
In fact, I think you could argue that selling weapons isn't necessarily unholesome, but

58
00:07:36,440 --> 00:07:41,000
I think it's the intention.

59
00:07:41,000 --> 00:07:47,360
What I mean is a person who has a weapons, in fact, owns weapons, in fact, or builds

60
00:07:47,360 --> 00:07:49,400
a weapons factory or whatever.

61
00:07:49,400 --> 00:07:57,600
It's not unholesome the whole time, just owning it isn't accumulating carbon necessarily.

62
00:07:57,600 --> 00:08:04,200
I mean, it doesn't seem like it, but the intentions and the decision and the decision

63
00:08:04,200 --> 00:08:19,040
to continue, anyway, the most important point is the intention, and so then we go to the

64
00:08:19,040 --> 00:08:27,160
next level of actions that lead to wrong livelihood, so the description of wrong livelihood

65
00:08:27,160 --> 00:08:34,680
is wrong livelihood that involves wrong action, wrong speech, wrong action, and wrong speech,

66
00:08:34,680 --> 00:08:42,680
our speech that is still based on the mind, and still the unholesome answer of the mind.

67
00:08:42,680 --> 00:08:50,560
So when you speak harshly or you speak to create a division between people to divide people,

68
00:08:50,560 --> 00:08:59,840
and even you use the speech, the point is that there's a mind behind it that is cultiving

69
00:08:59,840 --> 00:09:14,000
bad habits, it's not the actual speech that is wrong.

70
00:09:14,000 --> 00:09:19,880
Sometimes people say certain things to get out of the attention of other people, parents

71
00:09:19,880 --> 00:09:25,320
when they scold their children, sometimes there's, it can't, it's possible to do it without

72
00:09:25,320 --> 00:09:39,120
unholesome as teachers when they teach things they can do it with a good heart, so still

73
00:09:39,120 --> 00:09:47,720
it's our mind that we have to purify that, in the noble latefold path, we're talking

74
00:09:47,720 --> 00:09:54,520
about this kind of livelihood, right livelihood is that which is free from killing, from

75
00:09:54,520 --> 00:10:10,720
making your livelihood based on killing, stealing, lying, cheating, if you do any of these

76
00:10:10,720 --> 00:10:14,360
things to make a living that's wrong livelihood.

77
00:10:14,360 --> 00:10:24,360
But here in the suit that we have something a little bit more curious, it's livelihood

78
00:10:24,360 --> 00:10:36,960
based on desire, based on greed, or based on seeking, seeking out wealth or gain.

79
00:10:36,960 --> 00:10:41,360
That's what's really being discussed here in all these ways that religious people have

80
00:10:41,360 --> 00:10:45,400
to make a living.

81
00:10:45,400 --> 00:10:50,800
I want to live as a religious person, so I'm going to run errands for people in order

82
00:10:50,800 --> 00:10:56,840
to make my livelihood, or I'm going to tell fortune, so all these things that's

83
00:10:56,840 --> 00:11:12,800
very good to mention, right livelihood in its purest sense from monk is not livelihood at all,

84
00:11:12,800 --> 00:11:15,880
since it's not seeking out livelihood.

85
00:11:15,880 --> 00:11:19,360
We have the bare ability to wander through the village in the morning, and the idea is

86
00:11:19,360 --> 00:11:25,960
to wander through the village looking to see if anyone is already offering food.

87
00:11:25,960 --> 00:11:43,000
It wasn't begging, it wasn't something we would have created, it was just a convenient

88
00:11:43,000 --> 00:11:53,880
aspect of society that they support and religious people, and that's really what proper

89
00:11:53,880 --> 00:12:04,040
religious livelihood is, it's living off of the support of others, and the support means

90
00:12:04,040 --> 00:12:11,840
people who actually support what you do, not trying to find a way to gain livelihood,

91
00:12:11,840 --> 00:12:20,000
and living a life and behaving in such a way that people support you.

92
00:12:20,000 --> 00:12:28,640
This is for monks, you see, but there's something, they said it, this can be easily extrapolated.

93
00:12:28,640 --> 00:12:33,840
Take a person living a night, working a nine to five job in an office, they have options

94
00:12:33,840 --> 00:12:43,080
open to them, some people would take it as an opportunity to rise up and rise, and they

95
00:12:43,080 --> 00:12:56,560
might do unwholesome things together, say things manipulate others, scheme and plot,

96
00:12:56,560 --> 00:13:14,000
buy and cheat, to get no distance, be favored, for what to gain weight of livelihood?

97
00:13:14,000 --> 00:13:21,080
The point here is to be content, where some people, that's how it is, they work and

98
00:13:21,080 --> 00:13:29,800
they do it, to gain money, they don't even think to gain more money than to gain, become

99
00:13:29,800 --> 00:13:42,920
more powerful, because livelihood is such an intrinsic part of our lives, it necessarily

100
00:13:42,920 --> 00:13:51,560
colors our practice, for a monk, if they're all constantly obsessing over livelihood,

101
00:13:51,560 --> 00:14:00,760
get nicer food, better food, better roads, better, shout, better homes, and their minds

102
00:14:00,760 --> 00:14:05,760
will never be at peace, and they will be able to find clarity of mind, that lay people

103
00:14:05,760 --> 00:14:22,320
is very much the same, if you're obsessed with your job, trying to succeed, worried about

104
00:14:22,320 --> 00:14:34,440
success in a worldly sentence, a never-sixteenth spiritual, people would always come to

105
00:14:34,440 --> 00:14:41,880
my teacher asking for advice, should they get a new job or so on, invariably he would

106
00:14:41,880 --> 00:14:46,520
say no, should stay with what you've got, the blast and what they should sell, I remember

107
00:14:46,520 --> 00:14:52,840
once someone came and asked him, what do you think I should sell, I don't want to open

108
00:14:52,840 --> 00:14:59,600
the store and sell something, of course the John didn't want to answer, he could sell

109
00:14:59,600 --> 00:15:09,840
salt, so salt, it was like reluctant advice, when you get a good point, because everyone

110
00:15:09,840 --> 00:15:21,800
needs salt, but it's such a simple thing to say, stop looking for more, do something

111
00:15:21,800 --> 00:15:33,440
good, and just get a job and work, so salt, like this unangami who sold parts, but he

112
00:15:33,440 --> 00:15:37,640
didn't really sell them, he just put them by the side of the road and people came by

113
00:15:37,640 --> 00:15:44,440
and offered, whatever they thought the pot was worth, he never put price tags on them,

114
00:15:44,440 --> 00:15:52,080
even when they asked how much is this, he would say just leave whatever you want, that's

115
00:15:52,080 --> 00:16:05,480
ideal, that's beautiful, it's such peace of mind if you can, so of course then people

116
00:16:05,480 --> 00:16:10,920
argue that we're not having enough food to eat, it's hardly conducive to peace of mind

117
00:16:10,920 --> 00:16:22,640
and it's true, you do have to work, you should work, it's likely it is important, but it

118
00:16:22,640 --> 00:16:29,640
shouldn't consume you, you should work hard at it, so that you're working as efficient

119
00:16:29,640 --> 00:16:41,600
amount, not to be blamed, livelihood is a duty, amongst have a duty to teach, he may say

120
00:16:41,600 --> 00:16:52,240
it's not livelihood purposes, but I think partially it is, amongst has a duty to teach,

121
00:16:52,240 --> 00:17:00,440
it's just like a man selling parts, you don't teach to make money, you teach because

122
00:17:00,440 --> 00:17:06,080
that's our duty, and the people want us to continue to teach, but I'm sure we can

123
00:17:06,080 --> 00:17:13,560
defend, so they would have this about support, you work in a company, they value you, so

124
00:17:13,560 --> 00:17:27,240
they keep paying you, all you have to do is be valuable, something to think about, livelihood

125
00:17:27,240 --> 00:17:33,880
is our livelihood contributing to our spiritual practice or is it detracting from it, hard

126
00:17:33,880 --> 00:17:38,800
work in a bad work environment doesn't necessarily have to detract from spiritual practice,

127
00:17:38,800 --> 00:17:46,160
again it's all about what your mind is, if you want it can be like a duty, I remember

128
00:17:46,160 --> 00:17:54,320
when I after I began practicing meditation, I was still not even a monk, I went tree

129
00:17:54,320 --> 00:18:04,360
planting in Northern Ontario, tree planting is, wow, what a job, hard, the hardest work

130
00:18:04,360 --> 00:18:16,320
you can ever see, the work, the work coming out as a day, the morning until some day,

131
00:18:16,320 --> 00:18:20,960
eight hours, for some reason I want to say 10, but I don't think that's possible, anyway,

132
00:18:20,960 --> 00:18:31,480
you're working all day, planting thousands of trees, and the point is that some of the

133
00:18:31,480 --> 00:18:35,720
worst people, from my experience it was some of the worst people, but I think I got a really

134
00:18:35,720 --> 00:18:42,560
bad company, and they were just the lowest of the low, and the people who were supervising

135
00:18:42,560 --> 00:18:56,200
us were really awful, and the quality of people, I mean it was a lot of, I guess I would

136
00:18:56,200 --> 00:19:03,080
cast, and I remember the environment was not also all that wholesome, and I shouldn't

137
00:19:03,080 --> 00:19:10,080
judge, actually some of the people were okay, everyone was people, you know, and it hopes

138
00:19:10,080 --> 00:19:15,800
and dreams and wishes and so on, but just being to say that it was a rather coarse,

139
00:19:15,800 --> 00:19:23,520
coarse environment, and I remember just trying to be as mindful as I could, and the worst

140
00:19:23,520 --> 00:19:27,360
part of course was the mosquitoes, the black clouds of mosquito, so I would do walking

141
00:19:27,360 --> 00:19:35,440
meditation, wearing a full body mosquito net, because there were honestly black clouds

142
00:19:35,440 --> 00:19:46,280
of mosquito, you never seen, so I tried to plant trees mindful, I think it's possible

143
00:19:46,280 --> 00:19:53,920
to do all these things, to be surrounded by people who are not very mindful, this doesn't

144
00:19:53,920 --> 00:20:02,320
be mindful, but I said we're happy among those who are unhappy, which is preferable to

145
00:20:02,320 --> 00:20:11,320
be with good people who support me in practice, because without saying, we shouldn't

146
00:20:11,320 --> 00:20:17,760
be discouraged by environment, in fact that's the key is to not be discouraged, to

147
00:20:17,760 --> 00:20:23,640
just see it as, see it objectively as it is, seeing, hearing is now interesting, thinking

148
00:20:23,640 --> 00:20:28,560
no matter what it is, even if you don't have livelihood and you're living on the street,

149
00:20:28,560 --> 00:20:33,160
dying of thirst and hunger, it's still just seeing, hearing is now, it's not interesting

150
00:20:33,160 --> 00:20:42,480
if you want to, and then that's the key, that's how the eyes, that's the look on the, that's

151
00:20:42,480 --> 00:20:49,780
how we amongst dream ourselves, if I don't get food today, I won't eat, this is old

152
00:20:49,780 --> 00:20:58,840
monk in Chiang Mai, he was passing on some words of wisdom, that are quite common in that,

153
00:20:58,840 --> 00:21:02,640
I remember hearing this stuff from the very head of the Chiang Mai province, I went

154
00:21:02,640 --> 00:21:08,680
to get something signed, they started talking, they didn't get many rich visitors, and

155
00:21:08,680 --> 00:21:15,640
they were talking about eating, how's food, and okay, and he said yeah, he said, we got

156
00:21:15,640 --> 00:21:22,640
Chiang Mai, we got you, we got you, it sounds so much better than chiang Mai, we got you,

157
00:21:22,640 --> 00:21:28,440
chiang, we got you, it's a play on one, if you have food, you've chiang, we don't have food

158
00:21:28,440 --> 00:21:34,940
chan joy, it's a common one tied up place in the atmosphere, works that sound similar.

159
00:21:34,940 --> 00:21:35,940
Chan means eat.

160
00:21:35,940 --> 00:21:43,440
If you have food you eat, if you don't eat you, choy means sit still or be neutral.

161
00:21:43,440 --> 00:21:48,440
Choy means neutral.

162
00:21:48,440 --> 00:21:56,440
Stay calm.

163
00:21:56,440 --> 00:21:59,440
It's true, livelihood, livelihood that is not livelihood.

164
00:21:59,440 --> 00:22:02,440
And I think it's possible for late people as well.

165
00:22:02,440 --> 00:22:04,440
I just have to see it all as a duty.

166
00:22:04,440 --> 00:22:08,440
Okay, they won't need a work, okay, I will.

167
00:22:08,440 --> 00:22:15,440
Okay, they want to fire me, okay, go without food today, go without money today.

168
00:22:15,440 --> 00:22:17,440
Maybe not to that extent.

169
00:22:17,440 --> 00:22:24,440
But seeing it as a duty, we can all get to that level of not letting our work overwhelm us,

170
00:22:24,440 --> 00:22:27,440
not becoming obsessed with work.

171
00:22:27,440 --> 00:22:31,440
Just do it as a duty, do our thing, go home.

172
00:22:35,440 --> 00:22:38,440
The spiritual practice can take place anywhere.

173
00:22:38,440 --> 00:22:42,440
The spiritual practice is not what you do, it's how you do it.

174
00:22:42,440 --> 00:22:47,440
That's how where your mind is when you do it.

175
00:22:47,440 --> 00:22:59,440
So here's wishing you all all of us find clarity of mind, the ability to do things with a clear mind.

176
00:22:59,440 --> 00:23:03,440
Live our lives with a clear mind.

177
00:23:03,440 --> 00:23:09,440
And find true clarity of light.

178
00:23:09,440 --> 00:23:19,440
So that's all, thank you for tuning in, have a good night.

