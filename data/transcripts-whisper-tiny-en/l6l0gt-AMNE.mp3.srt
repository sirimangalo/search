1
00:00:00,000 --> 00:00:07,000
Hi, and welcome back to Ask A Monk. Today's question comes from minor players.

2
00:00:07,000 --> 00:00:15,000
As I meditate, I first want to develop a context of equanimity and loving kindness with respect to my thoughts and emotions as they arise.

3
00:00:15,000 --> 00:00:24,000
We live in a culture of attack or be attacked, and you do a short guided meditation on calm abiding, please.

4
00:00:24,000 --> 00:00:36,000
Yeah, calm abiding or these meditations on the altruistic emotions can be very useful.

5
00:00:36,000 --> 00:00:44,000
It's something that's best used as a companion to insight meditation.

6
00:00:44,000 --> 00:00:53,000
I wouldn't recommend someone to just go ahead and start practicing, developing loving kindness or equanimity or so on.

7
00:00:53,000 --> 00:00:58,000
If they're not really practicing insight meditation, they're not practicing meditation to see things as they are.

8
00:00:58,000 --> 00:01:02,000
Because it's generally basically just covering up our emotions.

9
00:01:02,000 --> 00:01:18,000
When you're angry and then you pretend that you love everyone, it's really not a way of gaining sincere love or sincere appreciation of the suffering that the beings are going through.

10
00:01:18,000 --> 00:01:25,000
The best way to develop loving kindness and compassion and equanimity and so on is through insight meditation.

11
00:01:25,000 --> 00:01:32,000
So we often encourage people after they finish practicing to develop these things.

12
00:01:32,000 --> 00:01:47,000
I would be careful trying to, creating this idea that somehow we have to develop these things first before we can adequately develop insight meditation.

13
00:01:47,000 --> 00:01:58,000
I would say that if that's the case, then you really have to examine your emotions that are causing you to think this way.

14
00:01:58,000 --> 00:02:12,000
You have great states of anger and hatred in your mind or maybe even a state of attachment to the peace and the happiness wanting to feel this state of love and equanimity.

15
00:02:12,000 --> 00:02:20,000
You don't want to have to deal with the suffering that comes from looking at the negative side of reality, looking at our negative emotions.

16
00:02:20,000 --> 00:02:27,000
And you really have to, in the end, put that aside and approach these negative emotions.

17
00:02:27,000 --> 00:02:36,000
When you feel angry, you have to learn about the best way to do away with it is to understand that when you have an attachment, when you want to feel love,

18
00:02:36,000 --> 00:02:46,000
you want to feel peace or when you don't like the ups and downs of the mind where the mind is liking, disliking, disliking.

19
00:02:46,000 --> 00:02:54,000
You have to examine that one thing that dislikes for the turbulence in the mind.

20
00:02:54,000 --> 00:03:02,000
That being said, as I mentioned, it's something that we do recommend people to practice in tandem with insight meditation.

21
00:03:02,000 --> 00:03:23,000
So at the time when you're practicing insight meditation, you can also take a break and develop thoughts of love when there's a real serious problem in your mind when you feel kind of angry about other people or you feel upset about things that other people have done for you or you feel like the world is out to get you or so on.

22
00:03:23,000 --> 00:03:42,000
And it's a really appropriate time to develop loving kindness and try to smooth out these emotions and calm your mind in this way, just reassuring yourself of the good emotions and the proper way to approach our interaction,

23
00:03:42,000 --> 00:03:45,000
the way to interact with the world around us.

24
00:03:45,000 --> 00:03:53,000
So a good way to do this is they say to first start with yourself and make a wish for yourself to be happy and free from suffering.

25
00:03:53,000 --> 00:04:04,000
May I find peace and bliss and freedom and then start wishing it for other people starting with maybe the people who are closest to you.

26
00:04:04,000 --> 00:04:07,000
It can be the people who are in closest proximity to you.

27
00:04:07,000 --> 00:04:11,000
You can make a wish for your family to be happy and free from suffering.

28
00:04:11,000 --> 00:04:19,000
Another way to do it is start with those people that you love because they might not be physically close to you. They're emotionally close.

29
00:04:19,000 --> 00:04:23,000
And this is because these are the easiest people to send thoughts of love to.

30
00:04:23,000 --> 00:04:28,000
So either people who are physically close to you or people who are emotionally close to you, may they be happy.

31
00:04:28,000 --> 00:04:30,000
They may be free from suffering.

32
00:04:30,000 --> 00:04:33,000
They may find peace and bliss and so on.

33
00:04:33,000 --> 00:04:39,000
Then you go to people who are a little bit further from you, people who maybe you don't have positive feelings about.

34
00:04:39,000 --> 00:04:44,000
Maybe you don't have negative feelings about maybe people in the near vicinity.

35
00:04:44,000 --> 00:04:46,000
So you can go into directions.

36
00:04:46,000 --> 00:04:51,000
You can go in terms of your emotional proximity or you can go in terms of your physical proximity.

37
00:04:51,000 --> 00:04:56,000
I generally like to go in terms of physical proximity because it's much more neutral.

38
00:04:56,000 --> 00:05:05,000
It's much less biased in terms of involving emotions of attachment and lust and so on.

39
00:05:05,000 --> 00:05:13,000
So you start with the people who are nearby, the people in this building say and then start with and then go to the next to the people in this city.

40
00:05:13,000 --> 00:05:16,000
Then the people in this country, then all the people in the world.

41
00:05:16,000 --> 00:05:25,000
And then you can extend it to other beings and extend it to animals, extend it to angels, extend it to ghosts and all beings.

42
00:05:25,000 --> 00:05:28,000
Going in terms of proximity.

43
00:05:28,000 --> 00:05:34,000
Now the texts do encourage you also to do it in terms of your emotional closeness.

44
00:05:34,000 --> 00:05:45,000
So you can start with people who you love and then go on to people who are neutral to you and then go on to people who are maybe enemies to you.

45
00:05:45,000 --> 00:05:50,000
People who are problems with people who are angry and angry at you.

46
00:05:50,000 --> 00:05:59,000
And this is really a good way to solve a lot of those emotional problems that we have within the people when we're in a battle with someone we can start in this way.

47
00:05:59,000 --> 00:06:04,000
First think of the people we love, then ordinary people and then go on to this person.

48
00:06:04,000 --> 00:06:15,000
When we're not able to send love to them anymore, we give rise to too much anger, come back to the neutral person, come back to the person you love and slowly go back out in and out and in and out.

49
00:06:15,000 --> 00:06:19,000
And finally we can send love to all three.

50
00:06:19,000 --> 00:06:30,000
And that's also a good way to start spreading it out to other beings as well. Once you can do that, then send it to start sending it to all beings.

51
00:06:30,000 --> 00:06:32,000
This is in terms of love.

52
00:06:32,000 --> 00:06:35,000
It also is the practice of compassion.

53
00:06:35,000 --> 00:06:36,000
So these two really go together.

54
00:06:36,000 --> 00:06:42,000
Love is the positive side where you wish for beings to have to get good things, to be happy.

55
00:06:42,000 --> 00:06:45,000
Compassion is just wishing for them to be free from suffering.

56
00:06:45,000 --> 00:06:53,000
And this is useful for people who you feel sad about, you feel sorry for people who have sickness, people who have problems.

57
00:06:53,000 --> 00:07:02,000
When you look at someone and you feel empathic towards empathy, you can empathize with their situation.

58
00:07:02,000 --> 00:07:10,000
And it's a way of calming your mind and being a support for them so that you're not an emotional wreck because of their difficulties.

59
00:07:10,000 --> 00:07:22,000
It's also a way of supporting them and allowing them to see this calm state, giving them a support.

60
00:07:22,000 --> 00:07:25,000
These two go together.

61
00:07:25,000 --> 00:07:36,000
The third one is wishing for people or not wishing, but respecting people's good accomplishments, the things that they get.

62
00:07:36,000 --> 00:07:39,000
This is joy, feeling happy for people when they get good things.

63
00:07:39,000 --> 00:07:45,000
This is something that is, I think, not generally used as a meditation.

64
00:07:45,000 --> 00:07:48,000
It can be and it's a perfectly valid meditation object.

65
00:07:48,000 --> 00:07:53,000
But from what you see people using it, it's not emphasized so much.

66
00:07:53,000 --> 00:07:57,000
And what you do see it coming into play and where I use it a lot is in daily light.

67
00:07:57,000 --> 00:08:02,000
When you see someone get something good, it's actually say to them, say, oh, I'm very happy for you.

68
00:08:02,000 --> 00:08:03,000
And we do this normally.

69
00:08:03,000 --> 00:08:05,000
This is something which we should cultivate.

70
00:08:05,000 --> 00:08:18,000
When someone gets something good, we say, oh, I feel happy for you and wishing for them to get good things and to keep the things that they have.

71
00:08:18,000 --> 00:08:23,000
Feeling happy for people when good things come to them, not feeling jealous.

72
00:08:23,000 --> 00:08:34,000
This one is the giving up of jealousy, the giving up of always comparing ourselves with other people wishing for the things that they have or not wanting them to get the things that they have.

73
00:08:34,000 --> 00:08:49,000
But it also means spreading the good things that you have when you have wealth to give to other people, when you have knowledge to give knowledge to other people to share the teachings and to help other people to gain good things.

74
00:08:49,000 --> 00:08:51,000
And feel happy about other people getting good things.

75
00:08:51,000 --> 00:08:58,000
This is another important quality of mind that that calms our mind and helps us to practice meditation.

76
00:08:58,000 --> 00:09:07,000
As for equanimity, this is something that really goes hand in hand with Vipas in the meditation.

77
00:09:07,000 --> 00:09:09,000
It almost is Vipas in the meditation.

78
00:09:09,000 --> 00:09:14,000
You can almost say that the best way to practice to develop equanimity is through Vipas in the meditation.

79
00:09:14,000 --> 00:09:25,000
Now, you can do intellectual exercises like understanding karma when you study the law of karma and you look at beings and you say they're going according to their karma.

80
00:09:25,000 --> 00:09:36,000
This is useful intellectually to calm the mind and to do away with this sense that it's unfair and why do that have to happen to them.

81
00:09:36,000 --> 00:09:54,000
Feeling angry when you see the political situation of the country or you see people in great states of poverty and so on seeing the very rich and the very poor and feeling angry and wanting to change things, wanting to go in protest.

82
00:09:54,000 --> 00:10:04,000
The intellectual exercise of saying to yourself and all beings go according to their karma is actually a sort of a meditation because it gives you this sense of equanimity.

83
00:10:04,000 --> 00:10:18,000
This is one important practice that you can undertake when you study karma and then you see beings in great states of loss and suffering and in great states of power and influence, affluence.

84
00:10:18,000 --> 00:10:37,000
You can remind yourself that these people in the state of great affluence, if they continue to repress those people in poverty, they're the ones who are going to obtain state and attain to these states of poverty themselves in the future.

85
00:10:37,000 --> 00:10:55,000
When you think like that, you really do away with a sense of unfair, you see that it's perfectly fair and there's no need to get angry at those people who are oppressing the poor or hurting other people.

86
00:10:55,000 --> 00:11:04,000
They're the ones who are going to have to suffer in the future and the reason why people suffer in the present is because they've done this to others in the past.

87
00:11:04,000 --> 00:11:10,000
But the best way to develop equanimity is through the practice of insight meditation.

88
00:11:10,000 --> 00:11:13,000
When you do so, you start to see everything in quantumists.

89
00:11:13,000 --> 00:11:22,000
In fact, to the point, once you practice insight meditation, as I already mentioned, it's the best way to develop these four states of mind.

90
00:11:22,000 --> 00:11:28,000
When you practice meditation, you understand yourself and you understand reality.

91
00:11:28,000 --> 00:11:35,000
As an extrapolation to that, you understand all beings.

92
00:11:35,000 --> 00:11:40,000
You understand the nature of the sentient being.

93
00:11:40,000 --> 00:11:45,000
So you can empathize with people when they're suffering.

94
00:11:45,000 --> 00:11:47,000
You know what they're going through.

95
00:11:47,000 --> 00:11:54,000
Even when they're angry or upset or engaged in immoral acts, you can understand them.

96
00:11:54,000 --> 00:12:01,000
You can feel pity for them that they're addicted or they're stuck in this cycle that they can't break free from.

97
00:12:01,000 --> 00:12:03,000
It's really not their fault.

98
00:12:03,000 --> 00:12:08,000
It's the development of habitual tendencies through ignorance.

99
00:12:08,000 --> 00:12:11,000
They're simply not knowing what they're doing.

100
00:12:11,000 --> 00:12:15,000
When people are in suffering, you can empathize with them.

101
00:12:15,000 --> 00:12:17,000
When people get good things, you feel happy for them.

102
00:12:17,000 --> 00:12:18,000
You don't feel jealous.

103
00:12:18,000 --> 00:12:23,000
You feel good that good things have come because you know that it leads to happiness.

104
00:12:23,000 --> 00:12:29,000
You're happy and content in yourself, so you don't need to compare yourself to others.

105
00:12:29,000 --> 00:12:34,000
Ultimately, you see everything with equanimity.

106
00:12:34,000 --> 00:12:39,000
You don't prefer to be with this person or that person.

107
00:12:39,000 --> 00:12:47,000
You accept things for what they are, and you realize that partiality is not of any use,

108
00:12:47,000 --> 00:12:51,000
and it's not of any benefit, and doesn't lead to real happiness.

109
00:12:51,000 --> 00:12:54,000
This is rather than a guided meditation.

110
00:12:54,000 --> 00:12:59,000
This is a little bit of more than a theoretical sign, but explaining exactly how you do it.

111
00:12:59,000 --> 00:13:02,000
I think it's important that it works for you.

112
00:13:02,000 --> 00:13:08,000
If you want to develop a meditation based on loving kindness or based on equanimity,

113
00:13:08,000 --> 00:13:15,000
that you should create the sentence in your mind and develop these thoughts.

114
00:13:15,000 --> 00:13:22,000
According to your own situation, as I said, you can do it based on people who are close to you,

115
00:13:22,000 --> 00:13:29,000
and then going out in terms of physical proximity, or you can do it in terms of emotional proximity.

116
00:13:29,000 --> 00:13:35,000
But it's something that should come naturally, and something that you should develop in tandem with insight meditation.

117
00:13:35,000 --> 00:13:49,000
Something that I wouldn't really recommend giving too much of our time on, in terms of developing intensive meditation in this area.

118
00:13:49,000 --> 00:13:56,000
Simply because it's time consuming, and we'd be much better served to be developing insight meditation,

119
00:13:56,000 --> 00:14:01,000
and using this to accent our development of insight meditation.

120
00:14:01,000 --> 00:14:07,000
When a problem arises or when it really gets overwhelming to use this as a support for our meditation.

121
00:14:07,000 --> 00:14:35,000
I hope that in some way helps, and keep the questions coming.

