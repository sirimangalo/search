1
00:00:00,000 --> 00:00:09,240
Hello and welcome back to our study in the Damapada.

2
00:00:09,240 --> 00:00:17,240
Today we continue on with verse number 57, which reads as follows,

3
00:00:17,240 --> 00:00:39,600
for those, for those who are endowed with morality,

4
00:00:39,600 --> 00:00:46,280
a pamada vihari nangdwelling in vigilance or without negligence,

5
00:00:46,280 --> 00:00:51,760
samadhanya vimutta nang, who are liberated through wisdom,

6
00:00:51,760 --> 00:00:56,400
through right wisdom or rightly through wisdom.

7
00:00:56,400 --> 00:01:03,760
Mahro-manga nuindhati, Mahara is unable to find their path.

8
00:01:03,760 --> 00:01:14,160
And this teaching was given in regards to a story that is a little bit hard to

9
00:01:14,160 --> 00:01:22,960
explore, a little bit controversial, and I'll explain it, it'll become clear as I tell the story.

10
00:01:22,960 --> 00:01:32,960
There was a mongodika, he was dwelling in Black Rock, New Rajika,

11
00:01:32,960 --> 00:01:36,040
when the Buddha was dwelling in Rajika.

12
00:01:36,040 --> 00:01:44,080
Goedika was dwelling at Black Rock, and he was practicing, would appear to be

13
00:01:44,080 --> 00:01:48,960
some at the meditation, so he was attaining what we call jitohimuti,

14
00:01:48,960 --> 00:01:53,360
the abrasion of mind, which comes through the attainment of the janas,

15
00:01:53,360 --> 00:01:58,240
the samadha janas, so he had this profound peace and tranquility in his mind,

16
00:01:58,240 --> 00:02:02,720
and he was able to enter them quite easily.

17
00:02:02,720 --> 00:02:08,400
But then he became sick, and he had this, the sickness prevented him,

18
00:02:08,400 --> 00:02:12,720
or it didn't prevent him, but it was such that every time he attained to this

19
00:02:12,720 --> 00:02:21,280
liberation or to the worldly meditative attainment, he would slip away from it,

20
00:02:21,280 --> 00:02:26,160
so he was unable to sustain it, and this happened once, it happened twice,

21
00:02:26,160 --> 00:02:31,520
it happened three times, it happened six times, and so the seventh time that had

22
00:02:31,520 --> 00:02:39,920
happened, he made a resolution, he said, look, I'm dying here, and if I die without,

23
00:02:39,920 --> 00:02:45,760
it's clear, I'm going back and forth in between the jana and an ordinary mind state,

24
00:02:45,760 --> 00:02:52,400
and if I die in an ordinary mind state, it's not sure where I'm going to go, I might wind up in hell,

25
00:02:52,400 --> 00:03:00,000
I might wind up as an animal, but if I die in the meditative state, then for sure, at least,

26
00:03:00,000 --> 00:03:11,200
I will go to the higher god realms as a Brahma, I'll explain about that after,

27
00:03:12,800 --> 00:03:19,280
and so he made a decision, he took his razor that he used to cut his hair,

28
00:03:19,280 --> 00:03:23,760
and he went into his kutti, his hut, and he laid down and he decided that he was going to kill

29
00:03:23,760 --> 00:03:29,280
himself, he was going to cut his windpipe with this razor,

30
00:03:29,280 --> 00:03:35,360
and Mara, the evil one who was like the Buddhist Satan, he came along and

31
00:03:36,960 --> 00:03:42,080
saw that this was happening, and due to his own understanding, seems to be misunderstanding

32
00:03:42,080 --> 00:03:49,840
of the way things work, he thought that if he agreed with this monk, he said, well, if this monk

33
00:03:49,840 --> 00:03:56,720
kills himself, he'll go to the high Brahma realms where he'll be free from my domain,

34
00:03:56,720 --> 00:04:05,760
and I don't want that, so I'll go and I'll convince the Buddha to tell his student not to kill

35
00:04:05,760 --> 00:04:09,600
himself, because surely the Buddha doesn't want his students to kill himself, to kill themselves,

36
00:04:10,480 --> 00:04:18,880
and so Mara went to the Buddha, Mara is like this evil spirit, and he said to the Buddha,

37
00:04:18,880 --> 00:04:25,520
that your student, even though he's practicing to overcome death, and he's entered into this

38
00:04:25,520 --> 00:04:31,360
state of trying to overcome death, or overcoming death, or whatever, still he's afraid of death,

39
00:04:31,360 --> 00:04:33,840
or he's still contemplating death, or he's going to kill himself.

40
00:04:37,040 --> 00:04:46,800
And the Buddha brushes him off and says that this monk is someone who is not afraid of death,

41
00:04:46,800 --> 00:04:51,040
so someone who has overcome death, who is set in morality and so on and so on,

42
00:04:51,040 --> 00:05:02,240
and brushes off Mara and says leave me be, and goes to Black Rock to where this monk is staying

43
00:05:02,240 --> 00:05:09,200
takes amongst there, and goes and sees that Godica, and sees that Godica actually has, it has taken

44
00:05:09,200 --> 00:05:19,120
his life, and Mara is there as well and Mara comes and sees that Godica has died,

45
00:05:19,120 --> 00:05:30,320
and he vanishes, and starts racing around the universe, trying to find where Godica has been

46
00:05:30,320 --> 00:05:36,320
reborn, and he appears as this kind of a Black cloud, and this is something you see,

47
00:05:36,320 --> 00:05:42,880
it occurs in the Buddha's teaching, Mara, whenever he's looking for someone where they've been

48
00:05:42,880 --> 00:05:50,560
reborn, he's like this Black cloud going through the racing through the skies, trying to find

49
00:05:50,560 --> 00:05:58,400
this being where they've been reborn, where they've continued, and the Buddha says you see that

50
00:05:58,400 --> 00:06:04,640
Black cloud up there, that's Mara and he's trying to find where this monk has been reborn,

51
00:06:04,640 --> 00:06:11,200
and he won't ever find where that monk is, where Godica has been reborn, because Godica has

52
00:06:11,200 --> 00:06:16,960
become an Raha, it's become free from suffering, Mara comes back and asks him basically that,

53
00:06:16,960 --> 00:06:22,000
where has he gone, and the Buddha said you'll never find such a person, you'll never find Godica,

54
00:06:22,880 --> 00:06:34,320
and then he gave this verse teaching, he said even a thousand of you, a thousand Mara's, or a

55
00:06:34,320 --> 00:06:40,880
thousand angels, or a thousand spirits, couldn't find such a being, and then he gave the verse,

56
00:06:41,760 --> 00:06:48,960
those who, for those who are endowed with morality and virtuous conduct, who are

57
00:06:50,000 --> 00:06:58,720
who dwell in vigilance and are liberated through right wisdom, Mara cannot find their path.

58
00:06:58,720 --> 00:07:05,760
So this is a controversial teaching, because it can be taken in several different ways,

59
00:07:07,760 --> 00:07:17,040
for some people this seems to be a allowance for suicide, or a nullification of any ethical

60
00:07:19,040 --> 00:07:27,120
quality to the act of killing yourself. The commentary takes it in probably the most,

61
00:07:27,120 --> 00:07:32,960
or the least dangerous way, it gives the least dangerous interpretation to it by saying that

62
00:07:34,400 --> 00:07:40,400
Godica and another monk like him, who did a similar thing in his record in Spittica,

63
00:07:42,480 --> 00:07:47,040
they weren't enlightened, and then when they cut their, when he cut his windpipe,

64
00:07:48,080 --> 00:07:52,160
thinking that he was somehow going to go to the Brahmor alms, he realized what an idiot he had been,

65
00:07:52,160 --> 00:07:57,600
and he became afraid of death, and practiced then insight meditation.

66
00:07:59,840 --> 00:08:06,640
And the point being that the moment of death is actually a really good moment to practice

67
00:08:07,440 --> 00:08:12,000
insight meditation. It's a time when you have to let go of everything where you're confronted

68
00:08:12,000 --> 00:08:16,400
with your whole life, you're confronted with all your good and bad deeds, and you're forced to

69
00:08:16,400 --> 00:08:22,480
confront a lot. Now, since he had been practicing meditation up into that point, even though it

70
00:08:22,480 --> 00:08:29,200
seemed, all he entertained was a summit to insight. It was a base for quite clear insight to

71
00:08:29,200 --> 00:08:40,640
arise as a result of his, to rise as a result of the impending death. So that answers a lot of

72
00:08:40,640 --> 00:08:45,600
the questions as to, you know, how can you condone suicide? How can suicide be a good thing? How can

73
00:08:45,600 --> 00:08:56,080
someone do it innocently? But there's still the question of whether suicide itself is a bad deed,

74
00:08:56,800 --> 00:09:04,640
and of course in Buddhism we don't consider deeds to be good or bad, right? So actually the

75
00:09:04,640 --> 00:09:09,840
act of killing ourselves isn't the problem, and I think this kind of solves some of the dilemmas

76
00:09:09,840 --> 00:09:18,000
about this verse about this story. In that an ordinary person when they want to kill themselves

77
00:09:18,000 --> 00:09:26,080
is doing it out of generally anger, a desire to end whatever or to be free from whatever

78
00:09:27,600 --> 00:09:34,640
suffering they're going to have, the suffering they have out of depression or anger or sadness

79
00:09:34,640 --> 00:09:40,560
or whatever people decide to kill themselves. And that's a really dangerous thing to do because

80
00:09:40,560 --> 00:09:49,040
that act is done out of negative emotions. And as a result that sort of person is going to go to

81
00:09:49,040 --> 00:09:56,080
a bad place or is likely to go to a bad place. But that having been said, they still have the moment

82
00:09:57,360 --> 00:10:03,360
from the time of doing that moment of doing that act of cutting their throat until the moment

83
00:10:03,360 --> 00:10:08,640
they die. In the case of cutting your throat or just letting your wrists, that's still some time

84
00:10:09,840 --> 00:10:16,880
if during that time they're able to come to grips with their experience, it's possible that a

85
00:10:16,880 --> 00:10:21,520
person who kills themselves could go to a good place because they're forced to deal with those things.

86
00:10:22,240 --> 00:10:29,920
That doesn't take away the negative impact of killing yourself. It can be a bad deed. It seems

87
00:10:29,920 --> 00:10:34,000
likely that it would be a bad deed in most cases, even in the case of Godhika, there's still a good

88
00:10:34,000 --> 00:10:44,800
argument for it being a bad karma. It's just that in the interim period between the killing

89
00:10:44,800 --> 00:10:54,800
himself and the actual dying, he was able to gain insight. That being said, it seems like in

90
00:10:54,800 --> 00:11:01,360
certain cases, there might even be an argument for an Arahan to be able to kill themselves.

91
00:11:02,160 --> 00:11:06,000
Bhikkhu Bodhi, I think, he interprets this by the Singyutani Kaya version,

92
00:11:08,000 --> 00:11:11,920
or maybe it's the story about Chandhana, there's another monk who kills himself.

93
00:11:14,320 --> 00:11:18,160
It seems reasonable to interpret it as actually an Arahan, someone who's enlightened who has

94
00:11:18,160 --> 00:11:22,160
no defilements. There's nothing wrong with them killing themselves. In that case,

95
00:11:22,160 --> 00:11:25,760
the point being the only reason they would kill themselves is because they're dying anyway.

96
00:11:28,400 --> 00:11:34,800
It's like a cure for the sickness or it's not wanting to be a burden on other people. To me,

97
00:11:34,800 --> 00:11:40,720
that's kind of controversial. I'm not going to go that far, but I might suggest that it's possible

98
00:11:41,600 --> 00:11:50,640
possible in certain situations where a person is free from any attachment to other people. I guess

99
00:11:50,640 --> 00:11:56,160
that the point is, if it's just the effect on yourself, it's not murder. It doesn't seem to be

100
00:11:56,160 --> 00:12:01,200
on the scale of murder. Killing yourself seems to be a voluntary choice that people make,

101
00:12:01,200 --> 00:12:05,200
if it's not affecting others, if you're not leaving dependence behind or people who love you,

102
00:12:05,200 --> 00:12:09,520
like if you're a monk living off from the forest alone, it doesn't matter to anyone,

103
00:12:09,520 --> 00:12:18,000
you're not hurting others by killing yourself. It seems that Buddhism doesn't have anything specifically

104
00:12:18,000 --> 00:12:22,480
ethical to say about it. Now, monks aren't allowed to kill themselves. It's actually explicitly

105
00:12:22,480 --> 00:12:27,440
against the rules, but I think the safest thing we can say is that in certain cases,

106
00:12:31,680 --> 00:12:35,840
killing yourself gives one the opportunity to,

107
00:12:36,880 --> 00:12:42,640
and that's not even say that, but in certain cases, people have committed the act of wanting

108
00:12:42,640 --> 00:12:50,320
to kill themselves, and then being able to, in spite of that, come to become enlightened. And it

109
00:12:50,320 --> 00:12:57,840
seems actually to have indirectly or in a roundabout ways somehow helped them, because it brought

110
00:12:57,840 --> 00:13:05,520
them closer to death and helped them understand death. It's by no means an encouragement for suicide

111
00:13:05,520 --> 00:13:18,880
or even an allowance for the condoning of suicide in any way, as suicide is considered to be,

112
00:13:20,000 --> 00:13:24,720
well, it's not allowed by monks, and it's certainly considered in Buddhism to be a negative thing,

113
00:13:24,720 --> 00:13:30,160
in general, as I said, it's something that is based on anger and based on negative emotions,

114
00:13:30,160 --> 00:13:36,080
and I would say there's argument to be made, for even in the case of Godica or these monks

115
00:13:36,080 --> 00:13:42,720
who are practicing hard, that it was a bad choice to make. Just happened that by fluke,

116
00:13:42,720 --> 00:13:49,680
they were, as a result, able to contemplate death and die. It's like a gamble, because you know

117
00:13:49,680 --> 00:13:56,640
that death is a time when you can study yourself. It's a time when you'll be able to,

118
00:13:56,640 --> 00:14:06,240
you'll be able to learn more about your emotions and come to terms with them. So I guess the

119
00:14:06,240 --> 00:14:10,160
argument that you make again, that is that Godica was dying anyway, and if he had waited until he

120
00:14:10,160 --> 00:14:15,120
was really going to die naturally, it would have actually been a much better situation, and for

121
00:14:15,120 --> 00:14:20,240
sure he would have still been able to deal with the emotion. So he didn't gain anything by killing

122
00:14:20,240 --> 00:14:27,840
himself, but it just hastened his eventual enlightenment, which came not as a result of killing himself,

123
00:14:27,840 --> 00:14:33,200
but as a result of dying, which was going to come anyway. Okay, so that's how I would resolve

124
00:14:33,200 --> 00:14:40,240
in my mind the ethical ambiguity there. But more importantly, that's not concern ourselves about

125
00:14:40,240 --> 00:14:45,280
the question of suicide too much. It's certainly something that has to be taken seriously, and you

126
00:14:45,280 --> 00:14:52,800
really have to know one last thing I wanted to say is about the gamble, because the negative

127
00:14:52,800 --> 00:15:01,200
aspects of wanting to kill yourself, to anger the self-hatred or whatever the depression or the

128
00:15:01,200 --> 00:15:09,280
negative emotions are, is not going to set a good stage for your last moments. And so to take this

129
00:15:09,280 --> 00:15:15,920
as anything like encouragement for suicide is really to take it the wrong way. Death is

130
00:15:15,920 --> 00:15:22,080
a gamble, and that's what Godica did realize. He was just trying to stack his stack, the odds

131
00:15:22,080 --> 00:15:27,280
in his favor. He didn't actually succeed, right? He wanted to go to Brahma realms. He actually did

132
00:15:27,280 --> 00:15:33,600
something quite different, and that's became enlightened, and therefore was not reborn. But for

133
00:15:33,600 --> 00:15:38,480
most of us, death is a gamble. And so if you think that somehow hastening your death is going to put

134
00:15:38,480 --> 00:15:44,080
you closer to enlightenment, it just puts you closer to that gamble, and you're stacking the odds

135
00:15:44,080 --> 00:15:49,920
against yourself, actually. If you're committing deeds like killing yourself based on anger,

136
00:15:49,920 --> 00:15:55,360
based on depression, based on negative emotions, so certainly bad idea, and most likely to lead

137
00:15:55,360 --> 00:16:01,040
you to a bad destination. It's actually just an exception here, I would say. It's a curious

138
00:16:01,040 --> 00:16:11,520
exception that we have that these monks were in such a state that such a state that their death

139
00:16:11,520 --> 00:16:16,800
was caused for them to become enlightened. It basically didn't matter or didn't matter enough

140
00:16:16,800 --> 00:16:22,960
that they had killed themselves. That's how I would interpret it. But the point here is actually

141
00:16:22,960 --> 00:16:28,320
in the verse, which isn't dependent at all on the story. It was a cause for them to say this

142
00:16:28,320 --> 00:16:34,400
tomorrow, or to say this to the monks, to teach this to the monks. But the actual emphasis is

143
00:16:34,400 --> 00:16:45,920
on the qualities that allow one to become free from Mara's search. So take this out, you want Mara

144
00:16:45,920 --> 00:16:52,480
in Buddhism. There are five Mara's. There's Kanda Mara, which are the five aggregates. Our aggregates

145
00:16:52,480 --> 00:16:58,480
are considered a kind of evil, because they cause us lots of suffering. Our body, our feelings,

146
00:16:58,480 --> 00:17:04,480
our memories, our thoughts, our consciousness are things that cause us a lot of suffering.

147
00:17:04,480 --> 00:17:10,720
Like when a person's sick, that's Kanda Mara, because their bodies cause them suffering.

148
00:17:10,720 --> 00:17:17,760
So it's kind of an evil in that sense. There's Kiele Samara, which are the evil deeds that

149
00:17:17,760 --> 00:17:23,200
are the evil, the defilements that we have inside. So anger, greed, delusion, conceit, arrogance,

150
00:17:24,160 --> 00:17:30,320
worry, depression, et cetera, et cetera, all of the negative emotions and bad stuff inside.

151
00:17:30,320 --> 00:17:38,240
These are called Kiele Samara. They're evil. Bad. So evil is just the word. The word we use to describe

152
00:17:38,240 --> 00:17:45,200
these things. The third Mara is Abhisankara Mara. These are the bad deeds that we perform as a

153
00:17:45,200 --> 00:17:50,480
result of our defilements of killing, stealing, lying, cheating, all the bad stuff that we do,

154
00:17:50,480 --> 00:17:55,840
these are evil. It's evil to kill. It's evil to steal, et cetera, et cetera. The fourth kind of

155
00:17:55,840 --> 00:18:02,320
Mara is muchumara, death. Death is considered an evil. It's the kind of thing that it's this big

156
00:18:02,320 --> 00:18:06,800
gamble that you take. You don't know where you're going after you die. Totally based on the

157
00:18:06,800 --> 00:18:11,120
mind state at the moment of death and all that you've cultivated up to that point.

158
00:18:11,120 --> 00:18:19,040
And number five is called Devaputamara, where we, this angel of evil. It's an evil angel who

159
00:18:20,240 --> 00:18:24,560
has said to torment, have tormented the Buddha and torments all of us. So he's this guy who

160
00:18:24,560 --> 00:18:29,600
whispers in your ear and tells you to do bad things and tells you to kill yourself, for example,

161
00:18:31,440 --> 00:18:35,040
he was the guy who would go around whispering to the monks to tell them to do this or that and

162
00:18:35,040 --> 00:18:41,200
who tried to dissuade people from practicing or encourage people to practice in the wrong way.

163
00:18:41,200 --> 00:18:43,600
This kind of thing kind of like Satan in Christianity.

164
00:18:45,360 --> 00:18:51,840
So there's this idea that there is this angel who does these sorts of things. But you can also

165
00:18:51,840 --> 00:18:59,520
just think of it metaphorically dealing with our struggle to become enlightened. Mara is chasing

166
00:18:59,520 --> 00:19:05,760
us around and trying to find our path, trying to keep us on the wrong path and trying to keep us

167
00:19:05,760 --> 00:19:14,160
under his control. Every time we get angry, this is a Mara, every time we have greed, this is a Mara,

168
00:19:14,160 --> 00:19:20,640
this is our path, and this is the path of Mara. All of these things keep us going round and round,

169
00:19:20,640 --> 00:19:25,840
keep us tied to the wheel of samsara, keep us being reborn, keep us suffering,

170
00:19:25,840 --> 00:19:31,920
keep us hurting others. All of these things are the realm of Mara. There are seven

171
00:19:31,920 --> 00:19:36,720
paths that we have to choose from. There's the path to hell and that is the anger,

172
00:19:36,720 --> 00:19:41,360
anger leads us to hell, this is a path that Mara can clearly see and encourages us on.

173
00:19:41,360 --> 00:19:47,600
There's the path of greed, this is, or the path to become a ghost, this is the path of greed,

174
00:19:48,160 --> 00:19:53,600
so all of our greed and attachments, all of our desires and likes and so on. This is

175
00:19:53,600 --> 00:19:58,160
cultivating the addiction that eventually turns you into a ghost, this is a path of Mara,

176
00:19:59,120 --> 00:20:04,800
that Mara knows. There's the path to become an animal, this is the path of delusion,

177
00:20:04,800 --> 00:20:10,160
a person who is full of delusion. All of our delusions, these are leading us to become animals,

178
00:20:10,160 --> 00:20:17,120
all of the intoxicants and the drugs and alcohol that we take and all of our arrogance and

179
00:20:17,120 --> 00:20:22,480
all of our conceit and all of our bigotry and all of our prejudice. This is all leading us to be

180
00:20:22,480 --> 00:20:28,800
born as animals. This is the path that Mara knows very well. There's the path to become a human,

181
00:20:28,800 --> 00:20:34,080
even this path, Mara knows very well. This is the path to keeping the five precepts. Sometimes

182
00:20:34,080 --> 00:20:39,440
Mara is this guy who encourages you, it's okay, you don't have to practice, just keep the five

183
00:20:39,440 --> 00:20:45,600
precepts. I was talking about that early. Don't practice meditation, it's enough. Keep the five

184
00:20:45,600 --> 00:20:53,040
precepts. You'll be born as a human being, so Buddha's stuff can get coerced into, get tricked into

185
00:20:53,040 --> 00:20:56,960
this one, thinking that it's enough to just keep the five precepts. At least I'm keeping the five

186
00:20:56,960 --> 00:21:01,600
precepts and I'm a good person, I'll be born as a human being. Still the path of Mara, still

187
00:21:01,600 --> 00:21:06,480
you'll be born again and again and again and that's not a stable path because eventually you forget

188
00:21:06,480 --> 00:21:12,400
the Buddha's teaching and you can veer away into one of the three lower paths. Five is the path

189
00:21:12,400 --> 00:21:18,960
to become an angel and this is the path of good deeds. Again, we're often coerced and convinced

190
00:21:19,760 --> 00:21:24,720
by Mara, tricked by Mara into just performing good deeds, being good people, being charitable,

191
00:21:24,720 --> 00:21:30,000
being nice to others, we can get quite caught up in the excitement of that, the goodness of that.

192
00:21:30,000 --> 00:21:34,960
It's a wonderful thing and as a result we're born as angels and that can be a long, long,

193
00:21:34,960 --> 00:21:39,600
lasting thing, but it's still the path of Mara. It's a path that Mara knows well and still

194
00:21:39,600 --> 00:21:46,080
encourages people and if he thinks it'll keep them under his control. The sixth path is the

195
00:21:46,080 --> 00:21:53,120
path to Brahma. This is the path of Samata Jana. This is the path that Gondika was practicing.

196
00:21:53,760 --> 00:21:59,280
It was practicing meditation to gain Jatoi Muti, which is liberation of mind, means the mind is

197
00:21:59,280 --> 00:22:04,800
free from all greed, anger and delusion, but it's only the mind states that are arising. These

198
00:22:04,800 --> 00:22:12,640
mind states are free from all of that. The potential for those states has still been not eradicated.

199
00:22:12,640 --> 00:22:16,880
So as a result of practicing this one is born in the Brahma realms for a long, long time.

200
00:22:17,840 --> 00:22:24,960
A lot of many meditators are convinced, are tricked into practicing this path and as a result

201
00:22:24,960 --> 00:22:30,960
are reborn as Brahma's and live there a long, long time, but it's still, I'm not sure if it is a

202
00:22:30,960 --> 00:22:39,680
path that Mara is able to find. He seemed to think from this story that it was outside of his path.

203
00:22:39,680 --> 00:22:46,000
I think Mara is an angel and as a result, when he sees people go to the Brahma realms, they

204
00:22:46,000 --> 00:22:51,200
disappear. He can't find them. So he thinks they're gone, but actually they're just in a higher

205
00:22:51,200 --> 00:22:55,360
realm than gone realm. So even Mara can't find them. I'm not sure about this, but if I remember

206
00:22:55,360 --> 00:23:01,920
correctly, they're outside of his realm, but they're not actually. They're still going to come

207
00:23:01,920 --> 00:23:08,320
back and when Mara passes away or some Mara is going to see these Brahma's come down. They're

208
00:23:08,320 --> 00:23:12,800
going to come back and eventually they'll be back in the lower realms, the angel realms or the

209
00:23:12,800 --> 00:23:18,720
human realms or so on. So they're still not free, still not free from Mara. But the seventh

210
00:23:18,720 --> 00:23:24,960
path is the path that the Buddha is talking about in this verse, sampana, silaana, someone who has

211
00:23:27,040 --> 00:23:34,480
who has endowed with morality or endowed with virtue. This doesn't just mean keeping the precepts,

212
00:23:34,480 --> 00:23:42,080
this means someone who has a mind that is well trained, a mind that doesn't incline towards

213
00:23:42,080 --> 00:23:51,600
evil deeds or even evil thoughts, evil speech, a mind that is well controlled and well

214
00:23:52,960 --> 00:24:02,320
well guarded. A mind that stays with the present moment, stays with reality, stays with

215
00:24:02,320 --> 00:24:14,000
the here and the now. And endowed with true virtue and conduct. So the mind that is here and

216
00:24:14,000 --> 00:24:22,400
now. A pamada vihari nang is the one who dwells vihari in a pamada, which is free from

217
00:24:23,040 --> 00:24:30,720
negligence or drunkenness intoxication. The mind that is totally clear, conscious, sober, alert,

218
00:24:30,720 --> 00:24:36,720
mindful. And this is the mind that is mindful, the mind that is aware of things as they are,

219
00:24:36,720 --> 00:24:45,520
seeing, seeing, hearing, as hearing and sees things on a one-to-one level. It is what it is,

220
00:24:45,520 --> 00:24:51,200
and it's nothing else not projecting or judging or identifying or attaching to anything

221
00:24:51,200 --> 00:24:52,960
any aspect of experience.

222
00:24:52,960 --> 00:25:00,320
Samadhanya vi mutta nam, someone who is liberated through wisdom. Okay, liberated through wisdom

223
00:25:00,320 --> 00:25:08,560
is different from liberation through this mind state of the Janas. This mind state that fixes the

224
00:25:08,560 --> 00:25:14,400
mind. Liberation through wisdom is through knowing better, through getting on, getting the mind

225
00:25:14,400 --> 00:25:20,800
on a different track so that the mind has no potential for the future arising of bad thoughts

226
00:25:20,800 --> 00:25:29,920
or deeds or speech. The mind that is clearly aware that whatever arises ceases, that nothing in

227
00:25:29,920 --> 00:25:36,000
this world is permanent, satisfying or controllable. The mind that has no attachment or no reason

228
00:25:36,000 --> 00:25:44,080
to attach, no inclination to attach or to desire or to strive after anything. The mind that sees

229
00:25:44,080 --> 00:25:52,720
no benefit in clinging or in chasing after anything or running away from anything or controlling

230
00:25:52,720 --> 00:26:00,000
anything. The mind that sees things as they are and therefore doesn't get confused or misunderstand

231
00:26:00,000 --> 00:26:06,800
or do things or say things or think things that lead one to suffer, that lead one to be disappointed,

232
00:26:06,800 --> 00:26:14,800
that lead one to be dissatisfied. Such a person doesn't go anywhere. The mind doesn't go anywhere.

233
00:26:14,800 --> 00:26:21,200
There's no nibana is not a place. It's not a destination. It's the seventh path that doesn't lead

234
00:26:21,200 --> 00:26:28,720
anywhere. There you go. There's a good description of nibana. It's not that it's a nowhere or

235
00:26:28,720 --> 00:26:35,520
nothing. It's the non going. Nibana means the mind doesn't go anymore. It doesn't go out to the

236
00:26:35,520 --> 00:26:43,680
eye or to the ear or to the nose or to the tongue or to the body or even to thoughts and objects

237
00:26:47,440 --> 00:26:59,680
of mental cognition. Such a person, such a person is free from all suffering and is outside

238
00:26:59,680 --> 00:27:10,560
of the realm of Mara. Mara cannot find such a mind. That is the meaning of this verse and that's

239
00:27:10,560 --> 00:27:19,280
the lesson that we take away from this and the idea of all these other paths within the realm

240
00:27:19,280 --> 00:27:27,440
of Mara and our duty, our job, as being to rise above these, to free ourselves from these traces

241
00:27:27,440 --> 00:27:32,720
that we leave behind that allow Mara to track us, all of the karma, all of the things that we cling to

242
00:27:33,360 --> 00:27:40,640
that keep us tied to some Zara tied to the world tied to the universe tied to suffering.

243
00:27:42,720 --> 00:27:49,680
So another good verse and one that we can take a real lesson away from and use to help us and

244
00:27:49,680 --> 00:27:56,720
support us in our mental cultivation and our cultivation of the Buddha's teaching and the path to

245
00:27:56,720 --> 00:28:03,520
Nibana. So thank you all for tuning in. I hope this has been useful and I'll try to make more

246
00:28:03,520 --> 00:28:09,680
videos now, try to keep this series going. So I'm wishing you all to progress on the path and find

247
00:28:09,680 --> 00:28:27,440
true peace, happiness and freedom from suffering. Thank you.

