1
00:00:00,000 --> 00:00:16,160
Hello everyone, we are now broadcasting live January 3rd, 2016.

2
00:00:16,160 --> 00:00:21,120
So a lot of travel recently yesterday I went to Stony Creek.

3
00:00:21,120 --> 00:00:29,120
Today I went to Catherine's, more I'm going all the way to Toronto, and there's rumors

4
00:00:29,120 --> 00:00:36,880
that I might even be going to Taiwan in February and Thailand.

5
00:00:36,880 --> 00:00:46,800
Let's start, let's see, imagine I'll make a week trip to Thailand, it'll be like a dream.

6
00:00:46,800 --> 00:00:51,120
I'll get over jet lag when I get back.

7
00:00:51,120 --> 00:00:57,960
I'll just become accustomed to time chains and open back in Canada.

8
00:00:57,960 --> 00:01:08,080
And we're ridiculous really, but it would be nice to go and talk to Ajahn Tong about

9
00:01:08,080 --> 00:01:17,240
a new monastery, and that's running.

10
00:01:17,240 --> 00:01:27,840
Brothers in Taiwan, so I can visit him, there's a conference in Taiwan.

11
00:01:27,840 --> 00:01:33,280
The conference and I found out what it is, it's a conference on religious freedom.

12
00:01:33,280 --> 00:01:40,080
If it's the Taiwanese or concerned about their religious freedom, or religious freedom in

13
00:01:40,080 --> 00:01:46,800
China, for example, it's a big reason why they don't want to become a part of China, I think.

14
00:01:46,800 --> 00:02:08,780
So I know it's problems with religious freedom, so I'll just be tagging along with the head

15
00:02:08,780 --> 00:02:10,680
monk from Stony Creek.

16
00:02:10,680 --> 00:02:17,680
He's the one who got invited, not me, but he's invited me to go with him.

17
00:02:17,680 --> 00:02:28,800
I mean, they're very concerned, the Khmer-Krom people are, they'd like to do anything

18
00:02:28,800 --> 00:02:34,880
they can to preserve whatever religious freedom is left in South Vietnam, and that doesn't

19
00:02:34,880 --> 00:02:41,440
have a very good record for religious freedom, but practically trying to wipe out

20
00:02:41,440 --> 00:03:02,560
terra rata buddhism, Khmer-Krom, so that's the head, I think it's no other big announcement

21
00:03:02,560 --> 00:03:08,560
that are meetings today.

22
00:03:08,560 --> 00:03:09,560
Questions?

23
00:03:09,560 --> 00:03:10,560
We do.

24
00:03:10,560 --> 00:03:14,600
Well, what are the four great elements?

25
00:03:14,600 --> 00:03:20,440
I know they refer to our direct experience and not something like atoms, but I don't understand

26
00:03:20,440 --> 00:03:27,640
what exactly each element is experienced as.

27
00:03:27,640 --> 00:03:35,960
The water element doesn't experience, but it's inferred based on how the other three

28
00:03:35,960 --> 00:03:37,760
present themselves.

29
00:03:37,760 --> 00:03:48,000
So the air element is stiffness, the earth element is hardness, the fire element is heat,

30
00:03:48,000 --> 00:03:53,160
the water element is cohesion, but for example, in the roof of your mouth, the tongue

31
00:03:53,160 --> 00:03:58,280
sticks to the roof of your mouth, that's because of the water element, by the experience

32
00:03:58,280 --> 00:04:03,680
it still has tension, but the tension is there because of the cohesion, so in your hands

33
00:04:03,680 --> 00:04:10,040
stick together, you pull them apart, you feel the tension, their tension arises, not

34
00:04:10,040 --> 00:04:15,160
because you're pushing, because of the sticky.

35
00:04:15,160 --> 00:04:21,160
We're actually talking about these elements in the city market class.

36
00:04:21,160 --> 00:04:27,240
Yeah, and if you see the muggy, you see you're actually talking about blood, for example,

37
00:04:27,240 --> 00:04:35,960
blood is an example of the water element, but it's really not, I mean, I'm not expert

38
00:04:35,960 --> 00:04:45,200
on all of this, especially the water element and exactly how it works, but these are conceptual

39
00:04:45,200 --> 00:04:46,200
water.

40
00:04:46,200 --> 00:04:58,000
I think they called it mobile water and it was mobile water and something else, maybe it was

41
00:04:58,000 --> 00:05:03,760
mobile blood, I'm not sure if something was mobile.

42
00:05:03,760 --> 00:05:07,240
If I assign 10 minutes for walking in 10 minutes for sitting, will the timer sound go

43
00:05:07,240 --> 00:05:13,520
off after 10 minutes or only at the end of the 20 minute period?

44
00:05:13,520 --> 00:05:18,000
It should go off after both if it's working properly.

45
00:05:18,000 --> 00:05:22,160
I guess for some people, the timer doesn't work, but it's always worked on my phone and

46
00:05:22,160 --> 00:05:32,680
my PC, so it depends.

47
00:05:32,680 --> 00:05:37,640
Is there to a way to upload a profile picture without using gravitar?

48
00:05:37,640 --> 00:05:43,760
It should be, you can't upload, but you have to put it somewhere on the internet and link

49
00:05:43,760 --> 00:05:44,760
to it.

50
00:05:44,760 --> 00:05:53,400
We're going to have a place for you to upload it.

51
00:05:53,400 --> 00:05:57,760
I think I uploaded mine from photo bucket, but it came out wrong.

52
00:05:57,760 --> 00:05:59,080
I think it's old.

53
00:05:59,080 --> 00:06:01,120
You notice, it's now been fixed.

54
00:06:01,120 --> 00:06:03,160
Oh, did you fix that monthly?

55
00:06:03,160 --> 00:06:05,080
I uploaded a version to our website.

56
00:06:05,080 --> 00:06:06,680
Oh, thank you.

57
00:06:06,680 --> 00:06:08,080
I had always meant to do that.

58
00:06:08,080 --> 00:06:12,880
Yeah, if you upload from something like photo bucket, your photo comes out, oval and sort

59
00:06:12,880 --> 00:06:13,880
of stretch.

60
00:06:13,880 --> 00:06:14,880
Oh, that's not it.

61
00:06:14,880 --> 00:06:18,720
You uploaded a non square photo.

62
00:06:18,720 --> 00:06:28,400
It says quite clearly on the instruction, you know, the square photo, okay, so you're supposed

63
00:06:28,400 --> 00:06:30,720
to crop it as a square, then?

64
00:06:30,720 --> 00:06:31,720
Yeah.

65
00:06:31,720 --> 00:06:32,720
Okay.

66
00:06:32,720 --> 00:06:38,800
Otherwise, because I think I could probably figure it away, but I'm not clever enough to.

67
00:06:38,800 --> 00:06:45,720
I probably could, but I don't have the time to work a, it just so much easier if the

68
00:06:45,720 --> 00:06:49,320
photo is square to make a circle.

69
00:06:49,320 --> 00:06:54,920
Good to know.

70
00:06:54,920 --> 00:06:58,800
I've been practicing Kung Fu as a form of meditation for years now, and I'd like to

71
00:06:58,800 --> 00:07:04,120
know your opinion on Shaolin monks and Kung Fu meditation, maybe talking about some of

72
00:07:04,120 --> 00:07:09,200
the clear differences between this kind of practice and sports or fitness, which ultimate

73
00:07:09,200 --> 00:07:13,440
goal is just to do physical exercise in order to have a good body or be more attractive

74
00:07:13,440 --> 00:07:14,920
to others.

75
00:07:14,920 --> 00:07:15,920
Thanks in advance.

76
00:07:15,920 --> 00:07:21,800
Yeah, I don't have anything to say much, it's not what I keep.

77
00:07:21,800 --> 00:07:40,240
So, kind of made a rule of not answering questions about other people's practices.

78
00:07:40,240 --> 00:07:45,760
Do the weekly commitments tick over individually from when the button is put?

79
00:07:45,760 --> 00:07:49,840
Do the weekly commitments tick over individually from when the button is pushed?

80
00:07:49,840 --> 00:07:52,840
Or is there a time when they all refresh each week?

81
00:07:52,840 --> 00:07:56,320
If it's a weekly thing, it just gives the last seven days.

82
00:07:56,320 --> 00:07:59,280
If it's a monthly thing, it gives the last month.

83
00:07:59,280 --> 00:08:00,920
It's a daily thing.

84
00:08:00,920 --> 00:08:03,560
I think it's just the last 24 hours.

85
00:08:03,560 --> 00:08:09,440
I can't remember how it works.

86
00:08:09,440 --> 00:08:11,440
Daily might be a bit different.

87
00:08:11,440 --> 00:08:16,240
Yeah, that sounds right, because it's not like there's one point in the week where it all

88
00:08:16,240 --> 00:08:17,240
gets refreshed.

89
00:08:17,240 --> 00:08:26,120
But I can't remember if the daily, maybe there's an exception for that because the daily

90
00:08:26,120 --> 00:08:30,920
you kind of want to do it for that day, right?

91
00:08:30,920 --> 00:08:37,000
Well, with the commitments, there's kind of a neat feature I've noticed.

92
00:08:37,000 --> 00:08:40,200
If you're good with your commitment, it's green.

93
00:08:40,200 --> 00:08:44,920
If you're partly done with your commitment, it's sort of yellowish green.

94
00:08:44,920 --> 00:08:49,000
And if you haven't done your commitment at all, by the end of the day, it's red.

95
00:08:49,000 --> 00:08:52,200
So it does seem to kind of let you know your progress.

96
00:08:52,200 --> 00:08:58,400
If you've committed for so many minutes and you've only done half of that, it'll be like

97
00:08:58,400 --> 00:08:59,400
a yellowish.

98
00:08:59,400 --> 00:09:05,720
If you hover over it, it tells you the percentage.

99
00:09:05,720 --> 00:09:16,640
Is there someone criticized that because did you know that 10% of people in the world

100
00:09:16,640 --> 00:09:17,640
are colorblind?

101
00:09:17,640 --> 00:09:20,800
I can't tell the difference between red and green.

102
00:09:20,800 --> 00:09:24,200
So the color schemes are apparently not that.

103
00:09:24,200 --> 00:09:28,280
Yeah, shouldn't be red to green, but whatever.

104
00:09:28,280 --> 00:09:30,320
I'm not colorblind.

105
00:09:30,320 --> 00:09:35,560
It's not fair is it?

106
00:09:35,560 --> 00:09:38,560
If you're colorblind, you can hover over it and see the percentages.

107
00:09:38,560 --> 00:09:39,560
Yeah.

108
00:09:39,560 --> 00:09:42,560
I mean, it doesn't change anything.

109
00:09:42,560 --> 00:09:43,560
Yeah.

110
00:09:43,560 --> 00:09:46,560
Just kind of a warning.

111
00:09:46,560 --> 00:09:50,560
Yeah.

112
00:09:50,560 --> 00:09:54,600
I think the quality of my sitting meditation practice is very good, but I wonder if it can

113
00:09:54,600 --> 00:09:56,240
be improved.

114
00:09:56,240 --> 00:10:00,240
During daily meditation, is it better to meditate for as long as one is able to in a single

115
00:10:00,240 --> 00:10:13,200
setting or is it more beneficial to practice several shorter meditation sessions each day?

116
00:10:13,200 --> 00:10:23,520
Well, you should do both walking and sitting, not just single settings.

117
00:10:23,520 --> 00:10:25,280
And it's good to make it consistent.

118
00:10:25,280 --> 00:10:30,160
So if you practice, if you practice, I mean, I would say

119
00:10:30,160 --> 00:10:31,880
there's a happy medium.

120
00:10:31,880 --> 00:10:39,040
If you're practicing like 10 minutes walking, 10 minutes sitting, five times a day, the

121
00:10:39,040 --> 00:10:42,160
benefit to that is that you're continuously mindful throughout the day.

122
00:10:42,160 --> 00:10:46,520
But the detriment is for the bad side is that you're never really testing yourself.

123
00:10:46,520 --> 00:10:49,360
None of it's really serious meditation.

124
00:10:49,360 --> 00:10:55,400
Whereas if you do two hours at once and then nothing for the rest of the day, then the

125
00:10:55,400 --> 00:11:01,560
benefit is to really push yourself, but the problem is that the rest of the time, you're

126
00:11:01,560 --> 00:11:03,320
not doing any form of meditation.

127
00:11:03,320 --> 00:11:09,920
So sort of a happy medium is to do two sessions a day, maybe three, you have time, maybe

128
00:11:09,920 --> 00:11:13,640
two long ones, one in the morning, one in the morning, and then during the day, during

129
00:11:13,640 --> 00:11:18,840
work or whatever, take time to do a shorter one, because there's two aspects.

130
00:11:18,840 --> 00:11:28,040
One is pushing yourself to really the testing yourself and forcing the difficult conditions

131
00:11:28,040 --> 00:11:34,240
to come up and challenging yourself to the patient, but the other is to make it consistent

132
00:11:34,240 --> 00:11:36,240
during the day.

133
00:11:36,240 --> 00:11:50,720
To keep them both in land, they're doing once a day is not really, not doing too short

134
00:11:50,720 --> 00:12:00,480
of sessions, it's also not all that beneficial, so it should be fairly long, and a couple

135
00:12:00,480 --> 00:12:08,600
of times or a few times a day.

136
00:12:08,600 --> 00:12:11,320
Is too much setting meditation bad?

137
00:12:11,320 --> 00:12:13,240
Can you be addicted to it?

138
00:12:13,240 --> 00:12:16,320
Is meditation a need or a want?

139
00:12:16,320 --> 00:12:18,640
Is meditation a need or a want?

140
00:12:18,640 --> 00:12:22,000
Do it as a yellow question mark?

141
00:12:22,000 --> 00:12:26,480
I think I get the pass on that one.

142
00:12:26,480 --> 00:12:28,480
No, he's son.

143
00:12:28,480 --> 00:12:40,960
He's been meditating, and I think he's been meditating, just not in the last couple of hours.

144
00:12:40,960 --> 00:12:52,800
All right, he's Canadian as well, so he gets a pass, we'll give him a pass.

145
00:12:52,800 --> 00:12:55,600
Is too much setting meditation bad?

146
00:12:55,600 --> 00:12:56,600
No.

147
00:12:56,600 --> 00:13:02,640
Well, it can be harmful to the body, so you should do walking as well, and you'll be addicted

148
00:13:02,640 --> 00:13:04,680
to meditation.

149
00:13:04,680 --> 00:13:13,840
That's why not, at least intellectually, it can be an ego thing.

150
00:13:13,840 --> 00:13:19,200
I think it'd be hard pressed to lust after meditation, you know, in the same way that you

151
00:13:19,200 --> 00:13:20,520
lust after food.

152
00:13:20,520 --> 00:13:26,520
Like you see, you're sitting man here saying, oh, I think that's unlikely.

153
00:13:26,520 --> 00:13:36,480
But, you know, I suppose you could conceive of possibility, but highly unlikely.

154
00:13:36,480 --> 00:13:45,120
Meditation a need or a want, no meditation is a practice.

155
00:13:45,120 --> 00:13:58,160
You know, that worked quite up with the questions.

156
00:13:58,160 --> 00:14:07,120
All right, lots of meditators today, since the weekend, let's see who do we have, we got somebody

157
00:14:07,120 --> 00:14:12,120
from Cambodia, too many Americans as usual.

158
00:14:12,120 --> 00:14:16,120
You said it like a bad thing.

159
00:14:16,120 --> 00:14:18,120
Oh, it's a terrible thing.

160
00:14:18,120 --> 00:14:23,800
You know that, like 70% of the people who follow me on YouTube are Americans.

161
00:14:23,800 --> 00:14:30,120
Well, sure, I mean, you speak English, and there's a lot of us, but think about the

162
00:14:30,120 --> 00:14:33,120
good you're doing.

163
00:14:33,120 --> 00:14:39,120
I'm just, it just goes with the job of being Canadian, I have to criticize America.

164
00:14:39,120 --> 00:14:50,040
I know, down my Canadian nose at you, lots of Americans, hey, power to you, you're putting

165
00:14:50,040 --> 00:14:53,280
us the rest of the world to shame.

166
00:14:53,280 --> 00:14:57,800
All your meditation, Americans make really good meditators, actually, I've said this before

167
00:14:57,800 --> 00:14:59,280
many times.

168
00:14:59,280 --> 00:15:10,920
They have a certain confidence, they do things through, I mean, it's a gross generalization

169
00:15:10,920 --> 00:15:16,200
of course, but you can generalize when you've taught around the world, especially teaching

170
00:15:16,200 --> 00:15:21,640
in Thailand, I would say so many people from so many different countries.

171
00:15:21,640 --> 00:15:27,880
The Americans tend to be strong, they go for it, they finish what they set out to do.

172
00:15:27,880 --> 00:15:32,720
Americans get halfway through the course and go home because of course where we're known

173
00:15:32,720 --> 00:15:42,680
for waffling, and it's the truth, again, a gross generalization, interesting nonetheless.

174
00:15:42,680 --> 00:15:51,600
Samantha, who is Sri Lankan living in the UK, we have a Brazilian, another UK fifth

175
00:15:51,600 --> 00:15:56,240
rounder, is that a drinking reference?

176
00:15:56,240 --> 00:16:02,720
Maybe a football reference that I don't get.

177
00:16:02,720 --> 00:16:08,880
Another Canadian, two people with no country, I feel sorry for you.

178
00:16:08,880 --> 00:16:15,640
No, actually, not every country, it can be a very good thing.

179
00:16:15,640 --> 00:16:26,800
Players can be irksome, the instruments, it's no Argentina, mud and death from Argentina.

180
00:16:26,800 --> 00:16:33,480
Well, thanks everyone for joining us, a couple of people with hearts beside their names,

181
00:16:33,480 --> 00:16:39,120
we should all put hearts beside our names, send love to each other, and we won't be

182
00:16:39,120 --> 00:16:54,520
well and happy, there we go, see some hearts, all right, I'll have a good night everyone.

183
00:16:54,520 --> 00:16:55,520
You as well, fun.

184
00:16:55,520 --> 00:17:03,760
Oh, this is what I wanted to say, I think we're going to have to switch, Tuesdays and Thursdays

185
00:17:03,760 --> 00:17:14,720
are going to be done by them from now on, because my Mondays and Wednesdays are pretty busy.

186
00:17:14,720 --> 00:17:22,800
I think in Tuesdays, I don't make it more sustainable as well, it puts less pressure on

187
00:17:22,800 --> 00:17:32,840
me to have to research twice a week, I know, I was doing seven before, right, but

188
00:17:32,840 --> 00:17:41,320
what are your thoughts on the Buddhism basics, the Buddhism basics, you were thinking

189
00:17:41,320 --> 00:17:46,000
of maybe restarting the basic Buddhism series?

190
00:17:46,000 --> 00:17:55,480
Well, let's see, I can do one demo for the week and one other thing a week, but we can

191
00:17:55,480 --> 00:18:02,800
do a third one on Saturday, we can do something different on Saturday, Tuesdays, Thursdays,

192
00:18:02,800 --> 00:18:20,480
Thursdays, and then Saturday, something, weekends, probably, we could, it sounds good, okay,

193
00:18:20,480 --> 00:18:36,640
but, it's a good night, it's ready, thank you Bhante, good night.

