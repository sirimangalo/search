WEBVTT

00:00.000 --> 00:19.000
So I'm going to talk about the meditation that I practice, and this is kind of important

00:19.000 --> 00:26.000
to disclaim in the beginning because there are obviously many different kinds of meditation.

00:26.000 --> 00:31.000
So it may not be the same as the meditation that some of you practice.

00:31.000 --> 00:38.000
It may not be the same as any of you practice, but I'd ask that you keep an open mind,

00:38.000 --> 00:50.000
give it a try, and see what it does for you, see how it goes for you.

00:50.000 --> 01:04.000
In order to understand the meditation that I'm going to explain to you,

01:04.000 --> 01:11.000
there's one thing that you need to be clear about is what we mean by the word meditation.

01:11.000 --> 01:27.000
It's a word that can mean many different things.

01:27.000 --> 01:37.000
But in the practice that I'm going to explain to you, meditation means to

01:37.000 --> 01:53.000
reflect in a sense or to examine and to look at and to learn about the objects of our experience.

01:53.000 --> 02:02.000
So it's not necessarily the case that when you begin to practice you'll feel peaceful and calm,

02:02.000 --> 02:13.000
or happy, or you'll experience some kind of special or super mundane state of awareness.

02:13.000 --> 02:16.000
That's not what we mean by meditation.

02:16.000 --> 02:20.000
I mean by meditation is to look at what's already there.

02:20.000 --> 02:33.000
The very mundane experiences that you will have on a moment-to-moment basis,

02:33.000 --> 02:44.000
just sitting here in the room and to learn about them and to come to understand

02:44.000 --> 02:55.000
both the stimulus that you receive from the outside world and your reactions to it.

02:55.000 --> 03:06.000
Because therein really lives all of our difficulties and all of the problems in our lives.

03:06.000 --> 03:15.000
And therein lives all of the good things in our lives as well.

03:15.000 --> 03:34.000
But the point of meditating is to allow us to adjust our reactions

03:34.000 --> 03:40.000
that when we experience something from the outside world or when we think of something

03:40.000 --> 03:47.000
that rather than reacting in a judgmental way we're able to see it clearly for what it is

03:47.000 --> 03:58.000
and react in a way based on wisdom and understanding.

03:58.000 --> 04:04.000
So in order to do this we're actually going to meditate on the experiences that we have.

04:04.000 --> 04:09.000
We're not going to try to create some new experience or some new state of being,

04:09.000 --> 04:17.000
apart from simply clear awareness of the reality around us.

04:17.000 --> 04:23.000
To do this we have a tool that we use.

04:23.000 --> 04:30.000
And it's a tool that should be well familiar to people who know something about meditation.

04:30.000 --> 04:34.000
This tool is called a mantra.

04:34.000 --> 04:50.000
Now a mantra is a word that is most often used for the purposes of focusing on some super mundane or spiritual object.

04:50.000 --> 05:03.000
But the point of the mantra is really the focus and the clarity of focus that comes from putting a word to an object.

05:03.000 --> 05:12.000
This is one way to understand the word mindfulness or satin.

05:12.000 --> 05:19.000
In Buddhism we talk about this word satin which we translate as mindfulness.

05:19.000 --> 05:24.000
But satin really means to remind yourself or to remember something for what it is.

05:24.000 --> 05:26.000
This is the meaning of the word.

05:26.000 --> 05:31.000
This is how it's used in meditative sense.

05:31.000 --> 05:43.000
So it means rather than extrapolating on the object or projecting our own ideas about the object.

05:43.000 --> 05:46.000
We simply see it's intrinsic qualities.

05:46.000 --> 05:48.000
We see it for what it is.

05:48.000 --> 05:52.000
When we see something we know we're seeing, when we hear something we know we're hearing.

05:52.000 --> 05:55.000
When we walk we know that we're walking.

05:55.000 --> 05:58.000
And that's all that we know.

05:58.000 --> 06:02.000
The idea is that we have a one-to-one relationship with reality.

06:02.000 --> 06:17.000
We experience reality as it is rather than judging it or extrapolating upon it.

06:17.000 --> 06:19.000
So that's what the mantra gives us.

06:19.000 --> 06:23.000
It reminds us of that essential quality of the experience.

06:23.000 --> 06:28.000
It keeps us from projecting on it.

06:28.000 --> 06:37.000
Giving rise to projections or judgments and so on.

06:37.000 --> 06:43.000
So we're going to apply this mantra on our everyday experience

06:43.000 --> 06:52.000
to allow us to actually focus on many things that we wouldn't otherwise have any interest in looking at.

06:52.000 --> 06:59.000
The negative emotions that we have, if we get angry, we're actually going to meditate on the anger.

06:59.000 --> 07:08.000
Because until we come to understand the nature of things like anger or greed or conceit or arrogance or so on,

07:08.000 --> 07:23.000
all these many negative emotions, then it won't really be possible for us to be free from them.

07:23.000 --> 07:32.000
There'll always be the opportunity for them to arise because we don't yet understand how truly negative they are.

07:32.000 --> 07:41.000
We may believe them to be negative intellectually.

07:41.000 --> 07:53.000
But once we know the truth of them, once we understand the truth of them, we won't give rise to them.

07:53.000 --> 08:00.000
And moreover, when we understand the truth about the experiences that we have,

08:00.000 --> 08:08.000
we won't have any reason to become greedy or angry or conceited and so on.

08:08.000 --> 08:15.000
When we see things for what they are, we won't have any reason to give rise to any negative emotion.

08:15.000 --> 08:21.000
We won't see things in terms of good and bad, me and mine.

08:21.000 --> 08:28.000
We'll see things in terms of how they are, in terms of their intrinsic qualities.

08:28.000 --> 08:37.000
And we'll be able to live our lives at peace in any situation.

08:37.000 --> 08:43.000
And this is the idea of this practice.

08:43.000 --> 08:50.000
So when we experience something that we'd normally consider to be bad, for instance, when we have pain in the body,

08:50.000 --> 09:01.000
we'll try to simply see it as pain and keep ourselves from judging it as bad.

09:01.000 --> 09:07.000
And slowly our mind will come to accept the fact that actually pain has nothing intrinsically bad about it.

09:07.000 --> 09:10.000
The bad comes with the disliking.

09:10.000 --> 09:19.000
And as we look at the disliking, we'll come to see that the disliking is really the bad useless, unbeneficial quality.

09:19.000 --> 09:22.000
So this is basically how we're going to practice.

09:22.000 --> 09:32.000
When something arises, we're going to have a mantra, a word that allows us to focus and to see it clearly just for what it is and to remain objective about it.

09:32.000 --> 09:36.000
So instead of thinking in our mind, this is good, this is bad, I like it, I don't like it.

09:36.000 --> 09:42.000
We're going to think to ourselves, this is this, it is what it is.

09:42.000 --> 09:48.000
So for instance, when we feel the pain, we'll just remind ourselves that that's pain.

09:48.000 --> 10:02.000
When we can say to ourselves, pain, pain, pain, just simply reminding ourselves, that's what that is.

10:02.000 --> 10:11.000
In the beginning, this can be quite a difficult skill to acquire something that doesn't come easily because we're so used to proliferating.

10:11.000 --> 10:18.000
We're so used to projecting and creating many, many different thoughts about every object that arises.

10:18.000 --> 10:30.000
It's very difficult to focus the mind in such a way as we just think of it as what it is.

10:30.000 --> 10:39.000
So in the beginning, we have an exercise that we give meditators to train themselves in this skill.

10:39.000 --> 10:52.000
And so probably many of you are familiar with breath meditation where we're going to use breath meditation to allow us to see the reality of our body and mind.

10:52.000 --> 10:59.000
But we can't exactly use the breath because the breath is just an idea that we have in our minds.

10:59.000 --> 11:05.000
Our experience is actually the sensations that arise in the body.

11:05.000 --> 11:13.000
The breath coming in and going out is just an idea that we have in our mind that arises based on the sensations.

11:13.000 --> 11:16.000
The truth is that we're going to have certain sensations in the body.

11:16.000 --> 11:22.000
Most prominent is going to be in the stomach.

11:22.000 --> 11:29.000
If you haven't practiced meditation or if you live a stressful life,

11:29.000 --> 11:35.000
you may not have any sensations in the stomach or in the chest.

11:35.000 --> 11:45.000
But when you begin to practice meditation, when you've been practicing for some time, you'll find that as your body relaxes, as your breath becomes more natural,

11:45.000 --> 11:49.000
the natural tendency is to breathe through the stomach.

11:49.000 --> 11:52.000
You can verify this by lying on your back sometimes.

11:52.000 --> 12:01.000
If you lie on your back you'll find that naturally the stomach rises and falls quite prominently during the breath.

12:01.000 --> 12:06.000
So we're going to take this as our introductory exercise.

12:06.000 --> 12:13.000
This rising and falling of the stomach, which is the result of the breath.

12:13.000 --> 12:21.000
And if in the beginning it's not easy to experience, you can put your hand on your stomach.

12:21.000 --> 12:25.000
We'll try to get a feeling for it.

12:25.000 --> 12:28.000
But it's an easy object because it's very clear what's going on.

12:28.000 --> 12:33.000
There's the rising motion and then there's the falling motion.

12:33.000 --> 12:38.000
And all we're going to try to do is to see the rising for what it is and see the falling for what it is.

12:38.000 --> 12:49.000
It's just an example of something that we can see clearly, teaching the mind that when an experience arises.

12:49.000 --> 12:53.000
Teaching it to just see it for what it is.

12:53.000 --> 12:56.000
This is rising.

12:56.000 --> 13:01.000
This is falling, not judging it, not thinking about it, not wondering about it.

13:01.000 --> 13:06.000
Just knowing it is what it is.

13:06.000 --> 13:09.000
So the mantra we use is just rising and falling.

13:09.000 --> 13:15.000
When the stomach rises, we say to ourselves, rising.

13:15.000 --> 13:22.000
As it rises, when the stomach falls, we say to ourselves,

13:22.000 --> 13:23.000
falling.

13:23.000 --> 13:30.000
Rising, falling.

13:30.000 --> 13:33.000
We're not saying the mantra, loud, it's a meditation.

13:33.000 --> 13:39.000
So we're just using it in our minds to remind ourselves

13:39.000 --> 13:49.000
or to bring about an objective state of mind, reminding ourselves of the true nature of the object.

13:49.000 --> 13:53.000
So maybe we can try that all together now, everyone.

13:53.000 --> 14:01.000
If you want, you can put your hand on your stomach or otherwise, just put your hands on your lap

14:01.000 --> 14:08.000
and watch the stomach rising and falling.

14:08.000 --> 14:14.000
Again, we're just observing, so we're not trying to control the breath or force it.

14:14.000 --> 14:18.000
In the beginning, you might find the natural tendency is to try to force the breath.

14:18.000 --> 14:24.000
Force it to be smooth, force it to be deep.

14:24.000 --> 14:31.000
And this is an important observation because you begin to see how much suffering is caused by forcing things.

14:31.000 --> 14:43.000
So in the beginning, it might be quite unpleasant as you teach the mind to the disadvantages or the dangers in clinging or enforcing.

14:43.000 --> 14:50.000
And the mind begins to learn to just let the breath go as it will.

14:50.000 --> 14:57.000
So it's not a problem to go through this experience where it's uncomfortable and stressful.

14:57.000 --> 15:11.000
Because, again, we're trying to teach the mind the true nature of reality and teach the mind the...

15:11.000 --> 15:20.000
teach the mind about the negative qualities.

15:20.000 --> 15:30.000
Like clinging or wanting a version and so on.

15:30.000 --> 15:33.000
This can only be done by seeing the true nature of these things.

15:33.000 --> 15:38.000
I've seen the nature of clinging and seeing the nature of a version and so on.

15:38.000 --> 15:40.000
So just let it go with it.

15:40.000 --> 15:44.000
However your mind reacts, consider that to be a learning experience.

15:44.000 --> 15:49.000
We learn from our experiences, learn from our mistakes.

15:49.000 --> 15:58.000
And try to just let the breath go as it's going to go and remind yourself this is rising.

15:58.000 --> 16:05.000
It's just falling, say to yourself, rising, falling.

16:05.000 --> 16:31.000
I will pay attention to my notebooks.

16:31.000 --> 16:34.000
.

16:34.000 --> 16:37.000
..

16:37.000 --> 16:39.000
..

16:39.000 --> 16:41.000
..

16:41.000 --> 16:43.000
..

16:43.000 --> 16:45.000
..

16:45.000 --> 16:47.000
..

16:47.000 --> 16:49.000
..

16:49.000 --> 16:51.000
..

16:51.000 --> 16:53.000
..

16:53.000 --> 16:55.000
..

16:55.000 --> 16:59.000
..

16:59.000 --> 17:04.000
..

17:04.000 --> 17:09.000
..

17:09.000 --> 17:11.000
..

17:11.000 --> 17:12.000
..

17:12.000 --> 17:18.000
The first thing you should begin to see is how it's not as easy as it sounds.

17:18.000 --> 17:24.000
It's actually quite difficult to keep the mind focused on a single object.

17:24.000 --> 17:28.000
But it's not really the point of this meditation to focus on a single object.

17:28.000 --> 17:36.000
Our experience, our ordinary everyday experience, is not the experience of a single object.

17:36.000 --> 17:43.000
So, we can expand our meditation to include every object of our experience,

17:43.000 --> 17:52.000
and still consider it to be meditation, because we're still clearly seeing the experience for what it is.

17:52.000 --> 17:59.000
For instance, you might experience pain, or some sort of feeling arising in the body.

17:59.000 --> 18:03.000
It might be a painful feeling, you might feel happy or pleasant.

18:03.000 --> 18:14.000
You might experience great calm.

18:14.000 --> 18:19.000
And so, rather than ignoring the sensations or trying to push them away,

18:19.000 --> 18:26.000
and to find some way to be that keeps you free from the sensations.

18:26.000 --> 18:35.000
When you feel pain moving, adjusting your position, trying to avoid the experience,

18:35.000 --> 18:41.000
we're going to dive right in and be with the experience, and learn about the experience.

18:41.000 --> 18:44.000
Take it as our teacher.

18:44.000 --> 18:52.000
Because as I said, things like experiences like pain are not intrinsically negative.

18:52.000 --> 19:05.000
It's just the body expressing the tension or the nature of the experience.

19:05.000 --> 19:17.000
There's the sensation of pressure, or tension, or hardness, or so on, stiffness.

19:17.000 --> 19:27.000
But when picked up by the mind, it leads to disliking and worrying and stress about the pain.

19:27.000 --> 19:33.000
So, we're going to change the way we look at this pain, and just see it as pain.

19:33.000 --> 19:49.000
Say to yourself, pain, pain, pain, or aching, aching, or whoever it appears to.

19:49.000 --> 19:53.000
And you can just stay with the pain as long as it's there.

19:53.000 --> 19:57.000
Not trying to make it go away, not concerned about it.

19:57.000 --> 20:04.000
In any way, just being with it.

20:04.000 --> 20:11.000
This is obviously a very difficult thing to do in the beginning.

20:11.000 --> 20:22.000
But if you become skilled in this practice, you can get to the point where pain itself is no longer a problem.

20:22.000 --> 20:30.000
Because you become familiar with it, and when pain arises, it's not scary, it's not unpleasant, it's familiar.

20:30.000 --> 20:38.000
You know the truth of pain, it is what it is.

20:38.000 --> 20:44.000
You know that it's not going to go away just because you want it to go away.

20:44.000 --> 20:49.000
And you know that the more you want it to go away, the more suffering you have.

20:49.000 --> 20:53.000
And the more stressful it becomes for you.

20:53.000 --> 20:57.000
So you learn to just accept it for what it is.

20:57.000 --> 21:07.000
Say when you feel happier, you feel calm, you know that clinging to it and liking it.

21:07.000 --> 21:16.000
It's only a cause for more stress when it disappears, or more stress in trying to keep it and trying to maintain it.

21:16.000 --> 21:26.000
It means more work for you to have to keep developing the experience.

21:26.000 --> 21:30.000
And so you learn to let go of even pleasant experiences.

21:30.000 --> 21:37.000
Just to see happiness for what it is, happy, happy, happy or calm.

21:37.000 --> 21:54.000
See calm for what it is, calm, calm and let it be, let it come, let it go.

21:54.000 --> 21:56.000
Or there may be thoughts when you're meditating.

21:56.000 --> 22:01.000
You may start thinking about the past, thinking about the future.

22:01.000 --> 22:07.000
You may have good thoughts, you may have bad thoughts.

22:07.000 --> 22:10.000
Normally we think, oh, this is keeping me from meditating.

22:10.000 --> 22:14.000
My thoughts are getting in the way of my meditation, thinking too much.

22:14.000 --> 22:18.000
But really thinking too much is just a judgment.

22:18.000 --> 22:28.000
Too much of anything is just our own arbitrary measurement of what is enough and what is too much.

22:28.000 --> 22:34.000
The truth is that that's how much you're thinking, you're thinking, that's the truth.

22:34.000 --> 22:40.000
So the meditation practice is to remind ourselves that that's all that there is, it's just thinking.

22:40.000 --> 22:45.000
It's not good, it's not bad, it is what it is, thinking.

22:45.000 --> 22:55.000
So to remind ourselves, we say to ourselves, thinking, thinking every time that you think, just bring your mind back to what is real.

22:55.000 --> 22:58.000
This is just a thought.

22:58.000 --> 23:03.000
We can destroy ourselves with thoughts, thinking again and again about the same thing.

23:03.000 --> 23:11.000
I'm obsessing, worrying, stressing out until we have to take medication for our stress.

23:11.000 --> 23:19.000
And then we start stressing about the medication and so on.

23:19.000 --> 23:32.000
Once we can remind ourselves that it's just thoughts, they're just memories.

23:32.000 --> 23:42.000
That will cease to have any power over you.

23:42.000 --> 23:57.000
Thank you.

23:57.000 --> 24:21.000
And we can also be mindful or should be mindful of the emotions that arise in the mind.

24:21.000 --> 24:30.000
They can arise based on physical sensations, you'll be watching the rising and falling and suddenly you don't like it or you like it.

24:30.000 --> 24:42.000
Suddenly you start to doubt about it or you're confused or you're worried about something, worried that it's something's wrong with your body.

24:42.000 --> 24:58.000
Maybe you have feelings, painful feelings and you start to like it or you dislike them, happy feelings and you like them or you worry about your pain or you feel doubt about the feelings.

24:58.000 --> 25:09.000
Or maybe you have thoughts that you like or dislike, worry about them, stress about them, doubt about them.

25:09.000 --> 25:24.000
These sort of emotions are all considered to be hindrances in the meditation practice because they take you away from clear awareness of reality.

25:24.000 --> 25:36.000
But no fear, they're not to be rejected, they're to be understood and through understanding they will be given up naturally.

25:36.000 --> 25:45.000
The truth is someone who sees reality for what it is will never give rise to liking, disliking, wanting and addiction, clinging.

25:45.000 --> 25:52.000
They never give rise to boredom or fear or frustration or sadness.

25:52.000 --> 25:56.000
They won't have any reason for these emotions.

25:56.000 --> 26:05.000
Their mind will be at peace, at peace with reality, comfortable in any situation.

26:05.000 --> 26:16.000
Because they've seen the way reality goes, that it's not amenable to our will, to our control.

26:16.000 --> 26:24.000
And so they don't see the point of clinging or chasing away experience.

26:24.000 --> 26:34.000
Their happiness is no longer conditional, it's no longer dependent on certain types of experience.

26:34.000 --> 26:40.000
They can be happy in any situation.

26:40.000 --> 26:53.000
So to get to this point we have to come to understand the disadvantages of these negative emotion or these types of emotion.

26:53.000 --> 26:55.000
I have to come to see them for what they are objectively.

26:55.000 --> 26:59.000
We don't have to believe them to be evil or bad.

26:59.000 --> 27:09.000
We simply have to watch them and learn about all of our emotions, the good ones and the bad ones, to come to see whether they are truly good or truly bad.

27:09.000 --> 27:14.000
Useful or useless, beneficial or harmful.

27:14.000 --> 27:23.000
So when we like something we consider ourselves liking, liking, just seeing the liking for what it is.

27:23.000 --> 27:29.000
When we have disliking we consider ourselves disliking or disliking.

27:29.000 --> 27:37.000
Or when we want something we can say wanting, wanting angry or frustrated or bored or sad.

27:37.000 --> 27:45.000
We can say to ourselves, bored or frustrated or frustrated or sad or so.

27:45.000 --> 28:07.000
As the feeling appears to us.

28:07.000 --> 28:23.000
If we feel worried or stressed or distracted we can say to ourselves stressed or worried or distracted.

28:23.000 --> 28:33.000
If we have doubt or confusion we can say to ourselves doubting, doubting or confused, confused.

28:33.000 --> 28:38.000
If we feel drowsy or tired we can say to ourselves drowsy and drowsy.

28:38.000 --> 28:47.000
Whatever state of mind arises we just try to see it for what it is and let it be.

28:47.000 --> 29:00.000
Not becoming stressed about our stress or worried about our worried or angry about our anger.

29:00.000 --> 29:13.000
Giving them a fair hearing, letting them come up, judging them impartially and letting them go.

29:13.000 --> 29:20.000
So altogether this is a fairly comprehensive explanation on this sort of practice.

29:20.000 --> 29:36.000
Whenever there is nothing clearly visible or clearly perceivable in terms of thoughts or feelings or emotions.

29:36.000 --> 29:43.000
We can just come back to the breath, come back to the body because we will always have to breathe.

29:43.000 --> 29:51.000
When the stomach rises just say to ourselves, rise and the stomach falls.

29:51.000 --> 30:02.000
Or even just become aware of the fact that you are sitting if the breath doesn't seem like the easiest object to be mindful of.

30:02.000 --> 30:04.000
You can focus instead on the body.

30:04.000 --> 30:16.000
Just be aware that you are sitting, say to yourself, sitting, sitting.

30:16.000 --> 30:22.000
And then when something else comes up, shift your attention, focus on the new object.

30:22.000 --> 30:27.000
When it's gone come back again to the body.

30:27.000 --> 30:32.000
So maybe we can try practicing in this way for a little while without any talking.

30:32.000 --> 30:36.000
And then if people have questions I'm happy to take questions as well.

31:02.000 --> 31:25.000
Thank you.

31:25.000 --> 31:54.000
Thank you.

31:54.000 --> 32:17.000
Thank you.

32:17.000 --> 32:40.000
Thank you.

32:40.000 --> 33:03.000
Thank you.

33:03.000 --> 33:26.000
Thank you.

33:26.000 --> 33:49.000
Thank you.

33:49.000 --> 34:12.000
Thank you.

34:12.000 --> 34:35.000
Thank you.

34:35.000 --> 35:01.000
Thank you.

35:01.000 --> 35:27.000
Thank you.

35:27.000 --> 35:53.000
Thank you.

35:53.000 --> 36:19.000
Thank you.

36:19.000 --> 36:45.000
Thank you.

36:45.000 --> 37:11.000
Thank you.

37:11.000 --> 37:37.000
Thank you.

37:37.000 --> 38:03.000
Thank you.

38:03.000 --> 38:29.000
Thank you.

38:29.000 --> 38:55.000
Thank you.

38:55.000 --> 39:21.000
Thank you.

39:21.000 --> 39:47.000
Thank you.

39:47.000 --> 40:13.000
Thank you.

40:13.000 --> 40:39.000
Thank you.

40:39.000 --> 41:05.000
Thank you.

41:05.000 --> 41:31.000
Thank you.

41:31.000 --> 41:57.000
Thank you.

41:57.000 --> 42:23.000
Thank you.

42:23.000 --> 42:49.000
Thank you.

42:49.000 --> 43:15.000
Thank you.

43:15.000 --> 43:41.000
Thank you.

43:41.000 --> 44:07.000
Thank you.

44:07.000 --> 44:33.000
Thank you.

44:33.000 --> 44:59.000
Thank you.

44:59.000 --> 45:25.000
Thank you.

45:25.000 --> 45:51.000
Thank you.

45:51.000 --> 46:17.000
Thank you.

46:17.000 --> 46:43.000
Thank you.

46:43.000 --> 47:09.000
Thank you.

47:09.000 --> 47:35.000
Thank you.

47:39.000 --> 48:05.000
Thank you.

48:05.000 --> 48:31.000
Thank you.

48:31.000 --> 48:57.000
Thank you.

48:57.000 --> 49:23.000
Thank you.

49:23.000 --> 49:49.000
Thank you.

49:49.000 --> 50:15.000
Thank you.

50:19.000 --> 50:45.000
Thank you.

50:45.000 --> 51:11.000
Thank you.

51:11.000 --> 51:37.000
Thank you.

51:37.000 --> 52:03.000
Thank you.

52:03.000 --> 52:29.000
Thank you.

52:29.000 --> 52:55.000
Thank you.

52:55.000 --> 53:21.000
Thank you.

53:21.000 --> 53:47.000
Thank you.

53:47.000 --> 54:13.000
Thank you.

54:13.000 --> 54:39.000
Thank you.

54:39.000 --> 55:05.000
Thank you.

55:05.000 --> 55:31.000
Thank you.

55:31.000 --> 55:57.000
Thank you.

55:57.000 --> 56:23.000
Thank you.

56:23.000 --> 56:49.000
Thank you.

56:49.000 --> 57:15.000
Thank you.

57:15.000 --> 57:41.000
Thank you.

57:41.000 --> 58:07.000
Thank you.

58:07.000 --> 58:33.000
Thank you.

58:33.000 --> 58:59.000
Thank you.

58:59.000 --> 59:25.000
Thank you.

59:25.000 --> 59:51.000
Thank you.

59:51.000 --> 01:00:17.000
Thank you.

01:00:17.000 --> 01:00:43.000
Thank you.

01:00:43.000 --> 01:01:09.000
Thank you.

01:01:09.000 --> 01:01:35.000
Thank you.

01:01:35.000 --> 01:02:01.000
Thank you.

01:02:01.000 --> 01:02:27.000
Thank you.

01:02:27.000 --> 01:02:53.000
Thank you.

01:02:53.000 --> 01:03:19.000
Thank you.

01:03:19.000 --> 01:03:45.000
Thank you.

01:03:45.000 --> 01:04:11.000
Thank you.

01:04:11.000 --> 01:04:37.000
Thank you.

01:04:37.000 --> 01:05:03.000
Thank you.

01:05:03.000 --> 01:05:29.000
Thank you.

01:05:29.000 --> 01:05:55.000
Thank you.

01:05:55.000 --> 01:06:21.000
Thank you.

01:06:21.000 --> 01:06:47.000
Thank you.

01:06:47.000 --> 01:07:13.000
Thank you.

01:07:13.000 --> 01:07:39.000
Thank you.

01:07:39.000 --> 01:08:05.000
Thank you.

01:08:05.000 --> 01:08:31.000
Thank you.

01:08:31.000 --> 01:08:57.000
Thank you.

01:08:57.000 --> 01:09:23.000
Thank you.

01:09:23.000 --> 01:09:49.000
Thank you.

01:09:49.000 --> 01:10:15.000
Thank you.

01:10:15.000 --> 01:10:41.000
Thank you.

01:10:41.000 --> 01:11:07.000
Thank you.

01:11:07.000 --> 01:11:33.000
Thank you.

01:11:33.000 --> 01:11:59.000
Thank you.

01:11:59.000 --> 01:12:25.000
Thank you.

