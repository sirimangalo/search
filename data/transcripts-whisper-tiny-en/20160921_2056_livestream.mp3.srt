1
00:00:00,000 --> 00:00:21,320
Good evening everyone, welcome to our live broadcast, and finding myself doing many different

2
00:00:21,320 --> 00:00:29,160
things and running the Buddhism club at McMaster and we got a piece club as well, so I just

3
00:00:29,160 --> 00:00:37,160
came from our piecewalk on the international day of peace.

4
00:00:37,160 --> 00:00:47,480
So it's been a long day and I'm concerned that might affect my ability to answer questions

5
00:00:47,480 --> 00:00:55,840
and teach them so apologies, I'm going to try to keep up this daily broadcast, but may

6
00:00:55,840 --> 00:01:08,320
not always be as fulfilling satisfying as one language.

7
00:01:08,320 --> 00:01:31,440
But tonight we're looking at a good tourniquire book of fours, so to one set 177, Raoula Sutta.

8
00:01:31,440 --> 00:01:41,680
Why I stopped on this one is just so that we could take a moment to reflect on the four elements.

9
00:01:41,680 --> 00:01:50,760
So what the Buddha says in this, first he defines he's defining reality or a part of reality,

10
00:01:50,760 --> 00:02:01,000
the physical aspect of reality, as having four parts and those four parts are split

11
00:02:01,000 --> 00:02:09,240
into two as being internal and external and those four parts of course of the element.

12
00:02:09,240 --> 00:02:19,600
So all the physical reality is summarized under these headings, it's either internal or

13
00:02:19,600 --> 00:02:29,520
external and it's made up of the four elements, every physical, every aspect of physical

14
00:02:29,520 --> 00:02:40,440
reality, now it's important to remember and keep in mind the paradigm under which this

15
00:02:40,440 --> 00:02:45,000
functions, functions under the paradigm of experience.

16
00:02:45,000 --> 00:03:01,880
So reality is defined as moments of experience, contact between the physical and the mental.

17
00:03:01,880 --> 00:03:09,400
And as for the physical it's made up of the four elements, that's the physical aspect

18
00:03:09,400 --> 00:03:10,400
of experience.

19
00:03:10,400 --> 00:03:18,600
This has nothing about articles, subatomic particles, quarks, strings, quantum fields,

20
00:03:18,600 --> 00:03:28,800
that's only referring to what can be observed directly and what can be observed directly

21
00:03:28,800 --> 00:03:35,280
is earth, air, water, fire, that's it.

22
00:03:35,280 --> 00:03:46,880
And so when we learned about in grade 9 science we learned about the four elements and

23
00:03:46,880 --> 00:03:50,480
we learned that that was the primitive way of understanding matter before we actually knew

24
00:03:50,480 --> 00:03:51,840
what matter was.

25
00:03:51,840 --> 00:03:58,720
It's funny because we don't consider it primitive at all, we consider it real and we consider

26
00:03:58,720 --> 00:04:09,080
that physics has actually gone off script, learning about things that are, you could argue

27
00:04:09,080 --> 00:04:18,200
practically useful in terms of building bombs and that kind of thing, splitting atoms,

28
00:04:18,200 --> 00:04:26,040
but not ultimately useful in terms of relating to ultimate reality.

29
00:04:26,040 --> 00:04:35,200
That's what these four elements do, the four elements are useful in ultimate science

30
00:04:35,200 --> 00:04:47,000
because they bring us closer to the truth and clear up our misunderstandings and our useless

31
00:04:47,000 --> 00:05:03,080
improper, unwholesome, unproductive problematic, dangerous reactions to reality.

32
00:05:03,080 --> 00:05:13,320
We want to see reality for what it is, we find peace in regards to reality no longer fighting

33
00:05:13,320 --> 00:05:32,960
no longer chasing away the objects of our experience and so this is important, framework

34
00:05:32,960 --> 00:05:42,760
to remember defining matter in terms of the four elements, shifts our point of view away

35
00:05:42,760 --> 00:05:49,760
from concepts like body and hand and foot and face and man and woman and so on and so

36
00:05:49,760 --> 00:06:02,760
on to experience.

37
00:06:02,760 --> 00:06:11,520
We have the Earth element which is hardness, we feel something hard, softness, we feel

38
00:06:11,520 --> 00:06:17,280
something is soft.

39
00:06:17,280 --> 00:06:24,160
The water element is cohesion when you have this, something sticky, or not sticky.

40
00:06:24,160 --> 00:06:36,440
The fire element you have, you have heat and you have cold and the air element, you have

41
00:06:36,440 --> 00:06:45,760
attention and non-tension or fluidity and that's all you have that makes up how we experience

42
00:06:45,760 --> 00:06:56,040
the physical realm and physical aspect of reality and that's it, that's all.

43
00:06:56,040 --> 00:07:02,400
The Buddha is saying in the sutta which is of course the whole point is to see this should

44
00:07:02,400 --> 00:07:09,400
be seen correctly with right wisdom.

45
00:07:09,400 --> 00:07:19,300
And that's the Buddha comes from the sinner, same as the sinner from the sinner, should

46
00:07:19,300 --> 00:07:28,560
be seen with right wisdom, the tah Buddha as it is.

47
00:07:28,560 --> 00:07:42,040
How, dang, that neitang mama is not mine, neisohamasami, I am not that, namisohata, this is

48
00:07:42,040 --> 00:07:46,720
not myself.

49
00:07:46,720 --> 00:07:56,440
Those three, this is not mine, this I am not, this is not myself, those three are the

50
00:07:56,440 --> 00:08:13,120
description of the manifestation of the three types of clinging, cling through views,

51
00:08:13,120 --> 00:08:20,000
this is myself, you cling through conceit, I am this, you cling through craving, this

52
00:08:20,000 --> 00:08:31,040
is mine, so these three things are giving up views, conceit and craving, the three causes

53
00:08:31,040 --> 00:08:46,640
of clinging, and once you see this clearly that actually, these are not worth clinging

54
00:08:46,640 --> 00:08:53,640
to that, they are not me, they are not mine, they are not mine, they are not myself.

55
00:08:53,640 --> 00:09:02,200
When you see that clearly, they aim on this vah, having seen apoda to your pata vida to

56
00:09:02,200 --> 00:09:10,040
your nimindati, when it becomes disenchanted, you can see that there is no control, there

57
00:09:10,040 --> 00:09:20,120
is no ownership, there is no reckoning with these things, they come and go and they change

58
00:09:20,120 --> 00:09:25,720
in ways that are almost completely out of our hands.

59
00:09:25,720 --> 00:09:34,040
When you see that, how about you start to get disenchanted, you lose all this lust and desire

60
00:09:34,040 --> 00:09:44,880
for them, because you see they are just taking you on a wild goose chase, looking for

61
00:09:44,880 --> 00:09:58,400
the wild goose of satisfaction which you will never find, you end up chasing your tail.

62
00:09:58,400 --> 00:10:06,440
So when it becomes disenchanted, lose this interest, the taters can often be surprised

63
00:10:06,440 --> 00:10:11,760
by how the things that you stay interest them no longer interest them, they are just no

64
00:10:11,760 --> 00:10:22,560
longer give rise to the desire for aversion towards anything, just don't see the point,

65
00:10:22,560 --> 00:10:40,040
it's not out of, it's not because they are ignorant or uninterested or it's out of wisdom,

66
00:10:40,040 --> 00:10:44,720
because those things are not actually pleasant, those things actually don't, there's no

67
00:10:44,720 --> 00:10:59,920
resolution that can be gained from chasing them or chasing them away, they are unsatisfying.

68
00:10:59,920 --> 00:11:22,240
The mind becomes, when it hatches the mind from it, the mind that is clinging, from

69
00:11:22,240 --> 00:11:34,880
that priest's mind, from that clinging, that obsession, so that's the truth for tonight,

70
00:11:34,880 --> 00:11:41,120
but as far as I'm going to go, because I think tonight it's going to be a short session,

71
00:11:41,120 --> 00:11:46,520
we had a peacewalk and then we went on this, we had a peacewalk and then we had a bonfire

72
00:11:46,520 --> 00:11:53,320
and we sat around talking about all sorts of projects that were interested in in terms

73
00:11:53,320 --> 00:11:59,560
of cultivating peace, some of the good group, a lot of them are interested in mental health

74
00:11:59,560 --> 00:12:10,520
meditation, so I hope to get into some things in that regard.

75
00:12:10,520 --> 00:12:21,400
So without further ado, let's look at some questions, some questions, not all these questions.

76
00:12:21,400 --> 00:12:40,240
What can I do, let's just want to go to the chat, what can I do in meditation without

77
00:12:40,240 --> 00:12:44,680
a word for a concentration, there are places and things that I cannot find in now words

78
00:12:44,680 --> 00:12:45,680
for.

79
00:12:45,680 --> 00:12:49,960
Well, try me, try the word for pretty much anything.

80
00:12:49,960 --> 00:12:55,240
Feeling or knowing is often a good one, calm, quiet is one that people stumble over or forget

81
00:12:55,240 --> 00:13:05,480
about quite often, but just knowing or feeling often works as well, all right, I have

82
00:13:05,480 --> 00:13:07,120
to a good place.

83
00:13:07,120 --> 00:13:15,440
In many stories, we see many individuals wishing for something, should go and practice

84
00:13:15,440 --> 00:13:24,360
like that, at least the ways of the wise, what should be the intention of doing good deeds

85
00:13:24,360 --> 00:13:29,240
if the goal is to become an errand and you should do good deeds, thinking this will

86
00:13:29,240 --> 00:13:33,760
help me become an errand, the goal is to go to heaven, you should think this will help

87
00:13:33,760 --> 00:13:38,080
me get to heaven, the goal is to be rich, you should do things with the intention of

88
00:13:38,080 --> 00:13:46,600
making rich, but yeah, it's not a hunt, and that's so much concern with me, I get this

89
00:13:46,600 --> 00:13:48,640
without it.

90
00:13:48,640 --> 00:14:00,160
By the practice of meditation, I intend to heal my defilements, good for you, but I can't,

91
00:14:00,160 --> 00:14:05,440
I can't help myself to feel a superiority in regards to people are not aware, sometimes

92
00:14:05,440 --> 00:14:11,520
I catch myself thinking that the aran goal is just to pursue it based on ego, how can I

93
00:14:11,520 --> 00:14:15,920
reduce, well no, no, no, you see when you meditate all your defilements come up, just like

94
00:14:15,920 --> 00:14:20,160
when you do anything actually, the meditation is no different, so when you meditate you're

95
00:14:20,160 --> 00:14:26,080
going to feel equitistical about things like your meditation, it's not a problem, it is a

96
00:14:26,080 --> 00:14:31,960
problem, but it's not unexpected, but the fact that you see the aranigatistic goes great,

97
00:14:31,960 --> 00:14:37,440
I mean that's what's unique about meditation is you're in a unique position to understand

98
00:14:37,440 --> 00:14:42,880
these experiences and to see them for what they are and to see how disgusting and unpleasant

99
00:14:42,880 --> 00:14:49,320
and desirable they are, slowly get rid of them.

100
00:14:49,320 --> 00:15:02,800
Well, do enlightened mind have a need to do formal meditation practice, assuming they're

101
00:15:02,800 --> 00:15:08,920
perfect mindfulness, so enlightened mind doesn't have a need to do anything, but they do

102
00:15:08,920 --> 00:15:14,000
do things like meditation, just because they keep doing things, but having no need and no

103
00:15:14,000 --> 00:15:29,000
desire, much has been away with and when they pass away there's no more, no more rising.

104
00:15:29,000 --> 00:15:33,400
What constitutes a skillful question?

105
00:15:33,400 --> 00:15:42,440
I would say it's relative I suppose, I mean what makes something skillful and you're

106
00:15:42,440 --> 00:15:47,400
good at asking question, and if you're just asking what is it, what makes a good question?

107
00:15:47,400 --> 00:15:57,680
A good question is one that allows you to get an answer that helps you, helps you get

108
00:15:57,680 --> 00:16:06,280
closer to enlightenment, that's a good point, why are you asking the question, are you

109
00:16:06,280 --> 00:16:10,240
asking a question, when someone asks a question, are they asking it, because they're just

110
00:16:10,240 --> 00:16:14,480
curious, well it's not a very skillful thing, they're asking it because it's something

111
00:16:14,480 --> 00:16:22,440
that they believe is necessary for them to know in order to become a mind, but that seems

112
00:16:22,440 --> 00:16:26,560
a good question, even if they're wrong, the answer is no, no, you don't need to know

113
00:16:26,560 --> 00:16:33,440
that, but it's good that they ask, if they sincerely believe, but sometimes we just

114
00:16:33,440 --> 00:16:40,440
ask questions because we're lazy, if that's what curious, because whatever, those are not

115
00:16:40,440 --> 00:16:41,440
so skillful.

116
00:16:41,440 --> 00:16:49,560
I'm doing walking practice and becoming steady, it seems to happen when I'm being mindful

117
00:16:49,560 --> 00:16:50,560
and walking practice.

118
00:16:50,560 --> 00:16:57,560
It must be my mind trying to pull away from my mindfulness, when my drifts if I'm walking

119
00:16:57,560 --> 00:17:03,240
steady, well it's, yes in the beginning I mean it's awkward, you're doing something that

120
00:17:03,240 --> 00:17:10,080
is, it's maybe like you could think of it as like trying to chew gum and juggle at the

121
00:17:10,080 --> 00:17:16,640
same time or something, don't do two things at once, so it's something that your mind

122
00:17:16,640 --> 00:17:21,920
is struggling with, and when your mind struggles it stumbles and therefore the body also

123
00:17:21,920 --> 00:17:36,320
depends on your mind, but eventually it becomes more, it becomes more comfortable with it.

124
00:17:36,320 --> 00:17:40,000
I wouldn't worry about it, I mean I guess it can come even for an event when a tater

125
00:17:40,000 --> 00:17:47,160
as well, it has to do with the brain being limited and imperfect, it's why stumbling

126
00:17:47,160 --> 00:17:58,200
is an interesting experience.

127
00:17:58,200 --> 00:18:03,280
On last night's broadcast there was some kind of flying bug in the room, I caught the bug

128
00:18:03,280 --> 00:18:10,720
in my hand and I put it out the window, I don't kill, and I certainly wouldn't, well

129
00:18:10,720 --> 00:18:23,360
that's something I can tell you for sure I don't do, which would be the speed of walking

130
00:18:23,360 --> 00:18:33,720
meditation compared to regular walking, well regular walking if you start being mindful

131
00:18:33,720 --> 00:18:38,520
you realize that you're actually going somewhat quickly, so it's probably a bit slower,

132
00:18:38,520 --> 00:18:43,160
but it shouldn't feel slow, if it feels slow you're going to slow, if it feels faster

133
00:18:43,160 --> 00:18:50,440
you're going to pass, I would say mind you, an ordinary person, when you're just starting

134
00:18:50,440 --> 00:18:55,960
meditating you're probably geared to doing things quickly, so you have to maybe spend

135
00:18:55,960 --> 00:19:06,880
some time being fairly careful, start a bit slow and eventually get a sense of a natural

136
00:19:06,880 --> 00:19:12,200
calm, madam and then of course change from time to time, which shouldn't be fast

137
00:19:12,200 --> 00:19:18,840
shouldn't be slow, imagine tongue just said not too fast, not too slow, how long should

138
00:19:18,840 --> 00:19:30,440
we not, how long should we not standing, where in the body do we not standing, so up to

139
00:19:30,440 --> 00:19:36,240
you, you can actually not standing for hours if you want, the one month we did 12 hours

140
00:19:36,240 --> 00:19:40,320
of standing meditation, I wouldn't recommend it, I'd recommend going back to the rise

141
00:19:40,320 --> 00:19:46,720
of the following, it's probably a better object, but when we do walking meditation we know

142
00:19:46,720 --> 00:19:53,360
that three times standing, standing, standing in the middle, turn or walk, where in the

143
00:19:53,360 --> 00:19:58,040
body do you note standing, it's just a general sense of the feelings in the body, it doesn't

144
00:19:58,040 --> 00:20:05,800
really matter, sometimes it's different spots, just being aware of the feeling of standing,

145
00:20:05,800 --> 00:20:15,520
feeling better, better than what, I've been having burping, so I guess that's some kind

146
00:20:15,520 --> 00:20:23,000
of suffering, it's not that unpleasant then, I got medication today, that prevents acid

147
00:20:23,000 --> 00:20:28,720
production in the body, they want to test and make sure that that's all it is, so for 2 weeks

148
00:20:28,720 --> 00:20:35,960
I get to test, it's actually, I think, a little bit better today, after taking that medicine,

149
00:20:35,960 --> 00:20:40,320
if you please give an example of experiences associated with air element in the earth element,

150
00:20:40,320 --> 00:20:44,520
right, so the air element, if you feel in the stomach, the tension when you breathe in

151
00:20:44,520 --> 00:20:49,000
and the release when you breathe out, that's an example of the air element, or when you

152
00:20:49,000 --> 00:20:54,160
sit up straight, the pressure you feel in your back, that's the air element, the pressure,

153
00:20:54,160 --> 00:20:58,480
earth element is when you step on the on a hard floor and it feels hard, that's the earth

154
00:20:58,480 --> 00:21:05,840
element, when you step on a flush carpet, it feels soft, that's also the earth element.

155
00:21:05,840 --> 00:21:14,000
I don't answer questions about my practice or anybody else's practice, sorry, and that's

156
00:21:14,000 --> 00:21:21,200
all, thank you for being going easy on me or maybe I just would spend through those questions,

157
00:21:21,200 --> 00:21:31,080
but that's it, those are the questions for tonight. Apologies for not being 100% if I could

158
00:21:31,080 --> 00:21:38,800
just do one thing, but I find myself, there's an idea to start a chaplain, actually there's

159
00:21:38,800 --> 00:21:44,720
a new chaplain at McMaster, an ecumenical chaplain, seems like a nice guy, and he wants

160
00:21:44,720 --> 00:21:52,400
to bring non-Christian religions to the table, not sure how sincere he is, but he seems

161
00:21:52,400 --> 00:21:59,600
nice, nice and perhaps a Christian way, I'm not sure yet, but we'll have to see if he's

162
00:21:59,600 --> 00:22:04,480
genuinely nice then it'll be nice, so I'm just mean to say I'm involved with different

163
00:22:04,480 --> 00:22:12,600
things, peace mostly and Buddhism, but good stuff, trying to get involved with the local

164
00:22:12,600 --> 00:22:22,600
community and sort of align myself in ways that allow for the cultivation of wholesome

165
00:22:22,600 --> 00:22:32,920
interactions and groups and communities, and allow me to, you know, there's some sense

166
00:22:32,920 --> 00:22:40,520
of experimenting, from my point of view, experimenting and reaching out and dialoguing

167
00:22:40,520 --> 00:22:47,520
with people who might be interested in what I do, or not even just that, who's not

168
00:22:47,520 --> 00:22:56,600
about that, who's not even who, dialoguing with people in an effort to cultivate peace,

169
00:22:56,600 --> 00:23:01,800
and maybe not even an effort, just dialoguing with people in ways that are peaceful,

170
00:23:01,800 --> 00:23:07,240
you know, to deal with the people you have, I've made a decision to live here in Canada,

171
00:23:07,240 --> 00:23:16,560
doing what comes naturally, I should think, in terms of cultivating peace and working together.

172
00:23:16,560 --> 00:23:21,760
We complain about society, we complain about the world, but it's all changeable, it's

173
00:23:21,760 --> 00:23:30,000
all malleable, all it takes is doing what needs to be done, you complain about something,

174
00:23:30,000 --> 00:23:36,920
it's changeable, everything is, do you have the patience, do you have the purity of mind

175
00:23:36,920 --> 00:23:43,920
to see it through, and doing goodness is always worth it, mom be convey by tapunyana,

176
00:23:43,920 --> 00:24:00,920
you don't be afraid of goodness, apologies for perhaps in the near future, reducing some

177
00:24:00,920 --> 00:24:08,280
of my effort put into the online community, I won't, you know, and I only say that because

178
00:24:08,280 --> 00:24:12,600
it's actually the most important part of what I do, so I'm not suggesting I'm going to

179
00:24:12,600 --> 00:24:21,960
give it up, but I mean no longer be 100%, hopefully no still be 80% or 90%, you know,

180
00:24:21,960 --> 00:24:36,640
explain everyone, see you all again soon.

