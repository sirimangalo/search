1
00:00:00,000 --> 00:00:09,280
Hello everyone, just here today to give an update on our urban Buddhist monastery and

2
00:00:09,280 --> 00:00:16,840
meditation centre project that we've begun here in Ontario, Canada.

3
00:00:16,840 --> 00:00:31,840
The project started as an attempt to gauge interest in a potential future creation

4
00:00:31,840 --> 00:00:37,120
of a monastery and meditation centre here in Ontario, Canada.

5
00:00:37,120 --> 00:00:44,280
And the date was set to January of 2016 that if we had enough support by that time, we would

6
00:00:44,280 --> 00:00:55,040
go ahead and begin to acquire a property on a one-year lease and begin to establish

7
00:00:55,040 --> 00:00:57,240
a presence here.

8
00:00:57,240 --> 00:01:07,720
The response was so great as you can see if you've been to the project support page.

9
00:01:07,720 --> 00:01:18,760
But that in about 10 days we had received half of our expected support or hope for support.

10
00:01:18,760 --> 00:01:24,320
And so we asked the question as to whether or not we discussed the question, whether or

11
00:01:24,320 --> 00:01:33,680
not we could just go ahead, not wait till January, because in September we will be involved

12
00:01:33,680 --> 00:01:41,280
with starting up the McMaster Buddhism Association in Hamilton, Ontario at the university.

13
00:01:41,280 --> 00:01:50,640
And we'll begin to get involved with the university studying, teaching Buddhism, meditation,

14
00:01:50,640 --> 00:01:51,640
and so on.

15
00:01:51,640 --> 00:01:58,920
So the idea would be to, or the idea was that we would begin right away and establish

16
00:01:58,920 --> 00:02:04,920
our presence at the beginning of the semester school year, a September school year.

17
00:02:04,920 --> 00:02:08,360
And we agreed that this was doable and so we've gone ahead and done it.

18
00:02:08,360 --> 00:02:16,280
We now have signed a lease and we'll be moving into a property close to the McMaster

19
00:02:16,280 --> 00:02:21,040
University in September.

20
00:02:21,040 --> 00:02:26,920
So the project has changed, as well as accelerated.

21
00:02:26,920 --> 00:02:31,320
And now we're in the phase where we're looking for, looking to answer the question as

22
00:02:31,320 --> 00:02:33,400
to whether or not this is sustainable.

23
00:02:33,400 --> 00:02:40,040
We now have a one year lease, so can we sustain it for a year and have an outlook that

24
00:02:40,040 --> 00:02:42,920
to be able to extend it into the future.

25
00:02:42,920 --> 00:02:50,200
If at the end of the year we have the support, we will continue it and this will become

26
00:02:50,200 --> 00:02:56,240
a resource here in Ontario.

27
00:02:56,240 --> 00:02:59,720
So just a little bit of background on the project because if you're seeing this video

28
00:02:59,720 --> 00:03:03,720
it might be the only thing that you see about the project.

29
00:03:03,720 --> 00:03:09,160
It's on the one hand the idea is to be a resource for people here in Ontario, especially

30
00:03:09,160 --> 00:03:12,880
at the university, so for university students.

31
00:03:12,880 --> 00:03:15,880
But the idea behind that is to create a community.

32
00:03:15,880 --> 00:03:20,400
It doesn't matter where it is, this just happens to be the place where I find myself.

33
00:03:20,400 --> 00:03:28,840
So the idea was to start a community here to set up a network and a support group that

34
00:03:28,840 --> 00:03:38,120
or an organization that could carry out, maintain, support the activities that I've been

35
00:03:38,120 --> 00:03:44,360
doing more or less on my own for a long time or with the help of various people at various

36
00:03:44,360 --> 00:03:45,360
times.

37
00:03:45,360 --> 00:03:51,280
It is now to establish this such a group here as could help with things like recording

38
00:03:51,280 --> 00:04:03,280
videos, maintenance of the website and who could help organize residential courses.

39
00:04:03,280 --> 00:04:07,120
And so part of it is going to be about local activities.

40
00:04:07,120 --> 00:04:14,920
So we'll try to have daily meetings, daily sessions here with an audience or at the new property

41
00:04:14,920 --> 00:04:19,120
with an audience coming to listen to talks, to learn how to meditate, we'll have an hour

42
00:04:19,120 --> 00:04:25,680
a day where people can come in and learn how to meditate for the first time.

43
00:04:25,680 --> 00:04:29,880
We'll have daily talks and so on.

44
00:04:29,880 --> 00:04:34,960
And I will be giving talks at McMaster, it'll allow me to be closely involved with the university

45
00:04:34,960 --> 00:04:41,160
and to also provide a resource for the McMaster Buddhism Association, a location where they

46
00:04:41,160 --> 00:04:53,040
can hold activities that are better suited for a dedicated location.

47
00:04:53,040 --> 00:04:58,720
But on top of that, there will be residential courses which of course will be probably

48
00:04:58,720 --> 00:05:01,520
most beneficial to the local community.

49
00:05:01,520 --> 00:05:08,560
But on top of that, of course the other part of this is to be a support for the international

50
00:05:08,560 --> 00:05:12,200
activities that I've been doing for such a long time.

51
00:05:12,200 --> 00:05:17,000
So first of all, the residential courses are international, people do come from other countries.

52
00:05:17,000 --> 00:05:25,880
So it is a resource for people who have come here where I am now and not having moved yet.

53
00:05:25,880 --> 00:05:33,400
People have come from Europe, come from America, people have even, people visiting from

54
00:05:33,400 --> 00:05:36,720
Asia, that kind of thing.

55
00:05:36,720 --> 00:05:38,520
All over the world really.

56
00:05:38,520 --> 00:05:44,680
So there's that and there's also of course the internet activities.

57
00:05:44,680 --> 00:05:52,080
Now a lot of the things I do on the internet, as you can see, they've been sporadic and

58
00:05:52,080 --> 00:05:53,080
quickly changing.

59
00:05:53,080 --> 00:05:58,640
I tend to move from one project to another because my own situation changes my ability

60
00:05:58,640 --> 00:06:04,920
and my resources and the situation I find myself in is fluid.

61
00:06:04,920 --> 00:06:11,680
Now the idea is with a more solid location that will be able to set up a better schedule

62
00:06:11,680 --> 00:06:17,400
and with the network of people with people who can help, for example, recording and coding,

63
00:06:17,400 --> 00:06:25,520
uploading videos, be able to be more efficient and as a result get more out there, maybe

64
00:06:25,520 --> 00:06:30,200
finish off some of the YouTube projects that I haven't finished yet.

65
00:06:30,200 --> 00:06:32,400
So that's basically what it's about.

66
00:06:32,400 --> 00:06:37,560
To make it happen though, we need support and so this is what I said is we need people

67
00:06:37,560 --> 00:06:38,560
to take the initiative.

68
00:06:38,560 --> 00:06:42,880
I said I'm not going to, if we're going to do this project, it can't be under my initiative.

69
00:06:42,880 --> 00:06:48,720
There are many things as a month that I can't be the one to initiate and so we have great

70
00:06:48,720 --> 00:06:54,560
volunteers from the internet community who have stepped up to help with that but we also

71
00:06:54,560 --> 00:07:00,120
need support from the general community and so that they have one of the volunteers set

72
00:07:00,120 --> 00:07:08,360
up this online support project which this video is attached to and if not then there's

73
00:07:08,360 --> 00:07:14,600
a link in the description to the project and please check it out and if you think it's

74
00:07:14,600 --> 00:07:19,840
a worthy cause then by all means help us to make this a reality.

75
00:07:19,840 --> 00:07:28,120
So for the benefit, if you found that these teachings are beneficial then for your own

76
00:07:28,120 --> 00:07:32,520
benefit, if you think it's beneficial to others and you want to do this as a service to

77
00:07:32,520 --> 00:07:38,480
the world like the rest of us are doing then by all means become a part of something great,

78
00:07:38,480 --> 00:07:44,280
something beneficial and we're all doing this because of the importance of goodness and

79
00:07:44,280 --> 00:07:49,120
because of a belief and an understanding that goodness and happiness are close to related

80
00:07:49,120 --> 00:07:56,840
if you want to be happy this requires the undertaking of goodness if you aren't cultivating

81
00:07:56,840 --> 00:08:00,960
goodness don't expect happiness in your future.

82
00:08:00,960 --> 00:08:07,600
So this is our practice of goodness with an intention to bring happiness both to ourselves

83
00:08:07,600 --> 00:08:09,600
and to others.

84
00:08:09,600 --> 00:08:17,120
So thank you for watching this and thank you to everyone, joy and appreciation to everyone

85
00:08:17,120 --> 00:08:23,000
who has expressed their support and their appreciation and most importantly who has put

86
00:08:23,000 --> 00:08:28,720
these teachings into practice to show us that the effort is not in vain so that's all

87
00:08:28,720 --> 00:08:57,800
I have to say thank you again wishing you all the best and peace and happiness to all.

