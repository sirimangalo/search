1
00:00:00,000 --> 00:00:24,600
Okay, good evening, everyone.

2
00:00:24,600 --> 00:00:39,400
Welcome to our evening Dhamma session.

3
00:00:39,400 --> 00:00:49,920
At an interesting, something interesting happened on the bus again today.

4
00:00:49,920 --> 00:00:57,700
These are, of course, interesting places. A woman spilled her coffee on me this evening.

5
00:00:57,700 --> 00:01:06,640
Drop the whole mug between me and another woman.

6
00:01:06,640 --> 00:01:23,660
But the man in the wheelchair got back on, same guy, and asked the bus driver why his

7
00:01:23,660 --> 00:01:30,480
white, why he was growing his beard out, whether he was growing it out for the 25th, he said.

8
00:01:30,480 --> 00:01:38,080
An Indian man, I think he really didn't quite understand what Christmas was.

9
00:01:38,080 --> 00:01:47,680
He thought this bus driver with the white beard was growing it out for Christmas.

10
00:01:47,680 --> 00:02:01,360
The main thing, maybe we should talk about culture, culture is an interesting topic.

11
00:02:01,360 --> 00:02:10,760
So in this context, culture is a very conceptual thing that has to do with not only people

12
00:02:10,760 --> 00:02:19,800
but groups of people. That's not so interesting for meditators, so keeping it in the spirit

13
00:02:19,800 --> 00:02:28,120
of meditation. I mean, there are some interesting aspects of culture when it boils down

14
00:02:28,120 --> 00:02:35,120
to on an individual level, comes down to what I've been talking about recently and this

15
00:02:35,120 --> 00:02:45,800
idea of the personality. And part of our job as meditators is to recognize, therefore,

16
00:02:45,800 --> 00:02:58,040
our culture, what part of us is culture. And so these are good examples how, in modern

17
00:02:58,040 --> 00:03:17,000
globalized science, society, we find culture, culture is clashing. We can say how people

18
00:03:17,000 --> 00:03:29,320
get in the way of understanding each other, culture is something incidentally that I've

19
00:03:29,320 --> 00:03:42,160
felt quite strongly as a Western Buddhist monk. It's funny, as soon as you become a monk

20
00:03:42,160 --> 00:03:52,720
and you, especially when you don't use money, you become very close to the culture in which

21
00:03:52,720 --> 00:04:04,240
you live very much. You see the culture in a whole new way, like similar to how any other

22
00:04:04,240 --> 00:04:14,720
homeless beggar would, you're no longer privileged, you no longer have any status in society,

23
00:04:14,720 --> 00:04:22,840
so you get to see the very drags of society. And so you wind up very much caught up in

24
00:04:22,840 --> 00:04:28,400
culture. And as a foreigner, that can be quite difficult. So I've felt culture quite strongly.

25
00:04:28,400 --> 00:04:40,080
It's interesting. There's not quite interesting. It's often tied up with things like nationalism.

26
00:04:40,080 --> 00:04:47,600
It's very much caught up in the past and a identity that's very much caught up in our

27
00:04:47,600 --> 00:05:03,040
history. It's about identity and ethnicity, skin color, there's so many gender. All of these

28
00:05:03,040 --> 00:05:13,480
are very strong, very strong impact on our relationships and on our own constantly on our lives.

29
00:05:13,480 --> 00:05:36,360
We're having a debate over this new movement to try and get to create an obligation in society

30
00:05:36,360 --> 00:05:40,960
to call people by whatever gender they prefer to be called by, not just male or female,

31
00:05:40,960 --> 00:05:49,840
but could be any number of constructed genders, which is interesting as a Buddhist because

32
00:05:49,840 --> 00:06:03,840
we would agree that gender is somewhat of a construct. It's physical besides the physical,

33
00:06:03,840 --> 00:06:09,280
there's nothing, there's nothing mental about gender. So you can decide to be whatever gender

34
00:06:09,280 --> 00:06:17,200
you want to be, about smacks of egoism and identification. And actually the point that's how it

35
00:06:17,200 --> 00:06:26,640
all ties in here, culture, culture can be quite a terrible thing. And for the most part, I would

36
00:06:26,640 --> 00:06:39,760
argue it is a negative thing. Now, where culture is useful is where it protects, culture can

37
00:06:39,760 --> 00:06:46,160
sometimes be an extension of religion. So you'll often see Buddhist culture can be quite beautiful,

38
00:06:46,160 --> 00:06:51,360
can be. A lot of problems with many Buddhist countries that I've lived in just as there are

39
00:06:51,360 --> 00:06:56,480
problems everywhere, but there's some very beautiful things that you find in these cultures that

40
00:06:56,480 --> 00:07:05,200
are not exactly Buddhism. There are many methods and means of performing good deeds,

41
00:07:05,200 --> 00:07:21,200
a lot of charity work is systematized, simplified. And this result becomes a very much a part of

42
00:07:21,200 --> 00:07:30,720
people's lives. They've assimilated into who they are. I am someone who gives. Our culture is one

43
00:07:30,720 --> 00:07:38,560
of giving, for example. Our culture is one of keeping moral precepts. Our culture is one of

44
00:07:38,560 --> 00:07:49,920
meditation. Culture is a lot like an eggshell or a casing in this way. It's a protector.

45
00:07:53,200 --> 00:07:58,800
It protects religion because it becomes tradition. Religion, religious views and beliefs,

46
00:07:58,800 --> 00:08:05,440
practices, experiences become enshrined in the culture. That's where it's useful.

47
00:08:05,440 --> 00:08:13,520
The problem is we begin to take culture as religion. So we begin to take

48
00:08:15,680 --> 00:08:18,320
traditions, the practices that we've

49
00:08:22,480 --> 00:08:30,720
called that we've manufactured. We take them to be in and of themselves important.

50
00:08:30,720 --> 00:08:40,000
I was just thinking about this now. I just had a shower because I like to shower before I

51
00:08:40,000 --> 00:08:46,080
teach the dhamma and I was thinking, what a silly thing, but it's a gesture. But you see, this is

52
00:08:46,080 --> 00:08:52,240
this sort of thing. How this can become a ritual. So I do it because it feels like, well, it's

53
00:08:52,240 --> 00:08:56,800
respectful to when you're teaching the dhamma. It's actually in the text. This is a thing before

54
00:08:56,800 --> 00:09:09,120
teaching they would bathe. So keeping with tradition and out of respect for the dhamma

55
00:09:09,120 --> 00:09:16,640
because I hadn't showered today, I had a shower. But it's silly because Buddhism is not

56
00:09:16,640 --> 00:09:25,680
to talk about physical cleanliness. You could argue that it somehow gives you good

57
00:09:25,680 --> 00:09:31,440
concentration. We often have our meditators to forego showering when they get to the really intense

58
00:09:31,440 --> 00:09:39,760
parts. We tell them to stop showering because it resets things. It's a comfort. It's a way of,

59
00:09:40,480 --> 00:09:45,200
it's not such a big deal here in the West specifically talking about showering. But

60
00:09:47,200 --> 00:09:51,520
if you'd ever lived in Southeast Asia, you'd understand the importance of it because

61
00:09:51,520 --> 00:09:58,080
if you don't shower twice a day, people start to think there's something wrong with you,

62
00:09:58,080 --> 00:10:04,800
even if you don't smell. It's a big part of the discourse. Have you showered yet?

63
00:10:08,960 --> 00:10:14,320
It's a big part of Thai society anyway. So telling a Thai person not to shower is

64
00:10:14,320 --> 00:10:24,160
has a lot worse than anything. Any other thing you could tell them not to do.

65
00:10:25,840 --> 00:10:34,960
But you see, this is culture. Culture can be good. But it should never be mistaken for religion.

66
00:10:34,960 --> 00:10:47,040
Religion being the views and being the content behind the reasons why you're doing it.

67
00:10:48,320 --> 00:10:52,960
Our rituals should not be more important than the purpose for them.

68
00:10:57,360 --> 00:11:01,920
But anyway, that mostly is about culture on a conventional level.

69
00:11:01,920 --> 00:11:06,240
But I thought it's interesting because it boils down, it ties into

70
00:11:08,880 --> 00:11:11,120
a person, the inner workings of a person's mind.

71
00:11:16,320 --> 00:11:21,920
We come into the meditation practice with an identity, so much cultural baggage,

72
00:11:23,280 --> 00:11:25,680
even if we think we don't, and often we think we don't.

73
00:11:25,680 --> 00:11:32,640
Sri Lankan people don't think they are specifically somehow

74
00:11:34,160 --> 00:11:37,600
odd for being Sri Lankan. It's just the ordinary way of being,

75
00:11:37,600 --> 00:11:43,920
except when you're not a Sri Lankan or Thai or Canadian. I like to think as a Canadian,

76
00:11:43,920 --> 00:11:49,920
I don't have, I'm not cultured, but oh boy, we have a real culture.

77
00:11:49,920 --> 00:11:59,360
It's funny, I was walking down the hallway and it's so wonderful as a Canadian because it's just,

78
00:11:59,360 --> 00:12:06,240
it's our culture when you turn, because the hallways and these narrow hallways and all the

79
00:12:06,240 --> 00:12:12,160
students coming out of their classes. And so if you turn a corner and there's your facing someone,

80
00:12:12,800 --> 00:12:18,400
you say sorry, and they say sorry, and then you go on your way, that's the culture. We both

81
00:12:18,400 --> 00:12:24,400
apologize. It's a Canadian, it really is, every time. And so it was funny, I did this once,

82
00:12:25,200 --> 00:12:32,240
and I said sorry, and this, she was Asian, if you had Asian features, and she said,

83
00:12:33,520 --> 00:12:39,200
she said something like, it's okay or something like that, but like it was,

84
00:12:40,640 --> 00:12:44,160
and it was so, it was so jarring, but it was like she was trying to reassure me and I'm like,

85
00:12:44,160 --> 00:12:48,880
no, and I'm really not, I'm sad, it's just, this is what we, I didn't say that, it was just quick,

86
00:12:48,880 --> 00:12:53,280
a quick exchange, but it was the wrong exchange. She gave the wrong answer.

87
00:12:57,680 --> 00:13:03,440
So I'm definitely, we're all definitely cultured, but for an Asian person or even an American,

88
00:13:03,440 --> 00:13:08,800
this is absurd. We didn't do anything, why are you apologizing? When I do it in America, it's,

89
00:13:08,800 --> 00:13:15,440
it's quite funny, the response, they're like, what? Sorry, but now they know,

90
00:13:15,440 --> 00:13:22,960
you must be from Canada. So we come with this, this is who we are, our personalities.

91
00:13:24,080 --> 00:13:29,440
Most of us are very much have a lot of, a lot invested in our personalities, it's very important to us.

92
00:13:29,440 --> 00:13:37,920
And then we bring this to religion, we say, well, I believe

93
00:13:40,800 --> 00:13:44,160
as a Buddhist, it's quite irks from the here, people say, I believe as though it was

94
00:13:44,160 --> 00:13:48,560
meaningful, it's like, who cares? And I really don't care what you believe, it's meaningless to me.

95
00:13:50,080 --> 00:13:54,880
I mean, that's the modern society is all about what you believe, it's about expressing your

96
00:13:54,880 --> 00:14:01,760
beliefs and having a plurality of beliefs. Buddhism is like rubbish, throw it all out,

97
00:14:03,280 --> 00:14:11,200
whatever beliefs you could possibly espouse is meaningless. Now, the Buddha was, was quite

98
00:14:11,200 --> 00:14:20,080
pointed in this, he said, when you say, I believe something, what you're doing a good thing,

99
00:14:20,080 --> 00:14:24,800
you're doing a good thing because you're not saying this is true.

100
00:14:26,240 --> 00:14:29,520
If you say, I believe X, it's better than saying X is true.

101
00:14:31,360 --> 00:14:33,840
That's not better. It's, it's less committed, right?

102
00:14:35,760 --> 00:14:39,680
But it's a clever thing that he did to say this. I mean, this is the point is that

103
00:14:40,640 --> 00:14:43,200
when I say, I believe X, it's really quite meaningless.

104
00:14:45,200 --> 00:14:49,120
As far as X, it's quite meaningful in terms of understanding you.

105
00:14:49,120 --> 00:14:55,680
What a person believes has a lot more about them than it does about what is true.

106
00:15:02,880 --> 00:15:06,960
Of course, Buddhism is much more interested in what you've learned and what you know.

107
00:15:08,800 --> 00:15:12,160
And by now, we mean by having learned it, by having seen it.

108
00:15:12,160 --> 00:15:18,800
Right? The emphasis on the term vipassana, meaning to see clearly,

109
00:15:20,480 --> 00:15:23,360
it's nothing to do with belief. It's nothing to do with conjecture.

110
00:15:24,880 --> 00:15:29,280
And consequently, it's nothing to do with who you are. It's nothing to do with your personality.

111
00:15:31,680 --> 00:15:35,520
Meditation should not reaffirm who you are. It should tear it down.

112
00:15:37,040 --> 00:15:40,720
It's a challenge, the very core of who you are. It's quite disconcerting

113
00:15:40,720 --> 00:15:43,760
to have your ego ripped out from under you.

114
00:15:46,160 --> 00:15:48,720
But that's what meditation does. That's what it's for.

115
00:15:50,480 --> 00:15:53,600
And so I've often argued, I think there's an argument that could be made that

116
00:15:57,040 --> 00:15:59,840
the only culture Buddhists should have is Buddhist culture.

117
00:16:02,320 --> 00:16:03,520
And culture shouldn't be...

118
00:16:03,520 --> 00:16:11,200
Shouldn't be Canadian or American or Sri Lankan.

119
00:16:20,560 --> 00:16:25,920
And I think it's too harsh because whenever culture you're in,

120
00:16:27,200 --> 00:16:30,480
when dealing with ordinary people, you have to conform to their culture.

121
00:16:30,480 --> 00:16:37,520
But I think that goes with this point, is that a really good Buddhist can fit into any culture,

122
00:16:37,520 --> 00:16:40,800
because the only culture they have of their own is Buddhist culture.

123
00:16:45,360 --> 00:16:47,840
I don't know what that means. I don't know if that means anything.

124
00:16:48,400 --> 00:16:50,240
Buddhist culture may mean no culture.

125
00:16:52,000 --> 00:16:53,040
I think there are certain...

126
00:16:53,040 --> 00:17:00,720
certain customs of the Buddha and customs of the followers of the Buddha.

127
00:17:04,160 --> 00:17:11,680
But it's quite different from having your own mental culture, your own personality.

128
00:17:11,680 --> 00:17:26,720
And so when we all look at our cultures, when we look at our personalities, we have to understand

129
00:17:26,720 --> 00:17:32,160
that none of this is at all related to the practice. You can't bring this to the practice.

130
00:17:32,160 --> 00:17:43,120
You can't practice to promote this or to enhance your own personality.

131
00:17:44,960 --> 00:17:49,360
And in the end, the clinging to personality gets in the way of our meditation practice.

132
00:17:51,840 --> 00:17:55,440
So with all this, I'm practicing for this reason or that reason or

133
00:17:55,440 --> 00:18:01,920
one meditator today said to me, I prefer this. I think a good example.

134
00:18:03,840 --> 00:18:07,920
Because it's meaningless what you prefer. I don't really care what you prefer.

135
00:18:08,640 --> 00:18:12,800
It has no bearing on your practice or should have no bearing on your practice whatsoever,

136
00:18:12,800 --> 00:18:16,880
except the fact that you prefer it, which is an interesting state in a nutshell.

137
00:18:16,880 --> 00:18:24,640
And so the real thing is asking ourselves what our personality means.

138
00:18:25,920 --> 00:18:30,800
There may be aspects of our personality that are beneficial, that are helpful, useful for us.

139
00:18:33,680 --> 00:18:40,800
But not because there are. They should be good because they're good, they shouldn't be good

140
00:18:40,800 --> 00:18:47,280
because there are. And so we have to examine, we don't have to reject who we are, but we have

141
00:18:47,280 --> 00:18:54,320
to deconstruct it because it's only made up of moments, moments of experience that become habits.

142
00:18:55,440 --> 00:19:00,880
And like any habit they can be built up and they can be torn down. What's important is to

143
00:19:00,880 --> 00:19:07,120
see clearly so we can discern and differentiate the good habits from the bad habits.

144
00:19:07,120 --> 00:19:12,560
That's what we do in meditation.

145
00:19:14,480 --> 00:19:18,960
So I think we should all be quite careful in our

146
00:19:22,400 --> 00:19:29,600
investigation and understanding our own culture and how we've been cultured and indoctrinated

147
00:19:29,600 --> 00:19:38,080
and what we take for granted. A lot of people come to spirituality with baggage and with ideas

148
00:19:38,080 --> 00:19:43,120
about what meditation should be. It should be comfortable. It should be pleasing. It should be calming.

149
00:19:44,320 --> 00:19:50,000
It should be easy. It should be easy. I think that's the worst one. Why should it be easy?

150
00:19:50,000 --> 00:20:00,640
Why should it be easy? Why should happiness be easy? This is what we, the greatest fallacy

151
00:20:00,640 --> 00:20:04,720
of the modern world, I think, is that happiness should be easy.

152
00:20:07,600 --> 00:20:14,400
And we act in such a way. We think we're getting happiness all the time. We're getting happier and

153
00:20:14,400 --> 00:20:21,680
happier. Why it is? Look at how easy it is to get happiness. Turn on the television. It's

154
00:20:21,680 --> 00:20:28,240
happiness. Turn on the computer. It's happiness. Turn on the music. It's happiness. Open the fridge.

155
00:20:28,240 --> 00:20:34,720
It's happiness. Lie on your bed. It's happiness. So many happiness.

156
00:20:36,560 --> 00:20:39,600
So we all should be really, really happy in society.

157
00:20:39,600 --> 00:20:48,480
Or not. So funny because we think we are. We pretend that we are. If you ever ask us,

158
00:20:50,720 --> 00:20:54,560
do these things lead to happiness? You know, do these things make you happy? Of course they do.

159
00:20:57,680 --> 00:21:04,400
But I think if you ask the same people often, are you happy? I'm not admitted, but to you,

160
00:21:04,400 --> 00:21:17,040
but be quick, clear that they're actually not all that happy. Some of the most intent upon

161
00:21:17,680 --> 00:21:24,480
central gratification are often the most depressed, even suicidal.

162
00:21:24,480 --> 00:21:35,840
I went through a teenage years, intent upon complete debauchery, in all ways.

163
00:21:37,600 --> 00:21:42,400
And I've never been so unhappy. That was the most unhappy period of my life. Well, it's the

164
00:21:42,400 --> 00:21:48,560
teen years as well, but there's nothing happy about the debauchery, but chasing after central

165
00:21:48,560 --> 00:22:03,840
pleasures of all kinds. So maybe in this and in all things, we should not take anything for granted.

166
00:22:05,520 --> 00:22:09,840
It was fun. When I was in Los Angeles, I did this often with the Thai people. And one man,

167
00:22:09,840 --> 00:22:17,840
he said, he caught the trend of my thinking. Someone would say something and I'd say,

168
00:22:17,840 --> 00:22:24,160
are you sure? I did this several times and he caught me and he said, Oh, when he says,

169
00:22:24,160 --> 00:22:31,120
are you sure? You know, there's a problem. Because then I would challenge it. Thai culture can be

170
00:22:31,120 --> 00:22:38,400
very fixed and set in ways that it's not always beneficial. You know, Socrates, I think would do this.

171
00:22:38,400 --> 00:22:44,480
It's not that he didn't agree with you, is that he wondered why you took it for granted.

172
00:22:44,480 --> 00:22:49,520
So he would ask you questions about it. The Buddha would do this as well.

173
00:22:52,000 --> 00:22:56,720
You don't have to tell people that what they're doing is wrong. That's not the point. I mean,

174
00:22:56,720 --> 00:23:03,840
a bigger point is people don't question. We often don't question. We take things for granted.

175
00:23:03,840 --> 00:23:19,280
This is right. This is who we are. The whole, I mean, there's many examples. One example is

176
00:23:19,280 --> 00:23:25,040
of not eating meat. People say, if you eat meat, it's just like killing. So we have this big

177
00:23:25,040 --> 00:23:30,560
problem as Buddhists because we're not strictly vegetarians. I have teravada Buddhism anyway,

178
00:23:30,560 --> 00:23:40,240
Tibetan Buddhism as well, I think. Well, if you don't kill, how can you eat meat,

179
00:23:41,600 --> 00:23:48,240
things like that? I mean, why I bring that one up is because when you get into a conversation,

180
00:23:48,240 --> 00:23:52,480
trying to explain it, you can see that the way people look at things, it's just so different.

181
00:23:52,480 --> 00:24:04,240
And the way we look at the world, the way we look at the world as Buddhists is quite different

182
00:24:04,240 --> 00:24:10,080
from the way ordinary people look at the world. We're not concerned about people dying. We're

183
00:24:10,080 --> 00:24:16,800
concerned about the act of killing. When I eat meat, I'm not killing. I'm not doing anything harmful.

184
00:24:16,800 --> 00:24:29,280
So what this points to is the idea that in this example that ethics is something intellectual

185
00:24:29,280 --> 00:24:34,400
that you have to think about what's right and what's wrong. Buddhism, you don't think about what's

186
00:24:34,400 --> 00:24:40,080
right and what's wrong. You just have a wholesome mind state. When your mind is wholesome,

187
00:24:40,080 --> 00:24:49,280
that's ethical. Anyway, lots of examples of it.

188
00:24:53,120 --> 00:24:57,760
I thought this would be a good thing to point out to us how we take certain things for granted

189
00:24:57,760 --> 00:25:02,960
and how we come to the practice of all sorts of baggage that ties into this idea of

190
00:25:02,960 --> 00:25:10,880
meditation as being something separate from our personality. His personality is all tied up

191
00:25:10,880 --> 00:25:15,920
with ego and identification. And as you practice, you start to tear that down. You start to see it

192
00:25:16,640 --> 00:25:23,840
for what it is as a torture device, something that tortures us by forcing us to take certain

193
00:25:23,840 --> 00:25:31,360
positions and to get angry and belligerent when people threaten us or question us or

194
00:25:38,640 --> 00:25:44,160
just thinking again about this guy in the wheelchair the first time when the guy responded and

195
00:25:44,160 --> 00:25:49,360
that's really it. There's always going to be people who do stupid things, you know, who act in

196
00:25:49,360 --> 00:26:01,920
belligerent ways. But it's up to the real problem comes when we respond.

197
00:26:05,040 --> 00:26:11,920
We all have culture and our culture conflicts with each with other people and our culture

198
00:26:11,920 --> 00:26:17,040
conflicts with our experience, meaning what we want things to be, how we want things to be,

199
00:26:17,040 --> 00:26:24,000
is often different from how they are. And what we want is often based on,

200
00:26:24,720 --> 00:26:28,720
well, it's usually based on our personality, our habits, including our culture.

201
00:26:31,840 --> 00:26:35,600
And so we're always going to come up with these conflicts. The question is how we react.

202
00:26:35,600 --> 00:26:41,440
The Buddha said this for this reason. When someone gets angry, it's worse, the worse,

203
00:26:41,440 --> 00:26:48,560
it's bad to be angry, of course, but the person who replies with anger does a worse thing

204
00:26:52,720 --> 00:26:58,880
because they create the problem. This is the way it goes with all things. You're always going

205
00:26:58,880 --> 00:27:04,160
to have things like anger when you meditate, you'll get angry at times, you'll get upset at times.

206
00:27:04,160 --> 00:27:12,160
That's bad, it's not good, it's stressful, it causes you suffering, but what's worse is when

207
00:27:12,160 --> 00:27:13,920
you reply to it, get upset about it.

208
00:27:21,200 --> 00:27:28,560
Anyway, these are some thoughts on culture and on the baggage that we come to the meditation

209
00:27:28,560 --> 00:27:37,840
practice with. It's quite useful to keep in mind that who we are, who we are, should have

210
00:27:37,840 --> 00:27:44,480
no impact on the meditation, except to be studied as an object of meditation because we are

211
00:27:44,480 --> 00:27:52,400
the subject in insight meditation with other meditations, the subject or the object might be

212
00:27:52,400 --> 00:27:58,320
something external to ourselves. In this one, we are studying ourselves.

213
00:28:00,080 --> 00:28:06,720
We are both the lab technician and the lab rat.

214
00:28:06,720 --> 00:28:18,960
You learn about yourself, you learn about your culture, your views, your beliefs and so on.

215
00:28:27,360 --> 00:28:32,640
This is what leads us to freedom, this is what leads us to peace.

216
00:28:32,640 --> 00:28:45,280
Once we are able to let go of who we are, there you go, there's the demo for tonight,

217
00:28:46,320 --> 00:28:50,880
a few thoughts on culture. Thank you all for coming out.

218
00:28:53,360 --> 00:28:58,320
Apologies that we're still stuck in second life, someone commented today that they don't

219
00:28:58,320 --> 00:29:04,800
like these second life videos. Wow, there are some good sides to second life. We've actually

220
00:29:04,800 --> 00:29:12,400
got a community here. You guys can go. It's nice to be able to see that I've got an audience

221
00:29:14,240 --> 00:29:16,000
and you can do the real-time chatting.

222
00:29:16,000 --> 00:29:30,000
Well, at the same time we are recording, so people can watch on YouTube or they can listen.

223
00:29:30,000 --> 00:29:47,200
We have live audio and then the audio is recorded to the website.

224
00:29:50,160 --> 00:29:55,040
So, if anyone has any questions, I'm happy to answer them. If you want to comment,

225
00:29:55,040 --> 00:30:04,800
if you have thoughts on culture of your own, culture is such an interesting topic.

226
00:30:05,360 --> 00:30:08,560
It's interesting when you've traveled the world and seen other people's cultures.

227
00:30:10,240 --> 00:30:14,800
There's a difference between people who have left their own culture and people who haven't,

228
00:30:14,800 --> 00:30:20,880
not life-changing different, but there is something. Something you see when you've

229
00:30:20,880 --> 00:30:27,200
seen other people's cultures and realize how your own culture is not the only way people live.

230
00:30:32,960 --> 00:30:37,360
It feels like we hold the teravada culture apart from other types of Buddhism. Is this wrong?

231
00:30:40,880 --> 00:30:46,880
Well, it's a problem, right? Because other people call themselves Buddhism and they teach things

232
00:30:46,880 --> 00:30:52,000
that we say, well, that's not the same as what we teach.

233
00:30:54,640 --> 00:31:04,960
There is a problem sometimes where, yes, we cling to our school as being different from your school

234
00:31:07,680 --> 00:31:12,000
and then it becomes an identity, right? It's just another problem.

235
00:31:12,000 --> 00:31:20,720
And so what you find is that the similarities, we are blind to the similarities.

236
00:31:23,120 --> 00:31:28,800
It's funny when sometimes when I meet other types of Buddhists, you can just feel that there's

237
00:31:30,160 --> 00:31:38,160
this sort of ego and I mean, I even have friends, it feels like they're looking down upon me

238
00:31:38,160 --> 00:31:44,080
because I'm at the wrong type of Buddhist. And we do this as well and teravada Buddhism is the only

239
00:31:44,080 --> 00:31:54,800
pure Buddhism, etc, etc. But the Buddha wasn't like that. The Buddha was, if people say

240
00:31:54,800 --> 00:32:00,800
thing, he was about the teachings, right? If what they teach is true, I agree with it. What they

241
00:32:00,800 --> 00:32:05,200
teach is false. That which they teach is true. Well, I agree with that. That which they teach

242
00:32:05,200 --> 00:32:11,440
is false. Well, I disagree with that. So in that sense, we would differentiate ourselves based

243
00:32:11,440 --> 00:32:18,960
on the teachings. I'm calling yourself a teravada Buddhist is like all identities. It has a

244
00:32:18,960 --> 00:32:26,720
practical value because you don't have to explain, I believe, a, b, c, d, f, g. You can just say

245
00:32:26,720 --> 00:32:33,760
I'm a teravada Buddhist and for many people, that's already meaningful. That's a set of beliefs

246
00:32:33,760 --> 00:32:43,200
already grouped together in a useful way. So, I mean, with all things, it's about how you're

247
00:32:43,200 --> 00:32:48,720
with things like this. It's about how you use it and how you relate to it. As to having a

248
00:32:48,720 --> 00:33:00,800
teravada, actual culture though. I mean, there isn't one really, there's a Thai Buddhist culture,

249
00:33:00,800 --> 00:33:08,000
there's a Sri Lankan Buddhist culture and they're quite different. There's a Cambodian

250
00:33:08,000 --> 00:33:13,920
and Lao are fairly similar to Thai, but still different. And then there's Burmese.

251
00:33:19,280 --> 00:33:26,560
So, you know, the few aspects of actual teravada culture we might have are

252
00:33:26,560 --> 00:33:43,040
relating to our privileging of the one Buddha historical Buddha, but not even so much.

253
00:33:45,840 --> 00:33:55,360
The appreciation of the Arahant as a, as a fallen goal. I mean, just our Buddhism does

254
00:33:55,360 --> 00:34:02,880
teravada Buddhism does have a specific flavor to it. Next semester, I'm enrolled in introduction

255
00:34:02,880 --> 00:34:07,840
of Buddhism and today we just got this syllabus. I don't know if I'm actually going to fall

256
00:34:07,840 --> 00:34:13,200
through with it because it says, and I got to take the, it's the teacher's a friend of mine,

257
00:34:13,200 --> 00:34:17,840
but he's a Tibetan Buddhist and so the syllabus says we'll be studying Buddhism as its

258
00:34:17,840 --> 00:34:31,920
practice in, as it appeared in India, Tibet, China, and Japan, I think. I feel like they just

259
00:34:31,920 --> 00:34:38,560
missed the whole half of the picture. What about Southeast Asia? There'll be nothing from Sri Lanka.

260
00:34:38,560 --> 00:34:45,280
The missed that whole tradition of from Sri Lanka is that, is that the idea here?

261
00:34:47,840 --> 00:34:52,080
It's just going to be unfortunate. I'd like to talk to him about it.

262
00:34:52,080 --> 00:35:03,280
Yeah, teravada Buddhism, I guess, does have its own culture and some extent.

263
00:35:08,880 --> 00:35:13,920
As I said, I mean, I think the real simple answer is that as with all cultures, like Canadian

264
00:35:13,920 --> 00:35:18,800
culture, I think there's some really good things about it, but they shouldn't be good things

265
00:35:18,800 --> 00:35:23,840
because they're Canadian because I'm Canadian. They should be good because they're good,

266
00:35:23,840 --> 00:35:29,440
and there are a lot of things about Canadian culture that I might think is a Canadian or yes,

267
00:35:29,440 --> 00:35:34,080
I'm proud of those things, but then as a Buddhist, I think wait a minute, that's wrong.

268
00:35:35,280 --> 00:35:40,640
Like our penchant for drinking strong beer, for example. Yes, our beer's stronger than the

269
00:35:40,640 --> 00:35:49,920
Americans were proud of that. So Buddhist, not so much. So same goes with, say, teravada culture.

270
00:35:49,920 --> 00:35:55,760
It's not that it's teravada that it's good. It's good because it's good. So we would say there

271
00:35:55,760 --> 00:36:02,800
are probably aspects of teravada culture that are good, or at the very least harmless,

272
00:36:02,800 --> 00:36:09,840
when there are others that could potentially be harmful.

273
00:36:16,320 --> 00:36:19,040
I guess if you want a better answer, you'd have to give me something specific.

274
00:36:19,040 --> 00:36:29,760
No other questions?

275
00:36:39,040 --> 00:36:44,560
Yeah, culture, that's a good topic, I think. Good thing to think about. There's aspects that tie

276
00:36:44,560 --> 00:36:50,000
into our own practice. Maybe tomorrow we'll take on science. Science is the other of the three.

277
00:36:50,000 --> 00:36:59,040
I have this trifectia or this triad. Science, religion, and culture. Three very interesting things.

278
00:37:02,480 --> 00:37:04,080
Tomorrow we'll attack science.

279
00:37:04,080 --> 00:37:12,720
I suppose science. I don't know. We'll talk about science tomorrow.

280
00:37:15,760 --> 00:37:17,280
And then we'll try to put it all together.

281
00:37:20,480 --> 00:37:23,280
All right, so have a good night, everyone. Thank you all for tuning in.

282
00:37:23,280 --> 00:37:39,200
We'll show you all good practice.

