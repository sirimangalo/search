importance of metta. How important is metta meditation? It is not something I have done often, and I'm not even certain, I know how to do it correctly.
If you want a really good discussion or explanation of all four of the Brahmavi Harras,
I would recommend a book authored by Mahasi Sayada called Brahmavi Haradhamma. It is quite extensive and gives detailed instructions
on how to develop loving kindness. There are two ways of understanding metta. You can use it as a
support for your meditation, or you can use it to develop samatha, tranquility meditation, which can lead to the first three
nanas. If you want to use metta to develop samatha, this should be done with a qualified teacher
who can help lead you to the nanas. We often practice metta after we pass in a meditation
as a means of extending the wholesome states of mind, cultivated during the pastana practice.
You can also practice metta before we pass in a practice to clear up anger or aversion.
You can also practice it during the pastana practice. When you are overwhelmed with anger,
you can use metta to help set your mind straight. Metta helps with straightening out your mind because
anger and hate are based on this understanding. Metta helps remind you that relating to people
with aversion is wrong. Metta practice leads you to not just to give up the anger, but to give up
the delusion that says, this person is bad. When you send metta, you develop the right view that
it is proper for all beings to be happy. So you are not just developing love, but you are also
developing right view. It is similar to how focusing on the various parts of the body does not just
lead to giving up bodily lust, but it also leads to giving up the wrong view that this body is
beautiful.
