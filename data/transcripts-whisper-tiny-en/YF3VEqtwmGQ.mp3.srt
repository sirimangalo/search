1
00:00:00,000 --> 00:00:07,000
Okay, good evening, everyone.

2
00:00:07,000 --> 00:00:16,000
Welcome to our question and answer session.

3
00:00:16,000 --> 00:00:34,000
So we've started our broadcast of these buttons to push.

4
00:00:34,000 --> 00:00:37,000
All right.

5
00:00:37,000 --> 00:00:40,000
We have 11 questions tonight.

6
00:00:40,000 --> 00:00:52,000
There were only a few last week. It seems that over the summer things slowed down, but either way fewer questions lately, which is fine.

7
00:00:52,000 --> 00:01:02,000
So I didn't do Q&A last week. We've got them saved up.

8
00:01:02,000 --> 00:01:09,000
Okay, so let's start. It looks like some mostly questions about meditation. It's a good selection.

9
00:01:09,000 --> 00:01:15,000
What is the really, what is the real meaning of samadhi?

10
00:01:15,000 --> 00:01:23,000
Are samadhi an enlightenment the same?

11
00:01:23,000 --> 00:01:26,000
So samadhi is more general.

12
00:01:26,000 --> 00:01:32,000
Samadhi means concentration or focus.

13
00:01:32,000 --> 00:01:39,000
It's when your mind is quiet. They're not exactly quiet, but that's associated.

14
00:01:39,000 --> 00:01:49,000
Quiet means positive, but it's a part of it.

15
00:01:49,000 --> 00:01:57,000
When you're focused, it's a one part of

16
00:01:57,000 --> 00:02:04,000
the state of mind of a meditator. It's the part that is

17
00:02:04,000 --> 00:02:10,000
able to clearly understand the experience.

18
00:02:10,000 --> 00:02:18,000
It's like a camera that is in focus is clearly able to capture the subject.

19
00:02:18,000 --> 00:02:30,000
Concentration is that sort of strength or intensity of the mind,

20
00:02:30,000 --> 00:02:37,000
some another strength of the mind, the power of the mind that is able to focus.

21
00:02:37,000 --> 00:02:44,000
That isn't diffuse. That isn't scattered or quick to move or lightly touching the object.

22
00:02:44,000 --> 00:02:48,000
It's the taking of the object with strength.

23
00:02:48,000 --> 00:02:52,000
Samadhi is in the mulakasuta.

24
00:02:52,000 --> 00:02:58,000
But it says sati is the driving force.

25
00:02:58,000 --> 00:03:00,000
Samadhi is the driving force.

26
00:03:00,000 --> 00:03:02,000
It's like if you have a car,

27
00:03:02,000 --> 00:03:05,000
Samadhi is the power of the car,

28
00:03:05,000 --> 00:03:11,000
or it's the direction of the car.

29
00:03:11,000 --> 00:03:14,000
When you're not just all over the road,

30
00:03:14,000 --> 00:03:17,000
you're headed in one direction, sati is the driver.

31
00:03:17,000 --> 00:03:20,000
So they work together.

32
00:03:20,000 --> 00:03:23,000
But enlightenment is a kind of a samadhi,

33
00:03:23,000 --> 00:03:26,000
because the mind is so powerful.

34
00:03:26,000 --> 00:03:28,000
It's like the ultimate samadhi,

35
00:03:28,000 --> 00:03:30,000
but it's the term.

36
00:03:30,000 --> 00:03:33,000
But that's just technically speaking,

37
00:03:33,000 --> 00:03:37,000
because samadhi is refers to the state of not being distracted

38
00:03:37,000 --> 00:03:41,000
while enlightenment is the ultimate non-distraction.

39
00:03:41,000 --> 00:03:46,000
Does devotion play a role in meditation?

40
00:03:46,000 --> 00:03:51,000
When it depends what you mean by devotion.

41
00:03:51,000 --> 00:03:53,000
If you're devoted to the meditation,

42
00:03:53,000 --> 00:03:55,000
then yes, that's a very important part.

43
00:03:55,000 --> 00:03:58,000
If you're kind of half-hearted about it,

44
00:03:58,000 --> 00:04:00,000
then it's far less effective.

45
00:04:00,000 --> 00:04:05,000
But if you mean being devoted to God or to Buddha,

46
00:04:05,000 --> 00:04:08,000
or something like that,

47
00:04:08,000 --> 00:04:10,000
maybe being devoted to the Buddha,

48
00:04:10,000 --> 00:04:13,000
having faith or confidence or that sort of thing,

49
00:04:13,000 --> 00:04:16,000
or just conviction,

50
00:04:16,000 --> 00:04:18,000
yeah, it can be helpful certainly,

51
00:04:18,000 --> 00:04:20,000
but in a much more roundabout way,

52
00:04:20,000 --> 00:04:23,000
it's just a strengthening factor.

53
00:04:23,000 --> 00:04:25,000
When you have that in your mind,

54
00:04:25,000 --> 00:04:27,000
it helps strengthen the mind.

55
00:04:27,000 --> 00:04:37,000
I'm trying to do meditation when I feel pain.

56
00:04:37,000 --> 00:04:42,000
What should I do?

57
00:04:42,000 --> 00:04:44,000
I feel like I can't bear the pain

58
00:04:44,000 --> 00:04:45,000
and I stand up and check the time,

59
00:04:45,000 --> 00:04:47,000
and that is 45 minutes.

60
00:04:47,000 --> 00:04:49,000
Maybe 45 minutes is fine.

61
00:04:49,000 --> 00:04:51,000
It sounds like you're not doing walking meditation,

62
00:04:51,000 --> 00:04:52,000
though.

63
00:04:52,000 --> 00:04:54,000
You should be doing walking first and then sitting

64
00:04:54,000 --> 00:04:56,000
and you don't have to do so much sitting.

65
00:04:56,000 --> 00:04:58,000
You see, sitting for a long time can be painful.

66
00:04:58,000 --> 00:05:00,000
Well, to have walking,

67
00:05:00,000 --> 00:05:04,000
you'll find it less painful.

68
00:05:04,000 --> 00:05:06,000
Music is an attachment.

69
00:05:06,000 --> 00:05:10,000
Is there an importance of music in life?

70
00:05:10,000 --> 00:05:12,000
I don't really understand the question,

71
00:05:12,000 --> 00:05:20,000
but I don't really think of what importance music might have.

72
00:05:20,000 --> 00:05:24,000
If you're talking in terms of importance,

73
00:05:24,000 --> 00:05:29,000
it's like asking what importance cheesecake has in your life,

74
00:05:29,000 --> 00:05:31,000
something like that.

75
00:05:31,000 --> 00:05:33,000
It's not really an...

76
00:05:33,000 --> 00:05:36,000
I don't think it has any importance,

77
00:05:36,000 --> 00:05:42,000
but the way you ask it is kind of belies

78
00:05:42,000 --> 00:05:46,000
your conception of what life is like,

79
00:05:46,000 --> 00:05:51,000
that somehow something like music could have a play role in it,

80
00:05:51,000 --> 00:05:54,000
you know, for someone living in the world,

81
00:05:54,000 --> 00:05:56,000
may very well be that from their point of view,

82
00:05:56,000 --> 00:05:58,000
music is a very important part of life

83
00:05:58,000 --> 00:06:01,000
because their conception of what life should be.

84
00:06:01,000 --> 00:06:05,000
It's quite different from the conception of what life should be as a Buddhist.

85
00:06:09,000 --> 00:06:13,000
These multiple questions I did, oh, I wasn't at the top, was I?

86
00:06:13,000 --> 00:06:16,000
Sorry, I think I was at the bottom.

87
00:06:16,000 --> 00:06:18,000
Questions with multiple questions

88
00:06:18,000 --> 00:06:23,000
and then I'm going to be a little bit harsh and it's not fair.

89
00:06:23,000 --> 00:06:24,000
Right?

90
00:06:24,000 --> 00:06:27,000
Each question has to have a separate entry,

91
00:06:27,000 --> 00:06:30,000
so I'll just delete them and have you ask them again,

92
00:06:30,000 --> 00:06:33,000
just to be fair.

93
00:06:33,000 --> 00:06:36,000
Fair to do, I'm not quite sure, but

94
00:06:36,000 --> 00:06:41,000
make that a rule.

95
00:06:41,000 --> 00:06:43,000
If parents want you to socialize,

96
00:06:43,000 --> 00:06:44,000
didn't I answer this one?

97
00:06:44,000 --> 00:06:45,000
This one sounds very familiar.

98
00:06:45,000 --> 00:06:47,000
No, this is one I read,

99
00:06:47,000 --> 00:06:49,000
didn't answer last week, I guess.

100
00:06:49,000 --> 00:06:52,000
If parents want you to socialize with other relatives

101
00:06:52,000 --> 00:06:54,000
and eat together and do other meaningless things,

102
00:06:54,000 --> 00:06:56,000
how to decide where to draw the line.

103
00:06:56,000 --> 00:06:59,000
Well, relationships with your relatives

104
00:06:59,000 --> 00:07:02,000
is a practical thing in the world.

105
00:07:02,000 --> 00:07:06,000
It's quite useful for maintaining some sort of harmony

106
00:07:06,000 --> 00:07:09,000
and a sort of stability in your life.

107
00:07:09,000 --> 00:07:14,000
The support of relatives can be very powerful and beneficial

108
00:07:14,000 --> 00:07:17,000
to all concerned.

109
00:07:17,000 --> 00:07:18,000
So that's really the only thing

110
00:07:18,000 --> 00:07:20,000
that you would have to keep in mind there.

111
00:07:20,000 --> 00:07:23,000
You don't want to jeopardize

112
00:07:23,000 --> 00:07:27,000
that or create chaos or conflict or stress

113
00:07:27,000 --> 00:07:30,000
with people who you're probably going to spend

114
00:07:30,000 --> 00:07:32,000
a fair amount of time with.

115
00:07:32,000 --> 00:07:34,000
That being said,

116
00:07:34,000 --> 00:07:37,000
if you're dedicated to the Dhamma,

117
00:07:37,000 --> 00:07:42,000
really you should give up ties to other people

118
00:07:42,000 --> 00:07:46,000
and not be so concerned about not hurting people's feelings

119
00:07:46,000 --> 00:07:47,000
or so on.

120
00:07:47,000 --> 00:07:49,000
Because we don't really have time for that.

121
00:07:49,000 --> 00:07:51,000
If you just spend all your time worrying

122
00:07:51,000 --> 00:07:53,000
about making everyone else happy,

123
00:07:53,000 --> 00:07:55,000
you die before you actually cultivate it

124
00:07:55,000 --> 00:07:58,000
anything worthwhile in yourself.

125
00:08:00,000 --> 00:08:03,000
Helping others is fine.

126
00:08:03,000 --> 00:08:06,000
In so far as it leads to peace,

127
00:08:06,000 --> 00:08:10,000
but when it just leads to distraction

128
00:08:10,000 --> 00:08:17,000
and mutual idleness

129
00:08:17,000 --> 00:08:20,000
or distraction from goodness

130
00:08:20,000 --> 00:08:22,000
where it's just about idle chatter

131
00:08:22,000 --> 00:08:26,000
and idle gossip and so on.

132
00:08:26,000 --> 00:08:29,000
I don't think you should just discard

133
00:08:29,000 --> 00:08:31,000
your family and your relatives.

134
00:08:31,000 --> 00:08:33,000
They should certainly put it in perspective

135
00:08:33,000 --> 00:08:35,000
and understand that we're born alone,

136
00:08:35,000 --> 00:08:37,000
we die alone

137
00:08:37,000 --> 00:08:40,000
what's most important is our own journey

138
00:08:40,000 --> 00:08:41,000
for everyone

139
00:08:41,000 --> 00:08:43,000
for always concerned with other people

140
00:08:43,000 --> 00:08:45,000
that we're going to get anywhere,

141
00:08:45,000 --> 00:08:48,000
any of us.

142
00:08:56,000 --> 00:08:58,000
How can I be mentally kinder

143
00:08:58,000 --> 00:09:01,000
and reduce judging the others?

144
00:09:01,000 --> 00:09:04,000
You see people doing

145
00:09:04,000 --> 00:09:08,000
unmindful things and you judge it.

146
00:09:08,000 --> 00:09:10,000
This isn't really the right way

147
00:09:10,000 --> 00:09:13,000
of looking at things from a meditative perspective

148
00:09:13,000 --> 00:09:15,000
because we're not about

149
00:09:15,000 --> 00:09:20,000
working to be this way or that way.

150
00:09:20,000 --> 00:09:23,000
For meditation in our tradition

151
00:09:23,000 --> 00:09:24,000
or from our understanding,

152
00:09:24,000 --> 00:09:28,000
our perspective is about

153
00:09:28,000 --> 00:09:30,000
coming to understand things.

154
00:09:30,000 --> 00:09:33,000
So the right perspective on the situation

155
00:09:33,000 --> 00:09:37,000
is to observe and

156
00:09:37,000 --> 00:09:41,000
I come to understand and appreciate

157
00:09:41,000 --> 00:09:45,000
the stress and suffering

158
00:09:45,000 --> 00:09:49,000
that comes from being unkind and judgmental

159
00:09:49,000 --> 00:09:51,000
because that's really what you're experiencing.

160
00:09:51,000 --> 00:09:52,000
You're experiencing that,

161
00:09:52,000 --> 00:09:54,000
this is not something I like about myself

162
00:09:54,000 --> 00:09:55,000
and I'm this way.

163
00:09:55,000 --> 00:09:57,000
And if you observe that enough,

164
00:09:57,000 --> 00:09:59,000
it won't be just about not liking it

165
00:09:59,000 --> 00:10:01,000
about yourself, it'll be about seeing

166
00:10:01,000 --> 00:10:02,000
how stressful and unpleasant it is.

167
00:10:02,000 --> 00:10:04,000
If you spend your time trying to be

168
00:10:04,000 --> 00:10:06,000
kinder and less judgmental

169
00:10:06,000 --> 00:10:08,000
at its fake, it's not real,

170
00:10:08,000 --> 00:10:11,000
it's not based on your own

171
00:10:11,000 --> 00:10:13,000
intrinsic understanding.

172
00:10:13,000 --> 00:10:16,000
It's based on some cerebral thing

173
00:10:16,000 --> 00:10:18,000
that really is easy to discern

174
00:10:18,000 --> 00:10:24,000
from true objectivity

175
00:10:24,000 --> 00:10:33,000
which comes from wisdom.

176
00:10:33,000 --> 00:10:35,000
Sorrow, lamentation, pain, grief,

177
00:10:35,000 --> 00:10:36,000
despair, and other states,

178
00:10:36,000 --> 00:10:39,000
anger, lust, desire, feelings,

179
00:10:39,000 --> 00:10:41,000
lust, desire.

180
00:10:41,000 --> 00:10:43,000
Are these feelings or formations?

181
00:10:43,000 --> 00:10:45,000
What about heat, cold and physical pain

182
00:10:45,000 --> 00:10:47,000
would those be feelings or formations?

183
00:10:47,000 --> 00:10:49,000
So if you're talking in terms of

184
00:10:49,000 --> 00:10:50,000
the five aggregates,

185
00:10:50,000 --> 00:10:53,000
the fourth aggregate is

186
00:10:53,000 --> 00:10:55,000
a lot of different things.

187
00:10:55,000 --> 00:10:57,000
What makes it unique,

188
00:10:57,000 --> 00:10:59,000
it's qualities of mind,

189
00:10:59,000 --> 00:11:05,000
so it's the associated states

190
00:11:05,000 --> 00:11:08,000
involved with an experience.

191
00:11:08,000 --> 00:11:10,000
So it's really everything else,

192
00:11:10,000 --> 00:11:12,000
but it's mental, it's in the mind.

193
00:11:12,000 --> 00:11:15,000
It's associated with the experience.

194
00:11:15,000 --> 00:11:19,000
The main quality of it is

195
00:11:19,000 --> 00:11:24,000
our is judgment or response,

196
00:11:24,000 --> 00:11:27,000
how an experience is processed.

197
00:11:27,000 --> 00:11:29,000
That's in Sunkara.

198
00:11:29,000 --> 00:11:31,000
So it's mental formations.

199
00:11:31,000 --> 00:11:33,000
It's the formations that come about

200
00:11:33,000 --> 00:11:35,000
because of an experience.

201
00:11:35,000 --> 00:11:36,000
So the experience you have is root

202
00:11:36,000 --> 00:11:38,000
by the physical aspect of the experience.

203
00:11:38,000 --> 00:11:40,000
We need to recognize the feeling,

204
00:11:40,000 --> 00:11:43,000
the pain, the pleasure of the calmness of it.

205
00:11:43,000 --> 00:11:46,000
Sanya is the recognition.

206
00:11:46,000 --> 00:11:48,000
It's this or that.

207
00:11:48,000 --> 00:11:50,000
This thing or that thing.

208
00:11:50,000 --> 00:11:52,000
It's recognizing that it's like

209
00:11:52,000 --> 00:11:54,000
something that you experienced before.

210
00:11:54,000 --> 00:11:56,000
Sunkara is what you think of the object,

211
00:11:56,000 --> 00:11:58,000
how you appreciate it,

212
00:11:58,000 --> 00:11:59,000
how you react to it,

213
00:11:59,000 --> 00:12:00,000
how you judge it.

214
00:12:00,000 --> 00:12:03,000
And Vinyane is the consciousness of it.

215
00:12:03,000 --> 00:12:06,000
It's the overarching aspect of experience.

216
00:12:06,000 --> 00:12:10,000
It's the actual experiencing of it.

217
00:12:10,000 --> 00:12:13,000
So anger, lust, desire,

218
00:12:13,000 --> 00:12:15,000
those are formations.

219
00:12:15,000 --> 00:12:19,000
Heat and cold are rupa.

220
00:12:19,000 --> 00:12:23,000
They're the dejodat or the fire element.

221
00:12:23,000 --> 00:12:26,000
Physical pain, that's way done.

222
00:12:26,000 --> 00:12:29,000
I mean, there really is no such thing as physical pain

223
00:12:29,000 --> 00:12:31,000
in the sense that the body

224
00:12:31,000 --> 00:12:34,000
doesn't find anything painful.

225
00:12:34,000 --> 00:12:37,000
But when the painful aspect of it is mental,

226
00:12:37,000 --> 00:12:48,000
that's why feelings are considered mental.

227
00:12:48,000 --> 00:12:50,000
When practicing walking meditation

228
00:12:50,000 --> 00:12:52,000
should the breath and step synchronize?

229
00:12:52,000 --> 00:12:53,000
No.

230
00:12:53,000 --> 00:12:55,000
Walking is about watching your foot move.

231
00:12:55,000 --> 00:12:57,000
So the breath should have.

232
00:12:57,000 --> 00:12:58,000
If you notice the breath,

233
00:12:58,000 --> 00:13:02,000
you shouldn't focus on the walking.

234
00:13:02,000 --> 00:13:04,000
Is it necessary to practice walking

235
00:13:04,000 --> 00:13:06,000
and sitting in succession?

236
00:13:06,000 --> 00:13:08,000
It's not necessary,

237
00:13:08,000 --> 00:13:10,000
but they support each other.

238
00:13:10,000 --> 00:13:12,000
So it's a good idea to do them together

239
00:13:12,000 --> 00:13:16,000
if you're going to be diligent about it.

240
00:13:20,000 --> 00:13:21,000
Sometimes when I meditate,

241
00:13:21,000 --> 00:13:23,000
I feel pain for long breeds of time.

242
00:13:23,000 --> 00:13:25,000
Is it going to stay with that all the time?

243
00:13:25,000 --> 00:13:27,000
Or should I try to return to the stomach?

244
00:13:27,000 --> 00:13:28,000
Stay with it for a while.

245
00:13:28,000 --> 00:13:29,000
After a while,

246
00:13:29,000 --> 00:13:34,000
if it becomes sort of banal or your mind is no longer

247
00:13:34,000 --> 00:13:36,000
concerned with it,

248
00:13:36,000 --> 00:13:38,000
you've kind of really observed it

249
00:13:38,000 --> 00:13:40,000
for quite some time

250
00:13:40,000 --> 00:13:42,000
and really observed it for all its worth,

251
00:13:42,000 --> 00:13:44,000
then you can just ignore it

252
00:13:44,000 --> 00:13:46,000
and go back to the rising and falling.

253
00:13:46,000 --> 00:13:48,000
If your mind gets taken away,

254
00:13:48,000 --> 00:13:52,000
do it again, then go back to it again.

255
00:13:52,000 --> 00:13:54,000
Is there a need or benefit to practice

256
00:13:54,000 --> 00:13:56,000
somatow or is just practicing

257
00:13:56,000 --> 00:13:59,000
Vipasana enough to attain full enlightenment?

258
00:13:59,000 --> 00:14:02,000
So it's a big debate.

259
00:14:02,000 --> 00:14:05,000
But when you say somatow and Vipasana,

260
00:14:05,000 --> 00:14:07,000
as separate practices,

261
00:14:07,000 --> 00:14:09,000
you're talking about something that sort of

262
00:14:09,000 --> 00:14:12,000
elucidated more clearly in the commentaries,

263
00:14:12,000 --> 00:14:14,000
and especially the Risudimaga.

264
00:14:14,000 --> 00:14:17,000
So somatow practice has outlined in Risudimaga

265
00:14:17,000 --> 00:14:18,000
is not necessary.

266
00:14:18,000 --> 00:14:19,000
It's helpful,

267
00:14:19,000 --> 00:14:22,000
and there's a whole huge section devoted to it.

268
00:14:22,000 --> 00:14:24,000
So is there a benefit?

269
00:14:24,000 --> 00:14:26,000
Yes, there certainly is a benefit.

270
00:14:26,000 --> 00:14:27,000
Is there necessity?

271
00:14:27,000 --> 00:14:32,000
There's not necessity.

272
00:14:32,000 --> 00:14:35,000
Because somatow and Vipasana is qualities,

273
00:14:35,000 --> 00:14:38,000
come about really together.

274
00:14:38,000 --> 00:14:40,000
If you're practicing Vipasana,

275
00:14:40,000 --> 00:14:41,000
if you're practicing,

276
00:14:41,000 --> 00:14:44,000
yeah, when you practice proper Vipasana,

277
00:14:44,000 --> 00:14:45,000
they both come together

278
00:14:45,000 --> 00:14:48,000
and you just practice certain types of meditation,

279
00:14:48,000 --> 00:14:49,000
like maintenance,

280
00:14:49,000 --> 00:14:52,000
so only somatow will arise.

281
00:14:52,000 --> 00:14:56,000
That's why there's a distinction.

282
00:14:56,000 --> 00:14:59,000
Many thoughts of anger and fear,

283
00:14:59,000 --> 00:15:00,000
in order to limit,

284
00:15:00,000 --> 00:15:04,000
should I practice Vipasana or simply observe?

285
00:15:04,000 --> 00:15:07,000
It can be ever more mindful to stop it.

286
00:15:07,000 --> 00:15:10,000
So ideally you're mostly focusing on mindfulness,

287
00:15:10,000 --> 00:15:11,000
but even then,

288
00:15:11,000 --> 00:15:13,000
Vipasana is a good thing to practice.

289
00:15:13,000 --> 00:15:16,000
My first teacher was a layman,

290
00:15:16,000 --> 00:15:18,000
but he was pretty keen

291
00:15:18,000 --> 00:15:20,000
that after we do every sitting

292
00:15:20,000 --> 00:15:24,000
that we do in meditation.

293
00:15:24,000 --> 00:15:28,000
Not agenda, agenda doesn't mention it too much,

294
00:15:28,000 --> 00:15:30,000
but he's certainly not against it.

295
00:15:30,000 --> 00:15:34,000
He's certainly for the practice of meditation.

296
00:15:34,000 --> 00:15:36,000
As a support,

297
00:15:36,000 --> 00:15:38,000
it's something that helps clear your mind

298
00:15:38,000 --> 00:15:41,000
and clear your feelings about things

299
00:15:41,000 --> 00:15:43,000
when you just make a determination.

300
00:15:43,000 --> 00:15:45,000
We all beings be happy,

301
00:15:45,000 --> 00:15:46,000
and certainly during our course,

302
00:15:46,000 --> 00:15:49,000
there are times where we'll have the meditation

303
00:15:49,000 --> 00:15:52,000
or make those vows.

304
00:15:52,000 --> 00:15:54,000
Jita Nupasana,

305
00:15:54,000 --> 00:15:56,000
some of the teachers said you should stay with the thought

306
00:15:56,000 --> 00:15:58,000
and understand its nature.

307
00:15:58,000 --> 00:16:01,000
So I'm suspicious about that sort of thing.

308
00:16:01,000 --> 00:16:02,000
The Buddha said,

309
00:16:02,000 --> 00:16:04,000
Nanyimita, Nanyimita,

310
00:16:04,000 --> 00:16:07,000
Nanyamita, Nanyamita, Nanyamya,

311
00:16:07,000 --> 00:16:08,000
and I always bring this quote up

312
00:16:08,000 --> 00:16:09,000
because it repeats it again

313
00:16:09,000 --> 00:16:10,000
and again and again.

314
00:16:10,000 --> 00:16:11,000
He says,

315
00:16:11,000 --> 00:16:13,000
don't focus on the particulars

316
00:16:13,000 --> 00:16:16,000
or the characteristics of things.

317
00:16:16,000 --> 00:16:18,000
Jinta, jinta,

318
00:16:18,000 --> 00:16:19,000
vinya, vinya,

319
00:16:19,000 --> 00:16:22,000
vatamatang, vaisati,

320
00:16:22,000 --> 00:16:23,000
let it just be thinking.

321
00:16:23,000 --> 00:16:26,000
I don't really agree with the idea

322
00:16:26,000 --> 00:16:32,000
that we should try to

323
00:16:32,000 --> 00:16:34,000
analyze things

324
00:16:34,000 --> 00:16:36,000
and figure out their true nature.

325
00:16:36,000 --> 00:16:38,000
We're not really interested in their true nature.

326
00:16:38,000 --> 00:16:39,000
They're impermanent,

327
00:16:39,000 --> 00:16:41,000
they're suffering in their non-self.

328
00:16:41,000 --> 00:16:43,000
That's really enough for us.

329
00:16:43,000 --> 00:16:44,000
It arises,

330
00:16:44,000 --> 00:16:45,000
it ceases ultimately.

331
00:16:45,000 --> 00:16:47,000
That's all you need to know.

332
00:16:47,000 --> 00:16:48,000
The problem is that we know more

333
00:16:48,000 --> 00:16:49,000
than that,

334
00:16:49,000 --> 00:16:50,000
that we get caught up in the details

335
00:16:50,000 --> 00:16:51,000
in the particulars

336
00:16:51,000 --> 00:16:52,000
and that's why we judge

337
00:16:52,000 --> 00:16:53,000
and so on.

338
00:16:53,000 --> 00:16:54,000
If we just saw everything

339
00:16:54,000 --> 00:16:55,000
as arising and ceasing

340
00:16:55,000 --> 00:16:56,000
there would be no greed,

341
00:16:56,000 --> 00:16:58,000
no anger and no delusion,

342
00:16:58,000 --> 00:17:00,000
because that's reality.

343
00:17:00,000 --> 00:17:02,000
Very hard to understand that one,

344
00:17:02,000 --> 00:17:08,000
but that is how the Buddha taught.

345
00:17:08,000 --> 00:17:10,000
Is releasing ourselves

346
00:17:10,000 --> 00:17:11,000
from suffering the best way

347
00:17:11,000 --> 00:17:13,000
to contribute to the world?

348
00:17:13,000 --> 00:17:15,000
Or should we hold ourselves

349
00:17:15,000 --> 00:17:16,000
from enlightenment

350
00:17:16,000 --> 00:17:17,000
if it means finding ways

351
00:17:17,000 --> 00:17:18,000
to help others?

352
00:17:18,000 --> 00:17:20,000
So should.

353
00:17:20,000 --> 00:17:22,000
I always want to talk about

354
00:17:22,000 --> 00:17:25,000
or reinforce this

355
00:17:25,000 --> 00:17:27,000
or reaffirm or reiterate this idea

356
00:17:27,000 --> 00:17:30,000
that there is no should in life.

357
00:17:30,000 --> 00:17:33,000
There's only

358
00:17:33,000 --> 00:17:36,000
sort of logical and illogical

359
00:17:36,000 --> 00:17:38,000
or coherent and incoherent.

360
00:17:38,000 --> 00:17:40,000
It's inconsistent,

361
00:17:40,000 --> 00:17:42,000
internally inconsistent

362
00:17:42,000 --> 00:17:44,000
or incoherent

363
00:17:44,000 --> 00:17:46,000
for someone to do evil things.

364
00:17:46,000 --> 00:17:47,000
And that's the only reason

365
00:17:47,000 --> 00:17:48,000
why you should say

366
00:17:48,000 --> 00:17:50,000
someone shouldn't do them.

367
00:17:50,000 --> 00:17:51,000
Because anytime anyone says

368
00:17:51,000 --> 00:17:53,000
this is going to benefit me,

369
00:17:53,000 --> 00:17:54,000
well, there are certain things

370
00:17:54,000 --> 00:17:55,000
that just don't benefit you.

371
00:17:55,000 --> 00:17:57,000
So it's inconsistent,

372
00:17:57,000 --> 00:17:58,000
it's wrong.

373
00:17:58,000 --> 00:18:01,000
It's like taking the 401,

374
00:18:01,000 --> 00:18:05,000
the 400 north of Toronto,

375
00:18:05,000 --> 00:18:06,000
none of you probably know

376
00:18:06,000 --> 00:18:07,000
what this is,

377
00:18:07,000 --> 00:18:08,000
but there's a highway north of Toronto.

378
00:18:08,000 --> 00:18:09,000
Take a north and say

379
00:18:09,000 --> 00:18:10,000
I'm going to Windsor.

380
00:18:10,000 --> 00:18:12,000
You'll never get to Windsor that way.

381
00:18:12,000 --> 00:18:14,000
I mean,

382
00:18:14,000 --> 00:18:16,000
you run it a highway first,

383
00:18:16,000 --> 00:18:18,000
because it's the wrong way.

384
00:18:18,000 --> 00:18:22,000
Windsor is south of here.

385
00:18:22,000 --> 00:18:26,000
So that kind of thing is wrong.

386
00:18:26,000 --> 00:18:28,000
So why is killing wrong?

387
00:18:28,000 --> 00:18:29,000
Killing is only wrong

388
00:18:29,000 --> 00:18:31,000
in the context of wanting to be happy.

389
00:18:31,000 --> 00:18:33,000
If you want to be happy

390
00:18:33,000 --> 00:18:34,000
or at peace,

391
00:18:34,000 --> 00:18:35,000
don't kill.

392
00:18:35,000 --> 00:18:36,000
I mean, killing,

393
00:18:36,000 --> 00:18:37,000
saying I'm going to kill

394
00:18:37,000 --> 00:18:38,000
and be really happy.

395
00:18:38,000 --> 00:18:41,000
It's just craziness.

396
00:18:41,000 --> 00:18:45,000
So it really depends on your goal.

397
00:18:45,000 --> 00:18:46,000
If you say,

398
00:18:46,000 --> 00:18:48,000
I want to become a Buddha.

399
00:18:48,000 --> 00:18:49,000
Then,

400
00:18:49,000 --> 00:18:50,000
but I'm going to practice

401
00:18:50,000 --> 00:18:51,000
we pass in a meditation

402
00:18:51,000 --> 00:18:52,000
and see nibana,

403
00:18:52,000 --> 00:18:54,000
then you're messed up

404
00:18:54,000 --> 00:18:56,000
because becoming a Buddha

405
00:18:56,000 --> 00:18:58,000
is about putting off nibana.

406
00:18:58,000 --> 00:19:00,000
That's really sort of

407
00:19:00,000 --> 00:19:02,000
where we get different paths from.

408
00:19:02,000 --> 00:19:04,000
So you'd have to put off

409
00:19:04,000 --> 00:19:06,000
the practice of insight meditation

410
00:19:06,000 --> 00:19:08,000
or any other reason.

411
00:19:08,000 --> 00:19:13,000
I don't think there's anything else

412
00:19:13,000 --> 00:19:16,000
that works as a goal,

413
00:19:16,000 --> 00:19:17,000
where you would say,

414
00:19:17,000 --> 00:19:19,000
because of this goal,

415
00:19:19,000 --> 00:19:20,000
I should put off

416
00:19:20,000 --> 00:19:21,000
enlightenment.

417
00:19:21,000 --> 00:19:23,000
I should put off

418
00:19:23,000 --> 00:19:26,000
becoming free from suffering.

419
00:19:26,000 --> 00:19:28,000
So when you say

420
00:19:28,000 --> 00:19:29,000
you want to,

421
00:19:29,000 --> 00:19:30,000
when you say

422
00:19:30,000 --> 00:19:32,000
best way to contribute to the world,

423
00:19:32,000 --> 00:19:33,000
from my point of view,

424
00:19:33,000 --> 00:19:34,000
contributing to the world

425
00:19:34,000 --> 00:19:35,000
is not a worthy goal.

426
00:19:35,000 --> 00:19:37,000
I mean, it's a goal

427
00:19:37,000 --> 00:19:38,000
that's just temporary.

428
00:19:38,000 --> 00:19:39,000
What do you mean

429
00:19:39,000 --> 00:19:40,000
by contributing to the world,

430
00:19:40,000 --> 00:19:41,000
making the world a better place?

431
00:19:41,000 --> 00:19:42,000
Okay,

432
00:19:42,000 --> 00:19:45,000
so it becomes a better place than what?

433
00:19:45,000 --> 00:19:46,000
That ends,

434
00:19:46,000 --> 00:19:51,000
and then it becomes a crappy place again.

435
00:19:51,000 --> 00:19:53,000
The world will never really be

436
00:19:53,000 --> 00:19:55,000
a happy, peaceful place

437
00:19:55,000 --> 00:19:57,000
because of the nature of it

438
00:19:57,000 --> 00:19:58,000
of being chaotic

439
00:19:58,000 --> 00:19:59,000
and uncertain

440
00:19:59,000 --> 00:20:04,000
and unpredictable and complicated.

441
00:20:04,000 --> 00:20:07,000
So anyone who holds up society,

442
00:20:07,000 --> 00:20:09,000
the Buddha was very critical of this,

443
00:20:09,000 --> 00:20:10,000
you know,

444
00:20:10,000 --> 00:20:11,000
worrying about society,

445
00:20:11,000 --> 00:20:13,000
concerned for the world,

446
00:20:13,000 --> 00:20:15,000
concerned for things

447
00:20:15,000 --> 00:20:17,000
that are just going to,

448
00:20:17,000 --> 00:20:18,000
you know,

449
00:20:18,000 --> 00:20:19,000
I always say

450
00:20:19,000 --> 00:20:20,000
that the earth is going to

451
00:20:20,000 --> 00:20:22,000
burn to a crisp,

452
00:20:22,000 --> 00:20:23,000
eventually.

453
00:20:23,000 --> 00:20:24,000
There's going to,

454
00:20:24,000 --> 00:20:25,000
there will eventually

455
00:20:25,000 --> 00:20:26,000
mean no earth left

456
00:20:26,000 --> 00:20:28,000
for us to live on.

457
00:20:28,000 --> 00:20:30,000
Will all be dead?

458
00:20:30,000 --> 00:20:32,000
And we, I mean,

459
00:20:32,000 --> 00:20:33,000
humans in general,

460
00:20:33,000 --> 00:20:34,000
no more humans,

461
00:20:34,000 --> 00:20:36,000
that's going to happen eventually.

462
00:20:36,000 --> 00:20:39,000
So contributing the world

463
00:20:39,000 --> 00:20:42,000
seems a bit short-sighted.

464
00:20:48,000 --> 00:20:51,000
I suffer from shyness.

465
00:20:51,000 --> 00:20:53,000
When I get completely angry,

466
00:20:53,000 --> 00:20:58,000
angry at being shy,

467
00:20:58,000 --> 00:21:00,000
the shyness disappears.

468
00:21:00,000 --> 00:21:01,000
When you can set it

469
00:21:01,000 --> 00:21:02,000
as a skillful method

470
00:21:02,000 --> 00:21:04,000
toward a shyness,

471
00:21:04,000 --> 00:21:06,000
there's a certain assertiveness

472
00:21:06,000 --> 00:21:08,000
in anger, right?

473
00:21:08,000 --> 00:21:10,000
No, I wouldn't consider that

474
00:21:10,000 --> 00:21:11,000
wholesome.

475
00:21:11,000 --> 00:21:12,000
The thing is,

476
00:21:12,000 --> 00:21:14,000
your anger is triggering

477
00:21:14,000 --> 00:21:16,000
other states,

478
00:21:16,000 --> 00:21:17,000
which are wholesome,

479
00:21:17,000 --> 00:21:18,000
I suppose.

480
00:21:18,000 --> 00:21:19,000
It's triggering

481
00:21:19,000 --> 00:21:21,000
conviction, confidence,

482
00:21:21,000 --> 00:21:22,000
energy, effort.

483
00:21:22,000 --> 00:21:24,000
A lot of physical

484
00:21:24,000 --> 00:21:26,000
states that then trigger

485
00:21:26,000 --> 00:21:27,000
some wholesomeness,

486
00:21:27,000 --> 00:21:28,000
which is like focus

487
00:21:28,000 --> 00:21:30,000
and determination

488
00:21:30,000 --> 00:21:33,000
and certainty and confidence.

489
00:21:33,000 --> 00:21:34,000
But the anger part

490
00:21:34,000 --> 00:21:36,000
of it is,

491
00:21:36,000 --> 00:21:38,000
is ultimately destructive.

492
00:21:38,000 --> 00:21:39,000
It's bad habit

493
00:21:39,000 --> 00:21:40,000
to cultivate.

494
00:21:40,000 --> 00:21:45,000
You're cultivating habits with this.

495
00:21:45,000 --> 00:21:47,000
I was like saying,

496
00:21:47,000 --> 00:21:48,000
when I'm really tired,

497
00:21:48,000 --> 00:21:49,000
I should think

498
00:21:49,000 --> 00:21:50,000
of something that I crave

499
00:21:50,000 --> 00:21:51,000
because it wakes me up

500
00:21:51,000 --> 00:21:52,000
and goes on,

501
00:21:52,000 --> 00:21:53,000
you know,

502
00:21:53,000 --> 00:21:54,000
sure.

503
00:21:54,000 --> 00:21:56,000
But then you want the craving

504
00:21:56,000 --> 00:21:57,000
to deal with

505
00:21:57,000 --> 00:21:59,000
and you're cultivating an addiction.

506
00:21:59,000 --> 00:22:01,000
So,

507
00:22:01,000 --> 00:22:04,000
there are many

508
00:22:04,000 --> 00:22:06,000
different qualities of mine,

509
00:22:06,000 --> 00:22:07,000
some wholesome,

510
00:22:07,000 --> 00:22:08,000
some unhold,

511
00:22:08,000 --> 00:22:09,000
some useful,

512
00:22:09,000 --> 00:22:11,000
some harmful.

513
00:22:11,000 --> 00:22:13,000
And ideally you want to,

514
00:22:13,000 --> 00:22:14,000
you know,

515
00:22:14,000 --> 00:22:15,000
like,

516
00:22:15,000 --> 00:22:17,000
there are other ways

517
00:22:17,000 --> 00:22:19,000
to give up shyness.

518
00:22:19,000 --> 00:22:21,000
There are other qualities of mine

519
00:22:21,000 --> 00:22:23,000
that you could cultivate.

520
00:22:23,000 --> 00:22:24,000
Like think about the Buddha,

521
00:22:24,000 --> 00:22:25,000
for example,

522
00:22:25,000 --> 00:22:26,000
that'll give you conviction

523
00:22:26,000 --> 00:22:27,000
if you have faith

524
00:22:27,000 --> 00:22:28,000
in him and so on.

525
00:22:28,000 --> 00:22:31,000
That's why people have already

526
00:22:31,000 --> 00:22:32,000
just,

527
00:22:32,000 --> 00:22:33,000
you know,

528
00:22:33,000 --> 00:22:35,000
a really just faith,

529
00:22:35,000 --> 00:22:37,000
convictions.

530
00:22:37,000 --> 00:22:40,000
I'm talking about the benefits of faith.

531
00:22:40,000 --> 00:22:41,000
You know,

532
00:22:41,000 --> 00:22:43,000
it's just mundane sense.

533
00:22:43,000 --> 00:22:45,000
It's quite a beneficial quality.

534
00:22:45,000 --> 00:22:47,000
It gives you conviction.

535
00:22:47,000 --> 00:22:48,000
It gives you energy.

536
00:22:48,000 --> 00:22:51,000
It gives you strength and so on.

537
00:22:51,000 --> 00:22:53,000
So even if it's

538
00:22:53,000 --> 00:22:55,000
belief in something

539
00:22:55,000 --> 00:22:57,000
silly,

540
00:22:57,000 --> 00:22:59,000
like God or something,

541
00:22:59,000 --> 00:23:02,000
it can still be powerful psychologically.

542
00:23:02,000 --> 00:23:03,000
Of course,

543
00:23:03,000 --> 00:23:04,000
when it's in the Buddha,

544
00:23:04,000 --> 00:23:05,000
it's much more powerful

545
00:23:05,000 --> 00:23:08,000
because he really is awesome.

546
00:23:08,000 --> 00:23:10,000
What's awesome?

547
00:23:10,000 --> 00:23:14,000
Still is.

548
00:23:14,000 --> 00:23:16,000
Okay.

549
00:23:16,000 --> 00:23:17,000
So that,

550
00:23:17,000 --> 00:23:18,000
I hope more questions.

551
00:23:18,000 --> 00:23:19,000
They're popping up.

552
00:23:19,000 --> 00:23:20,000
Yeah,

553
00:23:20,000 --> 00:23:22,000
this isn't fair.

554
00:23:22,000 --> 00:23:26,000
Hmm.

555
00:23:26,000 --> 00:23:27,000
What do I do now?

556
00:23:27,000 --> 00:23:28,000
Do I keep answering?

557
00:23:28,000 --> 00:23:29,000
Do I keep asking?

558
00:23:29,000 --> 00:23:30,000
Sure.

559
00:23:30,000 --> 00:23:31,000
Why not?

560
00:23:31,000 --> 00:23:32,000
It means you're actually here

561
00:23:32,000 --> 00:23:33,000
listening, right?

562
00:23:33,000 --> 00:23:36,000
So that's useful to answer questions

563
00:23:36,000 --> 00:23:39,000
of people who are actually here.

564
00:23:39,000 --> 00:23:41,000
But I will cut it off eventually.

565
00:23:41,000 --> 00:23:43,000
I'm not going to stay here all night

566
00:23:43,000 --> 00:23:47,000
and answer questions.

567
00:23:47,000 --> 00:23:48,000
And also,

568
00:23:48,000 --> 00:23:49,000
we haven't had time to vet these.

569
00:23:49,000 --> 00:23:50,000
That's the thing.

570
00:23:50,000 --> 00:23:52,000
It might end up just

571
00:23:52,000 --> 00:23:55,000
deleting questions.

572
00:23:55,000 --> 00:23:57,000
Dalai Lama,

573
00:23:57,000 --> 00:23:58,000
I don't,

574
00:23:58,000 --> 00:24:06,000
I don't really want to talk about his views.

575
00:24:06,000 --> 00:24:09,000
How do we overcome the feeling of being imprisoned

576
00:24:09,000 --> 00:24:12,000
by the body?

577
00:24:12,000 --> 00:24:14,000
Hmm.

578
00:24:14,000 --> 00:24:16,000
I'll just say to yourself

579
00:24:16,000 --> 00:24:17,000
feeling,

580
00:24:17,000 --> 00:24:18,000
again,

581
00:24:18,000 --> 00:24:20,000
we're not trying to overcome feelings.

582
00:24:20,000 --> 00:24:22,000
If it really is just a feeling,

583
00:24:22,000 --> 00:24:24,000
then just a feeling,

584
00:24:24,000 --> 00:24:25,000
if you dislike the feelings

585
00:24:25,000 --> 00:24:26,000
that you dislike,

586
00:24:26,000 --> 00:24:27,000
this liking,

587
00:24:27,000 --> 00:24:32,000
this liking.

588
00:24:32,000 --> 00:24:34,000
Is it true that our needs

589
00:24:34,000 --> 00:24:36,000
and wants in life

590
00:24:36,000 --> 00:24:38,000
is part of conventional reality

591
00:24:38,000 --> 00:24:42,000
and the only thing out there is desire.

592
00:24:42,000 --> 00:24:44,000
Needs and wants,

593
00:24:44,000 --> 00:24:49,000
what needs and wants are two different things.

594
00:24:49,000 --> 00:24:54,000
A need implies a goal

595
00:24:54,000 --> 00:24:58,000
or a desired state

596
00:24:58,000 --> 00:25:00,000
or a related state.

597
00:25:00,000 --> 00:25:04,000
You need a hammer to pound in the nail.

598
00:25:04,000 --> 00:25:08,000
You need food to stay alive.

599
00:25:08,000 --> 00:25:11,000
You need air to stay alive.

600
00:25:11,000 --> 00:25:14,000
You need oxygen to light a fire.

601
00:25:14,000 --> 00:25:15,000
That sort of thing.

602
00:25:15,000 --> 00:25:16,000
These are what need.

603
00:25:16,000 --> 00:25:18,000
It involves something else.

604
00:25:18,000 --> 00:25:20,000
So you need x4y.

605
00:25:20,000 --> 00:25:22,000
You can't just need x.

606
00:25:22,000 --> 00:25:24,000
I think that's pretty clear.

607
00:25:24,000 --> 00:25:27,000
So we don't need food.

608
00:25:27,000 --> 00:25:28,000
You don't have it.

609
00:25:28,000 --> 00:25:28,000
You'll die.

610
00:25:28,000 --> 00:25:29,000
It cause an effect.

611
00:25:29,000 --> 00:25:31,000
Need is just a convention

612
00:25:31,000 --> 00:25:33,000
relating to something else.

613
00:25:33,000 --> 00:25:36,000
You need x4y.

614
00:25:36,000 --> 00:25:38,000
Our wants,

615
00:25:38,000 --> 00:25:40,000
wants are real and desires real.

616
00:25:40,000 --> 00:25:43,000
But I think you're meaning something like,

617
00:25:43,000 --> 00:25:45,000
is there any validity to our wants?

618
00:25:45,000 --> 00:25:49,000
Like, I like cheesecake

619
00:25:49,000 --> 00:25:51,000
and that's a part of who I am.

620
00:25:51,000 --> 00:25:53,000
I'm kind of saying which isn't true.

621
00:25:53,000 --> 00:25:54,000
It's just desire.

622
00:25:54,000 --> 00:25:57,000
It doesn't have any meaning beyond that.

623
00:26:01,000 --> 00:26:02,000
Okay.

624
00:26:02,000 --> 00:26:04,000
That's all the questions.

625
00:26:04,000 --> 00:26:09,000
Thank you all for tuning in.

626
00:26:09,000 --> 00:26:16,000
Have a good night.

