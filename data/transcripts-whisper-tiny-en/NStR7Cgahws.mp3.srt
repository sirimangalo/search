1
00:00:00,000 --> 00:00:04,320
Someone, I feel like I will never find happiness in lay life and I'm very depressed.

2
00:00:04,880 --> 00:00:06,960
Is this a sign for me to become a monk?

3
00:00:09,360 --> 00:00:16,320
You don't need a sign to become a monk. Just become a monk. There's no, if you're not

4
00:00:16,320 --> 00:00:21,440
tied down to anything, just become a monk. There's nothing else. There's nothing better that you could do.

5
00:00:21,440 --> 00:00:32,720
Of course, you'll never find happiness in lay life. There's no happiness to be found.

6
00:00:40,640 --> 00:00:42,480
Is that going to turn people off? I don't know.

7
00:00:42,480 --> 00:00:49,120
But I have to, you know, you think you just have to get to that.

8
00:00:51,520 --> 00:00:58,880
If you're not afraid of being a ordaining and you don't have burdens that are keeping you from

9
00:00:58,880 --> 00:01:07,440
ordaining, then I don't see a reason not to. Worse, you'll just die on the street or something.

10
00:01:07,440 --> 00:01:17,120
Very good. Arment to give up everything. It would certainly fix your depression.

11
00:01:18,000 --> 00:01:22,960
I don't know about that. It depends because sometimes you go to the monastery and realize how

12
00:01:22,960 --> 00:01:30,000
depressing it is, how depressing Buddhism has become. Monasteries can be modern. Monasteries can be a

13
00:01:30,000 --> 00:01:35,680
little bit depressing as well. But the monk's life is not depressing. No, I think that's

14
00:01:35,680 --> 00:01:40,320
not true because you have the monk's life. Monk's life is not depressing. It's wonderfully

15
00:01:40,320 --> 00:01:49,040
liberating. You're so in control of your own destiny. Nothing can freeze you. Maybe you don't eat

16
00:01:49,040 --> 00:02:02,880
for a day. Inconsequential. Maybe you don't have soap or toothbrush. Maybe you're lying sick

17
00:02:02,880 --> 00:02:12,320
in the cave somewhere. You're such freedom. You don't have to worry about anything.

18
00:02:14,720 --> 00:02:16,880
So I think a very good cure for depression.

19
00:02:21,120 --> 00:02:22,560
Certainly a cure for the lay life.

20
00:02:22,560 --> 00:02:31,920
It's just a joke, you see, because when you become a monk, you are no longer a lay person.

21
00:02:31,920 --> 00:03:01,760
So it's a cure for the lay life.

