1
00:00:00,000 --> 00:00:03,040
Hello, welcome back to Ask a Monk.

2
00:00:03,040 --> 00:00:07,760
Today's question comes from Pedro C, who asks,

3
00:00:07,760 --> 00:00:10,880
people tend to think one can learn from errors.

4
00:00:10,880 --> 00:00:13,760
However, only seldom this is true.

5
00:00:13,760 --> 00:00:15,560
What makes us learn from mistakes,

6
00:00:15,560 --> 00:00:19,240
and when do we ignore these lessons?

7
00:00:19,240 --> 00:00:20,040
Good question.

8
00:00:23,360 --> 00:00:28,560
The Buddha was pretty clear about this sort of thing

9
00:00:28,560 --> 00:00:34,040
that it's not true that a person who suffers

10
00:00:34,040 --> 00:00:37,240
is necessarily going to be any wiser than a person

11
00:00:37,240 --> 00:00:38,840
who does not suffer so much.

12
00:00:38,840 --> 00:00:41,760
A person who makes a lot of mistakes

13
00:00:41,760 --> 00:00:45,160
is not necessarily going to learn more.

14
00:00:45,160 --> 00:00:47,840
However, we're all quite aware that it's

15
00:00:47,840 --> 00:00:51,400
a chance to learn something, because obviously,

16
00:00:51,400 --> 00:00:53,920
the learning from one's mistakes is how you avoid

17
00:00:53,920 --> 00:00:57,040
making them in the future.

18
00:00:57,040 --> 00:00:59,720
The difference is why it's attention, something

19
00:00:59,720 --> 00:01:02,400
called the Oni so Manasikara.

20
00:01:02,400 --> 00:01:09,120
Manasikara means to keep in mind or to pay attention

21
00:01:09,120 --> 00:01:12,400
to Manas means in the mind.

22
00:01:12,400 --> 00:01:15,880
Cara means to make Manasi in the mind.

23
00:01:15,880 --> 00:01:20,760
Cara to make, to establish in the mind wisely,

24
00:01:20,760 --> 00:01:24,920
or to set your mind on something with wisdom.

25
00:01:24,920 --> 00:01:31,920
Oni so literally translates, going back to the origin.

26
00:01:31,920 --> 00:01:35,160
So it means when something occurs,

27
00:01:35,160 --> 00:01:38,800
when something happens, the ability to examine it

28
00:01:38,800 --> 00:01:42,240
and reflect upon it, and to see it clearly,

29
00:01:42,240 --> 00:01:48,920
which obviously is the practice of meditation

30
00:01:48,920 --> 00:01:53,240
when you are meditating, the word meditation,

31
00:01:53,240 --> 00:01:55,280
that's really what it means.

32
00:01:55,280 --> 00:01:58,800
It means the measurement, the examination,

33
00:01:58,800 --> 00:02:02,240
the analysis, and the understanding

34
00:02:02,240 --> 00:02:05,120
of a situation, of a reality.

35
00:02:05,120 --> 00:02:08,440
So when we make a mistake, when we say something wrong,

36
00:02:08,440 --> 00:02:12,560
when we do something wrong, we can see what are the causes.

37
00:02:12,560 --> 00:02:14,720
So we can see what are the effects.

38
00:02:14,720 --> 00:02:22,000
We can see what it was that led us to fall into the mind

39
00:02:22,000 --> 00:02:26,160
states that caused us to do something wrong.

40
00:02:26,160 --> 00:02:30,200
The interesting thing is we can often see

41
00:02:30,200 --> 00:02:32,840
how many of the things that we thought were mistakes

42
00:02:32,840 --> 00:02:34,560
in the sense that we did something wrong

43
00:02:34,560 --> 00:02:39,400
are actually not really mistakes per se.

44
00:02:39,400 --> 00:02:43,720
For example, you might forget an appointment,

45
00:02:43,720 --> 00:02:48,480
or you might make a mistake in calculating,

46
00:02:48,480 --> 00:02:54,880
or in you might give a speech and say the wrong thing.

47
00:02:54,880 --> 00:03:02,200
And it may be simply a physical failure,

48
00:03:02,200 --> 00:03:05,760
where the brain, the memory fails you,

49
00:03:05,760 --> 00:03:14,480
or the organic nature of our being is in that way.

50
00:03:14,480 --> 00:03:17,400
And so I think it's important to understand

51
00:03:17,400 --> 00:03:23,400
that oftentimes we, instead of learning from our mistakes,

52
00:03:23,400 --> 00:03:27,440
actually feel guilty about those situations

53
00:03:27,440 --> 00:03:30,120
that are not necessarily our fault.

54
00:03:30,120 --> 00:03:33,280
And I think this is an important part

55
00:03:33,280 --> 00:03:35,000
of learning from our mistakes as well,

56
00:03:35,000 --> 00:03:38,280
that the most important point isn't that you

57
00:03:38,280 --> 00:03:40,360
made a mistake.

58
00:03:40,360 --> 00:03:43,480
The important point is the clear awareness

59
00:03:43,480 --> 00:03:45,840
and understanding of the situation.

60
00:03:45,840 --> 00:03:50,160
So if anger arises and you agree to arises

61
00:03:50,160 --> 00:03:52,440
and you've delusion and ego and so on,

62
00:03:52,440 --> 00:03:56,120
arises and it's the ability to catch this and to see it

63
00:03:56,120 --> 00:04:00,080
and to adjust the mind and to create wisdom

64
00:04:00,080 --> 00:04:03,240
and to remove those states, if something happens

65
00:04:03,240 --> 00:04:05,560
that causes people to get angry at you

66
00:04:05,560 --> 00:04:12,560
or give rise to ego and self-righteousness and so on,

67
00:04:12,560 --> 00:04:14,720
self-righteousness and so on, in other people

68
00:04:14,720 --> 00:04:17,600
where they say this person, did this terrible thing

69
00:04:17,600 --> 00:04:21,600
or this person is lazy or this person is incompetent

70
00:04:21,600 --> 00:04:26,600
or so on, then oftentimes it's not our fault.

71
00:04:27,240 --> 00:04:31,000
Oftentimes this is the nature of reality.

72
00:04:31,000 --> 00:04:33,600
So it's important on the one hand

73
00:04:33,600 --> 00:04:38,400
to be able to discriminate between what is truly a mistake

74
00:04:38,400 --> 00:04:41,000
and what is simply a part of life.

75
00:04:41,000 --> 00:04:43,480
You can't please everyone all the time

76
00:04:43,480 --> 00:04:46,400
and it can be that with the best intentions,

77
00:04:46,400 --> 00:04:51,400
you said something that insulted or angered other people,

78
00:04:52,360 --> 00:04:54,360
it could be even that you said something right

79
00:04:54,360 --> 00:04:55,640
and people didn't like it.

80
00:04:55,640 --> 00:04:57,160
It could be that you said something wrong

81
00:04:57,160 --> 00:05:00,960
but you didn't mean it or you weren't clear enough

82
00:05:00,960 --> 00:05:03,280
or you used the wrong words or so on.

83
00:05:04,920 --> 00:05:09,600
And so important not to be too hard on ourselves,

84
00:05:09,600 --> 00:05:12,480
but on the other hand, when we do make mistakes,

85
00:05:12,480 --> 00:05:16,200
when we do give rise to greed, anger, delusion,

86
00:05:16,200 --> 00:05:19,320
and so on, of jealousy, sin, genus or whatever,

87
00:05:19,320 --> 00:05:21,680
something that leads us to do something

88
00:05:21,680 --> 00:05:26,680
that intentionally causes suffering for ourselves or others.

89
00:05:26,680 --> 00:05:28,920
This is the point, this is what is really a mistake

90
00:05:28,920 --> 00:05:33,920
and learning from it, it doesn't simply mean

91
00:05:34,800 --> 00:05:39,160
not doing it again, it means coming to understand

92
00:05:39,160 --> 00:05:42,760
the situation because too often we think

93
00:05:42,760 --> 00:05:45,000
learning from a mistake is simply hating,

94
00:05:45,000 --> 00:05:46,360
getting angry at ourselves and saying,

95
00:05:46,360 --> 00:05:48,960
don't do that ever again, don't do that ever again.

96
00:05:48,960 --> 00:05:52,440
And it may lead us to be able to avoid

97
00:05:52,440 --> 00:05:57,440
the certain situation simply by brute force of will,

98
00:05:59,760 --> 00:06:03,320
but it won't change the reasons why we did that,

99
00:06:03,320 --> 00:06:05,240
which are the greed, the anger, the delusion,

100
00:06:05,240 --> 00:06:07,400
the unwholesomeness and the mind.

101
00:06:07,400 --> 00:06:10,680
Once we come to see that this is causing suffering,

102
00:06:10,680 --> 00:06:13,360
we won't do it anymore, as I've said before,

103
00:06:13,360 --> 00:06:17,360
there's no one who intentionally causes suffering,

104
00:06:17,360 --> 00:06:21,400
we do it out of the idea that it's somehow going

105
00:06:21,400 --> 00:06:25,160
to bring happiness, that it's going to bring benefit to us,

106
00:06:25,160 --> 00:06:29,040
that it's going to somehow bring happiness to us.

107
00:06:29,040 --> 00:06:31,960
And once we realize that it's actually causing us suffering,

108
00:06:31,960 --> 00:06:36,360
we won't engage in it, we'll realize that there's no benefit.

109
00:06:36,360 --> 00:06:38,600
Once we realize there's no benefit,

110
00:06:38,600 --> 00:06:40,280
this is learning from your mistakes,

111
00:06:40,280 --> 00:06:42,400
so that's from a Buddhist point of view,

112
00:06:42,400 --> 00:07:07,120
so I hope that helps, thanks for the question.

