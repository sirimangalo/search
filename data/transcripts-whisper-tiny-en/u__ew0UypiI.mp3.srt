1
00:00:00,000 --> 00:00:06,200
Hello everyone, just a bulletin here to let you know that if you haven't heard already

2
00:00:06,200 --> 00:00:08,600
I've turned my video

3
00:00:10,320 --> 00:00:14,600
Teachings on how to meditate into a book and here it is

4
00:00:15,240 --> 00:00:17,240
this book is

5
00:00:19,040 --> 00:00:24,320
Based on the transcriptions of the videos on how to meditate on YouTube

6
00:00:24,800 --> 00:00:26,800
but with a lot of

7
00:00:26,800 --> 00:00:29,640
Added it added clarified

8
00:00:32,080 --> 00:00:34,080
Even changed

9
00:00:34,080 --> 00:00:36,080
Some of the information to make it a little bit

10
00:00:37,160 --> 00:00:39,640
More more clear and more correct

11
00:00:40,040 --> 00:00:44,320
There's obviously only so much you can do with a video you can only record it once or twice

12
00:00:45,520 --> 00:00:49,280
Especially when you're dealing with a an entire camera crew

13
00:00:49,760 --> 00:00:52,440
so the book that was a lot easier and

14
00:00:53,120 --> 00:00:55,120
hopefully I can update it

15
00:00:55,120 --> 00:01:00,240
As I see fit and add more information, maybe adding there's even an appendix in here

16
00:01:02,000 --> 00:01:07,880
You see it's got pictures of how to do the prostrations

17
00:01:09,880 --> 00:01:15,680
How to do the walking meditation and sitting meditation, you know basic information how to stand and how to sit

18
00:01:16,400 --> 00:01:19,400
the point really is that this booklet can be

19
00:01:19,400 --> 00:01:27,960
It can be read without the need for a computer. You can print it up on my website. There is a link to

20
00:01:29,240 --> 00:01:32,760
PDF versions that you can print up by their in letter paper or a four

21
00:01:33,400 --> 00:01:36,040
and so it doesn't require the

22
00:01:38,040 --> 00:01:40,040
any technology to

23
00:01:40,040 --> 00:01:46,320
To use it and the other thing is of course as I said that it allows for added information

24
00:01:46,320 --> 00:01:49,720
So I think this is I'm kind of excited about it

25
00:01:49,720 --> 00:01:53,280
I think it's something that's gonna be really useful. I'm using it to teach

26
00:01:54,320 --> 00:01:56,320
inmates in the Federal Detention Center

27
00:01:57,040 --> 00:02:03,040
because they don't have access to a DVD player and I think it's going to be of similar use to

28
00:02:03,440 --> 00:02:05,440
other people around the world so

29
00:02:05,440 --> 00:02:08,880
I'm gonna probably print it up and maybe even publish it in a

30
00:02:09,760 --> 00:02:12,560
proper booklet form. It's not that big. It's about 40

31
00:02:12,560 --> 00:02:15,680
45 pages long and

32
00:02:16,800 --> 00:02:21,040
You can print it up and put staples in it if you got a good stapler and

33
00:02:21,840 --> 00:02:24,720
Make a video. Okay, I'll make a booklet. So

34
00:02:25,600 --> 00:02:27,600
there you have it

35
00:02:27,840 --> 00:02:31,040
That's all just that I'd let people know to

36
00:02:31,840 --> 00:02:34,640
You know to get it out to more people. I'm not looking to

37
00:02:35,280 --> 00:02:38,960
Sell myself. I'm not trying to be a star or or anything

38
00:02:38,960 --> 00:02:42,320
I think this is something that's honestly useful for the world and I think it's

39
00:02:43,360 --> 00:02:49,560
It's it's a good idea for anyone any one of us who's interested in in living in peace and happiness

40
00:02:49,560 --> 00:02:51,560
I truly believe this that

41
00:02:52,320 --> 00:02:56,400
The best way to live in peace and happiness is to not only help yourself

42
00:02:56,400 --> 00:03:05,520
But to help those around you and to help the whole world and we can really turn this world into a better place if we if we all work at it like this

43
00:03:05,520 --> 00:03:09,360
So and this is what I can do as a Buddhist monk

44
00:03:09,760 --> 00:03:14,400
Thanks to everyone for your support your encouragement and your appreciation of the work

45
00:03:15,200 --> 00:03:20,880
If you have questions, you're welcome to ask them. Please don't forget to subscribe for updates and

46
00:03:20,880 --> 00:03:36,800
You'll keep in touch. Okay, that's all. See you soon.

