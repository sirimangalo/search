1
00:00:00,000 --> 00:00:09,880
This is, we know that in little children's development process, fairy tales is very important,

2
00:00:09,880 --> 00:00:15,440
but at the same time it creates an illusion for them, but your advice and what would be

3
00:00:15,440 --> 00:00:18,640
good to teach them.

4
00:00:18,640 --> 00:00:27,880
Yeah I don't think the truth of a story is necessary for it to be useful beneficial.

5
00:00:27,880 --> 00:00:32,560
A story itself has to be, you have to ask the question, what good is a story itself?

6
00:00:32,560 --> 00:00:37,600
Like if I tell you a true story or I tell you a story that never happened, I don't think

7
00:00:37,600 --> 00:00:43,800
that is a salient feature or an important feature of the story or the usefulness.

8
00:00:43,800 --> 00:00:50,040
The question is to what benefit are stories and you say that we know that fairy tales

9
00:00:50,040 --> 00:00:58,400
are very important and I'm going to seed to your knowledge on that.

10
00:00:58,400 --> 00:01:04,000
It makes sense to me, it's not something I don't have children personally so I've never

11
00:01:04,000 --> 00:01:12,320
really thought about how stories benefit, but you have to look more at the content of

12
00:01:12,320 --> 00:01:13,320
the story.

13
00:01:13,320 --> 00:01:23,280
If you tell a true story that corrupts them and tell them all about these horrible people

14
00:01:23,280 --> 00:01:35,520
and may even glorify it a little bit and talk about how some stories you read are just

15
00:01:35,520 --> 00:01:44,800
of no use of no benefit to people, whether they be true or not, I'm thinking of stories

16
00:01:44,800 --> 00:01:55,800
that aren't true of novels and so on that talk about the truth actually and it's an

17
00:01:55,800 --> 00:02:05,480
interesting question, so stories that are made up but are based on the way things actually

18
00:02:05,480 --> 00:02:11,560
go, so they talk about corruption and they show people being corrupt and they show this

19
00:02:11,560 --> 00:02:17,680
as being the state of the world and so there's truth in that, it's really talking about

20
00:02:17,680 --> 00:02:23,080
the true state, people can go into slums and research and write a story about how horrible

21
00:02:23,080 --> 00:02:30,800
life there can be, but the question of in what way and in what context and to what audience

22
00:02:30,800 --> 00:02:43,240
that's useful is something I think quite interesting and the most important question, so

23
00:02:43,240 --> 00:02:49,320
I think it's generally understood that moral stories with moral are very important, stories

24
00:02:49,320 --> 00:02:58,240
that teach you something, but what I wanted to say about the point I was making about the

25
00:02:58,240 --> 00:03:05,040
stories about the horrors of life is I think stories that motivate are important, stories

26
00:03:05,040 --> 00:03:10,800
that encourage are important, so if your story is just teaching us, they talk about this

27
00:03:10,800 --> 00:03:16,760
catharsis, so a lot of Shakespearean stuff, these stories that end just horribly and I

28
00:03:16,760 --> 00:03:22,880
can never understand it and I think I'm leaning towards a justification that are a belief

29
00:03:22,880 --> 00:03:27,400
that these sort of stories are not justified even for bringing about a catharsis which

30
00:03:27,400 --> 00:03:33,360
of course is not a Buddhist concept, a purging of emotions, so it's a relief to see

31
00:03:33,360 --> 00:03:40,920
such horrible things because to me it leads to despair and depression and the feeling

32
00:03:40,920 --> 00:03:52,160
that there is no way out which a lot of stories do, so stories that are I think encouraging

33
00:03:52,160 --> 00:03:57,280
are important, now they have to of course encourage in the right way, the other side

34
00:03:57,280 --> 00:04:05,720
is these fairy tale happily ever after stories which are probably equally harmful I would

35
00:04:05,720 --> 00:04:12,000
say to children, the idea, and this has been brought up I think in sociology circles

36
00:04:12,000 --> 00:04:20,200
or psychology circles where they pointed this as a real problem where for example the

37
00:04:20,200 --> 00:04:29,440
princess waits for her prince and so this leads women to feel like they are just objects

38
00:04:29,440 --> 00:04:35,320
waiting to be plucked off of a tree or so on, but in general the idea that at the end of

39
00:04:35,320 --> 00:04:42,320
the story Prince Charming or whatever, the guy gets the girl, the girl gets the guy

40
00:04:42,320 --> 00:04:49,600
and they live happily ever after, this is also problematic because it and stories like

41
00:04:49,600 --> 00:04:57,000
it that lead us closer to the wheel, lead us or remind us or encourage us or teach us

42
00:04:57,000 --> 00:05:07,480
the rightness of the wheel of samsara in terms of following the system, succeeding

43
00:05:07,480 --> 00:05:15,680
in life, meeting with all the trials and tribulations of life and conquering them by

44
00:05:15,680 --> 00:05:26,120
becoming a successful whatever or defeating the enemies and so on, leads people to, leads

45
00:05:26,120 --> 00:05:34,520
people in the wrong direction, so the question of which sort of stories are useful

46
00:05:34,520 --> 00:05:40,680
is a really interesting one, which stories as a Buddhist would you recommend, so first

47
00:05:40,680 --> 00:05:45,360
of all your question is about illusion, I don't think the illusion is the problem, the

48
00:05:45,360 --> 00:05:54,720
moral is the problem, what the story is teaching is the problem or is the question and

49
00:05:54,720 --> 00:05:58,880
so I think the answer from Buddhist point of view is quite simple is to teach of course

50
00:05:58,880 --> 00:06:03,000
Buddhist principles and this is what Christians do, this is what all religions do, they

51
00:06:03,000 --> 00:06:12,520
teach, they have fairy tales that teach certain morals in their religion and so for example

52
00:06:12,520 --> 00:06:19,280
the Jatakas are actually a really good tool for this, I printed up some of some retellings

53
00:06:19,280 --> 00:06:26,080
of the Jatakas that are on the internet, the past life stories of the Buddha and shared

54
00:06:26,080 --> 00:06:30,800
them and shared them or gave them to these novices and had them, we sat around and read

55
00:06:30,800 --> 00:06:40,760
them, these Vietnamese, Cambodian, mostly Vietnamese and Cambodian kids who ordained as

56
00:06:40,760 --> 00:06:45,960
novices for the summer, these were pretty bad kids, some of them were gangsters, most

57
00:06:45,960 --> 00:06:53,760
of them were hooked on, like most kids hooked on PS2 or whatever it was at the time,

58
00:06:53,760 --> 00:07:02,000
I don't know, they had this playstation, I think they had a playstation, so I ended up,

59
00:07:02,000 --> 00:07:07,440
they were all staying in this one room and just lying around playing playstation and

60
00:07:07,440 --> 00:07:12,600
so this is what they were doing as novices, which of course is not allowed but that's

61
00:07:12,600 --> 00:07:16,880
how it goes, so in the beginning I wasn't my monastery, I was just a visitor and so for the

62
00:07:16,880 --> 00:07:21,320
first few days I just let it be and said it's not my business but after a while it got

63
00:07:21,320 --> 00:07:26,600
horrible, they were lying around swearing and cursing and really making a nuisance of

64
00:07:26,600 --> 00:07:31,720
themselves, so I went into the kitchen and found the fuse box and turned off their electricity

65
00:07:31,720 --> 00:07:37,680
so that turned off all the fans and of course turned off their playstation and they were

66
00:07:37,680 --> 00:07:44,280
lying in the middle of summer in this basement room suffering all of a sudden, of course

67
00:07:44,280 --> 00:07:48,040
they had no clue what had happened and they didn't know where the fuse box was, so then

68
00:07:48,040 --> 00:07:53,160
it was a lot of fun and then I was able to really talk to them and I taught them these,

69
00:07:53,160 --> 00:08:04,280
I read with them, these joticles, I didn't have a big effect, but I think because they

70
00:08:04,280 --> 00:08:09,040
weren't that interested and after a while, because other things had a more of an effect,

71
00:08:09,040 --> 00:08:19,400
it was much more hands-on, I ended up taking them on a camping trip and we ended up turning

72
00:08:19,400 --> 00:08:24,920
from the joticles to the Majima Nikayam, the Balapandita suit that I think and because

73
00:08:24,920 --> 00:08:28,840
they were asking suddenly when I think they read in one of the joticles about hell and

74
00:08:28,840 --> 00:08:34,520
they were asking about hell and so then I got into the, as Larry you were talking about

75
00:08:34,520 --> 00:08:38,920
dropping drawn and quartered, well there's descriptions of what the hell is like and

76
00:08:38,920 --> 00:08:43,480
drawn and quartered is actually pretty tame in comparison with some of the tortures that

77
00:08:43,480 --> 00:08:47,240
they talk about in the Majima Nikayam, so this really hit the gangster kids, through

78
00:08:47,240 --> 00:08:52,360
these two gangster kids who really, really were moved by the whole experience of ordaining

79
00:08:52,360 --> 00:08:58,360
and my torturing them with these stories and then talking about ghosts in the monastery

80
00:08:58,360 --> 00:09:02,360
and so on because I was just relating other people telling me ghost stories that they'd

81
00:09:02,360 --> 00:09:08,840
seen ghosts in the monastery and so one night I was telling them what I had heard with the

82
00:09:08,840 --> 00:09:13,960
intent to really freak them out because they really needed to be freaked out and then in the

83
00:09:13,960 --> 00:09:19,080
morning the abbot came down and scolded me and said did you tell, were you telling them

84
00:09:19,080 --> 00:09:23,320
ghost stories last night? I was just telling them what I had heard. He said well last night

85
00:09:23,320 --> 00:09:30,280
you know I had 10 novices come to my room and refuse to sleep anywhere, refuse to

86
00:09:30,280 --> 00:09:38,760
leave and ended up sleeping on his floor in his room. So I think that's a good example,

87
00:09:38,760 --> 00:09:44,200
I mean ghost stories can work for certain kids, I mean these were these were kids who who really

88
00:09:44,200 --> 00:09:53,240
had no many of them anyway, some of them were good, many of them had no strong parental guidance

89
00:09:53,240 --> 00:09:57,880
and so it was necessary to shock them with things like that but I think for good kids man the

90
00:09:57,880 --> 00:10:04,280
jot, jot is can be a wonderful experience, it's a shame that most of them are still not,

91
00:10:04,280 --> 00:10:07,960
well they're not, they're still mostly not translated, they're translated in old old

92
00:10:07,960 --> 00:10:15,240
English which which kids would not understand but there are some retelling jot, retold jot

93
00:10:15,240 --> 00:10:19,960
the guys which are lots of fun and I think would be really good for kids. I gave them to my

94
00:10:19,960 --> 00:10:26,040
brothers once, I was kind of in my proselytizing face, I was new to Buddhism and I wanted everyone

95
00:10:26,040 --> 00:10:31,160
to become Buddhists, I gave them away, I printed them up and gave them to my brothers and think

96
00:10:31,160 --> 00:10:38,360
they ever read them but I would recommend the jot because specifically that if you if you want

97
00:10:38,360 --> 00:10:43,160
to tell fairy tales, another good thing is on the internet there's this one guy on youtube

98
00:10:45,720 --> 00:10:51,320
who actually has some of the jot because in cartoon form, I don't know where he got them whether

99
00:10:51,320 --> 00:10:56,200
he made them or not but it looks like he's making or someone's making in India all sorts of

100
00:10:56,200 --> 00:11:02,040
cartoons mostly Hindu but some Buddhists. So if you look up jot like on youtube, I bet you'll find

101
00:11:02,040 --> 00:11:08,360
his stuff and you'll probably find some other stuff and and you know get the book, this book

102
00:11:08,360 --> 00:11:14,360
off Buddha net and print it up and read the jot because here kids read these stories to your kids

103
00:11:14,360 --> 00:11:19,080
because they're wonderful, some of them are just excellent you know if I had been read this stuff

104
00:11:19,080 --> 00:11:26,200
when I was a kid, I can't imagine how different my life would have been to have this kind of

105
00:11:27,400 --> 00:11:33,560
moral teaching and to have heard something so beautiful and so pure because that's really what's

106
00:11:33,560 --> 00:11:44,760
lacking in most of most of our our fairy tales and our our stories and our entertainment.

107
00:11:44,760 --> 00:11:57,400
We people nowadays can put together brilliant entertainment but tend to lack in any moral,

108
00:11:57,400 --> 00:12:06,040
you know don't think to put a moral in the movie or the book or the story. Disney did some good

109
00:12:06,040 --> 00:12:10,920
stuff I think later on, the early stuff was probably pretty horrible, it was pretty horrible for

110
00:12:10,920 --> 00:12:20,120
morals but I don't know I mean I haven't watched Disney in a long time but you know Mulan I

111
00:12:20,120 --> 00:12:24,200
think that was kind of you know I mean it was not in a Buddhist sense but it was good for that

112
00:12:24,200 --> 00:12:29,560
was a good one for you know seeing the equality of gender equality I'm trying to think now.

113
00:12:30,280 --> 00:12:35,640
I know Disney got better, I think they did eventually have you know like like what is it that

114
00:12:35,640 --> 00:12:44,520
Lion King? Not totally Buddhist but some idea of letting go right letting go of the past this

115
00:12:45,240 --> 00:12:51,960
that that one scene where where he's told to let go of the past because he says yeah yeah I

116
00:12:51,960 --> 00:12:55,720
don't want to go back because how bad the past was in this monkey hits him in the head

117
00:12:56,440 --> 00:13:00,840
and he says oh that hurts he says what would he mean it hurts it's in the past or some oh no

118
00:13:00,840 --> 00:13:05,400
I'm getting it all mixed up. He says I don't want to go back to the past a kunamata that

119
00:13:05,400 --> 00:13:11,000
city's saying that's an interesting it's actually a weird lesson isn't it so he's given up the past

120
00:13:11,000 --> 00:13:14,360
he says I don't want to deal with the past I don't want to worry about it and then he hits him

121
00:13:15,000 --> 00:13:20,840
and he says what it's in the past and then you realize this ah the past is important which is weird

122
00:13:20,840 --> 00:13:27,880
it's not really Buddhist anyway I just mean to give as examples of stories that do have morals

123
00:13:27,880 --> 00:13:34,040
whether they're Buddhist morals or not but for the most part you know look at all of these

124
00:13:34,040 --> 00:13:39,480
fairy tales that we did a project in high school on on nursery rhymes and fairy tales

125
00:13:40,440 --> 00:13:44,360
and they're really weird you know like Humpty Dumpty for example

126
00:13:46,440 --> 00:13:53,160
little Jack Horner a little Jack Horner I think is actually supposed to have stolen this deed

127
00:13:53,160 --> 00:13:55,960
or something I mean it's weren't for these weren't actually children's stories

128
00:13:55,960 --> 00:14:04,520
um georgi porgy I mean what are these things this is this is this is the stuff that I mean this

129
00:14:04,520 --> 00:14:10,920
is the kind of stuff that kids like because it's kind of naughty but you know and what is this

130
00:14:10,920 --> 00:14:14,760
rock-a-bye baby on the tree tops I mean horrific

131
00:14:17,880 --> 00:14:22,920
horrific stuff this is useless and meaningless so supplanting this with some really cool Buddhist

132
00:14:22,920 --> 00:14:30,120
stuff would be great so recommend the jotikas

