1
00:00:00,000 --> 00:00:29,440
Good evening everyone, broadcasting live, February 24th, 2016.

2
00:00:29,440 --> 00:00:39,200
Today, we have a quote from the Sanyatani Kaya, it's a pretty awesome quote, one of those

3
00:00:39,200 --> 00:00:50,660
quoteables, so in Pali we have Saya, Tapi, Bhikhave, Kumbu, Anand, Haru, Suk, Poateo,

4
00:00:50,660 --> 00:01:08,000
Hote, Sadharo, Du Poateo, Hote, Bhikhave, all because you have seen the Baya in Samsara, the

5
00:01:08,000 --> 00:01:19,960
danger in Samsara, Saya, Tapi, Kumbu, just as a pot, Anand, Haru, Suk, Poateo,

6
00:01:19,960 --> 00:01:31,040
Hote, Apat, Anand, Haru, without supports, Suk, Poateo, Hote is easily overturned,

7
00:01:31,040 --> 00:01:41,840
Sadharo, Du Poateo, with supports, it is hard to turn off.

8
00:01:41,840 --> 00:01:56,680
Bhikhave, Thang, Anand, Haru, Suk, Poateo, Hote, Sadharo, Du Poateo, Hote, Ava, Mavakho,

9
00:01:56,680 --> 00:02:06,000
even so because, Thang, Anand, Haru, mind, without supports, Suk, Poateo, is easily

10
00:02:06,000 --> 00:02:19,760
overturned, or upset, Sadharo, Du Poateo, Hote, with supports, hard to upset.

11
00:02:19,760 --> 00:02:29,280
Go to Bhikhavejita, Sadharo, and what, or because, is a support for the mind, Ayameva,

12
00:02:29,280 --> 00:02:39,000
Ariyova, Thang, Du Poateo, Mango, this very noble eightfold path, so the noble eightfold

13
00:02:39,000 --> 00:02:42,120
path is a support for the mind.

14
00:02:42,120 --> 00:02:47,440
The idea that the Buddha is getting out here is that the eightfold number path, the eight

15
00:02:47,440 --> 00:02:56,680
path factors, work together to support the mind, and you can't have a pastor enlightenment

16
00:02:56,680 --> 00:03:00,160
without all eight factors.

17
00:03:00,160 --> 00:03:10,160
If one factor is wrong, if you have a wrong view or wrong thought, or wrong action, wrong

18
00:03:10,160 --> 00:03:22,320
speech, wrong action, wrong livelihood, wrong effort, wrong mindfulness, wrong concentration,

19
00:03:22,320 --> 00:03:35,960
took Poateo, easily overturned, mind is easily shaken.

20
00:03:35,960 --> 00:03:44,960
This idea of being unshaken, this is the essence of the Buddhist path, there is this

21
00:03:44,960 --> 00:03:46,960
idea of being invincible.

22
00:03:46,960 --> 00:04:00,240
I mean, they are given, it's they are given given against staying in some sorrow, against

23
00:04:00,240 --> 00:04:13,600
seeking out happiness in impermanent things, because that sort of thing is unstable.

24
00:04:13,600 --> 00:04:24,280
It's sort of things where renders you unstable, vulnerable, invincible, I don't think

25
00:04:24,280 --> 00:04:32,720
it's such a word, suppose the invincible probably comes from the Latin, Winko, Winkari,

26
00:04:32,720 --> 00:04:46,480
which is to conquer, Winko, mean would mean to conquer, in Winko, or invincible, just

27
00:04:46,480 --> 00:05:00,920
guessing, not subject to being conquered.

28
00:05:00,920 --> 00:05:16,280
So the idea behind Buddhism is to find a unassailable, indisputable, you know, the kind

29
00:05:16,280 --> 00:05:34,280
of state that you can't, that is free from danger, that has no opening for attack.

30
00:05:34,280 --> 00:05:37,480
So these, and if this is, these are the, these are what the supports of the 8th full

31
00:05:37,480 --> 00:05:44,400
noble path give, the idea, the claim is through the practice of the 8th full noble path,

32
00:05:44,400 --> 00:05:51,160
for keeping to the 8th full noble path, if one perfects all aid of these factors, right

33
00:05:51,160 --> 00:05:57,240
view, right thought, right speech, right action, right livelihood, right effort, right mindfulness,

34
00:05:57,240 --> 00:06:06,360
right concentration, one that's invincible, this, this supports the mind in always, that

35
00:06:06,360 --> 00:06:14,080
there's nothing that could break through the defenses, nothing could tip you over like

36
00:06:14,080 --> 00:06:27,360
a pot, but the 8th full noble path you might ask, well can I just use some of them, some

37
00:06:27,360 --> 00:06:31,520
of the supports, or can I work on one of the supports at once, it doesn't really work

38
00:06:31,520 --> 00:06:40,600
that way, and the 8th full noble path is a lot more theoretical than it is practical,

39
00:06:40,600 --> 00:06:45,920
in the sense that it's important to know about these things, but in terms of practice,

40
00:06:45,920 --> 00:06:50,480
you don't just go about practicing one and then practicing the other or so on, perfecting

41
00:06:50,480 --> 00:06:54,080
one and then okay, that one's perfect, let's go perfect the other, it's more that when

42
00:06:54,080 --> 00:06:59,560
you practice, your practice has to incorporate all 8 of these, so if you're practicing

43
00:06:59,560 --> 00:07:04,080
insight meditation and you're lacking one of those, then you can, you can know that your

44
00:07:04,080 --> 00:07:15,880
practice is lacking, it's not that you actually have to practice all 8, but in some ways

45
00:07:15,880 --> 00:07:19,120
one way of looking at it is all you need is right view, and once you have right view,

46
00:07:19,120 --> 00:07:24,560
the other seven follow along, another way of looking at it is you start with mindfulness,

47
00:07:24,560 --> 00:07:31,240
and when you have mindfulness, then effort and concentration build up, and based on that,

48
00:07:31,240 --> 00:07:37,880
all the rest of the path factors, I mean practically speaking, it's not so simple as practicing

49
00:07:37,880 --> 00:07:43,200
the full noble path, you have to understand each of the 8 and where they fit in, because

50
00:07:43,200 --> 00:07:48,360
it's not like you can say, oh my livelihood is pure, so there I purify livelihood, it's

51
00:07:48,360 --> 00:07:56,040
not really what it means, it's just as part of those things that I have to be abstained

52
00:07:56,040 --> 00:08:07,160
from that are vulnerabilities, you need that support, all 8 are necessary, and they come

53
00:08:07,160 --> 00:08:11,400
through proper practice, it's actually not something you have to worry too much about,

54
00:08:11,400 --> 00:08:17,040
they go on my missing one, it's good to see if you are missing one, but if you practice

55
00:08:17,040 --> 00:08:22,040
mindfulness, if you practice that day, if you cultivate the five faculties and balance them

56
00:08:22,040 --> 00:08:28,200
and then strengthen them, the 8 full noble paths will come by itself, the 8 full noble

57
00:08:28,200 --> 00:08:33,840
path is really at the moment when you see clearly, when you just see perfectly for the

58
00:08:33,840 --> 00:08:42,480
first time, and it's one moment, the noble path, one moment cuts the defilements and opens

59
00:08:42,480 --> 00:08:49,920
the door to nibana, everything after that is fruit, it's the fruition, but it's only

60
00:08:49,920 --> 00:08:55,880
one moment is the path, it's when everything comes together perfectly and that one moment,

61
00:08:55,880 --> 00:09:09,360
they are perfectly supported and the mind is able to break free, so anyway, we had a couple

62
00:09:09,360 --> 00:09:17,640
visitors this evening, a woman and her father came by to practice meditation, her father,

63
00:09:17,640 --> 00:09:22,840
I probably made a mistake by teaching the walking meditation first, because I demonstrated

64
00:09:22,840 --> 00:09:29,680
walking meditation, her father said, oh no, he's got some kind of Scottish accent or

65
00:09:29,680 --> 00:09:36,320
some things, that's not really what we were expecting now, he said, but she said, well

66
00:09:36,320 --> 00:09:43,080
it's exactly what I was expecting, he wasn't having it, so I said okay you can just sit

67
00:09:43,080 --> 00:09:47,320
there and you don't have to do it, probably should have taught the sitting meditation

68
00:09:47,320 --> 00:10:02,840
first, would have been more palatable I think, it was kind of cute and he was hard of

69
00:10:02,840 --> 00:10:19,120
hearing as well as a bit, but yeah, so people are coming out, some engineers or something,

70
00:10:19,120 --> 00:10:31,480
some group, some class, they want to hold a meditation class, yeah, lots of interesting

71
00:10:31,480 --> 00:10:37,800
things, of course today was Wednesday, so I taught meditation in the gym, three new people

72
00:10:37,800 --> 00:10:44,160
and three or four old people, some people are coming back every week, which is great, we

73
00:10:44,160 --> 00:10:50,880
had, we must have had eight people, nine people maybe there today, and on Mondays people

74
00:10:50,880 --> 00:10:57,960
are coming, Friday's not so much, but then Friday's I do all day, this Friday I'll be

75
00:10:57,960 --> 00:11:11,920
doing all day, five minute meditation lessons, as usual, so if you have questions, come

76
00:11:11,920 --> 00:11:22,000
on, hang out, thank you Pandit, I have a question, alright, come for me, so we're talking

77
00:11:22,000 --> 00:11:28,160
about the reflections, I was just thinking about the ten perfections, the Buddha mentioned

78
00:11:28,160 --> 00:11:36,680
and how they relate to the moment of time, or are they different from these eight perfections,

79
00:11:36,680 --> 00:11:41,720
which are not really cool, was the word perfect is wrong, he shouldn't have used the word

80
00:11:41,720 --> 00:11:45,880
perfect, because it's confusing, it's not what the word Samma means, Samma doesn't mean

81
00:11:45,880 --> 00:11:53,560
perfect, it means right, I'm not even sure, I don't know the etymology, but it's always

82
00:11:53,560 --> 00:11:59,520
translated as right, perfect, I don't think it has the sense of perfect, no it means

83
00:11:59,520 --> 00:12:07,120
right, there's no question, this monk is the only, he's a really good monk and as far

84
00:12:07,120 --> 00:12:17,600
as I know, he's a good guy, but he's got some entranslation to that, so he's translating

85
00:12:17,600 --> 00:12:27,200
Samma as perfect, the ten perfections that the word is Parami or Paramita, and it comes

86
00:12:27,200 --> 00:12:37,040
from the word Parama, Parama means having no, nothing beyond, which means ultimate,

87
00:12:37,040 --> 00:12:44,520
so highest, so completions may be perfections, it works really well for a translation

88
00:12:44,520 --> 00:13:00,520
of Paramita, means that which is the highest, perfection really, thank you Paramita,

89
00:13:00,520 --> 00:13:14,720
hi Larry, good evening, good to see you, good to see you, thanks, so I had an exam this

90
00:13:14,720 --> 00:13:29,320
afternoon, and the lotus sutra had a chance to take it apart and criticize it, I was

91
00:13:29,320 --> 00:13:39,000
not that critical, you can't be, I mean you've got to be fair, we're scholars, so we can't

92
00:13:39,000 --> 00:13:48,840
be palimasists, but it's kind of disgusting, the lotus sutra kind of disgusting, it

93
00:13:48,840 --> 00:14:09,760
just so obviously, I don't know, it's hard to see them putting words in the buddha's mouth,

94
00:14:09,760 --> 00:14:13,560
I mean there's criticism that, well that that polydepitika, we don't know that the buddha

95
00:14:13,560 --> 00:14:19,000
said all of it, and I'll agree that some of the commentaries may have taken liberties, but

96
00:14:19,000 --> 00:14:26,480
it's not so blatant, so I can't say one way or the other, this is pretty blatant, I mean

97
00:14:26,480 --> 00:14:33,680
they've made up all sorts of new doctrines and suddenly the buddha's this, I don't know,

98
00:14:33,680 --> 00:14:37,800
some kind of angel up and have it, everybody in the class is confused and I'm like, yeah,

99
00:14:37,800 --> 00:14:45,880
I understand your confusion, it's like this is Buddhism, no, this is for many of them,

100
00:14:45,880 --> 00:14:50,040
this is their first introduction of Buddhism, which is a shame because it's all messed

101
00:14:50,040 --> 00:14:56,040
up, but he's doing a good job of showing how it got messed up over the course of its movement

102
00:14:56,040 --> 00:15:02,120
to East Asia, I mean I think he actually agrees with Mahayana principles, but Mahayana

103
00:15:02,120 --> 00:15:10,340
grew up in India, and Mahayana in India is different from I think even the Lotus Sutra,

104
00:15:10,340 --> 00:15:14,800
Lotus Sutra is Indian, but never really gained much ground in India, gained a lot more

105
00:15:14,800 --> 00:15:24,120
ground in East Asia, but Buddhism in East Asia is an interesting phenomenon, they developed

106
00:15:24,120 --> 00:15:29,800
this idea that the Buddha has three bodies, so he's actually up in heaven, and in Nibbana

107
00:15:29,800 --> 00:15:37,120
end on earth, and he comes to earth as like it's just an illusion, his birth and his 45

108
00:15:37,120 --> 00:15:48,960
years of teaching, and the 45 years of teaching is lies, that's what Uplayakusa is,

109
00:15:48,960 --> 00:15:56,000
he lies in order to get us to the point where we can hear the truth, he tricks us into

110
00:15:56,000 --> 00:16:03,800
becoming our Hans, because that's the best we can do, because we're too deluded to understand

111
00:16:03,800 --> 00:16:09,160
that we all have to become Buddhas, that's what, I mean I'm not making this up and this

112
00:16:09,160 --> 00:16:14,200
isn't actually what the Lotus Sutra says, so I'm probably making a lot of Buddhas to

113
00:16:14,200 --> 00:16:26,960
angry by saying this on the internet, but you know, what has that ever stopped? I don't know,

114
00:16:26,960 --> 00:16:32,120
I think the heart Sutra is better actually, isn't that the one where Rupan Shulnya

115
00:16:32,120 --> 00:16:38,160
dang isn't that the heart Sutra? I really can't tell. I think the heart Sutra

116
00:16:38,160 --> 00:16:45,160
doesn't, but the heart Sutra I don't like it because it's kind of suffistic, it's

117
00:16:45,160 --> 00:16:50,280
softism, you know, softism, like just sounding intelligent, like it says, form is emptiness,

118
00:16:50,280 --> 00:16:55,520
emptiness is form, what the heck does that mean? And of course they say, ah, you see, you're

119
00:16:55,520 --> 00:17:00,240
not Mayana, so you don't get to understand, and I'm like, go away, I have no use for

120
00:17:00,240 --> 00:17:10,240
yourself is them. It's just sounding, you sure anyone can sound, I should stop before I get

121
00:17:10,240 --> 00:17:14,120
in trouble, this is bad. I'm sad.

122
00:17:14,120 --> 00:17:22,520
I had an experience like that listening to talk. I don't know, if you practice this, and

123
00:17:22,520 --> 00:17:30,200
then you go in here, people talk about emptiness, and they don't, all sorts of crazy

124
00:17:30,200 --> 00:17:36,560
ness. Anyway, there shouldn't be too critical of other people's beliefs. I promise that

125
00:17:36,560 --> 00:17:44,360
I, I always promise that I won't do this. I won't go on and on about other people's

126
00:17:44,360 --> 00:17:50,000
practices and beliefs. Then I do, don't I, I've criticized Christianity, I think I've

127
00:17:50,000 --> 00:17:57,240
gotten into Islam, Judaism, I just have a torn into Judaism yet.

128
00:17:57,240 --> 00:18:04,600
They're all garbage, they're all wrong, just not the kind of thing you should say on

129
00:18:04,600 --> 00:18:11,680
the internet. I mean, I like reading criticism and of course criticism of our own tradition

130
00:18:11,680 --> 00:18:12,680
as well.

131
00:18:12,680 --> 00:18:17,560
Yeah, we should criticize our own traditions. I'll never be open to it.

132
00:18:17,560 --> 00:18:23,560
So this is, this is something I've been looking at, in case anyone was interested in

133
00:18:23,560 --> 00:18:30,280
criticism of the traditions. I just, I found that interesting, there's something in

134
00:18:30,280 --> 00:18:31,280
this picture.

135
00:18:31,280 --> 00:18:39,040
You know, there's, you know something that I didn't hear at all, but I caught part of

136
00:18:39,040 --> 00:18:45,120
a speech, and I want you all to know about this guy because I'm not supposed to be political,

137
00:18:45,120 --> 00:18:48,560
it's another thing we're not supposed to be, but there's this guy running for president

138
00:18:48,560 --> 00:18:55,840
of the United States. And he said something I think it was last night or the night before.

139
00:18:55,840 --> 00:19:02,360
That was pretty awesome. I mean, maybe not terra lotta Buddhist awesome, but pretty

140
00:19:02,360 --> 00:19:08,160
awesome in the last. Bernie Sanders, did anybody know this guy, Bernie Sanders?

141
00:19:08,160 --> 00:19:20,920
He's very popular with a lot of the Democrats, very, maybe more so than Hillary Clinton.

142
00:19:20,920 --> 00:19:26,520
I think they are the two that are by most closely for the Democratic nomination.

143
00:19:26,520 --> 00:19:37,160
Right. He said, they asked him what his religion was, I think. And he says, my religion

144
00:19:37,160 --> 00:19:46,160
is that we're all in this together. When you hurt, when your child hurts, I hurt.

145
00:19:46,160 --> 00:19:56,920
That is something like that. Your child is my child. My child is your child. He totally

146
00:19:56,920 --> 00:20:01,800
breaking down barriers about, you know, it's basically, I guess it's a lot like the song

147
00:20:01,800 --> 00:20:10,240
I imagined by John Lennon. The idea that we're just people, I mean, I guess you could

148
00:20:10,240 --> 00:20:16,000
argue could label him as a humanist. He believes in people. And he believes that when we

149
00:20:16,000 --> 00:20:22,840
stand together, when we come together, when we work together, good things happen. When

150
00:20:22,840 --> 00:20:28,360
we cooperate instead of, you know, so he's a socialist, which means he believes in cooperation.

151
00:20:28,360 --> 00:20:34,760
I mean, that's absolutely the big problem with capitalism is it's about competition. And

152
00:20:34,760 --> 00:20:46,360
it's based on the virtue of competing with each other as opposed to cooperating, as

153
00:20:46,360 --> 00:20:52,600
opposed to working together for the greater good. And so he's all about breaking down barriers,

154
00:20:52,600 --> 00:21:01,240
breaking down walls, divisions, bringing people together to, for goodness. I mean, if you

155
00:21:01,240 --> 00:21:06,360
listen to him talk, he's honest. If you don't like what he says, you can't argue that

156
00:21:06,360 --> 00:21:13,400
this guy is a liar or a shister, sorry, that's a bad word, but he has Jewish, I think,

157
00:21:13,400 --> 00:21:22,440
you know, shisters not a good word, I apologize. But, you know, that kind of, he's honest

158
00:21:22,440 --> 00:21:29,400
and sincere. You ask, does he want to help people or does he just want to become president?

159
00:21:29,400 --> 00:21:36,520
You know, this is a guy who is in politics because he wants to help people. I mean, how

160
00:21:36,520 --> 00:21:43,720
awesome is this? How many, how long have we wished or hoped for such a person who wants to lead

161
00:21:43,720 --> 00:21:52,120
the people out of the goodness of his heart? I mean, this doesn't exist very often. I could

162
00:21:52,120 --> 00:21:57,560
tell you that. It doesn't exist. Do you know who are you? Sorry, I just joined, that's how I said.

163
00:21:57,560 --> 00:22:06,200
How do you know who Bernie Sanders is? Yeah, I'm from the UK and in my country. It's very

164
00:22:06,200 --> 00:22:15,400
competitive based. So it's very capitalist in this country. They want to get rid of free health care

165
00:22:15,400 --> 00:22:25,080
which is not great. We have free health care here in Canada. Yeah, they want to get rid of it

166
00:22:25,080 --> 00:22:33,240
because it's... What's the thing? I mean, what's Bernie Sanders platform? He wants to, you know,

167
00:22:33,240 --> 00:22:38,600
I don't know. I don't want to get into what he's giving people and so on. But to care about the

168
00:22:38,600 --> 00:22:43,000
fact that there are millions of people who can't get adequate health care. I mean, there's people

169
00:22:43,000 --> 00:22:48,680
who are... I get... These people are my students. I have one student who was telling me his mother

170
00:22:48,680 --> 00:22:56,360
got sick and he was basically having to choose between food and medicine and rent. I mean,

171
00:22:56,360 --> 00:23:03,960
living on a budget that you can't live off of. And the things... He talks about these things.

172
00:23:03,960 --> 00:23:10,200
Who else is talking about these things and making them platform? There are people starving. There

173
00:23:10,200 --> 00:23:16,120
are people... There are children living in poverty. In the richest country in the world, I guess,

174
00:23:16,120 --> 00:23:23,160
United States is the richest country in the world. I don't know. And there's an obscene number

175
00:23:23,160 --> 00:23:28,040
of people living in poverty who don't have health care who can't get an education. He wants

176
00:23:28,040 --> 00:23:36,600
education to be free, which is really a no-brainer to pull a bit of a pun. But I mean,

177
00:23:38,280 --> 00:23:42,760
education... Going back to school, I mean, I've been talking to students here and saying,

178
00:23:42,760 --> 00:23:48,760
there are students here in Canada where it's heavily subsidized who still can't get an

179
00:23:48,760 --> 00:24:01,000
education, who still have to work two jobs just to be able to take classes. And as a result,

180
00:24:01,000 --> 00:24:04,600
aren't learning any... Well, aren't learning what they should be learning because they're too

181
00:24:04,600 --> 00:24:10,760
tired because they're too stressed. This is... I mean, maybe this is all worldly talk, but if we

182
00:24:10,760 --> 00:24:17,800
talk about helping people and people's spiritual well-being, goodness, remember Buddhism, all

183
00:24:17,800 --> 00:24:26,200
about goodness. And there's a lot of badness happening. There's a lot of greed, corruption,

184
00:24:27,160 --> 00:24:33,240
and just too much politicizing people who want to be who are politicians just for their own

185
00:24:33,240 --> 00:24:43,320
benefit, you know. They get their bought by the rich elite and they make lots of money giving

186
00:24:43,320 --> 00:24:51,560
speeches and holding these dinners and whatever. I mean, guaranteeing that the rich stay rich

187
00:24:51,560 --> 00:24:57,000
and the poor stay poor. I mean, you can't argue with the facts. The fact is there's lots and lots

188
00:24:57,000 --> 00:25:01,720
of people who are very, very poor and that hasn't changed and isn't changing. It's getting

189
00:25:01,720 --> 00:25:13,160
worse. Here comes Robin to tell me I'm renting. Robin, you can talk for yourself. I won't say

190
00:25:13,160 --> 00:25:18,600
anything you don't want me to say on the internet. No, I'm here to say, go Bernie. You're in

191
00:25:18,600 --> 00:25:26,120
with Bernie. Absolutely. I actually changed my party affiliation today. I was going to say she's a

192
00:25:26,120 --> 00:25:31,320
Republican and we were talking about Bernie. I changed so that I can vote for Bernie. He's

193
00:25:31,320 --> 00:25:40,440
awesome. Well, I think if we're any, if you and I are any indication, he's got the Buddhist vote.

194
00:25:41,080 --> 00:25:51,240
I think so. Not that I know. He comes about as close to being what used to be referred to as a

195
00:25:51,240 --> 00:26:01,960
true statesman. So many politicians are just hey boys for corporate power and wealthy people. It's

196
00:26:01,960 --> 00:26:08,840
amazing that he's been able to stay in office so long. You're in office by virtue of the votes

197
00:26:08,840 --> 00:26:18,440
of a constituency. So he must have a very enlightened constituency. He has like 83% of approval

198
00:26:18,440 --> 00:26:26,840
rating in his state or something. 80% more. It's just like, hey, yeah. In a poll that came out right

199
00:26:26,840 --> 00:26:34,600
after the New Hampshire primary where he just won in a landslide, there was a poll where the question

200
00:26:34,600 --> 00:26:42,040
was, are the candidates trustworthy? Well, the right button. It got like 5% and he had 95%. So no

201
00:26:42,040 --> 00:26:46,280
matter what, you know, you think of his policies. People do believe that he's dead.

202
00:26:46,280 --> 00:26:53,240
I mean, that's to me that's a very, very important point. As before we talk about what he's

203
00:26:54,360 --> 00:26:59,560
promising, you know, what's his character? You know, because politicians promise everything.

204
00:26:59,560 --> 00:27:04,920
All the politicians promise what they think will get them elected. I bet he's guilty of that a

205
00:27:04,920 --> 00:27:09,000
little bit as well. He knows, well, I have to say this or I won't get elected even though

206
00:27:09,000 --> 00:27:21,160
maybe he doesn't quite believe it. But there's not that that isn't a huge question in people's

207
00:27:21,160 --> 00:27:26,680
minds because they know he's sincere. And they know that the majority of what he at least the

208
00:27:26,680 --> 00:27:33,400
majority of not all of what he stands for, he really stands for. And that, I mean, that's important.

209
00:27:33,400 --> 00:27:41,640
It's not tricking it. It's not deceptive. There seems to be very much in favor of placing

210
00:27:42,920 --> 00:27:53,000
constraints on corporations and throttling corporations, which is so very much needed in this country

211
00:27:53,000 --> 00:28:05,480
and corporate power and the very wealthy powerful people that are running corporations is just

212
00:28:05,480 --> 00:28:16,040
horrendous. And he seems to be in favor of trying to deal with that, which would be really

213
00:28:16,040 --> 00:28:23,400
difficult, you know, because presidents got to work with the Congress. And we've of course seen

214
00:28:24,440 --> 00:28:31,960
the Obama's difficulties working with the Congress that we have had the last few years.

215
00:28:35,000 --> 00:28:39,800
The other neat thing about him is he does work well with people. I mean, when you're that

216
00:28:39,800 --> 00:28:46,520
honest, he gets things done. He apparently has a record people saying, well, he's how could he ever

217
00:28:46,520 --> 00:28:51,960
work. He's got a 40 year record, a long record of working with people, actually getting strong,

218
00:28:52,760 --> 00:28:59,160
which, I mean, that's it. You said it. He's a real statesman. The Republicans appreciate that.

219
00:28:59,160 --> 00:29:12,760
Well, some of them, sure, but he's worked with, anyway. I just wanted to say, here's someone

220
00:29:13,320 --> 00:29:20,440
who is worthy of support. We need someone out over here, every new kid. Didn't you guys

221
00:29:20,440 --> 00:29:29,320
have liked some socialist as well? Well, we're like very capitalist over here, not very socialist. So, we've got

222
00:29:29,320 --> 00:29:39,320
corporate businesses and street guidelines and such. It's not the kind of society that I enjoy

223
00:29:39,320 --> 00:29:52,120
living in. The UK has a history of impasse competition. In Denmark, he actually, as soon as you leave

224
00:29:52,120 --> 00:29:56,840
at what's it called, like, primary school. I mean, as soon as you're not really a kid anymore,

225
00:29:56,840 --> 00:30:02,760
you're going to start getting paid to go to school, paid for business school and university.

226
00:30:02,760 --> 00:30:10,280
Just enough to, if you have an apartment, a small apartment. I mean, it's really a no brainer.

227
00:30:10,280 --> 00:30:17,080
What's the result of that? As the result, people who are unproductive and know you have people

228
00:30:17,080 --> 00:30:25,160
who are intelligent. The problem is rich people are not rich, but rich people are not all bad,

229
00:30:25,160 --> 00:30:35,800
but greedy, corporate, terrible people. One people to be stupid. It's really, this is,

230
00:30:36,520 --> 00:30:46,440
it doesn't like also like selfism as well. It's not like selfism as well, where some people

231
00:30:46,440 --> 00:30:52,440
hold to the knowledge and want to dumb down other people. That's not, I think, what selfism

232
00:30:52,440 --> 00:30:57,160
means. Selfism is when you just try to sound intelligent. That's not something else entirely.

233
00:30:59,000 --> 00:31:03,640
It's elitism or something, or I don't know. There's probably a word for it.

234
00:31:07,960 --> 00:31:13,800
I think there's great, one of the great things about America is that rich, a lot of rich people do

235
00:31:13,800 --> 00:31:22,040
do good things. Those people who have gained power, Bill Gates, for example. Apparently, there's

236
00:31:22,040 --> 00:31:29,160
conspiracy surrounding him, so maybe he's a terrible person as well. I don't know. The ideal of a

237
00:31:29,160 --> 00:31:35,560
person who gets very rich from something like Microsoft Windows or Microsoft, which is actually a

238
00:31:35,560 --> 00:31:47,640
terrible operating system. Then to use that money to say wipe out malaria or polio or polio,

239
00:31:47,640 --> 00:31:52,600
which is a really terrible terrible disease. So whether the facts, I don't know what the facts are

240
00:31:52,600 --> 00:32:02,040
and criticisms of him, particularly inside, there is a sense of rich people being charitable.

241
00:32:02,040 --> 00:32:05,400
I don't know. I mean, maybe I'm being charitable by saying that, I don't know, but

242
00:32:05,960 --> 00:32:11,000
no, definitely. Even like athletes in celebrities in the United States, they're very, very

243
00:32:11,000 --> 00:32:17,240
helped me open with their foundations. Many people have their own foundations. They do good works.

244
00:32:17,800 --> 00:32:22,040
It's maybe it's a little bit for publicity, but someone benefits from it anyway.

245
00:32:22,040 --> 00:32:26,920
And then you have to, I guess you have to ask whether it's systemic and whether it's as

246
00:32:26,920 --> 00:32:35,160
whether it works as well, because I don't know. Some people argue that the government

247
00:32:35,160 --> 00:32:42,520
can't. The government is full of waste and so on. Anyway, I don't want to get into all

248
00:32:42,520 --> 00:32:50,120
the politics of it, just the goodness of it. And here, I wanted to shout, give a shout out

249
00:32:50,120 --> 00:32:58,440
to Bernie Sanders for what seems to be awesome as far as I can see. He's hard as in the right

250
00:32:58,440 --> 00:33:04,600
place and he walks the walk. He actually does do good things for people. He has done good things

251
00:33:04,600 --> 00:33:12,680
for people. He's helped people. Maybe he's a Bodhisattva. Could be. Couldn't very well be.

252
00:33:12,680 --> 00:33:19,960
I mean, I guess I please endorse me yesterday. Sorry. He got Spike Lee's endorsed me yesterday.

253
00:33:19,960 --> 00:33:25,880
Who has Spike Lee? I've heard this name before. He's a filmmaker. Okay, right. I think I knew that.

254
00:33:25,880 --> 00:33:27,000
I don't know any of his films.

255
00:33:30,280 --> 00:33:34,440
Well, he's big on the rap scene as well. Just to add a little bit. Okay.

256
00:33:34,440 --> 00:33:41,800
If I could endorse a political person, he'd have my endorsement as well.

257
00:33:43,080 --> 00:33:49,560
As it is, he has my respect. How about spiritual endorsement? Is this such a thing?

258
00:33:51,400 --> 00:33:56,600
He has my spiritual endorsement, absolutely. I endorse him. I should send him one of these books.

259
00:33:56,600 --> 00:34:03,480
I'll get Aaron, I'll just send him a guide to how to meditate. Yeah, do that.

260
00:34:05,400 --> 00:34:07,080
Maybe I'll read a letter to him as well.

261
00:34:09,560 --> 00:34:11,240
Not a lot of people do that nowadays.

262
00:34:12,760 --> 00:34:20,040
Sorry. Not a lot of people write hand written letters nowadays. It's an art form now.

263
00:34:20,040 --> 00:34:32,840
Yeah. Well, not when I write chicken and scratch. You could also send in your lessons in practical

264
00:34:32,840 --> 00:34:38,120
Buddhism. Yeah, I don't imagine he's got a lot of time or interest in reading. This one's kind

265
00:34:38,120 --> 00:34:44,200
of practical, so maybe it'll be maybe his wife would read a interesting thing.

266
00:34:44,200 --> 00:34:50,680
He's less busy. He's just so busy it seems, which is another awesome thing. I mean, he's tireless.

267
00:34:51,560 --> 00:34:54,920
He really, I've seen him, you know, he's old.

268
00:34:56,280 --> 00:34:58,200
I'm going very strong.

269
00:35:01,160 --> 00:35:02,360
He pray, he's healthy.

270
00:35:03,640 --> 00:35:06,280
He what? He probably eats healthy.

271
00:35:06,280 --> 00:35:13,000
Right. Well, maybe maybe he would maybe he would like one of these.

272
00:35:15,000 --> 00:35:17,560
I'll send him a few, maybe. Sure.

273
00:35:20,920 --> 00:35:26,840
Okay, I'm going to say good night. Thank you all. Good. Good to come up with something

274
00:35:26,840 --> 00:35:39,240
interesting to talk about every night. Good night. Good night. Good night. Good night. Good night.

