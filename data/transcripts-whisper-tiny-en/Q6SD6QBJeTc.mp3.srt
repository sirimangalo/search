1
00:00:00,000 --> 00:00:09,000
We'll ask, is Clear Comprehension the same as direct understanding? How do they relate to mindfulness?

2
00:00:09,000 --> 00:00:23,000
These are just words. It's very difficult to give an answer, but if I translate these directly, then Clear Comprehension.

3
00:00:23,000 --> 00:00:31,000
Clear Comprehension is often used as a translation of some pajanya.

4
00:00:31,000 --> 00:00:35,000
But some pajanya doesn't exactly mean Clear Comprehension.

5
00:00:35,000 --> 00:00:50,000
Some means full complete or right, and pajanya means full, or it also means full.

6
00:00:50,000 --> 00:00:59,000
And janya means knowledge. So some pajanya means full and right knowledge.

7
00:00:59,000 --> 00:01:12,000
Direct understanding could be a translation of yatabutagnana-desana, which means knowledge and vision of things as they are.

8
00:01:12,000 --> 00:01:21,000
It could also be a translation of pajanya-desana, which means full understanding or full knowledge.

9
00:01:21,000 --> 00:01:38,000
But yeah, direct understanding could be a translation of sati, karana-desana, the making, seeing something for yourself or so on.

10
00:01:38,000 --> 00:01:53,000
Both of those first two are, I would say, synonyms for wisdom, synonyms for knowledge of things as they are.

11
00:01:53,000 --> 00:02:12,000
And so our proper to be said as being the same. So it's hard when you're using English words. What does the word comprehension mean? It does mean very much understanding, and I don't think that there's much difference that can be gained.

12
00:02:12,000 --> 00:02:26,000
And unless you talk about the four types of sambhajanya, then you see that sambhajanya has a specific meaning. There's goat jara-sampajanya, so on. I can't remember them all.

13
00:02:26,000 --> 00:02:34,000
As Samuha sambhajanya is the one that relates to mindfulness. Then you have the word mindfulness, which is actually a difficult word.

14
00:02:34,000 --> 00:02:45,000
Mindfulness is actually probably a fairly good translation of sambhajanya as well, even though we use it as a translation for sati.

15
00:02:45,000 --> 00:02:59,000
Because sambhajanya again is full, this idea being full, having a full awareness or knowledge, or knowledge of something fully and rightly.

16
00:02:59,000 --> 00:03:07,000
But how they relate to sati, they are very often conjoined in the Buddha's teaching.

17
00:03:07,000 --> 00:03:12,000
The Buddha will often talk about sambhajanya as being connected.

18
00:03:12,000 --> 00:03:21,000
But sati is like the taking hold and sambhajanya is like the cutting.

19
00:03:21,000 --> 00:03:29,000
Or the cutting away of when you grab hold of rice and you cut with a knife, you grab hold of it and then you cut.

20
00:03:29,000 --> 00:03:35,000
So they perform a different function. The mindfulness or the sati is the recognition of the object.

21
00:03:35,000 --> 00:03:45,000
The act of cognizing the object rightly, and the sambhajanya is what results, which is the right comprehension.

22
00:03:45,000 --> 00:03:52,000
So sati is the action that you perform, sambhajanya is the result, the wisdom that results, or that should result.

23
00:03:52,000 --> 00:03:54,000
But they have to work together.

24
00:03:54,000 --> 00:03:59,000
And you can't just use the noting, for example, and not really know what's going on.

25
00:03:59,000 --> 00:04:06,000
But you can't just know what's going on without having a firm grasp of it, or else it will give rice to diversification.

26
00:04:06,000 --> 00:04:09,000
So if you're talking about sati and sambhajanya, this is their relationship.

27
00:04:09,000 --> 00:04:16,000
sati is the grasping so that the mind doesn't waver from the knowing, and sambhajanya is that knowing itself.

28
00:04:16,000 --> 00:04:24,000
And you need both of them. Without the knowing, it's just grasping and with just concentration, saying to yourself, pain, pain, pain, for example,

29
00:04:24,000 --> 00:04:27,000
without really focusing on the pain or knowing it.

30
00:04:27,000 --> 00:04:37,000
And just knowing the pain without grasping it, just having the knowing without a firm and stable grasp of it.

31
00:04:37,000 --> 00:04:44,000
So without using the noting, it's so too easy to give rice to other thoughts, rather than just the thought of this as being pain.

32
00:04:44,000 --> 00:04:49,000
It's too easy to give rice to the thought of this as me, this is mine, even not to the thought.

33
00:04:49,000 --> 00:04:55,000
But the perception still has room to grow without the sati.

34
00:04:55,000 --> 00:05:08,000
So it requires both of these.

