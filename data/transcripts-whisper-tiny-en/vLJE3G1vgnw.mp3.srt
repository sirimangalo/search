1
00:00:00,000 --> 00:00:27,920
Okay. Good evening, everyone. Welcome to our evening

2
00:00:27,920 --> 00:00:40,480
down my session. Today we're doing questions and answers. So I'd like to just start by

3
00:00:40,480 --> 00:00:54,400
addressing what I said last week. I think I was concerned about the potential for a obsession

4
00:00:54,400 --> 00:01:01,560
with asking questions, right? Because if our Buddhist practice becomes just about talking

5
00:01:01,560 --> 00:01:08,960
about the number, wondering about the dumbest speculating about the dumb I can be, misleading.

6
00:01:08,960 --> 00:01:16,000
It can be problematic for the progress of our practice. But that being said, I'm thinking

7
00:01:16,000 --> 00:01:24,640
about it. It's really, it's really pretty awesome to have this. I mean, it's an honor to be

8
00:01:24,640 --> 00:01:34,600
asked questions and to have so many people wanting to hear what I have to say on things

9
00:01:34,600 --> 00:01:41,960
is an honor, really. And it's an honor to be able to answer questions about the Buddhist

10
00:01:41,960 --> 00:01:48,000
teaching. So on the face of it, it's really a great thing. I mean, it's great. Our whole

11
00:01:48,000 --> 00:01:56,800
community is really pretty incredible. We're all quite amazed that we have this online

12
00:01:56,800 --> 00:02:05,880
community together of people who know we, most of us have never met. I guess I'm lucky

13
00:02:05,880 --> 00:02:14,680
I get to meet many of you come to visit me or we do video chat. Sometimes we do a study

14
00:02:14,680 --> 00:02:25,320
group together. But I wouldn't just think it's worth appreciating how great it is that

15
00:02:25,320 --> 00:02:31,000
we have this opportunity to share the Buddhist teaching and something that I believe

16
00:02:31,000 --> 00:02:43,000
in quite strongly. And I think it's exceptional that 2,500 years later we're still able

17
00:02:43,000 --> 00:02:50,440
to get together. The mediums are a little bit different, but we're still able to talk about

18
00:02:50,440 --> 00:03:00,320
these things to practice these things. So let's go through the questions, no complaints.

19
00:03:00,320 --> 00:03:08,840
You know, always this slight concern that we don't think of this as the practice of

20
00:03:08,840 --> 00:03:14,800
the Buddhist teaching. This is a chance to talk about those things that get in the way

21
00:03:14,800 --> 00:03:22,080
of our practice of the Buddhist teaching. Okay, so first question, there's questions

22
00:03:22,080 --> 00:03:27,160
when I probably wouldn't normally answer, but it's well worded and I think it's a good

23
00:03:27,160 --> 00:03:33,480
opportunity to sort of talk about Buddhism in general. So the question is what the differences

24
00:03:33,480 --> 00:03:38,840
between Theravada and Zen, particularly the difference associated with Zazen and Vipasana

25
00:03:38,840 --> 00:03:44,200
meditation is one likely to lead to Nibana, whether there's fake Buddhism. And then the

26
00:03:44,200 --> 00:03:49,920
general complaint that it's very confusing to sort through the various sects without being

27
00:03:49,920 --> 00:03:54,840
puzzled by their various similarities and differences, particularly in terms of meditation

28
00:03:54,840 --> 00:04:01,400
practice. So I guess I'll start by not really answering your question, by talking about

29
00:04:01,400 --> 00:04:07,960
the second part and just hopefully providing a little bit of clarity about what is Buddhism,

30
00:04:07,960 --> 00:04:15,920
right? What is Buddhism? I took an introduction of Buddhism recently at university. I

31
00:04:15,920 --> 00:04:19,360
thought it would be interesting. It was interesting to be in the class, you know, I was

32
00:04:19,360 --> 00:04:27,600
able to help out a little bit and contribute. But they used the textbook that was entitled

33
00:04:27,600 --> 00:04:37,200
Buddhism. And I think that's fairly accurate if a little bit misleading in the sense that

34
00:04:37,200 --> 00:04:42,760
it of course has to try and treat all of them equally, right? And of course attempts as best

35
00:04:42,760 --> 00:04:50,520
it can to talk about them as if they were parallels, which isn't quite fair. I don't think

36
00:04:50,520 --> 00:05:00,960
I'm at, I think it's hard for me not to sound biased. But when we talk about Buddhism, and I'm

37
00:05:00,960 --> 00:05:10,320
mostly focused on my type of Buddhism, right? Theravada Buddhism. The difference is generally

38
00:05:10,320 --> 00:05:23,240
speaking are, are important. So Theravada Buddhism has a, we can say two sets of texts,

39
00:05:23,240 --> 00:05:30,720
two strata of texts that it focuses on. And the texts are important in defining Theravada

40
00:05:30,720 --> 00:05:35,400
Buddhism. It would be different when we talk about other types of Buddhism. But for Theravada

41
00:05:35,400 --> 00:05:43,040
Buddhism, there's these two sets of texts. There are the polycanon texts. And then there

42
00:05:43,040 --> 00:05:53,360
is the combinatorial texts. So the canon, the tepitika, is claimed to be in the Buddhist

43
00:05:53,360 --> 00:06:01,080
teaching. It's original authentic, it is what it purports to be. So it purports to be the teaching

44
00:06:01,080 --> 00:06:08,960
of the Buddha, some of his close disciples, and a few books that are pretty clearly later.

45
00:06:08,960 --> 00:06:17,280
And don't really make much pretension to not be later. But the majority of it is, is

46
00:06:17,280 --> 00:06:21,320
claimed to be what the Buddha taught. So there's that part. And then the other part

47
00:06:21,320 --> 00:06:29,640
is commentaries on that set of texts, right? These two together make up the Theravada.

48
00:06:29,640 --> 00:06:38,760
And it's important to talk about that because it defines a very specific and very ancient

49
00:06:38,760 --> 00:06:46,760
set of teachings. You might say dogmas or beliefs or religious principles or religious

50
00:06:46,760 --> 00:06:54,960
doctrine, I guess, is so it's, it's quite internally consistent. Maybe not completely.

51
00:06:54,960 --> 00:06:58,520
It may not be exactly what the Buddha taught. We don't have any proof that it was actually

52
00:06:58,520 --> 00:07:04,280
what the Buddha taught, except that when we practice it, it appears to be something quite

53
00:07:04,280 --> 00:07:11,480
profound. And for those who practice intently, they will swear up and down that it is

54
00:07:11,480 --> 00:07:16,320
definitely the teaching of an enlightened being because, well, it's enlightened as far

55
00:07:16,320 --> 00:07:22,040
as the teachings go. But it's a very old and very specific set of teachings. You've

56
00:07:22,040 --> 00:07:27,160
got what we claim to be the Buddhist teachings. And then you've got commentaries on it.

57
00:07:27,160 --> 00:07:33,800
Now they're putting aside whether they are the teachings of Buddha because, of course,

58
00:07:33,800 --> 00:07:39,520
many different schools claim to have many different texts that were taught by the Buddha.

59
00:07:39,520 --> 00:07:46,720
But it's a very internally consistent and quite simple, really. If you read the texts people

60
00:07:46,720 --> 00:07:51,160
in our tradition who have read through the, the, the, these set of texts and then go

61
00:07:51,160 --> 00:07:56,160
and read other sets of texts that claim to be Buddhist are struck by the difference. How

62
00:07:56,160 --> 00:08:13,600
much more flowery and, how much more complicated? I don't know what the right word is.

63
00:08:13,600 --> 00:08:19,680
There's a word that I can't find, but they're not as straightforward. It would be a sort

64
00:08:19,680 --> 00:08:27,000
of a pejorative description of them. But what you get with other types of Buddhism.

65
00:08:27,000 --> 00:08:33,280
And so this is in, in strong contrast to practice, to sects of Buddhism or types of Buddhism

66
00:08:33,280 --> 00:08:39,000
like Zen, where the majority of the teachings are much later and claim to be much later.

67
00:08:39,000 --> 00:08:43,200
You know, there's the teachings of Bodhi Dharma and there's, and, you know, there's

68
00:08:43,200 --> 00:08:56,600
all these teachings that are thought to be canonical, but are much, much later. When you

69
00:08:56,600 --> 00:09:07,960
focus on ten-time Buddhism, there are much later teachings. And there are, there's much

70
00:09:07,960 --> 00:09:15,840
less of a cohesion to the teachings. You know, some of them try to bring in texts from

71
00:09:15,840 --> 00:09:23,320
all over and texts that don't really have any frame of reference for where they arose

72
00:09:23,320 --> 00:09:32,080
and have a much greater emphasis on modern or, or at least pseudo-modern, semi-modern texts

73
00:09:32,080 --> 00:09:40,760
and commentary there in, generally, in different languages, a lot of Chinese. Of course,

74
00:09:40,760 --> 00:09:46,160
it was Zen, it's a lot of Japanese. So we're not talking about something quite so ancient

75
00:09:46,160 --> 00:09:57,200
or so cohesive. You know, why I approach this question from this angle is, because I want

76
00:09:57,200 --> 00:10:05,920
to personally cast a difference, like not to disparage necessarily. And I don't, I'm

77
00:10:05,920 --> 00:10:10,720
not that fond of other types of Buddhism, obviously. I mean, that comes across, I think,

78
00:10:10,720 --> 00:10:16,720
in the things that I say. But putting that aside, to be fair, terabyte of Buddhism is

79
00:10:16,720 --> 00:10:21,720
quite internally consistent. It's quite simple. I mean, from my point of view, okay, other

80
00:10:21,720 --> 00:10:27,120
people might say, not Zen can, as seen as quite simple. But it's simple in a sense. It's

81
00:10:27,120 --> 00:10:34,120
quite straightforward. There's nothing surprising in the sense of how it's composed, right?

82
00:10:34,120 --> 00:10:41,280
You've got texts. They teach something. There, it's clear what the texts are. And you

83
00:10:41,280 --> 00:10:46,480
follow them. They're internally consistent. They are what they are. So it's a package, I guess,

84
00:10:46,480 --> 00:11:00,040
as the point. It's maybe not quite fair of other, but it's, I think, I think it's hard

85
00:11:00,040 --> 00:11:06,840
to find another type of Buddhism that's quite so straightforward. And I didn't mean to

86
00:11:06,840 --> 00:11:14,920
quite sound so positive about terabyte of Buddhism. But when you're talking about being

87
00:11:14,920 --> 00:11:20,520
confused by all the types of Buddhism, I just wanted to point out how understandable that

88
00:11:20,520 --> 00:11:26,280
is, because Buddhism is, it's just a word. And what you're talking about is a lot of

89
00:11:26,280 --> 00:11:31,680
different teachers, a lot of different teachings that gets quite complicated, quite quickly,

90
00:11:31,680 --> 00:11:40,160
which texts you're focusing on, which teachers, who's enlightened, who's not, and so on.

91
00:11:40,160 --> 00:11:45,800
And I think it's fair to say that with other schools of Buddhism, it's not as easy to

92
00:11:45,800 --> 00:11:52,520
define what the teachings are. And, you know, to even get a grasp of, okay, these texts,

93
00:11:52,520 --> 00:11:59,920
right? Whereas with terabyte of Buddhism, you have that. Again, I don't mean to be so

94
00:11:59,920 --> 00:12:09,160
disparaging. But when you try to compare these types of Buddhism, you're talking about

95
00:12:09,160 --> 00:12:20,560
two fairly different approaches. And you're talking about this nebulous sort of cohesion

96
00:12:20,560 --> 00:12:26,040
that Buddhism doesn't really have. You know, it's the cohesion in Buddhism that makes

97
00:12:26,040 --> 00:12:36,040
everything Buddhism is understandably nebulous, because there are so many different teachings

98
00:12:36,040 --> 00:12:43,360
and opposing views and different schools and so on, that something that, from a terabyte

99
00:12:43,360 --> 00:12:50,560
of point of view, seems quite simple and becomes something quite complicated. I don't know.

100
00:12:50,560 --> 00:12:55,600
I'm supposing that people from other sects would probably argue, can test that point.

101
00:12:55,600 --> 00:13:01,960
But I didn't want to try to end up promoting terabyte. That's not what I'm trying to do.

102
00:13:01,960 --> 00:13:09,800
I'm just trying to maybe explain why I'm not really keen on answering questions about

103
00:13:09,800 --> 00:13:15,840
other types of Buddhism or comparing practices, because to me, it's like comparing apples

104
00:13:15,840 --> 00:13:24,000
and oranges. It's two fairly different things. Terabyte of Buddhism is one thing. And most

105
00:13:24,000 --> 00:13:31,240
other types of Buddhism are something, and from my point of view, something quite different.

106
00:13:31,240 --> 00:13:35,000
That's all I really wanted to say about that. So when you ask me about the difference

107
00:13:35,000 --> 00:13:41,080
between terabyte and zen, and zazen, and vipassen meditation, it's not fair to compare

108
00:13:41,080 --> 00:13:47,640
them, like, as two different practices, because what is zen? What is zazen even? Depends

109
00:13:47,640 --> 00:13:54,000
very much on who you talk to. What is vipassen in meditation? It still depends, but much

110
00:13:54,000 --> 00:13:58,720
less. Vipassen in meditation depends much less on who you talk about. Like, take the

111
00:13:58,720 --> 00:14:04,720
goangkah tradition and the Mahasi tradition. These are the two modern vipassen traditions.

112
00:14:04,720 --> 00:14:10,200
They're different. But read the theory behind what they teach. Like, their techniques

113
00:14:10,200 --> 00:14:15,720
are quite different, and the way they go about it is different. But if I read anything

114
00:14:15,720 --> 00:14:20,880
that goangkah wrote, it's quite clearly from the same source, you know, talking about

115
00:14:20,880 --> 00:14:26,720
the same things. What is vipassen? And to see impermanence suffering in oneself, it's going

116
00:14:26,720 --> 00:14:33,000
to be the same from tradition to tradition. I don't think you can, it's fair to say

117
00:14:33,000 --> 00:14:38,440
that about zen, because it's much more nebulous. It's not completely nebulous, and there

118
00:14:38,440 --> 00:14:43,480
is something, it is a valid meditation practice, or something wrong with these other types

119
00:14:43,480 --> 00:14:48,320
of Buddhism. It's just hard to pinpoint them down, and it's not fair to then say, well,

120
00:14:48,320 --> 00:14:55,120
Buddhism in general is quite nebulous, because no within our tradition, it's really not.

121
00:14:55,120 --> 00:15:02,640
It's why, personally, I stick to this tradition. I feel like I've really come across

122
00:15:02,640 --> 00:15:09,360
as down another Buddhist traditions, and, well, it's not really fair of me. I think

123
00:15:09,360 --> 00:15:17,400
to say on public forum, so I apologize for that aspect of what I said. Again, it's

124
00:15:17,400 --> 00:15:23,400
my personal opinion, and I can't avoid that. I'm not too fond of other Buddhist tradition.

125
00:15:23,400 --> 00:15:34,960
Sorry. How do I test myself that I've truly forgiven someone after they have hurt me?

126
00:15:34,960 --> 00:15:45,960
Should I look at it in terms of lack of harsh speech and anger and evil and hatred? I wouldn't

127
00:15:45,960 --> 00:15:50,120
worry about that. I mean, that's a very egotistical question to ask, and I'm not trying

128
00:15:50,120 --> 00:15:55,160
to attack you or insult you, but be clear about what that question, why I say egotistical

129
00:15:55,160 --> 00:16:01,280
is that question is talking about a self, a continuous self, I who have forgiven someone.

130
00:16:01,280 --> 00:16:05,960
What does that even mean? I mean, you could come up with a definition that means that anger

131
00:16:05,960 --> 00:16:10,240
never arises towards that person, but the only way that's going to happen is if you're

132
00:16:10,240 --> 00:16:17,800
an anagami, otherwise anger could always rise towards that person. So rather than trying

133
00:16:17,800 --> 00:16:26,280
to ask that sort of question, you much more be this cliche idea, but it's not cliche,

134
00:16:26,280 --> 00:16:32,040
but overworked concept of staying in the present moment, and it's overworked, but you have

135
00:16:32,040 --> 00:16:41,240
to say it again, be pre-present now. Are you angry at this person now? No, no, good. If

136
00:16:41,240 --> 00:16:48,560
yes, bad, and take things as they come, it would be clear that the only way you could truly

137
00:16:48,560 --> 00:16:54,880
not get angry at someone is to become enlightened. So don't worry too much about whether

138
00:16:54,880 --> 00:17:04,920
you are or just try your best. What's going on here? I'm clicking on, okay, can someone

139
00:17:04,920 --> 00:17:14,480
tell our IT people, when I click on a question, it just disappeared. What happened?

140
00:17:14,480 --> 00:17:23,880
Did someone even see that question? I clicked on answer. No, it's gone. Did someone delete

141
00:17:23,880 --> 00:17:30,600
it? Maybe someone else deleted it? I was just about to answer that one. I don't know,

142
00:17:30,600 --> 00:17:36,360
maybe it wasn't appropriate. The Upanisha suit dimensions that suffering is the supporting

143
00:17:36,360 --> 00:17:43,440
condition for faith. Could you explain that causality? Suffering causes faith. Well,

144
00:17:43,440 --> 00:17:48,360
the Buddha taught that there is suffering. So when people see that, it gives them confidence

145
00:17:48,360 --> 00:17:55,800
in what the Buddha taught. Yeah, life isn't just a bowl of cherries. I can't always get

146
00:17:55,800 --> 00:18:05,840
what I want. I don't know. I don't, offhand I don't remember the Upanisha suit. But I wouldn't,

147
00:18:05,840 --> 00:18:11,880
I wouldn't confuse abidama causality with this sort of teaching. The Buddha is speaking

148
00:18:11,880 --> 00:18:17,640
rhetorically to some extent. You know, he's speaking conventionally. Suffering doesn't

149
00:18:17,640 --> 00:18:22,640
directly lead to faith, but it certainly can. And in a general sense, practically speaking

150
00:18:22,640 --> 00:18:27,240
it does. You hear about the Buddha's teaching you and say, there's no suffering. My life

151
00:18:27,240 --> 00:18:31,040
is perfect. And then you suffer a lot and you're like, Oh, yeah, maybe the Buddha was

152
00:18:31,040 --> 00:18:44,640
right. Maybe I am susceptible to suffering because I have my attachments. Oops. Oh, I know

153
00:18:44,640 --> 00:18:52,360
what I'm doing. Am I hitting the wrong button? Shoot. I just disappeared a question. Oops.

154
00:18:52,360 --> 00:19:00,440
It was something about, I have great anger towards people. No, I see everyone with all

155
00:19:00,440 --> 00:19:06,000
that, with great evil. Every being I see every being is having great evil and I have a

156
00:19:06,000 --> 00:19:11,440
hard time not getting angry. What can I do with something like that? I recommend practicing

157
00:19:11,440 --> 00:19:20,680
meditation because I can't just fix things, but I'll try your best to be mindful.

158
00:19:20,680 --> 00:19:25,720
This belief in Buddha's protection, just psychological help, or involves some divine beings

159
00:19:25,720 --> 00:19:36,640
guiding practitioner on the path. Oh, it can. I mean, there's no law to it. But if you focus

160
00:19:36,640 --> 00:19:43,200
on the Buddha, the angels will be keen on you, you know, they'll be keen to protect you.

161
00:19:43,200 --> 00:19:48,240
If you're a kind of compassionate person, apparently the angels, angels and spiritual beings

162
00:19:48,240 --> 00:19:56,240
are much more positively inclined towards you. I don't have a lot of experience with

163
00:19:56,240 --> 00:20:01,880
that personally. Like, I'm not a sort of person who can see angels, but that's what they

164
00:20:01,880 --> 00:20:12,240
tell me. I'm understanding correctly, right view of the novel 8 full path states to only

165
00:20:12,240 --> 00:20:25,040
base our views on experience. It doesn't really. Right view is a view and views have

166
00:20:25,040 --> 00:20:33,120
various levels. So if you have belief in the four noble truths, that's right view, even

167
00:20:33,120 --> 00:20:42,840
though you don't haven't experienced it. It doesn't actually say only base your views on

168
00:20:42,840 --> 00:20:55,760
experience. But that being said, you know, I've addressed this question because the question

169
00:20:55,760 --> 00:21:00,160
is talking about how can one claim that reincarnation or the existence of spirits such

170
00:21:00,160 --> 00:21:07,000
as Deva are fact, they have not experienced it. One can't. But the belief in such things

171
00:21:07,000 --> 00:21:15,560
is not wrong view. It's just belief, you know, belief as opposed to view. And there is

172
00:21:15,560 --> 00:21:25,400
a difference because I have had people tell me about the existence of angels and those

173
00:21:25,400 --> 00:21:32,920
people seem to be reasonable and wise. And so I believe them. Just like I believe in

174
00:21:32,920 --> 00:21:39,560
Australia. Because I've never been there, but it's a little harder to disbelieve in Australia

175
00:21:39,560 --> 00:21:46,360
and med Australians and they tell me they live there. But then they're not nearly as wise

176
00:21:46,360 --> 00:21:55,720
as these people who tell me about it. Devas, angels and so on. As far as rebirth, reincarnation,

177
00:21:55,720 --> 00:22:01,480
the continuation of the mind after death, that one's a bit different. I think when you meditate

178
00:22:01,480 --> 00:22:09,440
and see things clearly, it's not a hard concept to understand. It's quite clear that the

179
00:22:09,440 --> 00:22:18,640
mind is capable of continuing irrespective of whether the body continues, irrespective

180
00:22:18,640 --> 00:22:24,400
of what happens to the body. And it's altered by the body. It's affected by the body.

181
00:22:24,400 --> 00:22:30,800
It can be crushed by the body in the sense of control if you have like part of the brain

182
00:22:30,800 --> 00:22:37,360
messed up. But once the brain dies, it's not the end of the mind. That's a different one,

183
00:22:37,360 --> 00:22:50,080
I think. But still, there's just evidence or sort of a framework for understanding the

184
00:22:50,080 --> 00:22:56,000
possibility. But we still don't have proof that we're going to be reborn. There's going to be

185
00:22:56,000 --> 00:23:01,600
a continuation of the mind after death. Just like we don't have proof that the mind suddenly stops

186
00:23:01,600 --> 00:23:12,880
when the brain dies. We do actually have some evidence. Third person, no, it's the word impersonal

187
00:23:12,880 --> 00:23:20,800
evidence. People whose brains have died and yet they continue to experience and they're

188
00:23:20,800 --> 00:23:33,920
able to relate experiences, that sort of thing. So if someone tells me there are angels

189
00:23:33,920 --> 00:23:41,280
and I believe them, it's different from me than claiming that I know that there are angels,

190
00:23:41,280 --> 00:24:04,960
or I know that there is rebirth. Anyway, I mean, the point is that

191
00:24:04,960 --> 00:24:13,840
the right view is not simply from experience. But in terms of understanding,

192
00:24:16,080 --> 00:24:28,080
in terms of holding a view, the most useful form of view is direct experience. Like my belief

193
00:24:28,080 --> 00:24:36,640
in angels is far less powerful or meaningful than knowledge. If I had seen an angel,

194
00:24:36,640 --> 00:24:41,520
it's much more meaningful because then I could say, hey, I got a friend who is an angel and he lives

195
00:24:41,520 --> 00:24:47,200
in this tree up back and I talked to him every day or her every day. That would be much more meaningful

196
00:24:48,000 --> 00:24:53,760
for my view and it would support my view. But it doesn't mean that my view has to be.

197
00:24:53,760 --> 00:24:59,360
It doesn't mean that my view, right, as it stands, not having an angel in the tree out back,

198
00:25:00,240 --> 00:25:07,680
is wrong view or is not right view. But that being said, when you're thinking of is

199
00:25:08,560 --> 00:25:13,280
in order for right view in terms of knowledge of the four noble truths to lead to nibana,

200
00:25:13,280 --> 00:25:22,080
for it to be perfect right view. It's necessary for that view to be empirical and personal

201
00:25:22,080 --> 00:25:27,600
experiential. You have to actually experience the four noble truths in order to become enlightened.

202
00:25:27,600 --> 00:25:33,920
So for it to be noble right view, it has to be empirical. It doesn't mean that it doesn't

203
00:25:33,920 --> 00:25:38,960
have anything to do with it being wrong to believe things that you don't have empirical evidence

204
00:25:38,960 --> 00:25:48,560
about. I may have kind of made that sort of been vague before about talking about that about

205
00:25:48,560 --> 00:25:56,320
all kinds of views that aren't empirical are not right view. But it's nuanced because there's

206
00:25:56,320 --> 00:25:59,760
so many things that we believe and they don't get in the way of our practice. We believe them

207
00:25:59,760 --> 00:26:14,400
for practical reasons. I believe that my bedrooms still exists. I believe that. I believe that

208
00:26:14,400 --> 00:26:21,360
anyway. So many things that we hold belief only that we even though we don't have knowledge.

209
00:26:25,680 --> 00:26:32,000
His loneliness the same as boredom and are they both caused by kamu padana wanting to please

210
00:26:32,000 --> 00:26:38,000
the senses? I don't know where he would get that idea. Are you thinking abidama-based?

211
00:26:38,000 --> 00:26:49,200
Loneliness is a, it depends. It could be a desire for companion jib. Desire for certain experiences

212
00:26:49,200 --> 00:26:58,640
or the concept of being associated with other people. Or it can be a virgin in the sense of sadness

213
00:26:58,640 --> 00:27:06,080
at not having those things. It's probably both of them in alteration, alternation. You want them

214
00:27:06,080 --> 00:27:08,880
and you feel sad because you don't have them. That sort of thing.

215
00:27:09,600 --> 00:27:19,360
Boredom is quite different. Boredom is an aversion to what you're doing. Or it's a desire for

216
00:27:21,360 --> 00:27:26,560
it's not a desire. It's usually based on a desire for something more exciting. You think of

217
00:27:26,560 --> 00:27:31,120
something that you want to be doing or something that excites you and you're not getting that. And

218
00:27:31,120 --> 00:27:38,320
so you feel bored. They're quite different. Both caused by kamu padana. They both can be. I don't

219
00:27:38,320 --> 00:27:43,920
think that they always are. They can be caused by bavatana. We bavatana as well, I think.

220
00:27:46,000 --> 00:27:52,160
But yeah, usually the senses, it's common. Did the Buddha stress more on the three marks of

221
00:27:52,160 --> 00:27:57,520
existence than the five precepts? I don't know what kind of a question that is. I mean,

222
00:27:57,520 --> 00:28:03,760
what did the Buddha stress on most? It's hard to say. I mean, Celesamadipanya. The five precepts

223
00:28:03,760 --> 00:28:09,440
aren't all that important because they're conventional. But Cela, which is the basis of the five

224
00:28:09,440 --> 00:28:18,160
precepts is essential. The three characteristics are Panya. So you need Celesamadipanya in these three

225
00:28:18,160 --> 00:28:30,000
and requires both of them. The five precepts themselves are just a sort of the boundary. If you're

226
00:28:30,000 --> 00:28:33,840
breaking the five precepts, it's very hard to see impermanence suffering on some.

227
00:28:38,160 --> 00:28:42,480
When you say to watch the rising and falling of the abdomen, yes, can it speak clear?

228
00:28:42,480 --> 00:28:49,920
The rising and falling of the abdomen is an English expression. And if you're not a native English

229
00:28:49,920 --> 00:28:57,120
speaker, it's hard to understand. And I realize this problem. They don't use rising and falling

230
00:28:57,120 --> 00:29:05,920
in most other languages. Panga, you know, that's Thai. Panga means to expand or to get full of

231
00:29:05,920 --> 00:29:13,200
going to expand really. And you know, it means to to fall or to contract. It's what I'll fall.

232
00:29:14,320 --> 00:29:19,280
It means to, it can be translated as fall, but it means to collapse. That's the word.

233
00:29:19,280 --> 00:29:26,240
It means to collapse. So it's not referring to a rising and a falling in terms of up and down,

234
00:29:26,240 --> 00:29:31,120
as if you translate it literally. But if you're an English natural speaker, you know that it

235
00:29:31,120 --> 00:29:36,480
rising is the expansion of the stomach and falling is the contraction. But that sort of means

236
00:29:37,280 --> 00:29:45,920
it means the outward movement. So it's the the latter. It's not the rising and falling of the mid-drift

237
00:29:45,920 --> 00:29:57,280
in a literal sense. It just means expansion in the contraction. I think it's because when you're

238
00:29:57,280 --> 00:30:08,320
lying in your back, of course, it does rise and fall. In terms of the afterlife, is there an

239
00:30:08,320 --> 00:30:12,720
advantage to focusing on meditation during a lifetime over focusing on devotional singing,

240
00:30:12,720 --> 00:30:21,120
chanting sacred phrases and prayer as an incredible advantage over that meditation purifies the mind,

241
00:30:21,120 --> 00:30:31,840
and singing, chanting. It's a different kind of purification, but they can be purifying,

242
00:30:32,560 --> 00:30:39,200
but they're artificial. My meditation is more more beneficial as first of all, it's because more

243
00:30:39,200 --> 00:30:44,800
pure. It's more, I mean, it's more direct in the sense of directly addressing the problem.

244
00:30:44,800 --> 00:30:50,880
But also because it can, if practice correctly, deal in wisdom. And wisdom is special

245
00:30:50,880 --> 00:30:59,680
because it's the core for future arising of wholesome states. If you actively and artificially

246
00:30:59,680 --> 00:31:05,040
try to be wholesome, it's a one-off thing. But if you learn the difference between wholesome

247
00:31:05,040 --> 00:31:09,520
and unwholesome, then you're much more than which is wisdom, then you're much more inclined in

248
00:31:09,520 --> 00:31:16,160
the future to be wholesome. So it's powerful in a whole other level as giving you the impetus to

249
00:31:16,160 --> 00:31:24,400
be more wholesome in the future. Sometimes you speak about magical powers like flying,

250
00:31:24,400 --> 00:31:27,760
mind-reading, do you truly believe these things to be possible? Yes, I do.

251
00:31:29,680 --> 00:31:35,200
I'm not just referring to stories. I do believe them possible. I believe lots and lots of things

252
00:31:35,200 --> 00:31:42,240
are possible. So if that's hard for you to swallow, I apologize. I do understand that it's not

253
00:31:42,240 --> 00:31:46,880
useful for ending sufferings, though. So there we go. Don't worry about it too much. All this stuff that

254
00:31:46,880 --> 00:31:55,920
you find hard to swallow in Buddhism may not be quite fair for me to say it, but regardless of whether

255
00:31:55,920 --> 00:32:02,960
it's fair or not, it would do you good to just put that aside and not worry so much about it.

256
00:32:02,960 --> 00:32:10,560
And we're so quick to jump on these things. I mean, which would be fine if we knew as much as we

257
00:32:10,560 --> 00:32:16,560
think we do. But Western science can be quite arrogant in terms of deciding what is real and what

258
00:32:16,560 --> 00:32:27,200
is not real. It's just gotten so tunnel vision centered on the physical that it's become arrogant

259
00:32:27,200 --> 00:32:33,600
in terms of disregarding the potential of the mind. That might sound arrogant in itself, but

260
00:32:33,600 --> 00:32:40,400
I'll stand by it. I mean, I don't have all the answers. The problem is that we don't have

261
00:32:42,160 --> 00:32:49,200
the same sort of in modern times anyway. We don't have the same sort of evidence to provide

262
00:32:50,240 --> 00:32:54,960
that one would, that one is accustomed to, right? You're accustomed to so much evidence from a

263
00:32:54,960 --> 00:32:59,360
physical point of view. From a mental point of view, it's harder to get that evidence. You can't

264
00:32:59,360 --> 00:33:07,520
get it in the same way because you can't easily show it to other people. Not the way you can show

265
00:33:07,520 --> 00:33:14,800
the physical. How can an anangami still have ignorance if belief in self and doubt in the buddhar

266
00:33:14,800 --> 00:33:23,760
gone? Because ignorance is more primary. I mean, you have to understand ignorance means there's

267
00:33:23,760 --> 00:33:29,520
still things that they don't know, they don't understand. And it's more like they don't understand

268
00:33:29,520 --> 00:33:35,040
clearly. I mean, the ignorance of an anangami is so small. It just means that they're still not

269
00:33:35,040 --> 00:33:41,200
perfectly clear in their mind, but it's minuscule. It's not something that's

270
00:33:41,200 --> 00:33:55,440
problematic. Can effect exist without the cause? No, by definition, no. I think by very

271
00:33:55,440 --> 00:34:00,720
definition, it's impossible. I mean, this is just a, I don't know, there's a particularly

272
00:34:00,720 --> 00:34:07,360
Buddhist question, but if you call something an effect, by definition, I think it requires a cause.

273
00:34:07,360 --> 00:34:16,480
What are your thoughts on marijuana? I was almost going to talk about this for my current affairs

274
00:34:16,480 --> 00:34:23,520
or current events. What are your thoughts on marijuana use to relax? I think it's probably

275
00:34:23,520 --> 00:34:29,600
very problematic. I don't know really, but marijuana use was probably one of the dumbest things

276
00:34:29,600 --> 00:34:36,240
I ever did. Dumbest, I don't mean the sense that I was dumb doing it, but it was just a dumb

277
00:34:36,240 --> 00:34:41,200
thing. I mean, there was nothing ever, I never got off on using marijuana. I mean, it never occurred

278
00:34:41,200 --> 00:34:52,000
to me while this is a useful thing for me to be doing. So for relaxation and enjoying the psychoactive

279
00:34:52,000 --> 00:34:57,840
effects, I think that's highly problematic because it's avoiding your problems. It's the opposite

280
00:34:57,840 --> 00:35:04,320
of what we're trying to do in meditation and the lack of clarity that comes from it is the opposite

281
00:35:04,320 --> 00:35:13,360
of what we're trying to do, the sort of stone state. It's not not cool. It's just like anything

282
00:35:13,360 --> 00:35:18,000
else. I mean, it's something especially wicked about marijuana use. It's just anything that

283
00:35:18,000 --> 00:35:23,600
distracts you. And these things are worse because they're directly deal with the brain. So psychoactive

284
00:35:23,600 --> 00:35:33,520
drugs of any kind. I mean, compare marijuana with, you know, Ritalin or I don't know any of those SSR

285
00:35:33,520 --> 00:35:41,440
eyes. Marijuana is probably world's better. I mean, definitely is, right? It's a whole, which kind

286
00:35:41,440 --> 00:35:46,400
of, you know, makes you wonder if instead of all these crappy drugs, people were to just smoke

287
00:35:46,400 --> 00:35:50,720
marijuana, the world were probably, you know, we'd probably mean a lot less trouble than we are

288
00:35:50,720 --> 00:35:56,240
now in the Western world. I don't know, the modern world may be in general by taking all these

289
00:35:56,240 --> 00:36:05,360
terrible SSRI drugs. So I think it's world's better than that stuff. But it's still probably

290
00:36:05,360 --> 00:36:13,440
problematic because you're cultivating these states of addiction to, and it's addiction. It's an

291
00:36:13,440 --> 00:36:20,960
addiction to, um, to relaxation. It's a psychological addiction. It makes it harder for you to deal

292
00:36:20,960 --> 00:36:31,840
with stress. There's a term of concept I read again and again early Buddhist. What is the difference

293
00:36:31,840 --> 00:36:37,680
between terror about an early Buddhism? Yeah, no, Viratira, don't bring him up. I don't know too much

294
00:36:37,680 --> 00:36:42,880
about him, but he's highly controversial. So yeah, he rejected the Abidama because he not accepted

295
00:36:43,760 --> 00:36:49,920
as a later newer script. How do you see that? I think the Abidama is great. I don't think anyone

296
00:36:49,920 --> 00:36:56,080
really, well, I don't think the Abidama that we have it is actually was actually taught by the Buddha,

297
00:36:56,800 --> 00:37:03,440
but I do believe that it was passed on by Sariputa and his followers. So how much later it is,

298
00:37:04,480 --> 00:37:10,000
I don't know. But, I mean, Sariputa got it directly from the Buddha and Sariputa put together the

299
00:37:10,000 --> 00:37:17,360
Abidama Pitika, except for book five, which is, is much later. But early Buddhism, terror

300
00:37:17,360 --> 00:37:22,480
about a Buddhism is about as early as it gets. There are other texts that are probably

301
00:37:23,360 --> 00:37:31,680
contemporaneous, contemporary, but terror about a Buddhism. You don't get much older than that,

302
00:37:34,160 --> 00:37:42,880
not that we have what we have left. We have extant. Do you have any recommendations for people who

303
00:37:42,880 --> 00:37:47,120
actively meditate and follow the teachings in the Buddha, but don't have access to a community

304
00:37:47,120 --> 00:37:52,000
other than the internet to practice with? Well, you could do an online course with me. That's

305
00:37:52,000 --> 00:37:58,000
about all I got. If you want to do an online course, we have online meditation courses here.

306
00:37:58,000 --> 00:38:02,880
I mean, I've been asked this question many times and I've tried my best to help out with that,

307
00:38:02,880 --> 00:38:07,680
but you know, I don't have a solution for that. I'll move to a place where there is one.

308
00:38:07,680 --> 00:38:14,960
Really, it's a good idea, honestly. With a blind man, a deaf man, without a tongue in a sense

309
00:38:14,960 --> 00:38:23,440
of touch, be born. I don't understand that question. I don't think it's somewhat speculative,

310
00:38:23,440 --> 00:38:27,600
not going to answer it. Sorry. That's all the questions. That was good.

311
00:38:28,960 --> 00:38:34,640
Are there comments today? I'm not going to read them. I'll go through quickly. Good evening,

312
00:38:34,640 --> 00:38:46,320
everyone. Good evening, good evening. Something about sexist. I think you're the most pretty

313
00:38:46,320 --> 00:38:52,160
and beautiful. I don't want to read these. YouTube, I think they're behaving, which is good.

314
00:38:55,200 --> 00:39:00,080
Second life, we've got a small audience tonight. Well, thank you all for coming out.

315
00:39:00,080 --> 00:39:10,080
That's all for tonight. Have a good night.

