1
00:00:00,000 --> 00:00:03,600
Hello, and welcome back to Ask A Month.

2
00:00:03,600 --> 00:00:08,040
Today's question comes from Tulu Flux.

3
00:00:08,040 --> 00:00:12,720
Monday, when I get distracted, I tell myself wandering.

4
00:00:12,720 --> 00:00:20,080
But when I do, I find myself in a bubble, a sort of meta view of myself meditating.

5
00:00:20,080 --> 00:00:25,040
And soon, I find it very difficult to go back wholeheartedly to rising, falling.

6
00:00:25,040 --> 00:00:27,040
How do I remedy this?

7
00:00:27,040 --> 00:00:47,040
The most difficult thing in the meditation, or one of the most difficult things, is really catching the essence of the experience that you're having.

8
00:00:47,040 --> 00:00:56,480
When you're wandering, and you say to yourself, wondering, wondering, there's probably several things going on,

9
00:00:56,480 --> 00:01:06,120
and several emotions in your mind, several associated mind states that are arising in sequence.

10
00:01:06,120 --> 00:01:19,080
So you'll be wandering, but then there's an enjoyment of the thought, or a worry, or a fear, or a stress, or whatever the emotion is, as well.

11
00:01:19,080 --> 00:01:23,040
And this is contributing to our state of distraction.

12
00:01:23,040 --> 00:01:28,680
The fact that you can't go back to the rising and falling says that there's something else going on.

13
00:01:28,680 --> 00:01:34,120
There's something happening that you're not noticing that you're not really clearly aware of.

14
00:01:34,120 --> 00:01:39,840
And the most important thing in the meditation is to ask yourself, what's happening right now.

15
00:01:39,840 --> 00:01:42,320
Not to say, I have to go back to the rising and falling.

16
00:01:42,320 --> 00:01:50,160
I have to try to go back to my meditation practice and think, this is distracting me from my meditation.

17
00:01:50,160 --> 00:01:58,680
This is pulling me away from my meditation and blot something out and try to ignore it, because that's what's going to make it difficult to watch the rising and falling.

18
00:01:58,680 --> 00:02:13,360
It's much more important that you watch what's really taking your attention away, focus on that, and do away with it before you even think about going back to the rising and falling.

19
00:02:13,360 --> 00:02:32,200
So when you say wandering, then when it's gone and you come back to the rising and falling, but when something else comes, when you feel the emotion or you feel stress or you feel bored or whatever, you have to focus on that.

20
00:02:32,200 --> 00:02:59,200
Even to the point where if it's difficult to watch the rising and the falling and you feel detached or you feel out of your body or unclear, just focus on that state as well, unclear or distracted or cloudy or whatever it feels to you.

21
00:02:59,200 --> 00:03:12,200
In the end, one of the catch-all phrases that you can use is just feeling, feeling, when you feel something, you feel a strange sensation, just remind yourself that it's only a feeling and it will go away.

22
00:03:12,200 --> 00:03:26,720
And in the end, the word in that sense isn't so important, even feeling can capture so many different things, but what it's doing is focusing you on the feeling on the experience as it's happening.

23
00:03:26,720 --> 00:03:41,720
So I hope that helps. Try to not focus so much of your attention on the rising and falling, but focus on whatever comes when there's nothing else to focus on, just then go back to the rising and falling.

24
00:03:41,720 --> 00:04:05,720
Okay, thanks for the question. Keep them coming.

