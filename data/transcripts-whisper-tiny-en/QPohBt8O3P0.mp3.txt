Hello, welcome everyone to our weekly
Dhamma session. My name is Yuta Dhamma Mowbiko. I'm here to share the Dhamma and I'm joined
today by Chris who will be asking questions on your behalf and by Jim and maybe even
Ulu, so Ulu seems to be having technical difficulties, they will be helping to sort and organize
questions. Chris is also our slide master, so he will be responsible for the display that
you see. So thank you to all of them for their help. Thank you to everyone for coming
out to listen. We don't use video for this session purposefully because the idea is not
to see me or see something. The idea behind these sessions is to cultivate the practice
while we listen, so he should close your eyes and focus on your own experience, try and
see clearly what arises and ceases in the present moment. Again, a better perspective on
your own reality. If you have questions post them in the chat, you can start posting them
already now. I'll spend a little time talking and once I'm done talking, then all the
questions will be organized and displayed, asked and answered.
So the topic of my talk today is Heartwood. This morning, we had a weekly Dhamma study
session, which is less about practice and more about theory. We read through a sutta and
we talked a little bit about it. And today sutta was called the Mahasarupamasuta,
a greater discourse on Heartwood, on the simile of the Heartwood. So the Buddha provides
simile that basically that Buddhism is like a big tree, magical tree. Our spirituality
is like a forest, you can go into the forest looking for something of value, looking
to take something away from the forest, looking to get something out of it, the practice
meditation, it's quite common to focus very much on the results. What are we going to get
out of it? Which is hard because you can't simultaneously focus on results and focus on
the practice, common from meditators to become distracted by thoughts of results.
So when we practice, it's important not to obsess over results, but it is good to have
some idea of what you might get out of the practice of the spiritual path, but you get
out of Buddhism. So that when you practice, you can focus on the practice and be reassured,
that the benefits are worth the cost. So the first benefit that comes from spirituality
and Buddhism more specifically is material and worldly gain. Now this is true for monks.
Someone becomes a Buddhist monk, they gain some status in Buddhist society and they receive
support from the Buddhist community. Now obviously this is not a very spiritual reason
for becoming Buddhist or becoming a Buddhist monk, but it bears mentioning. It bears
mentioning because there is some depth to gain the material benefit. In the sense that
someone who is on the right path will tend to prosper materially. It's not always as obvious
as when one ordains as a monk and gains support from the community, but more profoundly
when a person undertakes the practice becomes a better person. Their relationship with
the world around them, with other people, with their society can only improve their status
and their reputation. And all of this reverberates into their mundane, non-spiritual life.
This is why many non-spiritual people approach religion as a bank of sorts, where they
support religious institutions and expect some return on their investment. And often
do gain return on their investment. Certainly have not in this life, the promise that many
religious traditions, including Buddhism, give, is a better afterlife. Now Buddhism I think
has a better chance of providing that sort of promise, following through with that sort
of promise, because of the focus on truly important things like the cultivation of
good deeds and good thoughts, good inclinations, rather than beliefs and views and doctrines.
So when a person is charitable, when a person is ethical and so on, the benefits come not
from some spiritual entity, but they come from one's own goodness.
So there's more of a down-to-earth. Well, it's actually rooted in reality. So the benefits
are actually rooted in truth and they're actually real.
So I think I've muted there. What was the last thing you heard? Where did I get cut off,
rooted in reality? But so the benefits of spiritual practice are the benefits of the
worldly benefits of spiritual practice are very real. But they're certainly not the most compelling
reason to practice spirituality. They're not really a great benefit. They would have likened
to a person, a person who engages with in-spirituality for this reason, whether they be a
monk, a meditator, or an ordinary everyday person who takes up Buddhism as a religion
and then becomes content with the worldly benefits, the possible material benefits, even
up to including heaven. A person who becomes content with attaining and obtaining a heavenly
rebirth. But I said, this is like a person who goes into the forest looking for
some material. I want to build a house or a building or a piece of furniture or something.
And they come out with a handful, an armful, an arm load full of twigs and leaves. And
anybody who knew anything about building would be able to tell you that this person
isn't going to be able to build much with that. They got something out of it, that's
for sure, but it's not really that beneficial. There's nothing of any real benefit there
because heaven, of course, is impermanent, not to speak of any other material benefit that
might come. It's actually dangerous, just like if you try to build a house with sticks
and leaves, bad things can surely happen. The same way if you're dependent and complacent,
relying on material wealth, material gain, there will be certainly left unprepared for
the loss, the degradation or the dissolution of such support. And yet, it's necessary
to rely on these sorts of support where we go further. The monk relies on these things,
but so do ordinary people. If you don't have this kind of support of a community, anyone
who wants to undertake spirituality needs a mutual reliance on each other, on the community,
on relations, relatives, friends, associates, employers, employees to maintain their livelihood
to keep themselves going. But this isn't the heart would. So the second positive outcome
of spiritual practice is ethics. Ethics is the base. I'm going to identify this as the foundation
of religiosity, spirituality, spirituality must be grounded in this. And for some ethics,
morality is enough. It's the essence of their spiritual practice. Outside of Buddhism, there
are religions that focus on ethics and Judaism focuses very much on ethical precepts. Do
this, don't do that. Most religions have their edicts, their precepts, their commandment,
what you must do, what you must believe, what you can't believe.
Buddhism has these as well, the 5% and some important beliefs or views. Your views must
be to some extent in line with what we claim to be reality. It must be very hard for you
to progress. But it's certainly not enough. It's not the heart would. So for someone
who takes up ethics and thinks of it as the ultimate, this is like a person who comes out
of the forest with a handful of bark. Anyone can see it's not going to be a much use. You
can build some things out of bark, but not anything of any great endurance, not anything
of any great strength or durability. The third thing that comes from practice, from practice
or spirituality, is a depth of concentration and focus, a peace of mind, a stillness, a
appearance, a cohesion in the mind, where the mind becomes very strong, very still, very
pure. And this starts to get into something that's a fairly substantial benefit, something
that is realizable here and now, something that has depth and profundity to it. And for
many is the ultimate goal of their spiritual practice, the state of tranquility of absorption.
So many Buddhists think to be the sambhambhana-manbuddhism or experience to be that. Many
or the other religious traditions, it's considered to be communion with some kind of spiritual,
some kind of divine entity or divinity, some sort of ultimate reality. Meditation practice,
the mind can become quite still, quite absorbed. But the Buddha called this just like
more bark. I said it's a more refined sort of bark, just like some trees have an outer
bark and an inner bark. It's just like just the inner bark. He still said this was not
very valuable. Anyone who knows anything about building, about carpentry, about woodworking
can tell you that bark isn't going to get you very far. So even this seemingly profound
state of tranquility is not, not inefficient, but it's important. Ethics just as ethics is
the foundation of the spiritual practice, tranquility, stillness, strength of mind, cohesion
of mind, all these qualities that come from meditation, practice, these are the pillars
with which we build our, our host or the pillars there. Whatever it is that you build
after you've got the foundation, the frame, the supports. But it's a support. We use it
for what? We use it for something further. Buddha said the next great thing that comes
from spirituality is called nyana-dasana. Nyana means knowledge, dasana means vision. There
are many kinds of knowledge and vision that come from tranquility that come from peace
of mind, from depth of mind, from strength of mind, from intents and profound spiritual
practice, like mindfulness. Now in some traditions, even in Buddhism, there are mundane,
but profound spiritual attainments, like clear audience, clear audience. Some people remember
things far in the past. Some are apparently able to see the future. Some people are able
to read other people's minds, understand their other people's emotions. What are they
feeling? What are they thinking? Lots of interesting things that people claim that are
hard to believe if you've never had this depth of spiritual focus, the spiritual strength
as they seem to break the laws of physics. They have nothing to do with course with
physics because they're mental, but they seem unreal to those who have never had that
depth of mental fortitude. But in Buddhism, of course, the focus is elsewhere. It's not
on these magical powers. It's on the great and magical power of seeing clearly, of seeing
our own experience clearly, of understanding how the mind works, gaining a depth of understanding
about our own mind and the workings of our mind, the causes of addiction, the causes of suffering,
the causes of stress and anxiety and so on. And this, the Buddha said, is, like, real
would, real would. Any of these knowledge is the real result, something that you can tangibly
say, this is some tangible benefit, something that is unlike anything you would find outside
of spirituality. There's no pill that you can take. There's no possession. You can obtain
that's going to replace or equal this sort of benefit. So this is like, softwood.
It's like you've gone into the forest and you come up with softwood and someone could
say, well, you could build something with that. But it's not going to be that stable or
strong. Probably not going to make a very good construction. It will be weak. Subject
to dissolution, termites can eat that sort of wood. It will rot quite quickly. The final
benefit of spirituality is, is the consummation of mental training, which includes tranquility,
which relies on ethics, relies even on material well-being and grows out of knowledge and
vision. It comes as a result of knowledge and vision. Knowledge and vision is not really
the heart would of the tree. And anytime someone can become complacent in their knowledge,
quite often a meditator will practice, after practicing a little bit of meditation, become
convinced of the truth of the Buddha's teaching. And mistake that conviction for actual
realization and the conviction of it being true seems material seems real enough, substantial
enough to be considered knowledge and such a person thinks that they have come to know
the Buddha's teaching and becomes complacent. It certainly happens. But the true knowledge
in Buddhism is such strength and potency that upon gaining it, it leans to the cessation
of suffering. It leans immediately to the letting go, not just some kind of theoretical
or abstract letting go, but a very visceral cessation of suffering where there is the cessation
of all, some sorry experience of any arising of anything, even memory or perception is a complete
piece, complete stealing of mental formations. And in that piece is that experience or that
experience, that realization, that attainment, that cessation, that's the heart would. And
there's something that is of great and lasting benefit. I'm going to call this a samayya
we would say as samayya we would say as samayya means occasional as samayya, not occasional.
It's not ephemeral, it's not unstable. This is something that is stable. When the mind has
let go in this way, there's a true change through wisdom, through that very realization.
Once perspective on things, changes, one is gained some greater perspective,
and everything will be seen through this new light. Like a person who goes into the forest,
it comes out with an arm load or a big truck load of hardwood, and they can build whatever
they want with it, and build a refuge that is truly secure, truly strong and withstands the
vicissitudes of life, a person who has realized this truth, this freedom, liberation of mind
will be unaffected by the elements, by loss or pain, blame,
be unaffected by the good things in life, but also unaffected by the bad things. Their happiness
will not depend on their experience, their situation, they will never be tossed around by things,
because they've seen beyond it, their mind is beyond that. They've teamed to the goal, the
core of the spiritual life. So Buddhism has all these things.
This is the progression of practice that the Buddha talked about
in the Mahasana or Palmasuta. I think I've talked long enough, that's some food for thought,
food for the heart. Now, if there are any questions, we'll move on to our question and answer
period I'm happy to answer the questions that people have already asked. If you haven't
go ahead and place it in the place you're questioning in the text box, we're focusing on questions
that are of high importance to the people asking them. So it has to be a question that relates
to your own spiritual development, something you really need an answer to. So questions that are
based on curiosity or theory, we'll get a lower rating of importance.
Our team has organized the questions. We'll start asking them now.
Okay, let's begin.
How do you overcome the voice of self doubt and how do you teach yourself to let go of regret of
past mistakes and the anxiety of an uncertain future?
Well, as with all things relating to the mind, we focus on the understanding of them.
Now, our focus in relation to these things is quite often on fixing them, solving the problem.
And as I said, that's about focusing on results that's kind of to our detriment because during
that time that we're focused on results, we're not actually focused on any practice that might
bring about those results. And so it's always going to be a two step process. You can never
focus on the thing that you want to happen because you have to bring about that thing through
some other practice, through some practice. And what is it that's going to bring about the
desired result? Which is the basis of your question. The first answer is to focus on
not what you want to happen, but on what will make it happen. Meaning, what is the answer?
The answer is to undertake a practice separate from the intention for those things to be fixed
and ameliorated. That practice, while we would say, is mindfulness, the cultivation of a
clarity of understanding of those things. So for example, with self doubt, the way forward is
to understand it, not to try and overcome it, not to try and be free from it. When you have self
doubt, try and learn what it is to self, to doubt yourself, rather than trying to reassure yourself
for or fix it, you see. Focus on the focus on the cure. Focus on the actual practice that will
cure with self doubt. Once you see clearly, once you see even self doubt, clearly, of course,
there's far less doubt. Doubt is a funny one in that way because you might doubt the practice,
but the practice is actually, if you undertake the practice, will free your mind from doubt
directly by its very nature. It's an intrinsic aspect of mindfulness and clarity of mind that,
of course, it does away without. And as for regret and anxiety, as well, they can't survive
a mind that is focused on clarity and objectivity, because they are reactions, regret and anxiety
are not objective. They are not a consequence of seeing things as they are. They are a consequence
of imperfect skewed perceptions of things, skewed based mostly on things like identification,
partiality, none of which has anything to do with the reality. So, when you focus on things
as they are, your memories, for example, your perceptions about the future,
conceptions and plans for the future. When you focus on these things, see them just as they are,
there's no room for anxiety or regret. So, if you haven't read our booklet on how to meditate,
that might be a good place to start. If you're interested, you could take a course. We have these
at home courses. It's all free and find more information on our website.
Should I give in to unfocused sleepiness when it arises?
Well, there's no real hard and fast rule. It might happen that you do give into it.
It's not something you should feel really bad about. Sometimes you might not offer.
Sometimes if it's really strong, you might lie down and go to sleep and start.
Start up again when you wake up. But that's sort of a last resort. Something you shouldn't
discard out of hand. You should be prepared to have to succumb to it.
But certainly it shouldn't be your first resort. You should try your best to be mindful
once first resort should be to take it as an object of attention. When you feel drowsy or tired,
focus on it, say in yourself, tired, tired drowsy, try and just see it as it is. If that doesn't work,
there are many more extreme methods, extreme measures you can take. You can open your eyes,
turn on the light. You can stand up to walking meditation, standing meditation.
You can do some chanting. The Buddha recommended that sort of thing.
Just like when people are driving at night, they might start singing to the radio to help
them stay awake. It actually is quite effective. We don't encourage singing, but chanting.
If you learn some Buddhist chants, you can do some chanting. Stimulates the mind.
But ultimately, if none of that really works, splash some water on your face or whatever,
then you lie down and go to sleep and start again when you wake up.
I can sense a deep unhappiness inside of me. How do I deal with this?
As with the first question, it's something that you try to understand
and really try to move away from the idea of fixing it.
Because most importantly, most obviously, life is uncertain, and there will be
from time to time unhappiness on your practice until you become completely enlightened.
There will always be some sort of unhappiness.
Meaning it's something you have to be prepared for. It's something you have to understand
to be a part of your spiritual journey, from time to time there'll be unhappiness. It's not going to
be constant. Sometimes it's easy to lose sight of the better times. When unhappiness comes,
it can lead to great doubt as you think that perhaps the practice is not beneficial because it's
not a linear progression. Unhappiness comes back. Unhappiness like many things, even
unwholesome, unfruitful mind states, things like anxiety, depression, these can all come back
from time to time. Our practice is not going to be linear. It's quite often cyclical. We find ourselves
right back where we started from. The only thing that changes is our perspective.
We find that the reactions we can in our clarity and our purity of mind strengthens.
But suffering will always come back unhappiness. We're still going to be unhappy from time to time.
What changes much more rapidly than unhappiness to happiness is
our susceptibility to it, our reaction to it. We might feel unhappy but not be concerned
by it. We won't be upset or worried or consumed by it. We won't cling to it
because we start to understand that this is the result of our inclinations of mind. We
have this susceptibility to suffer. We become more patient with our pain, more patient with our
pleasure as well. Even in a light and being doesn't always feel happy. All they lose is
their susceptibility to pain and so they do never feel unhappy. But more importantly, they don't
cling to anything and that's why they don't feel unhappy. They've gone through the unhappiness
without reacting to it, without feeding it. So it wastes away.
They've gained a strength and a peace and a clarity of mind and is above it.
So don't be too concerned. It's not really the first step to free ourselves from unhappiness.
First step is to see things like unhappiness as they are and be above them.
It's a sense to be happy even when you're unhappy or to be at peace even when you're unhappy,
understanding that you may not always be happy all the time. Your practice is going to be a bumpy road
but with a proper perspective, you gain a vehicle that allows you to
coast through the unhappy time.
Is anxiety only a hindrance if it is accompanied by disliking?
Anxiety is a hindrance in and of itself. So hindrance, the word hindrance, you understand,
relates to seeing clearly. So anything that is a hindrance to seeing clearly
for that reason, it's called a hindrance. It's not because it makes you unhappy just
though that's related to it. The essence is that it prevents you from seeing clearly.
So anxiety will certainly do that. It may more importantly, anxiety is stressful.
It prevents you from being at peace. It gets in the way of happiness,
it's in the way of clarity, it gets in the way of understanding. I find myself calm in walking
meditation quite often and I note it. It becomes difficult to notice the dissolution of the calming
feeling since walking meditation is a naturally serene practice. Do you have advice on noting
this dissolution? We know what's present. At some period of your practice, you're going to be
more focused on the dissolution. At some points, you're going to be more focused on the arising.
None of it is more essential than the other. Just focus on however you experience it.
With things like calm, if it's something that lasts, you don't have to wait for it to go away,
it's again benefit. We're just trying to gain a better perspective on the calm. For example,
someone who experiences calm might become very much attached to it, liking it, enjoying it. Because
of that, they'll become attached to it. There will arise and persist the sort of clinging to it
that insists upon experiencing it and is dissatisfied when it doesn't arise.
So it's important to note calm and try to just stay with it until it goes away, but
if after sometime it doesn't go away, you can resume the walking and just ignore it.
If you like it, of course, focus on the liking and say liking, liking.
I once asked you about ADHD, and the answer was helpful, but I am still struggling with it.
Is there any way I can look at it and face it until it disappears from my life?
Trying not to focus too much on the disappearing from my life part, struggle with it.
Struggle is a sign of growth. Again, this is sort of the essence of what I was saying about
understanding that it's going to be a part of your practice.
Be patient with it. Life is not a spirituality, it's not a straight and
constantly upward path, it's bumpy road. I don't think of the ADHD disappearing, but
think of it as a struggle worth mastering to the point where you're no longer struggling,
and that's of course where the ADHD goes away. When you're able to face it, experience it without
reacting, that's where the goal is attained.
What should I change about my practice if after a session I find myself unable to make
conversation with others, I feel stuck. That I think just takes time. Be patient with yourself.
Mindfulness is in many cases. For many of us it's a radical departure from our ordinary
perspective on things, and so in the beginning it can be difficult to reconcile our
old ways and patterns with our new perspective. I would say don't be too
dogmatic about your understanding about things be flexible. Flexibility is something you'll gain
with the practice and welcome that. Don't shy away from the flexibility that comes. Don't
be too caught up in formal mindful practice that you are unable to relate to other people,
because that's only going to be a detriment to your practice as you're stressing about that, worrying
about your relations to others. Try to slowly slowly incorporate the two together where you're
able to be mindful in your relationships with others, but don't be rigid in your rejection of
worldly things. But be patient with yourself. The biggest thing is that it will come in time. Let it
come and be patient with yourself.
Living in a non but a society, there are numerous people with wrong views in my life.
I never pointed out, but I see judgment arising in the mind because I study the dhamma. Is it
correct to note, conceit? It's hard to put too much focus on it because during the time that you're
conceited, for example, they're not really mindful during that time, so once you realize it,
yes, you can note that. But more importantly, through your practice, that will slowly fade away.
It's one of those things that you're going to start to see about yourself that you perhaps
you most likely didn't see clearly before. So it's not exactly a product of the practice.
It's of one of the habits of the mind that we aim to smooth it out through our clarity or
observation, meaning as long as you cultivate the practice and persist in the practice, you'll find
it slowly fading away. Don't expect for it to just disappear. It's a pernicious one.
But over time and dedicate through dedication, you'll find that it slowly fades away. You don't
have to address it specifically. There's not some, it's not magic where you say conceit and you've
dealt with that. That's not how it goes. Like anything else, you can note it, but the real solution
to conceit is the general state of clarity of mind that comes from noting everything,
nothing to do with the conceit specifically, but to do with the purity of mind that doesn't allow
or doesn't admit any sort of conceit.
During walking meditation, should I walk extremely slowly so that I can note every step perfectly
or should I continue to walk moderately slowly? So trying to do everything perfectly is
never the right way. So that kind of language that you're using is a good morning indicator
that that's not the right way. Trying to fix things, trying to perfect things, make things better
than they are. Everything should be normal. Everything should be ordinary. You shouldn't feel
like you're walking fast or slow. It's most likely going to be slower than we normally walk
because we're normally with so much craving and ambition and inclination and what we're doing
that we leap ahead. So it will be quite a bit slower. That's, you know, you see walking meditation
is a very slow sort of movement, but it shouldn't really feel slow. It should just feel not
rushed anymore. So as you say moderately slowly is probably a good description, but eventually it won't
even feel slow. You won't have the idea that it's slower fast. It's just being with the
experience. So yeah, if it's too fast for you to observe it clearly, then it's probably too fast,
but you don't have to deliberately go slow, just because you can't experience it clearly.
That has much more to do with training and skill and proficiency. That just comes over time.
You have to be patient to cultivate the skill of clarity of mind.
During meditation, I felt my eyes water, goosebumps, and pressure on my forehead.
Is this good? There's nothing good or bad really in meditation. The only thing good is clarity
of mind. And that's not as mysterious as it sounds. It just means the perspective you have on
things. So for example, watering of the eyes, what's your perspective on those things? It sounds
like you're not sure. You have some hope that maybe this is a good thing, but hope is not a good
perspective. Hope is a bit of a clinging perspective clinging to the meaning behind it. Is it good?
We want it to be good. Goosebumps, pressure on your forehead. What's really good is a neutral
perspective that it is what it is when you feel all of these things that you're able to see them
just as what they are. That's what's good. So if you want to ask whether something is good,
that's what we should really be asked. Is it good to see these things as they are? That's what's
good. Yes, that is good. But for these things themselves, they have no capacity to be either good
or bad. They just are what they are. And if you can see that, that's good.
Can I change the object of my meditation during meditation? For example, if I am focusing on my
breath, can I change it to noting? I don't really understand the question. We focus on noting
if you haven't read our booklet on how to meditate and recommend that. Of course, you can sign
up for a course if you really want to go in depth of it. But if you mean you're noting the breath
and then you switch to noting something else, try and note what is clearest and if something
distracts you from the stomach rising and falling, then of course you should note it as well,
but noting is sort of the basis of our technique.
I could have an available time to meditate early in the morning, but I am discouraged because it
seems too difficult and undertaking even if it could have excellent benefits. How do I find the courage?
Courage is an important quality of mind,
something that comes from appreciation of the severity of our situation, of the importance of
spiritual practice, the urgency of it, of the greatness of it,
comes from things like study, it comes from association with good people.
It's a good question, it's a very practical question, even though we know we should be practicing
how do we actually go about picking ourselves up, putting aside our procrastination,
overcoming our procrastination, engaging in the practice for real, the manufacturers involved,
put a set association with good people, it's up there, the associated with good people, all good
things can be expected. Ultimately I think practically there is no
concrete answer to this question, it's a part of the struggle,
and so if you're struggling to practice in the morning then we'll continue to struggle,
don't try and find a resolution or because you'll just be disappointed if you can't,
if you try and then you fail, consider that the trying is the practice,
and if sometimes you are able to practice in the morning, well good, don't have expectations that
that's going to persist, just keep trying, that's how success comes about.
I'm a bit stuck with the formal practice, it feels too exhausting at the moment,
can keeping the precepts, watching dhamma talks, and reading sutas create a fertile soil for further progress.
Now without the practice, I mean I think it sounds like you've given throwing in the
towel with the practice and you really shouldn't, if something feels exhausting,
it's really somewhat abstract conception of it,
meditation can't be exhausting because it's not a real thing,
so what is real is this feeling of being exhausted or the feeling that makes you think you're
exhausted, maybe some tired, some disliking, maybe some laziness,
try and take things as they are from moment to moment, try and experience them.
Usually when this sort of thing happens, it's because we're not noting something,
we're not focused on what's actually there and we overlook the actual experience,
to try and take it as an object of meditation. You'll see that should improve things.
How does one deal with suicidal tendencies?
I think practically speaking one should seek professional help for suicidal tendencies,
they get extreme. Anyone can have a thought about suicide
without being suicidal, that if someone is truly considering suicide, they really should seek
professional help, there's not something that I'm qualified or
something I'm certified to help with,
and it's not necessarily something that it is going to be helped by spirituality.
I mean, your mind might be in such a state that you're not able to
appreciate and engage in teachings that I have to offer,
so in many cases like this, there are people who are better suited to support you during this time.
I guess I would say that if you really dedicate yourself to the practice,
there really shouldn't be a problem with such suicidal thoughts, and this relates very much to what
I was saying about how we can all experience these suicidal thoughts without actually being
suicidal, and that's the direction you want to go in, not getting rid of your suicidal thoughts
or inclinations, but trying to see them just as thoughts and inclinations, things that arise
and see their experiences as well. It's an experience of thought, it's not me thinking, it's not me
who whose life is an worth living, for example, these are just thoughts, they arise and they see
there's nothing more to them. If you can see that about everything, including suicidal ideation,
and then nothing will be a problem for you.
Dante, we've passed the hour, there are nine more questions in the first tier.
All right, go for it.
Would eating meat affect our meditation?
Maybe, I don't know if that meat particularly, if it has any effect, it's a very limited effect,
it's not something you should be concerned with. More than what you eat, how much you eat,
and how picky you are in your eating, that should be our concern.
Because those are things that have a real effect on the mind. If you're a glutton, if you're
obsessed and caught up in the act of eating throughout the day, it's going to distract you.
If you're picky about your food and selective and attached to taste, that's of course going to be
a great inhibitor, hindrance to seeing clearly.
Where exactly in nostril breath meditation does one focus?
Well, that's not our technique, so I'm going to defer that to defer, and I'm going to
re-direct that question. Does it address that if you're interested in what we do?
You can read our booklet and maybe take a natural meditation course.
How do you be mindful when being unintentionally dragged into stressful situations like work?
It seems I always forget to not practice, practice, practice. There really is no other
answer, work at it. I mean, I guess another factor is going to be how you live your life,
so try and find a life that is more supportive of your practice, but ultimately ultimately it's just
practice. It's part of the skill that we're trying to gain is the presence of mind, ability to be mindful.
If someone is not able to meditate, what is the best advice you can give to start again?
The best advice I think is that meditation starts now. It's not something you have to start in
some abstract sense. As soon as you think that you're not meditating, you can start meditating.
Every moment that you realize that you're not mindful, you can start to be mindful.
That's the best way to keep starting again and again. Keep yourself in it.
Just to remind yourself that you do it now anytime. As soon as you remember, as soon as you think of it,
if someone's not thinking about it at all, let's sure how much help one can be.
During my meditation, there comes a point where I become exceedingly focused. I start feeling a
very pleasant feeling in the body, like a lifting up in the air. Should I stay with the breath or
the feeling? Stay with the feeling if it's more clear. No, we don't focus exactly on the breath.
We focus on the movement and the stomach, rising and falling. If you're not doing our technique,
I encourage you to try reading our booklet to see how we teach and practice.
But these feelings are just more experiences. When they take your attention, just focus on them.
Feeling, feeling, feeling, if you like it, say liking, liking. If it's pleasant, you can just say pleasant or happy.
Does meditation increase one's sensitivity to sensations or emotions?
Yes. Yes, one becomes more present and so the clarity with which one experiences things
is increased. It feels things will feel more intense. You become more sensitive to them
and you're able to sense the more subtle changes in the more subtle variations of our mental
state. Often, in times of intense practice, I experience a phenomenon of lucid dreaming,
and I am a bit inexperienced in this area. Now, I find that it is more effective to meditate
when this state comes up. Is this proper? Well, it won't be more effective than your waking state,
but certainly if you have a wakefulness during sleep, you can try and apply the practice there.
Over time, you'll find that you dream less, you sleep less, so it isn't that, it isn't a long-term
issue, but certainly whenever you have the opportunity to be mindful, take it. I have followed
the advice to practice the mantras throughout the day, and I have been seeing how I am different
to the thoughts I have, is non-duality a part of this practice.
So, you haven't actually been seeing how you are different from the thoughts. You have what
you start to see is that the thoughts themselves are not self, but the idea that there is then a
self apart from that is still just some concept. This is still just some some extrapolation,
so I wouldn't pay too much attention to that. What's real is that you're going to see that
the thoughts are just thoughts, they arise and see, and that's what's important.
When the mind gets obsessively fixed on something, how does one be mindful of that?
Not just like anything else, not the obsession, not the clinging.
You see, mindfulness is always going to be intermittent, it's going to be in between everything
else. Your mind is going to incline in non-mindful ways, and to be mindful of those things,
you just see, remember mindfulness is not actually what we think of as mindfulness, it's
reminding yourself. So, it happens after you experience something, it always has to. As soon as you've
experienced something, you reply to it, you respond to it as an alternative to clinging, to reacting,
you respond to it with understanding, acknowledging, with a familiarity and an objectivity.
So, once you realize that you're obsessed and relaxed, that's when the mindfulness begins.
Okay, we've come to the end of our two-one questions. Okay, well, thank you all for good hour.
Thank you all for coming out. Thank you, Chris and team for your hard work.
Thank you, Jim and Dolo. It's our end-to. It's our end-to. It's our end-to.
They're all being a speed free from suffering. Have a good week, everyone.
