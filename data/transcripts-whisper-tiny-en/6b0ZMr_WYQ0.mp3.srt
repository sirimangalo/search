1
00:00:00,000 --> 00:00:07,000
First sex now murder. So now we have a question about killing people. What would you

2
00:00:07,000 --> 00:00:15,000
rather do kill one person to save a thousand or let one live and one thousand must die?

3
00:00:15,000 --> 00:00:20,280
One of these what if questions may be in the books full of these that you're supposed to

4
00:00:20,280 --> 00:00:24,760
torture yourself or it's really silly. I mean there's no I'm not causing a thousand people

5
00:00:24,760 --> 00:00:31,880
to die if I let one live. Yeah I could go around hunting out hunting down hunting out serial

6
00:00:31,880 --> 00:00:39,520
killers and shooting them and so on. I mean it's such a long long and complicated answer

7
00:00:39,520 --> 00:00:44,880
but this is the argument that we had over buying meat and so on. The point is you're

8
00:00:44,880 --> 00:00:52,080
only responsible for the actions that you do. An action is only unwholesome. It's only

9
00:00:52,080 --> 00:00:59,600
bad because of the nature of the mind of the person who performs it. That's it. There's

10
00:00:59,600 --> 00:01:06,080
no God telling us this is wrong. There's no society telling us that just because God tells

11
00:01:06,080 --> 00:01:10,040
you it's wrong is because society tells you it's wrong. It doesn't make anything wrong.

12
00:01:10,040 --> 00:01:18,480
Socrates pointed himself. Socrates said or Plato however but Socrates said that this man was

13
00:01:18,480 --> 00:01:23,920
going to take his parents to to court because he thought it's what the gods wanted and

14
00:01:23,920 --> 00:01:28,360
so Socrates asked him well so is it right to take your parents to court because the gods

15
00:01:28,360 --> 00:01:35,120
say it say it's right or do the gods say it's right because it's right and and either

16
00:01:35,120 --> 00:01:42,520
way you you can't evoke the gods. There is no entity that could tell you that something

17
00:01:42,520 --> 00:01:49,200
is right or wrong. The murder could be perfectly right. There's nothing intrinsic about

18
00:01:49,200 --> 00:01:56,120
the the act of killing someone that is in any way wrong. If one person kills another

19
00:01:56,120 --> 00:02:01,720
it's meaningless to you. If this person kills that person it's totally unrelated to your

20
00:02:01,720 --> 00:02:09,040
state of mind. You can just watch it and say seeing seeing. So there's no relationship

21
00:02:09,040 --> 00:02:12,440
and this is a scientific. This isn't Buddhist theory. I mean if you're a lot if you're

22
00:02:12,440 --> 00:02:17,080
thinking about this logically and removing your emotions from you thinking of it scientifically

23
00:02:17,080 --> 00:02:21,000
this is why science scientists have a real problem with ethics and why they cut up rats

24
00:02:21,000 --> 00:02:30,640
and poison rats and and wind up horrible people because they are unable to develop a clear

25
00:02:30,640 --> 00:02:34,440
code of ethics simply because from a scientific point of view there's nothing wrong

26
00:02:34,440 --> 00:02:39,400
with killing killing is the end of life. What does it mean? It means nothing. They're

27
00:02:39,400 --> 00:02:48,360
only understanding of ethics is a religious concept that has no basis in reality. So Buddhism

28
00:02:48,360 --> 00:02:55,600
just points one thing out that that not only does the the physical exist and go by laws

29
00:02:55,600 --> 00:03:03,920
of physics but the mind also exists and it goes by mind by laws of how the mind works.

30
00:03:03,920 --> 00:03:07,560
At the moment when you kill something you create kill someone you're creating a problem

31
00:03:07,560 --> 00:03:11,120
in your mind. When you let someone kill someone else you're not creating a problem in

32
00:03:11,120 --> 00:03:23,240
your mind. This is the only ethical principle that exists in teravada Buddhism anyway that

33
00:03:23,240 --> 00:03:28,360
the actions that you perform will affect your mind. The choices that you make. So if

34
00:03:28,360 --> 00:03:32,960
you're watching someone kill someone else or kill a thousand people creates anguish

35
00:03:32,960 --> 00:03:38,520
in your mind then you're responsible for the creating of anguish. If you could save those

36
00:03:38,520 --> 00:03:45,120
people and you choose to not save them. If all you had to do is sell them run then

37
00:03:45,120 --> 00:03:50,840
guys got a gun or something like that or someone's coming with a gun go ahead and you

38
00:03:50,840 --> 00:03:55,480
don't do that then you're responsible for that. You're not ever responsible for the killing

39
00:03:55,480 --> 00:04:00,880
unless you're the one pulling the trigger or unless you're the one telling a person to kill.

40
00:04:00,880 --> 00:04:05,040
If you say this person kill that man. I mean you're still not responsible for pulling

41
00:04:05,040 --> 00:04:09,080
the trigger. You're responsible for telling that person to do something. You're responsible

42
00:04:09,080 --> 00:04:15,400
for the ethical qualities of mind, the greed, the anger and the delusion inherent in

43
00:04:15,400 --> 00:04:23,520
the mind, inherent in the mind at that moment. So if you don't kill the person who's

44
00:04:23,520 --> 00:04:27,720
going to kill a thousand people then it may be that unwholesome miserises in your mind

45
00:04:27,720 --> 00:04:35,360
as a result. You think that those people die good, nothing to do, not my problem and so

46
00:04:35,360 --> 00:04:39,160
as a result you create some kind of worry and anguish afterwards. I couldn't save them

47
00:04:39,160 --> 00:04:46,680
all I had to do was kill that one man but on the other hand if you kill that one person

48
00:04:46,680 --> 00:04:51,120
then you have to say what is different. What is worse? This guilt that I have over

49
00:04:51,120 --> 00:04:58,640
that those thousand deaths which weren't my, weren't the result of my actions and so the

50
00:04:58,640 --> 00:05:03,480
only reason it comes up is because there's some intellectualizing or some minor feelings

51
00:05:03,480 --> 00:05:09,000
of guilt or the feeling of pulling a trigger. I think the real reason why people have a problem

52
00:05:09,000 --> 00:05:15,960
with this and a problem with understanding things like hunting in general is because

53
00:05:15,960 --> 00:05:24,040
they've never done it themselves. If you've never killed a significantly large being like

54
00:05:24,040 --> 00:05:30,920
my example is a deer because that's how far I went or even more a human being then you

55
00:05:30,920 --> 00:05:36,360
have no real right to argue this question and I don't have the right to argue but if

56
00:05:36,360 --> 00:05:41,880
you've read the books, if you've read accounts of people who have killed and how it

57
00:05:41,880 --> 00:05:48,040
affects them, any of these books the book I was referring to earlier was crime and punishment.

58
00:05:48,040 --> 00:05:54,000
It's just an excellent, excellent description of the torture that goes through the mind

59
00:05:54,000 --> 00:05:59,000
of someone who kills another human being that we can't even fathom most of us. We think

60
00:05:59,000 --> 00:06:03,480
well it's just intellectualizing so you kill someone what's the problem. Totally different

61
00:06:03,480 --> 00:06:08,720
when you actually try to do it. It's an incredibly powerful act. The only comparison I have

62
00:06:08,720 --> 00:06:14,160
is as I said with hunting with killing a deer, I had no compunction, no problem with killing

63
00:06:14,160 --> 00:06:20,560
and I killed insects and small animals and no problem with hunting. I got my hunting license

64
00:06:20,560 --> 00:06:28,880
trained and so on and then I sat up in the tree and when the deer came raising this crossbow

65
00:06:28,880 --> 00:06:34,440
ready to pull the trigger and suddenly my whole body started to shake. The deer was no threat

66
00:06:34,440 --> 00:06:39,520
to me. There was no fear. There should have been no fear in my mind. It wasn't like there

67
00:06:39,520 --> 00:06:46,920
was some danger to me but I just got this incredible dread and revulsion to it and my whole

68
00:06:46,920 --> 00:06:53,240
body started to shake and I could barely pull the trigger. That's how powerful it is.

69
00:06:53,240 --> 00:07:00,360
This wasn't totally not intellectual because I had no feelings of hesitation. I was perfectly

70
00:07:00,360 --> 00:07:04,920
ready to do this and yet when it came time to do that because I had never done it because

71
00:07:04,920 --> 00:07:11,800
my mind was too pure from that. Pure of that. It had an incredible effect on my mind and

72
00:07:11,800 --> 00:07:15,560
this is what you're looking at if you kill someone. If you're just talking about the guilt

73
00:07:15,560 --> 00:07:21,960
feelings of not having prevented other people's death when actually in the end all those people

74
00:07:21,960 --> 00:07:26,680
are going to have to die anyway. Whether it be from a bullet wound or whether it be from old age,

75
00:07:26,680 --> 00:07:34,680
everyone dies and in horrible ways you can't possibly save even a small portion of the people who

76
00:07:34,680 --> 00:07:42,600
have to die in horrible ways. If it's just a matter of feeling that kind of guilt from not having

77
00:07:42,600 --> 00:07:55,800
prevented the inevitable or put off the inevitable then it's far preferable to the horror of having

78
00:07:55,800 --> 00:08:06,760
killed a human being even an evil human being. There are other accounts of this. I can't remember

79
00:08:06,760 --> 00:08:13,960
where people, police officers who have had to kill humans and they can have the right. The one is

80
00:08:13,960 --> 00:08:20,840
post-traumatic stress disorder who will go to soldiers who go to war and will come back and

81
00:08:20,840 --> 00:08:26,200
just don't want to ever talk about it again. They've just locked off that part of their mind.

82
00:08:26,200 --> 00:08:31,560
It's just so horrific. If you've never been to war you can't imagine the horrors of war and people

83
00:08:31,560 --> 00:08:37,480
who go just have, you know, are not able to sleep at night because of what they've seen and what

84
00:08:37,480 --> 00:08:45,720
they've done. So, me and I was telling us about these caravans that they had to drive from point

85
00:08:45,720 --> 00:08:51,400
A to point B and they weren't allowed to stop for anything. A man, a woman, child standing in the

86
00:08:51,400 --> 00:08:57,560
middle of the road had to be run over basically. This kind of thing. Maybe that's going quite far

87
00:08:57,560 --> 00:09:14,760
or field, but the horror that is involved with these visceral acts or these real acts of body. There's an

88
00:09:14,760 --> 00:09:20,920
incredible power there. But the point is that there's no intellectualizing in Buddhism. Buddhism is

89
00:09:20,920 --> 00:09:26,600
experiential, experientially. It's based on experience. You can't come up with a theory of morality

90
00:09:26,600 --> 00:09:35,240
and call it Buddhism. Buddhism is you are responsible for your own actions and you can never escape

91
00:09:35,240 --> 00:09:42,600
that responsibility and you're responsible for nothing else. And that is scientific. It's

92
00:09:44,120 --> 00:09:51,000
verifiable. You can never verify what is the guilt of letting someone kill someone else.

93
00:09:51,000 --> 00:09:59,320
You can only verify what is the suffering involved in your own actions. So, if you choose to

94
00:09:59,320 --> 00:10:04,840
feel guilty about it, that's going to create suffering. If you choose to kill a person to not feel

95
00:10:04,840 --> 00:10:13,320
guilty, then that's going to cause, I would say as well, or even more intense feelings of horror and

96
00:10:13,320 --> 00:10:25,560
no nightmares, because now you're a murderer. Whether it was justified murder or not, it was murder.

