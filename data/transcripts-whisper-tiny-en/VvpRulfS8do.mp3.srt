1
00:00:00,000 --> 00:00:06,000
Okay, we need a bigger one.

2
00:00:06,000 --> 00:00:16,000
Welcome to our daily dumb session.

3
00:00:16,000 --> 00:00:36,000
I'm in an inadvertently caught up in a bit of a grew-ha-ha here in Ontario, Canada,

4
00:00:36,000 --> 00:00:45,000
regarding free speech and gender identity.

5
00:00:45,000 --> 00:00:53,000
So that I look tonight at free speech.

6
00:00:53,000 --> 00:00:58,000
The story is that I happen to be at this meeting.

7
00:00:58,000 --> 00:01:07,000
I'm on a subcommittee for this committee for building an inclusive community.

8
00:01:07,000 --> 00:01:11,000
I make Master University, so.

9
00:01:11,000 --> 00:01:22,000
And the idea is to combat, for the most part, combat things like racism and religious bigotry and so on.

10
00:01:22,000 --> 00:01:31,000
Sexism, homophobia, transphobia, that kind of thing.

11
00:01:31,000 --> 00:01:34,000
Because there's unfortunately quite a bit of it going around.

12
00:01:34,000 --> 00:01:47,000
There's not-theism has come up and there's, of course, this deep-seated hatred of Arabic people.

13
00:01:47,000 --> 00:01:56,000
And there's hatred for transsexual people, hatred for homosexual people.

14
00:01:56,000 --> 00:02:02,000
There's sexism. I've had a lot of problems in this world.

15
00:02:02,000 --> 00:02:10,000
Master University has its share and I think it's somewhat unique in its approach,

16
00:02:10,000 --> 00:02:20,000
or its interest in coming to terms with those things and trying to do away with them.

17
00:02:20,000 --> 00:02:24,000
Sometimes successful, sometimes perhaps not.

18
00:02:24,000 --> 00:02:29,000
I think it's worthwhile because all these things, of course, talk about defilements.

19
00:02:29,000 --> 00:02:43,000
They all come from a corrupt state of mind, bigotry and prejudice and a theme to Buddhism.

20
00:02:43,000 --> 00:02:47,000
They're not a part of Buddhism.

21
00:02:47,000 --> 00:02:51,000
Buddhism has something to say about these things.

22
00:02:51,000 --> 00:02:55,000
Anyway, so I was at this meeting and I got called up.

23
00:02:55,000 --> 00:02:57,000
I was just sitting in the back.

24
00:02:57,000 --> 00:03:00,000
So there's the main committee meeting and I just showed up.

25
00:03:00,000 --> 00:03:06,000
I thought it'd be nice to show my support and ended up getting corralled and to being in the picture.

26
00:03:06,000 --> 00:03:10,000
I ended up making-it was a long story.

27
00:03:10,000 --> 00:03:20,000
Somehow that picture got caught up in this violent debate over free speech and gender identity.

28
00:03:20,000 --> 00:03:30,000
I've been identified as a part of the social justice warrior and whatever they cause.

29
00:03:30,000 --> 00:03:32,000
Movement.

30
00:03:32,000 --> 00:03:49,000
It's quite unpleasant, I think, to read some of the comments that once he's associated with these things.

31
00:03:49,000 --> 00:04:00,000
One of the comments and it identified Buddhism as a devouring mother religion.

32
00:04:00,000 --> 00:04:03,000
I wish I had such a bizarre statement.

33
00:04:03,000 --> 00:04:07,000
I didn't understand if it was even grammatically correct.

34
00:04:07,000 --> 00:04:13,000
But it turns out some of you are probably smarter than me and let's know what this means.

35
00:04:13,000 --> 00:04:22,000
It turns out that devouring mother is an archetype I think is the word that young Carl Jung used, I think.

36
00:04:22,000 --> 00:04:33,000
If you look up devouring mother, you can try and figure out how Buddhism could be understood as that.

37
00:04:33,000 --> 00:04:39,000
I couldn't understand it.

38
00:04:39,000 --> 00:04:43,000
Of course, it's incredible.

39
00:04:43,000 --> 00:04:56,000
People look at this picture and say all of us are a bunch of freaks and typical social justice warrior folk.

40
00:04:56,000 --> 00:05:17,000
The drags of humanity because we don't look like the stereotypical male-female binary, beautiful, sexy, attractive, well-dressed,

41
00:05:17,000 --> 00:05:26,000
contributing member of society.

42
00:05:26,000 --> 00:05:27,000
That kind of thing.

43
00:05:27,000 --> 00:05:35,000
It's incredible for me is how much corruption of mine there is involved.

44
00:05:35,000 --> 00:05:44,000
You can't be impartial about this.

45
00:05:44,000 --> 00:05:47,000
There's no not taking sides.

46
00:05:47,000 --> 00:05:50,000
This is wrong.

47
00:05:50,000 --> 00:05:59,000
There's no question that such a bigotry and hatred is wrong.

48
00:05:59,000 --> 00:06:01,000
That's what's interesting about this debate for me.

49
00:06:01,000 --> 00:06:05,000
It's the difference between free and right.

50
00:06:05,000 --> 00:06:25,000
That's where Buddhism differs from a society and reasonably so because society is our society here in Canada's pluralistic.

51
00:06:25,000 --> 00:06:39,000
We would want, no one would want it to be, well, we wouldn't as a community want it to be following one religion because there's a whole bunch of us who wouldn't agree with that.

52
00:06:39,000 --> 00:06:49,000
So there's many values that we have to leave unlegislated.

53
00:06:49,000 --> 00:06:57,000
But people talk a lot about free speech and it goes beyond simply not legislating it.

54
00:06:57,000 --> 00:07:04,000
It goes to the point of believing that if I can say it, that's right.

55
00:07:04,000 --> 00:07:06,000
It's okay to say it.

56
00:07:06,000 --> 00:07:17,000
Because I can say it, that's enough to think that it's okay to say.

57
00:07:17,000 --> 00:07:20,000
I mean, basically you say what you want.

58
00:07:20,000 --> 00:07:21,000
That's free speech.

59
00:07:21,000 --> 00:07:24,000
We have these ideas about freedom.

60
00:07:24,000 --> 00:07:28,000
You know this term of the free thinker?

61
00:07:28,000 --> 00:07:32,000
Well, psychopaths are free thinkers.

62
00:07:32,000 --> 00:07:38,000
I think Hitler was probably to bring up the best example.

63
00:07:38,000 --> 00:07:43,000
Hitler was probably at the free thinker.

64
00:07:43,000 --> 00:07:52,000
Not so interested in freedom this sort in Buddhism.

65
00:07:52,000 --> 00:08:02,000
It's a freedom of immoral.

66
00:08:02,000 --> 00:08:09,000
It's an immoral freedom in the sense that there is no right and wrong.

67
00:08:09,000 --> 00:08:15,000
It's the free speech, it's the right to shout theater and a fire in a crowded theater.

68
00:08:15,000 --> 00:08:19,000
Theater in a crowded fire, in other words, plus put.

69
00:08:19,000 --> 00:08:23,000
You can say what you want, but that's not free speech.

70
00:08:23,000 --> 00:08:29,000
That's not how free speech should be understood because it's wrong speech.

71
00:08:29,000 --> 00:08:33,000
You yell fire in a crowded theater, that's very wrong.

72
00:08:33,000 --> 00:08:35,000
Are you afraid to say it?

73
00:08:35,000 --> 00:08:42,000
I mean, no one can really stop you, they can punish you for it, but it's not easy to stop someone.

74
00:08:42,000 --> 00:08:51,000
So in a sense we're all free to say such things.

75
00:08:51,000 --> 00:08:57,000
It's important to distinguish this as meditators because it gives us a sense of what we're trying to do here.

76
00:08:57,000 --> 00:09:01,000
We're trying to understand what is right.

77
00:09:01,000 --> 00:09:12,000
Free will, we're not so interested in understanding free will or answering this question of free will.

78
00:09:12,000 --> 00:09:19,000
We're very much interested in right will, right intention, right?

79
00:09:19,000 --> 00:09:20,000
Do we have free will?

80
00:09:20,000 --> 00:09:21,000
Who cares?

81
00:09:21,000 --> 00:09:22,000
Do we have right will?

82
00:09:22,000 --> 00:09:27,000
That's the question.

83
00:09:27,000 --> 00:09:35,000
We're not so interested in our own ideas because in the end it's not even freedom, right?

84
00:09:35,000 --> 00:09:44,000
We say I want free speech, but what you really mean is I want to be a slave to my personality,

85
00:09:44,000 --> 00:09:46,000
and that's what it is.

86
00:09:46,000 --> 00:09:54,000
You're not free, you're completely contrived and so you see people who grew up in conservative areas

87
00:09:54,000 --> 00:10:05,000
spouting nonsense about homosexual people and women and blacks and Mexicans and Muslims,

88
00:10:05,000 --> 00:10:07,000
Arabs.

89
00:10:07,000 --> 00:10:18,000
I'm not so keen on Islam myself, so I want to distinguish that, but talking about people.

90
00:10:18,000 --> 00:10:24,000
It's not free speech, there's nothing free about that.

91
00:10:24,000 --> 00:10:34,000
You're totally trapped in this vicious, pernicious, corrupt mind.

92
00:10:34,000 --> 00:10:43,000
I've met people like this, it's quite scary that such people exist.

93
00:10:43,000 --> 00:10:51,000
And so this is a crucial aspect of our meditation that we not be attached to our personality.

94
00:10:51,000 --> 00:10:59,000
And we see that we come to see that much of what we think of as free will, free thought, free speech.

95
00:10:59,000 --> 00:11:05,000
It comes from our personality.

96
00:11:05,000 --> 00:11:17,000
It's dependent on the artifices that we build up, the patterns of behavior that we carry with us.

97
00:11:17,000 --> 00:11:21,000
We're not free in any sense of the word.

98
00:11:21,000 --> 00:11:27,000
The only way you could truly be free is if you were free from the corruption,

99
00:11:27,000 --> 00:11:38,000
if you had right speech, because then you would be free not to, but you would be free from suffering,

100
00:11:38,000 --> 00:11:47,000
free from evil, free from blame, free from justified blame anyway.

101
00:11:47,000 --> 00:11:54,000
They're all going to blame you for something.

102
00:11:54,000 --> 00:12:01,000
So society has a problem with trying to legislate right to what is wrong.

103
00:12:01,000 --> 00:12:08,000
And so there is free speech is allowed.

104
00:12:08,000 --> 00:12:11,000
It's an important aspect of a pluralistic society.

105
00:12:11,000 --> 00:12:14,000
We want to be allowed to dissent.

106
00:12:14,000 --> 00:12:23,000
We want to be allowed to have independent opinions.

107
00:12:23,000 --> 00:12:27,000
So the recognition that many of you have different people have different views

108
00:12:27,000 --> 00:12:38,000
to allow this exchange of views, this exchange of ideas.

109
00:12:38,000 --> 00:12:51,000
So this question came up of where should society legislate, how far should society go to legislate speech?

110
00:12:51,000 --> 00:12:56,000
But there is a line. I mean, there's a clear line.

111
00:12:56,000 --> 00:12:59,000
I can attack your religion.

112
00:12:59,000 --> 00:13:03,000
I should be able to say, I don't really like Islam.

113
00:13:03,000 --> 00:13:07,000
I should be able to say Islam is not a very good religion in my view.

114
00:13:07,000 --> 00:13:14,000
Some good things. I mean, that would be overly general, but Islam has some problems.

115
00:13:14,000 --> 00:13:16,000
I should be able to say that.

116
00:13:16,000 --> 00:13:24,000
To be clear, I would say the same thing about Christianity and Judaism.

117
00:13:24,000 --> 00:13:27,000
Hinduism.

118
00:13:27,000 --> 00:13:33,000
But the worst ones are Judaism and Islam, I think, are probably the worst.

119
00:13:33,000 --> 00:13:38,000
I just have such horrible things in them.

120
00:13:38,000 --> 00:13:43,000
Some good. You can find gems of goodness as you can find.

121
00:13:43,000 --> 00:13:46,000
Nothing is cut and dried like that.

122
00:13:46,000 --> 00:13:49,000
But you should be able to say that as my point.

123
00:13:49,000 --> 00:13:58,000
But I shouldn't be able to say that Arab people are dirty or Arab people are evil or Arab people are thieves, crooks.

124
00:13:58,000 --> 00:14:01,000
You shouldn't be able to say that.

125
00:14:01,000 --> 00:14:06,000
That's called hate speech.

126
00:14:06,000 --> 00:14:09,000
It's not hate speech, in my mind anyway.

127
00:14:09,000 --> 00:14:20,000
I hope it never becomes a lot of hate speech to say that Islam is, you know, this view in Islam is bad.

128
00:14:20,000 --> 00:14:25,000
In Judaism, when they say you should throw stones at people, you should stone people to death.

129
00:14:25,000 --> 00:14:29,000
That's bad. That's wrong.

130
00:14:29,000 --> 00:14:39,000
But Jewish people are shisters, our crooks shouldn't be able to say that.

131
00:14:39,000 --> 00:14:42,000
That's hate speech.

132
00:14:42,000 --> 00:14:51,000
That's what we look at in Buddhism.

133
00:14:51,000 --> 00:15:03,000
It's very, it dovetails quite nicely with the line that we draw in society in Buddhism.

134
00:15:03,000 --> 00:15:13,000
One of the, I've always admired to some extent anyway, the law judges.

135
00:15:13,000 --> 00:15:28,000
If you ever read the opinions of judges on cases, they're thoughtful people and they tend to be, in many ways, a line with Buddhism.

136
00:15:28,000 --> 00:15:33,000
Like studying religion, how religion has been applied.

137
00:15:33,000 --> 00:15:35,000
In many ways, it's quite Buddhist.

138
00:15:35,000 --> 00:15:45,000
It analyzes religion in terms of what's actually going on to people actually hold these views and so on.

139
00:15:45,000 --> 00:15:54,000
So in this case, it dovetails nicely with the fact that we're talking about ethics.

140
00:15:54,000 --> 00:16:21,000
We have the right to say all sorts of crazy things, but if it's hurtful, if it's hateful, if it's disregarding, if it's negligent speech, purposefully disregarding someone's worth as a person,

141
00:16:21,000 --> 00:16:31,000
all of these things we would consider to be potential objects of legislation because they're potentially wrong.

142
00:16:31,000 --> 00:16:37,000
Not only wrong, but they're egregiously wrong, we would say.

143
00:16:37,000 --> 00:16:49,000
That's interesting from a Buddhist point of view as well because meditation or in a meditation center, there are many things that can go wrong.

144
00:16:49,000 --> 00:17:11,000
We talk about, in Christianity, they say that committing adultery, he said, if you think about your neighbor's wife or something like that, you've already committed adultery or something like that.

145
00:17:11,000 --> 00:17:29,000
So I'm not going to go around about it. So this isn't meant to be about bashing Christianity or his other religions, but we make this distinction in Buddhism as well between evil and egregiously evil.

146
00:17:29,000 --> 00:17:37,000
I made a comment that was perhaps problematic on Stack Exchange where I said all sexual activities are moral.

147
00:17:37,000 --> 00:17:55,000
And I've been thinking about that or I was thinking about that. I mean, they're just words, but you do want to be clear that I'm not saying you're an evil person because you have sex, but sexuality is involved with Loba, which is an unwholesome mind state.

148
00:17:55,000 --> 00:18:06,000
It's an evil mind state. But it's perhaps wrong because you want to be clear that you're not, there's different levels to this.

149
00:18:06,000 --> 00:18:15,000
So we wouldn't let just let people yelling at each other, for example, if I yell at you and say, I hate you, you're worthless, you're stupid.

150
00:18:15,000 --> 00:18:19,000
We don't want to let them just like that.

151
00:18:19,000 --> 00:18:34,000
But when you attack a person's very being or when your speech is harmful to one's very being, that's where we want to say it's egregious.

152
00:18:34,000 --> 00:18:40,000
And so in Buddhism, it's interesting, in Buddhism this sort of hate speech is actually legislated.

153
00:18:40,000 --> 00:18:45,000
You can't even jokingly.

154
00:18:45,000 --> 00:18:55,000
There was this horrible situation. Thailand can be really, I don't know, traditional societies sometimes get caught up in incredible prejudice.

155
00:18:55,000 --> 00:19:14,000
Just due to geographic isolation mostly, and remember when I first went to Thailand, I was sitting with this Thai monk and we were watching this dark skinned tourist walk by, and he turns to him and he says, oh, dirty.

156
00:19:14,000 --> 00:19:25,000
I don't know if that's hate speech or that's, you know, not in hate speech, but that's terrible racism, bigotry, whatever you want to call it. That's just awful.

157
00:19:25,000 --> 00:19:36,000
But then I lived with listening to some monks many years later, joke about this. There was this dark skin man going to stay up on a mountain monk going to stay up.

158
00:19:36,000 --> 00:19:47,000
And he wasn't even, he was Thai, but he was from the south so he had dark skinned, and he was going to stay up on the mountain.

159
00:19:47,000 --> 00:20:05,000
And one of the very white skinned Bangkok monks, rich kid, said to him, I mean they were good friends, but he says to him, he says, oh, we'll come back and the clouds will be all black.

160
00:20:05,000 --> 00:20:11,000
That's actually wrong, but that's breaking a rule in Buddhism, even as just as a joke.

161
00:20:11,000 --> 00:20:29,000
If you joke about someones, like if you make a joke about women, if I say something like, just as a joke, I say women should be barefoot and pregnant or something like that, even as a joke, breaking a rule.

162
00:20:29,000 --> 00:20:53,000
So there's this question now of whether people who don't identify, first the question of if I was born a man and I transitioned to become a woman,

163
00:20:53,000 --> 00:21:06,000
and then someone still refers to me as a man disregarding the fact that I want to be referred to as a woman. Is that hate speech?

164
00:21:06,000 --> 00:21:13,000
There's a big, this is a huge violent debate. This is the debate that I caught cut up in.

165
00:21:13,000 --> 00:21:27,000
And more than that, would if someone doesn't identify as either male or female and they want to be called day or them, they don't want you to refer to them as him or her he or she.

166
00:21:27,000 --> 00:21:41,000
And so there's this one professor that has refused to use third person pronouns or refused to use pronouns in general and says he's going to call people male or female. I think that's what his argument is.

167
00:21:41,000 --> 00:21:56,000
Or what his stance is. Anyway, I want to get too much into that, but looking at the, just getting back to the ideas that were interested in the results.

168
00:21:56,000 --> 00:22:15,000
Or it's an important thing to think about as well as the results of his obstinate and his insistence that this is a violation of free speech. I mean the results of that have not been pleasant.

169
00:22:15,000 --> 00:22:32,000
It has fed a huge amount of hatred and bigotry and corruption of mine. People who had these thoughts, people who was ignorance.

170
00:22:32,000 --> 00:22:39,000
And it is ignorance. I mean not to say that he's ignorant or that everyone is ignorant, but there is ignorance.

171
00:22:39,000 --> 00:22:51,000
People don't understand and they never really thought about what is it like to be someone who doesn't identify as male or female. What is it like to be, feel like a man trapped in a woman's body?

172
00:22:51,000 --> 00:23:04,000
I mean most of us can't understand it. You can't sympathize. We don't know what it's like, but we can sympathize and we can try to understand.

173
00:23:04,000 --> 00:23:24,000
Many people don't even try to understand or accept the fact that it might be very difficult, except that this person doesn't see themselves the way we see them for the way we would expect them to see themselves.

174
00:23:24,000 --> 00:23:40,000
And so to me there's an issue there. This is something where I would want to have compassion for this person. And I would want to be airing on the side of compassion.

175
00:23:40,000 --> 00:24:09,000
Could people take advantage of it, apparently, yes. Apparently there are people who want to be called pixiekin and dragonkin or wormkin or something, who they identify with. And I can't help but think that a lot of that's just frivolous and silly, but nonetheless it doesn't infringe upon my rights in any meaningful way to have to call someone pixiekin.

176
00:24:09,000 --> 00:24:24,000
If someone wants to be called a pixie, I want people to call me a beku. There are two sides. I'll get to another side of it. But this is the first side.

177
00:24:24,000 --> 00:24:48,000
I think it's dangerous for us to say that we should be allowed to disregard people's identity. We should be allowed to delegitimatize who they are on the one hand.

178
00:24:48,000 --> 00:25:09,000
Why? Because the results. The results are huge disparity and irregularity. I don't know what you call this statistical significant disparity between suicide rates, where you have trans people committing suicide in great numbers.

179
00:25:09,000 --> 00:25:22,000
Sexual people as well. But no, I would do things specifically talking about trans people here. It's just off the charts. It's very high.

180
00:25:22,000 --> 00:25:49,000
And besides suicide, you have murder. The results is such bad and hatred when you do this, when you fail to sympathize. When you fail to see the social importance of making allowance for these people.

181
00:25:49,000 --> 00:26:02,000
It's a difference between equity and equality, they say. Equality means there's the door we walk through it. Equity means well, if you're in a wheelchair, we'll make a ramp for you.

182
00:26:02,000 --> 00:26:10,000
Certain people have special needs. It's not whether I think they have the needs, it's whether.

183
00:26:10,000 --> 00:26:29,000
I mean, I guess it's to some extent whether they think they do worrying about abuses and other things, but no, I guess it's whether society thinks they do. Whether they legitimately do.

184
00:26:29,000 --> 00:26:45,000
But my point, I mean, this is what I got and caught up in. I'm not really caught up in it. It's just funny kind of that I've been called out on the internet and Buddhism has been called these bad names and so on by people who those opinions don't really matter to me.

185
00:26:45,000 --> 00:26:53,000
Though, criticism is always to be taken and considered.

186
00:26:53,000 --> 00:27:02,000
But the point is the corruption, the evil involved. There's no other way of putting it, this is a grain. Evil.

187
00:27:02,000 --> 00:27:13,000
But there's another side to this and the other side is the social justice warrior.

188
00:27:13,000 --> 00:27:26,000
Personally, I like this term. I think it's good to fight for social justice. It's a good thing. But it becomes mean and nasty.

189
00:27:26,000 --> 00:27:33,000
So there was a huge protest when this professor tried to come and talk and they wouldn't even let him talk, which I don't think he has anything.

190
00:27:33,000 --> 00:27:43,000
I meant personally, I'm not that impressed by what he has to say, but the fact that they were so angry at him. I don't see that as being a good thing either.

191
00:27:43,000 --> 00:27:58,000
If anything out of this as meditators, as Buddhists, what we can get is how fairly simple it is to figure out all these complex society issues.

192
00:27:58,000 --> 00:28:05,000
Untangling the tangling, right, what I was talking about last night. This was sort of the thing I was thinking about.

193
00:28:05,000 --> 00:28:14,000
It's not that difficult. If we could be sensitive and compassionate to others, and if we could be pure in our hearts.

194
00:28:14,000 --> 00:28:27,000
So the internal and the external, externally, we're conscious, we're mindful of the impacts that our acts and our words and our thoughts have on others.

195
00:28:27,000 --> 00:28:33,000
And we try to act accordingly, not with free thought, free speech, free action.

196
00:28:33,000 --> 00:28:39,000
That's rubbish. But with right thought, right speech, right action.

197
00:28:39,000 --> 00:28:46,000
And it doesn't have to be strict in line with this religious principle and that religious principle.

198
00:28:46,000 --> 00:29:02,000
But if we recognize that something is just a bad thing to do or say that it's causing harm and it's either outright evil or it's dismissive, then we should avoid that.

199
00:29:02,000 --> 00:29:08,000
Society should have something to say about that.

200
00:29:08,000 --> 00:29:15,000
I think.

201
00:29:15,000 --> 00:29:26,000
And so externally and internally, we look at our minds, working out why am I doing things.

202
00:29:26,000 --> 00:29:34,000
There's so much you can learn and all these stances that we take. I'm right, thinking I'm right.

203
00:29:34,000 --> 00:29:43,000
They all fall apart. They become quite humble, quite ashamed in many ways of who you are.

204
00:29:43,000 --> 00:29:50,000
Realizing how arrogant and pretentious we can be.

205
00:29:50,000 --> 00:29:59,000
How set we can become in our views and opinions, how caught up by them we become.

206
00:29:59,000 --> 00:30:05,000
It's very hard to change your view once you've committed to it, right?

207
00:30:05,000 --> 00:30:10,000
That's the wonder of meditation and say, you don't have to believe me, you don't have to agree with me.

208
00:30:10,000 --> 00:30:17,000
When you look inside, you'll see right and wrong.

209
00:30:17,000 --> 00:30:36,000
And I mean, perhaps one thing that is not well enough admitted or acknowledged by the world is that there is right and wrong beyond subjective ideas of it.

210
00:30:36,000 --> 00:30:45,000
But there is only one right and wrong. So the question of how you find it.

211
00:30:45,000 --> 00:30:48,000
Many people would say it's impossible to know right and wrong.

212
00:30:48,000 --> 00:30:52,000
It's actually quite easy, right? This leads to suffering.

213
00:30:52,000 --> 00:30:56,000
Okay, I'll stop that. That's wrong. This leads to happiness to peace.

214
00:30:56,000 --> 00:31:01,000
That's right. Everything becomes so simple.

215
00:31:01,000 --> 00:31:06,000
You can see through any complex problem, because you see what's going on in people's minds.

216
00:31:06,000 --> 00:31:12,000
I can have the greatest argument for why you should agree with me.

217
00:31:12,000 --> 00:31:15,000
And I can still be very wrong.

218
00:31:15,000 --> 00:31:25,000
You cut through it and you see this person's just full of themselves, arrogant, puffed up.

219
00:31:25,000 --> 00:31:32,000
And cut through it, cut through the delusion with a knife.

220
00:31:32,000 --> 00:31:41,000
The knife we use is objectivity based on the four foundations of mindfulness or mindfulness, let's say.

221
00:31:41,000 --> 00:31:49,000
When we observe things, when we remind ourselves, this is this, and is that anger is anger.

222
00:31:49,000 --> 00:31:58,000
Speech is speech. It's not free, it just is. It's not right, it just is.

223
00:31:58,000 --> 00:32:09,000
When we start to see what's really right and wrong is our own defilement, our own intentions.

224
00:32:09,000 --> 00:32:14,000
To see those qualities of mind that are wrong.

225
00:32:14,000 --> 00:32:22,000
And they're wrong in a very simple sense. They're wrong because they're the wrong.

226
00:32:22,000 --> 00:32:27,000
They don't lean to the desired result.

227
00:32:27,000 --> 00:32:30,000
They don't fulfill our expectations.

228
00:32:30,000 --> 00:32:33,000
Green doesn't fulfill our expectation for happiness.

229
00:32:33,000 --> 00:32:38,000
Anger doesn't fulfill our expectation for freedom from suffering.

230
00:32:38,000 --> 00:32:53,000
The delusion doesn't fulfill our expectation of self of identity.

231
00:32:53,000 --> 00:33:02,000
Because as we know, reality is impermanent, suffering, and non-self.

232
00:33:02,000 --> 00:33:11,000
That's what we're looking at. So I think it's a really good example of how meditation goes all the way

233
00:33:11,000 --> 00:33:20,000
from the microcosm to the macrocosm, how society can be understood as a meditator.

234
00:33:20,000 --> 00:33:30,000
It's also a good way of tackling this debate about free wills or free speech, free thinkers.

235
00:33:30,000 --> 00:33:34,000
Free thought.

236
00:33:34,000 --> 00:33:40,000
On the one hand, we want to give this to society. Society wants to be pluralistic.

237
00:33:40,000 --> 00:33:46,000
It's important as we acknowledge that not everyone is Buddhist.

238
00:33:46,000 --> 00:33:53,000
Unfortunately.

239
00:33:53,000 --> 00:34:03,000
But on the other hand, as Buddhists, we want to speak up and say, hatred is not allowed.

240
00:34:03,000 --> 00:34:06,000
The bigotry is not allowed.

241
00:34:06,000 --> 00:34:08,000
We will not tolerate it.

242
00:34:08,000 --> 00:34:10,000
What does that mean not tolerate? I don't know.

243
00:34:10,000 --> 00:34:21,000
I guess whether we will tolerate it, but we will put our foot down and say that this goes against the principles of a just and fair society.

244
00:34:21,000 --> 00:34:24,000
Things goes against goodness.

245
00:34:24,000 --> 00:34:35,000
So we wouldn't throw people in jail over it, but we would take a stand and say, that's wrong.

246
00:34:35,000 --> 00:34:42,000
Anyway, so there's the dhamma for tonight. Hopefully it wasn't too worldly for y'all.

247
00:34:42,000 --> 00:34:52,000
Thank you all for tuning in. Have a good night.

