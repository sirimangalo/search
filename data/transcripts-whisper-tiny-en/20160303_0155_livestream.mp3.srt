1
00:00:00,000 --> 00:00:27,440
Good evening, everyone, broadcasting live, March 2nd, 2016, so I've been sick for the past

2
00:00:27,440 --> 00:00:37,680
days last night, last night the power went out, so no internet, no

3
00:00:37,680 --> 00:00:48,040
not the power on the internet went out, power was on the internet went out, so

4
00:00:48,040 --> 00:01:07,360
probably it was still too sick to broadcasting, but I said freedom from sickness is

5
00:01:07,360 --> 00:01:21,840
the greatest gain, I don't know what I'm about, which is interesting, that face

6
00:01:21,840 --> 00:01:34,520
value it seems a little odd, of course you can deny the fact that when you're sick

7
00:01:34,520 --> 00:01:42,840
it doesn't matter what you get, what else you get, what else you've got, you can

8
00:01:42,840 --> 00:01:57,640
be rich, you can surround it by friends, you can have guards at every door of

9
00:01:57,640 --> 00:02:09,040
when you're sick, it's all means that the most comfortable bed can become a torture

10
00:02:09,040 --> 00:02:31,440
drink, the greatest food can become poison, warms, and it's too hot, or you can be freezing

11
00:02:31,440 --> 00:02:38,640
cold in a hot room, it can be boiling up in a cold room,

12
00:02:38,640 --> 00:02:51,920
I've missed a lot, but there's more to that, more to this saying than just that, freedom

13
00:02:51,920 --> 00:03:05,600
from sickness, even from sickness means more than just, more than just physical sickness,

14
00:03:05,600 --> 00:03:24,880
we're all sick, we're all sick in body and mind, we've got to eat, we've got to drink,

15
00:03:24,880 --> 00:03:40,360
we've got to breathe, we've got all sicknesses, I don't warn you tonight's talk might

16
00:03:40,360 --> 00:04:00,520
be a little depressive, or humorous depending on how Buddhist you are, I don't even know

17
00:04:00,520 --> 00:04:15,200
if anyone's this one, you're all gone, in a way for so many days, anyway this gets

18
00:04:15,200 --> 00:04:29,920
recorded, so someone here eventually, maybe after I'm dead, we have to urinate and

19
00:04:29,920 --> 00:04:41,480
defecate, these are sicknesses, sicknesses, try and stop yourself from defecating, try

20
00:04:41,480 --> 00:04:53,960
and stop yourself from urinate, you know, but those are maybe pretty extreme, we don't

21
00:04:53,960 --> 00:04:59,840
really think of them as sicknesses, so put them aside that's fine, still have the sicknesses

22
00:04:59,840 --> 00:05:12,000
of getting cold and flu, they have rather Buddha, in the giri mananda supta, he detailed

23
00:05:12,000 --> 00:05:21,640
all these different kinds of sickness, everyone should read the giri mananda supta, that's

24
00:05:21,640 --> 00:05:34,280
really a good time, it's one part about various types of sicknesses, there's the eye,

25
00:05:34,280 --> 00:05:56,120
there's eye sicknesses, and ear sicknesses, and those sicknesses, body sicknesses, it's

26
00:05:56,120 --> 00:06:10,760
all kinds of diseases, and really if you've got any of these that don't really matter

27
00:06:10,760 --> 00:06:21,280
what else you've got, but even that isn't really freedom from sickness, even freedom

28
00:06:21,280 --> 00:06:27,880
from the many diseases that we can get, where we recognize this disease, this is a true

29
00:06:27,880 --> 00:06:29,880
freedom, right?

30
00:06:29,880 --> 00:06:36,640
Because then we have mental sickness, and most people think that they're, I think most

31
00:06:36,640 --> 00:06:42,280
of us think that most of the rest of the people around them are free from mental illness,

32
00:06:42,280 --> 00:06:49,760
but like that, if not most, many people will admit to themselves that they have mental

33
00:06:49,760 --> 00:06:55,560
illness themselves, even if they won't admit it to others, or everything about going

34
00:06:55,560 --> 00:07:03,120
to see a doctor about it, but we look around at everybody else and we think well, they

35
00:07:03,120 --> 00:07:05,040
look normal, right?

36
00:07:05,040 --> 00:07:12,400
It's funny, you can barely, if you're not looking carefully, you can't tell the difference

37
00:07:12,400 --> 00:07:24,040
between people who, even people who are taking medication from mental illness, they're

38
00:07:24,040 --> 00:07:37,200
almost indistinguishable, very hard to tell the difference, but even taking medication

39
00:07:37,200 --> 00:07:43,000
doesn't solve the problem, and taking medication, a person on medication still knows

40
00:07:43,000 --> 00:07:50,240
they've got a problem, they're not fixing it, they're just covering it, most anyway, and

41
00:07:50,240 --> 00:07:56,080
you know some people who think that medication solves their problems, it's just, well,

42
00:07:56,080 --> 00:08:06,760
that's another argument, but if I just go, just talk about it, just talk about the mental

43
00:08:06,760 --> 00:08:17,240
illness, well, the first of all, we should back up and let's detail it, go over the

44
00:08:17,240 --> 00:08:27,800
standard enumeration for sickness, we would have integrated four types, four types of sickness

45
00:08:27,800 --> 00:08:44,600
based on their origin, we've got a sickness from the environment, and we've got sickness

46
00:08:44,600 --> 00:08:53,000
that comes from food, we've got sickness that comes from the mind and sickness that comes

47
00:08:53,000 --> 00:08:58,880
from karma, maybe not in that order, actually this may not have come to the Buddha

48
00:08:58,880 --> 00:09:07,360
camp remember, maybe in the commentaries, really sure it's from the Buddha, yeah, definitely

49
00:09:07,360 --> 00:09:28,980
just, so the first two are more physical, sickness comes from, from the environment, normally

50
00:09:28,980 --> 00:09:36,880
we understand this as viruses, bugs that you get when you touch the same door knob that

51
00:09:36,880 --> 00:09:50,960
someone else who had to stick inside of it, some of coughs into your face, and you kind

52
00:09:50,960 --> 00:09:58,440
of communicable disease, that it can also be radiation poisoning, lead or water, that's

53
00:09:58,440 --> 00:10:08,080
maybe from food, yeah, anything from the environment, now let's say water poisoning is

54
00:10:08,080 --> 00:10:18,440
also from the environment, these people in Michigan are about water poisoning, not the government

55
00:10:18,440 --> 00:10:40,840
didn't do anything about it, very bad karma, all that kind of sickness comes from the environment,

56
00:10:40,840 --> 00:10:44,080
even though I would say some kind of sickness that comes from food you could call environment

57
00:10:44,080 --> 00:10:51,600
or sickness instead, like food, like a lot of cancer and stuff, probably comes from chemicals,

58
00:10:51,600 --> 00:11:04,840
pesticides that gets in the food, gets into plastic and all that, excuse me, our meat

59
00:11:04,840 --> 00:11:12,440
is painted and people eat, liver that has mercury I think, or fish that has also some

60
00:11:12,440 --> 00:11:21,120
mercury and stuff, that's environment to poison, but food poisoning, poisoning from food,

61
00:11:21,120 --> 00:11:28,960
or sickness that comes from food, I suppose bacteria could be included there, but I would

62
00:11:28,960 --> 00:11:39,480
say this mostly deals with overeating or eating fatty foods, you know, diabetes, heart

63
00:11:39,480 --> 00:11:58,200
disease, obesity, these aren't so important, it's much more important, it's the third

64
00:11:58,200 --> 00:12:11,800
one, mind, because it comes from the mind, because your body can be sick but your mind can

65
00:12:11,800 --> 00:12:16,960
be okay, the other hand if your mind is sick, your body is not likely to stay okay, mind

66
00:12:16,960 --> 00:12:28,080
can hurt the body, can the body hurt the mind, I suppose, but not directly, depends on

67
00:12:28,080 --> 00:12:41,760
the mind, so mind is like an open sore, and then the body can harm the mind, the mind

68
00:12:41,760 --> 00:12:54,280
has to be reactionary, has to be reactive, the mind is strong or wise, the mind is free

69
00:12:54,280 --> 00:13:15,280
from clinging in the body has no effect, even if the body is sick and pain, even if you're

70
00:13:15,280 --> 00:13:30,560
dying, it can be a piece, this is what we aim to accomplish, this is actually relates to

71
00:13:30,560 --> 00:13:35,600
the fourth one, because sickness that comes from karma actually comes from the mind

72
00:13:35,600 --> 00:13:46,080
of the first place, but if it's indirect, the mind makes the body sick, that's karma,

73
00:13:46,080 --> 00:13:54,160
and you come and evil and just a person, you get sick, it's immediately for no reason,

74
00:13:54,160 --> 00:14:00,360
some people have very strange sicknesses that doctors can't figure out the cause for

75
00:14:00,360 --> 00:14:17,480
a who knows, we would argue that maybe, maybe some of them are caused by a karma, maybe

76
00:14:17,480 --> 00:14:22,880
some cancer is caused by this as well, because cancer is weird, sickness just seems to

77
00:14:22,880 --> 00:14:28,440
pop up, and sometimes it doesn't seem to go away, you can't rid of it and then just

78
00:14:28,440 --> 00:14:39,520
don't know where it pops up again, mysterious, it's almost as though you were born with

79
00:14:39,520 --> 00:15:00,240
some pre, pre, pre, pre, in connection to our cancer.

80
00:15:00,240 --> 00:15:03,920
So the mind is the most important, when we talk about freedom from sickness, we really

81
00:15:03,920 --> 00:15:17,640
mean a state of being free from the fires of greed and anger and delusion of the fevers,

82
00:15:17,640 --> 00:15:34,720
of the violence that inflame the mind, it's a not easy, not easy to overcome, I mean

83
00:15:34,720 --> 00:15:40,520
now, physical sickness, what's it gonna last for, what's the worst physical sickness

84
00:15:40,520 --> 00:15:50,000
gonna last for, these days doesn't last more than a year, five years, day outside, ten

85
00:15:50,000 --> 00:15:57,120
years, twenty years, fifty years, there's no sickness that lasts a hundred years, not

86
00:15:57,120 --> 00:16:07,520
that I know of, except for life, I guess life sometimes lasts, call back a sickness, sickness

87
00:16:07,520 --> 00:16:16,400
of hunger, sickness of thirst, these last, beyond that, that's it, the sickness in the

88
00:16:16,400 --> 00:16:28,520
mind is something we carry with us, carry with us to help, carry with us from life to

89
00:16:28,520 --> 00:16:34,720
life, and then we complain about how hard it is to get rid of these sickness, come

90
00:16:34,720 --> 00:16:41,760
to meditate, oh, a good practice thing, or a week already, why am I still getting angry,

91
00:16:41,760 --> 00:16:48,240
why am I still getting, why am I still attached to all these things, the practice will

92
00:16:48,240 --> 00:17:02,880
grow, or a year, well, I mean not to say that you have to equal it out, I have to somehow

93
00:17:02,880 --> 00:17:10,880
practice and equal number of years as you've been greedy or angry or deluded, but you

94
00:17:10,880 --> 00:17:18,320
have to keep things in proportion and building, we're building new habits, and new habits

95
00:17:18,320 --> 00:17:31,880
are stronger, because they're more immediate, but they're also weaker because they're

96
00:17:31,880 --> 00:17:45,120
very, they're only, they're only recent, so it's important not to lose hope, it's

97
00:17:45,120 --> 00:17:54,080
not, they just look down upon the fact that we've been working hard, we shouldn't ignore

98
00:17:54,080 --> 00:18:06,640
that, yes, we work hard, and that work isn't in vain, but it also worked hard and being

99
00:18:06,640 --> 00:18:13,480
greedy and angry and deluded, and done a good job cultivating those, we've had a lot more

100
00:18:13,480 --> 00:18:28,160
practice at that, so that's how we need this practice, practice, practice, every moment, every

101
00:18:28,160 --> 00:18:40,200
moment of mindfulness that's feeding into the habit of mindfulness, and I always start, never

102
00:18:40,200 --> 00:18:58,800
do that, anyway, so that sickness often led to more of the cough will be gone, but I'm

103
00:18:58,800 --> 00:19:09,040
also very behind in school, that this crazy idea to go back to school and study, study

104
00:19:09,040 --> 00:19:19,320
things, I'm sure how that's working out, but there's a piece symposium, like Master,

105
00:19:19,320 --> 00:19:29,760
that I'm hoping organized, like peace studies, that I'm encouraging peace, let's see how that goes,

106
00:19:29,760 --> 00:19:41,000
I'm starting a lot of sutra, I'm going to try to do an essay on the Pulima gaspects

107
00:19:41,000 --> 00:19:50,880
of a lot of sutra, because I like getting in trouble, the, my professor actually increasingly

108
00:19:50,880 --> 00:19:56,960
thinks I'm getting into topic, what's interesting is a lot of sutra never really got, apparently

109
00:19:56,960 --> 00:20:05,840
never really getting traction in India, but gained a lot of traction in East Asia, and

110
00:20:05,840 --> 00:20:17,360
what's interesting about that is, is that it was very much directed at mainstream Indian

111
00:20:17,360 --> 00:20:25,080
Buddhism, so the question is, how is it that when it moved to China, where there was

112
00:20:25,080 --> 00:20:32,080
no Buddhism, it became a hit, and I guess maybe what I'm going to look at is whether it became

113
00:20:32,080 --> 00:20:38,960
a hit for the wrong reasons, like how we in reinterpret religious texts in ways that they

114
00:20:38,960 --> 00:20:44,960
were never meant to be interpreted, because I'm told that I'm interpreting the lotus

115
00:20:44,960 --> 00:20:50,480
sutra completely wrong, which is funny, because I wonder if the people who are telling

116
00:20:50,480 --> 00:20:56,240
me that are actually, well, you know, we each interpret things our own way, and who

117
00:20:56,240 --> 00:21:03,680
is disabled is the right way, but maybe that is a clear, because take for example the

118
00:21:03,680 --> 00:21:13,360
Old Testament, and how the Old Testament was interpreted by the New Testament authors,

119
00:21:13,360 --> 00:21:21,760
to say that to be, to prophesize Jesus Christ, the Old Testament, according to Christians,

120
00:21:21,760 --> 00:21:27,480
or the Christian authors of the New Testament, was supposed to prophesize the coming

121
00:21:27,480 --> 00:21:32,760
of Jesus, there's a lot of passages that must have to do that, but according to biblical

122
00:21:32,760 --> 00:21:43,760
scholars, that's almost ridiculous, or it is a ridiculous claim, I mean, unless you

123
00:21:43,760 --> 00:21:50,000
are willfully ignorant in terms of saying, you know, well, each interpretation is equally

124
00:21:50,000 --> 00:21:58,560
valid, it's just not true. The Old Testament was clearly meant in what we call the Old

125
00:21:58,560 --> 00:22:06,360
Testament was clearly written by a diverse group of authors, but it was also written for

126
00:22:06,360 --> 00:22:14,800
clearly political and specific reasons. I mean, the point being, there's no way that I

127
00:22:14,800 --> 00:22:21,440
didn't need to do with prophesizing the coming of Jesus Christ. Just an example, believe

128
00:22:21,440 --> 00:22:30,160
it or not, that's what, so the question is whether the lotus sutras in a similar state,

129
00:22:30,160 --> 00:22:40,080
whether I just like to look at, I guess, how the Chinese teachers did, and maybe even

130
00:22:40,080 --> 00:22:46,400
do today interpret those passages of the lotus sutras that to me as an Indian Buddhist

131
00:22:46,400 --> 00:22:56,680
scholar, clearly clearly aimed at denouncing Indian Buddhism, which was totally unbeknownst.

132
00:22:56,680 --> 00:23:04,400
I mean, it was totally unknown to the people who came in contact with the lotus sutras,

133
00:23:04,400 --> 00:23:14,160
I think. But at any rate, not, not, sorry, don't mean that I'm confused, it's just

134
00:23:14,160 --> 00:23:20,560
that it's not that it was unknown, it's that it had no place. So when the lotus sutras

135
00:23:20,560 --> 00:23:29,120
and I guess teaching was like it came to China, they weren't reacting. They could be taken

136
00:23:29,120 --> 00:23:35,000
completely at face value, which I guess my argument is they weren't meant to be taken

137
00:23:35,000 --> 00:23:45,000
at face value, or they weren't intended, or they weren't expected to be taken at face value.

138
00:23:45,000 --> 00:24:03,200
They were expected to create controversy, because they were stepping into a realm of Buddhist

139
00:24:03,200 --> 00:24:13,200
orthodoxy. So I don't know where that came from, it must be the sickness talking.

140
00:24:13,200 --> 00:24:34,640
Anyway, I'm studying Latin as well. Studying Latin when you're sick is not the most

141
00:24:34,640 --> 00:24:46,120
important thing, but I'm going to go. That's all for tonight. Thank you all for tuning in.

