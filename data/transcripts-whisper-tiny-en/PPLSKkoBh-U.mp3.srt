1
00:00:00,000 --> 00:00:09,240
So here's a question, is there a sort of Buddhist approach when talking to people who accept

2
00:00:09,240 --> 00:00:15,040
that we look at, expect that we look at them and see the illusion they are creating as

3
00:00:15,040 --> 00:00:18,520
being the essence, may be best to just avoid?

4
00:00:18,520 --> 00:00:26,280
It's funny, this question came up today and it's come up recently, something I've been

5
00:00:26,280 --> 00:00:30,120
thinking about.

6
00:00:30,120 --> 00:00:37,040
Well sort of to beat around the bush, to flush it out a little bit.

7
00:00:37,040 --> 00:00:45,240
I think it's quite interesting and the worth examining the fact that in general this is

8
00:00:45,240 --> 00:00:53,280
what we do and this is why people who have done meditation practice find it very

9
00:00:53,280 --> 00:01:01,720
tired, tiresome, and often become very agitated or distressed or upset when they have to

10
00:01:01,720 --> 00:01:06,240
be around people again.

11
00:01:06,240 --> 00:01:09,800
And often people get the impression that when they've gone to practice meditation they come

12
00:01:09,800 --> 00:01:14,000
back and they're something wrong with them.

13
00:01:14,000 --> 00:01:18,240
They start to doubt their practice and everyone else really certainly doubts their

14
00:01:18,240 --> 00:01:21,920
practice and said, what have you done, you've gone and brainwashed yourself, you're not the

15
00:01:21,920 --> 00:01:25,200
same person you are.

16
00:01:25,200 --> 00:01:30,480
And they can't be in social situations, they feel awkward and agitated and so on.

17
00:01:30,480 --> 00:01:35,680
But the interesting thing here, and I think the answer that should clarify it up quite

18
00:01:35,680 --> 00:01:43,480
a bit, is that that's because they're no longer, I want to use a swear word, but they're

19
00:01:43,480 --> 00:01:51,440
no longer BSing themselves, they're no longer lying to the other person or lying to themselves,

20
00:01:51,440 --> 00:01:57,800
they're no longer pretending, they're no longer buying into it or at that time they're

21
00:01:57,800 --> 00:01:58,800
not.

22
00:01:58,800 --> 00:02:05,480
Now if they go on and on, eventually they're going to A, either buy back into it, which

23
00:02:05,480 --> 00:02:11,480
should be the case if they haven't done really intensive meditation or haven't really

24
00:02:11,480 --> 00:02:16,280
come to change the way their mind works, or they're going to become more skillful out

25
00:02:16,280 --> 00:02:24,200
of voiding problems, more skillful in dealing with the situation, because it's not easy,

26
00:02:24,200 --> 00:02:32,040
it's not easy to be true to yourself and still not have people think of you as a freak.

27
00:02:32,040 --> 00:02:40,600
When I first started meditating, when I came back home, I wasn't, I was sure that I had

28
00:02:40,600 --> 00:02:45,760
found something great and that this was what I wanted to do, but I couldn't figure out

29
00:02:45,760 --> 00:02:50,440
how to incorporate it into my life, I mean it was, it was difficult.

30
00:02:50,440 --> 00:02:58,120
Here I was surrounded by people who, from my point of view, were crazy, were all messed

31
00:02:58,120 --> 00:03:06,320
up, but when they looked at me, I was the one who was totally messed up to them.

32
00:03:06,320 --> 00:03:13,400
So when they would yell and when they would scold and complain about me, call me, say

33
00:03:13,400 --> 00:03:18,320
all these bad things, you're like a zombie, why don't you, can't you see that you've

34
00:03:18,320 --> 00:03:23,560
been brainwashed because I was on, I would just close my eyes and go back to meditating,

35
00:03:23,560 --> 00:03:30,040
rising, falling and oh they would get so angry.

36
00:03:30,040 --> 00:03:34,480
But it was like that, it was like, it was like, I know what I'm doing is right, so I'll

37
00:03:34,480 --> 00:03:42,280
just go back to doing it, and it created all sorts of difficulties and problems.

38
00:03:42,280 --> 00:03:46,120
So that's not answering your question, but hopefully it's kind of setting the tone here

39
00:03:46,120 --> 00:03:50,000
for what we're talking about.

40
00:03:50,000 --> 00:03:55,720
The approach I think is just something you learn over time, and rather than talk about

41
00:03:55,720 --> 00:04:02,040
what is the actual approach, so maybe I can give some tips, I think it's more important

42
00:04:02,040 --> 00:04:11,040
to just caution that as you learn how to deal with people, that don't buy into the idea

43
00:04:11,040 --> 00:04:20,160
that most important is to fit in, because this is where we lie to ourselves and to

44
00:04:20,160 --> 00:04:21,320
other people.

45
00:04:21,320 --> 00:04:28,920
Most of our interactions are just, oh, some people want us to say certain things, they'll

46
00:04:28,920 --> 00:04:31,840
talk about how great they are, and of course they want you to say, oh yes, you're

47
00:04:31,840 --> 00:04:36,600
right, right, and this is why people fight, because one person says I'm great and the other

48
00:04:36,600 --> 00:04:41,800
person doesn't say, oh you're great, the other person says, oh yeah, well, I've done better

49
00:04:41,800 --> 00:04:45,200
than that, and then they get angry and they fight, right, because who wants to sit there

50
00:04:45,200 --> 00:04:50,240
and compliment another person all day, right, we have our own defilements and we get bored

51
00:04:50,240 --> 00:04:52,520
and what do you see?

52
00:04:52,520 --> 00:04:59,480
So the only way to fit in with each other is to lie and to play up to each other's

53
00:04:59,480 --> 00:05:04,120
defilements, otherwise it leads to fighting and argument, that's why families end up

54
00:05:04,120 --> 00:05:10,760
by family members end up fighting a lot, or can end up fighting because they stop playing

55
00:05:10,760 --> 00:05:14,600
up to each other's defilements, and they go the other way and they get bored of it

56
00:05:14,600 --> 00:05:27,000
and they don't feel the need to coddle the other person or to pander, what is the word?

57
00:05:27,000 --> 00:05:35,800
And so they get into argument, so don't buy into it and watch yourself very carefully

58
00:05:35,800 --> 00:05:43,720
because it's very, it's a lot easier to just lie and not exactly tell lies, but become

59
00:05:43,720 --> 00:05:53,240
fake again and to pretend and to convince yourself that yeah, you do enjoy these stupid

60
00:05:53,240 --> 00:06:00,000
things that people are talking about and go back to talking about useless things and

61
00:06:00,000 --> 00:06:03,640
if you still have the craving for those things then yeah, actually getting involved in

62
00:06:03,640 --> 00:06:14,400
talking about them, so you have to learn a way to not do that and I think the easiest

63
00:06:14,400 --> 00:06:20,520
or the most important part and again it's easier for me because I'm not in these situations,

64
00:06:20,520 --> 00:06:26,160
I don't, I'm able to avoid them, you know, I wear the robe and people like oh well it's

65
00:06:26,160 --> 00:06:28,160
okay, he's a monk, right?

66
00:06:28,160 --> 00:06:32,440
If he doesn't talk, if he doesn't do this or that then well we understand because he's

67
00:06:32,440 --> 00:06:38,360
a monk and we may not agree but there's no expectation there, so they won't get angry

68
00:06:38,360 --> 00:06:46,840
at me when I say I can't go dancing or so on, but I think the important part is to keep

69
00:06:46,840 --> 00:06:54,360
quiet and to let, to only talk when people have questions for you and to keep your answers

70
00:06:54,360 --> 00:07:00,920
simple and to be more of an example than a teacher because every time you open your mouth

71
00:07:00,920 --> 00:07:04,080
you're going to say something that makes you sound funny, right?

72
00:07:04,080 --> 00:07:07,920
You're going to say something that is totally out of whack with what they, they'll be like

73
00:07:07,920 --> 00:07:12,480
hey, don't you think it's great, don't you think this movie is great or don't you think

74
00:07:12,480 --> 00:07:17,600
this, they show you something isn't this beautiful, right? I know, like what do I say?

75
00:07:17,600 --> 00:07:23,480
I mean, maybe I'm attached, attracted to it, but I know that beauty is this illusion and

76
00:07:23,480 --> 00:07:29,360
it only is based on, on attachment and I don't want to be attached to things.

77
00:07:29,360 --> 00:07:35,800
So what do you say? This was always funny, people ask in Thailand, they love to ask,

78
00:07:35,800 --> 00:07:39,560
how's the food? They don't, they don't ask how's the food, they say it's delicious.

79
00:07:39,560 --> 00:07:45,840
And what do you say? I mean, if you say it's delicious, it's like encouraging or it's,

80
00:07:45,840 --> 00:07:49,720
it's really embarrassing to have to say that something's delicious. Well, yeah, I like it,

81
00:07:49,720 --> 00:07:55,040
but that's my own attachment. That's not a good thing. It's not a compliment to say

82
00:07:55,040 --> 00:08:01,640
your food is delicious, it's admitting my own, my own fault.

83
00:08:01,640 --> 00:08:14,880
So yeah, it's difficult, it's difficult. I would say, don't, the most important thing,

84
00:08:14,880 --> 00:08:19,480
because eventually you'll learn how to do it, eventually you'll find a way to fit in.

85
00:08:19,480 --> 00:08:25,680
Meditation changes who you are. So it upsets that routine or it upsets your habit, it's

86
00:08:25,680 --> 00:08:32,040
scrambling the eggs, right? You can't make an omelette without breaking the eggs, right?

87
00:08:32,040 --> 00:08:37,200
So here we're breaking the eggs. The breaking the eggs is the bad part, but once you get used

88
00:08:37,200 --> 00:08:44,040
to this new way, you'll eventually find a life that works with it. For many people eventually

89
00:08:44,040 --> 00:08:50,080
that'll be becoming a monk for sure it will be. Either that or they'll just plot along,

90
00:08:50,080 --> 00:08:58,080
and people will go back and give up their meditation practice in many ways. But to do it

91
00:08:58,080 --> 00:09:04,880
in, you know, eventually even in your own life, you will come up with a way, and some

92
00:09:04,880 --> 00:09:10,520
things will change. Maybe you'll lose a lot of friends. I lost a lot of friends. I don't

93
00:09:10,520 --> 00:09:18,720
have any of my old friends left. Maybe one or two. But I have a lot of new friends, and

94
00:09:18,720 --> 00:09:23,920
so your life will change. I guess that's another advice I would give is not to be afraid

95
00:09:23,920 --> 00:09:34,280
of change. And never believe, never delude yourself in the thinking that you have to,

96
00:09:34,280 --> 00:09:38,400
you have some connection to this or that person. When it's time to let them go, let them

97
00:09:38,400 --> 00:09:47,440
go. And your environment will change. The people you surround yourself with, the people

98
00:09:47,440 --> 00:09:56,160
who come to connect with you will change. We look at who my friends are. You're all

99
00:09:56,160 --> 00:10:04,720
my friends. Some of you have never even met. And the people here, Manju is my friend.

100
00:10:04,720 --> 00:10:10,400
Manju is this guy who helps in the monastery. And who would have ever thought that he

101
00:10:10,400 --> 00:10:17,680
and I are. We spend a lot of time together. And we're from two totally different cultures.

102
00:10:17,680 --> 00:10:24,760
But that's life. Maybe we have some connection in the past. Maybe we don't. I don't know.

103
00:10:24,760 --> 00:10:29,880
What's important is that we're here now. And I'm not going to try to change that. I would

104
00:10:29,880 --> 00:10:34,500
never look at Manju and say, oh, he's just a worker here. My real friends are back in

105
00:10:34,500 --> 00:10:41,200
Canada or so on. And say, no, here I am with Manju and he's a person. And he may not be

106
00:10:41,200 --> 00:10:53,320
meditating, but I will give him my attention and try to do my best in regards to his life,

107
00:10:53,320 --> 00:11:03,080
in regards to our relationship, because it's here now. For sure, moving from one country

108
00:11:03,080 --> 00:11:07,600
to another, it really opens you up to these kind of things.

