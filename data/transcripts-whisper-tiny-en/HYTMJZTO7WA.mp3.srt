1
00:00:00,000 --> 00:00:19,140
Okay, good evening everyone, welcome to our evening dumber, tonight we are looking at

2
00:00:19,140 --> 00:00:42,840
the right speech, so we've finished with wisdom and we're moving on to morality, but it does

3
00:00:42,840 --> 00:00:54,240
say in one of the sootas that these are the two have to go together, even in the very beginning.

4
00:00:54,240 --> 00:01:02,460
So when you're going to practice mindfulness, when you're going to practice meditation, first

5
00:01:02,460 --> 00:01:09,440
you have to do two things, you have to establish yourself in morality and you have to straighten

6
00:01:09,440 --> 00:01:18,420
out your view. So if you come to the practice with all sorts of wrong views, or if you don't

7
00:01:18,420 --> 00:01:26,940
have proper morality, mindfulness isn't going to work. And in a way you can see that is

8
00:01:26,940 --> 00:01:34,100
adapting the full noble path to his discourse, so you can see that the full noble, he's

9
00:01:34,100 --> 00:01:41,940
describing the full noble path there, how they all three of them have to work together.

10
00:01:41,940 --> 00:01:49,980
But most, most expositions of the eightfold path or of the training begin with morality, because

11
00:01:49,980 --> 00:01:57,700
technically morality has to come first. If your mind is not organized, if you're distracted

12
00:01:57,700 --> 00:02:08,020
by unholesomeness, then externally, then you can't possibly cultivate wholesomeness internally.

13
00:02:08,020 --> 00:02:20,020
Your mind will be concentrated and wisdom won't arise. So before we even begin to meditate,

14
00:02:20,020 --> 00:02:25,100
we have to be clear about the things that are going to get in the way of our practice.

15
00:02:25,100 --> 00:02:31,340
Which in actions, this is the first speech actions, even livelihood, these things will

16
00:02:31,340 --> 00:02:37,740
get in the way of our practice. But it's also important to note that during the practice,

17
00:02:37,740 --> 00:02:46,500
there's no question of right, there's no question of wrong speech, wrong action, or

18
00:02:46,500 --> 00:02:54,820
wrong livelihood. So on the one hand, it refers to this preliminary path of rearranging

19
00:02:54,820 --> 00:03:00,220
your life so you're no longer killing and stealing and so on. You're still doing bad things

20
00:03:00,220 --> 00:03:07,300
and you change your livelihood up so that it's more in line with the meditation practice.

21
00:03:07,300 --> 00:03:14,580
But on the other hand, it's just describing another aspect on the noble aspect of thing.

22
00:03:14,580 --> 00:03:21,700
It's just describing another part of the purity, meaning that a person who or the mind

23
00:03:21,700 --> 00:03:33,100
that attains the noble path is through its purity, unable to incline towards wrong speech

24
00:03:33,100 --> 00:03:37,100
and wrong action. So when you're sitting in meditation, obviously you're running

25
00:03:37,100 --> 00:03:44,020
incline towards all these things as well. Once you practice and progress until you reach

26
00:03:44,020 --> 00:03:52,380
the manga and the knowledge of the path, at that moment there's no, there's a mind that's

27
00:03:52,380 --> 00:03:58,580
so pure that the idea of wrong speech, wrong action, it's not possible. So they call that

28
00:03:58,580 --> 00:04:08,060
a specific, there's a specific data, a aspect of the mind. But it's just technical.

29
00:04:08,060 --> 00:04:14,580
It's quite obvious that at that moment there's no opportunity to speak or to act. It's

30
00:04:14,580 --> 00:04:20,380
just describing another aspect of the purity. Nonetheless, for practical purposes, speeches

31
00:04:20,380 --> 00:04:28,060
of course are very important. So the Buddha outlined four types of wrong speech that

32
00:04:28,060 --> 00:04:37,780
we were framed from. Just in general to answer the question of why do we have speech

33
00:04:37,780 --> 00:04:45,260
and action, because speech is an action, isn't it? Physically speaking from a, from

34
00:04:45,260 --> 00:04:52,700
a physicalist point of view, of course it's the same thing, but mentally it's quite different.

35
00:04:52,700 --> 00:05:00,700
Speech is, is the physical act of speech is a very specific act. It has less to do with

36
00:05:00,700 --> 00:05:12,900
what we're doing than about what we're trying to convey. Speech is the act of transmitting

37
00:05:12,900 --> 00:05:26,980
information, transmitting facts, transmitting ideas, some means of transmitting information.

38
00:05:26,980 --> 00:05:31,180
So it's means of connecting with other people in a very specific way, not just in the

39
00:05:31,180 --> 00:05:37,740
sense of when you hit someone, you're transmitting pain. That's quite different from actually

40
00:05:37,740 --> 00:05:45,020
transmitting information. You're trying to pass on information, usually in the form of

41
00:05:45,020 --> 00:05:59,580
facts. And so there's another level of interaction more than just hitting someone. When

42
00:05:59,580 --> 00:06:04,340
you're talking to someone, you're actually engaging with their mind and not just engaging

43
00:06:04,340 --> 00:06:17,660
with their body, and your purpose is not to make their, your drums vibrate. Your purpose is

44
00:06:17,660 --> 00:06:31,940
to provide information, to affect their mind in a very specific and high level way, anyway.

45
00:06:31,940 --> 00:06:39,220
Why this is important, why I make the whole of bother to express that difference, is because

46
00:06:39,220 --> 00:06:50,220
it highlights the significance of speech, that because you're transmitting facts and information,

47
00:06:50,220 --> 00:06:58,940
there's another sort of potential for harm. If you hit someone well, it's fairly easy

48
00:06:58,940 --> 00:07:06,940
to be to react to that. But if you tell someone a lie, which is, of course, one of the

49
00:07:06,940 --> 00:07:16,940
four bad types of speech, it's a whole other level of twisted and complicated. It's a

50
00:07:16,940 --> 00:07:23,620
whole other type of harm. It's a very abstract sort of harm. If you tell someone a lie,

51
00:07:23,620 --> 00:07:31,900
when they believe it, you've affected their understanding of reality. You've distorted

52
00:07:31,900 --> 00:07:40,500
their understanding of reality. So there are four types of wrong speech. Lying is the first

53
00:07:40,500 --> 00:07:49,020
one. When we talk about lying, the question always comes up about withholding the truth,

54
00:07:49,020 --> 00:07:52,820
or situations where it might not be best to tell the truth. There's a Zen story that's

55
00:07:52,820 --> 00:08:00,700
really kind of odd to me. I mean, it seems to be not to speak badly of Zen, but this

56
00:08:00,700 --> 00:08:09,820
story in particular is, it's meant to illustrate how sometimes you have to good people should

57
00:08:09,820 --> 00:08:17,260
do bad things, should break the precepts, should lie in this instance. So this hunter is chasing

58
00:08:17,260 --> 00:08:24,980
after a deer, let's say, and there's a monk who's sitting in the forest, and he sees

59
00:08:24,980 --> 00:08:33,020
the deer and go by, and it goes down one path. And the hunter comes along and asks the

60
00:08:33,020 --> 00:08:40,180
monk, where did the deer? Did you see a deer here? Where did the deer see where that

61
00:08:40,180 --> 00:08:46,700
deer went? And the monk says, the monk should say, is what the story says. The deer

62
00:08:46,700 --> 00:08:53,220
went down that way when, of course, the deer went that way. I mean, this does illustrate

63
00:08:53,220 --> 00:08:58,060
a difference of opinion among Buddhists. For some Buddhists, that's the correct answer,

64
00:08:58,060 --> 00:09:15,060
and their point being that the rules are meaningless when it involves compassion. And why

65
00:09:15,060 --> 00:09:24,020
this story is strange to me is because there's no need to say anything. And there's a good

66
00:09:24,020 --> 00:09:28,420
example of what we mean by the difference between lying and withholding the truth. You don't

67
00:09:28,420 --> 00:09:33,700
have to tell the hunter where the deer went at all. In fact, it's as skillful, but what

68
00:09:33,700 --> 00:09:41,100
the Buddha would do, as far as I can figure, is something like engage with the man. There's

69
00:09:41,100 --> 00:09:47,900
a story where these monks were, sorry, these men, when the Buddha was newly enlightened.

70
00:09:47,900 --> 00:09:55,620
He was wandering on his way to Uru Wala. And he met these, he was sitting in the forest

71
00:09:55,620 --> 00:10:05,340
and he's 30 young men, large group of rich men, young men, came upon him in the forest

72
00:10:05,340 --> 00:10:14,180
and asked if he'd seen this woman. Now, this woman was a courtesan, an escort, I think

73
00:10:14,180 --> 00:10:21,100
we'd call them. And 39 of these men had wives, and they brought their wives with them out

74
00:10:21,100 --> 00:10:27,220
to a sport in the forest. And the 30th didn't have a wife, so they got a courtesan for him.

75
00:10:27,220 --> 00:10:30,980
And while they were all doing their thing, maybe they were sleeping, or maybe they were

76
00:10:30,980 --> 00:10:37,580
off swimming or something, this courtesan picked up all their jewels and ran away with them.

77
00:10:37,580 --> 00:10:42,060
And so they were looking for them, and they came upon the Buddha, and they said, have

78
00:10:42,060 --> 00:10:47,500
you seen this woman? Have you seen a woman going through the forest? And the Buddha says

79
00:10:47,500 --> 00:10:53,620
to the 30 men, he says, what do you think is it better to search after this woman or

80
00:10:53,620 --> 00:11:02,580
better to search after yourselves? And they said, better to search after ourselves. Of

81
00:11:02,580 --> 00:11:08,180
course. And the Buddha taught them the dumb and they all became our odds. They became monks.

82
00:11:08,180 --> 00:11:19,820
They were one of the first monks of the Sangha, first our odds, first group. So I think

83
00:11:19,820 --> 00:11:26,620
something along those lines. And this is an answer to this idea of, what about when telling

84
00:11:26,620 --> 00:11:32,140
the truth will be harmful? Do I look fat in this dress? Well, if they do, maybe don't

85
00:11:32,140 --> 00:11:39,780
want to say yes. But you can find some skillful way of saying it looks fine, or smiling

86
00:11:39,780 --> 00:11:45,220
at them and reassuring them. You don't have to lie to people. You shouldn't lie. I mean,

87
00:11:45,220 --> 00:11:49,580
and it's important, this distinction is important. It seems like nitpicking and splitting

88
00:11:49,580 --> 00:11:56,900
hairs. But if I say to you, if you say to me, do I look fat in this dress? And I look

89
00:11:56,900 --> 00:12:02,740
at you and indeed that dress somehow distorts your figure to make you look fat or then

90
00:12:02,740 --> 00:12:12,940
without when you're wearing something else? If I say to you, it looks fine, then you know,

91
00:12:12,940 --> 00:12:16,940
you may get upset later on when someone else tells you, no, no, actually it makes you

92
00:12:16,940 --> 00:12:23,740
look fat. There's a silly example, but this kind of thing, right? If you, if you

93
00:12:23,740 --> 00:12:32,780
equivocate, it might make people angry. But if you actually say to them, no, when the dress

94
00:12:32,780 --> 00:12:37,140
value is a subjective thing, it's not really a good example. But if you actually tell

95
00:12:37,140 --> 00:12:44,740
someone, something that's a lie, like if this man says, if this monk says, look, I don't

96
00:12:44,740 --> 00:12:51,340
want to wear the rat. I don't know where the deer is. But, you know, really, for me,

97
00:12:51,340 --> 00:12:56,820
it's a horrible thing or, you know, I wonder about this hunting, you know, something

98
00:12:56,820 --> 00:13:05,620
had something mindful, engaging the man and asking him what he's up to and trying

99
00:13:05,620 --> 00:13:09,460
to engage with the person so that they're able to get a sense of the horror of what

100
00:13:09,460 --> 00:13:15,260
they're doing. It's quite different from, you know, lying to the person, which can get

101
00:13:15,260 --> 00:13:24,060
you a lot to trouble. And again, it's a distortion. Lying as a rule by principle is against

102
00:13:24,060 --> 00:13:29,620
the Buddha's teaching because of this, because it distorts reality. You have to remember

103
00:13:29,620 --> 00:13:36,140
that in Buddhism, we're very much concerned about truth. It's the core of Buddhism,

104
00:13:36,140 --> 00:13:47,740
truth, knowledge, clarity. When you lie, you distort that. You dishonor truth and poetic

105
00:13:47,740 --> 00:13:56,340
way of saying it. So we don't lie as a rule. It's inconvenient sometimes. It certainly

106
00:13:56,340 --> 00:14:04,140
is. And a lot of the moral precepts are inconvenient. They do have, there is an awkwardness

107
00:14:04,140 --> 00:14:13,740
at times about them. And so it is, there are times where keeping the precepts is actually

108
00:14:13,740 --> 00:14:20,740
going to disturb your concentration. It's going to make you flustered, right? See, if it

109
00:14:20,740 --> 00:14:26,180
would be really easy to just tell a lie and go back to your practice. But then you've

110
00:14:26,180 --> 00:14:34,900
done that, you know, easy is breaking all the precepts makes things easy at one time or another.

111
00:14:34,900 --> 00:14:39,900
I mean, easy is not what we're looking for. Easy is not the path to enlightenment. It's

112
00:14:39,900 --> 00:14:47,620
not the path to clarity and purity of mind, not necessarily. It's not just because something

113
00:14:47,620 --> 00:14:53,820
is easy makes it good, makes it right. Even because things calm you down or pass it by

114
00:14:53,820 --> 00:15:05,340
a situation. Sometimes you have to break some eggs in order to make an omelet. Because

115
00:15:05,340 --> 00:15:11,900
ultimately we're concerned with this, the bigger picture of this cohesion of mind, cohesion

116
00:15:11,900 --> 00:15:20,660
of our universe. When we lie, we distort that, we destroy that. And it comes back, it

117
00:15:20,660 --> 00:15:25,580
affects your mind, you become a liar. And that's a part of who you are, and it affects

118
00:15:25,580 --> 00:15:30,460
your personality, it affects your state of mind. You're no longer honoring and upholding

119
00:15:30,460 --> 00:15:41,340
the truth. You're no longer steadfast, committed to the truth. I mean, karma is real. There

120
00:15:41,340 --> 00:15:48,780
is an aspect of reality, whereby our actions and our speech affect who we are and

121
00:15:48,780 --> 00:15:54,860
affect our state of mind. We're constantly creating karma. And so, lying is one extreme

122
00:15:54,860 --> 00:16:08,060
example of where we distort and disturb reality. The second type of wrong speech is

123
00:16:08,060 --> 00:16:17,940
harsh speech. So we talk about harsh speech. This is a speech that is out based on anger.

124
00:16:17,940 --> 00:16:24,300
Here he gives some examples. You're a goat. You're a pig. You're a buffalo, I think. I don't

125
00:16:24,300 --> 00:16:29,140
know what they are. I think you're a buffalo is one of them. In Thailand, that's a real

126
00:16:29,140 --> 00:16:43,540
insult. Calling someone a buffalo. You know, I'll answer insults. When you insult someone,

127
00:16:43,540 --> 00:16:49,060
it's a direct harsh speech. Obviously, a big problem. It's a problem because you're

128
00:16:49,060 --> 00:16:55,140
hurting someone. That's going to stay with you, this car. You can ever remember having

129
00:16:55,140 --> 00:17:01,740
been cruel to someone, having told them, said something to someone, hoping to cause

130
00:17:01,740 --> 00:17:10,940
them suffering, knowing that it would cause them suffering. Sometimes we, harsh speech is

131
00:17:10,940 --> 00:17:19,940
not always wrong. Or speech that appears harsh. There can be times where, you know,

132
00:17:19,940 --> 00:17:30,380
the Buddha himself called, sometimes he called monks moga purisa. And moga means useless.

133
00:17:30,380 --> 00:17:40,940
You're useless, man. When it's true, I remember a teacher, one of my high school teachers,

134
00:17:40,940 --> 00:17:46,620
it was really interesting. My much, she certainly, I don't think was perfectly enlightened,

135
00:17:46,620 --> 00:17:52,660
but I remember once I was listening to her talk to another student and she said, Andrew,

136
00:17:52,660 --> 00:17:57,540
you know, you're a bit of an idiot. She was scolding him because he wasn't doing his

137
00:17:57,540 --> 00:18:02,900
homework, and she was really trying to help him. I mean, in a mundane sense, it wasn't

138
00:18:02,900 --> 00:18:07,900
wrong speech. She didn't think he was an idiot, but she was telling him, you know, he hadn't

139
00:18:07,900 --> 00:18:15,260
done his homework, and he was procrastinating. And so she hit him, and she wasn't angry.

140
00:18:15,260 --> 00:18:25,220
She was just giving him tough love, what we would call right. So, but it's all, what

141
00:18:25,220 --> 00:18:29,780
her speech is only this note to make, that it's not always, speech that appears harsh is

142
00:18:29,780 --> 00:18:35,980
not always wrong. It's only when your intention is to harm. Like if, if this teacher looked

143
00:18:35,980 --> 00:18:41,420
at him and said, I don't want to do anything to do with you, you're an idiot, right?

144
00:18:41,420 --> 00:18:47,700
That would be very cruel. But when a mother's yell at their kids, well, they're usually

145
00:18:47,700 --> 00:18:52,540
very angry, so there's that as well. But sometimes out of love, they can say things that

146
00:18:52,540 --> 00:18:58,500
are very harsh, appear very harsh, but it's because they're concerned for their children's

147
00:18:58,500 --> 00:19:07,660
welfare. And the third type of speech, I'm actually at the backwards, I think, but the

148
00:19:07,660 --> 00:19:19,460
third is slanderous speech or gossip. Not only gossip, it's slander. And the definition

149
00:19:19,460 --> 00:19:26,900
here is speech that's not meant to hurt the listener, but speech that is meant to cause

150
00:19:26,900 --> 00:19:36,100
the listener to cultivate conflict with another person, speech that is designed to cultivate

151
00:19:36,100 --> 00:19:43,300
conflict, not even necessarily with the listener, but speech that you tell, hoping that

152
00:19:43,300 --> 00:19:51,660
it will cause conflict. Sometimes we just do this because we're fascinated. We tell

153
00:19:51,660 --> 00:20:00,740
gossip and spread stories. So sometimes it's true, right? Sometimes someone says something

154
00:20:00,740 --> 00:20:06,180
to us in confidence, boy, I really hate that person. And then you go and tell that person,

155
00:20:06,180 --> 00:20:14,620
hey, they said they really hate you. It's a very simplistic example. There's a story

156
00:20:14,620 --> 00:20:23,620
in the texts about this man who they were trying to, this king was trying to beat his

157
00:20:23,620 --> 00:20:29,380
enemies and he couldn't do it. So he sent his advisor and his advisor managed to, it's

158
00:20:29,380 --> 00:20:36,620
a really interesting story. I don't have time to go into it, but he goes around to each

159
00:20:36,620 --> 00:20:43,860
of these princes and eventually manages to sow discord among them. You know, that kind

160
00:20:43,860 --> 00:20:53,100
of thing where you break friends up, even not necessarily hoping to harm, it's just wrong.

161
00:20:53,100 --> 00:20:58,180
So it's an example of where telling the truth is not always a good thing. Sometimes telling

162
00:20:58,180 --> 00:21:06,260
the truth can be a very vicious thing. Malicious, I mean, some people tell truth with

163
00:21:06,260 --> 00:21:14,700
malicious intent and they say, well, it's true. Telling the truth is often wrong. In the

164
00:21:14,700 --> 00:21:20,940
case of slander, I don't think it's the right word.

165
00:21:20,940 --> 00:21:32,260
There's a part of it. If I say, yeah, that person is this and this and that. But I think

166
00:21:32,260 --> 00:21:37,700
slander is often untrue, right? The definition in English of slander is something that

167
00:21:37,700 --> 00:21:44,660
is not true. But this is more like what I said, what you've heard there, you don't say

168
00:21:44,660 --> 00:21:50,580
here, what you've heard here, you don't say there. So right speech is not this kind

169
00:21:50,580 --> 00:22:04,180
of a gossipy sowing discord. Someone tells you something in confidence. You do your best

170
00:22:04,180 --> 00:22:11,740
to discourage them from cultivating disharmony rather than bringing it to the other person.

171
00:22:11,740 --> 00:22:20,420
My good example is a really good example. When the schism, the big schism, when the Buddha

172
00:22:20,420 --> 00:22:25,060
was staying with these monks and one monk left some water in a bucket, one of the big

173
00:22:25,060 --> 00:22:32,220
head monks, the other big head monk told them, hey, that's an offense. And then amongst

174
00:22:32,220 --> 00:22:38,380
us, okay, well, then I confess it. And he says, well, let me confess it. And he says, well,

175
00:22:38,380 --> 00:22:44,020
if you didn't mean to, it's not an offense. And he says, okay, and he goes away. That

176
00:22:44,020 --> 00:22:50,260
monk goes, the other monk goes to tell his students, hey, the first, the first monk committed

177
00:22:50,260 --> 00:22:53,940
an offense and he didn't, he didn't confess it, which is of course what you have to do

178
00:22:53,940 --> 00:22:59,580
when you commit an offense. And so his students go and tell the others among students,

179
00:22:59,580 --> 00:23:04,660
your teacher has an offense that he hasn't confessed. And they told, and those students

180
00:23:04,660 --> 00:23:10,340
told their teacher, their teacher said, that other monk's a liar. So those students

181
00:23:10,340 --> 00:23:15,040
go back and tell the other students, your, our teacher says, your teacher's a liar. They

182
00:23:15,040 --> 00:23:25,020
tell their teacher, and the whole Sangha splits into right down the middle. The bikunis

183
00:23:25,020 --> 00:23:33,340
split into half of them on either side of the debate. The lay people split into half of

184
00:23:33,340 --> 00:23:39,300
them supporting one half. Even they say, even the angels guarding the monastery were divided

185
00:23:39,300 --> 00:23:45,500
by this. The whole monastery is split in half. When the Buddha tried to intervene, they

186
00:23:45,500 --> 00:23:51,020
said, go away, basically. So the Buddha left, this is when the Buddha went to the forest

187
00:23:51,020 --> 00:23:54,620
and was looked after by, I've told this story before, it was looked after by an elephant

188
00:23:54,620 --> 00:24:07,060
and a monkey. So there is some speech, not good. The fourth type of speech is useless

189
00:24:07,060 --> 00:24:18,260
speech, some papalapa. This kind of speech is not harmful directly, but it's still

190
00:24:18,260 --> 00:24:26,900
wrong. And it's still a hindrance to one's progress. The, the first type of speech lying

191
00:24:26,900 --> 00:24:33,340
is done away with at the Sotapana status. Sotapana doesn't lie. Once when it ends

192
00:24:33,340 --> 00:24:45,060
Sotapana, one, one keeps the five precepts. So lying is no longer a thing. When one reaches

193
00:24:45,060 --> 00:24:56,140
Anagami, the other two are speech, divisive speech. These are, these are done away with

194
00:24:56,140 --> 00:25:06,900
at Anagami, the third path. But only Arahan, this is an interesting fact, is that only

195
00:25:06,900 --> 00:25:12,460
an Arahan is free from Sumpapa. So means you can be an Anagami and still use, still

196
00:25:12,460 --> 00:25:19,780
saying things that are useless, still engaging in idle chatter, talking about the weather.

197
00:25:19,780 --> 00:25:27,340
I imagine Anagami has very little of it, but the point is it's still possible. They can

198
00:25:27,340 --> 00:25:32,140
still worry about things so they still might talk about the things they are worried about,

199
00:25:32,140 --> 00:25:38,780
right? Because an Arahan is pure, everything they say is pure. There's a pure intention,

200
00:25:38,780 --> 00:25:47,340
a clarity of mind. There's no, no useless speech. The Mahatsi Saya says you really, when

201
00:25:47,340 --> 00:25:51,940
you're keeping the precepts and undertaking meditation, you really have to keep all four.

202
00:25:51,940 --> 00:25:57,660
It's not enough to just not lie. But as meditators, we should keep a mind all four, especially

203
00:25:57,660 --> 00:26:02,540
the fourth one I think is the most, obviously the most difficult. It's not like we're

204
00:26:02,540 --> 00:26:07,860
not going to break it. It's not like if you break it, you should leave the meditation

205
00:26:07,860 --> 00:26:12,940
center or something. But engaging in it is a big problem for yourself and for the other

206
00:26:12,940 --> 00:26:18,860
meditators, obviously. It distracts you, it distracts them.

207
00:26:18,860 --> 00:26:23,980
The speech is a really big deal because of this, because it involves two people. If you

208
00:26:23,980 --> 00:26:34,860
take drugs and alcohol while you're definitely impeding your own progress. Even if you

209
00:26:34,860 --> 00:26:45,380
kill and steal while killing is quite, it's different, right? When you engage in speech,

210
00:26:45,380 --> 00:26:51,660
you're affecting other people. You're influencing them. That's a special. It's not worse

211
00:26:51,660 --> 00:27:02,780
than killing or stealing, but it's bad in a different way because of the engagement.

212
00:27:02,780 --> 00:27:12,660
So quite simply, we engage in right speech and we give up wrong speech as part of our practice,

213
00:27:12,660 --> 00:27:20,860
not only here in the meditation center, but in our lives as well. That's brief exposition

214
00:27:20,860 --> 00:27:33,260
on right speech, path factor number three. Before questions, we have someone who's leaving

215
00:27:33,260 --> 00:27:41,580
tomorrow. So I thought we'd have him come and say hello. This is someone who's meditated

216
00:27:41,580 --> 00:27:50,820
elsewhere. He's not, I think I'm not his first teacher. So it's actually less, is

217
00:27:50,820 --> 00:28:08,340
he? Can you see? Come closer. Can you give it a second, I think? It's lagging. Right.

218
00:28:08,340 --> 00:28:17,740
It's lagging, which might be a problem. I change the settings. I'll change them back

219
00:28:17,740 --> 00:28:31,380
for the next video, but keyframes not being sent off to know who current keyframes. Okay,

220
00:28:31,380 --> 00:28:40,420
so tell us about your experience here. You've practiced meditation before, but tell us not

221
00:28:40,420 --> 00:28:45,420
exactly experience, but how you felt before you came and how you feel now having finished

222
00:28:45,420 --> 00:28:57,420
the course. So the biggest thing I noticed was that the notes of basically negative mental

223
00:28:57,420 --> 00:29:05,660
states, they shortened a lot. So at the very beginning, I was noting, it seemed like

224
00:29:05,660 --> 00:29:15,300
sometimes hours, anger, all kinds of negative mental states, and towards the end, and to some

225
00:29:15,300 --> 00:29:22,820
degree now, even though today I did a bit of meditation, and I also used the internet

226
00:29:22,820 --> 00:29:32,180
of my phone, which distracted me a bit, it really felt like the negative states, they

227
00:29:32,180 --> 00:29:41,660
shortened, and they were a lot less persistent. How do you feel now? Pretty good. I feel

228
00:29:41,660 --> 00:29:48,700
fairly balanced, I would say. I don't feel super tranquil, but I think tranquility it comes

229
00:29:48,700 --> 00:30:01,180
and goes, and I feel like I got some insights that will persist. Okay, well good. You've done

230
00:30:01,180 --> 00:30:09,060
practiced before, so it's not going to be that extreme the changes, but would you recommend

231
00:30:09,060 --> 00:30:21,860
to practice the course to someone else? Okay, well that's good. I hope the stream's

232
00:30:21,860 --> 00:30:27,460
okay, YouTube's giving me all these warnings about bad video settings, but I think it's

233
00:30:27,460 --> 00:30:39,860
not so bad. Anyway, I'll just change it back next time. Okay, so let's go on to questions.

234
00:30:39,860 --> 00:30:48,700
Can animals be happier, or only have pleasure, or fear-based reactions? I don't really

235
00:30:48,700 --> 00:30:55,460
see the difference. Happiness, I mean, they can't be enlightened, so they can't truly

236
00:30:55,460 --> 00:30:59,860
be happy in a Buddhist sense, but they can experience pleasure the same way we do, and

237
00:30:59,860 --> 00:31:06,660
they call it happiness, just, they conceive of it as happy as the way we do. What are you

238
00:31:06,660 --> 00:31:12,260
referring to when you refer to the commentary? Well, there's an established commentary to

239
00:31:12,260 --> 00:31:25,260
much, if not all of the Buddhist teaching. It's called the Atakatakatat which means that discussion

240
00:31:25,260 --> 00:31:34,980
on the meaning. So that's the commentary. It's also in polly. A lot of them were written

241
00:31:34,980 --> 00:31:40,360
by Buddha Gosa or supposed to have been written by Buddha Gosa. But most of them come

242
00:31:40,360 --> 00:31:48,160
from something earlier that we don't have. So they're dealing with older texts or maybe oral

243
00:31:48,160 --> 00:31:57,360
transmission and they were a compilation of older teachings and older expositions. But

244
00:31:57,360 --> 00:32:05,160
they're I guess they're understood to have been written down from zero to 500

245
00:32:05,160 --> 00:32:13,660
common error. Since a mind moment arises in ceases is attachment simply a

246
00:32:13,660 --> 00:32:28,880
consequence that builds from an unmindful mind moment. Yeah. I don't really quite

247
00:32:28,880 --> 00:32:34,260
I don't really quite understand the question but sounds, yes, attachment is a

248
00:32:34,260 --> 00:32:40,360
consequence. A consequence of a lot of things ignorance being the main one. But

249
00:32:40,360 --> 00:32:47,120
it. It's caused by the pleasurable experience and attachment to it or an

250
00:32:47,120 --> 00:32:53,560
unpleasurable experience and then the repulsion from it. In previous video

251
00:32:53,560 --> 00:32:56,840
you mentioned craving leads to clinging, clinging leads to becoming. Please

252
00:32:56,840 --> 00:33:03,720
explain becoming. I don't quite understand what that means. So bhava bhava is

253
00:33:03,720 --> 00:33:11,200
understood in Buddhism has come a bhava in a sense. It's action. So when we

254
00:33:11,200 --> 00:33:15,560
talk about bhava what we really mean is the the mind state of craving and

255
00:33:15,560 --> 00:33:22,080
clinging leads to a physical manifestation of some sort. Usually in the form of

256
00:33:22,080 --> 00:33:26,600
doing something, right? I don't like you. So the physical manifestation is a

257
00:33:26,600 --> 00:33:34,240
hit. Or I want something so I grab for it. That's the becoming the the

258
00:33:34,240 --> 00:33:40,640
manifestation. Bhava just means being really bhava means existence or it's a

259
00:33:40,640 --> 00:33:49,720
very simple route. It's like the English word to be bhava. So it manifests

260
00:33:49,720 --> 00:33:54,200
itself. Up until that point it's been totally mental. You haven't actually

261
00:33:54,200 --> 00:34:02,960
initiated something. Bhava is when you initiate. So in a momentary sense,

262
00:34:02,960 --> 00:34:11,720
Bhava is the intention. I'm going to hit you. And then Jati, birth is when the

263
00:34:11,720 --> 00:34:16,120
actual hitting comes into play. Many people describe the patitya samapada in

264
00:34:16,120 --> 00:34:21,320
this way. Birth is referring to the action that you do. And then of course the

265
00:34:21,320 --> 00:34:28,360
result is getting in the fight with the person. So that's suffering. But it

266
00:34:28,360 --> 00:34:37,640
works on a macro cosmic level, macro level as well. When you actually die, when

267
00:34:37,640 --> 00:34:45,000
you die, the the intention to be reborn leads to Jati, leads to birth in the

268
00:34:45,000 --> 00:34:52,360
next being in the next realm. But Bhava is the manifestation, where it actually

269
00:34:52,360 --> 00:34:58,360
comes, it actually comes to food, actually creates an action. When Mar attempted

270
00:34:58,360 --> 00:35:02,400
the Buddha under the Bodhi tree, did it literally or physically happen? Or was it

271
00:35:02,400 --> 00:35:08,040
only a struggle in the mind? Can this happen to us in our meditation practice? Or

272
00:35:08,040 --> 00:35:12,400
if it was only in the mind, will we encounter the same scenario? In Buddhism,

273
00:35:12,400 --> 00:35:17,600
it's generally understood to be both. There is Mara in the mind and there are

274
00:35:17,600 --> 00:35:26,120
Mara's and beings that are external to us, not exactly physical, but

275
00:35:26,120 --> 00:35:32,360
somehow exist. What happened under the Bodhi tree? I don't know. I mean it seems to

276
00:35:32,360 --> 00:35:38,640
have been quite embellished and elaborated. But as to what actually happened,

277
00:35:38,640 --> 00:35:45,240
I always say, I don't know. But the idea that such beings exist seems reasonable

278
00:35:45,240 --> 00:35:51,360
to me. It seems far-fetched to most, but I have no evidence. It's just something

279
00:35:51,360 --> 00:35:57,160
that seems reasonable. Considering how little we experience a reality, I'm not

280
00:35:57,160 --> 00:36:02,120
surprised, I wouldn't be surprised to know that wouldn't be surprised if I

281
00:36:02,120 --> 00:36:06,640
experienced, if I met with such a being. But during our practice, it doesn't

282
00:36:06,640 --> 00:36:10,560
really matter because in our practice beings don't exist. They don't exist to

283
00:36:10,560 --> 00:36:15,040
a meditator. To a meditator, there's only seeing, hearing, smelling, tasting,

284
00:36:15,040 --> 00:36:18,600
feeling, thinking. I've had meditators where this has become an issue. Evil

285
00:36:18,600 --> 00:36:24,280
spirits come in and malevolent spirits, mischievous spirits, that kind of

286
00:36:24,280 --> 00:36:29,560
thing. I talked to a monk who had all sorts of spirits come to him. He had a

287
00:36:29,560 --> 00:36:39,680
ghost try to have sex with him once he sent. So the point is, in the end, it's

288
00:36:39,680 --> 00:36:44,360
all experiences. You have these experiences that seeing, hearing, smelling, tasting,

289
00:36:44,360 --> 00:36:48,200
feeling, thinking. And it doesn't matter whether it's a spirit or whether it's

290
00:36:48,200 --> 00:36:53,160
in the mind, it's an experience. So this isn't an issue, shouldn't be an issue

291
00:36:53,160 --> 00:36:57,400
for meditators. It's an issue because we deal still with concepts. But once we

292
00:36:57,400 --> 00:37:02,200
break through that, we're invincible. There's no Mara who can get in our way.

293
00:37:02,200 --> 00:37:07,880
That's sort of what the Buddhist story is meant to convey. This

294
00:37:07,880 --> 00:37:17,320
imperturbability. And he tips to prepare for a six-day meditation retreat.

295
00:37:20,120 --> 00:37:25,160
Creed starts in 30 days. We'll start meditating and keep the five precepts at

296
00:37:25,160 --> 00:37:32,840
least. Try to be mindful during daily life. Try and you try your best to pull

297
00:37:32,840 --> 00:37:38,200
yourself away from some of the things that are going to be bad habits.

298
00:37:38,200 --> 00:37:41,240
And during the meditation course,

299
00:37:41,240 --> 00:37:55,880
a lot of people, a lot of Buddhists criticize our tradition because we focus so

300
00:37:55,880 --> 00:38:01,800
much on meditation courses. And we really don't, or we shouldn't. I mean, a good

301
00:38:01,800 --> 00:38:05,880
foundation course is a great way to get started. But it doesn't end there, of

302
00:38:05,880 --> 00:38:13,240
course. Our course is not six days. It's like 21 days for that reason. We want you to

303
00:38:13,240 --> 00:38:17,720
really change who you are and change the way you live. So that when you leave here,

304
00:38:17,720 --> 00:38:26,840
you can live your life mindfully. So that six days should be just considered to be

305
00:38:26,840 --> 00:38:31,240
an intensive period of meditation. It's not your only meditation.

306
00:38:31,240 --> 00:38:35,640
Building up to it is great. And when you leave it, continuing on with it, don't think

307
00:38:35,640 --> 00:38:42,120
of the meditation practices or the course as your spiritual practice. It's only one

308
00:38:42,120 --> 00:38:47,880
period that is sort of an intensifying. The rest of your life is just as important.

309
00:38:47,880 --> 00:39:03,800
And that's all the questions. So thank you all for tuning in. Have a good night.

