1
00:00:00,000 --> 00:00:04,000
So here I am in Thailand,

2
00:00:04,000 --> 00:00:10,000
staying at one of my close friend houses.

3
00:00:10,000 --> 00:00:14,000
Sort of a relative.

4
00:00:14,000 --> 00:00:20,000
And here I am back in Thailand.

5
00:00:20,000 --> 00:00:24,000
One thing that I think is quite interesting is

6
00:00:24,000 --> 00:00:32,000
how it doesn't mean as much as it used to

7
00:00:32,000 --> 00:00:36,000
to travel and to go places.

8
00:00:36,000 --> 00:00:40,000
It's interesting how when you practice meditation

9
00:00:40,000 --> 00:00:46,000
you stop looking for the right place to be

10
00:00:46,000 --> 00:00:52,000
and focus much more on your own

11
00:00:52,000 --> 00:00:56,000
place in wherever you are,

12
00:00:56,000 --> 00:01:00,000
meaning how you fit in

13
00:01:00,000 --> 00:01:04,000
and how you work and how you react to the situation.

14
00:01:04,000 --> 00:01:06,000
You start to be more concerned with what's inside

15
00:01:06,000 --> 00:01:08,000
and what's outside.

16
00:01:08,000 --> 00:01:11,000
And that's really what we're trying to

17
00:01:11,000 --> 00:01:15,000
develop in the practice, what we try to teach

18
00:01:15,000 --> 00:01:19,000
in regards to the meditation that wherever you are

19
00:01:19,000 --> 00:01:23,000
it's much more important

20
00:01:23,000 --> 00:01:27,000
who you are and how you react to it

21
00:01:27,000 --> 00:01:29,000
and what's around.

22
00:01:29,000 --> 00:01:32,000
So, you know, this looks like a wonderful place

23
00:01:32,000 --> 00:01:34,000
and everyone always jealous

24
00:01:34,000 --> 00:01:37,000
and they see me going to these beautiful wonderful places.

25
00:01:37,000 --> 00:01:41,000
But, you know,

26
00:01:41,000 --> 00:01:46,000
you can get the same sense of peace, happiness

27
00:01:46,000 --> 00:01:49,000
and freedom wherever you are

28
00:01:49,000 --> 00:01:54,000
and this sort of beauty and nature

29
00:01:54,000 --> 00:01:57,000
and peace and serenity outside

30
00:01:57,000 --> 00:02:01,000
is totally lost if your mind is not

31
00:02:01,000 --> 00:02:03,000
in a good place.

32
00:02:03,000 --> 00:02:07,000
So, just a thought of the day

33
00:02:07,000 --> 00:02:10,000
if you want to find your place,

34
00:02:10,000 --> 00:02:12,000
it's all about what's inside.

35
00:02:12,000 --> 00:02:14,000
You have to find your place

36
00:02:14,000 --> 00:02:17,000
and how you react to the world around you.

37
00:02:17,000 --> 00:02:19,000
Because, in the end,

38
00:02:19,000 --> 00:02:22,000
it's all seeing, hearing,

39
00:02:22,000 --> 00:02:26,000
smelling, tasting, feeling and thinking.

40
00:02:26,000 --> 00:02:31,000
There's nothing beyond those six things.

41
00:02:31,000 --> 00:02:35,000
Wherever you go, they're all the same.

42
00:02:35,000 --> 00:02:37,000
What's different is your reaction,

43
00:02:37,000 --> 00:02:40,000
your ability to accept

44
00:02:40,000 --> 00:02:43,000
and to be happy wherever you are.

45
00:02:43,000 --> 00:02:46,000
At all times.

46
00:02:46,000 --> 00:02:51,000
So, this is my first video from Thailand

47
00:02:51,000 --> 00:02:55,000
and just thought I'd say hi

48
00:02:55,000 --> 00:02:58,000
and share my thoughts.

49
00:02:58,000 --> 00:03:21,000
Okay, for the best.

