1
00:00:00,000 --> 00:00:06,840
How to meditate by you to Domo Beepu, read by Adirokes.

2
00:00:06,840 --> 00:00:10,520
Chapter 6 Daily Life

3
00:00:10,520 --> 00:00:17,080
At this point, instruction in the basic technique of formal meditation practice is complete.

4
00:00:17,080 --> 00:00:22,840
The teachings in the previous chapters is enough for a newcomer to begin on the path towards

5
00:00:22,840 --> 00:00:26,120
understanding reality as it is.

6
00:00:26,120 --> 00:00:32,000
In this final chapter, I will discuss some of the ways in which the meditation practice

7
00:00:32,000 --> 00:00:38,680
can be incorporated into daily life, so that even when one is not formally meditating,

8
00:00:38,680 --> 00:00:45,080
one can still maintain a basic level of mindfulness and clear awareness.

9
00:00:45,080 --> 00:00:53,240
First, it is necessary to discuss activities that are harmful to one's mental clarity.

10
00:00:53,240 --> 00:00:58,960
Studies that one must avoid in order for the meditation to bring about sustained positive

11
00:00:58,960 --> 00:01:00,760
results.

12
00:01:00,760 --> 00:01:07,720
As I explained in the first chapter, meditation is the mental equivalent to medicine.

13
00:01:07,720 --> 00:01:13,080
When taking medicine, there are certain substances one must avoid.

14
00:01:13,080 --> 00:01:19,400
Substances that will either nullify the positive effects of the medicine, or worse, combine

15
00:01:19,400 --> 00:01:22,680
with the medicine to create poison.

16
00:01:22,680 --> 00:01:28,360
Likewise, with meditation there are certain activities that, due to their tendency to

17
00:01:28,360 --> 00:01:36,480
call the mind, have the potential to nullify the effects of the meditation, or worse, pervert

18
00:01:36,480 --> 00:01:42,960
one's understanding of the meditation, causing one to cultivate unwholesome mind states instead

19
00:01:42,960 --> 00:01:46,000
of wholesome ones.

20
00:01:46,000 --> 00:01:52,360
Meditation is meant to cultivate clarity and understanding, free from addiction, aversion,

21
00:01:52,360 --> 00:01:58,000
and delusion, and therefore free from suffering.

22
00:01:58,000 --> 00:02:03,480
Since certain bodily and verbal acts are intrinsically tied to negative qualities of the

23
00:02:03,480 --> 00:02:10,640
mind, they are considered contraindicative to the meditation practice.

24
00:02:10,640 --> 00:02:19,080
They have an effect opposite to what is desired, cultivating defilement instead of purity.

25
00:02:19,080 --> 00:02:25,480
Studies who insist on engaging in such behavior will face great difficulty in their practice,

26
00:02:25,480 --> 00:02:32,160
developing habits that are detrimental to both meditation practice and personal well-being.

27
00:02:32,160 --> 00:02:38,280
To ensure the mind is perfectly clear and capable of understanding reality, certain behaviors

28
00:02:38,280 --> 00:02:42,760
must be taken out of one's diet, so to speak.

29
00:02:42,760 --> 00:02:49,400
First, there are five kinds of action from which one must refrain completely, as they are

30
00:02:49,400 --> 00:02:52,360
inherently unwholesome.

31
00:02:52,360 --> 00:02:57,680
These five behaviors correspond with the five Buddhist moral precepts.

32
00:02:57,680 --> 00:03:00,880
One must refrain from killing living beings.

33
00:03:00,880 --> 00:03:06,520
In order to cultivate one's own well-being, one must be dedicated to well-being as a

34
00:03:06,520 --> 00:03:13,240
principle, refraining from killing any living being, even ants, mosquitoes, and other

35
00:03:13,240 --> 00:03:15,920
living beings.

36
00:03:15,920 --> 00:03:19,640
Two, one must refrain from theft.

37
00:03:19,640 --> 00:03:24,760
In order to find peace of mind, we must grant it to others as well.

38
00:03:24,760 --> 00:03:29,160
Steeling is a denial of the basic right to security.

39
00:03:29,160 --> 00:03:35,280
Further, if we wish to be free from addiction, we must be able to control our desires to

40
00:03:35,280 --> 00:03:39,200
the extent of respecting the possessions of others.

41
00:03:39,200 --> 00:03:46,960
Three, one must abstain from committing adultery or sexual misconduct.

42
00:03:46,960 --> 00:03:52,320
Romantic relationships that are emotionally or spiritually damaging to others due to existing

43
00:03:52,320 --> 00:03:58,400
commitments of the parties involved are a cause for stress and suffering and based on

44
00:03:58,400 --> 00:04:00,720
perversion of the mind.

45
00:04:00,720 --> 00:04:04,560
Four, one must refrain from telling lies.

46
00:04:04,560 --> 00:04:08,920
If one wishes to find truth, one must avoid falsehood.

47
00:04:08,920 --> 00:04:15,720
Intentionally leading others away from the truth is harmful both to oneself and others

48
00:04:15,720 --> 00:04:20,160
and incompatible with the goals of meditation.

49
00:04:20,160 --> 00:04:26,080
Five, one must refrain from taking drugs or alcohol.

50
00:04:26,080 --> 00:04:32,960
Any substance that intoxicates the mind is obviously contraindicative to meditation practice

51
00:04:32,960 --> 00:04:39,040
as it is the antithesis of a natural, clear state of being.

52
00:04:39,040 --> 00:04:44,040
Complete abstention from these activities is necessary if one wishes for meditation

53
00:04:44,040 --> 00:04:50,320
practice to be successful due to their inherently unwholesome nature and the invariably

54
00:04:50,320 --> 00:04:53,440
negative effect they have on the mind.

55
00:04:53,440 --> 00:04:59,440
Further, there are certain activities that must be moderated or they will interfere with

56
00:04:59,440 --> 00:05:02,320
the meditation practice.

57
00:05:02,320 --> 00:05:08,600
These are activities that are not necessarily unwholesome in and of themselves but will nonetheless

58
00:05:08,600 --> 00:05:15,120
inhibit clarity of mind and lessen the benefit of the meditation practice when undertaken

59
00:05:15,120 --> 00:05:16,720
in excess.

60
00:05:16,720 --> 00:05:21,440
The following is in accordance with the eight meditator precepts normally taken by Buddhist

61
00:05:21,440 --> 00:05:27,600
meditators on holidays or during intensive meditation courses, adding the three precepts

62
00:05:27,600 --> 00:05:32,800
below to the five above and undertaking total celibacy.

63
00:05:32,800 --> 00:05:35,760
One such activity is eating.

64
00:05:35,760 --> 00:05:41,120
If one wishes to truly progress in the meditation practice, one must be careful not to

65
00:05:41,120 --> 00:05:43,840
eat too much or too little.

66
00:05:43,840 --> 00:05:50,240
If one is constantly obsessed with food, it can be a great entrance to progress in meditation

67
00:05:50,240 --> 00:05:58,080
since not only does it cloud the mind over eating leads to drowsiness both in the body and mind.

68
00:05:58,080 --> 00:06:03,880
One should eat to stay alive rather than stay alive simply to eat.

69
00:06:03,880 --> 00:06:09,680
During intensive meditation courses, meditators eat one main meal per day and suffer

70
00:06:09,680 --> 00:06:16,080
no negative physical consequences as a result whereas the positive effects of such moderation

71
00:06:16,080 --> 00:06:21,480
are clarity of mind and freedom from obsession over food.

72
00:06:21,480 --> 00:06:27,840
Another activity that interferes with meditation practice is entertainment, watching movies,

73
00:06:27,840 --> 00:06:31,360
listening to music and so on.

74
00:06:31,360 --> 00:06:36,960
These occupations are not inherently unwholesome but can easily create states of addiction

75
00:06:36,960 --> 00:06:40,520
when undertaken in excess.

76
00:06:40,520 --> 00:06:45,920
Addiction is a form of in sobriety in a sense since it involves chemical processes

77
00:06:45,920 --> 00:06:50,880
in the brain that inhibit clear thought and clarity of mind.

78
00:06:50,880 --> 00:06:56,880
Since the pleasure that comes from entertainment is momentary and unsatisfying while the

79
00:06:56,880 --> 00:07:02,920
addiction and obsession carry over into one's life, a serious meditator should determine

80
00:07:02,920 --> 00:07:09,960
to make the best use of their short time in this life by cultivating peace and contentment

81
00:07:09,960 --> 00:07:15,800
rather than wasting it on meaningless activities that don't lead to long-term happiness and

82
00:07:15,800 --> 00:07:16,800
peace.

83
00:07:16,800 --> 00:07:23,880
If one wishes to find true happiness, one must therefore moderate one's engagement in entertainment,

84
00:07:23,880 --> 00:07:28,920
socializing on the internet and similar activities should be undertaken in moderation

85
00:07:28,920 --> 00:07:30,240
as well.

86
00:07:30,240 --> 00:07:35,600
The third activity that one must moderate is that of sleeping.

87
00:07:35,600 --> 00:07:38,840
Sleeping is an addiction that is often overlooked.

88
00:07:38,840 --> 00:07:46,000
Most people don't realize how attached they are to sleep as a means of escape from reality.

89
00:07:46,000 --> 00:07:51,600
Still others become insomnia, obsessed with the thought that they are not getting enough

90
00:07:51,600 --> 00:07:58,680
sleep, leading to increased stress levels and further difficulty in falling asleep.

91
00:07:58,680 --> 00:08:03,960
Through the meditation practice, one will find that one needs less sleep than before since

92
00:08:03,960 --> 00:08:06,760
one mind will become calmer.

93
00:08:06,760 --> 00:08:12,120
Insomnia is not a problem for meditators, since they are able to meditate even in the

94
00:08:12,120 --> 00:08:17,200
lying position and keep their minds free from stress.

95
00:08:17,200 --> 00:08:21,960
People who have difficulty falling asleep should train themselves to watch the stomach

96
00:08:21,960 --> 00:08:29,680
rise and fall, noting rising, falling, all night if necessary.

97
00:08:29,680 --> 00:08:34,520
Even if they are not able to fall asleep, which is unlikely given the calm state of

98
00:08:34,520 --> 00:08:40,640
mind while meditating, they will find themselves as rested as if they had slept soundly

99
00:08:40,640 --> 00:08:42,040
through the night.

100
00:08:42,040 --> 00:08:47,880
Finally, it is worth mentioning that to truly gain results in the meditation practice,

101
00:08:47,880 --> 00:08:54,480
a meditator should set aside at least a period of time to remain entirely celibate, not

102
00:08:54,480 --> 00:09:01,960
just avoiding immoral sexual activity, since all sexual activity is invariably intoxicating

103
00:09:01,960 --> 00:09:07,600
and will be a hindrance towards attainment of mental clarity and peace.

104
00:09:07,600 --> 00:09:13,040
Once one has put aside activities, then interfere with clarity of mind.

105
00:09:13,040 --> 00:09:18,960
One can begin to incorporate meditative awareness into ordinary life.

106
00:09:18,960 --> 00:09:24,160
There are two ways in which one can meditate on ordinary experience, and they should

107
00:09:24,160 --> 00:09:27,360
be practiced together as follows.

108
00:09:27,360 --> 00:09:32,840
The first method is to focus one's attention on the body, since it is the most clearly

109
00:09:32,840 --> 00:09:36,080
evident aspect of experience.

110
00:09:36,080 --> 00:09:42,600
As in formal meditation, the body is always available for observation, and thus serves

111
00:09:42,600 --> 00:09:48,160
as a convenient means of creating awareness.

112
00:09:48,160 --> 00:09:54,960
As in formal meditation, the body is always available for observation, and thus serves

113
00:09:54,960 --> 00:10:01,840
as a convenient means of creating clear awareness of reality in daily life.

114
00:10:01,840 --> 00:10:09,040
Since the body is generally in one of four postures, walking, standing, sitting, or lying

115
00:10:09,040 --> 00:10:16,760
down, one can simply become aware of one's posture as a meditation object to bring about

116
00:10:16,760 --> 00:10:18,880
clarity of mind.

117
00:10:18,880 --> 00:10:29,560
In walking, for example, one can note either walking, walking, walking, or left, right,

118
00:10:29,560 --> 00:10:34,080
left, right, as one moves each foot.

119
00:10:34,080 --> 00:10:42,760
When standing still, one can focus on the standing position and note standing, standing,

120
00:10:42,760 --> 00:10:51,440
and sitting, sitting, sitting, and when lying down, lying, lying.

121
00:10:51,440 --> 00:10:57,320
In this way, one can develop clarity of mind at any time, even when not practicing

122
00:10:57,320 --> 00:10:59,200
formal meditation.

123
00:10:59,200 --> 00:11:05,240
Further, one can apply the same technique to any small movement of the body.

124
00:11:05,240 --> 00:11:13,040
For instance, when bending or stretching the limbs, one can note bending or stretching.

125
00:11:13,040 --> 00:11:22,240
When moving the limbs, moving, when turning, turning, and so on, every activity can become

126
00:11:22,240 --> 00:11:28,960
a meditation practice in this way, when brushing one's teeth, brushing, when chewing

127
00:11:28,960 --> 00:11:37,960
or swallowing food, chewing, chewing, swallowing, swallowing, and so on.

128
00:11:37,960 --> 00:11:45,920
When cooking, cleaning, exercising, showering, changing clothes, even on the toilet, one

129
00:11:45,920 --> 00:11:51,920
can be mindful of the movements of the body involved, creating clear awareness of reality

130
00:11:51,920 --> 00:11:54,080
at all times.

131
00:11:54,080 --> 00:12:00,080
This is the first method by which one can and should incorporate the meditation practice

132
00:12:00,080 --> 00:12:03,240
directly into ordinary life.

133
00:12:03,240 --> 00:12:10,880
The second method is the acknowledgement of the senses, seeing, hearing, smelling, tasting,

134
00:12:10,880 --> 00:12:12,920
and feeling.

135
00:12:12,920 --> 00:12:19,040
Ordinary sensory experience tends to give rise to either liking or disliking.

136
00:12:19,040 --> 00:12:25,560
It therefore becomes a cause for addiction or aversion, and ultimately suffering, when it

137
00:12:25,560 --> 00:12:29,200
is not in line with one's partialities.

138
00:12:29,200 --> 00:12:34,600
In order to keep the mind clear and impartial, one should always try to create clear

139
00:12:34,600 --> 00:12:40,160
awareness at the moment of sensory experience, rather than allowing the mind to judge

140
00:12:40,160 --> 00:12:45,600
the experience according to its habitual tendencies.

141
00:12:45,600 --> 00:12:54,360
When seeing, therefore, one should know it simply as seeing, reminding oneself, seeing,

142
00:12:54,360 --> 00:12:55,440
seeing.

143
00:12:55,440 --> 00:13:01,240
When hearing a sound, one should likewise note, hearing, hearing.

144
00:13:01,240 --> 00:13:06,680
When smelling pleasant or unpleasant odors, smelling, smelling.

145
00:13:06,680 --> 00:13:13,480
When tasting food or drink, instead of becoming addicted to or repulsed by the taste,

146
00:13:13,480 --> 00:13:17,960
one should note tasting, tasting.

147
00:13:17,960 --> 00:13:25,760
When feelings arise in the body, hot or cold, hard or soft, and so on, one should note

148
00:13:25,760 --> 00:13:33,920
feeling, feeling, or hot, cold, and so on.

149
00:13:33,920 --> 00:13:39,400
Practicing in this way, one will be able to receive the full spectrum of experience without

150
00:13:39,400 --> 00:13:51,840
compartmentalizing reality into categories of good, bad, me, mine, us, them, and so on.

151
00:13:51,840 --> 00:13:58,960
As a result, true peace, happiness, and freedom from suffering is possible at all times

152
00:13:58,960 --> 00:14:01,720
in all situations.

153
00:14:01,720 --> 00:14:07,760
Once one understands the true nature of reality, the mind will cease to react to the objects

154
00:14:07,760 --> 00:14:14,680
of the sense, as other than what they truly are, and be free from all addiction and

155
00:14:14,680 --> 00:14:22,840
aversion, just as a flying bird is free from any need for a perch on which to claim.

156
00:14:22,840 --> 00:14:29,080
This then is a basic guide to practice meditation in daily life, incorporating the meditation

157
00:14:29,080 --> 00:14:35,000
practice directly into one's life, even when not formally meditating.

158
00:14:35,000 --> 00:14:40,520
Beyond these two methods, one can also apply any of the objects discussed in the first

159
00:14:40,520 --> 00:14:45,760
chapter, pain, thoughts, or the emotions.

160
00:14:45,760 --> 00:14:50,760
The techniques discussed in this chapter should be thought of as an additional means of

161
00:14:50,760 --> 00:14:58,160
making the meditation practice a continuous experience whereby one is learning about oneself

162
00:14:58,160 --> 00:15:02,880
and about reality at all times.

163
00:15:02,880 --> 00:15:07,520
This concludes the basic instruction on how to meditate.

164
00:15:07,520 --> 00:15:13,400
Remember that no book, no matter how detailed it may be, can substitute sincere and

165
00:15:13,400 --> 00:15:16,680
ardent practice of the teaching itself.

166
00:15:16,680 --> 00:15:22,280
One may learn by heart all wise books ever written and still be no better off than a cow

167
00:15:22,280 --> 00:15:28,520
herd guarding the cattle of others should one not practice accordingly.

168
00:15:28,520 --> 00:15:35,080
Life on the other hand, one accepts the basic tenets included in a book like this as sufficient

169
00:15:35,080 --> 00:15:42,520
theoretical knowledge and practices sincerely in accordance with them, one is surely guaranteed

170
00:15:42,520 --> 00:15:50,720
to attain the same results as countless others have likewise attained, peace, happiness,

171
00:15:50,720 --> 00:15:54,760
and true freedom from suffering.

172
00:15:54,760 --> 00:16:00,560
Thank you one last time for taking the time to read this short introduction on how to meditate,

173
00:16:00,560 --> 00:16:07,240
and once more I sincerely wish for this instruction to bring peace, happiness, and freedom

174
00:16:07,240 --> 00:16:13,440
from suffering to you and all of the beings with whom you come in contact.

175
00:16:13,440 --> 00:16:18,600
Should you find anything lacking or unclear in these pages, or if you would like more

176
00:16:18,600 --> 00:16:24,400
detailed or specific instructions in the practice of meditation, you are welcome to contact

177
00:16:24,400 --> 00:16:31,000
me through my web blog at utadamo.ceremungalow.org.

