1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhamapada.

2
00:00:05,000 --> 00:00:14,000
Today we continue with verses 85 and 86, which read as follows.

3
00:00:14,000 --> 00:00:43,000
This verse is our common chant.

4
00:00:43,000 --> 00:00:46,000
A common chant in Thailand.

5
00:00:46,000 --> 00:00:47,000
So quite familiar.

6
00:00:47,000 --> 00:00:50,000
These are the next ones in the chapter.

7
00:00:50,000 --> 00:00:56,000
So these two mean apakati minuses.

8
00:00:56,000 --> 00:01:01,000
So few are they among humans.

9
00:01:01,000 --> 00:01:06,000
Yajanaparagami who go to the farther shore.

10
00:01:06,000 --> 00:01:14,000
Atayangitarapaja, these other, here these other humans,

11
00:01:14,000 --> 00:01:17,000
where other people.

12
00:01:17,000 --> 00:01:24,000
Thirameva Anuwati simply run up and down the shore.

13
00:01:24,000 --> 00:01:29,000
Yajakho Samadakati.

14
00:01:29,000 --> 00:01:36,000
But those who...

15
00:01:36,000 --> 00:01:43,000
In regards to the well taught Dhamma, Dhamma Anuwati,

16
00:01:43,000 --> 00:01:51,000
are those who go according to the Dhamma.

17
00:01:51,000 --> 00:01:58,000
Thiranaparamisandhi.

18
00:01:58,000 --> 00:02:05,000
These people go to the farther shore.

19
00:02:05,000 --> 00:02:12,000
The Dhamma, these cross, these cross to the other side.

20
00:02:12,000 --> 00:02:23,000
Muchudayang of the kingdom of death, which is hard to cross, which is very hard to cross.

21
00:02:23,000 --> 00:02:29,000
So we have some imagery here, the imagery of crossing an ocean,

22
00:02:29,000 --> 00:02:33,000
and that most people just run up and down the shore.

23
00:02:33,000 --> 00:02:43,000
Thirameva Anuwati is running up and down.

24
00:02:43,000 --> 00:02:48,000
So this was given in regards to another very short story.

25
00:02:48,000 --> 00:02:58,000
It's rather more of a remark that the Buddha gave in regards to the habits of those who would come and listen to the Dhamma.

26
00:02:58,000 --> 00:03:06,000
It seems that there was a specific group of people who came to listen to the Buddha's teaching.

27
00:03:06,000 --> 00:03:14,000
They had done good deeds, or they had supported the monastery, and so they came and gave arms to the monks,

28
00:03:14,000 --> 00:03:19,000
and then stuck around intending to listen to the Dhamma all night.

29
00:03:19,000 --> 00:03:25,000
There would be teachings of the Dhamma where the monks would take turns, giving talks all night,

30
00:03:25,000 --> 00:03:33,000
maybe even one monk would talk all night, probably the former, probably it would be several monks taking turns.

31
00:03:33,000 --> 00:03:38,000
We once did this in Thailand, something like this.

32
00:03:38,000 --> 00:03:44,000
There were some ancient Buddha statues that were stolen.

33
00:03:44,000 --> 00:03:47,000
They were actually kept in, kept behind bars.

34
00:03:47,000 --> 00:03:48,000
They were quite valuable.

35
00:03:48,000 --> 00:03:52,000
They were kept behind bars, and someone actually cut through the bars,

36
00:03:52,000 --> 00:03:56,000
or bent the bars or something to steal these Buddha images, cut through the bars, I think,

37
00:03:56,000 --> 00:03:59,000
to steal the Buddha images.

38
00:03:59,000 --> 00:04:13,000
And somewhat miraculously, one of them was found at the bottom of one of the rivers, I guess, like a small river,

39
00:04:13,000 --> 00:04:18,000
when it dried up or something in the dry season, it was found.

40
00:04:18,000 --> 00:04:25,000
And so they pulled it up and brought it back and had an all night ceremony.

41
00:04:25,000 --> 00:04:27,000
It was something like this.

42
00:04:27,000 --> 00:04:33,000
Again, the details are in precise, maybe they were even paid too much attention to the details,

43
00:04:33,000 --> 00:04:35,000
but it was something like that.

44
00:04:35,000 --> 00:04:39,000
And so we had an all night ceremony to honour the Buddha,

45
00:04:39,000 --> 00:04:44,000
or maybe to reestablish its sanctity or something, its holiness.

46
00:04:44,000 --> 00:04:48,000
I mean, it was a very cultural sort of thing that I didn't pay too much attention to.

47
00:04:48,000 --> 00:04:51,000
It may have been broken or something, and there was a fixing that had to go on,

48
00:04:51,000 --> 00:04:57,000
and at the end of fixing it, you have to re-bless it or something.

49
00:04:57,000 --> 00:05:00,000
But it was kind of considered a miracle that they got it back.

50
00:05:00,000 --> 00:05:02,000
So it was a big deal.

51
00:05:02,000 --> 00:05:08,000
And all of the monks, I was a part of it, we would take turns chanting or giving talks

52
00:05:08,000 --> 00:05:12,000
or that kind of thing all night until the sun rose.

53
00:05:12,000 --> 00:05:17,000
So the spirit of this still goes on, even today.

54
00:05:17,000 --> 00:05:21,000
This idea of an all night dhamma thing,

55
00:05:21,000 --> 00:05:24,000
but it was sort of, I think it was a jantong that put it together.

56
00:05:24,000 --> 00:05:26,000
I don't know how often it happens.

57
00:05:26,000 --> 00:05:32,000
I know in Burma they do all night chantings and all night teachings from what I hear.

58
00:05:32,000 --> 00:05:38,000
Anyway, the situation here was all of them were trying to listen to the dhamma,

59
00:05:38,000 --> 00:05:47,000
but none of them lasted, or maybe not none of them, but many of them were unable to last.

60
00:05:47,000 --> 00:05:52,000
Right, none of them, they were unable to listen to the dhamma all night.

61
00:05:52,000 --> 00:06:01,000
Some of them were overcome by lust, sexual passion, and went home.

62
00:06:01,000 --> 00:06:09,000
Some of them were overcome by anger, boredom maybe.

63
00:06:09,000 --> 00:06:13,000
The anger is a very different kind, many different kinds.

64
00:06:13,000 --> 00:06:18,000
It can be boredom, it can be dislike of the dhamma teachings,

65
00:06:18,000 --> 00:06:28,000
the self-hatred idea that every time you hear something that hits too close to home.

66
00:06:28,000 --> 00:06:32,000
Maybe they're talking about things that are unwholesome,

67
00:06:32,000 --> 00:06:36,000
and you realize that you engage in some of those, so you get angry at yourself

68
00:06:36,000 --> 00:06:38,000
and feel guilty and all these things.

69
00:06:38,000 --> 00:06:42,000
So being overcome with that, they were unable to stay, and they went home.

70
00:06:42,000 --> 00:06:51,000
Some of them, some of them were overcome by mana,

71
00:06:51,000 --> 00:07:00,000
conceit, so that would be the feeling self-righteous and displeased by the dhamma,

72
00:07:00,000 --> 00:07:06,000
but not an angry thing to feeling like they knew better than the monks,

73
00:07:06,000 --> 00:07:10,000
or these monks don't know what they're talking about being attached to views,

74
00:07:10,000 --> 00:07:12,000
and that kind of thing.

75
00:07:12,000 --> 00:07:18,000
Some of them got caught up with Tina Mida, so they got tired, that's a common one.

76
00:07:18,000 --> 00:07:22,000
And we're nodding off and said, I realized that they had to go home is better,

77
00:07:22,000 --> 00:07:26,000
and so they went home in the middle of the teaching.

78
00:07:34,000 --> 00:07:36,000
And so they went off.

79
00:07:36,000 --> 00:07:48,000
The next day, the monks gathered and commented on this,

80
00:07:48,000 --> 00:07:56,000
and just were sort of remarking on how difficult it is for people to listen to the dhamma,

81
00:07:56,000 --> 00:08:04,000
and how people are all that keen to take the opportunity to listen to the dhamma.

82
00:08:04,000 --> 00:08:09,000
And the Buddha said, monks, for the most, the Buddha came in and asked them what they were talking about,

83
00:08:09,000 --> 00:08:16,000
and when they told him, he remarked that this is common for people.

84
00:08:16,000 --> 00:08:30,000
And for the most part, they are dependent upon, or they are attached to existence,

85
00:08:30,000 --> 00:08:35,000
Baba, becoming.

86
00:08:35,000 --> 00:08:47,000
Baba, we had a very well stuck, stuck in Baba, stuck in becoming.

87
00:08:47,000 --> 00:08:56,000
The meaning here, Baba is a sort of neutral term that sort of blanket statement for anything that leads to further becoming.

88
00:08:56,000 --> 00:09:01,000
And so when you want something, on a very basic level, it leads to becoming here,

89
00:09:01,000 --> 00:09:09,000
and now you give a rise to a new something new, which is the plan to get what you want.

90
00:09:09,000 --> 00:09:15,000
And you want to eat, and so suddenly the plan to make food arises.

91
00:09:15,000 --> 00:09:19,000
Maybe you want to watch a movie, so you plan to go out,

92
00:09:19,000 --> 00:09:25,000
and I guess people don't go to the video store anymore.

93
00:09:25,000 --> 00:09:36,000
You, whatever you want, you want to go to Thailand, and so you give a rise to something.

94
00:09:36,000 --> 00:09:43,000
But you give rise to, for the becoming something that's not just functional,

95
00:09:43,000 --> 00:09:47,000
but it's actually a desire base.

96
00:09:47,000 --> 00:09:52,000
A whole new set of variables.

97
00:09:52,000 --> 00:09:57,000
Or if you're angry, you give rise to something new as well, at the very least a headache.

98
00:09:57,000 --> 00:10:06,000
But you can also have anger give rise to argument, friction, conflict, war, even.

99
00:10:06,000 --> 00:10:11,000
Of course, war can be caused by greed, and often it's caused simply by greed.

100
00:10:11,000 --> 00:10:24,000
You can see it, you give rise to this competition, or this sense of this conflict based on power, struggle,

101
00:10:24,000 --> 00:10:29,000
and desire for dominance, oppression, this kind of thing.

102
00:10:29,000 --> 00:10:32,000
Oppressing others, belittling others, that kind of thing.

103
00:10:32,000 --> 00:10:40,000
So you create something new, or you'd simply create a new plan for yourself.

104
00:10:40,000 --> 00:10:47,000
I deserve to be kings, so I'm going to fight my way to become king.

105
00:10:47,000 --> 00:10:53,000
I deserve this or that, or I don't deserve this or that.

106
00:10:53,000 --> 00:10:58,000
And so you strive accordingly.

107
00:10:58,000 --> 00:11:01,000
Sometimes it's just out of laziness.

108
00:11:01,000 --> 00:11:07,000
This gives rise to sleep, but it also gives rise to sickness.

109
00:11:07,000 --> 00:11:14,000
It gives rise to conflict and problems when you don't act according to your duties,

110
00:11:14,000 --> 00:11:18,000
where you just consume, but don't produce.

111
00:11:18,000 --> 00:11:22,000
And as a result, people become upset, because you're simply a consumer.

112
00:11:22,000 --> 00:11:28,000
A mooch, lazy, etc.

113
00:11:28,000 --> 00:11:30,000
And so on.

114
00:11:30,000 --> 00:11:32,000
So this is the idea of becoming.

115
00:11:32,000 --> 00:11:34,000
It's really anything that leads to.

116
00:11:34,000 --> 00:11:38,000
So there was a question recently about what's wrong with,

117
00:11:38,000 --> 00:11:44,000
or there's always questions about what's wrong with seeking out pleasure or ordinary pleasure,

118
00:11:44,000 --> 00:11:47,000
because you do get some.

119
00:11:47,000 --> 00:11:59,000
And it's really this, to avert these two verses point to the mind set or the outlook of the Buddha

120
00:11:59,000 --> 00:12:03,000
and the Buddhists in general, is that it's not enough.

121
00:12:03,000 --> 00:12:08,000
And it's not sustainable, because the more you want, as I said, the more you suffer,

122
00:12:08,000 --> 00:12:12,000
the less you get, or the less satisfied you are.

123
00:12:12,000 --> 00:12:14,000
And in fact, it's the wrong way.

124
00:12:14,000 --> 00:12:16,000
It's unsustainable.

125
00:12:16,000 --> 00:12:19,000
You find yourself bouncing back and forth, or for a time,

126
00:12:19,000 --> 00:12:23,000
you're able to balance things before you fall one way or the other,

127
00:12:23,000 --> 00:12:25,000
and then it's back and forth again.

128
00:12:25,000 --> 00:12:28,000
And it's building up.

129
00:12:28,000 --> 00:12:37,000
And then disappointment, building up attachment, and stress, or it's working for a time to,

130
00:12:37,000 --> 00:12:45,000
to, well, to either repress, often it's simply to repress our desires.

131
00:12:45,000 --> 00:12:47,000
And we fluctuate back and forth.

132
00:12:47,000 --> 00:12:51,000
And go through life, where we go through lives again and again and again,

133
00:12:51,000 --> 00:12:58,000
and our born, old, sick, die, born, old, sick, die.

134
00:12:58,000 --> 00:13:05,000
So this is sort of the outlook that the Buddha is basing this on.

135
00:13:05,000 --> 00:13:11,000
These two verses are actually one thing that stands out for me,

136
00:13:11,000 --> 00:13:17,000
besides the obvious symbolism or imagery, which is quite powerful,

137
00:13:17,000 --> 00:13:22,000
is sort of congratulatory.

138
00:13:22,000 --> 00:13:31,000
It seemed like a good opportunity to congratulate all of the people involved with this community,

139
00:13:31,000 --> 00:13:36,000
all of the listeners, all of you who are listening to the Dharma, right?

140
00:13:36,000 --> 00:13:39,000
Taking the time at some of you every day too.

141
00:13:39,000 --> 00:13:41,000
It's not something I never thought would happen.

142
00:13:41,000 --> 00:13:44,000
I thought, okay, once a week, maybe.

143
00:13:44,000 --> 00:13:49,000
But some people actually come out every day to listen to the Dharma.

144
00:13:49,000 --> 00:13:54,000
You take the time to come online and sit still for an hour,

145
00:13:54,000 --> 00:13:58,000
half an hour an hour, even to ask questions,

146
00:13:58,000 --> 00:14:04,000
so to get involved, to perform this act of good karma of asking questions,

147
00:14:04,000 --> 00:14:10,000
and many of you are very respectful, and so being respectful,

148
00:14:10,000 --> 00:14:15,000
and all sorts of good karma going on.

149
00:14:15,000 --> 00:14:18,000
And that's something to be proud of,

150
00:14:18,000 --> 00:14:23,000
because even in the context of the story,

151
00:14:23,000 --> 00:14:26,000
many people, in the context of the story,

152
00:14:26,000 --> 00:14:30,000
these people are still to be congratulated because they tried.

153
00:14:30,000 --> 00:14:34,000
And it's not like they didn't hear some of the Dharma.

154
00:14:34,000 --> 00:14:37,000
It's just they tried to do something beyond their capabilities.

155
00:14:37,000 --> 00:14:43,000
But all of the people here should be congratulated as well,

156
00:14:43,000 --> 00:14:46,000
for having the good intentions.

157
00:14:46,000 --> 00:14:53,000
Many people don't even think about crossing.

158
00:14:53,000 --> 00:14:57,000
Or if they think about crossing, they never do anything to try to cross.

159
00:14:57,000 --> 00:14:59,000
They would never come and meditate.

160
00:14:59,000 --> 00:15:01,000
If you look at the list of meditators that we have,

161
00:15:01,000 --> 00:15:06,000
we've got to see how many we have tonight.

162
00:15:06,000 --> 00:15:10,000
We've got, actually, a fairly small list today.

163
00:15:10,000 --> 00:15:12,000
Oh, and a bunch of orange people.

164
00:15:12,000 --> 00:15:14,000
Maybe I spoke to you soon.

165
00:15:14,000 --> 00:15:17,000
What happened today?

166
00:15:17,000 --> 00:15:18,000
Well, it's still.

167
00:15:18,000 --> 00:15:20,000
We have many, many people,

168
00:15:20,000 --> 00:15:26,000
and we have people meditating on this side around the clock.

169
00:15:26,000 --> 00:15:33,000
People coming to listen to the Dharma every day.

170
00:15:33,000 --> 00:15:36,000
So this is a sign of at least wanting to cross.

171
00:15:36,000 --> 00:15:38,000
Maybe some people just listen to the talks,

172
00:15:38,000 --> 00:15:40,000
or watch my YouTube videos,

173
00:15:40,000 --> 00:15:43,000
and maybe dip their foot in the water,

174
00:15:43,000 --> 00:15:45,000
but don't ever cross.

175
00:15:45,000 --> 00:15:47,000
But at the very least,

176
00:15:47,000 --> 00:15:48,000
there's some thought,

177
00:15:48,000 --> 00:15:50,000
and this is the first step.

178
00:15:50,000 --> 00:15:52,000
The first step is thinking about it,

179
00:15:52,000 --> 00:15:59,000
having the giving rise to the intention,

180
00:15:59,000 --> 00:16:02,000
or the desire to better oneself.

181
00:16:02,000 --> 00:16:05,000
Is it supposed to just running up and down the shore?

182
00:16:05,000 --> 00:16:08,000
Because that's what it's like.

183
00:16:08,000 --> 00:16:11,000
That's what the Buddha likens most of our activity to,

184
00:16:11,000 --> 00:16:14,000
just running up and down the shore.

185
00:16:14,000 --> 00:16:18,000
Sometimes running up and down out of desire.

186
00:16:18,000 --> 00:16:21,000
Sometimes running up and down out of anger,

187
00:16:21,000 --> 00:16:25,000
fear, worry, conceit.

188
00:16:25,000 --> 00:16:35,000
Many ways of running around in circles.

189
00:16:35,000 --> 00:16:38,000
Running around in circles in the kingdom of death.

190
00:16:38,000 --> 00:16:40,000
This is where we find ourselves being born again,

191
00:16:40,000 --> 00:16:45,000
and again, dying again, again.

192
00:16:45,000 --> 00:16:55,000
Learning and forgetting.

193
00:16:55,000 --> 00:16:57,000
Not really learning from our mistakes.

194
00:16:57,000 --> 00:17:00,000
Our intention here, our practice here,

195
00:17:00,000 --> 00:17:03,000
is to break that, to cross over,

196
00:17:03,000 --> 00:17:07,000
to go beyond death,

197
00:17:07,000 --> 00:17:09,000
to figure the system out,

198
00:17:09,000 --> 00:17:11,000
to come to understand the system,

199
00:17:11,000 --> 00:17:14,000
and to understand becoming

200
00:17:14,000 --> 00:17:17,000
and as a result, not be dependent upon it.

201
00:17:17,000 --> 00:17:20,000
One way of explaining why we're dependent upon it is

202
00:17:20,000 --> 00:17:22,000
because we don't understand it.

203
00:17:22,000 --> 00:17:26,000
We're dependent upon it because it runs us.

204
00:17:26,000 --> 00:17:27,000
We don't run it.

205
00:17:27,000 --> 00:17:30,000
We don't lord over death.

206
00:17:30,000 --> 00:17:32,000
Death lords over us.

207
00:17:32,000 --> 00:17:35,000
Becoming as well.

208
00:17:35,000 --> 00:17:37,000
We act in such a way,

209
00:17:37,000 --> 00:17:46,000
usually that we don't fully understand the system.

210
00:17:46,000 --> 00:17:50,000
We don't fully grasp what we're dealing with,

211
00:17:50,000 --> 00:17:57,000
or what's in store for us when we chase after things.

212
00:17:57,000 --> 00:18:01,000
We're not clear in our minds as to the nature

213
00:18:01,000 --> 00:18:05,000
of our addictions and our versions and our conceits

214
00:18:05,000 --> 00:18:07,000
and our views.

215
00:18:07,000 --> 00:18:10,000
We hold on to them often without a clear understanding,

216
00:18:10,000 --> 00:18:12,000
or usually without a clear understanding.

217
00:18:12,000 --> 00:18:14,000
Once we understand them clearly,

218
00:18:14,000 --> 00:18:17,000
this is the crossing over.

219
00:18:17,000 --> 00:18:24,000
This is the rising above.

220
00:18:24,000 --> 00:18:27,000
It's rather a poetic verse.

221
00:18:27,000 --> 00:18:29,000
Simple verse, really.

222
00:18:29,000 --> 00:18:31,000
Simple meaning.

223
00:18:31,000 --> 00:18:36,000
It's kind of hard teaching.

224
00:18:36,000 --> 00:18:39,000
I said,

225
00:18:39,000 --> 00:18:42,000
it lately is bare the fact that most of what we do

226
00:18:42,000 --> 00:18:44,000
is just running up and down the shore.

227
00:18:44,000 --> 00:18:48,000
It doesn't really accomplish anything in the end.

228
00:18:48,000 --> 00:18:50,000
But we should, on the other hand,

229
00:18:50,000 --> 00:18:52,000
it should be an encouraging teaching because

230
00:18:52,000 --> 00:18:54,000
whatever the good things that we do,

231
00:18:54,000 --> 00:18:58,000
this is our coming closer to be one of those few people

232
00:18:58,000 --> 00:19:01,000
that was actually able to cross over.

233
00:19:01,000 --> 00:19:04,000
So actually actually able to rise above

234
00:19:04,000 --> 00:19:07,000
and free themselves from the wheel of suffering

235
00:19:07,000 --> 00:19:10,000
from the kingdom of death.

236
00:19:10,000 --> 00:19:14,000
So good work, everyone, and thank you all for tuning in

237
00:19:14,000 --> 00:19:19,000
and for supporting these teachings with your practice,

238
00:19:19,000 --> 00:19:23,000
for practicing and continuing on this practice.

239
00:19:23,000 --> 00:19:26,000
So that's the Dhammapada teaching for tonight.

240
00:19:26,000 --> 00:19:28,000
Thank you all for tuning in.

241
00:19:28,000 --> 00:19:52,000
Keep practicing.

