1
00:00:00,000 --> 00:00:07,840
Welcome back to Buddhism 101. Today we finish up our discussion of morality. The final aspect

2
00:00:07,840 --> 00:00:17,240
of morality refers to right livelihood. So we'll be looking today at how our livelihood

3
00:00:17,240 --> 00:00:26,360
plays a part in our protection of the mind through our actions and our speech. The first

4
00:00:26,360 --> 00:00:33,720
aspect of livelihood in regards to how it affects our meditation is these types of

5
00:00:33,720 --> 00:00:44,920
livelihood that will, by their very nature, tend to lean to distraction and potentially

6
00:00:44,920 --> 00:00:51,320
corruption of the mind. And there's five types of livelihood that are considered to be

7
00:00:51,320 --> 00:00:56,720
wrong livelihood in that sense, in the sense that they affect your spiritual well-being.

8
00:00:56,720 --> 00:01:05,000
The first is the selling of weapons. So I'm selling handguns and so on is considered

9
00:01:05,000 --> 00:01:11,440
to be wrong livelihood. Second is the selling of animals with live animals or human beings

10
00:01:11,440 --> 00:01:17,320
slavery of course. But animals are considered to be beings just like humans and so engaging

11
00:01:17,320 --> 00:01:24,440
in the trade of animals has ethical ramifications, especially in terms of having to cage

12
00:01:24,440 --> 00:01:36,200
them and so on. And the selling of animals is considered to be problematic. Meet, selling

13
00:01:36,200 --> 00:01:40,080
meat is considered to be problematic because you're dealing with killing and you have

14
00:01:40,080 --> 00:01:45,720
to engage with butchers and so on. So the type of people you'd be engaging in and of

15
00:01:45,720 --> 00:01:52,560
course the activity meat of course is problematic at best. Selling intoxicants of course

16
00:01:52,560 --> 00:01:57,520
is wrong livelihood because you're dealing with drugs and you're getting people drunk,

17
00:01:57,520 --> 00:02:05,960
you're making money off of people's bad habits, something that leads them to intoxication.

18
00:02:05,960 --> 00:02:13,880
And finally, selling poison. So if you're selling any rat poison or chemicals I meant

19
00:02:13,880 --> 00:02:19,480
for killing like pesticides or so on, this sort of thing is you're selling things that

20
00:02:19,480 --> 00:02:26,080
are immoral. All of these things should be fairly clear that you're making money off

21
00:02:26,080 --> 00:02:32,920
of immorality. And again, the point there is that these activities, the actual selling

22
00:02:32,920 --> 00:02:37,640
itself, the act of selling is not immoral. You're just moving stuff around, taking money

23
00:02:37,640 --> 00:02:43,360
and giving stuff. But because of the nature of them it's very close to closely involved

24
00:02:43,360 --> 00:02:48,720
with immoral activity and therefore highly unadvisable, considered to be wrong livelihood

25
00:02:48,720 --> 00:02:56,320
in a Buddhist sense. But on a more general level, the essence of wrong livelihood, which

26
00:02:56,320 --> 00:03:03,520
we'll be talking about later when we get into the full noble path, refers to any type

27
00:03:03,520 --> 00:03:11,000
of livelihood, any means of furthering your existence or your own well-being through

28
00:03:11,000 --> 00:03:17,640
wrong speech or wrong action. And so that refers back to the five precepts and engagement

29
00:03:17,640 --> 00:03:26,520
in immoral activities like killing, stealing, cheating, lying. If any of these activities

30
00:03:26,520 --> 00:03:33,680
are used for the furtherance of one's livelihood, then it's considered to be wrong livelihood.

31
00:03:33,680 --> 00:03:38,800
But Buddhism goes even deeper than that. And if you want to really understand right

32
00:03:38,800 --> 00:03:45,240
livelihood in a meditator sense, a meditator shouldn't engage in any type of livelihood

33
00:03:45,240 --> 00:03:51,600
whatsoever. Right livelihood means the giving up of seeking for material possessions,

34
00:03:51,600 --> 00:04:00,000
even to the extent of one's personal survival. So what this means is doing what you

35
00:04:00,000 --> 00:04:08,280
do and acting and living your life, whether it be during a meditation course or on a

36
00:04:08,280 --> 00:04:16,160
broader scale as a monastic, as a monk, as a recluse, by not providing or not seeking

37
00:04:16,160 --> 00:04:22,760
out remuneration for your actions, not doing things for the purpose of getting something

38
00:04:22,760 --> 00:04:28,280
in return, so not having any livelihood whatsoever. And this is how monks subsist monks

39
00:04:28,280 --> 00:04:34,280
will, we take vows to give up money, to give up any of the type of bartering, any type

40
00:04:34,280 --> 00:04:41,680
of trading. And so we teach and whenever, whenever, when we teach, we do it without expectation

41
00:04:41,680 --> 00:04:48,120
of remuneration or reimbursement of any kind. And we live our lives not hoping for people

42
00:04:48,120 --> 00:04:56,080
to praise us or to honor us or to support us in any way. We do it fully aware that we may

43
00:04:56,080 --> 00:05:03,200
receive no support, that we may, we may starve, we may go, we cold, we may be homeless

44
00:05:03,200 --> 00:05:19,120
and so on. And it's a powerful sort of determination or state of mind of total renunciation

45
00:05:19,120 --> 00:05:25,760
where you let, essentially, you let your karma take over, you let the universe take over.

46
00:05:25,760 --> 00:05:30,920
And if people think to give you food or shelter or support you, then they can do it

47
00:05:30,920 --> 00:05:35,600
of their own regard and you make a determination in your mind to be free from any sort

48
00:05:35,600 --> 00:05:43,720
of need for livelihood. Now, for most people, this is confined to a meditation course.

49
00:05:43,720 --> 00:05:50,040
So during the meditation course, you accept whatever you're given as a result, as a part

50
00:05:50,040 --> 00:05:54,480
of the meditation course, when people give you food, you eat, when they would ever shelter,

51
00:05:54,480 --> 00:05:59,560
they give you whatever room they give you, whatever things you're provided with during

52
00:05:59,560 --> 00:06:03,840
the meditation course. Of course, give up your livelihood base. And the point is, no conducting

53
00:06:03,840 --> 00:06:11,520
business during the course. But in essence, what we're moving towards and what a meditator

54
00:06:11,520 --> 00:06:16,280
moves towards is less and less livelihood, less and less interest in one's own personal

55
00:06:16,280 --> 00:06:24,920
well-being and more and more interest in acceptance, accepting things as they are. Right

56
00:06:24,920 --> 00:06:30,280
and livelihood for a monastic is to give up any sort of seeking. And so it's true that

57
00:06:30,280 --> 00:06:37,800
monks are allowed to go on arms round and so to the extent of providing for our existence,

58
00:06:37,800 --> 00:06:42,800
we can walk through the village and see, essentially see whether anyone is looking to give

59
00:06:42,800 --> 00:06:48,520
food on that day. So the closest we would get to actually seeking out food is to go and

60
00:06:48,520 --> 00:06:54,240
see if there's anyone who wants to give, not going and asking or going and begging, but

61
00:06:54,240 --> 00:06:59,200
simply going and seeing if there's anyone and we're allowed, of course, to take it, give

62
00:06:59,200 --> 00:07:05,440
us food. But essentially having no interest in livelihood. And this is an important determination.

63
00:07:05,440 --> 00:07:10,720
It helps us to let go of this distraction and the greed and the attachment to personal

64
00:07:10,720 --> 00:07:18,080
comfort and so on. Now, so this is the final aspect of morality. And again, I just want

65
00:07:18,080 --> 00:07:25,360
to recap because some of this might seem quite extreme. Going back to the use of the requisites,

66
00:07:25,360 --> 00:07:30,640
people are commenting that it seems quite extreme to give up everything and to just live

67
00:07:30,640 --> 00:07:36,160
a bear in simple life. But remember, all of these things are not meant to be rules or

68
00:07:36,160 --> 00:07:41,760
commandments. They're meant to be supporting precepts, so things that we undertake voluntarily

69
00:07:41,760 --> 00:07:45,360
in order to better our meditation practice. And there are things that the Buddha warned

70
00:07:45,360 --> 00:07:52,000
would get in the way of our practice if we engaged in them. So the abandoning, the refraining

71
00:07:52,000 --> 00:07:58,960
from certain things and the more we can control and stabilize our activity, our bodily activity

72
00:07:58,960 --> 00:08:03,600
and our speech, the more it will support our meditation practice. So all four of these aspects

73
00:08:03,600 --> 00:08:08,880
of morality are really guidelines. And you can take them as far as you want. You can practice them

74
00:08:08,880 --> 00:08:13,440
as little or as much as you like. And the point being that the more you practice them, the more

75
00:08:13,440 --> 00:08:18,960
controlled you are and the more focused you are, the more likely it's going to lead to

76
00:08:18,960 --> 00:08:24,400
concentration and therefore wisdom. And so it's for our own benefit. And if you don't like them,

77
00:08:24,400 --> 00:08:32,400
you don't have to keep them, but you've been warned basically. So this is an essential first step

78
00:08:32,400 --> 00:08:42,480
is to understand basically the boundaries outside of which we shouldn't trespass if we want to,

79
00:08:42,480 --> 00:08:48,160
or sort of the realm in which we have to work in order to cultivate concentration and wisdom.

80
00:08:48,160 --> 00:08:56,880
This is the realm of morality. So basic concept that's important to learn at the very outset

81
00:08:56,880 --> 00:09:03,920
of our practice of the Buddha's teaching. So there's another concept on our path to understand

82
00:09:03,920 --> 00:09:19,840
Buddhism on a basic level. Thank you for tuning in. See you next time.

