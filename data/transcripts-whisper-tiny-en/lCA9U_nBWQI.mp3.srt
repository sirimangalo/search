1
00:00:00,000 --> 00:00:03,000
Hello, welcome back to Ask a Monk.

2
00:00:03,000 --> 00:00:12,000
I assume some of you have noticed that I haven't really been posting in the past few months.

3
00:00:12,000 --> 00:00:19,000
And it's mainly because I've been trying to take some time alone.

4
00:00:19,000 --> 00:00:24,000
And there have been other engagements.

5
00:00:24,000 --> 00:00:27,000
I was traveling a little bit and I had to take it.

6
00:00:27,000 --> 00:00:32,000
I'll be traveling to America.

7
00:00:32,000 --> 00:00:36,000
So yeah, it's kind of broken up.

8
00:00:36,000 --> 00:00:41,000
I'm not just sitting around making videos.

9
00:00:41,000 --> 00:00:49,000
So one thing I wanted to say is that for people who are waiting for the next videos or looking for me to make more videos,

10
00:00:49,000 --> 00:01:00,000
I just want to be sure to be clear that the purpose is not to listen to the Buddhist teaching or to listen to talks.

11
00:01:00,000 --> 00:01:13,000
People who download MP3's vast libraries of Dhammatops or who read the whole of the Buddhist teaching again and again.

12
00:01:13,000 --> 00:01:23,000
It's not really the point. So the questions that have been asked the answers that I'm giving, I'm hoping that they're for the purpose of taking away to practice meditation.

13
00:01:23,000 --> 00:01:31,000
So really in the end it's not about how many questions are answered or how many videos are posted, how many talks you listen to.

14
00:01:31,000 --> 00:01:41,000
It's how much time you actually spend practicing and how clear is your mind and your awareness of the present moment.

15
00:01:41,000 --> 00:01:49,000
So if you're looking for the next step, the next step is not just to sit around waiting for more videos.

16
00:01:49,000 --> 00:01:56,000
I will be doing more, but there's a lot of questions people have been asking.

17
00:01:56,000 --> 00:02:01,000
The next step is to find a way to take time to actually practice.

18
00:02:01,000 --> 00:02:08,000
And once you've really begun to engage in the meditation practice, there aren't nearly so many questions.

19
00:02:08,000 --> 00:02:12,000
Most of the questions that you have answered themselves.

20
00:02:12,000 --> 00:02:26,000
So just to make sure that I'm happy to answer questions, but it's not really worth it if people just come back with more and more questions rather than seeking out the answers, which are ultimately within.

21
00:02:26,000 --> 00:02:39,000
So, but another thing that comes up, which is great about having this forum to answer questions, is that there are a lot of technical questions, which can't be answered.

22
00:02:39,000 --> 00:02:47,000
You can't sit down and practice and very easily come up with the technical answer, which should I be doing here, which should I be doing there?

23
00:02:47,000 --> 00:02:57,000
Often, because it's quite subjective, you know, what is the technique here? And unless you try it and test it out one way, you won't be able to tell whether it's more beneficial to do it this way.

24
00:02:57,000 --> 00:03:00,000
So technical questions are always welcome.

25
00:03:00,000 --> 00:03:02,000
Yeah, all questions are welcome.

26
00:03:02,000 --> 00:03:08,000
I just don't spend all your time asking questions.

27
00:03:08,000 --> 00:03:11,000
We should spend most of your time finding the answers to them.

28
00:03:11,000 --> 00:03:17,000
So here are some answers to a couple of technical questions. This should be short.

29
00:03:17,000 --> 00:03:27,000
First of all, when you say your mind shouldn't be in our mind, shouldn't be in our head, but in our stomach when we contemplate rising and falling, what do you mean?

30
00:03:27,000 --> 00:03:31,000
Can you elaborate it more?

31
00:03:31,000 --> 00:03:42,000
The mind is not physical. The mind is that quality of experience or that aspect of experience that knows that is aware of something.

32
00:03:42,000 --> 00:03:51,000
So when I say that the mind should be in the stomach, that's not really correct because the mind doesn't take up space and it doesn't go here or there.

33
00:03:51,000 --> 00:03:58,000
So it isn't one moment in the head, one moment in the stomach. It doesn't move. It is aware of the object.

34
00:03:58,000 --> 00:04:02,000
The idea of space only arises when you're talking about the physical.

35
00:04:02,000 --> 00:04:12,000
And that's really, in a sense, only a concept. The ultimate reality is the experience, which has a physical end and a mental end.

36
00:04:12,000 --> 00:04:21,000
And that doesn't take up space. Space is only a convention when we're talking about the physical realm.

37
00:04:21,000 --> 00:04:31,000
So what I mean by that is that it should feel like the mind is in the stomach in the sense that you're really knowing the rising.

38
00:04:31,000 --> 00:04:37,000
So when you say to yourself, rising, that's the mind recognizing.

39
00:04:37,000 --> 00:04:46,000
This is the rising. It's the mind with a clear awareness of what that is because this is what the mind does when it becomes aware of something.

40
00:04:46,000 --> 00:04:55,000
It immediately recognizes it as, well, first of all, as the basic realities of it seeing, hearing, smelling, tasting, feeling, thinking.

41
00:04:55,000 --> 00:05:14,000
But generally it will go on to recognize it as good, bad, me, mind, a source of pleasure, a source of pain, something that is beneficial, something that is harmful, and so it makes all sorts of judgments and categorizations and it creates all sorts of intentions based on this.

42
00:05:14,000 --> 00:05:20,000
And eventually leads to suffering and stress and busyness.

43
00:05:20,000 --> 00:05:27,000
So what we're trying to do in the meditation is to simply recognize it and to stop there.

44
00:05:27,000 --> 00:05:33,000
So the response in the mind should only be rising. It shouldn't be a rising.

45
00:05:33,000 --> 00:05:53,000
You know, my stomach is smooth, the rising is smooth or it's stuck and it's uncomfortable or this or that or the idea of trying to, I should make it longer, I should make it shorter, I should try to keep it a constant speed and so on.

46
00:05:53,000 --> 00:06:00,000
None of these things will arise if you're simply recognizing what it is.

47
00:06:00,000 --> 00:06:19,000
So the point of saying that it should be in the stomach is to avoid making it a mental exercise where you put the brain to work and start to create a concept or a conventional or a mental creation based on the rising.

48
00:06:19,000 --> 00:06:27,000
Because it's very easy to sit there and say to yourself, rising, falling, but it's very difficult to actually know that this is the rising.

49
00:06:27,000 --> 00:06:35,000
This is the falling and to have a strong awareness. Normally our awareness of the rising or the falling or anything is very superficial.

50
00:06:35,000 --> 00:06:49,000
So we know it and then we're off on a tangent thinking we're back up in the head, working with actually with the chemicals in the brain to create pleasure and the ones that create pain and stress and so on.

51
00:06:49,000 --> 00:07:03,000
And the interactions with our thoughts and our reactions to what should have been a very simple object which would be the rising.

52
00:07:03,000 --> 00:07:15,000
So it should feel like the mind is in the rising in the stomach because there is a clear awareness at that moment of a rising motion which we understand to occur in the stomach.

53
00:07:15,000 --> 00:07:34,000
So you can really tell the difference between simply saying it and you know it for a second and then you're up in the brain's rising and falling and actually knowing rising and that's really the trick.

54
00:07:34,000 --> 00:07:43,000
It's something that's quite difficult if you've never done it. It's something that takes time to perfect and time to develop.

55
00:07:43,000 --> 00:07:48,000
Okay and another question I'm going to pack them together here because they're fairly simple.

56
00:07:48,000 --> 00:07:54,000
Second one is sorry could you please answer a few questions. Well it's actually one question.

57
00:07:54,000 --> 00:07:56,000
Oh two questions.

58
00:07:56,000 --> 00:08:04,000
When meditating how to breathe can only breathe through your nose or inhale through the nose exhale through your mouth?

59
00:08:04,000 --> 00:08:22,000
Okay how to breathe? I suppose the simplest answer is to say however you normally breathe when you're relaxed, when you're not thinking and when you're not forcing the breath, when you're not running, when you're sitting still normally, how do you breathe?

60
00:08:22,000 --> 00:08:34,000
Because the idea of breathing and breathing in through your nose, inhale through the nose and exhale through the mouth. That sounds like a construct.

61
00:08:34,000 --> 00:08:44,000
It sounds like you're actively trying conscious. You're making a conscious effort to breathe in one and breathe out the other. We normally don't breathe that way.

62
00:08:44,000 --> 00:09:10,000
If you subconsciously, unconsciously breathe that way then there's no problem with it. But as soon as you start to say okay in through the mouth, into the nose, out through the mouth, or in the mouth, out through the nose or however you want to do it, if you start to say opening your mouth and trying to breathe through the mouth when normally you breathe through the nose or vice versa, then your mind doesn't really with the present moment.

63
00:09:10,000 --> 00:09:20,000
It isn't with the breath, it isn't with the stomach, it isn't with the body with reality. It's on this intention. Okay now through the mouth, out through the nose, it's not natural.

64
00:09:20,000 --> 00:09:30,000
So you have this process on top of the observation. You're no longer simply observing the motion, which is really what we're trying to do here.

65
00:09:30,000 --> 00:09:44,000
And it should be clearly understood that that's where the practice is. The practice is in simply observing a motion that is already occurring. And so in the technique that I follow it, it doesn't really matter what goes on up here.

66
00:09:44,000 --> 00:10:03,000
We're not focused on this aspect of the breathing. We're focused on the stomach aspect of the contact with the body and the expansion of the body based on the breath going in and the contraction based on it going out.

67
00:10:03,000 --> 00:10:15,000
So it shouldn't have any difference. But if you're practicing mindfulness of breathing, I would say you're focusing on the nose. I would say at the same moment, you're focusing on the breath area.

68
00:10:15,000 --> 00:10:23,000
Then you shouldn't try to control the nature of the breath you should be watching.

69
00:10:23,000 --> 00:10:52,000
And I suppose if you were practicing for another purpose, if your purpose was to develop certain super mundane or supernatural states of magical powers or develop great strength of mind or great bliss and special meditative states, as opposed to simply trying to understand things as they are, then some control can be useful.

70
00:10:52,000 --> 00:11:06,000
Because it develops your concentration, you know, pratana yoga, prana yoga, I don't know how they say it. They're very much into deep breaths and when I did martial arts, we were into deep breaths and so on.

71
00:11:06,000 --> 00:11:12,000
And the idea was to get your breath, slowly your breath down to one minute for breath and so on.

72
00:11:12,000 --> 00:11:37,000
So in that case, you know, some people even I think can breathe in through one nostril and out through the other, they have down to such a side. And this gives you great power. Now this is not what we're looking for here because this power is most often accompanied by delusion, the ego, the idea that I, that this is me, this is, I am doing it with the control idea.

73
00:11:37,000 --> 00:11:50,000
And it works for a while, but eventually it breaks apart and it breaks down falls apart and disappears. And so it, it isn't really a self or an ego or me or mine.

74
00:11:50,000 --> 00:12:01,000
And this kind of delusion is simply a waste of time and worse it leads you on the wrong path. It keeps you from understanding things and letting go of things, seeing things as they are.

75
00:12:01,000 --> 00:12:10,000
So, no, the breath should be natural, it should be however it happens, I would say however it happens when you're asleep.

76
00:12:10,000 --> 00:12:20,000
However it happens when you're just relaxed, when you're not thinking about it, it should continue on in that manner.

77
00:12:20,000 --> 00:12:28,000
And what you'll see is the difference between your normal breath and your meditative breath is the problem.

78
00:12:28,000 --> 00:12:35,000
That when you're meditating that your breath is forced, your breath is controlled and that's the problem. And that's why we're meditating.

79
00:12:35,000 --> 00:12:47,000
Because anytime we focus on something, there'd be something outside of us, anything we pay attention to it, we can't, we're unable ordinary beings to simply observe and be aware of it.

80
00:12:47,000 --> 00:12:53,000
We have to control, we have to obtain, possess and so on. And this is the problem.

81
00:12:53,000 --> 00:13:03,000
And until we learn that this is suffering, this is a cause for stress, that this is a cause for all of the troubles in our lives.

82
00:13:03,000 --> 00:13:08,000
Well, then we'll always have difficulties, troubles and stress.

83
00:13:08,000 --> 00:13:20,000
Once we realize this, then our breath will come very much back to normal, our whole body, our whole brain, the whole of our being will be very much more natural.

84
00:13:20,000 --> 00:13:33,000
And when we focus on when we are aware of something, it won't change. When we're aware of the breath, the stomach rising and falling will be as if we weren't even paying attention to it. It won't change in the slightest.

85
00:13:33,000 --> 00:13:40,000
Now this is important. This is really what we're aiming for.

86
00:13:40,000 --> 00:13:53,000
So the breath definitely not a good idea to try to force it in one way or another unless you're practicing those kinds of meditation.

87
00:13:53,000 --> 00:14:05,000
If you're trying to understand things as they are, let the breath go as it will. And notice when it's not natural, when it's not ordinary, when it's not unconditioned.

88
00:14:05,000 --> 00:14:13,000
And you'll see that that's where there is delusion. There is the idea of control, forcing and so on. And it's a habit.

89
00:14:13,000 --> 00:14:17,000
It's something that we've developed and that we're trying to do away with.

90
00:14:17,000 --> 00:14:24,000
Because once you see that it's unpleasant that it's a cause for suffering, this forcing, this control, you'll let go of it.

91
00:14:24,000 --> 00:14:30,000
After you repeatedly observe this, eventually your mind will change the habit.

92
00:14:30,000 --> 00:14:35,000
It will say, no, this is not leading to happiness. It's actually leading to stress and suffering.

93
00:14:35,000 --> 00:14:39,000
Okay, so there's two questions. I'll try to answer some more now.

94
00:14:39,000 --> 00:14:47,000
Actually, I'm thinking I'm going to bring my video camera, this little portable video camera that this was donated.

95
00:14:47,000 --> 00:14:56,000
Three years ago, and has seen a lot of use in this. Good little camera. I think I'll bring it with me when I go to America.

96
00:14:56,000 --> 00:15:04,000
And because I'm not going to be doing so much, so probably I'll have some time to answer some videos on the road.

97
00:15:04,000 --> 00:15:12,000
Answer some questions on the road. I think I've still got about 100 questions to answer. I'm sorry.

98
00:15:12,000 --> 00:15:17,000
I mean, I'd like to open it up. I'd like to take questions and answer questions that people have.

99
00:15:17,000 --> 00:15:27,000
What do you do when you've got 100 waiting? It just keeps piling up and piling up, so you'll have to bear with me.

100
00:15:27,000 --> 00:15:34,000
And I know a lot of people have, as a result of closing it down or stopping to accept questions.

101
00:15:34,000 --> 00:15:39,000
They've started sending questions to me directly, which I really can't do. That's the reason for...

102
00:15:39,000 --> 00:15:47,000
That's even worse for me because then I have to answer the same question when each person asks it.

103
00:15:47,000 --> 00:15:59,000
And so I find myself with a far bigger workload than I'm actually actually able to handle.

104
00:15:59,000 --> 00:16:07,000
So this is the best I can do. If you really have questions, and this is what I'd really like to see.

105
00:16:07,000 --> 00:16:13,000
Come on out. Come on over to Sri Lanka. It's a beautiful place.

106
00:16:13,000 --> 00:16:22,000
I haven't seen any. When I say one poisonous snake, a couple of scorpions, lots of leeches, but that doesn't matter.

107
00:16:22,000 --> 00:16:36,000
No worries here. If you come on over, you can ask all the questions you like, and I'm happy to answer them, as long as you dedicate yourself to the meditation practice and join us in our practice.

108
00:16:36,000 --> 00:16:39,000
Okay, so thanks for tuning in all the best.

