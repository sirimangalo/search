Good evening, everyone, broadcasting live to some pretense, 2015.
What happened today?
Yesterday we had our weekly meditation at the university.
That was kind of neat.
Always nice.
We had two newcomers.
So I taught two new people how to meditate.
The owner of the house where we're living is trying to sell his house.
But from the sounds of it, the person buying it would want us to be in the house.
He kind of made a joke that he said, they are usually looking for houses that already have tenants,
but that he can only guarantee us till the end of this contract.
And he said, oh, well, that's okay.
We might be looking for another place anyway.
Somehow, I didn't think I quite said it like that.
But he actually asked, I think,
you know, would you want to stay another?
Are you planning to stay around?
And I said, I think so.
And then he said, would you want to extend the contract?
And I said, well, you know, we might be looking for another place anyway.
I think he got a little bit concerned when I brought that up.
So it doesn't sound like they want us to move.
But we'll have to see.
This place actually works quite well.
So we have a quote today.
It's actually a pretty awesome quote.
It's one of those hardcore quotes.
But I get the feeling that he's translating this poorly.
So let's take a look at this.
So it's confusing, really, if you read it, right?
He says, and what is the burden?
I would teach the burden, the taking hold of the burden,
the taking up of it, and the putting it down.
That's not what it actually says.
Because that's confusing.
The number two and number three seem almost the same, taking hold of it,
taking it up.
That's not what it says.
It says, I will teach you the burden, the taker up of it.
So there's the burden, and there's the one who carries the burden.
So if you look at the verse, the five aggregates are the burden,
the Caesar of the burden is the person.
The holder actually, it's not Caesar, it's not.
Again, he's translating very poorly.
It's not harder as him to see, it's harder as to carry.
And that's really to the point, right?
You've got this burden.
So the question is, who carries it?
And the person is the one who carries it.
And who is the one who carries the burden?
And the answer is the person of such and such a name of such and such a village.
You see?
So there's nothing to do with the taking hold.
It's the one who carries.
So the one who carries the burden is the person, this person, that person.
And that's a double, there's a double meaning there.
As people we carry the burden of the five aggregates, so we have experience,
which is basically a burden, having to deal with life and challenges and difficulties and so on.
But also, it's the identification with them that causes all of our trouble, right?
So the whole idea of the person, not just the fact that a person has to carry the five aggregate,
the person itself is the problem.
So it's what makes these things the burden.
Because we identify with them, we react to them, we want to fix them,
want to solve our problems and so on.
Is there our problems, right?
Even we talk about our problems, but then we also have our families' problems in our society's problems,
our countries' problems, our worlds' problems.
So we take on so much more and more and more as our own.
Now everyone's worried about the planet.
You don't have to worry about the planet, it would be here long after we're gone.
You see, it's the whole eye, we're worried about ourselves really.
So ordinarily, the first stages we take ourselves to be a cell.
This is me.
This is my body.
But then we go even further and we say,
this is my computer, or this is my robe, or this is my house.
Then you see a person and say, well, that's my child, my husband, my wife, my father, my mother.
Then you go further and say, well, this is my city.
This is my country.
This is my world.
And it's all exactly the same sort of clinging.
And that becomes a burden.
Barra, how we bunch up content.
So we take on our five aggregates and then we take on other people's five aggregates,
and we take on the whole world's five aggregates.
That's our own.
And we take on our own, our own, our own, our own, our own, our own.
If one lays down this heavy burden, having laid down that,
if having laid down this heavy burden,
one should not take another, maybe one should not take.
Not taking up another burden.
Samul-Hang, Samul-Hang, Samul-Hang,
Ma-Bui-Ha.
I'll take that, mate.
Right.
Pulled up, craving by the root.
With its roots.
Roots and all.
Together with its roots.
Nichato-parinimbuto.
Nichato.
Nichato means nichato.
Nichato-nichato-nichato.
Mm-hmm.
Having no hunger.
Having no desire.
Do we have any questions tonight?
We do.
And it was actually from a couple of days ago.
And it just moved off the panel because we had some new comments.
But I basically remember it.
It was a question about the Buddhist point of view of procreation.
Your thoughts on it.
I'm going to say you have to be a little more specific.
Yeah.
Procreation.
There were a couple of different parts of it.
One part was, wouldn't our hunt ever want to procreate once they became an our hunt?
What do you think, Robin?
I think no.
I think no.
Why not?
Well, there wouldn't really be a point to it.
I mean, the thing, the reason that we want to, you know, have children is to,
it just wouldn't, I don't think it would appeal to an our hunt because the reason that
we want to have children to have little mini-mes and have something to be
conceited about and have someone to love and to cling to.
And I don't think an our hunt would really be into all that.
I think the our hunt would be into certain things or want things that have purpose.
No, I don't think an our hunt would want children.
I don't know if they would want.
What would an our hunt want?
Food for the day?
I think they want that.
I think, I mean, I think an our hunt doesn't even want that.
I think it's simply just that they don't have any wanting.
So what keeps them from just lying down and dying?
There's not much. I mean, they do sit down quite a bit.
You know, sit still quite a bit.
But they're human beings.
So functionally, they act according to, according to what is appropriate.
You know, human beings are rocks.
So there is the,
you know, a lot of our intentions just come from the brain.
The brain saying, no, it's time to do this.
Now it's time to do that.
So the brain will suggest to do this or suggest to do that.
Normally we like that idea or we dislike it.
An our hunt won't have either.
So they'll do it if it's function or they'll approach it from a functional point of view.
They'll still say things that seem fairly kind of quirky.
You know, like quite unique to that person.
So they still in many ways will seem like that individual.
And that's why all our hunts are different because
a lot of it's just coming from the brain.
It's nothing to do with.
Desire.
But how would you explain mindfulness to a seven-year-old?
Well, I'd have them do exercises probably.
Wouldn't try to explain it.
I'd just talk to them about meditation and how it helps you know how to understand yourself.
Helps you clear your mind, helps you feel happy and calm.
Helps to be smart and wise.
And then I'd have them try to be mindful of something.
Just say, like, look at a blue color and same blue blue or so on.
And to get them used to this idea.
Because focusing on one's self is difficult for most seven-year-olds.
So it doesn't focus on the concept.
Like blue or white or yellow or red or something.
And once they could do that and then focusing on different kinds of concepts.
Then I would have them focus on themselves.
Once they get the idea.
So then I would have them focus rising for example.
I made some videos.
You can watch my videos that sort of my impression of how one
way that one could teach children.
Hey, how's the Florida thing going?
The Florida thing is going awesome.
People have donated over $1300 so far.
So that leaves about $1,000 or the children's home in Tampa.
That's going to be great.
Merry Christmas, everybody.
Merry, happy holidays.
I don't know, this whole Christmas thing, you know?
It's funny.
Someone was, I read an article today, a Jewish person.
I was just waiting for lunch and so I picked up the campus newsletter.
And she was going on about how she complains about Christmas.
And so on.
And why is it the default sort of right?
It's a good point, really.
Some people have tried to talk about something called festivals.
I think that's from Seinfeld, isn't it?
I think it's from Seinfeld.
Yeah, it was an episode.
I'm pretty sure it was something from Seinfeld.
Another part of popular culture that I'll never
can be involved, be a part of.
But no, the idea.
Because it's really, you know, bizarre that we would even talk about Christmas as non-Christians.
And yet you can't help it because it's everywhere.
I'm thinking a lot about Christmas recently because I don't really enjoy this time of year.
I never did even when I was Christian.
It just feels like madness.
So I'm glad when it's over.
But there's much more too.
I mean, there's so little to do with Jesus for us non-Christians.
Or men are not for most people.
It's not about Jesus.
It's about presents and Santa Claus and Christmas trees and candy canes.
It's about traditions and lots of traditions.
And pagan rituals.
Yeah.
I mean, not even.
No one thinks of them as pagan rituals.
There was a governor in Rhode Island.
And he changed the name of the Christmas tree, the state Christmas tree, to the state holiday tree.
People got all up in arms about it.
The holiday tree that was terrible.
Of course, the Christmas tree is a pagan tradition.
It doesn't have anything to do with Jesus.
Yeah.
He was in the mail to a cross.
It was a mail to a tree.
No.
There was no tree involved as far as I know.
Maybe it was a pine cross.
The yule log, I believe, is pagan ritual.
I mean, most of that is.
But people get very attached to it and don't change anything.
Don't change my traditions.
Well, many people think that many people in your country think that it's a Christian nation, right?
Yes.
I think it should be a Christian country.
Yes, you've heard all the talk.
I imagine of this week of my university.
Some students in my university were just today or yesterday.
The target of hate crimes.
Someone put like a knife in their door or something.
These five Muslim girls.
It's this thing now here in Hamilton.
These people.
And the worst thing is, yeah, there's a lot of people in some position of influence, including the media.
The whole point of terrorism is to make people afraid.
And so a way to give them what they want, right?
A way to make everyone afraid.
I mean, I think Islam has some some accounting to do.
And people in Islam have to make bold statements about which side they're on.
But I think we have to see people as people not identify them by their religion,
but identify them by their actions and their beliefs.
And we should give people the benefit of the doubt and trust people.
What was it? There's this.
There's this jotica where.
See if I can find this jotica for you all.
These two kings meet.
Let me see.
I'm trying to find another jotica. I need to find it.
About this bird, it tries to put out the forest fire by shaking its wing tips
and drop putting drops of water, dropping drops of water.
And find it.
So it's called the brave little parrot.
Yeah, you have it.
Tell me about it.
Five seconds.
Once a little parrot lived happily in a beautiful forest,
but one day without warning lightning flashed,
thunder crashed, and a dead tree burst into flames.
That's impermanence.
Sparks carried on the rising wind,
began to leap from branch to branch and tree to tree.
The little parrot smelled the smoke.
Fire she cried, run to the river.
Flapping her wings rising higher and higher,
she flew toward the safety of the river's far shore.
After all, she was a bird and could fly away.
But if she flew, she could see many animals were already surrounded
by the flames and could not escape.
Suddenly a desperate idea, a way to save them came to her,
darting to the river, she dipped herself in the water,
then she flew back over the now raging fire.
Big smoke coiled up filling the sky.
Walls of flame shot up.
Now and one side, now the other.
Those of fire leapt before her, twisting and turning through a mad maze of flame,
the little parrot flew briefly on.
Having reached the heart of the burning forest,
the little parrot shook her wings,
and the few tiny drops of water that still clung to her feathers,
tumbled like jewels, down into the flames and vanished with a hiss.
Then the little parrot flew back through the flames and smoked to the river,
once more she dipped herself in the cool water,
and flew back over the burning of forest.
Once more she shook her wings and a few drops of water tumbled like jewels into the flames.
Yes.
Back and forth she flew time and again from the river to the forest,
from the forest to the river.
Her feathers becoming charred, her feet in close were scorched, her lungs ached,
her eyes burned, her mind spun dizzily as a spinning spark,
still the little parrot flew on.
This goes on for a while more.
Keep going.
Can you tell me which jot the guy is?
It seems like it was kind of written for kids.
It doesn't actually give the reference.
But it's shame.
Yeah.
But it's a river, huh?
Let's see, maybe it was the river that I...
There's a lot of lightning, lightning struck the tree.
I mean, they've adapted that. That's very much adapted.
Yeah, there's a couple of different versions.
They look like they're written for kids.
But none of them references the original.
And this one.
Okay, I'm trying to shook himself.
Yeah.
I can keep reading this one while you're looking for the real one.
Sure.
But the little parrot continued to fly on through the smoke and flames.
She could hear the great eagle flying above her as the heat grew fiercer.
He called out, stop, foolish little parrot.
Stop, save yourself.
I don't need some great shining eagle,
off the little parrot to tell me that my own mother, the dear bird,
could have told me the same thing long ago.
Advice, I don't need advice.
I just need some help.
Rising higher, the eagle who as a god watched a little parrot flying through the flames,
high above he could see his own kind.
Those carefree gods still laughing and talking.
Even as many animals cried out in pain and fear far below,
he grew ashamed of the god's carefree life and the single desire was
kindled in his heart.
God, though I am, he exclaimed how I wish I could be just like that little parrot,
flying on brave and alone, risking all to help.
What a rare and marvelous thing.
What a wonderful little bird.
Excuse me.
Moved by these new feelings, the great eagle began to weep.
Stream after stream of sparkling tears began pouring from his eyes.
Wave upon wave, they fell, washing down like a torrent of rain upon the fire,
upon the forest, upon the animals and the little parrot herself.
Oh, wait, I'm not listening.
He's going to put it up with his tears.
Yes.
Where does...
That's not the original story.
Where are they going with it?
Where those cooling tears fell, the sparks shrank down and died,
smoke still curled up from the scorched earth,
yet now life was already boldly pushing forth.
Shoots, stems, blossoms and leaves.
Green grass sprang up all along the still glowing cinders.
Where the eagles tear drops sparkled on,
where the eagles tear drops sparkled on the little wink,
parrot swings, new feathers now grew.
Red feathers, green feathers, yellow feathers too.
Such bright colors, such a pretty bird.
The animals looked at one another in amazement.
They were whole and well.
No one had been harmed.
Up in the clear blue sky, they could see their brave faint friend,
a little parrot looping and soaring in delight.
When all hope was gone, some how she had saved them.
Hurry, they cried.
Hurry for the brave little parrot and the sudden miraculous rain.
That probably took some liberties with the original, I guess.
Yeah, in the end, it's Sakha who comes and the original Sakha who comes and
asks him, what do you hope to accomplish?
It's such an important one because it's the reply that he gives.
It's not whether you succeed.
It's the effort.
Now I'm doing it again.
This is all I can do and I will always do the most I can do.
I wonder if it's in Mahan Janaka.
So how did it end in the, how did it end in that shot of comfort?
Sakha puts out the fire for him.
With his ears?
Sorry.
No, no.
Sakha's King and the angels he can do magic.
Okay.
Okay.
So this is in regards to, so we're talking about how to deal with people who,
even people who are, who wish us evil.
So we have this story about the King of Kosula and he had passed a sentence in a very
difficult case involving moral wrong.
So he came to see the Buddha.
Buddha asked him why he came, why he comes.
And he came, he says, I come because I was sitting on a difficult case.
I was judging a difficult case involving moral wrong.
And the Buddha says, to judge it, cause with justice and impartiality is the right thing.
That is the way to heaven.
And he, he praises him.
He says, it's no wonder having me, having someone like me to give you advice.
It's no wonder that you can make wise judgments.
And on anyway, at the end he tells the story of the past that these two kings meet
in a place where the, where there's only room for one carriage.
So there's no room to pass.
So someone has to get out of the way.
And the first king says, get out of the way, make way.
This is the king of, this is the great king of King Malika.
And the other one says, well, this is the great king, Brahmadatta.
Brahmadatta is the Bodhisattva.
And then the king of Baneras says to himself, oh, here's another king, what should we
do?
And then he asks him, oh, he says, well, okay, here's an idea.
Whoever's older will, whoever's younger will make way.
And then they ask, turns out they're both the same.
And then they ask, he asks about their power, wealth, glory, and all these things,
trying to figure out who's the, the higher ranking king.
And it turns out they were both alike in power, wealth, and glory,
and the nature of their family, and the need.
But then they quickly comes up with something.
The king says, well, here's the thing.
We'll talk about our virtues.
Whoever is the more virtuous will get the road.
And the other one will have to get off the road for them.
And so the man, the driver, he asks his driver, tell this,
tell this other king your virtues, or my virtues, sorry, tell him about my virtues.
So the driver says, and here's the verse,
rough to rough King Malika, the mild with mildness,
sways, masters the good by goodness, and the bad with badness pays.
Give place, give place, or drivers, such are this monarch's way.
And the man of the king of Beneris says,
oh, is that all you have to say about your king's virtues?
And the one says, yes, if these are, and then the other says,
if these are his virtues, what must his vices be?
And the first guy says, vices be at them, if you will,
but let us hear what your king's virtues may be like.
And so the driver for the Bodhisattvas says, listen then,
and he repeats the second verse.
He conquers wrath by mildness, the bad with goodness,
sways, by gifts, the miser of anguishes,
and lies with truth, repays.
Give place, give place, or drivers, such are this monarch's way.
Really worth reading all these stories, no?
And at these words, both the king Malika and his driver
descended from their carriage, loosened the horses
and moved the carriage out of the way to give way
to the king of Beneris.
Then the king of Beneris, Hamadatta, and Bodhisattva,
gave an admonition to the other king, telling him to change.
There's a really good jataka, because those two verses
express two very different philosophies.
The first one is the eye for the eye,
the other one is to turn the other cheek.
And I know it's very much like what Jesus came to me.
He conquers wrath by mildness, the bad with goodness,
ways, by gifts, the miser of anguishes, and lies with truth,
repays.
Give place, give place, own driver, such are this monarch's way.
Did you find the parent one yet?
I didn't know.
Didn't see anything that referenced the original jataka.
Do you have time for a couple of questions, Monday?
Yep.
Monday, are you in need of anything currently?
Do you find yourself going through any gift cards faster than others?
Hmm. Well, I've got mostly Tim Hortons and subway gift cards.
I've only got a few Peter Pitt gift cards,
and I've started using that more.
I'm going through the...
I'm using a lot of the credit at McMaster up,
actually on campus.
But nothing is low.
I mean, I guess Peter Pitt, there's only...
I don't know how much.
There's still probably...
People have already paid for quite a bit.
There's over $100.
Credit, something.
But the rest of them are all much.
Starbucks, I only have $17, but I don't go there very often at all.
There's not like a drinking coffee or anything.
Although I had a coffee this morning, a small coffee.
It does help.
It helps with exams.
Whether that's appropriate or not, I don't know.
Honestly, if you have to write an exam, it seems to be useful.
Just in tomorrow, I'm not going to have one, so we'll see how that is.
Well, hopefully, just one day didn't get you too addicted to it.
I find even one day...
Really?
Yeah.
That the next day, it's like, oh, what's wrong?
Because it's...
It is a drug.
Yes, I don't.
Not a hundred percent on doing that, but on the other hand, my exam this morning was...
My mind was clear, skipping through it.
I even went over it again and found some really clever tricks they pulled on us in this exam.
Clever to the point where you think you got it.
And going over it again, you realize, oh, that's a trick.
They expected us to put that, like the difference between book.
Book is...
Libros...
Libros would be books.
So I put, where are you going to...
Where are you going to send all of your books after this difficult war or something like that?
That's how I translated it, but then I looked over again and it was Libros,
which means free ones.
But free ones is the idiom...
It's an idiomatic way of saying, or it's a colloquial way of saying, children.
Children are Liberty.
So Libros means the children.
Libros would have been the books.
Wow.
For a sense, where are you going to send your children?
Yeah.
So the brain was working and you got to say that the coffee,
just a small coffee.
But you can feel it.
Yeah, not a big deal.
It's this...
Most monks drink a lot of coffee.
Many monks drink coffee every day.
They don't want to get into the habit.
Yeah.
When you decide to give it up, it's...
I used to drink a lot of coffee and when you do give it up,
it's about one miserable week until you feel...
I drink decaf coffee now because I actually like drinking coffee or tea or anything hot.
But I'm just drinking decaf coffee and herbal tea.
Decaf coffee has some caffeine in it, but probably not much.
It does, yes.
I think it says 98% caffeine free.
So it's got about 2% of the caffeine of regular coffee.
Small amount.
When we...
In the...
In front of the exam today, I was talking with one of my classmates.
And he pulled...
He was talking when he said, I didn't sleep last night.
Like, wow.
And this afternoon he has another thing he has to hand in and he says,
that's why I have this and he pulled out this little energy drink.
And I was looking at it, it's got 200 milligrams of caffeine in this little bottle.
Yeah.
So how many milligrams of caffeine does one coffee have?
We were debating that.
I think 175.
Oh, so what?
I wonder so much.
Pardon?
200 is not much then.
Let's see.
There's more like 20 per coffee.
There's 10 coffees in there.
Maybe not.
Actually, this one says 95 milligrams for one 8 ounce coffee.
Okay.
So that would be about a 16 ounce coffee.
No, I thought it was a lot more.
Yeah, they even put caffeine in things like food now.
I heard of peanut butter with caffeine in it.
There's water with caffeine in it.
It's kind of crazy.
Okay.
More questions?
Yes.
What are thoughts on aggression?
Is it a necessary thing to get things done or to discipline?
What are thoughts on aggression?
Is it a necessary thing to get things done or to discipline focus?
Aggression may lead to suffering.
Some beings are naturally aggressive.
Is it dangerous?
It's imprecise.
What do you mean by aggression?
The whole idea of anyone being naturally anything is misleading.
Because it implies that God, it really, that kind of language is only,
it comes from Judeo-Christian theology, really.
You know, by nature.
What does that mean to a Buddhist?
It means nothing.
Unless you're talking about the universe by nature,
experience by nature.
But beings by nature.
Yeah.
I mean, we still use it, but we would have used it in Hinduism.
It comes from a fantastic idea that there's some order to the universe.
The only order is impermanence, you know.
So things can change.
This is what we learn in science that we're talking about,
natural selection or constant change in adapting adaptation.
So there's that.
But specifically talking about aggression,
you're talking about something imprecise.
It involves many different mind states.
Aggression might be a strong will paired with animosity or anger
and desire as well in there.
There's a lot of moments of minds that are connected with each other
associated with each other.
But there's much more complicated than just being aggressive.
The person is a type A, for example.
Well, you know, you have to talk about what exactly that means
on a momentary basis.
What minds states rise?
The person is ambitious.
What does that mean?
There's desire, but there's also maybe a strong attachment to one's
desire and the sense of believing, believing it right.
Ambition takes a lot of righteousness.
Don't believe something's really right.
If you don't think this is important,
you won't be ambitious about it.
How would you know if you're doing something out of desire or for functionality?
You have to be very mindful.
Very much of what we do is just functional.
You take my speech, for example.
I have the, maybe I have the desire to speak in the beginning,
but each movement on my lips doesn't take desire.
I don't have to want to move my lips to the front,
and then open my mouth wide.
There's one, and then the brain does most of the rest,
and the body does from muscle memory.
So all of that is functional.
When I walk, for example, I don't have to want to put one foot after the other,
but there was a one thing to go to class, for example.
Now, the mind can also be functional,
so there can be an intention to do something
that doesn't have desire in it.
I would say many of the things we do during the day are like that.
It wouldn't be like that.
It's actually, because the desire is, there's ubiquitous.
It's everywhere.
But they are reading a book.
Each time you decide, now I'm going to turn the page,
could have desire in it, but it doesn't have to.
It actually could.
Oh, what's on the next page, right?
Or even just slightly.
But if you're mindful, you can turn without desire.
I think one thing is, based on the Abidhamma,
you have to really have to really have some deep knowledge
of the Abidhamma to answer it exactly,
but I think the functionals are reserved for the Arahans,
because doing something mindfully, as a non-Arahan,
is considered wholesome.
Holesome in that, it's going to bring you closer to enlightenment.
But once you're enlightened, there's nowhere to go,
so it's not wholesome anymore.
There's these technicalities and so on.
So being mindful is just functional for an Arahan.
It's just the way they are.
Yeah, I mean, the point is when an Arahan does good deeds,
they're not actually wholesome.
They're called functional.
And some comments about coffee.
That trick was right, coffee has about 100.
I guess I was thinking of big coffees.
And dark roasts have about a third of the amount of caffeine of light roasts,
which seems counterintuitive,
apparently that's the way it is.
It's a big thing.
I mean, I think an argument that can be made for caffeine
is it doesn't intoxicate you.
So it's not a drug drug like that.
But I don't know.
I mean, I find it affects me.
I have to be quite mindful of the feelings that come.
I'm just even a happiness that comes from it or pleasure or so.
But if you do drink a lot of coffee, it can feel a little bit like intoxication.
If you really drink a lot of coffee, it can't focus.
You can't stay focused.
Yeah.
I mean, because they make this argument with nicotine,
I don't think you can really make either an argument.
I mean, I could make an argument that that caffeine helped me do my work, right?
So it was a tool, kind of.
Not a very good argument though.
Not convinced.
You'll find out tomorrow when you see how you feel without the caffeine.
Yeah.
Let me let's not have problem with this.
But that's true.
That's a good point.
What are the results, right?
Well, I guess it's a better argument.
If someone were to say I needed every day, then it's kind of a dependency.
But if you use it for a purpose,
there's an argument to be made there, I think.
So when will you be leaving for Florida one day?
21st, I think.
Still got two more exams.
Oh, well.
So next week as well?
Or are they both tomorrow?
Next week, and then on the day I leave.
So the morning before I leave, I've got an exam.
The 16th and the 21st.
Well, spaced out.
Is that all the questions?
That is all the questions.
All right.
Love.
Say good night then.
Thank you, Bhante.
If anyone can find that to Jotica and tell me which one it is,
may not even be the Jotica.
That might be the problem.
But I'm pretty sure it is.
They were referencing that it was a Jotica in these adopted.
The brave little parrot is from an ancient Jotica tale from India.
It's not saying which.
Maybe someone can find it.
Someone can find the actual Jotica.
A little bird stops a fire.
Many animals were too slow.
Many animals were too slow.
A lot of different versions in here and that one that I've come across is referencing the original
Jotica.
Thank you.
I'll find it.
Say good night everyone.
Thank you.
