1
00:00:00,000 --> 00:00:05,000
Well, sitting, the effort to come back to the abdomen eventually ruins my

2
00:00:05,000 --> 00:00:10,360
vigilance and concentration, whereas following experiences mindfully as they

3
00:00:10,360 --> 00:00:16,160
naturally unfold, helps me remain mindful. Isn't it contrary to the teachings?

4
00:00:16,160 --> 00:00:23,960
What should I do? No, it's not contrary to the teachings necessarily. Now again

5
00:00:23,960 --> 00:00:29,960
you have to be honest with yourself, which we often aren't, I'm sorry to say,

6
00:00:29,960 --> 00:00:40,400
when the problem is actually bringing the mind back, or when the problem is

7
00:00:40,400 --> 00:00:46,160
simply the suffering that you have in the mind and the disintegration to deal

8
00:00:46,160 --> 00:00:50,840
with it. So sometimes when we come back to the present moment, when we come back

9
00:00:50,840 --> 00:00:59,360
to the here and now, it's unpleasant, because our mind is inclined to go with it,

10
00:00:59,360 --> 00:01:08,160
to enjoy things, and to be fluid, and to just let the mind go as it let the

11
00:01:08,160 --> 00:01:14,560
mind go after chase after objects, which is disrupted by the practice of

12
00:01:14,560 --> 00:01:20,000
mindfulness. So there's two aspects. When you force your mind back to the

13
00:01:20,000 --> 00:01:26,680
stomach, that's wrong, because it's not being honest with yourself about the

14
00:01:26,680 --> 00:01:32,960
nature of reality, you're deluding yourself about the idea that you have some

15
00:01:32,960 --> 00:01:40,560
kind of control and can benefit from trying to force things. But by letting your

16
00:01:40,560 --> 00:01:49,040
mind go and just following after whatever whatever comes up, then you incline

17
00:01:49,040 --> 00:01:56,760
instead towards laziness and indulgence. There's the potential to indulge and

18
00:01:56,760 --> 00:02:04,640
to to enjoy experiences and give rise to liking and attachment. So one

19
00:02:04,640 --> 00:02:14,000
experience that comes up is not a, it's not ruining your vigilance, but it's

20
00:02:14,000 --> 00:02:21,160
going to ruin your sense of enjoyment, which we call concentration. So it's

21
00:02:21,160 --> 00:02:26,000
quite common for people to say this practice is ruining my concentration. It's

22
00:02:26,000 --> 00:02:30,800
designed to do that, because your concentration is concentration is not

23
00:02:30,800 --> 00:02:36,400
necessarily wholesome, you see. You can be concentrated on a bad thing. Angry

24
00:02:36,400 --> 00:02:42,320
people can be, maybe not from an ambidama point of view, but can be in some

25
00:02:42,320 --> 00:02:47,960
way very, very much focused. You can be very much focused on central desire,

26
00:02:47,960 --> 00:02:53,760
you know, you get caught up in watching a movie or listening to a song and

27
00:02:53,760 --> 00:03:01,440
you can become very much entranced by it in a bad way, you see, because there's

28
00:03:01,440 --> 00:03:08,120
a whole bunch of sensual attacks of the filement in there. And so by breaking

29
00:03:08,120 --> 00:03:14,120
that up, it's unpleasant. It's like crying like a child when the child gets

30
00:03:14,120 --> 00:03:19,920
what it wants. It's totally at peace and it's totally focused. And when you

31
00:03:19,920 --> 00:03:24,360
take, take its toy away from it, it starts to wind and complain. And so that's

32
00:03:24,360 --> 00:03:27,480
essentially what the mind does when you bring it back to the present moment, it

33
00:03:27,480 --> 00:03:33,480
winds and complains. So I was they oftentimes you're not actually forcing the

34
00:03:33,480 --> 00:03:38,280
mind back. You're just choosing something neutral, which the mind doesn't

35
00:03:38,280 --> 00:03:41,240
like. The mind doesn't want to come back to the stomach. That's boring. That's

36
00:03:41,240 --> 00:03:46,320
useless. That's uninteresting. And that's the point. The point is to keep you on

37
00:03:46,320 --> 00:03:51,920
something that's not going to intoxicate you, which the mind doesn't like.

38
00:03:51,920 --> 00:03:55,120
So the mind is like, I'd much more prefer to go after these interesting things

39
00:03:55,120 --> 00:03:59,800
and enjoyable things. And that's mostly what you're feeling. Now, if and when

40
00:03:59,800 --> 00:04:03,360
you're actually forcing them, it's not you, but that's mostly what people feel

41
00:04:03,360 --> 00:04:08,120
and I'd imagine in much of what you're feeling is that. That's my guess. But

42
00:04:08,120 --> 00:04:13,080
there are people who force themselves. So when something comes up, they ignore

43
00:04:13,080 --> 00:04:18,400
it and instead go back, force themselves to stay with one object. So that's not

44
00:04:18,400 --> 00:04:22,800
good. When something does come up, you're to acknowledge it. Now, once you've

45
00:04:22,800 --> 00:04:27,840
acknowledged it, best advice is to go back to the rising and falling without

46
00:04:27,840 --> 00:04:32,240
paying attention to what's going to come next. It will be uncomfortable to do

47
00:04:32,240 --> 00:04:36,200
that in many cases because the mind isn't interested in the rising and

48
00:04:36,200 --> 00:04:41,200
falling. The mind wants to chase after more enjoyable things, which is not

49
00:04:41,200 --> 00:04:46,320
beneficial, which is a problem because it encourages the

50
00:04:46,320 --> 00:04:52,160
filement and delusion and the idea of permanence of stability and so on,

51
00:04:52,160 --> 00:04:59,160
which we're all trying to, all of which we're trying to overcome.

