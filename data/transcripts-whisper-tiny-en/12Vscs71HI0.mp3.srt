1
00:00:00,000 --> 00:00:08,000
Welcome to Ask a Monk. Today's question comes from Isanene.

2
00:00:08,000 --> 00:00:13,000
I often find myself daydreaming with no hope of getting the situation I'm visualizing.

3
00:00:13,000 --> 00:00:20,000
As the enjoyment from the fantasy alone can seem not to carry the drawbacks of chasing material things.

4
00:00:20,000 --> 00:00:26,000
Can you explain your philosophical approach to great daydreams?

5
00:00:26,000 --> 00:00:47,000
Well, it's a bit of a strange sort of idea that just because something's immaterial doesn't mean that it doesn't have the same drawbacks of chasing material things.

6
00:00:47,000 --> 00:00:59,000
Obviously, chasing after things is craving is a clinging to them that there's not the impartial balance state of mind.

7
00:00:59,000 --> 00:01:06,000
The truth of the matter is that you have gotten what you want and that is the experience in the daydream.

8
00:01:06,000 --> 00:01:14,000
The reality according to Buddhism is experiential.

9
00:01:14,000 --> 00:01:24,000
The idea that something is immaterial is actually an illusion, I suppose, because everything is really immaterial.

10
00:01:24,000 --> 00:01:29,000
When you are attached to a site, you're attached to the seeing of the object.

11
00:01:29,000 --> 00:01:38,000
When you're attached to a sound, you're attached to the hearing of the object and the smelling, the tasting, the feeling, and or the thinking.

12
00:01:38,000 --> 00:01:42,000
So when you're daydreaming, there are a combination of things.

13
00:01:42,000 --> 00:01:50,000
There's the thinking, there's maybe the seeing, there's maybe even a concept of hearing something and so on.

14
00:01:50,000 --> 00:01:52,000
The point is not the stimulus.

15
00:01:52,000 --> 00:02:06,000
The point is the feeling that comes from it, which is a pleasure and enjoyment and the enjoyment of it, the liking of it, which takes one out of balance.

16
00:02:06,000 --> 00:02:18,000
If you admit that quote-unquote material things have a drawbacks, then there's no reason to think that a new material object would not have the same drawbacks.

17
00:02:18,000 --> 00:02:28,000
Since it's the very same clinging, to say that you're not getting, suppose you're daydream about food and to say that you're not getting the food is really inconsequential.

18
00:02:28,000 --> 00:02:39,000
There is a attainment of a pleasurable stimulus and the arising of the pleasure based on its stimulus.

19
00:02:39,000 --> 00:02:54,000
And then the liking, the enjoyment of it, which pulls one to one side out of balance and causes one to snap back into a state of upsetting and suffering.

20
00:02:54,000 --> 00:02:58,000
When one is without the objects of desire.

21
00:02:58,000 --> 00:03:15,000
So, I don't think there's any difference there. I hope that helps to explain the idea of getting or not getting in this case is not a factor because there's the attainment of pleasure.

22
00:03:15,000 --> 00:03:31,000
Okay.

