Okay, one rather simple question about eating and meditation.
For example, I am doing something which will take me half an hour, or hour to finish.
And after that I am thinking to do some meditation, but during this half hour I become hungry.
Should I eat and when I finish doing what I am doing meditate, or not eat and keep working,
then do the meditation on hungry stomach, or maybe just have a snack.
It is not exactly a simple question, there is a lot in there, that is a bit, I am not quite
clear on the whole of the question.
But one thing I would say, which is kind of smurmy, is that your work should be meditation
and your eating should be meditation.
So that is really where your answer, that is the most important part of the answer, it is
not really smurmy at all.
It is probably not the answer that most people would expect, but the meditation starts now.
It doesn't start 30 minutes from now when you are going to meditate, because then you are
wasting 30 minutes thinking it is okay, in 30 minutes I will meditate, or after this I will
meditate.
And during all that 30 minutes or hour you are wasting good meditation time.
So when you work you should try to be mindful, when you eat you should try to be mindful.
You should work on that.
Why it is not such a smurmy answer, after all is that that will really help your formal meditation.
If a person just does formal meditation and doesn't think at all about meditation during
their daily life, the formal meditation becomes quite difficult and it is a constant uphill
struggle, because it is out of tune with your ordinary reality.
So you should try to constantly be bringing your mind back to attention, I mean even right
now here we are sitting together, we should try to make it a mindful experience, being
aware at least of our emotions, likes and dislikes, the distractions in the mind, the feelings
that we have.
If you are just listening here, because I am talking I have to do more action, but for some
of you just listening you can just revert back to your meditation, unless you have questions.
But as far as me making decisions as to when you should do eating and when you should
do meditating, meditating on an empty stomach is interesting, can be useful, to help
you to learn about hunger.
But I'd say generally you need the food for the energy for the meditation, so better
to have a little bit of food before you meditate.
I don't meditate any time, whenever you meditate, meditate, get some time for it and do
it, just don't procrastinate, it kind of sounds like you might be talking about procrastinating,
which is dangerous, so it goes work for a half an hour and then I'll do, then I'll have
a snack and then when I'm having a snack I think about all, then I have to go check
Facebook or email and check email and then something comes up in email and I have to call
this person or that person or do this or I have to go to the store and then you never
meditate after all.
That kind of thing is dangerous, so better to be clear with yourself that if you really
need food and you're really hungry than eat or eat a little bit, but if you don't sometimes
it's just procrastinating because meditation can be difficult and the mind will actually
develop a version towards it if you're not careful and if you're not aware of the aversion
if you're not looking at it and contemplating it as well, it can become a great hindrance
and more than a hindrance it can actually trick you into every time you think of meditation
your mind says, oh he's thinking of meditation again, let's divert his attention, hey
look at that, that's nice, hey aren't you hungry, hey I want to go for burgers or something,
my mind will do this to you because it doesn't want amenities like oh no don't put me back
there, that's torture, so be careful of that one.
