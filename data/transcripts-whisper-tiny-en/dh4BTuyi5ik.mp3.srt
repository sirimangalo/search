1
00:00:00,000 --> 00:00:06,720
Hello and welcome back to our study of the Dhamupada.

2
00:00:06,720 --> 00:00:15,920
Tonight we're looking at verse 176, which means as follows.

3
00:00:15,920 --> 00:00:45,640
For one who transgresses one Dhamma, one thing, the person who

4
00:00:45,640 --> 00:01:01,600
speaks falsely, this one thing, reaching a parallocation, one who has discarded the after life,

5
00:01:01,600 --> 00:01:11,720
thrown away there after life, nati, papa, nakarya, and there is no evil they won't do.

6
00:01:11,720 --> 00:01:27,280
There is no evil that will not be done by such a person, some weighty words.

7
00:01:27,280 --> 00:01:32,840
So the Buddha told this person response to, and I keep saying this, but this is one of

8
00:01:32,840 --> 00:01:38,160
the more famous stories in Buddhist, I guess they're all pretty well known, but this

9
00:01:38,160 --> 00:01:44,520
one especially I think is one of the more well known in Asian culture, in Asian Buddhist

10
00:01:44,520 --> 00:01:47,800
culture.

11
00:01:47,800 --> 00:01:52,240
Probably not so much in the West, we don't know a lot of the stories, which is a shame,

12
00:01:52,240 --> 00:01:59,520
but we're trying to remedy that partly in this series.

13
00:01:59,520 --> 00:02:08,200
Now the story goes, as it's been passed down, that the Buddha was spreading his teachings

14
00:02:08,200 --> 00:02:23,520
throughout the area in Uttar Pradesh, Bihar, that area of the world, and his virtues

15
00:02:23,520 --> 00:02:34,640
were being spread and he was becoming quite famous and he was honored by kings and rich

16
00:02:34,640 --> 00:02:42,480
people alike and so his teaching was flourishing, because of just how great it was, I don't

17
00:02:42,480 --> 00:02:47,160
think there should be any doubt that that was the case considering how great his teaching

18
00:02:47,160 --> 00:02:57,320
is, for most of the people who have been watching these videos since the beginning, it's

19
00:02:57,320 --> 00:03:01,160
quite clear that there's some greatness to the Buddha's teaching, for people who have

20
00:03:01,160 --> 00:03:12,440
practiced the meditation in this tradition, they can easily agree that there's no wonder

21
00:03:12,440 --> 00:03:17,480
that Buddhism is such a powerful force in the world, even in the midst of a lot of other

22
00:03:17,480 --> 00:03:28,440
powerful forces.

23
00:03:28,440 --> 00:03:34,280
And speaking of powerful forces, even in India, they weren't all Buddhists, in fact, of

24
00:03:34,280 --> 00:03:42,320
course when the Buddha rose, there were already religious traditions and religious teachers

25
00:03:42,320 --> 00:03:51,960
present, and they weren't very happy about the Buddha's fame, they became quite disturbed

26
00:03:51,960 --> 00:04:01,080
by it, so the text says they would gather in the street and chastise people for being

27
00:04:01,080 --> 00:04:12,360
so taken by the Buddha, saying, it's the monk Gautama, the only Buddha, we also are Buddhists.

28
00:04:12,360 --> 00:04:16,520
That alone, which is given to him, yield abundant fruit, which is given to us, returns

29
00:04:16,520 --> 00:04:17,520
abundant fruit.

30
00:04:17,520 --> 00:04:24,320
So this was important for people, they supported religious people and there was a tradition

31
00:04:24,320 --> 00:04:34,240
of supporting the religion that you believed would bear fruit, for a lot of people they're

32
00:04:34,240 --> 00:04:40,560
not thinking about becoming enlightened, they just want a better life, and there was

33
00:04:40,560 --> 00:04:45,480
a belief and still is a belief that I think is in some ways well founded, in some ways

34
00:04:45,480 --> 00:04:52,960
it's embellished, I think by religious people that if you give to a religious person

35
00:04:52,960 --> 00:05:00,800
there's some good karma that comes from it, you get benefits back, we certainly, Buddha

36
00:05:00,800 --> 00:05:04,520
certainly affirmed that sort of a thing, and I'll talk a little bit more, I think it's

37
00:05:04,520 --> 00:05:10,920
actually an interesting lesson, but I think we have to be careful not to embellish this

38
00:05:10,920 --> 00:05:11,920
idea.

39
00:05:11,920 --> 00:05:18,560
Anyway, this was people's thinking and these monks were like, we'll give to us, support

40
00:05:18,560 --> 00:05:31,120
us, give us all these monasteries and food and roads, we're Buddhists as well.

41
00:05:31,120 --> 00:05:35,440
But didn't work, of course, because they're teachings, I mean this is the thing, not all

42
00:05:35,440 --> 00:05:39,960
teachings, it should go without saying and it shouldn't need to be, it shouldn't need

43
00:05:39,960 --> 00:05:47,520
to be said, but not all teachings are alike, anyone who says that all religions are alike

44
00:05:47,520 --> 00:05:52,760
or teach the same thing, I've said this before, it's a nice thought that it certainly

45
00:05:52,760 --> 00:05:59,040
is useful, practical, and when you're living amongst, when you're living in a multicultural,

46
00:05:59,040 --> 00:06:10,120
multi-religious society, otherwise you just fight, but it's not really true, I mean it's

47
00:06:10,120 --> 00:06:15,280
true that there are teachings out there that are kind of bizarre and there are meditation

48
00:06:15,280 --> 00:06:22,720
teachings that certainly don't lead to great things, there are others that lead to great

49
00:06:22,720 --> 00:06:29,240
things, but not to the greatest thing, so it's not even fair to say that all meditation

50
00:06:29,240 --> 00:06:36,440
teachings are the same, all spiritual paths lead to the same goal, it's not really true,

51
00:06:36,440 --> 00:06:40,120
there are many goals and there are practices that don't even lead to the goals that they

52
00:06:40,120 --> 00:06:47,880
say, so we will claim that the Buddha's teaching is that which leads to the highest goal,

53
00:06:47,880 --> 00:06:53,360
all these other teachings they might do good, they might be good, but they don't lead

54
00:06:53,360 --> 00:07:02,840
to the ultimate goal, anyway, at the time there was a woman who was ordained in one of

55
00:07:02,840 --> 00:07:09,000
these traditions, which is neat, I mean even outside of Buddhism at the time, it wasn't

56
00:07:09,000 --> 00:07:16,000
that women weren't allowed or weren't found practicing spiritual life, even outside of

57
00:07:16,000 --> 00:07:23,840
the Buddha's teaching it seems there were women rare, I guess, there were women who undertook

58
00:07:23,840 --> 00:07:30,000
to practice spirituality, I mean that shouldn't be a surprise, it's kind of we make Indians

59
00:07:30,000 --> 00:07:40,320
society out to be ancient Indian society, out to be very anti-woman or some patriarchal

60
00:07:40,320 --> 00:07:46,840
or sexist, and I think there's definitely something to that, I don't think even the

61
00:07:46,840 --> 00:07:59,360
Buddha was free from that sort of thinking, simply because of cultural nature of the culture,

62
00:07:59,360 --> 00:08:08,800
but anyway, it's not to say that women certainly were not in any way less interested

63
00:08:08,800 --> 00:08:16,000
in enlightenment, we can say that for sure, so this woman unfortunately found her way into

64
00:08:16,000 --> 00:08:23,680
the wrong group, we can say with some certainty it was the wrong group even if you think

65
00:08:23,680 --> 00:08:27,120
some of these other groups had their heads on straight, this group certainly didn't,

66
00:08:27,120 --> 00:08:35,960
they were very jealous and so one day they thought ah well she'll help us, well we'll

67
00:08:35,960 --> 00:08:45,760
get her to find a way to cast a reproach upon the monk go to the, yes that is the way

68
00:08:45,760 --> 00:08:51,160
and so when she came to the monastery they made this whole ordeal out of it, they just sat

69
00:08:51,160 --> 00:08:56,680
there and wouldn't talk to her, they see paid respect to them and they kind of just turned

70
00:08:56,680 --> 00:09:03,000
away, they wouldn't talk to her and she said what have I done, have I done anything wrong?

71
00:09:03,000 --> 00:09:10,800
They wouldn't talk to her, they said what don't you know, there was an act really,

72
00:09:10,800 --> 00:09:17,880
they don't you know, there's go to Mahdi's ruining us, everyone's falling for his

73
00:09:17,880 --> 00:09:30,160
hunting words, his sweet tongue, something about him, he's charming them or so on and

74
00:09:30,160 --> 00:09:37,720
he says oh she said I know I don't know who that is but can I help, can I help you

75
00:09:37,720 --> 00:09:49,160
in some way and they said well if you wish as well find a way to cast reproach upon the

76
00:09:49,160 --> 00:09:54,480
monk go to man, put an end to the gain and honor bestowed upon him, find some way to,

77
00:09:54,480 --> 00:10:02,840
this smirk is good name, okay I will do that, I will take the responsibility, she seems

78
00:10:02,840 --> 00:10:08,160
to have not been a very good person either so it's reasonable to understand, it's an easy

79
00:10:08,160 --> 00:10:14,480
to understand how she got caught up with these losers, she appears to have been a bit of

80
00:10:14,480 --> 00:10:32,640
one herself and so she undertook to some kind of subterfuge what she did was in the beginning

81
00:10:32,640 --> 00:10:37,520
she would just go and spend the night near the monastery or she would walk to the monastery

82
00:10:37,520 --> 00:10:42,320
when everyone was coming back so every evening the Buddha would give a talk and in the

83
00:10:42,320 --> 00:10:47,040
evening when they were all coming back it kind of late night perhaps she would walk towards

84
00:10:47,040 --> 00:10:51,640
the monastery and people would ask where are you going and she's in the beginning she said

85
00:10:51,640 --> 00:10:59,760
I don't, I'm not, it's private something like that and just something actually to make

86
00:10:59,760 --> 00:11:03,560
them suspicious you know you don't tell them where you're going and they even get even

87
00:11:03,560 --> 00:11:08,240
more suspicious and then she would go and sleep somewhere near the monastery or the

88
00:11:08,240 --> 00:11:13,160
Buddha was staying and in the morning when the people would come to give food and listen

89
00:11:13,160 --> 00:11:18,560
to the Buddha's teaching in the morning she would walk away from the monastery and he said

90
00:11:18,560 --> 00:11:23,680
where are you coming from and again she would say that's my own business and she did

91
00:11:23,680 --> 00:11:29,560
this for like a month and after a month she started telling people and they asked her

92
00:11:29,560 --> 00:11:39,080
she would say I'm staying with, I'm staying with Gautama staying with him in his, in his

93
00:11:39,080 --> 00:11:51,440
good day and unfortunately as we all know some not everyone is discerning enough to see the

94
00:11:51,440 --> 00:11:57,880
truths of such statements it's not impossible it's not even probably all that hard to

95
00:11:57,880 --> 00:12:04,840
fool the majority or a good majority of people you can see this in politics it's not really

96
00:12:04,840 --> 00:12:12,480
to cast aspersions on such people but sometimes you're trying to live your life you don't

97
00:12:12,480 --> 00:12:20,680
have the time to investigate them you find yourself listening and especially for people who

98
00:12:20,680 --> 00:12:26,880
didn't have a strong knowledge of the Buddha's teaching or a strong sense of the goodness

99
00:12:26,880 --> 00:12:32,400
and the rightness of it for them it was quite easy to convince them or at least make them

100
00:12:32,400 --> 00:12:38,880
suspicious didn't convince them but they became very suspicious and they said to themselves

101
00:12:38,880 --> 00:12:44,240
is it true or is it false they were they hadn't doubt and doubt is of course a problematic

102
00:12:44,240 --> 00:12:55,000
mind state and then after three or four months of that so it was a real long con she wrapped

103
00:12:55,000 --> 00:13:02,720
her belly up with bandages and went about you know so to make her stomach a little bit larger

104
00:13:02,720 --> 00:13:10,320
and she went around saying oh I'm pregnant I'm carrying Gautama's child and again people

105
00:13:10,320 --> 00:13:16,040
became more suspicious and some people lost interest in the Buddha and you know how that

106
00:13:16,040 --> 00:13:23,800
goes right a reputation is so easy to lose it's not entirely true but it's easy to hurt

107
00:13:23,800 --> 00:13:33,800
someone's reputation it's such a fickle thing and then after eight or nine months she put

108
00:13:33,800 --> 00:13:42,560
some wood a big actually a big half sphere I guess or something in her belly to I guess

109
00:13:42,560 --> 00:13:49,360
to put some weight and drew a cloak over it and then she beat herself like pounded her arms

110
00:13:49,360 --> 00:14:00,880
and legs to make her make them swell so she appeared swollen and then pretending to be tired

111
00:14:00,880 --> 00:14:07,120
she went went one evening and this is the climax of the story went to where the Buddha

112
00:14:07,120 --> 00:14:11,600
was teaching so in front of all the people and Buddha was there with a hall full of people

113
00:14:11,600 --> 00:14:23,440
teaching and stood before him and she gave quite a speech she said oh look at this great

114
00:14:23,440 --> 00:14:31,960
group of people you've you come to teach you have a sweet voice soft lips she actually

115
00:14:31,960 --> 00:14:40,520
says that I guess you have soft lips but you're the father of my child and I'm getting

116
00:14:40,520 --> 00:14:47,920
ready to deliver my time of delivery as soon in spite of this you haven't supported

117
00:14:47,920 --> 00:15:00,160
me you make no effort to give me the requirements yeah the other so on and she says you

118
00:15:00,160 --> 00:15:04,160
know how well enough how to take your pleasure but you know not how to look after the

119
00:15:04,160 --> 00:15:12,760
child you have forgotten thus did she reviled the Buddha can you imagine remember the theme

120
00:15:12,760 --> 00:15:26,160
here is lies well as lies go this is a pretty I mean yeah it's a pretty severe one I mean

121
00:15:26,160 --> 00:15:30,360
towards the Buddha for such a great and powerful being from Buddhist point of view there's

122
00:15:30,360 --> 00:15:37,720
not much worse you can do and the Buddha stopped teaching then he looked at her what do

123
00:15:37,720 --> 00:15:42,440
you think he did do you think he denied it it's interesting sometimes when you deny it it makes

124
00:15:42,440 --> 00:15:50,720
it worse but what he said to her and they say it's like a lion's roar a lion's roar it's

125
00:15:50,720 --> 00:15:55,560
just like a speech that is very powerful and the most powerful speech here I mean I think

126
00:15:55,560 --> 00:16:04,520
it is quite powerful is to call upon the truth and he said to her he says to her in regards

127
00:16:04,520 --> 00:16:21,040
to this matter you know the truth and I know the truth that was his response and you didn't

128
00:16:21,040 --> 00:16:27,720
let up she says so who is the side between what is true and what is false when only you

129
00:16:27,720 --> 00:16:37,600
and I know and then suddenly I'm skipping a bit here but we'll say suddenly through some

130
00:16:37,600 --> 00:16:48,040
means or other fortuitous circumstance the piece of wood slipped and fell on her feet

131
00:16:48,040 --> 00:16:53,520
and the text says it cuts or cut her feet or toes off but I'm going to leave that part

132
00:16:53,520 --> 00:16:57,560
out as well it fell on her feet and we'll say she got hurt suddenly just slipped out

133
00:16:57,560 --> 00:17:01,000
through whoops

134
00:17:01,000 --> 00:17:06,000
suffice to say that people listening to the Buddha again we're talking about ordinary

135
00:17:06,000 --> 00:17:12,400
people not just that not only people who are from meditation practitioners in light

136
00:17:12,400 --> 00:17:18,040
and beings and so in some of them we're not in light and by any means and some of them

137
00:17:18,040 --> 00:17:24,440
had perhaps been harboring suspicions and now they were very angry at being fooled and

138
00:17:24,440 --> 00:17:29,400
so these people got up and chased her out of the monastery beating her hitting her and

139
00:17:29,400 --> 00:17:37,880
so on and the story goes that she went to hell the story goes the earth opened up underneath

140
00:17:37,880 --> 00:17:44,280
her feet and swallowed her whole but either way I think it's fair to assume that she did

141
00:17:44,280 --> 00:17:51,240
in fact go to hell as a result that's the story the Buddha tells a past life story where

142
00:17:51,240 --> 00:17:59,760
a similar thing happened it's an interesting story but that's for another day and I encourage

143
00:17:59,760 --> 00:18:06,840
you if you're interested to read the chapter of stories associated with these but we're

144
00:18:06,840 --> 00:18:11,880
going to get into the lesson which more interested in what this means to us as meditators

145
00:18:11,880 --> 00:18:24,480
and Buddhists so again I think there are two different lessons the first one relates I think

146
00:18:24,480 --> 00:18:38,880
it's interesting to us relates to prosperity and how remarkable it is that people think

147
00:18:38,880 --> 00:18:48,520
as these wandering recklessness thought that you can become prosperous through evil but

148
00:18:48,520 --> 00:19:00,480
somehow evil needs lies in this case and worse than lies malicious lies designed to harm

149
00:19:00,480 --> 00:19:08,880
someone to greatly harm someone the idea that that could lead to prosperity is quite

150
00:19:08,880 --> 00:19:19,360
remarkable and so it brings a broader question to the fore about prosperity through

151
00:19:19,360 --> 00:19:27,920
evil in general I mean isn't it true that a person who does an evil deed prospered

152
00:19:27,920 --> 00:19:37,400
it's a true question I suppose but it's an argument that's put forth I mean well certainly

153
00:19:37,400 --> 00:19:45,360
true that a person who steals gains right in the short term of course the arguments

154
00:19:45,360 --> 00:19:58,700
are there other arguments involved but there's a question of karma because if a person

155
00:19:58,700 --> 00:20:04,680
steals from you I didn't why did they the person who did the bad deed gain and why do

156
00:20:04,680 --> 00:20:10,480
I the person who never did any bad deed lose so the broader picture of course would tell

157
00:20:10,480 --> 00:20:21,640
us more but more to the point I think we have to understand that we have to ask the question

158
00:20:21,640 --> 00:20:32,480
of whether the theft is that which allowed for the the profit and so I think I want to explain

159
00:20:32,480 --> 00:20:36,800
this because I think it's more than that and this helps us understand karma and it helps

160
00:20:36,800 --> 00:20:45,760
us understand many things like meditation even because to say that the theft is what allowed

161
00:20:45,760 --> 00:20:52,320
the person the thief to prosper is actually missing a lot of the story because if you talk

162
00:20:52,320 --> 00:20:59,960
about what it takes to steal take two people one person is incompetent and the other person

163
00:20:59,960 --> 00:21:10,200
is competent one person is clever the other person is not clever so take these two people

164
00:21:10,200 --> 00:21:15,560
and ask yourself which one is going to succeed in stealing which one is more likely to succeed

165
00:21:15,560 --> 00:21:23,960
more often in stealing in doing any bad deed right take a weak person and a strong person

166
00:21:23,960 --> 00:21:32,800
which one is going to succeed in things like torture or rape or that sort of thing so what

167
00:21:32,800 --> 00:21:37,680
I'm getting at here is these are actually all positive qualities physical and mental

168
00:21:37,680 --> 00:21:43,760
qualities the differentiate being and putting aside physical qualities we just focus on

169
00:21:43,760 --> 00:21:50,640
mental qualities a thief needs things like confident so what we're actually talking about

170
00:21:50,640 --> 00:21:56,680
is capabilities and if you want to think of it karma is kind of we talk about the karma

171
00:21:56,680 --> 00:22:02,200
bank and people wonder whether karma is like a bank they put money in in a way at kind

172
00:22:02,200 --> 00:22:10,240
of is as long as we understand what we mean by by karma and by the bank but if you think

173
00:22:10,240 --> 00:22:17,680
of it as like a system of points merit and demerit points what what they really are

174
00:22:17,680 --> 00:22:25,040
is abilities or capabilities right like if you if you practice meditation you become more

175
00:22:25,040 --> 00:22:31,240
confident if you like there's these books that teach you how to be how to be confident

176
00:22:31,240 --> 00:22:36,360
right and they'll tell you to recite mantras like I can do it I can do it that sort of

177
00:22:36,360 --> 00:22:40,160
thing and they'll tell you all sorts of things but you'll find that most often it's

178
00:22:40,160 --> 00:22:49,320
a sort of meditation it's a means of mental development if you want to be focused right

179
00:22:49,320 --> 00:22:54,920
the person is not focused would make a very bad evil doer because you have to focus on

180
00:22:54,920 --> 00:23:03,080
the task at hand and not get distracted and you have to be composed or else you'll tell

181
00:23:03,080 --> 00:23:09,560
the wrong person and get found out and that sort of thing and all of these things come from

182
00:23:09,560 --> 00:23:14,200
not only meditation but they come actually from good deeds a person who does lots of good

183
00:23:14,200 --> 00:23:20,640
deeds for others it's going to become more confident and stronger of mind if you look

184
00:23:20,640 --> 00:23:30,360
conversely to what things like theft do for a person does theft make you more confident

185
00:23:30,360 --> 00:23:36,360
and more focused does it make you happier I think it's a question we have to ask because

186
00:23:36,360 --> 00:23:41,320
you say well the person got what they wanted they got what they wanted because they had

187
00:23:41,320 --> 00:23:45,400
the capabilities they didn't get it because they stole that they got it because they were

188
00:23:45,400 --> 00:23:51,240
capable they were in a position to steal it and they chose but the act of stealing what

189
00:23:51,240 --> 00:23:56,880
are the results of that and because there's no there's no question here from a Buddhist

190
00:23:56,880 --> 00:24:02,520
perspective that the stealing was a bad act and it will only have bad consequences so

191
00:24:02,520 --> 00:24:09,400
the stealing itself the consequences for the mind are quite profound now I was once tried

192
00:24:09,400 --> 00:24:17,720
to teach meditation to a thief a guy who was guilty of theft and I don't think he ever

193
00:24:17,720 --> 00:24:25,840
got caught for it but he couldn't sit still he was really in a bad state his mind was totally

194
00:24:25,840 --> 00:24:34,480
unfocused I always bring up crime and punishment this novel I think sums it up quite well

195
00:24:34,480 --> 00:24:43,200
if you do a very very evil deans there's no escaping them and of course many other things

196
00:24:43,200 --> 00:24:49,880
of course you you can get caught and you'll be tortured and punished and so on and

197
00:24:49,880 --> 00:24:54,960
and then another interesting aspect is you make the world a worse place right a person

198
00:24:54,960 --> 00:24:59,920
who take a thief a thief is someone who likes the things of this world who wants them

199
00:24:59,920 --> 00:25:06,960
who craves them not all thieves but you know the average thief is desperate so desperate

200
00:25:06,960 --> 00:25:15,040
for the things of this world that they will steal them they will take them against the wishes

201
00:25:15,040 --> 00:25:22,640
of others and so their attachment to this world is certainly going to keep them in this world

202
00:25:22,640 --> 00:25:28,720
and by being a thief have they made the world a better place no a world with thieves

203
00:25:28,720 --> 00:25:32,720
is it better or worse I think there's no question that it's worse and that even just from the

204
00:25:32,720 --> 00:25:39,360
sense of people are less comfortable they lock their doors right I grew up in the middle of

205
00:25:39,360 --> 00:25:45,040
nowhere and we never locked our doors and we always bragged about it but I thought often as a kid

206
00:25:45,040 --> 00:25:50,320
about that you know we would visit the city and they would lock their doors and we think wow

207
00:25:50,320 --> 00:25:55,680
people actually lock their doors and places never we didn't have to you know the nearest

208
00:25:55,680 --> 00:25:58,880
neighbor was a mile away they didn't lock their doors either

209
00:26:03,120 --> 00:26:08,960
when you start locking your doors and worrying about your kids and you know you have to worry about

210
00:26:08,960 --> 00:26:16,400
young girls because for the worst reasons right you have to guard your money and guard your bank

211
00:26:16,400 --> 00:26:22,320
accounts guard your internet guard everything this is what the world does to us this is what

212
00:26:22,320 --> 00:26:27,440
evil does to the world and guess who inherits the world those people who are desperate enough for

213
00:26:27,440 --> 00:26:33,600
it that they're willing to go to extremes they're the ones who are sure to be here or even in

214
00:26:33,600 --> 00:26:44,720
worst places that's another topic so this idea of prosperity I think it's an interesting lesson

215
00:26:44,720 --> 00:26:52,240
for us to think about and to acknowledge that there are people and that there is this there is this

216
00:26:52,240 --> 00:26:58,160
perversion in the world and even in our minds that we can gain from doing things that are

217
00:26:59,760 --> 00:27:00,880
not a source of gain

218
00:27:00,880 --> 00:27:15,440
the second lesson and so the more to the point one is about lying and the Buddha says that lying

219
00:27:15,440 --> 00:27:25,520
is lying is something is like a gate it's like a line in the sand to line that once crossed

220
00:27:25,520 --> 00:27:34,400
it leads to all kinds of evil the person who is willing to lie two things I guess

221
00:27:34,400 --> 00:27:38,400
that we have to talk about the most the first one is they've thrown away their future

222
00:27:39,600 --> 00:27:44,640
we did we didn't bear a locus we didn't bear a locus someone who has thrown away

223
00:27:45,520 --> 00:27:50,080
their afterlife the means is any hope for

224
00:27:50,080 --> 00:27:55,440
heaven any hope for prosperity in their next life they throw it away

225
00:27:56,560 --> 00:28:02,800
it's a pretty harsh one just from lying I think one reason for giving this teaching is because

226
00:28:02,800 --> 00:28:09,360
it's a probably often overlooked you know we talk about killing and stealing those are really

227
00:28:09,360 --> 00:28:15,680
bad right what about lying I think well yeah can't be as bad as killing or stealing right

228
00:28:15,680 --> 00:28:22,880
but karma is an interesting thing I don't think the Buddha is saying that it is the worst evil

229
00:28:23,760 --> 00:28:28,320
that lying is the worst thing you can possibly do but he's saying something interesting

230
00:28:29,920 --> 00:28:37,760
that lying is a line and it is it's different from other kinds of evil if you kill or hurt someone

231
00:28:37,760 --> 00:28:45,200
that's a quite a straightforward act I mean it's an act out of anger and it's in the person's face

232
00:28:46,560 --> 00:28:53,760
if you steal it's often you know sometimes you can steal hit someone in the face and take their

233
00:28:53,760 --> 00:29:00,160
lunch money but more often you do it in secret so that's an invasion and that's I think

234
00:29:00,160 --> 00:29:08,160
it in some sense worse than than physical violence because it's deceitful or not quite the right

235
00:29:08,160 --> 00:29:16,960
word but it's it's an unknown it's something you can't face you know person who steals from

236
00:29:16,960 --> 00:29:21,760
you it's just suddenly the thing that you own who's gone that can be devastating it can feel

237
00:29:21,760 --> 00:29:30,800
very devastating to people but neither of these are like a lie in the sense that a lie goes

238
00:29:30,800 --> 00:29:36,400
against the truth a lie is a disregard for the value of the truth

239
00:29:37,840 --> 00:29:43,680
maligning the truth which from a Buddhist perspective is an important thing

240
00:29:43,680 --> 00:29:52,720
in some ways more important than pain or possession you know without the truth

241
00:29:54,080 --> 00:29:58,640
and it's not like a person who lies to you can deceive you about the world I mean they can if

242
00:29:58,640 --> 00:30:07,680
you're young and impressionable but the very the idea of a lie in what it represents is very important

243
00:30:07,680 --> 00:30:16,800
and so the idea is that there's this sense that a person is willing to malign the truth

244
00:30:17,760 --> 00:30:23,760
who is a distorted of truth right and not just a distorted a denier a

245
00:30:24,960 --> 00:30:29,440
reverser someone who reverses who makes truth a true statement

246
00:30:29,440 --> 00:30:36,640
who changes what is true into what is false it's like this they say it's not like this

247
00:30:37,440 --> 00:30:42,320
this is an engagement of the truth it's some kind of special

248
00:30:43,040 --> 00:30:48,640
some kind of special thing because it relates I mean it's all psychological the idea is that

249
00:30:48,640 --> 00:30:52,800
there's a psychology behind us that a person who does this is then

250
00:30:52,800 --> 00:31:06,800
an unequipped or is hurting their ability their psychological or mental ability mental capacity

251
00:31:07,680 --> 00:31:13,280
to tell the difference between true and false the results of lying

252
00:31:13,280 --> 00:31:24,640
are created to create a nebulous or vague or uncertain state of of reality

253
00:31:27,760 --> 00:31:33,200
and so there are deans like it killing no other truth like killing is wrong stealing is wrong

254
00:31:34,800 --> 00:31:41,040
become suspect become vague you know there's not this certainty you can tell because

255
00:31:41,040 --> 00:31:50,080
when you undertake to practice never to lie and again by lie we don't mean lies of omission

256
00:31:50,080 --> 00:31:55,520
we don't mean even deception where you trick someone to thinking something we mean where you

257
00:31:55,520 --> 00:32:24,800
actually say x is not x why is not why not why is why there's no question

258
00:32:25,600 --> 00:32:31,120
that there is a line being crossed when you lie I mean it's a very powerful practice to never lie

259
00:32:32,400 --> 00:32:38,240
and it makes your mind very strong and very devoted to truth to reality

260
00:32:39,760 --> 00:32:44,800
it's a very special kind of evil to lie it's something we shouldn't take for granted

261
00:32:44,800 --> 00:32:52,880
certainly the Buddha didn't think so of course this story has more to it than just a lie right

262
00:32:52,880 --> 00:33:00,000
it's a malicious lie some lies or you could argue you're not gonna be the earth isn't going to

263
00:33:00,000 --> 00:33:05,360
swallow you up hold just for lying so there's no question that this was more than just a lie

264
00:33:05,360 --> 00:33:13,440
it was a devastating evil wicked malicious lie and there was a lot more involved it was quite a

265
00:33:13,440 --> 00:33:19,360
quite a work up she spent months planning and plotting could you imagine all the bad karma she

266
00:33:19,360 --> 00:33:26,480
was cultivating in her mind it's like a nine-month meditation she did a nine-month meditation course on

267
00:33:26,480 --> 00:33:36,160
evil that's what it takes to swallow you up well culminating in the final act of lying in front of

268
00:33:36,160 --> 00:33:42,880
the Buddha and accusing the Buddha of something pretty serious not not you know like he raped her

269
00:33:42,880 --> 00:33:49,680
something but it's really serious for a monk and for a religious teacher to be accused of

270
00:33:51,760 --> 00:33:56,640
sleeping with a woman and so on so

271
00:33:56,640 --> 00:34:16,720
understanding karma understanding that there is something I mean very real about good and bad

272
00:34:16,720 --> 00:34:25,840
karma and understanding the nature of truth that truth is important and lies are very bad

273
00:34:25,840 --> 00:34:31,520
because of how important the truth is we're not in Buddhism concerned about belief or even hard

274
00:34:31,520 --> 00:34:40,720
work the only hard work we should be doing is to understand the truth and so lies are very very

275
00:34:40,720 --> 00:34:47,760
bad for that something we should never engage in and that's the demo part of her tonight

276
00:34:47,760 --> 00:34:57,760
so thank you all for tuning in wish you all the best

