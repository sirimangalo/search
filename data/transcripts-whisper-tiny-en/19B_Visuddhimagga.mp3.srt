1
00:00:00,000 --> 00:00:09,200
Here Buddha said that, if you are forgetful, if you are not fully aware, you cannot practice mindfulness meditation.

2
00:00:09,200 --> 00:00:13,000
Mention as a breathing meditation, but the common data said here.

3
00:00:13,000 --> 00:00:21,600
Although any meditation subject, no matter what, is successful only in one who is mindful and fully aware.

4
00:00:21,600 --> 00:00:29,800
Yet any meditation subject other than this one gets more evident as it goes on, giving its attention.

5
00:00:29,800 --> 00:00:34,800
But this mindfulness of breathing is difficult, difficult to develop.

6
00:00:34,800 --> 00:00:40,800
A field in which only the minds of Buddhas, Pachikha Buddhas and Buddhas, sons are at home.

7
00:00:40,800 --> 00:00:45,800
It is no trivial matter, nor can it be cultured by trivial persons.

8
00:00:45,800 --> 00:00:51,800
In proportion as continued attention is given to it, it becomes more peaceful and more subtle.

9
00:00:51,800 --> 00:00:56,800
So strong mentalness and understanding are necessary here.

10
00:00:56,800 --> 00:01:10,800
Stronger mindfulness and understanding are necessary in the practice of mindfulness of breathing meditation, and then in the practice of other kinds of meditation.

11
00:01:10,800 --> 00:01:14,800
Just as one doing needle work on a piece of fine cloth, he find needle is needed.

12
00:01:14,800 --> 00:01:21,800
And he still finer instrument for boring the needle's eye, so when developing this meditation subject.

13
00:01:21,800 --> 00:01:34,800
This resembles fine cloth, both the mindfulness, which is the counterpart of the needle, and the understanding associated with it, which is the counterpart of the instrument for boring the needles I need to be strong.

14
00:01:34,800 --> 00:01:46,800
If you could must have the necessary mindfulness and understanding and must look for the interest and address nowhere else, then the place normally touched by them.

15
00:01:46,800 --> 00:01:55,800
So just by keeping the mind at the place where they touched before, he waited for the breathing to become evident again.

16
00:01:55,800 --> 00:01:58,800
And then there's a simile of the plow man.

17
00:01:58,800 --> 00:02:01,800
Let us keep that.

18
00:02:01,800 --> 00:02:05,800
And on the next page, there's 307.

19
00:02:05,800 --> 00:02:09,800
When he does so in this way, the sign soon appears.

20
00:02:09,800 --> 00:02:22,800
So he perceives years in giving his mind at the tip of the nose, and so the breathing becomes evident to him again.

21
00:02:22,800 --> 00:02:34,800
Then he dwells on that sign or that breathing continuously, and so soon appears to him the sign.

22
00:02:34,800 --> 00:02:40,800
But it is not the same for all.

23
00:02:40,800 --> 00:02:47,800
So the sign appears not as the same for all.

24
00:02:47,800 --> 00:02:57,800
So to one person, it may one kind of sign will appear, and to another it may be another.

25
00:02:57,800 --> 00:03:08,800
So there is not one sign, but many forms of signs.

26
00:03:08,800 --> 00:03:16,800
But this is the exposition given in the commentaries.

27
00:03:16,800 --> 00:03:27,800
It appears to some like a star or cluster of gems or a cluster of paths to others like a rough touch, like that of his silk cotton seeds or effect made of hardwood.

28
00:03:27,800 --> 00:03:33,800
So that is some, what some meditative feel.

29
00:03:33,800 --> 00:03:44,800
So if they thought something was put into their nose in the nostrils, maybe even difficult to breathe.

30
00:03:44,800 --> 00:03:49,800
Others like a long blade stream, or a wreath of flowers, or a puff of smoke,

31
00:03:49,800 --> 00:03:57,800
do others like a stretched out cobweb or a femme of cloud, or a low dust flower, or a chariot feel, or a moon's disc, or the sun's disc.

32
00:03:57,800 --> 00:04:04,800
So any kind of sign can appear to a person.

33
00:04:04,800 --> 00:04:19,800
So when my father meditated, practiced this medicine, he said, he saw the sign like a sheet of silver.

34
00:04:19,800 --> 00:04:32,800
So any one of those mentioned here, or even those that are not mentioned here, may appear to a meditative.

35
00:04:32,800 --> 00:04:40,800
So anything can appear as the sign of this meditation.

36
00:04:40,800 --> 00:04:46,800
In fact, these resembles an occasion when a number of bikas are sitting together, reciting is so tender.

37
00:04:46,800 --> 00:04:50,800
When a bikka asks, what does this soda appear like to you?

38
00:04:50,800 --> 00:04:59,800
Once it appears to me like a great morning torrent, another to me, it is like a line of forest trees, and so on and so on.

39
00:04:59,800 --> 00:05:17,800
So, depending on different persons, there can be multiplicity of many varieties of these signs.

40
00:05:17,800 --> 00:05:23,800
Similarly, this single meditation subject appears differently because of difference in perception.

41
00:05:23,800 --> 00:05:29,800
It is born of perception, its source is perception, it is produced by perception.

42
00:05:29,800 --> 00:05:34,800
Therefore, it should be understood that when it appears differently, it is because of difference in perception.

43
00:05:34,800 --> 00:05:42,800
People have different outlooks or different notions, and so they appear differently to different people.

44
00:05:42,800 --> 00:05:46,800
And here, the consciousness that has implied as its object is one.

45
00:05:46,800 --> 00:05:50,800
The consciousness that has outbred as its object is one, another.

46
00:05:50,800 --> 00:05:55,800
And the consciousness that has assigned as its object is another.

47
00:05:55,800 --> 00:06:02,800
This you know from a bidhamma, because consciousness can take only one object at a time.

48
00:06:02,800 --> 00:06:12,800
So, consciousness that takes the input as an object is one, and then consciousness that takes the outbred as object is another.

49
00:06:12,800 --> 00:06:20,800
So, they are different.

50
00:06:20,800 --> 00:06:23,800
And the consciousness that has designed as its object is another.

51
00:06:23,800 --> 00:06:32,800
For the meditation subject reaches neither absorption nor even access in one, who has not got these three things, clear.

52
00:06:32,800 --> 00:06:38,800
Who does not see clearly the inbred, the outbred, and the sign.

53
00:06:38,800 --> 00:06:43,800
But it reaches access and also absorption in one, who has called these three signs, clear.

54
00:06:43,800 --> 00:06:45,800
And it is said.

55
00:06:45,800 --> 00:06:52,800
Now, when the sign appeared in this way, what must be the metadata do?

56
00:06:52,800 --> 00:06:59,800
The bikhu should go to the teacher and tear him, when the rusa sat in such as a beer to me.

57
00:06:59,800 --> 00:07:07,800
But, say the digger reciders, the digger should say neither, this is sign, no, this is not sign.

58
00:07:07,800 --> 00:07:10,800
After saying, it happens like this friend.

59
00:07:10,800 --> 00:07:16,800
So, the digger should just say, it happens like this.

60
00:07:16,800 --> 00:07:20,800
He should not say, this is a sign or this is not a sign.

61
00:07:20,800 --> 00:07:22,800
That is what the digger reciders think.

62
00:07:22,800 --> 00:07:26,800
They should tell him, go on giving a detention again and again.

63
00:07:26,800 --> 00:07:33,800
For if he were told it is the sign, he might become complacent and stop short at that.

64
00:07:33,800 --> 00:07:38,800
That means, this sign is difficult to get.

65
00:07:38,800 --> 00:07:41,800
And one of the pieces said that you have called this sign.

66
00:07:41,800 --> 00:07:44,800
He may think, oh, I have called what is difficult to get.

67
00:07:44,800 --> 00:07:53,800
And so, I may slow down a little and I can, again, practice again anytime I like something like that.

68
00:07:53,800 --> 00:07:59,800
And if he were told it is not a sign, he might get discouraged and give up.

69
00:07:59,800 --> 00:08:05,800
So, he should encourage him to keep giving it his attention without saying either.

70
00:08:05,800 --> 00:08:08,800
So, the digger reciders say firstly.

71
00:08:08,800 --> 00:08:13,800
The digger reciders mean reciders of the long discourses.

72
00:08:13,800 --> 00:08:22,800
You know, there are collections or wrong discourses and middle-length discourses and then

73
00:08:22,800 --> 00:08:30,800
candler discourses and the gradual sayings.

74
00:08:30,800 --> 00:08:36,800
So, there are monks who made special study of the long discourses and there are others who

75
00:08:36,800 --> 00:08:41,800
made special study of middle-length discourses and so on.

76
00:08:41,800 --> 00:08:44,800
So, the digger reciders say like this.

77
00:08:44,800 --> 00:08:55,800
But the much more reciders, those who whom each special study of middle-length discourses say that he should be told this is a sign friend.

78
00:08:55,800 --> 00:08:56,800
Well done.

79
00:08:56,800 --> 00:09:00,800
Keep giving attention to etiquette and again.

80
00:09:00,800 --> 00:09:13,800
Because they thought that there is no reason for a meditator to be discouraged or to be complacent when he knows that he has called the sign.

81
00:09:13,800 --> 00:09:25,800
Because he practiced meditation just to attain to the journal stage and then later on to the attainment of enlightened men.

82
00:09:25,800 --> 00:09:32,800
So, when the digger says to him that it is a sign, then he must be encouraged.

83
00:09:32,800 --> 00:09:34,800
I mean, he will get encouraged.

84
00:09:34,800 --> 00:09:44,800
So, it is good according to the maximum reciders to tell the student that it is the sign.

85
00:09:44,800 --> 00:09:48,800
And that is also good because it is the second thing.

86
00:09:48,800 --> 00:09:49,800
That is right.

87
00:09:49,800 --> 00:09:51,800
Do you remember that?

88
00:09:51,800 --> 00:09:57,800
That is very good.

89
00:09:57,800 --> 00:10:00,800
So, after that he should fix his men on that same sign.

90
00:10:00,800 --> 00:10:04,800
And so, from now on, his development proceeds by way of fixing.

91
00:10:04,800 --> 00:10:08,800
Now, we come to, I think, third or fourth?

92
00:10:08,800 --> 00:10:09,800
Fourth.

93
00:10:09,800 --> 00:10:10,800
Fourth stage.

94
00:10:10,800 --> 00:10:11,800
Fixing.

95
00:10:11,800 --> 00:10:19,800
As soon as the sign appears, his hindrances are suppressed, his defilements are side,

96
00:10:19,800 --> 00:10:25,800
his mindfulness is established and his consciousness is concentrated in excess concentration.

97
00:10:25,800 --> 00:10:32,800
Then, after excess calm, Jana concentration.

98
00:10:32,800 --> 00:10:41,800
Then, he should not give attention to the sign as to its color or review it as to its specific characteristic.

99
00:10:41,800 --> 00:10:52,800
Because if he give attention as to its color, then it would become a color meditation and not breathing meditation.

100
00:10:52,800 --> 00:11:08,800
And if you review it as to its characteristic, it will become another kind of meditation and not breathing meditation or mindfulness or breathing meditation.

101
00:11:08,800 --> 00:11:15,800
So, he should not pay attention as to its color or its characteristic.

102
00:11:15,800 --> 00:11:22,800
He should guard it as carefully as a king's chief queen guards the child and I want you to become a real turning monarch.

103
00:11:22,800 --> 00:11:24,800
That means universal monarch.

104
00:11:24,800 --> 00:11:27,800
So, he should guard it.

105
00:11:27,800 --> 00:11:35,800
Then, guarding it thus, he should make it grow and improve with repeated attention.

106
00:11:35,800 --> 00:11:40,800
So, he should pay attention to it, dwell on it for a long time.

107
00:11:40,800 --> 00:11:50,800
And he should practice the ten full skill in absorption that takes you back to chapter 4 and bring about evenness of energy.

108
00:11:50,800 --> 00:11:56,800
That means evenness of balance of energy and concentration.

109
00:11:56,800 --> 00:12:04,800
As his tribe thus, full full and five whole Jana is achieved by him on that same sign in the same way as described under the earth, Kaseena.

110
00:12:04,800 --> 00:12:15,800
So, Jana rises in him and then from first Jana, he would go to second, third, fourth and fifth Jana's.

111
00:12:15,800 --> 00:12:24,800
During the moments of Jana, the mind is very, very fixed, very, very, very still and very fixed.

112
00:12:24,800 --> 00:12:28,800
And so, this is a full stage with the fixing.

113
00:12:28,800 --> 00:12:34,800
After fixing comes one.

114
00:12:34,800 --> 00:12:38,800
After fixing, observing.

115
00:12:38,800 --> 00:12:45,800
Now, what is observing?

116
00:12:45,800 --> 00:12:48,800
Observing is inside, it is explained there.

117
00:12:48,800 --> 00:12:54,800
So, now, he will change to Vibhasana meditation.

118
00:12:54,800 --> 00:13:03,800
Until this point, he practiced breathing meditation as a Shammatama division and he gets to Jana's stage.

119
00:13:03,800 --> 00:13:08,800
So, after that, it will become Vibhasana meditation.

120
00:13:08,800 --> 00:13:18,800
So, however, Vibhasana Vibhasana has achieved the full full and five whole Jana and wants to reach purity by developing the meditation subject through observing and through turning away.

121
00:13:18,800 --> 00:13:24,800
He should make that Jana familiar by attaining mastery in it in the five ways.

122
00:13:24,800 --> 00:13:30,800
And then embark upon insights by defining mentality, materiality and how.

123
00:13:30,800 --> 00:13:38,800
So, since he has caught Jana, he makes the Jana basis for his Vibhasana meditation.

124
00:13:38,800 --> 00:13:42,800
So, he enters into Jana.

125
00:13:42,800 --> 00:13:49,800
And then, after emerging from the Jana, he concentrates on the Jana.

126
00:13:49,800 --> 00:13:59,800
First, he embark upon insight by defining mentality and materiality.

127
00:13:59,800 --> 00:14:06,800
That is, defining that this is mind, this is matter.

128
00:14:06,800 --> 00:14:13,800
On emerging from the attainment, he sees that the interest and outbursts of the physical body and the mind as they are origin.

129
00:14:13,800 --> 00:14:15,800
Because they are caused by mind.

130
00:14:15,800 --> 00:14:22,800
And that just as when he blokes me as bellows have been blown, the wind moves all into the back and to the men's appropriate effort.

131
00:14:22,800 --> 00:14:29,800
So, to impress and outbursts are due to the body and the mind.

132
00:14:29,800 --> 00:14:36,800
Now, next, he defines the inbursts and outbursts and the body as materiality.

133
00:14:36,800 --> 00:14:45,800
And the consciousness and the states associated with the consciousness as immaterial or mind.

134
00:14:45,800 --> 00:14:49,800
You know why it is stated here, why it is given here?

135
00:14:49,800 --> 00:14:56,800
Because Vibhasana, a real Vibhasana begins with the designing of mind and matter clearly.

136
00:14:56,800 --> 00:15:08,800
If able to see, not just guess, able to see clearly the mind and matter through meditation.

137
00:15:08,800 --> 00:15:13,800
First, we need concentration.

138
00:15:13,800 --> 00:15:20,800
And after the concentration of mind comes, the designing of mind and matter.

139
00:15:20,800 --> 00:15:26,800
So, this is the actual beginning of Vibhasana meditation.

140
00:15:26,800 --> 00:15:35,800
So, after that, the designing of mind and matter are defining of mind and matter is given in a later chapters.

141
00:15:35,800 --> 00:15:40,800
Having defined mentality, materiality in this way, he seeks its condition.

142
00:15:40,800 --> 00:15:52,800
Now, from the stage of descending mentality and materiality, he progresses to the stage of seeing their conditions.

143
00:15:52,800 --> 00:15:57,800
Or seeing that they are conditioned, seeing their causes.

144
00:15:57,800 --> 00:16:07,800
With such he finds it and so overcomes his doubts about the way of materiality or currents in the three divisions of time.

145
00:16:07,800 --> 00:16:21,800
That means the breath, the breath and the body are materiality and mindfulness and others are mentality.

146
00:16:21,800 --> 00:16:26,800
And they are not uncrossed or they are not unconditioned.

147
00:16:26,800 --> 00:16:31,800
They are conditioned by the breath is conditioned by mind.

148
00:16:31,800 --> 00:16:43,800
And the consciousness and mental factors are conditioned by each other and also conditioned by the material breath and so on.

149
00:16:43,800 --> 00:16:53,800
So, the next stage is that seeing the conditionality of things actually, seeing the conditions of mind and matter.

150
00:16:53,800 --> 00:17:04,800
And then, his thoughts being overcome, he attributes the three characteristics, beginning with that of suffering, to mentality and materiality, comprehending them by groups.

151
00:17:04,800 --> 00:17:12,800
He abandons the ten imperfections of insight beginning with illumination, which arise in the first stages of the contemplation of rise and fall.

152
00:17:12,800 --> 00:17:19,800
And he defines as the path and knowledge of the way that is free from these imperfections.

153
00:17:19,800 --> 00:17:25,800
This is the description in very brief of the stages of Wipassana meditation.

154
00:17:25,800 --> 00:17:31,800
And they will be treated in detail in succeeding chapters.

155
00:17:31,800 --> 00:17:37,800
So, after descending conditions, he descends what?

156
00:17:37,800 --> 00:17:47,800
Three characteristics, suffering, I mean, impermanence, suffering and insubstantinality.

157
00:17:47,800 --> 00:17:50,800
So, these three characteristics, he come to see.

158
00:17:50,800 --> 00:17:55,800
And then, he reaches contemplation of dissolution by abandoning, attention to arising.

159
00:17:55,800 --> 00:18:07,800
Now, at the stage of seeing rising and falling, at the early stage of seeing rising and falling, the impediments come in.

160
00:18:07,800 --> 00:18:12,800
The impediments to Wipassana are in impediments to progress.

161
00:18:12,800 --> 00:18:16,800
That is seeing light, of feeling very happy and so on.

162
00:18:16,800 --> 00:18:21,800
When they arise, a yogi may think that he has a change in light and light.

163
00:18:21,800 --> 00:18:31,800
And if he thinks so, then he will not make effort to reach higher stages.

164
00:18:31,800 --> 00:18:37,800
So, he must understand that these are not the way to light and light.

165
00:18:37,800 --> 00:18:39,800
But they are just obstacles.

166
00:18:39,800 --> 00:18:42,800
So, he has to overcome these obstacles.

167
00:18:42,800 --> 00:18:55,800
And when he has overcome these obstacles, he reaches the higher stage of descending rising and falling.

168
00:18:55,800 --> 00:19:00,800
So, there are two stages in the discernment of rising and falling.

169
00:19:00,800 --> 00:19:03,800
Let us say, the lower stage and higher stage.

170
00:19:03,800 --> 00:19:10,800
So, after reaching the lower stage, the ten impediments occur in him.

171
00:19:10,800 --> 00:19:17,800
And after conquering these ten impediments, he reaches the higher stage of descending rising and falling.

172
00:19:17,800 --> 00:19:25,800
From there, he reaches another stage where he sees only the falling, only the dissolution or disappearing of things.

173
00:19:25,800 --> 00:19:35,800
Only the dissolution of things appear to him clear.

174
00:19:35,800 --> 00:19:39,800
So, that is the contemplation of the solution.

175
00:19:39,800 --> 00:19:43,800
He reaches contemplation of the solution by abandoning attention to your rising.

176
00:19:43,800 --> 00:19:50,800
When all formations of a peer as terror owing to the contemplation of the incessant dissolution.

177
00:19:50,800 --> 00:19:56,800
So, after that, he sees them as dangerous, as dangers.

178
00:19:56,800 --> 00:20:04,800
Although he is not afraid of them, he does not fear them, but he sees them as dangers.

179
00:20:04,800 --> 00:20:06,800
It is different.

180
00:20:06,800 --> 00:20:08,800
You are afraid of something.

181
00:20:08,800 --> 00:20:13,800
Sometimes you are not afraid of them, but you recognize them as danger.

182
00:20:13,800 --> 00:20:17,800
So, here also, he does not get afraid of them.

183
00:20:17,800 --> 00:20:25,800
If he gets afraid of them, then he has acoustalia in him and not meditation.

184
00:20:25,800 --> 00:20:36,800
So, when he sees things just disappear in disappearing, he comes to see that this is danger.

185
00:20:36,800 --> 00:20:43,800
That is danger in being disappearing.

186
00:20:43,800 --> 00:20:49,800
After that, he becomes dispassionate towards them.

187
00:20:49,800 --> 00:20:54,800
When you see something as dangerous, then you are not attached to them.

188
00:20:54,800 --> 00:20:55,800
You are not attached to it.

189
00:20:55,800 --> 00:20:57,800
You want to get away from it.

190
00:20:57,800 --> 00:20:59,800
So, he becomes dispassionate towards them.

191
00:20:59,800 --> 00:21:06,800
He is free for them, fades away, and he is liberated from them.

192
00:21:06,800 --> 00:21:16,800
Librated from them means he got rid of attachment, anger, and delusion, and becomes enlightened.

193
00:21:16,800 --> 00:21:22,800
After he has thus reached the full noble path in due succession, and has become established

194
00:21:22,800 --> 00:21:29,800
in the fruition of an anchered, he at last attains to the 19 kinds of reviewing knowledge.

195
00:21:29,800 --> 00:21:37,800
Reviewing knowledge means after attainment of each stage of enlightenment, that is what

196
00:21:37,800 --> 00:21:38,800
we call reviewing.

197
00:21:38,800 --> 00:21:47,800
Reviewing the hurt, reviewing the fruit, reviewing the nibana, reviewing the mental

198
00:21:47,800 --> 00:21:54,800
defalments, eradicated, and reviewing the mental defalments which remained.

199
00:21:54,800 --> 00:22:03,800
So, there are five kinds of reviewing after the first stage, second stage, and third stage.

200
00:22:03,800 --> 00:22:09,800
But after the fourth stage, that is, after becoming an araha, there are only four reviewing.

201
00:22:09,800 --> 00:22:19,800
Because there are no mental defalments left, and an an eradicated.

202
00:22:19,800 --> 00:22:25,800
So, in the case of an araha, there are only four reviewing.

203
00:22:25,800 --> 00:22:32,800
Reviewing of fat, fruit, nibana, and defalments eradicated.

204
00:22:32,800 --> 00:22:42,800
So, all together, we have 19 kinds of reviewing knowledge, and he becomes fit to receive

205
00:22:42,800 --> 00:22:45,800
the highest skill from the world with its deities.

206
00:22:45,800 --> 00:22:52,800
So, this is how it wasn't first practiced as mindfulness from reading meditation as

207
00:22:52,800 --> 00:22:55,800
some atama meditation and gets jana.

208
00:22:55,800 --> 00:23:01,800
And then making that jana, the basis for wipasana meditation, he practices wipasana

209
00:23:01,800 --> 00:23:08,800
meditation, and gradually reach the stage of araha.

210
00:23:08,800 --> 00:23:13,800
At this point, his development of concentration through mentalness of breathing, beginning

211
00:23:13,800 --> 00:23:18,800
with counting and ending with looking back, is completed.

212
00:23:18,800 --> 00:23:25,800
So, in the stages, observing means wipasana, right?

213
00:23:25,800 --> 00:23:37,800
And turning away means attainment of fat, or taking enlightenment, and looking back means reviewing.

214
00:23:37,800 --> 00:23:45,800
So, all the eight, not seven, I think eight.

215
00:23:45,800 --> 00:23:48,800
How many stages of the eight, right?

216
00:23:48,800 --> 00:23:51,800
So, all stages are complete now.

217
00:23:51,800 --> 00:23:57,800
Looking back is completed, this is the commentary on the first tetra in all aspects.

218
00:23:57,800 --> 00:24:05,800
So, you remember the first four things, the first four medams.

219
00:24:05,800 --> 00:24:16,800
Long, short, whole breath body, and tranquilizing the breath, right?

220
00:24:16,800 --> 00:24:18,800
These four.

221
00:24:18,800 --> 00:24:22,800
Now, since there is no separate method for developing the meditation subject in the case

222
00:24:22,800 --> 00:24:27,800
of the other tetrats, they are meaning they are only being understood according to the

223
00:24:27,800 --> 00:24:28,800
word commentary.

224
00:24:28,800 --> 00:24:42,800
So, there is no special method for practicing the other sets of four, because the other sets

225
00:24:42,800 --> 00:24:51,800
of four have to be practiced after he person reached the channel stage by practicing

226
00:24:51,800 --> 00:24:53,800
the first four medams.

227
00:24:53,800 --> 00:25:01,800
So, there are no special medams for the practice of the other tetrats.

228
00:25:01,800 --> 00:25:09,800
Now, in the second tetrats, he trains the isial breath in, isial breath of experiencing

229
00:25:09,800 --> 00:25:12,800
happiness of PD.

230
00:25:12,800 --> 00:25:20,800
That means clearly seeing or clearly experiencing PD, clearly knowing PD.

231
00:25:20,800 --> 00:25:24,800
That is making happiness known, making it plain.

232
00:25:24,800 --> 00:25:29,800
Here in, the happiness is experiencing two ways with the object.

233
00:25:29,800 --> 00:25:36,800
That means by way of the object and by way of non-confusion.

234
00:25:36,800 --> 00:25:41,800
I think with, with does not mean by way of, right?

235
00:25:41,800 --> 00:25:44,800
It's better to see by way of.

236
00:25:44,800 --> 00:25:50,800
So, how, how is the happiness of PD experience with the object?

237
00:25:50,800 --> 00:25:55,800
He attains the two genres in which happiness is present.

238
00:25:55,800 --> 00:26:08,800
At the same time, when he has actually entered upon them, the happiness is experienced with the object,

239
00:26:08,800 --> 00:26:13,800
only due to the object of the genre, because of the experiencing of the object.

240
00:26:13,800 --> 00:26:19,800
Now, when a person attains the second, first genre or second genre, what is the object of the

241
00:26:19,800 --> 00:26:26,800
genre?

242
00:26:26,800 --> 00:26:29,800
What is the object of the genre?

243
00:26:29,800 --> 00:26:31,800
The science, right?

244
00:26:31,800 --> 00:26:33,800
The counterpart science.

245
00:26:33,800 --> 00:26:41,800
So, at the moment of genre, the actual object is counterpart science and not happiness.

246
00:26:41,800 --> 00:26:46,800
But here, he said that he is experiencing, he is knowing the happiness, he is knowing the

247
00:26:46,800 --> 00:26:49,800
beauty at that moment.

248
00:26:49,800 --> 00:26:56,800
So, that means at the time of reaching the genre, happiness of PD is said to be known clearly

249
00:26:56,800 --> 00:26:58,800
by way of the object.

250
00:26:58,800 --> 00:27:03,800
That means because the object is clearly known at the time.

251
00:27:03,800 --> 00:27:09,800
So, when the object is known, it is said that PD is also known.

252
00:27:09,800 --> 00:27:17,800
Not that PD becomes the object of genre, but by way of the object itself, we can say that

253
00:27:17,800 --> 00:27:40,800
PD is also known.

254
00:27:40,800 --> 00:27:46,800
That is by way of the object, under the heading of the object.

255
00:27:46,800 --> 00:27:48,800
What is meant, man?

256
00:27:48,800 --> 00:27:53,800
Just as one man who is looking for a snake discovers its abode.

257
00:27:53,800 --> 00:27:56,800
The snake is as it were already discovered and caught.

258
00:27:56,800 --> 00:28:02,800
Oh, into the ease with which he will then be able to catch it with charms and spells.

259
00:28:02,800 --> 00:28:09,800
He has not yet caught its snake, but it is as good as having caught it.

260
00:28:09,800 --> 00:28:15,800
So, to run the object, which is the abode of happiness, its experience, then the happiness,

261
00:28:15,800 --> 00:28:24,800
it is said to be experienced through going to the ease with which it will be apprehended

262
00:28:24,800 --> 00:28:28,800
and it is specific and general characteristics.

263
00:28:28,800 --> 00:28:35,800
Specific characteristic means it is characteristic not shared by other man who states.

264
00:28:35,800 --> 00:28:41,800
And general characteristics means it is infamous and suffering and no sole nature.

265
00:28:41,800 --> 00:28:49,800
So, when the object is clearly seen, then PD is said to be also clearly seen.

266
00:28:49,800 --> 00:28:52,800
So, that is what is meant here.

267
00:28:52,800 --> 00:28:55,800
Then, how with non-confusion?

268
00:28:55,800 --> 00:29:01,800
When after entering a bone and emerging from one of the two genres accompanied by happiness,

269
00:29:01,800 --> 00:29:07,800
he comprehends with insight that happiness associated with the genre.

270
00:29:07,800 --> 00:29:12,800
He has liable to destruction and to form. Then, at the actual time of insight,

271
00:29:12,800 --> 00:29:17,800
the happiness is experienced with non-confusion or by way of non-confusion,

272
00:29:17,800 --> 00:29:21,800
all of you will be born in the tradition of his characteristics of impermanence and so on.

273
00:29:21,800 --> 00:29:24,800
That means, when he practices, we pass a number the decision on it.

274
00:29:24,800 --> 00:29:29,800
So, first he enters into the first or second genre, he emerges from it,

275
00:29:29,800 --> 00:29:36,800
and then takes the happiness which is one of the genre factors

276
00:29:36,800 --> 00:29:41,800
as an object of meditation and contemplate on it,

277
00:29:41,800 --> 00:29:46,800
contemplate on it as impermanence, suffering and soleness.

278
00:29:46,800 --> 00:29:49,800
So, at that moment, he clearly sees the,

279
00:29:49,800 --> 00:29:56,800
here he really sees the PT, I mean, happiness.

280
00:29:56,800 --> 00:30:02,800
And non-confusion means he sees it as it is, as impermanence,

281
00:30:02,800 --> 00:30:07,800
as suffering and as soleness, and not as otherwise.

282
00:30:07,800 --> 00:30:11,800
And then there is the, the, the, the, the, the collision of what he some bit of mother,

283
00:30:11,800 --> 00:30:13,800
that we can scale it.

284
00:30:13,800 --> 00:30:17,800
Then the next theatres, there are many three crosses,

285
00:30:17,800 --> 00:30:19,800
or not later, that the crosses.

286
00:30:19,800 --> 00:30:23,800
There are many three crosses should be understood in the same way as to meaning,

287
00:30:23,800 --> 00:30:25,800
but there is this difference here.

288
00:30:25,800 --> 00:30:31,800
The experiencing of bliss, Sukha must be understood to be through three genres.

289
00:30:31,800 --> 00:30:34,800
You know, Sukha is a,

290
00:30:34,800 --> 00:30:38,800
Sukha is present in first genre, second genre and that genre.

291
00:30:38,800 --> 00:30:45,800
So, here, through three genres, and that of mental formation through four.

292
00:30:45,800 --> 00:30:49,800
What is mental formation here?

293
00:30:49,800 --> 00:30:52,800
We don't have to ask because it's explained.

294
00:30:52,800 --> 00:30:56,800
The mental formation consists of the two aggregates of feeling and perception.

295
00:30:56,800 --> 00:30:59,800
So, here, mental formation means feeling and perception.

296
00:30:59,800 --> 00:31:04,800
So, feeling and perception perception are called mental formation,

297
00:31:04,800 --> 00:31:06,800
or check the sunflower.

298
00:31:06,800 --> 00:31:10,800
Because they arise only when consciousness arises.

299
00:31:10,800 --> 00:31:15,800
So, depend, they depend on, on consciousness for their arising.

300
00:31:15,800 --> 00:31:19,800
So, they are said to be conditioned by,

301
00:31:19,800 --> 00:31:23,800
say that, I mean by consciousness or mind.

302
00:31:23,800 --> 00:31:32,800
So, mental formation really means, mind formed or formed by our conditions by consciousness.

303
00:31:32,800 --> 00:31:37,800
And here, conditioned by consciousness is,

304
00:31:37,800 --> 00:31:42,800
made to mean feeling and perception.

305
00:31:42,800 --> 00:31:51,800
So, experiencing the mental formation means, experiencing the,

306
00:31:51,800 --> 00:31:54,800
feeling and perception.

307
00:31:54,800 --> 00:31:57,800
So, in, in the case of the cross experiencing bliss,

308
00:31:57,800 --> 00:32:03,800
it is said in the bliss and within, in order to show the playing of insight here as well.

309
00:32:03,800 --> 00:32:09,800
Bliss, there are two kinds of bliss, for every bliss and mental bliss.

310
00:32:09,800 --> 00:32:12,800
Tranquilizing the mental formation means,

311
00:32:12,800 --> 00:32:15,800
tranquilizing the gross mental formation, stopping it,

312
00:32:15,800 --> 00:32:24,800
just a minute or so, experiencing bliss and tranquilization of mental formation.

313
00:32:24,800 --> 00:32:26,800
Right.

314
00:32:26,800 --> 00:32:29,800
So, in the first journal, second journal,

315
00:32:29,800 --> 00:32:33,800
that journal, there is suka bliss.

316
00:32:33,800 --> 00:32:39,800
And so, during those journal, a person is said to experience the bliss.

317
00:32:39,800 --> 00:32:42,800
And then, tranquilizing the mental formation,

318
00:32:42,800 --> 00:32:45,800
tranquilizing the gross mental formation, that means,

319
00:32:45,800 --> 00:32:48,800
gross feeling and perception, stopping it is mean.

320
00:32:48,800 --> 00:32:52,800
And this will be understood in detail in the same way as given under the body

321
00:32:52,800 --> 00:32:54,800
deformation, if you go back.

322
00:32:54,800 --> 00:32:57,800
Here, however, in the happiness clause,

323
00:32:57,800 --> 00:33:00,800
feeling which is actually being contemplated on in this journal,

324
00:33:00,800 --> 00:33:06,800
is stated under the head of happiness, which is a formation.

325
00:33:06,800 --> 00:33:14,800
That means, in the text, it is said that experiencing bliss.

326
00:33:14,800 --> 00:33:28,800
And experience, here, however, in the happiness clause, for beauty clause,

327
00:33:28,800 --> 00:33:32,800
which is actually being contemplated and etc.

328
00:33:32,800 --> 00:33:37,800
is stated under the head of happiness, which is a formation.

329
00:33:37,800 --> 00:33:41,800
But in the bliss clause, feeling is stated in its own form.

330
00:33:41,800 --> 00:33:44,800
Now, the Pali word, p-t, p-t,

331
00:33:44,800 --> 00:33:49,800
but it's some way de and suka, but it's some way de.

332
00:33:49,800 --> 00:33:54,800
So, in the word, in the cross, p-t, but it's some way de.

333
00:33:54,800 --> 00:34:00,800
That is, experiencing happiness.

334
00:34:00,800 --> 00:34:06,800
Feeling is stated under the head of happiness.

335
00:34:06,800 --> 00:34:12,800
Although, it is said, happiness, we should understand it to mean feeling.

336
00:34:12,800 --> 00:34:15,800
You know why he is saying this here?

337
00:34:15,800 --> 00:34:21,800
Because this second, theater has to do with contemplation on feelings.

338
00:34:21,800 --> 00:34:25,800
So, although it is said that experiencing the bliss,

339
00:34:25,800 --> 00:34:33,800
I mean, experiencing happiness, the real thing is clearly understanding

340
00:34:33,800 --> 00:34:36,800
of clearly knowing the feeling.

341
00:34:36,800 --> 00:34:40,800
So, the feeling is here, described under the heading of happiness.

342
00:34:40,800 --> 00:34:47,800
So, the real word uses happiness, but what we have to understand is feeling.

343
00:34:47,800 --> 00:34:57,800
But in the phrase, in the cross, experiencing happiness,

344
00:34:57,800 --> 00:35:00,800
it is stated in its own form.

345
00:35:00,800 --> 00:35:04,800
That means, suka, p-t, and way de.

346
00:35:04,800 --> 00:35:08,800
Now, suka, happiness belongs to a feeling.

347
00:35:08,800 --> 00:35:16,800
So, here, feeling is stated directly.

348
00:35:16,800 --> 00:35:20,800
So, we do not have to go around here.

349
00:35:20,800 --> 00:35:26,800
So, experiencing the bliss means experiencing the feeling.

350
00:35:26,800 --> 00:35:34,800
Experiencing, experiencing happiness means experiencing feeling.

351
00:35:34,800 --> 00:35:40,800
Experiencing bliss means experiencing feeling.

352
00:35:40,800 --> 00:35:50,800
So, here, it is stated in its own form, not under the heading of any other thing.

353
00:35:50,800 --> 00:35:55,800
So, in the two ways, mental formation clause, the feeling is dead necessarily associated

354
00:35:55,800 --> 00:35:59,800
with perception because of the words perception and feeling belong to the mind.

355
00:35:59,800 --> 00:36:03,800
These things being bound up with the mind of mental formations.

356
00:36:03,800 --> 00:36:10,800
Now, the type clause, tranquilizing the mental formations.

357
00:36:10,800 --> 00:36:17,800
Although mental formation means feeling and perception, here, it just may be being feeling

358
00:36:17,800 --> 00:36:19,800
accompanied by perception.

359
00:36:19,800 --> 00:36:25,800
The same thing actually, associated with perception, feeling accompanied by or associated

360
00:36:25,800 --> 00:36:26,800
with perception.

361
00:36:26,800 --> 00:36:34,800
That is why this state clause has to do with contemplation on feeling.

362
00:36:34,800 --> 00:36:42,800
So, the community that is explaining this to us, because we may ask, in the second clause,

363
00:36:42,800 --> 00:36:51,800
it is said that he is aware of p-t, which is not feeling, but you included in the feeling

364
00:36:51,800 --> 00:36:57,800
of contemplation.

365
00:36:57,800 --> 00:37:04,800
Actually, the word p-t is mentioned, but we must understand feeling, not p-t.

366
00:37:04,800 --> 00:37:13,800
So, the second, second, the state clause has to do with the contemplation on feelings.

367
00:37:13,800 --> 00:37:22,800
And this second clause that can be practiced only after one gets genres.

368
00:37:22,800 --> 00:37:29,800
And in order to understand this, you have to understand that the first genre, second

369
00:37:29,800 --> 00:37:38,800
genre accompanied by p-t, and third genre accompanied by Sukhah, full genre also accompanied

370
00:37:38,800 --> 00:37:45,800
by Sukhah, right? And the fifth genre, not accompanied by Sukhah, it is accompanied by

371
00:37:45,800 --> 00:37:46,800
opiate.

372
00:37:46,800 --> 00:37:52,800
So, that is why sometimes it is the first two genres or three genres and so on.

373
00:37:52,800 --> 00:37:56,800
So, this is the end of the second-day clause.

374
00:37:56,800 --> 00:38:03,800
So, if the third-day clause and so on, we will finish next week.

375
00:38:03,800 --> 00:38:08,800
So, next week, week once again is the last clause.

376
00:38:08,800 --> 00:38:13,800
And last clause, the next clause will start off with the foreground of the horrors or

377
00:38:13,800 --> 00:38:14,800
of bodies.

378
00:38:14,800 --> 00:38:20,800
And that is a wonderful, the social emotions, wonderful, meditation, very accessible,

379
00:38:20,800 --> 00:38:41,800
so I will pitch for you, for the rest of you needing attention.

380
00:38:41,800 --> 00:38:59,800
Thank you.

381
00:38:59,800 --> 00:39:19,800
Thank you.

