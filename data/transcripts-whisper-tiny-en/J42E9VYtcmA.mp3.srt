1
00:00:00,000 --> 00:00:18,000
Okay, good evening everyone, welcome to our, welcome to our Dhamma broadcast.

2
00:00:18,000 --> 00:00:39,000
Tonight I'd like to talk about the Mahawitaka, the reflections of a great being.

3
00:00:39,000 --> 00:00:54,000
That's what it's called, the reflections of a great, or the great reflections.

4
00:00:54,000 --> 00:01:05,000
And these great reflections are, when they're described as being reflections that a great being has.

5
00:01:05,000 --> 00:01:25,000
But specifically they're about, when they're about a specific topic which is, what sort of person, what sort of person is this teaching for?

6
00:01:25,000 --> 00:01:35,000
Well now when you hear that, it makes you think maybe this teaching isn't for everyone.

7
00:01:35,000 --> 00:01:47,000
It makes us think that it makes us wonder and it feeds the doubt that maybe this isn't right for us.

8
00:01:47,000 --> 00:01:56,000
Maybe we're not right for this practice. Maybe this practice wasn't made for me.

9
00:01:56,000 --> 00:02:02,000
And we hear claims that this meditation is for everyone.

10
00:02:02,000 --> 00:02:11,000
We like to think that the Buddha's teaching is universal.

11
00:02:11,000 --> 00:02:24,000
So to understand what it means when we say the sort of person that this Dhamma is, that this teaching is for, we have to understand what we mean by person.

12
00:02:24,000 --> 00:02:31,000
The person is not a static entity. We're changing all the time.

13
00:02:31,000 --> 00:02:35,000
Every day we're a different person.

14
00:02:35,000 --> 00:02:43,000
Every hour it seems, the morning I'm energetic and the afternoon I'm tired.

15
00:02:43,000 --> 00:02:55,000
Maybe I like walking in the morning maybe at the first sitting in the evening.

16
00:02:55,000 --> 00:02:58,000
But we change.

17
00:02:58,000 --> 00:03:05,000
And so that's of course the more important question that we often have in our minds is,

18
00:03:05,000 --> 00:03:09,000
how can we change? How should we change?

19
00:03:09,000 --> 00:03:12,000
What sort of person do I want to become?

20
00:03:12,000 --> 00:03:17,000
How can I become the sort of person that I want to become?

21
00:03:17,000 --> 00:03:23,000
And so these reflections are really more like admonishments,

22
00:03:23,000 --> 00:03:35,000
self admonishments, a great being reflects on ways that they can improve.

23
00:03:35,000 --> 00:03:41,000
And someone more importantly here I think is someone who understands this teaching.

24
00:03:41,000 --> 00:03:56,000
It starts to understand that, well, we really aren't prepared or we really aren't good at this.

25
00:03:56,000 --> 00:04:10,000
Or we can't be any old person of all the persons that we might be and all the qualities we might develop.

26
00:04:10,000 --> 00:04:15,000
There are some requirements that we're going to have to fulfill if we want to succeed.

27
00:04:15,000 --> 00:04:19,000
But it doesn't mean come here having fulfilled them.

28
00:04:19,000 --> 00:04:23,000
It means fulfilled them as a part of our practice.

29
00:04:23,000 --> 00:04:27,000
So the direction that our practice has to take.

30
00:04:27,000 --> 00:04:33,000
So the story goes that Anuruda was staying in the dear park.

31
00:04:33,000 --> 00:04:42,000
In, oh no, blessed was staying in the dear park. Anuruda was staying in JT.

32
00:04:42,000 --> 00:04:57,000
JT is a place, I guess, among the JT is in a bamboo park.

33
00:04:57,000 --> 00:05:04,000
And, well, you have to know about Anuruda. He's, you know, the story about the Nutty Cakes menu, you know.

34
00:05:04,000 --> 00:05:12,000
But Anuruda, you might say maybe he was a little bit distracted.

35
00:05:12,000 --> 00:05:15,000
I think we can say about Anuruda.

36
00:05:15,000 --> 00:05:19,000
Anuruda was one of the Buddha's relatives.

37
00:05:19,000 --> 00:05:29,000
And he had a brother, Mahanama. And all the Buddha's relatives were becoming monks.

38
00:05:29,000 --> 00:05:36,000
And so Mahanama said to Anuruda, look, no one from our family has become a monk following the Buddha,

39
00:05:36,000 --> 00:05:41,000
has gone and become a student of the Buddha. So one of us should.

40
00:05:41,000 --> 00:05:45,000
Otherwise, what are they going to say about us?

41
00:05:45,000 --> 00:05:49,000
And then, and he said, so Anuruda, you pick.

42
00:05:49,000 --> 00:05:53,000
You want to be a monk, go and live it with the Buddha,

43
00:05:53,000 --> 00:06:03,000
or you want to just stay as household, stay in the world, get a job and so on.

44
00:06:03,000 --> 00:06:08,000
Anuruda says, oh, well, I've heard about how those monks live off in the forest.

45
00:06:08,000 --> 00:06:15,000
It sounds quite, I don't know if I can do that. I think I'll stay where I am.

46
00:06:15,000 --> 00:06:18,000
Thank you very much.

47
00:06:18,000 --> 00:06:21,000
Because he was kind of, well, they say he was royalty.

48
00:06:21,000 --> 00:06:27,000
I don't know what that means exactly, but he was well off, apparently.

49
00:06:27,000 --> 00:06:30,000
And so Mahanama says, okay, then I'll become a monk.

50
00:06:30,000 --> 00:06:33,000
And the first I got to teach you, because Anuruda was young.

51
00:06:33,000 --> 00:06:40,000
He said, I'm going to teach you how to lead, how to take care of business, right?

52
00:06:40,000 --> 00:06:44,000
And so he starts telling him about all the things that he has to do.

53
00:06:44,000 --> 00:06:50,000
Well, first you have to plow the field, and then you have to plant the grain

54
00:06:50,000 --> 00:06:54,000
and make sure it all gets in at the right time, and then you have to.

55
00:06:54,000 --> 00:06:57,000
So on, and so on, bring the grain in and thrash it,

56
00:06:57,000 --> 00:07:01,000
and whatever it is that you do with grain and turn it into flower

57
00:07:01,000 --> 00:07:05,000
and take it to market, and you have to make sure everyone's working, and everyone gets paid.

58
00:07:05,000 --> 00:07:10,000
And he starts going through all this stuff, and Anuruda says, wait, wait, wait, wait.

59
00:07:10,000 --> 00:07:16,000
No, no, I'll become a monk, thank you, that's fine.

60
00:07:16,000 --> 00:07:19,000
And so he goes off and lives, he goes and becomes a monk.

61
00:07:19,000 --> 00:07:21,000
That's the story of Anuruda.

62
00:07:21,000 --> 00:07:24,000
But he wasn't a bad monk.

63
00:07:24,000 --> 00:07:28,000
He was actually quite an exceptional monk, but as far as meditation goes,

64
00:07:28,000 --> 00:07:33,000
he seems to have gotten distracted with what we call summit meditation.

65
00:07:33,000 --> 00:07:38,000
So he had great thoughts, and this suit is interesting in that regard as well.

66
00:07:38,000 --> 00:07:43,000
For us, it's more interesting because of the contents, but the context is also interesting.

67
00:07:43,000 --> 00:07:45,000
So Anuruda comes up with these seven thoughts.

68
00:07:45,000 --> 00:07:52,000
He's sitting in meditation like all of us, and he thinks to himself, he says,

69
00:07:52,000 --> 00:08:03,000
wow, this dhamma, this dhamma is for one who has few wishes, who has few desires.

70
00:08:03,000 --> 00:08:07,000
If you've got lots of desires, you're not going to be able to practice this dhamma.

71
00:08:07,000 --> 00:08:10,000
It's not going to work for you.

72
00:08:10,000 --> 00:08:13,000
That's the first one.

73
00:08:13,000 --> 00:08:17,000
Second, this dhamma is for one who is content.

74
00:08:17,000 --> 00:08:22,000
This one who has discontent won't be able to practice.

75
00:08:22,000 --> 00:08:33,000
Third, this dhamma is for one who is included.

76
00:08:33,000 --> 00:08:45,000
Not for one who enjoys society.

77
00:08:45,000 --> 00:08:52,000
Before this is, this dhamma is for one who has strong effort.

78
00:08:52,000 --> 00:08:56,000
Not for one who is lazy.

79
00:08:56,000 --> 00:09:03,000
Five, this dhamma is for one who has well-established mindfulness.

80
00:09:03,000 --> 00:09:06,000
The mindfulness that we're cultivating here.

81
00:09:06,000 --> 00:09:10,000
Not for one who has forgotten themselves.

82
00:09:10,000 --> 00:09:19,000
They're mindfulness forgotten.

83
00:09:19,000 --> 00:09:24,000
Number six, this dhamma is for one who is well-composed, concentrated,

84
00:09:24,000 --> 00:09:25,000
or focused.

85
00:09:25,000 --> 00:09:30,000
Not for one who is unfocused.

86
00:09:30,000 --> 00:09:34,000
Number seven, this dhamma is for one who is wise.

87
00:09:34,000 --> 00:09:43,000
It's not for one who is unwise.

88
00:09:43,000 --> 00:09:50,000
It resonates because it makes you, it's what we realize that this is not some simple teaching.

89
00:09:50,000 --> 00:09:53,000
It's not what you would normally think of meditation.

90
00:09:53,000 --> 00:09:59,000
It's something I'll sit down and I'll focus on a candle flame and I'll say fire or fire.

91
00:09:59,000 --> 00:10:08,000
I mean, not to disparage that sort of meditation, but it's kind of stupid.

92
00:10:08,000 --> 00:10:15,000
It's kind of...

93
00:10:15,000 --> 00:10:24,000
There's no cultivation or development of...

94
00:10:24,000 --> 00:10:29,000
I mean, it doesn't challenge you in so many ways.

95
00:10:29,000 --> 00:10:34,000
Of course, that meditation, any kind of meditation does challenge you.

96
00:10:34,000 --> 00:10:44,000
Maybe in fact, all of these qualities or many of these qualities are required for any meditation.

97
00:10:44,000 --> 00:10:45,000
You can't just come here.

98
00:10:45,000 --> 00:10:50,000
It's something you come here and it just happens to you.

99
00:10:50,000 --> 00:10:56,000
Something that requires you to come to change, to progress as a being.

100
00:10:56,000 --> 00:11:06,000
That's really maybe what's so impressive or inspiring about this practice is how much you do change.

101
00:11:06,000 --> 00:11:16,000
You leave here and it's kind of eerie how unsettling how different a person you can be.

102
00:11:16,000 --> 00:11:21,000
A better person. I mean, happier person, more content, cleaner.

103
00:11:21,000 --> 00:11:26,000
You feel so much cleaner and pure in the mind.

104
00:11:26,000 --> 00:11:29,000
And stronger having cultivated all of these things.

105
00:11:29,000 --> 00:11:31,000
So the Buddha comes to Anuruna.

106
00:11:31,000 --> 00:11:33,000
He hears these are seven.

107
00:11:33,000 --> 00:11:37,000
Buddha comes to Anuruna and he says, well, that's really good Anuruna.

108
00:11:37,000 --> 00:11:44,000
I mean, that's really true and great being thinks these things.

109
00:11:44,000 --> 00:11:48,000
These thoughts that you have are the thoughts of a great being.

110
00:11:48,000 --> 00:11:51,000
So there's one more.

111
00:11:51,000 --> 00:11:54,000
And it's really defining for the Buddha's teaching.

112
00:11:54,000 --> 00:12:00,000
And it's this one which allowed Anuruna to become enlightened.

113
00:12:00,000 --> 00:12:12,000
And so the Buddha says, this dhamma is for one who is one who is engaged in...

114
00:12:12,000 --> 00:12:15,000
And he says, delights in, but it's just a word.

115
00:12:15,000 --> 00:12:20,000
Anuruna delights in non-proliferation.

116
00:12:20,000 --> 00:12:22,000
That's Bikabodhi's translation.

117
00:12:22,000 --> 00:12:24,000
I think I'm out of the poly here.

118
00:12:24,000 --> 00:12:28,000
Bikabodhi says, non-proliferation.

119
00:12:28,000 --> 00:12:31,000
Not one who delights in proliferation.

120
00:12:31,000 --> 00:12:36,000
You might say, one who doesn't delight in proliferation, maybe better.

121
00:12:36,000 --> 00:12:38,000
I'll explain that.

122
00:12:38,000 --> 00:12:42,000
And the important one.

123
00:12:42,000 --> 00:12:47,000
Well, proliferation, I should explain because it's the one that changes everything from proliferation

124
00:12:47,000 --> 00:12:51,000
is when you make more of something than it is, right?

125
00:12:51,000 --> 00:12:56,000
I'll talk about it in more detail, but it's that one which made him realize that actually,

126
00:12:56,000 --> 00:13:04,000
in some ways all these thoughts that he were having, he was having their kind of a proliferation in themselves.

127
00:13:04,000 --> 00:13:11,000
And he got in trouble on this account more than once for being distracted.

128
00:13:11,000 --> 00:13:20,000
But it was this that was the missing link, and this is really what defines insight meditation.

129
00:13:20,000 --> 00:13:29,000
You can't be the sort of person who lives a complicated or has a complicated philosophy,

130
00:13:29,000 --> 00:13:43,000
in the sense of things like God and one who is engaged in karmic activities, right?

131
00:13:43,000 --> 00:13:53,000
The ambition or philosophies theory is about the universe and reality.

132
00:13:53,000 --> 00:13:59,000
Taking away, taking the mind away from what is real, right?

133
00:13:59,000 --> 00:14:04,000
Because all that is real is our sense of seeing, hearing, spelling, tasting, feeling, thinking.

134
00:14:04,000 --> 00:14:09,000
When you get outside that, you get into the realm of concepts which is infinite.

135
00:14:09,000 --> 00:14:17,000
What you see can be infinite, but seeing is always going to be seeing.

136
00:14:17,000 --> 00:14:24,000
It's only one thing and one thing alone.

137
00:14:24,000 --> 00:14:29,000
Anyway, so all eight of these are useful qualities, so it's good for us to think about.

138
00:14:29,000 --> 00:14:35,000
And then the Buddha says this to an order and then he goes home back to where he's staying

139
00:14:35,000 --> 00:14:39,000
and tells talks to the monks about these things and explains them.

140
00:14:39,000 --> 00:14:42,000
So that's what I'll do, briefly and go over them.

141
00:14:42,000 --> 00:14:44,000
It's good for us to think about this.

142
00:14:44,000 --> 00:14:51,000
What are the qualities you need to cultivate through the practice?

143
00:14:51,000 --> 00:15:03,000
So having few desires, the ideal limitation is designed to help us overcome our desires.

144
00:15:03,000 --> 00:15:10,000
But what Anurada is saying is quite true that if you're obsessed with your desires,

145
00:15:10,000 --> 00:15:19,000
if you need to have this kind of food or if you need to have this kind of bed or this kind of clothing or this kind of...

146
00:15:19,000 --> 00:15:21,000
That's right, that's actually the next one.

147
00:15:21,000 --> 00:15:29,000
But if you need, if you have a strong attachment to anything,

148
00:15:29,000 --> 00:15:34,000
the Buddha says actually for this one,

149
00:15:34,000 --> 00:15:39,000
one who wants to be, who is proud of themselves and wants people to know them,

150
00:15:39,000 --> 00:15:44,000
wants people to like them.

151
00:15:44,000 --> 00:15:48,000
One who is ambitious, I think, is the point.

152
00:15:48,000 --> 00:15:51,000
One who wants to be famous or so on.

153
00:15:51,000 --> 00:15:56,000
One who wants to be powerful, wants to be in control.

154
00:15:56,000 --> 00:16:01,000
I mean, this would be a big thing for monks wanting to be the head monk wanting to be a teacher,

155
00:16:01,000 --> 00:16:14,000
wanting to be well-esteem, wanting everyone to give them food and clothing and so on.

156
00:16:14,000 --> 00:16:19,000
But it applies, of course, in lay life, even stronger our ambitions, wanting to be this wanting to be that,

157
00:16:19,000 --> 00:16:23,000
wanting to be rich, wanting to...

158
00:16:23,000 --> 00:16:27,000
All of this gets in the way.

159
00:16:27,000 --> 00:16:30,000
All of our desires here.

160
00:16:30,000 --> 00:16:33,000
It can take... it can happen in meditation centers.

161
00:16:33,000 --> 00:16:37,000
You can attach to the food and get attached to your...

162
00:16:37,000 --> 00:16:44,000
to the quiet and get attached to many different things.

163
00:16:52,000 --> 00:16:56,000
The second one, regards to being content.

164
00:16:56,000 --> 00:17:06,000
The tenth, the dumbest for one who is content, well, this is in regards to things like food and robes and possessions.

165
00:17:06,000 --> 00:17:11,000
Being content with what you have.

166
00:17:11,000 --> 00:17:23,000
And really being content with the conditions, if it's loud, if it's cold, if it's hot.

167
00:17:23,000 --> 00:17:32,000
If it's ugly, you don't like your room because it's ugly, or you want to go out in nature or something.

168
00:17:32,000 --> 00:17:44,000
Being content with the present reality, being content, is content being content means being objectives.

169
00:17:44,000 --> 00:17:51,000
Being content means seeing things as they are without saying this is this rather than,

170
00:17:51,000 --> 00:17:57,000
which it was something else, which is discontent.

171
00:17:57,000 --> 00:18:06,000
And so if you have this discontent, it's very difficult to become objective.

172
00:18:06,000 --> 00:18:13,000
The third is resorting to solitude.

173
00:18:13,000 --> 00:18:21,000
A meditator like I kind of ruined the realises that if you're just sitting around chatting,

174
00:18:21,000 --> 00:18:29,000
if you're to seek out company, that's very difficult to get to this point where you can see who you are.

175
00:18:29,000 --> 00:18:33,000
So you can understand yourself.

176
00:18:33,000 --> 00:18:40,000
Company distracts you, it detracts you, it gets you caught up in concepts.

177
00:18:40,000 --> 00:18:43,000
It's very hard to talk with other people about ultimate reality.

178
00:18:43,000 --> 00:18:54,000
You'll inevitably get caught up in abstract ideas and talking about the world and what's going on in the world and so on.

179
00:18:54,000 --> 00:19:01,000
If you want to understand reality, it's not something that comes from discussion not easily.

180
00:19:01,000 --> 00:19:09,000
Ultimately, teachings, even like teachings like this are just the point you're in the right direction.

181
00:19:09,000 --> 00:19:22,000
And so the Buddha says here, he says, for a teacher, when the monk goes off to the forest, if it's a good monk,

182
00:19:22,000 --> 00:19:31,000
all the everyone's going to go and see them, even royalty and all kinds of people are going to go and ask for teaching from them.

183
00:19:31,000 --> 00:19:39,000
And he says, when the monk teaches, they're actually thinking, what can I say to this person to make them go away?

184
00:19:39,000 --> 00:19:42,000
This is how a monk should teach.

185
00:19:42,000 --> 00:19:49,000
It's an interesting thing you'd think, well, that's not very nice of the monk, but it's interesting in two ways.

186
00:19:49,000 --> 00:19:54,000
I mean, it reminds us that ultimately we have to look, we have to help ourselves.

187
00:19:54,000 --> 00:20:00,000
If you're not benefiting yourself, you don't end up being a good teacher anyway.

188
00:20:00,000 --> 00:20:10,000
It's improper in that way, but also because if you're teaching people so that they come and stay around with you, right?

189
00:20:10,000 --> 00:20:12,000
When are they going to go and meditate?

190
00:20:12,000 --> 00:20:23,000
If a meditation teacher is all about keeping them in their presence, talking to them, chatting about even chatting about the dhamma,

191
00:20:23,000 --> 00:20:25,000
then when do they get to meditate?

192
00:20:25,000 --> 00:20:30,000
Everything will often cultivate the teaching.

193
00:20:30,000 --> 00:20:35,000
I've seen this in practice. It's actually quite interesting.

194
00:20:35,000 --> 00:20:40,000
And it doesn't mean saying, get out of my sight because that doesn't get rid of people.

195
00:20:40,000 --> 00:20:42,000
That just causes lots of problems.

196
00:20:42,000 --> 00:20:44,000
It makes them get upset at you.

197
00:20:44,000 --> 00:20:48,000
It makes them a worse meditate and makes them agitated, right?

198
00:20:48,000 --> 00:20:53,000
So you have to teach them that you do whatever you can to make them content.

199
00:20:53,000 --> 00:20:55,000
It's a very interesting thing to say.

200
00:20:55,000 --> 00:20:59,000
I mean, it's quite hard, but it's quite true.

201
00:20:59,000 --> 00:21:03,000
Do whatever you can to satisfy them is actually what they're saying.

202
00:21:03,000 --> 00:21:07,000
Because if you say something and they're not satisfied, you say, okay, yeah, we'll do this and that's enough.

203
00:21:07,000 --> 00:21:09,000
Thinking, I want this person out of my sight.

204
00:21:09,000 --> 00:21:13,000
They're just going to say, well, what do you mean by that?

205
00:21:13,000 --> 00:21:21,000
It's very hard to satisfy a meditator, but when you do, they're happy and they go away and meditate.

206
00:21:21,000 --> 00:21:24,000
And the monk can go back into their meditation as well.

207
00:21:24,000 --> 00:21:27,000
Or the teacher, whoever it is.

208
00:21:27,000 --> 00:21:30,000
The next one, energetic.

209
00:21:30,000 --> 00:21:32,000
One has to be energetic.

210
00:21:32,000 --> 00:21:35,000
I'm of course very impressed.

211
00:21:35,000 --> 00:21:38,000
Always impressed to see how energetic the meditators are.

212
00:21:38,000 --> 00:21:41,000
Everyone's putting their effort in.

213
00:21:41,000 --> 00:21:44,000
It pays off, you know.

214
00:21:44,000 --> 00:21:46,000
It can seem daunting.

215
00:21:46,000 --> 00:21:48,000
How many days you have to stay, you know?

216
00:21:48,000 --> 00:21:50,000
When I did my first course,

217
00:21:50,000 --> 00:21:53,000
I remember counting the days.

218
00:21:53,000 --> 00:21:55,000
My second course was 10 days.

219
00:21:55,000 --> 00:21:57,000
I remember I had these little stones.

220
00:21:57,000 --> 00:21:59,000
And I put one stone for each day.

221
00:21:59,000 --> 00:22:01,000
Oh, only three days left.

222
00:22:13,000 --> 00:22:17,000
You have to build up energy.

223
00:22:17,000 --> 00:22:20,000
This is the sort of person you have to be.

224
00:22:20,000 --> 00:22:21,000
And you can.

225
00:22:21,000 --> 00:22:24,000
I mean, the remarkable thing is how much energy you can cultivate

226
00:22:24,000 --> 00:22:26,000
if you're in the present moment.

227
00:22:26,000 --> 00:22:29,000
To really succeed, you need to conserve energy.

228
00:22:29,000 --> 00:22:32,000
You don't, in fact, need to push really hard.

229
00:22:32,000 --> 00:22:36,000
You just have to stop tiring yourself out with all these distractions

230
00:22:36,000 --> 00:22:41,000
and thoughts and wants and needs and likes and dislikes

231
00:22:41,000 --> 00:22:44,000
and judgments and arrogance and conceit and all,

232
00:22:44,000 --> 00:22:47,000
and all the many things that tires out.

233
00:22:47,000 --> 00:22:49,000
And then you realize, wow, we've got this.

234
00:22:49,000 --> 00:22:52,000
Now I've got a lot of energy left.

235
00:22:52,000 --> 00:22:55,000
Now that I'm not wasting all that energy on my mind

236
00:22:55,000 --> 00:23:03,000
on abstracts and concepts, past, future, and so on.

237
00:23:03,000 --> 00:23:05,000
That's really what laziness is.

238
00:23:05,000 --> 00:23:06,000
It's laziness.

239
00:23:06,000 --> 00:23:08,000
We think of laziness as lack of energy.

240
00:23:08,000 --> 00:23:11,000
But in fact, laziness is too much.

241
00:23:11,000 --> 00:23:21,000
Too much use of energy.

242
00:23:21,000 --> 00:23:25,000
Because desire, for example, the desire to sleep takes a lot of energy,

243
00:23:25,000 --> 00:23:29,000
the desire for it, the attachment to it.

244
00:23:29,000 --> 00:23:32,000
When you give that up, you're, wow, I don't need to sleep so much.

245
00:23:32,000 --> 00:23:36,000
One more energy.

246
00:23:36,000 --> 00:23:41,000
Number five, mindfulness while established.

247
00:23:41,000 --> 00:23:43,000
One who is very mindful.

248
00:23:43,000 --> 00:23:45,000
Well, we know this one.

249
00:23:45,000 --> 00:23:48,000
You can all see the difference between being mindful

250
00:23:48,000 --> 00:23:50,000
and being unmindful.

251
00:23:50,000 --> 00:23:52,000
When you're mindful, you suddenly wake up.

252
00:23:52,000 --> 00:23:54,000
You're suddenly present.

253
00:23:54,000 --> 00:23:56,000
You're suddenly here.

254
00:23:56,000 --> 00:23:58,000
When you're unmindful, you get lost.

255
00:23:58,000 --> 00:23:59,000
You're no longer present.

256
00:23:59,000 --> 00:24:02,000
You're no longer here.

257
00:24:02,000 --> 00:24:06,000
And you realize what deep knowledge and wisdom and insight

258
00:24:06,000 --> 00:24:09,000
comes from being mindful, from being present.

259
00:24:09,000 --> 00:24:13,000
How much you learn about yourself and how much

260
00:24:13,000 --> 00:24:19,000
suffering you overcome, just by being mindful.

261
00:24:19,000 --> 00:24:23,000
Number six, one has to be concentrated.

262
00:24:23,000 --> 00:24:25,000
There's damage for one who is concentrated.

263
00:24:25,000 --> 00:24:30,000
If you're distracted, of course.

264
00:24:30,000 --> 00:24:34,000
You can't see clearly.

265
00:24:34,000 --> 00:24:38,000
And so we enter into great states of concentration, right?

266
00:24:38,000 --> 00:24:41,000
After some time, your mind becomes quite focused.

267
00:24:41,000 --> 00:24:43,000
Even when you have distracting thoughts, you're there.

268
00:24:43,000 --> 00:24:44,000
You're with them.

269
00:24:44,000 --> 00:24:45,000
You're present.

270
00:24:45,000 --> 00:24:47,000
I mean, this is the challenge.

271
00:24:47,000 --> 00:24:48,000
This is what we're cultivating.

272
00:24:48,000 --> 00:24:54,000
You can't ever see the truth until you become focused.

273
00:24:54,000 --> 00:24:55,000
Don't worry.

274
00:24:55,000 --> 00:24:57,000
I mean, it comes over time.

275
00:24:57,000 --> 00:25:00,000
You think halfway through the course and boy, I'm really

276
00:25:00,000 --> 00:25:01,000
unfocused.

277
00:25:01,000 --> 00:25:02,000
But you're dealing with it.

278
00:25:02,000 --> 00:25:04,000
You're training in it.

279
00:25:04,000 --> 00:25:08,000
And eventually you become more focused.

280
00:25:08,000 --> 00:25:14,000
And you start to see more clearly.

281
00:25:14,000 --> 00:25:17,000
Number seven, one has to be wise.

282
00:25:17,000 --> 00:25:22,000
The stem is not for one who is unwise.

283
00:25:22,000 --> 00:25:24,000
It's not a thing that you can just do.

284
00:25:24,000 --> 00:25:28,000
You can't just follow my instructions.

285
00:25:28,000 --> 00:25:30,000
You have to be present.

286
00:25:30,000 --> 00:25:33,000
I mean, wisdom in many ways comes from just being mindful.

287
00:25:33,000 --> 00:25:37,000
But you have to be open to it.

288
00:25:37,000 --> 00:25:39,000
You can't just, like, say to yourself, pain, vein,

289
00:25:39,000 --> 00:25:43,000
and then wonder why it's not going away and so on.

290
00:25:43,000 --> 00:25:46,000
You have to be open to what you're experiencing.

291
00:25:46,000 --> 00:25:50,000
I mean, it's quite common to be discouraged when you start

292
00:25:50,000 --> 00:25:52,000
to see the truth.

293
00:25:52,000 --> 00:25:55,000
And that discouragement is a sign of lack of wisdom.

294
00:25:55,000 --> 00:25:56,000
It's common.

295
00:25:56,000 --> 00:25:57,000
I mean, this is, we all are.

296
00:25:57,000 --> 00:25:58,000
I've been there.

297
00:25:58,000 --> 00:26:02,000
We start to complain about, I'm doing what I can,

298
00:26:02,000 --> 00:26:03,000
but it's not going away.

299
00:26:03,000 --> 00:26:04,000
It's not getting better.

300
00:26:04,000 --> 00:26:07,000
I can't control it.

301
00:26:07,000 --> 00:26:09,000
It's not stable.

302
00:26:09,000 --> 00:26:12,000
My mind is chaotic.

303
00:26:12,000 --> 00:26:14,000
I think I've got it all fixed.

304
00:26:14,000 --> 00:26:16,000
And then suddenly something comes out of nowhere.

305
00:26:16,000 --> 00:26:20,000
I mean, this is actually the truth that we're trying to see.

306
00:26:20,000 --> 00:26:25,000
The meditation is going to give you a big slap in the face.

307
00:26:25,000 --> 00:26:28,000
It's a wake-up call.

308
00:26:28,000 --> 00:26:30,000
It's going in many ways.

309
00:26:30,000 --> 00:26:34,000
The meditation beats, it defeats you.

310
00:26:34,000 --> 00:26:36,000
You don't conquer and win.

311
00:26:36,000 --> 00:26:37,000
We're not here to succeed.

312
00:26:37,000 --> 00:26:39,000
The meditation will end up defeating us.

313
00:26:39,000 --> 00:26:41,000
And that's the purpose.

314
00:26:41,000 --> 00:26:43,000
Because then you let go of yourself.

315
00:26:43,000 --> 00:26:46,000
You can see it.

316
00:26:46,000 --> 00:26:51,000
You give up and you let go.

317
00:26:51,000 --> 00:26:53,000
Let it take wisdom.

318
00:26:53,000 --> 00:26:55,000
It's not just pushing, pushing.

319
00:26:55,000 --> 00:26:56,000
I'm going to win.

320
00:26:56,000 --> 00:26:58,000
I'm going to succeed.

321
00:26:58,000 --> 00:27:06,000
When you practice, you start to see how poorly you're practicing.

322
00:27:06,000 --> 00:27:08,000
Meditation is much more about failing.

323
00:27:08,000 --> 00:27:10,000
In fact, then it is about succeeding.

324
00:27:10,000 --> 00:27:15,000
Because it's your failures that show you that you're wrong.

325
00:27:15,000 --> 00:27:19,000
You're hitting your head against the wall, trying to succeed.

326
00:27:19,000 --> 00:27:22,000
Until you realize that there's nothing to succeed at.

327
00:27:22,000 --> 00:27:24,000
I'm just hitting my head against the wall.

328
00:27:24,000 --> 00:27:33,000
This isn't the way I do suffering.

329
00:27:33,000 --> 00:27:37,000
And finally, the last one really has a lot to do with wisdom.

330
00:27:37,000 --> 00:27:42,000
And it could be seen as a sort of a wisdom, but non-proliferation.

331
00:27:42,000 --> 00:27:45,000
I mean, the Buddha includes this in here.

332
00:27:45,000 --> 00:27:46,000
I really am.

333
00:27:46,000 --> 00:27:49,000
As I said, it's the key.

334
00:27:49,000 --> 00:27:52,000
It's really the key to practice.

335
00:27:52,000 --> 00:27:55,000
It's the key to insight and wisdom.

336
00:27:55,000 --> 00:27:57,000
It's to see things as they are.

337
00:27:57,000 --> 00:27:59,000
Yatabut and Yannadasana.

338
00:27:59,000 --> 00:28:02,000
Knowledge and wisdom of things as they are.

339
00:28:02,000 --> 00:28:09,000
Knowledge and vision of things as they are.

340
00:28:09,000 --> 00:28:17,000
And the point being that knowledge and vision of things as they are is very limited.

341
00:28:17,000 --> 00:28:18,000
Right?

342
00:28:18,000 --> 00:28:20,000
How are things?

343
00:28:20,000 --> 00:28:23,000
If I ask you how are things?

344
00:28:23,000 --> 00:28:26,000
Most people will tell me, oh, you know my back hurts.

345
00:28:26,000 --> 00:28:31,000
And there's this person who I'm having conflict with.

346
00:28:31,000 --> 00:28:34,000
My kid is sick, and my car is...

347
00:28:34,000 --> 00:28:36,000
This is papancha.

348
00:28:36,000 --> 00:28:38,000
This isn't how things really are.

349
00:28:38,000 --> 00:28:44,000
How things really are as well, seeing, hearing, hearing, smelling, smelling, tasting,

350
00:28:44,000 --> 00:28:46,000
tasting, feeling is thinking.

351
00:28:46,000 --> 00:28:49,000
It's quite simple.

352
00:28:49,000 --> 00:28:54,000
So when I say knowledge of things as they are, that really doesn't mean anything to us

353
00:28:54,000 --> 00:28:58,000
because the way we see things as the...

354
00:28:58,000 --> 00:29:03,000
How things are to us is quite complicated for the most part.

355
00:29:03,000 --> 00:29:04,000
Right?

356
00:29:04,000 --> 00:29:07,000
We're worried about ourselves, we're worried about other people,

357
00:29:07,000 --> 00:29:11,000
we're worried about our situation, where our...

358
00:29:11,000 --> 00:29:14,000
How things are, things for us is papancha.

359
00:29:14,000 --> 00:29:18,000
It's complicated.

360
00:29:18,000 --> 00:29:20,000
Neep papancha.

361
00:29:20,000 --> 00:29:26,000
Nonproliferation or non-complication is when we really do,

362
00:29:26,000 --> 00:29:29,000
seeing, seeing, hearing, and hearing.

363
00:29:29,000 --> 00:29:34,000
And it challenges us in so many ways because then it's...

364
00:29:34,000 --> 00:29:39,000
Even in the face of great stuff, great pain, or death, or...

365
00:29:39,000 --> 00:29:42,000
Things that would make normal people...

366
00:29:42,000 --> 00:29:50,000
Cower, make normal people lose their minds.

367
00:29:50,000 --> 00:29:54,000
Loss, danger.

368
00:29:54,000 --> 00:29:57,000
When I'm a silly example, I remember it was just...

369
00:29:57,000 --> 00:30:00,000
It was really funny, and I've said I've talked about this before.

370
00:30:00,000 --> 00:30:03,000
After I finished my course, I finished with another man.

371
00:30:03,000 --> 00:30:07,000
So we went together into the city, and we're sitting in the cafe,

372
00:30:07,000 --> 00:30:10,000
and we're just kind of sitting there and like,

373
00:30:10,000 --> 00:30:15,000
oh, here, wow, we've just done 20 days of meditation.

374
00:30:15,000 --> 00:30:20,000
And we're having a cup of coffee, I think, in one of the cafes.

375
00:30:20,000 --> 00:30:25,000
And suddenly, after the side of us, someone drops something.

376
00:30:25,000 --> 00:30:28,000
I'm still at this date, I don't know what it was exactly,

377
00:30:28,000 --> 00:30:32,000
but it must have been a bunch of plates and a big crash.

378
00:30:32,000 --> 00:30:37,000
And you could sort of tell that the whole restaurant went like this.

379
00:30:37,000 --> 00:30:40,000
And the two of us just sat there, and were like hearing,

380
00:30:40,000 --> 00:30:43,000
and I looked at him and we both laughed,

381
00:30:43,000 --> 00:30:46,000
because neither one of us flinted.

382
00:30:46,000 --> 00:30:52,000
It's a silly little example, but I think it illustrates

383
00:30:52,000 --> 00:31:00,000
the sort of idea of nipapancha.

384
00:31:00,000 --> 00:31:15,000
Where everyone else becomes disturbed by the world.

385
00:31:15,000 --> 00:31:21,000
And one who doesn't have papancha is no longer disturbed by the world.

386
00:31:21,000 --> 00:31:26,000
Nothing can hurt them, nothing can phase them, shake them.

387
00:31:26,000 --> 00:31:33,000
They find great peace, ordinary people find it a little bit boring, I think.

388
00:31:33,000 --> 00:31:37,000
And an interesting, right?

389
00:31:37,000 --> 00:31:40,000
We always like it when everyone else is upset.

390
00:31:40,000 --> 00:31:41,000
Everyone will.

391
00:31:41,000 --> 00:31:42,000
Yeah, look at that.

392
00:31:42,000 --> 00:31:44,000
Everyone's looking at the broken dishes.

393
00:31:44,000 --> 00:31:47,000
Look at us all looking, and those two who aren't one of those weirdos.

394
00:31:47,000 --> 00:31:49,000
They don't care.

395
00:31:49,000 --> 00:31:59,000
They lack of concern for the broken dishes, lack of concern for the world.

396
00:31:59,000 --> 00:32:00,000
It's funny, you know?

397
00:32:00,000 --> 00:32:03,000
We get so concerned with the state of things,

398
00:32:03,000 --> 00:32:08,000
and we all talk and talk about how things are going wrong.

399
00:32:08,000 --> 00:32:16,000
And we worry every day, and we watch the news anticipating the next big disaster.

400
00:32:16,000 --> 00:32:22,000
We're always seeking out romance, we're seeking out companionship,

401
00:32:22,000 --> 00:32:27,000
and seeking out money, seeking out employment.

402
00:32:27,000 --> 00:32:33,000
I mean, usually seeking, often seeking out beyond what is necessary we seek out

403
00:32:33,000 --> 00:32:39,000
because we want possessions and so on.

404
00:32:39,000 --> 00:32:46,000
We get caught up in the world in so many different ways,

405
00:32:46,000 --> 00:32:56,000
thinking we're going to get better that way, thinking that we're going to set up a system of complexities

406
00:32:56,000 --> 00:33:01,000
that's going to be stable, satisfying, and controllable.

407
00:33:01,000 --> 00:33:08,000
Until we come here and see that it can never be stable, satisfying, or controllable.

408
00:33:08,000 --> 00:33:13,000
Because we see things as they are.

409
00:33:13,000 --> 00:33:18,000
We simplify things down so much that we're able to see the essence of reality.

410
00:33:18,000 --> 00:33:23,000
And we see that the essence is impermanent, unsatisfying, uncontrollable.

411
00:33:23,000 --> 00:33:26,000
And we delight in that.

412
00:33:26,000 --> 00:33:29,000
We stop delighting in this.

413
00:33:29,000 --> 00:33:33,000
Stop getting caught up in proliferation,

414
00:33:33,000 --> 00:33:38,000
because we see the stress and the suffering that it comes within the delusion, really.

415
00:33:38,000 --> 00:33:42,000
Just the wrongness of it all around.

416
00:33:42,000 --> 00:33:48,000
So the Buddha taught this and Anuru did practice this and reflecting on these eight.

417
00:33:48,000 --> 00:33:52,000
He became an arrow hunt based on this teaching.

418
00:33:52,000 --> 00:33:57,000
And he actually spoke of it later.

419
00:33:57,000 --> 00:34:03,000
And he mentions it in this set of verses that he apparently spoke or wrote down.

420
00:34:03,000 --> 00:34:06,000
No spoke, I guess.

421
00:34:06,000 --> 00:34:10,000
That the Buddha taught him something extra.

422
00:34:10,000 --> 00:34:16,000
And he mentioned specifically that the Buddha taught him this concept of a buncha.

423
00:34:16,000 --> 00:34:19,000
And that's the suit.

424
00:34:19,000 --> 00:34:29,000
I think that's something useful for us to think about a good demo for us to reflect upon.

425
00:34:29,000 --> 00:34:32,000
And that's the demo for tonight.

426
00:34:32,000 --> 00:34:57,000
Thank you all for coming up.

427
00:34:57,000 --> 00:35:04,000
And the questions page is not coming up.

428
00:35:04,000 --> 00:35:07,000
So no, I'm going to skip that.

429
00:35:07,000 --> 00:35:09,000
Thank you all for coming up.

430
00:35:09,000 --> 00:35:10,000
Questions?

431
00:35:10,000 --> 00:35:12,000
Any questions or more posted?

432
00:35:12,000 --> 00:35:35,000
I'm going to try to answer them tomorrow, maybe.

