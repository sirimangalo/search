1
00:00:00,000 --> 00:00:29,920
So, the idea of the mind being intrinsically pure,

2
00:00:29,920 --> 00:00:34,360
is interesting. There's some reference to that.

3
00:00:34,360 --> 00:00:38,640
Pombasurang bhikoui, pombasurangidang bhikoui jitam.

4
00:00:38,640 --> 00:00:44,000
This mind is luminous, and it's only through

5
00:00:44,000 --> 00:00:49,680
defilements are only something that are visited upon the mind.

6
00:00:49,680 --> 00:00:53,760
They are not intrinsic to the mind,

7
00:00:53,760 --> 00:00:56,880
but that's really more figurative than anything.

8
00:00:56,880 --> 00:01:03,040
I mean, in reality, the mind is a moment of awareness

9
00:01:03,040 --> 00:01:08,480
can be accompanied with anger. It was a part of what it is, really.

10
00:01:08,480 --> 00:01:12,320
So, it's just figuratively. What it means is that

11
00:01:12,320 --> 00:01:19,120
when you free yourself from anger, what's left over is quite pure.

12
00:01:19,120 --> 00:01:25,200
Yeah, I think I see what you're getting at it.

13
00:01:25,200 --> 00:01:29,760
It's possible if you want to try and equate the two.

14
00:01:29,760 --> 00:01:35,040
It's not a sense of becoming anything. It's a sense of removing the delusion,

15
00:01:35,040 --> 00:01:38,880
which is sort of what this Buddha Nature philosophy is all about.

16
00:01:38,880 --> 00:01:42,880
That we're in nibana. We just don't realize that kind of thing.

17
00:01:42,880 --> 00:01:48,640
This is where they say that some sarin nibana are just sides of the same coin.

18
00:01:48,640 --> 00:01:50,720
And nibana isn't something outside of yourself.

19
00:01:50,720 --> 00:02:20,560
It's something that you get once you strip away all the delusion, which is true.

20
00:02:20,560 --> 00:02:36,480
Okay, well, thank you all for coming out. See you all next time.

