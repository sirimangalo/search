I wish to get enlightened. Where do I start?
I love the good, good simple questions.
Well, start with my booklet on how to meditate.
Look up, utadamo how to meditate.
That's what I recommend to everyone.
Again, it's to plug my own, to toot my own horn.
But to explain that it's not really something that I've taught.
It's just my explanation of what I've been taught.
And since you're asking me, this is the best I can do for you.
If you ask someone else, they'll give you quite a different response.
But my response is what's in that book.
It's the best way to start to become enlightened from my point of view.
Having done that, having read the booklet, then you should make a decision whether you agree or disagree if you don't
or you should put it into practice.
Once you begin to practice the teachings in the booklet,
then if you decide you agree and it seems plausible that this is the way to become enlightened,
then there are other steps that should be taken.
And you shouldn't undertake proper meditation course with proper guidance and go from there.
