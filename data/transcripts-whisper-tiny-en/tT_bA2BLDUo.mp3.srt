1
00:00:00,000 --> 00:00:19,760
Good evening, and the number for tonight is about perversion.

2
00:00:19,760 --> 00:00:28,040
I don't have a better word, but it's an interesting word to talk about.

3
00:00:28,040 --> 00:00:30,040
That's the job.

4
00:00:30,040 --> 00:00:34,160
I guess I'm not going to tell you what is perverse.

5
00:00:34,160 --> 00:00:43,560
I want to address the issue of seeing things as perverse.

6
00:00:43,560 --> 00:00:49,560
I don't really know the etymology of the word perversion, but I know how it's used.

7
00:00:49,560 --> 00:00:54,600
What it means, basically, what it means to those who use it.

8
00:00:54,600 --> 00:00:59,400
It seems a lot by religious people.

9
00:00:59,400 --> 00:01:08,480
We have some idea of what's normal, and that which is not normal, we call perverse.

10
00:01:08,480 --> 00:01:11,600
And, of course, it goes deeper than that.

11
00:01:11,600 --> 00:01:24,280
We have the idea of a divine order, a divine sort of normal, and what falls outside of that.

12
00:01:24,280 --> 00:01:28,440
I call perverse.

13
00:01:28,440 --> 00:01:40,960
And that which we see as perverse as something one should feel guilty about, feel bad about

14
00:01:40,960 --> 00:01:56,880
hate oneself of my clothes, it's the idea of love the sinner, hate the sin.

15
00:01:56,880 --> 00:02:04,240
Sins are evil perverse.

16
00:02:04,240 --> 00:02:08,960
So I think we have to separate two things here.

17
00:02:08,960 --> 00:02:14,440
I think it can be perverse, but for something, of course, to be perverse, there has to

18
00:02:14,440 --> 00:02:30,800
be something, some reason, some benchmark or a measure stick by which we some normal.

19
00:02:30,800 --> 00:02:33,840
How could we say that something's normal?

20
00:02:33,840 --> 00:02:37,520
We have to figure that out first before we can call something perverse, but then we

21
00:02:37,520 --> 00:02:40,240
could call something perverse.

22
00:02:40,240 --> 00:02:52,800
But the other side is what it means to us when something is perverse.

23
00:02:52,800 --> 00:03:09,120
So I think we generally tend to agree that on Buddhism anyway, that hating yourself

24
00:03:09,120 --> 00:03:16,840
because of something that we might agree on as being perverse, is actually perverse.

25
00:03:16,840 --> 00:03:28,160
It's very wrong and twisted, perverse is like twisted, crooked, messed up.

26
00:03:28,160 --> 00:03:33,240
It's messed up to hate yourself because you're perverse, because you have perversion, because

27
00:03:33,240 --> 00:03:40,840
you're a pervert, and this word pervert, it's usually related to sexual things.

28
00:03:40,840 --> 00:03:48,840
So we call homosexuals perverts, of course, a lot of people call, most people I think call

29
00:03:48,840 --> 00:04:00,520
pedophiles perverts perverse, someone who is overly sexual, people might call them pervert.

30
00:04:00,520 --> 00:04:12,400
Things we use it in jokingly.

31
00:04:12,400 --> 00:04:22,800
But we see that hating ourselves or hating other people who are, according to some measure

32
00:04:22,800 --> 00:04:26,600
or other perverted, it's not useful.

33
00:04:26,600 --> 00:04:36,280
So what this means for meditators, as a meditator, is that anything you might conceive

34
00:04:36,280 --> 00:04:48,480
of, believe to be, or even come to see clearly as being perverse in some way, should

35
00:04:48,480 --> 00:04:57,080
not then become a source of self-hatred or loathing or guilt.

36
00:04:57,080 --> 00:05:03,040
You should recognize that it's just messed up, I think messed up in the colloquial usage,

37
00:05:03,040 --> 00:05:06,160
it's just a little more palatable here.

38
00:05:06,160 --> 00:05:17,320
But we're talking about something that is messed up, something that is skewed, something

39
00:05:17,320 --> 00:05:24,800
that is not quite right.

40
00:05:24,800 --> 00:05:33,440
In the Buddhist tradition, to sort of help clarify what exactly sorts of things we're

41
00:05:33,440 --> 00:05:46,560
talking about, we have three kinds of perversion, based on three kinds of mental activity.

42
00:05:46,560 --> 00:05:53,040
And so the word for perversion or that I'm translating here as perversion is vipalasa.

43
00:05:53,040 --> 00:06:04,040
We, of course, means outside or kind of could be like twisted or skewed.

44
00:06:04,040 --> 00:06:17,160
Vipalasa means some mental activity that is skewed.

45
00:06:17,160 --> 00:06:35,280
Then yeah, Vipalasa perversion of perception, Jita Vipalasa perversion of thought, and Jita

46
00:06:35,280 --> 00:06:43,080
Vipalasa perversion of view, perception, thought, and view.

47
00:06:43,080 --> 00:06:48,080
These are the three sorts of perversions.

48
00:06:48,080 --> 00:06:59,480
And what would it mean for a perception, what's the quality of a perverse thought?

49
00:06:59,480 --> 00:07:03,600
So we're not dealing with what God thinks is perverse or even what the Buddha thinks is

50
00:07:03,600 --> 00:07:09,000
perverse or what some divine order says is perverse.

51
00:07:09,000 --> 00:07:16,280
Perversion and Buddhism is something that is.

52
00:07:16,280 --> 00:07:21,720
You could say internally inconsistent.

53
00:07:21,720 --> 00:07:29,480
It's perverse because it just doesn't make sense.

54
00:07:29,480 --> 00:07:41,440
If I were to say, every day I push a boulder up a mountain and then every night it falls

55
00:07:41,440 --> 00:07:42,440
back down to the bottom.

56
00:07:42,440 --> 00:07:50,160
And every day I push it up again, trying to miss a myth of cystophus, to Greek myth and

57
00:07:50,160 --> 00:07:52,200
Greek mythology.

58
00:07:52,200 --> 00:07:58,680
I would say, I would say to myself, that's valuable work.

59
00:07:58,680 --> 00:08:01,080
There's some benefit to that.

60
00:08:01,080 --> 00:08:09,520
That's the sort of perversion we're talking about, that we do all the time, not so glaringly

61
00:08:09,520 --> 00:08:16,440
obvious perhaps, but I engage in a lot of activity that doesn't end up bringing any benefit

62
00:08:16,440 --> 00:08:20,520
and then we say it brings benefit, well that's messed up.

63
00:08:20,520 --> 00:08:21,880
That's perverse.

64
00:08:21,880 --> 00:08:26,680
And perverse isn't probably the right word, I just don't have a better one.

65
00:08:26,680 --> 00:08:36,760
I think my use of the word perversion here is to clarify that and Buddhism, we don't

66
00:08:36,760 --> 00:08:40,960
look at things quite that way in terms of feeling guilty or bad.

67
00:08:40,960 --> 00:08:49,960
We just see it as messed up, kind of deluded or delusional.

68
00:08:49,960 --> 00:08:59,680
So suddenly we pull us as some kind of perception, you experience something and the way you

69
00:08:59,680 --> 00:09:17,160
filter it, the way you react to it is messed up.

70
00:09:17,160 --> 00:09:23,520
Think that we pull us as you have some thoughts that arise about it and those thoughts

71
00:09:23,520 --> 00:09:30,600
are messed up and we did give you pull us as you have a view about it and belief about

72
00:09:30,600 --> 00:09:38,560
something and that's messed up because those are the categories, haven't you got into

73
00:09:38,560 --> 00:09:44,480
what sorts of things are perverse and we have four of them.

74
00:09:44,480 --> 00:09:53,200
So now we start to see what sorts of things are perverse and Buddhism and how we relate

75
00:09:53,200 --> 00:10:08,640
to how we look at perversion.

76
00:10:08,640 --> 00:10:23,000
The first one is Sumo Yibalasa, perversion of beauty and the second one is Sukho Yibalasa,

77
00:10:23,000 --> 00:10:25,880
perversion of happiness.

78
00:10:25,880 --> 00:10:37,320
There it is, Nietzsche Yibalasa, perversion of permanent.

79
00:10:37,320 --> 00:10:45,600
The fourth is Ata Yibalasa, perversion of self.

80
00:10:45,600 --> 00:10:50,600
So you see this is when language actually gets us in a little bit of trouble because we're

81
00:10:50,600 --> 00:10:57,400
basically going to say that beauty really is a perversion, right?

82
00:10:57,400 --> 00:11:00,920
You're perverted if you think something is beautiful.

83
00:11:00,920 --> 00:11:03,440
So that's not quite right.

84
00:11:03,440 --> 00:11:08,720
The word perversion probably doesn't apply but using the word allows us to see how Buddhism

85
00:11:08,720 --> 00:11:12,800
looks at things a little differently.

86
00:11:12,800 --> 00:11:21,760
Finding something beautiful is mixed up, it's a misperception.

87
00:11:21,760 --> 00:11:34,200
When you experience, when you see something, your perception of that's beautiful is arbitrary

88
00:11:34,200 --> 00:11:43,360
and not only arbitrary, but problematic, it's a very difficult one.

89
00:11:43,360 --> 00:11:53,760
I think here's where we get in on a little bit of trouble with pop, with mainstream audience.

90
00:11:53,760 --> 00:12:03,360
I'm not going to, I can't really sugarcoat this, except to say that it's not that beauty

91
00:12:03,360 --> 00:12:15,480
is evil, it's that reality is something deeper than beauty.

92
00:12:15,480 --> 00:12:28,440
We have a lot of trouble in the beginning with enjoying experiences and relishing, appreciating

93
00:12:28,440 --> 00:12:40,880
trouble in the sense that it's hard to, it's hard to let go of and there's a very strong

94
00:12:40,880 --> 00:12:51,000
misconception that somehow there's some value to physical beauty.

95
00:12:51,000 --> 00:13:00,360
I think you could argue that there is a mental beauty, but that's not exactly beautiful,

96
00:13:00,360 --> 00:13:09,440
but we could say that wisdom is beautiful and mindfulness is beautiful, love is beautiful.

97
00:13:09,440 --> 00:13:15,400
But to say that a flower is beautiful, there's a misperception.

98
00:13:15,400 --> 00:13:20,440
It's an extrapolation.

99
00:13:20,440 --> 00:13:31,880
Why is a, why is a flower beautiful and a pile of dog feces is not beautiful?

100
00:13:31,880 --> 00:13:42,760
Why is the human body beautiful, but a pig body is not beautiful in the same way, right?

101
00:13:42,760 --> 00:13:52,000
Everywhere it gets fairly perverse, a more intensely perverse is when it comes to sexuality,

102
00:13:52,000 --> 00:14:00,080
of course, and it's perverse, only in the Buddhist sense, not in a shameful, guilty sort

103
00:14:00,080 --> 00:14:01,720
of way.

104
00:14:01,720 --> 00:14:05,320
Why do we find the physical body attractive?

105
00:14:05,320 --> 00:14:12,600
Why does some of us find a certain physical body attractive, right, for the most part we

106
00:14:12,600 --> 00:14:24,280
can male humans find female humans physically and beautiful, attractive for the most part,

107
00:14:24,280 --> 00:14:41,040
female women, female humans find male humans physically attractive, why?

108
00:14:41,040 --> 00:14:57,120
What do we mean by mixed up is seeing something that isn't there and getting caught up

109
00:14:57,120 --> 00:15:03,960
in problematic state, right, when you see something as beautiful, it's not just like seeing

110
00:15:03,960 --> 00:15:12,920
something as tall, or it's not like saying, well, the Eiffel Tower is tall, that's quite

111
00:15:12,920 --> 00:15:18,720
innocuous, you could say, well, that's seeing something that isn't there, but there's

112
00:15:18,720 --> 00:15:25,880
a problem with seeing the Eiffel Tower is beautiful, for example, well, that's a little

113
00:15:25,880 --> 00:15:33,400
bit abstract, but seeing a human body is beautiful.

114
00:15:33,400 --> 00:15:36,800
It's fairly intensely perverse because it really has nothing to do with the physical

115
00:15:36,800 --> 00:15:47,800
body at all, the physical body is actually quite putrid, smelly, leaking all the time,

116
00:15:47,800 --> 00:15:53,040
there's nine holes in the body, but that's not the point, it's not the point of arguing

117
00:15:53,040 --> 00:16:00,200
whether the human body is ugly or beautiful, just pointing out the fact that it's just based

118
00:16:00,200 --> 00:16:07,920
on a very intense addiction, attachment that we build up lifetime after lifetime, there's

119
00:16:07,920 --> 00:16:13,800
no diamonds or rubies or gold, there's nothing that even we normally otherwise think of

120
00:16:13,800 --> 00:16:33,440
as beautiful, there's nothing objectively beautiful about the human body, full of fat and

121
00:16:33,440 --> 00:16:31,120
person blood and so on.

122
00:16:31,120 --> 00:16:39,800
And so we perceive the human body as being beautiful, take it as the best example because

123
00:16:39,800 --> 00:16:45,240
it's very clear, then we think of it, you know, many people hearing this are probably

124
00:16:45,240 --> 00:16:48,640
thinking to themselves, well, the human body is beautiful and they'll have all these thoughts

125
00:16:48,640 --> 00:16:50,640
about it.

126
00:16:50,640 --> 00:17:01,120
So those thoughts arise, that's messed up, I say it's messed up, of course there's a disagreement,

127
00:17:01,120 --> 00:17:05,360
but then it becomes views, so you might say, well, yeah, that's your view and your belief,

128
00:17:05,360 --> 00:17:14,680
and you say, but I believe, and when you say I believe, well, that's ditty, I believe

129
00:17:14,680 --> 00:17:21,360
the body is truly, the human body is truly beautiful, I believe it was created in God's

130
00:17:21,360 --> 00:17:34,800
image, I believe it is divine and it's my temple, many things people say about the body.

131
00:17:34,800 --> 00:17:42,200
So the teaching here is not to theorize, I'm theorizing with you and I don't want to mislead

132
00:17:42,200 --> 00:17:51,120
you there, I'm laying this out because this is meant to be practical advice for meditators,

133
00:17:51,120 --> 00:17:55,720
not for you to believe what I'm saying, but for you to be able to pinpoint what's going

134
00:17:55,720 --> 00:18:02,160
on in your mind.

135
00:18:02,160 --> 00:18:10,080
So the first one, beauty, how do you, how do you see the way in which you misunderstand

136
00:18:10,080 --> 00:18:18,720
the body, the beauty, mindfulness of the body, we have these four satipatana, the four

137
00:18:18,720 --> 00:18:22,960
foundations of mindfulness.

138
00:18:22,960 --> 00:18:27,520
They say in the commentaries that the Buddha taught in the four satipatana in order

139
00:18:27,520 --> 00:18:40,600
to counter these four perversions, these four misunderstandings.

140
00:18:40,600 --> 00:18:43,520
So one of the things you're going to see and one of the things that you should keep an

141
00:18:43,520 --> 00:18:56,920
eye out for is how your, your observation and clarity about the body changes the way you

142
00:18:56,920 --> 00:19:03,320
look at the physical reality, changes how you look at things like beauty and so on.

143
00:19:03,320 --> 00:19:09,960
There's instead of thinking that there's some value to, to beauty to whatever it is that

144
00:19:09,960 --> 00:19:16,240
you see as beautiful, to come to see that it's actually why it's, why beauty is in the

145
00:19:16,240 --> 00:19:22,960
eye of the beholder is because it's arbitrary and it's based much more on our addictions

146
00:19:22,960 --> 00:19:33,600
to brain chemicals than to anything intrinsic in the, in the object.

147
00:19:33,600 --> 00:19:38,360
When you focus on the body, you'll see really just how disgusting the body is and, and,

148
00:19:38,360 --> 00:19:42,480
and it's not even so important that you see it's discussing but you will and that kind

149
00:19:42,480 --> 00:19:47,960
of puts things in perspective where you say, in what sense is this beautiful, I mean

150
00:19:47,960 --> 00:19:51,720
in what sense is it disgusting really, it's neither nor it's just experience.

151
00:19:51,720 --> 00:19:57,200
But we see we're out of whack, we see certain things as beautiful and certain other things

152
00:19:57,200 --> 00:20:03,600
as disgusting and there's no rhyme or reason to it.

153
00:20:03,600 --> 00:20:07,320
We have no reason to think the body is objectively beautiful.

154
00:20:07,320 --> 00:20:13,000
I think you could make a fairly solid argument that pure gold is beautiful or a diamond

155
00:20:13,000 --> 00:20:14,000
is beautiful.

156
00:20:14,000 --> 00:20:16,040
I think there's a better argument there.

157
00:20:16,040 --> 00:20:19,760
I mean even still you could break it down and say that's ridiculous or something beautiful

158
00:20:19,760 --> 00:20:22,640
about a diamond.

159
00:20:22,640 --> 00:20:30,200
But even us, even ordinary individuals who haven't practiced meditation are, are just messed

160
00:20:30,200 --> 00:20:36,240
up in thinking that the body is somehow beautiful because at the same time we're, we're

161
00:20:36,240 --> 00:20:41,840
prestigious about cleaning the body, erasing any trace of the nature of the body which

162
00:20:41,840 --> 00:20:52,920
is smelly and ugly and sticky and gooey and sickly and aging, you know, how hard we work

163
00:20:52,920 --> 00:21:00,160
to get rid of wrinkles and stains on our teeth and the oils in our hair, the flakes of

164
00:21:00,160 --> 00:21:06,720
skin on our body, how we rub and scrape and all the beauty products we use to cover up the

165
00:21:06,720 --> 00:21:16,440
body and when you're mindful one of the things you'll see when it's a great relief really,

166
00:21:16,440 --> 00:21:35,560
the relief of being obsessed with the body, having to work so hard to possess the beauty

167
00:21:35,560 --> 00:21:44,200
of, not the beauty of a, of a romantic partner or even your own physical beauty when you

168
00:21:44,200 --> 00:21:54,800
look in the mirror or a beautiful sight like flowers or pictures or beautiful sounds even.

169
00:21:54,800 --> 00:22:04,800
So that's the first perversion or misperception for, what are we, what are we going to

170
00:22:04,800 --> 00:22:05,800
use?

171
00:22:05,800 --> 00:22:16,040
It's the first wepalasa, messed up and the second one, sukovipalasa, well, this is where we,

172
00:22:16,040 --> 00:22:24,880
we practice weight in a, when we practice weight in a nubasa in a mindfulness of feelings.

173
00:22:24,880 --> 00:22:38,800
Kovipalasa is the misperception of, or the misconception of things as happiness.

174
00:22:38,800 --> 00:22:40,520
Happiness is a challenging word.

175
00:22:40,520 --> 00:22:46,520
I think it's, beauty is challenging, I think maybe happiness is even more challenging.

176
00:22:46,520 --> 00:22:48,680
What does happiness mean?

177
00:22:48,680 --> 00:22:51,960
What do we referring to when we say happiness?

178
00:22:51,960 --> 00:23:01,560
I think it's more clear to people, to, to people who don't see it so clearly and might

179
00:23:01,560 --> 00:23:07,520
not be able to articulate what happiness is but have a good idea of it and they will

180
00:23:07,520 --> 00:23:17,720
all, many people will tell you they've experienced happiness from time to time.

181
00:23:17,720 --> 00:23:22,360
So you come to see through that in meditation, you know, see while that's actually a little

182
00:23:22,360 --> 00:23:29,760
bit inaccurate, that those experiences were not actually anything very special.

183
00:23:29,760 --> 00:23:37,400
They're fraught with addiction, attachment, craving, clinging, and stress even.

184
00:23:37,400 --> 00:23:45,160
You start to see there's a lot of stress tied up in our seeking out of pleasure.

185
00:23:45,160 --> 00:23:47,880
It's ultimately pleasure.

186
00:23:47,880 --> 00:24:04,120
Soka here, it's at least meant to point out the, the mistaking of pleasure for happiness.

187
00:24:04,120 --> 00:24:06,920
What do we mean when we say happiness?

188
00:24:06,920 --> 00:24:07,920
What do we mean?

189
00:24:07,920 --> 00:24:14,680
Do we mean these pleasant feelings that come and go?

190
00:24:14,680 --> 00:24:21,960
When you say to yourself, happy, happy, you're calm, calm.

191
00:24:21,960 --> 00:24:25,760
Just like when you focus on the body, you start to see clearly, it's just body, it's

192
00:24:25,760 --> 00:24:32,080
not beautiful or ugly or anything, really attachment or clinging, craving for the body.

193
00:24:32,080 --> 00:24:36,520
But the same when you say pain, pain or happy, happy, you're calm, calm, and you focus

194
00:24:36,520 --> 00:24:37,520
on the feelings.

195
00:24:37,520 --> 00:24:45,480
It'll lose any sense that one is better than the other, of course, that it takes time.

196
00:24:45,480 --> 00:24:50,120
But clearly, there's clearly a path there.

197
00:24:50,120 --> 00:25:00,560
You start to see how your mind differentiates less, less pushed and pulled.

198
00:25:00,560 --> 00:25:05,640
It's not that you become a zombie, not at all, it's not that you become, you know, like

199
00:25:05,640 --> 00:25:09,560
everything is just gray, not at all.

200
00:25:09,560 --> 00:25:12,400
Everything is colors, but they're just colors.

201
00:25:12,400 --> 00:25:16,560
They are what they are rather than pushing and pulling you.

202
00:25:16,560 --> 00:25:19,840
What, what, what good is there in disliking unpleasant feelings?

203
00:25:19,840 --> 00:25:21,920
Is there a good in it?

204
00:25:21,920 --> 00:25:23,640
Maybe someone would think there is.

205
00:25:23,640 --> 00:25:31,760
Yes, because then you can avoid them, which is really a topology, so it's circular reasoning.

206
00:25:31,760 --> 00:25:33,800
What's wrong with it?

207
00:25:33,800 --> 00:25:35,360
What's good about a pleasant feeling?

208
00:25:35,360 --> 00:25:45,360
It's really hard to give any cogenter, logical answer to that.

209
00:25:45,360 --> 00:25:55,000
What benefit is there to pleasure?

210
00:25:55,000 --> 00:25:59,720
A lot of our meditation practices is a relaxing.

211
00:25:59,720 --> 00:26:04,280
You know, we don't like to talk too much about relaxing because we don't need to cling

212
00:26:04,280 --> 00:26:10,280
to the feeling of being relaxed, but it really is what's going on.

213
00:26:10,280 --> 00:26:17,760
They're just not anymore, not pushing away the unpleasant feelings, not holding on, clinging

214
00:26:17,760 --> 00:26:25,920
to or wishing for pleasant feelings or neutral feelings, calm feelings.

215
00:26:25,920 --> 00:26:35,280
You just stop fussing, why?

216
00:26:35,280 --> 00:26:43,120
Because you stop mistaking, you lose these misconceptions that somehow, oh, that pleasure

217
00:26:43,120 --> 00:26:46,960
is going, that's great, that's a great thing.

218
00:26:46,960 --> 00:26:54,080
There's something good there, and so you stop clinging for it, clinging to it, you stop

219
00:26:54,080 --> 00:26:59,320
being disappointed and wishing for it when it's not there, and there's less stress when

220
00:26:59,320 --> 00:27:03,960
it's not there or when you have to experience pain.

221
00:27:03,960 --> 00:27:10,480
You relax, you know, you find true, what we might call true happiness and true peace.

222
00:27:10,480 --> 00:27:18,040
Again, they're just words, but you certainly feel more peaceful, more happy, why?

223
00:27:18,040 --> 00:27:26,320
Because you've stopped misunderstanding, you've lost that perversion of perception.

224
00:27:26,320 --> 00:27:31,920
So when you perceive a feeling as pleasant as you are, like this, liking this feeling,

225
00:27:31,920 --> 00:27:38,040
then when you think, I like that feeling, pleasant feelings are happiness.

226
00:27:38,040 --> 00:27:42,800
And if you believe that true happiness is in pleasure, you can see these are the three

227
00:27:42,800 --> 00:27:43,800
levels.

228
00:27:43,800 --> 00:27:52,000
So you start to lose that mindfulness of feeling helps with that.

229
00:27:52,000 --> 00:28:06,200
The third wepalasa is nitya wepalasa, perminence, nitya means perminence.

230
00:28:06,200 --> 00:28:11,800
And the third zatibatana, jitana vasa, the mindfulness of the mind, helps to see through

231
00:28:11,800 --> 00:28:25,160
this, and this nitya is here mainly related to the idea of stability, not only the things

232
00:28:25,160 --> 00:28:39,240
around us are continuous, but that our own self is continuous.

233
00:28:39,240 --> 00:28:46,720
The idea that there is stability and a lasting nature to things outside of us and inside

234
00:28:46,720 --> 00:28:52,120
of us.

235
00:28:52,120 --> 00:29:00,800
This is important because on a very deep level, it's a part of what makes us cause us as

236
00:29:00,800 --> 00:29:02,360
to cling to things.

237
00:29:02,360 --> 00:29:08,080
You can't cling to something that arises and sees us, but it's very easy to, it's much

238
00:29:08,080 --> 00:29:22,800
easier to cling to the conception of something like a possession or a body or the mind.

239
00:29:22,800 --> 00:29:27,320
When you focus on the mind, when you focus on your thoughts, you see that every experience

240
00:29:27,320 --> 00:29:30,280
is dependent on consciousness.

241
00:29:30,280 --> 00:29:37,600
When you watch systemic rising, the stomach doesn't actually exist, and we think of the stomach

242
00:29:37,600 --> 00:29:46,440
as always there, when you say there's a rising, falling, you see that the rising comes

243
00:29:46,440 --> 00:29:49,560
and goes.

244
00:29:49,560 --> 00:29:53,400
When you focus on the mind, like when you say there's self-stinking thinking, this is

245
00:29:53,400 --> 00:29:58,920
where you start to really appreciate what that means when you don't see, instead of seeing

246
00:29:58,920 --> 00:30:02,240
the stomach, seeing the rising and the falling.

247
00:30:02,240 --> 00:30:12,120
Because the mind is constantly changing, the other thing constant about it is the change.

248
00:30:12,120 --> 00:30:24,320
The idea of a soul of a self of some cohesion or continuity disappears when you see random

249
00:30:24,320 --> 00:30:28,520
thoughts coming out of nowhere and coming literally out of nowhere.

250
00:30:28,520 --> 00:30:36,920
Not out of a soul or a self or a me or an eye, but random thoughts and conflicting thoughts

251
00:30:36,920 --> 00:30:49,000
and arbitrary thoughts, focusing on the mind allows you, when you say there's a thinking

252
00:30:49,000 --> 00:31:00,200
and so on and distracted and so on, it changes your perception of the real nature of things

253
00:31:00,200 --> 00:31:05,960
as being stable and permanent.

254
00:31:05,960 --> 00:31:19,880
It also provides you with a new framework, a flexible, adaptable state of interaction.

255
00:31:19,880 --> 00:31:25,280
When we're not really accustomed to, we would much rather sink and sit down, as we

256
00:31:25,280 --> 00:31:26,280
get lazy.

257
00:31:26,280 --> 00:31:32,560
We want to just cling to something, if I just hold on to this long enough, maybe I'll

258
00:31:32,560 --> 00:31:38,240
be happy, while everything changes around you and you just end up sticking the mind, you

259
00:31:38,240 --> 00:31:42,600
get stuck.

260
00:31:42,600 --> 00:31:53,760
When you cultivate mindfulness, you become flexible, you keep up, you're able to keep up

261
00:31:53,760 --> 00:32:01,240
to be in tune with the changes in life, they don't take you by surprise.

262
00:32:01,240 --> 00:32:08,200
Life changes, you're right there with it, you're familiar with change, you're not taken

263
00:32:08,200 --> 00:32:11,800
off guard by change.

264
00:32:11,800 --> 00:32:20,240
Because of how you see the mind, this is why Chitanupasana is so caught up in this one.

265
00:32:20,240 --> 00:32:28,840
Because you start to see how reality is based on experience, it's based on the mind.

266
00:32:28,840 --> 00:32:39,560
The fourth Mipalasa is Ataripalasa, a perversion of self.

267
00:32:39,560 --> 00:32:49,200
This refers more to possession, having something being me or mine, mine is in a possession

268
00:32:49,200 --> 00:33:01,600
or me as identifying with, like when you feel pain and you identify that, I'm in pain.

269
00:33:01,600 --> 00:33:07,880
When you try to control something, this is mine, try to control your breathing, control

270
00:33:07,880 --> 00:33:11,680
your thoughts.

271
00:33:11,680 --> 00:33:22,240
Most glaringly, when we identify with our emotions, the fourth one, of course, relates

272
00:33:22,240 --> 00:33:29,000
to Dhamma Nipasana, the only one left, and you can see that clearly when you look at

273
00:33:29,000 --> 00:33:33,320
what's in the Dhamma Nipasana, the first is what we call the hindrances.

274
00:33:33,320 --> 00:33:41,000
They're a good example of this because it relates to our emotions, our reactions.

275
00:33:41,000 --> 00:33:47,480
When we talk about our likes and our dislikes, we conceive of them, we perceive them as

276
00:33:47,480 --> 00:33:52,040
self, we think of them as self, we believe them to be self.

277
00:33:52,040 --> 00:33:58,720
I, this is my personality, this is an arbitrary, it's who I am.

278
00:33:58,720 --> 00:34:06,920
I like apples or eye like oranges or I like cats or I like dogs.

279
00:34:06,920 --> 00:34:09,760
I'm this sort of person and that sort of person.

280
00:34:09,760 --> 00:34:10,760
I think a lot.

281
00:34:10,760 --> 00:34:19,120
I have an anger problem, I'm depressed, I'm anxious, I have phobia, it's nowhere more

282
00:34:19,120 --> 00:34:30,480
clearer, more clearer than in our emotions, in our likes and dislikes, our neuroses and

283
00:34:30,480 --> 00:34:42,280
all of our mental states.

284
00:34:42,280 --> 00:34:45,520
It's not even so much about, again, it's not about theory, it's not about whether they

285
00:34:45,520 --> 00:34:51,760
are or not, it's about how messed up is to think like that.

286
00:34:51,760 --> 00:34:55,920
You're adding something in, when you say it's me, it's mine, there's no place there

287
00:34:55,920 --> 00:35:00,920
for that.

288
00:35:00,920 --> 00:35:06,040
When anger arises, the best thing you can say about it is the most clear thing, the most

289
00:35:06,040 --> 00:35:13,360
useful thing you can say about it is that anger has arisen, it's anger, when you're afraid

290
00:35:13,360 --> 00:35:20,160
of something, there's nothing really better or nothing useful about saying anything more

291
00:35:20,160 --> 00:35:25,760
than this is fear.

292
00:35:25,760 --> 00:35:34,560
When you add to it, most especially, and if you add self, me, mine, it's just going

293
00:35:34,560 --> 00:35:44,320
to cause you, at the very least, stress and confusion and delusion, the worst it leads

294
00:35:44,320 --> 00:35:51,480
to, great suffering and evil, can lead you to, do evil things when people attack you,

295
00:35:51,480 --> 00:36:00,200
I don't deserve this, or I deserve this, and you find ways to manipulate others to get

296
00:36:00,200 --> 00:36:08,440
what you want based on your views and thoughts and perceptions.

297
00:36:08,440 --> 00:36:13,360
There's no room for conceit or arrogance and someone who doesn't believe in any of these

298
00:36:13,360 --> 00:36:17,320
things, who doesn't have any of these misperceptions, that's why they're misperceptions.

299
00:36:17,320 --> 00:36:20,320
That's why they're perverse, you could say.

300
00:36:20,320 --> 00:36:22,560
Why is it perverse to find something beautiful?

301
00:36:22,560 --> 00:36:27,200
Because it messes you up, it's not that God thinks it's bad, or I think it's bad,

302
00:36:27,200 --> 00:36:29,640
or people are going to point their finger at you.

303
00:36:29,640 --> 00:36:34,360
They might, they might point their finger at you for things that are innocent as well.

304
00:36:34,360 --> 00:36:41,040
That's not the point, the point is it messed up and it messes you up.

305
00:36:41,040 --> 00:36:45,240
Maybe in the opposite order, it's not even so important or so significant that it's messed

306
00:36:45,240 --> 00:36:46,240
up.

307
00:36:46,240 --> 00:36:54,400
If you believe that London is the capital of France, that's messed up, but it doesn't mess

308
00:36:54,400 --> 00:36:59,280
you up unless you take a flight to Paris.

309
00:36:59,280 --> 00:37:09,560
But what really messes you up, these are things that are perverse to believe in itself.

310
00:37:09,560 --> 00:37:14,480
It's a perverse to even think and even conceal something as me and mine.

311
00:37:14,480 --> 00:37:20,800
Why messes you up causes stress and suffering.

312
00:37:20,800 --> 00:37:21,800
And you don't have to believe that.

313
00:37:21,800 --> 00:37:26,800
Of course, it isn't about believing me or having some other view, but this is the sort

314
00:37:26,800 --> 00:37:28,960
of thing that you'll see when you practice mine from this.

315
00:37:28,960 --> 00:37:37,240
So this kind of teaching is really just to clarify and how it makes sense of what you're

316
00:37:37,240 --> 00:37:43,760
seeing, even through the meditation you've already done, you shouldn't be able to.

317
00:37:43,760 --> 00:37:48,760
I appreciate some of this, I mean it should be clear and I guess, at least some of this

318
00:37:48,760 --> 00:37:50,760
I can see.

319
00:37:50,760 --> 00:38:00,400
It's about helping you to direct your mind because you're going to see these things and

320
00:38:00,400 --> 00:38:06,000
it might lead to doubt, well, so something wrong with the meditation, why am I suddenly no

321
00:38:06,000 --> 00:38:12,560
longer finding things beautiful or why am I so no longer suddenly no longer looking for

322
00:38:12,560 --> 00:38:19,920
happiness or why can I no longer find myself, right, doubts that you have?

323
00:38:19,920 --> 00:38:20,920
Is there a self?

324
00:38:20,920 --> 00:38:21,920
Isn't there a self?

325
00:38:21,920 --> 00:38:28,040
It's going to start making you doubt these things, not because the doubting is good,

326
00:38:28,040 --> 00:38:37,080
but because the things you were sure of that are shaken, we were sure that the body

327
00:38:37,080 --> 00:38:43,000
was beautiful or so, I'm sure that there was a self maybe.

328
00:38:43,000 --> 00:38:48,960
Well, you're going to uproot anyway, because even people who don't have views of self

329
00:38:48,960 --> 00:38:55,440
still have thoughts of self conceits arrogance, attachment, this is me, this is mine, worries

330
00:38:55,440 --> 00:39:00,840
about their physical body and so on.

331
00:39:00,840 --> 00:39:09,360
Of course, in that money, there's much more than just the hindrances that also has the

332
00:39:09,360 --> 00:39:16,680
five aggregates, the six senses, and if we just take the six senses, they're very important.

333
00:39:16,680 --> 00:39:23,520
Noting the six senses helps you to see, this is the basis of reality, not this body,

334
00:39:23,520 --> 00:39:31,920
not me, I am not the basis of my reality, experiences, seeing is real, hearing is real, smelling

335
00:39:31,920 --> 00:39:34,280
is real.

336
00:39:34,280 --> 00:39:41,240
Kogito-Arigosum, the only way you can say there's an eye is because the existence comes

337
00:39:41,240 --> 00:39:53,400
from the experience, not the other way around.

338
00:39:53,400 --> 00:39:59,360
So some food for thought, a little bit of an outline in regards to the sort of things

339
00:39:59,360 --> 00:40:06,200
that you're going to start to see and start to question, pointing out that it's right

340
00:40:06,200 --> 00:40:15,840
to question, I'm still giving it to believe me, but we could take it as a kind of a challenge

341
00:40:15,840 --> 00:40:24,600
if I say to you, there's no beauty is not of any value, well, you investigate and you

342
00:40:24,600 --> 00:40:33,280
tell me whether I'm right or not, you can challenge that assumption, that claim, that

343
00:40:33,280 --> 00:40:39,840
happy, that pleasure is not real happiness, sort of to believe me.

344
00:40:39,840 --> 00:40:47,480
Watch, pay attention to your feelings, your happy feelings, painful feelings, but when

345
00:40:47,480 --> 00:40:59,040
you start to see it, don't ignore it, it's very important, changes something very valuable.

346
00:40:59,040 --> 00:41:04,480
When you start to see how chaotic the mind is, you feel like you have nothing to hold on

347
00:41:04,480 --> 00:41:11,720
to, there's like there's no bottom, don't ignore that, that's teaching you a new way

348
00:41:11,720 --> 00:41:18,440
to live, and you feel like nothing's under your control, there's no me, there's no

349
00:41:18,440 --> 00:41:27,200
line, there's nothing, don't even know who I am anymore, don't worry about that, don't

350
00:41:27,200 --> 00:41:33,520
feel like something's wrong, something was wrong, something was wrong that these were

351
00:41:33,520 --> 00:41:41,800
wrong, our views were wrong, our thoughts were wrong, our perception's wrong, not meaning

352
00:41:41,800 --> 00:41:47,560
we should feel guilty or feel like we're bad people, there's just wrong, they have no basis

353
00:41:47,560 --> 00:41:53,720
in fact, they were built on delusion, built on ignorance, that's what you'll see, you know,

354
00:41:53,720 --> 00:42:00,120
to believe me, and if I'm wrong, you can leave whenever you want, there's the power of truth,

355
00:42:00,120 --> 00:42:09,120
right, you don't have to convince anyone or tell them to believe blindly, you make a claim

356
00:42:09,120 --> 00:42:20,320
and if it's true, it's the most powerful thing in the world, unassailable, that's one

357
00:42:20,320 --> 00:42:26,840
translation of dhamma is truth or reality and that's what we teach, so we try to teach,

358
00:42:26,840 --> 00:42:33,840
claim the teach, hope to teach, so that's the dhamma for tonight, thank you for listening.

