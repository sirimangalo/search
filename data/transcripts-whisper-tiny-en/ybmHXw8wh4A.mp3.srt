1
00:00:00,000 --> 00:00:05,000
Is it a good thing to have a goal in meditation practice?

2
00:00:05,000 --> 00:00:08,000
For example, my goal is to enter the first chana.

3
00:00:12,000 --> 00:00:14,000
Yeah, sure.

4
00:00:14,000 --> 00:00:15,000
Why not?

5
00:00:15,000 --> 00:00:17,000
That's a goal.

6
00:00:17,000 --> 00:00:20,000
Goals are good.

7
00:00:20,000 --> 00:00:21,000
They can be good and bad.

8
00:00:21,000 --> 00:00:22,000
No, it depends.

9
00:00:22,000 --> 00:00:24,000
Are you just sitting in meditation saying,

10
00:00:24,000 --> 00:00:27,000
when's the first chana going to come?

11
00:00:27,000 --> 00:00:29,000
I don't like talking about the chana.

12
00:00:29,000 --> 00:00:32,000
It's because they're so, they're so tricky.

13
00:00:32,000 --> 00:00:34,000
And if you read what people say about them,

14
00:00:34,000 --> 00:00:36,000
there was this funny book,

15
00:00:36,000 --> 00:00:38,000
one of our meditators,

16
00:00:38,000 --> 00:00:40,000
who actually became a novice for a while.

17
00:00:40,000 --> 00:00:42,000
He brought a book about Samadhi.

18
00:00:42,000 --> 00:00:44,000
I think the book was called Samadhi.

19
00:00:44,000 --> 00:00:46,000
And he was saying I should read it.

20
00:00:46,000 --> 00:00:47,000
He said, you know, you should read it.

21
00:00:47,000 --> 00:00:49,000
And so I started looking through it and I said,

22
00:00:49,000 --> 00:00:51,000
but you've got 10 different people

23
00:00:51,000 --> 00:00:54,000
giving 10 different opinions of chana basically.

24
00:00:54,000 --> 00:00:56,000
It was a Buddhist book about Samadhi.

25
00:00:56,000 --> 00:00:59,000
And he said, yeah, isn't it funny?

26
00:00:59,000 --> 00:01:04,000
This one single book showed how ridiculous it was.

27
00:01:04,000 --> 00:01:07,000
How nobody had, they couldn't come to any consensus.

28
00:01:07,000 --> 00:01:10,000
There was no agreement among the 10 different people.

29
00:01:10,000 --> 00:01:13,000
I don't know how many people about what it means.

30
00:01:13,000 --> 00:01:16,000
And so you get people asking questions like,

31
00:01:16,000 --> 00:01:18,000
I got this in this state.

32
00:01:18,000 --> 00:01:19,000
Was that the first chana?

33
00:01:19,000 --> 00:01:22,000
Or I think I entered into the first chana or so.

34
00:01:22,000 --> 00:01:25,000
And you really get a sense of attachment

35
00:01:25,000 --> 00:01:27,000
that they're really attached to this idea that,

36
00:01:27,000 --> 00:01:28,000
did I get it?

37
00:01:28,000 --> 00:01:30,000
I got it and so on.

38
00:01:30,000 --> 00:01:36,000
And to me, that shows the danger potential danger of,

39
00:01:36,000 --> 00:01:39,000
no, I mean, there's nothing dangerous about the states.

40
00:01:39,000 --> 00:01:44,000
But the danger of chana or what is it?

41
00:01:44,000 --> 00:01:46,000
Giving them an entity,

42
00:01:46,000 --> 00:01:49,000
you know, saying the first chana and the second chana.

43
00:01:49,000 --> 00:01:50,000
They're just stages.

44
00:01:50,000 --> 00:01:52,000
I mean, it's the same meditation.

45
00:01:52,000 --> 00:01:55,000
It's just they get deeper and deeper and deeper.

46
00:01:55,000 --> 00:01:57,000
Like seasons.

47
00:01:57,000 --> 00:01:58,000
Well, kind.

48
00:01:58,000 --> 00:02:02,000
I mean, like stages of stages of fruit or stages of milk.

49
00:02:02,000 --> 00:02:05,000
Like first it's milk and then it's a ghee and then it's butter

50
00:02:05,000 --> 00:02:10,000
and then it's cheese.

51
00:02:10,000 --> 00:02:15,000
But the thing, the mark between the two is arbitrary,

52
00:02:15,000 --> 00:02:16,000
are you saying?

53
00:02:16,000 --> 00:02:17,000
It's not arbitrary.

54
00:02:17,000 --> 00:02:18,000
It's not arbitrary.

55
00:02:18,000 --> 00:02:21,000
But I don't like to put labels on things.

56
00:02:21,000 --> 00:02:22,000
And you shouldn't.

57
00:02:22,000 --> 00:02:24,000
If you look, read this book, you say,

58
00:02:24,000 --> 00:02:27,000
oh, you know, jana can be many different things.

59
00:02:27,000 --> 00:02:31,000
The point is, do you have the hindrances or not?

60
00:02:31,000 --> 00:02:35,000
And if you want to go, the goal should be to get rid of the hindrances.

61
00:02:35,000 --> 00:02:37,000
I wouldn't reach so low as the first jana.

62
00:02:37,000 --> 00:02:40,000
I would, you know, reach to the end, get rid of the

63
00:02:40,000 --> 00:02:42,000
defilements in the mind or at least, you know,

64
00:02:42,000 --> 00:02:46,000
look at it as getting rid of the hindrances.

65
00:02:46,000 --> 00:02:49,000
I mean, put it in practical terms rather than say,

66
00:02:49,000 --> 00:02:50,000
attain this.

67
00:02:50,000 --> 00:02:51,000
And it's not just jana.

68
00:02:51,000 --> 00:02:53,000
It goes with soda pun as well.

69
00:02:53,000 --> 00:02:56,000
You hear a lot of people ask me this question about becoming soda pun.

70
00:02:56,000 --> 00:02:59,000
Should I make a goal to attain soda pun?

71
00:02:59,000 --> 00:03:01,000
You know, don't add jana chas.

72
00:03:01,000 --> 00:03:03,000
Don't become anything.

73
00:03:03,000 --> 00:03:08,000
I mean, he said, you know, what do you want to be something?

74
00:03:08,000 --> 00:03:10,000
Why do you want to take something on?

75
00:03:10,000 --> 00:03:13,000
Buddhism is not about adding to yourself.

76
00:03:13,000 --> 00:03:16,000
It's about subtracting from the self.

77
00:03:16,000 --> 00:03:21,000
So, you know, look at it as becoming a better person, if you will,

78
00:03:21,000 --> 00:03:26,000
or look at it as removing things like the Buddha said,

79
00:03:26,000 --> 00:03:27,000
what are we aiming for?

80
00:03:27,000 --> 00:03:28,000
It's not happiness.

81
00:03:28,000 --> 00:03:30,000
It's the freedom from suffering.

82
00:03:30,000 --> 00:03:31,000
Why is that?

83
00:03:31,000 --> 00:03:32,000
Because you don't want to add something.

84
00:03:32,000 --> 00:03:35,000
We're not looking for a positive state of happiness.

85
00:03:35,000 --> 00:03:38,000
We're looking to be free to give up, to let go,

86
00:03:38,000 --> 00:03:41,000
so that we can accept anything, so that we can,

87
00:03:41,000 --> 00:03:44,000
so that we don't need anything.

88
00:03:44,000 --> 00:03:48,000
So, but if you have certain goals,

89
00:03:48,000 --> 00:03:52,000
I would say they can potentially be beneficial in terms of,

90
00:03:52,000 --> 00:03:57,000
you know, getting your butt out of bed and onto the meditation mat.

91
00:03:57,000 --> 00:04:02,000
And if it's just that, if it's just something that pushes you on,

92
00:04:02,000 --> 00:04:04,000
that can be useful.

93
00:04:04,000 --> 00:04:06,000
I think.

94
00:04:06,000 --> 00:04:09,000
Well, you know, you're saying a very important point,

95
00:04:09,000 --> 00:04:14,000
which is the distinction between that meditation is not

96
00:04:14,000 --> 00:04:17,000
the object of your meditation.

97
00:04:17,000 --> 00:04:19,000
Your mind is really a meditation,

98
00:04:19,000 --> 00:04:23,000
whether you're focusing on an object or not,

99
00:04:23,000 --> 00:04:26,000
whether you're concentrating on your breath,

100
00:04:26,000 --> 00:04:29,000
whether you're concentrating on your walking,

101
00:04:29,000 --> 00:04:34,000
really your meditation is being very consciously aware of,

102
00:04:34,000 --> 00:04:39,000
as Gotama said, of sitting, standing, eating, sleeping,

103
00:04:39,000 --> 00:04:45,000
you know, lying down, that is really your meditation.

104
00:04:45,000 --> 00:04:50,000
And where is the mind and where is the emotion?

105
00:04:50,000 --> 00:04:53,000
You know, are you scattered throughout the day?

106
00:04:53,000 --> 00:04:56,000
Well, that is really your meditation.

107
00:04:56,000 --> 00:05:02,000
You know, maybe when you try to sit down and focus on an object of concentration,

108
00:05:02,000 --> 00:05:09,000
you know, we can talk about, oh, I've attained this level of samadhi or jhana,

109
00:05:09,000 --> 00:05:18,000
but really you're probably, if you're not recognizing that it has to be a continuity

110
00:05:18,000 --> 00:05:23,000
of that mind throughout all your active conscious activities,

111
00:05:23,000 --> 00:05:29,000
and some meditators will go so far as to say even during sleep.

112
00:05:29,000 --> 00:05:35,000
So, you know, talking about achieving levels is,

113
00:05:35,000 --> 00:05:39,000
it's still part of, I must admit, we were discussing all of this.

114
00:05:39,000 --> 00:05:40,000
You know?

115
00:05:40,000 --> 00:05:43,000
Okay, for sure me.

116
00:05:43,000 --> 00:05:47,000
Okay, I got another couple of really interesting questions.

117
00:05:47,000 --> 00:06:03,000
I'm going to quit this.

