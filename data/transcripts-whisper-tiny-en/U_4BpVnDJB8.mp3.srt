1
00:00:00,000 --> 00:00:06,280
If someone doesn't have a teacher and can't get anyone to help in a more

2
00:00:06,280 --> 00:00:10,800
direct manner, which that person do, assume there's no monastery or center

3
00:00:10,800 --> 00:00:16,040
available, get this question a lot, which is why I'm doing this tour down the

4
00:00:16,040 --> 00:00:21,480
East Coast because I'm thinking, well here's my answer, I'll come see you, don't

5
00:00:21,480 --> 00:00:27,360
need to sit in the courtyard and wait, I'll come to you, sort of, we're kind of

6
00:00:27,360 --> 00:00:38,200
doing. I'm going to put a video out soon about the final tour. The can't get

7
00:00:38,200 --> 00:00:41,640
anyone to help in a more direct manner. I mean, it's not about getting someone to

8
00:00:41,640 --> 00:00:46,040
help us about going to them as I said earlier. Really, you have to find a way

9
00:00:46,040 --> 00:00:53,000
to go to the teacher. That's the thing to do. At least to visit best if you

10
00:00:53,000 --> 00:01:01,840
can stay for some time, find someone South Carolina. Yeah, I'm going through

11
00:01:01,840 --> 00:01:10,520
South Carolina, right? Maybe we're going to meet. So that's something. But anywhere in

12
00:01:10,520 --> 00:01:15,200
the US, you can find things, but you have to travel from state to state. You

13
00:01:15,200 --> 00:01:18,760
have to probably go to another state. You have to work for it. I mean, that's

14
00:01:18,760 --> 00:01:22,280
really the problem with YouTube is it's too easy for people and so they they

15
00:01:22,280 --> 00:01:26,200
want it all now. There's a potential to just want things to fall into your

16
00:01:26,200 --> 00:01:31,040
lab, which they don't. You have to work for them. So again, think back to

17
00:01:31,040 --> 00:01:34,480
sitting in the courtyard. This guy had traveled all the way to China and then

18
00:01:34,480 --> 00:01:38,520
had to sit in a courtyard. He got there. They wouldn't let him in. So rather than

19
00:01:38,520 --> 00:01:42,080
turn around and go home, he had such determination that he sat in this

20
00:01:42,080 --> 00:01:48,600
courtyard in the rain for two days. And they finally let him in. That kind of

21
00:01:48,600 --> 00:01:53,880
thing. You need perseverance. That's what I would recommend. Apart from that,

22
00:01:53,880 --> 00:01:57,760
you know, the standard answer is to look on the internet. There's lots of stuff.

23
00:01:57,760 --> 00:02:03,480
There are lots of resources in personal resources. But none of it compares to

24
00:02:03,480 --> 00:02:09,600
actually living and studying under a qualified teacher. So that's what I

25
00:02:09,600 --> 00:02:19,600
would know. Recommend. Get it.

