1
00:00:00,000 --> 00:00:09,720
What is the point or goal of meditation? Happiness, the point of everything is

2
00:00:09,720 --> 00:00:14,400
happiness. You don't do something unless you think it's going to make you

3
00:00:14,400 --> 00:00:22,200
happy. So we practice meditation to become happy. We should never give up

4
00:00:22,200 --> 00:00:30,800
happiness. We should never avoid happiness. We should never run away from it. We

5
00:00:30,800 --> 00:00:40,800
should never practice meditation thinking that it's good to suffer as one. We

6
00:00:40,800 --> 00:00:44,600
should suffer in meditation knowing that the suffering is not because of the

7
00:00:44,600 --> 00:00:58,880
meditation, but because of our our mind states and our character qualities that

8
00:00:58,880 --> 00:01:05,960
interrupt the meditation practice. Interrupt our clear awareness of things. And you

9
00:01:05,960 --> 00:01:10,360
should not cultivate states of pain in meditation. You should not cultivate

10
00:01:10,360 --> 00:01:17,320
states of stress and meditation. Meditation shouldn't be something that you build

11
00:01:17,320 --> 00:01:22,240
up, build up, build up a state of concentration. You shouldn't force anything in

12
00:01:22,240 --> 00:01:28,000
the meditation trying to become enlightened, trying to be happy, trying to find

13
00:01:28,000 --> 00:01:33,680
peace. You shouldn't build up this stress. Meditation should be that

14
00:01:33,680 --> 00:01:39,760
which leads you to peace and thereby to true happiness.

