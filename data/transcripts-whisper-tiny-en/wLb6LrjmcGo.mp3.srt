1
00:00:00,000 --> 00:00:05,400
Hello and welcome back to our study of the Dhamupanda. Today we continue on with

2
00:00:05,400 --> 00:00:15,400
the first 136 which brings us follows. A tapapani kamani karang bhalo nubudjhati.

3
00:00:15,400 --> 00:00:30,960
Say he kamai, he dummedo, agidadowa, tampati which means when an evil did, when one

4
00:00:30,960 --> 00:00:44,960
commits an evil did, the fool does not realize it, nabudjhati. Say he kamai, he dummedo,

5
00:00:44,960 --> 00:00:56,960
and buy those evil deeds, the fool, the unwise person is burnt up the tea, agidadowa

6
00:00:56,960 --> 00:01:07,120
is still by a fire. Say if burnt by a fire. So the back story of this one is quite

7
00:01:07,120 --> 00:01:20,400
interesting actually, so hard to believe for the skeptics. But it's a ghost story and we

8
00:01:20,400 --> 00:01:25,680
have many ghost stories in the Buddhist teachings, but ghost stories in Buddhism are not

9
00:01:25,680 --> 00:01:34,440
about how fearsome the ghost is, and more about how fearsome it is to be a ghost.

10
00:01:34,440 --> 00:01:40,920
So the story goes that Mogulana when he was going down from Gijaku to a vultures peak

11
00:01:40,920 --> 00:01:48,040
in Rajagata, which you can still go see if you visit. He was going with his attendant

12
00:01:48,040 --> 00:01:59,800
or his assistant Luckana, and suddenly he smiled, and Luckana has to marry a smiley, and

13
00:01:59,800 --> 00:02:08,240
he said, better wait until we get to see the Buddha before I tell you. So curious, but

14
00:02:08,240 --> 00:02:14,720
respectful, Luckana waited, and when they had gone for arms round and eaten I guess and

15
00:02:14,720 --> 00:02:25,160
went to see the Buddha, Luckana asked Mogulana again, why were you smiling? And Mogulana

16
00:02:25,160 --> 00:02:34,280
explained to him in front of the Buddha. He said, well we were walking I saw a ghost in

17
00:02:34,280 --> 00:02:40,800
the form of a boa constrictor with flames proceeding from his head, going from its head

18
00:02:40,800 --> 00:02:48,200
to its tail and back from its tail to its head and flames on either side, completely engulfed

19
00:02:48,200 --> 00:02:56,720
by flames. I said, but I didn't say anything because I didn't think you might believe

20
00:02:56,720 --> 00:03:03,520
not believe me. Even Mogulana was wary of skeptics, but the Buddha reaffirmed it and said

21
00:03:03,520 --> 00:03:07,560
I've seen that ghost before, but I also didn't want to say anything because I wanted

22
00:03:07,560 --> 00:03:21,040
to wait until I had a witness as well, otherwise people might be skeptical. So this is

23
00:03:21,040 --> 00:03:28,000
the ghost, and why was this ghost in the form? Why was this ghost being burnt? And the

24
00:03:28,000 --> 00:03:35,720
point here is that it was in pain, wailing and being tortured by this flames actually burning

25
00:03:35,720 --> 00:03:47,600
up. This wasn't a comfortable state. This wasn't a fearsome ghost, this was a pitiful state.

26
00:03:47,600 --> 00:03:52,920
So they had the monks asked the Buddha, what did someone have possibly done to be born

27
00:03:52,920 --> 00:04:02,600
in such a state? And so here is the reply. In the past there was a rich man named Sumangala

28
00:04:02,600 --> 00:04:08,840
in the time of the Buddha Kastapa, and he was much like the great lay disciples of the

29
00:04:08,840 --> 00:04:14,760
Buddha's time, building monasteries. In the Buddha's time we have this man, Anatapindika,

30
00:04:14,760 --> 00:04:25,880
who built Jetamana, the monastery where the Buddha spent most of his career. And he

31
00:04:25,880 --> 00:04:31,040
did it by, in order to buy it because Jetamana wasn't going to sell Jetamana. He had to

32
00:04:31,040 --> 00:04:38,560
cover the space with gold coins. And that's what Jetamana said, figuring that Anatapindika

33
00:04:38,560 --> 00:04:42,480
would never do such a thing, would never pay such an exorbitant amount, but Anatapindika

34
00:04:42,480 --> 00:04:51,360
was determined and so he paid the price. So in this time, Sumangala is sent to have done

35
00:04:51,360 --> 00:04:58,720
the same sort of thing and spent at least as much money, building an actual monastery.

36
00:04:58,720 --> 00:05:06,640
And then spent an equal amount of money, a third portion of money, giving a festival,

37
00:05:06,640 --> 00:05:13,840
giving celebration and ceremonies and feeding the monks and feeding people and having this

38
00:05:13,840 --> 00:05:20,800
celebrating this event of opening the monastery. So it was a great man, a great Buddhist,

39
00:05:20,800 --> 00:05:27,880
a great supporter of the Buddha Kastapa. And one day when he was on his way to see the

40
00:05:27,880 --> 00:05:34,600
Buddha, he noticed in a rest house by the side of the road, this man with his feet all

41
00:05:34,600 --> 00:05:43,400
spattered with mud and his robe, his clothes drawn up over his head. And he said, he said,

42
00:05:43,400 --> 00:05:49,560
oh, this man with feet all spattered with mud must be some kind of night prowler,

43
00:05:49,560 --> 00:05:58,520
probably a robber kind of thing in hiding, because it looked like he didn't want to be seen. And the

44
00:05:58,520 --> 00:06:06,280
thief heard this and he conceived of a grudge towards the treasure. And you've got to wonder whether

45
00:06:06,280 --> 00:06:12,520
there might be something harm in going on here, because you would think that such a person

46
00:06:12,520 --> 00:06:19,480
would have a thick enough skin not to be. Well, it's just that it's funny what triggers us,

47
00:06:19,480 --> 00:06:25,000
because this really triggered him. And I made wonder whether there was some karma involved,

48
00:06:25,000 --> 00:06:30,680
where he was just waiting to get back. And as soon as he heard these harsh words of the treasure,

49
00:06:30,680 --> 00:06:35,320
which weren't really harshly intended, it was more speaking out loud to himself, not realizing

50
00:06:35,320 --> 00:06:41,160
that the thief was going to hear it. But he conceived a grudge, but very angry and he said,

51
00:06:41,160 --> 00:06:49,480
I know what to do to you. And he proceeded to do an incredible amount of evil. He burnt the treasure,

52
00:06:49,480 --> 00:06:54,840
the rich men's fields seven times, cut off the feet of his cattle seven times,

53
00:06:56,440 --> 00:07:04,280
which is in and of itself an inconceivable sort of cruelty, and burnt his house seven times.

54
00:07:05,560 --> 00:07:10,600
So he did nasty nasty things again and again. But in spite of this,

55
00:07:10,600 --> 00:07:15,160
he was unable to satisfy his grudge towards the treasure. He was still angry, which

56
00:07:15,160 --> 00:07:23,640
you know, kind of makes you, in and of itself, is a lesson that appeasing your grudge doesn't

57
00:07:23,640 --> 00:07:29,000
actually as soon as it doesn't actually make it feel better. It just makes you more and more

58
00:07:29,000 --> 00:07:35,880
inclined towards cruelty and anger, makes you a more and more coarse individual. And he said,

59
00:07:35,880 --> 00:07:42,920
what am I going to do? Why kind of do something? Find something that will really make him upset

60
00:07:42,920 --> 00:07:49,960
because he's not even upset. The treasure or the rich man was still able to live his life and

61
00:07:49,960 --> 00:07:57,080
rebuild his house and be patient because of course he was a Buddhist and probably a meditator

62
00:07:57,080 --> 00:08:03,560
and able to be at peace no matter what happened, which was also a very good example for us.

63
00:08:03,560 --> 00:08:09,400
And then he realized that he realized that I know what I can do. And because he saw

64
00:08:09,400 --> 00:08:12,680
that he was going to see the Buddha all the time, he thought, what if I destroyed the Buddha's

65
00:08:12,680 --> 00:08:17,800
house? And so he went to the Buddha's hut when the Buddha was away and burnt it to the ground

66
00:08:18,760 --> 00:08:27,800
and everything, smashed everything up and sent it on fire. And everyone was,

67
00:08:27,800 --> 00:08:34,280
everyone, when the people saw this, they ran around yelling, oh, the Buddha's hut is on fire.

68
00:08:35,320 --> 00:08:42,680
And the rich man, when he heard it, he ran immediately or not ran, but he immediately went to

69
00:08:42,680 --> 00:08:52,920
where the Buddha's hut was and he was quiet. And he looked at the ashes. He, when he got there,

70
00:08:52,920 --> 00:08:58,520
it was too late and it was burnt to the ground and it was just ashes remaining. And he was quiet

71
00:08:58,520 --> 00:09:08,760
for a second. And then it says he did something that I don't quite understand, but it's something

72
00:09:08,760 --> 00:09:15,480
like this, I don't know if there was some, something that they did was equivalent to clapping

73
00:09:15,480 --> 00:09:20,120
and clapping their hands. He clapped his hands basically, but the way it's described here is a little

74
00:09:20,120 --> 00:09:29,400
bit odd. But he clapped his hands together and smiled, enjoyed. And you know why?

75
00:09:30,840 --> 00:09:36,280
They asked him, why are you, you're the one who paid all the money to build this, why are you

76
00:09:36,280 --> 00:09:47,480
so joyful at seeing it burnt to the ground? And he said, I was able to, I had the incredible

77
00:09:47,480 --> 00:09:54,840
opportunity to build this once. Now I've got this incredible rare opportunity to do the same thing

78
00:09:54,840 --> 00:10:01,400
all over again, to make the same amount of merit. And so I'm overjoyed by this opportunity.

79
00:10:03,880 --> 00:10:09,720
This is sort of polyanna levels of goodness or

80
00:10:09,720 --> 00:10:20,840
well, good intentions, positivity anyway. So again, he spent the same amount of money rebuilding

81
00:10:20,840 --> 00:10:29,240
the Buddha's good day and presented it to the Buddha. But he saw this and found out that he had

82
00:10:29,240 --> 00:10:36,520
actually made the rich man happier. He said, you know, I guess there's nothing I can do to him

83
00:10:36,520 --> 00:10:45,640
except the one final thing that I can do and that's kill him. And so he determined that that's

84
00:10:45,640 --> 00:10:57,320
what he would do. And he decided he strapped a knife to his belt and followed the treasure

85
00:10:57,320 --> 00:11:00,280
wherever he went. But he didn't get an opportunity because the

86
00:11:00,280 --> 00:11:09,000
treasure was, the rich man was busy with the ceremonies of giving this new cutty. And so he

87
00:11:09,000 --> 00:11:14,920
was surrounded by monks and lay people. So for seven days, he didn't get an opportunity.

88
00:11:18,120 --> 00:11:24,440
Until the seventh day finally, he was watching from the sort of the crowd and he was listening

89
00:11:24,440 --> 00:11:32,520
and he heard the rich man tell the Buddha, venerable serve seven times, a certain man,

90
00:11:32,520 --> 00:11:38,440
there's a certain man out there. I don't know who he is. But he burnt my fields, cut the

91
00:11:38,440 --> 00:11:46,200
feet off my cows and burnt down my house seven times. He must have probably been the same person

92
00:11:46,200 --> 00:11:54,360
who burnt down the Buddha's good day. So in response to this, I would please ask that I

93
00:11:54,360 --> 00:12:00,760
to dedicate the merit, the first fruit, the first portion of this offering, the first portion of

94
00:12:00,760 --> 00:12:04,920
the merit, the goodness of doing this deed, I would like to dedicate it to him.

95
00:12:12,120 --> 00:12:18,760
And this finally, finally touched this wretched man who had done these awful deeds.

96
00:12:19,560 --> 00:12:23,240
And he came up and he prostrated himself before the rich man. So what's the meaning of this?

97
00:12:23,240 --> 00:12:26,680
And he said, I'm the one who did this. I'm the one who did those things to you.

98
00:12:27,800 --> 00:12:33,000
And he said, please, why did you do that? I've never seen you before. He said, no, you saw me.

99
00:12:33,720 --> 00:12:40,280
I was that man that you said I was a thief. He said, oh, yes, I did say that. Please forgive me.

100
00:12:42,200 --> 00:12:52,280
We asked forgiveness from the guy and he said, please forgive me. He said, please forgive me.

101
00:12:52,280 --> 00:12:57,160
He said, yes, I forgive you. Go your way. And he said, please let me be your slave.

102
00:12:57,160 --> 00:13:04,600
Let me do something to a tone for it. And he said, look, it was because of what I said

103
00:13:04,600 --> 00:13:10,440
that you did all this. But honestly, I don't see how we could possibly be friends after all the

104
00:13:10,440 --> 00:13:14,440
bad things you've done it. It really just wouldn't be possible. So I pardon you,

105
00:13:14,440 --> 00:13:22,920
friendly. Please go your way, friend. Totally unmoved and angry, but also wise, you know,

106
00:13:22,920 --> 00:13:29,160
circumspect. Not, you know, you normally think of people who do good deeds that they're somehow

107
00:13:29,160 --> 00:13:36,920
gullible and naive. That's the sort of feeling that we get. We think that anyone who is worldly

108
00:13:37,720 --> 00:13:42,680
who is wise and knowledgeable would never be so good because they would know that people would

109
00:13:42,680 --> 00:13:48,520
take advantage, et cetera. But here's an example, or maybe you would argue, or it would be skeptical

110
00:13:48,520 --> 00:13:54,120
and things such a person couldn't possibly exist. But here's an example of someone who is incredibly

111
00:13:55,320 --> 00:14:03,560
beneficent, is incredibly noble and kind and charitable. But at the same time, wise and

112
00:14:03,560 --> 00:14:11,080
circumspect. So he's perfectly willing to freely let this guy go on his way and without any grudge

113
00:14:11,080 --> 00:14:14,680
for all the terrible things that he did. But at the same time, he wasn't one step nothing to do with

114
00:14:14,680 --> 00:14:21,080
him. He realizes that he's clear that it's not that I'm, it's called forgiving, but not forgetting,

115
00:14:21,080 --> 00:14:27,320
which is, I think, an important Buddhist practice. It's a good example for us. This is a

116
00:14:27,320 --> 00:14:34,520
question. It's a nice story, I think. So that's the story. And you know, you'd think, well,

117
00:14:34,520 --> 00:14:41,400
it's good that he, he has forgiveness, but unfortunately, you know, asking forgiveness for the bad

118
00:14:41,400 --> 00:14:50,280
things he'd done isn't enough really. I mean, there is no way to prevent the, I mean, there's

119
00:14:50,280 --> 00:14:55,640
no way to prevent the power of unwholesome deeds. The only thing you can do is to mitigate them

120
00:14:56,360 --> 00:15:01,640
and offset them by good deeds. So if he had then dedicated his life to doing good deeds,

121
00:15:01,640 --> 00:15:08,040
it's a potential that he could have somehow found his way back into wholesome state of mind,

122
00:15:08,040 --> 00:15:16,280
but it seems that he didn't. And as a result, he was born in hell for many eons and

123
00:15:17,320 --> 00:15:23,000
a long time. And after being born in hell for a long period, he was, because it wasn't yet

124
00:15:23,000 --> 00:15:30,920
exhausted, he was born as a ghost, being tortured by fire on Vulture's Peak. Vulture's Peak

125
00:15:30,920 --> 00:15:35,000
apparently has lots of ghosts. It's the kind of place, if you ever go there, you get a feeling

126
00:15:35,000 --> 00:15:41,560
that it was the sort of place that would have lots of tortured spirits. Or maybe it was

127
00:15:41,560 --> 00:15:46,520
because the Buddha lived at the tortured spirits would come to visit, hoping they'd make some form

128
00:15:46,520 --> 00:15:52,440
of merit and goodness. And so then the Buddha taught this verse.

129
00:15:52,440 --> 00:16:02,120
So what this has to do with us? Well, I think it's neat to see the contrast between

130
00:16:02,840 --> 00:16:10,440
Mogulana and the ghost, first of all, and then the rich man and the thief. So the contrast

131
00:16:10,440 --> 00:16:19,560
between Mogulana, what does he do? What does his first response when he sees this ghost? It's

132
00:16:19,560 --> 00:16:26,040
not to feel horrified or sad, or even to have some sort of compassionate response towards

133
00:16:26,040 --> 00:16:32,840
this ghost. He smiles, because he thinks to himself, isn't it wonderful that I'm free from

134
00:16:32,840 --> 00:16:40,840
all of this? Which seems somewhat like a cruel sort of thing to do, but it's only because we

135
00:16:40,840 --> 00:16:50,440
have this idea, or this, we confuse compassion with, I guess, empathy, an empathy meaning in the

136
00:16:50,440 --> 00:16:55,640
sense of feeling the feelings of others, which is not really a beneficial thing if someone is suffering

137
00:16:55,640 --> 00:17:01,720
and then you suffer because of that. It doesn't really do anyone any good. It's not what anyone

138
00:17:01,720 --> 00:17:06,680
would want for you. It doesn't make the other person suffering any less. It doesn't make you any

139
00:17:06,680 --> 00:17:13,080
better equipped to help the person. I mean, the fact is, Mogulana was an incredibly compassionate

140
00:17:13,080 --> 00:17:20,360
person, constantly striving for the benefit of himself and others, helping the monks, helping

141
00:17:20,360 --> 00:17:25,080
people even helping animals. He had a credible amount of compassion, but he wasn't moved to

142
00:17:25,080 --> 00:17:31,800
tears or even sadness by any of this. I mean, he hadn't seen so much that he had this curious response

143
00:17:31,800 --> 00:17:38,040
that is common, apparently, to the Arans, to smile and to out of peace, out of a sense of peace

144
00:17:38,040 --> 00:17:43,480
and happiness for what they have accomplished, because Arans are truly happy and truly at peace.

145
00:17:46,600 --> 00:17:55,480
And the other contrast is with the thief and the rich man. I mean, it's just a really good

146
00:17:55,480 --> 00:18:03,640
example for us to be beneficent, to be good, even in the face of evil and to not be moved by

147
00:18:03,640 --> 00:18:13,720
suffering. The things, how quick the thief was to hold a grudge and how impossible it was to

148
00:18:15,160 --> 00:18:19,640
create the same feelings of animosity in the rich man. No matter what happened to,

149
00:18:19,640 --> 00:18:30,360
he was made to undergo great suffering, great loss anyway, without suffering, without reacting,

150
00:18:30,360 --> 00:18:31,480
without getting upset about it.

151
00:18:39,240 --> 00:18:46,520
And then focusing on the thief himself, we have to all see that this potential danger for us

152
00:18:46,520 --> 00:18:54,680
in holding grudges and in the things that the grudges make us do. Even if we don't act on them,

153
00:18:54,680 --> 00:18:59,640
it will influence our anger, it will influence our speech, it will influence our thoughts,

154
00:19:00,360 --> 00:19:06,280
it will prevent us from cultivating meditation, becoming concentrated, it will certainly prevent

155
00:19:06,280 --> 00:19:12,920
us from seeing clearly and understanding the truth. It's very important that we cultivate things

156
00:19:12,920 --> 00:19:20,360
like loving kindness and moreover that we cultivate an understanding of the anger and to see the

157
00:19:20,360 --> 00:19:26,360
anger as just an experience, not me, not mine, not something I should cling to. Someone

158
00:19:26,360 --> 00:19:33,000
says something nasty to you, it's not you that they're attacking. This you doesn't even exist,

159
00:19:33,000 --> 00:19:39,000
it's just words, it's just sounds. The you that they are attacking is not real.

160
00:19:39,000 --> 00:19:47,960
And to see that it's just an experience that's coming gone, the only thing that remains

161
00:19:48,680 --> 00:19:54,360
is your reaction and you're clinging to it. If you can see that and you can see the clinging,

162
00:19:54,360 --> 00:20:00,040
it's clinging, you can see the anger as anger, you can see the suffering as suffering, then you

163
00:20:00,040 --> 00:20:09,800
can let it go and be free. Don't be consumed by your wicked deeds. If you are, you'll be like the fool

164
00:20:09,800 --> 00:20:17,400
who is burnt as though by a fire, again don't know what that would be. So that's the

165
00:20:17,400 --> 00:20:33,320
number part of teaching for today, thank you for tuning in and seeing all the best.

