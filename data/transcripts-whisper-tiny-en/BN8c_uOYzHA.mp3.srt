1
00:00:00,000 --> 00:00:05,000
Okay, hello and welcome back to our study of the Tamil Bhada.

2
00:00:05,000 --> 00:00:13,000
Today we continue on with 1st number 72, which reads as follows.

3
00:00:13,000 --> 00:00:20,000
Now, we are going to talk about the Bhada.

4
00:00:20,000 --> 00:00:28,000
We are going to talk about the Bhada.

5
00:00:28,000 --> 00:00:38,000
We are going to talk about the Bhada.

6
00:00:38,000 --> 00:00:47,000
We are going to talk about the Bhada.

7
00:00:47,000 --> 00:01:00,000
Knowledge arises, to the extent that knowledge arises in a fool, in Bala.

8
00:01:00,000 --> 00:01:09,000
Anataya, to that extent it arises to that person's disadvantage.

9
00:01:09,000 --> 00:01:15,000
So, knowledge arises to the disadvantage of a fool.

10
00:01:15,000 --> 00:01:32,000
Hantim Bala says, soaksam, for it destroys, kills, or puts an end to their good luck, soaksam.

11
00:01:32,000 --> 00:01:40,000
Mindamasa, Vibhata, young, it crushes his or her head.

12
00:01:40,000 --> 00:01:48,000
Crushes her head, his head, their head.

13
00:01:48,000 --> 00:01:51,000
Very important verse.

14
00:01:51,000 --> 00:02:01,000
For all of us to keep in mind, this is the sort of verse that you can think of as exemplary

15
00:02:01,000 --> 00:02:16,000
of the Buddha's teaching, or it is indicative of the nature of the Buddha's teaching.

16
00:02:16,000 --> 00:02:21,000
And it shows how the place he gave to knowledge.

17
00:02:21,000 --> 00:02:28,000
It's not being a bad thing, but clearly not being enough.

18
00:02:28,000 --> 00:02:34,000
Having its limitations and potential disadvantages.

19
00:02:34,000 --> 00:02:40,000
So, what was this verse told in regards to?

20
00:02:40,000 --> 00:02:51,000
It seems that there's a theme here, as with the last verse, verse 71.

21
00:02:51,000 --> 00:02:57,000
This is told about Morgana, and the things he saw on Vulture's Peak.

22
00:02:57,000 --> 00:03:02,000
So, it seems that Morgana was coming down from Vulture's Peak again.

23
00:03:02,000 --> 00:03:11,000
Again, with the same monk, Lakana, who was, I guess, a good friend of Morgana.

24
00:03:11,000 --> 00:03:15,000
And again, he smiled.

25
00:03:15,000 --> 00:03:18,000
And again, Lakana asked him what he was smiling about.

26
00:03:18,000 --> 00:03:22,000
And again, just as before, Morgana refuses to answer and says,

27
00:03:22,000 --> 00:03:28,000
let me again, when we get in front of the Buddha.

28
00:03:28,000 --> 00:03:31,000
And in the Lakana, there's exactly that.

29
00:03:31,000 --> 00:03:37,000
And again, the Buddha admits that he saw, again,

30
00:03:37,000 --> 00:03:42,000
and again, Morgana tells him what he saw, and in this case,

31
00:03:42,000 --> 00:03:54,000
he's all ghost, huge invisible ethereal being.

32
00:03:54,000 --> 00:04:00,000
And this time, having his head crushed by hammers.

33
00:04:00,000 --> 00:04:07,000
So, again, and again, hammers would crash upon his skull and would burst.

34
00:04:07,000 --> 00:04:12,000
And again, and again, his head would be rebuilt.

35
00:04:12,000 --> 00:04:14,000
He would rebuild itself.

36
00:04:14,000 --> 00:04:22,000
And again, the hammers would crash down and repeat in this thing.

37
00:04:22,000 --> 00:04:26,000
And he said, I smiled because I've never seen such a being before.

38
00:04:26,000 --> 00:04:28,000
This is what the text says.

39
00:04:28,000 --> 00:04:33,000
So, it's an interesting thing.

40
00:04:33,000 --> 00:04:35,000
And then the Buddha says that he saw such a being,

41
00:04:35,000 --> 00:04:39,000
but didn't want to say anything because people would,

42
00:04:39,000 --> 00:04:42,000
if he didn't have a witness, people wouldn't believe him.

43
00:04:42,000 --> 00:04:46,000
And I think such is the case nowadays when I tell these stories,

44
00:04:46,000 --> 00:04:52,000
I risk, we risk people disbelieving it,

45
00:04:52,000 --> 00:04:55,000
feeling that such a being could not exist.

46
00:04:55,000 --> 00:04:57,000
So, we have a bit of a problem here.

47
00:04:57,000 --> 00:05:04,000
And I'd like people to focus not on details.

48
00:05:04,000 --> 00:05:10,000
You shouldn't focus on facts.

49
00:05:10,000 --> 00:05:12,000
In this historical facts.

50
00:05:12,000 --> 00:05:13,000
Did this happen?

51
00:05:13,000 --> 00:05:14,000
Did that not happen?

52
00:05:14,000 --> 00:05:15,000
It's not really important.

53
00:05:15,000 --> 00:05:17,000
Does such a being exist?

54
00:05:17,000 --> 00:05:18,000
Does such a being not exist?

55
00:05:18,000 --> 00:05:21,000
It's more important to understand the theory.

56
00:05:21,000 --> 00:05:25,000
And obviously, more important to understand this verse,

57
00:05:25,000 --> 00:05:30,000
which it doesn't require the story to be true.

58
00:05:30,000 --> 00:05:32,000
On the other hand, it would be, of course,

59
00:05:32,000 --> 00:05:36,000
much to our advantage if we could understand the mechanics

60
00:05:36,000 --> 00:05:40,000
behind such a being, how such a being can arise.

61
00:05:40,000 --> 00:05:44,000
And be clear in our minds that there's also the potential

62
00:05:44,000 --> 00:05:47,000
for us to be born as a huge,

63
00:05:47,000 --> 00:05:51,000
hulking being, having our head crushed by hammers.

64
00:05:51,000 --> 00:05:55,000
Apparently, there is such a potential.

65
00:05:55,000 --> 00:05:59,000
Even if we can't do that, if we can't bring our minds around the fact

66
00:05:59,000 --> 00:06:04,000
that beings have a different nature than ourselves exist.

67
00:06:04,000 --> 00:06:10,000
And the potential for the mind to create existence

68
00:06:10,000 --> 00:06:15,000
that is undetectable, or seemingly undetectable,

69
00:06:15,000 --> 00:06:19,000
by physical means, ordinary physical means.

70
00:06:19,000 --> 00:06:30,000
If we can, we shouldn't let it get in the way of our practice

71
00:06:30,000 --> 00:06:35,000
and our appreciation of the knowledge in this verse.

72
00:06:35,000 --> 00:06:42,000
If you can, well, there's always a benefit there.

73
00:06:42,000 --> 00:06:50,000
The benefit would be that you can then have faith in confidence

74
00:06:50,000 --> 00:06:53,000
in your practice.

75
00:06:53,000 --> 00:06:56,000
And it gives you encouragement in your practice

76
00:06:56,000 --> 00:06:58,000
to avoid such states.

77
00:06:58,000 --> 00:07:00,000
Nobody wants to be born such a being.

78
00:07:00,000 --> 00:07:05,000
Anyway, then amongst we're all like asking the Buddha

79
00:07:05,000 --> 00:07:09,000
how such a thing could occur, how such a being could exist,

80
00:07:09,000 --> 00:07:14,000
what is such a being due to obtain such an existence

81
00:07:14,000 --> 00:07:18,000
and the Buddha told the story of the past?

82
00:07:18,000 --> 00:07:22,000
So it seems there was once a cripple.

83
00:07:22,000 --> 00:07:28,000
And this cripple was very good in one thing

84
00:07:28,000 --> 00:07:31,000
and that was using a sling.

85
00:07:31,000 --> 00:07:34,000
So he had this sling

86
00:07:34,000 --> 00:07:41,000
and he would shoot stones at these big leaves,

87
00:07:41,000 --> 00:07:44,000
whatever big leaves there were.

88
00:07:44,000 --> 00:07:47,000
And he could cut the leaves,

89
00:07:47,000 --> 00:07:49,000
which is by slinging stones,

90
00:07:49,000 --> 00:07:51,000
he was so such a sharp shooter

91
00:07:51,000 --> 00:07:53,000
that he could cut the leaves in a certain pattern.

92
00:07:53,000 --> 00:07:56,000
And so he would, just for fun,

93
00:07:56,000 --> 00:07:59,000
he would cut them into shapes.

94
00:07:59,000 --> 00:08:02,000
And he did this to amuse the children.

95
00:08:02,000 --> 00:08:04,000
And the children would follow him around

96
00:08:04,000 --> 00:08:06,000
and ask him to make an elephant

97
00:08:06,000 --> 00:08:08,000
or ask him to make a bird or so on,

98
00:08:08,000 --> 00:08:12,000
and he would have blied.

99
00:08:12,000 --> 00:08:16,000
And one day they were in the King's garden

100
00:08:16,000 --> 00:08:18,000
and they were telling him

101
00:08:18,000 --> 00:08:21,000
he was making all these shapes and cutting the leaves out

102
00:08:21,000 --> 00:08:24,000
in various patterns.

103
00:08:24,000 --> 00:08:28,000
When all of a sudden the King came

104
00:08:28,000 --> 00:08:30,000
to the garden and then the children ran away

105
00:08:30,000 --> 00:08:32,000
and left the cripple behind.

106
00:08:32,000 --> 00:08:34,000
And the King found out

107
00:08:34,000 --> 00:08:37,000
and the King went, of course,

108
00:08:37,000 --> 00:08:40,000
to rest in the shade

109
00:08:40,000 --> 00:08:43,000
and found that the shade was patched.

110
00:08:43,000 --> 00:08:44,000
That the shade wasn't complete

111
00:08:44,000 --> 00:08:46,000
and he looked up and he saw these shapes

112
00:08:46,000 --> 00:08:48,000
and he said, what is this?

113
00:08:48,000 --> 00:08:49,000
Where is this happening?

114
00:08:49,000 --> 00:08:51,000
How is this happening?

115
00:08:51,000 --> 00:08:53,000
And he inquired and found out

116
00:08:53,000 --> 00:08:55,000
that there was this cripple.

117
00:08:55,000 --> 00:08:57,000
He thought, wow.

118
00:08:57,000 --> 00:08:59,000
You know, I could use such a person

119
00:08:59,000 --> 00:09:01,000
and he asked him, you know, would you be able to shoot

120
00:09:01,000 --> 00:09:04,000
and would you be able to shoot these pebbles

121
00:09:04,000 --> 00:09:07,000
speaking of the sharp shooting?

122
00:09:07,000 --> 00:09:09,000
Do you think you'd be able to shoot these pebbles

123
00:09:09,000 --> 00:09:11,000
into someone's mouth?

124
00:09:13,000 --> 00:09:15,000
And the cripple said, yeah, I could do that.

125
00:09:15,000 --> 00:09:17,000
And he said, I'd like you to shoot

126
00:09:17,000 --> 00:09:20,000
a pint pot of horse down,

127
00:09:20,000 --> 00:09:22,000
cow down again.

128
00:09:22,000 --> 00:09:25,000
Into someone's mouth for me.

129
00:09:25,000 --> 00:09:28,000
It turns out the King had an advisor

130
00:09:28,000 --> 00:09:30,000
who couldn't keep his mouth shut

131
00:09:30,000 --> 00:09:34,000
and would just lather on about anything and everything.

132
00:09:34,000 --> 00:09:36,000
And the King was annoyed by this

133
00:09:36,000 --> 00:09:40,000
and so thought they would teach this guy a lesson.

134
00:09:40,000 --> 00:09:45,000
And the cripple said, yes, I could do that.

135
00:09:45,000 --> 00:09:47,000
So the King took him to the court

136
00:09:47,000 --> 00:09:50,000
and hid him behind a curtain

137
00:09:50,000 --> 00:09:54,000
and the cripple poked a little hole in it

138
00:09:54,000 --> 00:09:57,000
and every time this advisor opened his mouth

139
00:09:57,000 --> 00:10:01,000
he would shoot a little pellet of dung into the guy's mouth

140
00:10:01,000 --> 00:10:03,000
and shut him up.

141
00:10:05,000 --> 00:10:07,000
Apparently he got away with this

142
00:10:07,000 --> 00:10:10,000
until he completely took a whole pint pot

143
00:10:10,000 --> 00:10:14,000
full of dung into the guy's mouth.

144
00:10:14,000 --> 00:10:17,000
And right.

145
00:10:17,000 --> 00:10:21,000
And the King then turned to the advisor

146
00:10:21,000 --> 00:10:23,000
and said, you know,

147
00:10:23,000 --> 00:10:27,000
he was such a bladder mouth, I can't take it anymore.

148
00:10:27,000 --> 00:10:31,000
And he said, all this time you've been sitting there

149
00:10:31,000 --> 00:10:34,000
and even when you couldn't even keep your mouth shut

150
00:10:34,000 --> 00:10:38,000
even when we were shooting pellets of dung into your mouth

151
00:10:38,000 --> 00:10:40,000
or something like that.

152
00:10:40,000 --> 00:10:41,000
It's bizarre, really.

153
00:10:41,000 --> 00:10:44,000
I didn't do this twice.

154
00:10:44,000 --> 00:10:47,000
It's quite interesting.

155
00:10:47,000 --> 00:10:50,000
This is the story that's been passed down.

156
00:10:50,000 --> 00:10:53,000
It's a bizarre sort of story, I think.

157
00:10:53,000 --> 00:10:54,000
An interesting way too.

158
00:10:54,000 --> 00:10:56,000
But it's a harmless way, I suppose.

159
00:10:56,000 --> 00:10:58,000
I mean, they're not hurting the guy.

160
00:10:58,000 --> 00:11:05,000
So it is kind of an ingenious way to get your point across.

161
00:11:05,000 --> 00:11:12,000
It said that the advisor never was very careful

162
00:11:12,000 --> 00:11:15,000
the openness is mouth in the future.

163
00:11:15,000 --> 00:11:25,000
But the point is, then this cripple became quite wealthy.

164
00:11:25,000 --> 00:11:30,000
The King showered honor and gained upon him.

165
00:11:30,000 --> 00:11:33,000
And our story comes in.

166
00:11:33,000 --> 00:11:38,000
Our verse comes in when the story of our verse comes in

167
00:11:38,000 --> 00:11:42,000
when another man saw this and was quite impressed

168
00:11:42,000 --> 00:11:44,000
and thought, wow, if I could learn that skill,

169
00:11:44,000 --> 00:11:51,000
I could probably gain favor and gain and fame as well.

170
00:11:51,000 --> 00:11:56,000
And so here's the problem when someone does something

171
00:11:56,000 --> 00:12:01,000
for the purpose of fame and fame and gain and all this.

172
00:12:01,000 --> 00:12:09,000
And so his desire was already unwholesome.

173
00:12:09,000 --> 00:12:11,000
His desire to learn.

174
00:12:11,000 --> 00:12:14,000
So he could probably purchase this cripple and the cripple

175
00:12:14,000 --> 00:12:19,000
doesn't want to teach him and he persuades him to teach him

176
00:12:19,000 --> 00:12:23,000
and finally he says, fine, I'll teach you what I know.

177
00:12:23,000 --> 00:12:31,000
And the cripple, he does this, he butters up the cripple

178
00:12:31,000 --> 00:12:36,000
and takes care of him and acts as a servant for a while

179
00:12:36,000 --> 00:12:39,000
until finally the cripple says, fine, I'll teach you what I know.

180
00:12:39,000 --> 00:12:42,000
And so he teaches him how to use this sling.

181
00:12:42,000 --> 00:12:44,000
And the man gets quite good at it.

182
00:12:44,000 --> 00:12:49,000
And so the short of it is that we have this man

183
00:12:49,000 --> 00:12:57,000
who's learned how to use a sling and is keen to show off.

184
00:12:57,000 --> 00:13:00,000
And so the cripple says, so how are you going to test?

185
00:13:00,000 --> 00:13:02,000
How are you, what are you going to do with this skill?

186
00:13:02,000 --> 00:13:06,000
And he says, well, I figure I'll find something to shoot at

187
00:13:06,000 --> 00:13:10,000
so that people can know what a sharp shooter I am.

188
00:13:10,000 --> 00:13:15,000
I'll figure I'll shoot at a cow or maybe I'll shoot at a man somewhere.

189
00:13:15,000 --> 00:13:17,000
And the cripple says, what do you talk?

190
00:13:17,000 --> 00:13:21,000
You can't just go around shooting things and living beings.

191
00:13:21,000 --> 00:13:24,000
If you shoot a cow, you'll get fined 500 gold pieces.

192
00:13:24,000 --> 00:13:28,000
If you shoot a human, you'll get fined the thousand gold pieces,

193
00:13:28,000 --> 00:13:30,000
something like this.

194
00:13:30,000 --> 00:13:34,000
He says, what you have to do is you have to find something to

195
00:13:34,000 --> 00:13:39,000
shoot at that nobody's going to miss.

196
00:13:39,000 --> 00:13:44,000
That has no father, no mother, nobody to complain about.

197
00:13:44,000 --> 00:13:50,000
So maybe some of you can tell where this is going.

198
00:13:50,000 --> 00:13:53,000
He says, good idea. I'll find something like that.

199
00:13:53,000 --> 00:13:56,000
So he looks around and he sees a cow.

200
00:13:56,000 --> 00:13:59,000
He says, no, that has an owner.

201
00:13:59,000 --> 00:14:02,000
And the owner is going to be concerned about it.

202
00:14:02,000 --> 00:14:05,000
And he looks and he sees a man.

203
00:14:05,000 --> 00:14:06,000
That's true.

204
00:14:06,000 --> 00:14:10,000
This man has a mother and a father and a wife and children and

205
00:14:10,000 --> 00:14:12,000
not a good idea.

206
00:14:12,000 --> 00:14:14,000
And he looks around finding something.

207
00:14:14,000 --> 00:14:15,000
She's a child.

208
00:14:15,000 --> 00:14:16,000
He's all these.

209
00:14:16,000 --> 00:14:21,000
Until finally, he sees a particular Buddha.

210
00:14:21,000 --> 00:14:24,000
And he says, this man.

211
00:14:24,000 --> 00:14:26,000
He sees some going on arms round.

212
00:14:26,000 --> 00:14:29,000
And it's amazing how he could just miss the fact that this is

213
00:14:29,000 --> 00:14:33,000
an enlightened being, but that's the case with people who are

214
00:14:33,000 --> 00:14:39,000
so blinded by their own desires and their attachments.

215
00:14:39,000 --> 00:14:44,000
And he says, this man.

216
00:14:44,000 --> 00:14:49,000
No father, no mother, no wife, no children.

217
00:14:49,000 --> 00:14:52,000
This man is the perfect target.

218
00:14:52,000 --> 00:14:57,000
No one will miss him.

219
00:14:57,000 --> 00:15:02,000
And so he takes this thing and he shoots the

220
00:15:02,000 --> 00:15:04,000
the particular Buddha right in the head and the

221
00:15:04,000 --> 00:15:09,000
particular Buddha dies.

222
00:15:09,000 --> 00:15:12,000
Oh, I'm a good shot with that.

223
00:15:12,000 --> 00:15:20,000
And the people of the village, of course, are very much at

224
00:15:20,000 --> 00:15:22,000
hat to the particular Buddha.

225
00:15:22,000 --> 00:15:25,000
And they find out he hasn't come for arms and so they go and

226
00:15:25,000 --> 00:15:27,000
look for him and they find that he's passed away.

227
00:15:27,000 --> 00:15:31,000
He's been killed and murdered.

228
00:15:31,000 --> 00:15:36,000
And this silly person comes up and says, yeah, that was me.

229
00:15:36,000 --> 00:15:43,000
I was, you know, look at what a sharpshooter I am.

230
00:15:43,000 --> 00:15:45,000
Killing this guy.

231
00:15:45,000 --> 00:15:49,000
The story is that the text says that he was able to shoot

232
00:15:49,000 --> 00:15:53,000
the stone in one ear and out the other ear.

233
00:15:53,000 --> 00:15:57,000
According to the text, yes.

234
00:15:57,000 --> 00:16:03,000
We can agree that's quite a feat.

235
00:16:03,000 --> 00:16:07,000
And yeah, his bragging has the,

236
00:16:07,000 --> 00:16:11,000
it doesn't quite have the expected result.

237
00:16:11,000 --> 00:16:15,000
They lynch him and I think they beat him and kill him.

238
00:16:15,000 --> 00:16:18,000
And he ends up going to hell as a result.

239
00:16:18,000 --> 00:16:25,000
And once he's finished his stint in hell,

240
00:16:25,000 --> 00:16:30,000
he has reborn as a ghost on Vulture speak and has

241
00:16:30,000 --> 00:16:33,000
hammers crashing down upon his skull.

242
00:16:33,000 --> 00:16:39,000
And the Buddha says, so you see,

243
00:16:39,000 --> 00:16:49,000
Yahweh Deva and Nataya, knowledge arises in a fool to his or her

244
00:16:49,000 --> 00:16:50,000
disadvantage.

245
00:16:50,000 --> 00:16:53,000
It destroys their good luck.

246
00:16:53,000 --> 00:16:58,000
It crushes their head.

247
00:16:58,000 --> 00:17:01,000
The text, the verse is obviously metaphorical.

248
00:17:01,000 --> 00:17:04,000
And so you don't need the story of a ghost having his head crushed,

249
00:17:04,000 --> 00:17:07,000
but that's the reference there.

250
00:17:07,000 --> 00:17:12,000
It actually can physically crush your head if you're not careful.

251
00:17:12,000 --> 00:17:16,000
Be born with hammers crushing your head.

252
00:17:16,000 --> 00:17:18,000
But this isn't the point of the verse.

253
00:17:18,000 --> 00:17:23,000
The point of the verse is that knowledge

254
00:17:23,000 --> 00:17:27,000
in the wrong hands isn't useful at all.

255
00:17:27,000 --> 00:17:29,000
We use it to our detriment.

256
00:17:29,000 --> 00:17:30,000
It's important.

257
00:17:30,000 --> 00:17:33,000
In a worldly sense, this is an important philosophy,

258
00:17:33,000 --> 00:17:42,000
an important ethical point with that knowledge is ethically

259
00:17:42,000 --> 00:17:44,000
variable.

260
00:17:44,000 --> 00:17:47,000
It's not always a good thing to have knowledge.

261
00:17:47,000 --> 00:17:52,000
More knowledge isn't always a good thing.

262
00:17:52,000 --> 00:17:56,000
And the variable is whether someone is wise or not.

263
00:17:56,000 --> 00:18:00,000
Wisdom and intelligence are obviously two very different things.

264
00:18:00,000 --> 00:18:07,000
You can have wise people with very little knowledge or intelligence.

265
00:18:07,000 --> 00:18:10,000
In fact, you could argue that to some extent

266
00:18:10,000 --> 00:18:13,000
they're inversely related.

267
00:18:13,000 --> 00:18:16,000
The more intelligence a person has,

268
00:18:16,000 --> 00:18:20,000
the harder it is for them to be wise.

269
00:18:20,000 --> 00:18:22,000
It's easier to become,

270
00:18:22,000 --> 00:18:24,000
conceded about your knowledge.

271
00:18:24,000 --> 00:18:29,000
It's easy to become attached to the gain that comes from your knowledge.

272
00:18:29,000 --> 00:18:34,000
And so even you find that professors of religion

273
00:18:34,000 --> 00:18:37,000
and even professors who study Buddhism

274
00:18:37,000 --> 00:18:42,000
go all the way to get their PhD and even become teachers in their own right.

275
00:18:42,000 --> 00:18:51,000
It can be very silly at times and have strange and bizarre views and beliefs.

276
00:18:51,000 --> 00:18:56,000
Obviously, in a worldly sense, it's even more clear

277
00:18:56,000 --> 00:19:00,000
how knowledge in the wrong hands can lead to suffering.

278
00:19:00,000 --> 00:19:06,000
It's a knowledge of nuclear,

279
00:19:06,000 --> 00:19:08,000
nuclear,

280
00:19:08,000 --> 00:19:10,000
they call it reactivity.

281
00:19:10,000 --> 00:19:14,000
Whatever it is that led them to create a atom bomb.

282
00:19:14,000 --> 00:19:20,000
So nuclear power could be used for benefit.

283
00:19:20,000 --> 00:19:21,000
Could be used for detriment.

284
00:19:21,000 --> 00:19:24,000
In fact, even nuclear power itself as we can see

285
00:19:24,000 --> 00:19:32,000
when it has the potential to cause great suffering,

286
00:19:32,000 --> 00:19:36,000
knowledge is not always a good thing.

287
00:19:36,000 --> 00:19:38,000
Much of our knowledge even today,

288
00:19:38,000 --> 00:19:40,000
as we can see now,

289
00:19:40,000 --> 00:19:43,000
is because of our lack of wisdom.

290
00:19:43,000 --> 00:19:55,000
We're using it to gain wealth and prosperity in the present moment

291
00:19:55,000 --> 00:19:57,000
at the expense of our future,

292
00:19:57,000 --> 00:20:00,000
as it seems with the change of the climate

293
00:20:00,000 --> 00:20:05,000
based on human activity and so on.

294
00:20:05,000 --> 00:20:10,000
Knowledge in fact seems to be one of those one thing that's set to kill us.

295
00:20:10,000 --> 00:20:16,000
Before we had engrossed ourselves in this knowledge

296
00:20:16,000 --> 00:20:19,000
as led to our technology,

297
00:20:19,000 --> 00:20:25,000
we were never in danger of destroying our environment

298
00:20:25,000 --> 00:20:27,000
and the environment in which we depend.

299
00:20:27,000 --> 00:20:29,000
It's an interesting example.

300
00:20:29,000 --> 00:20:31,000
The same course was nuclear power.

301
00:20:31,000 --> 00:20:37,000
We were never at risk of destroying civilization or civilizations.

302
00:20:37,000 --> 00:20:42,000
So knowledge is power and power, of course,

303
00:20:42,000 --> 00:20:45,000
is ethically variable.

304
00:20:45,000 --> 00:20:49,000
It can be beneficial.

305
00:20:49,000 --> 00:20:52,000
It can be used to create great wholesomeness.

306
00:20:52,000 --> 00:20:57,000
It can be used to create great evil as well.

307
00:20:57,000 --> 00:21:01,000
But on the meditation side of things,

308
00:21:01,000 --> 00:21:05,000
there's also a point to be made here that knowledge of meditation

309
00:21:05,000 --> 00:21:07,000
can sometimes be to our detriment.

310
00:21:07,000 --> 00:21:12,000
If you know too much, it can actually get in the way of your practice.

311
00:21:12,000 --> 00:21:16,000
Certainly, it doesn't necessarily help your practice.

312
00:21:16,000 --> 00:21:20,000
Knowledge accompanied by practice on the other hand,

313
00:21:20,000 --> 00:21:23,000
again, the power of knowledge can be a great thing

314
00:21:23,000 --> 00:21:27,000
if you're actually practicing to have knowledge of,

315
00:21:27,000 --> 00:21:30,000
of course, the correct way to practice,

316
00:21:30,000 --> 00:21:35,000
to have knowledge of background Buddhist theory.

317
00:21:35,000 --> 00:21:40,000
We have been done by the nature of the mind of wholesomeness,

318
00:21:40,000 --> 00:21:43,000
even knowledge of these stories.

319
00:21:43,000 --> 00:21:47,000
If a person grasps them in the wrong way

320
00:21:47,000 --> 00:21:51,000
and becomes skeptical, thinking such things could never happen,

321
00:21:51,000 --> 00:21:53,000
could never exist.

322
00:21:53,000 --> 00:21:57,000
How is it possible for such a ghost being too reborn?

323
00:21:57,000 --> 00:22:02,000
They focus on that aspect of it and become full of doubt.

324
00:22:02,000 --> 00:22:09,000
If they don't have the experience to see how such a thing is possible,

325
00:22:09,000 --> 00:22:13,000
how the mind can create such an existence,

326
00:22:13,000 --> 00:22:16,000
then it will actually create doubt in the mind,

327
00:22:16,000 --> 00:22:18,000
be a detriment to the practice.

328
00:22:18,000 --> 00:22:20,000
If on the other hand, one grasps it correctly

329
00:22:20,000 --> 00:22:23,000
and sees the message behind it,

330
00:22:23,000 --> 00:22:30,000
more over is even better is able to understand how such a thing could come about,

331
00:22:30,000 --> 00:22:39,000
be able to see how the mind works and experience how it's able to create such an existence,

332
00:22:39,000 --> 00:22:42,000
then it can be to one's advantage,

333
00:22:42,000 --> 00:22:49,000
reminding one of the potential for suffering for those who are

334
00:22:49,000 --> 00:22:56,000
bent upon the wrong path and on and wholesomeness.

335
00:22:56,000 --> 00:23:00,000
So this is just one more lesson for us to keep in mind that knowledge

336
00:23:00,000 --> 00:23:02,000
and wisdom are two very different things

337
00:23:02,000 --> 00:23:04,000
and simply studying the Buddha's teaching,

338
00:23:04,000 --> 00:23:08,000
even listening to watching these videos

339
00:23:08,000 --> 00:23:14,000
and reading about the Buddha's teaching is not enough.

340
00:23:14,000 --> 00:23:18,000
It isn't a substitute for actually practicing the Buddha's teaching.

341
00:23:18,000 --> 00:23:23,000
So, more food for thought from the Buddha

342
00:23:23,000 --> 00:23:27,000
and have started in the Dhamapada.

343
00:23:27,000 --> 00:23:32,000
That's all for today. Thank you all for tuning in.

344
00:23:32,000 --> 00:23:56,000
Thank you very much.

