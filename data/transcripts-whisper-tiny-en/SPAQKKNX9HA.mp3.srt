1
00:00:00,000 --> 00:00:21,000
evening everyone broadcasting live may 10th tonight's quote is on the Buddha the

2
00:00:21,000 --> 00:00:31,560
Dhamma and the Sangha. So I'm going to tell you got Teragatak actually the

3
00:00:31,560 --> 00:00:45,720
verses to the elder monks. We have recorded verses that were spoken by the

4
00:00:45,720 --> 00:00:59,040
various arahans as poetry and reflection on their attainments. And we have stories

5
00:00:59,040 --> 00:01:09,600
behind each one of these. So this one is from a monk called Dheikitakaari. Dheikitakaari

6
00:01:09,600 --> 00:01:27,200
one who does Dheikitaka. Let's see if we can look at this. He was the son of a

7
00:01:27,200 --> 00:01:32,560
brahmana, Subandhu. Subandhu. It was so called because he brought safety into the

8
00:01:32,560 --> 00:01:41,120
world. He was brought safely into the world with the aid of physicians. I don't

9
00:01:41,120 --> 00:01:54,440
understand the word. When he was grown up his father incurred the jealousy and

10
00:01:54,440 --> 00:02:03,080
suspicion of a minister, Chanika, Chanika, and Chandagoota who had him throw his

11
00:02:03,080 --> 00:02:09,360
father was put in prison. And so the son Dheikitakaari and his fright fled and

12
00:02:09,360 --> 00:02:14,920
taking refuge with the forest while a monk entered the order and

13
00:02:14,920 --> 00:02:31,680
brought in the open air, never sleeping in heedless of heat and cold. Many

14
00:02:31,680 --> 00:02:35,680
are the reasons why people become monks.

15
00:02:35,680 --> 00:02:45,520
I'm now going to say this is, is that Melinda asking Melinda asks him why did you

16
00:02:45,520 --> 00:02:50,560
become a monk? Did you become a monk to become an arrow? It's all that was young

17
00:02:50,560 --> 00:02:57,240
kind of embarrassed the way he says it. Some people become monks for the wrong

18
00:02:57,240 --> 00:03:05,160
reason. Sometimes for the wrong reason but then find the right reason.

19
00:03:05,160 --> 00:03:24,360
But it's an important part of society. I think to have this institution, this

20
00:03:24,360 --> 00:03:38,000
option for people who are at their stage at a point in their lives or in their

21
00:03:38,000 --> 00:03:53,360
journeys, I'm sorry, that they are able and willing and they are turned off by

22
00:03:53,360 --> 00:04:06,360
the world, having this ability to choose to dedicate yourself to meditation and

23
00:04:06,360 --> 00:04:17,400
study and teaching of spiritual truths and paths to enlightenment and so on.

24
00:04:17,400 --> 00:04:22,560
I mean this is not only are these sort of people important people who have the

25
00:04:22,560 --> 00:04:31,120
time and the energy and the knowledge and the practice to teach but also for

26
00:04:31,120 --> 00:04:38,920
those who are seeking it, those who can't find a place in the world, who have

27
00:04:38,920 --> 00:04:48,840
an earnest wish to become a spiritual person and deliver a inclusive life.

28
00:04:48,840 --> 00:05:00,800
Anyway, so this monk's verses are about the Buddha, the demon of Sangha. He says,

29
00:05:00,800 --> 00:05:14,840
Buddha, my young Anusarapasana, one should reflect with a good heart, with a

30
00:05:14,840 --> 00:05:30,640
pure heart on the Buddha who is immeasurable, Pitya, Puttasari, Roho, he sees Satatamud-goh,

31
00:05:30,640 --> 00:05:46,840
one's sari that is one's whole being, Puttas, Puttas means permeated with rapture,

32
00:05:46,840 --> 00:05:56,280
with exaltation, with this excitement, this energy, this rapture, he sees Satatamud-goh,

33
00:05:56,280 --> 00:06:04,240
one will always be, always be uplifted and be constantly uplifted, the same with the

34
00:06:04,240 --> 00:06:09,720
demon of Sangha. These are just reflecting on the Buddha, the demon of Sangha, just thinking

35
00:06:09,720 --> 00:06:19,480
about these things, just reminiscing or revering and just that, not even practicing it.

36
00:06:19,480 --> 00:06:28,680
They're so powerful that even worshipping them is a great benefit. It makes you uplifted,

37
00:06:28,680 --> 00:06:39,200
unda-goh, unda-goh, makes you uplifted, satatam, continuously. So if you keep the Buddha,

38
00:06:39,200 --> 00:06:46,360
the demon of Sangha in mind, you will never be depressed. There was one, I told this story

39
00:06:46,360 --> 00:06:57,080
several times about this woman in the U.S. who was in a very bad situation. So she was doing

40
00:06:57,080 --> 00:07:03,480
some meditation, but she was basically trapped in somebody else's house, not physically, but

41
00:07:05,240 --> 00:07:09,720
for all intents and purposes, quite trapped where she was in some bad situation and they were

42
00:07:09,720 --> 00:07:21,160
sort of very fundamentalist, non-Buddhist religious people. And so I told her also to recite

43
00:07:23,960 --> 00:07:28,680
some chanting to the Buddha and the demon of Sangha's short chance and chant a Buddha image

44
00:07:28,680 --> 00:07:34,520
and she was doing this and then the family confiscated the Buddha image. But the power of it,

45
00:07:34,520 --> 00:07:43,560
they were going to destroy this Buddha image, but the power of her practice and her devotion to

46
00:07:43,560 --> 00:07:51,720
the Buddha, coupled with their hatred and vilification of the Buddha. She had to find someone to

47
00:07:51,720 --> 00:07:56,120
take this Buddha image away or they were going to destroy it. And so she found someone to take it

48
00:07:56,120 --> 00:08:05,560
and when they came to take it, they talked and out of mutual sort of appreciation, they found a

49
00:08:05,560 --> 00:08:12,040
job for her and she moved all because of this Buddha image and because of her practice, she was

50
00:08:12,040 --> 00:08:18,680
able to buy chance, meet these people who were also, I guess, Buddhist or at least

51
00:08:18,680 --> 00:08:31,880
amicable towards Buddhism and was able to leave the house right away. It was quite an inspiring

52
00:08:31,880 --> 00:08:40,520
story all because of her chanting, the indirectly of course, but oil and water can't mix good and

53
00:08:40,520 --> 00:08:53,800
evil, a hard time staying in the same house. So it's important to, this is one of the protective

54
00:08:53,800 --> 00:09:01,000
meditations, meditating on the Buddha, the demon, the sangha. Protects, keeps you safe, keeps your

55
00:09:01,000 --> 00:09:14,200
mind strong and vibrant, ecstatic, energetic, something worth doing, worth reading about the Buddha,

56
00:09:14,200 --> 00:09:19,480
reading about his life, worth going to India to see where the Buddha lived, all these things to

57
00:09:20,520 --> 00:09:25,080
make the Buddha a part of your life and a part of your conscious experience.

58
00:09:25,080 --> 00:09:39,720
So anyway, not too much to say about that, there are questions tonight, see a bunch of

59
00:09:40,920 --> 00:09:50,920
bunch of comments. I'm going to just skip questions that don't have the queue in front,

60
00:09:50,920 --> 00:09:57,000
because otherwise it makes it so much easier to skim through. So they don't have the special

61
00:09:57,000 --> 00:10:04,840
queue in front, special question mark, this one is. Wow, I won't call the game. But what

62
00:10:04,840 --> 00:10:11,320
says I said this question was stupid, I didn't really say that today, maybe I choked about it or

63
00:10:11,320 --> 00:10:18,840
something. Do you ever read great works from later Buddhist masters like Nagarjuna,

64
00:10:18,840 --> 00:10:24,920
nope, nope, not so much. But I have heard good things about Nagarjuna, so

65
00:10:28,600 --> 00:10:33,160
I wouldn't be against reading it. I mean, I think that's the sort of Mahayana stuff that I'd

66
00:10:33,160 --> 00:10:44,040
actually be interested in. Suppose to East Asian Mahayana stuff, which is quite unpalatable to me.

67
00:10:44,040 --> 00:10:51,240
I imagine I'd probably have some critical, be fairly critical in the Nagarjuna personally, but

68
00:10:52,520 --> 00:10:59,240
I don't know. I'm sure I'd find some of it impressive because I understand he was a fairly impressive

69
00:10:59,240 --> 00:11:14,280
person. No other questions. And a lot last night, we must have gone through them all.

70
00:11:14,920 --> 00:11:23,640
Michael tells me one of my videos goes on Reddit, run on Reddit in the past few days or something.

71
00:11:23,640 --> 00:11:31,880
And it's very highly ranked or something like that. Under the meditation, some Reddit.

72
00:11:35,160 --> 00:11:39,560
Why did Anana take so long to become an Arahant? Because he was very busy

73
00:11:40,760 --> 00:11:45,800
looking after the Buddha, spent all this time caring for the Buddha that he didn't actually

74
00:11:45,800 --> 00:11:50,680
meditate much. And he was concerned about this himself with the Buddha said, don't be concerned,

75
00:11:50,680 --> 00:11:54,760
after I be attained, by any bandhayana you will become an Arahant.

76
00:11:59,240 --> 00:12:06,600
What's the name of the chanting you mentioned about reflection on the Buddha, the Dhamman, the Sangha?

77
00:12:06,600 --> 00:12:20,920
So if you're looking at any good Buddhist, Theravada Buddhist chanting, look, you'll find

78
00:12:23,240 --> 00:12:29,560
these three things. If you want to look it up, it's in the Dhanjika Sutta. It's actually

79
00:12:29,560 --> 00:12:36,680
from a Sutta, so the Buddha actually recited these. As an example for us, the

80
00:12:37,960 --> 00:12:46,440
pop of the banner or the banner protection or something, the Dhanjanga.

81
00:12:46,440 --> 00:12:58,200
A little Buddha, yes, I did see it. And in fact, when I was before I was a monk,

82
00:13:01,160 --> 00:13:09,640
my late teacher had me stitched together the Buddha story parts of the Buddha

83
00:13:09,640 --> 00:13:16,680
to have a good Buddha story of the Buddha. But again, that movie stops as soon as he becomes a

84
00:13:16,680 --> 00:13:22,680
Buddha. And so it's like very so very Mahatmayana in a sense that doesn't care about the Buddha in

85
00:13:22,680 --> 00:13:28,840
his life. It's all about how to become a Buddha, which is disappointing from the point of view of

86
00:13:28,840 --> 00:13:36,440
those of us who appreciate the 45 years that came after much more, maybe not much more,

87
00:13:36,440 --> 00:13:45,640
but consider it to be actually much more important. They always skipped the important part

88
00:13:45,640 --> 00:14:09,960
from my point of view. Yeah, the Buddha story part was not entirely coding to the texts,

89
00:14:09,960 --> 00:14:19,960
but it wasn't bad. I mean, I'm a little older. I'm 37 yesterday, so 20 years ago,

90
00:14:19,960 --> 00:14:26,360
isn't all that all that. But I could say it must have seen it after after I became a Buddha.

91
00:14:26,360 --> 00:14:40,600
So sometime in 2000, 2001, probably.

92
00:14:40,600 --> 00:15:02,840
Okay, who leaves. All right, well, let's call it there. We'll show all a good night. Good practice.

93
00:15:03,560 --> 00:15:09,640
Oh, we got one more. Something you would address the discussion when doing deeds without expecting

94
00:15:09,640 --> 00:15:14,200
in return in regards to last night's quoted doers or choices. Well, I did, didn't I? Last night,

95
00:15:14,200 --> 00:15:23,240
I talked about it. It may be disagree, but I can do it again. If you do deeds without expecting

96
00:15:23,240 --> 00:15:29,080
anything in return, it's, you know, what good is it? Why are you doing them?

97
00:15:29,080 --> 00:15:43,080
I guess I suppose an Arahan acts in that way, but that's because they've done what needs to

98
00:15:43,080 --> 00:15:48,280
be done for anyone who hasn't done what needs to be done. They have to do things expecting

99
00:15:48,280 --> 00:15:53,160
yourself and looking for yourself, you know, purposefully. So doing good deeds is purposeful,

100
00:15:53,160 --> 00:16:01,400
and Arahan has no purpose, so they do deeds without, but they don't purposefully do. They just

101
00:16:01,400 --> 00:16:09,320
do things as a matter of course. Or you could put it that way that they do deeds without expecting it,

102
00:16:09,320 --> 00:16:13,720
but that's the way of it. If someone who's already done what needs to be done, got done,

103
00:16:13,720 --> 00:16:23,880
cut her knee out. We have not yet done what needs to be done, so that's why we do good deeds.

104
00:16:23,880 --> 00:16:27,720
That's not even really the point of the quote. The quote isn't that you do, isn't saying that

105
00:16:27,720 --> 00:16:32,520
you do something expecting to rejoice. It's saying that when you do good deeds, you do rejoice.

106
00:16:33,720 --> 00:16:38,840
It's not quite even, yeah. When you feel happy, happiness comes from doing good deeds whether you

107
00:16:38,840 --> 00:16:50,920
want it or not. When that's what it's saying, it's not saying you should do good deeds, one thing

108
00:16:50,920 --> 00:16:56,760
to feel happy. It's saying you should, it's saying if you do good deeds, you feel happy. I mean,

109
00:16:58,120 --> 00:17:03,640
it's a way of explaining the difference between good and evil. Evil leads to suffering.

110
00:17:03,640 --> 00:17:13,960
In the Sotchati page, the Sotchati, they serve their weak, they sorrow here, they're sad here,

111
00:17:13,960 --> 00:17:28,360
they're sad in the next life. Those who do evil deeds. Maybe that clears it up a little.

112
00:17:28,360 --> 00:17:35,400
Okay, you're welcome. I didn't really call your questions stupid, did I? I don't do that.

113
00:17:37,880 --> 00:17:41,000
I know I can be insensitive sometimes. I apologize.

114
00:17:46,840 --> 00:17:52,360
I blame it on my parents. I didn't go to public school. I was home schooled through my childhood,

115
00:17:52,360 --> 00:17:59,880
so I don't know how to deal with people. Never been good with social morals and norms.

116
00:18:09,720 --> 00:18:15,080
I didn't say it was either. I don't believe you. You're making this up.

117
00:18:15,080 --> 00:18:26,200
Anyway, it's in the past, so I'm not going to dwell. I just should you.

118
00:18:26,200 --> 00:18:49,000
Okay, good night, everyone. Thank you all for tuning in, wishing you all the best.

