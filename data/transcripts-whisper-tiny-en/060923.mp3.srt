1
00:00:00,000 --> 00:00:19,760
This morning I continue with a series of talks on the greatest blessings of tricks

2
00:00:19,760 --> 00:00:35,880
in the world. The next set in order is R, R, T, E, R, T, A, P, M, J, P, M, A, P, M, A, P, M, A,

3
00:00:35,880 --> 00:00:45,880
T, M, T, M, M, M, M, M, M, M, M, M, M, M.

4
00:00:45,880 --> 00:00:55,880
We are continuing to talk about the Lord Buddha taught to be things which bring personal development,

5
00:00:55,880 --> 00:01:03,880
which lead one to progress and to develop in one's life.

6
00:01:03,880 --> 00:01:14,880
And here we have a new set of three blessings, three things which lead one to development.

7
00:01:14,880 --> 00:01:21,880
Lead one to develop and to progress in one's life.

8
00:01:21,880 --> 00:01:42,880
R, T, V, R, T, P, P, A, to avoid and abstain from all evil.

9
00:01:42,880 --> 00:01:49,880
to give up alcohol and intoxication.

10
00:01:49,880 --> 00:02:08,880
A permit was a tummy, so to give up negligence in regards to the damage.

11
00:02:08,880 --> 00:02:12,880
The dam of the truth.

12
00:02:12,880 --> 00:02:20,880
To be ever vigilant and heedful in regards to the damage.

13
00:02:20,880 --> 00:02:24,880
All dammen.

14
00:02:24,880 --> 00:02:39,880
It among these are things which need one to development and prosperity in one's life.

15
00:02:39,880 --> 00:02:49,880
All that dewitted deep up.

16
00:02:49,880 --> 00:02:56,880
Means to stay in from those things which are unwholesome which lead to suffering.

17
00:02:56,880 --> 00:03:06,880
Those things which are considered to be evil in the highest sense.

18
00:03:06,880 --> 00:03:10,880
As for evil, there are three kinds of evil.

19
00:03:10,880 --> 00:03:17,880
There's evil by way of body, evil by way of speech, and evil by way of mind.

20
00:03:17,880 --> 00:03:26,880
When a person does evil deeds, it's either by way of one of these three doors.

21
00:03:26,880 --> 00:03:34,880
By way of body we have killing and stealing and sexual misconduct.

22
00:03:34,880 --> 00:03:46,880
By way of speech we have lying and divisive speech, heart speech, useless speech, frivolous speech.

23
00:03:46,880 --> 00:03:59,880
By way of mind we have greed, anger, and delusion.

24
00:03:59,880 --> 00:04:06,880
One thing with other people have being hateful towards other people, and having wrong view

25
00:04:06,880 --> 00:04:13,880
or a wrong understanding about the world.

26
00:04:13,880 --> 00:04:24,880
Lord Buddha said, I can't even do any evil.

27
00:04:24,880 --> 00:04:28,880
He's just not worth doing.

28
00:04:28,880 --> 00:04:37,880
Even a small evil deed is something which we should avoid and abstain from at all times.

29
00:04:37,880 --> 00:04:39,880
We avoid and abstain from evil deeds.

30
00:04:39,880 --> 00:04:40,880
There are three ways.

31
00:04:40,880 --> 00:04:42,880
The first way is to keep morality.

32
00:04:42,880 --> 00:04:51,880
We make a promise to ourselves not to kill, not to steal and so on.

33
00:04:51,880 --> 00:05:04,880
This is the way to stop the bad deeds of body and speech, but the bad deeds of mind are much more difficult to root out.

34
00:05:04,880 --> 00:05:08,880
When we practice morality we keep precepts.

35
00:05:08,880 --> 00:05:17,880
We make a promise not to kill, not to steal, not to commit adultery, not to use wrong speech.

36
00:05:17,880 --> 00:05:26,880
The five precepts are the eight precepts, and the ten precepts are even 227 precepts.

37
00:05:26,880 --> 00:05:34,880
This is considered to be morality.

38
00:05:34,880 --> 00:05:38,880
The second way we practice meditation and develop our minds.

39
00:05:38,880 --> 00:05:48,880
We sit and practice rising, falling, watch the breath.

40
00:05:48,880 --> 00:06:01,880
Our mind develops concentration and focus and is able to do away with the bad deeds of mind so that our minds become pure.

41
00:06:01,880 --> 00:06:12,880
We sit and simply acknowledge the simple things which arise and cease.

42
00:06:12,880 --> 00:06:18,880
We acknowledge the pains and the aching which arise and cease in the body, which come and go.

43
00:06:18,880 --> 00:06:23,880
Without attaching to anything we just watch.

44
00:06:23,880 --> 00:06:30,880
We'll truth around us.

45
00:06:30,880 --> 00:06:43,880
When we do this all of the bad things in the mind all greed and anger and confusion disappear temporarily.

46
00:06:43,880 --> 00:06:49,880
Even once we practice meditation all of the bad things disappear.

47
00:06:49,880 --> 00:06:56,880
If we stop there, I stop at the practice of meditation.

48
00:06:56,880 --> 00:07:07,880
Our minds may be clear and calm at that time, but they will not be clean in the highest sense.

49
00:07:07,880 --> 00:07:14,880
Once we sit down and practice meditation alone Buddha taught us, we need to seek clearly.

50
00:07:14,880 --> 00:07:19,880
We need to acknowledge everything which arises and cease.

51
00:07:19,880 --> 00:07:26,880
After our minds become peaceful and calm, we have to pay attention to everything which arises.

52
00:07:26,880 --> 00:07:31,880
Even calm feelings, we pay attention to your knowledge, calm, calm.

53
00:07:31,880 --> 00:07:35,880
When we're thinking about this or about that, we acknowledge thinking.

54
00:07:35,880 --> 00:07:47,880
Bring our minds always back to the present home, to the rising and the falling, to the movements of the foot and so on.

55
00:07:47,880 --> 00:07:54,880
When we practice in this way, watching present reality become to gain wisdom.

56
00:07:54,880 --> 00:08:01,880
On this can destroy all tendency to give rise to defilements.

57
00:08:01,880 --> 00:08:08,880
Not only do the defilements disappear to bad things in our minds and all the bad deeds of body and speech, they disappear.

58
00:08:08,880 --> 00:08:16,880
But also any tendency for them to arise again, any potential is rooted out because of wisdom.

59
00:08:16,880 --> 00:08:22,880
We know for ourselves, what is the truth?

60
00:08:22,880 --> 00:08:25,880
There's no possibility for any of these bad things to arise.

61
00:08:25,880 --> 00:08:30,880
Even if we stop practicing, truth stays with us.

62
00:08:30,880 --> 00:08:35,880
Be with us for all time.

63
00:08:35,880 --> 00:08:46,880
The Lord Buddha taught this as something suspicious in the highest sense, leads to development, and leads to prosperity.

64
00:08:46,880 --> 00:08:55,880
When we don't do evil deeds, we don't have enemies, we don't have fear of repercussions of our bad deeds.

65
00:08:55,880 --> 00:09:04,880
Our minds are peaceful and calm, we don't have guilt, any guilty feelings for bad things we've done.

66
00:09:04,880 --> 00:09:11,880
When we refrain from evil deeds, it's a, on whatever level we refrain.

67
00:09:11,880 --> 00:09:20,880
It's a cause for greater and further development, greater and further spiritual gain.

68
00:09:20,880 --> 00:09:23,880
This is something we should set ourselves in.

69
00:09:23,880 --> 00:09:28,880
We're staying from bad deeds of body and staying from bad deeds of speech,

70
00:09:28,880 --> 00:09:35,880
and we're staying from bad deeds of mind.

71
00:09:35,880 --> 00:09:37,880
We're just the first one.

72
00:09:37,880 --> 00:09:52,880
We're just going to give up intoxication, alcohol and drugs and narcotics things which lead to the mind to a drunken,

73
00:09:52,880 --> 00:09:56,880
unmindful state.

74
00:09:56,880 --> 00:10:09,880
Many people ask why is it so important to give up alcohol to give up drugs and narcotics which lead to intoxication?

75
00:10:09,880 --> 00:10:18,880
When we come to practice meditation, why is it so important that we live our lives free from these things?

76
00:10:18,880 --> 00:10:29,880
The most important factor in the development of the mind is mindfulness, what we call mindfulness, the word sett.

77
00:10:29,880 --> 00:10:36,880
Sett means to be aware of what's happening, to remind yourself all the time what's going on around you,

78
00:10:36,880 --> 00:10:43,880
to bring yourself to the present moment, to acknowledge rising and falling, this is mindfulness.

79
00:10:43,880 --> 00:10:47,880
It's knowing clearly what's happening.

80
00:10:47,880 --> 00:10:57,880
When we take drugs or intoxicants or alcohol, any of these substances which cloud the mind,

81
00:10:57,880 --> 00:11:01,880
it has the opposite effect of mindfulness.

82
00:11:01,880 --> 00:11:07,880
Even some substances bring great clarity of mind.

83
00:11:07,880 --> 00:11:09,880
They can never bring mindfulness.

84
00:11:09,880 --> 00:11:13,880
In fact, they take away the mindfulness of the mind.

85
00:11:13,880 --> 00:11:23,880
Mindfulness is like the ultimate sobriety, the ultimate state of sobriety, being sober in the ultimate sense.

86
00:11:23,880 --> 00:11:27,880
The minds are not intoxicated by anything whatsoever.

87
00:11:27,880 --> 00:11:35,880
Not intoxicated by sights, by sounds, by smells, by teeth, by feelings or by thoughts.

88
00:11:35,880 --> 00:11:47,880
When we take intoxicating substances, the purpose of taking them and the result from taking them is the intoxication of the mind.

89
00:11:47,880 --> 00:12:01,880
Some sort of happy, blissful state, which is unable to comprehend the truth of suffering or the cause of suffering.

90
00:12:01,880 --> 00:12:18,880
It's unable to see that the disadvantages of clinging, this advantage is upholding onto the eye, the ear, the nose, the tongue, the body and the heart.

91
00:12:18,880 --> 00:12:23,880
It's unable to be mindful.

92
00:12:23,880 --> 00:12:31,880
So we have to start as a base for our practice.

93
00:12:31,880 --> 00:12:38,880
The extension from alcohol and narcotics is a base for the practice of the past and meditation.

94
00:12:38,880 --> 00:12:47,880
Without it, it's impossible to develop meditation and the need in any real sense.

95
00:12:47,880 --> 00:12:54,880
Mindfulness is being sober, both in body and in mind.

96
00:12:54,880 --> 00:13:09,880
If we're not committed to sobriety, we can never hope to develop in the past and the meditation, because our minds will always be clouded and unclear.

97
00:13:09,880 --> 00:13:26,880
This is also a great auspiciousness. You can tell whether someone is going to develop, whether they are committed to giving up intoxication or not.

98
00:13:26,880 --> 00:13:34,880
If someone is still indulging in intoxication and intoxicating, intoxicating in this way or that way.

99
00:13:34,880 --> 00:13:43,880
Even not in terms of substances, but in terms of the things around them, sites and sounds and smells and tastes and feelings and thoughts.

100
00:13:43,880 --> 00:13:54,880
If they're still intoxicated by any one of these things, you can see they're not going to be sure they're not going to develop or they'll be difficult for them to develop.

101
00:13:54,880 --> 00:14:02,880
We talk to people who have given these things up. It's quite sure that these people who have prosperity and progress in their lives.

102
00:14:02,880 --> 00:14:17,880
This is so this is considered a great auspiciousness, something which will certainly lead to goodness in the future of blessing in the highest sense.

103
00:14:17,880 --> 00:14:46,880
The third one in third blessing today is in regards to all dumbness, all good things and all bad things to have up-a-mata, up-a-mata means the opposite of negligence, not being negligent, being vigilant, being diligent, being hateful, being mindful, being aware at all times.

104
00:14:46,880 --> 00:15:02,880
In regards to dumbness, there are two kinds of reality. One is good things, one is bad thing.

105
00:15:02,880 --> 00:15:24,880
Good things means all wholesome or beneficial realities and bad things means all unwholesome, all unbeneficial, all unhelpful realities, all things which lead to suffering.

106
00:15:24,880 --> 00:15:42,880
The Lord Buddha taught we should never be negligent in regards to these things, never be forgetful, never simply let our minds follow after good things or follow after bad things.

107
00:15:42,880 --> 00:16:00,880
In regards to all wholesome and unwholesome things, we should not be negligent. Whatever good things exist in the world means whatever wholesome things exist, whatever beneficial things, beneficial qualities of mind there are. We should develop these things.

108
00:16:00,880 --> 00:16:23,880
For instance, mindfulness, concentration, effort, confidence, wisdom and so on. We should not be negligent in developing these things. We should have every chance that we get, we should try to develop these things in our mind.

109
00:16:23,880 --> 00:16:40,880
We should continue to practice meditation with unending effort and concentration and focus and confidence and so on.

110
00:16:40,880 --> 00:16:51,880
We can develop wisdom and mindfulness, become free from suffering. We should not be negligent in regards to unwholesome things either.

111
00:16:51,880 --> 00:16:59,880
Whatever bad things exist in the mind, we should not let them simply let them continue and persist in our minds.

112
00:16:59,880 --> 00:17:11,880
We should work our hardest to root them out, to take them out of the mind. We should make a determination that we will not let these things stay in our minds.

113
00:17:11,880 --> 00:17:30,880
We will continue to practice mindfulness until these things have left the mind, until these things will no longer come back into the mind, until we attain the state of no return, so that all bad things will never return into our mind.

114
00:17:30,880 --> 00:17:59,880
We should not be negligent in regards to good things and bad things. Whatever is good, we should develop it, keep it in our minds.

115
00:17:59,880 --> 00:18:08,880
Whatever is bad, we should throw it away and keep it out of our minds. Keep it away from our mind.

116
00:18:08,880 --> 00:18:16,880
This is a great auspiciousness that leads to happiness and peace, because the cause of all suffering is unwholesome.

117
00:18:16,880 --> 00:18:25,880
Whatever suffering there is in our lives, it comes from unwholesomeness of bad things we have in our minds in the present moment.

118
00:18:25,880 --> 00:18:41,880
We have done in the past because of our unwholesomeness. Whatever happiness there is, it comes from wholesomeness, from doing good deeds, from having a clear mind, a clean mind.

119
00:18:41,880 --> 00:19:04,880
This is a truth which the Lord Buddha taught. When we practice mindfulness in this way, we become someone free from unwholesomeness with a mindful of wholesome, full of goodness.

120
00:19:04,880 --> 00:19:19,880
We become someone who is not negligent in regards to the Dharma, in regards to reality, who comes to act in accordance with reality and accordance with the truth.

121
00:19:19,880 --> 00:19:43,880
This is considered a great auspiciousness, it leads to happiness and freedom from suffering.

122
00:19:43,880 --> 00:19:51,880
Thank you.

