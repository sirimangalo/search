1
00:00:00,000 --> 00:00:08,120
Welcome back to Buddhism 101. Today we continue on with our study of morality and the

2
00:00:08,120 --> 00:00:19,200
aspect of morality known as reflection on the use of requisites. So no matter how simple

3
00:00:19,200 --> 00:00:27,160
a life we live, as human beings there are certain things that we require in order to continue

4
00:00:27,160 --> 00:00:34,160
our lives in order to meet with success even on the level of the practice of meditation.

5
00:00:34,160 --> 00:00:43,720
So in the context of the Buddha's teaching there are these certain set of things that we have

6
00:00:43,720 --> 00:00:54,800
to use and so try to use them wisely and reflecting before partaking in their use. The

7
00:00:54,800 --> 00:01:07,640
four main requisites in Buddhism are food, robes, food clothing, shelter and medicines. And these

8
00:01:07,640 --> 00:01:13,800
are considered to be four things that in general human beings need to survive and so even

9
00:01:13,800 --> 00:01:20,240
people undertaking intensive meditation practice still have to partake of. The point is here

10
00:01:20,240 --> 00:01:27,200
that with these things or with other things, other objects, possessions that we use in our

11
00:01:27,200 --> 00:01:33,800
lives there's a potential for going outside of the boundaries of what is conducive to

12
00:01:33,800 --> 00:01:38,960
meditation practice and becoming distracted or intoxicated by the use of material

13
00:01:38,960 --> 00:01:49,320
possessions. So of course using our possessions for pleasure, for indulgence, for distraction

14
00:01:49,320 --> 00:01:55,000
and diversion, for things that will take us away from the meditation practice and away from

15
00:01:55,000 --> 00:02:03,640
our goal. So in general the fits in with the practice of morality in the sense of restricting

16
00:02:03,640 --> 00:02:15,320
our activities and preventing us from becoming distracted and intoxicated by the world around us.

17
00:02:15,320 --> 00:02:22,200
So building on these fours as examples we use food simply to stay alive as I've talked

18
00:02:22,200 --> 00:02:31,000
about before. We won't use food for the purpose of entertainment or for the purpose of

19
00:02:31,000 --> 00:02:36,960
intoxication from the strength that comes from it or from fatening our bodies and from building

20
00:02:36,960 --> 00:02:46,000
up bulk or for beautifying the body for creating a good complexion. So we won't be concerned

21
00:02:46,000 --> 00:02:51,440
with these things. We'll use food simply to continue our lives, simply to allow us to continue

22
00:02:51,440 --> 00:02:57,520
with the spiritual path and so we'll try to use just the amount of food necessary for that

23
00:02:57,520 --> 00:03:02,440
and not going beyond that, not fall prey to our desires and our attachments, our addictions

24
00:03:02,440 --> 00:03:10,680
to food. Clothing will only use clothing to cover the body both to ward off heat and

25
00:03:10,680 --> 00:03:22,680
cold and insects and pests of all sorts and also to cover the body to keep us from being

26
00:03:22,680 --> 00:03:28,800
naked and from the distraction that causes and the problems that it causes from exposing

27
00:03:28,800 --> 00:03:39,240
ourselves. We won't use it for beautification, we won't use clothing for essential pleasure

28
00:03:39,240 --> 00:03:45,520
for indulging in the soft and comfortable touch. So we'll try to use just the sort of

29
00:03:45,520 --> 00:03:51,080
clothes that are necessary to keep us alive as monks. We try to perfect this by just

30
00:03:51,080 --> 00:03:56,560
wearing a simple cloth. It's a rectangular cloth and we have three rectangular cloths that

31
00:03:56,560 --> 00:04:02,520
we wear that's considered to be just enough to keep us reasonably comfortable without

32
00:04:02,520 --> 00:04:09,680
becoming luxurious or without engaging in some kind of beautification through clothing

33
00:04:09,680 --> 00:04:21,480
or style or enjoyment of color and beauty and so on. And the shelter we only use it for

34
00:04:21,480 --> 00:04:27,640
the purpose of protecting ourselves from the elements from heat and cold and for insects

35
00:04:27,640 --> 00:04:32,920
and pests and so on, but also for privacy. So we'll use it for the purpose, not privacy,

36
00:04:32,920 --> 00:04:38,640
but solitude. We'll use it for the purpose of segregating ourselves from all the things

37
00:04:38,640 --> 00:04:45,680
that would normally distract us. So when you have your own room, a place to stay or often

38
00:04:45,680 --> 00:04:50,160
the forest, if you have a place that is secluded, it can be a great support for your

39
00:04:50,160 --> 00:04:54,560
meditation, especially in the early stages when your mind is still easily distracted

40
00:04:54,560 --> 00:05:03,520
by the objects of the sense and people and activities going on in the world around.

41
00:05:03,520 --> 00:05:09,920
Now what it's not used for is for indulgence in things that you don't want to do in

42
00:05:09,920 --> 00:05:26,120
private, so any kind of entertainment or indulgence in sleeping and relaxation. So our

43
00:05:26,120 --> 00:05:31,240
houses, our rooms, our accommodation should be simple and actually quite bare. We should

44
00:05:31,240 --> 00:05:43,280
try our best to not use comfortable bedding or high in luxurious bedding or to use our,

45
00:05:43,280 --> 00:05:48,960
we should be careful because in solitude there also comes a tendency towards engaging

46
00:05:48,960 --> 00:05:55,640
in activities that you wouldn't want other people to know about. So try to use it for,

47
00:05:55,640 --> 00:06:00,600
simply for what it's meant to be used for and try to be careful not to fall prey

48
00:06:00,600 --> 00:06:14,040
to the comfort in the luxury of a beautiful, of a luxurious dwelling. And medicines as

49
00:06:14,040 --> 00:06:20,520
well, the fourth requisite we'll only use for overcoming sickness and only to the extent

50
00:06:20,520 --> 00:06:27,480
necessary to overcome sickness and to allow us a mind that is unclouded and free from

51
00:06:27,480 --> 00:06:35,720
the effects of sickness and disease and a body that is able to perform the meditation

52
00:06:35,720 --> 00:06:47,200
practice and able to function on a daily basis. So we wouldn't use medicines to cover

53
00:06:47,200 --> 00:06:57,120
up unpleasant experiences, for example, using painkillers or drugs that are simply for

54
00:06:57,120 --> 00:07:07,120
the purpose of creating states of comfort and avoiding unpleasant experiences or using

55
00:07:07,120 --> 00:07:12,840
medicines like supplements that are meant simply to strengthen us and to give us a false

56
00:07:12,840 --> 00:07:24,920
sense of health and comfort in order to avoid the inevitability of sickness and pain

57
00:07:24,920 --> 00:07:30,960
and so on. So again, trying to use all of these things just for what is necessary.

58
00:07:30,960 --> 00:07:35,000
And using these examples, you can really apply this to anything. So if you have a car,

59
00:07:35,000 --> 00:07:39,040
you should be using the car only for what is necessary. If you have a house, the house

60
00:07:39,040 --> 00:07:43,680
should only be for what is necessary. If you have a computer, the computer should only

61
00:07:43,680 --> 00:07:51,600
be used as necessary. Why is reflection at the use of our requisites? We should take

62
00:07:51,600 --> 00:07:57,960
a look around ourselves and try to eliminate those things that those possessions, those

63
00:07:57,960 --> 00:08:02,520
material possessions that are distracting us, that are taking us away from the path, away

64
00:08:02,520 --> 00:08:09,720
from the practice. Because it's just another outlet that takes us outside of what would

65
00:08:09,720 --> 00:08:14,720
be considered moral. But again, moral in the sense of supportive for the meditation practice

66
00:08:14,720 --> 00:08:19,800
leading towards concentration and focus. So anything that takes us outside of that, we

67
00:08:19,800 --> 00:08:24,880
consider to be immoral. It's just a subjective definition. But in a Buddhist context,

68
00:08:24,880 --> 00:08:31,520
this is what we mean by Sila or morality. So there we go. Another aspect of morality

69
00:08:31,520 --> 00:08:38,560
and just one more concept that we need to know as Buddhists, as Buddhist meditators, as followers

70
00:08:38,560 --> 00:09:08,400
of the Buddha's teaching. So thank you for tuning in, have a good day.

