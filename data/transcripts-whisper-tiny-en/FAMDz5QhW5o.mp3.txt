Would anything cause you to disrobe? Do you see any benefit to the lay life?
Hmm, they're the kind of two different questions.
Would anything cause me to disrobe?
I have seen circumstances where people were forced to disrobe.
People who had no interest in the lay life were forced to disrobe,
generally dealing with their parents.
One really impressive man in Thailand,
who was a monk for 17 years and a very strong meditator.
And by all appearances seems to be,
perhaps in one of the four enlightened types of beings.
Probably not in Arahat because they say that requires ordination.
But certainly a very strong meditator and a very wise individual.
And he collects garbage and sells garbage in Thailand,
sells recycled, recyclables,
which is actually a fairly lucrative trade.
But he just gets on his motorcycle with a side car or a trailer or something.
And he rides around looking for garbage to pick up and to sell or to trade or whatever.
But he had disrobe because of his mother.
I can't remember the exact story, but his mother was sick and couldn't take care of herself.
And he was her only son.
And so he felt he had disrobe.
And he lives his life celibate and alone and a very simple life.
He cooks food for his mother and then goes off to work and comes back in the evening.
And they say he's called rice.
And so it's quite a noble life.
So what I mean to say is that even in his case,
where he was obviously someone who was quite advanced,
he still felt the need to disrobe.
So I can't say that nothing would cause me to disrobe.
But I think there are two pertinent questions because it points to the fact that
you would have to see something, some benefit in the lay life to disrobe.
People tend to make a mistake about disrobing that it's giving up something.
You're giving up the path.
It's not a mistake, but it's really only one way of looking at the issue.
Because disrobing doesn't just mean taking off the robes.
I mean if it just means taking off the robes,
you become a naked aesthetic or something or become an animal again.
But it means taking off the robes and generally means putting on another set of culturally
and role-specific clothing.
Because when you put on pants, it really does have quite a meaning.
The pants are not the default and the t-shirt or whatever it is that you end up wearing.
Or not the default.
In fact, you might think that robes are much more a default clothing.
And that's why the Buddha chose them.
So to become a lay person again,
I would think you would have to see some benefit in the lay life.
You would have to think that some other was a benefit to lay life.
Do I see any benefit in the lay life?
Besides the benefit that I just mentioned, taking care of your parents,
which I'm still skeptical of because in many cases you can,
as a monk, take care of your parents.
You can't do everything for them.
But for myself, I wouldn't probably have such a concern.
I can't say that.
You never know what the future is going to bring.
But there are always ways to even as a monk to support your parents.
For example, given that I have a number of students who appreciate what I do
and wish for me to continue doing what I do,
I can explain to them if there is a desire for me to remain a monk.
Then maybe I can have some people help me to help my parents.
This is an example.
So in my case, it's not that pressing of a concern,
or not that big of a worry.
I might someday want to go back to Canada to be closer to them.
I'm always thinking of that,
but that's, I think, another story.
Obviously it wouldn't be to disroven,
and I don't have any thought in that direction.
But seeing a benefit, right, besides taking care of your parents,
seeing a benefit in the lay life, no,
no, I don't see any benefit in the lay life.
It really speaks of where your mind is.
The point is that personally, I don't see a great benefit in, for example,
supporting or contributing to society,
because I think maybe this is something that keeps people as lay people
and makes them disroven, because they want to go
and do good things in the world, and they feel like as a monk,
or a nun that they're not able to do that,
which I think is really, or I don't see things in that way.
So as a result, I probably wouldn't follow such a thought.
Because from my point of view,
the best thing that helps people the most is meditation,
and the practice of the Buddha's teaching.
I mean, even today, just the wonderful things that went on in this ceremony,
again and again, it's such a wonderful thing to give,
and to be a part of giving, and even not to give,
but to rejoice in the giving of others,
to watch other people give and be generous and be kind and be helpful.
That's just a wonderful thing, and it's a wonderful thing that becomes so very powerful
in a Buddhist context, because it's our spiritual practice.
Of course, it would be the same in, I suppose, in a Christian context,
or Jewish context, whatever context,
Muslim context, the Hindu context is giving.
Well, of course, given our belief or our view,
our opinion that the Buddha's teaching as much is the purest.
Of course, because as very much the core of the Buddha's teaching
is to do good deeds and to avoid bad deeds.
There's no overhead.
There's no extraneous.
No extraneous views or opinions or beliefs or practices.
Practice is really to make yourself a better person,
to make your mind more pure by giving gifts and by keeping morality.
This is really our spiritual practice.
This is the wonderful thing,
is that this is people getting together to do this in a spiritual sense.
You can only really get that in a community like this,
or it becomes much more powerful in a community like this,
in a monastic community.
There's no question in my mind that the benefit of being a monk is the greater benefit
to oneself and to others, to the whole world.
Even just as an example, even just as someone,
it's a very powerful thing to be an example,
to see someone doing good things,
to see someone meditating.
When we're sitting around and suddenly we see one of us is meditating.
We all remember, oh yes,
it brings us back to our meditation,
but it can really change your life.
And of course, as changed many people's lives,
just to see other people ordained.
When we go on arms round,
or when people come to the monastery and see us, they're so happy.
And it really changes people.
So I don't see benefit because I'm not really concerned about society,
or the direction that it's taking,
or the benefits towards society.
I'm concerned, I suppose, or concerned,
I'm interested, or I don't know how to put it correctly.
I enjoy, I suppose, and appreciate,
and do as a matter of course,
try to help people, help humanity.
But I don't think that has anything to do with the lay life.
I don't think the lay life is anything.
The benefits people, because of course,
when you put on the pants,
when you put on the shirt,
when you go to get a job,
you're taking part in many of the things that we see as being the problem,
the consumerism and materialism.
And just so much uselessness that goes on out there,
even just our interaction with people who come here,
the comparison, what they get from us,
and what we pick up from them is really two different things.
So what we get from them is their greed and their stress
and their frustrations and sadness
and suffering that they have in their lives
and living in the world.
And when you compare it with the peace and the happiness
and the simplicity of the life that we lead,
it's really not difficult to make a choice.
I would say the biggest reason,
the only really powerful reason
that leads people to disrupt is sensuality
or sexuality to put it bluntly.
I suppose you could expand that to sensuality.
Some people are just attached to their food,
for example.
Funny how silly people can be wanting this food
or wanting that food and knowing that
as a monk, it's not convenient to get it,
entertainment and so on.
But at the core, I would say sexuality
and it's something that leads people to not be able to ordain.
Now, I've, you know, been down there that road
and I'm happy to discuss that.
The question of sexuality, it's really a,
that's one of the more wonderful benefits
of being a Buddhist monk
is to look objectively at the sexual desire
and the sexual impulse
and to come to break it up as I've said in other videos
and to see the pieces of it,
to see the pleasure
and to finally,
because even as laypeople we repress
and we feel guilty about our sexual desires
and it's something that you have to do in private
and something that you have to hide.
But we still, even though we might say
a Buddhist monk is someone who's repressing these desires,
it's actually the opposite.
You finally have a forum in which you can approach these,
these issues.
You know, you can sit in your kutti
and you can, you know, not have to feel guilty
and say, you know, I know I'm not going to follow it,
I'm just sitting here and I have this desire
and so on.
And so actually it winds up being the opposite.
But you know, finally you have a chance to say,
yes, I have this sexual desire,
I have this pleasure arising in the body,
all these chemical reactions and so on.
Anyway, that's not really what the question was about,
but for myself I think I've been lucky
to have good teachers and I'm able to look,
honestly, at the issues that are inside myself.
So, you know, these issues that where you might say,
I know it's wrong, but I have to disrupt.
It seems to me that when you look,
you really look at them and when you are honest about them
that they'll cease to become a problem
and they simply become another object of meditation.
So, a good question and these questions are always interesting,
I think, for people because there's a lot of people out there
looking to ordain or, you know, sorry,
considering ordination.
So, you know, interesting to talk about.
And these are also the videos that become very contentious
because half the people say wonderful,
wonderful, I agree completely.
And other people say, how can you denounce society and lay life?
And the Buddha said that lay people
could become enlightened as well and so on and so on.
So, there's nothing wrong with being a lay person
and you can practice meditation,
but there's no question that from Buddhist point of view,
the monastic life or the simple meditative life
is to be, you know, you could say,
really could say the monastic life is of greater benefit.
So, you're giving up something that is of lesser benefit
or is of lesser use or is less conducive
for the practice of meditation
because, of course, the monastic community has its benefits as well
even though lay people can live meditative simple lives.
It's very easy for them to get off track
and to get lost because of the lack of discipline
and community which is gained from being a Buddhist monastic.
So, those are my thoughts.
Just some thoughts.
