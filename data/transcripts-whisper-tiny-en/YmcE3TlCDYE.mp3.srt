1
00:00:00,000 --> 00:00:15,000
Good evening, everyone. Welcome to our evening done this session.

2
00:00:15,000 --> 00:00:21,200
So last night we talked about mindfulness. We thought it would be good to follow that

3
00:00:21,200 --> 00:00:28,200
up with the other half of the equation, which is Vipasana.

4
00:00:28,200 --> 00:00:38,200
Once the other half of the equation is we describe this technique, the tradition that I follow

5
00:00:38,200 --> 00:00:50,200
as Sati Patana Vipasana. Mahasi Sayyodha was maybe one of the first to use that phrase.

6
00:00:50,200 --> 00:00:59,200
He used it in his book called Sati Patana Vipasana.

7
00:00:59,200 --> 00:01:12,200
Whenever you take a course with my teacher in Thailand, you'll have you actually memorize a phrase.

8
00:01:12,200 --> 00:01:22,200
Vipasana, Nainya, Sati Patana. Vipasana in line with the four Sati Patana, four foundations

9
00:01:22,200 --> 00:01:34,200
of mindfulness. One body, two feelings, three mind, four dumbah.

10
00:01:34,200 --> 00:01:37,200
You repeat that. So you get it through your head.

11
00:01:37,200 --> 00:01:44,200
What are we practicing here?

12
00:01:44,200 --> 00:01:54,200
Vipasana is an interesting word, or it's interesting what is said about it.

13
00:01:54,200 --> 00:02:05,200
I often remember being told that Vipasana, you can't find Vipasana in the teptica.

14
00:02:05,200 --> 00:02:09,200
A monk once told me that.

15
00:02:09,200 --> 00:02:15,200
Now, for just looking for the word Vipasana, you can, of course, find that word in the teptica.

16
00:02:15,200 --> 00:02:20,200
It's in the Buddha's words.

17
00:02:20,200 --> 00:02:35,200
That you don't have the various explanations and the frameworks that have been made up to describe and teach Vipasana.

18
00:02:35,200 --> 00:02:46,200
You don't have that really in the Buddha's words, or what we have of the Buddha's words.

19
00:02:46,200 --> 00:02:53,200
But Vipasana is a concept, or as a thing.

20
00:02:53,200 --> 00:02:56,200
It's very much a part of the Buddha's teaching.

21
00:02:56,200 --> 00:03:07,200
I think it's fairly easy if you have your familiar with the texts to see why the later traditions have stressed this word.

22
00:03:07,200 --> 00:03:13,200
They've used this word as a catch phrase to describe what we're practicing.

23
00:03:13,200 --> 00:03:19,200
Nowadays, we hear about Vipasana meditation or insight meditation.

24
00:03:19,200 --> 00:03:26,200
As I've said, it's not really that accurate. You don't practice Vipasana.

25
00:03:26,200 --> 00:03:31,200
You practice Satipatana, you practice mindfulness.

26
00:03:31,200 --> 00:03:42,200
But we call it Vipasana meditation, or if we call it Vipasana meditation, because this is what happens when you practice mindfulness.

27
00:03:42,200 --> 00:03:48,200
You come, you call it Vipasana, you come to see clearly.

28
00:03:48,200 --> 00:03:54,200
So anyone who says that this isn't, this is a later invention, or it's not something the Buddha came up with.

29
00:03:54,200 --> 00:04:02,200
It doesn't really have a familiarity with the texts or hasn't really thought deeply, but what the texts are actually saying.

30
00:04:02,200 --> 00:04:21,200
Vipasana comes up quite often, but more than that, you find again and again these phrases whenever the Buddha is talking about, or often when he's talking about cultivating wisdom or seeing things as they are.

31
00:04:21,200 --> 00:04:31,200
He'll say things like, a dita nanwakami and a patikankay and a gatan. But jupyana jayodamang tata tata vipasati.

32
00:04:31,200 --> 00:04:34,200
Don't go back to the past, don't worry about the future.

33
00:04:34,200 --> 00:04:38,200
What's in the past is gone, what's in the future, hasn't come.

34
00:04:38,200 --> 00:04:52,200
Whatever arises in the present moment in front of you, see that clearly. Vipasati.

35
00:04:52,200 --> 00:05:09,200
Again, this word Vipasati, Vipasati is the verb, means he or she sees clearly, or however you want to translate Vipasana.

36
00:05:09,200 --> 00:05:16,200
Blind is this world, and the Buddha and the local, this world is blind, very few see clearly.

37
00:05:16,200 --> 00:05:26,200
That's what mindfulness is supposed to do.

38
00:05:26,200 --> 00:05:34,200
Again and again, you have the Buddha talking about seeing clearly or seeing with wisdom.

39
00:05:34,200 --> 00:05:42,200
When we talk about what is Vipasana, we often simplify it by saying Vipasana means to see three things.

40
00:05:42,200 --> 00:05:51,200
Because the Buddha said, sabbay, sankara, anitya, di, yada, panya, yapasati, whoever sees with wisdom.

41
00:05:51,200 --> 00:06:02,200
When one sees with wisdom that all formations are impermanent, sabbay, sankara, anitya.

42
00:06:02,200 --> 00:06:14,200
Sabbay, sankara, dukha, all formations are dukha, which means suffering or unsatisfying as well.

43
00:06:14,200 --> 00:06:38,200
Sabbay, dhamma, anitya, all dhammas, all realities formed or unformed or not self.

44
00:06:38,200 --> 00:06:52,200
So, it's actually a figure of speech, and it makes sense that this wouldn't be something the Buddha used chiefly.

45
00:06:52,200 --> 00:07:16,200
The Buddha used the word, or phrases, or the construct, panya, or panjana, tea.

46
00:07:16,200 --> 00:07:23,200
But it means that panjana means to know panjana means fully.

47
00:07:23,200 --> 00:07:26,200
So, more often, the Buddha uses the word, no.

48
00:07:26,200 --> 00:07:36,200
Vipasana means, or pasana is a word that is when you see something, when you look at something and you see it.

49
00:07:36,200 --> 00:07:46,200
But what's useful about it, of course, even in some ways more than wisdom, is that it reminds us that this isn't intellectual.

50
00:07:46,200 --> 00:07:55,200
You have to see with wisdom, but you have to see for yourself another verb the Buddha uses is, satchikaroti.

51
00:07:55,200 --> 00:08:20,200
Satchikaroti means to see for oneself, to know independently of belief or rationalization, logic, to really know for oneself, to experience really.

52
00:08:20,200 --> 00:08:26,200
But so, what do we mean when we talk about Vipasana? What it refers to?

53
00:08:26,200 --> 00:08:32,200
So, in brief, it refers to seeing the three characteristics, impermanence, suffering, non-self.

54
00:08:32,200 --> 00:08:46,200
This is what Vipasana meditation should show you, and it's the way it's phrased often without much explanation.

55
00:08:46,200 --> 00:08:55,200
You can make this teaching a little confusing. It's not like you're going to look, and you're going to say, oh, look, impermanence jumps out at you.

56
00:08:55,200 --> 00:09:12,200
What it means is that our attachment to people places things, to pleasant experiences, our aversion to unpleasant experience, it comes from misunderstanding about reality.

57
00:09:12,200 --> 00:09:19,200
We take certain things as stable, constant, lasting.

58
00:09:19,200 --> 00:09:25,200
We take them as satisfying, or fixable.

59
00:09:25,200 --> 00:09:36,200
We have the idea that by clinging, by fretting, by obsessing over things, we can find happiness.

60
00:09:36,200 --> 00:09:47,200
If we work with our experiences, if we fix them just right, we can find happiness.

61
00:09:47,200 --> 00:10:00,200
And we believe that there is substance, and there is control, and there is a self, and a soul, and a possessor.

62
00:10:00,200 --> 00:10:06,200
All these things having to do with self, which is really a complex set of views.

63
00:10:06,200 --> 00:10:16,200
But it relates to the idea that there is some essence to things, like people actually existing.

64
00:10:16,200 --> 00:10:29,200
Or like bodies being under our control. I can move my arms and so on.

65
00:10:29,200 --> 00:10:35,200
I think we're in control, because look at me, I'm moving my arms.

66
00:10:35,200 --> 00:10:44,200
And so all of these turn out to be based on a poor grasp of reality.

67
00:10:44,200 --> 00:10:51,200
And when you focus on reality, moment to moment, you start to say it's not quite the way we think it is.

68
00:10:51,200 --> 00:10:58,200
That both the body and the mind are very much impermanent, and the world around us, very much impermanent.

69
00:10:58,200 --> 00:11:02,200
We went through this today in our study group.

70
00:11:02,200 --> 00:11:06,200
I think that was a really good section that we went through, because details.

71
00:11:06,200 --> 00:11:08,200
What do you mean by impermanence?

72
00:11:08,200 --> 00:11:10,200
This comes and it's gone.

73
00:11:10,200 --> 00:11:13,200
When the next thing comes, that doesn't come.

74
00:11:13,200 --> 00:11:16,200
The last thing doesn't come over. It's gone.

75
00:11:16,200 --> 00:11:26,200
Every moment, when you're hungry, you experience the physical reality,

76
00:11:26,200 --> 00:11:30,200
it's very harsh and unpleasant and ugly.

77
00:11:30,200 --> 00:11:37,200
Everything is the physical form, it's unpleasant.

78
00:11:37,200 --> 00:11:45,200
When you're satiated, everything is wonderful, and the physical form is comfortable.

79
00:11:45,200 --> 00:11:47,200
It's pleasing.

80
00:11:47,200 --> 00:11:58,200
It says, when you're aware of this, you can see how reality changes from moment to moment,

81
00:11:58,200 --> 00:12:01,200
and how we experience it one moment.

82
00:12:01,200 --> 00:12:07,200
It's very different from how we experience it the next.

83
00:12:07,200 --> 00:12:09,200
Impermanence.

84
00:12:09,200 --> 00:12:15,200
Start to see that, thereby we can't find satisfaction or real happiness

85
00:12:15,200 --> 00:12:19,200
by clinging to things or by trying to fix things.

86
00:12:19,200 --> 00:12:23,200
Because they're impermanent, but also because third, they're not self.

87
00:12:23,200 --> 00:12:26,200
They're not under control.

88
00:12:26,200 --> 00:12:29,200
So you think it's you moving your arms whenever you want.

89
00:12:29,200 --> 00:12:34,200
While trying to try watching a simple movement, and we watch the stomach,

90
00:12:34,200 --> 00:12:36,200
rising and falling.

91
00:12:36,200 --> 00:12:39,200
Beginning, we think I'm the one making it rise and fall,

92
00:12:39,200 --> 00:12:42,200
but as you watch and watch.

93
00:12:42,200 --> 00:12:47,200
And when you walk, and you think I am walking, I'm making myself walk.

94
00:12:47,200 --> 00:12:50,200
But when you really watch and see what's going on, you see, oh, I see.

95
00:12:50,200 --> 00:12:57,200
There's actually just this interplay between body and mind, moments of mind's mind and body state.

96
00:12:57,200 --> 00:13:00,200
There's no self involved.

97
00:13:00,200 --> 00:13:10,200
And there you come to see how constructed and artificial it always.

98
00:13:10,200 --> 00:13:15,200
This is the sort of thing that you start to let go.

99
00:13:15,200 --> 00:13:18,200
When you see clearly in this way,

100
00:13:18,200 --> 00:13:23,200
you start to see how the word sankara would actually mean.

101
00:13:23,200 --> 00:13:26,200
It means that our reality is constructed.

102
00:13:26,200 --> 00:13:30,200
It's like an artificial, it's like a robot.

103
00:13:30,200 --> 00:13:38,200
We are like robots, constructs, and you become disenchanted

104
00:13:38,200 --> 00:13:42,200
at the moment that they do okay.

105
00:13:42,200 --> 00:13:47,200
So the second thing that we pass in a means is not just the three characteristics.

106
00:13:47,200 --> 00:13:55,200
It also refers to this process of realization that comes as you start to see the three characteristics.

107
00:13:55,200 --> 00:13:59,200
So it's a real shift because, you know,

108
00:13:59,200 --> 00:14:03,200
much of what I'm saying might sound foreign to many of you.

109
00:14:03,200 --> 00:14:06,200
This is the kind of thing that should sound wrong.

110
00:14:06,200 --> 00:14:09,200
I mean, the idea that the body isn't under control,

111
00:14:09,200 --> 00:14:11,200
the idea that things aren't stable,

112
00:14:11,200 --> 00:14:15,200
the idea that you can't find satisfaction or happiness in the world.

113
00:14:15,200 --> 00:14:17,200
This shouldn't sound quite wrong to us.

114
00:14:17,200 --> 00:14:20,200
I mean, it's normal for that to be the case

115
00:14:20,200 --> 00:14:25,200
because our ordinary understanding of reality is very different.

116
00:14:25,200 --> 00:14:31,200
We think, you know, some sara is wonderful.

117
00:14:31,200 --> 00:14:40,200
So much, so much to be found, so much to be sought out and gained.

118
00:14:40,200 --> 00:14:44,200
But as you make this process so when you first step through the door

119
00:14:44,200 --> 00:14:49,200
and you start to see things clearly, we pass in by using mindfulness,

120
00:14:49,200 --> 00:14:54,200
by being objective, you know, you're not indoctrinating yourself

121
00:14:54,200 --> 00:15:00,200
or taking up a perverse practice or a cultish practice.

122
00:15:00,200 --> 00:15:04,200
You're just trying to see things objectively when you are perfectly

123
00:15:04,200 --> 00:15:09,200
and strictly objective and honest with yourself.

124
00:15:09,200 --> 00:15:10,200
Why does it change?

125
00:15:10,200 --> 00:15:17,200
And so you go through a series of knowledges which, you know,

126
00:15:17,200 --> 00:15:19,200
they're very much the same.

127
00:15:19,200 --> 00:15:21,200
They go in the same line for everyone,

128
00:15:21,200 --> 00:15:23,200
and that's why you can document them.

129
00:15:23,200 --> 00:15:26,200
You can talk about the stages of insight.

130
00:15:26,200 --> 00:15:28,200
Some people are, I think, rather skeptical.

131
00:15:28,200 --> 00:15:31,200
It's always quite skeptical until I saw how it works.

132
00:15:31,200 --> 00:15:34,200
You know, everyone is different and they won't all manifest the same,

133
00:15:34,200 --> 00:15:38,200
but it's quite interesting how the mind goes through a progression,

134
00:15:38,200 --> 00:15:42,200
but it's basically the progression of moving from seeing things as stable,

135
00:15:42,200 --> 00:15:47,200
satisfying, and controllable to see them as an impermanent,

136
00:15:47,200 --> 00:15:52,200
unsatisfying, and controlled.

137
00:15:52,200 --> 00:15:57,200
And so it starts by seeing this discreetness of experience

138
00:15:57,200 --> 00:16:05,200
as they arise and see if this one doesn't continue on to the next one.

139
00:16:05,200 --> 00:16:12,200
And then you start to investigate that,

140
00:16:12,200 --> 00:16:20,200
and the mind starts to appreciate this discreetness,

141
00:16:20,200 --> 00:16:22,200
especially the cessation aspect.

142
00:16:22,200 --> 00:16:31,200
And the mind starts to realize that given that this discreetness,

143
00:16:31,200 --> 00:16:35,200
it means nothing lasts, it means nothing stays.

144
00:16:35,200 --> 00:16:37,200
There's nothing that is a stable refuge,

145
00:16:37,200 --> 00:16:45,200
and it starts to kind of obsess or really observe the cessation

146
00:16:45,200 --> 00:16:50,200
that begins to see how unstable reality is.

147
00:16:50,200 --> 00:16:53,200
It can be quite frightening at times,

148
00:16:53,200 --> 00:16:56,200
but the mind starts to wake up.

149
00:16:56,200 --> 00:17:02,200
The meditator begins to alert, become alert to the danger,

150
00:17:02,200 --> 00:17:05,200
like standing on, walking on thin ice.

151
00:17:05,200 --> 00:17:07,200
I thought this was a solid platform.

152
00:17:07,200 --> 00:17:09,200
This body, this reality.

153
00:17:09,200 --> 00:17:11,200
As you start to see everything cease,

154
00:17:11,200 --> 00:17:13,200
you start to wonder, what can I hold on to?

155
00:17:13,200 --> 00:17:16,200
And the mind is grasping for something to cling to.

156
00:17:16,200 --> 00:17:19,200
And you begin to see that nothing's worth clinging to.

157
00:17:19,200 --> 00:17:24,200
You see how the clinging is causing you stress and suffering.

158
00:17:24,200 --> 00:17:26,200
It's causing you stress and suffering because nothing,

159
00:17:26,200 --> 00:17:29,200
nothing that you're clinging to is stable, satisfying,

160
00:17:29,200 --> 00:17:32,200
or controllable.

161
00:17:32,200 --> 00:17:37,200
And that's when Nibida comes as you start to become disenchanted.

162
00:17:37,200 --> 00:17:40,200
And as you become disenchanted, you start to feel trapped.

163
00:17:40,200 --> 00:17:44,200
You really feel this constructed nature,

164
00:17:44,200 --> 00:17:48,200
this artificial nature, everything feels like a trap.

165
00:17:48,200 --> 00:17:52,200
Your body is a trap, the mind is a trap.

166
00:17:52,200 --> 00:17:55,200
You feel like you're caught in the cage.

167
00:17:55,200 --> 00:17:58,200
When you start to incline away, like a snake,

168
00:17:58,200 --> 00:18:01,200
you may use this simile of a snake,

169
00:18:01,200 --> 00:18:03,200
shedding off its skin.

170
00:18:03,200 --> 00:18:07,200
The skin starts to feel kind of dead.

171
00:18:07,200 --> 00:18:13,200
A fake, no?

172
00:18:13,200 --> 00:18:15,200
And then one strives.

173
00:18:15,200 --> 00:18:18,200
It's the higher stages of knowledge, and then one is on the path.

174
00:18:18,200 --> 00:18:22,200
And there's no question if they continue where they're going to go.

175
00:18:22,200 --> 00:18:28,200
Because at that point, they've got this unshakable conviction

176
00:18:28,200 --> 00:18:32,200
based on strong observation.

177
00:18:32,200 --> 00:18:35,200
Until eventually they become completely equanimous.

178
00:18:35,200 --> 00:18:37,200
And the final stage of insight,

179
00:18:37,200 --> 00:18:41,200
where we get to the peak of Fipasana,

180
00:18:41,200 --> 00:18:45,200
is when you really see clearly, and you see that everything,

181
00:18:45,200 --> 00:18:47,200
nothing is worth clinging.

182
00:18:47,200 --> 00:18:49,200
You use clinging to.

183
00:18:49,200 --> 00:18:52,200
You see everything simply as experiences that arise.

184
00:18:57,200 --> 00:18:59,200
And that's the last of the Fipasana stages,

185
00:18:59,200 --> 00:19:02,200
where you have perfect equanimity.

186
00:19:02,200 --> 00:19:05,200
Doesn't mean you enter into a state where you're perfectly calm.

187
00:19:05,200 --> 00:19:10,200
It means everything you experience, you see clearly as arising.

188
00:19:10,200 --> 00:19:15,200
And ceasing, not as good or bad or me or mine.

189
00:19:15,200 --> 00:19:20,200
But you become more and more refined in your observation.

190
00:19:20,200 --> 00:19:25,200
There's no more reacting, no very little liking or disliking.

191
00:19:25,200 --> 00:19:31,200
Until eventually, it's like this feedback loop

192
00:19:31,200 --> 00:19:33,200
where it gets stronger and stronger.

193
00:19:33,200 --> 00:19:35,200
And just because it's more and more refined,

194
00:19:35,200 --> 00:19:40,200
you need mindfulness to build up such strength

195
00:19:40,200 --> 00:19:42,200
that it's like a sonic boom,

196
00:19:42,200 --> 00:19:45,200
the sound waves building up, building up, building up.

197
00:19:45,200 --> 00:19:48,200
Because you're seeing clear and clear and clear.

198
00:19:48,200 --> 00:19:51,200
To the point where there's the epiphany moment

199
00:19:51,200 --> 00:19:54,200
where one sees the four noble truths.

200
00:19:54,200 --> 00:19:57,200
One sees everything.

201
00:19:57,200 --> 00:20:00,200
Everything in some sorrow.

202
00:20:00,200 --> 00:20:05,200
The desire is worth clinging to.

203
00:20:05,200 --> 00:20:08,200
And one thus turns away from it.

204
00:20:08,200 --> 00:20:11,200
And one the mind withdraws.

205
00:20:11,200 --> 00:20:14,200
It's like it drops away.

206
00:20:14,200 --> 00:20:18,200
Completely relinquishes its hold on some sorrow

207
00:20:18,200 --> 00:20:31,200
and enters into an Obama cessation.

208
00:20:31,200 --> 00:20:34,200
Which is, of course, the ultimate goal of epipassana.

209
00:20:34,200 --> 00:20:37,200
Epipassana is not for epipassana.

210
00:20:37,200 --> 00:20:40,200
So our goal is not our actual goal isn't to see clearly.

211
00:20:40,200 --> 00:20:45,200
Seeing clearly is only the stepping stone for becoming free.

212
00:20:45,200 --> 00:20:49,200
Epipassana is this freedom from suffering.

213
00:20:49,200 --> 00:20:53,200
That it's very hard to describe or understand.

214
00:20:53,200 --> 00:20:56,200
It's the kind of thing you really have to experience for yourself,

215
00:20:56,200 --> 00:20:59,200
even for just a moment.

216
00:20:59,200 --> 00:21:01,200
And it changes you.

217
00:21:01,200 --> 00:21:06,200
It changes you something drastic.

218
00:21:06,200 --> 00:21:08,200
Such peace and relief.

219
00:21:08,200 --> 00:21:12,200
And you find that you're all the things that you cling to.

220
00:21:12,200 --> 00:21:16,200
Not all of them, but many of them.

221
00:21:16,200 --> 00:21:18,200
You relinquish your hold.

222
00:21:18,200 --> 00:21:22,200
You relinquish your ambition

223
00:21:22,200 --> 00:21:26,200
for getting, for obtaining, for being,

224
00:21:26,200 --> 00:21:31,200
for fixing, for controlling, for some sorrow.

225
00:21:31,200 --> 00:21:39,200
You've given up your attachment or you've reduced it.

226
00:21:39,200 --> 00:21:42,200
So that's epipassana.

227
00:21:42,200 --> 00:21:44,200
Epipassana is this practice.

228
00:21:44,200 --> 00:21:46,200
Epipassana is really why we focus on it.

229
00:21:46,200 --> 00:21:48,200
It is because it's this progression.

230
00:21:48,200 --> 00:21:52,200
It comes from the practice of mindfulness.

231
00:21:52,200 --> 00:21:54,200
Mindfulness, we put first.

232
00:21:54,200 --> 00:21:55,200
It's what we practice.

233
00:21:55,200 --> 00:21:57,200
But what happens when you're mindful,

234
00:21:57,200 --> 00:21:59,200
it's not just, oh, now I'm mindful.

235
00:21:59,200 --> 00:22:02,200
Now I'm going to live my life in a better way.

236
00:22:02,200 --> 00:22:04,200
No, you're going to change quite a bit.

237
00:22:04,200 --> 00:22:09,200
Change the process of change is vipassana

238
00:22:09,200 --> 00:22:12,200
when you guys you come to see clearly.

239
00:22:12,200 --> 00:22:16,200
Well, many things about you and your life will change for the better.

240
00:22:16,200 --> 00:22:18,200
The none for the worse.

241
00:22:18,200 --> 00:22:22,200
It's not like you have to doubt about it.

242
00:22:22,200 --> 00:22:24,200
In fact, it's not something that,

243
00:22:24,200 --> 00:22:27,200
it's not the kind of change that you have to decide on.

244
00:22:27,200 --> 00:22:29,200
It doesn't take a decision because it's all clear.

245
00:22:29,200 --> 00:22:35,200
It's all unequivocally unarguably right.

246
00:22:35,200 --> 00:22:40,200
The change is from your view through seeing clearly

247
00:22:40,200 --> 00:22:43,200
without a doubt, proper course of action.

248
00:22:43,200 --> 00:22:45,200
So after you've seen clearly, for someone to say,

249
00:22:45,200 --> 00:22:47,200
are you sure?

250
00:22:47,200 --> 00:22:50,200
That doubt doesn't arise in the mind.

251
00:22:50,200 --> 00:22:53,200
You're absolutely sure because you've seen

252
00:22:53,200 --> 00:22:57,200
far better than any argument or any text or any teaching could do.

253
00:22:57,200 --> 00:23:02,200
You've seen clearly the right and the wrong way.

254
00:23:02,200 --> 00:23:06,200
You've seen that which leads to happiness and that which leads to suffering.

255
00:23:06,200 --> 00:23:08,200
That's what it means to see clearly.

256
00:23:08,200 --> 00:23:12,200
That's what vipassana is.

257
00:23:12,200 --> 00:23:14,200
And that's the demo for tonight.

258
00:23:14,200 --> 00:23:16,200
So thank you all for tuning in.

259
00:23:16,200 --> 00:23:31,200
We'll show you all the best.

