1
00:00:00,000 --> 00:00:08,000
Bante, could you please tell us how you would meet criticism regarding rebirth from modern materialists such as Richard Dawkins?

2
00:00:08,000 --> 00:00:14,000
Okay, here's one that we can all sink our teeth into Richard Dawkins.

3
00:00:14,000 --> 00:00:19,000
I'll go on the record saying I don't really, I'm not really fond of Richard Dawkins.

4
00:00:19,000 --> 00:00:26,000
I like some of the things he says, but if you look at, I don't know, this is an ad hominem, isn't it?

5
00:00:26,000 --> 00:00:31,000
But there is something from a Buddhist perspective to...

6
00:00:31,000 --> 00:00:34,000
It's not answering the question at all, really.

7
00:00:34,000 --> 00:00:36,000
It's hopefully kind of sitting with you.

8
00:00:36,000 --> 00:00:43,000
Is that it's sad, really, to see how materialism destroys someone's spiritual qualities,

9
00:00:43,000 --> 00:00:48,000
makes them seem very, very coarse and unpleasant in a sense.

10
00:00:48,000 --> 00:00:55,000
I mean, to me, as refined as he sounds, it's clear that he's not a very refined individual from my brain.

11
00:00:55,000 --> 00:00:58,000
But I'd like to open it up to other people.

12
00:00:58,000 --> 00:01:07,000
So it seems that Richard Dawkins had a very tough English and childhood, you know?

13
00:01:07,000 --> 00:01:10,000
Honestly.

14
00:01:10,000 --> 00:01:19,000
But the really difficult thing is to convince or to show materialists that they're holding a faith,

15
00:01:19,000 --> 00:01:29,000
that they believe in material, physical world is a belief that is not founded in actual experience.

16
00:01:29,000 --> 00:01:31,000
Or physics.

17
00:01:31,000 --> 00:01:36,000
Well, physics is all based on assumptions and theories that are not.

18
00:01:36,000 --> 00:01:43,000
No, but I mean, modern physics shows that modern physics seems to suggest barring a multiverse or so on,

19
00:01:43,000 --> 00:01:49,000
or anywhere worlds that are not perceived.

20
00:01:49,000 --> 00:01:54,000
It seems to suggest that it is the case that entities don't exist, and it's all based on...

21
00:01:54,000 --> 00:01:58,000
To some extent, based on it's true.

22
00:01:58,000 --> 00:01:59,000
Yeah.

23
00:01:59,000 --> 00:02:00,000
Well, how?

24
00:02:00,000 --> 00:02:01,000
Believe it, yeah.

25
00:02:01,000 --> 00:02:06,000
If you really want to look at it, material form doesn't even actually...

26
00:02:06,000 --> 00:02:13,000
Color doesn't even actually exist.

27
00:02:13,000 --> 00:02:17,000
It's what we perceive as color, but really in the outer world,

28
00:02:17,000 --> 00:02:21,000
it's just vibrations being reflected off of surfaces.

29
00:02:21,000 --> 00:02:22,000
It had...

30
00:02:22,000 --> 00:02:23,000
There is no color.

31
00:02:23,000 --> 00:02:25,000
We interpret the world.

32
00:02:25,000 --> 00:02:30,000
We create this world from, you know, our interpretations of things

33
00:02:30,000 --> 00:02:38,000
with solidity, where, and color, and form, and meaning where, really,

34
00:02:38,000 --> 00:02:44,000
to another being with different senses, it's a completely different world.

35
00:02:44,000 --> 00:02:47,000
And that's the problem with...

36
00:02:47,000 --> 00:02:49,000
Not exactly you.

37
00:02:49,000 --> 00:02:54,000
Because you can do the same experiment a million times and get the same...

38
00:02:54,000 --> 00:02:58,000
Same physics experiment a million times and get the same result.

39
00:02:58,000 --> 00:03:03,000
So obviously, there is not proof, but a clear indication that a physics would say

40
00:03:03,000 --> 00:03:08,000
that there is the physical realm.

41
00:03:08,000 --> 00:03:09,000
Oh, no, no, no.

42
00:03:09,000 --> 00:03:12,000
I'm saying that there is the physical realm.

43
00:03:12,000 --> 00:03:19,000
What I'm implying is that our interpretation as to what is the reality of this physical realm.

44
00:03:19,000 --> 00:03:21,000
The perception, yeah.

45
00:03:21,000 --> 00:03:24,000
There's a lot of psychological research.

46
00:03:24,000 --> 00:03:27,000
It's me who's saying that there's no physical realm.

47
00:03:27,000 --> 00:03:33,000
Okay, but you're actually giving fodder to their argument.

48
00:03:33,000 --> 00:03:38,000
Because then they would say, well, yes, that's why we can't trust experience.

49
00:03:38,000 --> 00:03:43,000
Why we can't trust meditation, because our perceptions are skewed.

50
00:03:43,000 --> 00:03:44,000
We have to use...

51
00:03:44,000 --> 00:03:50,000
This is why they fight against meditation as, quote unquote, a special kind of knowing,

52
00:03:50,000 --> 00:03:53,000
or any special kind of knowing.

53
00:03:53,000 --> 00:03:59,000
You can only know something from a materialist point of view through rigorous third,

54
00:03:59,000 --> 00:04:07,000
like impersonal experimentation where you've taken a subject completely out of the picture.

55
00:04:07,000 --> 00:04:12,000
So, you know, this idea of subjectivity is actually a bad thing.

56
00:04:12,000 --> 00:04:17,000
And subjective experience is just that subjective in a bad way.

57
00:04:17,000 --> 00:04:23,000
Well, that's quite all paid. So, yeah, quite all from... I mean, not problem.

58
00:04:23,000 --> 00:04:29,000
It's in every psychology book, psychology book.

59
00:04:29,000 --> 00:04:38,000
You can find the subject of perception, how the art works, and how we actually experience reality.

60
00:04:38,000 --> 00:04:44,000
So, like, through our senses, how is it, how is it...

61
00:04:44,000 --> 00:04:49,000
There's a lot of question marks. We don't know how, how it works.

62
00:04:49,000 --> 00:04:53,000
Science, or we don't still don't know how it works.

63
00:04:53,000 --> 00:04:59,000
Yeah, but we have materialists have pretty good evidence to support their view.

64
00:04:59,000 --> 00:05:02,000
Like, at death, there's nothing.

65
00:05:02,000 --> 00:05:06,000
There's nothing left of brain activity, for example.

66
00:05:06,000 --> 00:05:09,000
So, just if we can get back to... What is the question?

67
00:05:09,000 --> 00:05:14,000
The question is how do we... How would you deal with people?

68
00:05:14,000 --> 00:05:18,000
How do you refute... How do you meet criticism regarding rebirth?

69
00:05:18,000 --> 00:05:22,000
Anyone read Richard Doki's book that we got permission?

70
00:05:22,000 --> 00:05:25,000
Yeah, but that's not about rebirth. Let's stay on track.

71
00:05:25,000 --> 00:05:27,000
Yeah, I'll just ask him, anyone?

72
00:05:27,000 --> 00:05:30,000
I listen. I'll do a book.

73
00:05:30,000 --> 00:05:33,000
But that's not the good one.

74
00:05:33,000 --> 00:05:38,000
I'll tell you, personally, I'm still...

75
00:05:38,000 --> 00:05:44,000
I don't hold any belief on rebirth and reincarnation at all.

76
00:05:44,000 --> 00:05:47,000
I'm still open on that.

77
00:05:47,000 --> 00:05:53,000
I can't say I have a belief or a faith in rebirth.

78
00:05:53,000 --> 00:05:56,000
I think it's in the next slide.

79
00:05:56,000 --> 00:06:01,000
Yeah, I mean, we just wait long enough. I'll know the answer.

80
00:06:01,000 --> 00:06:03,000
It's not really that important, is it?

81
00:06:03,000 --> 00:06:09,000
Because you kind of have a choice.

82
00:06:09,000 --> 00:06:13,000
If your outlook on life is that...

83
00:06:13,000 --> 00:06:16,000
Well, you can even avoid the subject entirely and just say,

84
00:06:16,000 --> 00:06:18,000
well, just in the present moment.

85
00:06:18,000 --> 00:06:20,000
But there are problems with that as well.

86
00:06:20,000 --> 00:06:28,000
So, if you have the view, if your worldview is based on permanent deaths,

87
00:06:28,000 --> 00:06:30,000
not at the moment of death,

88
00:06:30,000 --> 00:06:36,000
then the problems with that are what you see from the materialists,

89
00:06:36,000 --> 00:06:40,000
which you see from secularists in general.

90
00:06:40,000 --> 00:06:51,000
They tend to get caught up in addiction and following after their desires

91
00:06:51,000 --> 00:06:56,000
and living in a very materialistic lifestyle.

92
00:06:56,000 --> 00:06:59,000
And we can see the result that it's having on the world.

93
00:06:59,000 --> 00:07:03,000
We can see the result that has on people's minds and their lives.

94
00:07:03,000 --> 00:07:06,000
But the response, of course, is that, well, it's okay.

95
00:07:06,000 --> 00:07:08,000
When you die, you've just got free.

96
00:07:08,000 --> 00:07:13,000
All these negative habits of addiction and aversion that you've built that,

97
00:07:13,000 --> 00:07:15,000
partialities and so on.

98
00:07:15,000 --> 00:07:16,000
You give them up on your day.

99
00:07:16,000 --> 00:07:24,000
It's not quite that simple because actually you have to deal with them even in this life as well.

100
00:07:24,000 --> 00:07:29,000
It's if you ever stop and look at yourself in what you're doing for yourself.

101
00:07:29,000 --> 00:07:30,000
It's quite unpleasant.

102
00:07:30,000 --> 00:07:41,000
If you take, on the other hand, the view of continuity and the impotence,

103
00:07:41,000 --> 00:07:45,000
the impotence of death, in that sense,

104
00:07:45,000 --> 00:07:50,000
to deprive you of the fruit of your habits,

105
00:07:50,000 --> 00:07:53,000
then you've got to be a lot more careful about how you act

106
00:07:53,000 --> 00:07:57,000
and you really do have to take the mind and experience seriously.

107
00:07:57,000 --> 00:08:03,000
You have to take the facts of your actions seriously.

108
00:08:03,000 --> 00:08:07,000
That tends to, one would think,

109
00:08:07,000 --> 00:08:10,000
more peace and harmony in the world.

110
00:08:10,000 --> 00:08:16,000
On the one hand, and also, well, it leads to a different way of behavior,

111
00:08:16,000 --> 00:08:18,000
I think, quite radically.

112
00:08:18,000 --> 00:08:21,000
So it's not trivial.

113
00:08:21,000 --> 00:08:26,000
I'd like to interrupt you and ask you a question, please.

114
00:08:26,000 --> 00:08:31,000
And it has exactly to do with what we're asking about the question.

115
00:08:31,000 --> 00:08:38,000
It's the ontological dilemma of what would take rebirth

116
00:08:38,000 --> 00:08:44,000
if we don't have a permanent auto, a permanent cell.

117
00:08:44,000 --> 00:08:50,000
What of the five scondas or all of the five scondas would take rebirth

118
00:08:50,000 --> 00:08:53,000
that we would call an eye or a cell?

119
00:08:53,000 --> 00:08:56,000
I'll answer that if you like.

120
00:08:56,000 --> 00:09:01,000
That's why we bring that up right now because

121
00:09:01,000 --> 00:09:05,000
it's the perfect thing for you to really elucidate

122
00:09:05,000 --> 00:09:08,000
because that cuts a lot of confusion.

123
00:09:08,000 --> 00:09:12,000
But it's not what I was trying to discuss

124
00:09:12,000 --> 00:09:15,000
is the difference between the few views

125
00:09:15,000 --> 00:09:17,000
and the different effect that it has on your mind.

126
00:09:17,000 --> 00:09:21,000
Regardless of whether one is right or how you explain one or the other.

127
00:09:21,000 --> 00:09:23,000
So you have these two views.

128
00:09:23,000 --> 00:09:26,000
Then you also have the view that neither one is important

129
00:09:26,000 --> 00:09:28,000
and you should just focus on the present moment.

130
00:09:28,000 --> 00:09:32,000
I think there's potential there.

131
00:09:32,000 --> 00:09:36,000
And it certainly seems to be very much with the Buddha taught many times

132
00:09:36,000 --> 00:09:40,000
to just focus on the present moment.

133
00:09:40,000 --> 00:09:50,000
But if the future or the repercussions of your actions,

134
00:09:50,000 --> 00:10:00,000
in other words, the potential for this moment to create another moment

135
00:10:00,000 --> 00:10:05,000
and another moment and to have no escape from the next moment,

136
00:10:05,000 --> 00:10:08,000
if you don't have that,

137
00:10:08,000 --> 00:10:10,000
and if you're just really sticking and saying,

138
00:10:10,000 --> 00:10:14,000
well, Carpe Diem sees the day, live for the now, so on.

139
00:10:14,000 --> 00:10:23,000
Without some sort of philosophical understanding,

140
00:10:23,000 --> 00:10:25,000
or doctrine of continuity,

141
00:10:25,000 --> 00:10:27,000
or maybe not even philosophical,

142
00:10:27,000 --> 00:10:29,000
but the idea of cause and effect.

143
00:10:29,000 --> 00:10:30,000
Let's put it this way.

144
00:10:30,000 --> 00:10:32,000
Without the idea of cause and effect,

145
00:10:32,000 --> 00:10:36,000
even a theory of just staying in the year and now

146
00:10:36,000 --> 00:10:41,000
does in practical terms lead people to selfishness and to

147
00:10:41,000 --> 00:10:47,000
hedonism, hedonism is I think very much living in the now know.

148
00:10:47,000 --> 00:10:50,000
So I think that is really the key,

149
00:10:50,000 --> 00:10:52,000
and it's also the key to rebirth.

150
00:10:52,000 --> 00:10:54,000
Cause and effect.

151
00:10:54,000 --> 00:10:56,000
Science works in terms of cause and effect.

152
00:10:56,000 --> 00:10:59,000
The laws of science show very clearly

153
00:10:59,000 --> 00:11:01,000
the idea of cause and effect.

154
00:11:01,000 --> 00:11:04,000
Quantum physics shows cause and effect.

155
00:11:04,000 --> 00:11:06,000
So if I get classical mechanics,

156
00:11:06,000 --> 00:11:08,000
classical physics shows cause and effect,

157
00:11:08,000 --> 00:11:10,000
but so does quantum physics.

158
00:11:10,000 --> 00:11:16,000
It just changes the realm of realm in which we talk about cause and effect.

159
00:11:16,000 --> 00:11:20,000
Buddhist meditation shows cause and effect.

160
00:11:20,000 --> 00:11:25,000
It's quite clear from all aspects that cause and effect is reality.

161
00:11:25,000 --> 00:11:27,000
That's what we mean by karma.

162
00:11:27,000 --> 00:11:29,000
When people say karma is faith based,

163
00:11:29,000 --> 00:11:32,000
this is a total misunderstanding of the concept of karma.

164
00:11:32,000 --> 00:11:35,000
When people say that rebirth is faith based.

165
00:11:35,000 --> 00:11:37,000
From a Buddhist perspective,

166
00:11:37,000 --> 00:11:43,000
that's totally a total misunderstanding of our approach towards rebirth.

167
00:11:43,000 --> 00:11:46,000
Rebirth is just an extrapolation of cause and effect.

168
00:11:46,000 --> 00:11:51,000
We don't believe we don't accept the belief

169
00:11:51,000 --> 00:11:57,000
that physical death changes anything in the causal continuum

170
00:11:57,000 --> 00:12:00,000
of consciousness from one moment to the next,

171
00:12:00,000 --> 00:12:02,000
which we clearly perceive to be occurring.

172
00:12:02,000 --> 00:12:04,000
And it goes back to just our,

173
00:12:04,000 --> 00:12:06,000
the framework within which we're working,

174
00:12:06,000 --> 00:12:08,000
which is a mental framework.

175
00:12:08,000 --> 00:12:12,000
We're not, we don't subscribe to the idea that

176
00:12:12,000 --> 00:12:17,000
subjective reality is a negative thing or subjective experience is a negative thing.

177
00:12:17,000 --> 00:12:22,000
We differentiate between the aspects of personal experience

178
00:12:22,000 --> 00:12:26,000
and the subjective ones in a way that science doesn't.

179
00:12:26,000 --> 00:12:30,000
Our materialist science says,

180
00:12:30,000 --> 00:12:33,000
all experience is subjective and leaves it at that.

181
00:12:33,000 --> 00:12:35,000
And it's certain that's certainly not the case.

182
00:12:35,000 --> 00:12:40,000
It's possible to get into a state of mind,

183
00:12:40,000 --> 00:12:43,000
aka what we know of as mindfulness,

184
00:12:43,000 --> 00:12:48,000
and have the results and the observations

185
00:12:48,000 --> 00:12:53,000
of that experience be reproducible in all cases for all people.

186
00:12:53,000 --> 00:12:58,000
So you can have a thousand people performing the same experiments

187
00:12:58,000 --> 00:13:02,000
and provided their truthful and so on and so on.

188
00:13:02,000 --> 00:13:07,000
Provided they're sane, provided they are ordinary and ordinary human beings.

189
00:13:07,000 --> 00:13:10,000
They will be able to achieve the same results.

190
00:13:10,000 --> 00:13:14,000
This is what Sam Harris talks about in terms of everyone being able to see

191
00:13:14,000 --> 00:13:17,000
that compassion, that it's possible to enter into these states

192
00:13:17,000 --> 00:13:22,000
that he seems to think are truly objectively positive or so on.

193
00:13:22,000 --> 00:13:25,000
I mean, he seems to get a little bit caught up in them.

194
00:13:25,000 --> 00:13:28,000
But the point, he makes a good point that it's objectively,

195
00:13:28,000 --> 00:13:30,000
there's an objective mindset.

196
00:13:30,000 --> 00:13:31,000
It does truly occur.

197
00:13:31,000 --> 00:13:37,000
And you could do materialist diagnostics or studies of it

198
00:13:37,000 --> 00:13:43,000
to see whether it was having the same effect on the physical side.

199
00:13:43,000 --> 00:13:51,000
So we simply take that objective reality

200
00:13:51,000 --> 00:13:54,000
on a mental level, more on a mental level,

201
00:13:54,000 --> 00:13:57,000
to be the basis of reality.

202
00:13:57,000 --> 00:14:01,000
And so you would have to introduce the belief

203
00:14:01,000 --> 00:14:06,000
that the belief of nihilism, of annihilation at death.

204
00:14:06,000 --> 00:14:09,000
Whether this actually, you know,

205
00:14:09,000 --> 00:14:13,000
a phases, a materialist is debatable.

206
00:14:13,000 --> 00:14:20,000
I talked to the atheist association of Austin.

207
00:14:20,000 --> 00:14:21,000
What are they called?

208
00:14:21,000 --> 00:14:23,000
Do you know these guys that are on YouTube?

209
00:14:23,000 --> 00:14:24,000
The atheists of...

210
00:14:24,000 --> 00:14:25,000
Yeah, I know.

211
00:14:25,000 --> 00:14:26,000
I heard a lot of them.

212
00:14:26,000 --> 00:14:27,000
Yeah.

213
00:14:27,000 --> 00:14:31,000
And so I started, I sent them an email

214
00:14:31,000 --> 00:14:33,000
and they actually responded to me.

215
00:14:33,000 --> 00:14:34,000
There was this mad guy.

216
00:14:34,000 --> 00:14:35,000
Oh, he was horrible.

217
00:14:35,000 --> 00:14:36,000
He was worse than Richard.

218
00:14:36,000 --> 00:14:37,000
Like just the courses.

219
00:14:37,000 --> 00:14:39,000
No, no, no, no, no.

220
00:14:39,000 --> 00:14:41,000
I think these people were very smart.

221
00:14:41,000 --> 00:14:44,000
There was a guy named Aaron Raw.

222
00:14:44,000 --> 00:14:46,000
I remember an Aaron.

223
00:14:46,000 --> 00:14:50,000
Matt Dillahonti, or whoever's name is, he's the host.

224
00:14:50,000 --> 00:14:53,000
And you know, he's great when he's attacking theists.

225
00:14:53,000 --> 00:14:54,000
But when...

226
00:14:54,000 --> 00:14:56,000
But he just lumped meditation in with all that.

227
00:14:56,000 --> 00:14:58,000
You know, he said to me at one point,

228
00:14:58,000 --> 00:15:01,000
you could make the next exposed movie.

229
00:15:01,000 --> 00:15:05,000
This ridiculous movie about teaching evolution in school.

230
00:15:05,000 --> 00:15:07,000
He said, yeah, you want to teach mindfulness?

231
00:15:07,000 --> 00:15:09,000
Well, you could write the next...

232
00:15:09,000 --> 00:15:11,000
No, what was that?

233
00:15:11,000 --> 00:15:13,000
Exposed, was that movie?

234
00:15:13,000 --> 00:15:15,000
I didn't watch it, but...

235
00:15:15,000 --> 00:15:19,000
No, but this Ben Stiller guy.

236
00:15:19,000 --> 00:15:22,000
So the mad...

237
00:15:22,000 --> 00:15:25,000
Sam Harris is an atheist who makes the distinction there

238
00:15:25,000 --> 00:15:28,000
and who makes this distinction.

239
00:15:28,000 --> 00:15:30,000
Not all...

240
00:15:30,000 --> 00:15:31,000
Do you mean Ben?

241
00:15:31,000 --> 00:15:33,000
Do you mean Ben Stein?

242
00:15:33,000 --> 00:15:34,000
Right.

243
00:15:34,000 --> 00:15:36,000
Ben Stein, not Ben Stein.

244
00:15:36,000 --> 00:15:38,000
Ben Stein.

245
00:15:38,000 --> 00:15:42,000
But Sam Harris, as I'm saying,

246
00:15:42,000 --> 00:15:47,000
you know, there is the objective aspect of personal experience

247
00:15:47,000 --> 00:15:51,000
that, you know, truly,

248
00:15:51,000 --> 00:15:55,000
whatever theory you have about it, it truly holds

249
00:15:55,000 --> 00:16:01,000
and, you know, practices based on it do have an effect.

250
00:16:01,000 --> 00:16:04,000
You know, this happiest man in the world article.

251
00:16:04,000 --> 00:16:06,000
This guy really did get happy.

252
00:16:06,000 --> 00:16:13,000
Oh, but at the end of the day,

253
00:16:13,000 --> 00:16:19,000
the acts of the reaper and karma

254
00:16:19,000 --> 00:16:25,000
and all these teams are beyond what's going to happen.

255
00:16:25,000 --> 00:16:29,000
It's not that important in what this is.

256
00:16:29,000 --> 00:16:32,000
No, karma is incredibly important.

257
00:16:32,000 --> 00:16:33,000
Karma is...

258
00:16:33,000 --> 00:16:35,000
I mean, no, no, no, no, no, no.

259
00:16:35,000 --> 00:16:37,000
You can say instead of karma,

260
00:16:37,000 --> 00:16:38,000
you can...

261
00:16:38,000 --> 00:16:39,000
Karma is a moment.

262
00:16:39,000 --> 00:16:40,000
Yes, they are.

263
00:16:40,000 --> 00:16:41,000
You can say suffering.

264
00:16:41,000 --> 00:16:42,000
I mean...

265
00:16:42,000 --> 00:16:43,000
But yes, but you see,

266
00:16:43,000 --> 00:16:45,000
what is the problem with suffering?

267
00:16:45,000 --> 00:16:46,000
There is the cause of suffering.

268
00:16:46,000 --> 00:16:47,000
The cause of suffering is what?

269
00:16:47,000 --> 00:16:48,000
That's...

270
00:16:48,000 --> 00:16:49,000
Yeah.

271
00:16:49,000 --> 00:16:50,000
The reason...

272
00:16:50,000 --> 00:16:51,000
You do have bad deeds.

273
00:16:51,000 --> 00:16:52,000
You feel horrible, right?

274
00:16:52,000 --> 00:16:54,000
You don't feel bad about it.

275
00:16:54,000 --> 00:16:55,000
Yes.

276
00:16:55,000 --> 00:16:56,000
That's all you need.

277
00:16:56,000 --> 00:16:59,000
But that's all you need.

278
00:16:59,000 --> 00:17:00,000
Oh, okay.

279
00:17:00,000 --> 00:17:02,000
But don't call it karma.

280
00:17:02,000 --> 00:17:04,000
You just call it observation.

281
00:17:04,000 --> 00:17:06,000
This is the problem.

282
00:17:06,000 --> 00:17:07,000
This is the problem.

283
00:17:07,000 --> 00:17:08,000
This is not you.

284
00:17:08,000 --> 00:17:09,000
This is how good...

285
00:17:09,000 --> 00:17:11,000
This is how people think the neighborhood is.

286
00:17:11,000 --> 00:17:12,000
Well, that's all that's important.

287
00:17:12,000 --> 00:17:13,000
This karma thing.

288
00:17:13,000 --> 00:17:14,000
Throw that out.

289
00:17:14,000 --> 00:17:15,000
But that's exactly...

290
00:17:15,000 --> 00:17:16,000
What do you think karma is?

291
00:17:16,000 --> 00:17:17,000
It's practical.

292
00:17:17,000 --> 00:17:19,000
It's not a theory where you think,

293
00:17:19,000 --> 00:17:20,000
wow, yeah, if I kill someone in this life,

294
00:17:20,000 --> 00:17:23,000
I'll be killed in my next life.

295
00:17:23,000 --> 00:17:25,000
All that is is an extrapolation.

296
00:17:25,000 --> 00:17:26,000
If it's true or not true.

297
00:17:26,000 --> 00:17:28,000
I mean, it makes sense.

298
00:17:28,000 --> 00:17:30,000
If you do a bad thing, it hurts you now.

299
00:17:30,000 --> 00:17:33,000
But it also has the potential to ripple into it.

300
00:17:33,000 --> 00:17:35,000
It's the potential to ripple out and hurt you in the future.

301
00:17:35,000 --> 00:17:36,000
It can hurt you in this life.

302
00:17:36,000 --> 00:17:38,000
Maybe the person will come back and tell you in this life.

303
00:17:38,000 --> 00:17:39,000
That makes sense.

304
00:17:39,000 --> 00:17:42,000
Yeah, if you realize if you replace things like karma

305
00:17:42,000 --> 00:17:47,000
with observation, we replace meditating with light training.

306
00:17:47,000 --> 00:17:48,000
Oh, that's not...

307
00:17:48,000 --> 00:17:49,000
No, I...

308
00:17:49,000 --> 00:17:51,000
Karma is the way it is.

309
00:17:51,000 --> 00:17:53,000
Observation is seeing how it is.

310
00:17:53,000 --> 00:17:54,000
Two different things.

311
00:17:54,000 --> 00:17:55,000
Yeah.

312
00:17:55,000 --> 00:17:59,000
I mean, you know, you don't have to know the concept of karma.

313
00:17:59,000 --> 00:18:01,000
Just to see the idea.

314
00:18:01,000 --> 00:18:03,000
This is...

315
00:18:03,000 --> 00:18:04,000
I mean...

316
00:18:04,000 --> 00:18:05,000
Yeah.

317
00:18:05,000 --> 00:18:07,000
So, I mean, you see it.

318
00:18:07,000 --> 00:18:09,000
But you can see, like...

319
00:18:09,000 --> 00:18:10,000
Straight away.

320
00:18:10,000 --> 00:18:11,000
Sometimes...

321
00:18:11,000 --> 00:18:12,000
Sometimes...

322
00:18:12,000 --> 00:18:13,000
Sometimes not...

323
00:18:13,000 --> 00:18:14,000
Sometimes not...

324
00:18:14,000 --> 00:18:15,000
Sometimes...

325
00:18:15,000 --> 00:18:16,000
Sometimes...

326
00:18:16,000 --> 00:18:21,000
It's not necessary to see what's going to happen in your next life based on what you did in this life.

327
00:18:21,000 --> 00:18:22,000
But it's...

328
00:18:22,000 --> 00:18:23,000
That's right.

329
00:18:23,000 --> 00:18:24,000
It's just part of the theory.

330
00:18:24,000 --> 00:18:28,000
You know, you can't stay exactly, but it makes sense that,

331
00:18:28,000 --> 00:18:31,000
you know, it's the bad effects of doing that.

332
00:18:31,000 --> 00:18:32,000
Exactly.

333
00:18:32,000 --> 00:18:32,000
Exactly.

334
00:18:32,000 --> 00:18:33,000
It makes sense.

335
00:18:33,000 --> 00:18:38,000
And I don't think that if we regard Buddhism as might not be religion,

336
00:18:38,000 --> 00:18:41,000
that 80s shouldn't be attacking Buddhism.

337
00:18:41,000 --> 00:18:43,000
Because there's no...

338
00:18:43,000 --> 00:18:45,000
There's not even one thing that should be...

339
00:18:45,000 --> 00:18:46,000
It's not...

340
00:18:46,000 --> 00:18:47,000
Because it's...

341
00:18:47,000 --> 00:18:48,000
But reason.

342
00:18:48,000 --> 00:18:49,000
Karma.

343
00:18:49,000 --> 00:18:50,000
Liberty and karma is something they do it.

344
00:18:50,000 --> 00:18:52,000
Many people do it exactly for this reason.

345
00:18:52,000 --> 00:18:54,000
They take it from...

346
00:18:54,000 --> 00:18:55,000
Well, ask backwards.

347
00:18:55,000 --> 00:18:57,000
I think it's the technical term.

348
00:18:57,000 --> 00:19:01,000
They start, you know, from next life and work their way back,

349
00:19:01,000 --> 00:19:05,000
which is not how we just explain it.

350
00:19:05,000 --> 00:19:08,000
It starts from this moment of ripples out.

351
00:19:08,000 --> 00:19:09,000
It happens in the next life.

352
00:19:09,000 --> 00:19:10,000
Well, it's conjecture.

353
00:19:10,000 --> 00:19:14,000
But it makes sense that it's based on, in some part,

354
00:19:14,000 --> 00:19:17,000
on what we do in this life.

355
00:19:17,000 --> 00:19:19,000
Yeah.

356
00:19:19,000 --> 00:19:20,000
Makes sense.

357
00:19:20,000 --> 00:19:21,000
Basically.

358
00:19:21,000 --> 00:19:23,000
I'm not going to give you true, but it makes sense.

359
00:19:23,000 --> 00:19:28,000
See?

