1
00:00:00,000 --> 00:00:09,000
Hello, today I will be beginning a new series of videos on a text called the Dhamapada.

2
00:00:09,000 --> 00:00:18,000
The Dhamapada is a group of 423 verses that are said to have been taught by the Lord Buddha

3
00:00:18,000 --> 00:00:21,000
at various times during his life.

4
00:00:21,000 --> 00:00:29,000
So, it's generally considered to be a fairly good summary of the Buddha's teachings.

5
00:00:29,000 --> 00:00:37,000
And so, I thought it would be quite a good subject for videos in order to spread the Buddha's teaching

6
00:00:37,000 --> 00:00:43,000
and help more people to realize the benefits of the teaching.

7
00:00:43,000 --> 00:00:49,000
So, what I'm going to do is read you here that I've got the polyverse.

8
00:00:49,000 --> 00:00:52,000
I'm going to read it in poly one verse at a time.

9
00:00:52,000 --> 00:00:57,000
And after the verse I will translate it piece by piece into English

10
00:00:57,000 --> 00:00:59,000
to give you an understanding of what it means.

11
00:00:59,000 --> 00:01:03,000
Then I will tell the story which goes with the verse

12
00:01:03,000 --> 00:01:09,000
because each verse has a story that gives the occasion

13
00:01:09,000 --> 00:01:13,000
and the circumstances in which the Buddha gave this teaching.

14
00:01:13,000 --> 00:01:18,000
And after I give this sort of short summary of the story,

15
00:01:18,000 --> 00:01:22,000
then I'll explain how I think we should understand this verse

16
00:01:22,000 --> 00:01:26,000
and how it can relate to our lives and to our practice.

17
00:01:26,000 --> 00:01:32,000
So, one note about the stories is that there's two ways we can go.

18
00:01:32,000 --> 00:01:37,000
Some people will pay more attention to the stories than the verses

19
00:01:37,000 --> 00:01:40,000
and remember the Dhamapada as a series of stories.

20
00:01:40,000 --> 00:01:44,000
Another group of people will never have heard or have read the stories

21
00:01:44,000 --> 00:01:48,000
and will therefore think of the Dhamapada as a group of verses.

22
00:01:48,000 --> 00:01:52,000
And I think I'm going to try to hit a happy medium

23
00:01:52,000 --> 00:01:58,000
because we don't go to either extreme because the verses have a great benefit

24
00:01:58,000 --> 00:02:04,000
to them in providing a good example of how the teachings can be applied

25
00:02:04,000 --> 00:02:07,000
and how they apply to our lives.

26
00:02:07,000 --> 00:02:12,000
They provide us with encouragement when we hear about people practicing good things

27
00:02:12,000 --> 00:02:19,000
and help us to adjust ourselves when we hear about the results of bad things and so on.

28
00:02:19,000 --> 00:02:23,000
They help put it into context to some extent.

29
00:02:23,000 --> 00:02:29,000
But what I'd like to say is that we should not let the stories dictate the context of the verse

30
00:02:29,000 --> 00:02:34,000
because even though they do provide some context,

31
00:02:34,000 --> 00:02:39,000
it's clear that the Buddha was not meaning to apply the verse only to the context

32
00:02:39,000 --> 00:02:40,000
in which it was taught.

33
00:02:40,000 --> 00:02:46,000
The teaching came as a result of a certain event or occurrence

34
00:02:46,000 --> 00:02:51,000
and the Buddha's time is true, but the teaching, of course, is much more broad

35
00:02:51,000 --> 00:02:57,000
and deep often than the circumstance allows for.

36
00:02:57,000 --> 00:03:00,000
So today I'm going to give the first verse.

37
00:03:00,000 --> 00:03:04,000
And this is of the Yamakawaga, which is the first group of verses.

38
00:03:04,000 --> 00:03:12,000
It's the Dhamapada 123 verses is separated into different sections.

39
00:03:12,000 --> 00:03:16,000
So this is the first verse of the first section.

40
00:03:16,000 --> 00:03:44,000
And the verse goes in Pali.

41
00:03:44,000 --> 00:03:50,000
It's all Dhammas are preceded by the mind.

42
00:03:50,000 --> 00:03:53,000
All things, all reality.

43
00:03:53,000 --> 00:03:56,000
Everything is preceded by the mind.

44
00:03:56,000 --> 00:04:03,000
Manosetah, Manomaya, they are governed by the mind and made by the mind.

45
00:04:03,000 --> 00:04:06,000
They are governed by the mind.

46
00:04:06,000 --> 00:04:09,000
Manomaya, they are made or formed by the mind.

47
00:04:09,000 --> 00:04:10,000
All things.

48
00:04:10,000 --> 00:04:13,000
This is the Buddhist words.

49
00:04:13,000 --> 00:04:20,000
Manasaji, Badutena, if with a impure mind,

50
00:04:20,000 --> 00:04:23,000
Basadiwakarodiwa, one acts or speaks.

51
00:04:23,000 --> 00:04:27,000
If one acts or speaks with an impure mind,

52
00:04:27,000 --> 00:04:31,000
Tatonandukamandwetidhakumwahatulvadam,

53
00:04:31,000 --> 00:04:34,000
suffering follows there from,

54
00:04:34,000 --> 00:04:40,000
just as the wheel of the cart follows the ox that pulls it.

55
00:04:40,000 --> 00:04:43,000
So if an ox is pulling a cart,

56
00:04:43,000 --> 00:04:45,000
the wheel has to follow the ox.

57
00:04:45,000 --> 00:04:48,000
There's no way that it can't stop.

58
00:04:48,000 --> 00:04:51,000
It can't change its course.

59
00:04:51,000 --> 00:04:53,000
It will have to follow after the foot.

60
00:04:53,000 --> 00:04:56,000
So on the path, you will see the footprint of the ox,

61
00:04:56,000 --> 00:05:00,000
and you will see the footprint of the cart following it always.

62
00:05:00,000 --> 00:05:03,000
Because as long as the ox is pulling the cart,

63
00:05:03,000 --> 00:05:06,000
in the same way, when we act or speak,

64
00:05:06,000 --> 00:05:09,000
if our heart is impure, suffering will follow.

65
00:05:09,000 --> 00:05:12,000
Just as the cart follows the ox.

66
00:05:12,000 --> 00:05:14,000
That's the same course.

67
00:05:14,000 --> 00:05:19,000
So this verse was given in relation to the

68
00:05:19,000 --> 00:05:23,000
venerable elder monk, Chakubala.

69
00:05:23,000 --> 00:05:26,000
Chakubala, Chakub means I.

70
00:05:26,000 --> 00:05:28,000
So Bala and Bala means guardian.

71
00:05:28,000 --> 00:05:31,000
So his name means one who guards his I.

72
00:05:31,000 --> 00:05:34,000
And this is a name he was given.

73
00:05:34,000 --> 00:05:36,000
His original name was actually Bala.

74
00:05:36,000 --> 00:05:41,000
But the story goes that he became ordained into the Buddha

75
00:05:41,000 --> 00:05:43,000
a little bit later in life.

76
00:05:43,000 --> 00:05:46,000
And so he didn't spend so much time studying.

77
00:05:46,000 --> 00:05:49,000
He stayed five years with the Buddha to do the basic training

78
00:05:49,000 --> 00:05:50,000
as a monk.

79
00:05:50,000 --> 00:05:52,000
But after that he asked permission to go off and practice

80
00:05:52,000 --> 00:05:55,000
in the forest with some of his fellow monks.

81
00:05:55,000 --> 00:06:03,000
And he spent three months of the rain season in the forest.

82
00:06:03,000 --> 00:06:06,000
And he made a determination not to do lying,

83
00:06:06,000 --> 00:06:09,000
not to lie down for three months.

84
00:06:09,000 --> 00:06:13,000
So he undertook this practice to only do walking,

85
00:06:13,000 --> 00:06:15,000
standing, and sitting.

86
00:06:15,000 --> 00:06:16,000
And so he would do walking meditation.

87
00:06:16,000 --> 00:06:18,000
He would do sitting meditation.

88
00:06:18,000 --> 00:06:19,000
He would do standing meditation.

89
00:06:19,000 --> 00:06:21,000
But he would never lie down not for three months.

90
00:06:21,000 --> 00:06:22,000
No sleeping.

91
00:06:22,000 --> 00:06:25,000
So this means unless he would sleep sitting up or not

92
00:06:25,000 --> 00:06:28,000
off sitting up by accident.

93
00:06:28,000 --> 00:06:32,000
This is a practice that monks will undertake

94
00:06:32,000 --> 00:06:34,000
when they've developed.

95
00:06:34,000 --> 00:06:39,000
And after they've progressed in the meditation

96
00:06:39,000 --> 00:06:43,000
will become proficient and confident in their practice.

97
00:06:43,000 --> 00:06:45,000
So he did this for three months.

98
00:06:45,000 --> 00:06:48,000
And during this time he developed a sick disease

99
00:06:48,000 --> 00:06:51,000
of the eye as a sickness in his eyes.

100
00:06:51,000 --> 00:06:55,000
And this doctor told him that he had to take this medicine.

101
00:06:55,000 --> 00:06:59,000
He said he would have to lie down to take this medicine.

102
00:06:59,000 --> 00:07:02,000
And something they put it in is, it was put in his nose,

103
00:07:02,000 --> 00:07:08,000
or something, some ancient Ayurvedic cure.

104
00:07:08,000 --> 00:07:11,000
And now Jacobali, he didn't say yes.

105
00:07:11,000 --> 00:07:12,000
He didn't say no.

106
00:07:12,000 --> 00:07:13,000
He took the medicine.

107
00:07:13,000 --> 00:07:14,000
He went home.

108
00:07:14,000 --> 00:07:15,000
And he thought, it went back to the monastery.

109
00:07:15,000 --> 00:07:16,000
And he thought to himself, you know,

110
00:07:16,000 --> 00:07:18,000
what should I do?

111
00:07:18,000 --> 00:07:20,000
He said, well, I've given this vow

112
00:07:20,000 --> 00:07:22,000
that I'm not going to lie down.

113
00:07:22,000 --> 00:07:25,000
And I really want to carry out my vow

114
00:07:25,000 --> 00:07:28,000
and to really exert myself in the practice.

115
00:07:28,000 --> 00:07:30,000
And so as a result, he sat up.

116
00:07:30,000 --> 00:07:32,000
And he kind of took the medicine sitting up,

117
00:07:32,000 --> 00:07:34,000
but he didn't lie down.

118
00:07:34,000 --> 00:07:37,000
As a result, his sickness didn't get better.

119
00:07:37,000 --> 00:07:38,000
He had been back cut worse.

120
00:07:38,000 --> 00:07:41,000
And he went back on arms round and the doctor came up to him

121
00:07:41,000 --> 00:07:43,000
and asked him, how is the sickness?

122
00:07:43,000 --> 00:07:44,000
He's getting better.

123
00:07:44,000 --> 00:07:48,000
And he said, oh, and the wind is still hurting my eyes.

124
00:07:48,000 --> 00:07:50,000
And he said, well, did you lie down to take the medicine?

125
00:07:50,000 --> 00:07:52,000
And he said, and he didn't say anything.

126
00:07:52,000 --> 00:07:55,000
He just stood there and the doctor said,

127
00:07:55,000 --> 00:07:58,000
sure, you have to take the, you have to lie down to take it.

128
00:07:58,000 --> 00:07:59,000
And he said, well, thank you.

129
00:07:59,000 --> 00:08:01,000
And he went away.

130
00:08:01,000 --> 00:08:03,000
The doctor started to get suspicious.

131
00:08:03,000 --> 00:08:05,000
And so he went back to the monastery,

132
00:08:05,000 --> 00:08:07,000
followed after the elder,

133
00:08:07,000 --> 00:08:10,000
and went and looked at his dwelling

134
00:08:10,000 --> 00:08:12,000
and saw that there was no bedding.

135
00:08:12,000 --> 00:08:14,000
And he said, venerable sir, where is your bed?

136
00:08:14,000 --> 00:08:16,000
And he said, and he didn't say anything.

137
00:08:16,000 --> 00:08:18,000
He said, venerable sir, you can't do this.

138
00:08:18,000 --> 00:08:21,000
You're going to go blind if you do this.

139
00:08:21,000 --> 00:08:24,000
You have to take care of your eyes.

140
00:08:24,000 --> 00:08:26,000
You need them if you want to be a monk.

141
00:08:26,000 --> 00:08:28,000
You're going to give them a little lecture.

142
00:08:28,000 --> 00:08:33,000
And the monk said, thank you, but I will know what to do by myself.

143
00:08:33,000 --> 00:08:35,000
I'll figure out what to do by myself.

144
00:08:35,000 --> 00:08:38,000
And so the doctor said, fine, but don't tell anyone.

145
00:08:38,000 --> 00:08:42,000
Then you don't tell anyone that I was the one who

146
00:08:42,000 --> 00:08:44,000
cured you or who gave you the medicine.

147
00:08:44,000 --> 00:08:49,000
And don't tell them that I don't want to have anything to do with you.

148
00:08:49,000 --> 00:08:50,000
And he said, yes, thank you.

149
00:08:50,000 --> 00:08:52,000
And so the doctor went away.

150
00:08:52,000 --> 00:08:55,000
So Chakupala or Pala, he sat down and he thought,

151
00:08:55,000 --> 00:08:57,000
and he said, well, what do I do?

152
00:08:57,000 --> 00:09:01,000
I can either take care of my eyes or I can take care of my practice.

153
00:09:01,000 --> 00:09:06,000
I can either guard the physical body or I can guard the truth.

154
00:09:06,000 --> 00:09:11,000
And he said to himself, if I don't take this medicine,

155
00:09:11,000 --> 00:09:14,000
if I don't take it properly, then I might lose my eyes.

156
00:09:14,000 --> 00:09:16,000
My eyes will be ruined.

157
00:09:16,000 --> 00:09:21,000
And he said, but these eyes, these ears, this body,

158
00:09:21,000 --> 00:09:25,000
this self, this thing that I cling to.

159
00:09:25,000 --> 00:09:27,000
Eventually, it will all be ruined.

160
00:09:27,000 --> 00:09:28,000
It will all fall apart.

161
00:09:28,000 --> 00:09:31,000
He said, why would, why should I base my life

162
00:09:31,000 --> 00:09:34,000
on the well-being of something physical?

163
00:09:34,000 --> 00:09:37,000
And he said, it's much more important

164
00:09:37,000 --> 00:09:39,000
that I should guard the dumb, I should guard the truth,

165
00:09:39,000 --> 00:09:42,000
than that I should guard the physical body.

166
00:09:42,000 --> 00:09:45,000
And so he didn't lie down and he didn't take the medicine.

167
00:09:45,000 --> 00:09:48,000
He continued on with his practice.

168
00:09:48,000 --> 00:09:51,000
As a result of this, two things happened.

169
00:09:51,000 --> 00:09:53,000
The first is he lost his eyes.

170
00:09:53,000 --> 00:09:56,000
And the second is that he gained his eyes.

171
00:09:56,000 --> 00:09:58,000
And this is how the text goes.

172
00:09:58,000 --> 00:10:02,000
As he was doing walking meditation at the same moment,

173
00:10:02,000 --> 00:10:05,000
his eyes got worse and worse and worse.

174
00:10:05,000 --> 00:10:10,000
And finally, they deteriorated or something changed.

175
00:10:10,000 --> 00:10:13,000
Something switched off.

176
00:10:13,000 --> 00:10:16,000
His eyes suddenly became useless.

177
00:10:16,000 --> 00:10:18,000
And his physical eye.

178
00:10:18,000 --> 00:10:20,000
And at that same moment, as this was happening,

179
00:10:20,000 --> 00:10:22,000
and as he was watching it and worrying about it

180
00:10:22,000 --> 00:10:25,000
and looking at his worrying and looking at his clinging to the body

181
00:10:25,000 --> 00:10:29,000
and letting it go and seeing the suffering inherent

182
00:10:29,000 --> 00:10:33,000
in this clinging mind, wanting and liking the fact

183
00:10:33,000 --> 00:10:38,000
that he can see in his eyes and the self and the ego

184
00:10:38,000 --> 00:10:40,000
and so on, seeing the suffering.

185
00:10:40,000 --> 00:10:42,000
This is the suffering.

186
00:10:42,000 --> 00:10:43,000
He saw the portable truth.

187
00:10:43,000 --> 00:10:46,000
He saw suffering in the cause of suffering.

188
00:10:46,000 --> 00:10:48,000
And when he saw that he let go

189
00:10:48,000 --> 00:10:51,000
and realized the cessation of suffering.

190
00:10:51,000 --> 00:10:54,000
So at the same moment, he saw the truth

191
00:10:54,000 --> 00:10:58,000
and he lost his vision forever, forever,

192
00:10:58,000 --> 00:11:00,000
his physical vision.

193
00:11:00,000 --> 00:11:04,000
He became an aura hunt at the same time.

194
00:11:04,000 --> 00:11:07,000
So at the end of the rain season after his practice,

195
00:11:07,000 --> 00:11:10,000
and after he helped out the rest of the other monks

196
00:11:10,000 --> 00:11:13,000
to become an able to practice correctly as well.

197
00:11:13,000 --> 00:11:15,000
He made his way back to the Buddha

198
00:11:15,000 --> 00:11:19,000
and spent some time back at the monastery

199
00:11:19,000 --> 00:11:23,000
with the Buddha was staying, I believe, in Sawawati, in India.

200
00:11:23,000 --> 00:11:28,000
And in Jaitawana, the great monastery at the Buddha.

201
00:11:28,000 --> 00:11:30,000
And while he was there, of course,

202
00:11:30,000 --> 00:11:32,000
there were other monks taking care of him

203
00:11:32,000 --> 00:11:35,000
and met him and monks who he would teach

204
00:11:35,000 --> 00:11:38,000
and they would look after him physically.

205
00:11:38,000 --> 00:11:41,000
And many visiting monks.

206
00:11:41,000 --> 00:11:44,000
And his name got around as a fairly proficient teacher.

207
00:11:44,000 --> 00:11:48,000
People thought the rumor went that he was enlightened.

208
00:11:48,000 --> 00:11:52,000
And so monks would come to visit him one day,

209
00:11:52,000 --> 00:11:55,000
one night in the middle of the night

210
00:11:55,000 --> 00:11:57,000
he was doing walking meditation.

211
00:11:57,000 --> 00:11:59,000
And he would do the walking meditation outside.

212
00:11:59,000 --> 00:12:01,000
And it had rained.

213
00:12:01,000 --> 00:12:03,000
So it had rained heavily all night.

214
00:12:03,000 --> 00:12:06,000
And then in the early morning, 3 a.m. or so on,

215
00:12:06,000 --> 00:12:09,000
he got up to do walking meditation.

216
00:12:09,000 --> 00:12:11,000
Now, those times in India,

217
00:12:11,000 --> 00:12:14,000
and even now in here in Sri Lanka and Thailand,

218
00:12:14,000 --> 00:12:18,000
they have something like a termite or an ant

219
00:12:18,000 --> 00:12:21,000
that it's really the most useless animal in the world.

220
00:12:21,000 --> 00:12:23,000
I don't know how they managed to survive

221
00:12:23,000 --> 00:12:25,000
because they die in droves.

222
00:12:25,000 --> 00:12:27,000
They fly around and they lose their wings

223
00:12:27,000 --> 00:12:31,000
and then they just lie there and die.

224
00:12:31,000 --> 00:12:33,000
It seems like.

225
00:12:33,000 --> 00:12:37,000
And these insects were coming in the night.

226
00:12:37,000 --> 00:12:41,000
They come when it rains because they're there.

227
00:12:41,000 --> 00:12:43,000
Their layers get flooded.

228
00:12:43,000 --> 00:12:45,000
And so they were recovering this walking path.

229
00:12:45,000 --> 00:12:46,000
And it took a while.

230
00:12:46,000 --> 00:12:49,000
It came out in the morning after the rains on the 3 a.m.

231
00:12:49,000 --> 00:12:50,000
and so on.

232
00:12:50,000 --> 00:12:52,000
And started doing walking meditation.

233
00:12:52,000 --> 00:12:54,000
And as he was doing walking meditation,

234
00:12:54,000 --> 00:12:56,000
many of these insects died.

235
00:12:56,000 --> 00:12:57,000
She was walking back and forth.

236
00:12:57,000 --> 00:12:59,000
He did no idea that they were there.

237
00:12:59,000 --> 00:13:06,000
And they died many, many of these insects were squashed.

238
00:13:06,000 --> 00:13:10,000
So in the morning these monks came to see him to meet him

239
00:13:10,000 --> 00:13:12,000
and said, where is the venerable Jekyll Bala?

240
00:13:12,000 --> 00:13:14,000
This is the name he was given.

241
00:13:14,000 --> 00:13:16,000
And the monks who looked after him said,

242
00:13:16,000 --> 00:13:17,000
that's his monost.

243
00:13:17,000 --> 00:13:18,000
That's his cookie over there.

244
00:13:18,000 --> 00:13:22,000
So they went over to look and they saw this walking path

245
00:13:22,000 --> 00:13:25,000
that was covered with his footprints

246
00:13:25,000 --> 00:13:32,000
stepping on these termites or these ants or whatever they are.

247
00:13:32,000 --> 00:13:36,000
And these monks were terribly offended.

248
00:13:36,000 --> 00:13:39,000
And they thought, this is how could this be an enlightened monk

249
00:13:39,000 --> 00:13:45,000
who's here engaging in one-ton slaughter of these innocent creatures?

250
00:13:45,000 --> 00:13:48,000
And so they were very offended.

251
00:13:48,000 --> 00:13:50,000
And they went to see the Buddha and they said,

252
00:13:50,000 --> 00:13:52,000
and they said, this is right.

253
00:13:52,000 --> 00:13:55,000
This monk, he should be taught how to practice correctly,

254
00:13:55,000 --> 00:13:59,000
how can he be an elder and still not know the correct.

255
00:13:59,000 --> 00:14:02,000
How wrong it is to kill.

256
00:14:02,000 --> 00:14:04,000
And this is where the Buddha gave this teaching,

257
00:14:04,000 --> 00:14:09,000
which actually becomes a part of a very famous

258
00:14:09,000 --> 00:14:13,000
or the very important part of the Buddha's teaching is the teaching.

259
00:14:13,000 --> 00:14:19,000
The Buddha's teaching of karma, which actually denies the efficacy of karma.

260
00:14:19,000 --> 00:14:22,000
So people say the Buddha taught the theory of karma.

261
00:14:22,000 --> 00:14:25,000
In fact, you can say the Buddha taught against the theory of karma

262
00:14:25,000 --> 00:14:28,000
because the Buddha said, my son, Taghupala,

263
00:14:28,000 --> 00:14:30,000
is not guilty of anything.

264
00:14:30,000 --> 00:14:31,000
He's innocent.

265
00:14:31,000 --> 00:14:33,000
And then he said, bonopobangamadamra,

266
00:14:33,000 --> 00:14:36,000
the mind perceives all dumb ones.

267
00:14:36,000 --> 00:14:39,000
The mind is what leads to suffering.

268
00:14:39,000 --> 00:14:42,000
If you act or speak with an impure mind,

269
00:14:42,000 --> 00:14:45,000
that's where suffering belongs.

270
00:14:45,000 --> 00:14:47,000
So the Buddha took this verse,

271
00:14:47,000 --> 00:14:49,000
was given in a negative context.

272
00:14:49,000 --> 00:14:52,000
The point was not to say that suffering is going to

273
00:14:52,000 --> 00:14:55,000
in a positive sense of suffering will lead to this using.

274
00:14:55,000 --> 00:14:57,000
If your mind doesn't have those things,

275
00:14:57,000 --> 00:15:00,000
then it can't lead to suffering.

276
00:15:00,000 --> 00:15:06,000
So it's the fact that karma cannot lead to unpleasant results.

277
00:15:06,000 --> 00:15:10,000
And this is an incredibly profound statement, I think,

278
00:15:10,000 --> 00:15:14,000
because it's not something that we would think of ourselves.

279
00:15:14,000 --> 00:15:18,000
If someone gets hurt because of something we do,

280
00:15:18,000 --> 00:15:20,000
then we think of ourselves as guilty.

281
00:15:20,000 --> 00:15:22,000
We feel bad, and if we don't feel bad,

282
00:15:22,000 --> 00:15:24,000
maybe the first person is angry at us,

283
00:15:24,000 --> 00:15:27,000
and they can get angry at us whether we meant to do it or not.

284
00:15:27,000 --> 00:15:30,000
But the point that the Buddha is making here

285
00:15:30,000 --> 00:15:35,000
really shows the emphasis on the meditation practice.

286
00:15:35,000 --> 00:15:42,000
How the Buddha's teaching is really a practice of meditation and of contemplation.

287
00:15:42,000 --> 00:15:47,000
Why? Because we don't understand things in terms of beings,

288
00:15:47,000 --> 00:15:50,000
in terms of concepts,

289
00:15:50,000 --> 00:15:53,000
that we think of that person, and I hurt them,

290
00:15:53,000 --> 00:15:54,000
and so on.

291
00:15:54,000 --> 00:15:58,000
We think of it in terms of actual reality in the experience.

292
00:15:58,000 --> 00:16:01,000
So maybe the person does get angry at me for something that I did.

293
00:16:01,000 --> 00:16:03,000
Maybe these monks got angry at Chakupala,

294
00:16:03,000 --> 00:16:05,000
and therefore they think, oh, that was bad karma,

295
00:16:05,000 --> 00:16:07,000
because it made people angry at him.

296
00:16:07,000 --> 00:16:09,000
But at the same time, you can say,

297
00:16:09,000 --> 00:16:12,000
well, Chakupala doesn't face him at all, really.

298
00:16:12,000 --> 00:16:15,000
He had no bad intentions towards the insects.

299
00:16:15,000 --> 00:16:18,000
He had no bad intentions towards these monks.

300
00:16:18,000 --> 00:16:19,000
And if they get angry at him,

301
00:16:19,000 --> 00:16:22,000
it's really water off a duck's back,

302
00:16:22,000 --> 00:16:28,000
or it's like the Buddha said, like a mustard seed on a needle,

303
00:16:28,000 --> 00:16:31,000
or water off of a lotus leaf.

304
00:16:31,000 --> 00:16:33,000
It doesn't stick.

305
00:16:33,000 --> 00:16:35,000
So because his mind is pure,

306
00:16:35,000 --> 00:16:37,000
because his mind doesn't have any of his clinging,

307
00:16:37,000 --> 00:16:39,000
any of his anger.

308
00:16:39,000 --> 00:16:41,000
Whereas on the other hand, if he did have anger,

309
00:16:41,000 --> 00:16:43,000
if you did want bad intentions,

310
00:16:43,000 --> 00:16:46,000
and if he didn't intend to kill these insects,

311
00:16:46,000 --> 00:16:48,000
that is what would lead him to suffering.

312
00:16:48,000 --> 00:16:50,000
And that's what makes it unethical.

313
00:16:50,000 --> 00:16:53,000
An act is not unethical, as I've said before,

314
00:16:53,000 --> 00:16:56,000
simply because it fits into a category,

315
00:16:56,000 --> 00:16:58,000
like killing his unethical or so on.

316
00:16:58,000 --> 00:17:01,000
Killing is only unethical because of the mind states

317
00:17:01,000 --> 00:17:03,000
that are required to kill.

318
00:17:03,000 --> 00:17:05,000
In order to intentionally kill something,

319
00:17:05,000 --> 00:17:07,000
you have to give rise to a harmful,

320
00:17:07,000 --> 00:17:09,000
and actually a perverted mind state.

321
00:17:09,000 --> 00:17:11,000
One that wants to, you know,

322
00:17:11,000 --> 00:17:13,000
doesn't want to die oneself,

323
00:17:13,000 --> 00:17:16,000
and the India wants to cause harm to others.

324
00:17:16,000 --> 00:17:20,000
So the mind is of ultimate importance.

325
00:17:20,000 --> 00:17:22,000
And this shows, as I said,

326
00:17:22,000 --> 00:17:24,000
shows the emphasis on meditation,

327
00:17:24,000 --> 00:17:25,000
because it's only through meditation

328
00:17:25,000 --> 00:17:27,000
that we can affect the mind.

329
00:17:27,000 --> 00:17:28,000
And mind is,

330
00:17:28,000 --> 00:17:31,000
that the mind is what is affected through meditation.

331
00:17:31,000 --> 00:17:33,000
As we practice meditation,

332
00:17:33,000 --> 00:17:35,000
we see the craving,

333
00:17:35,000 --> 00:17:38,000
we see the stress that is caused

334
00:17:38,000 --> 00:17:40,000
by the impure mind,

335
00:17:40,000 --> 00:17:43,000
and we come to differentiate,

336
00:17:43,000 --> 00:17:47,000
and we come to affect the change on our minds.

337
00:17:47,000 --> 00:17:49,000
When we see things as they are,

338
00:17:49,000 --> 00:17:51,000
when we do walking meditation,

339
00:17:51,000 --> 00:17:53,000
or do sitting meditation,

340
00:17:53,000 --> 00:17:57,000
we learn to experience life in an interactive,

341
00:17:57,000 --> 00:17:59,000
rather than a reactive way,

342
00:17:59,000 --> 00:18:02,000
so that when we see and we hear and we smell

343
00:18:02,000 --> 00:18:04,000
and we experience things,

344
00:18:04,000 --> 00:18:06,000
we're able to experience them as an experience.

345
00:18:06,000 --> 00:18:07,000
So someone yells at us,

346
00:18:07,000 --> 00:18:09,000
we experience it as a sound.

347
00:18:09,000 --> 00:18:10,000
Someone hits it, hits us,

348
00:18:10,000 --> 00:18:12,000
we experience it as a feeling.

349
00:18:12,000 --> 00:18:13,000
We don't think of the person,

350
00:18:13,000 --> 00:18:14,000
we don't cling,

351
00:18:14,000 --> 00:18:15,000
we don't hold on.

352
00:18:15,000 --> 00:18:17,000
And because we've seen the suffering,

353
00:18:17,000 --> 00:18:19,000
and this is what the Buddha was referring to,

354
00:18:19,000 --> 00:18:22,000
that the mind that claims,

355
00:18:22,000 --> 00:18:24,000
the mind that is impure,

356
00:18:24,000 --> 00:18:26,000
the mind that is stressed,

357
00:18:26,000 --> 00:18:27,000
has stress in it.

358
00:18:27,000 --> 00:18:29,000
This is what leads to suffering.

359
00:18:29,000 --> 00:18:31,000
Suffering only comes from the mind,

360
00:18:31,000 --> 00:18:34,000
all of the things that we create,

361
00:18:34,000 --> 00:18:36,000
all of the things that we do,

362
00:18:36,000 --> 00:18:41,000
the only have an influence on our minds,

363
00:18:41,000 --> 00:18:43,000
if we cling to them,

364
00:18:43,000 --> 00:18:46,000
if we have some attachment in the mind at that moment.

365
00:18:46,000 --> 00:18:49,000
And this is a warning from the Buddha,

366
00:18:49,000 --> 00:18:52,000
that anything that we do with an impure mind

367
00:18:52,000 --> 00:18:55,000
will have this influence on our lives,

368
00:18:55,000 --> 00:18:56,000
an influence on our minds,

369
00:18:56,000 --> 00:18:58,000
it will lead to greater stress in our minds,

370
00:18:58,000 --> 00:19:00,000
greater clinging, greater suffering.

371
00:19:00,000 --> 00:19:04,000
And just as the cart follows through the foot of the ox.

372
00:19:04,000 --> 00:19:06,000
So this is the meaning of the verse,

373
00:19:06,000 --> 00:19:07,000
and the Buddha said,

374
00:19:07,000 --> 00:19:09,000
in fact, all things come from the mind.

375
00:19:09,000 --> 00:19:12,000
If you act with an impure mind,

376
00:19:12,000 --> 00:19:13,000
it will lead you to suffering,

377
00:19:13,000 --> 00:19:16,000
if you want to know where suffering comes from.

378
00:19:16,000 --> 00:19:19,000
This is where it and all other things come from,

379
00:19:19,000 --> 00:19:20,000
from the mind.

380
00:19:20,000 --> 00:19:23,000
And this is the first verse of the Damabada.

381
00:19:23,000 --> 00:19:25,000
So I'd like to thank you for tuning in,

382
00:19:25,000 --> 00:19:28,000
and I hope that we will have many more of these,

383
00:19:28,000 --> 00:19:32,000
and I'll be able to get through all 423 verses

384
00:19:32,000 --> 00:19:36,000
before my time is up.

385
00:19:36,000 --> 00:19:38,000
So thank you for tuning in again,

386
00:19:38,000 --> 00:20:02,000
and wish you all the best.

