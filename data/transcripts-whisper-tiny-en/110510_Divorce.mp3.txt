Okay, welcome back to Ask a Monk.
Next question is about divorce.
I was asked the question about how to deal with a divorce when you are a third party,
or in this case, your parents are divorced or divorcing in the process of it.
How do you deal with it?
And I guess, as always, we should put the question in context.
And because here we are talking about several different things.
We are talking about the idea of family and one's parents.
We are talking about the concept of marriage.
And we are talking about suffering.
So these are all three of the issues that come up in relation to divorce.
The concept of a family has to do with all of those concepts that most of us seem,
most of us find to seem or that seem to most of us to be ultimately real.
For instance, being human.
We think that humanity is something real.
And we think of our gender as being something real.
And as a result, these concepts form our opinion, our ideas about reality,
our ideas of family, our idea, the concept of having parents.
And I see these are concepts, and I think they should be broken apart.
They should be broken down and understood to be simply concept.
Reality doesn't admit of any of these concepts.
But in an ultimate sense, there is no human, there is no gender, there is no family,
there is no mother and father and so on.
And I'm going to get in trouble for that.
Because on a conventional level, people understand that there is mother and father.
If I say there is no mother and father, then people are going to say,
what that means we shouldn't respect our parents, or we shouldn't be thankful for the things they've done.
And obviously that's not the case.
But on an ultimate, ultimate reality doesn't admit of these things.
And when we can break away from these concepts,
then we can break away from a lot of the suffering that comes from them.
So, for instance, in this case, our attachment to our parents,
we understand them to be a specific entity.
Our father is thus, our mother is thus, our family is this unit.
There is an entity that we call our family.
There is a relationship that we call family, that we call blood relationship and so on.
But simply looking at, if we examine this from a scientific point of view,
it's easy to see the falsehood of them.
I mean, as human beings, our understanding is that by blood we're related to each and every one,
each and every human on the planet, by blood ties, so we're all family.
Obviously this is a beneficial realization as well,
because it breaks down all of the cultural barriers, all of the racism and prejudice and so on.
The us and them, mentalities that pervade the world.
But whether it's useful or not, it's the truth.
The reality is that there is no barrier or boundary where we can see this is family and this is not.
It's just a matter of degree.
And that degree is important in one sense, in terms of our understanding of each other,
our ability to relate to each other, our knowledge of each other's past and so on.
And this is the relationship we have with their parents.
There is a, there are many emotions and many memories and many ultimate realities that are tied up with this concept of family.
Once we have parents, then we have many, many attachments that go along with that.
We have many comforts and supports.
There's many beneficial things that come obviously from having a stable family unit.
But something that will help us to deal with divorce is to understand that there is really no change occurring,
apart from the actual suffering that comes from a divorce, where you lose the stability,
where you lose the cohesion, where you lose the support of having two parents and a family unit.
There's nothing wrong.
There's nothing wrong.
There's no problem, except in your own mind, where you've had your life disrupted.
You've had your way of looking at reality change.
And in fact, it's in a way that seems like something that was very real and whole has broken into,
has broken into pieces.
When in fact that's not the case, because the idea of a family unit is in our mind, it's a concept.
If our parents split up, it just means that this entity, this mass of physical and mental realities has gone here,
and this one has gone here, and they're relating to each other in a different way.
The problem comes with our attachment, and so this gets into understanding of suffering.
The other concept I'd like to talk about is the one of marriage, because it relates to the first one.
It relates to the idea of having parents.
So our reliance on our parents is one attachment that we have.
It's something that we've misled ourselves from our very birth.
We've been misleading ourselves into thinking that there was something stable about the family unit.
And throughout our lives, that stability will be eroded or shattered or taken away from us.
And it will generally lead to a great amount of suffering.
And as a result, learning, understanding that what we thought was stable is not in fact stable.
And it will allow us to open our minds up to the idea that life is transient, that we shouldn't cling to things.
And as a result, we'll be able to accept change better.
Most people come to this.
So they come to a realization and they come to wisdom through the suffering and realizing that they were setting themselves up for this,
through their ignorance, through their misunderstanding that there was stability.
Marriage is another concept, and it's also tied up with a lot of clinging, obviously.
So the idea of a divorce is one that we think of as breaking something up.
That there is something that has been shattered with the marriage.
And we feel bad about that even if we're not involved in it.
We also feel like there is something incomplete.
There's some knot that has been untied and you have to tie it back together and so on.
But marriage is simply a contract.
It's a conventional agreement between two people.
And that agreement has to do with morality.
It has to do with not cheating on each other.
It has to do with supporting each other and so on.
So there isn't that mutual support anymore.
These people have come to an agreement that they're not going to act in those same ways anymore.
It's really divorce is nothing. It's just a change of agreement, a change of our relationships.
It's like leaving one job and going to another job.
It's simply a change.
So again, a lot of the suffering that comes from divorce just comes from our attachment to concepts,
the idea of the solid family.
And the idea of stability, which is a false one, in the end we have to accept the fact that reality is impermanent.
Everything is changing.
And if we, whatever we cling to, it's only going to lead us to suffering because it's not stable.
So this is one side of things.
And if this is causing you suffering, then you should immediately wipe that out of your mind and move on.
The real suffering that comes from divorce, and this is one that is much more difficult to overcome,
is the negative emotions that are caused by divorce.
The anger, when the greed, when there's jealousy involved with a divorce.
So anger comes when you are blaming each other for the divorce and so on.
There's guilt involved with it, the guilt in relation to one's children and so on.
There is a lot of stress involved with the work that's required to split up.
And there's a lot of greed involved in terms of splitting up the belongings of the family, who gets what and how much.
And then lawyers get involved and there's more stress.
There is jealousy in terms of competing for the love of the children, trying to not be seen as the bad guy.
And so you'll have both parents vying for the love of their children saying how great they are and how trying to show off.
And because they're afraid that the other parent is doing the same thing and it's a competition because they don't want to look like the bad guy.
And they care what their children think and they're afraid their children are going to judge them.
And it's like this when two people are vying for the love of one person.
So the children get caught in the middle and the parents fight using the children.
And that can be a great amount of suffering for both the parents and the children obviously.
The children feel terrible having to be put in the middle and obviously they don't want to choose between their parents.
So how do you deal with these sufferings?
Once you can come to terms with the idea of the divorce and overcoming the concept of the life that you had that has now been shattered.
This is where obviously meditation is of great benefit.
So if you begin to practice meditation, if you're able to look at the experience as it is, I think it'll be essential and the incredibly beneficial during that time.
You have to understand that it's going to be a specific length of time.
Eventually your parents are going to move on.
They may already have moved on.
It's been a while since this question was posted.
But whatever suffering comes from the divorce or even long-term suffering in terms of not having the same stability that you had before has to be ameliorated through understanding, through the ability to accept the reality in front of you.
Just because it's different doesn't mean it's bad, just because one is stressed and has headaches and is having to deal with difficult, more difficult situation than before, even having to work more or work to support one's family, work to support one's self and so on.
That doesn't mean that one has to suffer if you can accept and live with the reality and not cling to your idea of how it should be or be upset by disappointment, by not getting things the way you think they should be.
When you're able to simply understand things as they are and to see that this is what's happening, when your relationship with reality is not, I wish it could be some other way or I hope it stays this way, your relationship with reality is it is this way.
And every moment knowing that now it is this way, now it is this way, when you're able to think in that way that this is reality, this is reality, and you see things and you know that you're seeing, you hear things and you know that you're hearing and your relationship with reality is an understanding and an acceptance and a clear awareness of it as it is with no thoughts of the past or the future in terms of regret.
Or worry or feelings of loss and so on.
Then you will be, obviously you will be far better off.
This is clearly the preferable path and so this is really the way out of suffering.
This is the path that the Buddha taught.
So you can take a look at some of the videos I posted on how to practice meditation.
I think it will be of great help to anyone suffering from this sort of stress or any other stress.
And I hope those are views quickly.
If you haven't looked at those videos, then it's a very simple practice to see things as they are instead of wishing they were some other way or holding on to these things and hoping that they'd stay this way.
You simply remind yourself that it is what it is.
You don't put any value judgment on it.
When you see something, you say to yourself, seeing, knowing that this is seeing.
When you hear hearing, when you smell, you say to yourself, smelling.
When you feel angry, you say to yourself, angry.
When you feel sad, you say to yourself, sad.
When you feel pain or headache, there's one of them pain or headache, aching, aching is right.
Simply knowing it for what it is, this teaches you how to accept and to understand reality for what it is instead of wishing it were some other way or holding on.
Or holding on to and clinging to it and setting yourself up for greater suffering.
So, but I'd encourage you to look at the videos that I've posted and if you have time to read the book, I have a book on my web blog that's better than the videos.
At least in terms of the information, it contains and it should give a basic understanding of how to practice meditation in the tradition that I follow.
So, thanks for the question, hope that helped are the best.
