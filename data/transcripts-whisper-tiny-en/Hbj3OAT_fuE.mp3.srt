1
00:00:00,000 --> 00:00:10,320
Next question comes from Zach Earthworm. In one video you mentioned that there is a popular

2
00:00:10,320 --> 00:00:15,160
notion that is a myth, that is that people who are bad get along well, while people who

3
00:00:15,160 --> 00:00:20,520
are good don't succeed. What can you say about the similar myth as in the adage ignorance

4
00:00:20,520 --> 00:00:32,720
is bliss. Ignorance is a word that can be interpreted in different ways and can be used

5
00:00:32,720 --> 00:00:39,480
in different ways. I would say we acknowledge generally that there are two kinds of ignorance.

6
00:00:39,480 --> 00:00:48,640
There are ignorance of ordinary things and then there is ignorance that leads to defilement,

7
00:00:48,640 --> 00:00:56,800
leads to greed or anger that leads us to evil. Ignorance of ordinary things of facts

8
00:00:56,800 --> 00:01:04,400
is like not knowing mathematics or geography or history or so on and not knowing people's

9
00:01:04,400 --> 00:01:13,520
names or so being ignorant even of customs and traditions and so on, being ignorant of concepts.

10
00:01:13,520 --> 00:01:18,680
Being ignorant of ultimate reality is the one that we are concerned about, being ignorant

11
00:01:18,680 --> 00:01:29,360
about the nature of reality. So is ignorance bliss? I would say ignorance of certain concepts

12
00:01:29,360 --> 00:01:38,640
can be blissful because it means you don't have to involve yourself in that field. It

13
00:01:38,640 --> 00:01:46,120
can often absolve you from any need to help people or so on. I would say people who are

14
00:01:46,120 --> 00:01:51,320
simply concerned on their own happiness can find a great amount of bliss from not studying

15
00:01:51,320 --> 00:01:58,400
and not learning about worldly things. People who are concerned with finding peace and

16
00:01:58,400 --> 00:02:07,840
happiness don't have to study, engage themselves in learning things. But if you want to

17
00:02:07,840 --> 00:02:14,640
be happy even just for yourself or even if you want to help others, you have to give up

18
00:02:14,640 --> 00:02:20,640
the ignorance in regards to reality. What we are trying to do in Buddhism is do away with

19
00:02:20,640 --> 00:02:25,480
the ignorance which obviously gets in the way of bliss and gets in the way of happiness,

20
00:02:25,480 --> 00:02:32,720
gets in the way of peace and that is ignorance of the way things work of reality. Specifically

21
00:02:32,720 --> 00:02:36,160
in Buddhism we talk about ignorance in regards to four things and these are the four

22
00:02:36,160 --> 00:02:43,280
noble truths. Ignorance in regards to suffering. That means we see certain objects as being

23
00:02:43,280 --> 00:02:48,960
desirable, as being satisfying, that there are certain things in this world that are going to

24
00:02:48,960 --> 00:02:53,840
when we attain them or obtain them are going to satisfy. So we are going to make us truly happy

25
00:02:53,840 --> 00:02:59,600
and that is simply not the case. So it is the case of ignorance. The second one is ignorance

26
00:02:59,600 --> 00:03:10,800
of the cause of suffering. Ignorance that the arising of our state of upset our state of suffering

27
00:03:10,800 --> 00:03:20,640
of pain, of discomfort and so on have a cause and what that cause is. So often we think the cause

28
00:03:20,640 --> 00:03:26,800
is the object itself and we will try to run away from it. We will think that the cause is

29
00:03:26,800 --> 00:03:35,040
someone else or the cause is the experience, but the Buddha thought that the cause is our attachment

30
00:03:35,040 --> 00:03:39,440
to the object, our needing for things to be a certain way or to not be a certain way,

31
00:03:40,080 --> 00:03:46,640
needing for certain objects of the sense and for the absence of other objects of the sense.

32
00:03:47,920 --> 00:03:56,160
And so ignorance not knowing this is something that obviously leads us to give rise to these

33
00:03:56,160 --> 00:04:02,480
states when we know that clinging and needing now leads to suffering then we stop needing,

34
00:04:02,480 --> 00:04:10,160
we stop desiring and we stop suffering. The third one is ignorance of the cessation of suffering,

35
00:04:10,160 --> 00:04:16,720
which is when there is no more wanting, there is no more suffering. And number four is

36
00:04:16,720 --> 00:04:23,280
ignorance of the path. So ignorance of the way that leads us to become free from suffering and that

37
00:04:23,280 --> 00:04:30,480
is the clear awareness and understanding of things as they are. So this is the only real wisdom

38
00:04:30,480 --> 00:04:36,560
that we need and this is the understanding of things as they are, the understanding of reality.

39
00:04:37,440 --> 00:04:43,920
You don't have to understand everything, you don't have to know everything to be blissful,

40
00:04:43,920 --> 00:04:50,320
to be happy and often ignorance of many things as I said can be blissful. But ignorance of these

41
00:04:50,320 --> 00:04:55,520
four things is never blissful and will certainly not bring you peace or happiness. Okay,

42
00:04:55,520 --> 00:05:25,360
so thanks for the question, hope that works for an answer.

