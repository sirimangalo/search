1
00:00:00,000 --> 00:00:02,760
Hi.

2
00:00:02,760 --> 00:00:08,660
This video is just a update on my series called Ask a Monk

3
00:00:08,660 --> 00:00:13,200
to let everyone know that you can now submit questions.

4
00:00:13,200 --> 00:00:18,200
I'd ask that you submit questions to Google Moderator.

5
00:00:18,200 --> 00:00:20,480
It's a service provided by Google that

6
00:00:20,480 --> 00:00:23,440
allows people to ask questions and for them

7
00:00:23,440 --> 00:00:29,040
to be rated by other viewers or other users.

8
00:00:29,040 --> 00:00:32,360
And this will make the answering process a lot easier.

9
00:00:32,360 --> 00:00:36,680
It'll let me know which questions are most popular.

10
00:00:36,680 --> 00:00:40,760
And it will allow me to organize the questions in one

11
00:00:40,760 --> 00:00:41,400
area.

12
00:00:41,400 --> 00:00:45,080
I'd ask that you use only this service to ask questions.

13
00:00:45,080 --> 00:00:48,280
And if it works out well, I'll use this.

14
00:00:48,280 --> 00:00:51,760
And only this service to answer questions.

15
00:00:51,760 --> 00:00:53,560
So please, if you have any questions,

16
00:00:53,560 --> 00:00:57,400
submit them to the Google Moderator page called Ask a Monk.

17
00:00:57,400 --> 00:01:01,560
And I'll try to get answers many of them as I can.

18
00:01:01,560 --> 00:01:02,040
OK.

19
00:01:02,040 --> 00:01:03,040
Thanks.

20
00:01:03,040 --> 00:01:23,560
See you.

