1
00:00:00,000 --> 00:00:09,720
Good evening everyone, welcome to our live broadcast.

2
00:00:09,720 --> 00:00:22,120
It seems to me that a discussion about not broadcasting every night was understood to actually

3
00:00:22,120 --> 00:00:23,120
be a decision.

4
00:00:23,120 --> 00:00:25,160
It wasn't a decision.

5
00:00:25,160 --> 00:00:29,960
We're still going to do or I'm still going to do daily broadcasts.

6
00:00:29,960 --> 00:00:41,440
At least for the near future, if any change comes on that channel, but I think nightly

7
00:00:41,440 --> 00:00:44,400
broadcasts is fine.

8
00:00:44,400 --> 00:00:49,440
This is as long as you're all fine with some of them being short, sometimes I'll just

9
00:00:49,440 --> 00:00:54,960
come on and say good evening and give some brief teaching or some nights maybe just answer

10
00:00:54,960 --> 00:00:55,960
questions.

11
00:00:55,960 --> 00:01:01,400
It's very easy for me to sit here and answer questions to actually come up with something

12
00:01:01,400 --> 00:01:02,400
to talk about.

13
00:01:02,400 --> 00:01:10,480
Well, some nights it might be, I might just have other things on my plate.

14
00:01:10,480 --> 00:01:24,440
Anyway, tonight we are looking at a good learning guy, a book of three is number 49 or

15
00:01:24,440 --> 00:01:25,440
50.

16
00:01:25,440 --> 00:01:26,440
I don't know.

17
00:01:26,440 --> 00:01:31,960
The English says 49, the poly says 50, so if you're following along with Pico Bodhi, it's

18
00:01:31,960 --> 00:01:36,680
number 49 in the book of three.

19
00:01:36,680 --> 00:01:47,080
Our door, as the Americans say, our door without the U, if it looks funny, it's because

20
00:01:47,080 --> 00:01:50,440
I'm not American.

21
00:01:50,440 --> 00:01:56,760
After our door is an interesting word, it's, it'd be nice to have a different one because

22
00:01:56,760 --> 00:02:02,160
our door is, has different connotations in English, but it's the best because the word

23
00:02:02,160 --> 00:02:09,720
atapi, or atapi, or atapi, or atapi, right, atapi.

24
00:02:09,720 --> 00:02:19,760
Atapi comes from temperature, right, that's the same word as, the same root as the English

25
00:02:19,760 --> 00:02:39,120
word temperature, you know, atapi comes from atapi, atapi comes atapi, so the idea is

26
00:02:39,120 --> 00:02:47,800
in India, exertion was seen as, was described as being energetic, so in English we have

27
00:02:47,800 --> 00:02:52,160
that with ardor, right, but we don't use it exactly in the same way.

28
00:02:52,160 --> 00:02:58,520
We don't think of work as, or effort as, as getting hot about something, I think of getting

29
00:02:58,520 --> 00:03:05,200
hot about something, or he did up his either to do a lust or anger, which is not exactly

30
00:03:05,200 --> 00:03:14,800
what, not at all what the Buddha is referring to or not how it's used in poly.

31
00:03:14,800 --> 00:03:25,720
But ardor can also mean energy or energetic effort, so the meaning here is atapi, atapi,

32
00:03:25,720 --> 00:03:37,000
bangarani, there's the Buddha says there are three, three, three things or three subjects,

33
00:03:37,000 --> 00:03:46,040
three topics on which one should cultivate atapi, atapi, bangarani, yangarani means should

34
00:03:46,040 --> 00:03:54,960
be done, atapi's, ardor or effort.

35
00:03:54,960 --> 00:04:04,040
And I think this is fairly interesting teaching, I mean the Buddha taught different reactions

36
00:04:04,040 --> 00:04:13,720
to the various problems that we are challenges that we're facing, the various aspects

37
00:04:13,720 --> 00:04:16,240
of the cause of suffering.

38
00:04:16,240 --> 00:04:22,200
So in certain instances one should exert one's self, in certain instances one should

39
00:04:22,200 --> 00:04:36,960
restrain one's self, in certain instances one should continue, one should not push or

40
00:04:36,960 --> 00:04:46,240
not retreat, but in certain cases one should simply continue the way one is going.

41
00:04:46,240 --> 00:04:58,160
And in certain cases one should cultivate dispassion, so sort of not give, take out the

42
00:04:58,160 --> 00:05:06,520
zeal for something, so not exactly restraint, but abandon, I guess, anyway, different

43
00:05:06,520 --> 00:05:09,400
ways of looking at how you deal with defilements.

44
00:05:09,400 --> 00:05:15,160
And I think that's a valid thing to say, because here it only tells part of the story,

45
00:05:15,160 --> 00:05:22,160
and it tells the part of the story when these three things, where the salient quality

46
00:05:22,160 --> 00:05:26,640
or the outstanding quality is effort.

47
00:05:26,640 --> 00:05:30,920
And so whether that means anything or not, but it appears that there are these three

48
00:05:30,920 --> 00:05:45,640
are especially relating to exertion, because actually there are four great exertions, right?

49
00:05:45,640 --> 00:05:50,120
But here they don't, here are only two are mentioned, so the Buddha says, in regards

50
00:05:50,120 --> 00:06:04,520
to aneurysm evil states, one should cultivate effort for their non-arising, if they

51
00:06:04,520 --> 00:06:09,960
haven't arisen, one should work hard to make sure they don't arise.

52
00:06:09,960 --> 00:06:19,400
For aneurysm wholesome numbers, one should work hard for their arising, and one should

53
00:06:19,400 --> 00:06:34,800
work hard for cultivating anti-wasanaya, anti-wasanaya, anti-wasanaya, which means bearing

54
00:06:34,800 --> 00:06:45,720
with wasanaya means to live, or to dwell, and anti-means on top of, or with while they're

55
00:06:45,720 --> 00:07:01,200
existing, one should cultivate forbearance or ability to bear with a risen painful feelings

56
00:07:01,200 --> 00:07:11,240
in the body that are harsh and unpleasant, racking, sharp, piercing, harrowing, disagreeable,

57
00:07:11,240 --> 00:07:18,520
wrapping one's vitality, sapping one's vitality, that's interesting, sapping one's vitality

58
00:07:18,520 --> 00:07:28,800
should probably read, banaharang should probably read life threatening, I think sapping

59
00:07:28,800 --> 00:07:39,640
one's vitality is, it's not how we like to translate this, the point is even when the painful

60
00:07:39,640 --> 00:07:46,680
feelings could potentially kill you, even when they seem deadly, I don't know, that's

61
00:07:46,680 --> 00:07:55,200
how it's normally translated, bikabodhi has chosen banaharang, banaharang to mean sapping

62
00:07:55,200 --> 00:08:00,960
vitality, which is, I'm not convinced, I think the Buddha is trying to make a point

63
00:08:00,960 --> 00:08:10,200
that, why don't you cultivate the ultimate equanimity, I'm not quite sure why these three,

64
00:08:10,200 --> 00:08:15,920
the thing about the Ingutarnikai is sometimes the, in the book of twos, threes, even

65
00:08:15,920 --> 00:08:23,200
fours and fives, you have lists that are incomplete, and I think generally the reason

66
00:08:23,200 --> 00:08:30,400
why they're incomplete is given as being that in later books, well later books there, there

67
00:08:30,400 --> 00:08:37,520
are more of them, but that in certain instances, only certain qualities were important

68
00:08:37,520 --> 00:08:45,320
for the audience, the Buddha tailored his teaching according to the audience, so sometimes

69
00:08:45,320 --> 00:08:49,200
he would highlight certain teachings, either that or as I said it might be that these

70
00:08:49,200 --> 00:08:56,440
ones are specifically relating to effort, because you've got the under reason unwholesome

71
00:08:56,440 --> 00:09:02,560
states, so there's the, when you have the potential still to get angry about things, when

72
00:09:02,560 --> 00:09:07,920
you have the potential still to get attached to things, you work hard to stay mindful

73
00:09:07,920 --> 00:09:14,400
so that that doesn't happen, right, but then the same goes with a reason unwholesome

74
00:09:14,400 --> 00:09:20,080
states, if evil has arisen in your mind, if you are angry, if you are addicted to something,

75
00:09:20,080 --> 00:09:25,520
when they do arise, you strive to be mindful, to not let them continue to cut them off,

76
00:09:25,520 --> 00:09:41,240
to take away their nourishment, that which is feeding them, which is the repeated application

77
00:09:41,240 --> 00:09:50,080
of the mind to the same state of mind, by changing it to objectivity, to seeing it

78
00:09:50,080 --> 00:09:57,160
from a reason, so on, and so you have it both ways, whether it's an unarising, don't let

79
00:09:57,160 --> 00:10:03,520
them arise, arisen or cut them off, and the same goes with wholesome, here the Buddha talks

80
00:10:03,520 --> 00:10:12,680
about giving rise to wholesome is, work hard to make wholesome qualities arise, so through

81
00:10:12,680 --> 00:10:18,080
mindfulness, mindfulness is something we work hard to make arise, but through mindfulness,

82
00:10:18,080 --> 00:10:22,520
through the hard work, we knew of mindfulness, we work hard to cultivate all sorts of wholesome

83
00:10:22,520 --> 00:10:41,400
states, wisdom, concentration, effort, confidence, we work hard to cultivate wholesome states,

84
00:10:41,400 --> 00:10:45,720
but the same goes with wholesome states and have already arisen, you have to work hard

85
00:10:45,720 --> 00:10:52,080
to keep them, so there are four right efforts, based on unarising or risen wholesome and

86
00:10:52,080 --> 00:11:00,400
unwholesome, here the Buddha only talks about two of them, which is curia, but it's

87
00:11:00,400 --> 00:11:14,120
relating to things of unarising, and the most important point here is that what is nice

88
00:11:14,120 --> 00:11:19,200
about this teaching instead, you've got these effort in regards to wholesomeness, effort

89
00:11:19,200 --> 00:11:25,760
in regards to unwholesomeness, but what about in regards to your circumstance?

90
00:11:25,760 --> 00:11:31,600
The third one, what this teaching points out is that we're not exerting effort to change

91
00:11:31,600 --> 00:11:35,320
our circumstance, right, because what's the third one, the third one is being patient with

92
00:11:35,320 --> 00:11:38,240
everything else, basically.

93
00:11:38,240 --> 00:11:42,160
What are we working hard to change, people would say, you know, what do you work for

94
00:11:42,160 --> 00:11:46,240
in life, well, you want it to be happy to get rid of your suffering, right?

95
00:11:46,240 --> 00:11:51,160
Not really, in Buddhism that's not actually the focus of our practice, you know, we talk

96
00:11:51,160 --> 00:11:56,000
about being free from suffering and being happy, and that's kind of the goal, you could

97
00:11:56,000 --> 00:12:03,920
say, but I mean, it definitely is the goal, but in a practical sense, it's not the present

98
00:12:03,920 --> 00:12:10,240
goal, it's not what we're trying to do, it's not how we're approaching experience.

99
00:12:10,240 --> 00:12:14,920
When they're suffering, you're in no way, shape, or form trying to work hard to get rid

100
00:12:14,920 --> 00:12:15,920
of it.

101
00:12:15,920 --> 00:12:16,920
Absolutely not.

102
00:12:16,920 --> 00:12:22,920
In fact, it would be to your detriment, in two ways, it would be to your detriment because

103
00:12:22,920 --> 00:12:26,640
of the quality of a version that it would cultivate, it would also be to your detriment

104
00:12:26,640 --> 00:12:31,040
because suffering is what's going to teach you.

105
00:12:31,040 --> 00:12:34,320
If you get rid of suffering, or if you try to get rid of suffering, not only are you

106
00:12:34,320 --> 00:12:39,400
going to cultivate a version, but you're also missing on an opportunity, an opportunity

107
00:12:39,400 --> 00:12:45,240
to see a version, to see how you react to suffering, and to learn the difference between

108
00:12:45,240 --> 00:12:49,520
suffering and a version, that suffering is just suffering, or experiences like pain and

109
00:12:49,520 --> 00:12:54,480
so on, or just experiencing.

110
00:12:54,480 --> 00:12:59,880
So the real point, I think that we should highlight in this teaching, is that all we have

111
00:12:59,880 --> 00:13:06,280
to focus on is wholesome and unwholesome, or not concerned about pain or happiness, happiness

112
00:13:06,280 --> 00:13:12,040
and pain, happiness and suffering, these are to be seen as experiences.

113
00:13:12,040 --> 00:13:18,760
They arise and they cease, they're not to be judged or reacted to, and that's another

114
00:13:18,760 --> 00:13:23,960
thing that's missing here from the third one, and the third teaching, the Buddha talks

115
00:13:23,960 --> 00:13:31,560
simply about being patient with unpleasant feelings, but what we don't think of too often

116
00:13:31,560 --> 00:13:37,400
is the Buddha's teaching on how we have to be patient with pleasant feelings.

117
00:13:37,400 --> 00:13:41,680
And this is a very important part of the Buddha's teaching.

118
00:13:41,680 --> 00:13:47,360
Not only do we have to be patient with suffering, but we have to be what one would call

119
00:13:47,360 --> 00:13:57,920
patient with happiness, and it's the same state and the same quality of mind, because

120
00:13:57,920 --> 00:14:08,480
it's a reaction, either way, it's the changing of the mind to give up our inclination

121
00:14:08,480 --> 00:14:13,120
to react from when we experience unpleasant feelings.

122
00:14:13,120 --> 00:14:23,280
Our inclination is to immediately retreat, recoil from when we experience pleasant feelings

123
00:14:23,280 --> 00:14:32,520
are immediate inclinations, to seek them out, to incline towards them, to gravitate

124
00:14:32,520 --> 00:14:42,240
towards them, and true and patience is being able to deal with both, being able to bear

125
00:14:42,240 --> 00:14:48,040
with both, so it's like the dog, you know, this trick where you put a bone, a milk bone

126
00:14:48,040 --> 00:14:54,080
on the dog's nose, and if the dog is really well-trained, it won't snap it, and it'll sit

127
00:14:54,080 --> 00:15:00,640
there and wait until the owner says, okay, go for it, and then it will leave it.

128
00:15:00,640 --> 00:15:06,960
Training the dog, training the mind to be like, to be well-trained, just like you train

129
00:15:06,960 --> 00:15:16,280
the dog, you know, training the mind to let go of it, and patience is such an important

130
00:15:16,280 --> 00:15:22,080
part of the practice, it's very much the feeling that a meditator should have throughout

131
00:15:22,080 --> 00:15:27,480
the practice that they're being patient, and it feels like they're burning off the

132
00:15:27,480 --> 00:15:28,480
file.

133
00:15:28,480 --> 00:15:32,960
It's another good thing about Adhapi, Adhapi, and traditional Buddhist circles is described

134
00:15:32,960 --> 00:15:39,960
why we use the word relating to temperature is because you're burning up the file.

135
00:15:39,960 --> 00:15:46,240
And patience is the great way, what it said, Kanti, but among the devotee-dika, patience is the

136
00:15:46,240 --> 00:15:52,920
best form of dapa, dapa being another form of the word Adhapi, it's the same root, same

137
00:15:52,920 --> 00:16:01,080
basic form, dapas, dapas is really means temperature or heat, but what you're doing is

138
00:16:01,080 --> 00:16:05,400
you're burning up to filements and you burn them up through patience, when you want something

139
00:16:05,400 --> 00:16:08,760
and instead you're patient about it.

140
00:16:08,760 --> 00:16:15,280
You could feel that even the physical, the chemicals will bubble and boil inside all of

141
00:16:15,280 --> 00:16:26,640
these chemicals waiting to engage and bring about states of pleasure, and when something

142
00:16:26,640 --> 00:16:35,480
unpleasant comes, there's the tension in the body and you can feel the tension in the

143
00:16:35,480 --> 00:16:44,800
habitual reactivity, waiting to strike, waiting to get upset, waiting to get angry, and

144
00:16:44,800 --> 00:16:46,800
instead you're patient with it.

145
00:16:46,800 --> 00:16:52,840
That's what you should feel in the meditation, you should feel patient, you should feel

146
00:16:52,840 --> 00:16:56,760
like you're burning up to the filements just by sitting with them, just by sitting with

147
00:16:56,760 --> 00:17:02,120
the things that normally make you react.

148
00:17:02,120 --> 00:17:09,480
So, three types, three types of effort, if you want to learn about effort, effort does

149
00:17:09,480 --> 00:17:15,840
it relates to wholesomeness, effort as it relates to unwholesomeness, and effort as it

150
00:17:15,840 --> 00:17:20,880
relates to being patient with everything else, because the only two things that are really

151
00:17:20,880 --> 00:17:27,600
important are wholesomeness and unwholesomeness, called debating wholesomeness, destroying

152
00:17:27,600 --> 00:17:33,440
unwholesomeness, and being patient with everything else, which is really the same thing,

153
00:17:33,440 --> 00:17:36,040
it's a part of the same thing.

154
00:17:36,040 --> 00:17:39,280
So, that's our teaching for tonight.

155
00:17:39,280 --> 00:17:45,520
I don't know if Robin, oh, Robin didn't come tonight, maybe she took it seriously that

156
00:17:45,520 --> 00:17:51,400
we're not supposed to have, but I said we're not going to have them every night, it doesn't

157
00:17:51,400 --> 00:17:52,400
matter.

158
00:17:52,400 --> 00:17:58,560
I think we don't have any old questions, I did scroll through, and I think our first

159
00:17:58,560 --> 00:18:06,800
one is this evening from Granny's, when I started to think about the knowledge in your

160
00:18:06,800 --> 00:18:12,720
videos and then note that I am thinking, the act of noting stops the thinking, I can't

161
00:18:12,720 --> 00:18:18,680
not think at the same time, but don't I want to be able to think about the knowledge,

162
00:18:18,680 --> 00:18:26,280
no, not really, I would say the knowledge is meant to evoke something in you, and that

163
00:18:26,280 --> 00:18:30,520
to trigger something, and I would say that's enough.

164
00:18:30,520 --> 00:18:33,920
The point is to encourage you to say to yourself, thinking, thinking that's where the point

165
00:18:33,920 --> 00:18:39,880
of all the teaching, if you consider what I was teaching just now, hopefully the understanding

166
00:18:39,880 --> 00:18:45,360
you get from it is that, oh, now I have to cultivate mindfulness, so I have to work harder

167
00:18:45,360 --> 00:18:49,680
at cultivating mindfulness.

168
00:18:49,680 --> 00:18:54,680
I would say if you have to spend time mulling it over thinking about it, you know, in

169
00:18:54,680 --> 00:18:59,400
a worldly sense that's fine, and it's important for things like teaching or things

170
00:18:59,400 --> 00:19:05,080
like explaining it to others, figuring out what is the best way to explain, I mean the

171
00:19:05,080 --> 00:19:12,240
Buddha, it's not that the Buddha didn't think or that our hands don't think, and I guess

172
00:19:12,240 --> 00:19:18,760
that's the point is that you're not always, you know, not necessarily always supposed to

173
00:19:18,760 --> 00:19:19,760
be meditating.

174
00:19:19,760 --> 00:19:22,600
I mean there are other times where you want to do other things, sort of what you want

175
00:19:22,600 --> 00:19:28,040
to teach someone, instead of meditating you have to think, you know, you have to meditate

176
00:19:28,040 --> 00:19:31,640
as well, but there will be a portion of time where you have to think, when am I going

177
00:19:31,640 --> 00:19:37,560
to teach tonight, you know, of course, much more important is that you're meditating,

178
00:19:37,560 --> 00:19:44,760
there's no question there, but for things like teaching, explaining to others, and even

179
00:19:44,760 --> 00:19:50,120
sometimes for stepping back and evaluating your practice, sometimes you have to think,

180
00:19:50,120 --> 00:19:54,160
so at that time you don't meditate, that's all.

181
00:19:54,160 --> 00:20:01,760
But it's not really, none of that's really necessary if you're just able to be mindful,

182
00:20:01,760 --> 00:20:06,120
or able to say to yourself thinking, it disappears, well that's great, you're starting

183
00:20:06,120 --> 00:20:09,920
to see that everything arises in seasons, you're starting to teach yourself, and even

184
00:20:09,920 --> 00:20:18,760
our minds are arising and ceasing all the time, but no, there will just be times where

185
00:20:18,760 --> 00:20:25,800
you just think, so don't worry about that, at that time you're not meditating.

186
00:20:25,800 --> 00:20:34,120
Wow, one question tonight, I think I scared everyone away by saying I wasn't going

187
00:20:34,120 --> 00:20:43,520
to do it every night, don't see, I can wait a couple of minutes, because there's a delay

188
00:20:43,520 --> 00:20:50,040
I think, so I have to wait for, wait a minute, at least, more questions come on, so look

189
00:20:50,040 --> 00:21:01,120
we got a big long list of meditators tonight, must be like 20, more than 20, we got Canadians,

190
00:21:01,120 --> 00:21:20,560
we got Americans, we got India, New Mexico, Sri Lanka, another Mexico, Romania, New

191
00:21:20,560 --> 00:21:33,320
Zealand, Norway, Moritas, is that real, we got someone from, I don't even know where that

192
00:21:33,320 --> 00:21:54,520
is, Moritas, Moritas, and Singapore, all over, alright, then we'll end it there, thank

193
00:21:54,520 --> 00:22:07,040
you all for tuning in, see you all tomorrow, maybe.

194
00:22:07,040 --> 00:22:34,060
Thank you.

