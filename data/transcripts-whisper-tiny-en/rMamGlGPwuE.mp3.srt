1
00:00:00,000 --> 00:00:13,280
Okay so today's talk is going to be about contentment and so here's a quote to start us off

2
00:00:16,560 --> 00:00:23,520
Health is the greatest possession. Contentment is the greatest wealth. Trustworthiness is the

3
00:00:23,520 --> 00:00:41,600
greatest relation and Nirvana is the greatest happiness. So today's talk is on specifically contentment

4
00:00:42,800 --> 00:00:49,360
and how it relates to the Buddhist teaching. I was asked about a week ago to give a talk on this

5
00:00:49,360 --> 00:01:00,960
subject. So here we are. Thank you all for coming. Good to see you on this lovely Sunday afternoon.

6
00:01:00,960 --> 00:01:08,720
Great thing about Second Life is it. Never rains unless you want it to. We can always make use of

7
00:01:08,720 --> 00:01:22,800
this wonderful always burning campfire. So what is contentment? I have to do with the Buddhist teaching.

8
00:01:22,800 --> 00:01:35,280
How does contentment play a part in Buddhism? I was asked to talk about this and so I had to do a

9
00:01:35,280 --> 00:01:51,920
little bit of research. But what I know about contentment in the Buddhist teaching

10
00:01:51,920 --> 00:02:06,320
leads me to understand that there are three kinds of contentment and each one of these three

11
00:02:06,320 --> 00:02:25,680
plays a different role in Buddhism in the Buddhist teaching. The first type of contentment to get it

12
00:02:25,680 --> 00:02:29,600
out of the way is the type of contentment that the Buddha taught against.

13
00:02:29,600 --> 00:02:40,640
So you read in the Buddha's teaching that the Buddha taught if you want to be a great being

14
00:02:42,720 --> 00:02:49,600
if you are striving for true greatness. The Buddha said you have to be full of discontent

15
00:02:49,600 --> 00:02:56,320
and he taught against a certain type of contentment.

16
00:03:06,320 --> 00:03:13,760
And so this type of contentment it is important to talk about because it often pops its

17
00:03:13,760 --> 00:03:25,200
rears its head both in the world and in spirituality as well. This is a contentment with one's

18
00:03:26,720 --> 00:03:36,960
self. I've often said it's a mark of a high mind

19
00:03:36,960 --> 00:03:43,040
that they're constantly working to improve themselves.

20
00:03:46,000 --> 00:03:51,600
I would say it's a mark that someone is just saying that someone is spiritually developed

21
00:03:51,600 --> 00:03:57,200
that they're all constantly trying to develop themselves. That the undeveloped person

22
00:03:58,960 --> 00:04:05,840
is known by their lack of interest in developing themselves. They're feeling that everything's

23
00:04:05,840 --> 00:04:17,520
fine the way it is. That there's no need to develop those things that wise people

24
00:04:17,520 --> 00:04:22,480
encourage us to develop and there's no need to do away with those things that wise people

25
00:04:22,480 --> 00:04:38,400
encourage us to do away with.

26
00:04:52,480 --> 00:04:58,000
Just make sure everyone can hear me here.

27
00:05:11,120 --> 00:05:16,640
Okay, that's a yes. If you can't hear me it's probably because you don't have voice on

28
00:05:16,640 --> 00:05:21,680
either that or you don't have if you can't hear me this is useless.

29
00:05:21,680 --> 00:05:33,280
Can someone explain the reason why you can't hear me? Doesn't do much good to speak to you and tell

30
00:05:33,280 --> 00:05:59,360
you why you can't hear me. So it's a mark of someone who is developing that they

31
00:05:59,360 --> 00:06:04,880
are interested in developing themselves, obviously.

32
00:06:10,400 --> 00:06:17,840
It's one easy way to distinguish between spiritual people and people who are not interested

33
00:06:17,840 --> 00:06:25,840
in spirituality. It's also a way of reassuring ourselves that even when our mental development

34
00:06:25,840 --> 00:06:31,680
is progressing with difficulty that we should be encouraged by the fact that we're trying

35
00:06:32,640 --> 00:06:44,640
and effort in the Buddha's teaching is really a very core value that failure in the meditation

36
00:06:44,640 --> 00:06:51,840
is really only possible when one ceases to practice or ceases to practice correctly.

37
00:06:51,840 --> 00:07:01,360
But if you when you stop trying or when you start putting out effort in another direction

38
00:07:03,200 --> 00:07:12,880
in a way that is antithetical to the path or give up your effort, that's the only way you can

39
00:07:12,880 --> 00:07:19,920
really fail. As long as you're trying, striving, you're watching the breath, watching the stomach

40
00:07:19,920 --> 00:07:27,520
when it rises and falls, for example. But you find you can't focus on it. You try and you try and

41
00:07:27,520 --> 00:07:44,960
it's easy to become frustrated and disheartened. But we shouldn't become discouraged

42
00:07:44,960 --> 00:07:55,760
just because we're not, quote unquote, getting anywhere. Our effort is our discontent and our

43
00:07:55,760 --> 00:08:03,680
intention to improve ourselves, self-improvement is a sign of Buddhism. And so in one way this

44
00:08:03,680 --> 00:08:08,960
distinguishes spiritual people between spiritual people and people who are content with being

45
00:08:08,960 --> 00:08:17,200
a mediocre person. It also distinguishes between two types of spiritual people. The type of person,

46
00:08:17,200 --> 00:08:25,760
the type of spiritual person who is content with their level of spirituality and can often become

47
00:08:25,760 --> 00:08:35,200
content with simple states of bliss or happiness or sort of pseudo-spirituality. Never challenging

48
00:08:35,200 --> 00:08:40,640
one's views and beliefs, practicing only as it's comfortable. So you see often people when

49
00:08:40,640 --> 00:08:45,760
they sit in meditation, they'll sit against the wall or they'll sit in a chair or they'll sit

50
00:08:45,760 --> 00:08:54,880
on a bench or so. And always striving to make the meditation pleasurable. And at any time when

51
00:08:54,880 --> 00:09:06,880
it's not pleasurable, one either stops practicing or adjusts on to practice. These are the kind

52
00:09:06,880 --> 00:09:12,000
of people who when they're sitting in meditation need everybody to be quiet. I had a man ask

53
00:09:12,000 --> 00:09:23,840
me what I thought about using earplugs in meditation. And I don't think, I think it should be

54
00:09:23,840 --> 00:09:35,280
pretty clear that earplugs are a sign that you have some sort of attachment to the sound.

55
00:09:36,480 --> 00:09:41,680
So this sort of contentment is one that we want to try to avoid resting on our laurels,

56
00:09:41,680 --> 00:09:48,160
per se. If you're enlightened and you have no defilements left, then I think this is the only

57
00:09:48,160 --> 00:09:53,680
time one should become content with one spiritual development. But as long as we know that in our

58
00:09:53,680 --> 00:09:59,040
selves we still have more work to do, we should set ourselves in doing that work.

59
00:10:00,640 --> 00:10:05,600
So this is the first sense that we might miss this in thinking that the Buddha taught us to be

60
00:10:05,600 --> 00:10:12,320
content. And so don't worry, don't try, don't do anything. Practicing meditation is too much work.

61
00:10:12,320 --> 00:10:17,760
Going on meditation treats is just torturing yourself. You should just live your life

62
00:10:17,760 --> 00:10:28,720
peacefully and somehow spiritually. The type of contentment that the Buddha did teach and encourage

63
00:10:30,400 --> 00:10:34,560
can be broken up into two types. So there are two other types of contentment that the Buddha

64
00:10:34,560 --> 00:10:42,320
did encourage. And the first type is what he generally referred to as contentment itself,

65
00:10:42,320 --> 00:10:55,920
and the word he uses is santutin, which means being contentment. The Buddha used the word

66
00:10:55,920 --> 00:11:06,480
contentment, he most often was referring to the conceptual objects of our reality. So in Buddhism

67
00:11:06,480 --> 00:11:13,680
we separate reality into two parts, the conceptual and the ultimate. Ultimate reality is

68
00:11:13,680 --> 00:11:20,480
the building blocks of reality. Just like in physical reality and physics they talk about

69
00:11:21,520 --> 00:11:25,600
what are the building blocks, the atoms. And then they had atoms and suddenly there were

70
00:11:25,600 --> 00:11:29,920
subatomic particles and so on. And they're still not sure what is the essence of the physical

71
00:11:29,920 --> 00:11:40,400
in the scientific community. But in Buddhism we have a very good understanding of what are the

72
00:11:40,400 --> 00:11:46,160
essential building blocks. So in the physical we're talking about the four elements and these are

73
00:11:46,160 --> 00:11:54,880
simply the four aspects of matter as it's experienced. Matter can be experienced as hot or cold,

74
00:11:54,880 --> 00:12:02,160
it can be experienced as hard or soft and it can be experienced as tense or flaccid.

75
00:12:06,800 --> 00:12:12,320
The mental side of experience is the awareness of the physical or the awareness of an object.

76
00:12:12,320 --> 00:12:16,000
So when we see something and we know that we're seeing, when we hear something and we know that

77
00:12:16,000 --> 00:12:21,120
we're hearing, this is the ultimate reality. Everything else is conceptual. The chair that you're

78
00:12:21,120 --> 00:12:26,560
sitting in is conceptual. The word chair arises in your mind. You only know that it's a chair

79
00:12:29,360 --> 00:12:36,400
once you process the experience in your mind. So without the processing in the mind there's

80
00:12:36,400 --> 00:12:41,200
simply the feelings that arise from touching the chair. And when you see it there's a scene.

81
00:12:41,840 --> 00:12:47,840
In the same way that the seats, that some of us are sitting on here in the virtual reality are

82
00:12:47,840 --> 00:12:56,400
only conceptual. So the seats in second life that we're seeing are our seats because we process

83
00:12:56,400 --> 00:13:04,000
the site. The seats that we're sitting on in real life are conceptual. There are only seats

84
00:13:04,000 --> 00:13:10,000
because we process the feeling or the site or so on. But at any rate it's still a concept. Our body

85
00:13:10,000 --> 00:13:21,120
is a concept. Because the reality is the experience according to the Buddha. So the first type of

86
00:13:21,120 --> 00:13:26,480
contentment is talking about conceptual contentment, contentment with things, contentment with objects.

87
00:13:28,400 --> 00:13:36,800
This is one of those teachings that is not exactly core. So it's not a theoretical teaching.

88
00:13:36,800 --> 00:13:44,080
It's a practical teaching for everyday life. And for most of us it's a very important teaching.

89
00:13:44,960 --> 00:13:55,280
One of the criticisms of many types of Buddhism, modern Buddhism, is that it often overlooks

90
00:13:55,280 --> 00:14:03,200
practical but conventional teachings, like being content with possessions, being content with

91
00:14:03,200 --> 00:14:08,640
the things that you own with your belongings. And often though people dive right into the

92
00:14:08,640 --> 00:14:15,760
theoretical and talking about ultimate reality and so on. And not that this isn't the most

93
00:14:15,760 --> 00:14:22,800
important but we have so many delusions and we're so attached to the conceptual reality that

94
00:14:22,800 --> 00:14:28,880
it's like overlooking a very important part of the path which is going to be our relationship

95
00:14:28,880 --> 00:14:34,320
with those things that we cling to, our relationship with those things, those conceptual

96
00:14:34,320 --> 00:14:41,120
objects that we interact with. So on a practical level it's very important that we come to terms

97
00:14:41,120 --> 00:14:53,360
with. Our needs, our materialistic desires, not being content with our clothes, the clothes that

98
00:14:53,360 --> 00:15:02,720
we wear, what you use clothing to protect the body, to cover up the parts of the body that

99
00:15:02,720 --> 00:15:18,320
are best left covered. You use food simply for doing away with hunger to bring energy to the body.

100
00:15:18,320 --> 00:15:26,480
You use shelters simply to guard from the elements and medicines to guard from guard of sickness.

101
00:15:30,800 --> 00:15:32,320
And the Buddha said these are the four,

102
00:15:36,960 --> 00:15:42,000
these are the four requisites of a human being, these four things are what we all cannot do

103
00:15:42,000 --> 00:15:47,600
without. But for most of us that's not nearly enough especially in modern times we're

104
00:15:47,600 --> 00:15:58,160
incredibly materialistic. So we are always needing more and we become slaves actually of our

105
00:15:58,160 --> 00:16:06,080
belongings and of our addictions that we always need more and more. And as a result our minds

106
00:16:06,080 --> 00:16:13,920
are not focused on the here and the now. We're not content with this state of reality. We need

107
00:16:13,920 --> 00:16:18,480
another state where there's this object or that object where we can see this thing or hear that

108
00:16:18,480 --> 00:16:24,880
thing or feel this or smell that or taste this or or even simply just conceive of the fact that

109
00:16:24,880 --> 00:16:33,520
we own this and own that and we have so many belongings this and that delicious food, beautiful clothes,

110
00:16:33,520 --> 00:16:47,680
you know, games and toys and possessions of all sorts. And because our mind is constantly thinking

111
00:16:47,680 --> 00:16:51,840
about getting more and more and more and is really addicted to the pleasure that comes from

112
00:16:51,840 --> 00:16:57,280
getting what we want, it's very difficult for us to find contentment. It's very difficult for us

113
00:16:57,280 --> 00:17:04,480
to stay in the present reality to accept reality for what it is because we're so used to being able

114
00:17:04,480 --> 00:17:16,480
to categorize reality or to compartmentalize reality. We don't have to accept everything. We don't

115
00:17:16,480 --> 00:17:21,120
have to accept reality for what it is because when it's not the way we like we have this idea

116
00:17:21,120 --> 00:17:27,200
that we can change it, that we can control it. And so we set ourselves on on quite an imbalanced

117
00:17:27,200 --> 00:17:36,560
state, imbalanced path and and are always unfulfilled, always unsatisfied and always in a state of

118
00:17:36,560 --> 00:17:44,480
discontent. And at any time that we don't get what we want, we crash the perpetual building up

119
00:17:44,480 --> 00:17:51,200
and building up of greater states of attachment, eventually snowballs leads and leads us to a great

120
00:17:51,200 --> 00:17:59,520
disappointment when we can't get what we want, when we're unable to to compartmentalize reality any

121
00:17:59,520 --> 00:18:17,840
further. So I think this is perhaps one way of of seeing seeing the benefits of meditation

122
00:18:17,840 --> 00:18:26,000
practice or one one use good use that comes that is that the meditation practice has in our

123
00:18:26,000 --> 00:18:33,440
lives is in looking at our desires for things when we want something when we need something

124
00:18:34,320 --> 00:18:38,880
looking at our possessions and coming to see that the important truth that these are just

125
00:18:38,880 --> 00:18:50,720
conceptual that we say I own this and I own that and to realize that that this entire sentence

126
00:18:50,720 --> 00:19:00,880
is every word of it as false, the I, the owned this, I is not true. It's not it's not mine

127
00:19:03,600 --> 00:19:12,000
even even our own self is just a conglomeration of of mind and body and states arising and ceasing.

128
00:19:12,000 --> 00:19:19,520
Most of which are very far out of our control, our states of emotion are once in our needs,

129
00:19:19,520 --> 00:19:22,720
we find that we really can't control them as we think we do.

130
00:19:30,160 --> 00:19:35,360
The own is not true, we can't for it, we can't control things and say let it always be like this.

131
00:19:36,400 --> 00:19:42,160
We can't even control the happiness that comes from the object because after a while we

132
00:19:42,160 --> 00:19:52,640
begin board of our possessions and we need more or we need the next thing, the newest thing.

133
00:19:52,640 --> 00:20:05,280
The identification of the thing as an object is also not a part of reality because the object

134
00:20:05,280 --> 00:20:16,240
itself is either seeing, hearing, smelling, tasting, feeling or thinking.

135
00:20:22,880 --> 00:20:24,880
When we practice meditation we're able to

136
00:20:24,880 --> 00:20:35,520
address this issue of materialism, of being attached to things and needing more and not being

137
00:20:35,520 --> 00:20:45,360
content. The Buddha said it's a mark of a noble person, it's the lineage which the word

138
00:20:45,360 --> 00:20:58,960
Wang sat, I believe it means the tradition, I guess, of the noble ones to be content with whatever

139
00:20:58,960 --> 00:21:05,360
robes, whatever clothes, whatever food, whatever shelter, whatever medicines they get,

140
00:21:06,320 --> 00:21:11,520
to be content with these things is the way of the noble ones. It's a mark of nobility

141
00:21:11,520 --> 00:21:15,600
or noble in the Buddhist sense in terms of being enlightened.

142
00:21:18,800 --> 00:21:23,440
So this is something that's very important, something we should always be looking at in our lives.

143
00:21:23,440 --> 00:21:28,400
It's easy to think we're practicing meditation and we're accumulating and accumulating and

144
00:21:28,400 --> 00:21:35,040
always getting more and more and new and so on. It's very difficult to control,

145
00:21:35,040 --> 00:21:38,240
it's very easy to rationalize why we need more and more in this and that,

146
00:21:38,240 --> 00:21:46,240
it's very difficult to be without. But the most important form of contentment is not actually

147
00:21:46,240 --> 00:21:49,040
called contentment in the Buddha's teaching as far as I've found.

148
00:21:51,440 --> 00:21:55,360
But to me it's the most important type of contentment and it's what is really being

149
00:21:55,360 --> 00:22:02,720
talked about when the Buddha talks about contentment with robes with clothes and food and so on.

150
00:22:02,720 --> 00:22:11,280
He's really talking about contentment with reality as it is. Contentment with

151
00:22:13,440 --> 00:22:16,320
experience as it is.

152
00:22:23,280 --> 00:22:28,880
Being totally in tune with reality to the point that nothing faces you.

153
00:22:28,880 --> 00:22:35,600
And it's not called contentment I think because the word contentment it can be quite misleading in

154
00:22:35,600 --> 00:22:46,560
this sense. It can be used to, it can slip into enjoyment. So we say, you know, I'm, you know,

155
00:22:46,560 --> 00:22:51,200
enjoying smelling the flowers, you know, stopping and smelling the flowers and just enjoying

156
00:22:51,200 --> 00:22:57,840
life being content with my life. This isn't what is meant here. What is meant here is contentment

157
00:22:57,840 --> 00:23:08,960
with every experience in terms of not being, not being discontent, not being drawn into a judgment

158
00:23:08,960 --> 00:23:15,440
about reality, not being drawn into a need. And in this sense there are two kinds of discontent,

159
00:23:15,440 --> 00:23:22,640
discontent based on anger and discontent based on greed. You could even say third, discontent based

160
00:23:22,640 --> 00:23:31,200
on delusion. If we want to go, go there as well. But what we notice most in our lives is,

161
00:23:31,200 --> 00:23:36,080
is these two discontentment based on anger and discontent, discontent based on greed.

162
00:23:37,200 --> 00:23:42,080
That we'll be sitting in meditation, even just sitting here listening to a talk and it's very

163
00:23:42,080 --> 00:23:47,360
difficult to keep the mind content simply to listen, simply to be here and now.

164
00:23:47,360 --> 00:23:54,800
Content with what I'm saying, content with the feelings going on in your body,

165
00:23:56,000 --> 00:23:59,360
content with the things around you, content with the thoughts in your mind.

166
00:24:01,200 --> 00:24:07,200
There's so many things that make us angry and upset, worried, afraid, bored.

167
00:24:07,200 --> 00:24:19,200
And our ordinary way of looking at this is to immediately need to change,

168
00:24:19,200 --> 00:24:27,360
need to alter our reality, to suit our, our defilement, to suit our, our defile state of mind,

169
00:24:27,360 --> 00:24:31,840
to suit our anger, our dislike. When we don't like something, we should get rid of it,

170
00:24:31,840 --> 00:24:38,160
we should remove it. This is the way we look at things, not just in terms of our, our innate

171
00:24:38,160 --> 00:24:42,960
sense, but also in terms of our views, our idea of what is right. We think that it's right that

172
00:24:42,960 --> 00:24:47,760
when you feel pain, you should adjust, you should move, you should do whatever necessary to get

173
00:24:47,760 --> 00:24:54,800
rid of the pain. This is one kind of discontent that is, is antithetical to the, what is teaching

174
00:24:54,800 --> 00:25:01,680
and it's, it's the purpose of the meditation practice to remove this sort of discontent.

175
00:25:02,480 --> 00:25:05,680
Rather than trying to change the pain, to change the experience,

176
00:25:07,760 --> 00:25:14,320
we change the way we look at the experience. So we, we, we simply see it for what it is,

177
00:25:14,320 --> 00:25:21,440
and when we're discontent, or when we're, in pain or, or maybe too hot, too cold,

178
00:25:21,440 --> 00:25:29,440
when we're hungry, when there's loud noises or, or whatever, when we're thinking about bad things,

179
00:25:30,880 --> 00:25:34,720
that we simply see it for what it is, and the word bad disappears. So we're no longer thinking

180
00:25:34,720 --> 00:25:41,360
about bad things or feeling bad feelings. We're just thinking or feeling or seeing or hearing

181
00:25:41,360 --> 00:25:48,960
or smelling and tasting. And all of our experience of reality is, is one of contentment,

182
00:25:48,960 --> 00:25:53,680
in the sense of simply seeing it for what it is, not discontent in terms of

183
00:25:55,360 --> 00:26:02,080
needing, needing it to be different. The other form of discontent is based on, on desire.

184
00:26:04,080 --> 00:26:06,880
It's not that there's anything wrong with our present state of being here.

185
00:26:08,080 --> 00:26:14,080
It's that we want something more, or we, we begin thinking about something and it makes us

186
00:26:14,080 --> 00:26:20,560
want it, makes us desire to do this or do that to obtain this or obtain that experience.

187
00:26:24,080 --> 00:26:27,600
And I, I would say from, from the practice looking at these two,

188
00:26:29,200 --> 00:26:35,040
the, really the, the, the only way to deal with them adequately is through meditation

189
00:26:35,040 --> 00:26:44,560
is through breaking them up into their, their building blocks, the pieces and seeing that actually

190
00:26:44,560 --> 00:26:50,560
we're not talking about a, an entity or a, a single experience. We're talking about several

191
00:26:50,560 --> 00:26:56,000
different experiences. I'll jumbled up into one. First, there's the experience of the object,

192
00:26:56,880 --> 00:27:04,240
then there's the feeling that the, the, the recognition of it as something good or something bad,

193
00:27:04,240 --> 00:27:10,400
then there's the feeling that comes of happiness or pain. Then there's the liking into the

194
00:27:10,400 --> 00:27:15,840
disliking it. Once there's the liking and the disliking it, there's the intention to do this

195
00:27:15,840 --> 00:27:22,160
and, or to do that, to change the experience, to attain, obtain something different or to remove

196
00:27:22,160 --> 00:27:31,040
something from our experience. And only then is there the acting out on it. And all of these

197
00:27:31,040 --> 00:27:38,160
can be broken up and separated from each other at any, at any one time. When you see something

198
00:27:38,160 --> 00:27:41,840
or, or let's take an easy one, when you hear something, suppose you're listening to what I'm saying

199
00:27:41,840 --> 00:27:47,520
and you really don't like it. Maybe I'm saying nasty things about, um, maybe what I'm saying

200
00:27:47,520 --> 00:27:54,960
just seems totally, um, in a pose diametrically opposed to what you believe.

201
00:27:54,960 --> 00:28:03,120
Or suppose I start yelling at you and saying nasty things to you or so on. Suppose someone comes

202
00:28:03,120 --> 00:28:08,400
into your room and starts, you know, bit complaining and so on.

203
00:28:11,440 --> 00:28:15,600
Right away, you can say to yourself hearing, hearing, remind yourself that it's just a sound.

204
00:28:16,480 --> 00:28:20,160
You can try that right now when I'm talking. You like it. You don't like it. It's not important.

205
00:28:20,160 --> 00:28:26,320
As a meditation exercise, simply when you hear my voice saying to yourself, hearing, hearing,

206
00:28:26,320 --> 00:28:28,320
hearing, it's a great way to listen to dhamma talks.

207
00:28:31,120 --> 00:28:33,920
People, you can even become enlightened this way. Listening to the dhamma,

208
00:28:35,040 --> 00:28:40,560
realizing the truth at the same time. One teacher in Thailand, he would always say,

209
00:28:40,560 --> 00:28:44,320
best way to listen to a dhamma talk. Just say hearing, hearing, hearing the whole time.

210
00:28:44,320 --> 00:28:50,640
Because that's really why we're teaching is to encourage people to meditate.

211
00:28:54,560 --> 00:28:58,000
And if that doesn't work, if that's, if you're not quick enough with that,

212
00:28:58,960 --> 00:29:03,200
then when the feeling arises, when you feel happiness or pain,

213
00:29:04,560 --> 00:29:11,440
you just focus on the happy, happy, happy, or pain, pain, or sad, sad, or whatever.

214
00:29:11,440 --> 00:29:19,840
And again, you don't let it build into a real liking or disliking

215
00:29:19,840 --> 00:29:24,080
and the intention to change things. You simply, when you have pain in the body, for instance,

216
00:29:24,080 --> 00:29:27,120
knowing that it's pain and just seeing it for what it is.

217
00:29:29,280 --> 00:29:32,320
And if you're still, if you can't catch it there, then you can catch it at the liking or the

218
00:29:32,320 --> 00:29:40,160
disliking, saying to yourself, liking, liking, or wanting, wanting, disliking, angry, upset,

219
00:29:40,160 --> 00:29:48,640
bored, scared, sad, as well. We still can't catch it there, then you can catch the intention.

220
00:29:48,640 --> 00:29:55,200
You want to do this. You want to obtain something. You want to say something. You want to

221
00:29:56,160 --> 00:30:02,720
chase someone away or run away or so on. And you can focus on that intention,

222
00:30:02,720 --> 00:30:08,080
wanting, wanting, or intending, intending. In this sense, not wanting as greed or as desire,

223
00:30:08,080 --> 00:30:10,080
but as an intention to do something.

224
00:30:16,320 --> 00:30:19,680
If you still can't catch it there, you're in trouble. Because at that point, you're going to go out

225
00:30:19,680 --> 00:30:23,840
and do something about it. But the amazing thing is, is when you focus on all of these things,

226
00:30:23,840 --> 00:30:35,040
you can see the problem with this line of reaction. You see how when you want things to be

227
00:30:35,040 --> 00:30:43,040
different than what they are, your whole body is tense and your mind is in a state of upset,

228
00:30:43,040 --> 00:30:47,760
of turmoil. Even if it's a desire and you feel happy and you really want something,

229
00:30:47,760 --> 00:30:51,680
you think, wow, this is going to make me happy. When you look at it, when you analyze it,

230
00:30:51,680 --> 00:30:57,920
you see that it's not so pleasurable at all, really. Your mind is in a state of clinging.

231
00:30:57,920 --> 00:31:02,080
You can really feel like you're clinging. It's as though you're grabbing something in your

232
00:31:02,080 --> 00:31:07,120
fist and holding tightly. That's how your mind feels at the moment when you want something.

233
00:31:08,080 --> 00:31:13,600
When you think you have to do something to obtain your pleasure, the object of your desires,

234
00:31:13,600 --> 00:31:20,720
even then, you can see that it's not really pleasurable, not to speak of anger when you want to

235
00:31:20,720 --> 00:31:26,000
hurt someone. When you look at it, you can see that this is not a wholesome state of mind. It's

236
00:31:26,000 --> 00:31:33,040
not a proper state of mind.

237
00:31:42,640 --> 00:31:50,000
And so this is how we really develop true contentment and Buddhism, coming to separate things into

238
00:31:50,000 --> 00:31:54,720
their reality. When you see something instead of being attached to it wanting it,

239
00:31:54,720 --> 00:31:59,280
needing it, clinging to it, simply seeing it for what it is. If you're happy, you're happy.

240
00:31:59,280 --> 00:32:07,360
If you're unhappy or unhappy, contentment in the sense of doing away with any need for things to

241
00:32:07,360 --> 00:32:11,200
be different, any need for reality to be other than what it is.

242
00:32:14,720 --> 00:32:22,320
This is really the ultimate state. And it's difficult. It's dangerous to say this,

243
00:32:22,320 --> 00:32:28,400
in a sense, just to leave it at that because it sounds as though, you know, don't do anything

244
00:32:28,400 --> 00:32:33,120
then, be content with things as they are, as you know, as I said, but it taught us to be discontent.

245
00:32:40,000 --> 00:32:46,960
But it's the key is it's not easy. This is not the ordinary nature of our minds. We're working

246
00:32:46,960 --> 00:32:55,120
hard striving to stop striving. We have to work in order that we don't have to do anything,

247
00:32:55,120 --> 00:33:01,040
in order that we no longer need to attain anything, to do away with our need to attain things.

248
00:33:02,240 --> 00:33:08,400
We're working hard to stop our minds from working so hard, in a sense.

249
00:33:08,400 --> 00:33:19,520
As we practice, as we develop the meditation practice, we find ourselves more and more content.

250
00:33:25,440 --> 00:33:31,440
So through working in this way, through developing ourselves, we're able to experience all things,

251
00:33:31,440 --> 00:33:37,360
and we no longer compartmentalize reality. We no longer separate things into the bearable and

252
00:33:37,360 --> 00:33:45,120
unbearable, the acceptable and the unacceptable. We're able to accept and react

253
00:33:46,080 --> 00:33:51,040
rationally and honestly and with wisdom to everything that arises.

254
00:33:52,960 --> 00:34:00,800
This is the Buddha's teaching on contentment. So that was the dhamma I would like to give today.

255
00:34:00,800 --> 00:34:06,800
And if there are any questions, I'm happy to take them. Otherwise, thanks everyone for coming.

