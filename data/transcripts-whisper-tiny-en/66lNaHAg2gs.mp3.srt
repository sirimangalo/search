1
00:00:00,000 --> 00:00:11,360
Very good evening, tonight we are continuing our study of the Dhamabada, looking at

2
00:00:11,360 --> 00:00:22,440
verse 1 verse 2-12, which reads as well.

3
00:00:22,440 --> 00:00:52,360
What is dear from what is dear from what is cherished?

4
00:00:52,360 --> 00:01:07,480
Come, sorrow, from what is cherished comes fear, for one who is freed from holding

5
00:01:07,480 --> 00:01:13,080
things from cherishing.

6
00:01:13,080 --> 00:01:26,640
There is no sorrow when it is fear, how could there be fear?

7
00:01:26,640 --> 00:01:41,080
So this verse was taught in regards to a man who had lost his son, very simple story.

8
00:01:41,080 --> 00:01:43,280
It is not much to it.

9
00:01:43,280 --> 00:01:48,440
The story goes that there was this man in the time of the Buddha, I think we have had stories

10
00:01:48,440 --> 00:01:49,440
like this.

11
00:01:49,440 --> 00:01:58,800
In fact, the second verse of the Dhamabada is a very similar story, this man lost his son

12
00:01:58,800 --> 00:02:10,720
and just spent all his time wailing and moaning and the morning his loss, losing a

13
00:02:10,720 --> 00:02:22,000
child, no, it is one of the things we never wish, we are not in the room of it.

14
00:02:22,000 --> 00:02:27,320
It is hard to beat the loss of a child, meaning the loss of a parent is sad but

15
00:02:27,320 --> 00:02:36,720
something you can live with, but the worst, the most dreadful thing is the loss of one's

16
00:02:36,720 --> 00:02:52,400
child, which should never happen in the grand scheme of things, if there were a grand scheme.

17
00:02:52,400 --> 00:02:59,920
The Buddha found out about this man and thought to himself, there is someone who might

18
00:02:59,920 --> 00:03:05,720
be able to understand the Dhamabada, these stories and it sounds like we Buddhists are

19
00:03:05,720 --> 00:03:14,960
we are always keen to find those people who are miserable, they may make a good Buddhist.

20
00:03:14,960 --> 00:03:21,960
It is not really fair, but it becomes suspiciously like that where you think religion

21
00:03:21,960 --> 00:03:29,160
might be leveled this accusation, that religion prays upon those who are desperate or

22
00:03:29,160 --> 00:03:35,960
those who are vulnerable and it certainly is possible a person who is vulnerable can

23
00:03:35,960 --> 00:03:44,840
be preyed upon even by people calling themselves religious, but that is an uncharitable

24
00:03:44,840 --> 00:03:56,480
connection because religion in most cases tries to or claims to work towards answering

25
00:03:56,480 --> 00:04:05,120
questions that science and society can't, you know, you bought into society, did everything

26
00:04:05,120 --> 00:04:10,880
right and still everything goes wrong, what is wrong so it tries to look beyond, it claims

27
00:04:10,880 --> 00:04:12,920
that there is something beyond.

28
00:04:12,920 --> 00:04:19,600
Buddhism as well claims that there is something beyond society, even beyond science.

29
00:04:19,600 --> 00:04:25,200
Science can tell you all sorts of things about the brain and the body can't tell you

30
00:04:25,200 --> 00:04:35,000
much about the mind of the psyche.

31
00:04:35,000 --> 00:04:39,960
So this man was miserable and he had done everything right, he was I think probably quite

32
00:04:39,960 --> 00:04:51,880
wealthy and certainly was in destitute, but here he was reduced to ruin not by loss of

33
00:04:51,880 --> 00:05:01,440
wealth or loss of status, but by loss of something that he couldn't, by he couldn't hold

34
00:05:01,440 --> 00:05:12,560
on to with money or power or even intelligence and logic and reasoning, so it goes beyond

35
00:05:12,560 --> 00:05:20,800
that, what do you do when you lose your child, and the Buddha went to him and asked him

36
00:05:20,800 --> 00:05:30,080
why are you crying, man told him and the Buddha said, you know, why is people don't act

37
00:05:30,080 --> 00:05:31,080
in this way?

38
00:05:31,080 --> 00:05:37,920
He said, he wasn't critical, I may sound a little harsh when I tell it, but he wasn't

39
00:05:37,920 --> 00:05:46,040
so harsh, he said, in ancient times laymen, householder, he would call them householder.

40
00:05:46,040 --> 00:05:55,440
In ancient times when a man lost his son, he wouldn't cry, he would think to himself and

41
00:05:55,440 --> 00:06:02,440
here we have a poly that's probably a good one to remember, I was just trying to marinamatang,

42
00:06:02,440 --> 00:06:12,560
marinamatang, marinamatang, bhijinamatang, bhijinamang bhijang, which means what is of a nature to

43
00:06:12,560 --> 00:06:23,880
die has died, what is of a nature to break apart has broken, and he taught this in reference

44
00:06:23,880 --> 00:06:30,880
and then he taught also the urajatika, urajatika, urajatika, urajatika, urajatika, urajatika,

45
00:06:30,880 --> 00:06:39,880
urajatika, urajatika, urajatika, urajatika, urajatika means snake, it's a snake jatika, and

46
00:06:39,880 --> 00:06:45,040
it only tangentially has to do with the snake because this man loses his son to a snake,

47
00:06:45,040 --> 00:06:51,080
a snake bites his son, and once met a monk, and I lived with a monk in Sri Lanka, very

48
00:06:51,080 --> 00:06:58,680
good, close friend that when I was there and just a great monk, when he was young, his

49
00:06:58,680 --> 00:07:09,440
sister, his younger sister got bitten by a cobra and died, this kind of thing happens.

50
00:07:09,440 --> 00:07:16,680
And in the urajatika, this was the bodhisattva who was the father, and the story tells

51
00:07:16,680 --> 00:07:25,920
that since he got married and had kids through his whole household career as a father,

52
00:07:25,920 --> 00:07:32,840
as a head of a household, he taught his whole family and they taught each other and they

53
00:07:32,840 --> 00:07:43,800
talked constantly about death, they would remind each other of death and talk about it.

54
00:07:43,800 --> 00:07:48,960
So it was an open and common topic of conversation.

55
00:07:48,960 --> 00:07:55,000
I find it funny that how shocking the shock of value of talking about death often when

56
00:07:55,000 --> 00:08:02,920
we bring it up and talk about how you might die and people are disturbed, it's kind

57
00:08:02,920 --> 00:08:11,320
of funny because of how taboo at all, not taboo, but how un-impolite it is to talk about

58
00:08:11,320 --> 00:08:23,560
death in many cultures, probably most cultures, besides really hardcore Buddhist circles.

59
00:08:23,560 --> 00:08:34,640
So this was, I think, probably at the time quite unique or exceptional, and today thinking

60
00:08:34,640 --> 00:08:38,960
about it, it's exceptional, talking to your kids about death, what a horrible thing, right?

61
00:08:38,960 --> 00:08:47,400
My parents never talked to me about death, but as a result, when the sun died, if we read

62
00:08:47,400 --> 00:08:52,800
the story, it's a really good job to come, one of the ones worth reading, one of the ones

63
00:08:52,800 --> 00:08:59,160
especially worth reading, especially for the verses and he says things like this, if you

64
00:08:59,160 --> 00:09:05,280
spill milk, the milk jug breaks or not for pot breaks, you don't cry, crying won't bring

65
00:09:05,280 --> 00:09:15,640
it back, being in a dumbong, being known, what has of a nature to break has broken, life

66
00:09:15,640 --> 00:09:21,840
is of a nature to break, and he taught the Uruga Jataka, I told the story of this man

67
00:09:21,840 --> 00:09:29,440
and how they burnt the sun and the sun's body and this man came along and said, it must

68
00:09:29,440 --> 00:09:34,600
be an enemy of yours, you're burning, he said, no, no, this is my son, what?

69
00:09:34,600 --> 00:09:35,600
How could this be?

70
00:09:35,600 --> 00:09:41,200
You're all here very peaceful and serene and not crying and not upset and he, this man talks

71
00:09:41,200 --> 00:09:49,240
to all the family and they all recite verses and say, if you call the moon down, wish for

72
00:09:49,240 --> 00:09:57,520
the moon and yell at the moon to come down and it won't come, can't bring my son back,

73
00:09:57,520 --> 00:10:04,160
and then each verse had a different way of putting it.

74
00:10:04,160 --> 00:10:09,320
So he told the Uruga Jataka here and then taught this verse, very simple story, as a

75
00:10:09,320 --> 00:10:15,440
result of teaching the verse, I think the man became a sort of a name, was able to gain

76
00:10:15,440 --> 00:10:19,080
a new perspective on things, so he was someone that would have picked out these people

77
00:10:19,080 --> 00:10:24,200
who were just ripe, not like us, we all have to work very, very hard to even think

78
00:10:24,200 --> 00:10:30,080
at anywhere, they would have picked the low hanging fruit, the people who were ready,

79
00:10:30,080 --> 00:10:34,920
because it's a good example, teaching those people who were ready and are interested

80
00:10:34,920 --> 00:10:43,640
in learning, instead of trying to encourage people who don't want to learn and pull people

81
00:10:43,640 --> 00:10:47,720
who don't want to learn in, often we make that mistake, trying to pull in people because

82
00:10:47,720 --> 00:10:57,320
there are friends or family members, and it's a lot of work for very little benefit, whereas

83
00:10:57,320 --> 00:11:02,440
instead if you found the people who wanted to come and where everyone would think they

84
00:11:02,440 --> 00:11:08,600
were crazy, but they're crazy enough to come to a place like this and dedicate themselves

85
00:11:08,600 --> 00:11:17,480
to walking back and forth and sitting very still, repeating mantras in their heads.

86
00:11:17,480 --> 00:11:23,480
If we focus on those people, much better results for much less effort and so it's efficient

87
00:11:23,480 --> 00:11:28,840
and those people go out and help other people and it spreads that way, Buddhism has never

88
00:11:28,840 --> 00:11:34,160
been a proselytizing religion for that reason, it's a difficult thing to practice Buddhism.

89
00:11:34,160 --> 00:11:39,200
We teach those people who are ready, who are interested, who are often miserable, but most

90
00:11:39,200 --> 00:11:46,920
important are sensitive, a person who is on the upswing, who is everything good in their

91
00:11:46,920 --> 00:11:47,920
lives.

92
00:11:47,920 --> 00:11:52,160
It's often very insensitive to the idea that something might go wrong, that something might

93
00:11:52,160 --> 00:11:55,360
break.

94
00:11:55,360 --> 00:11:59,640
Sometimes again the difference between brains even, apparently our brains are different,

95
00:11:59,640 --> 00:12:09,400
which makes sense, meaning some people are much more susceptible to suffering than others.

96
00:12:09,400 --> 00:12:16,360
Something goes wrong, they react more negatively, other people are insulated from it, meaning

97
00:12:16,360 --> 00:12:20,920
something bad happens and they're very quick to rebound, just based on how the brain works

98
00:12:20,920 --> 00:12:25,680
and of course you can train that to some extent, but to some extent it's organic, it's

99
00:12:25,680 --> 00:12:30,880
the way the brain is composed and so on, that's what it appears to the scientists.

100
00:12:30,880 --> 00:12:42,760
I mean it seems reasonable, but so those people for whom it's very hard to see the negative,

101
00:12:42,760 --> 00:12:43,760
right?

102
00:12:43,760 --> 00:12:47,920
It happens, you forget about it very quickly, it's very hard to see the potential for disaster

103
00:12:47,920 --> 00:12:55,120
because there isn't really, you're insulated, temporarily of course, and the bigger picture

104
00:12:55,120 --> 00:13:01,680
is really what this verse is part of what this verse is talking about.

105
00:13:01,680 --> 00:13:07,600
So the verse has three key words, the first one is cherish, what is cherish, the second

106
00:13:07,600 --> 00:13:19,120
one is sadness and the third is fear and the idea that something that is cherished doesn't

107
00:13:19,120 --> 00:13:24,760
lead to happiness, doesn't lead to contentment and bliss and good life, doesn't lead to

108
00:13:24,760 --> 00:13:26,560
goodness.

109
00:13:26,560 --> 00:13:32,480
We kind of have this idea, I suppose, that the things that we like, the things that bring

110
00:13:32,480 --> 00:13:38,000
us happiness, that happiness is goodness, and we put the cart before the horse whereas

111
00:13:38,000 --> 00:13:44,720
in Buddhism goodness leads to happiness, we seek out happiness in the world, in Buddhism

112
00:13:44,720 --> 00:13:49,640
it's not that way, Buddhism we see through that and see that seeking out happiness isn't

113
00:13:49,640 --> 00:14:00,080
good, happiness doesn't lead to goodness, doesn't even lead to happiness.

114
00:14:00,080 --> 00:14:04,840
So we cherish things and this is the idea behind the story and this whole chapter of course

115
00:14:04,840 --> 00:14:13,520
is a chapter on things that are cherished and so putting stress, giving it a whole chapter

116
00:14:13,520 --> 00:14:20,800
gives us this idea that there was a recognition of how important it is, this concept of things

117
00:14:20,800 --> 00:14:27,280
that we cherish, concept of holding things dear which ultimately comes down to what we

118
00:14:27,280 --> 00:14:36,240
like and it's this idea of craving and clinging and addiction and so on.

119
00:14:36,240 --> 00:14:42,400
So what I think is interesting about this verse and this teaching is what we talked about

120
00:14:42,400 --> 00:14:50,280
last week and we'll talk about for the, well not next week but for quite a few verses

121
00:14:50,280 --> 00:14:53,360
for this whole chapter probably.

122
00:14:53,360 --> 00:15:01,120
But what is interesting is this separation here between the distinction, between sorrow

123
00:15:01,120 --> 00:15:18,720
and fear which I think is interesting, so the idea that holding something dear could

124
00:15:18,720 --> 00:15:32,600
lead to sorrow and the clear ideas that when you like things, when there are things

125
00:15:32,600 --> 00:15:41,360
that you hold dear you might lose them and if you lose them that will be great suffering.

126
00:15:41,360 --> 00:15:45,640
And this of course applies to people and applies to things that it's the idea sort of

127
00:15:45,640 --> 00:15:50,120
the concept, one of the concepts of suffering and Buddhism that you might lose it and if

128
00:15:50,120 --> 00:15:52,280
you lose it that would be suffering.

129
00:15:52,280 --> 00:16:00,240
But the idea of fear and here fear is, it's a sin and or it's the same word that they

130
00:16:00,240 --> 00:16:07,520
use for danger and it seems a little bit odd because in English of course there are two

131
00:16:07,520 --> 00:16:14,200
very different words fear and danger but the idea, the commonality behind them is the

132
00:16:14,200 --> 00:16:20,920
anticipation or the concern for the potential of suffering.

133
00:16:20,920 --> 00:16:25,400
When you fear something, you don't fear what is happening to you, right?

134
00:16:25,400 --> 00:16:30,560
If you're in great pain you're not afraid of the pain, you're afraid of what might come

135
00:16:30,560 --> 00:16:36,920
as a result of the pain and you might say it's a disliking or an aversion to a concept.

136
00:16:36,920 --> 00:16:45,360
So fear isn't actually I don't think it's not a real thing in it's not a real experience

137
00:16:45,360 --> 00:16:50,280
but when we experience fear what we call something fear, what we're experiencing is a concept

138
00:16:50,280 --> 00:16:55,920
a thought, right, suppose there's a spider and you're afraid of the spider, you're not

139
00:16:55,920 --> 00:17:00,880
afraid of the spider, you're afraid of you have a concept of it jumping you at attacking

140
00:17:00,880 --> 00:17:13,680
you, biting you or crawling on you and that fear is of the anticipation.

141
00:17:13,680 --> 00:17:19,160
That's not even quite, it's more complicated because if you can see a spider on you and

142
00:17:19,160 --> 00:17:24,600
just be freaked out by the touch but it's still very much associated with the idea that

143
00:17:24,600 --> 00:17:28,920
the spider might bite you or that sort of thing anyway.

144
00:17:28,920 --> 00:17:36,680
The point is ultimately it comes down to a simple mind state but what's interesting about

145
00:17:36,680 --> 00:17:42,400
it is that you can have all the things that you love, you can be surrounded by the people

146
00:17:42,400 --> 00:17:49,080
that you love and still suffer and suffer not because you've lost something, not because

147
00:17:49,080 --> 00:17:53,320
you can't care what you want, not because you've gotten what you don't want but simply

148
00:17:53,320 --> 00:18:00,840
because you could lose what you want and this is something that makes meditation stand

149
00:18:00,840 --> 00:18:11,880
out, mindfulness stand out because of the insight that it gives into the suffering inherent

150
00:18:11,880 --> 00:18:17,560
in having things that you cherish, that it's not just the sadness that comes from not getting

151
00:18:17,560 --> 00:18:27,520
what you want but it's the constant, not constant but ever-present incessant and repeated

152
00:18:27,520 --> 00:18:37,000
and regular appearance of terror, of fear, of worry, the concern that you might lose

153
00:18:37,000 --> 00:18:41,040
what you have.

154
00:18:41,040 --> 00:18:45,720
Sometimes this is why, of course, thinking about death is so powerful, why it's important

155
00:18:45,720 --> 00:18:49,600
for us to think about death because death is sort of the biggest thing that we might

156
00:18:49,600 --> 00:18:52,400
fear but there's many others.

157
00:18:52,400 --> 00:18:56,600
Think about all the things that you own, think about your house, your car, think about

158
00:18:56,600 --> 00:19:01,080
your family, more deeply valued things, your relatives.

159
00:19:01,080 --> 00:19:05,960
What if your parents died, what if your children died, what if your siblings and friends

160
00:19:05,960 --> 00:19:10,000
and lovers, what if they all died?

161
00:19:10,000 --> 00:19:15,360
What if you lost your health, what if you got sick, what if you got in a car accident,

162
00:19:15,360 --> 00:19:21,600
what if you lost all your money, I mean, many things, anything that we hold dear, what

163
00:19:21,600 --> 00:19:25,400
if suddenly you were bankrupt, you had no money, what if suddenly this house burnt down

164
00:19:25,400 --> 00:19:30,920
and we were all here, regardless of what future aid we might get, we were all forced to

165
00:19:30,920 --> 00:19:41,920
go and walk out in the cold in the snow, bringing up, not everything, many things that

166
00:19:41,920 --> 00:19:50,840
would like this, you bring them up to someone and they, it scares them.

167
00:19:50,840 --> 00:19:58,280
And this points to something very important, I think, that meditation, mindfulness shows

168
00:19:58,280 --> 00:20:07,440
us the true nature of cherishing, the true nature of holding things dear, the state, the

169
00:20:07,440 --> 00:20:14,760
truth about this state of, of craving things and clinging to things and loving things.

170
00:20:14,760 --> 00:20:23,680
And that that love is always going to be tainted with fear, with danger, not just danger,

171
00:20:23,680 --> 00:20:30,240
I think fear is here more relevant because someone can be oblivious to the danger, but

172
00:20:30,240 --> 00:20:39,120
when someone fears losing what they have, it's almost as bad or perhaps as bad as actually

173
00:20:39,120 --> 00:20:46,440
losing the thing because it's constant, it's incessant.

174
00:20:46,440 --> 00:20:54,840
And so in our meditation, we see the not only potential for loss, but the very real stress

175
00:20:54,840 --> 00:21:00,440
that comes from holding on to things, right? Sometimes you want something, you can't get

176
00:21:00,440 --> 00:21:05,920
it, you suffer, but sometimes you have something, you're afraid you might lose it, you

177
00:21:05,920 --> 00:21:13,120
have stress over it or worry over keeping it.

178
00:21:13,120 --> 00:21:24,560
And it goes beyond fear as well, it goes to the simple stress involved with wanting things.

179
00:21:24,560 --> 00:21:28,960
And you want something and then you're concerned about getting it or worried about how to

180
00:21:28,960 --> 00:21:35,160
get it or have to think about how to get it, many, many things that you see in meditation.

181
00:21:35,160 --> 00:21:45,680
And what I think also bears noting is how ignorant we are to this without the practice

182
00:21:45,680 --> 00:21:56,320
of mindfulness, it almost seems like a silly thing to say that things you have, you might

183
00:21:56,320 --> 00:22:05,320
lose them and that there's this fear associated because our perspective of thinking that

184
00:22:05,320 --> 00:22:11,640
happiness comes from getting what you want, not the Buddhist perspective, the ordinary perspective

185
00:22:11,640 --> 00:22:18,120
of thinking, if I get what I want, if I work things out in life and work hard and get

186
00:22:18,120 --> 00:22:23,080
everything okay, then I'll be happy.

187
00:22:23,080 --> 00:22:29,680
This idea has an inherent and implicit assumption, so it's something we already know and

188
00:22:29,680 --> 00:22:36,080
kind of assume, generally I think, that it's precarious, that there is always going to

189
00:22:36,080 --> 00:22:43,760
be this concern because part of getting what you want means keeping what you want, what

190
00:22:43,760 --> 00:22:51,440
you like and keeping what you like involves some stress, some work, some worry, some fear.

191
00:22:51,440 --> 00:22:55,200
You have to protect it.

192
00:22:55,200 --> 00:23:02,000
And so the point in that, what I mean to say is this kind of happiness, the happiness

193
00:23:02,000 --> 00:23:06,680
of ordinary society where people have houses and they have jobs and they have families

194
00:23:06,680 --> 00:23:14,440
and so on, is always going to be inferior, is always going to be lacking.

195
00:23:14,440 --> 00:23:18,600
And when people say they're happy in life, it's really simply because they haven't really

196
00:23:18,600 --> 00:23:23,600
been mindful and they're lack of mindfulness keeps them oblivious to how much stress

197
00:23:23,600 --> 00:23:30,600
and suffering is involved, how limited their happiness is, no matter how happy you can

198
00:23:30,600 --> 00:23:36,040
be, I think I'm so happy, I've got everything I want, how limited that is, it's limited

199
00:23:36,040 --> 00:23:40,760
by sorrow when you lose, when you can't get the things you want, when the things you want

200
00:23:40,760 --> 00:23:43,000
change or so on.

201
00:23:43,000 --> 00:23:54,400
But it's also the stress of having to keep worrying about keeping worrying about losing.

202
00:23:54,400 --> 00:24:01,760
And so the end of the verse, nati, soko, kutom, kutom, nati, soko, kutom, payam, for someone

203
00:24:01,760 --> 00:24:09,120
who frees themselves, wipomutah, someone who becomes free from cherishing.

204
00:24:09,120 --> 00:24:11,000
It's an odd thing to say, right?

205
00:24:11,000 --> 00:24:14,720
We don't think of ourselves needing to be freed from it.

206
00:24:14,720 --> 00:24:18,000
We leap head first into cherishing.

207
00:24:18,000 --> 00:24:20,880
Yes, cherish.

208
00:24:20,880 --> 00:24:26,880
Really beloved, we are our whole culture, most cultures in the world are about cherishing,

209
00:24:26,880 --> 00:24:30,360
hold on to the things you cherish.

210
00:24:30,360 --> 00:24:39,680
Find something you love, do something you love, we have not in Buddhism, in mindfulness

211
00:24:39,680 --> 00:24:47,680
we see differently, we come to, we make claims about seeing through that and claims about

212
00:24:47,680 --> 00:25:00,200
the limited nature of cherishing, and more importantly, for this part of the verse, the

213
00:25:00,200 --> 00:25:07,360
constraint and the binding and the servitude involved with cherishing things, that in fact

214
00:25:07,360 --> 00:25:14,400
we are not in charge, in fact even if we wanted to stop cherishing things we couldn't,

215
00:25:14,400 --> 00:25:19,440
we wanted to stop holding things there and suddenly we say, okay, my son is dead, I don't

216
00:25:19,440 --> 00:25:20,440
love him anymore.

217
00:25:20,440 --> 00:25:25,160
Let's just get rid of that attachment now because it's no good to me, even if we wanted

218
00:25:25,160 --> 00:25:33,360
to do that, most people don't, they're quite happy to suffer, even if we wanted to,

219
00:25:33,360 --> 00:25:39,360
we couldn't, we couldn't, we couldn't because we were ignorant because we don't see

220
00:25:39,360 --> 00:25:50,760
the stressing suffering, we don't realize the, the trap that we're stuck in, we don't

221
00:25:50,760 --> 00:25:59,080
see clearly, so through our practice of meditation here you, you might find yourself letting

222
00:25:59,080 --> 00:26:03,760
go of some things, it can be scary, it can be scary because you think all, I want to put

223
00:26:03,760 --> 00:26:13,240
all those things I love, I'm going to lose them, but what you will find is not that suddenly

224
00:26:13,240 --> 00:26:20,160
you've lost things that you love, you'll find yourself gradually letting go, gradually

225
00:26:20,160 --> 00:26:30,080
opening up to, to possibilities, possibilities of what we call loss and gain, getting things

226
00:26:30,080 --> 00:26:34,640
you don't want, losing things you do want, but we call them those things, but it's simply

227
00:26:34,640 --> 00:26:41,840
really, in reality, it's just changed, you open yourself up to change, change no longer

228
00:26:41,840 --> 00:26:51,360
threatens you, it's no longer scary, it's no longer dangerous, weepamutya, you become

229
00:26:51,360 --> 00:26:59,360
freed, released from the bondage of having to find happiness in things, your happiness,

230
00:26:59,360 --> 00:27:14,560
your peace comes from, it comes from, freedom, a good verse, another one of these verses

231
00:27:14,560 --> 00:27:22,080
about cherishing, we're going to have many more I think, and this verse is verse 2-12 of

232
00:27:22,080 --> 00:27:31,840
423, there are 423 verses in the Dhamapada, we've covered 212, which means we've passed

233
00:27:31,840 --> 00:27:38,880
halfway with this verse, so thank you all for keeping up and you're interested in the

234
00:27:38,880 --> 00:27:44,240
Dhamman, interested in the practice, thank you all for being here and practicing, I wish you

235
00:27:44,240 --> 00:27:56,640
all the best in your practice, that's the Dhamapada, thank you, have a good night.

