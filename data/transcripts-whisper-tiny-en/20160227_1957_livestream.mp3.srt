1
00:00:00,000 --> 00:00:10,000
If you can't get voice, if they can't get voice working, you can maybe someone can post the link to the live audio, or just turn to the link.

2
00:00:10,000 --> 00:00:13,000
Yes.

3
00:00:13,000 --> 00:00:22,000
That's a new line, whoever knows the link.

4
00:00:22,000 --> 00:00:25,000
So we're now live.

5
00:00:25,000 --> 00:00:31,000
If you can't hear us in second life, or if you can't log into the sim.

6
00:00:31,000 --> 00:00:35,000
So then there's to be a new sim next time.

7
00:00:35,000 --> 00:00:49,000
Send them over there, and have them send in seats, and they can send it to our website.

8
00:00:49,000 --> 00:00:53,000
So, good afternoon, everyone.

9
00:00:53,000 --> 00:00:56,000
Oh, it's still morning, huh?

10
00:00:56,000 --> 00:00:59,000
Good morning, everyone.

11
00:00:59,000 --> 00:01:04,000
Good afternoon, where I am.

12
00:01:04,000 --> 00:01:07,000
It's funny, that sentence is such a funny thing to say.

13
00:01:07,000 --> 00:01:10,000
It's afternoon where I am.

14
00:01:10,000 --> 00:01:14,000
I'm assuming so much, right?

15
00:01:14,000 --> 00:01:19,000
What the heck does afternoon mean?

16
00:01:19,000 --> 00:01:38,000
It means that me, my body, this thing we call the Earth, and the Sun, are in a particular configuration.

17
00:01:38,000 --> 00:01:45,000
But, look at the clock up in our second life, and it's still 11.59.

18
00:01:45,000 --> 00:01:53,000
And, since I am sitting on that mat right there, it's also morning.

19
00:01:53,000 --> 00:02:04,000
Where I am, depending on what you mean by where I am.

20
00:02:04,000 --> 00:02:17,000
That's not what I wanted to talk about today.

21
00:02:17,000 --> 00:02:24,000
So, today, I'm going to talk about the...

22
00:02:24,000 --> 00:02:29,000
Well, I'm going to talk about Buddhism.

23
00:02:29,000 --> 00:02:38,000
That's what I always talk about, right?

24
00:02:38,000 --> 00:02:46,000
It's such a funny thing, this Buddhism thing.

25
00:02:46,000 --> 00:02:51,000
It's not unique in its funniness.

26
00:02:51,000 --> 00:02:54,000
Christianity is also kind of funny.

27
00:02:54,000 --> 00:02:56,000
Islam is just as funny.

28
00:02:56,000 --> 00:03:01,000
Judaism is funny in its own ways.

29
00:03:01,000 --> 00:03:07,000
But, by funny, I mean, what the heck is Buddhism, really?

30
00:03:07,000 --> 00:03:12,000
Does anyone get that?

31
00:03:12,000 --> 00:03:23,000
The more you study Buddhism as an outsider, I think the more confused you get about what Buddhism actually is.

32
00:03:23,000 --> 00:03:35,000
I got in trouble recently, and probably still getting in trouble for saying something.

33
00:03:35,000 --> 00:03:44,000
I can't even quite remember what I said, but some rather unflattering things about certain Mahayana Buddhist texts.

34
00:03:44,000 --> 00:03:55,000
But, if you read those texts, you'd have a whole different idea about what Buddhism is completely different idea.

35
00:03:55,000 --> 00:04:06,000
Well, a much different idea of what Buddhism is than if you read the texts in my tradition.

36
00:04:06,000 --> 00:04:08,000
It's a different flavor to them.

37
00:04:08,000 --> 00:04:21,000
It's interesting how they carry over some of the thematic devices.

38
00:04:21,000 --> 00:04:28,000
And there's some paraphrases and similar manner of speaking, but the doctrine is so different.

39
00:04:28,000 --> 00:04:41,000
And there are Buddhists who believe that there's a Buddha land where Buddhists are waiting or living where you can go and become enlightened.

40
00:04:41,000 --> 00:04:43,000
You can be reborn there.

41
00:04:43,000 --> 00:04:52,000
There are Buddhists who believe that you just do a bunch of chanting and everything you want comes to you.

42
00:04:52,000 --> 00:04:59,000
I mean, that's probably oversimplifying a little bit, but basically.

43
00:04:59,000 --> 00:05:08,000
There are Buddhists who believe in tantric sex as a religious practice.

44
00:05:08,000 --> 00:05:19,000
There are Buddhists who believe that killing and stealing are always wrong.

45
00:05:19,000 --> 00:05:23,000
Killing, stealing, lying, cheating, drugs in alcohol are always wrong.

46
00:05:23,000 --> 00:05:30,000
And then there are Buddhists who believe that such things can be used for the right purposes.

47
00:05:30,000 --> 00:05:35,000
There are Buddhists who follow a historic Buddha.

48
00:05:35,000 --> 00:05:45,000
And there are Buddhists who think that they are Buddhists or on a path to become a Buddha.

49
00:05:45,000 --> 00:05:51,000
There are people who claim to be enlightened.

50
00:05:51,000 --> 00:05:56,000
There are Buddhists who claim that there's no such thing as enlightenment.

51
00:05:56,000 --> 00:05:59,000
There are Buddhists who believe in rebirth.

52
00:05:59,000 --> 00:06:02,000
There are Buddhists that don't believe in rebirth.

53
00:06:02,000 --> 00:06:07,000
I mean, it's not probably saying anything too surprising because most religions are like this.

54
00:06:07,000 --> 00:06:14,000
But I think it's particularly problematic in Buddhism.

55
00:06:14,000 --> 00:06:18,000
Like it's really gone very, very far field, I don't.

56
00:06:18,000 --> 00:06:24,000
And I think take Christianity, for example, for the most part, take follow Jesus.

57
00:06:24,000 --> 00:06:27,000
Islam, for the most part, follows Muhammad.

58
00:06:27,000 --> 00:06:31,000
And they just have different interpretations of what this historic dude said.

59
00:06:31,000 --> 00:06:34,000
Buddhism isn't like that.

60
00:06:34,000 --> 00:06:43,000
There's just so many different ways of talking about what Buddhism means.

61
00:06:43,000 --> 00:06:50,000
And Buddhism and Buddhism and the idea of Buddha and so many different things to different people.

62
00:06:50,000 --> 00:07:01,000
Where I'm going with this is I'd like to try and with the qualifier that I am just one Buddhist teacher.

63
00:07:01,000 --> 00:07:10,000
And obviously I don't represent any kind of the Buddhism or Orthodox Buddhism.

64
00:07:10,000 --> 00:07:22,000
But I'd like to present to you something that I think is very core to Buddhism.

65
00:07:22,000 --> 00:07:42,000
And to that extent, important in order to be a pillar, sort of centering ourselves.

66
00:07:42,000 --> 00:07:54,000
In India, when they would build a city, they would have something called the Indra's pillar, Indakila.

67
00:07:54,000 --> 00:07:59,000
So they, and it would have meant is they'd put a, and I don't know where the tradition came from.

68
00:07:59,000 --> 00:08:04,000
But as I understand they'd put a pillar, solid pillar in the center of the city.

69
00:08:04,000 --> 00:08:12,000
And that was sort of the inauguration of the building of the city or something like that.

70
00:08:12,000 --> 00:08:17,000
So they'd have Indakila, maybe it had something to do with the Ashokan pillars.

71
00:08:17,000 --> 00:08:32,000
But Indakila became, it's just something that sort of is the foundation of the city.

72
00:08:32,000 --> 00:08:37,000
And so that's the idea I'd like to give here sort of a foundation.

73
00:08:37,000 --> 00:08:48,000
Something to get your bearings on, to use to get your bearings on what is Buddhism.

74
00:08:48,000 --> 00:08:57,000
If such a question is actually answerable at all.

75
00:08:57,000 --> 00:09:02,000
So in Buddhism, we often hear about what are called the Four Noble Truths.

76
00:09:02,000 --> 00:09:07,000
And this is probably one of the most widely agreed upon teachings.

77
00:09:07,000 --> 00:09:12,000
Buddhism, not by all schools, I don't think, but...

78
00:09:12,000 --> 00:09:19,000
Oh, maybe by all schools.

79
00:09:19,000 --> 00:09:31,000
But another thing, and another aspect of the Four Noble Truths sets really may be more equally important.

80
00:09:31,000 --> 00:09:39,000
Or equal important to talk about in detail is what we call the Eightfold Noble Path.

81
00:09:39,000 --> 00:09:47,000
And so often we say that the Buddhist Path or the Buddhist way of life has eight parts.

82
00:09:47,000 --> 00:09:54,000
And those eight parts sort of constitute in a highly abbreviated form.

83
00:09:54,000 --> 00:10:07,000
What it means to practice as a Buddhist, or the qualities of life that a Buddhist should strive to cultivate.

84
00:10:07,000 --> 00:10:09,000
So we have eight things.

85
00:10:09,000 --> 00:10:15,000
And even that might seem a little bit onerous because you've got about eight things that you sort of have to get your head around.

86
00:10:15,000 --> 00:10:17,000
But it's actually a lot easier than that.

87
00:10:17,000 --> 00:10:20,000
If you want to understand, it's even easier than eight.

88
00:10:20,000 --> 00:10:22,000
We've only actually got three things.

89
00:10:22,000 --> 00:10:25,000
And these are what the Buddha called the Three Trainings.

90
00:10:25,000 --> 00:10:30,000
So in Buddhism, we strive to train ourselves.

91
00:10:30,000 --> 00:10:36,000
And when I say in Buddhism, I'm actually talking about my tradition because I don't know a heck of a lot about all the other traditions.

92
00:10:36,000 --> 00:10:38,000
So I can't speak for them.

93
00:10:38,000 --> 00:10:44,000
But as a sort of a foundation path, we have three things that we train ourselves in.

94
00:10:44,000 --> 00:10:49,000
I think it's pretty common throughout Buddhist tradition.

95
00:10:49,000 --> 00:10:55,000
So the eight full normal paths, you can separate it into three parts.

96
00:10:55,000 --> 00:11:05,000
We have what we call Sila or Morality, Samadhi, which we call the Transite as concentration or focus,

97
00:11:05,000 --> 00:11:09,000
and Banya, a wisdom.

98
00:11:09,000 --> 00:11:15,000
These are the Pali words, which are probably close to what the historic Buddha used to.

99
00:11:15,000 --> 00:11:21,000
They're probably close to the language of the Buddha himself.

100
00:11:21,000 --> 00:11:25,000
He stands with their other words, but the Buddha probably didn't speak Sanskrit.

101
00:11:25,000 --> 00:11:28,000
He wasn't a Brahman. It wasn't his language.

102
00:11:28,000 --> 00:11:32,000
When Pali is almost Sanskrit anyway, but it's more of a common word.

103
00:11:32,000 --> 00:11:38,000
So we have Sila, Samadhi, Panya.

104
00:11:38,000 --> 00:11:44,000
Morality or ethics, concentration and wisdom.

105
00:11:44,000 --> 00:11:47,000
So this is what I'd just like to go through today.

106
00:11:47,000 --> 00:11:49,000
It's a good thing for us.

107
00:11:49,000 --> 00:11:53,000
It's a good basic teaching for us to go through as a reminder.

108
00:11:53,000 --> 00:11:58,000
If you find yourself getting overwhelmingly what the heck is this Buddhism thing,

109
00:11:58,000 --> 00:12:03,000
getting myself caught up in your mind yourself of just three things.

110
00:12:03,000 --> 00:12:08,000
And this is basically all we need is followers of the Buddha.

111
00:12:08,000 --> 00:12:15,000
So Sila, Morality, to understand it, we break it up into four parts.

112
00:12:15,000 --> 00:12:18,000
It's just one way of breaking it up, but it's a good way.

113
00:12:18,000 --> 00:12:21,000
The first one is we follow certain rules.

114
00:12:21,000 --> 00:12:24,000
So in my tradition or in most traditions,

115
00:12:24,000 --> 00:12:33,000
laborists will try to follow five precepts, not to kill, not to steal, not to cheat, not to lie,

116
00:12:33,000 --> 00:12:36,000
and not to take drugs and alcohol.

117
00:12:36,000 --> 00:12:38,000
This is keeping rules.

118
00:12:38,000 --> 00:12:42,000
Now, Morality isn't all about rules, but rules are useful.

119
00:12:42,000 --> 00:12:44,000
They are important.

120
00:12:44,000 --> 00:12:47,000
Rules are like fence posts.

121
00:12:47,000 --> 00:12:52,000
If you're trying to keep a horse in a fence in,

122
00:12:52,000 --> 00:12:55,000
the first thing you need is fence posts.

123
00:12:55,000 --> 00:12:57,000
But then if you ask our fence posts enough,

124
00:12:57,000 --> 00:13:00,000
if you put some fence posts in the ground,

125
00:13:00,000 --> 00:13:03,000
can you ever keep a horse contained?

126
00:13:03,000 --> 00:13:07,000
Not unless it's a pretty stupid horse.

127
00:13:07,000 --> 00:13:10,000
Or unless you got a lot of fence posts.

128
00:13:10,000 --> 00:13:11,000
Right?

129
00:13:11,000 --> 00:13:13,000
Which is true of rules as well.

130
00:13:13,000 --> 00:13:16,000
If you had enough rules, probably be pretty easy.

131
00:13:16,000 --> 00:13:18,000
That's probably enough.

132
00:13:18,000 --> 00:13:21,000
Like monks have hundreds and hundreds of rules.

133
00:13:21,000 --> 00:13:23,000
So lots of fence posts.

134
00:13:23,000 --> 00:13:27,000
Makes it easier to see the boundaries.

135
00:13:27,000 --> 00:13:30,000
But it'll never be enough.

136
00:13:30,000 --> 00:13:34,000
They'll always be holes between the fence posts, right?

137
00:13:34,000 --> 00:13:36,000
Fence posts are there to put up fences,

138
00:13:36,000 --> 00:13:39,000
but they aren't fences.

139
00:13:39,000 --> 00:13:41,000
Unless without fence posts,

140
00:13:41,000 --> 00:13:43,000
your fence will fall over.

141
00:13:43,000 --> 00:13:46,000
Without rules, your Morality is weak.

142
00:13:46,000 --> 00:13:52,000
It's waivers, it shakes, it falls over.

143
00:13:52,000 --> 00:13:55,000
So the first part is keeping rules.

144
00:13:55,000 --> 00:13:58,000
Second part of Morality is,

145
00:13:58,000 --> 00:14:01,000
I'm probably going to get the order wrong,

146
00:14:01,000 --> 00:14:05,000
that it's considering your requisites,

147
00:14:05,000 --> 00:14:12,000
considering the things that you use to visualize.

148
00:14:12,000 --> 00:14:25,000
I mean, in general, it means being mindful and conscientious

149
00:14:25,000 --> 00:14:31,000
about how you live your life in terms of possessions,

150
00:14:31,000 --> 00:14:34,000
in terms of your environment.

151
00:14:34,000 --> 00:14:38,000
So you could argue that there's has to do with environmental conservation,

152
00:14:38,000 --> 00:14:43,000
but it more has to do with greed,

153
00:14:43,000 --> 00:14:46,000
of course, is the greatest killer of the environment anyway.

154
00:14:46,000 --> 00:14:50,000
But I agreed for clothing, beautiful clothing,

155
00:14:50,000 --> 00:14:55,000
and greed for luxury and greed for food,

156
00:14:55,000 --> 00:15:11,000
junk food, food for the taste rather than for the nutritive value.

157
00:15:11,000 --> 00:15:18,000
Greed for luxury and accommodation and possession,

158
00:15:18,000 --> 00:15:22,000
wanting the best things, wanting beautiful things,

159
00:15:22,000 --> 00:15:27,000
wanting comfortable, soft, pleasant things.

160
00:15:27,000 --> 00:15:31,000
All of this can lead to obsession,

161
00:15:31,000 --> 00:15:36,000
to lean to laziness, lead to complacency,

162
00:15:36,000 --> 00:15:40,000
Bamaada.

163
00:15:40,000 --> 00:15:44,000
So this is why Buddhist monks have to live quite frugally,

164
00:15:44,000 --> 00:15:51,000
because we're trying to fit ourselves into that.

165
00:15:51,000 --> 00:15:56,000
I feel we're trying to attain this,

166
00:15:56,000 --> 00:16:02,000
build this state of being free from you.

167
00:16:02,000 --> 00:16:13,000
Greed and indulgence, that kind of thing.

168
00:16:13,000 --> 00:16:27,000
Because these not only do luxurious things,

169
00:16:27,000 --> 00:16:32,000
cloud the mind with the pleasure that they,

170
00:16:32,000 --> 00:16:35,000
that they come from them and then greed,

171
00:16:35,000 --> 00:16:37,000
but they lead to obsession.

172
00:16:37,000 --> 00:16:39,000
They lead to worry, they lead to concern,

173
00:16:39,000 --> 00:16:45,000
they lead to distraction, they lead to dizziness.

174
00:16:45,000 --> 00:16:48,000
You have to be busy trying to attain,

175
00:16:48,000 --> 00:16:51,000
and trying to keep and maintain the things that you like.

176
00:16:51,000 --> 00:16:53,000
If you want all sorts of luxuries,

177
00:16:53,000 --> 00:16:56,000
well you've got to look, you've got to work for it.

178
00:16:56,000 --> 00:17:03,000
We've got to work for some money to get a nice car,

179
00:17:03,000 --> 00:17:10,000
and a nice house.

180
00:17:10,000 --> 00:17:20,000
Lots and lots of things that get us tied down.

181
00:17:20,000 --> 00:17:24,000
So we have to be mindful of the things that we need.

182
00:17:24,000 --> 00:17:29,000
The third is we have to be mindful of our livelihood,

183
00:17:29,000 --> 00:17:31,000
how we live our lives.

184
00:17:31,000 --> 00:17:33,000
You have to go and make a living,

185
00:17:33,000 --> 00:17:35,000
well even monks have to make a living,

186
00:17:35,000 --> 00:17:37,000
but we have to be conscientious about this.

187
00:17:37,000 --> 00:17:39,000
So part of morality is our livelihood.

188
00:17:39,000 --> 00:17:41,000
How do you make a living?

189
00:17:41,000 --> 00:17:42,000
Do you cheat people for a living?

190
00:17:42,000 --> 00:17:43,000
Do you kill for a living?

191
00:17:43,000 --> 00:17:45,000
Do you steal for a living?

192
00:17:45,000 --> 00:17:48,000
Do you live for a living?

193
00:17:48,000 --> 00:17:52,000
Do you sell things that harm other beings?

194
00:17:52,000 --> 00:18:00,000
Engage in practices that are directly supportive of suffering?

195
00:18:00,000 --> 00:18:04,000
This is an important part of morality,

196
00:18:04,000 --> 00:18:06,000
because it worries us.

197
00:18:06,000 --> 00:18:11,000
We feel guilty and will be caught up in associating with people

198
00:18:11,000 --> 00:18:17,000
and situations that are on wholesome.

199
00:18:17,000 --> 00:18:24,000
Our cause for a mercy.

200
00:18:24,000 --> 00:18:30,000
The fourth one is the final one,

201
00:18:30,000 --> 00:18:34,000
because it actually leads into the practice of concentration,

202
00:18:34,000 --> 00:18:37,000
and that's guarding the senses.

203
00:18:37,000 --> 00:18:41,000
The highest form of morality is guarding one's senses.

204
00:18:41,000 --> 00:18:43,000
If you can guard your senses,

205
00:18:43,000 --> 00:18:45,000
the rest of them will come by themselves.

206
00:18:45,000 --> 00:18:47,000
What does it mean by guarding the senses?

207
00:18:47,000 --> 00:18:50,000
The senses are our doors to reality.

208
00:18:50,000 --> 00:18:55,000
I think I talked about this a couple weeks ago.

209
00:18:55,000 --> 00:19:00,000
An analogy is like a door that really lets people in.

210
00:19:00,000 --> 00:19:04,000
It's important to be careful who you let in.

211
00:19:04,000 --> 00:19:09,000
It's important that we be careful what we let in through the doors of the senses.

212
00:19:09,000 --> 00:19:17,000
If we let the doors of the senses give rise to liking,

213
00:19:17,000 --> 00:19:23,000
we agree with the version,

214
00:19:23,000 --> 00:19:27,000
then it will leave you.

215
00:19:27,000 --> 00:19:31,000
It will lead to suffering,

216
00:19:31,000 --> 00:19:33,000
at least in distraction.

217
00:19:33,000 --> 00:19:35,000
It prevents us from focusing.

218
00:19:35,000 --> 00:19:39,000
It prevents us from being collected in the mind,

219
00:19:39,000 --> 00:19:43,000
from cultivating a collective mindset.

220
00:19:43,000 --> 00:19:50,000
This is where meditation comes in.

221
00:19:50,000 --> 00:19:52,000
Once you start practicing meditation,

222
00:19:52,000 --> 00:19:54,000
then concentration comes,

223
00:19:54,000 --> 00:19:57,000
but the meditation is really about guarding the senses.

224
00:19:57,000 --> 00:20:00,000
When you see, let it just be seen.

225
00:20:00,000 --> 00:20:03,000
When you hear, let it just be hearing.

226
00:20:03,000 --> 00:20:06,000
Actually, in my tradition, we'll actually remind ourselves,

227
00:20:06,000 --> 00:20:13,000
see, when you hear something hearing,

228
00:20:13,000 --> 00:20:16,000
if your pain was a pain,

229
00:20:16,000 --> 00:20:22,000
just staying with it and trying to be mindful.

230
00:20:22,000 --> 00:20:25,000
So that our mind becomes collected,

231
00:20:25,000 --> 00:20:30,000
becomes focused.

232
00:20:30,000 --> 00:20:32,000
So that's all for morality.

233
00:20:32,000 --> 00:20:35,000
The second one, concentration.

234
00:20:35,000 --> 00:20:38,000
Concentration in our tradition is,

235
00:20:38,000 --> 00:20:43,000
I guess, most traditions is of two kinds.

236
00:20:43,000 --> 00:20:46,000
We have what is called upachara, samadhi,

237
00:20:46,000 --> 00:20:49,000
and apalincy.

238
00:20:49,000 --> 00:20:52,000
So samadhi in general is about focusing the mind,

239
00:20:52,000 --> 00:20:56,000
and apalincy means you've attained perfect focus.

240
00:20:56,000 --> 00:21:05,000
Apalincy is about attained or reached, entered into.

241
00:21:05,000 --> 00:21:08,000
So there's concentration where one actually enters into a focus.

242
00:21:08,000 --> 00:21:12,000
Stay the trance state, really.

243
00:21:12,000 --> 00:21:16,000
A state where one takes a single object

244
00:21:16,000 --> 00:21:27,000
and stays with that object fixed on that object.

245
00:21:27,000 --> 00:21:33,000
So this is a common concentration that comes from

246
00:21:33,000 --> 00:21:36,000
the practice of samadhi meditation.

247
00:21:36,000 --> 00:21:40,000
When you focus on a concept like a color,

248
00:21:40,000 --> 00:21:46,000
when you practice loving kindness,

249
00:21:46,000 --> 00:21:49,000
mindfulness or breathing that kind of thing,

250
00:21:49,000 --> 00:21:54,000
anything that focuses you on a single thing.

251
00:21:54,000 --> 00:21:58,000
Upachara samadhi is also found in

252
00:21:58,000 --> 00:22:01,000
some of the meditation on tranquility meditation,

253
00:22:01,000 --> 00:22:04,000
but it takes a different object,

254
00:22:04,000 --> 00:22:06,000
or it takes different objects.

255
00:22:06,000 --> 00:22:10,000
It doesn't focus only on a single lasting concept

256
00:22:10,000 --> 00:22:13,000
that you can depend upon.

257
00:22:13,000 --> 00:22:18,000
It focuses on things that are potentially changing.

258
00:22:18,000 --> 00:22:22,000
Our focus is on different things at different times.

259
00:22:22,000 --> 00:22:26,000
Our focus is on things that are not clear,

260
00:22:26,000 --> 00:22:30,000
not clearly defined.

261
00:22:30,000 --> 00:22:33,000
So there are certain tranquility practices

262
00:22:33,000 --> 00:22:37,000
that lead to what we thought Upachara samadhi,

263
00:22:37,000 --> 00:22:43,000
which means Upachara means the neighborhood

264
00:22:43,000 --> 00:22:45,000
or the area around.

265
00:22:45,000 --> 00:22:49,000
So Upachara means approaching the approach.

266
00:22:49,000 --> 00:22:51,000
So if Oppenai is when you enter into something,

267
00:22:51,000 --> 00:22:53,000
like entering into a house,

268
00:22:53,000 --> 00:22:59,000
Oppenai is the courtyard or the sidewalk or something.

269
00:22:59,000 --> 00:23:03,000
It's the approach.

270
00:23:03,000 --> 00:23:07,000
And it's rather than being fixed.

271
00:23:07,000 --> 00:23:11,000
It's more of a fluid, dynamic, flexible sort of

272
00:23:11,000 --> 00:23:13,000
mindfulness of concentration,

273
00:23:13,000 --> 00:23:16,000
where your mind can move from one object to another,

274
00:23:16,000 --> 00:23:18,000
but it's still focused.

275
00:23:18,000 --> 00:23:21,000
It's able to be focused on each thing

276
00:23:21,000 --> 00:23:23,000
for the time that it arises,

277
00:23:23,000 --> 00:23:26,000
for the time that it's experienced.

278
00:23:26,000 --> 00:23:29,000
And so that's the sort of concentration

279
00:23:29,000 --> 00:23:31,000
that we use in our tradition,

280
00:23:31,000 --> 00:23:33,000
in vipassana meditation.

281
00:23:33,000 --> 00:23:35,000
We focus on something just for the time that it's there,

282
00:23:35,000 --> 00:23:37,000
so let's say C.

283
00:23:37,000 --> 00:23:40,000
And it's there, but then we let it go.

284
00:23:40,000 --> 00:23:42,000
Because it goes, it disappears.

285
00:23:42,000 --> 00:23:45,000
You have pain, you say pain, pain,

286
00:23:45,000 --> 00:23:46,000
but then it disappears.

287
00:23:46,000 --> 00:23:50,000
So you're jumping from one thing to another.

288
00:23:50,000 --> 00:23:53,000
But this is an important type of concentration

289
00:23:53,000 --> 00:23:57,000
because it allows us to see nature.

290
00:23:57,000 --> 00:23:59,000
If you focus on a concept and go into a trance,

291
00:23:59,000 --> 00:24:02,000
you're not really seeing anything about nature.

292
00:24:02,000 --> 00:24:05,000
You're just pure in the mind, calm in the mind.

293
00:24:05,000 --> 00:24:06,000
It's a nice state.

294
00:24:06,000 --> 00:24:10,000
There's nothing to do with the causes for your problems,

295
00:24:10,000 --> 00:24:13,000
the causes for your suffering.

296
00:24:13,000 --> 00:24:16,000
When instead you're flexible in looking at reality

297
00:24:16,000 --> 00:24:18,000
from moment to moment,

298
00:24:18,000 --> 00:24:26,000
you're concentrated on your own experiences.

299
00:24:26,000 --> 00:24:29,000
Then this has the potential to lead to the third training.

300
00:24:29,000 --> 00:24:32,000
So this is an important one.

301
00:24:32,000 --> 00:24:34,000
It leads to wisdom.

302
00:24:34,000 --> 00:24:38,000
Wisdom will begin to arise.

303
00:24:38,000 --> 00:24:42,000
Now wisdom is of three kinds.

304
00:24:42,000 --> 00:24:49,000
I mean, these numberings are not the definitive enumeration,

305
00:24:49,000 --> 00:24:51,000
but they're a good way of understanding.

306
00:24:51,000 --> 00:24:54,000
So there's three kinds of what we could call wisdom.

307
00:24:54,000 --> 00:24:58,000
First kind of wisdom comes from thinking,

308
00:24:58,000 --> 00:25:03,000
sorry for hearing, from studying,

309
00:25:03,000 --> 00:25:05,000
from having someone else tell you,

310
00:25:05,000 --> 00:25:07,000
all of what I'm telling you now,

311
00:25:07,000 --> 00:25:09,000
if you didn't know about that,

312
00:25:09,000 --> 00:25:12,000
now you can say, oh yes, there are three types.

313
00:25:12,000 --> 00:25:14,000
There are three things we should train ourselves in.

314
00:25:14,000 --> 00:25:16,000
You could go to someone and say,

315
00:25:16,000 --> 00:25:17,000
well, it's Buddhist.

316
00:25:17,000 --> 00:25:19,000
There are three things we should train ourselves in.

317
00:25:19,000 --> 00:25:20,000
More out.

318
00:25:20,000 --> 00:25:22,000
And you can rattle it all off.

319
00:25:22,000 --> 00:25:26,000
And people would say, oh, wow, you're wise.

320
00:25:26,000 --> 00:25:28,000
And we might shake that off and say,

321
00:25:28,000 --> 00:25:30,000
oh no, no, I just heard this from someone,

322
00:25:30,000 --> 00:25:33,000
but nonetheless, it is wisdom that you have passed on.

323
00:25:33,000 --> 00:25:36,000
It's not, in a sense, we would say it's not your wisdom,

324
00:25:36,000 --> 00:25:39,000
but into some extent it is.

325
00:25:39,000 --> 00:25:42,000
It's something that you know to some degree.

326
00:25:42,000 --> 00:25:44,000
To some degree, of course, you still don't know it,

327
00:25:44,000 --> 00:25:46,000
unless you've realized it for yourself.

328
00:25:46,000 --> 00:25:49,000
And that's, of course, the more important knowing,

329
00:25:49,000 --> 00:25:51,000
but you know the teaching.

330
00:25:53,000 --> 00:25:56,000
I mean, it's an important distinction to me,

331
00:25:56,000 --> 00:25:58,000
because otherwise you hear something from someone,

332
00:25:58,000 --> 00:26:00,000
you say, oh yeah, I know that already.

333
00:26:00,000 --> 00:26:03,000
But you can know everything and know nothing.

334
00:26:03,000 --> 00:26:05,000
You can know the whole tititica,

335
00:26:05,000 --> 00:26:06,000
the whole of the Buddha's teaching,

336
00:26:06,000 --> 00:26:08,000
and not understand any of it.

337
00:26:11,000 --> 00:26:13,000
So we give a nod to that type of wisdom,

338
00:26:13,000 --> 00:26:16,000
but of course it's the most inferior,

339
00:26:16,000 --> 00:26:22,000
the most base, and it's an inferior type of wisdom.

340
00:26:22,000 --> 00:26:23,000
It's still important.

341
00:26:23,000 --> 00:26:27,000
I mean, actually, we shouldn't underestimate the importance

342
00:26:27,000 --> 00:26:29,000
of that kind of wisdom,

343
00:26:29,000 --> 00:26:32,000
because if you didn't hear about these things,

344
00:26:32,000 --> 00:26:35,000
the odds of you finding them on your own,

345
00:26:35,000 --> 00:26:37,000
it's pretty, pretty low.

346
00:26:46,000 --> 00:26:49,000
The second type of wisdom is from thinking.

347
00:26:49,000 --> 00:26:51,000
So whether you've heard something,

348
00:26:51,000 --> 00:26:54,000
or whether you've just come up with thoughts on your own,

349
00:26:54,000 --> 00:26:55,000
you start to think and ponder,

350
00:26:55,000 --> 00:26:57,000
and this is for all the philosophers,

351
00:26:57,000 --> 00:27:00,000
like all the philosophers write their books.

352
00:27:00,000 --> 00:27:03,000
I bet many of those philosophers

353
00:27:03,000 --> 00:27:05,000
didn't do intensive meditation practice,

354
00:27:05,000 --> 00:27:08,000
but a lot of philosophers had strong minds

355
00:27:08,000 --> 00:27:13,000
and were able to come up with all sorts of theories and ideas.

356
00:27:13,000 --> 00:27:15,000
But I bet if you look at some of their lives

357
00:27:15,000 --> 00:27:19,000
that they were actually many of them quite miserable.

358
00:27:19,000 --> 00:27:22,000
I bet you would see that not all of them were happy,

359
00:27:22,000 --> 00:27:25,000
not all of them were satisfied.

360
00:27:25,000 --> 00:27:27,000
Sometimes the smarter they are,

361
00:27:27,000 --> 00:27:30,000
the more miserable they were.

362
00:27:33,000 --> 00:27:35,000
Because they knew what they didn't know.

363
00:27:35,000 --> 00:27:38,000
Or they came up with ideas that were actually wrong

364
00:27:38,000 --> 00:27:40,000
and were able to justify them,

365
00:27:40,000 --> 00:27:44,000
until they followed wrong philosophies.

366
00:27:44,000 --> 00:27:46,000
But because they had strong minds,

367
00:27:46,000 --> 00:27:48,000
they could argue quite well.

368
00:27:48,000 --> 00:27:51,000
That kind of thing.

369
00:27:51,000 --> 00:27:52,000
So if I'm thinking,

370
00:27:52,000 --> 00:27:54,000
I mean, for Buddhist meditators,

371
00:27:54,000 --> 00:27:55,000
even this thinking is important,

372
00:27:55,000 --> 00:27:58,000
it's important to think about your practice from time to time

373
00:27:58,000 --> 00:28:00,000
and to say, okay, something's wrong here.

374
00:28:00,000 --> 00:28:02,000
I have to adjust this.

375
00:28:02,000 --> 00:28:04,000
Maybe I'm doing too much sitting meditation.

376
00:28:04,000 --> 00:28:08,000
I should do walking as well and maybe I'm doing too much walking

377
00:28:08,000 --> 00:28:11,000
and so I should sit down or maybe I need to lie down

378
00:28:11,000 --> 00:28:13,000
or maybe I shouldn't lie down.

379
00:28:13,000 --> 00:28:16,000
Maybe I should try to eat less,

380
00:28:16,000 --> 00:28:18,000
try to sleep less, try to talk less.

381
00:28:18,000 --> 00:28:21,000
You start to analyze things.

382
00:28:21,000 --> 00:28:24,000
And so this is just kind of wisdom is important.

383
00:28:24,000 --> 00:28:27,000
It's important to have this sense of

384
00:28:27,000 --> 00:28:29,000
what's right and what's wrong

385
00:28:29,000 --> 00:28:33,000
and it's the right way to go about things.

386
00:28:33,000 --> 00:28:36,000
Of course, just coming up with things

387
00:28:36,000 --> 00:28:39,000
intellectually,

388
00:28:39,000 --> 00:28:44,000
still isn't the same as really knowing them.

389
00:28:44,000 --> 00:28:47,000
And so the final type of wisdom

390
00:28:47,000 --> 00:28:51,000
and really the pinnacle of all Buddhist practice

391
00:28:51,000 --> 00:28:56,000
is the wisdom that comes from

392
00:28:56,000 --> 00:29:00,000
actual experience.

393
00:29:00,000 --> 00:29:04,000
Comes from the practice of

394
00:29:04,000 --> 00:29:07,000
magnifying your experience and a sense of

395
00:29:07,000 --> 00:29:12,000
bologna and bolognes, cultivation.

396
00:29:12,000 --> 00:29:17,000
So the idea is there are certain truths

397
00:29:17,000 --> 00:29:20,000
that are in the world around us.

398
00:29:20,000 --> 00:29:22,000
They're right under our nose,

399
00:29:22,000 --> 00:29:24,000
they're right in front of us.

400
00:29:24,000 --> 00:29:25,000
They're all around us.

401
00:29:25,000 --> 00:29:28,000
They're in everything we do everywhere.

402
00:29:28,000 --> 00:29:31,000
We go there, these truths are there.

403
00:29:31,000 --> 00:29:34,000
But we can't see them because we're not focused.

404
00:29:34,000 --> 00:29:36,000
And so this is the type of wisdom

405
00:29:36,000 --> 00:29:40,000
that comes from focus, from concentration.

406
00:29:40,000 --> 00:29:43,000
It's where our minds are like a lens out of focus

407
00:29:43,000 --> 00:29:45,000
and until you focus the lens,

408
00:29:45,000 --> 00:29:47,000
until you learn how to focus the lens,

409
00:29:47,000 --> 00:29:50,000
clearly the things right under your nose,

410
00:29:50,000 --> 00:29:52,000
you can see things right in front of you

411
00:29:52,000 --> 00:29:57,000
are not comprehendable.

412
00:29:57,000 --> 00:30:00,000
And so that's really, in a sense,

413
00:30:00,000 --> 00:30:01,000
that's all meditation is.

414
00:30:01,000 --> 00:30:03,000
It's about clearing up the mind

415
00:30:03,000 --> 00:30:07,000
so that you can see things

416
00:30:07,000 --> 00:30:08,000
as they are.

417
00:30:08,000 --> 00:30:11,000
You can see these truths of reality.

418
00:30:11,000 --> 00:30:13,000
You can see it most importantly,

419
00:30:13,000 --> 00:30:15,000
the form of the truth.

420
00:30:15,000 --> 00:30:18,000
You see the suffering, what is suffering?

421
00:30:18,000 --> 00:30:19,000
What is suffering?

422
00:30:19,000 --> 00:30:21,000
What sorts of things are suffering?

423
00:30:21,000 --> 00:30:23,000
You see what is the cause of suffering?

424
00:30:23,000 --> 00:30:25,000
What is it that's causing you this suffering?

425
00:30:25,000 --> 00:30:27,000
What am I doing to be able to see that?

426
00:30:29,000 --> 00:30:31,000
They will be able to see that

427
00:30:31,000 --> 00:30:34,000
you're only suffering because of certain things.

428
00:30:34,000 --> 00:30:37,000
And when those things are seeing the cease,

429
00:30:37,000 --> 00:30:39,000
you don't suffer.

430
00:30:41,000 --> 00:30:44,000
It sees three things that sort of set you on the fourth one,

431
00:30:44,000 --> 00:30:46,000
which is the right path.

432
00:30:46,000 --> 00:30:48,000
So eventually you get on the path

433
00:30:48,000 --> 00:30:50,000
which leads to the cessation of suffering.

434
00:30:56,000 --> 00:30:58,000
So three things,

435
00:30:58,000 --> 00:31:03,000
three trainings

436
00:31:03,000 --> 00:31:06,000
that we undertake as Buddhists

437
00:31:06,000 --> 00:31:08,000
that I think are fairly universal

438
00:31:08,000 --> 00:31:11,000
and that's important.

439
00:31:11,000 --> 00:31:13,000
You're going to get an understanding

440
00:31:13,000 --> 00:31:14,000
of what Buddhism is.

441
00:31:14,000 --> 00:31:18,000
I think these three things are more or less essential.

442
00:31:18,000 --> 00:31:21,000
It would be hard pressed to go without them.

443
00:31:21,000 --> 00:31:24,000
So I imagine probably there is a Buddhist school

444
00:31:24,000 --> 00:31:28,000
that doesn't sort of describe it by far.

445
00:31:28,000 --> 00:31:32,000
There are sort of three things that are the pillars of Buddhism.

446
00:31:37,000 --> 00:31:40,000
That's my teaching for today.

447
00:31:40,000 --> 00:31:43,000
So I think in becoming, if you have any questions,

448
00:31:43,000 --> 00:31:46,000
I'm happy to take them.

449
00:31:46,000 --> 00:32:10,000
Welcome to speed.

450
00:32:16,000 --> 00:32:18,000
Thank you.

451
00:32:32,000 --> 00:32:36,000
You're most welcome and happy to come out.

452
00:32:36,000 --> 00:32:46,000
There are no questions.

453
00:32:46,000 --> 00:32:49,000
So thank you.

454
00:32:49,000 --> 00:32:52,000
It's good to see such a turnout to new people as well.

455
00:32:52,000 --> 00:32:55,000
People have never seen.

456
00:32:55,000 --> 00:32:59,000
Old people, people are close to recognize.

457
00:32:59,000 --> 00:33:02,000
Good to see you all.

458
00:33:02,000 --> 00:33:06,000
Have a good day.

459
00:33:32,000 --> 00:33:36,000
Have a good day.

460
00:34:02,000 --> 00:34:06,000
Have a good day.

