1
00:00:00,000 --> 00:00:26,000
Good evening, everyone.

2
00:00:26,000 --> 00:00:34,000
Good morning, everyone.

3
00:00:34,000 --> 00:00:42,000
Good morning.

4
00:00:42,000 --> 00:00:49,000
Today's quote is from the Jataka.

5
00:00:49,000 --> 00:01:00,000
Jataka, of course, it's best to go with a column translation, because they rhyme.

6
00:01:00,000 --> 00:01:06,000
But to get a sense of why it's important that they should rhyme,

7
00:01:06,000 --> 00:01:13,000
and look at the polling first, because it's, well, I mean, important.

8
00:01:13,000 --> 00:01:21,000
It's actually not all that important, but, you know, there's some rhythm to it,

9
00:01:21,000 --> 00:01:26,000
and you could argue that that's a good thing, because it makes it easy to remember.

10
00:01:26,000 --> 00:01:28,000
It makes it memorable.

11
00:01:28,000 --> 00:01:30,000
It has a certain power to it.

12
00:01:30,000 --> 00:01:37,000
It's power to create confidence and appreciation.

13
00:01:37,000 --> 00:01:46,000
But on the other side, it's possible to become attached to it, so we should be careful.

14
00:01:46,000 --> 00:01:53,000
Anyway, verses are quite powerful, so here they are.

15
00:01:53,000 --> 00:02:22,000
It's the first verse, easy to understand the cries of the jackal or the bird.

16
00:02:22,000 --> 00:02:30,000
But the cry of a human looking is far harder to understand than those.

17
00:02:30,000 --> 00:02:35,000
That's my translation, but here's the column translation.

18
00:02:35,000 --> 00:02:40,000
The cry of jackals or birds is understood with ease.

19
00:02:40,000 --> 00:02:41,000
Yay.

20
00:02:41,000 --> 00:02:48,000
But the word of men looking is darker, far than these.

21
00:02:48,000 --> 00:02:54,000
So it's easy to understand the cry of an animal, animals when they talk.

22
00:02:54,000 --> 00:02:56,000
They're pretty easy to understand.

23
00:02:56,000 --> 00:02:59,000
Do they like you? Do they dislike you?

24
00:02:59,000 --> 00:03:11,000
In a long time, you can get a sense of the emotion behind their cries for the most part easy to understand.

25
00:03:11,000 --> 00:03:17,000
The only animal that probably comes close that I can think up to humans is, of course,

26
00:03:17,000 --> 00:03:28,000
the monkey kind, who tend to be able to trick each other and humans as well.

27
00:03:28,000 --> 00:03:32,000
If you ever live with monkeys, they can be pretty sneaky.

28
00:03:32,000 --> 00:03:35,000
But humans take the cake.

29
00:03:35,000 --> 00:03:42,000
We can say one thing, do another thing, and think an entirely different thing.

30
00:03:42,000 --> 00:03:58,000
We totally deceptive. In fact, for the most part, we spend much of our days being deceptive saying things that cover up rather than elucidate our feelings.

31
00:03:58,000 --> 00:04:14,000
We're very good as human beings to say things like, someone asks, how are you, and we feel terrible as an animal.

32
00:04:14,000 --> 00:04:29,000
Do I look fat in this? No, that kind of thing.

33
00:04:29,000 --> 00:04:32,000
These little white lines we talk about.

34
00:04:32,000 --> 00:04:49,000
Maybe that's going too far. I wasn't thinking of lies. When you're angry at someone but you want to be polite, and so you hide it, you smile.

35
00:04:49,000 --> 00:04:55,000
Humans are very good at this. We're very good at hiding how we feel when you're at school.

36
00:04:55,000 --> 00:05:14,000
It's quite incredible when you actually sit down and talk to some of the students. Most of them, I have many of them, are in a lot of stress, and they hide it, and they smile, and they joke, and they pretend that they're happy, because well everyone else seems like they're happy, so if you'll guilty that they're not happy, then they pretend.

37
00:05:14,000 --> 00:05:30,000
A lot of pretending going on. Today I thought meditation, then I was a very busy day. Then a test in Latin, didn't do very well.

38
00:05:30,000 --> 00:05:53,000
Then I spent the rest of it most of the day sitting, not too many people, but a handful, maybe 10 people today came by, and then in the afternoon I taught a group of meditators who, by special invitation invited me to give them a talk on meditation.

39
00:05:53,000 --> 00:06:19,000
So I had a half an hour with them, and then I went back downstairs, sat at the five-minute meditation table. Then this afternoon I went to a meeting of the McMaster Indigenous Student Community Alliance.

40
00:06:19,000 --> 00:06:46,000
And it was a neat thing, the First Nations people, the Indigenous people, they have, well, more than that, and why it's appropriate for this talk, is they were really crapped on by the Canadian people, and continue to be.

41
00:06:46,000 --> 00:07:08,000
And it's not well publicized how poorly they've been treated, and I think how it ties into this is how messed up we are really.

42
00:07:08,000 --> 00:07:23,000
We do it every weekend to manipulate each other. I'm not talking actually individually, but more on a global level. This world is pretty messed up.

43
00:07:23,000 --> 00:07:45,000
I think trying to fix the situation is probably futile, and the worst is trying to change political systems, or trying to protest.

44
00:07:45,000 --> 00:08:11,000
If it's going to change, if there's some hope for humanity still, it's got to be through appeal to goodness, trying to convince people of the truth that manipulating each other and hurting each other, doesn't lead to happiness, doesn't lead to peace, doesn't lead to anything good.

45
00:08:11,000 --> 00:08:26,000
To start, I guess, being more straight, so the part of this is how tricky humans can be. That's really what this quote is about. It has some curious twists and turns, and it says a few things.

46
00:08:26,000 --> 00:08:43,000
So the first thing it says is that it's hard to know, and that's really the point that's being made in this chapter. The king, this is the backstory, is this king asks, a goose, a bodhisattva is a goose, an animal who can talk, and so they're talking.

47
00:08:43,000 --> 00:09:05,000
And he says to the goose, you know, stay with me. Live here with me. You're a king of the goose, a geese. I'm a king of humans. You can live together. He says, no, no, no. You'll get drunk. He says, probably you'll get drunk one day, and decide you want to have roast duck, a roast goose, and that'll be it for me.

48
00:09:05,000 --> 00:09:24,000
And he says, and I'll stop. He says, no, no, I'll stop drinking. I will drink alcohol while you're here, and then he launches into this. He says, oh, it's hard to know the words of men. If you were a jackal or a bird, then I'd be able to understand what you're saying.

49
00:09:24,000 --> 00:09:39,000
Manosa, Vasitang, Raja, Dubi, Jana, Taran, Dato, it's hard to understand, especially for a goose, I guess. So now he can understand the human. But he still doesn't trust him.

50
00:09:39,000 --> 00:09:55,000
And then he says, apite, manatee, poo, son, yati, mito, sakati, wah. Whether, hmm, cuz the verses are hard to see.

51
00:09:55,000 --> 00:09:59,000
Okay, we think, right?

52
00:09:59,000 --> 00:10:10,000
One may, a man may think, nati, mito, sakati, right? This is a relative of friend or companion.

53
00:10:10,000 --> 00:10:34,000
But, yo, pube, sumino, hu, wah, this is a good quote. Yo, pube, sumino, hu, wah, pacha, sambhajatee, diso.

54
00:10:34,000 --> 00:10:42,000
Let's see what the English is. A man may think, this is my friend, my comrade or my kin, is my comrade of my kin.

55
00:10:42,000 --> 00:10:48,000
But friendship goes and often hate and enmity begin.

56
00:10:48,000 --> 00:10:54,000
Disread the English, who has your heart is near to you with you or air he be.

57
00:10:54,000 --> 00:11:02,000
But who dwells with you and your heart is strange, a far as he.

58
00:11:02,000 --> 00:11:08,000
When your house of kindly heart shall be is kindly still the far across the sea.

59
00:11:08,000 --> 00:11:15,000
When your house shall host, I'll be of heart. Hostile he is the ocean wide apart.

60
00:11:15,000 --> 00:11:20,000
Thy foes, O Lord of chariots, though near the RFR.

61
00:11:20,000 --> 00:11:25,000
But foster of thy realm, the good in heart, close link it are.

62
00:11:25,000 --> 00:11:32,000
Who stays too long, find oftentimes that friend is changed to foe.

63
00:11:32,000 --> 00:11:36,000
Then air I lose your friendship, I will take my leave and go.

64
00:11:36,000 --> 00:11:40,000
So he's making an argument for why he should leave.

65
00:11:40,000 --> 00:11:48,000
Because, you know, friend and foe, well, human ingus, you know how the story ends.

66
00:11:48,000 --> 00:12:00,000
And often ends in dead goose.

67
00:12:00,000 --> 00:12:06,000
But that's the way of humans is where heart to understand.

68
00:12:06,000 --> 00:12:08,000
So how does this relate to our meditation?

69
00:12:08,000 --> 00:12:13,000
How can I relate this to our meditator?

70
00:12:13,000 --> 00:12:21,000
We have to be careful in our minds. You can, you can fool others, but you can't fool yourself.

71
00:12:21,000 --> 00:12:25,000
Like you can, you can try and you can work at it.

72
00:12:25,000 --> 00:12:27,000
But the result isn't satisfying.

73
00:12:27,000 --> 00:12:34,000
I mean, so by trick others, you can trick others into doing what you want.

74
00:12:34,000 --> 00:12:36,000
But it doesn't work with the mind.

75
00:12:36,000 --> 00:12:41,000
But you don't get positive results by fooling yourself.

76
00:12:41,000 --> 00:12:44,000
You don't get more or more messed up.

77
00:12:44,000 --> 00:12:47,000
So the inside, whether we tell the truth to others,

78
00:12:47,000 --> 00:12:51,000
if we tell the truth to ourselves, it's very important.

79
00:12:51,000 --> 00:12:54,000
That's the essential in meditation.

80
00:12:54,000 --> 00:13:06,000
So we have to stop pretending and often it comes down to just,

81
00:13:06,000 --> 00:13:11,000
denying our experiences.

82
00:13:11,000 --> 00:13:13,000
You know, thinking, no, no, no, I can't do that.

83
00:13:13,000 --> 00:13:17,000
I can't be that. This is not right. This is not right.

84
00:13:17,000 --> 00:13:20,000
Instead of actually looking at the experience,

85
00:13:20,000 --> 00:13:24,000
instead of being honest with ourselves saying it's there.

86
00:13:24,000 --> 00:13:31,000
And it's under my control.

87
00:13:31,000 --> 00:13:40,000
And looking at it, honestly, giving it an honest analysis.

88
00:13:40,000 --> 00:13:47,000
We look at our emotions, look at our thoughts.

89
00:13:47,000 --> 00:13:50,000
And being honest with them, not trying to suppress them or say,

90
00:13:50,000 --> 00:13:52,000
no, no, no, it's not right.

91
00:13:52,000 --> 00:13:53,000
It doesn't work.

92
00:13:53,000 --> 00:13:57,000
You have to give them an honest appraisal.

93
00:13:57,000 --> 00:14:01,000
The good ones and the bad ones.

94
00:14:01,000 --> 00:14:03,000
It's the only way you can learn.

95
00:14:03,000 --> 00:14:08,000
It's the only way you can learn what's right and wrong.

96
00:14:08,000 --> 00:14:15,000
You have to be honest with ourselves.

97
00:14:15,000 --> 00:14:17,000
Should be honest with each other as well.

98
00:14:17,000 --> 00:14:19,000
Should be open.

99
00:14:19,000 --> 00:14:22,000
And it would be great if we could all be straight and honest.

100
00:14:22,000 --> 00:14:26,000
I mean, sometimes it's not right to be open and honest.

101
00:14:26,000 --> 00:14:28,000
Sometimes you have to be a little bit deceptive.

102
00:14:28,000 --> 00:14:34,000
Should never lie, but there are times where you should be careful with the truth.

103
00:14:34,000 --> 00:14:39,000
Because if you're too straight forward, people get the wrong idea,

104
00:14:39,000 --> 00:14:42,000
because we all have, you know, defilements.

105
00:14:42,000 --> 00:14:48,000
If you're too honest, you can make people angry or so.

106
00:14:48,000 --> 00:14:52,000
Sometimes you have to use subterfuge.

107
00:14:52,000 --> 00:14:58,000
Not lying. Lying is problem.

108
00:14:58,000 --> 00:15:01,000
But other people sometimes you have to be human.

109
00:15:01,000 --> 00:15:06,000
I mean, it actually can be useful thing at times to, you know,

110
00:15:06,000 --> 00:15:13,000
even if you don't want to scare someone, you want to give them confidence.

111
00:15:13,000 --> 00:15:24,000
Or you want someone to do, to not dwell on their faults.

112
00:15:24,000 --> 00:15:28,000
You can encourage them and it can see.

113
00:15:28,000 --> 00:15:31,000
But with yourself, it doesn't work like this.

114
00:15:31,000 --> 00:15:33,000
It's your self.

115
00:15:33,000 --> 00:15:39,000
The best way is honesty.

116
00:15:39,000 --> 00:15:43,000
Be honest with yourself, that's what insight is.

117
00:15:43,000 --> 00:15:46,000
It's about looking honestly and saying, this is the truth.

118
00:15:46,000 --> 00:15:49,000
Truth is this.

119
00:15:49,000 --> 00:15:53,000
I have good things and bad things and good things seeing them.

120
00:15:53,000 --> 00:15:55,000
It's good for seeing them for what they are.

121
00:15:55,000 --> 00:15:59,000
The bad things also seeing them for what they are.

122
00:15:59,000 --> 00:16:03,000
Because the clearer you see things, the better it gets.

123
00:16:03,000 --> 00:16:07,000
Bad things don't seem good when you look at them.

124
00:16:07,000 --> 00:16:11,000
The closer you look at them, the more clearly they are there.

125
00:16:11,000 --> 00:16:14,000
The problem with them is, more clearly you can see they are either

126
00:16:14,000 --> 00:16:18,000
the problem they are evil.

127
00:16:18,000 --> 00:16:26,000
How they hurt you, how they stress you.

128
00:16:26,000 --> 00:16:32,000
It's been a long day today, so I don't know.

129
00:16:32,000 --> 00:16:39,000
I think there is some interesting number, so I'm going to stop there.

130
00:16:39,000 --> 00:16:46,000
I'm going to open up the hangout, if anybody wants to come on and talk.

131
00:16:46,000 --> 00:17:10,000
Otherwise, we'll call it a night.

132
00:17:10,000 --> 00:17:34,000
Pretty busy this time of year.

133
00:17:34,000 --> 00:17:42,000
I've already said I'm going to New York, so that's happening.

134
00:17:42,000 --> 00:17:49,000
I'm not sure about going to UK.

135
00:17:49,000 --> 00:17:54,000
They want me to go for a long time, I think, but it's not going to work.

136
00:17:54,000 --> 00:17:56,000
Not if I want to write that.

137
00:17:56,000 --> 00:18:00,000
There's the idea to go to Thailand and Sri Lanka that might be happening.

138
00:18:00,000 --> 00:18:05,000
But that's the thing, is how long can I stay away?

139
00:18:05,000 --> 00:18:13,000
What's the minimum amount of time to stay in Thailand and Sri Lanka as well?

140
00:18:13,000 --> 00:18:16,000
It gets to be a long trip.

141
00:18:16,000 --> 00:18:29,000
So I'm only thinking the UK would be viable if either for a short time or if there was really something substantial going on.

142
00:18:29,000 --> 00:18:41,000
Because we kind of agreed that I probably should go to Thailand to see my teacher getting calls.

143
00:18:41,000 --> 00:18:52,000
I'm just trying to hang out with me.

144
00:18:52,000 --> 00:18:57,000
Who are you?

145
00:18:57,000 --> 00:19:04,000
I'm like, oh, make someone called Joseph trying to join the hangout.

146
00:19:04,000 --> 00:19:09,000
If you want to join the hangout, you have to go to meditation.surimungalow.org.

147
00:19:09,000 --> 00:19:21,000
That's where you find the link.

148
00:19:21,000 --> 00:19:29,000
Whoa, someone else.

149
00:19:29,000 --> 00:19:33,000
More than one person pinging me on hangouts.

150
00:19:33,000 --> 00:19:38,000
I'm an atheist, but I find your talks really insightful.

151
00:19:38,000 --> 00:19:40,000
You keep it open for everyone and thank you.

152
00:19:40,000 --> 00:19:50,000
Well, I'm an atheist too, so don't be afraid.

153
00:19:50,000 --> 00:19:54,000
And here is Ken.

154
00:19:54,000 --> 00:19:56,000
Do you guys have questions or are you just?

155
00:19:56,000 --> 00:20:06,000
Yeah, I have a question about things that maybe you're not really able to be mindful about.

156
00:20:06,000 --> 00:20:18,000
For instance, when I'm bringing something, I feel like I really can't be mindful of it because once I'm noting reading reading that I don't retain anything.

157
00:20:18,000 --> 00:20:22,000
Is there anything that you said about that kind of thing?

158
00:20:22,000 --> 00:20:24,000
Just two different activities.

159
00:20:24,000 --> 00:20:29,000
Reading is a different activity from meditating.

160
00:20:29,000 --> 00:20:31,000
You can do it in between reading.

161
00:20:31,000 --> 00:20:35,000
When you're starting to get a headache or something, it'll be mindful of you.

162
00:20:35,000 --> 00:20:38,000
You can still be mindful in between reading.

163
00:20:38,000 --> 00:20:40,000
The reading is not as linear as you think.

164
00:20:40,000 --> 00:20:47,000
There's time to process and then your mind wanders and you bring your mind back and so there is time in between readings

165
00:20:47,000 --> 00:20:50,000
to be mindful.

166
00:20:50,000 --> 00:20:53,000
But the act itself, that's so much.

167
00:20:53,000 --> 00:20:57,000
Not everything is meditation.

168
00:20:57,000 --> 00:20:59,000
Okay.

169
00:21:11,000 --> 00:21:12,000
Hello.

170
00:21:12,000 --> 00:21:14,000
Hey, Ken?

171
00:21:14,000 --> 00:21:15,000
Yeah.

172
00:21:15,000 --> 00:21:18,000
Can you hear me?

173
00:21:18,000 --> 00:21:20,000
I can hear you.

174
00:21:20,000 --> 00:21:21,000
Okay.

175
00:21:21,000 --> 00:21:28,000
I just wanted to say hi, but also that's an interesting thing about when people say, how are you?

176
00:21:28,000 --> 00:21:33,000
And then a lot of times I don't really know what to say.

177
00:21:33,000 --> 00:21:40,000
Sometimes I just say, oh, not bad, but there's kind of like a social convention to say you're doing fine.

178
00:21:40,000 --> 00:21:46,000
You know, and it's pretty subtle, but you know, it's sometimes confusing.

179
00:21:46,000 --> 00:21:49,000
And sometimes for a while there I used to just say,

180
00:21:49,000 --> 00:21:53,000
sukaduka, because it sounds like super duper, but it's actually saying,

181
00:21:53,000 --> 00:21:55,000
sukaduka, so you're good and bad.

182
00:21:55,000 --> 00:21:57,000
So I don't know.

183
00:21:57,000 --> 00:22:01,000
The Buddha would say, the Buddha and the monks would say,

184
00:22:01,000 --> 00:22:11,000
in fact, this is what they ask each other.

185
00:22:11,000 --> 00:22:13,000
Are you able to bear with it?

186
00:22:13,000 --> 00:22:14,000
Are you able to bear with it?

187
00:22:14,000 --> 00:22:16,000
Are you able to stand it?

188
00:22:16,000 --> 00:22:19,000
Can you even say, comedy, yes.

189
00:22:19,000 --> 00:22:21,000
I'm basically bearing up.

190
00:22:21,000 --> 00:22:30,000
I'm getting by and able to bear with it.

191
00:22:30,000 --> 00:22:38,000
I don't know about this pinging me on Hangouts, but

192
00:22:38,000 --> 00:22:41,000
if you think about all the questions, this is the problem now with questions.

193
00:22:41,000 --> 00:22:44,000
This is the answer to them all.

194
00:22:44,000 --> 00:22:48,000
And I'm just getting questions that I've answered that question a thousand times.

195
00:22:48,000 --> 00:22:51,000
Someone's asking about meditating with music.

196
00:22:51,000 --> 00:22:56,000
And I'm sure I've got two or three videos on meditating.

197
00:22:56,000 --> 00:23:01,000
I may have less than most of answered that one, right?

198
00:23:01,000 --> 00:23:02,000
Anyway.

199
00:23:02,000 --> 00:23:07,000
I've got a more probably a little more deeper question.

200
00:23:07,000 --> 00:23:14,000
You know, on the eightfold novel past, there's the right thought.

201
00:23:14,000 --> 00:23:24,000
And then one is where a link was meant.

202
00:23:24,000 --> 00:23:29,000
And the thought of renunciation.

203
00:23:29,000 --> 00:23:32,000
That's how I understood it anyways.

204
00:23:32,000 --> 00:23:39,000
And that seems like one that's sort of trying to cultivate that one.

205
00:23:39,000 --> 00:23:44,000
Seems like it's a bit of finesse.

206
00:23:44,000 --> 00:23:48,000
Right now, I'm just thinking, what would it be like right now?

207
00:23:48,000 --> 00:23:51,000
All I've got is what would it be right now?

208
00:23:51,000 --> 00:23:58,000
Right now, if I didn't have any craving, or I didn't have any aversion?

209
00:23:58,000 --> 00:24:00,000
Well, how would that be right now?

210
00:24:00,000 --> 00:24:02,000
And then I think, oh, that'd be positive.

211
00:24:02,000 --> 00:24:08,000
But just to renounce, seems like a bit of a jump.

212
00:24:08,000 --> 00:24:15,000
Well, you're dealing with English translations and with superficial understandings.

213
00:24:15,000 --> 00:24:19,000
I mean, not necessarily insult, but you have to understand exactly what's being said.

214
00:24:19,000 --> 00:24:25,000
They're all it is, it's the opposite of gamma-wetaka, which is sensual desire,

215
00:24:25,000 --> 00:24:27,000
lustful thoughts, desirous thoughts.

216
00:24:27,000 --> 00:24:34,000
And they come and just mean thoughts of thoughts that are free from desire.

217
00:24:34,000 --> 00:24:40,000
So when you see something that you want, being able to see it without wanting it.

218
00:24:40,000 --> 00:24:45,000
And that's more or less right thought.

219
00:24:45,000 --> 00:24:50,000
Yeah.

220
00:24:50,000 --> 00:24:55,000
It's not, you're overthinking it, I think, in terms of,

221
00:24:55,000 --> 00:24:57,000
you know, it means you have to give up.

222
00:24:57,000 --> 00:25:01,000
So if it's not actually thought, it's not actually thoughts like, oh,

223
00:25:01,000 --> 00:25:03,000
I should give this up, or I should give that up.

224
00:25:03,000 --> 00:25:08,000
I mean, those are good thoughts, but right thought is just thought without,

225
00:25:08,000 --> 00:25:11,000
without desiring, greed and greed.

226
00:25:11,000 --> 00:25:16,000
So it's just a more general that nakama-wetaka means.

227
00:25:16,000 --> 00:25:19,000
Name means need, not.

228
00:25:19,000 --> 00:25:22,000
So it's in comma.

229
00:25:22,000 --> 00:25:29,000
I'm actually not sure about that, but basically that sort of means without greed.

230
00:25:29,000 --> 00:25:38,000
So for a while, it might, there just might be a period of just kind of warming up to that idea of not

231
00:25:38,000 --> 00:25:43,000
thinking about things with desire or aversion.

232
00:25:43,000 --> 00:25:48,000
Well, it's more you look at the desire and you look at the things that you desire

233
00:25:48,000 --> 00:25:53,000
and you start to see that they're not worth desiring.

234
00:25:53,000 --> 00:25:55,000
That's what we pass in as all about.

235
00:25:55,000 --> 00:25:58,000
That's what the three characteristics are in permanent suffering and non-self.

236
00:25:58,000 --> 00:25:59,000
What does that mean?

237
00:25:59,000 --> 00:26:06,000
It means these things that you thought were stable, satisfying and controllable or not actually.

238
00:26:06,000 --> 00:26:12,000
When you see that, the desire goes away, desire for them disappeared.

239
00:26:12,000 --> 00:26:18,000
That's where that's essentially a core we pass in.

240
00:26:18,000 --> 00:26:25,000
Seeing the things that you thought were stable and unstable and changing all the things you thought were satisfying,

241
00:26:25,000 --> 00:26:28,000
not satisfying, mainly because they're unstable.

242
00:26:28,000 --> 00:26:32,000
And you can't change it, you can't control it, you can't fix it.

243
00:26:32,000 --> 00:26:36,000
Some things you thought were controllable and not controllable.

244
00:26:36,000 --> 00:26:45,000
So you give them up in this garbage, not for me.

245
00:26:45,000 --> 00:26:48,000
So slowly slowly develop it.

246
00:26:48,000 --> 00:26:50,000
I just try to see things as they are.

247
00:26:50,000 --> 00:26:51,000
I mean, the truth is there.

248
00:26:51,000 --> 00:26:54,000
It's not about belief or changing your opinion.

249
00:26:54,000 --> 00:26:56,000
It's not about opinions.

250
00:26:56,000 --> 00:27:01,000
Look and see and the claim is that this is what you'll see.

251
00:27:01,000 --> 00:27:03,000
You'll see impermanence suffering and non-self.

252
00:27:03,000 --> 00:27:04,000
Your desire will just disappear.

253
00:27:04,000 --> 00:27:06,000
And that's where right thought comes here.

254
00:27:06,000 --> 00:27:13,000
Right thought will naturally come through the practice.

255
00:27:13,000 --> 00:27:20,000
Now, if you want to be a little bit more conceptual about it and sort of daily life kind of thing,

256
00:27:20,000 --> 00:27:22,000
they're good thoughts, thoughts over an sensation.

257
00:27:22,000 --> 00:27:25,000
I think what sort of things can I give away?

258
00:27:25,000 --> 00:27:26,000
It's not right thought.

259
00:27:26,000 --> 00:27:29,000
I mean, that's not the definition of right thought.

260
00:27:29,000 --> 00:27:33,000
That's just a thought and it can still be filled with greed, anger, and delusion.

261
00:27:33,000 --> 00:27:37,000
You can be thinking, I want to be a good Buddhist, so I'm going to do this.

262
00:27:37,000 --> 00:27:42,000
I hate all this suffering that I have, so I'm going to get rid of everything.

263
00:27:42,000 --> 00:27:45,000
And then I'll be free for this anger base.

264
00:27:45,000 --> 00:27:54,000
They're good thoughts and they're the preliminary thoughts, but it's not right thought or noble right thought.

265
00:27:54,000 --> 00:27:56,000
But why I bring it up is because that's a good thing.

266
00:27:56,000 --> 00:27:59,000
It's a good thing. It's good to think about such things. I'm not trying to discount these thoughts.

267
00:27:59,000 --> 00:28:04,000
I could live without this. I could live without that.

268
00:28:04,000 --> 00:28:07,000
Those are good. Conventionally good.

269
00:28:07,000 --> 00:28:11,000
It's good to have such thoughts.

270
00:28:11,000 --> 00:28:16,000
And to be thoughtful in general, in a conventional sense.

271
00:28:16,000 --> 00:28:18,000
Sometimes we put too much emphasis on meditation.

272
00:28:18,000 --> 00:28:20,000
I don't want to do that.

273
00:28:20,000 --> 00:28:22,000
It's good to be thoughtful as a human being.

274
00:28:22,000 --> 00:28:26,000
And so social thoughtfulness is good as well.

275
00:28:26,000 --> 00:28:30,000
Worry about the environment, not worry, but be conscious of the environment.

276
00:28:30,000 --> 00:28:35,000
Be conscious of, like today we were talking about the TRC, the in Canada.

277
00:28:35,000 --> 00:28:40,000
We have this Truth and Reconciliation Commission, which is really interesting as a Buddhist,

278
00:28:40,000 --> 00:28:47,000
because it talks about people respecting each other and learning to live in harmony.

279
00:28:47,000 --> 00:28:52,000
Because the Canadian government really, not as much as the American government,

280
00:28:52,000 --> 00:29:01,000
I don't think, but they really crapped on the First Nations people.

281
00:29:01,000 --> 00:29:04,000
And continue to do so.

282
00:29:04,000 --> 00:29:10,000
It's getting maybe a little better, but not where it should be.

283
00:29:10,000 --> 00:29:18,000
I know.

284
00:29:18,000 --> 00:29:20,000
Thinking about these things and talking to people and appreciating people.

285
00:29:20,000 --> 00:29:21,000
I love at the university.

286
00:29:21,000 --> 00:29:24,000
There's so many good people doing good things.

287
00:29:24,000 --> 00:29:28,000
Most people are just people, but there are groups that are doing this good thing.

288
00:29:28,000 --> 00:29:34,000
So I always take time as I walk by to look at all the club tables.

289
00:29:34,000 --> 00:29:41,000
Because sometimes I'll sit there with the meditation, but otherwise, just walk by and talk to them.

290
00:29:41,000 --> 00:29:47,000
And if they're doing something good, like for AIDS or for the AIDS table was funny,

291
00:29:47,000 --> 00:29:49,000
because they're handing stuff out.

292
00:29:49,000 --> 00:29:54,000
And so they tried to hand me a package of candy and a condom.

293
00:29:54,000 --> 00:29:56,000
And I laughed at the condom.

294
00:29:56,000 --> 00:29:58,000
I said, no, thank you.

295
00:29:58,000 --> 00:30:02,000
And the person was kind of a little bit, you know, why are you laughing kind of thing?

296
00:30:02,000 --> 00:30:08,000
I guess I shouldn't laugh, but I'm so embarrassed.

297
00:30:08,000 --> 00:30:16,000
Not going to have much use for that.

298
00:30:16,000 --> 00:30:18,000
But yeah, but I talked to them.

299
00:30:18,000 --> 00:30:20,000
I said, good job.

300
00:30:20,000 --> 00:30:29,000
I was a group raising for homeless and getting a thumbs up and raising money.

301
00:30:29,000 --> 00:30:32,000
Most of them raising money, and they don't ever have money obviously.

302
00:30:32,000 --> 00:30:38,000
So I'll just give them a moral support.

303
00:30:38,000 --> 00:30:40,000
It's great to talk to people.

304
00:30:40,000 --> 00:30:42,000
I have good thoughts.

305
00:30:42,000 --> 00:30:46,000
The university is a place where a lot of good thoughts come about.

306
00:30:46,000 --> 00:30:54,000
And I say, there's a lot of problems at the university, but there is a segment that is a lot of goodness.

307
00:30:54,000 --> 00:30:57,000
So thinking about these things and being conscious, the living is good.

308
00:30:57,000 --> 00:31:00,000
I shouldn't confuse it with meditation or right thought.

309
00:31:00,000 --> 00:31:02,000
Right thought is much deeper.

310
00:31:02,000 --> 00:31:04,000
Right thought you need a pure mind.

311
00:31:04,000 --> 00:31:09,000
And that doesn't have any need to be talking to people or thinking about issues or something.

312
00:31:09,000 --> 00:31:12,000
Thinking about your possessions is not that.

313
00:31:12,000 --> 00:31:21,000
Right thought is in general and in totality in the end to be a type of thoughts that are free from green anger.

314
00:31:21,000 --> 00:31:24,000
And it was from whatever those thoughts made me.

315
00:31:24,000 --> 00:31:32,000
So when you think about something that would normally make you angry, you're not getting angry.

316
00:31:32,000 --> 00:31:36,000
So if you really have right thought, then you're pretty much...

317
00:31:36,000 --> 00:31:40,000
If you really have it, you're pretty much in your hand.

318
00:31:40,000 --> 00:31:53,000
But a soda panner attains it in the moment right before they realize the abandon or the moment of realizing the abandon.

319
00:31:53,000 --> 00:32:01,000
That's technically the noble aid to go back doesn't arise until the moment of enlightenment.

320
00:32:01,000 --> 00:32:04,000
That's a soda panner.

321
00:32:04,000 --> 00:32:12,000
So up until that point you're practicing what's called the pubangramanga, which is developing, cultivating the path factors.

322
00:32:12,000 --> 00:32:21,000
But they don't ever come to be noble until the moment of enlightenment.

323
00:32:21,000 --> 00:32:27,000
But you put them up there as the goal and then think what we're working towards that.

324
00:32:27,000 --> 00:32:32,000
So my mind is more free from green anger and delusion.

325
00:32:32,000 --> 00:32:38,000
So I'm getting closer than that kind of thing.

326
00:32:38,000 --> 00:32:41,000
I like the right...

327
00:32:41,000 --> 00:32:46,000
I always found that I could be mindful as long as I didn't start talking.

328
00:32:46,000 --> 00:32:51,000
But as soon as I started talking, I kind of lost my mindfulness.

329
00:32:51,000 --> 00:32:54,000
And then when I didn't really...

330
00:32:54,000 --> 00:32:57,000
I didn't really check out the April...

331
00:32:57,000 --> 00:33:02,000
But then on right speech it was like, oh, if you follow all these kind of rules,

332
00:33:02,000 --> 00:33:10,000
then you kind of got to remember them as you're talking and then you can sort of get to mindfulness.

333
00:33:10,000 --> 00:33:21,000
Again, it's still fairly conventional because right speech is probably no speech in the end.

334
00:33:21,000 --> 00:33:33,000
Because it has to have the other seven factors and you can't really have right mindfulness and so on when you're speaking.

335
00:33:33,000 --> 00:33:35,000
I mean, actually it is possible.

336
00:33:35,000 --> 00:33:39,000
It's possible you could become enlightened while you're speaking, but you'd have to be watching your lips.

337
00:33:39,000 --> 00:33:42,000
You have to be aware of that feeling.

338
00:33:42,000 --> 00:33:44,000
You have to be really present.

339
00:33:44,000 --> 00:33:47,000
Because speaking is actually it's a process.

340
00:33:47,000 --> 00:33:51,000
There's the thought and the thought leads to the speech and you can feel that.

341
00:33:51,000 --> 00:34:02,000
You have a thought and then suddenly your lips start moving and your voice box starts rattling and you can be mindful of all that as it's happening.

342
00:34:02,000 --> 00:34:10,000
Once had a late meditate to get very angry at me for this, you thought it was absurd that you could be mindful while you're talking.

343
00:34:10,000 --> 00:34:13,000
How would you grow about noting that?

344
00:34:13,000 --> 00:34:33,000
If you're feeling watching your lips and so on.

345
00:34:33,000 --> 00:34:39,000
That's those to have some discussion.

346
00:34:39,000 --> 00:34:50,000
Yeah, especially like well where I am, like pretty much any discussion I have is usually going to be on the internet.

347
00:34:50,000 --> 00:34:55,000
I don't really know tons of people who are practicing.

348
00:34:55,000 --> 00:35:02,000
Almost not actually.

349
00:35:02,000 --> 00:35:11,000
Well that's what we're here for.

350
00:35:11,000 --> 00:35:14,000
It's nice to be able to bring people together.

351
00:35:14,000 --> 00:35:15,000
Thank you very much.

352
00:35:15,000 --> 00:35:20,000
I got more than enough to go over here.

353
00:35:20,000 --> 00:35:39,000
I have a good night.

354
00:35:39,000 --> 00:35:58,000
Of course.

