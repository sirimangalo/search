sublime abiding. As I meditate, I first want to develop a context of equanimity and loving kindness
with respect to my thoughts and emotions as they arise. We live in a culture of attack or be
attacked. Can you do a short guided meditation on calm abiding please? Calm abiding or
meditations on altruistic emotions like friendliness can be very useful as companions to
with pastana meditation. I would not recommend to practice developing loving kindness or equanimity
alone because that can lead to avoiding the emotions they are meant to counter. When you are angry
and pretend that you love everyone it is really not a way of gaining since your friendliness
or appreciation of the suffering and other beings are going through. The best way to develop
friendliness, compassion, appreciation and equanimity is together with the pastana meditation.
I would caution against the view that you need to develop them before you can adequately develop
insight meditation. We should examine the emotions that are causing you to think that way.
Maybe you have states of great anger or hatred in your mind that you are trying to shy away from
or maybe they are states of attachment to the peace and the happiness wanting to feel love and
equanimity. In the end, you have to put aside such partiality and try to face your negative
and positive emotions with equal objectivity. When you feel angry, you have to learn about anger.
The best way to do away with it is to understand it. When you want to feel love or peace,
or when you do not like the ups and downs of the mind, where the mind is liking,
disliking, liking, disliking, and you wish you could be equanimous, you have to examine that
wanting for equanimity and the aversion towards the turbulence in the mind. During the practice
of a pastana meditation, you can switch to friendliness or compassion when overwhelming emotions arise
when you feel hatred or rage towards other beings. That is an appropriate time to develop
friendliness or compassion as a means of weakening the emotion, redirecting the mind towards
wholesome mind states. A simple exercise in friendliness is to first focus on yourself and make
an explicit wish that you may be happy and free from suffering. May I be happy. Next, make the same
wish towards others starting with those who are closest to you. It can be the people who are in
closest physical proximity to you or those who are closest to you emotionally. This is because
either group is the easiest to send thoughts of friendliness to. Say to yourself, may they be happy,
may they find peace, then gradually do this for beings who are further and further away from you,
either physically or emotionally. Physically, you can start with beings in the same building,
then those in the same city, country, and finally all beings in the entire world. You can also
specify humans first, then do the same things for animals, angels, ghosts, etc. Emotionally,
after those who are close to you, focus on those who you do not have either positive or negative
feelings towards. And finally, focus on those who you do have negative feelings towards,
or who have negative feelings towards you. If you are not able to send love to any group,
you can return to focus on the previous group until you are stronger. Compassion is practiced in
the same way as friendliness, except their friendliness is the wish for beings to be happy.
Compassion is wishing for them to be free from suffering. Compassion is useful for when you feel
sorry for someone or sad about the suffering of others. It is a way of calming your mind so that
you are not incapacitated by your sadness, so you can be a grounded support for those who are in
trouble. Appreciation, moodita, is appreciation for the attainment of other beings. This is often
cultivated in ordinary life using the expression sadhu, which means good. When we tell someone that
we are happy for them or offer congratulations, this is moodita. It is helpful in giving up jealousy
and resentment, helping let go of comparing oneself with others, wishing for the things they have
or not wanting them to get the things that we have. Appreciation is another important quality of
mind that brings peace of mind and helps us to practice meditation. As for equanimity, there are two
kinds, equanimity towards beings and equanimity towards experiences. When we pass in a meditation,
we practice cultivating equanimity towards experiences. In the context of the other three
qualities discussed here, the related practice is cultivating equanimity towards beings, which involves
an acknowledgement of karma. When you see being suffering or benefiting and you remind yourself,
they are going according to their karma. This is cultivating equanimity. It is a useful practice to
do away with anger or sadness in the face of perceived injustice related to politics or
economics. When you see the disparity between the very rich and very poor and you feel angry,
reminding yourself, all beings go according to their karma can help you keep a level head so you
are not consumed by negative emotions. When we see beings in great states of loss, suffering,
or in great states of power, influence and affluence, we can remind ourselves that if these people
in the state of great affluence continue to repress those who are impoverished and they themselves
will be doomed to attain the same state of poverty in the future. It is a cycle of cause and effect
wherein inevitably our destiny reflects our actions. When you think like this, you do away with
any sense of unfairness. You are able to understand that life is ultimately fair and there is no
need to get angry at those who oppress others as they are going to have to suffer the same fate
in the future. And the reason why oppressed beings suffer in the present is because they have done
it to others in the past. Reflecting in this way brings peace of mind and so it too is a support for
the past and a meditation. Practicing the past and a meditation is likewise the best support for
developing all four states of mind. In the past and a meditation, one becomes equanimous towards
all things as perceptions of beings become secondary to the primary perspective of reality as
moments of experience arising and seizing. As a result, you understand the experiences of other
beings. When they are suffering, you can appreciate what they are going through. Even when they are
angry or upset or enraged in immoral acts, you can understand them and you can feel pity for them
that they are stuck in a cycle of karma, that they cannot break free from. And rather than blame
the individual, you understand the cause to be the development of habitual tendencies through
ignorance, through simply not knowing what they are doing. When beings are suffering, you can
empathize with them. When beings are prosperous, you do not feel jealous. You appreciate the nature
of good qualities because you know that they lead to happiness. You are happy and content yourself,
so you do not feel the need to compare yourself to others and ultimately see other beings with
equanimity, not preferring to be with this person or that person. You accept reality for what it is
and you realize that partiality is not of any use or benefit as it does not lead to real happiness.
If you want to develop a meditation based on loving kindness or equanimity, you should formulate
as explained and cultivate the associated intonations according to your own situation. As mentioned,
it can be based on those who are in physical or emotional proximity, but it is something that
should come naturally and something that you should develop in tandem with the Bipassana meditation.
I would not really recommend spending too much of your time in terms of developing intensive
meditation in this area, simply because it is time consuming and you would be much better served
by training in Bipassana meditation and when an emotion becomes overwhelming, use these practices
as a support for your main meditation practice.
