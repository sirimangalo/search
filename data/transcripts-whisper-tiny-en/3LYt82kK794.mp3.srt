1
00:00:00,000 --> 00:00:06,000
Good evening.

2
00:00:06,000 --> 00:00:17,320
Tonight we continue on with our study of the Dhamapada, verse 203, which reads as follows.

3
00:00:17,320 --> 00:00:45,320
Hanga is the greatest sickness.

4
00:00:45,320 --> 00:00:55,320
The formations are the ultimate suffering.

5
00:00:55,320 --> 00:01:14,320
For one who sees that as it is, it is the ultimate happiness.

6
00:01:14,320 --> 00:01:25,320
This was told in regards to a story that is fairly well known.

7
00:01:25,320 --> 00:01:28,320
It is a particular story.

8
00:01:28,320 --> 00:01:34,320
It is often used to extrapolate it.

9
00:01:34,320 --> 00:01:42,320
It is used in a way that justifies certain practices or provides guidelines.

10
00:01:42,320 --> 00:01:57,320
Because the story is about a man, a farmer who had oxen and he heard that Buddha was teaching

11
00:01:57,320 --> 00:02:02,320
and he wanted to go see the Buddha, listen to the Buddha's teaching.

12
00:02:02,320 --> 00:02:09,320
But he woke up in the early morning and realized his oxen and went away.

13
00:02:09,320 --> 00:02:15,320
So he debated what to do.

14
00:02:15,320 --> 00:02:26,320
He decided he had to go after his oxen and spent all morning looking for them, chasing after

15
00:02:26,320 --> 00:02:33,320
them and bringing them back home.

16
00:02:33,320 --> 00:02:37,320
He was quite hungry by that time but he thought to himself, well maybe I can catch the

17
00:02:37,320 --> 00:02:41,320
end of the Buddha's teaching if I go now.

18
00:02:41,320 --> 00:02:49,320
So Buddha's oxen in the corral and went off to find the Buddha, went off to the monastery

19
00:02:49,320 --> 00:02:57,320
to listen to the Buddha.

20
00:02:57,320 --> 00:03:03,320
Now the Buddha had, as it turns out, come specifically to teach this man.

21
00:03:03,320 --> 00:03:13,320
He had this capacity to connect with a man's psyche and understand that this man was

22
00:03:13,320 --> 00:03:16,320
ready to understand the teaching.

23
00:03:16,320 --> 00:03:26,320
So when he came to the place, the monastery and everyone had come together to listen to his

24
00:03:26,320 --> 00:03:33,320
teachings, he sat there and held his peace.

25
00:03:33,320 --> 00:03:36,320
I came all this way and just said to himself, I came all this way and just teach this man

26
00:03:36,320 --> 00:03:40,320
and I'm not going to teach until he gets here.

27
00:03:40,320 --> 00:03:44,320
And because he sat still, all the monks sat still and sat quietly and because all the

28
00:03:44,320 --> 00:03:49,320
monks had quietly, all the laypeople sat quietly and no one dared to say anything.

29
00:03:49,320 --> 00:03:55,320
So rather than listening to a talk which they expected, they all sat quietly and meditated.

30
00:03:55,320 --> 00:04:01,320
I assume it actually doesn't say quite in that detail.

31
00:04:01,320 --> 00:04:08,320
I guess it would have depended on the individuals but it can imagine all the monks were quite

32
00:04:08,320 --> 00:04:13,320
quiet and orderly listening, waiting for the Buddha to speak.

33
00:04:13,320 --> 00:04:28,320
And so when the man arrived, when he came, he sat down to one side and prepared to listen

34
00:04:28,320 --> 00:04:35,320
and not really sure what was going on but he found that he couldn't actually pay attention

35
00:04:35,320 --> 00:04:37,320
or focus on the Buddha.

36
00:04:37,320 --> 00:04:46,320
He couldn't actually sit still, his mind was distracted by hunger.

37
00:04:46,320 --> 00:04:55,320
And so they were all sitting quietly waiting for the Buddha to speak and this man couldn't

38
00:04:55,320 --> 00:04:56,320
listen.

39
00:04:56,320 --> 00:05:00,320
He couldn't even sit still.

40
00:05:00,320 --> 00:05:12,320
He was fainting perhaps with hunger or restless because his bodily needs.

41
00:05:12,320 --> 00:05:17,320
And the Buddha actually spoke up at that point and he said to the monks,

42
00:05:17,320 --> 00:05:21,320
is there any food left over from Neil?

43
00:05:21,320 --> 00:05:26,320
And the monk said to us, and he told the monks, he said, directed the monks,

44
00:05:26,320 --> 00:05:31,320
could you feed that man off there in the back?

45
00:05:31,320 --> 00:05:40,320
In a sense, it appears to be simply the Buddha cutting through any nonsense,

46
00:05:40,320 --> 00:05:44,320
any sort of bureaucracy or formality.

47
00:05:44,320 --> 00:05:47,320
I'm going to teach this guy.

48
00:05:47,320 --> 00:05:49,320
Oh, now he's here.

49
00:05:49,320 --> 00:05:51,320
Oh, but he's hungry.

50
00:05:51,320 --> 00:05:58,320
Okay, feed that man over there in the current because that was his whole intention of coming.

51
00:05:58,320 --> 00:06:04,320
And so the monks and I guess the late people would have arranged to feed him.

52
00:06:04,320 --> 00:06:09,320
But anyway, food was given to the man and once he was given food then the Buddha began to talk

53
00:06:09,320 --> 00:06:14,320
and give a speech to all the people.

54
00:06:14,320 --> 00:06:22,320
Which this man was able to understand and was able to realize the dhamma for himself and listening to the teaching,

55
00:06:22,320 --> 00:06:26,320
along with other people perhaps.

56
00:06:26,320 --> 00:06:29,320
But the monks were talking about it afterwards and they thought,

57
00:06:29,320 --> 00:06:30,320
wasn't that remarkable?

58
00:06:30,320 --> 00:06:32,320
This has never happened before.

59
00:06:32,320 --> 00:06:40,320
Just out of the blue, the Buddha decides that this man should be given some food.

60
00:06:40,320 --> 00:06:45,320
And the Buddha came in, of course, and heard what they were saying and asked them what they were saying,

61
00:06:45,320 --> 00:06:47,320
and they told them.

62
00:06:47,320 --> 00:06:54,320
And monks, it's really a big deal, he said.

63
00:06:54,320 --> 00:07:00,320
Hunger is the greatest sickness, and he taught this first.

64
00:07:00,320 --> 00:07:10,320
So the verse is used sort of generally to, or the story, is used generally to support the idea of,

65
00:07:10,320 --> 00:07:19,320
well, first of all, there are being conditions required for understanding and appreciating the dhamma that are mundane.

66
00:07:19,320 --> 00:07:28,320
And so I mentioned this story briefly in a talk I gave recently about things that might supersede the need to practice.

67
00:07:28,320 --> 00:07:30,320
And here's an example, food.

68
00:07:30,320 --> 00:07:38,320
If you're very hungry, it's practically speaking hard to understand and appreciate and take up the Buddha's teaching,

69
00:07:38,320 --> 00:07:43,320
the same if you're falling asleep, for example.

70
00:07:43,320 --> 00:07:56,320
But it's used even more broadly to talk about societal requirements, for example, the need for just economic policies.

71
00:07:56,320 --> 00:08:07,320
There are lots and lots of poor people, it seems to be going against the Buddha's teaching here if you extrapolate it to that extent.

72
00:08:07,320 --> 00:08:17,320
Because we're as Buddhists, we're promoting the dhamma, but in order to promote the dhamma, we have to ensure that people are able to appreciate it.

73
00:08:17,320 --> 00:08:26,320
And so an argument could be made to extrapolate this verse to talk about injustice and equality.

74
00:08:26,320 --> 00:08:37,320
I mean, equality not of outcomes perhaps, but equality of quality of opportunity.

75
00:08:37,320 --> 00:08:50,320
So that we allow people to have the potential to develop spiritually, we give them the resources necessary to make open the opportunity for them.

76
00:08:50,320 --> 00:08:53,320
So that's one of the reasons why it's quite popular.

77
00:08:53,320 --> 00:08:56,320
It doesn't really have anything to do with our practice.

78
00:08:56,320 --> 00:09:00,320
So the question, as always, is what does this verse mean to us?

79
00:09:00,320 --> 00:09:09,320
How does this relate to our practice and what's the lesson we can take from it?

80
00:09:09,320 --> 00:09:14,320
So I think there's two parts and it really relates mainly to the verse.

81
00:09:14,320 --> 00:09:19,320
The first part is the idea of hunger being the greatest sickness.

82
00:09:19,320 --> 00:09:21,320
And it can be taken in two ways.

83
00:09:21,320 --> 00:09:29,320
I'm not entirely sure that the Buddha was simply referring to physical hunger.

84
00:09:29,320 --> 00:09:36,320
But it's not entirely clear that he was hinting at something greater, because of course there's two kinds of hunger.

85
00:09:36,320 --> 00:09:45,320
You can think of dhamma, which means thirst, but it's referring to greed, desire, addiction.

86
00:09:45,320 --> 00:09:49,320
That sort of thing. And that's a kind of hunger.

87
00:09:49,320 --> 00:09:56,320
But simply referring to physical hunger, it's still a very important point.

88
00:09:56,320 --> 00:10:07,320
When we talk about suffering, we talk about the nature of our existence.

89
00:10:07,320 --> 00:10:15,320
Suffering goes far beyond simple physical pain, for example.

90
00:10:15,320 --> 00:10:19,320
It goes beyond things that we can avoid.

91
00:10:19,320 --> 00:10:23,320
Our ordinary understanding of suffering is generally done.

92
00:10:23,320 --> 00:10:35,320
It's something that you experience once in a while, and you can, and should, find ways to avoid it, ways to fix it.

93
00:10:35,320 --> 00:10:37,320
If it comes up.

94
00:10:37,320 --> 00:10:40,320
But there are things that get in the way.

95
00:10:40,320 --> 00:10:43,320
Of course sickness is something that gets in a way of doing that.

96
00:10:43,320 --> 00:10:49,320
It's always spend a lot of time and energy and money on trying to avoid sickness.

97
00:10:49,320 --> 00:10:58,320
And some of them are easy to avoid, easy to eradicate, easy to fix, and there's a knot.

98
00:10:58,320 --> 00:11:02,320
There are sicknesses, of course, like heart disease and diabetes.

99
00:11:02,320 --> 00:11:06,320
Diabetes generally often being manageable at best.

100
00:11:06,320 --> 00:11:14,320
And of course cancer, which is quite often even though you might go to great extremes to destroy the cancer inside.

101
00:11:14,320 --> 00:11:18,320
And the end use of come to it and die.

102
00:11:18,320 --> 00:11:23,320
But hunger, just physically speaking, hunger is really at the top of the list.

103
00:11:23,320 --> 00:11:27,320
Because ultimately, it's a sickness that we can ever avoid.

104
00:11:27,320 --> 00:11:33,320
If you look at it in a certain light, it's the one thing that we always have to take medicine for.

105
00:11:33,320 --> 00:11:42,320
Of course, we've adapted it to being something pleasurable, where we look at eating as something that actually is a positive thing.

106
00:11:42,320 --> 00:11:49,320
Not having to eat would be quite unpleasant for most people.

107
00:11:49,320 --> 00:12:04,320
But nonetheless, it points out that the very fabric of our existence of human beings as a dependency is one that is susceptible to great suffering.

108
00:12:04,320 --> 00:12:12,320
And simply being without food, which is, of course, something that we've become desensitized to.

109
00:12:12,320 --> 00:12:18,320
But if there were a being who didn't have to eat and saw that we all had to eat, it would seem horrific.

110
00:12:18,320 --> 00:12:28,320
Wow, you mean three times a day you have to take medicine from the moment you're born every single one of you, and you haven't found a cure for this, yeah?

111
00:12:28,320 --> 00:12:38,320
How horrific. And if you don't eat even one day, if you don't, if you can go one day, you start feeling pain, you start feeling weak.

112
00:12:38,320 --> 00:12:43,320
Your body starts to waste away, your whole being starts to crumble.

113
00:12:43,320 --> 00:12:48,320
And you could die if you go long enough without food or water.

114
00:12:48,320 --> 00:12:51,320
Horrific.

115
00:12:51,320 --> 00:13:00,320
So, physically speaking, it puts us a little bit into perspective in the broad and the greater scheme of things.

116
00:13:00,320 --> 00:13:10,320
What we are as human beings, our existence is something very contrived, very specific and very much.

117
00:13:10,320 --> 00:13:25,320
We're in slaves or prisoners to our whole humanity. We don't have free rain over our experience.

118
00:13:25,320 --> 00:13:32,320
We are dependent on things like food and water, air, even temperature, and so on.

119
00:13:32,320 --> 00:13:38,320
We couldn't just walk out into the snow. We need to be aware of heat and cold.

120
00:13:38,320 --> 00:13:43,320
And then we have to be aware of what kind of food we need and so on and so on.

121
00:13:43,320 --> 00:13:51,320
So, this helps to sort of, this is a basic teaching, but it's an important basic teaching to reframe things.

122
00:13:51,320 --> 00:14:07,320
We're instead of looking at all the good things that we can chase after, starting to see that we have a great potential for suffering or human beings.

123
00:14:07,320 --> 00:14:23,320
It helps to open us up to somehow freeing ourselves from this, realizing that we are vulnerable and trying to seek out a way to be involved in them, so that the potential for suffering isn't there.

124
00:14:23,320 --> 00:14:32,320
Because, of course, that's reality. There will be people who go hungry. There will be people who get sick. There will be death. There will be old age.

125
00:14:32,320 --> 00:14:44,320
This is a part of life. It's a negative side, something that we could potentially find a way to free ourselves from.

126
00:14:44,320 --> 00:14:53,320
Of course, it gets much deeper when you look at mental thirst and mental hunger, because then you really get to the solution.

127
00:14:53,320 --> 00:15:03,320
The solution is to change the way we look at things rather than seeking them out, rather than craving and clinging for good food and so on.

128
00:15:03,320 --> 00:15:10,320
We learn to let go for great, rather than craving for the pleasant feeling of having eaten.

129
00:15:10,320 --> 00:15:29,320
We're okay with being hungry or sick or so on, because that's really the only way. Giving up our partiality on thirst. I want this. I want that. Giving up our hunger. It's the only way to really be free from the potential for suffering.

130
00:15:29,320 --> 00:15:47,320
But the deeper teaching, I still say that's on one side. If we talk about mental hunger, then yes, we start to get into the deeper sort of philosophy of meditation, why we're trying to be more objective, because we're trying to change this.

131
00:15:47,320 --> 00:16:06,320
The other part is the rest of the verses quite is on the deepest level, let's say. It's on a level that's very hard for people to appreciate. It's hard. It makes Buddhism hard to teach at the deepest level, which often newcomers are looking for.

132
00:16:06,320 --> 00:16:12,320
It's very hard to appreciate and often unpalatable.

133
00:16:12,320 --> 00:16:23,320
It's hard to swallow. Because you're talking about the ultimate, and that's what's important to understand. You're talking about the ultimate level of understanding.

134
00:16:23,320 --> 00:16:44,320
And by that, I mean, something that goes beyond this, I need food, so I have to seek out food and pleasure and construct an artificial nature of being what we call human.

135
00:16:44,320 --> 00:17:07,320
The things that we think of as being true and right, I have to go to school and get a job and work and make money, even just more simply get food to eat and live and find pleasure and get married and have sex and have children and so on and so on.

136
00:17:07,320 --> 00:17:21,320
It goes beyond the human existence to ask, what is the nature of reality? What does it mean to be? What does it mean to exist? What is the truth of the universe?

137
00:17:21,320 --> 00:17:28,320
It means much more fundamentally than anything that being human could possibly mean.

138
00:17:28,320 --> 00:17:39,320
So it's two parts and they're directly related. The first part, the idea that Sankara is Sankara are the ultimate suffering.

139
00:17:39,320 --> 00:17:59,320
And the Buddha says, once you've understood this as it is, you're taught Buddha. And that's important because we're talking about something that doesn't make any sense until you understand it for yourself.

140
00:17:59,320 --> 00:18:11,320
That best, it's a claim that we can make. It's a very bold claim and I think that's useful because we don't have to present it as a belief, as a theory, as a dogma.

141
00:18:11,320 --> 00:18:37,320
But we can boldly claim that what it literally says is that all arisen things, the Sankara is anything that arises, are dukkah suffering.

142
00:18:37,320 --> 00:18:48,320
And that part of it by saying we don't really mean suffering. And you can say it just means that it's not satisfying or it can cause you suffering if you cling to it and so on.

143
00:18:48,320 --> 00:18:59,320
And that's not technically wrong, but it's even a little bit deeper than that. What it's saying is that

144
00:18:59,320 --> 00:19:10,320
you gain the perspective. So it's not even a theory or an idea, it's just a perspective, the way you look at experience.

145
00:19:10,320 --> 00:19:22,320
It's not having the theory I believe all Sankara are dukkah. It's how you look at what arises in front of it because our ordinary way of looking at things is, oh yes, that will make me happy.

146
00:19:22,320 --> 00:19:42,320
Yes, good, bad. Seeing them all as dukkah means seeing them without any desire, any attachment, seeing them as not something to cling to, not something to seek out.

147
00:19:42,320 --> 00:19:55,320
Having a sense of dismissal at best. I wouldn't say you get disgusted. You can be quite disgusted in meditation by things often food becomes quite disgusting.

148
00:19:55,320 --> 00:20:05,320
It's quite remarkable, I think one of the most remarkable things that meditative will come across is the experience of not wanting to eat.

149
00:20:05,320 --> 00:20:12,320
Whereas, normally, very obsessive about our food and thinking, oh, tomorrow morning I'll have this food in that food.

150
00:20:12,320 --> 00:20:17,320
But there comes a plant where you're just like, alright, now I've got to go eat again.

151
00:20:17,320 --> 00:20:23,320
And you realize, wow, that's quite a change. And this is a sign you're starting to change.

152
00:20:23,320 --> 00:20:31,320
And we make the claim and an important claim that this has nothing to do with dogma or indoctrination.

153
00:20:31,320 --> 00:20:49,320
We try to keep our teaching and our dogma at a minimum because what we want and what we claim to be giving you, we say, an understanding for yourself of things just as they are.

154
00:20:49,320 --> 00:20:55,320
And our claim is that when you look objectively, this is what you'll see, this is what will happen.

155
00:20:55,320 --> 00:21:04,320
You'll start to see the passion, the desire is all quite uncomfortable and unpleasant, unnecessary, unhelpful and beneficial.

156
00:21:04,320 --> 00:21:08,320
And you'll start to let go and your perspective will change.

157
00:21:08,320 --> 00:21:12,320
Rather than looking at things, oh, that's interesting, is that going to make me happy?

158
00:21:12,320 --> 00:21:18,320
You say, oh, no, I've seen, I've been there, I've seen that that doesn't make me happy.

159
00:21:18,320 --> 00:21:24,320
And you'll start to become disenchanted and be done.

160
00:21:24,320 --> 00:21:31,320
And as a result of that, you'll see things as Duca.

161
00:21:31,320 --> 00:21:36,320
I say, oh, no, no, no, no.

162
00:21:36,320 --> 00:21:48,320
Until you get to the point and it's kind of, in an ultimate sense, this idea that all some cards are Duca.

163
00:21:48,320 --> 00:21:55,320
Some cards that are the ultimate suffering is just a moment of realization.

164
00:21:55,320 --> 00:21:57,320
And it's so hard to explain this.

165
00:21:57,320 --> 00:22:09,320
And it must be, for some people hearing this, this must be quite repulsive and uncomfortable to hear.

166
00:22:09,320 --> 00:22:17,320
Certainly, something that people will not be happy to not agree with, let's say.

167
00:22:17,320 --> 00:22:23,320
But it becomes a moment.

168
00:22:23,320 --> 00:22:28,320
And this idea that it's the ultimate suffering is a moment of experience,

169
00:22:28,320 --> 00:22:36,320
where you have an attitude, a perception, a perspective that inclines away,

170
00:22:36,320 --> 00:22:50,320
that turns away from some sorrow, from even experience.

171
00:22:50,320 --> 00:22:56,320
It's hard to find an analogy, because really anything, if we were to say,

172
00:22:56,320 --> 00:23:03,320
oh, instead of often you hear about people talking about just, it's like a child

173
00:23:03,320 --> 00:23:06,320
who, like, certain toys or something.

174
00:23:06,320 --> 00:23:10,320
And if you gave them gold, they wouldn't like the gold, but an adult would know what gold is.

175
00:23:10,320 --> 00:23:16,320
But that doesn't quite do it justice, because, you know, the hold is still wanting something.

176
00:23:16,320 --> 00:23:27,320
At best, there's one in the Risudimaga that talks about a bird hopping through the trees and looking for fruit.

177
00:23:27,320 --> 00:23:33,320
It's from branch to branch looking for fruit until it finally realizes the fruit gets it.

178
00:23:33,320 --> 00:23:37,320
It's like, oh, wait a minute, this tree has no fruit.

179
00:23:37,320 --> 00:23:39,320
So it's presented as that.

180
00:23:39,320 --> 00:23:43,320
The letting go is when the bird flies off and the bird flies off, because,

181
00:23:43,320 --> 00:23:48,320
realizes there's no fruit, but that also doesn't quite get to the point,

182
00:23:48,320 --> 00:23:53,320
because it still gives you the sense of what this bird is looking for something

183
00:23:53,320 --> 00:23:55,320
and that it didn't find here.

184
00:23:55,320 --> 00:23:57,320
It goes deeper than that.

185
00:23:57,320 --> 00:24:04,320
Seeing Sankara as Dukka is not even in the sense of, oh, there must be Sukka somewhere else.

186
00:24:04,320 --> 00:24:07,320
It must be happiness somewhere else.

187
00:24:07,320 --> 00:24:12,320
It's a changing of perspective in the sense of,

188
00:24:12,320 --> 00:24:19,320
I often talk about this idea of a bird and maybe a bird in a nest would be a good analogy.

189
00:24:19,320 --> 00:24:25,320
There's this horrible fact of life that I understand that mother birds will kick their,

190
00:24:25,320 --> 00:24:29,320
the baby birds out of the nest when they get old enough to fly.

191
00:24:29,320 --> 00:24:34,320
So it's like us being kicked out of the nest.

192
00:24:34,320 --> 00:24:39,320
The bird being kicked out of the nest is at that moment it has a great horror

193
00:24:39,320 --> 00:24:53,320
and that feeling of the potential for suffering is how we exist as human beings.

194
00:24:53,320 --> 00:24:55,320
We're very happy in the nest.

195
00:24:55,320 --> 00:24:57,320
Happy with our mother birds feeding us.

196
00:24:57,320 --> 00:25:01,320
I'm happy with all the things that we can find, all the joy that we can find.

197
00:25:01,320 --> 00:25:05,320
And anytime we get kicked out of the nest, we're horrified,

198
00:25:05,320 --> 00:25:13,320
because we're losing our firm foundation and potentially falling into great suffering.

199
00:25:13,320 --> 00:25:15,320
We're afraid of the suffering of falling.

200
00:25:15,320 --> 00:25:21,320
Anyone that would happen to it would be great fear.

201
00:25:21,320 --> 00:25:25,320
But the truth of reality that we don't understand as human beings

202
00:25:25,320 --> 00:25:30,320
and that the bird, baby bird, doesn't understand is that we have wings.

203
00:25:30,320 --> 00:25:33,320
Is that reality isn't like that.

204
00:25:33,320 --> 00:25:38,320
Reality and the greatness of Buddhism and why it's not a pessimistic religion

205
00:25:38,320 --> 00:25:41,320
and meditation is not at all pessimistic.

206
00:25:41,320 --> 00:25:44,320
Is that happiness is something very different.

207
00:25:44,320 --> 00:25:49,320
Reality is something very different.

208
00:25:49,320 --> 00:25:55,320
This whole concept that somehow happiness has anything to do with things that you get,

209
00:25:55,320 --> 00:25:58,320
pleasure that you receive is just wrong.

210
00:25:58,320 --> 00:26:03,320
It's based on delusion, it's based on misunderstanding, it's inferior.

211
00:26:03,320 --> 00:26:08,320
And when the baby bird is kicked out of the nest and then it starts,

212
00:26:08,320 --> 00:26:12,320
it's just, I guess, by instinct, it spreads its wings.

213
00:26:12,320 --> 00:26:13,320
And it's flying.

214
00:26:13,320 --> 00:26:17,320
It's like, oh, well, that wasn't so bad after all.

215
00:26:17,320 --> 00:26:19,320
In fact, it's much better.

216
00:26:19,320 --> 00:26:22,320
It's a necessary change of perspective.

217
00:26:22,320 --> 00:26:29,320
And suddenly this is no longer a feed me, feed me a big mouse in a bird's nest.

218
00:26:29,320 --> 00:26:31,320
Suddenly it's a bird flying through there.

219
00:26:31,320 --> 00:26:34,320
I think that's a fairly good analogy.

220
00:26:34,320 --> 00:26:39,320
This change of perspective.

221
00:26:39,320 --> 00:26:46,320
The second part, it related is I think perhaps the most hard to swallow part.

222
00:26:46,320 --> 00:26:50,320
The most unpalatable part of Buddhism, hardest to explain.

223
00:26:50,320 --> 00:26:54,320
You could explain that Nibana, Nibana and Paramungsu come.

224
00:26:54,320 --> 00:26:58,320
For someone who has seen that all sun currents are suffering,

225
00:26:58,320 --> 00:27:02,320
Nibana is the ultimate happiness.

226
00:27:02,320 --> 00:27:04,320
So they look at happiness in a different way.

227
00:27:04,320 --> 00:27:09,320
It should be easy to understand, easier to understand now that I've explained that size.

228
00:27:09,320 --> 00:27:14,320
When you get the context, when easy to understand in a sense, you understand it's quite specific.

229
00:27:14,320 --> 00:27:19,320
It's not something that relates at all to any kind of happiness in the world.

230
00:27:19,320 --> 00:27:22,320
So if you say, well, how could Nibana be happiness?

231
00:27:22,320 --> 00:27:26,320
I mean, maybe you think that if you don't know what Nibana is, you say,

232
00:27:26,320 --> 00:27:27,320
oh, that's okay.

233
00:27:27,320 --> 00:27:28,320
Nibana, that's kind of cool.

234
00:27:28,320 --> 00:27:30,320
Maybe it's this place that I go to.

235
00:27:30,320 --> 00:27:36,320
There's crystal trees and music and Buddha is listening now.

236
00:27:36,320 --> 00:27:39,320
Nibana is accessation.

237
00:27:39,320 --> 00:27:42,320
It's an experience of cessation.

238
00:27:42,320 --> 00:27:46,320
So when people hear about that, it's quite frightening.

239
00:27:46,320 --> 00:27:50,320
And it's because it's out of context.

240
00:27:50,320 --> 00:27:53,320
We compare it to any kind of happiness in the world.

241
00:27:53,320 --> 00:27:55,320
And it doesn't work out.

242
00:27:55,320 --> 00:27:59,320
I mean, it's so unpalatable for that reason.

243
00:27:59,320 --> 00:28:01,320
So I'm saying, I'm going to kick you out of the nest.

244
00:28:01,320 --> 00:28:03,320
And there'll be much happier.

245
00:28:03,320 --> 00:28:05,320
The truth is, you would be.

246
00:28:05,320 --> 00:28:08,320
The bird will be as well.

247
00:28:08,320 --> 00:28:13,320
I think the mother bird would be happier as well, right?

248
00:28:13,320 --> 00:28:16,320
But I think baby bird doesn't know that.

249
00:28:16,320 --> 00:28:22,320
So it freaks out.

250
00:28:22,320 --> 00:28:26,320
Because it's understanding of happiness is very specific.

251
00:28:26,320 --> 00:28:36,320
Very dependent on experiences and partiality to certain experiences.

252
00:28:36,320 --> 00:28:40,320
So Nibana is the greatest happiness because it's freedom from that.

253
00:28:40,320 --> 00:28:51,320
It's freedom from all of the incessant getting and liking and needing and wanting and chasing

254
00:28:51,320 --> 00:29:00,320
and getting again or not getting again and being disappointed.

255
00:29:00,320 --> 00:29:05,320
Nibana is the highest happiness specifically because it's non-arising.

256
00:29:05,320 --> 00:29:08,320
So it was even asked, someone asked,

257
00:29:08,320 --> 00:29:12,320
well, how can Nibana be happiness if there's no way to know, no feeling in there?

258
00:29:12,320 --> 00:29:19,320
Because of course, happy feelings or what you think of when you think of happiness, pleasurable feelings.

259
00:29:19,320 --> 00:29:21,320
And sorry, put to answer.

260
00:29:21,320 --> 00:29:27,320
This is actually in the exams when it has monks, you have to know this answer.

261
00:29:27,320 --> 00:29:32,320
So everybody said it's precisely because there is no way to know there is no feeling.

262
00:29:32,320 --> 00:29:35,320
That Nibana is the highest happiness.

263
00:29:35,320 --> 00:29:38,320
It's very hard to understand and I think unpalable.

264
00:29:38,320 --> 00:29:40,320
I want to appreciate that.

265
00:29:40,320 --> 00:29:50,320
But this is an important teaching to help us understand how that has to be contextualized.

266
00:29:50,320 --> 00:29:53,320
And I'm not trying to trivialize it or marginalize it or say,

267
00:29:53,320 --> 00:29:55,320
oh, don't worry about it.

268
00:29:55,320 --> 00:29:59,320
I'm just saying, it's not something we can understand and that's the whole point.

269
00:29:59,320 --> 00:30:04,320
Why it's an ultimate happiness and why some cars or the highest suffering is because

270
00:30:04,320 --> 00:30:08,320
our understanding, our perspective is so very different.

271
00:30:08,320 --> 00:30:11,320
Our perspective on suffering is wrong.

272
00:30:11,320 --> 00:30:14,320
Our perspective on happiness is wrong.

273
00:30:14,320 --> 00:30:19,320
And those are bold claims, but I present them as claims and Buddhism presents them as claims

274
00:30:19,320 --> 00:30:21,320
that can be investigated.

275
00:30:21,320 --> 00:30:26,320
And so I absolutely, if that's not what you find when you look objectively at reality,

276
00:30:26,320 --> 00:30:28,320
then maybe we're wrong.

277
00:30:28,320 --> 00:30:30,320
There's no fear of being wrong.

278
00:30:30,320 --> 00:30:32,320
We're not afraid that people investigate.

279
00:30:32,320 --> 00:30:37,320
The real tragedy is when people don't investigate.

280
00:30:37,320 --> 00:30:42,320
And instead, they rely upon belief and they're constructed carefully.

281
00:30:42,320 --> 00:30:48,320
Lifetime after lifetime constructed culture, as I said,

282
00:30:48,320 --> 00:30:56,320
getting to school, getting a job, getting money and marriage and children.

283
00:30:56,320 --> 00:31:03,320
Pleasure from food and drink and music and all sorts of things and thinking,

284
00:31:03,320 --> 00:31:05,320
yes, this is reality.

285
00:31:05,320 --> 00:31:10,320
This is what makes sense and wondering why it is that the world is falling apart

286
00:31:10,320 --> 00:31:15,320
and why it is that we're destroying the environment around us and the air is becoming polluted

287
00:31:15,320 --> 00:31:18,320
and the water is becoming polluted.

288
00:31:18,320 --> 00:31:23,320
And society is becoming polluted and so on and so on.

289
00:31:23,320 --> 00:31:26,320
And the reality is that it's just wrong.

290
00:31:26,320 --> 00:31:27,320
We've got it all wrong.

291
00:31:27,320 --> 00:31:32,320
And the only good, the only real happiness that comes is when people change

292
00:31:32,320 --> 00:31:40,320
and gain a better perspective and realize that greed and craving and hunger and thirst are not sustainable.

293
00:31:40,320 --> 00:31:43,320
They don't mean to happiness.

294
00:31:43,320 --> 00:31:48,320
So that in fact, getting things is not the way to find happiness.

295
00:31:48,320 --> 00:31:56,320
And if they go deeper, they'll see that the real way of finding happiness is letting go.

296
00:31:56,320 --> 00:31:58,320
You hear that, not just in Buddhism.

297
00:31:58,320 --> 00:32:01,320
Just in Buddhism, we take it seriously.

298
00:32:01,320 --> 00:32:06,320
And we go all the way and say, don't hold onto anything.

299
00:32:06,320 --> 00:32:10,320
It might go completely because that's why you've got wings.

300
00:32:10,320 --> 00:32:13,320
That's the analogy.

301
00:32:13,320 --> 00:32:17,320
So that's the number part of her tonight, very good verse.

302
00:32:17,320 --> 00:32:20,320
Thank you all for listening.

