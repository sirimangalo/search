1
00:00:00,000 --> 00:00:04,000
At the top there, when I practice meditation and focus on the breath,

2
00:00:04,000 --> 00:00:08,700
I feel like I'm controlling it too much instead of letting go and seeing it clearly.

3
00:00:08,700 --> 00:00:13,300
How do I deal with this? This is a big problem in my progress.

4
00:00:17,300 --> 00:00:20,300
Anybody want to start?

5
00:00:20,300 --> 00:00:37,300
So I take it when you meditate and let's say if you're focusing on the rising and falling of the stomach wall,

6
00:00:37,300 --> 00:00:47,300
are you saying that you're actually feeling like that you're controlling the rising falling rather than actually noticing while noting the rising and falling?

7
00:00:47,300 --> 00:00:58,300
Because I think for my own experience, that's the kind of thing that I've experienced initially when you first start to sit and meditate,

8
00:00:58,300 --> 00:01:03,300
because obviously you're focusing on the rising and falling and then now as you get into the meditation,

9
00:01:03,300 --> 00:01:15,300
you start to know other things like thinking and pain and hearing and all the other aggregates that arise during the meditation.

10
00:01:15,300 --> 00:01:26,300
So I don't know if that's what's happening or it's a different kind of meditation we're focusing on the breath passing through the nose.

11
00:01:26,300 --> 00:01:36,300
But I think to start with, you have to focus on it initially, which can sometimes perceive that you're controlling it.

12
00:01:36,300 --> 00:01:52,300
Yeah, one of the big aspects, whether it can key to any question is asking what is the reason for asking it?

13
00:01:52,300 --> 00:02:00,300
Looking at what is the reason for asking it? The key here is in where you say, I feel like I'm controlling it too much.

14
00:02:00,300 --> 00:02:15,300
Because we can say that there's something wrong with control in and of itself, but the real problem here is the fact that there's something unpleasant going on and that's suffering, which is really an important thing because no one wants to suffer.

15
00:02:15,300 --> 00:02:34,300
So the key here problem is not necessarily to look at the control, that's what comes from misunderstanding the suffering, from misunderstanding reality. Based on misunderstanding reality, we try to control it, which actually creates more suffering.

16
00:02:34,300 --> 00:02:41,300
When you look at the suffering, and look at the experience as it is.

17
00:02:41,300 --> 00:02:47,300
And look at your disliking of it, looking at, based on Petitya Simupada, right?

18
00:02:47,300 --> 00:03:01,300
Looking at the various pieces as they arise, looking at the feelings, looking at the object, looking at the liking or disliking of it, looking at your intentions, based on the liking and disliking of it.

19
00:03:01,300 --> 00:03:14,300
You'll come to see that it's not worth controlling. You'll come to teach yourself that there is no, it doesn't admit to control.

20
00:03:14,300 --> 00:03:22,300
And so you'll naturally stop controlling. The worst thing is when you get frustrated about your control and try to stop yourself from controlling.

21
00:03:22,300 --> 00:03:32,300
These of course, that's just adding further attempts to control the situation.

22
00:03:32,300 --> 00:03:47,300
I would say to focus on the reaction to the control and focus on the object of the control, because control is already wrong practice.

23
00:03:47,300 --> 00:03:54,300
And so if that's going on, there's nothing you can't react to wrong practice. You're already practicing incorrectly at that moment.

24
00:03:54,300 --> 00:04:02,300
So you focus on what it is that you're reacting to incorrectly, and you'll teach yourself to react correctly, and then you won't control.

25
00:04:02,300 --> 00:04:10,300
So when you feel uncomfortable and discomfort, for example in the stomach, which is a very uncomfortable thing, especially if you're controlling it.

26
00:04:10,300 --> 00:04:25,300
Focus on the discomfort, discomfort or pain, pain or feeling, feeling, and disliking and so on.

27
00:04:25,300 --> 00:04:35,300
And for sure you'll find, well that's the key to retrain yourself. So you start with the rising and falling, and this is what we're expecting to see.

28
00:04:35,300 --> 00:04:42,300
We're expecting to react incorrectly and bring for it.

29
00:04:42,300 --> 00:04:48,300
And you'll deal with that.

30
00:04:48,300 --> 00:05:09,300
Or retrain our mind, based on the listening and understanding.

