1
00:00:00,000 --> 00:00:06,500
Okay, this is a question in response to a comment made during one of the meditation sessions.

2
00:00:06,500 --> 00:00:13,000
Someone mentioned that he, she should meditate more than reading or listening to Buddhist

3
00:00:13,000 --> 00:00:16,100
talks because it is also a pleasure.

4
00:00:16,100 --> 00:00:23,300
Do you suggest us to stop reading or watching Buddhist YouTube talks and just meditating

5
00:00:23,300 --> 00:00:25,900
for now and just meditate for now?

6
00:00:25,900 --> 00:00:35,700
Well, not all pleasure is wrong, in fact no pleasure is wrong, pleasure isn't the problem.

7
00:00:35,700 --> 00:00:41,400
Not all pleasure is associated with negative mind-states, there's a difference between pleasure

8
00:00:41,400 --> 00:00:48,400
and attachment or pleasure in liking if you want to say.

9
00:00:48,400 --> 00:00:53,100
There's also a kind of, you could say there's a kind of liking that is in a roundabout

10
00:00:53,100 --> 00:00:55,400
way beneficial.

11
00:00:55,400 --> 00:01:00,600
So people who like to practice meditation, people who like to listen to the Buddhist

12
00:01:00,600 --> 00:01:07,900
teaching, who are waiting for these sessions because it stimulates them and so on, it's kind

13
00:01:07,900 --> 00:01:12,800
of a negative thing because what they're really attached to is the feelings and maybe

14
00:01:12,800 --> 00:01:19,200
when I joke or when they hear stories, right, there are these people, Buddhist teachers

15
00:01:19,200 --> 00:01:22,600
who give talks and everyone likes to hear their stories.

16
00:01:22,600 --> 00:01:26,100
And if you read some of the old Buddhist texts, there's a lot of nice stories in there

17
00:01:26,100 --> 00:01:27,100
and it's stimulating.

18
00:01:27,100 --> 00:01:30,400
In the mind, it makes you feel happy, it makes you feel sad.

19
00:01:30,400 --> 00:01:35,600
It's like when we watch a movie, when we read a novel or something, it stimulates you.

20
00:01:35,600 --> 00:01:42,600
So that's kind of negative that aspect, but in a roundabout way, it can be helpful

21
00:01:42,600 --> 00:01:54,100
because of the content, because actually when you get here, the whole, or the focus changes

22
00:01:54,100 --> 00:01:58,100
or the focus is different.

23
00:01:58,100 --> 00:02:01,000
So the point really is in the content.

24
00:02:01,000 --> 00:02:06,000
If the content of the teachings is truly the Buddhist teaching and you truly are paying

25
00:02:06,000 --> 00:02:19,000
attention and making effort to understand and are conforming your mind to the teachings,

26
00:02:19,000 --> 00:02:23,600
trying to get your head around it, then for sure it's a good thing to do all of that.

27
00:02:23,600 --> 00:02:26,800
I mean, you can become enlightened listening to the dhamma.

28
00:02:26,800 --> 00:02:30,200
I hope that was kind of clear when we were talking about the last question.

29
00:02:30,200 --> 00:02:34,700
As you're listening, just listening to me talk can be a meditation practice in itself

30
00:02:34,700 --> 00:02:43,200
because you're going back again to your moment-to-moment experience,

31
00:02:43,200 --> 00:02:56,200
and just becoming aware of the practical aspects of the teaching.

32
00:02:56,200 --> 00:03:03,200
But as to the amount, yeah, you really need to meditate more than you go on YouTube

33
00:03:03,200 --> 00:03:05,200
and listen to my talks.

34
00:03:05,200 --> 00:03:12,200
I would say if you've watched all my talks, that's probably too much.

35
00:03:12,200 --> 00:03:16,200
The best thing would be if you could watch a selection of them and then go meditate

36
00:03:16,200 --> 00:03:22,200
or, I don't know, it depends if, but not just talking about my talks,

37
00:03:22,200 --> 00:03:33,200
but the, you know, you use it in moderation. I guess I've only got, what, 307 videos, if it was just mine,

38
00:03:33,200 --> 00:03:38,700
then the one a day you've got a whole year's worth or two a day or whatever,

39
00:03:38,700 --> 00:03:45,700
and then there's all those teachings from all sorts of different traditions, all different teachers.

40
00:03:45,700 --> 00:03:50,700
It has to be in moderation, and I think there are people out there.

41
00:03:50,700 --> 00:04:00,700
This is an endemic, an endemic, this is a problem in Buddhism that people become intellectual Buddhists

42
00:04:00,700 --> 00:04:12,700
and do a lot of studying, and sometimes a lot of talking, discussing, arguing, debating, thinking.

43
00:04:12,700 --> 00:04:16,700
If you watched my talk that this really can be really like this, talk this really can meditate

44
00:04:16,700 --> 00:04:21,700
or two, I've talked to really like this one about. I really like it too, I give this talk often.

45
00:04:21,700 --> 00:04:25,700
It's not my talk actually. First of all, it's based on the Suhta.

46
00:04:25,700 --> 00:04:30,700
Second of all, it's a talk my teacher always gives. That's why I picked it up.

47
00:04:30,700 --> 00:04:36,700
On the five types of people in the world, only one of which is said to be someone,

48
00:04:36,700 --> 00:04:42,700
or five types of Buddhists, only one of which is said to be someone who lives by the dhamma.

49
00:04:42,700 --> 00:04:47,700
I think the title is called one who lives by the dhamma, something like that.

50
00:04:47,700 --> 00:04:51,700
The pali is dhamma rihari.

51
00:04:51,700 --> 00:04:56,700
A person who studies the Buddhist teaching, this isn't considered to be a dhamma rihari.

52
00:04:56,700 --> 00:05:05,700
A person who studies and then teaches others, this isn't a dhamma rihari.

53
00:05:05,700 --> 00:05:11,700
A person who thinks about the teachings, goes and considers and reflects and so on.

54
00:05:11,700 --> 00:05:16,700
This isn't a dhamma rihari. A person who chants and recites and memorizes the teachings.

55
00:05:16,700 --> 00:05:19,700
This is also a dhamma rihari.

56
00:05:19,700 --> 00:05:24,700
But a person who does two things is considered to be someone who lives by the dhamma.

57
00:05:24,700 --> 00:05:28,700
A person who takes the teachings and the Buddha uses the word studies.

58
00:05:28,700 --> 00:05:34,700
A person after they study, after they become full of knowledge of the Buddhist teaching.

59
00:05:34,700 --> 00:05:39,700
So he says, he's clear that the knowledge is a good thing.

60
00:05:39,700 --> 00:05:47,700
Then they go off and practice tranquillity meditation, calm their mind down, and develop insight.

61
00:05:47,700 --> 00:06:01,700
Utherin jasapaniaya, atangpajana, they come to seek clearly the meaning of that knowledge, the meaning or the essence,

62
00:06:01,700 --> 00:06:09,700
the realization of what is meant by the teachings.

63
00:06:09,700 --> 00:06:17,700
So we can memorize the teachings and we can logically accept them, and we hear any jangdu kanganata, impermanence, suffering, non-sap.

64
00:06:17,700 --> 00:06:21,700
But the actual meaning we get from the practice.

65
00:06:21,700 --> 00:06:33,700
And utherin jasapaniaya, utherin jasapaniaya, utherin jasapaniaya, utherin utherin utherin jasapaniaya, utherin utherin utherin.

66
00:06:33,700 --> 00:06:37,700
Utherin atang, I think, is to look at the grammar.

67
00:06:37,700 --> 00:06:48,700
But a higher meaning with width, width of the banyaya, meaning that is higher than that knowledge, something like that.

68
00:06:48,700 --> 00:06:51,700
So, yeah, don't watch too much.

69
00:06:51,700 --> 00:06:55,700
Don't let it, don't let yourself be just as Buddha said at the end of the sutra.

70
00:06:55,700 --> 00:06:57,700
He said, don't be negligent.

71
00:06:57,700 --> 00:07:03,700
Don't just be a person who studies a lot or so on.

72
00:07:03,700 --> 00:07:10,700
Go and meditate after, go and meditate after you've studied or based on your studies.

73
00:07:10,700 --> 00:07:13,700
So if you're not meditating at all, then you've got a problem.

74
00:07:13,700 --> 00:07:15,700
We're just studying and so on.

75
00:07:15,700 --> 00:07:18,700
Only meditating a little bit.

76
00:07:18,700 --> 00:07:20,700
But the purpose is to meditate.

77
00:07:20,700 --> 00:07:23,700
Studying is just a roadmap.

78
00:07:23,700 --> 00:07:26,700
If you don't read the roadmap, it's hard to find your way.

79
00:07:26,700 --> 00:07:49,700
But if you just sit there reading the roadmap, it's a problem.

