1
00:00:00,000 --> 00:00:17,240
okay good evening everyone welcome to our live broadcast I'm told that the sound is

2
00:00:17,240 --> 00:00:23,760
too quiet so hopefully that's improved I think it should be a little bit louder

3
00:00:23,760 --> 00:00:51,800
now I think it's fair to say it's a universal characteristic of beings that we seek out

4
00:00:51,800 --> 00:01:00,800
what is valuable

5
00:01:00,800 --> 00:01:08,720
when we work for

6
00:01:08,720 --> 00:01:16,280
when we strive for what we plan for what we seek out

7
00:01:16,280 --> 00:01:25,680
in all aspects of our life is what is valuable

8
00:01:25,680 --> 00:01:31,400
we come to meditate because we think that there's something valuable to

9
00:01:31,400 --> 00:01:48,320
being gained from meditation we have a job employment we work in the world who

10
00:01:48,320 --> 00:02:03,760
is valuable and we see how many different kinds of things that are valuable

11
00:02:03,760 --> 00:02:10,040
different people see different things as valuable of course it goes without

12
00:02:10,040 --> 00:02:17,040
saying that some things are not valuable some people see

13
00:02:17,040 --> 00:02:25,920
things that are not valuable thinking they're valuable the things that we

14
00:02:25,920 --> 00:02:33,120
generally think of as valuable we recognize in Buddhism in the world the

15
00:02:33,120 --> 00:02:51,440
things that we think of as valuable gain status praise pleasure these are the

16
00:02:51,440 --> 00:03:02,240
things we see as valuable in the world we seek out we work hard we go to

17
00:03:02,240 --> 00:03:14,840
school we get a job for our gain we wish to gain money possessions this we get

18
00:03:14,840 --> 00:03:25,040
in the world this we seek out and this we strive for and this we obtain thinking

19
00:03:25,040 --> 00:03:34,000
or or feeling or or understanding that this is valuable we seek out status

20
00:03:34,000 --> 00:03:53,200
wanting to be well well known or esteemed good looking for a promotion looking

21
00:03:53,200 --> 00:04:07,160
for some sort of rank or status in society to not be a nobody this is

22
00:04:07,160 --> 00:04:24,200
valuable it's valuable to be powerful well known I ranked in society

23
00:04:38,120 --> 00:04:46,960
we seek out praise we do anything we can to get people's good opinion

24
00:04:48,800 --> 00:04:57,120
we work and we work and then we wait and we watch to people like me what are

25
00:04:57,120 --> 00:05:03,680
people saying about me everything it's valuable to have people's good

26
00:05:03,680 --> 00:05:17,760
opinion it's valuable to be praised people like you people esteem you people

27
00:05:17,760 --> 00:05:25,360
feel you to be valuable we all want to be valuable ourselves and of course

28
00:05:25,360 --> 00:05:35,520
happiness pleasure much of what we do is simply to seek out pleasure we have

29
00:05:35,520 --> 00:05:45,800
so many entertainment sources design just for this purpose we have pleasurable

30
00:05:45,800 --> 00:05:52,400
sights and sounds and smells and tastes and feelings and thoughts we seek out

31
00:05:52,400 --> 00:05:59,640
we strive for we work towards we work to maintain and cultivate we cling

32
00:05:59,640 --> 00:06:07,760
to we hold on to his valuable what would life be right without the

33
00:06:07,760 --> 00:06:16,520
pleasures without pleasure what would life be it would be meaningless worthless

34
00:06:16,520 --> 00:06:26,440
what's this how we feel about pleasure so putting aside the question of

35
00:06:26,440 --> 00:06:32,880
whether these things are actually valuable even if we accept for the moment

36
00:06:32,880 --> 00:06:36,680
that these are valuable it's hard to say really because what would it mean to

37
00:06:36,680 --> 00:06:41,320
say that they are valuable most people would say well I value them and

38
00:06:41,320 --> 00:06:47,800
therefore they're valuable suppose that's a good enough reason if you see

39
00:06:47,800 --> 00:06:54,040
something is valuable it's hard really to without going deeper into Buddhism

40
00:06:54,040 --> 00:07:01,880
which we will but on the surface it's hard to argue if you say it's valuable

41
00:07:01,880 --> 00:07:10,560
it's valuable the moment it doesn't mean anything to say it's valuable as

42
00:07:10,560 --> 00:07:17,480
then we have to ask what is it valuable for what is it good for and this is where

43
00:07:17,480 --> 00:07:25,920
we start to run into a problem because as much as you might say that gain is

44
00:07:25,920 --> 00:07:40,480
valuable gain is not something that you can control gain is not something that

45
00:07:40,480 --> 00:07:46,360
you're in charge of you can work really hard and maybe you become rich maybe

46
00:07:46,360 --> 00:08:00,960
you'll stay poor the point is that gain is paired with loss as much in our

47
00:08:00,960 --> 00:08:07,200
life many times in our life or much of our life is filled with gain getting

48
00:08:07,200 --> 00:08:14,040
what we want getting what is valuable getting what we esteem money possessions

49
00:08:14,040 --> 00:08:26,960
friends but likewise much of our life is filled with loss and as much as we

50
00:08:26,960 --> 00:08:40,720
cultivate gain we're always going to be disappointed and displaced by loss

51
00:08:40,720 --> 00:08:45,320
true does they go together and if you're attached to gain if you're attached to

52
00:08:45,320 --> 00:08:50,800
your possessions there's a problem there it makes you susceptible to loss

53
00:08:50,800 --> 00:08:57,080
that in fact a steaming gain is valuable holding on to it and appreciating it

54
00:08:57,080 --> 00:09:04,680
liking it and clinging to it it's really just a recipe for suffering you're

55
00:09:04,680 --> 00:09:08,800
always going to be dissatisfied you're always going to be vulnerable you're

56
00:09:08,800 --> 00:09:21,200
always going to be crushed when you lose the things that are most dear to you it's

57
00:09:21,200 --> 00:09:27,760
valuable to have money for example so you can buy the things you need

58
00:09:27,760 --> 00:09:39,400
I wouldn't say that when it makes it valuable it makes it useful in a

59
00:09:39,400 --> 00:09:44,800
practical sense I stiffer from something being valuable money isn't

60
00:09:44,800 --> 00:09:54,120
valuable money is something that it's like a debt really without it you have to

61
00:09:54,120 --> 00:09:59,200
cough it up just in order to survive and under the gaining money you're

62
00:09:59,200 --> 00:10:05,440
just working so that people don't starve you working just so that you can feed

63
00:10:05,440 --> 00:10:13,520
your body and protect your your life

64
00:10:13,520 --> 00:10:22,960
we're actually just slaves in that sense being forced to work we couldn't

65
00:10:22,960 --> 00:10:29,160
stop working it's not really valuable it's a trap that we're stuck in

66
00:10:29,160 --> 00:10:45,800
stardust I think is less common and less of a big deal in my society in this

67
00:10:45,800 --> 00:10:50,120
society anyway maybe that's not true I mean of course for many people it is

68
00:10:50,120 --> 00:10:58,000
important but we don't have a ranked society a caste system like an

69
00:10:58,000 --> 00:11:04,840
India still there's always a sense of being somebody you know making a mark in

70
00:11:04,840 --> 00:11:09,680
the world you don't just want to be some nobody flipping burgers you want to

71
00:11:09,680 --> 00:11:15,440
be a manager or a boss or someone that your parents will be proud of someone who

72
00:11:15,440 --> 00:11:22,360
has some kind of power I'm going to influence some kind of worth value

73
00:11:22,360 --> 00:11:31,520
and of course status is it is a fickle thing you can be a steamed one day and

74
00:11:31,520 --> 00:11:39,800
vilified the next or unknown the next you can be a high-class low-class

75
00:11:41,280 --> 00:11:45,880
someone's going to be the threat when you get a razor promotion there's always

76
00:11:45,880 --> 00:11:50,440
going to be the threat of someone else wanting to take your position the

77
00:11:50,440 --> 00:11:58,160
threat of having to of losing it by not performing up to snuff

78
00:12:00,880 --> 00:12:09,160
and praise and praise comes with blame we love to be praised well in place of

79
00:12:09,160 --> 00:12:14,000
praise there will always be blame they pair they're paired with each other not

80
00:12:14,000 --> 00:12:20,840
you know gay and you know there is no one in this world who is not blamed at

81
00:12:20,840 --> 00:12:28,280
some point they don't talk bad about right we're going to have everyone

82
00:12:28,280 --> 00:12:35,120
speak well about you and it's devastating if you have a strong sense of self-esteem

83
00:12:35,120 --> 00:12:39,640
or if you place importance on your what people think of you I mean this is

84
00:12:39,640 --> 00:12:46,800
really the silly thing is our concern for what other people think of you here's

85
00:12:46,800 --> 00:12:50,280
the secret in an ultimate sense it means nothing what other people think of

86
00:12:50,280 --> 00:12:57,280
you really doesn't again you might say practically speaking of course it

87
00:12:57,280 --> 00:13:02,040
makes a difference if people everyone hates you well practically or

88
00:13:02,040 --> 00:13:08,600
and conventionally that's a problem but ultimately it's not ultimately that's

89
00:13:08,600 --> 00:13:15,680
not the issue that's not what leads to suffering it's the suffering is your

90
00:13:15,680 --> 00:13:23,160
own reaction to your experience your own estimation if everyone hates you

91
00:13:23,160 --> 00:13:27,360
but you're at peace with yourself then you're far happier and far more

92
00:13:27,360 --> 00:13:33,000
better far better off and someone who everyone loves and is always a

93
00:13:33,000 --> 00:13:41,480
afraid of censure vulnerable to censure when you get a hundred up votes on your

94
00:13:41,480 --> 00:13:48,680
video and then you fret about the one down vote or when someone when you

95
00:13:48,680 --> 00:13:54,160
and everyone says nice things about you it says a quality of the internet people

96
00:13:54,160 --> 00:13:58,720
say nice things and then one person says something nasty and it just

97
00:13:58,720 --> 00:14:05,920
devastates you we're vulnerable this would value put placing value in these

98
00:14:05,920 --> 00:14:13,240
things that are frail fragile or uncertain

99
00:14:13,240 --> 00:14:24,720
instable unpredictable and happiness of course pleasure even if pleasure is

100
00:14:24,720 --> 00:14:29,000
to be understood as valuable it's hard to say one way or the other what does it

101
00:14:29,000 --> 00:14:35,000
mean to say it's valuable we like it we'd have to say does it improve your

102
00:14:35,000 --> 00:14:41,160
quality of life for we might even say does pleasure make you happy we want to

103
00:14:41,160 --> 00:14:46,920
say well of course it makes me happy unfortunately this is what we see in

104
00:14:46,920 --> 00:14:52,080
meditation that in fact it doesn't as you watch you see all it does is make you

105
00:14:52,080 --> 00:14:58,240
cling to it make you desire it more and stress and feel disappointed when you

106
00:14:58,240 --> 00:15:05,800
don't get it frustrated it's very easy to to move and to be content when you

107
00:15:05,800 --> 00:15:12,280
always get what you want right as long as the good things come to us you can

108
00:15:12,280 --> 00:15:18,480
never really measure the the level of your happiness of your mental

109
00:15:18,480 --> 00:15:23,600
development why meditation is so uncomfortable it's because it takes this

110
00:15:23,600 --> 00:15:26,320
away from you

111
00:15:27,400 --> 00:15:32,280
you know by life thinking I'm a pretty content individual maybe nothing's

112
00:15:32,280 --> 00:15:37,240
put not everything's perfect but mostly that's just because we're not we're

113
00:15:37,240 --> 00:15:46,520
generally getting what we want we generally have enough gain to get by people

114
00:15:46,520 --> 00:15:51,360
aren't saying nasty things about us we have a position in society and and we

115
00:15:51,360 --> 00:15:57,040
can find pleasure we have entertainment ways of enjoying ourselves so we

116
00:15:57,040 --> 00:16:01,680
think I'm a pretty content individual until you come to meditate you don't

117
00:16:01,680 --> 00:16:11,560
have any of that it's stressed the max suddenly have to deal with deal with

118
00:16:11,560 --> 00:16:16,440
your mind and only your mind without all these things that you hold value

119
00:16:16,440 --> 00:16:27,240
and realize that all it's done it all it's done this leave you discontent

120
00:16:27,240 --> 00:16:50,080
so the Buddha taught a series of things that are truly valuable

121
00:16:50,080 --> 00:16:58,680
setting money be conveyed and money makes there are these seven treasures seven

122
00:16:58,680 --> 00:17:12,760
valuable things they're called the seven noble treasures so if we compare all

123
00:17:12,760 --> 00:17:19,200
these worldly things with the dhamma we can start to get a sense of what it

124
00:17:19,200 --> 00:17:24,720
means for something to be truly valuable we can get a sense of how limited the

125
00:17:24,720 --> 00:17:33,040
value is of worldly things the first one is said how which means confidence or

126
00:17:33,040 --> 00:17:43,840
conviction because these are things that can't be taken away from you so

127
00:17:43,840 --> 00:17:50,240
can't take your conviction away from you if they can then it's a problem with

128
00:17:50,240 --> 00:17:54,800
your conviction it means you don't have enough you don't have enough of it

129
00:17:56,320 --> 00:18:01,600
money's not like that wealth is not like that pleasure is not like that it's

130
00:18:01,600 --> 00:18:06,760
not the more you get the harder it is to take away conviction the more you

131
00:18:06,760 --> 00:18:16,160
have the harder it is to take away the more you develop conviction

132
00:18:17,160 --> 00:18:24,160
we're seeing the truth through practicing the Buddha's teaching and I

133
00:18:24,160 --> 00:18:29,720
mean to understand the way things are let's practice meditation and you see

134
00:18:29,720 --> 00:18:35,240
form as impermanent feeling as impermanent and so on according to the

135
00:18:35,240 --> 00:18:40,360
discourses we talked about the past few sessions

136
00:18:44,480 --> 00:18:51,440
more you see that the more conviction you gain you don't waiver you don't

137
00:18:51,440 --> 00:18:55,280
have any doubt about is this valuable is that valuable is this right is that

138
00:18:55,280 --> 00:19:01,600
right are we have treasure and conviction that most people don't have because

139
00:19:01,600 --> 00:19:05,720
they don't have a sense of direction we might have beliefs but they're

140
00:19:05,720 --> 00:19:13,840
based on shaky foundations and as a result not very stable the conviction of

141
00:19:13,840 --> 00:19:21,600
most people is not very strong they're lacking in this treasure even very

142
00:19:21,600 --> 00:19:25,120
religious people have to work really hard to maintain their conviction

143
00:19:25,120 --> 00:19:33,360
because it's based on shaky principles it's based on principles or beliefs

144
00:19:33,360 --> 00:19:45,920
that are unverifiable so that's the first treasure the second treasure is

145
00:19:45,920 --> 00:19:55,520
see the ethics people can't take your ethics away from you the more

146
00:19:55,520 --> 00:20:08,760
ethically set you are the harder it is to take it away this is another treasure

147
00:20:08,760 --> 00:20:13,000
that is very hard to take away from someone who has it it's easy for those who

148
00:20:13,000 --> 00:20:16,880
don't really have it even many Buddhists if they say yes I keep the five

149
00:20:16,880 --> 00:20:24,800
precepts but if they're being attacked by a vicious beast like a mosquito they'll

150
00:20:24,800 --> 00:20:31,080
be quick to change their tune in order to save their blood

151
00:20:35,000 --> 00:20:42,000
the person who truly has ethics is unshakable they have no problem dying you

152
00:20:42,000 --> 00:20:49,280
know ethics on this ethics on a noble level it's not something you can bargain

153
00:20:49,280 --> 00:20:54,000
with so somebody you wouldn't bargain with it goes beyond that the power of it

154
00:20:54,000 --> 00:21:02,520
is that there's not a question of breaking it to save your life your life is

155
00:21:02,520 --> 00:21:10,240
less valuable than ethics it's a very powerful thing because of course our

156
00:21:10,240 --> 00:21:16,800
life is it's not as valuable as ethics you might say it's valuable but it's

157
00:21:16,800 --> 00:21:23,280
only valuable in terms of how ethical we are your life is full of corruption

158
00:21:23,280 --> 00:21:32,760
and evil your life is actually whatever the opposite of valuable is

159
00:21:32,760 --> 00:21:40,600
you accumulate debt through it through your life the debt of evil the debt of

160
00:21:40,600 --> 00:21:45,440
corruption and when you die you have to pay up

161
00:21:54,120 --> 00:22:00,040
the third treasure is he the third and the fourth are he and all the

162
00:22:00,040 --> 00:22:05,840
about and these go together so they have to make kind of explain together

163
00:22:05,840 --> 00:22:10,520
their shame and fear those are the words we used to describe them in a

164
00:22:10,520 --> 00:22:14,640
minute it really doesn't do them justice but that is the sort of the words

165
00:22:14,640 --> 00:22:19,160
the Buddha used but the meaning isn't that you actually feel bad or so on

166
00:22:19,160 --> 00:22:25,400
shame means a disintegration to perform evil deeds to perform those

167
00:22:25,400 --> 00:22:33,800
unethical deeds here is a state of mind that focuses on karma and says

168
00:22:33,800 --> 00:22:40,240
it's about bad karma just through understanding you know through

169
00:22:40,240 --> 00:22:47,040
experience when you see again and again what your evil deeds do what your

170
00:22:47,040 --> 00:22:53,800
well an impure deeds I mean you see what certain deeds certain states of

171
00:22:53,800 --> 00:22:59,840
mind certain inclinations you see their results and meditation again and

172
00:22:59,840 --> 00:23:10,920
again you see that you see where your mind leads you you shy away from it you

173
00:23:10,920 --> 00:23:16,960
think about those of the idea of doing this or inclining in this direction is

174
00:23:16,960 --> 00:23:23,400
undesirable there's an aversion in a sense to there's a recoiling from it that's

175
00:23:23,400 --> 00:23:29,360
here that's shame and Ota-pa fear Ota-pa is the same idea but it's focused on

176
00:23:29,360 --> 00:23:40,120
results it's the fear of the results of bad deeds so when you think about certain

177
00:23:40,120 --> 00:23:48,720
acts or thoughts or words that you might do or say or think you can

178
00:23:48,720 --> 00:23:54,520
consider that there's the result of the of those deeds what would be the outcome

179
00:23:54,520 --> 00:24:02,040
what would they lead to and your recoil from that meaning wisely you understand

180
00:24:02,040 --> 00:24:07,520
that that's not a good result doing that saying that thinking that killing

181
00:24:07,520 --> 00:24:13,120
stealing lying cheating by careful observation and and thorough

182
00:24:13,120 --> 00:24:19,800
understanding you come to see how the how how negative the result of these

183
00:24:19,800 --> 00:24:30,320
things is the results are here in Ota-pa this is not something that someone

184
00:24:30,320 --> 00:24:39,440
can take away from you you have these things if you understand these things and

185
00:24:39,440 --> 00:24:44,840
you have no concern for people forcing you to do evil deeds or coursing

186
00:24:44,840 --> 00:24:50,160
you because there's nothing more valuable than that you start to see

187
00:24:50,160 --> 00:24:54,920
what's truly valuable it might give you money bribe you money to do this to do

188
00:24:54,920 --> 00:25:03,960
that it was a story of a leper I think it's a very poor man and

189
00:25:03,960 --> 00:25:09,560
tomorrow one of the gods came to test him and said give you a give you a

190
00:25:09,560 --> 00:25:14,600
make your rich man if you just renounce the Buddha say I don't take refuge in the

191
00:25:14,600 --> 00:25:19,280
Buddha the dumb and the song and he was a sort upon this guy and he see he

192
00:25:19,280 --> 00:25:35,680
says who are you get away from me I'm not I'm not poor I don't need money I'm rich

193
00:25:35,680 --> 00:25:39,840
you have to ask ourselves what is truly valuable these are things that are truly

194
00:25:39,840 --> 00:25:48,560
valuable that no one can take away from you number five is Suta Suta means

195
00:25:48,560 --> 00:25:55,720
learning learning is something can we take it away from you can forget it and

196
00:25:55,720 --> 00:26:01,840
the message very valuable I can't be taken away by ordinary means you can't

197
00:26:01,840 --> 00:26:08,840
rip it from someone's mind without giving them a little bottomy but it's

198
00:26:08,840 --> 00:26:12,280
really for most of us it's there to stay I mean it's a very valuable thing to

199
00:26:12,280 --> 00:26:19,600
have knowledge of course they're knowledge there's knowledge in a

200
00:26:19,600 --> 00:26:22,840
worldly sense and we know that that's valuable but here we're really talking

201
00:26:22,840 --> 00:26:32,560
about learning spiritual teachings learning those teachings that are going to

202
00:26:32,560 --> 00:26:39,320
help us on our path to become free from suffering that's a great value even

203
00:26:39,320 --> 00:26:45,200
just memorizing the Buddha's teachings or learning it thoroughly it's like

204
00:26:45,200 --> 00:26:56,440
having a guide on the path it's invaluable it's valuable something that is

205
00:26:56,440 --> 00:27:02,920
priceless something that you can't do without it's necessary and you would say

206
00:27:02,920 --> 00:27:09,480
without it you're wandering lost so learning and Buddhism is we talk a lot

207
00:27:09,480 --> 00:27:14,360
about how it's not enough and maybe presented as something that's inferior to

208
00:27:14,360 --> 00:27:19,480
practice which of course it is nonetheless great learning is a it's a great

209
00:27:19,480 --> 00:27:24,280
treasure something you don't mean you don't have to go and look it up in a

210
00:27:24,280 --> 00:27:28,400
book when you know something when you've learned something you can carry it

211
00:27:28,400 --> 00:27:41,360
with you the sixth treasure is Jaga which means generosity or giving giving

212
00:27:41,360 --> 00:27:48,800
is maybe the best why because giving all something giving up the

213
00:27:48,800 --> 00:28:00,040
word sword and then we're giving giving the ability to give things away so

214
00:28:00,040 --> 00:28:03,760
charity is an important part of this being generous to others right this is

215
00:28:03,760 --> 00:28:07,080
something people can't take away from you they can take away all you can

216
00:28:07,080 --> 00:28:13,160
hold up all your treasure and it can be taken away in a heart beat but

217
00:28:13,160 --> 00:28:20,120
charity is something people can't take away unless they unless you have

218
00:28:20,120 --> 00:28:24,000
nothing I suppose but you see there's it's really impossible to have nothing

219
00:28:24,000 --> 00:28:30,920
especially if you have all these noble treasures they might take away your

220
00:28:30,920 --> 00:28:36,240
your belongings your pure mere material possessions but you can still be very

221
00:28:36,240 --> 00:28:44,160
generous and kind helping others teaching others advising others relating

222
00:28:44,160 --> 00:28:51,960
the things you've learned generosity is a great gift ability to give is a

223
00:28:51,960 --> 00:29:02,800
great wealth so many good things come from it friendship esteem really all

224
00:29:02,800 --> 00:29:06,880
the good things in life the world lead material good things in life they all

225
00:29:06,880 --> 00:29:11,120
come not from hoarding them or not from seeking them out but they come from

226
00:29:11,120 --> 00:29:18,440
generosity from things like generosity and kindness and sharing giving these

227
00:29:18,440 --> 00:29:25,040
things up when you give them up you're more inclined to people are more inclined

228
00:29:25,040 --> 00:29:36,680
to appreciate you be grateful for you and the last one is

229
00:29:36,680 --> 00:29:46,720
Banya the wealth the treasure of wisdom it's important to see that this is

230
00:29:46,720 --> 00:29:54,760
separate from separate from Suta so what we mean here is not wisdom through

231
00:29:54,760 --> 00:30:06,840
learning or intellectual activity Suta means learning Banya means wisdom in

232
00:30:06,840 --> 00:30:22,560
the sense of understanding this is what comes from is what we mean when we

233
00:30:22,560 --> 00:30:28,960
talk about wheat bus and what we strive for through the meditation

234
00:30:28,960 --> 00:30:35,440
practice inside meditation mindfulness meditation as you practice you come to

235
00:30:35,440 --> 00:30:41,200
understand about yourself and about the world around you impermanence the

236
00:30:41,200 --> 00:30:46,480
things that you cling cling to as stable as satisfying as valuable you come to

237
00:30:46,480 --> 00:30:53,200
see that they're impermanent and in fact not really valuable at all they can't

238
00:30:53,200 --> 00:31:18,160
really make you happy

239
00:31:24,040 --> 00:31:30,080
so we're having a little trouble with the audio I don't know if that helps

240
00:31:30,080 --> 00:31:47,040
it's not really hard to adjust this seeing things as they are learning to

241
00:31:47,040 --> 00:31:52,600
adjust your sentence of what's valuable it's useful let's see what's right and what's

242
00:31:52,600 --> 00:31:58,280
wrong and to align yourself with what's right with what's actually going to

243
00:31:58,280 --> 00:32:04,000
make you happy to understand what's truly valuable

244
00:32:08,840 --> 00:32:13,080
surely what we aim for in Buddhism wisdom is the most valuable something

245
00:32:13,080 --> 00:32:17,200
people can't take away take away from you certainly if you have true wisdom it

246
00:32:17,200 --> 00:32:24,840
can never leave you wisdom is something that changes the core of who you are

247
00:32:24,840 --> 00:32:34,280
changes your future changes your destiny wisdom is that would really

248
00:32:34,280 --> 00:32:43,480
changes it really frees you from all the bad things all the things all the

249
00:32:43,480 --> 00:32:48,880
bad paths you might follow through lack of wisdom is

250
00:32:48,880 --> 00:32:54,760
real lack of understanding right if we knew something was bad for us why would

251
00:32:54,760 --> 00:33:02,200
we go that way so the only way to truly be free from suffering is to

252
00:33:02,200 --> 00:33:06,520
cultivate understanding of those things that cause us suffering and those

253
00:33:06,520 --> 00:33:10,520
things that actually truly lead to happiness it's a very difficult thing to do

254
00:33:10,520 --> 00:33:14,560
because it's easy to think that something is valuable it's much more

255
00:33:14,560 --> 00:33:21,800
difficult to actually know and be sure through understanding through

256
00:33:21,800 --> 00:33:30,800
realization so that's the demo for tonight I apologize if recordings were not

257
00:33:30,800 --> 00:33:38,880
that good but dealing with limited really hacking this to get a broadcast up

258
00:33:38,880 --> 00:33:46,880
it also hopefully it was at least more or less understandable we show all the

259
00:33:46,880 --> 00:34:06,280
best

260
00:34:06,280 --> 00:34:25,360
okay so we have a question on the site

261
00:34:25,360 --> 00:34:44,120
why do you people ask questions about my monastic life is it really all that interesting

262
00:34:44,120 --> 00:34:59,400
there's a bunch of questions on the site you can go yes please rather you

263
00:34:59,400 --> 00:35:02,240
guys don't stay around for the questions because they're not related to your

264
00:35:02,240 --> 00:35:21,040
practice

265
00:35:21,040 --> 00:35:24,520
not related to practice at all okay so when I ordained the

266
00:35:24,520 --> 00:35:32,240
18 18 of us ordained but 17 of them were just ordaining well 15 of them were just

267
00:35:32,240 --> 00:35:37,320
ordaining temporarily that's the answer to that two of them were kind of not

268
00:35:37,320 --> 00:35:42,440
sure I think those are the two foreigners actually I don't really know I don't

269
00:35:42,440 --> 00:35:45,800
really know the story of all 18 I know that 15 of them were ordaining for

270
00:35:45,800 --> 00:36:01,440
specific function they were ordaining for the king's birthday so that was that

271
00:36:01,440 --> 00:36:16,920
necessary to observe so-called Rupa Calapas I mean yeah I mean you don't

272
00:36:16,920 --> 00:36:20,120
really understand them in that way that's just a technical description of

273
00:36:20,120 --> 00:36:28,120
course you have to study Rupa like moments of Rupa but it's not anything like

274
00:36:28,120 --> 00:36:32,560
microscope where you see a color where you see the smallest divisible

275
00:36:32,560 --> 00:36:38,640
important to see Rupa and to see that Rupa is impermanent to see that they're

276
00:36:38,640 --> 00:36:45,600
discreet sight when you extend your arm there's a discreet experience that

277
00:36:45,600 --> 00:36:51,320
arises in cases so I have to go through all the ten insight knowledges four

278
00:36:51,320 --> 00:36:58,840
times technically yes 16 there's 16 insight knowledges although how does that

279
00:36:58,840 --> 00:37:05,560
work yeah something like that

280
00:37:05,560 --> 00:37:23,680
laying down to meditate

281
00:37:24,120 --> 00:37:29,000
heartbeat is the main thing I'm aware of I note beating sure yes note that

282
00:37:29,000 --> 00:37:37,360
your heart is beating feeling feeling and if eventually your mind let's

283
00:37:37,360 --> 00:37:42,880
go of it then you can go back to the stomach there's nothing that you

284
00:37:42,880 --> 00:37:45,720
don't have to worry about things being a distraction just be mindful of

285
00:37:45,720 --> 00:37:51,560
them if you note that there's a sinking with your heart beat and say

286
00:37:51,560 --> 00:37:56,200
knowing knowing so I'll just take what you're talking about as experience it's

287
00:37:56,200 --> 00:38:00,760
going to take you some time to untangle it all but that's the practice

288
00:38:10,760 --> 00:38:15,960
I recently called my brother unimpathic conceited an arrogant over a text

289
00:38:15,960 --> 00:38:22,560
message this considered ill will it's probably considered harsh speech if you

290
00:38:22,560 --> 00:38:26,760
had anger in your mind then it was probably wrong speech but only an

291
00:38:26,760 --> 00:38:31,640
on-a-gami is free from such things it's important to be right I mean

292
00:38:31,640 --> 00:38:38,520
is he an empathic conceited an arrogant if he is then it's not so bad might be

293
00:38:38,520 --> 00:38:44,520
consider it would be considered unskillful unless you are completely devoid of

294
00:38:44,520 --> 00:38:49,920
anger and you really just thought this was something he needed to hear if you

295
00:38:49,920 --> 00:39:00,640
did it out of anger not so good for you how old does one have to be to

296
00:39:00,640 --> 00:39:05,200
our day and become a Buddhist monk 20 years old

297
00:39:09,440 --> 00:39:12,960
this Tuesday is considered terrible I don't know this Tuesday is

298
00:39:12,960 --> 00:39:22,800
this Tuesday this Tuesday is the Southeast Asian New Year which I think

299
00:39:22,800 --> 00:39:28,680
actually comes from India I think this is the plowing ceremony I'm not quite

300
00:39:28,680 --> 00:39:33,600
sure can't remember now anyway I think it's it's tied up with Hinduism more

301
00:39:33,600 --> 00:39:51,360
than Buddhism nothing to do with terror about Buddhism is it breaking the second

302
00:39:51,360 --> 00:39:57,320
pre-septive you secretly take back something that was stolen from you

303
00:39:57,320 --> 00:40:10,760
it's a good question if you relinquish ownership of something then it's

304
00:40:10,760 --> 00:40:23,000
considered stolen no if you relinquish ownership of something if you relinquish

305
00:40:23,000 --> 00:40:27,560
ownership of it at some point like if someone stole it from you that's

306
00:40:27,560 --> 00:40:33,080
stealing but then if at some point you give it up and you say okay it's there

307
00:40:33,080 --> 00:40:38,200
is now and then you take it back then it's stealing this is really

308
00:40:38,200 --> 00:40:44,240
technicality but if you never do and then you find a do has it or you always

309
00:40:44,240 --> 00:40:48,480
knew who had it and you always thought of that's mine and you take it back it's

310
00:40:48,480 --> 00:41:01,080
not considered stealing technically good question I see suffering and

311
00:41:01,080 --> 00:41:05,720
unhappiness in my family I'm unfortunate to have found Buddhism planning

312
00:41:05,720 --> 00:41:13,320
or dating as a monk but I feel obligated to stand out my family I'm sorry I

313
00:41:13,320 --> 00:41:17,280
don't really have any advice you have to make a choice for yourself it's very

314
00:41:17,280 --> 00:41:23,640
hard to give people that sort of general advice to make the decision

315
00:41:23,640 --> 00:41:27,440
yourself because if you're not really a hundred percent confident about your

316
00:41:27,440 --> 00:41:32,920
desire to become a monk and your position to become a monk it's very difficult

317
00:41:32,920 --> 00:41:40,560
to stay a monk isn't charity more than simply giving up I think give up

318
00:41:40,560 --> 00:41:47,640
something by throwing it away or leaving behind charity requires you admit or

319
00:41:47,640 --> 00:41:52,880
karuna well what I meant is that charity requires you have to have the ability

320
00:41:52,880 --> 00:42:02,160
to let go of something you're unable to let go of it and and and more

321
00:42:02,160 --> 00:42:07,600
important than simply charity is the ability to let go in general because you

322
00:42:07,600 --> 00:42:11,200
say yes you you can you can give something up by throwing it away but you say

323
00:42:11,200 --> 00:42:15,680
it as though that's a insignificant act right how many people can throw away

324
00:42:15,680 --> 00:42:21,560
something that they cling to and so the ability to throw something away is a

325
00:42:21,560 --> 00:42:24,480
good sign it's a very important thing it's actually more important than

326
00:42:24,480 --> 00:42:28,720
charity payments every hunger went through this I just happened I think of him

327
00:42:28,720 --> 00:42:33,520
whenever I talk about jhaga because they wanted to translate jhaga as

328
00:42:33,520 --> 00:42:40,320
generosity said no that's not what it means and explain that it's it's more

329
00:42:40,320 --> 00:42:46,320
than just giving it's about giving up I think he's I think he's right to

330
00:42:46,320 --> 00:42:51,840
he's right in a cent I mean it's it's you can see it in many different ways

331
00:42:51,840 --> 00:42:59,920
but giving up is more important the ability to throw things away you

332
00:42:59,920 --> 00:43:06,240
you you make it sound like it's something easy it's really not it's easy to throw

333
00:43:06,240 --> 00:43:10,560
away things that are not valuable to you just like it's easy to give away

334
00:43:10,560 --> 00:43:16,720
things that are not valuable to you it's very difficult to throw away or give

335
00:43:16,720 --> 00:43:22,960
away things that are valuable to you so this is an important quality of

336
00:43:22,960 --> 00:43:28,720
mind something that people can't take away from you right if if you don't

337
00:43:28,720 --> 00:43:33,040
want anything the no one can ever steal it ever makes you suffer from

338
00:43:33,040 --> 00:43:38,160
stealing anything from you anything they take from you right it's the

339
00:43:38,160 --> 00:43:45,280
greatest possession it's contentment that's what I mean but yes you're in the

340
00:43:45,280 --> 00:43:49,600
suit to hear it actually does just refer I mean but the Buddha gives of

341
00:43:49,600 --> 00:43:53,200
course the teaching is on many different levels but on the most basic level

342
00:43:53,200 --> 00:43:57,840
it just means and the Buddha says this in the suit just means giving

343
00:43:57,840 --> 00:44:09,120
giving a supporting others giving arms giving charity

344
00:44:14,400 --> 00:44:17,680
all right so those are all the questions from our site and again I'm not

345
00:44:17,680 --> 00:44:24,080
answering questions elsewhere because it's just too much confusion and

346
00:44:24,080 --> 00:44:28,880
well this is easiest and it means you have to work in order to ask a

347
00:44:28,880 --> 00:44:34,000
question so hopefully that worked out well and try to keep tweaking this

348
00:44:34,000 --> 00:44:55,360
broadcast thank you all for coming out

