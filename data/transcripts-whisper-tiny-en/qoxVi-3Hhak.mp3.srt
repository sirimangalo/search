1
00:00:00,000 --> 00:00:15,000
Okay, good evening, everyone. Welcome to our evening dhamma, the night we are looking at the second noble truth.

2
00:00:15,000 --> 00:00:42,000
The cause of suffering is suffering. The whole idea of a cause sets the Buddha's teaching up as a...

3
00:00:46,000 --> 00:00:51,000
a cause-based religion, a cause-based philosophy teaching.

4
00:00:54,000 --> 00:01:09,000
And it's important because there are philosophies, teachings, religions that believe in superpowers.

5
00:01:09,000 --> 00:01:30,000
And that's different from a cause-based religion because it implies potency and it implies agency.

6
00:01:30,000 --> 00:01:41,000
The ability to control, to change, to wield mastery over something.

7
00:01:43,000 --> 00:01:57,000
A cause-based teaching is different from this and gets in the way of the idea of a master or a god.

8
00:01:57,000 --> 00:02:13,000
It conflicts with it. So it's on the side of science which tries to see order and an impersonal order.

9
00:02:13,000 --> 00:02:40,000
So the order is not dictated by someone's whim ultimately, but our whims, on the other hand, are a part of the system that the system has some rules to it, some nature to it.

10
00:02:40,000 --> 00:02:51,000
So, for example, if there were an omnipotent being, then there could never be a cause for suffering.

11
00:02:52,000 --> 00:02:59,000
Because the omnipotent being could change and could say, oh wait, I think today we'll make this the cause of suffering.

12
00:02:59,000 --> 00:03:12,000
As soon as you say there's a cause for suffering, I mean, in the sense that we do in Buddhism, we're going against that idea.

13
00:03:13,000 --> 00:03:17,000
And this isn't really what the formable truths are about, but it's an important point.

14
00:03:17,000 --> 00:03:37,000
Such that, just describing this nature, this quality of the Buddha's teaching, was enough to lean Mogalana and Sariputta to become Sotapana.

15
00:03:38,000 --> 00:03:42,000
They just heard that the Buddha taught cause and effect.

16
00:03:42,000 --> 00:03:47,000
And it was such a novel idea.

17
00:03:49,000 --> 00:03:58,000
It was just what they were looking for. It was all they needed to see the truth, to let go.

18
00:03:59,000 --> 00:04:04,000
So I think the implications of it are not easy to appreciate.

19
00:04:04,000 --> 00:04:23,000
It's hard to understand how a simple statement gave Dhamma, Hetupababa, whatever Dhamma's arise by a cause, the Buddha has taught their cause and the cessation of the cause.

20
00:04:23,000 --> 00:04:34,000
And just that was enough for them to become Sotapana.

21
00:04:35,000 --> 00:04:46,000
So the whole reason why the cause of suffering is craving is because it's out of line with reality.

22
00:04:46,000 --> 00:04:56,000
Whenever you want something, whenever you want, you're going against this idea of order.

23
00:04:57,000 --> 00:05:05,000
A want has nothing to do with the way things are.

24
00:05:05,000 --> 00:05:24,000
You're considered and it's such a profound truth that, or it's such an important or a fundamental reality that it makes almost no sense to us.

25
00:05:25,000 --> 00:05:34,000
The idea that craving in its very nature is out of touch with reality.

26
00:05:34,000 --> 00:05:38,000
I think this is something new to most people.

27
00:05:39,000 --> 00:05:42,000
I mean, you can think philosophically about how craving is.

28
00:05:42,000 --> 00:05:50,000
Yes, yes. If you want things, you'll end up sometimes not getting what you want, but sometimes you'll get what you want.

29
00:05:50,000 --> 00:06:07,000
But we don't go the distance and to understand that by its very nature, craving is out of wanting, is out of touch with reality.

30
00:06:07,000 --> 00:06:17,000
But it's an ignorance as the cause of the cause of craving.

31
00:06:18,000 --> 00:06:26,000
It's only through ignorance that we want things. It's based on a misunderstanding of reality.

32
00:06:26,000 --> 00:06:41,000
I mean, the most sensible way of living with reality is something quite foreign to most people, but the most sensible way is to be in touch with reality.

33
00:06:42,000 --> 00:06:48,000
I mean, this whole teaching on mindfulness, what does it mean? It's the alternative to wanting, really.

34
00:06:48,000 --> 00:07:01,000
Wanting is the pushing and the pulling, the inclining, the manipulating.

35
00:07:04,000 --> 00:07:15,000
Mindfulness is the adjusting and adapting, accepting, understanding, the being, right?

36
00:07:15,000 --> 00:07:21,000
Mindfulness is now it's changing like this, now it's changing like that.

37
00:07:22,000 --> 00:07:26,000
Wanting is not changed like this, may it change like that?

38
00:07:29,000 --> 00:07:37,000
There's a real difference there, and we have to understand that these are two fundamentally different ways of approaching life.

39
00:07:37,000 --> 00:07:44,000
They're really the two options.

40
00:07:45,000 --> 00:07:52,000
And the Buddhist Akhrosa in his wisdom that the option of craving is the cause for suffering.

41
00:07:52,000 --> 00:08:13,000
They're the choice to try and manipulate and the way of inclining, favoring, clinging, attaching.

42
00:08:13,000 --> 00:08:21,000
This is a cause for suffering.

43
00:08:22,000 --> 00:08:33,000
He saw that it's based on this craving that all of our problems come about, all these things that the Buddhist Akhrosa problem, birth, you know, birth is caused by craving.

44
00:08:33,000 --> 00:08:44,000
We're only reborn because we have some desire left. We're going to saw this, old age sickness and death thereby come from that.

45
00:08:44,000 --> 00:09:04,000
But even if we're not a Buddha or someone who can see those things, it's quite clear that the other sorts of suffering that come from craving, not getting what you want, getting what you don't want.

46
00:09:04,000 --> 00:09:14,000
You can't always get what you want. It's just the harsh reality. I mean, it's set up in such a way to fail.

47
00:09:15,000 --> 00:09:21,000
Craving is not static where you say, I think today I'll want something. I think today I'll want this.

48
00:09:22,000 --> 00:09:27,000
Okay, enough of that back to not wanting it. It's not how it works.

49
00:09:27,000 --> 00:09:36,000
It's habitual. You want and it becomes a part of your personality, especially when you get what you want.

50
00:09:37,000 --> 00:09:43,000
Getting what you want unavoidably leads to an increase in wanting.

51
00:09:43,000 --> 00:10:00,000
And that fact is crucial because if you could just keep wanting the same amount, that would be fine. Every time you get what you want, okay, good. Tomorrow I'll want it again and I'll get it again, good.

52
00:10:01,000 --> 00:10:03,000
It doesn't work that way.

53
00:10:03,000 --> 00:10:14,000
The next time you get what you want, it's not enough. Your wanting has increased.

54
00:10:15,000 --> 00:10:32,000
So not only is it a problem when you don't get what you want, when reality doesn't provide the object of your desire, but also you need more.

55
00:10:32,000 --> 00:10:40,000
But getting the object of your desire is no longer enough. The object of your desire increases.

56
00:10:40,000 --> 00:11:01,000
We never think of this option. We never think of this as a viable way to live, a viable way to approach reality, to just be.

57
00:11:01,000 --> 00:11:15,000
I suppose in the abstract we think of ourselves as most of the time just living. The truth is we're not. The truth is we're not really living.

58
00:11:15,000 --> 00:11:37,000
We're constantly losing track of reality incessantly, repeatedly getting lost in not how things are, but how we wish they were.

59
00:11:37,000 --> 00:12:04,000
In the one hand that's a problem because it's not going to be the way we want. If you take the mind as the observer, as though we're in a car looking, watching as things go by, we're not always going to experience what we want, but it's worse than that in fact.

60
00:12:04,000 --> 00:12:12,000
Because based on this one, we're not only going to wish and hope. I hope I see this, I hope I see that.

61
00:12:12,000 --> 00:12:20,000
But we're going to take detours and we're going to make things more complicated as we want more and more.

62
00:12:20,000 --> 00:12:36,000
This isn't theoretical, this is what's happening in the world right now. War is almost entirely caused by greed, crime, family, domestic abuse.

63
00:12:36,000 --> 00:12:52,000
It's all caused by not getting what you want. It's the reason why families fight. It's the reason why spouses cheat on each other.

64
00:12:52,000 --> 00:13:08,000
It's the reason why families fight for sure. You start getting more and more selfish and cruel to each other, manipulative and angry, because you're not given what you want.

65
00:13:08,000 --> 00:13:34,000
It's the cause of the raping of the natural world for resources, mining for resources, burning fuel, tearing down forests, putting up cities, driving here and there.

66
00:13:34,000 --> 00:13:47,000
It's the cause of so much hustle and bustle, so much complexity, so much energy invested.

67
00:13:47,000 --> 00:14:09,000
As a Buddhist, when you begin to meditate, you just realize, take a step back and look at society, look at the world and say, oh my, that's all it is. There's nothing mystical or magical about human to human world.

68
00:14:09,000 --> 00:14:24,000
It's just a whole heap of craving and all the things, all the problems and stresses that come with it. It's all it's ever been. It's just gotten more and more complicated.

69
00:14:24,000 --> 00:14:30,000
How can we get what we want? And there's so, so many.

70
00:14:30,000 --> 00:14:50,000
The Buddha said there are three kinds of craving that lead to further becoming, craving for sensuality, craving for becoming and craving for non-becoming.

71
00:15:00,000 --> 00:15:29,000
It's all craving for being and non-being. It's all craving because everything, all of our ambitions, all of our complicated goals wanting to be, this wanting to be that, wanting to be rich or famous, wanting to become something, wanting to be born and heaven.

72
00:15:30,000 --> 00:15:37,000
And wanting to be one with God.

73
00:15:43,000 --> 00:15:58,000
So there's the desire for sensuality. We just, we want beautiful sights. We have craving for sights and sounds and smells, tastes and feelings that are pleasant.

74
00:15:58,000 --> 00:16:17,000
The desire for becoming is our ambitions, wanting to be rich and famous. Our attachment to ourselves, our body, our mind, attachment to a personality.

75
00:16:17,000 --> 00:16:33,000
The desire for non-becoming is desire for things not to be. I wish I were taller. I wish I were smarter. I wish I didn't.

76
00:16:33,000 --> 00:16:42,000
I wish I had different parents or this kind of thing. I wish I could quit my job.

77
00:16:42,000 --> 00:16:49,000
I wish I could go live in the forest. I wish I could leave this, leave that. I wish I didn't have this or that.

78
00:16:49,000 --> 00:17:01,000
I wish the mosquitoes weren't buzzing around my ear. I wish I could just die.

79
00:17:01,000 --> 00:17:14,000
Suicide is this sort of thing. An allationism wishing for things to just end.

80
00:17:14,000 --> 00:17:22,000
Wishing it and the funny thing, wishing for things to end is another kind of craving that leads for things to come again.

81
00:17:22,000 --> 00:17:37,000
Because of the desire for things not to be, there will be more being. Special kinds of being that are more void, but not really sure exactly how that works.

82
00:17:37,000 --> 00:17:46,000
In the bigger picture, but generally, you're going to be without.

83
00:17:46,000 --> 00:17:59,000
When you want to be without, I think.

84
00:17:59,000 --> 00:18:24,000
So, the, and remember of each of the truth, there is a, there is a, a kitchen, there is a task to be done.

85
00:18:24,000 --> 00:18:31,000
There are different kinds of abandoning. There's a, a mindfulness is actually only a temporary abandoning.

86
00:18:31,000 --> 00:18:36,000
At the time when your mindful, there's no craving.

87
00:18:36,000 --> 00:18:42,000
So, it's abandoned. At the time when you're meditating in tranquillity meditation, there's also no craving.

88
00:18:42,000 --> 00:18:47,000
When you enter into a trance, a jhana.

89
00:18:47,000 --> 00:18:51,000
But these, these kinds of abandoning are only temporary.

90
00:18:51,000 --> 00:18:57,000
And this is where you see the difference between tranquillity and insight meditation and tranquillity meditation.

91
00:18:57,000 --> 00:19:00,000
Yes, you abandon them.

92
00:19:00,000 --> 00:19:08,000
But so what? And when they come back, when you leave the, the jhana, you're not any more free of them.

93
00:19:08,000 --> 00:19:12,000
But when you practice insight meditation, they're abandoned.

94
00:19:12,000 --> 00:19:26,000
But the state of abandoning, because you're focused on the objects of your, of your attraction, you also see that those things are not worth clinging to.

95
00:19:26,000 --> 00:19:32,000
You see why they are causing you suffering. Why the craving is not leading to satisfaction.

96
00:19:32,000 --> 00:19:38,000
Oh, because these things are based on cause and effect.

97
00:19:38,000 --> 00:19:43,000
You see that the craving increases that the objects don't increase.

98
00:19:43,000 --> 00:19:48,000
The objects of your desire are ultimately finite.

99
00:19:48,000 --> 00:19:57,000
And these are to understand these things because you're actually watching the process.

100
00:19:57,000 --> 00:20:02,000
And so the real abandoning is through wisdom, through seeing this.

101
00:20:02,000 --> 00:20:11,000
And ultimately through seeing an nibana, which is something that is actually satisfying and peaceful.

102
00:20:11,000 --> 00:20:14,000
And when you see nibana, of course, that's the ultimate.

103
00:20:14,000 --> 00:20:22,000
The abandoning through leaving behind some sara is that when you say, oh, right, yes.

104
00:20:22,000 --> 00:20:27,000
Well, I guess that stuff isn't, isn't useful anymore.

105
00:20:27,000 --> 00:20:34,000
Now I've seen something better.

106
00:20:34,000 --> 00:20:38,000
It's abandoned by seeing suffering, by seeing the truth of suffering.

107
00:20:38,000 --> 00:20:44,000
When you see the truth of suffering, you, you abandon craving because you see the things that you cling to.

108
00:20:44,000 --> 00:20:48,000
Remember again, the five aggregates.

109
00:20:48,000 --> 00:20:52,000
You see them as suffering.

110
00:20:52,000 --> 00:21:03,000
And again, not suffering inherently, but as something that is just going to cause you to suffer if you cling to it.

111
00:21:03,000 --> 00:21:06,000
It's something that can't satisfy.

112
00:21:06,000 --> 00:21:09,000
It can provide happiness.

113
00:21:09,000 --> 00:21:14,000
The attachment to it only leads to more attachment.

114
00:21:14,000 --> 00:21:16,000
Doesn't lead to greater happiness.

115
00:21:16,000 --> 00:21:31,000
It leads to more and more and more wanting less and less and less pleasure.

116
00:21:31,000 --> 00:21:34,000
So I think it's important that we compare these two paths.

117
00:21:34,000 --> 00:21:39,000
The path of mindfulness and the path of craving the way we look at the world.

118
00:21:39,000 --> 00:21:40,000
It's how we're taught.

119
00:21:40,000 --> 00:21:42,000
It's why we're born.

120
00:21:42,000 --> 00:21:50,000
Because we think this way, we think in terms of what do I want and how can I get it?

121
00:21:50,000 --> 00:21:59,000
Instead of what's happening and what is it?

122
00:21:59,000 --> 00:22:01,000
A mindful way.

123
00:22:01,000 --> 00:22:05,000
A mindful way of seeing things as they are.

124
00:22:05,000 --> 00:22:12,000
What's the cause? What's the result? What's happening right now?

125
00:22:12,000 --> 00:22:18,000
Mindfulness is just being mindfulness is this pure state.

126
00:22:18,000 --> 00:22:21,000
It's actually quite simple.

127
00:22:21,000 --> 00:22:23,000
But for us, it's like this.

128
00:22:23,000 --> 00:22:26,000
The mindfulness has become a real buzzword.

129
00:22:26,000 --> 00:22:29,000
It's something exalted and it is.

130
00:22:29,000 --> 00:22:31,000
But only because we're so wretched.

131
00:22:31,000 --> 00:22:36,000
We're so lost. We're so caught up in the wrong path.

132
00:22:36,000 --> 00:22:40,000
That this simple path, a simple way of being.

133
00:22:40,000 --> 00:22:46,000
For Narahand is just second nature, first nature.

134
00:22:46,000 --> 00:22:51,000
It just seems so far away up on the mountains.

135
00:22:51,000 --> 00:22:56,000
Something we have to climb to the peak to get to.

136
00:22:56,000 --> 00:23:01,000
It's such a foreign concept, which is really our problem.

137
00:23:01,000 --> 00:23:05,000
It's nothing hard to understand about mindfulness.

138
00:23:05,000 --> 00:23:13,000
It's just we're really on the wrong path.

139
00:23:13,000 --> 00:23:19,000
So second, noble truth, the cause of suffering that's craving.

140
00:23:19,000 --> 00:23:24,000
Craving is what leads to suffering.

141
00:23:24,000 --> 00:23:27,000
It's not in line with reality.

142
00:23:27,000 --> 00:23:30,000
It's all about what we want and wish it could be rather than what it is

143
00:23:30,000 --> 00:23:35,000
and how we understand it, how we experience it.

144
00:23:35,000 --> 00:23:40,000
The result that leads to the world that we see around us,

145
00:23:40,000 --> 00:23:48,000
stress, strife, suffering, dissatisfaction.

146
00:23:48,000 --> 00:24:00,000
So that's the demo for tonight. Thank you all for tuning in.

147
00:24:00,000 --> 00:24:03,000
If there are any questions, I'm happy to answer them.

148
00:24:03,000 --> 00:24:07,000
I'm just going to go and see how many.

149
00:24:07,000 --> 00:24:13,000
Why is YouTube still offline?

150
00:24:13,000 --> 00:24:18,000
It still says it's offline.

151
00:24:18,000 --> 00:24:23,000
Oh, because I'm in the wrong person now.

152
00:24:23,000 --> 00:24:26,000
Let's see here.

153
00:24:26,000 --> 00:24:28,000
I think we are online.

154
00:24:28,000 --> 00:24:31,000
Yes, we are online.

155
00:24:31,000 --> 00:24:33,000
I created a new account.

156
00:24:33,000 --> 00:24:36,000
I've been trying to figure out how to do this for a while,

157
00:24:36,000 --> 00:24:39,000
but before I tried before,

158
00:24:39,000 --> 00:24:44,000
so we can create a new channel and have managers.

159
00:24:44,000 --> 00:24:47,000
So I made Robin a manager,

160
00:24:47,000 --> 00:24:54,000
which means more than one person can upload and curate the channel.

161
00:24:54,000 --> 00:25:00,000
So we now have a Siri-Mungal International channel that would be good for putting together playlists.

162
00:25:00,000 --> 00:25:02,000
I mean, I'm not going to do any of that,

163
00:25:02,000 --> 00:25:06,000
but if we can get a group of people, all of our volunteers together,

164
00:25:06,000 --> 00:25:08,000
they can put together playlists,

165
00:25:08,000 --> 00:25:14,000
and we can actually have some organization.

166
00:25:14,000 --> 00:25:20,000
So if anybody wants to volunteer anyone who is anyone who we know and trust,

167
00:25:20,000 --> 00:25:27,000
we should talk about that on stack, maybe.

168
00:25:27,000 --> 00:25:42,000
Forty-seven people watching on YouTube, that's good.

169
00:25:57,000 --> 00:26:00,000
Okay, a bunch of questions tonight.

170
00:26:00,000 --> 00:26:04,000
How does one deal with attachment to family?

171
00:26:04,000 --> 00:26:10,000
I did almost impossible to lose the desire to see my daughter grow in the happy life.

172
00:26:10,000 --> 00:26:14,000
This is a form of desire that will eventually need to be overcome.

173
00:26:14,000 --> 00:26:17,000
It's not a bit overcoming desire.

174
00:26:17,000 --> 00:26:22,000
You eventually see that that desire is not helpful, useful, beneficial to you.

175
00:26:22,000 --> 00:26:32,000
We approach this the wrong way again. I mean, this talk tonight hopefully shed some light on this sort of question.

176
00:26:32,000 --> 00:26:40,000
Your attachment, your desire is in the end.

177
00:26:40,000 --> 00:26:51,000
It's meaningless. It's not useful. It's not helpful.

178
00:26:51,000 --> 00:26:57,000
It's not the desire that makes you a good father or mother.

179
00:26:57,000 --> 00:27:10,000
It's the wisdom and the clarity of mind.

180
00:27:10,000 --> 00:27:21,000
I think it's not easy being a mindful parent and I think in fact it somehow eventually breaks down.

181
00:27:21,000 --> 00:27:27,000
Because you just don't have the capacity to care for it.

182
00:27:27,000 --> 00:27:37,000
It's send them off to school in the morning and get involved with them.

183
00:27:37,000 --> 00:27:45,000
You see, we're dealing with two very different worlds.

184
00:27:45,000 --> 00:27:50,000
The whole reproduction system and I don't mean the reproductive system,

185
00:27:50,000 --> 00:27:59,000
but the reproduction, the whole system of giving birth and raising kids and so on is the wrong way.

186
00:27:59,000 --> 00:28:05,000
It's an artificial form of rebirth that we've created. We're playing a game.

187
00:28:05,000 --> 00:28:08,000
It's not real. You have to get out of this.

188
00:28:08,000 --> 00:28:11,000
We're not dealing with God here.

189
00:28:11,000 --> 00:28:19,000
You have to get out of this idea that this was all preordained by some magnificent,

190
00:28:19,000 --> 00:28:32,000
some beneficent, beneficent, benevolent, benevolent, omnipotent being.

191
00:28:32,000 --> 00:28:36,000
It's us. We've put this all together. We're playing a game.

192
00:28:36,000 --> 00:28:39,000
Okay, now I'll be the kid.

193
00:28:39,000 --> 00:28:45,000
My turn. I'll be your child this time.

194
00:28:45,000 --> 00:28:48,000
And so it breaks down when you start to meditate.

195
00:28:48,000 --> 00:28:55,000
You step back, whoa, you say, this is not the way to happiness.

196
00:28:55,000 --> 00:29:02,000
I think part of our attachment as parents is based on a view of belief in the system,

197
00:29:02,000 --> 00:29:06,000
a belief in the reproduction system, a buying into it.

198
00:29:06,000 --> 00:29:11,000
Yes, I'm the father. I'm the mother. This is my child.

199
00:29:11,000 --> 00:29:20,000
I now am the father, the mother, the parent. This is my child.

200
00:29:20,000 --> 00:29:25,000
It's just all kind of rubbish. It's all just a game we're playing.

201
00:29:25,000 --> 00:29:28,000
It's romantic. It's romance, really.

202
00:29:28,000 --> 00:29:34,000
So this romantic notion doesn't mean much, which is very hard to see.

203
00:29:34,000 --> 00:29:46,000
Again, there's a good example of how very different and how very in opposition these two paths are.

204
00:29:46,000 --> 00:29:50,000
But practically speaking, that's more theoretical.

205
00:29:50,000 --> 00:29:54,000
Practically speaking, it's not so dire or drastic.

206
00:29:54,000 --> 00:29:58,000
But you will find, as you meditate, that you're less attached.

207
00:29:58,000 --> 00:30:02,000
And I think this is going to point out this, that this is the reason.

208
00:30:02,000 --> 00:30:05,000
And the reason is that it's not really meaningful.

209
00:30:05,000 --> 00:30:09,000
You're being a parent. They're being a child.

210
00:30:09,000 --> 00:30:15,000
It's just your romantic notion of how things are.

211
00:30:15,000 --> 00:30:21,000
And, I mean, I guess it sounds easy for me to say,

212
00:30:21,000 --> 00:30:26,000
I've never been married or had children, not in this life.

213
00:30:26,000 --> 00:30:30,000
But I know people who have and who have said the same sorts of things.

214
00:30:30,000 --> 00:30:32,000
When they meditate, they get less.

215
00:30:32,000 --> 00:30:36,000
Like this woman came to me, she wanted me to teach her son to meditate.

216
00:30:36,000 --> 00:30:42,000
She wanted me to help her son. She said, my son, she'd heard about me.

217
00:30:42,000 --> 00:30:47,000
I've been helping out at the time monastery in California,

218
00:30:47,000 --> 00:30:51,000
and she came and said, okay, what's been help her son?

219
00:30:51,000 --> 00:30:55,000
I said, okay, I'll help your son. You come and meditate.

220
00:30:55,000 --> 00:30:58,000
I said, and I tried to explain to her that if you come and meditate,

221
00:30:58,000 --> 00:31:01,000
well, your relationship with him with him will be better.

222
00:31:01,000 --> 00:31:05,000
You'll be better able to help him.

223
00:31:05,000 --> 00:31:08,000
She came and, you know, eventually I convinced her,

224
00:31:08,000 --> 00:31:11,000
and she came and tried it out and eventually finished the course with me.

225
00:31:11,000 --> 00:31:14,000
I saw her later and asked her, how's your son?

226
00:31:14,000 --> 00:31:16,000
He said, I don't, you know, I don't care.

227
00:31:16,000 --> 00:31:17,000
He's going his own way.

228
00:31:17,000 --> 00:31:19,000
He was grown up at the time.

229
00:31:19,000 --> 00:31:22,000
He wasn't a toddler by any means.

230
00:31:22,000 --> 00:31:25,000
And of course, there's Eva in Kinga.

231
00:31:25,000 --> 00:31:31,000
Kinga is this little girl who was conceived during,

232
00:31:31,000 --> 00:31:34,000
well, in between meditation courses.

233
00:31:34,000 --> 00:31:40,000
Sorry, interesting. She was pregnant with this girl when she came into the course with us.

234
00:31:40,000 --> 00:31:44,000
And so she's been raising this girl as a meditator.

235
00:31:44,000 --> 00:31:48,000
And she's described some of the difficulties of it.

236
00:31:48,000 --> 00:31:52,000
But I think some of the power as well, this girl hasn't grown up.

237
00:31:52,000 --> 00:31:56,000
Kinga hasn't grown up like any other girl.

238
00:31:57,000 --> 00:31:59,000
Because her mother was so mindful.

239
00:32:06,000 --> 00:32:09,000
But she's had to struggle with this sort of question and

240
00:32:09,000 --> 00:32:14,000
not really exactly the attachment because in fact, you know, she's noticed that

241
00:32:14,000 --> 00:32:18,000
it's not really attached, but she has this sense of obligation,

242
00:32:18,000 --> 00:32:20,000
which is, I think, reasonable.

243
00:32:23,000 --> 00:32:25,000
It is her responsibility.

244
00:32:25,000 --> 00:32:29,000
And it's her lot in life to look after this girl.

245
00:32:29,000 --> 00:32:31,000
But the girl is just another person.

246
00:32:31,000 --> 00:32:34,000
It's not like it's her daughter.

247
00:32:34,000 --> 00:32:39,000
It's just a person who is under her care.

248
00:32:39,000 --> 00:32:42,000
I don't know if this is all that.

249
00:32:42,000 --> 00:32:44,000
If this is exactly the answer you're looking for.

250
00:32:44,000 --> 00:32:49,000
I think the most important thing for most people to keep in mind is not

251
00:32:49,000 --> 00:32:52,000
to, not that you have to let go.

252
00:32:52,000 --> 00:32:54,000
But that eventually you do let go.

253
00:32:54,000 --> 00:32:57,000
And that's a bold claim that you can test.

254
00:32:57,000 --> 00:33:01,000
You don't have to worry because if you're the only way you'll let go of it

255
00:33:01,000 --> 00:33:05,000
as if you see for yourself without me trying to convince you

256
00:33:05,000 --> 00:33:07,000
that it's not worth clinging to.

257
00:33:07,000 --> 00:33:10,000
If you still see that it's worth clinging to it, there's no way

258
00:33:10,000 --> 00:33:12,000
anyone can make you let go of it.

259
00:33:12,000 --> 00:33:14,000
That's how mindfulness works.

260
00:33:19,000 --> 00:33:23,000
Should formal meditation and daily mindfulness begin to feel similar?

261
00:33:23,000 --> 00:33:24,000
Yes, absolutely.

262
00:33:24,000 --> 00:33:32,000
At the meditation conference, one of the neurologists

263
00:33:32,000 --> 00:33:36,000
stated that mindfulness is done without any energy or effort.

264
00:33:36,000 --> 00:33:40,000
It's not according to our tradition also.

265
00:33:40,000 --> 00:33:42,000
I mean, no, absolutely not.

266
00:33:42,000 --> 00:33:44,000
But you know how I talk about energy.

267
00:33:44,000 --> 00:33:53,000
It's important to differentiate between the forceful energy and the uplifting energy.

268
00:33:53,000 --> 00:33:57,000
It should feel effortless eventually.

269
00:33:57,000 --> 00:34:03,000
But that's because it's like when you're peddling a bicycle, right?

270
00:34:03,000 --> 00:34:10,000
If you want to get up the hill, you've got a pedal really hard.

271
00:34:10,000 --> 00:34:13,000
So the point is we're lazy.

272
00:34:13,000 --> 00:34:15,000
Our default is lazy.

273
00:34:15,000 --> 00:34:20,000
Our default is slothful.

274
00:34:20,000 --> 00:34:26,000
It's either slothful or the energy is focused in the wrong direction.

275
00:34:26,000 --> 00:34:29,000
Some people are too energetic.

276
00:34:29,000 --> 00:34:40,000
But the energy that we have to put out is to give out both the slothfulness

277
00:34:40,000 --> 00:34:47,000
and the distraction and the excitement of too much energy.

278
00:34:47,000 --> 00:34:53,000
So it's not that I wouldn't say that it's done without any energy or effort.

279
00:34:53,000 --> 00:34:58,000
But that, and one's one, is truly mindful that it feels effortless.

280
00:34:58,000 --> 00:35:02,000
It doesn't feel like any special energy need to be exerted.

281
00:35:02,000 --> 00:35:10,000
But that's because one's peddling, one's peddling the bike enough and is now coasting.

282
00:35:10,000 --> 00:35:14,000
That's because one is perfectly tuned, right?

283
00:35:14,000 --> 00:35:18,000
It should feel the most natural thing in the world.

284
00:35:18,000 --> 00:35:23,000
It really is.

285
00:35:23,000 --> 00:35:28,000
Lately, no thoughts, images or emotions arise to note.

286
00:35:28,000 --> 00:35:31,000
There is only the object of meditation, the abdomen.

287
00:35:31,000 --> 00:35:34,000
There is his experiment, experience is impermanent.

288
00:35:34,000 --> 00:35:36,000
I wonder if this is a problem.

289
00:35:36,000 --> 00:35:38,000
Well, if you're wondering, then that's something you can say,

290
00:35:38,000 --> 00:35:43,000
wondering, wondering, worried, worried, concerned.

291
00:35:43,000 --> 00:35:47,000
If you feel quiet or calm, you should note that.

292
00:35:47,000 --> 00:35:52,000
Calm, calm or quiet, quiet.

293
00:35:52,000 --> 00:35:54,000
Neutral feelings are still feelings.

294
00:35:54,000 --> 00:35:56,000
It's a feeling of calm.

295
00:35:56,000 --> 00:35:59,000
But no, it's not a problem.

296
00:35:59,000 --> 00:36:00,000
It's impermanence.

297
00:36:00,000 --> 00:36:03,000
You're seeing something new.

298
00:36:03,000 --> 00:36:04,000
It wasn't like this before.

299
00:36:04,000 --> 00:36:05,000
Now it's like this.

300
00:36:05,000 --> 00:36:10,000
You're seeing how things can change.

301
00:36:10,000 --> 00:36:14,000
The question in regards to the first novel truth is being in the womb before birth

302
00:36:14,000 --> 00:36:21,000
and the nourishment that it's fed to the fetus have an impact on the mind-body experience throughout life.

303
00:36:21,000 --> 00:36:25,000
Maybe it's absolutely.

304
00:36:25,000 --> 00:36:30,000
In fact, you might say those are even more important because they're formative.

305
00:36:30,000 --> 00:36:33,000
That's when the physical form is being formed.

306
00:36:33,000 --> 00:36:40,000
It's interesting to look into how the mind, the baby's mind,

307
00:36:40,000 --> 00:36:45,000
the experiences of the baby in the womb,

308
00:36:45,000 --> 00:36:50,000
the impact of those experiences on one's life.

309
00:36:50,000 --> 00:36:55,000
On the formation of the fetus, the formation of the physical body,

310
00:36:55,000 --> 00:36:59,000
if the mind is constantly being stressed, I would bet

311
00:36:59,000 --> 00:37:04,000
that there would be changes, probably not huge,

312
00:37:04,000 --> 00:37:11,000
but there would be changes to the physicality of the baby because it's very formative.

313
00:37:11,000 --> 00:37:14,000
It's an interesting thought.

314
00:37:14,000 --> 00:37:18,000
I don't have any evidence or of that,

315
00:37:18,000 --> 00:37:28,000
but it seems quite, quite likely.

316
00:37:28,000 --> 00:37:36,000
Of course, there's much that is already decided of the physical DNA from the parents,

317
00:37:36,000 --> 00:37:42,000
and that one has already made a determination to get involved in,

318
00:37:42,000 --> 00:37:49,000
to live in, is already quite fixed.

319
00:37:49,000 --> 00:37:55,000
So it's not going to be huge changes,

320
00:37:55,000 --> 00:37:58,000
but I bet there would be.

321
00:37:58,000 --> 00:38:01,000
Suffering from weight, often eat very much,

322
00:38:01,000 --> 00:38:05,000
and often in the healthy things, I also smoke and drink alcohol,

323
00:38:05,000 --> 00:38:07,000
and these things give me an option to escape the emptiness.

324
00:38:07,000 --> 00:38:11,000
What can I do? When I meditate and I reach pleasant condition,

325
00:38:11,000 --> 00:38:15,000
I then have thoughts, but this condition too is perishable.

326
00:38:15,000 --> 00:38:19,000
What can one cling to when everything is transient?

327
00:38:19,000 --> 00:38:22,000
Well, one doesn't cling, right?

328
00:38:22,000 --> 00:38:28,000
This is, again, this talk today, I think, is what you need as an answer.

329
00:38:28,000 --> 00:38:35,000
The whole way you've looking at the world of clinging, of trying to change,

330
00:38:35,000 --> 00:38:39,000
that the emptiness is a problem, that loneliness is a problem,

331
00:38:39,000 --> 00:38:43,000
that depression is a problem, they're not problems.

332
00:38:43,000 --> 00:38:46,000
That weight is a problem.

333
00:38:46,000 --> 00:38:52,000
Weight is, you know, what is weight? Weight is completely relative.

334
00:38:52,000 --> 00:38:56,000
I'm sure you weigh less than a great blue whale,

335
00:38:56,000 --> 00:39:00,000
and I'm sure that if you were up on, if you were on Mars,

336
00:39:00,000 --> 00:39:03,000
you would weigh a lot less.

337
00:39:03,000 --> 00:39:05,000
You don't have a problem with weight.

338
00:39:05,000 --> 00:39:08,000
Weight is not a problem.

339
00:39:08,000 --> 00:39:12,000
You could say that eating too much is a problem, but it's not really.

340
00:39:12,000 --> 00:39:16,000
The problem is craving.

341
00:39:16,000 --> 00:39:18,000
The problem is the way you look at the world.

342
00:39:18,000 --> 00:39:22,000
You look at the world in terms of what do I want and how can I get it?

343
00:39:22,000 --> 00:39:26,000
That's the wrong paradigm.

344
00:39:26,000 --> 00:39:34,000
Okay.

345
00:39:34,000 --> 00:39:36,000
My English is terrible.

346
00:39:36,000 --> 00:39:47,000
For putting up the effort.

347
00:39:47,000 --> 00:39:49,000
So, I hope that helped.

348
00:39:49,000 --> 00:39:52,000
That's all the questions for tonight.

349
00:39:52,000 --> 00:40:14,000
Thank you all for tuning in.

