1
00:00:00,000 --> 00:00:06,360
Okay, next question comes from Ubasika Sujata.

2
00:00:06,360 --> 00:00:14,240
Can you explain briefly how is the understanding of Sunyata in Teravada Buddhism?

3
00:00:14,240 --> 00:00:17,400
Sunyata means emptiness.

4
00:00:17,400 --> 00:00:25,240
Emptiness is a word that is, I'd like to say, thrown around a lot in Buddhism.

5
00:00:25,240 --> 00:00:33,120
And I really believe that's an appropriate way of putting it because a lot of our understanding

6
00:00:33,120 --> 00:00:37,840
of Sunyata is quite superficial.

7
00:00:37,840 --> 00:00:43,200
We understand Sunyata to be an experience where you'll be sitting in meditation and suddenly

8
00:00:43,200 --> 00:00:47,520
everything goes blank or it suddenly is very empty.

9
00:00:47,520 --> 00:00:53,800
You feel very peaceful, very calm, there's no suffering arising and so on.

10
00:00:53,800 --> 00:00:57,600
People say that therefore you've come to see emptiness and that's not the case, that's

11
00:00:57,600 --> 00:00:59,720
not true at all.

12
00:00:59,720 --> 00:01:08,840
This feeling of emptiness, you know, nothingness or vastness or so on is a very well-documented

13
00:01:08,840 --> 00:01:15,760
Buddhist meditation experience and it can actually be a hindrance to true development of

14
00:01:15,760 --> 00:01:22,200
insight because for this very reason it leads you to believe that you've become enlightened,

15
00:01:22,200 --> 00:01:27,320
you've attained some sort of a special experience and that therefore there's nothing left

16
00:01:27,320 --> 00:01:29,640
for you to do.

17
00:01:29,640 --> 00:01:37,640
Which of course isn't the case, it's a formed state, it has causes, it's based on the

18
00:01:37,640 --> 00:01:43,520
concentration that you gain in meditation, it comes about because of the concentration

19
00:01:43,520 --> 00:01:49,240
and it lasts as long as the power of concentration is still there.

20
00:01:49,240 --> 00:01:54,960
So you can build it, you can develop it but it doesn't lead you to a permanent and lasting

21
00:01:54,960 --> 00:01:56,360
peace and happiness.

22
00:01:56,360 --> 00:02:04,080
There's of course that can only be gained through realization, through understanding the

23
00:02:04,080 --> 00:02:06,800
objective reality in front of you.

24
00:02:06,800 --> 00:02:14,680
So I would say 90% of our experience of what we term to be nothingness is actually nothing

25
00:02:14,680 --> 00:02:20,520
of the or to be emptiness is actually nothing of the sort, it's just a temporary state

26
00:02:20,520 --> 00:02:27,200
which is impermanent, which is unsatisfying, which is not under our control and it's

27
00:02:27,200 --> 00:02:30,600
not belong to the self.

28
00:02:30,600 --> 00:02:35,720
And actually these three characteristics are what we're really trying to gain in Buddhism,

29
00:02:35,720 --> 00:02:45,080
it's not any special state of bliss or quiet tune of mind or so and we're trying to

30
00:02:45,080 --> 00:02:49,480
come to understand things as they are in terms of these three things and this is where

31
00:02:49,480 --> 00:02:58,560
emptiness really plays a part because impermanence, suffering or what we call the unsatisfactory

32
00:02:58,560 --> 00:03:05,280
nature of all things that nothing can satisfy you and non-self for the fact that nothing

33
00:03:05,280 --> 00:03:12,880
is under your control, nothing really is subject to your domination.

34
00:03:12,880 --> 00:03:22,720
These are the three ways of becoming free from suffering of gaining realization of the

35
00:03:22,720 --> 00:03:28,480
truth of reality and the realization that comes from each of these three has a specific

36
00:03:28,480 --> 00:03:29,480
term.

37
00:03:29,480 --> 00:03:34,000
So the realization of enlightenment can come through one of three ways and one is through

38
00:03:34,000 --> 00:03:38,240
impermanence, the others are suffering, the third is through non-self.

39
00:03:38,240 --> 00:03:45,520
The gaining of realization through impermanence is called animita manga, the path through

40
00:03:45,520 --> 00:03:53,360
seeing the sinlessness, nature of phenomena, that things change without warning.

41
00:03:53,360 --> 00:04:00,600
There is no seeing that everything comes and goes without any previous warning, without

42
00:04:00,600 --> 00:04:06,920
any sign, it can change in a heartbeat and we see this in our lives, our lives change very

43
00:04:06,920 --> 00:04:14,160
rapidly in many cases, you'll be living your life in a certain way and suddenly everything

44
00:04:14,160 --> 00:04:18,280
changes, suddenly you lose something in a heartbeat.

45
00:04:18,280 --> 00:04:24,680
In meditation we see this acutely that everything is changing and things can change in a heartbeat.

46
00:04:24,680 --> 00:04:31,280
When we see this, this can be a cause for the mind to let go for realization of the truth.

47
00:04:31,280 --> 00:04:35,760
The realization through suffering is to see that nothing is worth clinging to, it's

48
00:04:35,760 --> 00:04:43,240
called apanyita manga, the path through seeing or through non-desiring or through seeing

49
00:04:43,240 --> 00:04:47,600
the non-desirability of phenomena.

50
00:04:47,600 --> 00:04:53,600
When you see that things are changing all the time, you start to see that they're unsatisfying.

51
00:04:53,600 --> 00:04:59,640
Some people see this aspect of this stress involved with clinging, this stress involved with

52
00:04:59,640 --> 00:05:10,160
waiting, with expecting, with trying to chase after a certain mode of existence and suffering

53
00:05:10,160 --> 00:05:14,720
thereby because it's impermanent, it's not stable.

54
00:05:14,720 --> 00:05:16,880
This is the second path, the second path through realization.

55
00:05:16,880 --> 00:05:22,200
The third path through realization is through non-self, is seeing the uncontrollability of

56
00:05:22,200 --> 00:05:28,600
things and this is termed sunyata manga, which is the path through emptiness.

57
00:05:28,600 --> 00:05:33,600
The real meaning of emptiness is not a feeling of everything is empty or there's nothing

58
00:05:33,600 --> 00:05:34,600
or so much.

59
00:05:34,600 --> 00:05:39,080
It's a fine meditation state but it's not realization of the truth.

60
00:05:39,080 --> 00:05:43,000
Emptiness is the realization that everything, every experience is empty and it's empty

61
00:05:43,000 --> 00:05:50,600
of self, it's empty of being, it's not an entity in and of itself.

62
00:05:50,600 --> 00:05:53,680
It comes and it goes and arises and it ceases.

63
00:05:53,680 --> 00:06:03,280
There's no solidity to it, everything is ephemeral, everything changes and does so outside

64
00:06:03,280 --> 00:06:08,040
of the control of any creator, any self.

65
00:06:08,040 --> 00:06:12,840
The mind is able to intervene, is able to alter the course but it's not able to turn things

66
00:06:12,840 --> 00:06:19,720
off or to make a leap from one state of being to another to say, I'm like this and suddenly

67
00:06:19,720 --> 00:06:21,840
change to be like that.

68
00:06:21,840 --> 00:06:28,360
The mind is only able to affect things in terms of its judgment and in terms of its desires

69
00:06:28,360 --> 00:06:30,600
when we want things to be a certain way.

70
00:06:30,600 --> 00:06:35,920
There's a certain power to that that leads us in that direction but as we can see in

71
00:06:35,920 --> 00:06:40,960
the meditation, things like pain, we can't just turn it off, things like the rising and

72
00:06:40,960 --> 00:06:44,360
the falling of the abdomen, we can't force it to be smooth all the time.

73
00:06:44,360 --> 00:06:48,160
In fact, the more we try to force it, we see the more we suffer as with the pain, as

74
00:06:48,160 --> 00:06:52,960
with our thoughts, trying to control our minds, trying to control our emotions, the more

75
00:06:52,960 --> 00:06:56,560
we try to control things, we see the more suffering we have.

76
00:06:56,560 --> 00:07:03,160
Once you start to see things as coming and going on their own, rising when they want,

77
00:07:03,160 --> 00:07:07,400
seeing when they want, the rising and falling of the abdomen, going of its own accord,

78
00:07:07,400 --> 00:07:10,240
not according to our wishes.

79
00:07:10,240 --> 00:07:15,840
This is the realization of Sunyatta, there really is no self involved, there is no control

80
00:07:15,840 --> 00:07:22,080
involved that they are empty of any being.

81
00:07:22,080 --> 00:07:30,080
This is the third path, so this is where emptiness plays a part in the meditation practice.

82
00:07:30,080 --> 00:07:36,120
On a more theoretical level, emptiness can be used to describe all three paths and in

83
00:07:36,120 --> 00:07:43,600
fact all aspects of reality, that all of reality is empty of four things and this is

84
00:07:43,600 --> 00:07:52,600
the meaning of kandha, kandha, which means aggregate, so aggregate meaning reality is made

85
00:07:52,600 --> 00:07:59,520
up of different parts, there is the form aggregate which is physical, there is the feeling

86
00:07:59,520 --> 00:08:06,000
aggregate which is the part of reality that is the pain, happiness and calm, there is

87
00:08:06,000 --> 00:08:11,680
the recognition aggregate, the part of reality that remembers things, recognizes things

88
00:08:11,680 --> 00:08:18,320
for being this and that, there is the thought part where we not the judgment part, what

89
00:08:18,320 --> 00:08:23,200
we think of things, when we remember something, when we recognize something, how we react

90
00:08:23,200 --> 00:08:27,880
to it, what we think of it and then there is the consciousness aggregate, which is the part

91
00:08:27,880 --> 00:08:33,040
of reality that knows that is aware, right now that you are aware of me speaking to you,

92
00:08:33,040 --> 00:08:37,040
the sound and the sight and so on.

93
00:08:37,040 --> 00:08:44,480
All of these five aggregates, the meaning of the word aggregate, or kandha means kang sum

94
00:08:44,480 --> 00:08:54,540
yang tardeti, kandha means all of these aggregates are empty of desirability, empty of

95
00:08:54,540 --> 00:09:04,240
permanence, empty of self and so on, they are empty of any meaning every part of reality

96
00:09:04,240 --> 00:09:10,320
empty of any reason to cling to it or any benefit from clinging to it and saying

97
00:09:10,320 --> 00:09:14,800
this is going to make me happy when it comes and this is going to make I'm going to be happy

98
00:09:14,800 --> 00:09:21,920
when this goes. So the dependency on things that are impermanent and satisfying and uncontrollable,

99
00:09:22,640 --> 00:09:29,280
that from my understanding is how emptiness plays a part in the teachings of the Buddha.

100
00:09:29,280 --> 00:09:39,280
So thanks for the question.

