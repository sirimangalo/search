1
00:00:00,000 --> 00:00:12,320
So sometimes you'll read in the sutas that the Buddha will say something like you know how to bring

2
00:00:12,320 --> 00:00:18,080
about wholesome things you know it causes wholesome things and makes them arise and make

3
00:00:18,080 --> 00:00:22,840
some stay around wholesome things you know how what makes them arise you know what makes

4
00:00:22,840 --> 00:00:33,880
them stay you know make them stay you know how to manipulate the states I guess how do we do

5
00:00:33,880 --> 00:00:49,080
that in our practice well it all starts really with mindfulness so I mean it can start with

6
00:00:49,080 --> 00:00:55,560
so many different things it can start with generosity it can start with morality it can

7
00:00:55,560 --> 00:01:07,480
start with meditation practice concentration but no matter what in the end it has to come

8
00:01:07,480 --> 00:01:13,240
down to mindfulness the development of wholesomeness I think if you read there's somewhere

9
00:01:13,240 --> 00:01:19,640
in the sutas where the Buddha says if you're not if you don't mention mindfulness you haven't

10
00:01:19,640 --> 00:01:28,840
complete you haven't really discussed wholesomeness no conversation about wholesomeness is complete

11
00:01:29,400 --> 00:01:34,360
without including the meaning of the word mindfulness or without including the word mindfulness

12
00:01:34,360 --> 00:01:44,360
or set date which so you said it doesn't actually mean mindfulness this this straightening of

13
00:01:44,360 --> 00:01:50,440
the mind this is what really with the Buddha is talking about the removing of the unwholesomeness

14
00:01:50,440 --> 00:01:54,760
the creation of the wholesomeness is the straightening of the mind really and everything we do

15
00:01:55,640 --> 00:01:59,880
giving charity is for the purpose of straightening the mind if it's for another purpose then it's

16
00:01:59,880 --> 00:02:07,560
not really not really the Buddha's teaching if morality is for the purpose of straightening the

17
00:02:07,560 --> 00:02:11,720
mind if it's for some other purpose then it's not really the Buddha's teaching because these

18
00:02:11,720 --> 00:02:18,760
things can of course lead to conceit can lead to attachment if done for other reasons and

19
00:02:18,760 --> 00:02:30,840
set date the remembrance or the reminding oneself the focusing in on the ultimate reality and the

20
00:02:30,840 --> 00:02:38,680
actual true nature of experience that is the straightening of the mind that's the act of

21
00:02:38,680 --> 00:02:44,600
straightening the mind which is really what's meant by that it it's important not to think that

22
00:02:44,600 --> 00:02:53,640
we're trying to create something right that we're trying to build up a sankara this is important

23
00:02:53,640 --> 00:03:02,760
to understand a sankara is something that is going to lead you away from from truth and clarity

24
00:03:02,760 --> 00:03:13,000
and understanding we're trying to bring ourselves back to a non creative process where

25
00:03:13,000 --> 00:03:21,960
we interact with reality rather than react and we don't build concepts and projections based

26
00:03:21,960 --> 00:03:30,200
on the reality we simply live with reality exist with it so we're not trying to in that sense

27
00:03:31,160 --> 00:03:39,720
build wholesomeness or actively work to become a kinder person or a nicer person or a wiser person

28
00:03:39,720 --> 00:03:46,280
you can do all these things but it ends up just being a sankara and it ends up just covering up the

29
00:03:46,280 --> 00:03:52,680
unwholesomeness because the mind is not straight but once the mind is straight then you can pile

30
00:03:52,680 --> 00:03:57,960
whatever you want on it and it's just it's all going to be straight you can practice loving

31
00:03:57,960 --> 00:04:05,000
kindness and it will be pure loving kindness you can study and you can learn about reality and

32
00:04:05,000 --> 00:04:10,040
it will all be pure understanding pure learning you won't cling to it with conceit and so on

33
00:04:12,520 --> 00:04:20,760
so I think simply it comes down to being mindful which is the straightening of the mind and that's

34
00:04:21,480 --> 00:04:29,720
really the true development of wholesomeness that being said we often miss that

35
00:04:29,720 --> 00:04:37,720
wholesomeness or this straightening can and should be augmented by things like

36
00:04:37,720 --> 00:04:44,440
Donna, Charity, See, Limorality, obviously meditation practice there are some people who believe

37
00:04:44,440 --> 00:04:48,840
that mindfulness is something that you don't meditate about they actually think

38
00:04:49,720 --> 00:04:54,920
it's not meditation so they won't practice meditation they'll try to develop mindfulness in their

39
00:04:54,920 --> 00:05:02,760
daily life totally in their daily life or in regards to their everyday activities people who

40
00:05:02,760 --> 00:05:10,360
believe that it's Anatai heard one very famous teacher in Thailand say Sati is Anatai it's

41
00:05:10,360 --> 00:05:15,880
non-self so you can't force it to come about you can't work to bring it about you just have to

42
00:05:15,880 --> 00:05:20,680
sit there with near him and he says this sit near me and you'll develop mindfulness naturally

43
00:05:20,680 --> 00:05:26,200
just very dangerous I think because it is possible that a person could become more mindful staying

44
00:05:26,200 --> 00:05:35,480
around mindful people but it obviously comes much slower because the person isn't actively

45
00:05:35,480 --> 00:05:42,760
developing it so all of these things are helpful and supportive especially in the beginning

46
00:05:43,720 --> 00:05:49,000
to the development of mindfulness a person should practice wholesomeness should develop

47
00:05:49,000 --> 00:05:58,440
putting it or goodness should do good deeds should help people should be kind and generous

48
00:05:58,440 --> 00:06:05,320
and so on and moral and focused and concentrated because all of things these things will be a

49
00:06:05,320 --> 00:06:10,520
support for the building of your mindfulness you should sit down and practice meditation if you don't

50
00:06:11,000 --> 00:06:15,400
you could theoretically build up mindfulness but it's not nearly going to be on the same level

51
00:06:15,400 --> 00:06:31,400
as if you do actively you know create the the the requisite or the the environment for the

52
00:06:31,400 --> 00:06:50,840
arising of mindfulness I remember when you said that Nagazena that some of Ayama is the right effort

53
00:06:50,840 --> 00:07:04,040
is the development no the elimination of the of the unhold some thoughts that have not yet arisen

54
00:07:05,000 --> 00:07:20,520
and they are making sure that they won't arise again and the development of the

55
00:07:20,520 --> 00:07:30,120
wholesome mind states and the maintenance of wholesome mind states that are have arisen already

