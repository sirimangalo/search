1
00:00:00,000 --> 00:00:08,400
Hi, so the next question is from someone in Los Angeles, California and the first question

2
00:00:08,400 --> 00:00:16,720
is what do you need, what does it need to be a Buddhist and is a monk life difficult?

3
00:00:16,720 --> 00:00:25,240
To be a Buddhist, traditionally you would take the Buddha, his teachings and the teachers

4
00:00:25,240 --> 00:00:29,200
are the followers of the Buddha who have become enlightened after him and who have passed

5
00:00:29,200 --> 00:00:31,800
on the teachings as your refuge.

6
00:00:31,800 --> 00:00:40,720
You would consider those three things to be your idol or your role model and that those

7
00:00:40,720 --> 00:00:45,320
three things would then be the leaders in your life and the three things which you would

8
00:00:45,320 --> 00:00:49,880
look up to and look for guidance in the same way as you would with any religion.

9
00:00:49,880 --> 00:00:54,640
So in the Buddhist religion, the thing that you look up to or you look to for guidance

10
00:00:54,640 --> 00:01:01,640
and assistance and as a model around which to shape your life is the Buddha, his teachings

11
00:01:01,640 --> 00:01:05,800
and the people who have passed on his teachings.

12
00:01:05,800 --> 00:01:11,400
And there are various ceremonies by which you one could do that, where you do the chanting,

13
00:01:11,400 --> 00:01:15,320
you chant, I take the Buddha as my refuge, I take the Dhamma as my refuge, I take the

14
00:01:15,320 --> 00:01:20,920
Dhamma as my refuge and so on. The other thing that traditionally you should do as a Buddhist

15
00:01:20,920 --> 00:01:26,680
is take the first step towards following the Buddha's teaching and that is to keep the five

16
00:01:26,680 --> 00:01:31,880
moral precepts. It's considered that if you're not following these five precepts, you can't

17
00:01:31,880 --> 00:01:36,560
really be considered to be a follower of the Buddha no matter how much you might pretend

18
00:01:36,560 --> 00:01:43,320
to be and the five are not to kill, not to steal. The kill means killing any animal life,

19
00:01:43,320 --> 00:01:51,800
not to steal, not to cheat in terms of committing adultery or extramarital affairs and so on.

20
00:01:51,800 --> 00:01:57,200
Not extramarital breaking up someone's relationship, either your own relationship or somebody

21
00:01:57,200 --> 00:02:05,800
else's for romantic reasons, lying and taking drugs or alcohol. So if you can do those

22
00:02:05,800 --> 00:02:11,360
two things, taking these three refuges and keeping the five precepts, you'd be considered

23
00:02:11,360 --> 00:02:15,920
to be a Buddha and there are ceremonies for doing that, maybe one day I'll post a ceremony

24
00:02:15,920 --> 00:02:22,280
that people could use to follow along. The second thing is a monk's life difficult. Yes,

25
00:02:22,280 --> 00:02:29,080
a monk's life is very difficult. The monks have 227 official rules that they have to follow

26
00:02:29,080 --> 00:02:37,040
as opposed to just the five and a whole slew of other rules that are official but are

27
00:02:37,040 --> 00:02:42,800
not mentioned in the list. They're in the books and you have to go and read and understand

28
00:02:42,800 --> 00:02:47,640
them. I mean things like opening the door while you're holding your arms ball. This is against

29
00:02:47,640 --> 00:02:54,480
the rule. You know, lots of different things. When you wear your robes, you have to put a

30
00:02:54,480 --> 00:03:01,040
mark on them first. If you're not wearing a robe that is clean, that has no stain on it,

31
00:03:01,040 --> 00:03:09,840
you break a roll. There's hundreds of them. I would suppose that's the really it. You've

32
00:03:09,840 --> 00:03:19,160
got a lot of rules and so your life is very restricted in that sense. I mean, it's like

33
00:03:19,160 --> 00:03:23,940
a soldier's life. You ask yourself, is it difficult being a soldier? Yes, it is. But the

34
00:03:23,940 --> 00:03:29,680
benefit is that you're able to go out and do away with all of the enemies, all of the

35
00:03:29,680 --> 00:03:36,480
bad guys. So the benefit of being a monk is that your life is so restrictive that you have

36
00:03:36,480 --> 00:03:42,960
the power and the focus to do away with all of the bad things in the mind to be able

37
00:03:42,960 --> 00:03:48,600
to do away with all of your inner enemies, all of the things that are causing you suffering.

38
00:03:48,600 --> 00:03:54,320
I think a lot of people misunderstand the monk's life, simply from not really having

39
00:03:54,320 --> 00:03:58,920
much connection with it. It's something that's very difficult. It takes a lot of practice.

40
00:03:58,920 --> 00:04:04,320
The real reason why I'm doing it is I thought in the beginning that it would be a wonderful

41
00:04:04,320 --> 00:04:09,040
challenge. I thought this is something that is not easy because there were other things

42
00:04:09,040 --> 00:04:14,080
in my life that seemed really easy and I usually gave them up because they seemed to be

43
00:04:14,080 --> 00:04:17,680
so simple that they weren't going to lead me anywhere. But this was really challenged

44
00:04:17,680 --> 00:04:22,600
me to the core of my being. Being a monk has been the most challenging thing that I've

45
00:04:22,600 --> 00:04:30,040
ever undertaken besides, of course, the meditation practice itself. And they go hand in hand

46
00:04:30,040 --> 00:04:35,400
being a monk. If you do it properly, it's quite beneficial towards the meditation practice

47
00:04:35,400 --> 00:04:40,800
because you live alone. You're surrounded by the dhamma. You're surrounded by the Buddha's

48
00:04:40,800 --> 00:04:48,440
teaching. I guess in that sense it's not difficult. It's difficult in that it's very difficult

49
00:04:48,440 --> 00:04:55,640
to stay on track and to not fall away from the monk's life. But it's really easy in another

50
00:04:55,640 --> 00:05:03,960
sense to be forced into the right path because you've got so many checks and balances

51
00:05:03,960 --> 00:05:15,240
stopping you from going off the path. As long as you can keep it up, as long as you can

52
00:05:15,240 --> 00:05:21,000
correct all of the misbehavior that is going on in your mind when you want to go off

53
00:05:21,000 --> 00:05:25,880
the path, it's very easy to see what's right and what's wrong. It's not as confusing

54
00:05:25,880 --> 00:05:33,240
or as it's also not as tired as the lay life where you have to engage in activities

55
00:05:33,240 --> 00:05:38,280
which are often meaningless. You have to work for something that you might not believe

56
00:05:38,280 --> 00:05:46,200
in. You have to do work which has no end result except allowing you to survive and run

57
00:05:46,200 --> 00:05:51,240
the rat race or so on. There's a lot of distraction and a lot of getting on the path, getting

58
00:05:51,240 --> 00:05:58,120
off the path in the lay life as an ordinary person. As a monk, you're forced on the path.

59
00:05:58,120 --> 00:06:08,120
It's either get on the path or get off. I hope that helps. I hope those are serious

60
00:06:08,120 --> 00:06:12,760
questions and there's my answer.

