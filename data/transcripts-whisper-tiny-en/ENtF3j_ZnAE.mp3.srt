1
00:00:00,000 --> 00:00:05,000
Sir, I have failed to understand why people are born with physical disability.

2
00:00:05,000 --> 00:00:09,000
Are they born to suffer throughout their life?

3
00:00:09,000 --> 00:00:13,000
But why?

4
00:00:13,000 --> 00:00:15,000
Why?

5
00:00:15,000 --> 00:00:18,000
Why is an interesting question as well?

6
00:00:18,000 --> 00:00:23,000
I mean, one answer you can make is that it really doesn't matter why.

7
00:00:23,000 --> 00:00:26,000
It is the case that they're born with disabilities, no?

8
00:00:26,000 --> 00:00:34,000
The whole thing of karma is interesting, and it is important.

9
00:00:34,000 --> 00:00:39,000
But you have to understand, and this is something interesting here,

10
00:00:39,000 --> 00:00:42,000
because we always talk about how the Buddha taught karma.

11
00:00:42,000 --> 00:00:47,000
Then there are the Buddhists who claim that the Buddha didn't teach karma,

12
00:00:47,000 --> 00:00:53,000
but it was just because that was what the people in the society around him taught.

13
00:00:53,000 --> 00:00:55,000
I think that's a silly idea.

14
00:00:55,000 --> 00:00:58,000
Of course, the Buddha believed in karma, because of course it happens,

15
00:00:58,000 --> 00:01:01,000
because of course from life to life these things carry over.

16
00:01:01,000 --> 00:01:02,000
We don't really know how it works.

17
00:01:02,000 --> 00:01:06,000
We have the Buddha's words on how it works, and we have an idea,

18
00:01:06,000 --> 00:01:09,000
and the more we practice meditation, the clearer that idea becomes like,

19
00:01:09,000 --> 00:01:14,000
if I keep these mind states, I'm going to be a lot of suffering following it.

20
00:01:14,000 --> 00:01:19,000
If I cultivate these mind states, and if I cultivate these tendencies,

21
00:01:19,000 --> 00:01:22,000
I'm going to suffer horribly because of these.

22
00:01:22,000 --> 00:01:26,000
But we don't really know, you know, this person is born this way,

23
00:01:26,000 --> 00:01:28,000
because of this, and this person is born that way.

24
00:01:28,000 --> 00:01:30,000
This is what they did in the past.

25
00:01:30,000 --> 00:01:32,000
You have to know exactly how experience works.

26
00:01:32,000 --> 00:01:36,000
You have to be able to see what's going on in their mind at every moment,

27
00:01:36,000 --> 00:01:40,000
and see where that all came from.

28
00:01:40,000 --> 00:01:45,000
Where their mind was when they died, and what they had done in their past.

29
00:01:45,000 --> 00:01:47,000
It was a very difficult thing.

30
00:01:47,000 --> 00:01:53,000
People just said, no one can do it except a Buddha.

31
00:01:53,000 --> 00:02:00,000
But whether the Buddha taught this idea of past life, future life, or so on,

32
00:02:00,000 --> 00:02:05,000
what the Buddha was trying to impress upon people is the present moment.

33
00:02:05,000 --> 00:02:08,000
A Dita, Nana, Nana, Gamae, don't go back to the past.

34
00:02:08,000 --> 00:02:11,000
Napati, Gankay, and Nagatang, don't worry about the future.

35
00:02:11,000 --> 00:02:14,000
Whatever comes up in the present moment, the thought that we pass it to.

36
00:02:14,000 --> 00:02:17,000
What's in the past is gone, what's in the future.

37
00:02:17,000 --> 00:02:19,000
It's not coming out.

38
00:02:19,000 --> 00:02:21,000
See whatever arises clearly.

39
00:02:21,000 --> 00:02:24,000
We pass it to you as the same word as we pass the night.

40
00:02:24,000 --> 00:02:25,000
It means insight.

41
00:02:25,000 --> 00:02:27,000
See with insight.

42
00:02:27,000 --> 00:02:30,000
That which arises in the present moment.

43
00:02:30,000 --> 00:02:40,000
So one way to answer it, but is to say, it is what it is.

44
00:02:40,000 --> 00:02:45,000
The question of why they're born that way is not so important.

45
00:02:45,000 --> 00:02:52,000
The idea of having some meaning or some purpose, or having a why.

46
00:02:52,000 --> 00:02:53,000
The why is not so important.

47
00:02:53,000 --> 00:02:56,000
The question is what you're going to do with it.

48
00:02:56,000 --> 00:03:00,000
What you're going to get out of that life.

49
00:03:00,000 --> 00:03:08,000
Because in Buddhism, we have no idea of, in fact, an important thing to mention is this idea of purpose.

50
00:03:08,000 --> 00:03:12,000
You think everything, we hear that everything happens for a purpose.

51
00:03:12,000 --> 00:03:17,000
And there must be some meaning that there's a lesson to be learned.

52
00:03:17,000 --> 00:03:23,000
That you learn from these things or these things, kind of like are given to you by a deity.

53
00:03:23,000 --> 00:03:29,000
Of course, in deistic religions or theistic religions, there's the belief that these conditions

54
00:03:29,000 --> 00:03:36,000
were imposed upon us for some ulterior purpose, some greater purpose that we might learn something.

55
00:03:36,000 --> 00:03:38,000
But we don't have that in Buddhism.

56
00:03:38,000 --> 00:03:40,000
You are in charge of your destiny.

57
00:03:40,000 --> 00:03:43,000
You lead yourself to be born as a cripple.

58
00:03:43,000 --> 00:03:45,000
However, you got there, you got there.

59
00:03:45,000 --> 00:03:47,000
That's where you are right now.

60
00:03:47,000 --> 00:03:52,000
A person who is born with physical disabilities has no one to blame with themselves.

61
00:03:52,000 --> 00:03:53,000
I don't know.

62
00:03:53,000 --> 00:03:56,000
I think people have a hard time hearing that.

63
00:03:56,000 --> 00:03:59,000
And they're blaming the victim or something like that.

64
00:03:59,000 --> 00:04:04,000
But if you think of things scientifically, the universe is what it is.

65
00:04:04,000 --> 00:04:11,000
We have experience, experience does exist, and our whole of reality is based on this experience.

66
00:04:11,000 --> 00:04:16,000
So, what could our future come from, except for our experience?

67
00:04:16,000 --> 00:04:22,000
Whether it is true that we're blaming the victims and victims of crimes and victims of great suffering.

68
00:04:22,000 --> 00:04:25,000
And we blame them saying, you know, it's just their karma.

69
00:04:25,000 --> 00:04:29,000
It's not because it's also the karma of the person inflicting the crimes.

70
00:04:29,000 --> 00:04:36,000
But we get ourselves into these situations if we didn't act in a certain way.

71
00:04:36,000 --> 00:04:38,000
We wouldn't find ourselves in these situations.

72
00:04:38,000 --> 00:04:45,000
And this isn't fatalistic because the most important thing is not how we got here

73
00:04:45,000 --> 00:04:48,000
or where we find ourselves or how we find ourselves.

74
00:04:48,000 --> 00:04:52,000
It's what we do with it and where we go from here.

75
00:04:52,000 --> 00:04:57,000
If you're a cripple person, maybe walking meditation isn't possible.

76
00:04:57,000 --> 00:05:00,000
Certainly, the development of goodness is possible.

77
00:05:00,000 --> 00:05:09,000
There's the story of what's his name, Mata Kundalini, this boy who couldn't even raise his hands

78
00:05:09,000 --> 00:05:11,000
even to pay respect to the Buddha.

79
00:05:11,000 --> 00:05:13,000
Even to do that much.

80
00:05:13,000 --> 00:05:16,000
You don't talk about practicing meditation, but he'd never seen the Buddha before.

81
00:05:16,000 --> 00:05:23,000
And when he saw the Buddha, he had so much rapture in him that he just wanted to bow down to the Buddha

82
00:05:23,000 --> 00:05:27,000
and pay respect to him and learn from him.

83
00:05:27,000 --> 00:05:30,000
And so he thought this in his mind, homage to the Buddha, and he died.

84
00:05:30,000 --> 00:05:32,000
He was very, very sick.

85
00:05:32,000 --> 00:05:36,000
As a result of that, he was born in heaven.

86
00:05:36,000 --> 00:05:38,000
He was born as an angel.

87
00:05:38,000 --> 00:05:43,000
And he was as a result able to come back and listen to the Buddha's teaching and practice and become enlightened.

88
00:05:43,000 --> 00:05:46,000
I mean, this is a story.

89
00:05:46,000 --> 00:05:51,000
Whether you believe these kinds of stories are not a thing that I'm just crazy to tell them.

90
00:05:51,000 --> 00:05:56,000
It expresses the point that is verifiable.

91
00:05:56,000 --> 00:06:01,000
That goodness comes from the mind and can be performed at any moment in any circumstance.

92
00:06:01,000 --> 00:06:08,000
And so that's really just fall to fly.

93
00:06:08,000 --> 00:06:10,000
That's really what we try to do in this life.

94
00:06:10,000 --> 00:06:16,000
We don't worry about our situation or it's totally empowering for everyone,

95
00:06:16,000 --> 00:06:23,000
for a disabled person, for a person who is in a state of suffering,

96
00:06:23,000 --> 00:06:36,000
even a person who is in some form of slavery or hard labor or who has great debt or so on.

97
00:06:36,000 --> 00:06:43,000
In the end, they can live their life out to its end and become enlightened.

98
00:06:43,000 --> 00:06:48,000
Anyway, that leads them closer to enlightenment.

99
00:06:48,000 --> 00:07:02,000
I would like to say that in Europe, people with physical disabilities or mental disabilities are not called handicapped or so.

100
00:07:02,000 --> 00:07:07,000
They are called now people with special needs.

101
00:07:07,000 --> 00:07:13,000
I think that man is very beautiful to put it,

102
00:07:13,000 --> 00:07:23,000
because if you look at it without,

103
00:07:23,000 --> 00:07:33,000
I would say that, I think the suffering,

104
00:07:33,000 --> 00:07:42,000
many of those people have, becomes because they are judged by others who are different.

105
00:07:42,000 --> 00:07:54,000
We, with two arms and two legs, moving them as we want, judge them and say,

106
00:07:54,000 --> 00:07:59,000
oh, they don't have that, they are different, they are disabled.

107
00:07:59,000 --> 00:08:03,000
But it's our judgment.

108
00:08:03,000 --> 00:08:06,000
They are in and of themselves perfect.

109
00:08:06,000 --> 00:08:10,000
And there are people without legs or without arms,

110
00:08:10,000 --> 00:08:15,000
and they still draw pictures and do things.

111
00:08:15,000 --> 00:08:24,000
And they are people with special needs who are very, very happy,

112
00:08:24,000 --> 00:08:28,000
living happy lives, living good lives, not suffering.

113
00:08:28,000 --> 00:08:32,000
Because they are not judged by others.

114
00:08:32,000 --> 00:08:36,000
They are just supported in the way they are.

115
00:08:36,000 --> 00:08:40,000
And if they need to do things that they cannot do,

116
00:08:40,000 --> 00:08:44,000
because they are different,

117
00:08:44,000 --> 00:08:49,000
then people help them instead of judging them.

118
00:08:49,000 --> 00:09:01,000
So, I think we should think more of that of how we judge them

119
00:09:01,000 --> 00:09:06,000
and what we do with that to them,

120
00:09:06,000 --> 00:09:10,000
instead of just being natural to them

121
00:09:10,000 --> 00:09:13,000
and accept them as they really are,

122
00:09:13,000 --> 00:09:17,000
and help them if help is needed.

123
00:09:17,000 --> 00:09:25,000
And learn from them that life can be very good,

124
00:09:25,000 --> 00:09:31,000
even if it's not perfect, even if body and mind are not perfect, if we want it.

125
00:09:31,000 --> 00:09:34,000
Well, if I can ask you a corollary question,

126
00:09:34,000 --> 00:09:38,000
what do you think of the fact that it's often karmic?

127
00:09:38,000 --> 00:09:41,000
Because there is one, you know,

128
00:09:41,000 --> 00:09:43,000
I want to be very careful here,

129
00:09:43,000 --> 00:09:46,000
because I don't want to give the impression that we should have any kind of judging,

130
00:09:46,000 --> 00:09:50,000
but what you often see with people who have,

131
00:09:50,000 --> 00:09:54,000
what you might call special needs, is not all the time.

132
00:09:54,000 --> 00:09:56,000
And so this is why I want to be careful,

133
00:09:56,000 --> 00:10:00,000
but you often see that they are unable to practice meditation

134
00:10:00,000 --> 00:10:06,000
because of some intense unholismness that has led them to this state.

135
00:10:06,000 --> 00:10:10,000
What would you think of that idea?

136
00:10:10,000 --> 00:10:19,000
That there isn't, there is something out of whack,

137
00:10:19,000 --> 00:10:28,000
or out of whack, something out there,

138
00:10:28,000 --> 00:10:31,000
especially in a person who has mental disabilities,

139
00:10:31,000 --> 00:10:36,000
is that it's not for the purpose of judging them,

140
00:10:36,000 --> 00:10:41,000
but I think probably the best part of that would be

141
00:10:41,000 --> 00:10:45,000
for a person who has these things to yes feel empowered,

142
00:10:45,000 --> 00:10:48,000
but to not feel overconfident and think,

143
00:10:48,000 --> 00:10:54,000
I don't have to give rise to this kind of feeling like,

144
00:10:54,000 --> 00:10:57,000
wow, because of my negligence,

145
00:10:57,000 --> 00:10:59,000
I've come to this situation,

146
00:10:59,000 --> 00:11:03,000
which I think would only come to a person who believes in karma and vast lives and so on,

147
00:11:03,000 --> 00:11:08,000
it can actually be beneficial because it helps you to give rise to this sense of urgency

148
00:11:08,000 --> 00:11:14,000
and the need to root out whatever it is that has brought you to that state.

149
00:11:14,000 --> 00:11:18,000
So just something to think about not to take too seriously

150
00:11:18,000 --> 00:11:21,000
because you don't want to look at people and say,

151
00:11:21,000 --> 00:11:23,000
oh, those people are bad karma or so on,

152
00:11:23,000 --> 00:11:26,000
but there is something there,

153
00:11:26,000 --> 00:11:29,000
something that why they weren't born,

154
00:11:29,000 --> 00:11:33,000
why their experience led them to be born in the state,

155
00:11:33,000 --> 00:11:37,000
that is obviously out of whack,

156
00:11:37,000 --> 00:11:40,000
I think is the term.

157
00:11:40,000 --> 00:11:45,000
Do you have to say?

158
00:11:45,000 --> 00:11:47,000
Do you understand what I'm asking you?

159
00:11:47,000 --> 00:11:51,000
I do understand more or less.

160
00:11:51,000 --> 00:11:55,000
I have one experience a long, long ago,

161
00:11:55,000 --> 00:12:11,000
I was preparing for a theatre piece where I had to act a disabled girl

162
00:12:11,000 --> 00:12:16,000
with I think it's down syndrome called.

163
00:12:16,000 --> 00:12:20,000
So I had to play that

164
00:12:20,000 --> 00:12:32,000
and I went to a daycare centre where people with special needs

165
00:12:32,000 --> 00:12:39,000
spend their days and afterwards in the evening would go back to the families.

166
00:12:39,000 --> 00:12:48,000
And there were some people with the down syndrome

167
00:12:48,000 --> 00:12:52,000
and for some month I worked with them,

168
00:12:52,000 --> 00:12:55,000
I worked with them, that's wrong.

169
00:12:55,000 --> 00:13:00,000
I just visited them and spent time and we played and so on.

170
00:13:00,000 --> 00:13:07,000
And they were so beautiful, so good,

171
00:13:07,000 --> 00:13:14,000
so really, really much better than them.

172
00:13:14,000 --> 00:13:19,000
They were angels.

173
00:13:19,000 --> 00:13:22,000
I don't know what to say.

174
00:13:22,000 --> 00:13:29,000
Maybe they are not able to meditate,

175
00:13:29,000 --> 00:13:32,000
but they could be still.

176
00:13:32,000 --> 00:13:37,000
We could have very, very silent moments together.

177
00:13:37,000 --> 00:13:48,000
But probably there are other forms of mental illness,

178
00:13:48,000 --> 00:13:53,000
which I haven't researched

179
00:13:53,000 --> 00:13:59,000
where something of the bad past karma is left or so on.

180
00:13:59,000 --> 00:14:03,000
I don't know, but I don't think that the person is really actually taking

181
00:14:03,000 --> 00:14:08,000
the bad into the next life.

182
00:14:08,000 --> 00:14:11,000
It's just maybe...

183
00:14:11,000 --> 00:14:24,000
I've had meditators who have disabilities in one way or another.

184
00:14:24,000 --> 00:14:28,000
I just kind of get the feeling that there is something.

185
00:14:28,000 --> 00:14:31,000
There can be something to watch out for.

186
00:14:31,000 --> 00:14:34,000
And I guess the idea would be the only thing I'd want to,

187
00:14:34,000 --> 00:14:39,000
the only time I'd want to bring such a thing up is for all of us.

188
00:14:39,000 --> 00:14:41,000
Because we all have disabilities.

189
00:14:41,000 --> 00:14:45,000
We all have things in our life, obstacles in our lives.

190
00:14:45,000 --> 00:14:49,000
And even if they don't bring it into their next life

191
00:14:49,000 --> 00:14:51,000
and have negative mind states,

192
00:14:51,000 --> 00:14:54,000
because if it's still a reason for us to be clear

193
00:14:54,000 --> 00:14:57,000
that this sort of thing can arise in our experience.

194
00:14:57,000 --> 00:15:02,000
And if we come to understand reality as being based on what it is,

195
00:15:02,000 --> 00:15:04,000
really reality is experience.

196
00:15:04,000 --> 00:15:06,000
It is reality.

197
00:15:06,000 --> 00:15:10,000
Then we realize that we have to be very careful with this.

198
00:15:10,000 --> 00:15:11,000
Like an egg.

199
00:15:11,000 --> 00:15:15,000
If you are not careful, it will break.

200
00:15:15,000 --> 00:15:17,000
To be careful with our experience.

201
00:15:17,000 --> 00:15:24,000
And to be quite meticulous about the cultivation of certain mind states,

202
00:15:24,000 --> 00:15:27,000
which can have, you know, if you read in the Sutras, it's just the smallest thing.

203
00:15:27,000 --> 00:15:31,000
The other thing that kind of, I would say, collaborates the ideas.

204
00:15:31,000 --> 00:15:35,000
When people do suffer, they often, and not always,

205
00:15:35,000 --> 00:15:39,000
but they often do become better people because of it,

206
00:15:39,000 --> 00:15:43,000
not because they see the potential.

207
00:15:43,000 --> 00:15:46,000
Or because they've dealt with suffering,

208
00:15:46,000 --> 00:15:48,000
and they know how to deal with suffering,

209
00:15:48,000 --> 00:15:51,000
and they become more patient.

210
00:15:51,000 --> 00:15:57,000
And they become more compassionate because they know what it means to suffer.

211
00:15:57,000 --> 00:16:07,000
My niece, for example, was as a child had the tendency to be very sick for suddenly.

212
00:16:07,000 --> 00:16:12,000
It could just appear that she got very sick.

213
00:16:12,000 --> 00:16:19,000
And living in that danger to become very sick every moment

214
00:16:19,000 --> 00:16:25,000
and get fever so high that it really could be life-threatening

215
00:16:25,000 --> 00:16:31,000
made her a very, very mature person who...

216
00:16:31,000 --> 00:16:34,000
I'm seeing a lot going on.

217
00:16:34,000 --> 00:16:43,000
So she is 17 now, and she is very, very special, very different from other people in that age.

218
00:16:43,000 --> 00:16:45,000
It's a very good approach.

219
00:16:45,000 --> 00:16:49,000
So just to reconfirm what you said.

