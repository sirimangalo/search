1
00:00:00,000 --> 00:00:07,000
Hi, just a short video here on a couple of projects I've been working on in terms of helping

2
00:00:07,000 --> 00:00:12,680
to make it easier to interact with the people who are interested in meditation.

3
00:00:12,680 --> 00:00:17,080
The first one, as most of you have probably noticed, is called Ask a Monk.

4
00:00:17,080 --> 00:00:24,800
It's just a way for me to make it easier to answer a lot of the reoccurring questions that

5
00:00:24,800 --> 00:00:29,320
people have about the meditation and about Buddhism in general.

6
00:00:29,320 --> 00:00:36,600
So the way it works is you go to my channel at youtube.com, frontslash, utadhamu, and

7
00:00:36,600 --> 00:00:41,280
go to scroll down to the moderator panel.

8
00:00:41,280 --> 00:00:42,280
Enter your question.

9
00:00:42,280 --> 00:00:45,360
You have to be logged in to enter questions.

10
00:00:45,360 --> 00:00:53,640
So if you haven't signed up for YouTube, sign up and enter your question and click submit.

11
00:00:53,640 --> 00:01:00,360
And then just hang on, wait and see, wait for my answer, if it seems like a reasonable

12
00:01:00,360 --> 00:01:03,720
question, I'll answer, try to answer as many as I can.

13
00:01:03,720 --> 00:01:07,480
If you want to see which ones I've been answered, you can go to the link, which is on

14
00:01:07,480 --> 00:01:13,000
the right-hand side, which leads you to Google Moderator, and then you get a list of all

15
00:01:13,000 --> 00:01:19,000
the questions that I've been asked, and the ones that have a response, save you a response,

16
00:01:19,000 --> 00:01:22,360
click on the view response, and you'll see my answer.

17
00:01:22,360 --> 00:01:27,240
You can also subscribe to my channel if you want to just get all the updates to all the

18
00:01:27,240 --> 00:01:28,920
questions.

19
00:01:28,920 --> 00:01:36,600
Just click on the subscribe button or click on the link, and you should get an update

20
00:01:36,600 --> 00:01:43,960
in your email or on your home screen, your home page, YouTube homepage, whenever I

21
00:01:43,960 --> 00:01:48,240
place a new video on YouTube.

22
00:01:48,240 --> 00:01:53,440
So this is something I encourage everyone to make use of if you don't have any questions

23
00:01:53,440 --> 00:01:54,440
yourself.

24
00:01:54,440 --> 00:01:59,200
You can vote on some of the questions that I've already been asked to try to give me

25
00:01:59,200 --> 00:02:09,280
a good idea of which ones are more interesting to people, have more of an audience.

26
00:02:09,280 --> 00:02:15,600
And just take part and happy to see people interested in the meditation and to people

27
00:02:15,600 --> 00:02:18,560
who appreciate the work.

28
00:02:18,560 --> 00:02:23,040
The other thing I'm involved in doesn't have to do with YouTube, so a lot of people

29
00:02:23,040 --> 00:02:25,440
may be missed it.

30
00:02:25,440 --> 00:02:34,520
It's something to do with a website called ustream.tv, that's the letter ustream.tv.

31
00:02:34,520 --> 00:02:43,720
And I've put up a channel there for real-time broadcasting, where when we do our real-life

32
00:02:43,720 --> 00:02:53,320
meditation, where we're here in California doing our daily meditation, you can watch.

33
00:02:53,320 --> 00:02:57,120
So first of all, give a talk, and you can listen to the talk in real-time as I'm giving

34
00:02:57,120 --> 00:02:58,120
it.

35
00:02:58,120 --> 00:03:02,320
And then once I finish the talk, you can meditate with this.

36
00:03:02,320 --> 00:03:06,840
It's besides the fact that it gives you a chance to listen to the Buddha's teaching,

37
00:03:06,840 --> 00:03:11,240
or listen to the teaching on the meditation, it also gives in a sort of a structure to

38
00:03:11,240 --> 00:03:16,280
our meditation practice, where you feel like you're actually practicing in a group.

39
00:03:16,280 --> 00:03:18,440
I don't know how successful it is.

40
00:03:18,440 --> 00:03:22,440
I can see there's a lot of people listening to the talks, maybe not so many who are

41
00:03:22,440 --> 00:03:28,520
actually meditating with us, but it does give you that human connection, where you can

42
00:03:28,520 --> 00:03:32,760
actually connect with other meditators in a way and feel like you're meditating with

43
00:03:32,760 --> 00:03:33,760
us.

44
00:03:33,760 --> 00:03:35,160
It's an excuse to meditate.

45
00:03:35,160 --> 00:03:41,360
So not sure how that's going to go, but at least it also gives you an idea of what we

46
00:03:41,360 --> 00:03:45,960
do here at the Meditations Center, and let's just see that we actually are conducting

47
00:03:45,960 --> 00:03:47,640
meditation courses.

48
00:03:47,640 --> 00:03:50,280
Let's just see what's going on here.

49
00:03:50,280 --> 00:03:53,640
So if you want to check that out, you're welcome to go to the link.

50
00:03:53,640 --> 00:04:00,440
It's ustream.tv, front slash channel, front slash, monk radio, all one word.

51
00:04:00,440 --> 00:04:04,960
Okay, so thanks for tuning in everyone, and keep the questions coming.

52
00:04:04,960 --> 00:04:05,960
In the comments as well.

