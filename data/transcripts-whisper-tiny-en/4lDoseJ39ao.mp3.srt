1
00:00:00,000 --> 00:00:22,720
Good evening, everyone, and welcome to our evening meeting.

2
00:00:31,000 --> 00:00:37,000
Saturday night, we're just about full up.

3
00:00:37,000 --> 00:00:43,000
If you weren't able to make it and you're watching this on YouTube,

4
00:00:43,000 --> 00:00:51,000
there's always the audio stream if you want to follow live.

5
00:00:51,000 --> 00:00:59,000
I got a limit here of 20 people, so not everyone can join us in virtual reality.

6
00:00:59,000 --> 00:01:16,000
We have two meditators here in Hamilton in somewhat less virtual reality, still virtual,

7
00:01:16,000 --> 00:01:23,000
still not ultimate reality.

8
00:01:23,000 --> 00:01:34,000
An ultimate reality, there is only what we call barmatadhamma.

9
00:01:34,000 --> 00:01:38,000
Actually, we translate barmatadhamma as ultimate reality.

10
00:01:38,000 --> 00:01:50,000
Barmata, barmata means highest atam means meaning, or in the highest sense.

11
00:01:50,000 --> 00:01:57,000
Dhamma is in the highest sense.

12
00:01:57,000 --> 00:02:02,000
We have two visitors today from Toronto,

13
00:02:02,000 --> 00:02:05,000
and I was explaining to them the four foundations of mindfulness,

14
00:02:05,000 --> 00:02:07,000
and then I came to the fourth one, and I said,

15
00:02:07,000 --> 00:02:12,000
well, Dhamma is a little bit hard to explain.

16
00:02:12,000 --> 00:02:18,000
And they nodded and said, yes, we know.

17
00:02:18,000 --> 00:02:22,000
Or they made a sign of the affirmative.

18
00:02:22,000 --> 00:02:26,000
Dhamma is a difficult word.

19
00:02:26,000 --> 00:02:29,000
It's used in many different ways.

20
00:02:29,000 --> 00:02:31,000
It's funny because before the Buddha came along,

21
00:02:31,000 --> 00:02:33,000
it wasn't such a complicated word.

22
00:02:33,000 --> 00:02:34,000
It's not actually difficult.

23
00:02:34,000 --> 00:02:39,000
It's just complicated and Buddhism.

24
00:02:39,000 --> 00:02:44,000
Or by the time the Buddha came around.

25
00:02:44,000 --> 00:02:49,000
So a Dhamma is something that one holds,

26
00:02:49,000 --> 00:02:54,000
something that is held.

27
00:02:54,000 --> 00:02:56,000
Dhar is the root.

28
00:02:56,000 --> 00:03:02,000
It means to hold or to carry.

29
00:03:02,000 --> 00:03:11,000
So it came to take on a figurative sense.

30
00:03:11,000 --> 00:03:18,000
The idea of holding on to some doctrine.

31
00:03:18,000 --> 00:03:21,000
So there was the Katya Dhamma,

32
00:03:21,000 --> 00:03:25,000
and the Brahmana Dhamma.

33
00:03:25,000 --> 00:03:30,000
It would be the code of the nobles and the priests.

34
00:03:30,000 --> 00:03:34,000
And then the,

35
00:03:34,000 --> 00:03:38,000
whatever you call the merchants,

36
00:03:38,000 --> 00:03:44,000
we saw in the cult.

37
00:03:44,000 --> 00:03:48,000
The merchant class had their own Dhamma.

38
00:03:48,000 --> 00:03:52,000
And it was sort of a code of social order.

39
00:03:52,000 --> 00:03:54,000
Those who subscribed,

40
00:03:54,000 --> 00:03:58,000
those who preached this code,

41
00:03:58,000 --> 00:04:02,000
were intent upon keeping social order

42
00:04:02,000 --> 00:04:04,000
or maintaining a sort of an aristocracy,

43
00:04:04,000 --> 00:04:08,000
or a case caste system,

44
00:04:08,000 --> 00:04:11,000
whereby the nobles ruled the country,

45
00:04:11,000 --> 00:04:16,000
and they had their duties as kings and rulers and warriors.

46
00:04:16,000 --> 00:04:20,000
And the Brahmins had their role as priests

47
00:04:20,000 --> 00:04:22,000
to perform religious ceremonies,

48
00:04:22,000 --> 00:04:29,000
perpetuate the gods,

49
00:04:29,000 --> 00:04:34,000
the merchants had their merchant code,

50
00:04:34,000 --> 00:04:38,000
their honor in dealing with

51
00:04:38,000 --> 00:04:42,000
each other in buying and selling and trading.

52
00:04:42,000 --> 00:04:45,000
And then of course the lowest class,

53
00:04:45,000 --> 00:04:47,000
while it was a means of reminding them that,

54
00:04:47,000 --> 00:04:50,000
well, this is just due to your karma,

55
00:04:50,000 --> 00:04:52,000
you were born into this lower class,

56
00:04:52,000 --> 00:04:57,000
and you should just accept it.

57
00:04:57,000 --> 00:05:04,000
Why says the Sanskrit waysa is poly?

58
00:05:04,000 --> 00:05:06,000
The waysa are the merchants,

59
00:05:06,000 --> 00:05:13,000
and the suit does are the lower class.

60
00:05:13,000 --> 00:05:20,000
So it's another means of

61
00:05:20,000 --> 00:05:21,000
telling the poor people that,

62
00:05:21,000 --> 00:05:23,000
well it's just part of your lot in life

63
00:05:23,000 --> 00:05:26,000
to be poor and hungry all the time.

64
00:05:26,000 --> 00:05:29,000
And so this works as sort of a social order.

65
00:05:29,000 --> 00:05:32,000
Anyway, the point was Dharamo is this idea.

66
00:05:32,000 --> 00:05:35,000
And so when the Buddha came around,

67
00:05:35,000 --> 00:05:39,000
well, there were actually

68
00:05:39,000 --> 00:05:41,000
quite a number of religious teachers

69
00:05:41,000 --> 00:05:43,000
who didn't really buy into,

70
00:05:43,000 --> 00:05:47,000
or maybe never had bought into the social structure

71
00:05:47,000 --> 00:05:51,000
of rulers and kings and merchants and so on,

72
00:05:51,000 --> 00:05:54,000
and priests didn't buy into the religion

73
00:05:54,000 --> 00:05:58,000
and the brahmanas, and so they would go off into the forest

74
00:05:58,000 --> 00:06:03,000
and preach their own Dharamo.

75
00:06:03,000 --> 00:06:05,000
Because they would see that the Dharam,

76
00:06:05,000 --> 00:06:08,000
these other Dharamas were actually quite corrupt

77
00:06:08,000 --> 00:06:09,000
in many ways.

78
00:06:09,000 --> 00:06:12,000
The Dharamo of the kasat kathias

79
00:06:12,000 --> 00:06:14,000
meant they could kill with impetua,

80
00:06:14,000 --> 00:06:18,000
impet, impunity,

81
00:06:18,000 --> 00:06:20,000
kill with impunity,

82
00:06:20,000 --> 00:06:23,000
as long as they were doing it.

83
00:06:23,000 --> 00:06:26,000
In accordance with the rules of war,

84
00:06:26,000 --> 00:06:29,000
killing was not wrong.

85
00:06:29,000 --> 00:06:34,000
At the brahmanas were greedy with impunity,

86
00:06:34,000 --> 00:06:39,000
they were cultivating all these myths

87
00:06:39,000 --> 00:06:44,000
and beliefs based on

88
00:06:44,000 --> 00:06:51,000
insubstantiated claims about reality,

89
00:06:51,000 --> 00:06:55,000
and hoarding money and hoarding possessions,

90
00:06:55,000 --> 00:06:59,000
and talking about brahmana and holiness,

91
00:06:59,000 --> 00:07:03,000
and yet having wives and female slaves

92
00:07:03,000 --> 00:07:06,000
and concubines and prostitutes and so on,

93
00:07:06,000 --> 00:07:08,000
and eating and drinking and luxury,

94
00:07:08,000 --> 00:07:15,000
and reaping the fruits of sacrifices,

95
00:07:15,000 --> 00:07:19,000
and the vases would be all corrupt

96
00:07:19,000 --> 00:07:21,000
in their dealings with each other,

97
00:07:21,000 --> 00:07:27,000
all about money and capitalism,

98
00:07:27,000 --> 00:07:32,000
striving for a bigger piece of the pie.

99
00:07:32,000 --> 00:07:34,000
And then the Sudha as well,

100
00:07:34,000 --> 00:07:36,000
and the idea that this was their dhamma

101
00:07:36,000 --> 00:07:41,000
to suffer miserably while they had these rich people

102
00:07:41,000 --> 00:07:46,000
lounged around in opulence.

103
00:07:46,000 --> 00:07:48,000
There were, I think,

104
00:07:48,000 --> 00:07:51,000
teachers at the time of the Buddha,

105
00:07:51,000 --> 00:07:53,000
for this and for other reasons,

106
00:07:53,000 --> 00:07:56,000
some just for the reasons that they wanted to be teachers

107
00:07:56,000 --> 00:07:58,000
on their own,

108
00:07:58,000 --> 00:08:00,000
would have been desired to be famous,

109
00:08:00,000 --> 00:08:02,000
they would teach an alternative doctrine,

110
00:08:02,000 --> 00:08:05,000
but often it was because they saw that society

111
00:08:05,000 --> 00:08:08,000
was quite corrupt in its nature,

112
00:08:08,000 --> 00:08:13,000
and the Buddha was one of these teachers.

113
00:08:13,000 --> 00:08:17,000
He left home seeing that

114
00:08:17,000 --> 00:08:20,000
this dharma is quite corrupt.

115
00:08:20,000 --> 00:08:22,000
He was set to become the king,

116
00:08:22,000 --> 00:08:25,000
but he saw this can't save me from suffering.

117
00:08:25,000 --> 00:08:29,000
This is only setting me up for great

118
00:08:29,000 --> 00:08:33,000
attachment and addiction,

119
00:08:33,000 --> 00:08:37,000
setting me up for great suffering and its luxury.

120
00:08:41,000 --> 00:08:45,000
And so he left home to find a higher dharma

121
00:08:45,000 --> 00:08:49,000
or dharma, we would say in Pali.

122
00:08:49,000 --> 00:08:53,000
And so he went to two different teachers,

123
00:08:53,000 --> 00:08:55,000
and if you know the story of the Buddha,

124
00:08:55,000 --> 00:08:56,000
they taught their dharma,

125
00:08:56,000 --> 00:08:57,000
and he asked them,

126
00:08:57,000 --> 00:08:58,000
what is your dharma?

127
00:08:58,000 --> 00:08:59,000
They taught it,

128
00:08:59,000 --> 00:09:00,000
and he mastered it,

129
00:09:00,000 --> 00:09:02,000
and he said, well, this dharma is still limited.

130
00:09:02,000 --> 00:09:08,000
And so eventually he came up with his own dharma,

131
00:09:08,000 --> 00:09:10,000
but this is where it starts to get complicated,

132
00:09:10,000 --> 00:09:13,000
is that the Buddha doesn't claim to have his own dharma.

133
00:09:13,000 --> 00:09:18,000
He claims to have realized the dharma,

134
00:09:18,000 --> 00:09:22,000
in a different sense of the word.

135
00:09:22,000 --> 00:09:27,000
And so here the meaning is that which holds

136
00:09:27,000 --> 00:09:31,000
or that which is held, held up

137
00:09:31,000 --> 00:09:41,000
to investigation when you learn about reality.

138
00:09:41,000 --> 00:09:46,000
These teachings, these truths, hold up.

139
00:09:49,000 --> 00:09:51,000
So in Buddhism, dharma became,

140
00:09:51,000 --> 00:09:56,000
dharma became to be known as reality.

141
00:09:56,000 --> 00:09:58,000
And so it's used interchange,

142
00:09:58,000 --> 00:10:03,000
it's used in two different ways.

143
00:10:03,000 --> 00:10:07,000
There is the dharma, the paramata dharma,

144
00:10:07,000 --> 00:10:10,000
these realities that the Buddha realized,

145
00:10:10,000 --> 00:10:14,000
then there's the sadharma, which is the teachings of the Buddha,

146
00:10:14,000 --> 00:10:17,000
and they're distinct.

147
00:10:21,000 --> 00:10:23,000
And it became even more complicated

148
00:10:23,000 --> 00:10:26,000
when he started using the reality's idea

149
00:10:26,000 --> 00:10:29,000
in terms of ideas.

150
00:10:29,000 --> 00:10:34,000
So we have dharma, dharma,

151
00:10:34,000 --> 00:10:37,000
anything that is the object of the mind,

152
00:10:37,000 --> 00:10:42,000
and it's called the dharma, and the armana is object.

153
00:10:42,000 --> 00:10:45,000
So you have dhamas.

154
00:10:45,000 --> 00:10:48,000
And so when it comes to explain the fourth foundations

155
00:10:48,000 --> 00:10:50,000
of my foundation of mindfulness,

156
00:10:50,000 --> 00:10:52,000
I haven't heard a satisfactory answer yet.

157
00:10:52,000 --> 00:10:54,000
What the word dharma means there,

158
00:10:54,000 --> 00:10:57,000
I've asked monks, I've asked lay scholars.

159
00:10:57,000 --> 00:11:01,000
I even asked my teacher, and he just said it's dharma,

160
00:11:01,000 --> 00:11:08,000
since they're quite simple.

161
00:11:15,000 --> 00:11:16,000
I think he said it's tumachat,

162
00:11:16,000 --> 00:11:18,000
because in Thai they have this one,

163
00:11:18,000 --> 00:11:19,000
and probably there's tumachati,

164
00:11:19,000 --> 00:11:23,000
it's just nature.

165
00:11:23,000 --> 00:11:25,000
So the dhamas are nature,

166
00:11:25,000 --> 00:11:29,000
but in the context of the fourth foundations of mindfulness,

167
00:11:29,000 --> 00:11:31,000
I think it just means the teachings,

168
00:11:31,000 --> 00:11:33,000
various teachings,

169
00:11:33,000 --> 00:11:36,000
various important, important specific teachings

170
00:11:36,000 --> 00:11:41,000
that are a part of the meditator's path.

171
00:11:48,000 --> 00:11:54,000
Anyway, tonight I wanted to talk about the paramata dhamas.

172
00:11:54,000 --> 00:11:57,000
Talk about this word dharma,

173
00:11:57,000 --> 00:11:59,000
because this is what we're trying to see.

174
00:11:59,000 --> 00:12:01,000
We have this phrase

175
00:12:01,000 --> 00:12:04,000
when the Buddha was about to pass away.

176
00:12:04,000 --> 00:12:07,000
He said,

177
00:12:07,000 --> 00:12:04,000
whatever person, dharma, nudhamma,

178
00:12:04,000 --> 00:12:12,000
patipa,

179
00:12:12,000 --> 00:12:15,000
patipa, no.

180
00:12:15,000 --> 00:12:19,000
Anyone who wishes to pay homage to the Buddha

181
00:12:19,000 --> 00:12:24,000
should practice the dharma

182
00:12:24,000 --> 00:12:28,000
in line with the dharma.

183
00:12:28,000 --> 00:12:31,000
dharma nudhamma.

184
00:12:31,000 --> 00:12:34,000
So this is interpreted to mean

185
00:12:34,000 --> 00:12:38,000
practicing the teachings

186
00:12:38,000 --> 00:12:40,000
to realize the truth.

187
00:12:40,000 --> 00:12:43,000
So here we have the use of

188
00:12:43,000 --> 00:12:45,000
the two different versions of the word dharma,

189
00:12:45,000 --> 00:12:47,000
dharma nudhamma,

190
00:12:47,000 --> 00:12:52,000
practicing the dharma in order to realize the dharma.

191
00:12:52,000 --> 00:12:55,000
It means practicing the four satipatana

192
00:12:55,000 --> 00:12:57,000
in order to realize nibana,

193
00:12:57,000 --> 00:13:02,000
in order to realize the end of suffering.

194
00:13:06,000 --> 00:13:11,000
Or in order to see the dharma's.

195
00:13:11,000 --> 00:13:14,000
And so it's this idea of seeing the dharma's

196
00:13:14,000 --> 00:13:21,000
that we focus on in our practice of satipatana.

197
00:13:21,000 --> 00:13:23,000
All four satipatana

198
00:13:23,000 --> 00:13:25,000
involves seeing the dharma's,

199
00:13:25,000 --> 00:13:28,000
seeing reality.

200
00:13:28,000 --> 00:13:30,000
When we focus on the body,

201
00:13:30,000 --> 00:13:34,000
we're not actually concerned about the body.

202
00:13:34,000 --> 00:13:35,000
We're concerned with

203
00:13:35,000 --> 00:13:40,000
dharma's that are physical.

204
00:13:40,000 --> 00:13:42,000
We're concerned with experiences,

205
00:13:42,000 --> 00:13:43,000
things that arise

206
00:13:43,000 --> 00:13:47,000
and sees moment after moment,

207
00:13:47,000 --> 00:13:49,000
movements of the body,

208
00:13:49,000 --> 00:13:51,000
the sensation of moving,

209
00:13:51,000 --> 00:13:53,000
touching,

210
00:13:53,000 --> 00:13:55,000
the sensation of heat and cold,

211
00:13:55,000 --> 00:13:57,000
hard, soft.

212
00:14:01,000 --> 00:14:03,000
The tension in the various parts of the body,

213
00:14:03,000 --> 00:14:05,000
for example, in the stomach,

214
00:14:05,000 --> 00:14:07,000
which is an object of meditation that we use.

215
00:14:13,000 --> 00:14:15,000
Each of these is a dharma

216
00:14:15,000 --> 00:14:18,000
because it holds up to react to an investigation.

217
00:14:18,000 --> 00:14:24,000
The idea of whether the body exists

218
00:14:24,000 --> 00:14:26,000
is just a matter of opinion.

219
00:14:26,000 --> 00:14:29,000
It's just a concept.

220
00:14:29,000 --> 00:14:32,000
When we look around in the room,

221
00:14:32,000 --> 00:14:34,000
we see other things and bodies.

222
00:14:34,000 --> 00:14:36,000
When we look down, we see our body.

223
00:14:36,000 --> 00:14:39,000
But whether it's actually there or not.

224
00:14:39,000 --> 00:14:42,000
It's just a belief, just a conjecture.

225
00:14:44,000 --> 00:14:47,000
We could just be in virtual reality.

226
00:14:47,000 --> 00:14:49,000
Instead of actually sitting here,

227
00:14:49,000 --> 00:14:51,000
we might all be hooked up.

228
00:14:51,000 --> 00:14:54,000
In fact, this might all be a figment of our imagination.

229
00:14:54,000 --> 00:14:57,000
I might be the only person in existence.

230
00:14:57,000 --> 00:15:01,000
I don't know whether any of you actually exist.

231
00:15:03,000 --> 00:15:07,000
But what I do know is that there is sensations.

232
00:15:07,000 --> 00:15:10,000
There is tension when I make a fist.

233
00:15:10,000 --> 00:15:12,000
There's tension when I breathe in.

234
00:15:12,000 --> 00:15:14,000
There's tension.

235
00:15:14,000 --> 00:15:17,000
When I'm in a hot room, there's heat.

236
00:15:17,000 --> 00:15:20,000
When I'm out in the cold, there's cold.

237
00:15:25,000 --> 00:15:27,000
When I stand on the hard floor,

238
00:15:27,000 --> 00:15:28,000
there's hardness.

239
00:15:28,000 --> 00:15:30,000
When I sit on the soft cushion,

240
00:15:30,000 --> 00:15:31,000
there's softness.

241
00:15:31,000 --> 00:15:32,000
There's these experiences.

242
00:15:32,000 --> 00:15:34,000
So they're dharma.

243
00:15:34,000 --> 00:15:35,000
They're dumbness.

244
00:15:35,000 --> 00:15:37,000
They actually exist for sure

245
00:15:37,000 --> 00:15:42,000
because they're actually experiences.

246
00:15:46,000 --> 00:15:49,000
When we wait in us,

247
00:15:50,000 --> 00:15:52,000
all of the feelings that we experience

248
00:15:52,000 --> 00:15:54,000
where they're painful or pleasant.

249
00:15:54,000 --> 00:15:57,000
Again, these arise and cease.

250
00:15:58,000 --> 00:16:00,000
But they're real.

251
00:16:02,000 --> 00:16:06,000
They're real, not in the sense that they exist

252
00:16:06,000 --> 00:16:10,000
and as an entity that comes and goes,

253
00:16:10,000 --> 00:16:12,000
but it's always existent.

254
00:16:12,000 --> 00:16:15,000
They come from cessation

255
00:16:15,000 --> 00:16:17,000
and they go back to cessation.

256
00:16:17,000 --> 00:16:19,000
They don't exist permanently.

257
00:16:19,000 --> 00:16:21,000
But during the moment of experience,

258
00:16:21,000 --> 00:16:22,000
they're real.

259
00:16:22,000 --> 00:16:25,000
That's what it means to be a dharma.

260
00:16:29,000 --> 00:16:30,000
When we watch the pain,

261
00:16:30,000 --> 00:16:32,000
we see it arising and ceasing.

262
00:16:32,000 --> 00:16:33,000
When we feel happy,

263
00:16:33,000 --> 00:16:36,000
we see it arising and ceasing.

264
00:16:41,000 --> 00:16:42,000
When we watch the mind,

265
00:16:42,000 --> 00:16:44,000
we see our thoughts,

266
00:16:44,000 --> 00:16:47,000
thoughts arising and ceasing.

267
00:16:48,000 --> 00:16:49,000
And finally, the dhammas,

268
00:16:49,000 --> 00:16:51,000
but here dhammas in the sense of teaching,

269
00:16:51,000 --> 00:16:53,000
so we have the knee-water in the dhamma.

270
00:16:53,000 --> 00:16:57,000
These are dhammas that the Buddha taught.

271
00:16:57,000 --> 00:16:58,000
And they're also real.

272
00:16:58,000 --> 00:17:00,000
They're also dhammas

273
00:17:00,000 --> 00:17:03,000
in the sense of being real,

274
00:17:03,000 --> 00:17:05,000
liking, disliking, drowsiness,

275
00:17:05,000 --> 00:17:06,000
distraction,

276
00:17:06,000 --> 00:17:09,000
doubt these are experienced.

277
00:17:09,000 --> 00:17:10,000
Seeing, earrings,

278
00:17:10,000 --> 00:17:11,000
smelling, tasting,

279
00:17:11,000 --> 00:17:13,000
feeling these are dhammas.

280
00:17:16,000 --> 00:17:17,000
And so on.

281
00:17:24,000 --> 00:17:26,000
The importance of this is that

282
00:17:26,000 --> 00:17:28,000
there's a distinct difference between dhammas

283
00:17:28,000 --> 00:17:31,000
and dhammas and concepts.

284
00:17:40,000 --> 00:17:42,000
When we spend our time looking at concepts,

285
00:17:42,000 --> 00:17:45,000
when we see people in places and things,

286
00:17:48,000 --> 00:17:51,000
we're able to cultivate all sorts of ideas and views

287
00:17:51,000 --> 00:17:53,000
and beliefs about these things

288
00:17:53,000 --> 00:17:55,000
because they don't actually exist.

289
00:17:55,000 --> 00:17:58,000
When we see a person,

290
00:17:58,000 --> 00:18:01,000
we can have an idea of what they're like.

291
00:18:01,000 --> 00:18:03,000
We can build up a whole narrative about,

292
00:18:03,000 --> 00:18:05,000
oh, there's that person.

293
00:18:06,000 --> 00:18:08,000
We might base it on experiences.

294
00:18:08,000 --> 00:18:11,000
We might just base it on our own delusions.

295
00:18:12,000 --> 00:18:14,000
And often do.

296
00:18:14,000 --> 00:18:16,000
We get into a relationship with someone

297
00:18:16,000 --> 00:18:19,000
and we have this very powerful concept

298
00:18:19,000 --> 00:18:20,000
of what they're like,

299
00:18:20,000 --> 00:18:23,000
and then they act in a different way

300
00:18:23,000 --> 00:18:25,000
and we get reps at.

301
00:18:35,000 --> 00:18:37,000
Or after some time our mind changes

302
00:18:37,000 --> 00:18:40,000
and our concept of them is no longer pleasing to us

303
00:18:40,000 --> 00:18:44,000
and we become bored and lose interest.

304
00:18:49,000 --> 00:18:52,000
Because concepts don't play out

305
00:18:52,000 --> 00:18:53,000
in ultimate reality.

306
00:18:53,000 --> 00:18:57,000
We reality continues according to

307
00:18:57,000 --> 00:19:01,000
very real patterns, laws,

308
00:19:04,000 --> 00:19:06,000
but concepts don't.

309
00:19:09,000 --> 00:19:11,000
So by living in a world of concepts,

310
00:19:11,000 --> 00:19:13,000
we build up all these delusions

311
00:19:13,000 --> 00:19:15,000
and based on our partialities,

312
00:19:15,000 --> 00:19:17,000
we build up ideas of what is good,

313
00:19:17,000 --> 00:19:19,000
what is bad,

314
00:19:19,000 --> 00:19:22,000
that have no basis in ultimate reality.

315
00:19:22,000 --> 00:19:25,000
We think of certain foods as good,

316
00:19:25,000 --> 00:19:28,000
certain sites as good and certain sounds as good

317
00:19:28,000 --> 00:19:30,000
and others as bad.

318
00:19:34,000 --> 00:19:39,000
And based on these beliefs and these partialities,

319
00:19:41,000 --> 00:19:45,000
we strive to accumulate or we strive to escape.

320
00:19:45,000 --> 00:19:49,000
And we act in ways that are totally unrelated

321
00:19:49,000 --> 00:19:50,000
to ultimate reality.

322
00:19:50,000 --> 00:19:53,000
They're based on this illusory sense

323
00:19:53,000 --> 00:19:55,000
of the way things are

324
00:19:55,000 --> 00:20:00,000
and how things, how the world works.

325
00:20:03,000 --> 00:20:07,000
It's like blind people living blind

326
00:20:07,000 --> 00:20:10,000
in a house and imagining what everything looks like.

327
00:20:10,000 --> 00:20:13,000
Meditation is like turning on the lights

328
00:20:13,000 --> 00:20:15,000
and suddenly you see.

329
00:20:15,000 --> 00:20:17,000
As you practice meditation,

330
00:20:17,000 --> 00:20:19,000
you suddenly see that these things that you are clinging to

331
00:20:19,000 --> 00:20:22,000
are not actually yours,

332
00:20:22,000 --> 00:20:26,000
are not actually satisfying or beneficial

333
00:20:26,000 --> 00:20:29,000
or worth clinging to in any way.

334
00:20:33,000 --> 00:20:36,000
You don't see anything very esoteric or profound,

335
00:20:36,000 --> 00:20:39,000
but what's profound is the simplicity of it.

336
00:20:39,000 --> 00:20:44,000
Is that by seeing the dumb ones,

337
00:20:44,000 --> 00:20:47,000
by seeing everything arise and cease.

338
00:20:50,000 --> 00:20:53,000
You straighten out all this crookedness in the mind,

339
00:20:53,000 --> 00:20:56,000
all the attachments and addictions

340
00:20:56,000 --> 00:21:01,000
and all the aversion and delusions.

341
00:21:01,000 --> 00:21:06,000
And your mind becomes pure.

342
00:21:10,000 --> 00:21:14,000
There's a great power to reality in this way,

343
00:21:14,000 --> 00:21:18,000
but it's important you have to make this shift.

344
00:21:18,000 --> 00:21:22,000
The mind has to come to understand reality

345
00:21:22,000 --> 00:21:32,000
in terms of dumb ones or experiences.

346
00:21:36,000 --> 00:21:38,000
It's important that when we think of the Buddha-Dhamma,

347
00:21:38,000 --> 00:21:42,000
we're not actually just thinking of the Buddha's teachings,

348
00:21:42,000 --> 00:21:46,000
learning them all and getting an intellectual sense of them,

349
00:21:46,000 --> 00:21:50,000
but we use the teachings in order to

350
00:21:50,000 --> 00:21:54,000
see the truth, in order to see reality for what it is,

351
00:21:54,000 --> 00:21:56,000
which really is transformative,

352
00:21:56,000 --> 00:22:00,000
which truly does free us from addiction and aversion

353
00:22:00,000 --> 00:22:03,000
and ego and clinging and all that.

354
00:22:06,000 --> 00:22:09,000
So, there you go.

355
00:22:09,000 --> 00:22:12,000
There's the dumber for tonight.

356
00:22:12,000 --> 00:22:17,000
Encouragement for us all to strive to understand the dumbness,

357
00:22:17,000 --> 00:22:22,000
to understand reality on an experiential level.

358
00:22:22,000 --> 00:22:39,000
Anyone has any questions? I'm happy to answer.

359
00:22:52,000 --> 00:22:56,000
Thank you.

360
00:23:22,000 --> 00:23:33,000
If there are no questions, then thank you all for coming.

361
00:23:33,000 --> 00:23:36,000
Good to see you all.

362
00:23:36,000 --> 00:24:00,000
I'm wishing you all a good night.

