1
00:00:00,000 --> 00:00:05,600
As a monk allowed to hold jobs, if a job does not break any of the precepts.

2
00:00:07,600 --> 00:00:15,040
Well, the first thing is, I thought, yes, you can hold a job here in the monastery and there's

3
00:00:15,040 --> 00:00:20,800
heaps of work and you have to make sure that you don't break any precepts. But I guess the question

4
00:00:20,800 --> 00:00:30,480
is holding a job out in the world, which is not possible. Supposed to hold it closer.

5
00:00:32,160 --> 00:00:37,440
Yeah, I mean, how far the question is how far you can take that? No, because everyone here has

6
00:00:37,440 --> 00:00:42,320
roles. Actually, it's a good question because it helps us. We can, I don't think it's a chance to

7
00:00:42,320 --> 00:00:47,040
talk about everyone here. We've got a camera person, for example. Her job is the dedicated

8
00:00:47,040 --> 00:00:54,960
camera person. This person on the far left is our, the monastery bookkeeper.

9
00:00:58,000 --> 00:01:04,240
It's an end secretary. And Kathy doesn't have a job because she refuses to ordain.

10
00:01:06,080 --> 00:01:14,640
Yes, I is our groundskeeper. So he does the, he's going to take my, take over my duty of

11
00:01:14,640 --> 00:01:20,640
looking and seeing what needs to be done in the monastery and hopefully help with arranging

12
00:01:20,640 --> 00:01:24,640
workers and so on. It was best you can. I mean, neither of us speak single. So it's, it's a lot

13
00:01:24,640 --> 00:01:32,320
of fun trying to explain our ideas to them and for them to explain their ideas to us. Equally

14
00:01:33,200 --> 00:01:38,400
entertaining. And our camera person is not just a camera person. She also does the

15
00:01:38,400 --> 00:01:46,160
kid's manager. And he's the kitchen manager. And, and Kathy has been helping with the kitchen

16
00:01:46,160 --> 00:01:53,200
as well. And then to render also. But anyway, so, so even, even the monastics we have, we have jobs.

17
00:01:55,760 --> 00:02:00,480
But, but, you know, how far you can take that, whether it means, whether, whether, if you mean working

18
00:02:00,480 --> 00:02:05,520
in the monastery, that's most likely not what you're referring to, most likely referring to

19
00:02:05,520 --> 00:02:13,840
doing something for lay people, for example, you're doing something not for the Buddha's teaching.

20
00:02:13,840 --> 00:02:25,200
And I would say the Vinaya may not directly explicitly disallow it, but the Dhamma certainly does.

21
00:02:25,200 --> 00:02:33,280
And the teachings of the Buddha, even within the Vinaya are quite clear that we're not to

22
00:02:33,280 --> 00:02:42,560
engage in any sort of worldly pursuits or assistance for lay people besides teaching.

23
00:02:42,560 --> 00:02:48,480
I mean, there are monks who decide that they want to be doctors and so they provide cures for

24
00:02:48,480 --> 00:02:55,200
people. And this is quite explicitly in the Buddha's teaching, not allowed. The performance of

25
00:02:55,200 --> 00:03:00,080
services other than meditation. People will often ask, well, but if someone's a good doctor,

26
00:03:00,080 --> 00:03:05,920
why can't they use their their skills? Well, of course they can use their skills,

27
00:03:05,920 --> 00:03:11,280
but they should be a doctor, not a monk, right? You become a monk means you take on a specific

28
00:03:11,280 --> 00:03:20,000
life, the life of a reckless meditation practitioner and perhaps teacher. And so you've committed

29
00:03:20,000 --> 00:03:27,280
yourself to that. And then to use it for something totally outside of the realm of the practice

30
00:03:27,280 --> 00:03:34,080
and the teaching of Buddhism is completely inappropriate. I mean, of course this is contentious

31
00:03:34,080 --> 00:03:40,480
and controversial. There are people who will argue against this. But from my point of view,

32
00:03:40,480 --> 00:03:45,840
anyway, this is quite clear that even doing good things for people, if you want to do good things,

33
00:03:47,280 --> 00:03:53,600
if you wanted to do whatever job you were asking about, if you want to do that job, then become

34
00:03:53,600 --> 00:03:59,040
that person. If you want to fight legal courses, become a lawyer. If you want to heal people's

35
00:03:59,040 --> 00:04:05,840
illnesses, become a doctor. If you want to be free from suffering, become a monk or even to teach

36
00:04:05,840 --> 00:04:14,000
other people meditation, become a monk. I would say that's the best job that you have

37
00:04:15,040 --> 00:04:18,800
aside from jobs in the monastery supporting the community, which is, I think, on a different level.

38
00:04:18,800 --> 00:04:26,800
In the Bhikunipati Maka, there is even a rule for that. We are not allowed to do chores for lay people.

39
00:04:27,600 --> 00:04:36,640
I don't think we have any assistance. We have it explicitly in the party Maka, that we are not

40
00:04:36,640 --> 00:04:48,400
allowed to do chores for lay people. And that is to have some, even to do something for lay

41
00:04:48,400 --> 00:05:00,320
person to get more food or to get some benefit out of it. I can certainly say we are not allowed

42
00:05:00,320 --> 00:05:10,800
to do any job for money or for food or something like that. I think the point of the rule is

43
00:05:11,760 --> 00:05:16,800
to prevent people from doing that. We are not allowed to do it at all because it gives the opportunity

44
00:05:16,800 --> 00:05:26,240
for corrupts, for corruption. People who are only beginning in the monastic life to

45
00:05:28,720 --> 00:05:36,320
get on the wrong path. It's very easy that people say, oh, if you do me this favor, I give you

46
00:05:36,320 --> 00:05:44,080
something because that's how the world rules or is ruled. And so it becomes a corruption. It's just

47
00:05:44,080 --> 00:05:50,960
not allowed. We have to be very strict. And we have left the world. We are not meant to get back

48
00:05:50,960 --> 00:05:57,040
involved in it. You have to consider that the monks have left. They are not in your circle of

49
00:05:57,840 --> 00:06:04,640
your pool of workers. You can't ask monks to help you build your house or something like that.

50
00:06:04,640 --> 00:06:11,120
Help you fix your computer. We can do jobs for our family. I have helped my father with his

51
00:06:11,120 --> 00:06:18,640
computer as a monk and he's the only person I did that for. Yeah, there is an allowance to help

52
00:06:19,360 --> 00:06:26,960
family, like direct families, like mother and father and brother and sisters. Because you're

53
00:06:26,960 --> 00:06:42,880
also allowed to ask them for whatever you want. So there's that kind of thing. Okay.

