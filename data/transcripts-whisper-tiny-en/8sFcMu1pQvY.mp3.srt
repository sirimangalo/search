1
00:00:00,000 --> 00:00:23,000
You can come sit closer, maybe I can talk quietly, most of the time, it's hard to hear.

2
00:00:23,000 --> 00:00:52,000
So, probably, what we'll do tonight is we're going to do a little bit of a little bit of that.

3
00:00:52,000 --> 00:00:56,000
Probably, what we'll do tonight, I'll give a short talk.

4
00:00:56,000 --> 00:01:11,000
This is the custom, and then I'll invite the new people to go and I'll show you how to do the meditation and everyone else can subscribe.

5
00:01:26,000 --> 00:01:45,000
But tonight is our English session together.

6
00:01:45,000 --> 00:01:54,000
When I talk, it's important that we prepare ourselves, use it as a time to prepare ourselves for the meditation.

7
00:01:54,000 --> 00:02:02,000
We're not here just to learn theory or ideas.

8
00:02:02,000 --> 00:02:05,000
We're here to practice.

9
00:02:05,000 --> 00:02:20,000
So, here we have this introduction where I talk, and you can start to bring your mind back to the here and now.

10
00:02:20,000 --> 00:02:36,000
It may not be most entertaining form of speech, but in this way, it's more useful.

11
00:02:36,000 --> 00:02:45,000
So, when we listen, we can close our eyes and start to become more meditative.

12
00:02:45,000 --> 00:03:09,000
If we've already begun to practice, we can just continue with our practice while we're listening.

13
00:03:09,000 --> 00:03:29,000
One of the ways of understanding the practice of meditation is in terms of the difference between happiness and peace.

14
00:03:29,000 --> 00:03:41,000
And I would say, only when we understand and begin to prefer peace to what we normally understand to be happiness.

15
00:03:41,000 --> 00:03:51,000
Can we really appreciate things like meditation?

16
00:03:51,000 --> 00:04:04,000
Because that's really what we're doing is teaching our minds to give up the need to seek out pleasure again and again.

17
00:04:04,000 --> 00:04:14,000
Teaching our minds to be here to not worry about getting things, achieving things, accomplishing things.

18
00:04:14,000 --> 00:04:24,000
To be content with the present moment, the here and the now.

19
00:04:24,000 --> 00:04:33,000
Most of how we live our lives is chasing after happiness and running away from suffering.

20
00:04:33,000 --> 00:04:41,000
And so, as I said before, we compartmentalize reality.

21
00:04:41,000 --> 00:04:45,000
We're no longer open to the full spectrum of experience.

22
00:04:45,000 --> 00:04:50,000
Only a certain part of our experience is acceptable.

23
00:04:50,000 --> 00:04:54,000
For instance, sitting here right now, there's probably a lot of things that are unacceptable.

24
00:04:54,000 --> 00:04:57,000
The pain that we feel in the body by having this is still.

25
00:04:57,000 --> 00:05:00,000
Maybe we feel hot or cold.

26
00:05:00,000 --> 00:05:03,000
Maybe we're itching.

27
00:05:03,000 --> 00:05:08,000
Maybe we're thinking about the past or the future.

28
00:05:08,000 --> 00:05:14,000
There are many things that we can't accept about the reality in front of us.

29
00:05:14,000 --> 00:05:20,000
And on the other hand, there are many things that we require to be happy.

30
00:05:20,000 --> 00:05:28,000
So, we're thinking about food or we're thinking about family or we're thinking about friends.

31
00:05:28,000 --> 00:05:33,000
We're thinking about places and things that we have to do.

32
00:05:33,000 --> 00:05:35,000
Plans that we have for the future.

33
00:05:35,000 --> 00:05:39,000
Maybe thinking about things that happen to us in the past.

34
00:05:39,000 --> 00:05:44,000
Good things or bad things.

35
00:05:44,000 --> 00:05:48,000
And we're constantly judging.

36
00:05:48,000 --> 00:05:53,000
We're constantly dividing the world up into good and bad.

37
00:05:53,000 --> 00:06:00,000
And when experience goes according to our wishes, we're happy.

38
00:06:00,000 --> 00:06:09,000
When it doesn't, we're unhappy.

39
00:06:09,000 --> 00:06:15,000
This is the ordinary way of approaching the idea of happiness.

40
00:06:15,000 --> 00:06:31,000
The ordinary method for human beings to find true happiness is to chase after pleasures and run away from suffering.

41
00:06:31,000 --> 00:06:42,000
The reason why we come to practice meditation is because we begin to see that this is a poor model for finding true happiness.

42
00:06:42,000 --> 00:06:53,000
For happiness, for life, in terms of finding peace, in terms of finding real and lasting happiness.

43
00:06:53,000 --> 00:07:02,000
Because the more we get what we want, what is enjoyable to us, the more we feel happy,

44
00:07:02,000 --> 00:07:08,000
the more we like that happiness and want that happiness.

45
00:07:08,000 --> 00:07:30,000
The more we distinguish that happiness from other states, which are then labeled as unhappy or boring or unexciting.

46
00:07:30,000 --> 00:07:39,000
And so rather than finding more happiness, our search for happiness actually leads us to more suffering.

47
00:07:39,000 --> 00:07:50,000
To a constant chase, a constant search for pleasure, needing this and that and so many things.

48
00:07:50,000 --> 00:07:59,000
And becoming angry and upset when we can't get what we want.

49
00:07:59,000 --> 00:08:11,000
When things aren't the way we want, we have to fix, have to change our experience.

50
00:08:11,000 --> 00:08:19,000
The first thing we realize about meditation is it's not really about happy feelings.

51
00:08:19,000 --> 00:08:24,000
It's not about getting something.

52
00:08:24,000 --> 00:08:35,000
Our goal when we sit is not to sit and feel states of bliss, states of pleasure.

53
00:08:35,000 --> 00:08:41,000
What we're looking for is a state of peace.

54
00:08:41,000 --> 00:08:46,000
When we practice meditation, because we realize that this peace is something that we're missing in our lives.

55
00:08:46,000 --> 00:08:52,000
No matter how much happiness we have, we rarely have ever have any peace.

56
00:08:52,000 --> 00:09:02,000
Because we're always chasing, we're always looking, we're always unsatisfying.

57
00:09:02,000 --> 00:09:12,000
And so the practice of meditation is creating contentment and satisfaction with the way things are.

58
00:09:12,000 --> 00:09:15,000
No longer needing things to be other than what they are.

59
00:09:15,000 --> 00:09:22,000
But even when things are experience of reality is painful, it's unpleasurable.

60
00:09:22,000 --> 00:09:24,000
To not be upset by it.

61
00:09:24,000 --> 00:09:25,000
Because we can't avoid these states.

62
00:09:25,000 --> 00:09:26,000
We can't avoid getting old.

63
00:09:26,000 --> 00:09:28,000
We can't avoid getting sick.

64
00:09:28,000 --> 00:09:30,000
We can't avoid dying.

65
00:09:30,000 --> 00:09:44,000
We can't avoid so many unpleasant experiences that are going to arise for us in our lives.

66
00:09:44,000 --> 00:09:54,000
And so once we experience these problems of life again and again, we realize that there's something wrong with the way we're looking at the world.

67
00:09:54,000 --> 00:09:59,000
And our minds are not stable, our minds are not calm.

68
00:09:59,000 --> 00:10:02,000
The minds are not the way we'd like them to be.

69
00:10:02,000 --> 00:10:05,000
We don't have any peace.

70
00:10:05,000 --> 00:10:09,000
Even just sitting here listening to me talk, you'll find your mind.

71
00:10:09,000 --> 00:10:11,000
It's not really a peace.

72
00:10:11,000 --> 00:10:21,000
It's thinking about the good things that we can get and becoming upset very quickly about the bad things that arise.

73
00:10:21,000 --> 00:10:25,000
Unpleasiveness.

74
00:10:25,000 --> 00:10:31,000
And so we've already, even just sitting here in a few minutes, we've already segregated.

75
00:10:31,000 --> 00:10:34,000
We've already made judgment after judgment about everything that arises.

76
00:10:34,000 --> 00:10:37,000
The things that I'm saying right away there's judgment.

77
00:10:37,000 --> 00:10:41,000
The temperature in the room, there's judgment.

78
00:10:41,000 --> 00:10:45,000
It's the feeling of the sitting mat that you're on.

79
00:10:45,000 --> 00:10:54,000
The pains in the body and the itching and discomfort.

80
00:10:54,000 --> 00:11:00,000
Thinking about the past, thinking about the future.

81
00:11:00,000 --> 00:11:02,000
We're not really at peace.

82
00:11:02,000 --> 00:11:05,000
Our minds are very clingy.

83
00:11:05,000 --> 00:11:07,000
They cling to everything.

84
00:11:07,000 --> 00:11:08,000
They have a judgment.

85
00:11:08,000 --> 00:11:11,000
They have an opinion about everything.

86
00:11:11,000 --> 00:11:13,000
And they're chattering away.

87
00:11:13,000 --> 00:11:14,000
This is good.

88
00:11:14,000 --> 00:11:15,000
This is bad.

89
00:11:15,000 --> 00:11:16,000
This is right.

90
00:11:16,000 --> 00:11:17,000
This is wrong.

91
00:11:17,000 --> 00:11:18,000
Telling us what to do.

92
00:11:18,000 --> 00:11:19,000
Do this.

93
00:11:19,000 --> 00:11:20,000
Do that.

94
00:11:20,000 --> 00:11:21,000
Don't do this.

95
00:11:21,000 --> 00:11:24,000
Don't do that.

96
00:11:24,000 --> 00:11:31,000
Our minds are like a little child, whining and complaining all the time.

97
00:11:31,000 --> 00:11:35,000
Unable to sit still, unable to be.

98
00:11:35,000 --> 00:11:37,000
Just be.

99
00:11:37,000 --> 00:11:43,000
You can hear it now.

100
00:11:43,000 --> 00:11:50,000
So the reason, this is the reason why we use this tool called meditation of

101
00:11:50,000 --> 00:11:53,000
contemplating things as they are.

102
00:11:53,000 --> 00:11:56,000
Instead of judging things, we see them for what they are.

103
00:11:56,000 --> 00:12:02,000
So when we feel a pain in the body, we just see it for pain.

104
00:12:02,000 --> 00:12:05,000
It's totally changing the way we look at the world.

105
00:12:05,000 --> 00:12:11,000
We've never before have thought to actually focus on a painful situation.

106
00:12:11,000 --> 00:12:16,000
Our normal reaction is to avoid it, to run away from it, to cover it up,

107
00:12:16,000 --> 00:12:24,000
to ignore it and pretend it doesn't exist.

108
00:12:24,000 --> 00:12:31,000
And the reason why we're doing the exact opposite is because we want to understand,

109
00:12:31,000 --> 00:12:38,000
we come to see that we don't really have a proper understanding of reality.

110
00:12:38,000 --> 00:12:41,000
We don't really have a proper relationship with reality.

111
00:12:41,000 --> 00:12:46,000
And we're always messing up making mistakes.

112
00:12:46,000 --> 00:12:48,000
We're addicted to things.

113
00:12:48,000 --> 00:12:54,000
We're a versatile thing to situations that lead us to get angry and frustrated,

114
00:12:54,000 --> 00:12:59,000
and bored and worried and scared and depressed.

115
00:12:59,000 --> 00:13:08,000
Adicted.

116
00:13:08,000 --> 00:13:12,000
And so in meditation, we want to change that.

117
00:13:12,000 --> 00:13:17,000
We want to come to understand reality and figure out the proper way

118
00:13:17,000 --> 00:13:24,000
to react to the phenomena that arise.

119
00:13:24,000 --> 00:13:26,000
We want to learn about the pain.

120
00:13:26,000 --> 00:13:29,000
We want to understand what is pain.

121
00:13:29,000 --> 00:13:31,000
What happens when pain arises?

122
00:13:31,000 --> 00:13:33,000
How does my mind react?

123
00:13:33,000 --> 00:13:38,000
And to see where the true suffering is.

124
00:13:38,000 --> 00:13:42,000
So we remind ourselves, we use this word to remind ourselves,

125
00:13:42,000 --> 00:13:48,000
this is pain, we say to ourselves, pain, pain,

126
00:13:48,000 --> 00:13:54,000
simply knowing that it's pain.

127
00:13:54,000 --> 00:13:56,000
And we make peace with this pain.

128
00:13:56,000 --> 00:13:59,000
We come to understand that it simply is pain.

129
00:13:59,000 --> 00:14:06,000
There's nothing intrinsically negative about the feeling at all.

130
00:14:06,000 --> 00:14:13,000
There's nothing in it that says, this is bad, I'm bad, hate me.

131
00:14:13,000 --> 00:14:15,000
We do that to ourselves.

132
00:14:15,000 --> 00:14:20,000
We create that for ourselves.

133
00:14:20,000 --> 00:14:23,000
When we feel pain right away, it's bad.

134
00:14:23,000 --> 00:14:34,000
It's a problem, it's my problem, my pain.

135
00:14:34,000 --> 00:14:39,000
So we, instead of all of that, we understand the pain for what it is.

136
00:14:39,000 --> 00:14:41,000
We see this is pain.

137
00:14:41,000 --> 00:14:44,000
We stop it there.

138
00:14:44,000 --> 00:14:51,000
And we come to force our minds to accept this, to accept the reality of it.

139
00:14:51,000 --> 00:14:57,000
We stop denying the fact that it is pain, that it's there, that it exists.

140
00:14:57,000 --> 00:15:04,000
And we make peace with the pain.

141
00:15:04,000 --> 00:15:08,000
We find peace in all situations, whatever arises.

142
00:15:08,000 --> 00:15:11,000
We stop compartmentalizing reality.

143
00:15:11,000 --> 00:15:14,000
Everything becomes peaceful for us.

144
00:15:14,000 --> 00:15:16,000
Everything becomes acceptable.

145
00:15:16,000 --> 00:15:21,000
Nothing that arises can cause us to suffer.

146
00:15:21,000 --> 00:15:24,000
This is in brief what we're trying for, what we're aiming for.

147
00:15:24,000 --> 00:15:30,000
And it's, as I said, it's the highlighting of this difference between happiness and peace.

148
00:15:30,000 --> 00:15:36,000
If you're seeking, always seeking after happy experiences, pleasurable experiences,

149
00:15:36,000 --> 00:15:38,000
you'll never be at peace.

150
00:15:38,000 --> 00:15:40,000
You're creating addiction.

151
00:15:40,000 --> 00:15:45,000
You're creating attachment.

152
00:15:45,000 --> 00:15:55,000
The irony of it is this, that the more you want to be happy, the less happy you'll become.

153
00:15:55,000 --> 00:15:58,000
The less happy because the less peaceful, your mind is no longer at peace.

154
00:15:58,000 --> 00:16:01,000
In practice meditation, we're not trying to give rise to a certain experience.

155
00:16:01,000 --> 00:16:03,000
We're trying to find peace.

156
00:16:03,000 --> 00:16:07,000
We're trying to be at peace with ourselves.

157
00:16:07,000 --> 00:16:09,000
To be at peace with the world around us.

158
00:16:09,000 --> 00:16:11,000
To be at peace with the universe.

159
00:16:11,000 --> 00:16:15,000
To stop fighting.

160
00:16:15,000 --> 00:16:18,000
To stop needing and wanting.

161
00:16:18,000 --> 00:16:21,000
To accept things for what they are.

162
00:16:21,000 --> 00:16:28,000
This is why we practice.

163
00:16:28,000 --> 00:16:33,000
And so without further ado, I'll invite everyone to begin.

164
00:16:33,000 --> 00:16:37,000
For those of you who have practiced before, you can continue on.

165
00:16:37,000 --> 00:16:41,000
Start with the mindful frustration when you're walking in the city.

166
00:16:41,000 --> 00:17:08,000
For the new people, I'll take you now to show you how to practice meditation.

