1
00:00:00,000 --> 00:00:05,380
Should the four foundations of mindfulness and the five janifactors of

2
00:00:05,380 --> 00:00:10,920
concentration and absorption be learned before we pass in a meditation can

3
00:00:10,920 --> 00:00:21,860
be correctly used. Okay well there are two pads of practice that should be

4
00:00:21,860 --> 00:00:27,420
clear. The first path of practice is to start with train quality meditation

5
00:00:27,420 --> 00:00:35,300
and then go on with we pass in this. So the way that works is you cultivate pure

6
00:00:35,300 --> 00:00:45,780
concentration and or you cultivate no tranquility and then your wisdom your

7
00:00:45,780 --> 00:00:51,000
knowledge comes and when they meet in a peak when you have both and both the two of

8
00:00:51,000 --> 00:01:00,240
them then there's the spark and because of the focus the focus knowledge or

9
00:01:00,240 --> 00:01:07,800
insight there is the attainment of nibana. Now the other way is to build up both

10
00:01:07,800 --> 00:01:13,560
concentration and insight together until the last moment and there's the spark.

11
00:01:13,560 --> 00:01:22,560
So a person who practices the five janifactors of concentration and absorption

12
00:01:22,560 --> 00:01:29,160
is someone who goes the first way first tranquilizing the mind then cultivating

13
00:01:29,160 --> 00:01:38,140
insight based on that. Now it's it can be stronger it's certainly more

14
00:01:38,140 --> 00:01:45,120
full because it gives you the opportunity to explore the conceptual world

15
00:01:45,120 --> 00:01:48,640
because the reason it's calm without insight it's because it's focused on the

16
00:01:48,640 --> 00:01:52,280
concept and because it allows you you're in the conceptual world you can play

17
00:01:52,280 --> 00:01:56,680
with it and you can you can actually cultivate magical powers like reading

18
00:01:56,680 --> 00:02:03,520
people's minds remembering vast lives all sorts of neat phenomena that are

19
00:02:03,520 --> 00:02:10,040
in the conceptual world you can't get that from from from cultivating the

20
00:02:10,040 --> 00:02:17,360
two together not very easily anyway but it seems to take longer though the

21
00:02:17,360 --> 00:02:22,080
time you're spending in cultivating first concentration could have been used

22
00:02:22,080 --> 00:02:27,040
in cultivating insight so it seems like it probably takes a little bit longer

23
00:02:27,040 --> 00:02:32,720
because if you just do pure insight and concentration you seem to be able to get

24
00:02:32,720 --> 00:02:38,520
pretty good results in a few weeks or at most a month whereas if you do

25
00:02:38,520 --> 00:02:43,080
tranquility first it can take months to get the same result even years

26
00:02:43,080 --> 00:02:47,320
depending on whether you ever get around to insight meditation afterwards

27
00:02:47,320 --> 00:02:51,200
some people will just stick to the common and not realize that they have to

28
00:02:51,200 --> 00:02:57,200
add insight or never get instruction in how to cultivate insight so the

29
00:02:57,200 --> 00:03:00,760
question of whether you have to learn them now the question you hear it you

30
00:03:00,760 --> 00:03:04,400
hear your question is should they be learned now I'm not sure whether that

31
00:03:04,400 --> 00:03:09,320
means study or practice if you just means do you have to study about these

32
00:03:09,320 --> 00:03:16,600
things then well first of all you don't you don't have to practice them first

33
00:03:16,600 --> 00:03:23,200
so they so you don't have to learn how to or you don't have to cultivate the

34
00:03:23,200 --> 00:03:28,440
five genre factors but first if you just want to do them both together but and

35
00:03:28,440 --> 00:03:36,040
so you obviously in that case don't have to study them either but if you want

36
00:03:36,040 --> 00:03:41,080
to do it the the summit of first way the tranquility first way then you have to

37
00:03:41,080 --> 00:03:46,440
both learn and practice them first study and practice them as for the four

38
00:03:46,440 --> 00:03:53,160
foundations of mindfulness if the question is do you have to practice them first

39
00:03:53,160 --> 00:03:58,480
so you have to undertake to practice them before practicing insight

40
00:03:58,480 --> 00:04:03,000
meditation then this is a confusion about what is meant by insight meditation

41
00:04:03,000 --> 00:04:07,240
because we pass the night insight meditation is the practice of the four

42
00:04:07,240 --> 00:04:11,800
foundations of mindfulness you practice the four foundations of mindfulness to

43
00:04:11,800 --> 00:04:17,320
cultivate insight you can use certain aspects of the four foundations of

44
00:04:17,320 --> 00:04:25,280
mindfulness to cultivate tranquility but in its most bare form it's a type of

45
00:04:25,280 --> 00:04:30,400
insight meditation practice now the question do you have to study the four

46
00:04:30,400 --> 00:04:36,320
foundations first obviously you certainly you don't have to study them in

47
00:04:36,320 --> 00:04:40,000
detail but obviously you have to know about them because they are the

48
00:04:40,000 --> 00:04:46,080
practice that you're undertaking knowing about and understanding how to

49
00:04:46,080 --> 00:04:50,320
practice each of the four in regards to each of the four foundations at

50
00:04:50,320 --> 00:04:55,480
least on a basic level is essential because without obviously without

51
00:04:55,480 --> 00:04:59,480
understanding what it is they're going to practice you can't be expected to

52
00:04:59,480 --> 00:05:07,800
practice but certainly important to study them but the question of whether you

53
00:05:07,800 --> 00:05:11,600
should practice them first doesn't arise because once you begin to practice

54
00:05:11,600 --> 00:05:15,960
them that's the practice of the bus and insight meditation unless you're

55
00:05:15,960 --> 00:05:20,760
focusing on this the specific parts that can lead to tranquility first like

56
00:05:20,760 --> 00:05:26,080
mindfulness or the breath or the cemetery contemplations loads some

57
00:05:26,080 --> 00:05:44,640
as that kind of thing the 32 parts of the body for example okay

