1
00:00:00,000 --> 00:00:17,540
Good evening and welcome to our live broadcast today we are continuing with the

2
00:00:17,540 --> 00:00:33,240
Ungutranikaya. I'm just gonna go right to the next one because it's I mean it's

3
00:00:33,240 --> 00:00:41,080
not terribly deep but it's interesting for those wondering about the role the

4
00:00:41,080 --> 00:00:48,320
heavens play in in Buddhism so apologies that this isn't a deep

5
00:00:48,320 --> 00:00:52,600
meditative meditation teaching I don't have any meditators here right now

6
00:00:52,600 --> 00:01:04,840
so we're just gonna go on from number 36 to number 37 so apparently they're in

7
00:01:04,840 --> 00:01:13,620
Buddhism that but it's cosmology the Buddha described various levels of angels so

8
00:01:13,620 --> 00:01:18,640
there are bhuma devo which are angels that live around live on earth apparently

9
00:01:18,640 --> 00:01:27,440
the living trees or whatever they delight in the fairies I guess you know

10
00:01:27,440 --> 00:01:37,880
I think and then there are the successively higher planes of angels there are

11
00:01:37,880 --> 00:01:43,460
the four great kings that if you you may be recognized some of you this from

12
00:01:43,460 --> 00:01:49,340
Chinese mythology which gets it from Buddhism the four great kings that look

13
00:01:49,340 --> 00:01:54,760
after the four directions apparently there are four angels who also are

14
00:01:54,760 --> 00:02:02,280
involved with the earth but they take care of the four for quadrants of the

15
00:02:02,280 --> 00:02:10,120
earth in directions doesn't really work because the earth is around so I don't

16
00:02:10,120 --> 00:02:18,440
quite understand it but there are apparently four great kings and they report to

17
00:02:18,440 --> 00:02:29,760
the angels of the tawatings which is the next level and tawatings is I guess

18
00:02:29,760 --> 00:02:36,360
the highest angel realm that's directly interested in in the earth so tawatings

19
00:02:36,360 --> 00:02:42,000
at tawam means three and things I mean 30 so it's the end they happen of the

20
00:02:42,000 --> 00:02:50,520
33 and it's called that because there were these 33 companions who went to

21
00:02:50,520 --> 00:02:57,560
heaven together and they're led by Sakka Sakka is the king of the angels of

22
00:02:57,560 --> 00:03:04,240
the 33 and so I guess a group of angels who live in their their own realm but

23
00:03:04,240 --> 00:03:11,080
somehow take an interest in the earth and so the four great kings report to

24
00:03:11,080 --> 00:03:16,480
them four great kings are responsible for making sure that everything on

25
00:03:16,480 --> 00:03:23,640
earth is I guess going according to whatever divine plan that they have I've all

26
00:03:23,640 --> 00:03:29,520
this sounds fairly theistic or deistic well I suppose you could call it

27
00:03:29,520 --> 00:03:33,640
deistic although I'm not even sure quite what that word means but there's

28
00:03:33,640 --> 00:03:38,600
and there you know there is a sense of that in the time of the Buddha people

29
00:03:38,600 --> 00:03:43,320
were obviously worshiping these angels and so the Buddha was acknowledging

30
00:03:43,320 --> 00:03:54,040
the existence of the of such angels but what he denied was the efficacy of

31
00:03:54,040 --> 00:04:00,840
prayer or the importance of prayer that sure you could you could pray and

32
00:04:00,840 --> 00:04:09,640
beseech the gods for blessings or for mercy or so on but there's no reason to

33
00:04:09,640 --> 00:04:23,520
think that they would comply nor is there any benefit from or any spiritual

34
00:04:23,520 --> 00:04:35,760
benefit to such activities they certainly didn't have the power to enlighten

35
00:04:35,760 --> 00:04:41,840
one or to bring the highest benefit and I guess that's really where Buddhism

36
00:04:41,840 --> 00:04:48,200
goes beyond the theistic religion it's it doesn't deny the fact that angels

37
00:04:48,200 --> 00:04:54,640
can and when you might call gods can potentially help human beings it's it's

38
00:04:54,640 --> 00:04:59,160
not even the point to suggest that they that God can't move mountains as they

39
00:04:59,160 --> 00:05:07,680
say in Christianity which anyway it's that moving mountains wouldn't really

40
00:05:07,680 --> 00:05:15,640
help you know if if you pray to God and God were to answer all of your prayers

41
00:05:15,640 --> 00:05:21,000
still wouldn't mean much because well the prayers that God can answer are me

42
00:05:21,000 --> 00:05:25,680
I become enlightened me I'd be free from suffering me I'd be free from greed

43
00:05:25,680 --> 00:05:31,240
anger and delusion me I become a better person it just can't happen doesn't

44
00:05:31,240 --> 00:05:37,920
come from the grace of God the grace of God sure maybe you could get rich you

45
00:05:37,920 --> 00:05:49,040
could live in luxury but you couldn't go to heaven God doesn't have the power

46
00:05:49,040 --> 00:05:57,880
to bring someone to heaven or prevent them from entering heaven nor does any

47
00:05:57,880 --> 00:06:10,280
God or angel have the power to send someone to hell not with some

48
00:06:10,280 --> 00:06:16,360
qualification because of course beings can affect other beings but the

49
00:06:16,360 --> 00:06:23,080
alternate power comes from within if your mind is pure no one can send you to

50
00:06:23,080 --> 00:06:27,040
hell if you mind is impure it's possible to instigate that's certainly

51
00:06:27,040 --> 00:06:33,640
possible that angels could manipulate you and trick you and into doing

52
00:06:33,640 --> 00:06:40,560
leads that would send you to hell manipulate a person into sending them to

53
00:06:40,560 --> 00:06:44,440
heaven so I mean the only it just means that there is an acknowledgement

54
00:06:44,440 --> 00:06:49,800
whether you believe this or not that there are certain beings that are not

55
00:06:49,800 --> 00:06:56,280
quantity not a qualitatively different from human beings in the sense that

56
00:06:56,280 --> 00:07:03,240
they still have defilements and they still have lifespans and they're still

57
00:07:03,240 --> 00:07:12,160
limited to affecting the physical realm they can't actually manipulate another

58
00:07:12,160 --> 00:07:26,000
person's mind not directly but anyway there there is some order to the

59
00:07:26,000 --> 00:07:29,400
angel realms if for those of you who are interested and curious about these

60
00:07:29,400 --> 00:07:32,840
things what's interesting about this suit and what really makes it Buddhist

61
00:07:32,840 --> 00:07:39,200
because obviously talking about angels isn't particular in Buddhist is that

62
00:07:39,200 --> 00:07:44,120
Sakai is a Buddhist the king of the gods of the 33 is a follower of the

63
00:07:44,120 --> 00:07:56,160
Buddha and they're concerned about the activities of human beings the

64
00:07:56,160 --> 00:08:00,600
goodness of human beings it's a group and it's not to say that all angels will

65
00:08:00,600 --> 00:08:06,920
be like this but this group of angels particularly is concerned as to

66
00:08:06,920 --> 00:08:16,640
whether human beings are honoring religious people honoring elders behaving

67
00:08:16,640 --> 00:08:21,000
properly towards their father and mother whether they are keeping holy days

68
00:08:21,000 --> 00:08:28,120
in the sense of having religious observances spiritual practice on at least

69
00:08:28,120 --> 00:08:36,360
once a week and doing good deeds are they giving charity are they keeping

70
00:08:36,360 --> 00:08:40,720
morality are they practicing meditation but it's kind of funny why they're

71
00:08:40,720 --> 00:08:50,240
interested in this and somewhat self-serving or pragmatic I guess they're

72
00:08:50,240 --> 00:08:54,000
concerned with this because they say if the four great kings report to them

73
00:08:54,000 --> 00:08:59,080
that human beings are doing all these things then they get really happy because

74
00:08:59,080 --> 00:09:02,720
they say oh that means that's great that means there'll be more angels in the

75
00:09:02,720 --> 00:09:08,760
future it means there'll be more of them more people coming and will have a

76
00:09:08,760 --> 00:09:18,080
greater retinue will become it's actually somewhat of a base desire to swell

77
00:09:18,080 --> 00:09:24,400
their ranks and they're and if they find out that people are doing bad things

78
00:09:24,400 --> 00:09:28,160
and and not doing good things and they're sad because there won't be as many

79
00:09:28,160 --> 00:09:36,080
angels in the future I mean for us it simply means it's a reminder that the

80
00:09:36,080 --> 00:09:39,200
only way to go to heaven really is to be a good person is to have a pure

81
00:09:39,200 --> 00:09:46,280
mind and all of that comes through doing good deeds being good to other people

82
00:09:46,280 --> 00:09:51,400
being respectful to other people being kind having a pure mind not having

83
00:09:51,400 --> 00:09:57,080
ulterior motives or a crooked treacherous mind not being obsessed with base

84
00:09:57,080 --> 00:10:06,520
desires or or hatred or aversion grudging grudges if your mind can stay pure in

85
00:10:06,520 --> 00:10:18,080
this to file the world then and only then can you free yourself from it from the

86
00:10:18,080 --> 00:10:27,160
coarseness from the basis from the mundane to enter into something more pure

87
00:10:27,160 --> 00:10:35,440
more peaceful more sublime and then and then there's something funny that

88
00:10:35,440 --> 00:10:43,960
Saqa the King of the Angels reminds the angels he says the person who would be

89
00:10:43,960 --> 00:10:48,880
like me should basically do lots of good deeds the person who'd be like me

90
00:10:48,880 --> 00:10:55,600
should keep the holy keep the holy day he says keep the

91
00:10:55,600 --> 00:11:02,080
both the time which means this day where people keep the eight preset and then

92
00:11:02,080 --> 00:11:07,800
Buddhist says you know when he said this it wasn't well said it wasn't it

93
00:11:07,800 --> 00:11:13,520
wasn't a proper verse to say not because it's it's not good to keep the religious

94
00:11:13,520 --> 00:11:18,000
preset sort of practice goodness and I can say but because Saqa you know

95
00:11:18,000 --> 00:11:22,320
what who should who no one should should strive to be like Saqa

96
00:11:22,320 --> 00:11:28,720
because Saqa is still not devoid of last hatred and delusion and he says but I

97
00:11:28,720 --> 00:11:33,920
say the same thing a person who would be like me should observe all of these

98
00:11:33,920 --> 00:11:41,680
things because I am because I know not me sorry it's not me a beaucoup who has

99
00:11:41,680 --> 00:11:45,760
who has become an aran someone who has become enlightened not just the Buddha

100
00:11:45,760 --> 00:11:48,720
but anyone who's become enlightened should therefore say the person who would

101
00:11:48,720 --> 00:12:06,240
be like me so I mean this kind of puts angels in their place and it's really

102
00:12:06,240 --> 00:12:10,520
how his Buddhists we look at the world you know we don't see angels most

103
00:12:10,520 --> 00:12:15,080
people will live their lives never having any evidence that these beings

104
00:12:15,080 --> 00:12:22,280
exist but in so far as you might think that they do exist there's still

105
00:12:22,280 --> 00:12:32,560
there's still subject to the same laws and and regulations of the universe

106
00:12:32,560 --> 00:12:40,560
that goodness goodness is what leads to heaven and evil is what leads to

107
00:12:40,560 --> 00:12:45,360
hell they don't these beings don't have the power to make people better people

108
00:12:45,360 --> 00:12:51,240
but they're happy to know that people do they rejoice as all good people

109
00:12:51,240 --> 00:12:59,200
should I mean we'd rejoice for we see more people meditating it means there

110
00:12:59,200 --> 00:13:03,440
will be more meditators it's in fact not a terribly bad thing to think

111
00:13:03,440 --> 00:13:07,960
because it makes our practice easier makes it easier to do good deeds when

112
00:13:07,960 --> 00:13:13,240
other people are keen on good deeds when everyone's doing unwholesome

113
00:13:13,240 --> 00:13:22,160
and wholesome deeds and obsessed with the filements becomes more difficult to do

114
00:13:22,160 --> 00:13:33,320
good things because you get sucked you get drained by the negativity of others

115
00:13:33,320 --> 00:13:40,320
anyway so that's a little bit of demo for today anyway I'm just working

116
00:13:40,320 --> 00:13:56,320
and that's all about we have any questions we do Monday in practical way

117
00:13:56,320 --> 00:14:04,320
we're just talking but I should mention it first just a reminder to people

118
00:14:04,320 --> 00:14:18,080
that we have we may have young people coming on this site and any sort of adult

119
00:14:18,080 --> 00:14:28,520
conversation about sexual intercourse or other things I guess drugs alcohol

120
00:14:28,520 --> 00:14:37,240
should be moderated or moderate tempered so just be mindful of where we are

121
00:14:37,240 --> 00:14:47,240
and the fact that we're not all of age so not that anything inappropriate has

122
00:14:47,240 --> 00:14:59,640
been done or said but just a reminder that to keep it keep it PG or there's

123
00:14:59,640 --> 00:15:05,840
potential problems with talking about such things to people who are

124
00:15:05,840 --> 00:15:13,480
potentially underage okay that's all okay in practical insight meditation

125
00:15:13,480 --> 00:15:18,360
Mahasi Sayyadha talks about reflecting on certain things but to also notice

126
00:15:18,360 --> 00:15:22,480
reflecting to keep it to a minimum but you seem to be saying it's not

127
00:15:22,480 --> 00:15:26,080
necessary to think or reflect at all during meditation and it's only

128
00:15:26,080 --> 00:15:31,160
necessary to teach etc so are you disagreeing with him or am I missing

129
00:15:31,160 --> 00:15:35,400
something and how is it possible to garner insight without reflecting on your

130
00:15:35,400 --> 00:15:47,440
experience I don't think I said that it's not I like reflection has a place in

131
00:15:47,440 --> 00:15:56,280
terms of stepping back and reflecting on on the quality of your practice but

132
00:15:56,280 --> 00:16:00,120
the only benefit there is to adjust your practice if your practice is going

133
00:16:00,120 --> 00:16:06,840
fine I can see you know you know there's no benefit to reflecting on things

134
00:16:06,840 --> 00:16:13,840
that you've learned you know certainly reflecting is natural that it's

135
00:16:13,840 --> 00:16:19,240
something you should be mindful of but it's not the practice and I mean read

136
00:16:19,240 --> 00:16:24,080
with the Mahasi Sayyadha says about what we call nyana

137
00:16:24,080 --> 00:16:34,720
is one of the ten uppercu they say when your knowledge the knowledge is

138
00:16:34,720 --> 00:16:39,040
that you gain when you become obsessed with it fixate it on it when you start

139
00:16:39,040 --> 00:16:44,840
thinking about it that's actually to the detriment of your practice so the

140
00:16:44,840 --> 00:16:51,480
question about how could insight arise if you don't reflect I mean that's

141
00:16:51,480 --> 00:16:57,760
not how in what insight is insight is a direct realization of things it's

142
00:16:57,760 --> 00:17:02,520
not reflection when you see that things are arising and ceasing that's

143
00:17:02,520 --> 00:17:07,200
insight and the more you see that the closer you'll get to letting go once you

144
00:17:07,200 --> 00:17:13,280
see it clearly you'll just have kind of like an epiphany where you were not

145
00:17:13,280 --> 00:17:19,000
even not even a thought but it's just an observation that everything that

146
00:17:19,000 --> 00:17:22,080
arises ceases and there's nothing worth clinging to and in the mind that's

147
00:17:22,080 --> 00:17:29,200
go there's no there's no place in there for reflection I think it's a

148
00:17:29,200 --> 00:17:34,240
misunderstanding of what insight is inside is not a thought inside is actually

149
00:17:34,240 --> 00:17:38,800
we pass and a bus and I mean seeing we means clearly or especially or whatever

150
00:17:38,800 --> 00:17:44,040
you want to translate we really means with wisdom banya banya actually

151
00:17:44,040 --> 00:17:51,240
just means banya knowledge but complete you know when you see when you know

152
00:17:51,240 --> 00:17:54,360
that this is rising when you know that this is falling that's actually wisdom

153
00:17:54,360 --> 00:18:01,280
when you see it arising and ceasing that's wisdom that's all you need

154
00:18:05,480 --> 00:18:10,720
I don't think Mahasim Saiya does suggest otherwise but you'd have to

155
00:18:10,720 --> 00:18:15,320
probably read the Burmese or be sure that it was properly translated and then

156
00:18:15,320 --> 00:18:18,480
you have to understand that you know he's generally he generally makes

157
00:18:18,480 --> 00:18:24,520
allowances you know if he's actually advising people to stop and reflect and

158
00:18:24,520 --> 00:18:30,720
I'd be quite surprised but on the other hand there is an advice for stepping

159
00:18:30,720 --> 00:18:35,840
back and reflecting in so far as it helps you see whether your practice is

160
00:18:35,840 --> 00:18:40,000
correct whether you're actually doing things properly and reflect on

161
00:18:40,000 --> 00:18:45,160
whether you're you're adequately you know you're really missing something or

162
00:18:45,160 --> 00:18:49,540
there's something to be adjusted that's called the monks one of the four

163
00:18:49,540 --> 00:18:53,760
you get part of one of the four roads to success

164
00:18:56,600 --> 00:19:01,120
dear Bhante what is the difference between a sense of apathy versus an

165
00:19:01,120 --> 00:19:05,840
impartial stance towards something thank you

166
00:19:05,840 --> 00:19:14,440
I don't know I mean they're just words you'd have to see what the actual state

167
00:19:14,440 --> 00:19:26,240
was someone who says I don't care I mean it's it sounds like it actually exists

168
00:19:26,240 --> 00:19:36,800
but it's it's just words and it's just a view and it's a lot of ego in the

169
00:19:36,800 --> 00:19:45,400
sense of I you know I am above that kind of thing but they're just words I

170
00:19:45,400 --> 00:19:50,080
mean when an experience occurs are you actually an

171
00:19:50,080 --> 00:19:58,160
un-un-un-reactive towards it because such people actually when they do when

172
00:19:58,160 --> 00:20:02,600
they are presented with something that does affect them and they react

173
00:20:02,600 --> 00:20:07,240
eventually the point is not everyone is reactive towards towards everything

174
00:20:07,240 --> 00:20:14,600
for some people some things are not going to create a reaction but that's you

175
00:20:14,600 --> 00:20:22,240
know it's not a sign that they are apathetic it's just a sign that they let

176
00:20:22,240 --> 00:20:28,040
that thing is doesn't create it doesn't have any kind of history with that

177
00:20:28,040 --> 00:20:35,360
person so you know for some people loud noises is obnoxious here in the West

178
00:20:35,360 --> 00:20:40,240
if your neighbors are having a party you get quite upset in Asia if your

179
00:20:40,240 --> 00:20:44,880
neighbors are having a party people don't even seem to hear it it's quite

180
00:20:44,880 --> 00:20:49,120
interesting to see some of the differences from culture to culture the sort of

181
00:20:49,120 --> 00:20:58,280
things that bothers bother people from different cultures a general sense of

182
00:20:58,280 --> 00:21:07,880
apathy I mean there can be a delusion delusion is is generally associated

183
00:21:07,880 --> 00:21:11,360
with with equanimity and I guess that's really the technical answer is that

184
00:21:11,360 --> 00:21:18,080
questions whether it's associated with delusion but the delusion would be kind

185
00:21:18,080 --> 00:21:26,920
of this distracted thought if a person is engaged in mental

186
00:21:26,920 --> 00:21:31,480
fermentation if they're thinking about things and pondering and wondering and

187
00:21:31,480 --> 00:21:38,480
confused and that kind of thing that's delusion and there's there's equanimity with it

188
00:21:44,120 --> 00:21:51,160
and then there's the the other the wholesome kind is with equanimity one does

189
00:21:51,160 --> 00:21:57,680
something good or one calculates wholesomeness in the mind through meditation

190
00:21:57,680 --> 00:22:01,520
but the equanimity you know the I guess the point of the equanimity is the same

191
00:22:01,520 --> 00:22:06,720
but there's nothing to do with the equanimity it's the delusion whether in the

192
00:22:06,720 --> 00:22:15,600
mind there's ego or there's confusion or there's mental distraction whether the

193
00:22:15,600 --> 00:22:20,520
mind is clear or the mind is is clouded

194
00:22:20,520 --> 00:22:31,000
Thank you. Thank you. Is there any point being grateful karma is going to

195
00:22:31,000 --> 00:22:35,400
manifest anyways could or bad how should we include gratitude in our

196
00:22:35,400 --> 00:22:44,240
practice? Absolutely I mean that's not a good outlook. I mean you think okay I

197
00:22:44,240 --> 00:22:46,880
shouldn't be grateful because that person's going to get what's coming to

198
00:22:46,880 --> 00:22:50,840
them anyway who cares who cares whether they get what's coming to them or not

199
00:22:50,840 --> 00:22:56,800
that's not the point gratitude is a wonderful state of mind gratitude is we

200
00:22:56,800 --> 00:23:00,520
don't worry about other people we're not grateful so that it helps the other

201
00:23:00,520 --> 00:23:07,720
person we're grateful so that we don't become mean and terrible people who have

202
00:23:07,720 --> 00:23:13,080
no sense of appreciation for the good deeds of others

203
00:23:13,080 --> 00:23:17,480
gratitude is something that should come naturally and if you find yourself

204
00:23:17,480 --> 00:23:26,480
ungrateful or un-mindful of the good deeds of others you should not that

205
00:23:26,480 --> 00:23:30,920
and try to understand why is it because himself is yourself absorbed

206
00:23:30,920 --> 00:23:36,720
that's generally based in due to either one of the two a strong sense of greed

207
00:23:36,720 --> 00:23:45,960
and or a strong sense of a strong delusion being self-absorbed and or you

208
00:23:45,960 --> 00:23:50,080
know it can also come from anger I suppose if you're totally obsessed with your

209
00:23:50,080 --> 00:23:57,280
own suffering so that when people help you you just fix and focus on your own

210
00:23:57,280 --> 00:24:05,280
problems and how your own problems are solved or not solved than thinking

211
00:24:05,280 --> 00:24:11,560
about the person who helped you gratitude is most apparent in those who have

212
00:24:11,560 --> 00:24:15,960
the least attachment those who are at least concerned about their own well-being

213
00:24:15,960 --> 00:24:24,160
people who are most able to appreciate the good things in others are those who

214
00:24:24,160 --> 00:24:28,520
have good things in themselves or those who aren't concerned about their own

215
00:24:28,520 --> 00:24:33,840
good things in the sense that they don't have feelings of guilt or worry the

216
00:24:33,840 --> 00:24:37,920
people who have lots of unpleasant things inside tend to be too self-absorbed

217
00:24:37,920 --> 00:24:43,640
worried about their own well-being concerned and greedy or angry and

218
00:24:43,640 --> 00:24:48,200
frustrated and upset and deluded and conceded you can see how all of those

219
00:24:48,200 --> 00:24:52,360
things get in the way of of gratitude so when all of those gone it's very easy

220
00:24:52,360 --> 00:24:57,920
to appreciate the good things in others you're able to mindful be mindful of

221
00:24:57,920 --> 00:25:05,800
the truth sometimes that this person helped you I wouldn't try to cultivate it

222
00:25:05,800 --> 00:25:10,960
too much I'd just be more aware as to whether you have it or not and if you don't

223
00:25:10,960 --> 00:25:17,600
have it there's a sign it's a sign that you've got something blocking it

224
00:25:22,400 --> 00:25:27,160
can you change the life broadcast to a different time is there's someone else

225
00:25:27,160 --> 00:25:33,560
who would want that question I can change it to another time this time happens

226
00:25:33,560 --> 00:25:40,080
to be fairly convenient for me won't be convenient on Monday soon but apart

227
00:25:40,080 --> 00:25:44,240
from that I can't think of a better time for me I can do it early afternoon

228
00:25:44,240 --> 00:25:49,440
but that would probably be a bad choice for most of our viewers since most of

229
00:25:49,440 --> 00:25:56,280
our viewers are in North America which I know doesn't really help those of you

230
00:25:56,280 --> 00:26:01,440
who are in Europe but look at the if you look at the meditation list majority

231
00:26:01,440 --> 00:26:11,000
or USA if you Canada and we got a bunch of Mexico and so on which I mean the

232
00:26:11,000 --> 00:26:14,840
question is what is the cause what is the effect whether it's you know because

233
00:26:14,840 --> 00:26:24,520
we're broadcasting evening right that could just be because of the time

234
00:26:24,520 --> 00:26:30,360
there's the Europeans aren't going to be on much anymore except for rustlin

235
00:26:30,360 --> 00:26:37,480
rustlin yeah I had just read down a little bit it was because the person

236
00:26:37,480 --> 00:26:43,440
often had some follow-up questions to ask during the life broadcast but

237
00:26:43,440 --> 00:26:49,760
decided that he would be patient and use it as a practice or you could get

238
00:26:49,760 --> 00:26:54,320
really fancy but they have Tuesday and Thursday's early and Monday Wednesday

239
00:26:54,320 --> 00:26:58,640
Friday at the normal time and I can't even keep track of my schedule as it is

240
00:26:58,640 --> 00:27:07,360
I don't know what day it is at most days keeping it simple is good too

241
00:27:07,360 --> 00:27:12,000
Monday during my last meditation session it suddenly felt like my legs were

242
00:27:12,000 --> 00:27:16,720
not covered by my skin the legs still felt like mine but not the skin it was

243
00:27:16,720 --> 00:27:23,000
sickening to my stomach and I still practicing right well that's not practice

244
00:27:23,000 --> 00:27:27,720
you see that's that's experience the practice is how you relate to that

245
00:27:27,720 --> 00:27:34,680
experience be very clear that these are two distinct things what you experience

246
00:27:34,680 --> 00:27:39,560
and what arises based on your practice it's not the practice whether it be

247
00:27:39,560 --> 00:27:44,360
knowledge or whether it be strange experiences like this or even whether it be

248
00:27:44,360 --> 00:27:49,760
unpleasant experiences defilements if anger arises that's not the

249
00:27:49,760 --> 00:27:54,160
practice it's not like oh I'm very angry I must be doing something wrong

250
00:27:54,160 --> 00:28:00,760
anger is an experience it's a mental reaction it is a bad thing but don't

251
00:28:00,760 --> 00:28:04,600
worry about that that's not your concern it happened your concern is what you're

252
00:28:04,600 --> 00:28:08,800
going to do about it so when you have this experience how are you going to

253
00:28:08,800 --> 00:28:13,280
react to it back there discussed it is potentially problematic because there's

254
00:28:13,280 --> 00:28:18,400
a version disliking so that's not the proper reaction but now that that's

255
00:28:18,400 --> 00:28:23,480
happened you should say disliking disliking or disgusting disliking is probably

256
00:28:23,480 --> 00:28:29,280
better if you're just aware of it then you would say knowing knowing aware

257
00:28:29,280 --> 00:28:36,520
or something it's a feeling you might say feeling and that's the

258
00:28:36,520 --> 00:28:41,240
practice then you see it comes and it goes I think the problem is it's

259
00:28:41,240 --> 00:28:52,240
practice is terribly unremarkable the practice itself is too simple it's

260
00:28:52,240 --> 00:28:58,600
unremarkable that's it you just say thinking thinking seeing seeing feelings

261
00:28:58,600 --> 00:29:04,440
in angry angry but it's made up for the fact that when you do that there's

262
00:29:04,440 --> 00:29:08,600
fireworks you know the things that you're observing are incredibly

263
00:29:08,600 --> 00:29:14,440
interesting there's a real transformation that goes on so be clear that the

264
00:29:14,440 --> 00:29:20,440
practice is incredibly simple it's not much to it but the experiences that

265
00:29:20,440 --> 00:29:27,920
you're going to have are diverse and very terribly exciting if you

266
00:29:27,920 --> 00:29:38,000
let them be but of course you shouldn't it should be mindful of it

267
00:29:38,000 --> 00:29:42,360
is it best practice to always keep the eyes open or can the eyes be closed

268
00:29:42,360 --> 00:29:46,600
while chewing and swallowing it certainly can be closed while chewing and

269
00:29:46,600 --> 00:29:53,240
swallowing there's no rule harder fast but they'll be really keen on it

270
00:29:53,240 --> 00:29:59,400
closing your eyes is a good thing that's your focus on focus more on the the

271
00:29:59,400 --> 00:30:05,360
mouth and the throat taste and so on

272
00:30:07,160 --> 00:30:13,000
well the current Android app still be working when the new site goes live no

273
00:30:13,000 --> 00:30:19,080
we probably could make it work if someone wanted to make a better Android app

274
00:30:19,080 --> 00:30:24,440
and be great but on the other hand it might be nice just to use the website as it

275
00:30:24,440 --> 00:30:31,320
is because then we only have to work on one thing but the new site looks a lot

276
00:30:31,320 --> 00:30:37,160
like the Android app if you've seen it so yeah give up the Android app once

277
00:30:37,160 --> 00:30:49,320
the what's the new site goes live no there's an iOS app apparently but I think

278
00:30:49,320 --> 00:30:52,040
that may work

279
00:30:55,640 --> 00:31:00,840
and you still can sign in to the regular site on a phone so if it's just a

280
00:31:00,840 --> 00:31:06,440
matter of wanting to be portable you can still log your time in just through

281
00:31:06,440 --> 00:31:10,920
regular browser on the phone and done that well yeah the new the new site

282
00:31:10,920 --> 00:31:15,000
will look a lot like the Android app anyway it won't be as hard to do like

283
00:31:15,000 --> 00:31:20,120
this one you have to find the little button but on the new one it'll be much

284
00:31:20,120 --> 00:31:23,560
more mobile friendly

285
00:31:24,840 --> 00:31:29,320
my sitting meditation is usually stressful and it seems to be getting worse

286
00:31:29,320 --> 00:31:33,800
it feels like I'm forcing myself to stay seated do I just continue to work

287
00:31:33,800 --> 00:31:40,040
really yeah I mean this is a case where you might want to use my lungs as step

288
00:31:40,040 --> 00:31:44,840
back and reflect on what you might be doing wrong what you might be missing

289
00:31:44,840 --> 00:31:49,800
not doing wrong exactly but the aversion is what you should be meditating

290
00:31:49,800 --> 00:31:55,000
one you know if you feel stressed and focus on the stress it's not easy and

291
00:31:55,000 --> 00:31:59,800
this is where the fireworks happen this is where the simple technique does a

292
00:31:59,800 --> 00:32:05,480
number on you it really messes with you you know challenges you

293
00:32:05,480 --> 00:32:09,000
it's called the it sounds like probably

294
00:32:09,000 --> 00:32:12,440
why not probably you know from the sounds of it you're

295
00:32:12,440 --> 00:32:16,680
you're doing fine because you're cultivating patience

296
00:32:16,680 --> 00:32:19,960
if it feels like you're forcing then it's not that you're forcing it's that

297
00:32:19,960 --> 00:32:24,520
you're missing something you're not actually being mindful of the aversion

298
00:32:24,520 --> 00:32:28,280
try and be as mindful of the aversion as you can it's quick

299
00:32:28,280 --> 00:32:32,200
catch what's really happening because there's not such thing as meditation

300
00:32:32,200 --> 00:32:36,440
there's not you don't have sitting meditation it doesn't exist

301
00:32:36,440 --> 00:32:39,400
what happens when you sit down and close your eyes

302
00:32:39,400 --> 00:32:45,160
and then you watch the stomach now the tendency probably is to try and force

303
00:32:45,160 --> 00:32:49,480
the stomach to rise and fall which is an interesting thing it's an

304
00:32:49,480 --> 00:32:53,400
interesting fact that that your mind you know this is the case

305
00:32:53,400 --> 00:33:00,680
that your mind is doing that so the things that your mind that our mind does

306
00:33:00,680 --> 00:33:07,960
and is what we're interested in we're interested to learn how the mind works

307
00:33:07,960 --> 00:33:12,680
to learn about the good things and the bad things in the mind

308
00:33:12,680 --> 00:33:15,720
what we're doing right what we're doing wrong it sounds like probably like

309
00:33:15,720 --> 00:33:20,280
all of us you're doing something wrong so just keep watching and you'll

310
00:33:20,280 --> 00:33:27,480
slowly see oh I'm doing this all wrong boy I should just stop

311
00:33:30,840 --> 00:33:35,560
can I do mindful meditation without mantras

312
00:33:36,280 --> 00:33:39,000
you can but you'd have to find a different teacher and a different

313
00:33:39,000 --> 00:33:43,320
tradition because some people say that you don't you shouldn't use mantras

314
00:33:43,320 --> 00:33:48,200
in our tradition we say you should and it's actually important to use

315
00:33:48,200 --> 00:33:53,000
mantras

316
00:33:54,040 --> 00:33:58,440
so not in our tradition sorry I mean why would you want to I guess it's the

317
00:33:58,440 --> 00:34:01,720
question of course I understand why people want to but

318
00:34:01,720 --> 00:34:06,120
just believe you're shaking your head because they're so beneficial

319
00:34:06,120 --> 00:34:09,960
that's the most powerful thing we have is this tool

320
00:34:09,960 --> 00:34:15,880
reminding ourselves that the best meditation technique I can think of

321
00:34:15,880 --> 00:34:18,760
people that don't use them are missing out and don't know what they're missing

322
00:34:18,760 --> 00:34:20,920
out

323
00:34:26,360 --> 00:34:29,960
you're all caught up on questions one day

324
00:34:29,960 --> 00:34:35,960
all right then that's all for tonight thanks Robin for your help

325
00:34:35,960 --> 00:34:41,480
thanks everyone for coming out and for meditating and for your questions

326
00:34:41,480 --> 00:35:06,320
thank you

