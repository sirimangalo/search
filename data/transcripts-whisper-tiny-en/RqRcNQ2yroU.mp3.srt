1
00:00:00,000 --> 00:00:19,800
Welcome back to Ask a Monk.

2
00:00:19,800 --> 00:00:29,320
Today I will be answering the question as to what we should do for those people who are

3
00:00:29,320 --> 00:00:35,520
suffering from addiction and as a result might turn away from the meditation practice

4
00:00:35,520 --> 00:00:40,720
or might not be able to immediately step into the meditation practice and the suggestion

5
00:00:40,720 --> 00:00:47,200
is that possibly for some people meditation isn't the answer or isn't the immediate answer

6
00:00:47,200 --> 00:00:56,280
that rather such people should undertake traditional therapy or rehab or so on before taking

7
00:00:56,280 --> 00:00:58,480
on the meditation.

8
00:00:58,480 --> 00:01:05,880
Now I think it's important to understand that there are answers besides the meditation

9
00:01:05,880 --> 00:01:06,880
practice.

10
00:01:06,880 --> 00:01:10,880
The meditation practice isn't the only thing that's necessary.

11
00:01:10,880 --> 00:01:16,360
This is why the Buddha recommended morality as well because ideally we do away with these

12
00:01:16,360 --> 00:01:17,360
states.

13
00:01:17,360 --> 00:01:23,240
Ideally we give them up when we start to practice meditation and he himself said or made

14
00:01:23,240 --> 00:01:31,840
it quite clear in different ways that the meditation relies on morality and without morality

15
00:01:31,840 --> 00:01:34,840
it's not truly going to succeed.

16
00:01:34,840 --> 00:01:41,720
Now the problem with saying that one should avoid meditation or should not consider meditation

17
00:01:41,720 --> 00:01:49,400
is that it ignores the fact that meditation should indeed be a part or is a part of learning

18
00:01:49,400 --> 00:01:54,800
to keep morality, that even for people who can't keep basic morality who are stuck in

19
00:01:54,800 --> 00:02:06,360
addiction and have physical and mental addictions to substances or to the hormones in the

20
00:02:06,360 --> 00:02:12,680
body it was mentioned about masturbation in the person who asked the question was mentioning

21
00:02:12,680 --> 00:02:24,960
that and so on that there is that side but the meditation, the mental side is very much

22
00:02:24,960 --> 00:02:31,960
a part of the problem and that's very important to address.

23
00:02:31,960 --> 00:02:39,040
Now when we practice meditation we have to keep in mind the physical side and we have

24
00:02:39,040 --> 00:02:45,560
to understand that our state of being is going to affect our meditation is going to really

25
00:02:45,560 --> 00:02:51,560
set the tone of our meditation practice so we need to eat the right food, we need to

26
00:02:51,560 --> 00:02:58,560
look after our bodies and our health in terms of medicine and so on we have to find

27
00:02:58,560 --> 00:03:02,520
a suitable place to live and so on.

28
00:03:02,520 --> 00:03:08,480
Along with the many other aspects of our practice that we have to keep in mind so there's

29
00:03:08,480 --> 00:03:18,320
no reason not to practice meditation but especially for people who are suffering from

30
00:03:18,320 --> 00:03:23,680
severe forms of addiction, extreme forms of addiction, there are going to be many other

31
00:03:23,680 --> 00:03:28,640
things that they'll have to keep in mind, the Buddha gave a talk on all the many ways

32
00:03:28,640 --> 00:03:34,520
to do away with the problems in the mind for instance we have to guard our senses so we

33
00:03:34,520 --> 00:03:45,600
shouldn't just look around the stare at things and watch and engage in the pleasures

34
00:03:45,600 --> 00:03:51,920
of the sense, we should restrain ourselves in terms of food, in terms of entertainment

35
00:03:51,920 --> 00:04:02,240
and so on, we should guard our faculties so that we are mindful and we are able to keep

36
00:04:02,240 --> 00:04:09,120
track of our state of mind and of the world around us so to not get caught up in entertainment

37
00:04:09,120 --> 00:04:10,120
and pleasure.

38
00:04:10,120 --> 00:04:16,120
I've given talks on this before the various parts of the practice so that certainly is

39
00:04:16,120 --> 00:04:24,960
true but the concern is to whether people who take up the meditation practice and aren't

40
00:04:24,960 --> 00:04:32,200
capable that they might therefore turn away from the practice and give it up is it seems

41
00:04:32,200 --> 00:04:40,440
a little bit specious that the truth of the matter is and what we see is there are reasons

42
00:04:40,440 --> 00:04:50,080
why they give up and to us to an extent we can mitigate these, it's often because there's

43
00:04:50,080 --> 00:04:56,880
improper instruction, it's often because there isn't the comprehensive practice, it's often

44
00:04:56,880 --> 00:05:02,160
because of the surrounding so every person's position, every person's situation

45
00:05:02,160 --> 00:05:10,120
is different, what's important is how we address this issue of people giving up the practice

46
00:05:10,120 --> 00:05:14,560
and leaving it behind, the first thing we should say is that just because a person gives

47
00:05:14,560 --> 00:05:19,920
up the practice doesn't mean they haven't gained anything, so a person might begin to practice

48
00:05:19,920 --> 00:05:28,280
meditation and get to a certain level or get to a certain point and then give it up or

49
00:05:28,280 --> 00:05:35,720
put it aside, now we shouldn't therefore be discouraged or think that this person is useless

50
00:05:35,720 --> 00:05:42,920
or that they have no potential in the meditation practice, it can be that after some time

51
00:05:42,920 --> 00:05:47,520
they'll come back to it and what we gain the things that we do especially things that

52
00:05:47,520 --> 00:05:53,680
affect our state of mind have a profound effect on our psyche and they stay deeply ingrained

53
00:05:53,680 --> 00:05:57,840
in our memory, meditation is something that it's very difficult to forget and people

54
00:05:57,840 --> 00:06:03,840
can always come back to it, just learning the basics, the technique of meditation without

55
00:06:03,840 --> 00:06:10,040
even practicing it can be a great thing because in times of need it often comes back

56
00:06:10,040 --> 00:06:17,600
and people do take up the practice in earnest, so I think that's the first point that

57
00:06:17,600 --> 00:06:27,720
I would make, the second one is that environment plays a great role in addiction recovery

58
00:06:27,720 --> 00:06:35,160
so the physical aspects of addiction are obvious and environment isn't going to get rid

59
00:06:35,160 --> 00:06:38,160
of those no matter where you are, you still have the hormones coming up, you still have

60
00:06:38,160 --> 00:06:46,840
the chemical reactions and so on, the physical craving but at least half of the problem

61
00:06:46,840 --> 00:06:52,480
actually much more of the problem is the mental side and that you can influence by your

62
00:06:52,480 --> 00:06:57,720
environment, by the people you surround yourself with, by the interactions you have, by the

63
00:06:57,720 --> 00:07:02,800
situations that you get yourself into, and obviously if all of your friends are addicted

64
00:07:02,800 --> 00:07:09,240
as well, all of your friends go out to bars and drinking or do drugs or so on, or if

65
00:07:09,240 --> 00:07:17,200
there's this hypersexuality in the world around you, then watching television or going

66
00:07:17,200 --> 00:07:22,720
to the mall, going to the beach and so on and seeing the objects of your desire, then obviously

67
00:07:22,720 --> 00:07:29,240
it's going to be much more difficult for you to overcome the states, now this is where

68
00:07:29,240 --> 00:07:35,160
meditation can excel because a meditation center is pretty much the ideal environment,

69
00:07:35,160 --> 00:07:41,160
you're surrounded by people who are interested in meditation, who are trying to purify

70
00:07:41,160 --> 00:07:46,640
their own minds, who are supportive, who are talking about the same things and are encouraging

71
00:07:46,640 --> 00:07:51,360
each other in the same things, you have people talking about the meditation practice

72
00:07:51,360 --> 00:07:55,680
and teaching the meditation practice, you have a really supportive environment and that's

73
00:07:55,680 --> 00:07:58,480
really important, that makes a real difference.

74
00:07:58,480 --> 00:08:05,640
I think the people who turn away most often are those people who have never had that environment

75
00:08:05,640 --> 00:08:10,960
or who have not had it on a long term basis so people will go to a retreat for 10 days

76
00:08:10,960 --> 00:08:14,880
and all of the people come together for 10 days but no one's living there, no one's staying

77
00:08:14,880 --> 00:08:21,880
there, so there isn't the community feeling, you don't feel like you're really living

78
00:08:21,880 --> 00:08:26,000
in this place, in this environment, it's quite different when you have a monastery that

79
00:08:26,000 --> 00:08:30,520
you go to and there are people staying there and living there and you can live for a month

80
00:08:30,520 --> 00:08:39,280
or a year and undertake the practice as a lifestyle, that's a real great support and

81
00:08:39,280 --> 00:08:43,160
you'll see that in addiction therapy as well, they advise the same thing that it should

82
00:08:43,160 --> 00:08:50,280
be residential, so the meditation in that sense provides an excellent form of addiction

83
00:08:50,280 --> 00:08:56,760
therapy just by the basic environment.

84
00:08:56,760 --> 00:09:02,280
Another thing is in regards to the physical addiction itself and I've said this, I've talked

85
00:09:02,280 --> 00:09:10,320
about this before, that physical addiction is one thing but part of our practice is to not

86
00:09:10,320 --> 00:09:15,840
be free from the physical addiction but to rise above it, to see that it's only a physical

87
00:09:15,840 --> 00:09:20,520
reaction, the cravings that occur in the mind are actually not cravings, they're just

88
00:09:20,520 --> 00:09:25,640
physical processes, so someone who's addicted to nicotine, for example, or someone who's

89
00:09:25,640 --> 00:09:34,280
addicted to the sexual hormones, this is only the physical side, it's something that arises

90
00:09:34,280 --> 00:09:41,600
and ceases, it's actually neutral, it's only our deeply ingrained reactions to the

91
00:09:41,600 --> 00:09:50,000
physical that causes the problem, so if we can simply see the feelings, the sensations

92
00:09:50,000 --> 00:09:56,720
for what they are, the sensation of hormones arising, whatever the chemical interactions

93
00:09:56,720 --> 00:10:04,560
of the hormones of nicotine and the cravings in the brain and so on, then they'll cease

94
00:10:04,560 --> 00:10:11,000
to have any power over us, this is why I said actually the physical is not the real problem

95
00:10:11,000 --> 00:10:20,640
but and so for many people it's actually the lack of instruction, I know there are often

96
00:10:20,640 --> 00:10:24,800
people who will go to a meditation center and will not get proper instruction for whatever

97
00:10:24,800 --> 00:10:29,680
reason, sometimes it's because they don't listen and this comes back to the idea that many

98
00:10:29,680 --> 00:10:36,080
of people have pointed out already that you can't help everyone, so in the end it is true

99
00:10:36,080 --> 00:10:40,720
that and it's very much worth bearing in mind that we should never be frustrated when the

100
00:10:40,720 --> 00:10:45,440
people around us don't want to meditate, we should take it upon ourselves to meditate

101
00:10:45,440 --> 00:10:52,080
and that will have an effect on our friends and family and so on, but in the end it's

102
00:10:52,080 --> 00:10:56,920
up to the individual and there's so many people in the world who won't ever meditate,

103
00:10:56,920 --> 00:11:02,760
not in this life, which is why I said give people what you can and help people as you

104
00:11:02,760 --> 00:11:16,000
can and don't expect too much, but on the other hand if we do give and if we are clear

105
00:11:16,000 --> 00:11:22,240
and if we understand correctly what it is that what is the meditation and how should one

106
00:11:22,240 --> 00:11:27,920
practice meditation, I've never really had a problem, never found anyone who didn't gain

107
00:11:27,920 --> 00:11:32,640
benefit from the practice, you know you have to give up at a certain point and say that's

108
00:11:32,640 --> 00:11:37,680
all the person could gain and that's enough for them and not expect them or get frustrated

109
00:11:37,680 --> 00:11:44,720
when they don't get more or when they don't take it more seriously, but you have to, there

110
00:11:44,720 --> 00:11:50,120
has to be someone there to guide them and to instruct them, so I think it's important that

111
00:11:50,120 --> 00:11:59,640
we study and that we get clear in our own minds about the practice and try our best to

112
00:11:59,640 --> 00:12:03,840
give people the information, what they do with that information is up to them and I would

113
00:12:03,840 --> 00:12:10,400
submit that even just giving them the information as I said is a great thing, and I would

114
00:12:10,400 --> 00:12:14,440
never say to someone that you shouldn't meditate, you should do something else first,

115
00:12:14,440 --> 00:12:18,680
I would say there are many other things that you could do with the meditation, complementing

116
00:12:18,680 --> 00:12:23,800
the meditation, but meditation should be essential and eventually becomes really the only

117
00:12:23,800 --> 00:12:30,520
thing that you need once you understand and get it and experience the benefits and the results

118
00:12:30,520 --> 00:12:35,800
of the meditation, then your mind will incline towards it and you'll find that it more and

119
00:12:35,800 --> 00:12:41,640
more becomes your answer to just about every problem that you have, so there's an answer

120
00:12:41,640 --> 00:12:47,360
to this question that's been another episode of Ask a Monk, thank you all for tuning in

121
00:12:47,360 --> 00:12:53,720
and hope to see you on the forum submitting your own questions and your own answers, I'd

122
00:12:53,720 --> 00:12:59,040
like to thank everyone for submitting answers, it certainly makes my job easier to have people

123
00:12:59,040 --> 00:13:05,400
who have, you know, in this way I've studied the meditation, it's not my teaching, but

124
00:13:05,400 --> 00:13:12,560
I've studied from the Buddha's teaching and this tradition of the Buddha's teaching, this

125
00:13:12,560 --> 00:13:18,160
tradition based on the Buddha's teaching and they're able to use that to help others, it's

126
00:13:18,160 --> 00:13:23,360
great to see and I'd encourage you to do that not only here but also, you know, in

127
00:13:23,360 --> 00:13:29,240
your own family, in your own town, in your own area, so again, thanks for tuning in

128
00:13:29,240 --> 00:13:43,240
and I'll have the best.

