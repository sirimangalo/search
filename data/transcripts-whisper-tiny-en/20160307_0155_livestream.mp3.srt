1
00:00:00,000 --> 00:00:19,000
Good evening, everyone, broadcasting live March 6th.

2
00:00:19,000 --> 00:00:42,000
Today's quote is about the five hundred senses.

3
00:00:42,000 --> 00:00:54,000
These are our enemosite.

4
00:00:54,000 --> 00:01:05,000
These are the bad guys.

5
00:01:05,000 --> 00:01:27,000
We set out on this journey to find clarity of mind, to find enlightenment, to find the truth.

6
00:01:27,000 --> 00:01:41,000
When we work so hard to see the truth, to discover the truth, to investing the truth.

7
00:01:41,000 --> 00:01:48,000
Sometimes we don't see the truth.

8
00:01:48,000 --> 00:01:53,000
Sometimes we can't see the truth.

9
00:01:53,000 --> 00:01:57,000
There are reasons why we can't see the truth.

10
00:01:57,000 --> 00:02:02,000
We can't find the truth.

11
00:02:02,000 --> 00:02:07,000
That's what this quote is all about.

12
00:02:07,000 --> 00:02:14,000
Think of the mind as a bowl of water.

13
00:02:14,000 --> 00:02:18,000
You think of those tide pools by the ocean.

14
00:02:18,000 --> 00:02:27,000
If you look into the tide pool, when I was 13, we drove along the California coast.

15
00:02:27,000 --> 00:02:40,000
I remember looking at these tide pools full of hermit crabs and starfish and all sorts of interesting things.

16
00:02:40,000 --> 00:02:44,000
But the thing is the water has to be clear.

17
00:02:44,000 --> 00:02:59,000
If the water is all stirred up, the water is polluted.

18
00:02:59,000 --> 00:03:01,000
Or any number of things.

19
00:03:01,000 --> 00:03:12,000
These five conditions of the mind that are like the five conditions of the bowl of water.

20
00:03:12,000 --> 00:03:26,000
If a bowl of water is mixed up with black, turmeric, blue or yellow dye,

21
00:03:26,000 --> 00:03:32,000
you wouldn't be able to see into the pool here and talk about actually reflection.

22
00:03:32,000 --> 00:03:39,000
If you look into that pool of water and try and see your reflection in the pool of water,

23
00:03:39,000 --> 00:03:46,000
it's all full of turmeric, dye or something.

24
00:03:46,000 --> 00:04:04,000
And you wouldn't see a reflection in the way of it truly was.

25
00:04:04,000 --> 00:04:10,000
In the same way of the mind is obsessed by central desires.

26
00:04:10,000 --> 00:04:23,000
It's for pleasant sight and sounds and smells and tastes and feeling.

27
00:04:23,000 --> 00:04:25,000
Again, you can't all right from wrong.

28
00:04:25,000 --> 00:04:31,000
You can't see the truth by yourself and about the world.

29
00:04:31,000 --> 00:04:34,000
You can't see the truth about things.

30
00:04:34,000 --> 00:04:40,000
You can get deluded by thing, confused by thing.

31
00:04:40,000 --> 00:04:42,000
It will mix up.

32
00:04:42,000 --> 00:04:46,000
You can't see clearly the water is not clear.

33
00:04:46,000 --> 00:04:56,000
The water of the mind is not clear.

34
00:04:56,000 --> 00:05:00,000
Or what if the water is heated up to boiling and bubbling?

35
00:05:00,000 --> 00:05:07,000
If then you're to look at this water and think maybe I'll see what my face looks like.

36
00:05:07,000 --> 00:05:21,000
Well, if the, if the, if the pot of water and boiling and bubbling,

37
00:05:21,000 --> 00:05:28,000
you won't be able to see much of anything, not along your face.

38
00:05:28,000 --> 00:05:31,000
You wouldn't like in this anger.

39
00:05:31,000 --> 00:05:37,000
If your mind is full of anger and you know, well,

40
00:05:37,000 --> 00:05:41,000
how could you possibly understand the truth?

41
00:05:41,000 --> 00:05:46,000
How could you see clearly your mind is, as a flame,

42
00:05:46,000 --> 00:05:51,000
is boiling over its range and anger?

43
00:05:51,000 --> 00:05:54,000
You can't see the truth.

44
00:05:54,000 --> 00:06:01,000
You can't tell truth from a falsehood right from wrong good from bad.

45
00:06:01,000 --> 00:06:19,000
All you know is, would you like in this life?

46
00:06:19,000 --> 00:06:26,000
I suppose the water was overgrown with plants, water plants.

47
00:06:26,000 --> 00:06:31,000
It would come to this pool of water and thinking, oh, look and see how my,

48
00:06:31,000 --> 00:06:34,000
see what my reflection looks like.

49
00:06:34,000 --> 00:06:37,000
We'll come upon it and oh, you can't see anything.

50
00:06:37,000 --> 00:06:39,000
It's overgrown with water.

51
00:06:39,000 --> 00:06:45,000
Let's go overgrown with plants.

52
00:06:45,000 --> 00:06:50,000
I'm going to tell this is like when the mind is,

53
00:06:50,000 --> 00:06:52,000
the mind has caught up in laziness,

54
00:06:52,000 --> 00:06:55,000
sloth, torpor,

55
00:06:55,000 --> 00:07:01,000
drowsiness.

56
00:07:01,000 --> 00:07:05,000
And you can't see anything in the mind.

57
00:07:05,000 --> 00:07:09,000
So the mind doesn't have the effort to go out to the object,

58
00:07:09,000 --> 00:07:15,000
to be mindful of the objects as they arise.

59
00:07:15,000 --> 00:07:18,000
You see the objects as they are in it.

60
00:07:18,000 --> 00:07:22,000
Note the energy that it takes to know the object.

61
00:07:22,000 --> 00:07:25,000
Stop there.

62
00:07:25,000 --> 00:07:27,000
Don't get lazy.

63
00:07:27,000 --> 00:07:34,000
You won't see anything there.

64
00:07:34,000 --> 00:07:39,000
I suppose the water was stirred up by the wind and there were waves

65
00:07:39,000 --> 00:07:44,000
and ripples caused by the wind.

66
00:07:44,000 --> 00:07:47,000
Same way if anyone looks in that pool.

67
00:07:47,000 --> 00:07:53,000
There's no question and I'm going to see that reflection as it is.

68
00:07:53,000 --> 00:08:00,000
And we would like in this to the mind as full of restlessness and worry.

69
00:08:00,000 --> 00:08:04,000
If you're restless and you're not focused on one thing,

70
00:08:04,000 --> 00:08:10,000
mind is running hither and thither.

71
00:08:10,000 --> 00:08:14,000
Caught up.

72
00:08:14,000 --> 00:08:19,000
Caught up with thoughts of the past and the future.

73
00:08:19,000 --> 00:08:33,000
Mind isn't settled enough.

74
00:08:33,000 --> 00:08:35,000
The mind isn't settled.

75
00:08:35,000 --> 00:08:44,000
You can't see anything sitting about like a butterfly.

76
00:08:44,000 --> 00:08:49,000
You can't focus on anything, you can't understand anything clearly.

77
00:08:49,000 --> 00:08:53,000
So you do have to focus on everything.

78
00:08:53,000 --> 00:08:58,000
You have to be conscientious about everything that arises.

79
00:08:58,000 --> 00:09:02,000
Treat everything that arises like a guest.

80
00:09:02,000 --> 00:09:08,000
Don't dismiss and don't overlook the guests.

81
00:09:08,000 --> 00:09:14,000
You have to see whatever guest, just like a good host has to see what the guest needs.

82
00:09:14,000 --> 00:09:18,000
You have to stop and greet the guest.

83
00:09:18,000 --> 00:09:24,000
It would be a bad person or a good person, but you all know in this greet.

84
00:09:24,000 --> 00:09:29,000
You have to take the time to check them out.

85
00:09:29,000 --> 00:09:37,000
So you have to be conscientious with every experience, not being superficial about them.

86
00:09:37,000 --> 00:09:46,000
And finally, suppose there's a bowl of water that is muddy,

87
00:09:46,000 --> 00:09:52,000
so it's stirred up on the bottom, so it's on muddy and set in the dark.

88
00:09:52,000 --> 00:09:57,000
Suppose it's dark and you look in a mud puddle.

89
00:09:57,000 --> 00:10:10,000
This is the bottom, lichens dilution, ignorance, doubt, sorry, doubt.

