1
00:00:00,000 --> 00:00:10,000
Welcome back to our study in the Dhamapada. Today we continue on with verse 166, which reads as follows.

2
00:00:30,000 --> 00:00:58,000
What is good? What is important or useful? What is good for oneself? One should never abandon, or neglect, but it's good for oneself in favor of what is good for someone else, even though it might be much.

3
00:01:00,000 --> 00:01:26,000
even though it may be more. Fully knowing what is good for oneself, one should be intent upon one's own good. Selfish, one should be selfish, basically it.

4
00:01:26,000 --> 00:01:38,000
Well, there's context to this first of all, but there is, they've talked about this before, and they're interesting sort of philosophical implications of this.

5
00:01:38,000 --> 00:01:53,000
It's surrounding this to understand why it might actually be the right teaching. Of course, we tend to think it is because it's promoted as being something the Buddha taught, and we have faith in him.

6
00:01:53,000 --> 00:02:04,000
So, let's explore this together and see how it's useful for our meditation, of course.

7
00:02:04,000 --> 00:02:23,000
So, the story goes, and this is a great story. It's a short story as usual, but it's a memorable one, one that I used recently and used before to make a point.

8
00:02:23,000 --> 00:02:43,000
So, the Buddha spent 45 years of his life teaching other people. You'll remember from Sutta study on Saturday that he didn't want to teach. He wasn't inclined to teach, but he was invited to teach, and so he did.

9
00:02:43,000 --> 00:03:02,000
He had spent 45 years tirelessly teaching, and that's really the interesting thing about it. He was quite self-absorbed, probably the wrong word for it, but the best word we have, the best sort of word in English.

10
00:03:02,000 --> 00:03:20,000
He was very much fixed and focused on happiness, really, and on the piece that he had attained. That any interest in teaching, it just didn't, and no reason occurred to him to teach.

11
00:03:20,000 --> 00:03:44,000
So, that sort of life that he had set out for him would have been quite peaceful, and carefree, carefree, of course, but without any requirements put on him, would have been a life of living in peace and meditation.

12
00:03:44,000 --> 00:04:06,000
So, clearly, that was fine for him. That was easy to understand how that would be considered to be perfectly natural to incline in that direction, but then, as soon as he was invited to teach, he incline the different direction towards a life that was a complete opposite.

13
00:04:06,000 --> 00:04:21,000
And I think that says something important about the idea of not caring. The Buddha was such that it would have been fine either way, and the 45 years he did spend work.

14
00:04:21,000 --> 00:04:44,000
In many ways, externally chaotic and stressful. I mean, there must have been physically stressful on the Buddha, and there must have been a lot more pain and discomfort physically, tiring and tiring. It must have been.

15
00:04:44,000 --> 00:04:53,000
And that was just as fine with him. There was no preference in that sense.

16
00:04:53,000 --> 00:05:08,000
But at the end of the 45 years, he was ready to leave, ready to be done, and he made a determination that this was it, that he wasn't going to try and live out his life to 100 years.

17
00:05:08,000 --> 00:05:29,000
About 80 or 80 years enough. And so, he looked at his students and his monks, female monks, male monks, female they disciples, male they disciples, they had all become quite established.

18
00:05:29,000 --> 00:05:45,000
And the song was, by that time, quite prominent in India, and well established such that it would, he knew it would carry his teachings far into the future.

19
00:05:45,000 --> 00:05:51,000
So, that life done. I've done what was asked. So, that's that.

20
00:05:51,000 --> 00:06:01,000
And he declared, he made known to his students in four months. He would be passing away into Parnibana. That would be it. They'd never see him again.

21
00:06:01,000 --> 00:06:12,000
No one would ever see him again. He would never have any further contact with existence of any sort.

22
00:06:12,000 --> 00:06:38,000
Which was disturbing, to say the least, to many of his followers. Those monks who were not yet sold up on that were not yet seen this detour for themselves were quite disturbed and saddened by, and perturbed in the sense that it made them want to spend all their time with a Buddha.

23
00:06:38,000 --> 00:06:52,000
Rather than doing anything else, they thought, well, for four months, if that's going to be it, then we'd better just stay with him all the time so we can learn as much as we can.

24
00:06:52,000 --> 00:07:02,000
And so, they did this. This was their thinking, well, if the Buddha is going to pass away in four months, better spend all our time around him.

25
00:07:02,000 --> 00:07:15,000
This is it. Seems reasonable, I think. No. But one monk, one monk in the monks name was Atadata.

26
00:07:15,000 --> 00:07:31,000
Atadata means, Atadata, the D is just as superfluous. It's not meaningful. Atadata means self and Atadata means benefit or meaning or purpose.

27
00:07:31,000 --> 00:07:40,000
In this case, purpose or no benefit, I suppose. Interest, one's own interest. Atadata is self.

28
00:07:40,000 --> 00:07:50,000
One who has one's own interest. One who looks after one's own interest. One who is selfish, self-centered, self-focused.

29
00:07:50,000 --> 00:08:00,000
Focused on what is good for one's self. He thought to himself, the Buddha is going to pass away in four months.

30
00:08:00,000 --> 00:08:05,000
I should spend all my time meditating away from Buddha.

31
00:08:05,000 --> 00:08:14,000
He said it quite better than that. He said, Ahunchamhi, Auitarago.

32
00:08:14,000 --> 00:08:22,000
I indeed am not free from passion.

33
00:08:22,000 --> 00:08:35,000
He says, I will put out effort for becoming an Atan. Well, Buddha is still alive, but Buddha is still here.

34
00:08:35,000 --> 00:08:59,000
Atadata means, Auitarago, Auitarago, Auitarago, Auitarago, Auitarago.

35
00:08:59,000 --> 00:09:13,000
In the other monks who woke up early in the morning and immediately went to the Buddha's good day to wait for him to come out and stay with him and learn from him.

36
00:09:13,000 --> 00:09:19,000
They saw this and they said, Hey, why aren't you? What's wrong with you? You don't ever come and see the Buddha.

37
00:09:19,000 --> 00:09:23,000
They noticed that he never went to see the Buddha. It was like he was avoiding the Buddha.

38
00:09:23,000 --> 00:09:30,000
In fact, whereas before he would have maybe gone to pay respect or listened to the Buddha's teaching, he just didn't go to see the Buddha.

39
00:09:30,000 --> 00:09:33,000
So they thought, what's with this guy?

40
00:09:33,000 --> 00:09:40,000
It looks like they dragged him to the Buddha. They actually picked him up and took him to the Buddha.

41
00:09:40,000 --> 00:09:48,000
They said, Why are you bringing this monk? Why are you bringing him here?

42
00:09:48,000 --> 00:09:55,000
And they said, Oh, one day this monk is doing this. He's off.

43
00:09:55,000 --> 00:10:05,000
He's avoiding you. And the Buddha looked at him and said, Why are you behaving in this way?

44
00:10:05,000 --> 00:10:22,000
And so he told them, he said, But one day you have said that in four months, you're going to pass into Parnimanda. As long as you're still here, I will strive for the attainment of our hunch.

45
00:10:22,000 --> 00:10:35,000
And the Buddha looks at him and, let's see, they've heard that he actually says Sadhu. It's one of the few times.

46
00:10:35,000 --> 00:10:42,000
What does it say? Yeah, the Buddha, Sata, tasasadhu karang dattua.

47
00:10:42,000 --> 00:10:45,000
So I don't know that he brought his hands up, probably not.

48
00:10:45,000 --> 00:10:58,000
Sadhu karang dattua. He gave him praise, basically. He approved. He gave his approval.

49
00:10:58,000 --> 00:11:13,000
And then he said, Bhikkhu a yasama yisineho ati, for whoever has love for me, basically.

50
00:11:13,000 --> 00:11:21,000
Whoever has love for me, they should act in the same way as this monk who has his own benefit at heart.

51
00:11:21,000 --> 00:11:26,000
And that's how we got the name. That's how we got the name ati datta.

52
00:11:26,000 --> 00:11:37,000
One new looks after their own benefit and then he taught this verse. Ati datta, Bhikkhu tainan, so on.

53
00:11:37,000 --> 00:11:52,000
So there's a few lessons here. The first one, because the story isn't exactly in regards to choosing between one's own benefit and the benefit of others.

54
00:11:52,000 --> 00:12:01,000
But one's own interest in what's in the interest of other people.

55
00:12:01,000 --> 00:12:21,000
It's more about choosing what is truly useful and beneficial over what is actually just superficial.

56
00:12:21,000 --> 00:12:27,000
And this sort of teaching is quite important for cultural Buddhists.

57
00:12:27,000 --> 00:12:43,000
Buddhist societies can be quite wonderful in terms of supporting Buddhism, providing a sort of a nest or a protection with the culture.

58
00:12:43,000 --> 00:12:51,000
Because it becomes a part of society and this protection of Buddhism and this revering of Buddhism.

59
00:12:51,000 --> 00:13:04,000
But it's very easy and it's a sort of a slippery slope to eventually see greater importance in the cultural aspects.

60
00:13:04,000 --> 00:13:16,000
And the more what we might call religious aspects or the more important aspects to put a fine point on it.

61
00:13:16,000 --> 00:13:29,000
And so we can see that sort of thing happening here where the monks had become quite comfortable with what had become what we might call Buddhism and the Buddha.

62
00:13:29,000 --> 00:13:41,000
And so they revered him and they revered him so much that they forgot that what was important.

63
00:13:41,000 --> 00:13:50,000
And so this sort of teaching this story along with this verse is very important as Buddhism grows.

64
00:13:50,000 --> 00:13:56,000
And as we carry Buddhism, it's important that we carry teachings like this with us.

65
00:13:56,000 --> 00:14:06,000
That remind us what aspects of life are important and what you're superfluous.

66
00:14:06,000 --> 00:14:14,000
I think it shows sort of a higher sense of purpose.

67
00:14:14,000 --> 00:14:21,000
And that we sometimes get quite carried away with appearance.

68
00:14:21,000 --> 00:14:26,000
What is expected of us?

69
00:14:26,000 --> 00:14:36,000
You know how we want to look and how we want other people to see us and how we feel obligated to others.

70
00:14:36,000 --> 00:14:41,000
How we feel like we fit into our roles in society.

71
00:14:41,000 --> 00:14:47,000
I mean, it speaks to a greater sort of going against the stream.

72
00:14:47,000 --> 00:14:52,000
Not doing things just because that's what you think is expected of you.

73
00:14:52,000 --> 00:15:02,000
And being bold enough to really say no, this isn't actually useful, this isn't actually beneficial.

74
00:15:02,000 --> 00:15:14,000
Because it seems insensitive of this monk to stop caring about the Buddha and stop hanging around with the Buddha.

75
00:15:14,000 --> 00:15:22,000
On the one hand, it's not the only thing here. There's another sense that these monks thought that, hey, this is useful.

76
00:15:22,000 --> 00:15:25,000
It's good for us to learn all we can from the Buddha.

77
00:15:25,000 --> 00:15:30,000
When in fact, the Buddha just kept telling them, go meditate, go meditate.

78
00:15:30,000 --> 00:15:35,000
You know, like people who watch all my videos, but never actually meditate.

79
00:15:35,000 --> 00:15:40,000
I've got 1,500 videos on YouTube.

80
00:15:40,000 --> 00:15:49,000
If you've watched them all, you've watched too many people watch, half of them you've watched too many.

81
00:15:49,000 --> 00:16:01,000
I mean, I appreciate that people are interested in the teachings, but this is a good example for us reminding us what is really useful.

82
00:16:01,000 --> 00:16:07,000
And this monk certainly found it.

83
00:16:07,000 --> 00:16:13,000
I mean, listening to teachings is easier, makes you feel good.

84
00:16:13,000 --> 00:16:23,000
Meditation, you see these people here come to do meditation courses very difficult.

85
00:16:23,000 --> 00:16:27,000
But it's a difference between the conceptual and the real.

86
00:16:27,000 --> 00:16:31,000
Meditation deals with the real, it deals with reality.

87
00:16:31,000 --> 00:16:38,000
It has a power that study and listening, learning doesn't.

88
00:16:38,000 --> 00:16:49,000
It has the power to reach into the very mechanics of experience and change them and refine them.

89
00:16:49,000 --> 00:16:57,000
We'll find them simply by seeing them more clearly, not making mistakes based on ignorance.

90
00:16:57,000 --> 00:17:05,000
Seeing the mistakes that we make based on ignorance based on our imperfect understanding of experience.

91
00:17:05,000 --> 00:17:07,000
So that's the first part.

92
00:17:07,000 --> 00:17:09,000
This difference between what is useful and not useful.

93
00:17:09,000 --> 00:17:14,000
The second part, of course, is this idea beneficial to others, beneficial to self.

94
00:17:14,000 --> 00:17:21,000
And I guess that sort of relates to the idea of what's expected of you.

95
00:17:21,000 --> 00:17:27,000
More than expected is kind of a simplification that you're disrespecting the Buddha.

96
00:17:27,000 --> 00:17:30,000
You're not fulfilling your obligation.

97
00:17:30,000 --> 00:17:33,000
And in fact, there may have been a sense of obligation.

98
00:17:33,000 --> 00:17:36,000
Every day everybody has to go and the Buddha is going to give a talk.

99
00:17:36,000 --> 00:17:37,000
You have to go listen.

100
00:17:37,000 --> 00:17:38,000
The Buddha comes out.

101
00:17:38,000 --> 00:17:41,000
You have to go and pay respect to him.

102
00:17:41,000 --> 00:17:44,000
You don't go and see the Buddha is considered to me.

103
00:17:44,000 --> 00:17:45,000
Not so good.

104
00:17:45,000 --> 00:17:47,000
And the Buddha didn't think so.

105
00:17:47,000 --> 00:17:49,000
The Buddha didn't agree with this.

106
00:17:49,000 --> 00:17:52,000
He discounted the concerns of these monks and said,

107
00:17:52,000 --> 00:17:59,000
look, if you love me, stop following this carcass around.

108
00:17:59,000 --> 00:18:01,000
You know, this one monk who followed the Buddha,

109
00:18:01,000 --> 00:18:04,000
he said, what are you doing following this carcass around?

110
00:18:04,000 --> 00:18:10,000
They stopped following the carcass,

111
00:18:10,000 --> 00:18:15,000
stopped following this rotten putrid thing with nine holes,

112
00:18:15,000 --> 00:18:21,000
exuding all sorts of filth through nine holes,

113
00:18:21,000 --> 00:18:24,000
and the skin as well.

114
00:18:24,000 --> 00:18:30,000
Stop following this bag of garbage around.

115
00:18:30,000 --> 00:18:32,000
Stop doing what's not useful.

116
00:18:32,000 --> 00:18:42,000
Stop buying into the expectations and these roles and these

117
00:18:42,000 --> 00:18:46,000
artifices that we set up and ask yourself,

118
00:18:46,000 --> 00:18:48,000
what's important?

119
00:18:48,000 --> 00:18:51,000
You know, I'd like to meditate, but I can't.

120
00:18:51,000 --> 00:18:53,000
This reason, that reason,

121
00:18:53,000 --> 00:18:58,000
we're caught up in so much.

122
00:18:58,000 --> 00:19:00,000
We're going to get caught up in so much,

123
00:19:00,000 --> 00:19:02,000
and we think, oh, I'll just put meditation aside

124
00:19:02,000 --> 00:19:07,000
because I have all these other commitments and so on.

125
00:19:07,000 --> 00:19:10,000
I mean, it's not, we're not denouncing such things,

126
00:19:10,000 --> 00:19:13,000
but we have to understand that if you don't do it,

127
00:19:13,000 --> 00:19:18,000
it won't get done.

128
00:19:18,000 --> 00:19:23,000
And you don't get substitution points

129
00:19:23,000 --> 00:19:31,000
for being a good Buddhist and so on.

130
00:19:31,000 --> 00:19:34,000
But more interesting,

131
00:19:34,000 --> 00:19:36,000
especially in regards to those verses,

132
00:19:36,000 --> 00:19:41,000
the question, well, is it really better

133
00:19:41,000 --> 00:19:45,000
or, you know, always better to look out for your own interest

134
00:19:45,000 --> 00:19:49,000
instead of supposed teaching someone else meditation?

135
00:19:49,000 --> 00:19:52,000
At other times, the Buddha has made clear

136
00:19:52,000 --> 00:19:54,000
that helping others is helping yourself.

137
00:19:54,000 --> 00:19:56,000
When you help yourself, you're helping others,

138
00:19:56,000 --> 00:19:59,000
when you help others, you're helping yourself.

139
00:19:59,000 --> 00:20:03,000
And now that works, it's a kindness.

140
00:20:03,000 --> 00:20:06,000
It's being nice to someone.

141
00:20:06,000 --> 00:20:08,000
It's a cultivation of goodness to help others.

142
00:20:08,000 --> 00:20:11,000
So that's a support for your own practice.

143
00:20:14,000 --> 00:20:17,000
And so there's not as much of a,

144
00:20:22,000 --> 00:20:26,000
there's not as much of a dichotomy here or divergence here.

145
00:20:26,000 --> 00:20:29,000
It's not as much of an either or situation.

146
00:20:29,000 --> 00:20:33,000
We talk about being selfish and self-centered.

147
00:20:33,000 --> 00:20:38,000
But to be clear, it means doing what's in your own best interest,

148
00:20:38,000 --> 00:20:41,000
which only sounds problematic

149
00:20:41,000 --> 00:20:46,000
until you realize that helping others is helping yourself.

150
00:20:46,000 --> 00:20:49,000
And really the point is your focus.

151
00:20:49,000 --> 00:20:50,000
What is your focus?

152
00:20:50,000 --> 00:20:56,000
Is your focus to cure the sufferings of all the living beings on earth?

153
00:20:56,000 --> 00:21:01,000
Well, it's probably a problematic goal

154
00:21:01,000 --> 00:21:07,000
considering the numbers and the types of beings out there.

155
00:21:07,000 --> 00:21:09,000
It's a losing cause, a losing battle.

156
00:21:09,000 --> 00:21:13,000
But if your focus and your intention is to become enlightened,

157
00:21:13,000 --> 00:21:19,000
it really doesn't matter whether you help others or help yourself.

158
00:21:19,000 --> 00:21:24,000
If your focus and your intent in doing all these things,

159
00:21:24,000 --> 00:21:28,000
or whatever you do, and all the more things, the better.

160
00:21:28,000 --> 00:21:33,000
The greater your focus on helping yourself,

161
00:21:33,000 --> 00:21:36,000
it all becomes the practice.

162
00:21:36,000 --> 00:21:38,000
Eating becomes meditation, right?

163
00:21:38,000 --> 00:21:41,000
So teaching becomes, if not meditation,

164
00:21:41,000 --> 00:21:55,000
it becomes a support for your meditation as you help others.

165
00:21:55,000 --> 00:21:59,000
And so the final thing that I would say about this is,

166
00:21:59,000 --> 00:22:03,000
in regards to this paying respect to the Buddha,

167
00:22:03,000 --> 00:22:06,000
because Buddha also says here that someone who loves me,

168
00:22:06,000 --> 00:22:09,000
if they wish to pay respect to me,

169
00:22:09,000 --> 00:22:16,000
it's not through garlands and flowers and incense.

170
00:22:16,000 --> 00:22:26,000
They shouldn't worship me through their practice of the number.

171
00:22:26,000 --> 00:22:29,000
And this teaching, of course, is elsewhere.

172
00:22:29,000 --> 00:22:32,000
It's in the Mahapārṣṇyābāṇa sūtets.

173
00:22:32,000 --> 00:22:35,000
One of the last things the Buddha says is,

174
00:22:35,000 --> 00:22:42,000
you know, flowers, candles, incense, not really the way.

175
00:22:42,000 --> 00:22:45,000
And I thought I had more to say about that,

176
00:22:45,000 --> 00:22:56,000
but that's really the main point.

177
00:22:56,000 --> 00:23:07,000
Right, is that the practice which the Buddha recommends,

178
00:23:07,000 --> 00:23:11,000
it's not all these many things that we talk about in Buddhism,

179
00:23:11,000 --> 00:23:20,000
that giving charity or things like communal harmony

180
00:23:20,000 --> 00:23:25,000
and being good to other people and so on.

181
00:23:25,000 --> 00:23:31,000
The recommendation here is,

182
00:23:31,000 --> 00:23:35,000
what I want to say is, this is why we come to a meditation center

183
00:23:35,000 --> 00:23:37,000
and do a meditation retreat.

184
00:23:37,000 --> 00:23:40,000
This is explicitly the sort of thing that the Buddha recommended,

185
00:23:40,000 --> 00:23:45,000
not to just be a good Buddhist or even learn to meditate.

186
00:23:45,000 --> 00:23:50,000
You know, really ideally, all of the people that we teach,

187
00:23:50,000 --> 00:23:55,000
I teach on the internet and get interested

188
00:23:55,000 --> 00:23:58,000
and start to pick up this type of meditation ideally.

189
00:23:58,000 --> 00:24:01,000
They find a way someday, somehow,

190
00:24:01,000 --> 00:24:04,000
to do what Atadata did.

191
00:24:04,000 --> 00:24:08,000
This monk who got the name Atadata really understood

192
00:24:08,000 --> 00:24:12,000
what is to his benefit, what was the greatest thing to do

193
00:24:12,000 --> 00:24:15,000
and went off and practiced on their own.

194
00:24:15,000 --> 00:24:20,000
I mean, I don't know if it's clear, but the idea is

195
00:24:20,000 --> 00:24:25,000
that we really can only benefit ourselves

196
00:24:25,000 --> 00:24:27,000
when you talk about benefiting others.

197
00:24:27,000 --> 00:24:29,000
What's the best you can do for them?

198
00:24:29,000 --> 00:24:31,000
You can talk about healing their wounds

199
00:24:31,000 --> 00:24:35,000
or giving them food to eat or money or shelter all those things

200
00:24:35,000 --> 00:24:38,000
and that's useful, beneficial.

201
00:24:38,000 --> 00:24:42,000
But it's not nearly as beneficial as

202
00:24:42,000 --> 00:24:48,000
them going off and practicing meditation to become enlightened.

203
00:24:48,000 --> 00:24:52,000
Obviously, there's a hierarchy of needs that need to be taken care of,

204
00:24:52,000 --> 00:24:56,000
but you talk about what has the greatest benefit

205
00:24:56,000 --> 00:25:00,000
and the greatest result? It's helping oneself.

206
00:25:00,000 --> 00:25:03,000
And so whether you help yourself or whether you help other people

207
00:25:03,000 --> 00:25:06,000
to help yourself, the focus always has to be on this,

208
00:25:06,000 --> 00:25:09,000
the helping of oneself.

209
00:25:09,000 --> 00:25:13,000
And if that's your focus, perhaps,

210
00:25:13,000 --> 00:25:18,000
that's the way to become free from suffering.

211
00:25:18,000 --> 00:25:24,000
That's the best way to be.

212
00:25:24,000 --> 00:25:27,000
And if you take it to its logical conclusion,

213
00:25:27,000 --> 00:25:30,000
it means going off on your own

214
00:25:30,000 --> 00:25:35,000
and cultivating proper and intensive meditation practice.

215
00:25:35,000 --> 00:25:40,000
So this verse relates to our practice,

216
00:25:40,000 --> 00:25:42,000
for those of you who are here doing a course,

217
00:25:42,000 --> 00:25:45,000
to be encouraged that you are at that,

218
00:25:45,000 --> 00:25:49,000
that you are people who know your own benefit.

219
00:25:49,000 --> 00:25:51,000
You've done something that's very difficult

220
00:25:51,000 --> 00:25:54,000
and people watching here on the internet often

221
00:25:54,000 --> 00:25:56,000
maybe wish they could do.

222
00:25:56,000 --> 00:26:01,000
Maybe don't even feel up to doing.

223
00:26:01,000 --> 00:26:07,000
But you found the path to truly help yourselves

224
00:26:07,000 --> 00:26:11,000
and to dive right into what is to your own benefit.

225
00:26:11,000 --> 00:26:13,000
It's to be commended.

226
00:26:13,000 --> 00:26:16,000
It's reason I have great respect for anyone who does it,

227
00:26:16,000 --> 00:26:18,000
having done it myself.

228
00:26:18,000 --> 00:26:22,000
It's a great and profound thing.

229
00:26:22,000 --> 00:26:26,000
So at least this gives you a good encouragement

230
00:26:26,000 --> 00:26:29,000
to those of us who are here

231
00:26:29,000 --> 00:26:32,000
and encouragement to those who haven't come yet.

232
00:26:32,000 --> 00:26:37,000
This is the kind of thing that makes

233
00:26:37,000 --> 00:26:40,000
one a true practitioner in the Buddhist teaching.

234
00:26:40,000 --> 00:26:43,000
So that's the Dhammapada for tonight.

235
00:26:43,000 --> 00:27:07,000
Thank you all for tuning in.

