1
00:00:00,000 --> 00:00:12,480
Okay, so here we have a meditation setup, we're going to practice meditation here.

2
00:00:12,480 --> 00:00:20,680
This computer is going to be broadcasting our meditation, the webcam around the world.

3
00:00:20,680 --> 00:00:24,320
So we're just preparing now.

4
00:00:24,320 --> 00:00:28,000
I'll give the talk first and then we'll do meditation.

5
00:00:28,000 --> 00:00:54,000
So I'll just show you how the, let's just see how the internet works.

6
00:00:54,000 --> 00:01:23,320
So here we're broadcasting live over the internet and I'll be giving a talk and a talk

7
00:01:23,320 --> 00:01:26,320
will be heard over the internet.

8
00:01:26,320 --> 00:01:34,520
So this is part of our daily routine at 7pm, we'll have this, we have this group meeting.

9
00:01:34,520 --> 00:01:38,920
Just to just see the format.

10
00:01:38,920 --> 00:01:45,840
We'll do, today we're going to do a brief one because we've got work to do for the

11
00:01:45,840 --> 00:01:47,800
ceremony tomorrow.

12
00:01:47,800 --> 00:01:54,800
So we're going to do say 15 minutes walking, 15 minutes sitting, hopefully we'll see.

13
00:01:54,800 --> 00:01:58,600
I'll just give a short talk in English and then we'll do a short meditation.

14
00:01:58,600 --> 00:02:04,760
Normally we do up to an hour and a half meditation, half an hour talking, an hour and

15
00:02:04,760 --> 00:02:09,640
a half meditation, but we're going to cut it short today.

16
00:02:09,640 --> 00:02:37,080
Yeah.

17
00:03:37,080 --> 00:03:49,560
Thank you very much.

18
00:03:49,560 --> 00:04:19,160
Okay, so welcome everyone, we're going to start now, this is our daily meeting.

19
00:04:19,160 --> 00:04:42,160
And so the way we do this is normally I'll give a talk for 20, 30 minutes and then we'll do a meditation.

20
00:04:42,160 --> 00:04:48,160
And normally we do a meditation for an hour and a half, but we're just going to do a half an hour meditation today.

21
00:04:48,160 --> 00:05:08,160
So I'll try to, I'll try to talk for until 730 and then we'll practice until 8.

22
00:05:08,160 --> 00:05:13,160
Everybody close your eyes.

23
00:05:13,160 --> 00:05:19,160
I'll try to listen to what I say and try your best to understand it.

24
00:05:19,160 --> 00:05:24,160
Meditation is for watching yourself or seeing yourself.

25
00:05:24,160 --> 00:05:29,160
We're seeing who you are, so we don't have to watch anybody else.

26
00:05:29,160 --> 00:05:31,160
We don't have to look at anybody else.

27
00:05:31,160 --> 00:05:35,160
We have to look at our own mind.

28
00:05:35,160 --> 00:05:42,160
So everybody take a deep breath and try to settle down.

29
00:05:42,160 --> 00:05:45,160
Bring your mind back in.

30
00:05:45,160 --> 00:05:47,160
Let go of all the things that you did today.

31
00:05:47,160 --> 00:05:50,160
Don't think about what you did today.

32
00:05:50,160 --> 00:05:58,160
Don't think about what you're going to do tomorrow.

33
00:05:58,160 --> 00:06:05,160
The interesting thing that we come to realize in meditation is the more you know yourself,

34
00:06:05,160 --> 00:06:08,160
the more you know everybody else.

35
00:06:08,160 --> 00:06:13,160
You know yourself, the less you understand other people.

36
00:06:13,160 --> 00:06:22,160
This is especially true because our ability to understand others depends on our own state of mind.

37
00:06:22,160 --> 00:06:29,160
Mostly we look at things through, like they say, through rose-colored glasses.

38
00:06:29,160 --> 00:06:35,160
We look at things through filters, through filters of our own state of mind.

39
00:06:35,160 --> 00:06:40,160
So when our mind is not clear, we can't see things clearly.

40
00:06:40,160 --> 00:06:43,160
We can't understand other people.

41
00:06:43,160 --> 00:06:47,160
So the first thing is to understand ourselves.

42
00:06:47,160 --> 00:06:49,160
Where do we start to understand ourselves?

43
00:06:49,160 --> 00:06:54,160
We start by looking at what is clearly apparent.

44
00:06:54,160 --> 00:06:58,160
The most clear thing is going to be our stomach.

45
00:06:58,160 --> 00:07:03,160
So we're going to watch our stomach when we start meditating.

46
00:07:03,160 --> 00:07:07,160
The stomach rises, we say to ourselves rising.

47
00:07:07,160 --> 00:07:12,160
The stomach falls falling.

48
00:07:12,160 --> 00:07:16,160
I say falling.

49
00:07:16,160 --> 00:07:17,160
I say falling.

50
00:07:17,160 --> 00:07:22,160
Even when you're listening to a talk on meditation, you should still be meditating.

51
00:07:22,160 --> 00:07:29,160
It's the only reason for giving the talk to them on meditation is to help us to meditate better.

52
00:07:29,160 --> 00:07:33,160
And meditation is something which we can practice anytime.

53
00:07:33,160 --> 00:07:43,160
So we should, of course, even practice meditation when we're listening to the dhamma, when we're listening to a talk.

54
00:07:43,160 --> 00:07:51,160
Right now we look around the world and the world is full of all sorts of suffering, all sorts of stress, all sorts of difficulty.

55
00:07:51,160 --> 00:07:58,160
People all over the world are suffering.

56
00:07:58,160 --> 00:08:04,160
And a lot of our suffering, some of our suffering is made by nature.

57
00:08:04,160 --> 00:08:13,160
It comes with sicknesses and natural disasters and so on, famine.

58
00:08:13,160 --> 00:08:23,160
Many of them are probably, it's not possible to directly attribute to human sources.

59
00:08:23,160 --> 00:08:32,160
But we can see that a lot of our sufferings in the world are caused by other human beings, are caused by ourselves, caused by each other.

60
00:08:32,160 --> 00:08:45,160
They're caused by not understanding each other and by seeing other people as the enemy or as foreign or as something different.

61
00:08:45,160 --> 00:09:03,160
This is something that I get every day because I have to be faced with much attention, unwanted or unfriendly even attention.

62
00:09:03,160 --> 00:09:06,160
From people who see me as something different.

63
00:09:06,160 --> 00:09:19,160
Because, of course, in this society, a Buddhist monk is not something common, not something familiar, especially one who follows the practice of going on alms around and so on.

64
00:09:19,160 --> 00:09:27,160
So it's something that hate me that really a lot of our suffering in this world just comes from not understanding each other.

65
00:09:27,160 --> 00:09:36,160
We have a planet here with human, only human beings, just the human beings, it's over five million, five billion.

66
00:09:36,160 --> 00:09:48,160
I don't know how many billion now.

67
00:09:48,160 --> 00:09:58,160
And so the incredible diversity of life on this planet, even just among human beings, homo sapiens, one species.

