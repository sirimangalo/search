1
00:00:00,000 --> 00:00:05,000
Do you make a distinction between desire and need?

2
00:00:05,000 --> 00:00:10,000
Is me desiring money to provide food and shelter to my family wrong?

3
00:00:10,000 --> 00:00:18,000
How can I go about in this best of world without desiring something I need to survive?

4
00:00:22,000 --> 00:00:25,000
Who wants to get this one up?

5
00:00:25,000 --> 00:00:37,000
No, I don't think it's wrong to be providing for your family.

6
00:00:37,000 --> 00:00:41,000
It's very good to be providing for your family.

7
00:00:41,000 --> 00:00:45,000
It's very wrong if you're not providing for your family.

8
00:00:45,000 --> 00:00:49,000
I don't know whether the word desire is correct.

9
00:00:49,000 --> 00:01:04,000
Maybe a must to go away and work and provide for your family to feed your children, to provide a shelter.

10
00:01:04,000 --> 00:01:14,000
If there's an excessive want in there, we want a bigger house and we want a bigger car and all the other things in this.

11
00:01:14,000 --> 00:01:21,000
There's a bit of a problem there, but it's not wrong to provide for your family.

12
00:01:21,000 --> 00:01:29,000
If we had gotten to that yesterday, maybe we would answer this question.

13
00:01:29,000 --> 00:01:32,000
It's actually one of the six directions.

14
00:01:32,000 --> 00:01:37,000
We're talking about the six directions and reviewing, venerating the six directions.

15
00:01:37,000 --> 00:01:45,000
One of them is your family and you need to provide for them.

16
00:01:45,000 --> 00:01:48,000
Remember that not everything is done with desire.

17
00:01:48,000 --> 00:01:56,000
Arahant doesn't just sit there in a pool of their own urine and feces and veg out until they die.

18
00:01:56,000 --> 00:02:06,000
They might sit until they pass away, but Arahant would actually pass away because they're...

19
00:02:06,000 --> 00:02:08,000
It's interesting.

20
00:02:08,000 --> 00:02:16,000
In that case, you might have an argument that it is wrong to want to provide for your family.

21
00:02:16,000 --> 00:02:21,000
That an Arahant might not even do it.

22
00:02:21,000 --> 00:02:34,000
So wrong in the sense of still involved with thinking that an Arahant would be unable to go back to work to provide for their family.

23
00:02:34,000 --> 00:02:36,000
They just wouldn't do it.

24
00:02:36,000 --> 00:02:42,000
They would say to their family, you know, look, here's the...

25
00:02:42,000 --> 00:02:44,000
The...

26
00:02:44,000 --> 00:02:45,000
The...

27
00:02:45,000 --> 00:02:46,000
The...

28
00:02:46,000 --> 00:02:47,000
The...

29
00:02:47,000 --> 00:02:48,000
The...

30
00:02:48,000 --> 00:02:49,000
The...

31
00:02:49,000 --> 00:02:50,000
The...

32
00:02:50,000 --> 00:02:51,000
The...

33
00:02:51,000 --> 00:02:52,000
I don't know.

34
00:02:52,000 --> 00:02:53,000
And it's just the theory.

35
00:02:53,000 --> 00:02:58,000
But the theory goes that an Arahant, if they're a lay person, they will either ordain or pass away.

36
00:02:58,000 --> 00:03:04,000
They can't possibly live in the state of the lay person because of the profundity of the state.

37
00:03:04,000 --> 00:03:11,000
They're totally free from Samsara or any desire to support their family, for example.

38
00:03:11,000 --> 00:03:17,000
I mean, there's no concept of people in the mind of the Arahant.

39
00:03:17,000 --> 00:03:21,000
They understand that people have this concept, but they have no concept.

40
00:03:21,000 --> 00:03:30,000
They have no idea of the necessity to provide food for your family.

41
00:03:30,000 --> 00:03:37,000
The death of the other people is not really intrinsically.

42
00:03:37,000 --> 00:03:42,000
I mean, I mean, what I mean to say is it's not a impetus for them to go out and work.

43
00:03:42,000 --> 00:03:45,000
It's not an impetus for them to go out and do something.

44
00:03:45,000 --> 00:03:50,000
They certainly wouldn't want to bring suffering throughout their beings.

45
00:03:50,000 --> 00:03:55,000
But I think there's an argument for a new mountain person to not go out and do it.

46
00:03:55,000 --> 00:04:09,000
This means a fully in light in Arahant, which of course is totally beyond the comprehension of an ordinary person.

47
00:04:09,000 --> 00:04:13,000
Maybe I think of that.

48
00:04:13,000 --> 00:04:16,000
It's not a very fun thing to hear.

49
00:04:16,000 --> 00:04:22,000
The enlightened being would just abandon their family.

50
00:04:22,000 --> 00:04:25,000
I don't know.

51
00:04:25,000 --> 00:04:29,000
I guess you don't even know if you've become them.

52
00:04:29,000 --> 00:04:35,000
It's not a thing to call there.

53
00:04:35,000 --> 00:04:38,000
Is that dispassionate?

54
00:04:38,000 --> 00:04:43,000
Are you not being compassionate?

55
00:04:43,000 --> 00:04:47,000
Well, I think what they would do is not wanting.

56
00:04:47,000 --> 00:04:56,000
They would find a way to make sure their family was provided for it or do the best that they could.

57
00:04:56,000 --> 00:05:07,000
Look, I can't do this anymore, but I don't know if they say exactly like that, but it would at least be in the sense of encouraging

58
00:05:07,000 --> 00:05:21,000
or doing the most appropriate thing within their power or within what was appropriate without actually getting rights to desire, which of course would be impossible.

59
00:05:21,000 --> 00:05:36,000
So they would maybe encourage them to ordain or to let's go ordain in the monastery or at least finding a way for them to live without the support of this person.

60
00:05:36,000 --> 00:05:47,000
No, it might even be impossible for such a person to become an hour ahead until they had worked that out.

61
00:05:47,000 --> 00:05:52,000
You know how somehow your situation can get in the way of your progress.

62
00:05:52,000 --> 00:05:59,000
I think that's one important thing that you find in the practice is that as you progress, you actually pull people along with you.

63
00:05:59,000 --> 00:06:09,000
So the people who are close to you change based on your practice because of how intertwined their destiny is with yours.

64
00:06:09,000 --> 00:06:26,000
So it may just be a point in this exercise to even ask whether an hour ahead would do in that case because they would also bring their family with them.

65
00:06:26,000 --> 00:06:29,000
It's hard to say.

66
00:06:29,000 --> 00:06:33,000
I mean, there's just other things at work here that they are on to understand karma.

67
00:06:33,000 --> 00:06:39,000
So they understand that suffering comes from our own deeds that it's never a cut and dry like that.

68
00:06:39,000 --> 00:06:44,000
Where are you abandoned people who suffer?

69
00:06:44,000 --> 00:06:50,000
And that death is not, you know, and suffering are not intrinsically wrong.

70
00:06:50,000 --> 00:06:56,000
What's wrong is the attachment to things. People don't fall into suffering because they're suffering.

71
00:06:56,000 --> 00:06:59,000
They fall into suffering because of attachment, because of desire.

72
00:06:59,000 --> 00:07:05,000
And if they can let go of desire, they can be happy in any state.

73
00:07:05,000 --> 00:07:13,000
So the idea that people might suffer because of your actions is actually something that an hour hunt has come to see through.

74
00:07:13,000 --> 00:07:23,000
But it's just so difficult to explain. It sounds kind of horrific to even say such things, but you can't sugarcoat reality.

75
00:07:23,000 --> 00:07:35,000
It may very well be the case that that's what it means to be enlightened that they do in some cases.

76
00:07:35,000 --> 00:07:38,000
Keep up.

77
00:07:38,000 --> 00:07:43,000
Because they're not going to stick around and their hunt is not going to be reborn again, right?

78
00:07:43,000 --> 00:07:48,000
They're not going to cling and to worry about their family.

79
00:07:48,000 --> 00:08:04,000
So if they, if they happen to be in a state where it's either go out of your way to go out of your way,

80
00:08:04,000 --> 00:08:14,000
they give rise to the desire to help these people or order.

81
00:08:14,000 --> 00:08:16,000
Or they'll suffer.

82
00:08:16,000 --> 00:08:19,000
Then it may be that they're just,

83
00:08:19,000 --> 00:08:20,000
most of them just suffer.

84
00:08:20,000 --> 00:08:26,000
So all I'm going to say with that is that potentially it is quote unquote wrong, but it certainly isn't wrong.

85
00:08:26,000 --> 00:08:35,000
It's not, it's not like you're going to go to, go to hell or something because you're feeding your family.

86
00:08:35,000 --> 00:08:40,000
This kind of desire is so benign, so incredibly benign.

87
00:08:40,000 --> 00:08:50,000
And the good karma associated with feeding your family and with supporting people so intense that it's an incredibly good thing.

88
00:08:50,000 --> 00:08:56,000
But all I meant to say is technically speaking the desire there may be wrong.

89
00:08:56,000 --> 00:09:02,000
In the very, very, very limited sense that it may be conducive to further rebirth.

90
00:09:02,000 --> 00:09:06,000
If may be conducive to clinging, it may be conducive.

91
00:09:06,000 --> 00:09:10,000
It seems like there's a case for that, but it's so benign.

92
00:09:10,000 --> 00:09:15,000
It's so incredibly benign that it's not even worth worrying about.

93
00:09:15,000 --> 00:09:17,000
Never worry about such things.

94
00:09:17,000 --> 00:09:21,000
Do your duty, help people be kind, be generous.

95
00:09:21,000 --> 00:09:25,000
Do good deeds and practice meditation.

96
00:09:25,000 --> 00:09:49,000
When the time comes to be in our hand you may not do such things.

