1
00:00:00,000 --> 00:00:02,760
No, welcome back to Ask a Monk.

2
00:00:02,760 --> 00:00:09,640
Next question comes from Oscar Smack, who asks, is it important to go out of our way to

3
00:00:09,640 --> 00:00:15,480
apologize to those who we may have heard or been in conflict with in the past, even if

4
00:00:15,480 --> 00:00:21,760
these people are no longer necessarily a part of our lives?

5
00:00:21,760 --> 00:00:28,840
There are Buddhist texts that talk about this sort of thing as being an important precursor

6
00:00:28,840 --> 00:00:37,840
to the meditation practice, that if we currently have issues with people, we should

7
00:00:37,840 --> 00:00:39,480
try our best to resolve those.

8
00:00:39,480 --> 00:00:45,440
I mean, it's part of a broader issue of hindrances to the meditation practice.

9
00:00:45,440 --> 00:00:51,920
Many people, when they undertake intensive meditation practice, they find themselves unable

10
00:00:51,920 --> 00:01:01,960
to complete the course due to feelings of regret and attachment of various sorts, attachment

11
00:01:01,960 --> 00:01:08,000
even, you know, many kinds of different kinds of attachment, one of which is attachment to feelings

12
00:01:08,000 --> 00:01:11,240
of guilt.

13
00:01:11,240 --> 00:01:16,880
It's certainly not the most important thing to seek a Paulija, a Paulija, to seek forgiveness

14
00:01:16,880 --> 00:01:19,400
from people who we have made of her.

15
00:01:19,400 --> 00:01:20,400
But it can help.

16
00:01:20,400 --> 00:01:28,400
I mean, obviously, it can help the other person if they are feeling wrong to know that

17
00:01:28,400 --> 00:01:36,040
you feel, you understand the wrong, because part of the attachment that we have to these

18
00:01:36,040 --> 00:01:44,120
things is the feeling that it's an ego thing, but that people who don't meditate will

19
00:01:44,120 --> 00:01:51,720
have this feeling that the other person is in the wrong and that they have to be punished

20
00:01:51,720 --> 00:01:52,960
and so on.

21
00:01:52,960 --> 00:01:59,080
They have to be made to understand what they have done wrong and we feel the satisfaction

22
00:01:59,080 --> 00:02:00,880
from punishing people.

23
00:02:00,880 --> 00:02:07,920
So when you are able to let them know that you understand that what you've done is wrong,

24
00:02:07,920 --> 00:02:10,640
that can be very helpful.

25
00:02:10,640 --> 00:02:25,920
I think simply telling people that your story, apologizing to them, is in many ways insufficient.

26
00:02:25,920 --> 00:02:32,720
You know, often we say we're sorry and then we go out and do the same thing again and

27
00:02:32,720 --> 00:02:33,720
we find this a lot.

28
00:02:33,720 --> 00:02:40,920
I found often, even though I might be sincere about my apology, that people aren't,

29
00:02:40,920 --> 00:02:45,600
the other part of the party isn't able to accept it or it doesn't have a real effect

30
00:02:45,600 --> 00:02:46,600
on them.

31
00:02:46,600 --> 00:02:50,840
They still have the attachment in their minds.

32
00:02:50,840 --> 00:02:55,840
Oftentimes I feel really bad about it and I go and apologize and the other person is confused

33
00:02:55,840 --> 00:03:01,880
and doesn't understand or thinks I'm being ridiculous that I'm still holding on to this.

34
00:03:01,880 --> 00:03:06,200
And that's really crucial.

35
00:03:06,200 --> 00:03:13,080
The most important thing is, of course, to let go of the situation by both sides.

36
00:03:13,080 --> 00:03:22,200
So sometimes it helps to let go, to be able to make some sort of contact but I think oftentimes

37
00:03:22,200 --> 00:03:30,040
if you go back and make further contact it can stay to more conflict, obviously.

38
00:03:30,040 --> 00:03:38,120
Unless the people have changed, so I don't think it's necessary all the time to find

39
00:03:38,120 --> 00:03:42,840
all the people who you've wronged or who you've had conflict with.

40
00:03:42,840 --> 00:03:49,240
I think that when you start to practice meditation, obviously the truth is when you start

41
00:03:49,240 --> 00:03:55,840
to practice meditation, you're going to start to realize some of the things that you've

42
00:03:55,840 --> 00:04:00,480
done wrong and you're going to feel bad about it.

43
00:04:00,480 --> 00:04:09,640
And most important is for you to overcome that, to realize what has been done wrong but

44
00:04:09,640 --> 00:04:15,360
be able to let go of the whole situation and let go of the ego and the self.

45
00:04:15,360 --> 00:04:18,720
Also meaning letting go of feelings of guilt.

46
00:04:18,720 --> 00:04:25,160
So it's certainly not a positive thing to feel guilty about bad things you've done.

47
00:04:25,160 --> 00:04:32,520
It's important to understand that they were wrong and understand the wisdom, to know that

48
00:04:32,520 --> 00:04:38,320
it was something wrong and to not want to ever do it again but that doesn't come from

49
00:04:38,320 --> 00:04:44,840
punishing yourself and hating yourself and feeling bad about the things that you've done.

50
00:04:44,840 --> 00:04:53,880
So the most important thing is to let go and that's only going to come really from changing

51
00:04:53,880 --> 00:04:54,880
who you are.

52
00:04:54,880 --> 00:05:01,680
That really is the thing that the person who you've heard could wish the most is that

53
00:05:01,680 --> 00:05:02,680
you change.

54
00:05:02,680 --> 00:05:11,240
Obviously, they feel that you're a person, you have issues and problems and that has caused

55
00:05:11,240 --> 00:05:15,880
you to hurt them, they're probably angry at you and so on.

56
00:05:15,880 --> 00:05:20,200
If they find out that you've changed, they'll be delighted.

57
00:05:20,200 --> 00:05:25,960
They don't want to be told that you were right and what you did, they want to find out

58
00:05:25,960 --> 00:05:30,880
that you've changed and you would never do such a thing again and they can feel vindicated

59
00:05:30,880 --> 00:05:37,560
that their suffering was that they were right in the fact that you did something wrong.

60
00:05:37,560 --> 00:05:47,280
As a result, they'll feel better that you've learned from your mistake and that they

61
00:05:47,280 --> 00:05:55,200
don't have to feel bad and feel angry at you anymore because you're a new person.

62
00:05:55,200 --> 00:05:59,920
I think that's much more important but it certainly can help especially if we've really

63
00:05:59,920 --> 00:06:07,800
heard someone or really developed some sort of unholesome this towards another person

64
00:06:07,800 --> 00:06:13,400
for our own benefit, as I said because it can otherwise be a hindrance in our meditation

65
00:06:13,400 --> 00:06:17,000
where we feel guilty about the bad things that we've done.

66
00:06:17,000 --> 00:06:21,120
But I think in the end, this guilt is something that we have to let go of, it's something

67
00:06:21,120 --> 00:06:22,120
that we have to overcome.

68
00:06:22,120 --> 00:06:26,720
It's something that according to the Buddha's teaching is unholesome.

69
00:06:26,720 --> 00:06:29,480
It's not a positive thing.

70
00:06:29,480 --> 00:06:39,080
It doesn't lead to true understanding and lead you to true change.

71
00:06:39,080 --> 00:06:45,640
But the understanding that that what you've done is wrong immediately affects a change

72
00:06:45,640 --> 00:06:51,080
and causes you to never want to do those sorts of things again, so thanks for the question.

73
00:06:51,080 --> 00:07:14,080
I hope that helps.

