Good evening everyone, welcome to our weekly question and answer session.
So we only had 13 questions and we now have 11 questions which means good questions.
Some long questions which I wanted to say without that's probably a bad thing because
it means maybe you're thinking a lot but I actually know you have to explain
sometimes not all questions are one sentence questions so that's okay but
be careful your questions don't get too theoretical. So first question is
from Robin says a layperson becomes aware of a monk acting very strangely posting
extensive statements online implying that members of a particular
religion have poisoned and sick and him and many many others. Are you talking
about me? When I first read this I thought oh I wonder if she's talking about
no it was just just kidding but I knew often criticize other religions I
don't think that's what you're referring to here. I may try not to be too
critical but I for those that I'm familiar with I mean even the Buddha was
critical of other religions so it's just a matter of criticism it's not
necessarily a wrong thing. The Buddha was quite harsh in his criticism of
other religious teachers so the ones who had it all wrong the ones who were
fools they called fools. The teachings that were foolish, he called foolish
and so on. But so your question is what should a layperson do? I mean we should
broaden this out to ask which should a layperson do? Well first let's address your
question as being which should a layperson do when they see a monk do
something that you know generally is problematic let's say could be harmful to
the monk, could be harmful to others. In this case it appears to be harmful on a
broader social scale you know what it's doing to society giving adding fuel to
an already tense ethnic situation and giving people wrong ideas. Which should a
layperson do? I think it's a bit of a red herring I want to say that a better
question is what should a layperson do when a person is engaging in this kind
of activity because I think it's important that we don't exempt monks from the
consequences of bad behavior or the consequences of their behavior. I don't
think it serves any purpose to ignore the bad deeds of a monk. So you might want
to out of respect for the Buddha and the Sangha, the Buddha, the Dhamma and the
Sangha. Treat the monk with respect when you address the problem but that
doesn't mean you should address the problem any differently. Maybe instead of
saying hey you stop that you might say hey venerable sir don't do that. Something
like that would be a little polite because the politeness comes from your
respect for the robes. But the question is whether you really should ever tell
someone don't do that and I don't think that's all that different. I mean you
might there are different roles like you'd be more in your in the right to tell
your child hey don't do that then you would be to tell your parent hey don't do
that. So I think in general we just have to I will as far as I'm going to
interest in answering this question I would you know bear it down or boil it down to the two great
question is we have a few questions but first we'll address the rightness. What
is the right thing to do? So there are two kinds of rightness and that's all I'm
going to say about that part of the question and the one is of course the
rightness right for what? So right for attaining enlightenment. So how do you know
it's right as if it is supportive of your development and your practice leading
to enlightenment. The other kind of right is a duty, a right based on duty. So
it's right to treat your parents with respect. It's right to care for your
children. It's even right you could say to have some social responsibility,
political involvement that sort of thing. These are all right in a
conventional sense and they're not ultimately right. So they can be
intervened by someone practicing to become enlightened. They are, in the
word, they are superseded by the practice of enlightenment. So they don't
actually have any intrinsic rightness but there's a rightness to them
because they create a orderly society and a society that is ideally you know
if done right it's supportive of the meditation practice. I mean
contravening them tends to lead to chaos and disturbance, distractions, stress
and so on. It leads to greater potential for theorizing and defilements. So
your other part does the monastic song have a policy on mental illness. Yes
there are rules about mental illness. Amongst was mentally ill can't be
held responsible for their actions but that means they've gone crazy.
The person is mentally sick in terms of they have bad paranoia's or so on.
That is a really good question. It's a shame that they got our day and I think
that there might be even a encouragement for them to disrobe because they
clearly can't benefit or they can't carry out their activities. I mean you
can't force a monk to disrobe. I mean there are cut in Buddhist countries
they do but it's not according to the Vinaya but I think it would be encouraged
and perhaps that monk would eventually be expelled from the Sangha if
they couldn't maintain. I mean because the idea would be are they breaking rules
or not breaking rules. If they're breaking rules then that's where it
would become a problem. I mean in this case it sounds like this person is perhaps
paranoid, perhaps delusional, perhaps breaking small minor rules but honestly I
wouldn't take it so seriously. There are far worse things that a monk can do
and that from what I hear monks in certain places do do. So simply voicing a
political opinion or opinion about religions or claiming things. I mean if
he's lying that's breaking a rule he's saying things that are not actually
happening and it's not just a deluded belief but that's really something for
the monastic community. I mean the only one who has a responsibility to
address these monks as a duty is the other monk so as a lay person it's not
your duty to do that. So that's where the right comes in. I mean there's
really no no occasion that I can think of where someone who has very little
relation to you. Where they're the instruction or the addressing of their
problems is going to support you towards your enlightenment and so it's not
right in that sense. But as a duty it's also not right because as a lay person
you really have no connection with them in terms of this is a person I should
take care of except you know to support them to give them food and so on and you
do that for the sun guys a whole and you do that for the monks who are
behaving properly. So with holding food that's the big one that lay people
engaged and if a monk is acting inappropriate it's called overturning the
ball. Isn't it? No that's the other way around. Where a monk overturns the
ball in the sense that they don't accept food from lay people. For lay people
they just withhold food and that's a sign that they don't support this monk
or their behavior. And so the other part of the question can a monk be forcibly
disrobed, forced to disrobed. The only thing that can force a monk to
disrobed is breaking the four great rules and being crazy unfortunately isn't
one of them. It's interesting I can't remember exactly what his
said or can't think of anything particularly that said about what to do if a
monk is clinically insane. It might be something to look at but that's a
monastic thing again not something that lay people should be concerned with.
It's just like you shouldn't be concerned with the pope or his dealings
or you shouldn't be concerned with the Dalai Lama or you shouldn't be concerned
with any things that don't concern you. There's no duty there and so it's
not even a functional right to do it. I think I mean I know there is a
connection because you see you call yourself Buddhist and you call this monk
Buddhist and so you think why I have a relationship but really I wouldn't make
too much of that. I would think well this person is in another group of people
or the monks and I know that what I do for monks has given food but I'm not
gonna give him food to that monk because he's not behaving properly. That's
about it. There's not much reason to do much else that I can think of.
In fact sometimes doing things is what cause you know it aggravates the
situation because then you get conflict and you get more stress and
suffering. If I meet close relatives they tell me many times that I talk so
little. I don't feel the need to join their chatter about other people and
stuff that doesn't really concern me. I don't want to tell them because it
would hurt them. Is it enough to just be mindful and still and don't speak or
should I participate and just being careful about right speech? Well the thing
about meditation is the people you surround yourself will change and so you
might not want to surround yourself with close relatives because what does it
mean? What does the word mean? It's just a concept really. It's a description
that's a label for a particular set of circumstances. None of what's really
have much to do with enlightenment or happiness or freedom from suffering. So you
know there is power in family having a close family is a great blessing that
would have said. There's a conventional thing. There's no question that having
relatives who will support you unconditionally is a good thing. In a worldly
sense but there's no closeness beyond that. Real closeness only comes from
closeness of attitude, behavior, personality type, that sort of thing, ambitions,
goals. But you get a lot. What's the word you attract more flies with honey than
vinegar? It's not quite the proper saying here but there's no reason to be
morose or to be gloomy or to be that's the word. I guess it's more of an
external thing. There's no need to project this sort of zombie state even
though that's a description for a true meditative state. You can be approachable
and peaceful. A good thing to remember is to smile once in a while. Even if you
don't feel happy, smiling is a communication. It's communicating to the
person what you feel inside. I feel okay and when you smile it shows them. So
remember smiling is a form of communication like that. But yeah there's no
reason. You say there's no need to join in the chatter but there's also no need
to not join in some way. Yes you may not want to talk at all but smiling and
nodding and offering opinions if that's what's called for. People are having
a conversation and they say something that you disagree with you don't
have to jump in. But if it gets to the point where there's an expectation
that you're a part of the conversation then you know speak when it's appropriate.
People want your opinion and give it. If you don't have an opinion say you don't
have an opinion smile and nod your head.
Again there's no hard and fast rule. The hard and fast rule is to be mindful.
Beyond that everything else is going to be complicated and it's going to depend
very much on many different conditions and situations.
Well say reading a book how should we be mindful? What should we be mindful?
And how should we incorporate the force at the baton as well while reading?
Let this question and I think quite simply if your intention is to read
your intention is not to be mindful. It's unavoidable that we have to do things
like read but because it's a mental activity you can't be mindful when you're
actively reading. Now in between the reading you can pop in and out of mindfulness
and it's good to take breaks from the reading for that reason. Read for a bit
and then do some meditation for a bit. Bring back your mindfulness but it's a
different mental activity. So you can't be mindful all the time unless you give
up all that. If you give up reading during the time when you give up other
kinds of mental activity, it's wide during meditation. We say things like
reading are to be limited and hopefully abandoned during the course.
Okay I feel desired express frustration, disappointment or anger because I feel
it conveys the actual magnitude and importance of the situation. So the idea
is that the idea presented by this questioner is that there's something that
needs to be said or the anger conveys something right. Like I'm right you're
wrong and only the anger will disappointment frustration will convey that.
We'll make it clear to you. It's like when someone's just behaving like an
idiot you slap them right because the slap is the only thing that's going to
get them to wake up. The idea like a jerk like if someone's really being me
and this is I'm not proposing this. I'm just saying this is what people do. The
anger carries a certain amount of wisdom, right. I disagree with this the whole
premise here and I mean I think you're not saying that you're a bad person's
stupid for asking. It's clear that you're asking my advice. I think you don't
reconcile it. I don't think there's really wisdom associated with the anger. I
don't. I think it's a sign of wrong-headedness to try and convince other people
that you're right and they're wrong. Even if you are right and they're wrong
what's the point? So your last part you say if I genuinely feel that there's a
wrong in the world. Oh there are lots of wrongs in the world and you're not
going to fix that and you getting angry about it isn't going to fix it.
It's going to fix the world. Just going to add more anger. It's going to, it
debases the conversation. If the good people get angry, they're debased. It's
what is called stupid to their level. Not to their entire level but in some
way it is. In the way that you're giving rise to unholesome mind states,
mind states that are desirous of the harm to others. You want to stress them
out. I mean what you have to get rid of is your need to change the world. You
need to fix the world. You need to convince other people. Otherwise you'll
never be free from things like anger and greed as well and delusion. How can
we express ourselves to convey that wrong to others? I don't see a need to convey
the wrong to others. I ask your opinion, give them your opinion. It's much more
powerful really because it shows that you are in a hold of your realm. These
people who are fussing and obsessing and fighting over things. To be outside of
that and to be able to not be moved by that is a very powerful thing that can
move people. The right to people anyway. Here's a question that I think. It's been on
my mind for a while. This is just, oh, it's gone. I pushed the wrong button. Did I
delete it? No, it's an answer, right? So this person is Thai and they recently
developed a VR virtual reality application, but even more averse immersive. So
I don't live in Thailand. I can give you my email. My email is just
youtadamo at gmail.com, but don't tell anybody because then people will try and
email me. You can email me. I don't answer my emails. I think I've put it
out on the internet before. I get too many emails and I don't answer most of
them. My defense is I'm a monk and no monks probably shouldn't have email
addresses in the first place, but I do and I just don't answer them in all of
them. Most of them. So don't expect an answer if anyone needs to come to
email me. But this one, see, when I was in Thailand maybe last time or the time
before my uncle lives in Thailand and it's one of the reasons I originally went
his company had purchased a virtual reality or one of the people working there
had purchased a virtual reality system. Like the real deal with the headset
and everything. And so I they showed it to me and I tried it out and I ended up on
a mountain top cliff and I was sitting across, I actually sat down on the
carpet and the air condition was blowing on me so there was a breeze and I was
looking out at the cliff and there was a little dog and I could throw a stick
for it. But it was a little bit impressive and it made me think and I talked
about this, the idea of having a virtual reality meditation session. I've
talked about this I think before on the internet but I'm interested in what
you have and maybe someday we'll have a virtual reality set here and I can do
virtual reality conversations with my students or I can record virtual
I've looked at you can record virtual reality videos and then people who watch
them can have it be immersive. So there's a little benefit to that idea. I mean
of course the technological requirements are very steep still I think I think I
don't know and it's fake right but there's nothing less real about seeing
fake trees than there's about seeing real trees from Buddhist perspective so
the environment of it I think there's potential for being in a virtual reality
then you can just escape your room and all the clutter in it and all the
things that distract you and listen to someone sit there and teach you and
watch them do walking meditation I mean you can do virtual reality walking
meditation demonstrations and so on.
I'm interested mildly interested. Okay questions about the book that this person
has two questions. See clearly how we are causing should we
consciously be aware of the causing so we cause problems by our reaction so when
you say to yourself angry angry what I mean by causing seeing how you're
causing is seeing the problem with the anger seeing that anger is not useful
when you say to yourself angry angry it's almost indirect because all it
does is create objectivity but the objectivity that you create from being
mindful is going to allow you to see the repeated anger for what it is all
this comes up as a result of this comes up as a result oh it comes up as a
result again and watching that you're going to see how wrong it is to get
angry just because it's stressful because it's useless because it's
unhelpful so you see it again and again and again and that's how you're going
to understand. I wouldn't and you've got another question here but
understanding so let's just lump them together if I can so but
understanding that they also cause they are also the cause for suffering
and stress I wouldn't put too much emphasis I mean the word
understand means probably less than you think it does you're looking for some
kind of epiphany or that kind of thing there are some epiphanies but it's more
just seeing things as they are I mean anger is stressful they're you are
causing stress to yourself unless you're enlightened there is stress in
your in your makeup you know you react to things improperly you you leap
out after things you want you push away run away from and to try and destroy
the things you don't want you get arrogant you get conceded and all this so
observing and learning to be objective is going to show you all of that it's
also going to show you the nature of the things that you react that they're
not worth reacting to so they're seeing both but it's just from watching
and you that this noting technique is just a means of allowing us to watch
keeping us objective and making us objective creating this
rectitude of mind the straightness of mind there was a time when I give talks
every night am I going to do that again not not now I think talking too much
and teaching too much is hello is problematic because it focuses too much
attention on the listening on the theory on the thinking and and also it's
just it's not my idea of where I want to go and to teach too much and you
could become a professor in the university you could become a teacher who
teaches every day not really how I see myself okay question about the
domo part of this is nice when I give a video and then people come and ask
questions about it for for clarification so the question is about this word
I think it was we tina what's the word parallok kasa we tina parallok kasa
something we tina or whatever the word was just means thrown away discarded so
it's not asking whether it's not addressing the question of whether this
person believes in an afterlife or not what it means is when you lie it's one
of those things that that is the act of throwing away your future whether you
believe in it or not the result is the same so whether this person believes or
doesn't deserve relevant means they've thrown it away so it and it may be
ambiguous that that that may even mean that they have no belief in it right
that they've they've discarded it so the reason they're okay with lying is
because I don't I don't think so but I get what you're saying like they're
lying because they don't believe in an afterlife they've thrown away that
idea and that's what lead but it's not exactly right for the context because
it's just any kind of lie these these these this woman wasn't lying to the
Buddha because she didn't believe in an afterlife necessarily but I think it's
ambiguous because that's a part of it she can't have any strong knowledge of an
afterlife or else you wouldn't do something so horrible although you know
there are that's not even true because there are sects of Christianity I think
and Islam that believe in afterlife but do horrible things in support of it
or have done horrible things in support of the afterlife right so I don't
think that's what it's referring to I think it simply means they've thrown away
their future according to Buddhism are we allowed to lie to save an innocent
person's life you're allowed to do anything you want Buddhism is not about
disallowing things but there's consequences the Buddha recommend or the
Buddha laid down a precept not to lie but it's voluntary that's voluntary
anyone who wants to practice the Buddha's teaching should not lie if they
want to say they're following the Buddha's teaching if they're lying they're
not following the Buddha's teaching
can meditation I'm only gonna answer the first part of this question because
I don't have anything about are you meditating correctly it's
no read my booklet you want my advice read my booklet that's all I have to
say only teach one thing don't look to me for advice on anything else but
can meditation alleviate anxiety mood disorders under depression yes absolutely
try and read my booklet practicing that way I can guarantee that if you do it
properly it should help with your depression and anxiety and so on how can one
move we've got new questions sneaky how can one move beyond
syllabata parama sir become a sotapana okay these are new questions that's
not fair I haven't gone over them they haven't been vetted yet so new
questions can answer next week yes that's fair I think because they also have
to be voted on once we get lots of questions the good ones rise to the top
I'm less likely I do sometimes but I'm less likely to delete questions that
have lots of uploads but I will I still will
Buddhas and avatars just you Buddha's not an avatars just you know but as a
word for someone who is enlightened video output low okay that's all for tonight
thank you all have a good night
