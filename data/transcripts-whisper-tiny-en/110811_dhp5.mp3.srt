1
00:00:00,000 --> 00:00:04,580
Hello, welcome back to our study of Vedam Bada.

2
00:00:04,580 --> 00:00:10,100
Today we continue on with verse number 5, which goes.

3
00:00:10,100 --> 00:00:17,200
Nahi wirina wirani, sammantidha kudachana.

4
00:00:17,200 --> 00:00:28,400
Awirina jasamanti, asadamos anantamam, which means not indeed through enmity

5
00:00:28,400 --> 00:00:33,500
is enmity, appeased, not here or anywhere.

6
00:00:33,500 --> 00:00:40,300
Awirina jasamanti, it is appeased through non-enmity.

7
00:00:40,300 --> 00:00:44,300
This is an eternal truth.

8
00:00:44,300 --> 00:00:49,400
This teaching was given in regards to a famous story, another famous one,

9
00:00:49,400 --> 00:00:55,200
about two women who got very much caught up in the cycle of enmity or revenge,

10
00:00:55,200 --> 00:00:59,900
and would never have gotten out if it weren't for the teaching of the Buddha.

11
00:00:59,900 --> 00:01:03,100
This is the our understanding.

12
00:01:03,100 --> 00:01:07,400
These two women were married to the same man, and one of them was barren,

13
00:01:07,400 --> 00:01:09,600
the other one was fertile.

14
00:01:09,600 --> 00:01:14,200
And the fertile woman got pregnant three times, and each time the barren woman,

15
00:01:14,200 --> 00:01:21,900
being afraid of the being left out or being cast aside,

16
00:01:21,900 --> 00:01:26,600
causing the abortion in the fertile woman.

17
00:01:26,600 --> 00:01:30,100
It was slipping some kind of medicine into her food.

18
00:01:30,100 --> 00:01:35,100
And the third time the woman actually died as a result of the abortion.

19
00:01:35,100 --> 00:01:41,100
And while she was dying, she cursed her murderer, saying,

20
00:01:41,100 --> 00:01:56,600
valing revenge, valing to come back someday, and pay revenge for this evil deed.

21
00:01:56,600 --> 00:01:58,100
And she died.

22
00:01:58,100 --> 00:02:02,600
And the woman who killed her, or who caused the abortion,

23
00:02:02,600 --> 00:02:08,100
was beaten by her husband for being such an evil person,

24
00:02:08,100 --> 00:02:11,600
and so on, and her husband was a fairly rough person, I guess.

25
00:02:11,600 --> 00:02:20,100
And died as a result as well, was beaten to the point where she got so sick she died.

26
00:02:20,100 --> 00:02:26,100
And was born in the same house as a hen, a female chicken.

27
00:02:26,100 --> 00:02:34,100
And so happened that the woman who died from the abortion was born in the same house as a cat.

28
00:02:34,100 --> 00:02:36,100
So you can see where this is going.

29
00:02:36,100 --> 00:02:44,600
The hen laid eggs into the chicks hatched, and the cat came along and ate the chicks three times.

30
00:02:44,600 --> 00:02:50,600
And the third time actually ate the chicken as well, or killed the chicken as well.

31
00:02:50,600 --> 00:02:53,600
The chicken died and was born as a leopard.

32
00:02:53,600 --> 00:02:58,600
The cat died and was born as a deer.

33
00:02:58,600 --> 00:03:06,100
And the leopard came, and they were well familiar with each other at this point.

34
00:03:06,100 --> 00:03:11,600
The leopard came and ate the doe, the deer's fawns.

35
00:03:11,600 --> 00:03:18,600
And the third time ate or killed the doe as well.

36
00:03:18,600 --> 00:03:22,600
The doe came back, each time valing revenge, although the person who was killed with each time,

37
00:03:22,600 --> 00:03:24,600
keep it in their mind.

38
00:03:24,600 --> 00:03:29,600
When the hen was dying, she said, you've done this to me three times now.

39
00:03:29,600 --> 00:03:34,600
One day I'm going to come back, and indeed she came back, and the deer are the same,

40
00:03:34,600 --> 00:03:36,600
and the deer came back as an ogre.

41
00:03:36,600 --> 00:03:45,600
This is the woman who had died from the abortion and made a vow to come back to eat this woman's children.

42
00:03:45,600 --> 00:03:54,600
The died was born as an ogre, and the leopard was born as a woman.

43
00:03:54,600 --> 00:03:58,600
An ordinary woman living near, I believe, in Sawati.

44
00:03:58,600 --> 00:04:02,600
I'm just sitting in great city in the time of the Buddha.

45
00:04:02,600 --> 00:04:10,600
And twice this ogre, an ogre is like an evil spirit that eats your children.

46
00:04:10,600 --> 00:04:16,600
Again, this part of the story I don't vouch for, the idea that there could be an ogre eating,

47
00:04:16,600 --> 00:04:23,600
or children eating ogres, 2,500 years ago, seems to me a little bit far-fetched.

48
00:04:23,600 --> 00:04:28,600
So it may just have been a woman who was a cannibal or something.

49
00:04:28,600 --> 00:04:32,600
It may have just been, you know, whatever it was, because these things do happen.

50
00:04:32,600 --> 00:04:37,600
Even in our life, there are people who eat other people who kill and murder them,

51
00:04:37,600 --> 00:04:40,600
and so on, there are terrible things that happen in this world.

52
00:04:40,600 --> 00:04:42,600
So I don't disbelieve that.

53
00:04:42,600 --> 00:04:45,600
This is just, you know, an extraordinary example.

54
00:04:45,600 --> 00:04:48,600
But that it was an ogre, well, okay.

55
00:04:48,600 --> 00:04:55,600
It's not so important, because it doesn't affect the moral of the story at all, really.

56
00:04:55,600 --> 00:04:57,600
But let's go along with it.

57
00:04:57,600 --> 00:05:01,600
It was an ogre, and ate this woman's children twice.

58
00:05:01,600 --> 00:05:04,600
And the third time, the woman saw her coming and ran.

59
00:05:04,600 --> 00:05:09,600
And how it happened was the third time the woman left with her husband and said,

60
00:05:09,600 --> 00:05:10,600
we've got to get away from this.

61
00:05:10,600 --> 00:05:15,600
This is like, you know, every lifetime, we're in the cycle of revenge,

62
00:05:15,600 --> 00:05:16,600
and so she ran away.

63
00:05:16,600 --> 00:05:25,600
And the ogre chased after her and found her near Jita Wana,

64
00:05:25,600 --> 00:05:29,600
which is the monastery of the Buddha near Saluti.

65
00:05:29,600 --> 00:05:33,600
And they were bathing, she and her husband were bathing in the pool outside,

66
00:05:33,600 --> 00:05:37,600
and her husband went to bathe, and she was holding her child.

67
00:05:37,600 --> 00:05:40,600
And then she turned and saw there was the ogre coming.

68
00:05:40,600 --> 00:05:43,600
And right away, they recognized each other, and she knew right away who this was.

69
00:05:43,600 --> 00:05:46,600
It was like they had this bond.

70
00:05:46,600 --> 00:05:52,600
And so the woman ran into Jita Wana, and the Buddha at that time, at that moment,

71
00:05:52,600 --> 00:05:57,600
was giving a talk to the large group of people whose followers.

72
00:05:57,600 --> 00:06:00,600
And so she ran right up to the Buddha and dropped her son at the Buddha's feet

73
00:06:00,600 --> 00:06:05,600
and said, here's my child, please protect him, save his life.

74
00:06:05,600 --> 00:06:13,600
And fortunately, the angels of the garden, Jita Wana, at the time,

75
00:06:13,600 --> 00:06:15,600
didn't let the ogre in.

76
00:06:15,600 --> 00:06:19,600
They have power over it and spirits go like this.

77
00:06:19,600 --> 00:06:22,600
The idea that there might be evil spirits doesn't really face me.

78
00:06:22,600 --> 00:06:27,600
The idea that they might eat your children is a little bit, as I said, far-fetched.

79
00:06:27,600 --> 00:06:32,600
But okay, so there was a contact and this evil spirit wasn't allowed in.

80
00:06:32,600 --> 00:06:42,600
And until the Buddha called on and his attendant to go and bring in the ogre to invite her in,

81
00:06:42,600 --> 00:06:45,600
and the Buddha said, what are you doing?

82
00:06:45,600 --> 00:06:49,600
He said, how can you do not realize where this is headed here?

83
00:06:49,600 --> 00:06:54,600
Three lifetimes, you haven't been the aggressor every time.

84
00:06:54,600 --> 00:06:58,600
And you can't hope to possibly win every time.

85
00:06:58,600 --> 00:07:00,600
And you're creating a cycle of revenge.

86
00:07:00,600 --> 00:07:08,600
And he said, if you didn't come and find me, if you didn't have someone here to explain to you the problem inherent

87
00:07:08,600 --> 00:07:12,600
in this sort of behavior, you would have continued on like this forever.

88
00:07:12,600 --> 00:07:19,600
And then he said as verse, this verse that goes, not by enmity is enmity appeased,

89
00:07:19,600 --> 00:07:24,600
not here or anywhere, but only by non-enmity is enmity appeased.

90
00:07:24,600 --> 00:07:26,600
This is an eternal truth.

91
00:07:26,600 --> 00:07:31,600
And just by waking them up and explaining it to them in this way,

92
00:07:31,600 --> 00:07:38,600
he was able to help them to overcome it and actually the ogre was able to let go.

93
00:07:38,600 --> 00:07:44,600
Now, why I think this is a good lesson for us?

94
00:07:44,600 --> 00:07:51,600
Because it's part of this helping us to open up and see the bigger picture.

95
00:07:51,600 --> 00:07:55,600
You can see where we're leading ourselves and where our minds are taking us.

96
00:07:55,600 --> 00:08:00,600
That every act that we do does have an effect on our minds.

97
00:08:00,600 --> 00:08:05,600
Or can potentially have an effect on our mind if we cling to it.

98
00:08:05,600 --> 00:08:08,600
There's obviously some incredible clinging going on here.

99
00:08:08,600 --> 00:08:10,600
There's incredible clinging to revenge.

100
00:08:10,600 --> 00:08:14,600
This person heard me, as I said in the other verses, we have Kochimanga,

101
00:08:14,600 --> 00:08:17,600
Guadimanga, Dinimanga, Haatsimay.

102
00:08:17,600 --> 00:08:20,600
He hit me, he beat me, he hurt me, and so on.

103
00:08:20,600 --> 00:08:24,600
When we think like that, it builds up in our mind.

104
00:08:24,600 --> 00:08:29,600
And in this story, we see an example of the result and the consequences.

105
00:08:29,600 --> 00:08:31,600
It's not only in this life.

106
00:08:31,600 --> 00:08:39,600
And this is something that we obviously lose sight of for the most part in this world.

107
00:08:39,600 --> 00:08:43,600
Because we think of things only in terms of this one life.

108
00:08:43,600 --> 00:08:46,600
And so we think of in terms of getting ahead.

109
00:08:46,600 --> 00:08:48,600
We think of getting the better.

110
00:08:48,600 --> 00:08:54,600
We talk about the doggy dog world, and how if you can get the better of other people.

111
00:08:54,600 --> 00:08:56,600
And come out on top, and so on.

112
00:08:56,600 --> 00:09:00,600
So then by the time you're in your middle age, you've got money, and you've got power,

113
00:09:00,600 --> 00:09:03,600
and you've got influence, and you've got stability.

114
00:09:03,600 --> 00:09:05,600
That somehow you've achieved something.

115
00:09:05,600 --> 00:09:07,600
You've won in that sense.

116
00:09:07,600 --> 00:09:10,600
If you can do it morally and great, if you can.

117
00:09:10,600 --> 00:09:16,600
It's not really important, because in the end you're going to die anyway.

118
00:09:16,600 --> 00:09:20,600
So better to get the better of other people.

119
00:09:20,600 --> 00:09:26,600
And I think most of us in this world, or the majority of people in this world,

120
00:09:26,600 --> 00:09:29,600
do partake of this in some extent.

121
00:09:29,600 --> 00:09:33,600
We don't mind so much getting the better of other people.

122
00:09:33,600 --> 00:09:37,600
You know, in business, we don't mind cheating other people.

123
00:09:37,600 --> 00:09:40,600
We don't mind conning other people, and so on.

124
00:09:40,600 --> 00:09:44,600
We don't mind trying to get the better of people, and getting their money, and so on.

125
00:09:44,600 --> 00:09:47,600
Just so long as we can somehow come out on top,

126
00:09:47,600 --> 00:09:51,600
or we can get something good for ourselves at the time.

127
00:09:51,600 --> 00:09:56,600
And I think this is, here we have a really good example of these two women

128
00:09:56,600 --> 00:10:01,600
who are trying to secure a happiness for themselves

129
00:10:01,600 --> 00:10:09,600
against the rival, this woman who was jealous of the woman who was fertile.

130
00:10:09,600 --> 00:10:17,600
But the point we have in common with all of our feuds is that it carries on.

131
00:10:17,600 --> 00:10:20,600
And it's really quite easy to see.

132
00:10:20,600 --> 00:10:23,600
And this is why it's really quite easy to understand.

133
00:10:23,600 --> 00:10:28,600
It's funny to see so many people having trouble with the idea that the mind might continue on.

134
00:10:28,600 --> 00:10:37,600
We're building up an incredible power or an incredible energy through our deeds.

135
00:10:37,600 --> 00:10:40,600
People who cling, people who build up this cycle of revenge,

136
00:10:40,600 --> 00:10:46,600
who have quarrels with other people who fight and dispute and hurt and harm others,

137
00:10:46,600 --> 00:10:51,600
and fight and are victorious even over other people.

138
00:10:51,600 --> 00:10:55,600
They build up this incredible energy in their mind that drags them down,

139
00:10:55,600 --> 00:10:59,600
and actually puts them in the place of the victim in the future.

140
00:10:59,600 --> 00:11:03,600
As a result of the defilement of the mind, because defilement is, as I've said,

141
00:11:03,600 --> 00:11:07,600
the problem with it is that it drags your mind down.

142
00:11:07,600 --> 00:11:11,600
It defiles and it soils, it sullies the mind.

143
00:11:11,600 --> 00:11:17,600
It hurts you. It takes away your ability to function and cripples you.

144
00:11:17,600 --> 00:11:24,600
So the reason why we're in these unpleasant states is because of our crippled mind in the past.

145
00:11:24,600 --> 00:11:31,600
When a person comes out on top, they've developed all this clinging and all this evil in their mind,

146
00:11:31,600 --> 00:11:35,600
even though they might have great luxury and power externally.

147
00:11:35,600 --> 00:11:40,600
It drags them down and if they don't use their power in a good way,

148
00:11:40,600 --> 00:11:46,600
it will drag them down and the cycle of revenge will continue.

149
00:11:46,600 --> 00:11:51,600
Our inability to see beyond one life leads us to,

150
00:11:51,600 --> 00:11:58,600
this is something I've said, it doesn't really matter whether the mind does continue on.

151
00:11:58,600 --> 00:12:04,600
You can see the difference in our beliefs, if you believe that at death something stops,

152
00:12:04,600 --> 00:12:08,600
and that's it, and you don't have to go on, then it really doesn't matter what you do.

153
00:12:08,600 --> 00:12:13,600
We can see how that's, the fact that it's happening on the world,

154
00:12:13,600 --> 00:12:18,600
that it's deteriorating and we're depleting our resources.

155
00:12:18,600 --> 00:12:22,600
The depletion of resources, well who cares? When I die, that's it.

156
00:12:22,600 --> 00:12:24,600
Wrong, really.

157
00:12:24,600 --> 00:12:32,600
The effect of your mind states and the greed that's in your mind alone is something that will last you on,

158
00:12:32,600 --> 00:12:36,600
because of our attachment and our clinging.

159
00:12:36,600 --> 00:12:40,600
On the other hand, if you understand life, if you understand,

160
00:12:40,600 --> 00:12:45,600
or let's say if you believe, if we think of it parallel in terms of belief,

161
00:12:45,600 --> 00:12:53,600
the mind doesn't stop and this experience continues on and works itself out.

162
00:12:53,600 --> 00:13:00,600
So the energy that we build up through our actions has eventually to bring consequences.

163
00:13:00,600 --> 00:13:04,600
Then you really think, do you think it's possible you would go to war?

164
00:13:04,600 --> 00:13:08,600
Do you think it's possible you would cling to things?

165
00:13:08,600 --> 00:13:13,600
Or if you really believe this, would you ever have any reason to build up

166
00:13:13,600 --> 00:13:21,600
and to try to find pleasure in the year and now to try to become addicted to things

167
00:13:21,600 --> 00:13:30,600
and to reach, to crave for things or to run after ephemeral pleasures?

168
00:13:30,600 --> 00:13:33,600
You'd have no reason to, if you understand this,

169
00:13:33,600 --> 00:13:36,600
you really, when you look, when you sit back and think about it,

170
00:13:36,600 --> 00:13:41,600
you think this is really whatever I get in this world, whatever I achieve,

171
00:13:41,600 --> 00:13:47,600
whatever I accomplish, if I become rich, if I become famous, it's meaningless.

172
00:13:47,600 --> 00:13:56,600
And in the end, it's quite inconsequential,

173
00:13:56,600 --> 00:14:00,600
because it lasts for a moment and then it's gone.

174
00:14:00,600 --> 00:14:05,600
And this is the idea that it will be gone, that everything does cease

175
00:14:05,600 --> 00:14:13,600
and the evil that we do now will drag us down and bring evil unto us.

176
00:14:13,600 --> 00:14:20,600
So this is something that helps us to see the true nature of reality

177
00:14:20,600 --> 00:14:24,600
and it answers a lot of questions in regards to why people are victims,

178
00:14:24,600 --> 00:14:32,600
why people suffer, why people are in a place in a position of great inequality

179
00:14:32,600 --> 00:14:37,600
where some people are very rich, some people are very poor, and so on.

180
00:14:37,600 --> 00:14:43,600
We can see that there are these waves and the person who is rich now may be poor in the next life,

181
00:14:43,600 --> 00:14:45,600
depending on the state of their mind, really.

182
00:14:45,600 --> 00:14:48,600
If a rich person is very generous, then there's no reason

183
00:14:48,600 --> 00:14:51,600
if they use their power and their influence in the right way,

184
00:14:51,600 --> 00:14:55,600
then you can see that this is something that is going to be the last

185
00:14:55,600 --> 00:14:58,600
because their mind is powerful, their mind is strong,

186
00:14:58,600 --> 00:15:03,600
they are sure of themselves and they have this great charge of good energy,

187
00:15:03,600 --> 00:15:05,600
which will come back to them.

188
00:15:05,600 --> 00:15:08,600
Obviously it's something that you can feel.

189
00:15:08,600 --> 00:15:12,600
A person who does good deeds is able to hold their head up,

190
00:15:12,600 --> 00:15:17,600
they're able to sit up tall and they don't shrink down,

191
00:15:17,600 --> 00:15:23,600
there's none of this clinging that drags them down

192
00:15:23,600 --> 00:15:26,600
and actually drags them to help.

193
00:15:26,600 --> 00:15:29,600
And that lifts them up and makes them feel light,

194
00:15:29,600 --> 00:15:32,600
and makes them feel free from guilt and so on.

195
00:15:32,600 --> 00:15:41,600
So this lesson is in regards to helping us to give up our quarrels

196
00:15:41,600 --> 00:15:46,600
because any kind of victory that we could gain is ephemeral,

197
00:15:46,600 --> 00:15:51,600
it's temporary, and it only encourages more and more,

198
00:15:51,600 --> 00:15:56,600
more quarrelling, more and more enmity as the person who's defeated,

199
00:15:56,600 --> 00:15:58,600
unless they can somehow give up,

200
00:15:58,600 --> 00:16:03,600
one of us can in the end let go, then they will surely seek revenge.

201
00:16:03,600 --> 00:16:05,600
Regardless of whether they speak revenge,

202
00:16:05,600 --> 00:16:08,600
the evil that we have done will drag us down,

203
00:16:08,600 --> 00:16:14,600
and drag us down to a state of suffering and stress and so on.

204
00:16:14,600 --> 00:16:16,600
So that's the lesson here.

205
00:16:16,600 --> 00:16:21,600
This is verse number five, thanks for tuning in and all the best.

