1
00:00:00,000 --> 00:00:12,640
Okay, good evening everyone.

2
00:00:12,640 --> 00:00:28,000
Welcome to our live demo.

3
00:00:28,000 --> 00:00:43,920
Going back to the ocean again, the ocean has one taste of salt, and the dhamma has

4
00:00:43,920 --> 00:00:57,160
but one taste, and that is the taste of freedom, tonight I thought I'd talk a little bit more

5
00:00:57,160 --> 00:01:10,120
about freedom, freedom is interesting, it's interesting how we approach freedom, there are

6
00:01:10,120 --> 00:01:21,800
three ways of speaking about freedom, we speak of freedom from, we speak of freedom to, and

7
00:01:21,800 --> 00:01:31,720
we speak of some ambiguous freedom of, or general freedom of, so freedom of is the type

8
00:01:31,720 --> 00:01:41,000
of freedom, but the interesting, it gets interesting when we talk about freedom from and

9
00:01:41,000 --> 00:01:59,920
freedom to, too often freedom is phrased in terms of freedom to, too often because there's

10
00:01:59,920 --> 00:02:06,920
not really, it's not really freedom, when you talk about freedom to, you're not actually

11
00:02:06,920 --> 00:02:20,400
emphasizing the freedom, freedom to is still freedom from, freedom from something, freedom

12
00:02:20,400 --> 00:02:26,880
from obstruction, but when you phrase it like that, the emphasis isn't actually on the

13
00:02:26,880 --> 00:02:54,400
freedom, the emphasis is on the ambition, it's on the desire or the inclination, we miss

14
00:02:54,400 --> 00:03:00,240
that, because in order to be free, to do something, you need to be free from something

15
00:03:00,240 --> 00:03:10,640
else, and there's an important, it's really an important distinction, but as a, we're

16
00:03:10,640 --> 00:03:21,440
not really concerned about the freedom to, freedom to imply some sort of desire or inclination

17
00:03:21,440 --> 00:03:44,160
or need to do something, freedom to say what you want, freedom to do what you want, it's

18
00:03:44,160 --> 00:03:53,480
all about what we want, it's not actually about the freedom, and we miss that, we go beyond

19
00:03:53,480 --> 00:04:01,680
the freedom and into the ambition, which interestingly enough is not freedom at all, it's

20
00:04:01,680 --> 00:04:18,760
the freedom of the, the tar baby, the tar baby, the story of the tar baby, I don't

21
00:04:18,760 --> 00:04:23,560
know what the origin of the story, I heard it with a rabbit, there was this cartoon rabbit

22
00:04:23,560 --> 00:04:36,120
and it had a real, it was a real sort of cocky rabbit, so they made this caricature out

23
00:04:36,120 --> 00:04:43,120
of tar, I think this is actually even in the commentaries or maybe even the Buddha

24
00:04:43,120 --> 00:04:53,680
used this example, and so this rabbit comes up to the tar baby and attacks it, because

25
00:04:53,680 --> 00:04:59,120
the tar baby won't speak to him, so he punches it, and when he punches and he gets his

26
00:04:59,120 --> 00:05:13,080
fist up, and so then he tries to get his first fist out, and gets his second fist

27
00:05:13,080 --> 00:05:19,080
stuck, then he uses his feet to try to get his hands out, and eventually he gets completely

28
00:05:19,080 --> 00:05:26,040
stuck and is carrying away by the hunter, I think the Buddha used this example of a hunter,

29
00:05:26,040 --> 00:05:40,920
uses this tar figure to catch a monkey maybe, why that relates to freedom too is because

30
00:05:40,920 --> 00:05:48,480
if you look at the Buddhist cosmology, the understanding of the way the world works, it

31
00:05:48,480 --> 00:05:54,160
becomes quite clear this whole existence, we have our whole life as human beings in the

32
00:05:54,160 --> 00:06:02,520
society we've built up, it's just us getting further and further ensnared, all of this

33
00:06:02,520 --> 00:06:19,800
liberty, freedom too has the exact opposite of freedom, freedom to go in debt, freedom to

34
00:06:19,800 --> 00:06:32,200
become indentured, freedom to commit, freedom to get married to people, to your society, freedom

35
00:06:32,200 --> 00:06:52,280
to be born into a society with laws and rules and culture, man is born free and everywhere

36
00:06:52,280 --> 00:06:59,080
he is in chains, Jean-Jacques Rousseau said this, I don't care exactly what he was referring

37
00:06:59,080 --> 00:07:14,440
to but it's quite true, we were born without much of any kind of commitment or requirement

38
00:07:14,440 --> 00:07:23,400
and then we go about our whole lives enslaving ourselves, even just being born, it's

39
00:07:23,400 --> 00:07:32,280
not without slavery, we're slaves to hunger and thirst, whenever free from hunger or thirst

40
00:07:32,280 --> 00:07:41,760
or the need to urinate or defecate to breathe, our desires for sexual activity, our desires

41
00:07:41,760 --> 00:07:52,560
for romance, our desires for friendship, companionship, entertainment, appreciation, pride,

42
00:07:52,560 --> 00:08:04,840
a list goes on and on, accomplishment, ambition, all of this freedom too, no, that's the

43
00:08:04,840 --> 00:08:09,840
American dream, the America's so funny, everyone picks on America but it's all that America

44
00:08:09,840 --> 00:08:17,800
stands for is this enslavement, America's the land of the free, free to enslave yourself

45
00:08:17,800 --> 00:08:26,520
further and further into the depths and the dredges of Samsara, so what America was built

46
00:08:26,520 --> 00:08:35,840
on, right, get away from the constraints, do what you want, live like a cowboy, get further

47
00:08:35,840 --> 00:08:46,360
and further and meshed in the web of Samsara, that's not freedom, freedom too is not freedom

48
00:08:46,360 --> 00:09:04,920
at all, it's freedom from, so much that holds us back, that keeps us tied down, freedom

49
00:09:04,920 --> 00:09:12,800
from suffering, to be free from suffering, you need freedom from defilement, you need freedom

50
00:09:12,800 --> 00:09:19,720
from ignorance to be free from defilement, so the Buddha is very much about what we have

51
00:09:19,720 --> 00:09:43,880
to be free from, free from Samsara, that's the hardest kind of freedom, it's much easier

52
00:09:43,880 --> 00:09:53,520
to fight for your, freedom too do things than it is to be free from, fight for your rights,

53
00:09:53,520 --> 00:10:03,240
say how hard that is, it's much harder to be free from, to be free from defilement, to

54
00:10:03,240 --> 00:10:10,040
be free from suffering, on the one hand I guess on the other hand it could even be

55
00:10:10,040 --> 00:10:17,000
argued that it's much easier, that's much more attainable, because you cannot always be

56
00:10:17,000 --> 00:10:22,040
free to do what you want, you can fight for it and work for it and if not in this life

57
00:10:22,040 --> 00:10:30,920
then in the next you'll be able to attain your goals, but again last forever you can be

58
00:10:30,920 --> 00:10:38,080
happy for days, weeks, months, years, it is possible to be quite happy most of the time

59
00:10:38,080 --> 00:10:47,080
for a time to get what you want most of the time some people are able for a time, but

60
00:10:47,080 --> 00:10:55,320
it doesn't last, it's not permanent or stable, it's dependent on conditions and causes

61
00:10:55,320 --> 00:11:07,400
and ambitions and it's dependent on work, but freedom from, freedom from is such a powerful

62
00:11:07,400 --> 00:11:17,000
thing and the dhamma is such a powerful force to be able to deliver this very simple, simple

63
00:11:17,000 --> 00:11:30,760
stain of freedom, this is why meditation is so free from, a quitriment, it's so free from

64
00:11:30,760 --> 00:11:37,880
complication, meditation is quite simple really, right, when we hear about the meditation

65
00:11:37,880 --> 00:11:42,520
practice it sounds kind of stupid really, why should I sit there and say to myself,

66
00:11:42,520 --> 00:11:51,760
pain, pain, what the heck is that going to do right, we've got complex ideas about how to fix

67
00:11:51,760 --> 00:12:04,880
things, we're ambitious about it, but if you want to be free you have to be free here

68
00:12:04,880 --> 00:12:24,480
and now you have to be free, free and jail, being free and Buddhism is like being free and jail.

69
00:12:24,480 --> 00:12:28,600
Prisons interesting because you have the prisoners on the inside and then you have the

70
00:12:28,600 --> 00:12:33,040
guards that keep them in, but the guards are as much a prisoner as the prisoner, in

71
00:12:33,040 --> 00:12:38,360
a sense the guards are required to come back every day, the guards are required to work,

72
00:12:38,360 --> 00:12:43,560
the guards are required to worry and care and the prisoners are actually quite free,

73
00:12:43,560 --> 00:12:52,840
they don't have to do anything, they eat, they have food brought to them and they eat,

74
00:12:52,840 --> 00:12:58,640
they have no worries or stresses, well in prison there are prisons nowadays are quite terrible

75
00:12:58,640 --> 00:13:11,720
places for evil people, not evil people, but for poor conditions and they make people

76
00:13:11,720 --> 00:13:20,880
evil and crazy, but it's interesting because it's not even so much that the people who

77
00:13:20,880 --> 00:13:25,400
go to prison are evil, it's that when you put people together like that and treat

78
00:13:25,400 --> 00:13:34,400
them like animals, because they're not free, because they're not free from defilement,

79
00:13:34,400 --> 00:13:43,560
it's like a pressure cooker, they build up the pressure and it explodes, people out of

80
00:13:43,560 --> 00:13:52,160
fear and worry and attachment will go crazy in such an environment and so there's a lot

81
00:13:52,160 --> 00:13:58,680
of wickedness that comes out that is bread in prison, but ostensibly prison is quite

82
00:13:58,680 --> 00:14:05,320
peaceful, the concept of it, that there's so much suffering in prison and evil in prison

83
00:14:05,320 --> 00:14:12,600
because as people in prison don't know how to be free, I had one when I was teaching

84
00:14:12,600 --> 00:14:18,880
in the federal detention center in Los Angeles, and one meditator asked me if I could teach

85
00:14:18,880 --> 00:14:26,720
him how to meditate, to levitate, and to fly away, I said I was quite clever of him to

86
00:14:26,720 --> 00:14:34,800
think of that, and now that's not what I teach and that's not true for you know, that

87
00:14:34,800 --> 00:14:44,520
wouldn't make him free, so there have been stories of prisoners successfully conducting

88
00:14:44,520 --> 00:14:50,520
meditation courses and finding great freedom, I mean there's nothing terrible about prison

89
00:14:50,520 --> 00:14:58,040
ostensibly, there's any requirements, it's a lot like being a monk, you just have to come

90
00:14:58,040 --> 00:15:06,560
and eat, I don't know what else you have to eat, what else is required of you, I guess

91
00:15:06,560 --> 00:15:14,560
if you don't eat, you could stop eating, but you have to eat, that's about to eat and

92
00:15:14,560 --> 00:15:22,120
sleep, maybe there are chores to do in prison, I'm not sure, I had no prison as a horrible

93
00:15:22,120 --> 00:15:28,640
place, but the concept of a prison, I guess it's only horrible when we hear about it

94
00:15:28,640 --> 00:15:35,920
in places like America where it's for profit and not just for profit, but there's

95
00:15:35,920 --> 00:15:42,920
over incarceration, you know, another country's prison is quite peaceful and does its

96
00:15:42,920 --> 00:15:52,280
job in rehabilitating Canada as well as horrible prison system, apparently, but the point

97
00:15:52,280 --> 00:16:00,400
is you can be, you can be free in a prison theoretically, so our practice of meditation

98
00:16:00,400 --> 00:16:08,080
is, it's not about allowing us to do this or do that, it's about freeing us from stress

99
00:16:08,080 --> 00:16:13,840
and suffering and so it's such a torture to sit and meditate sometime, it's a torture

100
00:16:13,840 --> 00:16:24,680
because we're not free, and so the instinct is to get up and stop, to free yourself

101
00:16:24,680 --> 00:16:31,120
from the burden of having to meditate, it's keeping you from doing what you want, right,

102
00:16:31,120 --> 00:16:36,440
this freedom to, I meditate, I'm not free to do what I want, I'm not free to follow

103
00:16:36,440 --> 00:16:45,480
my greatness, I meditate to free ourselves from craving, right, to free ourselves from

104
00:16:45,480 --> 00:16:51,240
the need to do things, so it's an enlightened person, free, or free, and it's a enlightened

105
00:16:51,240 --> 00:16:59,520
person, is there freedom restricted in prison, right, and you restrict the freedom of

106
00:16:59,520 --> 00:17:08,120
an enlightened being, and you can bind their hands and feet, but duct tape over their

107
00:17:08,120 --> 00:17:15,400
mouth, you can blindfold them, but you can't take away their freedom because it's not

108
00:17:15,400 --> 00:17:25,960
freedom to, it's freedom from, and so meditation is free from, when we meditate and when

109
00:17:25,960 --> 00:17:31,280
we say to ourselves pain, we're freeing ourselves from the baggage that we carry around

110
00:17:31,280 --> 00:17:39,600
about pain, when we say seeing, we're freeing ourselves from the baggage of liking and

111
00:17:39,600 --> 00:17:52,880
disliking and all that leads to ambition and inclination, judgment, when we think, when

112
00:17:52,880 --> 00:17:57,560
we say thinking, thinking, we're reminding ourselves that it's only thinking, keeping

113
00:17:57,560 --> 00:18:10,280
ourselves free from views and beliefs and ideas and rationalization and so on, keeping

114
00:18:10,280 --> 00:18:24,680
our mind pure, cultivating a pure mind, and eventually the greatest freedom that comes is

115
00:18:24,680 --> 00:18:30,760
when we do this, then eventually we start to see, and we come free from the greatest

116
00:18:30,760 --> 00:18:41,280
bind of all in that ignorance, when we come free from ignorance and delusion, ignorance

117
00:18:41,280 --> 00:18:50,680
which prevents us from seeing right and wrong and good and bad and delusion which mistakes

118
00:18:50,680 --> 00:18:56,040
right for wrong and good for bad, and sets us on the wrong course and gets us mixed up,

119
00:18:56,040 --> 00:19:08,360
sometimes good, sometimes bad, and gets us caught more and more up in the web, and there

120
00:19:08,360 --> 00:19:30,360
you go, just some brief thoughts on freedom and that's the demo for tonight, do we have

121
00:19:30,360 --> 00:19:44,880
any questions, there's a couple on the site, but a formal meditation understandably is

122
00:19:44,880 --> 00:19:50,760
clear than when practicing in normal life, are there any thoughts to facilitate clarity

123
00:19:50,760 --> 00:19:57,400
and mindfulness in everyday life like developing agility and shifting, is there so many

124
00:19:57,400 --> 00:20:06,040
things happening quickly on the conformal practice, I don't know these questions, I get

125
00:20:06,040 --> 00:20:11,000
this kind of question a lot, what can you tell me that's going to make my practice easier,

126
00:20:11,000 --> 00:20:17,880
it's not easy, yes it's much more difficult in daily life, I don't have any, I mean I could

127
00:20:17,880 --> 00:20:23,400
give you tips if you want pointers but it's all the same, really the best thing to do is do

128
00:20:23,400 --> 00:20:31,000
a lot of intensive formal meditation so it becomes ingrained as a habit, it's like asking,

129
00:20:32,360 --> 00:20:37,800
it's like saying when you train as a boxer hitting the punching bag, it's quite easy,

130
00:20:39,000 --> 00:20:46,680
or when you train against a person holding up those pads, it's quite easy now,

131
00:20:46,680 --> 00:20:54,840
do you have any tips for me when I actually got into the boxing ring, I mean being in the boxing

132
00:20:54,840 --> 00:20:59,160
ring like being in real life is not like punching a punching bag or meditating formally,

133
00:21:01,240 --> 00:21:12,040
it's much more difficult, so you know the training is very very important and I guess another thing

134
00:21:12,040 --> 00:21:18,920
is training in life as well, training to see the difference, I didn't real life, it's more

135
00:21:18,920 --> 00:21:27,480
complicated and that's sort of what you're saying, and to train in that, it's a part of boxing

136
00:21:27,480 --> 00:21:35,320
is actually training in the ring, so I mean the only answer I have for you is work, work, work,

137
00:21:35,320 --> 00:21:47,000
train, train, train, practice, practice, how does one know for certain if I'm clinging to something

138
00:21:47,000 --> 00:21:51,480
real within myself or something created in the imagination such as a feeling or thought created

139
00:21:51,480 --> 00:22:00,920
externally, that's an interesting question, I think you have an interest, a strange understanding

140
00:22:00,920 --> 00:22:09,000
of the idea of real, what is real for you? It sounds like you have an idea that there exist real

141
00:22:09,000 --> 00:22:14,760
things, real entities, we don't have that idea in Buddhism, real is experience, if you experience

142
00:22:14,760 --> 00:22:20,760
something the experience is real, it doesn't matter whether you think of it as fake, like if you hear a

143
00:22:20,760 --> 00:22:27,400
noise, it doesn't matter whether it was really caused by your eardrum vibrating or whether it was

144
00:22:27,400 --> 00:22:32,760
created in the mind, it's still sound, if you see an image, it doesn't matter whether you have your eyes

145
00:22:32,760 --> 00:22:38,600
closed or whether you actually see it in the world around you, how would one go about having an

146
00:22:38,600 --> 00:22:43,640
insect like a tick removed from your skin in a benevolent manner? Well there are ways, I mean dealing

147
00:22:43,640 --> 00:22:52,040
with insects, ticks are that bad, ticks, a really interesting thing is wet wash cloth, put it on the

148
00:22:52,040 --> 00:23:01,960
tick and rotate it, rub the cloth in the wet cloth in circles, just water, and by rubbing it in

149
00:23:01,960 --> 00:23:11,720
circles, I think it was cloth, the tick eventually retracts itself, just keep rubbing in circles,

150
00:23:12,360 --> 00:23:19,640
apparently that works, there was a new tube video about that, we had ticks in when I was in

151
00:23:19,640 --> 00:23:24,600
Manitoba, but there are other ways, I mean the bigger problem might be like a tape worm,

152
00:23:24,600 --> 00:23:27,800
what would you do about a tape worm, that would be interesting, I'm not really sure.

153
00:23:27,800 --> 00:23:46,520
Okay, we got some local questions there as well, what's this?

154
00:23:49,720 --> 00:23:54,200
The second lifetalks aren't live streamed, they're not live streamed on YouTube, they are uploaded

155
00:23:54,200 --> 00:24:00,040
later, but there's an audio live stream if you want to read the audio, yes, so someone else, someone put

156
00:24:00,040 --> 00:24:16,280
that up, what's the cause of shyness, shyness, fear, fear which is anger based, I mean anger,

157
00:24:16,280 --> 00:24:23,720
it's a version based, you're averse to certain consequences, so people are shy because they're

158
00:24:23,720 --> 00:24:32,920
afraid of violence or repercussions of interaction and so on, shyness is sort of a fear.

159
00:24:34,120 --> 00:24:40,440
What's the cause is habit, having, you know, we develop habits for the strangest reasons,

160
00:24:40,440 --> 00:24:45,640
but we develop them over lifetimes, our habits sometimes just grow out of the smallest thing

161
00:24:45,640 --> 00:24:52,680
and they become obsessions and they can become very strong, the shyness can even become quite painful,

162
00:24:52,680 --> 00:24:54,200
but it's just a habit that builds up.

163
00:24:57,160 --> 00:25:01,560
Why wouldn't we focus on and appreciate the freedom to choose our religion, our tradition,

164
00:25:01,560 --> 00:25:06,760
our teacher, our practice, the freedom to accept precepts, be moral, make good choices, etc.

165
00:25:08,600 --> 00:25:14,280
Well, that's not freedom, it's not, I mean, it is freedom, but it's not freedom to,

166
00:25:14,280 --> 00:25:23,560
it's freedom from, you're being free from some external force, but no one can force you to,

167
00:25:25,080 --> 00:25:30,920
people can control you externally, so freedom from physical control is something I didn't really

168
00:25:30,920 --> 00:25:38,440
touch upon or kind of the idea of a prison, a prison is control, you're being oppressed,

169
00:25:38,440 --> 00:25:45,320
you're being controlled by something external, but ultimately it's insignificant,

170
00:25:46,360 --> 00:25:54,040
it's significant practically, but it's insignificant in an ultimate sense, it's not true

171
00:25:54,040 --> 00:26:09,640
imprisonment, but it's still, we're still talking about imprisonment by which means

172
00:26:09,640 --> 00:26:16,600
lack of freedom from oppression, so if you're not free to choose your religion, it's because

173
00:26:16,600 --> 00:26:22,920
what the freedom part is actually because, or if you're free to choose your religion, it's

174
00:26:22,920 --> 00:26:29,560
freedom from oppression, freedom from someone forcing you into a certain religion, but of course

175
00:26:29,560 --> 00:26:35,240
you can't be forced into a religion, we're coerced and tricked and so on, tricked into thinking,

176
00:26:36,600 --> 00:26:41,480
we have to do this and have to do that, someone holds a knife to your throat or a gun to your

177
00:26:41,480 --> 00:26:47,480
head and tells you and they're tricking you into thinking that somehow you should convert to their

178
00:26:47,480 --> 00:26:53,560
religion or die, you know, when in fact, if you understood the truth, you can just let them pull

179
00:26:53,560 --> 00:27:01,880
the trigger, or you wouldn't have any reason to fear death if you were enlightened.

180
00:27:05,400 --> 00:27:10,600
So when you're talking about choosing a religion, that's your desire and it's a different thing,

181
00:27:10,600 --> 00:27:19,400
I mean, when you have right intentions, it's not freedom, it's a, it's actually a imprisonment,

182
00:27:19,400 --> 00:27:24,920
you're imprisoned by your right intentions, you're imprisoned by wisdom, you're controlled by

183
00:27:24,920 --> 00:27:33,960
wisdom, a person who is wise can't make bad choices, they're unable to, they lack, they lack the

184
00:27:33,960 --> 00:27:42,680
freedom from the control of wisdom, right? So I think it's interesting that we talk about freedom

185
00:27:42,680 --> 00:27:48,360
too and the two part isn't really about freedom, it's even the focus is on our ambitions,

186
00:27:50,040 --> 00:27:54,120
you know, you're describing how you're free to do something, it's actually describing your freedom,

187
00:27:54,120 --> 00:28:05,240
it's round about, it's missing the point that freedom is always freedom from. And so with

188
00:28:05,240 --> 00:28:16,040
your examples, those are a good example, good things, but you know, too often we're excited

189
00:28:16,040 --> 00:28:26,040
and enjoyed by the fact that we can do this and can do that, the power to, the power today

190
00:28:26,040 --> 00:28:32,840
of instant gratification to get what we want when we want it. So intoxicating this power, it's not

191
00:28:32,840 --> 00:28:46,520
freedom, it's very dangerous power, that kind of quote unquote freedom. You know, the freedom part

192
00:28:46,520 --> 00:28:53,880
is great, fine, but the fact that we want to do all those things is very dangerous. So we miss

193
00:28:53,880 --> 00:29:00,120
the distinction, yes, it's great to be free from oppression and poverty and so on, but just

194
00:29:00,120 --> 00:29:04,840
because you're free from poverty by having lots of money doesn't mean you should go out and do

195
00:29:04,840 --> 00:29:10,360
whatever you want, whether it's that leap that we make, just because you're free from

196
00:29:11,320 --> 00:29:16,680
constraint means you should act upon the freedom and the compulsions.

197
00:29:16,680 --> 00:29:32,440
Isn't there a certain shyness called here? You know, that's not shyness, it's not how we use

198
00:29:32,440 --> 00:29:37,800
the word shy in English. Here you know, it's about interesting words, I mean, they really do mean

199
00:29:37,800 --> 00:29:42,920
fear and shame, but they can't be either fear or shame because both fear and shame are unwholesome.

200
00:29:42,920 --> 00:29:51,160
So there what stands in place, a good person appears to be ashamed of evil deeds and they

201
00:29:51,160 --> 00:29:55,960
appear to be afraid of the consequences of evil deeds, but it's not fear and it's not shame,

202
00:29:55,960 --> 00:30:01,960
it's just the disinclination to do bad deeds, both because of the nature of the deeds which is

203
00:30:01,960 --> 00:30:17,720
opened up and the cause or this are the result, which is here. I think most think of that as a

204
00:30:17,720 --> 00:30:24,360
blessing though, to make great choices naturally, yes, but it is a blessing, but it doesn't even

205
00:30:24,360 --> 00:30:34,120
do with freedom. It's your inclination, it's your power or the greatness of mind. You're not free

206
00:30:34,120 --> 00:30:41,320
to do all sorts of good things. Evil people can do them. You're lucky if anything. You're good,

207
00:30:41,320 --> 00:30:48,360
a good person. Isn't inclined. I think I'm actually quite sure what you're referring to, but

208
00:30:48,360 --> 00:30:59,560
anyway, there you go. There's the dhamma according to you to dhamma, you to dhamma.

209
00:30:59,560 --> 00:31:21,560
That's our session for tonight. Thank you all for coming. Have a good night.

