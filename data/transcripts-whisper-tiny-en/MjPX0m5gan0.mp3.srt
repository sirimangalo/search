1
00:00:00,000 --> 00:00:24,800
Good evening everyone, broadcasting live, February 13th.

2
00:00:30,000 --> 00:00:54,000
Okay, so I'm going to post this hangout.

3
00:00:54,000 --> 00:00:58,200
Actually, that's not more posted later.

4
00:00:58,200 --> 00:01:13,800
But joining the hangout, the idea was for people who have questions.

5
00:01:13,800 --> 00:01:22,700
We have a question you can join the hangout and ask it.

6
00:01:22,700 --> 00:01:36,100
I got someone sent me a note saying complaining about somehow being dismissive of Larry,

7
00:01:36,100 --> 00:01:44,000
I think last night, so I apologize if that was the case, but probably was in some

8
00:01:44,000 --> 00:01:55,800
way the case because I was kind of hoping to encourage not like a panel like we had set

9
00:01:55,800 --> 00:01:59,800
up in the past, but just people asking questions.

10
00:01:59,800 --> 00:02:06,500
So if you don't have a question, the ideas come on the hangout for a question, you

11
00:02:06,500 --> 00:02:13,500
don't have a question just listen at home.

12
00:02:13,500 --> 00:02:23,800
It doesn't have to be authoritarian or anything, but I think it's better to just keep that

13
00:02:23,800 --> 00:02:46,600
up for questions and I apologize if it seems somewhat cold or harsh or whatever.

14
00:02:46,600 --> 00:02:54,340
That's the thing when you teach, I'm going to put yourself out there and get you open yourself

15
00:02:54,340 --> 00:02:55,340
up for criticism.

16
00:02:55,340 --> 00:03:02,900
So reason why a lot of people I think are afraid of teaching, afraid of putting themselves

17
00:03:02,900 --> 00:03:12,700
out there in a thick skin, so you've got good feedback, positive feedback, but you'll

18
00:03:12,700 --> 00:03:19,740
get negative feedback and you'll make mistakes and people call you up on them if you hide

19
00:03:19,740 --> 00:03:28,220
away in your room, you don't have to face the criticism.

20
00:03:28,220 --> 00:03:46,180
So tonight we have a quote about gold, gold is of two kinds, unrefined and refined.

21
00:03:46,180 --> 00:03:54,220
So we come into the meditation unrefined, like unrefined gold with all sorts of defilements

22
00:03:54,220 --> 00:04:05,180
that has iron, copper, tin, lead and silver, these are five other types of metal that

23
00:04:05,180 --> 00:04:15,820
are mixed in with gold and keep it from being pure, pliant, keep it from being workable.

24
00:04:15,820 --> 00:04:26,580
You can't do much with ordinary gold or you need to purify it.

25
00:04:26,580 --> 00:04:45,260
In the same way the mind is defiled by five impurities and these are the five hindrances.

26
00:04:45,260 --> 00:04:58,780
When I first started meditating, there was this British meditator or meditator who,

27
00:04:58,780 --> 00:05:04,100
some years before that, I have made a cartoon of the five hindrances and so when I first

28
00:05:04,100 --> 00:05:09,020
got there they were handing out these pages with cartoons of the five hindrances and cartoons

29
00:05:09,020 --> 00:05:17,500
of the five faculties, five mental faculties being confident, effort, mindfulness, concentration

30
00:05:17,500 --> 00:05:24,460
and wisdom.

31
00:05:24,460 --> 00:05:31,980
And they're quite useful cartoons, quite useful to have those and have a visual reminder

32
00:05:31,980 --> 00:05:40,660
of them, I don't know if they still give them out or they still even have them, when

33
00:05:40,660 --> 00:05:43,100
I was working there we used to photocopy.

34
00:05:43,100 --> 00:05:55,220
The five hindrances are the first set of dumbas that we have to look at in the meditation

35
00:05:55,220 --> 00:05:56,220
practice.

36
00:05:56,220 --> 00:06:06,380
This is under that ketchup category of dumbas, which never had a successful explanation

37
00:06:06,380 --> 00:06:10,340
of how to translate dumba in that context.

38
00:06:10,340 --> 00:06:15,940
It seems like it means more than one thing, something that you couldn't really translate,

39
00:06:15,940 --> 00:06:25,780
you just have to understand it as a category of things.

40
00:06:25,780 --> 00:06:33,980
But the first category is the hindrances, which makes it seem like a real meditation teaching.

41
00:06:33,980 --> 00:06:42,060
The dumbas in Satyapatanas are referred to those things that you have to understand or

42
00:06:42,060 --> 00:06:51,980
you have to know about or that you encounter on your progress on your path.

43
00:06:51,980 --> 00:07:00,140
So as your mindful of reality, the objective reality, so this is the body and the mind

44
00:07:00,140 --> 00:07:09,180
ordinary reality that's not caught up in goodness or evil or not ethically charged, you're

45
00:07:09,180 --> 00:07:16,380
going to react, you're going to, when you experience through the senses, you're going

46
00:07:16,380 --> 00:07:23,420
to react to see things here, when you think things, you're going to react to your thoughts,

47
00:07:23,420 --> 00:07:32,380
when you experience physical sensations of pleasure and pain, you're going to react.

48
00:07:32,380 --> 00:07:33,900
And this is the five hindrances.

49
00:07:33,900 --> 00:07:40,300
So as your practice, the five hindrances will come up mainly as reaction, in reaction

50
00:07:40,300 --> 00:07:41,300
to experiences.

51
00:07:41,300 --> 00:07:48,660
Your hindrances, because they get in the way of your, your progress, you'll like certain

52
00:07:48,660 --> 00:07:55,060
things, dislike certain things, but I've gotten used to using simplified versions of these.

53
00:07:55,060 --> 00:08:02,100
The five hindrances, the technical names of them are gamma chanda meaning sense desire.

54
00:08:02,100 --> 00:08:12,580
So he's got them here, sense desire, ill will, and you have sloth and torpor, sloth and

55
00:08:12,580 --> 00:08:16,940
torpor are two words that we just don't use anymore, so it's a shame that we're still

56
00:08:16,940 --> 00:08:23,420
using sloth and torpor, lazy methods, because there's two words, basically laziness,

57
00:08:23,420 --> 00:08:35,180
inertia, the muddled, unwieldiness of the mind, and then restlessness and worry, that's

58
00:08:35,180 --> 00:08:41,860
a good definition, restlessness and worry are put together, but there are actually two different

59
00:08:41,860 --> 00:08:42,860
things.

60
00:08:42,860 --> 00:08:47,620
Restlessness is when the mind is not focused, when you're thinking lots of different things

61
00:08:47,620 --> 00:08:58,180
and worry is a little bit different, worry is a sort of a fear or anxiety, concerned

62
00:08:58,180 --> 00:09:03,860
about, I mean the technical descriptions are concerned about things that you've done in the

63
00:09:03,860 --> 00:09:12,980
past, bad deeds that you've done, worried about consequences, that kind of thing.

64
00:09:12,980 --> 00:09:18,780
And we cheeky challenge means doubt.

65
00:09:18,780 --> 00:09:26,500
The five hindrances are one of the first things that we teach meditators, they're always

66
00:09:26,500 --> 00:09:32,860
going to be important, they're what you focus on or they're a big part of your focus,

67
00:09:32,860 --> 00:09:37,660
and they always have to be in your mind from beginning to end of the course.

68
00:09:37,660 --> 00:09:43,060
If you don't understand the five hindrances, if you don't get them, they'll destroy it in

69
00:09:43,060 --> 00:09:44,060
the practice.

70
00:09:44,060 --> 00:09:51,300
As it gets harder, as it gets more intensive in a meditation course, not practicing in

71
00:09:51,300 --> 00:09:56,980
daily life, but of course they come into play there, but during a meditation course they

72
00:09:56,980 --> 00:09:59,300
really make a break your practice.

73
00:09:59,300 --> 00:10:04,460
As soon as you let them get to you and you forget to be mindful of them, or if you don't

74
00:10:04,460 --> 00:10:09,180
work hard to learn how to be mindful of them and how to overcome them, if you don't become

75
00:10:09,180 --> 00:10:17,180
comfortable dealing with them, then they'll start to weep when they cough, weasel their

76
00:10:17,180 --> 00:10:32,260
way in their into your mind and they'll start hitting away at your confidence, hitting

77
00:10:32,260 --> 00:10:37,820
away at your resolve, hitting away at your effort, and they'll drag you down until

78
00:10:37,820 --> 00:10:48,180
make you well make you discouraged and they can cause you to fail.

79
00:10:48,180 --> 00:10:53,580
Which should be really encouraging because that's all that is going to make you fail.

80
00:10:53,580 --> 00:10:57,900
We say that it gets tough or it is difficult to meditate or there are times where you feel

81
00:10:57,900 --> 00:11:06,540
like you can't meditate, you're just overwhelmed and you don't know what to do, but it's

82
00:11:06,540 --> 00:11:13,540
not the situation that makes it impossible, there's no situation in the course that makes

83
00:11:13,540 --> 00:11:20,780
it impossible to meditate, it's only your own mind, your own emotions, your own reactions

84
00:11:20,780 --> 00:11:23,460
to things.

85
00:11:23,460 --> 00:11:31,500
So it's actually quite easy to figure out the solution to the problems that come up, not

86
00:11:31,500 --> 00:11:36,500
only actually in meditation, but in our lives, it's quite encouraging for figuring out

87
00:11:36,500 --> 00:11:44,660
a succeeded life, not easy, but at least you have a clear picture of what you need to

88
00:11:44,660 --> 00:11:50,980
do and that's to just deal with the five hindrance, there's really the only thing standing

89
00:11:50,980 --> 00:11:59,060
between us and success in anything because they destroy the mind, they destroy the faculties

90
00:11:59,060 --> 00:12:00,060
of the mind.

91
00:12:00,060 --> 00:12:06,300
So they destroy your confidence, they destroy your effort, they destroy your mindfulness,

92
00:12:06,300 --> 00:12:12,940
your concentration and you risk them, they distract you from these things, they keep

93
00:12:12,940 --> 00:12:19,660
you from developing them, they keep you from, they keep them from these faculties from

94
00:12:19,660 --> 00:12:30,820
growing strong, from working together to free the mic.

95
00:12:30,820 --> 00:12:40,620
So for that reason, it's a very important dumbness, meditation, in our tradition we simplify

96
00:12:40,620 --> 00:12:45,580
them liking, disliking, drowsiness, distraction, doubt, I'm the one who came up with those

97
00:12:45,580 --> 00:12:47,980
words, I don't know that anyone else is using those.

98
00:12:47,980 --> 00:12:54,580
Why I use them is because it's liking and then for these liking, disliking, drowsiness,

99
00:12:54,580 --> 00:12:59,540
distraction, doubt, it's easy to say, it's easy to remember, but it's very similar to

100
00:12:59,540 --> 00:13:07,120
how my teacher teaches them and I chop, nah, mai chop, nah, warm, warm, so I chop, mai

101
00:13:07,120 --> 00:13:12,780
chop, warm, warm, so it gives them very simple words, even you don't know what those mean,

102
00:13:12,780 --> 00:13:18,460
you can hear that they're very easy to say, chop, mai chop, mai means no, no, chop means

103
00:13:18,460 --> 00:13:27,540
like chop, mai chop, warm, it's just tired or drowsy, full, full means, full sand, full

104
00:13:27,540 --> 00:13:37,900
sand is distracted or flustered, and song size is doubt.

105
00:13:37,900 --> 00:13:41,780
We need these simple words because we have to, we're on the front lines, we need to

106
00:13:41,780 --> 00:13:48,420
be able to use these as weapon, we need to have a weapon to combat these, so it's like

107
00:13:48,420 --> 00:13:56,140
knowing your enemy, you have to know that the way of getting to them, if you have these

108
00:13:56,140 --> 00:14:01,780
words like central desire, ill will and so on, sloth and lacing and that doesn't really

109
00:14:01,780 --> 00:14:10,700
work, practically speaking, it's important that we memorize these.

110
00:14:10,700 --> 00:14:18,340
And when, when people come to practice in Chantong, Ajantong will remind them of the

111
00:14:18,340 --> 00:14:22,260
starts with the four foundations of mindfulness, you have to memorize the four sati

112
00:14:22,260 --> 00:14:28,660
botana, kai oed, and then under dhamma, you have to memorize the five hindrances, chop,

113
00:14:28,660 --> 00:14:35,500
mai chop and you'll have your repeat after him, chop, chop, mai chop, mai chop, warm,

114
00:14:35,500 --> 00:14:44,820
warm, full, folks, all sides, all sides. And then you have to say it yourself, they're

115
00:14:44,820 --> 00:14:53,500
that important there. As you can see, the Buddha says, when you give up these five to

116
00:14:53,500 --> 00:15:07,980
five debasements, it's five defalment, one can then direct the mind to the realization by psychic

117
00:15:07,980 --> 00:15:13,180
knowledge of whatever can be realized by psychic knowledge, you can see it directly, whatever

118
00:15:13,180 --> 00:15:18,780
its range might be. So you're talking about psychic powers, magical powers, it's possible

119
00:15:18,780 --> 00:15:25,780
to gain these when your mind is free from hindrance, but it's also possible. The sixth

120
00:15:25,780 --> 00:15:35,980
of the Abhigna of the psychic powers is the destruction of defalment. So the highest

121
00:15:35,980 --> 00:15:44,660
psychic powers, the power of wisdom, of overcoming the defalment. So that's the dhamma

122
00:15:44,660 --> 00:15:53,340
for tonight. Now I'm going to post link to the hangout, but it's only really if you want

123
00:15:53,340 --> 00:16:08,180
to come on and ask questions. So if you have a question, come on the hangout, don't be shy.

124
00:16:08,180 --> 00:16:19,420
Come and ask, this is our new way of doing things. You've got to actually be brave enough

125
00:16:19,420 --> 00:16:24,460
to come on and ask your question. You also need to make in that case, which I guess is

126
00:16:24,460 --> 00:16:47,860
a bit of a thing some people might not have a mic. This person has posted this several

127
00:16:47,860 --> 00:16:56,820
times, I guess I never actually answered it. Someone's asking about whether they should

128
00:16:56,820 --> 00:17:02,500
learn more from normal how to videos. You're 17 years old, don't worry about the children's

129
00:17:02,500 --> 00:17:09,420
videos. I mean adults have liked them, didn't they answer this already? Adults have found

130
00:17:09,420 --> 00:17:16,620
those videos useful as well, and they're kind of useful because they simplify everything.

131
00:17:16,620 --> 00:17:21,620
But I recommend my booklet. If you haven't read the booklet on how to meditate, that's

132
00:17:21,620 --> 00:17:33,220
where you should start. Most of those kids videos are just about non-inside things.

133
00:17:33,220 --> 00:17:49,140
So no questions. No questions that I'm going to head out.

134
00:17:49,140 --> 00:18:10,300
Alright, have a good night everyone. See you all tomorrow.

