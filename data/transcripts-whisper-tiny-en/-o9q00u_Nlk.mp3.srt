1
00:00:00,000 --> 00:00:11,680
So I'm good evening, everyone.

2
00:00:11,680 --> 00:00:17,520
We're broadcasting on the internet as well, but I'm just going to let the internet go

3
00:00:17,520 --> 00:00:18,520
if it works.

4
00:00:18,520 --> 00:00:22,520
It works, if it doesn't, it doesn't.

5
00:00:22,520 --> 00:00:43,000
The stock is for you who have come here to meditate with us.

6
00:00:43,000 --> 00:00:53,720
Another adieu should just stay practicing, stay focused.

7
00:00:53,720 --> 00:01:03,280
He's finishing his course soon, so better to stay focused.

8
00:01:03,280 --> 00:01:11,360
So I thought something general and useful to talk about is, well, the title of the talk

9
00:01:11,360 --> 00:01:22,120
is going to be a meditator's life, living as a meditator.

10
00:01:22,120 --> 00:01:26,920
In the meditation center, it is quite simple.

11
00:01:26,920 --> 00:01:41,840
It may be risk over simplifying it, where we think the only thing is to repeat words,

12
00:01:41,840 --> 00:01:49,320
mantras in our mind, it's just a technique and that's how that's all we do.

13
00:01:49,320 --> 00:02:12,760
And even in a meditation center, life is not singular, it's still as its complexity.

14
00:02:12,760 --> 00:02:19,280
So you can take this talk as useful for us here in the center, but also useful in general.

15
00:02:19,280 --> 00:02:23,920
We always have questions about how to live our lives and incorporate meditation into our

16
00:02:23,920 --> 00:02:24,920
lives.

17
00:02:24,920 --> 00:02:27,560
So it's useful in that way as well.

18
00:02:27,560 --> 00:02:41,040
It's mostly hardcore for serious meditators to think about those supportive factors in

19
00:02:41,040 --> 00:02:48,320
the way we live our life that are going to help us not just progress in meditation, but

20
00:02:48,320 --> 00:02:58,200
to live a meditative life, a life that is by nature, pure, mindful, alert, aware, awake.

21
00:02:58,200 --> 00:03:01,880
It's a good way to live.

22
00:03:01,880 --> 00:03:09,800
That's the idea.

23
00:03:09,800 --> 00:03:15,240
So the first thing, of course, is to actually meditate and not just meditate because that

24
00:03:15,240 --> 00:03:17,560
word, it's a troubling word.

25
00:03:17,560 --> 00:03:19,560
It's a troublesome word.

26
00:03:19,560 --> 00:03:28,440
I have to meet people who say, oh, yeah, I meditate and of course that doesn't mean anything.

27
00:03:28,440 --> 00:03:38,480
That doesn't mean very much from, I mean, in terms of explaining the sort of, I mean,

28
00:03:38,480 --> 00:03:43,400
it does tell me that you have interest in some sort of mental development, but it's such

29
00:03:43,400 --> 00:03:45,720
a broad topic, right?

30
00:03:45,720 --> 00:03:50,160
So here we don't mean the word the Buddha used is seeing.

31
00:03:50,160 --> 00:03:53,880
But there's not much.

32
00:03:53,880 --> 00:03:56,640
The Buddha didn't use the word of meditation very much.

33
00:03:56,640 --> 00:03:57,840
There is a word for it.

34
00:03:57,840 --> 00:04:08,440
There's a couple of words that relate to the word meditation, but he used the word

35
00:04:08,440 --> 00:04:09,440
seeing.

36
00:04:09,440 --> 00:04:19,080
No, to be very particular about what we're trying to do, we're trying to see.

37
00:04:19,080 --> 00:04:26,560
So the first most important thing about our lives is to focus our attention on seeing.

38
00:04:26,560 --> 00:04:27,880
Seeing what?

39
00:04:27,880 --> 00:04:37,720
I mean, simply we're seeing reality, seeing the way things are.

40
00:04:37,720 --> 00:04:43,360
Seeing clearly, this is why we use the word vipasana, it means seeing clearly or seeing

41
00:04:43,360 --> 00:04:50,440
in a penetrative sort of way, penetrating means seeing through all the delusions and all

42
00:04:50,440 --> 00:05:05,280
the concepts, the see reality, to see things as they are.

43
00:05:05,280 --> 00:05:17,040
And so when you think about your meditation practice and are concerned about whether you're

44
00:05:17,040 --> 00:05:23,760
doing something valuable or useful and why are you doing what you're doing?

45
00:05:23,760 --> 00:05:25,240
Are you doing it?

46
00:05:25,240 --> 00:05:29,040
How do you know if you're doing it properly and so on?

47
00:05:29,040 --> 00:05:31,440
The focus should be on seeing.

48
00:05:31,440 --> 00:05:37,280
I mean, it really, I think it sounds more mysterious and meditation in general tends to

49
00:05:37,280 --> 00:05:45,400
sound more mystical and enigmatic than it actually is.

50
00:05:45,400 --> 00:05:50,080
It really just means to see what's going on in your body and your mind, what's going

51
00:05:50,080 --> 00:05:54,800
on with you right now.

52
00:05:54,800 --> 00:05:57,560
What's your state of mind, what's your state of body?

53
00:05:57,560 --> 00:06:04,680
You know, the sati patana is really useful framework, the four sati patana, the body,

54
00:06:04,680 --> 00:06:11,800
what is the body doing, learning about the body, even just isolating the body, you know?

55
00:06:11,800 --> 00:06:19,560
When we're mindful of the body, we learn so much, we learn to see how the way we move

56
00:06:19,560 --> 00:06:33,920
and creates stress and suffering, you know, our unmindful movements and how being mindful

57
00:06:33,920 --> 00:06:42,160
helps us to understand how the body works, helps us to be very present and it helps

58
00:06:42,160 --> 00:06:49,360
us to relate to the mind when we see how the mind affects the body, the tension that arises

59
00:06:49,360 --> 00:06:54,200
in the body because of the mind, when the mind is stressed and there's tension in the

60
00:06:54,200 --> 00:07:01,040
body, so meditating on the body, it helps you see the mind.

61
00:07:01,040 --> 00:07:07,400
It also helps you relax, it's very useful at helping you focus and relax because the

62
00:07:07,400 --> 00:07:09,400
body is neutral.

63
00:07:09,400 --> 00:07:14,640
When you watch your stomach rising and falling, it's quite powerful in that it makes your

64
00:07:14,640 --> 00:07:20,400
mind quite neutral because it's an object that is quite neutral.

65
00:07:20,400 --> 00:07:30,240
When you're walking, it seems dull and it can be felt to be quite boring and uninteresting,

66
00:07:30,240 --> 00:07:38,640
but on the other side it's quite peaceful and neutral and clear, something that we can

67
00:07:38,640 --> 00:07:46,280
easily relate to and cultivate, very powerful states of mind.

68
00:07:46,280 --> 00:07:50,400
Our feelings, being mindful of our feelings, learning about our feelings, learning about

69
00:07:50,400 --> 00:07:57,720
how feelings are not under our control, we can't always be happy, we can't escape pain

70
00:07:57,720 --> 00:08:08,480
that trying to escape pain is quite bothersome and unfruitful and unsuccessful.

71
00:08:08,480 --> 00:08:13,440
Also seeing these sorts of things, seeing about the mind, seeing how the mind works,

72
00:08:13,440 --> 00:08:19,640
or seeing how thinking is not quite what we thought it was.

73
00:08:19,640 --> 00:08:25,440
We tend to think, especially in the West, we have a culture of overthinking, what do we

74
00:08:25,440 --> 00:08:32,600
think, or sit and talk, or sit in debate and think and think, thinking is considered to

75
00:08:32,600 --> 00:08:39,600
be a virtue, a wonderful pastime, but when you investigate, when you examine you say,

76
00:08:39,600 --> 00:08:43,320
oh, it can actually be quite stressful as well.

77
00:08:43,320 --> 00:08:52,080
Thinking a lot is actually, it's not very weildy, it's quite chaotic and it's what

78
00:08:52,080 --> 00:08:59,320
leads very quickly to emotional difficulty because if it's going too quickly, you can't

79
00:08:59,320 --> 00:09:06,600
keep track of, oh, I'm slipping into a bad habit, I'm feeding into this bad habit, it's

80
00:09:06,600 --> 00:09:16,760
very hard to see because it's so quick and chaotic, you can see these habits and how difficult

81
00:09:16,760 --> 00:09:21,920
they are to overcome because of how complicated it all is and the way the mind works,

82
00:09:21,920 --> 00:09:30,680
it's running so quickly that it's very hard to work it out. And of course, the emotions

83
00:09:30,680 --> 00:09:40,320
being mindful of our likes and dislikes is very important, seeing how our likes and dislikes

84
00:09:40,320 --> 00:09:47,480
cause us stress and tension and suffering, changing it from the thing I don't like is causing

85
00:09:47,480 --> 00:09:53,200
me suffering to, hey, the fact that I don't like it is what's causing me suffering.

86
00:09:53,200 --> 00:09:59,360
And from not getting what I want is causing me suffering to the one thing, yes, I didn't

87
00:09:59,360 --> 00:10:04,800
want that thing that I don't have, it would be much more tenable than trying to always

88
00:10:04,800 --> 00:10:17,080
get what I want. So seeing this is our, it's actually quite simple. So don't skip over

89
00:10:17,080 --> 00:10:21,320
it and think, hey, where's the good stuff? Okay, all this stuff I see, but when am I going

90
00:10:21,320 --> 00:10:30,840
to get to see, where's the fireworks? It's not about seeing fireworks. It's about the

91
00:10:30,840 --> 00:10:39,400
nitty-gritty details of our mind, of our being. That's the first part of being a meditator

92
00:10:39,400 --> 00:10:53,680
that I think quite clear. The second part is in regards to our life, our food, our shelter,

93
00:10:53,680 --> 00:11:02,760
our clothing, they call the requisites in Buddhism, the use of medicine, the things we need

94
00:11:02,760 --> 00:11:11,920
to live. How you relate to your clothes, you know, as meditators while we're careful not

95
00:11:11,920 --> 00:11:23,480
to bring beautiful clothes or enticing attractive clothes that show off the body or are meant

96
00:11:23,480 --> 00:11:31,680
to create desires and others and so on, to make us look good, make us look cool, make

97
00:11:31,680 --> 00:11:40,080
us look handsome, beautiful. Work careful not to get attached to our clothes like, oh, this

98
00:11:40,080 --> 00:11:46,040
is so beautiful, this robe I'm wearing, how sexy, you know, how, how sorry, that's not

99
00:11:46,040 --> 00:11:59,960
the right word, how beautiful it is, how I like wearing this robe. And this is why we

100
00:11:59,960 --> 00:12:10,040
wear these robes that aren't in any way attractive. But as meditators, look at our meditators,

101
00:12:10,040 --> 00:12:13,760
they're not wearing attractive clothes, but it's important, you know, these kinds of things

102
00:12:13,760 --> 00:12:19,080
because that it's not something that you have to spend a lot of time thinking about, but

103
00:12:19,080 --> 00:12:24,280
it's certainly something as a meditator we definitely have to be aware of. Close or something

104
00:12:24,280 --> 00:12:31,000
we wear all day, they're one of the few things that we carry around with us, right?

105
00:12:31,000 --> 00:12:37,160
What do you carry around with you more than your clothes? Nothing. So something to think

106
00:12:37,160 --> 00:12:44,840
about, this thing that I'm always, that's always draped over my body. It's a part of our

107
00:12:44,840 --> 00:12:51,880
life, food. Food is important, you know, being here in the center, you can attest, right?

108
00:12:51,880 --> 00:12:55,400
Yeah, but go to sleep at night and think about what you're going to have in the morning

109
00:12:55,400 --> 00:13:01,760
for breakfast. What am I going to eat for lunch? Or you get lunch and it's not really

110
00:13:01,760 --> 00:13:10,360
what I wanted. Eventually as meditators, you're doing intensive practice, you get to

111
00:13:10,360 --> 00:13:19,880
the point where, oh boy, I have to go up and eat food again. I have a troublesome.

112
00:13:19,880 --> 00:13:28,000
But food can be so many things, right? It can be a strong addiction. The cravings for food,

113
00:13:28,000 --> 00:13:34,560
you must all have cravings, no? When we go through the course, the things we crave, cheeseburgers,

114
00:13:34,560 --> 00:13:49,320
pizza, cheesecake, or what do you call the noodle? Vietnamese noodle soup. They don't get

115
00:13:49,320 --> 00:14:01,440
any of that here, I don't suppose. Spring rolls. Spring rolls now in Vietnam. Food can

116
00:14:01,440 --> 00:14:11,080
be such a strong attachment. It can be an attachment when you don't like it, when it's dull

117
00:14:11,080 --> 00:14:23,840
and boring and inspiring and undelightful. Shelter, our residents, our bedding is in the

118
00:14:23,840 --> 00:14:30,120
part of that. We try to sleep on the floor. When I did my first course, it was like a

119
00:14:30,120 --> 00:14:35,040
thinnest mat and you could get it. On the hard wooden floor, I found out later that other

120
00:14:35,040 --> 00:14:43,080
meditators requested extras that I didn't. I was really hardcore my first time. This was

121
00:14:43,080 --> 00:14:48,840
like, this was why I went to Asia. I mean, I'm not the only one. I've met other people

122
00:14:48,840 --> 00:14:56,040
that were like that. It's romantic, you know, just going to give up everything, go for

123
00:14:56,040 --> 00:15:04,680
it. It can be helpful. But yes, trying to not torture yourself by sleeping on the wooden

124
00:15:04,680 --> 00:15:13,360
floor, but to be careful, because it will affect your practice, how you use these things.

125
00:15:13,360 --> 00:15:18,120
So the Buddha had us reflect on the things that we need to use medicine as well. Medicine

126
00:15:18,120 --> 00:15:25,120
is important. I mean, hopefully our meditators are not taking medicine, painkillers,

127
00:15:25,120 --> 00:15:30,720
you know, try not to take things like painkillers and so on. When you're here in the

128
00:15:30,720 --> 00:15:37,920
intensive course and coffee, you don't drink coffee to stay awake or a caffeinated tea thinking

129
00:15:37,920 --> 00:15:51,920
this will give me energy. Not really helpful. Medicine should be for emergency.

130
00:15:51,920 --> 00:16:03,120
The next part is, oh, I missed one. The second one is actually guarding. So the next

131
00:16:03,120 --> 00:16:14,240
one we'll talk about is guarding. Guarding the senses as we live our lives, we come in contact

132
00:16:14,240 --> 00:16:18,920
with sensory perception, right? When you're not meditating with your eyes close, you're

133
00:16:18,920 --> 00:16:24,440
going to see things, see beautiful things, ugly things. Maybe you look at this house

134
00:16:24,440 --> 00:16:32,360
and you think, boy, it's such a palace. So wonderful to me. No, probably not. You think

135
00:16:32,360 --> 00:16:41,320
to yourself, maybe, oh, it's so drab in here living in this musty basement. Same walls,

136
00:16:41,320 --> 00:16:48,560
same ceiling, having to deal with these sensory perceptions. So good ones, bad ones, they

137
00:16:48,560 --> 00:16:56,960
can wear on us if we react to them. When you've lived here in this century, a prison

138
00:16:56,960 --> 00:17:03,640
would, through the experience, prison, the idea of prison becomes much less of a scary

139
00:17:03,640 --> 00:17:09,640
thing because, I mean, besides many of the scary things that go on in prisons, but living

140
00:17:09,640 --> 00:17:16,520
in a jail cell, you start to realize that it's really all about your reactions. It's not

141
00:17:16,520 --> 00:17:23,240
about your experience. There's nothing less wonderful about living in a jail cell. But

142
00:17:23,240 --> 00:17:33,640
our sensory perceptions, our senses can and do cause us lots of stress and suffering.

143
00:17:33,640 --> 00:17:39,840
Guarding the senses, let's seeing, just be seeing, it's a very core Buddhist teaching.

144
00:17:39,840 --> 00:17:45,640
Let's seeing, just be seeing, hearing, just hearing, smelling, just has smelling, tasting,

145
00:17:45,640 --> 00:17:53,040
feeling, thinking. The problem is in what you experience. It's not that you have to live

146
00:17:53,040 --> 00:18:03,400
in this drab house with this drab room with the smells and the sounds and so on. It's really

147
00:18:03,400 --> 00:18:11,760
neither here nor there. You can be completely at peace and happy with all of that, or you

148
00:18:11,760 --> 00:18:21,000
can be miserable. You can be miserable living in a mansion. So that's the next one. The

149
00:18:21,000 --> 00:18:27,560
next, after that, the next part is what we, so we have what we have to use. Next, we have

150
00:18:27,560 --> 00:18:34,000
what we have to endure. Some things, it's not about using them, but it's about coming

151
00:18:34,000 --> 00:18:41,200
in contact with them and just having to bear with them. So loud noise. Apparently, that's

152
00:18:41,200 --> 00:18:47,040
an issue here in the basement. I think it's probably the propane water heater. It's

153
00:18:47,040 --> 00:18:54,480
apparently noisy. So having to deal with that, having to bear with that, can't be difficult.

154
00:18:54,480 --> 00:19:02,720
It doesn't really matter because no matter where you go, there's going to be things

155
00:19:02,720 --> 00:19:08,880
you have to endure. But worse than that, you have to endure pain. Sometimes you might

156
00:19:08,880 --> 00:19:20,760
be in your body might be in a bad condition. You might have a headache. Having to deal

157
00:19:20,760 --> 00:19:25,440
with insects, having to deal with mosquitoes. Here, I don't think there's so many in

158
00:19:25,440 --> 00:19:34,240
the house, but there may very well be one. We don't kill. So you may be just letting

159
00:19:34,240 --> 00:19:44,200
the mosquito have its dinner. You can be the mosquito's dinner and just let go and learn

160
00:19:44,200 --> 00:19:54,960
to endure and your heat and your cold. Cold and heat can be quite vexing. They can cause

161
00:19:54,960 --> 00:20:01,760
us great stress, but they're really just heat and cold. Unless you're going to get hypothermia

162
00:20:01,760 --> 00:20:09,840
or whatever the heat stroke for the most part, they're just something that we react to.

163
00:20:09,840 --> 00:20:15,840
You can actually be quite cold without any adverse effects if you're mindful, if you're

164
00:20:15,840 --> 00:20:23,280
not mindful. Same with heat. You can be very quite hot. I've been living with him in Winnipeg

165
00:20:23,280 --> 00:20:28,400
for a while. It was negative 40 below. One day, we thought we'd take a walk in the middle

166
00:20:28,400 --> 00:20:35,520
of winter. Me and this novice I was living with him. We walked quite a ways, but in our

167
00:20:35,520 --> 00:20:43,600
monks' robes, it wasn't, it was difficult. Then I've lived in Sri Lanka, which is very hot

168
00:20:43,600 --> 00:20:48,440
and Thailand in the summer. It could be very hot. Even the time monks were complaining,

169
00:20:48,440 --> 00:21:00,520
which was a sign. But there's just temperature. So enduring things, very important part

170
00:21:00,520 --> 00:21:08,520
of our practice. Pain, heat, cold, hunger thirst. If it won't kill you, it's not really

171
00:21:08,520 --> 00:21:11,760
something you have to worry about. Even if it will kill you, it's actually not something

172
00:21:11,760 --> 00:21:22,680
you have to worry about, but hopefully it doesn't get to that point. So things we have

173
00:21:22,680 --> 00:21:31,800
to endure, things we have to avoid. Here in the centre, hopefully there's not that much

174
00:21:31,800 --> 00:21:37,040
you have to avoid, but if there were poisonous snakes or poisonous spiders, we don't have

175
00:21:37,040 --> 00:21:45,440
any in Canada, really. Nothing that you'd ever find here and ever ever. But if we had

176
00:21:45,440 --> 00:21:56,320
them, well, you'd have to avoid a poisonous snake. Avoid charging elephants. There's an

177
00:21:56,320 --> 00:22:05,960
elephant charging well. Don't just stand there. Bears, we have bears in Canada. I think

178
00:22:05,960 --> 00:22:13,600
you're supposed to lie down and play dead. But yes, avoiding dangers, that sort of thing.

179
00:22:13,600 --> 00:22:18,840
In life, there are things it's worth mentioning because you don't just endure having

180
00:22:18,840 --> 00:22:29,720
a wild animal attack you. It pays to avoid calamity. There's some things that you should

181
00:22:29,720 --> 00:22:36,240
avoid because they're unhelpful for meditation. They're problematic, like going to parties,

182
00:22:36,240 --> 00:22:44,440
bars. You should avoid places where they sell alcohol. It's a good rule of life as a meditator.

183
00:22:44,440 --> 00:22:49,760
It doesn't mean you have to. It doesn't mean you ban them from your life, but don't

184
00:22:49,760 --> 00:22:55,400
go to bars. It's really not much reason. It'd be hard to find a reason for a meditator

185
00:22:55,400 --> 00:23:05,160
to go to a bar. I've actually, as a monk, been in the bar once, maybe. That was a extenuating

186
00:23:05,160 --> 00:23:10,880
circuit. It was a family thing that was just probably a mistake in the end, but it was

187
00:23:10,880 --> 00:23:19,480
for my grandmother. But yeah, avoid these things. Avoid the Buddha talking. This is a talking

188
00:23:19,480 --> 00:23:25,920
you have to monk, so he said, avoid prostitutes. It doesn't mean avoid buying, paying them

189
00:23:25,920 --> 00:23:32,040
for sex. It means avoid hanging out with them. Probably not good company, not that there's

190
00:23:32,040 --> 00:23:37,360
any, not that it's someone we should shun. I've actually, I taught a prostitute in Thailand

191
00:23:37,360 --> 00:23:46,080
how to meditate once. Very interesting stories. Very interesting woman. But hanging out

192
00:23:46,080 --> 00:23:56,200
with unmarried women, that sort of thing for monks. So, generalizing to, when you're engaging

193
00:23:56,200 --> 00:24:01,800
in meditation and you're trying to be celibate, those beings that you find sexually

194
00:24:01,800 --> 00:24:10,120
aromatically attractive, probably better to keep some distance, not that there's anything

195
00:24:10,120 --> 00:24:16,040
intrinsically wrong. It's just your reactions, but because we're not enlightened, we

196
00:24:16,040 --> 00:24:24,920
have to become to the practice with quite strong habits of reacting. It's in our best interest

197
00:24:24,920 --> 00:24:35,440
to take it slowly and retreat a little bit. Our goal is to face things, but let us not

198
00:24:35,440 --> 00:24:41,120
try to face everything all at once. Avoiding, and there's other reasons for avoiding various

199
00:24:41,120 --> 00:24:46,400
things like that. Because of how it, the sorts of people you're going to meet in a bar and

200
00:24:46,400 --> 00:24:56,200
the reputation you get, I mean just practical reasons for avoiding various things. Yeah,

201
00:24:56,200 --> 00:25:07,200
so avoiding the other thing, you know, avoiding. So, what do we have? We have seeing, and

202
00:25:07,200 --> 00:25:13,920
put them in the right order, guarding, using, enduring, avoiding, avoiding. And then the

203
00:25:13,920 --> 00:25:22,160
next one is abandoning. Yes, there are things and aspects, and this is really getting

204
00:25:22,160 --> 00:25:28,240
into getting back to the meditation. What we're trying to abandon, we should abandon

205
00:25:28,240 --> 00:25:36,200
thoughts of ill will, we should abandon thoughts of craving, addiction, we should abandon

206
00:25:36,200 --> 00:25:49,720
thoughts of arrogance, conceit, harmful, unethical thoughts, we should abandon them. And

207
00:25:49,720 --> 00:26:01,720
more deeply, we should come to abandon everything really. Our attachment to things, it's

208
00:26:01,720 --> 00:26:09,080
not a part of who we are that is of any use. There's no attachment to anything that is of any

209
00:26:09,080 --> 00:26:19,280
good to you. Sambhedamma nalangabhini waysiah. All them, as indeed, are not worth clinging

210
00:26:19,280 --> 00:26:31,600
to. So, yes, abandoning really is what we're trying

211
00:26:31,600 --> 00:26:38,760
to do. Our whole goal is to abandon. That's maybe not in general like that. It's more

212
00:26:38,760 --> 00:26:45,760
to let go, right? We use this word, let go. Letting go is misleading because it implies

213
00:26:45,760 --> 00:26:53,400
an activity. Like you should just sit there and, oh, let go of this. Oh, you know, actively

214
00:26:53,400 --> 00:27:00,600
letting go. It's not really how it works. It is about letting go, but letting go is a

215
00:27:00,600 --> 00:27:11,880
product of a mind that sees clearly, sees clearly that the thing one is clinging to is

216
00:27:11,880 --> 00:27:16,480
not worth clinging to. That's how letting go comes about. It doesn't come about by doing

217
00:27:16,480 --> 00:27:25,760
this. It doesn't come about by some mental opening of your fist, comes about by a vision,

218
00:27:25,760 --> 00:27:37,120
by knowledge. It's much closer. You can say to letting be rather than letting go. It's about

219
00:27:37,120 --> 00:27:42,640
letting be in the sense of not grasping in the first place because you see that it's not

220
00:27:42,640 --> 00:27:51,120
worth grasping to. Of course, it becomes much more like letting go when you see that everything

221
00:27:51,120 --> 00:27:55,840
is not worth clinging to. And there's this kind of like an epiphany, which is this state

222
00:27:55,840 --> 00:28:00,680
of becoming enlightened where the mind just not quite like an epiphany, but it's a moment

223
00:28:00,680 --> 00:28:08,240
of complete release where there's an overarching sense that nothing that clinging is just

224
00:28:08,240 --> 00:28:20,520
not good, not good at all. It's a moment of complete release. That's where the mind

225
00:28:20,520 --> 00:28:31,600
enters into the banner. So that's the last, the sixth one. The last one is called the

226
00:28:31,600 --> 00:28:39,920
things we have to develop. So again, coming back to our practice, this relates to seeing,

227
00:28:39,920 --> 00:28:45,040
but in your process of seeing, there's a cultivation. There's a progression. So people

228
00:28:45,040 --> 00:28:53,000
who are interested in, hey, where is this going? How do I know if I'm progressing? Well,

229
00:28:53,000 --> 00:29:01,600
you really have this set of things that you have to develop. First of all, mindfulness,

230
00:29:01,600 --> 00:29:10,360
of course, that's the base. Second is called investigation. You have to be inquisitive, not

231
00:29:10,360 --> 00:29:21,480
intellectually inquisitive, but you have to have a sense of keenness or attention to the

232
00:29:21,480 --> 00:29:26,560
experiences, which is tied up with mindfulness. It's really a part of the mindfulness process,

233
00:29:26,560 --> 00:29:30,600
but mindfulness isn't just about repeating words in your head, like pain, pain. It's

234
00:29:30,600 --> 00:29:38,160
about really putting your mind not forcefully, but seriously, like with serious intent

235
00:29:38,160 --> 00:29:45,480
and investigation into not into the details. We don't need to examine it for this or for

236
00:29:45,480 --> 00:29:53,920
that. Just put your mind there. You have to be willing to investigate, to observe. Like

237
00:29:53,920 --> 00:29:58,480
think of it as a scientist. A scientist doesn't go looking for something, particularly.

238
00:29:58,480 --> 00:30:05,280
They go to observe to see what's there. The scientist doesn't go insane. Well, I'm looking

239
00:30:05,280 --> 00:30:11,000
for this conclusion. I'm looking for it to be like this. The scientist goes and says,

240
00:30:11,000 --> 00:30:17,360
I'm looking to see the way it is, see which way it is, and I will record that. That's

241
00:30:17,360 --> 00:30:29,100
investigation. You need effort. Again, not forceful effort, but you need to get yourself

242
00:30:29,100 --> 00:30:37,680
up to do it. It's not going to meditate itself. Meditation's not going to do itself. Like

243
00:30:37,680 --> 00:30:46,640
the work's not going to do itself. That's the same with meditation. You do the work. You

244
00:30:46,640 --> 00:31:03,000
get the fruit. You need zest, so you need to be keen on it, kind of excited in a sense,

245
00:31:03,000 --> 00:31:08,320
but excited. It's too strong. You have to get caught up in the practice. So it has to be

246
00:31:08,320 --> 00:31:14,000
something that's kind of like getting into the groove, and you'll start to feel this. Those

247
00:31:14,000 --> 00:31:19,840
of you who have been here a while, beginning it, you don't have this zest or this rapture

248
00:31:19,840 --> 00:31:24,160
is the word they use. I think rapture is a fairly good word, even though it's quite strong,

249
00:31:24,160 --> 00:31:29,520
but it's kind of implies this sort of getting caught up in it, where you really get in

250
00:31:29,520 --> 00:31:38,640
your groove, you get into a groove, and it becomes powerful, because you're no longer struggling

251
00:31:38,640 --> 00:31:45,440
just to figure out how to meditate. You're working to do what you know how to do, because

252
00:31:45,440 --> 00:31:54,560
you know how to do it. Then you need quiet, and this is a struggle, and this is something

253
00:31:54,560 --> 00:31:59,440
you have to struggle with, but your mind has to quiet down. You can't encourage straight

254
00:31:59,440 --> 00:32:06,720
thoughts. You can't encourage your mind to wander, to think, to get distracted. You need to

255
00:32:06,720 --> 00:32:12,720
calm your mind down. It's not forcefully, forcing it doesn't work, but you have to be

256
00:32:12,720 --> 00:32:24,720
methodical and systematic and thoughtful and intent on keeping the mind present,

257
00:32:26,400 --> 00:32:30,240
quieting the mind, because that leads to concentration, because the other thing you need,

258
00:32:30,240 --> 00:32:37,360
the next thing you need is concentration. You want to become enlightened while you have to focus.

259
00:32:37,360 --> 00:32:42,400
It doesn't really matter where your mind goes. It's not a question of keeping the mind with the

260
00:32:42,400 --> 00:32:49,840
breath. If your mind goes here, put your concentration there. Your mind goes here. Change the

261
00:32:49,840 --> 00:32:56,480
quality of the mind, not the experience. That's not the way to go. When the mind is here,

262
00:32:56,480 --> 00:33:02,640
that's where you build concentration. Just change it from a distracted mind to a focused mind.

263
00:33:03,920 --> 00:33:07,120
That's what the words do. That's what it does when we repeat these words.

264
00:33:09,200 --> 00:33:16,720
And the final thing to develop is equanimity. Yes, you need to be equanimous. Again, you can't force it,

265
00:33:16,720 --> 00:33:21,680
but it's important to keep in mind, hey, how should we observe things? When you say pain,

266
00:33:21,680 --> 00:33:28,320
it shouldn't be pain, pain, pain, this is a bad thing. You should strive to remind yourself,

267
00:33:28,320 --> 00:33:37,040
hey, that's pain, which is just a thing. The reason for the words, I mean, a real big part of it

268
00:33:38,880 --> 00:33:44,800
is to say it's basically to say to yourself, it's neutral. It's neither good nor bad.

269
00:33:44,800 --> 00:33:52,480
It is what it is. That's important because that what it is is not good or bad.

270
00:33:54,400 --> 00:34:02,320
There's no bearing on right, wrong, good, bad, me, mind, all the concepts and conceptions we have

271
00:34:02,320 --> 00:34:08,800
about it are extraneous. So that fact that it is what it is. So that focus,

272
00:34:08,800 --> 00:34:17,360
that equanimity isn't essential for allowing us to see clearly, for allowing us to become free,

273
00:34:17,360 --> 00:34:30,240
become enlightened. So seven sets, it's a whole comprehensive, but it said specifically,

274
00:34:30,240 --> 00:34:39,760
he said, this is the way of overcoming all the many leaks. In other ways, you can leak out.

275
00:34:40,560 --> 00:34:46,320
Here meditation is kind of like keeping yourself contained,

276
00:34:47,040 --> 00:34:55,920
keeping yourself whole strong, present here, but there's leaks. So this is the way to

277
00:34:55,920 --> 00:35:06,960
stop yourself from getting caught up in some kind of outflow. So what? It's from the

278
00:35:06,960 --> 00:35:16,080
sabbasto. It's one of my favorite talks. I think it's quite in the sense that it's quite useful,

279
00:35:16,080 --> 00:35:32,000
I think, to talk to meditators about just sums everything up in a very good way.

