1
00:00:00,000 --> 00:00:06,000
I'm considering ordaining and I want to know how much interaction with family members is proper.

2
00:00:06,000 --> 00:00:13,000
I've seen the passage in the Angutranikaya 232. My family worries they will not see me as a monk.

3
00:00:13,000 --> 00:00:20,000
I actually look this up and if your numbering is the same as mine, it's something to do with gratefulness.

4
00:00:20,000 --> 00:00:26,000
232, if your numbering is actually the book of twos, number 32, then it's gratefulness.

5
00:00:26,000 --> 00:00:31,000
So I assume you're saying, wouldn't it be ungrateful if I avoided my parents?

6
00:00:31,000 --> 00:00:41,000
Now if you're going by some other, maybe the poly English, poly text society, numbering, then I have no idea what that suit is about.

7
00:00:41,000 --> 00:00:46,000
Numbering is difficult because there's so many different numberings.

8
00:00:46,000 --> 00:00:52,000
So you may be worried about being grateful, but anyway we will take the question as it is.

9
00:00:52,000 --> 00:00:57,000
I can't think of any instance where interaction with family members is improper.

10
00:00:57,000 --> 00:01:02,000
There are certainly many instances where the Buddha gave exceptions for family members.

11
00:01:02,000 --> 00:01:06,000
For example, your parents are allowed to give them food.

12
00:01:06,000 --> 00:01:11,000
First, if you get food, even before you've eaten any, you're allowed to give it to your parents.

13
00:01:11,000 --> 00:01:21,000
You're allowed to ask for medicine. You're allowed to actually go to someone's house and beg and actually ask them for medicine.

14
00:01:21,000 --> 00:01:27,000
Of course, there's no offense, but you shouldn't just go up to a stranger and ask them.

15
00:01:27,000 --> 00:01:30,000
But what the meaning is, would be someone who supports you.

16
00:01:30,000 --> 00:01:33,000
So you can go stand in front of their house, if they say, what do you need?

17
00:01:33,000 --> 00:01:36,000
You can say, look, my parents are sick and I need medicine for them.

18
00:01:36,000 --> 00:01:38,000
This is actually allowed.

19
00:01:38,000 --> 00:01:43,000
You're allowed to go to visit your parents during the rains.

20
00:01:43,000 --> 00:01:47,000
Because during the rains, you're normally not allowed to travel, but you're allowed to go visit your parents.

21
00:01:47,000 --> 00:01:52,000
Even if you're not invited, if there's some reason and you know that they're sick,

22
00:01:52,000 --> 00:01:56,000
but they don't send a messenger for you, but you find out that they're sick.

23
00:01:56,000 --> 00:02:00,000
You're able to go to them and stay overnight up to six nights.

24
00:02:00,000 --> 00:02:02,000
This kind of thing.

25
00:02:02,000 --> 00:02:08,000
So there's an obvious potential for relationships.

26
00:02:08,000 --> 00:02:14,000
That being said, it's not advisable to get too close to family members.

27
00:02:14,000 --> 00:02:23,000
Besides your parents, especially, so getting involved with brothers, sisters and cousins and so on.

28
00:02:23,000 --> 00:02:31,000
Just because they're relatives, it can be dangerous because the reason for associating has nothing to do with the dhamma often.

29
00:02:31,000 --> 00:02:37,000
So that being the case is something that we should regard with certain care.

30
00:02:37,000 --> 00:02:43,000
Now, your parents, of course, the idea being grateful to them and giving something back to them inspires us often

31
00:02:43,000 --> 00:02:56,000
to want to meditate with them or encourage them in good deeds and stay with them to hopefully rub off on them, for example.

32
00:02:56,000 --> 00:03:00,000
So there's nothing wrong with that.

33
00:03:00,000 --> 00:03:10,000
So I would say basically what you can tell your family is there's no need to worry that they will always be able to see you as a monk.

34
00:03:10,000 --> 00:03:16,000
Not because you're a monk that they won't be able to see you, it might be because you're off in the forest in Thailand.

35
00:03:16,000 --> 00:03:25,000
That being the case, yes, they may not see you for many years until you have the support and the ability to return or until they have the ability to come to visit you.

36
00:03:25,000 --> 00:03:28,000
So that's something you have to deal with.

37
00:03:28,000 --> 00:03:32,000
Of course, for Thai people, this is a non-issue or for Asian people in general.

38
00:03:32,000 --> 00:03:37,000
It's usually a non-issue because the person will ordain nearby.

39
00:03:37,000 --> 00:03:41,000
And so they're able to actually visit with each other.

40
00:03:41,000 --> 00:03:49,000
But there's certainly nothing about being among that requires you to be apart from your parents at the very least.

41
00:03:49,000 --> 00:03:53,000
And really your relatives in general.

42
00:03:53,000 --> 00:03:57,000
There's nothing to say that we have to be apart from people as Buddhists.

43
00:03:57,000 --> 00:04:04,000
We're to try to cultivate seclusion as a general way of living our lives, but you can do that when you're around your relatives as well.

44
00:04:04,000 --> 00:04:07,000
You visit them, and you're in your room a lot of the time.

45
00:04:07,000 --> 00:04:12,000
You come out to visit, to talk, and then you go back and rest and do your meditation in your room.

46
00:04:12,000 --> 00:04:18,000
There's no incompatibility there.

47
00:04:18,000 --> 00:04:33,000
But there is a nice story in the Vcedimaga that I would encourage you to read about this monk who was so intent on solitude that many years later after he had ordained, he went back to see his parents and they didn't recognize him.

48
00:04:33,000 --> 00:04:41,000
And they fed him and supported him for three months, and he never told them he was their son, and they never found out that he was their son.

49
00:04:41,000 --> 00:04:45,000
And so they gave him this robe at the end and sent him back and said,

50
00:04:45,000 --> 00:04:46,000
could you give this to my son?

51
00:04:46,000 --> 00:04:48,000
I know he's in the monastery that you're headed to.

52
00:04:48,000 --> 00:04:58,000
And so he headed off and halfway along the way he met his teacher going to visit this village where his parents were.

53
00:04:58,000 --> 00:05:05,000
And so he gave the robe to his teacher and said, look, I'm much happier back at my monastery. I'm going to go back there and live in its occlusion.

54
00:05:05,000 --> 00:05:14,000
And the teacher looked at the robe and didn't quite understand, but he went on and came to the village.

55
00:05:14,000 --> 00:05:26,000
And the mother saw this monk's teacher coming alone and thought that that meant that her son had passed away because she assumed that the son would be with the teacher.

56
00:05:26,000 --> 00:05:30,000
And so she started crying and the teacher said, what's wrong?

57
00:05:30,000 --> 00:05:37,000
And she explained and explained how she'd given the robe to be given to her son.

58
00:05:37,000 --> 00:05:41,000
And so he pulled out the robe and he said, was it this robe and he said, that was your son.

59
00:05:41,000 --> 00:05:46,000
He spent three months with him, but he's so dedicated to seclusion.

60
00:05:46,000 --> 00:05:49,000
He didn't want to get involved with his relatives.

61
00:05:49,000 --> 00:05:55,000
So that's a kind of story you maybe don't want to tell your parents if you're looking for their permission to ordain.

62
00:05:55,000 --> 00:06:00,000
But he was the example of a very ardent meditator.

63
00:06:00,000 --> 00:06:08,000
And the Buddha, one of the first things he did was go back to see his father and teach his not one of the first things.

64
00:06:08,000 --> 00:06:16,000
But eventually he did go back to see his father and teach his wife and his son and so on.

65
00:06:16,000 --> 00:06:22,000
So there's certainly nothing against that and following by that example, we can certainly try our best to help them out.

66
00:06:22,000 --> 00:06:25,000
To realize the goodness that we've realized.

