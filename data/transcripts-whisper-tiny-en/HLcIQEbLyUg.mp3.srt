1
00:00:00,000 --> 00:00:07,000
Hello, so today I thought I would start my series on how to meditate.

2
00:00:07,000 --> 00:00:18,000
The first thing to explain in this series how to meditate is what is meditation?

3
00:00:18,000 --> 00:00:24,000
So in this video I'll take a look at an answer to this question, what is meditation?

4
00:00:24,000 --> 00:00:36,000
And try to answer the question how meditation accomplishes its function, and then I'll give a short demonstration of how to practice sitting meditation.

5
00:00:36,000 --> 00:00:39,000
So the first question, what is meditation?

6
00:00:39,000 --> 00:00:49,000
This is a question which might give different answers, but in this case I would take it if we look at the actual word meditation,

7
00:00:49,000 --> 00:00:53,000
and we take it back to the origin of the word meditation.

8
00:00:53,000 --> 00:00:58,000
And we can see that it comes from a root very similar to the word medication.

9
00:00:58,000 --> 00:01:03,000
And this is useful because these two words then become parallel.

10
00:01:03,000 --> 00:01:14,000
The word medication is, as we all know, it means something which is used to remedy the sicknesses which exist in the body.

11
00:01:14,000 --> 00:01:24,000
So the meditation then is the parallel. Meditation is something which cures the sicknesses which exist in the mind.

12
00:01:24,000 --> 00:01:32,000
And if we give this definition of meditation then we can understand clearly what it is that we're trying to do in the practice of meditation.

13
00:01:32,000 --> 00:01:51,000
So whereas some people might understand that meditation is simply to feel peaceful and calm, or to get away from one's problems to find a way to escape, to find a quiet place, and avoid all of the difficulties and all of the problems which exist inside.

14
00:01:51,000 --> 00:02:09,000
So meditation in the understanding and the definition that I give to it here means that we come to look at and come to understand and come to do away with the sicknesses, the things which bring suffering to ourselves, and the things which bring suffering to other people.

15
00:02:09,000 --> 00:02:20,000
Because these things of course exist inside of ourselves. Things like anger or greed or jealousy or stress and worry, fear, doubt and so on.

16
00:02:20,000 --> 00:02:29,000
All of the many things which we could label as mental sickness and so meditation the purpose is to go away with these things. This is an understanding of what is meditation.

17
00:02:29,000 --> 00:02:40,000
Now the question of how meditation accomplishes its function in the sense of being something which does away with these causes of suffering.

18
00:02:40,000 --> 00:02:44,000
Meditation creates a clear understanding of these things.

19
00:02:44,000 --> 00:02:52,000
So we have sicknesses inside, we actually look at these things and we come to understand what is the cause of these things.

20
00:02:52,000 --> 00:03:00,000
When we have suffering in our mind, sadness or depression or despair or fear or worry, where do these things come from?

21
00:03:00,000 --> 00:03:03,000
Why is it that these things exist in our mind?

22
00:03:03,000 --> 00:03:09,000
And when we can see the cause, just like in medicine, when we can see the cause of the sickness, we can do away with the cause.

23
00:03:09,000 --> 00:03:20,000
When we see what's causing our discomfort in the body, we can do away with the discomfort by doing away with the cause and the same in meditation.

24
00:03:20,000 --> 00:03:25,000
So for instance, we come to look at the things which exist inside for instance anger.

25
00:03:25,000 --> 00:03:37,000
Suppose we feel angry inside, we come to look at our own anger instead of going off when we get angry going off and yelling or saying bad things or doing bad things.

26
00:03:37,000 --> 00:03:41,000
Instead we come back inside and look at the cause of our suffering.

27
00:03:41,000 --> 00:03:51,000
Mostly the cause is not some other person, the cause is not something external to ourselves, the cause is really this following after this anger.

28
00:03:51,000 --> 00:03:56,000
And so instead of following after it, we come to create what we call a clear thought.

29
00:03:56,000 --> 00:04:06,000
The practice of meditation is about creating a clear understanding or a clear awareness of the things inside of ourselves and by extension of the things around us.

30
00:04:06,000 --> 00:04:18,000
So when we feel anger, instead of following after it or repressing it, we come to stay with it, we watch it, we let it arise, let it come and let it go.

31
00:04:18,000 --> 00:04:28,000
How do we do this? When we feel anger, instead of saying this is good, this is bad, this is me, this is mine, this is because of that person because of this or that.

32
00:04:28,000 --> 00:04:39,000
We focus simply on the anger and we use something like a mantra, except this time the mantra in this case is focused on the ultimate reality, the reality of the anger.

33
00:04:39,000 --> 00:04:49,000
And we say to ourselves, angry, angry and we're not good or bad, not judging it at all, simply being with it.

34
00:04:49,000 --> 00:05:04,000
Angry, angry, angry and this is an example of how we create a clear thought. Once we create this, we find our minds are not going after it or not, we don't have to go in yellow, we don't have to hurt other people and we don't have to repress it and hurt ourselves.

35
00:05:04,000 --> 00:05:20,000
And we simply are with this, and by extension we can use this with everything. This is a brief explanation of what it means to practice meditation, to cure the things, the problems which exist inside.

36
00:05:20,000 --> 00:05:36,000
If we feel sad, so we can say to ourselves sad, sad and so on. How we do this, we start the way to begin, is to develop an exercise.

37
00:05:36,000 --> 00:05:52,000
So, just like when children are learning how to walk, they can't just get up and walk and run and play, they have to slowly start from holding onto a chair or a table and then slowly get up and walk.

38
00:05:52,000 --> 00:06:15,000
So, we have an exercise to give to the person who wishes to learn meditation. So, instead of sending you off creating clear understanding, we start with something very simple. We start with a sitting meditation and we're going to use this to help ourselves learn how to create clear awareness, not following after a repressing thing.

39
00:06:15,000 --> 00:06:34,000
The way to do it, in the traditional ways to sit cross-legged, with your right foot in front or one foot in front of the other leg, this with the heel touching the front of the foot, not underneath, just in front, in the right hand, on top of the left hand, with a thumb's touching.

40
00:06:34,000 --> 00:06:47,000
Now, this is the traditional posture, this can also be done on the chair, it can also be done lying down, sitting against the wall, in any position which is comfortable, which creates a good environment for concentration.

41
00:06:47,000 --> 00:06:55,000
I'm not something which is so comfortable, of course, that you fall asleep, but it can be done in any posture which the physical body allows.

42
00:06:55,000 --> 00:07:04,000
And then, to practice, we're going to close our eyes and we're going to watch the stomach. When the breath goes into the body, the stomach will rise.

43
00:07:04,000 --> 00:07:11,000
We'll do so naturally, you don't have to force it or try to make it deeper or make it shallow or faster or slower.

44
00:07:11,000 --> 00:07:26,000
Simply watching, as it rises, we create a clear thought about the rising, not letting our minds wander or getting obsessed with the phenomenon, just saying to ourselves, rising.

45
00:07:26,000 --> 00:07:42,000
And when the breath goes out, the stomach falls, we say, falling, rising, falling, and we do this for, say, ten minutes.

46
00:07:42,000 --> 00:07:46,000
So, we're sitting here, we will sit still for ten minutes.

47
00:07:46,000 --> 00:07:55,000
And this is a basic practice to give a foundation, sort of a preliminary understanding of how meditation works.

48
00:07:55,000 --> 00:08:07,000
You'll notice, as you practice in this way, that there's great difficulty for the mind to stay with the rising and falling, because there'll be many other objects which arise at the time when we're trying to focus.

49
00:08:07,000 --> 00:08:19,000
So, instead of ignoring or trying to block these things out, we can use them as our meditation. Again, we're trying to focus on reality and we're trying to focus on our own body and our own mind, our own real nature.

50
00:08:19,000 --> 00:08:31,000
So, when we have, suppose we have pain, instead of blocking it out, we simply switch, instead of saying rising, falling, we watch the pain, not getting upset or getting stressed out or angry about the pain.

51
00:08:31,000 --> 00:08:43,000
Simply saying to ourselves, pain, pain, replacing the attachment, the thought which gets upset within a thought which is purely and clearly aware.

52
00:08:43,000 --> 00:08:48,000
Pain, pain, and we simply say pain, pain until the pain goes away.

53
00:08:48,000 --> 00:09:01,000
If we are thinking, if the mind starts to think, thinking about the past or future, whatever kind of thought, we simply say to ourselves, thinking, thinking that goes the rising, falling.

54
00:09:01,000 --> 00:09:09,000
After we say thinking, then come back, we think again, then go out again, thinking, thinking.

55
00:09:09,000 --> 00:09:24,000
If we have any sort of emotions coming up, if we have liking or wanting, we say liking or wanting, if we feel angry or frustrated or upset or sad or depressed, we say angry, angry, frustrated, sad.

56
00:09:24,000 --> 00:09:33,000
We simply say to ourselves, give a name, create a clear thought, this is only what it is, not good, not bad, not me, not mine.

57
00:09:33,000 --> 00:09:43,000
And we don't therefore follow after the emotion. We feel drowsy, we say drowsy, drowsy, if we feel distracted, not focused, we say distracted, distracted.

58
00:09:43,000 --> 00:09:51,000
If we have doubt about this or that, ourselves, the practice, say doubtting, doubtting, doubtting.

59
00:09:51,000 --> 00:09:54,000
And then we come back always to the rising thought.

60
00:09:54,000 --> 00:10:04,000
So this is a short demonstration and a brief explanation of how to practice meditation.

