1
00:00:00,000 --> 00:00:02,000
Not everyone

2
00:00:06,000 --> 00:00:08,000
Just a second

3
00:00:17,000 --> 00:00:19,000
Take a good stuff

4
00:00:19,000 --> 00:00:21,000
Oh

5
00:00:24,000 --> 00:00:28,000
So good evening. No Robin. Hello, Bhante

6
00:00:34,000 --> 00:00:44,000
Terrible things happening in America. Nice visual. Yes, you mean the hostage situation at the plant parenthood?

7
00:00:46,000 --> 00:00:48,000
Yeah

8
00:00:48,000 --> 00:00:53,000
I mean whether it actually had anything to do with it. There's this big thing in America

9
00:00:54,000 --> 00:00:56,000
That abortion

10
00:00:58,000 --> 00:01:02,000
That's interesting. I've had of course thoughts on it because of course

11
00:01:03,000 --> 00:01:05,000
I'm a woman and I'm a uterus and so on

12
00:01:06,000 --> 00:01:08,000
No because

13
00:01:10,000 --> 00:01:12,000
The whole idea of life

14
00:01:13,000 --> 00:01:15,000
Whether it is a wrong

15
00:01:15,000 --> 00:01:17,000
Have an impression

16
00:01:18,000 --> 00:01:21,000
Problems with it and I think I am pro choice

17
00:01:22,000 --> 00:01:24,000
I think that's really the issue here

18
00:01:24,000 --> 00:01:26,000
and I never really got it before

19
00:01:27,000 --> 00:01:29,000
because

20
00:01:29,000 --> 00:01:35,000
What pro choice people say women should have control of their own body right

21
00:01:36,000 --> 00:01:39,000
And the answer is always well, but what about the babies body?

22
00:01:39,000 --> 00:01:41,000
Baby's body

23
00:01:42,000 --> 00:01:47,000
But I think the part of the argument that's being made is not just ignoring of the baby's body

24
00:01:48,000 --> 00:01:52,000
That's not really the point the point is that the woman has

25
00:01:54,000 --> 00:01:58,000
It's like it's her own country right her own state

26
00:01:59,000 --> 00:02:01,000
So she has

27
00:02:02,000 --> 00:02:04,000
She's the government of her own body

28
00:02:05,000 --> 00:02:07,000
Meaning everyone in there has to

29
00:02:07,000 --> 00:02:11,000
You know she's still it still sounds cruel and awful to me

30
00:02:12,000 --> 00:02:16,000
You know that you should kill all your subjects all your citizens

31
00:02:18,000 --> 00:02:25,000
It's basically what it's saying, but nonetheless I do believe that people should have choice. I do believe it

32
00:02:29,000 --> 00:02:31,000
I'm not keen on society

33
00:02:33,000 --> 00:02:35,000
I guess I shouldn't say I

34
00:02:35,000 --> 00:02:40,000
Think there's an argument to be made from a Buddhist point of view for giving people this kind of freedom

35
00:02:42,000 --> 00:02:44,000
because

36
00:02:46,000 --> 00:02:50,000
Especially living in a pluralistic society

37
00:02:51,000 --> 00:02:55,000
where we we only have a reason to

38
00:02:57,000 --> 00:02:59,000
impose morality

39
00:02:59,000 --> 00:03:04,000
where it's where it inhibits people's freedoms and so on

40
00:03:08,000 --> 00:03:11,000
I don't know. I guess anyway, it's an interesting debate and I'm not clearly

41
00:03:12,000 --> 00:03:14,000
anti-abortion

42
00:03:14,000 --> 00:03:15,000
pro-life just as a

43
00:03:16,000 --> 00:03:17,000
pro-life

44
00:03:17,000 --> 00:03:19,000
pro-life but also probably pro-choice

45
00:03:20,000 --> 00:03:23,000
I don't think the Buddha was very keen on laws

46
00:03:23,000 --> 00:03:28,000
He just would have us follow the laws

47
00:03:31,000 --> 00:03:35,000
But it goes without saying obviously that as a Buddhist one should never have any abortion

48
00:03:37,000 --> 00:03:38,000
Not cool

49
00:03:38,000 --> 00:03:45,000
But whether as a Buddhist the government should be should legalize abortions

50
00:03:46,000 --> 00:03:48,000
I don't think that's where I think

51
00:03:48,000 --> 00:03:50,000
I'm not sure that that's fair anyway

52
00:03:50,000 --> 00:03:55,000
On the other hand, I should probably just be quiet because it's none of my business that I'm just a monk

53
00:03:56,000 --> 00:03:58,000
Let society work these things out

54
00:04:03,000 --> 00:04:05,000
Any thoughts for other you have children?

55
00:04:06,000 --> 00:04:08,000
Do you like four children?

56
00:04:08,000 --> 00:04:10,000
One of them thoughts on abortion

57
00:04:12,000 --> 00:04:16,000
I'll not agree with abortion because I think we're probably pretty similar but on

58
00:04:16,000 --> 00:04:19,000
an abortion movement

59
00:04:20,000 --> 00:04:27,000
The anti-abortion movement when it becomes, you know, the vigilant like that when it becomes violent like that

60
00:04:27,000 --> 00:04:32,000
Is ridiculous because you're killing people to make your point not to kill people

61
00:04:33,000 --> 00:04:35,000
So

62
00:04:36,000 --> 00:04:40,000
I don't think they actually know the motive with this particular case tonight

63
00:04:40,000 --> 00:04:46,000
They talked about, you know, it could be domestic violence or a disgruntled worker or anything

64
00:04:46,000 --> 00:04:52,000
They really, they hadn't specifically said that it was one of these sort of violent pro-life people

65
00:04:53,000 --> 00:05:01,000
So the fact that he was in a Planned Parenthood office though, it's kind of box likely, right?

66
00:05:01,000 --> 00:05:07,000
He does likely, yeah, and they said that he brought some things in that they thought might be bombs or something

67
00:05:07,000 --> 00:05:12,000
But a lot of questions, what are people thinking?

68
00:05:12,000 --> 00:05:14,000
Did you see that comic I posted?

69
00:05:14,000 --> 00:05:17,000
It actually got quite a bit of, you got some negative flag

70
00:05:17,000 --> 00:05:20,000
Someone posted this comic that I thought was really neat, it said

71
00:05:21,000 --> 00:05:24,000
It was of all these world leaders and they're all killing each other

72
00:05:25,000 --> 00:05:31,000
Like there's a lot of, there's terrorists on the list and there's like Dick Cheney was on the list in Netanyahu

73
00:05:31,000 --> 00:05:35,000
And they're all talking about how if we kill people it's going to

74
00:05:35,000 --> 00:05:37,000
We're going to achieve our goals

75
00:05:38,000 --> 00:05:39,000
Yeah

76
00:05:40,000 --> 00:05:44,000
Killing people hasn't really had the effect we were hoping for

77
00:05:44,000 --> 00:05:46,000
Maybe we should try something new

78
00:05:46,000 --> 00:05:51,000
Yeah, like not killing people to show how killing people is wrong

79
00:05:54,000 --> 00:05:57,000
It's kind of the same with capital punishment

80
00:05:57,000 --> 00:06:01,000
You can say, you know, here we are killing someone to show how killing people is wrong

81
00:06:01,000 --> 00:06:03,000
Yeah

82
00:06:04,000 --> 00:06:06,000
There's a lot of that

83
00:06:07,000 --> 00:06:12,000
Yeah, I mean am I for a nice, I don't think a very well thought out strategy

84
00:06:15,000 --> 00:06:17,000
Doesn't seem to be

85
00:06:18,000 --> 00:06:21,000
I guess arguably it's a deterrent, right, but

86
00:06:22,000 --> 00:06:25,000
It doesn't seem to be a deterrent

87
00:06:25,000 --> 00:06:32,000
They'll do their horrible things and then someone else has to kill the person because it's their job

88
00:06:32,000 --> 00:06:34,000
And then they have that bad karma, right?

89
00:06:42,000 --> 00:06:44,000
We have some questions

90
00:06:45,000 --> 00:06:51,000
Okay, shoot, okay, will the medma be recorded and put up on your channel

91
00:06:51,000 --> 00:06:54,000
No, I put a picture, huh?

92
00:06:55,000 --> 00:06:57,000
Oh, how did it go?

93
00:06:59,000 --> 00:07:07,000
There were five of us, which was expected. I wasn't expecting a large. It wasn't a mom, but it's just a group

94
00:07:09,000 --> 00:07:11,000
So Monday, I'm going to try to do it in the atrium

95
00:07:12,000 --> 00:07:15,000
This is not an atrium and told us not that I ordered our burrito

96
00:07:16,000 --> 00:07:18,000
Although it's also an atrium

97
00:07:18,000 --> 00:07:21,000
Big open space with some trees in it

98
00:07:24,000 --> 00:07:26,000
It's actually a perfect room for it and it's usually

99
00:07:27,000 --> 00:07:30,000
Quite it's very quiet and there's people sometimes sitting at the benches

100
00:07:32,000 --> 00:07:34,000
And so Monday will do that

101
00:07:34,000 --> 00:07:41,000
That's an indoor room with trees in it. Yeah. That sounds really nice. Mm-hmm. Yeah, it's neat that they've done big window

102
00:07:41,000 --> 00:07:43,000
You know one was just windows

103
00:07:43,000 --> 00:07:47,000
And big trees

104
00:07:49,000 --> 00:07:51,000
Ever greens

105
00:07:54,000 --> 00:07:56,000
That sounds really nice

106
00:07:56,000 --> 00:07:59,000
And a waterfall, but the waterfall is chlorinated

107
00:08:00,000 --> 00:08:03,000
Which is absurd because the whole room smells like chlorine anyway

108
00:08:04,000 --> 00:08:06,000
Kind of just a funny

109
00:08:07,000 --> 00:08:09,000
People I don't understand

110
00:08:09,000 --> 00:08:11,000
Yeah

111
00:08:11,000 --> 00:08:13,000
I think there are some pretty serious

112
00:08:14,000 --> 00:08:18,000
Problems with water that that's not purified though isn't there with

113
00:08:19,000 --> 00:08:21,000
Isn't that where

114
00:08:22,000 --> 00:08:25,000
We'll just let the people get when they go on cruise ships and

115
00:08:26,000 --> 00:08:27,000
Never mind

116
00:08:27,000 --> 00:08:31,000
You know you can salt the water. My mother's pool in Florida has salt

117
00:08:31,000 --> 00:08:33,000
Not that this isn't all really let's take the biggest

118
00:08:34,000 --> 00:08:36,000
questions

119
00:08:36,000 --> 00:08:39,000
But yeah, yeah back on track next question

120
00:08:39,000 --> 00:08:40,000
Next question

121
00:08:40,000 --> 00:08:41,000
Next question

122
00:08:41,000 --> 00:08:46,000
Have you found that people's minds tend to have a bias or higher propensity towards one particular

123
00:08:46,000 --> 00:08:47,000
delusion over another?

124
00:08:47,000 --> 00:08:50,000
For example greed, anger, etc?

125
00:08:55,000 --> 00:08:57,000
Suppose yeah everyone's different

126
00:08:58,000 --> 00:09:02,000
Most people have all of this all of the filements mixed up inside

127
00:09:02,000 --> 00:09:05,000
We're mostly full

128
00:09:06,000 --> 00:09:08,000
Put them in full

129
00:09:10,000 --> 00:09:12,000
Follow the filements put to China

130
00:09:16,000 --> 00:09:21,000
When I first started doing walking meditation, I wobbled a lot and lost my balance a lot

131
00:09:21,000 --> 00:09:25,000
Now I'm better at balancing, but a lot of times my legs will shake when I'm walking

132
00:09:25,000 --> 00:09:27,000
And I feel like I'm not strong enough

133
00:09:28,000 --> 00:09:31,000
Am I not strong enough or am I walking too much or too little

134
00:09:31,000 --> 00:09:33,000
Or am I just doing it wrong?

135
00:09:34,000 --> 00:09:36,000
Just human

136
00:09:36,000 --> 00:09:38,000
It's not supposed to be perfect

137
00:09:38,000 --> 00:09:41,000
It's in fact supposed to be just supposed to show you the imperfect

138
00:09:41,000 --> 00:09:43,000
So we're probably doing it right

139
00:09:43,000 --> 00:09:46,000
And the fact that you're seeing the wobbling is good

140
00:09:46,000 --> 00:09:49,000
You're seeing that your body is imperfect

141
00:09:50,000 --> 00:09:53,000
But what you're also seeing is that you want it to be perfect

142
00:09:53,000 --> 00:09:57,000
Thinking of this tendency to desire for your body to get stronger and better

143
00:09:57,000 --> 00:09:59,000
And so it's not going to happen

144
00:09:59,000 --> 00:10:05,000
Guess what? You're going to get weaker, more stable, less balanced

145
00:10:05,000 --> 00:10:09,000
And then you get crippled and bend ridden and dying

146
00:10:09,000 --> 00:10:11,000
That's your future

147
00:10:11,000 --> 00:10:13,000
Congratulations

148
00:10:13,000 --> 00:10:15,000
No done for being born

149
00:10:17,000 --> 00:10:20,000
It's amazing how when you do something and you don't think about it

150
00:10:20,000 --> 00:10:24,000
If you're not walking mindfully, if you're just walking, you feel like you walk great

151
00:10:24,000 --> 00:10:28,000
But the more you really are in tune with the movements of walking

152
00:10:28,000 --> 00:10:31,000
The worse you realize you walk

153
00:10:31,000 --> 00:10:35,000
But you're not exactly letting it flow

154
00:10:35,000 --> 00:10:41,000
We're kind of interrupting the process on purpose

155
00:10:41,000 --> 00:10:44,000
And it's a bit more challenging

156
00:10:44,000 --> 00:10:47,000
But I think more to the point we get

157
00:10:47,000 --> 00:10:48,000
Kind of

158
00:10:52,000 --> 00:10:57,000
As you continue walking meditation, if you're not being very mindful of everything

159
00:10:57,000 --> 00:10:59,000
of all your movements

160
00:10:59,000 --> 00:11:06,000
It's very easy to build up a version or distraction or so on

161
00:11:06,000 --> 00:11:08,000
To the point where it affects you

162
00:11:08,000 --> 00:11:12,000
Because if you're very mindful, if you're very calm, if you're very focused, walking meditation

163
00:11:12,000 --> 00:11:14,000
It should be fairly easy

164
00:11:16,000 --> 00:11:23,000
But even still, the body is not well equipped to do slow movements like that

165
00:11:23,000 --> 00:11:28,000
You're not as well equipped as it is to do ordinary walking, which is easier

166
00:11:30,000 --> 00:11:32,000
Ordinary walking is

167
00:11:32,000 --> 00:11:34,000
So I get into rhythm

168
00:11:34,000 --> 00:11:37,000
And rhythms are good for the body because

169
00:11:37,000 --> 00:11:40,000
I'm for the mind because we build up this static

170
00:11:40,000 --> 00:11:46,000
Charge, so the mind is able to send out the instructions on a

171
00:11:46,000 --> 00:11:48,000
No, it's just comfortable

172
00:11:48,000 --> 00:11:50,000
The mind is good with this getting interrupt

173
00:11:50,000 --> 00:11:55,000
Kind of break that up on purpose because we want to see how the mind is going

174
00:11:55,000 --> 00:11:59,000
The mind reacts, we want to challenge and we want to

175
00:11:59,000 --> 00:12:05,000
So push the mind, push it to its limits until it

176
00:12:05,000 --> 00:12:09,000
Or force it to show if it's going to react

177
00:12:09,000 --> 00:12:13,000
So the mind's going to get upset about something, we want to see that

178
00:12:13,000 --> 00:12:15,000
We want to change that

179
00:12:15,000 --> 00:12:18,000
We have to push it to the point where it would get upset

180
00:12:18,000 --> 00:12:23,000
So that we can learn how to deal with difficulty and not get upset

181
00:12:23,000 --> 00:12:26,000
So it should be challenging, it should be uncomfortable

182
00:12:26,000 --> 00:12:29,000
It should force you out of your comfort zone

183
00:12:29,000 --> 00:12:33,000
So if it was just ordinary walking and it was easy, that wouldn't do that

184
00:12:33,000 --> 00:12:37,000
It should be impermanent, unstable and satisfying and constant

185
00:12:37,000 --> 00:12:40,000
Uncontrollable, that's what you should see

186
00:12:40,000 --> 00:12:51,000
So good for you

187
00:12:51,000 --> 00:12:53,000
Can we get caught up?

188
00:12:53,000 --> 00:12:55,000
We are caught up

189
00:12:55,000 --> 00:13:00,000
It's 958, so that's a good hour

190
00:13:00,000 --> 00:13:05,000
I'm afraid it would be up tonight, tomorrow morning probably

191
00:13:05,000 --> 00:13:09,000
So let's call it a night

192
00:13:09,000 --> 00:13:14,000
Thanks Robyn, patiently sitting with me

193
00:13:14,000 --> 00:13:17,000
Thank you everyone for tuning in

194
00:13:17,000 --> 00:13:19,000
Have a good night

195
00:13:19,000 --> 00:13:43,000
Thank you Bhante, good night

