1
00:00:00,000 --> 00:00:07,560
Should I always be tolerant? Hi, Bandai, I am a Buddhist starter. The renovate

2
00:00:07,560 --> 00:00:13,260
worker next to my room works until midnight, every day and plays loud music. I

3
00:00:13,260 --> 00:00:16,560
talk to them many times, but they didn't listen. Should Buddhists be always

4
00:00:16,560 --> 00:00:24,480
tolerant? It's not really the most important thing to ask. Should Buddhists always

5
00:00:24,480 --> 00:00:28,800
be tolerant? Obviously, that's what most people would ask. But just to point

6
00:00:28,800 --> 00:00:33,000
out that, let's approach this from the point of view of what's really going to

7
00:00:33,000 --> 00:00:36,400
benefit me. I mean, obviously, this isn't benefiting you, talking to them

8
00:00:36,400 --> 00:00:44,720
being intolerant. And I would say, anybody who wants to be happy should be

9
00:00:44,720 --> 00:00:50,560
tolerant. Because intolerance, and there's going to be a provide one, yeah,

10
00:00:50,560 --> 00:00:55,320
there's a qualifier. But for the most, in general, tolerance is always going

11
00:00:55,320 --> 00:00:59,120
to make you happier. It's always going to bring you more peace than intolerant.

12
00:00:59,120 --> 00:01:07,480
Now, there's a point where intolerance becomes delusion, where you, you, it

13
00:01:07,480 --> 00:01:15,440
actually is the covering up of your inclination to change things. So there's

14
00:01:15,440 --> 00:01:21,480
many cases of that tolerance of your own faults, tolerance of evil, in the

15
00:01:21,480 --> 00:01:27,560
sense of condoning it, when you have a chance to help someone to even just

16
00:01:27,560 --> 00:01:31,920
to advise someone. So you see someone doing something that's going to cause

17
00:01:31,920 --> 00:01:35,440
them suffering. And right away, there should be the inclination in a good

18
00:01:35,440 --> 00:01:40,440
person to help them, to want to help them to see and help them to become better

19
00:01:40,440 --> 00:01:47,600
people. But by quashing that and by tolerating their evil. At times, you know, at

20
00:01:47,600 --> 00:01:51,680
least when it's your place to say something, like if it's your children, parents

21
00:01:51,680 --> 00:01:55,520
who indulge their children and allow them to do whatever they want, or let them

22
00:01:55,520 --> 00:01:59,080
explore. That's fine if they want to do drugs or whatever, let them explore, if

23
00:01:59,080 --> 00:02:04,960
they want to get drunk and so on. And so on. Of course, there are times where

24
00:02:04,960 --> 00:02:12,840
you can't control other people, but there are times where you would just let

25
00:02:12,840 --> 00:02:16,480
it go. And it's not even letting it go, because there is the inclination to

26
00:02:16,480 --> 00:02:21,520
help in a clear mind. There will be the natural inclination to do something

27
00:02:21,520 --> 00:02:24,280
about it. It's the correct thing to do, the correct response. And by

28
00:02:24,280 --> 00:02:29,400
quashing that, which many people I think confuse with true Buddhist practice,

29
00:02:29,400 --> 00:02:32,800
they say, well, you should just let it go. But you're not really letting it go,

30
00:02:32,800 --> 00:02:36,840
because there's the inclination, the default is the inclination to help the

31
00:02:36,840 --> 00:02:41,120
inclination to engage, which is natural. Human beings are engaged with each other.

32
00:02:41,120 --> 00:02:47,240
We're a part of the universe to be detached. It's not really Buddhist. We're part

33
00:02:47,240 --> 00:02:50,760
of the process. There is an engagement there, which I think is quite crucial

34
00:02:50,760 --> 00:02:55,160
in many Buddhists don't realize. That's not what you're asking. Obviously, it's

35
00:02:55,160 --> 00:02:59,960
not your place to help these people overcome their music addiction. If you were

36
00:02:59,960 --> 00:03:04,200
the landlord, it would be your place to tell them to quiet down for the

37
00:03:04,200 --> 00:03:11,200
neighbors, for example. But certainly loud music should not bother. Your

38
00:03:11,200 --> 00:03:16,000
sleeping should not bother your life. This is something that absolutely you

39
00:03:16,000 --> 00:03:22,400
should meditate on and cultivate tolerance and equanimity. And understand that

40
00:03:22,400 --> 00:03:27,040
it's just sound. Obviously, we're conditioned against that in the West, in

41
00:03:27,040 --> 00:03:31,560
Europe and Canada, and Europe and North America. We're conditioned to expect

42
00:03:31,560 --> 00:03:35,480
things from other people, or conditioned to expect quiet and so on. If you go to

43
00:03:35,480 --> 00:03:39,480
Thailand, it's quite an eye opener and it's humbling, because the things

44
00:03:39,480 --> 00:03:45,240
they tolerate there, it's amazing. In our monastery, there would be two weeks

45
00:03:45,240 --> 00:03:50,440
out of every year, two solid weeks, where basically 24-7, they would have music

46
00:03:50,440 --> 00:03:55,960
blaring into the monastery. They really loud speakers up on the stage. I don't

47
00:03:55,960 --> 00:04:00,840
maybe not during the day. Yeah. I think it was basically 24-7, like really 24

48
00:04:00,840 --> 00:04:06,960
hours a day, loud music. I can't remember. It may stop somewhere in the early

49
00:04:06,960 --> 00:04:12,160
morning and pick up in the morning, but it just seemed like impossible. How

50
00:04:12,160 --> 00:04:16,080
could you possibly meditate through this? But Thai people were unfazed by it,

51
00:04:16,080 --> 00:04:19,600
and so they'd meditate. It's not really a problem. Or they were less

52
00:04:19,600 --> 00:04:24,240
phased by it. And in general, the amount of noise that people in Asia put up

53
00:04:24,240 --> 00:04:33,720
with. So it really is just a problem of the highly developed and privileged

54
00:04:33,720 --> 00:04:40,840
world, the imperial world, the people who went and conquered the rest of the

55
00:04:40,840 --> 00:04:45,120
world and got really rich off of it, because we expect something that is

56
00:04:45,120 --> 00:04:51,320
unreasonable. We expect to be treated like royalty, so we expect others to

57
00:04:51,320 --> 00:04:55,480
respect us. I mean, this is not obviously what you're going through your

58
00:04:55,480 --> 00:05:02,440
mind, but it's ingrained in us. So we have these expectations that, well, if

59
00:05:02,440 --> 00:05:07,120
nothing else are harmful, are causing us suffering. Without your expectations,

60
00:05:07,120 --> 00:05:10,800
it's probably a lot easier to bear people around you blaring their loud

61
00:05:10,800 --> 00:05:16,800
music. And so I would work very much on your expectations and on your views

62
00:05:16,800 --> 00:05:21,160
and come to see that it's really just a cultural thing where we require things

63
00:05:21,160 --> 00:05:30,120
from other people like quiet and civility and we expect them to we expect, as

64
00:05:30,120 --> 00:05:35,520
the Buddha said, speech at the right time, speech that is polite, we expect

65
00:05:35,520 --> 00:05:42,440
speech that is truthful, speech that is beneficial, but he said, you're always

66
00:05:42,440 --> 00:05:45,840
going to have speech that is unbeneficial, speech that is harmful and so on.

67
00:05:45,840 --> 00:05:50,120
You're always going to have things that disappoint you. So it's like, I think

68
00:05:50,120 --> 00:05:56,240
Milarepa is the one who said, if you're, if the earth is, if the whole earth

69
00:05:56,240 --> 00:06:02,640
was covered in glass, broken glass, you have two choices. One, you can cover the

70
00:06:02,640 --> 00:06:10,120
whole world with leather. Or two, you can wear sandals. And where the metaphor

71
00:06:10,120 --> 00:06:12,600
kind of breaks down because we're not trying to cover up the problem, but

72
00:06:12,600 --> 00:06:16,440
we're trying to change ourselves. It's it's clear what he's saying is, you know,

73
00:06:16,440 --> 00:06:20,080
two choices change the world, make the world so it's always as you

74
00:06:20,080 --> 00:06:24,440
would have it or change yourself so that you can deal with it. Because in the

75
00:06:24,440 --> 00:06:29,200
end, I mean, think of what some people in the world deal with. People, there are

76
00:06:29,200 --> 00:06:34,160
people in the world to deal with starvation and deal with the constant threat to

77
00:06:34,160 --> 00:06:44,280
their, to their very survival from military or corruption or disease or

78
00:06:44,280 --> 00:06:55,000
famine. And so the, the, it's quite clear that we're just dealing with our own

79
00:06:55,000 --> 00:07:01,600
expectations that are actually unreasonable. If we, in the point being that, that

80
00:07:01,600 --> 00:07:05,600
any of these things can come to us at any time. So if you're always expecting

81
00:07:05,600 --> 00:07:08,800
something from the world around you, you're always vulnerable. The only way to

82
00:07:08,800 --> 00:07:15,920
be invincible is to have no faults, to have to have no hooks where if this

83
00:07:15,920 --> 00:07:21,160
than that, if this comes, then I react to where you no longer react to the

84
00:07:21,160 --> 00:07:46,200
reinvices of the tunes of life.

