1
00:00:00,000 --> 00:00:26,000
Good evening, everyone.

2
00:00:26,000 --> 00:00:42,000
Welcome to our evening dhamma.

3
00:00:42,000 --> 00:00:47,000
Who sent a question today?

4
00:00:47,000 --> 00:00:59,000
Send a question today by someone who lives in the area.

5
00:00:59,000 --> 00:01:03,000
This internet thing now.

6
00:01:03,000 --> 00:01:06,000
Back in my day when you wanted to ask your teacher a question,

7
00:01:06,000 --> 00:01:11,000
you had to actually go see your teacher.

8
00:01:11,000 --> 00:01:17,000
No, I just think they can Facebook me.

9
00:01:17,000 --> 00:01:20,000
Not impressed.

10
00:01:20,000 --> 00:01:25,000
I have to make the dhamma a little bit difficult.

11
00:01:25,000 --> 00:01:28,000
You have to be.

12
00:01:28,000 --> 00:01:29,000
It can't be too convenient.

13
00:01:29,000 --> 00:01:33,000
You can't just pull out your phone and message your teacher.

14
00:01:33,000 --> 00:01:35,000
That's not cool.

15
00:01:35,000 --> 00:01:37,000
You can, I don't mind.

16
00:01:37,000 --> 00:01:40,000
I'm just not going to answer.

17
00:01:40,000 --> 00:01:44,000
Instead, I'm going to come here and answer.

18
00:01:44,000 --> 00:01:47,000
Use it to talk about the dhamma tonight.

19
00:01:47,000 --> 00:01:52,000
That's an interesting question.

20
00:01:52,000 --> 00:01:56,000
It's an interesting situation.

21
00:01:56,000 --> 00:02:08,000
The question is,

22
00:02:08,000 --> 00:02:12,000
when we first learn about Buddhism,

23
00:02:12,000 --> 00:02:14,000
it's quite simple, no?

24
00:02:14,000 --> 00:02:18,000
It's quite a simple concept and some very simple truths.

25
00:02:18,000 --> 00:02:20,000
It's some fairly simple practices.

26
00:02:20,000 --> 00:02:23,000
It's not easy.

27
00:02:23,000 --> 00:02:28,000
We undertake to practice meditation and we find it quite difficult.

28
00:02:28,000 --> 00:02:35,000
But we see the correlation between the teachings and the practice.

29
00:02:35,000 --> 00:02:46,000
Yes, indeed, as I practice, I sort of start to realize the truth of these teachings.

30
00:02:46,000 --> 00:02:56,000
I certainly am able to free myself up from some of the bad habits that I've carried around.

31
00:02:56,000 --> 00:03:01,000
I'm able to give up my addictions and so on.

32
00:03:01,000 --> 00:03:12,000
They become quite confident and secure in the results that you've gained from the practice.

33
00:03:12,000 --> 00:03:20,000
This has changed me. You might even start to think you're a little bit enlightened.

34
00:03:20,000 --> 00:03:24,000
For a while, you think your life has changed.

35
00:03:24,000 --> 00:03:33,000
Meditation has changed your life.

36
00:03:33,000 --> 00:03:43,000
And then after some time, short time, long time,

37
00:03:43,000 --> 00:03:46,000
all of those things that you thought you'd free yourself,

38
00:03:46,000 --> 00:03:52,000
all those changes you thought you'd made begin to unravel.

39
00:03:52,000 --> 00:03:58,000
So suddenly you find yourself confronted by the same old problems again.

40
00:03:58,000 --> 00:04:01,000
Addiction, aversion,

41
00:04:01,000 --> 00:04:19,000
they start to come back and you start to realize they were not really eradicated.

42
00:04:19,000 --> 00:04:27,000
It's an interesting case because someone can practice quite strenuously

43
00:04:27,000 --> 00:04:36,000
and really change the state of their mind, purify the state of their mind.

44
00:04:36,000 --> 00:04:46,000
But without the depth of intensive practice or long-term practice,

45
00:04:46,000 --> 00:04:51,000
most of the benefits one gains are temporary.

46
00:04:51,000 --> 00:04:57,000
They're the suppression of the defalments.

47
00:04:57,000 --> 00:05:05,000
So it means it's easy to become, to overestimate the results,

48
00:05:05,000 --> 00:05:09,000
one gains in the practice,

49
00:05:09,000 --> 00:05:17,000
to overestimate yourself and your attainment and your progress in the practice.

50
00:05:17,000 --> 00:05:24,000
And so in the beginning, it's one meditator put it well recently before he left.

51
00:05:24,000 --> 00:05:30,000
It's like...

52
00:05:30,000 --> 00:05:33,000
It's like the tip of the iceberg.

53
00:05:33,000 --> 00:05:36,000
When you first come, you only see the tip and you think,

54
00:05:36,000 --> 00:05:38,000
well, that's quite big.

55
00:05:38,000 --> 00:05:40,000
And then you okay?

56
00:05:40,000 --> 00:05:43,000
And you tackle this problem

57
00:05:43,000 --> 00:05:47,000
and you conquer the tip of the iceberg thinking,

58
00:05:47,000 --> 00:05:49,000
oh yes, this was a big thing deal.

59
00:05:49,000 --> 00:05:54,000
And once you conquer the tip of the iceberg, you look under the water and you see,

60
00:05:54,000 --> 00:06:04,000
oh, that was just the tip of the iceberg.

61
00:06:04,000 --> 00:06:12,000
This is what the meditators here are realizing through their intensive hours and hours a day.

62
00:06:12,000 --> 00:06:23,000
Through the course they're tackling the iceberg and by the end of the course they finally reached the surface of the water

63
00:06:23,000 --> 00:06:32,000
and they looked underneath and realized the magnitude of the problem.

64
00:06:32,000 --> 00:06:36,000
Because who we are, who we are, is just habits.

65
00:06:36,000 --> 00:06:40,000
It's a simple thing to say and it sounds like it should be easy to change,

66
00:06:40,000 --> 00:06:44,000
but our habits are far more far,

67
00:06:44,000 --> 00:06:51,000
or many orders of magnitude greater than we're aware.

68
00:06:51,000 --> 00:06:59,000
Spending into past lives and the mind is far more complicated,

69
00:06:59,000 --> 00:07:07,000
complex than we originally understand.

70
00:07:07,000 --> 00:07:15,000
And so in the beginning we have a simplistic understanding of the problem of the depth of our,

71
00:07:15,000 --> 00:07:24,000
our, let's say, neurotic behavior or our mental problems.

72
00:07:24,000 --> 00:07:31,000
And we're looking for always the quick fix and so we think there should be some simple cure and we're hoping,

73
00:07:31,000 --> 00:07:35,000
always hoping that it will go away soon.

74
00:07:35,000 --> 00:07:39,000
And so at the slightest hint of it easing,

75
00:07:39,000 --> 00:07:51,000
we get excited and we begin to think that maybe we've made a breakthrough and now it's going to be gone and not come back.

76
00:07:51,000 --> 00:08:01,000
I think this is a big, well a bit of a concern for people meditating at home on a daily basis

77
00:08:01,000 --> 00:08:11,000
who have never done intensive meditation practice is that it's easy to lull yourself into a false sense of security that you've somehow gained,

78
00:08:11,000 --> 00:08:15,000
who I've been practicing for years or so on.

79
00:08:15,000 --> 00:08:26,000
And it's not to, to realize that of course it's a great thing to do daily practice and you will realize all sorts of things, but

80
00:08:26,000 --> 00:08:38,000
it's also very easy to lull yourself into a false sense of security thinking that you've actually eradicated the problems

81
00:08:38,000 --> 00:08:53,000
and then be disappointed when they start to come back or when you find out that there's still there.

82
00:08:53,000 --> 00:09:09,000
The filaments are of three kinds of understanding, technical understanding of how the mind works or the types of defilements that sort of clearly lay it out to lay out what we mean by defilement.

83
00:09:09,000 --> 00:09:24,000
Kilesa, we have the, the kamakilesa, the actions to file the actions.

84
00:09:24,000 --> 00:09:32,000
This is speech and bodily actions that is based on unwholesumness.

85
00:09:32,000 --> 00:09:44,000
This is the worst. This is when you think of what makes a person evil or what makes a person do things that cause suffering to themselves and others.

86
00:09:44,000 --> 00:09:50,000
We think of the actions and the speech, the things they say and the things they do.

87
00:09:50,000 --> 00:09:59,000
This is how we judge people normally. It's very hard to judge people based on their state of mind because we're not privy to that.

88
00:09:59,000 --> 00:10:08,000
So instead we are able to see and we pay attention to people's behavior.

89
00:10:08,000 --> 00:10:14,000
So if we kill and steal and lie and cheat, this is the sort of, this is the real defilement.

90
00:10:14,000 --> 00:10:24,000
This is what defiles a person. It's not by water that one becomes pure.

91
00:10:24,000 --> 00:10:35,000
It's not by bathing. One is pure according to one's actions and one's speech.

92
00:10:35,000 --> 00:10:41,000
This is very real. I mean, these are a very real concern for Buddhists.

93
00:10:41,000 --> 00:10:51,000
And so most Buddhists are able to, to some extent, overcome this sort of defilement fairly easily.

94
00:10:51,000 --> 00:11:02,000
In fact, there are a lot of, I think, cultural Buddhists who tend to think that they're following the Buddhist teaching just because they don't kill and steal and lie and cheat and take drugs and alcohol.

95
00:11:02,000 --> 00:11:09,000
Of course, there are many Buddhists who aren't even able to keep that yet call themselves Buddhists.

96
00:11:09,000 --> 00:11:18,000
But it's not really enough. It's not really, I mean, this was the support. These were supportive measures.

97
00:11:18,000 --> 00:11:28,000
A person can keep all the five precepts and still not be free from any sort of defilement.

98
00:11:28,000 --> 00:11:40,000
And the potential to commit a moral activity to kill or steal or lie or cheat is still always present.

99
00:11:40,000 --> 00:11:58,000
It's a constant effort to remain vigilant and to refrain from unwholesome acts in speech because the mind is still defiled.

100
00:11:58,000 --> 00:12:07,000
Then so the second level of defilement is what's going on inside the mind.

101
00:12:07,000 --> 00:12:17,000
These are the five hindrances, liking and disliking desire and diversion.

102
00:12:17,000 --> 00:12:27,000
Worry, restlessness, fear, doubt, confusion, guilt.

103
00:12:27,000 --> 00:12:32,000
Even feeling guilty about your defilement, this is more defilement.

104
00:12:32,000 --> 00:12:44,000
Because one of the unwholesome misses is to worry about the bad things you've done.

105
00:12:44,000 --> 00:12:50,000
Now this is yet more real than the first type.

106
00:12:50,000 --> 00:13:01,000
The person can refrain and yet be full of anger and greed and yet refrain from killing and stealing and so on.

107
00:13:01,000 --> 00:13:11,000
The person can be sitting on a meditation mat with their eyes closed and be full of greed and anger and delusion.

108
00:13:11,000 --> 00:13:16,000
I'm sure our meditators here can relate at times. This is the state.

109
00:13:16,000 --> 00:13:30,000
Our minds are full of these defilements. This is what we wrestle with here in the center.

110
00:13:30,000 --> 00:13:42,000
This is what we normally understand as defilements.

111
00:13:42,000 --> 00:13:53,000
This is the arisen defilements and there's still a problem with that because when these are gone we think our minds are pure and we've become enlightened.

112
00:13:53,000 --> 00:14:03,000
There are cases where people enter into states that are free from these and as most of us do when we practice meditation.

113
00:14:03,000 --> 00:14:10,000
But then think to themselves that they've freed themselves from defilement.

114
00:14:10,000 --> 00:14:14,000
It's quite common in a meditation center to feel that way to some extent.

115
00:14:14,000 --> 00:14:23,000
You've done a long course in meditation and on the day you leave you feel quite different.

116
00:14:23,000 --> 00:14:31,000
You feel like you have no greed, no anger, no delusion.

117
00:14:31,000 --> 00:14:38,000
And it may be true that at that moment you don't, you're very mindful and agreed to anger.

118
00:14:38,000 --> 00:14:46,000
But there's still something missing because you see when you stay at a meditation center there's very little caused for greed, anger and delusion.

119
00:14:46,000 --> 00:14:56,000
Sure, on a basic level there's lots but there's none of the very strong instigators.

120
00:14:56,000 --> 00:15:03,000
There's no angry people or attractive objects of desire.

121
00:15:03,000 --> 00:15:12,000
There's no pressing needs and duties.

122
00:15:12,000 --> 00:15:20,000
There's nothing to worry about, there's nothing to stress over and so you become quite relaxed

123
00:15:20,000 --> 00:15:31,000
and have a feeling that your mind is very pure and your mind will be quite relatively quite pure just by staying in the meditation center.

124
00:15:31,000 --> 00:15:35,000
Because there's very little to make it impure.

125
00:15:35,000 --> 00:15:47,000
You might start lasting over the food in the kitchen or eyeing your bed with attraction or aversion.

126
00:15:47,000 --> 00:15:57,000
You might get angry about the guy next door who snores or who opens the door loudly or so on.

127
00:15:57,000 --> 00:16:06,000
But there's very little of any significance to make you really angry and so it's greedy and so it's quite easy at times.

128
00:16:06,000 --> 00:16:16,000
There'll still be many defilements that come up but at times it's easy to enter into a state that's free from defilement, a free from a risen defilement.

129
00:16:16,000 --> 00:16:27,000
And so the final level, there is a third level and it's called the Anusaya. I'm sure many of you have heard of this and many of you heard me talk about these levels.

130
00:16:27,000 --> 00:16:34,000
But the Anusaya or the Anu means under or...

131
00:16:34,000 --> 00:16:52,000
and it was a prefix, Saya means to lie down or to rest. So these are the underlying or the latent they say or the proclivities, the tendencies.

132
00:16:52,000 --> 00:17:01,000
The Anusaya means the potential, potential for defilement.

133
00:17:01,000 --> 00:17:08,000
And there are a whole list of these in some cases. There are five in other places. There are seven.

134
00:17:08,000 --> 00:17:12,000
It's not really important what they are. I mean, I can list them but it's...

135
00:17:12,000 --> 00:17:17,000
You miss the point if you list them or if you focus too much on the list.

136
00:17:17,000 --> 00:17:24,000
There's Kamaraga which is desire for sensuality. There's Battinga which is a version.

137
00:17:24,000 --> 00:17:34,000
And these are the bases for greed, the base, the underlying kernel, the root for greed and anger.

138
00:17:34,000 --> 00:17:44,000
Then there's Diti and Mana, Diti which is views, Mana which is conceived which you can tell which is doubt.

139
00:17:44,000 --> 00:17:50,000
Baoaraga there's desire for becoming so ambitions and so on.

140
00:17:50,000 --> 00:18:00,000
And Davidja which is ignorance. But none of these should be understood as the arisen defilement.

141
00:18:00,000 --> 00:18:10,000
It means it's the tendency for these and for all defilements to arise.

142
00:18:10,000 --> 00:18:23,000
It's the improper conception of reality. The ignorance, the misunderstanding, the delusion.

143
00:18:23,000 --> 00:18:30,000
That allows the potential for all of these things and others to arise.

144
00:18:30,000 --> 00:18:37,000
And this is what we're really getting at. Meditation as I've said quite a bit recently is about changing who you are.

145
00:18:37,000 --> 00:18:45,000
It's a very fundamental core of who you are. Much of who we are just has to be thrown out or discarded or discounted anyway.

146
00:18:45,000 --> 00:18:53,000
And trivialize, minimalize, ignored, not given power.

147
00:18:53,000 --> 00:19:04,000
We can't no longer rely on who we are. It doesn't mean we have to throw it all out just because it's who we are or we discard everything and put ourselves.

148
00:19:04,000 --> 00:19:14,000
But it all has to be subject to discrimination, to scrutiny.

149
00:19:14,000 --> 00:19:24,000
We have to scrutinize every aspect of who we are and change very much about who we are.

150
00:19:24,000 --> 00:19:37,000
And this is how the Anusea, how our potentials, our potential for defilement is uprooted.

151
00:19:37,000 --> 00:19:51,000
This takes a lot more work and is a lot more difficult than simply getting rid of the defilements in the mind, which are momentary.

152
00:19:51,000 --> 00:19:59,000
So important to understand these three levels, to be able to differentiate and do not be lulled into a false sense of security.

153
00:19:59,000 --> 00:20:16,000
Not aim only to stop greed and anger and delusion from arising, but to aim to change who you are and to really understand deeply the nature of our habits, the nature of who we are.

154
00:20:16,000 --> 00:20:20,000
Break it down and

155
00:20:20,000 --> 00:20:37,000
transform this being whatever it is, using positive habit, forming the formation of good habits and wholesomeness in the mind.

156
00:20:37,000 --> 00:20:53,000
The practice of charity, the practice of morality, the practice of tranquility and insight meditation.

157
00:20:53,000 --> 00:20:56,000
And this is the most difficult task, of course.

158
00:20:56,000 --> 00:21:03,000
It doesn't end when you stop, when you achieve the state of no greed, anger and delusion.

159
00:21:03,000 --> 00:21:17,000
It continues until you have wisdom, until you have sufficient wisdom to prevent any arising of further greed, anger and delusion.

160
00:21:17,000 --> 00:21:22,000
And it's a big iceberg.

161
00:21:22,000 --> 00:21:28,000
So a little bit of thought on defilements.

162
00:21:28,000 --> 00:21:34,000
And so this person's question was why these things come back when they thought they were eradicated.

163
00:21:34,000 --> 00:21:35,000
Sort of.

164
00:21:35,000 --> 00:21:39,000
It was a little bit different, it's a little bit specific.

165
00:21:39,000 --> 00:21:44,000
But that's the thing.

166
00:21:44,000 --> 00:21:51,000
Don't rest content just because you're able to enter into meditative states.

167
00:21:51,000 --> 00:21:59,000
You really have to change quite a bit fundamental aspects of who we are.

168
00:21:59,000 --> 00:22:03,000
You have to dig deep.

169
00:22:03,000 --> 00:22:08,000
So there you go, there's the dhamma for tonight.

170
00:22:08,000 --> 00:22:18,000
There are questions, I'm happy to answer.

171
00:22:18,000 --> 00:22:47,000
Here we go, a few online questions here.

172
00:22:47,000 --> 00:22:55,000
Does withstanding psychical discomfort, such as taking a cold shower, aid in meditation practice?

173
00:22:55,000 --> 00:23:08,000
No, because not likely because it includes the intentional causing of suffering.

174
00:23:08,000 --> 00:23:17,000
And so it's counterproductive because you're creating the intention to cause suffering to yourself.

175
00:23:17,000 --> 00:23:20,000
It's caused by impatience.

176
00:23:20,000 --> 00:23:28,000
Even asking this question is a sign that you want to push the process along somehow, to aid the process.

177
00:23:28,000 --> 00:23:31,000
There's no aiding meditation practice, you're either meditator, you don't.

178
00:23:31,000 --> 00:23:43,000
And if you're doing something else, you're not meditating.

179
00:23:43,000 --> 00:23:50,000
I was thinking of doing a mock retreat, tried meditating intensively, but got very disturbed.

180
00:23:50,000 --> 00:23:59,000
Well, you shouldn't really do intensive meditation.

181
00:23:59,000 --> 00:24:10,000
You shouldn't really do meditation, intensive meditation without the new comer, without the support of a teacher.

182
00:24:10,000 --> 00:24:13,000
It can be dangerous.

183
00:24:13,000 --> 00:24:18,000
Usually it'll just, as you say, you'll get disturbed and you'll just stop.

184
00:24:18,000 --> 00:24:28,000
That's the most common thing, but it's not particularly beneficial without a teacher to support you.

185
00:24:28,000 --> 00:24:38,000
I mean, as a beginner, because you're trying to go through a forest and you've never been in the forest,

186
00:24:38,000 --> 00:24:49,000
you're not familiar with the landscape, let alone knowing the way out.

187
00:24:49,000 --> 00:25:03,000
Once you've gone through intensive meditation through with a teacher, you'll have a much better layout of the land, and then it's easy to go through it again.

188
00:25:03,000 --> 00:25:08,000
What do you think about an approach of avoiding bad and good?

189
00:25:08,000 --> 00:25:20,000
No, and the good will come because of the effort to avoid the bad.

190
00:25:20,000 --> 00:25:25,000
It seems that when I focus on avoiding bad, good comes out automatically.

191
00:25:25,000 --> 00:25:41,000
Avoiding, speaking of tonight's talk, this is an example of a way to free your mind potentially temporarily of defilements, so you can avoid bad.

192
00:25:41,000 --> 00:25:48,000
But it's a good example of what won't work in the long run to actually change your habits because it's avoiding.

193
00:25:48,000 --> 00:25:52,000
Avoiding isn't sustainable.

194
00:25:52,000 --> 00:25:59,000
You have to work and work, it's some kind of okay, you have to work and work to avoid the evil.

195
00:25:59,000 --> 00:26:13,000
And all you do is build a habit of avoiding, and then when you stop, whatever you do to avoid unholesiveness,

196
00:26:13,000 --> 00:26:16,000
it was eventually going to come to an end, and then it's going to come back.

197
00:26:16,000 --> 00:26:20,000
So here's a good example of this person who asked me this question.

198
00:26:20,000 --> 00:26:24,000
Not the question just now, but the earlier question.

199
00:26:24,000 --> 00:26:31,000
They were doing what they could avoid engaging in certain addictive activities,

200
00:26:31,000 --> 00:26:46,000
and then eventually they just lost their effort that it took to stop, and suddenly they're engaging in those old activities again.

201
00:26:46,000 --> 00:26:50,000
Avoiding is not the answer, avoiding is not the way end of suffering.

202
00:26:50,000 --> 00:26:57,000
Four noble truths, that's what the four noble truths they answer this question.

203
00:26:57,000 --> 00:27:05,000
Suffering is to be fully understood that is the path.

204
00:27:05,000 --> 00:27:11,000
And when you fully understand suffering, then you don't have to avoid craving or unholesiveness.

205
00:27:11,000 --> 00:27:26,000
You abandon it as a matter of course, not truly.

206
00:27:26,000 --> 00:27:29,000
And that's all our questions then.

207
00:27:29,000 --> 00:27:35,000
So thank you again for coming out.

208
00:27:35,000 --> 00:27:45,000
Wishing you all a good night.

