I need each other, can't just work on morality in isolation.
The other one's all far.
Well, that's really important, I think,
and it points to what true morality is.
One someone had a comment on my video for children,
because it was just teaching meditation
with no context of morality or even wisdom, as he said.
But I think that's really missing the point of morality
and wisdom, and how they come together.
True morality isn't intellectual,
it doesn't come from taking on precepts,
or really it doesn't come from working.
It comes from enlightenment.
Morality comes through meditation.
The beginning of meditation is morality.
That's true morality.
Morality isn't taking precepts.
It isn't even refusing to do something
when you have the opportunity to do it.
It's, sorry, at least not in the sense of saying,
I'm not going to kill him when the opportunity comes to kill.
It's much deeper than that.
True morality is a mind state that refrains.
So this applies to all states that become distracted.
Morality is the development of concentration directly.
It's the focusing of the mind.
And that's why morality is said to lead to concentration.
Things that are immoral, they're all immoral,
and as a result, there's so many immoral things out there
that we don't generally consider immoral.
But they're immoral because they are diffusing the mind.
They're creating chaos in the mind.
Anything that makes the mind more chaotic
is for that reason immoral,
and that, therefore, is the definition of immorality
in a Buddhist sense.
It's totally utilitarian.
So something is immoral because of its effect on one's mind.
But it's also absolute because any number of things,
anything that we would normally consider immoral,
has that effect on the mind of killing
has that effect on the mind.
Stealing has useless speech has that effect on the mind.
It does the mind in it diffuses the mind.
It creates chaos and disorder in the mind.
So in that sense, working on morality and isolation
is a misunderstanding of what morality is.
Morality is meditation.
When you sit down to practice meditation,
that's when true morality arises.
Yeah, you should keep precepts.
You should know what is right and what is wrong.
But that's only a part of the framework
that is going to allow you to approach meditation
in the right way.
Once you know what sorts of things are immoral,
then you know the sorts of things
that are going to create diffusion in the mind,
that are going to cause distraction and mental upset.
That's all because the precepts are just a guide.
They aren't anything real, they're just concepts.
I mean, given that there's no beings, right?
Killing isn't, there's nothing wrong with killing
because you can't kill someone.
There's no being, there's no one to kill, right?
This is one way, this is a view in the Buddhist time.
When someone kills, there is no killing.
It's just, the knife goes through parts, atoms,
which is what this one teacher said.
The Buddha denied this because of the nature of the mind.
The mind is involved.
Yeah, the person, what's wrong with killing
isn't that the person dies?
What I'm a killing is that you've created a horrible intention
in your mind and that totally disrupts your own experience.
It's a very powerful thing to kill someone,
but not because of what it does to them.
Because of what it does to you.
And it goes beyond what we realize.
It's only when you meditate if you've killed before.
Once you meditate, you'll know what I'm talking about.
Because you really do feel it.
So to talk about, to get back to the original comment
of how difficult it might be to be moral without meditation.
Be careful that you're not trying to be moral too hard.
Because true, good to try, in some sense,
to keep the precepts, but much better to use them as a guide
and really gain true realization that these things are wrong
so that they just won't arise.
You just won't have wrong speech.
You just won't lie.
You won't have harsh speech.
Say bad things about it.
You won't have divisive speech trying
to break people apart and so on.
You give up these kinds of behaviors naturally.
And that's where it's tied into concentration in wisdom
because wisdom actually leads back to morality.
It's a circle.
Once you have wisdom, that's what brings true morality.
