1
00:00:00,000 --> 00:00:28,640
Okay, good evening everyone, welcome back to our live broadcast evening

2
00:00:28,640 --> 00:00:43,640
and I'm going to continue on from last night, I thought we'd move on to the Buddha's second discourse.

3
00:00:43,640 --> 00:01:01,640
When learning about the Buddha, it's quite common for the story to end at his enlightenment.

4
00:01:01,640 --> 00:01:11,640
Sometimes maybe end at his first discourse, but it's far more interesting what happened after his enlightenment

5
00:01:11,640 --> 00:01:16,640
and after his first discourse.

6
00:01:16,640 --> 00:01:24,640
When he first taught,

7
00:01:24,640 --> 00:01:30,640
the five ascetics were impressed, but only one of them became a Sotapana.

8
00:01:30,640 --> 00:01:36,640
Only one of them really understood what the Buddha was talking about.

9
00:01:36,640 --> 00:01:43,640
There's may have had some idea or some idea of how to put it into practice,

10
00:01:43,640 --> 00:01:49,640
but they weren't able to put it into practice and understand it

11
00:01:49,640 --> 00:01:54,640
and realize the teaching for themselves while the Buddha was teaching.

12
00:01:54,640 --> 00:02:01,640
So the Buddha had to stay with them and he stayed with them for five days,

13
00:02:01,640 --> 00:02:24,640
working with them individually, addressing the individual challenges.

14
00:02:24,640 --> 00:02:53,640
And helping through their meditation until all five of them became Sotapana.

15
00:02:53,640 --> 00:03:03,640
And which went to Buddha taught the second discourse.

16
00:03:03,640 --> 00:03:12,640
So here was the same audience, but now they were in a position to really understand the Buddha's teaching on a deeper level.

17
00:03:12,640 --> 00:03:23,640
And so something that tells us is that this teaching is not really a beginner teaching.

18
00:03:23,640 --> 00:03:33,640
If you really want to understand the second discourse of the Buddha, you really have to have done some intensive meditation,

19
00:03:33,640 --> 00:03:59,640
have taken the time to really get a clear understanding of reality.

20
00:03:59,640 --> 00:04:07,640
So for those of you who are here, practicing intensive, they would be quite useful, I think.

21
00:04:07,640 --> 00:04:11,640
For those of you at home, well, for some of you, you've been practicing for some time.

22
00:04:11,640 --> 00:04:21,640
For those of you who may be new to the teaching, I'll try to give a sort of a more basic and easy to understand introductory teaching.

23
00:04:21,640 --> 00:04:33,640
We'll use this, it's still a good basis for understanding the Buddha's idea of meditation.

24
00:04:33,640 --> 00:04:36,640
So the second discourse is about the five aggregates.

25
00:04:36,640 --> 00:04:42,640
For many of you, I'm sure, familiar with this idea.

26
00:04:42,640 --> 00:04:54,640
This set of concepts that the Buddha laid out.

27
00:04:54,640 --> 00:05:06,640
And so that the idea behind this suta is not to address the question of whether there is a self or there isn't a self.

28
00:05:06,640 --> 00:05:12,640
Because that question is not useful.

29
00:05:12,640 --> 00:05:19,640
It's not actually an answerable question.

30
00:05:19,640 --> 00:05:24,640
It's just like asking, is there a God or isn't there a God?

31
00:05:24,640 --> 00:05:38,640
It's perhaps even worse because with God ideally, theoretically, you could have some contact with God.

32
00:05:38,640 --> 00:05:50,640
God might speak to you or do something, but the self really is meaningless.

33
00:05:50,640 --> 00:05:57,640
But if I were to tell you that there is a self, what would that mean?

34
00:05:57,640 --> 00:06:03,640
Is there any way for you to verify whether there is a self or not?

35
00:06:03,640 --> 00:06:05,640
It's always just going to be your belief.

36
00:06:05,640 --> 00:06:08,640
It's always just going to be your belief.

37
00:06:08,640 --> 00:06:13,640
If I tell you there isn't a self, when are you going to do with that either?

38
00:06:13,640 --> 00:06:19,640
Is there a way for you to understand that there is no self, to verify that there is no self?

39
00:06:19,640 --> 00:06:23,640
Just because you haven't seen one, just because you haven't found one right?

40
00:06:23,640 --> 00:06:25,640
Can never be something.

41
00:06:25,640 --> 00:06:31,640
It's not a question that's amenable to understanding and verification.

42
00:06:31,640 --> 00:06:38,640
The self is not a falsifiable claim, so it's not a scientific claim.

43
00:06:38,640 --> 00:06:46,640
Meaning you can't verify or deny it.

44
00:06:46,640 --> 00:06:55,640
So we don't ask that question, simply not a useful question.

45
00:06:55,640 --> 00:07:03,640
Instead, we look at what is there, and we try to understand it.

46
00:07:03,640 --> 00:07:10,640
And one of the very important ways of understanding the things that do exist, that we can experience,

47
00:07:10,640 --> 00:07:18,640
that we can verify as coming into being, is whether they are self.

48
00:07:18,640 --> 00:07:26,640
There's any reason to think of them as self, because whether there is a self or not,

49
00:07:26,640 --> 00:07:38,640
it is an important, but we do cling to many, many things as me, mine, under my control.

50
00:07:38,640 --> 00:07:43,640
Without even thinking about, hey, this is me, this is mine.

51
00:07:43,640 --> 00:08:00,640
We interact with them in a way that makes them in a way that implies or has an implicit belief or conception of them as self.

52
00:08:00,640 --> 00:08:09,640
And so the first thing that Buddha does is make this jump from me and mine and the self and the being to our experiences.

53
00:08:09,640 --> 00:08:16,640
I mean, he's dealing with people who have begun to understand experience, so they're familiar with this.

54
00:08:16,640 --> 00:08:24,640
When he asks form, when he says about form, Rupa, it says form is not self.

55
00:08:24,640 --> 00:08:27,640
And these monks have been meditating on form.

56
00:08:27,640 --> 00:08:34,640
They've been meditating on the movements of their bodies and the movements of the body during the breath and so on.

57
00:08:34,640 --> 00:08:37,640
They've been meditating on the heat and the cold.

58
00:08:37,640 --> 00:08:45,640
All of this is the physical, the form aspect of experience.

59
00:08:45,640 --> 00:09:08,640
And we take form to be self instead of experiencing it as moments of heat and cold and hardness and softness and so on.

60
00:09:08,640 --> 00:09:15,640
My experience is that it has our body, this is a body, my body, yes.

61
00:09:15,640 --> 00:09:22,640
All of these experiences when they come together, this is me, this is mine, this I am.

62
00:09:22,640 --> 00:09:26,640
So we have this conception.

63
00:09:26,640 --> 00:09:37,640
When we look at the body, when we look at what's really there, we see experiences that arise in seeds and these experiences are not me or not mine.

64
00:09:37,640 --> 00:09:41,640
How do we know this? Because they're not amenable to our control.

65
00:09:41,640 --> 00:09:45,640
They cause affliction.

66
00:09:45,640 --> 00:09:54,640
If something were truly me and mine, then you should be able to say, hey, let it be this, let it not be this.

67
00:09:54,640 --> 00:10:00,640
We should be able to say, I don't want to be cold, I'm not going to be cold.

68
00:10:00,640 --> 00:10:06,640
I should be able to say, I don't want to be short, I want to be tall, I don't want to be fat, I want to be thin.

69
00:10:06,640 --> 00:10:16,640
I just probably really, I don't want to get into it.

70
00:10:16,640 --> 00:10:33,640
I mean, it's a problem with attachment to the body and attachment to our body because it's not something that actually exists and we get really hung up on it so many ways.

71
00:10:33,640 --> 00:10:43,640
The second one is feelings.

72
00:10:43,640 --> 00:10:56,640
Feelings, we're very much attached to our feelings, we have pain and it's my pain.

73
00:10:56,640 --> 00:11:04,640
I tell you, the pain is not yours, that's not really how we look at pain.

74
00:11:04,640 --> 00:11:09,640
You might think I'm crazy because it's not you that's feeling my pain.

75
00:11:09,640 --> 00:11:16,640
I'm the one who's feeling it and I feel it and boy does it hurt.

76
00:11:16,640 --> 00:11:25,640
It's remarkable that the pain isn't actually self, it's remarkable when you do start to understand this and see how there's really no reason to be upset

77
00:11:25,640 --> 00:11:29,640
and about it, it's not self.

78
00:11:29,640 --> 00:11:41,640
There's no connection to a self there, the experience arises and ceases, that's it.

79
00:11:41,640 --> 00:11:51,640
We can't control our feelings, we're not in charge of them and this is how we live our lives, running away from pain, running towards pleasure,

80
00:11:51,640 --> 00:12:02,640
grasping and clinging and holding on for dear life to all the comfortable and pleasant feelings.

81
00:12:02,640 --> 00:12:10,640
And as a result, becoming less and less satisfied, less and less happy because

82
00:12:10,640 --> 00:12:19,640
we can't get what we want because we need and we yearn for it.

83
00:12:19,640 --> 00:12:29,640
And we become less and less flexible, less and less adaptive to the change of feelings.

84
00:12:29,640 --> 00:12:37,640
The third is sunyas or sunyas also non-self, sunyas causes affliction, sunyas memory or recognition

85
00:12:37,640 --> 00:12:46,640
when you remember things in the past or when you recognize things in the present.

86
00:12:46,640 --> 00:12:52,640
And a person who's afraid of spiders, these spiders give a simple example.

87
00:12:52,640 --> 00:13:09,640
And recognize the spiders when we have memories of things that happened in the past

88
00:13:09,640 --> 00:13:23,640
we're very much attached to those memories, good ones, bad ones.

89
00:13:23,640 --> 00:13:30,640
But we can't only have the good memories, we can't be free from the bad memories.

90
00:13:30,640 --> 00:13:41,640
Again, this is very much about escaping our situations, our experiences,

91
00:13:41,640 --> 00:13:45,640
but learning how to see them for what they are.

92
00:13:45,640 --> 00:13:50,640
Non-self just means it is what it is.

93
00:13:50,640 --> 00:13:59,640
It's this powerful change of perception from clinging and identifying with experiences

94
00:13:59,640 --> 00:14:06,640
and just seeing them for what they are, just observing them, just being here and now

95
00:14:06,640 --> 00:14:18,640
without judgment, without expectation.

96
00:14:18,640 --> 00:14:20,640
So that's sunya.

97
00:14:20,640 --> 00:14:30,640
The fourth one is sunkara.

98
00:14:30,640 --> 00:14:31,640
Sunkara's reaction is the real where the real problem comes in.

99
00:14:31,640 --> 00:14:33,640
Sunkara is when you judge something.

100
00:14:33,640 --> 00:14:43,640
It's a lot of different things, but most prominently, it's when you like or dislike something.

101
00:14:43,640 --> 00:14:47,640
And we think of our likes and dislikes as I like this, I like that.

102
00:14:47,640 --> 00:14:58,640
So much of our perceptions and all of these conceptions of me and mine are because we've just never looked.

103
00:14:58,640 --> 00:15:16,640
We've just assumed, we've just been told, we use language and our ordinary way of looking at things is me, mine, self.

104
00:15:16,640 --> 00:15:27,640
But when we look, if I ask the meditators here, is liking self?

105
00:15:27,640 --> 00:15:31,640
I mean, are you in charge of the liking?

106
00:15:31,640 --> 00:15:38,640
Do you only like when you want to like want when you want to want?

107
00:15:38,640 --> 00:15:41,640
What about disliking? Are you in charge of that?

108
00:15:41,640 --> 00:15:46,640
Okay, I'm going to dislike this. No, I'm not going to dislike this.

109
00:15:46,640 --> 00:15:53,640
There's no switch to turn it off and on. It's not me, it's not mine. I don't have this power.

110
00:15:53,640 --> 00:16:02,640
It's quite alarming as a meditator to realize that our emotions are not under our control.

111
00:16:02,640 --> 00:16:07,640
That we're not in charge of them.

112
00:16:07,640 --> 00:16:14,640
And the fifth one is Vinyana. So altogether, these make up who we are, really.

113
00:16:14,640 --> 00:16:18,640
I mean, they're the five aspects of experience, really.

114
00:16:18,640 --> 00:16:23,640
The fifth one, Vinyana means the experience itself, consciousness.

115
00:16:23,640 --> 00:16:29,640
When you see something and you're conscious of it, there's all these five things involved,

116
00:16:29,640 --> 00:16:33,640
but the fifth one is the actual awareness of it.

117
00:16:33,640 --> 00:16:38,640
And so you can't say, let me be aware of this. Let me not be aware of that.

118
00:16:38,640 --> 00:16:42,640
Right? Someone's yelling and making noise.

119
00:16:42,640 --> 00:16:50,640
Well, I don't want to be aware of that. Let me not have to experience that.

120
00:16:50,640 --> 00:17:00,640
If I bagger get to like an affliction, they're like a torture device or a prison.

121
00:17:00,640 --> 00:17:07,640
I mean, that's fairly negative, I suppose, but there's something that's imposed upon us in a sense.

122
00:17:07,640 --> 00:17:16,640
The deeper sense we impose them on ourselves, but nonetheless, we have no choice in the matter.

123
00:17:16,640 --> 00:17:29,640
We're not in charge. We're not able to turn on and turn off our experiences.

124
00:17:29,640 --> 00:17:38,640
We learn that these experiences, these aspects, or these aspects of reality.

125
00:17:38,640 --> 00:17:46,640
We learn that there are simply things that arise and see if they're not anything to do with me or mine.

126
00:17:46,640 --> 00:17:51,640
So he explains this to them and gives them a one-way teaching.

127
00:17:51,640 --> 00:18:01,640
And then he does something to take it to the next level and to the sense where maybe they accept until actually what he's saying,

128
00:18:01,640 --> 00:18:06,640
but it's still not very clear to them. It's still something that they're struggling with.

129
00:18:06,640 --> 00:18:11,640
And so he tries to make it clear. And this is where he starts to bring up the three characteristics, right?

130
00:18:11,640 --> 00:18:16,640
So non-self appears to be most important. He's talked about suffering in the last one,

131
00:18:16,640 --> 00:18:22,640
but now he's focusing in non-self because that's really the key.

132
00:18:22,640 --> 00:18:33,640
Seeing experiences arising and seeing is not having any substance or identity.

133
00:18:33,640 --> 00:18:36,640
Now he says, well, how we understand non-self?

134
00:18:36,640 --> 00:18:48,640
So he asks, is form predictable or constant or is it unpredictable and un-constant?

135
00:18:48,640 --> 00:18:50,640
Is it certain or uncertain? So he asks,

136
00:18:50,640 --> 00:19:17,640
when he says, so if it's impermanent, well, then it is, is it,

137
00:19:17,640 --> 00:19:22,640
is it something that satisfies you or doesn't satisfy your meaning?

138
00:19:22,640 --> 00:19:29,640
Can you fix it? Can you keep it the way you want

139
00:19:29,640 --> 00:19:34,640
and prevent it from being the way you don't want?

140
00:19:34,640 --> 00:19:38,640
And they say, no, it's dukang bante.

141
00:19:38,640 --> 00:19:48,640
And he gets unsatisfying. It's stressful in fact. If you look at your form and watch your experiences,

142
00:19:48,640 --> 00:19:52,640
you'll see that there's nothing about it that satisfying.

143
00:19:52,640 --> 00:19:58,640
There's nothing about it that you can cling to.

144
00:19:58,640 --> 00:20:02,640
Even just watching your stomach. It's a good example because we watch our stomach

145
00:20:02,640 --> 00:20:05,640
and have this expectation that somehow it's going to be the way we want.

146
00:20:05,640 --> 00:20:10,640
It's going to be smooth. I always think something's wrong.

147
00:20:10,640 --> 00:20:15,640
I can't meditate. I'm not a good meditator because my stomach is not rising and falling smoothly

148
00:20:15,640 --> 00:20:19,640
the way it should, right?

149
00:20:19,640 --> 00:20:25,640
The way it should because we want it to, therefore it should.

150
00:20:25,640 --> 00:20:30,640
So what's wrong really is that it's not self.

151
00:20:30,640 --> 00:20:33,640
And when you see that, the Buddha said, well, then you should do regard it.

152
00:20:33,640 --> 00:20:38,640
If you see it changing like this, changing by itself, you see that you can't fix it

153
00:20:38,640 --> 00:20:41,640
and that it's not.

154
00:20:41,640 --> 00:20:46,640
It's only a cause of disappointment when you try to make things right.

155
00:20:46,640 --> 00:20:48,640
Then should you say that it's me in mind?

156
00:20:48,640 --> 00:20:52,640
Is it proper to say that form is me in mind?

157
00:20:52,640 --> 00:20:58,640
No hate dung bante. No indeed, venerable sir.

158
00:20:58,640 --> 00:21:03,640
At least it's the same for feelings.

159
00:21:03,640 --> 00:21:06,640
When feelings are not amenable to our control, we want to be happy.

160
00:21:06,640 --> 00:21:11,640
We feel pain. Try to get rid of the pain. It doesn't go away.

161
00:21:11,640 --> 00:21:17,640
Try to hold on to the happiness. It does go away.

162
00:21:17,640 --> 00:21:21,640
Start to see that reality is just there's an order to it.

163
00:21:21,640 --> 00:21:26,640
And there are ways of seeking and attaining happiness, but you can't control it.

164
00:21:26,640 --> 00:21:30,640
You can hold on to that happiness and say, okay, now stay.

165
00:21:30,640 --> 00:21:34,640
There's quite clearly a reason for the happiness.

166
00:21:34,640 --> 00:21:38,640
Maybe you sought out something that brought you pleasure.

167
00:21:38,640 --> 00:21:42,640
That's it. Cause and effect.

168
00:21:42,640 --> 00:21:46,640
It doesn't stay. It doesn't last.

169
00:21:46,640 --> 00:21:50,640
It's not under your control.

170
00:21:50,640 --> 00:21:56,640
So he said, feelings are impermanent. And if they're impermanent, then they're unsatisfying.

171
00:21:56,640 --> 00:21:59,640
You can't cling to them.

172
00:21:59,640 --> 00:22:02,640
You just suffer when you cling to them.

173
00:22:02,640 --> 00:22:08,640
And so they too should not be, should not be clung to itself.

174
00:22:08,640 --> 00:22:13,640
Naitang mama naiso ha must be nam naiso meyatati.

175
00:22:13,640 --> 00:22:19,640
This is not me. This is not mine. This is not myself.

176
00:22:19,640 --> 00:22:27,640
And memories, we shouldn't cling to memories.

177
00:22:27,640 --> 00:22:29,640
So he's asking them, he's asking them,

178
00:22:29,640 --> 00:22:34,640
are memories certain or uncertain predictable or unpredictable?

179
00:22:34,640 --> 00:22:46,640
Oh no, they're unpredictable. You can't be in charge of what you remember and what you don't, what you conceive and what you don't.

180
00:22:46,640 --> 00:22:57,640
So he says they should also be seen as not self.

181
00:22:57,640 --> 00:23:02,640
Our emotions, our consciousness, even our consciousness.

182
00:23:02,640 --> 00:23:06,640
We don't choose what we're conscious of. It's unpredictable.

183
00:23:06,640 --> 00:23:11,640
It's unpredictable. It's unsatisfying.

184
00:23:11,640 --> 00:23:14,640
Meaning happiness can't come from any of these.

185
00:23:14,640 --> 00:23:17,640
It can be our refuge. Our experiences can't be our refuge.

186
00:23:17,640 --> 00:23:20,640
There has to be something beyond that.

187
00:23:20,640 --> 00:23:23,640
In fact, the knowledge of non-self is the greatest refuge.

188
00:23:23,640 --> 00:23:29,640
Once you stop clinging to experience, it doesn't mean you stop experiencing.

189
00:23:29,640 --> 00:23:38,640
But you stop diversifying or you stop extrapolating upon experience.

190
00:23:38,640 --> 00:23:40,640
Experience just is experience.

191
00:23:40,640 --> 00:23:48,640
That's what it is. The whole idea that it might be me or mine and the whole concept of trying to control and trying to be in charge.

192
00:23:48,640 --> 00:23:51,640
That's all on us. That's not real.

193
00:23:51,640 --> 00:23:54,640
It's not intrinsic to reality and this is very important.

194
00:23:54,640 --> 00:24:03,640
The idea of what is intrinsic to reality and what is just our own belief view.

195
00:24:03,640 --> 00:24:09,640
As we've gotten to the point where our beliefs and our views have a life of their own and they take on importance.

196
00:24:09,640 --> 00:24:11,640
Which they shouldn't.

197
00:24:11,640 --> 00:24:21,640
I believe a view should have no importance beyond its basis in our experience and our ability to verify it.

198
00:24:21,640 --> 00:24:39,640
So he says, all form, all feeling, all perception, all emotion, all consciousness.

199
00:24:39,640 --> 00:24:48,640
Past future, present, gross subtle, internal, external, inferior, superior,

200
00:24:48,640 --> 00:24:57,640
far near. You should see it all with wisdom, with right understanding.

201
00:24:57,640 --> 00:25:01,640
Neetang mama, this is not mine.

202
00:25:01,640 --> 00:25:04,640
Neetang mama, this is not mine.

203
00:25:04,640 --> 00:25:07,640
Neetang mama, I am not this.

204
00:25:07,640 --> 00:25:17,640
Neetang mama, this is not me. This is not myself.

205
00:25:17,640 --> 00:25:23,640
He says, when a person sees this, this is about actually meditating.

206
00:25:23,640 --> 00:25:28,640
It's quite clear what we're talking about here is looking at your experience,

207
00:25:28,640 --> 00:25:38,640
watching it arise and seeing for yourself the impermanence and therefore the inability to satisfy.

208
00:25:38,640 --> 00:25:47,640
I mean, can't be the inability to be a refuge for us.

209
00:25:47,640 --> 00:25:53,640
And therefore, non-self, when we stop looking at things as this is my feeling,

210
00:25:53,640 --> 00:26:03,640
pour me with this pain or pour me with this thought or pour me with this experience.

211
00:26:03,640 --> 00:26:08,640
And likewise, feeling good about ourselves for having this experience,

212
00:26:08,640 --> 00:26:14,640
for being like this, for being like that ego and conceit and all that when we get rid of all that.

213
00:26:14,640 --> 00:26:25,640
I mean, just experience things as they are.

214
00:26:25,640 --> 00:26:36,640
We let go. So we have this idea, this concept of nimida, nimida is a very important turning point in a meditator's practice.

215
00:26:36,640 --> 00:26:41,640
So you get to the point where you begin to shift.

216
00:26:41,640 --> 00:26:47,640
Whereas your whole being and your whole inclination is finding happiness in experiences,

217
00:26:47,640 --> 00:26:52,640
getting the happy experiences, avoiding the unhappy one.

218
00:26:52,640 --> 00:26:57,640
You have this sense of dispassion or you lose your passion, you lose your ambition.

219
00:26:57,640 --> 00:27:03,640
It starts to drain away, mainly because in the meditation you'll hit your head against the wall,

220
00:27:03,640 --> 00:27:10,640
figuratively speaking again and again and again, trying and trying and trying to fix things.

221
00:27:10,640 --> 00:27:16,640
Until you start to realize, I'm not going to fix this. This is unfixable.

222
00:27:16,640 --> 00:27:22,640
This is uncontrollable. And it isn't even me.

223
00:27:22,640 --> 00:27:31,640
And you start to let it go, nimida, nimida, nimida, nimida becomes disenchanted with form,

224
00:27:31,640 --> 00:27:35,640
wade and us, wade and us, wade and nimida.

225
00:27:35,640 --> 00:27:45,640
When it becomes disenchanted, in regards to sunya, sunkara, nimiana.

226
00:27:45,640 --> 00:27:51,640
And this is the turning point because one begins to find real peace.

227
00:27:51,640 --> 00:27:57,640
One begins to be at peace with oneself, be at peace with reality,

228
00:27:57,640 --> 00:28:04,640
without having to change it, having to fix it, having to control it.

229
00:28:04,640 --> 00:28:08,640
With the fading of passion, one is liberated.

230
00:28:08,640 --> 00:28:16,640
When it becomes disenchanted, it becomes disenchanted.

231
00:28:16,640 --> 00:28:25,640
With liberation, with freedom, there is the knowledge I'm free.

232
00:28:25,640 --> 00:28:40,640
We see tung brahmacharyang, kina jati, we see tung brahmacharyang, katang karanyang.

233
00:28:40,640 --> 00:28:44,640
There is an understanding that, yes, I found the right way.

234
00:28:44,640 --> 00:28:49,640
There is a realization that you have found the true way out of suffering, which is quite simple.

235
00:28:49,640 --> 00:28:54,640
It means to really just be here now to stop judging, to stop reacting,

236
00:28:54,640 --> 00:29:00,640
to stop piling on baggage to all of our experiences,

237
00:29:00,640 --> 00:29:10,640
to learn how to just experience them and learn why, why it's,

238
00:29:10,640 --> 00:29:23,640
well, why we suffer, to learn the difference between the way we cling to things

239
00:29:23,640 --> 00:29:30,640
and to simply experience things as they are.

240
00:29:30,640 --> 00:29:34,640
Then we know we've done what needs to be done.

241
00:29:34,640 --> 00:29:40,640
When the Buddha gave this discourse, these five ascetics became aran.

242
00:29:40,640 --> 00:29:45,640
It became liberated from the taints, through clinging no more.

243
00:29:45,640 --> 00:29:49,640
They were practicing while he was teaching this, and they were really ready.

244
00:29:49,640 --> 00:29:58,640
These guys had been living in the forest for years, developing strong concentration.

245
00:29:58,640 --> 00:30:04,640
During the time, those five days, they progressed through the stages of knowledge.

246
00:30:04,640 --> 00:30:10,640
When the Buddha gave this talk, they all were able to, over the course of the talk.

247
00:30:10,640 --> 00:30:12,640
Quite remarkable, I suppose.

248
00:30:12,640 --> 00:30:16,640
They were all able to become enlightened.

249
00:30:16,640 --> 00:30:24,640
For the rest of us, this gives you an idea of, especially those of you doing intensive meditation.

250
00:30:24,640 --> 00:30:28,640
It gives you an idea of where we're headed.

251
00:30:28,640 --> 00:30:32,640
There's many other stages of knowledge that we can delineate,

252
00:30:32,640 --> 00:30:37,640
but the most important is to see things, just see them as they are.

253
00:30:37,640 --> 00:30:39,640
They're not trying to become anything.

254
00:30:39,640 --> 00:30:44,640
They're really concerned with Buddhism or the Buddha.

255
00:30:44,640 --> 00:30:51,640
They're concerned with you and your reality, concerned with the reactions

256
00:30:51,640 --> 00:30:56,640
and the baggage that we carry around.

257
00:30:56,640 --> 00:31:00,640
That makes our experiences so much more than they need to be.

258
00:31:00,640 --> 00:31:10,640
It causes us so much more pain and suffering than is warranted by the experience.

259
00:31:10,640 --> 00:31:15,640
There you go a little bit about the second discourse,

260
00:31:15,640 --> 00:31:22,640
without getting into too much technical detail.

261
00:31:22,640 --> 00:31:29,640
Thank you all for coming up.

262
00:31:29,640 --> 00:31:34,640
If you have any questions, I can take some questions,

263
00:31:34,640 --> 00:31:44,640
but I'm rather particular about the questions I answer, so be aware.

264
00:31:44,640 --> 00:31:57,640
What if your mind doesn't wander and there is no struggle?

265
00:31:57,640 --> 00:31:59,640
Do you still need to meditate?

266
00:31:59,640 --> 00:32:04,640
A better question is, if you don't suffer, if you have no suffering,

267
00:32:04,640 --> 00:32:09,640
and if you have a clear awareness of reality, then no, you don't need to meditate.

268
00:32:09,640 --> 00:32:22,640
Is control important in the worldly functional sense,

269
00:32:22,640 --> 00:32:26,640
keeping a trained schedule for instance?

270
00:32:26,640 --> 00:32:31,640
Yes, I mean, it's not really control.

271
00:32:31,640 --> 00:32:38,640
It's just cause and effect and understanding what comes next.

272
00:32:38,640 --> 00:32:41,640
Okay, let's take the questions from the site.

273
00:32:41,640 --> 00:32:53,640
I guess I can have to go over there because I'm the one who has to turn them on and off.

274
00:32:53,640 --> 00:33:01,640
So, we prioritize, as I said, questions on our meditation site.

275
00:33:01,640 --> 00:33:08,640
Mondays, your recent TEDx talk available to you online. They said it'd be up in a couple months.

276
00:33:08,640 --> 00:33:17,640
Now, keep in mind that I was sick, and I hadn't had time to prepare for it, so you'll see.

277
00:33:17,640 --> 00:33:24,640
I mean, I think it starts off a little bit wonky, but I think I think it was okay.

278
00:33:24,640 --> 00:33:30,640
So, it will be online, and I think there's something to it that's useful.

279
00:33:30,640 --> 00:33:35,640
If they decide to publish it, I'm not sure if they publish them all, but they said a couple of months,

280
00:33:35,640 --> 00:33:40,640
so it was in March 5th or something, right?

281
00:33:40,640 --> 00:33:44,640
So, no, probably not up yet.

282
00:33:44,640 --> 00:33:51,640
And they said they let us know, so I haven't heard anything yet.

283
00:33:51,640 --> 00:33:55,640
Online meditation course sign up. It's in the menu of this app.

284
00:33:55,640 --> 00:34:01,640
Hopefully someone talked to you over in chat, but if you manage to post the question,

285
00:34:01,640 --> 00:34:08,640
it means you're on the right track. You have to go to the three bars in the top left of the screen,

286
00:34:08,640 --> 00:34:17,640
and check out the schedule. That's what you're looking for.

287
00:34:17,640 --> 00:34:22,640
How long do you suggest someone should meditate and how much experience what the teaching should one have

288
00:34:22,640 --> 00:34:26,640
before trying to ordain as a monk?

289
00:34:26,640 --> 00:34:29,640
Well, becoming a monk is a whole different issue.

290
00:34:29,640 --> 00:34:33,640
I mean, I don't have any real advice for that.

291
00:34:33,640 --> 00:34:38,640
I think it's very much something you should talk to with the person who might ordain you.

292
00:34:38,640 --> 00:34:48,640
So, I think it's probably better off focusing on community,

293
00:34:48,640 --> 00:34:54,640
and becoming a monk will just be a part of the process of getting into that community.

294
00:34:54,640 --> 00:34:57,640
Rather than just saying, I want to become a monk, I'm going to go to Asia,

295
00:34:57,640 --> 00:35:01,640
because it's not really going to be the way you think it is.

296
00:35:01,640 --> 00:35:06,640
I mean, unless you get really lucky, you're most likely just to fall in with a bunch of,

297
00:35:06,640 --> 00:35:09,640
who knows what you've fallen with.

298
00:35:09,640 --> 00:35:12,640
It's not really how it works.

299
00:35:12,640 --> 00:35:16,640
Much better to find Buddhism in a community.

300
00:35:16,640 --> 00:35:23,640
And then, consider ordaining through that.

301
00:35:28,640 --> 00:35:30,640
Those are not my topics.

302
00:35:30,640 --> 00:35:33,640
So, that question is going.

303
00:35:33,640 --> 00:35:36,640
Okay, those are the ones on the site.

304
00:35:36,640 --> 00:35:43,640
How are we doing over here?

305
00:35:43,640 --> 00:35:50,640
Well, that's cruel.

306
00:35:58,640 --> 00:36:03,640
Are mudras necessary? No, don't teach them.

307
00:36:03,640 --> 00:36:08,640
If there's nothing to note, then is the default object arising and falling in the stomach?

308
00:36:08,640 --> 00:36:14,640
Can we? Yeah, it's a good one.

309
00:36:14,640 --> 00:36:18,640
You can also just be that you're sitting or standing or walking or lying down,

310
00:36:18,640 --> 00:36:23,640
or you can just note nothing, nothing or knowing, knowing that there's nothing.

311
00:36:23,640 --> 00:36:34,640
If you feel calm, you can say calm, calm.

312
00:36:34,640 --> 00:36:38,640
All right, that's all the questions I'm willing to answer from there.

313
00:36:38,640 --> 00:36:41,640
How are we doing over here?

314
00:36:41,640 --> 00:36:43,640
Oh, Dara's copying them.

315
00:36:43,640 --> 00:36:44,640
Sunka has a question.

316
00:36:44,640 --> 00:36:48,640
If we define self as anything that is under our total control,

317
00:36:48,640 --> 00:36:51,640
can't we say that there is no self?

318
00:36:51,640 --> 00:36:56,640
Isn't the validity of any such statement dependent on the meaning we give to the word self?

319
00:36:56,640 --> 00:36:58,640
Oh, maybe.

320
00:36:58,640 --> 00:37:01,640
But if you're asking whether you can say that there is no soul,

321
00:37:01,640 --> 00:37:05,640
there is no soul. You missed the point of my talk.

322
00:37:05,640 --> 00:37:09,640
Such a question is useless. Such an answer is also useless.

323
00:37:09,640 --> 00:37:33,640
All right, I'm going to say that's all the questions for tonight then.

324
00:37:33,640 --> 00:37:38,640
I know I've ignored a couple, but you have to be clear that I'm not answering just any old questions.

325
00:37:38,640 --> 00:37:45,640
So I've answered some. Thank you all for coming out.

326
00:37:45,640 --> 00:38:09,640
See you all next time.

