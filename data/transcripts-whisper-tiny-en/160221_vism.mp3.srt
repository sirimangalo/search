1
00:00:00,000 --> 00:00:14,840
Okay, so we are on page 386, still in the super normal powers, and we're on number 80.

2
00:00:14,840 --> 00:00:16,960
And Aurora, can you start us off?

3
00:00:16,960 --> 00:00:18,320
Yes.

4
00:00:18,320 --> 00:00:26,320
For the more, in Ta-Bahpani Island, Sri Lanka, while the elder Dhamadina resident of

5
00:00:26,320 --> 00:00:33,120
Talangara was sitting on the shrine terrace in the great monastery of Tisa, expounding the

6
00:00:33,120 --> 00:00:39,600
Apanyankasuta, because when a beaker possesses three things, he enters upon the untarished

7
00:00:39,600 --> 00:00:40,600
way.

8
00:00:40,600 --> 00:00:45,960
He turns his fan face downwards, and an opening right down to Avichi appeared.

9
00:00:45,960 --> 00:00:52,840
Then he turns his face upwards, and then opening right up to the brahma world appear.

10
00:00:52,840 --> 00:00:57,760
Having thus aroused fear of hell and longing for the bliss of heaven, the elder thought

11
00:00:57,760 --> 00:00:59,480
the Dhamma.

12
00:00:59,480 --> 00:01:06,960
Some became stream enters, some once returns, some non-returners, some our hands.

13
00:01:06,960 --> 00:01:24,240
Hi, Danas, are you able to read 81?

14
00:01:24,240 --> 00:01:25,240
I'm not hearing you.

15
00:01:25,240 --> 00:01:29,440
I'm not sure if your mic is hooked up, if not, you might have to go into the configure

16
00:01:29,440 --> 00:01:31,040
and do the audio wizard.

17
00:01:31,040 --> 00:01:34,680
But for now we'll skip ahead to Fernando, and if you have it all set for next time, just

18
00:01:34,680 --> 00:01:40,520
jump right in next time around.

19
00:01:40,520 --> 00:01:44,800
But one who wants to cause a vanishing makes light into the earth.

20
00:01:44,800 --> 00:01:50,760
Oki hides forests, obedient, or he makes what has to come into the visual field, calm

21
00:01:50,760 --> 00:01:53,480
no more into the visual field.

22
00:01:53,480 --> 00:01:54,480
How?

23
00:01:54,480 --> 00:02:01,560
If you want to make himself for another invisible heaven, who on conceal, or nearby he emerges

24
00:02:01,560 --> 00:02:10,000
from the face of Jana, in a bird's dash, let this light become darkness, or let this

25
00:02:10,000 --> 00:02:13,320
that is overheating the human.

26
00:02:13,320 --> 00:02:20,680
Or let this that has come into the visual field, not come into the visual field.

27
00:02:20,680 --> 00:02:27,600
Then he does the preliminary work and results in the way already described.

28
00:02:27,600 --> 00:02:33,640
He becomes as he has results simultaneously with the resolution.

29
00:02:33,640 --> 00:02:37,360
Others will not see even when they are nearby.

30
00:02:37,360 --> 00:02:42,880
He too does not see if he does not want to see.

31
00:02:42,880 --> 00:02:49,880
But by whom was this miracle firmly performed by the blessed ones?

32
00:02:49,880 --> 00:02:58,000
Or the blessed ones selected that when the class man was sitting beside him, his father

33
00:02:58,000 --> 00:03:00,040
did not see him.

34
00:03:00,040 --> 00:03:10,200
Likewise, after traveling 2,000 leagues to meet Mahakapina and establishing him in the

35
00:03:10,200 --> 00:03:17,760
friction of no return and his 1,000 ministers in the friction of extreme entry.

36
00:03:17,760 --> 00:03:29,520
He so acted that Queen Annoyia, who had followed with the king with 1,000 woman attendants

37
00:03:29,520 --> 00:03:37,520
and was sitting nearby, did not see the king and he would retune you.

38
00:03:37,520 --> 00:03:46,320
And when he was asked, have you seen the king Bernell C. He asked, but which is better

39
00:03:46,320 --> 00:03:53,000
for you to seek the king or to seek the yourself?

40
00:03:53,000 --> 00:04:03,360
She replied, myself, Bernell C. Then he likewise taught her the dhamma as she said

41
00:04:03,360 --> 00:04:04,360
the.

42
00:04:04,360 --> 00:04:11,360
So that, together with the 1,000 woman attendants, she became established in the

43
00:04:11,360 --> 00:04:20,080
friction of extreme entry, while the ministers reached the friction of no return and the

44
00:04:20,080 --> 00:04:23,640
king that of iron shape.

45
00:04:23,640 --> 00:04:24,640
83.

46
00:04:24,640 --> 00:04:36,160
Furthermore, this was performed by the elder Mahan Mahinda, who so acted on the day of his arrival

47
00:04:36,160 --> 00:04:42,280
in Taba, Pani, Ireland, that the king did not see the others who had come to come with

48
00:04:42,280 --> 00:04:43,280
him.

49
00:04:43,280 --> 00:04:58,080
One day, it's still a little hard to hear you, you're very quiet, that's still really

50
00:04:58,080 --> 00:05:00,200
quiet compared to us.

51
00:05:00,200 --> 00:05:03,200
How about now?

52
00:05:03,200 --> 00:05:04,200
That's better.

53
00:05:04,200 --> 00:05:05,200
Thank you.

54
00:05:05,200 --> 00:05:07,480
Thank you, sir.

55
00:05:07,480 --> 00:05:11,600
Tambhupani, I think, refers to Sri Lanka.

56
00:05:11,600 --> 00:05:20,520
Other more, all miracles of making evident are called in appearance, and all miracles of

57
00:05:20,520 --> 00:05:23,680
making unevident are called of vanishing.

58
00:05:23,680 --> 00:05:28,280
Here in, in the miracle of making evident, both the super normal power and the possessor

59
00:05:28,280 --> 00:05:31,200
of the super normal power are displayed.

60
00:05:31,200 --> 00:05:36,480
That can be illustrated with the twin miracle, for in that both are displayed thus.

61
00:05:36,480 --> 00:05:41,080
Here the perfect one performs the twin miracle, which is not shared by disciples.

62
00:05:41,080 --> 00:05:45,720
He produces a mass of fire from the upper part of his body, and a shower of water from

63
00:05:45,720 --> 00:05:48,200
the lower part of his body.

64
00:05:48,200 --> 00:05:53,520
In the case of the miracle of making unevident, only the super normal power is displayed,

65
00:05:53,520 --> 00:05:56,600
not the possessor of the super normal power.

66
00:05:56,600 --> 00:06:04,440
That can be illustrated by means of the Mahakasuta, and the Brahman-himantanika-suta.

67
00:06:04,440 --> 00:06:08,800
For there it was only the super normal power of the venerable Mahaka, and of the blessed

68
00:06:08,800 --> 00:06:14,840
one respectively that was displayed, not the possessor of the super normal power, according

69
00:06:14,840 --> 00:06:19,320
as it is said.

70
00:06:19,320 --> 00:06:26,840
Then he had sat down at one side, the householder fitted fit to the venerable Mahaka.

71
00:06:26,840 --> 00:06:31,840
Venerable sir, it would be good if the Lord would show me a miracle of the super normal

72
00:06:31,840 --> 00:06:36,920
power belonging to the higher than him in state.

73
00:06:36,920 --> 00:06:42,520
Then householder spread your upper robe out on the terrace and scatter a bundle of hay

74
00:06:42,520 --> 00:06:43,520
on it.

75
00:06:43,520 --> 00:06:59,320
Then the venerable Mahaka went into his dwelling, and fastened the latch after which he performed

76
00:06:59,320 --> 00:07:07,440
a feat of super normal power, such that flames came out from the keyhole and from the

77
00:07:07,440 --> 00:07:17,240
gap in the fourth things and burned the hay without burning the upper robe.

78
00:07:17,240 --> 00:07:28,360
Also according as it is said then because I performed a feat of super normal power, such

79
00:07:28,360 --> 00:07:44,240
that Brahma and Brahma's re-tune and those attached to Brahma's re-tune my hair, my voice

80
00:07:44,240 --> 00:07:53,560
and yet not see me, and having vanished in this way I spoke this stanza.

81
00:07:53,560 --> 00:08:03,040
I saw the fear in all kinds of becoming, including becoming that six non-becoming, and

82
00:08:03,040 --> 00:08:19,800
no becoming do I recommend, I cling to no delight there in at all.

83
00:08:19,800 --> 00:08:27,160
He goes unhindered through walls, through enclosures, through mountains, as though in open space.

84
00:08:27,160 --> 00:08:33,280
Here through walls is beyond walls, the underside of a wall is what is meant.

85
00:08:33,280 --> 00:08:39,240
So with the rest, and wall is a term for the wall of a house, enclosure is a wall surrounding

86
00:08:39,240 --> 00:08:47,280
a house, monastery, village, etc., mountain is a mountain of soil or a mountain of stone.

87
00:08:47,280 --> 00:08:56,000
Dindered means not sticking, as though in open space means just as if he were in open space.

88
00:08:56,000 --> 00:09:07,320
88 wants to go and this way should attain the space casino in the merge, and then do

89
00:09:07,320 --> 00:09:14,160
the preliminary work by adverting to the wall or the enclosure or saw of some such mountain

90
00:09:14,160 --> 00:09:20,840
as scenario, or the world's fear of mountains, and he should resolve, let there be space.

91
00:09:20,840 --> 00:09:26,400
It becomes space only, it becomes hollow for him if he wants to go down or up.

92
00:09:26,400 --> 00:09:29,880
It becomes cluffed for him if he wants to penetrate it.

93
00:09:29,880 --> 00:09:34,520
He goes through it unhindered.

94
00:09:34,520 --> 00:09:41,800
What there they elder, Tipitaka, cooler, Abaya said, friends, what is the use of attaining

95
00:09:41,800 --> 00:09:49,760
the space casino, Jana, was one who wants to create elephants, horses, etc., attaining

96
00:09:49,760 --> 00:09:54,800
elephant casino, Jana, or horse casino, Jana, and so on.

97
00:09:54,800 --> 00:09:59,960
Surely the only standard is a mastery in the eight attains, and after the preliminary

98
00:09:59,960 --> 00:10:06,120
work has been done, on any casino, it then becomes whatever he wishes.

99
00:10:06,120 --> 00:10:14,440
The beaucoup said, when they will say, only the space casino has been given in the text,

100
00:10:14,440 --> 00:10:17,800
so it should certainly be mentioned.

101
00:10:17,800 --> 00:10:29,440
Here is the text, he is normally an optimer of the space casino, taking the efforts

102
00:10:29,440 --> 00:10:33,840
through the wall, through the enclosure, through the mountain.

103
00:10:33,840 --> 00:10:41,000
When averted, he results with knowledge, better with space, there is a space.

104
00:10:41,000 --> 00:10:46,280
He goes unhindered, through the wall, through the enclosure, through the mountain.

105
00:10:46,280 --> 00:10:51,840
He says men normally not possess lots of paranormal, powerful, unhindered, while there

106
00:10:51,840 --> 00:10:54,560
is no obstruction or any pressure.

107
00:10:54,560 --> 00:11:01,080
So to this processor of the paranormal power, by his attaining mental mastery, goes

108
00:11:01,080 --> 00:11:08,840
unhindered, through the wall, through the enclosure, through the mountain, adds a soul

109
00:11:08,840 --> 00:11:11,880
in open space.

110
00:11:11,880 --> 00:11:23,880
A quality for moon time or a tree is raised in this vehicle, way where he is not harmed

111
00:11:23,880 --> 00:11:24,880
in that.

112
00:11:24,880 --> 00:11:34,440
For attaining and resolving again is like taking the dependence in the perceptors presence.

113
00:11:34,440 --> 00:11:42,840
And because this vehicle has a salt, let there be a space, there will be only space there.

114
00:11:42,840 --> 00:11:49,320
And because of the power of his fierce result, it is impossible that another moon time

115
00:11:49,320 --> 00:11:58,720
or three can have sprung up meanwhile, made by temperature, however, if it has been created

116
00:11:58,720 --> 00:12:06,280
by another processor of super number of power and created fuse, it prevails the former

117
00:12:06,280 --> 00:12:09,080
Moscow above overload.

118
00:12:09,080 --> 00:12:10,080
92.

119
00:12:10,080 --> 00:12:14,440
He dives in and out of the ground.

120
00:12:14,440 --> 00:12:20,160
Here, it is rising up that is called diving out and it is sinking down that is called

121
00:12:20,160 --> 00:12:22,200
diving in.

122
00:12:22,200 --> 00:12:46,240
One who wants to do this should attain the water casino, Jana, and emerge.

123
00:12:46,240 --> 00:12:50,800
Then he should do the preliminary work, determining thus, let the earth be in such an

124
00:12:50,800 --> 00:12:54,880
area that the earth in such an area be water.

125
00:12:54,880 --> 00:13:00,080
And he should resolve in a way already described, simultaneously with the resolve, that

126
00:13:00,080 --> 00:13:04,560
much extent of earth's according is determined, becomes water only.

127
00:13:04,560 --> 00:13:10,120
It is there he does the diving in and out.

128
00:13:10,120 --> 00:13:16,680
You're doing great, Kathy, we used to have a poly class after the Suimanka class and one

129
00:13:16,680 --> 00:13:19,880
of the things we learned in it was the sea sound is always a cha.

130
00:13:19,880 --> 00:13:26,440
So the sea would be cha, thank you, oh you're welcome.

131
00:13:26,440 --> 00:13:31,280
Here is the text, he is normally an obtainer of the water casino attainment, he adverts

132
00:13:31,280 --> 00:13:37,760
to earth, having inverted he resolves with knowledge, let there be water, there is water.

133
00:13:37,760 --> 00:13:42,840
He does the diving in and out of the earth, just as men not possessed of super normal

134
00:13:42,840 --> 00:13:47,960
power do diving in and out of water, so this possessor of super normal power by his attaining

135
00:13:47,960 --> 00:13:54,680
mental mastery, does the diving in and out of the earth as though in water.

136
00:13:54,680 --> 00:14:01,120
And he does not only dive in and out, but whatever else he wants, such as bathing, drinking,

137
00:14:01,120 --> 00:14:08,960
mouth washing, washing of chattles and so on, not only water, but there is whatever else

138
00:14:08,960 --> 00:14:14,480
he wants such as ski, oil, honey molasses and so on.

139
00:14:14,480 --> 00:14:21,160
And he does this preliminary work after advertising, thus let there be so much of this

140
00:14:21,160 --> 00:14:26,360
and this and resolves, it becomes as he resolved.

141
00:14:26,360 --> 00:14:35,160
If he takes them and fills dishes with them, the ghee is only ghee, the oil etc, only oil,

142
00:14:35,160 --> 00:14:37,840
the water, only water.

143
00:14:37,840 --> 00:14:41,040
If he wants to be wetted by it, he is wetted.

144
00:14:41,040 --> 00:14:47,520
If he does not want to be wetted by it, he is not wetted, and it is only for him that

145
00:14:47,520 --> 00:14:51,760
the earth becomes water, not for anyone else.

146
00:14:51,760 --> 00:15:02,400
People go on it on foot and in vehicle of etc, and they do their plan etc, but if he wishes

147
00:15:02,400 --> 00:15:07,360
let it be water for them too, it becomes water for them too.

148
00:15:07,360 --> 00:15:13,680
And the time determined has elapsed or the extent determined, except for water originally

149
00:15:13,680 --> 00:15:21,720
present in the water pots, on the etc, become earth again.

150
00:15:21,720 --> 00:15:28,000
On unbroken water, here water that one sinks into an trodden on is called broken, the

151
00:15:28,000 --> 00:15:34,400
opposite is called unbroken, but one who wants to go in this way should attain that

152
00:15:34,400 --> 00:15:40,080
earth cause in a genre, an image, then he should do the preliminary work, determining

153
00:15:40,080 --> 00:15:47,200
thus, let the water in such an area become earth, and he should resolve in the way already

154
00:15:47,200 --> 00:15:55,320
described simultaneously with the resolve, the water in that place becomes earth, he goes

155
00:15:55,320 --> 00:16:00,320
on that.

156
00:16:00,320 --> 00:16:11,640
Here is the text, he is normally an uptener of the earth casino attainment, he advert

157
00:16:11,640 --> 00:16:22,640
to water, having advertted, he resolves with knowledge, that there be earth, there is earth,

158
00:16:22,640 --> 00:16:33,200
he goes on unbroken water, just as men normally not possessed with super normal power, go

159
00:16:33,200 --> 00:16:35,920
on unbroken earth.

160
00:16:35,920 --> 00:16:47,840
So this possessor of super normal power by his attending of mental mystery goes on unbroken

161
00:16:47,840 --> 00:16:56,920
water, as he is on earth.

162
00:16:56,920 --> 00:17:01,400
And he not only goes, but he adopts whatever posture he wishes, and not only earth, but

163
00:17:01,400 --> 00:17:07,560
whatever else solid that he wants, such as gems, gold, rocks, trees, etc.

164
00:17:07,560 --> 00:17:15,640
He adverts to that and resolves, and it becomes as he resolves, and that water becomes

165
00:17:15,640 --> 00:17:21,080
earth only for him, it is water for anyone else.

166
00:17:21,080 --> 00:17:25,560
And fishes and turtles and waterbirds go about there as they like, but if he wishes to

167
00:17:25,560 --> 00:17:30,720
make it earth for other people he does so too, when the time determined has elapsed

168
00:17:30,720 --> 00:17:53,240
it becomes water again.

169
00:17:53,240 --> 00:18:01,120
He should do the preliminary work and determine an area the size of the seat for sitting cross-legged

170
00:18:01,120 --> 00:18:03,240
on, and he should resolve in the way already described.

171
00:18:03,240 --> 00:18:08,520
If he wants to go laying down, he determines an area the size of a bed.

172
00:18:08,520 --> 00:18:14,240
If he wants to go on foot, he determines a suitable area, the size of a path and then, and

173
00:18:14,240 --> 00:18:18,600
he resolves in the way already described, let it be earth.

174
00:18:18,600 --> 00:18:23,920
And simultaneously will the resolve it becomes earth.

175
00:18:23,920 --> 00:18:33,000
99, here is the text, seated cross-legged, he travels in space like a winged bed, he

176
00:18:33,000 --> 00:18:39,440
is normally an obtainer of the earth, a scene of attainment, he adverts space.

177
00:18:39,440 --> 00:18:45,240
Having averted, he resolves with knowledge, let there be earth.

178
00:18:45,240 --> 00:18:53,360
Here is the earth, he travels, stands sits, lies down in space in the sky, just as

179
00:18:53,360 --> 00:19:00,840
menormally, not possess a super normal power travel, walk, stand sit and lie down on

180
00:19:00,840 --> 00:19:02,480
earth.

181
00:19:02,480 --> 00:19:08,600
So this possess a super normal power, by the attaining of mental mastery, travels, stands sits

182
00:19:08,600 --> 00:19:17,680
and lies down in space in the sky.

183
00:19:17,680 --> 00:19:23,760
In Nabikuk, who wants to travel in a space, should be an obtainer of the divine eye.

184
00:19:23,760 --> 00:19:25,520
Why?

185
00:19:25,520 --> 00:19:32,240
On the way there may be mountains, trees, etcetera, that are tempted to originate or

186
00:19:32,240 --> 00:19:39,560
yellows, jellows, nagas, supanna, supanneas, etcetera, may create them.

187
00:19:39,560 --> 00:19:45,920
He will need to be able to see this, but what will be done on seeing them, visual attain

188
00:19:45,920 --> 00:19:51,480
the basic yana and the march, and then he should do the preliminary work thus, let there

189
00:19:51,480 --> 00:19:56,880
be space and resolve this.

190
00:19:56,880 --> 00:20:07,400
What is the use of attaining the attainment?

191
00:20:07,400 --> 00:20:18,120
It is not his mind concentrated, hence any area that has resolved us, let it be a space,

192
00:20:18,120 --> 00:20:19,600
is space.

193
00:20:19,600 --> 00:20:29,040
So he spoke thus, nevertheless, the marriage should be treated as described under the

194
00:20:29,040 --> 00:20:34,720
miracle of going on him direct through walls.

195
00:20:34,720 --> 00:20:42,480
Moreover, he should be an obtainer of a divine eye for the purpose of descending in a

196
00:20:42,480 --> 00:20:53,200
secluded place, for if he descends in a public place, in a baiting place, or at the

197
00:20:53,200 --> 00:20:59,040
abelation gate, he is exposed to the multitude.

198
00:20:59,040 --> 00:21:08,120
So seeing with the divine eye, he should avoid a place where there is no open space and

199
00:21:08,120 --> 00:21:13,080
this hand in an open space.

200
00:21:13,080 --> 00:21:18,440
With his hand, he touches and strokes the moon and sun, so mighty and powerful.

201
00:21:18,440 --> 00:21:24,240
Here the might of the moon and sun should be understood to consist in the fact that they

202
00:21:24,240 --> 00:21:31,800
travel at an altitude of 42,000 leagues and their power to consist in their simultaneous

203
00:21:31,800 --> 00:21:37,760
illuminating of three or four continents, or they are mighty because they travel overhead

204
00:21:37,760 --> 00:21:42,840
and give light as they do, and they are powerful because of that same might.

205
00:21:42,840 --> 00:21:49,680
He touches, he seizes, or he touches in one place, strokes, he strokes all over as if it

206
00:21:49,680 --> 00:21:55,280
was a surface of a looking glass.

207
00:21:55,280 --> 00:22:00,360
The super normal power is successful simply through the Jana that is made the basis for

208
00:22:00,360 --> 00:22:01,880
direct knowledge.

209
00:22:01,880 --> 00:22:08,840
There is no special Kacina attainment here, for this is said in the Bhakti Chasam Bida.

210
00:22:08,840 --> 00:22:13,520
With his hand, so mighty and powerful, here this possessor of super normal power who has

211
00:22:13,520 --> 00:22:17,560
attained mind mastery, averts to the moon and sun.

212
00:22:17,560 --> 00:22:22,360
Having averted, he resolves with knowledge, led to be within hands reach.

213
00:22:22,360 --> 00:22:27,720
It is within hands reach, sitting or lying down with his hand, he touches, makes contact

214
00:22:27,720 --> 00:22:31,120
with, strokes the moon and sun.

215
00:22:31,120 --> 00:22:36,200
Just as men normally not possessed of super normal power, touch, make contact with, stroke

216
00:22:36,200 --> 00:22:39,160
some material object within hands reach.

217
00:22:39,160 --> 00:22:44,560
So this possessor of super normal power by his attaining of mental mastery, sitting or lying

218
00:22:44,560 --> 00:22:51,240
down with his hands touches, make contact with, strokes the moon and sun.

219
00:22:51,240 --> 00:22:59,440
If he wants to go and touch them, he goes and touches them.

220
00:22:59,440 --> 00:23:05,280
But if he wants to touch them, he is sitting or lying down, he resolves, let them be within

221
00:23:05,280 --> 00:23:06,280
hands reach.

222
00:23:06,280 --> 00:23:11,240
Then he either touches them as they stand within hands reach.

223
00:23:11,240 --> 00:23:17,480
When they have come by the power of the resolve, like the palm mirror fruits, loosened

224
00:23:17,480 --> 00:23:21,840
from their stalk, or he does so by enlarging his hand.

225
00:23:21,840 --> 00:23:28,120
But when he enlarges his hand, does he enlarge what is clung to, or what is not clung

226
00:23:28,120 --> 00:23:29,120
to?

227
00:23:29,120 --> 00:23:34,520
He enlarges what is not clung to supported by what is clung to.

228
00:23:34,520 --> 00:23:41,200
Here they are the typical children, I understand, but friends, where does what is clung

229
00:23:41,200 --> 00:23:47,480
to, not becomes small and big too, when a bikka comes out through a keyhole, does not

230
00:23:47,480 --> 00:23:50,560
what is clung to become small.

231
00:23:50,560 --> 00:23:56,680
And when he makes his body big, does it not then become big, as in the case of elder

232
00:23:56,680 --> 00:24:01,760
Muhammad Galana.

233
00:24:01,760 --> 00:24:12,800
At one time, it seems when the householder on the top bikka had heard the last one preaching

234
00:24:12,800 --> 00:24:21,880
the dhamma, he invited him, does, venerable sir, take arms at our house, together with

235
00:24:21,880 --> 00:24:26,680
500 bikkas, and then he departed.

236
00:24:26,680 --> 00:24:36,280
The last one consented, when the rest of that day and part of the night had passed, he surveyed

237
00:24:36,280 --> 00:24:57,280
the 10,000-fold world elements in the early morning, then the royal naga serpent called

238
00:24:57,280 --> 00:25:06,080
Nanda, came within the range of his knowledge.

239
00:25:06,080 --> 00:25:11,600
The blessed one considered him thus, this royal naga has come into the range of my knowledge,

240
00:25:11,600 --> 00:25:16,720
has either potentiality for development, and he saw that he had wrong view and no confidence

241
00:25:16,720 --> 00:25:21,720
in the three jewels, he considered thus, who is there that can cure him of this wrong

242
00:25:21,720 --> 00:25:22,720
view.

243
00:25:22,720 --> 00:25:28,200
He saw that the elder Muhammad Galana could, then when the night had turned to dawn, after

244
00:25:28,200 --> 00:25:58,040
he had seen to the needs of the body, he addressed the venerable aunt.

