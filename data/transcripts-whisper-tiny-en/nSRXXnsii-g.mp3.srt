1
00:00:00,000 --> 00:00:17,040
Ok, good evening everyone, welcome to our evening Dhama session.

2
00:00:17,040 --> 00:00:25,280
We come together to practice and to reflect on our practice and to learn more about the

3
00:00:25,280 --> 00:00:42,280
practice. So it's all about any different kinds of learning. We're learning theory, how to

4
00:00:42,280 --> 00:01:06,280
practice technique, learning about reality, learning about ourselves, how our minds work.

5
00:01:06,280 --> 00:01:18,280
So of course the question that often comes up is why are we practicing? It's a question that might come up during your practice.

6
00:01:18,280 --> 00:01:34,280
Why am I here again? Sometimes we have superficial reasons. Sometimes our reasons change as we practice.

7
00:01:34,280 --> 00:01:52,280
It's common for people to practice meditation, thinking it's going to be a quick fix that it will be calming, peaceful.

8
00:01:52,280 --> 00:02:21,280
Allow us to control our unruly minds, maybe even experience something profound or spiritual, like having visions or maybe have ecstatic feelings.

9
00:02:21,280 --> 00:02:34,280
Some people even practice meditation to gain magical powers or super mundane experiences like seeing heaven or seeing hell or seeing angels or ghosts.

10
00:02:34,280 --> 00:02:44,280
People, many people practice hoping to remember past lives.

11
00:02:44,280 --> 00:02:59,280
But all of these are, it's difficult to maintain the desire to practice because many of these, or most of these reasons for practicing aren't very practical.

12
00:02:59,280 --> 00:03:03,280
They don't.

13
00:03:03,280 --> 00:03:15,280
They don't have a profound impact on your state of mind, your contentment, very fulfilling.

14
00:03:15,280 --> 00:03:26,280
And reality doesn't allow for satisfaction or fulfillment through these various ways.

15
00:03:26,280 --> 00:03:39,280
And so it can be quite disconcerting when meditation is stressful or unpleasant or unamenable to your wishes.

16
00:03:39,280 --> 00:03:43,280
I wonder why am I doing this again?

17
00:03:43,280 --> 00:03:48,280
So the first reason why we're doing this is for purity.

18
00:03:48,280 --> 00:03:58,280
We're practicing because we feel bad about various things, about ourselves and our attitude, our behavior and so on.

19
00:03:58,280 --> 00:04:02,280
We feel that we can improve.

20
00:04:02,280 --> 00:04:05,280
Sometimes we downright hate ourselves.

21
00:04:05,280 --> 00:04:17,280
But most often we just feel self-conscious or self-esteem issues that are often based on reality that, yes, we're not perfect.

22
00:04:17,280 --> 00:04:24,280
We have problems, there are problems about us.

23
00:04:24,280 --> 00:04:33,280
We're often taught to ignore these or to accept these or to think you're perfect just the way you are.

24
00:04:33,280 --> 00:04:39,280
Now in Buddhism we're not of that sort of bent.

25
00:04:39,280 --> 00:04:45,280
It's understandable if you don't have a solution for your problems.

26
00:04:45,280 --> 00:04:54,280
You don't have a solution for your inadequacies or your failings.

27
00:04:54,280 --> 00:05:02,280
Then it's reasonable to think, well maybe the answer is just to accept them and love yourself the way you are, right?

28
00:05:02,280 --> 00:05:07,280
I mean it sounds good. Sounds nice to think, oh, few.

29
00:05:07,280 --> 00:05:10,280
I don't actually have to fix these problems, right?

30
00:05:10,280 --> 00:05:17,280
It would be nice if someone could just tell you that you're okay just the way you are.

31
00:05:17,280 --> 00:05:22,280
And unfortunately I'm here to tell you you're not okay just the way you are.

32
00:05:22,280 --> 00:05:32,280
We've got lots of problems.

33
00:05:32,280 --> 00:05:40,280
But that's okay and I'm here to reassure you not that it's okay to be that way but that we've got solutions.

34
00:05:40,280 --> 00:05:48,280
That there is a way, there is a path, there is practice that can cultivate purity.

35
00:05:48,280 --> 00:05:56,280
Meditation is so powerful.

36
00:05:56,280 --> 00:06:05,280
When you're really in the present moment it changes you. It's like shining a bright light into the darkness.

37
00:06:05,280 --> 00:06:16,280
You will see many things about yourself that were previously hidden.

38
00:06:16,280 --> 00:06:23,280
You see how you react.

39
00:06:23,280 --> 00:06:26,280
You see how you get upset.

40
00:06:26,280 --> 00:06:31,280
You see how you crave, how we crave so many different things.

41
00:06:31,280 --> 00:06:43,280
Food, music, beauty, entertainment, romance, sex.

42
00:06:43,280 --> 00:06:53,280
Our cravings, our diverse and intense.

43
00:06:53,280 --> 00:06:57,280
When we see this about it we can see how our mind works.

44
00:06:57,280 --> 00:07:02,280
More importantly we see that the things that we cling to and the things that we dislike.

45
00:07:02,280 --> 00:07:08,280
You know talking about this last night but see that they're not worth clinging to.

46
00:07:08,280 --> 00:07:13,280
Our purity comes from this knowledge, from this learning.

47
00:07:13,280 --> 00:07:20,280
It doesn't come by forcing your mind to be peaceful.

48
00:07:20,280 --> 00:07:23,280
Purity isn't about just purity of mind.

49
00:07:23,280 --> 00:07:26,280
What do we mean by purity?

50
00:07:26,280 --> 00:07:31,280
We have this list of seven purifications.

51
00:07:31,280 --> 00:07:40,280
We certainly don't mean bodily purity or even purity of actions.

52
00:07:40,280 --> 00:07:43,280
But we don't even stop at purity of mind.

53
00:07:43,280 --> 00:07:51,280
We're not content just with a pure mind because a pure mind is a momentary thing.

54
00:07:51,280 --> 00:07:58,280
We would have talked about the purification of a being or the purity of a being.

55
00:07:58,280 --> 00:08:03,280
And by that he meant not just that your mind is pure at any given moment but that

56
00:08:03,280 --> 00:08:12,280
you have cleansed the potential for impurity to arise.

57
00:08:12,280 --> 00:08:15,280
You have changed yourself.

58
00:08:15,280 --> 00:08:22,280
I do no longer give rise to impure mind, to impure thoughts.

59
00:08:22,280 --> 00:08:30,280
And by impure we simply mean that which causes suffering for you and for others.

60
00:08:30,280 --> 00:08:37,280
That's a very practical reason so we can be more perfect.

61
00:08:37,280 --> 00:08:43,280
Something that we might have disbelieved before we started meditating.

62
00:08:43,280 --> 00:08:50,280
I think that you can't actually fix anything about yourself that we're stuck with our personality.

63
00:08:50,280 --> 00:08:56,280
And you meditate and you realize that our personality is changeable and it's always changing.

64
00:08:56,280 --> 00:09:05,280
Constantly shifting according to our habits.

65
00:09:05,280 --> 00:09:14,280
The second practical reason is to overcome mental illness.

66
00:09:14,280 --> 00:09:21,280
So it's related to purity but specifically referring to mental illness.

67
00:09:21,280 --> 00:09:28,280
And illness we mean in a very literal sense where ill or sick.

68
00:09:28,280 --> 00:09:34,280
Sick is maybe sounds a little too harsh but illness in Buddhism.

69
00:09:34,280 --> 00:09:43,280
Mental illness in Buddhism is any sort of disturbance, any sort of sadness or depression,

70
00:09:43,280 --> 00:09:48,280
anxiety, anger, addiction.

71
00:09:48,280 --> 00:09:51,280
All of these are considered to be mental illness.

72
00:09:51,280 --> 00:10:00,280
We're talking about the states of mind that cause great anguish, great suffering.

73
00:10:00,280 --> 00:10:08,280
The states of mind that are a lack of peace that take us away from peace.

74
00:10:08,280 --> 00:10:16,280
So if you have an anxiety issue or if you have an anger issue or if you have addiction issues,

75
00:10:16,280 --> 00:10:24,280
it's the issues, if you're depressed, depression is a big one.

76
00:10:24,280 --> 00:10:27,280
These are all related to the purity of the mind.

77
00:10:27,280 --> 00:10:31,280
If your mind is pure, these are the benefits.

78
00:10:31,280 --> 00:10:41,280
That you purify and you free yourself from mental illness.

79
00:10:41,280 --> 00:10:49,280
The third practical reason is to overcome suffering.

80
00:10:49,280 --> 00:10:53,280
You know the pain that you experience in.

81
00:10:53,280 --> 00:10:59,280
It's more likely that you're thinking meditation is very painful.

82
00:10:59,280 --> 00:11:02,280
It's unpleasant.

83
00:11:02,280 --> 00:11:05,280
There's a story of this monk who was meditating.

84
00:11:05,280 --> 00:11:09,280
Because he was there were these bandits who were going to kill him.

85
00:11:09,280 --> 00:11:14,280
So he broke both of his legs with a big rock.

86
00:11:14,280 --> 00:11:16,280
And said, look, I'm not going to run away.

87
00:11:16,280 --> 00:11:17,280
Just come back in the morning.

88
00:11:17,280 --> 00:11:23,280
You can take me away then if you want.

89
00:11:23,280 --> 00:11:28,280
And he meditated on the pain.

90
00:11:28,280 --> 00:11:34,280
Overcoming pain is important to understand we're not trying to escape suffering.

91
00:11:34,280 --> 00:11:36,280
Not directly.

92
00:11:36,280 --> 00:11:44,280
The attitude of wanting to be free from pain is problematic because it's based on aversion.

93
00:11:44,280 --> 00:11:48,280
That's sort of the catch that we find ourselves in regard to suffering.

94
00:11:48,280 --> 00:11:54,280
The less the more you don't want to suffer, the more you suffer.

95
00:11:54,280 --> 00:12:00,280
And so overcoming pain is affected through mindfulness.

96
00:12:00,280 --> 00:12:05,280
Not because mindfulness makes the pain go away.

97
00:12:05,280 --> 00:12:10,280
Because mindfulness stops you from being upset to changes the way you look at pain.

98
00:12:10,280 --> 00:12:13,280
So that you see it simply for what it is.

99
00:12:13,280 --> 00:12:19,280
Just something wrong with pain.

100
00:12:19,280 --> 00:12:25,280
We find we're so caught up with bad habits, really.

101
00:12:25,280 --> 00:12:27,280
That's so many things cause us pain.

102
00:12:27,280 --> 00:12:33,280
I mean, loud noise will cause you anger and stress and suffering.

103
00:12:33,280 --> 00:12:38,280
Bad food.

104
00:12:38,280 --> 00:12:42,280
Kind of food that you're not keen on.

105
00:12:42,280 --> 00:12:46,280
Smells, Keaton cold.

106
00:12:46,280 --> 00:12:51,280
It's a little bit cold, it's a little bit hot, and it's quite unpleasant for us.

107
00:12:51,280 --> 00:12:54,280
And the question is why is it unpleasant?

108
00:12:54,280 --> 00:12:56,280
You start to investigate meditation.

109
00:12:56,280 --> 00:12:59,280
With through meditation, of course, using mindfulness.

110
00:12:59,280 --> 00:13:02,280
You start to see that there is no good answer.

111
00:13:02,280 --> 00:13:10,280
There's no reason why pain should make us upset or cold or heat or hunger or thirst or good

112
00:13:10,280 --> 00:13:20,280
smells, bad smells. There's no reason for us to be attached to our verse of verses to anything.

113
00:13:20,280 --> 00:13:24,280
That's a very powerful thing that we can overcome pain.

114
00:13:24,280 --> 00:13:28,280
It's something that society tells you you can't overcome.

115
00:13:28,280 --> 00:13:33,280
The only thing to do is if you go to see the doctor and you tell them you have pain

116
00:13:33,280 --> 00:13:35,280
and they give you a pain and killer.

117
00:13:35,280 --> 00:13:38,280
That's so they could kill the pain.

118
00:13:38,280 --> 00:13:46,280
It's supposed technically they can, they kill the pain, but they can't kill the aversion to pain.

119
00:13:46,280 --> 00:13:52,280
In fact, it only makes it worse.

120
00:13:52,280 --> 00:13:56,280
Another practical reason is it helps us find the right path.

121
00:13:56,280 --> 00:13:59,280
You know, the path in Buddhism isn't about becoming a monk.

122
00:13:59,280 --> 00:14:03,280
It's not even about becoming Buddhist.

123
00:14:03,280 --> 00:14:08,280
When we talk about finding the path, we mean doing whatever you do mindfully.

124
00:14:08,280 --> 00:14:18,280
Finding mindfulness. The path is the path of mindfulness.

125
00:14:18,280 --> 00:14:25,280
What that means is you really don't have to change anything about your life intentionally.

126
00:14:25,280 --> 00:14:27,280
You don't have to make all sorts of decisions.

127
00:14:27,280 --> 00:14:29,280
You don't have to have the right answer.

128
00:14:29,280 --> 00:14:34,280
A phone call today was talking to someone about their life and they said,

129
00:14:34,280 --> 00:14:43,280
Boy, I wish Buddha had given more explicit instructions about how to live your life.

130
00:14:43,280 --> 00:14:52,280
But that's the thing is there is no correct answer on how to live your life.

131
00:14:52,280 --> 00:14:54,280
I mean, you're living your life no matter what.

132
00:14:54,280 --> 00:14:59,280
The question is what choices you make, what decisions you make.

133
00:14:59,280 --> 00:15:02,280
So we have this question about technical issues.

134
00:15:02,280 --> 00:15:04,280
What decisions should I make?

135
00:15:04,280 --> 00:15:08,280
That's not really what's important from a Buddhist perspective.

136
00:15:08,280 --> 00:15:13,280
The right path is when you make decisions mindfully.

137
00:15:13,280 --> 00:15:19,280
Because if you're truly mindful, you can't possibly make a decision for the wrong reason.

138
00:15:19,280 --> 00:15:26,280
If you're truly mindful, you see as clear as possible about the situation.

139
00:15:26,280 --> 00:15:29,280
You mind as pure.

140
00:15:29,280 --> 00:15:34,280
So your actions, your decisions are not based on selfishness or aversion.

141
00:15:34,280 --> 00:15:37,280
They're not based on addiction or attachment.

142
00:15:37,280 --> 00:15:42,280
They're not based on delusion or arrogance.

143
00:15:42,280 --> 00:15:50,280
You can see that they're based on wisdom, they're based on kindness, they're based on compassion.

144
00:15:50,280 --> 00:15:54,280
They're based on things that actually bring happiness to you and to others.

145
00:15:54,280 --> 00:15:57,280
They actually bring peace.

146
00:15:57,280 --> 00:16:08,280
Based on mind states that are actually good for you.

147
00:16:08,280 --> 00:16:22,280
And the final reason which may seem somewhat impractical, but perhaps the most practical is meditation is to realize nibana or nirvana.

148
00:16:22,280 --> 00:16:28,280
Meditation is to help us let go.

149
00:16:28,280 --> 00:16:38,280
And the mind sees again and again what it's doing wrong, eventually it says, oh, I better stop that.

150
00:16:38,280 --> 00:16:45,280
And when it sees again and again that the things that we cling to are not actually worth clinging to,

151
00:16:45,280 --> 00:16:48,280
then it lets go.

152
00:16:48,280 --> 00:16:53,280
When the mind lets go, this is called nirvana.

153
00:16:53,280 --> 00:17:03,280
This cessation of suffering and the mind enters into, doesn't even really enter into, but it stops entering.

154
00:17:03,280 --> 00:17:05,280
Seeing is an entering.

155
00:17:05,280 --> 00:17:07,280
Hearing is an entering.

156
00:17:07,280 --> 00:17:11,280
We enter into through these doors to see, to hear.

157
00:17:11,280 --> 00:17:17,280
And then the mind sees enough and it's clear about seeing and hearing and smelling.

158
00:17:17,280 --> 00:17:23,280
And has this true clarity about experiences, about the doors of the senses.

159
00:17:23,280 --> 00:17:25,280
And it lets go.

160
00:17:25,280 --> 00:17:29,280
It sees that there's nothing worth clinging to, there's nothing worth seeking out.

161
00:17:29,280 --> 00:17:32,280
So it stops seeking.

162
00:17:32,280 --> 00:17:40,280
And with the stopping of seeking, there is no more seeing or hearing or smelling or tasting or feeling or thinking.

163
00:17:40,280 --> 00:17:47,280
There is the cessation of suffering.

164
00:17:47,280 --> 00:17:51,280
Not many good reasons for practicing and they're all related.

165
00:17:51,280 --> 00:17:54,280
They relate to our state of mind.

166
00:17:54,280 --> 00:17:58,280
They relate to happiness, true happiness.

167
00:17:58,280 --> 00:18:01,280
They relate to peace.

168
00:18:01,280 --> 00:18:03,280
They relate to goodness.

169
00:18:03,280 --> 00:18:06,280
Meditation makes you a better person.

170
00:18:06,280 --> 00:18:12,280
It makes us better, all better, better in many ways.

171
00:18:12,280 --> 00:18:16,280
Better in terms of our goodness, better in terms of happiness,

172
00:18:16,280 --> 00:18:23,280
better in terms of health, mental health, physical health.

173
00:18:23,280 --> 00:18:29,280
Meditation makes us happy.

174
00:18:29,280 --> 00:18:33,280
Which may be hard to believe for those of you who are struggling through it.

175
00:18:33,280 --> 00:18:37,280
Happiness is not an easy thing, neither is peace.

176
00:18:37,280 --> 00:18:43,280
Our problem is that we're conditioned to think that happiness is something you can just have happened to you.

177
00:18:43,280 --> 00:18:53,280
You can get, you can buy, you can chase, you can find.

178
00:18:53,280 --> 00:18:57,280
You find happiness here, there or somewhere else.

179
00:18:57,280 --> 00:18:59,280
I think it's something easy.

180
00:18:59,280 --> 00:19:03,280
Happiness is not easy, happiness requires effort.

181
00:19:03,280 --> 00:19:05,280
It requires wisdom.

182
00:19:05,280 --> 00:19:08,280
It requires a lot of work.

183
00:19:08,280 --> 00:19:11,280
It requires a lot of work because we're all bent out of shape.

184
00:19:11,280 --> 00:19:15,280
We do things and we're inclined in our habits.

185
00:19:15,280 --> 00:19:17,280
Our set up to hurt us.

186
00:19:17,280 --> 00:19:22,280
We have habits set up that actually cause us stress and suffering in their habitual.

187
00:19:22,280 --> 00:19:24,280
So it takes a lot of work.

188
00:19:24,280 --> 00:19:28,280
The Buddha said if you have a tree leaning in one direction,

189
00:19:28,280 --> 00:19:33,280
you want to pull it in the other direction, you need a strong rope and you need to work really hard.

190
00:19:33,280 --> 00:19:40,280
You can't just cut the tree down and hope it falls in the other direction because it won't.

191
00:19:40,280 --> 00:19:45,280
If there's an elephant stuck in the mud, it's going to take a lot of work to get the elephant out.

192
00:19:45,280 --> 00:19:51,280
So we're stuck, we're stuck with our attachments and our versions.

193
00:19:51,280 --> 00:19:57,280
There's no question that being free from them would be good when bring us peace and happiness.

194
00:19:57,280 --> 00:20:02,280
The only question is how much work it's going to take to do it.

195
00:20:02,280 --> 00:20:05,280
And so it requires patience.

196
00:20:05,280 --> 00:20:10,280
I know I'd ask that all of you be patient and be patient with the suffering.

197
00:20:10,280 --> 00:20:16,280
And we can be patient with the work that this takes.

198
00:20:16,280 --> 00:20:25,280
Be vigilant, methodical, and don't give up because that's the nature of habits.

199
00:20:25,280 --> 00:20:28,280
You might think, oh, this is working.

200
00:20:28,280 --> 00:20:33,280
And then give up and then let your guard down or stop trying.

201
00:20:33,280 --> 00:20:36,280
Because it's habitual. Our habits are to be lazy.

202
00:20:36,280 --> 00:20:44,280
Our habits are to just indulge and enjoy and indulge in our likes and dislikes.

203
00:20:44,280 --> 00:20:47,280
But that won't make you happy. That won't bring you peace.

204
00:20:47,280 --> 00:20:51,280
So to fight against that habit and cultivate new ones,

205
00:20:51,280 --> 00:21:02,280
habits of mindfulness, habits of clarity.

206
00:21:02,280 --> 00:21:05,280
Many reasons, many good reasons to practice.

207
00:21:05,280 --> 00:21:11,280
And this I think this sort of thing gives us sort of talk, gives us clarity about the practice,

208
00:21:11,280 --> 00:21:13,280
but it also gives us encouragement.

209
00:21:13,280 --> 00:21:18,280
I mean, I want you to be encouraged and of course continue to practice.

210
00:21:18,280 --> 00:21:24,280
It's a great benefit, not only to you, but to the world.

211
00:21:24,280 --> 00:21:28,280
You're doing a great service, not only to yourself, but to all those around you.

212
00:21:28,280 --> 00:21:33,280
When you become a better person, when you become wise, you can help others.

213
00:21:33,280 --> 00:21:39,280
This is what lights up the world. This is what has made the world such a great place.

214
00:21:39,280 --> 00:21:53,280
People like you coming to better yourselves, to be a source of inspiration and insight and goodness for the world.

215
00:21:53,280 --> 00:22:01,280
So thank you all for coming out. Thank you all for listening, for being good people.

216
00:22:01,280 --> 00:22:11,280
I wish you all the success in the world in your practice.

217
00:22:11,280 --> 00:22:19,280
So thank you all. That's the demo for tonight. You can go back to your practice.

218
00:22:19,280 --> 00:22:24,280
For the internet, if you got any questions, please post them on our site.

219
00:22:24,280 --> 00:22:33,280
Hopefully some people already have, well, if they have all the answers.

220
00:22:33,280 --> 00:22:40,280
I wanted to clarify something I said last night that might have been a little bit unpleasant,

221
00:22:40,280 --> 00:22:45,280
or might have sounded wrong or been interpreted wrong.

222
00:22:45,280 --> 00:22:49,280
I've joked about how you're not being paid or you're not paying me.

223
00:22:49,280 --> 00:22:54,280
There's two points there. I mean, one is that I don't charge for this.

224
00:22:54,280 --> 00:22:59,280
The point wasn't that, hey, if you want me to answer your questions, you got to pay me because that's not how it works.

225
00:22:59,280 --> 00:23:09,280
The point is that I live my life and I'm not my goal in life isn't to change the world or answer everyone's questions.

226
00:23:09,280 --> 00:23:16,280
So this isn't a service that I'm providing and trying to get some gain out of it.

227
00:23:16,280 --> 00:23:24,280
This is just me living my life and people ask me to help them and so I help, but I'm never going to help everyone.

228
00:23:24,280 --> 00:23:27,280
There's always a limit has to be placed.

229
00:23:27,280 --> 00:23:32,280
But the other point that I was trying to make is that you're not paying for this, so there's no need to complain.

230
00:23:32,280 --> 00:23:37,280
If it's not suiting you, if it's not what you're looking for, then you didn't lose anything.

231
00:23:37,280 --> 00:23:44,280
All you lost is a little bit of time, go somewhere else and find something that does please you.

232
00:23:44,280 --> 00:23:54,280
That was all I meant. I didn't, wasn't trying to imply that somehow money is important.

233
00:23:54,280 --> 00:24:02,280
Are monks allowed to read non Buddhist material? Yes, that's not a problem.

234
00:24:02,280 --> 00:24:12,280
Technical texts, not a problem. I mean, you might, you might, yes, absolutely, but the question is why are you reading them?

235
00:24:12,280 --> 00:24:20,280
So, you know, a monk can learn how to build a Kooti who can learn how to do many different technical things,

236
00:24:20,280 --> 00:24:31,280
but it should be related to the monastic life or some goal that helps you progress from that animatomatically.

237
00:24:31,280 --> 00:24:37,280
Is taxation theft if one chooses not to use public services?

238
00:24:37,280 --> 00:24:47,280
Is taxation theft? Well theft is interesting. I mean, in some ways, it has to do with the law of the land,

239
00:24:47,280 --> 00:24:54,280
not in all ways, but in some ways.

240
00:24:54,280 --> 00:25:03,280
So, I would think that the Buddha would be, the Buddha is actually from the looks of it, he considers not paying your taxes

241
00:25:03,280 --> 00:25:14,280
to potentially be, to potentially be problematic and even theft.

242
00:25:14,280 --> 00:25:21,280
In certain instances, like if you take something out of the country and there's export tax on that,

243
00:25:21,280 --> 00:25:28,280
you're not paying the king or the government their dues on exports,

244
00:25:28,280 --> 00:25:36,280
and you take that thing out of the country, he considers that to be theft. The Buddha considered that or the texts that we have that

245
00:25:36,280 --> 00:25:41,280
purport to say that the Buddha considered that theft.

246
00:25:41,280 --> 00:25:47,280
So, it's an interesting issue.

247
00:25:47,280 --> 00:25:53,280
I don't think taxation would ever be considered theft. I don't think the Buddha would have considered to be theft.

248
00:25:53,280 --> 00:26:01,280
It's considered that that is owed to the government. The government makes up the rules.

249
00:26:01,280 --> 00:26:09,280
And I don't think the Buddha was looking at issues of trying to overthrow kings or so on.

250
00:26:09,280 --> 00:26:14,280
It was very much about trying to live within the system and do what he can,

251
00:26:14,280 --> 00:26:22,280
good to help people to see the right way, including the government, including kings.

252
00:26:22,280 --> 00:26:30,280
He didn't seem to think there was much benefit in trying to protest the system by, for example, not paying your taxes.

253
00:26:30,280 --> 00:26:35,280
It's an interesting issue. I don't have answers that Buddha didn't, but there are hints.

254
00:26:35,280 --> 00:26:43,280
I think it's wrong to think that the Buddha was some kind of rebel anarchist or something like that.

255
00:26:43,280 --> 00:26:46,280
The Buddha was trying to help people.

256
00:26:46,280 --> 00:26:56,280
I mean, just as a matter of course, he was working with everyone to help them, but he didn't have an agenda or so on.

257
00:26:56,280 --> 00:27:05,280
So, when he say that taxation is theft, I'm inclined to think not.

258
00:27:05,280 --> 00:27:14,280
I get where you're going. I mean, I understand that it's probably immoral if a king overtaxes their subjects,

259
00:27:14,280 --> 00:27:25,280
but I don't think tax in general is because you're a part of a system that's reasonable to believe that I know some people don't like taxes.

260
00:27:25,280 --> 00:27:30,280
I'm not one of those people. I haven't think taxes are actually quite wonderful and beneficial.

261
00:27:30,280 --> 00:27:37,280
I like the idea of buying into a system, theoretically, and the idea that everyone has to buy into it,

262
00:27:37,280 --> 00:27:45,280
and that you have an obligation to buy into the system and the society in which you live, no matter what you do.

263
00:27:45,280 --> 00:27:52,280
So, the question of how fair taxation is, and if taxation is unfair, that's a problem.

264
00:27:52,280 --> 00:27:59,280
And the question of what is fair? Should rich people pay more, or should everyone pay the same amount, etc., etc.

265
00:27:59,280 --> 00:28:06,280
Those are interesting issues, but I'm inclined to think taxation is a good thing personally.

266
00:28:06,280 --> 00:28:15,280
I live in a country where there's a very high taxation, relatively high taxation rate, compared to, say, America,

267
00:28:15,280 --> 00:28:20,280
but it seems to have good results.

268
00:28:20,280 --> 00:28:25,280
Is there a meditation for sending love and or forgiveness? Love, certainly.

269
00:28:25,280 --> 00:28:33,280
I mean, look up, make the practice of made die, and I have talks on that forgiveness.

270
00:28:33,280 --> 00:28:39,280
I mean, forgiveness, you don't have something you might meditate on, because it could become obsessive,

271
00:28:39,280 --> 00:28:49,280
but it's something that you should take the time just a few moments to ask forgiveness and to forgive others.

272
00:28:49,280 --> 00:28:54,280
Make a determination, I forgive those people, may they forgive me, etc., etc.

273
00:28:54,280 --> 00:29:00,280
And of course, even if you have time, go to them and actually say it to them.

274
00:29:00,280 --> 00:29:07,280
But it's a good thing. I mean, it's kind of a meditation to sit there and remind yourself, I forgive these people.

275
00:29:07,280 --> 00:29:13,280
And be part of your made-up practice. It's associated.

276
00:29:13,280 --> 00:29:19,280
It's a good question.

277
00:29:19,280 --> 00:29:24,280
And that's all. That's all for tonight. How's YouTube doing?

278
00:29:24,280 --> 00:29:35,280
How are you all doing on YouTube? Lots of comments. Are you fighting? Tell me you're not fighting again.

279
00:29:35,280 --> 00:29:39,280
Debating, maybe.

280
00:29:39,280 --> 00:29:42,280
Hi, YouTube.

281
00:29:42,280 --> 00:29:45,280
If you want to ask questions, I'm sorry, unfortunately.

282
00:29:45,280 --> 00:29:50,280
The problem with YouTube is the questions are unfiltered, and there's such a range of them,

283
00:29:50,280 --> 00:29:58,280
and I end up answering all sorts of questions and seems to be repetitive and rather unproductive.

284
00:29:58,280 --> 00:30:02,280
So making a little bit difficult, and then we get only the really good questions.

285
00:30:02,280 --> 00:30:06,280
People who really need answers go on our site.

286
00:30:06,280 --> 00:30:11,280
You have to actually log in. That takes some effort.

287
00:30:11,280 --> 00:30:18,280
And post the question there, and then it gets up-folded.

288
00:30:18,280 --> 00:30:21,280
How are we doing in second life?

289
00:30:21,280 --> 00:30:25,280
Good evening, second life.

290
00:30:25,280 --> 00:30:32,280
God, people asking questions, talking,

291
00:30:32,280 --> 00:30:36,280
chatting, commenting on my answers. That's good.

292
00:30:36,280 --> 00:30:40,280
I get some different perspectives.

293
00:30:40,280 --> 00:30:43,280
Well, that's all for tonight. Thank you all for tuning in.

294
00:30:43,280 --> 00:30:47,280
I wish you all the best.

295
00:30:47,280 --> 00:30:50,280
Thank you.

