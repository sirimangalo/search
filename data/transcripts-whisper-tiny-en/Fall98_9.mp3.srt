1
00:00:00,000 --> 00:00:11,000
The Dhamatau, you live by a variable, who is in London, at the Tagatar Media Distance

2
00:00:11,000 --> 00:00:21,000
Center, San Jose, California, special retreat of November, 1998.

3
00:00:21,000 --> 00:00:45,000
So, our retreat has come to an end, and I hope every yogi gets something out of this retreat,

4
00:00:45,000 --> 00:00:53,000
although what you can maybe not what you expected.

5
00:00:53,000 --> 00:01:00,000
But during the retreat, you keep your minds pure,

6
00:01:00,000 --> 00:01:12,000
you are away from mental defilements, so you enjoy your experience,

7
00:01:12,000 --> 00:01:20,000
the mental seclusion, seclusion from mental defilements,

8
00:01:20,000 --> 00:01:30,000
or seclusion from worries and anxieties from the outside world.

9
00:01:30,000 --> 00:01:38,000
To reach the final goal is not easy,

10
00:01:38,000 --> 00:01:48,000
and we don't know how much longer we will have to make effort to reach that goal.

11
00:01:48,000 --> 00:01:52,000
Even during the time of the Buddha,

12
00:01:52,000 --> 00:02:08,000
even though there were people who had potential to get emancipation,

13
00:02:08,000 --> 00:02:14,000
or to get enlightenment, did not all get enlightenment.

14
00:02:14,000 --> 00:02:26,000
There are many reasons that block the way they are way to the attainment of enlightenment.

15
00:02:26,000 --> 00:02:37,000
This explains to us that there are two reasons why people fail to get enlightenment,

16
00:02:37,000 --> 00:02:44,000
although they had the potential to reach enlightenment,

17
00:02:44,000 --> 00:02:50,000
and although they even get to see the Buddha.

18
00:02:50,000 --> 00:03:10,000
The reasons they gave one lack of exertion and two having a bad friend.

19
00:03:10,000 --> 00:03:18,000
Whatever we do, we need a good friend.

20
00:03:18,000 --> 00:03:22,000
By associating with a good friend,

21
00:03:22,000 --> 00:03:28,000
we can develop our own faculties,

22
00:03:28,000 --> 00:03:41,000
and we can develop into reaching into higher stages of spiritual path.

23
00:03:41,000 --> 00:03:48,000
At friends, then we are influenced by these people,

24
00:03:48,000 --> 00:03:53,000
and we tend to follow their wrong activities,

25
00:03:53,000 --> 00:03:58,000
or their wrong advices.

26
00:03:58,000 --> 00:04:06,000
We did very wrong things following the advice of bad friends.

27
00:04:06,000 --> 00:04:11,000
King Adyatta said,

28
00:04:11,000 --> 00:04:16,000
Now, the Lord Adyatta

29
00:04:16,000 --> 00:04:22,000
enticed him when Adyatta said he was a young prince

30
00:04:22,000 --> 00:04:30,000
to his side, and incited him to kill his own father

31
00:04:30,000 --> 00:04:33,000
so that he could become a king.

32
00:04:33,000 --> 00:04:39,000
As you know, Adyatta was a very ambitious man.

33
00:04:39,000 --> 00:04:45,000
He tried to kill the Buddha to become the Buddha himself,

34
00:04:45,000 --> 00:04:53,000
and so he incited the young prince Adyatta said to kill his own father

35
00:04:53,000 --> 00:04:55,000
to become a king.

36
00:04:55,000 --> 00:05:03,000
Adyatta said he was successful, so he became a king,

37
00:05:03,000 --> 00:05:06,000
and he killed his own father.

38
00:05:06,000 --> 00:05:11,000
Although, what had attempted three times to kill the Buddha,

39
00:05:11,000 --> 00:05:17,000
he was not successful because nobody can kill the Buddha.

40
00:05:17,000 --> 00:05:26,000
So, the first time he sent sharp shooters to shoot the Buddha,

41
00:05:26,000 --> 00:05:32,000
and the second time he sent the great elephant to attack the Buddha.

42
00:05:32,000 --> 00:05:40,000
When these two failed, he himself pushed a big rock onto the Buddha

43
00:05:40,000 --> 00:05:46,000
when Buddha was walking up and down on a hill.

44
00:05:46,000 --> 00:05:54,000
But the rock missed the Buddha, and hit some rock on the hill,

45
00:05:54,000 --> 00:06:01,000
and the splinter of that rock hit the Buddha on his toes,

46
00:06:01,000 --> 00:06:08,000
and so Buddha had his blood congealed there,

47
00:06:08,000 --> 00:06:12,000
and had a severe pain.

48
00:06:12,000 --> 00:06:18,000
But that was the most Adyatta can do to the life of the Buddha.

49
00:06:18,000 --> 00:06:28,000
But that act, that act of causing the blood congealed in the body of the Buddha,

50
00:06:28,000 --> 00:06:35,000
is as bad as killing one's own father.

51
00:06:35,000 --> 00:06:42,000
It is said that after killing his father,

52
00:06:42,000 --> 00:06:50,000
Adyatta Satu was remorseful, and he was always haunted by this horrible memory,

53
00:06:50,000 --> 00:06:57,000
and so he even could not go to sleep well.

54
00:06:57,000 --> 00:07:07,000
But years later, and he did not dare to go to the Buddha and see him,

55
00:07:07,000 --> 00:07:13,000
because he had killed the Buddha's disciple,

56
00:07:13,000 --> 00:07:16,000
who was his father.

57
00:07:16,000 --> 00:07:25,000
But years later, about eight years before the Buddha's death,

58
00:07:25,000 --> 00:07:29,000
he went to see the Buddha.

59
00:07:29,000 --> 00:07:38,000
So he asked his trusted friend, the royal physician,

60
00:07:38,000 --> 00:07:42,000
Givaka, to take him to the Buddha,

61
00:07:42,000 --> 00:07:46,000
and so he went out to the Buddha to see the Buddha,

62
00:07:46,000 --> 00:07:48,000
and he went out at night.

63
00:07:48,000 --> 00:07:53,000
So he was always afraid of people trying to kill him.

64
00:07:53,000 --> 00:07:58,000
Now, people who had done something like this is always suspicious,

65
00:07:58,000 --> 00:08:01,000
and they live in fear.

66
00:08:01,000 --> 00:08:05,000
So he was afraid that they might be attempt on his lives,

67
00:08:05,000 --> 00:08:12,000
and so he had a great number of people to guard him.

68
00:08:12,000 --> 00:08:18,000
It is said that he went to the monastery with 500 elephants.

69
00:08:18,000 --> 00:08:23,000
So he went to the monastery, he saw the Buddha,

70
00:08:23,000 --> 00:08:33,000
and he asked the Buddha to explain to him the fruits of being a recluse,

71
00:08:33,000 --> 00:08:38,000
that could be realized in this very life.

72
00:08:38,000 --> 00:08:42,000
And so in response to his question,

73
00:08:42,000 --> 00:08:51,000
he delivered to him his discourse, now known as Samanya Palat Suta.

74
00:08:51,000 --> 00:08:58,000
So it is a fairly long Suta included in the collection of long discourses.

75
00:08:58,000 --> 00:09:02,000
So at the end of the discourse,

76
00:09:02,000 --> 00:09:04,000
Adhara Sadhu was very glad,

77
00:09:04,000 --> 00:09:09,000
and so he declared to be the disciple of the Buddha,

78
00:09:09,000 --> 00:09:14,000
who took refuge in the Buddha, the Dhamma and the Sangha,

79
00:09:14,000 --> 00:09:17,000
and then he left.

80
00:09:17,000 --> 00:09:22,000
So after he left, the Buddha said to his monks,

81
00:09:22,000 --> 00:09:27,000
if this king had not killed his own father,

82
00:09:27,000 --> 00:09:33,000
he would have become a Soda Panah on this very seat.

83
00:09:33,000 --> 00:09:36,000
Now that he had killed his own father,

84
00:09:36,000 --> 00:09:44,000
he was to suffer in hell,

85
00:09:44,000 --> 00:09:50,000
and so after death, King Adhara Sadhu fell to hell,

86
00:09:50,000 --> 00:09:55,000
and so now he is suffering in one of the great hells.

87
00:09:55,000 --> 00:10:00,000
So even though he was very devoted to the Buddha after that,

88
00:10:00,000 --> 00:10:07,000
that devotion to the Buddha could not save him,

89
00:10:07,000 --> 00:10:13,000
or could not prevent him from being reborn in hell.

90
00:10:13,000 --> 00:10:16,000
Now Adhara Sadhu was,

91
00:10:16,000 --> 00:10:26,000
Adhara Sadhu had the capability to gain enlightenment in that life,

92
00:10:26,000 --> 00:10:31,000
and he did get to see the Buddha,

93
00:10:31,000 --> 00:10:36,000
and to listen to the Dhamma talks of the Buddha.

94
00:10:36,000 --> 00:10:45,000
But he was unable to gain enlightenment,

95
00:10:45,000 --> 00:10:49,000
not only was he unable to gain enlightenment,

96
00:10:49,000 --> 00:10:55,000
but he went down to hell and is suffering there,

97
00:10:55,000 --> 00:11:00,000
because he had killed his own father,

98
00:11:00,000 --> 00:11:03,000
and he killed his own father,

99
00:11:03,000 --> 00:11:08,000
because he associated with a bad friend,

100
00:11:08,000 --> 00:11:11,000
and the venerable Devadatta.

101
00:11:11,000 --> 00:11:19,000
So associating with a bad friend can be that much damaging to one's prosperity,

102
00:11:19,000 --> 00:11:22,000
one's spiritual growth,

103
00:11:22,000 --> 00:11:27,000
one's rebirth.

104
00:11:27,000 --> 00:11:33,000
So associating with a bad friend

105
00:11:33,000 --> 00:11:38,000
is one reason why people do not get enlightenment,

106
00:11:38,000 --> 00:11:45,000
although they have the capability to become arachans in that life,

107
00:11:45,000 --> 00:11:49,000
and they get to see the Buddha,

108
00:11:49,000 --> 00:11:57,000
the first reason,

109
00:11:57,000 --> 00:12:01,000
lack of action, lack of exertion.

110
00:12:01,000 --> 00:12:07,000
Now lack of action is of two kinds,

111
00:12:07,000 --> 00:12:13,000
lack of action on the part of the teacher,

112
00:12:13,000 --> 00:12:17,000
and lack of action on the part of,

113
00:12:17,000 --> 00:12:22,000
let us say, students.

114
00:12:22,000 --> 00:12:28,000
A wandering acetic and a leap person

115
00:12:28,000 --> 00:12:33,000
went to see the Buddha.

116
00:12:33,000 --> 00:12:42,000
And when the acetic saw the monks surrounding the Buddha,

117
00:12:42,000 --> 00:12:47,000
very calm and quiet and disciplined,

118
00:12:47,000 --> 00:12:50,000
he was very glad,

119
00:12:50,000 --> 00:12:57,000
and so he expressed his gladness or appreciation of this tranquility

120
00:12:57,000 --> 00:13:00,000
of the monks to the Buddha.

121
00:13:00,000 --> 00:13:03,000
So the Buddha told him that,

122
00:13:03,000 --> 00:13:05,000
among the monks, there were arachans,

123
00:13:05,000 --> 00:13:07,000
sort-a-banas, and so on,

124
00:13:07,000 --> 00:13:11,000
and also those who practice the full foundations of mindfulness.

125
00:13:11,000 --> 00:13:17,000
When he had this, the leap person, his name was Pisa.

126
00:13:17,000 --> 00:13:19,000
So the leap person said,

127
00:13:19,000 --> 00:13:24,000
Monday, it's wonderful that you have taught the foundations of mindfulness

128
00:13:24,000 --> 00:13:27,000
to all people,

129
00:13:27,000 --> 00:13:30,000
even we, he said,

130
00:13:30,000 --> 00:13:32,000
as leap persons,

131
00:13:32,000 --> 00:13:39,000
practice the foundations of mindfulness from time to time.

132
00:13:39,000 --> 00:13:44,000
But he talked to them,

133
00:13:44,000 --> 00:13:49,000
telling them that there are four kinds of persons,

134
00:13:49,000 --> 00:13:52,000
and then asked Pisa,

135
00:13:52,000 --> 00:13:55,000
which person,

136
00:13:55,000 --> 00:14:00,000
he pleases him.

137
00:14:00,000 --> 00:14:05,000
And so Pisa answered that the fourth person,

138
00:14:05,000 --> 00:14:09,000
please ask his mind,

139
00:14:09,000 --> 00:14:12,000
and then Buddha asked him, why,

140
00:14:12,000 --> 00:14:16,000
say, he likes the last,

141
00:14:16,000 --> 00:14:19,000
or the fourth person, and he gave reasons.

142
00:14:19,000 --> 00:14:23,000
And after giving the reasons,

143
00:14:23,000 --> 00:14:24,000
he just said,

144
00:14:24,000 --> 00:14:28,000
Bhanti, I'm busy, so I'm not going.

145
00:14:28,000 --> 00:14:30,000
So the Buddha said,

146
00:14:30,000 --> 00:14:32,000
do what you think, fit.

147
00:14:32,000 --> 00:14:35,000
So he got up and left.

148
00:14:35,000 --> 00:14:37,000
When he left,

149
00:14:37,000 --> 00:14:40,000
Buddha said to the monks,

150
00:14:40,000 --> 00:14:45,000
monks, if Pisa would have stayed

151
00:14:45,000 --> 00:14:51,000
until the end of my exposition of the four types of individuals,

152
00:14:51,000 --> 00:14:56,000
he would have been endowed with great benefits.

153
00:14:56,000 --> 00:14:59,000
So that means the commentary explains

154
00:14:59,000 --> 00:15:02,000
that he would have become a sort of banner.

155
00:15:02,000 --> 00:15:04,000
But now that he left,

156
00:15:04,000 --> 00:15:07,000
before the end of the talk,

157
00:15:07,000 --> 00:15:11,000
that he was deprived of that opportunity.

158
00:15:11,000 --> 00:15:13,000
But Buddha said,

159
00:15:13,000 --> 00:15:17,000
although he was deprived of this opportunity,

160
00:15:17,000 --> 00:15:19,000
to become a sort of banner,

161
00:15:19,000 --> 00:15:24,000
he got some things from coming and seeing him,

162
00:15:24,000 --> 00:15:29,000
that he, Pisa, got faith,

163
00:15:29,000 --> 00:15:31,000
or devotion in the Buddha,

164
00:15:31,000 --> 00:15:38,000
and also he heard about the four foundations of mindfulness

165
00:15:38,000 --> 00:15:42,000
for his father practice.

166
00:15:42,000 --> 00:15:47,000
In this story, Pisa failed to become a sort of banner,

167
00:15:47,000 --> 00:15:52,000
not because he was not endowed with the capabilities,

168
00:15:52,000 --> 00:15:55,000
not because he was endowed with paramis,

169
00:15:55,000 --> 00:15:58,000
and not because he did not see the Buddha,

170
00:15:58,000 --> 00:16:00,000
because he did see the Buddha,

171
00:16:00,000 --> 00:16:03,000
and he talks to the Buddha, he listened to the Buddha.

172
00:16:03,000 --> 00:16:11,000
But because he left before the discourse was over.

173
00:16:11,000 --> 00:16:15,000
So that is non-action,

174
00:16:15,000 --> 00:16:18,000
or lack of action on his part.

175
00:16:18,000 --> 00:16:25,000
That means he should have stayed until the end of the exposition,

176
00:16:25,000 --> 00:16:30,000
he should have listened to the exposition given by the Buddha.

177
00:16:30,000 --> 00:16:33,000
And he did not do that,

178
00:16:33,000 --> 00:16:35,000
he did not make that effort.

179
00:16:35,000 --> 00:16:40,000
And so for lack of effort on his part,

180
00:16:40,000 --> 00:16:47,000
he was unable to become a sort of banner.

181
00:16:47,000 --> 00:16:56,000
Now this story teaches us one good lesson.

182
00:16:56,000 --> 00:17:02,000
That is, when someone is giving a Dharma talk,

183
00:17:02,000 --> 00:17:07,000
you should listen to the Dharma talk until the end.

184
00:17:07,000 --> 00:17:15,000
So you should not get up and leave in the middle of a Dharma talk.

185
00:17:15,000 --> 00:17:20,000
And people are impatient.

186
00:17:20,000 --> 00:17:26,000
If they do not think they are not getting anything out of the talk,

187
00:17:26,000 --> 00:17:29,000
then they wanted to leave.

188
00:17:29,000 --> 00:17:36,000
And they had no patience to wait until the end of the talk.

189
00:17:36,000 --> 00:17:48,000
And so the last opportunity of getting information about the teachings of the Buddha,

190
00:17:48,000 --> 00:17:53,000
information about how to practice meditation and so on.

191
00:17:53,000 --> 00:18:04,000
And so they also lose a great opportunity by getting up and leaving before the Dharma talk ends.

192
00:18:04,000 --> 00:18:07,000
You can say,

193
00:18:07,000 --> 00:18:12,000
or this time we are too busy and so we have to leave,

194
00:18:12,000 --> 00:18:19,000
but we can listen to the tapes later on.

195
00:18:19,000 --> 00:18:25,000
But listening a talk while it is being given

196
00:18:25,000 --> 00:18:31,000
and listening the recording later at different.

197
00:18:31,000 --> 00:18:34,000
So your frame of mind is different.

198
00:18:34,000 --> 00:18:41,000
When the person was talking that you have more attention,

199
00:18:41,000 --> 00:18:44,000
you pay to the talk.

200
00:18:44,000 --> 00:18:53,000
And also you have this article that eagerness to listen to the talk.

201
00:18:53,000 --> 00:18:58,000
But when you play a table and listen to it,

202
00:18:58,000 --> 00:19:07,000
although you may be listening, you have not as much attention.

203
00:19:07,000 --> 00:19:13,000
You pay to the talk or you have not as much respect to the talk.

204
00:19:13,000 --> 00:19:18,000
So although you can listen to the talk later on tapes,

205
00:19:18,000 --> 00:19:26,000
it is better to listen to the talk while it is being given.

206
00:19:26,000 --> 00:19:39,000
So that is the lack of exertion on the part of the student or the listener.

207
00:19:39,000 --> 00:19:49,000
And the other one is lack of exertion or lack of action on the part of the teacher.

208
00:19:49,000 --> 00:19:54,000
I was a Brahmin called Janusani.

209
00:19:54,000 --> 00:20:00,000
So one day the venerable Saribuddha had about him

210
00:20:00,000 --> 00:20:10,000
and asked a monk whether that Brahmin practiced as heatfulness.

211
00:20:10,000 --> 00:20:13,000
And the answer was no.

212
00:20:13,000 --> 00:20:19,000
So when he met that Brahmin, Saribuddha asked him,

213
00:20:19,000 --> 00:20:24,000
do you practice heatfulness? He said, no bande.

214
00:20:24,000 --> 00:20:33,000
We have many things to do and he gave 10 excuses why he was not mindful.

215
00:20:33,000 --> 00:20:37,000
So Saribuddha talked to him.

216
00:20:37,000 --> 00:20:43,000
The ill consequences of heedlessness.

217
00:20:43,000 --> 00:20:57,000
And then later when that Brahmin was a gravely ill actually on his deadbed,

218
00:20:57,000 --> 00:21:04,000
he sent a man that to give respects to the Buddha in his name

219
00:21:04,000 --> 00:21:12,000
and also to invite the venerable Saribuddha to him and to come and talk to him.

220
00:21:12,000 --> 00:21:17,000
So the venerable Saribuddha accepted and went to him

221
00:21:17,000 --> 00:21:25,000
and then asked him whether his sickness is increasing or decreasing.

222
00:21:25,000 --> 00:21:29,000
And he said, my sickness is increasing.

223
00:21:29,000 --> 00:21:38,000
And so Saribuddha, venerable Saribuddha taught him something,

224
00:21:38,000 --> 00:21:47,000
asking him which is better to be reborn in hell or to reborn as an animal

225
00:21:47,000 --> 00:21:51,000
and then to be reborn as an animal and to be reborn as a human being

226
00:21:51,000 --> 00:21:58,000
and so on and so on until the highest realm of the divas.

227
00:21:58,000 --> 00:22:04,000
And then venerable Saribuddha said, which is better to be reborn

228
00:22:04,000 --> 00:22:09,000
in the highest diva realm or in the Brahma realm.

229
00:22:09,000 --> 00:22:12,000
So when the Saribuddha said Brahma realm,

230
00:22:12,000 --> 00:22:16,000
the Brahmin was very, very heavy.

231
00:22:16,000 --> 00:22:21,000
Then he said, oh, Saribuddha said Brahma realm, Saribuddha said Brahma realm.

232
00:22:21,000 --> 00:22:27,000
Then the venerable Saribuddha thought that the Brahmin's attached

233
00:22:27,000 --> 00:22:33,000
to the realm of the Braamas because their way is to reach the world of the Brahma.

234
00:22:33,000 --> 00:22:39,000
So the venerable Saribuddha taught him the way to reach the Brahma world.

235
00:22:39,000 --> 00:22:44,000
That means Saribuddha taught him the four Brahma weharas,

236
00:22:44,000 --> 00:22:47,000
the four divine abiding.

237
00:22:47,000 --> 00:22:52,000
So after teaching him how to practice loving highness compassion,

238
00:22:52,000 --> 00:22:58,000
sympathy, joy and economicity, the venerable Saribuddha left.

239
00:22:58,000 --> 00:23:11,000
So sometime after he left, the Brahmin died and he was reborn as a Brahma.

240
00:23:11,000 --> 00:23:16,000
Maybe Saribuddha was heavy when he went back to the monastery

241
00:23:16,000 --> 00:23:25,000
with the thinking that he had shown the Brahmin the way to the Brahma world.

242
00:23:25,000 --> 00:23:31,000
But at that time, Buddha was at the monastery in the Dhamma hall,

243
00:23:31,000 --> 00:23:43,000
giving Dhamma talk to monks and Buddha said, here comes Saribuddha.

244
00:23:43,000 --> 00:23:52,000
Although he could do better, he has just taught the Brahmin,

245
00:23:52,000 --> 00:24:02,000
the way to the Brahma world, which is actually inferior to becoming an arhant.

246
00:24:02,000 --> 00:24:07,000
Then Saribuddha came to the Buddha and paid respect to him,

247
00:24:07,000 --> 00:24:13,000
and said, Bhante, Brahmin, Dhananjani, paid respects to you.

248
00:24:13,000 --> 00:24:18,000
Then the Buddha said, Saribuddha, although you can do better,

249
00:24:18,000 --> 00:24:25,000
you just put him on the road to Brahma world and you come back.

250
00:24:25,000 --> 00:24:31,000
Now, the text didn't say more about this,

251
00:24:31,000 --> 00:24:45,000
but the commentary explains that that was a hint,

252
00:24:45,000 --> 00:24:54,000
that gave to Saribuddha. That means Saribuddha, you go to him and preach to him again.

253
00:24:54,000 --> 00:24:59,000
So, Saribuddha knew, or Saribuddha got that hint.

254
00:24:59,000 --> 00:25:07,000
And so, he went up to the Brahma world and gave it this course

255
00:25:07,000 --> 00:25:15,000
to the Janus only who had become a great Brahma.

256
00:25:15,000 --> 00:25:21,000
We are not full whether that great Brahma became an arhant or not,

257
00:25:21,000 --> 00:25:24,000
but I think he became an arhant.

258
00:25:24,000 --> 00:25:27,000
But it is said that from the time on,

259
00:25:27,000 --> 00:25:32,000
whenever the venerable Saribuddha gave it Dhamma talk,

260
00:25:32,000 --> 00:25:37,000
even if it is just a verse of four lines,

261
00:25:37,000 --> 00:25:43,000
he always include the four noble truths.

262
00:25:43,000 --> 00:25:50,000
This story, the Brahmin Janus only possessed the capability

263
00:25:50,000 --> 00:25:53,000
that you become an arhant.

264
00:25:53,000 --> 00:26:00,000
And he got to see the Buddha and his chief disciples, Saribuddha.

265
00:26:00,000 --> 00:26:07,000
But he did not become an arhant because there was lack of action

266
00:26:07,000 --> 00:26:11,000
on the part of the venerable Saribuddha.

267
00:26:11,000 --> 00:26:18,000
That means a venerable Saribuddha could have taught him

268
00:26:18,000 --> 00:26:22,000
the way to enlightenment.

269
00:26:22,000 --> 00:26:27,000
The foundations of four foundations of mindfulness

270
00:26:27,000 --> 00:26:32,000
and Vipasana practice, but instead he thought that the Brahmins

271
00:26:32,000 --> 00:26:37,000
are attached to the Brahma world and so he taught him

272
00:26:37,000 --> 00:26:41,000
just the way to reach the world of the Brahmas.

273
00:26:41,000 --> 00:26:46,000
So, here Janus only feels to become an arhant

274
00:26:46,000 --> 00:26:51,000
because of lack of action,

275
00:26:51,000 --> 00:26:56,000
that the right action on the part of the venerable Saribuddha.

276
00:26:56,000 --> 00:27:01,000
So, we must remember these stories

277
00:27:01,000 --> 00:27:05,000
and take lessons from these stories.

278
00:27:05,000 --> 00:27:07,000
That is, we should not,

279
00:27:07,000 --> 00:27:11,000
or we must not associate with bad people,

280
00:27:11,000 --> 00:27:18,000
associate with people who will detract us from this spiritual path

281
00:27:18,000 --> 00:27:25,000
and who will take us to the ways of evil things

282
00:27:25,000 --> 00:27:29,000
and who will eventually take us to the four woeful states.

283
00:27:29,000 --> 00:27:41,000
And also, we should see to it that

284
00:27:41,000 --> 00:27:46,000
there is no fault on our part.

285
00:27:46,000 --> 00:27:56,000
If we are students, then we will fulfill the obligations of students.

286
00:27:56,000 --> 00:28:02,000
We will make effort, a student should make.

287
00:28:02,000 --> 00:28:08,000
And if we are teachers also, we will try to lead the students

288
00:28:08,000 --> 00:28:15,000
in a right destination.

289
00:28:15,000 --> 00:28:19,000
So, we should keep these stories in mind

290
00:28:19,000 --> 00:28:27,000
and try to avoid both the faults on the part of the students

291
00:28:27,000 --> 00:28:30,000
and on the part of the teachers.

292
00:28:30,000 --> 00:28:35,000
As teachers say, we will say to it

293
00:28:35,000 --> 00:28:38,000
that we give you the right method.

294
00:28:38,000 --> 00:28:43,000
We teach you the right method of practice

295
00:28:43,000 --> 00:28:48,000
and give you correct instructions

296
00:28:48,000 --> 00:28:52,000
and as students,

297
00:28:52,000 --> 00:28:57,000
now we amongst both teachers and students.

298
00:28:57,000 --> 00:29:06,000
So, as students, we should not be lacking in a proper effort

299
00:29:06,000 --> 00:29:12,000
and we should follow the instructions faithfully.

300
00:29:12,000 --> 00:29:20,000
Now, sometimes people come with what is called a pride

301
00:29:20,000 --> 00:29:25,000
in their former practices or our resistance.

302
00:29:25,000 --> 00:29:29,000
And so, when they come to a center

303
00:29:29,000 --> 00:29:35,000
and then they are unable to get rid of that resistance.

304
00:29:35,000 --> 00:29:40,000
And with that resistance, they cannot hope to make any progress

305
00:29:40,000 --> 00:29:45,000
or they cannot hope to get any benefit out of that practice.

306
00:29:45,000 --> 00:29:52,000
So, it is important that as students or as meditators,

307
00:29:52,000 --> 00:29:57,000
we try to follow the instructions faithfully.

308
00:29:57,000 --> 00:30:00,000
So, when you go to a center,

309
00:30:00,000 --> 00:30:04,000
then you follow these instructions given at that center

310
00:30:04,000 --> 00:30:09,000
and do not have any resistance or arguments or whatever.

311
00:30:09,000 --> 00:30:14,000
Because the instructions given at the standards

312
00:30:14,000 --> 00:30:18,000
are built up through years.

313
00:30:18,000 --> 00:30:24,000
And so, they are well tested instructions.

314
00:30:24,000 --> 00:30:28,000
So, it is important that as students say

315
00:30:28,000 --> 00:30:34,000
we follow the instructions faithfully and closely.

316
00:30:34,000 --> 00:30:40,000
The teachers want their students to follow the instructions closely

317
00:30:40,000 --> 00:30:42,000
and faithfully.

318
00:30:42,000 --> 00:30:49,000
And so, these students should make effort

319
00:30:49,000 --> 00:30:53,000
to follow the instructions.

320
00:30:53,000 --> 00:31:00,000
As I said, instructions given are accumulated through the years

321
00:31:00,000 --> 00:31:04,000
and so, the instructions are not just one teacher's instruction.

322
00:31:04,000 --> 00:31:09,000
It is the instruction of the whole tradition,

323
00:31:09,000 --> 00:31:15,000
which is it roots in the teachings of the Buddha.

324
00:31:15,000 --> 00:31:20,000
So, when these instructions are faithfully followed,

325
00:31:20,000 --> 00:31:27,000
then people can begin to get results from following these instructions.

326
00:31:27,000 --> 00:31:40,000
So, it is important that the teachers give correct guidance

327
00:31:40,000 --> 00:31:46,000
in the practice of meditation and the students or the meditators

328
00:31:46,000 --> 00:31:51,000
should follow the guidance of the teachers.

329
00:31:51,000 --> 00:32:02,000
I want you to remember the instructions and that is why I say the instructions again and again.

330
00:32:02,000 --> 00:32:06,000
Say it may be boring to some people,

331
00:32:06,000 --> 00:32:15,000
but many people reported to me that it is good to hear the instructions every now and then

332
00:32:15,000 --> 00:32:19,000
so that they do not forget the instructions.

333
00:32:19,000 --> 00:32:30,000
So, instructions repeated are actually for the benefit of the meditators

334
00:32:30,000 --> 00:32:38,000
and if the instructions are too often, please be patient

335
00:32:38,000 --> 00:32:47,000
and take them as the presence given again and again.

336
00:32:47,000 --> 00:32:55,000
So, by practicing, by acting correctly towards the instructions,

337
00:32:55,000 --> 00:32:58,000
that is, by following the instructions,

338
00:32:58,000 --> 00:33:06,000
the yogis will be able to get the results they expect from the practice of meditation.

339
00:33:06,000 --> 00:33:13,000
During this retreat, first I talked about the Dhamma heritage.

340
00:33:13,000 --> 00:33:21,000
So, we who are practicing Vipasana meditation are at least making right efforts

341
00:33:21,000 --> 00:33:26,000
to take the Dhamma heritage of the Buddha.

342
00:33:26,000 --> 00:33:29,000
Tell me about the burden.

343
00:33:29,000 --> 00:33:31,000
The burden of the aggregates.

344
00:33:31,000 --> 00:33:40,000
The heavy burden we are carrying every moment of our lives

345
00:33:40,000 --> 00:33:48,000
and also that we can at least not pick up a new burden

346
00:33:48,000 --> 00:33:57,000
by practicing Vipasana meditation and by the power of Vipasana meditation,

347
00:33:57,000 --> 00:34:06,000
we will be able to lay down the heavy burden once and for all.

348
00:34:06,000 --> 00:34:16,000
Last night I talked about what to do when we have pain or when we are sick.

349
00:34:16,000 --> 00:34:22,000
Although I say I talked about all these are the teachings of the Buddha.

350
00:34:22,000 --> 00:34:33,000
So, what you heard during this retreat are all the teachings of the Buddha

351
00:34:33,000 --> 00:34:36,000
related by me.

352
00:34:36,000 --> 00:34:44,000
So, we have a respect for these teachings, for these pieces of advice

353
00:34:44,000 --> 00:34:51,000
and we should follow the lessons or advices given in these discourses.

354
00:34:51,000 --> 00:34:55,000
What heritage must we take?

355
00:34:55,000 --> 00:35:04,000
Dhamma heritage or material heritage.

356
00:35:04,000 --> 00:35:09,000
We must at least make effort to take the Dhamma heritage.

357
00:35:09,000 --> 00:35:17,000
We want to put down the burden.

358
00:35:17,000 --> 00:35:23,000
Do you really want to put down the burden?

359
00:35:23,000 --> 00:35:28,000
If so, we have to practice Vipasana meditation.

360
00:35:28,000 --> 00:35:32,000
So, by the practice of Vipasana meditation, little by little,

361
00:35:32,000 --> 00:35:37,000
we make our burden less heavy.

362
00:35:37,000 --> 00:35:43,000
And ultimately when we are totally successful with our practice,

363
00:35:43,000 --> 00:35:47,000
then we will be able to put down the burden altogether.

364
00:35:47,000 --> 00:35:54,000
And when we experience pain or when we are sick,

365
00:35:54,000 --> 00:36:09,000
what will we do, complain or angry with the sickness or angry with people or blaming others?

366
00:36:09,000 --> 00:36:13,000
No, we will not do any of these things.

367
00:36:13,000 --> 00:36:21,000
So, we will try not to let our minds be sick,

368
00:36:21,000 --> 00:36:26,000
although our body is sick.

369
00:36:26,000 --> 00:36:35,000
So, we will make effort to take the Dhamma heritage of the Buddha.

370
00:36:35,000 --> 00:36:46,000
We will try our best to lay down this heavy burden of five aggregates.

371
00:36:46,000 --> 00:36:52,000
And although our body is sick,

372
00:36:52,000 --> 00:36:58,000
we will see to add that our mind is not sick.

373
00:36:58,000 --> 00:37:10,000
So, may we all be successful following these determinations

374
00:37:10,000 --> 00:37:15,000
and achieve our goal in this very life.

375
00:37:15,000 --> 00:37:36,000
Thank you.

