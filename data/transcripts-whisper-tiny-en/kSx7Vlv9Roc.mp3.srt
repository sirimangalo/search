1
00:00:00,000 --> 00:00:00,840
Go ahead.

2
00:00:00,840 --> 00:00:04,560
Ian asks, over spring break, I'm going to New York

3
00:00:04,560 --> 00:00:08,360
and we'll have the opportunity to go to a monastery.

4
00:00:08,360 --> 00:00:10,920
The problem is they practice samata.

5
00:00:10,920 --> 00:00:13,840
Should I go to learn and hear dharma

6
00:00:13,840 --> 00:00:15,840
or just stay to myself and meditate?

7
00:00:23,080 --> 00:00:24,000
Well, it's hard to say.

8
00:00:24,000 --> 00:00:29,440
I mean, there are many types of monasteries out there.

9
00:00:29,440 --> 00:00:33,360
And one thing I'm always trying to prepare people for

10
00:00:33,360 --> 00:00:37,120
is not be shocked when you come to a monastery

11
00:00:37,120 --> 00:00:42,520
and it's not what you expected because there are good and bad

12
00:00:42,520 --> 00:00:44,640
and everything in between.

13
00:00:44,640 --> 00:00:50,440
So probably the best thing is if you contact me in private

14
00:00:50,440 --> 00:00:51,880
and give me a link to their website

15
00:00:51,880 --> 00:00:57,400
and I can give you more details about what insight

16
00:00:57,400 --> 00:01:01,080
I have into that monastery, if any.

17
00:01:01,080 --> 00:01:03,040
Some of them are really good, especially in America.

18
00:01:03,040 --> 00:01:08,040
They do have some fairly representative monasteries.

19
00:01:08,040 --> 00:01:10,080
I'm just quite surprised to have people send me links

20
00:01:10,080 --> 00:01:13,480
and then go and check it out and find out that there's

21
00:01:13,480 --> 00:01:15,240
quite neat things going on.

22
00:01:15,240 --> 00:01:17,560
But you have to check out the situation.

23
00:01:17,560 --> 00:01:19,520
It's always good to go and hear the dharma

24
00:01:19,520 --> 00:01:24,720
and something that you'll find if you can overcome

25
00:01:24,720 --> 00:01:26,040
your initial disappointment.

26
00:01:26,040 --> 00:01:30,520
Like, oh, the monks are wearing pants or the monks

27
00:01:30,520 --> 00:01:33,080
are wearing shirts or the monks are cutting grass

28
00:01:33,080 --> 00:01:35,280
or the monks are driving cars or something,

29
00:01:35,280 --> 00:01:38,360
realizing that they're not what you expected, right?

30
00:01:38,360 --> 00:01:42,760
The monks have phones, the monks have computers.

31
00:01:42,760 --> 00:01:46,760
If you can get over that and really focus on the dharma,

32
00:01:46,760 --> 00:01:49,360
which is really what we're doing, where people would say,

33
00:01:49,360 --> 00:01:51,040
why does a monk have a video camera?

34
00:01:51,040 --> 00:01:52,400
Why does a monk have a computer?

35
00:01:52,400 --> 00:01:55,360
Why does a monk teach on the internet?

36
00:01:55,360 --> 00:01:58,880
Well, it's because that's where the people are.

37
00:01:58,880 --> 00:02:01,880
People are not coming from all over the world

38
00:02:01,880 --> 00:02:03,680
to hear our daily dharma discourses,

39
00:02:03,680 --> 00:02:05,760
but they can hear them over the internet

40
00:02:05,760 --> 00:02:10,800
because that's where people find their information.

41
00:02:10,800 --> 00:02:15,480
So what I mean is it may not be as you expect it when

42
00:02:15,480 --> 00:02:21,720
you go there, but even if the monks are not strong meditation

43
00:02:21,720 --> 00:02:23,920
practitioners, the great thing that I found

44
00:02:23,920 --> 00:02:27,720
is they often have incredible store of knowledge

45
00:02:27,720 --> 00:02:30,720
and you can benefit greatly just from listening

46
00:02:30,720 --> 00:02:32,920
to them or even just talking to them.

47
00:02:32,920 --> 00:02:34,720
What's always amazed me is even those monks

48
00:02:34,720 --> 00:02:39,760
that I would think, phew, he's not pure in his morality

49
00:02:39,760 --> 00:02:42,960
or he doesn't even practice, he just sits

50
00:02:42,960 --> 00:02:46,160
at a computer or he's just a study monk.

51
00:02:46,160 --> 00:02:49,240
It's amazing some of the insights they have just into life,

52
00:02:49,240 --> 00:02:52,920
just from living the life, maybe not in total purity,

53
00:02:52,920 --> 00:02:58,600
but in the ability to teach that they have,

54
00:02:58,600 --> 00:03:01,000
because of course you don't have to be enlightened to teach

55
00:03:01,000 --> 00:03:03,400
and you don't have to be enlightened.

56
00:03:03,400 --> 00:03:05,280
The funny thing is you don't have to be enlightened

57
00:03:05,280 --> 00:03:07,120
to enlighten other people.

58
00:03:07,120 --> 00:03:08,920
There was a story in the Buddhist time of this monk

59
00:03:08,920 --> 00:03:12,280
that I've talked about before, who all of his students

60
00:03:12,280 --> 00:03:15,240
became enlightened and he was still an ordinary person

61
00:03:15,240 --> 00:03:20,240
and finally his students found out and gave him a big scare

62
00:03:20,240 --> 00:03:25,240
and sent him off into the forest to meditate on his own.

63
00:03:25,240 --> 00:03:28,520
But from your point of view, you can, for sure,

64
00:03:28,520 --> 00:03:31,520
you can always learn something from people.

65
00:03:31,520 --> 00:03:35,160
And if you keep that in mind and not have any expectations,

66
00:03:35,160 --> 00:03:36,720
then yeah, go and learn from,

67
00:03:36,720 --> 00:03:40,320
go and hear what they have to say and don't expect

68
00:03:40,320 --> 00:03:43,520
that you have to devote your, fall down at their knees

69
00:03:43,520 --> 00:03:46,120
and devote yourself to them for the rest of your life,

70
00:03:46,120 --> 00:03:47,320
but you can gain something for them.

71
00:03:47,320 --> 00:03:50,200
Be courteous, be respectful, don't go thinking,

72
00:03:50,200 --> 00:03:53,440
I'm better than you, but I'll pretend to hear what you say

73
00:03:53,440 --> 00:03:54,440
or something.

74
00:03:54,440 --> 00:03:58,760
Go with an genuine interest and appreciation

75
00:03:58,760 --> 00:04:01,160
for whatever they have to offer to you.

76
00:04:02,160 --> 00:04:04,240
And in general, I think you won't be disappointed

77
00:04:04,240 --> 00:04:06,440
if you do that and you'll make them really happy.

78
00:04:06,440 --> 00:04:08,840
It's always nice to have people come and visit

79
00:04:08,840 --> 00:04:10,960
who are interested in meditation in the truth.

80
00:04:11,960 --> 00:04:14,560
And often you'll find that they'll,

81
00:04:14,560 --> 00:04:15,800
I don't know what the center is,

82
00:04:15,800 --> 00:04:19,520
but you'll find that they'll let you practice as you like.

83
00:04:19,520 --> 00:04:21,840
They'll say, okay, if I will come and do practice

84
00:04:21,840 --> 00:04:23,200
and they won't give you much instruction,

85
00:04:23,200 --> 00:04:27,720
but they'll sit you down and they'll do chanting with you

86
00:04:27,720 --> 00:04:29,080
and then they'll do group meditation

87
00:04:29,080 --> 00:04:30,280
and you can do as you like.

88
00:04:31,360 --> 00:04:33,400
If it's a dedicated, really a dedicated

89
00:04:33,400 --> 00:04:35,120
summer to meditation center,

90
00:04:36,040 --> 00:04:39,320
then you have to decide whether it's in your interest

91
00:04:39,320 --> 00:04:40,320
to develop summer to,

92
00:04:40,320 --> 00:04:42,760
because summer to meditation takes a long time to develop

93
00:04:42,760 --> 00:04:44,880
and it takes dedication.

94
00:04:44,880 --> 00:04:47,560
And if it's a dedicated center of any sort,

95
00:04:47,560 --> 00:04:49,200
whatever meditation center you go to,

96
00:04:49,200 --> 00:04:50,840
you should dedicate yourself to,

97
00:04:50,840 --> 00:04:52,480
you should be prepared to dedicate yourself

98
00:04:52,480 --> 00:04:53,960
to their technique.

99
00:04:53,960 --> 00:04:56,000
If you don't dedicate themselves to your technique,

100
00:04:56,000 --> 00:04:57,560
you should be a, you, you,

101
00:05:01,040 --> 00:05:04,360
well, you shouldn't, you should not go to their center.

102
00:05:04,360 --> 00:05:08,520
You're, you're out of line with the protocol

103
00:05:08,520 --> 00:05:12,240
of that center, meditation centers really do expect

104
00:05:12,240 --> 00:05:15,640
their students to follow their technique as we do.

105
00:05:15,640 --> 00:05:17,840
And it's the, it's the one thing that sort of separates

106
00:05:17,840 --> 00:05:19,920
an ordinary monastery from a meditation center

107
00:05:19,920 --> 00:05:22,120
because a place like Watbanana Chad,

108
00:05:22,120 --> 00:05:25,200
apparently allows you to practice any sort of meditation

109
00:05:25,200 --> 00:05:27,600
and even advisors you to go and learn meditation elsewhere

110
00:05:27,600 --> 00:05:28,600
before coming or,

111
00:05:30,360 --> 00:05:33,160
but a place that is designated itself

112
00:05:33,160 --> 00:05:34,960
as a meditation training center,

113
00:05:34,960 --> 00:05:37,240
as we have in this, maybe perhaps this place,

114
00:05:37,240 --> 00:05:42,240
is that you're, you're expected to follow their technique

115
00:05:42,600 --> 00:05:45,040
and, and because of the time that it takes to develop

116
00:05:45,040 --> 00:05:48,520
some of the, if it's just for spring bake, it may not be,

117
00:05:48,520 --> 00:05:50,280
may not be totally worth it.

118
00:05:50,280 --> 00:05:52,600
Make up, you might get a technique that you can bring home.

119
00:05:57,160 --> 00:06:01,240
To practice, some of the time is, is not bad.

120
00:06:02,600 --> 00:06:06,200
As long as you don't forget that you come back

121
00:06:06,200 --> 00:06:08,240
to Vipassana afterwards.

122
00:06:08,240 --> 00:06:11,960
So if you, you can have some benefits,

123
00:06:11,960 --> 00:06:16,960
although if it's only over the spring break,

124
00:06:17,040 --> 00:06:22,040
as Bantas says, it's, you just can get a glimpse of it.

125
00:06:28,360 --> 00:06:33,360
And I think the most important is that you,

126
00:06:33,360 --> 00:06:43,360
you open your heart for the dhamma that they have.

