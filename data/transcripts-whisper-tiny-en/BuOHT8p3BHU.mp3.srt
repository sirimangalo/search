1
00:00:00,000 --> 00:00:04,480
Okay, good evening.

2
00:00:04,480 --> 00:00:10,240
So we're no longer doing live broadcasts, I think, not an immediate future.

3
00:00:10,240 --> 00:00:18,960
The live is, well, doing it this way gives me the option of having better quality broadcasts

4
00:00:18,960 --> 00:00:19,960
recordings.

5
00:00:19,960 --> 00:00:25,600
Anyway, most importantly, it's for you guys who are here doing the course.

6
00:00:25,600 --> 00:00:39,680
That's the core of what I feel is my work as, well, in sharing the dhamma course, the

7
00:00:39,680 --> 00:00:45,280
core of my work is my own, but in terms of sharing the dhamma with others, the core of it,

8
00:00:45,280 --> 00:00:50,560
from my perspective is, you're coming here to do the course, so I put myself at your disposal

9
00:00:50,560 --> 00:00:54,800
and does anything you need let me know.

10
00:00:54,800 --> 00:01:01,640
Part of it is giving regular talks, so I'm happy to do that for you, just to give encouragement

11
00:01:01,640 --> 00:01:11,520
and background and direction, trying to always keep it in line with the Buddha's teaching,

12
00:01:11,520 --> 00:01:16,880
not give you my perspective on things, but give you something that I think is greater

13
00:01:16,880 --> 00:01:19,080
than that and more valuable.

14
00:01:19,080 --> 00:01:27,920
It's the Buddha's perspective, so tonight I thought I would talk about something that seems

15
00:01:27,920 --> 00:01:35,640
to have been very important to the Buddha, and it seems fairly easy to see why that is,

16
00:01:35,640 --> 00:01:41,560
and not just important to the Buddha, but also important to his followers, it seems, because

17
00:01:41,560 --> 00:01:49,000
we know this because we have it in several places while we have it repeated, so something

18
00:01:49,000 --> 00:01:56,040
and might know about the Buddha's teaching as it's been recorded as there's a lot of repetition.

19
00:01:56,040 --> 00:02:01,480
But in this case, it's quite curious because we have a set of discourses called the

20
00:02:01,480 --> 00:02:07,920
Majimani kai, the middle-length discourses, it's one of the more prominent sets, because

21
00:02:07,920 --> 00:02:13,240
there's 152 different talks that the Buddha gave, and they're medium-sized, they're

22
00:02:13,240 --> 00:02:20,320
not really the long ones, but they're not also very short ones that are also collected.

23
00:02:20,320 --> 00:02:24,360
They're sort of in the middle, and so they're early good sort of entrance to the Buddha's

24
00:02:24,360 --> 00:02:25,360
teaching.

25
00:02:25,360 --> 00:02:29,760
There's just so much, such a diversity of topics.

26
00:02:29,760 --> 00:02:36,600
But one section near the end has, I haven't counted them, but there's several repetitions

27
00:02:36,600 --> 00:02:42,960
of this teaching, and they're not very long.

28
00:02:42,960 --> 00:02:48,920
But it's the same teaching again and again, and it's the monks repeating it, or the Buddha

29
00:02:48,920 --> 00:02:54,680
teaching it to them in various situations.

30
00:02:54,680 --> 00:03:00,760
So it seems to have been a teaching in the Buddha that was kept very closely in mind by

31
00:03:00,760 --> 00:03:05,360
his followers, and once you're here, I think, understand why.

32
00:03:05,360 --> 00:03:18,240
It's called the Bidekarata, which means a single, excellent night.

33
00:03:18,240 --> 00:03:20,680
There's a bit of controversy about that translation.

34
00:03:20,680 --> 00:03:28,280
Some people translate it as something else, it's because of how there's an ambiguity

35
00:03:28,280 --> 00:03:39,800
in the word, but it fairly clearly, from my perspective, refers to having one good night.

36
00:03:39,800 --> 00:03:47,320
And in Pali, something you have to understand is the word night is used in the same sense

37
00:03:47,320 --> 00:03:50,960
that we use the word day in English.

38
00:03:50,960 --> 00:03:54,840
So when we talk about having a good day, they would talk about having a good night.

39
00:03:54,840 --> 00:04:00,520
Why we know that is because that's how they talked about the passing of time, if you

40
00:04:00,520 --> 00:04:04,920
talk about how long something has gone on, you would say how many nights has it gone

41
00:04:04,920 --> 00:04:05,920
on.

42
00:04:05,920 --> 00:04:10,360
So instead of saying how many days you've been using, how many nights have you been here?

43
00:04:10,360 --> 00:04:16,720
When you talk about seniority, they would ask, they would talk about it in terms of having

44
00:04:16,720 --> 00:04:17,720
more nights.

45
00:04:17,720 --> 00:04:22,120
There's a term called rata new, which means one who knows many nights.

46
00:04:22,120 --> 00:04:25,880
The rata new is the most senior.

47
00:04:25,880 --> 00:04:30,440
So we have a monk in the Buddha's teaching, the most senior monk is called Anyokundanya.

48
00:04:30,440 --> 00:04:35,000
He was the first or the Indpiku.

49
00:04:35,000 --> 00:04:37,080
And so he's called rata new.

50
00:04:37,080 --> 00:04:42,600
One who knows, one who has lived through many nights.

51
00:04:42,600 --> 00:04:50,040
So when we hear Vedic rata, ekam means one, buddha is like buddha, buddha means exalted

52
00:04:50,040 --> 00:04:55,520
or it translates as exland, but it really means something like exalted when you put someone

53
00:04:55,520 --> 00:05:03,520
up high, you call them buddha, or buddha, I think, buddha is the word you refer to someone

54
00:05:03,520 --> 00:05:09,120
who is high, who you've put up like reverend or venerable.

55
00:05:09,120 --> 00:05:16,360
So having one venerable night, no, probably one exalted night.

56
00:05:16,360 --> 00:05:22,960
But in colloquial terms, it just means having a good day when you have a really good day,

57
00:05:22,960 --> 00:05:30,240
one good day, because it's a way of describing what you need, what is required, what

58
00:05:30,240 --> 00:05:32,880
is the goal?

59
00:05:32,880 --> 00:05:41,920
The goal is this one moment where you see everything clearly, getting to the point where

60
00:05:41,920 --> 00:05:43,000
you have a good day.

61
00:05:43,000 --> 00:05:48,640
Because it refers or it hints back to the buddha's good day and the buddha's good night

62
00:05:48,640 --> 00:05:53,040
when the buddha sat under the Bodhi tree and stayed up all night and throughout the night

63
00:05:53,040 --> 00:05:57,720
and three watches of the night, he saw three different things and the third watch was

64
00:05:57,720 --> 00:06:04,760
where he freed him, he became free from suffering.

65
00:06:04,760 --> 00:06:15,880
So it's in that vein that we think of having a good night.

66
00:06:15,880 --> 00:06:22,480
So the teaching goes, starts with something that should be very familiar to you.

67
00:06:22,480 --> 00:06:26,560
I repeat it in you, I've surely heard it from other places in Buddhism.

68
00:06:26,560 --> 00:06:42,800
I don't go back to the past or bring up the past and don't worry about the future.

69
00:06:42,800 --> 00:07:04,960
So fairly simple teaching and it is I think a good doorway, a good entrance to the practice

70
00:07:04,960 --> 00:07:10,400
of mindfulness to learn about being in the present moment.

71
00:07:10,400 --> 00:07:16,920
Being in the past and in the future they're not the only problems of course, but they're

72
00:07:16,920 --> 00:07:23,560
the first problem that we come to a meditation, the first challenge that we work on is

73
00:07:23,560 --> 00:07:28,920
in terms of trying to keep our mind from planning, from living in the future and to keep

74
00:07:28,920 --> 00:07:32,760
our mind from dwelling in the past.

75
00:07:32,760 --> 00:07:41,560
It's a common problem that can become quite intense for people when they are mourning

76
00:07:41,560 --> 00:07:47,400
loss or when they've had things happen to them in the past that we're just traumatic

77
00:07:47,400 --> 00:07:53,000
and meditators who are traumatized so that they couldn't let go of the past.

78
00:07:53,000 --> 00:07:57,160
People who are worried about the future anxious about the future can become quite an

79
00:07:57,160 --> 00:07:58,160
obsession.

80
00:07:58,160 --> 00:08:00,080
Of course for all of us it's a problem.

81
00:08:00,080 --> 00:08:07,560
When you finish the course you know you start thinking about the future, it's like drilling

82
00:08:07,560 --> 00:08:08,560
into your head.

83
00:08:08,560 --> 00:08:16,600
You can't get it out, it's very strong, you'll notice it can be very strong.

84
00:08:16,600 --> 00:08:21,600
So a very important teaching that's not mentioned here but what this relates to is the

85
00:08:21,600 --> 00:08:29,920
Buddha's teaching on how it, what is the reality of living in the past and in the future

86
00:08:29,920 --> 00:08:36,920
and he talks about, I think in the jataka, he talks about it like when you cut grass,

87
00:08:36,920 --> 00:08:40,880
when you live in the past or when you live in the future, you're cut off from reality

88
00:08:40,880 --> 00:08:48,160
and so your mind dries up like when you cut grass and there's a verse that goes something

89
00:08:48,160 --> 00:08:53,440
like, for the past I do not mourn or for the future weep.

90
00:08:53,440 --> 00:08:58,800
I take the present as it comes and thus my color keep, that's a translation of the jataka

91
00:08:58,800 --> 00:09:05,080
and that they've turned into poetry.

92
00:09:05,080 --> 00:09:09,880
But the problem is when you live in the past or live in the future it's abstract, it's

93
00:09:09,880 --> 00:09:14,560
conceptual, it's a very different state of mind than being mindful.

94
00:09:14,560 --> 00:09:19,120
If you sit here and you're aware of the breath, you're aware of yourself sitting, you're

95
00:09:19,120 --> 00:09:23,800
aware of the sound, you're here, you're aware of the sights and so on, you have a very

96
00:09:23,800 --> 00:09:32,320
simple connection with reality, a very simple experience of things and so there's very little

97
00:09:32,320 --> 00:09:38,320
stress and it's very, not very taxing on the mind.

98
00:09:38,320 --> 00:09:42,520
When you start getting into the past, the future bites, very nature, it requires more

99
00:09:42,520 --> 00:09:46,600
mind work and there's of course much more that can come up.

100
00:09:46,600 --> 00:09:51,720
If you see something, it's only very simple defilements that can come up, maybe you like

101
00:09:51,720 --> 00:10:00,560
it or you dislike it, but if you get into abstract thinking, so many of your phobias or

102
00:10:00,560 --> 00:10:09,760
neuroses can come up, so many potential problems, addictions and so on.

103
00:10:09,760 --> 00:10:14,080
So staying out of the past in the future it becomes a very important teaching, but there's

104
00:10:14,080 --> 00:10:19,880
another part to this and it's sort of another step, there's two parts, two main parts

105
00:10:19,880 --> 00:10:21,400
to this teaching.

106
00:10:21,400 --> 00:10:28,000
So the next part goes, but chupa nanjayo da mung tata tata vipasati and it's easy to sort

107
00:10:28,000 --> 00:10:35,280
of skip this one over because it says, whatever dhamma is, whatever dhamma's arise in the

108
00:10:35,280 --> 00:10:41,960
present moment in front of you, see them clearly.

109
00:10:41,960 --> 00:10:46,920
And so you might just take this as an extension of the first part, whereby we stay in

110
00:10:46,920 --> 00:10:50,560
the present, but it's not enough to be in the present.

111
00:10:50,560 --> 00:10:56,080
It's almost enough or in a way it's enough, but it's not simply to be in the present,

112
00:10:56,080 --> 00:11:00,800
but it's to see the present clearly and the commentary makes this clear that, I know the

113
00:11:00,800 --> 00:11:06,680
Buddha actually makes this clear when he talks about, just so he explains this after he

114
00:11:06,680 --> 00:11:13,840
gives this couple of verses, he explains it and he says it's possible to be defeated by

115
00:11:13,840 --> 00:11:19,200
the present moment, even when you're out of the past and out of the future, even the present

116
00:11:19,200 --> 00:11:24,280
moment, of course, can be a cause for the arising of conceptual thought, specifically

117
00:11:24,280 --> 00:11:31,160
the arising of ego, the idea that this is me, this is mine, if you see something or

118
00:11:31,160 --> 00:11:36,280
anything, you hear something, someone says to you, you're good, you're bad, if someone

119
00:11:36,280 --> 00:11:41,120
says good things about you, you're puffed up, if someone says bad things about you, you're

120
00:11:41,120 --> 00:11:48,040
angry or upset or depressed or lowered into your self-esteem.

121
00:11:48,040 --> 00:11:51,720
So it's not simply to be in the present moment, it's to see clearly in the present moment.

122
00:11:51,720 --> 00:11:55,240
The point being, you can only see the present moment, it's only the present moment that

123
00:11:55,240 --> 00:12:00,440
you can see clearly, it's not possible to see clearly about the past or the future,

124
00:12:00,440 --> 00:12:05,840
not in the way that the Buddha describes a vipasati, if you caught that, that's just another

125
00:12:05,840 --> 00:12:14,480
form of the word vipasana, vipasana is the noun, vipasati is the verb, one sees clearly.

126
00:12:14,480 --> 00:12:18,720
So seeing the present clearly is a description of what you're doing when you practice mindfulness,

127
00:12:18,720 --> 00:12:25,240
and it's the practice of mindfulness, it is the heart of this teaching, it allows you

128
00:12:25,240 --> 00:12:37,000
to break through conception, the concept, and abstract thinking, and see clearly reality.

129
00:12:37,000 --> 00:12:41,320
In a very simple sense of what's really there, not what you think is there, not what

130
00:12:41,320 --> 00:12:48,960
someone tells you is there, not what you believe is there.

131
00:12:48,960 --> 00:12:55,000
Of course, there's so much that comes from that, you start to see suffering and why

132
00:12:55,000 --> 00:13:01,480
you suffer, you start to see your emotions, positive and negative, qualities of mind that

133
00:13:01,480 --> 00:13:08,720
are beneficial to yourself and others, and harmful to yourselves and others.

134
00:13:08,720 --> 00:13:16,800
When you work through that, it's a process of change, mindfulness, insight, clarity of

135
00:13:16,800 --> 00:13:24,400
mind, change is so much, but hopefully you can start to see that it cleans so much.

136
00:13:24,400 --> 00:13:28,920
That's the essence of the Vedicarata teaching, the rest is more like a pepthak, it's a

137
00:13:28,920 --> 00:13:35,840
very beautiful teaching, it's a very short and concise, that's the first half.

138
00:13:35,840 --> 00:13:42,840
And it's one of the things he said most, any time anyone was in the past or in the future

139
00:13:42,840 --> 00:13:47,400
which of course often happens, he would repeat this person poly and then just the first

140
00:13:47,400 --> 00:13:56,960
part and then translate it into time, it's a very powerful one, and it's expressed in

141
00:13:56,960 --> 00:14:00,920
many different ways in the Buddha's teaching, but here this verse is literally repeated

142
00:14:00,920 --> 00:14:07,040
several times in a row and several students.

143
00:14:07,040 --> 00:14:14,680
So the next part relates more to why we should do this, and it's sort of a classic

144
00:14:14,680 --> 00:14:20,080
carrot and stick, talking about the dangers, if you don't, or the bad stuff, reasons

145
00:14:20,080 --> 00:14:26,320
why you should, because there's bad things and the good things that come of it.

146
00:14:26,320 --> 00:14:35,920
So the first part is asangiranga sankupam, this is unshakable, this is invincible.

147
00:14:35,920 --> 00:14:43,480
Tanvidhvamam ruhayi, one should be sure of this, knowing this one should be sure of it,

148
00:14:43,480 --> 00:14:46,480
something like that.

149
00:14:46,480 --> 00:14:54,240
I will pause here because this is, I think, a very important statement, and I mentioned

150
00:14:54,240 --> 00:15:09,240
this often to meditators and in my talks, about how perfect and potent, how efficacious,

151
00:15:09,240 --> 00:15:17,360
a big word, means how this practice is able to bring about results.

152
00:15:17,360 --> 00:15:25,320
So when we think of all of the methods and schemes and plans we have for finding happiness

153
00:15:25,320 --> 00:15:35,280
and peace and stability and so on, all of our plans and ambitions, they fall short.

154
00:15:35,280 --> 00:15:40,520
They can't help but fail because they're not permanent.

155
00:15:40,520 --> 00:15:44,480
They don't lead to something that's permanent, and the idea of a thing that is permanent

156
00:15:44,480 --> 00:15:49,360
is a red herring in the first place.

157
00:15:49,360 --> 00:15:55,360
If you try to create stability in your life, you know that that's only temporary.

158
00:15:55,360 --> 00:16:00,800
If you try to create pleasure, you should know that that's only temporary as well and problematic

159
00:16:00,800 --> 00:16:07,760
because these attempts that we may get us into dependency.

160
00:16:07,760 --> 00:16:11,880
And that's the key is because seeing clearly and being mindful and being in the present

161
00:16:11,880 --> 00:16:18,520
moment is independent, you can never be taken away from the present moment.

162
00:16:18,520 --> 00:16:25,200
No one can say to you, no, we're going to put you over here and remove you from the present

163
00:16:25,200 --> 00:16:26,200
moment.

164
00:16:26,200 --> 00:16:30,560
It's experience is the one thing that no one can take away from you.

165
00:16:30,560 --> 00:16:38,040
So if experience becomes your refuge rather than a curse or something that you're constantly

166
00:16:38,040 --> 00:16:42,160
running away from, which is a very important point, that's what we spend a lot of our

167
00:16:42,160 --> 00:16:43,160
time doing.

168
00:16:43,160 --> 00:16:48,680
We're so afraid or incapable of being in the present moment that we're always running

169
00:16:48,680 --> 00:16:54,920
into the past or the future, chasing after pleasures, chasing after things that can't

170
00:16:54,920 --> 00:16:56,680
possibly satisfy us.

171
00:16:56,680 --> 00:17:06,880
That the present is something so unfamiliar, scary, uninteresting, repulsive almost.

172
00:17:06,880 --> 00:17:19,920
But if we can change that by coming to be present and be alert and we'll have a clarity

173
00:17:19,920 --> 00:17:23,680
of mind about it, we become invincible.

174
00:17:23,680 --> 00:17:26,920
That's an important word.

175
00:17:26,920 --> 00:17:31,200
The only way you can become invincible is if you're able to accept everything, if you're

176
00:17:31,200 --> 00:17:40,560
able to not accept but able to be unfazed, unmoved, unshaken, a sungirang, a sungupan.

177
00:17:40,560 --> 00:17:45,640
So this is an important thing for you to understand and to get a sense of regardless

178
00:17:45,640 --> 00:17:51,320
of what results you've gained from the practice otherwise, like peace or happiness or all

179
00:17:51,320 --> 00:17:54,600
these good things we talk about.

180
00:17:54,600 --> 00:18:01,160
Knowing that is far more powerful because if you do the right thing, this is an important

181
00:18:01,160 --> 00:18:02,600
concept as well.

182
00:18:02,600 --> 00:18:08,200
If you do the right thing, whatever that means, once you get a sense that this is the right

183
00:18:08,200 --> 00:18:12,200
thing, if you're clear on that, you don't have to ever worry about what's going to be

184
00:18:12,200 --> 00:18:13,200
the result.

185
00:18:13,200 --> 00:18:17,400
You don't have to say, okay, I'm doing the right thing but is it going to lead to good things?

186
00:18:17,400 --> 00:18:20,440
It's such a wrong question.

187
00:18:20,440 --> 00:18:26,840
It's an absurd question because the only way it could be right is if it were leading to

188
00:18:26,840 --> 00:18:27,840
good things.

189
00:18:27,840 --> 00:18:32,440
I think that it helps very much to overcome doubt or you don't have to worry about results

190
00:18:32,440 --> 00:18:33,440
ever.

191
00:18:33,440 --> 00:18:39,200
It's a very important quality of mindfulness that you never worry about results.

192
00:18:39,200 --> 00:18:41,840
You're always focused on, is it right?

193
00:18:41,840 --> 00:18:50,080
Is this truly unshakable, is this truly invincible and am I truly doing something?

194
00:18:50,080 --> 00:18:57,080
And doing something that will make me safe and the reason why we can say that this is

195
00:18:57,080 --> 00:19:02,480
the case with such assurance is because of how simple it is, because of how pure it is.

196
00:19:02,480 --> 00:19:08,560
I'm not telling you to believe in God or believe in the Buddha or pray or do rituals or

197
00:19:08,560 --> 00:19:12,320
take one faith, some doctrine or so on.

198
00:19:12,320 --> 00:19:15,480
I'm saying, see clearly what's here and now, right?

199
00:19:15,480 --> 00:19:20,040
So when this becomes the teaching, the core teaching with nothing else on the dulled

200
00:19:20,040 --> 00:19:27,560
to rate it, then you can have perfect confidence if you're straight in your own mind,

201
00:19:27,560 --> 00:19:31,440
you can have perfect confidence that it's the right thing.

202
00:19:31,440 --> 00:19:38,000
It is what it says to be.

203
00:19:38,000 --> 00:19:46,240
And then the next part, Ajayvakitya Matabang, Kojanya Maranang's way, another important part

204
00:19:46,240 --> 00:19:48,360
of it that's often repeated.

205
00:19:48,360 --> 00:20:00,120
Ajayvakitya Matabang, today, the task should be done with effort, today the work should

206
00:20:00,120 --> 00:20:02,080
be done with effort.

207
00:20:02,080 --> 00:20:12,120
Kojanya Maranang's way, who knows whether death might come even tomorrow.

208
00:20:12,120 --> 00:20:17,120
This is why we, one of the reasons, anyway, it's an important reminder, we don't know

209
00:20:17,120 --> 00:20:24,680
what's coming in the future, I don't know, death probably not because it's not the most

210
00:20:24,680 --> 00:20:29,320
common thing for someone to die, though, absolutely there's a chance that any one of us

211
00:20:29,320 --> 00:20:36,680
in this room or listening here could die tomorrow, nobody knows, but anything could happen,

212
00:20:36,680 --> 00:20:47,080
someone near you could die, someone, you could lose something, you could get sick, anything

213
00:20:47,080 --> 00:20:48,080
could happen.

214
00:20:48,080 --> 00:20:53,000
There could be a tsunami, you know, when we were in Thailand and the tsunami came, how many

215
00:20:53,000 --> 00:21:00,680
people their whole life was destroyed, even if they did survive the horror of the aftermath

216
00:21:00,680 --> 00:21:06,240
of that big tsunami, anything could happen.

217
00:21:06,240 --> 00:21:12,000
So we do the work today, get ready, because all of that relates to change, and it's

218
00:21:12,000 --> 00:21:19,520
change that creates this instability and suffering, and so the part of being invincible

219
00:21:19,520 --> 00:21:27,760
is being flexible, being able to adapt, how can you adapt, by making your base and your

220
00:21:27,760 --> 00:21:33,400
refuge experience, because once you understand that our familiar with experience and

221
00:21:33,400 --> 00:21:38,320
are unfazed and unshaken by it, then you've covered everything, because there's nothing

222
00:21:38,320 --> 00:21:42,240
but experience.

223
00:21:42,240 --> 00:21:52,960
Ajayevakitsamata pankantanya Maranamsui, nahi nosangaranti nahi nahi nahi nahi nahi nahi nahi

224
00:21:52,960 --> 00:21:54,200
nahi nahi nahi nahi nahi nahi nahi nahi nahi nahi nahi nahi nahi nahi nahi.

225
00:21:54,200 --> 00:22:03,360
There's no bargaining with it, death, there's no bargaining with death, death with its great

226
00:22:03,360 --> 00:22:15,040
army, Mahasana, death, death is, I mean it is special, you could talk like this about

227
00:22:15,040 --> 00:22:25,160
anything, but death is a very good example, and probably the prime one, because we don't

228
00:22:25,160 --> 00:22:31,000
know, not only do we not know when it's going to come, we know that it is going to come,

229
00:22:31,000 --> 00:22:36,920
and it's very much, it's quite a vivid imagery, this idea of death with its armies.

230
00:22:36,920 --> 00:22:43,000
I see that if you, well you guys, some of you are fairly young, but as you get older and

231
00:22:43,000 --> 00:22:47,600
people start dying around you, you start to get a sense of death being like an army, I

232
00:22:47,600 --> 00:22:56,360
mean I'm still young, I'm 40, but as my three of my grandparents were gone, I had a cousin

233
00:22:56,360 --> 00:23:06,000
fall off a roof a few years ago, he's dead, my stepfather just found out he has cancer,

234
00:23:06,000 --> 00:23:10,320
death is coming, and we can feel the fortress weakening around us, you look and you see

235
00:23:10,320 --> 00:23:19,160
you're getting crow's nests gray hair, arthritis, you're getting fat and old and your mind

236
00:23:19,160 --> 00:23:22,840
is growing weak and so on.

237
00:23:22,840 --> 00:23:29,760
You can feel death pounding down, you're pounding down the doors, Mahasani and that great

238
00:23:29,760 --> 00:23:36,120
army, it's not just one threat, it's like one of those horror movies and you're in

239
00:23:36,120 --> 00:23:41,760
the house and people start dying around you and you just know you're next.

240
00:23:41,760 --> 00:23:50,120
When I say it with a smile, because we kind of have something powerful against it, that's

241
00:23:50,120 --> 00:23:58,880
the teaching, really the essence of the Madhekarata.

242
00:23:58,880 --> 00:24:07,680
So then the Buddha says, a one we're hiring out of being a horror tama tanditam, one working

243
00:24:07,680 --> 00:24:17,840
this day and night, a horror tam, a tanditam not flagging, not relaxing, not slag it, slag

244
00:24:17,840 --> 00:24:32,760
it, you know, it's the word slagging off, tang wai, tang wai bade kara toti, such a person is

245
00:24:32,760 --> 00:24:44,120
called the bade karaata, one who has a single excellent night, has a good night, a good day,

246
00:24:44,120 --> 00:24:54,800
one to a jikataimuni, some to a jikataimuni, I'm not sure if that's calling, I think

247
00:24:54,800 --> 00:24:58,640
that might be calling the Buddha talking about the Buddha as being peaceful, actually.

248
00:24:58,640 --> 00:25:04,280
I think it's the peaceful sage that says the Buddha is the peaceful sage, but I wanted

249
00:25:04,280 --> 00:25:08,880
to highlight the word santa, santa to a jikataimuni, so the Buddha was a moony, he

250
00:25:08,880 --> 00:25:15,280
was wise, but he was also peaceful, and I wanted to highlight that word because well again

251
00:25:15,280 --> 00:25:21,760
that's what you should gain from the practice, ultimately it is about finding peace, you

252
00:25:21,760 --> 00:25:26,280
stay in the present moment, you learn about reality, you become more comfortable and more

253
00:25:26,280 --> 00:25:32,440
familiar with the way your own mind works, so much changes and what changes is the stress

254
00:25:32,440 --> 00:25:40,760
and the chaos in the mind, the suffering in the mind has reduced and there's a great

255
00:25:40,760 --> 00:25:45,960
peace that comes from that.

256
00:25:45,960 --> 00:25:52,200
So I've gotten into the habit of at wishing people to have a good day and now you know

257
00:25:52,200 --> 00:25:56,440
it wasn't just because of being polite, I think it's a good politeness, wishing people

258
00:25:56,440 --> 00:26:03,120
to have a good day is kind of a secret way of wishing for them to have a real good day

259
00:26:03,120 --> 00:26:09,540
and an exalted day is an excellent day, an excellent night where they are able to find

260
00:26:09,540 --> 00:26:17,120
true peace and free themselves from stress and suffering, but there you go, that's the

261
00:26:17,120 --> 00:26:35,160
demo for tonight, thank you all for listening, have a good night, don't we have to turn

262
00:26:35,160 --> 00:26:52,080
this off this moment.

