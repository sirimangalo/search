Good evening everyone, welcome to our live broadcast.
Tonight we're looking at a good tour in the Cayenne Book of 3, so 21, no, no 21, so
18, 19, and 20, a barber nica, barber nica, barber nica, barber nica, barber nica, barber,
someone who keeps the shop, the shopkeeper.
I think this one's interesting.
The similes are always nice.
They give us a frame of reference, something that we're familiar with, how meditators should
behave well. Think of a meditator like a shopkeeper. Shopkeeper has to concern
themselves with profit and loss. They have to concern themselves with their
goods and their customers. Shopkeepers have to work. That's really the key here. So
the first two does about how shopkeepers have to work. Book of three is we have
three ways in which a shopkeeper doesn't succeed, doesn't succeed, doesn't
prosper. Three ways in which shopkeepers incapable of acquiring it wealth, not yet
acquired or increasing wealth already acquired. It's quite a simple formula.
There's nothing complicated here. They don't work in the morning. They don't
work in the afternoon, in the middle of the day, and they don't work in the evening.
They don't work at all. They don't apply themselves. Same with a biku, a meditator,
a meditator who possessing three faculties is unincapable of achieving a
wholesome state, just like a shopkeeper. Shopkeepers have to work. They have to
maintain their shop. They have to be vigilant. If you're not careful, shoplifters
will come in. Let's take away your goods. If you're not careful people who cheat
you, you're not careful. Your goods will be ruined by mice and disease and
moths and your shop will be ruined by rain and so on. If you're not careful with
your taxes, the king will take away or the government will take it away. Take
away your shop. Same with a monk. A monk has to be vigilant. A monk doesn't
practice in the morning or the evening or the day or the evening. Then they'll
lose everything they have. It is all there. All their spiritual gains and all
the benefit of living the holy life. So meditator doesn't apply themselves in
the morning. Then in the morning they lose their, and they lose their
concentration. They lose their focus. They lose their clarity of mind. They
don't during the day if they slack off. They lose what they gained in the
morning if they don't in the evening and they lose what they gained in the day.
It will be sleep at night. Even then we lose something. Meditation is something
that has to become a very much a part of your life. More vigilant you are, the
more benefit, the more you prosper in the spiritual path. We have shop lifters
coming to take away our spiritual gains in the form of
defilements. Anger will take away our spiritual gain and greed, delusion,
worry, restlessness. Five hindrances will take away our spiritual gain. When your
mind is distracted by worldly things, it's like a leaky roof raining down on
your goods and they become ruined if we don't so we don't spend our morning
or day and our nights in mindful practice. You might think, well I'm not a bad
person. I can just live my life in an ordinary way. I won't do anything. I won't
purposefully get angry or greedy or just live my ordinary life. I don't really need
to meditate. This is like leaving your, the Buddha said like leaving your roof,
leaks in your roof. The rain will come in.
This is a good one I think for people living their lives to remember. Remember
two things. First of all that and in fact it's interesting because it seems to
imply that if you practice in the evening, well at least you're good in that
sense. If you don't practice mindfulness or good things in the morning and
during the day but then in the evening you do some meditation or in the evening
you go and listen to a dhamma talk or that kind of thing. Then you get gained
from that but what's really bad is if you never do anything. Spend your days
never doing anything beneficial. Never doing anything to your gain for your
spiritual gain, spiritual development. The other thing is that to point
out that any time that we're not engaged in mindful exercise of keeping our
minds clear. It's a time when the mind can become caught up in
defilement and distraction and defilement. It caught up in unwholesomeness and lose our spiritual gain.
Or not succeed. We won't prosper spiritually just as a shopkeeper won't
prosper if they don't put on effort. So we should think of spirituality as kind
of like work. It's a job you have to do. It's a duty. It's a chore and it's not
something that should be seen as simple or easy or even comfortable. It should
challenge you. It's something you have to maintain. You have to work at happiness.
It's something that you have to work for. Happiness doesn't just fall in your
lap. It's not just something you can rely upon to always be there.
Happiness requires perfection. It requires goodness. It requires strength, power,
purity, all things that you have to work for. That's the first one quite a
simple suit. The second one's a little more interesting. So on the theme of a
shopkeeper. A shopkeeper needs three factors, has to possess three factors, three
things in order to attain vast and abundant wealth. If someone has these three
things of a shopkeeper, it maintains these three things. They will soon attain
vast and abundant wealth. What's three? A shopkeeper here, a shopkeeper has key
knives. It's responsible that has benefactors. How does a shopkeeper have key
knives? Key knives is responsible as benefactors. These are three things.
Key knives means the shopkeeper has to know about a knife. It has to understand
their goods. They have to understand the relationship between goods and money.
If I sell the goods at this price, I'll get this much gain. They have to be
good with money. They have to know how much capital is required. They
sell at this price, then they need this much capital. They're going to make this
much profit and I need this much profit to achieve my goals and
so on. Economics. The laws, they have to know that the laws of economics, the
economic formulas. Sounds like something a meditator has to concern themselves
about or what does this have to do with meditation? Well, how is a shopkeeper
responsible? Moving along. A shopkeeper is being responsible. Means they are
skilled in buying and selling.
They are skilled and also engaged in it. Meaning they don't just slack off and
let the goods sell themselves. They engage with customers and they work at
promoting and they do all the activities that are successful and skilled
shopkeeper undertake. They work to sell their goods and the shopkeeper finally
has should have benefactors. Should have people investors, you might say, I suppose.
Yes, investors. They should have investors who give capital because how
else are you going to buy? Unless you're going to buy the goods. So you're going
to set up your shop and keep your shop going. So you don't work with only the
money that you bring in. You work also on loans and capital. People who
support you and give you wealth because from your own modest upbringing or
beginnings, you can never become rich. You can never become a successful shopkeeper.
So three things that anyone looking to set up shop should think about. One, you
should be smart about economics. Two, you should be hardworking and three, you
should have investors possessing these three factors. A shopkeeper
soon attains vast and abundant. Well, what the heck does that have to do with
us? It's quite clever actually because there are three neat parallels with
meditators. A meditator should have keen eyes be responsible and have benefactors
or investors. So how does a meditator have keen eyes? Well, they know the
formulas. I'm not economic formulas, but economic and descendants in the
spiritual sense. They understand that if I do this, this comes from it. If I do
that, that comes from it. The four noble truths, they understand this is
suffering and this is the cause of suffering. These things that I cling to, these
are suffering. Why are they suffering? Because I'm clinging to them. Because
they can't and because they can't support that clinging. They can't support
my addiction. My attachment to these things is a cause for suffering. Why?
Because they're impermanent unsatisfying and uncontrollable. Seeing this
about your experience. Seeing that the body is changing all the time and you
can't rely upon it in the expectations you have in the body. I'm going to be
disappointed. The feelings, memories, thoughts, consciousness are all
undependable. You can't depend upon them. Understanding the laws of cause and effect. This
comes from meditation. This is something you can learn about, but it's much more
something that you have to get through experience. Just like a shopkeeper. Your
work is a shopkeeper's apprentice. So you work as a meditator practicing to be
a practitioner to be a meditator. When you gain this sort of experience through
the practice. So how is a meditator responsible? Like a shopkeeper keeps an eye on
their goods and works to sell them. A meditator works to cultivate him
insight. It's hard to work meditating. Try walking and sitting for hours and
days. Something you have to be patient with in the
thautical. Something you have to strive towards. You have to believe in it. It has to
be seen as worse doing. If a shopkeeper isn't, it isn't intent upon riches and
isn't intent upon greatness. They'll never prosper. They'll never succeed.
If a meditator is not intent upon spiritual development, they also will not succeed.
And the third one, so how does a meditator need investors for? You might think this
refers to meditators needing food or something, but no clever. The investors
and them for meditator are teachers. People who will answer their questions.
People who will explain the teachings to them. These are people who invest their
teachings in the meditator. They think the meditator is worth teaching. So
someone who is worth passing along the treasure, worth investing their time
and energy. Because they fully expect to get a return that this person will
become better, will become in and of themselves and great. And wonderful
person with profound insight. Maybe they don't expect such a return. They do
expect some return in that sense, because they wouldn't teach people who
weren't able to benefit from it, but they don't expect anything for themselves.
They expect a return in terms of progress for the meditator. The meditator will
gain spiritually, will benefit from it. They look at the meditator and they think
all this person is incompetent and they wouldn't invest their time and energy teaching them.
So there's three for a monk. We should think of ourselves as a shopkeeper. Our shop
is the meditation practice. We set up shop and we undertake to gain from our
goods. Walking and sitting is like a great work that you do that gives great
benefit and great, great income, prosperity, spiritual prosperity. But you have
to a keen eye and you need wisdom that has to come from real practice. You can't
just do walking and sitting like a buffalo. You have to do to be clever and be
attentive. And then you have to work like a shopkeeper works long hours. And then
you have to rely upon your teachers. Don't expect to gain and that you don't
think that you know everything and that you don't ever need any help. Don't be
too proud to accept the advice of others. Submit yourself to a teacher. It's very
important. So that's our demo for this evening about a shopkeeper. Before
questions we have I think a couple of announcements or one announcement anyway.
We're going to have a meeting Sunday, right, Robin?
You know anyone that's interested to show a better meeting. We need to go hang
out on Sunday and one o'clock and we're hoping to meet this Sunday. I realized
that's kind of close but for anyone who was available, I'm going to link it to the
meditation shop box so people can just jump into the link and jump into the hangout.
You put the link on Sunday, Sunday at one. So come back on Sunday and you'll see the link.
It's nice. It's a nice group. It's not a huge commitment. You just want to show up and just see what kind of
projects need working on. You're very welcome to you. You're not going to walk away
feeling overwhelmed or whatever. You know the first is interested in whatever time or
talents they're willing to share. We're happy to have those time and have those.
Okay. Questions? Yes. I don't understand how Kamajanda is
spurned by Iqakata. Is it because one pointedness develops only the five jhana factors?
Kamajanda. I know that each jhana factor is supposed to attack one of the
hindrances but really they all work together. When your mind is fixed and focused on a single
object, your mind is perfectly pure during that time. So there's no room for Kamajanda.
There's no room for the hindrances. It's quite simple. The hindrances come from diversifying
from reacting to the experience. If you keep your experience focused on a single object,
then there's no room for the reactions to come. I mean, that's the idea of the jhana's.
How do we explain ego in Buddhism? There's two parts to ego. There's belief in self and
there's three parts, I guess. There's the belief that there is a self. There is conceit
about one's self and then there is attachment to possessions. So those three are the three
aspects of what one might call ego. They're based on three defilements, diti, which is views,
mana, which is conceit and dunhana, which is craving. So those three working together are
one way of understanding the problem and the defilements. If a meditator experiences a
sudden absence of the self of being in a separate self or a doer and rather perceives consciousness
as being made of a unified substance. So as to speak, in other words, perceiver and perceived
as seen as one and the same, resulting in a sense of freedom, joy and complete effortlessness,
is this something different than having insight in on a done or not some? Yes, I think something,
sounds like something quite different. They won't get too scolded for this question.
If you have that feeling, it's a feeling, and it should be noted as such. And this is the
problem with spiritual practitioners that they interpret things. So you have a feeling in that
a perception, and those should be seen as feelings and perceptions. The feelings arise and
cease. The perceptions arise and cease. Just because you perceive something to be
X doesn't mean it's X. This is what happens with other religions. They perceive things to be
God. They perceive things to be meaningful. They find meaning and things that are not meaningful.
This is why the Buddha said don't pay attention to the particulars because that's infinite.
What it might be is infinite. But what it actually is, is a feeling or a perception.
If you look at that perception, you'll start to see, oh, actually consciousness is something
that arises and cease. The perception arises and ceases.
I don't scold people for questions, do I?
All the time, Bombay. I'll be more careful.
I'll be careful.
For every time you said, well, that's not a very good question.
No.
The idea of attention seems to apply a decision made by a decider,
exercising very well, and that if there is a decider and that could be seen as a sound.
But my subjective experience is that I don't have very well.
I become aware that I'm about to do something, but there is no decision for
answers or any evidence of an entity made that decision. So when the teaching will say to
no intending, I get a little bit confused as to one mental process this is referred to.
Can you clarify that feeling that you're about to do something that's called intending?
I mean, I wouldn't be to come to technical or philosophical about it and call it what you like,
but it's kind of like an intention. There's an intention, a stand, a intention to walk.
And if that feeling comes up, then you just say intending and tending.
The whole idea of self is just too much trouble. It's more trouble than it's worth.
Wouldn't worry about is there a self isn't there a self?
And what it didn't, and he didn't frame it that way, because it wasn't useful to do so.
It's useful to look at things as non-self, so that intention that you have is not self. It arises
and ceases. It just sort of sounds like that's what you're seeing.
Maybe it confuses, then you should say confused, confused. That's okay too.
But no, that's maybe being a little bit facetious, because it's a good question.
I say questions are good as well.
That's true, that is true.
Welcome back to Darr, a verminative recess, been away from the long time. She's back with a long retreat.
She was also active on second night, one of the teachers there.
In the past, you have talked about the importance of avoiding certain kinds of forcing.
Last night you talked about the importance of certain kinds of forcing.
Can you elaborate on the wrong kind of forcing or pushing, because I still don't understand.
What sort of forcing did I say is good?
Just doing a slip, because I don't normally say that forcing is good.
The other thing I can think of is pushing yourself to practice. I mean, you kind of sometimes have
to push yourselves. In a conventional sense, forcing can be good, but in an ultimate sense, when
you actually get down to like you're doing the meditation, and you've kind of maybe, this is all I can
think of, it's not, I wouldn't usually use the word forcing, but if you forced yourself to meditate,
you say, okay, I don't want to meditate, but I'm going to do it anyway. But then when you
med actually meditate, you can't force yourself. You're just trying to observe.
It's very important that you don't engage in forcing anything. And if you see that you're forcing
something that you know that as well, rather than cling to it as, as I'm forcing me, forcing.
And this is the same person. Also, I still fail to understand what it means to take breaks during
intensive practice, because when I take those mindful breaks from formal meditation during an intensive
practice, it's always at least as difficult as it is to just continue formal meditation.
Do you need the sometimes one should relax a little during the break?
Well, informal meditation is going to be less intensive. It's not, it shouldn't be as difficult,
although I get what you're saying. It's actually sometimes just as easy to continue meditating.
Well, in that case, then continue doing it, but you know, being able to stretch your limbs and go
outside and be a little bit less intensive about your meditation. If you, you know, you should still
be mindful walking around, but it's a less repetitive. There's a conventional sense. It's a
relaxing thing for the meditator. But in an ultimate, in the end, you're right, the break will be
just as much trouble. It's also good for your body to take a break, to not have to sit still
for so long. So if you get up and I don't know the stretch or something. But I always find that
an intensive practice is like, well, what does a break mean really? It gives you a chance to
step back. I guess probably the best thing that it does is that it gives you a chance to step
back and look at your practice. And when am I doing wrong? Why am I so stressed out?
If you're, you know, if it's difficult, taking a break allows you to step back and readjust
to make it more efficient.
You know, you don't have to say anything during that, just rising and falling is fine.
After some time, we'll give the meditator to do rising and falling and then switch,
because there may be a break, but not during the break. You just switch and focus on the fact
that you're sitting and you say to yourself, sitting. But you don't have to worry about it
between the breaths, but there's room there, so you just say sitting. And then wait for the next
rising, rising, falling, sitting. But the sitting might take some time. Anyway, that's something
we do for, and during the meditation course, we'll give more complicated exercises as the
meditators and mindfulness gets stronger.
On day based on your post today about choosing gender, would you please talk more about your
view on this? Can those that choose gender choose only for themselves? Or are they special in that
way that they have some keen vision thinking choose anyone and everyone's gender? Either so far
I don't understand how it would work. I don't know if sexuality is sometimes in the story
seeing this in gender change, but it's not a choice.
Honestly, I'm not for choosing your gender, but I can understand it because of, you know,
based on lives and past lives and that kind of thing. And it seems to me that it's probably
better to be true to your inner sexuality than to repress it or pretend, you know, if you,
I guess it's not even sexuality, but if you feel masculine, but you're a woman's body,
or if you feel feminine in your own man's body, that doesn't seem to be a problem to sort of
straighten things out. I guess the big problem is with hormone therapy and who knows what,
you know, you have to go through it. It's not, I'm not really for it and I don't think as a Buddhist,
it's proper to have a gender change or that kind of gender reassignment or whatever they call it.
It doesn't seem to me to be the proper way because of course Buddhism is about letting go of
our attachment. It would be much more efficient and profitable to just, you know, learn to see these
thoughts as just thoughts. Who cares what body you're in? I mean, what if I was in a body of a horse
or a big deal, you know, it's just a body. Of course the horse can't meditate, but you know,
masculine, male, female body, it doesn't really matter which body you're in, it's just a body.
To be honest, it was more about, I've been approached recently by Christians now that I'm getting
out and about seemed to be crawling under the woodwork. I met these two guys walking along and
one of them turns to me and says, hey, and they're so friendly. And I thought, oh, maybe these
people interested in meditation and we talked and then this woman came up and now there's three of
them. A young woman, a young man and young women. And then, you know, they asked me about religion
and what religions I'm interested in. And then one of them, then I start to get suspicious because
he says, you ever think about studying biblical religions. And then one of them tells me that they're
weird Christians and suddenly it's all about Jesus and then I'm like, okay, I gotta go.
And then today the guy looked at me walking on the street and walked past and then he walked
past again and he looked at me again and he said, are you a monk? And I said, yes. And he said,
I think he said cool or something. And as you walked away, he said, Jesus loves you.
So then when I saw the Pope was mousing off about these things that he shouldn't, I thought,
it just seemed like he was on such a role talking about poverty and things that were actually
going to be important. It just seems to me, why would you worry about other people's gender?
You know, why would you worry about other people having gender changes? Why would this be something
to be concerned with when there's such bigger things to worry about? I think. I mean, I guess I
shouldn't minimize it because it could be potentially problematic to have a gender change. I'm not
really for it. But I'm not going to judge people or condemn people for it. I just don't like
people to be obsessed with such things personally. So it was kind of, I don't know about that post.
But all the things you're asking are actual questions. Choosing gender means having a gender
reassignment where you actually change your body in some way. I'm not really sure how it works. But
it also takes hormone therapy and that kind of thing. I mean, there are teachings where
Buddha has to be male. So there may be some real consequences with messing with your gender. But
I would say it's probably not as dire as people might think.
It's just, I think there are much worse things to concern ourselves about. The clinging to
such beliefs. I mean, why do they care? Because it's not because they're concerned about the
person's well-being. It's because God told them it's wrong or their books tell them that God
told me it's wrong. It's really a silly reason to believe anything.
Finally, are you a boy, girl, man or woman, Monday? Thank you. I don't know how to answer that question.
I'm a monk. I mean, there's no eye. I mean, is this a trick question? I'm not supposed to fall
for it. I don't really understand. That's not a good question.
And the other thing you say, I don't answer questions about myself. Oh, yes. I don't answer
questions about myself. Next question.
Is it better to the practice to have a plant routine like the hour of going to sleep,
like an hour of going to sleep, waking up, meditating, etc?
Potentially, but there's also the potential for getting caught up in control. You see,
expectations. If you have a schedule, it's good in a sense to keep. I guess it's just that you'd
want to remind yourself or keep in mind that life is uncertain and things change and your schedule
will have to adapt. If you get so stuck on a schedule and then when something comes up to break
your schedule, you freak out. Or you say, I can't meditate because I missed the hour to meditate.
It's not how it works. So I would say it's probably more of a novice thing to do
and an advanced ninja meditator will be adaptive and will be able to fit meditation in wherever
it's convenient and not convenient, but wherever it's wherever there's room,
it's much more, you know, it eventually becomes much more natural and much more.
So I say, yes, actually the schedules are great as long as you don't cling to them
and you adapt them, but you're never, you know, it's good if you break them back to them when you
can. But it keeps you honest, it keeps you focused.
I have a question regarding some sama sama sama and then too. So the Buddha said the view
and I have no self was wrong to my limited knowledge. This doesn't seem right. Can you please
explain it? Thank you. Well, there's no eye to have no self. If you say I have no self,
you're a paradox kind of, right? That's all the Buddha's saying.
It's a good point. You know, this is why you get caught up, you get mixed up if you say there is
no self. You're kind of saying the same thing because you're already assuming that a self
could exist, meaning you're assuming the existence of entities and as soon as you assume the
existence of entities that exist, the existence of entities of which the self is not one,
you've already got self because an entity is a self. So the point is that the universe is
not made up of entities. That's really, I would say, the best way to understand self and not
self. So if you say, I've already got an entity.
It's not perceptions the wrong word. It's actually a mistranslation. The word perception
means actually consciousness. I didn't realize it, but if you look it up in the dictionary,
it really just means to be aware of something. But because we use it as your perception,
it was used for sunya. But sunya means, sun means like, nice to know, to know something like
something else. So it's recognition. There was a bikuni from Taiwan who argued with me about this and
she said, she said something else and I realized she's right that sunya can actually mean many
things. It's used in different ways. But in terms of the five aggregates, I'm quite clear that
it means memory and recognition. When you recognize something, which we call memory, that sunya.
So if you see something and you recognize it as red or yellow, or you recognize it as a cup,
or you recognize it as a person and you remember their name, that's sunya.
How is sunya different than sunya?
Sunya is the approximate cause of sunya. That's a very good point and it was a good question.
When you remember something for what it is and you re-remember it, re-remember it,
remind yourself about it. That's sunya. So they are related and in fact etymologically they're
related as well. No, they're not. They're not etymologically related. But sunya and sunya are
related and read the commentaries, which is a great way to explain why we say to ourselves
seeing sunya. Because that etyra, sunya, etyra means strong and firm. Sunya is recognition. When
you recognize something and you fix yourself on that recognition, that's etyra, that's sunya.
Of course it also means not recognizing it as red or blue, but recognizing it as seeing,
because red isn't real, red is sunya. Red is well, it's a kind of a sunya. Again you can see red
red, but because the red is not real, it's sunya, but a concept. It's a setty, it's still setty,
but it's not repass and I won't lean to insight. It's based on the object, that's all.
But yeah, sunya and sunya are cause and effect with each other. Sunya is the color.
I think it's really strength in it.
I am finding noting, wanting quite difficult, as it seems ambiguous and early clear,
in one of your videos you mentioned that wanting is a kind of stress. Would you elaborate
or give general advice on proper noting of it? Thank you, Bombay.
Well, wanting is momentary, you know, it's just, it's fleeting. So it wouldn't, unless you
realize that you're wanting something, wouldn't say wanting, but
you're thinking about it too much, because you can ask, you know, do you ever want anything?
Do you know that you're wanting something? For sure, there are times where you want something.
If you think of a cheeseburger and you want it, or you think of a song and you want to listen to it,
or you just think of a person and you want to see them, something pleasant and you want to
touch it, or smell it, or taste it, or listen to it. That's all wanting. The one thing is there.
I wouldn't overthink it if there's wanting, just say to yourself wanting. The point is not to
really focus and understand too much about the thing. The point is to not react to it, so that you
just see, okay, there it was wanting and now it's gone. And over time, you'll start to see things
in that way, okay, it came and it's gone. All we need to learn is that things come and go,
basically, because then you'll see that they're, you'll realize that they're unsatisfying and
uncontrollable. You wouldn't overthink it too much. If you're aware that, oh, here I am wanting
this, just say wanting. You don't have to be too, too. You don't have to catch it. You see, and you
can't catch it. In fact, it may very well be that your confusion is because you're in one of the
nyanas where you see things ceasing before you can really grasp them and get a sense of what they
are, which is important. It's important to see that things are always ceasing and you can't hold on
to them. They're not going to wait around for you to study them. And that's important because the
study that you need to do is that they're, they don't stick around.
Because what is in the crew, the notion of investigation of what's on mental, any emotional
processes for what, why, how?
When I really understand, I mean, our practice doesn't, if you read my book on how to meditate,
you'll get a sense of how we practice.
A question regarding all the hangouts. Do I need to add anyone before using the link in
connecting to the hangout on Sunday, or is the link rolling into connect?
I think the link is all you need to connect. It seems to work. I mean, some people have been
complaining it isn't working. So that's a shame. But my understanding is that it works.
I think you do have to do something to maybe maybe just make sure that you have hangouts installed
and set them properly. So probably just make sure that it's somewhere on your, you know, on your PC or
device, maybe go through the settings. Um, it's like, I'd seek to remember that I had to install
something at one point. But you don't need to add anyone. Um, I just feel that just click on the link.
Okay, you finished? All right. Thank you everyone. Good night. Thanks, Robin.
Yeah, joining us. Thank you. Thank you. Bye. Thank you.
