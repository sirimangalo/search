1
00:00:00,000 --> 00:00:08,480
We are going to come back to our study of the Dhamupada.

2
00:00:08,480 --> 00:00:22,280
Today we continue on with verses number 31 and 32, which goes follows.

3
00:00:22,280 --> 00:00:49,360
So let's begin with the Dhamupada, the Dhamupada and the Dhamupada and the Dhamupada and the Dhamapada and the Dhamapada.

4
00:00:49,360 --> 00:01:00,760
Nimban, asi, what? Santiki. So we'll go through this one by one. The first verse, they're

5
00:01:00,760 --> 00:01:10,920
very similar. The first verse is a pamanda, rato, bikhu, a monk who or a bikhu, who delights

6
00:01:10,920 --> 00:01:22,000
in heedfulness, pamade, bhyada, siva, and sees the fierceness or the danger in heedlessness.

7
00:01:22,000 --> 00:01:34,120
Sanyodhana, nuntulang, the fetters or the bonds, those things that bind the fetters.

8
00:01:34,120 --> 00:01:48,920
That fetters both large and small, dahang, agi, wagachati. He goes through them like a burning fire.

9
00:01:48,920 --> 00:02:00,120
So this verse was told in regards to a story about a particular monk. And the story is quite short and doesn't have so much meaning but it helps us

10
00:02:00,120 --> 00:02:13,520
to understand or visualize the simile here. Because it was a simile that came to this monk who was practicing diligently.

11
00:02:13,520 --> 00:02:22,920
He was practicing meditation and he was traveling along when suddenly a fire broke out.

12
00:02:22,920 --> 00:02:33,520
And he was walking through the forest and suddenly there was a fire and the whole forest was being consumed by fire.

13
00:02:33,520 --> 00:02:42,920
And so he quickly went to a place of safety up on a large hill area where there were no trees.

14
00:02:42,920 --> 00:02:52,120
And he watched the fire consuming the trees and he watched it slowly through the forest destroying the entire forest.

15
00:02:52,120 --> 00:02:57,720
And he took this as a kind of a metaphor for his practice and it was something of course very visual.

16
00:02:57,720 --> 00:03:08,320
People who stare at fire that it becomes an image in their mind, you can actually practice it as a meditation because it's something that sticks quite clearly in the mind.

17
00:03:08,320 --> 00:03:16,120
And so as he was meditating, this came back to him, this image of the fires just consuming everything.

18
00:03:16,120 --> 00:03:22,720
And so he actually applied this to his practice and said to himself, just as that fire goes through the forest so sure.

19
00:03:22,720 --> 00:03:25,720
So too should I go through the firemen.

20
00:03:25,720 --> 00:03:31,920
And the Buddha came to him and confirmed that and said yes indeed.

21
00:03:31,920 --> 00:03:45,920
Monk who delights in headfulness and sees the fearsomeness, the danger in heedlessness, burns up the feathers large and small just as a fire burns up the trees, burns up the fire.

22
00:03:45,920 --> 00:03:55,920
The second verse was told based on a completely different story of a different sort and also very fairly short.

23
00:03:55,920 --> 00:04:01,920
But the meaning is very similar, upper-modern it will be koopamandi by a dusty brother that's the same.

24
00:04:01,920 --> 00:04:06,920
Monk who delights in heedfulness and sees the danger in heedlessness.

25
00:04:06,920 --> 00:04:25,920
A bambho, padihana, he is unable to fall away or he is not possible for such a person to fall away or to go to waste or to waste away.

26
00:04:25,920 --> 00:04:34,920
Nímanas, a yuwa, santike, indeed such a person is very close to Nímana or close to freedom.

27
00:04:34,920 --> 00:04:42,920
And the story here is another simple story with sort of an interesting idea to it.

28
00:04:42,920 --> 00:04:49,920
The story is of a monk who was well known for his name was Níga Matisa.

29
00:04:49,920 --> 00:05:02,920
He was well known for his frugality, his funess of wishes and his strict behaviour, not seeking out luxury or special things.

30
00:05:02,920 --> 00:05:23,920
And so even though he was quite near to Sawati where the Buddha was staying and where there were so many faithful lay disciples who were providing the monks with so much luxury and so much affluence or opulence, you may say.

31
00:05:23,920 --> 00:05:35,920
He never came to accept the gifts of these people. And so the monks watched him and they watched this behaviour that he would go for arms round and every day he would go to his family.

32
00:05:35,920 --> 00:05:42,920
The village where he grew up which was fairly close to Sawati or a bit away from Sawati.

33
00:05:42,920 --> 00:05:46,920
And he would go and collect food from them and then go back and live in his forest.

34
00:05:46,920 --> 00:05:58,920
And he never came down whenever there was an invitation or a meal, whenever there was the offering of requisites he would never take part in the offer, never come to receive these things.

35
00:05:58,920 --> 00:06:10,920
So the monks kind of got the feeling that he was clinging to his family and he was just spending all his time amongst his family not getting involved in Buddhist culture, the Buddhist culture that centered around Sawati.

36
00:06:10,920 --> 00:06:21,920
And so they told us to the Buddha and the Buddha called him up and said, is it true that you're acting like this, that you're not getting involved in the society or you're not coming to accept the gifts of these faithful people?

37
00:06:21,920 --> 00:06:33,920
And he said, and that you're instead clinging to your family and getting involved in getting caught up in your family affairs.

38
00:06:33,920 --> 00:06:44,920
And he said, well, it's true that I'm not coming for these invitations, but it's not true at all that I'm caught up in my family affairs or caught on attached to my family.

39
00:06:44,920 --> 00:06:48,920
I think to myself, these people give me enough food to survive.

40
00:06:48,920 --> 00:06:52,920
The food that I get from the village is enough to survive and that's it.

41
00:06:52,920 --> 00:06:57,920
The duty for the food is finished. Why should I go to seek out more?

42
00:06:57,920 --> 00:07:03,920
I get enough food to survive and then I go back and practice meditation on my own.

43
00:07:03,920 --> 00:07:08,920
And the Buddha actually praised him for this.

44
00:07:08,920 --> 00:07:15,920
And as a result said, this is how a student of mine should act and he told the story of the pastor.

45
00:07:15,920 --> 00:07:29,920
Where the Buddha himself in one of his past lives was apparent and was just a story of contentment, where he was content in the same way and not needing to go anywhere.

46
00:07:29,920 --> 00:07:38,920
I'm just taking what came to him, not seeking out further records, or further material gain.

47
00:07:38,920 --> 00:07:46,920
And so then he said, this verse, such a monk is not declining. It's not a backsliding person.

48
00:07:46,920 --> 00:07:50,920
It's not a person who is giving up the training because they do this.

49
00:07:50,920 --> 00:07:57,920
A person who has no interest in Buddhist culture or taking part in the society.

50
00:07:57,920 --> 00:08:02,920
That's a person actually who's quite close to Nibana for their reality.

51
00:08:02,920 --> 00:08:07,920
This is a common theme actually that we can talk about.

52
00:08:07,920 --> 00:08:10,920
But the verses themselves are quite similar.

53
00:08:10,920 --> 00:08:17,920
And as with many of the stories, they relate much more to the general practice than they do to the actual.

54
00:08:17,920 --> 00:08:24,920
The verses relate much more to our actual practice than they do to the stories themselves.

55
00:08:24,920 --> 00:08:39,920
So it's a way of using some very ordinary things to give a teaching or to use in our meditation practice.

56
00:08:39,920 --> 00:09:05,920
The main theme as the theme of this whole chapter has been is heedfulness or vigilance, which we've explained as having mindfulness, always having mindfulness every moment by moment, being mindful, not getting angry and having a level head, having a balanced mind, having your mind clearly aware of things,

57
00:09:05,920 --> 00:09:14,920
neither partial towards or against something, having your mind stable and clearly perceiving the objects of awareness.

58
00:09:14,920 --> 00:09:18,920
And slowly giving up your greed.

59
00:09:18,920 --> 00:09:22,920
So basically being mindful and giving up partiality.

60
00:09:22,920 --> 00:09:28,920
Mindfulness is considered to be the essence of this distinction on heedfulness.

61
00:09:28,920 --> 00:09:42,920
So the point of our practice should be too full to develop this state of mind that is mindful, that is vigilant, and to avoid the state, which is negligent.

62
00:09:42,920 --> 00:09:46,920
This is a common theme in the Buddha's teaching that you'll see this too full teaching.

63
00:09:46,920 --> 00:09:48,920
And the Buddha even explained it in detail.

64
00:09:48,920 --> 00:09:51,920
He said, this is how you would teach a horse.

65
00:09:51,920 --> 00:10:02,920
If you have a horse that is untrained and you want to train it, then first of all, you train it with a kind lesson.

66
00:10:02,920 --> 00:10:08,920
You're kind to the horse, you encourage it on, you give it carrots, you use a carrot to teach.

67
00:10:08,920 --> 00:10:13,920
Giving it rewards when it does good things and teaching it in a kind way.

68
00:10:13,920 --> 00:10:19,920
If it doesn't react to that, then you teach it in the harsh way, you use the mystic.

69
00:10:19,920 --> 00:10:24,920
And if it doesn't react to either of those, then you kill the horse.

70
00:10:24,920 --> 00:10:28,920
And so the Buddha said, this is how I teach my disciples.

71
00:10:28,920 --> 00:10:33,920
I teach them kindly at first, and then I teach them harshly if that doesn't work.

72
00:10:33,920 --> 00:10:36,920
And if even that doesn't work, then I kill them.

73
00:10:36,920 --> 00:10:38,920
And so he explained what this means.

74
00:10:38,920 --> 00:10:41,920
First of all, the first way to teach is to teach good things.

75
00:10:41,920 --> 00:10:43,920
What are the good things?

76
00:10:43,920 --> 00:10:44,920
What are wholesome things?

77
00:10:44,920 --> 00:10:46,920
What are wholesome types of behavior?

78
00:10:46,920 --> 00:10:51,920
When the hopes that they'll catch on by themselves, that without having to reprimand them or scold them,

79
00:10:51,920 --> 00:10:56,920
that they will be inspired and take up this teaching, take up the practice for themselves.

80
00:10:56,920 --> 00:10:59,920
If that doesn't work, then you teach them what are the bad things.

81
00:10:59,920 --> 00:11:02,920
And you say, this is bad, this is wrong, this is wrong.

82
00:11:02,920 --> 00:11:06,920
As a means of scolding them, as a means of stopping them from backside,

83
00:11:06,920 --> 00:11:11,920
stopping them from falling into unwholesomeness.

84
00:11:11,920 --> 00:11:15,920
And if that doesn't work, what he means by killing them is you stop teaching.

85
00:11:15,920 --> 00:11:18,920
You give up teaching, and you think that this person is useless,

86
00:11:18,920 --> 00:11:25,920
and you take them out of your realm of instruction.

87
00:11:25,920 --> 00:11:30,920
And this is what it means in the Buddha's teaching to kill someone.

88
00:11:30,920 --> 00:11:33,920
And so here we have a teaching of this sort,

89
00:11:33,920 --> 00:11:36,920
where the Buddha, on the one hand, is explaining the good thing.

90
00:11:36,920 --> 00:11:41,920
Being up Amanda is something that we should delight in.

91
00:11:41,920 --> 00:11:44,920
We should delight in heedfulness and really see the benefit of it.

92
00:11:44,920 --> 00:11:48,920
See that every moment that you're mindful, your mind is clear.

93
00:11:48,920 --> 00:11:52,920
Pure water, like the pure nature around us,

94
00:11:52,920 --> 00:11:56,920
free from pollution, free from defoundment.

95
00:11:56,920 --> 00:11:59,920
When you go off in the forest and you see a pure waterfall,

96
00:11:59,920 --> 00:12:02,920
we should delight in our mindfulness in the same way.

97
00:12:02,920 --> 00:12:06,920
It's like this forest pool or this forest waterfall,

98
00:12:06,920 --> 00:12:09,920
or this clear water that is unpolluted,

99
00:12:09,920 --> 00:12:14,920
and it's done solid, untainted by any sort of artificial,

100
00:12:14,920 --> 00:12:19,920
any sort of foreign element.

101
00:12:19,920 --> 00:12:25,920
When we practice mindfulness, we feel somehow like this still forest pool,

102
00:12:25,920 --> 00:12:27,920
like the waterfall.

103
00:12:27,920 --> 00:12:30,920
Feel quite natural, and you can feel the same sort of delight

104
00:12:30,920 --> 00:12:34,920
as an ordinary person would have for such a sight.

105
00:12:34,920 --> 00:12:36,920
When they go off into the forest and see the waterfall,

106
00:12:36,920 --> 00:12:40,920
mostly people, that's the closest they can get to this clarity.

107
00:12:40,920 --> 00:12:46,920
It's by seeing something so pure, so natural.

108
00:12:46,920 --> 00:12:49,920
When we develop our mindfulness here in the forest,

109
00:12:49,920 --> 00:12:52,920
you somehow begin to feel very much like the natural forest.

110
00:12:52,920 --> 00:12:55,920
Your mind becomes very natural and very pure.

111
00:12:55,920 --> 00:12:58,920
And so this is something that we can delight in.

112
00:12:58,920 --> 00:13:02,920
Of course, in the beginning in our meditation, it seems the opposite.

113
00:13:02,920 --> 00:13:06,920
It seems that what we delight in is negligence.

114
00:13:06,920 --> 00:13:10,920
What is most delightful is the following after the development,

115
00:13:10,920 --> 00:13:15,920
following after our desires, what is delightful stories and fantasies,

116
00:13:15,920 --> 00:13:21,920
and beautiful sights and sounds and objects at a sense.

117
00:13:21,920 --> 00:13:26,920
Very difficult to notice it is very difficult to find delight in heatfulness.

118
00:13:26,920 --> 00:13:29,920
And so this is something that has to be impressed upon us.

119
00:13:29,920 --> 00:13:34,920
Here we have two verses that explain the need for it.

120
00:13:34,920 --> 00:13:37,920
An ordinary person is going in the opposite direction.

121
00:13:37,920 --> 00:13:42,920
This is why they are unable to have a balanced mind,

122
00:13:42,920 --> 00:13:47,920
why they're unable to keep their minds above the visitors of life.

123
00:13:47,920 --> 00:13:49,920
When good things come, chase after them,

124
00:13:49,920 --> 00:13:51,920
when bad things come, they run away from them.

125
00:13:51,920 --> 00:13:58,920
They're always in a state of greater and greater stress and dissatisfaction.

126
00:13:58,920 --> 00:14:02,920
So the Buddha's teaching is something that is quite different from the way of the world,

127
00:14:02,920 --> 00:14:04,920
the development and not only the development,

128
00:14:04,920 --> 00:14:09,920
but finding delight in the opposite way that ordinary people would find delight.

129
00:14:09,920 --> 00:14:14,920
Coming to incline our minds just as a tree would incline in one direction or the other,

130
00:14:14,920 --> 00:14:18,920
where our minds are ordinarily inclining towards an Anglican.

131
00:14:18,920 --> 00:14:22,920
We teach ourselves to incline towards heatfulness,

132
00:14:22,920 --> 00:14:23,920
something that we should delight in.

133
00:14:23,920 --> 00:14:25,920
So this is the teaching on good things.

134
00:14:25,920 --> 00:14:31,920
The teaching on bad things is seeing the danger in that which we ordinarily delight.

135
00:14:31,920 --> 00:14:34,920
Ordinarily we can't see the danger.

136
00:14:34,920 --> 00:14:40,920
We cling to these things and we only see the gratification of the benefit of it.

137
00:14:40,920 --> 00:14:45,920
We get some kind of a pleasure in the present moment and we don't see the bait.

138
00:14:45,920 --> 00:14:48,920
We don't see the hook behind the bait.

139
00:14:48,920 --> 00:14:52,920
We're unable to see the development and the cultivation of addiction.

140
00:14:52,920 --> 00:15:03,920
We're unable to see the imbalance in the mind and the stress and the power that is essential or inherent in clinging.

141
00:15:03,920 --> 00:15:06,920
When you cling to something you're creating a power in your mind,

142
00:15:06,920 --> 00:15:09,920
a power of addiction, a powerful habit.

143
00:15:09,920 --> 00:15:13,920
We're creating a habit of mind that creates requirement,

144
00:15:13,920 --> 00:15:18,920
that creates the need for such a thing in order to be happy.

145
00:15:18,920 --> 00:15:21,920
It's like bringing you out of balance.

146
00:15:21,920 --> 00:15:27,920
Seeing the danger that comes from partiality because being partial towards something

147
00:15:27,920 --> 00:15:30,920
takes you out of balance in favor of it.

148
00:15:30,920 --> 00:15:33,920
It makes you more and more averse to the opposite.

149
00:15:33,920 --> 00:15:43,920
More and more averse to that which is not in your estimation of what is beneficial or what is good.

150
00:15:43,920 --> 00:15:47,920
We'll just pleasant.

151
00:15:47,920 --> 00:15:54,920
We can talk about in many dangers of these things and this is the other part of the Buddha's teaching is talking about the negative side.

152
00:15:54,920 --> 00:15:57,920
The fact that it needs us to fight with one another.

153
00:15:57,920 --> 00:15:58,920
Why we go to war?

154
00:15:58,920 --> 00:16:03,920
Why there is such a thing as economy or why there is such a thing as laws.

155
00:16:03,920 --> 00:16:08,920
Why all of these things exist is because of our mainly because of our property.

156
00:16:08,920 --> 00:16:11,920
Our desire for material gain.

157
00:16:11,920 --> 00:16:16,920
They say there's enough resources to feed and clothe all the people in the world.

158
00:16:16,920 --> 00:16:22,920
But because of our negligence, because of our inability to be content with what we have,

159
00:16:22,920 --> 00:16:28,920
we're taking from others and withholding from others, stealing from others, cheating others.

160
00:16:28,920 --> 00:16:32,920
All the time creating great stress and suffering.

161
00:16:32,920 --> 00:16:38,920
And any time that we can't get what we want, any time that we're denied the objects of our pleasure.

162
00:16:38,920 --> 00:16:43,920
It gives rise to anger and we can destroy friendships.

163
00:16:43,920 --> 00:16:47,920
It does destroy friendships. It destroys families.

164
00:16:47,920 --> 00:16:52,920
The whole issue now of divorce, how people become married and then can't stay married.

165
00:16:52,920 --> 00:16:59,920
And now the common thing is for people to be married a short time and then decide that they have other interests.

166
00:16:59,920 --> 00:17:03,920
And they'll pursue a second marriage or a third marriage and so on.

167
00:17:03,920 --> 00:17:06,920
Never being satisfied. Never being content.

168
00:17:06,920 --> 00:17:09,920
And as a result, experiencing great suffering.

169
00:17:09,920 --> 00:17:14,920
And because they can't see this, the danger in it.

170
00:17:14,920 --> 00:17:18,920
They're unable to remember, unable to keep in their mind.

171
00:17:18,920 --> 00:17:20,920
Or not willing to keep in their mind.

172
00:17:20,920 --> 00:17:25,920
Because they delight so much in the objects of pleasure.

173
00:17:25,920 --> 00:17:28,920
They forget quite easily the suffering that they've been through.

174
00:17:28,920 --> 00:17:36,920
So people will claim that they have a happy life and so on when they've actually gone through an incredible amount of suffering.

175
00:17:36,920 --> 00:17:42,920
Because they're unable to remember. And so as a result, they continuously seeking after more and more pleasure.

176
00:17:42,920 --> 00:17:50,920
Unable to remember or recall or realize the suffering that they've had to embarrass a result of it.

177
00:17:50,920 --> 00:17:57,920
Often they'll think that this is the best that one can expect this pleasure, pain, pleasure, pain.

178
00:17:57,920 --> 00:18:06,920
The dichotomy, like the yoyo effect, or the pendulum effect swinging back and forth.

179
00:18:06,920 --> 00:18:10,920
And creating more and more stress in the mind. It's not half, half either.

180
00:18:10,920 --> 00:18:14,920
The more you cling, the more partial you are to things.

181
00:18:14,920 --> 00:18:16,920
The more stress you have in general.

182
00:18:16,920 --> 00:18:19,920
So even though you might get all of the things that you want.

183
00:18:19,920 --> 00:18:22,920
The stress that's building up in your mind can be quite intense.

184
00:18:22,920 --> 00:18:30,920
It can create headaches. It can create tension in the muscles and so on, tension in your limbs.

185
00:18:30,920 --> 00:18:37,920
It can create sickness in the body because of so much stress, even good stress, even the desire side.

186
00:18:37,920 --> 00:18:41,920
It can create great stress in the body and in the mind.

187
00:18:41,920 --> 00:18:47,920
And it winds you up and winds you up until you aren't able to get the rest.

188
00:18:47,920 --> 00:18:49,920
So that's the two-sided teaching.

189
00:18:49,920 --> 00:18:55,920
That's the first half of both of these verses. And they're where they differ.

190
00:18:55,920 --> 00:19:02,920
The first verses is quite interesting for us to think about and remember in our practice.

191
00:19:02,920 --> 00:19:04,920
About how the practice should progress.

192
00:19:04,920 --> 00:19:09,920
That our practice is not going to be something that we're going to complete in one day or in one course.

193
00:19:09,920 --> 00:19:12,920
Or maybe even in one lifetime.

194
00:19:12,920 --> 00:19:13,920
We can try our best.

195
00:19:13,920 --> 00:19:19,920
And the Buddha said, if you really put your heart into it in seven years you can become an hour on.

196
00:19:19,920 --> 00:19:21,920
Or even in seven days you can become an hour on.

197
00:19:21,920 --> 00:19:24,920
But it may be that that doesn't happen for us.

198
00:19:24,920 --> 00:19:29,920
It may be that because of our karma, because of our duties, because of our future, which is uncertain.

199
00:19:29,920 --> 00:19:32,920
That we're unable to make it.

200
00:19:32,920 --> 00:19:39,920
The way we should look at our practice is this steady kind of like a juggernaut.

201
00:19:39,920 --> 00:19:49,920
That destroys everything in its path. No matter what comes to block it from its path, the juggernaut will continue in its path.

202
00:19:49,920 --> 00:19:52,920
And it doesn't have to go quickly. It doesn't have to rush.

203
00:19:52,920 --> 00:19:55,920
Because it knows that nothing can stand in its way.

204
00:19:55,920 --> 00:19:59,920
It's like the fire in the forest doesn't immediately burn up the trees.

205
00:19:59,920 --> 00:20:01,920
But nothing can stop it.

206
00:20:01,920 --> 00:20:10,920
When it's taken hold of the forest, all there is to do is to wait for it to destroy all the trees.

207
00:20:10,920 --> 00:20:18,920
So the problem is that often people come into the practice with the idea that they're going to suddenly have some profound realization.

208
00:20:18,920 --> 00:20:20,920
And all of their troubles are going to disappear.

209
00:20:20,920 --> 00:20:23,920
That suddenly they're going to become enlightened.

210
00:20:23,920 --> 00:20:29,920
We hear about becoming enlightened, and we think that it's a momentary thing that didn't require

211
00:20:29,920 --> 00:20:34,920
that rather than happening gradually to do work, work, work as hard as you can.

212
00:20:34,920 --> 00:20:38,920
And there's an explosion of enlightenment suddenly.

213
00:20:38,920 --> 00:20:43,920
And so we often mistake our realizations for this experience of enlightenment.

214
00:20:43,920 --> 00:20:48,920
We'll have some epiphany, and we'll become complacent as a result, because we'll think we can make it.

215
00:20:48,920 --> 00:20:51,920
This is why we consider this one of the uppokie lessons.

216
00:20:51,920 --> 00:20:53,920
It's one of the uppokie lessons.

217
00:20:53,920 --> 00:21:00,920
This is Nyan, as I was talking about before, even having good realizations.

218
00:21:00,920 --> 00:21:06,920
So as one of the meditators said today, coming back between the course again,

219
00:21:06,920 --> 00:21:12,920
they're surprised to find the old defilements still there when they thought that they had rooted the mouth already.

220
00:21:12,920 --> 00:21:21,920
And so the thing that we should keep in mind is how slow and inexorable our practice will be.

221
00:21:21,920 --> 00:21:26,920
Our practice will not be quick, and the quicker we want to be, the slower the results will become,

222
00:21:26,920 --> 00:21:29,920
because then there's more craving and more clinging.

223
00:21:29,920 --> 00:21:34,920
We have to work through the defilements in our mind one by one by one piece by piece by piece,

224
00:21:34,920 --> 00:21:36,920
little by little by little.

225
00:21:36,920 --> 00:21:39,920
So the more you practice, the slower you realize that it is,

226
00:21:39,920 --> 00:21:43,920
and the more realistic you become about the results that you should expect.

227
00:21:43,920 --> 00:21:49,920
The more patience you become, we should be patient like the fire or patient like the juggernaut.

228
00:21:49,920 --> 00:21:58,920
That has confident in their progress, but not ambitious, and not overconfident, and not hurrying.

229
00:21:58,920 --> 00:22:02,920
So it says here how there's nothing stays in the way of the fire.

230
00:22:02,920 --> 00:22:08,920
Whether they be big trees or small trees, the fire obliterates them all.

231
00:22:08,920 --> 00:22:15,920
In the same way, our practice will allow us to get rid of everything, all of our attachments.

232
00:22:15,920 --> 00:22:20,920
But it happens as it's going to happen, it happens inexorably like a fire burning.

233
00:22:20,920 --> 00:22:25,920
So we should be very patient in our practice, and methodical as well.

234
00:22:25,920 --> 00:22:30,920
Part of the teaching on heatfulness is being methodical.

235
00:22:30,920 --> 00:22:38,920
We should not hurry ahead or not skip things, not skip any of our defilements,

236
00:22:38,920 --> 00:22:40,920
thinking that's only a minor defilement.

237
00:22:40,920 --> 00:22:43,920
We should see the danger in every little thing.

238
00:22:43,920 --> 00:22:46,920
And this one is an Anumad-Desu-Wentesu, Bayandasa.

239
00:22:46,920 --> 00:22:50,920
Seeing the fearfulness, the danger, and even the smallest offense,

240
00:22:50,920 --> 00:22:56,920
or the smallest wrong in our mind.

241
00:22:56,920 --> 00:23:04,920
And in this way, we'll be like the fire that can inexorably destroy all of the defilements in the mind.

242
00:23:04,920 --> 00:23:06,920
This is the first one.

243
00:23:06,920 --> 00:23:08,920
The second one is sort of this reassurance.

244
00:23:08,920 --> 00:23:12,920
It deals much more with the confidence that we should have as methodical.

245
00:23:12,920 --> 00:23:16,920
That if we're practicing in the right way, even though it may seem at times,

246
00:23:16,920 --> 00:23:18,920
nothing's happening.

247
00:23:18,920 --> 00:23:21,920
Or it may seem at times that we still have so many defilements to deal with.

248
00:23:21,920 --> 00:23:25,920
We should understand, and we should be clear in our minds,

249
00:23:25,920 --> 00:23:29,920
that there is no possibility for us to fall away.

250
00:23:29,920 --> 00:23:32,920
But it's an abubble, barihanaya.

251
00:23:32,920 --> 00:23:35,920
Because we understand the nature of mindfulness.

252
00:23:35,920 --> 00:23:39,920
Because we've experienced this clear state of mind,

253
00:23:39,920 --> 00:23:41,920
we have to remind ourselves,

254
00:23:41,920 --> 00:23:43,920
this is a clear state of mind.

255
00:23:43,920 --> 00:23:45,920
This mind is a pure state of mind.

256
00:23:45,920 --> 00:23:47,920
This is what we know when we use it.

257
00:23:47,920 --> 00:23:49,920
Every time we're mindful, we can see the purity,

258
00:23:49,920 --> 00:23:52,920
we can see the clarity of the mind.

259
00:23:52,920 --> 00:23:55,920
The problem is that then we have all these defilements come up

260
00:23:55,920 --> 00:23:59,920
and we become discouraged, thinking that it's not doing it.

261
00:23:59,920 --> 00:24:02,920
But the truth is it can't help but do something.

262
00:24:02,920 --> 00:24:07,920
Whether we see it or not, whether it becomes evident to us in the practice or not,

263
00:24:07,920 --> 00:24:13,920
we have to think that there's no possibility for this not to affect us in a good way.

264
00:24:13,920 --> 00:24:15,920
Because it's a clear state of mind.

265
00:24:15,920 --> 00:24:19,920
If you get angry, you see that the anger changes you as a person.

266
00:24:19,920 --> 00:24:22,920
You see how we've developed these habits.

267
00:24:22,920 --> 00:24:25,920
If you become greedy, if you like something,

268
00:24:25,920 --> 00:24:27,920
it will make you want it more and more and more.

269
00:24:27,920 --> 00:24:29,920
But it's not something that happens overnight.

270
00:24:29,920 --> 00:24:32,920
You can't be mindful one day and expect the next day to be enlightened,

271
00:24:32,920 --> 00:24:35,920
or even to be happy actually.

272
00:24:35,920 --> 00:24:38,920
You might still be miserable the next day and then you wonder,

273
00:24:38,920 --> 00:24:39,920
what good was yesterday?

274
00:24:39,920 --> 00:24:42,920
I practiced and I was still miserable.

275
00:24:42,920 --> 00:24:47,920
So the practice we have to see it as something gradual.

276
00:24:47,920 --> 00:24:52,920
The Buddha said, just as the ocean slopes gradually and doesn't immediately drop off.

277
00:24:52,920 --> 00:24:55,920
So too, the Buddha's teaching is a gradual teaching.

278
00:24:55,920 --> 00:25:00,920
One that goes gradually and requires patience and requires perseverance.

279
00:25:00,920 --> 00:25:05,920
And requires one to be steadfast like the fire.

280
00:25:05,920 --> 00:25:11,920
So the Buddha said, we should never be discouraged thinking that it's not coming quickly

281
00:25:11,920 --> 00:25:14,920
or thinking that we're gaining nothing.

282
00:25:14,920 --> 00:25:17,920
It just does drops going to the cup.

283
00:25:17,920 --> 00:25:21,920
Eventually it fills up and overflows the Buddha said,

284
00:25:21,920 --> 00:25:26,920
gave his reassurance here that it's not possible for such a person to fall away

285
00:25:26,920 --> 00:25:28,920
because they're developing good habits.

286
00:25:28,920 --> 00:25:31,920
What's the key here is that every mind state,

287
00:25:31,920 --> 00:25:36,920
every moment that we react or interact with an object,

288
00:25:36,920 --> 00:25:39,920
we're changing who we are, we're changing our habits,

289
00:25:39,920 --> 00:25:43,920
we're changing the way the mind works.

290
00:25:43,920 --> 00:25:46,920
Every moment, and so every moment of mindfulness,

291
00:25:46,920 --> 00:25:49,920
we are changing who we are, we're not static beings.

292
00:25:49,920 --> 00:25:53,920
Everything changes us, every instant changes who we are.

293
00:25:53,920 --> 00:25:57,920
And so every moment we have the ability to shape our future

294
00:25:57,920 --> 00:26:01,920
and to change the direction of our lives.

295
00:26:01,920 --> 00:26:04,920
The Buddha said, not only did they not fall away,

296
00:26:04,920 --> 00:26:06,920
but even further more such a person,

297
00:26:06,920 --> 00:26:11,920
Niva Anas, Avas, is quite close to Niva Anas.

298
00:26:11,920 --> 00:26:14,920
So as our practice progresses,

299
00:26:14,920 --> 00:26:17,920
the sign that we're becoming closer to Niva Anas,

300
00:26:17,920 --> 00:26:22,920
closer to freedom, is that our is this key force

301
00:26:22,920 --> 00:26:26,920
that the mind begins to delight, begins to leap out at the chance

302
00:26:26,920 --> 00:26:27,920
to be mindful.

303
00:26:27,920 --> 00:26:29,920
In the beginning, we have to push ourselves to be mindful

304
00:26:29,920 --> 00:26:33,920
and stop ourselves from clinging to things.

305
00:26:33,920 --> 00:26:37,920
Once we develop in the practice and come closer to freedom,

306
00:26:37,920 --> 00:26:41,920
closer to the realization in Niva Anas,

307
00:26:41,920 --> 00:26:43,920
the mind will be the opposite.

308
00:26:43,920 --> 00:26:48,920
To light and heedfulness, it will cringe at the idea of defilement.

309
00:26:48,920 --> 00:26:55,920
It will move away from any desire for clinging

310
00:26:55,920 --> 00:27:01,920
or for being for a version towards things.

311
00:27:01,920 --> 00:27:03,920
So our practice should be in this regard

312
00:27:03,920 --> 00:27:06,920
and we shouldn't have any doubt about this

313
00:27:06,920 --> 00:27:09,920
because we can seek clearly what

314
00:27:09,920 --> 00:27:12,920
mindfulness does, what vigilance does,

315
00:27:12,920 --> 00:27:14,920
what mindfulness does to our mind.

316
00:27:14,920 --> 00:27:17,920
So the more that we can create in our minds,

317
00:27:17,920 --> 00:27:19,920
the more we can develop this habit,

318
00:27:19,920 --> 00:27:21,920
the closer we become to Niva Anas,

319
00:27:21,920 --> 00:27:23,920
the closer we come to Niva Anas,

320
00:27:23,920 --> 00:27:27,920
and to our mind delights in it and eventually inclines towards it.

321
00:27:27,920 --> 00:27:30,920
And our practice becomes a work of its own,

322
00:27:30,920 --> 00:27:33,920
and there's nothing more for us to do

323
00:27:33,920 --> 00:27:35,920
because of the inclination of the mind,

324
00:27:35,920 --> 00:27:42,920
at which point the mind enters into Niva Anas's freedom from suffering.

325
00:27:42,920 --> 00:27:45,920
So just another short teaching, this is two verses,

326
00:27:45,920 --> 00:27:48,920
and we've now finished the up-a-ma-da-da.

327
00:27:48,920 --> 00:27:50,920
So thank you all for listening

328
00:27:50,920 --> 00:27:53,920
and now we continue with our practice.

