1
00:00:00,000 --> 00:00:09,400
Sophie, everyone, I'm Olivia, a volunteer with Siramungalow International, and today

2
00:00:09,400 --> 00:00:13,860
on behalf of our audio library project, I'm happy to announce that many of Venerable

3
00:00:13,860 --> 00:00:20,120
U.D.A.L.'s talks currently visible as videos on YouTube have been converted to podcasts

4
00:00:20,120 --> 00:00:23,000
for the ease of listening.

5
00:00:23,000 --> 00:00:28,240
Our current podcast offerings include Domitox, Question and Answer sessions, talks from

6
00:00:28,240 --> 00:00:31,560
the Domitox series, and many more.

7
00:00:31,560 --> 00:00:36,360
Talks that you'll find on our audio library not currently visible on YouTube include

8
00:00:36,360 --> 00:00:42,700
the Jotica Tales, live sessions from a meditation course in Los Angeles in 2009, the

9
00:00:42,700 --> 00:00:48,120
Visudimaga study group sessions, and the How to Meditate Audio Booklet in Spanish and English

10
00:00:48,120 --> 00:00:49,120
at present.

11
00:00:49,120 --> 00:00:55,000
Sadhu, to Sebastian Koshavra and Shrada for all their hard work and dedication to this

12
00:00:55,000 --> 00:00:59,480
project and for sharing the podcasts with all.

13
00:00:59,480 --> 00:01:06,560
If you would like to access the podcasts for yourself, go to podcast.seramungalow.org or search

14
00:01:06,560 --> 00:01:12,680
you to demo in any of the common podcast apps or sites such as Google Podcast, Apple

15
00:01:12,680 --> 00:01:16,880
Podcast, Spotify, and others.

16
00:01:16,880 --> 00:01:33,000
May you be well.

