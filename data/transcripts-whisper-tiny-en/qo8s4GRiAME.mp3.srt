1
00:00:00,000 --> 00:00:04,080
I am a Sri Lankan now living in Canada and have been following your YouTube channel,

2
00:00:04,080 --> 00:00:08,880
and let me thank you for the information and guidance to give us via that.

3
00:00:08,880 --> 00:00:14,160
I happen to stumble upon the live broadcast today. With respect to the venerable Bikuni,

4
00:00:14,160 --> 00:00:18,560
who is with you, I would like to ask a simple question about the attire of Buddhist monks.

5
00:00:19,360 --> 00:00:20,880
What is the attire of a Bikuni?

6
00:00:20,880 --> 00:00:42,240
It is basically the same as for a monk. We have a set of three ropes and in addition to extra parts,

7
00:00:42,240 --> 00:00:52,800
which the monks do not have. We have in total five parts that we are allowed to wear, which is

8
00:00:54,240 --> 00:01:02,560
upper rope, which can be a double layer, to keep us warm. Nowadays, it is basically only used for

9
00:01:03,840 --> 00:01:11,360
ceremonies and many monasteries, but we use it here to wear it, to keep us warm, although

10
00:01:11,360 --> 00:01:24,480
it is warm enough, and then we have the upper rope. So that was the outer rope.

11
00:01:24,480 --> 00:01:32,800
That was the outer rope. Then we have the upper rope, which covers the upper part of the body,

12
00:01:32,800 --> 00:01:44,400
and this large enough to keep the lower part of the body until over the knees covered. It is,

13
00:01:46,560 --> 00:01:56,400
how can I explain it, worn around one shoulder, and one shoulder is bare. I wouldn't say it's

14
00:01:56,400 --> 00:02:03,040
like, no, I can't say it's like a Zorokay, worn over one shoulder. I would like a Toga.

15
00:02:04,960 --> 00:02:07,760
Like the Toga of the Greeks, Toga, you know, Socrates?

16
00:02:09,920 --> 00:02:14,880
They modeled it around Buddhist one. And outside, of course, over both shoulders, when you go.

17
00:02:14,880 --> 00:02:31,360
Then we have one lower rope, which is, which we wear around our hips, and going, it going down

18
00:02:31,360 --> 00:02:40,880
to the weights, to the, well, it covers the knees. It doesn't have to go to the ankle, I think.

19
00:02:40,880 --> 00:02:50,880
The calves, I'm just thinking of the word you're looking for. The shins, the lower part of the leg.

20
00:02:50,880 --> 00:03:04,560
Yeah, it's somewhere. Anyway, then that is the basic set, and then in addition, we have something

21
00:03:04,560 --> 00:03:21,200
to cover our breast, to not make it seen when we go out. This is something that should be worn

22
00:03:22,640 --> 00:03:33,520
always when we go out. It's not necessary to wear it in a monastery, but I think it's

23
00:03:33,520 --> 00:03:38,800
advisable to do so. I looked it up in the commentary. I don't know if you're aware of sorry,

24
00:03:38,800 --> 00:03:45,200
it is to cut in. And the commentary says that it's from the bottom below the tailbone to the

25
00:03:45,200 --> 00:03:57,120
navel, whether that covers the navel, you knew that. And then we have an extra cloth that is

26
00:03:57,120 --> 00:04:07,600
pretty similar to the lower rope that we have. And that is a bathing cloth. So if we ever go to

27
00:04:08,880 --> 00:04:18,560
bathe in public, and like many people do here in Sri Lanka, they bathe in ponds or in rivers.

28
00:04:18,560 --> 00:04:28,320
So if we do that as a bikuni, we wrap ourselves in that bathing cloth. And it covers the difference

29
00:04:28,320 --> 00:04:32,880
between the bikos bathing cloth, the bikos bathing cloth, the lower body, the bikuni bathing

30
00:04:32,880 --> 00:04:40,160
cloth covers the whole upper body as well. And the neat thing about living here is you can actually,

31
00:04:40,160 --> 00:04:45,120
as you said, you can see them. People are wearing similar things. When we go on arms around,

32
00:04:45,120 --> 00:04:52,080
even we see them bathing in similar fashion. Even we are showing the Angulimala movie. And you can

33
00:04:52,080 --> 00:04:59,600
see the woman wearing a similar wrap. That's exactly goes in the lower below the collarbone to the

34
00:04:59,600 --> 00:05:04,240
navel. And we're pointing out that all the women were wearing a similar dress and that would give

35
00:05:04,240 --> 00:05:14,880
you traditional Indian dress. And there is no blouse. Right. In the set of ropes. There's no pants.

36
00:05:14,880 --> 00:05:23,920
In the set of ropes. No pants. No long warm trousers and so on. That's all there. That's all there

37
00:05:23,920 --> 00:05:34,800
allowed and very similar to the monks. In cold countries, moms could wear a very thick double layer

38
00:05:35,360 --> 00:05:41,200
outer rope to keep themselves warm. I've done that. I wore a blanket here in the Mojave Desert.

39
00:05:41,200 --> 00:05:49,680
We got a Sangatina. It was a blanket. They showed them at Watay of Los Angeles. And they were

40
00:05:49,680 --> 00:05:54,560
looking at me like, that's a blanket. Well, yeah, that's what the Sangatina is for. And so when I was

41
00:05:54,560 --> 00:05:59,120
in Mojave Desert, you know, like negative, you know, it was, I don't know what you're saying,

42
00:05:59,120 --> 00:06:05,840
American and Fahrenheit, but it was below freezing. Yeah, quite, quite useful.

43
00:06:05,840 --> 00:06:10,560
We're not allowed in general to wear lay clothes. That's one thing we didn't make.

44
00:06:11,760 --> 00:06:22,880
So there are restrictions referring to clothes that we are wearing. And a blouse is definitely a

45
00:06:22,880 --> 00:06:32,640
lay cloth. Lay people's clothing. So we should not wear it. I mean, some people, some bikinis

46
00:06:32,640 --> 00:06:39,040
do wear it and they have their reasons. And it's for, well, you were saying that it may actually

47
00:06:39,040 --> 00:06:44,000
stem from the fact that there was a Catholic. I mean, we have to be careful. I don't know if we

48
00:06:44,000 --> 00:06:55,920
would the hurt. But I don't have any proof of it. I heard that they wanted to restore the nuns

49
00:06:55,920 --> 00:07:04,240
and that a Catholic woman helped to design the ropes that they are going to wear.

50
00:07:04,240 --> 00:07:10,320
And the meaning of that is that, you know, this was, you know, sort of as a model taking the Catholic

51
00:07:10,320 --> 00:07:16,880
into some model, which is very kind of earned. It is great to see how the religions work together

52
00:07:16,880 --> 00:07:22,720
in that way. And I don't think she had any ulterior motives at all. But the point is that that

53
00:07:22,720 --> 00:07:31,920
maybe where that that idea came from. Because yeah. And I had something in mind to say

54
00:07:31,920 --> 00:07:53,120
I'm talking over you. Well, it's lost.

