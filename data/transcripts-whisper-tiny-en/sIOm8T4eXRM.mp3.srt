1
00:00:00,000 --> 00:00:05,000
Doing meditation right before sleep, yes, yes, no, no.

2
00:00:05,000 --> 00:00:09,000
I've heard that mind learns to associate meditation with sleep,

3
00:00:09,000 --> 00:00:14,000
and so when doing it at daytime one will have a hard time not to fall asleep

4
00:00:14,000 --> 00:00:17,000
because mind is conditioned. Your stance on it?

5
00:00:17,000 --> 00:00:24,000
No, that's certainly not the way we look at it.

6
00:00:31,000 --> 00:00:36,000
I think they're asking if you use meditation, if you do meditation right before you go to sleep,

7
00:00:36,000 --> 00:00:40,000
will you associate it with sleep, and then you think I'm sleepy during the day?

8
00:00:40,000 --> 00:00:47,000
I understand that it's interesting.

9
00:00:47,000 --> 00:00:52,000
Possibly that could happen, but it would mean you're not being mindful.

10
00:00:52,000 --> 00:00:59,000
Anytime you make that kind of an association, there's more going on than it's not passive.

11
00:00:59,000 --> 00:01:09,000
You see, you would have to cultivate the mental inclination to fall asleep.

12
00:01:09,000 --> 00:01:13,000
What happens when you're mindful is the exact opposite.

13
00:01:13,000 --> 00:01:25,000
You start to associate lying down with clarity, because you're actually in a sense fighting,

14
00:01:25,000 --> 00:01:30,000
but more like retraining the mind.

15
00:01:30,000 --> 00:01:35,000
Normally when you lie down, it's like let your mind go, let it wander until you pass out,

16
00:01:35,000 --> 00:01:40,000
right, from just exhaustion, really.

17
00:01:40,000 --> 00:01:44,000
Instead of doing that, we're having a clarity of mind.

18
00:01:44,000 --> 00:01:49,000
In fact, people often complain that it's harder to fall asleep when you're mindful.

19
00:01:49,000 --> 00:01:57,000
You end up just lying there awake, which is cool because that means you didn't really need to sleep anyway.

20
00:01:57,000 --> 00:02:00,000
But it's a retraining.

21
00:02:00,000 --> 00:02:07,000
Normally when we lie down, we enjoy the pleasure of the softness of our beds,

22
00:02:07,000 --> 00:02:10,000
which is why we have people sleep on the floor.

23
00:02:10,000 --> 00:02:17,000
When I started meditating, back in my day, you all have it easy, back in my day.

24
00:02:17,000 --> 00:02:23,000
When I was your age, no, but really this is a good back in my day story.

25
00:02:23,000 --> 00:02:25,000
We really did.

26
00:02:25,000 --> 00:02:30,000
We didn't have the worst of it. I've heard worse stories, concrete and so on, but we had nice,

27
00:02:30,000 --> 00:02:34,000
Parquet wooden floor.

28
00:02:34,000 --> 00:02:39,000
With the thinnest of mattresses, lumpy, the mattress was like,

29
00:02:39,000 --> 00:02:44,000
if you've ever seen these Thai monk mattresses, they're torture devices, they're lumps and they're,

30
00:02:44,000 --> 00:02:52,000
it's just two pieces of cloth with some fiber cloth in it.

31
00:02:52,000 --> 00:02:56,000
And me being bony is all get out.

32
00:02:56,000 --> 00:02:58,000
It was really torture.

33
00:02:58,000 --> 00:03:01,000
I couldn't lie on my side that I normally do.

34
00:03:01,000 --> 00:03:03,000
It was just too painful.

35
00:03:03,000 --> 00:03:08,000
So I had to lie on my back, which I never do, which apparently is quite healthy though.

36
00:03:08,000 --> 00:03:15,000
I would lie on my back and I couldn't fall asleep.

37
00:03:15,000 --> 00:03:17,000
Yeah, maybe that wasn't such a good story.

38
00:03:17,000 --> 00:03:25,000
It's not really what I recommend, but there is a point to it that the,

39
00:03:25,000 --> 00:03:28,000
you end up sleeping just as much as you need.

40
00:03:28,000 --> 00:03:35,000
And it is a good point because I think back to this other story where I actually,

41
00:03:35,000 --> 00:03:46,000
I was in California and everything was kind of falling apart and I had come back from

42
00:03:46,000 --> 00:03:51,000
Minnesota, I was teaching briefly in Minnesota, I came back.

43
00:03:51,000 --> 00:03:55,000
I had no place to stay and so someone was inviting me to stay in their house,

44
00:03:55,000 --> 00:03:59,000
but there was a woman in the house, so I wasn't comfortable with that.

45
00:03:59,000 --> 00:04:03,000
And we got to their house and got out of the car and I just walked away.

46
00:04:03,000 --> 00:04:06,000
Anyway, let's cut to the chase.

47
00:04:06,000 --> 00:04:10,000
I ended up sleeping under a picnic table in North Hollywood,

48
00:04:10,000 --> 00:04:14,000
a very dangerous part of the city.

49
00:04:14,000 --> 00:04:18,000
It was under a picnic table on the concrete and it was wonderful.

50
00:04:18,000 --> 00:04:22,000
I still think of that as one of the best nights in my life because I was totally free.

51
00:04:22,000 --> 00:04:24,000
I guess the context is important.

52
00:04:24,000 --> 00:04:27,000
Up into that point I had been so trapped.

53
00:04:27,000 --> 00:04:29,000
It's like, where am I going to stay?

54
00:04:29,000 --> 00:04:31,000
Where am I going to set up?

55
00:04:31,000 --> 00:04:35,000
And everything was, whatever was the problem going here going there.

56
00:04:35,000 --> 00:04:43,000
I guess I had dependent on people and you have to bend to this and to that.

57
00:04:43,000 --> 00:04:51,000
And then I just walked out, walked out and decided to live that night on my terms.

58
00:04:51,000 --> 00:04:54,000
And size went and slept under a picnic table.

59
00:04:54,000 --> 00:04:56,000
And it was so wonderful.

60
00:04:56,000 --> 00:05:00,000
It was freezing cold, you know, winters in California are not fun.

61
00:05:00,000 --> 00:05:08,000
That night, it gets down to almost zero, I think, almost freezing.

62
00:05:08,000 --> 00:05:13,000
You know, it's not zero, but almost freezing.

63
00:05:13,000 --> 00:05:16,000
And the desert at night, no.

64
00:05:16,000 --> 00:05:18,000
And the concrete.

65
00:05:18,000 --> 00:05:21,000
And so all I had were these nice thick robes.

66
00:05:21,000 --> 00:05:25,000
And so I kind of bunched them up in front of me and lay down.

67
00:05:25,000 --> 00:05:27,000
I did sitting for a while.

68
00:05:27,000 --> 00:05:35,000
And then eventually I just got too tired and I lay down almost face first in the concrete.

69
00:05:35,000 --> 00:05:36,000
And I slept.

70
00:05:36,000 --> 00:05:37,000
I slept for like three hours.

71
00:05:37,000 --> 00:05:38,000
And it was great.

72
00:05:38,000 --> 00:05:46,000
It was really, it was a wonderful, my mom.

73
00:05:46,000 --> 00:05:54,000
It was a wonderful, wonderful time.

74
00:05:54,000 --> 00:06:00,000
And that relates to your question just simply because you don't need that much sleep.

75
00:06:00,000 --> 00:06:03,000
If you need to sleep, you fall asleep.

76
00:06:03,000 --> 00:06:06,000
And that's a part of what we're aiming for.

77
00:06:06,000 --> 00:06:16,000
So being mindful will allow you to stay awake.

78
00:06:16,000 --> 00:06:23,000
There are monks, there was a monk once who said he could do twenty-four hours of lying meditation.

79
00:06:23,000 --> 00:06:28,000
This Burmese monk who claimed to be able to do twelve hours of standing, twelve hours of sitting,

80
00:06:28,000 --> 00:06:32,000
and twenty-four hours of lying meditation.

81
00:06:32,000 --> 00:06:37,000
He claimed to be able to be mindful for that long.

82
00:06:37,000 --> 00:06:40,000
Which is pretty darn impressive.

83
00:06:40,000 --> 00:06:45,000
So it wouldn't be that you change your relationship to lying, you know, to lying down.

84
00:06:45,000 --> 00:06:49,000
Lying down becomes a mindful exercise.

85
00:06:49,000 --> 00:06:53,000
It becomes enlightening.

86
00:06:53,000 --> 00:06:57,000
And for your online meditation group, you'll have to add another column.

87
00:06:57,000 --> 00:06:59,000
You have sitting meditation, walking meditation.

88
00:06:59,000 --> 00:07:01,000
You have to have the lying meditation.

89
00:07:01,000 --> 00:07:03,000
I do that sometimes.

90
00:07:03,000 --> 00:07:06,000
But you can still consider that sitting meditation.

91
00:07:06,000 --> 00:07:13,000
When I do lying meditation, I consider it lying isn't lying to myself that I'm not doing because I'm falling sweet.

92
00:07:13,000 --> 00:07:14,000
Right.

93
00:07:14,000 --> 00:07:15,000
No, but you can't.

94
00:07:15,000 --> 00:07:19,000
I mean, it's considered sitting because the same practice.

95
00:07:19,000 --> 00:07:23,000
You still do the rising, falling, lying, touching, you know.

96
00:07:23,000 --> 00:07:26,000
Rising, falling, lying, and so on.

97
00:07:26,000 --> 00:07:30,000
Instead of rising, falling, sitting, you do rising, falling, lying.

98
00:07:30,000 --> 00:07:33,000
So, yeah.

99
00:07:33,000 --> 00:07:36,000
Do it instead of the sitting meditation.

100
00:07:36,000 --> 00:07:39,000
It's quite useful if sitting meditation is.

101
00:07:39,000 --> 00:07:41,000
Lying meditation is useful when you're distracted.

102
00:07:41,000 --> 00:07:45,000
So if in sitting your mind is still buzzing and you just can't focus,

103
00:07:45,000 --> 00:07:48,000
lie down and you'll find yourself able to focus.

104
00:07:48,000 --> 00:07:49,000
But it's dangerous.

105
00:07:49,000 --> 00:07:50,000
It's easy to fall asleep.

106
00:07:50,000 --> 00:07:52,000
The point being, and the answer to your question is that,

107
00:07:52,000 --> 00:07:55,000
no, the key is to be clearly mindful.

108
00:07:55,000 --> 00:08:00,000
And when you're mindful, it will actually retrain you.

109
00:08:00,000 --> 00:08:09,000
And we knew off of the need to sleep.

110
00:08:09,000 --> 00:08:33,000
Okay.

