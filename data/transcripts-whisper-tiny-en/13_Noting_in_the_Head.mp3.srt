1
00:00:00,000 --> 00:00:06,960
noting in the head question, the instructions say that the mantra should not be at the mouth

2
00:00:06,960 --> 00:00:13,600
or in the head. Whenever I note something, it appears in my head as a thought when I am tired,

3
00:00:13,600 --> 00:00:22,640
I am thinking tired, tired, tired. Is this important? Answer, when you get better at noting,

4
00:00:22,640 --> 00:00:28,080
the thought is with the object, the mantra being at the head is usually a product of being

5
00:00:28,080 --> 00:00:34,000
accustomed to conceptual thought. When given this exercise, the untrained mind's only way

6
00:00:34,000 --> 00:00:40,480
of implementing it is to create it as a thought in the head. When I started, I was actually

7
00:00:40,480 --> 00:00:46,640
seeing the words like they were pasted onto the inside of my forehead and was getting headaches

8
00:00:46,640 --> 00:00:52,800
to do this. Part of why people think it is in the head is because in the beginning, they often

9
00:00:52,800 --> 00:00:59,120
get tension in the head from forcing themselves to be with the object. So they confuse that with

10
00:00:59,120 --> 00:01:05,520
the actual acknowledgement. They think, I am acknowledging in my head that they are not. Their head is

11
00:01:05,520 --> 00:01:12,320
just giving rise to tension as a result of the person's tense or through controlling mind state.

12
00:01:12,320 --> 00:01:18,240
This is just a matter of a lack of proficiency. Once you become proficient in the practice,

13
00:01:18,240 --> 00:01:25,040
the thoughts themselves become natural and a part of the experience. They are just experiencing

14
00:01:25,040 --> 00:01:32,640
reality as it is, as do the word is said in the foot of stomach itself. The only time it should be

15
00:01:32,640 --> 00:02:02,480
in the head is when you are acknowledging pain, aching, tension or feelings in the head.

