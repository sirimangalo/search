1
00:00:00,000 --> 00:00:03,760
I mean, mana, physical place, does the Buddha reside there with other arhans?

2
00:00:07,520 --> 00:00:11,440
Clementi? Martin, Martin, you're quiet over there.

3
00:00:13,120 --> 00:00:15,040
That's always snow, right away.

4
00:00:18,400 --> 00:00:20,960
Oh, and all of these times the Kukko, hang out.

5
00:00:22,480 --> 00:00:27,280
Well, there is a concept, there are a concept in the Mahayana of

6
00:00:27,280 --> 00:00:31,200
Pure lands, you know, of Buddhas.

7
00:00:34,240 --> 00:00:38,240
But did Siddhartha go Tama? I don't think so.

8
00:00:39,920 --> 00:00:48,080
I believe that once an arrowhat becomes a Buddha, that's the end of earth.

9
00:00:48,080 --> 00:00:58,960
So, I don't see them being born into a place that you would call the realm of Narhana.

10
00:01:00,640 --> 00:01:05,520
Well, Lymani, Narhana is kind of Imutti, it's free from the aggregate.

11
00:01:06,160 --> 00:01:10,880
So, there's no physical, first of all, if there were physical place, then it wouldn't be Lymana,

12
00:01:11,360 --> 00:01:15,040
because there would be the physical. If there was an experience of the physical,

13
00:01:15,040 --> 00:01:22,400
then there would be the mental aggregate feeling, perception, thoughts and consciousness.

14
00:01:22,400 --> 00:01:27,680
With all of, with any of those are all of those coming together, then it's not Lymana as well.

15
00:01:27,680 --> 00:01:32,640
So, that's an idea of the sort of criteria you can use to answer such a question.

16
00:01:32,640 --> 00:01:36,480
Does it have any, any or all of the five aggregates? Yes, then it's not Lymana.

17
00:01:38,800 --> 00:01:43,440
So, then what is it? Didn't we answer that one? Didn't we answer what is Lymana or something?

18
00:01:43,440 --> 00:01:49,520
Lymana is perfect happiness, perfect peace, freedom from suffering. That's what Lymana.

19
00:01:52,880 --> 00:02:01,600
I think what needs to be elucidated here is the distinction which I think the person asking the

20
00:02:01,600 --> 00:02:10,800
comment is really trying to apply. It is that there is a distinction between relative Narhana,

21
00:02:10,800 --> 00:02:19,520
like, and Parhana Narhana. And I really think that they're asking, does the Buddha exist after

22
00:02:19,520 --> 00:02:24,560
Parhana Narhana? Right. Well, the Buddha never existed in the first place. It's a concept.

23
00:02:25,200 --> 00:02:30,640
That's the answer to that one. People opt and ask, well, then does an Rahaan cease to exist at that?

24
00:02:33,520 --> 00:02:34,880
And that's not a proper question.

25
00:02:34,880 --> 00:02:40,400
It's like saying, it's like asking, when did you stop beating your wife?

26
00:02:42,000 --> 00:02:48,720
Well, enough started. I never beat my wife. You can't ask me that question.

27
00:02:48,720 --> 00:03:00,480
When did you give up crack cocaine?

28
00:03:00,480 --> 00:03:17,920
That's these trap questions where they ask a question that creates an assumption.

