1
00:00:00,000 --> 00:00:04,800
Hello and welcome back to our study of the Dhamapada.

2
00:00:04,800 --> 00:00:14,280
Today we continue on with first number 97 which reads as follows.

3
00:00:14,280 --> 00:00:24,960
As adhodha dho, Akatanyu jah, Santi jah dho to Yonaroh, Atawaka so wontas, so sahwe utamapore

4
00:00:25,960 --> 00:00:41,800
which on the face means one who is faithless, as adhodha, Akatanyu, one who is ungrateful,

5
00:00:41,800 --> 00:00:49,800
Santi jah dho, a robber, someone who breaks into other people's houses,

6
00:00:49,800 --> 00:00:58,000
to Yonaroh, whatever man or person, Atawaka so has destroyed opportunity,

7
00:00:58,000 --> 00:01:06,600
his cut off has opportunity cut off, one tasso is hopeless.

8
00:01:06,600 --> 00:01:15,000
sahwe utamapore so such a person is the height of humanity.

9
00:01:15,000 --> 00:01:25,000
So a rather odd verse coming from the Buddha, but you have to understand the context

10
00:01:25,000 --> 00:01:30,800
and then we'll explain that on face value it means one thing.

11
00:01:30,800 --> 00:01:34,400
But when you understand the context and when you think about it,

12
00:01:34,400 --> 00:01:40,800
when a person looks at the poly words, it means something quite different.

13
00:01:40,800 --> 00:01:48,200
So the story is again about sahwe utam and it's in regards to 30 monks

14
00:01:48,200 --> 00:01:55,000
who had been dwelling in the forest, intently practicing meditation,

15
00:01:55,000 --> 00:01:59,600
and the Buddha saw that there was somehow he saw that they needed something,

16
00:01:59,600 --> 00:02:10,000
they needed perhaps to move away from faith one might think, based on the story.

17
00:02:10,000 --> 00:02:13,600
The Buddha anyway saw that they needed some push and if they were given a push

18
00:02:13,600 --> 00:02:21,000
in the right direction it might put them back on the right path and lead them to become enlightened.

19
00:02:21,000 --> 00:02:23,600
He saw potential in them.

20
00:02:23,600 --> 00:02:36,200
And so he used sahri put that as his catalyst by asking sahri put a question

21
00:02:36,200 --> 00:02:38,000
when they were in his presence.

22
00:02:38,000 --> 00:02:42,200
So these monks came to the Buddha, they were back to the Buddha,

23
00:02:42,200 --> 00:02:46,400
decided how he was going to teach them, he turned to sahri put them and he says,

24
00:02:46,400 --> 00:02:58,600
sahri put that, do you have faith, sad dahasi, do you believe basically, do you believe

25
00:02:58,600 --> 00:03:10,400
that the faculty of faith when cultivated, when made much of leads to the deathless

26
00:03:10,400 --> 00:03:15,600
and ends in the deathless?

27
00:03:15,600 --> 00:03:19,000
And asked about all five of the faculties, it's not just the faith faculty,

28
00:03:19,000 --> 00:03:23,600
but there are five faculties, there's sad dah, which is faith or confident,

29
00:03:23,600 --> 00:03:30,600
media, which is effort, sad dah, which is mindfulness,

30
00:03:30,600 --> 00:03:38,200
samadhi, concentration or focus, and bhanya, which is wisdom.

31
00:03:38,200 --> 00:03:43,200
So he asked about these five faculties, but he asked, do you believe?

32
00:03:43,200 --> 00:03:48,200
And sahri put to give a sort of a nuanced reply.

33
00:03:48,200 --> 00:03:50,400
He didn't just say yes or no.

34
00:03:50,400 --> 00:03:55,200
He said, it's not out of faith.

35
00:03:55,200 --> 00:04:01,200
He said, nakwahang bhandi eta bhagavantu santai yakachami.

36
00:04:01,200 --> 00:04:12,200
I don't get to that out of faith in the blessed one.

37
00:04:12,200 --> 00:04:16,200
Because we're in regards to the faith's faculty being, when cultivated,

38
00:04:16,200 --> 00:04:20,200
leading to nybana and same with the effort, faculty and the mindfulness faculty

39
00:04:20,200 --> 00:04:25,200
and the concentration faculty and the wisdom faculty.

40
00:04:25,200 --> 00:04:33,200
And he says, npa, so basically he says, I don't go to faith in that regard.

41
00:04:33,200 --> 00:04:35,200
Faith in the blessed one in that regard.

42
00:04:35,200 --> 00:04:39,200
That's a polyway of saying, I don't have faith in the blessed one in that regard

43
00:04:39,200 --> 00:04:44,200
or it can be interpreted to mean that.

44
00:04:44,200 --> 00:04:48,200
And he said, but someone who has not seen the truth,

45
00:04:48,200 --> 00:04:52,200
who has not come to realize the truth for themselves,

46
00:04:52,200 --> 00:05:02,200
such a person would have faith in the blessed one thus.

47
00:05:02,200 --> 00:05:06,200
They thought that but a song sahri, they would have faith in someone else

48
00:05:06,200 --> 00:05:11,200
or would go to get to this by faith in someone else.

49
00:05:11,200 --> 00:05:14,200
So I don't go to faith in the blessed one.

50
00:05:14,200 --> 00:05:18,200
I don't get to that through faith in the blessed one.

51
00:05:18,200 --> 00:05:24,200
And as Buddhist, we tend to understand what he meant.

52
00:05:24,200 --> 00:05:32,200
It's not out of faith because he says, for someone who has not seen,

53
00:05:32,200 --> 00:05:35,200
not understood for themselves, they need to have faith.

54
00:05:35,200 --> 00:05:39,200
So it's actually pretty clear, but the poly somehow makes it ambiguous.

55
00:05:39,200 --> 00:05:47,200
And these 30 monks, again with the criticism, start talking amongst themselves

56
00:05:47,200 --> 00:05:49,200
and saying, even sari put it, it's amazing.

57
00:05:49,200 --> 00:05:51,200
He hasn't given up his wrong views.

58
00:05:51,200 --> 00:05:53,200
He's still faithless.

59
00:05:53,200 --> 00:05:55,200
He has no faith in the Buddha.

60
00:05:55,200 --> 00:06:01,200
He still after all this time doesn't have this faith in the Buddha.

61
00:06:01,200 --> 00:06:08,200
It's a valid criticism. I mean, it wouldn't be if they hadn't misunderstood what he was saying.

62
00:06:08,200 --> 00:06:16,200
If he had actually meant that, he just had no faith in the Buddha whatsoever.

63
00:06:16,200 --> 00:06:20,200
But of course, the Buddha hears about what they're saying and says,

64
00:06:20,200 --> 00:06:23,200
monks, what are you talking about?

65
00:06:23,200 --> 00:06:27,200
When they tell him, he explains to them.

66
00:06:27,200 --> 00:06:33,200
He says, but when I asked him, he answered me quite clearly.

67
00:06:33,200 --> 00:06:36,200
And what he meant was that it's not faith.

68
00:06:36,200 --> 00:06:37,200
It's not out of faith.

69
00:06:37,200 --> 00:06:39,200
It's through understanding.

70
00:06:39,200 --> 00:06:44,200
And because he has seen the truth through the practice of

71
00:06:44,200 --> 00:06:47,200
some intimidation, we pass in the meditation,

72
00:06:47,200 --> 00:06:51,200
has realized the path and fruition.

73
00:06:51,200 --> 00:06:57,200
Sadhana, parisana, sadhana, kachati doesn't require faith.

74
00:06:57,200 --> 00:07:01,200
It doesn't get there through faith in another.

75
00:07:01,200 --> 00:07:06,200
It gets there through his own knowledge.

76
00:07:06,200 --> 00:07:08,200
And then he said this verse.

77
00:07:08,200 --> 00:07:12,200
And so what's interesting about this verse is it confuses them even more, right?

78
00:07:12,200 --> 00:07:15,200
He says, someone who is faithless is the best.

79
00:07:15,200 --> 00:07:17,200
It's the height.

80
00:07:17,200 --> 00:07:20,200
And the meaning is they don't have faith.

81
00:07:20,200 --> 00:07:24,200
They don't require faith to know the truth.

82
00:07:24,200 --> 00:07:29,200
Such a person who has seen the truth for themselves and no longer requires,

83
00:07:29,200 --> 00:07:31,200
no longer has to believe, right?

84
00:07:31,200 --> 00:07:36,200
So you tell them the truth and they can verify or deny it.

85
00:07:36,200 --> 00:07:40,200
They can verify the truth without requiring belief.

86
00:07:40,200 --> 00:07:43,200
If you say, it's like this, they don't have to believe you.

87
00:07:43,200 --> 00:07:45,200
They know it is the truth.

88
00:07:45,200 --> 00:07:47,200
It's better, obviously.

89
00:07:47,200 --> 00:07:54,200
Akata knew, and then he goes on to confuse them more or play with them more.

90
00:07:54,200 --> 00:07:59,200
He says, Akata knew, Akata knew literally means one who,

91
00:07:59,200 --> 00:08:01,200
literally means one of two things.

92
00:08:01,200 --> 00:08:03,200
But Akata means not.

93
00:08:03,200 --> 00:08:05,200
Akata means done.

94
00:08:05,200 --> 00:08:10,200
And Anu, Anu means one who knows.

95
00:08:10,200 --> 00:08:15,200
So it usually means one who knows not what was done

96
00:08:15,200 --> 00:08:20,200
in the sense of knowing not what someone else has done for you.

97
00:08:20,200 --> 00:08:24,200
So if I do you a favor and you don't keep that in mind,

98
00:08:24,200 --> 00:08:28,200
you aren't conscious of the fact that something's been done for you.

99
00:08:28,200 --> 00:08:31,200
It's a way of saying you're ungrateful.

100
00:08:31,200 --> 00:08:36,200
Akata knew is a common word that means it's a compound that means someone who is ungrateful

101
00:08:36,200 --> 00:08:40,200
for the things other people have done for them.

102
00:08:40,200 --> 00:08:45,200
It can also, and the Buddha is using it here in a different way.

103
00:08:45,200 --> 00:08:49,200
It means one who knows that which is not done.

104
00:08:49,200 --> 00:08:54,200
Akata, Anu, Akata means what is not done or what is not made.

105
00:08:54,200 --> 00:08:57,200
The unmade here is Nibana.

106
00:08:57,200 --> 00:09:01,200
So it's a play on words that is of course lost in the English.

107
00:09:01,200 --> 00:09:07,200
But Akata knew means one who knows Nibana.

108
00:09:07,200 --> 00:09:12,200
Sundi Cheda literally means one who has cut the chain.

109
00:09:12,200 --> 00:09:16,200
Sundi means the connection or the chain.

110
00:09:16,200 --> 00:09:22,200
And it is a word that it is a compound that was used to mean one who breaks into people's houses,

111
00:09:22,200 --> 00:09:27,200
cutting through the locks.

112
00:09:27,200 --> 00:09:29,200
The lock on the door.

113
00:09:29,200 --> 00:09:33,200
But here is someone who has picked the lock on Samsara

114
00:09:33,200 --> 00:09:36,200
and unlocked or broken the chain is more correct.

115
00:09:36,200 --> 00:09:38,200
Broken the chain of rebirth.

116
00:09:38,200 --> 00:09:41,200
So being born old sick and dying is a chain.

117
00:09:41,200 --> 00:09:45,200
And someone who has broken that.

118
00:09:45,200 --> 00:09:48,200
Someone who is free from Samsara.

119
00:09:48,200 --> 00:09:56,200
Atabukasu, one who has destroyed opportunity or has had opportunity cut off,

120
00:09:56,200 --> 00:10:00,200
means one who has no opportunities for them.

121
00:10:00,200 --> 00:10:02,200
That's what it normally would mean.

122
00:10:02,200 --> 00:10:06,200
But here it's a specific meaning against a play on words,

123
00:10:06,200 --> 00:10:11,200
but it means for one for whom there is no opportunity for further becoming.

124
00:10:11,200 --> 00:10:18,200
So opportunity is, in a sense, another word for becoming this or becoming that.

125
00:10:18,200 --> 00:10:25,200
They don't have ambitions or plans because they don't think about the future.

126
00:10:25,200 --> 00:10:28,200
And finally, one does so means hopeless.

127
00:10:28,200 --> 00:10:32,200
And hopeless is, of course, a very good thing.

128
00:10:32,200 --> 00:10:33,200
It's an excellent thing to be hopeless.

129
00:10:33,200 --> 00:10:35,200
It means you have no hope.

130
00:10:35,200 --> 00:10:38,200
You don't hope for anything because you have no expectations,

131
00:10:38,200 --> 00:10:41,200
no expectations, no desires.

132
00:10:41,200 --> 00:10:44,200
You aren't living in the future again.

133
00:10:44,200 --> 00:10:47,200
You're living only in the present, perfectly flexible,

134
00:10:47,200 --> 00:10:53,200
and content no matter what happens, no matter what comes or doesn't come.

135
00:10:53,200 --> 00:10:55,200
So this is the meaning would have said that way.

136
00:10:55,200 --> 00:11:00,200
So such a one is indeed the height of humanity.

137
00:11:00,200 --> 00:11:04,200
So what does this mean for our practice?

138
00:11:04,200 --> 00:11:09,200
Well, first, before we actually get into the context,

139
00:11:09,200 --> 00:11:12,200
in the story, the Buddha talks about the five faculties,

140
00:11:12,200 --> 00:11:14,200
which in and of themselves are a really good teaching.

141
00:11:14,200 --> 00:11:18,200
This is a teaching that has been taken from the Sanyatana Gaya,

142
00:11:18,200 --> 00:11:20,200
and it's an important teaching.

143
00:11:20,200 --> 00:11:23,200
We have the concept of these five faculties

144
00:11:23,200 --> 00:11:27,200
and talk about them in the meditation practice.

145
00:11:27,200 --> 00:11:31,200
You have to balance confidence with wisdom

146
00:11:31,200 --> 00:11:34,200
and cultivate them both, but in tandem.

147
00:11:34,200 --> 00:11:37,200
So if you have too much confidence, not enough wisdom,

148
00:11:37,200 --> 00:11:38,200
that's a bad thing.

149
00:11:38,200 --> 00:11:41,200
You have a lot of wisdom, but you don't have any confidence in it,

150
00:11:41,200 --> 00:11:44,200
so you know a lot of things, not really wisdom.

151
00:11:44,200 --> 00:11:47,200
But without the confidence, you're always doubting

152
00:11:47,200 --> 00:11:51,200
the things that you learn and the things that you come to understand.

153
00:11:51,200 --> 00:11:55,200
Do I really, did I really see that? Is this really useful?

154
00:11:55,200 --> 00:11:58,200
Effort and concentration also have to be balanced.

155
00:11:58,200 --> 00:12:01,200
So if you have a lot of effort, you'll become distracted

156
00:12:01,200 --> 00:12:02,200
without enough focus.

157
00:12:02,200 --> 00:12:04,200
If the effort isn't focused enough.

158
00:12:04,200 --> 00:12:07,200
If you have a lot of focus, but not the effort,

159
00:12:07,200 --> 00:12:13,200
you'll fall into a stupor and you'll become tired and fall asleep.

160
00:12:13,200 --> 00:12:15,200
So these two have to be balanced as well,

161
00:12:15,200 --> 00:12:17,200
and they both have to be cultivated.

162
00:12:17,200 --> 00:12:21,200
Now, Sati, Sati is the ability to see things as they are,

163
00:12:21,200 --> 00:12:25,200
basically, to recognize reality.

164
00:12:25,200 --> 00:12:30,200
Sati means to recognize or to remember yourself.

165
00:12:30,200 --> 00:12:33,200
I mean, to remind yourself.

166
00:12:33,200 --> 00:12:37,200
The ability to see things and to recognize things as they are

167
00:12:37,200 --> 00:12:40,200
without any kind of extrapolation.

168
00:12:40,200 --> 00:12:43,200
Not seeing things as good or bad or so on.

169
00:12:43,200 --> 00:12:46,200
So mindfulness is actually what balances

170
00:12:46,200 --> 00:12:51,200
the other faculties, because you don't get off in one way

171
00:12:51,200 --> 00:12:52,200
or another.

172
00:12:52,200 --> 00:12:55,200
You don't get caught up in too much energy,

173
00:12:55,200 --> 00:12:56,200
too much concentration.

174
00:12:56,200 --> 00:13:00,200
You become naturally, you just let things sort themselves out

175
00:13:00,200 --> 00:13:02,200
without forcing anything.

176
00:13:02,200 --> 00:13:07,200
So absolutely these five, when they work together,

177
00:13:07,200 --> 00:13:11,200
and when you use mindfulness to balance the other four,

178
00:13:11,200 --> 00:13:14,200
are definitely the path to the deathless.

179
00:13:14,200 --> 00:13:18,200
And by the deathless is meant this freedom from the rounds

180
00:13:18,200 --> 00:13:21,200
of rebirth, freedom from being born again.

181
00:13:21,200 --> 00:13:24,200
The only way to not die is to not be born.

182
00:13:24,200 --> 00:13:26,200
And so if we're born again and again and again,

183
00:13:26,200 --> 00:13:29,200
we'll always have to die again and again and again.

184
00:13:29,200 --> 00:13:33,200
So deathless just means that state of freedom from that.

185
00:13:36,200 --> 00:13:37,200
That's the first lesson.

186
00:13:37,200 --> 00:13:41,200
Second lesson, of course, again, this lesson criticizing others

187
00:13:41,200 --> 00:13:46,200
can be a dangerous thing to do if you're not clear,

188
00:13:46,200 --> 00:13:49,200
but the other hand, these 30 monks were provided

189
00:13:49,200 --> 00:13:53,200
the perfect opportunity for the Buddha to teach the dhamma.

190
00:13:53,200 --> 00:13:55,200
So there is that.

191
00:13:55,200 --> 00:13:58,200
Something when something's unclear and when something appears

192
00:13:58,200 --> 00:14:01,200
to be wrong, it is important not to keep quiet

193
00:14:01,200 --> 00:14:04,200
and think, oh, well, I don't want to stir up the boat.

194
00:14:04,200 --> 00:14:06,200
It is important to bring it up and ask why.

195
00:14:06,200 --> 00:14:08,200
Why is it like that?

196
00:14:08,200 --> 00:14:12,200
Why does it seem like this monk is faithless?

197
00:14:16,200 --> 00:14:19,200
And then when we get into the actual verse,

198
00:14:19,200 --> 00:14:22,200
we have some important things to say, I think.

199
00:14:22,200 --> 00:14:26,200
Asadho is really an important point.

200
00:14:26,200 --> 00:14:29,200
It's a difference between Buddhism and other religions.

201
00:14:29,200 --> 00:14:33,200
It doesn't mean that you shouldn't believe anything.

202
00:14:33,200 --> 00:14:36,200
It just means that believing is something

203
00:14:36,200 --> 00:14:39,200
is inferior to knowing it for yourself.

204
00:14:39,200 --> 00:14:41,200
And Buddhism will always hold it to be so.

205
00:14:41,200 --> 00:14:45,200
Whereas other religions believe that faith is ultimate.

206
00:14:45,200 --> 00:14:48,200
They believe there are certain things that you can't know

207
00:14:48,200 --> 00:14:51,200
that it is somehow important to have faith in.

208
00:14:51,200 --> 00:14:55,200
And so they set up this system whereby you cultivate faith

209
00:14:55,200 --> 00:14:57,200
and you have to work hard to cultivate it.

210
00:14:57,200 --> 00:15:01,200
Because faith is not something that is stable,

211
00:15:01,200 --> 00:15:04,200
not like wisdom, or not like mindfulness,

212
00:15:04,200 --> 00:15:06,200
not like awareness.

213
00:15:06,200 --> 00:15:10,200
Faith is something, well, not like wisdom actually.

214
00:15:10,200 --> 00:15:14,200
Because wisdom is stable when you come to see things clearly.

215
00:15:14,200 --> 00:15:18,200
You don't have to work to maintain it.

216
00:15:18,200 --> 00:15:25,200
Well, it's something that is stronger.

217
00:15:25,200 --> 00:15:28,200
Whereas with faith, you have to push and push

218
00:15:28,200 --> 00:15:33,200
and you have to always be repressing your doubts.

219
00:15:33,200 --> 00:15:38,200
Or you could say faith that comes with wisdom

220
00:15:38,200 --> 00:15:41,200
when they're both balanced.

221
00:15:41,200 --> 00:15:43,200
The faith that comes with wisdom.

222
00:15:43,200 --> 00:15:46,200
Because in fact, you could also say that

223
00:15:46,200 --> 00:15:49,200
Sarah Buddha has perfect confidence in the truth.

224
00:15:49,200 --> 00:15:52,200
There is perfect confidence in the things that the Buddha

225
00:15:52,200 --> 00:15:53,200
was asking him.

226
00:15:53,200 --> 00:15:54,200
The answer is yes.

227
00:15:54,200 --> 00:15:56,200
And he has perfect confidence in that.

228
00:15:56,200 --> 00:16:01,200
But it's faith that's based on wisdom, based on knowledge.

229
00:16:01,200 --> 00:16:06,200
And so again, how you have to balance them.

230
00:16:06,200 --> 00:16:10,200
But the Buddha was fairly critical about the idea of people

231
00:16:10,200 --> 00:16:12,200
who just take things on faith.

232
00:16:12,200 --> 00:16:15,200
Which it was a big thing in the Buddha's time as well.

233
00:16:15,200 --> 00:16:17,200
The Brahmins were big on faith.

234
00:16:17,200 --> 00:16:21,200
They believed that their rituals had some higher meaning

235
00:16:21,200 --> 00:16:24,200
to them without any rational explanation.

236
00:16:24,200 --> 00:16:27,200
Or they believed in this God or that God.

237
00:16:27,200 --> 00:16:30,200
Without having seen or heard, they believed in Brahma.

238
00:16:30,200 --> 00:16:33,200
So they created all these beliefs.

239
00:16:33,200 --> 00:16:35,200
And then they just had faith in them.

240
00:16:35,200 --> 00:16:38,200
They didn't have any knowledge or understanding

241
00:16:38,200 --> 00:16:41,200
about their truth or falsehood.

242
00:16:41,200 --> 00:16:45,200
So in meditation, this should be our emphasis.

243
00:16:45,200 --> 00:16:49,200
Not unbelieving that some of this meditation is going to help us.

244
00:16:49,200 --> 00:16:52,200
But in using the meditation to learn about ourselves.

245
00:16:52,200 --> 00:16:53,200
So that we don't need faith.

246
00:16:53,200 --> 00:16:54,200
We don't need to believe anyone else.

247
00:16:54,200 --> 00:16:56,200
That's really what we're doing.

248
00:16:56,200 --> 00:16:58,200
You want to tell you all about the mind,

249
00:16:58,200 --> 00:17:01,200
how the mind works, and what reality is like.

250
00:17:01,200 --> 00:17:03,200
I can give you lecture after lecture.

251
00:17:03,200 --> 00:17:08,200
But that's always going to be inferior

252
00:17:08,200 --> 00:17:12,200
to you actually opening up the box and looking inside.

253
00:17:12,200 --> 00:17:14,200
Which is what you do in meditation.

254
00:17:14,200 --> 00:17:16,200
You open up a new look.

255
00:17:16,200 --> 00:17:19,200
And you see how our things going on.

256
00:17:19,200 --> 00:17:22,200
The technique that we give you is just to provide you

257
00:17:22,200 --> 00:17:26,200
with a framework like a tool in which to look.

258
00:17:26,200 --> 00:17:29,200
Like when you peer into a microscope and give you this

259
00:17:29,200 --> 00:17:31,200
and you look in the microscope and whatever is there,

260
00:17:31,200 --> 00:17:32,200
you'll see.

261
00:17:32,200 --> 00:17:35,200
So watching your stomach rising and falling.

262
00:17:35,200 --> 00:17:38,200
As you watch it and you use the mantra

263
00:17:38,200 --> 00:17:40,200
to keep yourself objective,

264
00:17:40,200 --> 00:17:42,200
you'll start to see things.

265
00:17:42,200 --> 00:17:44,200
You'll be in an objective frame of mind

266
00:17:44,200 --> 00:17:46,200
like you're focusing the microscope.

267
00:17:46,200 --> 00:17:48,200
And once you get it well focused,

268
00:17:48,200 --> 00:17:50,200
you'll be able to see things as they are.

269
00:17:50,200 --> 00:17:52,200
And then you won't need to believe me.

270
00:17:52,200 --> 00:17:58,200
I'll cut the new is about what this is all about.

271
00:17:58,200 --> 00:18:00,200
The height, the pinnacle,

272
00:18:00,200 --> 00:18:02,200
is to see that which is not made.

273
00:18:02,200 --> 00:18:04,200
Or to know that which is not made.

274
00:18:04,200 --> 00:18:07,200
And point being that as you look in this microscope

275
00:18:07,200 --> 00:18:09,200
through the meditation,

276
00:18:09,200 --> 00:18:12,200
what you're going to see is that everything that arises ceases.

277
00:18:12,200 --> 00:18:16,200
And you come to the realization or the understanding

278
00:18:16,200 --> 00:18:18,200
that there's nothing in the world

279
00:18:18,200 --> 00:18:21,200
that is truly and intrinsically of value.

280
00:18:21,200 --> 00:18:23,200
There's no experience that you can have

281
00:18:23,200 --> 00:18:25,200
no goal you can attain in the world

282
00:18:25,200 --> 00:18:28,200
because everything is made

283
00:18:28,200 --> 00:18:30,200
by causes and conditions that arises

284
00:18:30,200 --> 00:18:31,200
and it ceases.

285
00:18:31,200 --> 00:18:34,200
And it's a part of a web of causation.

286
00:18:34,200 --> 00:18:37,200
Now Nibbana, there is that

287
00:18:37,200 --> 00:18:40,200
which is outside of this cause of causation.

288
00:18:40,200 --> 00:18:43,200
It doesn't arise and therefore it doesn't cease.

289
00:18:43,200 --> 00:18:47,200
And this is considered to be the height

290
00:18:47,200 --> 00:18:49,200
when someone realizes this.

291
00:18:49,200 --> 00:18:53,200
It's unique. They have this experience of some unique state

292
00:18:53,200 --> 00:18:57,200
or unique entity or unique reality

293
00:18:57,200 --> 00:18:59,200
that is unmade.

294
00:18:59,200 --> 00:19:02,200
That is unformed, unborn.

295
00:19:02,200 --> 00:19:04,200
So that is the height.

296
00:19:04,200 --> 00:19:07,200
It's something that changes the way we look at reality

297
00:19:07,200 --> 00:19:10,200
because in contrast everything else becomes meaningless.

298
00:19:10,200 --> 00:19:14,200
And when one who has realized this or seen this,

299
00:19:14,200 --> 00:19:18,200
they'll never be enticed by or intoxicated

300
00:19:18,200 --> 00:19:21,200
by those things that arise and cease

301
00:19:21,200 --> 00:19:26,200
because they're clearly categorically inferior.

302
00:19:28,200 --> 00:19:31,200
Sun-de-chay-da means cutting the link.

303
00:19:31,200 --> 00:19:33,200
So another great thing is one doesn't have to be born

304
00:19:33,200 --> 00:19:35,200
or sick and dying.

305
00:19:35,200 --> 00:19:37,200
And when people sometimes, most humans

306
00:19:37,200 --> 00:19:39,200
are fairly much attached to life

307
00:19:39,200 --> 00:19:41,200
and you ask them, would you like to come back

308
00:19:41,200 --> 00:19:43,200
and do this all again and again?

309
00:19:43,200 --> 00:19:45,200
Many would I think say, yes, please,

310
00:19:45,200 --> 00:19:48,200
it'll give me an opportunity to learn more

311
00:19:48,200 --> 00:19:51,200
and to experience things I didn't experience in this life

312
00:19:51,200 --> 00:19:54,200
would be great to come back.

313
00:19:54,200 --> 00:19:56,200
Forgetting as we do,

314
00:19:56,200 --> 00:20:01,200
this is we have this confirmation bias or positive bias.

315
00:20:01,200 --> 00:20:05,200
So we're very quick to forget about all the problems in life.

316
00:20:05,200 --> 00:20:08,200
And moreover, when you think it through,

317
00:20:08,200 --> 00:20:11,200
what you're talking about is an infinite recursion.

318
00:20:11,200 --> 00:20:14,200
You're talking about this infinite cycle.

319
00:20:14,200 --> 00:20:18,200
And so it's that that is fairly problematic.

320
00:20:18,200 --> 00:20:20,200
Because it really becomes meaningless after a while

321
00:20:20,200 --> 00:20:24,200
and it ends up being not nearly as much trouble as it's worth.

322
00:20:24,200 --> 00:20:27,200
Being a human being is a lot of trouble.

323
00:20:27,200 --> 00:20:29,200
And clearly, in all the time,

324
00:20:29,200 --> 00:20:31,200
we've spent in the rounds of rebirth.

325
00:20:31,200 --> 00:20:33,200
We keep coming back to things like this.

326
00:20:33,200 --> 00:20:36,200
We go up and down and so sometimes it's great

327
00:20:36,200 --> 00:20:37,200
and sometimes it's horrible.

328
00:20:37,200 --> 00:20:42,200
Sometimes it's mind-numbingly painful and cruel

329
00:20:42,200 --> 00:20:44,200
and unpleasant.

330
00:20:44,200 --> 00:20:46,200
Sometimes it's pleasant,

331
00:20:46,200 --> 00:20:47,200
credibly,

332
00:20:47,200 --> 00:20:51,200
stupefyingly pleasant.

333
00:20:51,200 --> 00:20:55,200
But in the end, it's just a round of rebirth again and again.

334
00:20:55,200 --> 00:20:58,200
And in the end, it's such a completely unsatisfying.

335
00:20:58,200 --> 00:21:00,200
It's not something that you intellectually

336
00:21:00,200 --> 00:21:02,200
or let go out of faith.

337
00:21:02,200 --> 00:21:04,200
You don't have to believe me.

338
00:21:04,200 --> 00:21:06,200
The point is that when you look closely,

339
00:21:06,200 --> 00:21:08,200
you naturally incline away from it.

340
00:21:08,200 --> 00:21:10,200
So through the meditation,

341
00:21:10,200 --> 00:21:12,200
it's less inclined to create,

342
00:21:12,200 --> 00:21:15,200
less inclined to chase after this pleasure

343
00:21:15,200 --> 00:21:17,200
because it's a lot of work.

344
00:21:17,200 --> 00:21:20,200
And then it disappears and you're left with nothing

345
00:21:20,200 --> 00:21:23,200
except your attachment to it,

346
00:21:23,200 --> 00:21:26,200
which then leads you to lots of suffering.

347
00:21:26,200 --> 00:21:28,200
So cutting the stream,

348
00:21:28,200 --> 00:21:30,200
this is the, cutting the chain.

349
00:21:30,200 --> 00:21:33,200
This is the part of the goal.

350
00:21:33,200 --> 00:21:35,200
And it's just, again, with the theme,

351
00:21:35,200 --> 00:21:37,200
it just goes back to knowledge,

352
00:21:37,200 --> 00:21:38,200
not belief.

353
00:21:38,200 --> 00:21:39,200
It's not something to be afraid of.

354
00:21:39,200 --> 00:21:41,200
It just falls into it.

355
00:21:41,200 --> 00:21:43,200
Most people, I think, are afraid of the idea

356
00:21:43,200 --> 00:21:45,200
of new band of, or some kind of,

357
00:21:47,200 --> 00:21:53,200
unknowable, mysterious state of so-called freedom.

358
00:21:53,200 --> 00:21:55,200
But when you've experienced it,

359
00:21:55,200 --> 00:21:59,200
there's no question in the mind which is preferable.

360
00:21:59,200 --> 00:22:01,200
Let's go immediately

361
00:22:01,200 --> 00:22:04,200
of those things that are completely unbeneficial

362
00:22:04,200 --> 00:22:06,200
and unsatisfying.

363
00:22:06,200 --> 00:22:10,200
Atawak, I so means

364
00:22:10,200 --> 00:22:12,200
they've given up the opportunity for becoming,

365
00:22:12,200 --> 00:22:16,200
and this has to do with giving up defilements.

366
00:22:16,200 --> 00:22:19,200
It means giving up all kinds of unwholesomeness,

367
00:22:19,200 --> 00:22:21,200
but also any kind of wholesomeness.

368
00:22:21,200 --> 00:22:24,200
So giving up the desire to even do good in the world,

369
00:22:24,200 --> 00:22:27,200
to bring the world to good, to save the world,

370
00:22:27,200 --> 00:22:29,200
to change the world.

371
00:22:29,200 --> 00:22:31,200
All of these things was one event that gives up,

372
00:22:31,200 --> 00:22:33,200
and that's considered preferable

373
00:22:33,200 --> 00:22:35,200
because in the end you can't change the world.

374
00:22:35,200 --> 00:22:39,200
And the end everything that you've done in the world

375
00:22:39,200 --> 00:22:41,200
will cease and fade away.

376
00:22:41,200 --> 00:22:44,200
Over time, in the face of eternity,

377
00:22:44,200 --> 00:22:45,200
it's really nothing.

378
00:22:45,200 --> 00:22:48,200
You want to save the planet, save the environment.

379
00:22:48,200 --> 00:22:50,200
To some extent, that's a good deed.

380
00:22:50,200 --> 00:22:53,200
It's kind of you to think about other people

381
00:22:53,200 --> 00:22:56,200
and to think about society and want everyone to be happy.

382
00:22:56,200 --> 00:22:57,200
So it's good.

383
00:22:57,200 --> 00:22:59,200
And for most of us, it's a great thing.

384
00:22:59,200 --> 00:23:01,200
For someone who becomes enlightened,

385
00:23:01,200 --> 00:23:04,200
they give up any idea of fixing things.

386
00:23:04,200 --> 00:23:07,200
Because in the end, it's inherently broken.

387
00:23:07,200 --> 00:23:09,200
Of course, along the way, they do a lot of good.

388
00:23:09,200 --> 00:23:12,200
And by becoming enlightened, they also do a lot of good.

389
00:23:12,200 --> 00:23:15,200
And end up making the world a better place for everyone

390
00:23:15,200 --> 00:23:17,200
because they give up their own greed,

391
00:23:17,200 --> 00:23:20,200
their own attachments, their own conflicts and anger

392
00:23:20,200 --> 00:23:24,200
and their own arrogance and conceit and so on.

393
00:23:24,200 --> 00:23:28,200
So there's really nothing to criticize about it,

394
00:23:28,200 --> 00:23:30,200
but they certainly give up everything.

395
00:23:30,200 --> 00:23:38,200
They have no opportunity for this future becoming no more ambition.

396
00:23:38,200 --> 00:23:40,200
And what does so their hopeless?

397
00:23:40,200 --> 00:23:43,200
So our practice is to give up our hopes.

398
00:23:43,200 --> 00:23:46,200
Because if you hope it means you still have a goal.

399
00:23:46,200 --> 00:23:49,200
And when someone becomes free from suffering

400
00:23:49,200 --> 00:23:51,200
when they attain the ultimate goal,

401
00:23:51,200 --> 00:23:56,200
then there's no other reason for them to hope or wish.

402
00:23:56,200 --> 00:23:59,200
They say that hopes and wishes are actually the cause of suffering

403
00:23:59,200 --> 00:24:03,200
because even if you want, get what you wish for.

404
00:24:03,200 --> 00:24:08,200
You'll still at the same time be giving rise to attachment to it.

405
00:24:08,200 --> 00:24:11,200
And then you build up and build up the attachment

406
00:24:11,200 --> 00:24:18,200
until eventually that goal is attained and lost over that over time.

407
00:24:18,200 --> 00:24:21,200
It would point all your left with is the attachment,

408
00:24:21,200 --> 00:24:25,200
which again leads to suffering.

409
00:24:25,200 --> 00:24:29,200
So this is an explanation of that verse,

410
00:24:29,200 --> 00:24:36,200
sort of a outline of the sorts of qualities of an enlightened being.

411
00:24:36,200 --> 00:24:39,200
Our own practice, we emulate some of these

412
00:24:39,200 --> 00:24:43,200
and we cultivate some of these and we keep some of them in mind philosophically

413
00:24:43,200 --> 00:24:47,200
to help us know how to direct our minds in the meditation practice.

414
00:24:47,200 --> 00:24:50,200
So another useful if a bit,

415
00:24:50,200 --> 00:24:52,200
I would say tongue in cheek

416
00:24:52,200 --> 00:24:56,200
and I don't really want to implicate and implicate the Buddha in any way,

417
00:24:56,200 --> 00:24:59,200
but it's definitely a clever verse.

418
00:24:59,200 --> 00:25:07,200
And perhaps it seems to shock and to throw off guard the listener.

419
00:25:07,200 --> 00:25:10,200
So that's the verse for tonight.

420
00:25:10,200 --> 00:25:12,200
Thank you all for tuning in wishing you all the best.

421
00:25:12,200 --> 00:25:27,200
Be well.

