1
00:00:00,000 --> 00:00:07,200
short name. How does one help another not to be terrified of the thought of the ultimate

2
00:00:07,200 --> 00:00:21,480
uselessness of all things? It's terrifying. Well, I mean you can talk about how actually

3
00:00:21,480 --> 00:00:29,640
they are quite useful. Everything has a use. You know, like money has a use. Alcohol

4
00:00:29,640 --> 00:00:40,520
has a use. Everything has its use. The question is whether those uses or those purposes

5
00:00:40,520 --> 00:00:53,320
are beneficial or detrimental. So I did a video on this and it might take on it that I think

6
00:00:53,320 --> 00:00:59,640
is whether it's perfectly, perfectly honest or not. It's quite useful. I mean, I just mean

7
00:00:59,640 --> 00:01:06,160
some people might disagree, but the point being that when we say that the world has no

8
00:01:06,160 --> 00:01:13,040
purpose or life, life, let's say life has no purpose, we're not. It's not something negative.

9
00:01:13,040 --> 00:01:18,920
It's actually something liberating because the Christians, for example, say that the purpose

10
00:01:18,920 --> 00:01:28,080
of life is belief in Jesus Christ, is to gain faith in Jesus Christ. And if that's the

11
00:01:28,080 --> 00:01:33,840
case, then we're all really in big trouble, right? And we've got very little choice.

12
00:01:33,840 --> 00:01:37,800
There's not much we can do with our lives except, you know, our lives become a question

13
00:01:37,800 --> 00:01:43,640
of whether we're worshiping Jesus Christ or whether we're we have belief in Jesus Christ

14
00:01:43,640 --> 00:01:54,600
or not, nothing else matters. You have no freedom, no, if, you know, if the what Muslims

15
00:01:54,600 --> 00:02:00,600
are right, and then then belief in Allah is the only thing that matters. Trying to think

16
00:02:00,600 --> 00:02:08,520
of other crazy religions, most of the Eastern religions are pretty non crazy, really. Hinduism,

17
00:02:08,520 --> 00:02:15,400
Hinduism gets you stuck in whatever I don't know, not really. But as soon as you have a

18
00:02:15,400 --> 00:02:20,200
purpose, the point is you have a purpose, you get stuck. If the purpose of life is to make

19
00:02:20,200 --> 00:02:26,280
lots of money, then, you know, that's all you've got is whether you make money or not,

20
00:02:26,280 --> 00:02:31,080
saying that there's no purpose in life is completely liberating. You can do what you want.

21
00:02:31,080 --> 00:02:41,880
If you want to become a mass murderer, go for it. There's nothing intrinsically stopping

22
00:02:41,880 --> 00:02:54,760
from doing it. The only problem with such a thing is that it goes against your own intentions.

23
00:02:54,760 --> 00:02:59,880
You do it thinking it will make you happy or it will be a benefit to you. And it actually

24
00:02:59,880 --> 00:03:06,520
is to your detriment. The fact that these things contradict each other, we're not worried about

25
00:03:06,520 --> 00:03:18,440
purpose because we're trying to find benefit or clarity or simplicity and so on. So I think explaining

26
00:03:18,440 --> 00:03:24,200
it in that way that it actually brings freedom and allows you to worry less about what is right

27
00:03:24,200 --> 00:03:32,040
and what is wrong or what is the way and actually focus on what brings peace, happiness,

28
00:03:32,040 --> 00:03:41,320
and freedom from suffering and what doesn't. What actually makes sense is maybe the best thing.

29
00:03:42,440 --> 00:03:47,880
What makes sense supersedes the idea of having a predefined purpose,

30
00:03:47,880 --> 00:03:56,040
which would of course be completely arbitrary. There's no reason for us to believe that faith

31
00:03:56,040 --> 00:04:04,680
in Jesus Christ is any better than faith in Allah. Nobody looks like they have anything to say

32
00:04:04,680 --> 00:04:15,160
on this. What would you do if someone was afraid of, you know, maybe we can be more general

33
00:04:15,160 --> 00:04:23,160
being afraid of letting go, one meditator right now who is much better now. She said to me how

34
00:04:23,160 --> 00:04:30,600
much better she feels and then I finally looked at her with this question in my mind and it was

35
00:04:30,600 --> 00:04:35,480
amazing to think. What did she look like when she first came what did she look like now and you

36
00:04:35,480 --> 00:04:44,360
can see that she's a different person. She's radiant, she's much more alive, much less a character

37
00:04:44,360 --> 00:04:53,400
and much more a human being. But in the beginning she was very afraid of letting go, afraid of or

38
00:04:53,400 --> 00:04:59,640
she had these questions about how can I convince myself to let go.

39
00:04:59,640 --> 00:05:16,280
One classic answer that I would always give is that you never let go of the things that you

40
00:05:16,280 --> 00:05:26,760
don't want to let go of. So maybe that goes along with purpose as well. If you believe that

41
00:05:26,760 --> 00:05:33,160
x, y, or z is the purpose of life, then well don't believe us that it's not. But certainly

42
00:05:34,200 --> 00:05:39,160
don't just take your own blind faith in it. If you want, if you believe that there's a purpose,

43
00:05:39,800 --> 00:05:45,160
look at that purpose and try to understand it. If it's just a fear, if the person gets the

44
00:05:45,160 --> 00:05:49,080
fact that there is no purpose to life, but they're just afraid of it, well then the best thing

45
00:05:49,080 --> 00:05:54,120
is to look at the fear. Don't worry about whether there is or isn't a purpose. Look at this fear

46
00:05:54,120 --> 00:05:58,360
that you have and try to understand what's there as opposed to what's not there.

47
00:06:00,680 --> 00:06:06,680
I just think purpose has no place in an ultimate reality and it's a long and short of it.

48
00:06:06,680 --> 00:06:22,600
Nobody else? Stop there.

