1
00:00:00,000 --> 00:00:25,560
Good evening, broadcasting live, March 3rd, 2016.

2
00:00:25,560 --> 00:00:47,560
This is quote, this quote is, from Deagonica, and speech.

3
00:00:47,560 --> 00:01:09,560
It just takes part of the section on morality from the Brahman-Jalasutra.

4
00:01:09,560 --> 00:01:19,560
Brahman-Jalasutra is in certain versions of the vidhikas, the first text.

5
00:01:19,560 --> 00:01:23,560
So of all the teachings of the Buddha, it's what comes first.

6
00:01:23,560 --> 00:01:31,560
It's really a landmark discourse, so much in it.

7
00:01:31,560 --> 00:01:43,560
There's so many topics, it's not one single teaching.

8
00:01:43,560 --> 00:01:53,560
In Burma, they have a version of the tepithika that picks it apart word by word, and tries

9
00:01:53,560 --> 00:01:56,560
to explain almost every word of the tepithika.

10
00:01:56,560 --> 00:02:02,560
Well, maybe not every word, but it does start that way.

11
00:02:02,560 --> 00:02:10,560
So the Buddha starts A1, may sit down, thus has been heard by me.

12
00:02:10,560 --> 00:02:18,560
A1, and then it explains what A1 means, and then it means, what does may mean?

13
00:02:18,560 --> 00:02:21,560
And what does sit down mean?

14
00:02:21,560 --> 00:02:28,560
And so in Thailand, they started translating from the Burmese, actually.

15
00:02:28,560 --> 00:02:34,560
And of course, they started with the Brahman-Jalasutra.

16
00:02:34,560 --> 00:02:46,560
So all in all, it's an important sutta.

17
00:02:46,560 --> 00:02:53,560
So not to go into the sutta really, but to go into the quote and section regarding the quote

18
00:02:53,560 --> 00:02:54,560
is the section of morality.

19
00:02:54,560 --> 00:02:58,560
But why he picked this quote is pretty clear.

20
00:02:58,560 --> 00:03:04,560
Because mostly when we talk about morality, we deal with the negative aspects.

21
00:03:04,560 --> 00:03:08,560
Don't kill because killing is bad.

22
00:03:08,560 --> 00:03:09,560
Killing is bad.

23
00:03:09,560 --> 00:03:13,560
Don't steal stealing is bad.

24
00:03:13,560 --> 00:03:16,560
Don't cheat, cheating is evil.

25
00:03:16,560 --> 00:03:22,560
Lying, lying is evil.

26
00:03:22,560 --> 00:03:28,560
Malicious speech is evil.

27
00:03:28,560 --> 00:03:31,560
Harsh, speech.

28
00:03:31,560 --> 00:03:35,560
What are the words they use?

29
00:03:35,560 --> 00:03:45,560
Malicious, malicious, harsh, and useless, frivolous.

30
00:03:45,560 --> 00:03:47,560
Because these are bad.

31
00:03:47,560 --> 00:03:51,560
But here the Buddha does something different.

32
00:03:51,560 --> 00:03:58,560
He says, it gives up life and giving up life.

33
00:03:58,560 --> 00:04:05,560
So I can find the father here.

34
00:04:05,560 --> 00:04:07,560
Bah-nah-ti-pa-tang-baha-ya.

35
00:04:07,560 --> 00:04:11,560
I once actually tried to remember, I had this memorized at one point.

36
00:04:11,560 --> 00:04:17,560
Large parts was the first bit of theme of my jalasutra.

37
00:04:17,560 --> 00:04:22,560
Bah-nah-ti-pa-tang-baha-ya-pa-nah-ti-pa-tah-tiri-to.

38
00:04:22,560 --> 00:04:43,560
So it talks about the positive aspects.

39
00:04:43,560 --> 00:04:50,560
Once you've given up killing, when you abandon killing,

40
00:04:50,560 --> 00:04:57,560
you refrain from taking the life of others.

41
00:04:57,560 --> 00:05:03,560
You need to settle, having put down weapons,

42
00:05:03,560 --> 00:05:10,560
lungee, scrupulous, diap and no diap and no compassionate.

43
00:05:10,560 --> 00:05:19,560
Psa-yepad-dah-tya, linole trip.

44
00:05:19,560 --> 00:05:29,560
LamaIKBI Rmage-dah-tang, demon being.

45
00:05:29,560 --> 00:05:34,520
It means trembling.

46
00:05:34,520 --> 00:05:37,560
Thats how it translates.

47
00:05:37,560 --> 00:05:39,560
It literally means tremble.

48
00:05:39,560 --> 00:05:51,160
turned by, be moved by, really, as I knew it, moved by out of desire for the benefit

49
00:05:51,160 --> 00:06:08,040
of all beings, moved by desire for the, moved out of, in regards to our, in the interest

50
00:06:08,040 --> 00:06:18,840
of benefit of all beings and all living beings, trembling for the welfare of all that

51
00:06:18,840 --> 00:06:22,760
beings in terms of that.

52
00:06:22,760 --> 00:06:30,800
So this is a positive, positive state, out of compassion.

53
00:06:30,800 --> 00:06:37,960
One, one, we don't stop killing because we're afraid of, we don't stop killing simply

54
00:06:37,960 --> 00:06:44,040
because we're afraid of the evil consequences of killing.

55
00:06:44,040 --> 00:06:55,240
Because we're afraid of going against Buddha's words.

56
00:06:55,240 --> 00:07:02,800
When you practice mindfulness, when you start to see clearly, you see yourself in others,

57
00:07:02,800 --> 00:07:11,120
in your, in others, in yourself, as we have learned with the demographics of me, all

58
00:07:11,120 --> 00:07:21,120
tremble at the rod, comparing oneself to others, one should of kill or harm another.

59
00:07:21,120 --> 00:07:33,240
And so when it becomes naturally compassionate and at the thought of hurting others, when

60
00:07:33,240 --> 00:07:46,400
the shadow and shy is away naturally, adinada, nana, nana, nana, nana, nana, nana, nana,

61
00:07:46,400 --> 00:07:52,080
activate at those and then no go to move.

62
00:07:52,080 --> 00:07:58,920
Not taking wood is not given, having abandoned wood is, having abandoned the taking of

63
00:07:58,920 --> 00:08:15,320
wood is not given, nana, taking only wood is given, nana, particle, king, waiting with

64
00:08:15,320 --> 00:08:30,000
us is given, at the king of wood, adinada, nana, nana, we have adinada, adinada, adinada,

65
00:08:30,000 --> 00:08:51,880
without stealing and abandoning, abandoning, unchastity, that's interesting, I don't think

66
00:08:51,880 --> 00:09:04,200
it's quite what it means, it's a chamber of wood, nana, adinada, without stealing, so

67
00:09:04,200 --> 00:09:11,520
chamber of wood, nana, nana, nothing looking to that, they used to know what that means,

68
00:09:11,520 --> 00:09:21,800
I don't think it has to do its chastity, but there's something there, wait, I'm looking

69
00:09:21,800 --> 00:09:30,200
for the wood, living purely, waiting with it is given without stealing, not taking the

70
00:09:30,200 --> 00:09:41,280
wood, nana, and dwells, I see, dwells pure, there's purely accepting what it's given,

71
00:09:41,280 --> 00:09:55,160
there's purely, right, so it's not just about not stealing, it's about living with what

72
00:09:55,160 --> 00:10:03,400
it's given, it's about being content with what it's given, being content with what

73
00:10:03,400 --> 00:10:12,120
comes to you, and this is this includes work, this includes how anything comes to you,

74
00:10:12,120 --> 00:10:21,040
it's about contentment rather than means or manner, it doesn't mean you have to wait

75
00:10:21,040 --> 00:10:30,760
for handouts, if you work in a job when people pay you money, being content with that,

76
00:10:30,760 --> 00:10:37,680
being able to make do, it's interesting, we're so afraid, we're so worried about how am

77
00:10:37,680 --> 00:10:44,800
I going to make ends meet, and often we have legitimate concern, sometimes it is difficult

78
00:10:44,800 --> 00:10:54,960
to make ends meet, but often our fear prevents us from coming to trust, trust in the power

79
00:10:54,960 --> 00:11:10,560
of goodness, trust in the power of our own goodness, we won't fall, if anything becoming

80
00:11:10,560 --> 00:11:15,440
a monkey, if it's shown me anything, well, one of the greatest things it's shown me is

81
00:11:15,440 --> 00:11:24,840
that, well, when you do good things, people support you, it's supported people surround

82
00:11:24,840 --> 00:11:32,480
you, they're being sick the past week, they certainly show me that, what would I have done

83
00:11:32,480 --> 00:11:41,160
if I hadn't, people who were supporting you, remember one time in Thailand I got very

84
00:11:41,160 --> 00:11:48,000
sick, and when I was staying in what tambourine down, just any of you remember that, it

85
00:11:48,000 --> 00:12:03,920
was, many years ago now, and there were meditators there, it was back when Polanyani was

86
00:12:03,920 --> 00:12:15,320
there before she was a bikini, and so I think they were her and another meditator over there,

87
00:12:15,320 --> 00:12:25,120
and I had been going 3.5 kilometers on arms round, and getting enough food, see, the old

88
00:12:25,120 --> 00:12:29,280
monk there told us, you go on arms round, you won't get enough food to eat, but when

89
00:12:29,280 --> 00:12:39,080
I went on arms round, walk 3.5 kilometers there, and then 3.5 kilometers back, I got enough

90
00:12:39,080 --> 00:12:44,400
food for me and the meditators, so we just fed the meditators with arms food.

91
00:12:44,400 --> 00:12:54,200
And so the morning I woke up and I felt dizzy and probably feverish, but I knew if I didn't

92
00:12:54,200 --> 00:13:00,440
go on arms round the meditators, what it needs, I actually walked 3.5 kilometers, and I

93
00:13:00,440 --> 00:13:10,000
could barely have 3.5 kilometers, you think about how long that is, with a fever, remember

94
00:13:10,000 --> 00:13:16,000
that is one of the longest walks in my life, and when I got the village I had to sit

95
00:13:16,000 --> 00:13:24,600
down and asked someone, one, to stop people for water, okay, quite remember what happened,

96
00:13:24,600 --> 00:13:32,200
but somehow I think I think I got someone in the village to bring us food or something like

97
00:13:32,200 --> 00:13:33,200
that.

98
00:13:33,200 --> 00:14:00,600
Sometimes you have to go out sometimes, but if you do good, we trust in the goodness, we

99
00:14:00,600 --> 00:14:09,600
trust in the power of karma, and it is real, that the universe is only mine to me, it

100
00:14:09,600 --> 00:14:16,280
only comes from the mind, some bad things are happening to us, now we just let them happen,

101
00:14:16,280 --> 00:14:25,300
we don't care about the bad things, work towards the good things, the good things,

102
00:14:25,300 --> 00:14:37,000
we don't want to make good things, and good will come, this is what we stand, and so

103
00:14:37,000 --> 00:14:44,400
these, when I thought when I woke up that morning, these people are doing this good thing,

104
00:14:44,400 --> 00:14:49,400
it was enough of a motivation that these people were doing a meditation course, for me

105
00:14:49,400 --> 00:14:55,020
to walk 3.5 kilometers, that's how good was there indeed of doing the meditation

106
00:14:55,020 --> 00:15:03,020
course, that's how powerful meditation is, it compels six months to walk, three and a half kilometers

107
00:15:03,020 --> 00:15:26,300
that's the second one, the third one of Brahmacharya, Arachari Vittu, Vittu Nagaamadhamma,

108
00:15:26,300 --> 00:15:31,960
Arachari Vittu Nagaamadhamma, Arachari Vittu Nagaamadhamma, Arachari Vittu Nagaamadhamma,

109
00:15:31,960 --> 00:15:52,100
Arachari Vittu Nagaamadhamma, Arachari Vittu Nagaamadhamma,

110
00:15:52,100 --> 00:16:05,320
Arachari Vittu Nagaamadhamma, Vittu Nagaamadhamma, Vittu Nagaamadhamma, regards to sex, Vittu Nagaamma

111
00:16:05,320 --> 00:16:19,760
of sex, abandoning it, abstaining from it, Vittu Nagaamma, and here we get into the actual

112
00:16:19,760 --> 00:16:24,600
code that talks about the types of speech, which is the interest of this, well, it's

113
00:16:24,600 --> 00:16:29,400
interesting in why he quoted it because he talks about positive aspects.

114
00:16:29,400 --> 00:16:37,020
When you don't lie, you're a such a law indeed, such a son who would hate, but Jaiiko,

115
00:16:37,020 --> 00:16:49,300
Abhisangwa, Dakolukas, dwells, refraining from small speech, a truth speaker, one to be

116
00:16:49,300 --> 00:16:58,180
relied on trustworthy, dependable, not the seaver of the world, someone who doesn't lie

117
00:16:58,180 --> 00:17:06,500
that's a very powerful thing, it gets to the point where it's shocking to hear someone

118
00:17:06,500 --> 00:17:11,660
a lot and to find out that someone lied to you, if you surround yourself with good people

119
00:17:11,660 --> 00:17:22,780
enough, that's why it's shocking that when someone does actually lied to you because

120
00:17:22,780 --> 00:17:27,540
there's such a difference with people who tell the truth and telling the truth and how wonderful

121
00:17:27,540 --> 00:17:47,820
it is, so much more peaceful, much more powerful and harmonious and constructive, with

122
00:17:47,820 --> 00:17:58,900
Jaiiko, Abhisangwa, Dakolukas, not one who, the person who doesn't deceive the world,

123
00:17:58,900 --> 00:18:18,860
and Pizunangwa, Dankbaha, Yapisuna, Yawata, Yapati, Ritosa, Manogo, Tumu, Ito, Subran, Amutra, Akata,

124
00:18:18,860 --> 00:18:20,940
Imi, Sambihida, having heard something over there, having heard something here, one doesn't

125
00:18:20,940 --> 00:18:29,940
spread it over there in order to break these ones up, having heard something over there

126
00:18:29,940 --> 00:18:38,780
one doesn't spread it over here in order to break them up with those people, so that's

127
00:18:38,780 --> 00:18:44,220
what you don't do, but what does that do when you stop that, so the person doesn't

128
00:18:44,220 --> 00:18:58,300
make you, Iitimbina, Nangwa, Santata, Sahita, Nangwa, Anupanda, those who are broken up, one brings

129
00:18:58,300 --> 00:19:08,740
them together, those who are together, who doesn't break them up, Samangara, Mo,

130
00:19:08,740 --> 00:19:18,900
Samangara, Tumu, one who dwells in harmony, one who delights in harmony, one who

131
00:19:18,900 --> 00:19:27,660
rejoices in harmony, so they're all of the ways of saying the same thing, rejoices,

132
00:19:27,660 --> 00:19:37,300
rejoices, rejoices in harmony, speaks words that create harmony, Samangara,

133
00:19:37,300 --> 00:19:47,340
Nangwa, Jangbah, Sita, Arusanwa, Jangbah, Iitimbah, Arusanwa, Jangbah, Iitimbah,

134
00:19:47,340 --> 00:20:15,920
Sandamu, Nangwa,Kinda, Tumu, intensified, that

135
00:20:15,920 --> 00:20:29,880
Yasawa, Jawa, Jawa, whatever one Jawa ever speaks, nailas, nailas, nailas, nailas,

136
00:20:29,880 --> 00:20:39,080
there's to no others, nailas, nailas, nailas, nailas, nailas, nailas, nailas, nailas,

137
00:20:39,080 --> 00:20:48,840
nailas, khanasukha, khanas, the ears, hookas, happiness, or happy.

138
00:20:48,840 --> 00:20:54,080
Whatever speech makes the ear happy,

139
00:20:54,080 --> 00:21:00,160
Baim and the causes people to find your dear,

140
00:21:00,160 --> 00:21:04,840
endearing, hada-yangama, hada-yas, the heart,

141
00:21:04,840 --> 00:21:09,080
nailas, gamas, gos, or gone.

142
00:21:09,080 --> 00:21:16,080
Speech that goes to the heart, warms the heart.

143
00:21:16,080 --> 00:21:19,520
Bury, warms problems, everything,

144
00:21:19,520 --> 00:21:25,480
urbain, nailas, nailas, bahu-jannah-kantah,

145
00:21:25,480 --> 00:21:27,920
which is pleasing to many people,

146
00:21:27,920 --> 00:21:33,600
bahu-jannah-manah, which warms the hearts many years.

147
00:21:33,600 --> 00:21:41,440
Makes the mind happy, tatah-ru, bhigwa, jambasita,

148
00:21:41,440 --> 00:21:44,080
one speaks such words.

149
00:21:44,080 --> 00:21:46,440
You don't say things to hurt others,

150
00:21:46,440 --> 00:21:49,760
you don't say things to cause pain, suffering,

151
00:21:49,760 --> 00:21:55,960
or to make people upset, or to make them angry.

152
00:21:55,960 --> 00:21:58,160
You think you're careful with what you say,

153
00:21:58,160 --> 00:22:06,600
and you say things that bring peace to their mind.

154
00:22:06,600 --> 00:22:13,440
And finally, some papa-la-bung,

155
00:22:13,440 --> 00:22:15,920
it is useless speech.

156
00:22:15,920 --> 00:22:18,640
Even the word sounds like useless speech.

157
00:22:18,640 --> 00:22:21,200
Some papa-la-bha.

158
00:22:21,200 --> 00:22:23,440
It's like blah, blah, blah.

159
00:22:23,440 --> 00:22:26,920
It's really what it means.

160
00:22:26,920 --> 00:22:36,560
Some papa-la-bha, some papa-la-bha.

161
00:22:36,560 --> 00:22:40,080
Galawadhi speaks at the right time,

162
00:22:40,080 --> 00:22:43,680
Bhutawadhi speaks with its true.

163
00:22:43,680 --> 00:22:46,920
Atawadhi speaks with its meaningful,

164
00:22:46,920 --> 00:22:49,920
dhamma-wa-dhi speaks with its right.

165
00:22:49,920 --> 00:22:53,280
Vinayawadhi speaks with its moral,

166
00:22:53,280 --> 00:22:57,080
nidhanawadhi, nidhanawadhi, nidhanawadhi,

167
00:22:57,080 --> 00:23:01,240
wajang dasita.

168
00:23:01,240 --> 00:23:11,200
Nidhanawadhi speaks words that are to be treasured.

169
00:23:11,200 --> 00:23:17,200
Gali in a sub-bandi, some in the right time,

170
00:23:17,200 --> 00:23:18,560
in the right place.

171
00:23:18,560 --> 00:23:27,720
Bharyantawadhi speaks in the right, in the right measure.

172
00:23:27,720 --> 00:23:32,000
Atawadhi speaks with its true,

173
00:23:32,000 --> 00:23:38,600
connected with what is useful.

174
00:23:38,600 --> 00:23:43,640
So this is in regards to the first four precepts.

175
00:23:43,640 --> 00:23:45,960
It's actually talking about reasons

176
00:23:45,960 --> 00:23:49,720
why people praise the Buddha.

177
00:23:49,720 --> 00:23:51,920
The context of the quote is somewhat interesting,

178
00:23:51,920 --> 00:23:55,120
not bound in the actual quote,

179
00:23:55,120 --> 00:24:00,200
is that he's actually speaking in sort of,

180
00:24:00,200 --> 00:24:02,360
well, he's belittling these,

181
00:24:02,360 --> 00:24:05,000
he's calling these, these are called minor morality.

182
00:24:05,000 --> 00:24:10,240
He said, this is what ordinary people would praise the Buddha for.

183
00:24:10,240 --> 00:24:12,520
So in a sense, he's bragging, or boasting,

184
00:24:12,520 --> 00:24:17,520
and saying that all these great things,

185
00:24:17,520 --> 00:24:23,280
but anyone says that's why the Buddha is great?

186
00:24:23,280 --> 00:24:24,680
Forget about it.

187
00:24:24,680 --> 00:24:30,480
That's not why becoming a Buddha or the practice of Buddhism,

188
00:24:30,480 --> 00:24:33,680
or the practice of enlightenment, is a great thing.

189
00:24:33,680 --> 00:24:39,760
I don't just practice for morality.

190
00:24:39,760 --> 00:24:46,720
So on the one hand, this is great to learn about the speech,

191
00:24:46,720 --> 00:24:49,000
but you can become arrogant about it,

192
00:24:49,000 --> 00:24:53,320
you can say, well, yes, we can't speak like this,

193
00:24:53,320 --> 00:24:54,520
we can't speak like that,

194
00:24:54,520 --> 00:24:57,840
and then you feel the superior because of it.

195
00:24:57,840 --> 00:25:00,520
And when other people are speaking,

196
00:25:00,520 --> 00:25:02,640
poorly, I'm speaking well,

197
00:25:02,640 --> 00:25:06,520
and other people speak harshly, I speak softly

198
00:25:06,520 --> 00:25:09,760
and pleasing to the air, right?

199
00:25:09,760 --> 00:25:12,160
Easy to become attached to it.

200
00:25:12,160 --> 00:25:16,360
I don't lie, and you feel proud of that.

201
00:25:22,400 --> 00:25:25,640
The Buddha said, even animals can,

202
00:25:25,640 --> 00:25:32,640
when animals can go without speaking, speech isn't the measure.

203
00:25:40,440 --> 00:25:45,040
But nonetheless, speech is important, speech and action.

204
00:25:45,040 --> 00:25:49,760
When it comes back to haunt us, if we use wrong speech,

205
00:25:49,760 --> 00:25:52,960
just like if we act in the wrong way,

206
00:25:52,960 --> 00:25:55,440
it comes back to haunt us.

207
00:25:55,440 --> 00:25:59,760
If we speak in the wrong way, it will come back to haunt us.

208
00:25:59,760 --> 00:26:02,240
When we meditate, when we consider,

209
00:26:02,240 --> 00:26:05,120
when we're quiet and mindful,

210
00:26:05,120 --> 00:26:08,080
do we get to see the results and feel good or bad

211
00:26:08,080 --> 00:26:10,440
about the things that we've done?

212
00:26:10,440 --> 00:26:14,440
Being encouraged or discouraged about ourselves,

213
00:26:14,440 --> 00:26:28,040
we can compose or decomposed by our past actions and speech.

214
00:26:28,040 --> 00:26:32,440
So speech is something you have to be careful about.

215
00:26:32,440 --> 00:26:36,040
You know, that's the norm of our tonight,

216
00:26:36,040 --> 00:26:38,840
and you'll have to meet him.

217
00:26:38,840 --> 00:26:45,840
Goodnight.

