1
00:00:00,000 --> 00:00:08,400
Okay, so let's get started today. We're starting on Balong. Let's see

2
00:00:08,400 --> 00:00:13,080
where we're doing the rest of the questions at the end of the... no questions

3
00:00:13,080 --> 00:00:19,400
rest of the sample sentences at the end of the... of the text.

4
00:00:19,400 --> 00:00:33,600
So number 18, if you scroll right down to the bottom. Balong, Balong, I'm not

5
00:00:33,600 --> 00:00:46,120
saying where you are. Anyone tell me what that means? Balong is young or

6
00:00:46,120 --> 00:00:52,680
a child, right? Balong means a fool.

7
00:00:55,360 --> 00:00:59,600
I think I'll say when I jump out, I don't know.

8
00:01:03,320 --> 00:01:11,720
And now is not. Yes. So the fool's not doing something. What case is the

9
00:01:11,720 --> 00:01:29,640
fooling to one? So is that the subject of the sentence? The object of the

10
00:01:29,640 --> 00:01:45,280
sentence? Yes. Where's the subject? The subject must be, so wait, so wait,

11
00:01:45,280 --> 00:01:54,960
it must be. What the fool is the object? So I'm assuming the way it must be the

12
00:01:54,960 --> 00:02:02,840
subject now. Well, most... or many poly sentences don't even have a subject.

13
00:02:02,840 --> 00:02:08,600
And simplicity in the verb. See, we as actually a verb, a yet, is them the

14
00:02:08,600 --> 00:02:21,200
up to divending third person singular. You know, you can plug these into the

15
00:02:21,200 --> 00:02:27,200
digital poly reader. If you have it, you can go to the text pad, tp on the bottom,

16
00:02:27,200 --> 00:02:33,880
and paste it in, and then click Analyze.

17
00:02:34,360 --> 00:02:40,080
Yeah, except it's not don't because don't would be second person. It would be

18
00:02:40,080 --> 00:02:46,000
talking to second person. Say, wait, yeah, it's not second person. So it's some

19
00:02:46,000 --> 00:02:54,360
third person advice. He, she, or it, or in this case, one, one should not. Another

20
00:02:54,360 --> 00:02:57,920
reason for leaving out the subject is because you're talking about the general

21
00:02:57,920 --> 00:03:07,320
third person. One should not associate. So this is like a save when a save when

22
00:03:07,320 --> 00:03:15,520
a is the, save when a is the noun, save when a means associating association. I say

23
00:03:15,520 --> 00:03:24,000
when a means non association. Say it was the, is the root. Say what, say what

24
00:03:24,000 --> 00:03:39,000
the, say way, yeah, say one that you can agree. Banditamewa, bhajatu.

25
00:03:39,000 --> 00:04:08,120
Any ideas? Banditamewa is device, bhajatu, bhajatu comes from bhajana. But it's

26
00:04:08,120 --> 00:04:14,960
sort of to do with partaking. So bhajatu, it's similar, but it means to get

27
00:04:14,960 --> 00:04:28,400
involved with or to be associated with, similarly, close to, to partake in a sense of

28
00:04:28,400 --> 00:04:38,280
friendship. I see, so it's similar to save, save, yeah, the meaning is similar. So

29
00:04:38,280 --> 00:05:06,440
then associate with the wise, what form is bhajatu? One, one. Was it first person, second

30
00:05:06,440 --> 00:05:24,760
person, third person? Naga says first, that sounds good. Well, actually, one one would

31
00:05:24,760 --> 00:05:30,400
be third person, because in probably the third person is, they call it the first person.

32
00:05:30,400 --> 00:05:36,200
Well, we call the third person, they call the first person, but put them up, when he's

33
00:05:36,200 --> 00:05:46,760
there. And what tense is it?

34
00:06:36,200 --> 00:06:44,520
Can anyone try these? I guess some of you are not. I guess no one did any over the

35
00:06:44,520 --> 00:07:08,720
week. I looked at them, but I'm not, you know, what I have is not, not very clear.

36
00:07:08,720 --> 00:07:18,800
Bhaj and you add an hat and two. Bhaj is the stem and then you add two on the end. So

37
00:07:18,800 --> 00:07:25,600
where do we see two? What form would two be? Like why did you guys say it was a one, one,

38
00:07:25,600 --> 00:07:34,200
one, one of what, what tends? There's an imperative form with a two ending.

39
00:07:34,200 --> 00:07:39,760
There is indeed. In fact, that's what you should always think of, because if you see two

40
00:07:39,760 --> 00:07:45,240
on the end of a verb or a two especially, right away, it's imperative, one, one, third person

41
00:07:45,240 --> 00:07:55,520
singular, which means may he, she or it do something. In this case, should it's more not

42
00:07:55,520 --> 00:08:05,040
may, that should, one should again, like say, way is also a third person singular, but what,

43
00:08:05,040 --> 00:08:07,800
what should do is also third person singular, say, way is set to me, which we call the

44
00:08:07,800 --> 00:08:15,160
operative and budget to a sponge to me, which we call the imperative. So this sentence

45
00:08:15,160 --> 00:08:23,960
means you have to take the ava into account ava means indeed, or fairly, it's emphatic

46
00:08:23,960 --> 00:08:31,080
in some way, indeed one should or rather one should, you could say, suppose, suppose like

47
00:08:31,080 --> 00:08:40,760
indeed one should keep companionship with wise people, with, with a wise man. Those of,

48
00:08:40,760 --> 00:08:44,560
if someone meets a wise person, they should keep company with them.

49
00:08:44,560 --> 00:08:55,760
Okay, the next sentence is a relative correlative. The KYT system, K is for questions,

50
00:08:55,760 --> 00:09:00,720
but yeah, and that are for relative correlative. So the first one is the, in English we would

51
00:09:00,720 --> 00:09:10,360
be say the WT. So young is the Ws and T is, and dang is the T. So young means what, and

52
00:09:10,360 --> 00:09:21,320
dang means that. So it would go in this form, what, so and so does that you should do.

53
00:09:21,320 --> 00:09:30,400
In fact, that's exactly what this sentence means. What young is the, the object of the sentence.

54
00:09:30,400 --> 00:09:38,920
So what a dura wise man may do. In this case, this is set to me, but it's not in the

55
00:09:38,920 --> 00:09:44,320
sense of should, it should, it should, it should in the sense of may, whatever, in the sense

56
00:09:44,320 --> 00:09:50,960
whatever they should do, whatever they may do, it's quite, it's called octative. Whatever

57
00:09:50,960 --> 00:09:57,520
they should do, that you should do. That you should do. You should do it.

58
00:09:57,520 --> 00:10:03,160
Karasu is second person singular and again imperative, but it's in a regular form because

59
00:10:03,160 --> 00:10:08,520
Karasu is an irregular verb. So it's all going to look weird. But if you click on Karasu

60
00:10:08,520 --> 00:10:13,400
in this digital poly reader, it, and then you click on little C that comes up beside the

61
00:10:13,400 --> 00:10:19,760
word, decide the definition. If you scroll down and you can see that it's a two one

62
00:10:19,760 --> 00:10:36,880
imperative. Oh, that's cool. Yeah, whatever a wise person should do, tongue Karasu, that

63
00:10:36,880 --> 00:10:44,080
you should do. So we start the sentence with what, but what is this, the object of the

64
00:10:44,080 --> 00:10:50,520
sentence, but in order to have it make sense, we put it first, because this, the emphasis

65
00:10:50,520 --> 00:10:56,880
is on the what and the that, young is what, dung is that. This is a very simple relative

66
00:10:56,880 --> 00:11:14,800
correlative sentence and taking a lot more difficult than this. The next one. Next one's difficult

67
00:11:14,800 --> 00:11:31,120
because a bunch of guys are very, very irregular verb. It means to pass by. And in this

68
00:11:31,120 --> 00:11:49,240
form, it means it's imperative. So it means may it pass you by. It should pass you by. And

69
00:11:49,240 --> 00:11:56,240
the negative of the imperative uses my instead of not all other verb forms used not, but

70
00:11:56,240 --> 00:12:03,360
the imperative uses my. So it's just like now, but it's the negative. You should not.

71
00:12:03,360 --> 00:12:15,860
They should not. It should not. May it not. In this case, may it not. So may it not pass

72
00:12:15,860 --> 00:12:29,400
you by. And what is the subject of the sentence? Cano. And what does kind of mean? Moment.

73
00:12:29,400 --> 00:12:40,520
So the moment and then will is a short form of the second person plural, personal pronoun.

74
00:12:40,520 --> 00:12:59,160
You, you plural. And it's in this case, a to one, a to two, sorry, to two form of. So what

75
00:12:59,160 --> 00:13:15,240
is this? The object of the sentence means you all. Well, and no and day, these are very

76
00:13:15,240 --> 00:13:22,880
common day is the common plural, short form plural for a third person. And we're also

77
00:13:22,880 --> 00:13:31,960
the second person singular. Whoa, is third person plural singular, third person plural.

78
00:13:31,960 --> 00:13:39,320
And no is first person plural. May is first person singular. So for the pronouns, they're

79
00:13:39,320 --> 00:13:46,520
all shortened to may and no. That's the first person forms. And day and whoa, that's the

80
00:13:46,520 --> 00:13:56,880
second person, second person. Third person, third person doesn't really have that. So it's

81
00:13:56,880 --> 00:14:10,200
first and second person pronouns have shortened forms. May and no and day and whoa. So what

82
00:14:10,200 --> 00:14:37,520
does the sentence mean? May you not let the moment, I think I have their tense wrong,

83
00:14:37,520 --> 00:14:49,840
but not let the moment pass you by with the subject again, kind of, which means moment.

84
00:14:49,840 --> 00:14:56,360
So in this case, it's made a subject when you translate this verb. May the moment not

85
00:14:56,360 --> 00:15:26,080
pass you by. That's right.

86
00:15:26,080 --> 00:15:34,480
Okay, this next one is interesting. Kana kana dita is one whose moment is in the past, one

87
00:15:34,480 --> 00:15:40,280
for whom the moment has passed. It's an interesting word. I didn't, I hadn't, I thought

88
00:15:40,280 --> 00:16:08,880
about that one. What does the sentence mean? Is that the so-called statement, so Chanti?

89
00:16:08,880 --> 00:16:19,760
So Chanti, one whose moment has passed away with brief or something. What form is so Chanti?

90
00:16:38,880 --> 00:16:59,080
Okay, so Chanti, one whose moment has passed away with brief or something, so Chanti,

91
00:16:59,080 --> 00:17:16,120
one whose moment has passed away with brief or something, so Chanti, one whose moment has

92
00:17:16,120 --> 00:17:32,160
passed away with brief or something, so Chanti, one whose moment has passed away with brief

93
00:17:32,160 --> 00:18:02,120
or something, so Chanti, one whose moment has passed away with brief or something, so Chanti

94
00:18:02,120 --> 00:18:26,320
would form it is.

95
00:18:32,120 --> 00:18:57,920
Present plural third person. Yes, so it means they sorrow. Who are the day in this case?

96
00:18:57,920 --> 00:19:07,720
Yes, what form is Chanti time? That ending doesn't look familiar at all.

97
00:19:07,720 --> 00:19:22,280
Oh, are you saying one of the paradigms ends with ITA? What does this word? Chanti means

98
00:19:22,280 --> 00:19:42,120
moment? What's the rest of it? If you use the digital poly reader and you look at that

99
00:19:42,120 --> 00:19:53,320
little white box below the word, you can separate it into two parts. The second option

100
00:19:53,320 --> 00:20:05,920
is Kana and Adita. Adita means past, so the word here is Kana and Adita, that's the stem word, so it's not

101
00:20:05,920 --> 00:20:15,400
an ITA ending. Oh, I see. So according to digital poly reader, Kana is either a moment,

102
00:20:15,400 --> 00:20:34,160
a minute, or an opportunity, so. Yeah, and what does Adita? Missed opportunity.

103
00:20:34,160 --> 00:20:44,280
Are you going to cost? Adita means past. So this looks like it means past opportunities

104
00:20:44,280 --> 00:20:52,200
that are in the past, or, yeah, opportunities that are in the past, but this kind of compound,

105
00:20:52,200 --> 00:21:04,600
it turns it into an ownership, where it means one who has the thing that it's describing.

106
00:21:04,600 --> 00:21:13,160
So one whose moment is in the past, one whose moment has passed them by, one whose moment

107
00:21:13,160 --> 00:21:23,240
is already gone. And so it'll be masculine in this case, and it's masculine plural. This

108
00:21:23,240 --> 00:21:35,720
is actually a one-two form, just like Purisa, Purisa, is Purisa, Purisa, to a one-two is Purisa. So this is, this

109
00:21:35,720 --> 00:21:45,920
would be conjugated as Kana, Dito, Kana, Dita, Kana, Dita, Kana, Dita, Kana, Dita, Kana, Dita, Kana, Dita,

110
00:21:45,920 --> 00:21:56,680
and so on. If you click on the conjugation, you'll see it. It'll give right now, it's

111
00:21:56,680 --> 00:22:06,820
giving me the conjugation of Adita.

112
00:22:06,820 --> 00:22:19,720
It's got the volcano in the wrong place, that's interesting.

113
00:22:36,820 --> 00:23:01,700
So this means people whose moment in the past, indeed, will, indeed, grieve. It means people

114
00:23:01,700 --> 00:23:16,180
that the moment passed them by, indeed, grieve, indeed, they grieve. The next one's really

115
00:23:16,180 --> 00:23:21,940
tough, but we've already had this example. Maybe you didn't, it's in the book, so that's

116
00:23:21,940 --> 00:23:29,580
why he's giving it. Dana, I think it is above. It is in the text of Dana, means by them,

117
00:23:29,580 --> 00:23:40,540
but Aha is difficult. Aha is past, past, past tense, but Oka, I think. So again, first

118
00:23:40,540 --> 00:23:49,380
person, third person, singular, Borana is the subject of the sentence, means one who is in

119
00:23:49,380 --> 00:24:03,300
the past, one who is lived in the ancient times, Borana, Borana. And Dana here is specific

120
00:24:03,300 --> 00:24:12,860
meaning. It means, therefore, it's Dana means by that, literally, from that, no, by that,

121
00:24:12,860 --> 00:24:23,060
with that. But here means, like, because of that. Therefore, the Borana, the old the ancient

122
00:24:23,060 --> 00:24:29,260
one has said. That's what this means. So this is what you'll see in the commentaries,

123
00:24:29,260 --> 00:24:34,940
when, in the wee city manga that we're studying, he uses this kind of phrase quite often.

124
00:24:34,940 --> 00:24:39,340
They said something, and then he said, for that reason, it was said by the ancient, usually

125
00:24:39,340 --> 00:24:43,700
it's plural, but here it's singular.

126
00:24:43,700 --> 00:24:49,900
Bante, where is the implication of, of said? Aha is in a regular verb, it's in a regular

127
00:24:49,900 --> 00:24:58,180
past tense of, watch, I think, right? No. It's very regular.

128
00:24:58,180 --> 00:25:11,100
Yeah, let's us hear, exclamation of surprise. No, that's not what it means. Maybe I have

129
00:25:11,100 --> 00:25:16,540
the wrong form up here. Yeah, scroll down to the Dain plus Aha.

130
00:25:16,540 --> 00:25:25,860
Okay, thank you. So that's why that little box is there, because it's going to, it

131
00:25:25,860 --> 00:25:33,420
doesn't know can't interpret it like we can. This is Dain, Dain, Aha is gone, plus Aha,

132
00:25:33,420 --> 00:25:45,060
which is a regular form of, regular form of, what's the actual verb?

133
00:25:45,060 --> 00:25:52,900
Yeah, because it's sort of its own verb. Anyway, you have to learn about these

134
00:25:52,900 --> 00:25:55,740
irregular forms.

135
00:25:55,740 --> 00:26:15,460
Okay, the next one.

136
00:26:25,740 --> 00:26:46,580
Oh, Jana, I'm going to count. And Jana means a person, so Jana means people.

137
00:26:46,580 --> 00:27:08,620
Okay. And back, who is like multiple? Many here. Many much, let me tell you like that.

138
00:27:08,620 --> 00:27:35,260
It has here, sonipati, gathered. Yeah, inksu is third person plural, past tense.

139
00:27:35,260 --> 00:27:47,260
In a sense, it's something like, have gathered.

140
00:27:47,260 --> 00:28:12,260
So many people have gathered here.

141
00:29:17,260 --> 00:29:21,580
This right next one, I'm not clear on people.

142
00:29:47,260 --> 00:30:17,100
Let me see if I've got a typo here or not. Yeah, that should be a long night.

143
00:30:17,100 --> 00:30:18,100
Bye, Nana. Have a good day.

144
00:30:47,100 --> 00:30:55,100
Just a second.

145
00:31:17,100 --> 00:31:37,100
Thank you.

146
00:31:47,100 --> 00:32:09,340
Yeah. Why? King is in this case. Why? Do you, you have a lady of standard as though

147
00:32:09,340 --> 00:32:14,140
as though afraid? Yeah, why do you stand there? Because what means as though it's like a

148
00:32:14,140 --> 00:32:24,300
simile comparison as or like? So why do you stand there as though you're afraid? This

149
00:32:24,300 --> 00:32:32,820
is king, which means what? Kind of. But in the sense of why? What for what for are you

150
00:32:32,820 --> 00:32:39,340
standing there as if afraid? That's a difficult one grammatically, unless you really

151
00:32:39,340 --> 00:32:46,860
like it. I think, get it. What kind of makes it tough, but what comes right after the

152
00:32:46,860 --> 00:32:52,780
thing that it's actually comparing it to? So here it's like you were afraid, like someone

153
00:32:52,780 --> 00:33:00,820
who is afraid, beat as someone who's afraid, get the same means you stand. See, is the

154
00:33:00,820 --> 00:33:09,180
second person singular? Present. You stand. King, why do you stand? What, for what for do you

155
00:33:09,180 --> 00:33:18,300
stand? Beto, why as though afraid? Anyway, I have to go. Thank you all for tuning in.

156
00:33:18,300 --> 00:33:25,100
Next week, maybe we'll start on Chapupala, if there's still interest. I was thinking I

157
00:33:25,100 --> 00:33:31,460
might do this whole course again, using PowerPoint and YouTube, or maybe not even YouTube,

158
00:33:31,460 --> 00:33:36,900
but screen capture. You can just do screen capturing and post the videos on YouTube for

159
00:33:36,900 --> 00:33:44,220
everyone. Because with screen capture, I can actually write on the screen and put up slides

160
00:33:44,220 --> 00:33:50,140
with big text and do it piece by piece, make it a little clearer than this. That sounds

161
00:33:50,140 --> 00:33:55,540
good, yeah, what the visual would be great. On the other end, I'm starting to run out of

162
00:33:55,540 --> 00:34:05,140
time as we get closer to September. Anyway, thanks for joining, and hope to see you all

163
00:34:05,140 --> 00:34:28,140
next week. Thank you, Bhante. Sadhu, Bhante. Sadhu, Bhante.

