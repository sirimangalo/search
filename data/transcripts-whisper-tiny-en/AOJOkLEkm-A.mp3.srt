1
00:00:00,000 --> 00:00:26,000
Good evening everyone broadcasting lives and be very 12th.

2
00:00:26,000 --> 00:00:47,000
We are broadcasting a few minutes early.

3
00:00:47,000 --> 00:01:13,000
Today's quote is about the senses.

4
00:01:13,000 --> 00:01:24,000
Being someone who knows Pauli, you got to wonder what he means by unruffled.

5
00:01:24,000 --> 00:01:53,000
I don't know about the whole unruffled thing.

6
00:01:53,000 --> 00:02:00,000
It's the happiness of being unruffled and it's the happiness that is unsullied.

7
00:02:00,000 --> 00:02:03,000
That's what Bikumori translates.

8
00:02:03,000 --> 00:02:06,000
We'll go with Bikumori's translation.

9
00:02:06,000 --> 00:02:15,000
On seeing a form with the eye, he does not grasp, but it signs and features.

10
00:02:15,000 --> 00:02:25,000
That's an important quote that I often bring up.

11
00:02:25,000 --> 00:02:35,000
The name of Bikumori is the name of Bikumori.

12
00:02:35,000 --> 00:02:54,000
The name of Bikumori is the name of Bikumori.

13
00:02:54,000 --> 00:03:19,000
It's actually a criticism that we get from time to time about using a mantra, using a word to remind ourselves of the experience because it feels like you're not really getting deep into the experience.

14
00:03:19,000 --> 00:03:28,000
When you see something and you see seeing, it actually stops you from getting deep into it, from actually experiencing it.

15
00:03:28,000 --> 00:03:34,000
It's criticism by other Buddhists who think that, well, you can't really understand something unless you get deep into it.

16
00:03:34,000 --> 00:03:38,000
The words get in the way.

17
00:03:38,000 --> 00:03:42,000
The words are designed to get in the way.

18
00:03:42,000 --> 00:03:48,000
That's based on what the Buddha is saying here in the midi minikaya.

19
00:03:48,000 --> 00:03:57,000
When seeing a form with the eye, one doesn't grasp at the sign that I've talked about this before.

20
00:03:57,000 --> 00:04:03,000
We had this a couple of days ago, I think.

21
00:04:03,000 --> 00:04:26,000
So in the midi is a characteristic or a sign is a good word. It's just we don't use it in English really, but when you see a human being, as I said, how you know it's a man or it's a woman is by the sign of a man or the sign of a woman, the hint or the characteristic.

22
00:04:26,000 --> 00:04:33,000
Something that creates or induces recognition, that's a man, that's a woman.

23
00:04:33,000 --> 00:04:38,000
When you see an apple, that which tells you it's an apple and not an orange.

24
00:04:38,000 --> 00:04:45,000
When the moment you get that it's an apple, that's an emitter.

25
00:04:45,000 --> 00:04:57,000
Beyond that, there's the sign of beauty and the sign of ugliness.

26
00:04:57,000 --> 00:05:02,000
There's the sign of that which is desirable.

27
00:05:02,000 --> 00:05:11,000
When you grasp that, when you get yourself to the point of seeing as something as beautiful, then desire will be right.

28
00:05:11,000 --> 00:05:15,000
So this is what we want to avoid.

29
00:05:15,000 --> 00:05:31,000
When you see something, you see a person and you say to yourself, see, see, the person doesn't get to the point of a person, it doesn't go further to the point of beautiful or ugly.

30
00:05:31,000 --> 00:05:45,000
If someone you like or dislike, when you say seeing, seeing, that doesn't, that part of the chain doesn't arise as you've broken the chain.

31
00:05:45,000 --> 00:05:50,000
Anubianjana, it's another aspect of this.

32
00:05:50,000 --> 00:05:52,000
Anubianjana means the particulars.

33
00:05:52,000 --> 00:05:54,000
It's really saying the same thing in two different ways.

34
00:05:54,000 --> 00:05:57,000
But Anubianjana means particulars.

35
00:05:57,000 --> 00:06:03,000
So we don't want to know the particulars. Not interesting, because the particulars are where the danger comes.

36
00:06:03,000 --> 00:06:06,000
So the Buddha says next.

37
00:06:06,000 --> 00:06:31,000
Anubianjana means, when the eye faculty is not guarded, a santam, we run done dwelling with the unguarded.

38
00:06:31,000 --> 00:06:43,000
Evil unwholesome states of covetousness and grief might invade him.

39
00:06:43,000 --> 00:06:52,000
So the key, this is where likes and dislikes, because this is where pleasure and displeasure come.

40
00:06:52,000 --> 00:07:10,000
This is where addiction and aversion, these habits that we cultivate, that make us greedy, that make us angry, that create frustration and boredom, that create fear and that create worry, that create addiction,

41
00:07:10,000 --> 00:07:28,000
create withdrawal, disappointment, not getting what you want, getting what you don't want, all this trouble that we have in our lives. It's all created by grasping at signs in particular.

42
00:07:28,000 --> 00:07:40,000
So when we try, when you see something, we try to say to ourselves, see, remind ourselves, let's see. Otherwise it becomes beautiful or ugly.

43
00:07:40,000 --> 00:07:50,000
And sounds at the ear, the problem is that we're taught in society to look for the good sounds and the good sight.

44
00:07:50,000 --> 00:08:00,000
It's actually this kind of talk is highly unwelcome for most people.

45
00:08:00,000 --> 00:08:12,000
There was a talk about beauty as though there was something wrong with that, as though we should see things look beyond beauty, see beautiful things without noticing how beautiful they are.

46
00:08:12,000 --> 00:08:22,000
Spirituality, spiritual teachers or spiritual practitioners often make this mistake.

47
00:08:22,000 --> 00:08:34,000
They think this whole idea of stopping to smell the roses, as though there was something beneficial about smelling roses.

48
00:08:34,000 --> 00:08:53,000
University, it's surprising. I guess it speaks to the age group, but I would say somewhere around 50% of the undergraduate students at the university have these earbuds in their ears like in between classes all the time.

49
00:08:53,000 --> 00:09:04,000
They come to do meditation, today I did a five-minute meditation all day, so people would come and give them a five-minute meditation lesson.

50
00:09:04,000 --> 00:09:13,000
And most of them had to take the earbud out of their ear before they sat down. It's really a thing.

51
00:09:13,000 --> 00:09:24,000
We can't live in the world. It's bizarre for those of us who walked down the street and are in the world hearing the ordinary sounds.

52
00:09:24,000 --> 00:09:30,000
One of my classmates said, what did you say?

53
00:09:30,000 --> 00:09:45,000
Something about singing in the shower and I said, you sing in the shower and I said, oh yeah, I have to have music. And I said, you have music in the bathroom?

54
00:09:45,000 --> 00:10:01,000
And it was like, yes, I think music in the bathroom is important for a surgeon. She may end up watching this. She's a really good person.

55
00:10:01,000 --> 00:10:15,000
Which I haven't mentioned, I don't think. We're doing a piece symposium, not McMaster. People have been asking me, I don't really know what a symposium, but the word symposium means, but sort of like a piece fair.

56
00:10:15,000 --> 00:10:41,000
And in my mind it is actually, I don't think that's where the direction that piece studies program wants us to go. But I want it to be a healing experience where people come together and I mean like a piece, an active piece process where people come and they leave knowing more about piece, having skill learn skills about cultivating peace.

57
00:10:41,000 --> 00:10:55,000
Anyway, sorry, a little bit off track there. But the thing, we want to hear beautiful music, beautiful sounds.

58
00:10:55,000 --> 00:11:08,000
A lot of people ask whether you can meditate to music, which shows this misunderstanding about spirituality. I mean from a Buddhist point of view anyway.

59
00:11:08,000 --> 00:11:16,000
A lot of people would say the Buddhists have got it wrong, so to each their own.

60
00:11:16,000 --> 00:11:21,000
Smells, we want to smell good things.

61
00:11:21,000 --> 00:11:28,000
It's the nature of wanting to experience good things that you cultivate partiality.

62
00:11:28,000 --> 00:11:39,000
It's like a pendulum. If you pull it one way, you're creating the, you grab it with the potential energy.

63
00:11:39,000 --> 00:11:44,000
More you pull the more potential until finally you let go and it will swing back.

64
00:11:44,000 --> 00:11:51,000
You create the opposite. You create the aversion with the attraction.

65
00:11:51,000 --> 00:12:07,000
You become more irritable, more dissatisfied, more prone to disappointment, more prone to anger.

66
00:12:07,000 --> 00:12:15,000
This is why people fight. Those people who are steeped in central pleasure have so much central pleasure in being the ones who fight the most.

67
00:12:15,000 --> 00:12:20,000
Fight with each other, bigger with each other, argue with each other.

68
00:12:20,000 --> 00:12:25,000
So much conflict comes from sensuality.

69
00:12:25,000 --> 00:12:29,000
Comes from that which is supposed to make us happy.

70
00:12:29,000 --> 00:12:34,000
We go to war over our happiness.

71
00:12:34,000 --> 00:12:39,000
It's the truth of it.

72
00:12:39,000 --> 00:12:51,000
So when we smell good smells, we want to smell the roses, we don't want to smell bad smells.

73
00:12:51,000 --> 00:12:59,000
And it seems so ingrained in the experience where you think a bad smell is a bad smell, it's bad.

74
00:12:59,000 --> 00:13:10,000
This is the first myth that we have to dispel. The bad is a product of the particulars of the smell.

75
00:13:10,000 --> 00:13:16,000
If it's just smell, if you say smelling the smell, it's just smell.

76
00:13:16,000 --> 00:13:23,000
Same goes with taste, good taste, bad taste.

77
00:13:23,000 --> 00:13:36,000
It's easy to get caught up in taste, but it's also very easy, if you know how, to free yourself, to avoid that whole world of addiction and aversion.

78
00:13:36,000 --> 00:13:40,000
If you say there's a taste in tasting.

79
00:13:40,000 --> 00:13:47,000
All of the seeing, seeing with sensuality, with sexuality, with romance, seeing a beautiful person,

80
00:13:47,000 --> 00:13:53,000
seeing, seeing, it's just, it's just seeing. You cut it off totally.

81
00:13:53,000 --> 00:13:59,000
First you have to be diligent, of course.

82
00:13:59,000 --> 00:14:03,000
It's not something that just, you can break off with one go.

83
00:14:03,000 --> 00:14:08,000
It's a habit that you build. Eventually it becomes a habit to see things just as they are.

84
00:14:08,000 --> 00:14:11,000
And that's the habit we want to cultivate.

85
00:14:11,000 --> 00:14:20,000
When you can do that, you move towards letting go.

86
00:14:20,000 --> 00:14:27,000
So, a really good quote. Not so much more to say.

87
00:14:27,000 --> 00:14:35,000
Except we could talk about this happiness that is unsullied or unadulterated.

88
00:14:35,000 --> 00:14:46,000
So, it's the difference between happiness that is based on partiality because it's dependent on x and not y or not x.

89
00:14:46,000 --> 00:14:57,000
As opposed to the happiness that is free from reliance or dependence on experience.

90
00:14:57,000 --> 00:15:11,000
Happiness that comes from being free from letting go, from freedom from stress, freedom from the concern about the experience.

91
00:15:11,000 --> 00:15:17,000
Anyway. So, that's our Dhamma for today.

92
00:15:17,000 --> 00:15:22,000
Nobody joined me on the hangout today. Must be because last night didn't have one.

93
00:15:22,000 --> 00:15:29,000
You can go. Oh, I know because I didn't post anger. You can't join me.

94
00:15:29,000 --> 00:15:36,000
Right, there's a step missing here. Sorry.

95
00:15:36,000 --> 00:15:41,000
There's the hangout.

96
00:15:41,000 --> 00:15:43,000
Is there anybody around who wants to join you?

97
00:15:43,000 --> 00:15:45,000
Only join if you want to ask a question.

98
00:15:45,000 --> 00:15:52,000
You have a question? Join me as get in the hangout.

99
00:16:15,000 --> 00:16:25,000
Larry, do you have a question?

100
00:16:25,000 --> 00:16:37,000
No. I've been listening to the Dhamma talk and I just noticed that the link to this call-in showed up.

101
00:16:37,000 --> 00:16:47,000
So, I clicked on it. I've been wondering if I should have seen the link earlier.

102
00:16:47,000 --> 00:17:00,000
And I presume that a link, a prior link to a previous call would not work for this call.

103
00:17:00,000 --> 00:17:07,000
Okay. Everyone is uniquely coded or whatever for the internet. Okay.

104
00:17:07,000 --> 00:17:17,000
So, listen to your talk and click on the link to the call-in as soon as I saw it.

105
00:17:17,000 --> 00:17:27,000
But I do have a question. You sparked a question in me.

106
00:17:27,000 --> 00:17:36,000
So, when we're just not essentially noting, seeing, seeing, smelling, hearing.

107
00:17:36,000 --> 00:17:41,000
And of course those are based on the sense doors.

108
00:17:41,000 --> 00:17:46,000
The eyes, ears, nose, mouth, touching.

109
00:17:46,000 --> 00:17:54,000
So, is it inappropriate to just say something like sensing?

110
00:17:54,000 --> 00:18:09,000
And it would try to get, try to tie the noting to the specific sense door is that there's evidently value in that.

111
00:18:09,000 --> 00:18:11,000
And I'm just speculating.

112
00:18:11,000 --> 00:18:18,000
Right. I mean the word isn't that important. Whatever brings you close to the experience.

113
00:18:18,000 --> 00:18:23,000
But sensing to me is too abstract. It's not likely to bring you close to the experience.

114
00:18:23,000 --> 00:18:26,000
I mean, look at the bear experience.

115
00:18:26,000 --> 00:18:31,000
Right. It's a bit abstract.

116
00:18:31,000 --> 00:18:33,000
You don't want to send it something.

117
00:18:33,000 --> 00:18:35,000
Here's something.

118
00:18:35,000 --> 00:18:40,000
Good. I appreciate the clarification. I've wondered that.

119
00:18:40,000 --> 00:18:49,000
And like you say, just say, try to boil it down to the very central sensing.

120
00:18:49,000 --> 00:18:54,000
It isn't very satisfying.

121
00:18:54,000 --> 00:19:02,000
And, you know, the notionally or intellectually, it's not a very satisfying term to use.

122
00:19:02,000 --> 00:19:09,000
Yeah. I appreciate that.

123
00:19:09,000 --> 00:19:11,000
One thing I did want to talk about.

124
00:19:11,000 --> 00:19:13,000
Well, I got your attention, everybody.

125
00:19:13,000 --> 00:19:26,000
Tomorrow, tomorrow I'm having a weekly, we're started doing these weekly talks in second life at the Buddha Center.

126
00:19:26,000 --> 00:19:32,000
So if you know what second life is and you know where the Buddha Center is, it can come out.

127
00:19:32,000 --> 00:19:45,000
That's at 3 p.m. Or you can just come to the meditation site, meditation.ceremungalow.org.

128
00:19:45,000 --> 00:19:49,000
Simultaneously broadcasting.

129
00:19:49,000 --> 00:19:53,000
So you can listen to the audio.

130
00:19:53,000 --> 00:19:56,000
I don't know what I'm talking about yet.

131
00:19:56,000 --> 00:20:01,000
Anybody have anything they want me to give a talk on tomorrow?

132
00:20:01,000 --> 00:20:07,000
If you come on and give me a topic, I'll consider it.

133
00:20:07,000 --> 00:20:10,000
Can you give a talk on dependent origination?

134
00:20:10,000 --> 00:20:14,000
I actually gave one in second life on dependent origination.

135
00:20:14,000 --> 00:20:18,000
I've given several on dependent origination.

136
00:20:18,000 --> 00:20:24,000
You can look it up.

137
00:20:24,000 --> 00:20:30,000
I don't know if they're any good, but I think there's one on practical dependent origination, not second life.

138
00:20:30,000 --> 00:20:35,000
There's one that's called practical dependent origination.

139
00:20:35,000 --> 00:20:36,000
It seems to me.

140
00:20:36,000 --> 00:20:38,000
Maybe it's one I did in Sri Lanka.

141
00:20:38,000 --> 00:20:42,000
I think it's one I did on the Buddhist television in Sri Lanka.

142
00:20:42,000 --> 00:20:49,000
I remember seeing a YouTube that you had published quite some back.

143
00:20:49,000 --> 00:20:56,000
I think you were in Sri Lanka, somewhere overseas, about dependent origination.

144
00:20:56,000 --> 00:21:04,000
I have done a couple of these.

145
00:21:04,000 --> 00:21:10,000
Another thing, can you talk about right view?

146
00:21:10,000 --> 00:21:16,000
Sure.

147
00:21:16,000 --> 00:21:19,000
I have to think about that.

148
00:21:19,000 --> 00:21:23,000
There's different ways we're talking about right view.

149
00:21:23,000 --> 00:21:30,000
They can go over the different ways of looking at it.

150
00:21:30,000 --> 00:21:44,000
With the difference, different ways talking about the associated with, say, different sects of Buddhism,

151
00:21:44,000 --> 00:21:55,000
like the forced tradition, or what this is that, the way they define the nobelate-fold path,

152
00:21:55,000 --> 00:21:58,000
is that what you're saying?

153
00:21:58,000 --> 00:22:06,000
The different ways of talking about right view is because the Buddha talked about it in different ways.

154
00:22:06,000 --> 00:22:14,000
There's basically two kinds, mundane and nobel.

155
00:22:14,000 --> 00:22:19,000
A mundane right view is useful, but it's not enough.

156
00:22:19,000 --> 00:22:30,000
A mundane right view has to do with the love karma.

157
00:22:30,000 --> 00:22:46,000
I'll take a look at it and see because there is some right view of non-self.

158
00:22:46,000 --> 00:22:54,000
There is right view of the truth of suffering, or the formal truth.

159
00:22:54,000 --> 00:23:01,000
There is right view in terms of the karma.

160
00:23:01,000 --> 00:23:03,000
Those are three that I can think of.

161
00:23:03,000 --> 00:23:16,000
They're not exclusively different, but three ways of approaching right view.

162
00:23:16,000 --> 00:23:26,000
So, thank you all. That sounds like a good talk.

163
00:23:26,000 --> 00:23:30,000
You want me to do with the question? Is that why you're here?

164
00:23:30,000 --> 00:23:40,000
I was just going to ask about the four new trimmons in the Samaditi Suta,

165
00:23:40,000 --> 00:23:51,000
how they relate to dependent origination, because they believe that that kind of talks about putchutya,

166
00:23:51,000 --> 00:23:58,000
Samupada as well as the four new trimmons in the formal truth, and so on.

167
00:23:58,000 --> 00:24:01,000
You say the Samaditi Suta?

168
00:24:01,000 --> 00:24:11,000
Yeah. What are the four new trimmons?

169
00:24:11,000 --> 00:24:24,000
It was food, physical food, and then mental volition, so sankara, and then contact.

170
00:24:24,000 --> 00:24:34,000
It's not actually a tangent.

171
00:24:34,000 --> 00:24:40,000
And then there is passahara and miniyana-hara.

172
00:24:40,000 --> 00:24:41,000
Consciousness, right?

173
00:24:41,000 --> 00:24:42,000
Yeah.

174
00:24:42,000 --> 00:24:46,000
I didn't realize that came from the Samaditi Suta shows what a scholar I am.

175
00:24:46,000 --> 00:24:48,000
Which one is Samaditi?

176
00:24:48,000 --> 00:24:55,000
I think it's nine? Yeah, number nine.

177
00:24:55,000 --> 00:24:57,000
Yeah.

178
00:24:57,000 --> 00:25:02,000
I'm sorry, it was a Suta by Suta by Suta.

179
00:25:02,000 --> 00:25:11,000
Anyway, I'll give it a little bit.

180
00:25:11,000 --> 00:25:21,000
The arriving of craving there causes it, right?

181
00:25:21,000 --> 00:25:26,000
I think it says in the commentary that he's twenty-four different ways.

182
00:25:26,000 --> 00:25:29,000
Or maybe he even says something.

183
00:25:29,000 --> 00:25:33,000
In twenty-four different ways, there's some number of ways.

184
00:25:33,000 --> 00:25:38,000
It's the most number of times anyone's gone through the four new trits or something like that.

185
00:25:38,000 --> 00:25:47,000
Especially to relate to the topic that he's talking about back to the noble trits.

186
00:25:47,000 --> 00:25:52,000
I mean, Petitya Samapade is basically the four noble trits.

187
00:25:52,000 --> 00:25:56,000
It's kind of the four noble trits expanded upon.

188
00:25:56,000 --> 00:26:03,000
But even it, even Petitya Samapade is still a condensation of the Mahapatana.

189
00:26:03,000 --> 00:26:08,000
If you really want to know about causality, there are twenty-four,

190
00:26:08,000 --> 00:26:13,000
butchya, twenty-four conditionality.

191
00:26:13,000 --> 00:26:16,000
And it's much more complicated than just the lion.

192
00:26:16,000 --> 00:26:19,000
I mean, Petitya Samapade sounds like a lion, right?

193
00:26:19,000 --> 00:26:20,000
It sounds like it's in a chain.

194
00:26:20,000 --> 00:26:26,000
But it's not really. It's quite complicated.

195
00:26:26,000 --> 00:26:29,000
People try to think of it as simple as just,

196
00:26:29,000 --> 00:26:32,000
and some people say Petitya Samapade only relates to one moment.

197
00:26:32,000 --> 00:26:34,000
And it's only this life.

198
00:26:34,000 --> 00:26:38,000
It's nothing to do with past life, nothing to do with future life.

199
00:26:38,000 --> 00:26:51,000
Because, early, they tried to say that there's no Petitya Samapade that doesn't talk about past life.

200
00:26:51,000 --> 00:26:54,000
But it's quite, you know, the first,

201
00:26:54,000 --> 00:27:07,000
which Petitya Samkara is past life. It's just a way of expressing how we were born again and again based on ignorance.

202
00:27:07,000 --> 00:27:10,000
But ignorance is also involved with Tanha.

203
00:27:10,000 --> 00:27:12,000
So, it's Tanha, but...

204
00:27:12,000 --> 00:27:14,000
...waiting up Petitya Tanha.

205
00:27:14,000 --> 00:27:19,000
The way that it only gives rise to Tanha, there's a widget.

206
00:27:19,000 --> 00:27:26,000
But weight in it doesn't require a widget, not in this type.

207
00:27:26,000 --> 00:27:31,000
So, you experience weight in it even without a widget.

208
00:27:31,000 --> 00:27:36,000
But not originally. Originally, you needed a widget to be born.

209
00:27:36,000 --> 00:27:41,000
So, it's not linear.

210
00:27:41,000 --> 00:27:44,000
So, if you really want to learn about it, you should learn.

211
00:27:44,000 --> 00:27:50,000
Read the Matika of the Mahapatana.

212
00:27:50,000 --> 00:27:54,000
There's a really good chant that I actually put up on my website.

213
00:27:54,000 --> 00:27:55,000
It's... Burmese.

214
00:27:55,000 --> 00:27:57,000
It makes me think I was Burmese in the past life.

215
00:27:57,000 --> 00:28:01,000
Because the first time I heard this chant,

216
00:28:01,000 --> 00:28:03,000
I just just struck by it.

217
00:28:03,000 --> 00:28:06,000
I've never been struck by chanting before, but it was something so...

218
00:28:06,000 --> 00:28:09,000
...it just resonated with me in these monks.

219
00:28:09,000 --> 00:28:14,000
And Burmese had visiting Thailand, and they went around this...

220
00:28:14,000 --> 00:28:17,000
...the JT at the Endoisa tab.

221
00:28:17,000 --> 00:28:20,000
And I just had to know what that was.

222
00:28:20,000 --> 00:28:25,000
So, later I asked, would it be Sri Lankan monk?

223
00:28:25,000 --> 00:28:27,000
Burmese monk.

224
00:28:27,000 --> 00:28:30,000
And explained it to me.

225
00:28:30,000 --> 00:28:35,000
He said, oh, that's the Mahapatana.

226
00:28:35,000 --> 00:28:41,000
So, he had to put Joti, he had to hear to some people.

227
00:28:41,000 --> 00:28:50,000
So, that's the first one.

228
00:28:50,000 --> 00:28:51,000
So, that's the first one.

229
00:28:51,000 --> 00:28:52,000
He had to put Jai.

230
00:28:52,000 --> 00:28:54,000
When something is its cause.

231
00:28:54,000 --> 00:29:00,000
So, a cause of...

232
00:29:00,000 --> 00:29:07,000
...something is a cause of...

233
00:29:07,000 --> 00:29:16,000
...form together with its...

234
00:29:16,000 --> 00:29:19,000
...dhammas, I can't translate it.

235
00:29:19,000 --> 00:29:22,000
I used to know how this was translated.

236
00:29:22,000 --> 00:29:24,000
Something is a cause when it's...

237
00:29:24,000 --> 00:29:30,000
I mean, this is just very, very basic, very, very brief going through...

238
00:29:30,000 --> 00:29:32,000
...how something is a cause of something else.

239
00:29:32,000 --> 00:29:37,000
You have to read the translation.

240
00:29:37,000 --> 00:29:40,000
I'll definitely check that out.

241
00:29:40,000 --> 00:29:41,000
There is a...

242
00:29:41,000 --> 00:29:44,000
...there is a translation.

243
00:29:44,000 --> 00:29:53,000
Mahapatana, at least abbreviated, I think.

244
00:29:53,000 --> 00:29:55,000
That's all for tonight.

245
00:29:55,000 --> 00:29:57,000
Then, if there's talk by school.

246
00:29:57,000 --> 00:29:59,000
I'm going to head off.

247
00:29:59,000 --> 00:30:02,000
So, T.

248
00:30:02,000 --> 00:30:04,000
Manti.

249
00:30:04,000 --> 00:30:06,000
So, T is for hello.

250
00:30:06,000 --> 00:30:09,000
When we say goodbye, it's fine.

251
00:30:09,000 --> 00:30:11,000
I did.

252
00:30:11,000 --> 00:30:13,000
I tried to sort that out.

253
00:30:13,000 --> 00:30:16,000
I wasn't successful the other day.

254
00:30:16,000 --> 00:30:18,000
You know, honestly, it's probably okay.

255
00:30:18,000 --> 00:30:19,000
Just the tradition.

256
00:30:19,000 --> 00:30:21,000
I mean, it's only the tradition I know.

257
00:30:21,000 --> 00:30:23,000
But it's the same, hello.

258
00:30:23,000 --> 00:30:24,000
Sadhguru.

259
00:30:24,000 --> 00:30:25,000
Sadhguru.

260
00:30:25,000 --> 00:30:26,000
Sadhguru.

261
00:30:26,000 --> 00:30:27,000
Sadhguru.

262
00:30:27,000 --> 00:30:28,000
Sadhguru.

263
00:30:28,000 --> 00:30:29,000
Banti.

264
00:30:29,000 --> 00:30:30,000
Good night.

265
00:30:30,000 --> 00:30:31,000
You know.

266
00:30:31,000 --> 00:30:58,000
Thank you very much.

