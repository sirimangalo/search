WEBVTT

00:00.000 --> 00:07.040
other types of meditation, kune, samata and vipassana.

00:08.960 --> 00:16.240
Question, I started with samata meditation as I read a lot that you have to call me a mind before

00:16.240 --> 00:24.480
starting to practice vipassana. What is your interpretation of the differences between these two

00:24.480 --> 00:37.040
and which should be practiced first? Answer, samata means tranquility, vipassana means seeing clearly.

00:38.080 --> 00:44.240
The Buddha used these terms together and individually as qualities of mind.

00:45.040 --> 00:51.600
When you talk about samata meditation as something separate from vipassana meditation,

00:51.600 --> 01:00.400
you are taking the discussion into a post-canonical realm. You are taking it out of the realm of what

01:00.400 --> 01:09.840
the Buddha actually said and into the realm of interpretation. I follow the orthodox interpretation

01:09.840 --> 01:18.160
that does talk about samata meditation and vipassana meditation, so I do not mind talking about this.

01:18.160 --> 01:27.600
For those people who advise to practice coming your mind first, samata meditation and then practicing

01:27.600 --> 01:37.440
to see clearly after vipassana meditation, this means first entering into a trance wherein you can see

01:37.440 --> 01:47.520
one thing very clearly. This is only possible if that one thing is a concept. Since concepts

01:48.080 --> 01:56.800
are lasting entities, the mind can focus on a slang. Understanding the distinction between

01:56.800 --> 02:06.000
reality and concept is important. It allows us to determine whether a meditation practice will

02:06.000 --> 02:15.600
lead to a trance state or a clear understanding of reality. Focusing on concepts will not help you

02:15.600 --> 02:22.800
understand reality because you are focusing on something you have created in your mind.

02:23.680 --> 02:30.480
It is not real. It does not have any of the qualities of ultimate reality.

02:30.480 --> 02:39.440
It is not the same as watching your stomach rise and fall because when you watch your stomach rise

02:39.440 --> 02:48.160
and fall, it changes constantly. The movements of the stomach are impermanent on satisfying

02:48.160 --> 02:57.520
and uncontrollable. Watching the stomach helps you to see reality as it is and that is difficult.

02:57.520 --> 03:07.200
It is much easier and pleasant to create a concept in your mind. For example, you can close your eyes

03:07.760 --> 03:16.400
and utter your third eye in the center of your forehead. Imagine a collar and you say to yourself

03:16.400 --> 03:26.480
blue, blue, or red, red, or white, white. Just focusing on the collar you have imagined.

03:27.760 --> 03:33.680
Once you can fix your mind, only on the collar, thinking of nothing else,

03:34.880 --> 03:40.560
that is considered to be an attainment of the fruit of samata meditation.

03:40.560 --> 03:50.400
Many religious traditions, especially in India, make use of this sort of practice for the purpose

03:50.400 --> 03:59.120
of gaining spiritual powers like remembering past lives, reading people's minds, seeing

03:59.120 --> 04:09.040
heaven and hell, seeing the future, and leaving your body. To make use of samata meditation

04:09.040 --> 04:16.720
for the subsequent development of Vipassana, you simply apply the principles of

04:16.720 --> 04:26.640
Satipatana practice to the samata experiences. For example, you can note the focused mind

04:26.640 --> 04:40.240
as focused, focused. If the experience is pleasurable, you can note happy, happy. If you feel calm,

04:40.240 --> 04:49.760
calm, calm. If the experience involves lights or colors, you note seeing, seeing,

04:49.760 --> 05:00.000
etc. and other results of the mindful interaction, the experience will change from seeing the stable,

05:00.640 --> 05:08.720
satisfying and controllable concept to the impermanent, unsatisfying and uncontrollable reality.

05:10.320 --> 05:15.680
Because samata meditation involves the cultivation of poor,

05:15.680 --> 05:21.440
fundamental faculties, when you switch to Vipassana meditation,

05:22.480 --> 05:28.560
your mind is readily able to see clearly the nature of reality.

05:30.000 --> 05:35.440
For someone who does not take the time to practice samata meditation first,

05:36.160 --> 05:43.920
Vipassana meditation can be a challenge because the mind does not have the prior training

05:43.920 --> 05:51.360
or on more pleasurable objects. You have to face pain in the body,

05:52.240 --> 06:00.720
distractions in the mind and all kinds of emotions without any of the peace and calm of samata

06:00.720 --> 06:10.880
meditation. For the most part, this means only that the meditation practice is less pleasurable,

06:10.880 --> 06:18.560
not slower or less effective. In fact, by starting with Vipassana meditation,

06:19.440 --> 06:27.120
one avoids the potential pitfalls of becoming complacent or considered about,

06:27.120 --> 06:36.080
once samata-based meditative attainment, as their training of the mind begins with ultimate

06:36.080 --> 06:44.160
reality directly. The difference between the Vipassana meditation and samata meditation is quite

06:44.160 --> 06:53.440
profound. Samata meditation is for the purpose of calming the mind where as Vipassana meditation

06:53.440 --> 07:01.840
is for the purpose of seeing clearly. This does not mean that Vipassana meditation does not lead

07:01.840 --> 07:09.600
to tranquility. You should find that through the practice of Vipassana meditation,

07:09.600 --> 07:19.920
you are much calmer in general. Vipassana meditation is differentiated from samata meditation not

07:19.920 --> 07:29.280
because it does not involve tranquility, but because tranquility is not the goal. Whether you feel calm

07:29.280 --> 07:38.560
or not is not practically important. All that is important is that you see your state of calm

07:38.560 --> 07:47.520
or distress clearly. Samata meditation cannot lead directly to enlightenment

07:48.240 --> 07:55.680
because it is not focused on reality. The only way that it can be used to attain enlightenment

07:55.680 --> 08:05.360
is if you follow it up as stated with Vipassana meditation. The strength of mind gained from

08:05.360 --> 08:12.640
practicing samata meditation allows you to see clearer than you would have otherwise.

08:14.160 --> 08:22.160
Since the same can be said of developing mental fortitude, starting with Vipassana meditation directly,

08:22.160 --> 08:30.400
the only real benefit to practicing samata meditation first is the state of peace and calm

08:30.400 --> 08:36.480
and maybe spiritual powers if you dedicate yourself to it to a great degree.

08:39.120 --> 08:46.320
It is in fact clear from the text that starting with Vipassana is the quicker option of the two.

08:46.320 --> 08:50.480
Among one's approach to Buddha and said,

08:51.280 --> 09:02.320
I am old. I do not have much time left and my memory is not good. I am not able to learn everything.

09:03.040 --> 09:11.840
Please teach me the basics of the path. The Buddha told him that first he should establish himself

09:11.840 --> 09:20.960
in an intellectual acceptance of right view and purify his morality to make sure he is ethical

09:20.960 --> 09:29.040
and moral in this behavior. Once he has done that, he has done enough to begin practicing

09:29.040 --> 09:37.440
the four foundations of mindfulness. There is no talk about first focusing the mind or

09:37.440 --> 09:47.280
entering into any meditative state before focusing on reality. In mindfulness, one

09:47.280 --> 09:56.640
accesses meditative states naturally, entering into them based on reality. Even just focusing

09:56.640 --> 10:04.480
under rising falling can be considered an absorbed state for the moment that one is observing it.

10:04.480 --> 10:10.960
When you say stepping right, stepping up, invoking meditation,

10:12.000 --> 10:19.760
if your mind is clearly aware of the movement, you are in an absorbed state for that moment.

10:20.960 --> 10:31.840
So you are cultivating both samata and Vipassana at once. You are tranquil and you are also seeing

10:31.840 --> 10:40.880
flirty.

