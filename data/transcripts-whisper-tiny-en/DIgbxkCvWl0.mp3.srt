1
00:00:00,000 --> 00:00:29,840
Good evening, everyone.

2
00:00:29,840 --> 00:00:43,840
We're broadcasting live, March 24th.

3
00:00:43,840 --> 00:00:50,840
Today's quote is from the Wokkani Sutta.

4
00:00:50,840 --> 00:01:00,840
So Wokkali was unhappy as a layperson because

5
00:01:00,840 --> 00:01:11,840
A is very happy to see the Buddha

6
00:01:11,840 --> 00:01:20,840
And as a layperson he didn't have the opportunity to see the Buddha often.

7
00:01:20,840 --> 00:01:25,840
The Buddha was of course very beautiful.

8
00:01:25,840 --> 00:01:37,840
So Wokkali was one of these people who was enamored by the Buddha's features.

9
00:01:37,840 --> 00:01:46,840
And so he would go to see the Buddha quite often but he wasn't happy because

10
00:01:46,840 --> 00:01:53,840
as a layperson he didn't have the opportunity to see the Buddha as often as he liked.

11
00:01:53,840 --> 00:01:57,840
So he became a monk.

12
00:01:57,840 --> 00:02:10,840
But this Sutta says that he was actually quite sick.

13
00:02:10,840 --> 00:02:15,840
And so he asked his fellow monks to go find the Buddha.

14
00:02:15,840 --> 00:02:25,840
Tell them I'm sick and please see if they'll come visit me.

15
00:02:25,840 --> 00:02:30,840
And the Buddha comes and he's ecstatic and he tries to get up.

16
00:02:30,840 --> 00:02:32,840
And the Buddha says don't get up.

17
00:02:32,840 --> 00:02:33,840
It's okay.

18
00:02:33,840 --> 00:02:42,840
I'll sit down when you see it.

19
00:02:42,840 --> 00:02:54,840
And having sat down here he said to him he gave the standard question.

20
00:02:54,840 --> 00:03:06,840
Can you stand it Wokkali?

21
00:03:06,840 --> 00:03:17,840
Can you bear it?

22
00:03:17,840 --> 00:03:22,840
Are you able to do with the sickness?

23
00:03:22,840 --> 00:03:26,840
Can you see the Kachinukhivedana, Bhatty, Kamanthi?

24
00:03:26,840 --> 00:03:29,840
No, a big Kamanthi.

25
00:03:29,840 --> 00:03:31,840
How is it?

26
00:03:31,840 --> 00:03:35,840
Is the pain for feelings going away and not getting anything?

27
00:03:35,840 --> 00:03:39,840
Is it decreasing or not increasing?

28
00:03:39,840 --> 00:03:44,840
Bhatty, Kamanthi, Kamanthi, no, a big Kamanthi.

29
00:03:44,840 --> 00:03:49,840
Are they reducing, not increasing?

30
00:03:49,840 --> 00:03:54,840
So he's asking, are you well, basically?

31
00:03:54,840 --> 00:04:02,840
And Wokkali says Nami, Bhante, Kamanthi, and Nayaapanyam.

32
00:04:02,840 --> 00:04:05,840
I can't bear it.

33
00:04:05,840 --> 00:04:08,840
I can't stand it.

34
00:04:08,840 --> 00:04:22,840
My pain for feelings are getting worse, not decreasing, getting stronger, not weaker.

35
00:04:22,840 --> 00:04:31,840
It's the Buddha realizes why he's possibly not going to get over this.

36
00:04:31,840 --> 00:04:37,840
He says, well then, Wokkali, do you have anything you regret?

37
00:04:37,840 --> 00:04:43,840
Do you have any worries or regrets about the past, anything that's bothering you?

38
00:04:43,840 --> 00:04:59,840
And he says, yes, Bhante, I have some unresolved many, not a few, not a few worries.

39
00:04:59,840 --> 00:05:04,840
And he says, well, Wokkali, is it in regards to your morality?

40
00:05:04,840 --> 00:05:10,840
And he says, no, I have no problem with my morality.

41
00:05:10,840 --> 00:05:16,840
He says, well, if it's not about your morality, then what are you worried about?

42
00:05:16,840 --> 00:05:24,840
Making the point that really, anything else you could be worried about is not really important at this point.

43
00:05:24,840 --> 00:05:34,840
You're sick on your deathbed. Is your morality pure? Is your mind pure? How good intention?

44
00:05:34,840 --> 00:05:40,840
What could be wrong? He says, well, I haven't seen the Buddha in a long time.

45
00:05:40,840 --> 00:05:45,840
I don't get to see him often, and being sick, I'm not able to go and see the Buddha in there.

46
00:05:45,840 --> 00:05:53,840
I want to go and see you, and my body doesn't let me do it.

47
00:05:53,840 --> 00:05:59,840
And how does the Buddha respond? He says, a long Wokkali, enough Wokkali.

48
00:05:59,840 --> 00:06:19,840
What good is it for you to see this body?

49
00:06:19,840 --> 00:06:29,840
Wokkali had it all wrong. He thought, thinking about the Buddha and thinking about his physical form.

50
00:06:29,840 --> 00:06:34,840
It's not what thinking about the Buddha is.

51
00:06:34,840 --> 00:06:42,840
This is why we kind of have sort of an ambivalence towards images.

52
00:06:42,840 --> 00:06:50,840
People think a Buddha image allows you to think about the Buddha.

53
00:06:50,840 --> 00:06:58,840
And yes, it can, but only as a starting point. The image itself isn't the Buddha.

54
00:06:58,840 --> 00:07:06,840
The image can remind you of the Buddha, and when you see the Buddha, all right, the Buddha, and then you start thinking about the qualities of the Buddha.

55
00:07:06,840 --> 00:07:11,840
Not the physical qualities, the mental quality.

56
00:07:11,840 --> 00:07:16,840
He was perfectly self enlightened.

57
00:07:16,840 --> 00:07:21,840
Endowed with both knowledge of the truth and conduct according to the truth.

58
00:07:21,840 --> 00:07:29,840
He practiced to the priest, the incomparable trainer of humans and angels.

59
00:07:29,840 --> 00:07:34,840
Well gone, all these kind of things worthy of gifts and offerings.

60
00:07:34,840 --> 00:07:43,840
Just a word, worthy sort of person. Blessed enlightened.

61
00:07:43,840 --> 00:08:12,840
So the Buddha says, yo kovakali dam man basate so man basate is a famous, fairly famous quote, who sees the dam mases me.

62
00:08:12,840 --> 00:08:19,840
to see the Buddha, you have to see the done mind.

63
00:08:19,840 --> 00:08:23,840
It's one in this game.

64
00:08:23,840 --> 00:08:27,840
And so he tries to teach walkily about the truth

65
00:08:27,840 --> 00:08:30,840
because walkily has this idea of the beautiful body

66
00:08:30,840 --> 00:08:33,840
of the Buddha, it's being something pleasant, something

67
00:08:33,840 --> 00:08:36,340
he can find happiness in.

68
00:08:36,340 --> 00:08:38,840
And he says, what do you think?

69
00:08:38,840 --> 00:08:44,840
It's this form permanent or impermanent.

70
00:08:44,840 --> 00:08:48,840
So he has to admit, well, a Nitham Bhakai.

71
00:08:48,840 --> 00:08:52,840
This is what you can look at this as a form of reporting.

72
00:08:52,840 --> 00:08:57,840
I think we do these courses, and we meet with the teacher once a day.

73
00:08:57,840 --> 00:08:59,840
The Buddha did this as well.

74
00:08:59,840 --> 00:09:00,840
Yes, the Buddha.

75
00:09:00,840 --> 00:09:02,840
Tunging, manya, see, walkily.

76
00:09:02,840 --> 00:09:05,840
Rupang, Nithang, wah, Nithang, wah.

77
00:09:05,840 --> 00:09:08,840
What do you think of form? Is it permanent or impermanent?

78
00:09:08,840 --> 00:09:12,840
Stable or unstable, constant or incoherent?

79
00:09:12,840 --> 00:09:17,840
Nithang Bhante, and it's changing all the time.

80
00:09:17,840 --> 00:09:20,840
Not only are we getting older and our bodies changing,

81
00:09:20,840 --> 00:09:24,840
but the experience of the body only arises in senses.

82
00:09:24,840 --> 00:09:30,840
What we think of as the body is made up of experiences that arise in senses.

83
00:09:30,840 --> 00:09:35,840
Those experiences are changing all the time.

84
00:09:35,840 --> 00:09:36,840
So it's unstable.

85
00:09:36,840 --> 00:09:39,840
You know, there's nothing that you can hold on to.

86
00:09:39,840 --> 00:09:44,840
This is why it can change so quickly and suddenly the form is changed.

87
00:09:44,840 --> 00:09:45,840
Cut yourself.

88
00:09:45,840 --> 00:09:46,840
You cut yourself.

89
00:09:46,840 --> 00:09:47,840
You hurt yourself.

90
00:09:47,840 --> 00:09:49,840
You get sick.

91
00:09:49,840 --> 00:09:51,840
The whole experience can change.

92
00:09:51,840 --> 00:09:54,840
And then you're at a loss because you weren't prepared for that.

93
00:09:54,840 --> 00:09:56,840
You're clinging to something this way.

94
00:09:56,840 --> 00:09:59,840
You want to see the beautiful body of the Buddha.

95
00:09:59,840 --> 00:10:03,840
Then you get sick and you can't come and see the woman.

96
00:10:03,840 --> 00:10:10,840
And those are the root part that he wanted to see.

97
00:10:10,840 --> 00:10:11,840
And he says, what is so?

98
00:10:11,840 --> 00:10:13,840
What is young Bhante, Tandukkong?

99
00:10:13,840 --> 00:10:16,840
This is actually from the Anatala canister, as well.

100
00:10:16,840 --> 00:10:17,840
It's a standard teaching.

101
00:10:17,840 --> 00:10:23,840
Young Bhante, Tandukkong, who comes to Kanga.

102
00:10:23,840 --> 00:10:24,840
What is impermanent?

103
00:10:24,840 --> 00:10:28,840
Is that suffering or is it happiness?

104
00:10:28,840 --> 00:10:38,840
So this is the idea that the inconsistency of it means it can't fulfill your desire.

105
00:10:38,840 --> 00:10:41,840
It won't go into accordance with your desire.

106
00:10:41,840 --> 00:10:44,840
If you want it to be, it doesn't come.

107
00:10:44,840 --> 00:10:48,840
You want it to go, it doesn't go.

108
00:10:48,840 --> 00:10:50,840
It's not in sync with our desires.

109
00:10:50,840 --> 00:10:51,840
So we suffer as a result.

110
00:10:51,840 --> 00:10:59,840
We get stressed as well.

111
00:10:59,840 --> 00:11:03,840
This is where young Bhante, Tandukkong, who comes to Kanga,

112
00:11:03,840 --> 00:11:07,840
who comes to Kanga, who comes to Kanga, who comes to Kanga.

113
00:11:07,840 --> 00:11:17,840
What is impermanent and suffering and subject to alteration, subject to change?

114
00:11:17,840 --> 00:11:25,840
Is it proper for that to say, to say about that?

115
00:11:25,840 --> 00:11:28,840
This is mine.

116
00:11:28,840 --> 00:11:30,840
This is me.

117
00:11:30,840 --> 00:11:43,840
This is the three types of clinging with views, clinging with conceit, clinging with craving.

118
00:11:43,840 --> 00:11:48,840
It's clinging with craving.

119
00:11:48,840 --> 00:11:51,840
Something to be clinging to because we want them.

120
00:11:51,840 --> 00:11:59,840
So it must be this, I am, that's clinging with conceit.

121
00:11:59,840 --> 00:12:01,840
So we think of I am this, I am that.

122
00:12:01,840 --> 00:12:12,840
I'm strong, I'm smart, I'm beautiful, I'm ugly, I'm stupid, etc.

123
00:12:12,840 --> 00:12:16,840
We can.

124
00:12:16,840 --> 00:12:23,840
This is conceit, clinging to deep, to characteristics.

125
00:12:23,840 --> 00:12:28,840
Asomeata, asomeata.

126
00:12:28,840 --> 00:12:31,840
This is myself, that's a new clinging with view.

127
00:12:31,840 --> 00:12:36,840
So we cling to the idea that this is me, this is an entity, this is a thing.

128
00:12:36,840 --> 00:12:41,840
The self exists, the soul exists, I can control things,

129
00:12:41,840 --> 00:12:45,840
I can control my body because this body is myself.

130
00:12:45,840 --> 00:12:50,840
Those are views, we cling to reviews.

131
00:12:50,840 --> 00:13:14,840
A tongue mama is so hummus me, isomeata.

132
00:13:14,840 --> 00:13:27,840
Ask them, ask him, well in the middle of the left,

133
00:13:27,840 --> 00:13:33,840
and walk away, ask his friends to lift them up and carry him to Isimila.

134
00:13:33,840 --> 00:13:40,840
Isimila.

135
00:13:40,840 --> 00:13:41,840
Because he was ready to die.

136
00:13:41,840 --> 00:13:44,840
And he said, how can one like me think of dying among the house?

137
00:13:44,840 --> 00:13:47,840
He didn't want to die in the house.

138
00:13:47,840 --> 00:13:49,840
He died as a monk.

139
00:13:49,840 --> 00:13:53,840
So they took him to Isimila.

140
00:13:53,840 --> 00:14:15,840
Anyway, let's get on to a more or less than that.

141
00:14:15,840 --> 00:14:24,840
Some lesson here, but I mean I think the overall lesson is about the teaching, not the teacher, right?

142
00:14:24,840 --> 00:14:41,840
It's easy to focus on person, place and thing without focusing on our own wisdom and our own understanding.

143
00:14:41,840 --> 00:14:44,840
Nobody else can lead us to enlightenment.

144
00:14:44,840 --> 00:14:49,840
It's kind of in general, the attachment to form.

145
00:14:49,840 --> 00:15:03,840
You can attach to the form of meditation and say, okay, I have to do an hour of meditation or I have to do so many hours of meditation without actually meditating.

146
00:15:03,840 --> 00:15:09,840
You're so focused on the idea of the form that you forget the present moment.

147
00:15:09,840 --> 00:15:17,840
So it goes with all kinds of form, you can focus so much on a teacher or even on their words.

148
00:15:17,840 --> 00:15:25,840
You can like what they say without actually keeping it in mind, putting it into practice, that kind of thing.

149
00:15:25,840 --> 00:15:30,840
I mean, it's not to say that we all do this, but we have to be careful not to.

150
00:15:30,840 --> 00:15:37,840
People like walk a leg out into an extreme, watching them but are not listening to his teachings.

151
00:15:37,840 --> 00:15:40,840
Eventually he became enlightened.

152
00:15:40,840 --> 00:15:48,840
But something about teachers and people as well, you know, a lot of the Buddhist teaching is about.

153
00:15:48,840 --> 00:15:57,840
I think it was a couple of nights ago not being critical of others, looking at your own faults.

154
00:15:57,840 --> 00:16:02,840
Sometimes we go to a monastery or a meditation centre and all we can see are the faults of others.

155
00:16:02,840 --> 00:16:06,840
It's common. I mean, a lot, there's a lot of corruption in monasteries.

156
00:16:06,840 --> 00:16:13,840
It's not without it's justification but it doesn't really help.

157
00:16:13,840 --> 00:16:21,840
On the other hand, we sometimes put people up on a pedestal and when we find out they're human, well.

158
00:16:21,840 --> 00:16:29,840
We lose, we become discouraged, which, you know, we really shouldn't be focused on the teachings.

159
00:16:29,840 --> 00:16:32,840
They're much more important.

160
00:16:32,840 --> 00:16:38,840
You can ask yourself where these teachings go ahead and if the teachings are good, stick by them.

161
00:16:38,840 --> 00:16:44,840
See the dhamma, don't worry too much about seeing the Buddha.

162
00:16:44,840 --> 00:16:55,840
So, yeah, simple teaching, something to come out.

163
00:16:55,840 --> 00:16:59,840
I can post the thing out here.

164
00:16:59,840 --> 00:17:03,840
I'll welcome if anybody has any questions.

165
00:17:03,840 --> 00:17:10,840
I understand that I'm quite limiting my online presence and I think some people are complaining.

166
00:17:10,840 --> 00:17:14,840
They want to ask questions and they can't come on here.

167
00:17:14,840 --> 00:17:16,840
I understand that.

168
00:17:16,840 --> 00:17:23,840
I just don't have a means of sort of filtering.

169
00:17:23,840 --> 00:17:30,840
I'm doing the best I can but I think they can't help everyone.

170
00:17:30,840 --> 00:17:39,840
So, if you really want help from me, you've got to make the effort to come here or sign up for one of the slots

171
00:17:39,840 --> 00:17:48,840
or an online meditation course and actually do some meditating.

172
00:17:48,840 --> 00:17:52,840
Anyway, I don't know if there are any questions.

173
00:17:52,840 --> 00:17:55,840
I'm not going to answer if you come on the hangout.

174
00:17:55,840 --> 00:17:58,840
Otherwise, it's a good night.

175
00:17:58,840 --> 00:18:05,840
It's going to get fairly hectic the next week and a half.

176
00:18:05,840 --> 00:18:14,840
Next, well, next 20, 30 years probably.

177
00:18:14,840 --> 00:18:17,840
Yeah, the next week and a half probably is going to be a bit rough.

178
00:18:17,840 --> 00:18:23,840
I may not be able to broadcast every night.

179
00:18:23,840 --> 00:18:26,840
Okay, I'm going to say good night.

180
00:18:26,840 --> 00:18:27,840
Good night everyone.

181
00:18:27,840 --> 00:18:32,840
Thank you for coming out and meditating together.

182
00:18:32,840 --> 00:18:35,840
Have a good night.

