1
00:00:00,000 --> 00:00:08,680
Okay, so welcome back to our study of the Dhamapada, today we continue on with first number 41 which reads as follows

2
00:00:30,000 --> 00:00:49,000
a long indeed what the a young guy, oh this body, but doing a T.C. city will lie on the earth, chudhau, chudhau means discarded,

3
00:00:49,000 --> 00:00:58,000
apetawinyanal without consciousness or with consciousness having left, consciousness having departed from it.

4
00:00:58,000 --> 00:01:10,000
Niratang, what, niratang, what, keling garan, as useless as a log, as useless as a dead piece of wood or a log.

5
00:01:10,000 --> 00:01:32,000
So, this is seemingly a little bit depressing, versatile, referring to death before long you will be dead, as I say, reminder of the negative aspects of life.

6
00:01:32,000 --> 00:01:50,000
So, this was told in regards to, it's a fairly famous story and it's a fairly famous verse, this verse is used as a reminder of the inevitability of death.

7
00:01:50,000 --> 00:02:03,000
And so, it's often used at giving discourses and used in ceremonies and in good, in traditional Buddhism.

8
00:02:03,000 --> 00:02:28,000
So, the story goes that there was a man in Sawati who, having listened to the Buddha's teaching, became very convinced that the Buddha's greatness and the greatness of the teaching to the point that he decided to go forth.

9
00:02:28,000 --> 00:02:42,000
Niratang, the Buddha, the Buddha, the Buddha, the Buddha, the Buddha, the Buddha, the Buddha, the Buddha, the Buddha.

10
00:02:42,000 --> 00:02:56,000
Now, in a very short time, he became quite sick for whatever reason he contracted some sort of illness and had these boils all over his body, kind of like the chicken box or something.

11
00:02:56,000 --> 00:03:08,000
And they grew bigger and bigger and believe it's as eventually his lately burst and he was covered in this filthy fluid.

12
00:03:08,000 --> 00:03:20,000
And even eventually his bones broke, you know, just some horrible sickness that totally destroyed his body to the point where he was about to die.

13
00:03:20,000 --> 00:03:29,000
But he became so violent and putrid that the monks didn't want to take care of him, so they left him alone.

14
00:03:29,000 --> 00:03:36,000
And he was lying on his bed in his own urine and feces without anyone to look after him.

15
00:03:36,000 --> 00:04:01,000
Now, the Buddha, the commentary here says that the Buddha, it's his nature, it's his way to twice a day to look out into the world around him to see who would most benefit from his teaching.

16
00:04:01,000 --> 00:04:13,000
So, sometimes he would look out at the whole universe looking around through the whole of the earth and the heavens and so on.

17
00:04:13,000 --> 00:04:22,000
And then all the way back to where he was staying, or sometimes he would start in the monastery and work his way out.

18
00:04:22,000 --> 00:04:31,000
And whoever he, his mind, alighted upon as someone who could benefit from his teachings.

19
00:04:31,000 --> 00:04:38,000
He would make his way to them and give them the teaching that they needed.

20
00:04:38,000 --> 00:04:50,000
So, on this day, or on one day, when near the point where this monk was getting closer to death, the Buddha saw him in his mind's eye

21
00:04:50,000 --> 00:04:59,000
and knew that besides the Buddha himself, there was no one else who was willing to be his refuge or who could be his refuge.

22
00:04:59,000 --> 00:05:03,000
And so he headed out into the monastery.

23
00:05:03,000 --> 00:05:12,000
This was in the same monastery as the Buddha was staying and he walked out and he first went to the place where they lit fires.

24
00:05:12,000 --> 00:05:19,000
Especially designated place in the monastery for lighting fires and he lit fire and boiled some water.

25
00:05:19,000 --> 00:05:31,000
And he brought the water to this monk's room and proceeded to, or began to help the monk to wash himself.

26
00:05:31,000 --> 00:05:35,000
And then when the monks, of course, when they saw the Buddha come and they said, please, I don't know, let us do that.

27
00:05:35,000 --> 00:05:45,000
And so they helped with the monks, they were able to take his robes off and wash his robes and wash him and wash the bed.

28
00:05:45,000 --> 00:05:49,000
And lie him back down in a clean robe.

29
00:05:49,000 --> 00:05:52,000
And then the Buddha gave him this teaching.

30
00:05:52,000 --> 00:05:58,000
So, this is a teaching specifically given to a very sick person.

31
00:05:58,000 --> 00:06:07,000
And you might even think that it's a little bit of a specific teaching.

32
00:06:07,000 --> 00:06:16,000
So, it's a dedicated specifically for someone who is sick or someone who is dying.

33
00:06:16,000 --> 00:06:27,000
But I think I'd like to talk a little bit about this verse and how, as usual, how it relates to our practice.

34
00:06:27,000 --> 00:06:32,000
But I think it's an important verse to understand.

35
00:06:32,000 --> 00:06:40,000
It's important to understand that it has a broader reach than simply dealing with sick people.

36
00:06:40,000 --> 00:06:48,000
This very short and brief teaching is very brief teaching in the Buddha.

37
00:06:48,000 --> 00:07:00,000
It happens to be one of the major reasons why people begin to practice the Buddha's teaching.

38
00:07:00,000 --> 00:07:07,000
So, we ask ourselves, what was the use of this teaching at that time?

39
00:07:07,000 --> 00:07:10,000
How is it that this teaching helped that monk?

40
00:07:10,000 --> 00:07:16,000
Because according to the text here, he was able to become an arrow hunt as a result of listening to this teaching in Buddha.

41
00:07:16,000 --> 00:07:21,000
Just this very simple teaching was enough to set him on the path to become enlightened.

42
00:07:21,000 --> 00:07:26,000
So, you would understand that this was the beginning for that monk.

43
00:07:26,000 --> 00:07:33,000
And it's just as it's the beginning for all of us on our path to the realization of the truth.

44
00:07:33,000 --> 00:07:42,000
If we think back to the Buddha's story himself, one of the reasons why the Buddha went forth is because he saw a very sick person.

45
00:07:42,000 --> 00:07:49,000
And he realized that sickness was something that he himself was subject to as well.

46
00:07:49,000 --> 00:07:53,000
He saw a very old person, he saw a very sick person, he saw a dead person.

47
00:07:53,000 --> 00:08:00,000
And these were three things that moved him to the point of wanting to find a way out of suffering.

48
00:08:00,000 --> 00:08:16,000
To move him to realize that there was no point in pursuing sensual pleasures or things outside of the body or even pursuing the wellness of the body itself.

49
00:08:16,000 --> 00:08:29,000
Even pursuing life itself. There was no point in trying to extend life because eventually it would be offered not.

50
00:08:29,000 --> 00:08:37,000
And so, the first important way to understand this first is as an impetus for us to begin the practice.

51
00:08:37,000 --> 00:08:49,000
So, here we have this monk who was feeling useless, feeling like perhaps that maybe feeling afraid or feeling great suffering and depression because of his sickness.

52
00:08:49,000 --> 00:09:03,000
Feeling like he was now unable to practice the Buddha's teaching until the Buddha reminded him of the importance of now of putting the teaching into practice.

53
00:09:03,000 --> 00:09:14,000
And this allowed the monk to approach things in a different way to begin to look at the body and the whole world as being not self.

54
00:09:14,000 --> 00:09:19,000
He's not belonging to us and to begin to give it up.

55
00:09:19,000 --> 00:09:24,000
I says, how the Buddha began his quest to give up attachment.

56
00:09:24,000 --> 00:09:36,000
And it's a very common reason for people to give up the world or to begin meditation practices when they get sick or when they realize that sickness,

57
00:09:36,000 --> 00:09:56,000
all age sickness and death are an inevitable part of life. For some people this can be a great crisis to realization that one day they are going to have to die and all the people that they love and all the things that they care for are going to leave them.

58
00:09:56,000 --> 00:10:10,000
And it also happens to be the beginning of a much longer teaching which gets closer and closer and closer to a direct realization of impermanence, suffering and on self.

59
00:10:10,000 --> 00:10:23,000
So, this is really the beginning of the practice or one of the important beginnings of people ask, why should I practice meditation? Why should I torture myself or why should I be concerned about the purification of the mind,

60
00:10:23,000 --> 00:10:45,000
what understanding the mind, why should I be concerned about trying to let go of attachment? Why shouldn't I keep clinging to these things or pursuing worldly pleasures and so on?

61
00:10:45,000 --> 00:10:58,000
So, if this sort of teaching that really answers this question for us and makes us ask that turns us away from our pursuit of central pleasures,

62
00:10:58,000 --> 00:11:06,000
because you begin to realize that let alone those things outside of us that we might pursue.

63
00:11:06,000 --> 00:11:17,000
The things outside of us, even our own body, our own being, we can't say that it belongs to us.

64
00:11:17,000 --> 00:11:25,000
In the end this is the meaning of Chudho, Abetah, Vinyan, the mind will eventually be gone from this body.

65
00:11:25,000 --> 00:11:31,000
This very body, this very being that we hold to be ours, we hold to be ourselves.

66
00:11:31,000 --> 00:11:39,000
This one thing that we cling to more than anything in the world, even this one thing is in no way ours,

67
00:11:39,000 --> 00:11:44,000
is in no way under our control, it's not something that we can keep.

68
00:11:44,000 --> 00:11:50,000
At best you can say we're renting it at a high price, having to feed and to close and to care for it,

69
00:11:50,000 --> 00:11:53,000
and having to suffer a lot because of it.

70
00:11:53,000 --> 00:11:58,000
So, we're renting it, kind of like renting this thing because we think it's going to bring us pleasure,

71
00:11:58,000 --> 00:12:07,000
or we're trying to find pleasure based with it, until we realize that actually it's not worth the cost,

72
00:12:07,000 --> 00:12:14,000
that it's actually not something that is going to bring lasting peace and happiness.

73
00:12:14,000 --> 00:12:18,000
It's not something that we can never fix, that we can never hold on to.

74
00:12:18,000 --> 00:12:28,000
In fact, the nature of reality is nothing can ever possibly be so.

75
00:12:28,000 --> 00:12:33,000
There's nothing in this universe which when clung to will lead to happiness.

76
00:12:33,000 --> 00:12:40,000
The nature of reality, of objects, of possessions, is that they are temporary,

77
00:12:40,000 --> 00:12:49,000
that they come and they go, that are all of the work that we put into, keeping them and maintaining them.

78
00:12:49,000 --> 00:12:58,000
It's in the end, leads to no lasting and sustainable purpose due to the change,

79
00:12:58,000 --> 00:13:01,000
and the inevitability of losing everything.

80
00:13:01,000 --> 00:13:06,000
So, let alone all of the things in the world, this is very body that we have.

81
00:13:06,000 --> 00:13:08,000
It's not even ourselves.

82
00:13:08,000 --> 00:13:13,000
Body is really the key possession.

83
00:13:13,000 --> 00:13:19,000
So, when we talk about the body as being not tough as the body, it's not being belonging to us.

84
00:13:19,000 --> 00:13:26,000
We also, in extent by extension, understand the rest of the universe, the rest of the world to be so.

85
00:13:26,000 --> 00:13:37,000
When the body is not subject to our control, how could we possibly find control and stability in the things outside of the body?

86
00:13:37,000 --> 00:13:44,000
But, so starting from here, this is where we begin, we begin this movement inwards.

87
00:13:44,000 --> 00:13:54,000
This is the first realization among many realizations, where we come to realize that let alone this inevitability of death.

88
00:13:54,000 --> 00:14:05,000
There's actually the inevitability of change from, you know, on increasingly shorter intervals.

89
00:14:05,000 --> 00:14:13,000
So, when I was young, I was one way, and now I'm older, and then I'm getting older, when our body changes,

90
00:14:13,000 --> 00:14:22,000
and when we begin to get sick, when we begin to get old, when our physical appearance,

91
00:14:22,000 --> 00:14:32,000
our physical nature changes, when the mind changes, sometimes when we find ourselves becoming addicted to something or attached to something,

92
00:14:32,000 --> 00:14:37,000
or we find ourselves, the mind changing, maybe forgetting things more, and so on.

93
00:14:37,000 --> 00:14:45,000
The change that comes in the mind will happen in the body, and the mind will occur many times over, of course, in our lives.

94
00:14:45,000 --> 00:14:55,000
And eventually getting shorter and shorter and shorter, until we realize that actually this idea of death is really just the tip of the iceberg,

95
00:14:55,000 --> 00:14:59,000
and actually death is something that occurs at every moment.

96
00:14:59,000 --> 00:15:09,000
This teaching is something that begins us on this course to look inwards, until eventually we see that every moment we're born and die.

97
00:15:09,000 --> 00:15:18,000
Every moment things are changing, there's no reality that exists more than one moment.

98
00:15:18,000 --> 00:15:26,000
An ordinary person, when they're sitting for all of us, when we come to practice meditation,

99
00:15:26,000 --> 00:15:30,000
at first we see the body as being an entity.

100
00:15:30,000 --> 00:15:35,000
We think of it as being a stable, lasting entity.

101
00:15:35,000 --> 00:15:47,000
An ordinary person who has never thought about these things will generally give rise to this idea that this is something that is going to last, something that I can depend upon.

102
00:15:47,000 --> 00:15:57,000
And they will become negligent in terms of clinging to it. They will expect it to be there the next moment, the next day, the next month, the next year.

103
00:15:57,000 --> 00:16:03,000
And they will plan things out, so people will have plans for tomorrow, next month, next year.

104
00:16:03,000 --> 00:16:17,000
Even many years off into the future, they have a retirement plan, they have very, very long grandchildren and just plans like this.

105
00:16:17,000 --> 00:16:22,000
And then this realization comes that eventually you're going to die.

106
00:16:22,000 --> 00:16:27,000
And for some people it comes quite quickly, suddenly they get sick in the doctors as they have a month to live.

107
00:16:27,000 --> 00:16:38,000
And this is an incredible shock that changes their whole outlook on my suddenly from years, months or years it becomes days.

108
00:16:38,000 --> 00:16:52,000
So what am I going to do tomorrow and realizing that you can't cling to things beyond maybe the next few days or the next few weeks or even months?

109
00:16:52,000 --> 00:16:59,000
That's how we begin to come and practice meditation with these sorts of realizations, realizing that we can't possibly find satisfaction.

110
00:16:59,000 --> 00:17:07,000
And in the end we do have to come to terms with change, with the inevitability of losing the things that we cling to.

111
00:17:07,000 --> 00:17:18,000
Once we begin to practice meditation, we take this the next step of starting to see the mind is changing for a moment to moment.

112
00:17:18,000 --> 00:17:26,000
The body is changing for a moment, that even sitting here, our experience determines the reality.

113
00:17:26,000 --> 00:17:31,000
So the reality is not this body lasting from one moment to the moment.

114
00:17:31,000 --> 00:17:37,000
Not even from moment to moment, that will go on from day to day or week to week or month.

115
00:17:37,000 --> 00:17:42,000
It's not that we're going to die in days or weeks or months or years.

116
00:17:42,000 --> 00:17:49,000
It's that actually the we, the body is something that dies, is born and dies at every moment.

117
00:17:49,000 --> 00:17:54,000
As we begin to practice meditation, you begin to see this, you begin to see that actually there is no body.

118
00:17:54,000 --> 00:17:56,000
There is only the experience.

119
00:17:56,000 --> 00:18:00,000
So when you look at another person's body, look at your body, then you see it.

120
00:18:00,000 --> 00:18:06,000
And at that moment the body arises as a thought in the mind based on the image that you see.

121
00:18:06,000 --> 00:18:11,000
When you experience the tension in the legs, the tension in the back of sitting here,

122
00:18:11,000 --> 00:18:12,000
that's when the body arises.

123
00:18:12,000 --> 00:18:15,000
There's the mind and the body in that moment.

124
00:18:15,000 --> 00:18:21,000
Once the mind moves somewhere else to hearing or seeing or so on, that experience disappears.

125
00:18:21,000 --> 00:18:23,000
And therefore the body disappears.

126
00:18:23,000 --> 00:18:25,000
The body is at that moment gone.

127
00:18:25,000 --> 00:18:34,000
The reality of the body is ceased.

128
00:18:34,000 --> 00:18:39,000
And so this sort of realization starts with what is explained in this verse.

129
00:18:39,000 --> 00:18:41,000
So that's really the importance of this verse.

130
00:18:41,000 --> 00:18:48,000
We should understand it as the beginnings of Buddhist practice or as an example of how we begin Buddhist practice.

131
00:18:48,000 --> 00:18:52,000
We begin by accepting these very simple truths of life.

132
00:18:52,000 --> 00:18:54,000
That death is inevitable.

133
00:18:54,000 --> 00:19:04,000
This body, anyone who clings to this body has to face the question of what they're going to do.

134
00:19:04,000 --> 00:19:10,000
Death or what is the purpose of clinging when we're going to lose this body in the end?

135
00:19:10,000 --> 00:19:13,000
Why are you worrying about this body?

136
00:19:13,000 --> 00:19:14,000
Why are you concerned with it?

137
00:19:14,000 --> 00:19:21,000
Why are you attached to it when it's not obviously going to bring you lasting satisfaction?

138
00:19:21,000 --> 00:19:23,000
In the end you're going to lose.

139
00:19:23,000 --> 00:19:25,000
In the end this body is going to lie like a useless log.

140
00:19:25,000 --> 00:19:33,000
It's a vivid image for people who cling to the body because we tend not to like to think of our body

141
00:19:33,000 --> 00:19:35,000
as something that we have to throw away.

142
00:19:35,000 --> 00:19:40,000
We try our best to not have to lose the body, to care for it and concern about it.

143
00:19:40,000 --> 00:19:47,000
And we'll be concerned about all the limbs and organs and if we lose a finger, if we lose a hand, if we lose a leg,

144
00:19:47,000 --> 00:19:49,000
this would be travesty.

145
00:19:49,000 --> 00:19:58,000
If we go blind, if we go deaf, if we lose a tooth, even if we lose a tooth, it's an incredible travesty for us.

146
00:19:58,000 --> 00:20:04,000
Because we cling to this body as being me as being mine as being stable.

147
00:20:04,000 --> 00:20:10,000
As the last verse said, it's like a pot and these verses clearly go together.

148
00:20:10,000 --> 00:20:13,000
They're along the same sort of theme.

149
00:20:13,000 --> 00:20:20,000
That the body is fragile and in the end it's going to break and become useless like a charred log.

150
00:20:20,000 --> 00:20:25,000
Once we begin on this sort of introspection and many other introspection,

151
00:20:25,000 --> 00:20:32,000
the idea that habits can change the whole of your being, for example.

152
00:20:32,000 --> 00:20:39,000
It's realization that clinging leads to craving leads to clinging leads to suffering and so on.

153
00:20:39,000 --> 00:20:46,000
The realization that the way we're carrying out our life is unsustainable and in the end we're going to lose,

154
00:20:46,000 --> 00:20:52,000
we're going to lose this due to the inevitability of things like death and change.

155
00:20:52,000 --> 00:20:57,000
This leads us inward, it leads us to approach life from a different point of view.

156
00:20:57,000 --> 00:21:04,000
It's the beginning of a longer course of practice to see not only that death is inevitable,

157
00:21:04,000 --> 00:21:07,000
but that death occurs at every moment.

158
00:21:07,000 --> 00:21:11,000
Not only can we not cling to things next year or 10 years from now.

159
00:21:11,000 --> 00:21:16,000
We can't even cling to things in the next moment that they're going to change

160
00:21:16,000 --> 00:21:18,000
and they could come at any time.

161
00:21:18,000 --> 00:21:22,000
Death could come at any moment. We don't know what the experience is going to be in the next moment.

162
00:21:22,000 --> 00:21:27,000
Once we begin to practice meditation, we're going to begin to see the mind is changing.

163
00:21:27,000 --> 00:21:32,000
The body is changing and this occurs in the beginning, it occurs in terms of day by day.

164
00:21:32,000 --> 00:21:36,000
So you come here to practice meditation and today was a really good day.

165
00:21:36,000 --> 00:21:38,000
So you come and tell me about it and they're very happy.

166
00:21:38,000 --> 00:21:41,000
And then the next day you come back and tell me how horrible it was.

167
00:21:41,000 --> 00:21:44,000
You can understand how all of a sudden today was a very, very bad day.

168
00:21:44,000 --> 00:21:49,000
Or today was a very bad day and you're thinking of quitting and then tomorrow was a very good day.

169
00:21:49,000 --> 00:21:55,000
And then this happens again and again until finally you realize that this is the nature of life,

170
00:21:55,000 --> 00:21:57,000
that you can't depend on it.

171
00:21:57,000 --> 00:22:02,000
And then you begin to look closer and closer until you realize that it's first meditation practice,

172
00:22:02,000 --> 00:22:05,000
the meditation practice, you can't expect it to be the same.

173
00:22:05,000 --> 00:22:10,000
You can't cling to even a single meditation practice as being dependable.

174
00:22:10,000 --> 00:22:13,000
The next meditation practice might be totally different.

175
00:22:13,000 --> 00:22:18,000
And then eventually it gets to the point of realizing you can't cling from one moment to the next.

176
00:22:18,000 --> 00:22:21,000
And eventually this is how you would let go of everything.

177
00:22:21,000 --> 00:22:25,000
And the end you're able to let go of not only your body but the mind as well.

178
00:22:25,000 --> 00:22:33,000
And not be concerned about any experience, realizing that the true cause of suffering is our attachment to things.

179
00:22:33,000 --> 00:22:37,000
So it's this sort of realization that sets us on those.

180
00:22:37,000 --> 00:22:43,000
This was the benefit of this verse, especially for this monk who was being sick was to remind him.

181
00:22:43,000 --> 00:22:54,000
And to alert him to the fact that this was real and was a serious fact of life that he was going to have to face now.

182
00:22:54,000 --> 00:22:59,000
And to set him on the right path and help him to come to terms with us and to not be lax.

183
00:22:59,000 --> 00:23:05,000
Because when people are sick they'll often, they think that we think the correct response to get sad and upset

184
00:23:05,000 --> 00:23:15,000
and cry and depressed and angry and cry and cry out for medicines or maybe even cling to cling more to happy things.

185
00:23:15,000 --> 00:23:21,000
And say, well I've got to get, now I've only got so many days to live, I've got to get as much happiness in before I die.

186
00:23:21,000 --> 00:23:28,000
Not realizing that this is going to make you cling more and more and be less and less satisfied.

187
00:23:28,000 --> 00:23:36,000
And so for the most part people get depressed and upset because they're not going to be able to cling to the things that they're clinging to.

188
00:23:36,000 --> 00:23:43,000
And so here's the Buddha reminding him that it's too serious for us to be lax and negligent in this room.

189
00:23:43,000 --> 00:23:47,000
Realize that in the end the body, this body will land with us.

190
00:23:47,000 --> 00:23:50,000
It's something that we can always come back and think about.

191
00:23:50,000 --> 00:23:56,000
It's something always useful to come back and remind yourself that this body is like a log.

192
00:23:56,000 --> 00:24:02,000
Eventually it's going to fall and lie useless on the earth just as a log.

193
00:24:02,000 --> 00:24:11,000
It's something that this is a reason why this verse became quite famous because it's a good meditation to use.

194
00:24:11,000 --> 00:24:17,000
To remind yourself from time to time, whenever you're clinging to the body you're worried about the body or when you find yourself.

195
00:24:17,000 --> 00:24:32,000
Or you can use it to test yourself to point out to yourself how ridiculous is your love and your attachment to your body as being beautiful and wonderful and a source of stability and happiness and so on.

196
00:24:32,000 --> 00:24:34,000
So just one more verse.

197
00:24:34,000 --> 00:24:43,000
We're going through this one by one and using each of them to give us a little bit more food for thought and explore the Buddha's teaching from one more point of view.

198
00:24:43,000 --> 00:24:49,000
So thanks for tuning in, thanks for listening and until next time, back to our practice.

