1
00:00:00,000 --> 00:00:09,480
What is the importance of freedom and creativity in becoming an arahant or even Buddha?

2
00:00:09,480 --> 00:00:13,640
For lack of a better example, if you had a power which could instantly transform anyone

3
00:00:13,640 --> 00:00:17,840
into a Buddha, even without their consent, would you use it?

4
00:00:17,840 --> 00:00:18,840
If so, why?

5
00:00:18,840 --> 00:00:21,360
And if not, why not?

6
00:00:21,360 --> 00:00:22,360
Cover all your bases.

7
00:00:22,360 --> 00:00:28,760
So we have to answer the follow-up questions, which is fine.

8
00:00:28,760 --> 00:00:44,720
But it's curious because if what you're asking is, is there something wrong with bringing

9
00:00:44,720 --> 00:00:51,640
a person in light and meant without their, without allowing them their freedom and

10
00:00:51,640 --> 00:00:54,960
creativity?

11
00:00:54,960 --> 00:01:00,000
The answer is, of course, no, because whatever can, like hypothetically, whatever it

12
00:01:00,000 --> 00:01:05,360
is that can bring a person in light meant is, is the most important.

13
00:01:05,360 --> 00:01:09,920
And it sounds like maybe like a question that shouldn't even be entertained because it's

14
00:01:09,920 --> 00:01:13,920
so hypothetical, you know, could you instantly transform a person into a Buddha?

15
00:01:13,920 --> 00:01:16,000
It's ridiculous, of course.

16
00:01:16,000 --> 00:01:17,800
But there are interesting ramifications here.

17
00:01:17,800 --> 00:01:23,480
We're talking about, many people do get this kind of feeling, they don't enunciate it

18
00:01:23,480 --> 00:01:32,000
in such a blatant or explicit manner as you've done.

19
00:01:32,000 --> 00:01:40,120
But I think I get the feeling, it's that, following, what good is it if it's just brainwashing

20
00:01:40,120 --> 00:01:44,680
or following blindly after someone else, and you practice this and we're not even supposed

21
00:01:44,680 --> 00:01:51,120
to know all the theory behind it, just practice and you'll become free from suffering.

22
00:01:51,120 --> 00:01:57,200
And so people say, you know, I want some freedom and creativity and I would rather be free

23
00:01:57,200 --> 00:02:04,560
and creative and have my own interpretation and be unenlightened then to be, to have to follow

24
00:02:04,560 --> 00:02:16,800
after someone else or to have it imposed upon me, for example, and become enlightened.

25
00:02:16,800 --> 00:02:24,560
I mean, even when you explicitly state it like that, it sounds silly again, but there's

26
00:02:24,560 --> 00:02:29,040
this angst that people get when they have to follow things.

27
00:02:29,040 --> 00:02:37,040
And it's something that feels really good about what we're doing and that we don't have

28
00:02:37,040 --> 00:02:38,040
this.

29
00:02:38,040 --> 00:02:41,600
I mean, I don't agree with that at all and I see it quite clearly as a mistake that people

30
00:02:41,600 --> 00:02:45,880
make this idea of having their own interpretation.

31
00:02:45,880 --> 00:02:49,880
It's called postmodernism and I keep bringing this word up and what everyone to look

32
00:02:49,880 --> 00:02:57,080
it up on the Wikipedia, look up postmodernism on Wikipedia because it describes modern society

33
00:02:57,080 --> 00:02:58,280
and we don't realize it.

34
00:02:58,280 --> 00:03:03,120
Most people have never heard this word and don't realize that there is a word for it and

35
00:03:03,120 --> 00:03:10,520
it has been well documented and described as a phenomenon.

36
00:03:10,520 --> 00:03:18,520
Basically, it's simplistically that all truth is relative and everyone has to find their

37
00:03:18,520 --> 00:03:22,200
own truth.

38
00:03:22,200 --> 00:03:27,920
Another way of saying is, whatever's right for you is right because it's right for you.

39
00:03:27,920 --> 00:03:35,240
Whatever you believe is a belief that should be respected, for example.

40
00:03:35,240 --> 00:03:36,240
It's called postmodernism.

41
00:03:36,240 --> 00:03:40,400
You can look it up, a new age could have something to do with a new age has a lot of other

42
00:03:40,400 --> 00:03:42,160
weird stuff about it.

43
00:03:42,160 --> 00:03:46,200
New age is the age of Aquarius, the idea that we're coming into a new spiritual age

44
00:03:46,200 --> 00:03:50,800
and that's to do with astrology and whatever.

45
00:03:50,800 --> 00:03:55,560
And we don't even realize that this is phenomenon.

46
00:03:55,560 --> 00:04:00,840
We've come to accept, we in a general sense society has come to accept that that's the

47
00:04:00,840 --> 00:04:03,560
way it is and that's the truth.

48
00:04:03,560 --> 00:04:13,440
And we fight against people who would suggest that there is an ultimate truth or a specific

49
00:04:13,440 --> 00:04:19,160
method or specific path that everyone has to follow.

50
00:04:19,160 --> 00:04:23,160
I mean, just hearing this, jars with most people.

51
00:04:23,160 --> 00:04:25,120
Just the idea that there is the only way.

52
00:04:25,120 --> 00:04:29,800
We have this talk in the Buddha, Ekayanoyang, Bhikkhuemago and people say, no, no, it doesn't

53
00:04:29,800 --> 00:04:30,800
mean only way.

54
00:04:30,800 --> 00:04:34,480
It's technically, it probably doesn't, but the Buddha said several times, there's only

55
00:04:34,480 --> 00:04:39,000
one way and so on, but people don't want to hear this.

56
00:04:39,000 --> 00:04:44,480
So if I told you, I have the way to become enlightened and only this way leads to enlightenment.

57
00:04:44,480 --> 00:04:48,040
That would be so horrible for anyone to hear.

58
00:04:48,040 --> 00:04:56,560
Most people would reject that simply on the basis that it had this kind of a claim, which

59
00:04:56,560 --> 00:05:00,040
they feel goes against what is true and right.

60
00:05:00,040 --> 00:05:02,600
And in fact, it just goes against this idea of postmodernism.

61
00:05:02,600 --> 00:05:09,120
So it very well could be the fact that there is only one path to become free from suffering.

62
00:05:09,120 --> 00:05:12,000
There's no reason for us to think that not possible.

63
00:05:12,000 --> 00:05:13,680
It could be the case.

64
00:05:13,680 --> 00:05:16,440
The problem is we've tried this path and that path and none of them work.

65
00:05:16,440 --> 00:05:22,120
So we start to think, maybe there is no one path and maybe it all is all.

66
00:05:22,120 --> 00:05:26,400
It comes out of giving up religion, seeing that the religions that we followed aren't

67
00:05:26,400 --> 00:05:30,920
really satisfactory.

68
00:05:30,920 --> 00:05:37,280
So I would reject that sort of a, I would at least encourage people to question that

69
00:05:37,280 --> 00:05:42,160
feeling that arises, the feeling of the need to be creative and have your own interpretation

70
00:05:42,160 --> 00:05:46,280
and your own outlook on life.

71
00:05:46,280 --> 00:05:50,840
It may be that your outlook on life is wrong and it may be objectively wrong.

72
00:05:50,840 --> 00:05:54,720
The idea of something being objectively wrong is possible.

73
00:05:54,720 --> 00:05:58,400
We have this argument, philosopher said this argument, is it possible for something to be

74
00:05:58,400 --> 00:06:01,440
objectively right and objectively wrong?

75
00:06:01,440 --> 00:06:05,360
I say yes, it is possible to be objectively wrong and objectively right because I would

76
00:06:05,360 --> 00:06:08,000
say suffering is objective.

77
00:06:08,000 --> 00:06:11,840
Something causes suffering.

78
00:06:11,840 --> 00:06:15,760
When a person says there is suffering, that is objective.

79
00:06:15,760 --> 00:06:17,560
The suffering itself is not subjective.

80
00:06:17,560 --> 00:06:18,880
They're not happy about something.

81
00:06:18,880 --> 00:06:21,680
They're unhappy about something.

82
00:06:21,680 --> 00:06:26,320
You can't be unhappy about something and not suffer based on it.

83
00:06:26,320 --> 00:06:33,920
Suffering is objective and because it's objective, it has objective causes and because

84
00:06:33,920 --> 00:06:38,320
it has objective causes, there is an objective way out of suffering.

85
00:06:38,320 --> 00:06:44,000
So the four noble truths are objective and the path which leads to the cessation of suffering

86
00:06:44,000 --> 00:06:49,720
is an objective path and it's the eightfold path, or it could be the sevenfold path

87
00:06:49,720 --> 00:06:54,000
or the ninefold path or whatever you want to explain it, but it's based on the framework

88
00:06:54,000 --> 00:06:55,800
that the Buddha laid down.

89
00:06:55,800 --> 00:06:58,480
That's the way out of suffering.

90
00:06:58,480 --> 00:07:00,120
That's the only way to become free from suffering.

91
00:07:00,120 --> 00:07:05,640
There is no power to become instantly, become a Buddha, but there is no way that someone

92
00:07:05,640 --> 00:07:10,000
can make up a new path that is outside of the framework of the eightfold noble path.

93
00:07:10,000 --> 00:07:11,320
It's not possible.

94
00:07:11,320 --> 00:07:13,640
That is the objective truth.

95
00:07:13,640 --> 00:07:14,640
You can believe me or not.

96
00:07:14,640 --> 00:07:19,400
I don't mind if you disbelieve me, but that is the claim and it's worth, if anyone is interested

97
00:07:19,400 --> 00:07:21,720
or welcome to check that out, test it out.

98
00:07:21,720 --> 00:07:27,600
Read about the eightfold noble path, find a teacher who teaches it and see for yourself

99
00:07:27,600 --> 00:07:29,400
whether that's the truth.

100
00:07:29,400 --> 00:07:33,680
What you should see is as you practice it, you come to understand suffering and you start

101
00:07:33,680 --> 00:07:43,960
to let go of suffering, which means you let go, craving disappears or desire disappears

102
00:07:43,960 --> 00:07:51,320
and you should find that as desire disappears, suffering ceases to the point where all suffering

103
00:07:51,320 --> 00:07:54,320
ceases and one enters into Nibbana.

104
00:07:54,320 --> 00:07:55,880
That's what one should see for oneself.

105
00:07:55,880 --> 00:07:58,760
That's the theory here.

106
00:07:58,760 --> 00:08:04,200
So I hope that is related to your question and that is really what you were talking about.

107
00:08:04,200 --> 00:08:10,520
This sort of feeling that people have the need to be individual.

108
00:08:10,520 --> 00:08:15,560
It reminds me of before I became a monk that it was big in the 90s, I think, or even

109
00:08:15,560 --> 00:08:16,560
the 80s.

110
00:08:16,560 --> 00:08:24,000
The 80s was big because the pink and all of them, everyone being themselves and like Michael

111
00:08:24,000 --> 00:08:29,600
Jackson and all that, but I guess it's probably still the case.

112
00:08:29,600 --> 00:08:35,520
I haven't been very much in touch with society, the whole being an individual, being

113
00:08:35,520 --> 00:08:46,200
yourself and so having your own ideas, it just turned into a bit of a monster, actually.

114
00:08:46,200 --> 00:08:49,680
Where no one believes anything and is willing to follow anything.

115
00:08:49,680 --> 00:08:56,680
Orthodox became, it's funny, I was talking about this and what's happened is being Orthodox

116
00:08:56,680 --> 00:08:59,840
now is rebellious.

117
00:08:59,840 --> 00:09:05,120
You get into so much flak, just as before you would get into flak for being unorthodox.

118
00:09:05,120 --> 00:09:09,320
That was the whole punk movement and so on, oh, they were vilified and so on.

119
00:09:09,320 --> 00:09:10,480
But now it's the opposite.

120
00:09:10,480 --> 00:09:15,040
As soon as you try to be Orthodox and say, no, no, let's go by what the book says.

121
00:09:15,040 --> 00:09:19,120
People will blast you, people will tear you down.

122
00:09:19,120 --> 00:09:22,400
That's the funny thing.

123
00:09:22,400 --> 00:09:23,400
It makes me feel good.

124
00:09:23,400 --> 00:09:27,760
I mean, I used to be the big rebel, I was a punk, so now we're still rebellious, but

125
00:09:27,760 --> 00:09:29,560
we're rebellious.

126
00:09:29,560 --> 00:09:31,960
I think it's called avant-garde.

127
00:09:31,960 --> 00:09:38,960
Once everyone becomes avant-garde or once everyone becomes extreme, then you have to go

128
00:09:38,960 --> 00:09:44,280
back and take the Orthodox, something like that, no, that's not true.

129
00:09:44,280 --> 00:09:48,840
Whether it's Orthodox or not, the truth is the truth, but there is an objective truth.

130
00:09:48,840 --> 00:09:53,080
It would be horrible if there wasn't an objective truth, lucky for us there is an objective

131
00:09:53,080 --> 00:09:56,680
truth and therefore there is a way out of suffering.

132
00:09:56,680 --> 00:10:00,320
If the Buddha said this, if there wasn't a way out of suffering, I wouldn't enjoy

133
00:10:00,320 --> 00:10:03,840
you to practice for the cessation of suffering.

134
00:10:03,840 --> 00:10:10,600
But since there is a way out of suffering, therefore I enjoy you and treat you to practice

135
00:10:10,600 --> 00:10:37,720
for the cessation of suffering, I hope that helps.

