1
00:00:00,000 --> 00:00:07,000
Hello everyone, just an update here, I wanted to let you all know thank you for your support

2
00:00:07,000 --> 00:00:13,000
and your interest in my videos and in the things that I've been teaching and giving and passing on.

3
00:00:13,000 --> 00:00:18,000
I really don't think much of this is mine per se, things that I'm passing on,

4
00:00:18,000 --> 00:00:24,000
because the reason I want to say thanks today is because I hit a million views this week.

5
00:00:24,000 --> 00:00:29,000
And so I'd like to express my appreciation that everyone, that there are so many people out there

6
00:00:29,000 --> 00:00:37,000
who are interested in good things that people are not just interested in in comedy or all of these other videos that are out there

7
00:00:37,000 --> 00:00:43,000
that you're actually taking the time to look at those things that are essential and are benefit to you

8
00:00:43,000 --> 00:00:47,000
and are really going to bring you true peace happiness and freedom from suffering.

9
00:00:47,000 --> 00:00:52,000
So thanks a lot. I also wanted to give some announcements as to what other things I've been doing

10
00:00:52,000 --> 00:00:58,000
if you haven't been involved. I don't even think I've mentioned that we have a social

11
00:00:58,000 --> 00:01:05,000
network set up and I think at this point it's proper time to let everyone know that we have,

12
00:01:05,000 --> 00:01:10,000
you should if you haven't, go on over there and see what's up because a lot of people who have looked at these videos

13
00:01:10,000 --> 00:01:15,000
have subsequently gone to this site and have become a part of the community.

14
00:01:15,000 --> 00:01:20,000
And it's kind of like Facebook or Google Plus or MySpace or so on,

15
00:01:20,000 --> 00:01:27,000
but it's totally based around the things that I do and the people who were involved in the things,

16
00:01:27,000 --> 00:01:32,000
and these sorts of things do. It's our little group based on these videos or based on the teachings

17
00:01:32,000 --> 00:01:39,000
that these videos come from, based on the tradition that these videos let this teaching come from.

18
00:01:39,000 --> 00:01:44,000
And so it's really a very close knit or a very harmonious group.

19
00:01:44,000 --> 00:01:49,000
There's not a lot of debate and argument. Everyone's just kind of helping each other out and offering

20
00:01:49,000 --> 00:01:54,000
advice and encouragement, sharing things and so on. So head on over there if you haven't already.

21
00:01:54,000 --> 00:02:00,000
It's m-y-my.seriumongalo.org. I'll put a link in there.

22
00:02:00,000 --> 00:02:06,000
I think it's in the description of all my videos if it's not. I'll put it there.

23
00:02:06,000 --> 00:02:13,000
And the other thing is we've begun to experiment again with virtual reality.

24
00:02:13,000 --> 00:02:18,000
And the point of virtual reality is not that there's something wow or wonderful about it,

25
00:02:18,000 --> 00:02:26,000
but the spatial orientation of it is a lot like having webcam chat.

26
00:02:26,000 --> 00:02:33,000
The very web cam chat is you can see each other. The problem with it is it's very hard to set up with lots of people.

27
00:02:33,000 --> 00:02:40,000
For me anyway, because of my internet connection, the internet here in the forest obviously is not great.

28
00:02:40,000 --> 00:02:47,000
But virtual reality actually has a lower bandwidth footprint, so we can actually use it to be able to see each other.

29
00:02:47,000 --> 00:02:51,000
So we can sit around and see who's there as opposed to just texting with each other.

30
00:02:51,000 --> 00:03:00,000
And the idea is we're going to use this to give courses and study groups and talks hopefully in the future.

31
00:03:00,000 --> 00:03:06,000
We'll see how it goes. We'll have to see what sort of communication we can use.

32
00:03:06,000 --> 00:03:16,000
But we've got this big whiteboard set up and you can put teachings on there so we can study the texts and that sort of thing.

33
00:03:16,000 --> 00:03:23,000
And this is available. You can look it up at the wiki. I'll put a link here in the description as well.

34
00:03:23,000 --> 00:03:30,000
So it's wiki.serimungamow.org. I think front slash wiki, front slash dumb overs. It's called the dumb overs.

35
00:03:30,000 --> 00:03:36,000
Anyway, I'll put a link in the description if you want to get involved in that.

36
00:03:36,000 --> 00:03:41,000
I think that's about it. There's the question and answer for them, but I think many people know about that.

37
00:03:41,000 --> 00:03:45,000
If you don't know about that yet, go on there and you're welcome to ask questions.

38
00:03:45,000 --> 00:03:49,000
And I'm not making videos about the answers.

39
00:03:49,000 --> 00:03:57,000
If people call those questions on there, I'm not specifically making videos because, and this is the other thing I forgot it.

40
00:03:57,000 --> 00:04:02,000
I've started giving talks every week on UStream as well.

41
00:04:02,000 --> 00:04:07,000
Until YouTube gives me a live streaming account, please YouTube.

42
00:04:07,000 --> 00:04:14,000
If you know someone who works at Google, let them know that we need a live streaming account, let me in, let us in.

43
00:04:14,000 --> 00:04:21,000
In the meantime, we're on UStream via our radio page so you can go to radio.serimungalow.org

44
00:04:21,000 --> 00:04:27,000
and I'll put a link there in the description and you can see our radio page, which is not really a radio.

45
00:04:27,000 --> 00:04:35,000
It's actual webcam. So you get to see me and hear me giving a talk and hopefully the bandwidth is not a problem.

46
00:04:35,000 --> 00:04:40,000
I've given a few already in a pretty good turnout and then I record the videos and we can put them up on YouTube.

47
00:04:40,000 --> 00:04:47,000
You may be seeing some of those coming from UStream to YouTube because it does allow me to connect them.

48
00:04:47,000 --> 00:04:52,000
But that's where what I've started doing is answering questions there.

49
00:04:52,000 --> 00:04:59,000
So if you have questions and you'd like a video response, come and ask them after the UStream.

50
00:04:59,000 --> 00:05:11,000
In the UStream session, after we have our formal ceremony, it's a formal taking of refuge, which many people like to do.

51
00:05:11,000 --> 00:05:14,000
If you're interested, you're welcome to take part in that video. You don't have to.

52
00:05:14,000 --> 00:05:18,000
Anyway, you can come and check it out. You can follow along or not follow along, listen.

53
00:05:18,000 --> 00:05:28,000
And then after we have the formal taking of refugees and precepts, then I will answer questions and try to talk about

54
00:05:28,000 --> 00:05:36,000
Buddhist things to walk into taking. If you take part in that, excuse me.

55
00:05:36,000 --> 00:05:43,000
So that's all. Thanks for tuning in. And again, we've hit a million. I've got there's 5,000 subscribers.

56
00:05:43,000 --> 00:05:49,000
So it's like, I mean, it's not like bragging, but it's just amazing to think that there are so many people out there interested in these things.

57
00:05:49,000 --> 00:05:59,000
So thanks and congratulations to all of us. And I didn't do it alone. It's great to see that there are people interested in these things.

58
00:05:59,000 --> 00:06:23,000
Okay, so thanks. Follow back.

