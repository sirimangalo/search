1
00:00:00,000 --> 00:00:08,720
stunning and noting question when I sit and say to myself sitting I have to do a quick body

2
00:00:08,720 --> 00:00:15,600
stun at least of my legs and spine in order to really know that I am indeed sitting.

3
00:00:15,600 --> 00:00:22,000
Isn't that quick body stun somewhat contrary to a one point of mind? Is one label really in

4
00:00:22,000 --> 00:00:29,520
now? Answer, you do not just need to perform any such body stun trying to force the perception

5
00:00:29,520 --> 00:00:36,880
of sitting is not proper meditation because it reinforces the delusion of control. In tranquility

6
00:00:36,880 --> 00:00:43,920
meditation it might be okay to control the mind if your purpose is simply to calm the mind

7
00:00:43,920 --> 00:00:49,200
you can force it, push it, need it and mode it into shape.

8
00:00:49,200 --> 00:00:55,680
tranquility meditation is not for the purpose of cultivating insight so there is no harm there

9
00:00:55,680 --> 00:01:03,200
as far as attuning the goal of tranquility. In repassana meditation however the focus is not

10
00:01:03,200 --> 00:01:10,000
the concept of sitting but rather sitting is a description of the feelings that you experience.

11
00:01:10,000 --> 00:01:14,960
The fact that you are aware of the tension in your back or the pressure on the floor

12
00:01:14,960 --> 00:01:21,040
at that moment when you say sitting is enough. You are aware of that experience and you

13
00:01:21,040 --> 00:01:28,640
call that experience sitting. It is important to recognize the physical phenomena as they are.

14
00:01:28,640 --> 00:01:35,440
It is not so important to be aware that this is a sitting posture. Push your mind on the entire

15
00:01:35,440 --> 00:01:41,360
body and say to yourself sitting the recognition of the sitting posture comes when it's

16
00:01:41,360 --> 00:01:49,440
own or may not come at all. This is the fana, touch of repassana, a recognition that this is sitting.

17
00:01:49,440 --> 00:01:55,840
Fana, recognition is unpredictable and what you are seeing that leads you to say that you have to

18
00:01:55,840 --> 00:02:02,560
push it in order for the recognition to calm is not so. The recognition does not always come

19
00:02:02,560 --> 00:02:09,120
automatically. Sometimes it does come and you immediately know that you are sitting and you recognize

20
00:02:09,120 --> 00:02:16,080
this is a sitting posture. Sometimes you look and look and you never become aware that it is sitting.

21
00:02:16,080 --> 00:02:22,880
This is because it is not so. You cannot host yourself to know that it is sitting that you are

22
00:02:22,880 --> 00:02:30,320
experiencing. Understanding the nature of reality in this way is the purpose of practicing mindfulness.

23
00:02:30,320 --> 00:02:38,000
You will find that sometimes a certain sensation or set of sensations leads to the arising

24
00:02:38,000 --> 00:02:43,680
of the recognition that you are sitting. Sometimes such a recognition will not arise.

25
00:02:44,240 --> 00:02:49,600
Often meditators believe that something is wrong and they are unable to force the

26
00:02:49,600 --> 00:02:56,560
recognition to arise. Once they are experiencing, however, it is the nature of reality as

27
00:02:56,560 --> 00:03:04,240
impermanent, suffering and nonso. The purpose of meditation is to see these characteristics.

28
00:03:04,240 --> 00:03:34,080
So in this instance, they can be reassured that they are practicing correctly.

