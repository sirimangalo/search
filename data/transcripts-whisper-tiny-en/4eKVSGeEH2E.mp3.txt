Okay, good evening everyone, broadcasting live, July 4th, July 4th, and day when Americans
blow stuff up, speaking and blowing things up.
Most people doing no explosions hit Medina, Qatif, and Jedi in Saudi Arabia.
Some people celebrate their holidays by blowing other people up, and people do that.
Right back to doing the news, you know, commenting on the news, Buddhist news.
It's funny how the last, there's this article on Reuters, and the last sentence is they
have no religion, some cleric in Saudi Arabia, condemning the attacks, which is a great thing,
great that he's condemning the same as his and Darius lab, seeing that his lab isn't
about blowing people up, isn't about killing, I can say that is great.
But to say that these people have no religion, it's, I don't know.
I think I can understand what everyone's saying when they say this, but it doesn't make
much sense to me.
Religious people can interpret their religion, or even read literally their religion.
Or the evil that is there, I mean, there's evil in many religions, if you interpret it
literally, you know, of course you can interpret these things as we've talked about,
and I don't want to go into too much detail, but I think it's going too far to say these
people had no religion, because it denies the role that religion and religious texts
like to say that the Quran had nothing to do and the hadith had nothing to do, hadith
hadith had nothing to do with these attacks isn't pretty absurd.
So we have to acknowledge anything that, yes, these books talk about killing infidels,
which, you know, do these people means anybody who doesn't belong to our sect of Islam.
So we have to acknowledge that, I think, otherwise we, I don't know, I mean, people
make the argument that saying that this isn't true religion, God would never, God would
never sanctions such things, I mean, I don't know how effective that is, maybe it's effective,
maybe it's like this arguing over who is a true Scotsman, maybe it works in the end,
if you're saying you're not a true Muslim, if you kill people, problem is the books
tell the Quran, the Torah, I don't know about the New Testament, but the Jews and the Muslims
are pretty awful and they're God, you know, you'll read the Torah, the Torah has God
himself doing most of the killing in the, in the Torah, he's done by God, and horrific
killing at that and painful tortures, killing of innocents, well, yeah, killing of children,
but I can't see.
So I don't know what's up, people are killing each other still, killing each other everywhere,
killing each other in Florida, even, US, if all their guns has problems, and then at least
with all their religion has problems too much religion, probably, religions probably should
just die, maybe that's probably the way, you know, the way forward is just to stop being
so religious, you know, whether that had become Buddhist, else is happening in the news,
people are arguing over gay pride and Black pride or gay lives and Black lives, something
like that, in Toronto, you know, these people who are afraid to be who they are, afraid
to be who they are, in a very reasonable way afraid, criticizing people who have been driven
to fear by these terrible deeds because they're gay or because they're Black, not much
to say, except that part is about how horrific it is, but interesting as well is a little
bit of how much ego it gives, like, and maybe what naively suggests that the way forward
is not this, but the way forward is to give up our identity as I am gay or I am Black, you
know, when we stop with labels, it's kind of ridiculous at this day and age to be calling
each other Black and White, even gay and straight, starting to get a little bit silly,
doesn't define who you are right, especially not from Buddhist perspective, who you are,
who are you, who you are is nothing more than chaotic anger, get physical and mental
qualities of experience, the knowing and the known, there is no eye, there is no identity,
calling yourself Black as a silliest calling yourself a shoe, doesn't have any consequence,
doesn't have any meaning.
And I understand, I think there's more to it than that, honestly, it's this pride, you
know, it's not about being proud, it's about overcoming the fear, fighting back against
the fear and the terrorism, because that's what it is, they're being terrorized by people
who are mostly following religion, you know, I don't know what the Black people, Black
White situation with a gay situation is mostly about religion, almost sexuality, you could
argue there's some revulsion from Darwinian evolutionary perspective, because it, you know,
it does hurt the survival of the species one could argue, but for the most part it's
just religious, it's just my religion tells me how most sexuality is wrong, my first
teacher even tried to say that Buddhism says that homosexuality is wrong, he was trying
to get his meditators to not be homosexual, which is probably possible, but it's a little
bit misguided, we're sending spacecrafts to Jupiter, not so interesting, bang their attacks,
lots of bombing, but killing it's a huge mansion of bombing, walking down the street
and suddenly, to be hit with shrapnel from the bomb, someone blows themselves up, could
you imagine the minded things to get yourself worked up, so, so worked up that you walk
into a crowded area, you know, humans know, and America they're making this big deal over
the star, there's this guy Donald Trump, and he put on an ad depicting with a star on
it and I looked at it and it's just a star, it's not really the star of David and, oh,
they're making such a big deal over there.
This week, one thing that made me sad was this week is national fishing week, how dumb
are we, you know, we celebrate the whole week of terrorizing fairly innocent animal,
it's very fissured, probably killers you can argue, mostly they kill each other right,
especially if you need their babies, so they're not exactly innocent, but nobody deserves
having a hook stuck in their mouth, ripped out of their environment, it's too imagine
walking down the street and suddenly, oh, there's this hot dog sitting there or something
and you pick it up, start chewing on it, and suddenly you yank that into a cosmos with
this hook puncturing you to your cheek, and then you have your head cut off, I don't
know, it's just viciously cruel, viciously cruel to the poor fish, anyway, that's enough,
the news isn't that interesting, it's probably pretty depressing, if you spend too much
time on it, so it should be spent time on, the Buddha talks about four times, he may
be, and the quote that we have is a very brief quote that doesn't really explain anything,
what does he mean by the four times, what are these four things, what about them, why
are they right and what is good about them, but luckily we have two suitors, this is the
way of the certificate he loved and have one very short suit that, and then the next one
right after it helped fully is where the Buddha actually expanded upon it, he didn't
expand too much upon these items, the four times, but expanded upon why he was referring
them as being four times, what four times, listening to the dhamma, time-timely listening
of the dhamma, listening to the dhamma, calling the dhamma saga, timely discussing the
dhamma, so I ask him questions, calling the samasana, and I'm told this one should actually
be samasana, samasana depends which edition of the certificate, so we'll go with that,
we'll say timely, tranquility, and Kali, and now we pass on the timely insight, that's
it for the first one, but then if you skip ahead to the, so did it comes after it,
it talks about what do we mean by this, what does this mean, so that's going to the English
translation, oh master, because these four times rightly developed and coordinated,
gradually culminated in the destruction of the teens, still kind of an odd phrase in English,
because these aren't times, these are things done, Kali, nakali, nakali, nakali means time,
so Kali in the means by time, by time understood to mean at the right time, or from
time to time, timely.
So by engaging in these from time to time, salan and kayam bhapi, they lead to the destruction
of the asava, asava, of course, of the, the corruptions in the mind, kama, sava, bhava,
sava, of disaster, of sensuality, of ignorance, of desire to become, you know, this becoming
one thing, the ambitions, you know, that kind of thing, when it gives these four, so
what four things should we do from time to time, that will lead us to destroy the teens,
where you should listen to the dhamma from time to time, don't forget, don't forget
from time to time to, pull out the old recordings of teachers and pull out the books written,
recording the Buddha's teaching and study the dhamma, Kali, nakam masagatas, so not enough
to read, but also to discuss, to ask questions and to refine your understanding of the
dhamma, Kali and the samata, samata, samata, samata means mastery, but if we go with samata,
as the translation means tranquility, and it goes well with Vipassana, so we do that, so
from time to time cultivating tranquility, through the practice of samata bhava, the mindfulness
of the Buddha, mindfulness of breathing, my mid-dance on calming mind, and from time to time
Vipassana, Kali, namipassana, cultivating insight regularly, if you want to, maybe the best way
to translate this is regularly, and he uses the word coordinated, which is interesting,
I know Bhaniyu want the dhamana, keep on rolling, I don't know, I think he's extrapolating
a little bit, doesn't really mean coordinating, means keeping up doing, continually doing,
regularly doing, and then he says something, which is interesting about this second one,
and then he says something, which is actually, I think, a fairly common simile that the
Buddha uses, he says, just as when it is raining, the rain pours down and thick droplets
on the mountain top, so these four are like mountain tops, you do them, and you keep doing
them, it's like the rain pouring down on the mountain top, and so you look at the mountain
top, and you see the mountain top is getting wet, and you think, well, okay, that's the effect,
and that's sometimes how we look at our practice, or the things that we do, you listen
to the dhamana, you think, oh wow, I learned some dhamma, well that's the result of that,
and you think, you know, you talk about the dhamma, okay, well we've talked about the dhamma
as the result, and you think about, and you practice meditation, okay, I practice meditation,
that did something, and that's that, but there's more, the water flows down along the slope
and fills the cleft, scullies and creaks, these becoming full, fill up the pools, these becoming
full, fill up the lakes, these becoming full, fill up the stream, these becoming full, fill
up the rivers, and these becoming full, fill up the great ocean, so too these four times
rightly developed and coordinated, gradually culminated in the destruction of the
table, so this means if there's a follow up, you continue to practice and the rain continues
to fall, there are lasting and profound effects, as a result of doing things regular,
doing good things regular, dropped by drops, a bit by bit, token, token, same goes with evil,
you do evil, you don't see the real effects immediately, that eventually they build up,
and they change who you are and they change your life.
So, that's the quote for today, I have anybody, last night we had a question that someone tried
to sneak in and I was gone already, so this person who tries to develop, helped develop electronics
which will benefit humanity, over is overwhelmed by the stress and competition within the field
and afraid that his morals, his or her morals will be crushed by envy and even greed to the point
that I'm a worse version of myself compared to when I was born, it's just likely to happen.
Hmm, I can't comment on whether that's likely to happen, instead of valid concern.
It's easy to get burnt out, this is why actions are always
always inferior to intentions, inferior to the state of mind or the quality of the mind behind them.
So, it's not so much about what you do, but about your mind state when you're doing it.
Or maybe more accurately, your mind state is more important because it informs what you do
and it affects what you do and it changes what you decide to do.
Remember how you do it, it makes change your goals.
Our unpleasant memories come a way back, or are they simply the working of jith and niyama?
Some say, your questions I have to criticize because they don't seem all that practical.
They're very difficult and I'm happy for the challenge, but I don't understand what the benefit
of getting the answers of that is. The signs are swaging your doubt temporarily,
only to rear its ugly head the next day, or maybe you just like the challenge, which is good.
But honestly, I think I'm going to have to start calling you out on it and saying let's focus
on practical questions. You can explain to me why that's an important practical question,
or maybe to explain it in a way that makes it more practical. Also, in a way that most people
here can understand it because even I probably the only one to understand what the heck you're
talking about. Are the teachings of the dhamma from Prince Siddhartha? Well, he wasn't a prince
anymore, he was a Buddha, he was a setting. But it's the same person, but not the guy in the book,
Siddhartha. Siddhartha is a book based on the Buddhist life, but it's more Hindu than Buddhist,
really. Is it useful to share a merit with hungry ghosts, even if one is not sure they exist,
and so how does one share merit? Well, if you're not sure something exists, there's an element
of, probably you can argue there's an element of, while it's lacking wisdom, it's probably inferior.
All right, let me maybe that's too harsh. There's that question from doing something without any
evidence. But I think you can argue that we have the evidence of other people who have said that
these beings exist. And so a sort of a appreciation of their authority on that appreciation,
you accept, and you do. Because for the most part, it's really not about the results. It's about
your intentions. What are your intentions with your actions? And not only intentions, but what is your
more clearly? What is your mind state when you do something? Are you doing something with anger?
Well, that's not good. You're doing something out of greed or that's not good.
You're doing something really thinking that there's beings there. If it's your thought that they
exist and you do things for them, like kids who look after their imaginary friends or their
look after their stuffed animals, there's some element of delusion there for sure, but
there's also quite a bit of goodness in the sense of cultivating wholesome mind state.
And you could probably argue that even if you were acting, but acting in the sexual way,
yeah, I don't know about that, I think you have to be careful of the delusion aspect.
With hunger, we have a sense that they do exist. So we give kind of, and even, and it's not
even so much a matter of if they exist, it's a matter of whether they're there. Because they may
be hanging around, because there may be hunger, go sang around. We always remember them and make
them feel good by sharing the mirror with them, just in case they're hanging around.
Looking at Mahasi Saiyadha's teachings and seeing that there's complete the solution of
psychophysical phenomena. It seems counterintuitive. It doesn't seem to me that anything disappears
without a trace. Is that something to look for? Oh, yeah. Yeah, it's hard for Westerners, we think
too much. It's hard for us to imagine such a thing because science has told us it's not like that.
They have to change the way you look at reality. Reality is not sings, it's not atoms. It's not
entities. Reality is experiences, and you'll see these experiences arising and ceasing every moment.
In order to see that, you really have to get into meditation. You really have to get into this
paradigm of looking at the world in the point of view of momentary experiences, without any
frame of reference from even that you know the room is around because eventually when you
meditate, you even forget that you're in this room and you don't remember which way you're sitting
and the room disappears. The room didn't exist in the first place, it's just a content.
I'm aware, I perform as a bass player. I try to be mindful, but I'm aware that musical performances
and dancing are often regarded as unwholesome. I press on with my attempt to be mindful during this
or is it best to try and move away from this profession. I did. I was a guitarist and musician
and heavily into classical music and sort of classic rock music and classical music.
I gave up all my, gave away all my CDs, gave away my $1,200, 1976 Les Paul Deluxe.
That was hard. But not really, I suppose, because I wasn't ever in doubt. I was
anguish, but it was a good one. It was a strength to get rid of it to say to myself,
I don't need this anymore. I don't need this crutch anymore. To be clear in the mind that
this is something I like, something I want to do. That doesn't make me happy. That one thing
has never helped me. It's never made me a happier person. It doesn't actually made my life better.
It doesn't actually made me more at peace with myself, more content. It's in fact made me more
addicted to things, addicted to one thing. You don't have to give up music to me a Buddhist
or even be a Buddhist meditator, but certainly it helps. More of these things you can give up.
More clear your mind is, the more content you'll be, the more at peace with yourself you'll be.
You'll go through a period of withdrawal and you'll experience withdrawal consistently again and
again because you're so accustomed to it. It's so much a part of who you are.
But, you know, music is such a silly thing. I had this argument recently with my brother when
he came home and he was ever arguing how our meditation was actually healthy. My sister got into it
as well. She has read these studies about her music, but it's so silly because music is such a
silly thing. It's a manipulation. It's one of these things where we just
we manipulate. It's like food. It's like you decorate this food out on the plate. It's not
scientific in any way, shape, or form to say that music is healthy. It's really just to say that
manipulating the mind into convincing certain emotions is healthy for the body, I guess.
And there's a way of relaxing at temporarily, just as a massage is.
But, you know, if that was the argument we were going to make, then we should get scientific
about it. We should forget about music, which is all just subjective. And the thing about subjective
about subjective things, like music, is that you don't really know which is being, which is
healthier. And you're not really concerned with which is healthier. You're much more concerned with
taste and preference and very much what ego. Classical musicians are egotistical about their
classical music. Any kind of, any genre of music has its snobs and its aficionados. What's
all about us? It's all about as much more music. Yes, I relate to this. I hear it. It's cultivating
an identity based on the music. So we wanted to get scientific about the good of music.
We wanted to argue as a good of it. And we should forget about music and just concentrate like
these binaural beats have you heard about? I haven't studied it too much, but people keep asking
me about it. Some of this is helpful for the mind to be useful. None of it really works.
It can't really work because it's nothing to do. Nothing to do with wisdom, nothing to do with
knowledge, insight. It's all artificial. Yeah, it makes you feel good. Yeah, it might be healthy
for a temper. It doesn't fix the problem. He's with the Buddha found. He found that you have to go
deeper. That wisdom. What's unique about Buddhism is that your own wisdom and your own understanding
is what sets you free. Nothing else. How do we know the mind is not just the brain. Maybe the
brain can be aware of itself. The brain doesn't exist. It has to be the other way around because
the brain is just a concept. Yeah, I get in trouble when I say that. Sounds like it. Sounds like
some religious idiot. It's such a long argument, but there's no reason to think that the brain exists.
It's a paradigm that works practically, but not in ultimate reality. The brain doesn't exist.
What is the brain, but cells, which are made up of molecules, which are made up of
atoms, which are made up of subatomic particles, which are made out of nobody knows what?
Quantum fields is the latest thing, whatever the heck that means. Quantum field, I guess,
just means fields of probability. What have we come down to? What do these things really exist?
The brain isn't. The brain isn't thinking. When we talk about the brain, we mean a bunch of
cells that a lot of fat and protein and blood, none of these things are aware of these things
even exist. They're a part of the conceptual framework of the universe.
How I've been tedious is it to be ordained at a young age. These days, many like to criticize
our daily kids. Yeah, well, I mean, the kids don't want to ordain you. You should be
ordained for very much the wrong reasons. But then I think not to say no ordained for the wrong
reason. So he became an aura hunt. So it's not always wrong. Let's say there's probably too much
emphasis on it. I didn't know that that was a thing in Sri Lanka, but in Thailand, it's a big thing.
And it's a huge problem, both in Sri Lanka and Thailand, for the abuse, sexual abuse even.
Do you think there's a scandal in the Catholic church? Heck, someone should investigate the scandal
and Buddhism. So much and open it up. Someone, you know, why am I not doing it right?
Why are we not exposing the horrible atrocities that are being committed by Buddhist monks every day?
quote unquote, Buddhist monks, you know, they're not real monks. It's happening.
And you want to get outraged. Well, that's the things Buddhist don't get outraged.
It's not something we should keep quiet about. It's happening. I don't have proof. I actually
don't know that it's happening, but I've heard a lot of circumstantial evidence to suggest that
it is happening. A lot. I guess that's the thing is, without any evidence, what do you do?
And then we're getting all sorts of duplicates here.
I don't talk about my own state of mind. It's just not appropriate for monks to do.
And it just gets talking about yourself. My teacher said a bell doesn't ring itself.
It's kind of a, I don't know. And it just doesn't feel right. Confident and saying that I don't
talk about myself. I recall reading a teaching about when you conquer an enemy. It just leads to
more hate from the conquered. I think that's the double-ponder. Maybe the chapter can probably
both. And this seems to be what is happening with the extremists today. Those physical ground
blow up more people. What is a Buddhist approach to minimizing this kind of hate?
Well, meditate. My teacher said he said, these people are meditated. They wouldn't have flown
planes into the towers. I was meditating. I was doing a meditation course when
the world trade towers came in. When we went to see our teacher and told him and
just what he said. And that's about it.
Yeah, I mean, if you want to go beyond meditation, there's really no, there's nothing else really,
you know, I guess you talk about how meditators approach these people, approach other religious
people. Now everyone practices meditation. I think Master University, I've had actual Muslims
come and meditate with me. Women wearing this hijab. Nika, I don't remember what they're called.
hijab, I think it's called. Seeks, I had to seek guys that actually had the daggers
come and sit and meditate with me. A lot of people, Asian people, Western people, some of the
some of the catering crew work at the various restaurants at the University came to visit.
Lots of different people.
So if we can get people to come and meditate, I even had one Muslim guy come and sit with me
and clearly felt a bit threatened by me in the sense, not even a threatened, but
felt it felt that I was a bit of a threat in the sense that I think he made me
had heard that some of the Muslim women were coming and meditating with me.
And so he was trying to figure out, trying to get clear in his mind how I was wrong to do what
I was doing. I mean, people do this, you know, they know you're wrong. So they come and they try to,
you know, just reaffirm that for themselves by asking questions and until they get a sense that
yeah, this guy really is wrong. I don't know what I don't know if you got that in the end, but
the thing is what we can do is we can work towards people becoming more meditative.
That's really the answer to all these things. We don't have to
I was thinking about the revolution, you know, in America and this Bernie Sanders and so on.
He's too angry. I think that's of clear criticism I would have when we talk about this.
And a lot of people in America are angry.
You have to accept that there's going to be people in power who are going to control things
and manipulate things. But the question is whether they're doing it out of for good intentions
or for bad intentions. The only way you get people with good intentions is if the society expects
good intentions, if there is an atmosphere of good intentions, like I was thinking about
how much better off the US is than some of these like Saudi Arabia or some other countries that
are even worse, Iran, not in Iran. Where the governments are just terrible North Korea,
and the reason has got to be because of people's sense of what is good and what is right.
People don't stand for certain things. It's not to say that America is a great place, but
people don't stand for certain things. They know to some extent what's right and what's wrong.
I think to a better extent than some of them. I mean, that's not true for some of these countries.
People are just being oppressed. And it's terribly naive to say, but I think it is the way
moving forward. I think eventually, God maybe it's just naive to say. Because Buddhist did that
in India. In India, the Buddhist monks were doing their thing and they all got slaughtered, right?
I don't know. I think there's an argument to be made for purity, and that Buddhism was
becoming very corrupt in India. And if it hadn't been corrupt during the time when it wasn't
corrupt, it actually didn't change the minds of the dictators. So Ahsoka was called Kalasoka,
which means black Ahsoka. He was a brutal dictator who became a Buddhist and gave it all up. He even
gave up eating meat or reduced eating meat because he didn't want to have animal skin.
And then some thoughts.
When meditating, can we give detail the thoughts to increase the concentration,
and so are saying thoughts and those things. Give detail the thoughts to increase the
concentration. We're not trying to increase concentration. What you mean is to be able to control,
that's really what you're saying because you want to control things. As I understand them,
usually people who ask this are trying to gain control of a chaotic mind. And we don't want to do
that. That's counterproductive. The idea is to learn to see that yes, the mind is chaotic. The mind
is unpredictable. Why? So that you eventually let go of the mind. You can stop trying to fix it.
Just stop trying to make it better. Let's just let go. Just throw away the mind in me.
I feel that the meditation bleeds into the rest of my life.
I'll be walking around and click back to my breath and sometimes be blissful.
This is normal. The question is, is this normal is really weird, you know?
What does it mean normal? I guess the question is, am I going to go crazy?
Something going wrong? Is this a sign of good practice or bad practice?
It's not really a sign of anything. I mean, it's a sign probably that you're cultivating
certain states, tranquility that lead to bliss. It's probably a sign of the whole
soonness of what you're doing. The result of wholesomeness is happening.
It's made up for a disease for the benefit of a disease for some beneficial. Yes, it's
of course. It's beneficial to you first of all. It's beneficial to all your relatives.
It's beneficial to the world. It's beneficial to the deceased.
First of all, you could argue it's beneficial in the sense that
knowing that our relatives are going to do for us makes us happier when we die.
When we pass away knowing that our relatives do these things for us, it gives us some sense of
peace. You're sure extending their life and their legacy by doing things in their name.
But as we said, if they're hanging around after they died, they might even actually see you
doing these good things, which makes them feel happy. If they don't, apparently it's the
there are certain angels that go around telling other angels that their relatives have done
things in their name. So that, apparently, is a thing as well. Anyway, it makes them feel happy.
Has the potential to make me feel happy?
Okay, we're all caught up. Let's stop there. Say your questions for tomorrow if you got more
or post them and I'll try to get back to you tomorrow. That's all for tonight.
Thank you all for tuning in. Have a good night.
you
