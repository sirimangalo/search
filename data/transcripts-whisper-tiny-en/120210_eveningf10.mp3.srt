1
00:00:00,000 --> 00:00:24,920
So today we come back again for our daily group meditation, we've been talking about the

2
00:00:24,920 --> 00:00:33,520
various things that we need to keep in mind and develop and cultivate and deal with

3
00:00:33,520 --> 00:00:38,560
in our meditation in order to develop, in order to progress.

4
00:00:38,560 --> 00:00:52,840
I think the next stage in our discussion is in regards to the final

5
00:00:52,840 --> 00:01:04,800
realization of the truth, what we're aiming for in the practice, and the point

6
00:01:04,800 --> 00:01:11,480
isn't to expect to reach the goal today or tomorrow or whatever, but if we

7
00:01:11,480 --> 00:01:24,400
know what the aim for, if we know what direction our practice has to take, then

8
00:01:24,400 --> 00:01:33,800
we can adjust and we can augment our practice and our confidence in our

9
00:01:33,800 --> 00:01:44,760
practice, then progress at a steady pace, otherwise if we don't quite know

10
00:01:44,760 --> 00:01:50,760
where we're headed and we always have this question of, is this it, is that it,

11
00:01:50,760 --> 00:01:56,800
am I getting there, am I getting closer, and then we'll be like a person walking

12
00:01:56,800 --> 00:02:03,240
lost in the woods, really very hesitant, not knowing which way to go in all

13
00:02:03,240 --> 00:02:08,960
time, so it's important to understand what we mean by enlightenment, by

14
00:02:08,960 --> 00:02:20,040
realization of the truth, realization of the truth is a mode chat, a mode

15
00:02:20,040 --> 00:02:27,040
chat, you can see, you can see, body, like the body tree, it's a place where the

16
00:02:27,040 --> 00:02:36,280
Buddha became a Buddha, Buddha had body, same root, budge, budge, which means to

17
00:02:36,280 --> 00:02:53,120
become awake, to wake up, so we can ask, did the Buddha have a teaching on

18
00:02:53,120 --> 00:02:59,800
what is enlightenment and what is the factors of enlightenment and the answer is

19
00:02:59,800 --> 00:03:05,960
yes, I'm going to give a very powerful teaching on the more jungle, the

20
00:03:05,960 --> 00:03:18,480
hunger, the factors or constituents or the requisites for enlightenment, and it

21
00:03:18,480 --> 00:03:26,000
paints a very clear picture of the direction our practice has to take, all the

22
00:03:26,000 --> 00:03:30,680
way to the end, it's not just giving us a preliminary practice like this at

23
00:03:30,680 --> 00:03:34,880
the batana, we talk about the foundations of mindfulness, but when we believe

24
00:03:34,880 --> 00:03:38,680
that off there we don't have a clear picture of where we're going, so at this

25
00:03:38,680 --> 00:03:49,080
point, now that we're good at practicing in a long way, developing and honing

26
00:03:49,080 --> 00:03:55,880
your skill in the practice of sati batana of the mind, we have to look ahead

27
00:03:55,880 --> 00:04:02,720
to where we're aiming for, the first boat jungle, the first factor of enlightenment

28
00:04:02,720 --> 00:04:13,400
is actually mindfulness, and so it's not for no reason that we were emphasizing,

29
00:04:13,400 --> 00:04:18,320
we've been emphasizing the foundations of mindfulness the whole way, this is

30
00:04:18,320 --> 00:04:22,880
the foundation, this is the beginning point, this is where we start in our

31
00:04:22,880 --> 00:04:33,520
practice, so we can understand from the teaching that from the Buddha's point of

32
00:04:33,520 --> 00:04:40,880
view enlightenment starts with mindfulness, the attainment of enlightenment

33
00:04:40,880 --> 00:04:49,480
awakening starts with mindfulness, we'll remind full of the body, mindful of the

34
00:04:49,480 --> 00:04:53,960
feelings, mindfulness of the mind, mindfulness of our emotions and mind

35
00:04:53,960 --> 00:05:03,400
states and so on, there are sensors and aggregates, just these simple mundane

36
00:05:03,400 --> 00:05:10,120
things, this is where enlightenment starts, it really sets the field of

37
00:05:10,120 --> 00:05:20,560
framework for our practice, because if you ask an ordinary untrained person

38
00:05:20,560 --> 00:05:25,560
where to start looking for enlightenment, they'll never think to look inside

39
00:05:25,560 --> 00:05:29,760
themselves, maybe look inside themselves, but they'll think it must be

40
00:05:29,760 --> 00:05:34,360
something special, not seeing, hearing, smelling, tasting, feeling and

41
00:05:34,360 --> 00:05:40,200
thinking these aren't special, they'll talk about the third eye or the chakras or

42
00:05:40,200 --> 00:05:45,840
kundalini or they have all these mystical words in different traditions, God is

43
00:05:45,840 --> 00:05:52,000
wonderful, enlightenment is awakening is coming to realize God, the love of God

44
00:05:52,000 --> 00:05:58,240
or becoming one with God or so, if you look at the pattern it's to find

45
00:05:58,240 --> 00:06:03,280
things outside of ordinary mundane experience, which is why no one ever becomes

46
00:06:03,280 --> 00:06:07,560
enlightened, because it's right there under their nose and they look everywhere

47
00:06:07,560 --> 00:06:15,400
else, it's hidden, we think enlightenment must be super mundane, so we

48
00:06:15,400 --> 00:06:22,840
look at it, no that's mundane, it can't be it, but in fact the super mundane

49
00:06:22,840 --> 00:06:27,320
comes from letting go of the mundane, the only way to let go of the mundane is

50
00:06:27,320 --> 00:06:33,320
to understand that it's not worth clinging to, not worth pushing away, not worth

51
00:06:33,320 --> 00:06:40,080
pulling, and understanding that everything about us is mundane, all we are is

52
00:06:40,080 --> 00:06:45,000
the seeing, hearing, smelling, tasting, feeling and thinking, the body, the feeling

53
00:06:45,000 --> 00:06:50,800
is the mind and the dumbah, that's all we are, that's all that our experience,

54
00:06:50,800 --> 00:06:55,000
all that our existence, everything, whether it be an experience of God and

55
00:06:55,000 --> 00:07:00,120
experience of heaven and experience of self and experience of the third

56
00:07:00,120 --> 00:07:04,640
eye or the shock graduate or so, and it's all just seeing, hearing, smelling,

57
00:07:04,640 --> 00:07:13,720
tasting, feeling and or thinking, no more, no less, all of it, any special

58
00:07:13,720 --> 00:07:22,880
experience is only there, and so to understand reality we have to understand

59
00:07:22,880 --> 00:07:33,000
these things, to awaken to reality, we can't be looking for something special

60
00:07:33,000 --> 00:07:40,800
because in the end it's not special at all, we have to look at everything until

61
00:07:40,800 --> 00:07:45,880
we see that everything is, in a way everything is special, everything that arises

62
00:07:45,880 --> 00:07:50,040
especially because it's a reason, nothing else is a reason only this is a

63
00:07:50,040 --> 00:07:57,400
reason so this is special, and when we treat it as equal to everything else give

64
00:07:57,400 --> 00:08:02,040
it as much importance as everything else and we don't cling to it or anything

65
00:08:02,040 --> 00:08:10,600
else, so mindfulness is the beginning, now what does mindfulness lean to,

66
00:08:10,600 --> 00:08:17,520
mindfulness leads to something called dhamma vitya which is translated as

67
00:08:17,520 --> 00:08:29,960
investigation of reality or investigation of truth or the knowledge, the

68
00:08:29,960 --> 00:08:41,240
study, the attainment or acquisition of knowledge about reality, this is what

69
00:08:41,240 --> 00:08:45,240
mindfulness leads to, this is what you should get about the practice, put in

70
00:08:45,240 --> 00:08:51,360
learning about yourself, learning more about yourself, your experience and the

71
00:08:51,360 --> 00:08:57,840
world around you, learning how your mind works, learning how your body works,

72
00:08:57,840 --> 00:09:07,800
learning how they work together, learning what leads to what, this is the

73
00:09:07,800 --> 00:09:14,880
second factor of enlightenment, is this learning that we do, so you should be

74
00:09:14,880 --> 00:09:22,920
thinking every time I sit down, am I learning something new or not, you shouldn't

75
00:09:22,920 --> 00:09:25,960
be thinking when I sit down and it isn't more happy and more peaceful than

76
00:09:25,960 --> 00:09:32,520
last time, am I more blissful and more calm and so on, you shouldn't be thinking

77
00:09:32,520 --> 00:09:38,480
like this isn't the right direction, the right direction is am I learning

78
00:09:38,480 --> 00:09:42,720
something every time I sit down, because every time you sit you should learn

79
00:09:42,720 --> 00:09:48,480
something new about yourself and it should challenge you, your old ideas about

80
00:09:48,480 --> 00:09:52,200
what is meditation should be challenged, your old ways of meditating should

81
00:09:52,200 --> 00:10:00,760
be challenged, very easy to get in a rut, anybody who attains some special

82
00:10:00,760 --> 00:10:10,120
meditation experience and continues to attain so they get in a rut, and means

83
00:10:10,120 --> 00:10:15,360
they're stuck at that level, the only way forward for them is not to develop

84
00:10:15,360 --> 00:10:22,920
that more and more, but is to challenge yourself, to go deeper in and look at

85
00:10:22,920 --> 00:10:27,160
their experience and understand their experience, when someone practices

86
00:10:27,160 --> 00:10:32,160
summit meditation for example, the only way for them to move on is not to

87
00:10:32,160 --> 00:10:36,800
develop more and more summit, but to eventually look at the summit

88
00:10:36,800 --> 00:10:42,280
and meditation, when you have calm or when you have happiness, focus on mind to

89
00:10:42,280 --> 00:10:49,400
look at that and learn and ask yourself what is focus, what is calm, then you

90
00:10:49,400 --> 00:10:55,720
come to see that it is impermanent unsatisfying and uncontrollable, and then

91
00:10:55,720 --> 00:11:05,320
you move on, this is number two, number three, what does it lead to when you

92
00:11:05,320 --> 00:11:09,040
learn stuff about yourself, what good is that, how does that lead you closer to

93
00:11:09,040 --> 00:11:17,760
awakening, it leads to energy, it leads to effort, what you should what you

94
00:11:17,760 --> 00:11:24,120
should gain from the practice from seeing the truth is more energy, you're more

95
00:11:24,120 --> 00:11:29,800
alert, you're more focused, and you're more energetic in your awareness and

96
00:11:29,800 --> 00:11:36,480
your clarity of mind, in your practice of mindfulness, so you're developing

97
00:11:36,480 --> 00:11:40,080
mindfulness in the beginning, it's difficult, but what you should see as you

98
00:11:40,080 --> 00:11:47,320
progress is that because of the confidence that you gain, and the focus that

99
00:11:47,320 --> 00:11:52,920
you gain, so instead of wasting your energy on doubting or thinking about this

100
00:11:52,920 --> 00:11:57,720
or that, am I doing the right thing, am I practicing correctly, once you

101
00:11:57,720 --> 00:12:03,680
realize the truth and more and more understand that's useless, this is useless

102
00:12:03,680 --> 00:12:08,480
stop that, that's benefiting, then you focus your energies and you say this is

103
00:12:08,480 --> 00:12:15,000
useful, then you gain confidence and focus and as a result your mind becomes

104
00:12:15,000 --> 00:12:22,240
charged and you have this great energy and your mindful continuously, you're

105
00:12:22,240 --> 00:12:38,640
able to be mindful in succession because of your effort, and because of the

106
00:12:38,640 --> 00:12:43,720
focus your effort becomes focused, instead of having to push and push and push

107
00:12:43,720 --> 00:12:48,960
with no benefit, you start to refine your effort as well, so really a

108
00:12:48,960 --> 00:12:54,200
bone jungle, the enlightenment factor of effort isn't just pushing and pushing

109
00:12:54,200 --> 00:13:01,640
and pushing, but it means as you are strong in mind, you're able to fight the

110
00:13:01,640 --> 00:13:07,760
defilements when bad things come up, you're able to push them away, you're

111
00:13:07,760 --> 00:13:14,280
able to step like dancing to avoid the defilements, so when the

112
00:13:14,280 --> 00:13:20,280
defilements might arise, you're able to see it clearly, see or hearing, and the

113
00:13:20,280 --> 00:13:27,520
defilement doesn't even arise, here you know what is a benefit and you know

114
00:13:27,520 --> 00:13:33,400
how to create wholesome thoughts and wholesome mind states, so you are quicker

115
00:13:33,400 --> 00:13:41,040
to create them, they come with more power and when they come you're able to

116
00:13:41,040 --> 00:13:48,160
keep them, after the effort in meditation practice that we're looking for is

117
00:13:48,160 --> 00:13:54,920
not pushing, pushing, pushing, it's keeping away bad states of mind, it's kind of

118
00:13:54,920 --> 00:13:59,400
like a kung fu, you could think of it as, not just pushing, pushing, pushing

119
00:13:59,400 --> 00:14:06,440
but having been able to dance, so that the bad things disappear and don't come

120
00:14:06,440 --> 00:14:15,040
and the good things appear and stay, you have a more powerful

121
00:14:15,040 --> 00:14:40,680
ability, your technique, your ability to arrange your mind in the right way, and

122
00:14:40,680 --> 00:14:46,800
there's ability inside, this is the effort to fight against, so bad things come up,

123
00:14:46,800 --> 00:14:51,400
you get skillful, you know the way you think of this, you have the skills in

124
00:14:51,400 --> 00:14:54,640
the kung fu, so when a bad thing comes up, you know not to get angry about it,

125
00:14:54,640 --> 00:15:00,640
you know how to deal with it, okay, angry, angry, and you're really good at

126
00:15:00,640 --> 00:15:07,040
this mindfulness, this is the effort that comes from the energy of the strength

127
00:15:07,040 --> 00:15:12,120
you might say, so in the beginning your mindfulness is quite weak, you say pain,

128
00:15:12,120 --> 00:15:16,800
pain, and it doesn't go away, eventually you get so good that you know exactly

129
00:15:16,800 --> 00:15:22,920
you dance with it and you surf on the pain, it's like surfing on the wave, you know

130
00:15:22,920 --> 00:15:28,120
when you go as a kid to play in the ocean the waves drag you under, you know when

131
00:15:28,120 --> 00:15:35,920
you become a surfer you know how to ride the wave, you get like that, this is what

132
00:15:35,920 --> 00:15:42,480
we mean by effort, but don't just pushing, but you get good at that being mindful, once

133
00:15:42,480 --> 00:15:48,880
you get good at being mindful and you have the strength of mind, it gets you in a

134
00:15:48,880 --> 00:15:56,640
rut as well, but this is a good rut, it's called a peity or a rapture, you imagine

135
00:15:56,640 --> 00:16:03,280
getting so good in developing it so much that it becomes a habit, I think because we

136
00:16:03,280 --> 00:16:08,800
have all these bad habits, so bad habits are getting angry, bad habits are being

137
00:16:08,800 --> 00:16:14,800
greedy, bad habits are saying that or wanting this or wanting that, I'm

138
00:16:14,800 --> 00:16:21,440
doing this or doing that, was it wonderful to think that you can have a habit of

139
00:16:21,440 --> 00:16:26,600
being mindful, a habit of seeing things clearly, so that when you're faced with

140
00:16:26,600 --> 00:16:35,400
any problem, you get into the habit of just being mindful, this is what it means by rapture

141
00:16:35,400 --> 00:16:42,240
because your mind gets this tendency, starts to develop it until it becomes kind of

142
00:16:42,240 --> 00:16:50,720
like a rapture, rapture meaning that your mind is stuck and fixed in it, and it

143
00:16:50,720 --> 00:16:57,760
starts to become second nature, this mindfulness, so that you can do it at all the

144
00:16:57,760 --> 00:17:01,140
time, and then when you're eating your mind full and you're bathing your mind

145
00:17:01,140 --> 00:17:04,560
full and you're going to toilet your mind full and you're just walking around

146
00:17:04,560 --> 00:17:11,720
the monastery in your mind, when you're waking up your mind full and you're

147
00:17:11,720 --> 00:17:20,080
falling asleep your mind full, it just starts to become second nature, this is

148
00:17:20,080 --> 00:17:29,160
rapture, and you get stuck in the rotten, because you get stuck in this

149
00:17:29,160 --> 00:17:36,360
rat of being mindful, because your mind is more and more and more and more and more

150
00:17:36,360 --> 00:17:40,920
and more and more clear, your mind starts to calm down, and then there's this

151
00:17:40,920 --> 00:17:46,640
calm that kind of lasts, it's necessary that you really do feel calm, it's

152
00:17:46,640 --> 00:17:53,840
necessary to focus on the calm, but it starts to become kind of natural quiet, this

153
00:17:53,840 --> 00:17:59,920
is called bussity, your mind becomes tranquil, whereas before you might have been

154
00:17:59,920 --> 00:18:08,440
thinking a lot, once you really get into the practice, there's not much to

155
00:18:08,440 --> 00:18:17,920
think about, there's only like a soldier left doing performing the drills, doing

156
00:18:17,920 --> 00:18:23,520
your duty, following the schedule, the mind becomes very well trained, this is

157
00:18:23,520 --> 00:18:31,400
the point, like a soldier, the mind becomes very well trained, whereas before it

158
00:18:31,400 --> 00:18:37,720
was very untrained and doing this, that and running here and running, it becomes quite

159
00:18:37,720 --> 00:18:47,440
trained, so it gets quiet, also in the beginning it was fighting against it, and

160
00:18:47,440 --> 00:18:53,000
as you practice again and all habits are replaced by the new habit of mind

161
00:18:53,000 --> 00:19:02,200
from this, it quiets down, all of the rebellion it quiets down, the mind begins

162
00:19:02,200 --> 00:19:08,280
to order itself, so say no, this, then this is running away, no, it's

163
00:19:08,280 --> 00:19:25,800
watered and orderly, your mind is untracked, bussity, then samadhi, then the

164
00:19:25,800 --> 00:19:32,400
mind becomes balanced or focused, samadhi, we always translate as concentrated,

165
00:19:32,400 --> 00:19:39,480
but I think that really misses the point, it's samadhi, samadhi, samadhi is the same

166
00:19:39,480 --> 00:19:52,160
more, level, and this is where your mind becomes very, very clear, and two, and you

167
00:19:52,160 --> 00:19:58,160
can think of like a musical instrument that's now perfectly in the tune, you're

168
00:19:58,160 --> 00:20:05,800
going to play the guitar, for example, you can tell when it's in tune and out of tune,

169
00:20:05,800 --> 00:20:11,120
and as you develop and develop and quiet, your mind starts to become in tune, so every

170
00:20:11,120 --> 00:20:21,280
time your mind points like ringing a bell, being, catching it, perfectly catching it with

171
00:20:21,280 --> 00:20:34,120
just enough effort, just enough concentration, this is a focused mind, the mind is able

172
00:20:34,120 --> 00:20:40,640
to catch, when you say seeing, really are aware of this, hearing, really, this takes

173
00:20:40,640 --> 00:20:46,080
time, something you have to understand, you don't come here mindful, though, we don't

174
00:20:46,080 --> 00:20:51,280
give you the technique, you say, I get it and being you do it in your mind, you say, I get

175
00:20:51,280 --> 00:20:59,200
it, oh no, but I can't do it, then you work and you work and you practice, when you

176
00:20:59,200 --> 00:21:04,640
practice, when you start to think, what am I doing this for, you don't realize like the movie

177
00:21:04,640 --> 00:21:10,960
the karate kid, you never saw the original karate kid, this movie, he has some paint

178
00:21:10,960 --> 00:21:16,960
defense, why am I painting the fence, he hasn't waxed the cards, why am I waxing it, he

179
00:21:16,960 --> 00:21:22,320
comes back and he gets angry and he says, look, I'm happy to help you out by painting

180
00:21:22,320 --> 00:21:28,720
your fence and waxing your cards, but when am I going to learn karate, he says, show

181
00:21:28,720 --> 00:21:33,760
me how to wax, so I show me how to paint the fence and he goes up and up and then he punches

182
00:21:33,760 --> 00:21:41,920
them and he blocks it with his fence painting motion, and then how do you wash wax

183
00:21:41,920 --> 00:21:47,400
cards and he shows them how to wax and he punches them and he blocks it with his

184
00:21:47,400 --> 00:21:54,840
wax, car waxing motion, very, very good, good analogy for what we're doing, what are

185
00:21:54,840 --> 00:22:03,680
we doing, seeing, seeing, paying, paying, what's it for, we're getting better, better

186
00:22:03,680 --> 00:22:16,720
at it, what's it for, we're blocking the defilements, we're stopping our projections, our

187
00:22:16,720 --> 00:22:24,200
judgments, and only when you get really, really good at it, do you see the point you

188
00:22:24,200 --> 00:22:30,280
do understand, do you realize what you've been doing all this time, you've been

189
00:22:30,280 --> 00:22:42,840
training yourself, not you're training yourself to stop, hitting yourself to stop, and

190
00:22:42,840 --> 00:22:49,440
it can be coming addicted to things, stop your judgments, your partiality, training yourself

191
00:22:49,440 --> 00:22:57,280
to just see things as they are, sounds so simple, I have the easy job to teach you how

192
00:22:57,280 --> 00:23:05,560
to do it, it's very easy to teach you, you just have to say the right things, in practice

193
00:23:05,560 --> 00:23:17,600
you have to actually do them, that's the hard part, and what finally does the focus mean

194
00:23:17,600 --> 00:23:22,880
to, what is it, what good is it to be focused, what good is it to be, have this clarity

195
00:23:22,880 --> 00:23:30,400
of mind, the final boat genre, the final factor of alignment is equanimity, there's a lot

196
00:23:30,400 --> 00:23:36,000
of talk about equanimity and Buddhism, we have to understand equanimity, it's not the

197
00:23:36,000 --> 00:23:43,280
dummy equanimity of like a cow or something like a goat, you know they can be very angry

198
00:23:43,280 --> 00:23:48,960
at times, but you look at animals and they seem very equanimous because they have so

199
00:23:48,960 --> 00:23:53,840
much delusion that they can't really think clearly, they are unable to stay angry or greedy

200
00:23:53,840 --> 00:24:00,720
for a long time because they have too much delusion, so they just feel dumb, and we can

201
00:24:00,720 --> 00:24:07,240
feel dumb when we're using the computer for example, you just feel dumb, equanimous,

202
00:24:07,240 --> 00:24:17,280
so, but this isn't what is meant by equanimity, equanimity is like your intellectual perception

203
00:24:17,280 --> 00:24:24,920
of things, because we always have such a bad, we give such a bad rap to intellectual

204
00:24:24,920 --> 00:24:30,080
lies, you know, which is when we say, no, don't intellectual lies, why, because that's

205
00:24:30,080 --> 00:24:35,720
where all of our judgments come from, that's where all of our views come from, when you

206
00:24:35,720 --> 00:24:42,280
practice on the lower level of experience, all of that gets filtered out until your intellectual

207
00:24:42,280 --> 00:24:51,040
activity is totally neutral, you become kind of dumb in a sense, all of that chat intellectual

208
00:24:51,040 --> 00:25:00,160
chatter and all the grief it is, that's no good, you know, all levels out, you say, this

209
00:25:00,160 --> 00:25:08,600
is good, that's good, this is bad, that's bad, you say, oh, this is seeing, and intellectually

210
00:25:08,600 --> 00:25:13,440
you actually do that, and in the beginning you have to just shut up, shut up, shut up, put

211
00:25:13,440 --> 00:25:23,160
it aside, stop it, when you get to obey or pay cow, which where you take a boat, you don't

212
00:25:23,160 --> 00:25:32,880
have to do that anymore, it's kind of amazing actually, your mind is suddenly just seeing,

213
00:25:32,880 --> 00:25:44,280
but that's, oh, let's see, but here, there's nothing else in the mind, when you get to

214
00:25:44,280 --> 00:25:53,960
this point that your mind is just not even training anymore, it's knowing, that's the

215
00:25:53,960 --> 00:25:58,720
moment, that's the time when you're on the edge of the mountain, when you're ready to

216
00:25:58,720 --> 00:26:10,840
enter into an environment, when the mind will slide off, it's kind of like, the ordinary

217
00:26:10,840 --> 00:26:26,440
mind is like velcro, or like, if you have two substances with glue, or with sticky, something

218
00:26:26,440 --> 00:26:36,800
sticky in between them, like if you put jam, your hands are covered in jam, you get sticky,

219
00:26:36,800 --> 00:26:42,480
and it seems like that's the nature of them to stick together, and they think, oh, the

220
00:26:42,480 --> 00:26:47,400
hands are all sticky and they're stuck together, but when you pour water over them, you

221
00:26:47,400 --> 00:26:55,000
think to slide off, kind of think of some sticky things, some kind of bad example, but you

222
00:26:55,000 --> 00:27:06,720
have this glue, you know, when you pour water under the glue, just dissolves in the water.

223
00:27:06,720 --> 00:27:12,120
This is kind of, what, this is kind of how enlightenment occurs, how the one goes into

224
00:27:12,120 --> 00:27:18,200
any bond, it's not like suddenly broken or something like that, it's like, or you suddenly

225
00:27:18,200 --> 00:27:27,560
pull away from some sort of, you slide off of it, slowly, slowly, the water dissolves

226
00:27:27,560 --> 00:27:37,520
the glue, and show the mind's life off, because that's what's happening, right, you're

227
00:27:37,520 --> 00:27:43,840
clinging is the only less and less until you're just seeing things as they are, just

228
00:27:43,840 --> 00:27:49,760
experiencing things as they are, and less and less clinging, less of that's clinging

229
00:27:49,760 --> 00:28:03,080
into the mind's flies, and enters into Nirvana, this is the experience, I can't explain

230
00:28:03,080 --> 00:28:09,680
too much about Nirvana only, one thing my teacher always said, always keep in mind when

231
00:28:09,680 --> 00:28:17,880
you're wondering about Nirvana, this is not that, you have a question about Nirvana, is

232
00:28:17,880 --> 00:28:23,000
this Nirvana or is this what is this, because you're thinking is this the special thing

233
00:28:23,000 --> 00:28:31,360
that I'm looking for, always make your answer be, this is not that, because it's not, that's

234
00:28:31,360 --> 00:28:37,680
all I can say about Nirvana, if you keep that in mind you'll be okay, this is not that,

235
00:28:37,680 --> 00:28:42,960
then you'll always remind yourself you won't get, because otherwise you'll get stuck on

236
00:28:42,960 --> 00:28:52,040
something and it's not that, when you realize me by, I guess you can say enough more, there's

237
00:28:52,040 --> 00:28:59,240
no seeing, no hearing, no smelling, no tasting, no feeling and no thinking, there's

238
00:28:59,240 --> 00:29:11,880
not even any, any mind has become extinguished, there is a mind, but it's just technically

239
00:29:11,880 --> 00:29:19,600
really the mind has become free, because if you think the mind always has to take an

240
00:29:19,600 --> 00:29:24,920
object, right, if you don't think of a mind without an object, you don't really have

241
00:29:24,920 --> 00:29:38,400
a mind, but that's this, this is a mind which has no object, they say it has nibana as

242
00:29:38,400 --> 00:29:46,680
an object, what does that mean, what does it mean to say it has the unbinding as an

243
00:29:46,680 --> 00:29:54,840
object, so whatever object your mind takes and you think is this it, tell yourself,

244
00:29:54,840 --> 00:30:06,240
what is this, what is this is not that, but here we have a, we have a path and I think

245
00:30:06,240 --> 00:30:10,680
it's quite clear where it's leaving and it's quite reasonable to assume that this path

246
00:30:10,680 --> 00:30:18,480
is at least worth checking out, and if we're solving development mindfulness we've seen

247
00:30:18,480 --> 00:30:26,360
does it lead to the number each A does it lead to really a PPT, PPT, somebody will pay

248
00:30:26,360 --> 00:30:32,540
that, they lead to this one and if they do and when we see that they do then you don't

249
00:30:32,540 --> 00:30:44,120
need to doubt anyway, so anyway, this is a kind of another hint or another glimpse, another

250
00:30:44,120 --> 00:30:56,280
other in guide on which to base our practice, now we get to do the real work, get on with

251
00:30:56,280 --> 00:31:03,280
our practice, now we can start mindful frustration walking in today.

