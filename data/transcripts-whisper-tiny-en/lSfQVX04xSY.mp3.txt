On meditation and Buddhist practice,
the emphasis on practical application of the teachings.
So if you have any questions about your practice,
you're welcome to post them in the chat at any time.
First 15 minutes from now until quarter after the hour,
we will spend in silent meditation.
So once you've asked your question,
you can either do walking or sitting or walking and sitting.
And at quarter after the hour,
we will collect the questions that have been posted.
And begin to ask them with, again,
preference on questions that are practical in nature
and provide some guidance for people in their own practice.
So from now until quarter after silent meditation.
For more information,
You
You
You
You
You
You
You
You
You
You
You
You
You
You
Okay, we're back
So
So from here on
We'll restrict the chat to questions only you have any questions again
Post them in the chat at any time
You don't have any questions just sit back and stay mindful
Thank you one day we do have questions
When I know to wholesome thought it disappears should I keep noting or let the thoughts stay for a while?
Right, you're not
You're
Focuses and done making the thought go away
I
There's no such thing as a wholesome thought wholesome is the quality of mind
When thinking or
engaging in any activity
but alsoness requires mindfulness or
Or
Mindfulness is the
clearest and purest sort of wholesomeness
So whatever it is you're talking about as being wholesome thought is never going to beat being mindful
Seeing the Buddha said better to live one day seeing the arising and ceasing of phenomena than to live a hundred years and never see it
So seeing those thoughts disappears
If they do that for one day, it's better than you lived a hundred years and never saw it
more wholesome
So again, this isn't getting in the way of the thought staying but
When you note of course thoughts will cease because there's a new
Awareness but thoughts cease anyway
I
Thought isn't a thing that like a boat floating on the water that you can either drown it or let it stay
The thought is already passed by the time you make a decision
To stay or make it go or something like that
So you could cultivate another thought or you could be mindful and as I've said mindfulness is always the better choice
Because again the thoughts are not wholesome in and of themselves
I am a 23-year-old student and it's hard to let go of sensual desires and thoughts
Usually I end up having tasty food and watching media overnight
I feel I have no patience with my desires advice
Yeah, I just there's always this problem with
This this the statements about letting go
Because it doesn't sound like you're talking about letting go at all. It usually sounds like you're talking about getting rid of
Which is not at all the same as letting go
It's almost as like like a euphemism like like it sounds better to say letting go
But what you actually mean is you want to get rid of it?
letting go means not clinging and
In regards to central desires
It's even a little weirder because you're talking about not clinging to clinging
Which of course happens, but it's maybe know what you think it is
You don't let go of clinging you let go of the things that you're clinging to and then there's no clinging because letting go and clinging or their opposites
Right, but you can cling on top of the central diet for example
It sounds like you might be clinging to them because you're worried about them and that's a form of clinging
If you're if you're averse to them or you feel guilty about them or that sort of thing
But the actual central desires aren't something you let go of there's something that you destroy that you abandon
Because you let go of the things that you were clinging to instead of clinging to those things
You let go of them
So what you should be focusing on are the objects of your central desire
You can also focus on the central desire itself, but you should certainly
Be most defocused on on the things that give rise to central desire
And again, our focus is not on letting go
That should never be your focus
So if you talk about it being hard to let go
It seems like you're both missing the point and and you're missing the point of the practice and the point of letting of the concept of letting go
In the first place because letting go is not something you do it's something that happens
through seeing clearly and so our focus is on cultivating
Clear vision seeing seeing things clearly as they are
When you see things clearly as they are you'll see that they're not worth clinging to
And then it's not even so much a matter of letting go
It's a matter of
Never clinging in the first place
Because you saw clearly so the clinging is abandoned. It's destroyed. It's the opportunity for the arising of desire
is done away with
Generally
The idea of something being hard is fine and I don't criticize that for sure but
Part the really the accurate
Representation of what is hard is the
Cultivation of the skill the capacity to be mindful and
That is I don't even know if hard is the right word. It's something that just takes
Effort it takes practice it takes
Illigence
So there's really no secret and really the answer to the whole
Question of problem that you're proposing
Is really the practice of mindfulness so I don't know if you read our booklet on how to meditate if you've done our at home course
If you're interested in that that is what I would always
Propose that we have to offer and it really is for the purpose of seeing clearly which removes
Over through practice removes the arising of central desire
Like the idea of saying you have no patience talking about patience with desires because that is important
But it still may be missing the more important point of being patient with the object's your desire as well
Don't forget that those are generally going to be the more
common object those things that
Are the object of your desire including the pleasure which comes along with desire you have to note all of those
When I meditate I see vivid images and replays of places. I've been almost like a dream
I have strong and pleasant reactions
Why are these images coming up?
I practice the ways from your booklet
And why are my reactions so overwhelming?
When memories come up outside of formal meditation
I am not having overwhelming reactions to them in this way
So we're not
It's usually interested in the why of things that has no real value
There's no great merit in learning why something happens
I theoretically for your reassurance I guess
But it shouldn't be where your focus is
Why are the images coming up? I mean there is a reason I mean it's a silly thing to say but
It really doesn't go any further than that there are reasons why those images are coming up
I mean I don't know the exact working of your psyche, but it's not really hard to understand that's how trauma works
Trauma may be not the right word, but trauma is one of the things. There's trauma. There's addiction you know attachment
Anything that has a strong emotional response you can verify it for yourself
It tends to come back it tends to
Develop this power in your mind and then you experience echoes of it
But that fact isn't really useful in and of itself
We're much more interested in the
fact that they do arise and the fact that they
Give rise to strong reactions
so
Again, why that's happening more in meditation it doesn't necessarily have to be that way
But yes, it generally is that way because in meditation you're more focused your less
You're provided with less diversion
Your less distracted and so everything is going to be clearer and stronger
that's
That's helpful because it allows you to see them more clearly
but
When it leads to stronger negative reactions
it can be
Unpleasant but generally that's a very beginner sort of problem
as you become more
skilled in cultivating mindfulness you are better able to face things
That you couldn't face before without any any reactions
But that's the key is that
We're so
incapable of facing things that when we try to do that in meditation
We it freaks us out. It gives rise to lots of problems
And it's what really makes meditation hard is that we're incapable of facing without reacting anytime we're confronted with something
That would give rise to a reaction the reaction is almost immediate
And we're unable to see them objectively see the objects objectively so it basically is a description of
the
Work that you have to do
to learn how to face
Your experiences without reacting to them
Because again outside of practice. It's there's no facing. There's mostly just
Running away from appeasing
Allowing living as a slave unto our desires
I've become less social due to meditation. There is no longer desire
A meditative life seems incompatible with socializing
A life in solitude seems more appropriate. Do you recommend this?
Well, more importantly the Buddha recommended it so you don't have to ask me we have explicit
recommendation from the Buddha
Bantanja sayanasana. It's the teaching of all Buddhas to
find
secluded place to live
If you're a person who can appreciate the greatness of that then
kudos to you and they certainly recommend it
I make cancer imaging agents at a baro pharma company
My colleagues give animals cancer and then inject my product animals are then killed for further testing
I don't participate directly, but I am am I producing negative karma and is this considered wrong livelihood?
I don't know what cancer imaging agents are
If their poisonous or cancer causing then you'd have a problem, but it sounds like they are
agents which allow the visualization of cancer that's my guess
I assume that you're just talking about something that like a die or something
I don't know that allows you to see the cancer over some kind of machine
So clearly that isn't involved directly in any sort of killing and making those imaging agents
is not an wholesome karma so you kind of dodgeable it there
However, you are still working for a company that it sounds like is involved in mass water of
not just slaughter but torture of living beings which is a horrible horrible thing
and people engaged in that industry are horrible horrible
I don't want to say horrible horrible people that just sounds bad
but have incredible corruption in the mind I mean that isn't that isn't just some religious
teaching and giving you you can verify it you ever spend time with these people and
it's unpleasant it's it's kind of saddening to see how
they almost sometimes seem like lab animals and you know kind of where they're going when they die
because there's no escaping karma
So you're working for that company and I'm not saying that that makes you a bad person or anything
but it doesn't seem completely clean
So I guess what I would say is I wouldn't recommend working for such a company
but I think you can if you're in the position where you need the job to live and
without it you would die then to be patient with it but clearly to understand and appreciate
that the company you're working for doesn't sound like a wholesome operation to say the least
not even to say the least to avoid the whole white saying what's true is that it's horrible
what these companies do is just
ye terrible
and it's probably the cause for a lot of the illnesses that we now face because there's a lot of
karmic repercussions but that's harder to see I think if not impossible you'd have to be a
Buddha probably to see exactly how that works so
mostly I would just focus on what it does to your mind
there mine's not necessarily yours if you're not actually involved in the killing
Are there differences between going a reposna meditation and the Mahasi method
well you can learn for yourself they're not hard it's not hard to find instructions on how to
practice both I think it says somewhat of a I'm not sure I don't know who you are or where this
is coming from but nowadays it's not hard to find even briefly information on the two and so I'd
recommend you do that and you'll see the differences is that fair to say I mean maybe there's some
things I mean some of the differences I guess you'll just have to find when you go to
practice the two of them because not all the information is out there of course
I don't know I don't know too much about the going to tradition I know that it is different
so the answer is yes
I feel an impulse to check news every 15 minutes constantly is this an addiction if so how can I
overcome it? Sounds pretty handy pretty you know kind of like an addiction
but really it's not honestly going back to the other addiction question as well the other
central desire question they're not usually the first thing you have to focus on don't worry
about such things not so much not in the beginning I appreciate that they can sometimes
get in the way of your life but honestly mindfulness solves these problems it changes your
perspective on things you can't just decide not to for example check the news every 15 minutes you
have to change your perspective on reality you have to change your outlook change the direction
you're going in provide some direction sometimes because sometimes you're just so bored
and have nothing better to do so you just go ahead and check the news every 15 minutes
through the cultivation of mindfulness you give yourself something better to do
and and you feel better doing it and so that just takes away any desire to do all the other
impulsive things that you might engage in it it's not it's simple it it's not trivial it's hard work
and it can take years or even lifetimes to master but it's not it's not so mystery to it it's not
it's not a esoteric sort of path so if you haven't done our at home course that's where I'd
recommend read our booklet do the at home course see if that helps you if you've done all that
well maybe do an intensive course but again it's not the end of the world if you check the news
every 15 minutes it's not going to destroy your practice so it's kind of more down the road eventually
as your mind starts to change and you get rid of some of the course of defilements you start to
approach and deal with these more subtle ones
the key in the beginning is to get to the point where you no longer want to kill steel lie
cheat take drugs or alcohol if you can get to the point where those are no longer attractive in
this lightest then then you've made progress and then you can start to approach the more subtle ones
is there a danger for someone with OCD who does rituals to undo the unwholesome mind states that
they become aware of they're just adopt a different type of ritual by learning this practice
noting
right so no there is no danger because it's a different kind of ritual there's no in mindfulness
there's no there's no there's no there's no desire to undo there's no inclination there's no
drive to undo involved there's nothing in mindfulness that involves fixing and that's the key
that's what makes it different from other kind of rituals are the rituals they're looking for
a result mindfulness it's kind of not looking for a result it's not looking for a result in
relation to the object it's looking for a result in the change and how we perceive the object
but your focus isn't on the result your focus is just on the quality of mind the the approach
that you take you're really just zeroed in on the state of mind the quality of mind not concerned
with results not concerned with fixing anything
when I'm being anxious and trying to note it it gets amplified do I need more practice to see it
more clearly any advice I get amplified again this is the same as the other question so yes
we've had this question many times yes it can get amplified because you're not avoiding it
you're facing it which is in the beginning overwhelming but that just takes practice
but it's also amplified because of the clarity of mind you're just seeing it more clearly
a key part of this is going to be changing your perspective on even anxiety and instead of
seeing anxiety as a problem see it as an experience you have to get over the fact that
you get over the idea not the fact get over the idea that there's something wrong when you're anxious
because that makes you more anxious so what I'm saying is even if the anxiety gets amplified
you have to you have to get over the idea that that's a bad thing you have to change your
perspective that's the whole essence of the practice the whole perspective is on seeing things
clearly rather than trying to fix them so when pain is amplified when emotions are amplified
stop seeing that as a problem I mean what it's showing you technically is the three characteristics
unpredictable you don't know whether it's getting it better or worse it's suffering I mean it's
obviously anxiety is not pleasant and it's non-self you're not in control you thought by noting it
it would get it would get better why isn't it going away when I know it non-self it's not into your
control it doesn't belong to you you need to change your perspective on it I mean mindfulness
provides that perspective where you're not afraid when it gets stronger you're not taking off
guard or overwhelmed no matter how strong or how intense it might become because you don't see it
as a problem anymore you just see it as an experience that's why we use the noting not to get
rid of it you mentioned in an earlier discussion that incorrect labeling during meditation
could lead to delusions I am now fearful of noting him precisely in case I dilute myself these may
you advise I think you may that that's kind of like taking it out of out of not a context but
out of proportion blowing it out of proportion so delusion is illusion is all the time it's
what makes you afraid it was constant illusion anytime you're not mindful the the
really issues can yeah okay so real issues can come if you are noting something that it's not
but that's not a subtle thing that's like if you're seeing something and you note hearing
that would be very bad that would be a that would screw up your mind but there's no secret there
I mean that's that's not an obvious seeing is not hearing if you start noting if you start noting
things that you want to happen then it's gonna screw up your mind because you're gonna want
things like if you say calm calm not because you're calm because you want to become that's not
very good it creates desire and it creates expectation it creates a sense of self like you're
trying to control things because you're trying to create it but I mean in this case the the sort
of the the real issue here is is the fear and you have to focus on the fear rather than
obsessing over using the correct label because yes it's possible to get into trouble there
but that's not your problem your problem is the fear so that's just the sign that you have
this habit of getting afraid of things that are dangerous and it's like if someone gets afraid
because they see a bear or a person with a gun or something reasonable but yeah just focus on the
fear don't don't don't don't obsess over this idea that you could use the wrong labeling you have
to you know it's a kind of a misunderstanding what I was saying it's pretty obvious I mean yes it is
true but this goes for any meditation meditation is involving the mind and it's of course a very
powerful thing you can mess up your mind if you're not following directions and no question that
anytime you decide to undertake meditation you're engaging in something that has the power to
really help you but it also has the power if you you do practice it wrongly to this and you in
the wrong direction but one wrong direction for example is getting afraid of things that's a bad
direction that's going to cause you problems if you if you cultivate that fear and if you're not
mindful of it it's why mindfulness is so valuable because it's not dangerous but it has to be
mindfulness and it's not hard to understand that really it's not a subtle thing where it
whoops all this time I was doing something that was destroying me destroying my mind it's more that
you really just didn't get the concept of military of mindfulness which is quite simple seeing
is seeing hearing is hearing if you're labeling things as they are it's not a subtle thing
but like what was one of the examples that you might have been referring to that I was talking about
like when you watch the breath and you say buddho buddho I think that's a terrible idea
I don't think it go I mean again there's there's very famous teachers who use that practice so
power to them and some of them may get very strong states but from what I've seen they also get quite
as significant amount of delusion from what I've seen
but that's quite simple is the breath buddho no it's not the breath is the breath
and should never note things as they aren't
so it's not about imprecision it's not it's not like it's not the word imprecision isn't correct
it has nothing to do with noting imprecisely noting wrongly is much different from noting imprecisely
precise isn't important right is important you just don't note things as they aren't
that's all
when you're angry don't know liking or wanting or when you dislike something don't note liking
don't note to try to get rid of something note things as they are to remind yourself that's what
they are and it's not imprecision is not it's not important it can be imprecise and still be okay
that's not a problem but if it's wrong it's wrong and that's not a good thing and it can have
long-term repercussions I think but they're more long-term repercussions it's not like suddenly you
go crazy
I have meditated for around six years I have reached the point where I am ready to detach
further materially to the next level I live very minimally I wish to devote more time to
meditation as well advice
I'll devote more time to meditation and they don't have any advice to correct your perception
I mean unless you're conceited about all this because you very well might be that's a common
thing even for very good meditators to be conceited about their good qualities
so that's something to always watch out for though I don't know whether that's the case
I mean practically speaking you should consider to do an intensive course in meditation I don't
know if you have ever done an intensive course in meditation that's really the beginning
where you should always start and if you've done one already well then do another one
seven years the maximum is seven years to become an hour hunt if you practice stay in your room
and just meditate for seven years that's the maximum it could take
what are your thoughts on doing yoga and other breathing exercises and postures popular in yoga
I don't have any thoughts on those
can you suggest some ways to practice mindfulness in our day-to-day life as a commoner unlike
monks common folks like me have attachments and bonding in our day-to-day lives
I don't know if you read our booklet on how to meditate it does go over some of that
I would recommend if you haven't to do our at home course I mean I'm just repeating myself
sorry but it's you know we've set this up for that reason because this is the answer that we propose
so yeah that's what I would recommend don't know if maybe you've done that already I would
still recommend trying to find a way to do an intensive course if you have time take an actual
vacation but the at home course also gives you a lot of insight in how to be mindful
living in in my life I don't think there's anything special that needs to be said I mean there's
no there's no special a lot of the questions we get are asking for special advice like do
what do you have any any tips or something and there really isn't anything secret it's pretty
simple which can be frustrating because you want to be guided but that kind of defeats the purpose
it has to come from within you have to learn it for yourself you can't talk someone to enlightenment
there's only one way to be mindful that's to be mindful
but I said it's better to live one day to see rise in fancy things meaning
the Buddha used this technique to practice meditation and to become enlightened
the Buddha the way the Buddha became enlightened is quite specific
he practiced mindfulness of the breath so similar to what we do
but that he only practiced in a way that led to tranquility
and in the third watch of the night he contemplated to teach a samapada which is similar to what
we do and could be the same but he didn't need the technique that we use I don't think I think
it's probably fair to say he didn't need any technique he just applied his mind and his mind
was so pure that he saw immediately I mean you couldn't say it's the same technique because he
would have acknowledged things as they are but it wouldn't have taken even an hour to do
it was more a matter of as soon as he applied his mind he knew kind of thing I mean it sounds
like I wasn't there of course and even if I was I still wouldn't know but it sounds like it was
pretty impressive and trivial in a sense I mean trivial because he had done countless eons of work
already so it was quite direct and simple not to not to trivialize it but he didn't have to do
walking and sitting as we do and I don't know how that relates to why you would think that
that somehow relates to the saying because that saying has nothing to do with actual technique
but of course that's an important part of our technique is that through practicing
four foundations of mindfulness you will begin to see the arising that's easy
oh there's one deal with manipulative people who tend to pull you in arguments constantly
well people can't really pull you in you just have to get better at not being pulled in
smiling and walking away it can work though it can make them very upset which is not really your
problem you don't have to prime prevent people from getting upset sometimes they have to realize
that that's not the proper response and they have to realize for themselves
not your job to deal with people you just have to deal with yourself
you don't have to be you aren't responsible for their reactions you're responsible for your actions
and your reactions so try and watch your own feet you don't have to deal with people just deal with yourself
is the purpose of noting to realize that I am the awareness that is noting
so it creates a distance between the real eye and the experience no no it is not that is not the
purpose I don't know what this real eye you're talking about is I've never found such a thing
the theory is that it is not an accurate represent accurate description of reality
so there's also no interest in creating distance between one thing or another thing
even referring to things that do exist
the purpose of noting is reminding you of the object so that you don't make more of it than it is
or less of it than it is that you just make it what it is that's all no idea of self or this
self or that self or I or the real eye none of that plays a part
is nibbana lasting happiness because of understanding
family and friends are worried and asked why I meditate I answer lasting happiness or piece of
mind I wondered if this is the ultimate goal it is but it's a bit misleading for people because they
think you're just you're just sitting down and feeling very calm I would prefer telling people
it's for seeing clearly I really think that's more relatable and more reasonable more noble
because if you omit that part then then it it's not clear whether you're talking about
practicing summit or practicing robustness because summit is also for okay it's maybe not technically
lasting happiness but it kind of is and it's okay because you said lasting happiness that you're
kind of implying that it's something that comes from wisdom but that's not clear to anyone who
hears it peace of mind is also I mean there you have you've dropped the lasting part so peace of
mind is something that somebody does give you just not so that it's not lasting it's that it's
not permanent it's not stable that's the difference but the point is that anyone who hears that is
just going to think you're sitting down to feel calm which is trivializing the important part
the work that you're doing to see clearly so I would never really focus on that especially if
you're talking to someone else again our focus isn't on those goals our focus is on reminding
ourselves of what things are so that we can see them clearly and not be diluted
I think that impresses people a lot more because they appreciate that it's not just lazy or self
centered or so on it's about actually making yourself a better person the person who is wiser and
more clearer as a more clear perspective of things greater mental clarity
which is beneficial not just for yourself but others as well
and noting the fear of death when it arises some days lead up to me being free from it
yes that's the whole point
but you kind of have to remember to take a broader perspective that you can't be a one-issue
meditator like what is this where there's a one one issue voter or something if you're voting
in an election and you only have one issue you can't be a one-issue meditator
so don't focus on one one thing to try to get rid of it because fear of death might be a
long-term thing that is harder to get rid of probably not fear of death is probably one of the
things you can do away with pretty quickly relatively quickly but but two things first of all
you have to deal with right with right in front of you and if that's fear of death great if it's
something else don't get distracted by an obsession with one issue focus on what you're experiencing
another thing is that it takes work so occasional practice isn't going to be all that effective
if you can practice every day that's great if you can do intensive practice that's the best
how do I know when my path for example regarding a job is right for me when there is a lot of
resistance coming from externals how do I know if this is a phase or I should stop and change
direction I mean kind of you know from practicing mindfulness but one thing that you'll start to
learn from mindfulness is that things like jobs are pretty inconsequential which job you do
is only consequential in terms of the ethical qualities of it so it's consequential if you
it's involved killing for example then that's consequential it could also be consequential if it's
a wholesome job so then you could say oh this is a better job because it provides me the
opportunity to to cultivate wholesomeness I mean one example is not having to work a lot right maybe
you don't get paid as much but you don't have to think so much you don't have to work so much
some job that gives you the opportunity to cultivate mindfulness when you're on the job and gives
you the time off to be able to cultivate mindfulness off work but path doesn't involve the things
you do I mean especially not the things that are worldly conceptual things the path only
involves the quality of your mind as you do things as you engage in the world so not only will
you learn okay killing living beings for living isn't good but you'll also learn that apart from
that it doesn't matter whether I'm a carpenter or a plumber or an electrician all that matters is
that my capacity to be mindful to see clearly
can a Buddha endure a difficult meditation over some time by observing how it is progressing
through five stages of grief
Buddha doesn't have any difficult meditation there's nothing difficult for a Buddha
there's nothing difficult for an hour a hand even there's no grief for a Buddha I think may
have a different idea of this word that you're using the Buddha because a person who is light and
has no grief and also has nothing difficult they find nothing difficult I guess depending how
you define that but pretty much nothing difficult basically yeah I don't know where they would
ever call something difficult they might acknowledge that something was generally perceived as
being difficult by others but a Buddha doesn't have any difficulty with that thing so there's no
sense of them having to endure it they just they just live through it without any difficulty
but you have no no stages of grief for a Buddha
what is the antidote to conceit our friendship even an anagami still has conceit that's kind of a
kaba what is the direct antidote some in clarity seeing clearly conceit is a kind of a delusion and the
direct antidote to delusion is wisdom clarity
until we've run through the questions
okay don't we made it we had an hour so there weren't many questions in the beginning but
amazing how we have always a good set of questions so thank you all for your sense here interest in
these things I appreciate that and I hope that the answers some answers were helpful
and I wish for you all to find success in your practice and on your spiritual journeys
find peace happiness and freedom from suffering and that you have a good week
and maybe we'll see you back next week thank you Chris and Rahid for helping
is there a gym as well okay and everybody else in the chat side who
