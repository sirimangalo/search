1
00:00:00,000 --> 00:00:08,880
Hey, good evening, everyone.

2
00:00:08,880 --> 00:00:15,440
Welcome to our evening dumber.

3
00:00:15,440 --> 00:00:21,720
Tonight we are looking at, well, questions.

4
00:00:21,720 --> 00:00:32,160
We have eight questions on our website, we'll go through those.

5
00:00:32,160 --> 00:00:39,800
And then maybe I'll take up one of them in a little more detail.

6
00:00:39,800 --> 00:00:48,680
But first we'll talk, we'll sort of generalize, okay?

7
00:00:48,680 --> 00:01:03,800
Going to talk about the Buddha.

8
00:01:03,800 --> 00:01:20,560
So the story goes that countless encounters and countless encounters of time in the past

9
00:01:20,560 --> 00:01:33,720
someone decided that even though they were capable of becoming enlightened very quickly.

10
00:01:33,720 --> 00:01:43,720
There was someone who was quite powerful and well-developed spiritually, even though they

11
00:01:43,720 --> 00:01:53,200
could follow and realize the Buddha's teaching, that the teacher realized the truth very

12
00:01:53,200 --> 00:02:04,640
quickly they decided that they would put it off and they would rather than learn the teaching

13
00:02:04,640 --> 00:02:20,000
from someone else, they would work themselves to be able to discover the teaching for themselves.

14
00:02:20,000 --> 00:02:30,960
We see that happening today, people deciding on this, thinking that while these teachings

15
00:02:30,960 --> 00:02:36,760
lead to freedom from suffering, which is really an end to some sorrow and end to wandering

16
00:02:36,760 --> 00:02:46,160
on, and if I put an end to my wandering on, I'm leaving behind everyone else, abandoning

17
00:02:46,160 --> 00:02:54,480
all those beings who need my help, and so these people, because they're spurred on by

18
00:02:54,480 --> 00:03:04,040
not just helping in this life, but working to help as many countless beings as they can.

19
00:03:04,040 --> 00:03:13,320
There's no sense that you should or shouldn't do one way or the other, you shouldn't

20
00:03:13,320 --> 00:03:21,480
look down upon people who decide to do such a difficult thing, but likewise, there's no reason

21
00:03:21,480 --> 00:03:30,120
to look down on someone who actually tries and strives to practice the path to freedom

22
00:03:30,120 --> 00:03:33,760
from suffering, because that's what it's all about, right?

23
00:03:33,760 --> 00:03:39,080
The more people who decide not to be free themselves from suffering, the more suffering

24
00:03:39,080 --> 00:03:56,120
there will be, so we will reduce suffering greatly, really, by becoming free from suffering.

25
00:03:56,120 --> 00:04:03,840
But the goal and the end, ultimately, is an end to suffering, which really just means

26
00:04:03,840 --> 00:04:20,720
an end to stress, an end to clinging, an end to ambition, seeking out things that have

27
00:04:20,720 --> 00:04:33,040
no value, seeing satisfaction and things that don't actually satisfy us, ignoring the fact

28
00:04:33,040 --> 00:04:49,120
that we become more and more impatient, more and more clingy and addictive, addicted.

29
00:04:49,120 --> 00:04:58,520
Anyway, so we have some questions tonight about enlightened beings and about the Buddha.

30
00:04:58,520 --> 00:05:10,720
This question is, if Nibana releases one from Samsara, there's Pari Nibana anything

31
00:05:10,720 --> 00:05:19,520
other than obliteration, it's Pari Nibana different from an atheist's conception of death.

32
00:05:19,520 --> 00:05:31,280
And my conception of death is that it's an illusion, a conception, and it's not real,

33
00:05:31,280 --> 00:05:34,320
it's just a concept.

34
00:05:34,320 --> 00:05:43,200
What's real is experience, which continues on and gives rise to future experience through

35
00:05:43,200 --> 00:05:59,680
the power of ethically charged mind states, so another description of ethically charged

36
00:05:59,680 --> 00:06:08,640
mind states is those mind states that are productive of further mind states.

37
00:06:08,640 --> 00:06:16,760
So mindfulness, equanimity, objectivity, enlightenment, these are things which are not productive

38
00:06:16,760 --> 00:06:21,840
of future mind state.

39
00:06:21,840 --> 00:06:35,520
And so when past mind states come to an end, there's no future rising.

40
00:06:35,520 --> 00:06:40,400
So Nibana doesn't release one from Samsara, there's no one to release this idea of

41
00:06:40,400 --> 00:06:48,960
a being, so the second part of your first question is based on the error of the presumption

42
00:06:48,960 --> 00:06:55,080
in the first part of the question, which is that there is no one to be released and therefore

43
00:06:55,080 --> 00:07:00,960
it can't be an obliteration of anything, it's an obliteration of suffering, that's

44
00:07:00,960 --> 00:07:08,880
it, an obliteration of defilement, an obliteration of ignorance, it's all those things.

45
00:07:08,880 --> 00:07:16,440
We should read the beginning of the vinnaya, the beginning of the vinnaya, someone accuses

46
00:07:16,440 --> 00:07:20,800
the Buddha being an obliterationist and so on.

47
00:07:20,800 --> 00:07:30,040
Yes, yes, I obliterate suffering, obliterate illusion, ignorance.

48
00:07:30,040 --> 00:07:33,960
It's very Nibana different from an atheist conception of death, so you're referring to

49
00:07:33,960 --> 00:07:40,920
a person who believes that everyone, when they die, at the moment of the physical, break

50
00:07:40,920 --> 00:07:47,480
up of the physical body and the mind also ceases.

51
00:07:47,480 --> 00:07:54,160
So a physical is the person who believes that the mind is a product of an active brain

52
00:07:54,160 --> 00:08:04,160
and solely an aspect of physical existence, which when physical existence ceases, mental

53
00:08:04,160 --> 00:08:07,120
existence ceases as well.

54
00:08:07,120 --> 00:08:19,640
So the cessation of mental activity, I think I can buy that, just but it's the cessation

55
00:08:19,640 --> 00:08:42,200
and not due to physical death, it's the cessation due to the end of the end of continuing,

56
00:08:42,200 --> 00:08:51,040
there's another word for it, can't think of it, but it's going to say procreation, but

57
00:08:51,040 --> 00:08:54,440
that's not quite correct.

58
00:08:54,440 --> 00:09:01,960
So I'm going to say this one for the end, I'll do a little bit, go into detail, but

59
00:09:01,960 --> 00:09:08,520
let's move on to the third one, is an R behind able to laugh, actually I don't really

60
00:09:08,520 --> 00:09:17,880
understand that, no, I left it actually because laughing is physical, and an R behind might

61
00:09:17,880 --> 00:09:30,400
still engage in activities relevant to their station, so an R behind may very well laugh

62
00:09:30,400 --> 00:09:38,640
in my opinion, some, I have heard people say no, no, no, an R behind would never laugh,

63
00:09:38,640 --> 00:09:43,280
but it's being just a physical act, I believe an R behind would be capable, I don't think

64
00:09:43,280 --> 00:09:54,640
a Buddha would ever laugh, because Buddha's uproot, Upanisha means they have no idiosyncrasies,

65
00:09:54,640 --> 00:10:02,000
there's nothing particular, peculiar about them, what is all of perfect disposition,

66
00:10:02,000 --> 00:10:06,400
which means I don't think they laugh, I don't know that there's any text that says they

67
00:10:06,400 --> 00:10:17,400
don't, but it seems to me that they most likely do not, they smile, is there something

68
00:10:17,400 --> 00:10:23,720
called itching, or is it just weighed in that, no itching would be probably an earth element,

69
00:10:23,720 --> 00:10:31,480
the hardness, it's a hardness that is very strong and localized, it's not weighed in,

70
00:10:31,480 --> 00:10:35,440
no, it's not weighed in, it's associated, every experience has weighed in that in it,

71
00:10:35,440 --> 00:10:41,880
but weighed in as the feeling of pleasure, pain, or neutral, so an itching is most likely

72
00:10:41,880 --> 00:10:51,400
associated with the neutral feeling, of course the reaction is usually associated with

73
00:10:51,400 --> 00:11:01,880
the disliking of it, and negative feeling, and painful feeling, okay, this is a long

74
00:11:01,880 --> 00:11:11,160
question, I'm gonna try to distill it, so yes, this liking yourself, but then you say well

75
00:11:11,160 --> 00:11:17,360
myself doesn't exist, so why should I note that, so we don't note, we don't have, our

76
00:11:17,360 --> 00:11:24,160
noting has nothing to do with what it is that we're liking or disliking or reacting to, those

77
00:11:24,160 --> 00:11:30,600
things are just concepts generally, the important point is that there's a reality there,

78
00:11:30,600 --> 00:11:37,160
and the reality is you're disliking, so when you're disliking, the reality is to say to

79
00:11:37,160 --> 00:11:48,680
yourself, for the practices to focus on the reality and say to yourself, disliking, disliking,

80
00:11:48,680 --> 00:11:54,920
there's no need to remove concepts, you want to remove the concept of self, the removal

81
00:11:54,920 --> 00:12:01,400
comes from seeing what really is there, which is experience, when you see that there is

82
00:12:01,400 --> 00:12:15,640
experience, self has no place, no part in reality, sometimes the Buddha uses images like

83
00:12:15,640 --> 00:12:23,360
rotting corpses and so on, disease, cancer and so on, so you try to use these, I wouldn't

84
00:12:23,360 --> 00:12:33,000
use them, these are similes, similes are used to provide direction, but the direction

85
00:12:33,000 --> 00:12:39,880
should be towards practicing mindfulness and towards aligning yourself with the fact that

86
00:12:39,880 --> 00:12:45,160
the body is not something beautiful, this beauty in the body is really silly and there's

87
00:12:45,160 --> 00:12:53,320
good similes that allow you to see how ridiculous it is, we are biased against all

88
00:12:53,320 --> 00:12:58,760
of the things that the body is made up of, we were repulsed by all of them, and yet somehow

89
00:12:58,760 --> 00:13:03,160
we become attached when they're all put together in a certain way, it's just delusion

90
00:13:03,160 --> 00:13:08,040
and illusion, so when you look at the body as repulsive, it helps you see, oh yes, look

91
00:13:08,040 --> 00:13:13,840
it on the one hand, I'm repulsed by blood and snot and all sorts of these things, but on

92
00:13:13,840 --> 00:13:21,640
the other hand, somehow I'm attracted to it, you know, and most people are not attracted

93
00:13:21,640 --> 00:13:30,200
to a dog's body or a pig's body or something, it's all just conceptions, so seeing

94
00:13:30,200 --> 00:13:38,720
that, it sets you on the direction to be mindful and objective and learn to let go of

95
00:13:38,720 --> 00:13:46,880
your attachment, the way it doesn't often use these, I mean he does use them much more

96
00:13:46,880 --> 00:14:00,120
often, he talks about things like mindfulness, so this person does meditation with something

97
00:14:00,120 --> 00:14:06,360
in their ears, earplugs I guess, and then they heard something else and they were grateful

98
00:14:06,360 --> 00:14:13,720
for the sound, you know, all of this is reaction, you're reacting to the fear of some

99
00:14:13,720 --> 00:14:20,320
scary sound or you don't want to allow the arising of certain states that come from sounds

100
00:14:20,320 --> 00:14:25,880
and then when the other sound comes, you react to it and like it, maybe even being grateful

101
00:14:25,880 --> 00:14:32,600
means you relate it to a being who gave it to you or a force or something that gave it

102
00:14:32,600 --> 00:14:39,000
to you, which is all extrapolation or the reality is there's feelings, there's reactions,

103
00:14:39,000 --> 00:14:43,440
there's judgments, there's experiences and all of these things should be the object of

104
00:14:43,440 --> 00:14:48,360
the meditation practice, I mean this is a good example of the sorts of things that we want

105
00:14:48,360 --> 00:14:55,400
to try to work out and overcome and let go of experiences just experience, the sound is

106
00:14:55,400 --> 00:14:59,320
just sound and learn to say to yourself hearing hearing when you like it or dislike it

107
00:14:59,320 --> 00:15:10,400
or you're afraid of it or whatever, that all should be noted.

108
00:15:10,400 --> 00:15:16,760
This one I almost got rid of, I think it's okay, normally I don't like these comparative

109
00:15:16,760 --> 00:15:20,800
ones but it's fairly general and it's related to meditation, what are the difference

110
00:15:20,800 --> 00:15:25,840
between meditative practices and Theravada Buddhism and Mahayana Buddhism?

111
00:15:25,840 --> 00:15:30,160
The only big glaring difference is that generally the definition of Mahayana Buddhism

112
00:15:30,160 --> 00:15:38,240
is to put aside enlightenment, to be reborn and help others lifetime after lifetime, so

113
00:15:38,240 --> 00:15:43,040
the real difference, I mean there's so many different kinds of meditation in both traditions

114
00:15:43,040 --> 00:15:50,440
but the real difference essentially is that one is for the purpose of and leads to the

115
00:15:50,440 --> 00:15:55,360
consummation of becoming free from suffering and letting go of one's attachments in this

116
00:15:55,360 --> 00:16:02,640
life and the other is designed to maintain some level of attachment enough to be reborn

117
00:16:02,640 --> 00:16:08,000
again and want to help others in future lives.

118
00:16:08,000 --> 00:16:13,200
So our last question is someone wants me to do the medini wants me to do a video on the

119
00:16:13,200 --> 00:16:21,160
10 Perfections, so I thought I don't know that I want to do a video on the 10 Perfections,

120
00:16:21,160 --> 00:16:28,000
say one of those things that requires a scholar in my opinion, which I'm not, you know

121
00:16:28,000 --> 00:16:35,960
there were 10 or 12 books written, the Buddha, Charya, Bhattara, someone that's called

122
00:16:35,960 --> 00:16:41,640
anyway this Burmese set of books that was translated into English and it just goes, it's

123
00:16:41,640 --> 00:16:48,520
a huge, it's a bookshelf, books on the 10 Perfections, I don't know if those are still

124
00:16:48,520 --> 00:16:54,720
available, they were publishing them in Malaysia a while back, but very interesting and

125
00:16:54,720 --> 00:17:03,960
detailed set of texts about the 10 Perfections, which kind of makes me, knowing that

126
00:17:03,960 --> 00:17:09,280
that text is out there makes me think, oh no, I'm not something I want to dive into,

127
00:17:09,280 --> 00:17:20,440
but I don't go over them and refer to the introduction to the Jataka because it lists

128
00:17:20,440 --> 00:17:32,400
the 10 Perfections, so may that is this ascetic who has received a prognostication from

129
00:17:32,400 --> 00:17:43,280
the Buddha Deepankara, that this ascetic here will become a Buddha in the future, and

130
00:17:43,280 --> 00:17:48,360
so Sumeda goes and sits down and he says wow, what am I going to, this is it, I'm on

131
00:17:48,360 --> 00:17:58,960
the path, I've been verified by the Buddha, and so he thought down and he said to himself

132
00:17:58,960 --> 00:18:05,960
a search that I may find conditions which a Buddha make, above, below, to all 10 points

133
00:18:05,960 --> 00:18:13,560
wherever conditions hold their sway, and then I searched and saw the first perfection,

134
00:18:13,560 --> 00:18:23,680
Sila, which is, no, Donna, sorry, Donna, which is giving, Donna literally means giving.

135
00:18:23,680 --> 00:18:29,280
Giving is one of the Perfections of the Buddha, they are perfect and they're giving,

136
00:18:29,280 --> 00:18:37,520
they have perfect, not just like ordinary and light, even in light and being will give,

137
00:18:37,520 --> 00:18:45,040
but a Buddha is always giving and is perfect and giving, what is most useful to a re-individual.

138
00:18:45,040 --> 00:18:50,760
So I have to say, I have to develop this, and it's similarly, there's 10 similes that

139
00:18:50,760 --> 00:18:58,040
I want to go over, he says, as when a jar is brimming full and someone overturneth it,

140
00:18:58,040 --> 00:19:04,480
the water, the jar, its water all gives forth and nothing for itself, keeps back.

141
00:19:04,480 --> 00:19:11,480
So if you want to have perfection of giving, Mahasi Sayyada talks about this, and he goes

142
00:19:11,480 --> 00:19:18,240
into more detail than I'm going to, but I definitely recommend reading what he says on giving,

143
00:19:18,240 --> 00:19:24,520
and how you have to go for arms first as a monk, you have to go for arms first and give away

144
00:19:24,520 --> 00:19:29,280
all the arms, and then go for a second time, in a third time until you've given away to

145
00:19:29,280 --> 00:19:34,360
everyone who could possibly need food, usually monks who don't go on arms around the older

146
00:19:34,360 --> 00:19:45,160
monks or so on, teachers who don't have a chance to go out, and then you can go and

147
00:19:45,160 --> 00:19:52,640
have whatever's left, you can go for arms for yourself.

148
00:19:52,640 --> 00:19:59,440
So here the simile is giving everything. Are you able to give everything? And everything

149
00:19:59,440 --> 00:20:05,920
really means everything, including your body, including your limbs and your organs and

150
00:20:05,920 --> 00:20:15,720
your life, even? Are you able to give all of that? That's the way of a Buddha? So he said

151
00:20:15,720 --> 00:20:24,760
to himself, I'll give to anyone. May they be low or high, or middle born, doesn't matter.

152
00:20:24,760 --> 00:20:37,280
First one. The second one is Sila, morality. And his simile is, as when a yaks cows flowing

153
00:20:37,280 --> 00:20:44,040
tail is firmly caught by bush or thorn, see there upon her weights or her death, but will

154
00:20:44,040 --> 00:20:52,040
not tear and mar her tail. So a yak has a long tail, I guess. And if it gets caught on a bush

155
00:20:52,040 --> 00:20:58,440
or something, the yak will not pull because they are afraid to break the tail, to pull

156
00:20:58,440 --> 00:21:06,680
the hairs out of the beautiful tail, I guess. I mean, suppose there were such a yak. That's

157
00:21:06,680 --> 00:21:13,680
the way of someone who keeps to the perfection of preset or of ethics. Is that even for

158
00:21:13,680 --> 00:21:21,520
their own life? If they won't break the slightest preset, no matter what, no matter what

159
00:21:21,520 --> 00:21:29,560
the cost, they will not hit. They will not hurt. They will not harm another. They will

160
00:21:29,560 --> 00:21:36,600
not speak a lie. They will not speak harm to others. They will train themselves that they

161
00:21:36,600 --> 00:21:45,600
will see Anumate, Suwadji, Subhyadasa, where they will see danger in the slightest fault.

162
00:21:45,600 --> 00:21:55,640
They would have developed what he said to develop this, the perfection, or nothing, whatever

163
00:21:55,640 --> 00:22:10,800
may can break an ethical preset, or an ethical principle. The third is renunciation. And

164
00:22:10,800 --> 00:22:18,600
it's like a man who long has dwelt in prison, suffering miserably. No liking for the

165
00:22:18,600 --> 00:22:26,720
place conceives, but only longs for release. A person in prison, renunciation here means letting

166
00:22:26,720 --> 00:22:36,160
go of the world. So he says, I'll try and see the world as a prison. For the prison that

167
00:22:36,160 --> 00:22:44,720
it is, prison of my own make of our own making. Society is really in many ways a prison.

168
00:22:44,720 --> 00:22:51,120
We're imprisoning ourselves and imprisoning each other and manipulating each other. Politics,

169
00:22:51,120 --> 00:23:02,720
law, regulation. Some of it's good to get. For everyone to get something that they like,

170
00:23:02,720 --> 00:23:08,840
you need things like regulations, right? There's nothing wrong with regulations. The Bodhisatta,

171
00:23:08,840 --> 00:23:13,040
we have in one of the Sutras, the Bodhisatta was the first regulator, because people

172
00:23:13,040 --> 00:23:18,600
are stealing each other's rice, way back in the very beginning, back in the olden times

173
00:23:18,600 --> 00:23:23,680
when it was just rice, and everyone had some rice. But then someone said, I'm too lazy,

174
00:23:23,680 --> 00:23:28,960
I'll take someone else's rice. So they got the Bodhisatta to come and they said, hey,

175
00:23:28,960 --> 00:23:35,640
you know, you have to help us keep order here. So they had laws and regulations and so on.

176
00:23:35,640 --> 00:23:47,800
But only necessary because of desire, because we stopped clinging even to life. We just stopped

177
00:23:47,800 --> 00:23:57,720
clinging to this rock in the corner of the universe, clinging to this slug of a body that's

178
00:23:57,720 --> 00:24:06,760
creeping along this desolate rock in the corner of the universe. Yeah, we put it like that, maybe.

179
00:24:08,360 --> 00:24:11,720
Even life is not something terribly special. We're just quite fond of it.

180
00:24:19,240 --> 00:24:26,360
Okay, number four. So the point here is leaving it behind and leaving one's life,

181
00:24:26,360 --> 00:24:32,280
one's home, one's wealth and so on, giving it up and letting go and going off into the forest

182
00:24:32,280 --> 00:24:42,280
to cultivate what is truly useful, the mind. And the fourth is wisdom.

183
00:24:42,280 --> 00:24:57,560
And so he gives a simile, just like a brahmana priest went on his round. No, that's not right.

184
00:24:57,560 --> 00:25:01,880
Just as a monk or an ascetic on their arms round,

185
00:25:01,880 --> 00:25:11,800
or low, nor high, nor middling folk to the sun, but bags of everyone. So in India, of course,

186
00:25:13,000 --> 00:25:20,120
and there was a tradition, a long, ancient tradition of religious people going on to the village

187
00:25:20,120 --> 00:25:25,560
just to stay alive and some of them would beg, would ask people to give them food,

188
00:25:25,560 --> 00:25:32,440
Buddhist monks don't beg, but they do take if people are giving and they stay alive just by that,

189
00:25:32,440 --> 00:25:37,320
just by people actually initiating and saying, hey, you're looking for food,

190
00:25:37,320 --> 00:25:41,720
I have food for you. I'll give you some food, that kind of thing.

191
00:25:44,120 --> 00:25:51,000
But the simile here is the point is that just like such a monk, who doesn't discriminate,

192
00:25:51,000 --> 00:25:57,080
doesn't say, oh, I'll only go to this person or that person. He said, in that same way,

193
00:25:57,080 --> 00:26:02,840
I'll seek out wisdom from everyone. You see, a Buddha has to learn everything,

194
00:26:03,640 --> 00:26:09,160
so they won't shun any kind of knowledge, and they'll put together all the things they learn and

195
00:26:09,160 --> 00:26:15,080
try to discard the stuff that turns out to not fit with reality. You know, a true philosopher's

196
00:26:15,080 --> 00:26:22,440
quest, just to figure out everything. You see philosophers and throughout the ages trying to do

197
00:26:22,440 --> 00:26:28,600
this in vain because they're not Bodhisattas, but some of them might be, some of them might be on

198
00:26:28,600 --> 00:26:34,760
the path and so they're working it out lifetime after lifetime, refining it.

199
00:26:34,760 --> 00:26:45,400
The fifth is, let's see, I've got a very poor translation, and I'm not actually looking at the

200
00:26:45,400 --> 00:27:10,120
poly, which I probably should have done.

201
00:27:10,120 --> 00:27:19,000
Just as the lion, king of beasts, encroaching, walking, standing still with courage ever is

202
00:27:19,000 --> 00:27:27,160
instinct and watchful always end alert to the effort, walking and sitting and meditating,

203
00:27:28,360 --> 00:27:37,880
just like a lion that never lets up its guard. Honestly, I think lions have an overly good

204
00:27:37,880 --> 00:27:44,600
reputation, lions are apparently very lazy, and not like tigers, tigers are the more the

205
00:27:45,400 --> 00:27:52,120
kind of king, but lions are pretty laid back. I don't know what it was like in the time of the

206
00:27:52,120 --> 00:28:02,200
Buddha, but a lion is supposed to be the king, but I read a book once that said, no, the tigers

207
00:28:02,200 --> 00:28:13,000
are more like kings. Why are we? Oh, that's something else. Are we still broadcasting?

208
00:28:17,000 --> 00:28:24,120
I think we are live. Good.

209
00:28:24,120 --> 00:28:37,560
Media, so the effort of Bodhisattva is unflagging, and this means lifetime after lifetime,

210
00:28:37,560 --> 00:28:45,080
never giving up, never saying, oh, it's too hard. Lifetime after lifetime, working endlessly, ceaseless,

211
00:28:45,080 --> 00:28:52,440
just meticulously, cultivating, you know, you think the effort we put out here in the meditation

212
00:28:52,440 --> 00:28:59,160
center is a lot boy, thinking of the Buddha, what he had to go through to get to where he came,

213
00:28:59,160 --> 00:29:00,760
where he ended up.

214
00:29:05,560 --> 00:29:07,960
Let's fix this concrete patience.

215
00:29:12,680 --> 00:29:17,400
Just as the earth, whatever is thrown upon her, whether sweet or foul, all things in

216
00:29:17,400 --> 00:29:25,400
jurors and never shows repugnance nor complacency, it means no liking or disliking the earth,

217
00:29:26,440 --> 00:29:32,360
people can spit on it or defecate on it, throw rubbish on it, dig it up,

218
00:29:34,280 --> 00:29:40,520
hurl insults at the earth, if you want, the earth doesn't care, the earth doesn't mind.

219
00:29:40,520 --> 00:29:48,040
The Buddha gave many similes about this, like the simile of the earth, just like the earth,

220
00:29:51,240 --> 00:29:55,240
be like the earth, no matter what comes.

221
00:29:58,840 --> 00:30:03,960
See, we react out of total delusion, this idea that we have a self that we have to protect,

222
00:30:03,960 --> 00:30:15,960
it's all just clinging, and it's built up based on likes and dislikes and concern over loss,

223
00:30:15,960 --> 00:30:24,840
losing the things that you like, that builds up into a sense of self that you have to protect.

224
00:30:24,840 --> 00:30:35,560
The seventh is truth, but truth here means truthfulness.

225
00:30:35,560 --> 00:30:38,520
No, it means, let me see here.

226
00:30:38,520 --> 00:30:42,280
So we've got two of them, we've got satcha and aditana, satcha means,

227
00:30:45,080 --> 00:30:50,120
I work out the difference, aditana means, the ability to say,

228
00:30:50,120 --> 00:31:01,000
I vow to do x and then have it come true.

229
00:31:04,840 --> 00:31:10,920
I vow to sit here until I become enlightened, I will not get up until I become enlightened and

230
00:31:10,920 --> 00:31:23,400
then to see it through to have it happen. Satcha is to be able to say,

231
00:31:28,680 --> 00:31:32,600
well, I can't remember, but satcha is

232
00:31:32,600 --> 00:31:39,480
now, satcha is truth, but

233
00:31:46,440 --> 00:31:54,360
like there's this time where the Buddha says, if I am to be, if I am to be a Buddha,

234
00:31:55,960 --> 00:32:01,880
that's determination. If I am to become a Buddha, may this bowl, and he has a bowl, and he may

235
00:32:01,880 --> 00:32:07,720
it float upstream and it is bowl actually floats upstream, and I'm pretty sure that's aditana, satcha.

236
00:32:10,600 --> 00:32:20,760
Satcha is when you say, satcha is the ability to say,

237
00:32:20,760 --> 00:32:36,120
satcha is a power, it becomes a perfection because you've always told the truth,

238
00:32:37,320 --> 00:32:42,040
like you, if you, if you, if you stole something, you would say, I stole, but of course,

239
00:32:42,040 --> 00:32:47,560
Buddha, but I did, I think, steal and some of his, well, I don't remember, and it doesn't matter,

240
00:32:47,560 --> 00:32:53,800
it's the ability to say, I've never in this lifetime killed, and there's power behind that,

241
00:32:54,760 --> 00:33:02,280
and there's a power in truthfulness.

242
00:33:10,280 --> 00:33:14,520
It's been a while since I've gone over this. I know it's similar because there's a power that

243
00:33:14,520 --> 00:33:20,200
comes from, as you can say, to the power of this truth. I've never killed anyone in this lifetime,

244
00:33:20,200 --> 00:33:26,920
to the power of this statement of truth, and then may such and such happen, which is similar to

245
00:33:26,920 --> 00:33:35,240
saying, I vow for the such and such to happen may, may such and such occur, and have it happen.

246
00:33:36,680 --> 00:33:40,440
But this book that I was talking about, this series of books, it goes over how they're

247
00:33:40,440 --> 00:33:44,840
related and, and how they're distinct, because the commentary I think has a lot to say.

248
00:33:45,480 --> 00:33:50,280
Anyways, such a means telling the truth, and, and relying on the power of the truth.

249
00:33:54,760 --> 00:33:58,680
But I made a mess with that one, because I don't really remember about what that one refers to.

250
00:33:59,800 --> 00:34:05,560
But the simile is, just as the morning star on high, it's balanced course that I've ever

251
00:34:05,560 --> 00:34:11,640
kept, and through all seasons, times and years, that never from its pathways were. So the

252
00:34:11,640 --> 00:34:18,840
morning star is always on track. You never see that the morning star arise in the,

253
00:34:20,360 --> 00:34:22,360
in the wrong place, wherever that would be.

254
00:34:26,360 --> 00:34:31,240
Same with the person who always tells the truth. There's a power in truth that you see in the

255
00:34:31,240 --> 00:34:39,080
jot, because this is referring to those stories where the Buddha or the Bodhisatta says some

256
00:34:39,080 --> 00:34:43,480
truth. Like I've never done this, I've never done that, and there's a powerful thing that comes

257
00:34:43,480 --> 00:34:54,200
from it, some result. The next one is resolution. So this is when you determine I vow this, I vow that,

258
00:34:55,080 --> 00:35:00,760
and then you're able to stick with it. Like when you sit for an hour, and you don't give up at 55

259
00:35:00,760 --> 00:35:07,160
minutes. When you go for the 60 minutes, because you set your timer, that's cultivating this kind of

260
00:35:07,160 --> 00:35:16,680
thing, determination. So don't when you say, oh, just five minutes left, no, no. Well, it's okay,

261
00:35:16,680 --> 00:35:22,760
but you're not developing determination. My teacher once reminded us of that. So I'm just

262
00:35:22,760 --> 00:35:26,280
repeating what he said. It's a good, good dainching.

263
00:35:26,280 --> 00:35:34,680
So it's the simile here. Just as a rocky mountain peak unmoved stands firm, a stab,

264
00:35:34,680 --> 00:35:40,040
firm stablish it, and shaken by the boisterous scales and always in its place of

265
00:35:40,040 --> 00:35:45,400
bides. So the mountain stays firm. You don't see a Mount Everest suddenly,

266
00:35:45,400 --> 00:35:59,160
these get up and move somewhere else, because it's uncomfortable. The ninth is Maita. Maita is a

267
00:35:59,160 --> 00:36:04,920
perfection. Good will. Good will here. Good will is a good translation, but friendliness is

268
00:36:04,920 --> 00:36:08,280
literally. I don't know why anyone doesn't, why no one uses the word friendliness. That's

269
00:36:08,280 --> 00:36:14,280
literally what it means. As water cleans with all the like, the righteous and the wicked too,

270
00:36:14,280 --> 00:36:21,320
from dust and dirt, dirt of every kind and with refreshing coolness films. Water doesn't discriminate.

271
00:36:21,320 --> 00:36:27,720
It doesn't say, oh, this is an evil person. I'm not going to quench their thirst. It doesn't say,

272
00:36:27,720 --> 00:36:37,800
oh, this is a ugly person. I'm not going to clean them. Water cleanses everyone, righteous and wicked.

273
00:36:37,800 --> 00:36:42,200
Water make you have a shower. You get clean. Doesn't matter whether you're a good person or a bad

274
00:36:42,200 --> 00:36:48,680
person. He said, in the same way, I will develop friendliness towards all. Be they good or be they evil.

275
00:36:52,760 --> 00:36:59,080
And the tenth is equanimity, not indifference. That's a bad translation. Equanimity.

276
00:37:00,920 --> 00:37:06,600
Just as the earth, whatever is thrown in this is simple, similar. Didn't we have this similarly

277
00:37:06,600 --> 00:37:17,480
already? So patience and equanimity have the same similarly. Again, the earth, if you throw

278
00:37:17,480 --> 00:37:28,680
something on the earth, similar one anyway. Whatever is thrown upon or whether sweet or foul

279
00:37:28,680 --> 00:37:33,080
indifferent to all is to all alike. So it doesn't matter whether you throw garbage or you

280
00:37:33,080 --> 00:37:39,720
defecate on the earth. It treats it all the same. That's a very powerful practice. It's a part

281
00:37:39,720 --> 00:37:45,400
of our practice. But this was more far-reaching. The Buddha would, the Bodhisattva would go into any

282
00:37:45,400 --> 00:37:55,560
situation and be tortured and beaten and so on and always remind himself to stay objective and

283
00:37:55,560 --> 00:38:05,960
equanimists and not react. Those are the ten perfections. I think that's all I have to say

284
00:38:05,960 --> 00:38:15,640
about that. And that's all our questions for tonight. So thank you all for tuning in.

285
00:38:15,640 --> 00:38:29,160
YouTube's chatting away too much chatting. Are you all chatting over there or we're trying to

286
00:38:29,880 --> 00:38:36,840
focus on the Dhamma? You should chat after the talk. Is there a way you can keep chatting after

287
00:38:36,840 --> 00:38:52,760
I finish talking? Now you can chat away. Anyway, thank you all for listening. Have a good night.

