1
00:00:00,000 --> 00:00:13,000
Discussion of the Dhamma at the right time or on the proper occasion or frequently is a great blessing.

2
00:00:13,000 --> 00:00:19,500
So tonight for our Dhamma discussion we have from left to right,

3
00:00:19,500 --> 00:00:28,500
the normal Palañani, Bikuni, Nagasena.

4
00:00:28,500 --> 00:00:31,500
It's going to be his new name. I haven't told him yet.

5
00:00:31,500 --> 00:00:44,500
So here's the surprise unveiling of the new name for this person in between us in the middle.

6
00:00:44,500 --> 00:00:58,500
His new name is Nagasena, and tomorrow he will be becoming a Samanera, which is a little monk.

7
00:00:58,500 --> 00:01:05,500
It's a novice ordination, so he gets ten rules that he has to follow.

8
00:01:05,500 --> 00:01:08,500
It's the first step towards full ordination.

9
00:01:08,500 --> 00:01:17,500
It's a good halfway stepping stone, I guess you can say, to becoming fully ordained,

10
00:01:17,500 --> 00:01:22,500
because it allows you to become comfortable with the lifestyle,

11
00:01:22,500 --> 00:01:30,500
without having to be burdened with the full weight of responsibility and discipline and so on.

12
00:01:30,500 --> 00:01:39,500
So there's more leeway, the rules aren't so strict.

13
00:01:39,500 --> 00:01:44,500
The rules are strict, you have just far, far fewer of them.

14
00:01:44,500 --> 00:01:51,500
So that will be happening tomorrow, and maybe we'll have a video of that, who knows?

15
00:01:51,500 --> 00:02:00,500
And then to his right, you have me, and I think many of you are familiar with me.

16
00:02:00,500 --> 00:02:09,500
My name is Yutadama, and just a monk, Bikhu.

17
00:02:09,500 --> 00:02:17,500
So, welcome to tonight's episode of Ask a Monk, or Monk Radio, or whatever you want to call it.

18
00:02:17,500 --> 00:02:21,500
We'll now begin to take questions.

