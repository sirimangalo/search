1
00:00:00,000 --> 00:00:05,000
An example of family conflict would be addressing their bad use of bad language.

2
00:00:05,000 --> 00:00:12,000
Should I have just ignored it or was I right to show my disapproval?

3
00:00:12,000 --> 00:00:20,000
No, that sounds very, very, very evangelical.

4
00:00:20,000 --> 00:00:24,000
Is that the right word? I don't want to say Christian because it's not what I mean.

5
00:00:24,000 --> 00:00:25,000
I really don't.

6
00:00:25,000 --> 00:00:30,000
I really know I really am personally aware that there are good Christians out there.

7
00:00:30,000 --> 00:00:35,000
It's just a word that happens to roll off the tongue for bad things.

8
00:00:35,000 --> 00:00:41,000
There are really good Christians, Catholics, Protestants, people out there, Mormons, and Jehovah's Witnesses.

9
00:00:41,000 --> 00:00:46,000
But there's something, something that's not nothing to do with Christianity.

10
00:00:46,000 --> 00:00:52,000
So what is it? Is it Evangelism or Protestantism?

11
00:00:52,000 --> 00:00:54,000
What's the word?

12
00:00:54,000 --> 00:00:58,000
Zealous, Zealous, no.

13
00:00:58,000 --> 00:01:00,000
Over, Zealous.

14
00:01:00,000 --> 00:01:06,000
What do you call it when you try to impress your views on other people?

15
00:01:06,000 --> 00:01:09,000
Anyone?

16
00:01:09,000 --> 00:01:12,000
I think you're right. Zellitry, I think you call it.

17
00:01:12,000 --> 00:01:14,000
Zellitry.

18
00:01:14,000 --> 00:01:21,000
I mean, first of all, there's no such thing as bad words.

19
00:01:21,000 --> 00:01:28,000
There are just words. There was a monk once who went around calling me, calling his friends,

20
00:01:28,000 --> 00:01:31,000
Wasala. I think Wasala was it?

21
00:01:31,000 --> 00:01:35,000
Which is a word that means dog.

22
00:01:35,000 --> 00:01:40,000
It doesn't mean dog, but it's equivalent to it in English, how these people call each other.

23
00:01:40,000 --> 00:01:43,000
Hey, dog.

24
00:01:43,000 --> 00:01:49,000
Wasala, I believe it was the word was Wasala, and it means like,

25
00:01:49,000 --> 00:02:00,000
a servant or a very low-class person.

26
00:02:00,000 --> 00:02:04,000
But it was because he was just used to referring to people in that way.

27
00:02:04,000 --> 00:02:06,000
And so they went to the Buddha and they said,

28
00:02:06,000 --> 00:02:11,000
Look, this guy's calling his Wasala is really insulting.

29
00:02:11,000 --> 00:02:14,000
And the Buddha said, Well, that's just the way he, it's just Wohara.

30
00:02:14,000 --> 00:02:17,000
It's just the words that he used to.

31
00:02:17,000 --> 00:02:25,000
So it's a pretty good example of overreacting, because even if there is was something good about

32
00:02:25,000 --> 00:02:32,000
disapproving of other people's behavior, I wouldn't say it's a good example of what should be disapproved of.

33
00:02:32,000 --> 00:02:44,000
But as you'll probably learn the hard way, no, no, generally no good comes from disapproving at all anyway.

34
00:02:44,000 --> 00:03:00,000
I always felt they kind of have a duty to my parents to find some way to alert them to their deleterious behavior for their benefit, right?

35
00:03:00,000 --> 00:03:09,000
But other members of the family have never felt that way towards, I don't think it's held up by the Buddha's teaching.

36
00:03:09,000 --> 00:03:30,000
And I don't think it's really logical to think that we have some kind of just because we were born before the person or because we have the same blood as the person's gene pool or whatever that we have any reason to admonish them.

37
00:03:30,000 --> 00:03:35,000
They're not our students, they didn't ask for our teachings.

38
00:03:35,000 --> 00:03:44,000
So even when they're doing bad deeds, it's not our, first of all, it's not our duty, second of all, it's not helpful.

39
00:03:44,000 --> 00:03:55,000
The disapproval of other people's bad deeds, the open disapproval is rarely if ever, unless they're your students and they're really, really trust you.

40
00:03:55,000 --> 00:04:18,000
It's rarely beneficial, even if they're your students, unless they are really, I would like to say advanced or special people, they tend to get very, very loose faith, very, very quickly when you start criticizing them.

41
00:04:18,000 --> 00:04:25,000
So, except in special cases, criticism is very dangerous.

42
00:04:25,000 --> 00:04:30,000
So, showing your disapproval, no, that's really, really a bad idea.

43
00:04:30,000 --> 00:04:32,000
You have to show your happiness.

44
00:04:32,000 --> 00:04:37,000
It's like the fight that you have to fight and that you have to win is who's happier.

45
00:04:37,000 --> 00:04:45,000
Them who are engaged in all of their pursuit of sensuality or you who are, who have ended up.

46
00:04:45,000 --> 00:04:59,000
And if you're not happier, if they're the ones sitting around cussing and swearing and enjoying it, and you're all on their back about it, right?

47
00:04:59,000 --> 00:05:01,000
You're not winning, you're losing.

48
00:05:01,000 --> 00:05:06,000
You're losing the happiness battle. They're happier than you are.

49
00:05:06,000 --> 00:05:08,000
Yeah, they are happy, you're like miserable.

50
00:05:08,000 --> 00:05:11,000
That was the big problem for me, is that I was miserable.

51
00:05:11,000 --> 00:05:15,000
When you, I was doing the right thing, but I was like, this is like toughest nails.

52
00:05:15,000 --> 00:05:19,000
It was the hardest thing that I'd ever done, of course.

53
00:05:19,000 --> 00:05:26,000
And so, that kind of clicked with me at one point and I said, you know, I'm not, I can't win this battle.

54
00:05:26,000 --> 00:05:29,000
I can't prove to my family that what I'm doing as well.

55
00:05:29,000 --> 00:05:35,000
I can't convince them that what I'm doing is right, because I'm not happy.

56
00:05:35,000 --> 00:05:43,000
I know that what I'm doing is right, but, sorry, I wouldn't say miserable, but really struggling.

57
00:05:43,000 --> 00:05:55,000
Because there's no question that I totally, the suffering that I had before, after practicing, after finding meditation and realizing that badly, it was incredibly liberating.

58
00:05:55,000 --> 00:06:02,000
This is, the meditation is the way to go, but it was still like this is not easy for me.

59
00:06:02,000 --> 00:06:10,000
I have all these conflicting desires and emotions that I really don't want and have to work to get rid of.

60
00:06:10,000 --> 00:06:16,000
But I knew, there was no question that this is the right path, of course.

61
00:06:16,000 --> 00:06:29,000
But until you get to the point where you're actually able to express it, when you're fighting with these things, there's very little, you can do to convince other people.

62
00:06:29,000 --> 00:06:45,000
So if you gain happiness and meditation, the only way to convince people that you have gained that, that meditation is the better way, or your way is the better way, is to show them.

63
00:06:45,000 --> 00:06:48,000
And to give them proof that it's the better way.

64
00:06:48,000 --> 00:06:58,000
Because if your path leads you to be judgmental and disapproving, it's not really appealing.

65
00:06:58,000 --> 00:07:04,000
It's not really how anyone else would ever wish to be.

