1
00:00:00,000 --> 00:00:04,000
What exactly is noting in meditation?

2
00:00:04,000 --> 00:00:09,000
Well, if you're asking because you've heard the word and don't know what it means,

3
00:00:09,000 --> 00:00:14,000
I'd recommend you to read my booklet on how to meditate.

4
00:00:14,000 --> 00:00:17,000
That's the first thing.

5
00:00:17,000 --> 00:00:21,000
But if you're meaning more philosophically or more technically,

6
00:00:21,000 --> 00:00:26,000
what is going on there?

7
00:00:26,000 --> 00:00:33,000
It is the fixing of the mind and the straightening of the mind in regards to the object,

8
00:00:33,000 --> 00:00:39,000
reminding yourself of the essence of the experience,

9
00:00:39,000 --> 00:00:43,000
the reality of the experience, as opposed to seeing it as an entity

10
00:00:43,000 --> 00:00:53,000
or something, a relation in terms of liking or disliking it or identifying with it,

11
00:00:53,000 --> 00:00:59,000
just to experience it as it is to remind yourself of what it is.

12
00:00:59,000 --> 00:01:03,000
Because that's the word set the mindfulness, which translates mindfulness.

13
00:01:03,000 --> 00:01:07,000
But the word set the means remembrance.

14
00:01:07,000 --> 00:01:17,000
The reminding, remindedness, the recollectedness, the ability to recognize something for what it is.

15
00:01:17,000 --> 00:01:25,000
And so that's what the noting does. The noting is the creation in the mind of a recognition,

16
00:01:25,000 --> 00:01:29,000
which is why the word is not, which word you choose is not important,

17
00:01:29,000 --> 00:01:51,000
but the word itself is an act of recognizing the object as close as possible to what it actually is.

