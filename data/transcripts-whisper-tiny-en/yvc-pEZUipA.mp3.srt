1
00:00:00,000 --> 00:00:02,000
I'm asking for your opinion.

2
00:00:02,000 --> 00:00:04,000
Okay.

3
00:00:04,000 --> 00:00:11,000
In your opinion, do you think science will overthrow religion in the near future?

4
00:00:11,000 --> 00:00:19,000
It's an interesting question, I remember.

5
00:00:19,000 --> 00:00:26,000
I think, essentially, certain religious...

6
00:00:26,000 --> 00:00:35,000
Like, I think specific religious institutions might get minimized.

7
00:00:35,000 --> 00:00:37,000
Like Christianity.

8
00:00:37,000 --> 00:00:42,000
I think recently, before I came to you, I was reading about...

9
00:00:42,000 --> 00:00:54,000
There was a poll survey, how many people were Christians or atheists in America.

10
00:00:54,000 --> 00:01:04,000
And I think there was an actual increase in a number of people becoming atheists.

11
00:01:04,000 --> 00:01:14,000
So, in terms of religious institutions and people adopting specific types of religion,

12
00:01:14,000 --> 00:01:17,000
I think that's actually decreasing in a sense.

13
00:01:17,000 --> 00:01:20,000
But there is still religious thinking.

14
00:01:20,000 --> 00:01:30,000
There is still the habit of wanting to adopt a certain song withism.

15
00:01:30,000 --> 00:01:35,000
I think that's just the natural way that the brain works.

16
00:01:35,000 --> 00:01:45,000
We tend to buy into a type of blind faith if we don't really have any knowledge to it.

17
00:01:45,000 --> 00:01:47,000
It doesn't even have to be religion.

18
00:01:47,000 --> 00:01:53,000
It could be following sort of political ideology that we don't really know about.

19
00:01:53,000 --> 00:01:56,000
That's what happened in Germany.

20
00:01:56,000 --> 00:02:03,000
And that's what's happening in South Korea is that all other informations are not South Korea or North Korea.

21
00:02:03,000 --> 00:02:07,000
All other information is blocked and you're just...

22
00:02:07,000 --> 00:02:15,000
You're just believing this one idea and you're just following a blind faith without any other evidence.

23
00:02:15,000 --> 00:02:22,000
And so, I think science will eventually expand,

24
00:02:22,000 --> 00:02:32,000
but I think scientific value in terms of thinking things in complete and perfect reasoning

25
00:02:32,000 --> 00:02:39,000
and observing reality in complete and in complete objective manner,

26
00:02:39,000 --> 00:02:42,000
I don't think that will actually ever happen.

27
00:02:42,000 --> 00:02:46,000
We have discussed this before that science can be actually quite problematic sometimes.

28
00:02:46,000 --> 00:02:51,000
Even though it's inherent definition, it's not supposed to be like that.

29
00:02:51,000 --> 00:02:59,000
And the things that cloud science, like desire for wealth, fame, power.

30
00:02:59,000 --> 00:03:00,000
Yeah.

31
00:03:00,000 --> 00:03:05,000
How much are saying? How much are science dependent on where the money is?

32
00:03:05,000 --> 00:03:11,000
Yeah, and I will say actually those are actually sorts of different types of religion.

33
00:03:11,000 --> 00:03:16,000
They're also religious thinking that they're not based on actual reality.

34
00:03:16,000 --> 00:03:19,000
That they're based on personal agendas.

35
00:03:19,000 --> 00:03:24,000
They're based on beliefs that you are socialized to believe and you're not even...

36
00:03:24,000 --> 00:03:26,000
You don't even know it's true or not.

37
00:03:26,000 --> 00:03:31,000
You're just taught to believe that way and they bring defilements and bad habits

38
00:03:31,000 --> 00:03:35,000
and you don't really observe them yourself. You just go with it.

39
00:03:35,000 --> 00:03:47,000
And yeah, so I think specific religious sex or denominations will get decrease in the future,

40
00:03:47,000 --> 00:03:51,000
but I think religious thinking...

41
00:03:51,000 --> 00:03:54,000
I just think that's just general habit of the mind.

42
00:03:54,000 --> 00:03:59,000
It tends to deviate away from reality because it's easier that way.

43
00:03:59,000 --> 00:04:08,000
But hopefully in terms of meditation practice and not just Buddhism,

44
00:04:08,000 --> 00:04:12,000
but just the habit of seeing reality will expand more and more.

45
00:04:12,000 --> 00:04:21,000
People will have more peace and be more objective in a sense.

46
00:04:21,000 --> 00:04:30,000
As I was saying, we've got these two forces, many forces, but you can look at these two forces.

47
00:04:30,000 --> 00:04:34,000
We're working in a good way.

48
00:04:34,000 --> 00:04:39,000
To say that it's going to go one way or the other, it's really just going to be a struggle.

49
00:04:39,000 --> 00:04:44,000
There's always going to be forces of good and forces of evil.

50
00:04:44,000 --> 00:04:52,000
I mean, it really is that way that there is that in the world.

51
00:04:52,000 --> 00:04:55,000
Maybe the balance will never shift. Maybe it's a part of the universe to have yin and yang and so on.

52
00:04:55,000 --> 00:05:00,000
It's up to you whether you want to be on the winning side or the losing side or the good side or the best.

53
00:05:00,000 --> 00:05:04,000
Yeah, I mean, there has to be a contrast with things to exist.

54
00:05:04,000 --> 00:05:08,000
You have to have good, to have bad to exist and vice versa.

55
00:05:08,000 --> 00:05:10,000
I think that's always going to be the case.

56
00:05:10,000 --> 00:05:16,000
It's always going to be a minority of some people with different beliefs because everyone is different.

57
00:05:16,000 --> 00:05:19,000
Can't have everyone exactly the same.

58
00:05:19,000 --> 00:05:43,000
Yeah, hopefully that's a good answer.

