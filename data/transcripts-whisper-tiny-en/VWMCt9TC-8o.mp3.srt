1
00:00:00,000 --> 00:00:10,800
I know everyone more questions and answers.

2
00:00:10,800 --> 00:00:15,000
Today's question, all of these, most of these questions are questions that were asked

3
00:00:15,000 --> 00:00:26,000
many months ago, so I'm not really answering people's questions directly and they apologize

4
00:00:26,000 --> 00:00:36,400
for that. But the idea as well is to find questions that are frequently asked or questions

5
00:00:36,400 --> 00:00:52,000
that appeal to many, many meditators. And so the answers can have a broad reach.

6
00:00:52,000 --> 00:01:08,000
So today's question is about the tension that comes from meditating. So the question is,

7
00:01:08,000 --> 00:01:17,000
why do we hold our hands together when we do walking meditation? Because this person finds

8
00:01:17,000 --> 00:01:25,000
makes their body tense, and wouldn't it be better if we just had our hands relaxed at

9
00:01:25,000 --> 00:01:36,240
the side? So the question is whether there is some deeper reason for holding our hands

10
00:01:36,240 --> 00:01:44,720
together, or whether it's all the same if we just put our hands by our sides.

11
00:01:44,720 --> 00:02:01,000
So again, to do away with the technical side of things first, there's a minor reason

12
00:02:01,000 --> 00:02:12,360
for holding our hands together. I think it reflects a more focused posture than just

13
00:02:12,360 --> 00:02:17,400
swinging your hands by your side, right? Because then there's movement involved. If you

14
00:02:17,400 --> 00:02:28,000
clasp your hands together, they're not moving anymore. And so it leads to greater focus,

15
00:02:28,000 --> 00:02:39,800
right? So we try to create a state whereby we can focus on one experience at a time. And

16
00:02:39,800 --> 00:02:46,600
so we actually aren't walking. Walking is not what we're doing. What we're doing is standing

17
00:02:46,600 --> 00:02:54,680
quite still. And then moving one piece of our body, our foot, wait until that movement ends,

18
00:02:54,680 --> 00:02:59,720
and start another movement. And try and be aware of the beginning and the end of the movement,

19
00:02:59,720 --> 00:03:08,440
right? So from a technical perspective, the answer is yes, there's an important, and there's

20
00:03:08,440 --> 00:03:15,960
a reason for it. Put your holder hands together when you walk. But it's common for people

21
00:03:15,960 --> 00:03:26,840
to not want to do this because it is unnatural. And for sure in the beginning, for many people

22
00:03:26,840 --> 00:03:35,880
leads to tension, tension in the shoulders, in the back. And so questions, why would

23
00:03:35,880 --> 00:03:46,680
you do such a thing? Or wouldn't it be better to relax, to find some way to not feel that tension?

24
00:03:52,120 --> 00:04:02,520
And so the first two parts, the first part is that it's not actually, in most cases, the fact

25
00:04:02,520 --> 00:04:08,520
that you're holding your hands together that's causing attention. For a new meditator, the tension

26
00:04:08,520 --> 00:04:23,240
is generally caused by being tense, having a overactive, stressful state of being, having habits

27
00:04:23,240 --> 00:04:38,120
that are reoccurring stress, causing the mind to become stressed out repeatedly. So throughout

28
00:04:38,120 --> 00:04:43,320
the day, we start to think about things we have to do in the future, things that happen to us in

29
00:04:43,320 --> 00:04:50,040
the past, experiences in the present, our wants and our desires, what we could be doing instead

30
00:04:50,040 --> 00:04:59,240
of meditating, our irritations, the disliking or the worry about the practice,

31
00:05:04,520 --> 00:05:12,760
doubt about the practice, all of these things lead to stress. And that stress, of course, builds

32
00:05:12,760 --> 00:05:19,720
up in the body and leads to tension. And so it's quite common for a beginner meditator to

33
00:05:19,720 --> 00:05:24,680
experience the tension and to think, oh, this is uncomfortable, this is unpleasant, this is

34
00:05:25,320 --> 00:05:34,840
suffering. And based on their way of looking at things, to try and find some way to

35
00:05:36,520 --> 00:05:43,000
end the suffering. It seems reasonable, it's reasonable to want to try to end suffering, isn't

36
00:05:43,000 --> 00:05:54,280
that what Buddhism was all about. But the first point to clarify is, it's not that you hold your

37
00:05:54,280 --> 00:06:01,240
hands together, that you suffer, that there is suffering. It's not even that you hold your hands

38
00:06:01,240 --> 00:06:07,800
together, that your tense, because of course, meditators, some meditators do it and even from the

39
00:06:07,800 --> 00:06:13,880
beginning, I have no stress or suffering. Certainly someone who's done it for a while will finally

40
00:06:13,880 --> 00:06:27,160
do it without any stress or suffering. But the second thing is, even if, or even worried the case

41
00:06:27,160 --> 00:06:33,960
that holding your hands together lead to tension and practically speaking, that's what's happening

42
00:06:33,960 --> 00:06:39,320
for a beginner meditator, because of course, when you're very, very stressed out, you can find ways

43
00:06:39,320 --> 00:06:46,680
to relax your body, to keep the stress from impacting the body, right? When we have mental stress,

44
00:06:46,680 --> 00:06:56,520
we'll lie down or we'll go for a run or we'll engage in sorts of things dancing maybe or

45
00:06:56,520 --> 00:07:04,200
we eat lots of food or something, or do something, take drugs, right? Marijuana is one big one now,

46
00:07:04,200 --> 00:07:20,600
alcohol prevents the stress from taking over. So, practically speaking, holding your hands

47
00:07:20,600 --> 00:07:31,480
together in this case is causing you attention. So, we have to understand what we're trying to do

48
00:07:31,480 --> 00:07:42,280
in the meditation. Meditation is not for the purpose of relaxing or avoiding or manipulating or

49
00:07:42,280 --> 00:07:51,640
fixing even. Meditation in the Buddha's teaching is ultimately for the purpose of seeing, knowing,

50
00:07:51,640 --> 00:08:05,080
understanding, facing, being patient, and being objective, and equanimates about reality.

51
00:08:05,080 --> 00:08:17,000
So, what happens when we run away, when we relax, is that we reinforce our aversion to the tension,

52
00:08:17,960 --> 00:08:31,400
we create, or we augment our habits of disliking of the tense state or the suffering state,

53
00:08:31,400 --> 00:08:49,240
and it gets worse and worse and worse. So, in the Buddha's teaching, we understand suffering

54
00:08:49,240 --> 00:09:00,360
in a very different way than an ordinary understanding. So, our ordinary understanding of suffering

55
00:09:00,360 --> 00:09:09,480
is what we call dukkawed and we understand suffering to be like this. We have this feeling and that

56
00:09:09,480 --> 00:09:21,640
suffering towards the answer to our suffering and the feeling. When you change the condition,

57
00:09:22,280 --> 00:09:28,600
when you run away from it, when you do something, relax your arms, do some yoga, do some stretching,

58
00:09:28,600 --> 00:09:37,000
lie down, go for a swim, go for a run, take some drugs, whatever it might be.

59
00:09:40,680 --> 00:09:46,360
While the suffering is gone, right? And so, this is how people understand suffering and

60
00:09:46,360 --> 00:09:54,200
why people come to practice meditation is because while they've found that philosophy,

61
00:09:54,200 --> 00:10:01,320
that world view, that approach, lacking, they come across some suffering or some

62
00:10:02,920 --> 00:10:09,320
reality of suffering that they just can't figure out how to free themselves from.

63
00:10:11,000 --> 00:10:19,880
They start to see that they admit that they're not capable of escaping suffering,

64
00:10:19,880 --> 00:10:26,200
running away from it, of ending the feelings. This is called dukkhasabawa and when

65
00:10:26,200 --> 00:10:31,640
meditators come to practice, they're just grappling with this. So, half the time, they're still

66
00:10:31,640 --> 00:10:41,400
trying to run away and slowly, slowly, they're starting to become more capable, more

67
00:10:41,400 --> 00:10:55,080
able to face the suffering and to be with suffering and to change the way they look at suffering.

68
00:10:55,080 --> 00:11:01,320
Dukkhasabawa means suffering is a part of life. There are sufferings that we can't free ourselves

69
00:11:01,320 --> 00:11:09,400
from old age, sickness, death. Sometimes when we lose something, lose a loved one, lose a job,

70
00:11:09,400 --> 00:11:15,800
or a possession, or something, and we just can't get it back. There's no solution. It's not as

71
00:11:15,800 --> 00:11:22,920
simple as just relaxing your hand. We realize that this approach of trying to escape suffering

72
00:11:22,920 --> 00:11:30,440
is not actually helping us. It led us to this point where we suffer terribly for our losses or

73
00:11:30,440 --> 00:11:48,200
for our situation, when they get out of our out of hand, out of our ability to control them,

74
00:11:49,240 --> 00:11:58,760
then running away is no longer an option. We see that, and we see that that is only led us to this,

75
00:11:58,760 --> 00:12:09,160
to greater stress. Take this example, when you relax your hands, you are walking with your hands

76
00:12:09,160 --> 00:12:16,600
together and you relax them, and then the next time that tension comes, you immediately have to

77
00:12:16,600 --> 00:12:22,760
find some way to relax, and anytime tension comes, you have to find a way to relax, and more and

78
00:12:22,760 --> 00:12:29,720
more until, well, this is the state of most of us. People who go to get massages under regular

79
00:12:29,720 --> 00:12:36,680
basis, it's not that they're more tense than other people. Well, in fact, they become more tense,

80
00:12:36,680 --> 00:12:41,960
but it's that they're more reactive to the tension. They've reacted again and again to the tension.

81
00:12:45,000 --> 00:12:50,760
Someone who comes to practice mindfulness, who really gets into mindfulness,

82
00:12:50,760 --> 00:12:55,560
will change the way they look at suffering, and they'll start to see that. It's the wrong way of

83
00:12:55,560 --> 00:13:00,360
looking at things. A much better way to look at suffering is that it's a part of life.

84
00:13:02,600 --> 00:13:09,240
And instead of trying to escape the things that cause a suffering, we have to stop letting the

85
00:13:09,240 --> 00:13:14,120
things that cause a suffering cause a suffering. Letting those things cause a suffering.

86
00:13:14,120 --> 00:13:21,160
So when there's tension in your body, as long as the very short answer is just to say to yourself,

87
00:13:21,160 --> 00:13:28,840
10, 10, 10. Of course, when you do that, you'll start to face the tension. It will have less

88
00:13:28,840 --> 00:13:35,320
of a power, or there will be less of a reaction to it, and you'll be less stressed out,

89
00:13:35,320 --> 00:13:41,720
but as a result, you'll be less tense. The tension will slowly, the feedback loop goes backwards,

90
00:13:41,720 --> 00:13:46,200
and there'll be less and less tension because you're no longer stressed about your stress.

91
00:13:50,040 --> 00:13:54,360
Now, as you continue to practice, I mean, this sounds somewhat fatalistic,

92
00:13:55,000 --> 00:14:00,600
but you start to become more philosophical about it. You say, well, it's a part of life, right?

93
00:14:01,720 --> 00:14:09,480
And that changes the way you look at things, and you start to become more reasonable and rational

94
00:14:09,480 --> 00:14:18,600
and objective, and really wise about things, seeing that things change, that the things that

95
00:14:18,600 --> 00:14:27,240
we like, you know, are unpredictable, ultimately unpredictable. Even if you can predict for a long,

96
00:14:27,240 --> 00:14:33,960
long time, have a stable life where you get everything you want, it's never sure. We can't be

97
00:14:33,960 --> 00:14:43,960
sure forever. And so you become more philosophical and say, oh, everything changes. Of course,

98
00:14:43,960 --> 00:14:51,400
in meditation, it's not just an intellectual thing, it's every moment seeing the good things come,

99
00:14:52,120 --> 00:14:56,920
the bad things come and go and come and go, unpredictable.

100
00:14:56,920 --> 00:15:08,360
And so you start to, by philosophical, I mean, you become sort of dispassionate,

101
00:15:08,360 --> 00:15:18,520
at this point, rather than stressing or reacting to everything. And you start to see things

102
00:15:18,520 --> 00:15:24,920
more clearly and see that everything, everything that I'm noting, all these things that I'm

103
00:15:24,920 --> 00:15:40,360
experiencing, are unpredictable, are unsatisfying. It's called dukalakana. So, dukalakana means

104
00:15:40,360 --> 00:15:44,120
everything is suffering. What does that mean? It's very different, as I said, different from the way

105
00:15:44,120 --> 00:15:50,600
we think of suffering. Because of course, a pleasurable feeling is not suffering, not in the way

106
00:15:50,600 --> 00:16:00,120
we understand that. Why do we say that everything that arises is dukalakana? Dukalakana means it's not

107
00:16:00,120 --> 00:16:08,920
sukalakana. It can't satisfy you. It's not real happiness. When you have pleasure, if you see it

108
00:16:08,920 --> 00:16:14,600
as happiness, yes, this is it. This is what I'm looking for, what I want. Then you cling to it.

109
00:16:14,600 --> 00:16:23,560
And if you cling to it, then you want more. You cultivate a habit, develop an addiction to it.

110
00:16:25,240 --> 00:16:31,880
And it becomes more and more important that you get it. I think it's more and more necessary for

111
00:16:31,880 --> 00:16:37,960
you to be happy. And the same goes with unhappy things. When you have the tension, the more and

112
00:16:37,960 --> 00:16:46,840
more you try to change it, try and bring about the pleasure, bring about a happy state, a relaxed

113
00:16:46,840 --> 00:16:56,360
state. The more you become addicted to that and reactionary to the tension in the suffering.

114
00:16:57,880 --> 00:17:04,200
So, dukalakana doesn't mean that everything is painful. It means that everything is unsatisfying

115
00:17:04,200 --> 00:17:13,160
is not happiness. There's nothing. Nothing you can cling to. You can find, you can get, obtain, become,

116
00:17:14,600 --> 00:17:21,000
see, hear, smell, taste, feel, or think. Nothing that was satisfied. There's nothing that's worth

117
00:17:21,000 --> 00:17:29,240
clinging to. And you start to see this. This is really what happens. This is what's most imperative

118
00:17:29,240 --> 00:17:42,680
if this... What is necessary to happen in insight meditation is for you to see dukalakana,

119
00:17:42,680 --> 00:17:48,680
to see that nothing's worth clinging to. This tension is not worth clinging to, not worth trying

120
00:17:48,680 --> 00:17:58,680
to fix. Relaxation, a relaxed state is not worth clinging to. Nothing in the body or in the

121
00:17:58,680 --> 00:18:04,760
mind is worth clinging to me. Those are to see that through the practice. As you see, there's on

122
00:18:04,760 --> 00:18:14,120
and on and on and on. At this point, once you see more clearly and we've got the characteristic of

123
00:18:14,120 --> 00:18:22,840
suffering that all things are not worth clinging to are not dukalakana. Then you have... You start to

124
00:18:22,840 --> 00:18:30,440
have no problem with things like tension. You'll be equanimous, mindful, become dispassionate.

125
00:18:31,640 --> 00:18:36,600
So you start to see things and you really do become quite relaxed. But relaxed even when you're

126
00:18:36,600 --> 00:18:47,800
tense, even when you're in pain, even when you're thinking whatever you do throughout everything

127
00:18:47,800 --> 00:18:56,280
you do and you're relaxed. As you develop that, you come to the final understanding what we call

128
00:18:56,280 --> 00:19:05,160
the truth of suffering. The truth of suffering is... It's not a philosophy. I can't tell you what is

129
00:19:05,160 --> 00:19:11,720
the truth of suffering. It's outlined in the Buddha's teaching. But what we mean by the truth

130
00:19:11,720 --> 00:19:21,640
of suffering is you finally understand. You understand that nothing is worth clinging to. You

131
00:19:21,640 --> 00:19:33,720
understand that happiness is not in anything. That happiness is, there is happiness.

132
00:19:33,720 --> 00:19:46,680
But it's outside of experience. It's outside of phenomena. And you have to sort of like

133
00:19:46,680 --> 00:19:53,320
an epiphany. All that happens is this understanding that everything is... Nothing is worth

134
00:19:53,320 --> 00:19:59,160
clinging to just becomes very real. Instead of seeing individual things. So this isn't worth

135
00:19:59,160 --> 00:20:05,160
this thing that I'm this habit that I have. This need to be relaxed, this liking of the tension.

136
00:20:05,960 --> 00:20:13,480
This is cause for suffering and not worth clinging to. So you see that about individual things

137
00:20:13,480 --> 00:20:28,200
becomes an overarching, overwhelming realization. It's everything. Nothing. Nothing is worth

138
00:20:28,200 --> 00:20:35,320
clinging to. And it's not an intellectual thing. It's not something you think to yourself. It's an

139
00:20:35,320 --> 00:20:46,360
experience or a feeling, an outlook of you. Your whole mind is entranced by this

140
00:20:49,720 --> 00:20:54,600
feeling. There's this concept. Nothing is worth clinging to. It gets you.

141
00:20:56,040 --> 00:21:00,120
And that's the last moment. That's the moment of the realization of the truth of suffering.

142
00:21:00,120 --> 00:21:07,080
When you say that nothing is worth clinging to. And the next moment is nibana or nibana.

143
00:21:08,200 --> 00:21:20,760
The person enters into a state. No arising. You know suffering. And they become a noble one,

144
00:21:21,480 --> 00:21:27,000
someone who has seen the truth. And they live their life not worrying about things like

145
00:21:27,000 --> 00:21:33,400
the physical suffering, mental suffering.

146
00:21:36,680 --> 00:21:45,880
So a long answer to a very simple question. But I wanted to talk generally about this idea

147
00:21:45,880 --> 00:21:52,280
of trying to escape suffering. Because it comes up often in meditation. A meditator will

148
00:21:52,280 --> 00:21:59,880
need a comfortable seat or not be able to sit, have to do lying meditation, not be able to do

149
00:21:59,880 --> 00:22:08,520
any meditation, all because they have a perception that when they're suffering, you have to fix it.

150
00:22:09,240 --> 00:22:14,600
When there's a problem, you have to solve it. You have to change the condition.

151
00:22:14,600 --> 00:22:21,720
And that's absolutely the opposite of how insight meditation works.

152
00:22:23,400 --> 00:22:32,200
So when you come to this meditation practice, be aware, be aware that when an unpleasant

153
00:22:32,200 --> 00:22:38,440
experience arises, whether it's in walking or it's in sitting, and they will, the purpose of the

154
00:22:38,440 --> 00:22:44,360
meditation is to learn to face, is to learn to observe, is to learn to change the way we look at

155
00:22:44,360 --> 00:22:48,920
things rather than this is something to be avoided. This is something to be understood,

156
00:22:49,640 --> 00:23:00,520
to use as a tool to understand that nothing is worth clinging to. To start to see how our clinging,

157
00:23:00,520 --> 00:23:08,600
how our reactions, our aversion, means to suffering. So absolutely it has to be the opposite of that,

158
00:23:08,600 --> 00:23:15,160
whether or not relaxing our arms would get rid of the tension is not the point. The point is,

159
00:23:15,160 --> 00:23:22,040
the fact that you want to be free from tension is creating this habit, is reinforcing this way of

160
00:23:22,040 --> 00:23:28,360
looking at suffering, that it's all wrong. I must get away. I must, it's a vulnerability.

161
00:23:29,080 --> 00:23:34,360
When you become someone opposite, the Buddha said, put dasaloka, dami, he jittangyas in the

162
00:23:34,360 --> 00:23:45,560
company. When the changes in life come to you and your mind is not, it's not moved, it's not

163
00:23:45,560 --> 00:23:56,840
it doesn't waver. When you're not affected, when you don't react, that's safety. Then the

164
00:23:56,840 --> 00:24:08,200
Buddha, he was the word safety, camea. Camea means, safe means invincible, nothing in her dami.

165
00:24:09,400 --> 00:24:14,760
Take two people, one person experiences this tension, another person, both people experience this

166
00:24:14,760 --> 00:24:23,960
tension, one person has to get rid of it. This person is going to suffer because they feel this

167
00:24:23,960 --> 00:24:34,600
tension, the other person experiences it, nothing can bother you. They have no suffering,

168
00:24:34,600 --> 00:24:44,920
no vulnerability to suffering. Ultimately, it's the only sure way to be free from suffering.

169
00:24:44,920 --> 00:24:54,760
There's still not react to those things that cause you suffering. So there you go. Just trying to

170
00:24:54,760 --> 00:25:01,560
be clear when we talk about meditation, what type of meditation we're talking about, it's not

171
00:25:01,560 --> 00:25:10,200
meditation to relax, it's meditation to face, to overcome, to be capable of experiencing everything,

172
00:25:10,200 --> 00:25:22,840
the full spectrum of reality without suffering.

