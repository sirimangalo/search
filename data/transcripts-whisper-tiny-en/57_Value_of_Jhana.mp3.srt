1
00:00:00,000 --> 00:00:10,640
value of Jana. Question. Dear Yutadamo, how much value do you put on the practice of Jana?

2
00:00:12,000 --> 00:00:19,520
Did not the Buddha advocate Jana practice at least up to a point and does it not have a use

3
00:00:19,520 --> 00:00:30,000
in deepening repass on a practice? Answer. The word Jana is a highly debated one among modern

4
00:00:30,000 --> 00:00:39,920
teravada Buddhists. Not the meaning of the word, but it's use and place in Buddhism. I think a lot

5
00:00:39,920 --> 00:00:47,600
of the controversy comes from the fact that we apply too much meaning to the word. The word Jana

6
00:00:47,600 --> 00:00:57,760
means meditation or focusing or absorption. It means fixing the mind on an object and in truth

7
00:00:57,760 --> 00:01:06,400
that definition applies to all meditation. This is why the Buddha said there is no wisdom

8
00:01:06,400 --> 00:01:14,480
without Jana and there is no Jana without wisdom because true wisdom comes only through proper

9
00:01:14,480 --> 00:01:24,320
meditation practice and only those practices that lead to wisdom are proper to be called meditation.

10
00:01:25,840 --> 00:01:34,320
The teravada tradition categorizes meditation as being of two types. The first type is called

11
00:01:34,320 --> 00:01:41,600
some atom meditation where one focuses on a single conceptual object created in the mind.

12
00:01:41,600 --> 00:01:53,920
For example the Buddha or a color, the object of this type of meditation is not real and as a result

13
00:01:53,920 --> 00:02:04,160
it cannot bring wisdom or understanding about reality. It will bring great states of calm and so

14
00:02:04,160 --> 00:02:12,560
it is called samata or tranquility meditation. This type of meditation can be useful

15
00:02:13,120 --> 00:02:19,360
as a precursor to a pastana because it comes and strengthens the mind.

16
00:02:20,880 --> 00:02:30,960
It can also be used to attain extraordinary mental powers of various sorts, but it does not lead

17
00:02:30,960 --> 00:02:40,160
directly to freedom from suffering. To become free from suffering requires a different type of

18
00:02:40,160 --> 00:02:53,120
Jana called Vipasana Jana. Vipasana Jana means seeing clearly meditation. When you practice Vipasana

19
00:02:53,120 --> 00:03:02,880
you also focus on an object and so it can also be called Jana. Your mind is focused on

20
00:03:02,880 --> 00:03:13,520
and clearly aware of a single object. When we say to ourselves rising, we are clearly aware of

21
00:03:13,520 --> 00:03:22,880
the stomach rising. When we say falling, we are clearly aware of the falling. Through this practice

22
00:03:22,880 --> 00:03:32,800
the mind also gives up the mental hindrances of liking, disliking, drowsiness, distraction and doubt.

23
00:03:32,800 --> 00:03:44,400
It becomes fixed and focused and so you can say it enters Jana. It enters the Vipasana Jana.

24
00:03:46,080 --> 00:03:53,600
The real difference between samata meditation and Vipasana meditation is that

25
00:03:53,600 --> 00:04:04,000
samata takes a conceptual object and Vipasana meditation takes reality as an object. Anything

26
00:04:04,000 --> 00:04:13,120
that arises in the present moment, whether it be in body, feeling, thought, states of mind

27
00:04:13,680 --> 00:04:22,320
or sensory experience. A lot of the argument in debate about Jana misses the point.

28
00:04:22,320 --> 00:04:30,720
Knowing about terms and ideas can distract us from the goal, which is to understand

29
00:04:30,720 --> 00:05:00,560
ultimate reality and become free from suffering.

