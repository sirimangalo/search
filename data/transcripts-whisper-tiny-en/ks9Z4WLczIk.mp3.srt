1
00:00:00,000 --> 00:00:16,000
Column. I find it difficult to overcome noise, distraction when trying to meditate. I use earplugs, but I know that it is not the right way. Please advise.

2
00:00:16,000 --> 00:00:33,000
Yes, it is not the right way. It would be good to just acknowledge the noise and acknowledge the anger or the feelings of disturbance that arise due to that noise.

3
00:00:33,000 --> 00:00:47,000
We always have around things that we don't like, and we have to deal with it. So be mindful on the disliking that is arising.

4
00:00:47,000 --> 00:01:07,000
When the noise is there and you say hearing, hearing, then you will note a feeling of distraction or anger or whatever arises.

5
00:01:07,000 --> 00:01:24,000
So you know that by saying anger, anger, and by doing so you learn to slowly accept that there is noise around you.

6
00:01:24,000 --> 00:01:41,000
It sounds weird now when you don't have the experience that it would be possible just by saying anger, anger, hearing, hearing that you would be able to accept it.

7
00:01:41,000 --> 00:02:03,000
But this is what actually happens. So just give it a try and take off the earplugs and deal with the life as it is, deal with reality and note it as it is.

8
00:02:03,000 --> 00:02:15,000
Just some anecdotes, I mean that is the answer. But when I first started practicing, I got lots of crazy stories because I had no clue what I was doing.

9
00:02:15,000 --> 00:02:28,000
And so I thought when you acknowledge hearing, hearing the sound is supposed to disappear no matter what sound it is. So they were doing construction and they had those cement mixers outside of my cookie.

10
00:02:28,000 --> 00:02:40,000
And I was intent upon making that sound go, I got such a headache because of that.

11
00:02:40,000 --> 00:02:49,000
The real answer is to put it bluntly, do sound meditation. Look at it a different way. Stop looking at it as getting in the way of your practice.

12
00:02:49,000 --> 00:03:00,000
I think if you've been following you already know that. Sound meditation is a valid meditation object.

13
00:03:00,000 --> 00:03:15,000
Obviously the problem is that we have preconceived notions of what meditation should be. It should be some spiritual thing that you start floating on cloud nine or whatever cloud it is.

14
00:03:15,000 --> 00:03:25,000
And you see angels and butterflies and music tinkling in the background.

15
00:03:25,000 --> 00:03:31,000
And so you tend to blame people. You see don't you understand I'm trying to meditate? No.

16
00:03:31,000 --> 00:03:41,000
Which means you're trying to get up to cloud nine or at least cloud A's and where the butterflies, where the tinkling and the water falls and so on.

17
00:03:41,000 --> 00:03:52,000
And people even put on this nature sounds in the background because I think that's meditation. That's not meditation. That's a delusion. It's pretending.

18
00:03:52,000 --> 00:04:02,000
It's like putting on a show for yourself. It's not reality. Reality is seeing hearing, smelling, tasting, feeling and thinking.

19
00:04:02,000 --> 00:04:20,000
The noise is as much a valid object of meditation as the stomach or the breath or the Buddha or anything. In fact, at that moment it's far more valid.

20
00:04:20,000 --> 00:04:24,000
It's what is there. It's what is occurring.

21
00:04:24,000 --> 00:04:32,000
And so it really you have to be clear in your mind and we all have to be clear and we have to make it clear to people what we mean by meditation.

22
00:04:32,000 --> 00:04:38,000
This is how I started that booklet and I tried probably didn't do the best job but tried to make it clear.

23
00:04:38,000 --> 00:04:43,000
When we talk about meditation we're not talking about a drug. We're talking about medicine.

24
00:04:43,000 --> 00:05:02,000
So medicine can be bitter. It can be unpleasant but it cures you. Drugs tend to be pleasant and sweet but have generally no purpose besides to avoid the problem and often have harmful side effects as well.

25
00:05:02,000 --> 00:05:12,000
So it's amazing how simple that is. Because once you take the sound as a meditation, why would it bother you?

26
00:05:12,000 --> 00:05:17,000
It's like then you'll be sad when it goes away and like, oh, now what do I? Now what do I acknowledge?

27
00:05:17,000 --> 00:05:25,000
I actually focus on the sound as your main object. Do that for a while. Forget about the breath. Don't say, oh, I have to get back to the stomach.

28
00:05:25,000 --> 00:05:34,000
When is this sound going to go away? Take the sound as your main meditation object and have fun with it.

29
00:05:34,000 --> 00:05:45,000
You can become enlightened with sound. Another anecdote. I was watching my teacher. I'm not sure if I'm allowed to say this but let's say I was watching some teacher.

30
00:05:45,000 --> 00:05:58,000
Then I'm not saying something about someone. And he was explaining the six senses as he explains to every meditation. He'll explain it ten times a day.

31
00:05:58,000 --> 00:06:13,000
That's explain the same thing. Repeat the same thing over and over again. But at this moment, he said, okay, so the four postures you have to be mindful standing, walking, sitting and lying down, bending, stretching and so on. And also the six senses.

32
00:06:13,000 --> 00:06:35,000
So seeing, when you're seeing hearing, smelling, tasting, feeling, thinking. When you see something, you have to, don't just stare at it. Say to yourself, seeing, seeing. When you hear something, don't just listen to it. Say to yourself, hearing, hearing.

33
00:06:35,000 --> 00:06:47,000
And he entered into, who knows what, in the middle of an explanation. Because he started saying to himself, hearing.

34
00:06:47,000 --> 00:07:00,000
It was the most amazing thing I'd ever seen. Just sitting there watching. It wasn't a show. He doesn't have shows. He wasn't putting on any errors.

35
00:07:00,000 --> 00:07:14,000
And so we just sat there in the yogis sitting there like this. Kind of like, let's go. And the rest of us are just in a bit of amazement and admiration, of course.

36
00:07:14,000 --> 00:07:27,000
And then came back. Go practice. So totally forgotten what was going on in a total of them. Go practice.

37
00:07:27,000 --> 00:07:41,000
So that was that. You can become enlightened through, through, you can enter into cessation, through the sound, through acknowledgement of the sound. You can become enlightened through listening to sound.

38
00:07:41,000 --> 00:07:48,000
It's a very, it's in perfectly valid meditation object. Because once you see it for what it is, your mind changes.

39
00:07:48,000 --> 00:07:58,000
It's all about developing habits. Once you develop the habit of seeing things clearly your mind, let's go. The mind, the clinging disappears. You have this clinging habit.

40
00:07:58,000 --> 00:08:06,000
And then we have the habit that we develop. Once your mind sees things clearly, let's go and there's release. There's freedom.

41
00:08:06,000 --> 00:08:13,000
Were that close to freedom? No. All we have to do is go from this to this. And then we're free.

42
00:08:13,000 --> 00:08:25,000
May I add an anecdote as well? I was meditating for some month in city monastery.

43
00:08:25,000 --> 00:08:42,000
And there was construction going on anywhere, everywhere. And it happened that it was right next to me. They were breaking down, building and building something new afterwards.

44
00:08:42,000 --> 00:08:52,000
And right behind the wall, right behind me, was a restaurant. And they played eight hours of Thai pop.

45
00:08:52,000 --> 00:09:02,000
So I had really, and then in front of me, was the kitchen. So I had really sound from everywhere, noise from everywhere.

46
00:09:02,000 --> 00:09:12,000
And I thought I go crazy. It was just too much in the beginning. And I thought of running away.

47
00:09:12,000 --> 00:09:29,000
And then I remember it, Pontissette, that just observed the sound, hear the sound, and see the reality and see it as it is, and see the anger arising.

48
00:09:29,000 --> 00:09:40,000
And I worked with it for months. And then there was, I think, one, the light festival.

49
00:09:40,000 --> 00:09:50,000
And I was sitting in meditation. I was not taking part. And I was, I think in determination also.

50
00:09:50,000 --> 00:10:02,000
And then the, the cutie next to me started to catch fire. And I heard somebody yelling fire fire.

51
00:10:02,000 --> 00:10:13,000
But it was so far away that I later noticed that it was actually a, a cutie next to me catching fire.

52
00:10:13,000 --> 00:10:28,000
So, but, but that sound, all, all, all the sounds around had become kind of so, I just say that.

53
00:10:28,000 --> 00:10:30,000
Didn't consequential.

54
00:10:30,000 --> 00:10:39,000
Yeah, not bothering anymore. So, and then when they started building a new cutie because the other was, was, has burned up.

55
00:10:39,000 --> 00:10:49,000
They had the chainsaw right underneath my window. And it didn't bother me at all anymore.

56
00:10:49,000 --> 00:10:55,000
So, it's, it's really doable when you stick with it and work with it.

57
00:10:55,000 --> 00:11:17,000
It really do. But it becomes a wonderful meditation.

