1
00:00:00,000 --> 00:00:07,840
Hi. So in this video, I will be explaining the technique of walking meditation. Walking

2
00:00:07,840 --> 00:00:13,960
meditation has many of the same benefits of sitting meditation. So in the same way as when

3
00:00:13,960 --> 00:00:18,800
we do the sitting meditation, we're going to try to keep the mind in the present moment

4
00:00:18,800 --> 00:00:25,120
and aware of the phenomena as they arise. To try to create a clear awareness of the reality

5
00:00:25,120 --> 00:00:31,040
around us. But the question comes, why then do we have to switch to walking meditation?

6
00:00:31,040 --> 00:00:35,680
Why is it not enough that we sit still and do sitting meditation? This is because walking

7
00:00:35,680 --> 00:00:41,360
meditation has several benefits which are not found in the sitting meditation. First

8
00:00:41,360 --> 00:00:48,880
of all, we're able to then walk long distances. We find that nowadays we are, because

9
00:00:48,880 --> 00:00:53,960
we sit so much, we're sitting in cars or we're sitting in an office or we're sitting in school

10
00:00:53,960 --> 00:01:04,360
or so on, we find that we're unable to undertake long journeys on foot. Whereas in ancient

11
00:01:04,360 --> 00:01:09,800
times, we would have been able to walk 20 or 30 kilometers a day even. Nowadays, we find that

12
00:01:09,800 --> 00:01:16,440
even the walk to the store is too much of a hassle. The second benefit from walking meditation

13
00:01:16,440 --> 00:01:21,240
is that it gives us patience and it gives us endurance because it is something which is

14
00:01:21,240 --> 00:01:28,600
done very slowly and is very repetitious. We find that it tests our patience and if we are

15
00:01:28,600 --> 00:01:34,280
practicing it on a daily basis, it can lift our patience in our endurance in any kind

16
00:01:34,280 --> 00:01:40,600
of work or activity that we have to do, which is perhaps unpleasant, even though it's

17
00:01:40,600 --> 00:01:44,440
something that we know we have to do, we might not wish to do it. Once we practice walking

18
00:01:44,440 --> 00:01:50,800
meditation, we can overcome this. The third is that it helps to overcome sicknesses which

19
00:01:50,800 --> 00:02:01,000
exist in the body. If we have any sort of sickness, a disease or even diseases which are

20
00:02:01,000 --> 00:02:07,040
otherwise uncurable. Sometimes people through the practice of meditation have cured cancer,

21
00:02:07,040 --> 00:02:14,240
have overcome many kinds of sicknesses in the body, simply through the practice of meditation.

22
00:02:14,240 --> 00:02:18,680
Because at the moment of walking meditation, both your mind is very focused and your body

23
00:02:18,680 --> 00:02:27,560
is very calm. Just with other martial arts or other methods of healing using the mind,

24
00:02:27,560 --> 00:02:32,840
walking meditation is something which as a byproduct can help to heal the body. The fourth

25
00:02:32,840 --> 00:02:38,240
benefit is the effect that it has in the digestive system. When we sit around all the time,

26
00:02:38,240 --> 00:02:43,480
when we are not doing anything with the body, we find that sometimes our body's ability

27
00:02:43,480 --> 00:02:49,240
to digest the food which we've eaten can be limited. When we do the walking meditation,

28
00:02:49,240 --> 00:02:54,840
especially because it's slow and repetitious, the body is given a chance to work through

29
00:02:54,840 --> 00:03:01,800
the food which we have eaten and digest it for the best benefit for our body. The fifth

30
00:03:01,800 --> 00:03:05,960
benefit, which is most important in the meditation practice, and the reason why we do the walking

31
00:03:05,960 --> 00:03:13,640
before we do the sitting, is that the concentration which comes from walking because it's dynamic,

32
00:03:14,520 --> 00:03:20,280
that it lasts into the sitting meditation. It lasts for a long time. We can find that if we just do

33
00:03:20,280 --> 00:03:25,720
sitting meditation, once we finish, our concentration dissipates very quickly. When we practice

34
00:03:25,720 --> 00:03:31,640
walking meditation, this is otherwise, and it continues on into the sitting meditation. So the

35
00:03:31,640 --> 00:03:36,760
concentration has much more strength. When we begin to do the sitting meditation after the walking,

36
00:03:36,760 --> 00:03:42,600
we find that we're already ready. We're already able to focus the mind very quickly on the

37
00:03:42,600 --> 00:03:48,280
phenomena that they arise. These are the reasons, some of the reasons, anyway, why we practice

38
00:03:48,280 --> 00:03:53,640
walking meditation as well as sitting meditation. Now I'd like to give a demonstration on how

39
00:03:53,640 --> 00:04:01,960
exactly we do the walking meditation. Walking meditation is performed with the feet close together.

40
00:04:01,960 --> 00:04:08,600
The hands right hand holding the left hand, either in front or in behind.

41
00:04:13,320 --> 00:04:18,520
The eyes are open and you're looking at the floor in front of you about two meters out.

42
00:04:18,520 --> 00:04:28,040
You're going to walk in a line. You can walk about three or four meters with your eyes

43
00:04:28,040 --> 00:04:32,600
looking at the floor in front, the ground in front of you, about two meters or six feet out.

44
00:04:35,240 --> 00:04:41,240
You start with the right foot and you move at one foot length with the heel just to be in line

45
00:04:41,240 --> 00:04:48,600
with the toes of the back foot and then the same with the left foot, one foot length each time.

46
00:04:50,920 --> 00:04:56,200
As you do this, you say to yourself, as we have been from the beginning of this series,

47
00:04:58,200 --> 00:05:05,160
you make a note of the motion as it occurs. So here we say to ourselves, step being right,

48
00:05:05,160 --> 00:05:18,200
step being left, step being right and so on, making the acknowledgement or the note making the

49
00:05:18,200 --> 00:05:37,640
noting at the moment of the movement, step being left, step being right, step being left,

50
00:05:38,840 --> 00:05:46,680
step being right, step being left until you reach the end of the walking path.

51
00:05:46,680 --> 00:05:53,160
Once you come to the end, we have to turn around, we're going to walk in the other direction.

52
00:05:53,160 --> 00:05:58,280
The method of turning around is to bring the back foot up to stand next to the front foot.

53
00:05:58,920 --> 00:06:04,760
As you do this, you say to yourself, stop being, stopping, stopping, just coming aware of the

54
00:06:04,760 --> 00:06:11,960
fact that you're stopping. Once you're standing still, say to yourself, standing, standing, standing.

55
00:06:11,960 --> 00:06:18,920
When you go to turn around, the method of turning is to lift the right foot off the floor

56
00:06:20,440 --> 00:06:30,520
and turn at 90 degrees, saying to yourself, one time, turning and then the left foot, the same

57
00:06:30,520 --> 00:06:41,640
turning, and one more time, turning, turning, and then you're facing the other direction.

58
00:06:43,560 --> 00:06:48,440
When you're standing in the other direction, you start all over, say to yourself, standing, standing,

59
00:06:48,440 --> 00:07:01,960
standing, and then step being right, step being left, step being right, step being left.

60
00:07:01,960 --> 00:07:07,320
And the most important part is that you're making the acknowledgement as the foot moves.

61
00:07:07,880 --> 00:07:10,360
So if you say to yourself, stepping right and then move,

62
00:07:10,360 --> 00:07:18,920
or you move first and then say, step being right, this is incorrect because you're not really

63
00:07:18,920 --> 00:07:24,840
aware of the action as it occurs. To make a correct acknowledgement and to bring about the clarity

64
00:07:24,840 --> 00:07:30,840
of mind which we're hoping for, you have to bring the, move the foot as you make the acknowledgement,

65
00:07:30,840 --> 00:07:39,720
step being right, step being left. And as you're walking, if something comes into your mind,

66
00:07:39,720 --> 00:07:47,800
say a thought or so on, or a feeling or some emotion arises, during that time, instead of continuing

67
00:07:47,800 --> 00:07:53,000
to walk and letting your mind get distracted, bring your back foot up and actually stop in the

68
00:07:53,000 --> 00:07:59,240
middle of your walking step, say to yourself, stop being, stop being, stopping, and then become

69
00:07:59,240 --> 00:08:05,080
aware of the thought or aware of the emotion and say to yourself, thinking, thinking,

70
00:08:05,080 --> 00:08:14,040
or angry, or sad, or bored, or happy, or if you feel pain, say pain and so on. And once it goes

71
00:08:14,040 --> 00:08:23,000
away, then continue on your walking, step being right, step being left, step being right,

72
00:08:23,560 --> 00:08:32,120
step being left and so on. And the technique is to walk back and forth, making the acknowledgement

73
00:08:32,120 --> 00:08:42,280
in your mind. So you come to the end and then bring the foot up again, stop being, stopping,

74
00:08:43,640 --> 00:09:00,200
standing, standing, standing, turning, turning, turning, turning, turning, and then standing,

75
00:09:00,200 --> 00:09:11,320
standing, and so on, to go back and forth for, say, 10 minutes. And here we try to balance the

76
00:09:11,320 --> 00:09:17,400
walking meditation with the sitting meditation. So if suppose you were to do 10 minutes of walking

77
00:09:17,400 --> 00:09:22,440
meditation, then we would also expect that you would do 10 minutes of sitting meditation.

78
00:09:22,440 --> 00:09:30,680
If you do 15 minutes of walking meditation, then also do 15 minutes of sitting meditation, and so on.

79
00:09:30,680 --> 00:09:55,320
Let's conclude the demonstration of how to practice walking meditation. Thank you for tuning in.

80
00:09:55,320 --> 00:10:05,320
I wish you all the best.

