1
00:00:00,000 --> 00:00:05,000
A technical question, what is the reason to do first walking meditation and then sitting?

2
00:00:05,000 --> 00:00:10,000
Is it okay to do the reverse if that's more convenient for someone?

3
00:00:10,000 --> 00:00:15,000
Seems to me it's harder to concentrate during walking and become distracted much easily.

4
00:00:15,000 --> 00:00:17,000
Because there is one more sense included.

5
00:00:17,000 --> 00:00:19,000
I've tried with closed eyes, but it doesn't work.

6
00:00:19,000 --> 00:00:20,000
You lose your balance.

7
00:00:20,000 --> 00:00:22,000
Yes, don't walk with closed eyes.

8
00:00:22,000 --> 00:00:24,000
You lose your balance.

9
00:00:24,000 --> 00:00:29,000
Something I should have told you before you tried it.

10
00:00:29,000 --> 00:00:42,000
The classic problem that you're proposing here, or the classic problem with your question that comes up time and again is your goal.

11
00:00:42,000 --> 00:00:44,000
Your goal is concentration.

12
00:00:44,000 --> 00:00:46,000
That's wrong.

13
00:00:46,000 --> 00:00:49,000
The goal should not be concentration.

14
00:00:49,000 --> 00:00:55,000
This is something that has to be repeated again and again, mostly because not only concentration,

15
00:00:55,000 --> 00:01:01,000
but happy feelings, pleasant feelings, calm feelings are what we're looking for in meditation,

16
00:01:01,000 --> 00:01:04,000
which is really a huge hindrance.

17
00:01:04,000 --> 00:01:07,000
It's an attachment, it's a desire.

18
00:01:07,000 --> 00:01:10,000
You want your experience to be other than what it is.

19
00:01:10,000 --> 00:01:16,000
This isn't directly answering your question, but it's pointing out a part of the problem that you're having.

20
00:01:16,000 --> 00:01:19,000
The walking, there's nothing wrong with the walking.

21
00:01:19,000 --> 00:01:22,000
What you're seeing in the walking is at the very least,

22
00:01:22,000 --> 00:01:27,000
impermanence, suffering, and non-self of the mind.

23
00:01:27,000 --> 00:01:29,000
You can't keep the mind.

24
00:01:29,000 --> 00:01:32,000
The mind is changing all the time.

25
00:01:32,000 --> 00:01:35,000
One moment you're with the foot and then boom, all of a sudden you're gone,

26
00:01:35,000 --> 00:01:39,000
seeming like the two minds had no relationship with each other.

27
00:01:39,000 --> 00:01:41,000
Suffering.

28
00:01:41,000 --> 00:01:45,000
It's not the way you want it to be.

29
00:01:45,000 --> 00:01:50,000
It's not according to your desire.

30
00:01:50,000 --> 00:01:52,000
And non-self, you can't control it.

31
00:01:52,000 --> 00:01:54,000
You can't make it be the way you want it to be.

32
00:01:54,000 --> 00:01:58,000
So you think, now I'm going to be mindful from here to the wall

33
00:01:58,000 --> 00:02:01,000
and two steps later, you've forgotten and you're gone.

34
00:02:01,000 --> 00:02:04,000
If you're lucky, if you're not lucky, you're going to spend the next

35
00:02:04,000 --> 00:02:07,000
from here to the wall forcing yourself to be mindful.

36
00:02:07,000 --> 00:02:09,000
And some people actually do that and they say,

37
00:02:09,000 --> 00:02:16,000
I was able to be mindful, concentrated from one end of the wall to one wall to the other.

38
00:02:16,000 --> 00:02:18,000
And they think that's some kind of accomplishment

39
00:02:18,000 --> 00:02:22,000
and really all they've accomplished is a headache.

40
00:02:22,000 --> 00:02:28,000
Our mindfulness and this distraction has to go away naturally.

41
00:02:28,000 --> 00:02:30,000
You can't repress it and force it away.

42
00:02:30,000 --> 00:02:33,000
You're just giving rise to more delusion,

43
00:02:33,000 --> 00:02:36,000
the idea that I can control the mind.

44
00:02:36,000 --> 00:02:46,000
The proper concentration has to come naturally through understanding.

45
00:02:46,000 --> 00:02:52,000
The word samadhi is best not translated as concentration.

46
00:02:52,000 --> 00:02:54,000
The word samadhi means focus.

47
00:02:54,000 --> 00:02:58,000
And it means focus because someone has to do with same.

48
00:02:58,000 --> 00:03:05,000
It's the same root as the word same, meaning level, balanced.

49
00:03:05,000 --> 00:03:08,000
The important thing is not to become concentrated,

50
00:03:08,000 --> 00:03:11,000
it's to balance concentration with effort.

51
00:03:11,000 --> 00:03:18,000
That's what walking does because sitting meditation is a great thing to do.

52
00:03:18,000 --> 00:03:22,000
But if done for an extended period of time, it can make you drowsy.

53
00:03:22,000 --> 00:03:25,000
At which point you should get up and do walking.

54
00:03:25,000 --> 00:03:28,000
Walking meditation is great for developing energy,

55
00:03:28,000 --> 00:03:31,000
but it can have the problem of making you distracted.

56
00:03:31,000 --> 00:03:38,000
So when you feel distracted, you might decide to sit down and do sitting instead.

57
00:03:38,000 --> 00:03:41,000
This is the good part of what you're seeing,

58
00:03:41,000 --> 00:03:44,000
is that in order to balance the faculties,

59
00:03:44,000 --> 00:03:46,000
this is why we do both of them.

60
00:03:46,000 --> 00:03:49,000
In sitting you can become lazy and drowsy

61
00:03:49,000 --> 00:04:00,000
and get caught up in the calm and the tranquility you can fall into a trance in a certain state.

62
00:04:00,000 --> 00:04:04,000
So getting up and walking can help you to break that up.

63
00:04:04,000 --> 00:04:08,000
In fact, it can help you to overcome this attachment to pleasant states,

64
00:04:08,000 --> 00:04:13,000
because it breaks it up and it forces you to be objective.

65
00:04:13,000 --> 00:04:16,000
We like sitting because we like lying even better,

66
00:04:16,000 --> 00:04:22,000
because you can enter into very peaceful states lying down even falling asleep.

67
00:04:22,000 --> 00:04:25,000
Why we do walking first?

68
00:04:25,000 --> 00:04:27,000
So that's why we do them both,

69
00:04:27,000 --> 00:04:29,000
but that's not really your question.

70
00:04:29,000 --> 00:04:32,000
Important to talk about why we do them both is for balancing.

71
00:04:32,000 --> 00:04:35,000
Why we do walking first?

72
00:04:35,000 --> 00:04:37,000
Not a hard and fast rule,

73
00:04:37,000 --> 00:04:40,000
but my understanding is it's based on the Buddhist teaching

74
00:04:40,000 --> 00:04:42,000
of what are the benefits of walking meditation.

75
00:04:42,000 --> 00:04:50,000
The fifth benefit of walking meditation is that the focus of mind lasts for a long time.

76
00:04:50,000 --> 00:04:54,000
Because it's dynamic, it's, according to the Buddha,

77
00:04:54,000 --> 00:05:03,000
it has the ability to last on into the sitting meditation.

78
00:05:03,000 --> 00:05:06,000
So what you'll find, not only that,

79
00:05:06,000 --> 00:05:12,000
but also it's a segue from ordinary life into a static position.

80
00:05:12,000 --> 00:05:18,000
If you go from dynamic working in the world and acting in the world,

81
00:05:18,000 --> 00:05:21,000
directly to sitting, you'll find it so extreme.

82
00:05:21,000 --> 00:05:24,000
That's why people find themselves nodding often,

83
00:05:24,000 --> 00:05:31,000
and drifting off or even getting lost in thought.

84
00:05:31,000 --> 00:05:33,000
The walking meditation is halfway between.

85
00:05:33,000 --> 00:05:35,000
It's not perfectly still.

86
00:05:35,000 --> 00:05:39,000
It's a way of easing your way into the sitting,

87
00:05:39,000 --> 00:05:43,000
but combined with the fact that the concentration or the focus that comes,

88
00:05:43,000 --> 00:05:48,000
the balance of mind that comes from the walking meditation,

89
00:05:48,000 --> 00:05:54,000
is said to last for a long time that it actually augments the sitting meditation.

90
00:05:54,000 --> 00:05:57,000
And this is verifiable. If you do it for some time,

91
00:05:57,000 --> 00:06:00,000
you should be able to see that by the time you get to do sitting,

92
00:06:00,000 --> 00:06:05,000
you feel charged. You feel like you've wound up or charged your batteries,

93
00:06:05,000 --> 00:06:08,000
and you're sitting posture.

94
00:06:08,000 --> 00:06:12,000
You're already ready to have a clear mind.

95
00:06:12,000 --> 00:06:15,000
You'll find that you should find that you have a clear mind,

96
00:06:15,000 --> 00:06:19,000
but it's not a hard and fast rule, and you're welcome to experiment.

97
00:06:19,000 --> 00:06:24,000
When you're living an ordinary life,

98
00:06:24,000 --> 00:06:26,000
sometimes you want to give up the walking entirely,

99
00:06:26,000 --> 00:06:30,000
because you're just walking all day or you're active all day,

100
00:06:30,000 --> 00:06:33,000
and it can be that you might just do the sitting meditation,

101
00:06:33,000 --> 00:06:43,000
so you don't have to keep it as a hard and fast rule, especially when your life dictates.

102
00:06:43,000 --> 00:06:47,000
It makes you unmeditative.

103
00:06:47,000 --> 00:06:50,000
The time when the Buddha said that this is the way we should do,

104
00:06:50,000 --> 00:06:55,000
we should practice walking and sitting walking and sitting is when we're doing intensive meditation,

105
00:06:55,000 --> 00:06:58,000
sleep, lie down only four hours out of 24 hours,

106
00:06:58,000 --> 00:07:01,000
then you've got 20 hours to do walking, sitting walking,

107
00:07:01,000 --> 00:07:05,000
sitting that's how the Buddha explained we should do meditation.

108
00:07:05,000 --> 00:07:11,000
Why we do it first, I think, is because of the benefits it has in sitting.

109
00:07:11,000 --> 00:07:14,000
Everything.

110
00:07:14,000 --> 00:07:17,000
In my meditation experience,

111
00:07:17,000 --> 00:07:22,000
I found that doing the walking before the sitting is,

112
00:07:22,000 --> 00:07:24,000
as you said, bringing in.

113
00:07:24,000 --> 00:07:29,000
Bringing the sitting to focus.

114
00:07:29,000 --> 00:07:31,000
I think meditation.

115
00:07:31,000 --> 00:07:35,000
Many beginners don't like the walking,

116
00:07:35,000 --> 00:07:39,000
and there will come a point in your practice where suddenly that changes.

117
00:07:39,000 --> 00:07:41,000
Suddenly, you hate the sitting,

118
00:07:41,000 --> 00:07:45,000
and the walking is actually nice because the sitting is too intense,

119
00:07:45,000 --> 00:07:48,000
and then it goes back and back and forth.

120
00:07:48,000 --> 00:07:51,000
But in the beginning, people tend to prefer the sitting.

121
00:07:51,000 --> 00:07:55,000
That changes not so long of a time,

122
00:07:55,000 --> 00:07:59,000
and it seems so absurd because people in the beginning,

123
00:07:59,000 --> 00:08:01,000
they'll start to develop a grudge for walking,

124
00:08:01,000 --> 00:08:05,000
and you have to spend all your time explaining to them the benefits of walking,

125
00:08:05,000 --> 00:08:10,000
and encourage them, well, try it, and no, no, we're not really worried about concentration.

126
00:08:10,000 --> 00:08:13,000
We're trying to understand and deceive the distraction,

127
00:08:13,000 --> 00:08:16,000
and to overcome it and to let go of it.

128
00:08:16,000 --> 00:08:19,000
And then eventually, boom,

129
00:08:19,000 --> 00:08:22,000
they'll come to you and say,

130
00:08:22,000 --> 00:08:25,000
no, funny thing.

131
00:08:25,000 --> 00:08:29,000
Today, I really like the walking and the sitting,

132
00:08:29,000 --> 00:08:31,000
and so they're kind of embarrassed,

133
00:08:31,000 --> 00:08:34,000
and they've totally had to give up this bias

134
00:08:34,000 --> 00:08:37,000
that they had because they started to believe,

135
00:08:37,000 --> 00:08:39,000
I don't know if this meditation is for me.

136
00:08:39,000 --> 00:08:42,000
I think walking meditation is some crazy thing

137
00:08:42,000 --> 00:08:46,000
that this monk invented himself or something.

138
00:08:46,000 --> 00:08:49,000
So that's worth keeping in mind in case you have this kind of doubt

139
00:08:49,000 --> 00:08:52,000
about walking meditation.

140
00:08:52,000 --> 00:08:54,000
That will change,

141
00:08:54,000 --> 00:08:57,000
and there will come this kind of humorous time

142
00:08:57,000 --> 00:08:59,000
where you realize that,

143
00:08:59,000 --> 00:09:02,000
or your mind changes in suddenly, like the walking better,

144
00:09:02,000 --> 00:09:05,000
you feel more charged and more alert and more awakened,

145
00:09:05,000 --> 00:09:08,000
sitting, you're just wasting your time or you're dealing with pain

146
00:09:08,000 --> 00:09:10,000
or so on or however.

147
00:09:10,000 --> 00:09:12,000
Eventually, of course, that all levels out

148
00:09:12,000 --> 00:09:36,000
and it all becomes meditation.

