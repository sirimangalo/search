1
00:00:00,000 --> 00:00:07,000
Hi, in this video, we will put into practice some of the things we learned in the last video.

2
00:00:07,000 --> 00:00:14,000
If you haven't seen that one yet, you should go watch it first, then come back and watch this one after.

3
00:00:14,000 --> 00:00:17,000
Okay, let's start.

4
00:00:17,000 --> 00:00:22,000
In this video, we're going to focus on very simple objects.

5
00:00:22,000 --> 00:00:29,000
A simple object will be easier for the mind to focus on, so we're going to use one of the simplest objects that exist.

6
00:00:29,000 --> 00:00:31,000
Colors.

7
00:00:31,000 --> 00:00:40,000
In this video, we're going to meditate on four different colors, blue, yellow, red, and white.

8
00:00:40,000 --> 00:00:45,000
We'll spend a few minutes on each color before moving on to the next.

9
00:00:45,000 --> 00:00:58,000
Just like in the first video, we'll start by looking at the color with our eyes open and just saying in our minds, blue, blue, yellow,

10
00:00:58,000 --> 00:01:10,000
yellow, red, red, or white, white.

11
00:01:10,000 --> 00:01:23,000
Continuously, until we can see the color in our mind, then we close our eyes and continue in the same way for as long as we can see the color clearly.

12
00:01:23,000 --> 00:01:32,000
When we can't see it in our mind anymore, we can open our eyes and look at the color again until it comes back.

13
00:01:32,000 --> 00:01:34,000
Ready?

14
00:01:34,000 --> 00:01:37,000
Okay, let's begin.

15
00:01:37,000 --> 00:01:39,000
Blue first.

16
00:01:39,000 --> 00:01:55,000
Say in your mind, blue, blue, blue, just like we did before.

17
00:04:39,000 --> 00:04:51,000
Now yellow.

18
00:04:51,000 --> 00:05:16,000
Say in your mind yellow.

19
00:05:21,000 --> 00:05:40,000
Say in your mind yellow.

20
00:05:40,000 --> 00:05:59,000
Say in your mind yellow.

21
00:05:59,000 --> 00:06:18,000
Say in your mind yellow.

22
00:06:18,000 --> 00:06:37,000
Say in your mind yellow.

23
00:06:37,000 --> 00:06:56,000
Say in your mind yellow.

24
00:06:56,000 --> 00:07:15,000
Say in your mind yellow.

25
00:07:15,000 --> 00:07:34,000
Say in your mind yellow.

26
00:07:34,000 --> 00:07:58,000
Say in your mind red.

27
00:07:58,000 --> 00:08:07,000
Say in your mind red.

28
00:08:28,000 --> 00:08:47,000
Say in your mind yellow.

29
00:08:47,000 --> 00:09:06,000
Say in your mind yellow.

30
00:09:06,000 --> 00:09:25,000
Say in your mind yellow.

31
00:09:25,000 --> 00:09:44,000
Say in your mind yellow.

32
00:09:55,000 --> 00:10:14,000
Say in your mind yellow.

33
00:10:25,000 --> 00:10:44,000
Say in your mind yellow.

34
00:10:55,000 --> 00:11:19,000
Say in your mind white white white blue.

35
00:11:19,000 --> 00:11:43,000
Say in your mind yellow.

36
00:11:43,000 --> 00:12:07,000
Say in your mind yellow.

37
00:12:07,000 --> 00:12:31,000
Say in your mind yellow.

38
00:12:31,000 --> 00:12:55,000
Say in your mind yellow.

39
00:12:55,000 --> 00:13:19,000
Say in your mind yellow.

40
00:13:19,000 --> 00:13:45,000
Say in your mind yellow.

41
00:13:45,000 --> 00:14:11,000
Say in your mind yellow.

42
00:14:11,000 --> 00:14:37,000
Say in your mind yellow.

43
00:14:37,000 --> 00:14:42,900
color and practice for as long as you like. Once you're good at this practice, check out

44
00:14:42,900 --> 00:14:49,900
our next video, where we'll meditate on love. Thanks for tuning in and see you next time.

