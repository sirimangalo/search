1
00:00:00,000 --> 00:00:05,120
Is there a balance to be struck between worldly pursuits and letting go for

2
00:00:05,120 --> 00:00:10,440
a layman? Should any kind of ambitions, career goals, preferred professions, be

3
00:00:10,440 --> 00:00:19,200
allowed? If any desire is considered false, what if the desire for Nirvana?

4
00:00:19,200 --> 00:00:27,960
There's another question on the moderator site. Our Google moderator site.

5
00:00:27,960 --> 00:00:34,760
It's even better than this one. It's kind of a unique formulation, although I've

6
00:00:34,760 --> 00:00:40,200
heard the question many years ago. The question is, I've started to

7
00:00:40,200 --> 00:00:43,200
practice something like I've started to practice meditation and I can feel

8
00:00:43,200 --> 00:00:49,200
myself losing my sexual desire. And I'm afraid that I'm not going to be able to

9
00:00:49,200 --> 00:00:55,720
perform. I'm afraid if I lose my lust I won't be able to be with my wife

10
00:00:55,720 --> 00:01:06,920
anymore. And how can I balance my practice, but still, basically, how can I

11
00:01:06,920 --> 00:01:12,400
not accidentally be sure I'm not going to accidentally lose something that's

12
00:01:12,400 --> 00:01:20,640
precious to me? It's important to me. It's not the same question as this one,

13
00:01:20,640 --> 00:01:31,680
but it's along the same lines. But you know, you're talking about two opposite

14
00:01:31,680 --> 00:01:43,240
things in one sense. And so I guess this is where the concern arises, because

15
00:01:43,240 --> 00:01:50,560
clearly any even superficial reading of the Buddha's teaching shows us that

16
00:01:50,560 --> 00:01:55,360
Buddhism doesn't consider Buddhism considers any craving, any clinging, any

17
00:01:55,360 --> 00:02:02,640
ambition whatsoever, any desire, basically, to be unwholesome. Depending what

18
00:02:02,640 --> 00:02:06,960
you mean on the word desire, and we get into big arguments about this, I got

19
00:02:06,960 --> 00:02:11,160
into an argument sometime back with people who tried to say that desire can be

20
00:02:11,160 --> 00:02:16,000
beneficial. So it's a matter of semantics. What exactly you mean by the word

21
00:02:16,000 --> 00:02:25,600
desire? But worldly pursuits, definitely, is quite, there's quite a clear

22
00:02:25,600 --> 00:02:31,360
implication in the Buddha's teaching that worldly pursuits are

23
00:02:31,360 --> 00:02:41,480
antithetical to the progress towards enlightenment. So how do you strike

24
00:02:41,480 --> 00:02:51,320
a balance? It's like striking a balance between drinking alcohol and

25
00:02:51,320 --> 00:02:57,560
sobriety. There are opposites. It's one or the other. If you want to do them

26
00:02:57,560 --> 00:03:02,520
both, then it's a tug of war. You're being pulled in both directions and you

27
00:03:02,520 --> 00:03:10,160
have to understand that. That's the first part of the answer. The second part

28
00:03:10,160 --> 00:03:17,160
is to understand that it doesn't, in reality, it doesn't work that way. There isn't

29
00:03:17,160 --> 00:03:23,240
a choice being made. Like, I'm going to practice this much, or I'm going to hold

30
00:03:23,240 --> 00:03:28,480
under these things, I'm going to let go of those. So you could accidentally make

31
00:03:28,480 --> 00:03:32,040
the wrong choice and whoops. You've let go of something, and that's a real shame

32
00:03:32,040 --> 00:03:38,960
because you still like it. It can't happen because as long as you don't, because

33
00:03:38,960 --> 00:03:48,720
what people don't realize is how profound and complete insight into reality is.

34
00:03:48,720 --> 00:03:53,760
It really is insight. It really is understanding. It's not a book. We think of it

35
00:03:53,760 --> 00:03:58,520
like our only example, only comparison is book learning. And so we think it's

36
00:03:58,520 --> 00:04:01,720
something like you look up in a book and you get it. You say, oh yeah, I agree

37
00:04:01,720 --> 00:04:08,920
with Socrates or Kant or Schopenhauer. I agree with these guys. And there

38
00:04:08,920 --> 00:04:15,480
for, I know it. That's my understanding. I understand this theory of reality or

39
00:04:15,480 --> 00:04:20,440
this philosophy. It's not like that. The understanding that you gain from

40
00:04:20,440 --> 00:04:26,760
meditation is real understanding of the truth. So it's not something to worry

41
00:04:26,760 --> 00:04:35,880
about. It's only going to come if it really and truly is 100% for benefit,

42
00:04:35,880 --> 00:04:44,760
for good, for it is only 100% a positive thing. So this fear of this irrational

43
00:04:44,760 --> 00:04:48,040
fear people have of Nirvana. You hear about Nirvana and you think, whoa, that

44
00:04:48,040 --> 00:04:53,120
sounds scary. I'm leaving the world behind. That sounds like I have to be

45
00:04:53,120 --> 00:05:01,120
careful. I don't fall into that. Nirvana is based on a perfect

46
00:05:01,120 --> 00:05:08,080
realization of the truth. That's all you have to know. If you can meet anyone

47
00:05:08,080 --> 00:05:15,360
who says they want to delude themselves or if you ask someone whether they

48
00:05:15,360 --> 00:05:20,160
want to delude themselves into thinking that that which is unpleasant, that

49
00:05:20,160 --> 00:05:25,840
which is unpleasant is actually pleasant. No one in their right mind I think

50
00:05:25,840 --> 00:05:32,320
would answer yes. That's something that is hurting you. Would you rather carry

51
00:05:32,320 --> 00:05:36,560
the belief that it is causing you bringing you happiness? Or would you like to

52
00:05:36,560 --> 00:05:40,360
know that it's hurting you when it is in fact hurting you? That's that's really

53
00:05:40,360 --> 00:05:43,280
the question we have to be asking because the truth that you're going to see

54
00:05:43,280 --> 00:05:49,320
is that those things which you think are bringing you pleasure or bringing you

55
00:05:49,320 --> 00:05:55,040
happiness are actually hurting you are actually cause without question, without

56
00:05:55,040 --> 00:06:04,400
doubt, without any qualifier, are bringing you unqualified suffering.

57
00:06:04,400 --> 00:06:11,320
That is what we mean here. So there was the where I heard this question was

58
00:06:11,320 --> 00:06:16,480
I heard it in Thai or this numpa chodo, this monk in Bangkok, one of the big

59
00:06:16,480 --> 00:06:25,280
we passed in the meditation teachers about 50, 30, 40 years ago. He had a Western

60
00:06:25,280 --> 00:06:29,320
person, I think it was a Western person come up to him and say to him, you know

61
00:06:29,320 --> 00:06:34,240
I'm really afraid I feel myself letting go and I really love my girlfriend so

62
00:06:34,240 --> 00:06:39,360
I'm afraid that I'm going to let go of something and I don't want to let go of

63
00:06:39,360 --> 00:06:44,840
my love for her. And it's not something you have to put his answer was you know

64
00:06:44,840 --> 00:06:48,200
don't worry about it. There's lots of defilements left for you don't have to

65
00:06:48,200 --> 00:06:54,600
worry about them all getting disappearing. His point was that it's really not

66
00:06:54,600 --> 00:07:00,960
that easy. You don't just one day wake up and have no or one day come out of

67
00:07:00,960 --> 00:07:07,640
a meditation session suddenly whoops you've got no more desires left. It's

68
00:07:07,640 --> 00:07:18,200
really a painful arduous process of realisation that forces you to rethink or see

69
00:07:18,200 --> 00:07:28,840
things in a new way. See things in a new way. See things differently. So

70
00:07:28,840 --> 00:07:34,960
definitely not something you have to worry about. Now it's actually not

71
00:07:34,960 --> 00:07:39,600
not exactly what you're not answering everything that you're asking here. So

72
00:07:39,600 --> 00:07:43,760
let's let's be more concise into the point based on what we've just talked

73
00:07:43,760 --> 00:07:48,000
about. So the balance I've talked about it's not really a balance it's a tug of

74
00:07:48,000 --> 00:07:55,280
war. You have to understand. But that being said I think the so the

75
00:07:55,280 --> 00:08:01,560
concise answer is to continue what you're doing and until you understand or have

76
00:08:01,560 --> 00:08:06,400
some reason to believe that these things are causing you suffering then

77
00:08:06,400 --> 00:08:15,960
don't worry so much about them. Focus more on self investigation or

78
00:08:15,960 --> 00:08:21,600
investigation of reality than on condemning or or denouncing certain

79
00:08:21,600 --> 00:08:27,960
activities. So all in due time once you see that those activities are causing

80
00:08:27,960 --> 00:08:36,000
you suffering then it is proper to abandon them. To abandon them first works

81
00:08:36,000 --> 00:08:44,960
in in certain instances. But in the deeper ones it will not in the longer and

82
00:08:44,960 --> 00:08:51,400
it'll come and hit you. It'll it'll it'll get what you call it. It'll come back

83
00:08:51,400 --> 00:08:59,960
to you because you're ignoring or you're you're you're smoothing over the

84
00:08:59,960 --> 00:09:06,240
fact overlooking the the fact that you don't really see those things just

85
00:09:06,240 --> 00:09:14,680
harmful. So if you just say I'm going to give up all sensuality and so

86
00:09:14,680 --> 00:09:18,920
until you just you just stop it and try to live your life like that. Unless

87
00:09:18,920 --> 00:09:23,120
you're unless you truly without really truly understanding that it's wrong

88
00:09:23,120 --> 00:09:31,640
for you. This is the wrong way to go about things. You might want to what you

89
00:09:31,640 --> 00:09:35,720
should do is put everything aside put aside your worldly pursuits and

90
00:09:35,720 --> 00:09:40,680
investigate them. You can't just put them aside and try to live your life

91
00:09:40,680 --> 00:09:47,640
without the proper investigation. That's what I'm saying. So that this is why

92
00:09:47,640 --> 00:09:52,400
the Buddha allowed all these things because he encouraged people to lay

93
00:09:52,400 --> 00:10:00,000
people anyway into self exploration. Now if you want to take up the sort of

94
00:10:00,000 --> 00:10:04,920
self exploration that would allow you to give these things up then you give

95
00:10:04,920 --> 00:10:09,080
them all up. But at the same time practice the type of intense meditation

96
00:10:09,080 --> 00:10:13,600
intensive meditation that will allow you to see that they're wrong. Simply

97
00:10:13,600 --> 00:10:18,200
for example becoming a monk and not practicing meditation but but giving up

98
00:10:18,200 --> 00:10:25,600
all sensuality and living your life as a perfect example of Buddhist

99
00:10:25,600 --> 00:10:35,480
morality. No worldly pursuits, no sensual indulgences, et cetera, et cetera.

100
00:10:35,480 --> 00:10:39,320
I mean you can go you can live in a cave not speak just to clear yourself from

101
00:10:39,320 --> 00:10:44,720
reality but without actually investigating the problem to give up the

102
00:10:44,720 --> 00:10:50,120
attachment. We'll in the end do nothing for you and we'll come back and bite

103
00:10:50,120 --> 00:10:55,160
you in the butt and you'll find that you go back into society. Whenever you go

104
00:10:55,160 --> 00:11:00,320
back into society the desires and the aversions are still they're in full

105
00:11:00,320 --> 00:11:08,000
force. And so this is why you find as we were talking about before you find

106
00:11:08,000 --> 00:11:12,200
many monastics who actually aren't able to give up sensuality they find

107
00:11:12,200 --> 00:11:20,120
themselves indulging in it and having big white screen televisions and so on

108
00:11:20,120 --> 00:11:27,840
listening to music, et cetera. Because they are living life but not they're

109
00:11:27,840 --> 00:11:34,360
walking the walk. No I don't know. The external form is good but their minds are

110
00:11:34,360 --> 00:11:42,200
not actually at the point where they can give up the desires and the

111
00:11:42,200 --> 00:11:49,760
attachments. So that's the second that sort of addresses the second question is

112
00:11:49,760 --> 00:11:55,920
not worry too much about your ambitions except to ask yourself the question

113
00:11:55,920 --> 00:11:59,840
that a Buddhist should are you sure that they're actually a benefit to you and

114
00:11:59,840 --> 00:12:03,600
once you have some sense that they're not to the extent that you have a sense

115
00:12:03,600 --> 00:12:06,960
that they're not beneficial to you to that extent you should give them up but

116
00:12:06,960 --> 00:12:13,800
not to just believe the Buddha while blindly and say okay well I'm going to

117
00:12:13,800 --> 00:12:17,440
give that up not because I have any sense whatsoever that it's wrong for me

118
00:12:17,440 --> 00:12:24,520
but because my teacher said that I should that's not really helpful not in the

119
00:12:24,520 --> 00:12:30,080
long term. In the short term can be helpful if you're going to in the short

120
00:12:30,080 --> 00:12:35,080
term it works but unless you then apply meditation that allows you to see it

121
00:12:35,080 --> 00:12:39,720
for yourself in the end it's going to come right back and and maybe even be

122
00:12:39,720 --> 00:12:45,760
stronger because you've just been repressing it. Okay final question desire

123
00:12:45,760 --> 00:12:51,860
for Nirvana which again comes down to semantics. I would say desire for

124
00:12:51,860 --> 00:12:58,280
Nirvana could be a very bad thing because it's escapism and not a very bad

125
00:12:58,280 --> 00:13:05,760
thing but it could be a problem because it's it points to a desire to to run

126
00:13:05,760 --> 00:13:11,000
away from things desire to for something not to be which is some kind of

127
00:13:11,000 --> 00:13:17,760
partiality. You don't need desire to meditate. Meditation is about

128
00:13:17,760 --> 00:13:26,280
understanding reality. In in the beginning it can be useful you see

129
00:13:26,280 --> 00:13:33,520
because just like like any of these things use you come to meditate and you

130
00:13:33,520 --> 00:13:38,640
have so very passionate about meditation so that leads you to meditate but

131
00:13:38,640 --> 00:13:42,920
eventually you realize that that passion itself is getting in the way. So in

132
00:13:42,920 --> 00:13:47,040
the beginning it can be a good thing because it leads you to only because it

133
00:13:47,040 --> 00:13:50,920
leads you to the meditation center and so on but it leads you for the wrong

134
00:13:50,920 --> 00:13:55,200
reasons with the wrong understanding a desire to run away. Eventually you

135
00:13:55,200 --> 00:13:59,240
realize oh you can't really that doesn't really work and your whole reason for

136
00:13:59,240 --> 00:14:04,280
coming to the meditation center ironically is is a bad reason or it's an

137
00:14:04,280 --> 00:14:09,040
unwholesome thing. You will see that and you'll see that you have to give that up

138
00:14:09,040 --> 00:14:14,800
as well and to the point that you have no desires whatsoever you're just

139
00:14:14,800 --> 00:14:19,960
able to see things clearly as they are. Really it's all about slowly slowly

140
00:14:19,960 --> 00:14:25,680
giving up all desires. Once you have no desires then you're happy and you're

141
00:14:25,680 --> 00:14:33,120
satisfied. But again it's semantic because you could say you could talk about a

142
00:14:33,120 --> 00:14:41,080
certain type of desire just meaning referring to intention or the decision to

143
00:14:41,080 --> 00:14:44,880
do something. That in our hands still seems to have but it's an functional

144
00:14:44,880 --> 00:14:53,920
decision. If there were any reason for it not to not being able to follow

145
00:14:53,920 --> 00:14:59,560
through with their desires they wouldn't have any feeling either way. It would

146
00:14:59,560 --> 00:15:05,320
just be a real another realization that they can't that they have to change

147
00:15:05,320 --> 00:15:09,800
their decision make a new decision. So it's not really desire. All desire is I

148
00:15:09,800 --> 00:15:16,800
would say.

