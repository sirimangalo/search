1
00:00:00,000 --> 00:00:04,760
Is there any way for us to better understand how good acts bring good results?

2
00:00:04,760 --> 00:00:08,760
It seems sometimes that people who do good don't reap a noticeable benefit.

3
00:00:08,760 --> 00:00:11,720
Is it subtle, immaterial, etc.

4
00:00:11,720 --> 00:00:15,640
The only way to understand karma is through the practice of meditation.

5
00:00:15,640 --> 00:00:20,000
The only way to appreciate it properly is through the practice of meditation.

6
00:00:20,000 --> 00:00:24,960
It's not some magical thing whereby you kill someone and you get killed or you give

7
00:00:24,960 --> 00:00:29,680
someone food and you get food or so on.

8
00:00:29,680 --> 00:00:34,720
I've talked about this before, the Buddha rejected the concept of karma.

9
00:00:34,720 --> 00:00:37,040
We have this idea that the Buddha taught the law of karma.

10
00:00:37,040 --> 00:00:41,640
The Buddha himself even said he taught the law of karma, but if you look at what really

11
00:00:41,640 --> 00:00:49,040
went on, what the Buddha really did is he rejected karma, totally, completely rejected

12
00:00:49,040 --> 00:00:51,040
karma.

13
00:00:51,040 --> 00:00:58,640
It's not fair to say that because he redefined it, but what I mean by that is Hinduism

14
00:00:58,640 --> 00:01:11,520
had this concept that an act was in and of itself good or bad and it still does.

15
00:01:11,520 --> 00:01:20,520
I believe that Hinduism, brahmanism, the Vedas have this idea that ritual activity

16
00:01:20,520 --> 00:01:24,640
brings benefit.

17
00:01:24,640 --> 00:01:30,040
There were even the beliefs that ritual activity was to be done without the idea that

18
00:01:30,040 --> 00:01:36,240
it brings benefit, but not doing it would be an unthought of.

19
00:01:36,240 --> 00:01:42,000
There was no idea that there's a benefit that comes from it, it is done, you do the rituals.

20
00:01:42,000 --> 00:01:48,080
So the question of why you do them, there's no why it's just done.

21
00:01:48,080 --> 00:01:54,600
So this was karma, karma was things to be done and things that were bad, killing someone

22
00:01:54,600 --> 00:02:00,640
was bad because of the physical act.

23
00:02:00,640 --> 00:02:05,160
Eating certain foods is in a bad karma, eating meat, for example, this idea that eating

24
00:02:05,160 --> 00:02:08,080
meat is bad karma.

25
00:02:08,080 --> 00:02:16,960
The Buddha rejected all this, he said, Jaitana hung Bikovei, kamamadami and something like

26
00:02:16,960 --> 00:02:27,720
that, that he said, you know, what really goes on, what is really done is not a bodily

27
00:02:27,720 --> 00:02:32,720
action, what is really done, the act that is done is the intention.

28
00:02:32,720 --> 00:02:37,200
And so he didn't reject it as he said, he rejected what we would normally think of as karma.

29
00:02:37,200 --> 00:02:43,080
He said, what is really done, the act that is really done is a mental one and that's

30
00:02:43,080 --> 00:02:47,320
where karma exists.

31
00:02:47,320 --> 00:02:57,120
So when we talk about the law of karma, Buddhism, we're not talking about acts of body,

32
00:02:57,120 --> 00:03:02,440
we're not talking about people doing good things and getting a good thing come to them.

33
00:03:02,440 --> 00:03:11,040
We're talking about the changes that occur in one's mind based on one's intentions.

34
00:03:11,040 --> 00:03:18,680
So when one gives rise to a unwholesome intention, when a mind state that is associated with

35
00:03:18,680 --> 00:03:25,240
greed or anger or delusion, then it will give rise to suffering.

36
00:03:25,240 --> 00:03:31,440
If one gives rise to a state of mind that is based on non-greed, non-angual, non-delusion

37
00:03:31,440 --> 00:03:39,920
wisdom, renunciation and kindness or love, then it brings happiness, it brings peace.

38
00:03:39,920 --> 00:03:44,800
Now the only way you can verify this is through looking at these days, through watching,

39
00:03:44,800 --> 00:03:46,880
but it's very quick actually.

40
00:03:46,880 --> 00:03:52,520
If you take a meditation course for three days or five days, five days, I would say even

41
00:03:52,520 --> 00:03:58,720
three days, three days of intensive meditation, you'll begin to see the workings of karma.

42
00:03:58,720 --> 00:04:06,840
You can enter into, if you're with a proper guidance, in about three days of insight meditation,

43
00:04:06,840 --> 00:04:19,400
you can understand what we call, the ability to see the causes and effect, knowledge

44
00:04:19,400 --> 00:04:25,480
of cause and effect, which is karma, the ability to see that when the mind gives rise

45
00:04:25,480 --> 00:04:29,320
to this day, this state has to follow.

46
00:04:29,320 --> 00:04:33,960
Once you start to look at greed, look at anger, look at delusion, you can't look at delusion,

47
00:04:33,960 --> 00:04:37,200
looking at greed and anger.

48
00:04:37,200 --> 00:04:43,440
You will come to see the suffering associated with them, that as soon as the greed arises,

49
00:04:43,440 --> 00:04:47,680
there's tension in the mind of anger, rises, there's suffering in the mind.

50
00:04:47,680 --> 00:04:53,920
These are associated states, and you see how it changes the mind, how it dulls the

51
00:04:53,920 --> 00:05:04,320
mind, how it reduces the clarity of the mind.

52
00:05:04,320 --> 00:05:10,960
And not only the mind, but it disrupts reality, so the whole world around you is affected.

53
00:05:10,960 --> 00:05:15,360
As you get more angry, you tend to act, and for example, people who are angry tend to

54
00:05:15,360 --> 00:05:25,160
be less careful, so people who trip and fall or hit their hands or cut themselves or

55
00:05:25,160 --> 00:05:30,640
so on, it can often be because of the karma of cultivating anger in the past.

56
00:05:30,640 --> 00:05:37,240
So we say hurting yourself is caused by karma, but on a basic level, it's just because

57
00:05:37,240 --> 00:05:47,280
you're less careful, a person who is greedy will be less indulgent.

58
00:05:47,280 --> 00:05:56,640
So a person who is stingy, for example, people who can't give up and can't let go, try

59
00:05:56,640 --> 00:06:05,760
to steal things from others or take things from others, will, as a result, be constantly

60
00:06:05,760 --> 00:06:14,600
unhappy, constantly, or dissatisfied, and as a result, you will see that will eventually

61
00:06:14,600 --> 00:06:29,520
become less accepting of peace and happiness, and will engage in stressful activities.

62
00:06:29,520 --> 00:06:41,000
People who are stingy will be less likely to find happiness themselves, generalization

63
00:06:41,000 --> 00:06:47,000
there are many different types of people, but they will be, as a result of their state

64
00:06:47,000 --> 00:06:54,480
of mind, will have less peace, obviously, because of their general sense, the desire

65
00:06:54,480 --> 00:06:58,080
in the mind will make them less peaceful, because they'll always be thinking about it.

66
00:06:58,080 --> 00:07:03,560
As soon as they sit down, their mind will jump away to the object of their desire, if

67
00:07:03,560 --> 00:07:10,600
it's another human being that you're in love with, you say, you obviously can't

68
00:07:10,600 --> 00:07:14,640
meditate, you're always thinking about this person, for example, if it's food, then you're

69
00:07:14,640 --> 00:07:19,200
always thinking about the food, if it's computer games, then you're always thinking about

70
00:07:19,200 --> 00:07:21,200
computer games and so on.

71
00:07:21,200 --> 00:07:25,040
I remember I used to play computer games in it.

72
00:07:25,040 --> 00:07:28,840
I tried meditating when I was younger, but I would always, I couldn't, I realized I would

73
00:07:28,840 --> 00:07:33,000
gotten to such a point where I couldn't even sit alone in the room, because I would

74
00:07:33,000 --> 00:07:39,040
be constantly thinking about the computer, going to the computer, playing computer games.

75
00:07:39,040 --> 00:07:41,840
Anyway, so yeah, meditate.

76
00:07:41,840 --> 00:07:58,640
Best way to understand karma, how good the act brings results is to practice meditation.

