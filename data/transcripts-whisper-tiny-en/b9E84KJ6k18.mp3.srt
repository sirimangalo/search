1
00:00:00,000 --> 00:00:09,120
What is meant when it said an arahant is free from kamma, they obviously have no bad karma

2
00:00:09,120 --> 00:00:13,960
leading to unwholesome states, but do they also lack good karma leading to wholesome

3
00:00:13,960 --> 00:00:19,560
states, or does being free from kamma mean arahant's attainment of nibana is not dependent

4
00:00:19,560 --> 00:00:24,360
on kamma.

5
00:00:24,360 --> 00:00:32,920
Both actually. The attainment of nibana is uncaused, no. The attainment of nibana is not

6
00:00:32,920 --> 00:00:40,440
nibana itself is uncaused. There's something tricky there that are my taya bidamatitra

7
00:00:40,440 --> 00:00:50,400
taught us, something about how the mind that realizes nibana is uncaused. And I may be totally

8
00:00:50,400 --> 00:00:54,880
because my taya wasn't very good at the time, so I may be totally misunderstood. That's

9
00:00:54,880 --> 00:00:59,400
why we need nadya to come here. There's a nun in Thailand. If anyone goes to Watanam

10
00:00:59,400 --> 00:01:12,000
telling her she has to fly to Sri Lanka and teach us all a bidamma.

11
00:01:12,000 --> 00:01:17,200
But that's not what it means. Being free from kamma really does mean that arahant's

12
00:01:17,200 --> 00:01:22,080
don't perform, an enlightened being doesn't perform any karma. Doesn't perform any

13
00:01:22,080 --> 00:01:27,200
volitional acts, which means they never do anything with thinking that may this lead

14
00:01:27,200 --> 00:01:34,440
to that. They never have any thought in their mind that this act should lead to this

15
00:01:34,440 --> 00:01:39,680
or that. There's none of that charge in what they do. They act appropriately. They act

16
00:01:39,680 --> 00:01:46,280
according to the situation. An arahant doesn't have any desire to create anything, doesn't

17
00:01:46,280 --> 00:01:52,840
have the desire to become a teacher, but when someone comes and asks them something they act

18
00:01:52,840 --> 00:02:01,280
appropriately and they teach. When the opportunity to give comes along, they will give.

19
00:02:01,280 --> 00:02:06,720
You could think of it as the path of least resistance, which in fact is in many cases

20
00:02:06,720 --> 00:02:13,000
to do very, very good deeds that seem to be very, very good, like giving and giving up

21
00:02:13,000 --> 00:02:19,920
and renouncing and so on. But they don't do it with any thought that it might lead to something.

22
00:02:19,920 --> 00:02:24,960
They do it with clear awareness and comprehension that that is the proper thing to do as

23
00:02:24,960 --> 00:02:31,080
we are talking about yesterday. That's a really good example of the meaning of some pajanya.

24
00:02:31,080 --> 00:02:38,040
Some pajanya in one sense can mean understanding without even thinking or intellectualizing,

25
00:02:38,040 --> 00:02:42,960
because intuitively understanding the appropriateness of an act, whether it's appropriate

26
00:02:42,960 --> 00:02:53,560
or not, acting in a perfectly natural way to every situation, never acting beyond what

27
00:02:53,560 --> 00:02:59,320
is appropriate. So there are no good deeds and there are no bad deeds. Remember what we mean

28
00:02:59,320 --> 00:03:04,640
by karma is not the deed itself. In this way I say the Buddha actually rejected the

29
00:03:04,640 --> 00:03:11,520
theory of karma. The word karma means action and the theory of karma is that actions

30
00:03:11,520 --> 00:03:20,360
have ethical consequences. The Buddha rejected this. He said an act doesn't have a physical

31
00:03:20,360 --> 00:03:26,160
act, doesn't have ethical consequences. It's the mental act, the mental volition. So

32
00:03:26,160 --> 00:03:31,240
an Arahan might perform the same acts that an ordinary person performs, but they do it

33
00:03:31,240 --> 00:03:35,400
without the mental volition. They don't have any intention that it might lead to this or lead

34
00:03:35,400 --> 00:03:42,240
to that. And so as a result, they do avoid evil deeds. They avoid those deeds, which would

35
00:03:42,240 --> 00:03:48,640
be based on anger, which require some kind of intention. May this happen, may this change

36
00:03:48,640 --> 00:03:59,720
of karma, may there be this kind of stress or friction or tension or so on. And an Arahan

37
00:03:59,720 --> 00:04:06,440
doesn't do these things. Naturally, naturally avoids evil and naturally undertakes because

38
00:04:06,440 --> 00:04:11,360
that's the point of goodness is something that leads to happiness and evil is something

39
00:04:11,360 --> 00:04:16,720
that leads to suffering. So they naturally avoid that which leads to suffering and naturally

40
00:04:16,720 --> 00:04:23,400
take up that because no one would ever create suffering if they knew it was causing suffering.

41
00:04:23,400 --> 00:04:28,040
It's because we don't understand. We're not clear that it's leading to our detriment.

42
00:04:28,040 --> 00:04:35,600
An Arahan does free from them. So no good karma, no bad karma, but to all external appearances,

43
00:04:35,600 --> 00:04:39,280
it seems like they're doing a lot of good karma, but you can see the difference. I mean,

44
00:04:39,280 --> 00:04:49,760
I don't know that I've ever met an Arahan, but if you look at people who are quite advanced,

45
00:04:49,760 --> 00:04:55,520
you can see that they're really detached from it. They're really free from any cares about

46
00:04:55,520 --> 00:05:04,880
what's going to happen in all circumstances. I mean, they're a supreme example of what

47
00:05:04,880 --> 00:05:11,360
we're meant to be, what we can achieve in that sense. And they're a wonder to behold really

48
00:05:11,360 --> 00:05:17,680
in their ability to be free from everything, both good and evil.

49
00:05:24,960 --> 00:05:28,800
Well, no, you have to see this is what I think he did. I think he has the right understanding.

50
00:05:28,800 --> 00:05:34,560
Bad karma, as we understand, a good karma, it's important to clarify that it's good karma, bad

51
00:05:34,560 --> 00:05:40,000
karma refers to the deeds. The problem is we use the words incorrectly. We say, oh, you've got a

52
00:05:40,000 --> 00:05:47,600
lot of bad karma, but you don't have bad karma. You have the results of bad karma. So the

53
00:05:47,600 --> 00:05:53,200
Arahan can still experience the results of past bad karma. That's important to say because we

54
00:05:53,200 --> 00:05:58,880
can get confused, but I don't think he is confused that way. Even in Thai, in Buddhist countries,

55
00:05:58,880 --> 00:06:05,120
they will talk about having karma. To this, you can't have karma. You do karma. Karma is the

56
00:06:05,120 --> 00:06:13,200
action. It depends on what you mean. If bad karma, you mean the action itself, then no,

57
00:06:13,200 --> 00:06:18,960
they don't have those. If by having bad karma, you mean having done bad karma that's going to lead

58
00:06:18,960 --> 00:06:25,920
them to suffer, for sure. They can angle them, angle them all as a good example. I've got a video

59
00:06:25,920 --> 00:06:30,400
who tried to publish this video with subtitles of this Thai movie of angle them all. It's a really

60
00:06:30,400 --> 00:06:40,720
good movie. Anyway, yeah, Maha Mohalana suffered a lot, but he didn't have any bad karma. He didn't

61
00:06:40,720 --> 00:06:56,640
do any bad karma.

