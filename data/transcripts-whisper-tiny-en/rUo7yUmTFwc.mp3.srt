1
00:00:00,000 --> 00:00:24,240
Okay, good evening, welcome to the second, okay, good evening broadcasting live from

2
00:00:24,240 --> 00:00:35,760
Stony Creek, Ontario, August 5th. Today's rather long quote is actually a story

3
00:00:35,760 --> 00:00:45,840
that the Buddha told. Funny thing about this story is it's not clear where it came from,

4
00:00:45,840 --> 00:01:00,800
it seems to have been passed down and used by many teachers from different religions,

5
00:01:00,800 --> 00:01:06,560
or from different parts of India, different teachers have, this is a popped up in different places,

6
00:01:07,440 --> 00:01:11,920
so it may not have been the Buddha who originally said it. It's an apt

7
00:01:11,920 --> 00:01:22,960
allegory, I think it's what you call it. It's like he saw an ace ops favor of the

8
00:01:22,960 --> 00:01:28,240
blind men in the elephant. In fact, maybe it is found in ace ops as well, I'm not sure. But you

9
00:01:28,240 --> 00:01:35,040
can look it up on Wikipedia, there's interesting background about it. Anyway, it relates to Buddhism,

10
00:01:35,040 --> 00:01:51,040
quite apt. We liken the situation that we find in the religious world to the idea of a blind

11
00:01:51,040 --> 00:02:01,600
men in the elephant, blindness and the elephant. The interesting thing, there's two interesting

12
00:02:01,600 --> 00:02:13,120
points in this story. The first is the blindness. How blindness leads to thinking something is

13
00:02:13,680 --> 00:02:23,840
other than what it is. Blindness is what, of course, it creates confusion. But the other part of

14
00:02:23,840 --> 00:02:32,720
the story is the idea of them getting angry at each other. That's not a physical blindness

15
00:02:32,720 --> 00:02:38,080
doesn't necessarily make people angry individuals, doesn't make people fight. But why this

16
00:02:38,080 --> 00:02:47,680
allegory is actually more apt is because blindness and Buddhism, the blindness of

17
00:02:47,680 --> 00:02:56,640
defilements, the blindness of ignorance, blindness of true, wrong view and of Egypt is actually what

18
00:02:56,640 --> 00:03:02,320
leads to getting angry. Just as these men get frustrated because they all have a different

19
00:03:02,320 --> 00:03:18,560
idea of things. It's very much an interesting point that our blindness leads us to desire and

20
00:03:18,560 --> 00:03:26,480
to aversion, leads us to fight with each other simply because we're blind. Because we don't think

21
00:03:26,480 --> 00:03:36,960
you don't see things. If you think about it, as one way of explaining this concept and Buddhism

22
00:03:36,960 --> 00:03:45,920
or the idea of the importance of wisdom and Buddhism is if they all saw the elephant clearly,

23
00:03:45,920 --> 00:03:53,440
there would be no reason to fight. Likewise, if all of us understood things exactly as they were,

24
00:03:53,440 --> 00:04:01,040
there would be no or we would lose this reason to fight. Our anger would disappear. It's a way of

25
00:04:01,040 --> 00:04:10,640
understanding the dependence of anger on ignorance. Because when you understand things, when you see

26
00:04:10,640 --> 00:04:24,560
things clearly, you have no reason to fight with anyone. All of our potential quarrels, potential

27
00:04:24,560 --> 00:04:33,200
conflict disappears. If someone says, you're a buffalo, you're not a buffalo. And furthermore,

28
00:04:33,200 --> 00:04:42,560
you know why they're saying they are a buffalo. If someone tries to manipulate you or to provoke

29
00:04:43,840 --> 00:04:48,800
an angry response in you, you know, right away that this person is provoking an angry response.

30
00:04:49,360 --> 00:04:55,600
More importantly, you know, the danger of getting angry back, you know what it's going to lead to,

31
00:04:55,600 --> 00:05:02,240
you see, the more knowledge and understanding that we have, not necessarily knowledge,

32
00:05:02,240 --> 00:05:07,760
but true knowledge, like really knowing something which comes from, from empirical understanding,

33
00:05:07,760 --> 00:05:13,920
empirical observation. The less reason you have to get angry, the less reason you have to desire

34
00:05:13,920 --> 00:05:20,880
things. As you see, desiring in desire is actually making me more frustrated, making me more angry

35
00:05:20,880 --> 00:05:35,360
and so on. But this is something you find not just in, I mean, it's applicable to all

36
00:05:38,000 --> 00:05:43,040
on all levels. This is something you find in lay life, right? Why we fight is because we have

37
00:05:43,040 --> 00:05:49,680
all, we're all blind. Why we fight with our families, why we fight with our friends,

38
00:05:50,320 --> 00:05:54,080
why we have enemies in the first place is completely due to blindness.

39
00:05:56,320 --> 00:06:01,200
And just by seeing things clearly, this is the point of just by seeing things clearly,

40
00:06:01,200 --> 00:06:07,200
could we do away with this, the quarrels, the arguments that we have.

41
00:06:07,200 --> 00:06:13,440
But it's more prevalent in what he's talking about here,

42
00:06:17,840 --> 00:06:22,640
the wonders of other sex when religious circles, even more prevalent, right? People will kill each

43
00:06:22,640 --> 00:06:29,040
other because they believe in a different imaginary being. I believe in this image of this god.

44
00:06:29,760 --> 00:06:35,440
I mean, by imaginary, I really do mean that because you may argue that it exists,

45
00:06:35,440 --> 00:06:41,760
but even your argument that God exists is based on your imagination, even if he does it,

46
00:06:41,760 --> 00:06:48,320
even if he issue it does exist because you don't have anything beyond your own imagination to go

47
00:06:48,320 --> 00:06:55,200
by. This is why a lot of people give up things, a lot of theists and deists give up trying to

48
00:06:55,920 --> 00:07:00,960
understand God because they say, well, you know, whatever I think it's completely based on

49
00:07:00,960 --> 00:07:08,400
my own imagination, I can't look around and say, oh, God, there's a, you know, there's

50
00:07:08,400 --> 00:07:20,240
a, something that tells me what God disliked. So it's an imaginary. And based on our idea of what

51
00:07:20,240 --> 00:07:35,520
this being is, this entity is, we fight and we argue. So totally based on our, our lack of knowledge,

52
00:07:35,520 --> 00:07:41,280
our lack of understanding. You'll see people fighting over God, you'll see people fighting over

53
00:07:41,280 --> 00:07:47,040
beliefs and views. You see even see non-religious people fighting over their beliefs and views,

54
00:07:47,040 --> 00:07:54,880
fighting over, over, we fight over ideas about, you know, we were fighting over whether slavery was

55
00:07:54,880 --> 00:08:00,560
okay, whether women should have the right to vote, we're fighting over sexuality, we're fighting

56
00:08:00,560 --> 00:08:07,600
over abortions. And we, it's, it's very difficult for us to, to agree. It's very difficult for us

57
00:08:07,600 --> 00:08:12,880
to even ourselves know what is right. And so the best we get often is while I believe,

58
00:08:12,880 --> 00:08:20,400
and which is dangerous, you know, often our belief is, is based somewhat on wisdom, understanding

59
00:08:20,400 --> 00:08:32,000
and logic. But often it's emotional or arbitrary, I believe, can come to be a reason to believe,

60
00:08:32,000 --> 00:08:36,800
you know, it just came to me and well, you know, whatever that's what I believe, without actually

61
00:08:36,800 --> 00:08:39,360
thinking, well, it might be wrong and maybe I should change what I believe.

62
00:08:39,360 --> 00:08:47,360
The way we've always done it, it's what we've always believed, etc. Not a good way to do things.

63
00:08:48,400 --> 00:08:53,200
Blindness is very dangerous. You hurt yourself, you hurt other people, and you miss,

64
00:08:53,200 --> 00:09:00,720
you miss, you miss conceive things. You can see what is, you also conceive what is not there,

65
00:09:01,280 --> 00:09:08,160
in the case of God, for example, in many different ways. When you're blind, you might hear a sound

66
00:09:08,160 --> 00:09:11,840
and think it's a lion or something.

67
00:09:15,040 --> 00:09:20,320
Have you ever stepped on a rope and jumped because you thought it was a snake?

68
00:09:22,240 --> 00:09:26,000
You know what I think, you know what I mean? You know what is meant here.

69
00:09:28,560 --> 00:09:35,200
But you even find this within Buddhism. You'll find, we have a hard time agreeing with each other.

70
00:09:35,200 --> 00:09:41,440
And this is one reason why we like to go by the texts and like to have an orthodoxy.

71
00:09:43,200 --> 00:09:50,480
It's dangerous because orthodoxy can, can be wrong and can lead everyone in the wrong direction.

72
00:09:52,000 --> 00:09:58,640
But there's often a sense, people sometimes have a sense that orthodoxy in and of itself is wrong,

73
00:09:58,640 --> 00:10:03,440
and they don't like the idea of having one view, and that's much more dangerous.

74
00:10:03,440 --> 00:10:10,720
Because this is the difference between the truth and imagination. The truth is singular.

75
00:10:10,720 --> 00:10:19,360
There's only one reality. There's only one truth, but illusion is manifold.

76
00:10:21,520 --> 00:10:28,640
So either we either we all agree on the same lie, all believe the same lie,

77
00:10:28,640 --> 00:10:36,160
or we all believe the truth. Once you believe the truth, it's much easier than getting everyone to believe the same lie.

78
00:10:37,360 --> 00:10:40,000
And maybe not, actually.

79
00:10:41,440 --> 00:10:45,200
You can, you can get a lot of people to believe the same lie, but no, not really,

80
00:10:45,200 --> 00:10:49,200
because eventually people have different ideas and there's, that's where arguments break out.

81
00:10:49,200 --> 00:10:53,840
And if you focus, what I mean to say is if you focus on the truth, it's easier in the sense that there's only one truth.

82
00:10:53,840 --> 00:10:59,360
So once everyone's, if you get people looking at the truth, you don't have to worry about what's truth.

83
00:11:00,560 --> 00:11:05,840
This is why meditation is so adaptable. You just say, look, you know, you don't have to say,

84
00:11:05,840 --> 00:11:10,160
become Buddhist, wear these, these funny clothes or this funny hat or it's on,

85
00:11:11,520 --> 00:11:16,640
there's no specifics to it. It's very general and it works for everyone, because truth,

86
00:11:16,640 --> 00:11:22,720
because truth is singular. You don't have to believe. This is a key sign of what is true

87
00:11:22,720 --> 00:11:32,320
in a, it's not, it's not specific to any group. It's, and it's approachable by anyone.

88
00:11:33,440 --> 00:11:35,920
Truth is what you can find by yourself.

89
00:11:38,880 --> 00:11:41,840
Anyway, so some thoughts on this.

90
00:11:45,200 --> 00:11:47,520
I don't have much more to say anyone have any questions.

91
00:11:47,520 --> 00:11:53,840
Right. So our practice is getting to the truth. And it was thinking about something,

92
00:11:54,400 --> 00:12:01,600
the idea of the difference with this simile and reality is that these guys are stuck being

93
00:12:01,600 --> 00:12:06,000
blind. They're going to be blind who they're light their whole lives. We're not that way.

94
00:12:07,120 --> 00:12:11,120
But you think how impossible it is for a blind person to come to see.

95
00:12:11,120 --> 00:12:21,280
It's easier to become spiritually, and whatever the opposite of blind is,

96
00:12:21,280 --> 00:12:32,160
see, see, but at least it's possible. It's possible for someone who's blind to come to see.

97
00:12:33,440 --> 00:12:38,960
And once it was blind, and now I see this is, this is how religious people describe it.

98
00:12:38,960 --> 00:12:44,000
And they come to experience something that they hadn't experienced before.

99
00:12:47,840 --> 00:12:54,720
But it's a very apt comparison. Practically speaking, you experience this.

100
00:12:55,440 --> 00:13:02,240
You start to do meditation and you just realize you don't go anywhere or become anything.

101
00:13:02,800 --> 00:13:08,720
You just realize how amazing it is that a person, no, I'm the same person as I was

102
00:13:08,720 --> 00:13:15,040
before. But the difference between me when I was blind, and me when I can see, it's just like

103
00:13:15,040 --> 00:13:18,960
having your eyes closed versus having your eyes always a total difference.

104
00:13:20,800 --> 00:13:28,800
How important sight is, it's even more profound, the difference between a non-meditator and a

105
00:13:28,800 --> 00:13:34,160
meditator or someone who doesn't understand them, there's themselves, doesn't understand the

106
00:13:34,160 --> 00:13:39,760
workings of the life, and one who has come to understand the workings of the life.

107
00:13:46,400 --> 00:13:47,440
Okay, some questions.

108
00:13:50,240 --> 00:13:54,240
In Woodway does not eating past noon, aid one in their practice, and in their meditation

109
00:13:54,240 --> 00:13:58,960
specifically. Well, there's nothing about afternoon. A lot of the rules are just

110
00:13:58,960 --> 00:14:07,280
convention. It's like a framework. Why does a car have to be this shape? Why can't it be a truck?

111
00:14:10,480 --> 00:14:20,800
But the only eating once is much more important because it's the bare minimum.

112
00:14:20,800 --> 00:14:30,480
It's the renunciation of any kind of attachment in the body. So the idea is you need

113
00:14:31,760 --> 00:14:37,200
just this much food per day, and you do need to eat. So eating once a day

114
00:14:39,920 --> 00:14:47,360
is enough, and any more than that, because eating more than that or eating in a more complicated

115
00:14:47,360 --> 00:14:55,680
fashion would most likely give rise to further attachment, craving, desire, etc. This is a preferable

116
00:14:55,680 --> 00:15:03,760
way. Eating once a day has quite effective in curing you a lot of attachment to the body.

117
00:15:06,560 --> 00:15:12,640
And they say it. I mean, if you've ever been on a meditation retreat, where they have milk in

118
00:15:12,640 --> 00:15:19,120
the evening, or where you eat in the evening, it's very difficult to meditate when you

119
00:15:19,920 --> 00:15:21,280
start to feel drowsy.

120
00:15:27,040 --> 00:15:31,200
To access an absorption concentration as they are understood in terms of the genus,

121
00:15:31,920 --> 00:15:39,520
develop during the practice of vivasana. Again, not during the practice of vivasana, but during

122
00:15:39,520 --> 00:15:43,280
the time when you think that you're practicing vivasana, you could actually be practicing

123
00:15:43,280 --> 00:15:48,800
samata, and they could also arise based on past practice of samata, either in this life or

124
00:15:48,800 --> 00:15:55,680
in the past life. They can rise spontaneously, but easily. You can step into them easily,

125
00:15:56,240 --> 00:15:57,920
depending on your past practices.

126
00:15:57,920 --> 00:16:10,960
Walking meditation briskly to help with sleepiness, sloth, torpor. They had works. I did

127
00:16:10,960 --> 00:16:20,320
it once. I've got a story for this one. When I was in John Tang Thailand, there was a big monk,

128
00:16:20,320 --> 00:16:31,200
this big, fairly fat, but also just big monk, who was a forest monk. He would walk barefoot,

129
00:16:31,200 --> 00:16:37,760
wore these thick robes that I'm wearing right now, but in Thailand, you know, and had a vest

130
00:16:37,760 --> 00:16:43,280
full of buddhas. He had like a hundred pockets in each one had a buddha in it, and at his waist,

131
00:16:43,280 --> 00:16:49,200
he had his mother's ashes in the... I mean, this was... this guy was hardcore. Still one of my

132
00:16:49,200 --> 00:16:57,120
favorite people. Like a character. Like just one of those people who was out of a book, who

133
00:16:57,120 --> 00:17:01,840
you think, you know, he's not just any old monk. That's him, and he's iconic.

134
00:17:06,080 --> 00:17:11,200
And then there was this other monk. Oh, I've got stories to tell. This other monk,

135
00:17:11,200 --> 00:17:20,560
who was actually a new monk, whose parents were part of the Thai mafia, and he... I just

136
00:17:20,560 --> 00:17:26,960
had a totally unrelated story, but he at one point was showing me pictures of how he went

137
00:17:26,960 --> 00:17:35,520
he disrobed, because it was just a temporary ordination. He was going to start growing opium, I

138
00:17:35,520 --> 00:17:40,160
think, or cocaine. I can't remember. The other one he had pictures, and he was pointing me out,

139
00:17:40,160 --> 00:17:46,560
who was who, like, this guy was a... this guy was the gun for the gun, or the guy who kills people,

140
00:17:47,120 --> 00:17:52,960
and this guy is the... whatever the head of the family, and so... anyway. So the three of us,

141
00:17:55,600 --> 00:18:01,940
it's like a joke. He told... the other monk told me, hey, this

142
00:18:01,940 --> 00:18:11,060
a gen Yud was... every night he goes walking up in the monastery, and I went with him last night,

143
00:18:11,060 --> 00:18:17,780
and I was like, oh, that's interesting. Well, I'll come and see. And so that night, I went up with them,

144
00:18:18,580 --> 00:18:23,860
and up in the front of the monastery, they put sand out, the lay people when they come to the

145
00:18:23,860 --> 00:18:31,380
monastery, and it's a tradition to put sand out. The theory is, and it's an ancient, an old tradition,

146
00:18:32,980 --> 00:18:40,420
that when you walk to the monastery, when you leave the monastery, you carry mud out,

147
00:18:41,620 --> 00:18:49,780
and so with all these people coming and going, the monastery actually loses something. I mean,

148
00:18:49,780 --> 00:18:56,020
the... as I've heard it, the idea is, you're stealing from the monastery, but I think it could

149
00:18:56,020 --> 00:19:04,020
possibly just be... with lots of people coming, and it gets run down against the area, and the

150
00:19:04,020 --> 00:19:09,300
olden days, the area and the monastery would get, and have this problem. So at the front of the

151
00:19:09,300 --> 00:19:17,140
monastery, they have the sandy area, or a dirt area, and lay people every year who will bring sand,

152
00:19:17,140 --> 00:19:23,220
or sometimes I think maybe every... every special day. So there's just... in that time,

153
00:19:23,220 --> 00:19:27,220
there was a big area, and now they've closed it in a bit, and there's a big area of sand,

154
00:19:28,100 --> 00:19:35,620
probably 50 meters, something, or I don't know, distance wise. Yeah, maybe 50 meters.

155
00:19:37,860 --> 00:19:44,980
And so this is what we did, we walked. Quickly, 50 meters there and back there and back,

156
00:19:44,980 --> 00:19:51,860
and we did this from like nine to three, or nine to two, maybe. I think we took a break,

157
00:19:51,860 --> 00:19:58,260
but then nine p.m. or ten p.m. to two, or so they... there's probably three or three or four hours,

158
00:19:58,980 --> 00:20:03,460
if I can, or I think about three or four hours. And it felt great. It was awesome.

159
00:20:03,460 --> 00:20:08,740
It felt energetic, didn't need to sleep, it did end up going to sleep, I think, around two,

160
00:20:08,740 --> 00:20:15,460
and I was excited about it. I thought, this is great. And I went to see our gentong.

161
00:20:18,260 --> 00:20:24,020
And I told them about it. I said, yeah, at night we're going walking. Many looked at me.

162
00:20:27,700 --> 00:20:34,420
And he said, I think he laughed about it, but he said, that's... that's the foreign way,

163
00:20:34,420 --> 00:20:39,940
the Western way. Ex... he used the word exercise. He said,

164
00:20:39,940 --> 00:20:43,940
I'm bad for long. We have exercise and he has the English word.

165
00:20:48,500 --> 00:20:53,860
And then he got... he got rather serious and he said, the... the walking meditation in Buddhism,

166
00:20:54,660 --> 00:20:58,900
the Buddha's way of walking meditation, you have to go slowly. You have to go very slowly.

167
00:20:58,900 --> 00:21:06,500
You can't walk like that. The point is to understand... the point is actually not walking.

168
00:21:07,140 --> 00:21:15,300
It's to... to experience this clearly. And then this clearly. And then this clearly.

169
00:21:16,260 --> 00:21:22,660
This is what is about one thing, one movement. So our practice of walking meditation isn't about

170
00:21:22,660 --> 00:21:29,460
walking at all. I've done it with people with kids in a room. Too many kids in the room. I didn't

171
00:21:29,460 --> 00:21:33,860
want them to have to walk around. And I just had them lift their feet and put them down.

172
00:21:36,100 --> 00:21:42,900
Same thing. There's nothing to do with walking. We're not walking. We're experiencing motion

173
00:21:43,700 --> 00:21:49,060
clearly. And so if you don't do it slowly, there's nothing like meditation. Obviously what we were

174
00:21:49,060 --> 00:21:54,500
doing was even worse. We were chatting. We were talking all night. So absolutely not meditation.

175
00:21:54,500 --> 00:22:01,780
But you know, it's easy to get these ideas. So good walking briskly, be helpful. Sure, it's

176
00:22:01,780 --> 00:22:06,900
probably helpful for the body to some extent. But it's not conducive to meditation. Not in a way

177
00:22:06,900 --> 00:22:20,820
that we're looking at meditation. So we've got stack exchange questions.

178
00:22:26,660 --> 00:22:28,420
You've got a bunch of answers already.

179
00:22:28,420 --> 00:22:39,380
I've been dealing with negative mind states in my practice. When it gets overwhelming,

180
00:22:39,380 --> 00:22:44,180
I lose my sense of mindfulness and get lost in the middle of all the noise. And I guess I identify

181
00:22:44,180 --> 00:22:49,940
with it. Otherwise, I wouldn't feel pain when it happens. I mean, physical pain or mental pain.

182
00:22:49,940 --> 00:22:59,940
Is it a good idea to bring myself back to the breath? Or should I face this? I usually try to

183
00:22:59,940 --> 00:23:05,460
accept the experiences, experience in the moment, but it usually brings more pain because I can't

184
00:23:05,460 --> 00:23:28,900
see clearly what I'm going through. Yeah, it can be a good idea to bring yourself back. These

185
00:23:28,900 --> 00:23:36,900
kind of questions, there's no one answer. Meditation is a lot like boxing or kung fu or something.

186
00:23:36,900 --> 00:23:45,220
It's a lot about improvising. And not one answer. Not one answer is going to solve the problem.

187
00:23:47,380 --> 00:23:56,100
Not one answer is going to work all the time. So get that idea clear. Don't pick one solution.

188
00:23:56,100 --> 00:24:03,140
Go back to the breath, watch the stomach rising and falling, and then attack it again.

189
00:24:05,620 --> 00:24:10,820
Upside, upset, overwhelmed, overwhelmed, just remind yourself. And then it just becomes too

190
00:24:10,820 --> 00:24:18,020
much. And then you can try something else. One way of doing the kung fu thing is lie down instead.

191
00:24:18,580 --> 00:24:24,500
And then you feel stressed. So lie down. Do it lying meditation on your back rising.

192
00:24:24,500 --> 00:24:29,460
You might fall asleep. And when you wake up, you're able to do, you have a clear mind and you're

193
00:24:29,460 --> 00:24:34,900
able to do meditation. Or you might have a good meditation lying down. You might have just the

194
00:24:34,900 --> 00:24:44,500
perfect balance or get up and walk. Changing posture is important. Sometimes there's little

195
00:24:44,500 --> 00:24:53,940
things you can do like changing your diet, eating the right food, drinking lots of water.

196
00:24:53,940 --> 00:24:59,060
I mean, these are little tricks. I don't even bring them up because just to point out that there's

197
00:25:00,580 --> 00:25:06,420
it's like guerrilla warfare kind of, or like we're in a war. There's no blueprint for how it's

198
00:25:06,420 --> 00:25:12,100
going to go. It's chaotic. In the end, you're going to be, it's like being in a war. You're fighting

199
00:25:12,100 --> 00:25:18,580
for your life. Many people are in the state of such trauma and such immense mental pain that

200
00:25:18,580 --> 00:25:26,180
it's like being in war. And if you think of it as being a war, it's a lot easier. So a lot more

201
00:25:27,060 --> 00:25:33,780
dealable. You become a soldier. You start fighting. But you don't just run into battle or expect

202
00:25:33,780 --> 00:25:38,980
to be able to blow everything up with a nuclear bomb. You've got to be clever, or you're going to

203
00:25:38,980 --> 00:25:46,580
get overwhelmed. So take the techniques that you've learned, try to apply them, but don't expect

204
00:25:46,580 --> 00:25:51,940
it to be form a leg. It's going to be chaotic, and you're going to have to improvise.

205
00:25:54,420 --> 00:25:58,900
He said, we have to do this many minutes walking and sitting. I can't do some walking,

206
00:25:58,900 --> 00:26:04,100
nothing just a sitting. I can't do a sitting. We had one month. I don't know if I mentioned this. I

207
00:26:04,100 --> 00:26:11,220
said, he came to our teacher and he said, he was the worst trouble ever. He ended up setting himself

208
00:26:11,220 --> 00:26:18,500
on fire and disrupting in the end. He went totally crazy. I took, I spent, now I took him to help

209
00:26:18,500 --> 00:26:24,580
take him to the mental hospital. We got quite, we had quite a good relationship or familiar

210
00:26:24,580 --> 00:26:31,540
relationship with the mental hospital and changed my, not when I was a teacher. I never had a

211
00:26:31,540 --> 00:26:41,300
problem with my students. I've never had a student go crazy, but I've seen some craziness.

212
00:26:42,740 --> 00:26:49,780
Anyway, he came to the teacher or teacher once, and he said, Adjana can't sit. He sits, and he

213
00:26:49,780 --> 00:26:55,700
starts to stiffen up or something like that. Adjana said, well, then do walking. I said, I can't

214
00:26:55,700 --> 00:27:00,980
walk. Then do walking. There's some other reason I couldn't do. I said, well, then lie down. I'll know

215
00:27:00,980 --> 00:27:06,660
when I lie down. I suddenly find myself going, and Adjana said, well, then stand.

216
00:27:13,540 --> 00:27:20,980
And then they got him. As he said, oh, yeah, I can stand. So changing the posture is useful.

217
00:27:20,980 --> 00:27:30,980
A version and fear about being mindful about this, this is the, this is the, the cleverness.

218
00:27:30,980 --> 00:27:37,300
We monks that you need to be clever in your meditation. It's not a break that you're going to

219
00:27:37,300 --> 00:27:43,540
break with a hammer. A hammer doesn't work. So when you have, you have to be clever and ask yourself,

220
00:27:43,540 --> 00:27:49,140
what's the essence of this? In this case, you have a version and fear, focus on the aversion,

221
00:27:49,140 --> 00:27:54,020
fear, forget about the pain and whatever. Now you're afraid and you're, you have a version or

222
00:27:54,020 --> 00:28:00,260
something. Focus on that. Maybe you feel guilty because you're not mindful. Focus on the guilt,

223
00:28:00,260 --> 00:28:06,740
that feeling. Focus on whatever they're right then. That's the one formula that you can follow.

224
00:28:06,740 --> 00:28:11,780
The problem is that it's, it's tricky because it changes in the nature of the experience changes.

225
00:28:11,780 --> 00:28:16,420
So I'm getting yourself in a position where you can be mindful of it. That's the key.

226
00:28:16,420 --> 00:28:21,220
And you have to keep yourself. You have to keep pulling yourself back to this moment, this present

227
00:28:21,220 --> 00:28:25,220
moment. And your mind's going to keep slipping away and you have to just keep bringing it back.

228
00:28:25,940 --> 00:28:29,700
But there's no place of mindfulness. There's no plane, sorry, plane of mindfulness.

229
00:28:31,780 --> 00:28:36,420
You know, state of plane, plane, I think is about plane wrong.

230
00:28:37,300 --> 00:28:42,820
But you can't have a state of pure mindfulness. Your mindfulness is now, it's momentary.

231
00:28:42,820 --> 00:28:47,140
Once you've been mindful, that's gone. You only have the next moment, whether you can be mindful in

232
00:28:47,140 --> 00:28:48,100
that moment or not.

233
00:28:50,900 --> 00:28:58,740
Mindfulness is a moment by moment.

234
00:29:02,980 --> 00:29:09,140
The goal of meditation is to see reality of nature is uncontrollable and permanent and unsatisfactory.

235
00:29:09,140 --> 00:29:12,740
The goal of meditation is to banise freedom.

236
00:29:14,020 --> 00:29:26,660
Seeing and permanent suffering in non-self is the path or it's what the, yeah, it's the path.

237
00:29:31,380 --> 00:29:33,780
How about walking meditation? I'm going for arms then.

238
00:29:33,780 --> 00:29:38,980
Yeah, it's not meditation, but walking meditation, arms around shouldn't be meditated, but then

239
00:29:38,980 --> 00:29:43,140
it's just walking, walking, okay. It's not as powerful, not as effective.

240
00:29:44,340 --> 00:29:49,940
I mean, can you become enlightened running? Yes, sure. You can. Is it easy? No.

241
00:29:50,660 --> 00:29:56,980
It's very difficult to do it to become enlightened as a jogger. I mean, I suppose if you were jogging

242
00:29:56,980 --> 00:30:04,900
and you kept it up eventually, you could get into a point where it was kind of meditative.

243
00:30:04,900 --> 00:30:09,620
But still, it's nothing, nothing like what we're doing. There's one movement because that's very

244
00:30:09,620 --> 00:30:13,780
easy to be aware of the arising and ceasing of that movement.

245
00:30:13,780 --> 00:30:29,140
Experience. Can the mind be located at two places in the body at the same time?

246
00:30:29,140 --> 00:30:36,260
The mind has no location. The mind doesn't take up space. The mind is mental. It's not physical.

247
00:30:36,260 --> 00:30:44,820
Space and position is a derived quality of the matter. The mind is not material, so it doesn't

248
00:30:44,820 --> 00:30:56,260
take up space. The body doesn't exist. The space only exists as a derived quality of matter.

249
00:30:56,260 --> 00:31:05,380
It doesn't really, it's not truly real. All that's real is experience.

250
00:31:11,220 --> 00:31:17,220
This is great. It's nice having so many people come and having a platform where we can do it.

251
00:31:17,220 --> 00:31:20,900
So we have YouTube, people can watch, we have 32 viewers,

252
00:31:20,900 --> 00:31:25,220
and then we have our own site with little chat box.

253
00:31:25,940 --> 00:31:31,620
Now what I'm just looking at now is how to restart my dot-series mongolow.

254
00:31:32,580 --> 00:31:36,900
Does anyone remember my dot-series mongolow? It's been a while.

255
00:31:39,460 --> 00:31:41,940
I don't want to call it my dot-series mongolow. It's a bit

256
00:31:41,940 --> 00:31:50,980
crass, isn't it? Anybody got a better one? How about sangha dot-series mongolow?

257
00:31:52,020 --> 00:32:00,500
The idea is to make the most easy way to explain it is make a Facebook clone.

258
00:32:02,180 --> 00:32:08,340
But that's not exactly it. But it has the format like Facebook, and so that's why it's easy to

259
00:32:08,340 --> 00:32:15,380
explain it like that.

260
00:32:17,300 --> 00:32:22,340
Is that it wouldn't be a feed where people could post links and pictures and

261
00:32:26,660 --> 00:32:31,860
make a post, updates, make a form groups,

262
00:32:31,860 --> 00:32:37,060
I don't know events, maybe, I don't know,

263
00:32:38,340 --> 00:32:46,740
but it did well, no, it was useful. But there wasn't as much of a group than the

264
00:32:46,740 --> 00:32:48,980
reason I thought about bringing it back is because now we've got

265
00:32:49,860 --> 00:32:56,820
seems a little bit more of a group, and this chat is a little bit too ephemeral

266
00:32:56,820 --> 00:33:03,540
and simple, right? It's nice that it's nice and it's simplicity, but

267
00:33:07,300 --> 00:33:16,180
limited.

268
00:33:21,380 --> 00:33:25,540
Is it possible that we are reborn outside of earth and somewhere else in the universe?

269
00:33:25,540 --> 00:33:27,540
I don't know.

270
00:33:38,580 --> 00:33:43,220
I'm glad the YouTube live feed on to meditation. It's a little bit more complicated because

271
00:33:43,860 --> 00:33:49,620
the link changes each time, so I have to manually input the link each day. I'd have to find

272
00:33:49,620 --> 00:33:56,660
somewhere where I could some way to input the link. It's not a static link every day, so because

273
00:33:56,660 --> 00:34:02,900
each YouTube video has its own link to it. I mean, nice, if there was a static link,

274
00:34:05,780 --> 00:34:08,420
but there isn't.

275
00:34:13,220 --> 00:34:16,500
Yeah, I don't think I'd import the meditation group into that platform.

276
00:34:16,500 --> 00:34:21,620
Not at first anyway. I can see the benefit of that, but it is kind of

277
00:34:22,740 --> 00:34:26,340
there are two different beasts.

278
00:34:33,940 --> 00:34:36,340
Anyway, I want to just get it up first and see how it goes.

279
00:34:37,300 --> 00:34:39,940
So I'm thinking about sangha.ceremungalow.org.

280
00:34:40,900 --> 00:34:45,140
I know the word sangha is complicated because it's hard to spell, but

281
00:34:45,140 --> 00:34:49,780
it's the best word I can think of. sangha, that's what it would be.

282
00:34:54,980 --> 00:34:58,500
What do you all think? I think there's a delay.

283
00:35:00,260 --> 00:35:04,180
I think by the time you hear what I said, it's been a few seconds already. I'm going on to talking

284
00:35:04,180 --> 00:35:04,980
about something else.

285
00:35:04,980 --> 00:35:16,180
And Robin tells me that the YouTube video is not as best as it could be. It's unfortunate.

286
00:35:20,100 --> 00:35:23,540
We should investigate that. See whether it's every day or not.

287
00:35:25,140 --> 00:35:28,180
The recording, once it's recorded it, apparently,

288
00:35:28,180 --> 00:35:34,420
and the recording is not very well done or something.

289
00:35:41,300 --> 00:35:44,820
Yeah, so I'll just do that. I was headed up as sangha.ceremungalow.org.

290
00:35:45,780 --> 00:35:49,860
I can figure it out. It's already set up actually. If you go to sangha.ceremungalow.org,

291
00:35:50,580 --> 00:35:53,860
you'll see a blank page. And you know why you'll see a blank page?

292
00:35:53,860 --> 00:36:01,540
Because that blank page is our Facebook clone. But there's an error. And I don't know what

293
00:36:01,540 --> 00:36:07,700
that error is yet. So the next step is to figure why our Facebook clone is not loading.

294
00:36:15,940 --> 00:36:19,700
Now YouTube does have a 30-second delay. Whoa, that's pretty big.

295
00:36:19,700 --> 00:36:24,580
So if you want to, if you want real time, if you want to want to do this real time,

296
00:36:24,580 --> 00:36:32,100
better not to watch YouTube, better to listen, much more losses. That's surprising.

297
00:36:32,100 --> 00:36:36,820
I'm surprised that YouTube has such a buffer. Well, I guess it kind of makes sense.

298
00:36:36,820 --> 00:36:50,500
No, it doesn't because if we were having a hey, I don't know. Anyway.

299
00:36:55,380 --> 00:36:57,700
10 minutes disappearing. Was that from yesterday?

300
00:36:57,700 --> 00:37:10,900
We did indeed. Okay, I'm going to end the YouTube video anyway. That's the broadcast for tonight.

301
00:37:11,700 --> 00:37:17,460
And I think we'll get on. I'm going to get on to figure out this.

302
00:37:18,500 --> 00:37:23,620
So next time tomorrow, hopefully I'll have an update. I'm saying we've got a new sangha,

303
00:37:23,620 --> 00:37:28,980
a new online sangha, along with our meditation page. There'll be somehow a connection with them.

304
00:37:28,980 --> 00:37:58,820
Hopefully. Okay, so thanks for joining in. Peace.

