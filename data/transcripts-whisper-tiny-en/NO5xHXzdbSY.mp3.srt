1
00:00:00,000 --> 00:00:05,460
Thank you for watching.

2
00:00:30,000 --> 00:00:38,480
I

3
00:00:38,480 --> 00:00:41,060
have a

4
00:01:11,060 --> 00:01:23,700
Kannada away, Padinti, Gauru, Gauru and Iya, Yutha Dhamma, Swami in the Anwaran Saita, Memawad Sattan,

5
00:01:23,700 --> 00:01:35,060
Itheriata, Karaganyan Amin, Memawad Sattan, Memawad Sattan Adi, Bhavana, Mooli Kagunanga.

6
00:01:35,060 --> 00:01:45,020
Sajiva, Sita, Karna, Karay, Pupata, Unuhaansay, Iya, Praman, Paridhi, Memawad Sattan, Iya,

7
00:01:45,020 --> 00:01:50,980
Prakashikarana, Iya, Visas, Karna, Tara, Unuhaansay, Memawad Sattan, Iya, Iya, Iya,

8
00:01:50,980 --> 00:01:56,460
Iya, Maita, Gauru and Iya, Arad, Naka.

9
00:01:56,460 --> 00:02:09,860
Namau kasir, Vakawako, Arasetos, Amma, Samakasir, Nakmo, Tasir, Vakawako, Arasakos, Amma, Samakasir,

10
00:02:09,860 --> 00:02:18,180
Nakmo, Tasir, Vakawako, Arasakos, Amma, Samakasir.

11
00:02:18,180 --> 00:02:22,220
Welcome everyone to the Buddhist colony.

12
00:02:22,220 --> 00:02:28,060
Today is the point of day, the monthly Buddhist colony.

13
00:02:28,060 --> 00:02:36,660
So today I want to start by explaining a little bit about what we mean by Buddhist,

14
00:02:36,660 --> 00:02:41,620
what we mean by the Buddhist asana, what is the Buddhist asana?

15
00:02:41,620 --> 00:02:51,060
And to explain how it is that we come to practice the Buddhist asana.

16
00:02:51,060 --> 00:02:54,720
Bhakiayana is best for Guru.

17
00:02:54,720 --> 00:03:00,520
Therefore, if we are sittingress throughout this room,

18
00:03:00,520 --> 00:03:03,060
we want to stay there for a very sick eat today and to

19
00:03:03,060 --> 00:02:57,280
keep our

20
00:02:57,280 --> 00:03:18,800
our

21
00:03:18,800 --> 00:03:34,720
The word Buddha sasana, this is what we come to practice here on the Buddhist holiday.

22
00:03:34,720 --> 00:03:39,520
Buddha sasana, the sasana is the teaching of the Buddha and the sasana of the Buddha is

23
00:03:39,520 --> 00:03:41,600
in three parts.

24
00:03:41,600 --> 00:03:48,680
The first part is the Pariati sasana, the Buddha is teaching us to gain intellectual knowledge,

25
00:03:48,680 --> 00:03:58,040
or knowledge in our memory of the teaching.

26
00:03:58,040 --> 00:04:06,480
Buddha sasana is the first part of the Buddha sasana.

27
00:04:06,480 --> 00:04:10,480
Buddha sasana is the first part of the Buddha sasana.

28
00:04:10,480 --> 00:04:31,360
The second part of the Buddha is teaching the Pariati sasana, the Buddha is teaching us

29
00:04:31,360 --> 00:04:36,000
how to practice the teachings, how to put them into practice and how to train ourselves

30
00:04:36,000 --> 00:04:42,680
and how to develop our mind to come to a higher understanding and to finally come to realize

31
00:04:42,680 --> 00:04:44,800
the truth of life.

32
00:04:44,800 --> 00:05:13,280
The first part of the Buddha sasana is the first part of the Buddha sasana and the third part

33
00:05:13,280 --> 00:05:28,080
of the Buddha s teaching is the Pari

34
00:05:28,080 --> 00:05:49,360
sasana is the first part of the Buddha sasana is the first part of the Buddha sasana

35
00:05:49,360 --> 00:06:02,280
sasana is the first part of the Buddha sasana is the third part of the Buddha sasana

36
00:06:02,280 --> 00:06:12,280
is the third part of the Buddha sasana.

37
00:06:12,280 --> 00:06:18,280
to the Buddha's teaching, who come to the monastery to practice the Buddha's teaching,

38
00:06:18,280 --> 00:06:20,480
is we try to give you all three.

39
00:06:20,480 --> 00:06:25,200
It's important to understand that we can't stop simply at learning the Buddha's teaching,

40
00:06:25,200 --> 00:06:31,600
at hearing a day Sana, at reading books, and think that somehow we are practicing according

41
00:06:31,600 --> 00:06:33,080
to the Buddha's teaching.

42
00:06:33,080 --> 00:06:34,920
It's really not enough and it's not complete.

43
00:06:34,920 --> 00:06:40,200
So here now I want to take you to the next level and help to explain for those of you

44
00:06:40,200 --> 00:06:46,000
who don't understand yet how it is that we practice the Buddha's teaching.

45
00:06:46,000 --> 00:06:50,520
And through practicing the hope is that I can give you some kind of practice that will

46
00:06:50,520 --> 00:06:54,440
allow you to realize the Buddha's teaching for yourself and come to change something

47
00:06:54,440 --> 00:06:59,160
about who you are to make you more free from suffering.

48
00:06:59,160 --> 00:07:11,440
The Buddha hasentry to keep one person outside of who you are to work with.

49
00:07:11,440 --> 00:07:20,820
He does not think of the Buddha's teaching, not that Professor

50
00:07:20,820 --> 00:07:25,360
his chair because he was capable from to sleep.

51
00:07:25,360 --> 00:07:35,920
So, first of all, the explanation is that we have to give you the Pariyaki sasana.

52
00:07:35,920 --> 00:07:40,920
I will try to explain to you in very brief terms, because obviously we have very little

53
00:07:40,920 --> 00:07:42,600
time in today.

54
00:07:42,600 --> 00:07:46,320
What is it that the Buddha taught and how is it that we put into practice the Buddhist

55
00:07:46,320 --> 00:07:47,320
teaching?

56
00:07:47,320 --> 00:08:10,160
So, how do we practice the Buddhist teaching?

57
00:08:10,160 --> 00:08:14,440
What is the teaching of the Buddha that we are going to put into practice?

58
00:08:14,440 --> 00:08:17,840
The core of the Buddha's teaching is called upamanda pada.

59
00:08:17,840 --> 00:08:27,320
The path of vigilance or being careful of paying attention and trying to stay awake and alert

60
00:08:27,320 --> 00:08:29,600
in our daily lives.

61
00:08:29,600 --> 00:08:34,440
So, what I am going to try to teach you is how to be alert, how to be aware right here

62
00:08:34,440 --> 00:08:39,800
and now, and not lose track of what you are doing and not lose sight of what is going on

63
00:08:39,800 --> 00:08:43,280
in your mind and in your body and in the world around you.

64
00:08:43,280 --> 00:08:53,560
So, I am going to try to explain to you how to be aware of what is going on in your mind.

65
00:08:53,560 --> 00:09:01,320
The practice of upamanda of vigilance or being able to be aware of what is going on in

66
00:09:01,320 --> 00:09:09,560
your mind is going to be aware of what is going on in your mind.

67
00:09:09,560 --> 00:09:18,960
The practice of upamanda, of vigilance or avoiding pamanda, which is negligence, is considered

68
00:09:18,960 --> 00:09:24,560
to be the four foundations of mindfulness, the Tatarosatipakana, these four teachings that

69
00:09:24,560 --> 00:09:30,000
the Buddha gave, which are a very concise summary of how we put into practice the Buddhist

70
00:09:30,000 --> 00:09:35,160
teaching, of how we come to see the truth, how we come to realize the truth of the world

71
00:09:35,160 --> 00:09:36,160
around us.

72
00:09:36,160 --> 00:10:02,040
So, I am going to try to explain to you how to be aware of the

73
00:10:02,040 --> 00:10:12,280
Tatarosatipakana, the core of the Buddha's practical teaching.

74
00:10:12,280 --> 00:10:16,680
So, there are many teachings that the Buddha gave on practicing, for instance, Buddha

75
00:10:16,680 --> 00:10:24,680
Nusati or Mehta, Bhavanah, or so on, many of very useful Marana Nusati useful meditations.

76
00:10:24,680 --> 00:10:28,280
But at the very core of the Buddha's teaching, to help us to come to see things as they

77
00:10:28,280 --> 00:10:31,520
are, is the teaching on the Tatarosatipakana.

78
00:10:31,520 --> 00:10:38,560
So, the four Tatarosatipakana are gaya, vidana, jita, dhamma.

79
00:10:38,560 --> 00:10:43,600
And what I would suggest is, as far as paryati goes, as far as the study of the Buddha's

80
00:10:43,600 --> 00:10:49,600
teaching goes, if we can remember and understand what these four things are, then we should

81
00:10:49,600 --> 00:10:52,320
consider that we have enough.

82
00:10:52,320 --> 00:10:54,760
There's more that we can learn, obviously.

83
00:10:54,760 --> 00:10:58,840
But if we learn these four Tatarosatipakana and understand how we are going to put them

84
00:10:58,840 --> 00:11:00,520
into practice.

85
00:11:00,520 --> 00:11:05,560
This is enough for us to start in the beginning to come to understand the Buddha's teaching.

86
00:11:30,520 --> 00:11:58,640
So, before we even begin to start, we have to learn what are the four Tatarosatipakana.

87
00:11:58,640 --> 00:12:01,960
We traditionally would like to have people memorize these.

88
00:12:01,960 --> 00:12:04,840
Because when you memorize them, then you don't have to come back and ask me later what

89
00:12:04,840 --> 00:12:07,040
are they, what is the first one and so on.

90
00:12:07,040 --> 00:12:12,360
If you have them in your memory, then at any time you can come back and remember what

91
00:12:12,360 --> 00:12:16,480
they are and reflect on them and begin to practice them at a fresh.

92
00:12:16,480 --> 00:12:19,480
So I'd like to have everyone memorize them now with me and I'll have you repeat them

93
00:12:19,480 --> 00:12:20,880
one by one after me.

94
00:12:20,880 --> 00:12:45,120
So, please repeat after me.

95
00:12:45,120 --> 00:13:10,240
Please repeat after me, please repeat for me three times.

96
00:13:10,240 --> 00:13:24,320
I'm very good.

97
00:13:24,320 --> 00:13:28,240
My teacher would say that at this point, if any of you were to die right now, you would

98
00:13:28,240 --> 00:13:31,400
go straight to heaven.

99
00:13:31,400 --> 00:13:37,160
So this is because simply considering the Buddha's teaching, here we have the core of

100
00:13:37,160 --> 00:13:39,040
the Buddha's teaching, remember.

101
00:13:39,040 --> 00:13:41,720
These four things are not something very simple.

102
00:13:41,720 --> 00:13:47,160
There is something that is very high at the top of the list of the teachings of the

103
00:13:47,160 --> 00:13:48,160
Buddha.

104
00:13:48,160 --> 00:13:54,360
So if we keep this in our mind, this is a Mahakusala, a great wholesomenate.

105
00:13:54,360 --> 00:13:57,920
And if at this moment you were to pass away, it would be with the Buddha's teaching in

106
00:13:57,920 --> 00:14:01,080
your heart, right fresh in your mind.

107
00:14:01,080 --> 00:14:05,800
And even not knowing what are these four things before you have any chance to practice

108
00:14:05,800 --> 00:14:06,800
them.

109
00:14:06,800 --> 00:14:10,720
It's a cause for you to gain great happiness in the future.

110
00:14:10,720 --> 00:14:39,440
The Buddha – and now he is a conscious about the beginning of the Buddha's – repeat

111
00:14:39,440 --> 00:14:47,560
In the Buddha's time, there were many examples of even animals who when they heard the Buddha's teaching and when they kept it in their mind

112
00:14:47,940 --> 00:14:53,160
When they died at that time they went straight to heaven. There's a story of Mandukateva Devobuddha

113
00:14:53,440 --> 00:15:00,400
This frog who was listening to the Buddha's teaching and was killed by a person who hunted frogs

114
00:15:00,960 --> 00:15:04,560
But his mind was so much full of faith that when he passed away

115
00:15:04,560 --> 00:15:09,120
He was born from a frog to be an angel, without even understanding the Buddha's words

116
00:15:09,120 --> 00:15:13,740
But simply having faith and tranquility of mind was enough

117
00:15:43,740 --> 00:16:01,740
We are going to go one step further and we will actually learn what are the four city

118
00:16:01,740 --> 00:16:04,540
Patana and try to put them into practice.

119
00:16:04,540 --> 00:16:10,780
So I'd like to now explain what are these four kaya with the najita dhamma.

120
00:16:10,780 --> 00:16:13,620
Kaya is referring to the body.

121
00:16:13,620 --> 00:16:15,620
This is the very beginning of our practice.

122
00:16:15,620 --> 00:16:18,020
We have to focus on what is clear.

123
00:16:18,020 --> 00:16:22,060
If I asked you, what part of you is the most clear and the most obvious?

124
00:16:22,060 --> 00:16:25,540
If you thought about it, you'd have to answer that it would be the body.

125
00:16:25,540 --> 00:16:29,300
Our body is the most obvious and clear part of who we are.

126
00:16:29,300 --> 00:16:33,020
It's not all of who we are, but it's the most prominent.

127
00:16:33,020 --> 00:16:36,940
So when we start to practice, we start by focusing on the body and this is the meaning

128
00:16:36,940 --> 00:16:37,940
of kaya.

129
00:16:37,940 --> 00:17:07,580
So I'd like to have everyone now begin to practice.

130
00:17:07,580 --> 00:17:14,700
I'm going to teach you, but I want you to try it for yourself and to test out what I'm

131
00:17:14,700 --> 00:17:19,780
saying so that it's not simply an intellectual exercise, it's something that you actually

132
00:17:19,780 --> 00:17:21,340
put into practice.

133
00:17:21,340 --> 00:17:26,220
Gaia, when we focus on some part of the body, it can really be any part.

134
00:17:26,220 --> 00:17:30,140
Normally we have people focus on the breath because our breathing happens all the time,

135
00:17:30,140 --> 00:17:31,740
the breath going in and going out.

136
00:17:31,740 --> 00:17:35,740
So what part of the body moves when the breath comes in?

137
00:17:35,740 --> 00:17:38,660
The most prominent part is the stomach.

138
00:17:38,660 --> 00:17:43,540
When the breath comes in, if we're relaxed, our stomach will rise.

139
00:17:43,540 --> 00:17:45,700
When the breath goes out, the stomach will fall.

140
00:17:45,700 --> 00:17:47,140
It's very obvious.

141
00:17:47,140 --> 00:17:50,260
So it's something that's very easy to take as our meditation object.

142
00:17:50,260 --> 00:17:55,740
This is going to be the first suggestion that I have for you, the example of how to practice

143
00:17:55,740 --> 00:17:57,460
the porceti patana.

144
00:17:57,460 --> 00:17:59,420
We will watch the stomach.

145
00:17:59,420 --> 00:18:20,760
We recommend one of the

146
00:18:20,760 --> 00:18:43,460
So when the stomach rises, we'll simply say to ourselves, rising.

147
00:18:43,460 --> 00:18:47,560
And when the stomach falls, we'll say to ourselves, fall.

148
00:18:47,560 --> 00:18:52,400
Why we say to ourselves, rising and falling is to remind ourselves, again, the word

149
00:18:52,400 --> 00:19:00,840
sati patana means to establish mindfulness, sati means mindfulness, or it means to remember.

150
00:19:00,840 --> 00:19:04,200
To remember clearly what it is that we're experiencing.

151
00:19:04,200 --> 00:19:09,760
So in order to remember, we remind ourselves, when the stomach is rising, we remind

152
00:19:09,760 --> 00:19:11,520
ourselves, this is rising.

153
00:19:11,520 --> 00:19:14,840
It's not me, it's not mine, it's not good, it's not bad.

154
00:19:14,840 --> 00:19:16,840
It's simply the rising of the stomach.

155
00:19:16,840 --> 00:19:21,640
When it's falling, we say to ourselves, falling, reminding ourselves, this is the falling.

156
00:19:21,640 --> 00:19:25,240
We're not trying to create anything or believe anything.

157
00:19:25,240 --> 00:19:30,440
We're simply trying to see it exactly for what it is, not more and not less.

158
00:19:30,440 --> 00:19:39,520
To all the ways we have created it would be found in Canada, in a state and in a state.

159
00:19:39,520 --> 00:19:44,080
We might know if there is something up there, that doesn't happen.

160
00:19:44,080 --> 00:19:49,660
I won't say that there is nothing new, I know it is something else here.

161
00:19:49,660 --> 00:19:55,600
But we can see that everyone has to change their actions,

162
00:19:55,600 --> 00:20:06,240
For those of you who this is already familiar, or who already have even a different type

163
00:20:06,240 --> 00:20:13,360
of meditation practice, please don't be upset or worried, please feel welcome to practice

164
00:20:13,360 --> 00:20:14,360
your own way.

165
00:20:14,360 --> 00:20:18,760
I'd like to encourage everyone now to begin to practice, so everyone close your eyes

166
00:20:18,760 --> 00:20:22,440
and if you're going to follow the way I'm teaching you, that's fine.

167
00:20:22,440 --> 00:20:24,560
If you have your own way, that's also fine.

168
00:20:24,560 --> 00:20:29,920
Please everyone sit comfortably, sit as you like, you can sit leaning against the wall

169
00:20:29,920 --> 00:20:33,000
or sit in a chair or whoever is comfortable.

170
00:20:33,000 --> 00:20:37,600
Close your eyes and begin to focus on the stomach, simply saying to yourself, rise,

171
00:20:37,600 --> 00:20:39,640
sing, call.

172
00:20:39,640 --> 00:20:44,960
Not out loud, but knowing to yourself, thinking in the mind, reminding yourself mentally

173
00:20:44,960 --> 00:20:48,040
that this is the rising, this is the form.

174
00:20:48,040 --> 00:20:50,040
Or as you prefer.

175
00:20:50,040 --> 00:21:11,420
So all of us, here, the

176
00:21:11,420 --> 00:21:17,860
Excuse me, there is a foreign location here and there is no property.

177
00:21:41,420 --> 00:21:46,420
.

178
00:21:46,420 --> 00:21:51,420
.

179
00:21:51,420 --> 00:21:54,420
.

180
00:21:54,420 --> 00:21:57,420
.

181
00:21:57,420 --> 00:22:00,420
.

182
00:22:00,420 --> 00:22:05,420
..

183
00:22:05,420 --> 00:22:08,420
..

184
00:22:08,420 --> 00:22:09,420
..

185
00:22:09,420 --> 00:22:13,420
.

186
00:22:13,420 --> 00:22:18,420
.

187
00:22:18,420 --> 00:22:23,420
.

188
00:22:23,420 --> 00:22:28,420
.

189
00:22:28,420 --> 00:22:33,420
..

190
00:22:33,420 --> 00:22:37,420
..

191
00:22:37,420 --> 00:22:37,420
..

192
00:23:07,420 --> 00:23:37,380
Now, what you will notice in the beginning, when you

193
00:23:37,380 --> 00:23:42,900
first start to practice, is that the mind isn't always

194
00:23:42,900 --> 00:23:46,900
staying with the rising and the falling of the stomach.

195
00:23:46,900 --> 00:23:52,740
The mind is wandering, and there are many other things that are taking the attention of the mind

196
00:23:52,740 --> 00:24:21,180
away from the stomach.

197
00:24:21,180 --> 00:24:26,220
This is why we focus on the body, because it allows us to see not only the physical,

198
00:24:26,220 --> 00:24:31,900
but also the mental experience that is coming at every moment.

199
00:24:31,900 --> 00:24:37,660
So the next thing that we'll have to focus on is something that we should see quite quickly

200
00:24:37,660 --> 00:24:39,220
when we begin to practice.

201
00:24:39,220 --> 00:24:44,060
We're done, that means the sensations in the body, whether they be pleasant sensations,

202
00:24:44,060 --> 00:24:47,180
painful sensations, or neutral sensations.

203
00:24:47,180 --> 00:25:14,160
We want to appreciate the

204
00:25:14,160 --> 00:25:26,640
So when a sensation arises, whether it be painful, pleasant, or neutral, we take our minds

205
00:25:26,640 --> 00:25:27,800
away from the stomach.

206
00:25:27,800 --> 00:25:31,600
We don't have to force our minds to stay with the rising and falling.

207
00:25:31,600 --> 00:25:33,560
There's nothing special about it.

208
00:25:33,560 --> 00:25:38,400
And we focus instead where the mind is already inclined towards.

209
00:25:38,400 --> 00:25:40,640
And that is on the sensation.

210
00:25:40,640 --> 00:25:47,440
When you feel a pleasant sensation, you can say to yourself, pleasant, pleasant, or happy.

211
00:25:47,440 --> 00:25:53,320
If you feel pain, you can say to yourself, pain, pain, if you feel calm, or neutral, you

212
00:25:53,320 --> 00:25:57,280
can say calm, or neutral, and neutral.

213
00:25:57,280 --> 00:26:00,400
Just according to as it appears to you.

214
00:26:00,400 --> 00:26:12,580
Then the benefhasty sounds are difficult.

215
00:26:12,580 --> 00:26:20,020
But when you feel pain, your hands and hands are felt like it is difficult.

216
00:26:14,020 --> 00:26:21,900
If you feel delighted, you will be able to feel your favor.

217
00:26:21,900 --> 00:26:25,860
Please, sir.

218
00:26:25,860 --> 00:26:45,860
You can even simply say to yourself, feeling, feeling, feeling, for whatever type of feeling arises.

219
00:26:45,860 --> 00:26:50,340
The important thing is to remind yourself that it's only a feeling, so that your mind

220
00:26:50,340 --> 00:27:00,340
stays with a bare awareness, simply knowing it for what it is. Not giving rise to judging as me, mine, good, bad, and so on.

221
00:27:21,340 --> 00:27:44,340
When the sensation is gone, we simply return back to our focus of the rising and the falling of the stomach or some part of the body.

222
00:27:44,340 --> 00:27:58,340
If the stomach is in clear, you can also simply say to yourself, sitting, sitting, for example, as something that is clearly visible and clearly observable to you.

223
00:27:58,340 --> 00:28:13,340
So, the feeling that you meet close with your results in doing so, is something we can favor each other immediately.

224
00:28:13,340 --> 00:28:21,340
And if the stomach is in the center of the body, it is very wide.

225
00:28:21,340 --> 00:28:30,600
I am a citizen member being a civil major.

226
00:28:51,340 --> 00:28:54,340
.

227
00:28:54,340 --> 00:28:59,340
..

228
00:28:59,340 --> 00:29:03,340
..

229
00:29:03,340 --> 00:29:09,340
..

230
00:29:09,340 --> 00:29:12,340
...

231
00:29:12,340 --> 00:29:15,340
...

232
00:29:15,340 --> 00:29:18,340
...

233
00:29:18,340 --> 00:29:20,340
...

234
00:29:20,340 --> 00:29:42,340
..

235
00:29:42,340 --> 00:29:44,340
..

236
00:29:44,340 --> 00:29:45,340
..

237
00:29:45,340 --> 00:29:46,340
..

238
00:29:46,340 --> 00:29:47,340
..

239
00:29:47,340 --> 00:29:48,340
..

240
00:29:48,340 --> 00:29:53,420
the mind, the most important aspect is the thinking. Even though we might be

241
00:29:53,420 --> 00:29:57,820
thinking good thoughts or bad thoughts, we might be thinking many thoughts or

242
00:29:57,820 --> 00:30:03,940
few thoughts. The most important activity of the mind is to think. So in order to

243
00:30:03,940 --> 00:30:08,060
keep it simple, especially when we're beginning to practice, we can think of

244
00:30:08,060 --> 00:30:13,100
the mind as simply that which is thinking. When we're sitting in meditation,

245
00:30:13,100 --> 00:30:18,140
watching the stomach, or the feelings, or even knowing that we're sitting, or so on,

246
00:30:18,140 --> 00:30:23,580
there might arise thoughts, thinking of the past, thinking of the future, good

247
00:30:23,580 --> 00:30:28,180
thoughts or bad thoughts. Whatever kind of thought arises, it's very simple. We simply

248
00:30:28,180 --> 00:30:34,020
say to ourselves thinking, thinking, thinking, reminding ourselves that we're

249
00:30:34,020 --> 00:30:38,700
thinking and not letting ourselves get caught up in the thought and carried away by

250
00:30:38,700 --> 00:31:01,380
it.

251
00:31:38,700 --> 00:31:48,700
When the thought disappears, as with the Vedana, we simply come back again to the rising

252
00:31:48,700 --> 00:31:54,180
in the falling or the observation of the physical, of the body.

253
00:31:54,180 --> 00:32:01,180
Now, I am going to talk about the Vedana.

254
00:32:01,180 --> 00:32:10,180
I am going to talk about the Vedana.

255
00:32:10,180 --> 00:32:36,180
I am going to talk about the Vedana.

256
00:32:36,180 --> 00:33:02,180
I am going to talk about the Vedana.

257
00:33:02,180 --> 00:33:28,180
I am going to talk about the Vedana.

258
00:33:28,180 --> 00:33:44,020
The fourth set of Vedana and dhamma nupasana refers to the reality that will come to see

259
00:33:44,020 --> 00:33:49,260
those things that will arise through our observation.

260
00:33:49,260 --> 00:33:53,340
Most important referring to the kustala and the akhustala dhamma, the wholesome and

261
00:33:53,340 --> 00:33:57,940
wholesome dhammas that arise in our mind.

262
00:33:57,940 --> 00:34:03,140
When we begin to practice, the most important is going to be the overwhelming akhustala,

263
00:34:03,140 --> 00:34:08,060
which we've accumulated through our lack of practicing.

264
00:34:08,060 --> 00:34:14,380
And this is where we start when we focus on dhamma nupasana, is on the akhustala dhamma, the unwholesome,

265
00:34:14,380 --> 00:34:19,340
or those things which are hindrance to our practice and to our light.

266
00:34:19,340 --> 00:34:25,340
I'm going to practice awhile, and have today's idea.

267
00:34:25,340 --> 00:34:31,340
Lets go together and take a great, will seek that so much to you.

268
00:35:01,340 --> 00:35:25,020
The Niwarana Dhamma are five things that are very important for meditators, also very

269
00:35:25,020 --> 00:35:30,940
important for people who are not meditating because, as I said, they are hindrances.

270
00:35:30,940 --> 00:35:36,620
In Pali, they have very long names that make them very difficult to comprehend and to practice

271
00:35:36,620 --> 00:35:42,460
for an ordinary person of Gama, Tanda, Baya, Bada, and so on.

272
00:35:42,460 --> 00:35:45,540
When we're practicing meditation, we need simple words.

273
00:35:45,540 --> 00:35:51,100
Words that we can use to recognize very quickly what it is that has arisen.

274
00:35:51,100 --> 00:35:54,900
So here I have five words for these five things that I'm going to have you memorize as

275
00:35:54,900 --> 00:35:59,940
well, so that you can always remember when these things arise, how to catch them, how

276
00:35:59,940 --> 00:36:03,740
to practice according to the Satipatana.

277
00:36:29,940 --> 00:36:56,540
So you can keep your eyes closed, but please repeat after me.

278
00:36:56,540 --> 00:37:17,260
Liking, liking, disliking, drowsiness, distraction, doubt, liking, disliking, disliking,

279
00:37:17,260 --> 00:37:33,900
disliking, disliking, drowsiness, distraction, and doubt, can you say it please, disliking.

280
00:37:33,900 --> 00:37:39,580
So liking, disliking, drowsiness, distraction, and doubt, these are five words in English,

281
00:37:39,580 --> 00:37:52,860
and singing a little bit, we can give a translation of this, liking, you know, this is like

282
00:37:52,860 --> 00:38:16,900
this.

283
00:38:16,900 --> 00:38:21,540
So it doesn't matter what word we use, what language or what word, what matters is that

284
00:38:21,540 --> 00:38:28,620
it's something that captures the essence of the reality, of the phenomenon as it has arisen.

285
00:38:28,620 --> 00:38:33,700
So when we like something, when liking comes up or we want something, we simply say

286
00:38:33,700 --> 00:38:41,620
to ourselves, liking, liking, or wanting, wanting, when disliking comes up, we simply say

287
00:38:41,620 --> 00:38:48,500
to ourselves, disliking, disliking, disliking, when we feel angry or sad or frustrated or

288
00:38:48,500 --> 00:38:58,580
bored or scared, we simply say to ourselves, disliking, boring, angry, scared, afraid,

289
00:38:58,580 --> 00:39:05,540
whatever has arisen, however it appears to us, when we feel tired or drowsy, we say to

290
00:39:05,540 --> 00:39:12,780
ourselves, tired, tired or drowsy, when we're distracted or worried, we say to ourselves,

291
00:39:12,780 --> 00:39:19,380
distracted, distracted, worried, worried, and when we have doubt or we're confused, we say

292
00:39:19,380 --> 00:39:29,220
to ourselves, doubting or confused, confused, really whatever emotion or mind state arises,

293
00:39:29,220 --> 00:39:34,540
we catch it for what it is, pick a simple word, like these ones I've given you, and remind

294
00:39:34,540 --> 00:39:41,460
ourselves that that's all it is, it's not me, mine, good, bad, and there's no other need

295
00:39:41,460 --> 00:39:46,780
to go any further than simply knowing it for what it is.

296
00:39:46,780 --> 00:40:15,620
So, in

297
00:40:15,620 --> 00:40:22,200
you are driving this operation to start the entire discussion actual deployment of our

298
00:40:22,200 --> 00:40:26,040
vehicle, so it's part of the user warning that everything is necessary.

299
00:40:26,040 --> 00:40:32,560
Speaking fromla, we are trying to get a kind of Qna, so we haven't seen a couple of

300
00:40:32,560 --> 00:40:37,960
markets yet, in that way we have thrown products, a bit of a good savings.

301
00:40:37,960 --> 00:40:59,020
P

302
00:40:59,020 --> 00:41:06,020
They come back again to observe Kaya, to observe the body rising and falling or sit things.

303
00:41:06,020 --> 00:41:14,020
Now that I have a client, I have an enemy to have a Y-D-V-CT, my Y-D-V-CT, my Y-D-V-CT.

304
00:41:14,020 --> 00:41:38,020
Let's see what's going on here.

305
00:41:38,020 --> 00:42:04,020
Let's see what's going on here.

306
00:42:04,020 --> 00:42:30,020
Let's see what's going on here.

307
00:42:30,020 --> 00:42:56,020
Let's see what's going on here.

308
00:42:56,020 --> 00:43:22,020
Let's see what's going on here.

309
00:43:22,020 --> 00:43:48,020
Let's see what's going on here.

310
00:43:48,020 --> 00:44:14,020
Let's see what's going on here.

311
00:44:14,020 --> 00:44:40,020
Let's see what's going on here.

312
00:44:40,020 --> 00:45:06,020
Let's see what's going on here.

313
00:45:06,020 --> 00:45:32,020
Let's see what's going on here.

314
00:45:32,020 --> 00:45:58,020
Let's see what's going on here.

315
00:45:58,020 --> 00:46:24,020
Let's see what's going on here.

316
00:46:24,020 --> 00:46:50,020
Let's see what's going on here.

317
00:46:50,020 --> 00:47:16,020
Let's see what's going on here.

318
00:47:16,020 --> 00:47:42,020
Let's see what's going on here.

319
00:47:42,020 --> 00:48:08,020
Let's see what's going on here.

320
00:48:08,020 --> 00:48:34,020
Let's see what's going on here.

321
00:48:34,020 --> 00:49:03,020
Let's see what's going on here.

322
00:49:03,020 --> 00:49:29,340
Before we finish, before you open your eyes ask that everyone please extend our loving kindness

323
00:49:29,340 --> 00:49:44,340
or make that through all beings starting with ourselves may I be happy, may I be free from suffering.

324
00:49:44,340 --> 00:50:13,340
I'm extending it to all the angels who guard this place, extending it to all the beings in this

325
00:50:13,340 --> 00:50:39,340
place.

326
00:50:39,340 --> 00:51:05,340
Let's see what's going on here.

327
00:51:05,340 --> 00:51:31,340
When you finish, you can open your eyes.

328
00:51:31,340 --> 00:51:57,340
Let's see what's going on here.

329
00:51:57,340 --> 00:52:23,340
Let's see what's going on here.

