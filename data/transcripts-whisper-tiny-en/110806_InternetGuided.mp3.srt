1
00:00:00,000 --> 00:00:06,440
In Pauli, we have several words that we use when we talk about this thing that we call

2
00:00:06,440 --> 00:00:12,160
meditation, but we don't often use a word that is parallel to the word meditation, but

3
00:00:12,160 --> 00:00:17,160
in English it's a really good word.

4
00:00:17,160 --> 00:00:22,280
So if you think about this word, often it doesn't quite jive with our understanding

5
00:00:22,280 --> 00:00:28,120
of what we're doing when we sit down, because meditation means in some sense to ponder

6
00:00:28,120 --> 00:00:32,640
or to consider to examine.

7
00:00:32,640 --> 00:00:38,160
And often when we come to sit and think that we're practicing meditation, we're not

8
00:00:38,160 --> 00:00:40,960
actually meditating at all.

9
00:00:40,960 --> 00:00:48,520
Sometimes we're sitting and we're hoping for something or we're clinging to something,

10
00:00:48,520 --> 00:00:55,920
looking for a peaceful, calm state, sometimes we're trying to push ourselves or push

11
00:00:55,920 --> 00:00:58,120
away thoughts.

12
00:00:58,120 --> 00:01:03,920
Sometimes we're trying to just quiet the mind, oh, I want to make my mind quiet.

13
00:01:03,920 --> 00:01:07,480
And we work hard and work hard to make our mind quiet.

14
00:01:07,480 --> 00:01:12,800
But that's not included in the definition of the word meditation.

15
00:01:12,800 --> 00:01:18,360
And this is important because what I practice, what I would want to share with you and

16
00:01:18,360 --> 00:01:26,480
what as practice, what we practice under the Buddha, is a form of meditation.

17
00:01:26,480 --> 00:01:30,840
It's not that we're going to think, but that we're going to examine and we're going

18
00:01:30,840 --> 00:01:37,160
to look at and we're going to be with reality, with our experience.

19
00:01:37,160 --> 00:01:43,200
We're not going to try to create anything or to chase away anything, to be anything

20
00:01:43,200 --> 00:01:53,160
other than what we already are, we're just going to try to clear up and to see more clearly

21
00:01:53,160 --> 00:01:56,680
the things that are already right here in front of us.

22
00:01:56,680 --> 00:01:59,480
So this is how I want you to understand what we're doing for those of you who have never

23
00:01:59,480 --> 00:02:01,040
practiced before.

24
00:02:01,040 --> 00:02:03,720
This is called Vipasana.

25
00:02:03,720 --> 00:02:08,320
Vipasana means to see clearly.

26
00:02:08,320 --> 00:02:13,640
So it doesn't mean we're not going to practice to find some special state.

27
00:02:13,640 --> 00:02:21,200
We're going to practice to see our minds and to understand our minds clearly, to understand

28
00:02:21,200 --> 00:02:24,200
our minds for what they are.

29
00:02:24,200 --> 00:02:34,960
The method that we use to see clearly is generally understood to be the four foundations

30
00:02:34,960 --> 00:02:39,560
of mindfulness or the four satipatana, which many of you may have heard of.

31
00:02:39,560 --> 00:02:47,080
Now there are many ways to go about seeing clearly and many different techniques.

32
00:02:47,080 --> 00:02:54,480
But the reason why we say to see clearly you have to use or you do or we always use the

33
00:02:54,480 --> 00:03:00,040
four foundations of mindfulness is because when we say the four satipatana or the four foundations

34
00:03:00,040 --> 00:03:04,200
of mindfulness, it's really not some specific practice.

35
00:03:04,200 --> 00:03:12,280
It's just a way of understanding the whole of reality because all parts of our experience

36
00:03:12,280 --> 00:03:15,400
fit into the four foundations of mindfulness.

37
00:03:15,400 --> 00:03:22,080
Now the reason we use the four foundations of mindfulness is because it's a framework or

38
00:03:22,080 --> 00:03:28,120
a categorization of the parts of our experience so that when something comes up, when we

39
00:03:28,120 --> 00:03:34,040
experience something, when something arises in our frame of vision, we know what it is

40
00:03:34,040 --> 00:03:38,360
or we know approximately where it fits into reality.

41
00:03:38,360 --> 00:03:41,880
So when we hear a dog barking, we don't think of a dog.

42
00:03:41,880 --> 00:03:46,360
We don't think of it as a dog because we know dog isn't in the four foundations of mindfulness.

43
00:03:46,360 --> 00:03:51,840
So therefore, dog isn't in the ultimate reality of the experience.

44
00:03:51,840 --> 00:03:56,560
But hearing is, the sound that arises at the ear is real.

45
00:03:56,560 --> 00:04:02,240
That's a part of the four foundations of mindfulness and therefore it's a part of reality.

46
00:04:02,240 --> 00:04:07,320
We also know right away where that fits in, when we hear something, we know that that's

47
00:04:07,320 --> 00:04:08,320
hearing.

48
00:04:08,320 --> 00:04:13,400
We hear the dog barking, we hear our dog barking to be let in and so on, and we have all

49
00:04:13,400 --> 00:04:14,920
these ideas about it.

50
00:04:14,920 --> 00:04:19,120
But the core reality of it is only the hearing and so we right away catch it to be

51
00:04:19,120 --> 00:04:20,120
hearing.

52
00:04:20,120 --> 00:04:24,240
We know where it fits in with the four satipatana and we know how to deal with it.

53
00:04:24,240 --> 00:04:30,600
This makes it easy in our minds to understand what is real and what is not real and allows

54
00:04:30,600 --> 00:04:35,960
us to quickly establish a clear awareness of the object.

55
00:04:35,960 --> 00:04:37,640
So this is why we have this framework.

56
00:04:37,640 --> 00:04:43,280
This is why we say to see clearly, we use the four foundations of mindfulness.

57
00:04:43,280 --> 00:04:48,040
The four foundations, so this is how I'm going to explain the meditation and this is

58
00:04:48,040 --> 00:04:53,720
the meditation that I'm going to have you to practice with me.

59
00:04:53,720 --> 00:05:03,720
So the four foundations of mindfulness are the body, the feelings, the mind, and the dumb.

60
00:05:03,720 --> 00:05:10,280
Well dumb, I'm not going to translate yet, I'll explain it in a second body, is the

61
00:05:10,280 --> 00:05:14,520
from our head to our feet, everything that we think of generally as us.

62
00:05:14,520 --> 00:05:16,920
So in modern society, we think that's it.

63
00:05:16,920 --> 00:05:24,920
I am my body, the brain is what thinks and the heart is what lives and all of this is who

64
00:05:24,920 --> 00:05:25,920
I am.

65
00:05:25,920 --> 00:05:33,480
And Buddhism, this is only a part and it's the part that is that we can see that we can

66
00:05:33,480 --> 00:05:37,240
experience on a physical level.

67
00:05:37,240 --> 00:05:39,360
But there's more to it, reality than that.

68
00:05:39,360 --> 00:05:45,880
So the body is this physical experience and moreover, it's the experience of the physical

69
00:05:45,880 --> 00:05:46,880
body.

70
00:05:46,880 --> 00:05:53,320
So when we feel the foot moving or when we feel the stiffness in our chair or on the sitting

71
00:05:53,320 --> 00:06:01,640
mat, we feel stiffness in our back, we feel the pressure in our stomach, whenever we feel

72
00:06:01,640 --> 00:06:07,440
a feeling in the body, the feeling of our tongue touching the roof of our mouth and so

73
00:06:07,440 --> 00:06:08,440
on.

74
00:06:08,440 --> 00:06:15,320
Every piece of our experience, our hands touching one another when we're sitting, our legs

75
00:06:15,320 --> 00:06:17,560
touching the floor and so on.

76
00:06:17,560 --> 00:06:23,400
All of this experience of the physical, that's what we mean by body.

77
00:06:23,400 --> 00:06:40,080
Feelings is our basic sensory experience of these phenomena, the pressures and so on,

78
00:06:40,080 --> 00:06:42,280
all of the physical parts.

79
00:06:42,280 --> 00:06:47,240
So it can be a pleasant feeling, it can be an unpleasant feeling.

80
00:06:47,240 --> 00:06:54,520
For instance, when your legs are pressing and hard against the floor or when your back

81
00:06:54,520 --> 00:06:59,240
is stiff or so on, it can be an unpleasant feeling.

82
00:06:59,240 --> 00:07:04,160
Sometimes when you're sitting and you've had a good meal and the room is warm and so on,

83
00:07:04,160 --> 00:07:06,760
you might feel a pleasant feeling.

84
00:07:06,760 --> 00:07:09,880
But mostly, it will feel simply a neutral feeling.

85
00:07:09,880 --> 00:07:14,800
So when the pressure against the floor, the pressure against our back, the pressure of

86
00:07:14,800 --> 00:07:17,720
our hands touching, it just gives us a neutral feeling.

87
00:07:17,720 --> 00:07:23,880
We don't feel pleasure because of it and we don't feel displeasure.

88
00:07:23,880 --> 00:07:25,800
We can also have mental feelings.

89
00:07:25,800 --> 00:07:29,800
So we might feel happy in the mind when we think of something or we might feel unhappy

90
00:07:29,800 --> 00:07:35,160
in the mind when we think of something and so on or when we experience something, when

91
00:07:35,160 --> 00:07:40,720
we have loud noises around, we might get upset and therefore feel an unhappy feeling, unpleasant

92
00:07:40,720 --> 00:07:48,120
feeling in the mind and sometimes we might hear something nice, maybe you hear someone

93
00:07:48,120 --> 00:07:52,760
say something nice to you and you feel happy as a result, so there's pleasure in the

94
00:07:52,760 --> 00:07:53,760
mind.

95
00:07:53,760 --> 00:07:57,560
So there's the physical feelings and the mental feelings, but it means happy, unhappy or

96
00:07:57,560 --> 00:07:58,560
neutral.

97
00:07:58,560 --> 00:08:00,200
This is what we mean by feeling.

98
00:08:00,200 --> 00:08:04,280
Everything that we experience will have an associated feeling with it if it's not pleasant

99
00:08:04,280 --> 00:08:06,560
or unpleasant, it will be a neutral feeling.

100
00:08:06,560 --> 00:08:11,440
This is the second part of reality, the second part of our practice.

101
00:08:11,440 --> 00:08:21,480
The third part is the mind itself and the mind can be understood in many ways, and it can

102
00:08:21,480 --> 00:08:24,960
be formed in many ways, it can be in many different states.

103
00:08:24,960 --> 00:08:30,000
So sometimes the mind is distracted, sometimes the mind is very focused, sometimes the

104
00:08:30,000 --> 00:08:36,720
mind is loud and chaotic, sometimes the mind is quiet, sometimes the mind is clear, sometimes

105
00:08:36,720 --> 00:08:42,720
the mind is not clear, sometimes it's confused, sometimes it's angry, sometimes it's greedy,

106
00:08:42,720 --> 00:08:49,800
sometimes it's caring and loving and so on, there's all sorts of states of the mind.

107
00:08:49,800 --> 00:08:54,680
Now for the meditate, for the purposes of meditation, we're just going to understand it as

108
00:08:54,680 --> 00:09:01,360
that which experiences an object, that which examines the object.

109
00:09:01,360 --> 00:09:07,800
So when you have these experiences, first you have the object, for instance the sensation

110
00:09:07,800 --> 00:09:13,200
of our legs and our bottom against the floor when we're sitting, and then we have the

111
00:09:13,200 --> 00:09:18,200
feeling of it, and we also have a thought about it, or the examining mind about it can

112
00:09:18,200 --> 00:09:27,440
be judgments, it can be recognitions, when we recognize it to be, oh this is a feeling,

113
00:09:27,440 --> 00:09:32,520
or this is my stomach rising, this is my stomach falling, when we hear a sound and we think

114
00:09:32,520 --> 00:09:38,240
about it, we recognize that's a dog, that's a cat, that's my friend and my wife and my

115
00:09:38,240 --> 00:09:43,840
husband, and then we can judge it, we say that's a good sound, a bad sound, and we can

116
00:09:43,840 --> 00:09:48,280
judge the feelings and so on, all of the thoughts that go on in the mind, this is what we

117
00:09:48,280 --> 00:09:53,400
understand to be mind, so in general we're not going to go into detail in our practice

118
00:09:53,400 --> 00:09:58,440
and try to see exactly what state the mind is in, but whatever state it's in, we're

119
00:09:58,440 --> 00:10:02,080
going to take this aspect of reality just to be the mind.

120
00:10:02,080 --> 00:10:06,160
So the easiest way to do this is when we think, when we're thinking to just catch that

121
00:10:06,160 --> 00:10:11,960
up and to use our meditation and acknowledge thinking, thinking as I'll explain in a second,

122
00:10:11,960 --> 00:10:18,880
so let's understand the mind to be basically thinking, the thinking about an object, or

123
00:10:18,880 --> 00:10:25,280
the thinking that arises in relation to the object, this is number three, number four, I said

124
00:10:25,280 --> 00:10:32,600
I wasn't going to translate it because, well it's a difficult word to translate, actually

125
00:10:32,600 --> 00:10:40,200
the word dhamma simply means something that carries or something that is carried, something

126
00:10:40,200 --> 00:10:48,520
that is held, something that holds or something that is held, and so the Buddha dhamma is

127
00:10:48,520 --> 00:10:53,880
the dhamma, what the Buddha holds to be his truth, it holds to be the truth, the Buddha

128
00:10:53,880 --> 00:10:58,600
held certain things to be truth, and that was called his dhamma, of course we know the

129
00:10:58,600 --> 00:11:02,760
Buddha didn't hold them to be true, he experienced and understood them to be true, but

130
00:11:02,760 --> 00:11:08,080
still in terms of society they call that his dhamma, other people held other things to be

131
00:11:08,080 --> 00:11:15,360
true and so they call that dhamma, now it can also mean something that holds its own existence,

132
00:11:15,360 --> 00:11:25,600
so the feeling in your body and the sensations and the pain and the happiness, the pleasure

133
00:11:25,600 --> 00:11:30,600
and the thoughts, all of these hold their own existence in the sense that they do arise,

134
00:11:30,600 --> 00:11:37,720
they do really and truly arise, when you hear a dog barking the sound arises, now the dog

135
00:11:37,720 --> 00:11:43,880
doesn't arise, all that arises is in your mind there's a thought, oh that's a dog, but actually

136
00:11:43,880 --> 00:11:51,240
the dog is never experienced, he will never experience a dog, so dog is not a dhamma in this sense,

137
00:11:51,240 --> 00:11:57,000
when we talk about something that holds its own existence, because this comes from the verb

138
00:11:57,000 --> 00:12:03,480
dhatat, which means to hold or to carry, so this carries its own existence, this is the meaning

139
00:12:03,480 --> 00:12:10,120
of dhamma, so there are two possible meanings here and you can understand it both ways and that's

140
00:12:10,120 --> 00:12:15,320
why I'm not going to translate it, we understand it as actually groups of the Buddha's teaching,

141
00:12:15,960 --> 00:12:21,560
so the Buddha taught certain things, this is real, this is real and he taught what was going to

142
00:12:21,560 --> 00:12:27,000
arise when we practice meditation and so these are his teachings and he taught these things

143
00:12:27,000 --> 00:12:33,640
that arise will be for your benefit, these things that arise will be for your detriment and these

144
00:12:33,640 --> 00:12:38,920
things will be an indication of your progress and should be developed and so on, it should be

145
00:12:39,640 --> 00:12:45,000
encouraged and these things will arise for your detriment, these things should be avoided,

146
00:12:45,000 --> 00:12:49,720
should be discarded and thrown away and so on, so on, so in that hand we can think of these

147
00:12:49,720 --> 00:12:54,360
these things as ethical qualities in a sense, that's what we mean by dhamma here,

148
00:12:54,360 --> 00:13:01,640
or we can also think of it as all of reality and I think actually most translators like to

149
00:13:02,280 --> 00:13:09,400
like to take this translation, they will say this is mind objects, they will translate dhamma as

150
00:13:09,400 --> 00:13:14,040
mind objects, something the mind takes as an object, now the problem I have with this and this is

151
00:13:14,040 --> 00:13:20,440
just a technical problem, but for some of you this might be interesting, is that body feelings

152
00:13:20,440 --> 00:13:26,200
and mind are also dhamma then because you take the body as a mind object, you take the feelings

153
00:13:26,200 --> 00:13:33,560
as a mind object, you even take the mind as a mind object, this is the technical reality of it,

154
00:13:33,560 --> 00:13:39,160
so we're not really distinguishing a fourth category at all, or we're having a great overlap here,

155
00:13:39,160 --> 00:13:45,960
which makes it confusing and if you look at the layout of the fourth setipatana, it's quite

156
00:13:45,960 --> 00:13:51,320
clear that the Buddha laid it out in terms of his various teachings that he had given, so we can

157
00:13:51,320 --> 00:13:56,760
look at it both ways, but let's just say this is a group of things that the Buddha taught,

158
00:13:56,760 --> 00:14:03,480
that are going to come into play in our practice, so we have these first three, and then the fourth

159
00:14:03,480 --> 00:14:10,120
one we have various states of mind, various phenomenon that arise, good ones and bad ones that

160
00:14:10,120 --> 00:14:15,880
will arise in the mind, that we have to be able to distinguish and we have to be able to analyze

161
00:14:15,880 --> 00:14:19,880
and acknowledge and see for what they are because some of them will be good and some of them will be

162
00:14:19,880 --> 00:14:26,600
bad, some of them will be neutral, but they will give rise to ethical states as a result of

163
00:14:26,600 --> 00:14:33,480
our judgment, our experience, enough of them, so let's just leave this one as dhamma and understand it

164
00:14:33,480 --> 00:14:43,720
as certain sets of teachings or sets of phenomena. The first set, and it's just to make it really

165
00:14:43,720 --> 00:14:50,040
easy and so that you don't get confused by what I just said, the first set is really the most

166
00:14:50,040 --> 00:14:57,640
important, it's the bad dhammas, the dhammas that the Buddha held to be unethical or to be in a

167
00:14:57,640 --> 00:15:05,080
sense detrimental to our practice, so these are called the niwarana, and if this is all in the

168
00:15:05,080 --> 00:15:09,560
beginning, if this is all we understood dhamma to be then that's fine, that's enough, enough to get

169
00:15:09,560 --> 00:15:16,040
us started on the practice, so the dhamma, the niwarana dhamma, the dhammas that are going to be

170
00:15:16,040 --> 00:15:23,800
hindrances in our practice are five, and they have very long poly names, but to make it easy for

171
00:15:23,800 --> 00:15:33,080
our meditation we understand them in very simple terms, liking, disliking, drowsiness, distraction,

172
00:15:33,080 --> 00:15:40,680
and doubt, is I giving you very simple translations here because these are going to be

173
00:15:41,560 --> 00:15:47,800
how we're going to acknowledge them, this would make it very easy for us to recognize them when

174
00:15:47,800 --> 00:15:53,480
we practice the meditation, so liking, sometimes we'll like things, we experience something

175
00:15:53,480 --> 00:16:00,760
and we enjoy it, or we like it, or we hold on to it, sometimes we will dislike things, we'll be

176
00:16:00,760 --> 00:16:09,640
angry or frustrated or upset by things, sometimes we'll feel drowsy or tired, we'll feel like we're

177
00:16:09,640 --> 00:16:16,840
not able to catch the object up in our awareness, our mind is sluggish and so on, sometimes we feel

178
00:16:16,840 --> 00:16:22,520
distracted where our mind is too energetic and thinking about too many things and not able to focus

179
00:16:22,520 --> 00:16:29,480
on a specific object, and sometimes we will have doubt, not sure about what we're doing,

180
00:16:29,480 --> 00:16:35,720
is the practice useful, we're not sure if we can, we doubt ourselves, can we accomplish this,

181
00:16:36,520 --> 00:16:44,120
and so on, doubting any aspect of the Buddha's teaching or so on, and giving rise to this

182
00:16:44,120 --> 00:16:55,400
unsure state of mind, these five dumbas are unethical, and we have to understand why they're

183
00:16:55,400 --> 00:17:00,280
unethical, they're unethical only because they are of a detriment to our mind state,

184
00:17:00,280 --> 00:17:05,560
now some people say doubting is actually good sometimes because if you just believe everything,

185
00:17:05,560 --> 00:17:11,000
then you will believe bad things as well, but that's not the meaning of what we're talking about

186
00:17:11,000 --> 00:17:19,640
here, on a strictly phenomenological or experiential frame of reference,

187
00:17:20,760 --> 00:17:27,800
when you doubt something, it tears apart your mind or it takes your mind, takes away the focus

188
00:17:27,800 --> 00:17:35,880
and the concentration of the mind, so actually even if you believe something that is false,

189
00:17:35,880 --> 00:17:40,840
if you believe you want to kill someone and you believe it's a good thing to kill them,

190
00:17:40,840 --> 00:17:46,280
the faith that you have in that will actually be a good thing for you in the sense that it will

191
00:17:46,280 --> 00:17:50,680
help you achieve your goal, now killing people is bad and getting angry and wanting to do that is

192
00:17:50,680 --> 00:17:59,560
bad, but the confidence that you have is a positive, positive in the sense that it helps you

193
00:17:59,560 --> 00:18:05,800
to carry out your task, so when the point here being that when we have doubt in our mind,

194
00:18:05,800 --> 00:18:11,320
if we have doubt about the meditation, it will stand in our way of carrying out our tasks,

195
00:18:11,320 --> 00:18:18,600
so this is what we mean by unethical or hindrances, they are things that are going to

196
00:18:19,480 --> 00:18:25,640
harm us or be to our detriment, when we have doubt it's going to stop us from carrying out our

197
00:18:25,640 --> 00:18:32,200
tasks, now if we have bad tasks then it's not that the faith that we have is going to be a good

198
00:18:32,200 --> 00:18:36,440
thing, it's actually going to be a harm because it will help us carry out bad things,

199
00:18:36,440 --> 00:18:44,760
but the problem isn't the faith, and the doubt isn't good, but the act itself is the determining,

200
00:18:44,760 --> 00:18:49,080
so when we're practicing something good like meditation, it's very important that we have great

201
00:18:49,080 --> 00:18:53,560
faith in ourselves and faith in what we're doing, if you don't have that yet then you should

202
00:18:53,560 --> 00:19:00,280
examine and you should study and you should ask teachers and so on and try to get good answers,

203
00:19:00,280 --> 00:19:04,280
so that you are able to do away with the doubt because it will stand in your way,

204
00:19:04,280 --> 00:19:09,400
all five of these will stand in your way, that's why they're called new marina or hindrances,

205
00:19:11,400 --> 00:19:19,800
and this is the first group of dumbas, it's also the most important for beginners.

206
00:19:19,800 --> 00:19:24,280
A second group is the senses, so as I said hearing when you see something, when you smell

207
00:19:24,280 --> 00:19:28,200
something, but I don't want to get into too much detail, for those of you who have practiced

208
00:19:28,200 --> 00:19:32,600
before you know about this anyway, for those of you who haven't, let's stick to the basics.

209
00:19:33,960 --> 00:19:40,280
So this is basically how we come to see clearly, we look, we break reality up into little

210
00:19:40,280 --> 00:19:46,280
edibility pieces one moment at a time, and then we see it one moment at a time, we experience it,

211
00:19:46,280 --> 00:19:54,440
and we evaluate it, we examine it, we meditate on it one moment at a time, as we do this,

212
00:19:54,440 --> 00:20:01,320
we're going to come to realize many things that we couldn't see before, because we weren't

213
00:20:01,880 --> 00:20:09,640
looking, we weren't examining, we were simply following after or being led on by our

214
00:20:09,640 --> 00:20:16,520
experience, being led on by our habits, being led on by our preferences, our partialities,

215
00:20:16,520 --> 00:20:22,840
and so on. Now once we come to look at things clearly and simply see them from what they are,

216
00:20:22,840 --> 00:20:29,800
we're going to, it's going to open up many, many understandings and much wisdom for us,

217
00:20:29,800 --> 00:20:37,480
and that wisdom in and of itself will be enough to allow us to change, because once you see

218
00:20:37,480 --> 00:20:41,960
that something is causing you harm, you will never ever do it again, you will never ever

219
00:20:41,960 --> 00:20:47,560
engage in something that you see clearly as you harm, and once you see that something does you

220
00:20:47,560 --> 00:20:56,600
good on the other hand, your mind will naturally do to that wisdom naturally encourage it and

221
00:20:56,600 --> 00:21:04,040
naturally incline towards that sort of behavior. So this is what we mean by seeing clear,

222
00:21:04,040 --> 00:21:08,920
this is also the benefit of seeing clearly, that once you do, you will give up those things that

223
00:21:08,920 --> 00:21:13,000
are to your detriment, and you will take up those things that are to your benefit.

224
00:21:13,000 --> 00:21:21,880
So that's the first part of this program that I think is probably enough to allow us to now

225
00:21:22,680 --> 00:21:27,560
undertake a short practice. Most of all, maybe I can confirm that you're all still there,

226
00:21:27,560 --> 00:21:50,040
are we still connected? Okay. That's what I'd like to do next. So now we'll do a sort of a guided

227
00:21:50,040 --> 00:21:58,440
meditation together. So I assume you're all sitting, and we're going to try to do some sitting

228
00:21:58,440 --> 00:22:03,240
meditation, and we won't go into the walking meditation. I think that's much more difficult

229
00:22:03,240 --> 00:22:10,040
over the internet. Normally we'll put one hand on top of the other, but you're welcome to

230
00:22:10,040 --> 00:22:15,160
sit as you like, some people put their hands out like this. The point is to not make it fancy

231
00:22:15,160 --> 00:22:21,320
or not to put too much emphasis on your posture, on a specific position, like this,

232
00:22:21,320 --> 00:22:27,240
have to sit in the lotus position, have to put your hands up like this, or however. We normally

233
00:22:27,240 --> 00:22:33,240
said like this simply because it's a very basic position. It's one that the Buddha took up,

234
00:22:33,240 --> 00:22:41,720
putting one hand on top of the other because they're then kept inward, closing your eyes,

235
00:22:41,720 --> 00:22:52,760
and sitting in an ordinary cross-legged position. The easiest object for us to take in our

236
00:22:52,760 --> 00:22:57,480
meditation practice is the body, and all of the texts are clear about this, that this is what we

237
00:22:57,480 --> 00:23:03,240
should take first, because as I said, this is how most of us understand reality to be anyway,

238
00:23:03,240 --> 00:23:10,680
and it's how most of our experience is channeled through the body. When you're sitting still,

239
00:23:10,680 --> 00:23:17,480
there's very little for us to take as an object. Now, this is good because we don't want too

240
00:23:17,480 --> 00:23:21,640
many things to be going on at once, but it's also a problem. We have to find something,

241
00:23:21,640 --> 00:23:29,400
at least something to be mindful of. What you should notice, if you're not too tense and not too

242
00:23:29,400 --> 00:23:37,160
stressed, is that when the breath comes into your body, your stomach will rise, maybe just slightly,

243
00:23:37,160 --> 00:23:43,240
but it should rise a little bit, and then when the breath goes out of the body, it will fall.

244
00:23:46,040 --> 00:23:51,800
Rise and fall. This should be natural. Now, if it's not clear, because for many of us,

245
00:23:51,800 --> 00:23:58,600
we're quite tense and stressed through to jobs and due to our obligations and so on,

246
00:23:59,240 --> 00:24:05,400
in the beginning, you can try putting your hand on your stomach, and that'll allow you to

247
00:24:05,400 --> 00:24:12,920
experience the rising and the falling, at least in the beginning.

248
00:24:19,640 --> 00:24:28,760
Now, the way that we're going to go about examining and examining impartially the objects of

249
00:24:28,760 --> 00:24:39,000
our experience is using this mantra, this word that we have. Now, a mantra normally is

250
00:24:39,000 --> 00:24:44,920
understood to be some spiritual, spiritual or mystical word that we use to get in touch with

251
00:24:44,920 --> 00:24:54,840
something outside of ourselves, something external. But the fact of the matter is, a mantra is a

252
00:24:54,840 --> 00:25:01,480
useful tool to focus the mind and to keep the mind with the object. So we use this tool,

253
00:25:02,200 --> 00:25:08,120
but now we bring it back to focus on ourselves, to focus on our own experience.

254
00:25:09,320 --> 00:25:15,640
So when the stomach rises, we're going to use this mantra, this word rising, just a simple word

255
00:25:16,360 --> 00:25:21,320
because that's what it is. And when we affirm it for ourselves that that's what it is,

256
00:25:21,320 --> 00:25:25,960
our mind will be fixed on it, and will be fixed on the reality of it. So we'll come to see that

257
00:25:25,960 --> 00:25:31,800
reality quite clearly, and our mind won't be able to give rise to liking or disliking or judgment

258
00:25:31,800 --> 00:25:37,400
or clinging. So this is what we're going to do with the stomach rises, we're going to say to

259
00:25:37,400 --> 00:25:52,200
ourselves, rise, see. And when it falls, we're going to say to ourselves, fall, I see, fall,

260
00:25:52,200 --> 00:26:08,120
I see, falling, and so on. Maybe you can just try that now with me for a few minutes.

261
00:26:22,200 --> 00:26:44,200
So we're going to take a look at what we're going to do, and then we'll take a look at what we're going to do.

262
00:26:44,200 --> 00:27:10,200
So we're going to take a look at what we're going to do, and then we're going to take a look at what we're going to do.

263
00:27:10,200 --> 00:27:39,480
It's important to understand that we're not trying to force the breath.

264
00:27:40,600 --> 00:27:49,400
Or to control it, to make it long, or to make it smooth, and simply going to watch it when it rises.

265
00:27:49,400 --> 00:27:55,560
We will note that as being rising. Remind ourselves, this is rising.

266
00:27:55,560 --> 00:28:11,480
And falls, we will remind ourselves, this is fall, however it is.

267
00:28:25,560 --> 00:28:52,680
Now as we do this, we will come to see that actually there's much more going on than simply

268
00:28:52,680 --> 00:29:02,440
the rising and falling. This is why we now come to see that we're not only our body, we're not just the physical, because as I said, there are more other parts.

269
00:29:02,440 --> 00:29:12,120
So the next one is the feelings. If we feel happy or pain, pain or calm, we have to acknowledge this as well.

270
00:29:12,120 --> 00:29:26,440
Now we have to include this in our practice, so that we're able to see these experiences as they are as well, not judging or clinging or becoming attached to them either.

271
00:29:26,440 --> 00:29:33,280
So if we feel happy, we should say to ourselves, happy, happy if we feel calm, we should say calm, calm.

272
00:29:33,280 --> 00:29:47,720
If we feel pain, we should focus on the pain, not try to run away or get upset by it, but simply to see it for what it is as pain, say to ourselves, pain, pain.

273
00:29:47,720 --> 00:30:03,240
And as we do this, we'll find that the tension and the stress vanishes, disappears, because now we're no longer upset by it, no longer stressed out about it.

274
00:30:03,240 --> 00:30:16,200
And as we do this, we'll mind what our mind will become clear, and when the pain or the feeling goes away, we come back again to the rising and the falling.

275
00:30:16,200 --> 00:30:29,240
If we're thinking about things, we focus on the thought and say to ourselves, thinking, thinking, as I said, the third one is the mind and whatever the mind is thinking, just see it simply.

276
00:30:29,240 --> 00:30:37,240
We don't have to go into great detail. This isn't a theoretical course, this is practical, so we don't need to understand exactly what kind of thought it is.

277
00:30:37,240 --> 00:30:53,240
There's no point, there's no benefit. Once we see clearly and objectively what something is, this is a thought, it doesn't matter what kind of thought it is a thought, it's a specific type of reality.

278
00:30:53,240 --> 00:31:04,240
When we see that this is reality, it is what it is, that's enough. So we can simply say to ourselves, thinking, thinking, thinking.

279
00:31:04,240 --> 00:31:14,240
And that's easy because it will go away right away. Once it's gone, we come back again to the rising and falling.

280
00:31:14,240 --> 00:31:23,240
And as for the dumb ones, now you can see why I gave you simple names for them, liking, disliking, drowsiness, distraction, and doubt.

281
00:31:23,240 --> 00:31:39,240
Because now we can very easily acknowledge and see clearly these things as well. So when you like something or you want something, you say to yourself liking, liking, when you want something, wanting, wanting.

282
00:31:39,240 --> 00:31:49,240
If you dislike something, disliking, disliking, or angry, or frustrated, or bored, or sad, and so on.

283
00:31:49,240 --> 00:31:57,240
They say to yourself, bored, bored, depressed, or angry, or frustrated, and frustrated.

284
00:31:57,240 --> 00:32:08,240
When you do this, you'll find the objective, the objectivity takes over, and there's no longer this subjectivity or the partiality, the aversion.

285
00:32:08,240 --> 00:32:21,240
And so the anger or the disliking disappears.

286
00:32:21,240 --> 00:32:30,240
If you feel drowsies, it is of drowsy, drowsy, retired, tired.

287
00:32:30,240 --> 00:32:40,240
And if you'll distract and say distracted, distracted, then both of these actually work to balance out the mind. If you feel drowsy, it will give you energy to say drowsy and drowsy.

288
00:32:40,240 --> 00:32:46,240
But if you feel distracted, it will give you concentration to say distracted and distracted.

289
00:32:46,240 --> 00:32:53,240
When you're thinking too much, and your mind is not focused.

290
00:32:53,240 --> 00:33:04,240
And last, when you have doubt, the easiest way to overcome doubt, this is the best way to overcome all doubt, is to simply focus on the doubt, and give up whatever it is you are doubting about.

291
00:33:04,240 --> 00:33:11,240
Say to yourself, doubting, doubting, and then your whole of your practice is focused on your doubt.

292
00:33:11,240 --> 00:33:20,240
And when you do that, you'll find there's nothing to doubt about, because this is doubt, and that's a certainty, and therefore the doubt disappears.

293
00:33:20,240 --> 00:33:29,240
When these are gone, then you'll find your mind is much quieter, it's much more focused, much more stable.

294
00:33:29,240 --> 00:33:33,240
And then you can come back and focus on the stomach again, rising, falling.

295
00:33:33,240 --> 00:33:37,240
When there's nothing else, we just focus on the stomach.

296
00:33:37,240 --> 00:33:53,240
If you see or hear or smell or taste or feel or think the same technique goes, so seeing, seeing, hearing, hearing.

297
00:34:07,240 --> 00:34:27,240
Thank you very much.

298
00:34:27,240 --> 00:34:52,240
Thank you.

299
00:34:52,240 --> 00:35:18,240
Thank you.

300
00:35:18,240 --> 00:35:44,240
Thank you.

301
00:35:44,240 --> 00:36:10,240
Thank you.

302
00:36:10,240 --> 00:36:36,240
Thank you.

303
00:36:36,240 --> 00:37:02,240
Thank you.

304
00:37:02,240 --> 00:37:28,240
Thank you.

305
00:37:28,240 --> 00:37:54,240
Thank you.

306
00:37:54,240 --> 00:38:20,240
Thank you.

307
00:38:20,240 --> 00:38:46,240
Thank you.

308
00:38:46,240 --> 00:39:12,240
Thank you.

309
00:39:12,240 --> 00:39:38,240
Thank you.

310
00:39:38,240 --> 00:40:04,240
Thank you.

311
00:40:04,240 --> 00:40:30,240
Thank you.

312
00:40:30,240 --> 00:40:56,240
Thank you.

313
00:40:56,240 --> 00:41:22,240
Thank you.

314
00:41:22,240 --> 00:41:48,240
Thank you.

315
00:41:48,240 --> 00:42:14,240
Thank you.

316
00:42:14,240 --> 00:42:40,240
Thank you.

317
00:42:40,240 --> 00:43:06,240
Thank you.

318
00:43:06,240 --> 00:43:32,240
Thank you.

319
00:43:32,240 --> 00:43:58,240
Thank you.

320
00:43:58,240 --> 00:44:24,240
Thank you.

321
00:44:24,240 --> 00:44:50,240
Thank you.

322
00:44:50,240 --> 00:45:16,240
Thank you.

323
00:45:16,240 --> 00:45:42,240
Thank you.

324
00:45:42,240 --> 00:46:08,240
Thank you.

325
00:46:08,240 --> 00:46:34,240
Thank you.

326
00:46:34,240 --> 00:47:00,240
Thank you.

327
00:47:00,240 --> 00:47:26,240
Thank you.

328
00:47:26,240 --> 00:47:52,240
Thank you.

329
00:47:52,240 --> 00:48:18,240
Thank you.

330
00:48:18,240 --> 00:48:44,240
Thank you.

331
00:48:44,240 --> 00:49:10,240
Thank you.

332
00:49:10,240 --> 00:49:36,240
Thank you.

333
00:49:36,240 --> 00:50:02,240
Thank you.

334
00:50:02,240 --> 00:50:28,240
Thank you.

335
00:50:28,240 --> 00:50:54,240
Thank you.

336
00:50:54,240 --> 00:51:20,240
Thank you.

337
00:51:20,240 --> 00:51:46,240
Thank you.

338
00:51:46,240 --> 00:52:12,240
Thank you.

339
00:52:12,240 --> 00:52:38,240
Thank you.

340
00:52:38,240 --> 00:53:04,240
Thank you.

341
00:53:04,240 --> 00:53:30,240
Thank you.

342
00:53:30,240 --> 00:53:56,240
Thank you.

343
00:53:56,240 --> 00:54:22,240
Thank you.

344
00:54:22,240 --> 00:54:48,240
Thank you.

345
00:54:48,240 --> 00:55:14,240
Thank you.

346
00:55:14,240 --> 00:55:40,240
Thank you.

347
00:55:40,240 --> 00:56:06,240
Thank you.

348
00:56:06,240 --> 00:56:32,240
Thank you.

349
00:56:32,240 --> 00:56:58,240
Thank you.

350
00:56:58,240 --> 00:57:24,240
Thank you.

351
00:57:24,240 --> 00:57:50,240
Thank you.

352
00:57:50,240 --> 00:58:16,240
Thank you.

353
00:58:16,240 --> 00:58:42,240
Thank you.

354
00:58:42,240 --> 00:59:08,240
Thank you.

355
00:59:08,240 --> 00:59:34,240
Thank you.

