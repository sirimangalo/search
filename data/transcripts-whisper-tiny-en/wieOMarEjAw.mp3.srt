1
00:00:00,000 --> 00:00:06,820
Hi, today I'd like to talk about the progress of insight meditation practice or the meditation

2
00:00:06,820 --> 00:00:15,060
which is for the purpose of creating clear thought or clear awareness of the present reality.

3
00:00:15,060 --> 00:00:19,120
When we practice meditation, as I've said before, we're looking to create a clear thought

4
00:00:19,120 --> 00:00:23,000
or a clear awareness of every moment as it occurs.

5
00:00:23,000 --> 00:00:29,080
But then the question is, what does this create or what does this do for us?

6
00:00:29,080 --> 00:00:33,720
What is the next step or what are the results that we should look for?

7
00:00:33,720 --> 00:00:38,480
Once we've created a clear thought, what is it going to allow us to see?

8
00:00:38,480 --> 00:00:42,040
The practice of meditation allows us to see three things.

9
00:00:42,040 --> 00:00:46,640
Traditionally we talk about the three characteristics of all phenomenon.

10
00:00:46,640 --> 00:00:55,200
All things in the world share three characteristics which are incredibly important for us to come to see, come to understand.

11
00:00:55,200 --> 00:01:02,720
And because this is because there are three opposing characteristics which we think exist in everything in the world around us.

12
00:01:02,720 --> 00:01:09,360
First of all, we think that everything or we think that many things are a permanent or our lasting.

13
00:01:09,360 --> 00:01:14,640
And then we think that many things are happy or are satisfied.

14
00:01:14,640 --> 00:01:21,800
And finally, we think that many things are under our controllable or belong to us.

15
00:01:21,800 --> 00:01:28,120
So thinking that things are permanent, are satisfying and are under our control.

16
00:01:28,120 --> 00:01:34,960
When we look at it, these are three root causes for why we suffer.

17
00:01:34,960 --> 00:01:39,880
Because everything that we attain in this world or obtain in this world,

18
00:01:39,880 --> 00:01:42,440
in the end is subject to impermanence.

19
00:01:42,440 --> 00:01:50,440
It changes. It has to disappear. Whether it be a person, a place or a thing that we hold on to.

20
00:01:50,440 --> 00:01:57,040
And as a result, all of the things which we hold on to are cause for suffering, they're not satisfied.

21
00:01:57,040 --> 00:02:02,840
We can't say that this or that thing is going to satisfy us because it's impermanent.

22
00:02:02,840 --> 00:02:11,720
And finally, we can't say that we can control in any ultimate sense any of the things which we experience.

23
00:02:11,720 --> 00:02:20,040
So the practice of meditation allows us to see this about things very simple things such as the body or such as the mind.

24
00:02:20,040 --> 00:02:24,840
So when we sit in meditation, we come to see that even our own minds we can't control.

25
00:02:24,840 --> 00:02:28,840
We can't stop our minds from thinking, we can't stop our minds from wandering.

26
00:02:28,840 --> 00:02:33,960
And we can't stop our bodies from feeling pain or feeling discomfort at any time.

27
00:02:33,960 --> 00:02:36,920
And we start to see this about really everything around us.

28
00:02:36,920 --> 00:02:42,440
When we live our lives, during the time we're practicing meditation, we go ahead and live our lives.

29
00:02:42,440 --> 00:02:52,440
When we're aware of the situations which we encounter in the workplace or in school or in our daily life,

30
00:02:52,440 --> 00:02:58,440
we come to see that we're not able to control the things around us, including people, places or things,

31
00:02:58,440 --> 00:03:00,840
to be the way we want at all times.

32
00:03:00,840 --> 00:03:11,640
And we start to see that really there's no reason for us to chase after or try to chase away any of the things which we encounter in our daily lives or don't encounter.

33
00:03:11,640 --> 00:03:16,640
So when we don't get what we want, we come to realize that the problem is not that we're not getting what we want.

34
00:03:16,640 --> 00:03:23,640
It's that we're wanting in the first place because we can't control the situation to be anything other than what it is.

35
00:03:23,640 --> 00:03:32,840
We can't take away something which is unpleasant and we can't create something which will be completely pleasant and completely satisfying.

36
00:03:32,840 --> 00:03:44,840
So we come to see things in this way, it allows us to let go and it allows us to be free. It allows us to not cling and to not wish and want all of the time.

37
00:03:44,840 --> 00:03:58,840
Some people come to, when they hear about this teaching, they come to think that perhaps the meditation practice is something which makes life boring or uninteresting or unsatisfying simply because of the attitude or the outlook.

38
00:03:58,840 --> 00:04:09,840
And really we can see that this is mainly due to people's misunderstanding, very deeply ingrained misunderstanding of how one should approach one's life.

39
00:04:09,840 --> 00:04:18,840
We tend to think in everyday terms, we tend to think that chasing after and chasing away is the correct way to live one's life.

40
00:04:18,840 --> 00:04:23,840
When we're always chasing after good things, this is ambition, this is a good thing in life.

41
00:04:23,840 --> 00:04:30,840
But when we really look and study and deeply study reality and start to understand the way things really work,

42
00:04:30,840 --> 00:04:38,840
we come to see that this is actually the cause of suffering, that all of these things which we're chasing after, all of our ambitions, all of our goals are ephemeral,

43
00:04:38,840 --> 00:04:43,840
are not something which can last or not things which can truly satisfy us.

44
00:04:43,840 --> 00:04:50,840
And we can control them, we can't keep them, we can't make them last any longer than they are set to last.

45
00:04:50,840 --> 00:05:00,840
And so, opposed to this, in another way, if we look at life in another way and we'll try to come to live and to be comfortable and content in the present moment,

46
00:05:00,840 --> 00:05:03,840
we come to see that we have another alternative.

47
00:05:03,840 --> 00:05:09,840
And the other alternative is to live a life which is satisfying, which is permanent.

48
00:05:09,840 --> 00:05:19,840
And which is, you might say, free from this lack of control, that in some way we can control things by not controlling.

49
00:05:19,840 --> 00:05:24,840
We're able to control our emotions, we're able to control the way we look at things.

50
00:05:24,840 --> 00:05:33,840
Because we're not chasing, because we're not chasing away, because we're not trying to change things or make things to be always the way we want.

51
00:05:33,840 --> 00:05:42,840
We're able to take anything as it comes, we're able to live a sort of eternal existence, an existence which isn't based on the past, which isn't based on the future.

52
00:05:42,840 --> 00:05:58,840
It isn't based on anything external to ourselves. It's an existence which is pure and free from any sort of greed or aversion or any sort of attachment whatsoever.

53
00:05:58,840 --> 00:06:02,840
It allows us to live unattached like a bird flying through the air.

54
00:06:02,840 --> 00:06:05,840
This is what we're looking for in meditation, we're looking to see.

55
00:06:05,840 --> 00:06:13,840
So when we practice meditation, we're looking to see these three things, to see that the things which we experience are impermanent.

56
00:06:13,840 --> 00:06:17,840
Sometimes we see this and it makes us uncomfortable. We shouldn't feel this way.

57
00:06:17,840 --> 00:06:21,840
We should come to see that this is actually a good thing to be able to see this.

58
00:06:21,840 --> 00:06:28,840
And we should see that things are actually unsatisfying. We shouldn't look for satisfaction in the things external to ourselves.

59
00:06:28,840 --> 00:06:32,840
And also we shouldn't try to force things, even especially when we practice meditation.

60
00:06:32,840 --> 00:06:44,840
Since we're looking to see that things are in and of themselves inherently uncontrollable, then it's very important that we understand we're not to try to control even the very basic objects of meditation.

61
00:06:44,840 --> 00:06:53,840
So I hope this has been of some benefit to help people to understand what it is we're looking for and how the meditation does bring about peace and happiness.

62
00:06:53,840 --> 00:07:03,840
I wish for everyone, thank you for listening and I wish for you all to find peace and happiness for yourselves. Thank you and have a good day.

