1
00:00:00,000 --> 00:00:13,680
Hello and welcome to our... I did it again, didn't I? Hello and welcome to our evening

2
00:00:13,680 --> 00:00:33,120
tonight. Today we're doing question and answer. We have good questions tonight, 12 questions

3
00:00:33,120 --> 00:00:43,240
and I mean we get good questions every week but some of these are quite interesting. I didn't

4
00:00:43,240 --> 00:00:48,040
delete a single question so that's a good sign. There's one question that I'm not quite

5
00:00:48,040 --> 00:00:58,480
clear on but we'll get it. So without further ado, here are 12 questions. Maybe I'll wait a

6
00:00:58,480 --> 00:01:03,520
second and I can say something first.

7
00:01:03,520 --> 00:01:12,040
Waysock is coming up with Dr. Puja, T. Buddha's birthday, which is really universally celebrated

8
00:01:12,040 --> 00:01:22,680
Buddhist holiday. It's in May, the full moon of May and Canada has one of the... I don't

9
00:01:22,680 --> 00:01:39,480
know, it's even Ontario. We have one of the... we have a gink celebration. So I just sent an email

10
00:01:39,480 --> 00:01:44,520
and got an application form so we'll be having a meditation booth there. There's all these

11
00:01:44,520 --> 00:01:54,360
cultural displays. Every temple has a display of culture which really takes up most of the

12
00:01:54,360 --> 00:02:01,320
afternoon. It's that you might think it odd if you're not a cultural Buddhist. Why in a

13
00:02:01,320 --> 00:02:09,400
celebration of the Buddha's birthday are they dancing and singing and doing kung fu. And I think

14
00:02:09,400 --> 00:02:15,720
it's an argument that he made for the devotional acts. Sri Lanka has an interesting devotional

15
00:02:15,720 --> 00:02:22,040
acts. Every morning in the temp monastery where I was staying in Colombo and throughout

16
00:02:22,040 --> 00:02:28,000
the country, they make noise in front of the Buddha and it's really a cacophony of sounds,

17
00:02:28,000 --> 00:02:35,320
not pleasing to the air at all. So it... it removes that aspect of it. I think it's a really

18
00:02:35,320 --> 00:02:40,520
interesting tradition. They... one guy's beating Brahms and the other one is playing some kind of

19
00:02:41,880 --> 00:02:47,400
flute and they're just looking noise as far as I can tell. Maybe I just have the wrong ear for it.

20
00:02:47,400 --> 00:02:54,200
But there's a sense that it's... they've solved the problem of things like dancing and singing

21
00:02:54,200 --> 00:02:58,840
because you're doing it as a tribute for the Buddha, but didn't the Buddha teach that dancing and

22
00:02:58,840 --> 00:03:04,360
singing or craziness? Didn't he teach that dancing was craziness and singing is just like

23
00:03:04,360 --> 00:03:12,760
willing? He did. In fact, there's a suit to where he says that. So when you make... no, but when you

24
00:03:12,760 --> 00:03:20,280
make noise, you... it's a way of token offering. It might seem silly if you're just in the Buddhism

25
00:03:20,280 --> 00:03:27,000
for the meditation, but to understand that many people are not into meditation and yet they want

26
00:03:27,000 --> 00:03:36,040
to better themselves. They're trying to better themselves. There are many ways of bettering yourself.

27
00:03:36,040 --> 00:03:41,640
A lot of the questions tonight are about ways to better yourself. Those are interesting ones.

28
00:03:43,160 --> 00:03:48,920
So let's go ahead and start them. If the buttons actually work, there we go.

29
00:03:48,920 --> 00:03:58,360
So here's a classic question. This one is a common question and problem. As soon as I acknowledge

30
00:03:58,360 --> 00:04:04,200
thinking, the thought fleas, as if I'm repressing it, it seems I'm unable to see the thought

31
00:04:04,200 --> 00:04:11,800
all the way through as it's a problem. So you want to observe without using the keyword.

32
00:04:11,800 --> 00:04:19,560
So for example, with sound you're able to acknowledge hearing hearing and sound persists.

33
00:04:21,880 --> 00:04:27,240
So an argument could be made that the sound isn't actually persisting, but there's repeated sounds.

34
00:04:28,280 --> 00:04:35,000
Sound is still moments of experience. You're not always hearing the sound, but it's very quick and

35
00:04:35,000 --> 00:04:42,360
hard to see that. In fact, that's really the benefit of what you're describing as a benefit of

36
00:04:42,360 --> 00:04:48,440
focusing on the mind. The mind is what allows us to see impermanence. There are four

37
00:04:49,480 --> 00:04:57,000
perversions of perception, beauty, which we counter or overcome by mindfulness of the body,

38
00:04:57,000 --> 00:05:03,880
happiness, which we overcome or counter by mindfulness of the feelings,

39
00:05:04,520 --> 00:05:08,840
the perversion of permanence, which we overcome by practicing the mindfulness of the mind,

40
00:05:10,040 --> 00:05:15,640
and perversion of self, which we overcome by mindfulness of the dumbness, the hindrance, and so on.

41
00:05:16,440 --> 00:05:22,360
So the mind moves so quickly that it helps you see, break through this concept of continuity,

42
00:05:22,360 --> 00:05:29,800
the idea of a continuous self, or a canoe, a permanent entity, or anything being permanent.

43
00:05:30,840 --> 00:05:34,840
So that you're seeing the mind disappear and disappear is really what we want to see.

44
00:05:34,840 --> 00:05:41,560
You want to observe repeatedly that until your mind becomes clearly aware that this is the nature

45
00:05:41,560 --> 00:05:47,720
of reality. Its experience is arising and seizing. There's nothing wrong with it, and we're not

46
00:05:47,720 --> 00:05:54,200
trying to see things through or really go deeply into them. Mindfulness is just to remind yourself,

47
00:05:54,760 --> 00:06:00,440
when we say mindfulness, a better translation might be reminding. So

48
00:06:02,520 --> 00:06:09,160
sati means to be able to remember or in a sense remind yourself, or if you remind yourself,

49
00:06:09,160 --> 00:06:15,720
then you'll remember. You remember that it's only thinking, but it disappears like that,

50
00:06:15,720 --> 00:06:20,040
well, that's what you're seeing. Well, yeah, that's the nature of thoughts to arise and see,

51
00:06:20,040 --> 00:06:25,720
it's not repressing. There's no such thing really as repressing. Repressing is sort of a

52
00:06:25,720 --> 00:06:33,400
reaction of dislike. That's the only time you talk about repressing, but reacting is what it

53
00:06:33,400 --> 00:06:37,640
bears. So when you say thinking, you didn't react to it, it disappeared, that's fine, it's gone.

54
00:06:37,640 --> 00:06:48,040
I also become the idea of eye and permanence. So how do you see instability?

55
00:06:55,080 --> 00:07:01,080
It's uncomfortable, and it's disturbing, I think, because you want something more lasting and

56
00:07:01,080 --> 00:07:09,160
stable. We want to disrupt, not to disturb mind, so that the mind wakes up, really. If you don't

57
00:07:09,160 --> 00:07:18,200
get disturbed, you'll never wake up in a long sleep and need a bit of harm to shake to wake us up.

58
00:07:20,920 --> 00:07:24,840
I think anti-psychotic medication and I'm mostly doing some at the practice,

59
00:07:24,840 --> 00:07:29,320
with hope that it will strengthen my mind, so I may be able to practice with us in a future life.

60
00:07:29,320 --> 00:07:35,320
I hope I didn't give the idea that you can't practice with us and know when you're on things

61
00:07:35,320 --> 00:07:43,960
like anti-psychotics. It's just not really likely that you're going to become enlightened, let's

62
00:07:43,960 --> 00:07:50,760
say, well on anti-psychotics. It's harder because you're not able to fully experience the results

63
00:07:50,760 --> 00:07:58,840
and the nature of your reactions to things because you're repressing it. So it's harmful to your

64
00:07:58,840 --> 00:08:05,240
progress, but it doesn't make your practice impotent. If you're able to practice summit and you're

65
00:08:05,240 --> 00:08:13,400
able to practice with us and you certainly should, just being patient if you feel the need or if you

66
00:08:13,400 --> 00:08:21,080
have the need to take anti-psychotics and mindfulness won't hurt you. It's not that even that,

67
00:08:21,080 --> 00:08:29,080
it will own better you. It's just a question of how much. I wouldn't limit yourself to summit off the

68
00:08:29,080 --> 00:08:34,680
top of my head, it doesn't seem right to. Now, individual situations, I don't know. It's certainly

69
00:08:34,680 --> 00:08:42,040
possible that I might be wrong for your instance, but it seems to me that we pass in a mindfulness.

70
00:08:42,040 --> 00:08:46,760
We don't practice, we pass in, we practice mindfulness, which leads to we pass in.

71
00:08:46,760 --> 00:08:54,200
So if you want to see clearly, just be some mindfulness, it'll help you see a little bit more clearly

72
00:08:54,200 --> 00:09:00,840
about yourself. What do you think if someone met it, it's most of the time, even when in

73
00:09:00,840 --> 00:09:07,880
interacting with people, is that okay? I think it's fine. You know, some people might not think

74
00:09:07,880 --> 00:09:12,840
it's fine and you might lose some friends who think you've changed because your behavior certainly

75
00:09:12,840 --> 00:09:17,880
will change if you're mindful all the time. Certain people might not want to have anything to do

76
00:09:17,880 --> 00:09:22,600
with you because you don't resonate with their developments, with their attraction, with their

77
00:09:22,600 --> 00:09:30,440
excitement, their desire for excitement and so on. But, you know, certainly, I mean, actually

78
00:09:30,440 --> 00:09:36,680
being mindful during your interactions, it was a great power, a great benefit because so much

79
00:09:36,680 --> 00:09:43,640
reacting goes on. There's so much potential for reacting during the interaction of fear or out

80
00:09:43,640 --> 00:09:49,320
of worry, out of arrogance, ego and so on. Mindfulness can only purify all of that.

81
00:09:53,000 --> 00:09:58,360
What's the best way to cure anxiety, worry and restlessness? Okay, and then you talk about

82
00:09:58,360 --> 00:10:04,440
some of the your own situation of tapping your foot. And then something that's actually point out

83
00:10:04,440 --> 00:10:09,880
different. You're being in your head and re-evaluating how you're seen by others, being self-conscious.

84
00:10:09,880 --> 00:10:17,720
It leads to anxiety. But being self-conscious is ego. This is a clear example of how our ego, our

85
00:10:17,720 --> 00:10:27,000
delusion, our belief in self and our conceit about that self. They feed our suffering or stress.

86
00:10:27,000 --> 00:10:36,520
And feeds our defilements. And so, worry and restlessness, worry, anxiety. They are exacerbated

87
00:10:36,520 --> 00:10:41,640
by this belief in self and fed by it. If you don't have any concern for how people look at you,

88
00:10:42,360 --> 00:10:46,600
this is why I would say we're worthless. And if you think like you're worthless, it really

89
00:10:46,600 --> 00:10:51,880
counteracts that. It makes you think, oh, I have no more pressure to be worth anything anymore.

90
00:10:51,880 --> 00:11:00,200
I can let go, because so much of our energy is spent on worrying about what other people think,

91
00:11:00,200 --> 00:11:07,240
which is such a ridiculous, ridiculous way of living. I mean, I say that not judgmental,

92
00:11:07,240 --> 00:11:11,960
because I know how it is. You worry, people criticize you and you get upset.

93
00:11:13,880 --> 00:11:17,240
That's some real trap that we've cut ourselves in.

94
00:11:17,240 --> 00:11:25,960
It's a great power of mindfulness, the ability to experience anything, even people's judgment of

95
00:11:25,960 --> 00:11:38,040
you without reacting to be. So try and see that as something maybe worse or more important,

96
00:11:38,040 --> 00:11:50,120
in fact, than during your anxiety, your need, your concern over your identity. Because anxiety,

97
00:11:50,120 --> 00:11:53,800
anxiety is really something that's not that difficult once you get the right technique.

98
00:11:54,600 --> 00:12:00,120
You won't get rid of it, but you certainly can understand the way of changing it

99
00:12:00,120 --> 00:12:06,520
and slowly work on it. It takes time to actually change it, but you can pretty quickly gain

100
00:12:06,520 --> 00:12:12,360
the method. It's just to be mindful of it, to note anxious, anxious, and to break it into pieces.

101
00:12:12,360 --> 00:12:18,040
There's the anxiety, there's the tension in the body. It's a tense, tense. There's the heart

102
00:12:18,040 --> 00:12:24,600
beating, sort of feeling. There's thoughts and worries that lead to anxiety, you can note the thinking.

103
00:12:28,840 --> 00:12:35,800
But your concern over what people think of you is something separate, and it's feeding it

104
00:12:35,800 --> 00:12:43,000
as you can tell, I think. And so you have to be, I think some reflections do to make you

105
00:12:43,000 --> 00:12:49,320
remind yourself, you know, that's not the way to live, and it's a bad way to the cause for stress

106
00:12:49,320 --> 00:12:55,080
and suffering. And then, did you be mindful of your worries and your concerns of the thoughts about

107
00:12:55,080 --> 00:13:02,600
what people think of you and so on? Not get caught up in them. Here's a question which is

108
00:13:02,600 --> 00:13:08,040
borderline, I don't know, it's a response to one of my answers, and I just don't like getting

109
00:13:08,040 --> 00:13:16,120
very technical and theoretical because here's the thing, I had explained that consciousness is

110
00:13:16,120 --> 00:13:25,480
individual and linear for one sentient being to another. But then you have this logic, which is

111
00:13:25,480 --> 00:13:32,920
problem often, because logic, you have to prove it and then prove his sketchy. So you say that

112
00:13:32,920 --> 00:13:37,880
would imply the total number of sentient beings in any given moment must remain constant.

113
00:13:40,680 --> 00:13:48,120
Yes, okay, I think that makes sense. But I didn't actually suggest, I don't think, and it doesn't

114
00:13:48,120 --> 00:13:53,960
actually suggest that there couldn't be a spontaneous arising, a new consciousness. I don't think

115
00:13:53,960 --> 00:14:00,760
that happens. I don't think it's considered to happen that way according to Buddhism. But anyway, I mean,

116
00:14:01,480 --> 00:14:07,160
whatever is okay, total number of sentient beings are given. I think the idea is that it's infinite,

117
00:14:07,160 --> 00:14:12,840
really, there's an infinite number of beings. And then you say, which is a really curious

118
00:14:12,840 --> 00:14:19,640
statement, that it is scientific fact. Anything that starts with its a scientific fact is already

119
00:14:19,640 --> 00:14:27,320
sketchy. Are there any scientific facts? Give me one scientific fact. I don't think there's a single

120
00:14:27,320 --> 00:14:38,600
one, right? Science doesn't work in facts. Observations and theories based on, I mean, gravity is

121
00:14:38,600 --> 00:14:45,560
still a theory. Doesn't mean it's not, it doesn't mean it's not true. It just means it can't be

122
00:14:45,560 --> 00:14:50,840
proven as a fact. It can't be. It's not the nature of science as far as I understand it. So,

123
00:14:51,480 --> 00:14:56,440
okay, what's your scientific fact? But it's even hotter than that because you say the total number

124
00:14:56,440 --> 00:15:04,680
of sentient beings is far from constant. How would you know? Okay, I get it. If you're dealing only with,

125
00:15:04,680 --> 00:15:10,920
you say sentient beings are all the humans and animals on the planet, then that's far from constant.

126
00:15:10,920 --> 00:15:17,160
But that's not what we're saying. The sentient sentience goes far beyond the human in the

127
00:15:17,160 --> 00:15:23,480
animal realms and humans are becoming animals, animals are becoming humans, and both are becoming

128
00:15:23,480 --> 00:15:31,160
very different beings, angels, gods, elbings, ghosts, all sorts of categories of beings,

129
00:15:31,160 --> 00:15:40,920
you know, any sort of beings, many different kinds of sentience. So, I challenge that theory.

130
00:15:41,800 --> 00:15:52,440
It's not fact anyway, but you can say it's observable, but it's not even observable because you

131
00:15:52,440 --> 00:15:57,640
can't even observe a single sentient being. You can't even observe the sentence of a single being.

132
00:15:57,640 --> 00:16:12,680
But I get what you could argue that it's observable that the number of animal life forms

133
00:16:13,960 --> 00:16:20,280
on earth is variable, but that's not what we're limited to.

134
00:16:20,280 --> 00:16:30,840
So, it's a kind of, you can argue that it's scientific observation and it's hard to doubt that,

135
00:16:30,840 --> 00:16:39,160
but it's not related to the idea of sentience. Okay, then another criticism, which is valid

136
00:16:40,680 --> 00:16:46,280
objection. The idea of an individual consciousness that migrants unchanged from one life form to

137
00:16:46,280 --> 00:16:51,800
another seems to be in context for the concepts of impermanence and non-self. So, okay,

138
00:16:51,800 --> 00:16:56,520
there's no such thing as an individual consciousness that migrate unchanged from one life form to

139
00:16:56,520 --> 00:17:02,280
another. I'm sorry that that impression was given because that wasn't the impression that

140
00:17:02,280 --> 00:17:08,200
was intended. There's a stream of consciousness. I mean, if you think about it conceptually,

141
00:17:08,200 --> 00:17:13,000
you're thinking of these streams and these lines, but that's not a reality. Reality is, hey,

142
00:17:13,000 --> 00:17:20,040
look at this. This is reality. I am one reality. This is reality happening. And this reality is

143
00:17:20,040 --> 00:17:24,600
linear, right? There's no branching off. Oh, look, I'm going in two directions at once. It doesn't

144
00:17:24,600 --> 00:17:31,880
happen like that. It's not possible for it to happen that way. I mean, you could theorize, hey,

145
00:17:31,880 --> 00:17:36,360
what if there is two of me going this way? And I'm only experiencing one of them. The fact that

146
00:17:36,360 --> 00:17:40,920
you're only experiencing one of them means there was only one of them. Anything that you could

147
00:17:40,920 --> 00:17:47,480
theorize or conjecture about by definition is not real, by Buddhist definition, because our

148
00:17:47,480 --> 00:17:58,120
definition of real is the experience. The only experiencing a linear consciousness, but it's one

149
00:17:58,120 --> 00:18:05,240
consciousness after another. Each consciousness arises and ceases. But it's linear. I'm not becoming

150
00:18:05,240 --> 00:18:12,440
lots of people and I'm not just becoming fewer people than one person, one stream of consciousness.

151
00:18:15,400 --> 00:18:20,760
But that's descriptive. What's actually happening is that's conceptual. It's actually

152
00:18:20,760 --> 00:18:27,000
happening as moments of experience one after the other. And apparently that's happening for

153
00:18:27,000 --> 00:18:38,360
all of you as well, but I can't prove it. Besides noting the precepts, is there anything else

154
00:18:38,360 --> 00:18:45,240
that helps create conditions for we pass on it to arise? So there's there's lots of things.

155
00:18:45,240 --> 00:18:50,680
Maintaining smile while noting doesn't seem to me at all useful. In fact, it's highly problematic

156
00:18:50,680 --> 00:18:59,160
because it's it's it's artifice. You're you're it's construct. You're you're

157
00:18:59,160 --> 00:19:04,920
purposely trying to maintain a smile, which has been a cultivate very bizarre habits. I think

158
00:19:04,920 --> 00:19:12,040
possibly harmful ones where you pretend that you're happy or so. But things that there are many

159
00:19:12,040 --> 00:19:17,000
things that support insect meditation. I think people are always looking for this, you know,

160
00:19:17,000 --> 00:19:21,480
it's heart, insect meditation is hard. So what can I do that's going to support? And there's a lot.

161
00:19:22,680 --> 00:19:26,840
Give me the precepts a great thing being mindful during your daily life is a great thing.

162
00:19:28,280 --> 00:19:33,720
But there are others, you know, mindfulness of the Buddha. There are four things that I would say

163
00:19:34,360 --> 00:19:39,000
rise above anything else you could do. And that is mindfulness of the Buddha, so doing

164
00:19:39,000 --> 00:19:46,040
andting in front of the Buddha image once a day is a great institutional example of that.

165
00:19:47,080 --> 00:19:55,880
Mindfulness of of death, mindfulness of the low sameness of the body and made that.

166
00:19:57,000 --> 00:20:02,760
These four are four types of meditation that sort of support our insect meditation practice,

167
00:20:02,760 --> 00:20:07,560
especially when you're not in a meditation center, it's there are things you need to sort of guard your

168
00:20:07,560 --> 00:20:15,480
practice. Those four are I think useful. Other things, I mean, not working too much, not sleeping too

169
00:20:15,480 --> 00:20:21,640
much, not eating too much. There's lots of things that get in the way of your practice, eating too

170
00:20:21,640 --> 00:20:37,560
much, let's not know, sleeping too much, eating too much, working too much, socializing too much,

171
00:20:38,760 --> 00:20:45,080
and talking too much. Those five, not guarding your senses, I think it's the sixth.

172
00:20:45,080 --> 00:20:52,360
There may be one more, I say working, working, sleeping, eating,

173
00:20:58,040 --> 00:20:58,680
things like that.

174
00:21:03,320 --> 00:21:08,200
How much progress can one expect when being guided? Okay, questions that start, how much

175
00:21:08,200 --> 00:21:17,400
progress can one expect? I mean, just stop there first because expectations of progress

176
00:21:18,440 --> 00:21:25,160
are sketchy. There's so many variables, but I hope you can realize that there's other variables

177
00:21:25,160 --> 00:21:31,720
involved, your own, what you bring to the table is going to matter much more, or it's

178
00:21:31,720 --> 00:21:39,560
going to matter a lot in how quickly you practice. Being guided by a late teacher who doesn't

179
00:21:39,560 --> 00:21:47,240
keep the precepts, still drinks alcohol. That is a factor. If that late person comes to your

180
00:21:47,240 --> 00:21:53,640
drunk as a skunk, but recites the Buddha's teaching on mindfulness for you, you could take that

181
00:21:53,640 --> 00:21:59,480
information and become enlightened if you're bringing enough to the table. If your state of

182
00:21:59,480 --> 00:22:06,280
mind is such, they are ready to realize the teachings. On the other hand, the teacher who's not drunk

183
00:22:06,280 --> 00:22:11,000
would have a better chance of providing not only good information but encouragement

184
00:22:12,440 --> 00:22:17,800
and how do you feel comfortable? I mean, especially if you're not bringing so much, a good

185
00:22:17,800 --> 00:22:29,640
teacher can help to encourage you. At late teacher, there's much more likely a little, start with the

186
00:22:29,640 --> 00:22:34,520
alcohol, a person drinking alcohol is much more likely to give you the wrong advice, especially if

187
00:22:34,520 --> 00:22:38,520
they're drinking alcohol when they're giving the advice, but even if they're not, the fact that

188
00:22:38,520 --> 00:22:43,880
they drink alcohol is a sign that their advice they give may be wrong. It doesn't not necessarily

189
00:22:43,880 --> 00:22:49,640
wrong, it might be completely right. The fact that they're a late person, I think, had less

190
00:22:49,640 --> 00:22:55,400
to do with it, but a late person, it's a good indication that they might be more engaged in

191
00:22:56,600 --> 00:23:04,520
bad things, at least sensuality, they're much more likely to be having sex or romance or entertainment,

192
00:23:04,520 --> 00:23:12,040
that sort of thing. But it doesn't mean the late person often, there have been many late people

193
00:23:12,040 --> 00:23:17,720
who taught far better than most monks, so don't just think because some of us are day in there

194
00:23:18,440 --> 00:23:25,480
automatically lives. It's clinging a rise out of fear or hope from experience.

195
00:23:27,800 --> 00:23:33,800
One question, two questions here. It's clinging a rise out of fear or hope from experiences.

196
00:23:35,000 --> 00:23:41,480
Cleaning a rise is of many things, mainly of ignorance. The main reason why we cling in it's going to

197
00:23:41,480 --> 00:23:45,720
sound very general, but the main reason why we cling is because we don't understand that the things

198
00:23:45,720 --> 00:23:53,560
we're clinging that we're not willing to. There's many reasons why we become clingy. We generally

199
00:23:53,560 --> 00:23:57,800
we think that the thing that we're clinging to is going to make us happy in some way or another.

200
00:24:01,560 --> 00:24:07,240
But it sounds a little bit specked out of that question, I'm not sure what the point of it is.

201
00:24:07,240 --> 00:24:15,160
Okay, so when watching phenomenon and noting disliking, disliking, would it work to an anger, anger,

202
00:24:15,160 --> 00:24:25,720
it's true what anger is like, or just knowing what it is as anger, you know, angry, angry,

203
00:24:25,720 --> 00:24:33,560
or disliking, disliking, another one. Doesn't matter which one, but both are good. Depends,

204
00:24:33,560 --> 00:24:38,120
I mean, sometimes it doesn't feel like anger too, it just feels like disliking, so disliking would be

205
00:24:38,120 --> 00:24:44,440
better, but it's not a big deal. There's a rose one. Oh, I guess I probably should have

206
00:24:44,440 --> 00:24:47,800
removed that one because it's a good question, but it's not a question for us.

207
00:24:51,640 --> 00:24:55,880
And this is the one that I don't really understand. So you're asking

208
00:24:55,880 --> 00:25:04,920
repression and aversion, are they relative to the amount of delusion that we have? Is there a

209
00:25:04,920 --> 00:25:10,360
correlation between the amount of delusion that we have and the amount of repression and aversion?

210
00:25:11,560 --> 00:25:15,960
I mean, I think you could make such a correlation, but again, this is speculative. I mean,

211
00:25:15,960 --> 00:25:19,320
what's the point of asking this question? Again, if you have delusion, that's something you have

212
00:25:19,320 --> 00:25:25,720
to get rid of, you have repression and aversion, also something you have to get rid of. I mean,

213
00:25:25,720 --> 00:25:30,680
ultimately ignorance, which falls under delusion, is what we're trying to do away with an insight,

214
00:25:31,560 --> 00:25:39,880
but as for them being measurably the same, I don't know. I think that's a bit speculative.

215
00:25:41,480 --> 00:25:45,800
Here's a question, do you have any suggestions on how to do death meditation?

216
00:25:45,800 --> 00:25:54,200
Yeah, death meditation on death is, so again, I said mindfulness of the Buddha, mindfulness of death,

217
00:25:54,200 --> 00:26:01,080
mindfulness of the Lord'sness and Mehta. So death is one of the four. I do one amazing way down. My

218
00:26:01,080 --> 00:26:10,680
life is unstable. Only death is a stay is sure. Life is unsure that is sure. And do you

219
00:26:10,680 --> 00:26:20,760
be done, Mehta? My life is impermanent, unpredictable. Only death is predictable. You can predict

220
00:26:20,760 --> 00:26:27,400
that you're going to die, or certain maybe life is uncertain, but death is certain.

221
00:26:29,560 --> 00:26:33,880
Do you remind yourself you just sit and repeat that to yourself thinking about death that life is

222
00:26:34,680 --> 00:26:38,840
unstable. Death is a certainty. It's coming at the end of my life, but at the end of my life,

223
00:26:38,840 --> 00:26:44,280
I will die. There's no question all the many things that might have in my life. I know one of them

224
00:26:44,280 --> 00:26:50,920
for sure. I'm going to die. I may not get rich. I may not get famous. I may not have kids. I may not

225
00:26:52,120 --> 00:27:01,480
this or that, but I know that I will die for sure. For rich or poor, one end is sure. No.

226
00:27:01,480 --> 00:27:08,680
The young, the old, the young, the old, the middle and the years, the fool and eeked wives.

227
00:27:13,480 --> 00:27:19,480
For rich or poor, one end is sure. Each man among them buys. It's a jotic verse.

228
00:27:19,480 --> 00:27:32,280
Man's I didn't English poetry. I'm at a very high level of understanding. It will be careful.

229
00:27:33,480 --> 00:27:38,920
That's good to hear, but it's easy to overestimate yourself. You should never try to

230
00:27:39,560 --> 00:27:44,600
estimate your level of understanding. Try to work on what you have left to do.

231
00:27:44,600 --> 00:27:52,600
I feel like I am the one person who actually really gets what you're saying. That's unfortunate.

232
00:27:52,600 --> 00:27:58,360
I thought a lot of people were getting something out of what I was saying. It would be unfortunate

233
00:27:58,360 --> 00:28:05,320
if you were the only one, but I am glad that you understand something from it. I need practice and

234
00:28:05,320 --> 00:28:11,640
I need to physically align myself with the way. We have opening to come here. You're welcome to come

235
00:28:11,640 --> 00:28:18,600
and do a meditation course at this. You can look on our website. There's seremungalow.org. There's

236
00:28:18,600 --> 00:28:23,560
an application form. You apply. You come. It's all free. If you get here, we'll teach you.

237
00:28:26,280 --> 00:28:31,480
But please don't hold on to the idea that you're some high and mighty enlightened being,

238
00:28:31,480 --> 00:28:35,880
because if you are, don't come and see me. You don't need my help. But if you do, you have to

239
00:28:35,880 --> 00:28:41,720
be open to the fact that you still have things that need to be improved, because we're here. Here

240
00:28:41,720 --> 00:28:47,080
we're all about improvement, not about gloating over how good you are. Not that I'm suggesting

241
00:28:47,080 --> 00:28:56,280
that that's what you're doing, but just be clear. See these questions came went through. I don't

242
00:28:56,280 --> 00:29:03,080
think this is really all that important. Once the prepare for life is among again, I'm not really keen

243
00:29:03,080 --> 00:29:09,400
on answering those questions here. I know a lot of people want to become monks, but it's a bit off

244
00:29:09,400 --> 00:29:13,400
the mark, because these are supposed to be questions of a meditation. I don't want to distract

245
00:29:13,400 --> 00:29:19,960
by talking about ordination. Ordination at this point in my life, my relationship with

246
00:29:19,960 --> 00:29:26,200
ordination is close students of mine who want to ordain we talk about it or who are open to it.

247
00:29:26,200 --> 00:29:32,120
They've done quite a bit of meditation with me. It would be great if they could find a way to

248
00:29:32,120 --> 00:29:38,680
ordain that kind of thing. I don't know you. I'm not really, it's not really a conversation

249
00:29:38,680 --> 00:29:49,000
that I'm interested in. Sorry. So those are the questions for tonight. Those are some

250
00:29:49,000 --> 00:30:00,520
good answers or useful answers. Thank you all for tuning in. We'll see you all in the best.

