1
00:00:00,000 --> 00:00:27,600
Okay, good evening everyone, welcome to our weekly

2
00:00:27,600 --> 00:00:44,480
the dumb session where we try to find something that I think is beneficial for specific

3
00:00:44,480 --> 00:00:54,960
people who are interested in the cultivation of mindfulness and insight and peace,

4
00:00:54,960 --> 00:01:15,760
happiness. Freedom from suffering. So tonight we're looking at the topic of declaring or

5
00:01:15,760 --> 00:01:34,480
affirming one's enlightenment, one's level of attainment. It's an important part of the practice,

6
00:01:34,480 --> 00:01:39,760
so it's something that's important to talk about and it brings up at least a couple of important

7
00:01:39,760 --> 00:01:49,760
issues. The most important issue that it brings up is what we call reification. It's a really

8
00:01:49,760 --> 00:01:56,720
good English word, I don't think we use it enough. Reification, as I understand the word

9
00:01:56,720 --> 00:02:07,360
to mean and how I'm going to use it, is when you affirm the existence of something, you affirm

10
00:02:07,360 --> 00:02:15,760
something to be, you basically call it into existence with your mind. It's a very important part

11
00:02:15,760 --> 00:02:32,080
of our cognitive process, reification. We tend to reify this place as a meditation center.

12
00:02:32,080 --> 00:02:39,360
In fact, it's just a house, and you know, it's not even a house, that's a reification itself.

13
00:02:45,920 --> 00:02:51,280
Then you really realize that anything you come up with, say, well, that was

14
00:02:51,280 --> 00:02:56,480
mere walls and a floor, but even the walls aren't really walls.

15
00:02:56,480 --> 00:03:01,680
All the time our mind is constantly creating

16
00:03:01,680 --> 00:03:17,200
content due to form an idea and to categorize and to relate to our experiences

17
00:03:18,480 --> 00:03:26,400
in incredibly complex ways. If we compare the way our cognitive process works,

18
00:03:26,400 --> 00:03:35,040
to that of the Buddha and how the Buddha described reality. Again, and again, reminding us,

19
00:03:35,040 --> 00:03:38,240
it's only seeing, hearing, smelling, tasting, feeling, thinking.

20
00:03:40,880 --> 00:03:49,120
A really enlightened being is aware of that. It's clear about the reality that this is seeing,

21
00:03:49,120 --> 00:03:56,720
hearing, this is hearing, this is smelling. Whereas an ordinary experience, the reality is always

22
00:03:56,720 --> 00:04:04,080
as a person. This is a thing, that thing, and much more importantly, good, it's bad, it's me,

23
00:04:04,080 --> 00:04:11,040
it's mine, I like it, I don't like it, and so much. So, we problems. Now, the problem with this,

24
00:04:11,040 --> 00:04:19,680
the reason why it creates problems is, well, basically they're not, these things are real,

25
00:04:21,120 --> 00:04:27,200
but more specifically or more clearly what the problem is, is that because they're not real,

26
00:04:27,760 --> 00:04:36,640
they lend themselves to conceptions of permanence, stability, satisfaction.

27
00:04:36,640 --> 00:04:44,480
It can be satisfied by concepts, or you can have the delusion that you're being satisfied

28
00:04:44,480 --> 00:04:53,600
by concepts, that they're satisfied. The person, for example, we have my cousin, who's a

29
00:04:53,600 --> 00:05:00,400
user, poor or necklace, he's now being used as my Dharma example, now that he's died,

30
00:05:00,400 --> 00:05:10,320
he died, he died, and I guess it's a year now, is it? No, did he just die? He died.

31
00:05:14,320 --> 00:05:20,400
Pretty sure it was a year ago, boy time flies. Maybe it was just this January, what is it,

32
00:05:20,400 --> 00:05:33,120
it's March, no concept of past, was either this January or last January, and of course,

33
00:05:33,120 --> 00:05:41,280
this was incredibly upsetting for his parents, particularly his sister as well,

34
00:05:41,280 --> 00:05:53,920
because they're a conception of this person and their attachment to it.

35
00:05:56,400 --> 00:06:02,720
Our loved ones are things that we depend upon and we really love, we really appreciate,

36
00:06:02,720 --> 00:06:12,240
but what the truth is is that we're really attached to, there's a sense of this person, and so

37
00:06:12,240 --> 00:06:20,080
there's of course this utter despair when you lose, because it's on a very deep level,

38
00:06:20,720 --> 00:06:25,280
it goes against what you think should be, or what you believe to me.

39
00:06:25,280 --> 00:06:36,320
So they lend themselves a clinging, because they appear and give the appearance of permanence,

40
00:06:36,320 --> 00:06:44,320
satisfaction, and possessability, or controllability, or all these things associated with self,

41
00:06:45,840 --> 00:06:50,640
these three things being the exact opposite of what reality is, reality is,

42
00:06:50,640 --> 00:06:56,560
impermanent and untatisfying, and uncontrollable and predictable and dependable,

43
00:06:57,760 --> 00:07:00,880
unpossessable, you can't say it's me or mine.

44
00:07:05,760 --> 00:07:11,360
And unfortunately, all of the concepts, all of these things, the house, the person, the meditation

45
00:07:11,360 --> 00:07:21,520
center, they're all dependent upon reality, and because they're really just a mishmash of experience,

46
00:07:22,160 --> 00:07:29,200
when I look, I see this enter, when I feel, I feel wall, but it's just an experience and these

47
00:07:29,200 --> 00:07:32,400
experiences are unpredictable, undependable.

48
00:07:32,400 --> 00:07:41,680
So how does that relate to declaring enlightenment?

49
00:07:46,480 --> 00:07:51,360
It doesn't directly relate to Soota, but it's very important for us to talk about.

50
00:07:52,080 --> 00:07:55,920
A person, no, it does relate to the Soota, but it does mention, but there's more to it,

51
00:07:55,920 --> 00:08:04,240
but two kinds of people. So Soota Akhata is this guy, he comes to the Buddha, and he's heard that

52
00:08:04,240 --> 00:08:12,400
there's a bunch of monks who have declared enlightenment in front of the Buddha. They came to the

53
00:08:12,400 --> 00:08:17,760
Buddha and said, we're enlightened. So he asked the Buddha, he said, does everyone do

54
00:08:17,760 --> 00:08:27,200
declares this? Are they all enlightened? Buddha said, well, of course, some are and some aren't.

55
00:08:27,200 --> 00:08:36,160
And so then Soota Akhata is going to explain and he does something, so we'll back up a little

56
00:08:36,160 --> 00:08:44,800
bit before we get into explaining all that stuff I just talked about. The Buddha describes a

57
00:08:44,800 --> 00:08:52,160
several types of people. I think it's useful. It's for us to understand because

58
00:08:54,800 --> 00:08:58,880
it's important to understand that just because you think you're enlightened doesn't mean you are.

59
00:09:00,960 --> 00:09:08,480
I suppose it goes without saying or shouldn't go without saying, but I get often or from time to time

60
00:09:08,480 --> 00:09:18,000
people will contact me and say things like, I've become enlightened, I'm free from suffering,

61
00:09:18,000 --> 00:09:23,760
I've become this. Sometimes they'll tell me they're a sotabana or a stream member or this or that.

62
00:09:25,200 --> 00:09:30,960
And certainly, I mean, it's certainly possible. I don't doubt that it's possible, but

63
00:09:30,960 --> 00:09:40,400
I think it's actually much more likely or it's probably much more common to overestimate your

64
00:09:40,400 --> 00:09:46,000
practice, especially if you're contacting me by email. I would think enlightened being

65
00:09:48,320 --> 00:09:52,320
perhaps not piece of open about that anyway.

66
00:09:52,320 --> 00:10:05,280
So it is an issue here that we have to discuss. More importantly, it's an issue for all of us

67
00:10:05,280 --> 00:10:12,080
because it's a big part of our cognitive process as we're practicing. You practice for a while

68
00:10:12,080 --> 00:10:18,320
and then you wonder, what am I at? Where am I at? Am I close to being enlightened? Am I closer to

69
00:10:18,320 --> 00:10:25,040
being enlightened? Am I enlightened? Very dangerous question, a problematic.

70
00:10:28,960 --> 00:10:34,640
So what are the talks about different types of people? If you want to, first of all, to dispel

71
00:10:34,640 --> 00:10:39,600
the idea that this sort of person is the same as that sort of person. First, they have a person as

72
00:10:39,600 --> 00:10:48,320
someone who is intent upon central pleasure, intent upon material things, worldly things.

73
00:10:52,400 --> 00:10:58,400
So I wouldn't say there's a lot of spiritual people who think that that could possibly mistake

74
00:10:58,400 --> 00:11:03,360
that for enlightenment. But I would say there are people in the world who think that they are

75
00:11:03,360 --> 00:11:11,760
healthy. They have a healthy mind who are engrossed in centrality. They may be so confident that they've

76
00:11:11,760 --> 00:11:19,360
got everything worked out. I don't know such people. I certainly know some of these people who

77
00:11:20,480 --> 00:11:31,360
appear outwardly to be dived at all, figured out. They feel or they appear to feel for themselves

78
00:11:31,360 --> 00:11:39,920
that everything is fine, that they are healthy. Of course, these are the sorts of people

79
00:11:42,640 --> 00:11:54,560
who, well, first of all, tend to manipulate and without realizing it will always find ways to

80
00:11:54,560 --> 00:12:04,240
maintain their calm, their peace, their stability in life, often even at an unethical

81
00:12:04,240 --> 00:12:12,000
way at the expense of their morality. But more importantly, these are, of course, the people

82
00:12:12,000 --> 00:12:17,840
who become very much attached to worldly things and, of course, it's not enlightenment.

83
00:12:17,840 --> 00:12:26,400
And it's considered to be limited and limited value because it's impermanent. I mean, I've seen

84
00:12:26,400 --> 00:12:37,520
such people in desperate straits, crying, totally devastated. But then the next day, back, thinking

85
00:12:37,520 --> 00:12:44,880
that they're on top of the world, thinking that everything's good, tend to be not evil, evil

86
00:12:44,880 --> 00:12:49,680
people. I think evil people tend to be messed up in mind, their minds are not stable.

87
00:12:53,760 --> 00:13:00,560
But it's quite possible to, it's a good example of overestimation. You think you're happy, but

88
00:13:00,560 --> 00:13:07,840
anyone looking at you is a discerning eye could be quite clear that you're not all that happy

89
00:13:07,840 --> 00:13:17,920
overall. Again, it's one thing to think here on top of the world. It's sometimes another thing

90
00:13:17,920 --> 00:13:27,760
to actually be, which is really the topic of the second type of person. It has a person who enters

91
00:13:27,760 --> 00:13:32,960
into the genres. I'll go quickly through them. It's just, we'll group them all together. There's

92
00:13:32,960 --> 00:13:41,840
three different ones, but three different ones. But it's all about Jana. This is much more common.

93
00:13:41,840 --> 00:13:52,240
It's very common to attain some high spiritual state and think you're enlightened. A person

94
00:13:52,240 --> 00:13:56,960
who's attained some kind of absorption where their mind is just so still.

95
00:13:56,960 --> 00:14:13,440
Very quickly, very easy to jump to the quick conclusion that you've become enlightened.

96
00:14:13,440 --> 00:14:31,600
It's important to distinguish between two types of spiritual practice and other tranquility

97
00:14:33,840 --> 00:14:39,280
where your mind becomes very calm, very peaceful, and then there's insight

98
00:14:39,280 --> 00:14:47,040
where, honestly, you become very calm and very peaceful, but much more to the point,

99
00:14:47,040 --> 00:14:57,120
you come to understand reality, which I think has to be stressed and explained because

100
00:14:57,920 --> 00:15:05,440
I think a lot of people who have clearly just practiced tranquility and think that they do understand

101
00:15:05,440 --> 00:15:14,240
reality. Again, it's what this confidence does to you when you become something.

102
00:15:17,680 --> 00:15:25,680
You create this confidence, the sense that you are attained to something.

103
00:15:27,840 --> 00:15:31,440
It comes along with you, that you understand the right way.

104
00:15:31,440 --> 00:15:39,280
We have to emphasize what it means to understand reality because it's not just

105
00:15:41,600 --> 00:15:51,120
simple or conceptual thing where, oh yes, having attained this, that means I understand

106
00:15:51,120 --> 00:16:01,840
everything. No, it really means that you have investigated deeply into the admittedly very simple

107
00:16:02,720 --> 00:16:07,280
objects of reality or aspects of reality, what is real.

108
00:16:09,440 --> 00:16:12,960
Basically seeing, hearing, smelling, tasting, feeling, thinking, you've done

109
00:16:12,960 --> 00:16:25,040
deep, prolonged, extensive, usually extensive and belonging training in observing and understanding

110
00:16:25,040 --> 00:16:31,280
these things. That's quite specific. You can't just claim to understand things.

111
00:16:32,240 --> 00:16:40,160
It really is a scientific investigation, and it's that scientific investigation that

112
00:16:40,160 --> 00:16:48,160
allows one to then be sure or make the claim that they understand reality.

113
00:16:48,800 --> 00:16:51,520
Quite simple, you've investigated, therefore you understand,

114
00:16:52,320 --> 00:16:54,560
you've investigated until you understood.

115
00:16:59,600 --> 00:17:05,280
And so it's much more common for people to have some experience and then just get a feeling

116
00:17:05,280 --> 00:17:10,320
that they're enlightened, a feeling that makes them think they're enlightened.

117
00:17:13,280 --> 00:17:17,680
And so a good way to know whether it's real or not real is have you really investigated and

118
00:17:17,680 --> 00:17:27,520
understood the details of your experience as being what they are, as being not worth clinging to,

119
00:17:27,520 --> 00:17:30,160
that's what the Buddha is going to end up in a suit to sing.

120
00:17:30,160 --> 00:17:41,120
That are truly enlightened beings, such as one who has investigated to the point.

121
00:17:41,760 --> 00:17:46,560
And this isn't intellectual. You can't skip steps and say, okay, well, then let me intellectually

122
00:17:46,560 --> 00:17:53,760
understand that. No, it's not like that. You keep practicing until

123
00:17:53,760 --> 00:18:02,160
knowledge, clear understanding comes to you. The thing is worth clinging to.

124
00:18:03,360 --> 00:18:11,200
Not intellectual, not something that you can just assume having attained some peaceful state.

125
00:18:12,960 --> 00:18:16,560
So he says, a person who attains these peaceful states is intent upon them.

126
00:18:16,560 --> 00:18:25,200
It's impressive because they're no longer at all interested in sensuality,

127
00:18:26,720 --> 00:18:30,320
but that's one of the big reasons why they then think they're enlightened is because while

128
00:18:31,280 --> 00:18:36,720
being intent upon that, you could say attached to that, and certainly becoming

129
00:18:36,720 --> 00:18:44,640
somewhat egotistical about it in the sense of clinging to it as I am this or I am

130
00:18:47,200 --> 00:18:49,920
no longer interested in sensuality.

131
00:18:53,440 --> 00:18:58,800
But any meditative attainment is impermanent. The only thing that is really lasting,

132
00:18:58,800 --> 00:19:09,760
we would say, is in this waking up of insight where you see clearly that nothing is worth clinging

133
00:19:09,760 --> 00:19:14,960
to, or you experience that.

134
00:19:21,680 --> 00:19:27,920
And so then he says, there's two people, talks with these two people. One person comes to me and

135
00:19:27,920 --> 00:19:32,880
or no one person hears while the Buddha has taught that craving is,

136
00:19:35,120 --> 00:19:43,840
craving is an arrow, craving is like an arrow in your side, a poison arrow.

137
00:19:47,840 --> 00:19:53,680
But that arrow has been removed from me. I've gotten rid of all the poison.

138
00:19:53,680 --> 00:20:03,040
I am one who is completely intent on the Buddha. It's not true.

139
00:20:06,240 --> 00:20:08,160
They believe this, but it's not true.

140
00:20:11,040 --> 00:20:15,920
And he points out the danger, and this is where we get into this idea of reification.

141
00:20:15,920 --> 00:20:24,960
The danger is that because they think they're leading, they're going to stop with the

142
00:20:26,880 --> 00:20:28,800
intense urgency.

143
00:20:30,800 --> 00:20:34,880
They'll stop putting out the great effort. You think you're enlightened, of course, you'll

144
00:20:34,880 --> 00:20:42,160
stop practicing. It needs to complacency, which should be familiar from what we said earlier

145
00:20:42,160 --> 00:20:47,920
about how, when you reunify something, it creates contentment, it creates a false sense of security.

146
00:20:49,520 --> 00:20:54,480
And the very same thing, and probably one of the worst ways this can happen, is in

147
00:20:55,360 --> 00:21:01,760
spiritual practice when you believe that you're enlightened. It's one of the worst because

148
00:21:02,800 --> 00:21:06,080
it shuts the door for actual and true enlightenment.

149
00:21:06,080 --> 00:21:17,440
We should remind ourselves of what we're doing. We're not practicing to become anything.

150
00:21:19,680 --> 00:21:27,920
We're not, the idea is not to stop and see, am I a leading animal, and then can stop and rest.

151
00:21:27,920 --> 00:21:32,080
If you still want to stop and rest of the sign, you have some kind of clinging and some kind of

152
00:21:32,080 --> 00:21:42,640
aversion to whatever it is you think this work is that you have to do. We're practicing mindfulness

153
00:21:42,640 --> 00:21:51,920
to become mindful and enlightened, being as mindful. So if you think, if you still have the idea

154
00:21:51,920 --> 00:21:56,160
that you can stop meditating because you've stopped being mindful because you become enlightened,

155
00:21:56,160 --> 00:22:03,600
that's a good sign that you're not. But this is what happens. People then come complacent. He

156
00:22:03,600 --> 00:22:13,280
said it's like a person who gets struck by an arrow, right again, the arrow of defilement, poisonous

157
00:22:13,280 --> 00:22:19,760
arrow. And then a doctor comes and cuts the arrow out and cleans the wound,

158
00:22:19,760 --> 00:22:29,440
patches them all up and says, okay, you're good to go. You're all healed. Just take care of

159
00:22:29,440 --> 00:22:35,040
any, just take care of the wound. And the man goes home, but it turns out that the poison wasn't

160
00:22:35,040 --> 00:22:43,520
all taken out and there's some poison. And not only that, worse than that, he doesn't take care

161
00:22:43,520 --> 00:22:47,760
of the wound, doesn't look after it. The doctor said, oh, make sure it doesn't get dirty and keep

162
00:22:47,760 --> 00:22:56,480
it clean, doesn't do that. So he's doubly problematic because the wound gets not only poison,

163
00:22:56,480 --> 00:23:06,720
but infected and all that person dies. And then he says, there's a other type of person

164
00:23:07,680 --> 00:23:16,320
who comes and hears that I teach. I teach that craving is the cause of suffering, craving is a

165
00:23:16,320 --> 00:23:23,040
dark, there's an arrow. And they make a statement, wow, this craving is gone for me, or not

166
00:23:23,040 --> 00:23:28,880
wow, but they make a statement that the craving is, I have eradicated the craving. And that's

167
00:23:28,880 --> 00:23:34,400
actually true that they actually have. So this is the person who claims to be enlightened and is

168
00:23:34,400 --> 00:23:47,280
enlightened. And he says, this is like a person who is shot with an arrow, poison is arrow. And then

169
00:23:47,280 --> 00:23:51,840
the doctor comes and cleans it all out and patches them up and says, well, take care of,

170
00:23:53,200 --> 00:23:58,560
take care of this wound. It's all clean. The poison's all gone. He said that about the first

171
00:23:58,560 --> 00:24:07,040
guy as well, but this time he's maybe a little more sure. And he says, take care of it. So

172
00:24:07,840 --> 00:24:15,040
this person, they think, wow, my wound is healed, the poison's all out and then they take good

173
00:24:15,040 --> 00:24:20,880
care of the wound. They clean it, they keep it out of dirt, keep dirt out of it. And as a result,

174
00:24:20,880 --> 00:24:34,240
they're perfectly healthy. They have no death, no deadly suffering. And so there are different,

175
00:24:34,240 --> 00:24:38,560
these two people are different in two ways. They're different because one of them still has the

176
00:24:38,560 --> 00:24:46,720
poison, one of them doesn't. But they're also different in how they react to the wound. The wound

177
00:24:46,720 --> 00:24:57,840
has been cleaned and treated. And in light and being is not just that has not just done the work

178
00:24:57,840 --> 00:25:04,320
that needs to be done. There are a person who lives their life in the way that needs to be lived.

179
00:25:04,880 --> 00:25:10,880
So this is again, and the light being doesn't say, oh, I've done all my work now, I can stop.

180
00:25:10,880 --> 00:25:19,680
And in light and being has become mindful, the mindfulness practice that they did has reached

181
00:25:19,680 --> 00:25:24,240
a pinnacle that is a drop, doesn't stop.

182
00:25:31,280 --> 00:25:37,520
And so that, at the very least, that helps us to focus on what's important,

183
00:25:37,520 --> 00:25:44,480
because again, we're not trying to become something. And it leads me to the third thing, the third

184
00:25:44,480 --> 00:25:51,760
option that the Buddha doesn't talk about, for whatever reason he's giving a talk to a specific

185
00:25:51,760 --> 00:25:58,240
person. But important to talk about here as well is a third type of person. And that's really

186
00:25:59,280 --> 00:26:02,080
Buddhist meditators who are on the path to become enlightened.

187
00:26:02,080 --> 00:26:09,280
We hear about all these stages of enlightenment, sotapana, stream enter.

188
00:26:12,080 --> 00:26:14,800
I've had people come and tell me they're on a gummies. And

189
00:26:20,960 --> 00:26:28,400
lots of different ideas. I've had people ask me about how to become sotapana. There's this urgency,

190
00:26:28,400 --> 00:26:33,920
this desire to become. And there's the checking and people who read the texts, I've talked to

191
00:26:33,920 --> 00:26:41,280
people who read the texts and say, well, going by the text, I'm a sotapana. I'm a stream enter.

192
00:26:43,360 --> 00:26:48,560
Some of you probably never heard these words before, but they're basically someone who has seen

193
00:26:48,560 --> 00:26:55,120
nibana, but they're labels. And they are useful, but they are also problematic.

194
00:26:55,120 --> 00:27:00,560
Because they're a reification, this is where I was getting it, what I was getting it.

195
00:27:04,400 --> 00:27:09,040
I think a person who is a sotapana, who is seen nibana,

196
00:27:11,280 --> 00:27:20,160
is in very real danger of reifying that state and becoming complacent. You see, I'm a sotapana.

197
00:27:20,160 --> 00:27:27,360
And again, it becomes a thing. You've created in your mind this thing, this concept that you apply

198
00:27:27,360 --> 00:27:34,640
to the self, and do I? Well, a sotapana is actually free from views of self. They still have

199
00:27:34,640 --> 00:27:45,040
conceit, but they can become not greatly conceit. They wouldn't be evil about it, but it can become

200
00:27:45,040 --> 00:27:53,600
a real hindrance and prevent them from progressing. Because they cling to that idea, oh, I'm safe,

201
00:27:53,600 --> 00:28:01,760
I've made it. I've done something good, something very good, something noble, in fact.

202
00:28:05,840 --> 00:28:12,160
But if we think about what an enlightened being is like, how the end goal this person is just

203
00:28:12,160 --> 00:28:21,200
always mindful. It's like they're always practicing. Then we should only see ourselves as

204
00:28:22,240 --> 00:28:32,560
climbing the mountain or cleansing the mind. I think the best way is to talk about cleansing the mind.

205
00:28:34,000 --> 00:28:39,920
It could say, oh, boy, I've cleaned a lot, but you're still in reading that. There's still

206
00:28:39,920 --> 00:28:47,920
stuff left to be cleaned. You clean your house and you say, look, I managed to clean the center of

207
00:28:47,920 --> 00:28:56,000
the room and all that stuffer on the outside of the room is you just push everything up against

208
00:28:56,000 --> 00:29:03,920
the walls or you still have rest of the house to clean. You clean one room and say, wow,

209
00:29:03,920 --> 00:29:13,680
what I've done, I mean, clearly anyone would, anyone with any sense of cleanliness would say,

210
00:29:13,680 --> 00:29:20,880
oh, no, you've still got several rooms left clean. If we think of it in this way, we're not

211
00:29:20,880 --> 00:29:29,600
reifying anything. We're not cleaning any state. Now again, it can be encouraging to think of

212
00:29:29,600 --> 00:29:41,680
the progress you've made. It can also be in line with the person who is wrong about their

213
00:29:41,680 --> 00:29:46,320
practice. And I think it really, so the point is really doesn't matter whether you write

214
00:29:46,320 --> 00:29:55,040
or wrong. It's not something we should ever focus our attention on. You think, might you

215
00:29:55,040 --> 00:30:00,560
ensure that we'd say, do you still have greed? Do you still have anger? Do you still have

216
00:30:00,560 --> 00:30:08,880
delusion? Then you still have work to do. It's really the best way to look at it, because once

217
00:30:08,880 --> 00:30:15,280
you get rid of all three of those, then you're safe. But you're safe from slacking off is the point.

218
00:30:17,040 --> 00:30:20,720
If you ever think, oh, I'm done, but I can slack off, you're not there yet.

219
00:30:20,720 --> 00:30:29,520
Because in light and being doesn't slack off, there are always an energetic always on the ball.

220
00:30:29,520 --> 00:30:53,920
He says, the Buddha says, so what was there with a liquid in it, with a beverage in it,

221
00:30:53,920 --> 00:30:59,600
with a good color, a good smell, and a good taste, something that just looked quite appealing,

222
00:31:00,560 --> 00:31:12,800
but it's mixed with poison. If someone came and knew that it was one full drink and poison it,

223
00:31:12,800 --> 00:31:20,880
would they drink from it? And he says, no vendor was there. Same with someone who's in light,

224
00:31:20,880 --> 00:31:31,120
and they will never go back to unmindful ways, knowing that there is never go back to the sensuality,

225
00:31:32,240 --> 00:31:42,160
never go back to acquisition. He says, and then he says, suppose there were a snake and someone

226
00:31:42,160 --> 00:31:51,760
who wanted to live, who didn't, who wasn't fond of pain, when they stick their hand up to the

227
00:31:51,760 --> 00:31:59,280
poison, it's like, no, a vendor opposite. Likewise, when the night and being would never go back,

228
00:32:00,240 --> 00:32:06,480
those bad ways. When you read this today, it's quite, it's a good one, I think.

229
00:32:06,480 --> 00:32:15,760
But I wanted to address this idea, really it helps give some perspective of what's important.

230
00:32:15,760 --> 00:32:23,440
It's not important to become something. It's the work that's important and the cleansing of the

231
00:32:23,440 --> 00:32:37,920
blind in increments. We're very goal-oriented, I think is a big problem rather than be goal-oriented,

232
00:32:40,560 --> 00:32:44,800
we process oriented, doing our work, bit by bit.

233
00:32:44,800 --> 00:32:55,440
So that's the number for tonight. Thank you all for tuning in. I'm sure you have a good night.

234
00:32:55,440 --> 00:33:25,280
Good week.

