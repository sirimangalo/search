1
00:00:00,000 --> 00:00:25,000
So today I thought I would talk about the value, the purpose of meditation, the purpose of the meditation.

2
00:00:25,000 --> 00:00:36,000
I've talked a little bit about this already in talks about what is meditation and what is specifically Vipasana meditation.

3
00:00:36,000 --> 00:00:45,000
Now I'd like to talk in detail about the purpose of meditation, of Vipasana meditation.

4
00:00:45,000 --> 00:00:56,000
And this goes in line with the idea of us being teachers and the meditators being students.

5
00:00:56,000 --> 00:01:09,000
Because the teacher in any field is someone who has something of some benefit or some purpose or some value to the student.

6
00:01:09,000 --> 00:01:24,000
And the student comes to learn from the teacher because they expect some value or some benefit from the study which they've undergone.

7
00:01:24,000 --> 00:01:35,000
And the practice of meditation is very much like a study of our own hearts and our own minds, a study of reality.

8
00:01:35,000 --> 00:01:43,000
And the idea is that it will give us some benefit in our lives.

9
00:01:43,000 --> 00:01:57,000
And the benefits of Vipasana meditation, the Lord Buddha, laid down those things which were a value which can be gained through the practice of the Buddha's teaching.

10
00:01:57,000 --> 00:02:06,000
They're called the Sarathamma.

11
00:02:06,000 --> 00:02:11,000
Sarat means something which is of essence or of essential value or of benefit or of purpose.

12
00:02:11,000 --> 00:02:19,000
And the dhamma is, these are the dhammas, these are the realities where the things which can be realized for oneself.

13
00:02:19,000 --> 00:02:30,000
And these are the five things which come from the practice of the Buddha's teaching, the practice of Vipasana meditation, the practice to see clearly.

14
00:02:30,000 --> 00:02:40,000
And so these five things I would say are the value or the benefit which we gain from practicing meditation.

15
00:02:40,000 --> 00:02:50,000
And these are one, morality.

16
00:02:50,000 --> 00:02:53,000
Number two, concentration. Number three, wisdom. Number four, freedom.

17
00:02:53,000 --> 00:03:01,000
And number five, knowledge and vision of freedom. Knowing that we are free.

18
00:03:01,000 --> 00:03:12,000
And these five things the Lord Buddha said are of true value and are the true value which we can gain through the practice of the Lord Buddha's teaching.

19
00:03:12,000 --> 00:03:20,000
So when people come to practice Vipasana meditation and they wonder what value or what benefit, it will be to them.

20
00:03:20,000 --> 00:03:31,000
And we have something which is very concrete which we can use to explain the value of the Buddha's teaching.

21
00:03:31,000 --> 00:03:39,000
So the first item is morality. This is the thing which is of value to us that we gain from the meditation.

22
00:03:39,000 --> 00:03:47,000
You can see that when you first come to the meditation center already we are beginning to teach the meditators about morality.

23
00:03:47,000 --> 00:03:54,000
We are beginning to undertake a study of morality. How to be moral, how to be virtuous. We give meditators eight.

24
00:03:54,000 --> 00:04:01,000
Moral virtues or moral rules to live by during the time that they are here.

25
00:04:01,000 --> 00:04:08,000
And we explain to people about the five percent. Not to kill, not to steal, not to commit it.

26
00:04:08,000 --> 00:04:16,000
Wrong sexual misconduct, not to use false speech, not to take drugs and alcohol.

27
00:04:16,000 --> 00:04:26,000
So we started this point from this point giving people a sense of morality, a sense of something which is important.

28
00:04:26,000 --> 00:04:35,000
We give these abstentions an importance or we show the importance of these things to the meditators.

29
00:04:35,000 --> 00:04:41,000
The meditators to realize the importance of being a virtuous person and the benefit of it.

30
00:04:41,000 --> 00:04:55,000
And so the idea is that when a meditator leaves they will be keen in the observation and endurance to these moral virtues.

31
00:04:55,000 --> 00:05:10,000
Further in the meditation when we practice morality it means to keep the mind from wandering into defilements, to keep the mind from wanting to hurt other people and so on.

32
00:05:10,000 --> 00:05:18,000
When we simply give the meditator the presets you can consider that the meditator then has morality or is training in morality.

33
00:05:18,000 --> 00:05:31,000
But until we actually change the mind and adjust the mind based on our experience it's not really true that the meditator has morality.

34
00:05:31,000 --> 00:05:51,000
But once they are able to train themselves and see that these things are bad and remember bad things that we have done and see the danger in those bad deeds through clear comprehension and understanding.

35
00:05:51,000 --> 00:06:06,000
Once our mind settles and we start to see the bad things we've done, start to realize about bad deeds of body, bad deeds of speech, how there's something which leads to suffering for ourselves and suffering for others and things which are not good.

36
00:06:06,000 --> 00:06:14,000
Until we can realize that then you can't really say that the meditator the person has morality.

37
00:06:14,000 --> 00:06:25,000
To realize these things then we really and truly have morality. This is something which comes through the meditation that further from just explaining to the meditator about precepts.

38
00:06:25,000 --> 00:06:34,000
The practice of Yipasana or the practice of the Buddha's teaching has a great benefit that in grains moral virtue into the meditator's mind.

39
00:06:34,000 --> 00:06:52,000
The meditator becomes a kind and a patient, a generous, a grateful human being, someone who is not liable to do or say bad things, having seen for oneself the impact that these bad deeds have.

40
00:06:52,000 --> 00:07:07,000
So the meditator comes to be one who has moral virtue as a result of not doing bad deeds or saying bad things, the meditator is able to find happiness and peace and friendship among other beings.

41
00:07:07,000 --> 00:07:16,000
This is the first thing which is the value in the meditation practice. The second thing is concentration and the meditator starts to practice.

42
00:07:16,000 --> 00:07:30,000
This is something which many people already understand as being a benefit of meditation. They expect it, in fact, when you come to practice meditation you expect that you should gain some level of concentration.

43
00:07:30,000 --> 00:07:55,000
The concentration we're talking about here has the benefit that it destroys the nivarana, the hindrances. It takes bad emotions, unholesome emotions out of the mind. It takes anger out of the mind, it takes greed, it takes lust, it takes frustration, boredom, sadness.

44
00:07:55,000 --> 00:08:10,000
It keeps the mind calm and happy, keeps the mind from falling into unhappiness or into desire or lust or need or addiction.

45
00:08:10,000 --> 00:08:32,000
So when we practice meditation, once we start to fix the mind and focus the mind on the present moment and we're able to train the mind out of it, thinking about the past or thinking about the future or wandering or falling into obsession or obsessing about external things or internal things.

46
00:08:32,000 --> 00:08:50,000
When we can retrain the mind, we find our mind starts to calm and starts to quiet down and really we have none of the five hindrances, no more desire or aversion or drowsiness, mental distraction or doubt.

47
00:08:50,000 --> 00:09:06,000
That our mind is calm and peaceful and content, not wandering or worrying or fretting or upset or intoxicated or it's not obstructed in any way.

48
00:09:06,000 --> 00:09:22,000
That our mind is going straight and going quickly on the path which it intends. When we intend to do something, our mind is fixed and focused on that. When we walk, our mind is fixed and focused on the walking.

49
00:09:22,000 --> 00:09:28,000
When we see it, our mind is fixed and focused on the rising and the falling and the stomach.

50
00:09:28,000 --> 00:09:50,000
This is concentration that has the benefit of keeping our mind calm and at peace. When meditators practice, in days they can find that even after five or six days their mind starts to calm down and they're able to experience a state of peace and calm, which they have never perhaps felt before.

51
00:09:50,000 --> 00:10:06,000
We can see that this is a benefit much nicer and much happier than having to live through all sorts of frustration and boredom and anxiety and stress and anger and hatred and so on.

52
00:10:06,000 --> 00:10:21,000
And doubt and worry and distremental distraction that our mind becomes peaceful and calm and is quite pleasant, quite enjoyable.

53
00:10:21,000 --> 00:10:33,000
This is a second benefit of the meditation, something which is a real value which the Lord Buddha had set up as something beneficial or something of essential value.

54
00:10:33,000 --> 00:10:47,000
The third is wisdom. And wisdom is the best in the Buddha's teaching. It's the highest virtue or the highest thing which we can gain because morality or concentration or things which can only give benefit temporarily.

55
00:10:47,000 --> 00:11:02,000
If a person keeps more at moral precepts and tries to be a good person, it's surely still possible that if their mind is upset beyond their ability to control they can find themselves doing and saying bad things even in the future.

56
00:11:02,000 --> 00:11:14,000
No matter how long they train themselves in morality alone without concentration in wisdom, their minds can still become upset and lead them to do and say bad things.

57
00:11:14,000 --> 00:11:31,000
The same if a person has concentration, suppose they have morality and concentration, their minds are peaceful and calm and they're able to control their minds to the extent that bad things don't bad thoughts and bad emotions don't arise in the mind.

58
00:11:31,000 --> 00:11:42,000
As far as greed and as far as anger, but because delusions still exists or ignorance still exists, the mind is still unaware of the dangers.

59
00:11:42,000 --> 00:11:55,000
There's been no recognition of the danger of anger, the danger of greed and there's been no eradication of delusion.

60
00:11:55,000 --> 00:12:06,000
The mind is still able to fall into wrong view, still able to fall into conceit, holding oneself above others or holding oneself up as high as good.

61
00:12:06,000 --> 00:12:09,000
Holding onto this is me or mine.

62
00:12:09,000 --> 00:12:22,000
When one leaves off from practice of concentration, the mind goes straight back to greed, straight back to anger and all of the five hindrances come up again because the delusion has not yet been eradicated.

63
00:12:22,000 --> 00:12:26,000
Ignorance has not yet been given up.

64
00:12:26,000 --> 00:12:29,000
One simply hasn't come to realize the truth.

65
00:12:29,000 --> 00:12:37,000
And this is, you can compare it to someone who hasn't studied yet in university or hasn't studied yet in school.

66
00:12:37,000 --> 00:12:41,000
But they can work and work and train themselves as hard.

67
00:12:41,000 --> 00:12:45,000
They can work as hard as they may, as hard as they want.

68
00:12:45,000 --> 00:12:50,000
But if they are unskilled in whatever task they are doing, if they haven't gone to school for it,

69
00:12:50,000 --> 00:12:55,000
they are hard work in their effort, still in the end leads to very little.

70
00:12:55,000 --> 00:13:03,000
The person who is a carpenter or so on, if they've never studied in school, if they've never studied the details behind it.

71
00:13:03,000 --> 00:13:06,000
For instance, you want to put cement together.

72
00:13:06,000 --> 00:13:10,000
If you don't know anything about putting cement, you can work and work and put it together.

73
00:13:10,000 --> 00:13:16,000
And you don't know if you have to add water, you have to add sand, you have to add this mud.

74
00:13:16,000 --> 00:13:23,000
If you simply don't know, all of your work and effort can be without fruit.

75
00:13:23,000 --> 00:13:24,000
This is like Ignorance.

76
00:13:24,000 --> 00:13:28,000
Even though we might have so much effort in our practice, if we don't yet understand,

77
00:13:28,000 --> 00:13:36,000
or if we don't practice to come to see clearly, if we simply fix the mind on a single point or so on.

78
00:13:36,000 --> 00:13:43,000
We don't understand the, we're able to do away with the hindrances, but we don't understand the danger.

79
00:13:43,000 --> 00:13:47,000
We aren't able to see the disadvantages of holding on.

80
00:13:47,000 --> 00:13:50,000
We aren't able to see impermanent suffering in oneself.

81
00:13:50,000 --> 00:13:54,000
When we stop practicing, our mind goes straight back to the profoundness.

82
00:13:54,000 --> 00:13:59,000
So wisdom is something which is higher than morality and concentration.

83
00:13:59,000 --> 00:14:03,000
And this is considered another very essential thing.

84
00:14:03,000 --> 00:14:06,000
In wisdom, we can separate into three kinds.

85
00:14:06,000 --> 00:14:09,000
One is Suptamayapanya, wisdom which we've heard.

86
00:14:09,000 --> 00:14:13,000
When we hear someone else telling us explaining us about how to practice,

87
00:14:13,000 --> 00:14:18,000
then Jintamayapanya, when we consider what was said, and we think about it,

88
00:14:18,000 --> 00:14:24,000
and we put it into words that we can understand, and set out and try to practice it.

89
00:14:24,000 --> 00:14:27,000
Figure out how it is that this should be put into practice.

90
00:14:27,000 --> 00:14:29,000
What is exactly it means?

91
00:14:29,000 --> 00:14:32,000
Jintamayapanya, and then Pohenamayapanya.

92
00:14:32,000 --> 00:14:37,000
At the time when we practice, and then we see clearly, we see impermanent,

93
00:14:37,000 --> 00:14:40,000
and we see suffering, we see non-self.

94
00:14:40,000 --> 00:14:43,000
We see that the things which we hold on to are impermanent.

95
00:14:43,000 --> 00:14:49,000
We see that the things which we desire are unsatisfying.

96
00:14:49,000 --> 00:14:53,000
And we see that the things which we don't like and don't like

97
00:14:53,000 --> 00:14:57,000
and the things which we hold on to and try to control are not under our control.

98
00:14:57,000 --> 00:15:04,000
Not ours are not us, they're not me, they're not mine, and so on.

99
00:15:04,000 --> 00:15:07,000
This is what I want to call wisdom.

100
00:15:07,000 --> 00:15:13,000
This is Pohenamayapanya through meditation practice, through development of the mind.

101
00:15:13,000 --> 00:15:20,000
When wisdom arises, it has the value of abandoning even the deep rooted

102
00:15:20,000 --> 00:15:28,000
defilements, wrong view, conceit, all sorts of attachments are uprooted.

103
00:15:28,000 --> 00:15:33,000
Our interest in sensuality, our interest in anything internal or external,

104
00:15:33,000 --> 00:15:35,000
is uprooted.

105
00:15:35,000 --> 00:15:41,000
When we find that we are not keen, we are not interested in things which are impermanent,

106
00:15:41,000 --> 00:15:42,000
suffering and non-self.

107
00:15:42,000 --> 00:15:48,000
We've come to see that these things are not satisfied.

108
00:15:48,000 --> 00:15:56,000
This is something which is of most value, so that our minds will not strive after impermanent things.

109
00:15:56,000 --> 00:16:01,000
We will not fall into suffering when the things which we desire disappear.

110
00:16:01,000 --> 00:16:04,000
When bad things come, we will not become upset.

111
00:16:04,000 --> 00:16:09,000
When people say bad things or do bad things, our minds will not become upset

112
00:16:09,000 --> 00:16:14,000
because we will see that the things, even our own selves, are the things which we thought,

113
00:16:14,000 --> 00:16:18,000
the things which we take to belong to us, don't actually belong to us.

114
00:16:18,000 --> 00:16:22,000
Even ourselves, our own body, our own mind is not ours.

115
00:16:22,000 --> 00:16:26,000
There's something which arises in ceases in its own accord.

116
00:16:26,000 --> 00:16:31,000
When we see this in our mind, let's go, we see this through wisdom.

117
00:16:31,000 --> 00:16:35,000
My let's go and we become peaceful and calm all the time.

118
00:16:35,000 --> 00:16:39,000
Even if we stop practicing, we have knowledge.

119
00:16:39,000 --> 00:16:45,000
We can stop putting out effort, stop being mindful, stop walking, stop sitting,

120
00:16:45,000 --> 00:16:47,000
but the knowledge still exists.

121
00:16:47,000 --> 00:16:55,000
And at any moment that bad things arise, the mind sees those things based on one's knowledge.

122
00:16:55,000 --> 00:17:03,000
And there's no, there can be no more, no more falling into delusion.

123
00:17:03,000 --> 00:17:08,000
For instance, when a person is walking in a dark room and they step on a rope,

124
00:17:08,000 --> 00:17:11,000
if you've ever stepped on a rope in a dark room, you know what it's like.

125
00:17:11,000 --> 00:17:13,000
You think right away it's a snake.

126
00:17:13,000 --> 00:17:18,000
I think right away you've stepped on a snake and you jump.

127
00:17:18,000 --> 00:17:24,000
But if you're walking around in a lit room and you see the rope on the floor and you step on it,

128
00:17:24,000 --> 00:17:30,000
even if you turn the light off again and step on the rope, you know that it's a rope.

129
00:17:30,000 --> 00:17:37,000
In this way wisdom is something which is, which does not erode, does not fade away.

130
00:17:37,000 --> 00:17:47,000
The power namayapan is something which lasts to the end of samsara.

131
00:17:47,000 --> 00:17:56,000
And the fourth thing which is a value, which the word Buddha taught is freedom.

132
00:17:56,000 --> 00:18:01,000
And freedom is something which comes once a person has developed morality, concentration and wisdom.

133
00:18:01,000 --> 00:18:08,000
And then when the mind becomes free from its addictions and its attachments and its defilements of greed, of anger, of delusion,

134
00:18:08,000 --> 00:18:13,000
when these things no longer exist in the mind, this is called freedom.

135
00:18:13,000 --> 00:18:20,000
This is something which is of benefit because it stops the person from doing and saying bad things.

136
00:18:20,000 --> 00:18:24,000
It stops hindrances and obstacles from arising in the mind.

137
00:18:24,000 --> 00:18:28,000
It stops all unpleasantness from arising in the mind.

138
00:18:28,000 --> 00:18:34,000
Freedom from all unwholesome things means freedom from all suffering.

139
00:18:34,000 --> 00:18:39,000
Because no matter what arises, a person is peaceful and happy sometimes,

140
00:18:39,000 --> 00:18:45,000
unattached to those things which arise, not saying, oh, this bad thing has arisen for me,

141
00:18:45,000 --> 00:18:50,000
or oh, this good thing has arisen for me, has disappeared for me.

142
00:18:50,000 --> 00:18:54,000
Or my good thing has disappeared.

143
00:18:54,000 --> 00:18:59,000
Or that something bad has arisen for me and so on.

144
00:18:59,000 --> 00:19:01,000
There's only the arising and ceasing.

145
00:19:01,000 --> 00:19:08,000
And one is able to see clearly that these are simply phenomena which arise and cease, which have no owner.

146
00:19:08,000 --> 00:19:18,000
And so one never becomes upset or dissatisfied or angry or frustrated.

147
00:19:18,000 --> 00:19:22,000
And the fifth thing, which is a value which the Lord Buddha taught,

148
00:19:22,000 --> 00:19:29,000
which we gain through the practice of the Buddha's teaching, is knowledge and vision of freedom.

149
00:19:29,000 --> 00:19:33,000
The knowledge and vision of freedom is something which is of supreme value.

150
00:19:33,000 --> 00:19:41,000
It comes once we are free, once we have gained the wisdom, and we become free from the balance.

151
00:19:41,000 --> 00:19:45,000
The knowledge of it is of supreme value.

152
00:19:45,000 --> 00:19:51,000
And it is of supreme value, not exactly because it leads to anything else.

153
00:19:51,000 --> 00:20:00,000
But because it is the state of supreme happiness, and the state of supreme peace and security.

154
00:20:00,000 --> 00:20:09,000
As long as someone hasn't realized, hasn't come to know for themselves that the defilements have been eradicated.

155
00:20:09,000 --> 00:20:15,000
That one has no longer has evil and wholesome states in one's mind.

156
00:20:15,000 --> 00:20:21,000
For so long, the person will always be afraid and be worried and ashamed.

157
00:20:21,000 --> 00:20:33,000
It will always be in the state of unrest without peace, always looking for refuge, looking for some safety,

158
00:20:33,000 --> 00:20:36,000
trying to find something that will make one happy.

159
00:20:36,000 --> 00:20:42,000
But once we know for ourselves that this defilement is gone, this defilement is gone.

160
00:20:42,000 --> 00:20:50,000
We are able to see clearly clearly and clearly that more and more defilements have left, never to come back.

161
00:20:50,000 --> 00:20:53,000
We feel more and more secure and more and more safe.

162
00:20:53,000 --> 00:21:01,000
And we become free from fear and worry, free from sorrow and lamentation and despair.

163
00:21:01,000 --> 00:21:04,000
So this is something which is of great value.

164
00:21:04,000 --> 00:21:10,000
Once we realize for ourselves, our mind becomes without fear, without worry.

165
00:21:10,000 --> 00:21:14,000
We never have anything to fear from any quarter.

166
00:21:14,000 --> 00:21:21,000
Anyone is any person or play any person or place or thing, anything around us or inside of us.

167
00:21:21,000 --> 00:21:26,000
There's nothing which will bring us discomfort or despair.

168
00:21:26,000 --> 00:21:32,000
So these five things that Lord Buddha said are of the utmost value or the essential value.

169
00:21:32,000 --> 00:21:38,000
And these are the five benefits, the five things which we hope to gain from our training.

170
00:21:38,000 --> 00:21:41,000
As we train, we will become moral and virtuous people.

171
00:21:41,000 --> 00:21:44,000
We will stop doing and saying bad things.

172
00:21:44,000 --> 00:21:48,000
Our minds will become peaceful and will come to see clearly with wisdom.

173
00:21:48,000 --> 00:21:53,000
When we see clearly with wisdom, we'll be free when we're free, we'll know for ourselves that we're free.

174
00:21:53,000 --> 00:22:02,000
And when we know for ourselves that we're free, we'll be at peace and content in the knowledge that there's nothing left to be done.

175
00:22:02,000 --> 00:22:13,000
This is the benefits, the sara tama, sara tama, and the things which are of essential benefit.

176
00:22:13,000 --> 00:22:23,000
And the things which are of value, which we as teachers and we pass in a meditation, hope to impart onto our students.

177
00:22:23,000 --> 00:22:27,000
This is the bhamma which I would present to you today.

178
00:22:27,000 --> 00:22:34,000
So now we continue to practice first walking the first mindful frustration and then walking and then sitting.

