1
00:00:00,000 --> 00:00:05,960
Okay, welcome everyone to our evening session among radio. First up, he is

2
00:00:05,960 --> 00:00:11,000
announcements. Have any announcements?

3
00:00:11,000 --> 00:00:21,280
Jens did arrive and became Yasser in the meantime. That will be his name if he

4
00:00:21,280 --> 00:00:30,640
ordains. When he ordains, not if, but when, they are going to try to get a visa to

5
00:00:30,640 --> 00:00:44,160
tomorrow for Sumeda and Yasser. And we have some Sri Lankan meditators here. So for

6
00:00:44,160 --> 00:00:58,000
now, six yogis, the top rates so far, and another meditator is coming soon, Sri Lankan

7
00:00:58,000 --> 00:01:05,440
as well on the 17th. So we are booked out, I would say.

8
00:01:05,440 --> 00:01:17,480
We are running in full speed. So yeah, Yasser or Yogi formerly known as Jens is an old meditator

9
00:01:17,480 --> 00:01:25,600
from Thailand and a good friend of Palanyani. So when she went left to go back to

10
00:01:25,600 --> 00:01:31,040
Thailand, Jens came to Watambu et almoor.

11
00:01:31,040 --> 00:01:37,600
And it was just the two of us there for a while with the old monk. And that's when I met

12
00:01:37,600 --> 00:01:46,920
him. And then he left and, and Palanyani came and ordained as a nun. And, and, and

13
00:01:46,920 --> 00:01:51,160
since then he's been back here to Sri Lankan to Sri Lankan to visit and liked so much

14
00:01:51,160 --> 00:01:57,040
that he went back to Germany, cleared up his business and came back and it's no joining

15
00:01:57,040 --> 00:02:02,240
our community for long term.

16
00:02:02,240 --> 00:02:09,200
Another announcement is we, in all likelihood, we'll be going to Thailand. That's myself,

17
00:02:09,200 --> 00:02:15,640
Yasser and Sameda. But we're not arranging to travel together because I can't arrange

18
00:02:15,640 --> 00:02:21,560
to travel together with a woman. So I'm arranging to travel with Yasser. And Sameda can

19
00:02:21,560 --> 00:02:27,960
arrange to travel with Yasser as well. Something like that. But anyway, that's just it.

20
00:02:27,960 --> 00:02:32,040
I, I join you in, in my heart and in my thoughts.

21
00:02:32,040 --> 00:02:36,720
Well, we might have to talk about that. You, you, you, you may be, maybe interested in

22
00:02:36,720 --> 00:02:39,440
coming along. We should talk about that.

23
00:02:39,440 --> 00:02:45,360
We definitely should run so jealous that you are calling an icon. See, because they are

24
00:02:45,360 --> 00:02:56,720
going to see a Jantong and a Jansu Pan. And that would be so good if I ever could come.

25
00:02:56,720 --> 00:03:03,200
So maybe it would stay with a Jantong at least 10 days, but probably more like two weeks.

26
00:03:03,200 --> 00:03:08,480
And then maybe have another week to, to go around other places. And I have to tell my

27
00:03:08,480 --> 00:03:14,960
students in Bangkok, they want me to go. So maybe doing a little bit of teaching there.

28
00:03:14,960 --> 00:03:21,040
And then I will travel if it would be so I would travel with Sumeda.

29
00:03:21,040 --> 00:03:32,880
Right. So that's, that's another announcement. We're, we're getting more set or more

30
00:03:32,880 --> 00:03:40,960
structured here. So we have now a fairly good routine of 4.30 AM. We have a group meditation.

31
00:03:40,960 --> 00:03:46,200
And 9 PM we're doing, and this is kind of a new thing we're doing. I'd be done much

32
00:03:46,200 --> 00:03:52,680
hunting, but we're doing it. I'll tie novice style. I'd probably not just tie

33
00:03:52,680 --> 00:03:56,720
novice, but novice monk style where they all get together in the room and read their

34
00:03:56,720 --> 00:04:03,480
own books. And it's the most chaotic sound you've ever heard. I recorded it once. It's

35
00:04:03,480 --> 00:04:10,080
quite interesting listening to all these novices reciting poly. So that's what we do.

36
00:04:10,080 --> 00:04:15,240
We sit there and we focus on our own text. And we do that for a half an hour. And then

37
00:04:15,240 --> 00:04:21,680
we have a brief meeting and group meditation. I mean, I'll be there. We try to be, I'll

38
00:04:21,680 --> 00:04:26,400
try to be there till 11. And then during the day, we're trying to do every day almost

39
00:04:26,400 --> 00:04:32,200
at a two o'clock group meeting. And that's when we'll do the Dhamapada videos and other

40
00:04:32,200 --> 00:04:37,840
videos to try to give it some Dhamma every day, both for the people here and for the

41
00:04:37,840 --> 00:04:45,040
internet community. So that's another useful thing to let people know that this place is

42
00:04:45,040 --> 00:04:54,360
really falling in many ways is falling into place. I think we could mention that possibly

43
00:04:54,360 --> 00:05:01,160
tomorrow workers will come. They're not actually the way we talked about it. That was

44
00:05:01,160 --> 00:05:06,240
a false alarm. But soon we'll have two more rooms. They're soon they're going to pick

45
00:05:06,240 --> 00:05:15,720
that up. But it takes time. Everything here is very slow. It's kind of laid back sort

46
00:05:15,720 --> 00:05:23,920
of sort of culture. Okay. So that's our announcements for this evening. I can end it

47
00:05:23,920 --> 00:05:40,800
there.

