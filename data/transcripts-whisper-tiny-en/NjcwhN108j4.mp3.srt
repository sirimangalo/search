1
00:00:00,000 --> 00:00:05,340
Okay, good evening.

2
00:00:05,340 --> 00:00:08,500
Welcome back to our study of the Dhammupanda.

3
00:00:08,500 --> 00:00:21,460
Today we look at verse 202, which reads as follows.

4
00:00:21,460 --> 00:00:48,960
There is no fire like passion.

5
00:00:48,960 --> 00:01:00,100
There is no misfortune like anger.

6
00:01:00,100 --> 00:01:08,560
There is no suffering like the aggregates.

7
00:01:08,560 --> 00:01:14,000
There is no happiness apart from peace.

8
00:01:14,000 --> 00:01:21,780
This is a very popular verse, I guess they all are.

9
00:01:21,780 --> 00:01:24,420
Now this is a very powerful verse.

10
00:01:24,420 --> 00:01:37,660
Not tea, sun, tea, pran, sukhan is very well known, but it is quite a powerful verse.

11
00:01:37,660 --> 00:01:48,380
The story is again quite a simple story, not much to it, but it has a lesson for us.

12
00:01:48,380 --> 00:02:02,220
There was a young man and a young woman who were betrothed and married and were brought

13
00:02:02,220 --> 00:02:13,380
together to give arms to the Buddha and all the monks.

14
00:02:13,380 --> 00:02:25,940
And the young woman said about to provide food and drink for the monks, but the young

15
00:02:25,940 --> 00:02:36,380
man when he saw the young woman, when he just looking at her, watching her go about her activities.

16
00:02:36,380 --> 00:02:45,740
He was suddenly seized with a great desire, a great lust, and forgot all about his duties

17
00:02:45,740 --> 00:02:53,140
and didn't help her in any way with preparing the food or offering the food.

18
00:02:53,140 --> 00:03:05,580
He just stood there watching her and the lust filled his mind and it filled his mind

19
00:03:05,580 --> 00:03:17,820
to such an extent that it completely forgot where he was in a very sort of somber, well,

20
00:03:17,820 --> 00:03:30,300
reverential atmosphere, religious atmosphere, it's like when you're in church, forgot

21
00:03:30,300 --> 00:03:39,340
where he was, and reached out his hand, or hands maybe, he was going to grab her and it

22
00:03:39,340 --> 00:03:43,940
doesn't say what he was planning to do, I don't know that he even had any plans, maybe

23
00:03:43,940 --> 00:03:54,500
and just, well, in any way, he would not need to go there.

24
00:03:54,500 --> 00:03:59,460
And as he did so, all the Buddha realized what was happening and stopped him.

25
00:03:59,460 --> 00:04:10,740
They say the Buddha caused him to not see his wife or his betrothed and when he couldn't

26
00:04:10,740 --> 00:04:18,980
see his wife, he turned and looked at the Buddha and sobered up a little maybe and the

27
00:04:18,980 --> 00:04:33,180
Buddha said to him, this is a fire, you are caught up in a fire, a wildfire and then he

28
00:04:33,180 --> 00:04:41,940
taught this verse.

29
00:04:41,940 --> 00:04:51,020
So the story is one of any number of stories that exemplifies this verse quite well and

30
00:04:51,020 --> 00:04:55,860
the lesson I think is one and the same in the story and in the verse, they go all along

31
00:04:55,860 --> 00:05:03,380
very well, they go together very well, this verse provides a very good reflection I think

32
00:05:03,380 --> 00:05:13,780
for all of us, it's a good reflection on craving and desire in general.

33
00:05:13,780 --> 00:05:23,980
When you want anything, how much of our lives is caught up in desire, passion and how

34
00:05:23,980 --> 00:05:31,580
like a fire is it that it burns everything and destroys everything, the Buddha said there's

35
00:05:31,580 --> 00:05:39,900
no fire like passion, but it's very much like any other fire, it's just so far, far more

36
00:05:39,900 --> 00:05:48,860
dangerous and destructive, it's because of passion that we steal and kill, because of passion

37
00:05:48,860 --> 00:06:03,500
that rape is committed, it's because of passion that we destroy friendships and relationships

38
00:06:03,500 --> 00:06:15,940
committing adultery and impropriety, giving up any sense of what might be right or proper,

39
00:06:15,940 --> 00:06:27,300
even looking at propriety aside, what is right, it makes us forget, it destroys so much

40
00:06:27,300 --> 00:06:32,820
in the mind, it obliterates, like if you imagine that during this ceremony with all the

41
00:06:32,820 --> 00:06:38,020
monks in the Buddha and as to suppose they were both offering quite respectfully and then

42
00:06:38,020 --> 00:06:54,060
the fire broke out, they would destroy the whole ceremony of the whole procession of events,

43
00:06:54,060 --> 00:07:03,260
the whole craving does that as well, it breaks the goodness, ethics, the wholesomeness,

44
00:07:03,260 --> 00:07:15,860
it destroys propriety and goodness and all other things, it steam rolls them all in favor

45
00:07:15,860 --> 00:07:22,540
of its desires, when we want something we'll just take it, and like any fire it grows,

46
00:07:22,540 --> 00:07:28,740
fire doesn't say okay you fed me and I'm full and that's enough, no fire and craving

47
00:07:28,740 --> 00:07:37,500
like it, it's not something you can satisfy, fire the more it gets, the more it wants, the

48
00:07:37,500 --> 00:07:47,540
more it gets, the more it takes, craving is the same, passion is the same, they become

49
00:07:47,540 --> 00:07:55,380
more and more invested in the fuel, whatever it is that fuels your passion on, you want

50
00:07:55,380 --> 00:08:05,100
it more and more intensely such that you'll do more to get it, you need to get more of

51
00:08:05,100 --> 00:08:17,380
it, more intense until you find yourself forgetting all of the good, all of what's good and

52
00:08:17,380 --> 00:08:30,780
right and just forgetting really, so obliterate it in your search for satisfaction, not

53
00:08:30,780 --> 00:08:34,980
even thinking that you're going to be satisfied or not going to be satisfied, you just

54
00:08:34,980 --> 00:08:43,580
blind and caught up in the fire of it, out of control, that's what addiction is, addiction

55
00:08:43,580 --> 00:09:00,980
to anything, it inflames the mind, and it leads to Dosa, it leads to anger, the

56
00:09:00,980 --> 00:09:09,100
why the Buddha mentions anger as being nutty, nutty, Dosa, Samakali, there's no misfortune

57
00:09:09,100 --> 00:09:17,460
like anger is because the real problem with passion is that it leads you to want and want

58
00:09:17,460 --> 00:09:25,620
more intensely and the same time makes you vulnerable to displeasure when you don't get

59
00:09:25,620 --> 00:09:34,020
what you want, leads to anger when you don't get what you want or anger when you're

60
00:09:34,020 --> 00:09:47,260
obstructed, when you're given things other than what you want, Dosa is considered the

61
00:09:47,260 --> 00:09:53,900
Buddha, something quite interesting to say that it's a Kali, Kali is often used to describe

62
00:09:53,900 --> 00:10:00,340
misfortune, like an unlucky throw of the dice, if you throw some dice and you get

63
00:10:00,340 --> 00:10:09,460
a bad roll, no bad luck, it's a Kali, but it's also used to mean evil and general or

64
00:10:09,460 --> 00:10:13,580
it's used to describe evil so it doesn't necessarily literally mean this fortune, but

65
00:10:13,580 --> 00:10:17,180
it's interesting that they're put together, Kali is not a word that you hear it on

66
00:10:17,180 --> 00:10:28,100
quite often, so I think the sense of misfortune is meant to be retained there, it's a misfortune

67
00:10:28,100 --> 00:10:35,380
in the same way that if you throw the dice and if you throw them wrong bad things happen,

68
00:10:35,380 --> 00:10:45,220
you lose your money, you lose, you lose out, anger causes you to lose everything, craving

69
00:10:45,220 --> 00:10:56,220
burns and all out, but as long as the fire is going, there's a fire, there's something

70
00:10:56,220 --> 00:11:00,140
you're too caught up to realize what's gone and then when the fire burns out, when the

71
00:11:00,140 --> 00:11:08,860
fuel runs out, you're left with the ashes and the ashes are like anger, you can no longer

72
00:11:08,860 --> 00:11:19,660
get what you want and you're left with these, it's just ashes, anger is like ashes, and

73
00:11:19,660 --> 00:11:32,260
when you get angry, it's a misfortune in the sense that everything you do means to

74
00:11:32,260 --> 00:11:41,460
suffering, but it's unlike any other kind of misfortune because if you have bad luck, maybe

75
00:11:41,460 --> 00:11:47,300
you nine out of ten times you throw the dice and you get the wrong rule and you lose

76
00:11:47,300 --> 00:11:51,740
a lot of your money, but you still have a chance to make a right rule no matter how bad

77
00:11:51,740 --> 00:11:56,740
luck, bad your luck is, but with anger it's not like that, anger can never lean to a

78
00:11:56,740 --> 00:12:01,100
good result.

79
00:12:01,100 --> 00:12:07,700
When people fight husbands and wives and partners fight with each other, when parents fight

80
00:12:07,700 --> 00:12:14,260
with children, when friends fight with friends or enemies fight over things that they want,

81
00:12:14,260 --> 00:12:21,740
if they fight out of greed or even just fighting out of hatred, fighting out of delusion,

82
00:12:21,740 --> 00:12:32,580
then we destroy all good, we lose everything, we lose your friendships, you lose your relationships,

83
00:12:32,580 --> 00:12:42,900
you can lose your life and health, it's a great misfortune, the greatest, there's no

84
00:12:42,900 --> 00:12:53,060
misfortune like anger, nati kanda samadukha is a reminder in the same vein, it's in

85
00:12:53,060 --> 00:13:09,140
the same reflection reflecting on desire, passion, remembering about how the objects of

86
00:13:09,140 --> 00:13:19,900
our desire are ultimately just experience, kanda the aggregates refers to, that which makes

87
00:13:19,900 --> 00:13:26,900
up an experience when you see or hear or smell or taste or feel or think, the pre-moment

88
00:13:26,900 --> 00:13:35,540
of that there's an experience, there's the five aggregates, the form is the physical aspect,

89
00:13:35,540 --> 00:13:41,620
everything is like, as it knows, the pleasure or pain or neutral feeling associated

90
00:13:41,620 --> 00:13:49,380
with it, samya is the recognition of what you see or hear or so on, samkara is the

91
00:13:49,380 --> 00:13:56,260
reaction to it, minyan is the awareness of it, all of that is those five things come together

92
00:13:56,260 --> 00:14:08,740
or arise at every experience and what it means is that that's all there is and so the

93
00:14:08,740 --> 00:14:18,340
teaching on why this is dukha is quite profound and radical and it's an important part

94
00:14:18,340 --> 00:14:28,060
of the Buddha's teaching, the core part of the Buddha's teaching, because what this man

95
00:14:28,060 --> 00:14:32,980
is lusting after in his wife is not actually his wife, it's the five aggregates, it's

96
00:14:32,980 --> 00:14:42,340
the experiences and through his not being able to see that, he's coming to great suffering,

97
00:14:42,340 --> 00:14:53,420
he's obliterating all the good and so on, but in the teaching that the five aggregates

98
00:14:53,420 --> 00:15:00,100
are, there's no suffering like them, as much as a much deeper sort of teaching, it shows

99
00:15:00,100 --> 00:15:08,980
what he's lacking, what he's unable to see, he's only able to see that suffering comes

100
00:15:08,980 --> 00:15:15,700
from not getting what he wants, not that it's, the system is unable to grant him what

101
00:15:15,700 --> 00:15:27,820
he wants because it's only made up of moments of experience.

102
00:15:27,820 --> 00:15:32,900
Ordinary suffering, we think of suffering, nukkawedana and we get a painful feeling or any

103
00:15:32,900 --> 00:15:38,740
feeling that's not the one we want and we don't get a pleasant feeling where bored

104
00:15:38,740 --> 00:15:46,580
and frustrated, we get painful feelings, doubly so, not only did we not get what we want

105
00:15:46,580 --> 00:16:01,660
and we got what we didn't want, so we become doubly frustrated, pain becomes our enemy,

106
00:16:01,660 --> 00:16:13,500
so we understand suffering as being a way at which we don't want, we try to avoid it, but

107
00:16:13,500 --> 00:16:20,300
we come to see that, oh, in reality there's suffering that we can't avoid, we can't possibly

108
00:16:20,300 --> 00:16:26,140
avoid suffering in life, it's going to come to us in various forms, we see nukkawedana,

109
00:16:26,140 --> 00:16:35,420
that's what leads people to come to practice meditation, we see suffering more deeply.

110
00:16:35,420 --> 00:16:40,780
And when they start to practice meditation, because they've seen they can't escape suffering,

111
00:16:40,780 --> 00:16:48,780
they start to realize that it's a characteristic of reality, it's going to do kalakana,

112
00:16:48,780 --> 00:16:53,500
this understanding of suffering helps to the person to let go, as they start to see

113
00:16:53,500 --> 00:17:05,420
them, it's not suffering as a concept, as a reality, as a thing, as a subject, as a topic,

114
00:17:07,580 --> 00:17:14,220
suffering is not something that you can avoid because it's inherent in everything, meaning

115
00:17:14,220 --> 00:17:22,540
it comes from clinging, it doesn't matter what you cling to, it doesn't matter what you crave for,

116
00:17:25,900 --> 00:17:31,100
seeing things as providing a satisfaction, thinking that they're going to make you happy,

117
00:17:32,540 --> 00:17:35,820
it's the problem, so it leads us to suffer.

118
00:17:35,820 --> 00:17:43,580
And as you see that more and more and more clearly, you come to the moment where you see that

119
00:17:45,980 --> 00:17:52,780
nati kandas samandaka, you see that the aggregates experience itself,

120
00:17:54,780 --> 00:17:59,500
it's a different kind of understanding, because why I said radical is because it sounds

121
00:17:59,500 --> 00:18:04,460
very awful to think that experience is inherently suffering, and it's not really what it means,

122
00:18:04,460 --> 00:18:09,660
it's not exactly, it's on a whole different level, so this one really, I think,

123
00:18:11,100 --> 00:18:15,260
you can take the Buddha's words literally, it's not like any other suffering,

124
00:18:15,900 --> 00:18:18,860
other kinds of suffering as they will get that suffering all stay away from it,

125
00:18:19,500 --> 00:18:25,980
but the suffering of the aggregates is not like that, it's in a sense a noble sort of suffering,

126
00:18:25,980 --> 00:18:30,620
and you remember that understanding suffering is what we're trying to do as Buddhists,

127
00:18:30,620 --> 00:18:34,540
we're not trying to run away from it, we're trying to understand it, mainly because while it's

128
00:18:34,540 --> 00:18:41,100
suffering, it's bad, right? But more deeply, we're trying to understand rather than,

129
00:18:41,100 --> 00:18:45,340
we're trying to change the way we look at suffering rather than trying to run away from it.

130
00:18:46,940 --> 00:18:51,260
As freedom from suffering, if you think it's just in your painful feelings, you just run away

131
00:18:51,260 --> 00:18:59,740
from the feelings. If you think it's a part of reality, well, then you go and practice to

132
00:18:59,740 --> 00:19:04,220
change the way you understand reality, if it's part of all experiences,

133
00:19:06,780 --> 00:19:11,020
then you look deeper at why that is, and you start to realize it's not actually the experiences,

134
00:19:11,660 --> 00:19:18,380
to say that their suffering is not quite accurate, but they can't satisfy you. The suffering

135
00:19:18,380 --> 00:19:23,820
comes when you cling to anything, when you cling to experience, and in fact seeing

136
00:19:23,820 --> 00:19:30,780
experience for what it is is what releases you from suffering. So by paying attention to the

137
00:19:30,780 --> 00:19:36,380
aggregates, you come to understand things much more clearly, and it's a sort of the highest form of

138
00:19:36,380 --> 00:19:44,540
suffering, because it's seeing that truth, the truth of suffering, the truth of not being able to

139
00:19:44,540 --> 00:19:54,060
satisfy, and that causes you to let go and free yourself. Become free from suffering.

140
00:19:55,980 --> 00:20:02,220
Attain or experience a state that is free from any kind of stress or any that is completely

141
00:20:02,220 --> 00:20:09,180
peace, let's put it that way. Which leads to the fourth part, nut-tee-san-tee-parang-su-kang,

142
00:20:09,180 --> 00:20:18,620
the most famous part of the verse. There is no happiness apart from peace, which is different from

143
00:20:18,620 --> 00:20:25,580
the others. It's a different formula. Here we're not saying pieces unlike any other kind of

144
00:20:25,580 --> 00:20:31,100
happiness. The Buddha is actually saying, there is no other happiness. Anything else that

145
00:20:31,100 --> 00:20:39,260
you thought was happiness. The point is I think happiness itself is a bit of a red herring, it's a

146
00:20:39,260 --> 00:20:48,460
bit misleading. The problem with happiness is you have to like it, and the problem with any other

147
00:20:48,460 --> 00:20:56,380
type of happiness. It's not really happiness because you have to like it. If you said it's okay

148
00:20:56,380 --> 00:21:04,140
to be happy, but just don't like it. It defeats the whole purpose of happiness. You don't like

149
00:21:04,140 --> 00:21:09,420
it. You can't really call it happiness. That's how our formulation goes. It's happiness because I

150
00:21:09,420 --> 00:21:19,900
like it, and that's why we say happiness is subjective. That's what makes one person happy,

151
00:21:19,900 --> 00:21:24,860
doesn't make another person happy. Why? Because one person likes it and another person doesn't

152
00:21:24,860 --> 00:21:32,860
like it. There are feelings that anyone could say, okay, we all have this feeling, and we

153
00:21:32,860 --> 00:21:40,380
could say that's a happy feeling, but without liking, it's not really something you would seek out.

154
00:21:40,380 --> 00:21:45,820
It's only because we like that feeling. Even in meditation, we want to experience good feelings.

155
00:21:45,820 --> 00:21:54,700
We like them and so we seek them out. But there is an exception. I think peace provides

156
00:21:54,700 --> 00:21:59,420
that exception. You don't have to like peace or even seek it out for it to be worthwhile.

157
00:22:01,260 --> 00:22:05,420
It's a better kind of happiness. It's the Buddha said the only type of

158
00:22:05,420 --> 00:22:11,260
the only kind of happiness. Peace doesn't require liking. It isn't involved with liking.

159
00:22:11,260 --> 00:22:18,060
It doesn't even involve with feelings necessarily. Peace is like freedom. You don't need

160
00:22:18,060 --> 00:22:21,980
something to be peaceful. You don't need something to be free. You just need to get rid of all the

161
00:22:21,980 --> 00:22:29,820
stuff that's getting it caught up and causing you to be unpeaceful.

162
00:22:35,500 --> 00:22:41,820
So this is a good reflection. When you're caught up in the fire of passion and you really

163
00:22:42,700 --> 00:22:48,540
are out of control, remind yourself that this is a fire. You're destroying yourself. You're destroying

164
00:22:48,540 --> 00:22:56,220
the mind. You're cultivating. You're growing. Nothing. You're not growing happiness. You're

165
00:22:56,220 --> 00:23:03,660
growing. Nothing but anger. And you're caught up in things that can't possibly satisfy you

166
00:23:03,660 --> 00:23:10,780
because ultimately it's just the aggregate which are unsatisfying. The only thing that's truly

167
00:23:10,780 --> 00:23:18,460
you can say is satisfying is peace. It doesn't require any sort of liking to be peaceful.

168
00:23:19,420 --> 00:23:23,980
So, very good verse. Something we should all maybe even memorize. This would be a good one to

169
00:23:23,980 --> 00:23:33,500
memorize. And certainly a good one to reflect on. That's the demo for tonight. Thank you all for

170
00:23:33,500 --> 00:23:48,780
listening.

