1
00:00:00,000 --> 00:00:29,000
One of my afraid of women, police officers, why am I afraid of women, it's not a fear,

2
00:00:29,000 --> 00:00:34,000
it's a sort of awareness.

3
00:00:34,000 --> 00:00:38,000
And I say that sort of tongue in cheek.

4
00:00:38,000 --> 00:00:43,000
But, you know, we're talking about people who maybe are not Buddhist

5
00:00:43,000 --> 00:00:49,000
or who are not stable in their mind.

6
00:00:49,000 --> 00:00:54,000
Or, you know, you could say ordinary people, but they can be

7
00:00:54,000 --> 00:01:00,000
sometimes a little bit extreme on the side of attachment.

8
00:01:00,000 --> 00:01:02,000
And you get a lot of these.

9
00:01:02,000 --> 00:01:06,000
So you find people who cling to you.

10
00:01:06,000 --> 00:01:10,000
And that can be quite difficult as a monk.

11
00:01:10,000 --> 00:01:14,000
You know, you've got to be nice to people and kind,

12
00:01:14,000 --> 00:01:17,000
but sometimes you can be really, really kind to people

13
00:01:17,000 --> 00:01:24,000
and they can fall into some kind of attachment towards you.

14
00:01:24,000 --> 00:01:28,000
And I suppose the other thing is just how, you know,

15
00:01:28,000 --> 00:01:30,000
you get in so much trouble as a monk.

16
00:01:30,000 --> 00:01:33,000
Like sometimes you'd be alone in a place with a woman

17
00:01:33,000 --> 00:01:37,000
and someone sees you and suddenly the word gets around

18
00:01:37,000 --> 00:01:42,000
that you're sleeping with some with a woman.

19
00:01:42,000 --> 00:01:43,000
And so on.

20
00:01:43,000 --> 00:01:45,000
So you have to be really careful.

21
00:01:45,000 --> 00:01:47,000
That's the first thing.

22
00:01:47,000 --> 00:01:48,000
The second thing.

23
00:01:48,000 --> 00:01:50,000
Why am I afraid of police officers?

24
00:01:50,000 --> 00:01:54,000
Well, I'm living in America now.

25
00:01:54,000 --> 00:01:57,000
I'm not living in a Buddhist country.

26
00:01:57,000 --> 00:02:02,000
And so not to say anything.

27
00:02:02,000 --> 00:02:04,000
Actually, the problem isn't really police officers.

28
00:02:04,000 --> 00:02:09,000
It's more people who, you know, for whatever reason,

29
00:02:09,000 --> 00:02:16,000
through fear of the unknown or through just overall paranoia,

30
00:02:16,000 --> 00:02:18,000
caution, I don't know.

31
00:02:18,000 --> 00:02:23,000
You can often get overreact towards strange things,

32
00:02:23,000 --> 00:02:27,000
like things that seem strange to them like a monk.

33
00:02:27,000 --> 00:02:30,000
So last year I was put in jail.

34
00:02:30,000 --> 00:02:34,000
Someone called the police and claimed that I had,

35
00:02:34,000 --> 00:02:38,000
I was walking around in the forest playing with myself

36
00:02:38,000 --> 00:02:43,000
or opening my robes up and showing my private parts to people.

37
00:02:43,000 --> 00:02:45,000
And by the time I talked to the police,

38
00:02:45,000 --> 00:02:50,000
it had escalated into this story where I had run around

39
00:02:50,000 --> 00:02:54,000
showing, you know, opening my robes

40
00:02:54,000 --> 00:02:57,000
and showing my private parts to a man and his daughters.

41
00:02:57,000 --> 00:02:58,000
And I just crazy stuff.

42
00:02:58,000 --> 00:03:01,000
And there was a whole crowd of people that apparently saw me.

43
00:03:01,000 --> 00:03:06,000
I was trying to think if maybe I had gone unconscious

44
00:03:06,000 --> 00:03:09,000
or something.

45
00:03:09,000 --> 00:03:12,000
The killer turns out to be the innocent guy

46
00:03:12,000 --> 00:03:15,000
because he did it while he was in some kind of state.

47
00:03:15,000 --> 00:03:18,000
So I was thinking, wow, you know, I mean, is it possible

48
00:03:18,000 --> 00:03:19,000
that I could have happened?

49
00:03:19,000 --> 00:03:22,000
But what I was doing, I was meditating off in the forest,

50
00:03:22,000 --> 00:03:25,000
but it happened to be near a public beach.

51
00:03:25,000 --> 00:03:27,000
You know, they in the coast of California,

52
00:03:27,000 --> 00:03:29,000
by the ocean, they have these wonderful parks,

53
00:03:29,000 --> 00:03:31,000
but it's also near a public beach.

54
00:03:31,000 --> 00:03:33,000
I didn't go to the beach.

55
00:03:33,000 --> 00:03:41,000
But there was, you know,

56
00:03:41,000 --> 00:03:46,000
there's been many, many, many example

57
00:03:46,000 --> 00:03:49,000
or many cases up there of streakers

58
00:03:49,000 --> 00:03:51,000
because there's a new just colony up the road

59
00:03:51,000 --> 00:03:52,000
or something like that.

60
00:03:52,000 --> 00:03:54,000
So they get these kind of things all the time,

61
00:03:54,000 --> 00:03:56,000
apparently, which it was totally unbeknownst to me.

62
00:03:56,000 --> 00:03:59,000
I was just looking for a nice place to meditate.

63
00:03:59,000 --> 00:04:00,000
And I happened to make a video.

64
00:04:00,000 --> 00:04:02,000
I had set my camera up.

65
00:04:02,000 --> 00:04:04,000
I think that's what this guy saw.

66
00:04:04,000 --> 00:04:06,000
He saw me walking around looking for a good place

67
00:04:06,000 --> 00:04:07,000
to set up my video,

68
00:04:07,000 --> 00:04:09,000
and I must have looked like a crazy person

69
00:04:09,000 --> 00:04:11,000
walking around in circles.

70
00:04:11,000 --> 00:04:15,000
And so I spent a nice evening in the local,

71
00:04:15,000 --> 00:04:16,000
the state.

72
00:04:16,000 --> 00:04:18,000
I think the state pen attention.

73
00:04:18,000 --> 00:04:19,000
I don't know what it was.

74
00:04:19,000 --> 00:04:22,000
Some kind of state prison, just in a holding cell,

75
00:04:22,000 --> 00:04:24,000
until someone bailed me out.

76
00:04:24,000 --> 00:04:25,000
It was freaky.

77
00:04:25,000 --> 00:04:28,000
After that, I've had quite a phobia of police officers.

78
00:04:28,000 --> 00:04:31,000
And just recently, I was stopped by a police officer here.

79
00:04:31,000 --> 00:04:35,000
On my arms round, because he'd had someone call in

80
00:04:35,000 --> 00:04:37,000
and ask me who I was and what I was holding

81
00:04:37,000 --> 00:04:40,000
and what I was hiding underneath my robes

82
00:04:40,000 --> 00:04:42,000
because I carry this big bowl around.

83
00:04:42,000 --> 00:04:44,000
So that's the second thing I'm afraid of.

84
00:04:44,000 --> 00:04:47,000
But I suppose those aren't real fears.

85
00:04:47,000 --> 00:04:50,000
I think something that I could really say

86
00:04:50,000 --> 00:04:55,000
that I'm a fear of is fear of failure

87
00:04:55,000 --> 00:04:58,000
that I recognize this in myself.

88
00:04:58,000 --> 00:05:01,000
I don't think it's totally unhealthy.

89
00:05:01,000 --> 00:05:03,000
I think some sort of fear of failure

90
00:05:03,000 --> 00:05:05,000
is good to propel you on.

91
00:05:05,000 --> 00:05:12,000
But it's the idea that somehow I might lose my grasp

92
00:05:12,000 --> 00:05:14,000
on the path.

93
00:05:14,000 --> 00:05:17,000
So I'm often watching myself

94
00:05:17,000 --> 00:05:20,000
and concerned that some of the things

95
00:05:20,000 --> 00:05:25,000
that I have to engage in like teaching

96
00:05:25,000 --> 00:05:29,000
might get in the way or take time away from my own practice.

97
00:05:29,000 --> 00:05:33,000
And so I often worry about that.

98
00:05:33,000 --> 00:05:37,000
Because it's something that really used to devastate me.

99
00:05:37,000 --> 00:05:41,000
Something that was really a phobia of mine

100
00:05:41,000 --> 00:05:44,000
would be disrobing.

101
00:05:44,000 --> 00:05:48,000
When I ordained 18 monks

102
00:05:48,000 --> 00:05:50,000
ordained on the same day at the same time,

103
00:05:50,000 --> 00:05:52,000
we had to ordain 18 of us three at a time,

104
00:05:52,000 --> 00:05:56,000
six times, and I was in the last group.

105
00:05:56,000 --> 00:06:00,000
And then after I ordained,

106
00:06:00,000 --> 00:06:02,000
I left Thailand to go back to Canada.

107
00:06:02,000 --> 00:06:04,000
When I came back to Thailand again,

108
00:06:04,000 --> 00:06:07,000
I was the only one left out of 18.

109
00:06:07,000 --> 00:06:10,000
The rest of them had all disrobed.

110
00:06:10,000 --> 00:06:14,000
So the survival rate is very low.

111
00:06:14,000 --> 00:06:17,000
And most of them are ordaining for whatever reason,

112
00:06:17,000 --> 00:06:20,000
just for cultural reasons.

113
00:06:20,000 --> 00:06:26,000
Still, you see some pretty powerful monks disrobing.

114
00:06:26,000 --> 00:06:28,000
And so I've never had any thought

115
00:06:28,000 --> 00:06:30,000
that I want to disrobed at all,

116
00:06:30,000 --> 00:06:34,000
never, not once in my almost 10 years as a monk.

117
00:06:34,000 --> 00:06:38,000
And yet, everyone's saying, oh, you'd be careful.

118
00:06:38,000 --> 00:06:39,000
And you never know.

119
00:06:39,000 --> 00:06:40,000
And suddenly it comes up.

120
00:06:40,000 --> 00:06:42,000
And you see these people who've been monks for 20 years.

121
00:06:42,000 --> 00:06:44,000
And then suddenly they want to disrobed.

122
00:06:44,000 --> 00:06:47,000
So I used to have this dream.

123
00:06:47,000 --> 00:06:50,000
I'd wait, you know those dreams where you're in school.

124
00:06:50,000 --> 00:06:56,000
And you're not wearing clothes.

125
00:06:56,000 --> 00:06:58,000
This is a common psychological dream

126
00:06:58,000 --> 00:07:03,000
that Freud or Yang or whoever talked about.

127
00:07:03,000 --> 00:07:05,000
It's a common phobia people have.

128
00:07:05,000 --> 00:07:07,000
You're going to school, or you're going to work,

129
00:07:07,000 --> 00:07:09,000
and you're not wearing your underwear.

130
00:07:09,000 --> 00:07:12,000
Well, the dream I had would be that I'd been going out.

131
00:07:12,000 --> 00:07:15,000
And I was in clothes.

132
00:07:15,000 --> 00:07:16,000
I was wearing clothes.

133
00:07:16,000 --> 00:07:18,000
I wasn't wearing a monk's robe.

134
00:07:18,000 --> 00:07:21,000
And I was devastated because it meant that I disrobed.

135
00:07:21,000 --> 00:07:23,000
It meant that I was no longer a monk.

136
00:07:23,000 --> 00:07:25,000
And I was freaking out.

137
00:07:25,000 --> 00:07:28,000
And just so sad and so upset about that.

138
00:07:28,000 --> 00:07:32,000
And then I'd wake up and realize, oh, I'm still a monk.

139
00:07:32,000 --> 00:07:34,000
Still, I didn't disrobed.

140
00:07:34,000 --> 00:07:37,000
And that happened several times in the first couple of years

141
00:07:37,000 --> 00:07:38,000
that I was a monk.

142
00:07:38,000 --> 00:07:42,000
Because it's something that's very important to me.

143
00:07:42,000 --> 00:07:49,000
And to lose it, just this fear that people instill in you

144
00:07:49,000 --> 00:07:51,000
that watch out, you never know.

145
00:07:51,000 --> 00:07:53,000
You're going to suddenly want to disrobe

146
00:07:53,000 --> 00:07:54,000
and you're not going to be able to.

147
00:07:54,000 --> 00:07:56,000
I don't know.

148
00:07:56,000 --> 00:07:57,000
It's a long haul.

149
00:07:57,000 --> 00:08:00,000
You're talking about a training that last years.

150
00:08:00,000 --> 00:08:03,000
We're talking about a lifelong training.

151
00:08:03,000 --> 00:08:06,000
So the point is, you don't know what could happen.

152
00:08:06,000 --> 00:08:09,000
So it's something to always be on the watch out for.

153
00:08:09,000 --> 00:08:13,000
So that's something I think that could be close to be called a fear.

154
00:08:13,000 --> 00:08:16,000
Certainly was a fear fear in the beginning.

155
00:08:16,000 --> 00:08:19,000
I suppose I'm not so afraid of it now.

156
00:08:19,000 --> 00:08:22,000
But, well, there's the question and there's my answer.

157
00:08:22,000 --> 00:08:25,000
So I hope that's useful.

158
00:08:25,000 --> 00:08:29,000
That's the latest installment in life in a day.

159
00:08:29,000 --> 00:08:44,000
Have a month.

