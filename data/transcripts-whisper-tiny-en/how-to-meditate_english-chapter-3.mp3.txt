How to meditate by you to Domobiku, read by Adirokes, chapter 3, walking meditation.
In this chapter, I will explain the technique of walking meditation.
As with sitting meditation, the focus of walking meditation is on keeping the mind in the
present moment and aware of phenomena as they arise in order to create clear awareness
of one's reality.
Even its similarity to sitting meditation, one might wonder what purpose walking meditation
serves.
If one is not able to practice walking meditation, one will still be able to gain benefit
from sitting meditation.
But walking meditation has several unique benefits that make it a good complement and precursor
to sitting meditation.
I will enumerate the five traditional benefits here.
These five benefits are taken from Lichten Kama Sinta in the Angutura Nikaya, 5.1.3.9.
First, walking meditation provides physical fitness.
If we spend all of our time sitting still, our bodies will become weak and incapable of exertion.
Walking meditation maintains basic fitness even for an ardent meditator and can be seen
as a supplement for physical exercise.
Second, walking meditation cultivates patience and endurance.
Since walking meditation is active, it doesn't require as much patience as sitting still.
It is a useful intermediary between ordinary activity and formal sitting meditation.
Third, walking meditation helps to cure sickness in the body.
Whereas sitting meditation puts the body in a state of homeostasis, walking meditation
stimulates blood flow and biological activity while being gentle enough to avoid aggravation.
It also helps to relax the body, reducing tension and stress by moving slowly and methodically.
Walking meditation is thus useful in helping to overcome sicknesses like heart disease
and arthritis, as well as maintaining basic general health.
Fourth, walking meditation aids in healthy digestion.
The greatest disadvantage to sitting meditation is that it can actually inhibit proper digestion
of food.
Walking meditation, on the other hand, stimulates the digestive system, allowing one to
continue one's meditation practice without having to compromise one's physical health.
Fifth, walking meditation helps one to cultivate balanced concentration.
If one were to only practice sitting meditation, one's concentration may be either
too weak or too strong, leading either to distraction or lethargy.
As walking meditation is dynamic, it allows both body and mind to settle naturally.
If done before sitting meditation, walking meditation will help ensure a balanced state of
mind during the subsequent sitting.
The method of walking meditation is as follows.
One, the feet should be close together, almost touching, and should stay side by side throughout
the meditation.
Either one in front of the other, nor with great space between their pads.
The hands should be clasped, right holding left, either in front or behind the body.
Two, the hands should be clasped, right holding left, either in front or behind the body.
Three, the eyes should be open throughout the meditation, and one's gaze should be fixed
on the path ahead, about two meters or six feet in front of the body.
Four, one should walk in a straight line, somewhere between three to five meters, or ten
to fifteen feet in length.
Five, one begins by moving the right foot forward one foot length, with the foot parallel
to the ground.
The entire foot should touch the ground at the same time, with back of the heel in line
width, and to the right of the toes of the left foot.
Six, the movement of each foot should be fluid and natural, a single arcing motion from
beginning to end, with no breaks or abrupt change in direction of any kind.
Seven, one then moves the left foot forward, passing the right foot to come down with the
back of the heel, in line width, and to the left of the toes of the right foot, and
so on, one foot length for each step.
Eight, as one moves each foot, one should make a mental note just as in the sitting meditation,
using a mantra that captures the essence of the movement as it occurs.
The word, in this case, is stepping right when moving the right foot, and stepping left
when moving the left foot.
Nine, one should make the mental note at the exact moment of each movement, neither
before or after the movement.
If the mental note, stepping right, is made before the foot moves, one is noting something
that has not yet occurred.
If one moves the foot first, and then notes, stepping right, one is noting something in
the past.
Either way, this cannot be considered meditation, as there is no awareness of reality in
either case.
To clearly observe the movements as they occur, one should note step at the beginning
of the movement, just as the foot leaves the floor, being as the foot moves forward,
and right the moment when the foot touches the floor again.
The same method should be employed when moving the left foot, and one's awareness should
move between the movements of each foot, from one end of the path to the other.
On reaching the end of the walking path, turn around and walk in the other direction.
The method of turning while maintaining clear awareness is to first stop, bringing whichever
foot is behind to stand next to the foot that is in front, saying to oneself, stopping,
stopping, stopping, as the foot moves.
Once one is standing still, one should become aware of the standing position as standing,
standing, standing, then begin to turn around as follows.
One, lift the right foot completely off the floor and turn it 90 degrees to place it again
on the floor, noting one's turning.
It is important to extend the word to cover the whole of the movement, so the turn should
be at the beginning of the movement, and the ping should be at the end, as the foot touches
the floor.
Two, lift the left foot off the floor and turn it 90 degrees to stand by the right foot,
putting just the same, turning.
Repeat the movements of both feet one more time, turning, right foot, turning, left foot,
and then note, standing, standing, standing.
Four, continue with the walking meditation in the opposite direction, noting, stepping
right, stepping left, as before.
During walking meditation, if thoughts, feelings, or emotions arise, one can choose to
ignore them, bringing the mind back to the feet in order to maintain focus and continuity.
If, however, they become a distraction, one should stop moving, bringing the backfoot forward
to stand with the front foot, saying to oneself, stopping, stopping, stopping, then
standing, standing, standing, and begin to contemplate the distraction as in sitting meditation,
thinking, thinking, thinking, pain, pain, pain, angry, sad, bored, happy, etc., according
to the experience.
Once the object of attention disappears, continue with the walking as before, stepping
right, stepping left.
In this way, one simply paces back and forth, walking in one direction until reaching
the end of the designated path, then turning around and walking in the other direction.
Generally speaking, one should try to balance the amount of time spent in walking meditation,
with the amount of time spent in sitting meditation, to avoid partiality to one or the
other posture.
If one were to practice 10 minutes of walking meditation, for example, one should follow
it with 10 minutes of sitting meditation.
This concludes the explanation of how to practice walking meditation.
Again, I urge you not to be content with simply reading this book.
Please, try the meditation techniques for yourself and see the benefits they bring for yourself.
Thank you for your interest in the meditation practice, and again, I wish you peace, happiness
and freedom from suffering.
