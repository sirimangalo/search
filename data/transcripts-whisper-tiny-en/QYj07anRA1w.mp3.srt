1
00:00:00,000 --> 00:00:08,000
What are some connections between physics and Buddhism?

2
00:00:08,000 --> 00:00:12,000
I'm not well versed in physics.

3
00:00:12,000 --> 00:00:29,960
Well, I think quantum physics has some important or has the potential to give some insight for modern people into the important

4
00:00:29,960 --> 00:00:33,960
science of the Buddha's teaching.

5
00:00:33,960 --> 00:00:44,960
Obviously, physics is not interested in the development of the mind.

6
00:00:44,960 --> 00:00:48,960
Physics doesn't talk about getting rid of greed, anger, and delusion.

7
00:00:48,960 --> 00:01:01,960
So in that sense, there is no connection with the goal of physics and the goal of Buddhism.

8
00:01:01,960 --> 00:01:17,960
But the framework of quantum physics allows people to let go of the idea of a three-dimensional space that exists like a world.

9
00:01:17,960 --> 00:01:36,960
And a being, and an objective external reality.

10
00:01:36,960 --> 00:02:02,960
Because the problem that quantum physics tried to address is why reality seems to depend very much on the observation as opposed to some objective laws of physics.

11
00:02:02,960 --> 00:02:11,960
Depending on the experiment, depending on the observation, there arises reality.

12
00:02:11,960 --> 00:02:28,960
So for modern thinkers and for secularists who may be who had been materialists before when they said a quantum physics, at least certain or orthodox quantum physics,

13
00:02:28,960 --> 00:02:34,960
it can help them to overcome their doubts about, for instance, the existence of the mind and so on.

14
00:02:34,960 --> 00:02:43,960
Physics doesn't deal with the existence of the mind, but quantum physics pretty much requires some interaction.

15
00:02:43,960 --> 00:02:47,960
It points out that we can only explain reality in terms of observation.

16
00:02:47,960 --> 00:03:02,960
If you understand quantum physics correctly, then it shows, or correctly, in a certain way, then it shows the truth of the Buddha time.

17
00:03:02,960 --> 00:03:22,960
There's a professor that I keep coming back to Henry Stapp, who actually quotes about his monk and explains how things like reincarnation could be totally possible through physics and so on, using quantum physics and so on.

18
00:03:22,960 --> 00:03:35,960
But I think the important point is that no, there's no overlap in the goals. Quantum physics, even quantum physics, never had any idea of changing the quality of one's mind.

19
00:03:35,960 --> 00:03:48,960
It's physics. You can't purify the physical, and so you might say that they complement each other in one sense.

20
00:03:48,960 --> 00:03:58,960
But that they totally seek out different goals. Buddhism is for the purification of the mind.

21
00:03:58,960 --> 00:04:04,960
Physics is for the understanding of the physical.

22
00:04:04,960 --> 00:04:25,960
I don't have much to say about this. I only saw one children's video about quantum physics and was really quite impressed about what their everything is, is waves.

23
00:04:25,960 --> 00:04:30,960
I couldn't say that I really understand that.

24
00:04:30,960 --> 00:04:35,960
That's the one with the double slate experiment. Exactly that, yeah.

25
00:04:35,960 --> 00:04:53,960
And it was so astonishing that the particles, when they were observed, behaved different from when they weren't observed.

26
00:04:53,960 --> 00:05:14,960
I think that's the important thing. Obviously there are many interpretations of quantum physics and many wacky theories about why it is and the idea of multiverse and so on.

27
00:05:14,960 --> 00:05:22,960
But as I said, if you understand it in a specific way, then it really is proof of the Buddha's teaching.

28
00:05:22,960 --> 00:05:26,960
But of course you have to. Everyone can have their theory of why it's happening.

29
00:05:26,960 --> 00:05:34,960
But the mind, in the Buddha said, Manopu, Bangamada, the mind comes first.

30
00:05:34,960 --> 00:05:39,960
The mind creates the world when the mind ceases, the world ceases.

31
00:05:39,960 --> 00:05:58,960
And we create our reality. It actually, the realizations of quantum physics, if understood in the orthodox sense, really say the same things that the Buddha was saying that everything you do affects the universe.

32
00:05:58,960 --> 00:06:08,960
Your actions have consequences. Even the slightest change of your attention or your mind.

33
00:06:08,960 --> 00:06:26,960
The mind will have consequences. So our greed, our anger, our delusion will have specific results and our wisdom and mindfulness will also have specific results.

