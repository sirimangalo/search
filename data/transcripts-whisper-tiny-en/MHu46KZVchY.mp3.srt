1
00:00:00,000 --> 00:00:09,000
Yes, sorry, I got disconnected there.

2
00:00:09,000 --> 00:00:22,400
So there's, sorry, what I've said before is that two halves don't always make a hole.

3
00:00:22,400 --> 00:00:31,400
You can't take two half-persons and make them into a hole person.

4
00:00:31,400 --> 00:00:37,200
And there was a story, there was a book about this, there's one of those new age books that talked

5
00:00:37,200 --> 00:00:45,600
about this, the Celestein prophecy, I think, brought this up and it talks about people who

6
00:00:45,600 --> 00:00:51,200
grasp for other people as being half-persons, not being whole in and of themselves, which

7
00:00:51,200 --> 00:00:58,600
is really an interesting philosophy and it mirrors, I think Buddhism quite well, but you can't

8
00:00:58,600 --> 00:01:00,800
expect to find happiness outside of yourself.

9
00:01:00,800 --> 00:01:07,400
This is something that we're being lied to, we're being taught things that are false.

10
00:01:07,400 --> 00:01:18,200
The idea that happiness is going to come from other people, that true happiness is because

11
00:01:18,200 --> 00:01:25,200
the idea of a person is something totally conceptual, all we have really is our experiences

12
00:01:25,200 --> 00:01:30,200
at seeing, hearing, smelling, tasting, feeling and thinking, we come up with the idea of the other

13
00:01:30,200 --> 00:01:40,200
person in our own minds, it's an illusion that we give rise to.

14
00:01:40,200 --> 00:01:56,800
And based on this fact, it leads to all sorts of clinging and it can only, in the end,

15
00:01:56,800 --> 00:02:10,160
lead to disappointment and dissatisfaction, because when the truth changes, when that person

16
00:02:10,160 --> 00:02:20,160
dies or gets older or when reality becomes different from your illusion, from your concept,

17
00:02:20,160 --> 00:02:27,440
which it has to do, then you will suffer, you will be upset and disappointed.

18
00:02:27,440 --> 00:02:33,760
It's bound up with attachment, really, that's really the point, you attach to something

19
00:02:33,760 --> 00:02:40,680
and you say, this is preferable to something else.

20
00:02:40,680 --> 00:02:45,960
You can philosophize about it all you want and say that we need to find, you can use words

21
00:02:45,960 --> 00:02:57,560
like life partners, they search the words that you're using, and it sort of makes it seem

22
00:02:57,560 --> 00:03:03,000
like there's some meaning to it, or there's some importance to it, but actually all that

23
00:03:03,000 --> 00:03:12,440
it is is a craving, it's an attachment to pleasure, and there's many different causes of

24
00:03:12,440 --> 00:03:17,040
that pleasure, you know, the pleasure of just having someone to support you because you

25
00:03:17,040 --> 00:03:23,440
feel weak, because you're not strong in and of yourself, the pleasure of physical contact,

26
00:03:23,440 --> 00:03:36,720
the pleasure of safety, the pleasure that comes from conversation, from interacting with

27
00:03:36,720 --> 00:03:45,360
other people, it's something that we enjoy, and because we enjoy it, we cling to it,

28
00:03:45,360 --> 00:03:54,960
we want it more, and we wind up expecting it, and the workings of the brain are a really

29
00:03:54,960 --> 00:04:01,840
good indicator of the truth of this, because if you look at addiction research, as I've

30
00:04:01,840 --> 00:04:09,880
talked about before, you can see how this process is working in the brain, the chemicals

31
00:04:09,880 --> 00:04:18,520
in the brain are released, and they attach to these receptors, and therefore it gives pleasure,

32
00:04:18,520 --> 00:04:23,840
and that's great, except the next time the receptors have been weakened or have been broken

33
00:04:23,840 --> 00:04:29,360
or it's like it's been overstretched, the system has been over-taxed, and as a result

34
00:04:29,360 --> 00:04:39,880
it requires more chemicals than last time, so more stimulus than last time.

35
00:04:39,880 --> 00:04:49,280
It's really from a Buddhist point of view, it's because of the accumulated force, you

36
00:04:49,280 --> 00:04:55,760
know, the addiction stacks on top of each other, and it becomes worse and worse, so when

37
00:04:55,760 --> 00:05:01,560
you have this much addiction, and you go for it, and you chase after the things that you're

38
00:05:01,560 --> 00:05:06,240
attached to, you're stacking on more addiction, and because you have now this much addiction,

39
00:05:06,240 --> 00:05:12,680
you need more, it needs more food, it's like this monster is getting bigger, and when

40
00:05:12,680 --> 00:05:20,160
the monster gets bigger, it needs more food, it's the power of the addiction, and the habit,

41
00:05:20,160 --> 00:05:27,160
you're creating a stronger habit, a stronger addiction, and you can see how that works

42
00:05:27,160 --> 00:05:31,200
in the brain, but it's exactly how it works in the mind.

43
00:05:31,200 --> 00:05:40,200
So when your happiness is based on these sorts of things and really anything external

44
00:05:40,200 --> 00:05:51,160
to yourself or anything external, any object is only going to, the only happiness it can

45
00:05:51,160 --> 00:05:56,000
bring you is one that is rooted in attachment, rooted in addiction, and as a result it can

46
00:05:56,000 --> 00:05:59,440
never satisfy you.

47
00:05:59,440 --> 00:06:03,120
So that's part of what you're hopefully addressed as part of what you're asking, but

48
00:06:03,120 --> 00:06:08,400
I think the other important thing to mention here is in regards to your example, that

49
00:06:08,400 --> 00:06:12,800
there are a lot of people out there who want to have a partner, but are suffering because

50
00:06:12,800 --> 00:06:15,480
they cannot find one.

51
00:06:15,480 --> 00:06:25,000
And this is a mistake that we often make, making the wrong connection, that we are suffering

52
00:06:25,000 --> 00:06:47,440
because, give to the woman, no, she needs to thank you, flashlights came, sorry you get

53
00:06:47,440 --> 00:06:51,080
to your question in a second just to finish up this one.

54
00:06:51,080 --> 00:06:57,280
So we say we are suffering because we don't get what we want, and the Buddha said

55
00:06:57,280 --> 00:07:07,400
himself, yampi changnalat, but nalabhatitampidu, yampi changnalabhatitampidu, whenever you

56
00:07:07,400 --> 00:07:13,440
want, not getting that, that is suffering.

57
00:07:13,440 --> 00:07:18,080
So we think, well yeah, then how do you get what you want, right?

58
00:07:18,080 --> 00:07:24,000
What can we do to get what we want, because not getting it is managed, or is suffering?

59
00:07:24,000 --> 00:07:28,680
But the point here is not that you're not getting what you want, the point here is that

60
00:07:28,680 --> 00:07:39,720
you want, what the Buddha was saying actually is that one thing, things sets you up for

61
00:07:39,720 --> 00:07:46,880
suffering, because when you don't get what you want, you suffer.

62
00:07:46,880 --> 00:07:54,240
So he was kind of, I think it's quite clear that he was taking it for granted, the understanding

63
00:07:54,240 --> 00:07:58,280
that you can't always get what you want.

64
00:07:58,280 --> 00:08:04,080
Now it was kind of like, well hopefully you don't believe that you can get what you want,

65
00:08:04,080 --> 00:08:09,320
and given that you don't believe that you can always get what you want, then one thing

66
00:08:09,320 --> 00:08:15,920
is going to set you up for suffering, and that's why the second noble truth is the cause

67
00:08:15,920 --> 00:08:26,200
of suffering is craving, or desire, because, and this is really the key of the Buddha's teaching.

68
00:08:26,200 --> 00:08:30,840
If you learn about Buddhism and what the Buddha taught, you'll learn that the second noble

69
00:08:30,840 --> 00:08:35,480
truth is the cause of suffering is craving.

70
00:08:35,480 --> 00:08:42,440
So it's not that people are not suffering because they can't find a partner, they're

71
00:08:42,440 --> 00:08:45,880
suffering because they want to find a partner in the first place.

72
00:08:45,880 --> 00:08:50,960
And what it translates, what it means, the essence of it is, they're not content with

73
00:08:50,960 --> 00:08:55,200
their lives, they're not content with reality, they're not balanced, they're not here

74
00:08:55,200 --> 00:09:01,560
and now, they're not in tune with things as they are.

75
00:09:01,560 --> 00:09:05,960
They don't have the understanding yet of the reality around them, which, I mean it's

76
00:09:05,960 --> 00:09:10,520
not to say they're bad people, this is the state that we're all in.

77
00:09:10,520 --> 00:09:14,960
So I'm not trying to say that though these people are inferior in some way, this is

78
00:09:14,960 --> 00:09:16,600
how we are.

79
00:09:16,600 --> 00:09:21,800
What I'm trying to do is sort of flush out what is the path that we should take.

80
00:09:21,800 --> 00:09:28,600
We can either try to find a way so that we always get what we want, or we can try to

81
00:09:28,600 --> 00:09:31,760
find a way so that we have no more wants, no more wanting.

82
00:09:31,760 --> 00:09:34,160
I mean they're both, it's both the same goal.

83
00:09:34,160 --> 00:09:38,840
The goal in mind is to not have any more wanting, either you get everything that you want

84
00:09:38,840 --> 00:09:46,280
and are totally satisfied, or you give up wanting and are in the same way totally satisfied.

85
00:09:46,280 --> 00:09:53,160
But as I've said, the problem is that it may sound logical that way, but it's reality

86
00:09:53,160 --> 00:09:54,640
is something totally different.

87
00:09:54,640 --> 00:10:00,040
You can't become satisfied by always getting what you want, it's impossible.

88
00:10:00,040 --> 00:10:04,160
You're developing the habit of craving, the habit of addiction.

89
00:10:04,160 --> 00:10:12,920
If you go the other way and develop the habit, the way of contentment, then you'll be

90
00:10:12,920 --> 00:10:18,000
developing more and more contentment until eventually you're content with everything.

91
00:10:18,000 --> 00:10:20,160
And as a result, you have no more wanting.

92
00:10:20,160 --> 00:10:40,160
So that way actually works, they're not parallel and they don't both work.

