1
00:00:00,000 --> 00:00:14,760
Okay, good evening, everyone. Welcome to our evening dhamma session.

2
00:00:14,760 --> 00:00:30,760
Topic of tonight's talk is failure, so word we're all afraid of, right?

3
00:00:30,760 --> 00:00:48,240
Frame of being a failure. You ever felt like a failure in general, we place our

4
00:00:48,240 --> 00:00:57,600
hopes and our dreams on success as to other people, place their hopes and dreams

5
00:00:57,600 --> 00:01:06,480
on our success, our parents. Sometimes there are some, sometimes some people have

6
00:01:06,480 --> 00:01:19,280
intense pressure put on them by their parents to succeed and if they don't they're

7
00:01:19,280 --> 00:01:30,000
made to feel like a failure. Sometimes society puts pressures on us to succeed our

8
00:01:30,000 --> 00:01:45,680
neighbors, our friends, maybe everyone else is succeeding and boasting or proud of

9
00:01:45,680 --> 00:01:54,000
their achievements, and we feel like a failure because we've achieved nothing.

10
00:01:54,000 --> 00:02:04,040
It's a mental illness of sorts, it's a need to succeed and this attachment to

11
00:02:04,040 --> 00:02:12,320
failure or feeling like a failure. Because of course in Buddhism there's not

12
00:02:12,320 --> 00:02:28,080
really such a thing as failure, or maybe more, we're always failing. We fail until

13
00:02:28,080 --> 00:02:39,040
we succeed. So a question of whether you're succeeding or failing, we're

14
00:02:39,040 --> 00:02:48,480
failing, we're failing to see the truth, and we'll just keep failing and

15
00:02:48,480 --> 00:02:55,360
failing and failing from life to life, from life to life, time after life, time

16
00:02:55,360 --> 00:03:06,760
after lifetime. Until finally we see the truth, that's all there is to it. So when we

17
00:03:06,760 --> 00:03:15,080
bring these attitudes into meditation, when they cause us great stress and

18
00:03:15,080 --> 00:03:21,920
the meditators often feel like their failure and are very worried about

19
00:03:21,920 --> 00:03:26,560
failing the course, right? They come and take a course here while you think of

20
00:03:26,560 --> 00:03:43,880
it like any other course, it's like a pass or fail thing. A reality is not like

21
00:03:43,880 --> 00:03:59,320
that. First of all, we have eternity, we're not going to, you know, it's not

22
00:03:59,320 --> 00:04:07,760
like you wasted your life and that's it. But more importantly, it's not not

23
00:04:07,760 --> 00:04:12,600
really so much about what you accomplish and what you become, it's just about

24
00:04:12,600 --> 00:04:25,400
seeing the truth. So we're really looking at what it is that keeps us from seeing

25
00:04:25,400 --> 00:04:37,480
the truth. What is it that causes us to fail to see what's really right in

26
00:04:37,480 --> 00:04:56,080
front of us? And so it's a good opportunity for us to talk about some of the

27
00:04:56,080 --> 00:04:59,520
things that get in the way of our practice, right? I mean, what I want to

28
00:04:59,520 --> 00:05:05,720
impress upon you is that no one's going to fail the course. In fact, it's a

29
00:05:05,720 --> 00:05:11,880
great success even just to stay here and to do terribly at the course in the

30
00:05:11,880 --> 00:05:18,360
sense because you're learning about how incompetent you were learning about

31
00:05:18,360 --> 00:05:25,480
how incompetent we are, how unable we are to see the truth. You're a

32
00:05:25,480 --> 00:05:29,000
minute here, you might think they finish the course and be discouraged that

33
00:05:29,000 --> 00:05:35,360
they maybe didn't get the results they were hoping for or maybe they leave

34
00:05:35,360 --> 00:05:39,000
early and are discouraged by the fact that they couldn't finish the course

35
00:05:39,000 --> 00:05:51,440
and that's the wrong thinking. Because every moment that you're mindful

36
00:05:51,440 --> 00:05:58,400
and every insight you gain through being mindful is a success.

37
00:05:58,400 --> 00:06:10,000
That's one step closer to the goal. And so rather than talking about what makes

38
00:06:10,000 --> 00:06:16,400
you fail the course, because that doesn't happen or what you need to succeed.

39
00:06:16,400 --> 00:06:25,120
We talk about the sorts of things that you're going to have to overcome.

40
00:06:25,120 --> 00:06:33,600
It was in this course or next course or if you come back later, it's only a

41
00:06:33,600 --> 00:06:43,080
matter of time and a matter of ability. You know, you have to build up the skills

42
00:06:43,080 --> 00:06:56,040
for being mindful and energetic and concentrated and so on. But what gets in the

43
00:06:56,040 --> 00:07:00,600
way while the Buddha talked about eight things that get in the way of us

44
00:07:00,600 --> 00:07:03,320
seeing clearly.

45
00:07:03,320 --> 00:07:13,480
And these are actually quite practical. I mean this isn't such a deep teaching,

46
00:07:13,480 --> 00:07:17,960
although it gets quite deep near the end. I think this is a really good list.

47
00:07:17,960 --> 00:07:26,160
You'll see because it goes from the practical to the deep sublime teaching.

48
00:07:26,160 --> 00:07:36,240
So the first one, these are eight things that lead us to fail. Lead us to continue

49
00:07:36,240 --> 00:07:51,760
to fail and even to lose our way. And so the first one is kamaramata means to

50
00:07:51,760 --> 00:08:06,320
means a joy or getting caught up in or getting intoxicated by enjoyment of kamaras' work.

51
00:08:06,320 --> 00:08:14,240
So getting obsessed with or caught up in work.

52
00:08:14,240 --> 00:08:23,960
So the Buddha was actually thinking of for monks here, if monks go around working

53
00:08:23,960 --> 00:08:32,760
all the time. There's this story of this monk in the Dhamapada, I think, who in the

54
00:08:32,760 --> 00:08:36,120
morning he would sweep, in the afternoon he would sweep, and in the evening he would

55
00:08:36,120 --> 00:08:40,280
sweep, and the monks looked at them and said, wow, there's a real dedicated monk.

56
00:08:40,280 --> 00:08:47,040
Oh, we're sweeping. The morning you see him sweeping, in the afternoon you see him sweeping.

57
00:08:47,040 --> 00:08:55,640
And today you see these monks. It's how monks would sweep, sweep, sweep, sweep, work, work.

58
00:08:55,640 --> 00:08:59,680
So they went to the Buddha or the Buddha came to them and said, what are you talking about?

59
00:08:59,680 --> 00:09:04,960
Oh, I thought he would that monk. And the Buddha said, look, the person sweeps, they're

60
00:09:04,960 --> 00:09:14,920
called a janitor. So it's maybe not such an interesting story, but from a monastic point

61
00:09:14,920 --> 00:09:20,560
of view, it's kind of funny because there is a lot of praise given to monks who sweep.

62
00:09:20,560 --> 00:09:26,960
Some monk who keeps the monastery clean and some monk who is energetic and so on and so on.

63
00:09:26,960 --> 00:09:37,440
Or how the Buddha didn't have such high praise for janitors. Here in the meditation center,

64
00:09:37,440 --> 00:09:42,880
everyone, we've asked everyone to spend a little time doing something every day. Please

65
00:09:42,880 --> 00:09:51,320
keep that up. But if it's already clean, you don't have to go around cleaning it, you know?

66
00:09:51,320 --> 00:09:55,640
Clean what needs to be clean, but some people get caught up. I mean, the point is, especially

67
00:09:55,640 --> 00:10:01,760
during a meditation course, it's possible for you to try to find any excuse not to meditate.

68
00:10:01,760 --> 00:10:08,760
You know how this goes? The mind is a verse to something and then it finds ways to avoid it.

69
00:10:08,760 --> 00:10:16,600
And it becomes an obsession, finding ways to avoid the problem. Until finally, you catch yourself

70
00:10:16,600 --> 00:10:25,000
in your mindful and breakthrough. And of course, for those of you living in the world, it's

71
00:10:25,000 --> 00:10:32,920
important to understand. Working 80 hours a week, it's probably not going to be supportive

72
00:10:32,920 --> 00:10:40,920
of meditation practice or seeing things clearly. Don't get caught up in work. Take time.

73
00:10:40,920 --> 00:10:58,280
Number two, bus around with getting caught up or intoxicated, delighting in speech. Some

74
00:10:58,280 --> 00:11:04,560
people like to talk a lot, monks. Some monks would just sit around and talk and chat and

75
00:11:04,560 --> 00:11:14,400
gossip. I've been talking about the Dhamma. I've been to these Dhamma groups where everyone's

76
00:11:14,400 --> 00:11:22,080
just talking, talking, talking about the Dhamma. Conferences are the most fun. There's a conference

77
00:11:22,080 --> 00:11:28,160
this month and well, I will see, but I've been to some Buddhist conferences in my time.

78
00:11:28,160 --> 00:11:37,120
I mean, not to be overly critical and it's not good to talk bad about things and people,

79
00:11:37,120 --> 00:11:47,120
but I will say that there's a lot of talks sometimes. We're very good at talking about Buddhism

80
00:11:47,120 --> 00:11:55,440
sometimes. It's much easier to talk about it than to practice. I mean, in the meditation

81
00:11:55,440 --> 00:12:00,800
center, of course, it means don't sit around chatting with each other. I've always said the ideal

82
00:12:00,800 --> 00:12:06,800
meditator doesn't know the names of the other meditators. Sometimes I forget the meditators'

83
00:12:06,800 --> 00:12:15,600
names, but I'm supposed to know them and for all of the meditators. I mean, sometimes a meditator

84
00:12:15,600 --> 00:12:20,000
will come to me and they know not only their name but where they're from and all the other

85
00:12:20,000 --> 00:12:25,680
meditators, they know where the other meditators are from and a fair chunk of their life story.

86
00:12:26,400 --> 00:12:30,720
And you've got to wonder how that is when you're not supposed to be talking with each other.

87
00:12:33,760 --> 00:12:42,480
So during the course, it's, well, it's important because chatting, working, chatting, it's hard to

88
00:12:42,480 --> 00:12:52,160
be mindful. Your mind gets caught up in and the things. If you work a lot and you get tired,

89
00:12:52,160 --> 00:13:02,080
if you talk a lot, you get distracted. Number three, need daramatas, delighting in sleep.

90
00:13:03,760 --> 00:13:09,280
So the meditators are learning this as we tell them not to sleep more than six hours or four

91
00:13:09,280 --> 00:13:14,560
hours trying to sleep too much. And you realize how attached we are to sleep.

92
00:13:16,800 --> 00:13:21,680
And so attachment to sleep gets in the way of our practice. I was talking about that this morning,

93
00:13:21,680 --> 00:13:29,040
how you're marking on how, how more focused you are when you sleep less.

94
00:13:29,040 --> 00:13:36,080
It's not easy, you know, it's much more comfortable to sleep a lot.

95
00:13:40,480 --> 00:13:44,000
But during the time when you're asleep, you're certainly not mindful.

96
00:13:44,960 --> 00:13:51,680
And just the attachment to it, the pleasure, because it resets the time or it resets the state,

97
00:13:52,560 --> 00:13:54,880
you know, having to deal with all these challenging

98
00:13:54,880 --> 00:14:01,840
states, and then suddenly you relax and you free yourself from the need to actually deal with them.

99
00:14:04,160 --> 00:14:06,560
So it prevents you from really and truly letting go.

100
00:14:12,000 --> 00:14:18,400
Number four, Sanganikaramatas, which means company. It's related to speech, but

101
00:14:18,400 --> 00:14:28,000
this is just delighting in company. So here's one people talking about meditating in groups and

102
00:14:28,000 --> 00:14:35,200
how much more powerful it is to meditating groups. I mean really what they mean is it's much easier

103
00:14:36,480 --> 00:14:39,680
because you're more concerned about the people around you and you have a

104
00:14:39,680 --> 00:14:48,160
a consciousness of them, which isn't necessarily bad. It's just, it becomes a crutch. And

105
00:14:49,520 --> 00:14:54,960
I mean that's a mild example. Much worse, of course, is when meditators sit around and talk

106
00:14:55,920 --> 00:15:00,640
or in the world, you know, when you spend all your time socializing.

107
00:15:01,920 --> 00:15:06,800
Nowadays we do it all in the internet. How many hours the day we spend on Facebook?

108
00:15:06,800 --> 00:15:10,560
And I don't know, I'm not really all that keen on Facebook, but

109
00:15:11,920 --> 00:15:20,480
becomes an addiction. It's social networking.

110
00:15:24,000 --> 00:15:25,520
When it gets in the way of our practice.

111
00:15:29,520 --> 00:15:35,280
I remember when I was, I tell this story often about this. I was at this meditation

112
00:15:35,280 --> 00:15:41,920
center. I was asked to teach for a short time. And when I got there all the foreign meditators

113
00:15:41,920 --> 00:15:46,960
would do their meditation, but in between meditation they had a schedule where they'd all

114
00:15:46,960 --> 00:15:55,440
join together and sit around chatting and laughing and joking. It was like a party or something.

115
00:15:58,480 --> 00:16:02,720
And it was because they had, they were told they were supposed to do this many hours a day,

116
00:16:02,720 --> 00:16:08,000
but they weren't told anything else. And so they had to do 10 hours of practice a day or

117
00:16:08,000 --> 00:16:14,880
something. And that was all that was of concern. So when they came to me, I said,

118
00:16:14,880 --> 00:16:20,640
did I'm okay today? I want you to practice 18 hours a day. Because you sleep six hours,

119
00:16:20,640 --> 00:16:28,720
that's fine. But the other 18 hours, they're mine. You're mine. So I said, when you wake up,

120
00:16:28,720 --> 00:16:35,440
I want you to say, lying, lying. When you want to sit up wanting to sit, what is it? Raising,

121
00:16:35,440 --> 00:16:35,920
Raising.

122
00:16:41,040 --> 00:16:50,560
And throughout the day, be mindful of all your activities. And I said, I don't care how much formal

123
00:16:50,560 --> 00:16:56,480
meditation you do, but you have to be mindful throughout the day when you walk, walking, walking.

124
00:16:56,480 --> 00:17:09,200
And like within a day, the sitting around chatting was over. This isn't some kind of work that

125
00:17:09,200 --> 00:17:14,240
you do and then you get off work and you can go back to your life. This is our life. This is about

126
00:17:14,240 --> 00:17:22,080
living mindfully. There's nothing to do with how many hours a day you do. How many hours a day

127
00:17:22,080 --> 00:17:28,080
you're walking or sitting? You can walk and sit for hours and get nothing out of it. You can

128
00:17:28,080 --> 00:17:31,040
become enlightened in three steps if you're really mindful.

129
00:17:38,000 --> 00:17:47,120
So anyway, certainly benefited, I think, from giving up that social life.

130
00:17:47,120 --> 00:17:56,960
Number five, in Riesu, Agutadwarata, not guarding your senses.

131
00:18:00,400 --> 00:18:04,960
So we failed to say the truth because we're caught up in experience.

132
00:18:06,400 --> 00:18:14,640
Experience is here, but instead of experiencing it, we react to it. You see something and you get

133
00:18:14,640 --> 00:18:21,680
caught up in liking or disliking it. You hear something and the same thing, maybe it's a good sound

134
00:18:21,680 --> 00:18:31,440
bad sound. My sound, your sound, this sound, that sound, right sound, wrong sound. Smell, taste, feeling

135
00:18:32,720 --> 00:18:35,120
too hot, too cold, too hard, too soft.

136
00:18:35,120 --> 00:18:45,840
And it can become an obsession, right? Even thinking is the sixth one, right?

137
00:18:45,840 --> 00:18:54,800
Thinking can become obsessed with thoughts, right? The time sitting around for hours thinking about

138
00:18:54,800 --> 00:19:06,160
things, right? It gets in your way. These are not things that we have to be afraid of that

139
00:19:06,160 --> 00:19:11,920
are going to cause us to fail. These are things that keep us failing every moment. I mean, these are

140
00:19:11,920 --> 00:19:18,640
the things that we have to remove, if not now, then later, to learn to guard the senses.

141
00:19:18,640 --> 00:19:25,680
But guard doesn't mean to hide them away, but it means to be mindful at the senses. That's

142
00:19:25,680 --> 00:19:31,680
what we're doing here, right? But remind ourselves, that's where that's the object of meditation

143
00:19:31,680 --> 00:19:39,440
is the senses. Whenever the senses arise seeing, hearing, smelling, tasting, feeling, thinking,

144
00:19:39,440 --> 00:19:56,160
be mindful of it. Six bhojanae, amatanuta, not knowing moderation in food. And by extension,

145
00:19:56,160 --> 00:20:16,880
anything else we take in could be information, things we read, things we watch, things we listen to.

146
00:20:16,880 --> 00:20:25,520
But especially food. I mean, what else is it that we do every day, that we need every day, food?

147
00:20:28,320 --> 00:20:32,400
Water's not such a big deal. It's hard to get attached to drinking water, though some people do.

148
00:20:35,040 --> 00:20:40,160
It's very easy to get attached to food, eating too much, eating too little.

149
00:20:40,160 --> 00:20:48,720
We've had meditators who wanted to just drink juice and eat fruit. It doesn't work out so well.

150
00:20:50,160 --> 00:20:51,120
You need to eat enough.

151
00:20:54,320 --> 00:20:59,040
But it's quite common to eat a lot, and it's a good lesson for those living in the world.

152
00:21:00,080 --> 00:21:09,360
And when you realize how obsessed we are with food, not just three meals a day, I mean,

153
00:21:09,360 --> 00:21:18,480
here at the meditation center, we have one, two meals a day. But out in the world, it's sometimes

154
00:21:18,480 --> 00:21:24,400
not just three. It's three plus snacks. How many times do you snack during the day? Some people,

155
00:21:24,400 --> 00:21:30,000
it's a lot of snacking. Maybe not even to get full, but just to enjoy the taste.

156
00:21:30,000 --> 00:21:37,280
The meditators here realize this, you know, you have the cravings yet for this food or that food.

157
00:21:39,120 --> 00:21:43,440
If I start talking about delicious foods and it makes your mouth water,

158
00:21:45,200 --> 00:21:49,760
I think the food here is okay, but we don't eat enough to satiate our

159
00:21:50,720 --> 00:21:52,480
our obsessions, our desires.

160
00:21:52,480 --> 00:22:00,880
And as a result, we're able to learn to let go of it. If you always get what you want,

161
00:22:00,880 --> 00:22:05,200
you just spoil your mind, you corrupt your mind because it'll never be enough,

162
00:22:06,160 --> 00:22:12,320
and just be more and more addicted.

163
00:22:12,320 --> 00:22:21,440
Number seven is Tsung-sengaramata, Tsung-sengaramata.

164
00:22:23,200 --> 00:22:30,960
Tsung-senga is like contact or association. And I'm not quite sure what it means,

165
00:22:30,960 --> 00:22:37,600
but it means delighting in, delighting in experiences, I think, so that I would put it as

166
00:22:37,600 --> 00:22:45,520
one thing to experience certain things. So in medit for meditators, this can be wishing to

167
00:22:45,520 --> 00:22:51,280
experience blissful states. Sometimes you'll see bright colors or images wanting to see them.

168
00:22:53,920 --> 00:22:59,920
Some people may be at wanting magical powers flying through their levitating,

169
00:22:59,920 --> 00:23:11,520
seeing things with your eyes closed, leaving your body this kind of thing.

170
00:23:12,960 --> 00:23:21,120
Get attached to experiences in an ordinary sense. It's getting attached to being things or being

171
00:23:21,120 --> 00:23:31,040
places, going places, going here, going there.

172
00:23:35,760 --> 00:23:42,320
And through deep meditation, what our advanced meditators here who have been doing this for many

173
00:23:42,320 --> 00:23:50,240
days, now starting to realize, is that experiences, experiences diverse, but in the end,

174
00:23:50,240 --> 00:23:57,120
it's still just experience. There really isn't anywhere you can go up on a mountain under the sea,

175
00:24:00,480 --> 00:24:08,000
and that will free you from suffering. That will, or that will give you an experience that is

176
00:24:08,640 --> 00:24:19,680
satisfying, controllable, predictable, stable, worth clinging to, basically. Nothing is worth

177
00:24:19,680 --> 00:24:26,400
clinging to. So what you start to see? And so this idea of, hey, maybe I'll go to Niagara Falls,

178
00:24:28,000 --> 00:24:32,880
those of you who are from far away, that you've been thinking, oh boy,

179
00:24:32,880 --> 00:24:36,320
Niagara Falls, it's only about 45 minutes away from here.

180
00:24:36,320 --> 00:24:53,200
Yeah, that's sung, sung, sangar, amata, desire for experiences. It's difficult being this close to

181
00:24:53,200 --> 00:25:02,880
Niagara Falls must be a big one. Number eight, papanchar, amata, papancha, delighting in papancha,

182
00:25:02,880 --> 00:25:10,720
delighting in diversification, or yeah, it's not a really good translation for papancha.

183
00:25:10,720 --> 00:25:15,760
Papancha means making more out of something than it is. This word is incredibly important.

184
00:25:15,760 --> 00:25:21,440
I mean, this is a word that we talk about in English. We don't usually name the word,

185
00:25:23,920 --> 00:25:27,520
making more out of things than they actually are is really the whole of the problem.

186
00:25:27,520 --> 00:25:39,120
Without papanchad, there is no suffering, there is no, there's no problem, right? Because

187
00:25:39,120 --> 00:25:48,080
problem is papancha, things aren't problems. This snake in front of you is not a problem,

188
00:25:50,320 --> 00:25:54,720
but when you see it as a problem and you get afraid of the snake or the spider,

189
00:25:54,720 --> 00:26:09,200
the pain in your body, right? Pain is just pain until we decide or until we think of it as a problem,

190
00:26:09,200 --> 00:26:19,120
as more than that. So delighting in this, or not delighting in it, but getting obsessed with it,

191
00:26:19,120 --> 00:26:30,080
everything. Food, we don't just eat, we enjoy, right? We use the word enjoy as I enjoyed my,

192
00:26:30,080 --> 00:26:46,960
I enjoyed my food. Everything we see in here and smell and taste and feel and think all of our

193
00:26:46,960 --> 00:26:53,280
experiences, we extrapolate, we make more of them than they actually are.

194
00:26:53,280 --> 00:27:06,880
In the world, we delight in it, we delight in daydreaming, we delight in ambition, right?

195
00:27:06,880 --> 00:27:11,520
You have some money, you want to make more out of it, make more money, maybe you invest it,

196
00:27:11,520 --> 00:27:25,440
maybe you stay up late at night thinking of ways to make more money, or just ways to survive.

197
00:27:25,440 --> 00:27:35,200
The way to survive is actually proper, right? But that's the point, is that surviving is not

198
00:27:35,200 --> 00:27:40,480
making more of the things than they are, seeing things as they are. Yes, I need money or whatever

199
00:27:40,480 --> 00:27:47,600
to survive. But when you start to think, hey, how could I make more money and so on and so on?

200
00:27:49,360 --> 00:27:55,520
This, I think you could call in a worldly sense, but from the point of view, the practice,

201
00:27:55,520 --> 00:28:02,000
it means everything, making more out of things than they are. You're cold, but you don't like the

202
00:28:02,000 --> 00:28:07,040
cold and it's bad and it's a problem. Well, there you go, making more out of it than it actually is.

203
00:28:07,040 --> 00:28:13,360
You hear a noise and it frightens you. Why is it frighten you? Because you think it's a ghost,

204
00:28:13,360 --> 00:28:18,960
or you think it's a monster and wild animal. This is papanja.

205
00:28:23,040 --> 00:28:29,280
You hear a sound and you think it's beautiful and lovely and you want to go and find and capture

206
00:28:29,280 --> 00:28:35,280
whatever bird is making that noise. This is papanja, making more out of it than it is.

207
00:28:35,280 --> 00:28:40,320
And you know all you've seen through the course, all the ways that this happens.

208
00:28:41,680 --> 00:28:44,160
The mind makes more of things than they actually are.

209
00:28:49,120 --> 00:28:54,880
And that's the problem. Once you solve that and start seeing things just as they are,

210
00:28:54,880 --> 00:28:59,520
which is of course what the practice is for when you say to yourself, pain, pain, you're

211
00:28:59,520 --> 00:29:05,760
changing the way you look at the pain to see that surprisingly it's just pain. It's nothing more

212
00:29:05,760 --> 00:29:14,400
nothing less. When you do that, then you stop failing. Then you start succeeding.

213
00:29:16,640 --> 00:29:20,960
But it's not something anyone should ever feel desperate about, like I'm failing, like I'm a failure.

214
00:29:21,600 --> 00:29:24,320
We usually say that because of our experiences, right?

215
00:29:24,320 --> 00:29:30,400
I'm experiencing a certain thing in my practice and that's a sign of failure.

216
00:29:33,200 --> 00:29:38,800
And there you go, that's papanja, making more of things than they actually are.

217
00:29:39,520 --> 00:29:43,360
The sense of failure, that's the problem.

218
00:29:46,480 --> 00:29:48,000
Our experiences are not the problem.

219
00:29:48,000 --> 00:29:54,320
Once we can see them from what they are, then we succeed because then we stop clinging.

220
00:29:55,680 --> 00:29:59,600
There's nothing to cling to if seeing is just seeing and so on.

221
00:30:03,120 --> 00:30:11,200
And when you get there, watch out. That is the highest. That is where the mind becomes free.

222
00:30:11,200 --> 00:30:21,440
And then one realizes true peace, true happiness, that which is of true benefit and value.

223
00:30:25,120 --> 00:30:30,480
So there you go. There's some talk about failure. We should all feel encouraged

224
00:30:31,680 --> 00:30:34,800
every moment that we're meditating, every moment that we're mindful.

225
00:30:34,800 --> 00:30:42,320
In the moment we're not failing to see the truth. In that moment we are succeeding.

226
00:30:42,320 --> 00:31:07,200
So there's the demo for tonight. Thank you all for tuning in coming out. Have a good night.

