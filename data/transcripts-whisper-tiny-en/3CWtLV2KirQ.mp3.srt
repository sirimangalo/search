1
00:00:00,000 --> 00:00:16,240
Okay, good evening everyone broadcasting live October 1st now officially October.

2
00:00:16,240 --> 00:00:24,480
I was just informed just reminded that we've got less than a month left of the rains.

3
00:00:24,480 --> 00:00:30,800
Tonight I will be heading back, probably have to sign off early, be heading back to Stony Creek.

4
00:00:30,800 --> 00:00:34,560
We'll come back tomorrow morning here to Hamilton.

5
00:00:41,760 --> 00:00:48,800
Today I went walking through the forest. There's a nature trail.

6
00:00:48,800 --> 00:00:59,680
It's actually quite close by. It's half a kilometer away, less than getting into the nature trail,

7
00:00:59,680 --> 00:01:06,000
and it goes straight to McMaster University. So I took it, and then I went on another trail

8
00:01:06,720 --> 00:01:10,720
in the loop, and hey, I wonder

9
00:01:10,720 --> 00:01:21,600
Oh, you'll see it. The reason was, besides going on a nature walk,

10
00:01:23,360 --> 00:01:31,120
we'll be next Friday we'll be holding a peace walk. A walk for peace in the tradition of the Buddha,

11
00:01:32,240 --> 00:01:39,920
Mahatma Gandhi, Martin Luther King, Mahangosananda, Cambodian monk who walked for peace when

12
00:01:39,920 --> 00:01:45,760
they were killing each other in Cambodia, they would walk in between the fighting

13
00:01:46,960 --> 00:01:51,840
walk across the battlefield, trying to get the people to stop shooting each other.

14
00:01:53,760 --> 00:01:57,600
Of course, we won't be doing that, but we'll be walking for peace.

15
00:01:58,800 --> 00:02:05,120
So in our peace we will be trying to be peaceful and do it as a walking meditation,

16
00:02:05,120 --> 00:02:10,560
and just to make a statement, so I was thinking we could have like placards with

17
00:02:10,560 --> 00:02:15,360
peace walk on them, just to let people know what we were doing. Of course, we'll be in the forest,

18
00:02:15,360 --> 00:02:25,600
most of the way, so maybe that's not so applicable. And there's two peace organizations on campus

19
00:02:25,600 --> 00:02:30,080
that we're going to be coordinating with, hopefully, contact with both of them about it.

20
00:02:30,080 --> 00:02:36,320
We'll take about an hour and we're going to, so we're going to walk from McMaster to here,

21
00:02:37,040 --> 00:02:42,160
and then when we get here, we'll have hot apple cider and cookies.

22
00:02:43,680 --> 00:02:50,000
So I've got to get my organization on that, ordering apple cider and we'll have it hot.

23
00:02:51,040 --> 00:02:56,880
They've already talked to Adrana here about it. Apparently food basic sells apple cider,

24
00:02:56,880 --> 00:03:04,080
so we'll just have it, we need a big pot, maybe, to keep it hot.

25
00:03:06,240 --> 00:03:12,000
We'll have it ready and get some, I don't know, what do you have with apple cider?

26
00:03:13,600 --> 00:03:21,360
Oh, good fall stuff is like popcorn, popcorn, apple cider doughnuts, apples,

27
00:03:21,360 --> 00:03:28,400
pumpkin pie, pumpkin pie. Hey, when is Thanksgiving in Canada?

28
00:03:29,200 --> 00:03:34,800
I have no idea. Okay. It's different from America, that's all I know. You're just later, I think,

29
00:03:34,800 --> 00:03:44,400
ours is probably this one. Yes, ours is third week of November. Yeah, I think ours is like third week

30
00:03:44,400 --> 00:04:00,640
of October. October, okay. So there's that. Anyway, shouldn't talk too much because I got to go,

31
00:04:01,440 --> 00:04:06,320
but let's look at the quote Robin, would you do us the one who was reading it?

32
00:04:06,320 --> 00:04:13,520
Supposed an innocent baby boy lying on his back or because of some carelessness of his nurse

33
00:04:13,520 --> 00:04:19,120
to put a stick or a stone into his mouth. His nurse would immediately do something to remove it,

34
00:04:19,120 --> 00:04:23,440
and if she could not get it out immediately, she would hold the child's head with her left hand

35
00:04:23,440 --> 00:04:28,320
and with the finger of her right hand get it out, even if she had to draw blood. And why?

36
00:04:28,960 --> 00:04:34,240
Because such a thing is a danger to the child by no means harmless. Also, the nurse would do

37
00:04:34,240 --> 00:04:39,760
such a thing out of love for the child's benefit, out of kindness and compassion. But when that boy

38
00:04:39,760 --> 00:04:46,480
is older and more wise, the nurse no longer looks after him thinking, the boy can look after himself.

39
00:04:46,480 --> 00:04:53,520
He is done with foolishness. In the same way, if due to lack of faith, self-respect, fear of

40
00:04:53,520 --> 00:05:00,160
blame, energy and wisdom, good things are not practiced by one, then one must be watched over by me.

41
00:05:00,160 --> 00:05:05,840
But when good things are practiced, then I need not look after one thinking. He can now look after

42
00:05:05,840 --> 00:05:07,920
himself. He is done with foolishness.

43
00:05:07,920 --> 00:05:36,720
Thank you. Give me a second here.

44
00:05:36,720 --> 00:05:48,560
I'm just doing something. Let's take a second.

45
00:05:53,280 --> 00:05:53,520
Yes.

46
00:05:53,520 --> 00:06:09,440
This is actually a variant on a popular teaching of the Buddha,

47
00:06:09,440 --> 00:06:28,400
where he is asked whether he teaches people in other, he engages in harsh speech. And the

48
00:06:28,400 --> 00:06:40,240
Buddha said, no, I don't engage in harsh speech, speech designed to hurt others. And then he

49
00:06:40,240 --> 00:06:49,600
was asked, well, isn't it true that you have said this or said that to hurt other people?

50
00:06:49,600 --> 00:06:59,920
And he said, well, sometimes you have to give people a hard teaching.

51
00:07:00,720 --> 00:07:03,440
And digany kai, there's this story, very similar.

52
00:07:03,440 --> 00:07:22,000
The question is whether you should ever use tough love. And so he draws this comparison,

53
00:07:22,720 --> 00:07:32,320
and he points out that sometimes you'll do something to even do something that hurts.

54
00:07:32,320 --> 00:07:36,960
If you know that it's actually going to benefit in the long run. And I think to some extent you

55
00:07:36,960 --> 00:07:45,920
could apply that to Buddhism, because we tell meditators to meditate, to sit still when you have pain,

56
00:07:45,920 --> 00:07:53,360
not to relent. And so by saying that, we're actually telling you something is going to make

57
00:07:53,360 --> 00:07:58,160
you feel pain. Meditation is something that's probably going to bring a lot of people

58
00:07:58,160 --> 00:08:03,280
a lot of pain. So you can say, wow, these Buddhist meditation teachers making a lot of bad karma,

59
00:08:04,240 --> 00:08:05,680
hurting their students, right?

60
00:08:08,880 --> 00:08:11,920
So this is the whole old adage has to hurt if it's to heal.

61
00:08:13,360 --> 00:08:18,880
But into some extent, we defend ourselves. And I think there's a definite line that you

62
00:08:19,520 --> 00:08:22,800
that the Buddha would never cross. Like if you look at how the Buddha

63
00:08:22,800 --> 00:08:28,560
with the Buddha uses to compare himself, he talks about himself. He doesn't say, well, I, so I

64
00:08:28,560 --> 00:08:38,960
would draw blood on my students. Obviously he would never do that. But he will watch over them.

65
00:08:38,960 --> 00:08:47,040
So he uses a much more tame comparison. Like the worst the Buddha would do according to the

66
00:08:47,040 --> 00:08:52,240
Dika, the story in the Dikaya. Actually, I'm not sure. I think now that I think it was in the

67
00:08:52,240 --> 00:08:59,600
Majimani kai, not the Dikani kai. It's the prince, and by I think.

68
00:08:59,600 --> 00:09:17,440
I don't remember today. My memory is going. So they say again, when you get old,

69
00:09:18,880 --> 00:09:22,160
it shouldn't go like that though. It's just my mind has got other things in it.

70
00:09:22,160 --> 00:09:33,280
Like peace walks and stuff. But the prince Ambaya, anyway, same story.

71
00:09:35,600 --> 00:09:47,200
But he says to the prince Ambaya, he says that he would, how would he, I think it's the one where

72
00:09:47,200 --> 00:09:53,840
he trains horses. And he says, well, what would you do if you had a, I don't know, maybe I'm mixing

73
00:09:53,840 --> 00:09:59,280
the mouth now. Oh, it's terrible. Anyway, one point in the Buddha says, how would he, how he compares

74
00:09:59,280 --> 00:10:08,480
this when the child has the stone in its mouth or the toy in its mouth? He said, when I have a,

75
00:10:08,480 --> 00:10:13,280
I give a harsh teaching. I will tell them, tell them my students.

76
00:10:13,280 --> 00:10:20,720
And mixing these up, that's terrible. This would happen. So I see in order to really address this,

77
00:10:20,720 --> 00:10:28,560
I have to study first. But my point being, he would never, he doesn't actually harm his students.

78
00:10:28,560 --> 00:10:32,640
He doesn't do some, he doesn't do something quite so drastic as like pulling this out of their

79
00:10:32,640 --> 00:10:37,200
mouth. But he'll tell people, don't do that. He tells the students, don't do this, don't do that,

80
00:10:37,200 --> 00:10:39,600
which is considered to be a harsh teaching.

81
00:10:39,600 --> 00:10:46,480
I'm supposed to telling, telling people to do this, do that. It's an interesting distinction,

82
00:10:47,840 --> 00:10:54,400
especially for a teacher, because ideally, you don't ever want to have to tell your students,

83
00:10:54,400 --> 00:10:59,360
don't do this, don't do that. Ideally, you just want to be able to do this, do that. And then they

84
00:10:59,360 --> 00:11:05,360
say, okay, I'll do that. And then they did. But that's, the problem is something new. We are,

85
00:11:05,360 --> 00:11:09,840
we stray. And so when meditators do wrong things, you have to say, don't do this, don't do that.

86
00:11:11,600 --> 00:11:18,080
But it's an interesting distinction that prefer the nicer thing, the more ideal is when we just

87
00:11:18,080 --> 00:11:22,480
have to tell people what to do and we never have to tell people what not to do, as ideally,

88
00:11:22,480 --> 00:11:35,600
we're all doing good things. But that's an example of what the Buddha means by a harsh teaching.

89
00:11:37,040 --> 00:11:41,680
And another part of this is at the end, I like how this talks about watching over.

90
00:11:44,000 --> 00:11:47,680
Because it's an interesting aspect of the Buddha's teaching that we don't really

91
00:11:47,680 --> 00:11:56,240
follow our students. It's very much about we're trying to make our students independent.

92
00:11:56,240 --> 00:12:00,240
So that's what you should become out of this. If you find as you practice, you're more and more

93
00:12:00,240 --> 00:12:05,440
clinging to your teacher. It's a bit of a sign of a problem. It's a sign that you might be,

94
00:12:06,400 --> 00:12:12,000
well, you know, I mean, in the long run, right? In the beginning, of course, you have to,

95
00:12:12,000 --> 00:12:19,120
but ideally, you want to become independent. And like you, you in some ways become a clone of the

96
00:12:19,120 --> 00:12:24,880
Buddha, right? It's not like you become independent and you start making up your own religion. That's

97
00:12:24,880 --> 00:12:32,640
a sign that you didn't really get it. But you take on so much from the the Buddha from the

98
00:12:33,360 --> 00:12:40,240
Sangha that you become another, almost like a clone, which is kind of a scary thing to say,

99
00:12:40,240 --> 00:12:46,320
suppose we're just making a army of clones or something. But it kind of is, you know,

100
00:12:47,200 --> 00:12:53,440
there's only one truth and we claim to have it. And so if you believe that and if you practice

101
00:12:53,440 --> 00:12:58,560
so that you realize, hey, yeah, they've got the truth, then you just wind up saying the same

102
00:12:58,560 --> 00:13:05,520
things as your teacher did and acting the same way. I mean, the same sort of character in the sense

103
00:13:05,520 --> 00:13:18,960
of being at peace with yourself, comfortable, mindful with a clear mind. So he ends on an interesting

104
00:13:18,960 --> 00:13:26,240
note. He says, you don't have to watch over. And it's interesting, it's interesting comparison

105
00:13:26,240 --> 00:13:29,840
with the boy because, yes, young children, you have to look after them. And that's what we

106
00:13:29,840 --> 00:13:37,600
are, we're children when we come into the religion and we come into the practice. And as you practice,

107
00:13:37,600 --> 00:13:45,600
you start to grow. Sorry, put the Mogulana had a relationship like this. Sorry, put that was

108
00:13:45,600 --> 00:13:50,320
was like the one who gave birth, like a mother who gives birth because he would lead his students

109
00:13:50,320 --> 00:13:56,160
up to Sotapana and then he would set them free, but he would send them to Mogulana. So Mogulana was

110
00:13:56,160 --> 00:14:03,680
like the nurse mate. He would take them all the way to Arahanship. This is, this was the relationship

111
00:14:03,680 --> 00:14:08,720
they had. They would work. Sorry, put it would do, are going to be the harder, harder work, trying

112
00:14:08,720 --> 00:14:13,600
to just straighten their views. Once they were on the straight path, Mogulana would encourage

113
00:14:13,600 --> 00:14:20,800
them and raise them. I guess you have to ask, which is the harder work, giving birth,

114
00:14:20,800 --> 00:14:27,360
or raising. It's an interesting question. Robin, what would you say? You've done both, right?

115
00:14:28,000 --> 00:14:33,920
Yes. Well, when is short in intense and one is long in intense? So you've got the nine months in the

116
00:14:33,920 --> 00:14:39,920
womb, the nine months carrying the child. How's that? Not so difficult? It can be difficult,

117
00:14:39,920 --> 00:14:44,720
the beginning and the end is difficult, the middle's not so bad. But honestly, raising the child's

118
00:14:44,720 --> 00:14:50,960
probably the harder job, right? More tiring, sure. Sure. I mean, I don't think it's quite that way

119
00:14:50,960 --> 00:14:59,520
because remember we're dealing with sort of violence. It's not quite exact analogy.

120
00:15:02,080 --> 00:15:10,160
I guess you could say that raising is maybe more rewarding. I don't want to compare too much.

121
00:15:10,160 --> 00:15:19,440
I've never given birth. So anyway, that's our quote for today.

122
00:15:21,280 --> 00:15:25,440
If you have any questions today before I have to go, I might be interrupted at any moment and

123
00:15:25,440 --> 00:15:33,680
have to get in a vehicle and go. Sure. We do have one question. Will the meditation tradition that

124
00:15:33,680 --> 00:15:41,680
you teach allow me to investigate the truth about reincarnation for myself? No. There is no truth

125
00:15:41,680 --> 00:15:47,120
in reincarnation. Well, I know what you're talking about though. You're talking about the

126
00:15:47,120 --> 00:15:52,000
continuation of the mind at the moment of death. Does that make sense? Well, because you'll start to

127
00:15:52,000 --> 00:15:57,280
see that reality is just moment after moment of experience. And you lose this idea that somehow

128
00:15:57,280 --> 00:16:02,800
it's dependent on the brain. You'll see how the mind can continue on for a moment to moment.

129
00:16:02,800 --> 00:16:08,640
Through the power of craving, how it can give rise to new becoming. You'll see that for a moment

130
00:16:08,640 --> 00:16:14,720
to moment. And so it'll give you a framework by which you could understand the continuation of

131
00:16:14,720 --> 00:16:19,120
that consciousness even when the body fails. But it's not called reincarnation. It's just

132
00:16:20,560 --> 00:16:24,160
we don't, you know, it'll help you let go of the idea of death. That's more apt.

133
00:16:24,160 --> 00:16:35,760
So, okay to meditate on a chair. Yep. That's fine. You might want to try to learn how to sit

134
00:16:35,760 --> 00:16:46,240
on the floor because it can be beneficial. Actually sit in meditation pose on my chairs now too.

135
00:16:47,040 --> 00:16:51,760
Hey, could you guys try people who are watching or at the meditation page? Could you guys try

136
00:16:51,760 --> 00:16:59,200
clicking the the animodana, enjoying hands? If you like something what someone says, I'd like to

137
00:16:59,200 --> 00:17:03,920
test this out. If you like what someone says, just click and let's see if it actually works.

138
00:17:06,560 --> 00:17:11,920
Someone has a good question like Panagali had a good question. So, oh, there we go. Two pluses.

139
00:17:12,880 --> 00:17:18,800
Okay, so it is working. Yay. Don't just click them randomly, please. It's more if you think

140
00:17:18,800 --> 00:17:29,680
someone said something or asked something worth asking.

141
00:17:38,080 --> 00:17:42,080
I would argue strongly that it's probably unwholesome.

142
00:17:42,080 --> 00:17:49,680
But more in the delusion than on the anger, I would say there probably is some anger involved,

143
00:17:50,320 --> 00:17:54,880
but more on the delusion side. The idea that somehow that's going to work, it's like a trick.

144
00:17:55,760 --> 00:18:01,920
I don't agree. This is why like Zen meditator snap, and they call it satori. I don't believe

145
00:18:01,920 --> 00:18:09,360
it satori. When you wind them so tight that they snap, I don't know. I mean, that's how I look at

146
00:18:09,360 --> 00:18:14,560
it, and I shouldn't be too critical if someone else is tradition, but it's just my opinion.

147
00:18:14,560 --> 00:18:20,480
It's not something I would ever do for that reason, but they, I'm sure, have arguments,

148
00:18:20,480 --> 00:18:46,240
very persuasive arguments in favor of it, and that's fine.

149
00:18:46,240 --> 00:18:51,520
Not sure if this was a question for you or just in general, but it also says anybody know resources

150
00:18:51,520 --> 00:18:54,720
discussing safety concerns as in damaging the knee joints.

151
00:19:01,040 --> 00:19:05,200
No, no, I don't think there's any safety concerns, you mean in meditating?

152
00:19:06,560 --> 00:19:10,160
I'm assuming that's what he's referring to, yes. I don't think there's any.

153
00:19:11,280 --> 00:19:14,160
If you got really problem, you know, if your legs are causing a real problem,

154
00:19:14,160 --> 00:19:17,440
then yeah, you should go slowly into sitting cross legged.

155
00:19:19,600 --> 00:19:22,160
That's the only thing is trying to sit cross legged when you have

156
00:19:23,360 --> 00:19:26,800
bad knees or something or a bad back.

157
00:19:28,880 --> 00:19:33,280
So you should be a little bit concerned with propping up a little bit.

158
00:19:33,280 --> 00:19:37,040
You know, pain is not a problem. Pain is something you should learn about because it's the one

159
00:19:37,040 --> 00:19:44,880
that we're going to give rise to anger towards. So we want to learn to overcome that, but if it's

160
00:19:44,880 --> 00:19:49,440
intense and it's actually going to lead its difference between pain and pain and injury,

161
00:19:50,400 --> 00:19:55,680
you can injure yourself. You can practice meditation and feel pain, but if you practice

162
00:19:55,680 --> 00:20:00,880
meditation, if you stretch and strain yourself to the point of an injury, well, that's not helpful

163
00:20:00,880 --> 00:20:08,880
in any way. Do people feel sublime happiness just after a cessation?

164
00:20:09,920 --> 00:20:19,840
Generally, they'll feel a little bit disoriented maybe because in the sense like what just happened,

165
00:20:19,840 --> 00:20:26,880
like it was beyond anything. No, no concept of what just happened and then they'll feel

166
00:20:26,880 --> 00:20:36,480
peace often for hours or days as a result of it. In the beginning, it's just this amazing

167
00:20:36,480 --> 00:20:42,720
sense of the change that's gone through them. And so there's great bliss and peace and happiness

168
00:20:42,720 --> 00:20:51,040
and just a sense of awe and the new way of looking at the world and it'll wear us off, the novelty

169
00:20:51,040 --> 00:20:58,080
of it wears off, but the peace stays.

170
00:20:58,080 --> 00:21:19,280
Well, we've got a long line of meditators here. We deal. And must people are green.

171
00:21:19,280 --> 00:21:28,640
It's taking more time to click on everybody.

172
00:21:38,880 --> 00:21:44,320
How is the first official residential meditation course gearing up that starts

173
00:21:44,320 --> 00:21:50,400
about a week from now? I don't know if anyone's actually coming. I think there may be one person

174
00:21:50,400 --> 00:21:55,680
signed up. I'm just going to check on on Facebook. It's actually I don't think in Facebook.

175
00:21:57,760 --> 00:22:05,520
Where are my events? Is that on my page? How does this work?

176
00:22:05,520 --> 00:22:14,000
Should be on your page? Even upcoming events. Only one person.

177
00:22:16,400 --> 00:22:18,400
Four. Wait, four going. What do I see?

178
00:22:22,720 --> 00:22:27,760
Well, one lives in New York. I think I contacted her already.

179
00:22:27,760 --> 00:22:34,560
Mother looks like he lives in New York.

180
00:22:39,360 --> 00:22:45,200
There's in Bangladesh. That's a road trip. I'm not convinced that he's coming.

181
00:22:48,800 --> 00:22:53,840
We discussed it above, but would leg falling asleep going numb be good pain to sit through

182
00:22:53,840 --> 00:23:03,600
and take note of one sitting through what? Through your leg falling asleep or going numb,

183
00:23:03,600 --> 00:23:08,880
should you sit through that? Yeah, just take note of it. Yeah, that won't cause permanent damage.

184
00:23:15,200 --> 00:23:20,960
People do that all the time. I mean, once you just be careful, when you stand up afterwards,

185
00:23:20,960 --> 00:23:25,760
the danger can come if you stand up and try to stand on it and break your leg. I can't

186
00:23:25,760 --> 00:23:30,640
happen. Yeah, I've had that happen. I stood up too quickly, not realizing how much my foot was

187
00:23:30,640 --> 00:23:35,920
asleep and I fell right back down. You can break your leg. People have done it. Yeah.

188
00:23:37,200 --> 00:23:41,840
Recently read that some Buddhist monks sleep in the sitting position. Have you ever tried this

189
00:23:41,840 --> 00:23:55,360
Dante? Yeah, yeah, that's perfectly valid. It's a good way to keep the sitters practice,

190
00:23:55,360 --> 00:24:03,840
the non-lying down practice. We did seven days not lying down.

191
00:24:03,840 --> 00:24:12,400
The last two days were, the last two nights were for my teacher's birthday.

192
00:24:15,840 --> 00:24:19,760
It was the seventh day, it was seventh night, it was the night before my teacher's birthday and

193
00:24:19,760 --> 00:24:24,960
we did, I got the monks in the monastery to get together to do it and we all sat outside of his room

194
00:24:24,960 --> 00:24:29,440
meditating and when he came out in the morning, I think I had just woken up from like

195
00:24:29,440 --> 00:24:36,880
three hours of sitting sleep and it came out and now he was so, he was impressed. He said very

196
00:24:36,880 --> 00:24:42,080
good effort, congratulations. He was very happy to see us. That was nice. Right outside of his room,

197
00:24:42,080 --> 00:24:47,520
his bedroom. Because his bedroom at the time was inside this meditation hall. So we were in the

198
00:24:47,520 --> 00:24:52,800
meditation hall like 10 monks, being in all night, all night meditation practice in his honor.

199
00:24:53,520 --> 00:24:58,960
Was he aware that you had been doing that for seven days? No, I don't think I even ever told him.

200
00:24:58,960 --> 00:25:03,280
I had been up at noise to tap doing it and then I came down for his birthday.

201
00:25:05,680 --> 00:25:08,240
What do you think of Sam Harris' approach to mindfulness?

202
00:25:11,520 --> 00:25:15,200
I don't really know what that well except in an end of faith,

203
00:25:16,720 --> 00:25:23,120
Sam Harris, right? In the end of faith, he goes somewhere that I don't think is valid, he talks

204
00:25:23,120 --> 00:25:29,280
a lot about compassion. So just today, someone was telling me about his new book called Waking Up.

205
00:25:29,280 --> 00:25:34,560
If someone wants to send it to me, I'd be happy to read it and talk about it. I don't have that book.

206
00:25:34,560 --> 00:25:55,680
Apparently Waking Up is more about mindfulness.

207
00:26:04,800 --> 00:26:12,080
Okay, I'm going to go because I should get ready. He's going to be here soon.

208
00:26:13,520 --> 00:26:17,680
Thank you, Bhante. Thank you, Robin. Thanks everyone for tuning in.

209
00:26:17,680 --> 00:26:42,400
I'll have more time tomorrow. Good night.

