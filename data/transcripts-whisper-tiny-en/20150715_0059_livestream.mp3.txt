Okay, in broadcasting life, today from Sunderland, Ontario, where I'm visiting with a family.
Be here till Friday and then back to Stony Creek, Ontario, and then on Monday, the
morning, 20th, we're planning on taking a road trip, maybe, up to a minute to an island,
where I was born.
I spent a few days revisiting, and when I just wanted to show the monks where I live, because
they're always telling me about their homeland in southern Vietnam, but used to be a part
of Cambodia.
And I said, I should show you where I grew up, because they talk about how beautiful
Ontario is, and this is nothing to see in my home.
I want to show them something that's wrong, chance to get away, go off into the forest,
and do some meditation up on the cliffs.
Let's see.
Anyway, just details of life, and everyone know what's happening with me.
Today's reading, again, we're reading from the words, words of the Buddha, Buddha Vatsana
by Damika, and today's reading is about livelihood, really.
Well, I'm sorry we missed that yesterday, I missed yesterday, wasn't it?
I'll try not to miss, try to be here every day, but I can't always promise.
The way today's is about livelihood, someone's accusing, sorry, put the improper consumption
of goods, and he says that he refrains from basically refrains from what would be considered
a wrong livelihood for a monk, and therefore he is properly.
It's obviously, this is a specific teaching for monks, but the general sense is clearly
applicable in the playlist or anyone there, to everyone.
Right livelihood is something livelihood in general, it's an unavoidable consequence
of living in the world, you have to stay alive.
In modern times this means a lot more than simply survival means taking part in systems
and structures, organizations, getting a job, going to school, learning skills on this,
engaging with society, voting, renew news, livelihood can be quite complex, just to stay
alive to society, and so it is even more important that we understand the ethical
ins and outs of livelihood, it's a part of our lives, a big part of our lives.
So the Buddha gave instruction on the sorts of livelihood that are improper, and we've
talked about this a bit on the internet, for example the Buddha said like selling weapons
is considered wrong livelihood, but then what about working in a factory that makes
weapons, and if hunting is wrong, what about this wife who, this woman who was married
to a man who hunted and she cleaned all of his implements for it and cleaned his snares
and so on, is that wrong livelihood?
Of course, ultimately when we talk about right or wrong, we deal with intention, it isn't
that, and there is no gray area, it's not hard to understand, if your mind is full of
greed, anger, delusion, that's wrong, doesn't have anything to do with the actual acts.
So this is why it's possible to work in a factory or clean implements of murder-based
ism, without impure thoughts, without unholesing this, because your mind is simply performing
an action.
But when you sell something, you undertake, you make a decision what to sell, you make
a decision to sell weapons, for example.
So there is something there that's undeniably unholesing, working in a store, if you work
in a store, as a clerk selling things, it's not the same.
But if you open a liquor store or make a decision in a restaurant to sell alcohol, I think
you can be considered comfortable.
At this point of view, you've made a decision to sell things to people.
If you work in a store, you could argue that you could argue that you're simply responding
to orders.
Someone says, I'd like to buy this, and if you just bring them up, give them, take
their money, give their money, you're not actually handling or getting involved with
the sale, and you're just facilitating.
So we could say facilitating is also wrong, but I think it's possible for the intention
to be pure in that case, for the person who's manufacturing weapons, they're simply
putting stuff together.
In fact, I think you could argue that selling weapons isn't necessarily unholesome, but
I think it's the intention.
What I mean is a person who has a weapons, in fact, owns weapons, in fact, or builds
a weapons factory or whatever.
It's not unholesome the whole time, just owning it isn't accumulating carbon necessarily.
I mean, it doesn't seem like it, but the intentions and the decision and the decision
to continue, anyway, the most important point is the intention, and so then we go to the
next level of actions that lead to wrong livelihood, so the description of wrong livelihood
is wrong livelihood that involves wrong action, wrong speech, wrong action, and wrong speech,
our speech that is still based on the mind, and still the unholesome answer of the mind.
So when you speak harshly or you speak to create a division between people to divide people,
and even you use the speech, the point is that there's a mind behind it that is cultiving
bad habits, it's not the actual speech that is wrong.
Sometimes people say certain things to get out of the attention of other people, parents
when they scold their children, sometimes there's, it can't, it's possible to do it without
unholesome as teachers when they teach things they can do it with a good heart, so still
it's our mind that we have to purify that, in the noble latefold path, we're talking
about this kind of livelihood, right livelihood is that which is free from killing, from
making your livelihood based on killing, stealing, lying, cheating, if you do any of these
things to make a living that's wrong livelihood.
But here in the suit that we have something a little bit more curious, it's livelihood
based on desire, based on greed, or based on seeking, seeking out wealth or gain.
That's what's really being discussed here in all these ways that religious people have
to make a living.
I want to live as a religious person, so I'm going to run errands for people in order
to make my livelihood, or I'm going to tell fortune, so all these things that's
very good to mention, right livelihood in its purest sense from monk is not livelihood at all,
since it's not seeking out livelihood.
We have the bare ability to wander through the village in the morning, and the idea is
to wander through the village looking to see if anyone is already offering food.
It wasn't begging, it wasn't something we would have created, it was just a convenient
aspect of society that they support and religious people, and that's really what proper
religious livelihood is, it's living off of the support of others, and the support means
people who actually support what you do, not trying to find a way to gain livelihood,
and living a life and behaving in such a way that people support you.
This is for monks, you see, but there's something, they said it, this can be easily extrapolated.
Take a person living a night, working a nine to five job in an office, they have options
open to them, some people would take it as an opportunity to rise up and rise, and they
might do unwholesome things together, say things manipulate others, scheme and plot,
buy and cheat, to get no distance, be favored, for what to gain weight of livelihood?
The point here is to be content, where some people, that's how it is, they work and
they do it, to gain money, they don't even think to gain more money than to gain, become
more powerful, because livelihood is such an intrinsic part of our lives, it necessarily
colors our practice, for a monk, if they're all constantly obsessing over livelihood,
get nicer food, better food, better roads, better, shout, better homes, and their minds
will never be at peace, and they will be able to find clarity of mind, that lay people
is very much the same, if you're obsessed with your job, trying to succeed, worried about
success in a worldly sentence, a never-sixteenth spiritual, people would always come to
my teacher asking for advice, should they get a new job or so on, invariably he would
say no, should stay with what you've got, the blast and what they should sell, I remember
once someone came and asked him, what do you think I should sell, I don't want to open
the store and sell something, of course the John didn't want to answer, he could sell
salt, so salt, it was like reluctant advice, when you get a good point, because everyone
needs salt, but it's such a simple thing to say, stop looking for more, do something
good, and just get a job and work, so salt, like this unangami who sold parts, but he
didn't really sell them, he just put them by the side of the road and people came by
and offered, whatever they thought the pot was worth, he never put price tags on them,
even when they asked how much is this, he would say just leave whatever you want, that's
ideal, that's beautiful, it's such peace of mind if you can, so of course then people
argue that we're not having enough food to eat, it's hardly conducive to peace of mind
and it's true, you do have to work, you should work, it's likely it is important, but it
shouldn't consume you, you should work hard at it, so that you're working as efficient
amount, not to be blamed, livelihood is a duty, amongst have a duty to teach, he may say
it's not livelihood purposes, but I think partially it is, amongst has a duty to teach,
it's just like a man selling parts, you don't teach to make money, you teach because
that's our duty, and the people want us to continue to teach, but I'm sure we can
defend, so they would have this about support, you work in a company, they value you, so
they keep paying you, all you have to do is be valuable, something to think about, livelihood
is our livelihood contributing to our spiritual practice or is it detracting from it, hard
work in a bad work environment doesn't necessarily have to detract from spiritual practice,
again it's all about what your mind is, if you want it can be like a duty, I remember
when I after I began practicing meditation, I was still not even a monk, I went tree
planting in Northern Ontario, tree planting is, wow, what a job, hard, the hardest work
you can ever see, the work, the work coming out as a day, the morning until some day,
eight hours, for some reason I want to say 10, but I don't think that's possible, anyway,
you're working all day, planting thousands of trees, and the point is that some of the
worst people, from my experience it was some of the worst people, but I think I got a really
bad company, and they were just the lowest of the low, and the people who were supervising
us were really awful, and the quality of people, I mean it was a lot of, I guess I would
cast, and I remember the environment was not also all that wholesome, and I shouldn't
judge, actually some of the people were okay, everyone was people, you know, and it hopes
and dreams and wishes and so on, but just being to say that it was a rather coarse,
coarse environment, and I remember just trying to be as mindful as I could, and the worst
part of course was the mosquitoes, the black clouds of mosquito, so I would do walking
meditation, wearing a full body mosquito net, because there were honestly black clouds
of mosquito, you never seen, so I tried to plant trees mindful, I think it's possible
to do all these things, to be surrounded by people who are not very mindful, this doesn't
be mindful, but I said we're happy among those who are unhappy, which is preferable to
be with good people who support me in practice, because without saying, we shouldn't
be discouraged by environment, in fact that's the key is to not be discouraged, to
just see it as, see it objectively as it is, seeing, hearing is now interesting, thinking
no matter what it is, even if you don't have livelihood and you're living on the street,
dying of thirst and hunger, it's still just seeing, hearing is now, it's not interesting
if you want to, and then that's the key, that's how the eyes, that's the look on the, that's
how we amongst dream ourselves, if I don't get food today, I won't eat, this is old
monk in Chiang Mai, he was passing on some words of wisdom, that are quite common in that,
I remember hearing this stuff from the very head of the Chiang Mai province, I went
to get something signed, they started talking, they didn't get many rich visitors, and
they were talking about eating, how's food, and okay, and he said yeah, he said, we got
Chiang Mai, we got you, we got you, it sounds so much better than chiang Mai, we got you,
chiang, we got you, it's a play on one, if you have food, you've chiang, we don't have food
chan joy, it's a common one tied up place in the atmosphere, works that sound similar.
Chan means eat.
If you have food you eat, if you don't eat you, choy means sit still or be neutral.
Choy means neutral.
Stay calm.
It's true, livelihood, livelihood that is not livelihood.
And I think it's possible for late people as well.
I just have to see it all as a duty.
Okay, they won't need a work, okay, I will.
Okay, they want to fire me, okay, go without food today, go without money today.
Maybe not to that extent.
But seeing it as a duty, we can all get to that level of not letting our work overwhelm us,
not becoming obsessed with work.
Just do it as a duty, do our thing, go home.
The spiritual practice can take place anywhere.
The spiritual practice is not what you do, it's how you do it.
That's how where your mind is when you do it.
So here's wishing you all all of us find clarity of mind, the ability to do things with a clear mind.
Live our lives with a clear mind.
And find true clarity of light.
So that's all, thank you for tuning in, have a good night.
