1
00:00:00,000 --> 00:00:24,200
Hey, good evening, everyone.

2
00:00:24,200 --> 00:00:33,880
One thing that we're often concerned with in Buddhism, in the practice, and teaching of the

3
00:00:33,880 --> 00:00:54,160
Buddha's teachings, is the depth to which we understand,

4
00:00:54,160 --> 00:01:09,440
and appreciate and attain the number, the truth, the goal.

5
00:01:09,440 --> 00:01:21,360
There's a concern that our understanding of the Dharma might be shallow, and as a result

6
00:01:21,360 --> 00:01:42,360
our attainment of the Dharma, it will be lacking the inadequate, limited, not bad, or problematic,

7
00:01:42,360 --> 00:01:50,160
just insufficient or limited.

8
00:01:50,160 --> 00:02:04,160
For instance, it's a good thing to know about moral precepts and, of course, it's a good

9
00:02:04,160 --> 00:02:12,280
thing to practice, but let's see, it goes so much deeper and true.

10
00:02:12,280 --> 00:02:21,720
I think it goes so much deeper than just keeping rules, and with concentration and being

11
00:02:21,720 --> 00:02:40,280
focused, understanding the Buddha's teaching about being mindful and dedicated, but as

12
00:02:40,280 --> 00:02:49,520
well, dedicated towards good things, this is a very good quality, of course, samadhi

13
00:02:49,520 --> 00:03:02,080
goes much deeper than just focusing or even being mindful in an ordinary sense.

14
00:03:02,080 --> 00:03:13,840
Of course, wisdom is where we have the greatest concern that our understanding be more

15
00:03:13,840 --> 00:03:22,960
than just intellectual understanding, and this is important.

16
00:03:22,960 --> 00:03:34,960
If you read a lot or you study a lot, you come to know a lot about the Buddha's teaching.

17
00:03:34,960 --> 00:03:43,680
When you come to meditate, you realize all that knowledge is nothing.

18
00:03:43,680 --> 00:03:49,520
It's good, it's good to know these things, it's good in a practical sense, even it can

19
00:03:49,520 --> 00:03:55,520
help you in your life, if you have an understanding, all things are impermanent, it's too

20
00:03:55,520 --> 00:04:05,400
low pass.

21
00:04:05,400 --> 00:04:12,120
Makes you a better person, a happier person, this is a good thing, but we recognize there's

22
00:04:12,120 --> 00:04:23,800
a greater depth to be had than just knowledge, the Buddha's teaching, a deep, true ethics

23
00:04:23,800 --> 00:04:37,440
is activity that involves mindfulness when you're walking, stepping right, stepping left.

24
00:04:37,440 --> 00:04:53,560
It's impossible to be unethical when you have the mindful, clear awareness of the activity.

25
00:04:53,560 --> 00:04:59,840
True focus, of course, then comes from that sort of behavior, the body and mind are in

26
00:04:59,840 --> 00:05:05,160
sync, then you become very focused and it means your mind actually becomes free from

27
00:05:05,160 --> 00:05:14,800
unwholesome qualities, that you start to see clearly, true focus is not just being concentrated

28
00:05:14,800 --> 00:05:22,720
but being in focus, it's that which allows you to see clearly, and so true wisdom is

29
00:05:22,720 --> 00:05:23,720
seeing clearly.

30
00:05:23,720 --> 00:05:32,760
It's not intellectual, it doesn't have to be a thought, wisdom is his light, it's like

31
00:05:32,760 --> 00:05:41,640
shining a light on reality, so that's just an introduction, but I wanted to talk tonight

32
00:05:41,640 --> 00:05:51,420
about Buddha's teaching on depths, but I said there are six qualities, this is in the

33
00:05:51,420 --> 00:06:11,000
chakani pata, the ingutrani kaya, the rahan, the rahantavaga, I think, Mahantata, Mahantata

34
00:06:11,000 --> 00:06:26,800
means greatness, waipula, so the Buddha said, chahida mihi, bikhoi, chahida mihi, samanagatobiku,

35
00:06:26,800 --> 00:06:35,000
a biku, one who has seen the danger and clinging the danger and being reborn again and

36
00:06:35,000 --> 00:06:54,240
samsara, who is accomplished in six dhammas, who possesses six things, nachiraseva,

37
00:06:54,240 --> 00:07:07,240
Mahantata, in no long time, the papunati will attain to greatness, papunati, papunati,

38
00:07:07,240 --> 00:07:17,760
Mahantata, will attain to greatness, waipula ta, waipula, waipula kamsur mipula, which means

39
00:07:17,760 --> 00:07:36,720
deep, profound, waipula ta means depth or profundity, so it's, the idea here is attaining

40
00:07:36,720 --> 00:07:44,760
to success and greatness in the dhamma, papunati dhammas, so in regards to the teachings,

41
00:07:44,760 --> 00:07:54,160
in regards to the dhamma, in regards to the truth, reality, it's not about right or

42
00:07:54,160 --> 00:08:05,760
wrong, it's about greatness or the ordinary, the ordinary people are not bad necessarily,

43
00:08:05,760 --> 00:08:12,080
but there's nothing great or wonderful about being reborn again and again.

44
00:08:12,080 --> 00:08:18,760
Even evil people go to hell and so on, it's not so much that it's wrong, it's just that

45
00:08:18,760 --> 00:08:26,480
it's ignoble, there's nothing great about indulging in sensuality, there's nothing great

46
00:08:26,480 --> 00:08:32,080
about being rich or even famous, even being king of the whole world, it's not that great

47
00:08:32,080 --> 00:08:39,480
in the end, going to heaven, becoming a brahma, he's getting greater, there's a greatness

48
00:08:39,480 --> 00:08:47,720
to it, but it's not really great, it's not really deep or profound, what's profound is

49
00:08:47,720 --> 00:08:58,600
breaking free from the cycle, finding something that is lasting, stable, secure, safe, because

50
00:08:58,600 --> 00:09:06,320
even being born a brahma is not safe or secure, that alone being rich or famous or indulging

51
00:09:06,320 --> 00:09:17,760
in pleasures, so when we come to practice meditation, I think this is a great interest to us,

52
00:09:17,760 --> 00:09:24,600
how do we not just succeed, but how do we gain greater depth in our practice, I think

53
00:09:24,600 --> 00:09:32,000
it's a question for all Buddhist practitioners, it might be practicing for many years,

54
00:09:32,000 --> 00:09:37,080
but it might be shallow, how do we ensure that our practice goes deeper, so these are

55
00:09:37,080 --> 00:09:44,400
qualities, like many of these lists of qualities, they're a teaching, the idea is as you

56
00:09:44,400 --> 00:09:53,840
listen to the teaching you're practicing and you're taking these in, internalizing and

57
00:09:53,840 --> 00:10:04,920
simulating these teachings, applying them to yourself, using them not just for theory, but

58
00:10:04,920 --> 00:10:14,680
as a encouragement and direction for greater and more profound practice, so there's

59
00:10:14,680 --> 00:10:22,680
six of them, and the Buddha used interesting words, if you don't know Pauli, it might not

60
00:10:22,680 --> 00:10:27,360
be that interesting, but I have to explain because it's a little bit poetic, so the first

61
00:10:27,360 --> 00:10:39,280
one is aloka bhalo, aloka is light, and so he didn't explain these, we have to understand

62
00:10:39,280 --> 00:10:43,720
them, what did he mean by light, and you have to know something about the Buddha's teaching

63
00:10:43,720 --> 00:10:49,800
to understand, but I already gave it away that light of wisdom, aloka means you have

64
00:10:49,800 --> 00:10:56,160
to have wisdom, but rather than say wisdom, he said light, because it's a certain kind

65
00:10:56,160 --> 00:11:03,160
of wisdom, it's not seeing things with your eyes, of course, but it's the feeling of

66
00:11:03,160 --> 00:11:14,720
clarity and opening your eyes and leaving the darkness behind, it's like waking up from

67
00:11:14,720 --> 00:11:22,080
a sleep almost, or when you open your eyes and suddenly realize that you're bumping into

68
00:11:22,080 --> 00:11:33,920
things and why you're in so much pain, it's because you were blind, aloka bhalo, one who

69
00:11:33,920 --> 00:11:40,840
has greatness of wisdom, so in meditation it's important that your focus, your attention

70
00:11:40,840 --> 00:11:47,720
is on understanding, it's not magic, where you just repeat words to yourself and you become

71
00:11:47,720 --> 00:11:54,760
enlightened, you want to really try to see and understand, of course, it's not like a book

72
00:11:54,760 --> 00:12:03,880
where you can just, well, maybe it kind of is like a book, if you want to, similarly,

73
00:12:03,880 --> 00:12:11,880
you have to read it, you can't force the knowledge out of the pages, you can't pick up

74
00:12:11,880 --> 00:12:19,800
the book and stare more intently and the more intently is stare, the more knowledge you

75
00:12:19,800 --> 00:12:26,760
gain, meditation is kind of like reading a book, you have to go page by page and the knowledge

76
00:12:26,760 --> 00:12:35,320
comes on its own, it comes in its own time, so rather than trying to force or magically

77
00:12:35,320 --> 00:12:45,160
make knowledge or rise or even our idea that somehow you can make knowledge or rise if

78
00:12:45,160 --> 00:12:50,920
you analyze, if you look closely enough, rather than analyzing the book to get its knowledge,

79
00:12:50,920 --> 00:13:01,320
you have to read it, so in meditation you need it, you can't create the knowledge, but you

80
00:13:01,320 --> 00:13:10,840
have to be intent upon it and you have to be clear in your mind about the nature of things,

81
00:13:10,840 --> 00:13:17,400
you have to have that as your goal, as you watch, you're trying to confront things, you have clear

82
00:13:17,400 --> 00:13:22,760
theory to back it up, that's setting you in the right direction, what we intend to see,

83
00:13:22,760 --> 00:13:32,360
impermanence, suffering, and months out. It's important to be clever in the practice,

84
00:13:35,160 --> 00:13:41,480
but not get caught up in doubts and debates and analysis. Backed off in our

85
00:13:41,480 --> 00:13:51,320
search for knowledge gets in the way of our attainment of wisdom. It's important that we put

86
00:13:51,320 --> 00:14:01,480
that aside when we practice and focus just on seeing clearly. It's like if you want wisdom,

87
00:14:01,480 --> 00:14:08,440
you have to put aside knowledge, you have to put aside any kind of analysis you might have,

88
00:14:08,440 --> 00:14:17,640
just open your eyes, learn how to open your eyes and how to see, light and have great light,

89
00:14:18,440 --> 00:14:25,240
you have to dispel the darkness, ignore, avoid the darkness, which is all kinds of judgments and

90
00:14:26,120 --> 00:14:32,120
doubts, confusion, don't let yourself get confused or caught up in views, views, of course,

91
00:14:32,120 --> 00:14:39,560
very dangerous. Sometimes we judge a teaching and that gets in the way of appreciating

92
00:14:40,520 --> 00:14:42,600
the truth or any goodness that might be in it.

93
00:14:45,400 --> 00:14:50,920
A low kabah, low wisdom, of course, most important. If you want depth, you have to gain wisdom,

94
00:14:50,920 --> 00:14:59,880
your practice can be just tranquility. If you're fixed or focused on the pleasant, peaceful aspects

95
00:14:59,880 --> 00:15:07,320
of meditation, for example, you will never get depths of practice, focusing on somewhat

96
00:15:07,320 --> 00:15:13,480
meditation, focusing on peace or happiness or made-time meditation, which is good. All of what

97
00:15:13,480 --> 00:15:19,720
is good, samata is also good, but there's a lack of depth. Your depth will never come into

98
00:15:19,720 --> 00:15:28,920
your game wisdom and understanding, till you seek clearly. Number two, yoga bhullo,

99
00:15:30,040 --> 00:15:34,360
yoga is a strange one, but I didn't use the word yoga that much,

100
00:15:38,360 --> 00:15:44,360
but here he uses the word yoga. One should have much yoga, be full of yoga.

101
00:15:44,360 --> 00:15:53,960
So we have to understand the word yoga. It's not difficult to understand, but it meant something

102
00:15:53,960 --> 00:16:02,040
quite different from how we use the word in modern society. So in ancient times yoga,

103
00:16:03,720 --> 00:16:10,680
it was a word they used for spiritual practice. The word yoga, I mean something like a yoke,

104
00:16:10,680 --> 00:16:18,040
but not the yoke of an egg, it means a yoke as a piece of wood that you an ox would

105
00:16:21,720 --> 00:16:29,480
put on the back of the ox and it allows it to pull the plow. So it's like a harness,

106
00:16:30,120 --> 00:16:30,760
like a yoke.

107
00:16:30,760 --> 00:16:40,040
And so the idea was you would be dedicated or committed. Yoga was this commitment,

108
00:16:41,160 --> 00:16:47,960
spiritual commitment. It's how I like to translate the word religion. This word religion gets such

109
00:16:49,640 --> 00:16:55,320
a bad reputation because of how it's been used, but when it's used in the sense of being

110
00:16:55,320 --> 00:17:01,000
religious about something, in the sense of being dedicated to something intent upon it, I think

111
00:17:01,000 --> 00:17:07,800
that's very much what yoga means. So you're often, I'm not sure so much how much they would have

112
00:17:07,800 --> 00:17:13,080
used it, but in the commentaries, you're often here, this word yoga watch at her. Again and again,

113
00:17:13,080 --> 00:17:19,400
we're reading the Melinda Panda and again and again he refers to a meditator as a yoga watch at her.

114
00:17:19,400 --> 00:17:29,720
Which means someone basically who is committed to yoga, committed to a religious, spiritual

115
00:17:29,720 --> 00:17:37,800
practice, someone who is cultivating or is practicing in a spiritual way, dedicated way.

116
00:17:37,800 --> 00:17:43,080
I think we're all doing here. You're dedicated to your practice. So yoga means like dedication,

117
00:17:43,080 --> 00:17:50,520
exertion, I think it came to me, it came to me. So it probably has implications in regards to

118
00:17:50,520 --> 00:17:58,520
effort, I could say an important part of yoga is the effort. And effort and Buddhism really isn't

119
00:17:58,520 --> 00:18:07,720
about pushing yourself harder and harder, not so much. It really is about this intention or exertion,

120
00:18:07,720 --> 00:18:17,320
or dedication in the good word. You being dedicated to something, you try again, try and try again,

121
00:18:18,120 --> 00:18:24,200
never give up. It doesn't mean you have to push harder all the time. Just when you feel yourself,

122
00:18:25,640 --> 00:18:32,920
the time they say palta, it means slacking or flagging, you just feel like you're

123
00:18:32,920 --> 00:18:43,320
lacking an energy. You don't have to force yourself. You just have to try that dedication

124
00:18:44,520 --> 00:18:48,360
and even when you can't walk, so you stand, if you can't stand, you sit.

125
00:18:53,720 --> 00:18:55,800
You keep trying yoga baholu.

126
00:18:55,800 --> 00:19:05,640
Number three, wade baholu. I suppose with yoga, you'd have to confine it more to the effort

127
00:19:05,640 --> 00:19:15,160
and dedication sign, because wade is another aspect of religion. Wade is this religious feeling,

128
00:19:15,160 --> 00:19:26,120
wade the wade and that kind of. But here it refers to, I think it refers to some wagas, the idea of

129
00:19:26,120 --> 00:19:32,760
the urgency of the practice. If you want to really go deeper with your practice, it has to take

130
00:19:32,760 --> 00:19:43,000
on a sense of urgency, not just done as a hobby or a pastime, not even just as a matter of course.

131
00:19:43,000 --> 00:19:57,080
But as said, you should work to eradicate wrong view of self, you of me and mine.

132
00:19:58,840 --> 00:20:01,800
So you should work like a person with their head on fire.

133
00:20:03,960 --> 00:20:06,680
I suppose you're wearing a turban, I guess, and it was on fire.

134
00:20:06,680 --> 00:20:16,040
You'd be pretty darn intent upon. You'd have a sense of urgency in regards to putting out that

135
00:20:16,040 --> 00:20:20,920
fire and all the fire of wrong view is much more dangerous.

136
00:20:24,520 --> 00:20:27,320
We don't know what's going to happen in the future where we're going.

137
00:20:28,440 --> 00:20:30,840
If we die without any goodness in our hearts,

138
00:20:30,840 --> 00:20:37,320
we could be reborn in a very unpleasant place. We could even simply be born as an animal,

139
00:20:37,320 --> 00:20:42,920
which would be quite discouraging. It's very hard for an animal to be reborn as a human being.

140
00:20:44,520 --> 00:20:48,760
Much more common for them to be reborn as the same animal again and again and again.

141
00:20:48,760 --> 00:20:54,120
It's just very difficult to do the good qualities, the good activities, the cult of

142
00:20:54,120 --> 00:21:01,160
being good qualities that are required to be born to human.

143
00:21:04,360 --> 00:21:08,600
So a sense of urgency, we're going to get all, we're going to get sick, we're going to die,

144
00:21:08,600 --> 00:21:15,080
bad things could happen any time for not ready for them. It's very easy to get on the bed,

145
00:21:15,080 --> 00:21:25,240
a wrong, a bad path. We need a battle though.

146
00:21:27,080 --> 00:21:30,200
Number four, Asan Tutti, Baha'u know, Asan Tutti.

147
00:21:32,040 --> 00:21:39,960
Asan Tutti is contentment, which is actually funny enough, it's a very good quality in Buddhism.

148
00:21:39,960 --> 00:21:52,920
Asan Tutti is a great blessing to be contentment, and so the Buddha taught both contentment

149
00:21:52,920 --> 00:22:02,840
and discontent. Contentment, we should be content with food, whatever food we get.

150
00:22:02,840 --> 00:22:08,680
I shouldn't try too hard to get good food, delicious food anyway. You should be a little concerned

151
00:22:08,680 --> 00:22:14,760
perhaps about healthy food, not eating junk. But you shouldn't be obsessed with it, you should be

152
00:22:14,760 --> 00:22:24,440
content with whatever you have. Clothing, you should be content with dwelling, having a debate over

153
00:22:25,160 --> 00:22:30,200
allowing meditators to switch rooms or whether we have to force them to be in the room.

154
00:22:30,200 --> 00:22:39,080
I don't think we do. I think changing rooms sometimes is important, but I think it's true

155
00:22:39,080 --> 00:22:44,680
it quite often is just because oh that's a nice room or this room is hot, this room is cold.

156
00:22:46,120 --> 00:22:51,800
What you have to be careful about, don't get too caught up in luxury or following your preferences

157
00:22:51,800 --> 00:23:07,480
and that sort of thing. Contentment is good, contentment with lodging with medicine, not being too

158
00:23:07,480 --> 00:23:13,720
caught up in being too healthy or worrying about your health, worrying about pains or this sort of thing.

159
00:23:13,720 --> 00:23:21,640
But us and to tea is of course very important as well because it's easy to become complacent

160
00:23:21,640 --> 00:23:31,240
and this is in regards to practice, in regards to the dhamma. Don't be content with your gains,

161
00:23:31,240 --> 00:23:37,960
don't rest on your laurels. Again a lot from the meditation practice should be gaining

162
00:23:37,960 --> 00:23:47,800
every day, every time you sit down, in fact, you should be gaining something. It's easy when you gain

163
00:23:47,800 --> 00:23:54,600
something fairly significant or what seems significant, you become complacent, even a sotapana,

164
00:23:54,600 --> 00:23:58,360
a sakadagami, there were even anangamis that were complacent and the Buddha said,

165
00:24:00,600 --> 00:24:05,080
you guys are on the wrong path, he said you're not really followers of mine, anangami,

166
00:24:05,080 --> 00:24:11,320
you know, I didn't probably say it like that, but he said you're not doing the right thing by

167
00:24:11,320 --> 00:24:19,400
being complacent. Anangamis, someone who has come very far in the practice and he still

168
00:24:19,400 --> 00:24:24,120
is the Buddha, still said don't be complacent, don't be content with just that.

169
00:24:24,120 --> 00:24:37,320
Number five, Anikita duro. Anikita nikita means to throw away or to put down, I guess,

170
00:24:37,320 --> 00:24:46,600
nikita. Don't put down the work. Dura means duty. Don't

171
00:24:46,600 --> 00:24:55,160
shirk your duties or let them slide. It really means don't stop practicing. Dura,

172
00:24:55,160 --> 00:25:01,640
there are two dura, two duties in Buddhism, Kantatura, which is the duty of study,

173
00:25:02,360 --> 00:25:07,560
and nikita sana duro, which is the duty to see clearly. Don't shirk either of these.

174
00:25:07,560 --> 00:25:17,800
You should study, even just listening to the damas. That's how you study. It's a great way to study.

175
00:25:17,800 --> 00:25:19,640
Reading books can also be useful.

176
00:25:24,840 --> 00:25:29,080
The most importantly, of course, don't put down the practice of mindfulness.

177
00:25:29,080 --> 00:25:36,760
This means that, of course, in life, don't let years go by or you've stopped meditating just

178
00:25:36,760 --> 00:25:41,400
because you're complacent or could be many reasons. You don't doesn't have to be because you're

179
00:25:41,400 --> 00:25:48,440
contentor complacent. It's easy to get caught up in work or relationships, life, travel,

180
00:25:48,440 --> 00:25:58,520
any of the ten bodyboards, the ten impediments to practice. Don't let them get in your way.

181
00:25:59,720 --> 00:26:05,240
Don't put down the practice. You read in the Satipatanas who took commentary about on arms around

182
00:26:06,520 --> 00:26:12,520
the monks would take every step mindfully. If they took one step without mindfulness,

183
00:26:12,520 --> 00:26:21,800
they would stop. The monk behind them would know they were unmindful. It kept you quite vigilant

184
00:26:21,800 --> 00:26:26,840
actually because if you stopped, then the people behind you would know that you're not mindful,

185
00:26:26,840 --> 00:26:31,240
so you had to try for your best not to have to stop. The monks behind you wouldn't know.

186
00:26:31,240 --> 00:26:45,000
They wouldn't feel ashamed when making the monks behind you stop. Even on arms around.

187
00:26:45,880 --> 00:26:51,080
They wouldn't on arms around. They wouldn't be mindful. They wouldn't put it down. You have to put

188
00:26:51,080 --> 00:27:03,160
it down sometimes when you teach or even when you're studying, perhaps, when you're talking with

189
00:27:03,160 --> 00:27:09,240
people, you have difficulty when you're eating, but you shouldn't try your best not to.

190
00:27:10,680 --> 00:27:15,160
Even when eating, trying to be mindful. Even when talking, you can do some extent,

191
00:27:15,160 --> 00:27:21,720
anyway, be mindful. The physical act of talking is just a physical act and you can be aware of

192
00:27:21,720 --> 00:27:31,720
your lips moving and the feelings and so on. Anikita duro don't put down the practice,

193
00:27:32,840 --> 00:27:36,360
not just in terms of days, weeks, months, years, but in terms of moments.

194
00:27:37,960 --> 00:27:44,040
Throughout the day you're here, you have the opportunity to take this to its greatest extent.

195
00:27:44,040 --> 00:27:50,440
Not even sleeping. You sleep less. Anikita, you don't ever put it down. If you do that,

196
00:27:51,480 --> 00:27:55,160
it's not that it's wrong to sleep or anything, but the greatness that comes from

197
00:27:57,240 --> 00:28:03,720
constant and continuous exertion, patience,

198
00:28:03,720 --> 00:28:11,880
and clarity of mind. This is great. This is where depth comes. You take your practice to a deeper

199
00:28:11,880 --> 00:28:24,440
level. So that's what that's number five. Number six is gustale sudhamme su utarija bhatariti.

200
00:28:24,440 --> 00:28:38,520
Number six is gustale sudhamme. So in regards to wholesome dhammas, wholesome things, good things.

201
00:28:38,520 --> 00:28:57,560
Utarija bhatariti, bhatariti, coming to or attaining to attaining. Utari means greater,

202
00:28:57,560 --> 00:29:09,160
would more and more. It's in line with not being content, not being complacent. The exertion,

203
00:29:10,200 --> 00:29:20,200
greatness, as the Buddha's words, has to do with going further and further, not just having

204
00:29:20,200 --> 00:29:32,040
or just being content with keeping moral precepts, but trying to be ethical and all of your movements

205
00:29:33,960 --> 00:29:38,440
about going deeper and finding concentration, about finding wisdom and deeper wisdom.

206
00:29:38,440 --> 00:29:51,160
It's about not holding on to insights that you gain from the practice. It's about the fact that

207
00:29:51,160 --> 00:29:57,880
practice is always changing. It's always going deeper. It's always going further. It's always more

208
00:29:57,880 --> 00:30:07,160
to learn and tell you completely free yourself from suffering. It's an activity that's always

209
00:30:07,160 --> 00:30:14,600
about always adapting, always changing. The benefits and the knowledge and the understanding of the

210
00:30:14,600 --> 00:30:21,480
practice that you gained even today will not perfectly apply tomorrow because you have to go further.

211
00:30:23,160 --> 00:30:27,800
It's just one of the important challenges of meditation is it's not a constant thing.

212
00:30:28,920 --> 00:30:32,520
It's not like some at the meditation where the object's always going to be the same and your

213
00:30:32,520 --> 00:30:39,960
relation to it is just about refining and refining in insight. There's a refining that goes on,

214
00:30:41,880 --> 00:30:50,040
but there's also an adapting and a going deeper, going further. Our relationship with our

215
00:30:51,640 --> 00:31:02,280
neurotic behaviors, our depression, anxiety, worry, greed. It should constantly, we should constantly

216
00:31:02,280 --> 00:31:11,000
be coming to it with a fresh perspective, seeing, ready to see new things, ready to go deeper,

217
00:31:11,880 --> 00:31:12,840
ready to go further.

218
00:31:18,120 --> 00:31:26,040
So these are the six manta, manta, manta, manta, manta, dhamma. Dhamma's a greatness in depth,

219
00:31:26,040 --> 00:31:34,040
a way to pull up that. You want your practice to go deeper. It's just another way of

220
00:31:35,800 --> 00:31:44,440
describing, teaching, encouraging for us to dedicate ourselves to the practice. So

221
00:31:44,440 --> 00:32:00,360
that's the dhamma for tonight. Thank you for coming to listen.

