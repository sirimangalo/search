1
00:00:00,000 --> 00:00:03,000
Hello and welcome back to our study of the Dhamma Pandat.

2
00:00:03,000 --> 00:00:10,000
Today we will continue with verses number 11 and 12, which read as follows.

3
00:00:33,000 --> 00:00:38,000
So, it is a twin pair of verses.

4
00:00:38,000 --> 00:00:46,500
The first one, Assare Saramatino, for a person who understands what is essential to be

5
00:00:46,500 --> 00:00:47,500
unessential.

6
00:00:47,500 --> 00:00:52,000
Or understand, sorry, what understands what is unessential to be essential.

7
00:00:52,000 --> 00:00:56,000
Saro, tazare, tazare, tazino.

8
00:00:56,000 --> 00:01:01,000
And understand what is essential or sees what is essential as being unessential.

9
00:01:01,000 --> 00:01:04,000
Tazare and tikacatanti.

10
00:01:04,000 --> 00:01:13,000
Such a person doesn't come to what is essential.

11
00:01:13,000 --> 00:01:16,000
syaran tesare donahtulah.

12
00:01:16,000 --> 00:01:25,000
A person who knows the essential to be essential.

13
00:01:25,000 --> 00:01:27,640
What is unessential to be unessential.

14
00:01:27,640 --> 00:01:32,040
Teisarang adhikatanti sammasan kapagulchara,

15
00:01:32,040 --> 00:01:38,840
such a person does come to what is essential because they dwell in right thought.

16
00:01:38,840 --> 00:01:44,920
Now this verse was, these two verses were told in relation to the Buddha's two chief disciples.

17
00:01:44,920 --> 00:01:49,160
And their story, in brief, is that they were living in Rajakaha

18
00:01:49,160 --> 00:01:52,200
and the time when the Buddha had just become enlightened.

19
00:01:52,200 --> 00:01:57,560
And they had left the home life because they had seen that there was no

20
00:01:57,560 --> 00:02:02,680
essence or nothing, it wasn't essential or there was no benefit

21
00:02:02,680 --> 00:02:05,000
to be found in the life that they were living.

22
00:02:05,000 --> 00:02:08,360
They would go to these shows and they would laugh and they would cry and

23
00:02:08,360 --> 00:02:14,440
they found that it brought them no real true peace and happiness because it was

24
00:02:14,440 --> 00:02:20,600
bereft of any sort of essence or any depth.

25
00:02:20,600 --> 00:02:24,200
And so they left home and they went to try to find a teacher

26
00:02:24,200 --> 00:02:28,920
and they found the first teacher they found was named Sanjaya.

27
00:02:28,920 --> 00:02:31,960
Sanjaya was a teacher in the time of the Buddha and he thought

28
00:02:31,960 --> 00:02:38,200
his students how to avoid views, how to avoid

29
00:02:38,200 --> 00:02:42,360
any kind of teaching actually. So when people would ask them question they would say

30
00:02:42,360 --> 00:02:45,560
well I don't believe that but I don't believe otherwise.

31
00:02:45,560 --> 00:02:50,600
And so they would find this way to avoid answering the question and therefore

32
00:02:50,600 --> 00:02:55,400
avoid taking any committing to any stance. Well of course this has the benefit

33
00:02:55,400 --> 00:03:00,760
at least superficially of keeping a person from developing wrong views but it

34
00:03:00,760 --> 00:03:04,120
becomes a real conceit and attachment in itself.

35
00:03:04,120 --> 00:03:09,880
And of course the mind is always running, running around in circles trying to

36
00:03:09,880 --> 00:03:16,280
avoid any kind of stance on the theory and that you develop the

37
00:03:16,280 --> 00:03:22,440
theory as a result there's this theory that any stance is untenable.

38
00:03:22,440 --> 00:03:25,400
Which of course would make sense when no one has

39
00:03:25,400 --> 00:03:29,960
the understanding of reality because all of the people's views in the time

40
00:03:29,960 --> 00:03:38,040
were based simply on their partial or incorrect understanding of reality.

41
00:03:38,040 --> 00:03:43,080
But once you find the truth and then they would avoid even that

42
00:03:43,080 --> 00:03:47,640
they would avoid committing even to the truth. And so these

43
00:03:47,640 --> 00:03:50,680
at first they thought this was kind of a neat idea and so they practiced and

44
00:03:50,680 --> 00:03:53,640
when they realized that it was really quite superficial and shallow and

45
00:03:53,640 --> 00:03:58,040
simple actually all you have to do is just avoid everything. Avoid

46
00:03:58,040 --> 00:04:02,120
answering these difficult questions and there you have this teaching.

47
00:04:02,120 --> 00:04:05,800
So they left it behind as well and they went to find another teacher

48
00:04:05,800 --> 00:04:10,040
and then it so happened that one of them Upatissa their names were Upatissa and

49
00:04:10,040 --> 00:04:16,600
Colita. Upatissa one day he saw one of the Buddhist disciples walking for

50
00:04:16,600 --> 00:04:20,680
arms. It was one of the first five disciples of the Buddha

51
00:04:20,680 --> 00:04:24,520
and it had such a profound effect on him. It's just seeing this

52
00:04:24,520 --> 00:04:29,240
monk walking on arms because obviously this was an enlightened being

53
00:04:29,240 --> 00:04:33,560
and so the way he walked the way he held his robe, the way he held his

54
00:04:33,560 --> 00:04:37,080
ball, the way he looked, the way he

55
00:04:37,080 --> 00:04:41,000
would talk, the way he would keep silent. Everything about the way he acted

56
00:04:41,000 --> 00:04:44,280
his whole department said that there was something special about him. It was

57
00:04:44,280 --> 00:04:49,560
just so natural and so clear and so perfect and it wasn't that the

58
00:04:49,560 --> 00:04:55,880
way he was had some heavy little kind of focused or forced

59
00:04:55,880 --> 00:05:00,200
way of behaving it was that it was very natural and very fluid

60
00:05:00,200 --> 00:05:07,720
and and and it spoke volumes about his inner character. So he took this as a

61
00:05:07,720 --> 00:05:13,000
sign and decided that he would go up and talk to this monk.

62
00:05:13,000 --> 00:05:16,040
He waited for him to finish arms and finish eating and then he asked him. He

63
00:05:16,040 --> 00:05:17,960
came up to him and asked him and said,

64
00:05:17,960 --> 00:05:22,280
''Benerable sir, please tell me who is your teacher and what is his teaching.''

65
00:05:22,280 --> 00:05:28,920
And the monk asked him she was his name, said my teacher is the great Buddha

66
00:05:28,920 --> 00:05:31,960
and a fully enlightened Buddha and he said, and his

67
00:05:31,960 --> 00:05:36,840
as for his teaching he said, ''I've only recently gone forth. I'm a new

68
00:05:36,840 --> 00:05:40,600
I'm new to this, of course the Buddha's teaching was new so there were

69
00:05:40,600 --> 00:05:44,200
they were he was one of the seniors actually but he was still

70
00:05:44,200 --> 00:05:47,320
it was true he was still relatively new and he said so he said to be very

71
00:05:47,320 --> 00:05:51,480
difficult for me to teach it in depth and of course this was a reasonable

72
00:05:51,480 --> 00:05:53,800
thing to say but it also had a profound effect

73
00:05:53,800 --> 00:05:59,400
on a producer because it made him think wow if this is what a novice

74
00:05:59,400 --> 00:06:02,920
acts like and then I wonder what it would be like to be someone who

75
00:06:02,920 --> 00:06:07,240
fully understands the teaching. So he said please give me whatever you know

76
00:06:07,240 --> 00:06:11,320
because obviously you know something so please give me the essence

77
00:06:11,320 --> 00:06:15,480
whether it be a lot or what you survive for great words and long

78
00:06:15,480 --> 00:06:19,080
speeches just give me the essence.

79
00:06:19,080 --> 00:06:23,880
So I said you gave him the essence and it's so so core

80
00:06:23,880 --> 00:06:27,720
that it's it's very difficult for us to understand and even more difficult to

81
00:06:27,720 --> 00:06:30,920
understand how such a simple teaching could have the

82
00:06:30,920 --> 00:06:33,560
effect that it did have have on the producer.

83
00:06:33,560 --> 00:06:37,960
The verse goes that the verse that he gave that he said to have given to

84
00:06:37,960 --> 00:06:49,240
a producer was Yedama Heetubabawa Yed Tisangam Tisangheetungtatagataahu. Tisanghtayon Yerodhotya

85
00:06:49,240 --> 00:06:56,280
Yiwangwadimahasamu which means Yedama Heetubabawa those

86
00:06:56,280 --> 00:06:59,720
dhammas or those things which arise based on the

87
00:06:59,720 --> 00:07:08,520
cause. The cause of those dhammas

88
00:07:08,520 --> 00:07:16,360
of those things is what the the tatagata has taught the tatagata has

89
00:07:16,360 --> 00:07:20,440
taught us the causes of those things. This is the first part.

90
00:07:20,440 --> 00:07:24,280
Now when he finished this first part which is half of the verse

91
00:07:24,280 --> 00:07:32,200
he will participate became a soda pan. He realized the truth just from half of it.

92
00:07:32,200 --> 00:07:37,000
Most of us we listen to this and it's incomprehensible.

93
00:07:37,000 --> 00:07:40,760
Yedama Heetubabawa those dhammas that arise based on the cause

94
00:07:40,760 --> 00:07:44,280
the Buddha taught the cause of those dhammas.

95
00:07:44,280 --> 00:07:49,720
It seems so simple and it'd be very difficult. It's near impossible for us to

96
00:07:49,720 --> 00:07:53,560
imagine how it could lead someone to become enlightened and

97
00:07:53,560 --> 00:07:56,680
to assume the most people listening probably didn't become enlightened just

98
00:07:56,680 --> 00:08:02,040
hearing that. The second part they sent to Yodheodhotya

99
00:08:02,040 --> 00:08:07,480
Yewangwadimahasamu and the cessation of those dhammas.

100
00:08:07,480 --> 00:08:11,080
The great samana, the great sage of the great breakfast

101
00:08:11,080 --> 00:08:15,400
has this is his teaching. His teaching is the cessation

102
00:08:15,400 --> 00:08:20,760
of those dhammas. So actually what is teaching here is the

103
00:08:20,760 --> 00:08:23,720
foreignable truths that he learned from the Buddha and the original

104
00:08:23,720 --> 00:08:29,320
teaching and it's so it's so simple and maybe the idea is to

105
00:08:29,320 --> 00:08:33,080
humble the patissa because a sage he may not know that he would be able to

106
00:08:33,080 --> 00:08:38,760
understand it but it's also so core that Upatissa

107
00:08:38,760 --> 00:08:42,440
was able to grasp what is essential and he was able to see

108
00:08:42,440 --> 00:08:47,400
that actually this is the essential is that suffering has a cause

109
00:08:47,400 --> 00:08:51,000
that the problem in life or all of our questions

110
00:08:51,000 --> 00:08:55,720
can be answered based on cause and effect that every problem that I have

111
00:08:55,720 --> 00:09:00,600
and this is based on our meditation. It wasn't based simply on thinking

112
00:09:00,600 --> 00:09:02,920
but when he looked inside he was able to see

113
00:09:02,920 --> 00:09:06,920
all of the suffering, all of the delusion, all of the passion, all of the

114
00:09:06,920 --> 00:09:10,440
hatred and all of the conceits and problems that we have inside.

115
00:09:10,440 --> 00:09:14,280
They all have to do with cause and effect and when you see cause and effect

116
00:09:14,280 --> 00:09:17,880
you see this leads to this leads to that you're able to break it apart

117
00:09:17,880 --> 00:09:21,400
when you look and at the things you're looking at you see how they

118
00:09:21,400 --> 00:09:26,760
react with each other then you're able to change that you're

119
00:09:26,760 --> 00:09:30,840
able to refine your behavior as a result and more importantly

120
00:09:30,840 --> 00:09:34,200
is the second part where it talks about the sensation that actually with the

121
00:09:34,200 --> 00:09:37,640
Buddha focused on was the sensation of dhammas

122
00:09:37,640 --> 00:09:41,000
that actually there is no realization that there is nothing

123
00:09:41,000 --> 00:09:45,160
that is going to bring true peace and happiness. There's no one reality

124
00:09:45,160 --> 00:09:48,280
and so when you give them up when you let go of them that's true peace and

125
00:09:48,280 --> 00:09:51,320
true happiness. This is the meaning of it. Very difficult for us to

126
00:09:51,320 --> 00:09:55,560
understand but for what I say was actually quite

127
00:09:55,560 --> 00:09:59,560
quite simple. He had been training for a long time. In fact the story goes that

128
00:09:59,560 --> 00:10:03,960
he had gone throughout India or throughout that part of India

129
00:10:03,960 --> 00:10:07,080
and he would go up to teachers and ask them questions and he would answer all

130
00:10:07,080 --> 00:10:10,360
of their questions but they couldn't answer his questions and so he

131
00:10:10,360 --> 00:10:13,480
was never able to find a teacher. So he was actually quite

132
00:10:13,480 --> 00:10:18,040
keen. He was able to debate with anyone and to

133
00:10:18,040 --> 00:10:23,720
best anyone in an argument. So he had a very keen mind and that's why he was

134
00:10:23,720 --> 00:10:28,840
able to. He was ready to accept this and when he tried to find fault with it

135
00:10:28,840 --> 00:10:32,920
and tried to examine it in terms of his own experience

136
00:10:32,920 --> 00:10:37,560
he actually allowed him to open up and to become free.

137
00:10:37,560 --> 00:10:41,080
So immediately he said stop. That's enough. Thank you.

138
00:10:41,080 --> 00:10:46,520
Please tell me where is our teacher. At this point he had no doubt

139
00:10:46,520 --> 00:10:50,360
and Asaji said he's staying in Vailarana which is the bamboo group.

140
00:10:50,360 --> 00:10:54,120
So immediately Upatissa went immediately back to where his friend was staying

141
00:10:54,120 --> 00:10:59,320
and his friend Colita saw him coming and he went right away just as Upatissa had

142
00:10:59,320 --> 00:11:01,960
known that this was an enlightened being this was something

143
00:11:01,960 --> 00:11:05,080
with special and so he said something's different with my friend. He said

144
00:11:05,080 --> 00:11:07,880
please tell me Upatissa have you found the truth

145
00:11:07,880 --> 00:11:10,440
and Upatissa said yes I found the truth. He said

146
00:11:10,440 --> 00:11:14,120
please share it with me. And he told him the same verse

147
00:11:14,120 --> 00:11:18,040
and as a result of this teaching Colita as well was able to become a

148
00:11:18,040 --> 00:11:22,840
Sotopan which is the first stage of enlightenment.

149
00:11:22,840 --> 00:11:27,480
And so he said let's go. We have to go and meet our teacher now

150
00:11:27,480 --> 00:11:31,080
and Upatissa said but first we should go and see our own teacher

151
00:11:31,080 --> 00:11:33,400
and give him the opportunity to hear this as well. I have to

152
00:11:33,400 --> 00:11:38,520
respect for his patronage and they they discussed it and

153
00:11:38,520 --> 00:11:41,880
Upatissa said you know I really think that he'll be able to

154
00:11:41,880 --> 00:11:46,440
understand it and and appreciate it and if he does appreciate it it'll be

155
00:11:46,440 --> 00:11:50,360
able to come with us very quickly realize the truth as we have.

156
00:11:50,360 --> 00:11:54,040
So they went to see Sanjay and and it was not really as they expected.

157
00:11:54,040 --> 00:11:57,560
They said they explained the case to him and Sanjay said oh yes go ahead

158
00:11:57,560 --> 00:12:00,920
he said well come with us they said well come with us you can come and learn

159
00:12:00,920 --> 00:12:07,080
as well and Sanjay has said my son's how can you say such a thing look

160
00:12:07,080 --> 00:12:11,720
here I am a great teacher I am my own students and I am famous

161
00:12:11,720 --> 00:12:15,000
you want me to go and become a student again

162
00:12:15,000 --> 00:12:19,880
something very big and and someone very big and important like myself to go

163
00:12:19,880 --> 00:12:24,840
like that. He said it's such a thing as impossible

164
00:12:24,840 --> 00:12:28,760
and and Upatissa said that's really not the point you know it really does it

165
00:12:28,760 --> 00:12:31,960
really matter that these things really have any meaning

166
00:12:31,960 --> 00:12:37,640
what is essential is that we've now found the truth and Sanjay has said

167
00:12:37,640 --> 00:12:41,160
look let me put it to you this way he said are there more

168
00:12:41,160 --> 00:12:45,560
are there more wise people or more foolish people in the world

169
00:12:45,560 --> 00:12:51,240
and Upatissa said or Upatissa and Khalito or whoever said

170
00:12:51,240 --> 00:12:56,440
oh surely the foolish people are far more the one number of wise people in the world

171
00:12:56,440 --> 00:13:03,640
of very few and Sanjay said well then my son's you go and be with the wise

172
00:13:03,640 --> 00:13:08,440
Gautama which is the family name of the Buddha and all of the foolish people will

173
00:13:08,440 --> 00:13:13,720
come to me and I'll be very famous and I haven't great following

174
00:13:13,720 --> 00:13:17,400
so that's what he said and Upatissa and Khalito that you know it was like the last

175
00:13:17,400 --> 00:13:21,240
try really you know broke their faith in their teacher entirely and they

176
00:13:21,240 --> 00:13:23,560
said you know well in that case well that's

177
00:13:23,560 --> 00:13:26,840
but by the time the part ways he said how can you

178
00:13:26,840 --> 00:13:31,000
this is crazy if if that's all it's important to you

179
00:13:31,000 --> 00:13:34,600
that you should be surrounded by fools instead of be with a few wise

180
00:13:34,600 --> 00:13:39,400
the few wise people then let's go so they left and what happened when they

181
00:13:39,400 --> 00:13:43,320
left interestingly enough is that Sanjay lost all of his followers

182
00:13:43,320 --> 00:13:46,600
that he thought as a result of his teaching something foolish he would

183
00:13:46,600 --> 00:13:50,600
gain a lot of followers but what happened is actually most of them left with

184
00:13:50,600 --> 00:13:56,040
Sanjay with Upatissa and Khalito and as a result he got very angry and

185
00:13:56,040 --> 00:14:01,880
upset and got very sick and they said to have vomited blood as a result

186
00:14:01,880 --> 00:14:06,120
that's what the story says so Upatissa and Khalito went to see the Buddha

187
00:14:06,120 --> 00:14:10,040
and when they went to see the Buddha they they got in

188
00:14:10,040 --> 00:14:13,000
they got ordained by the Buddha and then eventually they became enlightened

189
00:14:13,000 --> 00:14:17,400
it took them longer than actually most other of the Buddhist disciples for

190
00:14:17,400 --> 00:14:21,960
Khalito it took him seven days for Upatissa it took him 15 days which

191
00:14:21,960 --> 00:14:25,160
you know considering their their perfection of mind is

192
00:14:25,160 --> 00:14:30,040
is quite a long time but the commentary says this is because of the

193
00:14:30,040 --> 00:14:34,200
profound view of their wish in the past that in past lives they had made wishes

194
00:14:34,200 --> 00:14:38,680
to become the Buddha's chief disciples and so as a result they had to do all

195
00:14:38,680 --> 00:14:42,200
their mind was set on that and they had to therefore go through

196
00:14:42,200 --> 00:14:47,640
a very profound understanding of reality before they could come to their

197
00:14:47,640 --> 00:14:51,640
final liberation especially Upatissa it took him longer

198
00:14:51,640 --> 00:14:54,920
now their names changed it Upatissa became Sariputa his mother was

199
00:14:54,920 --> 00:14:59,480
Saris and they called him Sariputa Mogulana I believe the village of the

200
00:14:59,480 --> 00:15:03,400
family was called Mogulana so they called him Mogulana I moved the family

201
00:15:03,400 --> 00:15:06,440
so they became Sariput and Mogulana they're very famous in

202
00:15:06,440 --> 00:15:12,600
Buddhist circles very well now after they became enlightened they

203
00:15:12,600 --> 00:15:15,960
they happened to be discussing or they went to ask the Buddha to talk to them

204
00:15:15,960 --> 00:15:21,160
and they said you know look miserable sir we were when we came when we wanted

205
00:15:21,160 --> 00:15:24,120
to come see you we told we told our teacher and this is what he said

206
00:15:24,120 --> 00:15:28,600
he said that he would stay with the fools and that

207
00:15:28,600 --> 00:15:31,080
therefore be very famous and you can go with with

208
00:15:31,080 --> 00:15:37,000
budgotama and be surrounded by just a few of the wise people and therefore be

209
00:15:37,000 --> 00:15:41,160
relatively insignificant and this is when the Buddha gave this verse

210
00:15:41,160 --> 00:15:46,600
he said Saris Saramatim he said a person who so this is what happens

211
00:15:46,600 --> 00:15:51,800
for a person who dwells on what is unessential

212
00:15:51,800 --> 00:15:55,560
they'll never come to the truth but for a person who does see what is

213
00:15:55,560 --> 00:15:59,720
essential as essential and what is unessential as unessential

214
00:15:59,720 --> 00:16:03,160
such a person does come to the truth it has to do with

215
00:16:03,160 --> 00:16:06,280
right understanding and obviously Sanjay I had a very wrong

216
00:16:06,280 --> 00:16:11,240
understanding now what this verse teaches us it teaches us many things on many

217
00:16:11,240 --> 00:16:16,360
levels the first thing that it teaches us is about our lives

218
00:16:16,360 --> 00:16:19,560
and how we often dwell in things that are unessential

219
00:16:19,560 --> 00:16:23,080
the example of Kolkata Nupatis is a good one that they were living a life of

220
00:16:23,080 --> 00:16:27,400
luxury and and opulence and and great

221
00:16:27,400 --> 00:16:30,520
sensual pleasure so they would go to these shows and

222
00:16:30,520 --> 00:16:33,880
it said that at this show they would have so much fun and and be entertained

223
00:16:33,880 --> 00:16:37,800
until one day they just realized how pointless it was

224
00:16:37,800 --> 00:16:41,240
and most of us have very different very difficult time coming to this

225
00:16:41,240 --> 00:16:44,840
realization we might have great suffering and depression and

226
00:16:44,840 --> 00:16:48,040
sadness at times in our lives but we're not able to

227
00:16:48,040 --> 00:16:51,080
make the connection and we still are living in under the

228
00:16:51,080 --> 00:16:56,120
misguided belief that somehow this sensual enjoyment is going to bring us

229
00:16:56,120 --> 00:16:58,760
someday or somehow is going to bring us true peace and

230
00:16:58,760 --> 00:17:02,920
happiness or we believe that some things just wrong with us the fact that we

231
00:17:02,920 --> 00:17:07,800
can't find happiness in this way is means there's something wrong so

232
00:17:07,800 --> 00:17:10,840
often we'll take medication for our depression or so on

233
00:17:10,840 --> 00:17:13,880
thinking that somehow as a result we're going to be happy

234
00:17:13,880 --> 00:17:17,080
and enjoy all of these things that aren't really bringing us true peace and

235
00:17:17,080 --> 00:17:21,240
happiness sorry put in Mogulana we're able to see through that and they were

236
00:17:21,240 --> 00:17:24,840
able to see that no and it's it's a difficult thing to see because

237
00:17:24,840 --> 00:17:28,920
there's so much pressure on from society society says this is good this is

238
00:17:28,920 --> 00:17:33,400
right this is the sort of thing that we're supposed to enjoy

239
00:17:33,400 --> 00:17:37,400
so this is something that we should take to heart and to

240
00:17:37,400 --> 00:17:41,320
use to help to change our view and our understanding that actually

241
00:17:41,320 --> 00:17:44,360
it's true that we're not going to find true peace and happiness through these

242
00:17:44,360 --> 00:17:47,880
things and if we expect to we're only going to be disappointed again and

243
00:17:47,880 --> 00:17:51,080
again and eventually be with the ultimate disappointment when we have to

244
00:17:51,080 --> 00:17:54,760
leave it all behind and realize that we've wasted our whole lives on

245
00:17:54,760 --> 00:17:57,960
things which are unessential and useless

246
00:17:57,960 --> 00:18:01,880
as a result will never come to the truth we'll die without coming to the truth

247
00:18:01,880 --> 00:18:04,840
and this is how people live their lives we waste our time

248
00:18:04,840 --> 00:18:07,640
so it's important that we should remember the Buddha's teaching not to let the

249
00:18:07,640 --> 00:18:13,160
moment pass us by don't waste your time in this life that we have

250
00:18:13,160 --> 00:18:16,920
the second the second thing it has to teach us is in regards to

251
00:18:16,920 --> 00:18:21,400
the unessential and in the religious path now Sanjay I was someone who

252
00:18:21,400 --> 00:18:25,240
you could expect had gained some of this realization and therefore had left

253
00:18:25,240 --> 00:18:29,000
the home life he may have done it for the wrong reasons but it's possible that

254
00:18:29,000 --> 00:18:32,360
he left the home life on to good intentions trying to find the truth

255
00:18:32,360 --> 00:18:37,480
the problem is that in the end he hit upon that which was totally unessential

256
00:18:37,480 --> 00:18:42,120
and he got caught up in what we say are the the locomotives he got caught up in

257
00:18:42,120 --> 00:18:49,560
fame and praise and wealth and and and pleasure you know all of these things

258
00:18:49,560 --> 00:18:54,200
that he got from being a great teacher and as a result he he saw that these

259
00:18:54,200 --> 00:18:56,760
were essentially thought it was essential to have lots of students and

260
00:18:56,760 --> 00:19:00,360
the new famous and that this was the most important even to the point where he

261
00:19:00,360 --> 00:19:05,240
thought it was better to be surrounded by foolish people to be surrounded by

262
00:19:05,240 --> 00:19:10,600
wise people in which he figured would be a lot fewer

263
00:19:10,600 --> 00:19:14,760
and so you find this often or you do find this in in

264
00:19:14,760 --> 00:19:19,800
religious circles and in in terms of religious teachers that often people

265
00:19:19,800 --> 00:19:23,400
get caught up in teaching and put aside their own practice or that

266
00:19:23,400 --> 00:19:27,400
people get caught up in you see many many people who call on this

267
00:19:27,400 --> 00:19:29,960
robe and then got caught up in as I've said before

268
00:19:29,960 --> 00:19:34,440
got up in fame got caught up in in wealth and we talked about this

269
00:19:34,440 --> 00:19:38,120
in the verses when they were at that that it's very easy to get caught up in

270
00:19:38,120 --> 00:19:41,720
these things and you see monks even with the best intentions will

271
00:19:41,720 --> 00:19:45,720
eventually get caught up in luxury and so on and and then they need this and

272
00:19:45,720 --> 00:19:50,840
that and and a nice bed and nice good food and

273
00:19:50,840 --> 00:19:54,360
they'll often start to obsess over these things and they won't they won't be

274
00:19:54,360 --> 00:19:57,240
able to see that these are unessential people who worry

275
00:19:57,240 --> 00:20:00,600
become a monk and then worry about their health to worry about their food

276
00:20:00,600 --> 00:20:05,320
who worry about their their bedding and so on

277
00:20:05,320 --> 00:20:08,920
you know I've been through all that but I think if you practice meditation

278
00:20:08,920 --> 00:20:12,440
you are able you should be able to overcome it when there were times where I

279
00:20:12,440 --> 00:20:15,480
said to myself you know I don't have I don't even have

280
00:20:15,480 --> 00:20:18,920
I didn't even have soap or I didn't have laundry powder and these things

281
00:20:18,920 --> 00:20:24,200
you know I just came it just was such a shock to me it was so

282
00:20:24,200 --> 00:20:27,400
such a stress that I didn't have these things what was I going to do and then I

283
00:20:27,400 --> 00:20:31,240
said well well then I realized well then I just won't wash and I won't wash my

284
00:20:31,240 --> 00:20:35,560
clothes that's the the answer and so I was able to let go of it

285
00:20:35,560 --> 00:20:39,800
realized that that's really unessential all of these things are an essential

286
00:20:39,800 --> 00:20:44,200
when some days we will go hungry and then you think oh no what a terrible thing

287
00:20:44,200 --> 00:20:47,400
what am I going to do then the answer is well you're going to be hungry

288
00:20:47,400 --> 00:20:51,320
that's it really doesn't mean anything maybe you'll get sick

289
00:20:51,320 --> 00:20:54,600
maybe you'll die doesn't really mean anything

290
00:20:54,600 --> 00:20:59,400
and when you put your emphasis on these things only I be healthy only I be

291
00:20:59,400 --> 00:21:05,800
I have a full stomach only I be alive even when you when this is your your

292
00:21:05,800 --> 00:21:10,040
essence you've missed the point what is the essence according to the

293
00:21:10,040 --> 00:21:13,720
Buddha there are five things and and even the third thing this teaches

294
00:21:13,720 --> 00:21:17,400
is that even in regards to what is essential we have to prioritize

295
00:21:17,400 --> 00:21:21,400
or we have to be complete so the Buddha would always give talks on what is the

296
00:21:21,400 --> 00:21:25,880
essence what is the core and there are five candidates there's

297
00:21:25,880 --> 00:21:34,760
morality concentration wisdom release of freedom and the awareness or the

298
00:21:34,760 --> 00:21:39,320
knowledge of freedom and we need all five of these some people

299
00:21:39,320 --> 00:21:42,520
take up some people are able to put aside the worldly dominant

300
00:21:42,520 --> 00:21:50,360
worldly pursuits and pursue after at least morality

301
00:21:50,360 --> 00:21:53,480
and so as a result they're able to keep morality they don't touch money they

302
00:21:53,480 --> 00:21:58,360
don't eat at the wrong the only eat once a day and they wear robes and

303
00:21:58,360 --> 00:22:04,280
so on even maybe wear rag robes they're able to do all these these these

304
00:22:04,280 --> 00:22:09,160
practices of morality but they become considered as a result and they see

305
00:22:09,160 --> 00:22:11,800
that as the essence and the Buddha said no this is not the essence

306
00:22:11,800 --> 00:22:16,120
this isn't the core it's part of the essence but it's only the first part

307
00:22:16,120 --> 00:22:19,880
if you don't go any further you you still haven't found the essence

308
00:22:19,880 --> 00:22:24,200
so next some people develop concentration so they're able to focus their mind

309
00:22:24,200 --> 00:22:28,520
and fix their minds on one object their minds become very quiet

310
00:22:28,520 --> 00:22:32,040
and very focused they can even gain magical powers they were that day

311
00:22:32,040 --> 00:22:36,520
himself they had a very focused mind and even gain magical powers at some

312
00:22:36,520 --> 00:22:40,360
point you was able to apparently I think fly through the air he was able to

313
00:22:40,360 --> 00:22:43,880
change his appearance so people he looked different to people

314
00:22:43,880 --> 00:22:49,960
and so on but so obviously this is not the essence and it leads people like

315
00:22:49,960 --> 00:22:54,600
David that to become conceited and therefore what doesn't lead them

316
00:22:54,600 --> 00:22:57,640
people can still become conceited based on these things

317
00:22:57,640 --> 00:23:00,520
and if they have conceited inside and haven't gone further

318
00:23:00,520 --> 00:23:04,520
then they become conceited and they'll use them for the wrong purposes

319
00:23:04,520 --> 00:23:08,600
the next one is wisdom no even people who gain some wisdom in inside can become

320
00:23:08,600 --> 00:23:13,800
conceited if they don't get to the liberation or freedom their mind was

321
00:23:13,800 --> 00:23:16,520
there was still become conceited and they'll believe that they're in light

322
00:23:16,520 --> 00:23:20,200
and just from the insights that they gained people who practice even

323
00:23:20,200 --> 00:23:25,400
inside meditation if they haven't realized the the perfect truth

324
00:23:25,400 --> 00:23:28,840
if their minds haven't become free and liberated they haven't realized the

325
00:23:28,840 --> 00:23:34,600
bananas then they will still fall into conceited and there's still

326
00:23:34,600 --> 00:23:39,480
wrong understanding and the wrong belief that they are enlightened

327
00:23:39,480 --> 00:23:44,680
as a result they will give up the practicing and think that they've done good

328
00:23:44,680 --> 00:23:49,800
enough and will feel content with what they've gained

329
00:23:49,800 --> 00:23:53,800
so this is also not the essence but finally is the fourth and the fifth one and

330
00:23:53,800 --> 00:23:57,640
really they come together but they are separate things the fourth one is

331
00:23:57,640 --> 00:24:01,240
release so when a person does become released in free and

332
00:24:01,240 --> 00:24:06,360
enters into the vanda where the mind is no longer coming out it's no longer

333
00:24:06,360 --> 00:24:10,840
seeing or hearing or smelling or tasting or feeling thinking the mind

334
00:24:10,840 --> 00:24:15,000
goes inside really and for that time doesn't come out

335
00:24:15,000 --> 00:24:18,280
that there is no experience of seeing hearing and spelling tasting

336
00:24:18,280 --> 00:24:22,280
thinking and that's true release and freedom and the mind has just

337
00:24:22,280 --> 00:24:27,240
found its center really and it's not it's not wavering and

338
00:24:27,240 --> 00:24:30,440
at that point the mind is free and this is called freedom this is really the

339
00:24:30,440 --> 00:24:33,160
essence and really there's nothing more that needs to be done

340
00:24:33,160 --> 00:24:37,480
but the next the fifth one is just this the aspect of coming out again

341
00:24:37,480 --> 00:24:41,640
and realizing that what it is that you've experienced then to actually start

342
00:24:41,640 --> 00:24:45,080
thinking about it and the realization that you've become free this

343
00:24:45,080 --> 00:24:48,760
notice as this is the greatest benefit the greatest

344
00:24:48,760 --> 00:24:52,440
essence once you become free the the life that you live in

345
00:24:52,440 --> 00:24:56,680
and the existence as someone who is free and someone who has

346
00:24:56,680 --> 00:25:00,840
seen the truth and as a result doesn't have the cringing

347
00:25:00,840 --> 00:25:04,040
clinging in in regards to that which is

348
00:25:04,040 --> 00:25:07,560
unessential or in regards to anything that is a reason

349
00:25:07,560 --> 00:25:10,360
all of the arisen dumbness because you've seen the cessation

350
00:25:10,360 --> 00:25:14,440
yay dhamma hita pambalaya you've seen all of these arisen things

351
00:25:14,440 --> 00:25:19,560
they center yo new road that you've seen their cessation

352
00:25:19,560 --> 00:25:22,920
so this is the essence and the person shouldn't be inclined towards these

353
00:25:22,920 --> 00:25:26,600
things this is really what we're aiming for and Buddhism and this is

354
00:25:26,600 --> 00:25:30,600
a kind of a teaching for us to see what is

355
00:25:30,600 --> 00:25:34,520
to see what in our lives maybe we are clinging on to that it's unessential

356
00:25:34,520 --> 00:25:38,520
and to remind us of what is truly essential and how we should give up

357
00:25:38,520 --> 00:25:42,200
what is unessential because it winds up and it leads us

358
00:25:42,200 --> 00:25:46,680
to waste our times and to lose the opportunity that we have in this life

359
00:25:46,680 --> 00:25:49,560
to put into practice those teachings that lead us

360
00:25:49,560 --> 00:25:52,840
to realization the truth and true peace happiness

361
00:25:52,840 --> 00:25:56,520
and freedom from suffering so that's our verses for today thank you

362
00:25:56,520 --> 00:26:00,600
for tuning in and wish you all the best

