1
00:00:00,000 --> 00:00:09,920
Hello, today I have an announcement.

2
00:00:09,920 --> 00:00:17,920
In December of last year, I was talking with the head of the International Department

3
00:00:17,920 --> 00:00:27,640
that what Mahat had in Bangkok, Section 5, the meditation section.

4
00:00:27,640 --> 00:00:34,840
He suggested that a good way to spread the dhamma, and he asked me to think about this

5
00:00:34,840 --> 00:00:40,040
and look into the idea of setting it up.

6
00:00:40,040 --> 00:00:49,320
In what would be to take the show on the road, as they say, because here we have centers,

7
00:00:49,320 --> 00:00:56,600
we have places where we teach and spread the dhamma on a local level.

8
00:00:56,600 --> 00:01:05,600
But there are, of course, thousands, maybe many thousands and thousands of people around

9
00:01:05,600 --> 00:01:11,600
the world who don't ever have the opportunity to travel to Thailand or to Sri Lanka or

10
00:01:11,600 --> 00:01:19,720
Burma because of time or money or other constraints.

11
00:01:19,720 --> 00:01:27,960
So he suggested that we put together a team and offer our services.

12
00:01:27,960 --> 00:01:33,040
So we don't set up courses in various places.

13
00:01:33,040 --> 00:01:38,200
We invite other people to set up courses and invite us to come.

14
00:01:38,200 --> 00:01:46,840
Tell us about them and we'll make arrangements to come to teach.

15
00:01:46,840 --> 00:01:54,440
I thought it sounded like a good idea, a little bit forward perhaps, and maybe a little

16
00:01:54,440 --> 00:01:59,080
bit of work, but it sounded interesting.

17
00:01:59,080 --> 00:02:06,600
And when I was talking to the abbot of what Lampung in Chennai, who was also a very well-known

18
00:02:06,600 --> 00:02:16,280
meditation teacher, he invited me tentatively to go with him to Mexico this June.

19
00:02:16,280 --> 00:02:23,160
So you know, it kind of got me thinking and so here finally I've sat down and started

20
00:02:23,160 --> 00:02:30,000
to put something together and come up with something called dhamma charika, dhamma means

21
00:02:30,000 --> 00:02:35,520
the teaching, the truth, and charika means travel.

22
00:02:35,520 --> 00:02:40,480
The word charika was used by the Buddha when he suggested to his disciples that they

23
00:02:40,480 --> 00:02:51,960
should charika to go traveling on a voyage to spread the teachings for the benefit of

24
00:02:51,960 --> 00:02:55,120
the people for the benefit of the world.

25
00:02:55,120 --> 00:03:02,880
And so this is the name I've chosen for this program that's going to be run by Siri

26
00:03:02,880 --> 00:03:08,800
Mongol International, that's the non-profit organization that we've set up.

27
00:03:08,800 --> 00:03:20,320
And it's going to organize or facilitate the organization of courses in meditation around

28
00:03:20,320 --> 00:03:29,920
the world with the help of, hopefully, of a jansupati in Bangkok and a jansupati in Chennai.

29
00:03:29,920 --> 00:03:40,720
So right as it stands, I'm probably going to America and maybe Mexico in May and June.

30
00:03:40,720 --> 00:03:46,200
So I thought, well, why not set up some kind of annual trip?

31
00:03:46,200 --> 00:03:51,120
Because when I go from Sri Lanka to America, I have to make a stop somewhere anyway.

32
00:03:51,120 --> 00:03:57,160
So why not see other people in this country or that country in this region or that region

33
00:03:57,160 --> 00:04:04,360
who have the ability to set up a course, have the interest and the means by which to set

34
00:04:04,360 --> 00:04:07,720
something up.

35
00:04:07,720 --> 00:04:08,720
And so this is what I've done.

36
00:04:08,720 --> 00:04:17,200
I've set up a website at jarika.seriamongalo.org and see the link in the description.

37
00:04:17,200 --> 00:04:18,520
And you can go and check it out.

38
00:04:18,520 --> 00:04:24,080
There's very little information there right now, but basically it's offering a contact

39
00:04:24,080 --> 00:04:31,200
forms of information and you can contact us and let us know if you'd like to host a course

40
00:04:31,200 --> 00:04:36,720
or a meditation talk or whatever host a program in your area.

41
00:04:36,720 --> 00:04:44,120
And we can then discuss back and forth with sort of a program with sort of requirements

42
00:04:44,120 --> 00:04:47,800
there would be.

43
00:04:47,800 --> 00:04:56,240
Now the non-profit organization, Sri'mongalo International has some means, so the idea

44
00:04:56,240 --> 00:05:02,960
is that if there's enough interest in a certain region that we could support or we could

45
00:05:02,960 --> 00:05:08,360
fund the travel expenses, I've got to go any way to America.

46
00:05:08,360 --> 00:05:14,640
So if in this state or that state or America or Canada or some part of North America or

47
00:05:14,640 --> 00:05:19,600
even Central or South America there are people and I know there are people who have contacted

48
00:05:19,600 --> 00:05:27,800
me about holding something in your area, if it's substantial enough, say you have a place

49
00:05:27,800 --> 00:05:32,520
and you have people who want to come to meditate for even a few days and you have room

50
00:05:32,520 --> 00:05:42,520
and you can work out the logistics of organizing the place and food and lodging and so

51
00:05:42,520 --> 00:05:48,680
on, then we'll be happy to come there, you don't need to buy us a plane ticket, at

52
00:05:48,680 --> 00:05:53,400
least I will go and there are the two other teachers, we're going to have to coordinate

53
00:05:53,400 --> 00:05:59,800
about their schedules but I know at least a Jansupati in Bangkok was quite interested

54
00:05:59,800 --> 00:06:04,960
and willing to get involved in this.

55
00:06:04,960 --> 00:06:11,080
If on the other hand there's some place where you don't have so many people or you don't

56
00:06:11,080 --> 00:06:17,440
have a place but you'd like us to come to give a talk, then it's also possible that

57
00:06:17,440 --> 00:06:24,320
if you're willing to pay for a plane ticket from the closest location, suppose I stopped

58
00:06:24,320 --> 00:06:29,960
in Los Angeles and you want me to come to San Francisco or one of the teachers or all

59
00:06:29,960 --> 00:06:35,120
of us or so on, someone to come to give a talk or to hold a day course or so on.

60
00:06:35,120 --> 00:06:40,360
If you can provide transportation then we'll go but if it's substantial enough and if

61
00:06:40,360 --> 00:06:48,760
you can show that you've got the means and the ability to organize something then we'll

62
00:06:48,760 --> 00:06:51,440
come on out.

63
00:06:51,440 --> 00:06:56,040
I think this is a good year to do it and the reason I've gone ahead and made this initiative

64
00:06:56,040 --> 00:07:03,600
is because this year is the 2600th anniversary of the Lord Buddhist Enlightenment so it's

65
00:07:03,600 --> 00:07:13,640
a rather special symbolic year and May is the anniversary of the Buddha's Enlightenment

66
00:07:13,640 --> 00:07:20,400
so this is a good time to hold it and it's a good time to start it so if this is something

67
00:07:20,400 --> 00:07:26,080
that works even if it doesn't work this year at least I've started to organize this

68
00:07:26,080 --> 00:07:32,080
then now we have very little time until May only a few months but if you can't organize

69
00:07:32,080 --> 00:07:36,160
something for this year then let's talk about organizing something for next year.

70
00:07:36,160 --> 00:07:41,880
There's lots of time for that and we can hold an annual course and I'll talk to these

71
00:07:41,880 --> 00:07:46,400
other teachers and there are other teachers that I know who would get involved as well

72
00:07:46,400 --> 00:07:52,080
in this same tradition and we've put something together I really think this is something

73
00:07:52,080 --> 00:07:53,280
that could work.

74
00:07:53,280 --> 00:07:59,640
If it comes to nothing it comes to nothing I'm not suffering I've got my forest but there's

75
00:07:59,640 --> 00:08:04,880
a lot of people out there who like the meditation practice that I teach, that we teach

76
00:08:04,880 --> 00:08:11,480
and these other teachers also teach and would like to go further and realize that the

77
00:08:11,480 --> 00:08:18,200
benefits that you get from the internet are limited and are looking for some way to have

78
00:08:18,200 --> 00:08:24,840
direct contact with the teacher at least for some time I personally would be willing

79
00:08:24,840 --> 00:08:30,600
and I know the other one, the chance to pilot at least would be willing to hold courses

80
00:08:30,600 --> 00:08:36,480
even for a week or two weeks if there's the interest, if you just have the interest in

81
00:08:36,480 --> 00:08:45,560
organizing a course at your local college or community center or church group or whatever

82
00:08:45,560 --> 00:08:54,000
whatever the program is we can work something out I think in many cases and with the potential

83
00:08:54,000 --> 00:08:59,520
even going to several countries in the world so please if you're interested go check

84
00:08:59,520 --> 00:09:05,440
out the website there's not much there as I said but at least you get some basic information

85
00:09:05,440 --> 00:09:11,280
and check back frequently if you're interested and you can see you can also let us know

86
00:09:11,280 --> 00:09:16,160
if you're interested in attending a course in your area so that we can put together a

87
00:09:16,160 --> 00:09:21,960
list and then if someone does come up with something we can send you an email and you

88
00:09:21,960 --> 00:09:27,160
know hey this person is looking to organize something get in touch with them tell them that

89
00:09:27,160 --> 00:09:32,760
you're interested and you want to help or you want to come or you want to get involved

90
00:09:32,760 --> 00:09:39,400
so send us an email through the contact form if you're interested and let's see what

91
00:09:39,400 --> 00:09:45,040
we can do we'll eventually we'll try to put up information on where we're likely to be

92
00:09:45,040 --> 00:09:50,960
headed where there are programs being organized and so on and update the background map

93
00:09:50,960 --> 00:09:57,160
and so on but for now it's it is what it is this is just the start don't be discouraged

94
00:09:57,160 --> 00:10:03,240
by the fact that there's nothing there this is the beginning and so let's work together

95
00:10:03,240 --> 00:10:10,600
to spread the teachings of the Buddha for the benefit of humanity for bringing true peace

96
00:10:10,600 --> 00:10:15,880
happiness and freedom from suffering to the world please do get in touch if you have questions

97
00:10:15,880 --> 00:10:23,320
just ask questions don't don't post to this video on YouTube go to the website ask questions

98
00:10:23,320 --> 00:10:28,760
through the through the contact form and we'll try to update and put more information

99
00:10:28,760 --> 00:10:36,880
up as questions common as as is necessary okay so thanks for listening and look forward

100
00:10:36,880 --> 00:10:52,880
to hearing from you all all the best

