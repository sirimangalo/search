1
00:00:00,000 --> 00:00:18,000
Okay, we're ready to go.

2
00:00:18,000 --> 00:00:25,000
Hello, everyone.

3
00:00:25,000 --> 00:00:30,000
Good broadcasting live.

4
00:00:30,000 --> 00:00:33,000
November 4th.

5
00:00:33,000 --> 00:00:38,000
So we're trying to mopada again.

6
00:00:38,000 --> 00:00:40,000
I'm really trying with this.

7
00:00:40,000 --> 00:00:45,000
Really, and the problem is we're in cramped quarters.

8
00:00:45,000 --> 00:00:54,000
I've tried several things tonight and just makes me, reminds me of all the troubles involved

9
00:00:54,000 --> 00:01:05,000
with this recording stuff, batteries, lights, big white sheets, tacked to your wall.

10
00:01:05,000 --> 00:01:17,000
Anyway, we'll keep trying.

11
00:01:17,000 --> 00:01:20,000
Hello, and welcome back to our study of the demo bada.

12
00:01:20,000 --> 00:01:29,000
Today we continue on with verses 104 and 105, which read as follows.

13
00:01:29,000 --> 00:01:56,000
We're going to start with a video of the video of the video of the video of the video of the video.

14
00:01:56,000 --> 00:02:02,000
Which means actually quite similar to the last one.

15
00:02:02,000 --> 00:02:12,000
Conquest of the self is better than the conquest of others, of other people.

16
00:02:12,000 --> 00:02:28,000
For one who has conquered themselves and is constantly restrained.

17
00:02:28,000 --> 00:02:52,000
Not giving rise to uncontrolled behavior or activity that causes harm to others.

18
00:02:52,000 --> 00:03:02,000
It's also another type of angel, I guess.

19
00:03:02,000 --> 00:03:10,000
The maros, the hubramuna, the mara, which is like the evil Satan, nor Brahmas.

20
00:03:10,000 --> 00:03:39,000
They will not be able to turn the victory into the feet, they will not be able to turn the victory of such a person to defeat.

21
00:03:39,000 --> 00:03:50,000
Great verse shows the power of self conquest.

22
00:03:50,000 --> 00:04:02,000
Simple story, story goes, there was a Brahmin who came to the Buddha, and asked him a question.

23
00:04:02,000 --> 00:04:13,000
I guess maybe he was a follower of the Buddha, or he understood how worthy the Buddha was, or how wise the Buddha was.

24
00:04:13,000 --> 00:04:16,000
And so he thought of it, he had this question.

25
00:04:16,000 --> 00:04:25,000
He wondered to himself whether the Buddha, whether Buddha is, whether those who are enlightened,

26
00:04:25,000 --> 00:04:32,000
know only what is a benefit, or do they also know what leads to detriment?

27
00:04:32,000 --> 00:04:40,000
Do they really know about gain, or do they know about loss as well?

28
00:04:40,000 --> 00:04:48,000
Ata, do they know ata mewa jana ti uda hu anatam bhi puchisa anatam bhi?

29
00:04:48,000 --> 00:04:53,000
Do they know only what is ata, ata means what is a benefit, or what is a value,

30
00:04:53,000 --> 00:04:57,000
or do they also know what is detriment?

31
00:04:57,000 --> 00:05:05,000
Anatam, which is harmful, or leads one away from success, leads one away from what is useful,

32
00:05:05,000 --> 00:05:10,000
which is useless, or harmful.

33
00:05:10,000 --> 00:05:13,000
And so he went to ask the Buddha this.

34
00:05:13,000 --> 00:05:17,000
I guess he had some idea that the Buddha only taught people good things.

35
00:05:17,000 --> 00:05:22,000
He only taught everyone's praising how the Buddha's teachings would lead people to good things.

36
00:05:22,000 --> 00:05:28,000
And they were talking about how great the Buddha's teaching Buddha would teach you, do this to that.

37
00:05:28,000 --> 00:05:32,000
And so he hadn't heard the Buddha talk about, don't do this, don't do that.

38
00:05:32,000 --> 00:05:36,000
And so he went to the Buddha, and the Buddha gave him an interesting quote.

39
00:05:36,000 --> 00:05:44,000
And I was just having a terrible time with it, because the English is incredible how sloppy they were with it.

40
00:05:44,000 --> 00:05:53,000
But translate fierceness, Chandikan, as moonlight.

41
00:05:53,000 --> 00:05:58,000
Which is, well, I mean, it's understandable, because Chanda would be the moon.

42
00:05:58,000 --> 00:06:07,000
But Chanda, with the, with the, how the population in the, anyway, doesn't mean moon at all.

43
00:06:07,000 --> 00:06:11,000
Unless there's another version that confuses the two.

44
00:06:11,000 --> 00:06:16,000
So the Buddha says, gives a list of six things.

45
00:06:16,000 --> 00:06:21,000
And I think I've got them all down.

46
00:06:21,000 --> 00:06:25,000
And he says, so he goes and asks the Buddha, do you know about loss as well?

47
00:06:25,000 --> 00:06:28,000
And the Buddha says, of course, and he tells him.

48
00:06:28,000 --> 00:06:33,000
He says, a tancha, han brahmana, janami, a tancha.

49
00:06:33,000 --> 00:06:38,000
I know both that, which is a benefit and that, which is judgment.

50
00:06:38,000 --> 00:06:44,000
And so he taught, says, please then tell me about what is to detriment.

51
00:06:44,000 --> 00:06:47,000
And the Buddha says, well, then listen.

52
00:06:47,000 --> 00:06:51,000
And he gives this verse, which isn't the dhammapanda verse.

53
00:06:51,000 --> 00:06:53,000
But there's, this is often the case with these stories.

54
00:06:53,000 --> 00:06:59,000
There will be other verses that, where they come from exactly, we don't know where they've been recorded.

55
00:06:59,000 --> 00:07:03,000
We don't know, but it's supposed to have been said by the Buddha as well.

56
00:07:03,000 --> 00:07:08,000
So he says, usura saiyan, which means, clear, that one's clear.

57
00:07:08,000 --> 00:07:10,000
It means sleeping beyond sunrise.

58
00:07:10,000 --> 00:07:12,000
So sleeping during the day.

59
00:07:12,000 --> 00:07:14,000
Well, it's time to get up and go to work.

60
00:07:14,000 --> 00:07:18,000
One, instead, stays in bed and doesn't do one's work.

61
00:07:18,000 --> 00:07:21,000
Allah siyang, laziness.

62
00:07:21,000 --> 00:07:26,000
And one is just lazy and doesn't want to do good things.

63
00:07:26,000 --> 00:07:31,000
Tendi kang means, again, ferocity, so being mean or nasty or cruel,

64
00:07:31,000 --> 00:07:39,000
being, you know, when people are overbearing and just constantly irritable.

65
00:07:39,000 --> 00:07:44,000
And then Dika sone dhyang is an interesting one, but I think, because Dika means long,

66
00:07:44,000 --> 00:07:50,000
but sone dhyang means addiction to intoxication or addiction to drink,

67
00:07:50,000 --> 00:07:52,000
usually refers to.

68
00:07:52,000 --> 00:07:55,000
And maybe intoxication is better.

69
00:07:55,000 --> 00:07:59,000
So I think it says something to do with drinking and toxicating drinks.

70
00:07:59,000 --> 00:08:03,000
And it may be a corruption, but I probably just don't know what it means.

71
00:08:03,000 --> 00:08:05,000
Anyway, but the English is terrible.

72
00:08:05,000 --> 00:08:07,000
It doesn't have any event.

73
00:08:07,000 --> 00:08:08,000
What does it say?

74
00:08:08,000 --> 00:08:14,000
Long continued prosperity, which is ridiculous, because Dika is long.

75
00:08:14,000 --> 00:08:19,000
But long continued prosperity is not a cause for ruin, except it can make you negligent,

76
00:08:19,000 --> 00:08:23,000
I suppose, but that's certainly not what's being said here.

77
00:08:23,000 --> 00:08:27,000
And then we've got, I think it's addiction to strong drink.

78
00:08:27,000 --> 00:08:30,000
Why? Because these come from the...

79
00:08:30,000 --> 00:08:34,000
or these are echoed in the...

80
00:08:39,000 --> 00:08:43,000
and the Dika nikaya sutta.

81
00:08:44,000 --> 00:08:46,000
It's the Siegelovada sutta.

82
00:08:46,000 --> 00:08:49,000
It discords the Siegelovada sutta.

83
00:08:49,000 --> 00:08:53,000
Siegelovada sutta talks about these various states that lead to loss, right?

84
00:08:53,000 --> 00:08:58,000
And it's addiction to drink is, of course, one of them.

85
00:08:58,000 --> 00:09:01,000
Ika sutta nangamana.

86
00:09:01,000 --> 00:09:08,000
So go... I believe means traveling alone, which they just translate as going on long journeys,

87
00:09:08,000 --> 00:09:11,000
but going on journeys doesn't lead to loss.

88
00:09:11,000 --> 00:09:14,000
Going on long journeys alone can lead to problems.

89
00:09:14,000 --> 00:09:18,000
In the time of the Buddha, it certainly could, because you could get robbed.

90
00:09:18,000 --> 00:09:24,000
It was a big deal, if you don't travel alone, it wasn't, it just wasn't a good idea.

91
00:09:24,000 --> 00:09:30,000
And finally, Para da rupa savanum, which means,

92
00:09:30,000 --> 00:09:34,000
chasing after other people's wives.

93
00:09:34,000 --> 00:09:37,000
Which was apparently a thing, or is really a thing.

94
00:09:37,000 --> 00:09:40,000
People committed adultery all the time.

95
00:09:40,000 --> 00:09:48,000
Bad idea leads to loss, leads to disrespect, leads to loss of friendship, etc.

96
00:09:48,000 --> 00:09:54,000
So he taught these things, and maybe he taught others, but that's the sort of things he taught.

97
00:09:54,000 --> 00:10:00,000
Certainly in the Siegelovada sutta, he taught many other things that are to your detriment.

98
00:10:00,000 --> 00:10:06,000
And it's curious, you know, because he doesn't say gambling.

99
00:10:06,000 --> 00:10:11,000
And yet gambling is considered one of the things that leads to loss.

100
00:10:11,000 --> 00:10:16,000
I don't believe it's in here, if I've translated it correctly, it's not there.

101
00:10:16,000 --> 00:10:20,000
And in the Siegelovada sutta, it's clearly there.

102
00:10:20,000 --> 00:10:29,000
And that's interesting, because the Brahman is then impressed, and it says very good, very good, that's awesome.

103
00:10:29,000 --> 00:10:34,000
Well spoken, you're a really, really great teacher, and you really know what is a benefit,

104
00:10:34,000 --> 00:10:37,000
and what is a detriment.

105
00:10:37,000 --> 00:10:44,000
And then the Buddha asks him, so Brahman, how do you make your livelihood?

106
00:10:44,000 --> 00:10:50,000
And turns out, the Brahman makes this Brahman, makes his livelihood through gambling.

107
00:10:50,000 --> 00:10:52,000
That's how he makes a living.

108
00:10:52,000 --> 00:11:03,000
I guess he must be sort of one of these people who call an artist, right?

109
00:11:03,000 --> 00:11:06,000
Or something like that.

110
00:11:06,000 --> 00:11:09,000
I guess there are professional poker players nowadays.

111
00:11:09,000 --> 00:11:12,000
It's a big thing, apparently, to gamble online.

112
00:11:12,000 --> 00:11:21,000
I have people who tell me about that, how they do gambling online.

113
00:11:21,000 --> 00:11:32,000
And so it's interesting that the Buddha didn't include it, but that is common to be very careful not to attack a person directly.

114
00:11:32,000 --> 00:11:43,000
So if you had brought up gambling, there's a bit as a Buddhist, it's more curious than this Brahman probably realized.

115
00:11:43,000 --> 00:11:51,000
He doesn't bring it up, but yet he wants to make it clear that gambling is in there as well,

116
00:11:51,000 --> 00:11:54,000
without actually saying all gambling is going to lead you to ruin.

117
00:11:54,000 --> 00:12:02,000
It's as though he purposefully left it out, or this story, part of the reason for telling this story,

118
00:12:02,000 --> 00:12:12,000
is to, or part of the meaning behind the story is to show that he didn't attack him directly.

119
00:12:12,000 --> 00:12:15,000
It's quite curious.

120
00:12:15,000 --> 00:12:19,000
But can be a really important aspect of teaching.

121
00:12:19,000 --> 00:12:27,000
So anyway, point is he tells him, the Brahman says he makes his living by gambling.

122
00:12:27,000 --> 00:12:33,000
And this is the funny sort of inside joke that we can kind of smile,

123
00:12:33,000 --> 00:12:39,000
because we know it's kind of what's going through the Buddhist mind, to some extent,

124
00:12:39,000 --> 00:12:50,000
that he knows that this Brahman is on a state of loss, and is heading in a bad way.

125
00:12:50,000 --> 00:12:52,000
Anyway.

126
00:12:52,000 --> 00:12:56,000
And so then the Buddha asks him, so who wins when you gamble?

127
00:12:56,000 --> 00:12:59,000
Do you win, or does your opponents win?

128
00:12:59,000 --> 00:13:04,000
He said, oh, well, sometimes I win, sometimes my opponents win.

129
00:13:04,000 --> 00:13:13,000
And then the Buddha says, then Buddha lays down the law, and says, Brahman, that's not a real victory.

130
00:13:13,000 --> 00:13:16,000
As you can see, that's the nature of victory.

131
00:13:16,000 --> 00:13:18,000
Sometimes you win, sometimes you lose.

132
00:13:18,000 --> 00:13:22,000
When we have this saying, you win some, you lose some.

133
00:13:22,000 --> 00:13:26,000
Because it's not a really, it means that on balance you come out with nothing, right?

134
00:13:26,000 --> 00:13:27,000
You never come out ahead.

135
00:13:27,000 --> 00:13:30,000
Or it's certainly not guaranteed that you're going to come out ahead.

136
00:13:30,000 --> 00:13:34,000
And there must be some other factors, often people would say luck,

137
00:13:34,000 --> 00:13:38,000
is all that allows you to come out ahead.

138
00:13:38,000 --> 00:13:42,000
And as we talked about, of course, it doesn't matter whether you come out ahead or not.

139
00:13:42,000 --> 00:13:46,000
When you come out ahead, what happens while you begin enmity?

140
00:13:46,000 --> 00:13:49,000
Your opponents are upset because they've lost.

141
00:13:49,000 --> 00:13:52,000
When you win, someone has to lose.

142
00:13:52,000 --> 00:13:56,000
That kind of victory is not really, he says, a trifling.

143
00:13:56,000 --> 00:14:01,000
That's how the English translate it.

144
00:14:01,000 --> 00:14:05,000
You can't really trust the English.

145
00:14:05,000 --> 00:14:10,000
That's a low, sort of, I believe, no, JOP.

146
00:14:10,000 --> 00:14:13,000
What do you ban on JOP?

147
00:14:13,000 --> 00:14:15,000
That's a question, sorry.

148
00:14:15,000 --> 00:14:18,000
Brahman, up a matricle, yeah, so it's a trifle.

149
00:14:18,000 --> 00:14:21,000
He doesn't say that's a bad kind of victory.

150
00:14:21,000 --> 00:14:25,000
That kind of victory is actually leading you to ruin, which in fact it is.

151
00:14:25,000 --> 00:14:27,000
He says that's a trifling victory.

152
00:14:27,000 --> 00:14:33,000
So in a roundabout way, he's trying to get this Brahman to see the error of his ways.

153
00:14:33,000 --> 00:14:35,000
He'll provide him with some friendly advice.

154
00:14:35,000 --> 00:14:38,000
The Brahman did ask after all what he leads to loss.

155
00:14:38,000 --> 00:14:44,000
And so he's trying to somehow tell him that his life's down.

156
00:14:44,000 --> 00:14:49,000
But instead he says, so to be very gentle, he says that's a trifling victory.

157
00:14:49,000 --> 00:14:51,000
A real victory is a self victory.

158
00:14:51,000 --> 00:14:55,000
A real conquest, that's conquering yourself.

159
00:14:55,000 --> 00:14:59,000
Why? Because that kind of conquest, you'll never lose.

160
00:14:59,000 --> 00:15:03,000
And so it's poignant in relation to this story,

161
00:15:03,000 --> 00:15:06,000
the idea of being invincible.

162
00:15:06,000 --> 00:15:13,000
Because the win of a gambler is always subject to threat.

163
00:15:13,000 --> 00:15:18,000
The next role of the dice, the next deal of the cards,

164
00:15:18,000 --> 00:15:23,000
could be, could change your fortune, could spell ruin.

165
00:15:23,000 --> 00:15:29,000
Whereas, and much victory is like this, victory in war, victory in business,

166
00:15:29,000 --> 00:15:38,000
victory in romance, victory of all kinds is unpredictable.

167
00:15:38,000 --> 00:15:41,000
And his stable is imperman.

168
00:15:41,000 --> 00:15:44,000
It doesn't last forever.

169
00:15:44,000 --> 00:15:52,000
It's always subject to the potential of being overdone, overthrown.

170
00:15:52,000 --> 00:15:55,000
But that's really the claim that's being made.

171
00:15:55,000 --> 00:15:57,000
So this is how it relates to our practices.

172
00:15:57,000 --> 00:16:05,000
It's an understanding of the extent and the magnitude of the practice that we're undertaking.

173
00:16:05,000 --> 00:16:08,000
I mean, this isn't just stress relief.

174
00:16:08,000 --> 00:16:13,000
I was thinking today about advertising the benefits of meditation.

175
00:16:13,000 --> 00:16:16,000
We're probably going to put up posters.

176
00:16:16,000 --> 00:16:26,000
And I mean, we think of people really catchy to say mindfulness-based stress reduction.

177
00:16:26,000 --> 00:16:29,000
Because it's non-sectarian.

178
00:16:29,000 --> 00:16:30,000
It's non-religious.

179
00:16:30,000 --> 00:16:35,000
It's really something that people can sort of rolls off the tongue.

180
00:16:35,000 --> 00:16:40,000
But then I thought it's kind of, you know, petty to say stress reduction.

181
00:16:40,000 --> 00:16:43,000
Because that's not really what we're talking about, is it?

182
00:16:43,000 --> 00:16:47,000
In Buddhism, we go quite, it's mindfulness-based, or it's based on sati.

183
00:16:47,000 --> 00:16:50,000
But it goes farther than just reducing stress.

184
00:16:50,000 --> 00:16:56,000
It uproots anything that could possibly cause you stress in the future.

185
00:16:56,000 --> 00:17:02,000
There's no potential for a future stress.

186
00:17:02,000 --> 00:17:04,000
Meaning, invent your invincible.

187
00:17:04,000 --> 00:17:10,000
And it goes beyond heaven. It goes beyond God. It goes beyond the universe.

188
00:17:10,000 --> 00:17:16,000
Something that goes beyond loss.

189
00:17:16,000 --> 00:17:20,000
Something that can never be taken from you.

190
00:17:20,000 --> 00:17:24,000
We're talking about invincibility.

191
00:17:24,000 --> 00:17:27,000
How does this come about?

192
00:17:27,000 --> 00:17:35,000
It comes about by a person who is at the Danta, Bosa.

193
00:17:35,000 --> 00:17:44,000
Neetjang Sanyatta Charino, who is constantly, constantly restrained.

194
00:17:44,000 --> 00:17:49,000
Meaning, they never, when seeing a form with the eye,

195
00:17:49,000 --> 00:17:52,000
they never give rise to likes or dislikes.

196
00:17:52,000 --> 00:17:56,000
They see it as it is in their objective.

197
00:17:56,000 --> 00:18:05,000
They free themselves from the potential for freedom,

198
00:18:05,000 --> 00:18:15,000
themselves from involvement in this game of gain and loss, happiness and suffering.

199
00:18:15,000 --> 00:18:22,000
It sounds, sounds actually quite fearsome for people who are addicted to pleasure

200
00:18:22,000 --> 00:18:28,000
and who are caught up in this game of gain and loss.

201
00:18:28,000 --> 00:18:35,000
It's something that you do have to kind of finesse when you talk to people.

202
00:18:35,000 --> 00:18:43,000
People often accuse, make the accusation that such a person would be someone like a zombie.

203
00:18:43,000 --> 00:18:52,000
When they are restrained, means they are just suppressing their urges,

204
00:18:52,000 --> 00:18:54,000
which isn't at all the case.

205
00:18:54,000 --> 00:18:56,000
Suppressing your urges wouldn't work.

206
00:18:56,000 --> 00:18:59,000
It's not something that is sustainable.

207
00:18:59,000 --> 00:19:03,000
It's not the true victory.

208
00:19:03,000 --> 00:19:14,000
True victory is imperturbable.

209
00:19:14,000 --> 00:19:18,000
I was thinking about this today, the whole idea of the zombie, right?

210
00:19:18,000 --> 00:19:23,000
We have this idea that a person who doesn't like and dislike things is just like a zombie.

211
00:19:23,000 --> 00:19:26,000
Their life has no flavor, has no meaning.

212
00:19:26,000 --> 00:19:34,000
But then if you look at people who are caught up, if you think of how the state of mind,

213
00:19:34,000 --> 00:19:40,000
a person who's caught up in chasing after things objects of their desire,

214
00:19:40,000 --> 00:19:46,000
how much like a zombie they are, how unaware they are of their own mind,

215
00:19:46,000 --> 00:19:54,000
of their own state, how inflamed the mind becomes with passion and hate.

216
00:19:54,000 --> 00:20:02,000
How the mind just in general becomes inflamed and so is subject to greed, anger and delusion constantly.

217
00:20:02,000 --> 00:20:08,000
Sometimes a such intense greed that they will do anything to get what they want,

218
00:20:08,000 --> 00:20:15,000
even at the detriment to other people, even at the cost of friendship.

219
00:20:15,000 --> 00:20:25,000
Sometimes give rise to such states and delusion that they alienate all of their friends

220
00:20:25,000 --> 00:20:32,000
due to their oppressive, arrogant, conceited nature.

221
00:20:32,000 --> 00:20:42,000
The level of conceit that were capable of the level of arrogance were capable of and how awful it is and how harmful it is.

222
00:20:42,000 --> 00:20:45,000
And anger, how it can be.

223
00:20:45,000 --> 00:21:02,000
So caught up with rage and hatred constantly depressed or angry, sad, afraid of these negative states.

224
00:21:02,000 --> 00:21:04,000
It's kind of like a zombie.

225
00:21:04,000 --> 00:21:14,000
One of those zombie movies or zombie shows that everyone's watching, evil zombies, how horrific they are.

226
00:21:14,000 --> 00:21:16,000
That's kind of what we're talking about.

227
00:21:16,000 --> 00:21:18,000
The lightning people have nothing like that.

228
00:21:18,000 --> 00:21:22,000
The lightning one is the opposite of a zombie they are awake.

229
00:21:22,000 --> 00:21:24,000
They are alive.

230
00:21:24,000 --> 00:21:26,000
They never die.

231
00:21:26,000 --> 00:21:29,000
They're invincible.

232
00:21:29,000 --> 00:21:31,000
So this is what we're striving for.

233
00:21:31,000 --> 00:21:32,000
It's something quite profound.

234
00:21:32,000 --> 00:21:38,000
I teach meditation, not teaching at the university, and I teach to all sorts of people.

235
00:21:38,000 --> 00:21:44,000
And a lot of people are just in it for stress reductions, stress relief.

236
00:21:44,000 --> 00:21:45,000
And so it kind of pulls you in.

237
00:21:45,000 --> 00:21:48,000
And then you start to realize, oh, it goes a little bit deeper.

238
00:21:48,000 --> 00:21:55,000
And you end up down the rabbit hole, so to speak, where you don't recognize the world around you anymore.

239
00:21:55,000 --> 00:21:57,000
Your whole world has changed us.

240
00:21:57,000 --> 00:22:03,000
It's not like it's a trap or something, it's just the sweater starts to unravel.

241
00:22:03,000 --> 00:22:10,000
The whole illusion that you built up around yourself.

242
00:22:10,000 --> 00:22:16,000
And you thought you were starts to unravel, and you see how asleep you've been.

243
00:22:16,000 --> 00:22:19,000
You're like a sleepwalker, like a zombie.

244
00:22:19,000 --> 00:22:20,000
That's how it should be.

245
00:22:20,000 --> 00:22:22,000
That's how it really is in meditation.

246
00:22:22,000 --> 00:22:27,000
You start to wake up and see, oh, I've got these problems, my mind does.

247
00:22:27,000 --> 00:22:30,000
Greed, my mind does anger, my mind does delusion.

248
00:22:30,000 --> 00:22:32,000
Oh, my, I'm in trouble.

249
00:22:32,000 --> 00:22:37,000
And so you start to do the hard work of changing that.

250
00:22:37,000 --> 00:22:40,000
That's what we do in meditation.

251
00:22:40,000 --> 00:22:43,000
So, and that is the highest victory.

252
00:22:43,000 --> 00:22:46,000
It's a victory that no one can take from you.

253
00:22:46,000 --> 00:22:48,000
That's the point of this verse.

254
00:22:48,000 --> 00:22:52,000
Sorry, well said, as are all the things the Buddha said.

255
00:22:52,000 --> 00:22:58,000
So, something that we should appreciate and remember, give us confidence that what we're

256
00:22:58,000 --> 00:23:00,000
aiming for is not something simple.

257
00:23:00,000 --> 00:23:07,000
It's not just a hobby that you can pick up and put down something that really and truly

258
00:23:07,000 --> 00:23:15,000
changes your life, transforms your way, wakes you up, and helps you rise above your troubles.

259
00:23:15,000 --> 00:23:22,000
So, that even though the situation may not change, no matter what the situation is, you are above

260
00:23:22,000 --> 00:23:24,000
it, you are beyond it.

261
00:23:24,000 --> 00:23:31,000
So, that's the teaching for tonight versus 104 and 105.

262
00:23:31,000 --> 00:23:34,000
We're almost a quarter of the way through the Dhamapanda.

263
00:23:34,000 --> 00:23:39,000
I think when we get to 108, that'll be one quarter of the way.

264
00:23:39,000 --> 00:23:40,000
So, thank everyone.

265
00:23:40,000 --> 00:23:42,000
Thank you all for tuning in.

266
00:23:42,000 --> 00:23:47,000
I'm wishing you all the best on your path to invincibility.

267
00:23:47,000 --> 00:23:52,000
Be well.

268
00:23:52,000 --> 00:24:17,000
I'm still there, Robin.

269
00:24:17,000 --> 00:24:22,000
This post publicly.

270
00:24:22,000 --> 00:24:26,000
It wasn't live until then.

271
00:24:26,000 --> 00:24:31,000
What happened there?

272
00:24:31,000 --> 00:24:36,000
Google.

273
00:24:36,000 --> 00:24:39,000
Sorry.

274
00:24:39,000 --> 00:24:47,000
What did you say?

275
00:24:47,000 --> 00:24:52,000
No, the hangout didn't broadcast.

276
00:24:52,000 --> 00:25:00,000
There was a new button that says post publicly or something.

277
00:25:00,000 --> 00:25:08,000
I'm not going to move the camera, so this is the man facing.

278
00:25:08,000 --> 00:25:11,000
Look.

279
00:25:11,000 --> 00:25:15,000
Computers over there.

280
00:25:15,000 --> 00:25:16,000
All right.

281
00:25:16,000 --> 00:25:37,000
So, we have questions.

282
00:25:37,000 --> 00:25:41,000
Thank you.

283
00:25:41,000 --> 00:25:45,000
Well, it's good to get feedback.

284
00:25:45,000 --> 00:25:46,000
It's funny.

285
00:25:46,000 --> 00:25:51,000
I mean, I probably could never have done this without meditation.

286
00:25:51,000 --> 00:25:55,000
I think that took me, I wrote that in one sitting.

287
00:25:55,000 --> 00:25:58,000
I've written almost all the chapters in one sitting.

288
00:25:58,000 --> 00:26:01,000
I mean, there's editing that goes into it.

289
00:26:01,000 --> 00:26:04,000
But it really comes a lot easier.

290
00:26:04,000 --> 00:26:09,000
I guess also because it's the dumb one, but writing things,

291
00:26:09,000 --> 00:26:14,000
studying, using the mind is a lot easier once you practice meditation.

292
00:26:14,000 --> 00:26:23,000
I shouldn't brag like that, but it is kind of neat that you can just write stuff.

293
00:26:23,000 --> 00:26:26,000
I don't think that's uncommon.

294
00:26:26,000 --> 00:26:43,000
People who meditate their mind works better.

295
00:26:43,000 --> 00:26:45,000
Oh, sorry.

296
00:26:45,000 --> 00:26:48,000
How old is she?

297
00:26:48,000 --> 00:26:53,000
14.

298
00:26:53,000 --> 00:27:01,000
Well, I'd recommend potentially how to meditate for children.

299
00:27:01,000 --> 00:27:08,000
And you can warn her that it's actually designed maybe for younger children, but she can choose.

300
00:27:08,000 --> 00:27:10,000
I would offer her both.

301
00:27:10,000 --> 00:27:13,000
Because adults have found the kids videos useful.

302
00:27:13,000 --> 00:27:14,000
They're quite simple.

303
00:27:14,000 --> 00:27:19,000
So I'd recommend my videos on how to meditate for kids.

304
00:27:19,000 --> 00:27:25,000
But certainly, she could also use the book.

305
00:27:25,000 --> 00:27:31,000
I mean, the meditations for kids aren't exclusively in sight meditation.

306
00:27:31,000 --> 00:27:41,000
So it may, it's more to the point to give her the actual booklet.

307
00:27:41,000 --> 00:27:44,000
I don't think it's difficult to understand.

308
00:27:44,000 --> 00:27:46,000
But you can also go through it with her.

309
00:27:46,000 --> 00:28:09,000
As long as you're following the book, you can teach her to explain to her how to practice.

310
00:28:09,000 --> 00:28:24,000
Sorry, better to practice compassion than made that passion than love.

311
00:28:24,000 --> 00:28:26,000
It really depends, actually.

312
00:28:26,000 --> 00:28:34,000
No, actually, compassion as we're learning from the VCD manga is best used when you have cruelty in your mind.

313
00:28:34,000 --> 00:28:42,000
So I mean, certainly, you can feel cruel towards an enemy.

314
00:28:42,000 --> 00:28:45,000
But more often, I would say you feel anger.

315
00:28:45,000 --> 00:28:52,000
Cruelty, you might feel towards someone who is suffering.

316
00:28:52,000 --> 00:28:56,000
Or someone who is under your care, that kind of thing.

317
00:28:56,000 --> 00:29:02,000
So helping them is out of compassion.

318
00:29:02,000 --> 00:29:07,000
Like, I mean, just to give an example as a teacher, why?

319
00:29:07,000 --> 00:29:09,000
Because it's common.

320
00:29:09,000 --> 00:29:11,000
It's referred to talked about in Buddhism.

321
00:29:11,000 --> 00:29:17,000
Not teaching when someone needs, and someone could use your teaching.

322
00:29:17,000 --> 00:29:43,000
So the Buddha had great compassion, because he went out of his way to teach people who needed it.

323
00:29:43,000 --> 00:29:55,000
Sorry, say a good.

324
00:29:55,000 --> 00:29:57,000
Slows down the evolution.

325
00:29:57,000 --> 00:30:00,000
I mean, honestly, I wouldn't say either of those things.

326
00:30:00,000 --> 00:30:05,000
So I'm really not quite sure where you're going with that.

327
00:30:05,000 --> 00:30:07,000
I don't understand the evolution.

328
00:30:07,000 --> 00:30:12,000
Slows down the evolution of our rampant minds.

329
00:30:12,000 --> 00:30:18,000
I mean, dude, read my book, teaches you how to be mindful.

330
00:30:18,000 --> 00:30:23,000
You'll overcome suffering.

331
00:30:23,000 --> 00:30:26,000
I guess, OK, if I'm going to talk about slowing down the mind,

332
00:30:26,000 --> 00:30:28,000
let's forget about the evolution of the rampant.

333
00:30:28,000 --> 00:30:31,000
I don't quite understand the context.

334
00:30:31,000 --> 00:30:39,000
But slowing down or guiding, I would say, it does both.

335
00:30:39,000 --> 00:30:42,000
Yes, absolutely.

336
00:30:42,000 --> 00:30:43,000
But it doesn't naturally.

337
00:30:43,000 --> 00:30:47,000
You see, I mean, it's not either one of those things.

338
00:30:47,000 --> 00:30:52,000
Our goal, our immediate goal and the practice,

339
00:30:52,000 --> 00:30:57,000
our goal is to see clearly the look, so that we can see and understand.

340
00:30:57,000 --> 00:31:01,000
And that guides and slows the mind naturally.

341
00:31:01,000 --> 00:31:07,000
Generally, because generally our minds tend to go faster than they should.

342
00:31:07,000 --> 00:31:12,000
It also can speed the mind up, make the mind quicker, less lethargic, less lazy.

343
00:31:12,000 --> 00:31:17,000
It depends on the person.

344
00:31:27,000 --> 00:31:32,000
Too hard to continue.

345
00:31:32,000 --> 00:31:38,000
You should look at why it's hard.

346
00:31:38,000 --> 00:31:42,000
I mean, sitting still is actually not that hard to do, surprisingly enough.

347
00:31:42,000 --> 00:31:46,000
Sitting still is not intrinsically a difficult thing.

348
00:31:46,000 --> 00:31:48,000
That's not surprising at all.

349
00:31:48,000 --> 00:31:51,000
Walking back and forth, also not all that difficult,

350
00:31:51,000 --> 00:31:54,000
unless you've only got one leg or no legs.

351
00:31:54,000 --> 00:31:56,000
It's a pretty simple thing to do.

352
00:31:56,000 --> 00:32:01,000
I mean, I had one woman who had multiple sclerosis.

353
00:32:01,000 --> 00:32:07,000
And she would do walking by laying herself on her walker

354
00:32:07,000 --> 00:32:13,000
and paddling with her legs, like basically pushing against the floor with her legs.

355
00:32:13,000 --> 00:32:16,000
She could move her legs, but just barely.

356
00:32:16,000 --> 00:32:18,000
So don't talk about, to me, about heart.

357
00:32:18,000 --> 00:32:20,000
I mean, she had it hard.

358
00:32:20,000 --> 00:32:25,000
But what you're referring is, too, is not the fact that it's hard to do the meditation.

359
00:32:25,000 --> 00:32:29,000
Not that the meditation is hard, but that your mind has something.

360
00:32:29,000 --> 00:32:37,000
That finds the meditation repellent or a verse to the meditation.

361
00:32:37,000 --> 00:32:39,000
And so you have to figure out what that is.

362
00:32:39,000 --> 00:32:44,000
It means you've got something in your mind that dislikes what you're doing.

363
00:32:44,000 --> 00:32:46,000
Or that wants something different.

364
00:32:46,000 --> 00:32:48,000
And there can be many reasons for that.

365
00:32:48,000 --> 00:32:51,000
It's often because of our addiction to sensuality.

366
00:32:51,000 --> 00:32:54,000
We want to be doing something else.

367
00:32:54,000 --> 00:32:59,000
It can be due to our aversion to pain when we have to.

368
00:32:59,000 --> 00:33:02,000
It can be a sense of boredom.

369
00:33:02,000 --> 00:33:04,000
I mean, they're actually all quite related.

370
00:33:04,000 --> 00:33:07,000
But how it appears to you.

371
00:33:07,000 --> 00:33:12,000
You have to figure out how it appears to you and deal with it according to that.

372
00:33:12,000 --> 00:33:16,000
Noting those things, breaking them up into their individual parts.

373
00:33:16,000 --> 00:33:30,000
And noting.

374
00:33:30,000 --> 00:33:31,000
It depends who you ask.

375
00:33:31,000 --> 00:33:33,000
In our tradition, the noting is necessary.

376
00:33:33,000 --> 00:33:35,000
So if you don't do it, you're not really meditating.

377
00:33:35,000 --> 00:33:36,000
It's not that you're not mindful.

378
00:33:36,000 --> 00:33:42,000
Mindfulness is just a part of a tricky concept.

379
00:33:42,000 --> 00:33:44,000
Mindfulness can mean many different things.

380
00:33:44,000 --> 00:33:46,000
But we would say rather than you're not meditating.

381
00:33:46,000 --> 00:33:48,000
If you're not noting, you're not really meditating.

382
00:33:48,000 --> 00:33:50,000
You're just sitting there.

383
00:33:50,000 --> 00:33:53,000
And so it's hidden in me as to whether you'll do your progress.

384
00:33:53,000 --> 00:33:56,000
From our point of view, I know that's highly contentious.

385
00:33:56,000 --> 00:34:16,000
But that's how it goes with us.

386
00:34:16,000 --> 00:34:21,000
It's hard, you know, because I get lots of emails.

387
00:34:21,000 --> 00:34:28,000
And I've kind of, you know, just in the end, you have to cut some.

388
00:34:28,000 --> 00:34:33,000
You have to at some point say, there's the limit.

389
00:34:33,000 --> 00:34:39,000
So I'm I'm I'm happy to talk over the phone.

390
00:34:39,000 --> 00:34:47,000
But only if it's related to your meditation practice.

391
00:34:47,000 --> 00:34:50,000
There's lots of questions you might have to ask me.

392
00:34:50,000 --> 00:34:57,000
So your questions are about, but there's not much I can do to solve your problems.

393
00:34:57,000 --> 00:35:02,000
If you want to learn how to meditate and how to practice meditation,

394
00:35:02,000 --> 00:35:09,000
that can certainly help your practice, help your life, your problem.

395
00:35:09,000 --> 00:35:12,000
So I'd recommend that reading my booklet.

396
00:35:12,000 --> 00:35:17,000
And then if you're interested in if you have questions about the meditation,

397
00:35:17,000 --> 00:35:25,000
problems with the meditation, then I mean, those are the kind of things you should be able to ask on here.

398
00:35:25,000 --> 00:35:29,000
In fact, everyone is probably not probably maybe a surprise,

399
00:35:29,000 --> 00:35:32,000
but for me to say this, and maybe not even very welcome to say it,

400
00:35:32,000 --> 00:35:46,000
but really anything you can't ask on here is not a question that is really necessary to ask.

401
00:35:46,000 --> 00:35:50,000
Or maybe not even necessary, but it's just it probably falls too close,

402
00:35:50,000 --> 00:35:56,000
too far into the realm of that home life for me to even really see,

403
00:35:56,000 --> 00:36:02,000
because then I have to give you an answer about your life,

404
00:36:02,000 --> 00:36:06,000
which has so many variables that I don't know and is so complicated,

405
00:36:06,000 --> 00:36:10,000
that it ends up not really being the problem and the problems of just being

406
00:36:10,000 --> 00:36:14,000
states of mind that can be dealt with in meditation.

407
00:36:14,000 --> 00:36:19,000
So at the end, it just does come back to questions about meditation.

408
00:36:19,000 --> 00:36:22,000
So that's what I'd recommend, learn how to meditate,

409
00:36:22,000 --> 00:36:26,000
and then start asking the right question.

410
00:36:26,000 --> 00:36:32,000
Probably not a very popular answer, but it really does come down to that.

411
00:36:32,000 --> 00:36:36,000
If having heard that, you still feel that you need some private time,

412
00:36:36,000 --> 00:36:38,000
then I'm sure we can arrange it.

413
00:36:38,000 --> 00:36:41,000
In fact, a lot of meditation questions are private,

414
00:36:41,000 --> 00:36:44,000
but that's why we're doing these courses.

415
00:36:44,000 --> 00:36:47,000
Again, it's about prioritizing.

416
00:36:47,000 --> 00:36:51,000
So I have 28 slots per week.

417
00:36:51,000 --> 00:36:54,000
I mean, it was 28 different people right now.

418
00:36:54,000 --> 00:36:57,000
It's about 24.

419
00:36:57,000 --> 00:37:00,000
And those are people who are doing two hours,

420
00:37:00,000 --> 00:37:05,000
or one at least two hours, eventually up to two hours,

421
00:37:05,000 --> 00:37:08,000
at least a meditation a day.

422
00:37:08,000 --> 00:37:12,000
And that keeps me pretty busy.

423
00:37:12,000 --> 00:37:16,000
So then I come on here to answer general questions,

424
00:37:16,000 --> 00:37:19,000
or people who aren't able to do those courses.

425
00:37:19,000 --> 00:37:24,000
Anyway, I hope that clarifies my stance.

426
00:37:24,000 --> 00:37:28,000
If you can get through to me, then I won't turn down a phone call,

427
00:37:28,000 --> 00:37:49,000
but I'll leave it up to you to figure out where to go next.

428
00:37:49,000 --> 00:37:56,000
Okay, then let's run before they catch up.

429
00:37:56,000 --> 00:37:58,000
Thank you, Robin, for your help.

430
00:37:58,000 --> 00:38:00,000
And thanks everyone for tuning in.

431
00:38:00,000 --> 00:38:27,000
Have a good day.

