Hello everyone, we're here on Saturday afternoon, I'm here today with Chris as usual,
and we have a couple of moderators as well, just to make sure that people stay wholesome
and that we stay on track.
We have some rules, so once we start into the questions period, you can only add questions
to the chat, no chatter, no answers, but to start out with, I'm going to start putting a little
more focus on the dumber portion at the beginning, we're asking for dumber talks, but they're
also asking for me to show my face, it's as though I'm hiding, as I've said in the Buddhist
time they would purposefully put a fan in front of their face to hide themselves, not
because they were afraid, although maybe somewhere when you first start teaching it can
be scary, but that's also useful because it takes you out of the picture that it helps
you remember, helps the audience remember, it's not about you, you know, you look at
this young monk giving a talk, maybe he just ordained or she just ordained and you wonder
what you could possibly learn from such a person, so you make it personal, when you look
at me, people say, oh, he's not even Asian, how can he be Buddhist, how can he be a monk,
how can he be a teacher, or he's young, or he's white, or whatever, and while it is valid
to critique people and question their behavior and their qualifications, it doesn't
add or take away from the message, and in fact it can mislead you if I was an old Asian
person with a beard, you might immediately think I was more qualified to teach, well maybe
you're not with a beard, you know, some old, wise and teacher.
We respect authority, we respect appearances, and they can mislead us, there's a story
of Tuchapotila who became enlightened because of the teachings of a seven-year-old novice.
So even a new monk or even a not someone who's not a monk, you should take them out
of the picture, you shouldn't let prejudice blind you to the truth or the falsehood of their
teachings.
You shouldn't just because someone's a famous teacher, just because they're a charismatic
teacher, just because they have a soft, deep, soothing voice, just because they're rhetoric,
so many things that can mislead you.
Anyway, so one of them is the appearance, but more importantly is that we're reminding ourselves
and reminding each other that it's not our teachings, that's not about us, it's about
the truth, and that we focus on the truth.
When you listen to a dumbah dog, you should close your eyes, try your best to make it a practical
experience.
Try your best to gain more than just the knowledge, more than just the words, more than
just the memory of having listened to the dumbah.
Today's topic is wisdom, and this morning, in our study group, we were studying the
Allah-gadupa Masuta, Allah-gada is a snake, so we talked about, about a simile of a person
who grabs a snake, or if you've ever grabbed a poisonous snake or learned about grabbing
poisonous snakes, it's not dangerous as long as you grab them in the right place, you
grab them wrong, the child grabs a snake, they're most likely in trouble.
The Buddha said, same with his teaching, like in his teaching, to a poisonous snake, there's
the thing evil about a poisonous snake, a snake per se, nothing, no supposed, it's intentions
to kill and eat other beings as though it is evil, but now the thing about a poisonous snake
is it's powerful, there's a real power to the poison, so the Buddha's teaching is
like a snake that has venom in it, you could like, you could compare it to a teaching that
doesn't have any venom in it meaning, doesn't have the power to kill, doesn't have the
power to destroy evil, the Buddha's teaching is potent, it's potent but with anything
as with anything that's powerful, in fact putting aside the Buddha's teaching, just talking
about the sort of teaching that the Buddha taught, talking about meditation, about mental
development, mental development in general is dangerous, it's dangerous because it's so
powerful, anything that deals with the mind is powerful, so if you're wrongly grasped
the Buddha's teaching you can hurt yourself, you can hurt others, most importantly what
someone remarked this morning that because it's so pure and so good and so wholesome,
it has a special power to harm people who grasp it wrongly, if people learn it or study
it for purpose of criticizing it, they use it for the purpose of manipulating others, there's
a relationship, a direct relationship between the purity of something and the power of
good or bad relations to it, so if you support the Buddha's teaching, if you say good
things about it, if you practice it rightly, if you share it with others, if you protect
it, all of these things are very powerful because of the purity of it and by the same
token, if you corrupt the Buddha's teaching, pervert it, if you denounce it or lie about
it, destroy it, harm it, let's see I'll add up a message, but one of the things that
it talks about is wisdom, talks about a person who grasps the dot teaching rightly, what
does it mean to grasp the teaching rightly, he said, it's a person who gives rise to wisdom
for whom the teaching creates wisdom, where it leads to wisdom for that person, this
is the topic today I wanted to talk a little bit about what that meant because there
were some issues I had with the translation and just talking about the difference between
intellectual study and meditation, realizations and meditation, Buddhism is a religion
about wisdom at the top, at the pinnacle of Buddhism is not God, it's not faith, it's
not magic, the pinnacle of the teaching is wisdom, it's the pinnacle of the training, it's
the key to freedom from suffering, it's the means by which a person becomes pure, but it
said, banyaya pary sujity, one becomes purified through wisdom, it's not an obvious statement
to me, quite often we hear things like purity comes through good intentions and what seems
more obvious is purity and coming through kindness, generosity, caring, love, love is all
you need, that's what would make you a good person, but in fact none of those things have
the power to purify the mind because they rely on the base views and assumptions of
the individual kindness, generosity, all of these things rely on vision, they rely on
purity to appear, you can't be truly kind if your perception is warped, you can't be
generous because your activities are going to be warped by the views and the opinions
and the perceptions of conceit and craving and views, these things aren't strong enough
to overcome those, they don't have any relationship with one's views and underlying assumptions
and perceptions, so what happens is we become partial instead of loving or kind, it could
become egotistical and attached to our kindness and our generosity, they don't have the
power to overcome those, only wisdom is at the root because it leads to clarity, it leads
to the deepest sort of purity whereby one is unable to harm another, unable to pervert
one's perception, just by its very definition, wisdom, meaning understanding, it implies
directly seeing things as they are and there's no, there can be no perversion of one's
intentions, if you intend to be kind, if you have wisdom you can be kind, there'd be no
perversion involved, meaning there's no straying from that, there can be no conceit or arrogance
because by its definition wisdom has seen through that, it's understood those.
The important thing about wisdom is to understand that it is not, it is not simply the
learning of good things or wise things, the Buddha talked about three types of wisdom,
I said, sutta mayapanya is wisdom that comes from study, sutta means hearing and so it
is a kind of wisdom, you could say that there is wisdom in learning other people's wisdom,
if you learned all of the Buddha's teaching, I would say you have a lot of wisdom but
it's not really accurate, it's misleading to say that, it's true, how do you have a lot
of wisdom while you memorize it, so you have it, it's yours, but it's not really yours.
You could buy the same token, say a parrot, if a parrot could memorize the Buddhist teaching,
it would have a lot of wisdom, it would have it, but the Buddha said this is like a cow
herd in India, they had lots of cows and one person would look after someone else's cows
and they might get some money or some payment, but they would never get to taste the milk
from the cow, they would never get the fruit of the labor, a person who memorizes the
Buddhist teaching, maybe teaches it, this is like a cow herd, not like the owner of the
cow, so they don't actually own the damma for themselves, and they might be able
to teach it to others, they might even become quite famous, but they'll never gain the
true benefit, the deeper benefit of the damma, but something we were talking about this
morning is how important this type of knowledge is, and I even said this is the one thing
that you should never abandon, it's one thing the translation seemed to imply that you
should abandon, you should let go of the teachings, and I think that is not proper, it's
the one thing you should never let go of, let's put it this way, if something is right
and true, then no good could ever come of letting go of that, if it's true.
So this kind of teaching is the one thing that you should always hold on to, whatever is
true and right that you should hold on to, of course, knowing what is true and right is
a bit of a problem, and for those of you who are not Buddhists and don't have some kind
of ulterior, exterior sort of faith in the Buddha might not be very palatable to suggest
that you should hold on to the Buddha's teaching, but nonetheless, good comes from holding
on to good things, so let's put it this way, when you see that there is benefit from
the teaching, once you have put it into practice, hold on to those teachings, because
practicing them again and again, and remembering them at all times will never fail you.
It's the one thing I think you can hold on to, and the one thing you really can't do without
without it, you've closed the door, without either hearing the teaching from someone else
or spending, however many countless lifetimes it takes to figure it all out on your own,
and to get to the point where you can figure it out all on your own, so the only two
ways to open the door to the practice, the second type of wisdom is jintamya panya, jintamim's
thinking, so sutamya panya can run you into trouble quite easily, you become egotistical
about it, teachers who know a lot, Buddhists who have studied a lot know everything but
know nothing, it can actually close the door if you become complacent, it's like you
stand there praising the door, you've got this nice house and you stand outside and talk
all about the house and what it's like and you never go inside, maybe like a realtor
who can show the house to others but never has a chance to live there, but jintamya
panya, I think is a big problem with educated people, people who have some degree of
higher education, I like to think about things, they like to doubt and debate things and
so it's easy to misunderstand that somehow thinking about the dhamma is going to have
a positive effect, this idea that after you've studied the dhamma you should spend some
time thinking about it until it makes sense to you.
The problem I see with that and I've seen quite often is the sense that you make out of it
much of the time is wrong, it is based on your own existing biases, if we're here to admit
and to work to overcome our biases and our misperceptions and so on, then we can't possibly
expect ourselves to really appreciate and understand the truth until we've seen it for
ourselves.
So taking the Buddha's teaching and studying them and analyzing them and debating them and
thinking about them, it comes up with something but as many times as not can be something
outside of the Buddha's teaching, a wrong grasp of the teaching and people become quite
attached to this knowledge.
Another example is when meditating, I hear about meditators saying they've had enlightenment
experiences, they had realizations, quite often my students will tell me they had a
realization.
And so in the discourse on the simile of the snake, the Buddha also has a simile of a
raft and he says, imagine a person who comes to a river wandering through the jungle and
they realize they can't cross that river without some help so they build a raft and
they spend time and energy building this raft and it's a really nice raft so they get
on the raft and they paddle them their way across the river and they're quite happy
with that fact and they look at that raft and they say, wow, this raft was really useful
for me and so they say, what if I were to pick this raft up?
I'll carry it on my back and continue on my way with this very useful raft that was so
helpful for me.
So they pick up the raft, put it on their back and go on their way.
The Buddha said, would this person be wise?
No hate on Monday, no indeed vendor was there.
What should the man do?
He should think, oh, this raft was very useful but I don't need it anymore.
I'll go on my way.
It's a good simile to remember when things arise in meditation, there are many good things
that arise but any time they arise we have to immediately let them go.
The benefit of them arising is distinct from any benefit that might come from us clinging
to them.
There's no benefit from clinging to them.
That benefit that they gave us was already arisen.
In fact, they are the benefit and to cling to them would be to lose our way.
So any thoughts, any realizations that come up as thoughts, this is why I don't use the
word Ripasana.
I don't use the word insight to translate Ripasana.
Ripasana means to see clearly, it's often translated as insight but the word insight in English
is misleading.
We often think of meditation as teaching you things.
You're going to realize things, it's going to be an intellectual realization.
In fact, more like a polishing of a lens, you're just going to see more clearly and there
will be realizations but those aren't the path, those aren't the practice.
This is polishing until you can see and when you see, of course, you're going to see lots
of things.
Those are jintamayapanya, the thinking about it, when you think about what you see.
The third type of wisdom is called bhavana mayapanya and that's what we try to gain out
of the practice.
That's the real benefit.
So what I can give you today is sutamayapanya and probably jintamayapanya.
So what jintamayapanya really all you need is an understanding of the meaning.
What jintamayapanya is wisdom really means.
It's once you gain some knowledge.
You should make sure you understand it.
Make sure you understand what's being said.
So if I tell you to do walking meditation, you have to understand that I'm saying, actually,
do walking meditation.
Not just, oh yeah, now I know how to do walking meditation, wow.
One more thing I know, and that happens.
People might listen to a talk on the dhamma or meditation and think, wow, now I know how
to do walking meditation or sitting meditation.
Now I know all about mindfulness, boy, I feel enlightened already.
You can't do that, but whatever you get from today, from the questions, oh, we have lots
of questions, whatever you get.
Make sure it leads you to seeing clearly, not seeing clearly intellectually, but seeing clearly
about your own experience and your own physical and mental reality.
All right, just some thoughts on wisdom.
And with that, we will move on to the Q&A portion.
So from now on, no more comments in chat, please, I'll ask the moderators to delete them.
Only questions and please, we're going to try to only answer questions related to meditation,
especially focused on questions that you need answers to, looking for people who need
our help, not curious or speculative theoretical, I want to give rise to bhavana mayapanya.
If you don't have questions or if you've already asked your question, just close your eyes
like me and we can sit here and meditate together and you can hear my voice and note hearing.
Okay, let's begin.
Sometimes when I notice and note hearing, I almost automatically subsequently open my hearing
awareness to whatever sounds are there and then note them, is this incorrect practice?
It's an old habit.
There's not really incorrect practice like that.
Whatever happens becomes an object of meditation.
So when you're hearing, you note hearing and then if you start to hear something else
or if you notice your mind curious about what you're hearing or so on, you can note that
as well, not just hearing, but you can note knowing, knowing that your mind is doing that.
Let's see, so it's not about incorrect or correct because it's not you doing it.
It's just habit.
Habit isn't you, it isn't yours.
Should you focus on helping yourself through meditation instead of using your time to help
others?
That's really no difference, a person who helps themselves, helps others.
Meditation is about purifying the mind.
A pure mind is much more helpful to oneself and others than an impure mind and back.
It's the only thing that's helpful and impure mind will never be helpful.
For the more purity you can bring to your mind, the more helpful you'll be to yourself
and others.
There's no difference.
In fact, the more pure you become, the less difference there is, the less selfish you
become, the more purely helpful you become.
Feeling cold after meditation, should I do anything?
You should not cold, cold, that's all.
If you don't like it or you're worried about it, should not that as well.
How to deal with relations when people get unintentionally irritated or offended from
your behavior to continue us to misunderstandings with others are stressful.
I'll try not to be around people who are easily irritated, it's not always possible,
but it is a wake up call to us that we're quite often fallen with people who are not of
like mind and of all the people you could surround yourself with for their benefit, for
your benefit, for the benefit of the world, your best to surround yourself with people
who are like-minded, best to surround yourself with people who are going to support you
in your practice and who has practiced you can support.
Because of course then all of you will work together to help the rest of the world.
Apart from that, you can try to be sensitive and careful, now it's quite possible that
you are behaving in such a way that is improper, so you have to practice being mindful
and you have to realize when you're doing things that harm others or make others upset.
You have to be willing to change, we all have to be willing to admit our mistakes and
none of us are perfect, and we have to work to better ourselves, but finally you can't
stop other people from getting irritated and isn't true that you're always at fault
when other people get upset with you.
I'd say a good default position to take is that you probably did something wrong, but
after examining carefully to see what it was that you did wrong, if you can't find anything
then either you're just blind or else, it may be possible that they are just- without
merit.
Of course getting upset is never a good thing, anyone who gets upset, first thing to
notice that this is a person who is- who is upsetable, volatile, and it may not be in your
best interest or theirs for you to spend too much time with them.
People who are like that really need to spend time with themselves, better themselves.
With that being said, we all have issues and even in meditation centers, people fight and
can agree at each other, it happens in monasteries, meditation centers.
So we shouldn't judge people for it, we should just give them the space they need to better
themselves.
In meditation, sometimes I feel like I'm trying to change things, often catch myself trying
to force equanimity, even in the tone of voice when noting in my head, I note knowing
any other advice.
No, that's good, I mean you can note if it upsets you're a song, disliking, frustrated
if it frustrates you, but that's fine.
See, it's just seeing because what you're seeing is that it's not actually you, you're
not actually trying to force or change things, and that's what you're going to realize
eventually is that the fact that it's coming back again and again, even though you're
not intending for that to happen, means that you're not forcing things at all.
See, so it's just a whole big mess of delusion, and that's all going to clear up as
you practice.
Is it okay to note in English, even though it is not my first language, and to use my
first language for words I don't know in English, I ask because I feel that English better
fits the experience.
I would probably recommend using one language either or, because if you can find the words
in your language you can help people in your country, maybe that someone else already
has, probably you can find person who can help you with the words, which words aren't
really that important.
Why I say use one language is because it's less distracting potentially.
Sometimes when I am focusing on the stomach and become aware of thinking, before I can
go away from stomach to note thinking, I am pulled back to the stomach.
Should I still note thinking?
Well, you should notice soon as you become aware of thinking, so it shouldn't require you
to go away from the stomach, you've already gone away from the stomach to be just to think.
So that'll just get easier as you go.
In the beginning you kind of have to fudge it, do what you can, it's not, I can't give answers
like this, and answers like this aren't really the point, the point is to do what you
can, and I'll note something, in the beginning it's a bit chaotic, it's a bit messy.
Don't try and expect for it to be somehow clear cut, note whatever you can in the present
moment, but in the long term there shouldn't be a problem when you're thinking, it's
just not thinking.
Do I have to note the disliking of an object to get the benefit of becoming dispassionate
about said liking, should I give liking and disliking some priority versus discomfort and tension?
Sometimes both seem to arise at once.
Just note whatever is clearest, again it doesn't really matter what, which just by being
clear-minded, clearly aware of something, you're cultivating this state of clarity, this habit,
I'm seeing things as they are, and that will get stronger and you'll be able to apply
it to more and more and more things.
Are there any tips on dealing with a version, especially the subtle ones, toward emotions?
Even when I'm trying to just accept my emotions, it always feels like it is only to get rid
of it.
You shouldn't be trying to do anything, you see, note when you are trying, note you have
to note the hat as well, when you want to accept them, say one thing, when you're frustrated
by the fact that you're not accepting.
Again, it's pretty chaotic in the beginning, you're going to be messed up in this way,
trying to control and then trying to not try to control, catch which again, try and keep
it simple, note absolutely everything.
When hearing others speak, my inner monologue seems to repeat what they are saying in
my head.
Because of this, I am having trouble noting hearing, how should I persevere?
If you hear it in your hand, that's also hearing, but you can note knowing, or if there's
any reaction to it, again, these kind of things are habits that mostly will go away through
actual, intensive and systematic and repetitive practice, you'll develop new habits of
interacting that are simpler and less distracted, less diffuse.
I should always persevere through patience.
How can one note disjointed, fragmented, incomplete thoughts?
I always note thinking, but sometimes it feels lazy to note it as such and seems to prompt
more similar thoughts.
So it won't prompt more similar thoughts, it's just that saying thinking doesn't stop
you from thinking, that's not the point, the point is that thinking makes you see it
just as thinking.
So when it comes again, you're going to see it as thinking and you'll be more clear about
that.
So if it comes again, say it again, thinking, the point is, don't misunderstand what the
note is for, it's not going to stop things, it's just going to purify the perception of them.
If while meditating, you note, clinging to something, how would this stop the clinging?
You could observe the same clinging over and over again, again, the same thing, the
point is not to stop the clinging, it's to see the clinging clearly.
The point is that when you see things clearly, by very definition of clearly means without
any inconsistency in the sense that you do things that are against your best interest,
you never would.
You would intentionally engage in some activity or some mind, even some mental activity.
That was not in your best interest.
The reason we do so is through lack of clarity, through a perverse mind state.
And by perverse, it just means something that is inconsistent.
Once one thing does something else, does something that's contrary to your best interest,
it's contrary to your ambitions or your goals?
So, clinging, we cling because of lack of clarity, because somehow we think that the clingies
to our benefit, if you see it enough, you'll start to see, what is it like to cling
to things, how it actually causes stress and suffering, and it's through seeing that,
that the clinging ceases, because again, we won't do things that are not in our best interest
when we know that they're not, and this highlights the difference between intellectual
understanding and actual wisdom, seeing clearly, because it's not enough to know intellectually
because you've heard the Buddha say or because you've thought it out yourself, or even
because you realized that some time ago about something else, you really do have to see
clearly and have a clear mind in order to see that nothing's worth clinging to.
That's what happens.
Sabe-dhamma-nalang-a-bhi-nui-saya, and see that indeed nothing is worth clinging to.
In my practice, I have noticed some things.
Some more prevalent feelings are fear, anxiety, obsession, and compulsion, and diagnosed
with OCD.
Sometimes I also notice impermanence, but only intellectually.
What should I do about these feelings aside from noting?
So we're not trying to do anything about them, this is the point.
Do implies that you're going to somehow change them.
You shouldn't do anything about them except note, which stops you from that, stops you
from trying to react to them, or engage with them, or get involved with them, act according
to them, noting just as the default state, it's the doing nothing option, it's the
keeping the mind free from engagement, and that frees you to do what's more proper and
more right, because you're seeing clearly, and you're able to see the way the nature
of things, the way forward would have been official, would have been harmful and so on.
I noted, I feel not good if I don't do enough meditation.
What should I do?
What should I let go?
Maybe this is also seeing uncontrollability.
Well, you should do enough meditation, I think is the answer.
It's kind of good that you can see that when you don't do enough, you don't feel so
good, but of course you should note that, the feelings, ultimately, though, yeah, do
enough.
After almost a year straight of daily meditation, I've found myself becoming distracted
and meditating inconsistently.
Are there meditations recommended practices to deal with this?
There are supported meditations, but meditation isn't the only thing that's supportive,
getting involved with communities, having a meditation group, visiting a monastery, living
at a monastery, that sort of thing, living at a meditation center, also doing good deeds,
being kind and generous to others is a good way to keep your mind vibrant and healthy and
energetic.
But there are some meditations that will help meditation on death, meditation on the Buddha.
Those ones are both good for giving confidence and energy.
When someone has tinnitus like myself, how should you note this, hearing or thought?
Hearing.
And also note the disliking of it or that sort of thing.
For a distracted or obsessive mind, it takes quite some effort to get it to stabilize
on the object, what is the relationship between restraint of mind wandering, mindfulness
and unification of mind?
Mindfulness, mindfulness restraints, that's the point.
You shouldn't try to restrain any other way, you should restrain with mindfulness.
Yeah, Nisotani, Lokas, Mingsat, Taites and Niwariang, Satyas, that which were strains or forbids or stops,
all of the distractions, all of the streams of consciousness that make the mind diffuse.
How does one know when one is doing meditation right and how does one know they're doing
it wrong?
Well, in a momentary basis, you know because your mind is clear.
When you're doing it right, your mind is clear, it's present.
It sees things as they are, it's not judging or liking or disliking.
In the long term, you know because you're gaining wisdom, you're gaining focus, and
you're reducing your greed, anger, and delusion.
That's basically the answer.
How do you know you're doing it wrong?
It's very hard to know if you're doing it wrong.
That's why it's good to have a teacher, a qualified teacher who can point out when you're
doing it wrong because if you're doing it wrong, your mind does not, purer is not clear.
So it'd be very hard to know, you wouldn't know, you'd be too preoccupied doing whatever
it was you were doing wrong.
You'll know when you usually know when something happens that you just can't deal with,
and you just stop meditating, this is what often happens.
Or you go crazy, it's rare, but sometimes people just keep pushing, pushing, pushing until
they go crazy, and it's temporary, it's a temporary thing, but it can happen when people
practice on their own, or when the teacher is not qualified, and that sort of thing.
It doesn't really happen in this tradition so much, and it's still can, if there's not
a good teacher, the teacher can cause problems.
The more time I spend unmindful, the more difficult it is for me to be mindful, especially
throughout the day, are unmindful moments due to lack of effort.
I can be due to lots of things, they're due to the hindrances.
You can't just say lack of effort, they're just due to bad habits, all sorts of bad
habits, which involve lots of bad stuff.
Ultimately it just means try to be mindful, when it's difficult, you can even note that.
Of course it's difficult because it's not a habit where familiar with or the habit that
we've developed, it's a new habit that you have to start fresh developing.
Is it a good practice to work on one of the ten perfections while meditating, like
noting rising and falling with the intention to enhance the quality of persistence in oneself?
No.
No, it's not how the ten perfections work, the ten perfections are the actual thing,
and doing that thing, and what you need to become a Buddha, so if you want to become a
Buddha you have to perfect ten things, you have to actually do those things, and meditation
is for another purpose.
After years of hardship I found a prestigious job, I note the conceit arising, I try
to take that as part of the eightfold path, just an honest, right livelihood, an honestly
sharing merit, any other tip.
The weight conceit and things like that, they only go away with intense and successful
meditation, an anigamic still has conceit, ultimately the order of progression is to remove
wrong view, so having right view about things like conceit, which ultimately means seeing
them clearly, and seeing that they are not a problem, and not being conceited or taking
conceit to be me, just knowing when you're conceited basically, and then once you are able
to understand such things that what is good for you and what is bad for you and you see
more clearly what that is, then you start to work on those, then you work immediately
on the likes and dislikes, our biases, you can see this still something that takes longer
to go away, once your biases are gone then you can start to really work on conceit, there's
nothing wrong with having a prestigious job, just don't take it as me or mine, just try
and take it as a practice, when noting sitting posture between each note there should be
an awareness of the posture, does this require looking at the posture and seeing before
noting each time we note as if keeping track, no but if you want before you start meditating
that you can take a look, but no it's not about vision, it's about awareness and it's
much more to do with the feelings of tension and pressure and hardness and softness.
It seems to me that I never grasp the emotions, as after once I note them, I can only see
the physical sensations clearly, how can I learn about the emotions if I can't see the
mental part?
So if you're looking for them that way, you're only ever going to see the physical because
you're trying to find something physical and emotions aren't like that, you're looking
for something that isn't there, you're thinking of it in the physical sense, because
an emotion will give an English, the emotion for us includes the physical part, but you
can be aware that you like something or dislike something, you just have to be clear
that the liking and disliking is not the physical feeling, the pleasure, I mean it's not
even physical so much as it's a feeling, it's not even so much being physical, it's just
it being separate from the actual liking or disliking and it can last longer than the
liking or disliking, so don't go looking for it, in fact the noting isn't about looking
for something, it's about responding to the thing, when there is liking then right away
because of that you say liking, but the liking's already just a moment before, and so
if you go looking for it, you won't find it, it's already gone, seeing that things are
gone like that is also important, if then you only see the feeling, you're not the feeling,
pain or pleasure, we're not trying to learn about anything, what you've already learned
is enough that it's not longer there, where to go, already disappeared, that's enough.
How to note the transition from sitting to walking meditation, what we do walking first
and then sitting, so there really shouldn't be a transition from sitting to walking, but
if you're going to do walking, sitting, walking, sitting that sort of thing, you just
note intending to stand and then standing, standing or bending or however you move
and then intending to walk and then walking.
Sometimes while meditating, I feel like a wet sock, keep getting distracted and it also stops
me from wanting to go to the gym, this makes me feel lazy and I feel bad because it's
sloth and torpor hindrance, please help.
If you're distracted, say distracted, if you feel lazy, say lazy, if you feel bad, say
less liking, if I can't help you, these are your states, not mine, it can help you help
yourself.
If you haven't read the booklet, maybe you haven't read our booklet, I recommend that.
If you're really interested, if you really want some help, you can take the ad home course,
also free, that might help.
I sometimes feel quite exhausted after continuously meditating for a while and I don't
feel like getting back to that intense focus, any solution to that.
So I don't know what type of meditation you're practicing, but that sort of intense
focus isn't what we're all about, I recommend maybe reading the booklet, maybe trying
to do an ad home course, I mean even in this tradition, people can give rise to these
sorts of states, that's not something that I'm unfamiliar with, it's just that it's something
that is not a part of the meditation, so it's something you'll have to learn eventually
to let go of, the habit will have to go away in favor of a more fluid, flexible awareness,
less intense.
And we've reached the end of questions about meditation, but there, okay, well we've reached
four o'clock, so just right.
So thank you all, you can now chat, everyone can say sad, who it is good, sad, who it
was good, sad, happy to come here and help, thank you Chris for coming to help, looks like
our moderators are also busy, so it's good we have a team, let's Chris focus more on
the questions, again imagine having to sift through them and figure out what tear everything
belongs to, I'm happy to do my part, okay that's all for today everyone, thank you for
coming.
