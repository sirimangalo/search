1
00:00:00,000 --> 00:00:06,000
How to meditate by Utadamo beepu, read by Adirokes.

2
00:00:06,000 --> 00:00:07,000
Introduction

3
00:00:07,000 --> 00:00:14,600
This booklet is taken from a six-part video series on YouTube, available at youtube.com

4
00:00:14,600 --> 00:00:17,300
slash Utadamo.

5
00:00:17,300 --> 00:00:22,400
It was originally intended for use in the Los Angeles Federal Detention Center, where

6
00:00:22,400 --> 00:00:27,900
it was impossible to distribute teachings by video, but has since become my preferred means

7
00:00:27,900 --> 00:00:32,400
of introducing the meditation practice to newcomers in general.

8
00:00:32,400 --> 00:00:37,700
While the videos provide a useful visual guide, this booklet contains much updated and

9
00:00:37,700 --> 00:00:41,600
expanded information that is not in the videos.

10
00:00:41,600 --> 00:00:46,300
The lessons are laid out, according to how I would expect a newcomer to learn meditation

11
00:00:46,300 --> 00:00:48,000
step by step.

12
00:00:48,000 --> 00:00:53,800
It may seem odd that chapters 2, 3, and 5 are presented in the opposite order in which

13
00:00:53,800 --> 00:00:56,000
they are to be practiced.

14
00:00:56,000 --> 00:01:01,000
The reasoning is that sitting meditation is easiest for a beginner to appreciate.

15
00:01:01,000 --> 00:01:06,300
Once one has become comfortable with the concepts involved in meditation, they may expand

16
00:01:06,300 --> 00:01:12,100
their practice to include walking, and even mindful frustration if they are so inclined.

17
00:01:12,100 --> 00:01:16,600
My only intention in completing this task is that more people may benefit from the meditation

18
00:01:16,600 --> 00:01:18,200
practice.

19
00:01:18,200 --> 00:01:23,200
It seems proper that if one wishes to live in peace and happiness, one should work to

20
00:01:23,200 --> 00:01:27,500
spread peace and happiness in the world in which one lives.

21
00:01:27,500 --> 00:01:31,800
I would like to thank all who have helped to make this book possible.

22
00:01:31,800 --> 00:01:38,200
My parents and all of my past teachers, my current teacher and preceptor, Ajahn Tong-Siri

23
00:01:38,200 --> 00:01:43,600
Mangalow, and those kind beings who originally transcribed the teachings from the YouTube

24
00:01:43,600 --> 00:01:44,600
videos.

25
00:01:44,600 --> 00:01:50,200
May all beings be happy, you to download.

26
00:01:50,200 --> 00:01:54,000
Chapter 1. What is Meditation?

27
00:01:54,000 --> 00:01:58,960
This book is meant to serve as an introductory discourse on how to meditate, for those with

28
00:01:58,960 --> 00:02:04,520
little or no experience in the practice of meditation, as well as those who are experienced

29
00:02:04,520 --> 00:02:10,120
in other types of meditation, but interested in learning a new meditation technique.

30
00:02:10,120 --> 00:02:15,080
In this first chapter, I will explain what meditation is and how one should go about practicing

31
00:02:15,080 --> 00:02:16,080
it.

32
00:02:16,080 --> 00:02:20,960
First, it is important to understand that the word meditation means different things to

33
00:02:20,960 --> 00:02:22,800
different people.

34
00:02:22,800 --> 00:02:28,280
For some, meditation simply means the calming of the mind, the creating of a peaceful or

35
00:02:28,280 --> 00:02:34,320
pleasurable state, as a vacation or escape from mundane reality.

36
00:02:34,320 --> 00:02:40,680
For others, meditation implies extraordinary experiences, or the creation of mystical, even

37
00:02:40,680 --> 00:02:45,040
magical, states of awareness.

38
00:02:45,040 --> 00:02:49,960
In this book, I'd like to define meditation based on the meaning of the word itself.

39
00:02:49,960 --> 00:02:55,960
The word meditation comes from the same linguistic base as the word medicine.

40
00:02:55,960 --> 00:03:01,080
This is useful in understanding the meaning of meditation, since medicine refers to something

41
00:03:01,080 --> 00:03:04,440
that is used to cure bodily sickness.

42
00:03:04,440 --> 00:03:10,640
As a parallel, we can understand meditation as being used to cure sickness in the mind.

43
00:03:10,640 --> 00:03:16,000
Additionally, we understand that medicine, as opposed to drugs, is not for the purpose

44
00:03:16,000 --> 00:03:22,280
of escaping into a temporary state of pleasure or happiness and then fading away, leaving

45
00:03:22,280 --> 00:03:25,040
one sick as before.

46
00:03:25,040 --> 00:03:30,720
Medicine is meant to affect a lasting change, bringing the body back to its natural state

47
00:03:30,720 --> 00:03:33,160
of health and well-being.

48
00:03:33,160 --> 00:03:38,080
In the same way, the purpose of meditation is not to bring about a temporary state of

49
00:03:38,080 --> 00:03:39,720
peace or calm.

50
00:03:39,720 --> 00:03:46,040
But rather to return the mind, suffering from worries, stresses, and artificial conditioning,

51
00:03:46,040 --> 00:03:50,520
back to a natural state of genuine and lasting well-being.

52
00:03:50,520 --> 00:03:55,160
So when you practice meditation according to this book, please understand that it might

53
00:03:55,160 --> 00:03:58,920
not always feel either peaceful or pleasant.

54
00:03:58,920 --> 00:04:05,240
Coming to understand and work through deep-rooted states of stress, worry, anger, addiction,

55
00:04:05,240 --> 00:04:11,760
etc., can be at times quite an unpleasant process, especially since we spend most of our

56
00:04:11,760 --> 00:04:16,800
time avoiding or repressing these negative aspects of our mind.

57
00:04:16,800 --> 00:04:22,160
It might seem at times that meditation doesn't bring any peace or happiness at all.

58
00:04:22,160 --> 00:04:26,760
This is why it must be stressed that meditation isn't a drug.

59
00:04:26,760 --> 00:04:31,400
It isn't supposed to make you feel happy while you do it and then return to your misery

60
00:04:31,400 --> 00:04:34,000
when you are not.

61
00:04:34,000 --> 00:04:39,200
One is meant to affect a real change in the way one looks at the world, bringing one's

62
00:04:39,200 --> 00:04:43,120
mind back to its natural state of clarity.

63
00:04:43,120 --> 00:04:49,040
It should allow one to attain true and lasting peace and happiness through being better

64
00:04:49,040 --> 00:04:53,560
able to deal with the natural difficulties of life.

65
00:04:53,560 --> 00:04:58,720
The basic technique of meditation that we use to facilitate this change is the creation

66
00:04:58,720 --> 00:05:01,200
of clear awareness.

67
00:05:01,200 --> 00:05:08,040
In meditation, we try to create a clear awareness of every experience as it occurs.

68
00:05:08,040 --> 00:05:14,840
Without meditating, we tend to immediately judge and react to our experiences as good,

69
00:05:14,840 --> 00:05:24,840
bad, me, mine, etc., which in turn gives rise to stress, suffering, and mental sickness.

70
00:05:24,840 --> 00:05:30,440
By creating a clear thought about the object, we replace these sorts of judgments with

71
00:05:30,440 --> 00:05:34,040
a simple recognition of the object as it is.

72
00:05:34,040 --> 00:05:38,760
The creation of clear awareness is affected through the use of an ancient but well-known

73
00:05:38,760 --> 00:05:42,520
meditation tool called a mantra.

74
00:05:42,520 --> 00:05:48,200
A mantra refers to a word or phrase that is used to focus the mind on an object, most

75
00:05:48,200 --> 00:05:51,640
often the divine or the supernatural.

76
00:05:51,640 --> 00:05:57,240
Here, however, we use the mantra to focus our attention on ordinary reality as a clear

77
00:05:57,240 --> 00:06:02,800
recognition of our experience as it is, free from projection and judgment.

78
00:06:02,800 --> 00:06:08,080
By using a mantra in this way, we will be able to understand the objects of our experience

79
00:06:08,080 --> 00:06:12,640
clearly and not become attached or reversed to them.

80
00:06:12,640 --> 00:06:17,160
For example, when we move the body, we use a mantra to create a clear awareness of the

81
00:06:17,160 --> 00:06:24,400
experience using a mantra that captures its essence as in moving.

82
00:06:24,400 --> 00:06:33,720
When we experience a feeling, feeling, when we think, thinking, when we feel angry,

83
00:06:33,720 --> 00:06:37,400
we say in our mind, angry.

84
00:06:37,400 --> 00:06:43,240
When we feel pain, we likewise remind ourselves silently, pain.

85
00:06:43,240 --> 00:06:48,440
We pick a word that describes the experience accurately and use that word to acknowledge

86
00:06:48,440 --> 00:06:54,040
the experience for what it is, not allowing the arising of a judgment of the object

87
00:06:54,040 --> 00:06:59,560
as good, bad, me, mind, etc.

88
00:06:59,560 --> 00:07:04,720
The mantra should not be at the mouth or in the head, but simply a clear awareness of the

89
00:07:04,720 --> 00:07:07,200
object for what it is.

90
00:07:07,200 --> 00:07:13,360
The word, therefore, should arise in the mind at the same location as the object itself.

91
00:07:13,360 --> 00:07:18,720
Which word we choose is not so important, as long as it focuses the mind on the objective

92
00:07:18,720 --> 00:07:24,320
nature of the experience, to simplify the process of recognizing the manifold objects of

93
00:07:24,320 --> 00:07:31,240
experience, we traditionally separate experience into four categories.

94
00:07:31,240 --> 00:07:35,520
Everything we experience will fit in one of these four categories.

95
00:07:35,520 --> 00:07:41,520
They will serve as a guide in systematizing our practice, allowing us to quickly recognize

96
00:07:41,520 --> 00:07:47,520
what is and what is not real, and identify reality for what it is.

97
00:07:47,520 --> 00:07:52,480
It is customary to memorize these four categories before proceeding with the meditation

98
00:07:52,480 --> 00:08:04,080
practice, one, body, the movements and postures of the body, two, feelings, bodily and

99
00:08:04,080 --> 00:08:12,560
mental sensations of pain, happiness, calm, etc., three, mind.

100
00:08:12,560 --> 00:08:21,480
Thoughts that arise in the mind of the past or future, good or bad, four, dhammas, a group

101
00:08:21,480 --> 00:08:27,720
of mental and physical phenomena that are of specific interest to the meditator, including

102
00:08:27,720 --> 00:08:32,880
the mental states that cloud one's awareness, the six senses by which one experiences

103
00:08:32,880 --> 00:08:36,680
reality and many others.

104
00:08:36,680 --> 00:08:42,960
These four, the body, the feelings, the thoughts and the dhammas are the four foundations

105
00:08:42,960 --> 00:08:45,640
of the meditation practice.

106
00:08:45,640 --> 00:08:49,680
They are what we use to create clear awareness of the present moment.

107
00:08:49,680 --> 00:08:56,360
First, in regards to the body, we try to note every physical experience as it happens.

108
00:08:56,360 --> 00:09:02,920
When we stretch our arm, for example, we say silently in the mind, stretching.

109
00:09:02,920 --> 00:09:06,640
When we flex it, flexing.

110
00:09:06,640 --> 00:09:10,920
When we sit still, we say to ourselves, sitting.

111
00:09:10,920 --> 00:09:15,480
When we walk, we say to ourselves, walking.

112
00:09:15,480 --> 00:09:20,880
Whatever position the body is in, we simply recognize that posture for what it is, and whatever

113
00:09:20,880 --> 00:09:27,120
movement we make, we simply recognize it's essential nature as well, using the mantra

114
00:09:27,120 --> 00:09:31,960
to remind ourselves of the state of the body as it is.

115
00:09:31,960 --> 00:09:37,840
In this way, we use our own bodies to create a clear awareness of reality.

116
00:09:37,840 --> 00:09:41,760
Next are the feelings that exist in the body and the mind.

117
00:09:41,760 --> 00:09:46,240
When we feel pain, we say to ourselves, pain.

118
00:09:46,240 --> 00:09:56,280
In this case, we can actually repeat it again and again to ourselves as pain, pain, pain.

119
00:09:56,280 --> 00:10:03,720
So that instead of allowing anger or aversion to arise, we see it merely as a sensation.

120
00:10:03,720 --> 00:10:09,440
We learn to see that the pain and our ordinary disliking of it are two different things,

121
00:10:09,440 --> 00:10:15,200
that there is really nothing intrinsically bad about the pain itself, nor is it intrinsically

122
00:10:15,200 --> 00:10:19,680
ours since we can't change or control it.

123
00:10:19,680 --> 00:10:24,800
When we feel happy, we acknowledge it in the same way, reminding ourselves of the true

124
00:10:24,800 --> 00:10:31,560
nature of the experience as happy, happy, happy.

125
00:10:31,560 --> 00:10:36,000
It is not that we are trying to push away the pleasurable sensation.

126
00:10:36,000 --> 00:10:41,280
We are simply ensuring that we do not attach to it either, and therefore do not create

127
00:10:41,280 --> 00:10:46,720
states of addiction, attachment, or craving for the sensation.

128
00:10:46,720 --> 00:10:52,160
As with pain, we come to see that the happiness and our liking of it are two different

129
00:10:52,160 --> 00:10:53,480
things.

130
00:10:53,480 --> 00:10:58,600
When there is nothing intrinsically good about the happiness, we see that clinging to the

131
00:10:58,600 --> 00:11:04,440
happiness does not make it last longer, but leads rather to dissatisfaction and suffering

132
00:11:04,440 --> 00:11:07,040
when it is gone.

133
00:11:07,040 --> 00:11:16,560
Likewise, when we feel calm, we say to ourselves, calm, calm, clearly seeing and avoiding

134
00:11:16,560 --> 00:11:20,360
attachment to peaceful feelings when they arise.

135
00:11:20,360 --> 00:11:26,000
In this way, we begin to see that the less attachment we have towards peaceful feelings,

136
00:11:26,000 --> 00:11:29,120
the more peaceful we actually become.

137
00:11:29,120 --> 00:11:31,920
The third foundation is our thoughts.

138
00:11:31,920 --> 00:11:36,960
When we remember events in the past, whether they bring pleasure or suffering, we say

139
00:11:36,960 --> 00:11:41,480
to ourselves, thinking, thinking.

140
00:11:41,480 --> 00:11:47,440
Instead of giving rise to attachment or aversion, we simply know them for what they are,

141
00:11:47,440 --> 00:11:48,840
thoughts.

142
00:11:48,840 --> 00:11:53,960
When we plan or speculate about the future, we likewise simply come to be aware of the fact

143
00:11:53,960 --> 00:12:00,040
that we are thinking, instead of liking or disliking the content of the thoughts, and thus

144
00:12:00,040 --> 00:12:05,320
avoid the fear, worry, or stress that they might bring.

145
00:12:05,320 --> 00:12:12,520
The fourth foundation, the Donlas, contain many groupings of mental and physical phenomena.

146
00:12:12,520 --> 00:12:17,160
Some of them could be included in the first three foundations, but they are better discussed

147
00:12:17,160 --> 00:12:21,040
in their respective groups for ease of acknowledgement.

148
00:12:21,040 --> 00:12:26,120
The first group of Donlas is the five hindrances to mental clarity.

149
00:12:26,120 --> 00:12:34,920
These are the states that obstruct one's practice, desire, aversion, laziness, distraction,

150
00:12:34,920 --> 00:12:37,360
and doubt.

151
00:12:37,360 --> 00:12:42,600
They are not only hindrances to attaining clarity of mind, they are also the cause for all

152
00:12:42,600 --> 00:12:45,920
suffering and stress in our lives.

153
00:12:45,920 --> 00:12:51,920
It is in our best interest to work intently to understand and discard them from our minds,

154
00:12:51,920 --> 00:12:56,760
as this is, after all, the true purpose of meditation.

155
00:12:56,760 --> 00:13:02,160
So when we feel desire, when we want something we don't have, or are attached to something

156
00:13:02,160 --> 00:13:09,200
we do, we simply acknowledge the wanting or the liking for what it is, rather than erroneously

157
00:13:09,200 --> 00:13:12,480
translating desire into need.

158
00:13:12,480 --> 00:13:21,960
We remind ourselves of the emotion for what it is, thus, wanting, wanting, liking, liking.

159
00:13:21,960 --> 00:13:27,400
We come to see that desire and attachment are stressful, and causes for future disappointment

160
00:13:27,400 --> 00:13:32,600
when we cannot obtain the things we want, or lose the things we like.

161
00:13:32,600 --> 00:13:38,800
When we feel angry, upset by mental or physical experiences that have arisen, or disappointed

162
00:13:38,800 --> 00:13:40,840
by those that have not.

163
00:13:40,840 --> 00:13:48,720
We recognize this as angry, angry, or disliking, disliking.

164
00:13:48,720 --> 00:13:55,200
When we are sad, frustrated, bored, scared, depressed, etc.

165
00:13:55,200 --> 00:14:05,080
We likewise recognize each emotion for what it is, sad, sad, frustrated, frustrated, etc.

166
00:14:05,080 --> 00:14:10,600
And see clearly how we are causing suffering and stress for ourselves by encouraging these

167
00:14:10,600 --> 00:14:13,760
negative emotional states.

168
00:14:13,760 --> 00:14:18,120
Once we see the negative results of anger, we will naturally incline away from it in the

169
00:14:18,120 --> 00:14:19,920
future.

170
00:14:19,920 --> 00:14:27,920
When we feel lazy, we say to ourselves, lazy, lazy, or tired, tired.

171
00:14:27,920 --> 00:14:33,240
And we will find that we are able to regain our natural energy in this way.

172
00:14:33,240 --> 00:14:36,840
When we are distracted, worried, or stressed.

173
00:14:36,840 --> 00:14:47,200
We can say, distracted, distracted, worried, worried, or stressed, stressed.

174
00:14:47,200 --> 00:14:50,480
And we will find that we are more focused.

175
00:14:50,480 --> 00:14:53,880
When we feel doubt, we are confused about what to do.

176
00:14:53,880 --> 00:15:01,720
We can say to ourselves, doubting, doubting, or confused, confused.

177
00:15:01,720 --> 00:15:07,880
And likewise, we will find that we are more sure of ourselves as a result.

178
00:15:07,880 --> 00:15:13,040
The clear awareness of these four foundations constitutes the basic technique of meditation

179
00:15:13,040 --> 00:15:17,200
practice as explained in the following chapters.

180
00:15:17,200 --> 00:15:22,320
It is therefore important to understand this theoretical framework before beginning to undertake

181
00:15:22,320 --> 00:15:25,760
the practice of meditation.

182
00:15:25,760 --> 00:15:31,080
Understanding and appreciating the importance of creating a clear awareness about the objects

183
00:15:31,080 --> 00:15:38,080
of our experience as a replacement to our judgmental thoughts is the first step in learning

184
00:15:38,080 --> 00:16:04,080
how to meditate.

