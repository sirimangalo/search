Good evening, everyone, and welcome to our evening gathering tonight, I promise to talk
about the middle way. Question was asked about what exactly the middle way means.
So I think it's important to talk about, first of all, because it's often misunderstood.
Easily abused, but also because it does in many ways capture the essence of what we're trying to do if understood properly.
It's a little bit misleading when we talk about the middle way, because it seems to sound like
there's a happy medium in between two things where you don't go extreme on anything, but on the other hand you don't give up anything.
And that's not generally speaking, if I can speak for the Buddha, which of course is fraught with some danger.
It's not generally how the middle way is understood. Take for example the first teaching of the Buddha, this is the most famous, most well-known usage of
this idea of the middle way. So first of all, some background we're talking about.
It's a talk given to five ascetics, five Hindu or Indian ascetics who had left Brahman society because they, like many before them, thought to themselves,
realized that there was something quite wrong about the indulgence in central pleasures.
So religion that was associated with sensuality, where the priests and the
supplicants or the applicants, those who practiced the sacrificial rituals, and they really weren't getting anything out of it.
So there was this movement in the time of the Buddha to leave behind and turn away from central pleasure with the realization that it was an addiction.
It was something that kept you tied to Samsara. It was a strong sort of mystical tradition.
A lot of talk about rising above and getting beyond Samsara.
Different ideas of what it meant, of how to see through the illusion of sensuality and how to break free from the bonds of sensuality.
So what you had was a broad movement to go to the other extreme.
Of course, it wasn't thought of this way. It was when you reject something.
And so the other thing is the right thing, the opposite for lack of a better word is the right thing.
And so you had a lot of people in the time of the Buddha torturing themselves, inflicting pain upon themselves,
thinking that if pleasure is the wrong way, then of course the right way must be whatever is the opposite is pain.
So if you torture yourself, you could free yourself from any desire for sensuality.
It's kind of odd when you think about it, but there's an on sort of logic to it.
You beat it out of yourself.
Somehow we think this when we punish people, right, we punish children who are following their cravings by hitting them.
We think if we give them pain, somehow it will make them want pleasure less.
It's kind of an odd sort of reasoning, but we have this sort of reasoning, right?
Sandy, if you hurt someone, there's some reason to be less inclined towards pleasure, which of course seems to be the opposite.
Makes one want to rethink the whole idea of punishment, right?
But of course, aren't into punishment. It's very bad karma.
But so this was the wrong view. This was the wrong idea.
And when the Buddha came to teach the Middle way, he said,
there are these two unties, the end of something, the extreme. It doesn't usually mean extreme, but it's used in that way here.
He's saying that somewhere in the between there's a happy medium where you don't go overboard with sensuality.
But on the other hand, you don't torture yourself too much.
And so we find meditators often taking up this view of a moderate view.
It's okay to engage in some addictions.
If you're addicted to chocolate, it's okay to enjoy the chocolate. It's just some moderation.
Just don't have too much alcohol. Some people go to the extent.
Buddhist go to the extent of moderation in alcohol.
Don't have enough to get drunk just enough to get a little happy.
A little liberating, it's both.
And don't push yourself too hard, but you have to push yourself.
You have to force yourself to do things, because who wants to really spend weeks and months meditating.
I don't push yourself to do it. Force yourself a little bit, but not too much.
We maybe don't use the word force, but it was sense that you have to push yourself.
Just don't push yourself too hard, moderation.
It's not at all really what the Buddha meant.
I mean, we can see that from the text. It's sensuality of any sort is useless.
Not just useless, it's problematic. It's wrong.
It's not sensuality exactly.
Of course, experiencing sensuality is not wrong.
Looking at something beautiful is not a problem, but indulging it in the sense of enjoying it.
Appreciating it, liking it, liking it as the point.
When the Buddha said he may aid a nupagama means nupagama means not approaching these two.
Not going to these two extremes.
The Buddha found the middle way.
We think somewhere in the middle is where you have to find.
Maybe it's a very fine line, but it's somewhere in between these two.
When in fact, it really isn't.
If you look at what the Buddha talked about as being the middle way, it has nothing to do with either of the others.
So it's actually sort of exposed as a mere artifice of the Buddha.
If you understand in the context, it makes sense.
He was trying to say, look, you've gone to the other extreme, and it's just as useless as the other extreme.
They're both useless, is what he was saying. Forget about those two.
Let's try a third alternative, actually, to one extent.
To some extent, though, it feels very much like being in the middle.
As with others, another will go through a couple of other ways that it can be.
You can understand it was used in the polycan and in the commentary.
But it's very much in the middle. It's not just a third alternative.
Like, oh, he found another extreme to go to.
It's a sense. It's the extreme of being free from something, from any sort of extremism.
If that makes any sense.
So, if you think about what the aidful noble path, what the middle way is being the aidful noble path, if you look at it.
It's very much about not this, nor that, or anything, really.
So, when sensuality comes up, it's not about any sort of moderation.
But on the other hand, it's not about rejection, or avoidance, or self-torture.
So, in a sense, it is bright in that fine line in the middle.
It just has nothing to do with the other two.
When you experience something beautiful, for example.
There's nothing beautiful about what you're seeing.
The beautiful, ugly is really meaningless.
It's all very much in our minds, according to our past habits, and our genetic makeup, and so on.
So, the middle way is really a more simple, experiencing things, an objective experience.
When you see something to be aware that you're seeing it.
When you feel pain to be aware that you're feeling pain.
So, this idea that somehow you should avoid this other idea of, I guess it's the opposite, in fact.
So, on the other hand, some people will say, you shouldn't see beautiful things.
Take down all your, anything that could be beautiful.
If there's music, plug your ears.
Food, make sure you have only plain food, nothing delicious.
Nothing that could be considered delicious.
Don't ever eat chocolate, that would be, that would be bad.
On the other hand, don't ever expose yourself to pain.
So, you have people who would say, you know, sitting through pain.
This is a tort self torture.
It wasn't very much against what the Buddha taught.
Mahasi Sayada goes on about this guy here.
He goes on and on about it.
Not on and on, but he explains it quite well.
He says that these well-meaning meditation teachers will guide their students to always avoid pain.
When pain comes up to change position, always be in a comfortable position, so you never experience pain.
He says, it's quite misguided.
The Buddha, in fact, is quite clearly one of the texts that we have.
He says that you should be willing to bear with pain, even if it kills you.
Even deathly pain.
It could be a great catalyst for enlightenment.
So, the middle way in this sense, I mean, it's just talking about a way that is free from any kind of reaction.
A way that is free from any kind of ambition or drive or exertion.
It's, in fact, effortless.
The reason why meditation seems like such a chore is because we're so lazy, because we're so much to the indulgent side.
Or because we're forcing it, we're practicing self torture, and we meditate, we force our minds.
No, stay with the stomach, no, stay with the foot, no, don't go wandering, don't get greedy, don't get angry.
We fall to the extremes.
We fall to the extremes, because we're not present, we're not in the center, in the middle.
So, it's not exactly a spectrum, it's not that take everything to moderation.
It's freedom from them both, which puts you right in the center, which you've centered.
Pure.
If you think purity, what is purity?
It's the freedom from defilement.
Clarity is the freedom from being clouded, being obscure, being blurred.
What is right?
You look at the eight full noble path, it's all about right, some entity right view.
And right view is really, for the most part, just not having any wrong view.
So, about having views of reality, what is nature of reality?
There's nothing, it's not like the middle, the right view is just knowledge of the formal truth,
which there's nothing, it's not like that's moderate.
It's in between two things.
It's just freedom from any other kind of view.
And so on.
If you look at the eight full noble path, you can clearly see it's not at all about moderation,
it's about being right and pure in the sense of not having anything that diverts you,
takes you to an extreme.
The other way that the middle way is used is, what's used in different ways.
Another way it's abused is to give the idea that the Buddha didn't teach non-self,
or that non-self doesn't mean there is no self.
And in fact doesn't mean there is no self, but it's important to nuance this
and be careful.
So people say, well, because it doesn't mean there is no self.
The Buddha never, as far as I know, said that.
He certainly didn't deny it.
But as I've said many times, this is because of the nature of reality.
There is no self, it's just a statement that doesn't make any sense.
It's like saying there is no cat.
Well, cats are not things that exist and neither are selves.
So you say, well, cats don't exist.
What do you mean?
They have a cat.
Right?
So it's confusing.
A cat is always an abstraction.
It's on a whole other level of reality.
Non-self is dealing with reality.
So the way the Buddha explained to you, people saying that when you die,
the soul continues on.
When the body dies, the soul enters another body.
The soul is eternal.
And other people would say, no, when you die, there is a cessation.
There's nothing.
There's no experience, nothing.
And the Buddha said, these are two extreme views.
These two views are our wrong view.
So he teaches dependent origination, which is in the middle.
Why is it in the middle?
Because it describes a causal chain of experience.
And then it doesn't rely upon the conceptual abstraction of there being a self or something.
Something that would be totally outside of the realm of experiential reality.
That we could call itself.
Right?
So people get all worried or confused about this idea of non-self,
or did the Buddha actually say there was a self?
And he really said, let go of all that stuff.
And the Buddhism is really an innocuous.
He just didn't say anything, really.
What he pointed out was that we have all these attachments to self.
We have ego, we cling to things as being me as mine and so on.
And so he advocated a giving up of self view,
giving up of any view relating to self.
I hope I don't sound like I'm trying to say that there is any sort of self.
It's not.
It was very much on the side of being non-self.
That any way we could conceive of a self,
any conceiving that might go on, that might arise in the mind.
I mean, it's just an arisen phenomenon.
This belief or this thought,
maybe there's a self, or I feel like there's a self.
All of that is problematic.
It's a cause for stress and suffering,
because then there's clinging.
And so he taught what he saw, what we can all clearly see,
that there is causal relationships between phenomena,
between experiences.
It's a very much a middle way.
It's a sort of a view that it should not be considered extreme,
but it is this idea that any view of self could be problematic.
It sounds very much like there is no self
and then you think, well, what about myself?
I was just reading the Wikipedia page on the middle way.
And it claims that, in terabyte of Buddhism,
there's the idea that, on our hunt, upon passing,
neither exists nor non-exists.
It says the polycannon says that.
It doesn't give us citations.
I'd like to have someone go in there
and put a citation missing or something,
notation because, pretty sure that's not what it says.
I remember in the polycannon,
it says someone asks, does the Narah hunt exist
after they pass away?
No, that's not the case.
Is it that they don't exist when they pass away?
No, that's not the case.
I mean, that's not proper to say.
Well, do they both exist and not exist?
No, that's not proper to say.
Do they neither exist nor not exist?
It says on Wikipedia.
And the answer was, no, that's not the case either.
Because, because Narah hunt is just a concept.
Existence is not like that existence is experience.
All that exists is moments of experience.
So sometimes these things that the Buddha said
and sometimes it was just an avoidance
because people get so confused
because they're living in this intellectual
or conceptual reality
where I exist and there are people in places and things.
We're very much extreme in this way.
We've gone to an extreme of delusion
where we believe that things exist
where we have caught up in our conceptions.
Our sunya recognize things
and it morphs into this belief
for this conception of things as existing
as having entities.
The real middle way is based on the three characteristics
and it's very much about not, not, not, not.
Not permanent, not stable, not lasting.
Not pleasant, not satisfying, not happiness.
Not me, not myself.
What it means is we have all these attachments
to things as being me and mine
as being pleasant, as being a source of happiness
and as being stable as being a refuge.
We just learned today in the Visudhi Manga.
We got to this.
One of the best passages I think
are one of the most important passages
I think in the Visudhi Manga.
Not for the aspect of the Buddhist teaching
that it describes, but just how well it describes
the three characteristics.
I mean, these are so important.
So it gives 40 ways that come from,
I think, the Patisambhida Maghara may be even earlier.
40 ways by what you can understand
the three characteristics.
We read through that.
I encourage everyone you can look it up.
It's in the beginning or the halfway through,
maybe the chapter on Maghara Maghana doesn't know
you said either.
Purification by knowledge and vision
of what is the path and what is not the path.
When one first begins to grasp what is the path?
The path is this.
The path is seeing through our illusions,
our ignorance, seeing through our conception of permanence.
And stability and satisfaction and control
to see impermanence suffering in non-self.
As you start to grasp this,
you make that shift and you start upon the noble path.
That's really the middle way.
When a person begins to see impermanence suffering in non-self,
meaning they begin to give up,
or they begin to get a glimpse of what it means
to let go and how one actually goes about letting go.
It's quite simple as you start to see impermanence.
You give up permanence or you give up those things
that you were clinging to because you thought they were stable.
As you start to see suffering,
you give up those things that you thought were bringing you happiness.
That weren't actually bringing you happiness.
As you start to see non-self,
you give up those things that you thought were you and yours
and belong to you.
Once that clinging ceases,
then suffering ceases.
The suffering and the stress which comes from the ignorance
and the delusion.
So the middle way is very much just a simplification.
It's about giving up those things that one might call extreme.
And often call extreme because they end up being opposites.
This is no good, so let's do this.
There's this argument over the two,
and the Buddha is just as neither is really good.
In terms of our meditation,
it does feel very much like being in the middle
because you give up your desires
and you give up your versions
because you're patient with pleasant things.
You're patient with unpleasant things obviously,
but you're also patient with pleasant things.
When something pleasant comes,
you have to be just as patient
in the sense of not jumping for it.
I think of something you want chocolate
due to find the patient.
The ability to be with the experience wanting,
wanting for example,
liking or feeling, feeling.
As much as you are for pain,
so when you feel unpleasant sensation,
you say pain, pain or disliking,
and you just be with them.
That's really the path.
When you experience disliking,
you're not trying to shut it off or stop it,
but you're also not going with it.
When you're angry, when you're bored,
when you're frustrated,
and when you want something,
you're not trying to shut it off.
Say, no, bad, that's a problem,
but you're also not going with it.
If you're objective,
you say liking or wanting,
and let it go.
Let it come, let it go.
So there you go.
There's a sort of abbreviated,
well, moderate.
There's a moderate talk.
Moderate length talk on the middle way.
So thank you all for tuning in.
I'll show you all the best.
Thank you.
two versions of the middle way, one for the laity and one for the monks.
You know, I read somewhere and I can't find it for the life of me.
There are two paths. There's the higher path and the lower path.
So you can all go if you're still on course, just go practice.
So the higher path just means the path for monks and the lower path is the path for laypeople.
I've looked for it. I'm not even sure if it's in the canon or the commentaries.
There's a long time ago when I read it.
But it does seem to me that it's somewhere in the canon or commentaries.
Just talk about a higher and a lower path, but it's not in the middle way.
There's nothing to do with the middle way. The middle way is this.
It's the avoiding these extremes. So anyone who's Buddhist has to follow the middle way.
It means not to go to either of the extreme.
But yes, I'm quite sure at some point, maybe it seems to me that it's actually using poly words,
but I've searched for it and can't find it.
Upari is higher and I can't remember what it's used for the lower path.
But it just means that the monastic life is more powerful and more exalted in some way.
I mean, it's not inferior or superior in a sense, but there is something considered to be higher about the monastic path.
It's more powerful.
So the Dhamachakapawat in the suit is yes, the one that he gave to the five ascetics.
That was about the middle way, yes.
And so another thing I probably should have said is the middle way is not something the Buddha taught very often.
It's not something that he went around teaching again and again. You only find it in a few places.
And so to make a huge deal out of it and say, you know, to bring it up as sort of a basic Buddhist teaching,
it's a bit misleading. It's not that it's not an important concept,
but it's certainly not something the Buddha harped upon or, you know, talked about again and again.
It's much more a bit of a gimmick in a sense.
An artifice, as I said, to help people see that they're, you know, they're not doing any better than the other way.
You think you've, by avoiding one thing you've found the right thing?
Well, no, you've just gone to another extreme.
And so when people came up with this fighting this way or that way, the Buddha said, you know, you're both wrong.
So Buddhism is in the middle in a sense.
But he didn't use it. He didn't talk about it that much.
It doesn't even have its own sunute. It's not a topic that is really all that important.
Now, the path, of course, is, is, is key to Buddhism, but it's not that often talked about as being the middle way.
But the context with these five ascetics is very important. For them, they were doing just this.
They had abandoned one practice that was useless.
And they'd taken on the opposite practice, which turned out to be also useless.
And so the Buddha said, these are two extremes.
They were two useless practices, given them both up.
But he did it by using the idea of the middle way.
It feels like the middle. When you give one up and you give the other one up,
well, you feel like you're kind of in the middle.
That's not about moderation or anything like that. It's about giving up.
All right. Any other questions?
What distinguishes Theravada from other Buddhist practices?
And why is it the right one?
Well, it's not the only one that's right.
But we take issue with many, many of the things that came out of the later schools.
So I don't take issue with the Mahayana per se in the sense of wanting to become a Buddha
and thinking it's better to become a Buddha.
I mean, I don't have a problem with it. I don't think most Theravada Buddhists do.
But Theravada means following the original teachings of the Buddha,
there are later texts that would have you believe that it's all a lie.
And they tell you it's not a lie, but it's just not the truth.
And it's a means of arguing for their practices,
as opposed to the mainstream practice.
I mean, Theravada's very mainstream. It's very simple.
It's very much agreed upon as the sort of thing that the Buddha taught.
It may not be exact. We don't know.
But everyone sort of has a sense that this is really what the Buddha taught.
It's just a lot of people are like, eh, eh, only taught that because we couldn't understand the real truth,
which is really, if you look at the history of it,
it was just a ploy to get people to accept their new teachings.
The problem is that it was later adopted in societies that really didn't have the context for this argument.
And so they took it to me and I took it to many different things and came up with all sorts of different things.
If you look at some of the later schools of Buddhism,
they really, really go quite far from the mold.
They end up saying you have to have perfect faith and stop practicing
and have perfect faith in the Buddha.
Because any sort of practice means you don't have faith and their ideas,
Buddha will save you, things like that.
It just totally bizarre.
So there's not quite a direct answer to your question.
Obviously, I'm going to say Theravada is right, but you can't set up a false dichotomy and say
there's either A or B and why is A right and B is not.
It's a false equivalence or a false dichotomy, a false equivalence maybe.
It's Mahayana, not just Mahayana, but all the other schools grow out of this base
that the Theravada does the best to encapsulate.
And I think everyone's pretty clear, even the Mahayana,
it's pretty clear that the Theravada is a good representation of the actual things that Buddha taught.
The most, for the most part.
Question from a dominant discussion.
But it's all in our translation.
I mean, it's a fire, it's a fire, it's an interesting question.
I mean, a fire actually isn't hot.
Because hot and cold are relative.
They're relative actually to the human, to the being.
Not just the human, to a being who experiences them.
If you put your hand in fire, then you get hot.
So if you say that, let's take Rupa, form is suffering.
Well, if you look at the form, if you look at a rock, for example,
the rock isn't suffering.
It's not like the rock is crying or wailing or screaming.
And the rock just sitting there isn't hurting you either, so it's not even causing you suffering.
But Dukka is more of an active thing.
It's something that hurts you when you, it's not even something that hurts you.
It's something that is not happiness.
It's something that is useless.
Something that is even in the sense worse than useless, even in the sense it's bad.
But the press is probably, I mean they use this word, but it's inferior or it's undesirable.
It's not even undesirable.
It's not worth, it's not good for the point.
Because when you have to experience it, it's, I mean, the ultimate sense is not even that it's bad.
It's just that it's, there's no point to experiencing it.
There's no benefit from experiencing it.
There's no good that comes from it.
And so that's why the Buddha would often use Banjupa Dhanakanda.
It really only causes you suffering when you have expectations about it,
when you want it or when you want to avoid it.
And because it's not amenable to you, it's not something that can be a refuge or something you can control
than it causes you suffering.
The suffering doesn't actually come from, depending on how you define suffering.
It doesn't really come from the experience.
So the root by isn't really suffering.
Just because you experience root, that doesn't mean you're suffering.
Does that make sense?
But another way of looking at it is all rubbish, that's all garbage.
It's all bad in the sense.
Yes, all formations are dukkah.
It's a very good point.
That doesn't just relate to the clinging to them.
But they're all dukkah.
If you look at that in context, it's based on kara anitya.
It's not that they are somehow...
I mean, it's not that they're...
It's not even so much that they're erratic or chaotic.
It's that they are not permanent.
So dukkah means they're not happiness.
You can't really say that they're suffering.
But you can say that they're bad, I suppose.
I mean, because the point of reference is nibana.
And inherently flawed is maybe a good one.
But the point of reference is nibana.
I guess for all intents and purposes you might as well just say they're suffering.
It's just...
It's hard to understand, and unless you're an arahant, it's not the kind of thing that...
No, it's not even unless you're an arahant, unless you're well versed in the Buddha's teaching.
It's not something that most people are able to stomach or to accept.
Because clearly they would think we're not suffering all the time.
In fact, an arahant would look to some extent that an arahant is suffering all the time.
We all are suffering all the time, but there are sufferings.
It's too harsh of a word.
It's not really what it means.
We're all constantly oppressed, or we're all constantly bombarded by things that are arising and ceasing.
And in contrast with nibana, which has no arising and ceasing, they're inferior.
So it's really the idea that there's something bad about arising and ceasing.
And unless you're really dedicated to Buddhism, even if you're a Buddhist, I think it's often hard to stomach that idea.
The idea that everything in samsara is bad.
It's not something you believe.
It's something that you start to see.
And our claim is that eventually, if you see the truth, eventually you give up everything.
You're just inclined towards any experience.
You don't have to worry about it.
Just look and see the truth.
If we're wrong, we're wrong.
If there's something worth clinging to, you'll see it.
And you'll cling to it.
And good for you, if you can find something that is worth clinging to, good for you.
I suppose that's not something I should say, because it's easy to be misled and think that I found something worth clinging to.
So be objective and try and see the truth.
And you will see that nothing is worth clinging to.
That's the claim.
Something that can be totally investigated and verified.
All right, so that's all for tonight.
Thank you all for tuning in.
See you all next time.
