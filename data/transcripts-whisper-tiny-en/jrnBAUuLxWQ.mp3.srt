1
00:00:00,000 --> 00:00:07,000
If you are very distressed in some medical condition, which is not yet diagnosed,

2
00:00:07,000 --> 00:00:12,000
and not sure of your future life, plus you have a family,

3
00:00:12,000 --> 00:00:19,000
in that case, how do you focus on meditation?

4
00:00:19,000 --> 00:00:27,000
Well, the whole, the plus part, I don't know about, I think it's,

5
00:00:27,000 --> 00:00:32,000
you've got a point there, but the, if you are distressed part

6
00:00:32,000 --> 00:00:34,000
in having a medical condition, I would say,

7
00:00:34,000 --> 00:00:37,000
how could you not focus on meditation?

8
00:00:37,000 --> 00:00:42,000
What else would you think to do, then practice meditation,

9
00:00:42,000 --> 00:00:45,000
if you are in such a situation?

10
00:00:45,000 --> 00:00:49,000
Why would you be at all concerned about worldly affairs?

11
00:00:49,000 --> 00:00:52,000
And I think this would spill over into the plus.

12
00:00:52,000 --> 00:00:55,000
I don't think these two statements go together.

13
00:00:55,000 --> 00:01:00,000
That it may seem brutal, and it may seem cold,

14
00:01:00,000 --> 00:01:03,000
but you have to leave your family behind,

15
00:01:03,000 --> 00:01:06,000
whether it's because of this illness,

16
00:01:06,000 --> 00:01:08,000
or something because of something else.

17
00:01:08,000 --> 00:01:12,000
In the end, either you leave them behind or they leave you behind.

18
00:01:12,000 --> 00:01:17,000
No one takes their family members with them when they die.

19
00:01:17,000 --> 00:01:21,000
This is, this is incredibly obvious,

20
00:01:21,000 --> 00:01:26,000
yet very rare that people who actually realize this,

21
00:01:26,000 --> 00:01:30,000
because we know it intellectually up here, but in our hearts,

22
00:01:30,000 --> 00:01:36,000
we're acting in a way that's totally in Congress with that knowledge,

23
00:01:36,000 --> 00:01:39,000
so we don't really realize it.

24
00:01:39,000 --> 00:01:42,000
If you have to leave your family behind,

25
00:01:42,000 --> 00:01:50,000
then you better do some hefty work to free yourself,

26
00:01:50,000 --> 00:01:54,000
from the sorrow and the suffering that's going to come from that.

27
00:01:54,000 --> 00:02:00,000
Do you want to set yourself up for years and years of mourning

28
00:02:00,000 --> 00:02:03,000
that some people do when a family member passes away?

29
00:02:03,000 --> 00:02:07,000
Do you want to be on your deathbed feeling miserable

30
00:02:07,000 --> 00:02:13,000
because you're leaving behind people who are going to miss you

31
00:02:13,000 --> 00:02:18,000
and people who you love, people who you are attached to?

32
00:02:18,000 --> 00:02:21,000
I guess the answer to these questions is yes for some people,

33
00:02:21,000 --> 00:02:24,000
and that's why I said it can sound cold and brutal,

34
00:02:24,000 --> 00:02:28,000
but it's kind of our doctors.

35
00:02:28,000 --> 00:02:31,000
If you go to the doctor and you get all emotional,

36
00:02:31,000 --> 00:02:34,000
the doctor will never be emotional, they'll be so cold, right?

37
00:02:34,000 --> 00:02:40,000
And how doctors talk about sex and talk about the body and talk about the body parts.

38
00:02:40,000 --> 00:02:43,000
It just seems so cold and scientific,

39
00:02:43,000 --> 00:02:50,000
and that's kind of how you have to make allowance for at least in my case anyway.

40
00:02:50,000 --> 00:02:52,000
I think it's important for me to say these things,

41
00:02:52,000 --> 00:02:54,000
and I don't want to offend anyone,

42
00:02:54,000 --> 00:02:59,000
but this is the reality that you're faced with.

43
00:02:59,000 --> 00:03:04,000
Meditation should be at the top of your list.

44
00:03:04,000 --> 00:03:07,000
It should be one of the most important things in your life,

45
00:03:07,000 --> 00:03:12,000
especially if you know that you're not going to live any longer.

46
00:03:12,000 --> 00:03:15,000
The question that you're asking, I suppose,

47
00:03:15,000 --> 00:03:24,000
is with my mind so full of worry, sadness, fear, attachment,

48
00:03:24,000 --> 00:03:26,000
how can I focus on meditation?

49
00:03:26,000 --> 00:03:28,000
How can I calm my mind?

50
00:03:28,000 --> 00:03:30,000
It's probably the idea that's going through.

51
00:03:30,000 --> 00:03:34,000
How can I experience this state of peace and happiness?

52
00:03:34,000 --> 00:03:41,000
And obviously, I think for someone who's been meditating,

53
00:03:41,000 --> 00:03:43,000
and obviously the answer is going to be is that,

54
00:03:43,000 --> 00:03:45,000
well, that's why you're meditating.

55
00:03:45,000 --> 00:03:50,000
You're meditating in order to overcome these

56
00:03:50,000 --> 00:03:54,000
very emotions that you think of as getting in the way.

57
00:03:54,000 --> 00:03:58,000
Your defilements don't need to get in the way of your meditation.

58
00:03:58,000 --> 00:04:03,000
As I said, you can learn directly from the defilements

59
00:04:03,000 --> 00:04:10,000
from your mistakes,

60
00:04:10,000 --> 00:04:12,000
from the problems that you cause,

61
00:04:12,000 --> 00:04:15,000
coming to see this incredible danger.

62
00:04:15,000 --> 00:04:18,000
And this is why when people get sick like this

63
00:04:18,000 --> 00:04:21,000
or know that they have not so long to live,

64
00:04:21,000 --> 00:04:26,000
that they are actually able to put in some really good practice

65
00:04:26,000 --> 00:04:31,000
and actually able to attain really profound realizations

66
00:04:31,000 --> 00:04:34,000
because they can see the danger in clinging.

67
00:04:34,000 --> 00:04:36,000
It hits them very strongly,

68
00:04:36,000 --> 00:04:39,000
this attachment that they have to their own body

69
00:04:39,000 --> 00:04:43,000
to people around them to so many different things.

70
00:04:45,000 --> 00:04:49,000
And so I would say you're in a prime position to meditate

71
00:04:49,000 --> 00:04:53,000
when they're right, when these suppressed and hidden emotions

72
00:04:53,000 --> 00:04:55,000
are now coming to the fore

73
00:04:55,000 --> 00:04:57,000
and you're actually able to sort them out

74
00:04:57,000 --> 00:04:59,000
and to see the danger in them,

75
00:04:59,000 --> 00:05:01,000
see the problem with them, give them up

76
00:05:01,000 --> 00:05:04,000
and develop those minor states that are appropriate,

77
00:05:04,000 --> 00:05:07,000
that are beneficial, that lead to your happiness

78
00:05:07,000 --> 00:05:09,000
and the happiness and benefit of others,

79
00:05:09,000 --> 00:05:11,000
because obviously clinging to each other

80
00:05:11,000 --> 00:05:13,000
is not going to work at this time.

81
00:05:13,000 --> 00:05:16,000
Clinging to your family is not going to work.

82
00:05:16,000 --> 00:05:19,000
And just one more thing before I hand it over,

83
00:05:19,000 --> 00:05:21,000
I'm sorry to do all the talking,

84
00:05:21,000 --> 00:05:30,000
but the butt here is that

85
00:05:30,000 --> 00:05:34,000
you still have to have many responsibilities

86
00:05:34,000 --> 00:05:36,000
that you'll have to deal with.

87
00:05:36,000 --> 00:05:38,000
That's where I think you've got a point

88
00:05:38,000 --> 00:05:40,000
that things like family,

89
00:05:40,000 --> 00:05:42,000
not only family, but your responsibilities,

90
00:05:42,000 --> 00:05:45,000
your life can really get in the way.

91
00:05:45,000 --> 00:05:47,000
It's a very difficult situation.

92
00:05:47,000 --> 00:05:50,000
The best advice you can give to someone,

93
00:05:50,000 --> 00:05:52,000
I think that you could give to someone

94
00:05:52,000 --> 00:05:53,000
who's in a difficult situation

95
00:05:53,000 --> 00:05:55,000
where they might be faced with poverty

96
00:05:55,000 --> 00:05:59,000
where they might be subject to

97
00:06:01,000 --> 00:06:05,000
having great stress and difficulty in life.

98
00:06:05,000 --> 00:06:07,000
You know, even to the point

99
00:06:07,000 --> 00:06:09,000
where they might lose their house,

100
00:06:09,000 --> 00:06:11,000
they might lose their job and so on

101
00:06:11,000 --> 00:06:14,000
is to remember that no matter where you are,

102
00:06:14,000 --> 00:06:16,000
no matter what you do,

103
00:06:16,000 --> 00:06:19,000
no matter what happens to you in the end,

104
00:06:19,000 --> 00:06:23,000
your experience is the same as everyone else's experience.

105
00:06:23,000 --> 00:06:26,000
It's still just seeing, hearing,

106
00:06:26,000 --> 00:06:28,000
smelling, tasting, feeling and thinking.

107
00:06:28,000 --> 00:06:30,000
You learned this to a certain extent

108
00:06:30,000 --> 00:06:32,000
by becoming a monk,

109
00:06:32,000 --> 00:06:36,000
because as I said before, I've slept before on a park bench.

110
00:06:36,000 --> 00:06:39,000
I've slept under a picnic table in a park.

111
00:06:39,000 --> 00:06:44,000
I've slept in the forest on a mat and so on.

112
00:06:44,000 --> 00:06:49,000
So to some extent, this is a part of this experience,

113
00:06:49,000 --> 00:06:53,000
the realization that what's going to happen to you?

114
00:06:53,000 --> 00:06:55,000
I got bitten by a snake.

115
00:06:55,000 --> 00:06:57,000
Oh, now I've been bitten by a snake.

116
00:06:57,000 --> 00:06:59,000
Maybe I'm going to die. I started laughing about it.

117
00:06:59,000 --> 00:07:00,000
I went to see the mug.

118
00:07:00,000 --> 00:07:02,000
I said, I've been bitten by a snake.

119
00:07:02,000 --> 00:07:04,000
And he was kind of, oh, he didn't understand

120
00:07:04,000 --> 00:07:07,000
because I wasn't upset about it.

121
00:07:12,000 --> 00:07:17,000
You start to give up this partiality and attachment.

122
00:07:17,000 --> 00:07:20,000
So a very difficult situation.

123
00:07:20,000 --> 00:07:23,000
I just wanted to say that I think it'll be a real soulless

124
00:07:23,000 --> 00:07:25,000
if you can remind yourself that in the end it's all

125
00:07:25,000 --> 00:07:27,000
and remind the people around you as well

126
00:07:27,000 --> 00:07:31,000
who are going to depend on you for guidance and support.

127
00:07:31,000 --> 00:07:34,000
That in the end, our experience goes on.

128
00:07:34,000 --> 00:07:36,000
And when we pass away, we go on.

129
00:07:36,000 --> 00:07:37,000
We move on.

130
00:07:37,000 --> 00:07:41,000
In the end, the worst that can happen is that you pass away.

131
00:07:41,000 --> 00:07:42,000
Okay.

132
00:07:42,000 --> 00:07:45,000
So please take the mic away from me.

133
00:07:49,000 --> 00:07:54,000
I remember when I read the question one word

134
00:07:54,000 --> 00:07:57,000
that I like very much. It's very,

135
00:07:57,000 --> 00:08:03,000
when you ask, how can I meditate in such a meditation?

136
00:08:03,000 --> 00:08:11,000
You need very, and that is kind of a heroic effort.

137
00:08:15,000 --> 00:08:22,000
Everything is breaking apart and life is unsure anyway

138
00:08:22,000 --> 00:08:28,000
for everybody in such a situation you describe it.

139
00:08:28,000 --> 00:08:34,000
It's kind of really unsure and uncertain.

140
00:08:34,000 --> 00:08:38,000
And then you need the effort.

141
00:08:38,000 --> 00:08:41,000
You need to do the effort and say,

142
00:08:41,000 --> 00:08:43,000
I need to sit down.

143
00:08:43,000 --> 00:08:49,000
I need to meditate now because this is the only thing that helps

144
00:08:49,000 --> 00:08:55,000
to get me out of here, out of that suffering that there is.

145
00:08:55,000 --> 00:08:58,000
And once you started to sit,

146
00:08:58,000 --> 00:09:08,000
once you got all that point that once you got over that

147
00:09:08,000 --> 00:09:11,000
which was taking you, holding you back,

148
00:09:11,000 --> 00:09:13,000
then it's getting easier.

149
00:09:13,000 --> 00:09:17,000
You don't, for the next time you don't need so much effort anymore

150
00:09:17,000 --> 00:09:21,000
because you learn that actually the sitting in meditation

151
00:09:21,000 --> 00:09:25,000
is beneficial and helpful for you.

152
00:09:25,000 --> 00:09:29,000
It does get better. It does get easier.

153
00:09:29,000 --> 00:09:33,000
I think it's easier to get discouraged thinking that it's not getting better.

154
00:09:33,000 --> 00:09:35,000
It's not getting easier.

155
00:09:35,000 --> 00:09:42,000
And I think it's important for us to actually be able to see the benefit

156
00:09:42,000 --> 00:09:45,000
and to remind ourselves of the benefit.

157
00:09:45,000 --> 00:09:49,000
And that generally our expectations of what we should have gained

158
00:09:49,000 --> 00:09:53,000
from our practice so far is about, say, this big.

159
00:09:53,000 --> 00:09:57,000
And what we've gained is probably maybe about this big if we're lucky.

160
00:09:57,000 --> 00:09:59,000
But that's really something.

161
00:09:59,000 --> 00:10:01,000
It's actually an incredible benefit.

162
00:10:01,000 --> 00:10:05,000
You know, you do a couple of hours of meditation a day,

163
00:10:05,000 --> 00:10:07,000
you know, for a few weeks and then you think,

164
00:10:07,000 --> 00:10:09,000
oh, I'm not enlightened yet.

165
00:10:09,000 --> 00:10:13,000
I'm still getting angry. What's up with this?

166
00:10:13,000 --> 00:10:17,000
And at the same time, you may have gained some of the most profound

167
00:10:17,000 --> 00:10:22,000
experience, profound realizations that could potentially change your life.

168
00:10:22,000 --> 00:10:25,000
You should focus on that.

169
00:10:25,000 --> 00:10:27,000
Maybe that's even getting off track here,

170
00:10:27,000 --> 00:10:31,000
but it is in regards to what you're saying to be encouraged

171
00:10:31,000 --> 00:10:35,000
and to see that meditation does get easier

172
00:10:35,000 --> 00:10:40,000
and to see for yourself that you are actually gaining something from it

173
00:10:40,000 --> 00:10:44,000
to be realistic in seeing that,

174
00:10:44,000 --> 00:10:46,000
you know, I am actually gaining something.

175
00:10:46,000 --> 00:10:49,000
Meditation seems like a horrible thing.

176
00:10:49,000 --> 00:10:53,000
Oh, I have to sit in a sit now for a half an hour, an hour.

177
00:10:53,000 --> 00:10:58,000
And then when you actually do it, it's not so bad.

178
00:10:58,000 --> 00:11:00,000
It's not so bad.

179
00:11:00,000 --> 00:11:01,000
And it's not so bad.

180
00:11:01,000 --> 00:11:03,000
It can be life changing.

181
00:11:03,000 --> 00:11:05,000
You begin to learn.

182
00:11:05,000 --> 00:11:09,000
You've lived a life without any learning about yourself.

183
00:11:09,000 --> 00:11:12,000
Suddenly, you begin to learn about yourself.

184
00:11:12,000 --> 00:11:15,000
And, you know, it's important.

185
00:11:15,000 --> 00:11:17,000
Another thing I'd say is you don't only need media,

186
00:11:17,000 --> 00:11:24,000
but something that goes hand in hand is you need the Kali-anamita.

187
00:11:24,000 --> 00:11:27,000
Or if you don't have one, you need a heck of a lot of media.

188
00:11:27,000 --> 00:11:34,000
But having Kali-anamita is having friends having support.

189
00:11:34,000 --> 00:11:39,000
You know, obviously part of why we're happy to do this sort of thing

190
00:11:39,000 --> 00:11:41,000
is to provide this sort of support.

191
00:11:41,000 --> 00:11:44,000
Obviously, we assume that this is encouraging people to meditate.

192
00:11:44,000 --> 00:11:49,000
They are not just sitting here checking Facebook, coming back to chat

193
00:11:49,000 --> 00:11:51,000
that you're actually interested in meditating.

194
00:11:51,000 --> 00:11:55,000
And this is going to encourage you in meditation.

195
00:11:55,000 --> 00:12:00,000
That might be another thing that can help you find a group,

196
00:12:00,000 --> 00:12:05,000
find a guide, find friends who are interested.

197
00:12:05,000 --> 00:12:10,000
Look around you for a meditation group, for example.

198
00:12:10,000 --> 00:12:16,000
Another sort of piece of advice.

199
00:12:16,000 --> 00:12:21,000
Not just in.

200
00:12:21,000 --> 00:12:27,000
Well, I think that when you hit some sort of crossroads

201
00:12:27,000 --> 00:12:32,000
in life like this, where there's a difficult situation is that

202
00:12:32,000 --> 00:12:37,000
you kind of, you have to go into the unknown.

203
00:12:37,000 --> 00:12:39,000
You have to do what's not sure.

204
00:12:39,000 --> 00:12:44,000
And what this maybe think of was a deeper maw, you know, deeper maw.

205
00:12:44,000 --> 00:12:49,000
Is that, because what got her into meditation is that

206
00:12:49,000 --> 00:12:51,000
she thought she was going to die.

207
00:12:51,000 --> 00:12:55,000
So she gave her child to her neighbor and told her to watch her child

208
00:12:55,000 --> 00:13:00,000
when she went off to a Mahasi Sayadha's meditation center.

209
00:13:00,000 --> 00:13:04,000
And there's like, where I don't know.

210
00:13:04,000 --> 00:13:07,000
She got her start with meditation there.

211
00:13:07,000 --> 00:13:10,000
And now look at everyone who's deep in law and all this

212
00:13:10,000 --> 00:13:14,000
because she chose to go that route.

213
00:13:14,000 --> 00:13:19,000
So you reach the situation where you have to choose to keep on

214
00:13:19,000 --> 00:13:20,000
cleaning.

215
00:13:20,000 --> 00:13:23,000
And if you're Buddhist, even if you haven't had any insight

216
00:13:23,000 --> 00:13:26,000
into anything, if you just understand it intellectually

217
00:13:26,000 --> 00:13:28,000
and you agree with it.

218
00:13:28,000 --> 00:13:29,000
And then you have to go.

219
00:13:29,000 --> 00:13:31,000
And the cleaning is the problem.

220
00:13:31,000 --> 00:13:35,000
You have to go the way of letting go and start crossroads

221
00:13:35,000 --> 00:13:39,000
where you have to be, do I want to continue to continue

222
00:13:39,000 --> 00:13:42,000
the cleaning, which I know is the problem,

223
00:13:42,000 --> 00:13:45,000
or do I want to let go, which I know is the answer.

224
00:13:45,000 --> 00:13:52,000
So you just have to choose the way that you know is the answer.

225
00:13:52,000 --> 00:13:54,000
Make sure.

