1
00:00:00,000 --> 00:00:29,360
Okay, good evening everyone, welcome to our daily number.

2
00:00:29,360 --> 00:00:56,520
When we try to define or explain Buddhism, lots of different ways we can try to sum up Buddhism.

3
00:00:56,520 --> 00:01:06,240
I think you can say that Buddhism is all about suffering, that's a common one, right?

4
00:01:06,240 --> 00:01:17,360
Right here about foreignable truths, I think, you know, want to define a picture of Buddhism

5
00:01:17,360 --> 00:01:27,560
it's suffering, or maybe we think of the goal of Buddhism, and we say Buddhism is all about freedom,

6
00:01:27,560 --> 00:01:35,960
liberation, or more specifically Nirvana, because Buddhism about it's about escaping

7
00:01:35,960 --> 00:01:49,640
some sorrow, or we might look at the quality of Buddhism, and see how Buddhism is different

8
00:01:49,640 --> 00:01:56,320
from faith-based religions, and say Buddhism is all about wisdom, or understanding, or

9
00:01:56,320 --> 00:02:05,880
insight, you might even say Buddhism is all about moderation, or the

10
00:02:05,880 --> 00:02:14,360
middle way, that people who think of the first teaching of the Buddha, where he thought

11
00:02:14,360 --> 00:02:30,680
what he called the middle way, and they think, well that's the essence of Buddhism, different

12
00:02:30,680 --> 00:02:37,280
ways to explain it, we might say, think about the Satipatanas, and think of mindfulness,

13
00:02:37,280 --> 00:02:45,880
say Buddhism is all about mindfulness, it's the Buddha called mindfulness, the aikai and

14
00:02:45,880 --> 00:02:51,080
the manga, and then he devoted a lot of his attention to a lot of his time to practice

15
00:02:51,080 --> 00:03:04,480
your teaching before Satipatanas, before foundations of mindfulness.

16
00:03:04,480 --> 00:03:17,560
But there's one concept, one idea that I think perhaps best sums up the Buddha's teaching

17
00:03:17,560 --> 00:03:32,920
or is most true to the Buddha's intention for what his legacy was to be, if you're

18
00:03:32,920 --> 00:03:44,480
going to, in fact it can be detrimental to try and explain Buddhism as being one simple

19
00:03:44,480 --> 00:03:55,080
concept, so as a gross oversimplification, if we're going to grossly oversimplify the

20
00:03:55,080 --> 00:04:02,360
Buddha's teaching, we should sum it up by, really we should sum it up by the concept of

21
00:04:02,360 --> 00:04:18,640
upper manner, simply because this is how the Buddha would have it, it seems would have

22
00:04:18,640 --> 00:04:26,720
it summed up, and how those who've carried on the Buddha's teaching and compiled a bit

23
00:04:26,720 --> 00:04:36,200
ago, how they would have it summed up, how we know about the Buddha is because his last

24
00:04:36,200 --> 00:04:56,280
words were upper-modenas and bodhites, and everything that arises ceases, all formations

25
00:04:56,280 --> 00:05:01,720
are subject to cessation, means anything that we build up will fall down, anything we cling

26
00:05:01,720 --> 00:05:19,280
to will, will eventually fade away, will lose everything we hold dear, and so we should

27
00:05:19,280 --> 00:05:35,940
up-modenas and bodhites, we've become full or bring to fulfillment or be ever engaged

28
00:05:35,940 --> 00:05:51,100
in upper-modenas, we have an endama-pada, we have verses like upper-modenas, upper-modenas,

29
00:05:51,100 --> 00:06:17,800
upper-modenas, upper-modenas, upper-modenas, upper-modenas, upper-modenas, upper-modenas, the path

30
00:06:17,800 --> 00:06:31,480
Bhamana, which is the opposite, is the path of death.

31
00:06:31,480 --> 00:06:37,360
Those who are upamanda never die.

32
00:06:37,360 --> 00:06:48,320
Those who are upamanda are as though already dead.

33
00:06:48,320 --> 00:06:59,120
Or we have regards to Angulimala, Yopu B. Bhamajik-twaya-pachana-sap-pachason-apamajati.

34
00:06:59,120 --> 00:07:17,960
Who in the past was Bhamada, but in the present becomes upamada, they illuminate like

35
00:07:17,960 --> 00:07:18,960
Angulimala.

36
00:07:18,960 --> 00:07:33,760
They light up the earth, they light up the world, like the moon coming out from behind

37
00:07:33,760 --> 00:07:34,760
the cloud.

38
00:07:34,760 --> 00:07:45,520
It's our mind, our being, it's capable of so much brightness, so much greatness.

39
00:07:45,520 --> 00:07:56,000
When more Bhamanda is shrouded, it's covered, it's obscured.

40
00:07:56,000 --> 00:07:57,000
So how do we understand Bhamada?

41
00:07:57,000 --> 00:08:01,640
It's an interesting question, I like to ask this of my Sri Lankans, because they tell

42
00:08:01,640 --> 00:08:07,600
me, well, upamada means to not be late, it's the scariest thing, how it's changed in

43
00:08:07,600 --> 00:08:15,320
single needs, but I think it comes from the idea that you lose track of time if you're

44
00:08:15,320 --> 00:08:16,320
Bhamanda.

45
00:08:16,320 --> 00:08:23,760
You lose track of yourself, right, that sounds quite Buddhist, doesn't it, when you're

46
00:08:23,760 --> 00:08:30,160
not aware, oh, it's time to go, it's time to do this, it's time to do that, you're

47
00:08:30,160 --> 00:08:31,160
not present.

48
00:08:31,160 --> 00:08:36,640
So you lose track of time and as a result, you're late, something like that.

49
00:08:36,640 --> 00:08:46,360
But no, the best way, the easiest way to understand Bhamada and upamada, Bhamada being

50
00:08:46,360 --> 00:08:55,360
the bad one, upamada being the good one, is when we look at how the Buddha discussed

51
00:08:55,360 --> 00:09:09,720
in toxicants, alcohol, the fifth precept, Suram area, munch, upamada, tana, alcohol, liquor

52
00:09:09,720 --> 00:09:30,200
and booze, liquor and beer, is a pamada tana is a basis for pamada, so the ordinary way of

53
00:09:30,200 --> 00:09:39,560
understanding pamada is that it's intoxicating, intoxication, when we're drunk, that's what

54
00:09:39,560 --> 00:09:40,560
means to be pamada.

55
00:09:40,560 --> 00:09:51,920
It comes from the root munch, like yofu be pamajitwa, pachano, so nah, pamajati, munch, munch means

56
00:09:51,920 --> 00:10:09,480
to be drunk, means to be confused or deluded, clouded in the mind, unclear, but just

57
00:10:09,480 --> 00:10:20,120
turns it into something a little more special, a little more extreme, so pamaj is something

58
00:10:20,120 --> 00:10:27,960
like when you're actually mentally incapacitated, and munch is just the idea of being confused,

59
00:10:27,960 --> 00:10:38,840
I think, but pamaj or pamada, when it becomes, this is when you're truly intoxicated,

60
00:10:38,840 --> 00:10:47,680
so either physically from taking alcohol or intoxicant, or mentally, from engaging in

61
00:10:47,680 --> 00:10:58,480
or being overwhelmed by desire, aversion, delusion, which cloud the mind confused the

62
00:10:58,480 --> 00:11:11,120
mind, destroy the clarity of the mind, and the purity of the mind.

63
00:11:11,120 --> 00:11:23,040
Of course, pamada has everything to do with sati, with mindfulness, but it's saditya,

64
00:11:23,040 --> 00:11:31,640
wipawasu, a pamatoti, wajiti, to never be without sati, this is what it means to

65
00:11:31,640 --> 00:11:42,480
be a pamanda, so we understand pretty clearly, it's not like this is a hard to understand

66
00:11:42,480 --> 00:11:49,720
or an poorly understood concept for those who have studied it, it's not like the texts

67
00:11:49,720 --> 00:11:58,640
are unclear, but it's important that we understand exactly what this is an important concept

68
00:11:58,640 --> 00:12:04,680
to understand, clarity of mind is what we're talking, so pamada means not being clouded,

69
00:12:04,680 --> 00:12:10,640
not being confused, not being intoxicated really, the idea is that the defilements of

70
00:12:10,640 --> 00:12:21,040
the mind intoxicators, they take away our clarity, they keep us from seeing clearly,

71
00:12:21,040 --> 00:12:25,280
so our practice and meditation really, this is what it's all about, we can talk about

72
00:12:25,280 --> 00:12:29,560
mindfulness, this is a good thing to talk about, we can talk about wisdom or insight,

73
00:12:29,560 --> 00:12:39,320
these are good aspects of it, but the real active component is cultivating clarity of mind.

74
00:12:39,320 --> 00:12:50,600
We use mindfulness to do that when you remind yourself seeing pain, pain, thinking, thinking

75
00:12:50,600 --> 00:12:56,800
or whatever, you're actively cultivating clarity of mind, you're trying to remind yourself,

76
00:12:56,800 --> 00:13:05,840
hey, this is what's happening, this is what's present, this is reality, bringing your

77
00:13:05,840 --> 00:13:16,280
mind back, dragging your mind out of the clouds, back down to the mundane objective

78
00:13:16,280 --> 00:13:22,920
reality of our experience, what we're seeing, what we're hearing in every moment,

79
00:13:22,920 --> 00:13:35,200
smelling, tasting, feeling, thinking, the body of the mind.

80
00:13:35,200 --> 00:13:39,920
So there's this idea that we're intoxicated, we're intoxicated, we're intoxicated due to ignorance,

81
00:13:39,920 --> 00:13:47,720
we're intoxicated due to our attachments in our version, we're like drunk people, really

82
00:13:47,720 --> 00:13:57,120
most of the time drunk on life, drunk on youth, drunk on sensual enjoyment, we don't

83
00:13:57,120 --> 00:14:11,880
see clearly, we have very strong delusions, our habits are based on delusion, our reactions

84
00:14:11,880 --> 00:14:18,840
to our experiences are based on delusion, why we like certain things, why we dislike certain

85
00:14:18,840 --> 00:14:28,760
things, why we react violently, either positive or negative to our experiences, because

86
00:14:28,760 --> 00:14:32,960
we are intoxicated, we're drunk.

87
00:14:32,960 --> 00:14:38,000
And so our meditation practice is about sobering up, it's about having this pure state

88
00:14:38,000 --> 00:14:45,960
of mind, this clear state of mind, that's the active component, that's what changes

89
00:14:45,960 --> 00:15:01,320
us, that's what lifts us out of the quagmire, lifts us out of the morass of the delusion

90
00:15:01,320 --> 00:15:06,080
that we're caught in.

91
00:15:06,080 --> 00:15:11,320
So if you want to, I mean the use of this is to help us understand or help us get a sense

92
00:15:11,320 --> 00:15:16,600
of how meditation should be experienced, what should it feel like to meditate, how do you

93
00:15:16,600 --> 00:15:19,440
know if you're truly meditating?

94
00:15:19,440 --> 00:15:29,800
And it's the difference between following your delusions and seeing them clearly.

95
00:15:29,800 --> 00:15:35,800
Once you start to see your desires, your aversion, your confusion, your delusion, your doubt,

96
00:15:35,800 --> 00:15:45,240
your worry, your stress, once you start to see it clearly, and you start to be present,

97
00:15:45,240 --> 00:15:50,080
it's like waking up, you feel like you lived your life asleep and you're only just waking

98
00:15:50,080 --> 00:15:58,840
up, it's like you lived your life drunk and you've only just sobered up.

99
00:15:58,840 --> 00:16:03,640
And that's really, I mean again there are many ways and many important aspects of the Buddha's

100
00:16:03,640 --> 00:16:09,480
teaching, but the last words of the Buddha, what the commentary says is the whole of the

101
00:16:09,480 --> 00:16:15,720
tepitika, all these thousands of teachings of the Buddha, when you sum it up, it's the path,

102
00:16:15,720 --> 00:16:25,560
or the way and the means to become a pamadha, to become sober, un-toxicated.

103
00:16:25,560 --> 00:16:38,040
And so along with all the other aspects of the teaching, this is what we see as the essence

104
00:16:38,040 --> 00:16:45,400
of Buddhism, because it leads to all of the good things, and it's the use of mindfulness

105
00:16:45,400 --> 00:16:53,360
to create clarity of mind which brings about morality, concentration, wisdom, and understanding

106
00:16:53,360 --> 00:17:04,960
and freedom and release, liberation from suffering.

107
00:17:04,960 --> 00:17:08,840
So to be clear, this is what our meditation should bring, it shouldn't just make you calm

108
00:17:08,840 --> 00:17:15,920
or make you feel good or reinforce your beliefs or your ideas, meditation should help

109
00:17:15,920 --> 00:17:21,440
you, should make this shift where you start to see things clearly, you start to understand

110
00:17:21,440 --> 00:17:30,600
yourself and see what you've overlooked, start to see what's in front of you, and see

111
00:17:30,600 --> 00:17:35,920
the nature of your own mind, and the nature of the world around you and others imperman

112
00:17:35,920 --> 00:17:44,800
in suffering and non-self, and you begin to let go, and begin to give up all these bad

113
00:17:44,800 --> 00:17:54,880
habits that are like darkness, and in darkness grow many unpleasant things, mushrooms,

114
00:17:54,880 --> 00:18:02,880
bacteria, mold, but when you shine the light, when the sun shines down, it all dries

115
00:18:02,880 --> 00:18:12,720
up and cleans up and becomes purified, purified by the light, all these bad habits, all

116
00:18:12,720 --> 00:18:18,720
these unwholesome qualities of mind are dried up through the power of mindfulness, through

117
00:18:18,720 --> 00:18:31,240
the power of clarity, so something for us all to remember, to think about, and to keep

118
00:18:31,240 --> 00:18:37,880
in mind when we practice, that our goal is clarity of mind, so there you go, that's the

119
00:18:37,880 --> 00:19:07,400
demo for tonight, thank you all for tuning in, wish you all the best, and thank you for

120
00:19:07,400 --> 00:19:13,280
an end, I was trying to remember what that was, I was just thinking tonight, because Doug

121
00:19:13,280 --> 00:19:21,880
I think asked me, how would you describe the Buddha's last words, if you were to sum them

122
00:19:21,880 --> 00:19:29,080
up in an easy to understand way, what was it, wasn't it, stuff breaks sober up?

123
00:19:29,080 --> 00:19:59,000
You know, I think I was asked, well yeah, I remember that, stuff breaks, and I was

124
00:19:59,000 --> 00:20:16,440
actually not that tough, life is actually quite easy when you're truly sober, it's just

125
00:20:16,440 --> 00:20:45,920
even when we're sober we're still drunk, what is the technical reason behind singling

126
00:20:45,920 --> 00:21:02,480
out craving as the cause of suffering and leaving out aversion, it's a good question, I

127
00:21:02,480 --> 00:21:07,320
can think of two reasons, I mean this is a common question, I'm sure someone else, someone

128
00:21:07,320 --> 00:21:16,200
better than me could come up with a very good orthodox response, but first, aversion is suffering,

129
00:21:16,200 --> 00:21:23,720
an aversion mine state is, I mean this is a very technical, probably a very orthodox answer,

130
00:21:23,720 --> 00:21:33,720
is that aversion mines with aversion are always accompanied by suffering, you can't have aversion

131
00:21:33,720 --> 00:21:42,600
without actually suffering, so it's not, it's not the cause of suffering, it is suffering,

132
00:21:42,600 --> 00:21:50,880
in fact the only mental suffering is an aversion mine, but craving is never associated

133
00:21:50,880 --> 00:21:58,880
with suffering, the craving mine never has mental suffering associated with it, so rather

134
00:21:58,880 --> 00:22:05,600
than being suffering it's considered to be the cause of suffering, it's one reason, another

135
00:22:05,600 --> 00:22:14,960
reason perhaps more practical or less technical answer is that I have two more things to

136
00:22:14,960 --> 00:22:32,600
say about it, what the first is that craving is much more the about our goals, craving

137
00:22:32,600 --> 00:22:42,640
is, aversion is somehow based on craving still, because it's based on being not what

138
00:22:42,640 --> 00:22:58,920
you want, we have the things, the idea being our goals in life and we don't generally

139
00:22:58,920 --> 00:23:06,760
have goals, I don't know, maybe that's not such a good answer, but I think there is a sense

140
00:23:06,760 --> 00:23:18,640
that craving is much more the essence of what defines our goals in life, right, so it's what

141
00:23:18,640 --> 00:23:28,000
leads us to do things, but the other one is that even in done, you still have, we

142
00:23:28,000 --> 00:23:36,760
bubble it down, which could be construed as referring to aversion, the desire for something

143
00:23:36,760 --> 00:23:40,320
not to be, now the commentary doesn't really do this, the commentary is pretty clear

144
00:23:40,320 --> 00:23:46,640
that it's, I don't know, the commentary seems to suggest anyway that it's a special

145
00:23:46,640 --> 00:23:52,640
type of craving, but we bubble it down, does seem to be the desire for something not to

146
00:23:52,640 --> 00:24:05,240
be, yeah, wanting to do something cruel to be seen as craving, but I don't know, I mean

147
00:24:05,240 --> 00:24:10,320
wanting something not to exist as much more simple, when you feel pain wanting for it

148
00:24:10,320 --> 00:24:23,640
to go away is what makes you angry, which makes you upset about it, I mean it's really

149
00:24:23,640 --> 00:24:35,960
just, I mean the point is when you die, you still have some kind of attachment, some

150
00:24:35,960 --> 00:24:43,480
kind of ambition, whether it be for existence or non-existence, you have some kind of ambition

151
00:24:43,480 --> 00:24:46,960
is probably a pretty good English word, it helps us understand that you have some

152
00:24:46,960 --> 00:24:53,160
drive, some goal, and that's what leads you to be reborn again, and so the whole idea

153
00:24:53,160 --> 00:25:13,280
is that Tanhai is the only thing that is capable of creating another birth, our emotions

154
00:25:13,280 --> 00:25:24,360
Nama and Rupa, emotions are Nama, so for example if you are angry, well the anger is

155
00:25:24,360 --> 00:25:40,640
going to have a physical effect, but the anger is still mental, if you're worried, the

156
00:25:40,640 --> 00:25:46,080
worry is mental, but it'll have physical consequences, so you'll feel tense in the body,

157
00:25:46,080 --> 00:25:52,800
but the tension isn't worried, the worry causes the tension, that's an important distinction

158
00:25:52,800 --> 00:25:57,800
to me, it's important to know that these are mental, that when you're tense in the body

159
00:25:57,800 --> 00:26:01,120
that's not a sign that you're worried, it's a sign that you were worried and now you're

160
00:26:01,120 --> 00:26:25,680
concerned, but everything, pretty much everything is mental, the only thing that is physical

161
00:26:25,680 --> 00:26:34,840
is the experience of the four elements, tension, the acidity, hardness, softness, heat

162
00:26:34,840 --> 00:27:02,760
and cold, that's really about it, that's physical.

163
00:27:02,760 --> 00:27:32,540
All right.

164
00:27:32,540 --> 00:27:41,660
Alright, well if that's it, and thank you all. Have a good night. See you all next time.

165
00:27:41,660 --> 00:27:47,620
Probably tomorrow that we'll see. Tomorrow I've got a long day in the teaching meditation

166
00:27:47,620 --> 00:28:00,340
all day in the university at 5 minute meditation lesson. I'll see how it goes. I'll be

167
00:28:00,340 --> 00:28:07,660
here if I get all my work done. Okay, have a good night.

