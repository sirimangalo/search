1
00:00:00,000 --> 00:00:04,580
Hey, good evening, everyone.

2
00:00:04,580 --> 00:00:29,920
We're broadcasting live, remarks, women, today's quote is about sand on the finger now.

3
00:00:29,920 --> 00:00:48,880
I'm going to like to not like to do the Buddha, was accustomed to, or was, he was really good

4
00:00:48,880 --> 00:01:07,200
at finding examples in nature, and coming up with comparisons between the nature of the world,

5
00:01:07,200 --> 00:01:19,080
and the nature, the natural world, and the nature of reality, so I'm able to show, similar.

6
00:01:19,080 --> 00:01:30,680
I'm using simple examples in nature, so it's very common, common tool, so one day he picked

7
00:01:30,680 --> 00:01:45,600
up a few grains of sand on his finger now, and he asked Ananda, Ananda, he asked a bunch of

8
00:01:45,600 --> 00:01:58,400
because, yes, because Tunging, Mani, Tungi, Kewe, Kata, Manu, Koo, Baha'u, Tarang, tell me what you

9
00:01:58,400 --> 00:02:09,080
think, monks, which is more, you know, Ayyam, Maia, Barito, Nakasik, Ayyam, Tung, Su, Aru, Pitua,

10
00:02:09,080 --> 00:02:23,040
Ropit, Po, the little bit of sand that I've put on my finger now, Waha, or Ayyam,

11
00:02:23,040 --> 00:02:28,920
Maia, but the, or the scrader.

12
00:02:28,920 --> 00:02:41,960
He used the simile for different things, in one place he talks about, not exactly the simile,

13
00:02:41,960 --> 00:03:02,080
but he says, this, even a little bit of, there's one place where he talks about a little

14
00:03:02,080 --> 00:03:10,840
bit of feces, a little bit of manure cow down, or human down, excrement, a little bit

15
00:03:10,840 --> 00:03:11,840
of excrement.

16
00:03:11,840 --> 00:03:17,480
You see, if you have a little bit of excrement, is it any better than a lot of excrement?

17
00:03:17,480 --> 00:03:24,280
It's a little bit of excrement, suddenly beautiful, suddenly wonderful, and I said, no,

18
00:03:24,280 --> 00:03:31,800
even a little bit of excrement is a bad thing, and the Buddha said it on the same way, even

19
00:03:31,800 --> 00:03:42,000
a little bit of suffering, a little bit of samsara, a little bit of rebirth, is, it's

20
00:03:42,000 --> 00:03:48,680
still bad, still not the way, still not a goal, or something you should look for, should

21
00:03:48,680 --> 00:03:49,680
seek for.

22
00:03:49,680 --> 00:03:57,880
But here he uses this little bit to say that's like the number of beings that are born

23
00:03:57,880 --> 00:04:04,720
as humans, being born as a human is a rare thing, the number of beings that are born

24
00:04:04,720 --> 00:04:24,880
as animals, as insects, or rodents, birds, and even the higher mammals, fish, all these

25
00:04:24,880 --> 00:04:32,120
various types of animals, it's like the greater, it's far, far more than the only

26
00:04:32,120 --> 00:04:52,960
few human, and even what he doesn't say here, but what he says elsewhere is, up a kat-de-manu-sei-su-ji-jana-paraganu,

27
00:04:52,960 --> 00:05:01,200
you are a few of the humans who actually make it to a good destination, who actually headed

28
00:05:01,200 --> 00:05:04,120
in the right direction.

29
00:05:04,120 --> 00:05:09,720
So being born as a human being is like winning a lottery, and then most of us just

30
00:05:09,720 --> 00:05:23,480
ponder it, waste it, it's like just a coincidence, and then it's gone, go back into the

31
00:05:23,480 --> 00:05:41,640
pool of some sara, so there are many ways that we have to remember to take advantage

32
00:05:41,640 --> 00:05:48,200
of being born as a human being, we have to act like human beings, there are human beings

33
00:05:48,200 --> 00:05:56,920
that act like hell beings, there are human beings that act like ghosts, there are human

34
00:05:56,920 --> 00:06:03,600
beings that act like animals, and that's where they're going, that's where they're heading.

35
00:06:03,600 --> 00:06:15,480
Someone who's full of anger and hatred, it's like there are a demon on earth, a person

36
00:06:15,480 --> 00:06:23,400
who's full of greed, one thing is, one thing that can overcome their one thing, it's

37
00:06:23,400 --> 00:06:39,520
like a ghost, a hungry ghost, wailing, one thing never satisfied, like in the Christmas

38
00:06:39,520 --> 00:06:53,080
Carol, Marily, Scrooge's partner, was destined to walk fierce, never satisfied, because

39
00:06:53,080 --> 00:06:58,880
he was full of greed when he was alive.

40
00:06:58,880 --> 00:07:11,160
Things are full of delusion, arrogant and conceited, just like cats, cats are so arrogant

41
00:07:11,160 --> 00:07:21,200
and conceited, there are many of them, and dogs are just ignorant and kind of dumb.

42
00:07:21,200 --> 00:07:31,520
So if we're keen on ignorance and arrogance and conceited and all these things, if we're

43
00:07:31,520 --> 00:07:40,920
caught up in drugs and alcohol, deluding and defuttling our minds, it's just like being

44
00:07:40,920 --> 00:07:51,120
an animal, it's like being an animal on a human animal, but then there are other humans

45
00:07:51,120 --> 00:07:57,880
that are like angels, humans like that are like gods, there are other humans that are

46
00:07:57,880 --> 00:08:07,520
like good does, and these are the ones that we aim for, and there are ways as a human

47
00:08:07,520 --> 00:08:15,400
being to be like an angel or a god or even a Buddha, it may not be a Buddha, but we can

48
00:08:15,400 --> 00:08:27,880
be like one, we follow the Buddha's example, we cultivate mindfulness and clarity and wisdom,

49
00:08:27,880 --> 00:08:37,600
we have to be mindful of our position as he is being, I've been getting calls lately,

50
00:08:37,600 --> 00:08:42,920
and I have to say something, someone calls, it's really, religion really does strange

51
00:08:42,920 --> 00:08:52,600
things to people, spirituality, sometimes it's just kind of funny, because people come

52
00:08:52,600 --> 00:09:00,000
up to me all the time and say the wackiest things, and the emails I get, and now the phone

53
00:09:00,000 --> 00:09:10,200
calls I'm getting, but sometimes it's, you have to be mindful of yourself, or someone

54
00:09:10,200 --> 00:09:15,680
call me up today, not to really, you know, not to get upset or anything, but just to

55
00:09:15,680 --> 00:09:22,280
talk about this, call me up today and just ask if this wasn't the minute, the international

56
00:09:22,280 --> 00:09:33,400
meditation center, I think, and without introducing himself, without asking how I was,

57
00:09:33,400 --> 00:09:39,960
if I've got time to talk and answer questions, just start asking me questions about monks

58
00:09:39,960 --> 00:09:48,760
and why monks weren't keeping rules and why I'm not doing my videos, and I didn't

59
00:09:48,760 --> 00:09:58,400
want to answer him, I didn't feel comfortable, because I didn't feel like he was mindful,

60
00:09:58,400 --> 00:10:07,560
that's not how you start a conversation, when people come up, when someone comes to you,

61
00:10:07,560 --> 00:10:12,960
when you approach someone, you should ask how they are, you should introduce yourself,

62
00:10:12,960 --> 00:10:17,200
and you should be mindful of the fact that you don't know this person, and so to ask

63
00:10:17,200 --> 00:10:24,360
them deep questions about this, you know, not deep, but personal questions really about

64
00:10:24,360 --> 00:10:33,880
monastic life and why monks this and that, without ever even telling me your name, and

65
00:10:33,880 --> 00:10:40,840
ask this guy, do I know who you are, and anyway, it wasn't a big, I just, I thought

66
00:10:40,840 --> 00:10:46,960
afterwards that, because I didn't answer and I was just quite quiet and he started laughing

67
00:10:46,960 --> 00:10:58,880
at me, and you know, I just, that's past, that's not problem, I'm not trying to come on

68
00:10:58,880 --> 00:11:08,040
your event, but I know this, this kind of attitude, sometimes we're not mindful, we have

69
00:11:08,040 --> 00:11:14,320
to be mindful, because I think it's because it's out of our realm, you know, we get good

70
00:11:14,320 --> 00:11:22,000
at, we're good at being human in our, in the frame of reference that we have, so when

71
00:11:22,000 --> 00:11:29,960
we go to work or school, we get good at playing the game, so we get good at saying the

72
00:11:29,960 --> 00:11:36,560
right things, and as a monk guy, I am proficient at being a monk and proficient at it,

73
00:11:36,560 --> 00:11:43,040
you know, but when you're taken out of your, your comfort, so it's familiar, and you

74
00:11:43,040 --> 00:11:51,560
don't, you can't rely upon habit, I think this goes for a lot of situations, you know,

75
00:11:51,560 --> 00:11:59,320
when you're sick, people can be very irritable and, and unmindful when they're sick, because

76
00:11:59,320 --> 00:12:05,200
it's a situation that they're not accustomed to, when we're surrounded by people or

77
00:12:05,200 --> 00:12:15,720
when we're in public, or this is the, by the importance of understanding mindfulness, it's

78
00:12:15,720 --> 00:12:20,400
not, it's quite different from habit, it's different from concentration, it's not something

79
00:12:20,400 --> 00:12:24,520
you can build up, it's something you have to be, when you build it up, but you build

80
00:12:24,520 --> 00:12:32,440
it as a skill, and then you use it, you have to use it, you have to be mindful, to remember

81
00:12:32,440 --> 00:12:43,880
yourself, and so this is, let this be a lesson, it's an interesting lesson, acting, acting

82
00:12:43,880 --> 00:12:53,400
appropriately, when you talk to people, and engage with people, this is kind of, kind of

83
00:12:53,400 --> 00:13:00,400
a thing that's not related to being, not related to meditation, but also very much related

84
00:13:00,400 --> 00:13:06,680
to meditation, it means how we behave in the world around us, the things we do, the things

85
00:13:06,680 --> 00:13:14,360
we say, it's not meditation, and people often ask me questions about, what should I do

86
00:13:14,360 --> 00:13:24,400
in this situation, how should I behave as a human being in this situation, in a situation,

87
00:13:24,400 --> 00:13:29,160
and it's, these are difficult questions to answer, people are expecting a solution to

88
00:13:29,160 --> 00:13:36,280
your problems, but it's very much, you know, so it's unrelated to meditation, but very much

89
00:13:36,280 --> 00:13:41,720
related to meditation, because I can't give you the answer, that's not how it works,

90
00:13:41,720 --> 00:13:52,080
the, the, the requirement is a foundation that is capable of dealing with your own problem,

91
00:13:52,080 --> 00:13:58,200
you have to find a solution for yourself, that's the only way it works, like tell you

92
00:13:58,200 --> 00:14:04,440
the answer, and then you go and do this or do that, it's, it's artificial, you see, and

93
00:14:04,440 --> 00:14:11,760
whatever you say or whatever you do, it's not from the heart, it's not really, so it requires

94
00:14:11,760 --> 00:14:21,040
mindfulness as a basis, it's why it's so important, you want to truly be a human, truly

95
00:14:21,040 --> 00:14:28,880
live as a human being, you need mindfulness as the basis, always, always come back to mind,

96
00:14:28,880 --> 00:14:43,920
or sati, mindfulness may not be the best way, right, I was, I meditated earlier, and

97
00:14:43,920 --> 00:14:48,960
then somehow the timer didn't work, so I clicked it late, so I think it says I'm just

98
00:14:48,960 --> 00:14:55,760
finishing meditation, I, I did my hour, if you're wondering why I'm supposed to be meditating

99
00:14:55,760 --> 00:15:00,600
when I'm doing it, because we've got everybody signs in for when they're going to meditate,

100
00:15:00,600 --> 00:15:11,040
and I was doing meditation an hour, an hour, an hour ago, and then I didn't click for

101
00:15:11,040 --> 00:15:18,920
some reason, so I, after I did walking, I checked it, and then I clicked it, so it's a half an

102
00:15:18,920 --> 00:15:27,600
hour late, I'm not just clicking and they're not meditating, that's the danger of it

103
00:15:27,600 --> 00:15:42,480
is, can't tell whether people are actually meditating, I don't know why anyone would, anyway,

104
00:15:42,480 --> 00:15:54,400
so yeah, that's the quote for this evening, it's given a little power, it's the power,

105
00:15:54,400 --> 00:16:06,480
a bit of a change, even so amongst few are those beings that are reborn as humans,

106
00:16:25,960 --> 00:16:33,200
tasmati habikhui ai wang sikitabung, therefore, amongst here you should train

107
00:16:33,200 --> 00:16:39,880
das apamata wiharisam.

108
00:16:39,880 --> 00:16:52,560
We will dwell with apamata apamata, not intoxicated, not confused in the mind,

109
00:16:52,560 --> 00:16:54,920
mindful basically.

110
00:16:54,920 --> 00:17:01,480
A-1-hihi wiharisikitabung, thus should you train for bikhus.

111
00:17:01,480 --> 00:17:05,720
This is the san-yutani kaya.

112
00:17:05,720 --> 00:17:09,880
Nakasikasut, the fingernail.

113
00:17:09,880 --> 00:17:14,820
So, Nakasika, what is sikka, Nakka?

114
00:17:14,820 --> 00:17:20,960
Nakka is fingernail, sikka.

115
00:17:20,960 --> 00:17:24,380
The top, the tip.

116
00:17:24,380 --> 00:17:30,680
Nakasikka is the tip of the fingernail.

117
00:17:30,680 --> 00:17:42,120
So, that's the dhamma for tonight, and if people have questions, I'm going to now post

118
00:17:42,120 --> 00:17:43,120
the hangouts.

119
00:17:43,120 --> 00:17:50,200
So, you're welcome to come and ask questions there, and I expect anyone who comes on

120
00:17:50,200 --> 00:17:57,640
to be mindful and to introduce themselves and to be respectful and mindful of the sorts

121
00:17:57,640 --> 00:18:00,280
of questions that should be asked.

122
00:18:00,280 --> 00:18:08,760
Another that I've scared everyone away, and there's come on and sure.

123
00:18:08,760 --> 00:18:20,040
It's just strange that people, there's a certain familiarity where people just come up to

124
00:18:20,040 --> 00:18:25,480
me and start talking with someone at university.

125
00:18:25,480 --> 00:18:33,520
I think I mentioned this came up to me and said, look, you can't walk around wearing

126
00:18:33,520 --> 00:18:38,600
that kind of clothes and not have people want to come up and ask you about it, and expect

127
00:18:38,600 --> 00:18:40,240
people to not come up and ask you.

128
00:18:40,240 --> 00:18:50,640
And I turned to her and said, why not?

129
00:18:50,640 --> 00:18:52,560
With IP, I turned out to be quite nice.

130
00:18:52,560 --> 00:18:59,360
He was actually quite respectful.

131
00:18:59,360 --> 00:19:05,680
Recently, another person called us from a radio station, and they want to come by tomorrow.

132
00:19:05,680 --> 00:19:12,680
They're going to come by an 11th world, which is bad timing, it's right when we're eating,

133
00:19:12,680 --> 00:19:20,320
but when they want to ask us about our program, you're basically to give us free publicity,

134
00:19:20,320 --> 00:19:26,320
which is awesome, very kind of.

135
00:19:26,320 --> 00:19:31,400
So, there are people who are starting to find out about us, they just have to be ready

136
00:19:31,400 --> 00:19:32,400
for it.

137
00:19:32,400 --> 00:19:37,840
And I think I have to start screening my calls, because this person wasn't very happy

138
00:19:37,840 --> 00:19:43,680
with me in the end, I don't think it wasn't answering.

139
00:19:43,680 --> 00:19:53,800
It's common for a lot of stories of our hands, even, who wouldn't answer questions, they

140
00:19:53,800 --> 00:19:54,800
just wouldn't say anything.

141
00:19:54,800 --> 00:20:02,440
When a question or when someone was saying something to them, that just wasn't, it's because

142
00:20:02,440 --> 00:20:11,520
of indifference, monks are not like the Buddha, so I've done this before, and people

143
00:20:11,520 --> 00:20:15,960
can be very upset at you for not answering their questions, because they're so used to,

144
00:20:15,960 --> 00:20:23,840
I mean, social, this is the social way, is to have someone, when you talk to someone,

145
00:20:23,840 --> 00:20:27,040
and you should answer them.

146
00:20:27,040 --> 00:20:32,200
And so this person was saying today, it's just an interesting point.

147
00:20:32,200 --> 00:20:40,760
He was saying, I've talked to a lot of people, and I've never had someone not answer,

148
00:20:40,760 --> 00:20:46,960
just not say anything, and I didn't reply to that, and then he said, I'm not sure

149
00:20:46,960 --> 00:20:56,360
it's what the Buddha would have done, or basically would have wanted or something, basically

150
00:20:56,360 --> 00:21:01,160
criticizing and saying, you know, this is what you're doing this wrong.

151
00:21:01,160 --> 00:21:03,600
But you know, the funny thing is, why do I care?

152
00:21:03,600 --> 00:21:05,560
Why does it matter to me what you think?

153
00:21:05,560 --> 00:21:06,560
It doesn't.

154
00:21:06,560 --> 00:21:13,040
I mean, something about being among that we're very much outside of this.

155
00:21:13,040 --> 00:21:15,840
I didn't come to you, you called me.

156
00:21:15,840 --> 00:21:22,560
So this is, it's an interesting point for me as a mountain to be in this sort of situation,

157
00:21:22,560 --> 00:21:27,160
but one interesting aspect of it is the power that we have.

158
00:21:27,160 --> 00:21:33,040
We're not required to, you know, how you have sometimes you play into other people's

159
00:21:33,040 --> 00:21:37,840
games, bait, you know, bait and switch.

160
00:21:37,840 --> 00:21:42,840
You've heard of this idea of bait and switch, or someone bait, see what's something,

161
00:21:42,840 --> 00:21:48,720
and then if you get into it with them, then they start on something else, and they drag

162
00:21:48,720 --> 00:22:02,280
you on, and into something that's often quite unwholesome and useless speed, and not

163
00:22:02,280 --> 00:22:07,720
have, we don't have the responsibility to play other people's games.

164
00:22:07,720 --> 00:22:11,920
I guess is the general point that it makes, and it's interesting to me.

165
00:22:11,920 --> 00:22:21,080
I think as a monk, I have that luxury not having to play into people's games, and it's

166
00:22:21,080 --> 00:22:28,880
really a power of having left the world, because there's something you can do to me.

167
00:22:28,880 --> 00:22:31,240
I'm happy to help.

168
00:22:31,240 --> 00:22:38,880
I tell a lot of this guy, if you want to learn meditation and I'm happy to help, that

169
00:22:38,880 --> 00:22:50,560
was really interesting, anyway.

170
00:22:50,560 --> 00:23:01,720
We have a great power and mindfulness in not worrying, not clinging, not needing anything

171
00:23:01,720 --> 00:23:10,520
from other people.

172
00:23:10,520 --> 00:23:13,720
Anyway, enough talk.

173
00:23:13,720 --> 00:23:14,720
Thank you all.

174
00:23:14,720 --> 00:23:21,720
I guess there's no questions, so that's all for this evening, I'm sure you are on good practice.

175
00:23:21,720 --> 00:23:36,720
Thank you.

