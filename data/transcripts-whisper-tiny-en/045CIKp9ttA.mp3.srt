1
00:00:00,000 --> 00:00:09,400
okay good evening everyone again still training at the audio the proper volume

2
00:00:09,400 --> 00:00:16,080
hopefully it's better tonight let me know if it's not

3
00:00:16,080 --> 00:00:42,940
the first set of dumbas in the Satipatanas would when we get to this section on

4
00:00:42,940 --> 00:00:55,960
dumbas the first set is the hindrances and you've got to think there's a reason

5
00:00:55,960 --> 00:01:04,760
for that before all of their dumbas for someone who's practicing Satipatanas

6
00:01:04,760 --> 00:01:17,720
practicing mindfulness the first thing outside of body feelings the mind is the

7
00:01:17,720 --> 00:01:24,680
hindrances the things that will get in your way certainly whatever the reason

8
00:01:24,680 --> 00:01:30,380
the actual reason for that is it certainly is useful to give them to

9
00:01:30,380 --> 00:01:36,140
meditators in the beginning because that's when of course they're the strongest

10
00:01:39,540 --> 00:01:48,660
our ordinary relationship with our emotions is problematic it's backwards

11
00:01:48,660 --> 00:02:02,820
really when we like something or we want something we take it as evidence or

12
00:02:02,820 --> 00:02:10,580
an indication that we need to get something that we should obtain the object or

13
00:02:10,580 --> 00:02:23,860
desire when we dislike something or angry at something we take it as an

14
00:02:23,860 --> 00:02:45,580
indication that we should do whatever we can to get rid of it when we are

15
00:02:45,580 --> 00:02:53,060
tired we take it as an indication that we should sleep when we're distracted

16
00:02:53,060 --> 00:02:59,420
or restless we take it as a sign that we should get up and do something not

17
00:02:59,420 --> 00:03:08,820
even intellectually we just this is we react our whole being is conditioned to

18
00:03:08,820 --> 00:03:17,540
react in a specific way react in this way when we are confused when we have

19
00:03:17,540 --> 00:03:23,420
doubt about something we have taken as indication we should stop stop what we're

20
00:03:23,420 --> 00:03:35,140
doing reject it and so we live our lives trying to fit the world around our

21
00:03:35,140 --> 00:03:43,340
expectations around our personality right how many times have we made decisions

22
00:03:43,340 --> 00:03:48,500
based on our personality based on our likes and dislikes we think that's

23
00:03:48,500 --> 00:03:53,980
natural right if you don't like something why would you seek it out if you

24
00:03:53,980 --> 00:04:00,900
like something of course that's where you should that's where you should focus

25
00:04:00,900 --> 00:04:10,340
your effort unfortunately there's the results of this are not as positive as we

26
00:04:10,340 --> 00:04:19,740
might think when you chase after what you want when you like you find yourself

27
00:04:19,740 --> 00:04:25,900
wound tighter and tighter bound tighter and tighter to that are those things

28
00:04:25,900 --> 00:04:31,660
that you like and your happiness becomes more and more dependent on them this

29
00:04:31,660 --> 00:04:37,060
is how the addiction cycle works and we're angry averse to things we become

30
00:04:37,060 --> 00:04:45,820
vulnerable to them more and more averse more and more fragile or vulnerable

31
00:04:45,820 --> 00:04:52,740
to their arising more and more susceptible to anger disappointment

32
00:04:52,740 --> 00:04:58,740
displeasure when they arise or when they can't be removed

33
00:04:58,740 --> 00:05:06,980
when we're tired if we just sleep whenever we're tired contrary to popular

34
00:05:06,980 --> 00:05:12,460
opinion that doesn't actually give you more energy a real problem with this

35
00:05:12,460 --> 00:05:17,340
for meditators who come to practice because psychologically they have this

36
00:05:17,340 --> 00:05:23,740
idea that if you're tired you should sleep and that somehow makes you less of a

37
00:05:23,740 --> 00:05:31,420
tired person and in fact it's it's somewhat the opposite the more quick you

38
00:05:31,420 --> 00:05:40,180
are to give in to sleep the more tired you become as an individual to some

39
00:05:40,180 --> 00:05:46,860
extent of course depending on your workload your body does need sleep but not to

40
00:05:46,860 --> 00:05:54,340
get tired and and if you're very very mindful and you'd far less sleep and you're

41
00:05:54,340 --> 00:06:01,340
far less susceptible to drowsiness than most people

42
00:06:04,620 --> 00:06:09,300
if you're restless and you just get up and go you become restless you become

43
00:06:09,300 --> 00:06:16,620
distracted as an individual reacting reacting to our emotions trying to fit

44
00:06:16,620 --> 00:06:21,500
the world around our emotions rather than learn to have our emotions fit the

45
00:06:21,500 --> 00:06:23,820
world

46
00:06:26,340 --> 00:06:32,180
and to see how our emotions are really the biggest reason for us not being

47
00:06:32,180 --> 00:06:39,900
able to fit into the world properly without stress or suffering and we have

48
00:06:39,900 --> 00:06:44,620
doubt or confusion for always doubting then we just become someone who doubts

49
00:06:44,620 --> 00:06:50,100
everything we can never accomplish anything of any great significance because

50
00:06:50,100 --> 00:06:59,860
the habit of doubt comes up and unless we're able to see quick results well even

51
00:06:59,860 --> 00:07:04,620
if we are in fact we find ourselves doubting even those things that are right in

52
00:07:04,620 --> 00:07:07,540
front of us clearly observable

53
00:07:07,540 --> 00:07:12,860
this happens for meditators they'll come they'll get good results and then

54
00:07:12,860 --> 00:07:16,540
they'll start doubting one day they'll have great results and be sure of the

55
00:07:16,540 --> 00:07:21,620
practice next day they forget all about those results and the doubt comes back

56
00:07:21,620 --> 00:07:26,860
and the doubt overrides the good results

57
00:07:26,860 --> 00:07:38,740
so we look at these differently most of these are most of our emotions the

58
00:07:38,740 --> 00:07:46,460
ones mentioned anyway our hindrances the point being there are positive

59
00:07:46,460 --> 00:07:53,780
emotions there are good emotions but they're also bad emotions and we don't

60
00:07:53,780 --> 00:08:03,740
take it that our emotions are or should be or are the proper impetus for

61
00:08:03,740 --> 00:08:13,940
action for speech even for thought we rather wish to cultivate emotions that

62
00:08:13,940 --> 00:08:26,980
are conducive to wholesome speech and deeds thought and some meditation as

63
00:08:26,980 --> 00:08:35,260
with all other things it's the act of retrospective reflection looking back on

64
00:08:35,260 --> 00:08:43,900
yourself rather than allowing the emotions to guide us we guide ourselves back

65
00:08:43,900 --> 00:08:48,580
to the emotions and we learn about them we study them because there's another

66
00:08:48,580 --> 00:08:54,060
way that people deal with emotions they reject them they suppress them they try

67
00:08:54,060 --> 00:09:01,340
to avoid them desires bad okay I'll just get really upset every time I want

68
00:09:01,340 --> 00:09:06,380
something anger's bad well feel really bad about that then I'll get angry at

69
00:09:06,380 --> 00:09:12,220
the fact that I'm angry if I'm tired while then I'll try to force myself to

70
00:09:12,220 --> 00:09:21,980
stay away tire myself out by working really hard if I'm distracted while

71
00:09:21,980 --> 00:09:32,500
then I'll force myself I'll exert myself with lots of effort I'll apply the

72
00:09:32,500 --> 00:09:36,700
effort to suppress the effort so that might work you can somehow stop

73
00:09:36,700 --> 00:09:43,620
yourself from being distracted and if I am doubt we'll just force myself to

74
00:09:43,620 --> 00:09:50,980
believe which of course is the best way to cultivate long-term doubt doesn't

75
00:09:50,980 --> 00:09:57,100
help the doubt at all and the long-term it just makes you realize how you

76
00:09:57,100 --> 00:10:04,860
never really understood or had any good basis for confidence it prevents you

77
00:10:04,860 --> 00:10:07,740
from gaining true confidence we've gained would absolutely gain true

78
00:10:07,740 --> 00:10:13,260
confidence and helps overcome all of these really his observation of them so

79
00:10:13,260 --> 00:10:18,460
as for the hindrances the Buddha called them hindrances but like all the

80
00:10:18,460 --> 00:10:21,500
other things in the Saty bitan they stood all the other aspects of our

81
00:10:21,500 --> 00:10:24,980
experience he said we should be mindful of them see how they arise see how

82
00:10:24,980 --> 00:10:36,660
they cease watch them observe them study them the mind is full of anger you

83
00:10:36,660 --> 00:10:43,860
know this is the mindful of anger the mind is full of lust passion you know

84
00:10:43,860 --> 00:10:48,900
this is the mindful of passion and you know the arising so that means you

85
00:10:48,900 --> 00:10:54,620
know also the things that give rise to them if you see something that will

86
00:10:54,620 --> 00:10:57,820
give rise to liking something beautiful something attractive so you're not

87
00:10:57,820 --> 00:11:04,220
seeing as well and if you're not this process you're able to understand the

88
00:11:04,220 --> 00:11:11,300
cause and the effect you see what it leads to you see the nature the

89
00:11:11,300 --> 00:11:16,580
impression that you get from it how desire is something that clouds the mind

90
00:11:16,580 --> 00:11:23,420
it colors the mind anger is something that inflames the mind drowsiness is

91
00:11:23,420 --> 00:11:30,860
something that stifles the mind and so on and so we note them just like

92
00:11:30,860 --> 00:11:38,420
everything else the five hindrances the five hindrances in bit in brief liking

93
00:11:38,420 --> 00:11:46,980
disliking drowsiness distraction and doubt these five things as I've been

94
00:11:46,980 --> 00:11:51,780
talking about he's a really the most important advice to give to men and

95
00:11:51,780 --> 00:12:01,060
important aspect of the dhamma to focus on for as a beginner meditator because

96
00:12:01,060 --> 00:12:05,140
in the beginning you're overwhelmed by these especially I mean throughout the

97
00:12:05,140 --> 00:12:08,740
course they'll come up but in the very beginning these are what are going to

98
00:12:08,740 --> 00:12:13,900
get in your way so it's when a new meditator really has to be vigilant about

99
00:12:13,900 --> 00:12:19,860
if your practice is not going well there's only five reasons really it's one

100
00:12:19,860 --> 00:12:22,420
of these five

101
00:12:22,420 --> 00:12:35,900
what liking or wanting disliking which includes boredom fear depression sadness

102
00:12:35,900 --> 00:12:43,180
drowsiness tiredness distraction distraction is also worry included in

103
00:12:43,180 --> 00:12:53,980
here things prevent you from focusing the mind on your work on your task and

104
00:12:53,980 --> 00:13:00,660
doubt doubt your confusion is the only reasons why you really fail and why it's

105
00:13:00,660 --> 00:13:05,660
hard to practice meditation it's important to look at these if these don't

106
00:13:05,660 --> 00:13:09,660
exist because mindfulness can be mindful you can be mindful of anything

107
00:13:09,660 --> 00:13:15,060
whatever happens it doesn't have to you don't have to stick to some formula

108
00:13:15,060 --> 00:13:21,140
and so without these you can adapt you can be flexible in someone experiences

109
00:13:21,140 --> 00:13:26,660
change you be mindful of the new experiences but with these it's very hard to

110
00:13:26,660 --> 00:13:34,900
accept change it's very hard to be open it's very hard to be flexible in the

111
00:13:34,900 --> 00:13:42,420
face of impermanence suffering non-self in the face of unmet expectations and

112
00:13:42,420 --> 00:13:44,740
so on

113
00:13:45,860 --> 00:13:50,500
so there you go I knew this was just gonna be a short teaching of decided to

114
00:13:50,500 --> 00:13:54,420
because I have to think of something to talk about every day we'll go with

115
00:13:54,420 --> 00:13:59,980
short talks every day this is what my teacher used to do this can be a little

116
00:13:59,980 --> 00:14:03,740
bit of done my every day something for us to think about in our practice

117
00:14:03,740 --> 00:14:14,220
so there you go that's the demo for tonight I think there's one question on

118
00:14:14,220 --> 00:14:22,860
the site but it wasn't really about practice if you want to ask questions again

119
00:14:22,860 --> 00:14:31,420
go to our meditation site there's two of them now no three of them okay how do

120
00:14:31,420 --> 00:14:39,060
you address monks oh it's up to you people my students call me Bandtay which

121
00:14:39,060 --> 00:15:03,500
means like venerable sir or something like that

122
00:15:03,500 --> 00:15:09,140
when we come a Buddhist monk and still keep in touch with family members yes sure go

123
00:15:09,140 --> 00:15:14,900
to weddings funerals and funerals maybe weddings might be problematic because

124
00:15:14,900 --> 00:15:20,620
being around alcohol is I mean it's not like you're gonna drink but it's not

125
00:15:20,620 --> 00:15:25,980
really a place for a Buddhist monk it's called agochara there are places that

126
00:15:25,980 --> 00:15:30,380
are not really proper for Buddhist monks and places where they're drinking

127
00:15:30,380 --> 00:15:35,940
alcohol is is getting there I mean there's no specific rule but my rule of

128
00:15:35,940 --> 00:15:44,700
thumb is places where there's alcohol use discretion but wedding ceremonies

129
00:15:44,700 --> 00:15:53,020
maybe on the other hand oh yeah yeah wedding ceremony and there's lots you

130
00:15:53,020 --> 00:15:55,980
know being in touch with your family certainly not a problem there's lots of

131
00:15:55,980 --> 00:16:01,380
rules that make it clear that monks are understood to keep in general

132
00:16:01,380 --> 00:16:05,780
relations with their families because we have special exceptions for our

133
00:16:05,780 --> 00:16:10,740
families exceptions to many rules allowance is allowing us to be involved

134
00:16:10,740 --> 00:16:16,300
and to care for our parents to give food to our relatives I kind of

135
00:16:16,300 --> 00:16:26,980
when craving arises how should I go about cutting it out I should read my

136
00:16:26,980 --> 00:16:34,380
booklet on how to meditate you can watch my video on pornography it's my

137
00:16:34,380 --> 00:16:40,980
most popular video probably by the title but I don't know it's a long maybe a

138
00:16:40,980 --> 00:16:44,580
little bit rambling I don't know but I think there's some good stuff in there

139
00:16:44,580 --> 00:16:52,020
it's very popular for whatever reason it was on pornography and masturbation

140
00:16:52,020 --> 00:16:58,020
based on a question someone asked many years ago as a Buddhist what should one

141
00:16:58,020 --> 00:17:02,580
do if a precept is accidentally broken well make a determination not to break

142
00:17:02,580 --> 00:17:08,580
it in the future driving in a squirrel jumped in front of the road and

143
00:17:08,580 --> 00:17:13,140
accidentally hit it well that's not breaking a preset killing is only killing

144
00:17:13,140 --> 00:17:17,620
if you intend to kill the fact that you feel bad about it is the real

145
00:17:17,620 --> 00:17:23,900
problem it's a problem because it's causing suffering you didn't intend for

146
00:17:23,900 --> 00:17:27,340
the squirrel to die there's no reason for you to be upset if it was a person

147
00:17:27,340 --> 00:17:31,660
who jumped out in front of your car nobody would come after you the law

148
00:17:31,660 --> 00:17:34,860
wouldn't come after you unless you were speeding or reckless or something like

149
00:17:34,860 --> 00:17:43,340
that I mean even then I don't think it'd be caught caught for manslaughter because

150
00:17:43,340 --> 00:17:52,260
a person jumped out in front of you on the road I don't know but yeah that's

151
00:17:52,260 --> 00:17:56,940
certainly not a bad thing but the bad thing is the guilt and the feeling bad

152
00:17:56,940 --> 00:18:04,260
about it you should be mindful of that and learn to let it go and be happier

153
00:18:04,260 --> 00:18:27,740
okay well that's all the questions on our site how are we doing in second

154
00:18:27,740 --> 00:18:51,580
life everybody hear me okay thank you all for coming out and have a good night

