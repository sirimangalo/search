1
00:00:00,000 --> 00:00:25,220
So I thought today would be an opportunity to talk about meditation from the perspective

2
00:00:25,220 --> 00:00:45,040
of providing something new, how meditation relates to change, how meditation brings about

3
00:00:45,040 --> 00:00:59,280
change, and the year is a great opportunity to talk about change, out with the old, in with

4
00:00:59,280 --> 00:01:14,940
the new, and meditation has a lot to say about change in the

5
00:01:14,940 --> 00:01:25,180
about bringing in the new, and taking out the old.

6
00:01:25,180 --> 00:01:31,820
Because really all meditation can be described in that way pretty much.

7
00:01:31,820 --> 00:01:44,140
Meditation being always related somehow to mental development, and mental development

8
00:01:44,140 --> 00:01:55,500
involving habits, the change in habits, the cultivation of new habits, and tearing down

9
00:01:55,500 --> 00:02:10,900
abandoning weakening and doing away with old habits, usually for the better.

10
00:02:10,900 --> 00:02:26,740
And so there are many meditations which promise the cultivation of specific habits.

11
00:02:26,740 --> 00:02:31,220
Loving kindness is a good example, there's a lot of talk in Buddhism about the practice

12
00:02:31,220 --> 00:02:38,060
of loving kindness, the Buddha recommended it himself, according to the text, and many Buddhist

13
00:02:38,060 --> 00:02:39,300
teachers talk about this.

14
00:02:39,300 --> 00:02:45,540
So here's an example of the cultivation of a new habit, the cultivation of the new, something

15
00:02:45,540 --> 00:02:52,620
good, that it's going to be new to the practitioner.

16
00:02:52,620 --> 00:02:58,740
Maybe you've always found it difficult to be kind to others, you find it too easy to get

17
00:02:58,740 --> 00:03:06,140
upset, annoyed, frustrated, vengeful.

18
00:03:06,140 --> 00:03:15,700
Towards others, and so the cultivation of loving kindness changes that it helps you cultivate

19
00:03:15,700 --> 00:03:25,140
this habit of being nice and kind of a lot of meditation.

20
00:03:25,140 --> 00:03:33,340
It's just designed to lead to calm, so there's some abstract object or concept that

21
00:03:33,340 --> 00:03:42,180
one focuses on, or maybe just the breath, and the goal is to calm the mind down, to enter

22
00:03:42,180 --> 00:03:51,340
into states of intense and profound concentration, a new habit, a new ability in the mind,

23
00:03:51,340 --> 00:04:01,100
a new quality of mind, for many people it's simply the continuation of practice that

24
00:04:01,100 --> 00:04:07,980
they've already undertaken in the past year, but it's always for the purpose of something

25
00:04:07,980 --> 00:04:12,700
new, something more.

26
00:04:12,700 --> 00:04:17,660
That's important, it's important to never think of meditation as just a hobby or something

27
00:04:17,660 --> 00:04:27,140
that you do to maintain some sort of state, but as something to cultivate a higher and

28
00:04:27,140 --> 00:04:32,100
greater and a better version of yourself.

29
00:04:32,100 --> 00:04:42,900
Now mindfulness meditation, as I've said before, and just to be clear about it, it's

30
00:04:42,900 --> 00:04:45,300
a little bit different.

31
00:04:45,300 --> 00:04:49,580
It's different than that, not only does it, of course, involve the cultivation of new

32
00:04:49,580 --> 00:04:57,100
habits and doing away with old habits, but it also involves a new understanding of our

33
00:04:57,100 --> 00:05:00,540
habits.

34
00:05:00,540 --> 00:05:10,260
It involves a deeper, the cultivation of a deeper and therefore new ability to discern

35
00:05:10,260 --> 00:05:15,220
what habits are bad and what habits are good without having to rely on the teacher to

36
00:05:15,220 --> 00:05:20,580
say, practice this way, and the results are good.

37
00:05:20,580 --> 00:05:29,500
It relies rather on your own investigation and your own new ability to understand and

38
00:05:29,500 --> 00:05:34,820
differentiate between the good and the bad.

39
00:05:34,820 --> 00:05:43,860
So mindfulness is always about some sort of objectivity rather than judging or reacting

40
00:05:43,860 --> 00:05:50,780
or trying to fix the problems in the mind.

41
00:05:50,780 --> 00:06:00,660
It's about observing and understanding, watching how the mind works.

42
00:06:00,660 --> 00:06:10,540
And in many ways, learning to do away with the habits of judgment, the habits of reaction.

43
00:06:10,540 --> 00:06:17,660
So rather than having fixed ideas about things, it's about studying those fixed ideas

44
00:06:17,660 --> 00:06:25,260
and beliefs, even the good ones, and getting a deeper new appreciation for the reality

45
00:06:25,260 --> 00:06:27,060
of the situation.

46
00:06:27,060 --> 00:06:33,980
Something is good, not just because I or the Buddha or you say it's good, but it's

47
00:06:33,980 --> 00:06:36,860
good because it leads to happiness and it leads to peace.

48
00:06:36,860 --> 00:06:43,740
And there's no doubt about that, for someone who has seen through the practice of mindfulness

49
00:06:43,740 --> 00:06:51,660
to be the case, you don't have to believe anything, once you see for yourself.

50
00:06:51,660 --> 00:06:58,660
So just a short video, I thought it would be a good opportunity to encourage not just

51
00:06:58,660 --> 00:07:06,020
the practice of meditation, but the practice of mindfulness and to stress again how meditation

52
00:07:06,020 --> 00:07:14,380
and very much has to do with the cultivation of something new, the arising of something new.

53
00:07:14,380 --> 00:07:17,460
It shouldn't be about reaffirming your beliefs.

54
00:07:17,460 --> 00:07:23,060
It's often surprising for long-time Buddhists or people who have studied the Buddhist

55
00:07:23,060 --> 00:07:28,620
text, even for people who have practiced other types of meditation.

56
00:07:28,620 --> 00:07:37,500
The things that they learn in mindfulness, meditation practice, are new, even to seasoned

57
00:07:37,500 --> 00:07:41,220
meditation practitioners.

58
00:07:41,220 --> 00:07:47,340
Should always be surprising, mindfulness meditation, if you're practicing it properly,

59
00:07:47,340 --> 00:07:52,100
should always show you something new, that's what it's meant to do when you're practicing

60
00:07:52,100 --> 00:07:53,100
it.

61
00:07:53,100 --> 00:07:57,780
It's increasing the clarity of your mind, so it's always going to show you something

62
00:07:57,780 --> 00:08:03,620
that you didn't quite see before or wasn't quite clear to you before.

63
00:08:03,620 --> 00:08:14,900
And so the cultivation of good habits and the eradication of bad habits becomes somewhat

64
00:08:14,900 --> 00:08:19,140
of a moot point because it's a default.

65
00:08:19,140 --> 00:08:25,100
By default, one doesn't engage in the cultivation of bad habits.

66
00:08:25,100 --> 00:08:30,460
And one is clear beyond any doubt that they're bad.

67
00:08:30,460 --> 00:08:40,060
And the cultivation of good habits comes without any second thought or uncertainty when

68
00:08:40,060 --> 00:08:44,660
one sees four oneself that they're good.

69
00:08:44,660 --> 00:08:49,940
So I wish you all a happy new year, a new year that is filled with peace, happiness, freedom

70
00:08:49,940 --> 00:08:59,380
from suffering, most importantly, one that involves an increase and continuation of the

71
00:08:59,380 --> 00:09:00,700
practice of mindfulness.

72
00:09:00,700 --> 00:09:27,820
Thank you, have a good day.

