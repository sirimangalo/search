1
00:00:00,000 --> 00:00:06,000
Okay, I will come to our evening demo session.

2
00:00:09,000 --> 00:00:12,000
Good evening, everyone.

3
00:00:17,000 --> 00:00:25,000
I often get asked by meditators both here and on online.

4
00:00:25,000 --> 00:00:30,000
What more they can do?

5
00:00:31,000 --> 00:00:36,000
Yeah, I'm being mindful, but it's not really working,

6
00:00:36,000 --> 00:00:46,000
or it's not really quick enough for my tape for my liking.

7
00:00:49,000 --> 00:00:52,000
What more can I do?

8
00:00:52,000 --> 00:00:59,000
It's kind of a greedy mind state, really.

9
00:01:02,000 --> 00:01:06,000
I mean, the whole idea of being able to do something is dangerous.

10
00:01:06,000 --> 00:01:09,000
Let's just start to think you're going to fix things.

11
00:01:09,000 --> 00:01:11,000
There's some trick.

12
00:01:11,000 --> 00:01:22,000
I mean, much more common for meditators will come and tell me about all the tricks that they've tried to fix their problems.

13
00:01:24,000 --> 00:01:31,000
Finding little ways to make it easier to make the practice more profitable.

14
00:01:31,000 --> 00:01:46,000
Otherwise, you get into the idea that you're in charge, that you're in control.

15
00:01:46,000 --> 00:01:50,000
In fact, meditation is about letting go of control.

16
00:01:50,000 --> 00:01:53,000
Stop trying to control.

17
00:01:53,000 --> 00:02:01,000
Stop having anything to do with your experiences except experiencing them.

18
00:02:01,000 --> 00:02:10,000
Learn how to be present here now, pure and clear.

19
00:02:10,000 --> 00:02:34,000
That being said, there are tricks, tricks that are ratify notes or they're spoken of well by the Buddha.

20
00:02:34,000 --> 00:02:40,000
For example, there are other meditations we can practice. I've talked about those.

21
00:02:40,000 --> 00:02:44,000
You can practice loving kindness.

22
00:02:44,000 --> 00:02:53,000
If you're feeling really angry, and sit there and say, yes, I have a bad relationship with this person or that person.

23
00:02:53,000 --> 00:03:00,000
But let's put that behind us, may they be well.

24
00:03:00,000 --> 00:03:06,000
Then practice mindfulness of the Buddha so people sometimes will do chanting or the bow down before the Buddha.

25
00:03:06,000 --> 00:03:13,000
Make a wish in front of the Buddha, may I follow in his footsteps.

26
00:03:13,000 --> 00:03:34,000
May I be guided by his teaching, may I always be surrounded by his followers.

27
00:03:34,000 --> 00:03:53,000
Another thing we can do is the Buddha gave a list of daily reflections, what he called topics for frequent reflection.

28
00:03:53,000 --> 00:04:04,000
It should be reflected upon regularly.

29
00:04:04,000 --> 00:04:14,000
The most important is for us to be mindful, for us to practice mindfulness meditation.

30
00:04:14,000 --> 00:04:19,000
It doesn't mean there are other important things that help us along.

31
00:04:19,000 --> 00:04:28,000
Things that we remind ourselves of these facts of life regularly, they keep us on track.

32
00:04:28,000 --> 00:04:34,000
There's a way of many things that keep you fenced in.

33
00:04:34,000 --> 00:04:40,000
Morality is one, for example, keeping the five precepts, just reciting the five precepts.

34
00:04:40,000 --> 00:04:46,000
It gives you some framework within which to work.

35
00:04:46,000 --> 00:04:52,000
But the Buddha gave five themes for reflection.

36
00:04:52,000 --> 00:04:55,000
They're fairly simple, but worth knowing.

37
00:04:55,000 --> 00:05:03,000
It's not that it's going to be a deep teaching, it's just going to be an instruction, hey, reflect on these every day.

38
00:05:03,000 --> 00:05:11,000
The first one is, and it doesn't matter, this one specifically says it's not just for monks.

39
00:05:11,000 --> 00:05:20,000
This is for anybody, everybody should reflect frequently.

40
00:05:20,000 --> 00:05:31,000
I am subject to old age, I am not exempt from old age.

41
00:05:31,000 --> 00:05:38,000
Number two, I am subject to illness, I am not exempt from illness.

42
00:05:38,000 --> 00:05:45,000
Number three, I am subject to death, I am not exempt from death.

43
00:05:45,000 --> 00:05:52,000
Number four, I must be parted and separated from everyone and everything, dear and agreeable to me.

44
00:05:52,000 --> 00:05:58,000
And number five, I am the owner of my kama, the heir of my kama.

45
00:05:58,000 --> 00:06:03,000
I have kama as my origin, kama as my relative, kama as my resort.

46
00:06:03,000 --> 00:06:16,000
It will be the heir of whatever kama, good or bad that I do.

47
00:06:16,000 --> 00:06:19,000
So let's go through these.

48
00:06:19,000 --> 00:06:25,000
They're fairly simple, they're related.

49
00:06:25,000 --> 00:06:38,000
I am subject to old age, people are intoxicated with youth, intoxicated with our strengths and our

50
00:06:38,000 --> 00:06:48,000
virility, our joy to view our ambitions, right?

51
00:06:48,000 --> 00:06:53,000
From the very outset, we get up, we can't wait to get up and walk.

52
00:06:53,000 --> 00:07:03,000
And when we walk, we want to run, we want to learn, we want to get, we want to become,

53
00:07:03,000 --> 00:07:06,000
and we build up personalities.

54
00:07:06,000 --> 00:07:19,000
We build up visions and dreams and wishes and wants, likes and dislikes.

55
00:07:19,000 --> 00:07:34,000
And we clamor for the things that we want, thinking we're feeling really good to be young.

56
00:07:34,000 --> 00:07:39,000
Spend some time every day thinking about the fact that you're going to get old.

57
00:07:39,000 --> 00:07:43,000
Put everything in perspective, ask yourself, what is important?

58
00:07:43,000 --> 00:07:45,000
Are all these things really useful?

59
00:07:45,000 --> 00:07:47,000
Is this really beneficial?

60
00:07:47,000 --> 00:07:49,000
Why am I working so hard?

61
00:07:49,000 --> 00:07:52,000
Why am I striving to become?

62
00:07:52,000 --> 00:07:55,000
You know?

63
00:07:55,000 --> 00:08:04,000
People put away money and for their retirement and then they die before they can use it.

64
00:08:04,000 --> 00:08:10,000
Or they're too old to enjoy it.

65
00:08:10,000 --> 00:08:21,000
It's not necessarily true, but if you waste your life thinking about, you know, so many

66
00:08:21,000 --> 00:08:29,000
useless things, you're not going to be young forever.

67
00:08:29,000 --> 00:08:39,000
Old age comes to us, we get, we lose our faculties, we lose our youth.

68
00:08:39,000 --> 00:08:43,000
Reflect on that regularly.

69
00:08:43,000 --> 00:08:48,000
Number two, we're going to be, we're subject to sickness.

70
00:08:48,000 --> 00:09:08,000
The human body is a, is a, is a, is a field, it's like the,

71
00:09:08,000 --> 00:09:15,000
well fertilized field, just waiting for the seeds to be planted.

72
00:09:15,000 --> 00:09:18,000
The body is, is a receptacle of sickness.

73
00:09:18,000 --> 00:09:23,000
So our sickness, sickness rests.

74
00:09:23,000 --> 00:09:34,000
It's a nest of, nest for the, for the growth and accumulation of illness.

75
00:09:34,000 --> 00:09:38,000
We, we get intoxicated by health.

76
00:09:38,000 --> 00:09:42,000
Very happy to be healthy.

77
00:09:42,000 --> 00:09:50,000
Consider the fact that you might get very sick, probably get very sick throughout your life.

78
00:09:50,000 --> 00:09:54,000
Consider that this health is only temporary.

79
00:09:54,000 --> 00:09:56,000
Doesn't mean it's bad to be healthy, right?

80
00:09:56,000 --> 00:09:57,000
We talked about this.

81
00:09:57,000 --> 00:09:59,000
Doesn't mean there's, it's not good to be healthy.

82
00:09:59,000 --> 00:10:01,000
It's quite good to be healthy.

83
00:10:01,000 --> 00:10:06,000
The fact of the matter is our health is uncertain.

84
00:10:06,000 --> 00:10:16,000
And the sickness is certain we're going to get sick.

85
00:10:16,000 --> 00:10:28,000
And then you ask yourself what, how meaningful are all these things that I cling to if, if that's the case?

86
00:10:28,000 --> 00:10:36,000
Quite a useful way of reducing our desires for meaningless things.

87
00:10:36,000 --> 00:10:44,000
So you start to see there must be something, there are things that are more important.

88
00:10:44,000 --> 00:10:50,000
The solution to life is to be go beyond sense pleasure.

89
00:10:50,000 --> 00:10:57,000
The sense pleasure can't help you when you're sick.

90
00:10:57,000 --> 00:11:02,000
Number three, we're all going to die reflect on your own death.

91
00:11:02,000 --> 00:11:05,000
I'm going to die.

92
00:11:05,000 --> 00:11:10,000
It really, it isn't a morbid thing to think about sickness or death.

93
00:11:10,000 --> 00:11:14,000
It's really quite calming and puts everything in perspective.

94
00:11:14,000 --> 00:11:17,000
It takes us out, you know, it stops all this conflict.

95
00:11:17,000 --> 00:11:26,000
The Buddha said, you know, people, people pick and fight as though they're going to live forever.

96
00:11:26,000 --> 00:11:31,000
If only they would stop and realize that we're all going to be dead in the hundred years.

97
00:11:31,000 --> 00:11:35,000
Why are we, what are we fighting about?

98
00:11:35,000 --> 00:11:37,000
What does it all mean?

99
00:11:37,000 --> 00:11:39,000
What's the meaning of it?

100
00:11:39,000 --> 00:11:41,000
There is no meaning.

101
00:11:41,000 --> 00:11:43,000
Put everything in perspective.

102
00:11:43,000 --> 00:11:50,000
And it really makes you chill out when you stop fussing about everything.

103
00:11:50,000 --> 00:11:57,000
Stop chasing after things that can't satisfy you, things you can't take with you.

104
00:11:57,000 --> 00:12:02,000
We have number four.

105
00:12:02,000 --> 00:12:12,000
We're going to be parted and separated from everyone and everything that we hold dear and agreeable.

106
00:12:12,000 --> 00:12:31,000
Sub-be, he may be a, he may not be, nah, nah, bhavu, we nah, bhavu.

107
00:12:31,000 --> 00:12:34,000
We used to chant this at one monastery.

108
00:12:34,000 --> 00:12:36,000
I was at a couple of places we've chanted this.

109
00:12:36,000 --> 00:12:44,000
Every day we would chant and then translate it into Thai.

110
00:12:44,000 --> 00:12:49,000
We hold on to all these things that we can't take with us.

111
00:12:49,000 --> 00:12:55,000
What are our, what are these things to us?

112
00:12:55,000 --> 00:13:00,000
Worrying about people, worrying about possessions.

113
00:13:00,000 --> 00:13:07,000
The problem is we try to cling to, it's like we try to cling to the water in the river.

114
00:13:07,000 --> 00:13:20,000
We're trying to cling to things that are flowing by.

115
00:13:20,000 --> 00:13:30,000
Let me try to hold on to a stable snapshot of reality.

116
00:13:30,000 --> 00:13:32,000
Reality is not like that.

117
00:13:32,000 --> 00:13:33,000
Reality is a movie.

118
00:13:33,000 --> 00:13:36,000
It's constantly moving, moving, changing, changing.

119
00:13:36,000 --> 00:13:45,000
And if you don't keep up, you get swept away.

120
00:13:45,000 --> 00:13:50,000
When you reflect on this, that we're going to have to lose everything.

121
00:13:50,000 --> 00:13:54,000
You start to see how danger is dangerous and unreasonable.

122
00:13:54,000 --> 00:13:59,000
It is to cling to things, cling to anything.

123
00:13:59,000 --> 00:14:06,000
Anything that you hold dear, it's not a source of strength or happiness.

124
00:14:06,000 --> 00:14:10,000
It's a strength of weakness and stress.

125
00:14:10,000 --> 00:14:17,000
Source of weakness and stress.

126
00:14:17,000 --> 00:14:21,000
And number five, what are we, what do we really own?

127
00:14:21,000 --> 00:14:26,000
What do we really have as a possession?

128
00:14:26,000 --> 00:14:30,000
Our deeds are good and bad deeds.

129
00:14:30,000 --> 00:14:33,000
Ask yourself, what are you going to carry with you?

130
00:14:33,000 --> 00:14:36,000
What is truly yours?

131
00:14:36,000 --> 00:14:40,000
What truly belongs to you?

132
00:14:40,000 --> 00:14:43,000
The only answer is, are good and are bad deeds.

133
00:14:43,000 --> 00:14:46,000
We carry them with us.

134
00:14:46,000 --> 00:14:49,000
We never let go of them.

135
00:14:49,000 --> 00:14:51,000
We can't sell them.

136
00:14:51,000 --> 00:14:53,000
We can't give them away.

137
00:14:53,000 --> 00:14:56,000
We can't shake them.

138
00:14:56,000 --> 00:15:00,000
On the positive side, if they're good,

139
00:15:00,000 --> 00:15:05,000
the one can steal them, no one can take them away from you.

140
00:15:05,000 --> 00:15:09,000
People can take away pretty much everything, including your life.

141
00:15:09,000 --> 00:15:12,000
But they can't take away your good and bad deeds.

142
00:15:12,000 --> 00:15:17,000
These things that make us who we are and give us our sense of worth,

143
00:15:17,000 --> 00:15:22,000
our sense of happiness and contentment.

144
00:15:22,000 --> 00:15:30,000
A person who is intent upon evil has evil as their possession.

145
00:15:30,000 --> 00:15:33,000
They carry it with them in their mind.

146
00:15:33,000 --> 00:15:36,000
They carry it with them as to who they are.

147
00:15:36,000 --> 00:15:41,000
An evil person, a person with an evil mind.

148
00:15:41,000 --> 00:15:49,000
A mind that is habitually unpleasant and displeased,

149
00:15:49,000 --> 00:15:54,000
cruel, mean, and so on.

150
00:15:54,000 --> 00:15:58,000
I've been hung back to wake it up.

151
00:15:58,000 --> 00:16:04,000
Let's go wake it up. These things should be frequently reflected upon.

152
00:16:04,000 --> 00:16:12,000
It's easy to remember, old age, sickness, death, loss of everything you hold dear,

153
00:16:12,000 --> 00:16:17,000
or the fact that the things that you hold dear are not don't belong to you,

154
00:16:17,000 --> 00:16:25,000
and that your deeds and good and bad deeds are something they do belong to you.

155
00:16:25,000 --> 00:16:34,000
So this isn't a meant, this isn't helpful for your meditation directly.

156
00:16:34,000 --> 00:16:41,000
But a real important reason for giving these talks is to inspire you

157
00:16:41,000 --> 00:16:47,000
to continue meditating, and I think this is a really inspiring teaching.

158
00:16:47,000 --> 00:16:50,000
Not the teaching itself, maybe, but the actual practice.

159
00:16:50,000 --> 00:16:54,000
If you put this into practice, reflecting on all five of these,

160
00:16:54,000 --> 00:17:00,000
the Buddha goes into quite detail, and it's really neat what he says.

161
00:17:00,000 --> 00:17:08,000
He says, as one often reflects on this theme, the path is generated.

162
00:17:08,000 --> 00:17:11,000
That's the English, it's not very poetic.

163
00:17:11,000 --> 00:17:21,000
See if I can find the poly.

164
00:17:21,000 --> 00:17:31,000
Mango, sun, jaya, the path is born.

165
00:17:31,000 --> 00:17:38,000
The path arises, meaning they start to see the right way.

166
00:17:38,000 --> 00:17:41,000
They start to set themselves right.

167
00:17:41,000 --> 00:17:45,000
It's really a neat little saying, because that's how it feels.

168
00:17:45,000 --> 00:17:48,000
When you wake up to this, hey, I'm going to die.

169
00:17:48,000 --> 00:17:53,000
You realign your priorities, right?

170
00:17:53,000 --> 00:17:58,000
Suddenly, those things that you thought were so important, no longer seem so important.

171
00:17:58,000 --> 00:18:03,000
Old ages inevitable, sicknesses inevitable death.

172
00:18:03,000 --> 00:18:05,000
It's not pessimistic to think about them.

173
00:18:05,000 --> 00:18:09,000
It's a reasonable concern.

174
00:18:09,000 --> 00:18:14,000
And the better you deal with it now, so that you're okay with it in the end.

175
00:18:14,000 --> 00:18:17,000
You're ready for it.

176
00:18:17,000 --> 00:18:23,000
Because so many of us are not ready for it.

177
00:18:23,000 --> 00:18:25,000
I mean, that's the thing.

178
00:18:25,000 --> 00:18:30,000
You don't have to think about the future, but if you do, think about when you die.

179
00:18:30,000 --> 00:18:33,000
Most of us are frightened of it.

180
00:18:33,000 --> 00:18:35,000
Or afraid of it.

181
00:18:35,000 --> 00:18:36,000
And so it's a good test.

182
00:18:36,000 --> 00:18:42,000
If you think about these things and it doesn't bother you, and you think, I'm ready for all that,

183
00:18:42,000 --> 00:18:45,000
then you're good to go.

184
00:18:45,000 --> 00:18:48,000
And until then, we remind ourselves of them.

185
00:18:48,000 --> 00:18:50,000
And they help realign us.

186
00:18:50,000 --> 00:18:52,000
They get us on the right path.

187
00:18:52,000 --> 00:18:57,000
So they say, hey, these things are not so important.

188
00:18:57,000 --> 00:18:59,000
The only thing that's important is good deeds.

189
00:18:59,000 --> 00:19:04,000
Being full of goodness, both in the mind and in the body.

190
00:19:04,000 --> 00:19:14,000
Good deeds, good speech, good thoughts, a clear mind, a pure mind, a free mind.

191
00:19:14,000 --> 00:19:16,000
So there you go.

192
00:19:16,000 --> 00:19:18,000
Five things to reflect upon.

193
00:19:18,000 --> 00:19:19,000
That's the dumb of her tonight.

194
00:19:19,000 --> 00:19:45,000
Thank you all for tuning in.

195
00:19:45,000 --> 00:19:52,000
I'll answer questions here.

196
00:19:52,000 --> 00:19:54,000
I'm unclear about the dumbest category.

197
00:19:54,000 --> 00:19:55,000
Please advise.

198
00:19:55,000 --> 00:19:56,000
So I've talked about this.

199
00:19:56,000 --> 00:20:00,000
I'm unclear about it myself, admittedly.

200
00:20:00,000 --> 00:20:09,000
But I think of dumbas as the sorts of things that a meditation teacher would advise

201
00:20:09,000 --> 00:20:10,000
a meditation student.

202
00:20:10,000 --> 00:20:15,000
So dumba here, I think of as the teachings of the Buddha really.

203
00:20:15,000 --> 00:20:18,000
At the same time as being sets of reality.

204
00:20:18,000 --> 00:20:19,000
But it's not all of them.

205
00:20:19,000 --> 00:20:24,000
It's just the ones that are important for meditators.

206
00:20:24,000 --> 00:20:26,000
So the first one is the hindrances.

207
00:20:26,000 --> 00:20:30,000
And I would take just take it as an expanded part of the practice.

208
00:20:30,000 --> 00:20:34,000
I mean, another thing that you have to understand is this suit that was given to a specific audience.

209
00:20:34,000 --> 00:20:36,000
Or it was also given to an audience.

210
00:20:36,000 --> 00:20:39,000
It was given with the audience in mind.

211
00:20:39,000 --> 00:20:44,000
So as they're listening to it, and even as we read it or as we hear it,

212
00:20:44,000 --> 00:20:52,000
when you get to the dumbas, by that time you've already started to set yourself on the practice.

213
00:20:52,000 --> 00:20:57,000
And so by the time you get there, all of these things are useful as an addition to that.

214
00:20:57,000 --> 00:21:04,000
And if you're quick, I mean, this would have been taught to people who probably had fairly advanced faculties.

215
00:21:04,000 --> 00:21:07,000
So I mean, it's interesting that dumba isn't one thing.

216
00:21:07,000 --> 00:21:08,000
It's many different things.

217
00:21:08,000 --> 00:21:17,000
So it appears to have been, or I would guess it's sort of a progression.

218
00:21:17,000 --> 00:21:19,000
You know, I could teach them in that order.

219
00:21:19,000 --> 00:21:20,000
I mean, it certainly works.

220
00:21:20,000 --> 00:21:33,000
If you study them in the order that they're given, they make a good progression or progressive practice.

221
00:21:33,000 --> 00:21:39,000
For beginners, we just start with the five hindrances and the six senses.

222
00:21:39,000 --> 00:21:42,000
Don't happen to you, Upadana.

223
00:21:42,000 --> 00:21:46,000
Upadana here mainly referred to kamapadana.

224
00:21:46,000 --> 00:21:48,000
It does include all four types.

225
00:21:48,000 --> 00:21:52,000
How do you explain did to you, Badana?

226
00:21:52,000 --> 00:21:54,000
You can cause by craving.

227
00:21:54,000 --> 00:21:56,000
I don't like these questions.

228
00:21:56,000 --> 00:21:58,000
What does that question?

229
00:21:58,000 --> 00:22:00,000
Does the answer that question really help you?

230
00:22:00,000 --> 00:22:02,000
Maybe it does.

231
00:22:02,000 --> 00:22:04,000
It's too tough a question.

232
00:22:04,000 --> 00:22:05,000
I want easy questions.

233
00:22:05,000 --> 00:22:10,000
Easy questions only.

234
00:22:10,000 --> 00:22:11,000
I don't know.

235
00:22:11,000 --> 00:22:14,000
I don't think I'm going to answer that.

236
00:22:14,000 --> 00:22:20,000
My first guess is that it probably is only kamapadana, but that's not even true.

237
00:22:20,000 --> 00:22:22,000
Did you do Badana?

238
00:22:22,000 --> 00:22:40,000
I think that the different ways of talking about Upadana cannot be always used in the same context.

239
00:22:40,000 --> 00:22:45,000
This is an interesting question.

240
00:22:45,000 --> 00:22:48,000
Reality non-reality is spoken of constructs.

241
00:22:48,000 --> 00:22:51,000
Can you please explain what they are?

242
00:22:51,000 --> 00:22:58,000
The construct was just a word, but generally we use that word to refer to things that arise.

243
00:22:58,000 --> 00:23:04,000
Because anything that arises has conditions that cause it to arise.

244
00:23:04,000 --> 00:23:06,000
Without those conditions, it doesn't arise.

245
00:23:06,000 --> 00:23:08,000
In a sense, it's constructed.

246
00:23:08,000 --> 00:23:09,000
It's a construct.

247
00:23:09,000 --> 00:23:11,000
Sankara is the word we use.

248
00:23:11,000 --> 00:23:14,000
Formation or construct.

249
00:23:14,000 --> 00:23:17,000
Although I have used the word in a different way.

250
00:23:17,000 --> 00:23:20,000
A mental construct is a concept.

251
00:23:20,000 --> 00:23:23,000
So you can create something in your mind.

252
00:23:23,000 --> 00:23:26,000
The thing itself is not real.

253
00:23:26,000 --> 00:23:31,000
The thing is something you've imagined in your mind.

254
00:23:31,000 --> 00:23:35,000
A rabbit with horns, say you can think of it in your mind.

255
00:23:35,000 --> 00:23:38,000
But that horned rabbit doesn't actually exist.

256
00:23:38,000 --> 00:23:42,000
It's just a thought in your picture in your mind.

257
00:23:42,000 --> 00:23:50,000
So it could be used in different ways, I suppose.

258
00:23:50,000 --> 00:23:53,000
Don't use that word that often.

259
00:24:05,000 --> 00:24:08,000
When I meditate, I become happy in the moment.

260
00:24:08,000 --> 00:24:13,000
Don't take my overbearing responsibility seriously and follow a secular path of normal people.

261
00:24:13,000 --> 00:24:16,000
When I stop meditating, I want to ordain.

262
00:24:16,000 --> 00:24:19,000
How would I find the middle ground between these two?

263
00:24:19,000 --> 00:24:23,000
Also, would a person's extreme sense of responsibility?

264
00:24:23,000 --> 00:24:28,000
Would you say a person's extreme sense of responsibility?

265
00:24:28,000 --> 00:24:32,000
Is an overcompensation for something such as guilt?

266
00:24:32,000 --> 00:24:36,000
I don't really quite understand what you're asking.

267
00:24:36,000 --> 00:24:44,000
You're happy overbearing responsibility, secular path.

268
00:24:44,000 --> 00:24:49,000
But when you stop meditating, you want to ordain.

269
00:24:49,000 --> 00:24:50,000
I don't know.

270
00:24:50,000 --> 00:24:53,000
I'm just going to say, if you want my advice, keep meditating,

271
00:24:53,000 --> 00:24:56,000
keep being mindful and be mindful of all these things,

272
00:24:56,000 --> 00:25:00,000
you'll find the answers for yourself.

273
00:25:00,000 --> 00:25:05,000
It's much more simple maybe than you're making it.

274
00:25:05,000 --> 00:25:07,000
We've sat up on a manatee,

275
00:25:07,000 --> 00:25:13,000
meaning that the trustworthy person is the ultimate relative familiar to us,

276
00:25:13,000 --> 00:25:15,000
but not really trustworthy.

277
00:25:15,000 --> 00:25:17,000
Your intimate is the word I'm in.

278
00:25:17,000 --> 00:25:22,000
It's not familiar.

279
00:25:22,000 --> 00:25:24,000
Someone who is intimate.

280
00:25:24,000 --> 00:25:27,000
And so as a result, you trust them.

281
00:25:27,000 --> 00:25:31,000
Someone who, but it's used in a sense of being familiar in the sense

282
00:25:31,000 --> 00:25:36,000
that you know that they wouldn't mind.

283
00:25:36,000 --> 00:25:39,000
It's not trusting each other.

284
00:25:39,000 --> 00:25:41,000
It's not about being trustworthy.

285
00:25:41,000 --> 00:25:43,000
It's about trusting each other.

286
00:25:43,000 --> 00:25:45,000
That word does mean trustworthy, I think.

287
00:25:45,000 --> 00:25:48,000
It's used in that sense, but I don't think that's directly

288
00:25:48,000 --> 00:25:54,000
what the word literally means.

289
00:25:54,000 --> 00:25:57,000
When you trust each other,

290
00:25:57,000 --> 00:26:02,000
so you can, someone who is a trusted or intimate.

291
00:26:02,000 --> 00:26:05,000
So in a sense, familiar, that's all I meant by that.

292
00:26:05,000 --> 00:26:08,000
You're right, it's probably not the best word I could use.

293
00:26:08,000 --> 00:26:11,000
An intimate might be better, but not even intimate.

294
00:26:11,000 --> 00:26:16,000
It's, yeah, people you can trust, I guess.

295
00:26:16,000 --> 00:26:18,000
People who you trust.

296
00:26:18,000 --> 00:26:33,000
Let go of our names, are they considered attachments?

297
00:26:33,000 --> 00:26:36,000
Names are just concepts in them.

298
00:26:36,000 --> 00:26:39,000
They're useful tools that allow us to refer to things.

299
00:26:39,000 --> 00:26:42,000
Just don't get attached to it.

300
00:26:42,000 --> 00:26:45,000
They get attached to them.

301
00:26:45,000 --> 00:26:49,000
I'm thinking of mantras disturbs my breath, does it?

302
00:26:49,000 --> 00:26:52,000
It feels less smoothly, really.

303
00:26:52,000 --> 00:26:55,000
When I focus on the stomach rising without the mantras,

304
00:26:55,000 --> 00:26:58,000
it's more fluent and pleasant.

305
00:26:58,000 --> 00:27:01,000
Is the mantra supposed to do that, or am I doing it somehow wrong?

306
00:27:01,000 --> 00:27:02,000
No, you're doing it right.

307
00:27:02,000 --> 00:27:04,000
The mantra doesn't do that.

308
00:27:04,000 --> 00:27:14,000
It's when you stop controlling and when you stop fixing it,

309
00:27:14,000 --> 00:27:24,000
that it starts to be unpleasant.

310
00:27:24,000 --> 00:27:29,000
You get out of that groove, the comfortable groove of enjoying it,

311
00:27:29,000 --> 00:27:38,000
because you can create pleasurable sensations in the mind if you want.

312
00:27:38,000 --> 00:27:41,000
But all you're doing is creating more and more attachment.

313
00:27:41,000 --> 00:27:47,000
What the mantra does is it forces you to see things without clinging to them,

314
00:27:47,000 --> 00:27:50,000
without attaching to them.

315
00:27:50,000 --> 00:27:53,000
As a result, it doesn't feed our addictions.

316
00:27:53,000 --> 00:28:01,000
It doesn't feed our desire for pleasure or desire for tranquility.

317
00:28:01,000 --> 00:28:04,000
So that's uncomfortable.

318
00:28:04,000 --> 00:28:07,000
The mantra is meant to take you out of your comfort zone

319
00:28:07,000 --> 00:28:16,000
and straighten your mind so that instead of being attached to the pleasant feeling,

320
00:28:16,000 --> 00:28:20,000
you're objective and flexible,

321
00:28:20,000 --> 00:28:25,000
and able to experience things however they may be.

322
00:28:25,000 --> 00:28:27,000
It helps you see impermanence,

323
00:28:27,000 --> 00:28:30,000
and what you're seeing is impermanent suffering in non-self,

324
00:28:30,000 --> 00:28:34,000
especially non-self, because you can't control.

325
00:28:34,000 --> 00:28:39,000
You want it to be smooth and it's not smooth.

326
00:28:39,000 --> 00:28:49,000
Suffering.

327
00:28:49,000 --> 00:28:52,000
How can we make a positive difference,

328
00:28:52,000 --> 00:28:55,000
trying to help people with compassion with them?

329
00:28:55,000 --> 00:29:00,000
There's an example in an idea how to properly offer food

330
00:29:00,000 --> 00:29:07,000
at this ceremony to the monks. Is there a specific way?

331
00:29:07,000 --> 00:29:10,000
Yeah, I mean, I can't speak for those monks,

332
00:29:10,000 --> 00:29:13,000
but monks in general are pretty laid back about it.

333
00:29:13,000 --> 00:29:16,000
They're just happy that people are giving them food.

334
00:29:16,000 --> 00:29:19,000
I mean, otherwise they die.

335
00:29:19,000 --> 00:29:22,000
So I wouldn't worry too much about that.

336
00:29:22,000 --> 00:29:26,000
You might get some late people trying to tell you how to do it,

337
00:29:26,000 --> 00:29:30,000
or even the monks maybe get caught up in this way or that way,

338
00:29:30,000 --> 00:29:34,000
but in the texts, it says that even if an novice

339
00:29:34,000 --> 00:29:38,000
were to be sitting on the shoulders of a monk

340
00:29:38,000 --> 00:29:43,000
and drops a lump of rice in his bowl,

341
00:29:43,000 --> 00:29:47,000
it's considered properly offered.

342
00:29:47,000 --> 00:29:52,000
Making the point that it's much more about making sure

343
00:29:52,000 --> 00:29:54,000
that it's given.

344
00:29:54,000 --> 00:29:57,000
We take as a rule to only eat food if it's given to us,

345
00:29:57,000 --> 00:29:58,000
and that keeps us honest.

346
00:29:58,000 --> 00:30:01,000
We don't ever go looking for food or wanting for food.

347
00:30:01,000 --> 00:30:05,000
The only way we can possibly stay alive is if people want us to stay alive

348
00:30:05,000 --> 00:30:09,000
and the only food that we can eat is food

349
00:30:09,000 --> 00:30:12,000
that is blamelessly given, people wanted to give it.

350
00:30:12,000 --> 00:30:14,000
They were happy to support us.

351
00:30:14,000 --> 00:30:20,000
It's not coercion, coerced, or bought by some favor

352
00:30:20,000 --> 00:30:24,000
or by some exchange, it's given because someone genuinely

353
00:30:24,000 --> 00:30:27,000
wanted you to have food.

354
00:30:27,000 --> 00:30:33,000
That keeps you quite honest and quite humble.

355
00:30:33,000 --> 00:30:36,000
As far as making a positive difference,

356
00:30:36,000 --> 00:30:38,000
I mean, if you want to make a positive difference,

357
00:30:38,000 --> 00:30:43,000
that's an attachment, so better off to let that go.

358
00:30:43,000 --> 00:30:47,000
Just do what comes naturally and mindfully,

359
00:30:47,000 --> 00:30:53,000
that'll be good.

360
00:30:53,000 --> 00:30:57,000
What do you think about mystic experiences that you're suffering?

361
00:30:57,000 --> 00:31:02,000
Use fuel to have more intense experiences.

362
00:31:02,000 --> 00:31:07,000
I don't think about them at all, really.

363
00:31:07,000 --> 00:31:10,000
Some in society say the future is your brain on drugs.

364
00:31:10,000 --> 00:31:12,000
What do you think about this?

365
00:31:12,000 --> 00:31:20,000
It's blasphemy. I don't think about it at all.

366
00:31:20,000 --> 00:31:23,000
Sorry, questions unrelated to what I do.

367
00:31:23,000 --> 00:31:25,000
I'm not that keen to answer them.

368
00:31:25,000 --> 00:31:28,000
There's just too many.

369
00:31:28,000 --> 00:31:33,000
They don't really have answers.

370
00:31:33,000 --> 00:31:43,000
Well, that was somewhat satisfying,

371
00:31:43,000 --> 00:31:48,000
and if it wasn't, I'll go meditate and let it go.

