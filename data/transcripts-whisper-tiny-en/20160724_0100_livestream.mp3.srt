1
00:00:00,000 --> 00:00:10,720
We're broadcasting live, we're trying something new tonight.

2
00:00:10,720 --> 00:00:20,560
Stumbled upon something that YouTube is putting together, there's actually a live broadcast

3
00:00:20,560 --> 00:00:30,720
feature on YouTube now, with some kind of neat features.

4
00:00:30,720 --> 00:00:44,880
If you're already tuned in, you can see the, finally we're all going to, just get started.

5
00:00:44,880 --> 00:01:03,720
But yeah, we have a chat now, so you can chat with me on YouTube and I can see it.

6
00:01:03,720 --> 00:01:10,040
So let me know if there's any issues, hopefully you can hear me now, if you can't let

7
00:01:10,040 --> 00:01:17,080
me know if there's any problems with the stream, let me know.

8
00:01:17,080 --> 00:01:27,080
Anyway, so the night's teaching, again we're continuing with the, I'm going to do the

9
00:01:27,080 --> 00:01:35,520
Kaya.

10
00:01:35,520 --> 00:01:48,280
So tonight's teaching is the third chapter of the, I'm going to do it, which begins

11
00:01:48,280 --> 00:02:04,080
as the other ones do.

12
00:02:04,080 --> 00:02:26,080
Now you should see, hopefully you see this.

13
00:02:26,080 --> 00:02:36,840
So we have another group of 10 sootas about the mind, I don't know if this is at all interesting,

14
00:02:36,840 --> 00:02:50,640
let's go back to the other one.

15
00:02:50,640 --> 00:03:01,400
We should observe about the mind, it's called the Akmani of Akka, the chapter on unwieldly,

16
00:03:01,400 --> 00:03:05,920
being unwieldy, that which is unwieldy.

17
00:03:05,920 --> 00:03:20,000
We talked about this word unwieldy, Akmani is the word, Akmani, I mean you can work it

18
00:03:20,000 --> 00:03:27,960
after you can use it, Akmani, I mean you can't use it, anyway, so it starts the first

19
00:03:27,960 --> 00:03:37,000
one, there are 10 and the general gist of this chapter is about the importance of the mind,

20
00:03:37,000 --> 00:03:43,880
the power of the mind, the efficacy of the mind, that the mind is efficacious, meaning

21
00:03:43,880 --> 00:04:00,640
it has an influence, it has an effect, it goes on in the mind, it has results, it's

22
00:04:00,640 --> 00:04:07,920
meaningful, it's not just what we would call an epiphenomenal, meaning something that

23
00:04:07,920 --> 00:04:15,120
occurs but has no effect on the system.

24
00:04:15,120 --> 00:04:26,560
This is really one of the most crucial aspects of the Buddha's teaching, a crucial

25
00:04:26,560 --> 00:04:36,560
points of doctrine in Buddhism, that the mind is efficacious, that the mind has an effect,

26
00:04:36,560 --> 00:04:45,320
the first verse of the Dhammapada, there's no coincidence, that the first verse is Manopobangamand

27
00:04:45,320 --> 00:04:57,400
Amma, all dhammas are preceded by the mind, that the mind is the beginning, for a long

28
00:04:57,400 --> 00:05:14,240
time this has been, this has been denied by science, modern materialist science because

29
00:05:14,240 --> 00:05:23,600
when you observe a physical system, you don't see the mind, you can't observe the mind,

30
00:05:23,600 --> 00:05:28,160
the mind is not part of the system.

31
00:05:28,160 --> 00:05:32,920
And so for the longest time it was thought that because when we observe the system there

32
00:05:32,920 --> 00:05:42,560
is no mind, the mind doesn't really exist, it's just someone explained to me it's a surface

33
00:05:42,560 --> 00:05:49,880
phenomenon, like heat or color, meaning it's a result of what is real but it's not real

34
00:05:49,880 --> 00:06:00,040
itself, it's like harmony when you bang a tuning fork, the vibrations create sound, and

35
00:06:00,040 --> 00:06:07,920
there's a great problem with that argument, but without going there, the interesting

36
00:06:07,920 --> 00:06:18,880
thing is in the 20th century when quantum physics came, became prominent, they realized

37
00:06:18,880 --> 00:06:27,880
that they had missed an important part of the equation, that when you say observing the

38
00:06:27,880 --> 00:06:36,760
system, you forget to take into account the act of observation, and that doesn't seem

39
00:06:36,760 --> 00:06:43,120
important from a material science point of view, but it, as quantum physics showed, it's

40
00:06:43,120 --> 00:06:52,040
crucially important, and that in fact reality goes beyond the physical systems that we observe,

41
00:06:52,040 --> 00:07:07,680
that the physical systems that we observe are, the nature is dictated by the observation,

42
00:07:07,680 --> 00:07:19,080
they don't come into being even until the, until observed or, right, the system is dependent

43
00:07:19,080 --> 00:07:30,320
on the observation, the system, the definition of the system has to include the observation

44
00:07:30,320 --> 00:07:35,720
in it, meaning if you observe things in a certain way you get a certain result, and

45
00:07:35,720 --> 00:07:44,240
the observation is, and the mind is crucially important, now that's just a description

46
00:07:44,240 --> 00:07:52,600
from a modern scientific point of view, but when the point of view of our meditation doesn't

47
00:07:52,600 --> 00:08:02,200
really matter what people believe or what modern theory is, because clearly the mind

48
00:08:02,200 --> 00:08:09,200
is the most crucial aspect of the meditative experience, regardless of whether you believe

49
00:08:09,200 --> 00:08:24,240
that it has an effect on the world around you, certainly has an effect on our experience,

50
00:08:24,240 --> 00:08:31,040
but this is important, and I think overlooked by beginner meditators, to their detriment

51
00:08:31,040 --> 00:08:41,400
to their peril, at their peril, because we're talking a lot about results and trying

52
00:08:41,400 --> 00:08:56,000
to verify the success that one's having or has had with one's practice, and the problem

53
00:08:56,000 --> 00:09:01,160
with that, I mean it's just an example really, but it's a good example, because the problem

54
00:09:01,160 --> 00:09:07,840
with that is when you start to wonder about your progress, when you start to wonder about

55
00:09:07,840 --> 00:09:15,680
the effects of the mind, let's put it this way, in a general sense, if you start to investigate

56
00:09:15,680 --> 00:09:23,720
is the mind efficacious, is my meditation having an effect on my life?

57
00:09:23,720 --> 00:09:34,840
That investigation is in and of itself efficacious, in a bad way, in an obstructive way,

58
00:09:34,840 --> 00:09:41,760
because you're no longer meditating, so what this means is when a meditator begins

59
00:09:41,760 --> 00:09:46,480
to question their practice, and this is the problem with doubt really, when you begin

60
00:09:46,480 --> 00:09:52,200
to doubt your practice, or question your practice, you're no longer able to observe your

61
00:09:52,200 --> 00:09:57,800
practice, you're no longer able to observe the benefits, and that's a big problem, so the

62
00:09:57,800 --> 00:10:03,200
way it plays out is a meditator will be practicing and getting good results, they will

63
00:10:03,200 --> 00:10:09,200
be learning things and their mind will be coming clear, but then something comes up that

64
00:10:09,200 --> 00:10:15,080
triggers a response and they're no longer being mindful, and as soon as they stop being

65
00:10:15,080 --> 00:10:20,800
mindful they start to question, because they're falling into delusion, their mind

66
00:10:20,800 --> 00:10:26,000
is no longer clear, they ask themselves, you know, oh, this bad thing came, there must

67
00:10:26,000 --> 00:10:32,080
be something wrong, it's our ordinary way of understanding bad things, some pain came

68
00:10:32,080 --> 00:10:38,200
up or my mind is being distracted and it's uncomfortable, so there must be something wrong

69
00:10:38,200 --> 00:10:43,760
and so you start to question, and as soon as you start to question you're no longer

70
00:10:43,760 --> 00:10:49,000
being mindful, because you're no longer being mindful, you're no longer getting any benefit,

71
00:10:49,000 --> 00:10:56,360
so it becomes a vicious spiral, you're not practicing, so you're not getting any benefit,

72
00:10:56,360 --> 00:11:01,760
you're not getting any benefit, so you don't think the practice is worth doing, so

73
00:11:01,760 --> 00:11:13,160
you don't practice, so you don't get any benefit, it sounds like a kind of a tricky thing

74
00:11:13,160 --> 00:11:22,200
to say that somehow I'm trying to avoid offering up any benefit to the meditation practice,

75
00:11:22,200 --> 00:11:29,600
but this is why you have to find benefit in the present moment, you have to see goodness,

76
00:11:29,600 --> 00:11:38,080
rather than worrying about results in the future, living in the future, living in the past,

77
00:11:38,080 --> 00:11:43,160
you have to find the benefit in the quality of the present moment, the quality of your present

78
00:11:43,160 --> 00:11:58,040
experience, the quality of your mind in the present moment, and you reassure yourself, you

79
00:11:58,040 --> 00:12:11,400
demand of yourself, the honesty and the appreciation that a pure mind is a good thing, regardless

80
00:12:11,400 --> 00:12:22,200
of exactly what results it brings, you force yourself to accept the honest truth that a good

81
00:12:22,200 --> 00:12:30,440
state can only bring good things, so you stop worrying about whatever results, but I bring

82
00:12:30,440 --> 00:12:35,960
this up because it's an interesting example of the efficacy of the mind, the importance

83
00:12:35,960 --> 00:12:51,440
of the state of mind and how subtle and delicate is the state of meditation practice, and

84
00:12:51,440 --> 00:12:59,240
also how real it is, this isn't a game where you can sit back and look and see how you're

85
00:12:59,240 --> 00:13:05,400
doing, see what the score is, as soon as you step back and try to keep score, you're affecting

86
00:13:05,400 --> 00:13:14,080
the game, and there's no stepping out, and this goes with everything, when you take a break

87
00:13:14,080 --> 00:13:20,760
from the meditation practice, you don't take a break from life, so you start to cultivate

88
00:13:20,760 --> 00:13:31,280
an unwholesome, so my teacher would, many times he said, they pointed out how karma works

89
00:13:31,280 --> 00:13:39,080
on the moment or level, he would say, every time that you're not mindful there's delusion,

90
00:13:39,080 --> 00:13:44,520
when you like something that's green, when you dislike something that's anger, when

91
00:13:44,520 --> 00:13:51,280
you're not mindful, whether you dislike it or like it or don't dislike it, don't like it,

92
00:13:51,280 --> 00:14:00,280
there's delusion, every moment.

93
00:14:00,280 --> 00:14:10,840
Our existence in Samsara is not static, not the way we think it is, so when we think

94
00:14:10,840 --> 00:14:16,960
of ourselves as having a life and we can live our lives and just, you know, coast through

95
00:14:16,960 --> 00:14:27,720
life, that we're constantly changing, we're constantly following our habits and cultivating

96
00:14:27,720 --> 00:14:36,760
habit, and so we're constantly becoming our habits.

97
00:14:36,760 --> 00:14:45,640
And so the importance of the mind, this is really the key in our practice, is to realize

98
00:14:45,640 --> 00:14:57,080
and to put into action the power of the mind, make the mind work for us, because as these

99
00:14:57,080 --> 00:15:13,240
suit to say, the first one says, I can see nothing, nothing that is, I can see nothing

100
00:15:13,240 --> 00:15:20,920
more unwieldy, nothing more useless probably, but I'm not tired, that's the next one, nothing

101
00:15:20,920 --> 00:15:34,280
more useless than the untrained mind, the untrained mind is of great, but on that time it

102
00:15:34,280 --> 00:15:48,760
also means detriment, and the untrained mind is of great detriment, like unlike anything else,

103
00:15:48,760 --> 00:15:54,760
like the untrained body, well, lots of things you can do if your body is well trained.

104
00:15:54,760 --> 00:16:01,520
But no matter what you have in the physical world, no matter your wealth or your power,

105
00:16:01,520 --> 00:16:10,280
your physical prowess, your physical possessions, your physical stature, status and position

106
00:16:10,280 --> 00:16:20,440
and fame and glory, if your mind is useless, you are useless, all the physical wealth

107
00:16:20,440 --> 00:16:29,840
in the world, useless, in the other hand, if you have nothing, if you live on a park bench,

108
00:16:29,840 --> 00:16:46,720
your mind is strong, your mind is wieldy, is cultivated, and you're invincible, your omnipotent

109
00:16:46,720 --> 00:17:01,040
and you have perfect power, there's nothing that can shake you, unshakable, so these

110
00:17:01,040 --> 00:17:08,480
tends to just go through different ways of saying that, they bring happiness, the trained

111
00:17:08,480 --> 00:17:19,600
mind brings happiness, the untrained mind leads to harm, leads to suffering, the uncultivated

112
00:17:19,600 --> 00:17:20,600
mind.

113
00:17:20,600 --> 00:17:32,280
And there's this word, ba-dubuta, which means made, big ability translated as manifest,

114
00:17:32,280 --> 00:17:40,320
and he understands it as being applied, being used, being tapped.

115
00:17:40,320 --> 00:17:49,640
If you don't put your mind, if you don't put your mind to use your voice to them, the

116
00:17:49,640 --> 00:17:58,320
greatest potential that we have to benefit and to prosper and to succeed in life and everything

117
00:17:58,320 --> 00:17:59,320
we do.

118
00:17:59,320 --> 00:18:07,080
I mean, think about training your body, if you compare these two, how much of training

119
00:18:07,080 --> 00:18:15,160
your body is mental, right, the mental fortitude, the mental prowess to actually undertake

120
00:18:15,160 --> 00:18:23,960
the training and persist with the training, to succeed in business, to succeed in study,

121
00:18:23,960 --> 00:18:28,960
to succeed in relationships, how much of it is dependent on the mind.

122
00:18:28,960 --> 00:18:33,320
And yet, what do we do with our mind?

123
00:18:33,320 --> 00:18:46,400
Do we train it, do we clean it, do we care for it, we abuse it, we neglect it, we

124
00:18:46,400 --> 00:19:06,040
sullient, we feed it with garbage and junk and we torture it, we repress it, we stretch

125
00:19:06,040 --> 00:19:16,040
it totally out of shame, and then we expect to find happiness, see the mind.

126
00:19:16,040 --> 00:19:27,200
And you feel there's no more, you feel there's greater nowhere else than in meditation.

127
00:19:27,200 --> 00:19:32,120
Meditation you can, when you're sitting still, you know, when you've got nothing else

128
00:19:32,120 --> 00:19:46,720
to distract you, and then your relationship with your mind is most important.

129
00:19:46,720 --> 00:19:53,640
If you become distracted, you'll feel it and start to get a headache, if you become a

130
00:19:53,640 --> 00:20:01,520
desireous of this or that, you'll feel the heat, if you have anger or aversion, you have

131
00:20:01,520 --> 00:20:15,080
to deal with it, it's like a pressure core, all the emotions, all the mental upset and

132
00:20:15,080 --> 00:20:25,480
stress and defilement, it's all in there, so this is why when you meditate you have to see

133
00:20:25,480 --> 00:20:30,760
things like doubt as a hindrance, you have to see things like desire and aversion and

134
00:20:30,760 --> 00:20:45,400
as hindrances, you can't entertain them, analyze and worry about them, if you want to

135
00:20:45,400 --> 00:20:51,480
really progress in meditation, everything has to be clinical and systematic, everything

136
00:20:51,480 --> 00:21:00,240
has to be seen as an experience, even your responses to the experiences, your interpretations,

137
00:21:00,240 --> 00:21:09,160
when you interpret something, the interpretation is also an experience, you can't see entities

138
00:21:09,160 --> 00:21:18,240
and you can't get caught up in things, you know, so this is about the mind, a little

139
00:21:18,240 --> 00:21:29,400
bit of dumb of her tonight, how are we doing, okay, I've got these needs, this need, dashboard

140
00:21:29,400 --> 00:21:34,640
here, and you've all got this great chat feature, which I'm kind of concerned about because

141
00:21:34,640 --> 00:21:39,600
now we're letting list of floodgates and that was what our website was for, it was meant

142
00:21:39,600 --> 00:21:46,800
to filter out meditators and non meditators, filter out non meditators, leave us with

143
00:21:46,800 --> 00:21:52,840
only meditators, so I'm not sure we're going, I think we're going to take questions only

144
00:21:52,840 --> 00:21:59,760
from our website for now, you all can chat on YouTube but if you don't sign up for our

145
00:21:59,760 --> 00:22:06,880
website and start meditating, sorry, because it's just too much, you know, I've done this

146
00:22:06,880 --> 00:22:13,200
before and it gets too far to broad base, or I never get anything done because I'm just

147
00:22:13,200 --> 00:22:32,560
answering question after question, so we're going back to our website, see if I can, this

148
00:22:32,560 --> 00:22:45,360
is our website meditation.ceremungalow.org, okay, okay, I want to tell you guys, don't

149
00:22:45,360 --> 00:23:03,720
argue so much, our chat box is full of debates and discussions, be chill out, okay, so

150
00:23:03,720 --> 00:23:09,000
some questions, did you ever teach people, did you ever teach to people who schizophrenia

151
00:23:09,000 --> 00:23:15,520
or bipolar, if so what was your experience? No, I've never had the honor, I may never because

152
00:23:15,520 --> 00:23:21,200
of course liability issues if I start to muck around with people who have, I don't

153
00:23:21,200 --> 00:23:27,080
know, I don't know that in Canada liability is such an issue but it could be, I may,

154
00:23:27,080 --> 00:23:34,360
I mean, I would like to, I would like to, I would be happy to, yes, I know such people

155
00:23:34,360 --> 00:23:41,280
are really a prime subject for the meditation practice to see whether it works, yeah, switch

156
00:23:41,280 --> 00:23:51,840
back here, I don't think, I think this is a better view, so watch the monk, the monk

157
00:23:51,840 --> 00:24:01,600
will do the reading for you.

158
00:24:01,600 --> 00:24:07,120
Because, I mean schizophrenia and bipolar, I think are quite different, I'm not sure,

159
00:24:07,120 --> 00:24:15,000
but schizophrenia is hallucination, which are, you know, lead to paranoia and extremes

160
00:24:15,000 --> 00:24:21,760
of emotion, but bipolar is just extremes of emotion, right?

161
00:24:21,760 --> 00:24:28,000
Both of them are problematic in terms of the reactions to experiences, so it's a prime

162
00:24:28,000 --> 00:24:38,520
example of the importance of training the mind to see things as they are and not judgment.

163
00:24:38,520 --> 00:24:42,480
You said in a listening to Dhamma video, we should meditate, well listening, does it mean

164
00:24:42,480 --> 00:24:49,680
regular meditation with labeling as usual, if we are in the words, yeah, just label hearing.

165
00:24:49,680 --> 00:24:53,800
You don't have to go back to the breath, you can stay with the hearing if someone's shouting

166
00:24:53,800 --> 00:24:58,240
at you, or even just talking to you, I mean, you don't even say it three times, you

167
00:24:58,240 --> 00:25:05,160
can just say it once, especially in your daily life, it's, you know, it's a war zone really

168
00:25:05,160 --> 00:25:10,960
when you try to meditate during daily life, if you can get one clear awareness of this

169
00:25:10,960 --> 00:25:19,200
is hearing, this is seeing, this is thinking, good for you, but yeah, if someone's talking

170
00:25:19,200 --> 00:25:26,280
on and on and you're not too concerned about getting involved with what they say or responding,

171
00:25:26,280 --> 00:25:30,560
you can just say hearing, hearing, it makes you feel a lot better, more at peace, much

172
00:25:30,560 --> 00:25:39,560
better able to respond when they give you a turn, perhaps hard to ingest and understand

173
00:25:39,560 --> 00:25:46,080
what they're saying and really contemplate it, but honestly, if you're mindful, you can

174
00:25:46,080 --> 00:25:51,800
get to just of what people say and then keep it simple, keep it down to earth.

175
00:25:51,800 --> 00:26:02,640
You won't get caught up in there in any kind of mental, rational, rational, rational

176
00:26:02,640 --> 00:26:14,040
rational, rational, formal sitting meditation, social rebalance, it was walking meditation.

177
00:26:14,040 --> 00:26:22,760
The balancing walking with sitting is more, when you're doing a meditation intensively,

178
00:26:22,760 --> 00:26:27,160
because you can't just sit around all day, you know, if you're just doing meditation

179
00:26:27,160 --> 00:26:32,080
in daily life, you don't have to balance and it doesn't mean that you have to balance

180
00:26:32,080 --> 00:26:42,320
as a rule because, you know, it's quite, you can meditate in any position, it's meaningless

181
00:26:42,320 --> 00:26:45,400
how much walking and how much sitting you do.

182
00:26:45,400 --> 00:26:51,600
What's nice about balancing walking and sitting is, it challenges your partiality, if

183
00:26:51,600 --> 00:26:58,640
you're partial to sitting, it challenges your partial to walking, it challenges you by

184
00:26:58,640 --> 00:27:03,560
changing, by taking you out of your comfort zone, by forcing you to be flexible, by changing

185
00:27:03,560 --> 00:27:13,240
the condition, teaching you to deal with impermanence.

186
00:27:13,240 --> 00:27:17,560
I notice I have become more intolerant of loud noises after I begin meditating.

187
00:27:17,560 --> 00:27:20,560
Isn't the opposite supposed to happen?

188
00:27:20,560 --> 00:27:22,800
Yes, the opposite is supposed to happen.

189
00:27:22,800 --> 00:27:27,280
I mean, the beginning, I mentioned that I've talked about this, in the beginning you're

190
00:27:27,280 --> 00:27:33,160
just allowing yourself to have reactions rather than reacting, like you have a reaction,

191
00:27:33,160 --> 00:27:40,640
I don't like this, so you run away from it, instead you're allowing the reaction to come.

192
00:27:40,640 --> 00:27:45,840
Let that come up when you're dislike loud noises, focus on the disliking, the other thing

193
00:27:45,840 --> 00:27:50,840
is your mind is much more focused, so you're able to see the experience, it's clear, so

194
00:27:50,840 --> 00:27:56,920
they seem more intense, but they should in general be more intense because you're allowing

195
00:27:56,920 --> 00:28:03,360
them to happen, you're allowing your mind to, you know, you're studying the nature of your

196
00:28:03,360 --> 00:28:14,600
mind, so you're no longer reacting to your reactions and trying to avoid them, and distract

197
00:28:14,600 --> 00:28:19,480
yourself, you're no longer distracting yourself from your actions, you're focusing on them,

198
00:28:19,480 --> 00:28:24,440
so you're getting to see them, and rather than reacting to them, just let them be when

199
00:28:24,440 --> 00:28:32,680
you're angry, let it be anger, say angry, angry, but eventually, yeah, you should become

200
00:28:32,680 --> 00:28:41,760
more tolerant, but again, you know, if you're worrying, if you're worrying about you're

201
00:28:41,760 --> 00:28:48,520
not getting good results, that worrying is getting in the way of your meditation, and

202
00:28:48,520 --> 00:28:53,800
you won't be able to see results that way, if you want to see the benefit of the meditation

203
00:28:53,800 --> 00:28:59,640
it has to be the benefit of the moment of meditation, when you're mindful at that moment,

204
00:28:59,640 --> 00:29:05,640
you can't look at it from a protracted, am I getting better at playing to view, because

205
00:29:05,640 --> 00:29:10,640
the mind is very complicated, it gets better, it gets worse, it's got all sorts of mess

206
00:29:10,640 --> 00:29:20,800
and jumble of good and bad things inside, I understand how meditation affects future karma,

207
00:29:20,800 --> 00:29:30,840
what could you explain how it affects past karma, it doesn't affect past karma, it affects

208
00:29:30,840 --> 00:29:36,520
the results of past karma, so if you've done something bad, but then you do something

209
00:29:36,520 --> 00:29:41,720
good, well, sometimes the good thing you do can cancel it out, meditation is a good thing,

210
00:29:41,720 --> 00:29:46,480
so it can affect bad and good things you've done in the past, affect the results of those

211
00:29:46,480 --> 00:29:53,720
things, certainly can't affect the karma that karma has done, how do you deal with hunger

212
00:29:53,720 --> 00:29:59,000
pains, I'm on my third day of not eating after 12 and it's harder than I thought, you

213
00:29:59,000 --> 00:30:02,440
know, you should really only do that if you're doing intensive meditation, otherwise

214
00:30:02,440 --> 00:30:07,720
yeah, it should be probably pretty difficult, I mean I guess you'll learn to eat a lot more

215
00:30:07,720 --> 00:30:17,440
in the morning, like quite a bit in the morning, but yeah, it's easier, it's mostly easy

216
00:30:17,440 --> 00:30:22,520
when you're doing meditation, when you're living a life that's not stressful, if you

217
00:30:22,520 --> 00:30:28,160
have to work all day, like I went, I was doing eight precepts and I was eating only in

218
00:30:28,160 --> 00:30:34,080
the morning and then I needed to get money to go to Thailand, this is when I actually ended

219
00:30:34,080 --> 00:30:41,560
up becoming a monk, and so I spent the summer tree planting and it four meals a day,

220
00:30:41,560 --> 00:30:49,720
as you had to, it's quite strenuous work, do you ever think it's a good idea to take

221
00:30:49,720 --> 00:30:53,400
a break from meditation at times, I felt like I needed to take a step back and have

222
00:30:53,400 --> 00:30:58,600
taken a week off or so could be, it's more likely if you don't have a teacher because

223
00:30:58,600 --> 00:31:03,280
it's easy to get on the wrong path, it's easy to practice in a way that makes you more

224
00:31:03,280 --> 00:31:13,000
stressed, that you're just plowing ahead, like an ox, and not actually practicing correctly,

225
00:31:13,000 --> 00:31:18,240
so sometimes stepping back and trying again, in fact in a sense meditation is always

226
00:31:18,240 --> 00:31:27,400
like that, it's about trying, doing it wrong, trying again, doing it wrong, giving up and

227
00:31:27,400 --> 00:31:34,440
then trying in a new way, trying it with a fresh view, as meditation is about changing

228
00:31:34,440 --> 00:31:40,080
the nature of your mind, the way we react to things, and the beginning your meditation

229
00:31:40,080 --> 00:31:47,120
is going to be all wrong, all wrong, they have to step back and start over, try again,

230
00:31:47,120 --> 00:31:52,440
and you'll slowly begin to change the way you meditate, that's key, it's not like you

231
00:31:52,440 --> 00:31:58,960
start meditating, and just keep doing it that way and you'll get results, it's not what

232
00:31:58,960 --> 00:32:05,240
meditation is, meditation is changing who you are, so thereby therefore also changing

233
00:32:05,240 --> 00:32:11,960
how you meditate, your meditation will change, the way you meditate will change if you're

234
00:32:11,960 --> 00:32:41,560
progressing, I'm worried that I might accumulate a version against

235
00:32:41,560 --> 00:32:47,240
meditation over time, and eventually give up, how can I prevent this, don't try to prevent

236
00:32:47,240 --> 00:32:53,640
anything, if you have a version towards meditation great, that's a good object of meditation,

237
00:32:53,640 --> 00:32:58,240
if you're worried that you might accumulate a version against meditation, that's great,

238
00:32:58,240 --> 00:33:03,720
that's worry, good for you to see it, learn about that worry, come to see that that worry

239
00:33:03,720 --> 00:33:08,800
is the problem, and that worry is stressful for you, and once you give up that worry

240
00:33:08,800 --> 00:33:13,000
you will be happier, you will be more at peace, I mean you don't have to go through all

241
00:33:13,000 --> 00:33:18,760
that, just say to yourself worry, you'll be able to see all of that, your mind will

242
00:33:18,760 --> 00:33:23,320
become clear and you'll feel better, when you have a version, when you do have a version

243
00:33:23,320 --> 00:33:29,600
towards your meditation, just say, just liking, just liking, you see, this is the mind,

244
00:33:29,600 --> 00:33:35,520
you can't step out like that, at the time when you have a version towards meditation,

245
00:33:35,520 --> 00:33:47,720
that's an important time to meditate, it's important that you meditate on that.

246
00:33:47,720 --> 00:33:52,880
Just to clarify, you advise to the formal meditation of listening to Dhamma talks, I do,

247
00:33:52,880 --> 00:34:09,760
I definitely do, I, it is possible to have great meditative results, great that you can

248
00:34:09,760 --> 00:34:14,960
attain to, you can attain in Bhana when listening to the Dhamma, you can become enlightened

249
00:34:14,960 --> 00:34:20,160
listening, if you meditate listening to the Dhamma, it's a great opportunity, because

250
00:34:20,160 --> 00:34:23,760
whatever the person, whatever the teacher is saying, if it's about the Dhamma, it will keep

251
00:34:23,760 --> 00:34:30,640
bringing you back to the person moment, so do meditate during the talk, there was this

252
00:34:30,640 --> 00:34:36,800
guy, you see this picture, I think it's in the video, right, you can see him, yeah, this

253
00:34:36,800 --> 00:34:42,880
guy up here, he always says, if you listen to his talks, he's passed away, but his talks

254
00:34:42,880 --> 00:34:48,320
are in mp3 and he'll say, you can put your hands down, I'm going to give a talk, put

255
00:34:48,320 --> 00:34:54,640
your hands down, and while you're listening, note at the ear hearing, hearing, and then he'll

256
00:34:54,640 --> 00:35:00,080
explain why that's the best way to listen to the Dhamma, there's different reasons for listening

257
00:35:00,080 --> 00:35:05,040
to the Dhamma, some people just do it to get knowledge, some people do it just because they know

258
00:35:05,040 --> 00:35:10,720
it's a good thing, so they do it for the good karma of listening to the Dhamma, but if you want

259
00:35:10,720 --> 00:35:18,800
to do it for, to become enlightened, you have to practice, it has to be a meditation practice.

260
00:35:20,640 --> 00:35:25,680
Is it beneficial to me to very accomplished meditator, so to put in beyond, yeah sure,

261
00:35:28,080 --> 00:35:33,920
it's quite beneficial, right, and Banditana, just say, when I

262
00:35:33,920 --> 00:35:42,080
are associating with the wise, some mananant and asanant to see the, the recklessness,

263
00:35:43,120 --> 00:35:54,080
people who are association with the wise, and good friendship, only Buddha talked about

264
00:35:54,080 --> 00:35:57,760
these quite often, you know, all these things talked about how great they are.

265
00:35:57,760 --> 00:36:11,120
But Ajahn Tang gives, gives good advice that is memorable in his, I think he once gave

266
00:36:11,120 --> 00:36:17,600
this talk and it was recorded and turned into a book, adapted into a book, and where he said

267
00:36:17,600 --> 00:36:29,200
Gandh Kalka, Ali Adjal could be, Gandh Nooka, Ali Adjal could be, Maidi Thal,

268
00:36:31,040 --> 00:36:35,520
Gandh Kataam Light on the Inkhaibin, or Ali Adjal, something like that,

269
00:36:36,880 --> 00:36:44,960
which means whether going to see an enlightened being, or staying with an enlightened being,

270
00:36:44,960 --> 00:36:49,680
neither of these are as good as becoming an enlightened being yourself.

271
00:36:56,800 --> 00:37:04,320
Who is the monk in the picture, that's Lunthol Shodok, Shodok, which would be Jodika, Jodok,

272
00:37:04,320 --> 00:37:15,520
Jodoka, Jodika, I guess was his name, Jodok, they say in Thai. He was a number one poly scholar in

273
00:37:15,520 --> 00:37:22,320
Thailand. Apparently I heard a story about the year he became the top level poly.

274
00:37:24,400 --> 00:37:27,280
He was the only one who got, who passed the exams.

275
00:37:27,280 --> 00:37:36,160
And the funny thing is that he didn't actually pass. He did the top of a poly exams and then he

276
00:37:36,160 --> 00:37:41,680
went to Burma to practice meditation. This is what I've heard, I don't know. It's just hearsay,

277
00:37:41,680 --> 00:37:48,880
but this monk who actually lived with him was telling me the story. He said, so he was

278
00:37:48,880 --> 00:37:53,200
often Burma and then the people were marking all these exams and they found that not a single

279
00:37:53,200 --> 00:37:58,000
person had passed the exams that year and they couldn't have that. Someone had to pass. So they

280
00:37:58,000 --> 00:38:03,760
tried to find who was least wrong, who failed the least. The exams are really that hard.

281
00:38:04,880 --> 00:38:10,880
And he was the one who failed the least. So he got the top level poly, but at that time he was

282
00:38:10,880 --> 00:38:16,720
often Burma and he was practicing meditation. He practiced meditation in Bangkok. Actually it's

283
00:38:16,720 --> 00:38:22,400
not true. He practiced meditation in Bangkok and went to Burma to see how they did things there as

284
00:38:22,400 --> 00:38:33,440
well, but his practice was mostly in Bangkok. But he became, after Asaba, this very important

285
00:38:33,440 --> 00:38:41,200
monk became, he went to prison. He became the head meditation teacher in Bangkok.

286
00:38:46,160 --> 00:38:51,520
I've heard the phrase about transcending the three realms. Well, that's a topic for a long

287
00:38:51,520 --> 00:38:59,920
discourse probably. But basically the three realms are the gamma, the realm of seeing, hearing,

288
00:38:59,920 --> 00:39:08,080
smelling, tasting and feeling. The realm of course materiality of sensual pleasures, of sense

289
00:39:08,080 --> 00:39:14,960
desires, of the enjoyment of seeing and hearing and smelling and tasting and feeling. The

290
00:39:14,960 --> 00:39:23,600
Rupa Wachura is a form, the realm of fine material, a form where there is, there is form, but

291
00:39:27,040 --> 00:39:36,400
not sensuality. There's no sex, there's no eating and drinking and listening to music. There's just

292
00:39:36,400 --> 00:39:46,320
form, mind and body. Then there's the Rupa Wachura, which is just mind, no body, no form.

293
00:39:47,520 --> 00:39:52,640
So the Rupa Wachura and Rupa Wachura, the form and formless, these are the Brahma realms, these are

294
00:39:52,640 --> 00:39:58,960
god realms. So there are god realms that have form and there are god realms that don't have form.

295
00:39:59,680 --> 00:40:05,520
But you have to kind of go into more detail about how you get there and what they're involved

296
00:40:05,520 --> 00:40:13,280
and what they exactly need. It's involving really good discussion. You have to learn the

297
00:40:13,280 --> 00:40:22,800
abbey done, I think. Okay, well this is great. It's been fun trying this out. We'll have to

298
00:40:22,800 --> 00:40:29,920
see how it works, see what the response is. I don't know, I mean this is a whole new interface,

299
00:40:29,920 --> 00:40:37,360
but I got this neat app that I can use. You see? So I can put things like this here.

300
00:40:39,920 --> 00:40:45,920
I've got this. I mean I could put some fancy introduction screen someday and I can do a desktop.

301
00:40:45,920 --> 00:40:51,200
I can show you my desktop and it fades in and out and I've got multiple audio sources,

302
00:40:51,200 --> 00:40:59,520
really need app. It's open source for Linux. Anyway, I'm going to stop there. So I wish you

303
00:40:59,520 --> 00:41:04,320
all a good night and hope to see you all tomorrow. If you have any questions that I haven't

304
00:41:04,320 --> 00:41:10,960
answered, please feel free to ask them again. I wasn't purposely ignoring you. It's just

305
00:41:10,960 --> 00:41:32,480
time to go. So have a good night, wishing you all the best.

