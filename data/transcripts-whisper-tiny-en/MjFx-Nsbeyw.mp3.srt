1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhamupanda.

2
00:00:05,000 --> 00:00:13,000
Today we look at verse 174, which reads as follows.

3
00:00:13,000 --> 00:00:19,000
And the Buddha, Ayangloco, the Nuket, how we pass it in.

4
00:00:19,000 --> 00:00:31,000
So Kuntou, Jalamutova, a person, guy, you got your name, which means blind and the Buddha.

5
00:00:31,000 --> 00:00:34,000
Blind is this world.

6
00:00:34,000 --> 00:00:45,000
Few here, see clearly, we pass it in.

7
00:00:45,000 --> 00:00:53,000
Just like birds that free themselves from a net, not so many.

8
00:00:53,000 --> 00:00:56,000
A bird caught up in a net is well caught.

9
00:00:56,000 --> 00:01:03,000
Fewer those who go to heaven, as few as those birds that are able to free themselves from a net.

10
00:01:03,000 --> 00:01:14,000
So this story, this verse, was told in regards to another fairly famous story.

11
00:01:14,000 --> 00:01:26,000
It's the story of the Pesakara Dita, the daughter of a weaver, a weaver's daughter.

12
00:01:26,000 --> 00:01:40,000
The Buddha was dwelling at Alavi, and then Alavi, he was invited for the meal, and he was invited to teach.

13
00:01:40,000 --> 00:01:49,000
So after eating, he began to teach the Dhamma.

14
00:01:49,000 --> 00:01:53,000
He happened to be teaching on that day because it was a general audience, I guess.

15
00:01:53,000 --> 00:02:02,000
He was teaching on death, so he taught them practice meditation on death.

16
00:02:02,000 --> 00:02:12,000
Adhu Wang Mi, Ji with my life is unstable, unstable, because you don't know.

17
00:02:12,000 --> 00:02:16,000
It's not going to last forever, and death could come at any time.

18
00:02:16,000 --> 00:02:25,000
But the Dhamma was a very small person, and I was a young person in the end of my life.

19
00:02:25,000 --> 00:02:32,000
I was a little older than a young person, and I was a little older than a little older than a young person.

20
00:02:32,000 --> 00:02:35,000
So you can actually meditate on this.

21
00:02:35,000 --> 00:02:43,000
Of course you can do it in English because the Polys are probably not very meaningful for most people.

22
00:02:43,000 --> 00:02:46,000
And death is the end of my life.

23
00:02:46,000 --> 00:02:51,000
My life is uncertain, death is certain.

24
00:02:51,000 --> 00:02:56,000
And they tell you, he said, practice this way, cultivate this.

25
00:02:56,000 --> 00:03:00,000
And then he left.

26
00:03:00,000 --> 00:03:11,000
And in the entire audience, I guess it was a large audience, only one person was...

27
00:03:11,000 --> 00:03:14,000
Only one person was affected by it, was moved by it.

28
00:03:14,000 --> 00:03:17,000
Everybody else went back to their daily life.

29
00:03:17,000 --> 00:03:23,000
That's interesting, back to the same old thing.

30
00:03:23,000 --> 00:03:29,000
But there was one young girl, and she was the daughter of a weaver.

31
00:03:29,000 --> 00:03:33,000
And it's the story.

32
00:03:33,000 --> 00:03:40,000
The story is about her, and she took it quite seriously.

33
00:03:40,000 --> 00:03:45,000
And so from then on, she remember the Buddha's words, and she would repeat that to herself.

34
00:03:45,000 --> 00:03:54,000
And the one, the baby, we don't do a long, modern now, and so on.

35
00:03:54,000 --> 00:03:59,000
Every day, constantly, for three years.

36
00:03:59,000 --> 00:04:09,000
And it so happened that three years later, the Buddha, and they say that every night the Buddha would enter into some kind of higher state of mind.

37
00:04:09,000 --> 00:04:16,000
And send his mind out, and get a sense of who was able to understand his teachings.

38
00:04:16,000 --> 00:04:18,000
Every night he would do this.

39
00:04:18,000 --> 00:04:24,000
Three years later, on one night, the Buddha was reflecting.

40
00:04:24,000 --> 00:04:34,000
And he thought about this girl, and he thought, she, by now, she is well able to understand my teaching.

41
00:04:34,000 --> 00:04:39,000
And so he went back to Alavi. Of course, Alavi wasn't where he was living most of the time.

42
00:04:39,000 --> 00:04:42,000
Most of the time he was in Titoana.

43
00:04:42,000 --> 00:04:47,000
But he traveled wherever, back to wherever Alavi is.

44
00:04:47,000 --> 00:04:53,000
And again, he was invited for people remembered and thought, oh, this is a wise man.

45
00:04:53,000 --> 00:04:58,000
They hadn't done anything about his teaching, but oh, yes, well.

46
00:04:58,000 --> 00:05:01,000
What he says is true.

47
00:05:01,000 --> 00:05:16,000
People who believe in faith in teaching is a lot more than those who actually put into practice, as far as religion goes.

48
00:05:16,000 --> 00:05:31,000
So they had faith in him, and confidence, and appreciation, and so they invited him, and then again invited him to teach.

49
00:05:31,000 --> 00:05:33,000
Buddha sat down and he didn't teach.

50
00:05:33,000 --> 00:05:40,000
He just sat there.

51
00:05:40,000 --> 00:05:53,000
So it turns out this young girl who had diligently practiced his teaching for three years, turns out that she heard that the Buddha was coming, and she was very excited.

52
00:05:53,000 --> 00:06:01,000
But then her father said to her, oh, I need more, I need to fill this shuttle they call.

53
00:06:01,000 --> 00:06:10,000
The shuttle is the thing you send back and forth between the moon, and I need you to fill it up with thread.

54
00:06:10,000 --> 00:06:12,000
I just thought, oh, no.

55
00:06:12,000 --> 00:06:14,000
I'll miss that sermon.

56
00:06:14,000 --> 00:06:17,000
Well, if I don't, my father will beat me.

57
00:06:17,000 --> 00:06:19,000
That's what the commentary says.

58
00:06:19,000 --> 00:06:23,000
Her father wasn't a nice guy, I guess.

59
00:06:23,000 --> 00:06:26,000
Or maybe she was, I don't know.

60
00:06:26,000 --> 00:06:28,000
Anyway, this is what she thought to herself.

61
00:06:28,000 --> 00:06:31,000
She said, even Peter, so I better go and fill it up.

62
00:06:31,000 --> 00:06:32,000
So she went to the workshop.

63
00:06:32,000 --> 00:06:34,000
I guess was far away from the house.

64
00:06:34,000 --> 00:06:41,000
She had to walk some ways through the town, get to the workshop, and fill up the shuttle.

65
00:06:41,000 --> 00:06:44,000
And on her way back, she took a detour.

66
00:06:44,000 --> 00:06:49,000
But this took her some time, and during this time the Buddha waited for her.

67
00:06:49,000 --> 00:06:57,000
I came all this way just to teach her, because these other people, well, we know what happens when they hear the good teaching.

68
00:06:57,000 --> 00:07:01,000
So he waited.

69
00:07:01,000 --> 00:07:05,000
And it says that everyone else, when he didn't speak, no one else spoke as well,

70
00:07:05,000 --> 00:07:10,000
because when the Buddha is sitting quiet, nobody dares to speak.

71
00:07:10,000 --> 00:07:13,000
So they all just waited.

72
00:07:13,000 --> 00:07:21,000
Finally, this young daughter of a weaver arrives at the congregation.

73
00:07:21,000 --> 00:07:29,000
That stands in the back and looks and tries to get an idea of what the Buddha is teaching.

74
00:07:29,000 --> 00:07:36,000
The Buddha looks, turns, and he looks at her.

75
00:07:36,000 --> 00:07:39,000
And she knows right away the Buddha once.

76
00:07:39,000 --> 00:07:41,000
Once to talk to me.

77
00:07:41,000 --> 00:07:44,000
I guess everyone else probably saw this, and turned to a seal.

78
00:07:44,000 --> 00:07:52,000
Who is this person? The Buddha is looking either way.

79
00:07:52,000 --> 00:07:57,000
So she walks up to where the Buddha is sitting.

80
00:07:57,000 --> 00:07:59,000
Stands right in front of him.

81
00:07:59,000 --> 00:08:00,000
Maybe he sits down.

82
00:08:00,000 --> 00:08:07,000
No, until she does.

83
00:08:07,000 --> 00:08:09,000
No, she stands.

84
00:08:09,000 --> 00:08:14,000
The Buddha was probably on a higher seat.

85
00:08:14,000 --> 00:08:21,000
They tended to put religious teachers up a bit high, so she stood in front of where he was sitting.

86
00:08:21,000 --> 00:08:27,000
And the Buddha asks her four questions.

87
00:08:27,000 --> 00:08:40,000
The Buddha, Kumaarike, young girl, Kuto Aagachase, where are you coming from?

88
00:08:40,000 --> 00:08:42,000
And she replies,

89
00:08:42,000 --> 00:09:06,000
Not Janami, but they cut a gamissacy where are you going?

90
00:09:06,000 --> 00:09:13,000
Not Janami, but I don't know.

91
00:09:13,000 --> 00:09:19,000
And the whole crowd starts to get quite upset and says,

92
00:09:19,000 --> 00:09:20,000
Who is this girl?

93
00:09:20,000 --> 00:09:21,000
What kind of answers are these?

94
00:09:21,000 --> 00:09:23,000
The Buddha asks her where she's coming from me.

95
00:09:23,000 --> 00:09:24,000
She says, I don't know.

96
00:09:24,000 --> 00:09:29,000
You know, are you coming from your father's workshop?

97
00:09:29,000 --> 00:09:32,000
You've got this shuttle here, clearly.

98
00:09:32,000 --> 00:09:36,000
You've asked her where you're going, you say, you don't know, you're going home.

99
00:09:36,000 --> 00:09:39,000
Obviously, you know where you're going.

100
00:09:39,000 --> 00:09:40,000
And then what's this?

101
00:09:40,000 --> 00:09:41,000
You don't know.

102
00:09:41,000 --> 00:09:42,000
But a challenge is you.

103
00:09:42,000 --> 00:09:43,000
And you say, you know.

104
00:09:43,000 --> 00:09:46,000
And then he says, asks you to confirm that you know.

105
00:09:46,000 --> 00:09:49,000
And you say, you don't know.

106
00:09:49,000 --> 00:09:50,000
Don't you know, I don't know.

107
00:09:50,000 --> 00:09:52,000
Do you know, I don't know.

108
00:09:52,000 --> 00:09:53,000
What a crazy young girl.

109
00:09:53,000 --> 00:09:58,000
What an impudent little.

110
00:09:58,000 --> 00:10:01,000
Bracked.

111
00:10:01,000 --> 00:10:04,000
And when the Buddha holds up his hand or something,

112
00:10:04,000 --> 00:10:07,000
he gets to be quiet.

113
00:10:07,000 --> 00:10:09,000
And he asks her, what did you mean?

114
00:10:09,000 --> 00:10:11,000
When did you mean by those answers?

115
00:10:11,000 --> 00:10:13,000
And she says, well, I know.

116
00:10:13,000 --> 00:10:16,000
The Buddha doesn't just ask idle questions.

117
00:10:16,000 --> 00:10:21,000
So then clearly, you're not asking about me coming.

118
00:10:21,000 --> 00:10:25,000
You know where I'm coming from.

119
00:10:25,000 --> 00:10:27,000
And I know how you teach.

120
00:10:27,000 --> 00:10:30,000
I've heard you teach before.

121
00:10:30,000 --> 00:10:34,000
And so I know when you ask, where are you coming from?

122
00:10:34,000 --> 00:10:37,000
It's about reality, about life.

123
00:10:37,000 --> 00:10:39,000
And I don't know where I'm coming from.

124
00:10:39,000 --> 00:10:41,000
Before I was born, that's all I have.

125
00:10:41,000 --> 00:10:43,000
That's the only memory I have.

126
00:10:43,000 --> 00:10:46,000
I don't even remember my birth.

127
00:10:46,000 --> 00:10:49,000
But before that, no idea.

128
00:10:49,000 --> 00:10:51,000
And where am I going?

129
00:10:51,000 --> 00:10:53,000
I also don't know.

130
00:10:53,000 --> 00:10:59,000
It depends very much on my state of mind, as you've talked.

131
00:10:59,000 --> 00:11:03,000
And then the other two, when you ask me, don't I know?

132
00:11:03,000 --> 00:11:07,000
I have to admit that I do know that I will die.

133
00:11:07,000 --> 00:11:10,000
So I do know something.

134
00:11:10,000 --> 00:11:13,000
But then when you challenge me, do I really know?

135
00:11:13,000 --> 00:11:17,000
And actually, about my death, even about my death, I don't know.

136
00:11:17,000 --> 00:11:18,000
I don't know where it's going to be.

137
00:11:18,000 --> 00:11:23,000
I don't know why it's going to happen.

138
00:11:23,000 --> 00:11:25,000
I don't know where they're going to throw my body.

139
00:11:25,000 --> 00:11:27,000
I don't know where my mind's going to go.

140
00:11:27,000 --> 00:11:29,000
I don't know much about anything at all.

141
00:11:29,000 --> 00:11:33,000
Nothing is certain.

142
00:11:33,000 --> 00:11:36,000
And the Buddha says sad.

143
00:11:36,000 --> 00:11:38,000
Does he hold up his hand?

144
00:11:38,000 --> 00:11:40,000
Sometimes he holds up his hand.

145
00:11:40,000 --> 00:11:42,000
Sadukarang dattva.

146
00:11:42,000 --> 00:11:46,000
He said sadu three times.

147
00:11:46,000 --> 00:11:53,000
I know he says sadu after everyone, every time.

148
00:11:53,000 --> 00:11:56,000
Sadukarang dattva.

149
00:11:56,000 --> 00:11:58,000
And then he asks me about the second question.

150
00:11:58,000 --> 00:12:01,000
What do you mean about this one?

151
00:12:01,000 --> 00:12:10,000
And then he turns to the people and he says, all of you,

152
00:12:10,000 --> 00:12:12,000
all of you have no idea.

153
00:12:12,000 --> 00:12:14,000
All of you, you couldn't understand my questions.

154
00:12:14,000 --> 00:12:18,000
You could just like you couldn't understand my teaching.

155
00:12:18,000 --> 00:12:21,000
Because of your delusion, what does he say?

156
00:12:21,000 --> 00:12:26,000
Because you're blind.

157
00:12:26,000 --> 00:12:30,000
And you scolded her.

158
00:12:30,000 --> 00:12:35,000
Because you're blind.

159
00:12:35,000 --> 00:12:48,000
Because only those who have the eye of which the only they can see.

160
00:12:48,000 --> 00:12:54,000
And then he pronounced the verse.

161
00:12:54,000 --> 00:12:56,000
So there's a few lessons here.

162
00:12:56,000 --> 00:12:57,000
Two lessons I guess.

163
00:12:57,000 --> 00:12:58,000
The lesson of the story.

164
00:12:58,000 --> 00:13:03,000
I mean the big lesson that the story gives us is about mindfulness of death,

165
00:13:03,000 --> 00:13:06,000
which is a curious thing as to why the Buddha taught mindfulness of death.

166
00:13:06,000 --> 00:13:12,000
You might ask, is that really the Buddha's teaching?

167
00:13:12,000 --> 00:13:15,000
In fact, mindfulness of death is quite useful.

168
00:13:15,000 --> 00:13:20,000
It's a very useful gateway to bring people

169
00:13:20,000 --> 00:13:22,000
to understand about impermanence.

170
00:13:22,000 --> 00:13:25,000
And it is a teaching on impermanence.

171
00:13:25,000 --> 00:13:29,000
We live our lives often as though we're going to live forever.

172
00:13:29,000 --> 00:13:32,000
Maybe we don't have the view, I'm going to live forever.

173
00:13:32,000 --> 00:13:39,000
But our short-sightedness and our narrow-mindedness is such that everything we do

174
00:13:39,000 --> 00:13:48,000
is only for our present gratification is for building up results

175
00:13:48,000 --> 00:13:58,000
that are in conflict with our deaths,

176
00:13:58,000 --> 00:14:01,000
such that when we die, we're going to lose it all.

177
00:14:01,000 --> 00:14:04,000
You become, suppose you become smart.

178
00:14:04,000 --> 00:14:06,000
You learn a lot.

179
00:14:06,000 --> 00:14:09,000
You forget it all when you die.

180
00:14:09,000 --> 00:14:15,000
Suppose you amass all sorts of wealth and security,

181
00:14:15,000 --> 00:14:19,000
this house, and I look at my security and you put money in the bank

182
00:14:19,000 --> 00:14:24,000
and you've got a good retirement.

183
00:14:24,000 --> 00:14:28,000
It's not wrong to do that, but if that's all you're living for,

184
00:14:28,000 --> 00:14:33,000
it's in conflict with the fact that you lose all that security

185
00:14:33,000 --> 00:14:36,000
and just start all over.

186
00:14:36,000 --> 00:14:41,000
That's what you've got your goal to starting over and do this again

187
00:14:41,000 --> 00:14:45,000
and again, that's one thing, but mostly we do it with the idea

188
00:14:45,000 --> 00:14:48,000
that it's going to bring us satisfaction,

189
00:14:48,000 --> 00:14:50,000
which of course it can do.

190
00:14:50,000 --> 00:14:55,000
It's only temporary.

191
00:14:55,000 --> 00:14:59,000
There's a mindfulness of death is a great eye opener.

192
00:14:59,000 --> 00:15:01,000
It reminds you that there's something more.

193
00:15:01,000 --> 00:15:04,000
If you're going to do all this,

194
00:15:04,000 --> 00:15:07,000
you're missing out on the most important aspect of reality.

195
00:15:07,000 --> 00:15:11,000
Life is much more than being a human in this life,

196
00:15:11,000 --> 00:15:15,000
it's much more than this person that I've been born as.

197
00:15:15,000 --> 00:15:18,000
We live our lives as though we are this person,

198
00:15:18,000 --> 00:15:20,000
which is kind of funny from a Buddhist perspective,

199
00:15:20,000 --> 00:15:24,000
because it's more like a role we're playing for a short time.

200
00:15:24,000 --> 00:15:27,000
We've given us this funny name.

201
00:15:27,000 --> 00:15:30,000
We've got this funny shape body with all these funny things.

202
00:15:30,000 --> 00:15:33,000
All these funny things in it.

203
00:15:33,000 --> 00:15:36,000
Not so funny, actually.

204
00:15:36,000 --> 00:15:38,000
A lot of suffering and stress.

205
00:15:38,000 --> 00:15:40,000
But it's not us.

206
00:15:40,000 --> 00:15:48,000
It's just a short temporary occupation in the whole scheme of things.

207
00:15:48,000 --> 00:15:56,000
It's only a very short stint of being this person.

208
00:15:56,000 --> 00:16:00,000
It's useful. If you want to talk to people about Buddhism,

209
00:16:00,000 --> 00:16:05,000
I mean, one of the first things you can do for people who are...

210
00:16:05,000 --> 00:16:07,000
It doesn't mean new to Buddhism,

211
00:16:07,000 --> 00:16:10,000
but people who aren't thinking spiritually.

212
00:16:10,000 --> 00:16:13,000
To get them to start thinking spiritually is that well,

213
00:16:13,000 --> 00:16:15,000
you're going to die.

214
00:16:15,000 --> 00:16:17,000
Are you ready for death?

215
00:16:17,000 --> 00:16:20,000
Are you living in accordance with this fact?

216
00:16:20,000 --> 00:16:34,000
Are you living with ignorance of the fact that the things you do might be taken away from you at any time?

217
00:16:34,000 --> 00:16:44,000
And the things you have obtained or attained are going to be obliterated or meaningless.

218
00:16:44,000 --> 00:16:47,000
The lesson from the verse is a little bit different.

219
00:16:47,000 --> 00:16:51,000
When it talks about blindness.

220
00:16:51,000 --> 00:16:54,000
So I think there's two things we get from the verse.

221
00:16:54,000 --> 00:16:56,000
The first is a sense of urgency.

222
00:16:56,000 --> 00:17:00,000
A reminder that Buddhism is not a simple teaching.

223
00:17:00,000 --> 00:17:05,000
It's fine if you are a sort of person who...

224
00:17:05,000 --> 00:17:12,000
I mean, it isn't evil or wrong to be a person who appreciates the Buddhist teachings

225
00:17:12,000 --> 00:17:18,000
or reveres the Buddha and calls yourself a Buddhist because you have such faith in the Buddha.

226
00:17:18,000 --> 00:17:21,000
When there's wholesomeness there.

227
00:17:21,000 --> 00:17:24,000
But that's not really...

228
00:17:24,000 --> 00:17:28,000
It's certainly not all the Buddha taught.

229
00:17:28,000 --> 00:17:33,000
And it's not even necessarily enough to help you to go to heaven

230
00:17:33,000 --> 00:17:38,000
or to go to a good place, to have a good future

231
00:17:38,000 --> 00:17:40,000
of their Buddhists who do very rotten things.

232
00:17:40,000 --> 00:17:44,000
Nowadays you hear a lot of rotten things that Buddhists do.

233
00:17:44,000 --> 00:17:52,000
There's apparently a lot of violence in Buddhist countries these days, which is very bizarre.

234
00:17:52,000 --> 00:17:59,000
Not only if you've been to these countries, you understand how religion becomes institutionalized

235
00:17:59,000 --> 00:18:07,000
and then it becomes corrupted, so that it's much more important to hold on to your institutions

236
00:18:07,000 --> 00:18:15,000
than to actually adhere to the teachings.

237
00:18:15,000 --> 00:18:24,000
So a reminder that most people are blind, fewer those who actually seek clearly

238
00:18:24,000 --> 00:18:33,000
that it's not enough to just be an evolutionary person

239
00:18:33,000 --> 00:18:37,000
but isn't about living your life in an ordinary way.

240
00:18:37,000 --> 00:18:41,000
There's much more than coming to see clearly as really what needs to be done.

241
00:18:41,000 --> 00:18:49,000
If you want to go to a good place, the only real reassurance is to have clear insight into how things work.

242
00:18:49,000 --> 00:18:51,000
Without that insight, you're so...

243
00:18:51,000 --> 00:18:56,000
In so much danger of doing the wrong thing, building the wrong habits

244
00:18:56,000 --> 00:19:01,000
of concentrating your mind in the wrong way because of your ignorance.

245
00:19:01,000 --> 00:19:06,000
So it's not about teaching you or believing in certain teachings.

246
00:19:06,000 --> 00:19:09,000
It's about seeing for yourself.

247
00:19:09,000 --> 00:19:15,000
And the other side of that is for those of us who are practicing this is an encouragement.

248
00:19:15,000 --> 00:19:17,000
It's an encouragement and it's a young girl.

249
00:19:17,000 --> 00:19:23,000
It's an encouragement to everyone who undertakes to practice inside meditation

250
00:19:23,000 --> 00:19:28,000
that you're doing something that's quite rare, quite valuable,

251
00:19:28,000 --> 00:19:32,000
quite worthwhile, quite powerful.

252
00:19:32,000 --> 00:19:36,000
There's the power to bring you a good future.

253
00:19:36,000 --> 00:19:42,000
A guarantee of happiness and peace.

254
00:19:42,000 --> 00:19:45,000
Freedom from suffering.

255
00:19:45,000 --> 00:19:49,000
So at the end of the verse, the young girl became a Sota Pandas.

256
00:19:49,000 --> 00:19:52,000
She was primed for it, quite a special person,

257
00:19:52,000 --> 00:19:56,000
just standing there, listening, standing up, she became a Sota Pandas.

258
00:19:56,000 --> 00:20:00,000
This is what the commentary says, believe it or not, as you like.

259
00:20:00,000 --> 00:20:02,000
But then she apparently went home.

260
00:20:02,000 --> 00:20:05,000
And when she got home, her father was asleep.

261
00:20:05,000 --> 00:20:07,000
And so she closed the door.

262
00:20:07,000 --> 00:20:08,000
She banged.

263
00:20:08,000 --> 00:20:10,000
She banged something.

264
00:20:10,000 --> 00:20:15,000
And he woke up and he was starred only.

265
00:20:15,000 --> 00:20:17,000
He jerked something.

266
00:20:17,000 --> 00:20:20,000
And the loom went stabbing something in the spindle.

267
00:20:20,000 --> 00:20:22,000
Maybe I don't know whatever.

268
00:20:22,000 --> 00:20:26,000
When stabbing into her chest and she died.

269
00:20:26,000 --> 00:20:32,000
And immediately it was born in one of the high heavens.

270
00:20:32,000 --> 00:20:36,000
To see diamonds, if you're interested.

271
00:20:36,000 --> 00:20:41,000
Her father was so upset.

272
00:20:41,000 --> 00:20:43,000
And he went to see the Buddha, apparently,

273
00:20:43,000 --> 00:20:47,000
and having gone to see the Buddha, he became a monk.

274
00:20:47,000 --> 00:20:52,000
Buddha taught him this teaching of, because he was so sad.

275
00:20:52,000 --> 00:20:55,000
He taught him the teaching of all the tears and all the waters

276
00:20:55,000 --> 00:20:58,000
and all the oceans are nothing compared to all the tears.

277
00:20:58,000 --> 00:21:01,000
You've cried over your daughter,

278
00:21:01,000 --> 00:21:04,000
or over the death of your daughter.

279
00:21:04,000 --> 00:21:09,000
How many lifetimes?

280
00:21:09,000 --> 00:21:12,000
When being, there's no benefit from crying.

281
00:21:12,000 --> 00:21:14,000
You don't get anything out of it.

282
00:21:14,000 --> 00:21:18,000
Better ways to spend your time, life is uncertain.

283
00:21:18,000 --> 00:21:20,000
Death is certain.

284
00:21:20,000 --> 00:21:22,000
And this became a monk.

285
00:21:22,000 --> 00:21:26,000
And he became enlightened, apparently, eventually.

286
00:21:26,000 --> 00:21:27,000
So that's the story.

287
00:21:27,000 --> 00:21:29,000
That's verse 174.

288
00:21:29,000 --> 00:21:32,000
That's the temple pad of her this week.

289
00:21:32,000 --> 00:21:47,000
Thank you all for tuning in.

