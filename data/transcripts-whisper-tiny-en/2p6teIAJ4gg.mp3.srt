1
00:00:00,000 --> 00:00:09,440
And here's the new kutti, the latest place to put kutis from outside.

2
00:00:09,440 --> 00:00:13,960
So after this is after we're finished, so now we can walk through it.

3
00:00:13,960 --> 00:00:16,840
This is what we took out of it.

4
00:00:16,840 --> 00:00:25,680
This was the way up, now we can't come up yet to get rid of the wood first.

5
00:00:25,680 --> 00:00:31,880
This is enough room for three kutis.

6
00:00:31,880 --> 00:00:43,120
And so there's another way down that I didn't know about until now, and that's this way.

7
00:00:43,120 --> 00:00:54,600
Watch the step, and look them down to this nice pool of water.

8
00:00:54,600 --> 00:01:04,000
This is where I first saw this four foot long lizard, swimming.

9
00:01:04,000 --> 00:01:13,560
Wonderful, it's a wonderful place, just another update for all of you who are interested

10
00:01:13,560 --> 00:01:29,160
here on Saturday, all the best.

