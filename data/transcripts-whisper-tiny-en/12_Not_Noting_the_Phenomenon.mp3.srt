1
00:00:00,000 --> 00:00:06,800
not noting the phenomenon question is cultivating a habit of noting that does not

2
00:00:06,800 --> 00:00:12,000
correspond to a special mindfulness of the phenomenon, a step in the wrong direction.

3
00:00:13,120 --> 00:00:18,800
Answer, yes, it would lead you in the wrong direction. The practice is to not

4
00:00:18,800 --> 00:00:24,160
the phenomenon or the experience that is the most prominent in the present moment,

5
00:00:24,160 --> 00:00:29,840
noted for as long as it lasts, like hearing, hearing, and when that experience

6
00:00:29,840 --> 00:00:36,640
ceases, go back to noting the raising and only of the abdomen. If you hear a dog back without

7
00:00:36,640 --> 00:00:43,520
seeing it, do you note, dog, dog or hearing hearing? You cannot experience dog, but you can

8
00:00:43,520 --> 00:00:50,560
experience hearing, and that is a very important distinction. Hearing is an experience, but dog

9
00:00:50,560 --> 00:00:57,200
is a concept. Concepts can be valid objects of meditation. You could actually focus on the

10
00:00:57,200 --> 00:01:04,720
sound of a dog barking and say dog, dog, visualizing a dog in your mind. That is a valid form

11
00:01:04,720 --> 00:01:11,360
of meditation, but it will not lead you to an understanding of reality, because it is not real.

12
00:01:12,000 --> 00:01:19,120
It is a concept that arises in your mind, a concept of a dog. The reality of it is a vision in the

13
00:01:19,120 --> 00:01:25,920
mind, a sound at the ear and the experience of hearing, which really does for care. The importance

14
00:01:25,920 --> 00:01:33,200
of using the wedge by hearing, hearing, hearing is that it first away the concept. In the beginning,

15
00:01:33,200 --> 00:01:39,360
it may not, as the mind may still be caught up in concepts, but eventually the mind lets go

16
00:01:39,360 --> 00:01:46,560
of the concept, because again and again, you are saying no, it is not a dog, it is just hearing,

17
00:01:46,560 --> 00:01:52,560
no, it is not a dog, it is just hearing. The mind would try to respond, but it is a dog,

18
00:01:53,120 --> 00:02:00,000
no, it is just hearing, but it is a dog, no, it is just hearing, but I like the sound,

19
00:02:00,720 --> 00:02:07,600
no, it is just sound. Eventually, the mind will become accustomed to being the actual experience,

20
00:02:07,600 --> 00:02:14,320
and it will concede that, oh yes, it is just sound, but just hearing. This is the importance

21
00:02:14,320 --> 00:02:20,560
of the correct practice. If you say dog, dog, dog, you will never get to that point where you

22
00:02:20,560 --> 00:02:27,680
experience reality for what it is. It is very important to know the difference between concepts

23
00:02:27,680 --> 00:02:34,480
and reality. People who have never practiced intensive meditation have a hard time understanding

24
00:02:34,480 --> 00:02:41,040
this difference. When thoughts about experiential reality, they become confused saying, what do you

25
00:02:41,040 --> 00:02:46,800
mean this is just a concept? What do you mean this is not reality? If you have never really spent

26
00:02:46,800 --> 00:02:53,760
time experiencing reality objectively without conceiving anything of it, it is hard to tell the

27
00:02:53,760 --> 00:03:00,720
difference between reality and conception. For a person who has spent time meditating mindfully

28
00:03:00,720 --> 00:03:06,240
on the other hand, it is hard to understand how you would not tell the difference between the two.

29
00:03:06,240 --> 00:03:13,760
This question also relates to the broad subject of why we have a wedge at all and why we use

30
00:03:13,760 --> 00:03:19,680
the noting technique. We use the noting technique because of our understanding of the wedge

31
00:03:19,680 --> 00:03:26,960
sati, which is usually translated as mindfulness. The wedge sati literally means remembering or

32
00:03:26,960 --> 00:03:34,080
recalling. Sati means remembering one's present experience for what it is without reaction or

33
00:03:34,080 --> 00:03:41,120
interpretation of any sort or mental conceiving whatsoever. The Buddha himself explains sati as

34
00:03:41,120 --> 00:03:48,160
when walking one knows I am walking, when standing one knows I am standing, when there is a painful

35
00:03:48,160 --> 00:03:54,800
feeling one knows there is a painful feeling. We use this sort of reminder to keep from forgetting

36
00:03:54,800 --> 00:04:00,400
about our experiences and getting lost in the conceptual world of our minds.

37
00:04:00,400 --> 00:04:06,960
Reminding ourselves of reality in this way allows us to dwell as the Buddha sage not clinging

38
00:04:06,960 --> 00:04:14,400
to anything, not being dependent on anything, leaving behind our mental constructs remembering

39
00:04:14,400 --> 00:04:21,920
experiences just as they are. What word one uses to remember one's experiences as they are

40
00:04:21,920 --> 00:04:28,480
isn't really all that important. As long as it has these three effects, one, the effect of bringing

41
00:04:28,480 --> 00:04:37,200
the mind closer to the object as it is to keeping it on the object and three preventing proliferation

42
00:04:37,200 --> 00:04:43,200
based on the object. If you do not use a word to remind yourself in this way, your mind will be

43
00:04:43,200 --> 00:04:50,400
hard pressed to fully fix on the object. This is why people practice mantra meditation in general,

44
00:04:50,400 --> 00:04:57,600
even tranquility based mantras like Buddha, or because the word focuses the mind on the object,

45
00:04:57,600 --> 00:05:04,320
whatever the objects might be spiritual and mundane. When you say to yourself, pain, pain,

46
00:05:04,320 --> 00:05:10,320
the mind is drawn to the pain, it gravitates towards the pain naturally. For some people,

47
00:05:10,320 --> 00:05:16,960
if they feel pain, they will say happy, happy, happy, because they want to feel happy, or they will

48
00:05:16,960 --> 00:05:24,160
say no pain, no pain, no pain. As they want to have no pain, ask yourself, what would that do,

49
00:05:24,160 --> 00:05:30,640
would that bring the mind to see the pain clearly for what it is, no, what it does is create

50
00:05:30,640 --> 00:05:37,840
a version in the mind and a tendency to recoil from pain. It is this sort of aversive attitude

51
00:05:37,840 --> 00:05:44,640
that leads some people to take medication or drugs to escape the experiences. Some people have

52
00:05:44,640 --> 00:05:50,640
the idea that meditation is going to be another drug that allows one to get only from pain.

53
00:05:50,640 --> 00:05:56,720
If you use a word that is not related at all, it is just going to create confusion.

54
00:05:57,440 --> 00:06:02,320
Adults the question is seriously considering that as a meditation practice,

55
00:06:02,960 --> 00:06:09,200
like you feel pain and say apple, apple, apple or something, you might use the functional words

56
00:06:09,200 --> 00:06:16,160
or phrase like no pain to get your mind onto something else. A common secular meditation technique,

57
00:06:16,160 --> 00:06:26,560
if you are angry, is to stay to yourself, 198, 97, 96, 95, counting down from 100.

58
00:06:26,560 --> 00:06:32,320
Eventually, you forget about the anger, so it is a valid means of getting rid of the anger,

59
00:06:32,320 --> 00:06:38,080
but it does not teach you about the anger. This is the crucial difference between mindfulness,

60
00:06:38,080 --> 00:06:44,640
meditation and many other mantra, meditation practices. In mindfulness meditation,

61
00:06:44,640 --> 00:06:52,080
we are using the mantra to bring us closer to the object, as opposed to avoiding the mundane reality.

