1
00:00:00,000 --> 00:00:28,600
Good evening.

2
00:00:28,600 --> 00:00:42,600
Last night we did a Dhamapada video.

3
00:00:42,600 --> 00:00:45,600
The intention is to do another one today.

4
00:00:45,600 --> 00:00:49,600
Last night the audio was really messed up.

5
00:00:49,600 --> 00:01:15,600
There it was terrible, terrible static.

6
00:01:15,600 --> 00:01:23,600
So trying something new today and that's to use this big mic for the audio.

7
00:01:23,600 --> 00:01:31,600
We got the camera plugged into the mixer board thing.

8
00:01:31,600 --> 00:01:36,600
I'm going to do a Dhamapada video.

9
00:01:36,600 --> 00:01:55,600
Test test test test test test test.

10
00:01:55,600 --> 00:02:09,600
I don't want it too loud when you record because you can always increase the volume later, but you don't want to hear that.

11
00:02:09,600 --> 00:02:14,600
Ready?

12
00:02:14,600 --> 00:02:15,600
Good evening.

13
00:02:15,600 --> 00:02:19,600
Welcome back to our study of the Dhamapada.

14
00:02:19,600 --> 00:02:33,600
We continue on with verse number 78 which reads as follows.

15
00:02:33,600 --> 00:02:46,600
Don't one should not associate with an evil friend.

16
00:02:46,600 --> 00:02:52,600
One should not associate with a low person.

17
00:02:52,600 --> 00:02:59,600
One should associate with a Kanyanamita, a beautiful friend.

18
00:02:59,600 --> 00:03:11,600
One should associate with a Purisutama, Uhtama is highest with the highest person.

19
00:03:11,600 --> 00:03:19,600
This verse we're told was taught in regards to chana.

20
00:03:19,600 --> 00:03:23,600
Chana is one of the more famous figures in the Buddha story.

21
00:03:23,600 --> 00:03:38,600
Every story that tells every story that you hear about how our great and glorious and wonderful and awesome leader became a Buddha.

22
00:03:38,600 --> 00:03:42,600
It includes chana.

23
00:03:42,600 --> 00:03:48,600
Have you ever seen a little Buddha with Kanyu Reeves?

24
00:03:48,600 --> 00:03:50,600
It has chana.

25
00:03:50,600 --> 00:03:51,600
Chana.

26
00:03:51,600 --> 00:03:53,600
Why are those men?

27
00:03:53,600 --> 00:03:55,600
Why is that men lying there?

28
00:03:55,600 --> 00:03:58,600
What is wrong with those people?

29
00:03:58,600 --> 00:04:01,600
They're sick, my Lord.

30
00:04:01,600 --> 00:04:04,600
Chana was there with him from childhood.

31
00:04:04,600 --> 00:04:09,600
Chana was there when he saw the four sites, the four great sites.

32
00:04:09,600 --> 00:04:17,600
He saw an old person, a sick person, a dead person, and then he saw a recluse.

33
00:04:17,600 --> 00:04:19,600
Who is that man?

34
00:04:19,600 --> 00:04:21,600
He is a recluse.

35
00:04:21,600 --> 00:04:28,600
He has gone forth from the home life in order to find freedom from suffering, freedom from death.

36
00:04:28,600 --> 00:04:31,600
That is what I must do.

37
00:04:31,600 --> 00:04:35,600
Ever watched a little Buddha?

38
00:04:35,600 --> 00:04:43,600
Or what's another story?

39
00:04:43,600 --> 00:04:51,600
They've all got this story, and then chana went with him when he left.

40
00:04:51,600 --> 00:04:56,600
But then he told chana to turn back, and chana would turn back.

41
00:04:56,600 --> 00:05:05,600
He said to chana, you must take Kanktaka, this horse, and my jewels.

42
00:05:05,600 --> 00:05:12,600
So all of his royal jewels and earrings and whatever he crown, whatever he was wearing.

43
00:05:12,600 --> 00:05:19,600
Probably another crown, but whatever jewels he was wearing, bring these back to my royal accoutrements.

44
00:05:19,600 --> 00:05:21,600
Because they have to go back to my family.

45
00:05:21,600 --> 00:05:24,600
They don't want them to get lost.

46
00:05:24,600 --> 00:05:29,600
And so chana went a little way.

47
00:05:29,600 --> 00:05:33,600
I hope I'm not mixing this up with somebody else's story.

48
00:05:33,600 --> 00:05:37,600
Chana went back, but Kanktaka died of a broken heart.

49
00:05:37,600 --> 00:05:40,600
His horse, his horse who had been born with him.

50
00:05:40,600 --> 00:05:43,600
The horse was born on the same day as the Bodhisatta.

51
00:05:43,600 --> 00:05:46,600
I think chana was as well, actually.

52
00:05:46,600 --> 00:05:50,600
There are five things as part of one of the exams we have to take.

53
00:05:50,600 --> 00:05:55,600
The first dhamma exams are like five things that were born on the same day as the Buddha.

54
00:05:55,600 --> 00:05:59,600
The Bodhisatta, the tree.

55
00:05:59,600 --> 00:06:06,600
The Bodhi tree was planted on the same day as the Bodhisatta was born.

56
00:06:06,600 --> 00:06:10,600
I think you saw Dara, his wife to be.

57
00:06:10,600 --> 00:06:17,600
Maybe Ananda, Kanktaka, the horse, and I don't know.

58
00:06:17,600 --> 00:06:20,600
Maybe Chana, I'm grasping, I don't quite remember.

59
00:06:20,600 --> 00:06:23,600
I know the horse and the Bodhi tree for sure.

60
00:06:23,600 --> 00:06:26,600
And the horse died of a broken heart.

61
00:06:26,600 --> 00:06:31,600
Because he thought that it would never see its master again.

62
00:06:31,600 --> 00:06:33,600
And it was reborn.

63
00:06:33,600 --> 00:06:36,600
Kanktaka was reborn as an angel, I believe.

64
00:06:36,600 --> 00:06:40,600
And there's some story about him that I can't remember either.

65
00:06:40,600 --> 00:06:48,600
But Chana went away and he thought, I can't bring these jewels back.

66
00:06:48,600 --> 00:06:51,600
If I go back without the Bodhi set, without the prince,

67
00:06:51,600 --> 00:06:55,600
they're going to think I did something to him.

68
00:06:55,600 --> 00:06:57,600
They're going to accuse me.

69
00:06:57,600 --> 00:07:04,600
And so he hung the jewels up on a tree and went and became an ascetic himself,

70
00:07:04,600 --> 00:07:06,600
lived in the forest.

71
00:07:06,600 --> 00:07:08,600
I think that's the story.

72
00:07:08,600 --> 00:07:10,600
It's easy to get these mixed up.

73
00:07:10,600 --> 00:07:11,600
I don't pay too much attention.

74
00:07:11,600 --> 00:07:13,600
But I think that's Chana's story.

75
00:07:13,600 --> 00:07:16,600
How he came to become a monk, I can't remember that.

76
00:07:16,600 --> 00:07:18,600
Eventually he came around to become a bikhu.

77
00:07:18,600 --> 00:07:22,600
So he ordained under the Buddha.

78
00:07:22,600 --> 00:07:27,600
And I imagine by that time, Sariput and Mogulana were already

79
00:07:27,600 --> 00:07:29,600
the two chief disciples.

80
00:07:29,600 --> 00:07:34,600
And Chana, as close again, we have one of these stories of being close

81
00:07:34,600 --> 00:07:38,600
to perfection and having no part in it.

82
00:07:38,600 --> 00:07:40,600
So he was so close to the Buddha.

83
00:07:40,600 --> 00:07:44,600
Perfect and perfectly enlightened Buddha.

84
00:07:44,600 --> 00:07:46,600
And what did he do?

85
00:07:46,600 --> 00:07:49,600
He went around mocking the two chief disciples and saying,

86
00:07:49,600 --> 00:07:52,600
I was there with him from the very beginning,

87
00:07:52,600 --> 00:07:57,600
and I never ever lost track of the Buddha.

88
00:07:57,600 --> 00:08:00,600
I was there when he left home.

89
00:08:00,600 --> 00:08:02,600
I was there when he saw the four sides.

90
00:08:02,600 --> 00:08:05,600
But these two, these two go around saying,

91
00:08:05,600 --> 00:08:07,600
I'm the chief disciple.

92
00:08:07,600 --> 00:08:10,600
It actually says that it's like he marks them.

93
00:08:10,600 --> 00:08:15,600
I'm the chief disciple.

94
00:08:15,600 --> 00:08:17,600
Can you imagine, right?

95
00:08:17,600 --> 00:08:19,600
For start to say that about Sariput and Mogulana,

96
00:08:19,600 --> 00:08:22,600
then he would say this out loud to people.

97
00:08:22,600 --> 00:08:30,600
You go around thinking there of all that.

98
00:08:30,600 --> 00:08:35,600
And word got back to the Buddha, of course.

99
00:08:35,600 --> 00:08:38,600
And the Buddha called Chana up and asked him if this was true.

100
00:08:38,600 --> 00:08:44,600
And Chana kind of got shamed and quieted down for a while.

101
00:08:44,600 --> 00:08:50,600
Stop saying such terrible things.

102
00:08:50,600 --> 00:08:52,600
But then he started up again.

103
00:08:52,600 --> 00:08:53,600
And he started up again.

104
00:08:53,600 --> 00:08:55,600
And the Buddha called them back.

105
00:08:55,600 --> 00:08:56,600
And he started up again.

106
00:08:56,600 --> 00:08:58,600
And the Buddha called them back the third time.

107
00:08:58,600 --> 00:09:00,600
The Buddha said, look, Chana.

108
00:09:00,600 --> 00:09:03,600
Sariput and Mogulana could be your best friends.

109
00:09:03,600 --> 00:09:06,600
They could help you so, so much.

110
00:09:06,600 --> 00:09:08,600
They could do such great things for you.

111
00:09:08,600 --> 00:09:10,600
If you'd be their friends, because they are the,

112
00:09:10,600 --> 00:09:13,600
they are among the highest in beings.

113
00:09:13,600 --> 00:09:17,600
They are true, Cali and Amita, true good friends.

114
00:09:17,600 --> 00:09:19,600
You should not disparage them.

115
00:09:19,600 --> 00:09:20,600
You should not think.

116
00:09:20,600 --> 00:09:24,600
Low, think little of them, be little of them, mock them.

117
00:09:24,600 --> 00:09:26,600
So it's all to your detriment.

118
00:09:26,600 --> 00:09:30,600
And he didn't listen.

119
00:09:30,600 --> 00:09:32,600
But the Buddha said, so before,

120
00:09:32,600 --> 00:09:34,600
so when he's giving him this lecture,

121
00:09:34,600 --> 00:09:35,600
this is when he said the verse,

122
00:09:35,600 --> 00:09:36,600
the Bhangi Papakini.

123
00:09:36,600 --> 00:09:41,600
They don't associate with those bad friends of yours.

124
00:09:41,600 --> 00:09:43,600
Don't associate with low people,

125
00:09:43,600 --> 00:09:46,600
associate with good friends like Sariput.

126
00:09:46,600 --> 00:09:49,600
They associate with the highest people like Mogulana

127
00:09:49,600 --> 00:09:51,600
and Sariput.

128
00:09:51,600 --> 00:09:53,600
But he didn't listen.

129
00:09:53,600 --> 00:09:57,600
And the story goes on.

130
00:09:57,600 --> 00:10:01,600
The Buddha said to Ananda, to said to the monks,

131
00:10:01,600 --> 00:10:03,600
he's not going to, during the time of my life,

132
00:10:03,600 --> 00:10:07,600
as long as I'm alive, he's never going to be,

133
00:10:07,600 --> 00:10:09,600
he will never be humbled.

134
00:10:09,600 --> 00:10:12,600
But once I pass away, he will be humbled.

135
00:10:12,600 --> 00:10:15,600
And so they at the Ananda asked him,

136
00:10:15,600 --> 00:10:20,600
this is a prelude to the Bhangi Banas,

137
00:10:20,600 --> 00:10:24,600
or a Bhangi Banas, or a Bhangi Banas, or ananda actually asks the Buddha.

138
00:10:24,600 --> 00:10:25,600
What are we supposed to do with China?

139
00:10:25,600 --> 00:10:28,600
It's like he remembered this part of the story.

140
00:10:28,600 --> 00:10:31,600
You remember that the Buddha had once said

141
00:10:31,600 --> 00:10:34,600
that after he passed away, China wouldn't be humbled.

142
00:10:34,600 --> 00:10:36,600
So he said, what do we do to humble him?

143
00:10:36,600 --> 00:10:39,600
The Buddha said, give him the highest,

144
00:10:39,600 --> 00:10:42,600
the ultimate punishment, the brahmananda.

145
00:10:42,600 --> 00:10:45,600
Then the mean stick, literally,

146
00:10:45,600 --> 00:10:47,600
but it seems to mean punishment.

147
00:10:47,600 --> 00:10:50,600
And brahmana means, of course, highest to God.

148
00:10:50,600 --> 00:10:52,600
Punishment of God, sort of.

149
00:10:52,600 --> 00:10:56,600
But it means the punishment of the highest or the highest punishment.

150
00:10:56,600 --> 00:10:58,600
The ultimate punishment.

151
00:10:58,600 --> 00:11:01,600
What do you think the ultimate punishment is?

152
00:11:01,600 --> 00:11:06,600
So they asked him, what is that ultimate punishment?

153
00:11:06,600 --> 00:11:08,600
What is the brahmananda?

154
00:11:08,600 --> 00:11:11,600
And Buddha said, let him say what he wants,

155
00:11:11,600 --> 00:11:15,600
but no one should consort with him,

156
00:11:15,600 --> 00:11:20,600
talk to him, teach him, and punish him.

157
00:11:20,600 --> 00:11:22,600
Get involved with him in any way.

158
00:11:22,600 --> 00:11:26,600
Let no one teach him, let no one help him.

159
00:11:26,600 --> 00:11:31,600
Let no one try and make him a better person.

160
00:11:31,600 --> 00:11:35,600
That right there, it's like killing him.

161
00:11:35,600 --> 00:11:42,600
It's like, there's the Buddha mentioned this kind of thing

162
00:11:42,600 --> 00:11:45,600
to Prince once. I think it was Princeambaya.

163
00:11:45,600 --> 00:11:46,600
I can't remember.

164
00:11:46,600 --> 00:11:49,600
I'm big enough, maybe.

165
00:11:49,600 --> 00:11:53,600
And he said, if I teach people,

166
00:11:53,600 --> 00:11:56,600
if I teach my disciples, if I give them a good teaching,

167
00:11:56,600 --> 00:11:58,600
tell them this is good, do this.

168
00:11:58,600 --> 00:11:59,600
And they don't listen.

169
00:11:59,600 --> 00:12:01,600
Then I give them a hard teaching.

170
00:12:01,600 --> 00:12:02,600
I say, don't do this.

171
00:12:02,600 --> 00:12:03,600
That's bad.

172
00:12:03,600 --> 00:12:08,600
And if they still don't listen, then I kill them.

173
00:12:08,600 --> 00:12:10,600
And he said, what would you mean to kill them?

174
00:12:10,600 --> 00:12:13,600
None of us, I don't teach them.

175
00:12:13,600 --> 00:12:14,600
I don't help them.

176
00:12:14,600 --> 00:12:19,600
And all of my disciples also cease helping

177
00:12:19,600 --> 00:12:21,600
and teaching them.

178
00:12:21,600 --> 00:12:23,600
The Prince said, indeed,

179
00:12:23,600 --> 00:12:25,600
that's as though killing them.

180
00:12:25,600 --> 00:12:29,600
So this is the ultimate punishment.

181
00:12:29,600 --> 00:12:31,600
And after the Buddha passed away,

182
00:12:31,600 --> 00:12:34,600
indeed Ananda went with a bunch of monks.

183
00:12:34,600 --> 00:12:36,600
He told Mahakasupa that this is what the Buddha had said

184
00:12:36,600 --> 00:12:37,600
at the first council.

185
00:12:37,600 --> 00:12:39,600
He told Mahakasupa.

186
00:12:39,600 --> 00:12:43,600
And Mahakasupa said, well, then you go and

187
00:12:43,600 --> 00:12:45,600
bestow this.

188
00:12:45,600 --> 00:12:47,600
Bestow the Brahmananda on Chandman.

189
00:12:47,600 --> 00:12:49,600
They went to Chandna.

190
00:12:49,600 --> 00:12:54,600
And Chandna was shaken by it

191
00:12:54,600 --> 00:12:59,600
and eventually humbled.

192
00:12:59,600 --> 00:13:01,600
So that's our story.

193
00:13:01,600 --> 00:13:04,600
How does this relate to our practice?

194
00:13:04,600 --> 00:13:06,600
Well, it's got this very simple teaching.

195
00:13:06,600 --> 00:13:07,600
But if we look at the story,

196
00:13:07,600 --> 00:13:10,600
we can find a couple of other points.

197
00:13:10,600 --> 00:13:13,600
First is the point of pride.

198
00:13:13,600 --> 00:13:14,600
No.

199
00:13:14,600 --> 00:13:17,600
Pride in stature.

200
00:13:17,600 --> 00:13:21,600
I've met people in Buddhist teachers, actually.

201
00:13:21,600 --> 00:13:24,600
We were very proud of their seniority.

202
00:13:24,600 --> 00:13:29,600
And I think it's very important to point out how senior they are.

203
00:13:29,600 --> 00:13:33,600
I was practicing since this time

204
00:13:33,600 --> 00:13:35,600
and this time I'm a senior student

205
00:13:35,600 --> 00:13:39,600
to this person and that person.

206
00:13:39,600 --> 00:13:41,600
Mahakas, no, it really matters.

207
00:13:41,600 --> 00:13:47,600
That's no, that says something.

208
00:13:47,600 --> 00:13:49,600
It's funny that Chandna wouldn't be like that.

209
00:13:49,600 --> 00:13:52,600
Being so close to the Buddha and still didn't understand.

210
00:13:52,600 --> 00:13:56,600
No, there's something to be said about seniority.

211
00:13:56,600 --> 00:13:59,600
But the Buddha said that's the easiest way

212
00:13:59,600 --> 00:14:03,600
to set up a system.

213
00:14:03,600 --> 00:14:05,600
So if you have a monastic system,

214
00:14:05,600 --> 00:14:08,600
seniority makes things a lot easier.

215
00:14:08,600 --> 00:14:11,600
But that only really applies to an institution, right?

216
00:14:11,600 --> 00:14:14,600
In an institution, seniority makes sense

217
00:14:14,600 --> 00:14:19,600
because how are you going to judge merit, right?

218
00:14:19,600 --> 00:14:24,600
And if merit leads to institutional rank,

219
00:14:24,600 --> 00:14:26,600
you can see where the problem lies.

220
00:14:26,600 --> 00:14:28,600
People pretending to have merit

221
00:14:28,600 --> 00:14:32,600
or people getting greedy about cultivating

222
00:14:32,600 --> 00:14:37,600
their practice simply to gain a stature

223
00:14:37,600 --> 00:14:40,600
and that kind of thing, right?

224
00:14:40,600 --> 00:14:43,600
So in that case, going by seniority makes sense.

225
00:14:43,600 --> 00:14:45,600
But to say that somehow you're better than someone

226
00:14:45,600 --> 00:14:47,600
or to hold yourself up because of seniority.

227
00:14:47,600 --> 00:14:49,600
It's with ridiculous.

228
00:14:49,600 --> 00:14:51,600
Functionally, it makes sense.

229
00:14:51,600 --> 00:14:54,600
It stops a lot of the ego.

230
00:14:54,600 --> 00:14:57,600
But it was funny.

231
00:14:57,600 --> 00:15:02,600
I talked to,

232
00:15:02,600 --> 00:15:04,600
there was a mistake with the monk once

233
00:15:04,600 --> 00:15:09,600
and he wasn't the greatest of monks.

234
00:15:09,600 --> 00:15:12,600
And we got in a feud.

235
00:15:12,600 --> 00:15:14,600
And at one point he was saying,

236
00:15:14,600 --> 00:15:18,600
oh, this monk is so much junior to me.

237
00:15:18,600 --> 00:15:22,600
I'm not sure if that was anywhere.

238
00:15:22,600 --> 00:15:23,600
There was something,

239
00:15:23,600 --> 00:15:24,600
and I went to see one of the big monks

240
00:15:24,600 --> 00:15:28,600
and I was saying, you know, this is the,

241
00:15:28,600 --> 00:15:32,600
this is what he's saying and he said,

242
00:15:32,600 --> 00:15:34,600
he said, seniority is of two.

243
00:15:34,600 --> 00:15:36,600
There's many kinds of seniority.

244
00:15:36,600 --> 00:15:40,600
One kind of seniority is seniority of ability.

245
00:15:40,600 --> 00:15:46,600
But the problem there is how do you decide who has the ability?

246
00:15:46,600 --> 00:15:51,600
Still, it makes a lot more sense.

247
00:15:51,600 --> 00:15:54,600
You have to take both into account.

248
00:15:54,600 --> 00:15:56,600
And in fact, you should never, right?

249
00:15:56,600 --> 00:15:59,600
Why would if Mogulan and Sariput over in that position,

250
00:15:59,600 --> 00:16:01,600
they would have never thought,

251
00:16:01,600 --> 00:16:06,600
who is this China guy who the Buddha holds up so highly?

252
00:16:06,600 --> 00:16:09,600
We should never hold oneself above others, right?

253
00:16:09,600 --> 00:16:13,600
So this idea of pride is really important.

254
00:16:13,600 --> 00:16:16,600
We should never think of ourselves as a advanced meditator,

255
00:16:16,600 --> 00:16:18,600
so I've been practicing for so many years.

256
00:16:18,600 --> 00:16:20,600
It's usually what we say when we meet other people.

257
00:16:20,600 --> 00:16:23,600
I've been practicing for so many years.

258
00:16:23,600 --> 00:16:25,600
It's really kind of unwholesome, you know?

259
00:16:25,600 --> 00:16:28,600
Because it's kind of boasting in the sense.

260
00:16:28,600 --> 00:16:31,600
Like you, you say it because you want people to be impressed.

261
00:16:31,600 --> 00:16:33,600
It's just really a bad thing.

262
00:16:33,600 --> 00:16:35,600
It doesn't really work very well, right?

263
00:16:35,600 --> 00:16:37,600
Because who wants to hear that?

264
00:16:37,600 --> 00:16:38,600
Oh, wow.

265
00:16:38,600 --> 00:16:41,600
You're better than me, right?

266
00:16:41,600 --> 00:16:43,600
It's a good way to create animosity.

267
00:16:43,600 --> 00:16:46,600
In fact, jealousy.

268
00:16:46,600 --> 00:16:49,600
That's a good test for us on the other hand.

269
00:16:49,600 --> 00:16:55,600
To be humble.

270
00:16:55,600 --> 00:17:02,600
At least to recognize our jealousy, to recognize our ego.

271
00:17:02,600 --> 00:17:05,600
Another thing we don't want to do is be falsely humble.

272
00:17:05,600 --> 00:17:09,600
To pretend to be humble or to say, to despise ourselves

273
00:17:09,600 --> 00:17:14,600
in order to appear humble or in order to cultivate humility.

274
00:17:14,600 --> 00:17:16,600
It's not really something you cultivate.

275
00:17:16,600 --> 00:17:17,600
It's funny, you know?

276
00:17:17,600 --> 00:17:23,600
Wholesome qualities for the most part aren't something you should cultivate.

277
00:17:23,600 --> 00:17:27,600
There's something that should come when your mind is purified.

278
00:17:27,600 --> 00:17:30,600
It's like you purify the soil and everything grows.

279
00:17:30,600 --> 00:17:33,600
All the good things grow naturally.

280
00:17:33,600 --> 00:17:36,600
So humility isn't something you can cultivate.

281
00:17:36,600 --> 00:17:39,600
Really, the only thing we should be cultivating is mindfulness.

282
00:17:39,600 --> 00:17:41,600
The Buddha put it in a special category.

283
00:17:41,600 --> 00:17:44,600
It's the only one that's always useful.

284
00:17:44,600 --> 00:17:49,600
The only one that you should always be focused on.

285
00:17:49,600 --> 00:17:52,600
Because through the practice of mindfulness, you'll see your ego.

286
00:17:52,600 --> 00:17:54,600
You'll see your jealousy.

287
00:17:54,600 --> 00:17:56,600
You'll see these qualities.

288
00:17:56,600 --> 00:17:58,600
If China had just been mindful,

289
00:17:58,600 --> 00:18:00,600
he would have seen that this wasn't helping him.

290
00:18:00,600 --> 00:18:04,600
It wasn't making him happier.

291
00:18:04,600 --> 00:18:11,600
But that's really the only way to do it.

292
00:18:11,600 --> 00:18:12,600
What else?

293
00:18:12,600 --> 00:18:14,600
Well, there's not too much here.

294
00:18:14,600 --> 00:18:22,600
But the big one is in regards to association with good people.

295
00:18:22,600 --> 00:18:24,600
All of us would kill.

296
00:18:24,600 --> 00:18:25,600
Not kill.

297
00:18:25,600 --> 00:18:29,600
That's the expression we would give a lot

298
00:18:29,600 --> 00:18:32,600
to have the opportunity to be that close to the Buddha.

299
00:18:32,600 --> 00:18:34,600
That close to the two disciples.

300
00:18:34,600 --> 00:18:37,600
Two chief disciples that alone the Buddha.

301
00:18:37,600 --> 00:18:42,600
Do you imagine having the access to a teacher like Saripuddha or Mughalama?

302
00:18:42,600 --> 00:18:46,600
And here he is going around this fire,

303
00:18:46,600 --> 00:18:48,600
barreaging them,

304
00:18:48,600 --> 00:18:51,600
saying bad things about them.

305
00:18:51,600 --> 00:18:53,600
Who do they think they are?

306
00:18:53,600 --> 00:18:55,600
These newcomers.

307
00:18:55,600 --> 00:18:57,600
The monks were like that as well.

308
00:18:57,600 --> 00:19:03,600
They wondered why Kundanya wasn't made the chief disciple.

309
00:19:03,600 --> 00:19:10,600
The Buddha said it's because of a difference of determination.

310
00:19:10,600 --> 00:19:12,600
The determination is the wish you make.

311
00:19:12,600 --> 00:19:18,600
The Atitana means what you fix on.

312
00:19:18,600 --> 00:19:20,600
What your goal is basically.

313
00:19:20,600 --> 00:19:23,600
So Kundanya's goal was to become the first one

314
00:19:23,600 --> 00:19:24,600
to realize the Buddha's teaching.

315
00:19:24,600 --> 00:19:26,600
And there's a story about him.

316
00:19:26,600 --> 00:19:29,600
I think we'll get to if we haven't already.

317
00:19:29,600 --> 00:19:34,600
And Saripuddha is Saripuddha and Mughalana made the determination.

318
00:19:34,600 --> 00:19:37,600
It was their long standing wish.

319
00:19:37,600 --> 00:19:39,600
They weren't newcomers.

320
00:19:39,600 --> 00:19:43,600
They have born and been born and died with the Buddha so many lifetimes.

321
00:19:43,600 --> 00:19:48,600
If you read the Jatakas, the number of Jatakas that included Saripuddha,

322
00:19:48,600 --> 00:19:51,600
more than Mughalana, I think, but also Mughalana.

323
00:19:51,600 --> 00:19:58,600
They were with him from long, long ago.

324
00:19:58,600 --> 00:20:02,600
Because when you associate with unpleasant people,

325
00:20:02,600 --> 00:20:05,600
how hard is it to be mindful, right?

326
00:20:05,600 --> 00:20:09,600
How easy it is to get caught up in unwholesomeness

327
00:20:09,600 --> 00:20:15,600
to acquire the qualities of these unwholesome people.

328
00:20:15,600 --> 00:20:20,600
Whereas in a way, all you have to do if you're open

329
00:20:20,600 --> 00:20:25,600
and you appreciate wise people, all you have to do is be around them

330
00:20:25,600 --> 00:20:29,600
and through your openness and your appreciation for them,

331
00:20:29,600 --> 00:20:32,600
you will only stand again.

332
00:20:32,600 --> 00:20:36,600
You will only stand to benefit, to progress,

333
00:20:36,600 --> 00:20:39,600
to better yourself, to emulate them,

334
00:20:39,600 --> 00:20:44,600
and to follow their example.

335
00:20:44,600 --> 00:20:48,600
So, simple teaching, simple story.

336
00:20:48,600 --> 00:20:51,600
That's the number of Pandavas for today.

337
00:20:51,600 --> 00:20:54,600
Thank you all for tuning in.

338
00:20:54,600 --> 00:20:58,600
And keep practicing and be well.

339
00:20:58,600 --> 00:21:03,600
Good night.

340
00:21:03,600 --> 00:21:06,600
Okay.

341
00:21:06,600 --> 00:21:11,600
So, that's the number of Pandavas.

342
00:21:11,600 --> 00:21:16,600
We are, of course, still live.

343
00:21:16,600 --> 00:21:18,600
We have some questions.

344
00:21:18,600 --> 00:21:22,600
Let's not do too many questions tonight because

345
00:21:22,600 --> 00:21:26,600
I was fighting all night last night with failed hard drive.

346
00:21:26,600 --> 00:21:30,600
And then today I was fighting with bureaucracy.

347
00:21:30,600 --> 00:21:34,600
Not fighting, but just losing energy.

348
00:21:34,600 --> 00:21:40,600
We had our first study group today at McMaster University

349
00:21:40,600 --> 00:21:43,600
in a total of one person shot up.

350
00:21:43,600 --> 00:21:46,600
And I was that one person.

351
00:21:46,600 --> 00:21:50,600
So, I don't think the study group is happening.

352
00:21:50,600 --> 00:21:53,600
Everyone's, someone brought it up and I was skeptical

353
00:21:53,600 --> 00:21:55,600
and several people were skeptical.

354
00:21:55,600 --> 00:21:58,600
So, someone called for a show of hands who would be interested

355
00:21:58,600 --> 00:22:02,600
in study group and several people put their hand up.

356
00:22:02,600 --> 00:22:07,600
But someone said, you know, we're studying so much at university.

357
00:22:07,600 --> 00:22:12,600
I don't know how much more studying we can add.

358
00:22:12,600 --> 00:22:16,600
And I think that's probably what happened here.

359
00:22:16,600 --> 00:22:21,600
As interested as people are, a lot of studying going on already.

360
00:22:21,600 --> 00:22:24,600
People are pretty busy.

361
00:22:24,600 --> 00:22:28,600
There was one man yesterday came to the peace festival with me.

362
00:22:28,600 --> 00:22:30,600
He's coming next week.

363
00:22:30,600 --> 00:22:34,600
He'll be coming for a few days.

364
00:22:34,600 --> 00:22:37,600
McMaster student during the reading week.

365
00:22:37,600 --> 00:22:45,600
So, we will be having a course for those who come.

366
00:22:45,600 --> 00:22:49,600
So, why don't we take a few questions then?

367
00:22:49,600 --> 00:22:50,600
Sure.

368
00:22:50,600 --> 00:22:53,600
Is there any difference between dealing with attachment

369
00:22:53,600 --> 00:22:57,600
for a person and attachment to a situation?

370
00:22:57,600 --> 00:23:01,600
No, they're pretty much the same.

371
00:23:01,600 --> 00:23:05,600
When I do walking meditation, I do inclining, taking off,

372
00:23:05,600 --> 00:23:08,600
lifting, going down, placing.

373
00:23:08,600 --> 00:23:10,600
Is that okay?

374
00:23:10,600 --> 00:23:11,600
It's fine.

375
00:23:11,600 --> 00:23:13,600
It's a little bit of an advanced technique.

376
00:23:13,600 --> 00:23:17,600
Finally, we run you through the steps, starting at the first step

377
00:23:17,600 --> 00:23:19,600
and going up in order.

378
00:23:19,600 --> 00:23:22,600
So, it's not how we do a course.

379
00:23:22,600 --> 00:23:30,600
But, you know, it's not wrong.

380
00:23:30,600 --> 00:23:33,600
While meditating today, I felt ambitious of power

381
00:23:33,600 --> 00:23:36,600
to be mindful was just beyond my hands.

382
00:23:36,600 --> 00:23:39,600
Do you have any advice?

383
00:23:39,600 --> 00:23:44,600
I don't understand the question.

384
00:23:44,600 --> 00:23:54,600
Ambitious of power, would that be like wanting power?

385
00:23:54,600 --> 00:23:59,600
Sounds like wanting power?

386
00:23:59,600 --> 00:24:01,600
Couldn't stay mindful?

387
00:24:01,600 --> 00:24:06,600
Oh, it's just beyond my hands in the sense I couldn't be mindful.

388
00:24:06,600 --> 00:24:09,600
Well, feeling, feeling, at the very least, I understand that.

389
00:24:09,600 --> 00:24:13,600
You can just say feeling, feeling, knowing, knowing.

390
00:24:13,600 --> 00:24:20,600
Sometimes that's all the best you can do if it's not clear exactly what the experience is.

391
00:24:20,600 --> 00:24:24,600
Because we don't have to grasp at the particulars.

392
00:24:24,600 --> 00:24:27,600
Not namitagahi na nambi njana gahi.

393
00:24:27,600 --> 00:24:33,600
Don't grasp at the details or the particulars of the experience.

394
00:24:33,600 --> 00:24:38,600
What do you think of Zochian techniques such as looking for this year?

395
00:24:38,600 --> 00:24:41,600
I don't think about dongjian techniques.

396
00:24:41,600 --> 00:24:44,600
What is the best meditation for concentration?

397
00:24:44,600 --> 00:24:46,600
Can you briefly touch on it?

398
00:24:46,600 --> 00:24:49,600
Thank you, Bhante.

399
00:24:49,600 --> 00:24:54,600
I don't teach meditations like that.

400
00:24:54,600 --> 00:24:59,600
As I am meditated, meditating and have quieted my monkey mind,

401
00:24:59,600 --> 00:25:03,600
try to look into my attachments or allow those thoughts to come naturally.

402
00:25:03,600 --> 00:25:05,600
I'd rather than to come naturally.

403
00:25:05,600 --> 00:25:09,600
Try and if you haven't read my booklet, I'd recommend reading my booklet on how to meditate.

404
00:25:09,600 --> 00:25:19,600
It gives you a way of approaching experiences.

405
00:25:19,600 --> 00:25:30,600
Thank you, Bhante.

406
00:25:30,600 --> 00:25:34,600
Let's go through much quicker with one every night.

407
00:25:34,600 --> 00:25:36,600
Has meditation helped you become a better student?

408
00:25:36,600 --> 00:25:40,600
Any differences from your earlier years in college?

409
00:25:40,600 --> 00:25:41,600
Yeah, definitely.

410
00:25:41,600 --> 00:25:44,600
You have to understand when I went to college the first time.

411
00:25:44,600 --> 00:25:50,600
Half my time was spent drinking alcohol, chasing after.

412
00:25:50,600 --> 00:25:55,600
Well, chasing things that probably weren't going to make me happy.

413
00:25:55,600 --> 00:26:04,600
And getting involved in many, many extracurriculars.

414
00:26:04,600 --> 00:26:06,600
I wasn't an evil person.

415
00:26:06,600 --> 00:26:11,600
I was actually interested in the environment and student politics and that kind of thing.

416
00:26:11,600 --> 00:26:14,600
Didn't do very well. I didn't do bad.

417
00:26:14,600 --> 00:26:17,600
I got think an A plus in calculus.

418
00:26:17,600 --> 00:26:21,600
I think I got a B in biology.

419
00:26:21,600 --> 00:26:24,600
I got A is mostly I think.

420
00:26:24,600 --> 00:26:29,600
But when I first went back to school after having practice meditation,

421
00:26:29,600 --> 00:26:34,600
I got nine A plus as I think out of the year.

422
00:26:34,600 --> 00:26:37,600
Nine A plus is an A and an A minus.

423
00:26:37,600 --> 00:26:42,600
I think something like that.

424
00:26:42,600 --> 00:26:45,600
So yeah, certainly helps your practice.

425
00:26:45,600 --> 00:26:47,600
I'm probably set to get all straight A's.

426
00:26:47,600 --> 00:26:49,600
But I'm not doing full time.

427
00:26:49,600 --> 00:26:53,600
You know, like I'm using the Latin quizzes.

428
00:26:53,600 --> 00:26:56,600
So it definitely helps your memory.

429
00:26:56,600 --> 00:26:58,600
And I'm getting older too.

430
00:26:58,600 --> 00:26:59,600
So there's that.

431
00:26:59,600 --> 00:27:06,600
It's not as easy as it was before.

432
00:27:06,600 --> 00:27:10,600
When noting how important is it to be specific?

433
00:27:10,600 --> 00:27:14,600
I know thinking to refer to all kinds of mental activities such as

434
00:27:14,600 --> 00:27:18,600
fantasizing and remembering past events, etc.

435
00:27:18,600 --> 00:27:22,600
Is specificity important?

436
00:27:22,600 --> 00:27:25,600
No.

437
00:27:25,600 --> 00:27:26,600
No, that's fine.

438
00:27:26,600 --> 00:27:31,600
I mean, my society does in certain cases talk about specificity

439
00:27:31,600 --> 00:27:36,600
specifying. So you can, based on his advice.

440
00:27:36,600 --> 00:27:40,600
But we don't generally.

441
00:27:40,600 --> 00:27:44,600
I think in his book he even talked about like if you're imagining

442
00:27:44,600 --> 00:27:48,600
visiting or arguing, say arguing, arguing.

443
00:27:48,600 --> 00:27:51,600
Yeah, it's a bit of an anomaly there.

444
00:27:51,600 --> 00:27:52,600
I'm not so sure.

445
00:27:52,600 --> 00:27:56,600
But you know, who am I to argue?

446
00:27:56,600 --> 00:28:02,600
So I'll cut up on questions.

447
00:28:02,600 --> 00:28:03,600
Great.

448
00:28:03,600 --> 00:28:04,600
Enough then.

449
00:28:04,600 --> 00:28:05,600
Good night.

450
00:28:05,600 --> 00:28:07,600
Thank you all for showing up.

451
00:28:07,600 --> 00:28:10,600
Lots of viewers on YouTube 44 viewers.

452
00:28:10,600 --> 00:28:11,600
That's great.

453
00:28:11,600 --> 00:28:13,600
Thank you all for tuning in.

454
00:28:13,600 --> 00:28:14,600
Have a good night.

455
00:28:14,600 --> 00:28:15,600
Thank you Robin for helping.

456
00:28:15,600 --> 00:28:16,600
Thank you, Bandai.

457
00:28:16,600 --> 00:28:33,600
Good night.

