1
00:00:00,000 --> 00:00:10,680
Okay, so here we are at the Mahaparini Bana Stupa, in the background there you see.

2
00:00:10,680 --> 00:00:22,720
This is the place where the Buddha passed into Arini Bana or Anupadisay Sanibana.

3
00:00:22,720 --> 00:00:40,080
He passed away. He died. People might say. So we talk a little bit about death.

4
00:00:40,080 --> 00:00:52,560
And again, like the Buddha's birth, the Buddha's death might seem like less of a consequence,

5
00:00:52,560 --> 00:01:00,560
or rather than less of a consequence, less of a reason to celebrate, then say his enlightenment.

6
00:01:00,560 --> 00:01:08,960
Indeed, when the Buddha passed away there was a lot of sadness, some of the monks who hadn't

7
00:01:08,960 --> 00:01:30,560
yet gained insight into impermanence were quite sad, crying and moaning and suffering because of the loss of the Buddha.

8
00:01:30,560 --> 00:01:46,480
When you come here, you feel a certain sense of peace. It makes me think and made me realize when I first came here that there is something very significant about the Buddha's

9
00:01:46,480 --> 00:02:00,400
Parini Bana that we don't often catch. And for me coming here for the first time was a real opener, eye opener or mind opener.

10
00:02:00,400 --> 00:02:07,360
To how important the Buddha's passing really was in a good way, in a positive way.

11
00:02:07,360 --> 00:02:15,600
In many ways you could say this is where peace exists. This is where you find the greatest peace on earth.

12
00:02:15,600 --> 00:02:20,880
This is where it happened.

13
00:02:20,880 --> 00:02:25,040
So in Buddhism there are three kinds of death and all of them are important.

14
00:02:25,040 --> 00:02:36,720
The first kind of death, some at the marana, some at the samuti, this is the death we're familiar with.

15
00:02:36,720 --> 00:02:44,640
When we use the word death we're most often talking about conceptual death.

16
00:02:44,640 --> 00:02:49,600
When we don't use the word conceptual we don't even think of it as conceptual.

17
00:02:49,600 --> 00:02:57,440
When a person dies we call that death. It's not a concept I didn't conceive of it. They really died, right?

18
00:02:57,440 --> 00:03:02,320
What's conceptual about that?

19
00:03:02,320 --> 00:03:09,520
We know in truth that it's simply an external observation.

20
00:03:09,520 --> 00:03:17,520
You see that the person died. Their experience of it is kind of glorically different.

21
00:03:17,520 --> 00:03:21,280
That the death that we think occurred didn't really occur.

22
00:03:21,280 --> 00:03:25,920
Or the death that occurred was only the death of the body.

23
00:03:25,920 --> 00:03:29,120
Which is only a concept as well.

24
00:03:29,120 --> 00:03:36,560
But death of a being is important, regardless of what you believe about

25
00:03:36,560 --> 00:03:42,000
reincarnation, rebirth or the continuation of the mind after death or

26
00:03:42,000 --> 00:03:51,280
the rearising of future minds based on the last mind in the last life.

27
00:03:51,280 --> 00:03:58,000
Regardless, death is significant.

28
00:03:58,000 --> 00:04:04,720
You think of all the changes that you've had to endure, all the tests we've had to

29
00:04:04,720 --> 00:04:10,400
pass or fail or muddle through.

30
00:04:10,400 --> 00:04:16,000
I think there can be things in life that are more challenging and trying than death.

31
00:04:16,000 --> 00:04:23,120
It can be challenged for years or even your whole life by your circumstances.

32
00:04:23,120 --> 00:04:27,920
But there's nothing so significant as death.

33
00:04:27,920 --> 00:04:31,760
There's nothing that so radically

34
00:04:31,760 --> 00:04:52,640
curtales or limits everything, ambition, growth, success, pleasure,

35
00:04:52,640 --> 00:04:55,920
possession, relationship.

36
00:04:55,920 --> 00:05:06,320
Everything we strive for in the world, everything we build,

37
00:05:06,320 --> 00:05:10,960
reconnection we make with other people with places and things.

38
00:05:10,960 --> 00:05:14,960
We lose them all and give them all up.

39
00:05:14,960 --> 00:05:21,600
And we know this because no matter how well you guard from other kinds of loss,

40
00:05:21,600 --> 00:05:30,160
loss of life is inevitable.

41
00:05:30,960 --> 00:05:41,280
It's ultimate and unavoidable.

42
00:05:41,280 --> 00:05:47,200
And so it was really ultimately death along with old age and sickness,

43
00:05:47,200 --> 00:05:53,600
right, that caused the Bodhisatta to leave home.

44
00:05:53,600 --> 00:05:57,200
Death is the catalyst.

45
00:05:58,960 --> 00:06:03,760
We saw the gate yesterday where the Bodhisatta left home.

46
00:06:03,760 --> 00:06:08,880
Why did he leave the palace where he had riches and pleasure and

47
00:06:08,880 --> 00:06:11,920
everything he could have asked for in the world, even a future where he could have

48
00:06:11,920 --> 00:06:15,200
been king of the world, they say.

49
00:06:15,200 --> 00:06:17,360
Why did he give it up?

50
00:06:17,360 --> 00:06:19,040
Death.

51
00:06:19,040 --> 00:06:20,560
So it was all subject to death.

52
00:06:20,560 --> 00:06:26,880
He said, once I strove myself being subject to death,

53
00:06:26,880 --> 00:06:31,600
I strove after things that were also subject to death.

54
00:06:31,600 --> 00:06:36,240
And then I thought, what if I try and find something that is beyond that,

55
00:06:36,240 --> 00:06:37,600
beyond death?

56
00:06:37,600 --> 00:06:39,280
Quite a profound thing to say.

57
00:06:39,280 --> 00:06:45,200
I think all of us struggle with this in our lives.

58
00:06:45,200 --> 00:06:48,480
As we're growing up, realisation that our parents are going to die,

59
00:06:48,480 --> 00:06:52,800
everyone around us is going to pass away the realisation that we are going to die.

60
00:06:54,560 --> 00:06:58,160
It limits everything, it limits our ambitions and so on.

61
00:07:01,360 --> 00:07:05,200
The Buddha pointed this out as the sort of defining factor,

62
00:07:05,200 --> 00:07:11,440
whether you could call someone developed or non-developed.

63
00:07:11,440 --> 00:07:15,920
He said, for some people, and he himself was in that category,

64
00:07:15,920 --> 00:07:21,600
all they have to do is hear about death, learn about death.

65
00:07:22,160 --> 00:07:27,200
It doesn't have to be someone they know or doesn't have to be a relative.

66
00:07:27,200 --> 00:07:32,480
Not even thinking about themselves when they're not even themselves in danger of death.

67
00:07:32,480 --> 00:07:41,360
Just the realisation that it's a fact of life, and immediately they give rise to

68
00:07:44,480 --> 00:07:51,920
the impulsion to find a way out, to do something, to prepare themselves for death.

69
00:07:53,680 --> 00:07:59,120
At the very least, if you can't figure out what it is that frees you from death,

70
00:07:59,120 --> 00:08:01,440
it will be ready for it.

71
00:08:03,200 --> 00:08:04,480
It has ready for it as you can.

72
00:08:05,120 --> 00:08:06,640
Most of us go through our lives,

73
00:08:07,680 --> 00:08:11,040
forgetting about the fact that we're going to die or not, preparing ourselves.

74
00:08:11,040 --> 00:08:12,880
Most of our time is not spent preparing.

75
00:08:14,560 --> 00:08:17,280
We might think, well, that's a pretty dreary way to go through life,

76
00:08:17,280 --> 00:08:20,400
but to always be preparing for death.

77
00:08:20,400 --> 00:08:21,440
But we're the opposite.

78
00:08:21,440 --> 00:08:29,360
We are building up potential problems for when we die.

79
00:08:29,360 --> 00:08:32,560
We don't realise that when we die, we face all of this.

80
00:08:32,560 --> 00:08:38,720
Our life flashes before our eyes, and we're confronted with the state of our mind,

81
00:08:38,720 --> 00:08:42,880
because the body breaks apart, and we've got left as our mind.

82
00:08:42,880 --> 00:08:45,360
Could you imagine being left alone with your own mind?

83
00:08:45,360 --> 00:08:47,760
And that's all you've got as your refuge.

84
00:08:47,760 --> 00:08:52,320
I think some of us, if we knew what the nature of our mind, we think, might.

85
00:08:54,480 --> 00:08:55,040
Mind here.

86
00:09:00,240 --> 00:09:03,120
How shocking to have to be faced with them.

87
00:09:03,120 --> 00:09:19,920
And so he talked about these types of people,

88
00:09:19,920 --> 00:09:24,080
types of people who, well, when they hear about death, they aren't moved.

89
00:09:24,080 --> 00:09:30,080
But when they hear about someone they know, or in some relative or friend or so on,

90
00:09:30,080 --> 00:09:33,280
then they're stirred.

91
00:09:33,280 --> 00:09:38,720
Because they realize it becomes real for them.

92
00:09:39,520 --> 00:09:43,760
Some people have to have someone, see someone die, a relative or a friend.

93
00:09:44,560 --> 00:09:46,960
Some people, it takes until they themselves are dying.

94
00:09:49,280 --> 00:09:54,000
And some people never are never stirred by those truly stirring things,

95
00:09:54,000 --> 00:09:59,920
old age, sickness, death, and never take time to prepare.

96
00:09:59,920 --> 00:10:00,960
And so they're not prepared.

97
00:10:03,440 --> 00:10:06,720
They don't even have any concept of preparation and so are

98
00:10:08,480 --> 00:10:12,480
tossed and tossed about in the oceans of samsara from life to life.

99
00:10:17,440 --> 00:10:22,400
So conceptual death, what we call conceptual death is important, but we call it conceptual death,

100
00:10:22,400 --> 00:10:26,720
because there's another more profound, more real type of death

101
00:10:26,720 --> 00:10:32,880
that is giving rise to the impression that we die, and that's momentary death.

102
00:10:36,160 --> 00:10:40,960
The reality of experience, every experience is born and dies.

103
00:10:42,080 --> 00:10:45,360
And given that experience is the basis of reality,

104
00:10:46,800 --> 00:10:48,240
we're born and die every moment.

105
00:10:48,240 --> 00:10:52,800
It's not even really a we, it's an experience.

106
00:10:54,000 --> 00:10:57,120
The sound of my voice and the hearing of the sound of my voice,

107
00:10:57,840 --> 00:10:59,600
they arise together, they cease together.

108
00:11:02,560 --> 00:11:05,520
When we die, this arising and ceasing continues on.

109
00:11:05,520 --> 00:11:11,760
The conceptual identities of body and of mind,

110
00:11:13,200 --> 00:11:15,200
they fall apart, they come and they go.

111
00:11:15,200 --> 00:11:21,600
But the experiences, the physical and mental experiences continues in various forms.

112
00:11:23,680 --> 00:11:31,680
And this of course constitutes the basis of our meditation practice.

113
00:11:31,680 --> 00:11:36,000
This is the objective field within which we practice.

114
00:11:36,000 --> 00:11:44,080
And it's the fertile soil for wisdom, wisdom grows out of this.

115
00:11:47,440 --> 00:11:53,440
Starting with Nama Rupaparichi Danyana, where we begin to see that

116
00:11:55,760 --> 00:11:59,120
we begin to see the world from an experiential point of view,

117
00:11:59,120 --> 00:12:08,800
that concepts of body and mind and me and I are only a product of physical and mental

118
00:12:08,800 --> 00:12:13,360
experiences. But in the end, there's only the physical and mental aspects of experience

119
00:12:13,360 --> 00:12:16,480
that arise and sees moment by moment.

120
00:12:23,760 --> 00:12:26,400
It continues on to seeing cause and effect.

121
00:12:26,400 --> 00:12:30,160
We see how they work together to build us up in this life,

122
00:12:30,160 --> 00:12:37,440
to create sankaras, abhisankara, karma, good and bad,

123
00:12:37,440 --> 00:12:41,440
and these habits of mind that go with us through life and lead us

124
00:12:42,800 --> 00:12:45,760
to rebirth after, to another birth after we die.

125
00:12:51,840 --> 00:12:54,560
We see the chaos of it, the impermanence, the suffering,

126
00:12:54,560 --> 00:13:02,080
the non-self. We come to see that the idea of permanence is a silly,

127
00:13:02,080 --> 00:13:08,000
is a foolish one. The idea that something could be born and not die,

128
00:13:10,720 --> 00:13:19,360
that has no basis in reality and so our striving for stability, for permanence,

129
00:13:19,360 --> 00:13:26,000
is futile, is wrong headed.

130
00:13:30,160 --> 00:13:34,000
We see suffering, we see our desire therefore to be satisfied,

131
00:13:34,960 --> 00:13:39,280
suffering just means we can't be satisfied, we can't find happiness in anything.

132
00:13:41,200 --> 00:13:44,960
Because happiness isn't a thing you find in something else, it's a state of

133
00:13:44,960 --> 00:13:52,080
it's a quality of mind. It comes from contentment, it comes from having everything

134
00:13:52,080 --> 00:13:56,080
you could ever want, which either means either you want, you get what you want all the time,

135
00:13:56,080 --> 00:13:59,840
or you stop wanting. And we know which one of those is unattainable.

136
00:14:05,760 --> 00:14:09,280
So we learn to find real happiness by letting go.

137
00:14:09,280 --> 00:14:21,760
We see non-self and our way of looking at things, me and mine, and this is an entity,

138
00:14:22,480 --> 00:14:28,320
sandal, shoe, or even a shoe, is conceptual in the mind. The body is conceptual,

139
00:14:28,320 --> 00:14:40,480
much more the self that we think of, it's just conceptual. The reality is moments of experience,

140
00:14:41,520 --> 00:14:47,920
and we see that. We see a rising and ceasing, we see cessation, cessation is,

141
00:14:49,200 --> 00:14:55,360
it comes the focus of our practice. Everything passes away, nothing is worth clinging to because

142
00:14:55,360 --> 00:15:03,920
in a moment it's gone. And this is what allows us to become disenchanted and turn away from

143
00:15:05,680 --> 00:15:16,000
samsara. This is the process of turning away. As we turn away, we seek out the deathless,

144
00:15:16,000 --> 00:15:19,200
we seek out something that is not subject to death.

145
00:15:19,200 --> 00:15:29,840
It's simply seeing again and again the nature of all of the things that we might cling to

146
00:15:31,280 --> 00:15:38,320
as subject to death. It brings us to the point of equanimity, finally,

147
00:15:39,360 --> 00:15:44,080
where we look upon everything is the same, nothing is greater or lesser than anything else,

148
00:15:44,080 --> 00:15:51,840
so no consequence. It can't make us happy, it can't satisfy us. It can't be the goal for the solution.

149
00:15:53,440 --> 00:16:00,320
It comes and it goes. And deeper and deeper, we eventually realize this, we have this

150
00:16:01,760 --> 00:16:02,480
clarity,

151
00:16:02,480 --> 00:16:12,560
yankinji samudayadamang sabantanirodadamangwudkundanya saw in the Isipatana Deer Park.

152
00:16:13,760 --> 00:16:18,480
Everything that arises is of a nature disease. Everything that is of a nature to arise is also

153
00:16:18,480 --> 00:16:19,280
of a nature disease.

154
00:16:22,720 --> 00:16:28,400
So that is the significance of momentary death is of course the most significant

155
00:16:28,400 --> 00:16:32,560
reality of our meditation practice.

156
00:16:35,600 --> 00:16:39,680
But in some ways you can say even more significant than those two is the third type of death.

157
00:16:43,200 --> 00:16:47,200
None of it would be complete without the third type, which is called Samu Chaitamana,

158
00:16:47,200 --> 00:16:53,200
death of literally death by cutting off, but it means the cutting off of defilements,

159
00:16:53,200 --> 00:17:01,200
the death of the death of defilement, the death of evil, the death of that which causes suffering,

160
00:17:03,120 --> 00:17:05,600
the death of clinging, craving, ignorance.

161
00:17:08,400 --> 00:17:14,880
This is the death which we seek in Buddhism and in fact it's the most uplifting of all

162
00:17:14,880 --> 00:17:21,040
teachings really. People who say Buddhism is pessimistic and someone came to the Buddha and said,

163
00:17:21,040 --> 00:17:26,800
do you teach nihilism? Yes, yes, I teach there's a way you could say that and what is the way?

164
00:17:27,520 --> 00:17:30,160
I teach the annihilation of evil.

165
00:17:32,240 --> 00:17:36,880
So when people talk about us as being negative and all about

166
00:17:38,720 --> 00:17:43,280
death and so on, yes, the death of evil, this is our focus.

167
00:17:44,160 --> 00:17:49,200
And that's what we have here at the Mahaparina Vanasdupa. That's what's so profound and important

168
00:17:49,200 --> 00:18:02,720
about the Parinibhana, Parinibhana. This is the cessation of defilement, the cessation of suffering.

169
00:18:06,240 --> 00:18:10,080
So someone who still has defilements,

170
00:18:13,200 --> 00:18:16,560
when they die of course they're reborn and reborn again and again.

171
00:18:16,560 --> 00:18:24,000
The significance of what the Buddha attained in Buddha Gaya and under the Bodhi tree

172
00:18:26,160 --> 00:18:35,760
is that he freed himself from rebirth. He freed himself from death and freed himself from the causes of

173
00:18:35,760 --> 00:18:47,600
suffering. He didn't seek out heaven, he didn't seek out riches or wealth or power. They sought out

174
00:18:47,600 --> 00:18:57,520
purity and sought out the state of mind that was free from, that had destroyed, cut off, killed,

175
00:18:58,960 --> 00:19:03,920
brought about the death of all states that one might ever call evil.

176
00:19:03,920 --> 00:19:11,360
That's the goal, that's the path of Buddhism. That's what it's all about.

177
00:19:11,360 --> 00:19:20,480
Samuji, the Mahaparina. A person who sees this fact, this reality that everything that arises

178
00:19:20,480 --> 00:19:30,240
sees is, this person cuts off their defilements. That's the whole point and purpose of the practice

179
00:19:30,240 --> 00:19:40,160
and of the realization. Our clinging or craving comes from thinking, holding on to some

180
00:19:42,160 --> 00:19:49,520
hope or belief or idea that we can find something to satisfy us, that something's going to make us

181
00:19:49,520 --> 00:19:57,040
happy. And the cessation that comes from meditation, this experience of letting go,

182
00:19:57,040 --> 00:20:08,640
the profound observation or experience of cessation tells you that

183
00:20:11,280 --> 00:20:20,160
there isn't anything. There is no underlying continuous, permanent thing that doesn't die,

184
00:20:20,160 --> 00:20:33,440
that can make us happy, that can satisfy us. There's a purification that comes,

185
00:20:33,440 --> 00:20:41,280
no reason to get angry, no reason to be craving to cling, no benefit that comes from these things.

186
00:20:41,280 --> 00:20:52,320
There's a clarity of mind that comes from this, the mind of the Buddha, the mind of the enlightened

187
00:20:52,320 --> 00:21:01,360
one that sees things as arising and ceasing as they are. Really, as we all think of ourselves,

188
00:21:01,360 --> 00:21:08,080
we think of ourselves as living our lives, here we are in this peaceful place, why aren't we happy?

189
00:21:08,080 --> 00:21:15,200
Why aren't we always happy? Why aren't we always at peace? And so we think it's because we

190
00:21:15,200 --> 00:21:20,240
don't have what we want, because we have what we don't want. We come to realize it's not because of

191
00:21:20,240 --> 00:21:29,120
any of that. It's because of the wanting and the not wanting. And we cut all of those off, we cut

192
00:21:29,120 --> 00:21:38,480
off, not what, not our circumstances, this is making me unhappy, this is I'm lacking this, I'm lacking

193
00:21:38,480 --> 00:21:52,240
it, but our perception of things, our reactions to them, how we feel about them.

194
00:21:52,240 --> 00:21:57,440
When we see that everything arises and sees us and we,

195
00:22:02,640 --> 00:22:09,200
the things that inside of us that cause us to suffer die off, this is Samuji, the

196
00:22:09,200 --> 00:22:13,920
moment. And that's what the Buddha did, that was what he found under the Bodhi tree.

197
00:22:13,920 --> 00:22:25,920
And all of the 45 years after that was simply the remainder of the results of the karma that

198
00:22:25,920 --> 00:22:33,600
he had performed in the past in his past. It was like the victory lap, where he spent his time teaching

199
00:22:33,600 --> 00:22:42,000
tirelessly and without any selfish interest.

200
00:22:45,840 --> 00:22:51,760
And then 45 years, what we have here is the fruition. Here we have the proof, we have the

201
00:22:52,880 --> 00:23:01,040
ultimate victory, the ultimate freedom, where the Buddha showed to all of us,

202
00:23:01,040 --> 00:23:14,960
demonstrated to all of us, gave us this final teaching, if you will, this final

203
00:23:17,360 --> 00:23:28,400
concept or idea of the cessation of suffering. So what we have here is the final death,

204
00:23:28,400 --> 00:23:37,520
where the Buddha found, came to an end of Samsara. He said, Anika Jati, Samsara.

205
00:23:39,520 --> 00:23:43,040
Without seeing the four noble truths we've traveled from life to life,

206
00:23:43,680 --> 00:23:50,000
the Buddha was so many, it was everything. He was an animal, he was a human, he was a god, he was

207
00:23:50,000 --> 00:24:07,040
a brother. He went through it all, looking, seeking for the freedom from death, the deathless.

208
00:24:09,200 --> 00:24:17,200
This is finally where he came to it in Kursinara. So this place is the

209
00:24:17,200 --> 00:24:23,840
ultimate ending of the Buddha's path, of the Buddha's quest, where he found

210
00:24:25,440 --> 00:24:28,560
victory and where he found peace. We come here and

211
00:24:30,480 --> 00:24:37,200
of all the places, this is the place most associated with peace. We think of peace, we can think

212
00:24:37,200 --> 00:24:46,720
of this as a peace that most of us couldn't fathom. It's so difficult for us to think about, think of

213
00:24:49,760 --> 00:24:57,440
to conceive of. So we should remember it as purity, something we can understand is the

214
00:24:59,840 --> 00:25:05,760
qualities of mind that keep us causing suffering for ourselves in others and that this is where

215
00:25:05,760 --> 00:25:15,120
the Buddha finally freed himself from those, or found the peace that comes from freeing yourself

216
00:25:15,120 --> 00:25:23,760
from all of those things that he had found 45 years earlier in Bodhgaya. So it has great importance.

217
00:25:23,760 --> 00:25:29,360
His enlightenment wouldn't meet anything without this place, without the, without what happened here.

218
00:25:29,360 --> 00:25:38,560
And so I think we find this place as a place of peace. There you go, that's a teaching on death.

219
00:25:38,560 --> 00:26:08,400
Thank you for listening.

