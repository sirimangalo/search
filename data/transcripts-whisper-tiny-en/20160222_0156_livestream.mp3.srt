1
00:00:00,000 --> 00:00:26,000
Good evening, broadcasting live.

2
00:00:26,000 --> 00:00:52,000
So today's quote starts with the word eta.

3
00:00:52,000 --> 00:00:57,000
It has a very powerful word.

4
00:00:57,000 --> 00:01:02,000
It's very iconically Buddhist.

5
00:01:02,000 --> 00:01:06,000
It's a really good dhamma-bada verse.

6
00:01:06,000 --> 00:01:09,000
It starts with eta.

7
00:01:09,000 --> 00:01:15,000
eta, pass eta, pass tiamang no kang.

8
00:01:15,000 --> 00:01:18,000
Come look at this world.

9
00:01:18,000 --> 00:01:33,000
And then, so I can find that verse.

10
00:01:33,000 --> 00:01:56,000
It has a very powerful word.

11
00:01:56,000 --> 00:02:19,000
It's the imperative form of the verb.

12
00:02:19,000 --> 00:02:22,000
The root is just letter I.

13
00:02:22,000 --> 00:02:26,000
It becomes 80.

14
00:02:26,000 --> 00:02:31,000
80 is he goes, he comes, goes, or comes.

15
00:02:31,000 --> 00:02:36,000
I think it can be neither.

16
00:02:36,000 --> 00:02:45,000
eta, come, pass eta, see, or look.

17
00:02:45,000 --> 00:02:53,000
It's a very core part of Buddhism, you know, come and see.

18
00:02:53,000 --> 00:02:59,000
The dhamma of the Buddha is called ahi-passikum.

19
00:02:59,000 --> 00:03:10,000
Ahi-passa, ahi-passa tah, pass eti, and an ika.

20
00:03:10,000 --> 00:03:18,000
Passika, something that can be seen, ahi-passika, something that can be seen, something that

21
00:03:18,000 --> 00:03:20,000
you can come and see.

22
00:03:20,000 --> 00:03:25,000
The very odd word, because ahi is actually imperative.

23
00:03:25,000 --> 00:03:31,000
Ahi-passika, something that you can say about it, come and see, something that invites

24
00:03:31,000 --> 00:03:36,000
you to come and see.

25
00:03:36,000 --> 00:03:43,000
Something about what you can say, this is come and seeable.

26
00:03:43,000 --> 00:03:49,000
It's interesting that that becomes a word, because this is something that the Buddha says

27
00:03:49,000 --> 00:03:50,000
often.

28
00:03:50,000 --> 00:03:56,000
Ahi-passa tah, and so in the dhamma of the verses, Ahi-passa thi-passa thi-mong-low kang-kang,

29
00:03:56,000 --> 00:03:58,000
look at this world.

30
00:03:58,000 --> 00:04:12,000
Ahi-tang-raj, ahi-pamong, that is decked out in the mind like a king's chariot.

31
00:04:12,000 --> 00:04:21,000
It appears to the mind to be bejeweled, to be golden, to be royal, like a royal chariot,

32
00:04:21,000 --> 00:04:24,000
all decked out.

33
00:04:24,000 --> 00:04:38,000
Yatambala, we see danti, this world which the full sinks into, we see that it becomes

34
00:04:38,000 --> 00:04:41,000
mired in.

35
00:04:41,000 --> 00:04:52,000
Nati-sango-vijanatam, but in regards to which there is no

36
00:04:52,000 --> 00:05:06,000
association by the wise, Wijanata, Wijanata.

37
00:05:06,000 --> 00:05:18,000
People who see clearly, who understand to have Wijanata or Wijanata, Wijanata is an interesting word

38
00:05:18,000 --> 00:05:23,000
to know clearly.

39
00:05:23,000 --> 00:05:27,000
Wijanata means to know clearly.

40
00:05:27,000 --> 00:05:30,000
Wijanata means wisdom.

41
00:05:30,000 --> 00:05:39,000
So fools, Bala, become immeshed and mired in this world because it's shiny.

42
00:05:39,000 --> 00:05:49,000
But there is no association with the world for those who see clearly, no clearly.

43
00:05:49,000 --> 00:05:56,000
That's not our verse today, that's not the passage that we're looking at today, but it's interesting,

44
00:05:56,000 --> 00:06:00,000
sort of apropos.

45
00:06:00,000 --> 00:06:04,000
So let's switch and look at our verse here.

46
00:06:04,000 --> 00:06:14,000
Our verse today is from Niyanguta or Nikaya.

47
00:06:14,000 --> 00:06:32,000
All right, it's from the Nikaya.

48
00:06:32,000 --> 00:06:44,000
The set of verses, a set of passages, five passages.

49
00:06:44,000 --> 00:06:51,000
So the quote that we have on our page doesn't adequately represent what's actually

50
00:06:51,000 --> 00:06:53,000
being, which is actually going on.

51
00:06:53,000 --> 00:06:59,000
This isn't actually the Buddha saying to the monks come.

52
00:06:59,000 --> 00:07:06,000
What he says is, is Yeite Anandabicuna nava ejirapupajita,

53
00:07:06,000 --> 00:07:11,000
Adunagata imangtamavindayan.

54
00:07:11,000 --> 00:07:16,000
Whatever monk is newly gone forth.

55
00:07:16,000 --> 00:07:19,000
Adunagata, what does that mean?

56
00:07:19,000 --> 00:07:22,000
Adunagata.

57
00:07:22,000 --> 00:07:27,000
Adunagata have recently come.

58
00:07:27,000 --> 00:07:32,000
Imangtamavindayan to this tabavindayan.

59
00:07:32,000 --> 00:07:37,000
Devo Anandada, they buy you Anandada.

60
00:07:37,000 --> 00:07:43,000
Devo Anandabicu, those bikhus Anandabayu,

61
00:07:43,000 --> 00:07:51,000
samadapetabha should be enjoined, should be incited,

62
00:07:51,000 --> 00:07:56,000
should be rallied, should be exhorted.

63
00:07:56,000 --> 00:08:01,000
But, just so dummies in regards to five dummies.

64
00:08:01,000 --> 00:08:04,000
So the neat thing about this quote is, there's actually five things.

65
00:08:04,000 --> 00:08:08,000
It's part of a bigger set.

66
00:08:08,000 --> 00:08:12,000
You can just take that quote out of context.

67
00:08:12,000 --> 00:08:14,000
It's a good one.

68
00:08:14,000 --> 00:08:18,000
It's an important one, but there's five aspects to this.

69
00:08:18,000 --> 00:08:23,000
That's why you can't really easily quote the Buddha's because you have to understand the context

70
00:08:23,000 --> 00:08:28,000
and understand it's much deeper than just quoting something.

71
00:08:28,000 --> 00:08:32,000
In this case, there's five parts.

72
00:08:32,000 --> 00:08:34,000
It says, five things new.

73
00:08:34,000 --> 00:08:36,000
You should tell people who come new.

74
00:08:36,000 --> 00:08:41,000
So, as with much of the Buddha's teachings, it's talking about the monks,

75
00:08:41,000 --> 00:08:45,000
but it fits just as well with lay people,

76
00:08:45,000 --> 00:08:50,000
people who have come to meditate for the first time.

77
00:08:50,000 --> 00:08:58,000
They should also be exhorted with these sorts of things.

78
00:08:58,000 --> 00:09:04,000
In which five...

79
00:09:04,000 --> 00:09:13,000
Oh, so samadapetabha, they should be enjoined or they should be caused to take up.

80
00:09:13,000 --> 00:09:17,000
Samadapetabha, you should encourage them to take up.

81
00:09:17,000 --> 00:09:25,000
Niywe say tabayya should establish them and encourage them to become settled in these things.

82
00:09:25,000 --> 00:09:30,000
But, it's happy that you should cause them to be established in these things.

83
00:09:30,000 --> 00:09:37,000
Settled, established, and to take up these things.

84
00:09:37,000 --> 00:09:39,000
Which five?

85
00:09:39,000 --> 00:09:42,000
And then he gives five quotes.

86
00:09:42,000 --> 00:09:49,000
Niywe say tabayya also, come new friend.

87
00:09:49,000 --> 00:09:57,000
Silawahu tah, be moral, be ethical.

88
00:09:57,000 --> 00:10:02,000
Butimokasamwara samutta vyarata, indwell,

89
00:10:02,000 --> 00:10:06,000
guarding the rules, guarding ethical precepts.

90
00:10:06,000 --> 00:10:09,000
Amongst of, we got lots of rules, the patimokas.

91
00:10:09,000 --> 00:10:12,000
That's what it's referring to, but you can adapt it.

92
00:10:12,000 --> 00:10:15,000
It's just be ethical.

93
00:10:15,000 --> 00:10:18,000
Ah, jara gojara sampana.

94
00:10:18,000 --> 00:10:21,000
Know your pasture.

95
00:10:21,000 --> 00:10:25,000
Know what is your pasture and what is not.

96
00:10:25,000 --> 00:10:28,000
Ah, jara gojara.

97
00:10:28,000 --> 00:10:35,000
Ah, jara is maybe mindful of ah, jara means conduct.

98
00:10:35,000 --> 00:10:38,000
Gojara means pasture.

99
00:10:38,000 --> 00:10:39,000
Go is a cow.

100
00:10:39,000 --> 00:10:42,000
So gojara is a place that a cow goes.

101
00:10:42,000 --> 00:10:44,000
It's literally means pasture.

102
00:10:44,000 --> 00:10:51,000
But it's adapted to mean your limits or your boundaries.

103
00:10:51,000 --> 00:10:55,000
So know whatever it's proper to go or it's not proper to go.

104
00:10:55,000 --> 00:10:56,000
What did it's proper to do?

105
00:10:56,000 --> 00:10:59,000
The sorts of people that's proper to hang out with.

106
00:10:59,000 --> 00:11:05,000
Proper to seek advice from that kind of thing.

107
00:11:05,000 --> 00:11:09,000
Ah, numateesu, what jesu be a dasaavino.

108
00:11:09,000 --> 00:11:16,000
Well, as one who sees the danger by a dasaavino, dasaavino,

109
00:11:16,000 --> 00:11:18,000
who sees the danger.

110
00:11:18,000 --> 00:11:23,000
Ah, numateesu, jesu, in the smallest faults.

111
00:11:23,000 --> 00:11:25,000
The smallest faults.

112
00:11:25,000 --> 00:11:31,000
See the danger in the smallest of Rome.

113
00:11:31,000 --> 00:11:37,000
Samadayasikatasikapadesu.

114
00:11:37,000 --> 00:11:44,000
Train yourselves in the precepts.

115
00:11:44,000 --> 00:11:47,000
Having accepted them.

116
00:11:47,000 --> 00:11:51,000
So the first thing he says is be ethical.

117
00:11:51,000 --> 00:11:58,000
Be aware of the proper conduct, behavior, speech.

118
00:11:58,000 --> 00:12:01,000
Of a monk, I mean, he's talking about monks.

119
00:12:01,000 --> 00:12:05,000
How is it proper behavior for a monk?

120
00:12:05,000 --> 00:12:07,000
Understand that.

121
00:12:07,000 --> 00:12:09,000
He goes for meditators as well.

122
00:12:09,000 --> 00:12:12,000
When you come to do a meditation course,

123
00:12:12,000 --> 00:12:16,000
you have to know it is proper etiquette in a monastery in a meditation center.

124
00:12:16,000 --> 00:12:21,000
And just in general, as we're at a much higher standard as Buddhists.

125
00:12:21,000 --> 00:12:29,000
Buddhism isn't a practice for mediocrity.

126
00:12:29,000 --> 00:12:37,000
Buddhists are and should be held to a higher standard,

127
00:12:37,000 --> 00:12:41,000
just because of calling themselves Buddhists.

128
00:12:41,000 --> 00:12:45,000
Once you call yourself Buddhist or you follow the Buddhist way,

129
00:12:45,000 --> 00:12:49,000
you have to be held to a higher standard because that's all it is.

130
00:12:49,000 --> 00:12:51,000
That's what being a Buddhist means.

131
00:12:51,000 --> 00:12:55,000
It's not about faith or belief or names or anything.

132
00:12:55,000 --> 00:13:04,000
You're a Buddhist if you cultivate higher things.

133
00:13:04,000 --> 00:13:05,000
That's the first one.

134
00:13:05,000 --> 00:13:07,000
The second one is the one we have in the court.

135
00:13:07,000 --> 00:13:17,940
We have a man who has an ear, who is one whom who sees

136
00:13:17,940 --> 00:13:24,020
a meditator infinity, and is a singer who sees one, because he sees one on one.

137
00:13:24,020 --> 00:13:32,020
Who sees one on the court, because he sees one on the court.

138
00:13:32,020 --> 00:13:34,940
You slide all the senses quickly.

139
00:13:34,940 --> 00:13:41,940
This is how mindfulness works, the Buddha talks often about this, it works as a guard.

140
00:13:41,940 --> 00:13:47,940
Like if you have a gate to a city, you only let in people who are trustworthy.

141
00:13:47,940 --> 00:13:52,940
The same way, it's like a filter, so in the same way mindfulness stands at the door of the senses,

142
00:13:52,940 --> 00:13:56,940
and only that's in the seeing, the hearing, the smelling.

143
00:13:56,940 --> 00:13:59,940
It doesn't let in the defilements.

144
00:13:59,940 --> 00:14:02,940
It doesn't let the defilements rise.

145
00:14:02,940 --> 00:14:06,940
It doesn't let in any of the baggage.

146
00:14:06,940 --> 00:14:12,940
All of it that's come to come to the consciousness is the experience.

147
00:14:12,940 --> 00:14:17,940
So it creates objectivity, that's the idea.

148
00:14:17,940 --> 00:14:23,940
Arakasatino with guard, the guard of mindfulness.

149
00:14:23,940 --> 00:14:25,940
Nipakasatino.

150
00:14:25,940 --> 00:14:28,940
Nipakas and interesting word, I don't really know it.

151
00:14:28,940 --> 00:14:36,940
Nipaka means infused, boiled, I don't know.

152
00:14:36,940 --> 00:14:40,940
I'm not quite sure about that. Oh, there's a variant.

153
00:14:40,940 --> 00:14:43,940
Nipaka.

154
00:14:43,940 --> 00:14:48,940
Let's look at Nipaka.

155
00:14:48,940 --> 00:14:51,940
We all probably lesson tonight.

156
00:14:51,940 --> 00:14:53,940
Prudence.

157
00:14:53,940 --> 00:14:59,940
Careful mindfulness. Nipaka is a strange one.

158
00:14:59,940 --> 00:15:05,940
Nipaka means careful mindfulness.

159
00:15:05,940 --> 00:15:07,940
Careful mindfulness.

160
00:15:07,940 --> 00:15:15,940
The mindfulness and caution.

161
00:15:15,940 --> 00:15:24,940
So this idea that mindfulness works or functions through caution.

162
00:15:24,940 --> 00:15:27,940
Your caution is mindfulness, mindfulness is caution.

163
00:15:27,940 --> 00:15:33,940
It's approaching things carefully, meticulously.

164
00:15:33,940 --> 00:15:47,940
And mindful.

165
00:15:47,940 --> 00:15:49,940
And then he launches into this,

166
00:15:49,940 --> 00:15:51,940
you know, that's really part of the quote,

167
00:15:51,940 --> 00:16:05,940
the quote, rinsara, kita manasas, sattara, cana jita sasamana gita.

168
00:16:05,940 --> 00:16:16,940
Be one who has, who is accomplished or has attained a mind guarded by mindfulness.

169
00:16:16,940 --> 00:16:22,940
With a guarded mind, become one who has a mind guarded with mindfulness.

170
00:16:22,940 --> 00:16:25,940
It's a bit awkward in English. It's just the way they speak in probably.

171
00:16:25,940 --> 00:16:28,940
And it's a way the Buddha would speak.

172
00:16:28,940 --> 00:16:33,940
It's a rhetorical tool of repeating yourself in different ways.

173
00:16:33,940 --> 00:16:36,940
So sara kita manasas.

174
00:16:36,940 --> 00:16:39,940
It means be one if I'm reading this correctly.

175
00:16:39,940 --> 00:16:45,780
Be one who has a mind that is well-guarded.

176
00:16:45,780 --> 00:16:48,420
Satara Keena Jaitasa Samanagata,

177
00:16:48,420 --> 00:17:00,580
Be one who is who goes with or who has a mind guarded by mindfulness.

178
00:17:00,580 --> 00:17:04,420
I think Satara Keena Satyara Keena Jaitasa.

179
00:17:04,420 --> 00:17:12,540
So, they should be established in guarding the senses.

180
00:17:12,540 --> 00:17:15,820
So, the first one is established in guarding the Patimoka,

181
00:17:15,820 --> 00:17:18,780
the second one is guarding the faculties.

182
00:17:18,780 --> 00:17:26,620
Then the third one, eta tumhyayasu apabasa hota.

183
00:17:26,620 --> 00:17:32,660
Be one who talks a little apabasa, one who talks only a little.

184
00:17:32,660 --> 00:17:42,700
Bhasay paryanta karyno, one who makes an end to speech,

185
00:17:42,700 --> 00:17:46,900
or knows the end speech maybe.

186
00:17:46,900 --> 00:17:51,900
Baryanta karyno, restricted, who has limited speech.

187
00:17:51,900 --> 00:17:54,940
There you go, the Buddha actually does say bhasay,

188
00:17:54,940 --> 00:18:00,420
baryanta karyno, one who limits one speech.

189
00:18:00,420 --> 00:18:06,740
puts a boundary on one speech, and that's what it means.

190
00:18:06,740 --> 00:18:11,540
So, bhasay paryanta, one who limits one speech,

191
00:18:11,540 --> 00:18:16,340
should be limited to the dhamma to good things.

192
00:18:16,340 --> 00:18:21,500
eta tumhyaya, number four, eta tumhyayasu aran yicahud,

193
00:18:21,500 --> 00:18:23,340
be one who dwells in the forest.

194
00:18:23,340 --> 00:18:26,500
Aran ya, one apatani.

195
00:18:26,500 --> 00:18:31,020
Pandani say nasana, nipati say wata.

196
00:18:31,020 --> 00:18:39,140
But they say wata, but they say wata means use or indulgence in.

197
00:18:39,140 --> 00:18:48,020
Makes you serve or makes you serve dwellings that are secluded in the forest.

198
00:18:48,020 --> 00:18:54,140
Aran yicah, be one who dwells in the forest.

199
00:18:54,140 --> 00:19:01,260
In general, kaya wo bhasay,

200
00:19:01,260 --> 00:19:10,780
bhasay, seclusion wo bhasa, be one.

201
00:19:10,780 --> 00:19:14,140
So, you should exhort them in bodily seclusion,

202
00:19:14,140 --> 00:19:17,740
seclusion of the body, because there's two kinds of seclusion.

203
00:19:17,740 --> 00:19:21,500
There's seclusion of the mind, and there's seclusion of the body.

204
00:19:21,500 --> 00:19:25,660
Seclusion of the mind is most important. That's through meditation practice.

205
00:19:25,660 --> 00:19:28,380
That seclusion of the body is important for the practice.

206
00:19:28,380 --> 00:19:33,340
That's why people come here and we just put them in their room and leave them for

207
00:19:33,340 --> 00:19:35,340
all day on their own.

208
00:19:35,340 --> 00:19:37,340
Because the idea is to be secluded.

209
00:19:37,340 --> 00:19:41,020
It's important from a physical perspective as well,

210
00:19:41,020 --> 00:19:43,820
just to include yourself.

211
00:19:43,820 --> 00:19:47,820
So, it's like in a lab, not so that you can run away from your problems,

212
00:19:47,820 --> 00:19:53,180
but so that you can get into lab mode and learn about yourself.

213
00:19:53,180 --> 00:19:56,940
Study yourself.

214
00:19:56,940 --> 00:19:59,340
Number four, number five.

215
00:19:59,340 --> 00:20:03,820
Eta tumayau so samadhika.

216
00:20:03,820 --> 00:20:06,700
Who would have? Be one who has right views.

217
00:20:06,700 --> 00:20:11,580
Samadha sannyena, one who sees rightly.

218
00:20:11,580 --> 00:20:19,820
Samadha gata. One who goes, who's accomplished in right vision and right

219
00:20:19,820 --> 00:20:24,300
view.

220
00:20:31,260 --> 00:20:34,860
In right view I've talked about, I actually talked about it last Saturday.

221
00:20:34,860 --> 00:20:39,180
Right view, there are five types of right view actually. I didn't mention this last

222
00:20:39,180 --> 00:20:41,580
time, we talked about it a bit differently.

223
00:20:41,580 --> 00:20:45,180
There's the right view of karma, the responsibility of karma.

224
00:20:45,180 --> 00:20:52,380
There's right view of Jana, right view in regards to Jana,

225
00:20:52,380 --> 00:20:56,380
regards to tranquility meditation.

226
00:20:56,380 --> 00:20:59,260
We passed in as samadhita, right view of insight,

227
00:20:59,260 --> 00:21:02,300
right view about the path, right view about the fruition.

228
00:21:02,300 --> 00:21:07,260
It's not so keen on that distinction of right view, but

229
00:21:07,260 --> 00:21:12,860
right view means to see clearly, it's really the core of teravada Buddhism,

230
00:21:12,860 --> 00:21:18,140
wisdom, to cultivate understanding,

231
00:21:18,140 --> 00:21:20,940
understand things as they are.

232
00:21:20,940 --> 00:21:25,020
Anyway, so that's the dhamma for tonight.

233
00:21:25,020 --> 00:21:28,860
The quote was specifically about one of the five,

234
00:21:28,860 --> 00:21:33,420
but altogether it puts together sort of a nice

235
00:21:33,420 --> 00:21:38,220
guide for training new monks, new meditators.

236
00:21:38,220 --> 00:21:42,540
They should be mindful, but they should also be ethical first,

237
00:21:42,540 --> 00:21:48,620
and then mindful, and they shouldn't,

238
00:21:48,620 --> 00:21:53,500
or something like three. They should speak only a little,

239
00:21:53,500 --> 00:21:57,580
they should be secluded physically,

240
00:21:57,580 --> 00:22:00,540
and they should be instructed in right view,

241
00:22:00,540 --> 00:22:05,980
or they should be encouraged to cultivate right view.

242
00:22:05,980 --> 00:22:09,020
So focused on that, really, that is the focus of our meditation.

243
00:22:09,020 --> 00:22:13,340
The meditation isn't just to calm down, it isn't just to relax.

244
00:22:13,340 --> 00:22:19,900
Focused on trying to see clearly. It's an important aim for us to focus

245
00:22:19,900 --> 00:22:23,020
on in our meditation.

246
00:22:23,020 --> 00:22:30,780
Okay, so that's the dhamma for tonight.

247
00:22:30,780 --> 00:22:37,340
I'll post the hangout. Come on if you want to ask a question.

248
00:22:37,340 --> 00:22:45,740
If you just want to say hi, you can come on, nice and pose.

249
00:22:45,740 --> 00:22:53,500
I'm going to pile on, I don't mind.

250
00:22:57,500 --> 00:23:01,180
But if you have a question, please come on, you're welcome to join.

251
00:23:01,180 --> 00:23:08,380
We get consistently 25-ish viewers on YouTube.

252
00:23:08,380 --> 00:23:14,220
This is good. It means we've got a small community here.

253
00:23:14,220 --> 00:23:17,420
Hello, Bunty. Nice, I'm in.

254
00:23:17,420 --> 00:23:20,620
Oh, wait a minute.

255
00:23:20,620 --> 00:23:30,140
My sound is not good. Let me just...

256
00:23:30,140 --> 00:23:46,620
There are no questions from this wish y'all a good night.

257
00:23:55,340 --> 00:23:56,940
Hello, Bunty.

258
00:23:56,940 --> 00:23:59,580
Hi, do you have a question?

259
00:23:59,580 --> 00:24:04,540
Nope, I just want to say hello.

260
00:24:04,540 --> 00:24:06,140
Thank you for the dhamma talk today.

261
00:24:06,140 --> 00:24:07,580
That was a wonderful quote.

262
00:24:07,580 --> 00:24:12,140
Yeah, which one did you think was wonderful?

263
00:24:12,140 --> 00:24:15,740
The quote or the whole passage?

264
00:24:15,740 --> 00:24:19,820
I mean, the whole passage really, but the quote really struck me as well.

265
00:24:19,820 --> 00:24:22,540
Just the two lines there.

266
00:24:22,540 --> 00:24:26,380
I was in all that struck by it. I found it kind of confusing.

267
00:24:26,380 --> 00:24:27,980
Oh, how's that?

268
00:24:27,980 --> 00:24:33,980
Well, watchfully mindful, carefully mindful with the ways of the mind well-watched.

269
00:24:33,980 --> 00:24:38,620
Maybe not confusing, but I just knew right away it wasn't quite what was being said.

270
00:24:38,620 --> 00:24:47,100
It starts out being watchfully mindful

271
00:24:47,100 --> 00:24:51,660
and then watching the ways of the mind

272
00:24:51,660 --> 00:24:55,420
with the ways of the mind well-watched, possessed of a mind.

273
00:24:55,420 --> 00:24:58,780
So, it's saying two different things.

274
00:24:58,780 --> 00:25:03,580
First, you're watching the mind and then you've got the mind that's watching.

275
00:25:03,580 --> 00:25:06,940
But it's not really wrong, it's just...

276
00:25:06,940 --> 00:25:11,900
That's why I went to the poly because I want to see what's really being said here.

277
00:25:11,900 --> 00:25:16,060
I mean, it's important to frame it in regards to the senses,

278
00:25:16,060 --> 00:25:20,620
which he does the quote does say, guarding the senses.

279
00:25:20,620 --> 00:25:25,980
But it's Arrakasatino Nipakasatino, which is just this rhetorical tool

280
00:25:25,980 --> 00:25:28,540
repeating the same thing in different words.

281
00:25:28,540 --> 00:25:39,740
With mindfulness as a guard, with mindfulness in careful mindfulness,

282
00:25:39,740 --> 00:25:43,740
with guarding mindfulness.

283
00:25:43,740 --> 00:25:47,900
With the mind well-guarded,

284
00:25:47,900 --> 00:25:51,340
and then with the mind that is guarded by mindfulness.

285
00:25:51,340 --> 00:25:59,100
So, I'm not really sure about that translation as usual.

286
00:25:59,100 --> 00:26:00,300
That makes sense.

287
00:26:00,300 --> 00:26:05,420
Yeah, so actually the thing that really struck me was just the first line there,

288
00:26:05,420 --> 00:26:08,940
come live with the nose of the senses guarded.

289
00:26:08,940 --> 00:26:12,380
As soon as I read that, it was like, that's a nice quote.

290
00:26:12,380 --> 00:26:15,740
But I definitely understand what you meant there.

291
00:26:15,740 --> 00:26:18,860
So, I mean, you see, watchfully mindful isn't correct.

292
00:26:18,860 --> 00:26:19,660
I mean, it's nice.

293
00:26:19,660 --> 00:26:21,580
That part I really like actually.

294
00:26:21,580 --> 00:26:24,060
But it's not watchfully mindful, watchfully.

295
00:26:24,060 --> 00:26:28,300
Arrakasatino, it's not watchfully, it's a guard.

296
00:26:28,300 --> 00:26:31,020
Arrakasatino's a guard or two.

297
00:26:31,500 --> 00:26:33,980
Maybe to care for you could say,

298
00:26:33,980 --> 00:26:37,820
but maybe he's doing okay.

299
00:26:37,820 --> 00:26:39,100
I mean, it's all fine.

300
00:26:39,100 --> 00:26:40,540
He's certainly got license.

301
00:26:40,540 --> 00:26:42,940
There's nothing wrong with changing it around.

302
00:26:42,940 --> 00:26:47,580
And it's just, I like going to the poly to find exactly what it's being said.

303
00:26:47,580 --> 00:26:51,920
And it's why it's nice to learn the original, because you get a taste of what the Buddha was actually saying.

304
00:26:54,940 --> 00:26:58,380
Yeah. There's no replacement for the original.

305
00:26:59,380 --> 00:27:02,660
That's true. It's always so wonderful to get a bit of the poly.

306
00:27:02,660 --> 00:27:13,900
Yeah, I just got thrown off by the second half of the translation, because it's a bit, uh, a bit wonky.

307
00:27:17,020 --> 00:27:17,500
I see.

308
00:27:19,500 --> 00:27:25,020
Well, thanks for making that career as well. And, uh, five points of pride for you as well.

309
00:27:25,340 --> 00:27:26,980
Yeah. We're adding them in.

310
00:27:26,980 --> 00:27:34,980
Okay. Good night, everyone. Good night, Simon. Good night. Thank you. Bye. Bye.

