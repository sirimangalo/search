1
00:00:00,000 --> 00:00:10,000
Hello and welcome back to Askamund. Today I'll be answering the question as to what a Buddhist does when someone they know dies.

2
00:00:10,000 --> 00:00:24,000
It's not directly related to the meditation practice, but as Buddhists and meditators in the Buddhist tradition, this is something that we experience that we have to deal with.

3
00:00:24,000 --> 00:00:35,000
That for most of us will be on the extreme end, something that is quite difficult to deal with and to come to terms with.

4
00:00:35,000 --> 00:00:44,000
So simply meditating in a way is not really sufficient. So there are things that we should talk about in regards to this.

5
00:00:44,000 --> 00:00:55,000
From a practical point of view, a Buddhist point of view, I would say there are three things that we should do that we should keep in mind.

6
00:00:55,000 --> 00:01:01,000
Three duties as Buddhist or three points to keep in mind when a person may know dies.

7
00:01:01,000 --> 00:01:13,000
The first one is that we should never ever agree or think that grieving is somehow appropriate, somehow useful, somehow good.

8
00:01:13,000 --> 00:01:32,000
The reason why we grieve is because it's great suffering for us. You can think of a small child who has to stay with a babysitter the first time when their parents go away will often cry all night, thinking that they're not even thinking that their parents have left,

9
00:01:32,000 --> 00:01:46,000
but simply suffering terribly because of the clinging that they have, that they've developed as a small child for their parents. So this terrible suffering that goes on at loss,

10
00:01:46,000 --> 00:01:52,000
which really carries over and everything in our lives. People cry over many different things.

11
00:01:52,000 --> 00:02:10,000
Now the point being that we tend to cling to people a lot more than we cling to any other object, really. Our other people are things that we can interact with that have great deep meaning for us in our lives, and so we cling to them greatly.

12
00:02:10,000 --> 00:02:32,000
And why we feel suffering when a person passes away. It's not based on any logic or reason that a person should grieve, and we should really get this point across that there is no reason to mourn.

13
00:02:32,000 --> 00:02:51,000
It is something that is based on our addiction. The reason why we cry is because of the chemicals involved with crying, crying is a reaction that we have developed as a species to deal with difficult situations.

14
00:02:51,000 --> 00:03:03,000
It's something that releases pleasure chemicals and orphans. I think it makes you feel calm and happy and peaceful, so it becomes an addiction actually.

15
00:03:03,000 --> 00:03:14,000
And this is how grieving works. It can be the cryings of people take to alcohol or any sort of addiction, but crying is again another addiction.

16
00:03:14,000 --> 00:03:27,000
And so we should never think that this is somehow important or a part of the process, a part of the grieving process. Many people will go on for years suffering because a person has passed away.

17
00:03:27,000 --> 00:03:45,000
And the point is that there's no benefit to it. You don't help the person who passed away. You don't help yourself. You don't do something that they would want for you. There's no one who wants their loved ones to suffer, to mourn, to have to be without.

18
00:03:45,000 --> 00:04:01,000
And so in the end, it's really just useless. In fact, worse than useless, it drags you down in many ways. It drags you down into a cycle of suffering and depression.

19
00:04:01,000 --> 00:04:16,000
And also addiction, it makes you become, you know, because you start to find a way out. You try to find a way to block it out and not have to think about it, which gives rise to many different types of addiction.

20
00:04:16,000 --> 00:04:33,000
Now this brings up a good point that often people are unable to follow this advice. This first point, important as it is, it's best phrased as don't see it as a good thing, because if you say to people don't mourn,

21
00:04:33,000 --> 00:04:51,000
well, unless they had no attachment to the person, they're going to feel sad. And I've given this talk before at funerals, and then after you give a talk, the people come up and cry. And so you think, well, why did I explain to them about not to cry and how it's not really useful or any benefit?

22
00:04:51,000 --> 00:05:05,000
It didn't intend to. You can't blame people for crying when a loved one dies. But some people go to the next level and think, yes, yes, this is good and this is important.

23
00:05:05,000 --> 00:05:15,000
So they encourage it in themselves and they encourage the phenomenon. And so they end up crying more, and they think of it as somehow a good thing.

24
00:05:15,000 --> 00:05:30,000
And so they can get caught up and actually waste a lot of time and energy doing so. But this also raises another point that we most often get ourselves into this position.

25
00:05:30,000 --> 00:05:45,000
When a person dies, we can go on for days and weeks and months and even years, mourning is because we've been negligent. We've been heedless in regards to our mind. We haven't been paying attention to our attachments.

26
00:05:45,000 --> 00:05:53,000
We live our life thinking that attaching to other people is good. It's intimacy and relationships and love and so on.

27
00:05:53,000 --> 00:06:05,000
But this really has nothing to do with love. It has to do with our own clinging. When you love someone, it's not what they can do for you and what they bring to you and the great things that they give to you.

28
00:06:05,000 --> 00:06:13,000
That's a kind of, we use the word love, but it really is an attachment. Love is when you wish for good things for other people.

29
00:06:13,000 --> 00:06:25,000
And there's nothing good that comes from crying. You don't somehow respect the person's memory by crying by bringing yourself suffering. You don't do something that they would want you to do.

30
00:06:25,000 --> 00:06:38,000
So this is the first point. But what I was going to say is that for this reason, because we get ourselves into this mess, part of this is even before a person dies.

31
00:06:38,000 --> 00:06:49,000
We should be quite careful with our relationships. Our relationships should be meaningful, but the meaning should come from our love for each other. Our wish for others each other to be happy.

32
00:06:49,000 --> 00:07:01,000
And in fact, our wish for all beings to be happy. The only way we can really be purely have pure love for other beings is to have it be impartial.

33
00:07:01,000 --> 00:07:13,000
It's not that we don't love anyone, we end up loving everyone. This is the Buddha said that we should strive to love all beings just as a mother might love her only child.

34
00:07:13,000 --> 00:07:19,000
And if we can get to this point, if we really have love for people, it won't matter who comes and who goes.

35
00:07:19,000 --> 00:07:31,000
Once it's true love, then the next person you see is a loved one for you. It's the one you love. And it continues on when they go.

36
00:07:31,000 --> 00:07:43,000
There's nothing in love that says they need to do anything for you or be anything for you. So when they're gone, there's no suffering, there's no stress, there's no sadness.

37
00:07:43,000 --> 00:07:59,000
So this is an important reason why we should be practicing meditation. Because when we don't practice meditation, we cling and cling and cling and we set ourselves up for so much stress and suffering that we see all around us.

38
00:07:59,000 --> 00:08:10,000
Many people are living their lives in great peace and happiness simply because they haven't met with the inevitable crash. They have an inevitable loss, loss of youth.

39
00:08:10,000 --> 00:08:21,000
When they get old, so many things that they can't enjoy and suddenly they're confronted with old age or sickness when people get cancer and eventually death.

40
00:08:21,000 --> 00:08:27,000
And the death of loved ones or the death of our own death was totally unprepared for.

41
00:08:27,000 --> 00:08:55,000
Meditation practices to allow us to have meaningful but non-attached relationships with other people where we give and we spread the goodness all around us and try our best to make other people happy and to bring peace to them instead of wishing for them to bring good things to us.

42
00:08:55,000 --> 00:09:07,000
Which is of course a totally uncertain something we can't depend on. So this is the first point that is really useless to grieve and it's something that as Buddhists we should not do.

43
00:09:07,000 --> 00:09:20,000
We should, on the other hand, spend our lives thinking about the inevitability of death and this brings me to the second point that when a person dies, it should be a reminder to us.

44
00:09:20,000 --> 00:09:34,000
And the Buddha was very clear about this for monks when a person dies and even for lay people when a person dies that we should consider this to be a sign. A sign that we too will have to go.

45
00:09:34,000 --> 00:09:39,000
We should be clear that this is something that we can't avoid. It's a fact of life.

46
00:09:39,000 --> 00:09:49,000
So the contemplation on the reality of death is a very important part of our acceptance when a person dies.

47
00:09:49,000 --> 00:10:08,000
So first of all the realization that this was a part of that person's life and that's a part of nature and not getting caught up in this idea of a person or this one life where this being existed and now they're gone or so on.

48
00:10:08,000 --> 00:10:20,000
But thinking in terms of reality, in terms of our experience and that this is the nature of reality that all things that arise in our mind the idea of a person will eventually cease.

49
00:10:20,000 --> 00:10:34,000
Everything that we cling to, a person, a place, a thing, even an ideology. Eventually we will have to give it up. Nothing will last forever.

50
00:10:34,000 --> 00:10:48,000
And so we use death as a really good wake-up call for us to realize the truth of reality and also to reflect on our own mortality.

51
00:10:48,000 --> 00:11:00,000
When a person dies we should take that as a warning to ourselves. We should watch and see and look and think of the person who is dead and think of this will also come to me.

52
00:11:00,000 --> 00:11:12,000
One day will I be ready for it. This is an important, not only is it a good meditation and useful for ourselves but it's a really good way to come to terms with a person to death.

53
00:11:12,000 --> 00:11:27,000
Because instead of grieving or thinking, oh now that person's gone, you realize that you remind yourself that this is reality and the real problem is our attachments to a specific state of being.

54
00:11:27,000 --> 00:11:34,000
A person, a place, a thing being like this, not being like that or being with us and not being away from us.

55
00:11:34,000 --> 00:11:44,000
Once we start to look at reality and look at the experience of reality for a moment to moment now, what's happening and so on.

56
00:11:44,000 --> 00:11:55,000
Then the idea of how things should be or how things used to be or how things will be or so on is really irrelevant.

57
00:11:55,000 --> 00:12:03,000
It doesn't have the same hold on us. We're able to live here and now in peace and happiness and that here and now extends forever of course.

58
00:12:03,000 --> 00:12:11,000
When you're able to be happy here and now then you're able to be happy for eternity and that's the reality of life.

59
00:12:11,000 --> 00:12:35,000
So that's number two is that we should use it as an opportunity to reflect on our own mortality and reflect on the inevitability of death. That it comes to all beings and we should remind ourselves that this is really the reality of that person and it shouldn't actually be any positive or negative significance.

60
00:12:35,000 --> 00:12:44,000
It should be seen as the way things are. We should not be happy or unhappy about it. We should not be regretful or mourning or so on.

61
00:12:44,000 --> 00:12:52,000
This is what we should do is we should take it as an opportunity to reflect on the way things go in light.

62
00:12:52,000 --> 00:12:59,000
Now this is more for ourselves. For the person who passed away, most people want to know what we should do for the person.

63
00:12:59,000 --> 00:13:06,000
This is the third point that as Buddhists, we should do things on behalf of the person who has passed away.

64
00:13:06,000 --> 00:13:28,000
Whatever good things that person undertook or even if they didn't do any good things, we should carry on their name or celebrate their life by giving, by building things by building things that will be of use to people.

65
00:13:28,000 --> 00:13:47,000
Dedicating our works to them and so on. Dedicating books or whatever we do, dedicating our work, our good deeds to that person be it things that they did in their life and we can continue on or just things done in their name with some mention of them.

66
00:13:47,000 --> 00:13:59,000
Because this is really a way of celebrating the person's life and it's a way of bringing meaning to that person's life. It's a way of saying that this person's life had real meaning.

67
00:13:59,000 --> 00:14:06,000
If this person hadn't lived, then these good deeds wouldn't come. We do these deeds especially for that person.

68
00:14:06,000 --> 00:14:17,000
Therefore, those persons good deeds that have been done, it's a way of respecting and celebrating and honoring the person who passed away.

69
00:14:17,000 --> 00:14:25,000
It's also a way of coming to terms with the death in the best way possible, celebrating the person's life, saying this person passed away.

70
00:14:25,000 --> 00:14:35,000
Most people, the best they can do is to remember the good things the person did and say, oh yeah, it was such a good person. So they celebrate and they have a party and they get drunk and so on. And that's it.

71
00:14:35,000 --> 00:14:45,000
But that's not really celebrating a person's life. When you celebrate a person's life, it should be all the good things that they did should be continued on or goodness should be done in their name.

72
00:14:45,000 --> 00:15:05,000
So if it's a person who means something to you and you think their life had meaning, then you should do something, it may not be a lot, but you should make effort to do something in their name and that will be your way of finding closure and of ending that person's life on a meaningful note.

73
00:15:05,000 --> 00:15:16,000
This person died and the result was that we have done this on behalf of that person. Sometimes what that means is an inheritance that we get.

74
00:15:16,000 --> 00:15:31,000
We use part of the inheritance to honor the person to do something, not because of duty or something, but as a good deed for us and a good deed for them and something bringing goodness to the world on behalf of that person.

75
00:15:31,000 --> 00:15:48,000
What you find more often, which is really the worst thing a person could possibly do when another person dies, is people fight over the inheritance of their parents or people who passed away, which is really a horrible thing, but apparently goes on quite often.

76
00:15:48,000 --> 00:16:02,000
And this is something as Buddhist we should guard against totally. We should never think of a person's, another person's possessions as somehow belonging to us or that somehow we deserve them.

77
00:16:02,000 --> 00:16:08,000
Never. We should never look at a person and think this person owes me something as Buddhist we should never think like that.

78
00:16:08,000 --> 00:16:17,000
So the idea, even if your parents, you help them or whatever, you should never think that somehow that's going to give you some inheritance and you're going to get something.

79
00:16:17,000 --> 00:16:26,000
We should be very careful to guard against this because it will crop up and when they die you'll find yourself fighting and breaking apart your family.

80
00:16:26,000 --> 00:16:39,000
Could you imagine what your parents would think when they see their children at each other's throats over things that they actually left behind, that their parents had to throw away, that's really garbage.

81
00:16:39,000 --> 00:16:47,000
It could be money, it could be home possession. It's all garbage because in the end they had to leave it behind and you have to leave it behind.

82
00:16:47,000 --> 00:16:59,000
Much better is whatever good things that they have, you can take some of it if it's useful to support you, but making sure that everyone else has taken care of and giving things in their name.

83
00:16:59,000 --> 00:17:18,000
If the least you do is giving up some hold that you have on what fights that people have over inheritance, in order that harmony might be bestowed upon your family.

84
00:17:18,000 --> 00:17:30,000
In order for the sake of harmony and out of respect for the person who passed away, I'm not going to fight over this. If that's the least you, then you've done a great thing in their memory and you've honored their memory.

85
00:17:30,000 --> 00:17:50,000
There are many ways that you can do this, but this could be the least is to not, for goodness sake, don't fight over people who passed away, fight over their belongings. It's a terrible, terrible thing and be very careful not to let that happen to you, because it might come into the mind.

86
00:17:50,000 --> 00:18:07,000
If you can ever be sure, we might find ourselves doing that. These are three points that I think you should keep in mind when the person you know passes away as a Buddhist or as a person who follows these sorts of teachings and really any good teachings, it's nothing specifically to do with Buddhism.

87
00:18:07,000 --> 00:18:21,000
So, hope this helps, thanks for tuning in and all the best.

