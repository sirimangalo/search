1
00:00:00,000 --> 00:00:26,500
Okay, good evening everyone, welcome to our evening dumber.

2
00:00:26,500 --> 00:00:31,000
Thank you all for coming out. It's always great to see such a turnout locally.

3
00:00:31,000 --> 00:00:41,800
Everyone has come out of their rooms. After a hard days work, struggling with their own minds,

4
00:00:41,800 --> 00:00:50,000
all of you in second life and on YouTube will come out to listen.

5
00:00:50,000 --> 00:01:07,000
I think a full house here in second life on YouTube. My channel has surpassed 50,000 subscribers apparently.

6
00:01:07,000 --> 00:01:19,000
I appreciate the effort people are taking to practice the Buddha's teaching. It's not easy.

7
00:01:19,000 --> 00:01:26,000
Sometimes it seems like Buddhism is all about suffering, no?

8
00:01:26,000 --> 00:01:36,000
Anyway, didn't the Buddha talk about suffering? Wasn't that something he'd like to talk about?

9
00:01:36,000 --> 00:01:42,000
It's not very encouraging.

10
00:01:42,000 --> 00:01:49,000
I think a lot of people, of course, look at Buddhism and are not satisfied with it.

11
00:01:49,000 --> 00:01:59,000
When the surface talking a lot about suffering, and then when they begin to practice meditation,

12
00:01:59,000 --> 00:02:06,000
a lot of suffering. What the heck, right?

13
00:02:06,000 --> 00:02:13,000
This isn't what we want. What do we want? What does everyone want?

14
00:02:13,000 --> 00:02:23,000
We want this happiness. Everyone wants happiness. No one wants suffering.

15
00:02:23,000 --> 00:02:32,000
So just spend all our time talking about suffering. Rather unpleasant.

16
00:02:32,000 --> 00:02:38,000
It's a kind of a recipe for unhappiness, wouldn't it?

17
00:02:38,000 --> 00:02:41,000
The question might come up. Why didn't the Buddha talk more about happiness?

18
00:02:41,000 --> 00:02:46,000
If he was so enlightened and so happy, was the Buddha happy?

19
00:02:46,000 --> 00:02:51,000
If so, why didn't he talk about it more?

20
00:02:51,000 --> 00:02:54,000
Is this a suspicion that perhaps the Buddha wasn't happy?

21
00:02:54,000 --> 00:03:09,000
Perhaps he was just denying the existence or the potential for happiness?

22
00:03:09,000 --> 00:03:12,000
Why did he phrase it in a negative?

23
00:03:12,000 --> 00:03:22,000
It's a question and it's led Buddha's teachers to try and reframe the four noble truths as happiness.

24
00:03:22,000 --> 00:03:30,000
Happiness, the cause of happiness, the cessation of happiness, and the path which leads to the cessation of happiness.

25
00:03:30,000 --> 00:03:37,000
And so to explain it in exactly the opposite way.

26
00:03:37,000 --> 00:03:46,000
It's fine, but the question remains why didn't the Buddha do that? Why didn't the Buddha talk like that?

27
00:03:46,000 --> 00:03:56,000
It comes down, I think. A big problem is the question of course what is happiness?

28
00:03:56,000 --> 00:04:05,000
What do we mean when we say happiness?

29
00:04:05,000 --> 00:04:14,000
We certainly don't mean physical pleasure, is that what we mean by happiness?

30
00:04:14,000 --> 00:04:20,000
I think most of us have gotten beyond that.

31
00:04:20,000 --> 00:04:27,000
That's children, we seek out the pleasure of ice cream, the pleasure of bright colors,

32
00:04:27,000 --> 00:04:36,000
shiny objects, toys, games, tickling, snuggling, hugging, kissing.

33
00:04:36,000 --> 00:04:46,000
The adults actually fall into that as well, but sensual happiness.

34
00:04:46,000 --> 00:04:52,000
And I think for the most part adults come to see beyond that.

35
00:04:52,000 --> 00:05:01,000
So as you grow up, you realize it's actually really just a recipe for disappointment, through addiction, right?

36
00:05:01,000 --> 00:05:13,000
When children become addicted to pleasure, they're ultimately disappointed when they can't get what they want.

37
00:05:13,000 --> 00:05:18,000
They throw a tantrum and they suffer a lot.

38
00:05:18,000 --> 00:05:29,000
When they start to see them, maybe not consciously, but certainly through practice and through experience,

39
00:05:29,000 --> 00:05:38,000
they see the nature of sensuality as being a mix, an inevitable mix of pleasure and pain.

40
00:05:38,000 --> 00:05:46,000
You can't really have one without the other, because being partial to something is having expectations about it.

41
00:05:46,000 --> 00:06:01,000
It's day and of course nothing lasts forever and when things eventually change, there's disappointment, there's pain, there's stress, there's suffering.

42
00:06:01,000 --> 00:06:12,000
So beyond that, I think generally people will look at more refined forms of happiness.

43
00:06:12,000 --> 00:06:19,000
And even though it's very hard to give up the addiction, essential pleasure, we tend to see that.

44
00:06:19,000 --> 00:06:22,000
It might be fun, but it can't be considered true happiness.

45
00:06:22,000 --> 00:06:32,000
And so we look beyond that to things like power, power, soap, or rebal, soap.

46
00:06:32,000 --> 00:06:34,000
The calm is so kind, it's not the way.

47
00:06:34,000 --> 00:06:40,000
But when one power, soap, and rebal, it's a good meaning, when about states are being

48
00:06:40,000 --> 00:06:43,000
able to look at us sitting in this room.

49
00:06:43,000 --> 00:06:46,000
How happy we are, how wonderful is this.

50
00:06:46,000 --> 00:06:52,000
We have a roof over our heads, good company, good food.

51
00:06:52,000 --> 00:07:09,000
I don't know how good the food is, but frozen food, warmth, our ability to sit here, together, and peace, not have to fight.

52
00:07:09,000 --> 00:07:13,000
There's a Baba Sukhoi Baba soap that we have.

53
00:07:13,000 --> 00:07:16,000
States are being, when you get a new job, for example.

54
00:07:16,000 --> 00:07:19,000
It's funny, because why would you celebrate that?

55
00:07:19,000 --> 00:07:25,000
It means you have to go work, but the idea of having a new job, the concept of it is a great thing.

56
00:07:25,000 --> 00:07:28,000
Everyone's congratulatory.

57
00:07:28,000 --> 00:07:37,000
There's nothing to do with sensuality, because essentially speaking it's a lot, you're just suffering because you have to do the work.

58
00:07:37,000 --> 00:07:49,000
But conceptually, it's a great thing, because it ultimately allows you more sensual pleasure, potentially, or it allows you freedom from pain.

59
00:07:49,000 --> 00:07:54,000
The concept of getting a job is a great happiness.

60
00:07:54,000 --> 00:07:59,000
Having a family, someone sent me a...

61
00:07:59,000 --> 00:08:06,000
I don't really want to tease this person too much, but they sent me an email.

62
00:08:06,000 --> 00:08:14,000
Because they wanted to let me know of the wonderful occasion of having a child.

63
00:08:14,000 --> 00:08:23,000
The way they expressed it was, they wanted to share the great moment with me.

64
00:08:23,000 --> 00:08:25,000
I thought it was a...

65
00:08:25,000 --> 00:08:31,000
From a Buddhist perspective, it's not really something we congratulate people.

66
00:08:31,000 --> 00:08:36,000
When the Buddha left home, he found out he was a father.

67
00:08:36,000 --> 00:08:39,000
And he's a rau-long jata.

68
00:08:39,000 --> 00:08:42,000
A fetter has been born.

69
00:08:42,000 --> 00:08:46,000
And the king heard this and the king said, well, let that be his name.

70
00:08:46,000 --> 00:08:49,000
Kind of out of spite, it seems, but that's what he call it.

71
00:08:49,000 --> 00:08:56,000
So it's a son ended up being called rau-la, which means a bond or a bind.

72
00:08:56,000 --> 00:09:06,000
Because, before he left the Buddha, he sent us a rau-long jata.

73
00:09:06,000 --> 00:09:09,000
But, you know, this is it.

74
00:09:09,000 --> 00:09:13,000
When things happen, they please us.

75
00:09:13,000 --> 00:09:15,000
This is what life is all about.

76
00:09:15,000 --> 00:09:21,000
The juada vivka, for example, is kind of taking a walk in the forest.

77
00:09:21,000 --> 00:09:23,000
It's not really the centrality per se.

78
00:09:23,000 --> 00:09:26,000
It's the peace.

79
00:09:26,000 --> 00:09:31,000
When you even say the vivka, the vivka is not having to deal with all the stress of life.

80
00:09:31,000 --> 00:09:35,000
If you go to work, you go to school, you deal with your family.

81
00:09:35,000 --> 00:09:37,000
And it's all stress and suffering.

82
00:09:37,000 --> 00:09:39,000
And so you leave.

83
00:09:39,000 --> 00:09:44,000
And just the freedom from all of that stress.

84
00:09:44,000 --> 00:09:50,000
That's happiness.

85
00:09:50,000 --> 00:09:53,000
And there are other ways of looking at happiness.

86
00:09:53,000 --> 00:09:55,000
Some people are optimistic.

87
00:09:55,000 --> 00:10:01,000
And they think optimism, positivity, is happiness.

88
00:10:01,000 --> 00:10:09,000
So as long as you see the good in everything, it's an example, I think.

89
00:10:09,000 --> 00:10:14,000
Polyanna is a bit of an example of that.

90
00:10:14,000 --> 00:10:20,000
When there was this movie, life is wonderful, I think, when I was in university,

91
00:10:20,000 --> 00:10:22,000
a long time ago.

92
00:10:22,000 --> 00:10:29,000
About the Holocaust, and this Jewish man, and his son, and he looked at everything positively.

93
00:10:29,000 --> 00:10:31,000
And as a result, good things happened to him.

94
00:10:31,000 --> 00:10:34,000
But eventually he was killed in a concentration camp.

95
00:10:34,000 --> 00:10:41,000
But up to the end, he had a positive attitude.

96
00:10:41,000 --> 00:10:46,000
So we look at this kind of thing and we say, well, that's it.

97
00:10:46,000 --> 00:10:51,000
Well, this is, I think, how neuroscientists tend to talk about happiness.

98
00:10:51,000 --> 00:10:56,000
And that some people are more capable of being happy.

99
00:10:56,000 --> 00:10:58,000
Because of their positive outlook.

100
00:10:58,000 --> 00:11:07,000
And so there was this one researcher who was talking about how, through meditation,

101
00:11:07,000 --> 00:11:09,000
you can become more positive.

102
00:11:09,000 --> 00:11:16,000
And you can retrain your brain, to be more happy.

103
00:11:16,000 --> 00:11:23,000
So a result of being more positive and rebounding from negative experience as quicker and so on.

104
00:11:23,000 --> 00:11:26,000
I think that gets closer to Buddhist concept.

105
00:11:26,000 --> 00:11:33,000
But of course, positivity has its problems as well.

106
00:11:33,000 --> 00:11:42,000
It's less likely to lead to disappointment, but it's still likely to lead one to complacency.

107
00:11:42,000 --> 00:11:52,000
In the sense that one suffers, but one doesn't take it seriously, and one refuses to.

108
00:11:52,000 --> 00:11:55,000
It doesn't engage in dwelling on it.

109
00:11:55,000 --> 00:11:58,000
At the same time, it doesn't engage in fixing it.

110
00:11:58,000 --> 00:12:00,000
It doesn't free oneself.

111
00:12:00,000 --> 00:12:04,000
So it keeps coming. The suffering does come back. It's just it doesn't last long.

112
00:12:04,000 --> 00:12:13,000
So it's arguably superior to the other sort of happiness.

113
00:12:13,000 --> 00:12:19,000
Moving on in Buddhism, the other type of happiness,

114
00:12:19,000 --> 00:12:26,000
sort of in a category of its own, is the happiness of meditation.

115
00:12:26,000 --> 00:12:31,000
It doesn't be together with this idea of positivity, but it's not exactly.

116
00:12:31,000 --> 00:12:35,000
It's a change in mind state to be sure.

117
00:12:35,000 --> 00:12:55,000
It's a change in mind state to the extent that one experience constant and prolonged states that are pleasurable are happy.

118
00:12:55,000 --> 00:13:03,000
So like the first John, the first trans of tranquility meditation, is a company with happiness.

119
00:13:03,000 --> 00:13:08,000
It's a kind of happiness that's removed from addiction.

120
00:13:08,000 --> 00:13:14,000
You can't actually be addicted to the happiness, because the mind isn't cultivating addiction.

121
00:13:14,000 --> 00:13:21,000
There's no it's so focused, but there's no room for clinging to it.

122
00:13:21,000 --> 00:13:27,000
It's so powerful that the mind doesn't develop the addiction.

123
00:13:27,000 --> 00:13:32,000
It's freeing, it's liberating in that sense.

124
00:13:32,000 --> 00:13:40,000
And so often people who practice this will describe it as being a liberation or being a liberation.

125
00:13:40,000 --> 00:13:44,000
It is possible to become attached to the idea of the John,

126
00:13:44,000 --> 00:13:52,000
and those of them when you leave them behind want them again.

127
00:13:52,000 --> 00:13:54,000
And so ultimately the Buddha noted about this type of happiness.

128
00:13:54,000 --> 00:14:01,000
He said, well that's the pinnacle really of any kind of feeling, but it's still impermanent.

129
00:14:01,000 --> 00:14:06,000
When you leave it, you can still be left wanting more.

130
00:14:06,000 --> 00:14:11,000
You think about that, and you compare it to what you have when you're out at the John end.

131
00:14:11,000 --> 00:14:15,000
You want to chat them back, but they're not permanent.

132
00:14:15,000 --> 00:14:20,000
And ultimately this is the real problem with any of the types of happiness,

133
00:14:20,000 --> 00:14:27,000
anything that we could call happiness, beyond the ultimate happiness,

134
00:14:27,000 --> 00:14:33,000
which we're personally going to get to.

135
00:14:33,000 --> 00:14:36,000
Is that, it doesn't last.

136
00:14:36,000 --> 00:14:38,000
You can say, I've got the best life.

137
00:14:38,000 --> 00:14:42,000
You know, this is working for me.

138
00:14:42,000 --> 00:14:47,000
People who have good jobs, good cards, even while you start at the bottom,

139
00:14:47,000 --> 00:14:54,000
people who have sensual height, the pinnacle of sensual pleasure, angels,

140
00:14:54,000 --> 00:15:00,000
for example, human beings who are rich, powerful.

141
00:15:00,000 --> 00:15:05,000
The drug addicts even will think for a time that they are in heaven,

142
00:15:05,000 --> 00:15:10,000
but it doesn't last when you have a good job, a good status,

143
00:15:10,000 --> 00:15:13,000
security, you feel secure.

144
00:15:13,000 --> 00:15:15,000
It doesn't last.

145
00:15:15,000 --> 00:15:20,000
People who have positive thoughts, if you're mind, if your brain is wired,

146
00:15:20,000 --> 00:15:25,000
such that you are able to be positive about everything,

147
00:15:25,000 --> 00:15:29,000
to be happy, even in the face of great suffering.

148
00:15:29,000 --> 00:15:32,000
Even that doesn't last.

149
00:15:32,000 --> 00:15:37,000
There's no reason to think that that state is going to be permanent.

150
00:15:37,000 --> 00:15:40,000
The positivity leads.

151
00:15:40,000 --> 00:15:49,000
That it's stable.

152
00:15:49,000 --> 00:15:55,000
In fact, what we find through wisdom,

153
00:15:55,000 --> 00:16:00,000
meaning as we truly understand the nature of all of this,

154
00:16:00,000 --> 00:16:06,000
reality, happiness and suffering,

155
00:16:06,000 --> 00:16:13,000
as that freedom from suffering is far preferable to any of these things.

156
00:16:13,000 --> 00:16:21,000
And so we have to talk about technically and happiness as being a negative thing.

157
00:16:21,000 --> 00:16:24,000
The negative in the sense of being free from something,

158
00:16:24,000 --> 00:16:28,000
as opposed to gaining something.

159
00:16:28,000 --> 00:16:31,000
It's not being free from anyone experience.

160
00:16:31,000 --> 00:16:34,000
It's not like we bother so aware when something goes away,

161
00:16:34,000 --> 00:16:37,000
you feel happy, all good, it's gone.

162
00:16:37,000 --> 00:16:38,000
What a relief.

163
00:16:38,000 --> 00:16:43,000
And that's a key point because that people misunderstand that freedom from suffering,

164
00:16:43,000 --> 00:16:49,000
the happiness is because, oh, good, I'm not suffering anymore, but it's not.

165
00:16:49,000 --> 00:16:51,000
It's not a relief.

166
00:16:51,000 --> 00:16:55,000
It's not a feeling of relief.

167
00:16:55,000 --> 00:17:02,000
Because the things that caused you happiness and caused you suffering before are still there.

168
00:17:02,000 --> 00:17:07,000
The change is that you're no longer seeking them.

169
00:17:07,000 --> 00:17:12,000
You're no longer disturbed by them.

170
00:17:12,000 --> 00:17:15,000
You're no longer subject to their control.

171
00:17:15,000 --> 00:17:20,000
So there's the happiness of freedom.

172
00:17:20,000 --> 00:17:25,000
The happiness of being in a state or of gaining the knowledge,

173
00:17:25,000 --> 00:17:32,000
the understanding that nothing is worth clinging to.

174
00:17:32,000 --> 00:17:36,000
Basically, there is nothing that can make you happy.

175
00:17:36,000 --> 00:17:41,000
It's a kind of happiness that goes beyond any type of happiness.

176
00:17:41,000 --> 00:17:44,000
It's not dependent on anything.

177
00:17:44,000 --> 00:17:49,000
It's not subject to change.

178
00:17:49,000 --> 00:17:52,000
It's not vulnerable.

179
00:17:52,000 --> 00:17:55,000
It's invincible.

180
00:17:55,000 --> 00:18:00,000
And beyond that, it is experienced as far preferable,

181
00:18:00,000 --> 00:18:05,000
far superior, anyone who has attained the state of freedom.

182
00:18:05,000 --> 00:18:10,000
No longer holds any delusion about sensuality or becoming,

183
00:18:10,000 --> 00:18:14,000
or even non-becoming, even the genres.

184
00:18:14,000 --> 00:18:23,000
The idea that they might be somehow ultimate or true happiness.

185
00:18:23,000 --> 00:18:26,000
And so this is the Buddha actually said, quite clearly,

186
00:18:26,000 --> 00:18:30,000
is that Nati-santi-parang sukham.

187
00:18:30,000 --> 00:18:33,000
Which isn't a quote that we often bring up,

188
00:18:33,000 --> 00:18:36,000
because it sounds kind of contentious,

189
00:18:36,000 --> 00:18:39,000
because it's hard to understand this,

190
00:18:39,000 --> 00:18:41,000
with all of our ideas of what is happiness.

191
00:18:41,000 --> 00:18:49,000
Nati-santi-parang sukham means there's no happiness outside of peace.

192
00:18:49,000 --> 00:18:56,000
Peace is the only sort of happiness.

193
00:18:56,000 --> 00:18:59,000
Which is sensible.

194
00:18:59,000 --> 00:19:04,000
It's reasonable, but it's hard to understand

195
00:19:04,000 --> 00:19:13,000
because it's a negative happiness.

196
00:19:13,000 --> 00:19:16,000
Peace is a sublime state.

197
00:19:16,000 --> 00:19:20,000
It's not really familiar to most people.

198
00:19:20,000 --> 00:19:25,000
It's not really even all that desirable for a lot of people.

199
00:19:25,000 --> 00:19:28,000
Except in Maimiya, an abstract sense,

200
00:19:28,000 --> 00:19:32,000
so I wish I had some peace of mind for example.

201
00:19:32,000 --> 00:19:44,000
But peace is something quite sublime.

202
00:19:44,000 --> 00:19:49,000
It was once asked of, I'm sorry, put that

203
00:19:49,000 --> 00:19:53,000
under the monks, Yamakai, I think was his name.

204
00:19:53,000 --> 00:19:55,000
He asks, sorry, put that.

205
00:19:55,000 --> 00:20:01,000
If there's no way, then there's no way to Nati-mana.

206
00:20:01,000 --> 00:20:04,000
How can you say it's happiness?

207
00:20:04,000 --> 00:20:06,000
How can you say Nati-mana is happiness when there's no way

208
00:20:06,000 --> 00:20:08,000
that Nati-mana means feeling.

209
00:20:08,000 --> 00:20:12,000
So happiness is a type of feeling, right?

210
00:20:12,000 --> 00:20:15,000
That's the idea of happy feelings.

211
00:20:15,000 --> 00:20:19,000
Pleasant feelings, unpleasant feelings, neutral feelings.

212
00:20:19,000 --> 00:20:22,000
If there's no feelings in Nati-mana,

213
00:20:22,000 --> 00:20:24,000
if freedom has no feelings,

214
00:20:24,000 --> 00:20:27,000
how can you say it's happy?

215
00:20:27,000 --> 00:20:31,000
Right, this is, I think, a sticking point for a lot of people.

216
00:20:31,000 --> 00:20:34,000
If there's a fear of the idea of Nibana,

217
00:20:34,000 --> 00:20:37,000
have to give up the world.

218
00:20:37,000 --> 00:20:43,000
But there's so much in the world that I want.

219
00:20:43,000 --> 00:20:46,000
Actually, Nibana isn't about giving up the world.

220
00:20:46,000 --> 00:20:50,000
It's about leaving it behind, and it's something

221
00:20:50,000 --> 00:20:53,000
that can be experienced for a short time.

222
00:20:53,000 --> 00:20:56,000
It's not something that's not an all or nothing thing

223
00:20:56,000 --> 00:20:59,000
that's what, like, whoops, wait, let me back.

224
00:20:59,000 --> 00:21:01,000
I want to go back.

225
00:21:01,000 --> 00:21:05,000
Let me off, let me off.

226
00:21:05,000 --> 00:21:09,000
Something experienced and you're able to see.

227
00:21:09,000 --> 00:21:10,000
Is it worth it?

228
00:21:10,000 --> 00:21:11,000
Is it not worth it?

229
00:21:11,000 --> 00:21:13,000
Sorry, but does reply was,

230
00:21:13,000 --> 00:21:16,000
it's precisely because there is no way

231
00:21:16,000 --> 00:21:21,000
that Nibana is happiness.

232
00:21:21,000 --> 00:21:25,000
Some blind are difficult to understand.

233
00:21:25,000 --> 00:21:27,000
Let's get practice meditation.

234
00:21:27,000 --> 00:21:29,000
Of course, as you practice,

235
00:21:29,000 --> 00:21:31,000
you start to shed more and more of your attachments,

236
00:21:31,000 --> 00:21:33,000
more and more of your desires.

237
00:21:33,000 --> 00:21:36,000
As you see, the things that you thought were happiness

238
00:21:36,000 --> 00:21:39,000
are not really happiness.

239
00:21:39,000 --> 00:21:41,000
And to finally, you truly let go,

240
00:21:41,000 --> 00:21:43,000
there's an epiphany, and the mind says,

241
00:21:43,000 --> 00:21:45,000
oh, yes, I think it's worth clinging to,

242
00:21:45,000 --> 00:21:49,000
and then poof the cessation.

243
00:21:49,000 --> 00:21:52,000
Then you can answer this question

244
00:21:52,000 --> 00:21:55,000
whether Nibana is happiness or not,

245
00:21:55,000 --> 00:21:57,000
when you experience it for yourself.

246
00:22:01,000 --> 00:22:03,000
So, there you go.

247
00:22:03,000 --> 00:22:05,000
There's the dhamma for this evening.

248
00:22:05,000 --> 00:22:07,000
I hope the audio didn't cut out.

249
00:22:07,000 --> 00:22:12,000
Thank you all here locally for sitting patiently,

250
00:22:12,000 --> 00:22:14,000
coming up to listen,

251
00:22:14,000 --> 00:22:18,000
and through everyone at home,

252
00:22:18,000 --> 00:22:20,000
wish you all a good practice,

253
00:22:20,000 --> 00:22:22,000
and thank you for tuning in.

