Are you saying that you analyze the act of sexual activities including masturbation in order to stop the act entirely?
Yes, yes exactly, that's what I'm saying.
But the point is that you need not engage in masturbation.
That's not part of it.
And it's a delicate line.
Why I say that and why I emphasize that is because masturbation will actually decrease your awareness.
It's not something you should feel guilty about.
Guilt is a negative mindset and it's not going to help.
But you should never get the idea that if I don't masturbate I'm going to start repressing it.
This is I think a pervasive misunderstanding.
When the desire comes up and you start engaging in bodily activities that are the precursor to masturbation,
start to watch those.
If it happens that you start to engage in masturbation then watch yourself.
And you'll see and it may be that in the beginning you can't stop yourself.
But you will begin to see, no, you will not begin to see it right away you will see the misunderstanding.
That there's actually no pleasure or no happiness to be found here.
Once you look and you accept this pleasure because the funny thing about engaging in sensuality is
we're not engaging in the happiness in the happiness at all.
We're engaging in the desire for the happiness.
We're focusing much more on the desire than on the happiness.
And when you focus on the happiness suddenly you reassure yourself that,
I don't need to do this because the happiness is right here.
And you look at it and then you say it's nothing.
You don't reject it and say this is horrible, this is actually suffering.
You'll see it's useless.
You don't even really see this useless, but you experience it.
There's happiness and that's it.
There's no reaction.
There's no thought this is good, this is bad, this is me, this is mine, this is this, this is that.
It's happiness and it's nothing.
It is what it is, it's there and then it goes.
You don't become angry or frustrated or disgusted by it.
You just become disenchanted with it.
You lose this enchantment, the desire disappears.
The happiness doesn't disappear or the happiness is not the issue.
But the important thing is that the desire disappears because the happiness will disappear.
It will be gone.
But as you watch the happiness, sorry, I'm focusing on the, I'm referring to the pleasure here.
Focusing on the pleasure will, first of all, allow you to stop the whole seeking process.
And in second of all, I'd like you to give up the desire for the happiness entirely.
It will allow you to become free from, you know, free from the desire.
It will allow you to become disenchanted, it will allow you to come to terms with the pleasure
and let you see that it's actually just an experience.
