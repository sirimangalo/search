1
00:00:00,000 --> 00:00:20,440
what's wisdom. I kind of answered that in the other one. Wisdom is knowing

2
00:00:20,440 --> 00:00:27,640
something thoroughly. The word we use is banya and banya is actually a compound word.

3
00:00:27,640 --> 00:00:35,680
Nya is this catch-all route that refers to knowing. Nya means no. It's

4
00:00:35,680 --> 00:00:41,740
actually probably the same family because Sanskrit poly come from the same

5
00:00:41,740 --> 00:00:48,080
family as Latin. They have the same ancestors. Nya and no it just means

6
00:00:48,080 --> 00:00:53,800
literally to no but but but turns it into what we call wisdom but it means to

7
00:00:53,800 --> 00:00:59,040
no thoroughly or thorough knowledge. So it may be that wisdom is actually a bad

8
00:00:59,040 --> 00:01:03,960
translation but it's a very spiritual word so we use it. It actually means to

9
00:01:03,960 --> 00:01:07,560
know something thoroughly and that's really what it is in a Buddhist sense.

10
00:01:07,560 --> 00:01:11,800
Because yeah we do know that we're walking. We do know that we're sitting. Why

11
00:01:11,800 --> 00:01:16,240
do we have to say to ourselves walking? Walking. Walking. And this is the answer

12
00:01:16,240 --> 00:01:22,480
because we want to know it thoroughly. According to the Buddhist text so the

13
00:01:22,480 --> 00:01:26,840
Buddha's teaching that we follow and according to our practice I mean it

14
00:01:26,840 --> 00:01:33,840
goes without saying. All you have to do is see something clearly in one

15
00:01:33,840 --> 00:01:38,120
moment and you'll enter into nibhana. It only takes one moment. This is talking

16
00:01:38,120 --> 00:01:43,440
about how the noble path the it full noble path is only one moment because at

17
00:01:43,440 --> 00:01:48,240
the moment when you're mind and I talked about this before it enters into anuloma.

18
00:01:48,240 --> 00:01:54,680
So it and anu means with loma means the grain. The mind begins to go with the

19
00:01:54,680 --> 00:02:02,280
grain. It's it's no longer out of sync out of touch with reality. It's now

20
00:02:02,280 --> 00:02:08,360
perfectly in tune with reality so it's going it's like what I was saying when

21
00:02:08,360 --> 00:02:11,520
you know those comics where they're pushing this guy out the door and he

22
00:02:11,520 --> 00:02:17,400
grabs he puts his arms out and pushes on the door frame. What kind of like this?

23
00:02:17,400 --> 00:02:22,280
We can't fit we can't escape because of all of our clinging right and when you

24
00:02:22,280 --> 00:02:32,360
let go you go with the grain and you you escape you're free. That's that's this

25
00:02:32,360 --> 00:02:40,680
one moment where the mind suddenly is in tune with reality and that's due to

26
00:02:40,680 --> 00:02:48,640
wisdom due to what we call wisdom the clear awareness of the object for what it is.

27
00:02:48,640 --> 00:02:54,040
In detail you can talk about what what is that experience like and we talk

28
00:02:54,040 --> 00:02:59,360
about the three characteristics. Wisdom means to see the three characteristics to

29
00:02:59,360 --> 00:03:06,480
see impermanence to see suffering and to see non-self. So I've talked about

30
00:03:06,480 --> 00:03:13,840
these before the impermanence means to see that it's not a entity. No it's

31
00:03:13,840 --> 00:03:20,800
not a thing that you know my hand for example it's not a hand it's a series of

32
00:03:20,800 --> 00:03:25,400
a momentary experiences. So when you see this when you actually see it or

33
00:03:25,400 --> 00:03:31,520
arising in CC this is a kind of wisdom and this is enough to allow you to

34
00:03:31,520 --> 00:03:36,760
let go. This is enough for you to say it's useless it's there's nothing in that.

35
00:03:36,760 --> 00:03:40,320
Before we looked at my hand and I say oh there's a scar and that's not a good

36
00:03:40,320 --> 00:03:45,840
thing and you know maybe I like my hand because it's well-formed and fleshy

37
00:03:45,840 --> 00:03:52,000
and strong or whatever. I don't like it it's it's too white or too pink or so

38
00:03:52,000 --> 00:03:56,480
and then look at my lines in my hand and they're not you know people do this

39
00:03:56,480 --> 00:04:01,600
palmistry and so on and so on. All of that is clinging and there's so much more

40
00:04:01,600 --> 00:04:06,640
clinging. At the moment when you see it in a different way you see or you

41
00:04:06,640 --> 00:04:11,440
experience the feeling of flexing of clenching your fist for example and you

42
00:04:11,440 --> 00:04:16,680
see that arising in CC takes incredible clarity of mind. That's a moment of

43
00:04:16,680 --> 00:04:23,040
wisdom where the mind lets go. The mind suddenly sees and just that shifting

44
00:04:23,040 --> 00:04:27,160
allows it to slip out. It's exactly like the mind is stuck in the door frame

45
00:04:27,160 --> 00:04:34,360
can't fit through and suddenly it shaves something off and fits. So wisdom is

46
00:04:34,360 --> 00:04:39,520
what I'm trying to say is that wisdom is really simple. It's not intellectual.

47
00:04:39,520 --> 00:04:45,800
It's the moment of epiphany of realization. Oh you're saying I actually did

48
00:04:45,800 --> 00:04:50,360
answer the question already. Well there's some more information. Okay so hopefully

49
00:04:50,360 --> 00:04:53,360
that was useful.

