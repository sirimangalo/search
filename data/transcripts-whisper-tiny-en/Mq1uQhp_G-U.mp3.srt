1
00:00:30,000 --> 00:00:41,880
Okay, good evening everyone, welcome to our evening dumb session, just supposed to be questions

2
00:00:41,880 --> 00:00:49,480
answers to it, but I think we exhausted all questions last week, there's only five this

3
00:00:49,480 --> 00:01:02,920
book. So maybe we'll try and elaborate on at least one of the questions, give a little

4
00:01:02,920 --> 00:01:17,200
bit of a longer talk and form of an answer. So some asks say a really good question, how

5
00:01:17,200 --> 00:01:23,920
do you explain ignorance playing second fiddle to craving as the root cause of suffering

6
00:01:23,920 --> 00:01:31,240
according to the second noble truth? In other words, why is craving highlighted as the

7
00:01:31,240 --> 00:01:44,920
cause of suffering instead of ignorance? So to recap, the context of this question, there

8
00:01:44,920 --> 00:01:56,360
are four noble truths. What it means is there's a difference between a noble truth and

9
00:01:56,360 --> 00:02:06,120
an ordinary truth. Truth is a very special thing, but there are many truths that there's

10
00:02:06,120 --> 00:02:11,520
special in the fact that they're true because there are many more things that are false.

11
00:02:11,520 --> 00:02:19,320
There's only one capital of friends in that spirit, or a lot of other things that are

12
00:02:19,320 --> 00:02:36,680
not a capital of friends. That's not a noble truth. So the four would explicitly delineated

13
00:02:36,680 --> 00:02:48,040
the number of noble truths as four, but none more than that. Many other truths in Buddhism.

14
00:02:48,040 --> 00:02:57,160
But the reason why only these four are noble is because they involve the realization

15
00:02:57,160 --> 00:03:09,120
of nibhana, they're the only ones that involve directly the realization of nibhana.

16
00:03:09,120 --> 00:03:18,440
So in that sense, actually, the truth of them is not really noble in the sense that

17
00:03:18,440 --> 00:03:26,760
if you know them intellectually, if you've read about the four noble truths and you

18
00:03:26,760 --> 00:03:41,480
believe them, that's still not noble. The four noble truths are the experience of freedom,

19
00:03:41,480 --> 00:03:51,480
the truth of suffering, the meditator realizes that experience is not something that you

20
00:03:51,480 --> 00:03:58,920
could cling to, it's not something that can satisfy you. Getting and getting more and

21
00:03:58,920 --> 00:04:08,880
more experiences, the right type of experiences will never, never lead to happiness.

22
00:04:08,880 --> 00:04:15,840
That's the first noble truth. It really, that's the first noble truth. The second noble

23
00:04:15,840 --> 00:04:25,840
truth is what is, what is the reason, the reason why suffer cause of suffering is while

24
00:04:25,840 --> 00:04:36,080
craving. And it's craving after those things that actually can't satisfy you. Raving after

25
00:04:36,080 --> 00:04:43,040
those things that cause you stress. They cause you stress when you cling to them. So the

26
00:04:43,040 --> 00:05:05,920
cause of stress is craving or clinging or having to say it. It's a cause of suffering because

27
00:05:05,920 --> 00:05:12,880
of where it leads us. I mean, you can extrapolate upon this the reason why we

28
00:05:12,880 --> 00:05:20,360
work so hard and strive so hard in the world and fight with each other. It's all because

29
00:05:20,360 --> 00:05:26,440
of our desire for specific experiences, certain experiences, or our aversion towards

30
00:05:26,440 --> 00:05:45,680
that are experiences, fear of certain, what comes down to just experiences. The third

31
00:05:45,680 --> 00:05:58,480
noble truth is the cessation of suffering. The noble truth, both in the fact that it means

32
00:05:58,480 --> 00:06:04,440
that the cessation of cravings, the cessation of suffering, but also it's noble in fact

33
00:06:04,440 --> 00:06:16,440
that that is the freedom, that is the cessation of suffering that is nibana. It's true happiness.

34
00:06:16,440 --> 00:06:24,120
The opposite of the first noble truth. And the fourth noble truth is the path that leads

35
00:06:24,120 --> 00:06:31,240
to the cessation of suffering. So again, the cause of happiness and the cause of happiness

36
00:06:31,240 --> 00:06:40,440
is really the opposite of craving. It's the opposite of reacting. So cravings is sort of

37
00:06:40,440 --> 00:06:47,680
a reaction to experience. When a thought arises in the mind, there's a desire for it.

38
00:06:47,680 --> 00:06:55,000
Like you think of malicious food, there arises a craving, a desire. And the desire of

39
00:06:55,000 --> 00:07:01,320
course leads you then to strive for it, which is the beginning of stress and suffering

40
00:07:01,320 --> 00:07:13,960
and problems. When you can't get what you want, when you refer your craving, your addiction,

41
00:07:13,960 --> 00:07:20,720
or when you get what you want, and it's a cause of suppose you want, you want cheesecake

42
00:07:20,720 --> 00:07:26,680
and that you get cheesecake, and getting it is actually, there's a lot of suffering involved,

43
00:07:26,680 --> 00:07:34,640
because maybe that's not what we've rather suffering involved because you have expectations.

44
00:07:34,640 --> 00:07:39,560
But there are consequences of getting what you want, right? If you get lots and lots of cheesecake

45
00:07:39,560 --> 00:07:47,880
you'll become sick, you become unhealthy. People who eat a lot of sweets or people who

46
00:07:47,880 --> 00:07:53,720
do drugs is a very extreme example. You get what you want is not, but it's not really fair

47
00:07:53,720 --> 00:08:02,040
to say that that's suffering. Suffering is for the because of the attachment. It may be

48
00:08:02,040 --> 00:08:10,720
a still fair, because people with attachment or a strong attachment are going to cultivate

49
00:08:10,720 --> 00:08:20,440
experiences that are very stressful. The core reason is because of their addiction, not

50
00:08:20,440 --> 00:08:26,120
because of the results. The getting sick isn't really suffering, it's suffering because

51
00:08:26,120 --> 00:08:32,120
you don't want to be sick, because we have desire for pleasant experiences. But it's true

52
00:08:32,120 --> 00:08:38,160
that the more you strive after pleasant experiences, more likely you are to fall into

53
00:08:38,160 --> 00:08:48,120
unpleasant experiences. A person who lives contently, who lives off in the forest, they'll

54
00:08:48,120 --> 00:08:57,400
be from certain types of suffering or stressful experiences, simply by virtue of not needing

55
00:08:57,400 --> 00:09:10,120
to fight with people and work, labor, laborious jobs and so on. Anyway, so the path

56
00:09:10,120 --> 00:09:19,160
so that cause of suffering is our craving. The path which leads out of that is the opposite

57
00:09:19,160 --> 00:09:24,160
when you have experience in your mind. Let me see it, full noble path really, but it

58
00:09:24,160 --> 00:09:30,080
involves this practice that we're doing of learning how to have a perfect experience,

59
00:09:30,080 --> 00:09:42,880
an experience that's free from that sort of reaction. So those are the four noble truths

60
00:09:42,880 --> 00:09:49,680
and the second one says that craving is the cause of suffering. But we also, if you look

61
00:09:49,680 --> 00:09:55,840
at the teacher's samupada, it's clear that independent origination, it's clear that ignorance

62
00:09:55,840 --> 00:10:01,840
is the beginning. Without ignorance, there would be no craving, but why is an ignorance

63
00:10:01,840 --> 00:10:08,960
the cause of suffering? Because what we mean by ignorance is ignorance of the fact,

64
00:10:08,960 --> 00:10:20,160
and so the four noble truths, that's what is being by ignorance. So I mean, suggesting

65
00:10:20,160 --> 00:10:28,560
that ignorance is the cause of suffering is, it's like saying you have a thorn in your

66
00:10:28,560 --> 00:10:37,600
side or a sliver in your hand or something, a piece of glass in your foot and it's causing

67
00:10:37,600 --> 00:10:43,440
suffering, but it's made worse by the fact that you don't even know that it's there, right?

68
00:10:46,560 --> 00:10:53,200
If you don't know that the sliver is there, that makes that, that's the reason why it continues

69
00:10:53,200 --> 00:11:02,560
to hurt you because you haven't gotten rid of it, or maybe more precisely, suppose you have,

70
00:11:02,560 --> 00:11:09,200
suppose you eat unhealthy food, it's the unhealthy food that's causing you sickness,

71
00:11:09,200 --> 00:11:14,480
but it's the ignorance of the fact that the food is unhealthy for you that's causing you to eat

72
00:11:14,480 --> 00:11:19,840
an unhealthy food. So it would be more fair to say that ignorance is the cause of craving.

73
00:11:21,680 --> 00:11:26,080
Without craving, there is no, without ignorance, there is no craving.

74
00:11:26,080 --> 00:11:35,520
And what that all that means is, if you knew that the things that you were craving were

75
00:11:35,520 --> 00:11:43,760
not satisfying, if you knew that craving was useless, was meaningless, or if you experienced things

76
00:11:43,760 --> 00:11:51,760
as they are, there would be no craving. When we walk, stepping right, stepping left in that moment,

77
00:11:51,760 --> 00:11:57,120
there's no opportunity for the arising, craving, because our interaction with our experiences

78
00:11:57,840 --> 00:12:01,680
is equanimus, it's clear, it's neutral, it's pure.

79
00:12:08,480 --> 00:12:13,440
So this is why ignorance is not, and the thing is that ignorance isn't really a thing, right?

80
00:12:13,440 --> 00:12:20,720
You don't suddenly give rise to ignorance. Ignorance just means the fact that you don't know yet.

81
00:12:22,960 --> 00:12:30,320
It's not something really that arises, so it's nothing. Ignorance is a state,

82
00:12:30,320 --> 00:12:37,360
it's a description of a type of person. A person has ignorance, it just means that they have

83
00:12:37,360 --> 00:12:45,280
not yet learned the truth. And it really means, I've not yet become enlightened,

84
00:12:45,280 --> 00:12:50,800
because it's the experience of nibana that does away with what we term ignorance,

85
00:12:50,800 --> 00:12:58,080
changes a person. So ignorance is a description of a world in an ordinary person. A person has

86
00:12:58,080 --> 00:13:07,760
become enlightened, it's free from that ignorance. It's in the category of being free from ignorance.

87
00:13:10,000 --> 00:13:12,000
Anyway, that may be enough to say about that.

88
00:13:16,000 --> 00:13:20,720
Why are the cemetery contemplations per to the city, but honestly, it's a good question, it's not

89
00:13:20,720 --> 00:13:27,920
really relatable for this group, because we're not wondering whether we should practice them.

90
00:13:27,920 --> 00:13:33,360
The Sati Bhutan is, so this maybe gives us a chance to talk about some of the time we pass in.

91
00:13:33,360 --> 00:13:38,400
There are certain types of meditation or ways of focusing your attention that are

92
00:13:41,600 --> 00:13:46,800
not just for the purposes of tranquilizing the mind. The cemetery contemplations are that sort of

93
00:13:46,800 --> 00:13:53,600
meditation, but they're also just generally useful. I think they're included there just because

94
00:13:53,600 --> 00:14:02,560
the Sati Bhutan is there, it seems to be a way of describing a life, a way of living your life,

95
00:14:02,560 --> 00:14:06,560
if you read through various sections of a special guy in a person, a body.

96
00:14:08,080 --> 00:14:13,920
That's one. If you contemplate dead bodies, it fortifies your inside practice, because

97
00:14:13,920 --> 00:14:24,080
it creates sobriety about the body and it reduces your delusions about the body being something

98
00:14:24,080 --> 00:14:30,640
beautiful or special or wonderful. When you see dead bodies rotting and

99
00:14:30,640 --> 00:14:44,720
pus coming out of them, breaking up with just flesh hanging off the bones and eventually which is

100
00:14:44,720 --> 00:14:51,200
bone. We have some questions that I don't really know. We'll go through them very quickly.

101
00:14:51,200 --> 00:15:01,920
What's the difference between a dead body and a body that's alive?

102
00:15:03,200 --> 00:15:07,440
My body isn't something that exists. I mean, I guess you'd want to say the mind isn't there

103
00:15:08,640 --> 00:15:20,400
in a dead body, but your body is just concept. It's a temporary amalgamation or

104
00:15:20,400 --> 00:15:28,640
conglomeration of physical particles, but there's a history of why it's there. You're just

105
00:15:28,640 --> 00:15:35,520
really seeing an echo of some activity, some metal activity. The dead body is like an echo of the

106
00:15:35,520 --> 00:15:43,680
mind. If you think back to how it arose, it rose in the womb of the mother and was created, we

107
00:15:43,680 --> 00:15:52,000
would say, based on the activity, the active mind in the womb. When you die on that seizes,

108
00:15:52,000 --> 00:15:58,960
that's all you've got left is sort of the echo of that activity. What's the relation between

109
00:15:58,960 --> 00:16:10,480
the previous and next birth? Well, that's maybe long question, long answer, explaining rebirth,

110
00:16:10,480 --> 00:16:17,040
but basically again, birth is just a concept. The mind is born and dies every moment.

111
00:16:19,280 --> 00:16:26,240
When we die, it's just the body, it's more like discarding in this body and starting fresh

112
00:16:26,240 --> 00:16:27,520
because the body wears out.

113
00:16:32,800 --> 00:16:37,280
Is rebirth caused by the result of the mind clinging onto more experiences?

114
00:16:37,280 --> 00:16:43,120
Yeah. It's really just generally the fact that there's clinging left because the mind

115
00:16:44,240 --> 00:16:50,400
with clinging will seek out. So clinging on to more experiences, absolutely.

116
00:16:53,440 --> 00:16:56,800
It's performing magic, not practicing the precept on deceit.

117
00:17:00,000 --> 00:17:04,960
If you're talking about slight of hand, not real magic, but performing

118
00:17:04,960 --> 00:17:12,160
fake magic, not really doing magical tricks, but presenting them as being magical tricks,

119
00:17:12,160 --> 00:17:17,120
so that's one thing. I'd say most magicians accept the fact that their audience doesn't

120
00:17:17,120 --> 00:17:23,840
believe that they actually are magical and then knows that they're being trick, which makes it not

121
00:17:23,840 --> 00:17:32,400
a trick. So magic just becomes the act of trying to figure out how someone did what they did.

122
00:17:32,400 --> 00:17:41,120
So it's a matter of magic. In that sense, it's just about hiding, trying to hide

123
00:17:41,120 --> 00:17:50,000
your methods and the audience is left wondering about your methods, not about pretending that

124
00:17:50,000 --> 00:17:55,760
you did something one way and instead doing another way. Like people don't actually leave

125
00:17:55,760 --> 00:18:02,640
while, although it's interesting, if you're talking about kids, you may actually do, you may actually

126
00:18:02,640 --> 00:18:11,520
fool the children. For example, Santa Claus, let's say, lying to children that there exists

127
00:18:11,520 --> 00:18:17,280
this Santa Claus is actually lying. It's actually unethical. Santa Claus is really bad, really bad

128
00:18:17,280 --> 00:18:23,520
idea, all around bad. There's nothing good about Santa Claus to come to think about it.

129
00:18:23,520 --> 00:18:29,040
I'm probably wrong, I'm probably if I thought about it long enough. But a superficial look at

130
00:18:29,040 --> 00:18:34,160
Santa Claus mixing them out to be a real bad guy. First of all, there's this guilt, right?

131
00:18:34,160 --> 00:18:39,120
It's like this boogeyman. He knows if you've been bad or good, right? So it's a way of guilt

132
00:18:39,120 --> 00:18:45,840
tripping kids. It's a way of making ethics and morality all about rewards. If you don't do this,

133
00:18:45,840 --> 00:18:50,720
you're not going to get some toy that you want. I mean, what does that say about morality?

134
00:18:50,720 --> 00:18:55,360
And they say it's going to be good for goodness sake, but you know, this song, right?

135
00:18:56,000 --> 00:18:59,200
But it's, Santa Claus is the opposite of doing good for goodness sake.

136
00:19:00,880 --> 00:19:05,280
Santa Claus is about doing good because your parents lied to you about getting rewards.

137
00:19:08,960 --> 00:19:12,640
You could say it's all for good fun, but not disagree.

138
00:19:13,520 --> 00:19:17,200
Tricking your children and then eventually they find that they've been lying to them all these

139
00:19:17,200 --> 00:19:21,920
years and they learn that's the good way to behave. They don't lie to people.

140
00:19:23,520 --> 00:19:30,480
Yeah, as I know, it's all good fun to some extent, but that's not my idea of good fun.

141
00:19:32,560 --> 00:19:37,200
I was thinking about this old gift giving thing. The gift giving around Christmas is also

142
00:19:37,200 --> 00:19:43,680
I really am not so keen on it, especially with kids because of all the green involved.

143
00:19:43,680 --> 00:19:48,560
But gifts should be much more natural, you know?

144
00:19:50,000 --> 00:19:54,640
For Christmas, there tends to be, I don't know. I mean, if people really enjoy it and have

145
00:19:54,640 --> 00:20:02,880
have, it's great to give. I'm just not convinced that it's the best idea to have a holiday

146
00:20:02,880 --> 00:20:08,640
forgiving. If you want to make a tradition in your, in your family that when everyone gets

147
00:20:08,640 --> 00:20:15,040
together, we all give gifts, but it's still, I think, misses the point. You know, this whole

148
00:20:15,040 --> 00:20:25,680
giving gifts to everyone. I mean, it's very active sort of giving. It's very much, you know,

149
00:20:25,680 --> 00:20:32,000
active in the sense of wanting to give and creating this desire to give, which is actually not

150
00:20:32,000 --> 00:20:39,440
really very Buddhist. Giving should be natural. Someone needs something, you give it to them.

151
00:20:40,800 --> 00:20:47,280
Someone asks you for something or, or someone deserves something like your, your parents,

152
00:20:47,280 --> 00:20:55,600
for example. You think, oh, I want to express my gratitude or try to do a teacher or that sort

153
00:20:55,600 --> 00:20:59,520
of thing. Oh, sorry, I shouldn't. I'm thinking of my teachers and so that. I'm not trying to

154
00:20:59,520 --> 00:21:10,160
say, just giving me anything, don't go there. But yeah, I had this whole Christmas thing.

155
00:21:11,920 --> 00:21:16,560
I think if Christmas is about getting together as a family, I think that part is fairly wholesome,

156
00:21:17,360 --> 00:21:25,040
but the gift giving part. We had this charity thing. We all got together and gave money to charity,

157
00:21:25,040 --> 00:21:31,600
I didn't, but all my students did and my family sort of got involved. And I think that's a

158
00:21:31,600 --> 00:21:38,160
Christmas tradition. I'm not, I'm not the only one family's do that, you know, but we did it as

159
00:21:38,160 --> 00:21:43,840
well. We got this really nice letter back from them saying how much we're helping children.

160
00:21:46,160 --> 00:21:48,400
That's better than this old Santa Claus crop.

161
00:21:48,400 --> 00:21:58,720
I'm being purposely controversial, I think. Well, you'd be broadcasting on second life in the

162
00:21:58,720 --> 00:22:04,640
future. I'm not sure actually. I've been, I've got an email saying in my inbox and I have to answer

163
00:22:04,640 --> 00:22:13,120
about that. But I was thinking about second life and I don't really see the point. I like to

164
00:22:13,120 --> 00:22:22,320
support the Buddhist Center that thing, but I'm not clear that it's actually a great, you know what

165
00:22:22,320 --> 00:22:28,640
I'd like to, I was thinking about today. I think it'd be a real gimmick if I did a virtual reality,

166
00:22:30,160 --> 00:22:34,640
some kind of virtual reality thing. I tried virtual reality when I was in Thailand.

167
00:22:34,640 --> 00:22:42,720
And I was able to sit up on a cliff and meditate and I was sitting up on a cliff, you know,

168
00:22:44,160 --> 00:22:49,920
and look around. I was thinking, you know, be a need by me and give me a mean and be something

169
00:22:50,880 --> 00:22:57,520
that might get the attention of the press or something. I mean, it's kind of silly thing to do,

170
00:22:57,520 --> 00:23:05,120
but it might be neat to go that way and make something that's not been done before is that,

171
00:23:05,120 --> 00:23:10,800
you know, you walk, you sit, you go into this virtual reality and suddenly suddenly I come

172
00:23:10,800 --> 00:23:15,120
walking towards you and explain to you how to meditate and we go through the exercises together.

173
00:23:16,560 --> 00:23:21,760
I don't know how difficult that would be to program, but if anybody out there knows how to do that,

174
00:23:21,760 --> 00:23:30,480
maybe we could think about that. But in a mass media is much more, there's two, two,

175
00:23:31,760 --> 00:23:39,200
I think two useful activities. One is to produce things that can be mass-consumed and

176
00:23:39,200 --> 00:23:45,600
others to do intensive one-on-one training. And I think that's sort of the two

177
00:23:45,600 --> 00:23:53,680
important aspects of, from my point of view, what should be done. But teaching small groups,

178
00:23:53,680 --> 00:24:03,200
giving a talk to small groups, it doesn't really interest me. Because if I'm going to teach 10

179
00:24:03,200 --> 00:24:18,160
people or 5 people who are doing a meditation course, it really, it's not that I'm

180
00:24:18,160 --> 00:24:21,120
mind doing it, it's like going out of my way to do it, it doesn't really seem

181
00:24:24,480 --> 00:24:27,840
all that beneficial. It's just too small of a, I mean, the benefit is

182
00:24:27,840 --> 00:24:34,880
just small. When you know, giving a talk on YouTube is the same, the same result

183
00:24:34,880 --> 00:24:41,520
except it reaches a lot more people. I guess what I really mean is it's a lot of work to

184
00:24:41,520 --> 00:24:47,920
boot up second life and get in there and it's not that I can't do it, it's just I have to ask

185
00:24:47,920 --> 00:24:51,680
myself why I'm doing it. And as a monk, that's an important question because

186
00:24:51,680 --> 00:25:00,480
I have to be careful that I'm not going outside of what a monk should do. Not so much

187
00:25:00,480 --> 00:25:05,120
that I feel lazy or something like that. It's not laziness. It's a question of

188
00:25:07,120 --> 00:25:12,960
going beyond this realm of teaching because people ask you to teach.

189
00:25:12,960 --> 00:25:23,200
I think there's online there. Someone says my microphone is broken, the voice is too low.

190
00:25:26,640 --> 00:25:30,480
There's a reason to stop all this entirely. It's really just not working.

191
00:25:30,480 --> 00:25:40,720
I don't know.

192
00:25:46,720 --> 00:25:47,520
That's test.

193
00:25:47,520 --> 00:26:04,960
I don't know. Yeah, why is that solo? I don't know. It still looks fine to me.

194
00:26:17,520 --> 00:26:28,080
I don't think it changed any settings.

195
00:26:28,080 --> 00:26:50,640
I'll turn your volume up maybe. Anyway, that's all the questions that I think that was

196
00:26:50,640 --> 00:26:56,560
so good number. So I'm going to call it a bit there.

197
00:27:01,280 --> 00:27:07,280
Maybe I'll just speak in quite a little. Anyway, have a good night. Thank you for

198
00:27:07,280 --> 00:27:23,760
tuning in.

