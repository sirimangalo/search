1
00:00:00,000 --> 00:00:09,420
Okay, on Galiana Mitata, I have a childhood friend whom I care for a great deal, but he's

2
00:00:09,420 --> 00:00:10,420
quite base.

3
00:00:10,420 --> 00:00:15,180
I don't like being around him when he talks about drinking, hunting, et cetera.

4
00:00:15,180 --> 00:00:19,740
He does have some potential, but is it worth continuing this relationship?

5
00:00:19,740 --> 00:00:28,420
I'm going to take a fairly hard-line Buddhist stance here, probably not no way you think.

6
00:00:28,420 --> 00:00:32,780
But the hard-line Buddhist stance is that it's not worth keeping any relationship.

7
00:00:32,780 --> 00:00:36,900
Okay, there's no worth in relationships.

8
00:00:36,900 --> 00:00:45,780
The only relationship that is really promoted by the Buddha is that the association with good

9
00:00:45,780 --> 00:00:52,540
people, so it's kind of radical or incorrect to me, even to say suggest that no relationship

10
00:00:52,540 --> 00:00:55,700
is worthwhile.

11
00:00:55,700 --> 00:00:59,940
So you would argue, and you would say, no, the Buddha said, and it's correct, the Buddha

12
00:00:59,940 --> 00:01:09,500
said, good relationships that with good people, people who are your equal in morality and

13
00:01:09,500 --> 00:01:19,540
concentration in wisdom, in practice, in discipline, or better, so better than you at those

14
00:01:19,540 --> 00:01:21,100
things.

15
00:01:21,100 --> 00:01:23,380
This is good.

16
00:01:23,380 --> 00:01:32,100
So the association with these people has potential, and the other type of relationship

17
00:01:32,100 --> 00:01:39,340
is useless, is harmful, is a drag on you.

18
00:01:39,340 --> 00:01:46,460
So the association with people who are less cultivated than you.

19
00:01:46,460 --> 00:01:50,180
We'll get back to that, because that even sounds not so nice, but let's go even harder

20
00:01:50,180 --> 00:01:54,780
than that and say no relationships are worth anything, because we can say that in an

21
00:01:54,780 --> 00:01:58,340
ultimate sense, even though it's not how the Buddha would teach, we can say it in an

22
00:01:58,340 --> 00:02:04,260
ultimate sense, and so we can really get a perspective here, because nothing is truly

23
00:02:04,260 --> 00:02:09,700
nothing in this world, nothing that arises has any intrinsic worth, and the end, even

24
00:02:09,700 --> 00:02:13,260
the good things in this world you have to let go of.

25
00:02:13,260 --> 00:02:20,020
If you cling to them as having some essential worth, you will get stuck, you will be stuck

26
00:02:20,020 --> 00:02:23,540
on them, you will cling to anything, even the good things.

27
00:02:23,540 --> 00:02:27,900
This is why the Buddha taught the simile of the raft, a raft is something you use to get

28
00:02:27,900 --> 00:02:31,740
across a river, it's not something once you get across the river that you put on your

29
00:02:31,740 --> 00:02:34,260
back and carry with you.

30
00:02:34,260 --> 00:02:38,980
So for that reason, even good relationships, you should be able to let go of them at the

31
00:02:38,980 --> 00:02:41,420
drop of the hat.

32
00:02:41,420 --> 00:02:44,060
So how does this relate to bad relationships?

33
00:02:44,060 --> 00:02:51,100
It sounds like what the Buddha is saying is, avoid these people, and I think yes, in some

34
00:02:51,100 --> 00:02:56,740
instances, you should actively avoid such people in cases where they are dragging you down.

35
00:02:56,740 --> 00:03:06,660
If such, a person is dragging you down and you're getting less pure and they're getting,

36
00:03:06,660 --> 00:03:09,420
they're getting less pure and they're dragging you down.

37
00:03:09,420 --> 00:03:14,780
They're not becoming any better for a relationship than you should avoid them.

38
00:03:14,780 --> 00:03:26,500
But I would sort of guess or suggest that in this case, the difference here between this

39
00:03:26,500 --> 00:03:31,260
relationship, a bad relationship and a good relationship, the meaning is a good relationship

40
00:03:31,260 --> 00:03:33,420
should be cultivated.

41
00:03:33,420 --> 00:03:43,420
You should seek out such people, you should visit them, you should incline towards the

42
00:03:43,420 --> 00:03:48,900
participation in the things they do, the things they say, which means you should listen

43
00:03:48,900 --> 00:03:50,420
to what they say.

44
00:03:50,420 --> 00:03:53,820
So there should be an active participation in the relationship.

45
00:03:53,820 --> 00:04:02,900
For bad relationships, the general rule of thumb that I would suggest is to not actively

46
00:04:02,900 --> 00:04:04,300
pursue them.

47
00:04:04,300 --> 00:04:11,620
So the sense is they come to you and this is actually how a teacher has to should behave

48
00:04:11,620 --> 00:04:19,060
in Buddhism, not seeking out students, not chasing after them, not pushing them to practice

49
00:04:19,060 --> 00:04:23,220
or nagging them about the practice, but letting them come.

50
00:04:23,220 --> 00:04:30,980
So when a student comes and asks questions and asks for teachings, then the teacher teaches.

51
00:04:30,980 --> 00:04:35,500
So after you get people who think the other way around, they wait for the teacher to teach

52
00:04:35,500 --> 00:04:38,700
them, and the teacher is like, why would they bother?

53
00:04:38,700 --> 00:04:43,700
It's not up to me to chase after my students.

54
00:04:43,700 --> 00:04:47,980
You often have to remind my students this, I'm not going to chase after you.

55
00:04:47,980 --> 00:04:53,100
If you want to learn, you come and chase after me, and you can come in like an ancient

56
00:04:53,100 --> 00:04:59,100
times, like I've mentioned before you had this sit out in the courtyard, and they wouldn't

57
00:04:59,100 --> 00:05:02,780
let you into the monastery, they'd go away, we don't want to accept people, or to get

58
00:05:02,780 --> 00:05:06,980
into the monastery, you had to actually sit in the courtyard for days, in the rain and

59
00:05:06,980 --> 00:05:12,740
the sun until they finally like fight club, I don't know if they fight club, and to sit

60
00:05:12,740 --> 00:05:20,500
there and tell you they're not going to let you in until finally they would see that

61
00:05:20,500 --> 00:05:25,380
you're very dedicated and that you're in, but at the very least you have to make the effort.

62
00:05:25,380 --> 00:05:35,820
Now, you shouldn't make this effort in regards to people who are your lesser, who are

63
00:05:35,820 --> 00:05:39,780
going to drag you down, let's put it that way, we're not trying to be judgmental and

64
00:05:39,780 --> 00:05:48,260
say lesser, better people who are not especially conducive towards your own practice,

65
00:05:48,260 --> 00:05:53,380
there's no reason to cultivate them, because seeking them out encourages them on their

66
00:05:53,380 --> 00:05:56,980
practice, it's like saying to them, yes, what you're doing is good, I want to be a part

67
00:05:56,980 --> 00:06:03,820
of it, now the only option would be, yes, or know you're doing bad, I want to help you

68
00:06:03,820 --> 00:06:08,540
with that, I want to save you with that, save you from that, but it doesn't in reality

69
00:06:08,540 --> 00:06:15,380
work that way, because you're letting them get their hooks in, any time we apply ourselves

70
00:06:15,380 --> 00:06:21,500
to someone, it's kind of, it's creating a karma, a connection, a hook with that person,

71
00:06:21,500 --> 00:06:26,540
and if it's a person inclined towards bad things, then they have a hook and you and they

72
00:06:26,540 --> 00:06:34,420
can drag you in a bad way, so it's something that should be looked upon with equanimity,

73
00:06:34,420 --> 00:06:39,140
so you talk about a childhood friend who might care for a great deal, which is really

74
00:06:39,140 --> 00:06:46,820
the problem, because in no case in Buddhism, hardline Buddhism, should you care for anyone?

75
00:06:46,820 --> 00:06:51,500
What that means is it shouldn't be about attachment, right?

76
00:06:51,500 --> 00:06:55,220
If you care for someone, I've gotten in trouble with this before, but I stand by it, I did

77
00:06:55,220 --> 00:07:01,060
a video on caring, you should stop caring, you should be uncaring, but it's just words,

78
00:07:01,060 --> 00:07:02,060
it's semantics.

79
00:07:02,060 --> 00:07:09,540
Now, if you care, that means when they suffer, when they're happy, you're happy, when

80
00:07:09,540 --> 00:07:14,020
they suffer, you're unhappy, and so you're dependent on your happiness depends on that

81
00:07:14,020 --> 00:07:16,780
person, it doesn't help that person in any way.

82
00:07:16,780 --> 00:07:26,660
Your caring for someone doesn't help them, it makes them feel cared for, which is good

83
00:07:26,660 --> 00:07:37,340
for people who have low self-esteem and who need their support, but it's not a wholesome

84
00:07:37,340 --> 00:07:41,860
thing for you to actually care for the person, so you can say to the person, I'm here

85
00:07:41,860 --> 00:07:46,460
for you if you ever need me, and that makes them feel good about it, but when you actually

86
00:07:46,460 --> 00:08:00,140
care, it's a difference between caring in the sense of an action, like I care for you

87
00:08:00,140 --> 00:08:06,140
by cleaning your wounds and changing your bandages, and I care for you in terms of feeling

88
00:08:06,140 --> 00:08:09,460
you, and I care for you to take care of someone.

89
00:08:09,460 --> 00:08:15,900
If you mean it in terms of taking care of someone, then care is great, even just to talk

90
00:08:15,900 --> 00:08:21,300
to the person and say, I'm here for you if you need anything, to care for them by teaching

91
00:08:21,300 --> 00:08:26,220
the meditation and so on and helping them in whatever way you can, but to actually care

92
00:08:26,220 --> 00:08:33,940
what happens to someone, it doesn't help the other person, it means that your happiness

93
00:08:33,940 --> 00:08:41,180
is dependent on something external, it doesn't actually make you a better caretaker,

94
00:08:41,180 --> 00:08:49,340
although it does make you more caught up in their happiness and suffering, so for lay

95
00:08:49,340 --> 00:08:58,460
people that have, because you have family and friend who you very much care for, and

96
00:08:58,460 --> 00:09:04,380
we do try to encourage it to be just a people to think just in terms of duties, your

97
00:09:04,380 --> 00:09:10,100
relationships with people are your position in life, and so you do things for such people

98
00:09:10,100 --> 00:09:15,180
because you have duties to your parents, to your families, but in the end no matter what

99
00:09:15,180 --> 00:09:23,260
there will be caring, we care for each other, if someone suffers, we suffer, and if someone

100
00:09:23,260 --> 00:09:30,460
leaves us, if someone dumps us, if someone dies, this causes us suffering, now this is not,

101
00:09:30,460 --> 00:09:36,140
the claim is this is not a beneficial thing, it just makes us suffer a needless thing, it

102
00:09:36,140 --> 00:09:42,700
doesn't make you any better at helping a person out, but what that's doing here is it's

103
00:09:42,700 --> 00:09:51,780
creating an arbitrary reason to maintain a friendship with the person, it says nothing

104
00:09:51,780 --> 00:09:57,300
about the quality of a friendship that you care for a person, the fact that you care a

105
00:09:57,300 --> 00:10:03,260
great deal about this person says nothing about whether it's a good relationship, so if

106
00:10:03,260 --> 00:10:14,780
I care very much for a vicious pitbull, it says nothing about whether that's a good thing

107
00:10:14,780 --> 00:10:21,460
or not, or if I care for a poisonous snake, like in the jotica, there was this snake

108
00:10:21,460 --> 00:10:27,540
charmer who was just in love with his snake and he had a very poisonous snake and he'd

109
00:10:27,540 --> 00:10:31,260
play with his snake all the time until one day, and they all pulled him out of your crazy,

110
00:10:31,260 --> 00:10:38,380
and he said, oh I care for this snake very much, and one day he stuck his hand in the

111
00:10:38,380 --> 00:10:43,220
snake's basket, and this snake was just in a bad mood and it bit him and killed him,

112
00:10:43,220 --> 00:10:53,180
and he's like, it really says not meaningful to say I care for the person, the only question

113
00:10:53,180 --> 00:10:58,220
is whether it's beneficial, whether it's good for you, whether it's a good thing,

114
00:10:58,220 --> 00:11:05,460
whether it's the proper way of approaching the universe, whether it's a part of the cosmic

115
00:11:05,460 --> 00:11:15,620
plan, but what cosmic plan in the sense of in line with reality as far as bringing happiness,

116
00:11:15,620 --> 00:11:19,740
and you don't distinguish between you and the other person, but just in general, doesn't

117
00:11:19,740 --> 00:11:28,820
get closer to a state of peace, or does it incline towards suffering and upset?

118
00:11:28,820 --> 00:11:34,900
This is how you have to look at, and we're usually not able to do that with relationships

119
00:11:34,900 --> 00:11:43,100
where we care for the person, because our reason for being friends with them is arbitrary.

120
00:11:43,100 --> 00:11:44,260
To some extent, arbitrary.

121
00:11:44,260 --> 00:11:49,260
Now, of course, reasons for carrying people usually have to do with the happiness that

122
00:11:49,260 --> 00:11:58,380
we bring each other, so there's that, but the caring in and of itself is actually a danger

123
00:11:58,380 --> 00:12:05,380
for you, because it means that your happiness is dependent on something else.

124
00:12:05,380 --> 00:12:09,020
It seems nice, and that's what society teaches us very strongly.

125
00:12:09,020 --> 00:12:23,140
If you don't carry your heartless, but the truth of it is, we have to be whole, we can't

126
00:12:23,140 --> 00:12:29,100
be dependent on others if you really want to be happy, if you really want to be truly

127
00:12:29,100 --> 00:12:36,140
happy, you have to have some kind of equanimous love for beings, so it has to be universal.

128
00:12:36,140 --> 00:12:41,780
The Buddha's love was universally able to have love for all beings and compassion for

129
00:12:41,780 --> 00:12:50,620
all beings without any kind of partiality, so he had no caring, but it didn't care about

130
00:12:50,620 --> 00:12:58,220
anyone in the sense of actually being upset when a person didn't get his teachings

131
00:12:58,220 --> 00:13:00,820
or didn't like him or so on.

132
00:13:00,820 --> 00:13:07,060
There was no sense of betrayal when they were that to be trained him, there was no sense

133
00:13:07,060 --> 00:13:15,900
of loss when his two chief disciples passed away because he was free, he was at peace,

134
00:13:15,900 --> 00:13:24,340
so things didn't bother him, even though he worked very hard to help the world, but he

135
00:13:24,340 --> 00:13:26,380
did it without caring.

136
00:13:26,380 --> 00:13:28,500
That's awful, I know.

137
00:13:28,500 --> 00:13:32,180
The problem with English language that word has become so steeped in goodness, right?

138
00:13:32,180 --> 00:13:40,500
Caring is good, but sorry, that's not really, that doesn't actually lead to happiness.

139
00:13:40,500 --> 00:13:46,500
Maybe that's a big part of the problem is our culture is so steeped in caring that we

140
00:13:46,500 --> 00:13:48,260
can get upset very easily.

141
00:13:48,260 --> 00:13:53,500
Even if I just tell you that, we care very much about caring, so if I tell you that

142
00:13:53,500 --> 00:13:58,460
it's not good to care, then you're right away get upset, I think many people will

143
00:13:58,460 --> 00:14:04,540
unsubscribe or put nasty comments to my videos and when they find that I'm telling people

144
00:14:04,540 --> 00:14:08,980
not to care that caring is bad, so this is the problem with caring is it actually makes

145
00:14:08,980 --> 00:14:15,420
you angry, it leads to lots of trouble.

146
00:14:15,420 --> 00:14:22,740
When you care about something, it leads to conflict, it does lead to conflict when things

147
00:14:22,740 --> 00:14:27,940
are go against your expectations, so what that helps.

