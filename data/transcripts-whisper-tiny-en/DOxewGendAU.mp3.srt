1
00:00:00,000 --> 00:00:19,540
Good morning.

2
00:00:19,540 --> 00:00:28,440
Okay, good morning.

3
00:00:28,440 --> 00:00:56,440
The Buddha taught, he said, mom, bikhui, anato, we had it, don't live without a refuge.

4
00:00:56,440 --> 00:01:07,440
Do kho bikhui, anato, we had it.

5
00:01:07,440 --> 00:01:23,440
Someone who is without refuge lives in suffering.

6
00:01:23,440 --> 00:01:41,440
I don't think this is hard for us to appreciate or understand something that should be familiar to many of us.

7
00:01:41,440 --> 00:01:52,440
Those of us who have been in our lives without refuge understand how unpleasant it can be.

8
00:01:52,440 --> 00:02:13,440
It's true for many people that doesn't seem such an urgent need to find protection.

9
00:02:13,440 --> 00:02:25,440
At times life can be pleasant, stable, peaceful, and for those people at those times,

10
00:02:25,440 --> 00:02:36,440
it seems like there's not much need for any kind of protection or refuge.

11
00:02:36,440 --> 00:03:00,440
Only when suffering is calamity, disaster, accident, loss, sickness, conflict.

12
00:03:00,440 --> 00:03:13,440
Then we search for something to protect us from that, or we wish we'd had something that would protect us.

13
00:03:13,440 --> 00:03:19,440
Mostly of course we look outside, we look for an external refuge.

14
00:03:19,440 --> 00:03:30,440
And it's not that there isn't any external refuge, and the Buddha himself is our refuge.

15
00:03:30,440 --> 00:03:37,440
The Buddha and the Buddha and the Buddha are the Buddha for refuge.

16
00:03:37,440 --> 00:03:39,440
The Dhamma is our refuge.

17
00:03:39,440 --> 00:03:39,440
The Dhamma is our refuge.

18
00:03:39,440 --> 00:03:47,440
The Dhamma is our refuge.

19
00:03:47,440 --> 00:03:49,440
The Sangha is our refuge.

20
00:03:49,440 --> 00:03:50,440
The Sangha is our refuge.

21
00:03:50,440 --> 00:03:51,440
The Sangha is our refuge.

22
00:03:51,440 --> 00:03:56,440
The Sangha is our refuge.

23
00:03:56,440 --> 00:03:58,440
The Sangha is our refuge.

24
00:03:58,440 --> 00:04:06,440
But the Buddha also said Atahir, the Nona.

25
00:04:06,440 --> 00:04:12,440
One is one's own refuge.

26
00:04:12,440 --> 00:04:19,240
And the Buddha helps, the Dhamma helps, the Sangha helps, but really mostly only because

27
00:04:19,240 --> 00:04:25,600
we help ourselves.

28
00:04:25,600 --> 00:04:39,880
The Dhamma of the Sangha, the Buddha, help us to help ourselves, give us the tools for

29
00:04:39,880 --> 00:04:52,800
us to do the work, because there are many dangers in the world that no external support

30
00:04:52,800 --> 00:05:00,680
or help or refuge can protect us from.

31
00:05:00,680 --> 00:05:12,520
No parent or guardian, no relative can keep us, relative or friend can keep us from so many

32
00:05:12,520 --> 00:05:29,600
types of suffering, so many sources of danger, externally sickness, pain, and even more

33
00:05:29,600 --> 00:05:33,560
internally.

34
00:05:33,560 --> 00:05:48,160
No one can keep us from greed, anger, delusion, from depression, anxiety, fear, addiction,

35
00:05:48,160 --> 00:06:06,800
there's no greater refuge than our own mind, there's no other refuge, there's no real refuge

36
00:06:06,800 --> 00:06:16,640
outside of our own mind, and so when the Buddha said, don't live without refuge, he

37
00:06:16,640 --> 00:06:32,560
was mainly talking about making yourself a refuge, having the foresight to prepare yourself

38
00:06:32,560 --> 00:06:37,200
for danger.

39
00:06:37,200 --> 00:06:46,600
Not in the sense of anticipating danger, like sitting around waiting for danger to come.

40
00:06:46,600 --> 00:07:02,200
But changing your situation from one that is vulnerable, so that your happiness depends

41
00:07:02,200 --> 00:07:03,840
on circumstance.

42
00:07:03,840 --> 00:07:10,120
If things are like this, I'll be happy, as long as they stay like this, as long as they

43
00:07:10,120 --> 00:07:16,320
aren't like that, that's dependent.

44
00:07:16,320 --> 00:07:24,320
That's vulnerable, so one that is independent and invulnerable, whether things are like this

45
00:07:24,320 --> 00:07:40,760
or like that, I am at peace, that's invulnerable.

46
00:07:40,760 --> 00:07:43,920
So how do we make ourselves a refuge?

47
00:07:43,920 --> 00:07:49,920
How do we gain this invulnerability, this is independent?

48
00:07:49,920 --> 00:08:01,440
Well, again, having a refuge doesn't just mean internal support, the best refuge is your

49
00:08:01,440 --> 00:08:10,960
own mind, but part of that involves the refuge that comes from the support of others.

50
00:08:10,960 --> 00:08:19,200
There's other people, the support of others is also dependent on our mind, whether we

51
00:08:19,200 --> 00:08:27,880
are the sort of person worth protecting, worth supporting, whether other people feel comfortable

52
00:08:27,880 --> 00:08:31,880
supporting us.

53
00:08:31,880 --> 00:08:58,600
And Buddha listed 10 ways of creating refuge, 10 dhamma, 10 dhammas for building a refuge.

54
00:08:58,600 --> 00:09:08,040
They're called the nata karana dhamma.

55
00:09:08,040 --> 00:09:13,560
The first one is Sila, of course.

56
00:09:13,560 --> 00:09:21,600
So many of the lists start with Sila, morality ethics, this is a great refuge.

57
00:09:21,600 --> 00:09:39,800
It's a great description of Sila, a great explanation of the importance of Sila that it's

58
00:09:39,800 --> 00:09:40,800
a refuge.

59
00:09:40,800 --> 00:09:48,120
Sila is not a burden, it's not a burden to stop killing and stealing and lying and cheating

60
00:09:48,120 --> 00:09:56,760
and taking drugs, alcohol, hurting others, speaking harsh words and so on, it doesn't

61
00:09:56,760 --> 00:09:57,760
a burden.

62
00:09:57,760 --> 00:10:11,040
It's a great boon if we are able even to just know these things, even to have a sense of

63
00:10:11,040 --> 00:10:25,920
the importance of abstaining from reckless behavior and speech, harmful behavior and

64
00:10:25,920 --> 00:10:48,640
speech.

65
00:10:48,640 --> 00:10:55,800
It's a refuge, of course, because my breaking precepts by doing things that are

66
00:10:55,800 --> 00:11:03,520
important, well, first of all, I'm moral and unethical, of course, you're going to get into

67
00:11:03,520 --> 00:11:18,200
trouble, get into trouble with the law, but more practically, you get in trouble with

68
00:11:18,200 --> 00:11:19,200
people.

69
00:11:19,200 --> 00:11:29,240
On a very basic level, you get in trouble in your own mind, feeling guilty, cultivating

70
00:11:29,240 --> 00:11:40,200
habits of anger and greed and crookedness, real crookedness, you don't think straight

71
00:11:40,200 --> 00:11:46,200
anymore, you think in terms of manipulating others, not in terms of bringing peace and

72
00:11:46,200 --> 00:11:58,200
harmony, your sense of good is skewed, everything is crooked, it's a great danger.

73
00:11:58,200 --> 00:12:06,200
Siles are a very important part of refuge, it's a very important refuge, it's a very stable

74
00:12:06,200 --> 00:12:12,440
refuge, something we can depend on.

75
00:12:12,440 --> 00:12:24,600
The second one is Bahu Sutta, Bahu Sutta, Bahu Sutta, Bahu Sutta literally hearing, what

76
00:12:24,600 --> 00:12:31,680
say learning is what it really means, the Buddha's time, everything was learned through

77
00:12:31,680 --> 00:12:37,120
voice, very little writing.

78
00:12:37,120 --> 00:12:43,680
And today, you see, much of the dhamma has come full circle, for a long time it was all

79
00:12:43,680 --> 00:12:57,560
written, everything would have to be written down, I mean in terms of this mass production

80
00:12:57,560 --> 00:13:04,200
of dhamma, when I learned the dhamma, it was all, in the beginning it was all oral as

81
00:13:04,200 --> 00:13:11,200
well, and go to a meditation center, the teachings you get are all oral, someone explaining

82
00:13:11,200 --> 00:13:26,840
to you, but we never had this potential to speak, to record our voice, now it's very easy

83
00:13:26,840 --> 00:13:34,160
to find recorded teachings, but the meaning is learning, learning the dhamma from

84
00:13:34,160 --> 00:13:49,160
speech or from printed word, it doesn't matter, Bahu Sutta means having much learning.

85
00:13:49,160 --> 00:13:54,800
Now learning is of course something we trivialise to some extent in Buddhism, don't rely

86
00:13:54,800 --> 00:14:05,200
on learning, don't confuse learning with understanding, it's very different, you can

87
00:14:05,200 --> 00:14:10,560
know everything about the dhamma and know nothing about the dhamma at the same time, two

88
00:14:10,560 --> 00:14:21,240
very different things, but that being said it is a great refuge, without any learning

89
00:14:21,240 --> 00:14:29,240
you have no dhamma, you'd have no practice, practice is not something you can do, find

90
00:14:29,240 --> 00:14:35,800
on your own, without the learning, either you spend all the countless lifetimes the Buddha

91
00:14:35,800 --> 00:14:52,280
spent learning for yourself, or you listen to someone who has been there.

92
00:14:52,280 --> 00:15:05,760
And more importantly that the much learning, that learning gives you a framework within

93
00:15:05,760 --> 00:15:19,160
to grow within which to grow, you don't need to learn a lot to just practice, but to grow

94
00:15:19,160 --> 00:15:26,480
and to keep growing, you need a framework, many of the things we learn in the dhamma are

95
00:15:26,480 --> 00:15:34,120
not immediately applicable, even you learn about these ten, not that karana dhamma, you learn

96
00:15:34,120 --> 00:15:40,200
about all these things, some of them won't immediately be practical, but when you remember

97
00:15:40,200 --> 00:15:48,680
them and you come back to them, then you hear them again and again, even just reminding

98
00:15:48,680 --> 00:15:57,000
yourself of them, having them in your mind allows you to grow into them, learning is

99
00:15:57,000 --> 00:16:03,800
like that, memorizing, memorizing the Buddha's teaching is great, if you're able to distinguish

100
00:16:03,800 --> 00:16:11,880
between what you know and what you only heard, it can be a great protection, a great

101
00:16:11,880 --> 00:16:18,240
refuge, danger is when you confuse them of course, you learn everything and then think

102
00:16:18,240 --> 00:16:29,320
you know everything, then the last way you should learn, if we're able, we should learn

103
00:16:29,320 --> 00:16:39,400
them, we should take care to distinguish what we know and what we don't know, we can

104
00:16:39,400 --> 00:16:48,560
grow into what we don't yet know, the things we've heard that we don't yet know for ourselves.

105
00:16:48,560 --> 00:17:04,440
Number three is Kalyanamita, one should be Kalyanamita, Kalyanamita, one should be a good

106
00:17:04,440 --> 00:17:17,560
friend, one should have good friends, being a good friend is a great refuge, people

107
00:17:17,560 --> 00:17:31,640
appreciate when you're friendly, Kalyanamita is so appraised by the Buddha, friendliness

108
00:17:31,640 --> 00:17:41,200
makes for harmony and peace and a great refuge and we should surround ourselves with

109
00:17:41,200 --> 00:17:49,520
good friends, trying to associate with people who have good qualities, trying to incline

110
00:17:49,520 --> 00:18:03,040
our minds towards cultivating friendship with good people, Kalyanamita means beautiful,

111
00:18:03,040 --> 00:18:10,880
doesn't mean we have to avoid actively avoid people who are unethical and immoral

112
00:18:10,880 --> 00:18:19,760
and harmful, means we have to be able to distinguish and understand what a true friendship

113
00:18:19,760 --> 00:18:38,440
is, true friendship requires kindness, friendliness, requires trust and support, protection,

114
00:18:38,440 --> 00:18:58,640
purpose, a mutual activity of support, care and kindness, thoughtfulness, so a great

115
00:18:58,640 --> 00:19:08,320
refuge to have good friends of course.

116
00:19:08,320 --> 00:19:26,880
Number four, Suwaju, Suwaja, Suwaja means easy in this case, Suwaja is what Suwaja is, being

117
00:19:26,880 --> 00:19:43,040
easy to speak to, easy to admonish is the meaning, it's a great danger to us if we are

118
00:19:43,040 --> 00:19:56,120
intractable and amenable to change, close to any kind of criticism, reactionary to any kind

119
00:19:56,120 --> 00:20:11,080
of judgment or pointing out of our faults, unable to take criticism, so very grave weakness,

120
00:20:11,080 --> 00:20:18,680
great danger to friendship, a great danger to progress, to spiritual development,

121
00:20:18,680 --> 00:20:32,480
obstinency, obstinacy, stubbornness, pride, grave dangers, danger to most if not all people,

122
00:20:32,480 --> 00:20:43,160
well, anyone who is not yet enlightened, stubbornness, intractability, the Buddha talked

123
00:20:43,160 --> 00:20:48,600
about this, and he reminded us if you want help, you have to be easy to help.

124
00:20:48,600 --> 00:20:56,360
Like a patient, if a patient is difficult, won't take their medicine, won't acknowledge

125
00:20:56,360 --> 00:21:03,120
that they're sick, that's a common one, the most dangerous, not acknowledging that you're

126
00:21:03,120 --> 00:21:04,120
sick.

127
00:21:04,120 --> 00:21:10,400
Sometimes we know we're sick, but we don't want to appear sick, we don't let other people

128
00:21:10,400 --> 00:21:15,760
know that we're sick, we don't accept when other people tell us we're sick even though

129
00:21:15,760 --> 00:21:24,000
we know it, because it feels like a vulnerability, a weakness, the grave danger to keep

130
00:21:24,000 --> 00:21:32,120
it hidden, no one will help us, no one will feel comfortable or confident helping us,

131
00:21:32,120 --> 00:21:41,480
feel helpless because we're not letting them help them, help us, and as with the physical

132
00:21:41,480 --> 00:21:56,520
so with the mental, when we don't let other people remind us of our faults, of our mistakes,

133
00:21:56,520 --> 00:22:07,240
point out our mistakes, point out our faults, for not easy to admonish, it's a grave danger.

134
00:22:07,240 --> 00:22:20,000
So it's a great protection for us, a great refuge, if we are amenable to instruction.

135
00:22:20,000 --> 00:22:30,120
A good teacher will not get angry at a student for being stubborn, unruly, hard to teach,

136
00:22:30,120 --> 00:22:39,920
or likewise most teachers will not, a teacher will generally not feel comfortable providing

137
00:22:39,920 --> 00:22:49,800
instruction to someone who is unruly, intractable, to some extent it's pointless, it's

138
00:22:49,800 --> 00:23:04,000
a waste of energy to try and help someone who isn't willing to be helped, isn't open

139
00:23:04,000 --> 00:23:10,160
to being helped, someone who is stubborn and hard to teach, it's a great refuge for us

140
00:23:10,160 --> 00:23:25,000
if we are humble, easy to teach, it's a very important sort of quality that we have

141
00:23:25,000 --> 00:23:34,320
to develop, something we have to keep track of, our pride and stubbornness, we have

142
00:23:34,320 --> 00:23:42,920
to be mindful, we vigilant, you're noticing when it arises and not letting it overwhelm us

143
00:23:42,920 --> 00:23:51,600
and consume us and keep us from receiving instruction, from receiving support, from engaging

144
00:23:51,600 --> 00:24:00,160
in this Anya Manya what Janina, but it said Anya Manya whatcha, Anya Manya means

145
00:24:00,160 --> 00:24:08,600
other and other, it means one another, whatcha, again this whatcha, so whatcha, Anya

146
00:24:08,600 --> 00:24:16,240
Manya whatcha means, speaking to one another doesn't mean, doesn't mean only listening

147
00:24:16,240 --> 00:24:25,920
to our teachers, right, sometimes we feel indignant because someone tries to teach us but

148
00:24:25,920 --> 00:24:38,400
you're not my teacher, we shouldn't be indignant like that, shouldn't be angry when someone

149
00:24:38,400 --> 00:24:46,840
tries to point out our faults, we should consider carefully whether it's true or not

150
00:24:46,840 --> 00:24:55,640
true and if it's true we should thank the person or at least objectively understand that it's

151
00:24:55,640 --> 00:25:02,000
true.

152
00:25:02,000 --> 00:25:22,600
Number five is, well it means helping our fellows with their work.

153
00:25:22,600 --> 00:25:35,720
Being Guernia Nia Nia, Nita Datta and Dakohoti, being keen on supporting others in their

154
00:25:35,720 --> 00:25:44,760
work, so this is, the idea here is in a monastery, monks should take an interest in each

155
00:25:44,760 --> 00:25:54,480
other's work, there's duties and tasks, suppose one monk's Guernia's broken, there's

156
00:25:54,480 --> 00:26:01,120
a leak or something, everyone should come together to help, even in the times when monks

157
00:26:01,120 --> 00:26:14,600
spend their time in silent meditation they would still help each other silently, division

158
00:26:14,600 --> 00:26:22,160
dividing duties in monasteries and so on, so this is for monks but the same of course applies

159
00:26:22,160 --> 00:26:30,480
in any community, in any residence and the general principle is harmony, harmony and mutual

160
00:26:30,480 --> 00:26:43,960
support, this idea of everyone for themselves, that's of course an idea that is antithetical

161
00:26:43,960 --> 00:26:52,240
to any kind of community or harmony and there's a great strength that comes from communal

162
00:26:52,240 --> 00:27:02,840
harmony and it's en sukho san casasamaki, the community that is in harmony, sukho is happiness,

163
00:27:02,840 --> 00:27:17,040
it is happiness, happiness is a harmonious community, it's a great refuge, it's a great refuge

164
00:27:17,040 --> 00:27:23,160
to be supportive of each other, others of course will support you, the community will be

165
00:27:23,160 --> 00:27:34,520
in harmony, the community will grow in flourish, it will attract support and so this of course

166
00:27:34,520 --> 00:27:42,760
is something that is clear for religious institutions but the same goes for families and societies,

167
00:27:42,760 --> 00:27:50,120
our societies will flourish if we help each other, one thing you see in times of trouble

168
00:27:50,120 --> 00:27:59,400
in times of difficulty is the people who come together to help, to support each other,

169
00:27:59,400 --> 00:28:08,440
you see it brings out the best and the worst in people, you can see the best and how

170
00:28:08,440 --> 00:28:17,520
great a refuge that is for society as a whole, for the people on both sides providing

171
00:28:17,520 --> 00:28:30,960
each other with refuge, providing each other with support, number six, dhamma kamo, it's

172
00:28:30,960 --> 00:28:38,120
one of the few times the Buddha uses the word kama in a good way, kama here means love,

173
00:28:38,120 --> 00:28:54,880
dhamma kamo in love with the dhamma, dhamma kamo bhavanghoti, one who loves the dhamma,

174
00:28:54,880 --> 00:29:07,840
one who is in love with the dhamma is a developed person, is a high-minded individual,

175
00:29:07,840 --> 00:29:16,560
motivated person, so obviously the Buddha is not saying we should crave the dhamma or have

176
00:29:16,560 --> 00:29:26,120
any kind of lust or anything, passion, but he acknowledges this attraction, describes

177
00:29:26,120 --> 00:29:32,080
this state of attraction to the dhamma, someone who is intent upon the dhamma, there is

178
00:29:32,080 --> 00:29:43,440
a real feeling of zest zeal because of how pure and profound and perfect the dhamma is

179
00:29:43,440 --> 00:29:51,880
in so many ways, this is a great refuge, it would be a great detriment to us if we looked

180
00:29:51,880 --> 00:30:11,800
at the dhamma with this, with the aversion, if we were a verse or a phrase or displeased

181
00:30:11,800 --> 00:30:19,360
by the dhamma, that would be a great danger to us, and it doesn't have to just mean the

182
00:30:19,360 --> 00:30:25,060
teaching of the Buddha, of course, it's not just because the Buddha taught it, it's a

183
00:30:25,060 --> 00:30:30,680
grave danger for us to look at anything right as though it's wrong, it would be a grave

184
00:30:30,680 --> 00:30:38,880
danger for us to look down upon any kind of practice that is beneficial to have that

185
00:30:38,880 --> 00:30:45,160
sort of wrong view is to be very much on the wrong path, a wrong perspective that, of course,

186
00:30:45,160 --> 00:30:54,040
leads us down other paths, to not see what is important as important, what is unimportant

187
00:30:54,040 --> 00:31:02,000
as unimportant, to not see what is beneficial as beneficial as a grave danger, it is a great

188
00:31:02,000 --> 00:31:08,720
support for us not only to know the dhamma but to be in love with it, in the sense of

189
00:31:08,720 --> 00:31:21,600
appreciating it, having our mind leap at the idea of practicing and learning the dhamma,

190
00:31:21,600 --> 00:31:29,680
when the mind leaps at the idea of doing good deeds, jumps up, ready to act, that's a great

191
00:31:29,680 --> 00:31:35,600
protection, a great refuge for us, of course, because it keeps us on the right path, keeps

192
00:31:35,600 --> 00:31:49,840
us doing good things, it's only our inclination that keeps us away from danger, all bad

193
00:31:49,840 --> 00:32:10,440
inclinations, that's what will lead us to danger, number seven, sun-to-t, sun-to-t means contentment,

194
00:32:10,440 --> 00:32:19,440
contentment is a very great refuge, what's it a refuge from, it's a refuge from greed,

195
00:32:19,440 --> 00:32:28,280
what a danger it is, to be addicted, to need things, and not just things but more and

196
00:32:28,280 --> 00:32:36,840
more and more, whatever you get will not be enough, it could rain gold, and that would

197
00:32:36,840 --> 00:32:46,880
never be enough for one person, we hear about billionaires and how they have so much money,

198
00:32:46,880 --> 00:32:51,120
they don't know what to do with, and then we hear about billionaires becoming trillion

199
00:32:51,120 --> 00:32:59,240
theirs, it will never be enough, that's not how greed works, greed is of a nature to

200
00:32:59,240 --> 00:33:11,840
not ever have enough, there's no appeasement for greed, so being content with nothing is

201
00:33:11,840 --> 00:33:19,960
a very different way of going, it appears powerless in many ways, when you don't have

202
00:33:19,960 --> 00:33:33,600
your powerless, your powerless but you are impervious, person without wealth, without possessions,

203
00:33:33,600 --> 00:33:39,440
is without power in many ways, but they are also without suffering in so many ways, because

204
00:33:39,440 --> 00:33:48,800
they are impervious to loss, contentment doesn't mean not getting or having anything of

205
00:33:48,800 --> 00:34:02,160
course, but it means being content with whatever you get, and on a practice level that's

206
00:34:02,160 --> 00:34:08,120
a very important part of mindfulness, because it no longer applies to possessions and things

207
00:34:08,120 --> 00:34:15,880
it applies to experiences, contentment with whatever experience, not having your happiness

208
00:34:15,880 --> 00:34:25,680
and your peace depend on the vicissitudes of experience, the constant unpredictable nature

209
00:34:25,680 --> 00:34:33,160
of reality, if our happiness depended on that we would always be stressed and suffering,

210
00:34:33,160 --> 00:34:53,280
it is a refuge for us to be content, number seven, number eight, arada vitya, arada vitya,

211
00:34:53,280 --> 00:35:06,560
arada means like strong or unrelenting, effort is a great refuge because laziness is a great

212
00:35:06,560 --> 00:35:22,640
danger, as long as we have this vulnerability, this

213
00:35:22,640 --> 00:35:31,880
correctness, crookedness in the mind, any kind of unwholesumness in our mind, laziness

214
00:35:31,880 --> 00:35:40,000
allows it to breed and fester and grow.

215
00:35:40,000 --> 00:35:48,000
One important characteristic of the path to freedom or the right way to live is that

216
00:35:48,000 --> 00:35:51,280
it involves effort.

217
00:35:51,280 --> 00:35:57,160
That doesn't mean we have to force ourselves to do good things, in fact forcing ourselves

218
00:35:57,160 --> 00:36:04,640
to some extent is lazy, it's much easier to try and force yourself to meditate, for example,

219
00:36:04,640 --> 00:36:10,320
than to actually be mindful, it's a lazy way to be effortful, to just force yourself,

220
00:36:10,320 --> 00:36:17,640
push yourself, effort doesn't mean pushing yourself, it means doing it again and again,

221
00:36:17,640 --> 00:36:28,000
it's quite different from forcing, effort arises in a moment, it has to be coupled with

222
00:36:28,000 --> 00:36:42,640
mindfulness and so the effort to be mindful means the spark at every moment, reminding

223
00:36:42,640 --> 00:36:52,000
yourself, inclining your mind to be mindful, again and again and again that's effort.

224
00:36:52,000 --> 00:37:04,320
Effort for the most part is about being methodical, consistent, not giving up, not backing

225
00:37:04,320 --> 00:37:16,600
off, not slacking off, it's a great refuge to keep our minds in all wholesomeness, requires

226
00:37:16,600 --> 00:37:27,720
effort, repetitive effort, again and again, not letting ourselves become complacent with good

227
00:37:27,720 --> 00:37:36,720
things we've done in the past or what we might do in the future, but repetitive inclination

228
00:37:36,720 --> 00:37:41,920
of the mind to good things.

229
00:37:41,920 --> 00:37:50,440
Number nine is of course sett, sett is the second most important thing, so it's the second

230
00:37:50,440 --> 00:38:02,120
last one, sett is the basis of our practice, sett is the core of our practice, sett

231
00:38:02,120 --> 00:38:09,360
to remember, not things that happened in the past or what we have to do in the future,

232
00:38:09,360 --> 00:38:17,240
but to remember the present, to remember what's happening, to have the mind stay with

233
00:38:17,240 --> 00:38:25,920
the experience without getting caught up in extrapolation or reaction, the mind that

234
00:38:25,920 --> 00:38:35,560
is aware of what's happening right now in the body, the feelings, the mind, the dhamma,

235
00:38:35,560 --> 00:38:42,480
that being in the present moment is the ultimate refuge, when the Buddha boiled us all

236
00:38:42,480 --> 00:38:47,760
down, he ultimately said, when he said, be your own refuge, how do you do that?

237
00:38:47,760 --> 00:38:59,960
He said, practice the force, sett, patana, sett, mindfulness, remembrance, this is the

238
00:38:59,960 --> 00:39:08,360
greatest refuge, said, second most important because the last one is wisdom, so mindfulness

239
00:39:08,360 --> 00:39:21,080
is great because it protects us every moment, but it only protects us at the moment

240
00:39:21,080 --> 00:39:28,400
when we're mindful, if we start being mindful then we lose the protection, it's flawed

241
00:39:28,400 --> 00:39:42,880
that way, it's limited that way, it's not a flaw, it's a limit, satti protects the

242
00:39:42,880 --> 00:39:54,400
mind and that's not just a, it doesn't make it just a temporary solution, it means it's

243
00:39:54,400 --> 00:40:05,000
not the final solution, it's not the ultimate goal, satti is what leans us to the goal

244
00:40:05,000 --> 00:40:11,760
because protecting our mind in this way, protecting our mind from delusion, from the darkness

245
00:40:11,760 --> 00:40:28,120
of ignorance and wrong view and so on, creates clarity in the mind, it allows us to see

246
00:40:28,120 --> 00:40:37,080
clearly the nature of reality, the nature of experience, it allows us to see right from

247
00:40:37,080 --> 00:40:47,720
wrong and good from bad, happiness from suffering, that's the real goal of satti of mindfulness,

248
00:40:47,720 --> 00:40:55,960
yes, it's a great protection, but the real refuge, the ultimate refuge is wisdom, seeing

249
00:40:55,960 --> 00:41:02,720
things clearly and fully and completely as they are, and that's what comes with mindfulness,

250
00:41:02,720 --> 00:41:09,160
the protection that we give to our mind at every moment, brings us to the ultimate

251
00:41:09,160 --> 00:41:18,240
protection that doesn't fade away.

252
00:41:18,240 --> 00:41:30,480
When you see things as they are, banyaya parisudhjati, through wisdom, one is purified, purity

253
00:41:30,480 --> 00:41:50,040
and the freedom from any kind of stain or fault, all it takes is wisdom to be pure, and

254
00:41:50,040 --> 00:41:54,600
see that there's nothing worth clinging to, that everything that arises inside of us and

255
00:41:54,600 --> 00:42:03,320
the world around us is impermanent on satisfying and uncontrollable, not worth clinging to.

256
00:42:03,320 --> 00:42:13,600
Just that wisdom, just that, freezes from all kinds of danger, all of the dangers of clinging,

257
00:42:13,600 --> 00:42:20,360
all of the dangers of misapprehending things as stable, satisfying and controllable, all

258
00:42:20,360 --> 00:42:28,840
the problems that come from misunderstanding reality and clinging to reality as though it might

259
00:42:28,840 --> 00:42:41,400
provide us with stability, satisfaction or control, all that we do away with, just by seeing

260
00:42:41,400 --> 00:42:47,080
clearly wisdom is the greatest refuge, all ten of these provide a really good framework

261
00:42:47,080 --> 00:42:57,000
for Buddhist practice and Buddhist theory.

262
00:42:57,000 --> 00:43:06,040
So that's the dhamma for this morning, a reminder, a refresher, something new for those

263
00:43:06,040 --> 00:43:15,720
who haven't heard it before, the importance of making a refuge for yourself and the means

264
00:43:15,720 --> 00:43:19,640
by which to do that, thank you all for listening.

