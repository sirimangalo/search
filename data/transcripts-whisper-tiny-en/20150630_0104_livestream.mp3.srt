1
00:00:00,000 --> 00:00:22,600
Good evening broadcasting live from Stony Creek, Terrio, Canada, coming to give them a little

2
00:00:22,600 --> 00:00:27,600
bit of dumber, trying to give them a little bit of dumber, I think.

3
00:00:27,600 --> 00:00:35,400
There's a lot to give on all the dumber teaching in the monday's.

4
00:00:35,400 --> 00:00:44,000
Ananda said Ananda was the Buddha's attendant.

5
00:00:44,000 --> 00:00:54,800
Cindy had 84,000 teachings of the Buddha, kept in memory, that's what the texts say.

6
00:00:54,800 --> 00:01:05,800
We have all the texts, whether there's 84,000 or not, there's a lot.

7
00:01:05,800 --> 00:01:13,000
Sometimes this is daunting, this makes us question us to whether we can truly know the

8
00:01:13,000 --> 00:01:14,800
Buddha's teaching.

9
00:01:14,800 --> 00:01:27,600
And take people always try to, to me always try to simplify or pinpoint the essence of

10
00:01:27,600 --> 00:01:31,000
the Buddha's teaching.

11
00:01:31,000 --> 00:01:39,200
Sometimes we'll bring up the eightfold noble path, sort of essential, the heart of

12
00:01:39,200 --> 00:01:49,200
us and that's eight things and that's something we can deal with.

13
00:01:49,200 --> 00:01:54,000
Actually, often we'll talk about the four noble truths, thinking that that's easier,

14
00:01:54,000 --> 00:01:57,400
but there's only four of those, right?

15
00:01:57,400 --> 00:02:03,600
The problem with that is the four noble truths don't really work as far as pinpointing

16
00:02:03,600 --> 00:02:09,600
the Buddha's teaching, because there's a lot.

17
00:02:09,600 --> 00:02:15,100
If you want to sum it up, you've got the four noble truths, but then while the fourth

18
00:02:15,100 --> 00:02:20,000
one is the eightfold noble paths and then you've got eight more there and to really explain

19
00:02:20,000 --> 00:02:25,700
the four noble truths, you have to separate each truth into three parts, there's the

20
00:02:25,700 --> 00:02:35,600
truth itself, there's the duty or the task that must be accomplished in regards to that

21
00:02:35,600 --> 00:02:36,600
truth.

22
00:02:36,600 --> 00:02:48,900
And then there's the attainment or the realization or the accomplishment of the task.

23
00:02:48,900 --> 00:02:53,400
And two, anyway, there's the truth itself and then there's the task, which are two different

24
00:02:53,400 --> 00:02:54,400
things, actually.

25
00:02:54,400 --> 00:02:58,400
You can't just say the first noble truth is suffering.

26
00:02:58,400 --> 00:03:05,100
You have to say the first noble truth is, first of all, that there is suffering, second

27
00:03:05,100 --> 00:03:14,200
of all that suffering should be fully understood and third that suffering has been fully

28
00:03:14,200 --> 00:03:15,200
understood.

29
00:03:15,200 --> 00:03:20,000
Because the only way you can understand the first noble truth is by fully realizing it for

30
00:03:20,000 --> 00:03:26,600
you, so fully understanding, so you're a Buddha made this clear, not saying, you can't

31
00:03:26,600 --> 00:03:37,400
say you understand the truth just by knowing that this is suffering, your ex is suffering.

32
00:03:37,400 --> 00:03:43,800
You have to also understand that it should be, should be fully understood, whereas

33
00:03:43,800 --> 00:03:48,600
you task in regards to suffering to fully understand it.

34
00:03:48,600 --> 00:03:55,400
And even then you can't say you fully understand the truth or suffering, only once you

35
00:03:55,400 --> 00:04:00,000
fully understood suffering, then you can say you understand, you know what is the first

36
00:04:00,000 --> 00:04:05,300
of the truth.

37
00:04:05,300 --> 00:04:10,800
So and then the second noble truth and the third and the fourth, each one has a task.

38
00:04:10,800 --> 00:04:15,800
So we want to go a little bit more to the point, we can just talk about the hateful

39
00:04:15,800 --> 00:04:22,800
noble path because practicing the full noble path allows you to understand suffering.

40
00:04:22,800 --> 00:04:31,600
And so we're down to one thing with eight parts and so eight, well, that's still a lot

41
00:04:31,600 --> 00:04:42,900
in each of the eight has to be explained in detail before you can understand it.

42
00:04:42,900 --> 00:04:54,600
We can simplify it and we can do so with authority, because we have confidence because

43
00:04:54,600 --> 00:05:00,800
we have the authority of the Buddha, that actually the eight full noble path is only

44
00:05:00,800 --> 00:05:05,800
three things, morality, concentration, and wisdom.

45
00:05:05,800 --> 00:05:13,600
So we have the three trainings, right speech, right action and right livelihood, these

46
00:05:13,600 --> 00:05:22,100
are morality, right effort, right mindfulness and right concentration, these are under

47
00:05:22,100 --> 00:05:30,400
some idea of concentration and right view and right thought, those are under wisdom.

48
00:05:30,400 --> 00:05:38,600
So morality, concentration, and wisdom is a summary of the full noble path and down

49
00:05:38,600 --> 00:05:40,900
to just three.

50
00:05:40,900 --> 00:05:46,400
And when we get down to just three, then we can take out the salient point of days three

51
00:05:46,400 --> 00:06:03,400
and that's the training, what we've got, seeker, the three seekers, the person who trains

52
00:06:03,400 --> 00:06:13,600
and these three things is called a seeker, someone who has become accomplished in them,

53
00:06:13,600 --> 00:06:23,600
someone who learns to be more, seekers becomes capable to become capable of something.

54
00:06:23,600 --> 00:06:32,600
It's similar to the word that means to be capable of sake, sake, sake, sake, sake.

55
00:06:32,600 --> 00:06:36,600
There may have something in common.

56
00:06:36,600 --> 00:06:44,600
Seek at the means, seek at the means to become competent, to train yourself, to learn

57
00:06:44,600 --> 00:06:55,600
a skill, to learn to be moral, to learn to be focused, and to learn to be wise.

58
00:06:55,600 --> 00:07:04,600
So we talk about morality, concentration, concentration, and wisdom, right?

59
00:07:04,600 --> 00:07:11,600
We've still got a lot better in morality than our four types of morality, morality of keeping

60
00:07:11,600 --> 00:07:22,600
precepts, morality of living a life of a utilitarian life in the sense of using things

61
00:07:22,600 --> 00:07:25,600
but not letting them own you.

62
00:07:25,600 --> 00:07:41,600
So being moral in your consumption, not living an excessive life with fancy clothes and

63
00:07:41,600 --> 00:07:51,600
high-end food and dwelling and medicines, to only use them as our nested necessary.

64
00:07:51,600 --> 00:07:58,600
We use our requisite few things for a purpose that is truly useful.

65
00:07:58,600 --> 00:08:06,600
We have to be moral in regards to our livelihood, and life morally, and moral in regards

66
00:08:06,600 --> 00:08:07,600
to our senses.

67
00:08:07,600 --> 00:08:12,600
So seeing, hearing, smelling, tasting, feeling, and thinking, not getting carried away

68
00:08:12,600 --> 00:08:21,600
isn't pleasant, or unpleasant, sight sounds, smells, tastes, feelings, and thoughts.

69
00:08:21,600 --> 00:08:32,600
Not giving free reign to our senses, so guarding, not staring, or seeking out pleasant

70
00:08:32,600 --> 00:08:36,600
sights, or sound, or so on.

71
00:08:36,600 --> 00:08:43,600
So morality, there's quite a bit in there.

72
00:08:43,600 --> 00:08:48,600
Concentration, there's a similar term, and it's amateur concentration, and we pass in a concentration.

73
00:08:48,600 --> 00:08:57,600
Can you cast a mountain, or pajara, a mountain, an arpanus, a mountain?

74
00:08:57,600 --> 00:09:11,600
Some ideas come, some mountains are same, it's a continent, and the English word same, but in the sense of being balanced.

75
00:09:11,600 --> 00:09:22,600
Some ideas come from some, some, or what exactly it comes from, but some mountains level.

76
00:09:22,600 --> 00:09:36,600
Or having a balanced mind, a mind that is tuned, and tuned, and tuned perfect, not too much, not too little.

77
00:09:36,600 --> 00:09:46,600
So we can be in tune with, we can be focused, have our mind, our lens, and focus on a single object, a conceptual object,

78
00:09:46,600 --> 00:10:01,600
that we give rise to in our minds, or that we stare at with our eyes, or even become absorbed in the new object.

79
00:10:01,600 --> 00:10:05,600
We're perfectly focused on the object, seeing it perfectly clearly.

80
00:10:05,600 --> 00:10:27,600
Or we can focus our minds on reality, and become focused on perfect focus, on reality, seeing things as they are, or experiences, focusing on them.

81
00:10:27,600 --> 00:10:47,600
And then wisdom, and training, and learning, and wisdom from learning, and wisdom from thinking, considering, and be trained in wisdom from understanding, from meditation, and training.

82
00:10:47,600 --> 00:11:01,600
From seeing, developing our mind, from practicing meditation.

83
00:11:01,600 --> 00:11:25,600
But we can simplify this down, as I said, to simply training, and become trained, when we train ourselves, and learn about these things, and then train ourselves to live a straight life, to have a mind that is straight and clear.

84
00:11:25,600 --> 00:11:41,600
This is why the Buddha, you can see why mindfulness was singled out as, sort of, that kickstart, sort of, practice, or keeps us on the practice.

85
00:11:41,600 --> 00:11:48,600
Singled out as the quality that the Buddha said is useful everywhere. It's always useful.

86
00:11:48,600 --> 00:12:10,600
Because it's the act of training. Training in an interest, in a very little sense of the word, to set your mind on the track, on a train track, to train your mind, to get it to follow a track.

87
00:12:10,600 --> 00:12:25,600
We think of morality, concentration, wisdom, and get on the track up. It's like a train, so we never go off, we never go outside of the training.

88
00:12:25,600 --> 00:12:33,600
We use mindfulness to train ourselves, and we slowly become more moral, we become more focused, we become more wise.

89
00:12:33,600 --> 00:12:49,600
We become more clear, we practice the simple teachings of mindfulness, reminding ourselves, this is seeing, this is here, this is standing, this is sitting, this is walking, this is liking, this is this thing.

90
00:12:49,600 --> 00:12:57,600
See things as they are, and train ourselves in this, we don't actually have to think about things like morality, for example.

91
00:12:57,600 --> 00:13:06,600
When you're mindful, you're not sure the gain morality, you don't steal, you don't lie, you don't cheat, your faculties are controlled.

92
00:13:06,600 --> 00:13:23,600
All it takes is for us to be mindful. And the great thing about mindfulness is it's something you practice now, you don't have to have practiced it in the past, it's something you start ever and again, and then we moment.

93
00:13:23,600 --> 00:13:35,600
So this is what we do in the meditation practice for training ourselves, training in morality comes in training in wisdom, it comes simply from being mindful.

94
00:13:35,600 --> 00:13:48,600
And as a result, we cultivate the full noble path, first the preliminary path, eventually train our minds so perfectly that we become noble in the end, turn to the noble path.

95
00:13:48,600 --> 00:14:09,600
And with the noble path comes the noble fruit, with the noble fruit, comes enlightenment, we understand that we are free, we understand that we are free, there's nothing left to be done, we found the goal.

96
00:14:09,600 --> 00:14:19,600
So that's the number for tonight, thank you all for tuning in.

