1
00:00:00,000 --> 00:00:02,000
Okay, again.

2
00:00:02,000 --> 00:00:05,000
Can you explain the inner workings of meditation?

3
00:00:05,000 --> 00:00:09,000
Like what's going on behind the scenes when we are noting our experiences

4
00:00:09,000 --> 00:00:14,000
and repeating mantras or resting online within an experience?

5
00:00:19,000 --> 00:00:26,000
Well, we can answer from a point of view of the theory of the Abidama,

6
00:00:26,000 --> 00:00:30,000
but I don't think we have to go quite so far.

7
00:00:30,000 --> 00:00:37,000
It's quite simply put, there's a moment in the mind

8
00:00:37,000 --> 00:00:41,000
where you decide about an object.

9
00:00:41,000 --> 00:00:45,000
Whether it's seeing, hearing, smelling, tasting, feeling, or thinking,

10
00:00:45,000 --> 00:00:47,000
these are the six doors.

11
00:00:47,000 --> 00:00:51,000
So there's the process of experiencing

12
00:00:51,000 --> 00:00:56,000
that first there's the object.

13
00:00:56,000 --> 00:01:00,000
Then the mind wakes up to the fact that there's an object.

14
00:01:00,000 --> 00:01:04,000
This all happens very quickly, but wakes up and it approaches the object

15
00:01:04,000 --> 00:01:06,000
and then it experiences the object.

16
00:01:06,000 --> 00:01:09,000
And then it decides about the object.

17
00:01:09,000 --> 00:01:15,000
Well, there's another part of recognizing it, associating it with past experiences.

18
00:01:15,000 --> 00:01:23,000
But then there's that moment of decision where the mind makes a decision about the object.

19
00:01:23,000 --> 00:01:27,000
Is it good? Is it bad? Is it me? Is it mine? All of these things?

20
00:01:27,000 --> 00:01:31,000
And maybe not just one moment, but there is.

21
00:01:31,000 --> 00:01:36,000
There is said to be that one moment per experience.

22
00:01:36,000 --> 00:01:40,000
And it can have all sorts of factors in it.

23
00:01:40,000 --> 00:01:51,000
But an ordinary state will usually have likes and dislikes.

24
00:01:51,000 --> 00:01:59,000
And we'll have identification as me and mine and so on.

25
00:01:59,000 --> 00:02:04,000
So quite simply put, the noting is a replacement for that.

26
00:02:04,000 --> 00:02:10,000
It's a decision that this is this.

27
00:02:10,000 --> 00:02:19,000
Ordinary reaction is, this is good, this is bad, this is me, this is mine.

28
00:02:19,000 --> 00:02:21,000
Never just this is this.

29
00:02:21,000 --> 00:02:28,000
So that reminding is really the best word possible to describe what we do.

30
00:02:28,000 --> 00:02:33,000
And I'm using it more and more these days instead of mindfulness or so on.

31
00:02:33,000 --> 00:02:40,000
The noting, acknowledging all those words don't really encapsulate what we're doing or doing is reminding ourselves.

32
00:02:40,000 --> 00:02:48,000
Reminding, so you bring the mind back to that original experience.

33
00:02:48,000 --> 00:02:50,000
This is this.

34
00:02:50,000 --> 00:02:57,000
Which can be described by the words objective, that is objective.

35
00:02:57,000 --> 00:03:01,000
It's objectivity, it's the definition of objectivity.

36
00:03:01,000 --> 00:03:07,000
And maybe not the definition, but it's exactly objective.

37
00:03:07,000 --> 00:03:10,000
And it's also the truth.

38
00:03:10,000 --> 00:03:16,000
It's the definition of observation, you see, when you do a science experiment,

39
00:03:16,000 --> 00:03:19,000
way back in high school and I did those kind of things.

40
00:03:19,000 --> 00:03:25,000
And all the observations, that's the key to a good experiment is accurate observations.

41
00:03:25,000 --> 00:03:28,000
So this is an accurate observation.

42
00:03:28,000 --> 00:03:32,000
It was really confusing to me when I first learned about Buddhism.

43
00:03:32,000 --> 00:03:35,000
When you walk, just walk.

44
00:03:35,000 --> 00:03:37,000
When you sit, just sit.

45
00:03:37,000 --> 00:03:44,000
So I was reading these vague concepts, not exactly, not exactly the way we do it.

46
00:03:44,000 --> 00:03:48,000
But it was this general concept of when you walk, just walk.

47
00:03:48,000 --> 00:03:52,000
That's not quite how I explained it now, but it's along the same ideas.

48
00:03:52,000 --> 00:03:55,000
And I was getting these ideas of what the heck does that mean?

49
00:03:55,000 --> 00:03:57,000
What good is it?

50
00:03:57,000 --> 00:04:02,000
You know, what does it accomplish and so on?

51
00:04:02,000 --> 00:04:05,000
It's the key to understanding.

52
00:04:05,000 --> 00:04:09,000
So the next thing that happens, it occurs in that case,

53
00:04:09,000 --> 00:04:13,000
is you start to see the experience as it is.

54
00:04:13,000 --> 00:04:17,000
Instead of with a deluded mind,

55
00:04:17,000 --> 00:04:20,000
you see, you start to realize that our ordinary reactions,

56
00:04:20,000 --> 00:04:29,000
our ordinary understanding of our experiences is our fraught with delusions

57
00:04:29,000 --> 00:04:37,000
and sort of a vagueness or a lack of clarity

58
00:04:37,000 --> 00:04:41,000
that allows for all sorts, a breeding ground of all sorts of developments.

59
00:04:41,000 --> 00:04:44,000
My teacher would always describe it as darkness.

60
00:04:44,000 --> 00:04:51,000
So you would often refer to mindfulness as a light that you shine in.

61
00:04:51,000 --> 00:04:54,000
In dark, it's a good analogy because in darkness,

62
00:04:54,000 --> 00:04:56,000
all sorts of bad things grow.

63
00:04:56,000 --> 00:05:00,000
In darkness, fungus grows, bacteria grows.

64
00:05:00,000 --> 00:05:05,000
If there were no sun, in places where there were no sun,

65
00:05:05,000 --> 00:05:13,000
like in dark caves and so on, that's where all of the sort of grow stuff grows.

66
00:05:13,000 --> 00:05:18,000
But as soon as the sun comes out, if you shine sun, sunlight,

67
00:05:18,000 --> 00:05:25,000
this can't grow, it can't fester.

68
00:05:25,000 --> 00:05:36,000
And so that's a non-technical or sort of partially quasi-technical explanation of what happens.

69
00:05:36,000 --> 00:05:42,000
I don't know if you want me to, if it's necessary to go any more technical than that.

70
00:05:42,000 --> 00:05:46,000
There's not much further you can go.

71
00:05:46,000 --> 00:05:59,000
There is some sense that you can have a system of sort of describing the building blocks.

72
00:05:59,000 --> 00:06:05,000
So in one sense, this question is asking for an atomic framework

73
00:06:05,000 --> 00:06:12,000
or some atomic depending, you know, as you would.

74
00:06:12,000 --> 00:06:15,000
And so the Abu Damo tries to provide that.

75
00:06:15,000 --> 00:06:20,000
It's quite interesting to find this atomic framework for the mind.

76
00:06:20,000 --> 00:06:24,000
And so you can actually study that and see exactly what's going on with it.

77
00:06:24,000 --> 00:06:27,000
But I think that's quite clear, it's quite simple.

78
00:06:27,000 --> 00:06:33,000
And as a result, it's quite easy to dispel a lot of these doubts that I used to have

79
00:06:33,000 --> 00:06:38,000
and a lot of people I might used to have about this type of practice.

80
00:06:38,000 --> 00:06:41,000
You know, what the heck does that do?

81
00:06:41,000 --> 00:06:45,000
This mantra of mindless brainwashing, you know, what is that?

82
00:06:45,000 --> 00:06:50,000
When in fact, it's quite simple and quite clear and quite obviously useful.

83
00:06:50,000 --> 00:06:59,000
I mean, maybe not obviously useful, but with a little bit of, you know, being honest with yourself.

84
00:06:59,000 --> 00:07:03,000
Another way of describing it is it's a changing of our habits.

85
00:07:03,000 --> 00:07:10,000
So over time, this changes the way we look at things in general.

86
00:07:10,000 --> 00:07:18,000
So our habit normally is reacting, responding, and you know, in exact, you know, cutting corners in a sense.

87
00:07:18,000 --> 00:07:26,000
Rather than really examining, we fall into a habit based on what works.

88
00:07:26,000 --> 00:07:31,000
So it's worked for us so far and we're able to get along.

89
00:07:31,000 --> 00:07:34,000
And that's how the mind works.

90
00:07:34,000 --> 00:07:43,000
It's fine with imprecision and imperfection, because it works.

91
00:07:43,000 --> 00:07:48,000
It fixed the problem to some extent.

92
00:07:48,000 --> 00:07:54,000
And so we get used to this habit of dealing with things in a way that works.

93
00:07:54,000 --> 00:07:59,000
Meditation is, you know, this process of seeing things,

94
00:07:59,000 --> 00:08:04,000
where it will show you the imperfections in our approach to experience.

95
00:08:04,000 --> 00:08:07,000
Sometimes they're quite glaring and we realize,

96
00:08:07,000 --> 00:08:09,000
how did I miss that?

97
00:08:09,000 --> 00:08:11,000
We realized that we're really doing things wrong.

98
00:08:11,000 --> 00:08:22,000
For the most part, a beginner meditator will see some very fairly glaring errors of analysis or processing of experience.

99
00:08:22,000 --> 00:08:24,000
And so those will change.

100
00:08:24,000 --> 00:08:33,000
You know, you naturally change your habits and you'll start to react differently or respond differently.

101
00:08:33,000 --> 00:08:35,000
And so your habits will change.

102
00:08:35,000 --> 00:08:43,000
You'll start to have the habit of seeing things as they are, not reacting, being objective, being impartial, being equanimous,

103
00:08:43,000 --> 00:08:51,000
being clear, being balanced, being pure, I mean there's so much incredible benefits to the practice.

104
00:08:51,000 --> 00:08:56,000
There's no question how wonderful it is.

105
00:08:56,000 --> 00:08:59,000
So that's what's happening, I think.

106
00:08:59,000 --> 00:09:08,000
I mean, I think that's an explanation of adequate explanation of what's happening.

107
00:09:08,000 --> 00:09:24,000
Thanks, good question.

