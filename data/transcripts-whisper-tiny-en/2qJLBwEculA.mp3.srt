1
00:00:00,000 --> 00:00:13,760
Okay, good evening, everyone, welcome to our evening session.

2
00:00:13,760 --> 00:00:26,800
It was invited next month to give a talk in a couple of talks, and one of them was asked

3
00:00:26,800 --> 00:00:33,000
that I give a talk on wrong mindfulness.

4
00:00:33,000 --> 00:00:46,560
And there actually is a term, right, there's right mindfulness, and there's wrong mindfulness.

5
00:00:46,560 --> 00:00:51,080
Something in me wants to say that, well, mindfulness, as we know from the Abhidhamma

6
00:00:51,080 --> 00:00:57,360
is mindfulness is always awesome.

7
00:00:57,360 --> 00:01:08,920
In the, in the sutta the Buddha said, satin chakwahang bhikkavay, sambhitik sambhati kang wadami,

8
00:01:08,920 --> 00:01:16,440
mindfulness I tell you, monks is always useful, it's always good, but then he talks about

9
00:01:16,440 --> 00:01:28,720
something called mikja sati, in the context of the Eightfold Path, I think it's a little

10
00:01:28,720 --> 00:01:35,800
misleading for us to say wrong mindfulness, I mean the technically mindfulness is always

11
00:01:35,800 --> 00:01:44,680
right, it's always good, but from a conceptual perspective, let me talk about mindfulness

12
00:01:44,680 --> 00:01:52,480
practice, we talk about being attentive and the English word mindfulness, which is a rather

13
00:01:52,480 --> 00:02:02,720
general and broad term, it is possible to understand it as a good and good or bad, sometimes

14
00:02:02,720 --> 00:02:05,760
good, sometimes bad.

15
00:02:05,760 --> 00:02:13,120
So I thought I'd talk a little bit about ways that the practice can go wrong, not specifically

16
00:02:13,120 --> 00:02:20,440
talking about the state of mind that is wrong, but when you're practicing meditation,

17
00:02:20,440 --> 00:02:27,000
right, what can go wrong, this is quite interesting and I think this was really what the,

18
00:02:27,000 --> 00:02:33,600
they were trying to get out with this, this invitation, I mean to talk about how the practice

19
00:02:33,600 --> 00:02:42,560
can go wrong, when you're practicing meditation, what can go wrong?

20
00:02:42,560 --> 00:02:51,920
How do you know you're practicing well, sometimes there's this doubt, am I doing it right?

21
00:02:51,920 --> 00:03:05,480
So the first way that the practice can go wrong, obviously, is you don't practice, of course

22
00:03:05,480 --> 00:03:12,520
that seems quite obvious and kind of irrelevant, we're talking about practice, what

23
00:03:12,520 --> 00:03:15,360
does it mean to not practice?

24
00:03:15,360 --> 00:03:21,040
Almost obviously it means those people who don't come, those people who never thought

25
00:03:21,040 --> 00:03:30,080
of practicing meditation, well that's not going to get you anywhere, but then there

26
00:03:30,080 --> 00:03:38,040
are those people who have heard of meditation, those people who have been even taught meditation,

27
00:03:38,040 --> 00:03:44,240
and are not interested in it, it's also quite common, sometimes you'll teach someone

28
00:03:44,240 --> 00:03:54,240
and it just goes in one ear and out the other, over their head, I'm not able to or

29
00:03:54,240 --> 00:04:05,840
not interested in understanding it, but more importantly, so for all of us, congratulations

30
00:04:05,840 --> 00:04:13,120
here, you've passed that one, but what you might find instead is that sometimes even though

31
00:04:13,120 --> 00:04:21,240
you intend to practice, you're actually not practicing, so walking and sitting on mindfully,

32
00:04:21,240 --> 00:04:26,320
we do a lot of walking and a lot of sitting and I'm sure sometimes you'll notice that

33
00:04:26,320 --> 00:04:34,080
you weren't being mindful, even though your intention was to be mindful and you were

34
00:04:34,080 --> 00:04:47,440
trying your best to be mindful and find yourself getting distracted or slipping, slipping

35
00:04:47,440 --> 00:04:55,240
back into old habits, so unmindfulness is of course the first way that the practice can

36
00:04:55,240 --> 00:05:00,440
go wrong and what's the result of that, the result of that is generally just a lot of stress

37
00:05:00,440 --> 00:05:06,120
and suffering, of course if you don't ever practice mindfulness at all, your life is going

38
00:05:06,120 --> 00:05:13,000
to be full of stress and suffering as most people are potentially, of course, it is possible

39
00:05:13,000 --> 00:05:20,800
that things will be good for a while, but it's quite uncertain because you're very easily

40
00:05:20,800 --> 00:05:29,400
sidetracked and get caught up in unhostiveness, as a meditator, meditating on mindfully,

41
00:05:29,400 --> 00:05:37,600
this is where it becomes stressful, your movements are unmindful and so walking back

42
00:05:37,600 --> 00:05:43,800
and forth and sitting like anything else can cause a lot of pain and aches and so on

43
00:05:43,800 --> 00:05:53,400
because of the tension in your body, the stress in the mind, make you tired, make you unhappy

44
00:05:53,400 --> 00:06:11,920
bored, easy to get bored if you're not being mindful right, the next way the practice

45
00:06:11,920 --> 00:06:20,240
can go wrong is if you're being mindful of the wrong thing and this of course comes in many

46
00:06:20,240 --> 00:06:32,840
ways, many, in many forms, so you could be mindful of the past or the future, if you

47
00:06:32,840 --> 00:06:39,640
get lost thinking about the past, sometimes in meditation it is possible that you, or it does

48
00:06:39,640 --> 00:06:44,280
happen, that you get caught up in the past or the future thinking about things that

49
00:06:44,280 --> 00:06:51,520
happen or planning about things that you're going to do right, it's quite common for meditators

50
00:06:51,520 --> 00:07:00,800
to get lost in planning or reminiscing and that is considered a sort of a mindfulness,

51
00:07:00,800 --> 00:07:06,720
in the poly, I mean the word mindfulness is actually imprecise but it's satty in the sense

52
00:07:06,720 --> 00:07:13,760
that your mind is focused on something that you're recollecting something, that your

53
00:07:13,760 --> 00:07:22,000
mind is focused on something, just focused on something that's not really useful, the past

54
00:07:22,000 --> 00:07:27,880
in the future problematic because of the effort it takes to be aware of them but more

55
00:07:27,880 --> 00:07:33,640
importantly because they're not real right, the past isn't here, doesn't actually exist

56
00:07:33,640 --> 00:07:42,760
except in your mind and so the result of living in concepts, in memories or planning

57
00:07:42,760 --> 00:07:55,280
about the future, the result of living in these make believe realms is stress and delusion

58
00:07:55,280 --> 00:08:02,160
because it's very easy to get lost, for example the past, you get caught up in emotions

59
00:08:02,160 --> 00:08:15,640
about the past or the future you get caught up in planning and get lost in potentiality

60
00:08:15,640 --> 00:08:26,120
as that may never come to pass, so there's a lot of illusion, it's not real.

61
00:08:26,120 --> 00:08:35,800
More common though in or equally common anyway in meditation is focused on something

62
00:08:35,800 --> 00:08:44,840
that's not past or future but still focused on an object that is not present, this means

63
00:08:44,840 --> 00:08:50,880
a concept so meditators will often talk about how they're daydreaming, that's a simple

64
00:08:50,880 --> 00:09:00,080
example of how they start to imagine themselves here or there in some situation but

65
00:09:00,080 --> 00:09:11,240
they're actually meditations that involve focusing on the concept and so this is a common

66
00:09:11,240 --> 00:09:28,520
misunderstanding or understanding, it's a common practice that is distinct from seeing things

67
00:09:28,520 --> 00:09:36,800
as they are being present, if you focus on people for example, wishing they all beings

68
00:09:36,800 --> 00:09:45,360
be happy, may I be happy, may this person be happy or so, if you focus on the parts of

69
00:09:45,360 --> 00:09:53,600
the body, if you focus on a candle flame actually had a, when I was teaching recently

70
00:09:53,600 --> 00:10:00,840
at a high semi high school and one of the teachers there he actually said that he just

71
00:10:00,840 --> 00:10:15,160
likes to look at a candle flame and that helps common down and to give him focus.

72
00:10:15,160 --> 00:10:23,000
So a concept is a common object of meditation, we have an idea or an image or a thing

73
00:10:23,000 --> 00:10:30,800
in our mind that we conceive of and we take that as our object of meditation.

74
00:10:30,800 --> 00:10:35,480
Which is actually fine, I mean it's a valid meditation practice in itself, there's

75
00:10:35,480 --> 00:10:40,280
nothing bad about it, it's quite good.

76
00:10:40,280 --> 00:10:48,480
But the thing about concepts is that they are distinct from reality, they are pleasant

77
00:10:48,480 --> 00:11:00,080
or satisfying, they're controllable, they're predictable.

78
00:11:00,080 --> 00:11:06,520
When you live in a world of concepts, you can make them any way you wish and in meditation

79
00:11:06,520 --> 00:11:14,240
this happens, you get into states that are quite calm and you're able to manipulate your

80
00:11:14,240 --> 00:11:24,840
awareness, so make large, expansive awareness, infinite consciousness and that kind of thing.

81
00:11:24,840 --> 00:11:29,320
Or in a more mundane sense you're able to imagine anything, imagination is great,

82
00:11:29,320 --> 00:11:37,680
it's imagination by its nature, it's preferable to reality in every way, imagination can

83
00:11:37,680 --> 00:11:43,680
be whatever you want, this is why books and television are so popular because it's a means

84
00:11:43,680 --> 00:11:53,480
of manipulating reality, controlling our awareness.

85
00:11:53,480 --> 00:12:03,400
So imagination is better than reality in every way but one, one important distinction

86
00:12:03,400 --> 00:12:14,160
is that it's not real, you can't live in imagination, imagination can't be a refuge.

87
00:12:14,160 --> 00:12:20,520
It's like virtual reality, virtual reality might be great, but in the end you have to turn

88
00:12:20,520 --> 00:12:28,840
it off and go eat or you have to turn it off and you depend on reality is the point.

89
00:12:28,840 --> 00:12:37,320
And reality has some important differences, it's distinct and important ways, distinct

90
00:12:37,320 --> 00:12:50,360
from imagination, it's not like that, so that's important to be clear about to be aware

91
00:12:50,360 --> 00:12:58,640
of that we're trying to be conscious of reality and so why we have you focus on your

92
00:12:58,640 --> 00:13:06,600
feet moving, why we're not doing meditation on a concept or something, why we're doing

93
00:13:06,600 --> 00:13:14,040
actually very mundane exercises because we want to learn about reality, it's much more

94
00:13:14,040 --> 00:13:22,480
interesting to us than anything else, the mundane or what we call the mundane is the real

95
00:13:22,480 --> 00:13:26,320
and we want to learn about the real.

96
00:13:26,320 --> 00:13:36,720
So next way that the meditation can go wrong or mindfulness can go wrong, the problems

97
00:13:36,720 --> 00:13:46,640
or the effect of focusing on concepts, see the most dangerous thing about concepts is

98
00:13:46,640 --> 00:13:52,880
that they're infinite and this really is a danger, this one is where meditation goes wrong

99
00:13:52,880 --> 00:14:00,560
because if you don't have a teacher practicing meditation based on a concept or just

100
00:14:00,560 --> 00:14:09,920
imagining and letting your imagination go, it can lead to psychotic episodes or whatever

101
00:14:09,920 --> 00:14:19,480
you call them, states of temporary insanity, if you see something and it's an image of

102
00:14:19,480 --> 00:14:22,920
something and you just go with it and you see where it leads and I've had meditators

103
00:14:22,920 --> 00:14:32,280
do that and without proper advice the meditator will manipulate and expand and get caught

104
00:14:32,280 --> 00:14:42,920
up in some feedback loop somewhere and then fear seeps in or anxieties or anger or addiction

105
00:14:42,920 --> 00:14:52,880
and it feeds and it feeds and it becomes dangerous to the mind, I mean it's not incredibly

106
00:14:52,880 --> 00:14:57,960
dangerous, there are people who kill themselves because they get to such a wound up state

107
00:14:57,960 --> 00:15:02,800
but I think that's rather rare, mostly it just drives you crazy for a while and then you

108
00:15:02,800 --> 00:15:07,680
stop meditating and get really afraid of meditation.

109
00:15:07,680 --> 00:15:13,480
So the realm of concept, it does that because it's infinite, you can go anywhere, you

110
00:15:13,480 --> 00:15:25,360
can easily easily get caught up in who knows what, any number of unlimited number of dangerous

111
00:15:25,360 --> 00:15:33,040
feedback loops, now that you don't have that problem with reality, reality is not going

112
00:15:33,040 --> 00:15:39,560
to get you caught up in anything, reality is just going to be stressful, it's going to

113
00:15:39,560 --> 00:15:47,320
be uncomfortable, but at best it's going to help you become invincible, at best it's

114
00:15:47,320 --> 00:15:53,400
going to teach you, and it's going to teach you to see things clearly, it's going to teach

115
00:15:53,400 --> 00:16:04,640
you enough so that you're no longer react to experience because all of our imagination,

116
00:16:04,640 --> 00:16:11,440
all of the concepts, people, places, and things that we live with, they're all based

117
00:16:11,440 --> 00:16:18,280
on experience, on reality, they all have reality as they're root, you can't have concepts

118
00:16:18,280 --> 00:16:27,960
without reality, once you understand reality then your concepts that you live with, that

119
00:16:27,960 --> 00:16:43,920
you work with become innocent, become innocuous, but there's still problems, so the problem

120
00:16:43,920 --> 00:16:54,880
that big problems that come up in meditation is attachment, and so the first attachment

121
00:16:54,880 --> 00:17:00,760
that a meditator comes to is the negative attachment, you start to practice properly and

122
00:17:00,760 --> 00:17:08,440
it's not very pleasant, you see impermanence, you think something's wrong, you must be doing

123
00:17:08,440 --> 00:17:14,520
something wrong because this sucks, meditation is not pleasant, it's not comfortable,

124
00:17:14,520 --> 00:17:23,120
this isn't the peaceful, blissful thing that you see on the read about books or see

125
00:17:23,120 --> 00:17:32,080
in movies, people meditating, smiling, you're sitting here crying and moaning and stressing

126
00:17:32,080 --> 00:17:40,320
and freaking out in your room and you're thinking, must be doing something wrong, a version

127
00:17:40,320 --> 00:17:50,760
is the first one, the stress that comes from looking at the things that we normally try

128
00:17:50,760 --> 00:18:10,320
to avoid, it's a great stress involved with that, we see impermanence, everything's changing,

129
00:18:10,320 --> 00:18:18,680
we see suffering, we see non-self, we find that we're not able to control our own minds

130
00:18:18,680 --> 00:18:23,440
and that we're not able to, we're not really in charge, that our minds are not responding

131
00:18:23,440 --> 00:18:39,720
and that it's out of our control, the second way that meditation goes wrong because

132
00:18:39,720 --> 00:18:49,960
eventually you start to accept, you start to see that there's nothing wrong with impermanence,

133
00:18:49,960 --> 00:18:57,360
it's just the way things are, all it means is when the real problem is the clinging, reality

134
00:18:57,360 --> 00:19:03,560
has no problem with us, we have a problem with reality and so as you become more familiar

135
00:19:03,560 --> 00:19:11,480
with it, you become more comfortable with it in the sense of it, it's not bothering you

136
00:19:11,480 --> 00:19:18,800
and so the problem which some of you here are most likely to encounter is the problem

137
00:19:18,800 --> 00:19:24,960
of good experiences as you get more comfortable with the practice and more proficient

138
00:19:24,960 --> 00:19:30,200
and stronger in mind, there will come a point where all this, all of this early stress

139
00:19:30,200 --> 00:19:35,360
and suffering and I think you're all getting there as it's becoming more comfortable

140
00:19:35,360 --> 00:19:45,520
or you're getting more stronger is the positive, you get stuck on the positive, so the

141
00:19:45,520 --> 00:19:55,320
texts talk about the problem of good experiences, if you're practicing correctly you should

142
00:19:55,320 --> 00:20:00,560
have some good experiences, it should start to get smooth, it should feel comfortable,

143
00:20:00,560 --> 00:20:06,160
not always and eventually you let go of this but in the early stages it can be quite

144
00:20:06,160 --> 00:20:15,600
comfortable, the problem is not feeling comfortable, the problem is that we cling to it

145
00:20:15,600 --> 00:20:25,120
and you begin to favor it and you get, you start to learn about what it is that that makes

146
00:20:25,120 --> 00:20:34,920
you comfortable, maybe you see lights or colors or pictures and you follow them, you get

147
00:20:34,920 --> 00:20:47,560
entranced by them and so you don't want to let them go and you do what you can to cultivate

148
00:20:47,560 --> 00:20:55,080
them, it might be a feel calm or maybe a feel happy and you just don't want to move

149
00:20:55,080 --> 00:21:02,720
you don't want to disturb the happiness or the calm and you get attached to it, these come

150
00:21:02,720 --> 00:21:08,240
because we're seeing clearly and we're no longer stressing and so our minds become quite

151
00:21:08,240 --> 00:21:16,680
focused, after four or five days of being here you should all go through these, some of

152
00:21:16,680 --> 00:21:24,160
them, maybe you start to know things and understand things about yourself, maybe you start

153
00:21:24,160 --> 00:21:33,480
to have exceptional experiences of clarity of knowledge, I mean magical sense or you start

154
00:21:33,480 --> 00:21:42,000
to read, be able to see things far away or hear things, some people have clear audience,

155
00:21:42,000 --> 00:21:47,240
clear avoidance, some people remember their past lives, these kind of things, I mean magical

156
00:21:47,240 --> 00:21:55,520
powers they say can come, which are kind of neat and if they don't come, even it's even

157
00:21:55,520 --> 00:22:00,640
worse because you might be waiting for them to come or looking for some kind of extraordinary

158
00:22:00,640 --> 00:22:07,920
experience, but more common is the meditative will start to understand the problems in

159
00:22:07,920 --> 00:22:14,400
their lives, to understand themselves better and so they go with that, they start to

160
00:22:14,400 --> 00:22:21,680
work out their problems and get lost in worldly knowledge, which is good, I mean meditation

161
00:22:21,680 --> 00:22:27,520
does help you solve everyday problems, it's just that when you're doing that you're

162
00:22:27,520 --> 00:22:39,080
no longer being mindful and so well it's practically speaking good from the point of

163
00:22:39,080 --> 00:22:51,280
view of meditation practice, it's not good, it caught up in it, you can get great confidence

164
00:22:51,280 --> 00:22:56,960
and get caught up in that, you can have great effort, things get caught up in that, sometimes

165
00:22:56,960 --> 00:23:05,040
meditators have great effort, quite exerting themselves quite well, but they get caught

166
00:23:05,040 --> 00:23:10,280
up in that and they start being mindful, even mindfulness itself can cause you to start

167
00:23:10,280 --> 00:23:17,120
being mindful as you start to get a hang of it, you start to find that you're able to

168
00:23:17,120 --> 00:23:22,080
know everything, then you get attached to that and you start meditating and that can

169
00:23:22,080 --> 00:23:28,520
happen again and again to you start to lose, it's not the novelty and you're just

170
00:23:28,520 --> 00:23:38,480
mindful, habitually mindful.

171
00:23:38,480 --> 00:23:47,000
So these two, I mean clinging to the bad and clinging to the good, both bad and good

172
00:23:47,000 --> 00:23:51,480
are valid objects of insight meditation but here's the point of you, clinging to the

173
00:23:51,480 --> 00:23:57,920
bad, you're quite likely to stop meditating and this is what leads meditators to run

174
00:23:57,920 --> 00:24:06,240
away when they can't handle the bad, things they don't like, the stressful, and those

175
00:24:06,240 --> 00:24:12,640
who get caught up in the good probably won't stop and in fact might become quite confident

176
00:24:12,640 --> 00:24:19,680
and quite attached to the meditation but they don't get anywhere, it's called sukapati-pataa

177
00:24:19,680 --> 00:24:26,600
dantabinya, a person who has pleasant practice but because they're clinging to the pleasant

178
00:24:26,600 --> 00:24:39,400
ness of it and their faculties are not sharp and they don't, they progress slowly.

179
00:24:39,400 --> 00:24:45,960
So I mean the most important lesson for us is first of all not to shun bad or good experiences

180
00:24:45,960 --> 00:24:53,200
but most importantly to be very careful that we're learning to be objective, that we're

181
00:24:53,200 --> 00:25:00,200
learning to be rooted and grounded, centered in reality as opposed to centered in that

182
00:25:00,200 --> 00:25:12,400
which we like and that which we don't like, trying to be mindful, you know, if you come

183
00:25:12,400 --> 00:25:19,400
to understand what it means to be mindful then you've found a way to be invincible.

184
00:25:19,400 --> 00:25:24,160
There's a great thing about mindful meditation is that if you understand it it's not hard

185
00:25:24,160 --> 00:25:30,400
to understand and you practice it correctly and theoretically it's not hard to practice

186
00:25:30,400 --> 00:25:37,600
either, it's just challenging because the mind is so keen not to be mindful but if you

187
00:25:37,600 --> 00:25:44,080
do it correctly there's nothing that can stand in your way, there's nothing that can cause

188
00:25:44,080 --> 00:25:51,240
you suffering, when you stop reacting, when you start just seeing and you learn how to

189
00:25:51,240 --> 00:25:59,360
just see, how to just be, so there you go some thoughts on how the practice can go wrong.

190
00:25:59,360 --> 00:26:21,760
It's the demo for tonight, thank you all for tuning in, you're back to your practice.

191
00:26:21,760 --> 00:26:29,280
So again, I have answered questions elsewhere but we're experimenting with just answering

192
00:26:29,280 --> 00:26:37,600
questions on the site because it was a happy medium between not answering any questions

193
00:26:37,600 --> 00:26:45,480
and just answering every question because it's static, people can ask the proposed questions

194
00:26:45,480 --> 00:26:50,520
throughout the day, throughout the week and eventually get answers.

195
00:26:50,520 --> 00:27:07,120
If it ever loads, it's not loading for me right now.

196
00:27:07,120 --> 00:27:26,480
On the site isn't loading, I think I'm going to take that as an excuse not to answer

197
00:27:26,480 --> 00:27:32,720
any questions tonight and hopefully it'll be working again and we'll answer questions

198
00:27:32,720 --> 00:27:40,160
another night, thank you all for coming out, wish you all a good night, good practice

199
00:27:40,160 --> 00:28:09,440
and that you find peace, happiness and freedom from suffering, thank you.

