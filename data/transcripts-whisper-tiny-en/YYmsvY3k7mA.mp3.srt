1
00:00:00,000 --> 00:00:05,000
When and where will you be in Thailand, and would I be able to study with you there?

2
00:00:05,000 --> 00:00:10,000
Yeah, sure, wherever I am, you'll be able to study with me.

3
00:00:10,000 --> 00:00:12,000
That's the second part of the question.

4
00:00:12,000 --> 00:00:15,000
When and where will I be in Thailand?

5
00:00:15,000 --> 00:00:26,000
We don't have dates yet, but I'm pretty much confirmed or set on going to Thailand after the rains.

6
00:00:26,000 --> 00:00:34,000
So I would say sometime in November, probably towards the end, middle or end of November.

7
00:00:34,000 --> 00:00:49,000
I'll be in Thailand, and then I'm kind of kind of committed to being in Thailand until February, maybe even the end of February.

8
00:00:49,000 --> 00:00:57,000
Which is a bit difficult because I only can get a three month visa, so I'll probably have to extend that to one year visa.

9
00:00:57,000 --> 00:01:00,000
But anyway, that's when I'll be there this time.

10
00:01:00,000 --> 00:01:07,000
And from the looks of it, I'll be going back and forth, so I might even spend the next rains in Thailand.

11
00:01:07,000 --> 00:01:15,000
Where I will be, I'm going to try to spend a month with my teacher.

12
00:01:15,000 --> 00:01:26,000
We'll see how that goes. And then another three months in this forest monastery, looking after the forest monastery where I was before in Thailand.

13
00:01:26,000 --> 00:01:30,000
While the monk was looking after it now goes home.

14
00:01:30,000 --> 00:01:41,000
So I'm going to try to put directions to that place up on the internet somewhere, because I may not have such good communication while I'm there.

15
00:01:41,000 --> 00:01:48,000
So I'd rather not be arranging people's directions or helping people to get there.

16
00:01:48,000 --> 00:01:51,000
I'll put directions and explanations how to get there.

17
00:01:51,000 --> 00:01:59,000
And if you're clever enough and lucky enough to find the place, then you're welcome to come and study and practice.

18
00:01:59,000 --> 00:02:06,000
Rather than have to arrange transportation or contact people and so on.

19
00:02:06,000 --> 00:02:12,000
Because it's not in the middle of town, it's quite out in the wilderness.

20
00:02:12,000 --> 00:02:16,000
It's not difficult to find actually, and the directions are quite simple.

21
00:02:16,000 --> 00:02:19,000
But it's not a very well-known place.

22
00:02:19,000 --> 00:02:24,000
So you have to give a little bit of direction to someone.

23
00:02:24,000 --> 00:02:28,000
And yeah, you're welcome to come and study.

24
00:02:28,000 --> 00:02:33,000
You have to let us know, because goodties are limited, and there are already people coming.

25
00:02:33,000 --> 00:02:40,000
Eva, one of my old students from Thailand, who's a very good friend, is coming to,

26
00:02:40,000 --> 00:02:48,000
who's already planning to come to Thailand with her daughter, who was in her stomach while in her stomach in her womb,

27
00:02:48,000 --> 00:02:51,000
while she was meditating with me.

28
00:02:51,000 --> 00:02:56,000
And she didn't know it at first, and I think she didn't know it.

29
00:02:56,000 --> 00:03:02,000
And then she found out that her husband was cheating on her.

30
00:03:02,000 --> 00:03:10,000
And then she came back to practice meditation, knowing all this.

31
00:03:10,000 --> 00:03:17,000
And then she went back to her home in Romania, I think she's in Romania.

32
00:03:17,000 --> 00:03:21,000
And raised her daughter, and we've been in contact ever since.

33
00:03:21,000 --> 00:03:24,000
And since she's coming, I don't know how old her daughter is.

34
00:03:24,000 --> 00:03:32,000
There must be five years or something. Three years, five years, I can, but she'll be coming.

35
00:03:32,000 --> 00:03:38,000
And I think there's a couple, I think Laurie, the man who's here from Finland.

36
00:03:38,000 --> 00:03:44,000
I think he will be joining me in Thailand if he's still in the boat.

37
00:03:44,000 --> 00:03:47,000
So it's not going to be an empty monastery.

38
00:03:47,000 --> 00:03:50,000
You'll have to let me know soon.

39
00:03:50,000 --> 00:03:55,000
So we will have to have some communication, but yeah, you kind of have to let us know.

40
00:03:55,000 --> 00:04:21,000
So we can make sure there's space for you.

