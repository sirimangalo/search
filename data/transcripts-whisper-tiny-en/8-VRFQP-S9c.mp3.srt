1
00:00:00,000 --> 00:00:10,240
Hello everyone broadcasting live and getting ready to.

2
00:00:10,240 --> 00:00:15,600
So see on days where I'm doing in the demo pod now I'm going to not broadcast that

3
00:00:15,600 --> 00:00:22,360
live because that will go up as it's own YouTube video shortly.

4
00:00:22,360 --> 00:00:27,040
So what we have now is just answering questions.

5
00:00:27,040 --> 00:00:32,000
This is not going to be that long because it's getting light here.

6
00:00:32,000 --> 00:00:43,200
I had two quizzes do today.

7
00:00:43,200 --> 00:00:49,280
Tomorrow we have our day of mindfulness if people come.

8
00:00:49,280 --> 00:01:01,040
I think there's at least one man coming and another thing we're doing is Monday.

9
00:01:01,040 --> 00:01:10,000
Monday and Friday we're going to try and do a med mom, a McMaster, which means we're just

10
00:01:10,000 --> 00:01:15,840
going to go and meditate in public in the student center which is like it's basically

11
00:01:15,840 --> 00:01:20,160
a cafeteria area but it's more than that it's like the everything there's a Starbucks

12
00:01:20,160 --> 00:01:27,880
and that Tim Hortons and restaurants and transportation hub where you buy your tickets for

13
00:01:27,880 --> 00:01:34,360
public transportation but it's a big area and there is an empty spaces where we could

14
00:01:34,360 --> 00:01:35,360
be meditating.

15
00:01:35,360 --> 00:01:43,240
I mean partly just because we don't have a space it's very difficult to book space

16
00:01:43,240 --> 00:01:53,520
that big master and partly to remind people you know to be public, to be present and

17
00:01:53,520 --> 00:01:59,280
to encourage that kind of thing in others.

18
00:01:59,280 --> 00:02:01,680
So that's happening.

19
00:02:01,680 --> 00:02:03,480
Have any questions?

20
00:02:03,480 --> 00:02:06,320
We have questions.

21
00:02:06,320 --> 00:02:11,080
How wide or narrow is the scope of the various experiences we know?

22
00:02:11,080 --> 00:02:18,800
For example during walking are we noting at the foot sole, foot, leg or whole body walking

23
00:02:18,800 --> 00:02:23,760
or for breathing is it one part or the whole abdomen or maybe the skin wrapping against

24
00:02:23,760 --> 00:02:24,760
the clothes?

25
00:02:24,760 --> 00:02:29,320
I think you might be overthinking you don't worry too much.

26
00:02:29,320 --> 00:02:35,720
Stay with the foot when you walk with the stomach when you see that might change.

27
00:02:35,720 --> 00:02:41,600
The dressing over it is probably a sign of the state of mind and then you should not that

28
00:02:41,600 --> 00:02:42,600
as well.

29
00:02:42,600 --> 00:02:49,880
It shouldn't be you shouldn't be worried about the specific but to specify we are focusing

30
00:02:49,880 --> 00:02:55,200
on the foot when we walk and the abdomen when we sit now that experience might change.

31
00:02:55,200 --> 00:03:00,680
It might sometimes just be the sole of the foot, sometimes just be the tip of the abdomen

32
00:03:00,680 --> 00:03:09,920
or you shouldn't be the idea of purposefully trying for it to be one thing or another

33
00:03:09,920 --> 00:03:19,560
is anti is counterproductive.

34
00:03:19,560 --> 00:03:27,000
Where can the full commentary for the Dhamapada be found?

35
00:03:27,000 --> 00:03:33,960
The journaling game, you get it from Harvard, well you're talking about the translation

36
00:03:33,960 --> 00:03:40,800
as soon, it's over 100 years old I think but the only English translation you can actually

37
00:03:40,800 --> 00:03:45,960
get it on our website we have it in our PDF archive.

38
00:03:45,960 --> 00:03:51,720
Someone sent it to me a while ago, someone wants to post that, I think if someone knows

39
00:03:51,720 --> 00:03:54,080
where our PDF archive is.

40
00:03:54,080 --> 00:04:04,080
I can post that after the broadcast which might be soon because that was the last question.

41
00:04:04,080 --> 00:04:32,080
Okay, all right then thank you everyone, good night, see you all tomorrow.

