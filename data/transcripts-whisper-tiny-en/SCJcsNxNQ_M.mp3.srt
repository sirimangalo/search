1
00:00:00,000 --> 00:00:03,760
Hi, welcome back to Ask a Monk.

2
00:00:03,760 --> 00:00:07,520
Next question comes from Night Goat.

3
00:00:07,520 --> 00:00:11,520
Please discuss the idea of God in Buddhist thinking.

4
00:00:11,520 --> 00:00:15,920
We are taught that the Buddha was not a prophet of God as in Western traditions, but to whom

5
00:00:15,920 --> 00:00:18,400
are the prayers of Buddhists offered?

6
00:00:18,400 --> 00:00:22,440
Clarify the meaning of Prachau and how it fits with Buddhism.

7
00:00:22,440 --> 00:00:28,440
Prachau just means Lord or in this case God, so it's a Thai word.

8
00:00:28,440 --> 00:00:36,040
I'm assuming that goat is Thai or has some connection with Thailand.

9
00:00:36,040 --> 00:00:39,240
Buddhists don't believe in a creator God.

10
00:00:39,240 --> 00:00:43,920
The biggest problem that we have with a creator God is the same problem that the Greeks

11
00:00:43,920 --> 00:00:54,040
had and it's a really an old problem that I think is never, it's not possible to answer.

12
00:00:54,040 --> 00:01:00,400
It's not possible to give an adequate answer and it's the problem of suffering.

13
00:01:00,400 --> 00:01:07,280
There's a really good book by Bard Erman called God's Problem and I really suggest anyone

14
00:01:07,280 --> 00:01:09,960
interested in this subject.

15
00:01:09,960 --> 00:01:11,680
Take it up.

16
00:01:11,680 --> 00:01:18,000
The gist of the problem is if God is all powerful, then why is there suffering?

17
00:01:18,000 --> 00:01:20,600
Why is there evil in the world?

18
00:01:20,600 --> 00:01:28,240
God is all powerful and all loving and there are ways of twisting out of this, but if you

19
00:01:28,240 --> 00:01:36,640
read this book, this is written, this book God's Problem is written by an ex pastor who

20
00:01:36,640 --> 00:01:37,640
is a very devout Christian.

21
00:01:37,640 --> 00:01:46,480
He's one of the leading Bible scholars in the world, but he doesn't believe in God and

22
00:01:46,480 --> 00:01:51,800
he has, this is one of the biggest reasons, the biggest problems for all of us is that there's

23
00:01:51,800 --> 00:01:53,320
no reason for us.

24
00:01:53,320 --> 00:01:56,960
If there's someone who created, as the Buddha said, if there's someone who created the

25
00:01:56,960 --> 00:02:04,880
universe, then he's guilty or that being is guilty for all of the crimes of humanity,

26
00:02:04,880 --> 00:02:09,760
all of the suffering that exists in the world.

27
00:02:09,760 --> 00:02:15,840
In Buddhism, we understand there to be many levels of reality, many types of being, just

28
00:02:15,840 --> 00:02:21,000
as there are many types of mind, and so there are beings called Brahma's, and these,

29
00:02:21,000 --> 00:02:24,560
I would always translate them as gods, I don't think there's anything wrong with that

30
00:02:24,560 --> 00:02:29,960
sort of translation, because they are the higher beings and there would be normally

31
00:02:29,960 --> 00:02:32,600
think of as a God.

32
00:02:32,600 --> 00:02:40,600
They haven't created the world, though there's an interesting story that the Buddha gives

33
00:02:40,600 --> 00:02:49,080
it seems to be quasi-historical, or it seems to be a historical account of why people

34
00:02:49,080 --> 00:02:55,600
might think that God created the universe and why God Himself or God's certain gods might

35
00:02:55,600 --> 00:02:58,240
think that as well.

36
00:02:58,240 --> 00:03:05,080
Why this, this belief exists, and it exists on the level of gods themselves or angels

37
00:03:05,080 --> 00:03:07,480
even because there are many levels.

38
00:03:07,480 --> 00:03:13,480
So suppose a God up at this level, gods up at this level who aren't in touch with the

39
00:03:13,480 --> 00:03:19,680
lower realms, one of them dies, so to speak, or moves down to a lower level that is in

40
00:03:19,680 --> 00:03:21,480
touch with the world.

41
00:03:21,480 --> 00:03:27,160
Suddenly, or not suddenly, but over time, he forgets about his past lives or depending

42
00:03:27,160 --> 00:03:35,440
on the state, no longer remembers the past, and as a result, I'm the only one, and this

43
00:03:35,440 --> 00:03:43,080
is back before there were beings, and after the Big Bang, at the end of one of the last

44
00:03:43,080 --> 00:03:47,200
cycles in the beginning of this cycle, is the first one to come down.

45
00:03:47,200 --> 00:03:53,280
So he thinks, I am in the beginning, there was one, or there was nothing, and then he thinks

46
00:03:53,280 --> 00:03:58,080
to himself, boy, wouldn't it be nice if there was something or he gets kind of bored?

47
00:03:58,080 --> 00:04:08,480
And after some time, later rises, or more beings come down, and so he attributes himself

48
00:04:08,480 --> 00:04:13,960
to be the cause and to be the creator, and it might seem like a fantastical story and nothing

49
00:04:13,960 --> 00:04:19,040
to do with reality, but think about it, this is how superstition arises, this is how religion

50
00:04:19,040 --> 00:04:26,960
is created, it's false attribution of a cause, we want something to happen, it happens,

51
00:04:26,960 --> 00:04:30,160
and then we think, what was I doing when I wanted it to happen?

52
00:04:30,160 --> 00:04:34,320
Maybe I had a horseshoe, or maybe I had a rabbit's foot, and I think, well that means

53
00:04:34,320 --> 00:04:39,280
horseshoes are lucky, if you're rubbing a rabbit's foot, it's lucky, why?

54
00:04:39,280 --> 00:04:42,920
Because I've seen it, when I rub the rabbit's foot, good things happen to me.

55
00:04:42,920 --> 00:04:48,240
It's false attribution, and this is what the Buddha, he goes back and talks about how

56
00:04:48,240 --> 00:04:54,400
this is why many gods think that they're the creators.

57
00:04:54,400 --> 00:05:02,680
And beings get the same problem, they can go into meditative states and suddenly think

58
00:05:02,680 --> 00:05:08,080
they're a prophet, because they can talk to angels and talk to the gods who then tell

59
00:05:08,080 --> 00:05:14,080
them, I'm the creator, I'm the one, I'm the Brahma, and so on and so on.

60
00:05:14,080 --> 00:05:20,240
And they can remember past lives, many prophets, remember being up in heaven and say I was

61
00:05:20,240 --> 00:05:25,360
sent by God, I had to come down, my mission wasn't finished and so on and so on.

62
00:05:25,360 --> 00:05:30,200
So I would say there's a lot of that in the religious traditions, and because these people

63
00:05:30,200 --> 00:05:37,640
tend to be strong-minded, they make very charismatic leaders, and they can be very convincing,

64
00:05:37,640 --> 00:05:42,880
because they have some connection with the other worlds, with angels and with gods.

65
00:05:42,880 --> 00:05:48,680
They themselves believe it, and these gods and angels are deluded in and of themselves,

66
00:05:48,680 --> 00:05:56,000
thinking, you know, having the various views of their own, so it all perpetuates itself.

67
00:05:56,000 --> 00:06:03,560
This is a Buddhist explanation of where God comes from, believe it or not, a more secular

68
00:06:03,560 --> 00:06:10,160
explanation would be in terms of false attribution of cause when we, you know, the rain falls

69
00:06:10,160 --> 00:06:15,040
and we attribute it to this or that action of ours and so on, and then we start to think

70
00:06:15,040 --> 00:06:18,360
well God likes me and God doesn't like me and so on, and this is all part of it.

71
00:06:18,360 --> 00:06:23,200
But I would say from a Buddhist point of view why we believe in God in general is because

72
00:06:23,200 --> 00:06:30,160
we remember these things, we remember these other worlds and we have some kind of innate

73
00:06:30,160 --> 00:06:35,720
understanding of these levels, we just don't have an understanding of the truth of them,

74
00:06:35,720 --> 00:06:44,600
that even gods and angels are impermanent being born and dying, going up and down.

75
00:06:44,600 --> 00:06:48,480
The other part of the question, I think, is much more interesting and that is the idea

76
00:06:48,480 --> 00:06:50,400
of prayer.

77
00:06:50,400 --> 00:06:54,640
Buddhists don't pray as far as, some Buddhists do pray, but it's not a very Buddhist thing

78
00:06:54,640 --> 00:06:56,640
to pray.

79
00:06:56,640 --> 00:07:03,680
What we do in Buddhism and it's often mistaken for prayer is we resolve on certain things.

80
00:07:03,680 --> 00:07:12,080
We make a determination, we make a wish, if you will, and we do this to establish our minds

81
00:07:12,080 --> 00:07:15,880
and to set our minds on what we want to occur.

82
00:07:15,880 --> 00:07:24,320
We say to ourselves, may this happen, may I be happy, may I be able to attain this or

83
00:07:24,320 --> 00:07:26,800
this or that state.

84
00:07:26,800 --> 00:07:32,240
We can even do it with mundane things, may my parents, may their sickness disappear

85
00:07:32,240 --> 00:07:36,920
or this or that, if someone is sick, you're making something like a prayer.

86
00:07:36,920 --> 00:07:44,680
But the idea of a prayer is it has power, your mind is fixed to focus, the faith in your

87
00:07:44,680 --> 00:07:53,160
mind is calming and tranquilizing and establishing the mind firmly.

88
00:07:53,160 --> 00:07:57,160
So it's not the fact that you're praying to someone, it's the fact that you're making

89
00:07:57,160 --> 00:08:01,560
a determination, you're resolving in your mind for this to happen, it's you're making

90
00:08:01,560 --> 00:08:06,560
a wish and that wish is going to form at least a part of your reality and it's going

91
00:08:06,560 --> 00:08:11,840
to have an effect and it can often help if your mind is strong enough, it can help to change

92
00:08:11,840 --> 00:08:14,120
the world around you.

93
00:08:14,120 --> 00:08:16,920
This is a very important part of Buddhist practice.

94
00:08:16,920 --> 00:08:19,440
It's something that we use in our meditation.

95
00:08:19,440 --> 00:08:25,040
I have this problem, may I become free from this problem, may I attain this for that,

96
00:08:25,040 --> 00:08:31,160
if you want to become a monk, may I have the opportunity to become a monk, if you want

97
00:08:31,160 --> 00:08:37,680
to go to a meditation course, may I, may all of the obstacles in my way disappear and

98
00:08:37,680 --> 00:08:42,760
may I be successful in this or that endeavor.

99
00:08:42,760 --> 00:08:49,520
This is totally valid and it's nothing to do with another being coming down and giving

100
00:08:49,520 --> 00:08:54,920
you blessing you with the results of your wish or the answer to your prayers.

101
00:08:54,920 --> 00:09:01,120
It has to do with your changing of reality and in a very mundane sense it's simply your

102
00:09:01,120 --> 00:09:08,760
intention in achieving that goal which focuses your mind and puts you on that path and

103
00:09:08,760 --> 00:09:12,160
propels you along it.

104
00:09:12,160 --> 00:09:19,760
So that's where prayer might come close to Buddhism, again it's not prayer but when you

105
00:09:19,760 --> 00:09:24,880
see people praying for this, wishing for this, wishing for that you can understand that

106
00:09:24,880 --> 00:09:28,240
if they've got their head on straight, if they're really following the Buddha's teaching

107
00:09:28,240 --> 00:09:32,920
then they're not expecting the Buddha to give them the wish and they're not expecting

108
00:09:32,920 --> 00:09:41,000
anything from God, they are just resolving for that and in some way it can affect the

109
00:09:41,000 --> 00:09:46,920
universe as well but mostly it's just going to set their minds on it and will help them

110
00:09:46,920 --> 00:09:50,080
to achieve their goals.

111
00:09:50,080 --> 00:09:53,840
A final note about the Buddha, why we don't expect the Buddha to help us is because the

112
00:09:53,840 --> 00:09:58,600
Buddha's passed away and by past the way we mean he hasn't been born, he wasn't born

113
00:09:58,600 --> 00:10:05,240
again whereas when we pass away we don't really pass away we just continue on but for

114
00:10:05,240 --> 00:10:12,880
someone who has attained Nirvana when they pass away they pass away and so the Buddha

115
00:10:12,880 --> 00:10:18,080
you could say he's still in Kusinara in India that was where he passed away and by

116
00:10:18,080 --> 00:10:25,760
past the way the technical meaning is he went inside, the mind stopped coming out, the

117
00:10:25,760 --> 00:10:32,360
mind ceased to arise, ceased to see here smell, taste feel or think so in a sense you

118
00:10:32,360 --> 00:10:37,160
could say the Buddha's there in Kusinara but is not coming out anymore, the mind is not

119
00:10:37,160 --> 00:10:42,400
arising, there's no more opportunity for it to arise so we see a really past away and

120
00:10:42,400 --> 00:10:48,000
there's no chance for the Buddha to hear your prayers or answer them, at least not the

121
00:10:48,000 --> 00:10:56,480
Buddha who's not the Buddha, there are many beings in Buddhism that are said to answer

122
00:10:56,480 --> 00:11:04,960
prayers but that's all other kind of worms and I guess that it has to be admitted that

123
00:11:04,960 --> 00:11:11,560
yes you can wish for things and ask for the angels and gods to help you and the problem

124
00:11:11,560 --> 00:11:14,960
is you don't know whether they're going to help you or not and it's completely up

125
00:11:14,960 --> 00:11:24,960
to them, there is no being out there who is purely compassionate and wise and is going

126
00:11:24,960 --> 00:11:32,720
to answer all of your prayers, there's a lot of beings out there who are going to pose

127
00:11:32,720 --> 00:11:42,800
in that rule and instead wind you around their finger and twist your will to theirs

128
00:11:42,800 --> 00:11:47,440
and you'll find yourself following them and that's I think the case with many meditators

129
00:11:47,440 --> 00:11:53,880
when they get caught up with angels and other beings they find that rather than the angels

130
00:11:53,880 --> 00:12:00,200
and gods serving them they wind up serving these beings and following after all of their

131
00:12:00,200 --> 00:12:05,720
defilements and their illusions and delusions as well, okay so I hope that gives an answer

132
00:12:05,720 --> 00:12:09,400
it's a very interesting question and I'd like to encourage everyone to make these

133
00:12:09,400 --> 00:12:14,560
determinations especially after you practice and especially in regards to your own happiness

134
00:12:14,560 --> 00:12:34,640
and the happiness of your fellow beings, may all beings be happy.

