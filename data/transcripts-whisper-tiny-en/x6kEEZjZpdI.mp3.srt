1
00:00:00,000 --> 00:00:06,200
Hello and welcome back to our study of the Dhamapada. Today we continue on with

2
00:00:06,200 --> 00:00:14,000
verses 143 and 142 which read as follows.

3
00:00:14,000 --> 00:00:41,800
Listen to Dhamapada coming with me and pleased Him to come together.

4
00:00:41,800 --> 00:01:11,760
The meaning of which are 143, here in Isidopu is a human person who is a person who is a

5
00:01:11,760 --> 00:01:22,720
person who is held back by shame, the shame being the aversion to bad deeds, the aversion

6
00:01:22,720 --> 00:01:29,800
towards unwholesunness, the disinclined unwholesunness, quote jidokasimimitati, whatever person

7
00:01:29,800 --> 00:01:36,480
in the world is found to be this way, restrained by the understanding that bad deeds are

8
00:01:36,480 --> 00:01:43,680
bad, wrong, what is wrong is wrong, you need to hang up a bow deity, this is an interesting

9
00:01:43,680 --> 00:01:55,760
one, it could mean who awakens from sleep, or it could mean who avoids, doesn't awaken

10
00:01:55,760 --> 00:02:11,320
nature, like a horse to the whip, so it's either a thoroughbred horse, either a thoroughbred

11
00:02:11,320 --> 00:02:19,320
horse, avoids the need to use the whip, or else they awaken because of the whip, I'm not

12
00:02:19,320 --> 00:02:25,680
sure which one it is, either translation seems to me, actually prefer the second one,

13
00:02:25,680 --> 00:02:36,840
awakening from sleep, like the horse, the whip, or the horse does one whipped, so a horse

14
00:02:36,840 --> 00:02:44,280
awakens because the whip, just a person awakens from their state of sleep, but it could

15
00:02:44,280 --> 00:02:51,840
also be ninja, which would mean something totally different, the point is don't sleep,

16
00:02:51,840 --> 00:03:00,840
and don't do that, which is worthy of said censure, one or the other. 144 Ahsoyatab and

17
00:03:00,840 --> 00:03:08,520
Rokasani Rito, who is touched by the whip, like the horse that is the thoroughbred horse

18
00:03:08,520 --> 00:03:15,480
that is touched by the whip, awdapino one should have effort, some wiggie no one should

19
00:03:15,480 --> 00:03:28,480
be urgentness, have a sense of urgency, balatha, become one who is effortful, or be one

20
00:03:28,480 --> 00:03:37,320
who is effortful, who cultivate effort and sense of urgency, with faith, with confidence,

21
00:03:37,320 --> 00:03:45,600
have ethics and effort, sadaya, cilayana, javilayana, samadhana, with concentration,

22
00:03:45,600 --> 00:03:57,400
dhammavini, caiana, with discernment, or the understanding about the dhamma, the siding

23
00:03:57,400 --> 00:04:02,600
based on the dhamma, making decisions based on the dhamma.

24
00:04:02,600 --> 00:04:12,600
Upa noi jajajajajana, being fully endowed with knowledge and conduct, but this sataya,

25
00:04:12,600 --> 00:04:22,320
mindful, or being one who is devoted to jajajajita, avoiding jajajita dukamidha nana pakana,

26
00:04:22,320 --> 00:04:35,280
you avoid this suffering which is not insignificant, that is a little bit, but there's many

27
00:04:35,280 --> 00:04:38,520
things in there, so we'll try and go through those.

28
00:04:38,520 --> 00:04:47,680
The first verse is relating to describing a person who is like a horse, like a thoroughbred

29
00:04:47,680 --> 00:04:52,960
horse, because they either don't inspire the use of the whip, or when they are whipped,

30
00:04:52,960 --> 00:04:57,480
they know what to do, they react appropriately.

31
00:04:57,480 --> 00:05:03,640
And like such a person, you should cultivate, you should be awake, cultivate effort and

32
00:05:03,640 --> 00:05:08,880
the sense of urgency and so on, that's the verse.

33
00:05:08,880 --> 00:05:17,160
The story behind this isn't actually a short but fairly memorable story of a young lad,

34
00:05:17,160 --> 00:05:29,120
who was wearing a simple loin cloth as a baker, you know, one of these people very destitute

35
00:05:29,120 --> 00:05:36,320
that all they have is a simple begging ball and one piece of cloth to their name is

36
00:05:36,320 --> 00:05:42,360
dirty and solid and it's all they've got to cover their bodies with, someone in a fairly

37
00:05:42,360 --> 00:05:44,960
desperate situation.

38
00:05:44,960 --> 00:05:50,520
And Ananda saw him one day and said to him, hey, don't you know, don't you, what do

39
00:05:50,520 --> 00:05:55,040
you think wouldn't it be better to become a monk, wouldn't it be coming a monk be better

40
00:05:55,040 --> 00:06:03,160
than this life, and he said, well who will ordain me, so come along our day.

41
00:06:03,160 --> 00:06:09,760
So he took him with him and bathed him and taught him meditation and made a monk of him,

42
00:06:09,760 --> 00:06:14,400
gave him the requisites and so on.

43
00:06:14,400 --> 00:06:23,840
And turned him into a monk and this new monk looked at his old rags and spread it out

44
00:06:23,840 --> 00:06:29,800
and looked at it and it was so solid, so awful, that there was not even a little bit

45
00:06:29,800 --> 00:06:34,320
of it that he could use for straining water because they would use cloth for straining

46
00:06:34,320 --> 00:06:38,240
water, but it was so disgusting that there was no part of it, but he couldn't give

47
00:06:38,240 --> 00:06:45,320
it up and so he bundled it up with his old little begging ball or whatever and put it

48
00:06:45,320 --> 00:06:52,160
up in the crook of a tree and went on.

49
00:06:52,160 --> 00:07:02,720
So eventually he was made a full monk and he was given all the wonderful accoutrements

50
00:07:02,720 --> 00:07:07,800
involved with being a Buddhist monk at the time, which meant he would have great robes

51
00:07:07,800 --> 00:07:16,440
and food and lodging and quite luck, quite a bit of luxury once Buddhism became famous,

52
00:07:16,440 --> 00:07:28,120
the monks were subject to a great amount of comfort and as a result he became complacent

53
00:07:28,120 --> 00:07:33,960
and fat apparently and eventually became discontent because he didn't really see what

54
00:07:33,960 --> 00:07:40,200
the point of all this was and it felt kind of I guess like being fake and he thought

55
00:07:40,200 --> 00:07:47,080
I should just go back to my old way but I'm not have to get involved with this thing

56
00:07:47,080 --> 00:07:51,040
that I don't really understand or believe in or doing it just to get fat and everyone

57
00:07:51,040 --> 00:07:57,720
is really this is really kind of hypocritical of me I should go back to how I was and

58
00:07:57,720 --> 00:08:03,120
so he went back to the tree and found this old rag and pulled it down and then he thought

59
00:08:03,120 --> 00:08:07,600
about that and he thought dude this is what I've left behind this life of destitution

60
00:08:07,600 --> 00:08:14,080
and poverty, his life of suffering I can't do that and so he went back and tried again

61
00:08:14,080 --> 00:08:18,960
but you know he felt like he wasn't really doing what he should be doing as a monk he wasn't

62
00:08:18,960 --> 00:08:26,000
really meditating or behaving properly so again he felt kind of bad about it and again

63
00:08:26,000 --> 00:08:31,280
he went back to the tree and picked up the robe but the cloth but he couldn't bring himself

64
00:08:31,280 --> 00:08:38,440
to do it just look at this cloth and said you know this is not the way and three times

65
00:08:38,440 --> 00:08:43,240
he went back and forth and the monks eventually started to see this and asked him you know

66
00:08:43,240 --> 00:08:51,880
where you're going is how I'm going to see my teacher and again and again he went back

67
00:08:51,880 --> 00:09:04,520
and forth finally or eventually you know contemplating this and contemplating in general

68
00:09:04,520 --> 00:09:08,000
and because it makes that kind of thing makes you somewhat full of soft and gone you think

69
00:09:08,000 --> 00:09:13,240
you know what is what really is the point to any of this he started to see and to take

70
00:09:13,240 --> 00:09:18,080
this cloth which would be a very good object of meditation that's solid and it's it's

71
00:09:18,080 --> 00:09:36,800
the symbol of some sorrow really with its filth and it's representing the need to seek

72
00:09:36,800 --> 00:09:45,200
out food and the need to clothe your body just what it represents it represents the suffering

73
00:09:45,200 --> 00:09:54,080
the potential for destitution that awaits us in our every turn we can fall into and eventually

74
00:09:54,080 --> 00:09:59,440
doing this he actually was able to apply himself I would go back and he would try to meditate

75
00:09:59,440 --> 00:10:04,560
and then he wouldn't be able to go back to his cloth and again and again and to finally he just

76
00:10:04,560 --> 00:10:10,960
let go and he became an arachn from I guess listening to the Buddhist teaching as well and having

77
00:10:10,960 --> 00:10:18,080
this great teacher in this ragro and so eventually he became an arachn and then immediately stopped

78
00:10:18,080 --> 00:10:25,440
he no longer would return to this tree where he kept this robe and the monk asked him

79
00:10:25,440 --> 00:10:32,400
he wasn't going to see your teacher and what happened he said oh they said well before I

80
00:10:32,400 --> 00:10:38,400
when I was attached the world then I I needed to teach her but now that I've cut off all

81
00:10:38,400 --> 00:10:46,080
ties I don't be a teacher anymore and they thought to them job one is this this is quite the

82
00:10:46,080 --> 00:10:50,400
boast and so they went to the teacher and they said this monk goes around saying something

83
00:10:50,400 --> 00:10:58,640
that's not true and the Buddha said the more did he say and he said well he says he used to walk

84
00:10:58,640 --> 00:11:02,720
with the teacher now he has no attachments he's now he's cut all the ties so he doesn't need

85
00:11:02,720 --> 00:11:09,840
to teach her anymore which is basically saying he's enlightened the most well yes that's truly

86
00:11:11,280 --> 00:11:16,480
and he called him my son when my son was attached to the world and he needed to teach her

87
00:11:16,480 --> 00:11:22,000
and so the Buddha didn't reveal what he knew about being a robe that he went back to so I needed

88
00:11:22,000 --> 00:11:27,200
to teach her before but now he has restrained himself and he no longer needs someone else to restrain

89
00:11:27,200 --> 00:11:34,800
it and so then he taught these two verses so we have two lessons here we have the lesson from

90
00:11:34,800 --> 00:11:40,000
the story and we have the lesson from the verses or a couple of lessons maybe from the verses

91
00:11:40,960 --> 00:11:45,920
but the story is interesting and in how it it gives an example of how the world can be

92
00:11:45,920 --> 00:11:50,400
your teacher how inanimate objects can teach you so much you know inanimate objects that mean

93
00:11:50,400 --> 00:11:58,640
something things that we as we personally ascribe meaning to I mean it's great meaning in the

94
00:11:58,640 --> 00:12:09,040
cloth as I said it represents his life represents life represents suffering and that's it you know

95
00:12:09,040 --> 00:12:18,320
kind of important our ability to comprehend the potential for suffering in the world

96
00:12:18,320 --> 00:12:26,240
but it would often talk about our ability to recognize the suffering in life recognize the potential

97
00:12:26,240 --> 00:12:32,160
that we too will get old we too will get sick we too will die we too could suffer destitution

98
00:12:32,160 --> 00:12:40,560
poverty hunger thirst you know any any number of types of suffering

99
00:12:40,560 --> 00:12:52,080
and so it really evokes in us this this questioning of why and and and how do I escape and

100
00:12:52,080 --> 00:13:00,640
what is the right way to go what is the right thing to do so we begin to question these things

101
00:13:00,640 --> 00:13:05,680
that we took for granted and say you know really is that all that there is is that the best

102
00:13:05,680 --> 00:13:11,520
that I can do is this life is this really the right way to go to live my life as though everything

103
00:13:11,520 --> 00:13:19,680
was alright to live my life as though I I would never fall into that for this man it was it was

104
00:13:19,680 --> 00:13:29,280
quite a visceral insight for him to to have this object that don't constantly remind him

105
00:13:29,280 --> 00:13:37,840
of his potential to fall right back into suffering and a reminder of what life how harsh

106
00:13:37,840 --> 00:13:44,000
life can be but then on the other hand he's like you know I can't just take advantage of this

107
00:13:44,000 --> 00:13:50,160
being among her staying as a monk that's what most of us do right we take advantage of our

108
00:13:50,160 --> 00:13:57,600
good fortune take advantage of having money and having family and having possessions when all

109
00:13:57,600 --> 00:14:04,720
these things can disappear in the heartbeat in many cases we don't even deserve them we haven't

110
00:14:04,720 --> 00:14:12,160
done anything but not not we aren't doing anything to to deserve them and so this this sorts of

111
00:14:12,160 --> 00:14:16,320
things what would what he would contemplate it's a good contemplation for all of us this is a very

112
00:14:16,320 --> 00:14:20,640
good example right they're not seem like it we're not all beggars living on the street but

113
00:14:20,640 --> 00:14:31,920
we live in this dilemma of falling asleep really in being lulled into a false sense of security

114
00:14:34,480 --> 00:14:46,640
and inevitable fall back into suffering and poverty and whatever so I think that's a good lesson

115
00:14:46,640 --> 00:14:52,560
for us a good reminder it's not a meditation practice but certainly supports our meditation

116
00:14:53,280 --> 00:15:02,640
because as a result we come to see that the only way is to change our minds and to be invincible

117
00:15:04,960 --> 00:15:14,240
to be unmoved by the vicissitudes of life that changes the potential because we can't stop

118
00:15:14,240 --> 00:15:22,560
we can't control the future and if that's the way we choose to go we'll always be disappointed

119
00:15:22,560 --> 00:15:30,560
when things change in ways that we're not comfortable with so that's I think the first lesson

120
00:15:30,560 --> 00:15:36,640
here the second lesson the lesson from the verse is perhaps more applicable directly to meditation

121
00:15:36,640 --> 00:15:42,880
and the first one is by the first verses is I guess really the same sort of lesson as the story

122
00:15:42,880 --> 00:15:50,160
is related to wake up waking up when we remember the suffering of life every time this man went

123
00:15:50,160 --> 00:16:00,240
back to the tree would remember how unpleasant it was being in his situation and so there's a sign

124
00:16:00,240 --> 00:16:05,680
of a good person but it's like the sign of a thoroughbred horse is that when they see the whip

125
00:16:05,680 --> 00:16:10,640
they know what to do when they feel the whip that they stand they know what to do but the same

126
00:16:10,640 --> 00:16:16,000
with the human when they hear about people suffering they know what to do they know that something

127
00:16:16,000 --> 00:16:21,760
needs to be done to avoid that to avoid the suffering associated with change with loss and so on

128
00:16:23,440 --> 00:16:28,880
and therefore they develop themselves they cultivate good things and so on

129
00:16:28,880 --> 00:16:45,600
and just like waking from sleep or avoiding the blame that comes from it not true

130
00:16:46,960 --> 00:16:52,160
but the second verse 144 is really quite it's probably one of the one of the most

131
00:16:52,160 --> 00:16:59,040
piecey of the demo by the verses it's because it's finally talking about a whole bunch of good

132
00:16:59,040 --> 00:17:04,480
qualities so what are the qualities like the qualities of thoroughbred horse what are the qualities

133
00:17:04,480 --> 00:17:10,880
of a thoroughbred meditator? meditator who is high, who is special, who is exceptional,

134
00:17:11,920 --> 00:17:20,400
odd, doppy, you know, they cultivate effort, they exert themselves, they don't just

135
00:17:20,400 --> 00:17:27,840
lays around doing nothing, they get up and they do the walking and the sitting, the mindful,

136
00:17:27,840 --> 00:17:33,360
they wake up their minds really, they have the effort of mind to repeatedly attend to the

137
00:17:33,360 --> 00:17:38,800
meditation object, some wiggle you know why because some wiggle they have a sense of urgency

138
00:17:39,680 --> 00:17:46,000
they realize that this could come tomorrow change could come at any time and change is the only

139
00:17:46,000 --> 00:17:54,240
constant and I'm not ready for that, I'm not comfortable with that, I'm not at peace with that

140
00:17:55,840 --> 00:18:00,320
and so they have a sense of urgency that they're going to suffer if they don't do that.

141
00:18:02,640 --> 00:18:08,880
Sadaya with confidence, so they have confidence in what they're doing, they find a path that

142
00:18:08,880 --> 00:18:14,880
is worthy of confidence and then they have confidence because it's also possible to doubt that

143
00:18:14,880 --> 00:18:20,720
which is worthy of confidence, you can find the right path and see that it gives you good results

144
00:18:20,720 --> 00:18:28,320
and still end up doubting it which is a pretty sad state but it's quite common because why

145
00:18:28,320 --> 00:18:34,720
because doubt is a habit, doubt is something to do with nothing to do potentially with what

146
00:18:34,720 --> 00:18:40,560
you're doing, it's a reaction when things get difficult, it's a defense mechanism in many cases,

147
00:18:40,560 --> 00:18:50,080
it stops us from having to do things that bring about suffering, so we should be, we should let

148
00:18:50,080 --> 00:18:57,280
go of our doubt when we found out which we know is beneficial. Celena, we should have ethics,

149
00:18:57,280 --> 00:19:05,120
we should avoid bad thoughts or bad deans, bad speech, we should work to cultivate ethics

150
00:19:05,120 --> 00:19:11,440
so that our mind stays focused, so that our mind does not distract and by all the unethical

151
00:19:11,440 --> 00:19:21,280
behaviors that we've engaged in, really in and again with media effort, some odd with concentration

152
00:19:21,280 --> 00:19:26,640
or focus, keeping our mind's focus not letting our mind get distracted, bringing the mind back

153
00:19:26,640 --> 00:19:41,920
again and again to attend on the truth, on reality, we need chaiya means giving an answer or

154
00:19:41,920 --> 00:19:54,640
no investigating or understanding maybe we need chaiya, discrimination maybe the better

155
00:19:54,640 --> 00:19:59,920
translation, so the ability to discriminate, this is good, this is bad, this is

156
00:20:02,080 --> 00:20:07,120
profitable, unprofitable, helpful, unhelpful, useful, unuseful,

157
00:20:08,240 --> 00:20:12,160
ability to see, because when you look inside you'll start to see those aspects, those

158
00:20:12,160 --> 00:20:17,920
behaviors, those habits that we have that are beneficial, that are to our benefit and

159
00:20:17,920 --> 00:20:28,160
that are helpful to the others as well, and then we can see those which are detrimental,

160
00:20:28,160 --> 00:20:35,680
so being fully endowed with not only knowledge, knowing the truth, but also acting accordingly,

161
00:20:35,680 --> 00:20:43,600
we need to generalize a good definition of Buddhist wisdom, it's the type of wisdom that involves

162
00:20:43,600 --> 00:20:49,120
practice as well, that is really about who you are, not just what you believe or what you think,

163
00:20:49,760 --> 00:20:55,520
wisdom is not just thinking or believing something, wisdom is knowing it to such an extent that it

164
00:20:55,520 --> 00:21:01,920
penetrates into your own behavior, who you are, it's about how you act about your behavior,

165
00:21:01,920 --> 00:21:12,000
it's unbelievable, you believe or anything, but this that I think means being mindful, either that

166
00:21:12,000 --> 00:21:18,560
or it's just, yeah, no, I think it's being mindful, but this that we have in the Satipatanasita,

167
00:21:19,360 --> 00:21:24,800
so specific type of mindfulness, but this Satipati means specifically, so it's like being

168
00:21:24,800 --> 00:21:33,040
just mindful, barely mindful, not judging, not reacting, that might be what it means,

169
00:21:34,240 --> 00:21:43,440
Jihi Sita took me down and up again, you will avoid this great mass of suffering, the suffering,

170
00:21:43,440 --> 00:21:55,040
which is not just a little bit, great suffering, you avoid suffering, which is not insignificant,

171
00:21:55,040 --> 00:22:01,520
so you could argue that you can totally be free from suffering, right, there is pain and there is

172
00:22:04,080 --> 00:22:11,360
unpleasant situations that you have to be faced with, but there's a huge, a much greater

173
00:22:11,360 --> 00:22:17,440
suffering in the mind, and the greatest part of suffering is not in the great wide world out there

174
00:22:17,440 --> 00:22:27,520
that is terrible and fearsome and the cause for so much upset, but the great mass of suffering

175
00:22:27,520 --> 00:22:36,320
is actually in this little invisible corner of the universe that is our mind,

176
00:22:36,320 --> 00:22:48,080
which is very simple, simple thing, simple reality of our own mind,

177
00:22:49,440 --> 00:22:53,840
and it's just one moment after, and others, where all the suffering goes again, the intense amount

178
00:22:53,840 --> 00:22:58,800
of stress and suffering and the great torture that we put ourselves through,

179
00:22:59,760 --> 00:23:04,240
there's nothing to do actually with the outside world, no matter what happens,

180
00:23:04,240 --> 00:23:14,080
gain or loss, praise or blame, no matter what good things or bad things come,

181
00:23:15,120 --> 00:23:20,480
matter what terrible things might happen, matter how bad things might get, our suffering has really

182
00:23:20,480 --> 00:23:27,200
nothing to do with any of that, the greater part of suffering will always come from our own mind,

183
00:23:27,200 --> 00:23:37,680
come from our bad habits and our lack of good qualities,

184
00:23:39,280 --> 00:23:46,480
our inability to penetrate each experience with wisdom and to see it clearly as it is,

185
00:23:46,480 --> 00:23:56,960
as a result instead we misjudge, misunderstand, and react inappropriately to all of our experiences

186
00:23:57,760 --> 00:24:03,440
and suffer as a result, not as a result of the experiences themselves, but as a result of our

187
00:24:03,440 --> 00:24:12,560
reactions to it, so that's less than for us, cultivating all these things, cultivating effort,

188
00:24:12,560 --> 00:24:21,920
and sense of urgency, we have to work at it, there's a good reminder for us

189
00:24:24,560 --> 00:24:27,920
that this isn't something that isn't a chore that we're obliged to do,

190
00:24:28,640 --> 00:24:34,080
it's an escape, that's a way out, you practice in this way a great amount of suffering,

191
00:24:34,080 --> 00:24:43,760
it will be free, so that's the demo panda for this evening, thank you all for tuning in,

192
00:24:43,760 --> 00:25:13,600
we're seeing you all good practice.

