Okay, I'm going to line it.
So today we're going to the Jumini Kyan number 27, the jurahatipa no pala sunta.
We've actually given talks on before and studied before, so it's going to be familiar to
some people.
It's going to be familiar to me anyway.
So again, the format is first we will be chanting part of the pali and then reading the
English and discussing it paragraph by paragraph.
If you're here live, you can ask questions via YouTube if you're watching this after
the fact as well, you get to leave comments, we're just going to start it here in a minute.
All right, get into the text here.
Okay, we're ready to start.
Let's begin.
Go jalam moko chasamam nang o tama pasam sisami
Pasatatasatou asu wa wang o tama se to di wa manu sanandhi
kambana bhaum wachayin o ata wa samsam pasamam no
pasamam nigotami wa manu pasam no di
se yatabi bho kusalona wa nikona
gawanam pavi se yah
so pasayana gawanam a hantam a di padam
di gato chaya tantinam yahan jawita tantam
so nitang a jayam a hantam o nang o tini
they were me wakou an bho yatou asu sanam samam nigotami
jatai padani a tantinam nigotam
samam bho baga wa swakatou wakawatam
so pasaytinam a gawatou asu wakasang o ti
kata mani chata rida amo pasami
a kajayati a pandai tindu
di pune a nata para pawa di
wallo a di rupa a di vinantam
mani a jaran di bhan yahakatin a di pakatani
they soon a nitang samarno kahou bho yatam o
a mukanam a gamam wa nigamam wa o sari sati di
they panam a pasang karun di imam ma yam panang samarnam
kata mano pasang kamitwa bho jisamam
a wang jayatou wakawatou a wang yahakatis sati
a wamasamayam wa danarou
pasamam a wang jayatinou bho pho
a wang yahakatis sati
a wang pasamayam wa dan la
pasamati
they soon a nitang samarno kahou bho
kahou bho yatam o a mukanam a gamam wa
nigamam wa o sattori
they a nitang samarno go tamao tane a pasang kamanti
they samarno go tamao dame yahakatis
a nitang satti
a nitang satti
a nitang satti
nitang satti
Bhagavad-ho, sawakasambuddhi, punejapar, ahambo, bhasam, yi-de, kachay brahmana, pani-dhi-dhi-pai, kah-apati-barndi-dhi-pai,
samana-pani-dhi-tai, nil-pani-kata-pa-ra-pa-wa-dhi-wa-la-wa-la-wa-dhi-dhi-ru-pai,
tae-bindantan, yhe-charanti-pa-nya, kate-na-dy-tik-a-tani,
tesunantisam-an-o-kalubo-gautam-o- Kamanga-ma-ma-ma-ma-ma,
nil-yagan-wa-o-sari-sati-dhi,
tae-pa-an-an-am-wisang-haran-ti-im-am-a-ma-a-m-ayam-ban-ham,
saman-ang-gautam-ma-mo-swa-sang-am-mit-wam-woo-chisam-am-mit-woo-chisam-a-m-ma-nah-
a one jay no lorto a one ya kari sati a one must some a young wad and a rope is
hama a one jay p no burpedo a one ya kari sati a one piece some a young wad and
a hero pays I'm party Tastes do none tee some ano color o goo j descending whad nigaman
while s processed to dete eat.
Camin mo go to moteinne pers Second Gandh
t is Yasam- dyno, Bourbon
the Bhang's aiti.
There is a man in a goat, a man in a miyakata,
yasanda, sita, samada, pita, samute,
jita, sampa, nasita, najay,
was samana, and goat, and mampana, and goat,
jandhi, kutosawana, rho, aitanti,
and yada, to samana, we ye,
a goat, and mamu, kasan, yajandhi, rasman,
nagali, and pamba, jaya,
tasamana, a goat, and mampana,
jita, tasamana, vu, bakata,
a pamata, pita, pita, pita,
we are undone, jita, sita,
yasata, yasuna, putasamadeva,
rasma, nagari, yaampa,
jandhi, tasana, rasman,
rasman, jaya, pari, yasana,
and goat, tay, wadami,
saya, mavinya, sajikatma,
pasampa, jaya, ranti,
tay, wasman, suman,
nana, kutosawana, sama,
mana, wadamu, vana, sama,
may, yan, kutu, vay,
sama, nana, sama, nana,
tay, tay, tay, tay, tay, jan,
nima, a bra, mana,
wasama, nana, mana,
tay, bakata, jan, nima,
anarato, sama, nana,
aarantama, tay, tay, tay, jan,
nana, nima, nima, sama,
aarata, nima, mavra,
mana, nana,
nima, nana, nima,
aarantu, tay,
yada, mama, sama, nima,
tay, mama, jatutampa,
nana, nana, sama,
kutasampa, nana, nita,
mama, mama, sama,
sambudu, vakata,
sama, tay, wadasamu,
sama, tay, panno, vakata,
sama, wadasam, wadasam,
gothi, yatoko,
hanbo, sama, nigo,
tame, mani, jatata,
nipata, ni, anasamata,
nita, mama, mama,
sama, sambudu, vakata,
vakata, vakata,
mama, supati,
vakata, sama, wadasamu,
tay, vakata,
jana, sony brahmanu,
sama, sita,
vakata, vakata,
hooro, he twah,
he comes and
tara, sankan,
kari twah, yay,
nabagawati,
nana, nana, nana,
vakata,
tikatutamu,
nana, mudani,
sie, namota,
sama, wadasamma,
sambudasa,
namota, sabbagawatu,
hara, hato, sama,
sambudasa,
namota, sabbagawatu,
hara, hato, sama,
sambudasa,
payu,
nama, maiyam,
vikata, jikara,
hati,
tay, namota,
vakata, mini,
nasa, jim,
samanga, jay,
nama,
payu, nama,
siyakawati,
vakata,
sambagawati,
hato, kojana,
soni, brahmano,
yay, navagawati,
nupasangami,
upa, sangami,
vakata,
vakata,
vakata,
vakata,
vakata,
vakata,
sambudani,
nankata,
sarni,
nyan,
viti, sari,
vakata,
akamantani,
sidi,
akamantani,
sindo,
kojana,
sari,
vakata,
jangami,
hato,
hoo,
sippi,
jikin,
vakata,
kajin,
sari,
kara,
sallapotansamamamara,
vakawati,
sisi,
ayamutay,
vakawati,
janu,
soni,
brahmanu,
yitalahocha,
nakobra,
vanai,
tah,
vatai,
tipadoo,
pamawitari,
nahari,
vakata,
vakata,
hoo,
japith yamana,
katith,
vakata,
pamawitari,
sambaridamo,
practise,
tamsuna,
jcouldho,
vakata,
sthenitin,
ayahangobata,
kajanu,
soni,
vakawataw,
vakata.
soar
af taxable
wakata,
chance. Just going to see how far we've
been here, and then we'll see how far we've been here.
Okay, we're still broadcasting. We can do what that's like, right? So some technical difficulty
are computer just for Thomas, and everything's up for it should be. So I think we're going
to stop the poly there. I think we're going to stop the poly there. We're only about a third
done in the soup death. But I think near the end it gets repetitive. It repeats from
other soup death. So hopefully we've actually done about a half of them, but with the
stuff. Let's see.
Just want to
see if we can find a better place to start.
Now I think we'll do it in three parts then. Okay, so today we'll do the first part. We've
gotten through the, sorry, I'll just pull up the text here again.
Okay, so we got through the
preface of the text, which is actually a whole section itself. So we'll stop there.
And we'll
start with English. So let's have a heard. On one occasion, the blessed one was doing a
sour tea in Jesus Grove, and out to Pentecost Park. Now, a little timbit of information.
This was the first two to preach by Mahindatira following his arrival in Sri Lanka. That's
interesting. I wonder if that's why it was, it's one of the only
souters that I taught in Sri Lanka. There's a video on knowing the Buddha, I think, I
pulled on a rua. I was just looking at it. Trying to think where I'd remember giving a
talk, and it was actually while we were visiting
I pulled in a rua in Sri Lanka. That's why I was giving it.
Now in that occasion, the Brahman, John
Osoni, was living out at Sawati in the middle of the day in an all-white chariot drawn
by white mares. He saw the wonder of Pilotika coming in the distance and asked him.
Now, where's Master Vachayana coming from in the middle of the day?
His name was Pilotika, but they called him by his clan name, which I am also his family name.
So this is a prologue to the actual suit.
What wouldn't cost it to come about? Why did the Buddha teach?
Sir, I am coming from the presence of the recluse Gautama.
What does Master Vachayana think of the recluse Gautama's lucidity of wisdom?
He is wise, is he not? Sir, who am I to know the recluse Gautama's lucidity of wisdom?
One would surely have to be as equal to know the recluse Gautama's lucidity of wisdom.
Master Vachayana praises the recluse Gautama with high praise indeed.
Sir, who am I to praise the recluse Gautama?
The recluse Gautama is praised by the praise to best among gods and humans.
There are some high praise, no, he is really setting him up.
What reasons does Master Vachayana see that he is such firm confidence in the recluse Gautama?
Here we get the, similarly of the elephant-hoof.
The elephant footprint, the elephant footprint, but this is the original part.
The Buddha is going to give his own interpretation.
He talks about four things.
Sir, suppose a wise elephant-whizman would enter an elephant-wood,
and were to see in the elephant-wood a big elephant's footprint, long in extent and broader cross.
He would come to the conclusion, indeed, this is a big bowl elephant.
So too, when I saw four footprints of the recluse Gautama, I came to the conclusion the blessed one is fully enlightened.
The dhamma is well proclaimed in the blessed one.
Sankas practicing the good way.
What are the four?
So, his reckoning is that when a wise elephant-whizman sees a big elephant's footprint,
they'll know right away that this is a big bowl elephant.
So you can tell an elephant by its footprint.
Now, spoiler, Buddha is going to say that you can't actually, he's going to actually extend this, which is interesting.
But it's still an interesting summary, the point that, although it is limited, you see, the problem with this is, of course, that people can be,
he's going to talk about praise, but you can be praised without warrant, right?
It's possible that people who don't don't warrant or don't deserve praise can be praised.
So, he's got this idea that if you hear about how great people are from other people, from others, it's a sign that's surely there are great.
Now, in this case, it's true, but there are, of course, cases where that's not true, where people are highly praised, but underneath, they're actually rogues and devils.
But anyway, he's got four ways of, four elephant footprints that he says, show him that the bunezer, true, will elephant.
So, the first one.
Sir, I have seen here certain learn nobles who are clever, knowledgeable about the doctrines of others, as sharp as hair splitting marksmen.
They wonder about, as it were, demolishing the views of others with the sharp wits.
When they hear the reckless go-to-mount will visit such and such a village or town, they formulate a question thus.
We will go to the reckless go-to-mount and ask him a question.
If he is asked like this, he will answer like this.
And so, his doctrine in this way, and if he is asked like that, he will answer like that.
And so, we will refute his doctrine in that way.
To people who, anyone who, the most clever of the clever, whether they be, and just to skip ahead, the four are actually going to be nobles, brahmins.
Would we have nobles, brahmins, householders, which are like merchants, and then recklessly?
So, the wisest of the wisest of all these four groups, sharp as hair splitting marksmen, the best of the best, they come to go to the Buddha.
Thinking that they are going to house smart him, and that probably he will be shamed in becoming their reckless.
He will be odd by their wit and their wisdom, and will become their disciples.
And then, they hear, the reckless go-to-mount has come to visit such and such a villager town.
They go to the reckless go-to-mount, and the reckless go-to-mount structures, rouses, and glens them with a talk in the dhamma.
After they have been instructed, urged, roused, and glened by the reckless go-to-mount with a talk in the dhamma.
They do not so much ask him the question, so how should they refute his doctrine?
And, actual fact, they become his subjects.
Thank you.
Oops.
Thank you very much.
Wonderful.
Thank you.
Thank you so much.
In fact, they become his disciples.
When I saw this first foot front of the reckless go-to-mount, I came to the conclusion, the blessed one is fully enlightened.
The dhamma is well proclaimed by the blessed one.
The sunka is practicing the good way.
So he kind of jumps, jumps to conclusions here.
It's a good sign, but the good is going to say that it's not the only sign.
Anyway, it's impressive that this occurs.
No matter who comes to see the Buddha, it's sure to impress us just about anyone.
In fact, that no matter who goes to try to debate with the Buddha, they always end up becoming his disciples.
So all of the nobles, this would be the kings and royalty, all of the high-class aristocracy.
They all end up becoming his disciples, even though they think that he is going to be shamed by them.
So this is the first.
The second is the Brahmans do the same.
The third is that householders do the same.
And the fourth is that the fourth is a little bit different.
The fourth is that the preclusers do the same, but not only do they become his students,
but let me start here in an actual effect.
An actual fact, they ask the repless go-to-mounta to allow them to go forth from the home life and the homelessness.
And it gives them the going forth.
Not long after they have gone forth, dwelling alone with drawn, diligent, ardent and resolute.
They are realizing for themselves with direct knowledge.
They hear now interupon and abide in that supreme goal of the holy life,
with a sake of which clans been rightly go forth from the home life and the homelessness.
They say thus, we were very nearly lost, we were very nearly perished.
We, for formerly, we claim that we were requeses, that we were not really requeses.
We claim that we were Brahmins, that we were not really Brahmins.
We claim that we were our hearts, that we were not really our hearts.
And now we are requeses, now we are Brahmins, now we are our hearts.
When I saw this fourth footprint of the reckless go-to-mounta, I came to the conclusion, the blessed one is fully enlightened.
Okay, so he's actually seen, so what he's seen of lay people is that they all become converted and came to establish great faith in the Buddha.
But what he's also seen is even somewhat more impressive, that those people who actually are looking for spiritual enlightenment,
were practicing spiritual path.
When they come to the Buddha, not only did they become converted, but they actually attain their goal.
They actually do become enlightened by practicing Buddha's teaching, and they claim for themselves to be true.
Our hearts, two true requeses, two Brahmins, two our hearts.
And so that the most impressive footprint of all, he points out to Januson.
When I saw these four footprints of the reckless go-to-mounta, I came to the conclusion, the blessed one is fully enlightened.
The Dharma is well proclaimed by the blessed one. The Sangha is practicing the good way.
Certainly reasonable to assume that this is not just any old run of the meal teaching, or run of the meal teacher.
And so Brahmins, Januson, he does what any of us would think to do.
He's not exactly, but he gets quite excited.
When this was said, the Brahmins, Januson, he got down from his all-white chariot, drawn by white mares.
And arranging his upper rope on one shoulder, he extended his hands in reverential salutation toward the blessed one,
and under this exclamation three times.
Honored to the blessed one, accomplished and fully enlightened.
Honored to the blessed one, accomplished and fully enlightened.
Perhaps some time or other, we might meet Master Gautama, and have some conversation with him.
This is actually, if you heard us in the chanting, this is Namontas Abhagavatil, and I had told some of them.
So you can see that this, this, Namontas, actually comes from Natapitika.
We have it here, and we have it elsewhere.
Common means we respect arranging a rover on shoulder, and holding your hands up to your chest in reverential salutation.
Doesn't mean holding like giving a military salute or something.
It's putting, it's pulling your hands up to your chest, and unduly like a prayer kind of thing.
And then he says, well, maybe I can meet them, Master Gautama, but then he does go with it right away,
or maybe later, maybe another day or something.
Then the Brahman, John Isone, went to the blessed one, and exchanged greetings with him.
When this courteous and amiable talk was finished, he sat down on one side and related to the blessed one,
his entire conversation with the wonder of Pilotika.
Thereupon the blessed one told him, at this point, Brahman,
the simile of the elephant's footprint, does not yet been completed in detail.
As to how, as to how it is completed in detail, listen in a tin carefully to what I shall say.
Yes sir, the Brahman, John Isone, he replied, the blessed one, said this.
That's as far as Gautama actually.
That is about a third, and we would have gone further, I think, but then we got cut out.
So we stopped there with the Pilots, let's stop there with the English.
So here we have the preface to the sipta tomorrow.
We'll actually get into what the Buddha says, and of course you can read along.
You can read ahead if you want.
If you have the sipta in front of you, it looks like we'll have three sessions.
Tomorrow we'll discuss the Buddha's idea of how you tell whether someone is a bull elephant.
Let's cut back to here, and we say ourselves again.
And that's all for tonight.
Thank you all for stopping in.
I don't, we don't have quite a question thing up this time.
I don't know whether that actually worked, but hopefully this video has gone up, and we've done the first third of the Julia Heartbeat by the Open Sipta.
Thank you all for tuning in, have a good night.
