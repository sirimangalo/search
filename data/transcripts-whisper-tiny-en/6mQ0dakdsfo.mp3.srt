1
00:00:00,000 --> 00:00:04,080
When I come to see something really clearly catching a bad thought in the

2
00:00:04,080 --> 00:00:09,760
middle of daily activities for instance, it often triggers joy in me. I feel I

3
00:00:09,760 --> 00:00:14,960
have insight. Do you think this joy is wholesome or is it based on ego? How to deal

4
00:00:14,960 --> 00:00:22,240
with it? Again, the importance or the usefulness of the Abhidhamma. Joy so

5
00:00:22,240 --> 00:00:29,960
can be either wholesome or unwholesome. In and of itself it's neither. It can be

6
00:00:29,960 --> 00:00:37,520
accompanied by wholesomeness or unwholesomeness. If you like something and that has

7
00:00:37,520 --> 00:00:43,400
a joy associated with it then that's unwholesome. If you do something positive

8
00:00:43,400 --> 00:00:48,200
like help someone else or cultivate insight and joy comes spontaneously from

9
00:00:48,200 --> 00:00:58,240
that then that's neutral. If you do something wholesome and you feel good

10
00:00:58,240 --> 00:01:02,600
about that wholesomeness and feel joy doing good deeds at the at-moment of

11
00:01:02,600 --> 00:01:07,600
doing then that's wholesome. But it's not wholesome because of the feeling.

12
00:01:07,600 --> 00:01:12,600
It's not wholesome because of the joy. Joy is indeterminate. It's a part of the

13
00:01:12,600 --> 00:01:20,080
mind that it's neither positive or negative. It's indeterminate. It's neutral.

14
00:01:20,080 --> 00:01:24,400
It depends what it's accompanied with and it can come in three spots. One in an

15
00:01:24,400 --> 00:01:33,160
unwholesome mind. Two in a wholesome mind. And three in a mind that is the

16
00:01:33,160 --> 00:01:38,200
result of a wholesome mind. So feeling happy because of something good that

17
00:01:38,200 --> 00:01:42,320
you've done. The first one is unwholesome. The second one is wholesome but the

18
00:01:42,320 --> 00:01:48,600
third one the result is neutral. And in all three cases the quality of the

19
00:01:48,600 --> 00:01:53,520
mind is not determined by the joy. So joy is indeterminate. You see how useful

20
00:01:53,520 --> 00:01:58,680
the abbey dumb is. I can answer that question without any doubt. Whether you

21
00:01:58,680 --> 00:02:04,160
believe me or not this is another thing. Dealing with it is just to say to

22
00:02:04,160 --> 00:02:08,520
yourself happy happy feeling feeling. If you like it say liking like it and

23
00:02:08,520 --> 00:02:32,520
so on. See it for what it is.

