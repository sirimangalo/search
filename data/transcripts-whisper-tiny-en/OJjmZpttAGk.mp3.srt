1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Damabanda.

2
00:00:05,000 --> 00:00:15,000
Today we continue on with verses 155 and 156, which read as follows.

3
00:00:15,000 --> 00:00:22,000
A chari to a brahmacharyang, Alanda, Yobani, Dhanang.

4
00:00:22,000 --> 00:00:32,000
Chin, kod, kon, cawa, jayanti, quin, machaywa, paula lee.

5
00:00:32,000 --> 00:00:38,000
A chari to a brahmacharyang, Alanda, Yobani, Dhanang.

6
00:00:38,000 --> 00:00:49,000
Santi, chapa, kin, kawa, puranani, annu, kunam.

7
00:00:49,000 --> 00:01:03,000
Which means having not practiced the holy life, having not gained wealth in one's youth.

8
00:01:03,000 --> 00:01:20,000
One wastes away, they waste away like old, like old herons, old herons in a pond where the fish are gone.

9
00:01:20,000 --> 00:01:28,000
Like old herons in a pond where the fish are gone.

10
00:01:28,000 --> 00:01:38,000
That's the first verse, the second verse, having not lived the holy life, having not gained wealth in one's youth.

11
00:01:38,000 --> 00:01:52,000
And they lie like arrows shot from a bow.

12
00:01:52,000 --> 00:01:59,000
Lamenting over things over things over things in the past.

13
00:01:59,000 --> 00:02:09,000
Lamenting over times gone.

14
00:02:09,000 --> 00:02:21,000
Again, this chapter is a little bit on the negative side, which is important, it's important to talk about these things.

15
00:02:21,000 --> 00:02:29,000
I'll get into that, I'd like this to actually be a positive experience, this one I think is quite positive for us.

16
00:02:29,000 --> 00:02:45,000
So the story goes that there was a young boy, and his parents were quite rich.

17
00:02:45,000 --> 00:02:54,000
Here in fact, so rich that they thought his parents thought to themselves that,

18
00:02:54,000 --> 00:03:04,000
why would we teach him how to make money, why would we teach him how to live, how to work?

19
00:03:04,000 --> 00:03:08,000
There's so much wealth he can never use at all.

20
00:03:08,000 --> 00:03:20,000
And so they simply instructed him on how to enjoy himself, thinking that his life would be a life of luxury, a life of pleasure.

21
00:03:20,000 --> 00:03:28,000
Instructed him in singing and then playing musical instruments, and that was all he received.

22
00:03:28,000 --> 00:03:36,000
And there was, in the same city, there was a very rich woman about his age, and her parents did likewise.

23
00:03:36,000 --> 00:03:38,000
And they spoiled them both.

24
00:03:38,000 --> 00:03:40,000
Let me use the word spoiled.

25
00:03:40,000 --> 00:03:50,000
I mean, here it's put quite neutrally, it's not spoiled, it's just thought him the things that would be important to him, which is basically nothing.

26
00:03:50,000 --> 00:03:59,000
I thought him how to enjoy himself, thinking nothing else could possibly be important, right?

27
00:03:59,000 --> 00:04:07,000
And eventually his parents, eventually these two got married, so this woman married this treasure, son, this rich man, son.

28
00:04:07,000 --> 00:04:14,000
They got married, so they had immeasurable wealth, a lot of money.

29
00:04:14,000 --> 00:04:19,000
And as a result, they were good friends with a king, and so they would go and wait upon the king.

30
00:04:19,000 --> 00:04:38,000
One day on his way to see the king, he saw this group of men by the side of the road, and these guys had, they knew that this guy was really rich, but actually quite naive.

31
00:04:38,000 --> 00:04:54,000
And so they thought it was a plan, a scam. And so they sat by the side of the road when he went by, and they sat drinking alcohol and enjoying themselves.

32
00:04:54,000 --> 00:05:10,000
And as he was going by, he saw these men who seemed really happy, and they were acting up to appear as happy and as joyful and talking about the benefits of what they were drinking.

33
00:05:10,000 --> 00:05:18,000
And so he asked them, nobody in drinking, so we're drinking alcohol. And he said, does it taste good?

34
00:05:18,000 --> 00:05:27,000
And he said, oh, in this world there's no drink like, and he said, well then I must have some.

35
00:05:27,000 --> 00:05:35,000
And so being in my youth, man that he was, he had never even heard of alcohol or had anything to do with it.

36
00:05:35,000 --> 00:05:43,000
And these men gave him some alcohol first a little, and then a little more until eventually he became, of course, addicted.

37
00:05:43,000 --> 00:05:50,000
Perhaps not physically addicted, although that's quite possible as well. But certainly mentally addicted.

38
00:05:50,000 --> 00:06:01,000
It was, I think this was one of the drugs at that time. I mean, even today it's a common drug of choice for a lot of people.

39
00:06:01,000 --> 00:06:12,000
He became an alcoholic. But I think part of the reason he became an alcoholic is simply because he was accustomed to enjoying whatever he could get his hands on.

40
00:06:12,000 --> 00:06:22,000
I don't think he was enjoyable. He had no sense that there might be a problem of getting addicted to it or over indulging it.

41
00:06:22,000 --> 00:06:29,000
So whereas a little bit of alcohol is a bad thing, a lot of alcohol is a terrible thing.

42
00:06:29,000 --> 00:06:41,000
And so eventually he became an alcoholic and as a result lost all of his sense of moderation, whatever it literally had.

43
00:06:41,000 --> 00:06:50,000
And whereas before living an ordinary life, it would have been quite difficult for him to use up all his money.

44
00:06:50,000 --> 00:07:03,000
As a result of his excessive or his extreme state of intoxication, he squandered his money.

45
00:07:03,000 --> 00:07:12,000
And he was actually able to waste it away. If you've ever seen, this is the funny thing about alcohol.

46
00:07:12,000 --> 00:07:26,000
If you've ever been to Las Vegas, to the casinos, apparently one of the important things is to apply the customers with alcohol because it makes them lose whatever it is that keeps you from gambling away your life savings.

47
00:07:26,000 --> 00:07:33,000
Likewise bars. A bar, it's incredible bar times that people can rack up.

48
00:07:33,000 --> 00:07:40,000
Not just that they will drink alcohol at all, but that they'll pay exorbitant amounts of money for the alcohol.

49
00:07:40,000 --> 00:07:47,000
As a result of being without their general sense of reason.

50
00:07:47,000 --> 00:08:01,000
And so he acted in this way, giving money away, left, right, center, and of course the people around him were these men who had introduced him to strong drink and they were robbing him blind.

51
00:08:01,000 --> 00:08:08,000
Didn't even have to go behind his back. They just convinced him to give away all his money.

52
00:08:08,000 --> 00:08:18,000
Eventually, he gave away all his money and he had to sell all of his things or that he found out that he had no money and he said, well, what about my wife because she had money?

53
00:08:18,000 --> 00:08:29,000
And so he had his wife give him all of her money as well. She had no idea. They had no sense of saving the money.

54
00:08:29,000 --> 00:08:42,000
And so squandered all of her money as well. Eventually, he had to sell all of his belongings, his carriages and his fine clothes and his jewelry and eventually his house.

55
00:08:42,000 --> 00:08:53,000
Eventually, he was so much in debt that he had, he was turned out of his house and eventually became a beggar.

56
00:08:53,000 --> 00:09:01,000
He was living in the streets of, living in the streets of Waternessy, which is usually we're in Solidity.

57
00:09:01,000 --> 00:09:16,000
But this time we're in Waternessy when Buddha was dwelling in Isipatana, which is interesting as well because when it didn't spend much time at Isipatana, anyway.

58
00:09:16,000 --> 00:09:27,000
Today the Buddha was walking through the city on arms round and he came upon these two old people begging for food.

59
00:09:27,000 --> 00:09:42,000
I saw the man anyway and so I'm begging for food and the Buddha stopped and smiled.

60
00:09:42,000 --> 00:09:50,000
Again, with a sense of humor of the Buddha, Anand asked him, the noble sir, why are you smiling?

61
00:09:50,000 --> 00:09:57,000
I was like, do you see that man over there? That man used to be one of the richest men in the city.

62
00:09:57,000 --> 00:10:11,000
And now after having squandered all of his life savings and all of his wife's life savings, all caused by indulgence in alcohol.

63
00:10:11,000 --> 00:10:27,000
And his parents lack of lack of any kind of education or even common sense in terms of how to maintain your wealth.

64
00:10:27,000 --> 00:10:46,000
Now, reduced to poverty and destitution, he has become like a heron and a dried up, a heron and a pond, a heron and a dried up pond.

65
00:10:46,000 --> 00:11:10,000
And Buddha says something interesting, he says, if he had in his, if you want to understand the real consequences of what he's done, if in his youth he had invested his money and applied himself to business, he could have been the number one business man in the world.

66
00:11:10,000 --> 00:11:20,000
He had enough wealth and he had enough intelligence that if he had only applied himself, he could have been the richest man in the city.

67
00:11:20,000 --> 00:11:27,000
If he had gone forth, if he had left the home life and become a reckless, become Buddhist monk, he could have become an a heron.

68
00:11:27,000 --> 00:11:33,000
His wife would have become an anha Gandhi.

69
00:11:33,000 --> 00:11:36,000
That was it when he was young, he had done it.

70
00:11:36,000 --> 00:11:46,000
Or in his middle years, if he had applied himself to business, he could have been the number two rich man in the world.

71
00:11:46,000 --> 00:11:54,000
Not as good because he wouldn't have had enough time and enough energy and enough capital.

72
00:11:54,000 --> 00:11:58,000
And if he had gone forth, he would have become an anha Gandhi.

73
00:11:58,000 --> 00:12:12,000
And if he had gone forth as an adult, and his wife would have become a Sakada Sakada Gandhi.

74
00:12:12,000 --> 00:12:27,000
If when he was in his third, sort of the middle-aged, sort of older persons years, the third period of adulthood, whatever that is,

75
00:12:27,000 --> 00:12:34,000
if he had applied himself for business, he could have been come, well, a very rich man, perhaps the third richest.

76
00:12:34,000 --> 00:12:41,000
And if he had gone forth, he would have become a Sakada Gandhi and his wife would have become a saute upon.

77
00:12:41,000 --> 00:12:44,000
But now he's nothing.

78
00:12:44,000 --> 00:12:50,000
Now he's lost any chance of being wealthy as a layman, as a householder.

79
00:12:50,000 --> 00:12:57,000
He's also lost any opportunity to become enlightened. His mind is too corrupt.

80
00:12:57,000 --> 00:13:05,000
Perhaps he's got liver disease and his brain has been at all to buy the alcohol.

81
00:13:05,000 --> 00:13:16,000
And so he then spoke to these two verses about if they haven't led the holy life or then anything in the world,

82
00:13:16,000 --> 00:13:23,000
you know, useless on both counts. And like a heron, and then in the dried-up pond without fish,

83
00:13:23,000 --> 00:13:27,000
or then like an arrow cast from a bow, it's quite poetic.

84
00:13:27,000 --> 00:13:37,000
When you shoot an arrow off into the forest and it just lies there useless, something that is cast off, something that is thrown away,

85
00:13:37,000 --> 00:13:44,000
gone far from any use, any value.

86
00:13:44,000 --> 00:13:51,000
So, again, fairly unpleasant story, a sad story.

87
00:13:51,000 --> 00:13:54,000
Of course, the Buddha founded humorist.

88
00:13:54,000 --> 00:14:02,000
But again, with the Buddha and with our aunts,

89
00:14:02,000 --> 00:14:08,000
it's an expression of the great peace and the great joy

90
00:14:08,000 --> 00:14:11,000
in having freed oneself from samsara.

91
00:14:11,000 --> 00:14:16,000
I mean, these two people, they're not doomed. They're just stuck.

92
00:14:16,000 --> 00:14:21,000
And that samsara, I mean, this is the way of things.

93
00:14:21,000 --> 00:14:25,000
They're going to be reborn again and they'll have another chance and they'll keep going.

94
00:14:25,000 --> 00:14:28,000
That's the way of the world. There's not anything you can do about it.

95
00:14:28,000 --> 00:14:35,000
We can't save everyone.

96
00:14:35,000 --> 00:14:41,000
But that's the really wonderful thing is that this first gives us cause to be joyful.

97
00:14:41,000 --> 00:14:44,000
The first thing it should do, and it's not the only thing.

98
00:14:44,000 --> 00:14:51,000
We're not gloating over those people who are unable to become enlightened.

99
00:14:51,000 --> 00:14:57,000
But I think it's important because we often get down on ourselves.

100
00:14:57,000 --> 00:15:02,000
I give all these talks on mindfulness and insight.

101
00:15:02,000 --> 00:15:06,000
And it sounds like to be of any worth you somehow have to

102
00:15:06,000 --> 00:15:10,000
attain some high level of enlightenment.

103
00:15:10,000 --> 00:15:17,000
But really, we should all be proud and encouraged anyway.

104
00:15:17,000 --> 00:15:23,000
Probably this may be not so good, but be encouraged about ourselves.

105
00:15:23,000 --> 00:15:25,000
Because we've done good things.

106
00:15:25,000 --> 00:15:29,000
Even just coming here to listen or even just coming to this YouTube channel

107
00:15:29,000 --> 00:15:32,000
and turning on the video.

108
00:15:32,000 --> 00:15:35,000
Even just coming to second life to the Buddhist Center thinking,

109
00:15:35,000 --> 00:15:39,000
maybe I'll find something of value here.

110
00:15:39,000 --> 00:15:43,000
That's an incredibly rare thing and it's an incredibly

111
00:15:43,000 --> 00:15:46,000
praise worthy activity.

112
00:15:46,000 --> 00:15:54,000
It shows some incredible foresight on your part.

113
00:15:54,000 --> 00:15:59,000
You might not think that. It might seem quite insignificant.

114
00:15:59,000 --> 00:16:04,000
But we underestimate the power of this.

115
00:16:04,000 --> 00:16:08,000
How many people would ridicule someone who came to the Buddhist Center?

116
00:16:08,000 --> 00:16:10,000
How many people would ridicule someone?

117
00:16:10,000 --> 00:16:13,000
How many people ridicule me for making these videos?

118
00:16:13,000 --> 00:16:17,000
How silly. Who is this guy?

119
00:16:17,000 --> 00:16:19,000
What is he doing?

120
00:16:19,000 --> 00:16:24,000
And those of you who watch thinking your all sheep

121
00:16:24,000 --> 00:16:29,000
or you're all wasting your time when you could be

122
00:16:29,000 --> 00:16:32,000
living your life to the fullest.

123
00:16:32,000 --> 00:16:34,000
Carpe diem.

124
00:16:34,000 --> 00:16:40,000
Eat drink and be merry.

125
00:16:40,000 --> 00:16:43,000
I say it's a rare thing in the world.

126
00:16:43,000 --> 00:16:49,000
The Buddha said it's well, but don't think it takes a Buddha to see how

127
00:16:49,000 --> 00:16:56,000
wonderful it is for people to actually incline in a good way.

128
00:16:56,000 --> 00:17:03,000
The Buddha said it's have a measurable benefit to meditate even for a moment.

129
00:17:03,000 --> 00:17:07,000
If we have one moment of mindfulness where we're here

130
00:17:07,000 --> 00:17:11,000
where we're present, where we see things as they are,

131
00:17:11,000 --> 00:17:14,000
where the body, the feelings, the mind, the dhamma,

132
00:17:14,000 --> 00:17:17,000
just one moment is an incredible thing.

133
00:17:17,000 --> 00:17:22,000
How much preparation it takes to get to that moment

134
00:17:22,000 --> 00:17:25,000
where you're actually mindful?

135
00:17:25,000 --> 00:17:27,000
Don't underestimate it.

136
00:17:27,000 --> 00:17:33,000
Don't underestimate the power of the mind that's inclined in the right direction.

137
00:17:33,000 --> 00:17:39,000
The second thing, of course, that it has to do is give us a kick in the pants

138
00:17:39,000 --> 00:17:42,000
reminding us that the clock is ticking.

139
00:17:42,000 --> 00:17:49,000
As with much in this chapter, it's the old age chapter.

140
00:17:49,000 --> 00:17:52,000
Stealing with the fact that we'll all get old,

141
00:17:52,000 --> 00:17:56,000
and if we've done nothing of any benefit by the time we get old,

142
00:17:56,000 --> 00:18:01,000
we just waste the waste, squander this opportunity that we have.

143
00:18:01,000 --> 00:18:03,000
We will pay for it.

144
00:18:03,000 --> 00:18:05,000
There will be consequences.

145
00:18:05,000 --> 00:18:09,000
We'll have wasted this valuable opportunity.

146
00:18:09,000 --> 00:18:13,000
It's not about failing or succeeding.

147
00:18:13,000 --> 00:18:15,000
It's about using or wasting.

148
00:18:15,000 --> 00:18:19,000
We have this opportunity, are you trying?

149
00:18:19,000 --> 00:18:23,000
Much of Buddhism is about trying and failing.

150
00:18:23,000 --> 00:18:26,000
There was something on Facebook that I've seen before.

151
00:18:26,000 --> 00:18:29,000
It was on Reddit, maybe.

152
00:18:29,000 --> 00:18:35,000
This wholesome, memes, subreddit.

153
00:18:35,000 --> 00:18:37,000
What is it?

154
00:18:37,000 --> 00:18:39,000
I haven't failed.

155
00:18:39,000 --> 00:18:44,000
I've just found a thousand ways that didn't work.

156
00:18:44,000 --> 00:18:47,000
I think that's a good thing.

157
00:18:47,000 --> 00:18:51,000
An important thing, important way of looking at things.

158
00:18:51,000 --> 00:18:56,000
Because you only really fail when you stop trying.

159
00:18:56,000 --> 00:18:59,000
And trying itself is succeeding.

160
00:18:59,000 --> 00:19:02,000
It's an activity that makes you a better person.

161
00:19:02,000 --> 00:19:05,000
Much of meditation is about failing.

162
00:19:05,000 --> 00:19:07,000
It's about realizing.

163
00:19:07,000 --> 00:19:10,000
It's about not coming to understand gradually.

164
00:19:10,000 --> 00:19:12,000
You're doing everything wrong.

165
00:19:12,000 --> 00:19:15,000
How you can't control things.

166
00:19:15,000 --> 00:19:18,000
It's how you aren't in charge.

167
00:19:18,000 --> 00:19:21,000
And so much of meditation is banging your head against the wall

168
00:19:21,000 --> 00:19:25,000
until you realize that your ordinary way of approaching things,

169
00:19:25,000 --> 00:19:32,000
meditation, is useless, is harmful.

170
00:19:36,000 --> 00:19:38,000
And so don't be discouraged.

171
00:19:38,000 --> 00:19:41,000
Thinking many people are discouraged, thinking I can't meditate.

172
00:19:41,000 --> 00:19:43,000
I'm just not able.

173
00:19:43,000 --> 00:19:44,000
I can't focus.

174
00:19:44,000 --> 00:19:47,000
I try to meditate, but I can't focus on that.

175
00:19:47,000 --> 00:19:52,000
And that is a great reason to learn how to meditate.

176
00:19:52,000 --> 00:19:57,000
Everyone who says they can't meditate because their mind won't stay still.

177
00:19:57,000 --> 00:20:01,000
It's misunderstanding the whole point of meditation.

178
00:20:01,000 --> 00:20:04,000
It has to be told anyway.

179
00:20:04,000 --> 00:20:06,000
That's the reason why you meditate.

180
00:20:06,000 --> 00:20:08,000
Because you can't focus.

181
00:20:08,000 --> 00:20:11,000
You meditate to understand that.

182
00:20:11,000 --> 00:20:14,000
You meditate to overcome that.

183
00:20:14,000 --> 00:20:17,000
And you meditate to let go of that.

184
00:20:17,000 --> 00:20:21,000
You don't meditate to force yourself not to get distracted.

185
00:20:21,000 --> 00:20:25,000
You meditate because somehow some people are able to find a switch

186
00:20:25,000 --> 00:20:27,000
where they turn their mind off.

187
00:20:27,000 --> 00:20:29,000
We want to meditate on the distraction.

188
00:20:29,000 --> 00:20:32,000
We want to learn about our distractions.

189
00:20:32,000 --> 00:20:35,000
We want to learn about our failures.

190
00:20:35,000 --> 00:20:37,000
That's all that's required.

191
00:20:37,000 --> 00:20:40,000
We're not required to succeed in any way.

192
00:20:40,000 --> 00:20:42,000
Succeeding comes from understanding.

193
00:20:42,000 --> 00:20:46,000
It's understanding what you're doing wrong.

194
00:20:46,000 --> 00:20:54,000
The third thing that this first tells us, of course, is the importance of good advice.

195
00:20:54,000 --> 00:21:04,000
Which is why we should never take for granted what we've gained from the Buddha's teaching.

196
00:21:04,000 --> 00:21:09,000
We should never waste an opportunity.

197
00:21:09,000 --> 00:21:18,000
I'm going to sound like proselytizing fears when I say this, but waste an opportunity to share the doubt with others.

198
00:21:18,000 --> 00:21:23,000
I mean, I think it would be fine if Christians and whoops, or if atheists were,

199
00:21:23,000 --> 00:21:28,000
if they went about sharing things, let's say other religions went about sharing their religions.

200
00:21:28,000 --> 00:21:31,000
If only their religion was true, it was the truth.

201
00:21:31,000 --> 00:21:33,000
It was at all useful.

202
00:21:33,000 --> 00:21:36,000
And in fact, some of the things they do share, we could say, are useful.

203
00:21:36,000 --> 00:21:39,000
So that's okay.

204
00:21:39,000 --> 00:21:45,000
But I would be completely on board with, I mean, I'm completely on board.

205
00:21:45,000 --> 00:21:49,000
It's not proselytizing or sharing is wrong.

206
00:21:49,000 --> 00:21:50,000
Two things.

207
00:21:50,000 --> 00:21:54,000
First, if the teachings are wrong, don't share them.

208
00:21:54,000 --> 00:21:55,000
Give them up.

209
00:21:55,000 --> 00:21:57,000
That's a problem.

210
00:21:57,000 --> 00:22:01,000
The second, doesn't mean pushing the teachings on others.

211
00:22:01,000 --> 00:22:06,000
And part of being able to, I think the next set of verses that we're going to learn about,

212
00:22:06,000 --> 00:22:10,000
or if one was coming up, we'll talk about this in more detail.

213
00:22:10,000 --> 00:22:17,000
But an opportunity to share the Dharma is not, hey, there's someone I think they should learn.

214
00:22:17,000 --> 00:22:19,000
Let me teach them.

215
00:22:19,000 --> 00:22:22,000
An opportunity to share the Dharma is someone suffering.

216
00:22:22,000 --> 00:22:29,000
They're asking you, or they are close enough to you that you are in a position.

217
00:22:29,000 --> 00:22:38,000
You fulfill a role as a relative, a friend, a companion.

218
00:22:38,000 --> 00:22:51,000
You fulfill a role whereby they would, and will, listen, and make use of things that you have learned.

219
00:22:51,000 --> 00:22:53,000
Hey, I heard about this meditation technique.

220
00:22:53,000 --> 00:22:57,000
I've been trying it really works for me if you need help.

221
00:22:57,000 --> 00:23:00,000
Try it. Maybe it works.

222
00:23:00,000 --> 00:23:12,000
Someone who's looking, or someone who is pleading, is asking, or is open to it.

223
00:23:12,000 --> 00:23:13,000
Not to convince.

224
00:23:13,000 --> 00:23:16,000
It's not about convincing people.

225
00:23:16,000 --> 00:23:24,000
If you have to convince someone beyond simply making a, presenting a case,

226
00:23:24,000 --> 00:23:32,000
if you have to convince them, then it's not likely to turn out all that well.

227
00:23:32,000 --> 00:23:34,000
Take the Buddha's example.

228
00:23:34,000 --> 00:23:42,000
For those who were unable to understand, he just smiled.

229
00:23:42,000 --> 00:23:46,000
I think that's hard for people to swallow.

230
00:23:46,000 --> 00:23:50,000
Smiling at other people's misfortune.

231
00:23:50,000 --> 00:23:56,000
Well, what can I say, that's how the Buddha rolls.

232
00:23:56,000 --> 00:24:06,000
But take the time to share the dhamma.

233
00:24:06,000 --> 00:24:13,000
Remember, some people like this, these two rich people.

234
00:24:13,000 --> 00:24:20,000
If only when they were young, or if they Buddha had caught them earlier, they could have done something.

235
00:24:20,000 --> 00:24:22,000
And they could have done something with their lives.

236
00:24:22,000 --> 00:24:28,000
With just the right friendship, the right advice, the right support.

237
00:24:28,000 --> 00:24:31,000
Instead, they fell into wrong friendship.

238
00:24:31,000 --> 00:24:37,000
And of course, one more thing that this first teaches us is the evils of bad friends,

239
00:24:37,000 --> 00:24:44,000
the evils of alcohol, things like that, evil evil evil.

240
00:24:44,000 --> 00:24:51,000
Again, I don't want to sound like I'm preaching, but I'm not going to argue

241
00:24:51,000 --> 00:24:53,000
that alcohol is evil.

242
00:24:53,000 --> 00:24:57,000
I know it's bad, bad, bad.

243
00:24:57,000 --> 00:25:00,000
It's the opposite of being mindful.

244
00:25:00,000 --> 00:25:03,000
It does as the opposite effect.

245
00:25:03,000 --> 00:25:08,000
It's mindfulness is about really experiencing reality.

246
00:25:08,000 --> 00:25:11,000
It's about experiencing all of parts of reality.

247
00:25:11,000 --> 00:25:15,000
We take alcohol to avoid who we are.

248
00:25:15,000 --> 00:25:17,000
I'm the kind of person who gets upset.

249
00:25:17,000 --> 00:25:20,000
I'm the kind of person who's worries and stresses about everything.

250
00:25:20,000 --> 00:25:25,000
Well, I'll just take alcohol and then I won't worry and stress.

251
00:25:25,000 --> 00:25:27,000
It's like any drunk.

252
00:25:27,000 --> 00:25:32,000
It takes you away from your problems rather than helps you deal with them.

253
00:25:32,000 --> 00:25:36,000
And as we can see, and as we see here, and we see other places,

254
00:25:36,000 --> 00:25:42,000
and as I've talked about earlier, alcohol is quite special in that.

255
00:25:42,000 --> 00:25:46,000
It removes your ability to reason.

256
00:25:46,000 --> 00:25:50,000
It leads you to squander your wealth,

257
00:25:50,000 --> 00:25:59,000
leads you to act in terribly embarrassing and unmindful ways.

258
00:25:59,000 --> 00:26:03,000
It removes your mindfulness.

259
00:26:03,000 --> 00:26:13,000
So a lot to learn from this fun, rather sad story of these two people,

260
00:26:13,000 --> 00:26:20,000
wherever they are now, they certainly getting a lesson,

261
00:26:20,000 --> 00:26:23,000
wherever that may be.

262
00:26:23,000 --> 00:26:29,000
So, there's the demo pad I've heard today. Thank you all for tuning in.

