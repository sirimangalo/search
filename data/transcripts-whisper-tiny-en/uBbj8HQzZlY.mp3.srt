1
00:00:00,000 --> 00:00:07,000
When everybody with the Dhammobi co, my understanding is that the 10 Asubha are conceptual objects.

2
00:00:07,000 --> 00:00:10,000
He is that a correct understanding.

3
00:00:10,000 --> 00:00:15,000
Another technical one, yes.

4
00:00:15,000 --> 00:00:19,000
The 10 Asubha are conceptual objects.

5
00:00:19,000 --> 00:00:23,000
How do I back that up?

6
00:00:23,000 --> 00:00:29,000
The Asubha don't really exist.

7
00:00:29,000 --> 00:00:34,000
The parts of the body are concepts.

8
00:00:34,000 --> 00:00:38,000
Both the name is a concept and the object is a concept.

9
00:00:38,000 --> 00:00:40,000
So there are two types of concepts.

10
00:00:40,000 --> 00:00:42,000
There is the name and there is the concept.

11
00:00:42,000 --> 00:00:44,000
There is the name and there is the object.

12
00:00:44,000 --> 00:00:49,000
When we say rising falling, the rising motion is ultimate reality.

13
00:00:49,000 --> 00:00:51,000
The word rising is a concept.

14
00:00:51,000 --> 00:00:59,000
So we use the rising, the word rising as a concept to bring our minds closer to the reality.

15
00:00:59,000 --> 00:01:01,000
Focus on it.

16
00:01:01,000 --> 00:01:10,000
When someone says quesa, which means hair, they are using a concept to focus on another concept.

17
00:01:10,000 --> 00:01:14,000
They are using the conceptual name, which every name is a conception.

18
00:01:14,000 --> 00:01:21,000
Focus on an object that is also conceptual.

19
00:01:21,000 --> 00:01:25,000
This is the nature of all summits and meditations.

20
00:01:25,000 --> 00:01:28,000
They don't have the power to bring about insight.

21
00:01:28,000 --> 00:01:29,000
And why don't they?

22
00:01:29,000 --> 00:01:31,000
Because hair is not impermanent.

23
00:01:31,000 --> 00:01:36,000
Yeah, intellectually you can say it is impermanent because it is going to turn gray and so on.

24
00:01:36,000 --> 00:01:39,000
But that is not impermanent in an ultimate sense.

25
00:01:39,000 --> 00:01:41,000
It is actually stable.

26
00:01:41,000 --> 00:01:44,000
If you wake up in the morning, your hair isn't falling off.

27
00:01:44,000 --> 00:01:46,000
And then you say that it is stable.

28
00:01:46,000 --> 00:01:53,000
It is actually these are the problems and the things that are standing in the way from us seeing ultimate reality.

29
00:01:53,000 --> 00:01:57,000
When you wake up in the morning, you look very much like you did the day before.

30
00:01:57,000 --> 00:02:01,000
So you make the mistake of thinking that you are the same person.

31
00:02:01,000 --> 00:02:03,000
Close your eyes, open them again.

32
00:02:03,000 --> 00:02:04,000
The hair is still there.

33
00:02:04,000 --> 00:02:08,000
So when you say quesa, quesa, quesa, quesa, you won't see impermanence.

34
00:02:08,000 --> 00:02:11,000
In fact, you will see permanence.

35
00:02:11,000 --> 00:02:12,000
You will see stability.

36
00:02:12,000 --> 00:02:16,000
The more you focus on the hair, the more stable your mind will become.

37
00:02:16,000 --> 00:02:17,000
Right?

38
00:02:17,000 --> 00:02:19,000
Which sounds great.

39
00:02:19,000 --> 00:02:23,000
But it also sounds suspiciously unlike what we are supposed to see in meditation.

40
00:02:23,000 --> 00:02:24,000
Right?

41
00:02:24,000 --> 00:02:26,000
In meditation we are supposed to see what?

42
00:02:26,000 --> 00:02:28,000
In permanence, suffering and non-so.

43
00:02:28,000 --> 00:02:31,000
When you focus on the hair, do you see that?

44
00:02:31,000 --> 00:02:32,000
No.

45
00:02:32,000 --> 00:02:37,000
You see stability, satisfaction and controllability.

46
00:02:37,000 --> 00:02:38,000
You have controlled your mind.

47
00:02:38,000 --> 00:02:39,000
Great.

48
00:02:39,000 --> 00:02:40,000
No, wonderful.

49
00:02:40,000 --> 00:02:47,000
So it is not insight meditation and it can't bring about true wisdom and understanding.

50
00:02:47,000 --> 00:02:50,000
If you do it repeatedly, it can bring out the understanding that it is futile.

51
00:02:50,000 --> 00:02:51,000
Which is good.

52
00:02:51,000 --> 00:02:53,000
That helps you come closer.

53
00:02:53,000 --> 00:02:54,000
It is not futile.

54
00:02:54,000 --> 00:03:00,000
It is a good exercise for the mind anyway that will help you to see reality in the same way.

55
00:03:00,000 --> 00:03:13,000
Because then if you then focus on the feeling of the hair, for example, or the feelings of other parts of the body, the sitting posture, or the rising and falling of the abdomen,

56
00:03:13,000 --> 00:03:19,000
using the same technique of the mantra to focus on an ultimate reality, then you will see impermanence, suffering and non-so.

57
00:03:19,000 --> 00:03:20,000
Right?

58
00:03:20,000 --> 00:03:25,000
Here is a good example for how noting is actually such a wonderful thing.

59
00:03:25,000 --> 00:03:35,000
People say noting is horrible. Why? Because you feel like you are forcing and it makes you feel awkward and it feels like you are just adding something, it is clumsy and so on and so on and all these excuses.

60
00:03:35,000 --> 00:03:40,000
But have them note, ques, loma, nakka, da-da-da-da-da-da, and ask them if they feel the same.

61
00:03:40,000 --> 00:03:42,000
Same technique, different object.

62
00:03:42,000 --> 00:03:46,000
Why is it all of a sudden permanent, satisfying and controllable?

63
00:03:46,000 --> 00:03:48,000
Why is it suddenly agreeable?

64
00:03:48,000 --> 00:03:52,000
Why do people want to say buddho, buddho, but they don't want to say rising and falling?

65
00:03:52,000 --> 00:03:55,000
My teacher gives a good talk on this.

66
00:03:55,000 --> 00:04:01,000
This is not exactly an answer to your question, but I was just listening a couple of nights ago to his talk.

67
00:04:01,000 --> 00:04:08,000
He said, why is it that people don't want to say rising and falling?

68
00:04:08,000 --> 00:04:14,000
They are really happy to say buddho, buddho, but they don't have the same faith in saying rising and falling.

69
00:04:14,000 --> 00:04:19,000
And he said it is because rising and falling is not a name of the buddho.

70
00:04:19,000 --> 00:04:24,000
People want to say buddho, buddho, because it is the name of our, that is really cool.

71
00:04:24,000 --> 00:04:26,000
It is a name of the buddha.

72
00:04:26,000 --> 00:04:30,000
And it is very mystical, the idea of the one who knows.

73
00:04:30,000 --> 00:04:34,000
And then he goes on a long talk about how actually, what is it that the buddha knew?

74
00:04:34,000 --> 00:04:36,000
And what did the buddha say about this?

75
00:04:36,000 --> 00:04:41,000
And he tells the story of wokily, who sat around looking at the buddha all day.

76
00:04:41,000 --> 00:04:43,000
And what did the buddha say to him?

77
00:04:43,000 --> 00:04:48,000
He said, why are you wasting your time looking at this rotten, filthy, stinky?

78
00:04:48,000 --> 00:04:51,000
Corpse of a body of mine.

79
00:04:51,000 --> 00:04:54,000
Why don't you go off and do something?

80
00:04:54,000 --> 00:04:58,000
He said, up he, get out of here.

81
00:04:58,000 --> 00:05:00,000
Then wokily got very upset.

82
00:05:00,000 --> 00:05:05,000
And so he started to see impermanence and became enlightened as a result.

83
00:05:05,000 --> 00:05:07,000
So, yeah.

84
00:05:07,000 --> 00:05:11,000
So, I mean, just to point out that it is the objects that are different.

85
00:05:11,000 --> 00:05:15,000
Not being the technique is exactly the same.

86
00:05:15,000 --> 00:05:23,000
And that shows how certain objects are concepts, especially the parts of the body.

87
00:05:23,000 --> 00:05:34,000
It can be used as a basis for insight, but will not directly lead to insight.

88
00:05:34,000 --> 00:05:50,000
Anybody?

