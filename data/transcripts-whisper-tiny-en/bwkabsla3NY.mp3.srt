1
00:00:00,000 --> 00:00:05,600
I have been in study and practice of Buddhist meditation since my teens.

2
00:00:05,600 --> 00:00:08,760
I have obtained a slight bit of clairvoyance.

3
00:00:08,760 --> 00:00:13,960
What I wanted to know is what are your thoughts about such mental powers as I understand

4
00:00:13,960 --> 00:00:16,920
the terabotic consider them hindrance?

5
00:00:16,920 --> 00:00:19,880
Know the teravadans do not consider them a hindrance.

6
00:00:19,880 --> 00:00:25,280
You have to consider them to be by-products.

7
00:00:25,280 --> 00:00:29,120
Find things that are not necessary for the path.

8
00:00:29,120 --> 00:00:32,840
It's not necessary to become clairvoyant, to become enlightened.

9
00:00:32,840 --> 00:00:38,920
They are a by-product of fruit, of the holy life, or the spiritual life.

10
00:00:38,920 --> 00:00:46,120
They are a fruit, recognizable, realizable, verifiable in the here and now.

11
00:00:46,120 --> 00:00:48,160
So those are my thoughts about them.

12
00:00:48,160 --> 00:00:53,280
I think they are neat things that have a great potential for creating right view and helping

13
00:00:53,280 --> 00:01:04,840
us to realize the importance of the mind, the vastness of reality beyond the material

14
00:01:04,840 --> 00:01:14,400
realm and so on, but certainly not necessary and not to be caught up, not to be caught

15
00:01:14,400 --> 00:01:22,600
up in, not to be the object of obsession or not something to let yourself get caught up

16
00:01:22,600 --> 00:01:23,600
in.

