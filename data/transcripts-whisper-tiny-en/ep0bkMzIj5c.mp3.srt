1
00:00:00,000 --> 00:00:08,500
Next question from Alex. How many times a day should one meditate? Is there a maximum amount of times per day?

2
00:00:10,400 --> 00:00:19,200
Okay, we get into the time, the issue of time in terms of how it relates to meditation.

3
00:00:21,600 --> 00:00:26,200
The easy answer to your question is that

4
00:00:26,200 --> 00:00:32,200
you should try to meditate in the morning and in the evening.

5
00:00:33,200 --> 00:00:37,200
I recommend a good way of dealing with scheduling it.

6
00:00:38,200 --> 00:00:43,200
If you have a job or a work or so on, try to do some when you wake up in the morning,

7
00:00:43,200 --> 00:00:49,200
some before you sleep at night. If you have a break during the day or during the days that you don't work,

8
00:00:49,200 --> 00:00:58,200
don't go to school, meditate in the afternoon as well or try to take breaks, five minutes here, five minutes there,

9
00:00:58,200 --> 00:01:00,200
and do a short meditation.

10
00:01:01,200 --> 00:01:05,200
When you're doing an intensive meditation course,

11
00:01:07,200 --> 00:01:13,200
of course, that increases and you would be doing five or six rounds a day,

12
00:01:13,200 --> 00:01:19,200
which would be one hour walking one hour sitting. Once you got into it, once you started getting up there,

13
00:01:19,200 --> 00:01:22,200
so you might even do 10 or 12 hours of meditation a day.

14
00:01:23,200 --> 00:01:27,200
You might even do more than that. There are people when they practice the practice all day and all night,

15
00:01:27,200 --> 00:01:31,200
back and forth walking, sitting, walking, sitting with breaks in between,

16
00:01:31,200 --> 00:01:37,200
so you might do even 16 hours of meditation or more in the 24 hour period.

17
00:01:37,200 --> 00:01:48,200
The more important thing to talk about is how unimportant this all is.

18
00:01:48,200 --> 00:01:55,200
That formal meditation is really only a formality or a structure,

19
00:01:55,200 --> 00:02:03,200
a way of simplifying the process of meditation,

20
00:02:03,200 --> 00:02:09,200
in the same way that training wheels are a way of simplifying the process of learning how to ride a bike.

21
00:02:09,200 --> 00:02:13,200
Once you get better at the meditation, the answer to your question really is,

22
00:02:13,200 --> 00:02:18,200
you should be practicing meditation thousands and thousands of times every day,

23
00:02:18,200 --> 00:02:25,200
which means every moment that you remember to create this clear thought

24
00:02:25,200 --> 00:02:31,200
and to examine things or understand things as they are at every moment

25
00:02:31,200 --> 00:02:35,200
when you feel pain in the body, pain, pain, when you're walking, walking,

26
00:02:35,200 --> 00:02:39,200
when you're thinking, thinking, thinking, when you're liking,

27
00:02:39,200 --> 00:02:43,200
just liking, angry, frustrated, bored, worried, sad, doubting, tired,

28
00:02:43,200 --> 00:02:47,200
so on. Everything that you do when you brush your teeth,

29
00:02:47,200 --> 00:02:49,200
when you eat your food, chewing, chewing, swallowing,

30
00:02:49,200 --> 00:02:53,200
every time you make this clear thought in your mind, that's meditation,

31
00:02:53,200 --> 00:02:57,200
so that's one meditation, so you count that.

32
00:02:57,200 --> 00:03:03,200
I'm not just being silly, this is how it's been explained

33
00:03:03,200 --> 00:03:09,200
that if you imagine enlightenment to be 10,000 acknowledgements away,

34
00:03:09,200 --> 00:03:13,200
then you just count them. How many times have I been mindful?

35
00:03:13,200 --> 00:03:17,200
You can imagine it in that sense anyway.

36
00:03:17,200 --> 00:03:21,200
Of course, if you're counting, you have to say counting, counting, or thinking, thinking,

37
00:03:21,200 --> 00:03:25,200
so that's not really the point, but you can really think of it

38
00:03:25,200 --> 00:03:29,200
in terms of how many times you're being mindful, and you should be doing that

39
00:03:29,200 --> 00:03:33,200
at your peak, you should be able to be doing that to yourself

40
00:03:33,200 --> 00:03:37,200
once every step, on average once every second.

41
00:03:37,200 --> 00:03:41,200
Every second you're saying, bending, stretching,

42
00:03:41,200 --> 00:03:45,200
moving, touching, chewing, swallowing, brushing,

43
00:03:45,200 --> 00:03:49,200
when you're in the washroom, when you're in the shower, and so on.

44
00:03:49,200 --> 00:03:54,200
Whatever you do, creating the clear thought in that sense,

45
00:03:54,200 --> 00:03:59,200
it doesn't matter how many times you meditate formally during the day,

46
00:03:59,200 --> 00:04:04,200
when you learn how to do this, when you get better at this,

47
00:04:04,200 --> 00:04:08,200
then you're meditating all day. You do a formal practice in the morning,

48
00:04:08,200 --> 00:04:11,200
then when you get started going to work, walking, walking, you're sitting in your garden,

49
00:04:11,200 --> 00:04:17,200
sitting, sitting, turning, seeing, shouting, whatever,

50
00:04:17,200 --> 00:04:22,200
as the day goes by, being mindful of everything that arises.

51
00:04:22,200 --> 00:04:25,200
So I hope that helps. Thanks for the question.

