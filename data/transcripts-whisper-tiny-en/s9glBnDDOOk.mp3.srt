1
00:00:00,000 --> 00:00:06,680
There's a question, is it against the Vinaya to put all of your worldly belongings in someone

2
00:00:06,680 --> 00:00:12,600
else's trust and become a monk for five years? The understanding being, if one decides

3
00:00:12,600 --> 00:00:17,680
to remain in the order, the property will go to that person, or if one disrupts one

4
00:00:17,680 --> 00:00:43,640
can resume lay life? Good question, go for it. It's not against the words, it's probably

5
00:00:43,640 --> 00:00:56,440
not against the letter, but probably against the sins, probably against the meaning

6
00:00:56,440 --> 00:01:10,040
of what the rules are for. It's kind of like retroactively breaking the rule.

7
00:01:10,040 --> 00:01:23,760
It's like suppose, suppose I say to Tardindu, when I become a monk, I want you to kill

8
00:01:23,760 --> 00:01:28,880
my preceptor so that I can do what I want, because once my preceptor is dead, I don't

9
00:01:28,880 --> 00:01:34,840
have to stay with him, I don't have to listen to him, right? And then I ordain. If Tardindu

10
00:01:34,840 --> 00:01:39,520
goes and kills that person, if I committed a Barajika, because killing people is of course

11
00:01:39,520 --> 00:01:47,480
against the Vinaya to the extent that you're no longer a monk. So the question is, am

12
00:01:47,480 --> 00:01:51,960
I still a monk? If he goes and kills my preceptor? Well, according to the letter of the

13
00:01:51,960 --> 00:02:00,880
Vinaya, yes, I am still a monk, because I didn't do it after I ordained. But does it make

14
00:02:00,880 --> 00:02:08,480
it any better? Does it make it at all? Does it make it at all different?

15
00:02:08,480 --> 00:02:15,480
Yeah, you could argue, you could argue that a monk committing an offense is more he-niz-he-niz-he-niz,

16
00:02:15,480 --> 00:02:26,480
or the fact that you did it in monk's robes. It makes it a lot worse. But it's certainly

17
00:02:26,480 --> 00:02:32,480
not correct. So the point being, well, money affairs is not unwholesome like killing,

18
00:02:32,480 --> 00:02:39,480
but it's still a mockery of sort of in the sense of the monastic life. I think you have

19
00:02:39,480 --> 00:02:44,480
to say it is making a mockery of the monastic life. It's trying to circumvent the rules,

20
00:02:44,480 --> 00:02:51,480
really. You say, oh, I know I can't use money when I become a monk, so I'll fudge it.

21
00:02:51,480 --> 00:02:55,600
I'll give money to someone and I'll say, okay, every month you have to make a contract

22
00:02:55,600 --> 00:03:01,080
with me that every month you are going to give me $1,000, or no, you're going to give

23
00:03:01,080 --> 00:03:07,080
me, not every month. Every day you're going to give me food, or you go to pizza hut,

24
00:03:07,080 --> 00:03:12,080
for example, and you say to them, you know, I've won a right of contract with you that every

25
00:03:12,080 --> 00:03:18,080
day for the rest of my life, you're going to send me pizza to my monastery. And you're

26
00:03:18,080 --> 00:03:22,080
not breaking any rule by doing that. You're not a monk yet. When you are a day and you're

27
00:03:22,080 --> 00:03:28,080
not breaking any rule. But the reason why you're doing that with them is because you know

28
00:03:28,080 --> 00:03:32,080
you won't be able to do that once you're a monk. So it's making a mockery of the rule.

29
00:03:32,080 --> 00:03:38,080
The rule is not for the purpose of making it difficult for you to get what you want.

30
00:03:38,080 --> 00:03:43,080
It's for the purpose of helping you to let go of wanting and let go of clinging. Because

31
00:03:43,080 --> 00:03:48,080
what you're saying here is that you want to ordain but still cling. You want to give up

32
00:03:48,080 --> 00:03:54,080
the world without actually giving it up, or like give it up but keep one hand in the

33
00:03:54,080 --> 00:04:07,080
spot. Which is totally improper. So I think I would have to say that it's improper for

34
00:04:07,080 --> 00:04:14,080
that reason because it's missing the whole point of the rules. It's making a mockery of the

35
00:04:14,080 --> 00:04:25,080
rules and to just a nuisance rather than a tool. I mean you could have, I was surprised by

36
00:04:25,080 --> 00:04:29,080
the end of any other question. I didn't read it so I thought it was just going to be if I set

37
00:04:29,080 --> 00:04:34,080
up a fund so that if I need anything I can get it from these people. That's one question

38
00:04:34,080 --> 00:04:39,080
that we often have to ask. If I set up a fund for these people in case as a monk I get

39
00:04:39,080 --> 00:04:46,080
sick or so on. Is that wrong? But here you're even worse because you're thinking with the

40
00:04:46,080 --> 00:04:51,080
intention that in five years I might be able to go back to the home life. That's not giving

41
00:04:51,080 --> 00:04:59,080
up the world at all really. Oh yes it is and it's a good intention that you have but I

42
00:04:59,080 --> 00:05:05,080
would say it's not sufficient. Becoming a monk is a really serious thing. It's not something

43
00:05:05,080 --> 00:05:10,080
that we should take lightly. It's not something that can come lightly or can come easily.

44
00:05:10,080 --> 00:05:16,080
Any little attachment that you have will come back to haunt you a thousand-fold once you've

45
00:05:16,080 --> 00:05:24,080
ordained. It will even get in the way of your ordaining. Polyniani for example, not in this

46
00:05:24,080 --> 00:05:30,080
regard but just as an example of how difficult it is to ordain, what not to become a

47
00:05:30,080 --> 00:05:38,080
kuni, but just to become a michi, to become a nun, sorry an eight precept nun. It was

48
00:05:38,080 --> 00:05:43,080
amazing the things that got in the way. When it's not ready to happen it will not happen

49
00:05:43,080 --> 00:05:48,080
and there was everything, everything inspired against us until finally this monk won't do it.

50
00:05:48,080 --> 00:05:54,080
That monk won't do it. We'll do it, we'll do it, we'll do it. Until finally we had this

51
00:05:54,080 --> 00:05:58,080
head monk's approval that I could do it. He's like well you can just do it and do it in my

52
00:05:58,080 --> 00:06:05,080
name and then all sign and write her this nun card, ID card. So we got back to the monastery

53
00:06:05,080 --> 00:06:10,080
and we're going to ready to perform this ceremony and they say okay we perform this ceremony

54
00:06:10,080 --> 00:06:16,080
all right and pull out your robes. She pulls out her robes two lower robes, two skirts.

55
00:06:16,080 --> 00:06:20,080
We pull them out of one package, one package is the upper robe. Turns out they're both

56
00:06:20,080 --> 00:06:26,080
skirts. She didn't have enough robes to her. It was ridiculous. So I went to my room

57
00:06:26,080 --> 00:06:32,080
and in the corner there was this old bed sheet. I was crumpled up and it wasn't even

58
00:06:32,080 --> 00:06:38,080
washed. I picked it up. It's white and I said here this is your upper robe.

59
00:06:38,080 --> 00:06:47,080
It's so her first robe. I said at that time that I never thought that I would end up

60
00:06:47,080 --> 00:07:05,080
in your bed sheets. I actually didn't find it that difficult

61
00:07:05,080 --> 00:07:12,080
toward a nun. This is funny how it all conspired. But there are actually

62
00:07:12,080 --> 00:07:20,080
nuns who want it to be a fully ordained nun or a bikini for 20 years or more and

63
00:07:20,080 --> 00:07:34,080
they just couldn't. If you look from it from that angle, my case was quick.

64
00:07:34,080 --> 00:07:42,080
And the other point is to reiterate that after your day and it's even worse, if you're still

65
00:07:42,080 --> 00:07:47,080
clinging to something, forget it, everything will come back to you. Whether you've treated

66
00:07:47,080 --> 00:07:58,080
your parents badly, whether you're still clinging to your family and to relatives,

67
00:07:58,080 --> 00:08:04,080
you really have to take it seriously. Otherwise it's quite meaningless.

68
00:08:04,080 --> 00:08:10,080
And even if you do stay as a monkey, find it. It's a waste of time.

69
00:08:10,080 --> 00:08:14,080
Clear everything up before your day and be sure about it before your day.

70
00:08:14,080 --> 00:08:19,080
Like really give up so that you say well, and even if it doesn't work out,

71
00:08:19,080 --> 00:08:24,080
I'll die before I go back to the late life. It really should be to that extent.

72
00:08:24,080 --> 00:08:30,080
It should be ready to give up everything.

73
00:08:30,080 --> 00:08:42,080
I agree. I agree with this to a large extent and I hope I will die in this

74
00:08:42,080 --> 00:08:55,080
ropes. But I want to say that sometimes it is better to be a monk for five years

75
00:08:55,080 --> 00:09:04,080
or even for three months than not to be a monk at all.

76
00:09:04,080 --> 00:09:12,080
It's true. The idea of the temporary ordination is interesting because obviously there's many places

77
00:09:12,080 --> 00:09:17,080
that do allow you to ordain temporarily with full knowledge that you're going back

78
00:09:17,080 --> 00:09:23,080
to these things, these luxuries and so on. People who are married who come

79
00:09:23,080 --> 00:09:27,080
and ordain for three months because their wife sends them. Go to the monastery for three

80
00:09:27,080 --> 00:09:36,080
months and become a good husband. Learn how to behave, go and be a monk.

81
00:09:36,080 --> 00:09:41,080
So the problem is as I said and as I've said before,

82
00:09:41,080 --> 00:09:45,080
another place as well is that it kind of makes a mockery of the monk's life.

83
00:09:45,080 --> 00:09:50,080
It's the same argument as monks should be able to get involved in politics.

84
00:09:50,080 --> 00:09:55,080
Why? Because monks can be really good politicians, but it's not appropriate.

85
00:09:55,080 --> 00:10:02,080
This is diluting. This is observable.

86
00:10:02,080 --> 00:10:09,080
I've been very closely involved in ordaining temporary monks again and again and again.

87
00:10:09,080 --> 00:10:18,080
All it ends up doing is the long-term monks wind up orienting their life around it

88
00:10:18,080 --> 00:10:24,080
and their livelihood as well because you can make a lot of money off of the temporary ordinations.

89
00:10:24,080 --> 00:10:31,080
They become totally overworked to be they have no time to meditate because they're spending all their time teaching new monks.

90
00:10:31,080 --> 00:10:37,080
It's really difficult just to teach one person to be a good monk is an incredible task

91
00:10:37,080 --> 00:10:45,080
and not alone teaching that person how to meditate, just teaching them the rudiments of them monastically.

92
00:10:45,080 --> 00:10:50,080
So what happens is they don't and therefore the monks don't practice correctly

93
00:10:50,080 --> 00:10:54,080
spend their three months as poor monks breaking rules and so on.

94
00:10:54,080 --> 00:11:00,080
So in the end, yeah, it's great for them that they've come and they've been able to do a lot of meditation or hopefully

95
00:11:00,080 --> 00:11:09,080
they've been able to do some meditation and give some things up, but it really is a corruption of the monastic organization.

96
00:11:09,080 --> 00:11:16,080
If they do decide, if they have the best intentions and as you say after five months, after five years or whatever,

97
00:11:16,080 --> 00:11:24,080
they realize it's not for them and decide to go back to their late life, then there's nothing wrong with that.

98
00:11:24,080 --> 00:11:35,080
But to hedge your bets is that would just and keep some stash just in case you change your mind.

99
00:11:35,080 --> 00:11:43,080
Look, you're not ready. If it's like that, then keep meditating.

100
00:11:43,080 --> 00:11:47,080
I would say, only because I want to keep the monastic life pure.

101
00:11:47,080 --> 00:11:51,080
And certainly I'm a hypocrite in the sense that I wasn't like that.

102
00:11:51,080 --> 00:11:57,080
I ordained with really the best of it. I never intended to destroy it, but I wasn't ready for it.

103
00:11:57,080 --> 00:12:06,080
I'm not a hypocrite, but in general, I would say clear things up because not everything was cleared up for me.

104
00:12:06,080 --> 00:12:11,080
But really, I didn't have that intention. When I ordained, it was a one-way path for me.

105
00:12:11,080 --> 00:12:15,080
There was no question whether I was going to, I might just robe it sometime in the future.

106
00:12:15,080 --> 00:12:23,080
Of course, I know you never can say, you never can say, but I've never had the worry, what will I do if I just robe?

107
00:12:23,080 --> 00:12:29,080
I know if I just robe, then maybe it'll be really difficult in the end, it'll only be seeing, hearing, smelling, tasting, feeling.

108
00:12:29,080 --> 00:12:34,080
And thinking, whether I'm living on the street, I mean, I've lived on the street as a monk.

109
00:12:34,080 --> 00:12:41,080
I've lived under a park bench and under a picnic table and gone without food and worn rags.

110
00:12:41,080 --> 00:12:45,080
So if that's what it's going to be when I just robe, then that's what it's going to be.

111
00:12:45,080 --> 00:12:57,080
But if you're not ready for that, I would say it's not proper for you to consider to ordained.

112
00:12:57,080 --> 00:13:01,080
Maybe that's too harsh, but that's what I would like to say.

113
00:13:01,080 --> 00:13:08,080
I'm a harsh sort of monk.

114
00:13:08,080 --> 00:13:17,080
All right.

