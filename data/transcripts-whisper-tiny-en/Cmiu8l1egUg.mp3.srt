1
00:00:00,000 --> 00:00:05,000
Hi, and welcome back to our study of the Dhamupada.

2
00:00:05,000 --> 00:00:12,000
Today we continue on with verse number 62, which reads as follows.

3
00:00:35,000 --> 00:00:37,000
I have sons.

4
00:00:37,000 --> 00:00:42,000
Dhanamati, I have wealth.

5
00:00:42,000 --> 00:00:44,000
Iti balo yanyati.

6
00:00:44,000 --> 00:00:52,000
Thus, the fool worries himself for his backstories.

7
00:00:52,000 --> 00:00:57,000
His kadap is concerned,

8
00:00:57,000 --> 00:01:00,000
he is distraught by these thoughts.

9
00:01:00,000 --> 00:01:09,000
Atahi, atanonati, one self indeed is not one self.

10
00:01:09,000 --> 00:01:12,000
kutto, kutto, kutto, dhanang.

11
00:01:12,000 --> 00:01:24,000
How, therefore, could either sons or wealth belong to one self?

12
00:01:24,000 --> 00:01:34,000
The story behind the verse, story goes that there was this rich man called Anandat.

13
00:01:34,000 --> 00:01:41,000
Anandati, rich man, dhanandat.

14
00:01:41,000 --> 00:01:45,000
And he taught his children.

15
00:01:45,000 --> 00:01:50,000
He lived his life and taught his children to not spend anything,

16
00:01:50,000 --> 00:01:54,000
never give away anything,

17
00:01:54,000 --> 00:01:59,000
because wealth is difficult to gain.

18
00:01:59,000 --> 00:02:01,000
It's hard to hold onto,

19
00:02:01,000 --> 00:02:04,000
and it can vanish at any time,

20
00:02:04,000 --> 00:02:07,000
so you have to work very, very hard to keep it,

21
00:02:07,000 --> 00:02:13,000
and keep every coin accounted for.

22
00:02:13,000 --> 00:02:18,000
So, over his years of cultivating wealth

23
00:02:18,000 --> 00:02:26,000
and obsessing over wealth, he came to view it as

24
00:02:26,000 --> 00:02:29,000
something that needed protecting.

25
00:02:29,000 --> 00:02:33,000
He got so wound up that every coin should be accounted for

26
00:02:33,000 --> 00:02:36,000
and should never give anything to anyone.

27
00:02:36,000 --> 00:02:43,000
To never give away or foolishly spend your wealth.

28
00:02:43,000 --> 00:02:48,000
Reminds us of the rich man with the pancakes,

29
00:02:48,000 --> 00:02:50,000
but no, this is a different man.

30
00:02:50,000 --> 00:02:52,000
It seems to be that wealth,

31
00:02:52,000 --> 00:03:00,000
there's a common theme in the polycan in the rich richness,

32
00:03:00,000 --> 00:03:03,000
often leading to miserliness,

33
00:03:03,000 --> 00:03:10,000
which of course you see in modern day as well.

34
00:03:10,000 --> 00:03:13,000
But there are some examples of rich people

35
00:03:13,000 --> 00:03:16,000
who are very generous and kinder.

36
00:03:16,000 --> 00:03:19,000
How many rich people who can only think about

37
00:03:19,000 --> 00:03:21,000
how they can amass more wealth?

38
00:03:21,000 --> 00:03:22,000
You can get more and more.

39
00:03:22,000 --> 00:03:26,000
This is what the Buddha said for this reason,

40
00:03:26,000 --> 00:03:31,000
even if it were to rain gold.

41
00:03:31,000 --> 00:03:36,000
You would never get enough for even one person.

42
00:03:36,000 --> 00:03:40,000
So this is story of Anandan, not much of a story

43
00:03:40,000 --> 00:03:44,000
because he passed away and left his fortune to his son.

44
00:03:44,000 --> 00:03:47,000
And the story is actually about his reincarnation.

45
00:03:47,000 --> 00:03:50,000
When he was reborn, he was reborn as an outcast.

46
00:03:50,000 --> 00:03:55,000
In a village of, it says, around a thousand outcast

47
00:03:55,000 --> 00:04:07,000
or beggars, or for unemployed outcasts,

48
00:04:07,000 --> 00:04:09,000
or unable, of course, to get any,

49
00:04:09,000 --> 00:04:11,000
but the worst of jobs,

50
00:04:11,000 --> 00:04:14,000
and had to live either off begging or off slavery

51
00:04:14,000 --> 00:04:17,000
and slave labor, et cetera.

52
00:04:17,000 --> 00:04:21,000
Now, from the moment that he was conceived in his mother's womb,

53
00:04:21,000 --> 00:04:27,000
the entire village was unable to get anything

54
00:04:27,000 --> 00:04:33,000
to make even the slightest bit of money or food.

55
00:04:33,000 --> 00:04:35,000
They weren't able to beg.

56
00:04:35,000 --> 00:04:37,000
They weren't able to work.

57
00:04:37,000 --> 00:04:39,000
From the moment that he was conceived,

58
00:04:39,000 --> 00:04:41,000
he cursed the whole village.

59
00:04:41,000 --> 00:04:44,000
So the story goes.

60
00:04:44,000 --> 00:04:49,000
Some kind of group karma and work, I guess.

61
00:04:49,000 --> 00:04:54,000
And so they split the village.

62
00:04:54,000 --> 00:04:56,000
They got together and they said,

63
00:04:56,000 --> 00:04:58,000
something must be wrong here.

64
00:04:58,000 --> 00:04:59,000
Someone must be causing this.

65
00:04:59,000 --> 00:05:02,000
And so they split the village in half

66
00:05:02,000 --> 00:05:06,000
and had them go in separate locations

67
00:05:06,000 --> 00:05:08,000
and go begging or go working or whatever.

68
00:05:08,000 --> 00:05:12,000
And they found that one half was able to get,

69
00:05:12,000 --> 00:05:16,000
to get by fine as per normal.

70
00:05:16,000 --> 00:05:20,000
The other group still was as it were cursed.

71
00:05:20,000 --> 00:05:23,000
And so they cut that group in half

72
00:05:23,000 --> 00:05:26,000
and then cut the other group in half

73
00:05:26,000 --> 00:05:29,000
and then half again until they got down to two families

74
00:05:29,000 --> 00:05:30,000
and they split them up.

75
00:05:30,000 --> 00:05:33,000
And they found that this woman with this pregnant woman

76
00:05:33,000 --> 00:05:35,000
was the cause of all the problems.

77
00:05:35,000 --> 00:05:41,000
So they kicked them out and sent them on their way.

78
00:05:41,000 --> 00:05:47,000
And then when he was born, his parents found the same thing

79
00:05:47,000 --> 00:05:50,000
that if they went on arms alone

80
00:05:50,000 --> 00:05:52,000
or if they went out working or whatever alone

81
00:05:52,000 --> 00:05:54,000
then they could get money just fine

82
00:05:54,000 --> 00:05:59,000
and they could get by as well as could be expected.

83
00:05:59,000 --> 00:06:02,000
But if he was with them, they would all get nothing.

84
00:06:02,000 --> 00:06:04,000
So they kicked him out as well

85
00:06:04,000 --> 00:06:10,000
and gave him a little piece of a piece of pot or something

86
00:06:10,000 --> 00:06:17,000
and said, go, you must spare for yourself.

87
00:06:17,000 --> 00:06:19,000
They kept him around until he was like seven years old

88
00:06:19,000 --> 00:06:22,000
and then they sent him off and let's go by yourself.

89
00:06:22,000 --> 00:06:24,000
The other thing is he was very, very ugly.

90
00:06:24,000 --> 00:06:26,000
So he kind of looked like an ogre.

91
00:06:26,000 --> 00:06:34,000
And this is the horrible repercussions of being such a miser.

92
00:06:34,000 --> 00:06:39,000
And one day he was wandering around trying to get whatever

93
00:06:39,000 --> 00:06:42,000
or whatever arms he could, which of course wasn't much.

94
00:06:42,000 --> 00:06:46,000
He wandered back to his old home and he saw his house

95
00:06:46,000 --> 00:06:48,000
and he recognized it.

96
00:06:48,000 --> 00:06:50,000
And so he just walked right in.

97
00:06:50,000 --> 00:06:52,000
And he started looking around the house,

98
00:06:52,000 --> 00:06:53,000
not quite sure why he was there,

99
00:06:53,000 --> 00:06:55,000
but I looked at him and looked very familiar

100
00:06:55,000 --> 00:06:56,000
and kind of like home.

101
00:06:56,000 --> 00:06:58,000
So he was walking around and he went into one room

102
00:06:58,000 --> 00:07:02,000
and there were his grandchildren, his son's sons.

103
00:07:02,000 --> 00:07:04,000
And they freaked out.

104
00:07:04,000 --> 00:07:06,000
And they called out and they said,

105
00:07:06,000 --> 00:07:09,000
there's a monster in there and the servants came over

106
00:07:09,000 --> 00:07:13,000
and beat him and threw him out.

107
00:07:13,000 --> 00:07:15,000
Just as the Buddha was walking by.

108
00:07:15,000 --> 00:07:17,000
So the Buddha's walking by going on arms around

109
00:07:17,000 --> 00:07:22,000
and he sees this young beggar beaten to a pulp

110
00:07:22,000 --> 00:07:25,000
or beaten quite severely,

111
00:07:25,000 --> 00:07:27,000
lying on the side of the road.

112
00:07:27,000 --> 00:07:28,000
Not to a pulp.

113
00:07:28,000 --> 00:07:29,000
He's still alive.

114
00:07:29,000 --> 00:07:32,000
He's beaten up and the Buddha looks at him

115
00:07:32,000 --> 00:07:35,000
and then he looks at Ananda, who is our Ananda.

116
00:07:35,000 --> 00:07:38,000
The rich Ananda walking beside him.

117
00:07:38,000 --> 00:07:40,000
He turns a look at Ananda.

118
00:07:40,000 --> 00:07:49,000
Ananda knows to take this as an instigation.

119
00:07:49,000 --> 00:07:52,000
To ask the question.

120
00:07:52,000 --> 00:07:54,000
So he asked the Buddha,

121
00:07:54,000 --> 00:07:59,000
what's the story of this guy, Reverend Sir?

122
00:07:59,000 --> 00:08:03,000
And the Buddha said he used to be the great rich man Ananda.

123
00:08:03,000 --> 00:08:06,000
And this was his house.

124
00:08:06,000 --> 00:08:11,000
Ananda called the rich man's son.

125
00:08:11,000 --> 00:08:14,000
And the Buddha explained to him,

126
00:08:14,000 --> 00:08:16,000
this was your father.

127
00:08:16,000 --> 00:08:18,000
And he said, I don't believe it looks at him

128
00:08:18,000 --> 00:08:20,000
and he's this ugly outcast.

129
00:08:20,000 --> 00:08:24,000
And he said, how can he go from a rich man to a beggar

130
00:08:24,000 --> 00:08:26,000
and the Buddha had him go in the house

131
00:08:26,000 --> 00:08:28,000
and find out all his treasures.

132
00:08:28,000 --> 00:08:30,000
And the kid was able to actually remember

133
00:08:30,000 --> 00:08:33,000
where everything was and so he proved it.

134
00:08:33,000 --> 00:08:35,000
And then the Buddha taught this verse.

135
00:08:35,000 --> 00:08:37,000
Simple story.

136
00:08:37,000 --> 00:08:41,000
And the point here is to remind us

137
00:08:41,000 --> 00:08:43,000
not to be negligent.

138
00:08:43,000 --> 00:08:45,000
It's a simple lesson.

139
00:08:45,000 --> 00:08:48,000
Let me think of material possessions.

140
00:08:48,000 --> 00:08:51,000
All of those things, not just put them down that

141
00:08:51,000 --> 00:08:53,000
when our sons and our wealth,

142
00:08:53,000 --> 00:08:55,000
but everything.

143
00:08:55,000 --> 00:09:00,000
And look at all of our belongings, all of our

144
00:09:00,000 --> 00:09:04,000
in-joyments in the material realm,

145
00:09:04,000 --> 00:09:07,000
thinking of them as me as mine,

146
00:09:07,000 --> 00:09:09,000
as somehow controllable.

147
00:09:09,000 --> 00:09:14,000
So we either hold on to them as ours to enjoy

148
00:09:14,000 --> 00:09:16,000
or we hold on to ours to control

149
00:09:16,000 --> 00:09:21,000
or ours to own or ours to dwell in.

150
00:09:21,000 --> 00:09:23,000
Or our body, we think of it as ourselves

151
00:09:23,000 --> 00:09:25,000
or house, we think of it as ourselves

152
00:09:25,000 --> 00:09:27,000
or car or bedroom, everything.

153
00:09:27,000 --> 00:09:30,000
All of our family, we think of them as our family

154
00:09:30,000 --> 00:09:33,000
or friends, we think of our friends.

155
00:09:33,000 --> 00:09:39,000
And so we get caught up in this habit

156
00:09:39,000 --> 00:09:43,000
of expectation of being able to control

157
00:09:43,000 --> 00:09:45,000
of being able to rely upon,

158
00:09:45,000 --> 00:09:49,000
of being able to enjoy all of these things.

159
00:09:49,000 --> 00:09:52,000
And the Buddha said, even yourself is not yourself.

160
00:09:52,000 --> 00:09:56,000
It's funny, the more common one that people

161
00:09:56,000 --> 00:09:58,000
know is that he had to know not to,

162
00:09:58,000 --> 00:10:01,000
which is, he wonder whether one of them is actually,

163
00:10:01,000 --> 00:10:04,000
or the other one is actually,

164
00:10:04,000 --> 00:10:06,000
but anyway, the Buddha taught both ways.

165
00:10:06,000 --> 00:10:09,000
At the he had to know not to himself is a refuge of self,

166
00:10:09,000 --> 00:10:13,000
which means one is one's own refuge.

167
00:10:13,000 --> 00:10:15,000
But that's referring to the force at Epitana

168
00:10:15,000 --> 00:10:18,000
when it makes a refuge by practicing

169
00:10:18,000 --> 00:10:22,000
on one's own, not relying on anyone else.

170
00:10:22,000 --> 00:10:28,000
But even that one can control one can't rely upon

171
00:10:28,000 --> 00:10:31,000
one's expectations, one can't be fulfilled

172
00:10:31,000 --> 00:10:39,000
in one's desires and in one's demands.

173
00:10:39,000 --> 00:10:49,000
So, as a result, itimbalo yanyati, a fool is vexed by these thoughts.

174
00:10:49,000 --> 00:10:51,000
They think of their children and their loved ones

175
00:10:51,000 --> 00:10:54,000
and all of the people in their life

176
00:10:54,000 --> 00:10:56,000
as being controllable in them

177
00:10:56,000 --> 00:10:59,000
and they're not able to control these things,

178
00:10:59,000 --> 00:11:02,000
throw these people, then they suffer.

179
00:11:02,000 --> 00:11:04,000
They're vexed, they're worried all the time,

180
00:11:04,000 --> 00:11:06,000
worried about how they might be

181
00:11:06,000 --> 00:11:09,000
and trying to figure out ways to control people,

182
00:11:09,000 --> 00:11:11,000
ways to control their family,

183
00:11:11,000 --> 00:11:14,000
ways to control their friends, ways to control their employers,

184
00:11:14,000 --> 00:11:18,000
their employees, their co-workers, and so on and so on.

185
00:11:18,000 --> 00:11:22,000
How can I control people to be the way I want them to be

186
00:11:22,000 --> 00:11:24,000
and the same goes with our done now,

187
00:11:24,000 --> 00:11:26,000
our possessions are well.

188
00:11:26,000 --> 00:11:28,000
All of our belongings, guarding our houses,

189
00:11:28,000 --> 00:11:31,000
guarding our valuables,

190
00:11:31,000 --> 00:11:36,000
guarding our possessions, guarding our enjoyments,

191
00:11:36,000 --> 00:11:41,000
regarding our own bodies, being careful

192
00:11:41,000 --> 00:11:44,000
to make sure that we can always enjoy the pleasure

193
00:11:44,000 --> 00:11:46,000
that we enjoy now.

194
00:11:46,000 --> 00:11:48,000
But none of this can be controlled

195
00:11:48,000 --> 00:11:51,000
and when it is out of our control,

196
00:11:51,000 --> 00:11:53,000
we're vexed by it.

197
00:11:53,000 --> 00:11:56,000
Especially foolish people who really believe

198
00:11:56,000 --> 00:11:58,000
that these things are going to make them happy.

199
00:11:58,000 --> 00:12:01,000
People who believe in the world bringing happiness

200
00:12:01,000 --> 00:12:03,000
wind up being vexed quite often.

201
00:12:03,000 --> 00:12:07,000
And because of their lack of wisdom,

202
00:12:07,000 --> 00:12:08,000
they're not able to see this.

203
00:12:08,000 --> 00:12:11,000
And so as a result, they constantly vexed.

204
00:12:11,000 --> 00:12:14,000
They do it again and again and again.

205
00:12:14,000 --> 00:12:16,000
Doing the same thing over and over again,

206
00:12:16,000 --> 00:12:18,000
expecting different results.

207
00:12:18,000 --> 00:12:22,000
Forgetting that they are constantly upset.

208
00:12:22,000 --> 00:12:24,000
The Buddha reminds us,

209
00:12:24,000 --> 00:12:26,000
even ourselves is not ourselves.

210
00:12:26,000 --> 00:12:28,000
Even what this means is,

211
00:12:28,000 --> 00:12:30,000
we can't even control ourselves.

212
00:12:30,000 --> 00:12:33,000
We can make choices, we can have intentions,

213
00:12:33,000 --> 00:12:35,000
but we can't really control it.

214
00:12:35,000 --> 00:12:39,000
We can't say, I'm going to be happy all the time

215
00:12:39,000 --> 00:12:42,000
or I'm going to be calm all the time.

216
00:12:42,000 --> 00:12:45,000
We can't say that we're never going to feel pain

217
00:12:45,000 --> 00:12:46,000
or we're not going to get angry

218
00:12:46,000 --> 00:12:48,000
or we're not going to get upset.

219
00:12:48,000 --> 00:12:51,000
We can't say this.

220
00:12:51,000 --> 00:12:53,000
Even about our own minds,

221
00:12:53,000 --> 00:12:56,000
even about our own being.

222
00:12:56,000 --> 00:12:58,000
So how then could we possibly do this?

223
00:12:58,000 --> 00:13:01,000
How then could we possibly hold on to these things?

224
00:13:01,000 --> 00:13:11,000
How then could we expect that we're going to enjoy the

225
00:13:11,000 --> 00:13:13,000
possession of these things forever?

226
00:13:13,000 --> 00:13:16,000
How could we possibly forget that we're going to lose them all

227
00:13:16,000 --> 00:13:17,000
when we die?

228
00:13:17,000 --> 00:13:20,000
And this is what Anandasati, this guy,

229
00:13:20,000 --> 00:13:23,000
didn't realize.

230
00:13:23,000 --> 00:13:28,000
And really the most impressive part of this story

231
00:13:28,000 --> 00:13:30,000
is how quickly he was gone.

232
00:13:30,000 --> 00:13:31,000
This is what shocked his son.

233
00:13:31,000 --> 00:13:33,000
He was unable to believe that.

234
00:13:33,000 --> 00:13:34,000
Just like that.

235
00:13:34,000 --> 00:13:38,000
One moment, the moment of death turned him from a rich,

236
00:13:38,000 --> 00:13:44,000
powerful, influential person

237
00:13:44,000 --> 00:13:47,000
to a nobody, to the lowest of the low

238
00:13:47,000 --> 00:13:51,000
outcast of the outcast.

239
00:13:51,000 --> 00:13:53,000
But that's the truth.

240
00:13:53,000 --> 00:13:55,000
That's what the Buddha means by its not self.

241
00:13:55,000 --> 00:13:57,000
You can't even hold on to your own being,

242
00:13:57,000 --> 00:13:59,000
your own status.

243
00:13:59,000 --> 00:14:00,000
I think I'm a monk.

244
00:14:00,000 --> 00:14:03,000
The moment I die, that's gone.

245
00:14:03,000 --> 00:14:05,000
You think you're a human being.

246
00:14:05,000 --> 00:14:06,000
The moment you die, that's gone.

247
00:14:06,000 --> 00:14:07,000
You can be.

248
00:14:07,000 --> 00:14:11,000
The next moment you can be an earthworm or a dung beetle.

249
00:14:11,000 --> 00:14:12,000
Everything that we collect,

250
00:14:12,000 --> 00:14:14,000
everything that we hold on to,

251
00:14:14,000 --> 00:14:16,000
no security for us,

252
00:14:16,000 --> 00:14:18,000
all these people in some cultures,

253
00:14:18,000 --> 00:14:20,000
they will bury their belongings with them,

254
00:14:20,000 --> 00:14:22,000
thinking that they can bring it into their next life

255
00:14:22,000 --> 00:14:27,000
or they'll burn certain objects in Thailand

256
00:14:27,000 --> 00:14:28,000
and China.

257
00:14:28,000 --> 00:14:32,000
They burn these really crappy houses

258
00:14:32,000 --> 00:14:35,000
and fake stuff and everything.

259
00:14:35,000 --> 00:14:37,000
And you think, wow, then they're going to get a lot of fake stuff

260
00:14:37,000 --> 00:14:39,000
when they go to have the ideas that they can take that stuff with them

261
00:14:39,000 --> 00:14:40,000
if they burn it.

262
00:14:40,000 --> 00:14:42,000
Since it's all garbage, you think,

263
00:14:42,000 --> 00:14:43,000
well, it's not really.

264
00:14:43,000 --> 00:14:45,000
Even if it did work.

265
00:14:45,000 --> 00:14:47,000
Of course, it doesn't work.

266
00:14:47,000 --> 00:14:50,000
You can't possess, we don't possess anything.

267
00:14:50,000 --> 00:14:53,000
We just happen to be going in the same direction as all this stuff.

268
00:14:53,000 --> 00:14:57,000
We have to be in the same physical, local, right?

269
00:14:57,000 --> 00:15:00,000
We manage to make choices that bring all this stuff near us,

270
00:15:00,000 --> 00:15:02,000
and that's it.

271
00:15:02,000 --> 00:15:03,000
We can't do any better than that.

272
00:15:03,000 --> 00:15:05,000
We can't control.

273
00:15:05,000 --> 00:15:10,000
Even our enjoyment is fleeting and ephemeral.

274
00:15:10,000 --> 00:15:13,000
Something that cannot be relied upon.

275
00:15:13,000 --> 00:15:17,000
Since it's very much out of our control,

276
00:15:17,000 --> 00:15:22,000
based on causes and conditions that are far more powerful than anyone of us.

277
00:15:22,000 --> 00:15:25,000
So our reminder for us,

278
00:15:25,000 --> 00:15:27,000
and in regards to meditation,

279
00:15:27,000 --> 00:15:30,000
our reminder for us to keep our minds clear

280
00:15:30,000 --> 00:15:32,000
and to be clear about all the things that we use,

281
00:15:32,000 --> 00:15:35,000
it doesn't mean that we shouldn't be using our possessions.

282
00:15:35,000 --> 00:15:42,000
It doesn't mean we shouldn't live.

283
00:15:42,000 --> 00:15:45,000
But it means that when we do experience even pleasure

284
00:15:45,000 --> 00:15:49,000
or when you're eating food,

285
00:15:49,000 --> 00:15:51,000
the pleasure of the food,

286
00:15:51,000 --> 00:15:53,000
when you're seeing beautiful things,

287
00:15:53,000 --> 00:15:56,000
the pleasure that comes from that.

288
00:15:56,000 --> 00:15:59,000
But we should see it simply as a feeling.

289
00:15:59,000 --> 00:16:02,000
We should see the experience simply as an experience.

290
00:16:02,000 --> 00:16:05,000
The experience is seeing the hearing,

291
00:16:05,000 --> 00:16:07,000
smelling, tasting, feeling.

292
00:16:07,000 --> 00:16:12,000
You can see liking, as liking, as disliking,

293
00:16:12,000 --> 00:16:15,000
and come to see that these things are not self,

294
00:16:15,000 --> 00:16:16,000
are not under control.

295
00:16:16,000 --> 00:16:18,000
Try to keep our minds clear,

296
00:16:18,000 --> 00:16:21,000
because all of this will catch you when you die.

297
00:16:21,000 --> 00:16:23,000
And if you have great wanting,

298
00:16:23,000 --> 00:16:25,000
so someone who is very, very rich,

299
00:16:25,000 --> 00:16:28,000
because they're clinging so much,

300
00:16:28,000 --> 00:16:30,000
what goes with them is the clinging,

301
00:16:30,000 --> 00:16:33,000
and that's why you're born very, very poor.

302
00:16:33,000 --> 00:16:36,000
That's why rich people are very quick to be born very poor

303
00:16:36,000 --> 00:16:38,000
because they have so much greed and attachment,

304
00:16:38,000 --> 00:16:41,000
and it's that want that goes with you.

305
00:16:41,000 --> 00:16:43,000
So you're born wanting,

306
00:16:43,000 --> 00:16:45,000
you're born in a constant state of want.

307
00:16:45,000 --> 00:16:48,000
I'm asking, well, it doesn't make you wealthy,

308
00:16:48,000 --> 00:16:50,000
being generous and kind,

309
00:16:50,000 --> 00:16:54,000
and having a mind of surplus.

310
00:16:54,000 --> 00:16:56,000
When you think you have surplus,

311
00:16:56,000 --> 00:16:58,000
when you're content with it,

312
00:16:58,000 --> 00:17:00,000
you'll have, then you'll always be content.

313
00:17:00,000 --> 00:17:02,000
You'll never know the words nutty.

314
00:17:02,000 --> 00:17:04,000
When you want something,

315
00:17:04,000 --> 00:17:05,000
you'll never be discontent.

316
00:17:05,000 --> 00:17:06,000
You'll never know discontent,

317
00:17:06,000 --> 00:17:08,000
because your mind is content.

318
00:17:08,000 --> 00:17:13,000
It's funny how it works that way.

319
00:17:13,000 --> 00:17:17,000
The mind that is clinging is the mind that is corrupt.

320
00:17:17,000 --> 00:17:19,000
And clinging to self is the worst type of corruption,

321
00:17:19,000 --> 00:17:23,000
because it leads to all other likes and dislikes.

322
00:17:23,000 --> 00:17:25,000
So we have to keep our minds clear.

323
00:17:25,000 --> 00:17:27,000
This is why meditation is important.

324
00:17:27,000 --> 00:17:29,000
When we live our lives, however, you live your lives.

325
00:17:29,000 --> 00:17:31,000
To keep your minds clear,

326
00:17:31,000 --> 00:17:33,000
and to not cling to self,

327
00:17:33,000 --> 00:17:35,000
either in regards to your own self,

328
00:17:35,000 --> 00:17:37,000
or in regards to the things that you enjoy.

329
00:17:37,000 --> 00:17:44,000
So a nice lesson from the Dhamupada and from our friend,

330
00:17:44,000 --> 00:17:46,000
the former Anandasati,

331
00:17:46,000 --> 00:17:50,000
he'll be managed to find his way out of that mist.

332
00:17:50,000 --> 00:17:52,000
So thank you all for tuning in,

333
00:17:52,000 --> 00:17:54,000
and wish you all peace,

334
00:17:54,000 --> 00:17:56,000
happiness, and freedom from suffering,

335
00:17:56,000 --> 00:18:11,000
and see you next time.

