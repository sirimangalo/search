1
00:00:00,000 --> 00:00:13,320
I don't believe in everyone broadcasting live.

2
00:00:30,000 --> 00:00:45,280
I'm sorry, broadcasting live, May 15th.

3
00:01:00,000 --> 00:01:14,240
Today's quote is from the commentary to the Kudaka Bhata, the first book of the Kudaka

4
00:01:14,240 --> 00:01:17,880
Nikaya.

5
00:01:17,880 --> 00:01:27,480
This is the commentary to the Sarna Gamanakata, which is called Sarna Daya, it's called the

6
00:01:27,480 --> 00:01:36,720
Sarna Daya, it's actually called Sarna Daya.

7
00:01:36,720 --> 00:01:49,880
The Kudaka Bhata is Kudaka mean small, short, could mean minor or kind of secondary insignificant

8
00:01:49,880 --> 00:02:06,240
innocence, might be miscellaneous, but short is appropriate, because the Sarna Daya is where

9
00:02:06,240 --> 00:02:13,720
we find Bhudtang Sarna Nang Gachan, the monks that are Nang Gachan, Sarna Nang Gachan.

10
00:02:13,720 --> 00:02:24,800
I go to the Buddha as my Sarna, Sarna as a refuge or as a reflection, a recollection,

11
00:02:24,800 --> 00:02:27,360
refuge may be better.

12
00:02:27,360 --> 00:02:33,520
The monks that are Nang Gachan, I go to the Dhamma, I go to the Sangha of a refuge.

13
00:02:33,520 --> 00:02:41,720
Dutti and Bhutan was Sarna Nang Gachan, for a second time I go to the Buddha as a refuge.

14
00:02:41,720 --> 00:02:46,320
I go to the Dhamma as a refuge, I go to the Sangha as a refuge.

15
00:02:46,320 --> 00:03:07,920
That T-M-P, then for a third time, and that's it, nine lines, 33 words, 33 words

16
00:03:07,920 --> 00:03:15,280
long, it's a small section, that's it.

17
00:03:15,280 --> 00:03:25,360
Commentary has some interesting things to say about this.

18
00:03:25,360 --> 00:03:31,120
First it talks about the Buddha, but we'll skip that part, only because it's not a

19
00:03:31,120 --> 00:03:42,400
quote, our quote is from the second part, that talks about the three refuges as a whole.

20
00:03:42,400 --> 00:03:48,760
And so there's some other other similes here that hopefully I can, I don't think this

21
00:03:48,760 --> 00:04:04,240
isn't it, oh, I find it, right, it's actually quite long here we have.

22
00:04:04,240 --> 00:04:34,200
The Dhamma taught by him is like the rays of the moon, the many rays that rays of light,

23
00:04:34,200 --> 00:04:35,200
that shine from the moon.

24
00:04:35,200 --> 00:04:49,720
Puna Chandakira Girana Samu Adita, Pimito Loco, Vyasamgo.

25
00:04:49,720 --> 00:05:04,000
The world loca that is delighted, please, that is lit up maybe, by the way.

26
00:05:04,000 --> 00:05:13,120
Maybe by the horizon, by the rays of the moon.

27
00:05:13,120 --> 00:05:24,360
So the Dhamma lights up the world, lights up is probably the best, this is the Sangha.

28
00:05:24,360 --> 00:05:34,280
So the Sangha are those of us that are lit up, light, light comes to you, a local body.

29
00:05:34,280 --> 00:05:40,600
It's like you were in a dark room before, and suddenly there is light, and Dhamma opens

30
00:05:40,600 --> 00:05:51,320
your eyes to things that were there and already, doesn't bring anything new, not chiefly,

31
00:05:51,320 --> 00:06:04,240
it teaches you about what's already there, that's the whole of it.

32
00:06:04,240 --> 00:06:09,440
They joke those people who practice breath meditation, they say it teaches you what's

33
00:06:09,440 --> 00:06:17,000
under your nose, because of course it uses the breath, they focus on the nose.

34
00:06:17,000 --> 00:06:23,080
But really it's here, the Buddha said, in this six-foot frame, this is where you find

35
00:06:23,080 --> 00:06:29,160
the beginning and the end of the universe, everything is already here, you don't have

36
00:06:29,160 --> 00:06:41,040
to go far, with this monk in Bangkok who gave an example of how we really don't know

37
00:06:41,040 --> 00:06:43,880
ourselves very well.

38
00:06:43,880 --> 00:06:49,920
They said, he asked this woman, she was 95 years old, he said, how many knuckles do you

39
00:06:49,920 --> 00:06:51,240
have on one hand?

40
00:06:51,240 --> 00:06:57,280
He said, don't count, but tell me, do you know, don't know, 95 years old, you still

41
00:06:57,280 --> 00:06:59,600
don't know.

42
00:06:59,600 --> 00:07:03,680
If you never counted, you don't know.

43
00:07:03,680 --> 00:07:10,560
So when you say you know something like the back of your hand, not so sure, they never

44
00:07:10,560 --> 00:07:11,560
look.

45
00:07:11,560 --> 00:07:15,320
You can look at your hand, that's not very difficult to figure out how many knuckles

46
00:07:15,320 --> 00:07:20,720
you have, but to understand your mind, how many people actually spend the time learning

47
00:07:20,720 --> 00:07:29,920
about themselves, about their mind.

48
00:07:29,920 --> 00:07:39,680
So this is the light, the light of the moon, the Buddha is like the moon, that's the

49
00:07:39,680 --> 00:07:40,680
first one.

50
00:07:40,680 --> 00:07:55,880
One is Bala, Bala is like foolish, but a problem here, because it shouldn't be in

51
00:07:55,880 --> 00:08:00,280
Bala, it should be messy.

52
00:08:00,280 --> 00:08:08,800
Or Bala, sorry, the newly arisen sun, I get it, Bala means young in this case, it usually

53
00:08:08,800 --> 00:08:09,800
means foolish.

54
00:08:09,800 --> 00:08:18,080
Bala surya is the dawn, the Buddha is like the dawn.

55
00:08:18,080 --> 00:08:46,680
Bala surya is the dawn, the dawn of the moon.

56
00:08:46,680 --> 00:08:57,040
Just the glow and the dhamma with its many qualities is like the glow of the sun, I don't

57
00:08:57,040 --> 00:08:58,040
know.

58
00:08:58,040 --> 00:09:03,160
It's like the qualities of the risen sun, same thing.

59
00:09:03,160 --> 00:09:10,480
Wood tapakaro, look clear about that, dhambo, my body is not great.

60
00:09:10,480 --> 00:09:25,000
Dayna, we had dhamda, karo, loco, we had sanko, the darkness, and the kara is, and the

61
00:09:25,000 --> 00:09:35,040
means blind, kara means that which makes you blind, and the kara is darkness.

62
00:09:35,040 --> 00:09:41,480
We had that, it means destroyed or dispelled.

63
00:09:41,480 --> 00:09:50,720
The world that whose darkness is destroyed by the sun is like the sun, so again this light,

64
00:09:50,720 --> 00:10:01,040
the Buddha is like the moon, he is like the sun, and when it lights up the world.

65
00:10:01,040 --> 00:10:02,040
Because that's it.

66
00:10:02,040 --> 00:10:07,480
ism isn't about, I had an interesting conversation today, I went to see my family today,

67
00:10:07,480 --> 00:10:14,760
I talked about music, and it was a good debate, and I had to admit some points, because

68
00:10:14,760 --> 00:10:23,720
there's definitely a potential to see good in music, in the sense, in the world, it

69
00:10:23,720 --> 00:10:32,920
can strengthen you, the rhythm, the trance that it puts you in, can strengthen you.

70
00:10:32,920 --> 00:10:37,840
So what I couldn't probably quite get across as well as I would have liked, it was about

71
00:10:37,840 --> 00:10:47,640
five or six on one, so I was vastly outnumbered, my family's in to music.

72
00:10:47,640 --> 00:10:52,240
What is that, the sensual, the sensual attached, I really didn't talk about it, but

73
00:10:52,240 --> 00:11:04,320
it's a tough crowd, not very convinced, is the attachment to it, you know, in the mind.

74
00:11:04,320 --> 00:11:09,080
You could be attached, you're attached to the sound, you like it, and liking is an addiction,

75
00:11:09,080 --> 00:11:15,360
and there's so many things that you have to, there's a world view that you have to, I guess,

76
00:11:15,360 --> 00:11:21,800
a change in world view, but the reason I bring it up is because it's not about dogma,

77
00:11:21,800 --> 00:11:27,840
we can't just say music is bad, we have to look at reality, we're not about, we can't

78
00:11:27,840 --> 00:11:35,920
be, if we want to be followed in Buddha, we can't accept dogma, we can't follow dogma,

79
00:11:35,920 --> 00:11:43,200
we can't believe things, we can't follow things just because it's a belief, we follow

80
00:11:43,200 --> 00:11:49,400
things because they're true, if they're not true, we have to stop following them, and

81
00:11:49,400 --> 00:11:56,880
there's a challenge there, you know, what can we know to be true?

82
00:11:56,880 --> 00:12:06,840
And so it's not, I mean, it's not about the whole truth, it's about two things, the good

83
00:12:06,840 --> 00:12:13,720
truth, the truth that is useful, but also the truth that we can know.

84
00:12:13,720 --> 00:12:21,200
So any truth that we are going to find has to be normal, and we have to come to know it,

85
00:12:21,200 --> 00:12:26,760
that's how we become a true follower of the Buddha, new practices, teachings, we don't

86
00:12:26,760 --> 00:12:32,960
believe it, we don't think it, we don't suspect it, we realize, it's very simple things,

87
00:12:32,960 --> 00:12:38,120
so we study music, for example, and you can say music is good, music is bad, music makes

88
00:12:38,120 --> 00:12:44,800
you happy, my main argument was that happiness doesn't make you happy, this is a very

89
00:12:44,800 --> 00:12:51,840
good Buddhist argument, and it didn't really work because well, when you're so opposed

90
00:12:51,840 --> 00:12:59,800
to something, when your view is very much opposed, so we have different views, but you

91
00:12:59,800 --> 00:13:04,040
can argue, you can say happiness makes you happy, happiness doesn't lead to happiness,

92
00:13:04,040 --> 00:13:07,520
it's not the cause of happiness, it's not the cause of itself.

93
00:13:07,520 --> 00:13:11,160
If it was, I mean actually it's kind of silly, because if it was, it would be a feedback

94
00:13:11,160 --> 00:13:16,960
loop, we'd always be happy, be happy once, you're happy forever, it's not the case,

95
00:13:16,960 --> 00:13:21,760
happiness doesn't lead to happiness, it's a bit simplistic and people would argue that

96
00:13:21,760 --> 00:13:27,040
it's more complicated than that, but happiness doesn't lead to happiness, doesn't have that

97
00:13:27,040 --> 00:13:33,640
power, something leads to happiness, we'd argue that things goodness in all its forms,

98
00:13:33,640 --> 00:13:37,800
things that we well eliminate, that's a tautology, we call it goodness because it leads to

99
00:13:37,800 --> 00:13:42,080
happiness, it leads to happiness because it's good and it's not really, there are certain

100
00:13:42,080 --> 00:13:50,360
things that lead to happiness, happiness is not one of them, but that's a good example

101
00:13:50,360 --> 00:13:56,040
because that's a claim that we can test, does happiness lead to happiness, most people

102
00:13:56,040 --> 00:14:01,200
will argue things like that, of course happiness is good in and of itself, you know,

103
00:14:01,200 --> 00:14:08,640
but let's investigate, but it's a problem really, people have views without investigating

104
00:14:08,640 --> 00:14:14,560
or they have views based on their own experience, anecdotal experience, without being

105
00:14:14,560 --> 00:14:24,440
systematic in their exploration, their investigation, what is about bringing light, it's not

106
00:14:24,440 --> 00:14:30,480
about changing anything, it's about bringing the purest light possible that we can, purest

107
00:14:30,480 --> 00:14:36,480
light we're able to, it's making ourselves as objective as we can, to see things as clearly

108
00:14:36,480 --> 00:14:43,200
as we can, and just accepting the truth, in fact it's not something you even have to accept

109
00:14:43,200 --> 00:14:51,200
once you see it, there are changes, you know, changes your whole perspective, you can't

110
00:14:51,200 --> 00:14:57,520
unsee it, you can't unno it, not that you would ever want to, right, I mean what's better

111
00:14:57,520 --> 00:15:02,320
in a dark room, what's better in a room, to know where everything is, or to not know where

112
00:15:02,320 --> 00:15:08,360
everything is, you want to forget where the chairs are, so you bump into them again,

113
00:15:08,360 --> 00:15:18,760
much better that you know, one and dah, hakkak, Puri, so we and buddha, one and dah, hakkak,

114
00:15:18,760 --> 00:15:26,040
I think dah, hakkak, I don't know, dah, hakkak, it must be like leader,

115
00:15:26,040 --> 00:15:37,660
don't think we have dah, hakkak, so when it leads you out of a forest I think dah, dah,

116
00:15:37,660 --> 00:15:49,200
dah, dah, burning, go, Buddha is like someone who burns down the forest, that's not

117
00:15:49,200 --> 00:15:54,000
Okay, it's a one-and-a-na, okay.

118
00:15:54,000 --> 00:15:57,600
The Buddha is one who burns down the forest.

119
00:15:57,600 --> 00:16:02,800
It's a person who burns down the forest, I think.

120
00:16:02,800 --> 00:16:14,280
The fire burning the forest is the one-and-a-na-na-na-na-na-na-na-na-na-na-na-na-na-na-na-na-na-na-na-na.

121
00:16:14,280 --> 00:16:31,040
The fire burning the forest, which is the right, like the fire burning the forest, is the

122
00:16:31,040 --> 00:16:50,740
dumb-na that burns the forest of Ko Cell, the forest of defilements, done Da Gram'n, Da

123
00:16:50,740 --> 00:17:12,980
I think it means the field, once the state of the forest being another, the thing, you know,

124
00:17:12,980 --> 00:17:32,980
that is, that has become a field, through the burning of the forest. Through the forest having been burned.

125
00:17:32,980 --> 00:17:41,580
Once the forest is consumed by fire, you have a field. It's not the best imagery. We tend to like our forests.

126
00:17:41,580 --> 00:17:51,180
And the Buddha liked forests as well, but commentator here has chosen some fairly strong imagery.

127
00:17:51,180 --> 00:18:01,980
But it's clever. It's clever because, and this is the sangha. The sangha is like that field. Once you've burnt down the forest, you can plant stuff in it.

128
00:18:01,980 --> 00:18:11,380
And so you have the, once the defilements are burned down, that person who has become a field of merit.

129
00:18:11,380 --> 00:18:20,180
Purnia Kaita. They're an incomparable field of merit. This is the sangha.

130
00:18:20,180 --> 00:18:29,480
They are incredible people to associate with enlightened beings. You can become a better person.

131
00:18:29,480 --> 00:18:40,680
You can do good deeds around them like nothing else. Even just giving them a gift.

132
00:18:40,680 --> 00:18:46,480
Even just giving a gift to like the Buddha or one of his chief disciples or something.

133
00:18:46,480 --> 00:18:54,380
Supporting anyone who practices meditation is awesome. They're a great field of merit.

134
00:18:54,380 --> 00:19:01,380
So it's not saying burned down forests or burning down forests. It's a good thing. But it's a curious, it's a clever imagery

135
00:19:01,380 --> 00:19:05,880
because we always talk about the sangha as being a field of merit.

136
00:19:05,880 --> 00:19:11,380
Mahame go via budho. Salida ooti oi a dambu.

137
00:19:11,380 --> 00:19:24,280
So the Buddha is like a great rain cloud. Salida ooti, the rain, the water of the rain is like the dambu.

138
00:19:24,280 --> 00:19:27,080
So what good is water from rain?

139
00:19:27,080 --> 00:19:33,380
Ooti nipa to pasamid tari no.

140
00:19:33,380 --> 00:19:40,080
We agenda panda oopasamid tari sereno sanko.

141
00:19:40,080 --> 00:19:43,580
So what is rain no?

142
00:19:43,580 --> 00:20:00,080
Dust. That removes the dust. Oopasamid tari, which removes basically or it's actually appeases, but that's not right here.

143
00:20:00,080 --> 00:20:11,080
Pasamid tari. The dust is appeased. The dust is cleanse. That's just loosely translated.

144
00:20:11,080 --> 00:20:26,580
The dust that is removed through the falling of the rain on the genopanda, on the countryside, is like the sangha.

145
00:20:26,580 --> 00:20:39,580
Yes, is like the sangha or the sangha is like that. The sangha who have pacified the dust of defilements.

146
00:20:39,580 --> 00:20:50,580
So they have cleansed the dust of defilements from their mind if wiped it away from their minds.

147
00:20:50,580 --> 00:21:02,580
The dust is the things that make our mind dirty, make our minds muddle, muddied the sense of not being able to see clearly.

148
00:21:02,580 --> 00:21:18,580
So the sangha who have cleansed their defilements using rain of the dhamma thought by the rain cloud of the buddha.

149
00:21:18,580 --> 00:21:24,980
So this is cleansing. We haven't gotten to the doctor yet. That's the quote today's quote.

150
00:21:24,980 --> 00:21:43,980
The sangha is a chariot here, isn't it? Yes. Buddha is like a skilled driver, chauffeur, not a chauffeur, chariot here.

151
00:21:43,980 --> 00:22:03,480
Asa janya vina yupai yupai yupai yupai yupai yupai yupai yupai yupai yupai yupai yupai yupai yupai yah dambu. Asan jkhaniya is a good horse that is trained and skillful by a skill.

152
00:22:03,480 --> 00:22:13,040
No, the skill and training horses of good breeding is like the dumber

153
00:22:14,080 --> 00:22:20,120
So a chariot here has to know how to train horses. This is a horse chariot, right?

154
00:22:22,920 --> 00:22:27,640
Nowadays it would be the skill and driving a car race car driver, maybe

155
00:22:27,640 --> 00:22:35,400
So we need to send Jan Nia Samuho, we as uncle

156
00:22:38,200 --> 00:22:41,400
The Samuho is a multitude of the

157
00:22:43,280 --> 00:22:45,760
Well-trained multitude of horses

158
00:22:47,000 --> 00:22:49,240
It's like the Sun

159
00:22:49,240 --> 00:22:51,240
The herd of horses, right?

160
00:22:51,240 --> 00:22:58,200
It's like the Sangha, so Buddhism is a training, very important Buddhist meditation isn't about

161
00:22:58,200 --> 00:23:02,600
blissing out or enjoying yourself. It's about training your mind,

162
00:23:04,280 --> 00:23:11,560
training your mind in clarity and seeing things as they are and training your mind through clarity

163
00:23:12,840 --> 00:23:14,840
The clarity trains you

164
00:23:14,840 --> 00:23:24,600
It trains you to do and say and think the right things because you know it's right and what's wrong for yourself without

165
00:23:25,560 --> 00:23:28,600
any kind of brainwashing or dogma

166
00:23:31,160 --> 00:23:36,600
I don't know if I should go on it looks like there's tons and tons and tons of these this would be a good commentary to translate

167
00:23:38,600 --> 00:23:42,520
And then go stop there though the doctor one comes up so that the court tonight is

168
00:23:42,520 --> 00:23:45,080
Can we skip ahead to the doctor?

169
00:23:47,960 --> 00:23:51,640
I don't know if I can even find it, maybe it's coming up

170
00:23:56,440 --> 00:23:58,440
No

171
00:24:01,880 --> 00:24:04,600
There's tons, there's like 50 or 60 different

172
00:24:04,600 --> 00:24:14,280
40, 20, 30, 40, there's lots of them think we'll end it there. So the quote was the Buddha is a skilled physician

173
00:24:15,960 --> 00:24:22,200
Like a physician who is able to heal the sickness, so he knows the sickness of defilements

174
00:24:25,080 --> 00:24:28,680
The dhamma is like the medicine, rightly applied the Sangha

175
00:24:28,680 --> 00:24:35,800
With their defilements cured or like people destroyed restored to health by that medicine

176
00:24:43,960 --> 00:24:48,360
Rightly applied, no even like medicine the dhamma has to be rightly applied

177
00:24:50,040 --> 00:24:56,600
Okay, just read the dhamma and decide to practice it. It's important to have the direction guidance

178
00:24:56,600 --> 00:24:58,600
Support those things

179
00:25:05,720 --> 00:25:10,280
So do you have any important questions? It's been a long day, so I'm not gonna

180
00:25:10,280 --> 00:25:27,800
Stay here too long, but if you got some important meditative questions

181
00:25:27,800 --> 00:25:38,040
So I'm gonna try and explain music. No, the other thing I wanted to say about music is that

182
00:25:38,920 --> 00:25:43,080
In a worldly sense, it could be healthy. They say, well, music is good for you

183
00:25:43,080 --> 00:25:49,400
It actually leads to healthy. We're arguing that there are cases of people being cured through music

184
00:25:50,440 --> 00:25:54,120
So in a worldly sense, I could see how the rhythm would be healthy for the body

185
00:25:54,120 --> 00:26:01,000
And even for the mind, rhythm, it puts you in a trance and so you're in a blissful space

186
00:26:05,800 --> 00:26:07,800
Yeah

187
00:26:08,840 --> 00:26:13,960
And whether that's right or wrong, good or bad, I mean, it's a lot of delusion involved with

188
00:26:14,760 --> 00:26:19,240
Good health, of course, but you know, that's not a big deal. worldly sense. Okay, agree that

189
00:26:19,240 --> 00:26:25,640
There are some states of mind that a trance like that can be brought about through music

190
00:26:26,840 --> 00:26:34,360
The bigger problem is the sensuality. Just the enjoyment of the sound that becomes addictive and it's just a minor addiction

191
00:26:35,400 --> 00:26:38,280
Nothing huge. It's not breaking the five precepts.

192
00:26:43,320 --> 00:26:45,640
Yeah, I mean, there's and then there's bad music like

193
00:26:45,640 --> 00:26:48,440
Music that teaches you how to be a bad person

194
00:26:49,560 --> 00:26:51,880
Gangster rap that kind of stuff, of course

195
00:26:54,920 --> 00:26:58,520
But my brother's a musician. It was a difficult argument

196
00:26:59,400 --> 00:27:01,400
That I want to get into it too, but

197
00:27:03,160 --> 00:27:09,720
Okay, I see that there are no pertinent questions if anyone had a pressing questions if any had anyone had pressing questions

198
00:27:09,720 --> 00:27:15,800
I would I think they would have been posted by now, so I think in a second night

199
00:27:17,240 --> 00:27:21,160
Tomorrow morning. I'm off to New York with two other monks and Michael's art driver

200
00:27:22,680 --> 00:27:24,680
so

201
00:27:24,920 --> 00:27:28,040
Probably nothing tomorrow, but I actually maybe

202
00:27:28,920 --> 00:27:32,600
If I can get Wi-Fi in this monastery and be Cambodia's monastery

203
00:27:33,640 --> 00:27:35,640
I'll try

204
00:27:35,640 --> 00:27:37,640
And broadcast something we'll see

205
00:27:37,640 --> 00:27:39,640
Anyway, good night

