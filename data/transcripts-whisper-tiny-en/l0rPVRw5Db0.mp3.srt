1
00:00:00,000 --> 00:00:26,000
Okay, here we are in my VIP meditation room in Chantong, in the hotel, but I'm sorry,

2
00:00:26,000 --> 00:00:33,000
meditation hall.

3
00:00:38,000 --> 00:00:45,000
That's it over in teacher's downtown at the end of the hall.

4
00:00:45,000 --> 00:00:49,000
So you can see what it is.

5
00:00:49,000 --> 00:00:53,000
And in the day to day.

6
00:00:53,000 --> 00:01:16,000
Okay, so here I am going to give me a special room now.

7
00:01:16,000 --> 00:01:27,000
This is all sorts of stuff I brought to give away.

8
00:01:27,000 --> 00:01:30,000
This is my own stuff.

9
00:01:30,000 --> 00:01:37,000
I just got this book done in Malaysia.

10
00:01:37,000 --> 00:01:39,000
That's not bad.

11
00:01:39,000 --> 00:01:46,000
Actually, they used an older version, which is too bad.

12
00:01:46,000 --> 00:01:49,000
There it is.

13
00:01:49,000 --> 00:01:52,000
And I got a balcony.

14
00:01:52,000 --> 00:01:57,000
I'll show you the rest of the center.

15
00:01:57,000 --> 00:02:01,000
This is where I first came to meditate.

16
00:02:01,000 --> 00:02:03,000
I didn't look at anything like this.

17
00:02:03,000 --> 00:02:06,000
All of this was rice fields.

18
00:02:06,000 --> 00:02:15,000
And over here, I see I think it's that one.

19
00:02:15,000 --> 00:02:17,000
Is that scene number 12 on it?

20
00:02:17,000 --> 00:02:19,000
Yes, number 12.

21
00:02:19,000 --> 00:02:22,000
That's where I first started meditating.

22
00:02:22,000 --> 00:02:27,000
And right behind it here was rice fields.

23
00:02:27,000 --> 00:02:30,000
And all the way out was rice fields.

24
00:02:30,000 --> 00:02:33,000
And now all the way out is meditation.

25
00:02:33,000 --> 00:02:38,000
Let me see there at the end of this construction.

26
00:02:38,000 --> 00:02:46,000
This is where they're building a good deep for a gentle into the future.

27
00:02:46,000 --> 00:02:50,000
And this over here.

28
00:02:50,000 --> 00:02:52,000
This was a bamboo.

29
00:02:52,000 --> 00:02:57,000
This used to be a single-story bamboo hut.

30
00:02:57,000 --> 00:03:03,000
And we used to go and meditate in and now it's two stories.

31
00:03:03,000 --> 00:03:04,000
It's actually older.

32
00:03:04,000 --> 00:03:05,000
This building that I'm in is newer than this.

33
00:03:05,000 --> 00:03:12,000
And over here is the newest addition to being school residents for the school.

34
00:03:12,000 --> 00:03:16,000
The school is over.

35
00:03:16,000 --> 00:03:19,000
Over there.

36
00:03:19,000 --> 00:03:22,000
The school is over there.

37
00:03:22,000 --> 00:03:28,000
This is the school building and that's the residence for the school.

38
00:03:28,000 --> 00:03:33,000
And there's some meditators coming over to meditate.

39
00:03:33,000 --> 00:03:36,000
And this is the women's section in here.

40
00:03:36,000 --> 00:03:38,000
And it goes way back there.

41
00:03:38,000 --> 00:03:40,000
You can't see it behind the trees.

42
00:03:40,000 --> 00:03:42,000
And this is the men's section with the month.

43
00:03:42,000 --> 00:03:51,000
Those buildings over there are houses where students of Agentong have come to stay.

44
00:03:51,000 --> 00:03:54,000
This building down here is the kitchen.

45
00:03:54,000 --> 00:03:56,000
And that's it.

46
00:03:56,000 --> 00:03:58,000
So there's the tour here.

47
00:03:58,000 --> 00:04:01,000
You can put this here.

48
00:04:12,000 --> 00:04:13,000
So there you have it.

49
00:04:13,000 --> 00:04:18,000
This is where I'm going to be staying for the next 10 days.

50
00:04:18,000 --> 00:04:23,000
I've been here three days already, already started meditating.

51
00:04:23,000 --> 00:04:25,000
I'm getting over jet lag.

52
00:04:25,000 --> 00:04:30,000
I made my last video when I was just starting to feel the effects of jet lag.

53
00:04:30,000 --> 00:04:33,000
And I'm still not completely over it.

54
00:04:33,000 --> 00:04:36,000
But today I'm going to start a meditation course.

55
00:04:36,000 --> 00:04:41,000
This will be the last you hear from me for the next 10 days.

56
00:04:41,000 --> 00:04:45,000
How is it being back here?

57
00:04:45,000 --> 00:05:01,000
I think it's surprising how crazy the place is really in how unhappy people generally are.

58
00:05:01,000 --> 00:05:04,000
It seemed to be.

59
00:05:04,000 --> 00:05:09,000
And I think it has something to do with how many people are here.

60
00:05:09,000 --> 00:05:11,000
As you can see, it's a huge place.

61
00:05:11,000 --> 00:05:16,000
But it also has something to do with the proximity to the city.

62
00:05:16,000 --> 00:05:22,000
The monastery is actually built sort of as a part of the city.

63
00:05:22,000 --> 00:05:27,000
And you can hear in the background noises of the cars.

64
00:05:27,000 --> 00:05:39,000
And what that means is there's a lot of activity here and a lot of different types of activity.

65
00:05:39,000 --> 00:05:42,000
So it's not just a meditation center.

66
00:05:42,000 --> 00:05:51,000
There's also the school and there's also the people coming for other reasons to make merit or to get involved.

67
00:05:51,000 --> 00:06:03,000
And there's there's there's monk politics and so on involved with the local monastic government and so on.

68
00:06:03,000 --> 00:06:14,000
But yeah, it seems that there's a lot of stress at this place.

69
00:06:14,000 --> 00:06:22,000
And another reason I think that is something that's more of interest to those who are meditating.

70
00:06:22,000 --> 00:06:31,000
And that's the fact that what we're doing is actually really fighting with what's inside

71
00:06:31,000 --> 00:06:42,000
in the same way a person who has a physical sickness has to fight with that and come to terms with it.

72
00:06:42,000 --> 00:06:45,000
The people here you can see that they're really fighting.

73
00:06:45,000 --> 00:06:55,000
And in fact, one thing I thought of was the fact that it's actually the people who are suffering,

74
00:06:55,000 --> 00:07:04,000
who actually seem more admirable, like the people who are smiling and joking and having fun.

75
00:07:04,000 --> 00:07:10,000
Other same people who then turn around to get angry and cause problems for the place.

76
00:07:10,000 --> 00:07:12,000
And you can see this.

77
00:07:12,000 --> 00:07:14,000
You can see this in a place like this.

78
00:07:14,000 --> 00:07:22,000
The people who are really serious and kind of grim based and will tell you how much trouble they're having.

79
00:07:22,000 --> 00:07:29,000
But the ones who are actually fighting with the problems that they have inside.

80
00:07:29,000 --> 00:07:38,000
And one thing I've realized being back here is how much I've learned in terms of rising above that.

81
00:07:38,000 --> 00:07:45,000
So you see here I am in this VIP room, but I stay here.

82
00:07:45,000 --> 00:07:51,000
I don't go out and get involved with the people in the monastery.

83
00:07:51,000 --> 00:07:54,000
A couple of days I just found out today they've been looking for me.

84
00:07:54,000 --> 00:07:56,000
They don't know where I'm staying even.

85
00:07:56,000 --> 00:07:58,000
People don't know that I'm here.

86
00:07:58,000 --> 00:08:04,000
And then they heard that I was back and people are looking for me and trying to get me those books.

87
00:08:04,000 --> 00:08:06,000
But I just don't get involved.

88
00:08:06,000 --> 00:08:12,000
And the people who have really succeeded here do the same.

89
00:08:12,000 --> 00:08:18,000
They've learned to bring themselves above conflict.

90
00:08:18,000 --> 00:08:24,000
So I think this is instructive for all of us in whatever situation we are.

91
00:08:24,000 --> 00:08:32,000
The way to win in a conflict or to overcome a conflict is not to defeat other people.

92
00:08:32,000 --> 00:08:34,000
It's to rise above the conflict.

93
00:08:34,000 --> 00:08:44,000
To live on a level above that dimension,

94
00:08:44,000 --> 00:08:49,000
the dimension that that conflict is on or those people are working on.

95
00:08:49,000 --> 00:08:54,000
Because there's a lot of conflict and politics in a place like this,

96
00:08:54,000 --> 00:09:00,000
or in any place where there's lots of people and egos and so on.

97
00:09:00,000 --> 00:09:04,000
When you rise above it, when you think in a different way,

98
00:09:04,000 --> 00:09:07,000
when you're a frame of reference,

99
00:09:07,000 --> 00:09:10,000
when your mode of existence is on a different level,

100
00:09:10,000 --> 00:09:14,000
then it can't affect you and it can't get at you.

101
00:09:14,000 --> 00:09:18,000
That's something that I think has really been instrumental.

102
00:09:18,000 --> 00:09:23,000
You see how the forces of evil, if you will,

103
00:09:23,000 --> 00:09:25,000
and these exist in all of us.

104
00:09:25,000 --> 00:09:30,000
And they seem to somehow exist palpably in the world around us.

105
00:09:30,000 --> 00:09:34,000
How they seek you out and try to pull you in and try to drag you down.

106
00:09:34,000 --> 00:09:40,000
And to control you, to bring you into their sphere of influence.

107
00:09:40,000 --> 00:09:43,000
And there's no way you can win against that.

108
00:09:43,000 --> 00:09:48,000
The more you fight, the more involved you become in that dimension,

109
00:09:48,000 --> 00:09:50,000
in that sphere of influence.

110
00:09:50,000 --> 00:09:53,000
The only way to win is to rise above it.

111
00:09:53,000 --> 00:09:56,000
And so if people want to control you externally,

112
00:09:56,000 --> 00:09:58,000
that them control you, they want to tell you where to,

113
00:09:58,000 --> 00:10:01,000
where to stay, what to do, and so on,

114
00:10:01,000 --> 00:10:05,000
it's on a certain dimension.

115
00:10:05,000 --> 00:10:11,000
And if you fight that, if you get involved with it,

116
00:10:11,000 --> 00:10:14,000
you can only suffer, even though you win,

117
00:10:14,000 --> 00:10:18,000
you're still on that dimension in the dimension of conflict.

118
00:10:18,000 --> 00:10:22,000
Only when you rise above it, when you're able to live,

119
00:10:22,000 --> 00:10:25,000
on the level of the mind,

120
00:10:25,000 --> 00:10:31,000
the dimension or true freedom exists, or true freedom is possible,

121
00:10:31,000 --> 00:10:34,000
which is within ourselves.

122
00:10:34,000 --> 00:10:37,000
Then you can rise above it,

123
00:10:37,000 --> 00:10:40,000
and then you can truly be victorious.

124
00:10:40,000 --> 00:10:42,000
So that's what I found.

125
00:10:42,000 --> 00:10:46,000
You can really feel the conflict in a place

126
00:10:46,000 --> 00:10:49,000
where there's so many people, and in such a small space,

127
00:10:49,000 --> 00:10:53,000
even in a place where people are ostensibly trying to

128
00:10:53,000 --> 00:10:56,000
purify themselves.

129
00:10:56,000 --> 00:10:59,000
And I think it's safe to say that not everyone here is doing that.

130
00:10:59,000 --> 00:11:01,000
It's even trying.

131
00:11:01,000 --> 00:11:04,000
And a lot of people are trying and not succeeding very well.

132
00:11:04,000 --> 00:11:11,000
But it's encouraging to see that there's so many people still trying.

133
00:11:11,000 --> 00:11:16,000
And I think it's instructive for those people who think that

134
00:11:16,000 --> 00:11:24,000
meditation should somehow be pleasant and comfortable all the time

135
00:11:24,000 --> 00:11:26,000
that it should be a joy ride.

136
00:11:26,000 --> 00:11:30,000
It's really a misunderstanding of what meditation is,

137
00:11:30,000 --> 00:11:33,000
as I've said before.

138
00:11:33,000 --> 00:11:37,000
Meditation should be compared to the healing of the sicknesses

139
00:11:37,000 --> 00:11:39,000
in the body, which is never fun.

140
00:11:39,000 --> 00:11:44,000
Nobody goes to the hospital and laughs and dances and sings

141
00:11:44,000 --> 00:11:47,000
during the time of their sickness.

142
00:11:47,000 --> 00:11:50,000
But when you're free from sickness, then the happiness

143
00:11:50,000 --> 00:11:55,000
and the freedom comes.

144
00:11:55,000 --> 00:12:00,000
I was thinking about, that's one way of looking at it

145
00:12:00,000 --> 00:12:04,000
is in terms of getting healed.

146
00:12:04,000 --> 00:12:07,000
When you're sick, you come to a place like this,

147
00:12:07,000 --> 00:12:11,000
and it's quite miserable to have to go through the treatment.

148
00:12:11,000 --> 00:12:16,000
And as you go through the treatment, it brings up a lot of unpleasantness

149
00:12:16,000 --> 00:12:19,000
just as the hospital does.

150
00:12:19,000 --> 00:12:21,000
But that's the healing process.

151
00:12:21,000 --> 00:12:23,000
It's hard work.

152
00:12:23,000 --> 00:12:27,000
It's quite trying to go through.

153
00:12:27,000 --> 00:12:29,000
And another way of looking at it, I think,

154
00:12:29,000 --> 00:12:31,000
is in terms of the struggle for freedom.

155
00:12:31,000 --> 00:12:35,000
If you look at any struggle for physical freedom,

156
00:12:35,000 --> 00:12:41,000
like the story of Nelson Mandela, who had to spend 27 years in jail,

157
00:12:41,000 --> 00:12:45,000
just to begin, as he said, to find freedom for the people.

158
00:12:45,000 --> 00:12:48,000
And even with the end of a part, I didn't.

159
00:12:48,000 --> 00:12:49,000
So that's what I forget.

160
00:12:49,000 --> 00:12:52,000
You knew it wasn't true freedom, which comes from within,

161
00:12:52,000 --> 00:12:58,000
which we had to build through trust and through understanding.

162
00:12:58,000 --> 00:13:04,000
And so we shouldn't underestimate the difficulty

163
00:13:04,000 --> 00:13:07,000
and the significance of what we're doing.

164
00:13:07,000 --> 00:13:10,000
It's not just sitting down for fun and poof your in light

165
00:13:10,000 --> 00:13:15,000
and poof your freedom from the sicknesses that we have inside.

166
00:13:15,000 --> 00:13:18,000
You're free from suffering.

167
00:13:18,000 --> 00:13:22,000
You know, if physical suffering took so many years

168
00:13:22,000 --> 00:13:26,000
just for a group of people in South Africa,

169
00:13:26,000 --> 00:13:30,000
externally, how much longer is it going to take for us

170
00:13:30,000 --> 00:13:34,000
to find true freedom within ourselves?

171
00:13:34,000 --> 00:13:39,000
So, you know, people always ask,

172
00:13:39,000 --> 00:13:41,000
you know, why I seem so serious,

173
00:13:41,000 --> 00:13:43,000
why my videos are not more humorous,

174
00:13:43,000 --> 00:13:45,000
more exciting, more entertaining.

175
00:13:45,000 --> 00:13:47,000
And I'd say, if that's what you're looking for

176
00:13:47,000 --> 00:13:50,000
than you're on the wrong track and you should go elsewhere.

177
00:13:50,000 --> 00:13:52,000
This is a very serious thing that we're doing.

178
00:13:52,000 --> 00:13:56,000
I know YouTube is, it's seen as a means of entertainment

179
00:13:56,000 --> 00:13:59,000
and people think it should be enjoyable.

180
00:13:59,000 --> 00:14:02,000
Well, yeah, this is kind of a wake-up call.

181
00:14:02,000 --> 00:14:05,000
What we're doing here is something very serious.

182
00:14:05,000 --> 00:14:09,000
And the YouTube is the medium that I use to teach

183
00:14:09,000 --> 00:14:13,000
and many other people use to teach, but we're teaching.

184
00:14:13,000 --> 00:14:20,000
The understanding that there's a lot of work for us to do

185
00:14:20,000 --> 00:14:26,000
and that the most important thing is not to enjoy

186
00:14:26,000 --> 00:14:31,000
what we're doing, but to do that which is leading us

187
00:14:31,000 --> 00:14:36,000
to true freedom from sickness, freedom from suffering.

188
00:14:36,000 --> 00:14:39,000
So, just another thought for today

189
00:14:39,000 --> 00:14:42,000
and that'll be my last one before I go into some serious

190
00:14:42,000 --> 00:14:47,000
meditation, some serious spiritual soul searching

191
00:14:47,000 --> 00:14:52,000
or whatever you call it, spiritual cleansing, I guess.

192
00:14:52,000 --> 00:14:56,000
The meditation is a re-adjustment of the mind.

193
00:14:56,000 --> 00:14:58,000
It's looking at how your mind works

194
00:14:58,000 --> 00:15:02,000
and seeing all the kinks, all the knots and slowly untime.

195
00:15:02,000 --> 00:15:05,000
So, that's what we do here.

196
00:15:05,000 --> 00:15:08,000
That's what I plan to be doing for the next 10 days

197
00:15:08,000 --> 00:15:11,000
and for the rest of my life.

198
00:15:11,000 --> 00:15:13,000
So, thanks again for tuning in every one

199
00:15:13,000 --> 00:15:17,000
and for the good comment, for the positive feedback,

200
00:15:17,000 --> 00:15:19,000
which is always welcome.

201
00:15:19,000 --> 00:15:24,000
The good feedback is welcome, but to an extent

202
00:15:24,000 --> 00:15:28,000
not looking for an argument if you don't agree

203
00:15:28,000 --> 00:15:30,000
you can just go elsewhere.

204
00:15:30,000 --> 00:15:50,000
But thanks for tuning in anyway and all the best everyone.

