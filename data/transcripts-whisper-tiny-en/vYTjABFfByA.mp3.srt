1
00:00:00,000 --> 00:00:23,000
Good evening, everyone, broadcasting live March 23rd, just reading the commentary.

2
00:00:23,000 --> 00:00:49,000
To our court today, our court talks about helpful people.

3
00:00:49,000 --> 00:00:55,000
Three helpful people, actually, die on me, be away from Google.

4
00:00:55,000 --> 00:01:07,000
From the last step of who, Cara, who, Cara, these three people are helpful to people who live with them.

5
00:01:07,000 --> 00:01:12,000
The commentary points out that they're...

6
00:01:12,000 --> 00:01:23,000
A great benefit to an undi-wasi Kapungalassa, to a person who lives with them, stays with them.

7
00:01:23,000 --> 00:01:27,000
Of course, the time of the Buddha, there was no internet.

8
00:01:27,000 --> 00:01:34,000
There weren't even books that they wrote this stuff down on.

9
00:01:34,000 --> 00:01:42,000
So you had to go off into the forest to find the teacher.

10
00:01:42,000 --> 00:01:46,000
The...

11
00:01:46,000 --> 00:01:50,000
Did I just crash the computer? No, okay.

12
00:01:50,000 --> 00:01:58,000
No, stop working.

13
00:01:58,000 --> 00:02:05,000
So there's these three types of people, and they can be understood on different levels.

14
00:02:05,000 --> 00:02:12,000
But I think what's interesting to me is that three concepts of what's important to be imparted.

15
00:02:12,000 --> 00:02:23,000
So someone who gives these things is helpful. Why? Because these things are helpful.

16
00:02:23,000 --> 00:02:33,000
So when it's helpful, going to the Buddha for refuge, the Dhamma for refuge, the Sangha for refuge.

17
00:02:33,000 --> 00:02:43,000
Someone who helps you do that is useful is a great use, because going for refuge is a great use.

18
00:02:43,000 --> 00:02:53,000
Not about this one for a while, because that can be understood on different levels.

19
00:02:53,000 --> 00:02:57,000
The most obvious one is you do a ceremony where you say,

20
00:02:57,000 --> 00:03:15,000
the Buddha, the Buddha for refuge, the Dhamma for refuge, the Sangha for refuge.

21
00:03:15,000 --> 00:03:27,000
The Saya support for help. I take the most of my support.

22
00:03:27,000 --> 00:03:37,000
Beyond just a ceremony, although I guess associated with the ceremonies, the level of actually taking the message support,

23
00:03:37,000 --> 00:03:42,000
which I would equate with becoming Buddhist.

24
00:03:42,000 --> 00:03:46,000
I think it's an interesting concept in something that we should talk about.

25
00:03:46,000 --> 00:03:53,000
There's a lot of people who are wary of, quote unquote, becoming Buddhist,

26
00:03:53,000 --> 00:03:57,000
or saying that they're Buddhist, or identifying themselves as Buddhists.

27
00:03:57,000 --> 00:04:03,000
Some people even say, identifying yourself as anything is conceit.

28
00:04:03,000 --> 00:04:11,000
There's an argument to be made there, but there's also a very strong argument to be made for the good on a conventional level.

29
00:04:11,000 --> 00:04:25,000
A very conventional level of identifying yourself as a Buddhist, because it provides you with a filter,

30
00:04:25,000 --> 00:04:31,000
or not a filter, but a guide to direct you.

31
00:04:31,000 --> 00:04:35,000
It informs your actions.

32
00:04:35,000 --> 00:04:41,000
If you do something and you're not a Buddhist, right? Maybe I'll do something Buddhist, or I'll do something.

33
00:04:41,000 --> 00:04:47,000
Again, that goes against the Buddhist teachings, but if you're Buddhist and you know it, then you think of yourself as that.

34
00:04:47,000 --> 00:04:53,000
They're much more likely to do things in line with Buddhist teaching.

35
00:04:53,000 --> 00:05:04,000
I think a lot of people are not at that level where they feel like they might practice a meditation.

36
00:05:04,000 --> 00:05:17,000
I just didn't think, well, this is useful, but I don't know if I can accept the Buddha as my refuge as my sort of guide.

37
00:05:17,000 --> 00:05:24,000
I can accept the Dhamma as my guide, or do I want to look outside of the Dhamma for guidance, that kind of thing.

38
00:05:24,000 --> 00:05:27,000
That's reason, well, that's understandable.

39
00:05:27,000 --> 00:05:39,000
Nobody is ever going to try to force you to become Buddhist, or push you to become Buddhist, but in the context of this quote, I think it's important to talk about the benefit of becoming Buddhist, which I think there is.

40
00:05:39,000 --> 00:05:48,000
I think when we talk about someone through whom you take refuge,

41
00:05:48,000 --> 00:05:59,000
I think part of it has to do with someone who can provide you with the answers and with the conviction, with the appreciation of Buddha, the Dhamma, and the Sangha.

42
00:05:59,000 --> 00:06:10,000
To the extent that you decide you want to become Buddhist, and you want to take the Buddha as your support, your guide.

43
00:06:10,000 --> 00:06:25,000
But that can even be someone who convinces you, that convinces you, but shows you how to practice meditation and tells you the benefits of it and gives you an appreciation of the Buddha's teaching.

44
00:06:25,000 --> 00:06:39,000
Because it's just about taking help, right? You could argue that anybody who practices the Buddha's teaching, any kind of Buddhist meditation, is going to the Buddha for refuge, going to the Dhamma for refuge, going to the Sangha for refuge.

45
00:06:39,000 --> 00:06:43,000
So it doesn't have to be a ceremony, and you don't have to call yourself Buddhist.

46
00:06:43,000 --> 00:06:48,000
I just think there's something to be said for.

47
00:06:48,000 --> 00:07:01,000
It's a finding the right community, and someone who provides you with that appreciation, so I think pretty awesome.

48
00:07:01,000 --> 00:07:09,000
I remember waking up at three in the morning during my first meditation course, told the story several times, but it just wasn't getting better.

49
00:07:09,000 --> 00:07:13,000
It was really awful because I just come out of...

50
00:07:13,000 --> 00:07:21,000
I spent the millennium drinking and smoking, and then like five days later I went off to practice meditation.

51
00:07:21,000 --> 00:07:23,000
It was really bad.

52
00:07:23,000 --> 00:07:34,000
I went to sleep really bummed out about it, and just really stressed about how awful I was doing in the course, how I couldn't get my mind to calm down.

53
00:07:34,000 --> 00:07:40,000
I was just really messed up, didn't really know what I was doing, knew that I was having a bad time off.

54
00:07:40,000 --> 00:07:45,000
I woke up at 3 a.m., and it just wasn't getting better, and it didn't blame the course or the centre at all.

55
00:07:45,000 --> 00:07:50,000
I really blamed myself and said, man, I'm a messed up.

56
00:07:50,000 --> 00:07:53,000
But I didn't know what to do.

57
00:07:53,000 --> 00:08:01,000
And so I was wandering around at 3 a.m., and I saw this golden Buddha statue in a bamboo back then.

58
00:08:01,000 --> 00:08:14,000
They had this bamboo hall where we could walk in meditation, and there was a big golden Buddha image inside, immediately gravitated towards it.

59
00:08:14,000 --> 00:08:21,000
I knelt down in front of the Buddha, and I went through the refuge, and I was crying.

60
00:08:21,000 --> 00:08:27,000
It really helped, you know, to just say, you know, I get it, because it was part of it was just like, I get it.

61
00:08:27,000 --> 00:08:29,000
I'm really...

62
00:08:29,000 --> 00:08:31,000
I need this.

63
00:08:31,000 --> 00:08:35,000
I need to straighten up.

64
00:08:35,000 --> 00:08:45,000
And so I just gave myself, I said, student to the Buddha, and really considered, I'm not saying this is how it should go.

65
00:08:45,000 --> 00:08:52,000
But this is, in my experience, it was quite useful.

66
00:08:52,000 --> 00:08:53,000
It wasn't enough.

67
00:08:53,000 --> 00:09:01,000
I always had to do a lot of work still, but taking refuge was a real support for me.

68
00:09:01,000 --> 00:09:10,000
You know, they talk about how it takes some of the pressure off of you because you put yourself in it.

69
00:09:10,000 --> 00:09:14,000
You put your trust, which, you know, is...

70
00:09:14,000 --> 00:09:20,000
You can be dangerous if you're putting your trust in something not trustworthy, but if you put your trust in something trustworthy,

71
00:09:20,000 --> 00:09:25,000
take some of the burden off of you to have to worry about, are you doing the right thing?

72
00:09:25,000 --> 00:09:32,000
I'm sorry, but you just put your trust in your saying, come what may I'm going to follow this.

73
00:09:32,000 --> 00:09:36,000
It makes it easier, and it gives you encouragement.

74
00:09:36,000 --> 00:09:40,000
It can help you through your practice.

75
00:09:40,000 --> 00:09:48,000
But you really only take refuge in the Buddha, the Dhamma and the Sun got technically when you become a sort of department.

76
00:09:48,000 --> 00:09:56,000
So the Pana takes refuge in the Buddha, but the commentary does something good, and it says,

77
00:09:56,000 --> 00:10:06,000
no, this is just referring to someone who gives the formula and helps you to make a determination that you're going to follow the Buddha's teaching,

78
00:10:06,000 --> 00:10:09,000
whatever that formula may take.

79
00:10:09,000 --> 00:10:13,000
Because the second one, someone who teaches you the four normal truths,

80
00:10:13,000 --> 00:10:17,000
well, that's someone who leads you to sort the pun.

81
00:10:17,000 --> 00:10:21,000
Well, so the pun is someone who sees and understands the form of truth.

82
00:10:21,000 --> 00:10:26,000
So you could argue that anyone who teaches you intellectually what are the four normal truths,

83
00:10:26,000 --> 00:10:31,000
well, that's not easy to find in that person who's done you a good service,

84
00:10:31,000 --> 00:10:38,000
but that's not what the commentary says is happening here.

85
00:10:38,000 --> 00:10:46,000
And the four number truths from a practical perspective are really the core of the bus and the core of what we're trying to gain.

86
00:10:46,000 --> 00:10:50,000
So if you wonder why the heck am I doing this walking and sitting so much?

87
00:10:50,000 --> 00:10:55,000
What's the real reason you're trying to understand the four number truths?

88
00:10:55,000 --> 00:10:58,000
The first one you're trying to understand suffering,

89
00:10:58,000 --> 00:11:06,000
you're trying to understand that there is suffering and that things which we cling to are actually unsatisfying,

90
00:11:06,000 --> 00:11:16,000
well, it's stressful. Everything, if you have physical things that you cling to,

91
00:11:16,000 --> 00:11:24,000
that you cling to, sites or sounds or smells or tastes or feelings or

92
00:11:24,000 --> 00:11:30,000
sites, sounds, smells, tastes, feelings or thoughts,

93
00:11:30,000 --> 00:11:34,000
that you cling to, that's a cause for suffering.

94
00:11:34,000 --> 00:11:39,000
So the second noble truth is that it's the craving that causes suffering.

95
00:11:39,000 --> 00:11:43,000
The clinging, the trying to fix things and trying to keep things,

96
00:11:43,000 --> 00:11:47,000
trying to maintain things that's suffering.

97
00:11:47,000 --> 00:11:56,000
You know, partiality that makes you want things to be a certain way and want them not to be a different way.

98
00:11:56,000 --> 00:11:59,000
The cessation of the third truth is the cessation of suffering.

99
00:11:59,000 --> 00:12:04,000
I mean, that's the awesome one. When there's no more craving, there's no more suffering.

100
00:12:04,000 --> 00:12:09,000
That's it. Quite simple.

101
00:12:09,000 --> 00:12:13,000
So the first two are really the ones that we work on in meditation.

102
00:12:13,000 --> 00:12:18,000
When you get to the point where you are able to let go of craving, when you really let go,

103
00:12:18,000 --> 00:12:22,000
then the third one. There's the cessation of suffering.

104
00:12:22,000 --> 00:12:26,000
The fourth one is the path to the cessation of suffering.

105
00:12:26,000 --> 00:12:29,000
It's last because it answers the question,

106
00:12:29,000 --> 00:12:32,000
okay, you want to be free from suffering, how do you do that?

107
00:12:32,000 --> 00:12:35,000
So the fourth one sums it all up and says,

108
00:12:35,000 --> 00:12:39,000
there is an eight full noble path and that's what things do in cessation of suffering.

109
00:12:45,000 --> 00:12:48,000
So the intellectual appreciation is quite useful,

110
00:12:48,000 --> 00:12:50,000
but someone who helps you see these things,

111
00:12:50,000 --> 00:12:54,000
who helps you actually practice so that you see

112
00:12:54,000 --> 00:12:58,000
that the things that you cling to and in fact you're clinging in general,

113
00:12:58,000 --> 00:13:03,000
it's all stressful, it's all useless.

114
00:13:03,000 --> 00:13:09,000
That person is the person who helps you see Nirvana for the first time.

115
00:13:09,000 --> 00:13:12,000
That's a hard person to play.

116
00:13:12,000 --> 00:13:15,000
So that's a useful thing.

117
00:13:15,000 --> 00:13:23,000
The reason why it's useful person is because seeing Nirvana is the most awesome thing you can see.

118
00:13:23,000 --> 00:13:36,000
It eradicates doubt and wrong view and wrong practice,

119
00:13:36,000 --> 00:13:43,000
because you know the right practice, because you've seen the truth.

120
00:13:43,000 --> 00:13:46,000
You've seen the goal.

121
00:13:46,000 --> 00:13:52,000
And the third person who's a great benefit is the person who helps you destroy the balance.

122
00:13:52,000 --> 00:13:56,000
So that's someone who helps you become an Arab hunt.

123
00:13:56,000 --> 00:14:00,000
It helps you become free from all the families.

124
00:14:00,000 --> 00:14:04,000
So after becoming a sort upon that,

125
00:14:04,000 --> 00:14:14,000
the commentaries, this is the person who helps you then go on to become the Arab hunt.

126
00:14:14,000 --> 00:14:24,000
The person who helps you to become an Arab hunt,

127
00:14:24,000 --> 00:14:35,000
is the person who helps you to become an Arab hunt.

128
00:14:35,000 --> 00:14:45,000
Which person, depending on which person, a person,

129
00:14:45,000 --> 00:15:04,000
abides having attained through seeing for oneself with higher knowledge in this very life.

130
00:15:04,000 --> 00:15:19,000
With the destruction of the Asava of the taints of the poisons of the mind,

131
00:15:19,000 --> 00:15:28,000
the freedom of my liberation of mind from the Asava,

132
00:15:28,000 --> 00:15:39,000
is now the untainted liberation of mind, the untainted liberation through wisdom.

133
00:15:39,000 --> 00:15:47,000
A celebration of mind actually is probably better in liberation through meditation,

134
00:15:47,000 --> 00:15:51,000
I guess you'll say, through samata, through tranquility.

135
00:15:51,000 --> 00:16:01,000
And then the medity is liberation through wisdom.

136
00:16:01,000 --> 00:16:03,000
You know, that's awesome.

137
00:16:03,000 --> 00:16:06,000
You can find someone who helps you do that.

138
00:16:06,000 --> 00:16:12,000
We really, the Buddha is the one who does all of these for us.

139
00:16:12,000 --> 00:16:17,000
We still have the Buddha, he's still around, through his teachings,

140
00:16:17,000 --> 00:16:21,000
teachings like this, teachings.

141
00:16:21,000 --> 00:16:26,000
In the tepitika we still have his teachings, and they're very good teachings.

142
00:16:26,000 --> 00:16:30,000
We can doubt that this is all exactly what the Buddha said that's fine,

143
00:16:30,000 --> 00:16:34,000
but there's no reason to doubt the teachings in the tepitika.

144
00:16:34,000 --> 00:16:36,000
Not in general.

145
00:16:36,000 --> 00:16:39,000
We want to doubt some specifics that's fine.

146
00:16:39,000 --> 00:16:43,000
They don't disagree with that, but the general message

147
00:16:43,000 --> 00:16:47,000
and teachings in the tepitika are very powerful.

148
00:16:47,000 --> 00:16:52,000
We have a great benefit to us all,

149
00:16:52,000 --> 00:16:55,000
to the fact that we've come across this,

150
00:16:55,000 --> 00:16:58,000
someone who in the history we never met,

151
00:16:58,000 --> 00:17:06,000
had this system and this understanding of reality.

152
00:17:06,000 --> 00:17:14,000
This is something that we should appreciate.

153
00:17:14,000 --> 00:17:20,000
So this is the sutta's about appreciation, appreciation of good things,

154
00:17:20,000 --> 00:17:25,000
and sort of laying out what are good things and appreciation of people like the Buddha,

155
00:17:25,000 --> 00:17:33,000
who, rather Buddha, who protest these things.

156
00:17:33,000 --> 00:17:38,000
I'm just a short sutta, with a good quote.

157
00:17:38,000 --> 00:17:49,000
Anyway, that's the damn number for tonight.

158
00:17:49,000 --> 00:17:52,000
I'm going to post the hangout if anybody has questions.

159
00:17:52,000 --> 00:18:02,000
Welcome to the command.

160
00:18:02,000 --> 00:18:09,000
It's a busy time with these days.

161
00:18:09,000 --> 00:18:12,000
Lots of things happening.

162
00:18:12,000 --> 00:18:15,000
I went to another Miska meeting,

163
00:18:15,000 --> 00:18:21,000
McMaster Indigenous students, community alliance,

164
00:18:21,000 --> 00:18:25,000
and they're going to work with us on the piece symposium.

165
00:18:25,000 --> 00:18:30,000
I really think it's important because we can't talk about

166
00:18:30,000 --> 00:18:33,000
peace elsewhere or peace in general,

167
00:18:33,000 --> 00:18:44,000
without addressing the fact that Canada has a problem

168
00:18:44,000 --> 00:18:46,000
from the point of view of peace.

169
00:18:46,000 --> 00:18:47,000
We're not at peace.

170
00:18:47,000 --> 00:18:52,000
We haven't made peace with the indigenous peoples of this country.

171
00:18:52,000 --> 00:18:55,000
Most people don't really know the extent of it.

172
00:18:55,000 --> 00:19:03,000
Apartheid the situation and the cultural genocide,

173
00:19:03,000 --> 00:19:06,000
and the legacy of residential schools,

174
00:19:06,000 --> 00:19:10,000
they just picked up kids from their communities.

175
00:19:10,000 --> 00:19:12,000
Some of them just kidnapped them,

176
00:19:12,000 --> 00:19:16,000
and brought them to these residential schools.

177
00:19:16,000 --> 00:19:19,000
You can go ahead, you can go ahead.

178
00:19:19,000 --> 00:19:22,000
I'm just going to talk about other things.

179
00:19:22,000 --> 00:19:25,000
It's just fun about them.

180
00:19:25,000 --> 00:19:27,000
You're welcome to stay tuned.

181
00:19:27,000 --> 00:19:32,000
Did you have a question?

182
00:19:32,000 --> 00:19:36,000
So, I really want them to be a part of our symposium.

183
00:19:36,000 --> 00:19:41,000
I really want to support them because

184
00:19:41,000 --> 00:19:42,000
they don't know.

185
00:19:42,000 --> 00:19:44,000
Maybe it's not entirely Buddhist,

186
00:19:44,000 --> 00:19:49,000
but I think there's an argument from the point of view of peace.

187
00:19:49,000 --> 00:19:53,000
No, I mean, the big thing for me is the mental health issues,

188
00:19:53,000 --> 00:19:58,000
because these people were really hurt,

189
00:19:58,000 --> 00:20:01,000
and there's a lot of hurt, a lot of anger,

190
00:20:01,000 --> 00:20:04,000
a lot of self-hatred.

191
00:20:04,000 --> 00:20:07,000
They're not self-hatred, but hated for their own culture,

192
00:20:07,000 --> 00:20:09,000
because they were brainwashed,

193
00:20:09,000 --> 00:20:12,000
and told that it was wrong.

194
00:20:12,000 --> 00:20:14,000
That kind of thing, and told them it was wrong

195
00:20:14,000 --> 00:20:16,000
to speak their own language.

196
00:20:16,000 --> 00:20:19,000
The mental health issues, of course, and the sexual abuse,

197
00:20:19,000 --> 00:20:22,000
and the physical abuse, and emotional abuse,

198
00:20:22,000 --> 00:20:25,000
that went on at these places.

199
00:20:25,000 --> 00:20:27,000
You're just the emotional abuse of the whole thing,

200
00:20:27,000 --> 00:20:31,000
being taken away from your families and your culture.

201
00:20:31,000 --> 00:20:33,000
That's our legacy.

202
00:20:33,000 --> 00:20:37,000
I think this was happening up until the 90s or something.

203
00:20:37,000 --> 00:20:38,000
I don't know.

204
00:20:38,000 --> 00:20:41,000
There was just such stuff going on.

205
00:20:41,000 --> 00:20:46,000
I want to help them.

206
00:20:46,000 --> 00:20:54,000
I want to not help them in a sense of condescending story,

207
00:20:54,000 --> 00:21:01,000
but be an ally, do what I can to help them

208
00:21:01,000 --> 00:21:05,000
to be supportive of their healing process.

209
00:21:05,000 --> 00:21:08,000
There's something I can do to own up

210
00:21:08,000 --> 00:21:12,000
to the responsibility that we all share.

211
00:21:12,000 --> 00:21:14,000
The Master University is actually on their land,

212
00:21:14,000 --> 00:21:16,000
I think it's like, I don't know.

213
00:21:16,000 --> 00:21:28,000
I don't know the history of it, but people don't appreciate the suffering that has gone on here.

214
00:21:28,000 --> 00:21:30,000
That's interesting.

215
00:21:30,000 --> 00:21:33,000
They're just nice people, and they have nice traditions.

216
00:21:33,000 --> 00:21:37,000
We smudge, we have the smoke at their meetings,

217
00:21:37,000 --> 00:21:41,000
and you put the smoke over you, and it's meant to represent

218
00:21:41,000 --> 00:21:45,000
purifying your thoughts, and it's a symbolic gesture,

219
00:21:45,000 --> 00:21:46,000
which I really like.

220
00:21:46,000 --> 00:21:49,000
I mean, Buddhists have symbolic gestures.

221
00:21:49,000 --> 00:21:55,000
The symbolic gesture, we're using the smoke to represent your good intentions.

222
00:21:55,000 --> 00:22:00,000
They put it in their eyes so that they look at things clearly,

223
00:22:00,000 --> 00:22:06,000
and they mouth so that they remind them to speak clearly.

224
00:22:06,000 --> 00:22:10,000
Really good traditions.

225
00:22:10,000 --> 00:22:14,000
That we have problems with some of the hunting and stuff that they do.

226
00:22:14,000 --> 00:22:18,000
It's not home roses, but I'm not so interested in their cultures.

227
00:22:18,000 --> 00:22:24,000
I am in helping them to find peace,

228
00:22:24,000 --> 00:22:30,000
not actively, but do whatever I can to support their journey to find peace.

229
00:22:30,000 --> 00:22:35,000
There's a lot of suffering, high suicide rates on reservations,

230
00:22:35,000 --> 00:22:37,000
drug abuse, alcohol abuse.

231
00:22:37,000 --> 00:22:39,000
I brought that up and they said,

232
00:22:39,000 --> 00:22:41,000
maybe it's not that big of a problem down here,

233
00:22:41,000 --> 00:22:45,000
but in the north it was apparently a big problem.

234
00:22:45,000 --> 00:22:51,000
I went to high school with lots of indigenous people.

235
00:22:51,000 --> 00:22:53,000
So there's that.

236
00:22:53,000 --> 00:22:55,000
There's the peace symposium we're having a meeting tomorrow.

237
00:22:55,000 --> 00:22:58,000
I went to the wellness center today, talked to Taren.

238
00:22:58,000 --> 00:23:00,000
She's really nice.

239
00:23:00,000 --> 00:23:03,000
They're going to do, they wanted to do something meditative,

240
00:23:03,000 --> 00:23:06,000
and maybe they still will.

241
00:23:06,000 --> 00:23:08,000
I hope they do.

242
00:23:08,000 --> 00:23:12,000
I'll have to go talk to her again about that, I think.

243
00:23:12,000 --> 00:23:16,000
Got my religious studies.

244
00:23:16,000 --> 00:23:19,000
Remember, I said I'm doing this paper on the Lotus Future.

245
00:23:19,000 --> 00:23:22,000
Well, I had to submit a proposal.

246
00:23:22,000 --> 00:23:25,000
Got it back today.

247
00:23:25,000 --> 00:23:29,000
I got it 95.

248
00:23:29,000 --> 00:23:34,000
And I shouldn't talk about it.

249
00:23:34,000 --> 00:23:36,000
That's good.

250
00:23:36,000 --> 00:23:40,000
So I can still do this scholarly thing.

251
00:23:40,000 --> 00:23:43,000
When I get my paper written,

252
00:23:43,000 --> 00:23:45,000
I'm going to post it on the internet

253
00:23:45,000 --> 00:23:49,000
and just have everybody out there,

254
00:23:49,000 --> 00:23:52,000
check it for grammar and stuff.

255
00:23:52,000 --> 00:23:54,000
That's what I'll do.

256
00:23:54,000 --> 00:23:59,000
Because he said, make sure someone reads your RTA said,

257
00:23:59,000 --> 00:24:00,000
make sure someone reads.

258
00:24:00,000 --> 00:24:03,000
You shouldn't be the only one who reads your paper before you submit it.

259
00:24:03,000 --> 00:24:07,000
So I'll just get you guys to read it.

260
00:24:07,000 --> 00:24:09,000
I did that with my book.

261
00:24:09,000 --> 00:24:13,000
My latest, the second part of my book,

262
00:24:13,000 --> 00:24:15,000
which I still haven't finished.

263
00:24:15,000 --> 00:24:22,000
I think there's still one chapter left, and then we'll finish.

264
00:24:22,000 --> 00:24:24,000
So nobody's coming on the hangout.

265
00:24:24,000 --> 00:24:28,000
I'll take that as a sign that I can go.

266
00:24:28,000 --> 00:24:33,000
Nothing else happening.

267
00:24:33,000 --> 00:24:36,000
What else is happening?

268
00:24:36,000 --> 00:24:40,000
There's too much stuff to think of.

269
00:24:40,000 --> 00:24:43,000
God, lists of stuff I have to do.

270
00:24:43,000 --> 00:24:46,000
Anyway, that's all for tonight.

271
00:24:46,000 --> 00:24:50,000
Thanks, guys. Have a good night.

272
00:24:50,000 --> 00:24:52,000
Thank you.

