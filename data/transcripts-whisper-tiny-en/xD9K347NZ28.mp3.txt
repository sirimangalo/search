Welcome back to Ask a Monk, today's question.
You said you don't recommend your practicing yoga, however you recommend walking meditation.
But in my opinion, you can do yoga very slowly and meditative mindfully.
So it's like walking meditation.
You agree and would you recommend this kind of yoga?
The only reason why we practice walking meditation really is because sitting around all
day is not good for the body.
And moreover is quite difficult to maintain because of the stress on the body.
So walking meditation is simply a relief of that strain and for the purposes of exercising
the body in the limited sense of allowing the blood to flow and the food to digest and
so on.
For instance, after you eat, if you go and sit down, the food in your body doesn't digest.
And some of the things that I went over in the how to meditate videos.
I don't think the same can be said for yoga.
It's of course obvious that you could practice yoga mindfully.
The question is why are you practicing yoga?
Why aren't you simply carrying out your daily life as normal?
The reason we do any sort of formal meditation is simply to limit the number of objects
that we have to be mindful of.
We sit down, we close our eyes.
It's not because there's something special about sitting down and closing your eyes that's
going to bring about some special state of enlightenment.
It's like controlling the variables in a laboratory experiment.
There's less to be taken to consideration and therefore it's easier to carry out your
experiments.
In the same way, with your eyes closed, it's easier to carry out the mindfulness.
The same can be said with walking meditation.
There's nothing special about it because it's slow or in some way meditative.
It's just that it's a piece by piece motion so you're able to watch one movement at a time.
You're able to watch your mind's reaction to each movement and because it's repetitive,
it forces your mind to be patient and it forces out a lot of our attachments and our
addictions and allows us to see quite clearly our addiction to pleasure and so on.
I don't personally think that the same can be said of yoga.
I think that it's a spiritual practice with its own theory and philosophy and goals and
I'm not 100% sure what those are but I get the feeling from what I've learned that they're
varied and so you couldn't say that yoga is for x, y or z.
It depends on which yoga teacher and which yoga tradition you're following but I think
you could practice yoga mindfully.
The question is why are you practicing yoga and I would say the answer to that question
probably makes it on some level incompatible with the your practice of Buddhism in the
sense that it's it's on a different path, a path which has its own spiritual tradition
and goals as I said so I've talked about yoga before but I know this has come up again
and probably is going to keep coming up so there's more thoughts on yoga so all the best.
