1
00:00:00,000 --> 00:00:16,680
There was a monastery in Anuratha Pura in Sri Lanka, which differs in opinion from those

2
00:00:16,680 --> 00:00:19,320
of the great monastery.

3
00:00:19,320 --> 00:00:27,840
So they held some views different than the views held by those of the great monastery.

4
00:00:27,840 --> 00:00:37,360
So now those people said that the ascetic practice does not belong to any of the three.

5
00:00:37,360 --> 00:00:45,080
It does not belong to Akusala or Akusala or Abhyagatau in determinate.

6
00:00:45,080 --> 00:00:54,960
But the commentator or the venerable Buddha Kosa said, if it is so, then it must be a concept

7
00:00:54,960 --> 00:01:02,400
and concept according to Buddhism is not reality, not Paramata.

8
00:01:02,400 --> 00:01:05,760
It is concept is not reality.

9
00:01:05,760 --> 00:01:10,960
So if it is not reality, how can it be practiced?

10
00:01:10,960 --> 00:01:21,600
So we cannot accept their opinion.

11
00:01:21,600 --> 00:01:33,840
And also if we take the ascetic practice to be concept, to be non-existing, then there

12
00:01:33,840 --> 00:01:47,160
will be contradiction with the word set by the Buddha proceeded to undertake the ascetic

13
00:01:47,160 --> 00:01:48,840
qualities.

14
00:01:48,840 --> 00:02:00,000
So the ascetic practices should not be taken as Panjati or as concept also.

15
00:02:00,000 --> 00:02:09,640
So ascetic practices are to be taken as either Kusala or Abhyagatau.

16
00:02:09,640 --> 00:02:26,160
And then the explanation of the words ascetic practices and those who talk about ascetic

17
00:02:26,160 --> 00:02:35,440
practices and so on and they are not difficult to understand a preacher of asceticism and

18
00:02:35,440 --> 00:02:36,440
so on.

19
00:02:36,440 --> 00:02:42,040
So some people practice themselves but do not encourage others to practice and some people

20
00:02:42,040 --> 00:02:49,720
only encourage others to practice and do not practice themselves.

21
00:02:49,720 --> 00:03:04,280
And the examples are given here like the venerable bhakula, right, on paragraph 82, one who

22
00:03:04,280 --> 00:03:09,360
has shaken office defalments with an ascetic practice but does not advise and instruct

23
00:03:09,360 --> 00:03:15,200
another in an ascetic practice like the elder bhakula.

24
00:03:15,200 --> 00:03:19,880
So he practiced himself but he did not encourage others to practice.

25
00:03:19,880 --> 00:03:29,040
And then the other one is Upananda, he encouraged others to practice but he did not practice

26
00:03:29,040 --> 00:03:38,720
himself.

27
00:03:38,720 --> 00:03:45,200
And then the other one who did not practice himself and who did not encourage others like

28
00:03:45,200 --> 00:04:00,840
Laludari and the last one is the Dhamma Sayyinapati, what is that?

29
00:04:00,840 --> 00:04:06,840
General of the Dhamma, you know who?

30
00:04:06,840 --> 00:04:13,680
The general of the Dhamma, Saripuddha, so Saripuddha is always called in Pali Dhamma

31
00:04:13,680 --> 00:04:24,360
Sayyinapati, general of the Dhamma.

32
00:04:24,360 --> 00:04:43,080
And Ananda is called the treasure of the Dhamma, he is the keeper of the Dhamma.

33
00:04:43,080 --> 00:04:49,040
And then ascetic states, the five states that go with the volition of an ascetic practice

34
00:04:49,040 --> 00:04:56,160
that is to say, fewness of wishes, contentment, effishment, seclusion, and that specific

35
00:04:56,160 --> 00:05:01,560
quality are called ascetic states because of the words, depending on fewness of wishes

36
00:05:01,560 --> 00:05:02,960
and so on.

37
00:05:02,960 --> 00:05:13,240
Now, I do not agree with the translation, that specific quality.

38
00:05:13,240 --> 00:05:27,200
The Pali word here is strange words but it means desire for these practices, desire for

39
00:05:27,200 --> 00:05:37,280
these wholesome states, not that specific quality, it is desire to practice or desire for

40
00:05:37,280 --> 00:05:40,520
these wholesome states.

41
00:05:40,520 --> 00:05:47,120
So it is actually, it is knowledge, it is explained later in paragraph 84, that specific

42
00:05:47,120 --> 00:05:52,960
quality is knowledge, so here in by means of non-grid, a man shakes of creed for things

43
00:05:52,960 --> 00:05:57,880
that are forbidden by means of non-delusion, that means knowledge.

44
00:05:57,880 --> 00:06:02,920
He shakes of the delusion that hides the dangers in those same things and by means of

45
00:06:02,920 --> 00:06:08,280
non-grid he shakes of indulgence and pressure due to sense desires that occur under the

46
00:06:08,280 --> 00:06:14,520
heading or using what is allowed and so on.

47
00:06:14,520 --> 00:06:34,240
Now, 13 practices and who can practice which first, which is suitable for which person.

48
00:06:34,240 --> 00:06:40,840
So for one of greedy temperament and for one of the looted temperament, there are six

49
00:06:40,840 --> 00:06:48,520
temperaments, we will study them in the next chapter, so because the cultivation of ascetic

50
00:06:48,520 --> 00:06:56,760
practices is both the difficult, difficult progress, actually it means a difficult practice,

51
00:06:56,760 --> 00:07:01,320
it is not an easy thing to practice, to take up ascetic practices.

52
00:07:01,320 --> 00:07:08,520
So it is a difficult practice and an abiding in a face, abiding in a face man and

53
00:07:08,520 --> 00:07:14,360
great subsides with the difficult practice, why delusion is called rid of in those

54
00:07:14,360 --> 00:07:19,400
diligent by a face man or the cultivation of the forest dwellers practice and the

55
00:07:19,400 --> 00:07:29,080
tree dwellers practice here are suitable for one of hating temperament, that means also

56
00:07:29,080 --> 00:07:37,800
for one of hating temperament, for hate to subsides in one who dwells there without coming

57
00:07:37,800 --> 00:07:47,400
into conflict, he lives alone, he does not get a quarrel with any person, he might

58
00:07:47,400 --> 00:08:02,200
quarrel with himself and then as to groups and also singly and now six as to groups,

59
00:08:02,200 --> 00:08:08,040
ascetic practices are in fact only eight, that is to say three principle and five individual

60
00:08:08,040 --> 00:08:15,960
practices, the three namely the house to house seekers practice, the one session as practice

61
00:08:15,960 --> 00:08:22,040
and the open air dwellers practice are principle practices, for one who keeps the house

62
00:08:22,040 --> 00:08:27,480
to house seekers practice will keep the arms food eaters practice and the bold food eaters

63
00:08:27,480 --> 00:08:33,560
practice and the later food refuses practice, will be well kept by one who keeps the one session

64
00:08:33,560 --> 00:08:42,840
as practice, so when one is taken the others are virtually taken and what need has one who keeps

65
00:08:42,840 --> 00:08:47,880
the open air dwellers practice to keep the three to dwellers practice or the any bad

66
00:08:47,880 --> 00:08:53,720
users practice, so there are these three principle practices that together with the five individual

67
00:08:53,720 --> 00:08:59,640
practices that is to say before as dwellers practice and so on come to eight only, so 13

68
00:08:59,640 --> 00:09:08,760
can be counted as eight only when we take the principal ones and then two concerned with robes,

69
00:09:08,760 --> 00:09:15,640
five concerned with arms food and so on, but what is interesting or what is important is

70
00:09:15,640 --> 00:09:33,160
who can practice which practices, so singly that is on page 83 for a graph 90, with 13 for

71
00:09:33,160 --> 00:09:41,480
bikhus, so months can practice all 13 practices, eight for bikhus, the nuns can practice only

72
00:09:41,480 --> 00:09:50,360
eight, 12 for novices and so many years novices can practice 12, seven for female

73
00:09:50,360 --> 00:10:03,160
provisioners and female novices, there are two kinds of female, female, I don't know what to say

74
00:10:03,160 --> 00:10:17,560
that, provisioners and novices, if a woman or a girl wants to become a nun, then she must spend

75
00:10:17,560 --> 00:10:27,000
some time on the provision about two years keeping only six precepts and then she becomes

76
00:10:27,000 --> 00:10:36,760
a ceremony in novices and then after that she becomes a nun, a bikuni, so for such

77
00:10:37,320 --> 00:10:46,920
persons seven are allowable and two for male and female lay followers, lay people can also practice

78
00:10:47,480 --> 00:10:54,040
some of these practices and there are two, they can practice, so they are on altogether 42,

79
00:10:54,040 --> 00:10:59,560
if there is a channel ground in the open that complies with the forest dwellers practice,

80
00:11:00,760 --> 00:11:06,200
one bikhu is able to put all the ascetic practices into effect simultaneously,

81
00:11:06,200 --> 00:11:11,960
so a monk can practice all the 13 practices, if there is a channel ground in the open

82
00:11:12,920 --> 00:11:23,160
and it is away from the village by about 1,000, 1,000 yards, then a monk living there can

83
00:11:23,160 --> 00:11:29,560
practice all these 13 practices simultaneously, but the two namely the forest dwellers practice

84
00:11:29,560 --> 00:11:35,960
and the later food refuses practice are forbidden to bikinis by training precepts, bikinis do not

85
00:11:35,960 --> 00:11:50,280
have to keep those precepts and so they cannot keep those ascetic practices, bikinis must not be

86
00:11:50,280 --> 00:11:58,920
on their own, they must live not too close but they must live close to the monks and so they

87
00:11:58,920 --> 00:12:04,200
cannot practice forest dwellers practice and it is hard for them to observe the three namely the

88
00:12:04,200 --> 00:12:09,160
open air dwellers practice, the three dwellers practice and the channel ground dwellers practice

89
00:12:09,160 --> 00:12:15,320
because bikuni is not allowed to live without a companion, so bikuni is not allowed to live alone,

90
00:12:15,320 --> 00:12:26,840
she must have a female companion and it is hard to find a female companion with like desire for

91
00:12:26,840 --> 00:12:31,800
such a place and even if available she would not escape having to live in company,

92
00:12:34,360 --> 00:12:44,200
to keep the the the purpose in keeping these practices is to enjoy seclusion and if you have to

93
00:12:44,200 --> 00:12:50,920
live with another person and you lose that, this being so the purpose of cultivating the ascetic

94
00:12:50,920 --> 00:12:57,240
practice would scarcely be served, it is because they are reduced by five owing to this

95
00:12:57,240 --> 00:13:03,400
inability to make use of certain of them that they are to be understood as eight only for bikinis,

96
00:13:03,400 --> 00:13:10,120
so bikinis can practice eight of them, except for the triple rope wearer's practice all the

97
00:13:10,120 --> 00:13:18,200
other twelve as stated should be understood to be for novices, so novices, male novices can practice

98
00:13:18,200 --> 00:13:33,800
twelve of them because novices cannot use the third rope, the double layer rope and that is used

99
00:13:33,800 --> 00:13:44,680
by or that is allowed for months only, so novices do not use the the third rope, so they cannot

100
00:13:44,680 --> 00:13:55,320
practice the ascetic practice of having three ropes only and all the other seven for female

101
00:13:55,320 --> 00:14:08,520
provisioners and female novices, so female provisioners and female novices can practice seven out

102
00:14:08,520 --> 00:14:16,200
of eight for nuns, the two namely the one session as practice and the bold food eaters practice

103
00:14:16,200 --> 00:14:23,880
are proper for male and female relief all who has to imply, so lead people can practice one session

104
00:14:23,880 --> 00:14:33,880
as practice eating at one sitting only or the bold food eaters practice eating in one bowl only,

105
00:14:33,880 --> 00:14:39,720
so these these two lead people can practice in this way there are two ascetic practices,

106
00:14:46,200 --> 00:14:48,840
this is the commentary as to the groups and also singly,

107
00:14:48,840 --> 00:15:05,720
so these 13 practices are not much practice nowadays and those living in villages or in towns

108
00:15:07,080 --> 00:15:14,520
cannot practice most of these practices, but those who live in the forest monasteries can

109
00:15:14,520 --> 00:15:26,280
practice many of them and there are still monks who practice many of them like living in the

110
00:15:26,280 --> 00:15:36,840
cemetery or living under a tree and eating in one bowl and not lying down and keeping only three

111
00:15:36,840 --> 00:15:47,560
ropes and these practices are meant for enforcement of mental defilements,

112
00:15:49,080 --> 00:15:57,320
we cannot do with mental defilements altogether by these practices, but we can make them

113
00:15:57,320 --> 00:16:10,520
reduced, we can scrape them little by little by these practices and so according to the

114
00:16:10,520 --> 00:16:19,640
Visudemanga, a monk must first purify his moral conduct, a monk must have pure sealer

115
00:16:19,640 --> 00:16:27,800
and then he must practice some of these ascetic practices and then next he will go on to practicing

116
00:16:28,920 --> 00:16:43,560
meditation, so these two chapters are for the basic practice before one goes to practice

117
00:16:43,560 --> 00:16:51,960
the calm meditation or insight meditation.

118
00:16:59,560 --> 00:17:04,840
So next week we'll move on to concentration. We go to concentration, yes.

119
00:17:04,840 --> 00:17:16,360
Very detailed instructions for the practice, for taking up the practice of meditation.

120
00:17:16,360 --> 00:17:33,240
But others, those ascetic practices and those sort of absolute requirement to before you can go on to

121
00:17:33,800 --> 00:17:44,600
concentration on the next step. Is it the basic? How important are they and also they can

122
00:17:44,600 --> 00:18:00,280
respect to the everyday and the people and not do that? What is the relevant respect for them?

123
00:18:02,040 --> 00:18:10,520
Shurity of morals is the absolute necessary because without the purity of morals,

124
00:18:10,520 --> 00:18:18,920
one cannot get concentration when he practices meditation. But the ascetic practices are just

125
00:18:19,880 --> 00:18:31,080
an extra practices. So even if you or a monk do not practice the ascetic practices,

126
00:18:31,080 --> 00:18:38,760
still it is possible for you or for that monk to practice with meditation provided that you

127
00:18:38,760 --> 00:18:49,720
have a moral purity. So moral purity is a very necessary or essential for the practice of

128
00:18:49,720 --> 00:18:58,040
meditation. That is because if there is no moral purity then we suffer from

129
00:18:58,040 --> 00:19:09,400
from remorse or from feelings of guilt. So suppose my my sila is not pure. So when my sila is not

130
00:19:09,400 --> 00:19:16,840
pure, I have this feeling of guilt. People think that I am a good monk, but in fact I am bad.

131
00:19:16,840 --> 00:19:25,480
And so when I try to practice meditation, this thinking comes to me again and again and torment me.

132
00:19:25,480 --> 00:19:32,040
And so when there is this feeling of guilt, there can be no

133
00:19:36,040 --> 00:19:41,800
happiness or no joy and there can be no tranquility, no concentration and so on.

134
00:19:42,680 --> 00:19:53,480
And in one of the sodas, the different, I mean the successive stages leading to the realization

135
00:19:53,480 --> 00:20:03,080
is given and the first one is the purely moral purity. So moral purity helps us to be free from

136
00:20:03,080 --> 00:20:13,560
remorse and the freedom from remorse promotes joy and joy promotes happiness, happiness promotes

137
00:20:14,840 --> 00:20:20,360
tranquility and tranquility promotes another kind of comfort of happiness.

138
00:20:20,360 --> 00:20:26,440
And then the happiness of mind and body promotes samadhi and concentration.

139
00:20:26,440 --> 00:20:32,520
So in order to get concentration, we need some kind of comfort or happiness,

140
00:20:33,320 --> 00:20:39,000
happiness in the sense of peacefulness. So moral purity is very important.

141
00:20:41,000 --> 00:20:47,240
But the ascetic practice is just an extra practices.

142
00:20:47,240 --> 00:21:01,720
And for lead people, it is not difficult to get moral purity because even though they are more

143
00:21:01,720 --> 00:21:10,280
habits are not pure in the past. So before the practice of meditation, they can make up their mind

144
00:21:10,280 --> 00:21:19,640
that they will not do breakthroughs in the future and they will keep their moral habits pure

145
00:21:19,640 --> 00:21:26,760
and take precepts and then that's all there is to it. But for months it is not so easy because they

146
00:21:26,760 --> 00:21:43,560
have to do some, there are some offenses which cannot be exonerated just by confession.

147
00:21:44,120 --> 00:21:50,920
Some require confession only and some require confession and then giving up of the things

148
00:21:50,920 --> 00:22:01,800
involving it and then some require to stay for underprovision for as long as one hides his own

149
00:22:01,800 --> 00:22:14,280
offense. Suppose I touch a woman with with lusty thoughts. So that is an offense and if I do not

150
00:22:14,280 --> 00:22:23,720
declare this offense to another monk, say for 10 days, then I must be underprovision for 10 days.

151
00:22:23,720 --> 00:22:29,560
And if I cover it up for one month, then I must be underprovision for one month and so on.

152
00:22:31,160 --> 00:22:43,240
And then I need monks to assemble and do some kind of formal act to take me back into the

153
00:22:43,240 --> 00:22:51,800
fold of Sangha. So such offenses are not easy to get to get rid of. So for a monk it is more

154
00:22:53,160 --> 00:22:57,640
more difficult to get purity of morals than for lay people.

155
00:22:57,640 --> 00:23:09,800
Thank you because the main precepts are fewer. Leave results of fewer. Yes, the minimum requirement

156
00:23:09,800 --> 00:23:16,920
for lay people is only five precepts, not killing, not stealing, no sexual misconduct, no lying

157
00:23:16,920 --> 00:23:24,920
and no intoxicants. So these are the five. And some people may break one or two of these rules,

158
00:23:24,920 --> 00:23:36,120
but before the practice of meditation that person really sincerely decided that decides to refrain

159
00:23:36,120 --> 00:23:42,920
from breaking these rules in the future and to keep his moral conduct pure during meditation,

160
00:23:42,920 --> 00:23:50,520
then that's all right for him. So he is said to be pure in his moral habits, but a monk must do

161
00:23:50,520 --> 00:23:58,920
something more than just making up his mind or confession. So it is more difficult for a monk to

162
00:23:58,920 --> 00:24:02,920
get purity of morals than lay persons.

163
00:24:02,920 --> 00:24:23,720
Because a monk has broken the rule, laid down by the Buddha. The five precepts are not laid

164
00:24:23,720 --> 00:24:31,880
down by the Buddha, but they are something like universal precepts. But the rules to be followed

165
00:24:31,880 --> 00:24:37,880
by monks are laid down by the Buddha. And so when I break a rule, I break the rule and also

166
00:24:39,640 --> 00:24:46,120
I show some disrespect for the Buddha, for the one who fully down the rules. So there's a double

167
00:24:47,160 --> 00:24:54,120
something like a double offense there. Breaking rule is one offense and then disrespect for the

168
00:24:54,120 --> 00:25:05,480
Buddha is another. Two things. So monks have to get free from such offenses by following some

169
00:25:05,480 --> 00:25:14,360
procedure. Some offenses just by confession confessing to another monk, but some require

170
00:25:15,640 --> 00:25:23,880
something like being underprovision for some days or some months. So it is more difficult for

171
00:25:23,880 --> 00:25:34,600
monks to be pure in moral, more friendly people. That means first you must ask the

172
00:25:34,600 --> 00:25:45,480
sangha to assemble. And then formally, formally give you a formally

173
00:25:45,480 --> 00:25:53,640
recognized US being underprovision. And when you are underprovision, you are not to enjoy

174
00:25:54,520 --> 00:26:04,280
being given respect by younger monks. And at the dining hall, you have to sit at the end of a

175
00:26:04,280 --> 00:26:10,760
line, although you may be the eldest of the monks there. So something like that. It kind of

176
00:26:10,760 --> 00:26:23,880
punishment. And then at the end, you need 20 months to assemble to perform a formal act of sangha

177
00:26:24,440 --> 00:26:33,160
to take you back into the folds of sangha. So while underprovision, you do not enjoy all the

178
00:26:33,160 --> 00:26:46,280
privilege of a monk. And you are not to not to accept respect from younger monks. And you are

179
00:26:46,280 --> 00:26:56,840
not to sleep under the same roof with another monk. So it is more difficult for a monk than a

180
00:26:56,840 --> 00:27:03,720
labors and to get purity of more hours before the practice of meditation.

181
00:27:08,520 --> 00:27:14,760
That means that another question that you have mentioned to me here about the concept

182
00:27:14,760 --> 00:27:31,640
idea, is that controversy or confusion arises because they're some kind of practices

183
00:27:31,640 --> 00:27:44,200
that might be similar to the ascetic practices, which is done merely for the purpose of

184
00:27:46,280 --> 00:27:54,680
attaining a certain type of power, or, you know, the seagulls, and psychic power, and magic of hours

185
00:27:54,680 --> 00:28:01,400
and so on. But the person who is doing those practices, which are similar to the ascetic practices,

186
00:28:03,480 --> 00:28:11,720
may not have the virtue and, you know, those in mind, then practices to do character of the world

187
00:28:11,720 --> 00:28:18,520
in that world of power. That's very similar. And then it might have a huge system about the

188
00:28:18,520 --> 00:28:26,440
between that and the ascetic practices that I mentioned here. And then by the definition

189
00:28:26,440 --> 00:28:34,520
already, you know, this is not done with the wholesomeness and so on, that it is not an ascetic

190
00:28:34,520 --> 00:28:45,000
practice anymore. I think the difference of opinion is whether the ascetic practices

191
00:28:45,000 --> 00:28:55,720
are to be included in the categories of wholesome and neither wholesome nor wholesome.

192
00:28:57,000 --> 00:29:06,200
And those people took ascetic practices to be just outside of these three, and so to them,

193
00:29:06,200 --> 00:29:21,240
it is just a concept. So there is no reality to represent these practices. But according to the

194
00:29:21,240 --> 00:29:33,480
opinion of the visual demaga and so opinion of common opinion of elders, the ascetic practice

195
00:29:33,480 --> 00:29:48,040
is reality because when you take up these practices, then you have the volition in your mind

196
00:29:48,040 --> 00:29:56,600
and also the understanding or the knowledge of it. And so they are the units of reality,

197
00:29:56,600 --> 00:30:06,600
but are much higher. But those other monks took the practice as to be just concepts. And so

198
00:30:07,880 --> 00:30:20,920
the argument from the side of when we go to Goza is if they are concept, then concept has no

199
00:30:20,920 --> 00:30:30,280
existence of its own. It exists only in the minds imagination and so they cannot be realities.

200
00:30:31,880 --> 00:30:41,480
But the ascetic practices are or belong to reality, one of the four ultimate truths,

201
00:30:41,480 --> 00:30:52,520
the consciousness, mental factors, material properties and neverness. So ascetic practices are not

202
00:30:54,600 --> 00:31:04,680
concepts, but they are realities. And so they belong to either Goza, wholesome or abiagata

203
00:31:04,680 --> 00:31:17,000
indeterminate. They cannot be unwholesome and they cannot be outside of wholesome unwholesome

204
00:31:17,000 --> 00:31:42,040
and indeterminate either.

205
00:31:42,040 --> 00:31:55,160
That's right. But they cannot be called ascetic practices because they promote mental

206
00:31:55,160 --> 00:32:17,160
development, they promote creative or some kind of attachment and so on.

207
00:32:25,160 --> 00:32:49,160
So they cannot be called ascetic. So they cannot be called ascetic. So they cannot be called ascetic.

208
00:32:49,160 --> 00:33:13,160
So they cannot be called ascetic. So they cannot be called ascetic. So they cannot be called ascetic.

209
00:33:13,160 --> 00:33:17,160
.

210
00:33:17,160 --> 00:33:21,160
.

211
00:33:21,160 --> 00:33:25,160
.

212
00:33:25,160 --> 00:33:29,160
.

213
00:33:29,160 --> 00:33:33,160
..

214
00:33:33,160 --> 00:33:37,160
..

215
00:33:37,160 --> 00:33:41,160
..

