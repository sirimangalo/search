1
00:00:00,000 --> 00:00:10,480
How to meditate by you to Domobiku, read by Adirokes, chapter 3, walking meditation.

2
00:00:10,480 --> 00:00:15,400
In this chapter, I will explain the technique of walking meditation.

3
00:00:15,400 --> 00:00:20,960
As with sitting meditation, the focus of walking meditation is on keeping the mind in the

4
00:00:20,960 --> 00:00:27,520
present moment and aware of phenomena as they arise in order to create clear awareness

5
00:00:27,520 --> 00:00:29,640
of one's reality.

6
00:00:29,640 --> 00:00:35,240
Even its similarity to sitting meditation, one might wonder what purpose walking meditation

7
00:00:35,240 --> 00:00:36,720
serves.

8
00:00:36,720 --> 00:00:42,160
If one is not able to practice walking meditation, one will still be able to gain benefit

9
00:00:42,160 --> 00:00:44,560
from sitting meditation.

10
00:00:44,560 --> 00:00:50,480
But walking meditation has several unique benefits that make it a good complement and precursor

11
00:00:50,480 --> 00:00:52,640
to sitting meditation.

12
00:00:52,640 --> 00:00:56,400
I will enumerate the five traditional benefits here.

13
00:00:56,400 --> 00:01:05,840
These five benefits are taken from Lichten Kama Sinta in the Angutura Nikaya, 5.1.3.9.

14
00:01:05,840 --> 00:01:09,840
First, walking meditation provides physical fitness.

15
00:01:09,840 --> 00:01:16,960
If we spend all of our time sitting still, our bodies will become weak and incapable of exertion.

16
00:01:16,960 --> 00:01:22,640
Walking meditation maintains basic fitness even for an ardent meditator and can be seen

17
00:01:22,640 --> 00:01:26,880
as a supplement for physical exercise.

18
00:01:26,880 --> 00:01:32,080
Second, walking meditation cultivates patience and endurance.

19
00:01:32,080 --> 00:01:38,520
Since walking meditation is active, it doesn't require as much patience as sitting still.

20
00:01:38,520 --> 00:01:45,200
It is a useful intermediary between ordinary activity and formal sitting meditation.

21
00:01:45,200 --> 00:01:50,480
Third, walking meditation helps to cure sickness in the body.

22
00:01:50,480 --> 00:01:56,280
Whereas sitting meditation puts the body in a state of homeostasis, walking meditation

23
00:01:56,280 --> 00:02:04,240
stimulates blood flow and biological activity while being gentle enough to avoid aggravation.

24
00:02:04,240 --> 00:02:12,600
It also helps to relax the body, reducing tension and stress by moving slowly and methodically.

25
00:02:12,600 --> 00:02:17,680
Walking meditation is thus useful in helping to overcome sicknesses like heart disease

26
00:02:17,680 --> 00:02:22,720
and arthritis, as well as maintaining basic general health.

27
00:02:22,720 --> 00:02:27,920
Fourth, walking meditation aids in healthy digestion.

28
00:02:27,920 --> 00:02:33,560
The greatest disadvantage to sitting meditation is that it can actually inhibit proper digestion

29
00:02:33,560 --> 00:02:35,520
of food.

30
00:02:35,520 --> 00:02:41,440
Walking meditation, on the other hand, stimulates the digestive system, allowing one to

31
00:02:41,440 --> 00:02:48,320
continue one's meditation practice without having to compromise one's physical health.

32
00:02:48,320 --> 00:02:54,080
Fifth, walking meditation helps one to cultivate balanced concentration.

33
00:02:54,080 --> 00:02:59,160
If one were to only practice sitting meditation, one's concentration may be either

34
00:02:59,160 --> 00:03:05,360
too weak or too strong, leading either to distraction or lethargy.

35
00:03:05,360 --> 00:03:12,320
As walking meditation is dynamic, it allows both body and mind to settle naturally.

36
00:03:12,320 --> 00:03:18,080
If done before sitting meditation, walking meditation will help ensure a balanced state of

37
00:03:18,080 --> 00:03:21,640
mind during the subsequent sitting.

38
00:03:21,640 --> 00:03:25,440
The method of walking meditation is as follows.

39
00:03:25,440 --> 00:03:32,520
One, the feet should be close together, almost touching, and should stay side by side throughout

40
00:03:32,520 --> 00:03:34,360
the meditation.

41
00:03:34,360 --> 00:03:39,720
Either one in front of the other, nor with great space between their pads.

42
00:03:39,720 --> 00:03:47,400
Two, the hand should be clasped, right holding left, either in front or behind the body.

43
00:03:47,400 --> 00:03:54,080
Three, the eyes should be open throughout the meditation, and one's gaze should be fixed

44
00:03:54,080 --> 00:04:00,520
on the path ahead, about two meters or six feet in front of the body.

45
00:04:00,520 --> 00:04:04,240
Four, one should walk in a straight line.

46
00:04:04,240 --> 00:04:10,440
Somewhere between three to five meters, or ten to fifteen feet in length.

47
00:04:10,440 --> 00:04:16,280
Five, one begins by moving the right foot forward one foot length, with the foot parallel

48
00:04:16,280 --> 00:04:18,120
to the ground.

49
00:04:18,120 --> 00:04:23,600
The entire foot should touch the ground at the same time, with back of the heel in line,

50
00:04:23,600 --> 00:04:28,400
with, and to the right of, the toes of the left foot.

51
00:04:28,400 --> 00:04:33,960
Six, the movement of each foot should be fluid and natural.

52
00:04:33,960 --> 00:04:40,320
A single arcing motion from beginning to end, with no breaks or abrupt change in direction

53
00:04:40,320 --> 00:04:42,200
of any kind.

54
00:04:42,200 --> 00:04:48,440
Seven, one then moves the left foot forward, passing the right foot to come down with

55
00:04:48,440 --> 00:04:55,120
the back of the heel in line with, and to the left of, the toes of the right foot, and

56
00:04:55,120 --> 00:04:56,600
so on.

57
00:04:56,600 --> 00:04:59,880
One foot length for each step.

58
00:04:59,880 --> 00:05:03,840
Eight, as one moves each foot, one should

59
00:05:03,840 --> 00:05:09,240
make a mental note just as in the sitting meditation, using a mantra that captures the

60
00:05:09,240 --> 00:05:12,840
essence of the movement as it occurs.

61
00:05:12,840 --> 00:05:21,600
The word, in this case, is stepping right, when moving the right foot, and stepping left,

62
00:05:21,600 --> 00:05:23,640
when moving the left foot.

63
00:05:23,640 --> 00:05:30,640
Nine, one should make the mental note at the exact moment of each movement, neither before

64
00:05:30,640 --> 00:05:32,960
or after the movement.

65
00:05:32,960 --> 00:05:38,960
If the mental note, stepping right, is made before the foot moves, one is noting something

66
00:05:38,960 --> 00:05:41,520
that has not yet occurred.

67
00:05:41,520 --> 00:05:47,600
If one moves the foot first, and then notes, stepping right, one is noting something in

68
00:05:47,600 --> 00:05:49,640
the past.

69
00:05:49,640 --> 00:05:55,200
Either way, this cannot be considered meditation, as there is no awareness of reality in

70
00:05:55,200 --> 00:05:57,360
either case.

71
00:05:57,360 --> 00:06:04,160
To clearly observe the movements as they occur, one should note step at the beginning

72
00:06:04,160 --> 00:06:13,120
of the movement, just as the foot leaves the floor, being as the foot moves forward, and

73
00:06:13,120 --> 00:06:17,800
right the moment when the foot touches the floor again.

74
00:06:17,800 --> 00:06:23,000
The same method should be employed when moving the left foot, and one's awareness should

75
00:06:23,000 --> 00:06:30,400
move between the movements of each foot, from one end of the path to the other.

76
00:06:30,400 --> 00:06:36,520
Upon reaching the end of the walking path, turn around and walk in the other direction.

77
00:06:36,520 --> 00:06:42,640
The method of turning while maintaining clear awareness is to first stop, bringing whichever

78
00:06:42,640 --> 00:06:50,560
foot is behind to stand next to the foot that is in front, saying to oneself, stopping,

79
00:06:50,560 --> 00:06:55,440
stopping, stopping as the foot moves.

80
00:06:55,440 --> 00:07:02,520
Once one is standing still, one should become aware of the standing position as standing,

81
00:07:02,520 --> 00:07:09,000
standing, standing, then begin to turn around as follows.

82
00:07:09,000 --> 00:07:15,160
One, lift the right foot completely off the floor and turn it 90 degrees to place it again

83
00:07:15,160 --> 00:07:20,640
on the floor, noting one's turning.

84
00:07:20,640 --> 00:07:28,120
It is important to extend the word to cover the whole of the movement, so the turn should

85
00:07:28,120 --> 00:07:35,280
be at the beginning of the movement, and the ping should be at the end, as the foot touches

86
00:07:35,280 --> 00:07:37,040
the floor.

87
00:07:37,040 --> 00:07:43,920
Two, lift the left foot off the floor and turn it 90 degrees to stand by the right foot,

88
00:07:43,920 --> 00:07:48,080
including just the same, turning.

89
00:07:48,080 --> 00:07:59,120
Three, repeat the movements of both feet one more time, turning right foot, turning left

90
00:07:59,120 --> 00:08:06,920
foot, and then note, standing, standing, standing.

91
00:08:06,920 --> 00:08:15,320
Four, continue with the walking meditation in the opposite direction, noting, stepping

92
00:08:15,320 --> 00:08:20,360
right, stepping left, as before.

93
00:08:20,360 --> 00:08:27,360
During walking meditation, if thoughts, feelings, or emotions arise, one can choose to ignore

94
00:08:27,360 --> 00:08:34,680
them, bringing the mind back to the feet in order to maintain focus and continuity.

95
00:08:34,680 --> 00:08:41,720
If, however, they become a distraction, one should stop moving, bringing the backfoot forward

96
00:08:41,720 --> 00:08:53,600
to stand with the front foot, saying to oneself, stopping, stopping, stopping, then standing,

97
00:08:53,600 --> 00:09:03,240
standing, standing, and begin to contemplate the distraction as in sitting meditation, thinking,

98
00:09:03,240 --> 00:09:21,160
thinking, pain, pain, pain, angry, sad, bored, happy, etc., according to the experience.

99
00:09:21,160 --> 00:09:27,480
Once the object of attention disappears, continue with the walking as before, stepping

100
00:09:27,480 --> 00:09:31,280
right, stepping left.

101
00:09:31,280 --> 00:09:36,920
In this way, one simply paces back and forth, walking in one direction until reaching the

102
00:09:36,920 --> 00:09:44,800
end of the designated path, then turning around and walking in the other direction.

103
00:09:44,800 --> 00:09:50,880
Generally speaking, one should try to balance the amount of time spent in walking meditation,

104
00:09:50,880 --> 00:09:56,880
with the amount of time spent in sitting meditation, to avoid partiality to one or the

105
00:09:56,880 --> 00:09:58,960
other posture.

106
00:09:58,960 --> 00:10:04,440
If one were to practice 10 minutes of walking meditation, for example, one should follow

107
00:10:04,440 --> 00:10:08,240
it with 10 minutes of sitting meditation.

108
00:10:08,240 --> 00:10:12,960
This concludes the explanation of how to practice walking meditation.

109
00:10:12,960 --> 00:10:18,560
Again, I urge you not to be content with simply reading this book.

110
00:10:18,560 --> 00:10:26,000
Please, try the meditation techniques for yourself and see the benefits they bring for yourself.

111
00:10:26,000 --> 00:10:32,160
Thank you for your interest in the meditation practice, and again, I wish you peace, happiness,

112
00:10:32,160 --> 00:11:02,000
and freedom from suffering.

