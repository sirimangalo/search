I accidently stepped on a baby rabbit and killed it while working in the yard today.
I immediately felt an intense confusing mix of thoughts and feelings.
Mostly killed, grief, foolishness, unworthiness and so on.
I might have worked through these.
The interesting question for me here is why we feel guilt.
Right?
I mean, it's not baffling or anything.
It's just curious as to why specifically, I'm interested in exploring why we feel guilt.
We feel bad.
We may not even feel guilt.
But at first, the first part is the feeling bad.
And I think we have to separate that out.
Because I think initially, there's just a horror that something has died.
When we hear about the baby rabbit being killed, we all feel bad.
It's awful to think of a baby rabbit being stepped on.
And I think you have to separate that out.
Because otherwise, it makes you think that your guilt is somehow justified.
So, I think that's where the...
Well, first of all, that's a very useful, I think, in coming to terms with it.
Our ability to separate these two.
The awful feeling that something like a baby bunny rabbit,
probably the epitome of cuteness, has been killed versus the judgment that we have killed this baby rabbit.
Because there's a disconnect there.
It's a non sequitur, meaning it's not related.
The fact that the baby rabbit has died because you're stepped on it.
It doesn't necessitate the conclusion that you are morally responsible.
Why would you be...
We think about it logically.
Why would you be held morally responsible for stepping on a baby rabbit?
Now, there may be answers there.
Maybe you knew there were baby rabbits in your backyard.
And people warned you about this and said,
oh, trancing around in the backyard, because there's baby rabbits, and you said, I don't care,
you know, whatever.
And you got all drunk and went trancing around in your backyard.
You didn't get drunk. Leave that out.
But you went trancing around in your backyard,
jumping here and there and everywhere.
And lo and behold, you stepped on the baby rabbit.
In the spirit of modern legal leaves or modern jurisprudence,
you'd be guilty of negligence, right?
So from a Buddhist point of view, if we were taking a page out of the jurisprudence book,
we would say that you're a Carmicling negligent.
And I think that would stick, Carmicling.
That's really an awful sort of mind state to disregard the potential for stepping on baby rabbits.
But much more likely is that you had no idea that there were baby rabbits out there.
You were not being overly unmindful.
And so that's something you have to keep in mind that how mindful you are,
can often affect these situations.
So if you're totally unmindful, no, you're going to head in the clouds.
It's much easier to step on the baby rabbit,
whereas if you're mindful and aware of what you're doing,
it's less likely.
So your guilty of that sort of thing, your potentially guilty of that sort of thing.
But still, it may be that you were not unmindful,
but you just were otherwise engaged because you don't always look at your feet.
So you were doing something else and walking,
and you could even be aware of your foot moving potentially.
But you happen to step on in the killer baby rabbit.
Now, why is that your fault?
I think thinking about that helps.
It helps you to realize that it's not your fault,
because otherwise we get the sense that I wouldn't feel guilty about it if it weren't my fault.
It's not actually the case, and that's what's interesting about this,
because you'd feel that if I didn't do anything wrong,
why do I feel guilty?
That makes you feel like you deserve it.
You know, I must have done something wrong because I feel guilty,
and clearly that's not the case.
And when you break it apart as with everything,
why we like certain things, why we hate certain things,
why we worry about certain things,
and now why we feel guilty about certain things.
It really has all to do with our the imprecision of our understanding of the situation.
So once you deeply understand the situation by breaking it up in this way,
and you can do that empirically or experientially phenomenologically,
as these things come up in your mind,
the horror of stepping on the baby rabbit,
the memory of the feeling of stepping on the baby rabbit,
maybe the sounds that it made, whatever.
And then the thoughts about the baby rabbit's mother,
and the pain that the baby rabbit went through,
and then the anger and the frustration and the sadness,
and then the guilt.
You'll be able to break all that up.
You'll find two things.
And so far I've only talked about the first one,
and that is that most likely you don't even deserve to feel guilty.
You don't deserve to punish yourself for it.
There's no karmic potency to that act,
because there was no intention.
But the second thing is,
and this is something that we have to stress and make clear in Buddhism,
is that even if you are culpable,
even if you saw the rabbit there and decided to step on it,
maybe even decided to torture it,
draw out its death as long as possible.
Surprise!
There still is no benefit to feeling guilty about it.
Now, it's highly likely that if you start to practice meditation,
you're going to feel incredibly guilty about it.
But even that guilt is unwholesome.
It's an anger.
It's an improper reaction.
And in fact, even then,
the best course of action is simply to realize
that the states that led you to do that are a problem,
are a cause of suffering for yourself.
That in fact, it's not in your best interest to act cruelly.
It's in your best interest to give up cruelly.
That in fact, cruelly corrupts your mind,
pollutes your mind, and leads you to state of suffering in the future.
Not guilt.
Guilting fact.
So that is also quite useful,
I think, in dealing with the problem,
is realizing that even if you are morally culpable,
and usually we can find some way to blame ourselves,
even if we know it was an accident,
we're still blame ourselves for being negligent.
Even then, guilt is useless.
Feeling guilty about something is actually unwholesome.
It's a feeling of what we call guilt
can be broken up into two things.
One, the knowledge that it's wrong,
and to the disliking of it,
the pain, the suffering, the sadness that comes from it,
the negative feeling about it.
That negative feeling is the problem.
Knowing that it's wrong isn't a problem.
It's a bad extent, one aspect of guilt is okay,
but once you feel guilty in the sense of bad feeling bad about it,
or if you're so bad that I did eggs,
that's not useful.
It's not helpful.
It's cultivating an unwholesome habit of self-hatred,
which can eventually become debilitating,
and is always unpleasant and useless.
So, there you go.
