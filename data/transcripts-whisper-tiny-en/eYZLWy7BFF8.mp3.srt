1
00:00:00,000 --> 00:00:22,160
Hello everyone, broadcasting live video, January 1st, 2016, we've just recorded the first

2
00:00:22,160 --> 00:00:34,160
done up on the video, first video, my first recording of 2016. Yes, today's

3
00:00:34,160 --> 00:00:42,000
recording. Remember that one minute, for those of you who were here and during that,

4
00:00:42,000 --> 00:00:49,680
or have now it's actually on YouTube, which is kind of silly, but that one

5
00:00:49,680 --> 00:00:56,400
minute video has been watched, they watch two people watch them. So, it has

6
00:00:56,400 --> 00:01:05,520
officially been viewed by 10x. Maybe this is my first step to giving a 10-talk.

7
00:01:05,520 --> 00:01:22,000
What else? Tomorrow, tomorrow I'm going to Stony Creek. There's, I think it's the

8
00:01:22,000 --> 00:01:26,480
head monk's mother's birthday, it's kind of a New Year's celebration, it's a Sunday.

9
00:01:27,600 --> 00:01:35,040
And then Monday, I'm going to Toronto in the morning, or a meeting there, putting together a new

10
00:01:35,040 --> 00:01:44,320
certificate program in a manual college in Toronto, the University of Toronto, for non-degree

11
00:01:45,520 --> 00:01:52,400
students, practitioners, people who may not even have a degree, but are looking for a certificate

12
00:01:54,960 --> 00:02:03,600
in Buddhism, practical Buddhism. So, I think it would be in conjunction with

13
00:02:03,600 --> 00:02:10,400
Buddhist organizations, who would train people, and these people would

14
00:02:11,760 --> 00:02:18,480
would have to qualify, maybe do a write-up or something, and get graded, or I'm not sure how it

15
00:02:18,480 --> 00:02:29,920
would set up, fuzzy. Anyway, I've been invited to be on the

16
00:02:29,920 --> 00:02:37,120
East earring committee, organizing committee, not sure really why. I think it's because I've

17
00:02:37,120 --> 00:02:44,560
talked at a couple of these meetings, and I have the ability to do stuff with words.

18
00:02:45,760 --> 00:02:51,920
So, they like when I talk. Got a lot of experience organizing, and it's interesting. I mean,

19
00:02:51,920 --> 00:02:58,800
even before I was a monk, I was involved with student leadership since I was 14.

20
00:02:58,800 --> 00:03:05,200
I was doing student leadership conferences, and then I got on the Yves Parliament,

21
00:03:06,800 --> 00:03:11,520
and I've always done this kind of thing. And then organizing, once I became a monk,

22
00:03:11,520 --> 00:03:20,400
did an organization for meditation courses. Got a lot of experience, strangely enough.

23
00:03:20,400 --> 00:03:31,680
Anyway, so that's Monday. And then Tuesday, I think, is my first day of classes.

24
00:03:33,280 --> 00:03:43,440
That's my life. We got lots of people signing up for meetings. So, a couple new people anyway.

25
00:03:43,440 --> 00:03:52,720
Let's take a look. So, for those of you who, yeah, there's only four available slots.

26
00:03:53,600 --> 00:03:56,800
So, I'm not going to bother making a video or advertising it, because then,

27
00:03:56,800 --> 00:03:59,280
they're just going to get people complaining that they're not enough slots.

28
00:04:00,400 --> 00:04:03,120
So, let's just keep quiet. It's our secret.

29
00:04:03,120 --> 00:04:15,920
Four slots is fine. Four empty slots. I mean, some of these people are going to be done soon.

30
00:04:16,640 --> 00:04:24,480
And today's 630 appointment didn't show up, Doug, or as Doug finished. Maybe Doug's already finished.

31
00:04:25,360 --> 00:04:28,240
I can't remember. He's still on the list though.

32
00:04:28,240 --> 00:04:34,560
You guys are just taking them off. I can't remember. Some people call me and tell me they can't

33
00:04:34,560 --> 00:04:40,960
make it, and I don't. I just say, okay, like Sunday, someone can't make it, can't remember who.

34
00:04:48,000 --> 00:04:49,760
So, that's a neat system.

35
00:04:49,760 --> 00:05:00,560
Right, Shayhan came by today with 700 books.

36
00:05:03,120 --> 00:05:11,200
So, they're in my room now, and they're going to come in handy for the five-minute meditation.

37
00:05:11,200 --> 00:05:19,280
Okay, I saw that. I posted the poster. We got a banner that's like, big, and

38
00:05:20,800 --> 00:05:27,760
starting in January. Hopefully, we're going to do five-minute meditation lessons at the university.

39
00:05:34,720 --> 00:05:39,520
So, a bunch of announcements. Do we have any questions? I think we do, right?

40
00:05:39,520 --> 00:05:42,160
We do have questions, one thing.

41
00:05:42,160 --> 00:05:45,600
Oh, wait, give me a second. Let's just make sure we're recording properly.

42
00:05:45,600 --> 00:05:50,160
Yeah. Okay. I think we're recording properly. Go ahead.

43
00:05:50,160 --> 00:05:50,800
Okay.

44
00:05:52,080 --> 00:05:56,720
Hi, can we please explain the difference between noting a phenomena and being aware of it?

45
00:05:57,680 --> 00:06:00,560
Which one happens first is one meditates? Thank you.

46
00:06:00,560 --> 00:06:12,000
This is my mood, right? Yes. Okay. Well, I was saying he's asked a lot of questions and hasn't yet

47
00:06:14,080 --> 00:06:22,800
logged any time, which is fine, but I want to be sure that he's actually meditating

48
00:06:23,920 --> 00:06:26,880
and meditating in this tradition because he hasn't posted anything.

49
00:06:26,880 --> 00:06:34,400
So, Robin says she can vouch for him as practicing in our tradition.

50
00:06:35,040 --> 00:06:42,160
So, I'll answer the question, but I want to just make sure that he's asked this person's

51
00:06:42,160 --> 00:06:48,400
actually meditating. I'll check with him. But just for newer people, if anyone missed it,

52
00:06:48,400 --> 00:06:52,880
we actually do log in our time. It's just right at the bottom of the page.

53
00:06:52,880 --> 00:06:57,680
So, just plug in your time and hit start.

54
00:07:02,160 --> 00:07:08,240
When you meditate, try to log it here. I don't log on my meditation here. I'm trying to,

55
00:07:08,240 --> 00:07:14,240
but I get interrupted and stuff, so it's not always convenient.

56
00:07:14,240 --> 00:07:24,240
Anyway, I mean, there's a really big difference. I mean, it's not, if you think about it,

57
00:07:24,240 --> 00:07:29,760
it's not that hard to see the difference. Noting is an act, it's an artificial act,

58
00:07:29,760 --> 00:07:35,360
awareness is something that happens all the time. When you like something or you dislike something,

59
00:07:35,360 --> 00:07:44,400
you're aware of it first, the awareness comes there as well. The problem is when people try to use

60
00:07:44,400 --> 00:07:56,160
the word awareness to mean something more, like somehow one is intentionally aware.

61
00:07:57,680 --> 00:08:03,680
It's just too vague to really pin down as being this or that or the other thing. You're always

62
00:08:03,680 --> 00:08:12,240
aware. So when you say intentionally aware or just aware, then the question is, what exactly is the

63
00:08:12,240 --> 00:08:18,240
difference? So that would be confusing when you have meditation where people say just be aware.

64
00:08:18,240 --> 00:08:22,800
Don't react to it. Just be aware of it. Questions, well, how do you do that?

65
00:08:23,600 --> 00:08:28,080
And it's through noting this artificial practice that we claim that is accomplished.

66
00:08:28,080 --> 00:08:34,480
Noting comes after you're aware, after you're aware of something you remind yourself,

67
00:08:34,480 --> 00:08:42,320
which is the meaning of sati, that that is that. That is, you know, pain is pain, seeing, seeing,

68
00:08:42,320 --> 00:08:52,400
thinking, thinking, sitting is sitting. And that's it. And that act of reminding yourself again

69
00:08:52,400 --> 00:09:00,720
and again, every time you experience it, pain, pain, thinking, thinking is what creates or

70
00:09:00,720 --> 00:09:08,480
that creates is what prevents anything but awareness from arising. So all that arises is just

71
00:09:08,480 --> 00:09:17,520
bare awareness. This is the tool that leads you to be simply aware. So I mean, it's a good question,

72
00:09:17,520 --> 00:09:26,000
but I think it's really easy to see if you're objective about it.

73
00:09:30,320 --> 00:09:35,920
Hello. I have a couple of questions this evening. I am new to meditation. I find I'm easily

74
00:09:35,920 --> 00:09:40,800
distracted by noise around me when meditating. I have two dogs, for example, and they're

75
00:09:40,800 --> 00:09:46,240
barking distracts me. How should I deal with this when meditating? Should I simply acknowledge

76
00:09:46,240 --> 00:09:52,800
my distraction and continue on? I also wanted to thank you while I am not well-versed in meditation.

77
00:09:52,800 --> 00:09:57,600
Your videos have helped me through tough times in my life with regard to anxiety, and I appreciate

78
00:09:57,600 --> 00:10:06,080
what you do.

79
00:10:06,080 --> 00:10:18,480
A sound, for example, is a proper meditation object. So you can meditate on the sound, just saying

80
00:10:18,480 --> 00:10:30,080
yourself hearing. Like don't let it be a distraction, have it be your meditation, which I suppose

81
00:10:30,080 --> 00:10:35,120
seems kind of strange, but this whole meditation must seem kind of strange if you're not clear

82
00:10:35,120 --> 00:10:39,840
on what's going on in your mind. You would have no reason why would I want to focus on the sound,

83
00:10:39,840 --> 00:10:44,960
what's going to happen? Is it something magical or something? What's special about the sound of my

84
00:10:44,960 --> 00:10:50,560
dog's bark? You can become enlightened listening to your dog's bark. That's how special it is.

85
00:10:51,440 --> 00:10:56,480
But that seems weird, and it seems strange, and how could that possibly be? It comes because

86
00:10:57,680 --> 00:11:04,240
the sound is an experience that arises and ceases, and your mind gets involved with it and reacts

87
00:11:04,240 --> 00:11:09,440
to it. Just by listening to the sound, you can see all of the aspects of your mind. See how your

88
00:11:09,440 --> 00:11:16,960
mind works, you can adjust your mind, begin to see how the truth of reality is just experiences.

89
00:11:16,960 --> 00:11:25,120
So the whole idea of dog's barking is 99% conceptual. The only piece of it that's real is the sound

90
00:11:25,120 --> 00:11:31,680
of the year. Everything else is real in terms of being a thought in the mind, but the only reality

91
00:11:31,680 --> 00:11:36,720
of it is it's being a thought or reactions and motions and so on.

92
00:11:41,200 --> 00:11:46,400
It's absolutely just meditate on the sound, hearing, hearing. If your mind eventually gets

93
00:11:47,200 --> 00:11:51,280
so it's not interested in the sound anymore, then you can ignore it and come back again to

94
00:11:52,640 --> 00:11:56,720
the stomach rising and falling. But try to note the sound for

95
00:11:56,720 --> 00:12:02,240
until it goes away or until your mind starts to become disinterested in it.

96
00:12:06,720 --> 00:12:10,320
If you kind of can't help reacting, would you switch your

97
00:12:11,040 --> 00:12:20,000
objective meditation to your reaction? Yeah, I mean you don't want to jump around too much.

98
00:12:20,000 --> 00:12:27,040
Eventually come back to the stomach, but not at least that reactions as well.

99
00:12:27,840 --> 00:12:31,440
I should have read this part with it as well. Would it be more beneficial to try and find a

100
00:12:31,440 --> 00:12:37,440
place more quiet without distraction to meditate? No, I'm going to do that.

101
00:12:41,120 --> 00:12:46,320
There happens to be a Leo Shinwat 10 miles from my home, and I'd like to spend time there.

102
00:12:46,320 --> 00:12:50,960
Would you please give guidance with basic etiquette and observances? I've only been to one

103
00:12:50,960 --> 00:12:54,160
we horror in the past. Thank you for your consideration, Bunthay.

104
00:12:55,520 --> 00:13:00,400
I'm not loud, not loud, I shouldn't have to ask them.

105
00:13:03,120 --> 00:13:07,520
I mean, it's tough because I'm so much more lax than all these places, so

106
00:13:08,400 --> 00:13:13,040
I'm not much good to you, I'm afraid. I mean, I know some of them. I know how to behave, I suppose.

107
00:13:13,040 --> 00:13:19,920
I'm just shirking my responsibility, but it's hard for me to really talk about all these

108
00:13:20,880 --> 00:13:23,920
ridiculous silly rules that places have.

109
00:13:26,320 --> 00:13:30,320
Well, I go to a Leo Shinwat Bunthay, so you can tell them how do you do it.

110
00:13:30,320 --> 00:13:32,640
When I pass this one off on me, I'd be happy to.

111
00:13:34,080 --> 00:13:40,320
The what did I go to? They actually have a sign poster on the wall with basic etiquette,

112
00:13:40,320 --> 00:13:48,720
and I love how simple they make it. Call the Monksa John, sit on your legs,

113
00:13:48,720 --> 00:13:55,200
so in other words, you don't sit cross-legged, you actually sit on your legs during the time

114
00:13:55,200 --> 00:14:01,280
that you're there. Take off your shoes, that's the first one. Those are the ones that are listed.

115
00:14:01,280 --> 00:14:06,320
I found out the hard way by making some mistakes, but everyone was always very, very nice and

116
00:14:06,320 --> 00:14:12,960
helped me out. Just a couple of other things, and I imagine this is fairly common,

117
00:14:13,920 --> 00:14:19,120
but don't walk in front of the monks, show respect by moving to the side and letting the monks move

118
00:14:19,120 --> 00:14:27,280
forward. And then with food, the monks would eat first, and then the lay people eat after the

119
00:14:27,280 --> 00:14:34,320
monks are completely done. So it's a little bit different, but I found people very friendly and very

120
00:14:34,320 --> 00:14:43,680
willing to help me out. The reason I don't want to relate all that is because it makes it sound like

121
00:14:44,400 --> 00:14:52,640
some of that. I mean, people always take this to be Buddhist rules. And there's a lot of thought

122
00:14:52,640 --> 00:15:01,760
that there's an obligation or an expectation, and there is no expectation. And I try my best,

123
00:15:01,760 --> 00:15:08,720
my view, and my impression I want to give people is that there are no expectations on you.

124
00:15:09,520 --> 00:15:16,720
Monks have a lot of expectations on them, and I think the only expectation is a respectful attitude.

125
00:15:17,760 --> 00:15:28,000
I find it surprising sometimes how disrespectful. And it's not like you have to hold monks

126
00:15:28,000 --> 00:15:32,720
as above you, but you should be respectful of the fact that they are ordained. They're not

127
00:15:33,360 --> 00:15:39,200
in society, so you shouldn't treat them as your pal, and you don't want to shake their hands,

128
00:15:39,200 --> 00:15:46,880
or hug them, or joke with them, or so on. Doesn't that mean putting them above you? I mean,

129
00:15:46,880 --> 00:15:52,400
many people do, and that's fine if you think of them as spiritual leaders or whatever, but

130
00:15:52,400 --> 00:16:02,320
as separate, as like you would have priest. You know, in modern Western culture there's a real

131
00:16:02,320 --> 00:16:10,160
sense of equality and treating people just as people and disregarding all of this

132
00:16:10,160 --> 00:16:20,240
pomp and ceremony and so on. I sympathize with that, who the most part, but I think there is,

133
00:16:24,160 --> 00:16:28,480
I mean, we should respect, I just think we should respect, we respectful for people in general,

134
00:16:28,480 --> 00:16:34,720
and be respectful of their situation. Monks have a specific situation, just like priests and rabbis,

135
00:16:34,720 --> 00:16:43,600
and I take that all very seriously. When I meet a rabbi or a priest, I respect their

136
00:16:44,800 --> 00:16:52,160
path, and I try not to infringe upon it. So for monks, I think that's the only one really

137
00:16:52,160 --> 00:16:56,160
you should keep in mind. I mean, obviously there's many more in the monastery, but

138
00:16:57,280 --> 00:17:02,800
general rule of thumb is it's a monk, and they're doing something fairly specific, so

139
00:17:02,800 --> 00:17:10,480
they're very least, leave them alone. Don't get too close or chummy or think of them as

140
00:17:12,160 --> 00:17:13,360
your pal or something.

141
00:17:18,080 --> 00:17:23,200
Even with lay people, I've done a little reading and you know about

142
00:17:23,200 --> 00:17:32,720
lay ocean people, and you know, and also just things people have indicated to me. Not really, it's not

143
00:17:32,720 --> 00:17:36,800
really a situation where you go up and hug someone or anything, it's a little bit of distance,

144
00:17:36,800 --> 00:17:42,720
and definitely when people are doing like an overnight meditation or on eight precepts,

145
00:17:42,720 --> 00:17:48,560
you wouldn't touch someone of the opposite sex at all, you wouldn't shake hands or anything like that.

146
00:17:48,560 --> 00:17:54,960
Yeah, that's a good point. When you're on eight precepts, and many people are,

147
00:17:55,760 --> 00:17:59,280
generally know contact, at least one between genders.

148
00:18:01,280 --> 00:18:05,680
There's also a scarf, like an upper body scarf, the people wear, it's called papiang,

149
00:18:06,480 --> 00:18:15,200
and it's worn diagonally across the chest, the papiang, yeah, the right shoulder is kept there,

150
00:18:15,200 --> 00:18:19,120
and you wear it across the chest, and everyone wears it, men, women, and children.

151
00:18:19,760 --> 00:18:27,840
And no one seems to know why, it's just habit or tradition, but everyone wears them.

152
00:18:27,840 --> 00:18:33,840
And I didn't think that I thought it might seem odd if I wore one, but right away people were

153
00:18:33,840 --> 00:18:39,520
saying, oh, you know, you should wear one, this is where you can get one, I'll bring you an extra

154
00:18:39,520 --> 00:18:47,360
one, so I wear one as well, it seems to be some sort of a respectful gesture, but no one knows exactly

155
00:18:47,360 --> 00:18:47,840
why.

156
00:18:47,840 --> 00:19:05,760
But I have difficulty to know if I control my breath or not, it's not clear, what should I note? Thanks.

157
00:19:18,000 --> 00:19:23,520
I'm not quite clear on what you're asking.

158
00:19:36,400 --> 00:19:37,840
I'm not quite clear on what you're asking.

159
00:19:39,200 --> 00:19:41,440
You should be noting the rising and the following.

160
00:19:41,440 --> 00:19:49,680
If you find yourself trying to control the breath, you should not knowing, knowing, or if you're

161
00:19:49,680 --> 00:19:57,360
frustrated about it, say frustrated or this unpleasant, which control often is, you can say unpleasant

162
00:19:57,360 --> 00:20:20,080
or we've got a good word in tight, but we don't have such a word in English. Feeling is fine.

163
00:20:28,320 --> 00:20:34,000
And we're all caught up on questions.

164
00:20:39,840 --> 00:20:44,400
And let's recall I wanted to put a little more detail on the question.

165
00:20:46,400 --> 00:20:52,400
This person had a good answer. If you're not sure whether you're controlling the breath and

166
00:20:52,400 --> 00:21:06,160
not the doubt, that's a good answer. If you're self-doubting, don't you?

167
00:21:22,560 --> 00:21:28,720
Okay, that's all then. Have a good night. Thank you, Robin, for your help.

168
00:21:28,720 --> 00:21:58,560
Thank you, Bante. Good night. Good night.

