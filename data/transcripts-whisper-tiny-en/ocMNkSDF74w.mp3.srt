1
00:00:00,000 --> 00:00:06,320
Meditation has helped me tremendously in my life journey. What would you say is the

2
00:00:06,320 --> 00:00:11,120
best way to work through personal family issues so that they do not lean to

3
00:00:11,120 --> 00:00:17,240
excessive thoughts of stress or worry based on a sense of instability and change?

4
00:00:17,240 --> 00:00:30,640
How do I solve my life? How do I fix everything? That's all you're asking.

5
00:00:30,640 --> 00:00:49,680
Why are we born to the people we're born to? Do we have the parents? We have the

6
00:00:49,680 --> 00:00:54,480
siblings. We have children. We have friends. There's a lot involved here.

7
00:00:54,480 --> 00:01:03,440
There's a backstory. The idea that there would be some answer that I could

8
00:01:03,440 --> 00:01:10,080
give over YouTube that would fix all of that is incredibly complex,

9
00:01:10,080 --> 00:01:18,480
spanning countless lifetimes. The backstory behind your family problems would

10
00:01:18,480 --> 00:01:22,560
seem like just family problems. The backstory behind them. Make that question a

11
00:01:22,560 --> 00:01:32,280
little bit difficult to answer. But I think that is an answer is that you have to

12
00:01:32,280 --> 00:01:37,400
realize that this isn't something that can be easily answered, easily fixed.

13
00:01:37,400 --> 00:01:43,760
This is a very important part of your practice. Your family, your friends,

14
00:01:43,760 --> 00:01:49,280
the people around you, the relationships you have. That's a very important factor

15
00:01:49,280 --> 00:02:01,320
in your practice. Usually, often, in many cases, a negative factor, an inhibiting

16
00:02:01,320 --> 00:02:05,400
factor, something that gets in the way of your meditation. The reason we're

17
00:02:05,400 --> 00:02:11,800
born to our families is not usually because we're all meditators. But for most

18
00:02:11,800 --> 00:02:16,960
of us, it's because of attachments, because we're attached to these people,

19
00:02:16,960 --> 00:02:22,400
either in positive or negative ways. So there's positive in the sense of

20
00:02:22,400 --> 00:02:26,120
attachment. It's not positive in a good way. It's positive in a sense of

21
00:02:26,120 --> 00:02:32,640
wanting, desiring. It could be negative in the sense of aversion, disliking,

22
00:02:32,640 --> 00:02:40,560
and we have our vengeance and revenge and so on and all this.

23
00:02:40,560 --> 00:02:47,960
So you're trying to fix that. It's not really the place to start. The place to

24
00:02:47,960 --> 00:02:58,680
start is your reactions to these things. So take the personal family issues, the

25
00:02:58,680 --> 00:03:02,480
stress that comes from them as your practice. Instead of what many people do is

26
00:03:02,480 --> 00:03:07,880
try to escape, so people will run off into the jungle, thinking that their

27
00:03:07,880 --> 00:03:13,240
family is getting in their way and the simplest answer is just to escape them.

28
00:03:13,240 --> 00:03:23,880
It doesn't always work. It doesn't often work. If you have strong, bad karma or

29
00:03:23,880 --> 00:03:29,960
bad feelings with your parents, especially, going off into the forest, rarely

30
00:03:29,960 --> 00:03:38,640
solves that. So I would say be careful about how you try to approach these

31
00:03:38,640 --> 00:03:45,080
things and about how you try to fix or try to find a quick fix for your

32
00:03:45,080 --> 00:03:49,480
family problems. They should be approached with incredible delicacy and

33
00:03:49,480 --> 00:03:56,280
shouldn't be seen as having a fix and immediate fix. One of the best things is

34
00:03:56,280 --> 00:04:04,200
to, I suppose, have a little faith. You know and you see the benefit of the

35
00:04:04,200 --> 00:04:12,480
meditation, right? You say it's happened tremendously. So make that leap. It's not

36
00:04:12,480 --> 00:04:16,280
really a faith, but it's kind of like a leap of faith. But your mind is

37
00:04:16,280 --> 00:04:21,080
missing something. You have to make the connection where you say this is

38
00:04:21,080 --> 00:04:27,160
also helping my family. This is also improving my family situation, even

39
00:04:27,160 --> 00:04:31,280
though it often doesn't look like it. Have some sort of faith, not exactly

40
00:04:31,280 --> 00:04:38,480
faith, but really logic, I would say, that tells you that, well, if it's affecting

41
00:04:38,480 --> 00:04:43,640
me, it must be affecting all the people around me. We're not islands. So they

42
00:04:43,640 --> 00:04:48,280
must be, if I'm being positively influenced, they must be positively influenced

43
00:04:48,280 --> 00:04:54,000
by this as well. And be patient with that. Be dedicated to that, to the

44
00:04:54,000 --> 00:04:57,640
practice. Again, right? Difference between practice and realizations. Be

45
00:04:57,640 --> 00:05:01,520
dedicated to the practice, the cultivation of goodness. Not so worried

46
00:05:01,520 --> 00:05:11,000
about, are we there yet? Is everything fixed? So not expecting for yourself to

47
00:05:11,000 --> 00:05:18,000
have a stress-free family life. But as your stress decreases, be happy with

48
00:05:18,000 --> 00:05:22,200
that. Be confident that you're on the right path. That's what all I have to

49
00:05:22,200 --> 00:05:27,880
say. And just that, such a general question can't be really answered. You have

50
00:05:27,880 --> 00:05:32,080
to give me more specifics. You have specific problems. Often we can try to

51
00:05:32,080 --> 00:05:37,280
muddle through them and offer strategies. But how do I fix my life?

52
00:05:37,280 --> 00:05:42,840
I don't have meditate. You can generally answer general question, generally

53
00:05:42,840 --> 00:05:55,540
answer. Medi-

