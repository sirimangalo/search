1
00:00:00,000 --> 00:00:02,100
Welcome back to Ask a Monk.

2
00:00:02,100 --> 00:00:07,000
Next question comes from the long on deck who asks,

3
00:00:07,000 --> 00:00:11,000
Do you ever miss the former life you had in the States?

4
00:00:11,000 --> 00:00:18,000
Sometimes do you ever have the urges or desires to partake in things most Americans consider normal?

5
00:00:18,000 --> 00:00:24,000
Like say going out to eat at a restaurant or maybe just going to a movie?

6
00:00:24,000 --> 00:00:29,000
First of all the funny thing is I'm not American.

7
00:00:29,000 --> 00:00:40,000
And the only life of any length that I know in America is my life as a monk which I don't particularly miss.

8
00:00:40,000 --> 00:00:48,000
That's not I think your question but you know as a monk in America I was arrested and put in jail and accosted by the police and so on.

9
00:00:48,000 --> 00:00:58,000
So it was a difficult life but I think you're asking more about my life in Canada because I'm from Canada.

10
00:00:58,000 --> 00:01:05,000
So in general I don't like to answer questions about my personal life or my personal path and so on.

11
00:01:05,000 --> 00:01:09,000
I don't think that's really the point.

12
00:01:09,000 --> 00:01:17,000
But the point of this question I guess is in regards to what to do about these urges and curiosity of what a monk

13
00:01:17,000 --> 00:01:23,000
does about these things and how a monk overcomes these are a meditator.

14
00:01:23,000 --> 00:01:25,000
And I think that's key.

15
00:01:25,000 --> 00:01:30,000
The only reason that I'm doing what I'm doing now is because I started practicing meditation.

16
00:01:30,000 --> 00:01:37,000
And I think without meditation you could never overcome these desires, these urges.

17
00:01:37,000 --> 00:01:47,000
One thing to do useless things, one thing to do things that only bring ephemeral pleasures.

18
00:01:47,000 --> 00:01:56,000
But in the end leave you only wanting more and with a muddled and confused state of mind.

19
00:01:56,000 --> 00:02:15,000
The urges come up but I think clearly through the practice of meditation you are able to separate the parts of the attachment or the desire.

20
00:02:15,000 --> 00:02:25,000
A lot for me a lot of the old things that I used to do.

21
00:02:25,000 --> 00:02:32,000
I guess sort of what was normal but more the things that I was passionate about let's put it that way.

22
00:02:32,000 --> 00:02:47,000
From myself I was a rock punk guitarist, rock guitarist in a band, I was into rock climbing and some sports and so on.

23
00:02:47,000 --> 00:02:53,000
And it was really difficult to give those up but on the one hand but also incredibly easy.

24
00:02:53,000 --> 00:03:00,000
It was interesting because on the one hand there's this desire that comes up to do these things.

25
00:03:00,000 --> 00:03:05,000
And on the other hand there's a knowledge that's a conditioned phenomenon.

26
00:03:05,000 --> 00:03:08,000
That's something that I've developed over the years.

27
00:03:08,000 --> 00:03:19,000
It's the same as anger, it's the same as many of our negative or unwholesome emotions.

28
00:03:19,000 --> 00:03:25,000
It's something that doesn't lead to, just because you have it doesn't mean you have to follow it.

29
00:03:25,000 --> 00:03:27,000
It doesn't mean there's any benefit in following it.

30
00:03:27,000 --> 00:03:43,000
This is what we really miss. We think that when anger arises it means someone's done something bad to you and therefore you have to get back at them or attack or do whatever it takes to get rid of the unpleasant situation.

31
00:03:43,000 --> 00:03:45,000
And the same goes for our desires.

32
00:03:45,000 --> 00:03:57,000
We have been trained in condition to believe that our desires are a perfectly valid reason for us to chase after things.

33
00:03:57,000 --> 00:04:01,000
If you want something that's a reason for you to go and get it.

34
00:04:01,000 --> 00:04:08,000
Through the meditation practice this changes because you look at it.

35
00:04:08,000 --> 00:04:17,000
It's really quite obvious actually that the desire is leading you to attachment and addiction.

36
00:04:17,000 --> 00:04:20,000
Why would you want to cultivate that?

37
00:04:20,000 --> 00:04:29,000
Clearly it's a conditioned state that is wrong that is unprofitable.

38
00:04:29,000 --> 00:04:37,000
And so instead of following it you don't repress it but you look at it and you stay with it.

39
00:04:37,000 --> 00:04:41,000
So you want something and you want something.

40
00:04:41,000 --> 00:04:43,000
And this is something that is very difficult.

41
00:04:43,000 --> 00:04:54,000
It's the key to the Buddha's teaching is to be able to see the one thing, to understand it, to see the phenomenon for what it is.

42
00:04:54,000 --> 00:05:01,000
It's something that has arisen and it's something that will see rather than extrapolating upon it and saying,

43
00:05:01,000 --> 00:05:07,000
therefore I have to do this, I have to that, I have to chase after it.

44
00:05:07,000 --> 00:05:10,000
It's something that I think most people don't want to do but that's based on their conditioning.

45
00:05:10,000 --> 00:05:21,000
I think if we were honest with ourselves and that's the thing, can you be truly and completely honest with yourself?

46
00:05:21,000 --> 00:05:33,000
Which most of us can't, we're perfectly happy to delude ourselves that we're somehow meeting with happiness.

47
00:05:33,000 --> 00:05:46,000
And so, and this is because we don't have the ability to, the clarity of mind, the focus of mind, the ability to see things as they are based on our lack of meditation practice.

48
00:05:46,000 --> 00:06:08,000
This is why in the beginning it's very important to do an intensive meditation course as opposed to doing a little bit at a time, because the intensity of it, not the intensity but the intensive nature that it's completely meditative allows you to pull yourself up out of the

49
00:06:08,000 --> 00:06:18,000
mire of the conditioning and allow you to come up and to dry land so to speak and be able to look down.

50
00:06:18,000 --> 00:06:26,000
So that when the conditions come up you're able to be more objective because for most of us it's just impossible.

51
00:06:26,000 --> 00:06:28,000
The desire comes up and we're gone already.

52
00:06:28,000 --> 00:06:35,000
We have no skills in regards to seeing things clearly as they are.

53
00:06:35,000 --> 00:06:40,000
That's what the meditation practice is for and that's why it's so important.

54
00:06:40,000 --> 00:06:43,000
So I hope that helps.

55
00:06:43,000 --> 00:06:52,000
I think that's, I've said talked about it before, the key is breaking it up into its components and being able to separate them.

56
00:06:52,000 --> 00:06:59,000
That this one is the problem, this one is not a problem when you want something.

57
00:06:59,000 --> 00:07:09,000
It's a problem in the sense that it's suffering but until you act upon it it's not going to cause addiction.

58
00:07:09,000 --> 00:07:20,000
If you watch the wanting, the pleasure, then the stress that is involved and so on, then it's not really a problem.

59
00:07:20,000 --> 00:07:22,000
You come to see that it's a problem.

60
00:07:22,000 --> 00:07:28,000
You will come to see oh this is not good and you'll let go of it and you won't give rise to it anymore.

61
00:07:28,000 --> 00:07:31,000
That's really the key.

62
00:07:31,000 --> 00:07:41,000
And this realization, I guess before the wanting goes away is the realization that it's only wanting.

63
00:07:41,000 --> 00:07:44,000
This is the first goal in the meditation practice.

64
00:07:44,000 --> 00:07:56,000
It's not the final goal but the first goal is the realization that the states that we have inside are not a reason to act.

65
00:07:56,000 --> 00:07:59,000
It's not good for us to act upon them.

66
00:07:59,000 --> 00:08:05,000
The realization that just because I want something doesn't mean I should go and get it.

67
00:08:05,000 --> 00:08:13,000
The wanting of things is not a good reason to chase after them.

68
00:08:13,000 --> 00:08:23,000
And which leads to this ability to examine the wanting and to come to teach yourself.

69
00:08:23,000 --> 00:08:28,000
The true nature of the desires and therefore to let them go peacefully.

70
00:08:28,000 --> 00:08:29,000
It's not about repression.

71
00:08:29,000 --> 00:08:34,000
It's not like I still want to go rock climbing but I'm just for pressing it.

72
00:08:34,000 --> 00:08:42,000
It's coming to realize that desire for anything is really quite useless.

73
00:08:42,000 --> 00:08:47,000
It doesn't lead to peace and happiness and freedom from suffering.

74
00:08:47,000 --> 00:08:58,000
It's difficult and probably undesirable for most people but that's why most people go around and around and around.

75
00:08:58,000 --> 00:09:01,000
That's the point really.

76
00:09:01,000 --> 00:09:25,000
So thanks for the question all the best.

