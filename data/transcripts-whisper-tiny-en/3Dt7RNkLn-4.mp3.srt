1
00:00:00,000 --> 00:00:08,800
Hey, welcome back to Askamunk. Today's question comes from Fisk Dam's consultant.

2
00:00:08,800 --> 00:00:13,880
What is really the mind and how did the Buddha use the last four parts of the five aggregates

3
00:00:13,880 --> 00:00:19,920
and Nama in Nama Rupa to explain the mind? Did the Buddha use any other ways of explaining

4
00:00:19,920 --> 00:00:26,080
the mind? Understanding what the mind is is hard, please help.

5
00:00:26,080 --> 00:00:32,960
What is really the mind? The definition of mind, and here this is the meaning of Nama,

6
00:00:32,960 --> 00:00:39,640
or Vinyana, or Dita, which in the end are all just different ways of saying basically

7
00:00:39,640 --> 00:00:47,000
the same thing. It's that which knows. In reality, we understand there are two parts.

8
00:00:47,000 --> 00:00:53,600
One part is, one part of reality isn't aware of anything. It's the unconscious part

9
00:00:53,600 --> 00:00:59,680
of reality. That part we call Rupa. In English, we translate that into form or material

10
00:00:59,680 --> 00:01:05,440
or the physical. The other part knows something. It's aware of things generally aware of the

11
00:01:05,440 --> 00:01:15,960
physical, but also aware of other mental states. This knowing, we call Nama. This is all

12
00:01:15,960 --> 00:01:23,240
that really exists in the mind. The other aspects of it are the quality of knowing, because

13
00:01:23,240 --> 00:01:28,960
not every knowing is the same. Sometimes we, this knowing, this awareness, this observation

14
00:01:28,960 --> 00:01:35,000
of things is based on the judgment, anger, greed. Sometimes it's impartial. Sometimes

15
00:01:35,000 --> 00:01:40,200
it's happy. Sometimes it's unhappy. Not every knowledge is the same. These are the various

16
00:01:40,200 --> 00:01:46,480
qualities of the mind, but essentially the meaning of mind is that which knows. In this

17
00:01:46,480 --> 00:01:54,080
sense, it's really not very difficult to understand at all. It's very much a part of meditation

18
00:01:54,080 --> 00:02:02,480
practice. If you practice sincere meditation, it's quite easy to see that which is the

19
00:02:02,480 --> 00:02:08,400
mind. For instance, when you're watching this stomach, if you watch it rising and falling,

20
00:02:08,400 --> 00:02:12,080
there is the reality of the stomach rising and falling, but there's also the knowing of it.

21
00:02:12,080 --> 00:02:17,360
These are two distinct things. We know this because you're not always aware of the rising

22
00:02:17,360 --> 00:02:23,040
and falling. If the mind doesn't go out to the object, then there's no awareness. Sometimes

23
00:02:23,040 --> 00:02:27,040
the mind is elsewhere. The rising and falling is still occurring, but the mind is not

24
00:02:27,040 --> 00:02:34,720
aware of it. So until the mind and the body come in contact, there's no awareness. This

25
00:02:34,720 --> 00:02:41,200
is what is meant by the mind. The body is one thing. The mind is another. When they come

26
00:02:41,200 --> 00:02:49,200
together, that's called experience. As far as the five aggregates, this is one very important

27
00:02:49,200 --> 00:02:57,760
way of understanding what is the body and mind, and essentially what is reality. The way

28
00:02:57,760 --> 00:03:04,080
the Buddha explained the five aggregates, it's they are something that arises at what are called

29
00:03:04,080 --> 00:03:09,520
the six senses. So we always talk about the five sense, but actually there are six, the five,

30
00:03:09,520 --> 00:03:15,600
that we normally understand, and the sixth one is the mind, the purely mental, the thinking,

31
00:03:15,600 --> 00:03:21,920
our mental landscape. So when you see something, there are five things that arise. There's the

32
00:03:21,920 --> 00:03:28,960
physical, which is the eye and the light that touches it, that's a physical reality. Then there is

33
00:03:28,960 --> 00:03:34,800
number two, the feeling, which is in the beginning neutral, but can evolve into a pleasant or

34
00:03:34,800 --> 00:03:42,320
an unpleasant feeling based on what we see, whether it's beautiful or ugly, or how we react to

35
00:03:42,320 --> 00:03:48,880
it in the mind. Number three is our recognition of it, our perception of it as yellow, white,

36
00:03:48,880 --> 00:03:58,160
blue, as a car, a bird, tree. Remember recognizing it as this person or that person. Number four

37
00:03:58,160 --> 00:04:02,480
is what we think of it, our judgments liking it, disliking, getting angry, or getting

38
00:04:02,480 --> 00:04:10,880
at becoming attached to it, and so on. Number five is the basic awareness, which governs all of

39
00:04:10,880 --> 00:04:17,840
the other, which governs the whole of the experience. These five things are called the five aggregates.

40
00:04:17,840 --> 00:04:21,520
That's how the Buddha explained it, and that's what happens when you meditate. When you see something,

41
00:04:21,520 --> 00:04:25,680
and you say to yourself, seeing, seeing, you're aware of all of this, and sometimes one is clear,

42
00:04:25,680 --> 00:04:31,920
sometimes the feeling is clear, sometimes the memory, the recognition that this is something I've seen

43
00:04:31,920 --> 00:04:37,600
before, sometimes it's the, what you think of it, the judgment of it. Sometimes it's just the clear

44
00:04:37,600 --> 00:04:43,680
awareness, the fact that you're aware of things. You know that you're seeing this knowledge of it.

45
00:04:44,480 --> 00:04:48,800
When you hear it's the same, when you smell, when you taste, when you feel, and when you think,

46
00:04:48,800 --> 00:04:54,000
when you think there, there's the physical, but it's not really in play. The physical is required

47
00:04:54,000 --> 00:05:02,480
as a base for the human experience, but then there are the, basically the four experiences based

48
00:05:02,480 --> 00:05:11,520
on thought, based on a thought that has arisen, which is, can arise because of the physical or

49
00:05:11,520 --> 00:05:19,600
because of the mental. So that's, did the Buddha use other ways of explaining the mind? Yeah,

50
00:05:19,600 --> 00:05:26,800
this is the most standard experience of explanation of how to understand the mind, but there's,

51
00:05:27,520 --> 00:05:33,280
the Buddha used a lot of ways to explain both body and mind, and the most comprehensive,

52
00:05:33,280 --> 00:05:37,040
if you're really interested in that sort of thing, is in what is called the Abhidama,

53
00:05:37,040 --> 00:05:45,520
which is the most detailed exposition on the body and the mind,

54
00:05:45,520 --> 00:05:53,920
which includes 121 different types of mind state, and it's basically a permutation of the various

55
00:05:53,920 --> 00:06:00,320
types of mind states that can come together. For instance, there are eight types of greed of

56
00:06:00,320 --> 00:06:07,280
consciousness based on greed or craving, and you get the number eight because there are three factors.

57
00:06:07,280 --> 00:06:13,360
There are greed states that are associated with pleasure and those that are associated with

58
00:06:13,360 --> 00:06:22,080
equanimity. There are greed states that are, that are accompanied by wrong view by the,

59
00:06:22,080 --> 00:06:28,240
the understanding that is good to be greedy, and there are greed states that are not associated

60
00:06:28,240 --> 00:06:32,080
with views. So the understanding that it's wrong, but still wanting some, the understanding that

61
00:06:32,080 --> 00:06:39,040
it's not useful for you, but still wanting something. And the third one, whether it is,

62
00:06:39,040 --> 00:06:45,760
it is prompted or unprompted, whether it's, it comes from itself or whether there's an external

63
00:06:45,760 --> 00:06:52,000
stimulus like someone urging you to steal or so on or, or, or, you know, some kind of other factor

64
00:06:52,000 --> 00:06:56,960
that it doesn't spontaneously arise. So with these three factors, you have two times, two times,

65
00:06:56,960 --> 00:07:02,240
two, and you get eight different kinds of, of greed minds. There are two kinds of anger,

66
00:07:02,240 --> 00:07:07,920
mind, two kinds of delusion, minds that are simply based on delusion, and those are the 12

67
00:07:07,920 --> 00:07:14,320
unwholesome, it's, and then it goes on and on and on. There are, there are eight great wholesums,

68
00:07:14,320 --> 00:07:22,080
or eight central spiritual wholesums and eight, I can't even remember it's been a long time

69
00:07:22,080 --> 00:07:26,400
at it. I, I've got the book and you can go through it and there's 121 of them and some of them

70
00:07:26,400 --> 00:07:36,560
are super mundane, some of them are based on meditative states. And, and then there are 52, I'm probably

71
00:07:36,560 --> 00:07:40,800
getting it wrong. There's a whole bunch of, of mental states that are involved here, like the ones

72
00:07:40,800 --> 00:07:45,680
that are involved in all of these minds, the mental qualities that are only in some of the minds,

73
00:07:45,680 --> 00:07:52,000
only in the wholesome minds, only in the unwholesome minds, and so on and so on. And so, it's a very

74
00:07:52,000 --> 00:08:02,160
complex breakdown of, of the, the mental landscape. All of that's really theoretical, but it, it does

75
00:08:02,160 --> 00:08:09,520
give a good understanding of, of the various details of the things that you're going to run into

76
00:08:09,520 --> 00:08:14,720
in, in your meditation practice and in your life, understanding what a mind state is. And it also

77
00:08:14,720 --> 00:08:20,240
helps you to break down some of the, as I said, entities, where we think this is a problem,

78
00:08:20,240 --> 00:08:24,080
something's going on, to be able to break it down and see, oh, it's actually just states of

79
00:08:24,080 --> 00:08:32,320
greed and states of anger and things that are arising in my mind. It's my extrapolation of thing,

80
00:08:32,320 --> 00:08:39,520
of something very simple, but certainly too much for a short video. I'm not going into detail

81
00:08:39,520 --> 00:08:46,080
about that, but just so you understand there are many different aspects or ways that the booty

82
00:08:46,080 --> 00:08:52,800
use of explaining the mind. And as you say, in the end, understanding the mind is hard. There's

83
00:08:52,800 --> 00:08:58,400
no getting around that. You can theorize, you can read texts and so on. But until you sit down

84
00:08:58,400 --> 00:09:04,080
in practice, meditation, you'll never really understand the mind. And once you sit down in practice

85
00:09:04,080 --> 00:09:10,800
meditation, it may still not be clear how that relates to, to the five aggregates or how the

86
00:09:10,800 --> 00:09:15,200
Buddha talked about mind or consciousness or so on. But it's not really important because

87
00:09:16,000 --> 00:09:20,720
all you, you know, you, you simply need someone like me to explain to you what is happening here

88
00:09:20,720 --> 00:09:24,960
and you're in here as I've just done, hopefully helps with that. But the point is that you're

89
00:09:24,960 --> 00:09:28,720
under, you're seeing reality. It's not important that you understand the Buddhist concept of mind.

90
00:09:28,720 --> 00:09:33,280
It's important you understand the truth of mind or the truth of the reality. It's not so important

91
00:09:33,280 --> 00:09:38,160
that you can say that's mind, that's body, that's this, that's this. It's much more important

92
00:09:38,160 --> 00:09:43,120
that you see it for what it is. It is what it is at that moment. You see it clearly and you're not

93
00:09:43,120 --> 00:09:49,680
judging it. You don't have to be clearly aware of what part of the Buddha is teaching that that fits

94
00:09:49,680 --> 00:09:55,600
into in terms of which one of the 121 minds it is for example. When you can see that certain

95
00:09:56,720 --> 00:10:02,480
minds states are unwholesome, then you give those up and you see that certain minds states are wholesome

96
00:10:02,480 --> 00:10:07,680
then you follow those. And this comes naturally through the practice and that's most important.

97
00:10:08,960 --> 00:10:15,760
But certainly it's not easy and as far as the fact that the mind is difficult to understand,

98
00:10:15,760 --> 00:10:22,080
there's a quote of the Buddha that's often given and in the Buddha's language. It goes

99
00:10:22,080 --> 00:10:34,240
dulangamang, ikatarang, asarirang, gohaseyang, yeetitang, zanyamisang, mukhandi, marabandana, which means basically.

100
00:10:35,360 --> 00:10:44,880
Dulangamang, it travels far, ikatarang, it travels alone. Asarirang, it has no form, gohaseyang,

101
00:10:44,880 --> 00:10:50,640
it dwells in the cave. And the point of this first part is to give you the idea of some terrible

102
00:10:50,640 --> 00:10:56,640
beast or demon or ghost, something that is very mysterious because it travels far and wide alone

103
00:10:57,600 --> 00:11:03,280
without form in living in a cave. And then the next part is yeetitang, zanyamisang,

104
00:11:03,280 --> 00:11:14,640
demokhandi, marabandana, which means a person who can calm the mind. This person becomes free

105
00:11:14,640 --> 00:11:22,720
from the bonds of mara, the bonds of evil. So the mind is considered something that goes far,

106
00:11:22,720 --> 00:11:28,960
it travels far and can't be controlled. In a moment you can think of something a million miles,

107
00:11:28,960 --> 00:11:36,960
or a thousand miles away. And it travels alone means that you're only aware of one thing at a

108
00:11:36,960 --> 00:11:42,160
time, so the mind is always on one thing or another. It's never in two places at once, and it's

109
00:11:42,160 --> 00:11:47,840
certainly not able to comprehend everything. If you don't catch it and fix it and focus on one

110
00:11:47,840 --> 00:11:55,760
thing, it's going to jump back and forth between objects. Asarirang means it's very difficult to

111
00:11:55,760 --> 00:12:00,320
understand. And this is why in Buddhist meditation we always have you focus on the body.

112
00:12:01,040 --> 00:12:05,200
And many people don't understand this. They'll say, you know, what's the point? I really want

113
00:12:05,200 --> 00:12:09,760
to understand the mind. I want to understand how my mind works, and that's where the problem is.

114
00:12:09,760 --> 00:12:16,080
True, but it doesn't have a physical form. Asarirang, the mind is not something you can grab at and say

115
00:12:16,080 --> 00:12:25,280
there it is. It's something that relies on the physical. And so we have you focus on the physical.

116
00:12:25,280 --> 00:12:34,080
And this is another thing that the texts talk about, saying that we have you always focus on

117
00:12:34,080 --> 00:12:40,000
a physical first. And the point is that when you focus on the physical, the mind becomes

118
00:12:40,800 --> 00:12:46,000
clear by itself. Once the body becomes clear, then the mind becomes clear automatically,

119
00:12:46,000 --> 00:12:53,120
and you don't have to do anything special. You don't have to be chasing after the mind,

120
00:12:53,120 --> 00:12:58,720
just like a hunter when they want to catch a deer or a wild animal. They don't go running

121
00:12:58,720 --> 00:13:02,720
up, running through the forest after the wild animal. They sit by the place where they know

122
00:13:02,720 --> 00:13:07,440
the deer is going to come, either the waterhole or the fruit trees or someplace where they know

123
00:13:07,440 --> 00:13:11,760
that the deer are bound to come at some point, and they sit there and they wait.

124
00:13:11,760 --> 00:13:18,720
And sure enough, the mind will come to them, and they'll be able to do what they want.

125
00:13:19,440 --> 00:13:23,600
The same with in our meditation, we don't have to go chasing after the mind. When we focus on

126
00:13:23,600 --> 00:13:28,400
the rising and the falling, well, that's the mind going out to the rising and to the stomach,

127
00:13:28,400 --> 00:13:32,960
and we're going to see how the mind works. We're going to get angry sometimes, frustrated at it,

128
00:13:32,960 --> 00:13:37,840
sometimes we're going to be happy sometimes, unhappy sometimes. We're going to see how our mind reacts

129
00:13:37,840 --> 00:13:44,160
to things. We're going to see how the quality and the characteristics of our mind are habits and

130
00:13:44,160 --> 00:13:52,480
so on. So it's very difficult to catch the mind without this sort of a technique. This is why we

131
00:13:52,480 --> 00:13:59,520
use the body first because it's asari, long, it has no physical form. And Gohase Young means it

132
00:13:59,520 --> 00:14:06,640
is the other part of this. It dwells in a cave, and that cave is the body. This mind is somehow

133
00:14:06,640 --> 00:14:12,320
trapped in the cage of the body. It always has to come back and be dependent on it. So when you focus

134
00:14:12,320 --> 00:14:17,600
on any part of the physical reality as it arises, you're going to be able to see the mind.

135
00:14:17,600 --> 00:14:24,960
You're going to be able to catch it. It's like finding the cage of the wild, where its limits are,

136
00:14:24,960 --> 00:14:32,160
you just watch those limits, which is in this case, the body. Okay, so good luck training the mind,

137
00:14:34,400 --> 00:14:43,520
calming the mind and keeping the mind clear and out of difficulty. This is the way as the

138
00:14:43,520 --> 00:14:49,520
Buddha said to overcome the bonds of Mara, which is like the Buddhist version of Satan,

139
00:14:49,520 --> 00:14:54,160
but it just means the evil that exists in our minds. And it's the way to overcome these things.

140
00:14:54,160 --> 00:15:24,000
Okay, so thanks for the question, hope that helps.

