1
00:00:00,000 --> 00:00:15,600
Okay, welcome everyone to the morning session in second life.

2
00:00:15,600 --> 00:00:29,440
Today, today I'd like to go back to something a little more basic and then perhaps

3
00:00:29,440 --> 00:00:44,800
usual and often I'm talking about the results of meditation or the place of meditation

4
00:00:44,800 --> 00:00:47,200
in our lives and so on.

5
00:00:47,200 --> 00:00:57,600
My meditation is so important, but I get sometimes people saying that these ideas are nice

6
00:00:57,600 --> 00:01:05,520
and interesting but very difficult to put into practice.

7
00:01:05,520 --> 00:01:15,560
And I think part of the reason for that is that if you rely on just talks and people

8
00:01:15,560 --> 00:01:36,240
giving a talk on Buddhism or meditation, it's necessarily very general, broad, vague and

9
00:01:36,240 --> 00:01:42,640
impractical as a complete method of practice.

10
00:01:42,640 --> 00:01:56,480
The purpose of a dhamma talk is to encourage and elucidate the meaning of the meditation

11
00:01:56,480 --> 00:02:03,360
practice which people are already engaged in for those people who have already received

12
00:02:03,360 --> 00:02:06,480
instruction in meditation.

13
00:02:06,480 --> 00:02:14,000
So the best role of a talk is for those people who are already meditating.

14
00:02:14,000 --> 00:02:20,480
Normally when I give talks it would be to those who are engaged in the intensive meditation

15
00:02:20,480 --> 00:02:35,520
course and I give the talk with my eyes closed and ask everyone to close their eyes.

16
00:02:35,520 --> 00:02:45,120
And practice meditation as they listen and the talk is a way to encourage and adjust

17
00:02:45,120 --> 00:02:50,400
one's practice based on the principles being discussed there.

18
00:02:50,400 --> 00:02:54,840
But it's anyway today I thought I'd go back and sort of talk about exactly what is it

19
00:02:54,840 --> 00:03:04,440
that I propose for Buddhists or people interested in Buddhist meditation to practice.

20
00:03:04,440 --> 00:03:22,800
One, one quality or characteristic virtue that is often talked about in Buddhism is this

21
00:03:22,800 --> 00:03:33,440
idea of mindfulness and I think we hear about mindfulness quite often in all of the major

22
00:03:33,440 --> 00:03:42,280
schools of Buddhism, it's considered to be a very integral part of Buddhist meditation

23
00:03:42,280 --> 00:03:53,160
practice and mental development.

24
00:03:53,160 --> 00:03:58,280
And I think this is with good cause because it's something that the Buddha himself taught

25
00:03:58,280 --> 00:04:10,000
probably more often than any other type of meditation practice and spoke of it in much

26
00:04:10,000 --> 00:04:17,400
higher praise than any other practice.

27
00:04:17,400 --> 00:04:22,280
The Buddha said that the practice of mindfulness or this word that we translate into English

28
00:04:22,280 --> 00:04:25,440
as mindfulness.

29
00:04:25,440 --> 00:04:32,480
He said it is the direct way or the straight path that leads only in one direction and

30
00:04:32,480 --> 00:04:40,320
that is to the purification of beings for the overcoming of sorrow, lamentation and despair

31
00:04:40,320 --> 00:04:48,640
for being free from mental and physical suffering, for attaining the right path and for

32
00:04:48,640 --> 00:04:52,680
realizing freedom from suffering.

33
00:04:52,680 --> 00:05:02,600
He never said this about any other practice and he was never this clear in his discussion

34
00:05:02,600 --> 00:05:09,520
of other meditation practices and so the question might come up as to why there are many

35
00:05:09,520 --> 00:05:17,800
different types of meditation practice in that case if this practice is the best way.

36
00:05:17,800 --> 00:05:25,240
And the thing I think about the Buddha is that he was able to see for individual people

37
00:05:25,240 --> 00:05:32,960
what they required in a way that I would say most if not all teachers of today are not

38
00:05:32,960 --> 00:05:36,520
able, even the best teachers.

39
00:05:36,520 --> 00:05:49,400
And I've seen and been in contact with several fairly experienced teachers and as great

40
00:05:49,400 --> 00:06:01,200
and wonderful as they are, they still have problems judging and assessing every meditators

41
00:06:01,200 --> 00:06:05,080
practice in the way that the Buddha was able to do.

42
00:06:05,080 --> 00:06:12,320
We have a story that apparently even the Buddha's chief disciples, Sariput, would get

43
00:06:12,320 --> 00:06:22,200
it wrong at times where he would recommend a certain meditation practice and it turned

44
00:06:22,200 --> 00:06:24,840
out to be the wrong one for that individual person.

45
00:06:24,840 --> 00:06:30,120
It's very difficult to know where a person is coming from, what's going to come up in

46
00:06:30,120 --> 00:06:34,160
meditation because we're not dealing just with this life, we're dealing with all of

47
00:06:34,160 --> 00:06:36,720
the complexities that make up who we are.

48
00:06:36,720 --> 00:06:45,040
When we come to a meditation course we can have a certain mode of behavior and that

49
00:06:45,040 --> 00:06:50,280
mode of behavior can completely change through the course of the practice due to other

50
00:06:50,280 --> 00:06:58,360
factors from another part of our history coming up and coming to the forefront as the

51
00:06:58,360 --> 00:07:05,680
old modes of behavior disappear, new modes of behavior appear and it's kind of like layers

52
00:07:05,680 --> 00:07:08,600
of an onion in that sense.

53
00:07:08,600 --> 00:07:15,600
So it's quite difficult and I think for this reason the safest bet is to stick with this

54
00:07:15,600 --> 00:07:24,680
very general broad-based and seemingly universal, universally applicable meditation.

55
00:07:24,680 --> 00:07:32,640
In Buddhism we often talk about the different character types and there are many different

56
00:07:32,640 --> 00:07:38,560
meditation practices that are suitable only for specific character types.

57
00:07:38,560 --> 00:07:49,720
So for some people if a person is of a lustful temperament then they should focus on the

58
00:07:49,720 --> 00:07:58,200
objective impurities of the body as an example as a way of sort of countering that, if

59
00:07:58,200 --> 00:08:04,080
their person who is very much addicted to sensuality then by focusing on the body you

60
00:08:04,080 --> 00:08:11,240
come to see that actually there's nothing particularly attractive about it.

61
00:08:11,240 --> 00:08:17,360
If a person is an angry or hateful temperament then they should practice loving kindness.

62
00:08:17,360 --> 00:08:22,880
But not vice versa if a person is a hating temperament full of anger they shouldn't focus

63
00:08:22,880 --> 00:08:24,440
on the impurities of the body.

64
00:08:24,440 --> 00:08:32,680
If a person is a lustful temperament then they also won't do very well necessarily focusing

65
00:08:32,680 --> 00:08:33,680
on loving kindness.

66
00:08:33,680 --> 00:08:41,080
I'm not sure about that one but the point is in the case of loving kindness for a person

67
00:08:41,080 --> 00:08:48,760
with lustful temperament it can easily become lust and attraction towards the object, the

68
00:08:48,760 --> 00:08:51,320
person who is the object.

69
00:08:51,320 --> 00:08:57,280
But there are many examples like that of meditation practices that aren't suitable for an

70
00:08:57,280 --> 00:09:01,840
individual person.

71
00:09:01,840 --> 00:09:06,600
But when it comes to practicing mindfulness we don't have this characterisation and I think

72
00:09:06,600 --> 00:09:11,360
this is a problem that I've encountered often with people who have been Buddhist for a long

73
00:09:11,360 --> 00:09:18,760
time and say that mindfulness meditation is just not suitable to their character.

74
00:09:18,760 --> 00:09:28,080
But according to the Buddhist texts the ones I follow anyway.

75
00:09:28,080 --> 00:09:35,920
The practice of mindfulness is outside of that classification because mindfulness is dealing

76
00:09:35,920 --> 00:09:36,920
with ultimate realities.

77
00:09:36,920 --> 00:09:46,480
It's not a meditation that is for calming the mind, it's not a creation based meditation

78
00:09:46,480 --> 00:09:53,440
where you focus on the concept or on the beings or something that you imagine or think

79
00:09:53,440 --> 00:09:58,280
of in your mind you're focusing on what's in front of you.

80
00:09:58,280 --> 00:10:02,240
And if you look at all of the Buddhist teaching you can see that this is really what the

81
00:10:02,240 --> 00:10:08,640
Buddha wanted us to focus on even when he was not saying meditating this way in that way.

82
00:10:08,640 --> 00:10:17,400
What he was talking about was always one form or another of the objects of mindfulness.

83
00:10:17,400 --> 00:10:24,520
The four what we call the four foundations or the four establishments of mindfulness.

84
00:10:24,520 --> 00:10:37,560
So I think this is without a doubt the most widely used and widely applicable form of

85
00:10:37,560 --> 00:10:42,240
meditation practice and it's the safest for a meditation teacher that's why so many

86
00:10:42,240 --> 00:10:49,480
people use it because it clearly has objective benefits for pretty much everyone who

87
00:10:49,480 --> 00:10:55,120
who undertakes to practice it.

88
00:10:55,120 --> 00:11:00,200
So a word about mindfulness that's the first problem, the next problem once we've established

89
00:11:00,200 --> 00:11:05,760
that mindfulness is a useful meditation is to understand what is mindfulness.

90
00:11:05,760 --> 00:11:11,360
And the problem of course is that the word mindfulness is a fairly poor translation and

91
00:11:11,360 --> 00:11:16,360
most Buddhist teachers who are familiar with the old languages of India, Sanskrit,

92
00:11:16,360 --> 00:11:22,040
Pauli will tell you that it's not really a proper translation.

93
00:11:22,040 --> 00:11:25,800
Actually I like the word and I think it is a good word.

94
00:11:25,800 --> 00:11:33,760
Mindfulness is this sense of being alert and knowing what you're doing and being present.

95
00:11:33,760 --> 00:11:37,640
And I think that's important in the meditation but it doesn't quite capture what we're

96
00:11:37,640 --> 00:11:41,720
doing when we meditate.

97
00:11:41,720 --> 00:11:42,800
Meditation is a work.

98
00:11:42,800 --> 00:11:48,640
It's something that you have to do, it's a practice that you have to undertake.

99
00:11:48,640 --> 00:11:55,840
Like in transcendental meditation you have an object and you have to develop that.

100
00:11:55,840 --> 00:11:58,280
Meditation is considered a form of development.

101
00:11:58,280 --> 00:12:06,400
So I think the problem with mindfulness is generally assume that if you're watching reality,

102
00:12:06,400 --> 00:12:09,600
if you're looking at it, that you're going to see it clearly.

103
00:12:09,600 --> 00:12:13,880
It's simply by being present when you walk knowing that you walk, when you sit knowing

104
00:12:13,880 --> 00:12:21,440
that you sit in the sense of having a consciousness of it, that that's enough.

105
00:12:21,440 --> 00:12:25,000
And this is an argument in Buddhism.

106
00:12:25,000 --> 00:12:30,160
Some people do believe there are meditation centers that do believe that that's the case.

107
00:12:30,160 --> 00:12:35,320
So I'm not going to say it's not true that you have some level of mindfulness.

108
00:12:35,320 --> 00:12:43,080
So what I'd like to suggest is that by working, by actually trying to develop a clearer

109
00:12:43,080 --> 00:13:02,600
and more perfect understanding of the present moment, you get a far more, a far deeper

110
00:13:02,600 --> 00:13:12,760
understanding and realization of the truth in a shorter time, that by simply watching

111
00:13:12,760 --> 00:13:20,640
you don't have the necessary focus and clarity of mind.

112
00:13:20,640 --> 00:13:24,920
This is what I've seen through the practice of meditation, of the meditation that I teach

113
00:13:24,920 --> 00:13:27,360
and that's why I teach it.

114
00:13:27,360 --> 00:13:32,360
It often seems a little bit counterintuitive for those people who kind of have this

115
00:13:32,360 --> 00:13:44,000
idea that meditation should be a relaxing or a comfortable state where you're just taking

116
00:13:44,000 --> 00:13:51,080
it easy, kind of settling into the moment as it were, as a way of calming the mind.

117
00:13:51,080 --> 00:13:56,520
And whereas calming the mind is a part of meditation, what I want to express is that

118
00:13:56,520 --> 00:14:03,280
from what I've seen, in order to really gain true and lasting food from the practice,

119
00:14:03,280 --> 00:14:08,480
there's a certain amount of work that is required and that we shouldn't be complacent

120
00:14:08,480 --> 00:14:14,560
and thinking that we can just sit, watch and kind of let our minds float around and expect

121
00:14:14,560 --> 00:14:17,080
to gain real benefits.

122
00:14:17,080 --> 00:14:21,680
I mean you can try both of them from what I've seen.

123
00:14:21,680 --> 00:14:25,360
There's a great benefit that comes from working and if you actually put out the effort,

124
00:14:25,360 --> 00:14:29,360
you find that your mind is much more stable, strong.

125
00:14:29,360 --> 00:14:36,640
It's kind of like working out the body when you put out the right amount of effort,

126
00:14:36,640 --> 00:14:45,040
when you work, when you push yourself again and again and again in a repetitious manner,

127
00:14:45,040 --> 00:14:51,840
what you gain out of it is not any visible benefit, like when you lift weights, you aren't

128
00:14:51,840 --> 00:14:56,560
actually lifting things and when you finish you don't have a bunch of things up in

129
00:14:56,560 --> 00:14:57,560
a high place.

130
00:14:57,560 --> 00:15:02,400
You're lifting up and down, up and down and there's no outcome, but the weights return

131
00:15:02,400 --> 00:15:07,880
to their original location, but what you gain is a physical strength.

132
00:15:07,880 --> 00:15:13,960
So the work and meditation, you do something repetitious again and again and again and

133
00:15:13,960 --> 00:15:18,200
we do this walking back and forth and people think, well that's boring and stupid, you're

134
00:15:18,200 --> 00:15:21,120
not going anywhere, it's just pacing back and forth.

135
00:15:21,120 --> 00:15:25,200
But in the same way as when you're lifting weights, at the end of the day you've gained

136
00:15:25,200 --> 00:15:31,480
something, you've gained a strength of mind, a fortitude of mind and this is something

137
00:15:31,480 --> 00:15:34,320
that's verifiable.

138
00:15:34,320 --> 00:15:43,800
So what I'm talking about here is an application of the mind, mindfulness in a meditative

139
00:15:43,800 --> 00:15:52,040
sense is best understood as the work that we put in to remind ourselves of the reality,

140
00:15:52,040 --> 00:15:58,760
the word septi, which we translate as mindfulness, actually has something to do with the word

141
00:15:58,760 --> 00:16:02,760
remembrance or to remember.

142
00:16:02,760 --> 00:16:07,720
When you remember your past lives, they call that septi, what we translate as mindfulness.

143
00:16:07,720 --> 00:16:22,160
When you think of the Buddha, they call that septi, when you think of any object, when

144
00:16:22,160 --> 00:16:28,200
you remember something or recollect or send your mind out to the object, thinking of it,

145
00:16:28,200 --> 00:16:30,000
they call that septi.

146
00:16:30,000 --> 00:16:35,600
This isn't exactly what I'm talking about here in terms of the objects of mindfulness

147
00:16:35,600 --> 00:16:40,800
or the foundations of mindfulness, but it gets you an idea of where the word comes from.

148
00:16:40,800 --> 00:16:52,240
It has to do with remembering or recognizing or establishing in the mind the object.

149
00:16:52,240 --> 00:16:59,160
So this is incredibly important in our relationship with ultimate reality, with the reality

150
00:16:59,160 --> 00:17:02,280
in front of us, because we don't do this.

151
00:17:02,280 --> 00:17:05,320
When we see something, we like it or we dislike it.

152
00:17:05,320 --> 00:17:09,040
When we hear something, we like it or we just, we're always judging.

153
00:17:09,040 --> 00:17:12,440
Even when I'm talking, now there's judgments going through everyone's mind, unless

154
00:17:12,440 --> 00:17:18,560
you're enlightened, you're thinking, boy, good talk, boy, bad talk, boring, and it'll change

155
00:17:18,560 --> 00:17:19,960
for a moment to moment.

156
00:17:19,960 --> 00:17:21,360
So you might enjoy the talk.

157
00:17:21,360 --> 00:17:27,960
I assume that you're here because you think I have something useful to say, but that

158
00:17:27,960 --> 00:17:32,320
be that as it may, there's going to be moments where suddenly your mind gets bored.

159
00:17:32,320 --> 00:17:37,800
Your mind starts to lose interest and starts to think about checking something out, else

160
00:17:37,800 --> 00:17:45,040
I would go back to Facebook or YouTube or whatever.

161
00:17:45,040 --> 00:17:46,880
It's not something that you can blame on yourself.

162
00:17:46,880 --> 00:17:47,920
It's the way of the mind.

163
00:17:47,920 --> 00:17:55,320
The mind judges liking, disliking, and reacting continuously.

164
00:17:55,320 --> 00:18:02,880
I've been studying a bit about quantum physics, if you've been following me, you've

165
00:18:02,880 --> 00:18:09,440
been probably hearing about how I've become interested in this.

166
00:18:09,440 --> 00:18:14,200
The reason I'm so interested in it is because that's exactly what quantum physics allows

167
00:18:14,200 --> 00:18:16,000
for or describes.

168
00:18:16,000 --> 00:18:24,760
I've been told by several people that the mind has nothing to do with quantum physics.

169
00:18:24,760 --> 00:18:30,720
But there's this really interesting author that I've been reading that one of the experts

170
00:18:30,720 --> 00:18:38,240
in the field of quantum physics, and he explains how quantum physics really leaves open

171
00:18:38,240 --> 00:18:44,560
or leaves a perfect space for the mind, for consciousness, for what we already experience.

172
00:18:44,560 --> 00:18:56,320
And that is to interpret and to make decisions with everything that is experienced.

173
00:18:56,320 --> 00:19:06,800
There's the moment where the mind intervenes and collapses the quantum state from a smeared

174
00:19:06,800 --> 00:19:14,200
out series of possibilities to a specific state, a decision that it's going to be like this.

175
00:19:14,200 --> 00:19:20,280
And this is very close to the description of karma in Buddhism.

176
00:19:20,280 --> 00:19:23,040
And it's very close to what is experienced in meditation.

177
00:19:23,040 --> 00:19:32,360
I was quite impressed to see that what we've been doing in meditation, realizing in meditation,

178
00:19:32,360 --> 00:19:39,480
is being explained by quantum physics, people who are testing this and saying, oh, this

179
00:19:39,480 --> 00:19:41,160
is how it looks like it's happening.

180
00:19:41,160 --> 00:19:48,200
And we're like, yeah, that is how it happens, which is quite exciting.

181
00:19:48,200 --> 00:19:53,480
So all we're trying to do here, this intervention goes on, the mental intervention goes

182
00:19:53,480 --> 00:19:58,040
on in every moment, every time we experience something, it's a mental intervention into the

183
00:19:58,040 --> 00:20:06,200
otherwise closed physical reality, this physical realm that goes in terms of cause and

184
00:20:06,200 --> 00:20:14,880
effect and is otherwise untouched, X causes, Y causes, Z and so on.

185
00:20:14,880 --> 00:20:24,960
When the mind gets in there, it's able to change things or it seems to play a part in this.

186
00:20:24,960 --> 00:20:29,440
This is quite obvious in meditation that we are able to decide to do this or decide to

187
00:20:29,440 --> 00:20:35,200
do that, judge things in this way or that way.

188
00:20:35,200 --> 00:20:41,120
So what we're doing in the practice of mindfulness is trying to purify that intervention

189
00:20:41,120 --> 00:20:43,520
or purify that mental state.

190
00:20:43,520 --> 00:20:45,720
When we see something, we see it purely.

191
00:20:45,720 --> 00:20:55,000
We have a pure response, a response that is creating peace, happiness, harmony, big

192
00:20:55,000 --> 00:21:12,160
bears with boxes on their feet and freedom from suffering.

193
00:21:12,160 --> 00:21:15,640
We do this at every moment in the meditation.

194
00:21:15,640 --> 00:21:19,680
Our meditation has to be a moment by moment practice.

195
00:21:19,680 --> 00:21:23,920
This is why you always hear in Buddhism, stay in the present moment, don't think about

196
00:21:23,920 --> 00:21:25,880
the future, don't think about the past.

197
00:21:25,880 --> 00:21:30,440
It's not just a nice thing to think or to appreciate.

198
00:21:30,440 --> 00:21:35,120
It's actually a meditation instruction that right now is when you're meditating.

199
00:21:35,120 --> 00:21:40,080
You can't say, I've been sitting for an hour, I'm going to sit for an hour in practice

200
00:21:40,080 --> 00:21:45,960
and then you say, I sat for an hour, that's really the wrong way to look at meditation.

201
00:21:45,960 --> 00:21:51,080
Right now, if you're meditating right now, then right now is meditative and that has no bearing

202
00:21:51,080 --> 00:21:57,800
on the next moment because the decisions that we make, the judgments that we make occur

203
00:21:57,800 --> 00:22:03,040
at every moment of seeing, hearing, smelling, tasting, feeling or thinking.

204
00:22:03,040 --> 00:22:04,680
This is incessant.

205
00:22:04,680 --> 00:22:09,280
Our meditation has to be incessant, it has to be moment to moment.

206
00:22:09,280 --> 00:22:13,000
This is sort of the basis of the practice that I teach.

207
00:22:13,000 --> 00:22:20,560
If you've been already following, indeed, if you've already been following the practice that

208
00:22:20,560 --> 00:22:32,200
I've been teaching, you can see that, this is what I'm talking about, when the stomach

209
00:22:32,200 --> 00:22:36,520
rises and you focus on the rising and then you forget about it and the stomach falls,

210
00:22:36,520 --> 00:22:41,480
you focus on the falling, when you feel pain in the body, you focus on the pain and you

211
00:22:41,480 --> 00:22:48,840
simply see it for what it is, you strengthen and fortify the mind at that moment so that

212
00:22:48,840 --> 00:22:55,080
when it makes a judgment, it makes one that's clear and pure and impartial and seeing

213
00:22:55,080 --> 00:23:01,280
is seeing, hearing is hearing, smelling is smelling, tasting is tasting, feeling is

214
00:23:01,280 --> 00:23:05,160
feeling, thinking is thinking.

215
00:23:05,160 --> 00:23:11,960
So this is the understanding that I have of what is meant by mindfulness and this is how

216
00:23:11,960 --> 00:23:15,640
I practice and how I teach.

217
00:23:15,640 --> 00:23:23,280
The method, if you're not familiar with it, is to use a label or a word or the way I like

218
00:23:23,280 --> 00:23:28,240
to talk about it now is a mantra because everybody's familiar, most people are familiar

219
00:23:28,240 --> 00:23:31,680
with mantras, it's something that is said to focus your mind.

220
00:23:31,680 --> 00:23:41,640
So it's kind of coincidental that these two practices use the same technique because

221
00:23:41,640 --> 00:23:45,960
a mantra meditation generally is nothing to do with the reality in front of us.

222
00:23:45,960 --> 00:23:51,520
You don't hear people saying mantras like pain, pain, pain or so on.

223
00:23:51,520 --> 00:23:58,440
You hear mantras like God or Jesus or Buddha or Om or so on, you have mantras which

224
00:23:58,440 --> 00:24:03,560
are have some spiritual meaning for people and are somehow special.

225
00:24:03,560 --> 00:24:10,800
So the idea of watching or stomach and saying rising, falling, rising, falling.

226
00:24:10,800 --> 00:24:16,360
It seems rather counterintuitive, it's something mundane.

227
00:24:16,360 --> 00:24:27,440
But the incredible thing is that it in fact is the more profound of the two because who

228
00:24:27,440 --> 00:24:31,320
we are is the most profound subject of all.

229
00:24:31,320 --> 00:24:37,400
And by focusing on ourselves and that is our very mundane selves, the reality of who

230
00:24:37,400 --> 00:24:43,800
we are, this is the body and the mind.

231
00:24:43,800 --> 00:24:47,600
We come to understand the whole of the universe because we're dealing in terms of not

232
00:24:47,600 --> 00:24:55,560
of something that is conceptual, like our concept of God or our concept of the soul,

233
00:24:55,560 --> 00:25:01,360
our concept of heaven or enlightenment or whatever.

234
00:25:01,360 --> 00:25:08,680
We're focusing on something that is perfectly real, that is real in some ultimate sense

235
00:25:08,680 --> 00:25:11,880
meaning verifiably.

236
00:25:11,880 --> 00:25:16,160
Everyone can verify that our stomach does rise and does fall, everyone can verify that

237
00:25:16,160 --> 00:25:22,080
we do have pain in the body.

238
00:25:22,080 --> 00:25:29,120
Everyone can verify that there is thought, can verify that there are emotions.

239
00:25:29,120 --> 00:25:37,000
And the problem is that most of the time we miss this and we think of these things as

240
00:25:37,000 --> 00:25:40,200
mundane, as boring, as uninteresting.

241
00:25:40,200 --> 00:25:47,200
We often have this sense of self, I don't want to say hatred, but it's a low self

242
00:25:47,200 --> 00:25:55,480
of steam or we dismiss the very nature of who we are as being mundane, boring, uninteresting

243
00:25:55,480 --> 00:25:56,480
and so on.

244
00:25:56,480 --> 00:26:00,440
One of the reasons why we come on second life and deck ourselves out to be something

245
00:26:00,440 --> 00:26:06,520
we're not because who we are is generally fairly disappointing.

246
00:26:06,520 --> 00:26:16,800
So, I would submit that the teachings of the Buddha are not to find something new and

247
00:26:16,800 --> 00:26:25,200
exciting, but to make that which is uninteresting, boring, dull and seemingly useless to

248
00:26:25,200 --> 00:26:33,120
find some meaning and use and benefit in it to come back to who we really are, what

249
00:26:33,120 --> 00:26:43,120
we really are and to transform that into a pure and meaningful reality.

250
00:26:43,120 --> 00:26:48,640
And we do this by watching it and as I said by purifying the whole system of who we are

251
00:26:48,640 --> 00:26:53,040
because that's really what we are, we're a system, we're a physical, we have a physical

252
00:26:53,040 --> 00:26:58,040
component and that system is going in terms of cause and effect, but then we also have

253
00:26:58,040 --> 00:27:02,240
a mental component that is able to adjust and to alter this.

254
00:27:02,240 --> 00:27:07,720
The practice of the Buddha is to adjust and alter it in such a way so that it becomes harmonious

255
00:27:07,720 --> 00:27:21,160
and pure, that it becomes free from defilement, free from evil, free from suffering so

256
00:27:21,160 --> 00:27:28,520
that we're able to work as a system in a way that is perfectly pure and beneficial

257
00:27:28,520 --> 00:27:32,960
most to ourselves and to other people.

258
00:27:32,960 --> 00:27:35,640
I would say the practice of meditation allows this.

259
00:27:35,640 --> 00:27:41,200
When you remind yourself of something, whether it be something in the body, say focusing

260
00:27:41,200 --> 00:27:46,200
on the movements of some part of the body, in this case, I always recommend the stomach.

261
00:27:46,200 --> 00:27:50,680
This is the one movement that we have when we're sitting still.

262
00:27:50,680 --> 00:27:55,800
If you put your hand on your stomach, you'll feel the rising in the following motion.

263
00:27:55,800 --> 00:28:00,800
So when it rises, you simply see it for what it is rising.

264
00:28:00,800 --> 00:28:08,440
Just remind yourself, stopping this or changing this mind that wants to judge, wants to

265
00:28:08,440 --> 00:28:14,760
like and dislike and get bored and upset and disappointed and so on.

266
00:28:14,760 --> 00:28:22,160
When it falls falling, you don't say it out loud, you create this idea in your mind

267
00:28:22,160 --> 00:28:30,320
that that's what that is, which is true, that's a concise explanation of what's going

268
00:28:30,320 --> 00:28:31,320
on.

269
00:28:31,320 --> 00:28:32,320
Is it the rising?

270
00:28:32,320 --> 00:28:38,120
It doesn't really matter what the word is, as long as it is as concise as possible understanding

271
00:28:38,120 --> 00:28:40,040
of what's happening.

272
00:28:40,040 --> 00:28:45,760
When you feel pain in the body, focusing on the pain, just remind yourself it's pain.

273
00:28:45,760 --> 00:28:50,560
Normally pain is something we don't want to focus on, we're very quick to run away from

274
00:28:50,560 --> 00:28:54,520
to try to escape.

275
00:28:54,520 --> 00:29:00,200
When we say to ourselves, pain, pain, pain, we change that.

276
00:29:00,200 --> 00:29:13,720
We change our reaction, our reaction to one that is accepting, understanding and able

277
00:29:13,720 --> 00:29:18,160
to live with the reality that's in front of us.

278
00:29:18,160 --> 00:29:21,280
When we're thinking something, just knowing that we're thinking, reminding ourselves

279
00:29:21,280 --> 00:29:22,520
this is a thought.

280
00:29:22,520 --> 00:29:28,880
You think about, this might seem fairly banal and uninteresting, but think about how often

281
00:29:28,880 --> 00:29:33,080
our thoughts destroy us, thoughts about what we've done in the past, stupid things we

282
00:29:33,080 --> 00:29:37,840
did or bad things that other people did to us, bad things that happened or worry about

283
00:29:37,840 --> 00:29:40,640
the future, what's going to happen and so on.

284
00:29:40,640 --> 00:29:47,440
We wind up destroying ourselves, we wind up creating great amounts of suffering for ourselves.

285
00:29:47,440 --> 00:29:55,160
And so when we know that it's just a thought, you can verify for yourself.

286
00:29:55,160 --> 00:30:00,000
You say to yourself, thinking, thinking, you'll see it disappears.

287
00:30:00,000 --> 00:30:05,280
It's one of the reasons why I've become more engaged, quote unquote.

288
00:30:05,280 --> 00:30:10,320
I never thought of myself as an engaged Buddhist, but I guess that's in a sense what

289
00:30:10,320 --> 00:30:19,800
it is to teach other people, but it's just so, you know, it creates such an imperative

290
00:30:19,800 --> 00:30:26,040
in the mind when you see people suffering from things that could be solved in about five

291
00:30:26,040 --> 00:30:31,480
minutes of explanation as to how to meditate, maybe more, but you know, there are times

292
00:30:31,480 --> 00:30:37,040
where I've sent an email to someone explaining, you know, they come to me with what seems

293
00:30:37,040 --> 00:30:43,480
to be a life-threatening problem of anxiety or depression or so on.

294
00:30:43,480 --> 00:30:50,000
And just one email from me, you know, not from me, but from someone who understands or

295
00:30:50,000 --> 00:30:54,760
is able to explain these things and it's not even being special, it's just having a

296
00:30:54,760 --> 00:31:00,160
basic understanding and being able to share that understanding with others, it can really

297
00:31:00,160 --> 00:31:04,760
just end the problem then and there and they've been taking drugs and medication and

298
00:31:04,760 --> 00:31:10,720
so on and the doctors say there's something wrong with them and suddenly poof, they're

299
00:31:10,720 --> 00:31:19,280
not healed, but they have a new way of looking at it and a way out and if they stick

300
00:31:19,280 --> 00:31:27,640
with it and if they have, you know, reminders from a teacher, if they keep in contact and

301
00:31:27,640 --> 00:31:32,360
there's no question that they'll be able to overcome even the most, even some of the

302
00:31:32,360 --> 00:31:39,000
most severe mental sicknesses, I would say there are probably cases that are difficult

303
00:31:39,000 --> 00:31:44,280
and I'm not even sure if they can be solved through meditation of true chemical imbalances

304
00:31:44,280 --> 00:31:51,720
in the brain bipolar, schizophrenia and so on, those I'm not sure and I have no length

305
00:31:51,720 --> 00:31:59,000
the experience as to how meditation deals with those, but, you know, I'd be willing

306
00:31:59,000 --> 00:32:09,920
to try and I'm sure there's a chance at least, but if you're interested in getting

307
00:32:09,920 --> 00:32:21,080
touch, that's very good to hear, there you go, so even some of the quote-unquote most,

308
00:32:21,080 --> 00:32:30,080
let me say, most extreme or most, you know, the ones that are said to have the most

309
00:32:30,080 --> 00:32:39,800
in terms of a physical cause can actually be altered and affected and benefited through

310
00:32:39,800 --> 00:32:47,680
the practice of meditation and the final one is our emotions, so there are four Satipatana

311
00:32:47,680 --> 00:32:51,800
which is the, we translate it generally as the four foundations of mindfulness, which isn't

312
00:32:51,800 --> 00:32:56,120
a very good translation, but that's what they're known as, it's the body, which is

313
00:32:56,120 --> 00:33:00,920
say the rising and the falling or the movements of the body, the feelings, which is pain

314
00:33:00,920 --> 00:33:08,080
or happiness or calm, the thoughts, which is just thinking about anything and the dhammas

315
00:33:08,080 --> 00:33:16,320
or the teachings of the Buddha, which is kind of just attack and add on category for

316
00:33:16,320 --> 00:33:19,440
what everything else that you're going to have to focus on.

317
00:33:19,440 --> 00:33:26,360
And it starts with the emotions, those things that get away, get in the way of our ability

318
00:33:26,360 --> 00:33:33,880
to see things impartially, liking things, disliking things, distractions, worry, fear,

319
00:33:33,880 --> 00:33:41,520
depression, boredom, laziness, doubt.

320
00:33:41,520 --> 00:33:48,080
And so focusing on these is of great importance, especially for someone who is just beginning

321
00:33:48,080 --> 00:33:55,160
because these tend to come to the fore for most people, if you're not familiar with meditation,

322
00:33:55,160 --> 00:34:01,080
we're sitting in meditation and thinking about what we want to do, what we'd rather be doing.

323
00:34:01,080 --> 00:34:07,480
We don't like what's going on, we're judging our practice, our minds, our minds are

324
00:34:07,480 --> 00:34:11,640
not still enough and we don't like it, sitting here is painful and uncomfortable, we don't

325
00:34:11,640 --> 00:34:12,640
like it.

326
00:34:12,640 --> 00:34:15,960
These emotions that come up, we start thinking about something from the past and it makes

327
00:34:15,960 --> 00:34:24,120
us upset or makes us feel happy or we want something for the future or so on.

328
00:34:24,120 --> 00:34:34,520
So again, we have this habitual state, which in many cases is chemically wired into

329
00:34:34,520 --> 00:34:42,280
the brain, we've got these cycles of addiction and these brain patterns that are happening

330
00:34:42,280 --> 00:34:44,320
anyway.

331
00:34:44,320 --> 00:34:49,560
All we're doing here is we're using the other side of our reality, which is the mind

332
00:34:49,560 --> 00:34:57,120
to adjust and to slowly alter and in many cases it's not that slow at all, it's something

333
00:34:57,120 --> 00:35:00,600
that has immediate benefits and immediate effects.

334
00:35:00,600 --> 00:35:06,040
When you simply remind yourself that it is what it is, then stop judging.

335
00:35:06,040 --> 00:35:12,760
You stop the cycle, you change the old way of intervening, the old way of approaching

336
00:35:12,760 --> 00:35:20,920
reality in terms of liking and disliking, which is a very mental thing, into a new way of

337
00:35:20,920 --> 00:35:28,000
affecting reality and that is based on impartiality, based on acceptance, just by accepting

338
00:35:28,000 --> 00:35:32,960
things they are when you're angry, say to yourself, angry, angry when you want something

339
00:35:32,960 --> 00:35:39,360
wanting, wanting, when you feel depressed, depressed, stressed, stressed, worried, worried,

340
00:35:39,360 --> 00:35:49,800
confused, confused, unsure, bored, whatever it is, this moment, a moment, altering of the

341
00:35:49,800 --> 00:35:59,040
mind state actually has a moment to moment effect on the cycles that occur in the body.

342
00:35:59,040 --> 00:36:05,840
So this is a sort of an overview of the practice of meditation as I understand it and

343
00:36:05,840 --> 00:36:10,360
as I teach it and I thought that would be something especially for people who are new

344
00:36:10,360 --> 00:36:16,840
that's of much more benefit than giving a talk for, that would be designed for people

345
00:36:16,840 --> 00:36:21,400
who are engaged in intensive meditation practice.

346
00:36:21,400 --> 00:36:29,840
So both ways are good but I thought this would be a good one to give a talk on and also

347
00:36:29,840 --> 00:36:36,760
record which I'm doing and place on the internet for people who are new to meditation.

348
00:36:36,760 --> 00:36:40,520
So that's all I have to say for today and I'd like to thank you all for coming.

349
00:36:40,520 --> 00:36:45,960
I hope that this has been of some benefit for all of you to progress on in your practice

350
00:36:45,960 --> 00:36:53,560
of meditation and for the realization of the truth of the Buddha's teaching through the

351
00:36:53,560 --> 00:36:58,800
benefits of the practice I wish that all of you and for all of the people who are watching

352
00:36:58,800 --> 00:37:04,160
and listening to this teaching may you all find peace, happiness and freedom from suffering

353
00:37:04,160 --> 00:37:10,000
for yourselves and be able to bring this teaching to help other people find peace, happiness

354
00:37:10,000 --> 00:37:11,920
and freedom from suffering for themselves.

355
00:37:11,920 --> 00:37:12,920
Thank you all for coming.

356
00:37:12,920 --> 00:37:18,560
If you have any questions I'm happy to answer and otherwise have a good day.

