1
00:00:00,000 --> 00:00:05,000
Okay, hello and welcome back to our study of the Dhamabanda.

2
00:00:05,000 --> 00:00:15,000
Today we continue on with verse 214, which reads as follows.

3
00:00:35,000 --> 00:00:45,000
From lust comes sorrow.

4
00:00:45,000 --> 00:00:50,000
From lust comes fear or danger.

5
00:00:50,000 --> 00:01:00,000
For one who is free from lust there is no sorrow.

6
00:01:00,000 --> 00:01:05,000
When it's fear or from where could there be fear?

7
00:01:05,000 --> 00:01:13,000
Danger.

8
00:01:13,000 --> 00:01:21,000
This story was supposed to have been taught in response to an event

9
00:01:21,000 --> 00:01:32,000
where the Buddha was living in a near-wisely-wisely-wisely.

10
00:01:32,000 --> 00:01:43,000
A-wisely was run by a community of nobles.

11
00:01:43,000 --> 00:01:56,000
It seems that their system was set up quite differently from most of the rest of India that was run by kings or lords.

12
00:01:56,000 --> 00:02:09,000
The way-wisely was run sort of as a republic, all the representatives would meet together.

13
00:02:09,000 --> 00:02:17,000
There was not one leader, there were representative leaders of sorts.

14
00:02:17,000 --> 00:02:34,000
One day the Buddha was going on arms round with the monks, a group of monks.

15
00:02:34,000 --> 00:02:39,000
They saw a group of these little movies coming out of the city.

16
00:02:39,000 --> 00:02:46,000
They would go into the city, conduct their business, meet together and all their finery,

17
00:02:46,000 --> 00:02:54,000
apparently they would look quite.

18
00:02:54,000 --> 00:02:59,000
They all decked out in adornments and find clothes.

19
00:02:59,000 --> 00:03:05,000
They were coming out of the city to go to the park.

20
00:03:05,000 --> 00:03:16,000
They would have been kind of a garden, a public garden or the royal garden, I don't know.

21
00:03:16,000 --> 00:03:20,000
The Buddha saw them and he said to the monks, he said, look monks.

22
00:03:20,000 --> 00:03:30,000
For those of you who have not seen the angels in heaven, look upon these little chubby princess,

23
00:03:30,000 --> 00:03:36,000
you want to get an idea of what angels are like.

24
00:03:36,000 --> 00:03:40,000
High praise really seemed.

25
00:03:40,000 --> 00:03:48,000
I wonder why the Buddha was pointing that out to the monks or not.

26
00:03:48,000 --> 00:03:56,000
It might seem interesting to know, to have a sense of the possibility.

27
00:03:56,000 --> 00:04:01,000
Being born in angel is quite impressive.

28
00:04:01,000 --> 00:04:23,000
Being born in heaven, you are quite fine, quite resplendent.

29
00:04:23,000 --> 00:04:33,000
But the Buddha had something else in mind and so they went into the city for arms.

30
00:04:33,000 --> 00:04:42,000
While they were in the city for arms, the little chubby went to the pleasure garden.

31
00:04:42,000 --> 00:04:49,000
There was a public place because there was a courtesan there.

32
00:04:49,000 --> 00:04:56,000
I guess a courtesan is something like a prostitute.

33
00:04:56,000 --> 00:04:58,000
I'm not quite sure.

34
00:04:58,000 --> 00:04:59,000
I assume so.

35
00:04:59,000 --> 00:05:03,000
I think it was a little more elevated than a prostitute.

36
00:05:03,000 --> 00:05:14,000
But women who would hire themselves out for the pleasure of men, I think.

37
00:05:14,000 --> 00:05:17,000
They all kind of fell in love with her.

38
00:05:17,000 --> 00:05:32,000
I guess she would dance and tell stories and really a performer in a way, I think.

39
00:05:32,000 --> 00:05:45,000
And all the monks fell in love with her and started vying for her affections.

40
00:05:45,000 --> 00:05:49,000
Maybe showing off at first.

41
00:05:49,000 --> 00:05:59,000
Maybe kind of insulting each other or attacking each other and eventually coming to blows.

42
00:05:59,000 --> 00:06:07,000
And indeed in the end, causing grave harm to each other, to the point where the story says they had to be carried out,

43
00:06:07,000 --> 00:06:21,000
and bloody and battered from fighting over over their own inner lust really.

44
00:06:21,000 --> 00:06:27,000
And probably he goes as well.

45
00:06:27,000 --> 00:06:31,000
And so they came out and they were coming out of the park, going back into the city,

46
00:06:31,000 --> 00:06:36,000
just as the Buddha was coming out of for arms out from arms.

47
00:06:36,000 --> 00:06:42,000
And all the monks got to see what happened to the chuvias.

48
00:06:42,000 --> 00:06:45,000
And they got back and they started talking about this and they said,

49
00:06:45,000 --> 00:06:48,000
look at those the chuvias.

50
00:06:48,000 --> 00:06:53,000
They were all decked out like angels.

51
00:06:53,000 --> 00:06:59,000
And because of a single woman, not because of the woman, I don't think.

52
00:06:59,000 --> 00:07:11,000
Because of their own lust, they are reduced to really hell in a sense, hellish being.

53
00:07:11,000 --> 00:07:21,000
Both being in hell themselves and being the wardens of each other's hell.

54
00:07:21,000 --> 00:07:33,000
And the Buddha said, oh, when Soka went by a one sorrow and danger arise,

55
00:07:33,000 --> 00:07:35,000
they arise because of lust.

56
00:07:35,000 --> 00:07:39,000
And then he thought this verse.

57
00:07:39,000 --> 00:07:42,000
Now lust is probably not the proper translation.

58
00:07:42,000 --> 00:07:44,000
I'm not convinced by it.

59
00:07:44,000 --> 00:07:50,000
I think actually love would be the best translation for ratty.

60
00:07:50,000 --> 00:07:57,000
Ratty means enjoying or liking maybe.

61
00:07:57,000 --> 00:07:59,000
But I think it's used generally to just mean love.

62
00:07:59,000 --> 00:08:08,000
But it's referring here and probably generally usually to romantic love, like falling in love.

63
00:08:08,000 --> 00:08:11,000
So again, this is the same verse.

64
00:08:11,000 --> 00:08:13,000
The Buddha is just changing one word.

65
00:08:13,000 --> 00:08:19,000
It means that he taught this verse repeatedly in different sort of inflection.

66
00:08:19,000 --> 00:08:25,000
But as a result, we're seeing how desire, greed, low blood,

67
00:08:25,000 --> 00:08:30,000
that it's basic form, manifests itself in many different ways.

68
00:08:30,000 --> 00:08:37,000
We saw how it manifested for Wisaka and she was very much attached to her daughter

69
00:08:37,000 --> 00:08:43,000
and seen for various stories in this chapter.

70
00:08:43,000 --> 00:08:52,000
But this one deals with desire in a different way. This is the sexual or the sensual romantic desire.

71
00:08:52,000 --> 00:08:58,000
And again, like all the other sorts of desire, the other forms of what we might call love.

72
00:08:58,000 --> 00:09:01,000
It's quite often seen in a positive light.

73
00:09:01,000 --> 00:09:12,000
Romance is touted as a great thing, a great aspect of humanity, a wonderful part of life.

74
00:09:12,000 --> 00:09:20,000
Falling in love, better to have loved and lost than to never have loved at all.

75
00:09:20,000 --> 00:09:25,000
And by that, they mean romantic love.

76
00:09:25,000 --> 00:09:31,000
Love is blind as another thing she experiences.

77
00:09:31,000 --> 00:09:39,000
Sort of.

78
00:09:39,000 --> 00:09:46,000
But many would say that romance is a great part of happiness.

79
00:09:46,000 --> 00:09:52,000
It's a great part of what makes life worth living.

80
00:09:52,000 --> 00:09:58,000
And people would even say it's an important part of what makes life possible at all, right?

81
00:09:58,000 --> 00:10:06,000
Without lust, without romance, they say there would be no human race.

82
00:10:06,000 --> 00:10:13,000
Which is, this sort of idea comes up often, this sort of criticism of Buddhism or Buddhist theory comes up often.

83
00:10:13,000 --> 00:10:20,000
And it misunderstands, or it shows how we're not really on the same page when we're talking about these things.

84
00:10:20,000 --> 00:10:29,000
Buddhism isn't going much deeper, and on a much more fundamental level is criticizing not just humanity, but some sorrow.

85
00:10:29,000 --> 00:10:34,000
The system within which humanity fits, the system of rebirth.

86
00:10:34,000 --> 00:10:37,000
Humanity is really just the epitome of it.

87
00:10:37,000 --> 00:10:41,000
It's the center of it, because humans can experience great pleasure.

88
00:10:41,000 --> 00:10:45,000
They can also experience great pain, and they're not overwhelmed by either.

89
00:10:45,000 --> 00:10:51,000
So they're in a sense, in a great position to observe both of these potentially.

90
00:10:51,000 --> 00:10:56,000
They're also in a great position to fall under the power of either.

91
00:10:56,000 --> 00:10:59,000
But there's nothing.

92
00:10:59,000 --> 00:11:12,000
There's not a very good argument to say that somehow keeping humanity going is a defense for romance.

93
00:11:12,000 --> 00:11:15,000
Really misunderstanding the whole argument.

94
00:11:15,000 --> 00:11:24,000
And that is that humanity is really part of the problem.

95
00:11:24,000 --> 00:11:31,000
We're not looking to save or cherish or preserve anything.

96
00:11:31,000 --> 00:11:39,000
We're looking to understand and to free ourselves from the suffering that exists for all of us.

97
00:11:39,000 --> 00:11:45,000
And in regards to romance, there is a great suffering involved.

98
00:11:45,000 --> 00:11:57,000
Also, like all other kinds of desire, it's mostly, most acutely, problematic in regards to not getting what you want.

99
00:11:57,000 --> 00:12:02,000
Or having what you want, be threatened.

100
00:12:02,000 --> 00:12:10,000
So the most acute, obvious glaring sort of suffering is when you love, fall in love with someone, and it's not returned.

101
00:12:10,000 --> 00:12:15,000
Just very common.

102
00:12:15,000 --> 00:12:27,000
It's only when someone gets lucky, then they find someone else who wishes to engage with them on the same level as they wish to engage.

103
00:12:27,000 --> 00:12:33,000
Also, common is falling in love with each other, and then falling out of love.

104
00:12:33,000 --> 00:12:40,000
Or one person falling out of love and devastating the other person, cheating on each other.

105
00:12:40,000 --> 00:12:45,000
A much more sinister sort of suffering that comes from romance.

106
00:12:45,000 --> 00:12:51,000
And that's relating to the story of the Litch of Jesus, what it does to you as a person.

107
00:12:51,000 --> 00:12:57,000
Lust and desire, like all forms of greed, don't make you a better person.

108
00:12:57,000 --> 00:13:04,000
You don't become more kind and generous and gentle and compassionate because of your desire.

109
00:13:04,000 --> 00:13:10,000
You become more possessive, more clingy, more stingy.

110
00:13:10,000 --> 00:13:16,000
These are the qualities we're not talking about exactly a romantic engagement.

111
00:13:16,000 --> 00:13:23,000
Because, of course, people can be married for many years without great amounts of lust arising,

112
00:13:23,000 --> 00:13:31,000
but lust, desire, craving is a key component in the degradation of the human mind.

113
00:13:31,000 --> 00:13:36,000
It causes great corruption.

114
00:13:36,000 --> 00:13:38,000
It's what causes us to fight.

115
00:13:38,000 --> 00:13:42,000
It causes us to hurt, lash out at each other.

116
00:13:42,000 --> 00:13:48,000
Why we hurt the ones we love, you see, from very small age.

117
00:13:48,000 --> 00:13:54,000
People who say, children are innocent, I think don't know very many children.

118
00:13:54,000 --> 00:14:01,000
They don't know children very well because children, as soon as they learn what it means,

119
00:14:01,000 --> 00:14:11,000
they start fighting over toys, fighting, fighting over sensuality,

120
00:14:11,000 --> 00:14:20,000
crying and yelling and screaming over, not getting what they want, throwing temper tantrums when they can't get what they want.

121
00:14:20,000 --> 00:14:22,000
It brings out the worst in us.

122
00:14:22,000 --> 00:14:25,000
Desire brings out the worst in us.

123
00:14:25,000 --> 00:14:32,000
It causes us to steal and kill and harm, talk badly about each other.

124
00:14:32,000 --> 00:14:36,000
It causes us to lash out and hurt those who we supposedly love or those who we do love,

125
00:14:36,000 --> 00:14:39,000
who we do care for, who we do wish well for.

126
00:14:39,000 --> 00:14:45,000
We hurt them because of our blindness from craving.

127
00:14:45,000 --> 00:14:47,000
We're so caught up.

128
00:14:47,000 --> 00:14:55,000
What it does is it gets us so caught up in wanting the thing that makes us happy that we are blind.

129
00:14:55,000 --> 00:15:02,000
And in our blindness we lash out, not because of any intellectual, rational reasoning,

130
00:15:02,000 --> 00:15:16,000
to do the simple blindness from lust and passion.

131
00:15:16,000 --> 00:15:21,000
And so like all forms of greed, you can point this all out.

132
00:15:21,000 --> 00:15:25,000
And I think the last one is most compelling.

133
00:15:25,000 --> 00:15:35,000
But some people might still argue that for them, love has been a great thing.

134
00:15:35,000 --> 00:15:43,000
You know, the saying, better to have loved and lost than to have never loved it all.

135
00:15:43,000 --> 00:15:51,000
Ascribe some sort of greatness to falling in love, greatness to romance.

136
00:15:51,000 --> 00:16:04,000
I think it again relates to this blindness, but it points out how complete this blindness is that we romanticize romance.

137
00:16:04,000 --> 00:16:07,000
We even romanticize the suffering.

138
00:16:07,000 --> 00:16:14,000
Some people kill themselves when their love is unrequited, but we romanticize that.

139
00:16:14,000 --> 00:16:19,000
We don't look at that and say, wow, I'm never loving because look at what it can happen.

140
00:16:19,000 --> 00:16:23,000
We think, oh, what a terrible thing that we kind of feel good about it.

141
00:16:23,000 --> 00:16:26,000
Yes, love is so terrible.

142
00:16:26,000 --> 00:16:37,000
But we kind of like that about love in the sense, which is so absurd, but it's the romanticizing of it.

143
00:16:37,000 --> 00:16:44,000
And so it goes so far as people quite common, I think, to look at their own situation and say,

144
00:16:44,000 --> 00:16:51,000
love has been pretty good to me. And so they can ignore all the horrors that it inflicts on others,

145
00:16:51,000 --> 00:16:55,000
or they can romanticize them, which I think is actually more common.

146
00:16:55,000 --> 00:17:00,000
You just don't deeply appreciate the suffering.

147
00:17:00,000 --> 00:17:05,000
And even when you do suffer, even a person who suffers from love probably doesn't say,

148
00:17:05,000 --> 00:17:09,000
oh, love is terrible. I should just stop loving because that would be the solution, right?

149
00:17:09,000 --> 00:17:15,000
A person who had unrequited love, all they have to do is see how terrible and horrible love can be.

150
00:17:15,000 --> 00:17:28,000
Our love is really. And they would give it up, which is the difference between what we do and what such a person does.

151
00:17:28,000 --> 00:17:33,000
It would ordinary people do when something is causing them suffering.

152
00:17:33,000 --> 00:17:40,000
They try to obliterate it to the extent of even obliterating themselves.

153
00:17:40,000 --> 00:17:46,000
We don't try to obliterate lusts. We try to understand it to see it.

154
00:17:46,000 --> 00:17:53,000
And so for this person or people who are inclined to say that there's a greatness or a goodness,

155
00:17:53,000 --> 00:18:03,000
or a inherent quality or benefit to lust or romance,

156
00:18:03,000 --> 00:18:08,000
these arguments that, oh, bad things could happen to you.

157
00:18:08,000 --> 00:18:11,000
Like all other kinds of greed are not relevant.

158
00:18:11,000 --> 00:18:16,000
People can look at others whose relatives have passed away and say,

159
00:18:16,000 --> 00:18:25,000
bad luck for them, but mostly there's greatness to attachment to affection.

160
00:18:25,000 --> 00:18:32,000
And so again, it's a good reason why meditation is so important.

161
00:18:32,000 --> 00:18:41,000
When we talk about mindfulness, we're talking about a clearer understanding of the experience.

162
00:18:41,000 --> 00:18:51,000
So we all experience these emotions throughout our lives. We're born with them as we grow up. We have desires.

163
00:18:51,000 --> 00:18:59,000
Meditation or mindfulness is simply looking at these realities that we are born with, that we live with.

164
00:18:59,000 --> 00:19:03,000
Looking at them more clearly than we have before.

165
00:19:03,000 --> 00:19:07,000
And the result is that we understand them in a way that we haven't.

166
00:19:07,000 --> 00:19:11,000
We understand them better than we did before.

167
00:19:11,000 --> 00:19:23,000
There's a sharper, a clearer, a more perfect understanding of the emotions.

168
00:19:23,000 --> 00:19:33,000
And it's that which frees us from their power, from the suffering,

169
00:19:33,000 --> 00:19:39,000
which frees us from them. Because we come to see not that.

170
00:19:39,000 --> 00:19:48,000
Not that lust is a dangerous or a cause for suffering, really.

171
00:19:48,000 --> 00:19:52,000
It's that it in a sense is suffering.

172
00:19:52,000 --> 00:20:05,000
In the sense of being useless, in the sense of being pointless, meaningless.

173
00:20:05,000 --> 00:20:19,000
Being superfluous, extraneous, and entirely would say Jung makes you busy without any purpose.

174
00:20:19,000 --> 00:20:25,000
Because you start to see how much energy is spent getting caught up in lustful thoughts,

175
00:20:25,000 --> 00:20:34,000
desirous thoughts. And then not to speak of all of the physical and verbal activity that has to go on.

176
00:20:34,000 --> 00:20:41,000
In order to obtain the objects of your desire, the people or be they things,

177
00:20:41,000 --> 00:20:45,000
how much mental and physical energy goes into it.

178
00:20:45,000 --> 00:20:51,000
And you observe the feelings of pleasure that arise from getting what you want.

179
00:20:51,000 --> 00:20:59,000
You observe the desire for those feelings, the liking of those feelings.

180
00:20:59,000 --> 00:21:06,000
And observing them, you see how meaningless they are.

181
00:21:06,000 --> 00:21:16,000
You don't see any sense of attraction because the attraction is actually extraneous, extrinsic to the experiences.

182
00:21:16,000 --> 00:21:22,000
It's not inbuilt into the pleasure that it should be that something that should be desired.

183
00:21:22,000 --> 00:21:27,000
You see the feelings just as feelings, because that is all they are.

184
00:21:27,000 --> 00:21:36,000
Actually, we miss this. We miss that there actually is no inherent good to the experiences.

185
00:21:36,000 --> 00:21:42,000
They're momentary, they arise and cease, and that's about all you can say of them.

186
00:21:42,000 --> 00:21:46,000
And so it's not that you become repulsed by them or turned away from them.

187
00:21:46,000 --> 00:21:49,000
You just see them as pointless, arising and ceasing.

188
00:21:49,000 --> 00:21:58,000
And so if they are, in the end, not helpful, not actually conducive to a greater sense of happiness or peace,

189
00:21:58,000 --> 00:22:06,000
because they are people who seek out the happiness the most are often less are actually not often,

190
00:22:06,000 --> 00:22:09,000
but are, as a result, less happy for it.

191
00:22:09,000 --> 00:22:18,000
A person who stops seeking out happiness, specifically because of their peace,

192
00:22:18,000 --> 00:22:25,000
because of their contentment, they are much happier.

193
00:22:25,000 --> 00:22:34,000
And so if those feelings that we're craving for and lusting after and even hurting other people for,

194
00:22:34,000 --> 00:22:43,000
causing stress and suffering in others in order to obtain, if they are garbage,

195
00:22:43,000 --> 00:22:46,000
then why are we doing this?

196
00:22:46,000 --> 00:22:52,000
Of course you have no reason to seek out, no reason to strive for,

197
00:22:52,000 --> 00:22:55,000
no reason to even crave for.

198
00:22:55,000 --> 00:22:58,000
So the craving becomes withers up.

199
00:22:58,000 --> 00:23:05,000
A lot of what you feel in meditation is like the weeds in your garden are just withering up and dying.

200
00:23:05,000 --> 00:23:10,000
All of the weeds, all of the stuff in the mind that it doesn't just suddenly disappear.

201
00:23:10,000 --> 00:23:12,000
It feels like it's actually withering up.

202
00:23:12,000 --> 00:23:17,000
It's becoming weaker, it's becoming less,

203
00:23:17,000 --> 00:23:23,000
moving, less powerful.

204
00:23:23,000 --> 00:23:31,000
And you feel left with a great sense of purity and peace and contentment, happiness.

205
00:23:31,000 --> 00:23:37,000
And you're free from the sorrows that come from it and you're free from the danger,

206
00:23:37,000 --> 00:23:42,000
the danger of being beat up for what you love,

207
00:23:42,000 --> 00:23:47,000
the dangers of beating other people up,

208
00:23:47,000 --> 00:23:54,000
the dangers of getting into strife and conflict.

209
00:23:54,000 --> 00:23:59,000
And if you extrapolate on this, you could point out that war has,

210
00:23:59,000 --> 00:24:04,000
I think most often been fought based on greed.

211
00:24:04,000 --> 00:24:07,000
There's also delusion and of course anger in there,

212
00:24:07,000 --> 00:24:10,000
but greed has always been the main cause.

213
00:24:10,000 --> 00:24:12,000
My country is going to war,

214
00:24:12,000 --> 00:24:20,000
greed by the leaders, greed for power and resources,

215
00:24:20,000 --> 00:24:28,000
money, wealth, fame, power, opulence.

216
00:24:28,000 --> 00:24:35,000
I'm going to do crazy, terrible, awful, the worst things in order to get what we want.

217
00:24:35,000 --> 00:24:37,000
It's very dangerous, that's the greatest danger,

218
00:24:37,000 --> 00:24:39,000
not that someone else might hurt you,

219
00:24:39,000 --> 00:24:43,000
but that you might become an evil person, a bad person,

220
00:24:43,000 --> 00:24:47,000
harmful to others, mean, nasty,

221
00:24:47,000 --> 00:24:50,000
because of your greed.

222
00:24:50,000 --> 00:24:57,000
So, this is why

223
00:24:57,000 --> 00:25:01,000
this is another reason why we

224
00:25:01,000 --> 00:25:07,000
take up mindfulness, why we esteem it so much.

225
00:25:07,000 --> 00:25:10,000
It's another important aspect of why we practice

226
00:25:10,000 --> 00:25:13,000
and what our practice is meant for.

227
00:25:13,000 --> 00:25:16,000
It should be a real great encouragement

228
00:25:16,000 --> 00:25:21,000
when we see and when we understand the truth of this,

229
00:25:21,000 --> 00:25:25,000
that our craving is not bringing us happiness,

230
00:25:25,000 --> 00:25:31,000
it's keeping us from it.

231
00:25:31,000 --> 00:25:35,000
So, great appreciation for all of the people

232
00:25:35,000 --> 00:25:38,000
who do practice mindfulness,

233
00:25:38,000 --> 00:25:40,000
not just for that peace and happiness

234
00:25:40,000 --> 00:25:41,000
you bring to yourselves,

235
00:25:41,000 --> 00:25:46,000
but the harmony and peace that you bring to the world.

236
00:25:46,000 --> 00:25:48,000
When you free yourself from desire,

237
00:25:48,000 --> 00:25:57,000
you free others from the evil potential,

238
00:25:57,000 --> 00:26:00,000
that would become a part of who you are,

239
00:26:00,000 --> 00:26:07,000
if you were intent on it.

240
00:26:07,000 --> 00:26:09,000
So, another important verse,

241
00:26:09,000 --> 00:26:13,000
an important teaching on the causes of suffering.

242
00:26:13,000 --> 00:26:18,000
That's the Dhammapada for tonight. Thank you for listening.

