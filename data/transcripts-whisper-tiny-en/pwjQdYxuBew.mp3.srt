1
00:00:00,000 --> 00:00:24,000
Hey good evening everyone, welcome to our Saturday, evening dhamma.

2
00:00:24,000 --> 00:00:37,000
Today we're looking at the Pia Jatikas with Midi Minigai 87.

3
00:00:37,000 --> 00:00:52,000
And it's not a very complicated lesson and there's not much to the lesson, but what is there I think is quite important.

4
00:00:52,000 --> 00:00:59,000
And it's a good story.

5
00:00:59,000 --> 00:01:08,000
It addresses an important issue because there's not a lot I think to say, but I'll say what I can.

6
00:01:08,000 --> 00:01:10,000
And I'll tell the story of course.

7
00:01:10,000 --> 00:01:13,000
So the story goes.

8
00:01:13,000 --> 00:01:19,000
And the Buddha was in sawati and the Buddha spent most of his life, majority of his life in sawati.

9
00:01:19,000 --> 00:01:26,000
It's a city, it was a city in India at that time.

10
00:01:26,000 --> 00:01:42,000
Now when you go to that area all you see is fields and the vague outline of the city wall.

11
00:01:42,000 --> 00:01:48,000
One of the biggest cities of time is disappeared, gone.

12
00:01:48,000 --> 00:01:53,000
2,500 years later, changed, no.

13
00:01:53,000 --> 00:01:59,000
Change gets rid of it, removes everything.

14
00:01:59,000 --> 00:02:06,000
But the Buddha was dwelling in sawati and there was a certain man.

15
00:02:06,000 --> 00:02:14,000
Some kind of, some probably a wealthy, well respected individual.

16
00:02:14,000 --> 00:02:27,000
And his son died and with the death of his son he lost all desire to work or eat.

17
00:02:27,000 --> 00:02:47,000
All he did was, went to the cemetery and cried, cried out, my only son where are you, my only son where are you?

18
00:02:47,000 --> 00:02:56,000
And after some time of this he became so distraught that he started to think,

19
00:02:56,000 --> 00:02:59,000
that's many people do and distress hits.

20
00:02:59,000 --> 00:03:01,000
Who could help me with this?

21
00:03:01,000 --> 00:03:03,000
And of course he thought of the Buddha.

22
00:03:03,000 --> 00:03:06,000
He'd heard good things about this.

23
00:03:06,000 --> 00:03:11,000
Records and sawati, go and see the Buddha.

24
00:03:11,000 --> 00:03:17,000
And he says to the Buddha he says, oh no he goes and sits down and the Buddha says to him,

25
00:03:17,000 --> 00:03:21,000
he says, you don't look so good.

26
00:03:21,000 --> 00:03:28,000
His, your faculties are not those of one who is in control of his own mind.

27
00:03:28,000 --> 00:03:32,000
He appeared deranged.

28
00:03:32,000 --> 00:03:39,000
You don't appear to be in control of your own mind.

29
00:03:39,000 --> 00:03:44,000
And he says, oh, how could I not be deranged?

30
00:03:44,000 --> 00:03:48,000
My dear and beloved son is died and since he died.

31
00:03:48,000 --> 00:03:57,000
I have no more desire to work or eat, I just go to the graveyard and cry.

32
00:03:57,000 --> 00:04:04,000
And the Buddha says, indeed, so it is, so it is.

33
00:04:04,000 --> 00:04:10,000
Soil lamentation, pain, grief and despair are born from those who are dear,

34
00:04:10,000 --> 00:04:14,000
arise from those who are dear.

35
00:04:14,000 --> 00:04:23,000
And that's the thesis of this, the core of this discourse,

36
00:04:23,000 --> 00:04:26,000
is this statement.

37
00:04:26,000 --> 00:04:33,000
We heard it before, I think, you've probably heard me say it.

38
00:04:33,000 --> 00:04:40,000
It's a hard statement to hear, let alone,

39
00:04:40,000 --> 00:04:45,000
agree with and appreciate and accept.

40
00:04:45,000 --> 00:04:49,000
And this man certainly is having nothing of it.

41
00:04:49,000 --> 00:04:52,000
He came all this way to hear the Buddha and the Buddha says this,

42
00:04:52,000 --> 00:04:55,000
and oh boy does he get upset?

43
00:04:55,000 --> 00:05:03,000
He says, who would everything that sorrow, lamentation, despair, pain, grief?

44
00:05:03,000 --> 00:05:07,000
I would think those things are born from who are dear.

45
00:05:07,000 --> 00:05:14,000
And herbal sir, happiness and joy are born from those who are dear.

46
00:05:14,000 --> 00:05:19,000
When we, something that is dear to us, that thing,

47
00:05:19,000 --> 00:05:26,000
how it gives us such happiness, right?

48
00:05:26,000 --> 00:05:38,000
For the side people, we think this sort of thing about all of our possessions.

49
00:05:38,000 --> 00:05:40,000
The story goes on.

50
00:05:40,000 --> 00:05:44,000
He gets very upsetting, he gets up and leaves.

51
00:05:44,000 --> 00:05:46,000
Displeased with the blessed ones words.

52
00:05:46,000 --> 00:05:52,000
It's very rare that someone gets up and leaves having been displeased by what the Buddha says.

53
00:05:52,000 --> 00:05:55,000
But anyway, this is the case.

54
00:05:55,000 --> 00:06:00,000
And he goes and, as he's walking,

55
00:06:00,000 --> 00:06:04,000
he's walking home and sees some gamblers.

56
00:06:04,000 --> 00:06:07,000
Some people playing dice.

57
00:06:07,000 --> 00:06:12,000
Gamblers, I think the connotation is these are sort of immoral,

58
00:06:12,000 --> 00:06:17,000
a shady individual, these are gamblers.

59
00:06:17,000 --> 00:06:27,000
Kind of people who drink alcohol and love a gamble, but the most noble of occupations.

60
00:06:27,000 --> 00:06:34,000
Obsessed with money, perhaps cheating, but certainly manipulative.

61
00:06:34,000 --> 00:06:36,000
There's something to do with a gambling gambling,

62
00:06:36,000 --> 00:06:39,000
as I don't think in general very.

63
00:06:39,000 --> 00:06:44,000
It's certainly given a negative connotation anyway.

64
00:06:44,000 --> 00:06:47,000
They don't go too much into that.

65
00:06:47,000 --> 00:06:49,000
He goes to these gamblers and he asks them.

66
00:06:49,000 --> 00:06:51,000
He says, oh, they went and said this.

67
00:06:51,000 --> 00:06:55,000
And the Buddha said this, and then I said this.

68
00:06:55,000 --> 00:06:59,000
And they say, yeah, happiness and they agree with them.

69
00:06:59,000 --> 00:07:02,000
Happiness and joy come from those who are here.

70
00:07:02,000 --> 00:07:06,000
And how soldier goes home and this man goes home and says,

71
00:07:06,000 --> 00:07:13,000
thinking to himself, I agree with the gamblers.

72
00:07:13,000 --> 00:07:16,000
So eventually the story gets to the king.

73
00:07:16,000 --> 00:07:18,000
The king percenna day, and he's not the most brilliant

74
00:07:18,000 --> 00:07:21,000
if you know anything about this king.

75
00:07:21,000 --> 00:07:25,000
His portrait is not a very brilliant individual to say the least.

76
00:07:25,000 --> 00:07:29,000
But his queen is quite smart.

77
00:07:29,000 --> 00:07:31,000
The king says this to the king.

78
00:07:31,000 --> 00:07:36,000
This is what the king is what I've heard about the Buddha.

79
00:07:36,000 --> 00:07:39,000
The Buddha says, but go to my, he says,

80
00:07:39,000 --> 00:07:44,000
sorrow, lamentation, pain, grief, and despair are born from those who are here.

81
00:07:44,000 --> 00:07:48,000
What it says is that liking things makes you unhappy.

82
00:07:48,000 --> 00:07:54,000
Those things that you like cause you stress and suffering.

83
00:07:54,000 --> 00:07:58,000
Liking things causes stress and suffering.

84
00:07:58,000 --> 00:08:04,000
And the queen says, well, if the Buddha has said it, then it is true.

85
00:08:04,000 --> 00:08:07,000
And the king gets angry at her and he says,

86
00:08:07,000 --> 00:08:10,000
you just agree with whatever he says.

87
00:08:10,000 --> 00:08:12,000
The Buddha says this.

88
00:08:12,000 --> 00:08:14,000
You just say so it is.

89
00:08:14,000 --> 00:08:18,000
The Buddha says that and he says so it is.

90
00:08:18,000 --> 00:08:27,000
And so he expels the queen, he tells her to go away.

91
00:08:27,000 --> 00:08:31,000
And so the queen goes to one of her,

92
00:08:31,000 --> 00:08:38,000
a brahmana, somebody who, an advisor, I guess you'd say.

93
00:08:38,000 --> 00:08:43,000
She says, go and talk to the Buddha and ask him what he meant by.

94
00:08:43,000 --> 00:08:46,000
Ask somebody who meant by that saying.

95
00:08:46,000 --> 00:08:53,000
The brahmana goes to the Buddha and the Buddha gives him the explanation.

96
00:08:53,000 --> 00:09:00,000
It's rather than, that's about it for the story.

97
00:09:00,000 --> 00:09:07,000
But the Buddha gives a simple explanation on why it is that something that is dear to us

98
00:09:07,000 --> 00:09:14,000
is considered to cause stress and suffering.

99
00:09:14,000 --> 00:09:22,000
He says that in this very city,

100
00:09:22,000 --> 00:09:28,000
if you think about the vision of the Buddha,

101
00:09:28,000 --> 00:09:39,000
I'm talking about a much broader vision than simply the situation that each of us finds ourselves in.

102
00:09:39,000 --> 00:09:46,000
And so what we'd say is that most of us are fairly narrow minded.

103
00:09:46,000 --> 00:10:01,000
And a person who is in the process of enjoying,

104
00:10:01,000 --> 00:10:06,000
enjoying possession of the things that they love.

105
00:10:06,000 --> 00:10:11,000
It's hard for them to see any truth in this saying

106
00:10:11,000 --> 00:10:16,000
that things that are dear to you bring suffering,

107
00:10:16,000 --> 00:10:30,000
that the overall result of what is dear is stress and suffering and despair.

108
00:10:30,000 --> 00:10:32,000
But the Buddha said in this city,

109
00:10:32,000 --> 00:10:41,000
that this is, you can find a person, a man, a woman, a young old.

110
00:10:41,000 --> 00:10:52,000
You can find them lamenting parents, children, brothers, sisters, wives, husbands.

111
00:10:52,000 --> 00:10:59,000
You can find people going crazy with despair and grief.

112
00:10:59,000 --> 00:11:04,000
My aunt and uncle right now are, I think, still quite upset to personally

113
00:11:04,000 --> 00:11:14,000
my aunt and uncle, because our cousin passed away.

114
00:11:14,000 --> 00:11:18,000
It's a curious thing when someone dies as a Buddhist,

115
00:11:18,000 --> 00:11:28,000
because there's not this sort of sadness.

116
00:11:28,000 --> 00:11:43,000
It's a hard thing to relate to for someone who has come to practice meditation,

117
00:11:43,000 --> 00:11:49,000
because you see the truth of this,

118
00:11:49,000 --> 00:12:03,000
that a person who is even enjoying the presence and the possession of the things that they love.

119
00:12:03,000 --> 00:12:08,000
Even this person who is on the upswing, you might say,

120
00:12:08,000 --> 00:12:20,000
is engaging in the process of stress and suffering.

121
00:12:20,000 --> 00:12:27,000
So the Buddha talks about how a person who holds someone dear

122
00:12:27,000 --> 00:12:37,000
and then loses that person, there's great suffering that comes from the change.

123
00:12:37,000 --> 00:12:47,000
But in meditator, it sees even the liking of something as stressful.

124
00:12:47,000 --> 00:12:54,000
It sees those people who are clinging that they're caught up in attention,

125
00:12:54,000 --> 00:12:57,000
there's attention to it.

126
00:12:57,000 --> 00:13:01,000
And you see how this tension is stressful.

127
00:13:01,000 --> 00:13:13,000
There's a worry about a mother or a father who loves their child so much.

128
00:13:13,000 --> 00:13:24,000
Because it ultimately comes down to something much simpler than loss or beings.

129
00:13:24,000 --> 00:13:34,000
It's at a much simpler level of losing and being losing a son, losing a daughter, a child, a parent, a loved one.

130
00:13:34,000 --> 00:13:38,000
It's about change.

131
00:13:38,000 --> 00:13:51,000
A fundamental level when we talk about losing someone, we're talking about a state of mind that is stuck

132
00:13:51,000 --> 00:13:59,000
and has been disturbed because of being out of sync with reality.

133
00:13:59,000 --> 00:14:06,000
If you think of reality as like a car,

134
00:14:06,000 --> 00:14:15,000
and then you are in a cart, let's say something simpler, like an ox cart.

135
00:14:15,000 --> 00:14:31,000
And you're riding in an ox cart and suddenly your robe gets stuck on a bramble bush.

136
00:14:31,000 --> 00:14:36,000
And you're riding in the ox cart and your robe gets stuck on it, but the ox cart keeps going.

137
00:14:36,000 --> 00:14:43,000
The ox cart keeps going because that's the nature of the ox cart.

138
00:14:43,000 --> 00:14:52,000
So because you're stuck on the bramble bush, great danger and great danger.

139
00:14:52,000 --> 00:14:53,000
You're out of sync.

140
00:14:53,000 --> 00:15:00,000
You're going to become soon quite out of sync with the ox cart to fall out of it and hurt yourself.

141
00:15:00,000 --> 00:15:07,000
The person who is stuck on something has lost their sync with reality.

142
00:15:07,000 --> 00:15:18,000
It's not even really pleasant for a meditator to like something or to indulge in pleasant sensations

143
00:15:18,000 --> 00:15:21,000
because it's out of sync.

144
00:15:21,000 --> 00:15:23,000
It's disturbed.

145
00:15:23,000 --> 00:15:28,000
It feels unnatural.

146
00:15:28,000 --> 00:15:31,000
It's in and of itself intrinsically.

147
00:15:31,000 --> 00:15:37,000
That's really what the Buddha says. He talks about change.

148
00:15:37,000 --> 00:15:48,000
He says, if someone you love, changes.

149
00:15:48,000 --> 00:15:50,000
This is what the queen says to the king.

150
00:15:50,000 --> 00:15:54,000
She says, do you love your son? He's, oh yes, I love my son.

151
00:15:54,000 --> 00:15:58,000
What if he changed?

152
00:15:58,000 --> 00:16:02,000
What if he died? But not just died. It doesn't take death.

153
00:16:02,000 --> 00:16:05,000
Because it's not really beings that we're attached to.

154
00:16:05,000 --> 00:16:08,000
It's experiences.

155
00:16:08,000 --> 00:16:15,000
And so if your son is, our daughter is always saying nice things to you.

156
00:16:15,000 --> 00:16:17,000
Or it's very cute.

157
00:16:17,000 --> 00:16:20,000
But then they grow up and they get a foul mouth.

158
00:16:20,000 --> 00:16:26,000
They turn 13, 14, and they start saying nasty things.

159
00:16:26,000 --> 00:16:35,000
Talking back, fighting, arguing, so much anger comes, so much anger.

160
00:16:35,000 --> 00:16:38,000
More anger than if someone you didn't know.

161
00:16:38,000 --> 00:16:48,000
More anger than if someone you didn't love were to say or to respond in that way.

162
00:16:48,000 --> 00:16:52,000
Because it's change. Because we're stuck on that.

163
00:16:52,000 --> 00:16:55,000
We're stuck on something that is not real.

164
00:16:55,000 --> 00:16:58,000
We're stuck on things being this way.

165
00:16:58,000 --> 00:17:06,000
But that way is uncertain.

166
00:17:06,000 --> 00:17:12,000
If you could be stuck on things being always the way they actually are,

167
00:17:12,000 --> 00:17:14,000
then that would be something else.

168
00:17:14,000 --> 00:17:16,000
But that's not how desire works.

169
00:17:16,000 --> 00:17:22,000
Desire is not something where you can just like whatever happens.

170
00:17:22,000 --> 00:17:26,000
This liking is a stickiness.

171
00:17:26,000 --> 00:17:29,000
It's nothing like reality.

172
00:17:29,000 --> 00:17:33,000
It's nothing like mindfulness.

173
00:17:33,000 --> 00:17:42,000
The only way to be content or to be happy with whatever comes is to be perfectly in sync with reality.

174
00:17:42,000 --> 00:17:45,000
It's why people will say,

175
00:17:45,000 --> 00:17:50,000
fine and good things that we love can cause us stress and suffering.

176
00:17:50,000 --> 00:17:54,000
But if you give them up, then you give up all the happiness that comes from them.

177
00:17:54,000 --> 00:17:56,000
And they think of Buddhism as kind of this,

178
00:17:56,000 --> 00:18:03,000
it's an option where you give up all happiness in order to give up all suffering.

179
00:18:03,000 --> 00:18:05,000
So no happiness, no suffering.

180
00:18:05,000 --> 00:18:09,000
But it's not really like that.

181
00:18:09,000 --> 00:18:15,000
We have a natural way, a way that is in tune with reality that has none of this stress.

182
00:18:15,000 --> 00:18:22,000
And then you have this stressful way that is really involved with wrong view.

183
00:18:22,000 --> 00:18:30,000
And it's a pernicious and highly deeply ingrained wrong view.

184
00:18:30,000 --> 00:18:32,000
But you have to understand that it's wrong view,

185
00:18:32,000 --> 00:18:42,000
that it is not a given that happiness comes from seeking out things.

186
00:18:42,000 --> 00:18:47,000
No, it's actually wrong from a Buddhist would argue that it's wrong.

187
00:18:47,000 --> 00:18:55,000
Which I think is harder to accept and maybe it even sounds.

188
00:18:55,000 --> 00:19:10,000
Deep down, we have this axiom that if you're looking for happiness, seek out that which makes you happy.

189
00:19:10,000 --> 00:19:14,000
So we take meditation in that way, right?

190
00:19:14,000 --> 00:19:17,000
I'm doing meditation because it's going to make me happy.

191
00:19:17,000 --> 00:19:26,000
I have that mindset even for meditation.

192
00:19:26,000 --> 00:19:31,000
But that whole idea, the idea of seeking something out, the idea of clinging to something,

193
00:19:31,000 --> 00:19:37,000
of liking something, that that might lead to happiness is wrong.

194
00:19:37,000 --> 00:19:46,000
And the state of liking actually takes away your happiness, takes away your peace of mind.

195
00:19:46,000 --> 00:19:57,000
It takes away your connection with reality, puts you in a new frame of reference where the only variables are,

196
00:19:57,000 --> 00:20:03,000
do I have what I want or do I not have what I want?

197
00:20:03,000 --> 00:20:12,000
And the only happiness comes from always getting what you want, which of course is impossible.

198
00:20:12,000 --> 00:20:16,000
And it's not even happiness because it's not real.

199
00:20:16,000 --> 00:20:22,000
It's no longer, I am experiencing this, this is what's happening.

200
00:20:22,000 --> 00:20:28,000
I am getting what I want.

201
00:20:28,000 --> 00:20:32,000
I am liking, I am wanting.

202
00:20:32,000 --> 00:20:41,000
It's an unnatural state, just state of mind that is no longer natural.

203
00:20:41,000 --> 00:20:48,000
So there's more to it than simply saying, well, we're going to give up things.

204
00:20:48,000 --> 00:20:56,000
There's this idea that in Buddhism you have to give up everything that makes you happy.

205
00:20:56,000 --> 00:20:59,000
But that's not really how it works, right?

206
00:20:59,000 --> 00:21:02,000
The point is that's not how it works.

207
00:21:02,000 --> 00:21:08,000
Happiness doesn't come from wanting or liking things.

208
00:21:08,000 --> 00:21:15,000
Happiness is a result of the result of peace of mind.

209
00:21:15,000 --> 00:21:20,000
And so there's two ways to get peace of mind, right?

210
00:21:20,000 --> 00:21:30,000
To be constantly fulfilling the objects of your desire or to stop desiring anything.

211
00:21:30,000 --> 00:21:37,000
And then the point is that even if you're constantly fulfilling the objects of your desire,

212
00:21:37,000 --> 00:21:41,000
the nature of it is such that you're not satisfied.

213
00:21:41,000 --> 00:21:45,000
The nature of desire is to breed for their desire.

214
00:21:45,000 --> 00:21:52,000
The nature of contentment is to breed contentment, so it's quite a different state.

215
00:21:52,000 --> 00:21:56,000
You can't say, I desire something and so that breeds contentment.

216
00:21:56,000 --> 00:22:01,000
It's not like that habits are not static.

217
00:22:01,000 --> 00:22:06,000
They don't lead to other things, habits lead to perpetuating themselves.

218
00:22:06,000 --> 00:22:12,000
So a habit of mindfulness of contentment leads to further mindfulness and contentment.

219
00:22:12,000 --> 00:22:19,000
A habit of wanting and liking leads to more wanting and more liking, stronger and stronger.

220
00:22:19,000 --> 00:22:30,000
Attachment to a specific state and that attached state in and of itself is artificial.

221
00:22:30,000 --> 00:22:32,000
It's artificial.

222
00:22:32,000 --> 00:22:37,000
Well, I'd encourage you to take up meditation and study.

223
00:22:37,000 --> 00:22:42,000
Really the important thing is that when we talk about losing a person,

224
00:22:42,000 --> 00:22:48,000
we have to look at what's actually going on in the mind.

225
00:22:48,000 --> 00:22:55,000
So if you focus your attention on the being, my son died, my daughter died,

226
00:22:55,000 --> 00:23:00,000
my husband, my mother, father died.

227
00:23:00,000 --> 00:23:07,000
It's very hard to see the reality of the situation.

228
00:23:07,000 --> 00:23:11,000
That's not the reality of the situation.

229
00:23:11,000 --> 00:23:22,000
There was a great pleasure of possession and the stability and the certainty of that thing,

230
00:23:22,000 --> 00:23:30,000
that person, that concept is now impossible.

231
00:23:30,000 --> 00:23:32,000
You never will see that person again.

232
00:23:32,000 --> 00:23:39,000
So seeing the experience of seeing is no longer accessible to you.

233
00:23:39,000 --> 00:23:41,000
You will never hear them speak.

234
00:23:41,000 --> 00:23:44,000
You will never feel their touch.

235
00:23:44,000 --> 00:23:50,000
You want to see them smile, we want to see them play, we want to see them laugh.

236
00:23:50,000 --> 00:23:57,000
You want to hear them talk to us and say nice things to us.

237
00:23:57,000 --> 00:24:02,000
Because again, it doesn't take death that person can just stop being nice to you.

238
00:24:02,000 --> 00:24:07,000
You have husbands and wives who love each other and then suddenly start hating each other.

239
00:24:07,000 --> 00:24:12,000
It's worse than if they've died.

240
00:24:12,000 --> 00:24:18,000
So what's really going on is made up of experiences.

241
00:24:18,000 --> 00:24:25,000
The experiences that you were attached to are no longer attainable.

242
00:24:25,000 --> 00:24:36,000
And instead you have experiences that you don't want.

243
00:24:36,000 --> 00:24:44,000
And so if you study those experiences, it really changes your perspective.

244
00:24:44,000 --> 00:24:51,000
I think that people get so caught up in this concept, you know, I lost this person or this person died.

245
00:24:51,000 --> 00:24:53,000
It's really just conceptual.

246
00:24:53,000 --> 00:24:59,000
From a meditator point of view, you hear this and you think, well, why are you crying?

247
00:24:59,000 --> 00:25:05,000
It's like a person is hitting themselves and you say, why are you hitting yourself?

248
00:25:05,000 --> 00:25:12,000
It's really what crying is all about, you know, a person who gets upset and say, oh, I'm so upset.

249
00:25:12,000 --> 00:25:15,000
Yes, like the Buddha says, he says, yes, yes.

250
00:25:15,000 --> 00:25:22,000
It's something that causes, when you like something, it causes something.

251
00:25:22,000 --> 00:25:28,000
It's really like someone hitting themselves, you say, why are you?

252
00:25:28,000 --> 00:25:29,000
I'm suffering.

253
00:25:29,000 --> 00:25:30,000
I'm suffering.

254
00:25:30,000 --> 00:25:34,000
Yes, so why?

255
00:25:34,000 --> 00:25:36,000
Or at least it's a shame you can't see.

256
00:25:36,000 --> 00:25:42,000
It's a shame you can't be more mindful about your stress and suffering.

257
00:25:42,000 --> 00:25:49,000
Because their attention is focused on the being, the concept, not on the reality.

258
00:25:49,000 --> 00:25:55,000
The meditator looks at the world quite differently than rather than thinking, I lost this person, I lost that person.

259
00:25:55,000 --> 00:26:00,000
They will think, here is a state of sadness.

260
00:26:00,000 --> 00:26:02,000
They might still be sad, of course.

261
00:26:02,000 --> 00:26:05,000
But if through the training of meditation, they'll be able to say,

262
00:26:05,000 --> 00:26:09,000
here is sadness, and it's because of not getting what I want.

263
00:26:09,000 --> 00:26:16,000
And they'll be able to note the sadness and let go of it.

264
00:26:16,000 --> 00:26:19,000
They'll be much better equipped to deal with the problem, right?

265
00:26:19,000 --> 00:26:24,000
A person who is, because take two people, one is mourning,

266
00:26:24,000 --> 00:26:32,000
and their only recourse is to, well, it would be to get back the thing that they lost

267
00:26:32,000 --> 00:26:35,000
which often times is impossible.

268
00:26:35,000 --> 00:26:42,000
And the meditator, who is also mourning, but their recourse is to change the way they look at the experience,

269
00:26:42,000 --> 00:26:52,000
the way they look at the object, and to be able to change that habit, they change their perspective.

270
00:26:52,000 --> 00:26:57,000
And they observe, and they see that they're causing themselves suffering.

271
00:26:57,000 --> 00:27:01,000
It's a person who actually sees, oh, I'm hitting myself, I better stop that.

272
00:27:01,000 --> 00:27:06,000
The person who is thinking about the being, they have no concept of what they are doing to themselves

273
00:27:06,000 --> 00:27:13,000
by regurgitating, rehashing these memories, and reaffirming their upset, their sadness.

274
00:27:13,000 --> 00:27:20,000
I say, I'm so sad, I'm so sad.

275
00:27:20,000 --> 00:27:30,000
So again, there's not much to this, but it really is a concept that is very key to Buddhism, of course.

276
00:27:30,000 --> 00:27:33,000
And very hard for people to swallow.

277
00:27:33,000 --> 00:27:38,000
Even the king wasn't up to it, but this man, even going to see the Buddha,

278
00:27:38,000 --> 00:27:43,000
could you imagine sitting in front of the Buddha and saying, you are so wrong.

279
00:27:43,000 --> 00:27:53,000
Hired, it's so hard for us to see because of our, because of the very nature of how we look at happiness.

280
00:27:53,000 --> 00:28:01,000
It's deeply ingrained in us to think that happiness should be found from getting things.

281
00:28:01,000 --> 00:28:07,000
Then we never stop to look at, what is that process of getting things like?

282
00:28:07,000 --> 00:28:11,000
What actually happens when we do that, when is it actually?

283
00:28:11,000 --> 00:28:14,000
What is the actual experience of it?

284
00:28:14,000 --> 00:28:20,000
When we see that, oh, it's not satisfying or pleasing at all, really.

285
00:28:20,000 --> 00:28:24,000
It's quite stressful all around, getting what you want.

286
00:28:24,000 --> 00:28:28,000
Stressful, because it's a constant, an increase of desire.

287
00:28:28,000 --> 00:28:37,000
Not getting what you want is, of course, an incredibly stressful.

288
00:28:37,000 --> 00:28:43,000
So, just some thoughts and some dumb of her tonight.

289
00:28:43,000 --> 00:28:50,000
Thank you all for tuning in. Have a good day.

