1
00:00:00,000 --> 00:00:13,480
Okay, so tonight I'm going to answer a question on love.

2
00:00:13,480 --> 00:00:29,840
So the question was something like, how do we love without attachment?

3
00:00:29,840 --> 00:00:43,840
And it's a difficult topic because it deals with some very complex

4
00:00:43,840 --> 00:01:01,840
minds, states, or complex processes, and some parts of those processes are positive, some of them are negative.

5
00:01:01,840 --> 00:01:03,840
It gets quite mixed up.

6
00:01:03,840 --> 00:01:11,840
And the reason, an important reason why that is, is because of how complex we are.

7
00:01:11,840 --> 00:01:25,840
And it's important for us to, for me, to understand this and be able to investigate this topic without becoming upset by it.

8
00:01:25,840 --> 00:01:34,840
These kind of videos are the ones that often end up upsetting someone with radical views of this sort of that sort.

9
00:01:34,840 --> 00:01:43,840
And this one's going to be probably pretty radical, so we have to slowly ease into it.

10
00:01:43,840 --> 00:01:51,840
The reason why sometimes the Buddha's teaching seems quite radical, one reason.

11
00:01:51,840 --> 00:02:09,840
It's because of how conditioned we are, and how artificial and specific our situation is as human beings.

12
00:02:09,840 --> 00:02:21,840
We exist as males and females, adults, children, relatives.

13
00:02:21,840 --> 00:02:32,840
We have reproductive organs, wombs, which allow us to give birth.

14
00:02:32,840 --> 00:02:44,840
We have chemicals that produce results and have interactions with the brain and the brain with the mind and so on.

15
00:02:44,840 --> 00:02:59,840
And our very specific formulation as human beings, not to mention our very specific cultural values that were taught when we're young.

16
00:02:59,840 --> 00:03:12,840
And the movies we watch, the stories we tell, the stories we read, the examples we set for each other are very specific.

17
00:03:12,840 --> 00:03:26,840
And so to some extent it's important for us to compare the mind and these qualities like love and attachment with the physical realm.

18
00:03:26,840 --> 00:03:29,840
And we've seen material scientists do this.

19
00:03:29,840 --> 00:03:38,840
They're able to take apart things like this or like this and tell you what it's actually composed of.

20
00:03:38,840 --> 00:03:43,840
And so I was thinking about this periodic table of the elements.

21
00:03:43,840 --> 00:03:55,840
And you don't see anything about a human or nothing in there that's specifically relating to a brain or much less society or culture.

22
00:03:55,840 --> 00:04:01,840
The physical world is made up of minute particles.

23
00:04:01,840 --> 00:04:03,840
And in fact not so many of them, right?

24
00:04:03,840 --> 00:04:09,840
There's a finite number of really small number of particles that everything's made up.

25
00:04:09,840 --> 00:04:14,840
And even if you go deeper, it's even fewer until you go so deep, you don't even know really what it is.

26
00:04:14,840 --> 00:04:17,840
And it appears to be something very strange.

27
00:04:17,840 --> 00:04:30,840
But the mind has a sort of periodic table as well, meaning there are finite and not a very large number of states.

28
00:04:30,840 --> 00:04:36,840
But again they mix together and they come in amounts, right?

29
00:04:36,840 --> 00:04:38,840
A large amount of small amount.

30
00:04:38,840 --> 00:04:40,840
And they mix with each other.

31
00:04:40,840 --> 00:04:52,840
It appears that there are an infinite number of personalities, right, or states.

32
00:04:52,840 --> 00:05:08,840
But it's important to be able to make that distinction because our ideas of what we call love and our views on our relationships with others are very much contrived.

33
00:05:08,840 --> 00:05:15,840
And often not based on any sort of good reason.

34
00:05:15,840 --> 00:05:24,840
I don't mean to say that it has to be logic or reason because of course I would say in many ways even Buddhism doesn't rely on logic or reason or intellect.

35
00:05:24,840 --> 00:05:30,840
So it's not to say that it's illogical to love or it's irrational to love.

36
00:05:30,840 --> 00:05:32,840
That's not even a good excuse.

37
00:05:32,840 --> 00:05:43,840
But the idea that it's bad, it's harmful or that it's positive and beneficial is important and you can look at it that way.

38
00:05:43,840 --> 00:05:52,840
Just as you can look at the physical realm, not from a good or bad perspective but from a perspective of the results.

39
00:05:52,840 --> 00:05:57,840
When you mix certain chemicals together they have certain results.

40
00:05:57,840 --> 00:06:01,840
Certain consequences, explosions even.

41
00:06:01,840 --> 00:06:16,840
So I normally want to separate love out into positive love and negative love but I don't even think that's going far enough.

42
00:06:16,840 --> 00:06:38,840
We have rather in Buddhism the word love is misleading.

43
00:06:38,840 --> 00:06:47,840
The reason why it's so vague means to be expressed.

44
00:06:47,840 --> 00:06:52,840
But we have in polly two words that I think sort of capture a lot of what we're talking about here and they're made that,

45
00:06:52,840 --> 00:06:57,840
which is a fairly well-known Buddhist term in Raga which may not be so well-known.

46
00:06:57,840 --> 00:07:01,840
Raga might not even be the best word but it's one good word.

47
00:07:01,840 --> 00:07:11,840
It's not the only word to talk about the other side. Raga means something like lust or passion or love in most of the ways that we mean it.

48
00:07:11,840 --> 00:07:14,840
I think it does mean love.

49
00:07:14,840 --> 00:07:18,840
But on the other side we have this thing called mitta.

50
00:07:18,840 --> 00:07:26,840
And mitta is often translated as loving kindness and I think it used to be translated often as love.

51
00:07:26,840 --> 00:07:32,840
It's a kind of love but then you have to explain what kind of love it is. It's altruistic in a sense.

52
00:07:32,840 --> 00:07:38,840
Or it's an unattached love or something but loving kindness is sort of what people have settled on.

53
00:07:38,840 --> 00:07:42,840
And I don't even think that's fair. I don't think that's going far enough as I said.

54
00:07:42,840 --> 00:07:48,840
I don't think that is an accurate definition of what mitta actually is.

55
00:07:48,840 --> 00:07:56,840
I mean just literally the word mitta means friendliness. Mitta comes from the word mitta.

56
00:07:56,840 --> 00:07:59,840
When you take a word mitta means friend.

57
00:07:59,840 --> 00:08:03,840
And when you take a word like friend and you want to say friendship,

58
00:08:03,840 --> 00:08:07,840
you just turn the eye into an ear, you strengthen it and it becomes mitta.

59
00:08:07,840 --> 00:08:14,840
It's literally grammatically it's the word friend become friendship or friendliness.

60
00:08:14,840 --> 00:08:24,840
Friendship may be not a friend, the state of being a friend or the quality of a friend.

61
00:08:24,840 --> 00:08:33,840
Friendliness is probably the most literal friendliness we would say.

62
00:08:33,840 --> 00:08:49,840
And so when we talk about having love without attachment, I don't think that's possible.

63
00:08:49,840 --> 00:08:56,840
Because I think it's a misunderstanding of where we're trying to get to or what's actually positive.

64
00:08:56,840 --> 00:09:04,840
So I would often say what is mitta, what is the difference between mitta and good love and bad love, for example.

65
00:09:04,840 --> 00:09:08,840
And you would say that good love is wishing for others to be happy.

66
00:09:08,840 --> 00:09:14,840
That's what we do when we practice mitta, we say may you be happy, may all beings be happy.

67
00:09:14,840 --> 00:09:21,840
But simply wishing for someone to be happy is not necessarily a good thing,

68
00:09:21,840 --> 00:09:30,840
it's not necessarily wholesome. Because if you take from a Buddhist perspective or it's not necessarily different from any other sort of attachment,

69
00:09:30,840 --> 00:09:37,840
because if you look at a mother's love for their children, they wish for their children to be happy of father as well.

70
00:09:37,840 --> 00:09:45,840
But they can often get quite angry and upset at their children when their children are upset when their children are unhappy.

71
00:09:45,840 --> 00:09:48,840
When their children suffer, they suffer as well.

72
00:09:48,840 --> 00:09:52,840
So it's clear that that's because they wish for their children to be happy.

73
00:09:52,840 --> 00:09:55,840
But it's still an attachment.

74
00:09:55,840 --> 00:10:01,840
So it's not to say that wishing for someone to be happy is wrong and you should never do it.

75
00:10:01,840 --> 00:10:05,840
May you be happy, oh, don't do that, you'll get attached.

76
00:10:05,840 --> 00:10:21,840
It's to say that there are qualities of mind that are involved with friendliness, that are positive,

77
00:10:21,840 --> 00:10:31,840
but that are quite different from any kind of desire for an outcome.

78
00:10:31,840 --> 00:10:37,840
And that's really the difference between the difference.

79
00:10:37,840 --> 00:10:41,840
You can see the difference even on a conceptual level.

80
00:10:41,840 --> 00:10:48,840
If you talk about friends, people who are friendly, take a friendly store clerk, for example.

81
00:10:48,840 --> 00:10:56,840
Sometimes you go into a store and the store clerk is friendly because they feel like they have to and they're trying to be friendly.

82
00:10:56,840 --> 00:11:06,840
Sometimes it feels artificial, but still it's pleasant and there's a pleasantness associated with their innocence or their innocuousness.

83
00:11:06,840 --> 00:11:10,840
They're not being a threat to you.

84
00:11:10,840 --> 00:11:14,840
They're state of not being out to get you.

85
00:11:14,840 --> 00:11:21,840
Waiters, waitresses are often very pleasant to engage in and they know it's not because they love us.

86
00:11:21,840 --> 00:11:25,840
They're not loving but they're friendly.

87
00:11:25,840 --> 00:11:29,840
And you see this quality in families.

88
00:11:29,840 --> 00:11:40,840
You'll see parents being friendly with their children being happy towards their children, pleasant towards their children, children towards their parents and so on.

89
00:11:40,840 --> 00:11:45,840
And we say, well, that's love because we mix it, we mix it with all the other qualities.

90
00:11:45,840 --> 00:11:54,840
But it's not actually, it's just this quality that people in the world also have.

91
00:11:54,840 --> 00:12:05,840
The actual desire for even for someone else to be happy is often what leads to great suffering.

92
00:12:05,840 --> 00:12:17,840
The most obvious ones are when you have romantic desire towards another person and they don't return that.

93
00:12:17,840 --> 00:12:22,840
They say, I kind of love you want for them to make you happy.

94
00:12:22,840 --> 00:12:31,840
But it doesn't even have to be that way when, as I said, when you wish for someone else to be happy and they're not happy or suffer greatly as well.

95
00:12:31,840 --> 00:12:39,840
When parents wish for their kids to be happy and so on.

96
00:12:39,840 --> 00:12:52,840
And so it all doesn't escape the realm of the desire for some kind of an outcome.

97
00:12:52,840 --> 00:13:08,840
It doesn't categorically differ from our desire for good food or desire for success or desire for wealth or desire for central pleasure.

98
00:13:08,840 --> 00:13:24,840
But on a molecular level, if we say the molecules of the mind, the results or the characteristics are even more pronounced.

99
00:13:24,840 --> 00:13:36,840
It's much easier to see sort of the difference and where, like in a family or in amongst friends or so on, you would see the differences.

100
00:13:36,840 --> 00:13:53,840
Because the qualities of love or any kind of attachment, any kind of desire are pulling or the kind of things that disturb the mind.

101
00:13:53,840 --> 00:14:03,840
If you are mindful and friendly, say, take a waiter or waitress, for example.

102
00:14:03,840 --> 00:14:13,840
They can be perfectly, imagine you're an enlightened waitress, waiter, a waiter, a waiter, staff, server, barista.

103
00:14:13,840 --> 00:14:17,840
I often go to Starbucks for food.

104
00:14:17,840 --> 00:14:20,840
And they're the nicest people.

105
00:14:20,840 --> 00:14:23,840
I don't know there's something about that company.

106
00:14:23,840 --> 00:14:28,840
Sorry, I probably skipped a laughing at me.

107
00:14:28,840 --> 00:14:34,840
Corporate advertisement here, we should do it.

108
00:14:34,840 --> 00:14:38,840
I'm just kidding.

109
00:14:38,840 --> 00:14:40,840
But they're the nicest people.

110
00:14:40,840 --> 00:14:42,840
They really are.

111
00:14:42,840 --> 00:14:45,840
It's clearly been trained at it.

112
00:14:45,840 --> 00:14:47,840
They've been selected for it or something.

113
00:14:47,840 --> 00:14:50,840
And they have this attitude of friendliness.

114
00:14:50,840 --> 00:14:59,840
So imagine you have an enlightened barista, someone who is just so pure in their heart.

115
00:14:59,840 --> 00:15:04,840
What I'm trying to say is you come in and you ask them or something and they do it.

116
00:15:04,840 --> 00:15:06,840
So they're disturbed.

117
00:15:06,840 --> 00:15:17,840
And so it looks like they've actually, because they want to help you, because they have a desire for you to be happy that they're bringing you what you need or so on.

118
00:15:17,840 --> 00:15:21,840
But they can do it very, very mindfully.

119
00:15:21,840 --> 00:15:32,840
So the difference comes when we get to those states and there are states where you're no longer able to perform the mindfully,

120
00:15:32,840 --> 00:15:39,840
where they're not no longer connected with states of clarity of mind and mindfulness.

121
00:15:39,840 --> 00:15:45,840
And this is where you really, really want for the other person to be happy.

122
00:15:45,840 --> 00:15:48,840
So when you say to yourself, may you be happy?

123
00:15:48,840 --> 00:15:56,840
When you say to just in your mind, when you think, may that person be happy, may all beings be happy or so on.

124
00:15:56,840 --> 00:16:07,840
You're expressing a perspective or an inclination and it's an inclination of friendliness.

125
00:16:07,840 --> 00:16:09,840
It's a attitude.

126
00:16:09,840 --> 00:16:13,840
You're not actually saying, I want you to be happy, boy.

127
00:16:13,840 --> 00:16:16,840
It matters to me whether you're happy.

128
00:16:16,840 --> 00:16:26,840
And this is where we started to get into waters where people might be up on displeased by hearing this because they think, well, you should be caring.

129
00:16:26,840 --> 00:16:35,840
I've talked before about not caring and how caring is actually where the problem starts.

130
00:16:35,840 --> 00:16:44,840
And you say, oh, that's very harsh.

131
00:16:44,840 --> 00:16:50,840
The problem, of course, with caring is that it's an unstable state.

132
00:16:50,840 --> 00:16:53,840
The state of friendliness is very composing.

133
00:16:53,840 --> 00:16:55,840
It's very powerful.

134
00:16:55,840 --> 00:16:56,840
It's very clear.

135
00:16:56,840 --> 00:17:08,840
In fact, in Buddhism, friendliness is actually not considered to be a separate mind state.

136
00:17:08,840 --> 00:17:22,840
The definition or the characteristic of friendliness is called adosa, which simply means the lack of anger.

137
00:17:22,840 --> 00:17:34,840
So when we practice mitta, people actually will take time to cultivate mitta as a meditation practice.

138
00:17:34,840 --> 00:17:40,840
And when we do that, what we're actually doing is purifying our minds from anger.

139
00:17:40,840 --> 00:17:41,840
That's simply it.

140
00:17:41,840 --> 00:17:46,840
When you have no anger, your default attitude is friendly.

141
00:17:46,840 --> 00:17:54,840
It's a inclination, a way of relating to other beings in a pure way.

142
00:17:54,840 --> 00:17:59,840
It actually isn't this positive state of love or caring even.

143
00:17:59,840 --> 00:18:03,840
So this is a state of mind that is just pure.

144
00:18:03,840 --> 00:18:11,840
It's kind of like clear, free of any kind of judgment or reaction.

145
00:18:11,840 --> 00:18:24,840
And it's another sort of, I think, endorsement of the state of freedom from defilement as being one of naturally friendly.

146
00:18:24,840 --> 00:18:32,840
You've ever been around people who are advanced in their meditation practice, they tend to be quite friendly.

147
00:18:32,840 --> 00:18:45,840
And it might even appear that they're very caring because they will go out of their way to help you because they're not bothered by it.

148
00:18:45,840 --> 00:18:56,840
But the state of actually caring, the state of actually being bothered and having your happiness depend on the happiness of others.

149
00:18:56,840 --> 00:19:04,840
Or even worse on the ability for others to make you happy, right?

150
00:19:04,840 --> 00:19:11,840
Because if your happiness depends on others, you're still just saying, you want them to make you happy.

151
00:19:11,840 --> 00:19:17,840
I will be happy if you succeed in this way or that way. I will be happy if you're happy that sort of thing.

152
00:19:17,840 --> 00:19:19,840
It's all about you again, right?

153
00:19:19,840 --> 00:19:23,840
It's actually, you could argue, it's quite selfish.

154
00:19:23,840 --> 00:19:29,840
When you care about someone's well-being, you're actually being quite selfish, you're saying, I won't be happy.

155
00:19:29,840 --> 00:19:33,840
It's not right unless this makes me happy.

156
00:19:33,840 --> 00:19:38,840
The reason why you should be happy is so that I'll be happy.

157
00:19:38,840 --> 00:19:43,840
It actually is that sort of way. I mean, that's the problem with it.

158
00:19:43,840 --> 00:19:50,840
It's a, I mean, the deeper problem is it's a disturbed state of mind.

159
00:19:50,840 --> 00:19:58,840
It's a state that is yearning, right? It's unstable, it's weak.

160
00:19:58,840 --> 00:20:05,840
And more importantly, it's associated with negative mind states.

161
00:20:05,840 --> 00:20:12,840
And so this gets into, I mentioned the reason why we use the word love and why it's so vague

162
00:20:12,840 --> 00:20:17,840
and why the topic of love has so much mysteries surrounding it, right?

163
00:20:17,840 --> 00:20:22,840
Love is blind, why do fools fall in love, right?

164
00:20:22,840 --> 00:20:24,840
It's really a mysterious sort of mind state.

165
00:20:24,840 --> 00:20:31,840
And the reason I think quite clearly is because it's covered in darkness.

166
00:20:31,840 --> 00:20:38,840
Because love is love or, you know, it's a good deeper attachment or clinging or desire.

167
00:20:38,840 --> 00:20:48,840
These are associated not with rational or, let's not go to rational, but not with a clear mind or a bright mind.

168
00:20:48,840 --> 00:20:53,840
But they're associated with ignorance and delusion.

169
00:20:53,840 --> 00:20:56,840
They're covered over in darkness. We don't know why we fall in love.

170
00:20:56,840 --> 00:20:59,840
And we think, well, that's very mysterious and that's the mysterious nature of love.

171
00:20:59,840 --> 00:21:03,840
It's not actually. We don't know because we're ignorant because we're in darkness.

172
00:21:03,840 --> 00:21:08,840
For a person is very mindful and they say, oh yeah, that's why I'm falling in love.

173
00:21:08,840 --> 00:21:13,840
And they can actually break it apart and are able to see it.

174
00:21:13,840 --> 00:21:20,840
I think more accurately when a person is mindful, they don't fall in love.

175
00:21:20,840 --> 00:21:25,840
They just doesn't arrive because their mind is clear and they have a friendliness.

176
00:21:25,840 --> 00:21:29,840
They have a purity and a peacefulness about them.

177
00:21:29,840 --> 00:21:36,840
Again, this gets into, starts to get into where this becomes quite unpleasant.

178
00:21:36,840 --> 00:21:42,840
Because of how deeply ingrained it is in us that we should care, that we should love.

179
00:21:42,840 --> 00:21:49,840
Even that we should desire, that these are a part of life, that these are a part of what it means to be human.

180
00:21:49,840 --> 00:21:54,840
All this just turns out to be real rhetoric and really meaningless.

181
00:21:54,840 --> 00:22:02,840
If people didn't love, there would be no humans. We wouldn't get married and have kids or we wouldn't have kids.

182
00:22:02,840 --> 00:22:06,840
What would happen to humanity if we all became like you?

183
00:22:06,840 --> 00:22:08,840
As if that means anything.

184
00:22:08,840 --> 00:22:17,840
I mean, again, the stars don't care about things like humans.

185
00:22:17,840 --> 00:22:24,840
The sun doesn't care that it's nourishing plants and so on.

186
00:22:24,840 --> 00:22:28,840
The mind and mind states are what they are.

187
00:22:28,840 --> 00:22:30,840
The state of being a human is just a construct.

188
00:22:30,840 --> 00:22:32,840
It's something that's evolved over time.

189
00:22:32,840 --> 00:22:33,840
We know this.

190
00:22:33,840 --> 00:22:35,840
We know that humans haven't always lived on the earth.

191
00:22:35,840 --> 00:22:42,840
It's something that's evolved not by divine providence or design.

192
00:22:42,840 --> 00:22:49,840
That's just simply something that happens, something that over time evolved.

193
00:22:49,840 --> 00:22:59,840
Our arguments of the rightness of it all are not nearly as important as the arguments of good and bad,

194
00:22:59,840 --> 00:23:05,840
beneficial, harmful, useful, useless.

195
00:23:05,840 --> 00:23:16,840
And I think ultimately perhaps consistent and inconsistent because we strive after all number of things.

196
00:23:16,840 --> 00:23:25,840
We love anything from cheesecake to sex, to music, to family, to helping other people.

197
00:23:25,840 --> 00:23:29,840
Now, a broad perspective of things that we might love.

198
00:23:29,840 --> 00:23:33,840
We love other beings. We love them so much. We want them to be happy.

199
00:23:33,840 --> 00:23:41,840
Our children, our parents, friends.

200
00:23:41,840 --> 00:23:47,840
And it's all for the purpose of happiness, and yet it doesn't lead to where we think it leads.

201
00:23:47,840 --> 00:23:49,840
It doesn't actually lead to happiness.

202
00:23:49,840 --> 00:23:58,840
The part of us that cares, that is bothered, that is dependent, is not a static state.

203
00:23:58,840 --> 00:24:03,840
When you want for someone to be happy, you're cultivating desire.

204
00:24:03,840 --> 00:24:13,840
When you want an orange or a cheesecake, or when you want to see something beautiful, a beautiful person, human,

205
00:24:13,840 --> 00:24:17,840
just beautiful work of art even.

206
00:24:17,840 --> 00:24:20,840
You're building up a desire. It's a habit in the mind.

207
00:24:20,840 --> 00:24:24,840
I mean, scientists can show you that this is how the brain works.

208
00:24:24,840 --> 00:24:27,840
It's not a static state.

209
00:24:27,840 --> 00:24:33,840
And so it will never be a stable, peaceful, harmonious way to live.

210
00:24:33,840 --> 00:24:39,840
It's actually a cause of a great deal of suffering in the world, of course.

211
00:24:39,840 --> 00:24:48,840
But it's inherently unstable, because it's building up an increase of a need for some specific way of being.

212
00:24:48,840 --> 00:24:51,840
If things are like this, I'll be happy.

213
00:24:51,840 --> 00:25:03,840
It's why families fight with each other, as I said, when our friends or our families or our loved ones don't act the way we want.

214
00:25:03,840 --> 00:25:12,840
It's of course the cause of suffering. When we don't get what we want, when we are unable to make a romantic connection with someone else, they don't love us the way we love them, or they do.

215
00:25:12,840 --> 00:25:20,840
And we go at it for a while, and then we break up because of the violence of our emotions, the instability.

216
00:25:20,840 --> 00:25:26,840
You know, these things people say about love and hate being, what does it love and hate are?

217
00:25:26,840 --> 00:25:29,840
So there's some saying about how they're almost the same.

218
00:25:29,840 --> 00:25:35,840
And it's not really that they're almost the same, it's that they're so closely tied to each other.

219
00:25:35,840 --> 00:25:41,840
You love the more you love this thing, the more violently you will react when you don't get it.

220
00:25:41,840 --> 00:25:46,840
So we say, why is it that I love someone that one day and I hate them the next day?

221
00:25:46,840 --> 00:25:52,840
It's because you liked the things that they did. You liked the experiences that you had that day.

222
00:25:52,840 --> 00:26:04,840
And as soon as something, because you developed that liking so much, that as soon as something came that wasn't that, as soon as you weren't able to get that, why aren't you the way you were yesterday, why aren't you?

223
00:26:04,840 --> 00:26:16,840
Because we react much more violently than if it was someone we didn't love or weren't attached to.

224
00:26:16,840 --> 00:26:25,840
So this is all not answering the question, but trying to detail the difference between,

225
00:26:25,840 --> 00:26:44,840
the difference between what we might consider a wholesome form of not love, but friendliness and everything else, all the other kinds of things we call love.

226
00:26:44,840 --> 00:26:55,840
And it's so confusing to us and hard to understand because of its association with ignorance.

227
00:26:55,840 --> 00:27:10,840
So I just want to drive home that point that we don't love because of any rational or any good reason.

228
00:27:10,840 --> 00:27:14,840
We don't cling or care.

229
00:27:14,840 --> 00:27:21,840
I mean, many people will actually criticize someone who tries to say, you know, it's not rational to love, as I said.

230
00:27:21,840 --> 00:27:29,840
It's not a bad argument to say, you know, you shouldn't criticize love just because it's not rational.

231
00:27:29,840 --> 00:27:36,840
Of course, some things are not rational. That's not really the most important thing in Buddhism.

232
00:27:36,840 --> 00:27:41,840
But we don't love for any good reason. We don't love thinking that it's actually going to bring us happiness.

233
00:27:41,840 --> 00:27:47,840
This is a hard point to understand because it appears that we love things because they bring us happiness.

234
00:27:47,840 --> 00:27:57,840
That appears to be the case. It appears to be, we have some kind of thought process that, yes, I'm going to get that because it makes me happy.

235
00:27:57,840 --> 00:28:06,840
We don't. We don't because we don't have that capacity when we're craving for something.

236
00:28:06,840 --> 00:28:19,840
And this is easiest to see with a drug addict. A drug addict is rational, is generally rational in their mind to the extent that they are generally aware that this isn't making them happy.

237
00:28:19,840 --> 00:28:28,840
And yet the process of engaging in their addiction continues. They're unable to stop it.

238
00:28:28,840 --> 00:28:35,840
It has nothing to do with any kind of good reason for doing it.

239
00:28:35,840 --> 00:28:39,840
It's steeped. This is what it means to be steeped in delusion and ignorance.

240
00:28:39,840 --> 00:28:44,840
It doesn't mean we're intellectually ignorant. It means the process of liking something.

241
00:28:44,840 --> 00:28:57,840
Literally, any kind of liking that process is in darkness. That moment when you like something, you don't have the clarity of mind to see that what you're liking is good or bad.

242
00:28:57,840 --> 00:29:09,840
That's literally how liking or that's actually how liking comes to be. That's necessarily a part of the process of liking.

243
00:29:09,840 --> 00:29:25,840
And so that's why a person who is mindful, we talk about myth and what it actually means, really isn't love because it isn't because it can't be because a state of clarity of mind has no love in it.

244
00:29:25,840 --> 00:29:37,840
And as uncomfortable as that might sound to some, we have to remember that the word love is a very contrived and complicated word.

245
00:29:37,840 --> 00:29:46,840
And really doesn't say much about what's going on underneath because it means we use it in many different contexts.

246
00:29:46,840 --> 00:29:54,840
But the more salient and more accurate point is that a state of clarity of mind can't clean, can't be bothered.

247
00:29:54,840 --> 00:30:03,840
It isn't bothered, of course. It's a strong state. It's a pure and clear and composed state.

248
00:30:03,840 --> 00:30:18,840
Getting to the actual answer of how, and so I'm going to modify the question because, of course, it's ultimately well love and attachment are still just the same thing, but how can you be friendly?

249
00:30:18,840 --> 00:30:30,840
How can you be kind to others? How can you help others? How can you be caring to the extent of not actually caring, but be perfectly.

250
00:30:30,840 --> 00:30:46,840
And ultimately, completely engaged in helping others, you see, because it doesn't stop that, it actually opens that up to a very pure state.

251
00:30:46,840 --> 00:31:02,840
How can you, so how can you have that state and not the other one, not the state of caring?

252
00:31:02,840 --> 00:31:31,840
So there is the practice of what we call meta, and that's, of course, one way of engaging in it. You can develop many different positive states of mind, and so many people, if they have strong anger issues, they might develop this kind of friendliness towards others.

253
00:31:31,840 --> 00:31:47,840
Ultimately, hopefully, it's become clear that what we're actually talking about is, and where this question is coming from, is really from this very large pool of mind states that are all ultimately caught up in attachment.

254
00:31:47,840 --> 00:32:00,840
And for that, you really should never practice meta. Meta is not designed as a practice through that, because, of course, if you have attachment to someone saying, may you be happy, it's not going to stop that attachment.

255
00:32:00,840 --> 00:32:21,840
Meta is not designed, and it doesn't have the capacity to free you from attachment. It's use, and it's benefit, is to free you from anger, because if you're constantly saying, may you be, may you suffer, may you be in pain, then changing that to say to yourself, may you be happy.

256
00:32:21,840 --> 00:32:30,840
It's a very good and useful tool. That's what meta is actually for. So if someone has anger and hatred towards others, it's a very useful tool.

257
00:32:30,840 --> 00:32:46,840
But it's not so useful for forgetting root of desire. So the way to have just meta, just friendliness, where you actually have a pure mind, is just that, to have a pure mind and to free yourself from desire.

258
00:32:46,840 --> 00:33:06,840
And the only way to do that, and this is what should really settle a lot of the discomfort surrounding this Buddhist proposition of not caring and not loving and not, and so on, is that it's objective investigation.

259
00:33:06,840 --> 00:33:35,840
I'm not proposing these things as a belief you have to have, or even something you have to understand. What we're talking about is perspectives, and we're describing a state of perfect clarity, where the mind actually knows what it's doing, and we're making kind of the claim that when you have that clarity, these kinds of states that we call love and caring and so on.

260
00:33:35,840 --> 00:33:48,840
They just can't arise. They just don't arise. Being bothered, being upset, and they don't arise in a clear state of mind. They don't, they don't, they don't, they don't compatible with it.

261
00:33:48,840 --> 00:34:01,840
And so when we cultivate mindfulness, we're actually doing ourselves a great service in terms of being more friendly, in terms of being better and kinder, nicer people to each other.

262
00:34:01,840 --> 00:34:14,840
Simply because we're doing away with our need for others to make us happy, we're doing away with a volatility that can taint any kind of friendliness we might have.

263
00:34:14,840 --> 00:34:23,840
Your friends with someone, but then they act in a certain way that you don't like because you've gotten attached to the way they are, you get angry at them, you get upset.

264
00:34:23,840 --> 00:34:36,840
Why? Because you need them to make you happy. When they suffer, then you get upset. So when people die, you get upset. So all of that is really, it does nothing to do with us being kind or nice to each other or even being happy.

265
00:34:36,840 --> 00:34:47,840
Just has to do with our, it's not even a good reason. It's not even a temporary happiness. It's not even something we're looking for like this will make me temporarily happy.

266
00:34:47,840 --> 00:34:57,840
It actually doesn't come from that. It's just blindness. It's a bad habit. When you look closely, it kind of dissolves.

267
00:34:57,840 --> 00:35:04,840
It become more content, more at peace. You don't need others to make you happy or concerned.

268
00:35:04,840 --> 00:35:14,840
When others suffer, you simply help when you can. Of course, because that's the right thing to do. It's just the natural pure way to act.

269
00:35:14,840 --> 00:35:24,840
It's also the way to fight happiness. Of course, helping others, being kind to each other, being kind to each other, leads to happiness. That's why you do it.

270
00:35:24,840 --> 00:35:36,840
Not because, sorry, not because it will make you happy when they are happy, but because the way of being, when you are kind to others, regardless of the outcome.

271
00:35:36,840 --> 00:35:45,840
That might be confusing because it sounds like I'm saying, you help others because once you've helped them, them being helped makes you happy.

272
00:35:45,840 --> 00:35:52,840
That's not the case. You helping them makes you happy. It gives you peace and strength really.

273
00:35:52,840 --> 00:35:59,840
It's a very pure state of mind to help someone. When you help someone with a pure mind, it doesn't matter what the result is.

274
00:35:59,840 --> 00:36:07,840
Of course, that's out of your hands. The reason why many aid workers become burnt out is because they get attached because they care.

275
00:36:07,840 --> 00:36:15,840
It might sound kind of cold or hard-hearted, but they aren't able to help in the end because they get burnt out.

276
00:36:15,840 --> 00:36:20,840
They don't actually make as much of a difference as they might if they were detached.

277
00:36:20,840 --> 00:36:39,840
As we say, I don't like to use that word because it sounds so dry and cold. Then it's not. Being not attached is being pure and content doesn't matter whether my actions bear fruit.

278
00:36:39,840 --> 00:36:46,840
I do them because they're good and the actions themselves are productive of happiness.

279
00:36:46,840 --> 00:36:54,840
The real answer, which I could have just said at the beginning, is be mindful of course.

280
00:36:54,840 --> 00:37:02,840
A lot of these questions have simple answers and simple answers are always trying to be more mindful.

281
00:37:02,840 --> 00:37:09,840
Cultivate mindfulness and clarity. When you see clearly, you let go of all the attachment.

282
00:37:09,840 --> 00:37:21,840
But it is important, as I said, things like clinging and craving are associated with things like delusion and ignorance.

283
00:37:21,840 --> 00:37:31,840
This concept of love is one that we have to accept or acknowledge is very much associated with delusion.

284
00:37:31,840 --> 00:37:40,840
With wrong view of the views relating to how good love is or that there is a good kind of love and caring is good and so on.

285
00:37:40,840 --> 00:37:50,840
That somehow you actually benefit or someone benefits from you getting upset when others suffer and that sort of thing.

286
00:37:50,840 --> 00:38:03,840
That's the answer to that question. Thank you for asking. I'll show all the best.

