1
00:00:00,000 --> 00:00:11,000
Hello YouTube. I've been giving talks lately on birthdays. It seems with some frequency.

2
00:00:11,000 --> 00:00:18,000
I've been invited around by people in the community to give talks on their birthdays.

3
00:00:18,000 --> 00:00:27,000
So it seems fitting that for my birthday tomorrow, May 9th, I should talk a little bit about birthdays.

4
00:00:27,000 --> 00:00:37,000
I've been trying to get it up on YouTube, but my recording of my talks is not very well coordinated.

5
00:00:37,000 --> 00:00:44,000
So the idea of celebrating your birthdays is of course curious from a Buddhist point of view,

6
00:00:44,000 --> 00:00:56,000
because it doesn't seem all that meaningful. It's actually just a convention based on the travels of the earth around the sun.

7
00:00:56,000 --> 00:01:07,000
To celebrate on this day every year. But to commemorate or to reflect upon your birthday does have some significance.

8
00:01:07,000 --> 00:01:16,000
I mean there's some value to it, because birth is important part of life.

9
00:01:16,000 --> 00:01:32,000
And so what I've been talking about lately just in brief is how remembering our birth reminds us of the importance of life or the significance of life.

10
00:01:32,000 --> 00:01:43,000
Just as death does. I've been talking about death as well recently. And birth and death are actually from Buddhist point of view, one in the same process.

11
00:01:43,000 --> 00:01:50,000
They're very closely connected. Death leads to birth. Why are you born because you die? Why do you die because you're born?

12
00:01:50,000 --> 00:01:54,000
And so they mark the beginning and end of a period that we call life.

13
00:01:54,000 --> 00:02:02,000
And so the significance of birth as with the significance of death is our life. How we live our lives.

14
00:02:02,000 --> 00:02:09,000
Our birth only has meaning based on the character of our lives. Same with death.

15
00:02:09,000 --> 00:02:18,000
And birth and death are two things that we don't have control over. You don't have control over when you're going to die or even how you're going to die.

16
00:02:18,000 --> 00:02:28,000
And birth as well is the same. When it happens it happens out of your control not subject to your desires.

17
00:02:28,000 --> 00:02:44,000
And yet life is that which allows us to choose or to alter our course. So depending on how you live your life.

18
00:02:44,000 --> 00:02:55,000
You'll be born and die in very different ways. Death will be different in your birth. Your rebirth will be different.

19
00:02:55,000 --> 00:03:10,000
It's a reminder. Death is a chance. Death and birth are chances to reflect upon the importance of life.

20
00:03:10,000 --> 00:03:18,000
And how are we living our lives? Do we know where we're going to be born? Do we have a good feeling about where we're headed?

21
00:03:18,000 --> 00:03:21,000
And we're confident that we're heading in the good direction.

22
00:03:21,000 --> 00:03:32,000
And so what I really wanted to talk about in this video was what I want from my birthday.

23
00:03:32,000 --> 00:03:41,000
And so tomorrow is my first day and I had originally thought of holding a meditation course. But as you can see I'm not in the monastery.

24
00:03:41,000 --> 00:03:53,000
I'm staying with my father. And so I thought instead why not do an online meditation session.

25
00:03:53,000 --> 00:04:01,000
And the other thing was doing a course I wasn't sure who would be able to attend or if anyone would be able to attend.

26
00:04:01,000 --> 00:04:09,000
But this way here's how I wanted to work. What I'd like is if you do meditation today and I hope you do.

27
00:04:09,000 --> 00:04:16,000
And if you want you can do it just for me on my birthday. Especially even if you're not a person who normally does meditation as a gift.

28
00:04:16,000 --> 00:04:25,000
What I want from my birthday is for you. Yes that means you. Even if you're someone who thinks oh he doesn't mean me because I'm not one of his meditators.

29
00:04:25,000 --> 00:04:37,000
Yes it means you. What I want from you is to do some meditation today. That's what I would like from everyone. Absolutely everyone no exception for my birthday.

30
00:04:37,000 --> 00:04:46,000
So what I want you to do once you've meditated. Put it as a comment to this video. How many minutes you meditate it.

31
00:04:46,000 --> 00:04:57,000
You do many times a day. Then each time one comment. Twenty minutes. Thirty minutes. One hour. Five minutes. You've only got time. Three minutes. One minute.

32
00:04:57,000 --> 00:05:09,000
At least it shows you care. And I think that would be wonderful. I think this would be a great way to encourage each other as well in the meditation.

33
00:05:09,000 --> 00:05:20,000
To see each other. Others comments. And I think this is potentially something we can do regularly. Where we say okay now we're doing a meditation.

34
00:05:20,000 --> 00:05:38,000
So we want to call it and everyone posts comments how many minutes they did. Everyone gets to see all these comments. So wait let's also put where you are. That's what location as well. If you would just want to put your country or put your city or

35
00:05:38,000 --> 00:05:51,000
street address would be going to far. But city and country would be preferred. That'd be great. Then we get to see how everyone around the world is. Okay so wishing you all a happy birthday.

36
00:05:51,000 --> 00:06:01,000
And remember that we're born in reality. We're born and die every day. We're born and die every moment. It's birth and death.

37
00:06:01,000 --> 00:06:10,000
As we understand them we're only concepts. The reality is that we're born and die with every experience which comes momentary.

38
00:06:10,000 --> 00:06:34,000
So wishing you all a happy day. And peace, happiness, freedom from suffering through the meditation practice. Be well.

