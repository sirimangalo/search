1
00:00:00,000 --> 00:00:10,000
Our involuntary movements of the body during meditation considered hindrance, even though mindfulness of these movements is present.

2
00:00:10,000 --> 00:00:14,000
I had this one recently.

3
00:00:14,000 --> 00:00:24,000
Ideally, you don't move in meditation. Ideally, you bear with it.

4
00:00:24,000 --> 00:00:31,000
Even when you have sharp pain, you put up with it. This is the ideal stage that we're trying to get to.

5
00:00:31,000 --> 00:00:39,000
But just like everything else, just like watching the stomach, it's a process and it takes time and it takes training.

6
00:00:39,000 --> 00:00:46,000
So regardless of this sort of teaching, it will seem impossible to the beginner meditator to do this.

7
00:00:46,000 --> 00:00:49,000
They'll find themselves moving quite often, actually.

8
00:00:49,000 --> 00:00:55,000
So this is where we say when you do move, just be mindful of it and you shouldn't feel bad about moving.

9
00:00:55,000 --> 00:01:01,000
But you should be clear that your goal is to eventually catch it so early on

10
00:01:01,000 --> 00:01:05,000
because what it means is you've caught it at the point where you're already upset about it

11
00:01:05,000 --> 00:01:11,000
and therefore are giving rise to the karma, the action of moving.

12
00:01:11,000 --> 00:01:14,000
When you catch it early enough, you don't even get upset about it.

13
00:01:14,000 --> 00:01:20,000
For instance, you have pain and you catch the pain where you have itching or so on

14
00:01:20,000 --> 00:01:29,000
and you catch the itching early on and eventually you're able to acknowledge it until it goes away rather than moving.

15
00:01:29,000 --> 00:01:38,000
So are the movements, the hindrance? No, you could look at them as more of a stage in your practice

16
00:01:38,000 --> 00:01:44,000
that you should strive to eventually rise above or not

17
00:01:44,000 --> 00:01:48,000
because of course if you're practicing once or twice a day

18
00:01:48,000 --> 00:01:52,000
and then going to work or going to school, you might not get to that position

19
00:01:52,000 --> 00:01:56,000
to that situation, at least not for a long time where you're able to sit with it.

20
00:01:56,000 --> 00:02:00,000
So you shouldn't be discouraged by the fact that you have to move

21
00:02:00,000 --> 00:02:03,000
and the correct answer is yes, to be mindful of the movements.

22
00:02:03,000 --> 00:02:09,000
If you move, say wanting to move, wanting to move, lifting, or as best you can,

23
00:02:09,000 --> 00:02:13,000
at least acknowledging the disliking and discomfort and so on,

24
00:02:13,000 --> 00:02:37,000
acknowledging what you can and being clear in your mind about it.

