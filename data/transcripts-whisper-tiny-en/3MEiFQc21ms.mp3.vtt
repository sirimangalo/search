WEBVTT

00:00.000 --> 00:07.920
How do Buddhists view military service?

00:07.920 --> 00:11.800
This is interesting because it actually relates to the monk question I think or the answer

00:11.800 --> 00:18.680
does because the word military service has come to mean so many different things, right?

00:18.680 --> 00:23.520
You don't have to necessarily kill people to be in the military.

00:23.520 --> 00:28.520
Military can stop floods, for example, right?

00:28.520 --> 00:33.560
If the military is put to use stopping floods, that's a great thing.

00:33.560 --> 00:37.760
You took away all the weapons from the military, took away all of their weapons and gave

00:37.760 --> 00:51.880
them shovels and what sandbags, then I think it would be a good thing.

00:51.880 --> 00:59.520
You have a standing army that looks after people's welfare.

00:59.520 --> 01:12.560
But no, obviously the military is for the purpose of doing battle, causing harm, hurting

01:12.560 --> 01:20.760
other beings, maybe in defense, but a person who undertakes who enters the military

01:20.760 --> 01:26.600
is doing it with the, I believe, with the understanding and the intention that they are

01:26.600 --> 01:37.280
going to bring violence to other beings, either, you know, and as I said, they can think

01:37.280 --> 01:48.480
of it as in defense, but it's a violent profession by its very nature.

01:48.480 --> 01:53.600
I don't know, I guess my question would be how, in what way would you think that military

01:53.600 --> 01:59.560
service would be a positive thing?

01:59.560 --> 02:07.200
Or is there any way that you can think of it as not being a negative thing, I suppose?

02:07.200 --> 02:20.800
Okay, my job would be to help children of foreign countries not hate Americans, but the

02:20.800 --> 02:25.200
question is, if there were a war, would you be asked to pick up a gun and fight?

02:25.200 --> 02:27.960
I mean, isn't, I don't really know how that's what I said.

02:27.960 --> 02:33.040
The military has become something quite a bit more than just guys with guns, but I still

02:33.040 --> 02:44.560
think you have, you're given a duty to fight, are you not?

02:44.560 --> 02:50.200
There are many issues here, because I know that you can, being in the American military,

02:50.200 --> 02:57.200
for example, can be a great way, theoretically, to help people.

02:57.200 --> 03:06.960
You're given a salary, you're given a lot of equipment and resources, and yeah, joining

03:06.960 --> 03:12.320
other organizations might be a great thing, but there's something to be said about the

03:12.320 --> 03:15.120
clout of the American military in that sense.

03:15.120 --> 03:20.560
The problem is that I don't understand it fully, but as I understand, when you join

03:20.560 --> 03:29.640
the military, no, I guess that's not true, no, if you're intelligence and in the Air Force,

03:29.640 --> 03:36.360
you're not involved with killing, more like a desk charmer, but then I guess there's

03:36.360 --> 03:43.320
the moral question, is there a moral question of joining an organization that at its

03:43.320 --> 03:55.640
very nature is for the purpose of, I don't want to say, the purpose is to engage in combat

03:55.640 --> 04:09.600
or to, well, it's a military, are there ethical, are there ethical problems there?

04:09.600 --> 04:16.280
I guess I'm, I'm not, I don't have strong feelings about this, you take your job, and

04:16.280 --> 04:23.160
if your job is for the purpose of helping other people, and if through your job, you can

04:23.160 --> 04:30.080
help other people, then power to you, even if your job doesn't help other people.

04:30.080 --> 04:37.800
I'm not of the bent that you have to be concerned with, with anything larger than doing

04:37.800 --> 04:43.120
your own task, and I know people are, and there's a lot of people who have this social

04:43.120 --> 04:49.200
conscience that they feel like they shouldn't work for an organization, this, this is

04:49.200 --> 04:53.600
the same, same debate about vegetarianism, right?

04:53.600 --> 04:59.760
You can't eat meat because you're contributing to the killing of animals, you can't use

04:59.760 --> 05:07.840
Gillette razors because you're contributing to the cruelty towards animals.

05:07.840 --> 05:13.800
You can't buy certain stocks, you can buy certain products, you can't, you know, don't

05:13.800 --> 05:21.000
buy Nike, don't buy this, don't buy that, these sort of things, and I don't buy it,

05:21.000 --> 05:27.920
it seems to put it in buy it, and there's even, we have this wonderful example that we

05:27.920 --> 05:32.840
always tell, this woman, whose husband was a hunter.

05:32.840 --> 05:36.880
When she was young, she went to practice meditation, listened to the Buddha's teaching,

05:36.880 --> 05:46.200
and became a sotapana, which means she was, she was in a sense enlightened, and then

05:46.200 --> 05:50.720
she got married because in India they often, their marriage would have been decided for

05:50.720 --> 05:59.400
them, so she was married off to this man, who was a hunter, and every day she would

05:59.400 --> 06:04.680
clean his traps, clean the blood and the guts and whatever, the hair off of these traps,

06:04.680 --> 06:11.120
and put them out on the table for him, and every day he would go out and kill animals.

06:11.120 --> 06:16.280
And so they asked, how is this possible that she could do this?

06:16.280 --> 06:22.440
And the Buddha said it's because he said it's like if your hand has no cut, if you have

06:22.440 --> 06:28.920
no wound on your hand, you can handle poison, but if you have a wound on your hand, you

06:28.920 --> 06:40.320
can't handle poison, he says a little more poetically than that, but the point being

06:40.320 --> 06:50.200
a person has no bad intentions, the act of cleaning, cleaning blood and gore off of these

06:50.200 --> 06:57.120
killing machines, even to that extent it's not considered to be immoral, because it's

06:57.120 --> 07:03.080
not, it's the same as eating meat, the act itself is never immoral, this is the Buddha

07:03.080 --> 07:08.160
actually taught against the idea of karma, he said karma is not the important thing,

07:08.160 --> 07:12.960
it's the intention, so if the woman is intending, oh here I'm helping my husband making

07:12.960 --> 07:19.560
him happy and I'm doing my job, you know if I don't he might beat me kind of thing,

07:19.560 --> 07:26.520
that might have had a part in it, because there's some horrible things to go on out there,

07:26.520 --> 07:33.040
but even, you know, he's a hunter, he's not the most cultured of beings, but even without

07:33.040 --> 07:42.320
that, just the idea that she's doing her job and doing what is her way, and for a person

07:42.320 --> 07:51.080
who eats meat, they're just eating the food in order to gain the benefit, this is in

07:51.080 --> 07:56.480
line I think with, this is certainly in line with my understanding of the Buddha's teaching,

07:56.480 --> 08:10.680
because we're a deal in terms of experience, don't some monks practice some military aspects

08:10.680 --> 08:15.960
like shelving, I'll never live this one down, no monks, Buddhist monks don't practice

08:15.960 --> 08:22.440
shelving, shelving monks practice shelving, I'm sorry, they call themselves Buddhist, but

08:22.440 --> 08:30.640
in wood way does Kung Fu have to do it, the Buddha's teaching, it's well to each their

08:30.640 --> 08:39.600
own, but no, shelving monks are a very specific type of monk, and I don't know, I guess

08:39.600 --> 08:43.240
I just can't answer that, because it's so different from what I do, I used to practice

08:43.240 --> 08:51.680
karate, but that was a long time ago, karate isn't kung fu, but it's not a part of the

08:51.680 --> 08:57.320
Buddha's teaching for sure, those monks are doing it, I mean there's a whole history there

08:57.320 --> 09:02.240
of how it came about because there was war and the monks had to defend themselves or whatever,

09:02.240 --> 09:09.560
not sure how it all came about, anyway, question on joining the military, I say, as I said,

09:09.560 --> 09:13.920
take it as your job, because the American military is such a huge organization and so many

09:13.920 --> 09:20.240
people aren't involved with the killing part of it, but you have to be fairly careful

09:20.240 --> 09:33.440
there as well because it's, if you are put in a position where you have to be involved

09:33.440 --> 09:43.200
with or encourage the use of force or the distribution of weapons, for example, it's

09:43.200 --> 09:48.240
certainly something that you have to take into consideration, be careful that you don't somehow

09:48.240 --> 09:55.840
get conscripted and your soldier going fight, kind of thing, but you know, we're living in hard

09:55.840 --> 10:01.680
times if you have a chance to help people then power to you, go out there and help, because

10:01.680 --> 10:08.160
actually in Canada the military is mostly involved in peacekeeping, helping to bring peace or

10:08.160 --> 10:24.080
they were, I don't know what it's like, anyway, military.

