It's becoming a sort of something spontaneous or is it gradual?
Can one be a sort of one and not be aware of it or have some sort of doubt about it?
Those are good questions.
Well, it's both really, you know, becoming a sort of pun is one moment.
You're not kind of a sort of pun. You're not partially a sort of pun.
You either are or you aren't. It's a categorical difference.
So the pun is someone who's seen the pun as someone who's seen the pun as someone who's not.
That's means it's spontaneous or instantaneous.
But it's not spontaneous in the sense that it is gradual practice.
You have to cultivate awareness to a point where the mind is able to break away from
some cars and reach the Asankata, the uninformed, which is the environment.
The second question is more difficult because I don't want to say the wrong thing and misrepresent the truth.
But to me, you know, so this is how am I going to be the arbiter of this question, right?
This is a question you have to ask the Buddha.
A sort of pun that doesn't have doubt.
But my understanding and I think the commentary is understanding is that,
and I may be given that the Pityka or Buddha's teaching, what we have in the Buddha's teaching,
is understanding that that refers to specifically doubt in the Buddha, doubt in the Dhamma and doubt in the Sangha.
Thus, the Buddha is enlightened.
His teachings are the path to freedom from suffering.
And a person who practices these teachings, a person who is enlightened is the person who has practiced these teachings.
That's the doubt.
That's the confidence of a Sotapana that they have no doubt in these three things.
It doesn't say that they have no doubt in themselves.
And I think that's reasonable.
I don't want to give a categorical answer here.
But to me, that seems reasonable because Sotapana is just a word.
And this happens all the time in meditation.
You get this idea that it's something, that it's an entity.
Am I there yet? You're waiting for the signpost.
And so you're looking, how do you know if you're a Sotapana?
I had some experience that makes me think that I'm a Sotapana, but am I a Sotapana?
So we don't tend to answer people's questions when they ask about how do you know your Sotapana and something.
Do you have greed? Do you have anger? Do you have delusion?
Well, then there's still further to go. That's all you should know.
Because the only thing that it would do if you did know that you're a Sotapana is,
it would give you the reassurance that would maybe make you stop practicing or go naked lazy.
If you are a Sotapana and you have doubt about it, you're stuck in a work really hard
to push on to cut off more defilements.
But I would say, yes, a Sotapana can have doubt.
My guess is, and I'm not sure that it's correct, a Sotapana can have doubt.
Because it's not, it's not a sure thing.
I would say an are a hunt.
I'd say I don't want to make these categorical statements.
I think it's reasonable soup to suggest that a Sotapana could have doubt.
And I think it's reasonable to suggest that an are a hunt may not have doubt.
Because the are a hunt is free from delusion.
The are a hunt is in my mind more likely of the two to be free from doubt in themselves.
Because in the Buddha said, we see dang Ramacharyan, katang kananyam, nati dani, punabhuoti, nati dani, tarat, pajanati.
He knows for himself that there is nothing further.
This is an are a hunt.
The are a hot nose for themselves.
I would say a Sotapana because they still have greed, anger and delusion, they might still have this confusion inside of themselves.
I must still have doubt.
The doubt that is disappeared is in the Buddha Damasanga.
So whether they know that they themselves have reached it, that just be confused and doubt about themselves.
But when they look deep down, they will be able to say that they have no doubt in the Buddha Damasanga.
