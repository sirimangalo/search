1
00:00:00,000 --> 00:00:05,120
I was watching your video on exercise and you said that monks only eat once in

2
00:00:05,120 --> 00:00:09,240
the morning. I was just wondering how one survives on that small amount of

3
00:00:09,240 --> 00:00:13,640
food. I was sort of shocked when you said it. Do you recommend people who

4
00:00:13,640 --> 00:00:22,360
meditate to take this on? Absolutely. I think I just answered this in the last

5
00:00:22,360 --> 00:00:31,040
question, but you can survive with one meal a day. You clearly can. You can't do

6
00:00:31,040 --> 00:00:39,160
much with it. You can't go out and lift weights or run marathons or lift bricks,

7
00:00:39,160 --> 00:00:42,960
or what was they doing? Carrying computers, I carried the carry all this

8
00:00:42,960 --> 00:00:53,120
computer from the room to here and that was not the most fun. But well I'd

9
00:00:53,120 --> 00:00:58,120
like to actually turn it over to a living example because this aesthetic on my

10
00:00:58,120 --> 00:01:03,160
right is now eating. Today he ate one meal and you can see he's still alive.

11
00:01:03,160 --> 00:01:17,880
Let's ask about he feels. I feel good. Like I said before, like you just said,

12
00:01:17,880 --> 00:01:22,720
it's going to depend on what you're doing, but for just being here and

13
00:01:22,720 --> 00:01:28,120
meditating and studying it's not that you're exerting this tremendous

14
00:01:28,120 --> 00:01:36,000
effort that's just pushing your body or anything. One meal is really enough and I

15
00:01:36,000 --> 00:01:43,040
found that eating more than you just leave yourself open to overeating and then

16
00:01:43,040 --> 00:01:56,960
you ovary and then you feel drowsy and we use energy really. So yeah, one meal, one

17
00:01:56,960 --> 00:02:04,280
meal is good. It keeps you going. I don't really even feel hungry. It's like I

18
00:02:04,280 --> 00:02:11,280
found like you're saying it's just your mind place tricks on you. It tells you

19
00:02:11,280 --> 00:02:18,200
that you need this or that, but you get used to it and now it's it's not really

20
00:02:18,200 --> 00:02:25,040
even the problem. The whole time I've been out here. At first it was harder. At first

21
00:02:25,040 --> 00:02:30,680
you're seeing two meals later and now you'll be like, oh man, I'm so hungry. I

22
00:02:30,680 --> 00:02:36,800
want this. I want that. But then you get used to it and you adjust to it and then

23
00:02:36,800 --> 00:02:43,480
it's fine. Then it becomes the normal thing then. And then there's no more

24
00:02:43,480 --> 00:02:58,960
problem. I remember that I on my first retreat with Banta Yutaramo. I was one of

25
00:02:58,960 --> 00:03:06,680
those people who complained about hunger in the evening. And I really learned

26
00:03:06,680 --> 00:03:14,680
in the first two weeks that it's not really necessary to have more than two

27
00:03:14,680 --> 00:03:24,280
meals per day. And after I think three or four weeks being on retreat, I want

28
00:03:24,280 --> 00:03:33,200
to try out how that is, how that feels to to have only one meal per day. And I

29
00:03:33,200 --> 00:03:41,440
was so surprised that it was so easy and it worked so well. And out of that

30
00:03:41,440 --> 00:03:47,600
experience, I can say two things. It works. It's possible when you meditate a lot.

31
00:03:47,600 --> 00:03:56,320
And if you go on retreat with Banta Yutaramo, he will not force you right away

32
00:03:56,320 --> 00:04:05,680
from the first day on to do that. So if you if you can do that's wonderful. And

33
00:04:05,680 --> 00:04:14,040
if not, that's that's fine as well. We will never force you to eat only one

34
00:04:14,040 --> 00:04:18,840
meal a day. What I say, what I tell the people here, I'm so happy to be here

35
00:04:18,840 --> 00:04:22,800
now because we're so free. We don't have to follow anybody else's schedule. So I

36
00:04:22,800 --> 00:04:28,000
say, okay, we get food in the morning. It's there. And you can take as much as

37
00:04:28,000 --> 00:04:34,440
you want as long as you eat it between dawn and noon. And you can eat this. I'm

38
00:04:34,440 --> 00:04:37,880
kind of push people not to have more than two meals, but I'll say, you know, eat

39
00:04:37,880 --> 00:04:45,520
it as you like. We're not actually allowed to to to tell our students to

40
00:04:45,520 --> 00:04:51,880
require our students to to eat only one time. What we're supposed to do

41
00:04:51,880 --> 00:04:56,200
according to the Buddha is you'd only one set of food. So we get our food for the

42
00:04:56,200 --> 00:05:00,560
day. And then we split it into two parts. If we choose, you can eat it all

43
00:05:00,560 --> 00:05:06,280
then and have one sitting. But this is considered to be the extreme end of what

44
00:05:06,280 --> 00:05:09,800
is allowed. So you know, many people do it. And it's not extreme by any means.

45
00:05:09,800 --> 00:05:14,880
It's it's quite normal and quite natural. Or you can split it into two parts.

46
00:05:14,880 --> 00:05:17,880
Eat some now and some later. But you're still considered to be eating one meal.

47
00:05:17,880 --> 00:05:22,880
You've got one set of food. The point being it it it it reduces the amount of

48
00:05:22,880 --> 00:05:27,360
of greed and attachment that's going to arise. As opposed to going out and

49
00:05:27,360 --> 00:05:30,120
trying to find a second meal, something different. You know, from the morning

50
00:05:30,120 --> 00:05:34,920
I had cereal and the oven for lunch, I want to pizza or something.

51
00:05:34,920 --> 00:05:39,280
It's torture. It's just talking about it.

52
00:05:39,280 --> 00:05:46,640
And that is easier not having more food later on because not not having in

53
00:05:46,640 --> 00:05:50,720
front of you, not having to think about it. And that's the last thing I was

54
00:05:50,720 --> 00:05:55,160
going to say is that eating one meal a day is incredible in so many ways.

55
00:05:55,160 --> 00:06:00,040
As as Naga saying that says, not thinking about it again, you've had one meal.

56
00:06:00,040 --> 00:06:03,520
That's it. That's food. Food done. No more food for the rest of the day.

57
00:06:03,520 --> 00:06:08,360
It's it's a non issue at that point. You know, the food. And when you do it

58
00:06:08,360 --> 00:06:12,240
again and again, if you do that every day, you find that food is totally a

59
00:06:12,240 --> 00:06:17,360
non issue. It's like, oh, now food time being finished. And that was the 24

60
00:06:17,360 --> 00:06:23,400
hour cycle. It becomes quite easy to to that to let go of food when you eat

61
00:06:23,400 --> 00:06:30,240
only one meal. In fact, two meals, two full meals is a lot more difficult.

62
00:06:30,240 --> 00:06:33,640
You you will find that in the evening, you're expecting the third one. When you

63
00:06:33,640 --> 00:06:39,000
only eat once, it is so reduced that the mind has no choice but to let go

64
00:06:39,000 --> 00:06:44,880
clinging and wanting food for the rest of the day would be horrible. So

65
00:06:44,880 --> 00:06:47,400
that, you know, the mind it's so extreme for the mind. The mind's just like,

66
00:06:47,400 --> 00:06:51,440
what am I going to do? And it decides to let go. And when it lets go, then you

67
00:06:51,440 --> 00:06:57,560
feel great. The other thing, the other obvious benefit, not obvious, but this

68
00:06:57,560 --> 00:07:02,240
obvious once you clearly, clearly, seeable when once you start doing it, is

69
00:07:02,240 --> 00:07:06,360
that you're free for the rest of the day. You know, how much trouble monks have

70
00:07:06,360 --> 00:07:12,920
these days trying to find food at 11 o'clock or sometimes 11 30 and looking at

71
00:07:12,920 --> 00:07:18,200
the clock and saying, oh, it's almost 12 o'clock. This 11 o'clock meal has

72
00:07:18,200 --> 00:07:24,760
really tortured us time and time again. Sometimes, you know, you had your

73
00:07:24,760 --> 00:07:27,440
breakfast and then someone says, hey, you want to go somewhere? Let's do this.

74
00:07:27,440 --> 00:07:33,160
Do that. Oh, you know, what about lunch? What do we do for lunch? And you've

75
00:07:33,160 --> 00:07:36,280
just lost half the day because you have to wait until at least noon before

76
00:07:36,280 --> 00:07:41,440
you can go anywhere. That's what I find now when I eat at 11 o'clock. And it's

77
00:07:41,440 --> 00:07:46,320
like, we have to go somewhere. When will we go? Probably have to go after lunch.

78
00:07:46,320 --> 00:07:50,040
So you've lost half the day. When you're eating only one meal a day, it's like,

79
00:07:50,040 --> 00:07:54,280
eight o'clock, we're out here. Well, what about lunch? I ate already. That's what

80
00:07:54,280 --> 00:08:02,200
we're going to try to do on Tuesday. Anyway, one meal is, we should strive for

81
00:08:02,200 --> 00:08:07,400
it. The one thing is that when monks ate one meal, the Buddha would still

82
00:08:07,400 --> 00:08:14,680
encourage or allow them or express approval of them eating some kind of rice

83
00:08:14,680 --> 00:08:23,800
soup in the morning or some just boiled rice and water to make a rice soup because

84
00:08:23,800 --> 00:08:26,920
he said that has several benefits. And it's a great, it's a great

85
00:08:26,920 --> 00:08:33,560
substitute because it gives you that little bit of energy. The problem is

86
00:08:33,560 --> 00:08:36,680
you eat after going on an arm's drum. So you've got to go on arms round on an empty

87
00:08:36,680 --> 00:08:40,600
stomach and that can be difficult sometimes. So it's good before that to have

88
00:08:40,600 --> 00:08:46,480
some little bit. And then after you come back to eat like when we were at

89
00:08:46,480 --> 00:08:52,200
Wat Tambatang and before I'm sure you would bring me the sauerkraut. She wasn't

90
00:08:52,200 --> 00:08:57,440
as she was a meat and not even a meaty, I guess, at that point. I was a meaty.

91
00:08:57,440 --> 00:09:06,080
And you would be able to keep the boxes, the tetrapax of soy milk. So six o'clock

92
00:09:06,080 --> 00:09:12,400
even have one I'd have one and then I'd go on arms round. And totally unrelated to

93
00:09:12,400 --> 00:09:16,680
the question. But that one time I was just thinking about it today when I had to

94
00:09:16,680 --> 00:09:19,880
go on arms round. I was thinking about the time when I had to go on arms

95
00:09:19,880 --> 00:09:24,760
round half dead. And I was like teetering like because there's what

96
00:09:24,760 --> 00:09:28,880
Tambatang is like is what three and a half kilometers from the village. So I

97
00:09:28,880 --> 00:09:32,880
had to walk three and a half kilometers with a fever. I could barely see

98
00:09:32,880 --> 00:09:37,200
in front of me and I was like swaying back and forth on the road and just

99
00:09:37,200 --> 00:09:40,600
like blurry looking at the road. Made it all the way to the village and just

100
00:09:40,600 --> 00:09:44,400
sat down. I couldn't even get into the village. It's like didn't even have the

101
00:09:44,400 --> 00:09:49,360
energy to go around from house to house. So I just sat there until someone

102
00:09:49,360 --> 00:09:52,800
brought me some water and got me some food and brought me back to the

103
00:09:52,800 --> 00:10:21,040
monastery. Totally unrelated to the thought.

