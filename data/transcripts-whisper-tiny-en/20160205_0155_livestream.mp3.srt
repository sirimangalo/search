1
00:00:00,000 --> 00:00:29,000
So, good evening everyone from broadcasting live February 4th, 2016.

2
00:00:29,000 --> 00:00:38,000
Today's quote is interesting.

3
00:00:38,000 --> 00:00:44,000
I had to read it a few times before I actually got a sense of what it means.

4
00:00:44,000 --> 00:00:47,000
Still, I don't have it completely.

5
00:00:47,000 --> 00:00:53,000
Translations a little bit weird.

6
00:00:53,000 --> 00:01:00,000
So, let's break it up concerning those internal things.

7
00:01:00,000 --> 00:01:12,000
I'm not convinced that's a very good translation.

8
00:01:12,000 --> 00:01:28,000
I don't really understand it.

9
00:01:28,000 --> 00:01:38,000
I don't think it's a part of the actual text.

10
00:01:38,000 --> 00:01:42,000
It's much simpler than it's actually written.

11
00:01:42,000 --> 00:01:56,000
So, say kassa beke beke uno, up, but dema de sassa, ma de sassa.

12
00:01:56,000 --> 00:02:00,000
Say kassa, it's talking about a say kind of a learner.

13
00:02:00,000 --> 00:02:04,000
One who is still in training.

14
00:02:04,000 --> 00:02:09,000
So, there are two kinds of monks.

15
00:02:09,000 --> 00:02:14,000
There are two kinds of Buddhists.

16
00:02:14,000 --> 00:02:17,000
Say kassa and a say kassa.

17
00:02:17,000 --> 00:02:24,000
Actually, I guess it's two kinds of enlightened beings, really.

18
00:02:24,000 --> 00:02:29,000
Say kassa is someone who still is in training.

19
00:02:29,000 --> 00:02:36,000
Say kate, one who trains, say kassa, one who is in training.

20
00:02:36,000 --> 00:02:43,000
Say kassa is one who trains.

21
00:02:43,000 --> 00:02:48,000
So, it's just a standard description of someone who's still striving.

22
00:02:48,000 --> 00:03:07,000
And then we have a put dema de sassa.

23
00:03:07,000 --> 00:03:17,000
One whose mind has not attained supreme freedom from yoga.

24
00:03:17,000 --> 00:03:19,000
One who doesn't practice yoga.

25
00:03:19,000 --> 00:03:21,000
No, supreme freedom from yoga.

26
00:03:21,000 --> 00:03:24,000
Yoga is a bind, a bond.

27
00:03:24,000 --> 00:03:27,000
Something that keeps you tired.

28
00:03:27,000 --> 00:03:36,000
Something that keeps you stuck.

29
00:03:36,000 --> 00:03:42,000
So, to attain supreme freedom from bondage.

30
00:03:42,000 --> 00:03:55,000
Then there's this funny thing about internal things.

31
00:03:55,000 --> 00:04:03,000
I read it as one who dwells.

32
00:04:03,000 --> 00:04:14,000
And then something about having made who dwells, having made internal that which is inside.

33
00:04:14,000 --> 00:04:19,000
I don't get it, but probably it's not good enough.

34
00:04:19,000 --> 00:04:22,000
I think it means focusing on internal things.

35
00:04:22,000 --> 00:04:26,000
One who dwells, focusing on internal things.

36
00:04:26,000 --> 00:04:28,000
And then it just says there's one thing.

37
00:04:28,000 --> 00:04:32,000
There's no other thing that is more useful than.

38
00:04:32,000 --> 00:04:39,000
You only saw Manasika again, a very common Buddhist term that you almost don't want to translate,

39
00:04:39,000 --> 00:04:44,000
but he translates as giving close attention.

40
00:04:44,000 --> 00:04:47,000
Giving close attention to the mind.

41
00:04:47,000 --> 00:04:50,000
Not quite how I would put it.

42
00:04:50,000 --> 00:05:02,000
You only saw, you only saw Manas, you only actually means womb, the source or the womb.

43
00:05:02,000 --> 00:05:18,000
So what it means is keeping things in mind, keeping the sources of things in mind or getting to the source of things.

44
00:05:18,000 --> 00:05:21,000
Analyzing things from a point of view of their source.

45
00:05:21,000 --> 00:05:23,000
So it can be used intellectually.

46
00:05:23,000 --> 00:05:25,000
It's usually not.

47
00:05:25,000 --> 00:05:30,000
It's usually used in terms of describing meditation.

48
00:05:30,000 --> 00:05:34,000
It's a really good description.

49
00:05:34,000 --> 00:05:37,000
Because insight meditation is all about getting to the source of things.

50
00:05:37,000 --> 00:05:43,000
This is why we have this word that we use to remind ourselves.

51
00:05:43,000 --> 00:05:52,000
To get to the your need, to get to the source.

52
00:05:52,000 --> 00:06:01,000
Because normally when we experience things, we create so much more out of them.

53
00:06:01,000 --> 00:06:06,000
That's all on a conceptual and abstract plane.

54
00:06:06,000 --> 00:06:16,000
And we get stuck on this abstract plane that ends up being quite removed from reality.

55
00:06:16,000 --> 00:06:20,000
It's removed from the source.

56
00:06:20,000 --> 00:06:28,000
So you only saw Manasik Gharas about returning to the source of things.

57
00:06:28,000 --> 00:06:37,000
Now, the Nani Mitah Gharinan will be handed in a Gharin.

58
00:06:37,000 --> 00:06:40,000
Not grasping at the particulars, not grasping at the signs.

59
00:06:40,000 --> 00:06:43,000
Nimitah is the sign.

60
00:06:43,000 --> 00:06:45,000
So when you see a flower.

61
00:06:45,000 --> 00:06:51,000
A sign of the flower is what makes you think it's a flower.

62
00:06:51,000 --> 00:06:58,340
When you hear a car, the sign of the car is what makes you think of as a car.

63
00:06:58,340 --> 00:07:07,160
The sign, and on the engine are the different characteristics, is it good or bad or beautiful

64
00:07:07,160 --> 00:07:10,840
or ugly or sweet or sour or so.

65
00:07:10,840 --> 00:07:15,520
So the characteristics that we don't cling to, we get to the source.

66
00:07:15,520 --> 00:07:19,720
When you hear a car, the truth of it is the sound.

67
00:07:19,720 --> 00:07:24,720
The source of it is the sound.

68
00:07:24,720 --> 00:07:38,800
So the Buddha says this is the one thing that allows us, if there's nothing, if you take

69
00:07:38,800 --> 00:07:45,880
nothing else, just have yoni soma nasi kara, and there's a nice verse that comes after yoni

70
00:07:45,880 --> 00:07:53,840
so yoni soma nasi kara, damo se casa bikkuno.

71
00:07:53,840 --> 00:08:02,240
Yoni soma nasi kara is the dama for bikkun in training.

72
00:08:02,240 --> 00:08:11,600
The tanyo a vang bhukarum, kutamatasa patiya.

73
00:08:11,600 --> 00:08:27,720
Not tanyo, there is no other dama that is of such great benefit that does so much as this

74
00:08:27,720 --> 00:08:32,920
or the attainment of the highest benefit of the highest goal.

75
00:08:32,920 --> 00:08:42,360
Yoni soma nasi kara bikkuno is for the source, very clear instruction, get to the source

76
00:08:42,360 --> 00:08:43,360
of things.

77
00:08:43,360 --> 00:08:48,100
It doesn't mean go back to their causes in the pastor's son, get to the source of the

78
00:08:48,100 --> 00:08:59,000
present, like when you say to yourself, pain, pain, be with the source of the pain, hearing,

79
00:08:59,000 --> 00:09:03,380
you're at the real, true source of the experience.

80
00:09:03,380 --> 00:09:18,940
Yang dukasa papuni, the destruction or the end of duka, the end of suffering, papuni, the

81
00:09:18,940 --> 00:09:31,380
reach, the reaches, papuni, not sure about them, papuni means it means attain, I just

82
00:09:31,380 --> 00:09:37,100
don't know the tense, I think it means he reach, yeah, here we are, pot, oh it's will should

83
00:09:37,100 --> 00:09:51,740
reach, would reach, papuni is potential, so it's simple but very powerful teaching, good

84
00:09:51,740 --> 00:09:59,940
dad do, good to explain it, to be clear about what it means, as they quote the transitions

85
00:09:59,940 --> 00:10:11,660
it's not ideal, it's not close attention to the mind, it's not the mind, it's the objects

86
00:10:11,660 --> 00:10:19,500
that the mind keeps close attention to, the mind not just close attention but attention

87
00:10:19,500 --> 00:10:32,060
to the source, yoni, so, yoni, so, but the hung be cool, a be cool who strives for the

88
00:10:32,060 --> 00:10:44,060
source, or sticks to the source, doesn't get caught up in extrapolation or judgment

89
00:10:44,060 --> 00:10:53,340
or reaction, it's amazing how it changes your outlook, just teaching students at the

90
00:10:53,340 --> 00:10:58,420
university tomorrow again, they'll be teaching me, just give people a five minute meditation

91
00:10:58,420 --> 00:11:05,300
lesson, you can show them something that they've never seen before, open a door that they've

92
00:11:05,300 --> 00:11:19,340
never even knew was there, I've been teaching hour long sessions and people say they never

93
00:11:19,340 --> 00:11:26,860
realized their mind was so chaotic or so messed up through not meditating, teach a course

94
00:11:26,860 --> 00:11:32,380
on the internet, I've been teaching courses on the internet and people who I don't

95
00:11:32,380 --> 00:11:36,340
know say that people who I've never met in person say they've changed their lives

96
00:11:36,340 --> 00:11:45,780
or meditation, and now we have three meditators here who are really on the sake of path,

97
00:11:45,780 --> 00:11:53,700
here at our house who are doing real intensive meditation, practicing many hours a day,

98
00:11:53,700 --> 00:12:02,460
all of this is the good work, the best part of Buddhism, best part of the Buddhist teaching,

99
00:12:02,460 --> 00:12:07,380
it's called doing the Buddhist work, like another religion, they have the Lord's work

100
00:12:07,380 --> 00:12:31,860
or so on, they have the Buddhist work, so that's our dumb drop for today, thank you all

101
00:12:31,860 --> 00:12:38,860
for tuning in, wishing you all the best practice, thank you for tuning in, wishing you

