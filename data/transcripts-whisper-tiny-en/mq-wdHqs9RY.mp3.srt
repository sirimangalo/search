1
00:00:00,000 --> 00:00:07,920
Good evening. Welcome back to our study of the Dhamupada. Today we continue on with

2
00:00:07,920 --> 00:00:27,160
verse number 78, which reads as follows.

3
00:00:27,160 --> 00:00:40,120
Don't one should not associate with an evil friend, one should not associate with a low person,

4
00:00:40,120 --> 00:00:49,240
one should associate with Kalyana Mita, a beautiful friend, one should associate with

5
00:00:49,240 --> 00:01:02,400
the Pulisutama, Utama's highest with the highest person. This verse we're told was taught

6
00:01:02,400 --> 00:01:11,160
in regards to chana. Chana is one of the more famous figures in the Buddha's story. Every

7
00:01:11,160 --> 00:01:19,840
story that tells every story that you hear about how our great and glorious and wonderful

8
00:01:19,840 --> 00:01:33,120
and awesome leader became a Buddha includes chana. Have you ever seen a little Buddha with

9
00:01:33,120 --> 00:01:43,680
Keanu Reeves? It has chana. Why are those men? Why is that men lying there? What is wrong

10
00:01:43,680 --> 00:01:52,280
with those people? They're sick my Lord. Chana was there with him from childhood. Chana

11
00:01:52,280 --> 00:02:01,080
was there when he saw the four sites, the four great sites. He saw an old person, a sick person,

12
00:02:01,080 --> 00:02:10,400
a dead person and then he saw a recluse. Who is that man? He is a recluse. He has gone forth

13
00:02:10,400 --> 00:02:16,880
from the home life in order to find freedom from suffering, freedom from death. That is

14
00:02:16,880 --> 00:02:32,520
what I must do. They've watched a little Buddha or what's another story. They've all got

15
00:02:32,520 --> 00:02:41,680
this story and then Chana went with him when he left, but then he told Chana to turn back

16
00:02:41,680 --> 00:02:51,760
and Chana would turn back and brought his, he said to Chana, you must take, take Kanpaka,

17
00:02:51,760 --> 00:02:59,440
his horse and my jewels. So all of his royal jewels and earrings and whatever he crown,

18
00:02:59,440 --> 00:03:04,320
whatever he was wearing, probably not a crown, but whatever jewels he was wearing bring

19
00:03:04,320 --> 00:03:09,720
these back my royal accoutrements because they have to go back to my family. They don't

20
00:03:09,720 --> 00:03:19,080
want them to get lost. And so Chana went a little way to help him not mixing this up with

21
00:03:19,080 --> 00:03:26,360
somebody else's story. Chana went back, but Kanpaka died of a broken heart, his horse,

22
00:03:26,360 --> 00:03:31,240
his horse who had been born with him, the horse was born on the same day as the Bodhisatta.

23
00:03:31,240 --> 00:03:37,040
I think Chana was as well actually. There are five things as part of one of the exams

24
00:03:37,040 --> 00:03:42,080
we have to take in the first Dhamma exams. They're like five things that were born on the

25
00:03:42,080 --> 00:03:52,880
same day as the Bodhisatta. The tree, the Bodhi tree was planted on the same day as the Bodhisatta

26
00:03:52,880 --> 00:04:05,200
was born. I think Yisodara, his wife to be, maybe Ananda and Kanpaka, the horse and I don't

27
00:04:05,200 --> 00:04:10,880
know, maybe Chana, I'm grasping, I don't quite remember. I know the horse and the Bodhi tree for sure.

28
00:04:11,920 --> 00:04:16,800
When the horse died of a broken heart, because he thought that it would never see

29
00:04:18,320 --> 00:04:25,920
its master again. And it was reborn. Kanpaka was reborn as an angel, I believe, and there's some

30
00:04:25,920 --> 00:04:34,880
story about him that I can't remember either. But Chana went away and he thought, I can't bring

31
00:04:34,880 --> 00:04:40,560
these jewels back. If I go back without the Bodhisatta, without the prince, they're going to think

32
00:04:40,560 --> 00:04:50,400
I did something to him. They're going to accuse me. And so he hung the jewels up on a tree and went

33
00:04:50,400 --> 00:04:56,800
and became an ascetic himself, lived in the forest. I think that's the story. It's easy to get these

34
00:04:56,800 --> 00:05:02,400
mixed up, I don't pay too much attention, but I think that's Chana's story. How he came to become

35
00:05:02,400 --> 00:05:07,600
a monk, I can't remember that, eventually he came around to become a bikhu. So he ordained under the

36
00:05:07,600 --> 00:05:16,320
Buddha. And I imagine by that time, Sariput and Mogulana were already the two cheap disciples,

37
00:05:16,960 --> 00:05:23,120
and Chana, as close again, we have one of these stories of being close to perfection

38
00:05:24,080 --> 00:05:29,760
and having no part in it. So he was so close to the Buddha, the perfect and perfectly enlightened

39
00:05:29,760 --> 00:05:37,600
Buddha. And what did he do? He went around mocking the two cheap disciples and saying,

40
00:05:37,600 --> 00:05:47,040
I was there with him from the very beginning and I never ever lost track of the Buddha. I was there

41
00:05:47,040 --> 00:05:53,200
when he left home. I was there when he saw the four sites. But these two, these two go around saying,

42
00:05:53,200 --> 00:05:59,680
I'm the chief disciple. It actually says that it's like he marks them. I'm the chief disciple.

43
00:06:03,120 --> 00:06:07,840
What can you imagine, right? First out to say that about Sariput and Mogulana, and he would say

44
00:06:07,840 --> 00:06:20,240
this out loud to people. They go around thinking they're all that. And word got back to the

45
00:06:20,240 --> 00:06:27,680
Buddha, of course. And the Buddha called Chana up and asked him if this was true. And Chana kind

46
00:06:29,280 --> 00:06:34,960
got shamed and quieted down for a while. Stop saying such terrible things.

47
00:06:38,240 --> 00:06:43,840
But then he started up again. And he started up again and the Buddha called him back. He started

48
00:06:43,840 --> 00:06:47,200
up again and the Buddha called him back the third time. And the third time the Buddha said, look,

49
00:06:47,200 --> 00:06:54,960
Chana, Sariput and Mogulana could be your best friends. They could help you so, so much. They

50
00:06:54,960 --> 00:07:00,480
could do such great things for you if you'd be their friends because they are among the highest

51
00:07:00,480 --> 00:07:07,600
in beings. They are true Kalyanamita, true good friends. You should not disparage them. You should

52
00:07:07,600 --> 00:07:13,760
not think little of them, belittle them, mock them. So it's all to your detriment.

53
00:07:13,760 --> 00:07:22,320
And he didn't listen. But the Buddha said so before, so when he's giving him this lecture,

54
00:07:22,320 --> 00:07:28,720
this is when he said the verse in the budge, papa came in, they don't associate with those bad

55
00:07:28,720 --> 00:07:34,160
friends of yours, don't associate with low people, associate with good friends like Sariput,

56
00:07:34,160 --> 00:07:42,880
associate with the highest people like Mogulana and Sariput. But he didn't listen. And the story

57
00:07:42,880 --> 00:07:50,960
goes on. The Buddha said to Ananda, to said to the monks, he's not going to, during the time

58
00:07:50,960 --> 00:07:57,920
of my life, as long as I'm alive, he's never going to be, he will never be humbled. But once

59
00:07:57,920 --> 00:08:05,520
I pass away, he will be humbled. And so they had Ananda asked him, this is a prelude to the

60
00:08:07,920 --> 00:08:12,720
Parini Banas, Mahaparini Banas, where Ananda actually asks the Buddha, what are we supposed

61
00:08:12,720 --> 00:08:18,880
to do with Chana? It's like he remembered this part of the story. The Buddha said,

62
00:08:20,000 --> 00:08:23,920
that after he passed away, Chana would be humbled. So he said, what do we do to humble him?

63
00:08:24,960 --> 00:08:30,400
And the Buddha said, give him the highest, the ultimate punishment, the brahmananda,

64
00:08:31,120 --> 00:08:36,880
then the means stick literally, but it seems to mean punishment. And brahmana means, of course,

65
00:08:36,880 --> 00:08:42,560
highest to God, punishment of God, sort of. But it means that the punishment of the highest, or the

66
00:08:42,560 --> 00:08:49,280
highest punishment, the ultimate punishment. What do you think the ultimate punishment is?

67
00:08:52,400 --> 00:08:55,840
So they asked him, what is that ultimate punishment? What is the brahmananda?

68
00:08:56,960 --> 00:09:04,480
And Buddha said, let him say what he wants, but no one should consult with him, talk to him,

69
00:09:04,480 --> 00:09:12,640
teach him, and manage him, get involved with him in any way. Let no one teach him, let no one help him.

70
00:09:15,120 --> 00:09:24,400
Let no one try and make him a better person. That right there, it's like killing him. It's like

71
00:09:26,560 --> 00:09:32,960
there's the Buddha mentioned this kind of thing to a prince once. I think it was prince

72
00:09:32,960 --> 00:09:43,520
ambaya. I can't remember. I can't remember. If I teach people, if I teach my disciples,

73
00:09:43,520 --> 00:09:47,680
if I give them a good teaching, tell them, this is good, do this. And they don't listen,

74
00:09:47,680 --> 00:09:52,720
then I give them a hard teaching. I say, don't do this, that's bad. And if they still don't

75
00:09:52,720 --> 00:10:00,960
listen, then I kill them. He said, what would you mean you kill them? None of us, I don't teach

76
00:10:00,960 --> 00:10:08,960
them, I don't help them, and all of my disciples also cease helping and teaching them.

77
00:10:09,760 --> 00:10:15,440
The prince said, indeed, that's as though killing them. So this is the ultimate punishment.

78
00:10:18,000 --> 00:10:23,280
And after the Buddha passed away, indeed, Ananda went with a bunch of monks. He told Mahakasupa that

79
00:10:23,280 --> 00:10:28,480
this is what the Buddha had said at the first council. He told Mahakasupa, and Mahakasupa said,

80
00:10:28,480 --> 00:10:36,320
well, then you go and bestow this. Bestow the Brahmadanda on Chandman. They went to Chandna,

81
00:10:37,760 --> 00:10:44,320
and Chandna was shaken by it and eventually humbled.

82
00:10:48,000 --> 00:10:54,960
So that's our story. How does this relate to our practice? Well, it's got us very simple teaching,

83
00:10:54,960 --> 00:11:00,640
but if we look at the story, we can find a couple of other points. First is the point of pride,

84
00:11:02,400 --> 00:11:10,640
pride in stature. I've met people in Buddhist teachers, actually, who are very proud of their

85
00:11:10,640 --> 00:11:20,560
seniority. And I think it's very important to point out how senior they are. I was practicing

86
00:11:20,560 --> 00:11:24,560
since this time and this time, I'm a senior student of this person in that person.

87
00:11:27,200 --> 00:11:30,800
Well, I guess no, it really matters, right? As though that says something.

88
00:11:35,200 --> 00:11:39,840
It's funny that Chandna would be like that. Being so close to the Buddha and still didn't understand.

89
00:11:40,880 --> 00:11:46,560
You know, there's something to be said about seniority. The Buddha said that's the easiest way

90
00:11:46,560 --> 00:11:55,440
to set up a system. If you have a monastic system, seniority makes things a lot easier.

91
00:11:56,720 --> 00:12:02,880
But that only really applies to an institution, right? In an institution, seniority makes sense,

92
00:12:02,880 --> 00:12:11,840
because how are you going to judge merit, right? And if merit leads to institutional rank,

93
00:12:11,840 --> 00:12:19,280
you can see where the problem lies. People pretending to have merit or people getting greedy about,

94
00:12:19,280 --> 00:12:27,840
you know, cultivating their practice simply to gain a stature and that kind of thing, right?

95
00:12:29,440 --> 00:12:33,680
So in that case, going by seniority makes sense. But to say that somehow you're better than someone

96
00:12:33,680 --> 00:12:39,360
or to hold yourself up because of seniority is ridiculous. Functionally, it makes sense.

97
00:12:39,360 --> 00:12:42,000
It stops a lot of the ego.

98
00:12:45,280 --> 00:12:46,800
But it was funny. I talked to

99
00:12:50,960 --> 00:12:56,400
there was ever staying with a monk once and he wasn't the greatest of monks.

100
00:12:57,840 --> 00:13:06,320
And we got a feud and at one point he was saying, oh, this monk is so much junior to me.

101
00:13:06,320 --> 00:13:12,400
I'm not sure if that was anywhere. There was something and I went to see one of the big monks

102
00:13:12,400 --> 00:13:19,120
and I was saying, you know, this is the, this is what he's saying and he said,

103
00:13:20,880 --> 00:13:25,680
he said, seniority is of too, there's many kinds of seniority. One kind of seniority,

104
00:13:25,680 --> 00:13:27,040
it's seniority of ability.

105
00:13:27,040 --> 00:13:33,600
But the problem there is how do you decide who has the ability?

106
00:13:37,200 --> 00:13:40,880
Still, it makes a lot more sense. You have to take both into account.

107
00:13:42,000 --> 00:13:47,600
In fact, you should never, right? Why would if Mogulan and sorry put over in that position,

108
00:13:47,600 --> 00:13:52,880
they would have never thought, who is this chanagai who the Buddha holds up so highly?

109
00:13:52,880 --> 00:13:56,800
One should never hold oneself above others, right?

110
00:13:57,760 --> 00:14:00,720
So this idea of pride is really important.

111
00:14:01,520 --> 00:14:05,600
You should never think of ourselves as a advanced meditator, so I've been practicing for so

112
00:14:05,600 --> 00:14:10,000
many years. It's usually what we say when we meet other people. I've been practicing for so

113
00:14:10,000 --> 00:14:16,720
many years. It's really kind of unwholesome, you know? Because it's kind of boasting in the sense,

114
00:14:16,720 --> 00:14:21,360
like you, you say it because you want people to be impressed. Which is really a bad thing.

115
00:14:21,360 --> 00:14:24,800
It doesn't really work very well, right? Because who wants to hear that?

116
00:14:24,800 --> 00:14:33,120
Ooh, wow. You're better than me, right? It's a good way to create animosity. In fact, jealousy.

117
00:14:35,120 --> 00:14:38,720
That's a good test for us, on the other hand, to be humble.

118
00:14:43,200 --> 00:14:47,760
At least to recognize our jealousy, to recognize our ego.

119
00:14:47,760 --> 00:14:54,880
Another thing we don't want to do is be falsely humble, to pretend to be humble, or to say,

120
00:14:54,880 --> 00:15:02,320
to disparage ourselves in order to appear humble, or in order to cultivate humility.

121
00:15:02,320 --> 00:15:08,400
It's not really something you cultivate. It's funny, you know? Wholesome qualities for the most part

122
00:15:08,400 --> 00:15:14,720
aren't something you should cultivate. There's something that should come when your mind is purified.

123
00:15:14,720 --> 00:15:19,120
It's like you purify the soil and everything grows. All the good things grow.

124
00:15:20,080 --> 00:15:23,840
Naturally. So humility isn't something you can cultivate.

125
00:15:24,480 --> 00:15:28,960
Really, the only thing we should be cultivating is mindfulness. The Buddha put it in a special category.

126
00:15:29,760 --> 00:15:33,200
It's the only one that's always useful. It's the only one that you should always

127
00:15:34,720 --> 00:15:40,000
be focused on. Because through the practice of mindfulness, you'll see your ego,

128
00:15:40,000 --> 00:15:45,360
you'll see your jealousy, you'll see these qualities. If China had just been mindful,

129
00:15:46,080 --> 00:15:49,920
he would have seen that this wasn't helping him, it wasn't making him happier.

130
00:15:52,640 --> 00:15:54,320
But that's really the only way to do it.

131
00:15:54,320 --> 00:16:06,400
What else? Well, there's not too much here. The big one is in regards to association with good people.

132
00:16:10,400 --> 00:16:14,160
All of us would kill. Not kill, wouldn't it? That's the expression we would

133
00:16:15,760 --> 00:16:20,720
give a lot to have the opportunity to be that close to the Buddha.

134
00:16:20,720 --> 00:16:24,160
That close to the two disciples, two chief disciples, that alone the Buddha.

135
00:16:25,440 --> 00:16:29,280
Do you imagine having the access to a teacher like Sariputar Mughalana?

136
00:16:30,640 --> 00:16:37,920
And here he is going around this by arranging them, saying bad things about them.

137
00:16:40,000 --> 00:16:45,840
Who do they think they are? These newcomers? The monks were like that as well. They

138
00:16:45,840 --> 00:16:53,200
wondered why Kundanya wasn't made the chief disciple. The Buddha said it's because of

139
00:16:54,000 --> 00:17:00,400
a difference of determination. The determination is the wish you make. The

140
00:17:02,160 --> 00:17:10,080
Atitana means what you fix on, what your goal is basically. So Kundanya's goal was to become

141
00:17:10,080 --> 00:17:14,560
the first one to realize the Buddha's teaching. And there's a story about him. And I think we'll

142
00:17:14,560 --> 00:17:23,440
get to if we haven't already. And Sariputar Mughalana made the determination. It was their

143
00:17:23,440 --> 00:17:30,000
long standing wish. They weren't newcomers. They had born and been born and died with the Buddha

144
00:17:30,000 --> 00:17:35,200
so many lifetimes. If you read the Jatakas, the number of Jatakas that included Sariputar,

145
00:17:35,200 --> 00:17:46,720
more than Mughalana, I think, but also Mughalana. They were with him from long, long ago. Because

146
00:17:46,720 --> 00:17:54,240
when you associate with unpleasant people, how hard is it to be mindful? How easy it is to get

147
00:17:54,240 --> 00:18:00,160
caught up in unwholesomeness to acquire the qualities of these unwholesome people.

148
00:18:00,160 --> 00:18:10,960
Where something in a way, all you have to do if you're open and you appreciate wise people,

149
00:18:10,960 --> 00:18:16,560
all you have to do is be around them. Enter your openness and your appreciation for them.

150
00:18:17,200 --> 00:18:25,920
You will only stand again. You will only stand to benefit, to progress, to better yourself,

151
00:18:25,920 --> 00:18:34,720
to emulate them, and to follow their example. So, simple teaching, simple story.

152
00:18:36,800 --> 00:18:41,120
That's the number of, number part of her today. Thank you all for tuning in.

153
00:18:41,120 --> 00:18:57,040
And keep practicing and be well. Good.

