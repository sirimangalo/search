1
00:00:00,000 --> 00:00:10,000
Hello, I'm here today to announce something that I think might be of interest to the online

2
00:00:10,000 --> 00:00:15,320
community of people who follow my work.

3
00:00:15,320 --> 00:00:24,240
This October I will be heading off to Thailand to pay respect and visit, learn from, and

4
00:00:24,240 --> 00:00:34,640
just to see my teacher, Adjantong Siri Mangalo, who will be 91 this year, and has been probably

5
00:00:34,640 --> 00:00:45,480
the biggest influence as a living person on my life, well by far has been.

6
00:00:45,480 --> 00:00:52,120
So someone who is very important to me, as a part of the trip, will also be practicing

7
00:00:52,120 --> 00:00:57,080
on my own and teaching others, leading retreats up on a mountain somewhere, but as part

8
00:00:57,080 --> 00:01:02,520
of the trip to see my teacher, I will be making an offering which is customary at the end

9
00:01:02,520 --> 00:01:07,560
of the reins to the offering of robes.

10
00:01:07,560 --> 00:01:12,600
So at the end of the reins after having three months where you couldn't go anywhere, monks

11
00:01:12,600 --> 00:01:15,520
will often be in need of a new set of robes.

12
00:01:15,520 --> 00:01:20,280
So the tradition is that at the end the monks would gather together all the cloth that

13
00:01:20,280 --> 00:01:27,160
they had made and present at least one set of robes to some monk who needed them, probably

14
00:01:27,160 --> 00:01:31,440
his robes would be worn out, or maybe it was just a monk that they respect if there

15
00:01:31,440 --> 00:01:35,640
was no one who really needed them, but it would be a tradition called the Katina where

16
00:01:35,640 --> 00:01:39,720
they would get together and make a symbolic offering of robes.

17
00:01:39,720 --> 00:01:46,040
Now it's evolved into, or even in the beginning it could be expanded into offering

18
00:01:46,040 --> 00:01:47,480
many sets of robes.

19
00:01:47,480 --> 00:01:52,880
So I won't be obviously the only person offering robes on that day, but what I'd like

20
00:01:52,880 --> 00:01:59,640
to do is open this up to people who might be interested in also offering robes.

21
00:01:59,640 --> 00:02:04,240
Now this doesn't mean that I need you to send me sets of robes, I think that's impractical,

22
00:02:04,240 --> 00:02:08,360
I won't even be bringing robes from Canada myself, we'll be getting them when I get to

23
00:02:08,360 --> 00:02:10,760
Thailand and offering them.

24
00:02:10,760 --> 00:02:16,440
But in order to open this up to people who are interested who want to get excited about

25
00:02:16,440 --> 00:02:22,600
doing good deeds who want to cultivate wholesomeness in this way, who have the resources

26
00:02:22,600 --> 00:02:30,080
by which they could offer a set of robes and would like to do something to cement their

27
00:02:30,080 --> 00:02:34,520
own mind in the practice and cultivation of goodness.

28
00:02:34,520 --> 00:02:41,200
We have thereby the organization that I run or am involved in and my board of directors

29
00:02:41,200 --> 00:02:49,680
and I, we have put together a Indiegogo project which you can go to, it's in the link

30
00:02:49,680 --> 00:02:59,160
in the description to this video and it basically allows you to get involved and you can

31
00:02:59,160 --> 00:03:06,440
go there and get details if you want to be involved with donating a set of robes.

32
00:03:06,440 --> 00:03:15,760
Now our goal is potentially to offer 84 sets of Buddhist monks robes to my teacher.

33
00:03:15,760 --> 00:03:17,800
Why 84 you ask?

34
00:03:17,800 --> 00:03:24,640
84 itself is a symbolic number, 84,000 is the number of teachings that are said to

35
00:03:24,640 --> 00:03:29,920
be contained in the scriptures that we have recorded of the Buddha.

36
00:03:29,920 --> 00:03:35,440
So it's symbolic of also paying respect to the dharma but the real question is what

37
00:03:35,440 --> 00:03:38,720
could one monk possibly do with 84 sets of robes?

38
00:03:38,720 --> 00:03:47,200
Well the monastery where my teacher lives and teaches has evolved from a small community

39
00:03:47,200 --> 00:03:56,840
temple to be a huge multi-acre meditation center with hundreds of meditators and actually

40
00:03:56,840 --> 00:04:00,360
over a hundred monks who will stay during the rains.

41
00:04:00,360 --> 00:04:04,960
So by the end of the rains there will be at least a hundred monks staying there.

42
00:04:04,960 --> 00:04:09,600
We actually won't be able to give a set of robes to all of them unless we get over

43
00:04:09,600 --> 00:04:13,960
our limit and decide to offer more than 84.

44
00:04:13,960 --> 00:04:18,600
Maybe we could switch to 108 which is also a symbolic number but for now we're setting

45
00:04:18,600 --> 00:04:23,680
limit at 84 which is the number that I've actually offered before I've done this before

46
00:04:23,680 --> 00:04:24,840
and it worked really well.

47
00:04:24,840 --> 00:04:28,520
The monks were ecstatic, they were able to get quality monks robes that many of them had

48
00:04:28,520 --> 00:04:38,080
never seen, many of the young monks who don't have any followers or so and they're never

49
00:04:38,080 --> 00:04:44,880
able to get this kind of support so they've never seen such quality monks robes.

50
00:04:44,880 --> 00:04:48,240
So it's something that is highly appreciated.

51
00:04:48,240 --> 00:04:52,760
It's also a great feeling to be able to come in there with such a huge number of robes

52
00:04:52,760 --> 00:04:58,480
and to really show the extent of our appreciation for our teacher.

53
00:04:58,480 --> 00:05:06,760
And more than that it's a symbolic gesture on our part to support the monastic order.

54
00:05:06,760 --> 00:05:12,280
If you're someone who is keen on the idea of becoming a monk one day or of supporting

55
00:05:12,280 --> 00:05:17,840
monks in their dissemination of the Buddha's teaching and their dedication to disseminating

56
00:05:17,840 --> 00:05:24,000
and practicing and upholding the teachings of the Buddha, especially in this case because

57
00:05:24,000 --> 00:05:31,360
this is a monastery, many of these monks have ordained specifically at this monastery because

58
00:05:31,360 --> 00:05:37,360
they were interested in the meditation practice and so this is a direct way to support

59
00:05:37,360 --> 00:05:41,720
the monastic tradition in this lineage.

60
00:05:41,720 --> 00:05:48,720
So symbolically it means a lot to make this kind of a donation for me at least.

61
00:05:48,720 --> 00:05:55,920
It means something great to be able to offer this, one of the basic requisites for a monk

62
00:05:55,920 --> 00:05:58,640
which is this robes that I'm wearing.

63
00:05:58,640 --> 00:06:01,120
Each set of robes has three robes in it.

64
00:06:01,120 --> 00:06:05,520
This is the upper robe that you see and underneath there's the lower robe and they're

65
00:06:05,520 --> 00:06:09,480
big pieces of cloth and they're not easy to sew.

66
00:06:09,480 --> 00:06:14,440
So it really is a great thing to be able to give this.

67
00:06:14,440 --> 00:06:17,800
Most people don't ever have the opportunity.

68
00:06:17,800 --> 00:06:24,320
And this special occasion is really the right time to offer because as I said it's

69
00:06:24,320 --> 00:06:30,120
when it's needed and it's when it's expected, it's when everyone comes together to do

70
00:06:30,120 --> 00:06:31,120
this.

71
00:06:31,120 --> 00:06:34,920
So it will be I think a great opportunity if we can actually do this.

72
00:06:34,920 --> 00:06:42,560
If we don't get 84 people interested or enough support together together, 84 sets of

73
00:06:42,560 --> 00:06:46,760
robes and we'll just offer whatever we have if we get more as I said maybe we'll increase

74
00:06:46,760 --> 00:06:47,760
it.

75
00:06:47,760 --> 00:06:53,640
I just wanted to put it out there in case there's anyone and I'm assuming there is who

76
00:06:53,640 --> 00:07:00,000
wants to get involved with this great endeavor to help us to support the Buddhist teaching,

77
00:07:00,000 --> 00:07:06,800
to support the Buddhist religion, to support our own practice of Buddhism by cultivating

78
00:07:06,800 --> 00:07:17,720
generosity and respect and renunciation and putting our vote, our resources and our effort

79
00:07:17,720 --> 00:07:20,400
behind something that we truly believe in.

80
00:07:20,400 --> 00:07:27,680
So I like to encourage you in this but only, I'm not asking for this, this is not something

81
00:07:27,680 --> 00:07:30,040
that you need to feel obliged to do good.

82
00:07:30,040 --> 00:07:35,440
But something that I would encourage because I myself believe in it and so if it's something

83
00:07:35,440 --> 00:07:39,320
that you'd like to do and it would come from your heart and you really feel would make

84
00:07:39,320 --> 00:07:47,640
you feel good to do, then absolutely this is a great way to cultivate generosity and

85
00:07:47,640 --> 00:07:51,480
respect and so on and all those things.

86
00:07:51,480 --> 00:07:59,920
So let us know if you're interested, visit the Indiegogo project below and you can leave

87
00:07:59,920 --> 00:08:08,320
comments if you have anything to say or get in touch with our organization via our website

88
00:08:08,320 --> 00:08:12,200
which I'll also put in the link below.

89
00:08:12,200 --> 00:08:19,880
And yeah, so thank you for your consideration and most importantly thanks for the support,

90
00:08:19,880 --> 00:08:28,560
the support, moral support that people have been giving just by following and liking and

91
00:08:28,560 --> 00:08:32,000
commenting on and practicing.

92
00:08:32,000 --> 00:08:38,600
The teachings that I presented in these videos, I mean I get so many awesome comments from

93
00:08:38,600 --> 00:08:45,400
people about how these teachings help and that's really all that we're trying to do.

94
00:08:45,400 --> 00:08:50,920
I'm not trying to put myself out of some kind of big teacher, obviously I don't have

95
00:08:50,920 --> 00:08:57,800
anything special to offer but personally but what I have in Buddhist teaching is something

96
00:08:57,800 --> 00:09:03,640
special and I think a lot of these teachings are not well represented or well understood

97
00:09:03,640 --> 00:09:10,520
by a great portion of the world and so it seems to be a good thing to spread those so

98
00:09:10,520 --> 00:09:12,600
that's what that's all about.

99
00:09:12,600 --> 00:09:20,760
But a little bit off topic, anyway topic is offering 84 sets of robes to my teacher who will

100
00:09:20,760 --> 00:09:25,200
then offer them to the monks, we'll share them with the monks staying with him during

101
00:09:25,200 --> 00:09:29,800
the rains, we'll have it all on video when I go there, I hope someone will have someone

102
00:09:29,800 --> 00:09:37,080
take a video and see the goodness of it so if any and the other thing is for some people

103
00:09:37,080 --> 00:09:42,000
this kind of thing is important just to mention that the robes set, I don't have one

104
00:09:42,000 --> 00:09:50,600
here, there's one over there but you can look at on the internet Google Buddhist monks

105
00:09:50,600 --> 00:09:55,320
robes and you'll see they're in a set and we're going to put, we can put if you'd rather

106
00:09:55,320 --> 00:10:01,680
not, we don't have to but we'll put the names or any name you choose as long as it's

107
00:10:01,680 --> 00:10:08,600
not any reasonable name, serious name on the monks robes when we offer them.

108
00:10:08,600 --> 00:10:13,400
So if you want to dedicate, meaning is if you want to dedicate an offering to someone,

109
00:10:13,400 --> 00:10:18,120
you can do that so if it's someone who's passed away or someone who is sick or someone

110
00:10:18,120 --> 00:10:26,800
who is, you think would benefit from dedicating this offering to, for some people that's

111
00:10:26,800 --> 00:10:35,680
a neat idea and might be appropriate so you're welcome to do that, you're welcome to offer

112
00:10:35,680 --> 00:10:41,680
many sets or even part of a set you can gather together, it's all the information on

113
00:10:41,680 --> 00:10:48,280
the indiegogo site and so this is just me letting you know about it if it's of interest

114
00:10:48,280 --> 00:10:57,760
you, so thanks for all those people who do get involved may, may list be a support for

115
00:10:57,760 --> 00:11:04,080
all of our practice and for our cultivation of goodness, for our dedication to purity

116
00:11:04,080 --> 00:11:09,440
of mind, wholesomeness, goodness, Buddhism, the Buddhist religion, the Buddhist monastic

117
00:11:09,440 --> 00:11:19,160
order, my teacher, our teacher and just the cultivation of the spiritual life, may we all

118
00:11:19,160 --> 00:11:25,960
obtain true peace, happiness and freedom from suffering and may this be a support for our

119
00:11:25,960 --> 00:11:52,800
progress towards that goal, thank you and wishing you all

