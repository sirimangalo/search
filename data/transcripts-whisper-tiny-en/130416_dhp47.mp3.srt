1
00:00:00,000 --> 00:00:05,760
Okay, hello and welcome back to our study of the Dhamabanda.

2
00:00:05,760 --> 00:00:14,040
Today we go on to verse number 47, which reads as follows.

3
00:00:14,040 --> 00:00:31,600
Pupani heva, pachinantam, bhyasatamandasangwaram, sutangamamamohogo matrudayyagatati, which means

4
00:00:31,600 --> 00:00:39,520
Pupani heva, pachinantam, like one who collects or gathers flowers, bhyasatamandasan

5
00:00:39,520 --> 00:00:53,360
around the man or a person who is with a distracted mind or with a negligent mind, with a

6
00:00:53,360 --> 00:01:04,360
heedless mind, sutangamamamohogo matrudayyagatati, matrudayyagatati, is carried off by death,

7
00:01:04,360 --> 00:01:16,240
or death carries away such a person, sutangamamohogo just as a great flood carries away

8
00:01:16,240 --> 00:01:26,160
a sleeping village. So one who's mind is distracted or was distracted in mind like someone

9
00:01:26,160 --> 00:01:39,600
collecting flowers is carried away by death, just as carried away unexpectedly by death,

10
00:01:39,600 --> 00:01:47,040
or unready not ready for death, just as a sleeping village is carried away unexpectedly

11
00:01:47,040 --> 00:01:57,440
without any time for preparation by a great flood. So this was told in regards to a fairly

12
00:01:57,440 --> 00:02:07,840
famous story and quite a long story, so I'll try to bridge it and give you the condensed

13
00:02:07,840 --> 00:02:18,280
version. Basically it starts with the important part of the story starts with King

14
00:02:18,280 --> 00:02:26,640
Poseinadhi, who was the king in the time of the Buddha, and he got, he gained great faith

15
00:02:26,640 --> 00:02:33,120
in the Buddha, and as a result invited monks to come to the palace, and for a few days he

16
00:02:33,120 --> 00:02:40,200
looked after them, but then something like after the seventh day he stopped, he lost interest

17
00:02:40,200 --> 00:02:50,440
and went back to his worldly affairs, and as a result the monks weren't taken care of,

18
00:02:50,440 --> 00:02:55,480
because if the king doesn't give the orders, no one really cares, and so the people

19
00:02:55,480 --> 00:03:00,080
in the palace when monks came weren't doing it out of faith, no one was really interested

20
00:03:00,080 --> 00:03:10,040
in the monks, and so they weren't fed or they were fed late or as much confusion

21
00:03:10,040 --> 00:03:20,440
and it wasn't really a wholesome scene. So as a result the monks slowly won by one by one,

22
00:03:20,440 --> 00:03:25,640
stopped coming to the palace, until finally the only one who was left was Ananda, because

23
00:03:25,640 --> 00:03:31,320
Ananda had this great quality about him of protecting the faith of families, so no matter

24
00:03:31,320 --> 00:03:45,040
of what he would stick to his duties with the purpose of keeping the faith of the king

25
00:03:45,040 --> 00:03:54,240
in this case. And so one day the king remembered the monks and he came to see, oh, how

26
00:03:54,240 --> 00:03:58,560
was it going with the feeding of the bikhus, and he goes there and he sees that there's

27
00:03:58,560 --> 00:04:02,880
only Ananda and he says, what's going on, we're not the monks so early, when you stop

28
00:04:02,880 --> 00:04:14,600
coming they slowly stopped coming as well. And he went to the Buddha and they can't

29
00:04:14,600 --> 00:04:27,760
remember what the Buddha said, but gave him some reasons as to why the monks weren't coming

30
00:04:27,760 --> 00:04:34,920
or something like that. And the Buddha taught as a result of this, he didn't force the

31
00:04:34,920 --> 00:04:40,720
monks to go there, he said, if this is he gave in the end good, then he said, if families

32
00:04:40,720 --> 00:04:51,640
don't have nine traits, nine qualities, it's not important to go to, you need not bother

33
00:04:51,640 --> 00:04:59,360
with visiting those families. But if they have the opposite nine qualities, or if they're

34
00:04:59,360 --> 00:05:04,880
free from those nine qualities, if they have the opposite, then they should be visited.

35
00:05:04,880 --> 00:05:13,840
So this is like, if they stand up respectfully when the monks come, if they stand up and

36
00:05:13,840 --> 00:05:23,480
are respectful and are happy to stand up as a respectful gesture, if they hold their hands

37
00:05:23,480 --> 00:05:30,560
up, if they venerate you respectfully, if they do it happily, if they put out seats happily,

38
00:05:30,560 --> 00:05:43,440
if they bring you lots of food, if they don't do these things, if they give coarse

39
00:05:43,440 --> 00:05:49,240
food and so on, if they don't listen to the dhamma when you teach it, if they're not

40
00:05:49,240 --> 00:05:53,880
interested in the dhamma, if they don't, if when you're talking, they don't listen and

41
00:05:53,880 --> 00:06:00,400
so on, then these are reasons not to go there. So the Buddha was kind of taking the sides

42
00:06:00,400 --> 00:06:05,640
of the monks, but the king went back home and so what it was is the Buddha had told

43
00:06:05,640 --> 00:06:14,400
him that it must be because there was no one there and there was no there was no unfamiliar

44
00:06:14,400 --> 00:06:17,960
there. They weren't familiar with the king or something like that, because the point

45
00:06:17,960 --> 00:06:25,080
of the story was that the king got it in his head that he would find some way to make

46
00:06:25,080 --> 00:06:29,760
himself dear to the monks and the vicinity, if you know the story of it was any, he wasn't

47
00:06:29,760 --> 00:06:38,560
the brightest, what do you say? It wasn't the sharpest knife in the drawer. So instead

48
00:06:38,560 --> 00:06:43,200
of thinking, maybe I'll practice meditation or maybe I'll actually listen to the monks

49
00:06:43,200 --> 00:06:50,200
when they teach or something. What did he think? He thought, I'll marry one of their,

50
00:06:50,200 --> 00:06:57,000
one of the Buddha's relatives. I'll take one of the Buddha's relatives as my queen and

51
00:06:57,000 --> 00:07:06,480
that will be a sure way of getting in good with Buddha. That's what he thought. And

52
00:07:06,480 --> 00:07:16,080
so he sent a messenger to the sakya's in Kapilawatu and said, please send me a queen,

53
00:07:16,080 --> 00:07:23,600
send me a woman to be my queen. I want to marry into the sakya in the sakya's, the

54
00:07:23,600 --> 00:07:31,680
same volume. And you know this isn't going anywhere good. Now the sakya's are, the sakya's

55
00:07:31,680 --> 00:07:41,560
are very proud. It was one of their failings. They, they would never mix with another tribe.

56
00:07:41,560 --> 00:07:47,480
And yet they knew that if they refused King vicinity, he would invade them and he would,

57
00:07:47,480 --> 00:07:51,800
he would be quite, he would, he would start a war with them and they didn't want that

58
00:07:51,800 --> 00:07:59,760
because they weren't definitely not as strong as him, nor as warlike. So they talked

59
00:07:59,760 --> 00:08:07,320
about it. And Mahanama, this famous sakya and prince, he suggested that they send his

60
00:08:07,320 --> 00:08:19,760
daughter who was a daughter with a slave woman. So the point being that she wasn't, she

61
00:08:19,760 --> 00:08:25,880
wasn't, she wasn't really a legitimate child. And so they wouldn't, they wouldn't feel

62
00:08:25,880 --> 00:08:34,640
like they were betraying their, their clan purity or whatever. And so this, they did this

63
00:08:34,640 --> 00:08:42,840
and they, they tricked the king and the king, for a long time thought that this was,

64
00:08:42,840 --> 00:08:53,400
you know, a real sakya in princess and he had a son with her. And then his son went

65
00:08:53,400 --> 00:08:59,520
back to, after many, after growing up, his son went back to visit the sakya's and overheard

66
00:08:59,520 --> 00:09:07,680
someone talking about him as that bastard child of the slave woman. And so the word got

67
00:09:07,680 --> 00:09:10,840
back to the king and the king went to see the Buddha and said, you know, that's not really

68
00:09:10,840 --> 00:09:16,760
correct. They shouldn't have done that. And he said, but your son is still the, still

69
00:09:16,760 --> 00:09:23,120
the son of a prince. And so he, he consoled the king about this. This is, you know, this

70
00:09:23,120 --> 00:09:26,120
is quite a bridge. So there's actually a lot more that goes on here. But now it starts

71
00:09:26,120 --> 00:09:35,520
to get interesting because the son, we do daba, something, we do daba his name. He doesn't

72
00:09:35,520 --> 00:09:39,760
let go of it. So the king says, well, that's fine. Yes, he's right. My son really is a prince.

73
00:09:39,760 --> 00:09:44,680
There's, there's nothing wrong with him. But his, the son himself is really upset by this

74
00:09:44,680 --> 00:09:53,160
and feels like he has been, I don't know if this, somehow he has been wrong by the sakya's.

75
00:09:53,160 --> 00:10:00,680
And so when king, the sanity dies and he takes, so he becomes king, he vows to kill all

76
00:10:00,680 --> 00:10:07,960
the sakya. He says, when he says, when I'm king, I'm going to destroy the sakya and race.

77
00:10:07,960 --> 00:10:18,240
And I'm going to wash my feet with their blood. That's what he says. And so he, he builds

78
00:10:18,240 --> 00:10:27,360
up this hatred in his mind. He builds up this vengeance in his mind. And one day he, he thinks

79
00:10:27,360 --> 00:10:33,520
of this, he thinks of this vengeance. He becomes king and then he makes plans and finally

80
00:10:33,520 --> 00:10:39,560
he gets into his heart. He says, now I'm going to go and he puts together his army and

81
00:10:39,560 --> 00:10:48,160
he leaves his home, which would be sabbati, I guess. And he ends out for Kapilawatu

82
00:10:48,160 --> 00:10:55,720
with his army. Now the Buddha, Buddha's have, have various duties. Buddha is considered

83
00:10:55,720 --> 00:11:03,120
to have four duties, I think, duty to oneself, duty to the world, duty to one's relatives

84
00:11:03,120 --> 00:11:07,520
and duties and duty to the lineage of the Buddhas or duty as a Buddha, whatever that

85
00:11:07,520 --> 00:11:13,800
mean. So, so there are examples of the Buddha looking after his relatives. And this is

86
00:11:13,800 --> 00:11:21,840
one prime example where he did so, sitting in his kutti, it came to his mind that this

87
00:11:21,840 --> 00:11:28,720
was happening, that that king, we do dambo as planning to destroy all of his relatives.

88
00:11:28,720 --> 00:11:40,600
His whole family was going to go and put an end to his family. And so he made a decision

89
00:11:40,600 --> 00:11:50,080
to travel and to intercept the king. And so he went halfway on the path, the Kapilawatu

90
00:11:50,080 --> 00:11:58,080
and he found a tree with very little, very few leaves on it, almost dead tree with just

91
00:11:58,080 --> 00:12:06,640
a few leaves, and he sat under it in the hot sun. And we do dambo came up and saw, oh,

92
00:12:06,640 --> 00:12:12,400
there's the Buddha. And so he got off his elephant and went up to the Buddha and bowed

93
00:12:12,400 --> 00:12:16,640
down to the Buddha and said, Lord Buddha, why are you here? Why are you sitting under this

94
00:12:16,640 --> 00:12:23,720
tree without any shade? And the Buddha said, oh, the shade of my relatives is enough

95
00:12:23,720 --> 00:12:36,360
for me. My relatives provide me all the shade I need. And the Buddha is here to protect

96
00:12:36,360 --> 00:12:44,240
his relatives. And so he gets on his elephant and then he turns around and goes back

97
00:12:44,240 --> 00:13:00,000
to sovereignty. But you know, kings, they're not the most stable sort of beings. And

98
00:13:00,000 --> 00:13:09,000
so he's unable to give up this hatred. You see, people who don't practice meditation.

99
00:13:09,000 --> 00:13:14,160
It's funny how he was so close to the Buddha and he never was able to practice meditation.

100
00:13:14,160 --> 00:13:20,840
But it's true. It's as well. The story says it's true in you. This is the story. This kind

101
00:13:20,840 --> 00:13:27,800
of thing happened for sure, though. People can be very close to Buddhism and even study

102
00:13:27,800 --> 00:13:33,000
all the Buddha's teachings and never actually practice. And so they never able to overcome

103
00:13:33,000 --> 00:13:38,080
and never able to change. And so he can't change because he's harbored this from when

104
00:13:38,080 --> 00:13:44,880
he was young. And when he first found out that he had been, that's funny because he wouldn't

105
00:13:44,880 --> 00:13:54,840
have been born if his father hadn't have married this woman. And so he decides again

106
00:13:54,840 --> 00:14:02,200
to go out. He gets angry again and he's unable to suppress it and unable to let go of it.

107
00:14:02,200 --> 00:14:08,200
And so he heads out on his elephant with his army. And again, the Buddha goes, sits under

108
00:14:08,200 --> 00:14:23,200
the same tree, says the same thing and we can have a turn around and goes back. I think

109
00:14:23,200 --> 00:14:35,280
that's the end of the story. No, sir. He goes home and like a week passes and a week later

110
00:14:35,280 --> 00:14:41,760
he's out on the elephant again. He just can't handle it. And the Buddha looks and he looks

111
00:14:41,760 --> 00:14:53,680
back into the past and he sees the sacchias have actually built up this state of affairs.

112
00:14:53,680 --> 00:14:59,760
They've done some sort of bad karma. I can't remember what it was. I don't think it

113
00:14:59,760 --> 00:15:10,560
may be even says. And there's no way he's going to be able to stop. We did numbers,

114
00:15:10,560 --> 00:15:22,660
advance. And he says their death, their relatives are destined to be wiped out. And so he

115
00:15:22,660 --> 00:15:29,020
doesn't go. He stays at home. And we did double heads out on his elephant and gets to

116
00:15:29,020 --> 00:15:33,320
copy the what-to. And see the thing about the sacchias is that they become Buddhist. They

117
00:15:33,320 --> 00:15:38,160
follow the Buddhist teaching and they're keeping the five precepts. So even the soldiers

118
00:15:38,160 --> 00:15:50,880
couldn't kill. So what did they do? They can, I think, they shoot arrows over the heads

119
00:15:50,880 --> 00:15:57,880
of their enemy. And I think they held up grass. They would collect a grass and they held

120
00:15:57,880 --> 00:16:04,280
it up like a weapon or something like that. And sticks. I can't remember what it was

121
00:16:04,280 --> 00:16:11,200
quite funny. If you read this story, how they thought they fought back and they all wiped

122
00:16:11,200 --> 00:16:16,760
out all the killed and he washed his feet with their blood. So it says the sacchias were

123
00:16:16,760 --> 00:16:21,280
actually wiped out. Which is interesting because many people in Nepal claim to be related

124
00:16:21,280 --> 00:16:27,480
to the Buddha, Sacchias, Sacchia. So the story seems to suggest otherwise. But on the other

125
00:16:27,480 --> 00:16:33,840
hand it's quite difficult to wipe out everyone in a race. It's not an easy thing. So maybe

126
00:16:33,840 --> 00:16:44,200
some escaped. And we did double his victorious and happy and feeling like he had won the

127
00:16:44,200 --> 00:16:50,480
day. And so he had headed back, started heading back home and he got to this river and he

128
00:16:50,480 --> 00:16:57,320
got all his men out and he went down by the river and washed the blood off of his feet.

129
00:16:57,320 --> 00:17:05,400
And they laid down to sleep by the river. Oh and they also had Mahanama with him. They

130
00:17:05,400 --> 00:17:08,680
captured Mahanama and they wanted to torture him or something like that. So they had him

131
00:17:08,680 --> 00:17:20,040
tied up as well with him with an army. And they slept there overnight and as they were

132
00:17:20,040 --> 00:17:35,040
asleep Maha Mahogo and a great flood came and swept them all away in the night. And so

133
00:17:35,040 --> 00:17:41,840
the monks were talking about this and they said it's amazing that there is this. How the

134
00:17:41,840 --> 00:17:48,600
world goes. I think you're on top of the world victorious in battle one day and push

135
00:17:48,600 --> 00:17:53,200
you on the next day. And so this is why the Buddha taught this verse. This is where this

136
00:17:53,200 --> 00:18:07,240
verse comes from. The Buddha said, indeed, in language and we think, when we think, I think

137
00:18:07,240 --> 00:18:16,240
we're invincible. People become so caught up in their arrogance and in their conceit, in

138
00:18:16,240 --> 00:18:25,280
their intoxication with life, intoxication with health, intoxication with youth. I think

139
00:18:25,280 --> 00:18:36,200
I'm still young. Their children who die. Some people die when they're like five years

140
00:18:36,200 --> 00:18:47,000
old and happen. True, no? Buddha said, go Tanya Maharanang Sway. Who knows whether that's

141
00:18:47,000 --> 00:18:54,080
welcome tomorrow? If you know you were going to die tomorrow, would you be negligent

142
00:18:54,080 --> 00:18:58,960
today? Would you just go and do whatever you want today? If you knew this was your last

143
00:18:58,960 --> 00:19:17,040
day? What would you do today, if you knew it was your last day? I'd get a puppy. Would

144
00:19:17,040 --> 00:19:37,120
that make you happy? The problem is that when we die, the mind continues. If we have

145
00:19:37,120 --> 00:19:47,760
clinging in our mind, we'll die with sadness. We'll die crying. We'll die longing. This is

146
00:19:47,760 --> 00:19:53,880
why people are born as ghosts. If you love your puppy today and then tomorrow you die,

147
00:19:53,880 --> 00:20:07,360
you'll be born as a ghost haunting your puppy. It's the danger of clinging. Don't realize

148
00:20:07,360 --> 00:20:17,720
that everything gets cut off. The only thing we bring with us is our subjectivity. If

149
00:20:17,720 --> 00:20:27,200
you look at this new research that I keep talking about, after people die, people die

150
00:20:27,200 --> 00:20:35,320
and then they have these experiences. Brain is dead. Hard has stopped. The temperature of

151
00:20:35,320 --> 00:20:45,320
the body is dropping and they keep in Japan, they actually pump ice or cold water into

152
00:20:45,320 --> 00:20:55,080
the bloodstream just to keep it from rotting. Hours later, they bring them back to life.

153
00:20:55,080 --> 00:20:59,720
These people talk about their experiences. It's totally subjective. It's based on the things

154
00:20:59,720 --> 00:21:05,280
that they've done in their life. It's based on their religion often. It's based on their

155
00:21:05,280 --> 00:21:11,560
ideas. It becomes totally subjective. Imagine what it would be like if you had such strong

156
00:21:11,560 --> 00:21:19,160
clinging to something. If there was something that you were afraid of spiders and you

157
00:21:19,160 --> 00:21:24,720
would see spiders, and then how would you react? When you now had nobody, there was

158
00:21:24,720 --> 00:21:31,880
nobody there to put the spider in a cup and take it outside. Suddenly, your spiders crawling

159
00:21:31,880 --> 00:21:37,000
all over you, this is what you'd see when you die. What if you couldn't handle that?

160
00:21:37,000 --> 00:21:43,840
This is why the Buddha said, we're picking flowers. Is that preparing yourself? What

161
00:21:43,840 --> 00:21:48,000
if you knew there was a flood coming through in the bag? You said, well, then I'll spend

162
00:21:48,000 --> 00:21:56,320
my time picking flowers. Why is this what we're like? It's not like where it's actually

163
00:21:56,320 --> 00:22:03,640
not even like a sleeping village. It's like a village that knows there's a flood coming.

164
00:22:03,640 --> 00:22:08,920
All the villagers say, well, then let's go pick some flowers. That's what the Buddha is

165
00:22:08,920 --> 00:22:15,880
talking about here. Like a sleeping village, but it's even worse than a sleeping village.

166
00:22:15,880 --> 00:22:20,320
It's like a village that goes to sleep knowing a flood is coming. We know there's a flood

167
00:22:20,320 --> 00:22:29,120
coming. We know death is coming. We know no end. We know it's coming. We know from Buddhism.

168
00:22:29,120 --> 00:22:38,840
We could say we have a belief that life continues after death, but there's many ways it

169
00:22:38,840 --> 00:22:44,440
doesn't have to be a belief. One of them is now this kind of evidence, even of people

170
00:22:44,440 --> 00:22:52,760
when they die, having subjective experiences. How it becomes totally subjective, where

171
00:22:52,760 --> 00:22:59,800
it's totally based on your mind. As you imagine, we do double who had just washed his feet

172
00:22:59,800 --> 00:23:04,920
in the blood of his enemies. What do you think he was thinking about when he died? When

173
00:23:04,920 --> 00:23:12,560
his brain stopped, what do you think? What images do you think he conjured up? Blood is not

174
00:23:12,560 --> 00:23:28,920
going to a good place. The Buddha said death carries you away unprepared. There's something

175
00:23:28,920 --> 00:23:36,120
about this that is not obvious unless you've been practicing meditation, and that's that

176
00:23:36,120 --> 00:23:42,400
we store up all of our experiences. Everything that we've done is stored in our mind, and

177
00:23:42,400 --> 00:23:47,200
you only see this when you meditate. As soon as you begin to meditate, you realize how much

178
00:23:47,200 --> 00:23:53,240
you're keeping inside you. All of the little obsessions that we have, because when you meditate,

179
00:23:53,240 --> 00:23:58,240
they come out and you remember things and oh, that was this bad thing I did that bad

180
00:23:58,240 --> 00:24:02,840
thing. You remember, you realize how you've hurt other people and you realize how you've

181
00:24:02,840 --> 00:24:10,680
caused suffering for others. And you feel very strongly that things that are causing you

182
00:24:10,680 --> 00:24:16,160
suffer. When you meditate, you'll think about what you want. All the things that you want

183
00:24:16,160 --> 00:24:21,640
you will think about. It will become so strong and so much suffering until you finally

184
00:24:21,640 --> 00:24:32,720
give it up. This is why some people, when they practice meditation, they think it's

185
00:24:32,720 --> 00:24:44,760
torture. I think of meditation as some kind of bad, bad idea, because it actually causes

186
00:24:44,760 --> 00:24:49,000
suffering. You have to sit still and go through all of this stuff that you just rather

187
00:24:49,000 --> 00:24:54,040
forget about, right? You don't want to have to think about this. You don't have to think

188
00:24:54,040 --> 00:25:05,040
about how much I want there, how much I want that. But that's exactly why we do it. Because

189
00:25:05,040 --> 00:25:09,120
if we don't work these things out now, we're going to have to work them out when we

190
00:25:09,120 --> 00:25:17,240
die. If we do all come up, and it's not even only when we die, then death is just one

191
00:25:17,240 --> 00:25:24,000
example. It's an extreme example, but it's true in all cases. If we have anger inside, then

192
00:25:24,000 --> 00:25:29,200
when anything happens, we'll get anger and we'll say bad things to others. If we have

193
00:25:29,200 --> 00:25:35,880
greed inside, then anytime we want something, we'll be impossible to deal with. I want

194
00:25:35,880 --> 00:25:59,480
to know who on it. Sounds like anybody you know? This is called negligence. We cling to

195
00:25:59,480 --> 00:26:05,840
things. We always try to get what we want, and that's like picking flowers. It's getting

196
00:26:05,840 --> 00:26:11,280
what you want. It doesn't make you a better person. It doesn't make you a happier person.

197
00:26:11,280 --> 00:26:19,080
I have this idea that the more you get the richer your life is. It's really not true. Not

198
00:26:19,080 --> 00:26:22,640
by any stretch of the imagination. The more you get what you want, the more wanting you

199
00:26:22,640 --> 00:26:30,360
have, the more clinging you have, the less fun you are to be around. Who are the nicest

200
00:26:30,360 --> 00:26:34,520
people? The people we love the most? Are the people we love the most? The ones who are

201
00:26:34,520 --> 00:26:40,840
the most taking people? Are the ones that take all the time? I love that person. Every

202
00:26:40,840 --> 00:26:44,600
time I go over to their house, they ask me if they can borrow something. They ask me

203
00:26:44,600 --> 00:26:50,200
for money. I love that person. Who do we love? Every time I go to their house, they give

204
00:26:50,200 --> 00:26:59,280
me something. They're always helpful. They're always kind. These are the kind of people

205
00:26:59,280 --> 00:27:04,120
we love. What's the kind of person that we want to be? Why is it that we want to

206
00:27:04,120 --> 00:27:08,640
be the person who gets things all the time? Give me, give me, give me. Why do we want

207
00:27:08,640 --> 00:27:22,600
to be that person? When nobody likes that person? What is the benefit that we get from

208
00:27:22,600 --> 00:27:36,760
the thing that we get? Do we like what we get? If we want it, if we want it, then we

209
00:27:36,760 --> 00:27:50,080
get it. Do we like it? Then are we satisfied with that? Like when you get a new toy, or

210
00:27:50,080 --> 00:27:56,680
Christmas, when Christmas comes, how long before you're bored of your toys at Christmas?

211
00:27:56,680 --> 00:28:02,960
How many hours? How many minutes? I remember Christmas when we get all these wonderful

212
00:28:02,960 --> 00:28:09,160
stuff. Really cool. Just the coolest toys. Then the next day we're like, okay, next Christmas

213
00:28:09,160 --> 00:28:19,040
I want this. Easter. What's next? Birthday. Birthday is coming up. Do you have Christmas?

214
00:28:19,040 --> 00:28:31,960
You must have Christmas, yeah. We had Christmas and Hanukkah, and eight. Hanukkah's eight

215
00:28:31,960 --> 00:28:47,680
days of Christmas. That's the theory. It doesn't really happen. But no, these things don't satisfy

216
00:28:47,680 --> 00:28:53,000
us. Things that we need are useful. But the point isn't getting what you want. The point

217
00:28:53,000 --> 00:28:57,680
is dedicating your life to that. What is most important in your life? Usually it's getting

218
00:28:57,680 --> 00:29:04,440
what we want, right? So there's no question about whether they're useful or not. The

219
00:29:04,440 --> 00:29:09,600
question is, why is that the focus of our lives? Why is that the kind of person we want

220
00:29:09,600 --> 00:29:17,520
to be? Someone who gets? Why is that how we're using our life? We all know people like

221
00:29:17,520 --> 00:29:22,640
this, people who want, people who cling, people who take. We don't want to be around such

222
00:29:22,640 --> 00:29:27,480
people. I don't want to go near that person. They're just going to ask me for this or that,

223
00:29:27,480 --> 00:29:35,520
right? Kids in class who are always, what do you got for lunch? Or did you do your homework?

224
00:29:35,520 --> 00:29:43,200
When I copy it, it's kind of me. Always, always. Can I borrow this? Can I borrow that?

225
00:29:43,200 --> 00:29:53,720
Can I borrow money? Can I borrow? These are people we don't want to be. So we have to consider

226
00:29:53,720 --> 00:29:57,880
carefully, how do we want to live our lives? And how should we live our lives so that

227
00:29:57,880 --> 00:30:07,400
it will be ready for anything, even death? Death is a great unknown. And for most of

228
00:30:07,400 --> 00:30:11,040
it, most of it's a very sad thing. I remember when I was young, it was one of the things

229
00:30:11,040 --> 00:30:19,280
that impaled me towards spirituality and finally Buddhism was being in my house and looking.

230
00:30:19,280 --> 00:30:26,000
I remember it's one of the oldest memories I have, or clearest of my young women, walking

231
00:30:26,000 --> 00:30:30,840
in the house and somehow it hit me. I don't remember how I walked downstairs and I looked

232
00:30:30,840 --> 00:30:38,480
at my mother and I said, she's going to die. I thought of my father and I said, he's

233
00:30:38,480 --> 00:30:48,160
going to die. My brothers, they're all going to die. What do I do? What can I do about

234
00:30:48,160 --> 00:30:59,760
that? It's just this kind of thinking that's important for us. We have to have an answer here.

235
00:30:59,760 --> 00:31:12,480
We have to have an answer to these questions. Death is an inevitability. Something that

236
00:31:12,480 --> 00:31:23,720
we all have to face. It's not... It often sounds kind of depressing, actually. I think,

237
00:31:23,720 --> 00:31:29,760
well, that's all life is just for death, but it's actually not. Death is not a terminal

238
00:31:29,760 --> 00:31:42,320
point. It's a conjunction. It's a junction point between this life is certain, but when

239
00:31:42,320 --> 00:31:45,400
we get to that junction, we don't know which way we're going. Are we going to go left?

240
00:31:45,400 --> 00:31:52,840
Are we going to go right? Are we going to go left? Which way? That's very important, right?

241
00:31:52,840 --> 00:31:55,400
When you're driving down the street and you come to a junction point, if you don't know

242
00:31:55,400 --> 00:32:04,680
which way you're going to go, it's crashed into the sidewalk. Left, no right, left. How do

243
00:32:04,680 --> 00:32:08,480
you know which way you're going to go? It's very important. So death isn't like terminal

244
00:32:08,480 --> 00:32:14,200
anything, well, why waste all your time thinking about that? No. Death is the moment where

245
00:32:14,200 --> 00:32:23,760
we become free from this certain life. This life which is set. Nothing is set in stone.

246
00:32:23,760 --> 00:32:34,480
Death. So this verse is a call to a wake-up call. We shouldn't be like a sleeping village.

247
00:32:34,480 --> 00:32:54,080
We shouldn't be, we should wake up and take life, take the wheel, make the, make our,

248
00:32:54,080 --> 00:33:01,200
take charge. That's a big charge of our lives so that we don't become a slave to our desires

249
00:33:01,200 --> 00:33:08,560
and a slave to our partialities. You can see this if you, I think it'd be really interesting

250
00:33:08,560 --> 00:33:13,880
to work in a hospice to work with dying people and to look and learn so much. I've talked

251
00:33:13,880 --> 00:33:20,040
to people who worked in hospice care and many things. One of the, one of the things is how

252
00:33:20,040 --> 00:33:25,680
clear the mind becomes just before death. People with dementia, even schizophrenia, there

253
00:33:25,680 --> 00:33:30,960
are studies done and probably not enough but there are studies if you look on the internet

254
00:33:30,960 --> 00:33:38,520
of people with, I think schizophrenia. But I think dementia as well. At the last moment,

255
00:33:38,520 --> 00:33:47,720
the mind becomes so clear and in pure or clear anyway. The point being that there is a

256
00:33:47,720 --> 00:33:58,080
lucidity at that last moment. And it's at that moment where the person has to face their

257
00:33:58,080 --> 00:34:02,320
karma, they have to face everything that they've done. Some people when they're dying,

258
00:34:02,320 --> 00:34:09,960
you can see the fear in their eyes. People when they're in pain, they're such fear. And

259
00:34:09,960 --> 00:34:16,840
you can see the hunted look in their eyes if they've killed other beings before. So if

260
00:34:16,840 --> 00:34:21,000
they can't come to terms with that in this life, if they can't work that out, let go

261
00:34:21,000 --> 00:34:37,240
of this guilt, this fear, they'll die with that and they'll certainly go to a bad place.

262
00:34:37,240 --> 00:34:41,560
So we have to take charge and we have to be ready for this. This is why. Excellent reason

263
00:34:41,560 --> 00:34:46,640
to meditate because we've prepared for anything when you meditate you're ready. If it floods

264
00:34:46,640 --> 00:35:00,840
you're ready, if it's built up your things, or sand, bikes, whatever. Yeah, embankments,

265
00:35:00,840 --> 00:35:11,120
no? You're ready for anything. So you sleep well, you sleep soundly. The person who

266
00:35:11,120 --> 00:35:18,920
meditates doesn't, doesn't, doesn't fear the future. The person who meditates isn't afraid

267
00:35:18,920 --> 00:35:24,400
of what comes. When you understand meditation, nothing makes you afraid. Even when you're

268
00:35:24,400 --> 00:35:31,080
afraid, you think, oh, I'm afraid of. It's a difference really because the person who meditates

269
00:35:31,080 --> 00:35:35,120
might still be angry, might still have greed, might still get afraid, you know, they might

270
00:35:35,120 --> 00:35:40,960
not have worked all these things out. But once they understand the meditation, even when

271
00:35:40,960 --> 00:35:46,880
these things come up, they don't go to that. They don't listen to the emotions. Fear

272
00:35:46,880 --> 00:35:53,920
comes up and they just say, oh, it's fear. They don't say, okay, I have to run. They

273
00:35:53,920 --> 00:36:00,240
don't follow the emotion. When they're angry, they know they're angry. When they want

274
00:36:00,240 --> 00:36:04,440
something, they know they want something. Why is it that when we want something, we say,

275
00:36:04,440 --> 00:36:10,520
well, then I have to get it. Why don't we say, oh, I have to work on the one day. Why

276
00:36:10,520 --> 00:36:23,440
have we fallen into the idea that wanting is a sign that we need something? Why do we equate

277
00:36:23,440 --> 00:36:30,240
these two? The many questions like this, that we have to study and we have to understand

278
00:36:30,240 --> 00:36:39,920
it is wanting really good for us. Is it really happiness? People who say, the important

279
00:36:39,920 --> 00:36:45,720
point of the most important thing in life is to experience, to have to these wonderful,

280
00:36:45,720 --> 00:36:51,600
amazing experiences and so on. But that's just a belief. We don't know if it's true. It

281
00:36:51,600 --> 00:36:58,080
sounds good. It's like, yeah, yeah, just do what you want, right? Okay, let's be scientific

282
00:36:58,080 --> 00:37:03,960
as ask ourselves, let's look and see what happens. When you get what you want, are you

283
00:37:03,960 --> 00:37:09,560
really happier? Is it really a stable state? And what happens when catastrophe hits? Are

284
00:37:09,560 --> 00:37:18,080
you able to deal with it? How strong are you? So many people are, we would say, relatively

285
00:37:18,080 --> 00:37:25,920
weak. When you start meditating, you see how weak people are weak. This weakness, this

286
00:37:25,920 --> 00:37:32,920
inability to deal with simple things. And all these stories of people who start meditating

287
00:37:32,920 --> 00:37:39,600
and then there's a little bit of noise around you can angry. It shows you how weak you

288
00:37:39,600 --> 00:37:46,080
are in meditation. So meditation, this is an example of how strong it makes you, how it works

289
00:37:46,080 --> 00:37:52,200
out these weaknesses. Remember after I finished, we finished our meditation course in

290
00:37:52,200 --> 00:37:57,720
Thailand. First meditation course and I went into the city with this friend, one of the

291
00:37:57,720 --> 00:38:04,480
fellow meditators and we got into the cafe and we ordered some coffee or something. And

292
00:38:04,480 --> 00:38:10,480
suddenly there was a huge, someone dropped a whole stack of dishes and huge noise and everybody

293
00:38:10,480 --> 00:38:15,240
in the, everybody in the, in the whole restaurant turn and look and we just sat there

294
00:38:15,240 --> 00:38:28,600
both of us like this hearing and then we looked at it and said, meditate. It's exciting

295
00:38:28,600 --> 00:38:35,480
actually to know that you can, you don't have to follow your instincts. You don't have

296
00:38:35,480 --> 00:38:42,760
to follow the habits. You don't have to slave to your habits. You may follow them, you

297
00:38:42,760 --> 00:38:48,480
may not, but you can choose, you can decide and you can see with wisdom whether they're

298
00:38:48,480 --> 00:38:54,280
going to make you happier. So this is the important point of this verse that we should

299
00:38:54,280 --> 00:39:02,840
not be like the heedless person gathering flowers because it doesn't prepare you to

300
00:39:02,840 --> 00:39:08,520
for the future. It doesn't make you strong. It doesn't bring you peace and happiness. Just

301
00:39:08,520 --> 00:39:16,160
gets you a bunch of dead flowers. All right. So that's the Dhamapada verse tonight. Number

302
00:39:16,160 --> 00:39:21,120
47. Thank you for tuning in. I hope that this has been useful and a reminder for us all

303
00:39:21,120 --> 00:39:27,280
that we should take, take life seriously. If you want to be happy, you should seriously

304
00:39:27,280 --> 00:39:33,360
figure out what is true happiness. Happiness is a very serious topic. If it were not,

305
00:39:33,360 --> 00:39:39,760
everyone would be happy. So let's take this seriously and truly find peace, happiness and

306
00:39:39,760 --> 00:40:08,160
freedom from suffering. Thank you and wish you all the best.

