1
00:00:00,000 --> 00:00:13,240
Now, we come to the third three of the third of purification.

2
00:00:13,240 --> 00:00:24,500
The other gives us information on the three steps of three stages,

3
00:00:24,500 --> 00:00:39,400
Sila, Samadhi and Panya. So, up to chapter 13, the first two stages Sila and Samadhi are explained.

4
00:00:39,400 --> 00:00:53,200
And so, with this chapter begins understanding or Panya, the third stage in the spiritual development,

5
00:00:53,200 --> 00:01:06,800
the description of the soil in which understanding grows. So, this is for the soil of understanding or the soil of Panya.

6
00:01:06,800 --> 00:01:18,200
So, the first chapter 14, but the first chapter of this part deals with the aggregates.

7
00:01:18,200 --> 00:01:27,300
But first, about Panya. So, now, concentration was described under the heading of consciousness in this

8
00:01:27,300 --> 00:01:33,100
kanga when a wise man established well in virtue of developed consciousness and understanding.

9
00:01:33,100 --> 00:01:38,000
And this stands up here at the beginning of the book.

10
00:01:38,000 --> 00:01:46,500
So, there we find the word consciousness. And by consciousness is meant Samadhi or concentration.

11
00:01:46,500 --> 00:01:56,400
So, concentration has been explained with different kinds of meditation.

12
00:01:56,400 --> 00:02:02,800
So, now, it's a time to talk about Panya. All that has been developed in all its aspects,

13
00:02:02,800 --> 00:02:07,700
like the Bhikkhu is just possessed of the more advanced development of concentration

14
00:02:07,700 --> 00:02:13,800
that is acquired with direct knowledge, the benefits and understanding,

15
00:02:13,800 --> 00:02:19,200
but understanding comes next. And that is still to be developed.

16
00:02:19,200 --> 00:02:22,900
So, up to this point, we have not yet developed understanding.

17
00:02:22,900 --> 00:02:29,700
We have developed concentration. So, now, that is not easy development of Panya.

18
00:02:29,700 --> 00:02:36,100
Firstly, even to know about, that is allowed to develop when it is taught very briefly.

19
00:02:36,100 --> 00:02:39,800
In order therefore, to deal with the detailed method of its development,

20
00:02:39,800 --> 00:02:47,500
there is the following set of questions. So, this question will be answered one by one.

21
00:02:47,500 --> 00:02:55,700
What is understanding? So, understanding Panya is of many sorts and has various aspects

22
00:02:55,700 --> 00:03:02,600
and in different kinds of Panya's. And also, the attempt to explain it or what accomplished

23
00:03:02,600 --> 00:03:07,000
neither its intention or its purpose and what besides lead to destruction.

24
00:03:07,000 --> 00:03:13,300
So, we shall confine ourselves to be kind intended here, which is understanding,

25
00:03:13,300 --> 00:03:18,400
consisting in insights knowledge associated with profitable consciousness.

26
00:03:18,400 --> 00:03:31,100
So, here understanding means kusara, wholesome or profitable, which accompanies the wholesome consciousness

27
00:03:31,100 --> 00:03:42,000
and which consists in insights knowledge. The word, we persona in party is, in fact,

28
00:03:42,000 --> 00:03:49,900
is synonym for Panya. So, we persona and Panya are synonyms in many places.

29
00:03:49,900 --> 00:03:56,200
So, here, by Panya is meant, we persona inside knowledge.

30
00:03:56,200 --> 00:04:02,300
In what sense is it understanding? It is a definition of the word Panya.

31
00:04:02,300 --> 00:04:06,500
It is understanding in the sense of act, of understanding.

32
00:04:06,500 --> 00:04:13,200
These piece definitions are actually very difficult to translate into any language.

33
00:04:13,200 --> 00:04:17,200
So, understanding is Panya, understanding is understanding.

34
00:04:17,200 --> 00:04:21,900
What is this act of understanding? It is knowing in a particular mode,

35
00:04:21,900 --> 00:04:25,400
separate from the modes of perceiving and cognizing.

36
00:04:25,400 --> 00:04:30,400
Now, there are three kinds of understanding or knowing.

37
00:04:30,400 --> 00:04:36,900
The knowing of the Panya, knowing of Sanya, and knowing of consciousness or Wiener.

38
00:04:36,900 --> 00:04:41,400
So, these three are differentiated here.

39
00:04:41,400 --> 00:04:50,900
So, Panya is said to be called understanding or knowing because it knows in a particular mode,

40
00:04:50,900 --> 00:04:57,900
separate from that means different from the modes of perceiving and cognizing.

41
00:04:57,900 --> 00:05:02,900
For though the state of knowing is equally present in perception, in consciousness,

42
00:05:02,900 --> 00:05:08,400
and in understanding nevertheless, perception is only the mere perceiving of an object.

43
00:05:08,400 --> 00:05:14,900
It is a blue or yellow. So, just mere perceiving of an object is called perception.

44
00:05:14,900 --> 00:05:21,900
Sanya, it cannot bring about the penetration of its characteristic as impermanent, painful, and not self.

45
00:05:21,900 --> 00:05:29,900
So, it is maybe it is the first reaction to the object.

46
00:05:29,900 --> 00:05:33,900
The next one is consciousness.

47
00:05:33,900 --> 00:05:39,900
Consciousness knows the object as blue or yellow, and it brings about the penetration of its characteristics.

48
00:05:39,900 --> 00:05:48,900
So, at the consciousness stage, it knows the characteristics, but it cannot bring about by endeavoring the manifestation of Superman.

49
00:05:48,900 --> 00:05:52,900
In fact, understanding knows the object in the way already stated.

50
00:05:52,900 --> 00:06:00,900
It means both mere perception and penetration of characteristics.

51
00:06:00,900 --> 00:06:07,900
It brings about the penetration of characteristics and it brings about by endeavoring the manifestation of the path.

52
00:06:07,900 --> 00:06:20,900
So, Panya is the best of the three. Only through Panya can there be penetration into the nature of things.

53
00:06:20,900 --> 00:06:26,900
And then, it similarly is given, suppose there were three people and so on.

54
00:06:26,900 --> 00:06:38,900
So, each child, a villager and a money changer or someone who is familiar with coins and money and so on.

55
00:06:38,900 --> 00:06:44,900
So, the understanding of these three persons are different. It is different.

56
00:06:44,900 --> 00:06:56,900
It knows that it is the wrong thing. It may not know that it can be used to exchange with some other things, but it will know that it is the wrong thing.

57
00:06:56,900 --> 00:06:59,900
It is yellow or it is white or something like that.

58
00:06:59,900 --> 00:07:08,900
But, a villager and ordinary person knows it is the wrong thing made of copper or silver or gold.

59
00:07:08,900 --> 00:07:12,900
And, he also knows that it can be used as money.

60
00:07:12,900 --> 00:07:31,900
But, the money changer or a person who is familiar with money knows all these two and also knows that it is a coin and he knows when it was trapped, when it was made.

61
00:07:31,900 --> 00:07:35,900
And then, he knows the value of the coins and so on.

62
00:07:35,900 --> 00:07:39,900
So, he knows everything there is to know about the coin.

63
00:07:39,900 --> 00:07:44,900
So, in the same way, pioneer is like the child.

64
00:07:44,900 --> 00:07:51,900
So, without discretion seeing the coin because it every ends the mere mode of fear and subject.

65
00:07:51,900 --> 00:07:54,900
Consciousness is like the villagers seeing the coin.

66
00:07:54,900 --> 00:08:00,900
And understanding is like the money changer, seeing the coin and then understanding everything about the coin.

67
00:08:00,900 --> 00:08:12,900
So, this is the difference between three kinds of knowing, knowing of sin, knowing of consciousness, knowing of pioneer.

68
00:08:12,900 --> 00:08:21,900
In the definition of these three words, there is the root, nia, san nia, there is nia.

69
00:08:21,900 --> 00:08:24,900
And, when nia, there is nia.

70
00:08:24,900 --> 00:08:26,900
And, pioneer, there is nia.

71
00:08:26,900 --> 00:08:29,900
So, the root nia means to know.

72
00:08:29,900 --> 00:08:35,900
So, all these three know the object, but they are knowing is different.

73
00:08:35,900 --> 00:08:40,900
So, knowing of pioneer is the most penetrating.

74
00:08:40,900 --> 00:08:52,900
That is why this act of understanding should be understood as knowing any particular mode, separate form, or different form, the mode of pursuing and recognizing.

75
00:08:52,900 --> 00:08:54,900
So, that is what the word.

76
00:08:54,900 --> 00:08:58,900
It is understanding in the sense of act of understanding referred to.

77
00:08:58,900 --> 00:09:08,900
However, it is not always to be found where perception and consciousness are.

78
00:09:08,900 --> 00:09:17,900
You know, there are, say, 89 types of consciousness and not all are accompanied by pioneer.

79
00:09:17,900 --> 00:09:19,900
Only some are accompanied by pioneer.

80
00:09:19,900 --> 00:09:27,900
Say, we act as a mean and wholesome consciousness, types of consciousness are not accompanied by pioneer.

81
00:09:27,900 --> 00:09:41,900
Even among the wholesome and say, the function, there are some, there are not accompanied by pioneer.

82
00:09:41,900 --> 00:09:46,900
So, it is not always to be found where perception and consciousness are.

83
00:09:46,900 --> 00:09:52,900
But when it is, it is not disconnected from those days.

84
00:09:52,900 --> 00:10:02,900
So, pioneer can be found with some kind, some types of consciousness, but perception accompanies every type of consciousness.

85
00:10:02,900 --> 00:10:08,900
So, wherever there is perception and consciousness, there is not necessarily pioneer.

86
00:10:08,900 --> 00:10:18,900
But wherever there is pioneer, there is also perception and consciousness.

87
00:10:18,900 --> 00:10:24,900
And when it is found with perception and consciousness, it is not disconnected from these days.

88
00:10:24,900 --> 00:10:30,900
That means it arise with these days.

89
00:10:30,900 --> 00:10:39,900
It is not mixed up with, say, perception and consciousness, mixed up by a way of its characteristic and so on.

90
00:10:39,900 --> 00:10:46,900
So, because it cannot be taken as disconnected, thus, this is perception, this is consciousness, this is understanding.

91
00:10:46,900 --> 00:10:51,900
Its difference is consequently subtle and hard to see.

92
00:10:51,900 --> 00:10:58,900
Hence, the vulnerable nervous in a set, a difficult thing opening has been done by the blessed one and so on.

93
00:10:58,900 --> 00:11:00,900
So, it is for ordinary people.

94
00:11:00,900 --> 00:11:12,900
It is very difficult to see, to differentiate, say, this is perception, this is consciousness, this is understanding, this is contact and so on.

95
00:11:12,900 --> 00:11:20,900
And here, vulnerable nervous in a set to King Melinda, that the Buddha has done a difficult thing.

96
00:11:20,900 --> 00:11:32,900
And what is the difficult thing defining or differentiating the immaterial states of consciousness and kids from confidence, which occur with a single object.

97
00:11:32,900 --> 00:11:41,900
That means consciousness and mental confidence arise at the same time, taking the same object.

98
00:11:41,900 --> 00:11:49,900
So, although they take the same object, they are bi-characteristic and so on or by nature, different.

99
00:11:49,900 --> 00:12:04,900
So, defining the immaterial states of consciousness and its commitments, which occur with a single object and which he declared that this is contact, this is feeling, this is perception, this is coalition, this is consciousness.

100
00:12:04,900 --> 00:12:18,900
So, it is very difficult for ordinary people to see these mental states clearly and to say, and this is contact, this is feeling and so on.

101
00:12:18,900 --> 00:12:36,900
But following his teachings and through practice of wibasana meditation, many of these mental states can be seen or known or experienced.

102
00:12:36,900 --> 00:12:50,900
Many meditators come to see these mental states clearly through the practice of wibasana meditation.

103
00:12:50,900 --> 00:13:08,900
And Venerable Nagasana gave it seemingly that you take a handful of water from the ocean and it would be easier to see that this water is from this river and this water is from another river and so on.

104
00:13:08,900 --> 00:13:22,900
It is much more difficult to say that this is contact, this is feeling and so on when consciousness and its commitments arise, taking the same same object.

105
00:13:22,900 --> 00:13:42,900
So, Panya when it arises with consciousness and perception is a different dhamma, not mixed with the other competence.

106
00:13:42,900 --> 00:13:58,900
Like people say, working together, so each person is different from another one, but they come together and they do some job together.

107
00:13:58,900 --> 00:14:20,900
And what are its characteristic function manifestation and approximate cause, so we have to understand these things taught in a bhidhamma with reference to these four aspects characteristic function manifest is mode of manifestation and approximate cause.

108
00:14:20,900 --> 00:14:49,900
So, we will find a lot of these in this chapter and we make chapters understanding has to be characteristic of penetrating the individual essence of states, that means penetrating the nature of things, penetrating the individual nature of characteristic of things as well as penetrating the common characteristic of things.

109
00:14:49,900 --> 00:15:17,900
Each state has its own characteristic and if you watch closely and you will come to see the characteristic of these states and then there is the common characteristic of all states, all conditioned states and that is impermanence and so on.

110
00:15:17,900 --> 00:15:22,900
So, that also you see through Panya or understanding.

111
00:15:22,900 --> 00:15:29,900
So, Panya the characteristic of Panya is penetrating the individual essence of states or the nature of states.

112
00:15:29,900 --> 00:15:38,900
Its function is to abolish the darkness of delusion, it is like lightning, when lightning flashes, darkness is dispelled.

113
00:15:38,900 --> 00:15:52,900
So, dispelling of darkness or here, darkness of delusion or ignorance is its function, the delusion which conceives the individual essence of states.

114
00:15:52,900 --> 00:15:56,900
So, delusion or ignorance is compared to darkness.

115
00:15:56,900 --> 00:16:06,900
So, when there is darkness, we cannot see things in this room, if this room is dark, then we cannot see things in this room and when there is light, we can see things.

116
00:16:06,900 --> 00:16:19,900
So, darkness, just as darkness hides things from being seen, dilution or ignorance conceives the individual essence of states.

117
00:16:19,900 --> 00:16:27,900
We cannot, we do not see or know the individual essence of states because they are dilution or ignorance.

118
00:16:27,900 --> 00:16:38,900
It is manifested as non-delution. So, when you watch Panya itself, it will appear to you, to your mind as, it is non-delution.

119
00:16:38,900 --> 00:16:45,900
Because of the words one who is concentrated knows and sees correctly, its proximate cause is concentration.

120
00:16:45,900 --> 00:16:59,900
So, the Panya meant here that is Vipasana, Panya, and its proximate cause is what? Concentration.

121
00:16:59,900 --> 00:17:12,900
That is why I always say concentration is important and when there is no concentration, no Panya or penetration into the nature of things can arise.

122
00:17:12,900 --> 00:17:26,900
And then many kinds of Panya understanding. First one, then two, three, four, and so on.

123
00:17:26,900 --> 00:17:32,900
So, Panya is one, according to its characteristic of penetrating the individual essence.

124
00:17:32,900 --> 00:17:47,900
But it is two, page 482, paragraph 9. As regards the painful section, the mundane is dead associated with the mundane path.

125
00:17:47,900 --> 00:18:00,900
Mandain path means especially during Vipasana meditation. So, during Vipasana meditation, the factors of path arise in your mind.

126
00:18:00,900 --> 00:18:08,900
Right effort, right mindfulness, right concentration, and right understanding.

127
00:18:08,900 --> 00:18:16,900
So, mundane understanding is that associated with the mundane path. That means Vipasana meditation.

128
00:18:16,900 --> 00:18:24,900
And super mundane is that associated with the super mundane path. That is at the moment of enlightenment.

129
00:18:24,900 --> 00:18:36,900
At the moment of enlightenment, what is called path consciousness arises. And Panya accompanying that path consciousness is called here, super mundane Panya.

130
00:18:36,900 --> 00:18:46,900
So, it is of two kinds. In the second diet, that subject to kangas is that which is the object of kangas.

131
00:18:46,900 --> 00:18:56,900
So, we can just see mental defilements. Which can be the object of mental defilements. That free from kangas is not their object.

132
00:18:56,900 --> 00:19:08,900
So, which cannot be the object of mental defilements. So, this diet is the same in meaning as the mundane and super mundane.

133
00:19:08,900 --> 00:19:13,900
So, subject to kangas means mundane and free from kangas means super mundane.

134
00:19:13,900 --> 00:19:18,900
The same as that applies to the diet, subject to kangas and free from kangas and so on.

135
00:19:18,900 --> 00:19:33,900
Now, in the third diet, when a man wants to begin inside his understanding of the defining of the four in material aggregates is understanding as defining of mentality.

136
00:19:33,900 --> 00:19:50,900
So, defining of mental ready. That means clearly understanding the mental ready is defining of mental ready and clearly seeing or understanding the material aggregates understanding as defining of material ready.

137
00:19:50,900 --> 00:19:59,900
So, it is of two kinds. So, in short, it is understanding of mind and understanding of matter.

138
00:19:59,900 --> 00:20:19,900
In the fourth diet, understanding belong to two of the kinds of sensory or profitable consciousness and belonging to sixteen of the kinds of fat consciousness with four of the jhanas and the full full method is accompanied by joy.

139
00:20:19,900 --> 00:20:29,900
Now, what is sixteen? The four paths with the first jhanas and those with the second, third and full.

140
00:20:29,900 --> 00:20:57,900
Out of the five. Now, now here, the common data is using the five full method, five jhanas, not four jhanas.

141
00:20:57,900 --> 00:21:07,900
So, the first, second, third and full jhanas are accompanied by joy and the field is accompanied by equanimity.

142
00:21:07,900 --> 00:21:26,900
So, accompanied by joining jhanas up to the fold. Understanding belonging to two of the kinds of sensory or profitable consciousness and belonging to the remaining four kinds of fat consciousness that is a fifth jhanas is accompanied by equanimity.

143
00:21:26,900 --> 00:21:55,900
So, Panya accompanied by joy and Panya accompanied by equanimity. In the sense of fear, consciousness to accompanied by joy and to accompanied by equanimity.

144
00:21:55,900 --> 00:22:02,900
In the fifth jaya, understanding belonging to the first part is called plane of seeing.

145
00:22:02,900 --> 00:22:12,900
Now, here seeing means seeing a neighbor in the first place or initial seeing of neighbor.

146
00:22:12,900 --> 00:22:17,900
So, plane of seeing means that belonging to the first path.

147
00:22:17,900 --> 00:22:25,900
The first path is called seeing or dasana in Bali, because it sees Nivana first.

148
00:22:25,900 --> 00:22:32,900
Understanding belonging to the remaining three paths, that is, second and third path, they are called plane of development.

149
00:22:32,900 --> 00:22:44,900
Bhavana. So, it is of two kinds. And then, that regards the triads. Understanding a quiet without fearing from another is that consisting in what is reasoned.

150
00:22:44,900 --> 00:22:57,900
That means understanding from one's own thinking. Understanding which arises from one's own thinking and not heard from others or not having read books.

151
00:22:57,900 --> 00:23:03,900
And that is understanding, understanding, consisting in what is what is reasoned or what is thought.

152
00:23:03,900 --> 00:23:17,900
And then, second one is understanding, consisting in what is heard. That means understanding, obtaining through listening to others or through reading books.

153
00:23:17,900 --> 00:23:25,900
And the last one is understanding, consisting in development. Here, development means practice of meditation.

154
00:23:25,900 --> 00:23:35,900
So, understanding, obtained through the practice of meditation, is that which consists in development.

155
00:23:35,900 --> 00:23:49,900
So, there are three kinds of panya mentioned here. And then, there is quotation.

156
00:23:49,900 --> 00:24:06,900
In this quotation, about five lines down, that concerns ownership of deeds. That means understanding that beings have come only as their own.

157
00:24:06,900 --> 00:24:25,900
Or is in conformity with truth. That means just Vipasana. Because Vipasana helps us to see the true nature of pains. And so, in conformity with truth, here means just Vipasana.

158
00:24:25,900 --> 00:24:43,900
And then, paragraph 15, say, the understanding that occurs contingent upon Sanskrit states. That's a limited object. That means understanding that takes Sanskrit states as object.

159
00:24:43,900 --> 00:24:54,900
That is called understanding which has a limited object. And then understanding, taking the fine material sphere states for immaterial sphere states as an object is called.

160
00:24:54,900 --> 00:25:10,900
An object is called exalted upon having an exalted object. And this is mundane inside. That which occurs contingent upon Nibana has a measured subject. And that is super mundane inside.

161
00:25:10,900 --> 00:25:26,900
Now, inside means Vipasana. And Vipasana is mundane, not super mundane. When you reach the enlightenment stage and enlightenment moment, it becomes super mundane.

162
00:25:26,900 --> 00:25:54,900
So, in fact, there is no super mundane inside. Inside is always mundane. But here, by super mundane inside, the other meant the threat understanding and the standing of the path. And not Vipasana understanding.

163
00:25:54,900 --> 00:26:10,900
So, it is of three kinds. And then, it is increased, that is called improvement. And that is true full as the elimination of hum and the arousing of good.

164
00:26:10,900 --> 00:26:30,900
Here an improvement is killed in these, according as it says. So, this is skill and improvement. Skill in, say, development. The next one is skill in detriment.

165
00:26:30,900 --> 00:26:45,900
Means understanding, understanding the diminution of good and the arousing of hum. And the third one is what?

166
00:26:45,900 --> 00:27:09,900
But in either of these cases, any skill in means, then in causes of production means and cause of production are the same here. Means means just the cause of production of such and such things which skill august at that moment.

167
00:27:09,900 --> 00:27:25,900
Because on that occasion is what is called skill in means. I think you are familiar with the word upaia. It is now used a lot by people here. So, upaia.

168
00:27:25,900 --> 00:27:45,900
Upaia is translated as means. So, skill in means means. In Bali, it is upaia cosala. Skill in understanding how to do things at the given moment. And it is aroused on that occasion.

169
00:27:45,900 --> 00:28:10,900
So, it just popped up when there is some occasion. So, it is not pre meditated. So, it is of three kinds. And then the other in the full triad, insight understanding initiated by apprehending one's own aggregated, interpreting the internal.

170
00:28:10,900 --> 00:28:36,900
Here, interpreting means just reflecting upon or paying attention to that is internal. And external is taking other people's aggregate as object of meditation. And that is external.

171
00:28:36,900 --> 00:28:58,900
And both are interpreting the internal and external. And there is a footnote for the for the what a big new reason part is. And the other said at the end that it is interpretation or misinterpretation and insistence that being chosen here as a rendering.

172
00:28:58,900 --> 00:29:20,900
So, interpretation and just means something like a view on a given object. And then tetraps, full kinds of knowledge or full kinds of understanding.

173
00:29:20,900 --> 00:29:32,900
Understanding of suffering, first noble truth, understanding of origin of suffering, second noble truth, understanding of cessation of suffering and understanding of the way leading to the cessation of suffering.

174
00:29:32,900 --> 00:29:48,900
And then the next tetraps is four kinds of discriminations. And these four kinds of discriminations are mentioned in the commentaries very often.

175
00:29:48,900 --> 00:30:04,900
And sometimes when the enlightenment of a person is described and the books would say he gained enlightenment along with the four discriminations.

176
00:30:04,900 --> 00:30:30,900
That is, this is a special knowledge. But you don't have to practice in a special way, but you just practice as, say, we pass on our meditation. And then when you become enlightened, these four kinds of knowledge automatically automatically come to you.

177
00:30:30,900 --> 00:30:42,900
So these are the called food discriminations, or participant videos. Knowledge about meaning is the discrimination of meaning. Knowledge about law is the discrimination of law.

178
00:30:42,900 --> 00:30:52,900
Knowledge about enunciation of language dealing with meaning and law is the discrimination of language. Knowledge about kinds of knowledge. Knowledge about knowledge.

179
00:30:52,900 --> 00:30:59,620
is discrimination of pospicuity, so there are four kinds of discriminations.

180
00:31:01,300 --> 00:31:05,220
Here in meaning, Arthur is briefly a term for the fruit of the cause.

181
00:31:05,860 --> 00:31:11,060
So, the pali word is atta, and the pali word atta can mean

182
00:31:12,100 --> 00:31:20,580
a result of purpose, and also fruit, also meaning,

183
00:31:20,580 --> 00:31:30,900
and also, yes, also a result. So, if you translate it as just meaning,

184
00:31:32,980 --> 00:31:46,260
it may create some misunderstanding, but it is difficult to translate this word into any language,

185
00:31:46,260 --> 00:31:52,180
because as we will see, the word atta here means five things,

186
00:31:53,620 --> 00:32:01,700
anything conditionally produced, that is one, two nibana, three, the meaning of what is spoken,

187
00:32:02,660 --> 00:32:10,260
that means the meaning of words, and four, result of comma, and five functional consciousness

188
00:32:10,260 --> 00:32:18,020
should be understood as meaning. So, one you say, knowledge about meaning

189
00:32:22,260 --> 00:32:26,820
discrimination of meaning means knowledge about these five things.

190
00:32:28,020 --> 00:32:32,740
When anyone reviews that meaning, any knowledge of his falling within the category,

191
00:32:32,740 --> 00:32:38,260
concerned with meaning is the discrimination of meaning. So, discrimination of meaning means

192
00:32:38,260 --> 00:32:45,940
understanding of the things conditionally produced, understanding of nibana,

193
00:32:45,940 --> 00:32:50,980
understanding of a meaning of a word, understanding of the result of the comma,

194
00:32:50,980 --> 00:32:57,060
and understanding of the functional consciousness. And then, second one is dhamma law.

195
00:32:57,060 --> 00:33:03,780
It is briefly a term for condition. For sense, the condition necessitates,

196
00:33:03,780 --> 00:33:09,220
whatever it may be, makes it over allows it to help and adjust their book called law or dhamma.

197
00:33:11,380 --> 00:33:20,740
So, discrimination of law means understanding of the condition or understanding of the cause,

198
00:33:22,100 --> 00:33:29,140
but in particular, the five things namely are meant by the word dhamma here, one, any cause

199
00:33:29,140 --> 00:33:36,260
that produces fruit, two, the noble fat. It is a cause for a cause of the fruition,

200
00:33:36,820 --> 00:33:41,860
three, what is spoken? That means the words of language, and four, what is profitable,

201
00:33:41,860 --> 00:33:49,380
cusala, and five, what is unprofitable, a cusala should be understood as law. So, by the word dhamma,

202
00:33:49,380 --> 00:33:54,660
these five things are meant. When anyone reviews that knowledge and that law, any knowledge of his

203
00:33:54,660 --> 00:34:02,580
falling within the category concerned with law, it is discrimination of law. So, discrimination of

204
00:34:04,580 --> 00:34:10,500
condition or discrimination of cause or understanding of causes, and this

205
00:34:10,500 --> 00:34:16,340
meaning is shown in the dhamma by the falling analysis. So, there is a quotation from the second

206
00:34:16,340 --> 00:34:25,460
book of the dhamma. Now, let us go to 25, paragraph 25, knowledge about enunciation of language

207
00:34:25,460 --> 00:34:34,420
dealing with meaning and law. That is the language that is individual essence. Now, the word

208
00:34:34,420 --> 00:34:44,980
subhava is translated, normally translated as individual essence, but here the word subhava

209
00:34:44,980 --> 00:34:58,020
means is explained in the sub commentary as something like correct or not,

210
00:35:01,460 --> 00:35:12,100
that which does not change or that which does not deteriorate. That is called the subhava,

211
00:35:12,100 --> 00:35:18,420
neurotic and parley. The usage that has no exceptions, that means that is definite,

212
00:35:19,540 --> 00:35:26,340
that is not ambiguous, something like that, and deals with that meaning and that law. Any knowledge

213
00:35:26,340 --> 00:35:30,820
falling within the category concerned with the enunciation of that, with speaking with the

214
00:35:30,820 --> 00:35:35,780
utterance of that concerned with the root speech of all beings, the margarine language that is

215
00:35:35,780 --> 00:35:45,380
individual essence. Now, in theorem of Buddhism, the language of margarine is said to be the

216
00:35:45,380 --> 00:35:54,420
root speech of all beings, the original language of all beings. So, the belief of theorem of

217
00:35:54,420 --> 00:36:01,620
the Buddhist is that the margarine language or the parley language as we know it now, parley

218
00:36:01,620 --> 00:36:10,900
language is the original language of all beings. Only after many, many years, different people

219
00:36:10,900 --> 00:36:19,380
develop different dialects and then they became languages. So, this is the belief of ancient, ancient

220
00:36:19,380 --> 00:36:44,180
teachers. Hindu. So, it is a margarine language is taken to be parley. I think the present day parley

221
00:36:44,180 --> 00:36:54,980
is the closest to the language used by the Buddha. It is difficult to say that it is the language

222
00:36:54,980 --> 00:37:02,100
of the Buddha, because we really don't know with which language the Buddha used, but Buddha used

223
00:37:02,100 --> 00:37:12,580
the language which was current at its time and which was understood by all people, not by

224
00:37:12,580 --> 00:37:21,700
high-class people only, because once two Brahmins who became monks asked him to turn his teachings

225
00:37:21,700 --> 00:37:32,900
into Vedic Sanskrit and Buddha rejected them. Buddha said, my demise for all people, not for a selected

226
00:37:32,900 --> 00:37:42,420
few. So, he used the language with us understood by all people, the common language, basically.

227
00:37:42,420 --> 00:37:49,860
So, we now think that actually the language of India would share many languages in the

228
00:37:49,860 --> 00:37:56,900
India now and very dialects, Buddha traveled greatly throughout India and maybe at that time there

229
00:37:56,900 --> 00:38:07,540
was more common. We don't know, but there may be different languages in different parts,

230
00:38:07,540 --> 00:38:17,060
or maybe dialects in different parts of the country. And the language Buddha used maybe

231
00:38:17,060 --> 00:38:28,660
understood by all people, where he roamed, and that language is said to be the mulebhasa,

232
00:38:28,660 --> 00:38:43,300
we call it mulebhasa, root speech of all beings. In other words, the language of law, as soon

233
00:38:43,300 --> 00:38:50,020
as it hears its spoken, pronouns, other notes. This is the individual essence language, this is not

234
00:38:50,020 --> 00:38:57,220
the individual essence language. That means it is a correct language, it is not a correct language,

235
00:38:57,220 --> 00:39:06,500
something like that. So, it was in who has got this kind of discrimination, knows that this is

236
00:39:06,500 --> 00:39:14,180
correct, and this is not, it is a prophetically correct, and this is not. So, the example given here is,

237
00:39:14,180 --> 00:39:19,300
one who has reached the discrimination of language knows on hearing the words faço,

238
00:39:19,300 --> 00:39:25,140
within a etc. That this is the individual essence language, but that means this is the correct language.

239
00:39:25,860 --> 00:39:32,180
And on hearing faça, we don't know, etc. He knows that this is not the individual essence

240
00:39:32,180 --> 00:39:42,820
language, it is not the correct word or correct language, because the word faça is masculine

241
00:39:42,820 --> 00:39:50,740
gender. So, it is in the nominative case, it must be faço, and it can never be faça in nominative

242
00:39:50,740 --> 00:39:56,900
case. So, when you say faça, you know this is wrong, this is chromatically wrong. The word

243
00:39:56,900 --> 00:40:04,660
we don't know belongs to feminine gender, it is never in the masculine gender. So, if you say

244
00:40:04,660 --> 00:40:16,180
we don't know, then you put, you put a feminine word into masculine gender, and so it is wrong.

245
00:40:16,180 --> 00:40:27,780
So, if you do not know Pali, then you try to get enlightenment and get these four kinds of

246
00:40:27,780 --> 00:40:34,980
discrimination, and then you will automatically understand Pali. That is what what, what is meant here.

247
00:40:36,660 --> 00:40:45,300
So, Pali language is said to be, said to be the root speech of all beings, and said to be the

248
00:40:45,300 --> 00:40:59,380
best of the languages. Now, the language we call Pali is never mentioned as Pali language

249
00:41:00,180 --> 00:41:08,500
in the commentaries. In the commentaries, it is referred to as marketer language or root language.

250
00:41:08,500 --> 00:41:17,540
The word Pali simply means the text as against commentaries and sub commentaries.

251
00:41:19,940 --> 00:41:29,060
But later on, since it is the language in which the text were recorded, we call it a Pali language.

252
00:41:29,060 --> 00:41:40,020
So, the word Pali language is of recent origin, not used by commentaries and sub commentaries.

253
00:41:40,020 --> 00:41:56,020
So, and what they used is the word mulabhasa or magatah or magatibasa, the language of the magatah,

254
00:41:56,020 --> 00:42:02,740
magatah country. It is, it is maybe like a state in this country.

255
00:42:06,420 --> 00:42:14,740
During the time of the border, there was 16, we call them 16 countries, but not 16 countries, 16

256
00:42:15,700 --> 00:42:19,620
states or districts. So, magatah was one of them.

257
00:42:19,620 --> 00:42:27,380
And knowledge about kinds of knowledge, that is knowledge of knowledge. So, that is

258
00:42:28,740 --> 00:42:30,820
discrimination of persecuity.

259
00:42:35,140 --> 00:42:47,780
And these four kinds of discrimination, we can make them

260
00:42:47,780 --> 00:42:56,340
clear, we can make them sharp, or we can make them, we can make ourselves

261
00:42:57,860 --> 00:43:05,140
adepts in these four by five means. And these five means are given in paragraph 28.

262
00:43:11,700 --> 00:43:16,980
And though they come into the categories of the two planes, that they are nevertheless distinguishable

263
00:43:16,980 --> 00:43:25,220
in five aspects, that is to see as achievement and so on. Actually, they can be brought to

264
00:43:25,220 --> 00:43:32,180
perfection by these five things, by achievement, that means by enlightenment, by achievement,

265
00:43:32,180 --> 00:43:39,540
by mastery of scriptures, by hearing, by questioning, and by prior effort.

266
00:43:39,540 --> 00:43:50,980
So, achievement is the reaching of our handship, but it is the best dimension here.

267
00:43:51,860 --> 00:43:57,380
So, reaching of other stages of enlightenment is also meant here.

268
00:43:57,380 --> 00:44:02,180
Mastery of scriptures is must be of Buddha's word. Hearing is learning the damn carefully

269
00:44:02,180 --> 00:44:07,460
and then attentively. Questioning is discussion of naughty passages and explanatory

270
00:44:07,460 --> 00:44:12,340
passages and historical commentaries and so on. Prior effort is devotion to insight in the dispensation

271
00:44:13,060 --> 00:44:21,460
of former Buddhas. So, it is like a barometer. Up to the vicinity of the stages of conformity

272
00:44:21,460 --> 00:44:27,460
and change of lineage by one who has practiced the duty of going with the meditation subject

273
00:44:27,460 --> 00:44:36,500
on Armstrong and coming back with it. Now, months have described as of different kinds and there are

274
00:44:36,500 --> 00:44:43,380
months who take meditation even when they go to the village for arms and they take that

275
00:44:43,380 --> 00:44:50,180
meditation to the monastery. That means when they go to the village, they go with meditation

276
00:44:50,180 --> 00:44:55,140
and when they come back from village, they come back with meditation. So, such persons. So,

277
00:44:56,180 --> 00:45:04,180
they have practiced meditation in this way. So, that is what is made here by the duty of going

278
00:45:04,180 --> 00:45:14,820
with the meditation subject on Armstrong and coming back with it. And they may have reached

279
00:45:15,540 --> 00:45:24,100
the stages of we bus an hour. Just short of enlightenment. If they are enlightened, they don't have to

280
00:45:25,940 --> 00:45:32,340
they don't have to worry about practicing again to gain enlightenment. So, here up to the

281
00:45:32,340 --> 00:45:40,660
visanity of the stages of conformity and change of lineage simply means just short of enlightenment.

282
00:45:43,380 --> 00:45:48,420
So, those who have practiced we bus an hour division in the former Buddhas. That means in the

283
00:45:48,420 --> 00:45:56,580
in the former life just short of enlightenment. So, that is called prior effort. So, such people

284
00:45:56,580 --> 00:46:01,940
may get enlightenment in this life and may get these four kinds of discriminations.

285
00:46:05,620 --> 00:46:13,540
And then some other set in a different way but more or less the same. So, prior effort,

286
00:46:13,540 --> 00:46:29,460
great learning, and then dialects, scriptures, questioning, achievements, with…

