1
00:00:00,000 --> 00:00:16,560
Okay, we're broadcasting right, sorry, we're a little bit behind schedule, some technical

2
00:00:16,560 --> 00:00:45,560
difficulties.

3
00:00:45,560 --> 00:00:52,960
We had audio, some things funny with our audio set up, so I'm going to be doing this alone

4
00:00:52,960 --> 00:01:00,760
and Robin's here, but she's going to be silent, because it just wasn't working out.

5
00:01:00,760 --> 00:01:07,160
Okay, I think we are set up to go, ah, ooh, mmm.

6
00:01:07,160 --> 00:01:13,120
See, I didn't start the Q&A panel and now I can't start it because we've already started

7
00:01:13,120 --> 00:01:15,120
broadcasting.

8
00:01:15,120 --> 00:01:19,960
Okay, I have to reset this, that means we're going to have to start a new hangout.

9
00:01:19,960 --> 00:01:25,560
So if you're watching this one, you're going to have to cancel watching this one and you're

10
00:01:25,560 --> 00:01:27,560
going to have to wait for the new one.

11
00:01:27,560 --> 00:01:31,240
It's a whole different video, it's going to be a whole different video page.

12
00:01:31,240 --> 00:01:37,280
I have to start over because otherwise we can't have the Q&A, so bear with us and we'll get

13
00:01:37,280 --> 00:01:44,320
this set up in a second.

14
00:02:07,280 --> 00:02:08,280
.

15
00:02:08,280 --> 00:02:24,280
.

16
00:02:38,280 --> 00:02:39,280
.

17
00:03:08,280 --> 00:03:09,280
.

18
00:03:38,280 --> 00:03:39,280
.

19
00:04:08,280 --> 00:04:09,280
.

20
00:04:38,280 --> 00:04:39,280
.

21
00:05:08,280 --> 00:05:09,280
.

22
00:05:38,280 --> 00:05:39,280
.

23
00:06:08,280 --> 00:06:09,280
.

24
00:06:38,280 --> 00:06:39,280
.

25
00:06:39,280 --> 00:06:40,280
.

26
00:06:40,280 --> 00:06:41,280
.

27
00:06:41,280 --> 00:06:42,280
.

28
00:06:42,280 --> 00:06:43,280
.

29
00:06:43,280 --> 00:07:08,280
.

30
00:07:08,280 --> 00:07:09,280
.

31
00:07:09,280 --> 00:07:10,280
.

32
00:07:10,280 --> 00:07:11,280
.

33
00:07:11,280 --> 00:07:12,280
.

