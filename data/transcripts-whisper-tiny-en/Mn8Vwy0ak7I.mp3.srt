1
00:00:00,000 --> 00:00:10,000
Hello and welcome back to our study of the Dhamupada. Today we continue on with verse 125 which reads as follows.

2
00:00:30,000 --> 00:00:52,080
So, which means who should attack or offend against lucity, a person who is without fault

3
00:00:52,080 --> 00:01:06,160
up a lutasa in the rasa? One bhosa, a man or a person who is soon happier and unanginate without fault

4
00:01:06,160 --> 00:01:09,080
without blame.

5
00:01:09,080 --> 00:01:30,080
Tameva balang pate bhapam. Such a that evil pate, pate goes back, goes back upon Tameva balang, that fool.

6
00:01:30,080 --> 00:01:47,080
Sukumura jho, atiwatam wa kipto, just as raja, a dust, that is sukum, that is fine, just as a fine dust,

7
00:01:47,080 --> 00:01:55,080
that is thrown against the wind. So if you throw dust against the wind, it comes back into your face.

8
00:01:55,080 --> 00:02:06,080
In the same way evil, done to someone who is blameless, comes back to bite you.

9
00:02:06,080 --> 00:02:14,080
So, quite memorable, memorable words.

10
00:02:14,080 --> 00:02:28,080
One of those things for us to keep in mind is a description of the law of karma. That is what this refers to.

11
00:02:28,080 --> 00:02:42,080
But it is even more because the idea is that evil doesn't harm the one who is the object of evil, evil harms the one who performs the people.

12
00:02:42,080 --> 00:02:50,080
And sometimes in seeming magical ways, which is sort of what this story behind this verse is about.

13
00:02:50,080 --> 00:02:58,080
So the story goes that there was a hunter named Goka.

14
00:02:58,080 --> 00:03:08,080
And he was, he had, this hunter was accompanied by a pack of hounds.

15
00:03:08,080 --> 00:03:19,080
So when he would go hunting, he would take dogs with him and the dogs would pick up his prey or maybe even help him kill it, and drag it down for him to kill.

16
00:03:19,080 --> 00:03:22,080
And so he had trained them to be quite vicious.

17
00:03:22,080 --> 00:03:29,080
One day when he was doing his hunting in the forest, he came across a monk.

18
00:03:29,080 --> 00:03:35,080
And somehow he had this idea that seeing a monk was a bad open.

19
00:03:35,080 --> 00:03:44,080
Seeing a monk in the morning was that this idea had been spread about maybe by enemies of monks or enemies of Buddhism.

20
00:03:44,080 --> 00:03:49,080
Anyway, he thought this was a bad open to see a monk.

21
00:03:49,080 --> 00:03:59,080
And so the monk went on his way to, for arms in the city, and the hunter went on his way to try and catch prey.

22
00:03:59,080 --> 00:04:03,080
And sure enough, the whole day he caught nothing.

23
00:04:03,080 --> 00:04:11,080
Meanwhile, the monk came back and he had his lunge and was perhaps meditating in the forest.

24
00:04:11,080 --> 00:04:20,080
And the hunter, on his way back to the forest, had been caught sight of the monk again.

25
00:04:20,080 --> 00:04:22,080
And was furious.

26
00:04:22,080 --> 00:04:28,080
I thought this monk, the reason, this monk is the reason why I got nothing somehow he had this idea.

27
00:04:28,080 --> 00:04:40,080
And so he yelled at the monk and accused the monk of being the cause of his bad luck.

28
00:04:40,080 --> 00:04:45,080
And he brought his hounds out and he was ready to set his hounds on the monk.

29
00:04:45,080 --> 00:04:49,080
And the monk begged him, pleaded for him not to do such a horrible thing.

30
00:04:49,080 --> 00:04:54,080
That he was innocent, that he had not done anything against the hunter.

31
00:04:54,080 --> 00:05:01,080
Part of it may have been because Buddhist monks were known to be against killing.

32
00:05:01,080 --> 00:05:03,080
They caught people to give up killing.

33
00:05:03,080 --> 00:05:11,080
And so as a result, there was this idea that somehow they were bad luck as well.

34
00:05:11,080 --> 00:05:21,080
Because it was something that they believed was wrong.

35
00:05:21,080 --> 00:05:29,080
Anyway, this hunter refused to listen to the monk and set his dogs on the monk.

36
00:05:29,080 --> 00:05:36,080
So immediately they were having climbed up a tree and was standing on this one of the branches.

37
00:05:36,080 --> 00:05:40,080
And so what the hunter did, he was so angry.

38
00:05:40,080 --> 00:05:42,080
He was really out to get this monk.

39
00:05:42,080 --> 00:05:47,080
And so he took it one of his arrows and he poked up at the monk's feet.

40
00:05:47,080 --> 00:05:52,080
And first the monk's right foot and the monk lifted up his right foot and put his left foot down

41
00:05:52,080 --> 00:05:57,080
and stabbed him under poked his left foot and really gouged his feet.

42
00:05:57,080 --> 00:06:02,080
And so then the monk pulled up both feet, but he was in so much pain.

43
00:06:02,080 --> 00:06:08,080
And he started to lose mindfulness awareness of what he was doing.

44
00:06:08,080 --> 00:06:17,080
And he dropped his outer robe, the big robe that would act as a blanket.

45
00:06:17,080 --> 00:06:23,080
And it fell on the hunter and covered him.

46
00:06:23,080 --> 00:06:29,080
And immediately the dogs turned and looked at the hunter and thought the monk has fallen out of the tree.

47
00:06:29,080 --> 00:06:34,080
And they turned on the hunter and ripped him to shreds and killed him.

48
00:06:34,080 --> 00:06:44,080
Thinking they were following their master's orders because they smelled this robe.

49
00:06:44,080 --> 00:06:50,080
And the monk looking down at what was going, he picked up a stick and he threw it at the dogs.

50
00:06:50,080 --> 00:06:54,080
The dogs looked up and saw that the monk was still in the tree.

51
00:06:54,080 --> 00:07:04,080
And the scampered off into the forest confused or maybe embarrassed at how foolish they had been.

52
00:07:04,080 --> 00:07:07,080
That's the story.

53
00:07:07,080 --> 00:07:10,080
And the monk actually felt bad about this.

54
00:07:10,080 --> 00:07:14,080
He thought, well, it's because of me that this hunter died.

55
00:07:14,080 --> 00:07:16,080
It was really my fault.

56
00:07:16,080 --> 00:07:20,080
It was because I dropped his robe on him because I was on mindfulness.

57
00:07:20,080 --> 00:07:23,080
Do I have any fault in that?

58
00:07:23,080 --> 00:07:28,080
And of course the Buddha said, no, no, be who you are, blameless.

59
00:07:28,080 --> 00:07:32,080
And moreover, he said, this isn't the first time.

60
00:07:32,080 --> 00:07:40,080
And he told the story of the past that apparently in the past life, this guy had the same sort of thing happened.

61
00:07:40,080 --> 00:07:45,080
And it was a cruel, terrible person and the same sort of thing happened.

62
00:07:45,080 --> 00:07:51,080
He was a physician and he was trying to sell his cures and sell his services.

63
00:07:51,080 --> 00:07:54,080
And no one was buying it and no one was sick.

64
00:07:54,080 --> 00:08:01,080
And he thought, well, maybe I'll just make people sick and get them to, then they'll need me.

65
00:08:01,080 --> 00:08:03,080
They'll need my help if they're sick.

66
00:08:03,080 --> 00:08:07,080
And so he took a poisonous snake and he stuck it into the tree.

67
00:08:07,080 --> 00:08:14,080
And he told these boys to go and fetch him that there was a bird in there in this hole.

68
00:08:14,080 --> 00:08:18,080
And they should go and fetch it for him.

69
00:08:18,080 --> 00:08:24,080
And so one of the boys stuck his hand in the tree and grabbed the snake.

70
00:08:24,080 --> 00:08:26,080
Thinking it was this bird that he was going to catch.

71
00:08:26,080 --> 00:08:32,080
And when he realized it was a snake, threw it, freaked out and was really quickly.

72
00:08:32,080 --> 00:08:41,080
And it hit the position in the head and wrapped around his head and bit him and killed him.

73
00:08:41,080 --> 00:08:44,080
Kind of silly stories.

74
00:08:44,080 --> 00:08:51,080
But the story of the hunter and the monk in the tree is a memorable one.

75
00:08:51,080 --> 00:08:54,080
How evil can we all roam?

76
00:08:54,080 --> 00:08:56,080
How are deeds?

77
00:08:56,080 --> 00:09:00,080
Sometimes turn back upon us.

78
00:09:00,080 --> 00:09:05,080
Now, there are different reasons why we can say this happens.

79
00:09:05,080 --> 00:09:09,080
I mean, the most obvious reason for our evil deeds to bring evil upon ourselves

80
00:09:09,080 --> 00:09:12,080
is the effect that they have on our minds.

81
00:09:12,080 --> 00:09:20,080
So anger, and greed, and delusion to three developments make you careless.

82
00:09:20,080 --> 00:09:24,080
When you're obsessed with something or when you're angry,

83
00:09:24,080 --> 00:09:31,080
it's bad karma first and foremost because of how it affects your mind.

84
00:09:31,080 --> 00:09:38,080
Because of how it changes you and changes the actions and the choices that you make.

85
00:09:38,080 --> 00:09:43,080
Furthermore, it does affect the world around you.

86
00:09:43,080 --> 00:09:48,080
The second way it does, I think, is in how others look at you.

87
00:09:48,080 --> 00:09:52,080
So in this case, there were no other people involved.

88
00:09:52,080 --> 00:09:59,080
But by disrupting the monk, he brought upon himself, at least you could say,

89
00:09:59,080 --> 00:10:05,080
some sort of chaos in which many things are possible.

90
00:10:05,080 --> 00:10:14,080
And when you attack someone, sometimes they freak out and it disturbs the order.

91
00:10:14,080 --> 00:10:17,080
But you see, I'm trying to get to the point where you could actually say,

92
00:10:17,080 --> 00:10:19,080
well, this is a result of his bad karma.

93
00:10:19,080 --> 00:10:22,080
Because it doesn't seem like it is.

94
00:10:22,080 --> 00:10:24,080
It seems like it was just a freak chance.

95
00:10:24,080 --> 00:10:28,080
And nine out of ten times, or ninety nine out of a hundred times,

96
00:10:28,080 --> 00:10:36,080
or nine hundred and ninety nine out of a thousand times, the hunter would have killed the monk in the tree.

97
00:10:36,080 --> 00:10:43,080
But what we want to say is that there is potentially things protecting people who are good,

98
00:10:43,080 --> 00:10:44,080
people who are blameless.

99
00:10:44,080 --> 00:10:46,080
I don't know that you can, actually.

100
00:10:46,080 --> 00:10:49,080
I don't know that it's actually proper and true to say that.

101
00:10:49,080 --> 00:10:54,080
We want to, and Buddhist like to try and say that, that there are things protecting you.

102
00:10:54,080 --> 00:10:56,080
I'm not so sure.

103
00:10:56,080 --> 00:11:01,080
I would agree that there are probably beings, like, who knows?

104
00:11:01,080 --> 00:11:10,080
Maybe there was some angel or spirit involved with this that pulled the robe off the monk and dropped it on the hunter.

105
00:11:10,080 --> 00:11:13,080
You know, that makes sense to me if such beings exist.

106
00:11:13,080 --> 00:11:18,080
Of course, many, many people think it's ridiculous that such beings would exist.

107
00:11:18,080 --> 00:11:23,080
I'd agree that it seems kind of fair-fetched that such a being would just be there and get involved.

108
00:11:23,080 --> 00:11:25,080
But hey, who knows?

109
00:11:25,080 --> 00:11:26,080
There's that kind of thing.

110
00:11:26,080 --> 00:11:28,080
And so that's a direct result of karma.

111
00:11:28,080 --> 00:11:34,080
People don't, other beings don't appreciate when you're cruel to people who don't deserve it.

112
00:11:34,080 --> 00:11:39,080
And that sort of thing really is a part of karma.

113
00:11:39,080 --> 00:11:46,080
Good people attract others who would protect them, attract good friends.

114
00:11:46,080 --> 00:11:53,080
And so if you try to harm a harmless being, you're less likely to succeed,

115
00:11:53,080 --> 00:11:56,080
just because they're surrounded by good people.

116
00:11:56,080 --> 00:12:13,080
But I think you could also argue that the power of goodness has some sort of reverberations in the universe,

117
00:12:13,080 --> 00:12:16,080
like it sets things up in a certain way.

118
00:12:16,080 --> 00:12:18,080
I think you could argue that.

119
00:12:18,080 --> 00:12:25,080
I think you could point to ways in which it's true that a person who does lots of good deeds,

120
00:12:25,080 --> 00:12:37,080
a person who's never done evil deeds, is somehow in a different sort of train or groove,

121
00:12:37,080 --> 00:12:40,080
you might say, from a person who is intent upon evil.

122
00:12:40,080 --> 00:12:45,080
Like they're so opposite that they're almost living in different universes.

123
00:12:45,080 --> 00:12:49,080
And it's very hard for them to come together and know it's possible.

124
00:12:49,080 --> 00:12:55,080
But I think you could point to that as being a reason why it takes a really great amount of effort

125
00:12:55,080 --> 00:12:57,080
to harm someone who is harmless.

126
00:12:57,080 --> 00:13:00,080
I don't want to say it's certainly not magic.

127
00:13:00,080 --> 00:13:03,080
But I think there's much, much more going on.

128
00:13:03,080 --> 00:13:14,080
And we often will, it's common for people to underestimate the power of goodness and the power of evil.

129
00:13:14,080 --> 00:13:22,080
Evil is something that shrinks inward or it comes back upon you.

130
00:13:22,080 --> 00:13:26,080
Good is something that's expensive and it spreads.

131
00:13:26,080 --> 00:13:33,080
So you can't spread evil is not the same as spreading good.

132
00:13:33,080 --> 00:13:38,080
It's much more likely to come back upon you.

133
00:13:38,080 --> 00:13:43,080
But I think at any rate I would argue that there's room for that.

134
00:13:43,080 --> 00:13:49,080
I think it's something that is very hard to find proof or evidence even for.

135
00:13:49,080 --> 00:13:55,080
But I would say that there's room for the idea that goodness protects good people.

136
00:13:55,080 --> 00:14:04,080
And people who try to do evil towards good people for various reasons have a harder time of it.

137
00:14:04,080 --> 00:14:07,080
And it's much more likely to come back and bite them in the face.

138
00:14:07,080 --> 00:14:12,080
I mean, absolutely even when they get away with it, it's far worse.

139
00:14:12,080 --> 00:14:26,080
And there's no question that it eventually comes back far, far worse on the person who's done an evil deal towards someone who doesn't deserve it.

140
00:14:26,080 --> 00:14:34,080
I mean, who hasn't done anything to instigate it, who is a blameless.

141
00:14:34,080 --> 00:14:53,080
Like if this monk had gone around setting animals free, well then you could say maybe he's somewhat middle or if he had purposefully wished or if he had done everything he couldn't to ruin this hunter to harm him.

142
00:14:53,080 --> 00:15:03,080
If he had done something to warrant even little bit of malice then it would be different.

143
00:15:03,080 --> 00:15:08,080
You could argue and it would be less of a weighty crime.

144
00:15:08,080 --> 00:15:10,080
But to harm someone who's harmless.

145
00:15:10,080 --> 00:15:14,080
I mean, you know it in your bones, you don't have to be Buddhist to believe this.

146
00:15:14,080 --> 00:15:21,080
It's just far more horrific to harm someone who has done nothing to deserve it.

147
00:15:21,080 --> 00:15:35,080
And for that reason, for sure it's going to affect your mind and make you blind to the dangers that you're facing and blind to your actions and blind to the circumstances and so on.

148
00:15:35,080 --> 00:15:44,080
But I think you could argue more over that it's such a powerful act that it jars with the universe.

149
00:15:44,080 --> 00:15:47,080
As a result is harder to perform.

150
00:15:47,080 --> 00:16:04,080
I'd like to think that there are things that protect people's lives just aspects of the universe that are fit together in such a way that it's not as easy to kill someone as we might think, especially if they don't deserve it.

151
00:16:04,080 --> 00:16:16,080
But the idea that the some extent most killing, most deeds for good or for evil have something to do with past karma, right?

152
00:16:16,080 --> 00:16:19,080
The idea that there is some sort of connection there.

153
00:16:19,080 --> 00:16:27,080
I don't think all, I think it seems possible to create a new karma with someone, so if someone is innocent you can harm them.

154
00:16:27,080 --> 00:16:38,080
But it's a very weighty karma, but you're starting something new and it's quite possible that they will come back and harm you even unintentionally, like in this case.

155
00:16:38,080 --> 00:16:41,080
Usually it takes lifetimes to do that.

156
00:16:41,080 --> 00:16:54,080
But anyway, these are aspects of karma that are kind of magical and hard to understand or hard to believe or people don't think of them as true.

157
00:16:54,080 --> 00:16:57,080
But this isn't so important for our practice.

158
00:16:57,080 --> 00:17:04,080
What's important for our practice is as with all of these sorts of stories, the consequences.

159
00:17:04,080 --> 00:17:11,080
At no question there are consequences to our evil deeds and our good deeds.

160
00:17:11,080 --> 00:17:14,080
I think that's exemplified here both.

161
00:17:14,080 --> 00:17:19,080
If you're a good person, you don't deserve to be hard.

162
00:17:19,080 --> 00:17:21,080
It's harder for people to harm you.

163
00:17:21,080 --> 00:17:23,080
It's harder for people to want to hurt you.

164
00:17:23,080 --> 00:17:29,080
It takes a really base and evil person to do it.

165
00:17:29,080 --> 00:17:41,080
And you've got to wonder whether this monk was pleading with this hunter to stop for his own benefit or for the benefit of the hunter.

166
00:17:41,080 --> 00:17:52,080
Because certainly even death by dog, by wild rabid dogs, is preferable.

167
00:17:52,080 --> 00:18:01,080
We don't think of it this way, but it's actually far preferable than to torture an innocent person.

168
00:18:01,080 --> 00:18:05,080
Because the Buddha said many things like this.

169
00:18:05,080 --> 00:18:13,080
He said, you should rather be killed and tortured than to do an evil deed.

170
00:18:13,080 --> 00:18:18,080
Because being killed and tortured doesn't lead you to a bad future.

171
00:18:18,080 --> 00:18:27,080
It doesn't hurt your mind. It doesn't affect you in the way that evil does.

172
00:18:27,080 --> 00:18:38,080
It's a big part of meditation that a beginner has to come to understand that pain in the meditation.

173
00:18:38,080 --> 00:18:44,080
When you're sitting in meditating and you feel pain, pain is not your enemy.

174
00:18:44,080 --> 00:18:47,080
Unpleasantness of all kinds.

175
00:18:47,080 --> 00:18:58,080
Whether it's itching or heat or loud noise or thoughts, nightmares, visions, etc.

176
00:18:58,080 --> 00:19:01,080
This is not the enemy, this is not the problem.

177
00:19:01,080 --> 00:19:07,080
The problem is evil of what we call unwholesumness.

178
00:19:07,080 --> 00:19:11,080
So greed, anger and delusion, these are problems.

179
00:19:11,080 --> 00:19:17,080
They call, they bring about things like pain and stress and suffering.

180
00:19:17,080 --> 00:19:22,080
So coming to see that difference is very important.

181
00:19:22,080 --> 00:19:28,080
To see that it's the evil that we have a problem with.

182
00:19:28,080 --> 00:19:36,080
So this quote is describing a very extreme sort of karma.

183
00:19:36,080 --> 00:19:41,080
The evil deed that is performed towards someone who doesn't deserve it.

184
00:19:41,080 --> 00:19:47,080
But it brings up all these issues.

185
00:19:47,080 --> 00:19:55,080
Who is the person who really receives the effect of a deed?

186
00:19:55,080 --> 00:20:00,080
If you try to harm someone, well they might be hurt or even killed.

187
00:20:00,080 --> 00:20:01,080
But that's it.

188
00:20:01,080 --> 00:20:06,080
It's actually quite insignificant compared to the evil that comes back upon you.

189
00:20:06,080 --> 00:20:09,080
So this is throwing dust in the wind.

190
00:20:09,080 --> 00:20:12,080
If you throw dust in the wind it comes back on you.

191
00:20:12,080 --> 00:20:14,080
Evil deeds are very much like that.

192
00:20:14,080 --> 00:20:16,080
Don't think of it, right?

193
00:20:16,080 --> 00:20:22,080
We think if you could get away with it, hurting someone else really bad for the person who you hurt or killed.

194
00:20:22,080 --> 00:20:25,080
It's actually very much the other way.

195
00:20:25,080 --> 00:20:26,080
That's what this verse teaches.

196
00:20:26,080 --> 00:20:29,080
This is the claim that's being made.

197
00:20:29,080 --> 00:20:36,080
And I think there's evidence, especially for meditators, that this is very much my knowledge evidence.

198
00:20:36,080 --> 00:20:38,080
This is very much the case.

199
00:20:38,080 --> 00:20:45,080
I was just trying to get the idea that maybe there's even more than just the obvious.

200
00:20:45,080 --> 00:20:47,080
The scars that leaves on your mind.

201
00:20:47,080 --> 00:20:49,080
Because that's the worst.

202
00:20:49,080 --> 00:20:57,080
But it seems that actually there are cases of being protected where you harm someone.

203
00:20:57,080 --> 00:21:00,080
It actually physically hurts you right then and there.

204
00:21:00,080 --> 00:21:03,080
That's considered to be deetendamavid and yukama.

205
00:21:03,080 --> 00:21:08,080
A comma that gives results in this life right here now.

206
00:21:08,080 --> 00:21:14,080
As opposed to the indirect where it changes you and as a result you make different life choices.

207
00:21:14,080 --> 00:21:20,080
And other people make different life, different choices in regards to you and how they think of you and so on.

208
00:21:20,080 --> 00:21:25,080
And eventually it harms you in an indirect way.

209
00:21:25,080 --> 00:21:31,080
Either way this is all part of the concept of good and evil.

210
00:21:31,080 --> 00:21:36,080
Or in Buddhism, in our meditation practice we strive for good.

211
00:21:36,080 --> 00:21:45,080
And we do it rationally not because we're told it's good or because it's good in and of itself.

212
00:21:45,080 --> 00:21:51,080
Because we really believe that it's the way to peace, happiness and freedom from suffering.

213
00:21:51,080 --> 00:21:53,080
But that's our practice.

214
00:21:53,080 --> 00:21:55,080
And that's the Damapada for tonight.

215
00:21:55,080 --> 00:21:56,080
Thank you for tuning in.

216
00:21:56,080 --> 00:22:01,080
We're showing you all good practice and peace, happiness and freedom from suffering.

217
00:22:01,080 --> 00:22:30,080
Thank you.

