1
00:01:00,000 --> 00:01:02,000
You

2
00:01:30,000 --> 00:01:32,000
You

3
00:02:00,000 --> 00:02:02,000
You

4
00:02:30,000 --> 00:02:32,000
You

5
00:03:00,000 --> 00:03:02,000
You

6
00:03:30,000 --> 00:03:32,000
You

7
00:04:00,000 --> 00:04:02,000
You

8
00:04:31,000 --> 00:04:34,000
Good evening everyone and welcome to our live broadcast

9
00:04:39,000 --> 00:04:46,000
We're becoming more advanced here as you can see there's a little window up here now

10
00:04:47,000 --> 00:04:51,000
It shows the room shows the meditators who are here

11
00:04:51,000 --> 00:04:57,000
And now we've got three meditators, but only one of them is coming to listen

12
00:04:57,000 --> 00:05:06,000
The other thing is up here you should see a little eye with a circle

13
00:05:06,000 --> 00:05:14,000
And that now has a link to our site in it hopefully we have to hover over it

14
00:05:14,000 --> 00:05:19,000
If you hover over the video you'll see an eye

15
00:05:19,000 --> 00:05:21,000
And it says more info

16
00:05:23,000 --> 00:05:27,000
And if you go to more info you should see a link to our site

17
00:05:29,000 --> 00:05:31,000
That's how YouTube does links

18
00:05:35,000 --> 00:05:37,000
The night's

19
00:05:37,000 --> 00:05:40,000
Dhamma is a good journey guy a book of force

20
00:05:41,000 --> 00:05:43,000
Supta 14

21
00:05:43,000 --> 00:05:49,000
Tatarimani bikhui padhanani

22
00:05:49,000 --> 00:05:53,000
There are these four strivings

23
00:05:59,000 --> 00:06:05,000
Four ways of striving striving of course big part of

24
00:06:05,000 --> 00:06:13,000
What makes us who we are what we strive for?

25
00:06:13,000 --> 00:06:17,000
Do we strive at all? How much do we strive?

26
00:06:19,000 --> 00:06:23,000
Are we lazy or are we ambitious?

27
00:06:23,000 --> 00:06:25,000
What are our goals in life?

28
00:06:25,000 --> 00:06:39,000
In Buddhism we have four, but here the Buddha gives four types of striving

29
00:06:39,000 --> 00:06:43,000
That are of spiritual benefit

30
00:06:43,000 --> 00:06:47,000
These are the kinds of striving

31
00:06:47,000 --> 00:06:51,000
The kind of work that you can do to better yourself spiritually

32
00:06:51,000 --> 00:06:56,000
And spiritually it just means bringing about better mental health

33
00:06:56,000 --> 00:06:59,000
More peace of mind, clarity of mind

34
00:07:01,000 --> 00:07:07,000
Freeing yourself from the bonds bonded which we talked about last night

35
00:07:11,000 --> 00:07:15,000
So what are the four kinds of striving in Buddhism?

36
00:07:15,000 --> 00:07:25,000
Sunwara padhanani striving to restrain

37
00:07:25,000 --> 00:07:31,000
Bahana padhanani striving to abandon

38
00:07:32,000 --> 00:07:40,000
Number three bhavana padhanani striving to cultivate

39
00:07:40,000 --> 00:07:45,000
And number four anurakarna padhanani

40
00:07:46,000 --> 00:07:52,000
Striving to protect

41
00:07:55,000 --> 00:08:01,000
Restraint, abandonment, development, and protection

42
00:08:03,000 --> 00:08:05,000
These are the four ways

43
00:08:05,000 --> 00:08:10,000
And they each describe a different facet of the practice

44
00:08:10,000 --> 00:08:13,000
Or a different way of looking at the meditation practice

45
00:08:13,000 --> 00:08:15,000
Or the path in general

46
00:08:15,000 --> 00:08:18,000
So restraint is in regards to the senses

47
00:08:19,000 --> 00:08:22,000
This famous quote that comes back again and again

48
00:08:22,000 --> 00:08:25,000
Not famous, but it's one that I keep bringing up again

49
00:08:25,000 --> 00:08:28,000
And again, this common quote of the Buddha is

50
00:08:28,000 --> 00:08:36,000
In the Bhikkhu, Chakuna, Rupandiswa, having seen a form with the eye

51
00:08:36,000 --> 00:08:44,000
Nanimitagahi, Nanimitagahi, Nanimitagahi, Hoti, Nanimitagahi

52
00:08:44,000 --> 00:08:52,000
Nanimitagahi, he is not one, or she is not one

53
00:08:52,000 --> 00:08:57,000
Who grasps at the signs or one who grasps at the particulars

54
00:08:57,000 --> 00:08:59,000
Meaning it's just seeing

55
00:08:59,000 --> 00:09:03,000
You don't make more out of it than that

56
00:09:03,000 --> 00:09:05,000
This is the whole of the practice

57
00:09:05,000 --> 00:09:09,000
The challenge of trying to just see things as they are

58
00:09:09,000 --> 00:09:11,000
To keep it real

59
00:09:11,000 --> 00:09:15,000
Keep our experience, our interaction with reality

60
00:09:15,000 --> 00:09:17,000
To just read what's real

61
00:09:17,000 --> 00:09:19,000
To just bear reality

62
00:09:19,000 --> 00:09:23,000
Don't have any speculation or belief or view

63
00:09:23,000 --> 00:09:28,000
Or attachment or aversion or identification

64
00:09:28,000 --> 00:09:37,000
Because when you leave the eye unguarded in this way

65
00:09:37,000 --> 00:09:41,000
When you do delve into the particulars, what is it I'm seeing?

66
00:09:41,000 --> 00:09:43,000
Is it good? Is it bad?

67
00:09:43,000 --> 00:09:45,000
That's nice, that's not nice

68
00:09:45,000 --> 00:09:47,000
That's beautiful, that's ugly

69
00:09:47,000 --> 00:10:13,940
The

70
00:10:13,940 --> 00:10:22,980
basically, or wanting in the version, and all evil dumbness, evil and wholesome things.

71
00:10:22,980 --> 00:10:26,660
All the problems, the causes of our suffering, they will all arise.

72
00:10:26,660 --> 00:10:32,740
They will all attack the mind.

73
00:10:32,740 --> 00:10:37,780
So when one practices are strained, seeing, let's see, just be seeing, keep the mind

74
00:10:37,780 --> 00:10:45,940
it to see, guard the eye, guard the ear, guard the nose, the tongue, guard the body, and

75
00:10:45,940 --> 00:10:58,740
guard the mind, to guard these six doors, and free yourself.

76
00:10:58,740 --> 00:11:07,020
Keep yourself protected, keep yourself protected from the dangers that come from clinging

77
00:11:07,020 --> 00:11:24,660
in the version, the suffering that comes from, from our heart, bad habits of mind.

78
00:11:24,660 --> 00:11:33,900
That's number one, number two, pahana pahana, through abandoning.

79
00:11:33,900 --> 00:11:41,140
Another way of looking at it is we strive to abandon, so we strive to guard the

80
00:11:41,140 --> 00:11:42,140
first one.

81
00:11:42,140 --> 00:11:43,140
So that means it's striving.

82
00:11:43,140 --> 00:11:50,020
It's the hard work that we're doing here, reminding ourselves, it's just seeing, kind

83
00:11:50,020 --> 00:11:53,180
of striving and striving to abandon.

84
00:11:53,180 --> 00:11:55,220
Abandoned, what?

85
00:11:55,220 --> 00:12:15,800
Yes, it's yes, no, yes, and we are starting here, not neighboring because the

86
00:12:15,800 --> 00:12:30,320
we know deity, we used to chant this, and risen thought, sensuality, one doesn't bear it,

87
00:12:30,320 --> 00:12:39,320
not you are saying to you, one abandons it, bhajati, we know deity, the spells, it's

88
00:12:39,320 --> 00:13:00,120
bhajanti karuti, makes an end to it, and then bhajavangameti causes it to go to cessation,

89
00:13:00,120 --> 00:13:11,120
makes it cease, so when there's a desire arises, one dispels it, one doesn't give it quarter,

90
00:13:11,120 --> 00:13:22,440
doesn't chase after it or allow it to take root, mindful, seeing liking is liking, one thing is

91
00:13:22,440 --> 00:13:41,800
one thing, a nut, giving it room to grow, bhajanti bhajanti bhajanti, the same goes with thoughts

92
00:13:41,800 --> 00:13:54,680
of aversion or disliking hatred, you will, doesn't bear a thought of you will, don't let

93
00:13:54,680 --> 00:14:02,540
it take root, don't identify with it, don't take it as your own, see clearly as just

94
00:14:02,540 --> 00:14:13,360
a thought of anger and let it go, abandon it, don't take these things on, and the same goes

95
00:14:13,360 --> 00:14:17,960
with vihingsa vidak, and these are the three wrong sorts of thoughts, vihingsa means any

96
00:14:17,960 --> 00:14:30,280
kind of oppressive mind state of arrogance or conceit to that kind of delusion based really,

97
00:14:30,280 --> 00:14:39,860
and any kind of unwholesum dhamma, upa nupa nay bhajakusulay dhamma, any kind of arisen, evil unwholesum

98
00:14:39,860 --> 00:14:47,880
dhamma, one doesn't bear them, don't bear with them, don't let them take root in your

99
00:14:47,880 --> 00:14:58,200
mind, quite simply remind yourself liking, liking, disliking, disliking, you're confused

100
00:14:58,200 --> 00:15:14,240
or deluded or attached to something, abandon it, give it up, number three, bhavana bhajana,

101
00:15:14,240 --> 00:15:18,920
striving to cultivate, so there are things that we should abandon, we should abandon

102
00:15:18,920 --> 00:15:23,520
these thoughts through our practice, then what is it that we should keep, what are the

103
00:15:23,520 --> 00:15:30,120
good things, Buddhism isn't all about bad things, there's lots of good and healthy things

104
00:15:30,120 --> 00:15:35,160
that we have in our minds that we can develop, most important, well what do we develop,

105
00:15:35,160 --> 00:15:40,320
we develop the seven bold jungles, bold jungles, the seven things that lead to enlightenment

106
00:15:40,320 --> 00:15:47,800
or factors of enlightenment, mindfulness, we cultivate mindfulness, some great thing to

107
00:15:47,800 --> 00:15:53,720
have, it's a great tool to have, you can use it throughout your life in so many situations

108
00:15:53,720 --> 00:16:01,080
when you're suffering a physical pain, when you have mental distress, when you have

109
00:16:01,080 --> 00:16:26,600
challenges or fears, anxieties, doubts, confusion, subties, what clears it all up, and

110
00:16:26,600 --> 00:16:39,920
then you're able to, you're able to investigate, you're able to see what is good and

111
00:16:39,920 --> 00:16:47,440
what is bad, mindfulness is the cultivation of objectivity, once you're objective you're

112
00:16:47,440 --> 00:16:51,680
able to see into your mind, is this good for me, is this bad for me, is this worth

113
00:16:51,680 --> 00:17:01,080
holding on to, or is this worth throwing away, you cultivate effort, strive to cultivate

114
00:17:01,080 --> 00:17:06,760
effort, the effort to be mindful to me, once your mind will be, I just mean doing it again

115
00:17:06,760 --> 00:17:12,280
and again, it's quite a lot of work and you need effort and encouragement to continue

116
00:17:12,280 --> 00:17:21,440
it, beat these some moods in the jungle, beat the means rapture, rapture just means getting into

117
00:17:21,440 --> 00:17:26,040
a groove in this case, and you have put out the effort, eventually it becomes a bit

118
00:17:26,040 --> 00:17:34,080
shul, mindfulness actually becomes a habit, bassadim means your mind calms down, becomes

119
00:17:34,080 --> 00:17:38,360
more tranquil, that's your practice, because you start to work out, and it doesn't feel

120
00:17:38,360 --> 00:17:42,880
like that at first, but you're dealing with all the chaos inside, and once you deal with

121
00:17:42,880 --> 00:17:49,600
it, it starts to become more orderly and you do find yourself calming down, some ideas,

122
00:17:49,600 --> 00:17:53,560
the result of calming down your mind becomes focused and you start to see things just

123
00:17:53,560 --> 00:18:01,000
as they are arising and ceasing, you become more equanimous about things, so seven, seven

124
00:18:01,000 --> 00:18:05,240
is opaque and you become, once you become more focused then you become more equanimous,

125
00:18:05,240 --> 00:18:15,880
not judging things is good about, so this is what it means through development, so what

126
00:18:15,880 --> 00:18:22,200
do we have, we have restraint, restraining the senses, abandoning, abandoning the defilements

127
00:18:22,200 --> 00:18:27,560
in the mind, development, develop the bone jungles, develop good qualities of mind, starting

128
00:18:27,560 --> 00:18:37,800
with mindfulness, and number four, guard or protect, and this is guarding the meditation

129
00:18:37,800 --> 00:18:51,400
practice, guarding, guarding the meditation object, and the other Buddha talks about

130
00:18:51,400 --> 00:18:59,320
actually some at the meditations, which is quite interesting, protecting, but in general

131
00:18:59,320 --> 00:19:06,520
it means protecting your meditation practice, so don't let go of it, don't stop meditating,

132
00:19:06,520 --> 00:19:11,960
try to meditate throughout the day, when you find yourself doing something that takes

133
00:19:11,960 --> 00:19:16,760
you away from your meditation practice, be quick to come back to it, remind yourself

134
00:19:16,760 --> 00:19:21,160
when I was doing that thing, I couldn't be mindful so now I should try harder than

135
00:19:21,160 --> 00:19:41,160
ever to be mindful, these are the four types of striving, some more roachapahana, bhavana,

136
00:19:41,160 --> 00:19:56,440
bhavana, bhavana, ahi bhavikhuita, yinda dapi, someone cultivates effort in regards to these

137
00:19:56,440 --> 00:20:06,600
kayang dukasabha pune, they attain the destruction of suffering, the ending of suffering,

138
00:20:06,600 --> 00:20:20,360
it's the only goal we're trying to aim for, just the complete end of suffering, there was

139
00:20:20,360 --> 00:20:25,400
one monk in the time of the Buddha who made his robe, but he went around telling people,

140
00:20:25,400 --> 00:20:32,040
oh, the Buddha, you know, not really worth following, all he does is teach you to end suffering,

141
00:20:32,040 --> 00:20:37,640
if you follow him, all that's going to happen is you're just going to end suffering, and the Buddha

142
00:20:37,640 --> 00:20:42,520
shook his head and said, you know, this guy thinking he's insulting me, he's actually

143
00:20:44,040 --> 00:20:49,640
doing me a favor by going around and telling people the truth, that's all we're trying to do

144
00:20:49,640 --> 00:21:02,200
is end suffering, suffering doesn't come from our experiences, it comes from our minds, our reactions,

145
00:21:02,200 --> 00:21:10,040
so if we guard our reactions and keep our reactions neutral and learn to see things objectively,

146
00:21:11,560 --> 00:21:15,480
abandoning unwholesome thoughts, cultivating good qualities of mind,

147
00:21:15,480 --> 00:21:21,080
and guarding our practice, guarding our minds, so that we don't slip,

148
00:21:23,560 --> 00:21:27,080
eventually it builds up, it becomes a bit shown, your mind starts to shift,

149
00:21:29,880 --> 00:21:35,080
and eventually you're able to let go, and maintain nibana, become enlightened,

150
00:21:37,720 --> 00:21:38,120
that's all,

151
00:21:38,120 --> 00:21:47,800
all right, so that's the dhamma for tonight, before tight, quite kinds of striving,

152
00:21:47,800 --> 00:21:59,240
you know,

153
00:21:59,240 --> 00:22:02,040
We have never seen anything.

154
00:22:02,040 --> 00:22:08,080
We disproportionally, the extensively doing the same thing.

155
00:22:10,240 --> 00:22:13,760
The concentrated enemy is extreme.

156
00:22:17,800 --> 00:22:05,280
The

157
00:22:05,280 --> 00:22:06,520
from all children,

158
00:22:06,520 --> 00:22:09,320
came when she

159
00:22:09,320 --> 00:22:13,220
subscribed to the channel

160
00:22:13,220 --> 00:22:14,720
something,

161
00:22:14,720 --> 00:22:15,840
did she watch

162
00:22:15,840 --> 00:22:17,480
to get okay and

163
00:22:17,480 --> 00:22:19,280
and

164
00:22:19,280 --> 00:22:20,280
and

165
00:22:20,280 --> 00:22:22,060
name

166
00:22:22,060 --> 00:22:23,260
nineteen

167
00:22:23,260 --> 00:22:24,940
has been

168
00:22:24,940 --> 00:22:26,620
crippled

169
00:22:26,620 --> 00:22:29,680
just

170
00:22:29,680 --> 00:22:33,220
to

171
00:22:33,220 --> 00:22:38,540
This is how people will getaram on Maggie's bike.

172
00:22:38,720 --> 00:22:44,280
How many of which are these 5 and 8's these 6leys.

173
00:22:45,500 --> 00:22:49,660
This one is having some fun time.

174
00:22:49,800 --> 00:22:52,180
And a 2 thousandophone of great

175
00:22:51,980 --> 00:22:55,060
and of course,

176
00:22:55,060 --> 00:23:10,060
Right, I can get rid of the room when I'm not sure about that room thing, but there it was.

177
00:23:10,060 --> 00:23:16,060
Alright, let's all robin's already unmuted, so there you are.

178
00:23:16,060 --> 00:23:18,060
Hi, robin.

179
00:23:18,060 --> 00:23:22,060
Hello, Bondi.

180
00:23:22,060 --> 00:23:24,060
Oh, shake and see the room.

181
00:23:24,060 --> 00:23:25,060
What I see is a little bit different.

182
00:23:25,060 --> 00:23:26,060
I don't see the room.

183
00:23:26,060 --> 00:23:27,060
Yep.

184
00:23:27,060 --> 00:23:32,060
You're on the direct line to me.

185
00:23:32,060 --> 00:23:36,060
I could probably set it up so that you could see the window.

186
00:23:36,060 --> 00:23:38,060
That would be weird.

187
00:23:38,060 --> 00:23:40,060
From that work.

188
00:23:40,060 --> 00:23:44,060
Yeah, probably one actually.

189
00:23:44,060 --> 00:23:46,060
Let me see here.

190
00:23:46,060 --> 00:23:48,060
See if I can do that.

191
00:23:48,060 --> 00:23:49,060
I don't know.

192
00:23:49,060 --> 00:23:51,060
It doesn't let me because it's not a hangout on there.

193
00:23:51,060 --> 00:23:54,060
What is it?

194
00:23:54,060 --> 00:23:57,060
Sure screen.

195
00:23:57,060 --> 00:24:01,060
Oh, I can.

196
00:24:01,060 --> 00:24:02,060
Okay.

197
00:24:02,060 --> 00:24:06,060
Now you should see.

198
00:24:06,060 --> 00:24:09,060
Now you should see.

199
00:24:09,060 --> 00:24:12,060
Basically, except now it's got this green bar at the top.

200
00:24:12,060 --> 00:24:17,060
It does not really forget it.

201
00:24:17,060 --> 00:24:18,060
Sorry.

202
00:24:18,060 --> 00:24:21,060
Oh, that's okay.

203
00:24:21,060 --> 00:24:23,060
Are you ready for some questions?

204
00:24:23,060 --> 00:24:24,060
I'm ready.

205
00:24:24,060 --> 00:24:25,060
Go ahead.

206
00:24:25,060 --> 00:24:29,060
Do we need to apply dependent origination to our walking or sitting?

207
00:24:29,060 --> 00:24:31,060
What is dependent origination?

208
00:24:31,060 --> 00:24:35,060
And why do some people say it's important to learn about it?

209
00:24:35,060 --> 00:24:44,060
Dependent origination is the law of cause and effect.

210
00:24:44,060 --> 00:24:51,060
Even it's not the final, even it's not the most complete.

211
00:24:51,060 --> 00:24:56,060
Even it's still an abbreviation to get true understanding of cause and effect of the

212
00:24:56,060 --> 00:25:05,060
formable truths you have to study the Mahapadhanah, which is a huge immense and incredibly dense work.

213
00:25:05,060 --> 00:25:09,060
But it goes through all the different permutations and types of conditionality.

214
00:25:09,060 --> 00:25:13,060
There's 24 types of conditionality.

215
00:25:13,060 --> 00:25:18,060
Dependent origination is just a one way of a simplification, actually.

216
00:25:18,060 --> 00:25:25,060
Just as the formable truths are and even further simplification.

217
00:25:25,060 --> 00:25:27,060
So learning about it is useful.

218
00:25:27,060 --> 00:25:29,060
It's what you come to understand through the practice.

219
00:25:29,060 --> 00:25:32,060
That's what the Buddha came to understand.

220
00:25:32,060 --> 00:25:38,060
Anarahan need not understand it exactly as the Buddha understood it.

221
00:25:38,060 --> 00:25:43,060
Because the Buddha understood it and involved past lives and involved the whole,

222
00:25:43,060 --> 00:25:52,060
it was basically a summing up of samsara, the essence of samsara.

223
00:25:52,060 --> 00:25:55,060
And how samsara works.

224
00:25:55,060 --> 00:25:58,060
Having observed it.

225
00:25:58,060 --> 00:26:02,060
But for meditators, we only need to concern ourselves with this life.

226
00:26:02,060 --> 00:26:06,060
We only need to concern ourselves with the present moment.

227
00:26:06,060 --> 00:26:09,060
So people talk about Bhattichasamupada.

228
00:26:09,060 --> 00:26:13,060
Dependent origination, having to do it past lives, future lives.

229
00:26:13,060 --> 00:26:17,060
Well, really that seems to be how it was taught.

230
00:26:17,060 --> 00:26:25,060
The essence of it is the formable truths and it's that ignorance of the truth.

231
00:26:25,060 --> 00:26:32,060
It means us to crave and craving leads to suffering, basically.

232
00:26:32,060 --> 00:26:38,060
But you don't apply it because dependent origination is the nature of thing.

233
00:26:38,060 --> 00:26:40,060
It's just the truth.

234
00:26:40,060 --> 00:26:44,060
So through the practice of Satipatana, you see this.

235
00:26:44,060 --> 00:26:48,060
Very confused. The only thing you should be practicing is the Satipatana.

236
00:26:48,060 --> 00:26:52,060
Wisdom, things like dependent origination are things that you'll start to see.

237
00:26:52,060 --> 00:26:56,060
You'll start to see that things are dependent on other things.

238
00:26:56,060 --> 00:27:00,060
There is a relationship when you react in certain ways.

239
00:27:00,060 --> 00:27:02,060
It leads to certain results.

240
00:27:02,060 --> 00:27:15,060
You'll start to incline towards positive results in the way from those things that cause you suffering.

241
00:27:15,060 --> 00:27:19,060
Sir, what is a good way to measure the meditation?

242
00:27:19,060 --> 00:27:22,060
To measure the meditation sessions are going well.

243
00:27:22,060 --> 00:27:26,060
For example, could it be the length of time during a session that I'm able to focus

244
00:27:26,060 --> 00:27:31,060
on one of the Satipatanas without being interrupted by one of the entrances?

245
00:27:31,060 --> 00:27:37,060
The reason I ask is so I can measure the correctness and then take corrected steps that are required.

246
00:27:37,060 --> 00:27:40,060
Well, you can't be aware of anything for more than a moment.

247
00:27:40,060 --> 00:27:47,060
So any sort of duration would be most likely forcing your mind on a single object.

248
00:27:47,060 --> 00:27:49,060
So that wouldn't be a good idea.

249
00:27:49,060 --> 00:27:52,060
The only way you can really measure it is by the moment.

250
00:27:52,060 --> 00:27:55,060
How many moments were you clearly aware of something?

251
00:27:55,060 --> 00:28:00,060
Were you reminding yourself this is pain, this is thinking, this is rising, this is falling?

252
00:28:00,060 --> 00:28:07,060
This is stepping right, this is lifting, this is placing or kind of thing?

253
00:28:07,060 --> 00:28:09,060
It's the quality of the moment.

254
00:28:09,060 --> 00:28:15,060
That's the only real measure.

255
00:28:15,060 --> 00:28:20,060
So even if you're just aware of your hundreds, is that counts as well?

256
00:28:20,060 --> 00:28:24,060
Oh yeah, it's great learning.

257
00:28:24,060 --> 00:28:28,060
Sir, of late I have been mostly bothered by distraction and drowsiness.

258
00:28:28,060 --> 00:28:33,060
To remove them I have played with three shields of walking and sitting, as well as the number of objects

259
00:28:33,060 --> 00:28:38,060
and sitting and number of faces and walking, sometimes changing them on the fly.

260
00:28:38,060 --> 00:28:43,060
Thinking back, today I was able to have two relatively prolonged periods of focus.

261
00:28:43,060 --> 00:28:47,060
I'm rising falling after 15 walking, 30 sitting.

262
00:28:47,060 --> 00:28:55,060
But with relatively forceful breathing, this forceful breathing okay is the overall approach of experimentation okay.

263
00:28:55,060 --> 00:29:03,060
I mean again you're forcing your mind, you're trying to find a trick to get it under control,

264
00:29:03,060 --> 00:29:08,060
which is antithetical to the actual practice.

265
00:29:08,060 --> 00:29:13,060
You being bothered by distraction and drowsiness, what the problem is that you're bothered by them,

266
00:29:13,060 --> 00:29:15,060
that you want to get rid of them.

267
00:29:15,060 --> 00:29:20,060
You should say disliking, disliking or frustrated or however it appears to.

268
00:29:20,060 --> 00:29:24,060
But the idea is not to find a trick to get rid of them.

269
00:29:24,060 --> 00:29:29,060
The idea is to learn to not give rise to them in the first place.

270
00:29:29,060 --> 00:29:38,060
It's not to avoid the opportunity for them to arise, but it's to allow the opportunity for them to arise,

271
00:29:38,060 --> 00:29:40,060
but not give a rise to them.

272
00:29:40,060 --> 00:29:44,060
So whatever condition is causing you to become distracted,

273
00:29:44,060 --> 00:29:52,060
it's to learn about that situation and learn about the distractions to the extent that you no longer react in that way.

274
00:29:52,060 --> 00:29:56,060
You just find a way to avoid the situation.

275
00:29:56,060 --> 00:29:58,060
It doesn't actually solve anything.

276
00:29:58,060 --> 00:30:04,060
So any kind of trick that you might have making it easier, making it better.

277
00:30:04,060 --> 00:30:06,060
It's not really going to help you in the long run.

278
00:30:06,060 --> 00:30:09,060
It's just trying to control things.

279
00:30:09,060 --> 00:30:12,060
Partial breathing is really bad practice.

280
00:30:12,060 --> 00:30:19,060
And your general experimentation, no it's also bad practice.

281
00:30:19,060 --> 00:30:25,060
If you're really distracted or really drowsy such that you just can't conquer it,

282
00:30:25,060 --> 00:30:27,060
then you would want to change.

283
00:30:27,060 --> 00:30:32,060
So if you're sitting you'd walk, if you walk in the sit, but otherwise don't play with the ratio.

284
00:30:32,060 --> 00:30:38,060
Keep it equal, because otherwise it becomes partiality and trying to control.

285
00:30:38,060 --> 00:30:41,060
Trying to avoid, for example.

286
00:30:41,060 --> 00:30:43,060
We want to learn about our hindrances.

287
00:30:43,060 --> 00:30:46,060
They're part of the practice.

288
00:30:46,060 --> 00:30:57,060
When you feel drowsy, the only real way to overcome it is to save yourself drowsy and drowsy.

289
00:30:57,060 --> 00:31:01,060
Who did the Portuguese translation of your booklet?

290
00:31:01,060 --> 00:31:05,060
I read it and some parts of it seemed like they could be improved.

291
00:31:05,060 --> 00:31:12,060
I'm sure I have who it is in my, I don't know offhand who it was, but I have it in my email somewhere.

292
00:31:12,060 --> 00:31:18,060
So if you want to get in touch with me, I can put you in contact with them,

293
00:31:18,060 --> 00:31:25,060
or if you want to help out and improve it, that'd be great.

294
00:31:25,060 --> 00:31:30,060
Do you see any benefit in studying things like reincarnation research?

295
00:31:30,060 --> 00:31:40,060
Jim Tucker, Ian Stevenson, or other kinds of research like that that shows that the mind is more than just physical?

296
00:31:40,060 --> 00:31:48,060
Yeah, sure. Interesting stuff.

297
00:31:48,060 --> 00:31:52,060
I sometimes started treating the mind as if I were training a child.

298
00:31:52,060 --> 00:31:58,060
For instance, when there's been thinking, and I catch it after some seconds, I haven't started saying to it.

299
00:31:58,060 --> 00:32:00,060
Now that was thinking.

300
00:32:00,060 --> 00:32:05,060
If there's itching, I might say, as if I'm speaking to a child, that's the experience of itching.

301
00:32:05,060 --> 00:32:08,060
Is this okay? Thank you, Bonte.

302
00:32:08,060 --> 00:32:13,060
Yeah, that's fine. I mean, it's more efficient to just say itching, itching.

303
00:32:13,060 --> 00:32:18,060
Otherwise, it kind of becomes a thing, you know?

304
00:32:18,060 --> 00:32:23,060
Yeah, you're including the idea of a child, for example.

305
00:32:23,060 --> 00:32:27,060
You're adding a tone to it, which we don't really want.

306
00:32:27,060 --> 00:32:32,060
It should be as neutral and as unflavored as possible.

307
00:32:32,060 --> 00:32:39,060
It's unadulterated, maybe? I mean, you don't want to, again, there's no trick.

308
00:32:39,060 --> 00:32:45,060
Anytime you find yourself trying to find a trick, a way to make it better, like an easier,

309
00:32:45,060 --> 00:32:47,060
you're setting yourself up for disappointment.

310
00:32:47,060 --> 00:32:52,060
It'll work for a while, and then it doesn't work, and you wonder, oh, maybe I have to find a new trick.

311
00:32:52,060 --> 00:32:55,060
You're trying to control. It's not going to be easy.

312
00:32:55,060 --> 00:32:58,060
It's not supposed to be easy.

313
00:32:58,060 --> 00:33:04,060
It's supposed to teach you patience, because it's always going to, it's like a child, you know?

314
00:33:04,060 --> 00:33:11,060
Best way to deal with children is to have no attitude whatsoever, give them the truth, right?

315
00:33:11,060 --> 00:33:13,060
Best way to deal with children.

316
00:33:13,060 --> 00:33:19,060
So try to keep it neutral, don't try to make it some kind of special noting.

317
00:33:19,060 --> 00:33:24,060
Just simply itching, itching, or thinking, thinking.

318
00:33:24,060 --> 00:33:27,060
Don't put any tone on it.

319
00:33:27,060 --> 00:33:33,060
It's not going to work in a long way.

320
00:33:33,060 --> 00:33:36,060
You talked about Kama Yoga last night.

321
00:33:36,060 --> 00:33:42,060
Are there any extraneous practices that you would recommend to help overcome or hasten

322
00:33:42,060 --> 00:33:45,060
the understanding relating to this problem?

323
00:33:45,060 --> 00:33:48,060
Any extra advice or help for anyone dealing with it?

324
00:33:48,060 --> 00:33:58,060
So if there's everybody wants to find a quicker way, I mean, okay, yeah, I mean, I do answer this one frequently.

325
00:33:58,060 --> 00:34:05,060
You know, that Kama is the best way to deal with it outside of Satipatana.

326
00:34:05,060 --> 00:34:11,060
I never forget that that's the best way, but it's mindfulness of the body, the parts of the body.

327
00:34:11,060 --> 00:34:19,060
So you do mindfulness of the hair, and you focus on the hair, and you say, geez, or hair, hair, hair.

328
00:34:19,060 --> 00:34:26,060
And then you do the body hair, facial hair, whatever.

329
00:34:26,060 --> 00:34:29,060
The nails, the teeth, the skin.

330
00:34:29,060 --> 00:34:36,060
And you do one piece by one piece, and you start to see that the body is not something worth clinging to.

331
00:34:36,060 --> 00:34:44,060
And the kind of sexual attraction is weakened through that.

332
00:34:44,060 --> 00:34:57,060
That's the main practice for dealing with essential attraction or sexual attraction.

333
00:34:57,060 --> 00:35:03,060
One day, I almost always have two through six seconds at the bottom of the falling of the breath,

334
00:35:03,060 --> 00:35:06,060
where there is no breath, no rising, no falling.

335
00:35:06,060 --> 00:35:08,060
I'm just waiting for the next rise of the stomach.

336
00:35:08,060 --> 00:35:16,060
To stop being distracted during this period, I have been noting, still, still, are waiting, waiting, or nothing, nothing.

337
00:35:16,060 --> 00:35:19,060
Yeah, that's fine. That's actually correct.

338
00:35:19,060 --> 00:35:23,060
But at that point, and probably where you are, I can't remember where you are on the course,

339
00:35:23,060 --> 00:35:30,060
but at this point, we would, once that starts to happen, it's about time to give you the third part.

340
00:35:30,060 --> 00:35:34,060
So you would, instead, say rising, falling, sitting.

341
00:35:34,060 --> 00:35:38,060
So switch to being mindful of sitting, rising, falling, sitting.

342
00:35:38,060 --> 00:35:42,060
Sitting doesn't have to be between breaths, but sometimes it is, sometimes there's room.

343
00:35:42,060 --> 00:35:47,060
So it doesn't matter, you just switch to sitting, even if the breath continues.

344
00:35:47,060 --> 00:35:51,060
So we're rising, falling, and then switch to sitting, and after you're sitting,

345
00:35:51,060 --> 00:35:57,060
then go back and look at the stomach, and wait for it to rise again.

346
00:35:57,060 --> 00:36:02,060
But yeah, we'll start to give you more objects to be aware of, to help.

347
00:36:02,060 --> 00:36:10,060
Because the breath is, in over time, it becomes insufficient to keep your mind focused.

348
00:36:10,060 --> 00:36:14,060
Your mind is able to see the gaps.

349
00:36:14,060 --> 00:36:21,060
There's nothing to do in between.

350
00:36:21,060 --> 00:36:24,060
What reason is there to be specific with labeling?

351
00:36:24,060 --> 00:36:30,060
For example, if I observe pain or calmness, why is pain pain or calm calm?

352
00:36:30,060 --> 00:36:35,060
Wouldn't it be better to incorporate the least conceptual label possible?

353
00:36:35,060 --> 00:36:39,060
In other words, feeling, in this case?

354
00:36:39,060 --> 00:36:50,060
It's not wrong, but pain is more quick to, it's closer in your mind.

355
00:36:50,060 --> 00:36:54,060
It's all about what brings your mind to the experience.

356
00:36:54,060 --> 00:36:58,060
If you're experiencing pain, you don't think of it as a feeling, right?

357
00:36:58,060 --> 00:37:01,060
You don't think of it as, oh, I'm, now I'm feeling this thing.

358
00:37:01,060 --> 00:37:03,060
It's like, no, now I'm feeling pain.

359
00:37:03,060 --> 00:37:05,060
That's just a feeling, no, no.

360
00:37:05,060 --> 00:37:07,060
You think of it as pain.

361
00:37:07,060 --> 00:37:11,060
Feeling is more for something that appears to you as just some type of feeling.

362
00:37:11,060 --> 00:37:13,060
Pain appears to you as pain.

363
00:37:13,060 --> 00:37:14,060
Calm appears to you as calm.

364
00:37:14,060 --> 00:37:17,060
So it's not wrong to say feeling, but it's a little more intellectual,

365
00:37:17,060 --> 00:37:20,060
because it's not the first thing you think of.

366
00:37:20,060 --> 00:37:22,060
It's not how you perceive it.

367
00:37:22,060 --> 00:37:23,060
You perceive it as pain.

368
00:37:23,060 --> 00:37:26,060
That's how we know it.

369
00:37:26,060 --> 00:37:40,060
It's not wrong to do either one, but it's whatever allows you to quickly remind yourself it is what it is.

370
00:37:40,060 --> 00:37:44,060
Through meditation, can you see or experience the heaven or hell realms?

371
00:37:44,060 --> 00:37:45,060
Thank you.

372
00:37:45,060 --> 00:37:50,060
It depends what type of meditation you're doing, but absolutely, there are lots of meditation.

373
00:37:50,060 --> 00:37:53,060
There are meditations that allow you to do that.

374
00:37:53,060 --> 00:37:54,060
Sure.

375
00:37:54,060 --> 00:37:58,060
I don't teach them that you could read that we see demogets,

376
00:37:58,060 --> 00:38:02,060
got lots of practices like that.

377
00:38:02,060 --> 00:38:12,060
I think you're all caught up with questions.

378
00:38:12,060 --> 00:38:18,060
Vantailas, somebody didn't put the question mark on it and check.

379
00:38:18,060 --> 00:38:20,060
How's YouTube doing?

380
00:38:20,060 --> 00:38:39,060
I love you too, love.

381
00:38:39,060 --> 00:38:53,060
So the comments on YouTube, they disappear as soon as the live stream is done.

382
00:38:53,060 --> 00:38:56,060
I don't know.

383
00:38:56,060 --> 00:39:00,060
I think it's just a live chat that people can chat about.

384
00:39:00,060 --> 00:39:05,060
It seems to go away once the live stream is over.

385
00:39:05,060 --> 00:39:10,060
They aren't actual comments, it's just a live chat feature.

386
00:39:10,060 --> 00:39:14,060
People can chat when they should be actually listening to the talk.

387
00:39:14,060 --> 00:39:16,060
Maybe I should turn that off.

388
00:39:16,060 --> 00:39:19,060
I probably should just turn that off.

389
00:39:19,060 --> 00:39:21,060
Because we've got the chat on our site.

390
00:39:21,060 --> 00:39:27,060
I'm just going to turn it off.

391
00:39:27,060 --> 00:39:35,060
There, now there's no live chat.

392
00:39:35,060 --> 00:39:36,060
All right.

393
00:39:36,060 --> 00:39:39,060
Well, then that's all for tonight.

394
00:39:39,060 --> 00:39:40,060
Thank you all for tuning in.

395
00:39:40,060 --> 00:39:42,060
Thanks for having me for all your help.

396
00:39:42,060 --> 00:39:44,060
Thank you, Bumpy.

397
00:39:44,060 --> 00:39:45,060
Good night.

398
00:39:45,060 --> 00:40:05,060
Thank you.

