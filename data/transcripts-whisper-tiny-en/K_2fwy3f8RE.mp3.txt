Welcome to our live broadcast.
We are continuing on with the study of the Anguparani guy, the good ones.
Today's Suptas are one guy, so it's a whole chapter.
Very short.
Very short what you might call Suptas.
But each one is only a couple of sentences.
And this is on the five hindrances.
The five hindrances are...
They're one of the staples of the meditation practice.
The first things you have to know in your practice of insight meditation,
or any type of meditation.
In fact they're essential for success in any realm, meditative rather wise.
Their designation as hindrances means they get in your way, they obstruct.
They prevent success.
They prevent progress.
They inhibit.
They keep you stuck.
New water now.
We talked about these knee water.
We talked about this, I think.
It was earlier, was it on the broadcast, I think, or knee water.
It means it keeps you back, holds you back, keeps you from attaining your goals.
You can see you just see this most clearly in meditation.
You're straining the mind and you're cultivating clear and profitable states of mind.
So you see quite clearly the things that are preventing you from having a clear mind.
There are five hindrances and they're essential learning for a new meditator,
essential that you're able to recognize and overcome five hindrances.
There are different ways of dealing with each of the five hindrances.
As with everything, there's the conventional methods and then there's the ultimate.
The methods based on ultimate reality.
So there are conventional methods like eat less and you'll be less tired.
It is conventional.
It's not based on the moment to moment experience.
In moment to moment experience, really, there's much more simply to do with how mindful you are.
As you're more mindful and cultivate all the whole some qualities that are necessary to overcome the hindrances.
But it's interesting just to single out the cause and the cure for each of the hindrances.
Because they have different qualities and therefore different causes and cures.
So the Buddha says, I don't see one other dumb.
That causes, causes under reason by which under reason central desire arrives.
Or a reason central desire becomes more, becomes greater.
So the first one is central desire.
And he phrases it this way that there's one thing that causes the arise to sense desire.
It's interesting to know it helps you get to the root of the problem.
Essential desire or desire in general is a hindrance.
It prevents you from seeing clearly.
It clouds your judgment, partiality.
When you like something, you're unable to see whether it's good or bad for you.
You're unable to see the true nature of it.
Your desire, super scenes, your reason, your judgment.
So it's a hindrance.
So what is the cause of what is the one dumb that leads to sense desire,
it's a desire, subanimita, subanimita.
Subanimita means beautiful.
Nimita, Nimita again may come up with this word.
We had it in the last chapter as well.
It gives this word in mind.
It's important.
This is the mental formation, the construct.
It's Nimita means sign, so this could be translated as the sign of beauty.
Subha.
Seeing something is beautiful.
There's a sign, there's a flag.
If you look around you, they can see this here.
Look at this.
You see when that comes into view?
There arises the sign of beauty.
Some people, unless you hate flowers.
It's gone.
The sign of beauty disappears.
There's a sign, there's a quality.
It's not inherent, actually.
There's nothing beautiful about flowers,
but the mind perceives them that's beautiful.
There are many other things.
You see them with a statue here.
You see my ugly face, and you have the sign of ugliness.
Beauty, ugliness.
These are in the mind.
The mind gives rise to them.
You see my face calm.
It might seem somewhat attractive, but then if I go like this,
then it suddenly becomes ugly.
You see, the sign arises.
And it changes in your mind.
Your perception changes.
It's just the sign.
So the sign of beauty is seeing things as beautiful,
perceiving things as beautiful.
This is what you have to look for.
You have to be aware of this perception of beauty.
Catch it when it comes.
And this is what you see in meditation.
You're able to break experience apart moment by moment.
As a result, you're able to overcome it.
You're able to achieve what is called a subanimita,
or the absence of subanimita.
A subanimita is conventionally.
A suban means not beautiful.
So if you examine something,
let me take a human, for example.
So you look at a human you might find them attractive.
But then if you focus on the nature of that human form,
and the unattractive nature, that human form,
it changes your perception.
So if you look at hair on the head,
facial hair, skin,
if you focus on these things,
you start to see that actually there's nothing beautiful about it, right?
Skin teeth.
You're not going to tooth.
And you think about it.
The beauty disappears.
Nails.
Maybe nails are beautiful.
But if you examine them, you focus on them.
There's a conventional way of sort of counteracting
or attachment to beauty,
to learn to see the ugliness and things.
But a subanimita can also mean
the lack of seeing things as not attractive,
simply not attractive.
I mean, looking at things in such a way
that they're no longer attractive,
not ugly,
but to see things impartial,
partially.
So when you say to yourself seeing things,
it really is just seeing the subanimita,
a subanimita,
no beauty or ugliness arises.
There's nothing beautiful about things.
I guess the real takeaway from this,
so it's not the thing itself that's beautiful.
It's the concept in the mind or the conceiving in the mind
of something as beautiful.
Number two, the abado.
Hill will or aversion.
Well, it will.
It will, as the other hindrance,
when you're something arises and you're upset about it
or you don't want something to arise.
The abado is technically in regards to people.
It means you don't like someone.
If you're angry at someone, this is a hindrance.
But it can be referring to any kind of experience.
It doesn't have to be a person.
Maybe the birds tripping outside that make you angry.
Maybe the room around you.
Maybe your own body makes you angry.
Maybe the pain you experience makes you angry.
Anger, you need.
That's what it's meant to you.
Anger is a hindrance.
Anger gets in the way.
It gets in the way of not only meditation practice,
but success in all these things,
just as desire does.
If you want, if you're obsessed with getting things,
that gets in the way of your goals and your ambitions,
as well as in meditation.
If you're angry about something,
that also saps your energy and takes you away from your focus.
And these both of these destroy your focus.
And they sapped your energy.
So they keep you from attaining your goal.
They're the real enemy.
Pain isn't the enemy.
The enemy is dislike, he is aversion.
It's anger. This is anger in the cause.
The one dumb mother, the Buddha says,
nah, I'm going to be coming.
There's no other besides this one dumb mother.
And that is the patikani.
Patikani means the nimita.
Again, here it can't be translated as sign.
It means nimita.
We have to translate it as the conceiving.
Or the mental construct.
Mental construct of patikani means aversion.
When you're a verse to something,
a risen, under risen anger will arise.
And a risen anger will become worse and become stronger.
If you're angry at someone, something,
if you're angry already and then you experience something
with aversion, anger gets worse.
Suppose you're already angry,
and then someone starts blaring their music in the next room.
If you're a verse to that, it feeds your anger.
But of course, anger that hasn't arisen arises
because of aversion.
Patikani is the date of secret.
It's the quality of mind inherent in any anger.
And so if you want to understand anger here,
you have to understand that it's based in a version,
it's based in an object that you perceive,
you conceive of, has unpleasant,
you conceive of it with displeasure,
with aversion.
Simple as that, I mean, this isn't really all that profound.
It's incredible in its ability to make things so simple,
keep things so simple.
What it's teaching is it's quite practical
and down to Earth, simple, or anything.
If you read it, it sounds like it must be missing something.
You don't realize how simple reality is.
And so as a result, when you're angry,
we try to figure out why we're angry.
Once I may be abused as a child,
there's my father may be an angry,
so maybe genetics.
So you think this is simple.
If I'm simplistic,
it's because we have only,
we're far too flat,
we go way beyond the truth.
We ignore the simple reality.
Why are you angry?
Because you have a version,
you've cultivated a habit of a version.
And so our meditation practices very much about
realigning our minds,
retraining our minds,
cultivating new habits of objectivity, clarity.
So what is it that?
What is the one dominant that
helps anger to subside,
meet that J.D.
we move to,
meet the friendliness.
Friendliness is the one dominant.
Again, this is conventional.
But it makes met friendliness
a very useful practice.
It's be our pado here is referring to people being,
when you're angry at someone,
this is a hindrance.
If you're angry at,
if you're angry,
it gets in the way of your practice.
So cultivating loving kindness is,
it's a very useful support and meditation to help us
to overcome these strong anger,
anger towards people,
towards individuals.
A deeper level is,
a deeper level is much more about just being objective.
How do you really get rid of a version to anything?
You give up the judgment
when you no longer judge the experience,
whether they be pleasant or unpleasant,
and it doesn't arise like you're in this thing.
Anyway, this is a conventional sort of teaching.
So the talk about cultivating met is,
it's useful.
It's good for us to remember as beings,
as human beings.
But as meditators,
we go deeper than this,
than this took the teachers.
And if you read this at the batana,
it took to the Buddha says,
when you're angry,
just know that you're angry.
How do you deal with anger?
Know that you're angry.
Know that anger has arisen.
That's it.
When you have greed,
lust, know that lust has arisen.
You can be objective about these things,
even the bad things.
Then you retrain the habit of reacting,
rather than reacting,
and just experience subjectively.
This is the second hindrance.
The third hindrance is,
Tina Mita.
This is often translated as,
sloth and torpor.
Vicobodis has dullness and drowsiness.
I'm trying to translate Tina and Mita.
Tina Mita is the mind that is sluggish.
It's dull or not dull,
but unwieldy state of mind.
When the mind has a strong focus on things,
the cause,
the one dumber that causes drowsiness,
or whatever we're going to translate,
it decides.
Is Jaita so lean at them?
Lean at that is an interesting word.
It comes from the verb lea dii.
Lea dii means to stick to cling.
The mind is the mental stickiness,
not stickiness, but being stuck mentally.
When your mind is unwieldy,
in a sense that it isn't flexible,
it isn't sharp, it isn't strong, it isn't wieldy.
It's slow to move sluggish.
You can see this best when you begin to practice the meditation.
You try to focus on the stomach rising and falling in your mind,
quickly slips back into its state of dullness.
It's torper.
It's sort of a dream state.
When you come back and you try again,
the mind slips off thinking or whatever.
You feel that in the difficulty,
they're involved in keeping the mind again and again noting.
To keep the mind coming back to the present moment requires
what is the cure for a team of Minda,
which is Aarandavirya.
Aarandada means like initiative.
Virya means effort.
Aarandavirya means effort and initiative
or initiative-based effort.
The effort to do, to get up and go in this case
to bring the mind back again to the object.
The people I misunderstand with effort that they should somehow push,
which is somehow work to cultivate effort.
It's not really like that.
It's the effort to do again and again to not stop.
To not quit, to not let the mind slip away.
Do it again, again, every moment
because it's incessant.
In the beginning, it's quite challenging.
It's quite tiring.
It's something that trains you in effort to come back again and again to the experience.
That's number three.
Here is Utakcha Kukucha.
Utakcha Kukucha.
Utakcha is restlessness.
Kukucha is worrying.
They're two different ones.
Two separate ones.
But they both restlessness and worry both come from Jetisohu as Samo.
Which means mental disquietude will pass Samo means quietude into tranquility.
Utakcha is not quiet.
The mind is not quiet.
The mind is not focused.
The mind is not settled.
This one is, I mean, I guess they all have much just to do with time,
but this one especially is not something you can force.
This one especially people try to force to prevent the mind from thinking,
prevent the mind from more, prevent the mind from wandering.
This just comes from time as the mind settles down.
This will pass Samo, eventually the mind settles.
It calms down.
They give the analogy of a baby cow.
When you have a cow and you're trying an ox,
there were train oxes to pull the cart.
So in order to train it, they would have to take it away from its mother and tie it out.
The mind would run around wailing and trying to get to its mother.
Kind of a cruel practice as an analogy goes, but in real, but as an analogy,
this is the mind.
We let the mind, we have to let the mind,
let the mind run.
But keep a rope on it, keep the rope of mind from it, so when it does run,
be mindful of it, be aware of this.
Seeing this is hearing, keep the mind, keep the mind on a rope.
It doesn't mean you tie it.
It doesn't mean you have to tie it down.
You just have to keep it on a rope and eventually the mind,
like they may be calf, but a young calf will retire.
Then it will settle down.
Once you don't give it any quarter.
The calming of the mind, there are ways to calm the mind artificially
through some of the meditation.
But the ultimate quieting of the mind is just cultivating the habit
of experiencing the very things that cause distraction,
experiencing them with focus, with clarity,
without reacting.
And in this case, reacting means giving rise to thought about the object.
When you see something rather than judging it
and creating all sorts of mental activity about it,
learn to just see it, let it go.
We're so quick to react to things.
You maybe see something on the internet,
and it makes you think, you like it,
or you dislike it, you get incensed by it,
or you get attracted to it.
And you start to think about it.
It's called debate's restlessness.
Once you learn to see things just as they are,
your mind becomes triangles quite simple.
This is the cure, this is the cause,
and this is the cure, the queen.
We're going to have to go looking for genetic or
traumatic causes for our problems,
even all of our mental illnesses.
They all come from simple, true, simple realities.
And number five is doubt.
Doubt here is, I think, the most interesting in this,
because what is the cause for doubt?
The cause for doubt the Buddha says is,
Ayoni so Manasika,
and we know these terms, those of you who are familiar
with Buddhist terminology.
Yoni so Manasika, you know,
Yoni so means to the source,
or from the source,
and Manasika in the mind,
and Karl means to make.
And it means it's to keep something in mind,
and it's very essence.
Keep the essence of something in mind.
Ayoni so Manasika,
I mean not keeping the essence of things in mind.
But it means, I mean that's an awkward translation.
Seeing things as they truly are,
or not seeing things as they truly are.
Or seeing things carefully or wisely as a soft and triumphant,
or unwisely.
The meaning here is that if when you experience something,
you don't see it as it is,
you're not clearly seeing what's truly there.
Yoni means the womb actually,
so where that means the womb,
but the reason how it's being used is the essence,
the very root of the thing.
So when you experience pain,
if you don't see it as pain,
there are little rise all sorts of ideas about it.
It's my pain.
Is it a problem, is it going to hurt me?
Doubt is not the same as disbelieving something.
So what it means here,
Doubt is uncertainty.
And uncertainty only arises
when you don't see things clearly.
Makes sense.
So if you want to solve all your uncertainty,
what is the right path?
Am I doing well?
All these questions that meditators have
is this path out thing, is this path good?
Any kind of doubt and uncertainty can I do it?
Am I good enough?
All of this comes from not,
you're not even asking the right question.
And we have questions, am I good?
Am I right?
Is this right?
Is this good?
The questions themselves are a problem.
Questions themselves are an indication.
I'm not seeing things clearly.
If you saw things clearly,
if you saw what was really going on,
you wouldn't even ask these questions
and the idea behind the questions
wouldn't even arise.
They come from not seeing things clearly
because you start to see it in terms of I and me
and past and future.
The idea of a meditation technique
or practice or meditation centers.
Or in terms of your life,
you see it as an issue that you have.
Should I do this?
Should I not do this?
Is this right?
Is this wrong?
When you see things clearly,
not only are these questions answered for him
because you see whether they're right or wrong.
You see the process.
But you also give up asking such questions.
You don't have any reason to ask.
You're too busy living.
You're too busy being.
Doubt itself and questioning itself.
It's just a rabbit hole.
You go down and you forget.
Reality is quite simple.
Anyway.
So I'm good to rest.
Remember the five hundredths.
To think about why they're cause.
But ultimately it comes down to our minute
to simply practicing in sight meditation.
It's your practice.
Your mind overcomes all of these.
Good to remember all five of them.
So what happens is you'll be practicing properly
and think you're progressing.
But suddenly it becomes difficult or not suddenly gradually
it becomes difficult to practice.
And you feel like you're stuck or it's you're doing something wrong.
If there's something wrong with your practice,
it's based on these five hundredths.
And this is where you should look.
If the meditation is not progressing smoothly,
it's because of all five of these.
One or more or many of these.
So that's the dumb of this evening.
Now let's get into some questions.
Any questions?
Our breakups and divorces due to the fading away of attachment
or passing away of emotions would turn firmly.
You know, many causes.
And often it's simply because we require new things to please us.
If the person in the relationship is not adapting to provide us
with a new stimuli, we can be attracted to a different stimuli
and attracted to a different person.
And suddenly want to break up because that thing is new.
So addiction works.
The same old, same old becomes less capable of providing pleasure.
So we require new stimuli.
And the old, same old person isn't going to provide that.
So that's one big reason.
Another is the aversion to certain things.
When you have pleasant stimuli, you're able to ignore it.
As the desire and the pleasure fades away,
the displeasure in regards to a person is magnified.
As a result, it can become so strongly you desire for that.
You actually call, it's like a switch.
At one point it becomes tips, the scale,
and suddenly rather than loving the person you hate them.
Could you describe the overview of how an online course goes?
Right, so an online course you should be doing.
Eric, you should go.
The online course is you have to do at least an hour of meditation a day,
have walking, have sitting, and by the end of the course we ask you to get up to two hours.
You require a web camera and a headset.
So you have to make sure that it's all a working order tested out.
Make sure your microphone is working.
Make sure you're your phone's working.
Make sure your web camera is working.
And then we meet once a week as per the meeting page.
And no, you don't need to take time off work.
You need to keep the 5% so familiarize yourself with them.
And try to keep us close to the 8% as you can,
not engaging in entertainment or not sleeping too much,
not eating too much, that kind of thing.
Button thing, does a newborn baby see seeing or is it just a case
if not knowing what to call the thing when he already takes his objects?
Try to take his objects and entity.
I don't understand.
The newborn baby reacts to things that they see,
just as we all do.
I mean, they have this.
They don't come in fresh.
They have this habit of reacting to things that they see.
And so they'll react with pleasure or with pain through seeing or hearing.
I guess the brain is quite simple.
And so as a result, one's the pleasure and the pain that come from seeing and hearing is quite simple.
And the recognition process is quite simple.
I don't know.
Not really clear on what you're trying to get at with that question.
It's somewhat speculative, I mean, why do you care?
Is that really affecting your meditation practice?
I'm kind of focusing more on meditative questions.
I know you have a lot of these technical questions,
and also I don't have answers to everything because I'm not the Buddha.
I'm not the Buddha.
I was curious how much a part of rules plays in meditation practice.
Sometimes he's very authoritarian.
Doesn't?
That's interesting.
There's meditation practice in authoritarian.
Like we sit there and force you.
Be mindful.
I mean, we have rules, but they're not exactly rules.
Maybe that's true.
If you're talking about a meditation course, there are rules.
Because you're wasting your time if you're not keeping these rules.
It's not really beneficial to the meditation mode progress.
There are certain things that are antithetical.
The counter indicative meditation practice process.
What do you think about asexuality?
Is it more wholesome than actually having sexual attraction?
Yes, because sexual attraction is unwholesome.
Simple asexuality can generally good, but it often just means bottling up your sexuality
much in the long run doesn't work.
It's not the answer, but it allows you to deal with the emotions, to look at them internally
rather than cultivating the habit of indulging.
When you become asexual, you're forced to look at these feelings.
If you have a technique that allows you to understand them and to deal with them, then you can be great.
You can actually overcome them.
How long does the online course usually go for?
Well, if an awful normal course is 21 days, and you don't finish the entire course online, you can.
The online course is about 15 weeks.
One day is equal to one week, so we do about 15, nah, maybe not.
14 somewhere around that, maybe 12, I don't know.
Maybe just 12 or 13 weeks.
I haven't counted.
Robin's leaving tomorrow.
Do you think in the morning or the afternoon evening?
Tomorrow morning, Robin is leaving.
Okay, the chairs as well.
I'll be all hoping it loaded up.
I don't know what we're going to do without you.
Well, we might have more people coming, and hopefully someone will be able to stay and help out.
But I really do need someone here as a steward.
It must Michael somehow comes back with it, not holding my breath.
Need to cultivate someone new.
Find someone new who will have them in practice and then stay and help.
There's a couple of people who are expressed in trust.
No more questions.
Well, that's good.
You can take an early break tonight.
Thank you all for tuning in.
You have more questions if you're just typing in your question about
it.
We'll save it for tomorrow.
It shouldn't be here at the same time, same place.
Good night, everyone.
