1
00:00:00,000 --> 00:00:06,960
And in the teachings of the Buddha, you know, the virtue or seat, you call it sila, the

2
00:00:06,960 --> 00:00:09,120
keeping of precepts.

3
00:00:09,120 --> 00:00:14,280
The virtue, I mean, the purity of virtue is very important because it is here set the

4
00:00:14,280 --> 00:00:16,320
fun ground of what you like.

5
00:00:16,320 --> 00:00:17,320
Right.

6
00:00:17,320 --> 00:00:18,320
I'm a figure.

7
00:00:18,320 --> 00:00:20,240
Yeah, I'm a field of energy on the ground.

8
00:00:20,240 --> 00:00:21,240
Yes.

9
00:00:21,240 --> 00:00:31,000
The sila is compared to ground and energy is compared to the feet and feet or confidence

10
00:00:31,000 --> 00:00:35,880
is compared to hand because if you have a hand, you can pick up things you want to pick

11
00:00:35,880 --> 00:00:36,880
up.

12
00:00:36,880 --> 00:00:38,680
If you do not have hands, you don't, you cannot.

13
00:00:38,680 --> 00:00:44,080
In the same way, if you do not have any confidence, then you cannot pick up a cursor or

14
00:00:44,080 --> 00:00:45,080
a good thing.

15
00:00:45,080 --> 00:00:55,160
And the hand is compared to the coming, confidence or feet is compared to a hand.

16
00:00:55,160 --> 00:01:02,640
And the foot, mother is compared to an X, which cuts, which cuts all together the mental

17
00:01:02,640 --> 00:01:03,640
defilements.

18
00:01:03,640 --> 00:01:08,800
So if your conduct isn't clearly virtuous, then there's no way that you're not going

19
00:01:08,800 --> 00:01:14,400
to be involved in the pre-incomic because if you have a just not virtuous, then you'll, whatever

20
00:01:14,400 --> 00:01:17,920
you call yourself, you slowly go ahead and let's try it.

21
00:01:17,920 --> 00:01:18,920
Yes.

22
00:01:18,920 --> 00:01:22,720
What is an example of functional?

23
00:01:22,720 --> 00:01:29,200
Functional is something like, I gave this example, but Tim what didn't like that?

24
00:01:29,200 --> 00:01:34,720
He said it was too, maybe, I don't know.

25
00:01:34,720 --> 00:01:40,200
It's like a fruit, which does not bear fruit, which does not bear fruit.

26
00:01:40,200 --> 00:01:47,560
It still is growing, living, it still has branches and leaves and so on, but it does not

27
00:01:47,560 --> 00:01:50,080
bear fruit, something like that.

28
00:01:50,080 --> 00:01:56,120
And ever hands, after becoming an ever hand, still do good things.

29
00:01:56,120 --> 00:02:03,040
They still teach people, still practice meditation and still help other people.

30
00:02:03,040 --> 00:02:12,040
But all their actions do not constitute karma because they have eradicated the root of

31
00:02:12,040 --> 00:02:20,600
karma, I mean, the root of existence, that is the craving and ignorance.

32
00:02:20,600 --> 00:02:28,080
So their actions are just actions and their actions do not bring reactions, I mean,

33
00:02:28,080 --> 00:02:34,200
perhaps.

34
00:02:34,200 --> 00:02:46,440
We, in Burma, at the New Year's ceremony, the cannons were fired.

35
00:02:46,440 --> 00:02:54,000
And so when something is just just noise and has no substance, we say, this is the New

36
00:02:54,000 --> 00:02:58,040
Year cannon or something like that, what do you call that?

37
00:02:58,040 --> 00:03:08,720
The only of something, yeah, so it is something like that, fruit, I mean, a tree is

38
00:03:08,720 --> 00:03:18,040
very no fruit, they are still good, it's called as a little of yes.

39
00:03:18,040 --> 00:03:33,280
Okay, then, second one, Sambasambudha, now Buddha, we use the word Buddha to refer to him,

40
00:03:33,280 --> 00:03:45,560
but his full habitat is Samma Samudha, now Samma is one prefix, Sam is one prefix, there

41
00:03:45,560 --> 00:03:57,080
are two prefixes here and Buddha, Samma means rightly and Sam means by himself, so he discovers

42
00:03:57,080 --> 00:04:04,640
the full noble truth, Buddha means to know, so he knows the full noble truth, or he discovers

43
00:04:04,640 --> 00:04:12,120
the full noble truth, rightly and without assistance from any person, by himself.

44
00:04:12,120 --> 00:04:18,400
So we emphasize this when we talk about Buddha, Buddha are those persons who do not need

45
00:04:18,400 --> 00:04:28,640
any teachers, so Bodhisattva went to two teachers before he became the Buddha, but he did

46
00:04:28,640 --> 00:04:38,560
not follow their advice up to the attainment of Buddha, he discarded their practice and

47
00:04:38,560 --> 00:04:47,280
then went to a place and practiced by himself, so we say Buddha had no teachers and this

48
00:04:47,280 --> 00:04:54,920
is emphasized by this Sambudha, so Sambudha means self enlightened and Sambudha means

49
00:04:54,920 --> 00:05:03,200
rightly enlightened, so Sambasambudha means rightly self enlightened, something like that,

50
00:05:03,200 --> 00:05:16,120
and here, do no means to know the full noble truth, and then on page 211, paragraph

51
00:05:16,120 --> 00:05:24,680
28, did the other things which Buddha knew are given, they are six groups of consciousness

52
00:05:24,680 --> 00:05:30,800
and so on, so these are the topics taught in Abidha.

53
00:05:30,800 --> 00:05:39,040
They are found in Suddha, but they are treatment in full, can be found in Abidha.

54
00:05:39,040 --> 00:05:49,080
Sometimes people take a bit of a separate thing, not connected with Suddha or something

55
00:05:49,080 --> 00:05:57,760
like that, but actually those we found in the Abidha, most of them are also taught in

56
00:05:57,760 --> 00:06:04,800
Suddha, that is why we need a knowledge of Abidha, but we don't understand the Suddhas.

57
00:06:04,800 --> 00:06:16,800
We should not separate them, treat them as separate, we cannot study Suddhas without

58
00:06:16,800 --> 00:06:23,360
a knowledge of Abidha, that means that is if we want to understand fully and correctly.

59
00:06:23,360 --> 00:06:37,360
So, these are topics taught in Abidha, and Suddha also.

60
00:06:37,360 --> 00:06:49,120
So, here, Buddha's penetration into the full noble tools, or discovery of full noble

61
00:06:49,120 --> 00:06:59,120
tools, is meant by this second attribute, Sambudha.

62
00:06:59,120 --> 00:07:23,120
Now, Mahayana, it's usually Dr. Arha is one who doesn't have it, who is enlightened by himself.

63
00:07:23,120 --> 00:07:31,520
And that's usually distinguished from Buddha, who isn't at this total night by himself,

64
00:07:31,520 --> 00:07:36,400
or in the vacuum, whether or not people.

65
00:07:36,400 --> 00:07:45,880
The particular Buddhas are not called Samma, Sambudha, they are called just Sambudha,

66
00:07:45,880 --> 00:07:49,120
or a particular Buddha, but not Samma, Sambudha.

67
00:07:49,120 --> 00:08:00,200
Rightly means, in all aspects, they do not understand in all aspects of the Tamil.

68
00:08:00,200 --> 00:08:11,880
They are self enlightened persons, but their knowledge may be not as wide as comprehensive

69
00:08:11,880 --> 00:08:17,040
as that of the Buddhas.

70
00:08:17,040 --> 00:08:24,760
And also, sometimes, they are called silent Buddhas, that comes from the fact that they

71
00:08:24,760 --> 00:08:30,960
do not teach much, although sometimes they teach, but they seldom teach, they want to

72
00:08:30,960 --> 00:08:36,400
be away from people, and live in the forest, and so they are called silent Buddhas.

73
00:08:36,400 --> 00:08:50,040
Sometimes, it's because they were taught, it's not going to teach.

74
00:08:50,040 --> 00:08:54,480
And also, they appear only when they are there are no Buddhas.

75
00:08:54,480 --> 00:09:10,040
So, they appear between enterments of Buddhas, and then, I hope you read the footnote,

76
00:09:10,040 --> 00:09:16,840
it's not so very good, so we'll go next to the third one.

77
00:09:16,840 --> 00:09:22,640
He is endowed with clear vision and virtuous conduct, so there are two words here, weak

78
00:09:22,640 --> 00:09:32,440
jar and chaturana, weak jar means understanding of vision, and chaturana means conduct.

79
00:09:32,440 --> 00:09:40,960
So, Buddha is endowed with both clear vision and conduct, clear, there are three kinds

80
00:09:40,960 --> 00:09:52,120
of clear vision, that means remembering his past lives, and then divine eye and destruction

81
00:09:52,120 --> 00:09:53,120
of the filaments.

82
00:09:53,120 --> 00:10:03,080
They are all three clear visions, so whenever three is mentioned, these are men, remembering

83
00:10:03,080 --> 00:10:14,960
past lives, divine vision, and destruction of the filaments, these are called three clear

84
00:10:14,960 --> 00:10:15,960
visions.

85
00:10:15,960 --> 00:10:23,960
Sometimes, eight kinds of clear visions are mentioned, so in that case, there are some

86
00:10:23,960 --> 00:10:24,960
more.

87
00:10:24,960 --> 00:10:34,060
So, the eight kinds are stated in the unbutter-soda, for there, eight kinds of clear vision

88
00:10:34,060 --> 00:10:41,040
are stated, and they are made up of six kinds of direct knowledge, together with insight

89
00:10:41,040 --> 00:10:47,200
and the super-normal power of the mind-baked gory, so there are eight kinds of clear visions

90
00:10:47,200 --> 00:10:53,120
mentioned in that soda, but if you go to that soda, you will not find them clearly stated,

91
00:10:53,120 --> 00:11:03,520
because in pari, when they don't want to repeat, they just put the sign there, like

92
00:11:03,520 --> 00:11:16,000
you use dots, three or four dots, so in pari, also, they use the letter P A, or sometimes

93
00:11:16,000 --> 00:11:23,600
L A, so when you see this, you understand that there are something missing here, or they

94
00:11:23,600 --> 00:11:28,280
don't want to repeat, so you have to go back to where the first occur, and if you don't

95
00:11:28,280 --> 00:11:33,680
know where to find them, then, you will be lost.

96
00:11:33,680 --> 00:11:48,120
So I look them up in D-100, D-100, D-100, and they were not prejudiced in the book, because

97
00:11:48,120 --> 00:11:54,280
they were mentioned in the second soda, D-D completely, I'm going to say that soda.

98
00:11:54,280 --> 00:12:00,200
This is one problem with us more than people.

99
00:12:00,200 --> 00:12:05,640
These books are meant to be read from the beginning to the end, not just because the soda

100
00:12:05,640 --> 00:12:10,200
in the mirror and you read it and you have to say that.

101
00:12:10,200 --> 00:12:16,600
So they are meant to be studied from the beginning.

102
00:12:16,600 --> 00:12:23,640
I can guess you the page numbers of the English translation.

103
00:12:23,640 --> 00:12:34,000
So the eight are the six kinds of direct knowledge, that means the performing miracles

104
00:12:34,000 --> 00:12:43,560
that is one, and then divine ear, and then reading other people's mind, and remembering

105
00:12:43,560 --> 00:12:51,640
past lives, and then divine eye, you get five, right?

106
00:12:51,640 --> 00:12:58,280
And then, destruction of mental departments, six, and then two are mentioned here.

107
00:12:58,280 --> 00:13:05,240
We pass on our insight, and super normal power of the mind-make body, that means you

108
00:13:05,240 --> 00:13:10,600
practice Jana and then you are able to multiply yourself.

109
00:13:10,600 --> 00:13:17,280
So these are the eight kinds of clear vision stated in that soda.

110
00:13:17,280 --> 00:13:23,600
By contact is meant 15 things here, restrained by virtue, guarding the doors of the sense

111
00:13:23,600 --> 00:13:29,080
record is knowledge of the right amount in eating, devotion to wakefulness, the seven good

112
00:13:29,080 --> 00:13:34,640
states, they are in the food note, and the four Jana's of the fine materials fear.

113
00:13:34,640 --> 00:13:41,200
Or it is precisely by means of these 15 things that a noble disciple contacts himself,

114
00:13:41,200 --> 00:13:43,440
that he goes towards the dead death.

115
00:13:43,440 --> 00:13:49,280
That is why it is called virtue conduct, according to that it is said now, you have to understand

116
00:13:49,280 --> 00:13:57,440
the body what Jana, to understand this explanation, the what Jana comes from the root Jana,

117
00:13:57,440 --> 00:14:02,520
C-A-R-A, and Jana means to walk, to go.

118
00:14:02,520 --> 00:14:13,120
So Jana means some means of going, so these 15 are the means of going to the burden of

119
00:14:13,120 --> 00:14:23,000
nipana, and this is why it is said here, it is precisely by means of these 15 things

120
00:14:23,000 --> 00:14:26,760
that a noble disciple contacts himself, that he goes towards the dead death.

121
00:14:26,760 --> 00:14:42,720
That is why it is called pari-charana, and then we will go to the next one.

122
00:14:42,720 --> 00:14:55,800
That line, paragraph 33, now please remember the funny word here, Suh-gata, because the

123
00:14:55,800 --> 00:15:01,160
English translation subline does not make much sense here.

124
00:15:01,160 --> 00:15:11,680
So Suh-gata, Suh means in the first meaning is good.

125
00:15:11,680 --> 00:15:18,960
So Suh means good, and Kata means going, so Suh-gata means good going.

126
00:15:18,960 --> 00:15:24,400
That means Buddha has a good going, that is called Buddha, that is why Buddha has called

127
00:15:24,400 --> 00:15:35,840
Suh-gata, because his going is purified and blameless, and what is that, that is a noble

128
00:15:35,840 --> 00:15:47,880
thought, so noble, it is called he has a good going, so that is the first meaning, Suh-gata.

129
00:15:47,880 --> 00:16:02,760
Now the second meaning is, also gone to an excellent place, now via gata means gone, not

130
00:16:02,760 --> 00:16:08,200
like at the first meaning, and the first meaning Kata means going, but in the second meaning

131
00:16:08,200 --> 00:16:15,920
Kata means gone to, Suh means an excellent place, so one who has gone to an excellent place,

132
00:16:15,920 --> 00:16:25,400
is called Suh-gata, here excellent place is Nibana, now the third meaning of having gone

133
00:16:25,400 --> 00:16:36,480
rightly, now Suh can mean also rightly, so who has gone rightly, is called Suh-gata means,

134
00:16:36,480 --> 00:16:47,800
and rightly here means not approaching to the extremes, but following the middle path, and

135
00:16:47,800 --> 00:16:56,360
the last one, the fourth meaning, because of enunciations rightly, now here the original

136
00:16:56,360 --> 00:17:07,720
word is Suh-gata, with a D, D as in David, and that D is changed to T, it is a polygrammatical

137
00:17:07,720 --> 00:17:17,520
explanation, so here it means Suh-gata, Kata means speaking or saying, and Suh means rightly,

138
00:17:17,520 --> 00:17:29,960
so one who speaks rightly is called Suh-gata, so there are four meanings to this word,

139
00:17:29,960 --> 00:17:42,600
so with regard to the fourth meaning, the Soda is quoted here, paragraph 35, such speaks

140
00:17:42,600 --> 00:17:47,320
as the perfect one knows to be untrue and incorrect, consciousness is too harm and displeasing

141
00:17:47,320 --> 00:17:54,760
and unwelcome to others, that it does not speak and so on, now according to the Soda,

142
00:17:54,760 --> 00:18:07,040
there are six kinds of speeches, and only two of them, Buddha's Jews, so those six are mentioned

143
00:18:07,040 --> 00:18:17,440
here in the translation of the Soda, so in brief they mean speech which is untrue, harmful

144
00:18:17,440 --> 00:18:28,040
and displeasing, Buddha's do not use such speech, and then speech which is true, but harmful

145
00:18:28,040 --> 00:18:37,480
and displeasing to the heroes, this also Buddha does do not use, and the third one is true

146
00:18:37,480 --> 00:18:45,880
beneficial, that means not harming, true beneficial, but displeasing to the listeners, that

147
00:18:45,880 --> 00:19:03,040
Buddha's use, but in any translation it is said, perfect one knows the time to expand,

148
00:19:03,040 --> 00:19:08,280
that means if it is time to say such such a speech, then Buddha's use this speech, so it

149
00:19:08,280 --> 00:19:21,000
may not be pleasing to the listeners, but if it is beneficial and it is true, then Buddha

150
00:19:21,000 --> 00:19:33,800
will say, that means if you follow his words and you will get benefits, or if you follow

151
00:19:33,800 --> 00:19:46,960
his words, you will come to the goal, opposite of that, you will come to failure or something,

152
00:19:46,960 --> 00:19:54,240
sometimes people talk to other people, people give advice to other people, and those

153
00:19:54,240 --> 00:20:08,880
advice may not be conducive to success, so which is true beneficial and displeasing, Buddha's

154
00:20:08,880 --> 00:20:19,720
use such speeches, then the fourth one untrue, harmful, but pleasing to other people, Buddha

155
00:20:19,720 --> 00:20:31,120
never use this, and then true, harmful, pleasing, no, Buddha never use, then the last one,

156
00:20:31,120 --> 00:20:37,920
true beneficial and pleasing to the listeners, so Buddha's use only two kinds of, one of

157
00:20:37,920 --> 00:20:44,640
the two kinds of speeches, true beneficial, displeasing or true beneficial pleasing,

158
00:20:44,640 --> 00:20:51,640
say, the other Buddha does not use, that is where Buddha is described as sukita, one

159
00:20:51,640 --> 00:21:07,040
who speaks rightly, then Noah of the world, Loka we do, you may read this, it is something

160
00:21:07,040 --> 00:21:19,600
like in any commentary, something like a Buddhist cosmology, and not all of them can actually

161
00:21:19,600 --> 00:21:25,880
be found in the sukhas, some of them can be found in the sukhas, and others have developed

162
00:21:25,880 --> 00:21:34,880
maybe later after the death of the Buddha, so you may take them as just a duplicate.

163
00:21:34,880 --> 00:21:56,720
Because if you look at the world right now, according to the knowledge of modern science

164
00:21:56,720 --> 00:22:11,280
about the world, then they are very different from, and a lot Indians are maybe the western

165
00:22:11,280 --> 00:22:19,720
people say Indians are a form of using numbers, say 84,000, something like that, so

166
00:22:19,720 --> 00:22:28,000
here also Noah of the world, now there are three kinds of words mentioned here, the

167
00:22:28,000 --> 00:22:34,720
world of formations, the world of beings and the world of location, you will find that in

168
00:22:34,720 --> 00:22:43,000
paragraph 37, the world of formations, the world of beings and the world of location,

169
00:22:43,000 --> 00:22:51,400
the world of formations really means, the world of both animates and animates, both beings

170
00:22:51,400 --> 00:22:56,600
and animates, the world of beings means just beings and the world of location means

171
00:22:56,600 --> 00:23:06,240
outside of the world, and so Buddha, I mean in animates world, like the earth and

172
00:23:06,240 --> 00:23:18,120
all the angels and so on, and if you can draw a diagram of what is mentioned here, you

173
00:23:18,120 --> 00:23:27,480
will get a rough picture of the Buddhist conception of the world and the other things,

174
00:23:27,480 --> 00:23:40,480
I mean with the sun and moon and the universe, then paragraph 38, the first word likewise,

175
00:23:40,480 --> 00:23:52,440
I think should go, not likewise, but it should be 4 or maybe in detail, because paragraph

176
00:23:52,440 --> 00:24:04,000
38 and those following are the detailed description of how Buddha knows the world, so one

177
00:24:04,000 --> 00:24:15,600
world being subsets by a new human and so on, and then paragraph 39, he knows all

178
00:24:15,600 --> 00:24:22,360
beings' habits, that is very important, he knows all beings' habits, knows they are in here

179
00:24:22,360 --> 00:24:27,640
and tendencies, knows their temperament, knows their bench, knows them as with little dust

180
00:24:27,640 --> 00:24:33,440
in their eyes and with much dust on their eyes, with keen faculties and with dull faculties,

181
00:24:33,440 --> 00:24:38,320
with good behavior and with bad behavior, each to teach and easy to teach and hard to

182
00:24:38,320 --> 00:24:44,280
teach, capable and incapable of achievement, therefore this world of beings was known to

183
00:24:44,280 --> 00:24:53,720
him in all ways, so Buddha, only Buddhas have this ability of knowing exactly the beings'

184
00:24:53,720 --> 00:25:04,000
habits in here and tendencies and so on, even the other hands do not possess this ability,

185
00:25:04,000 --> 00:25:16,960
and then go to understanding of the physical world, they developed, there are some statements

186
00:25:16,960 --> 00:25:26,040
with regard to the physical world and then they developed later on, so this description

187
00:25:26,040 --> 00:25:31,360
of the world according to the Buddhists.

188
00:25:31,360 --> 00:25:41,800
And also the food node is very helpful and informative with regard to this, you get a view

189
00:25:41,800 --> 00:25:51,720
of the Buddhist universe, Buddhist universe is a round, round thing, not like a globe,

190
00:25:51,720 --> 00:26:00,120
but it is round and it is surrounded by what we call the wild cycle mountains, so there

191
00:26:00,120 --> 00:26:09,600
are mountains around and then in this circle there is water and also the full great

192
00:26:09,600 --> 00:26:16,600
islands, we call them the full great islands, maybe the continents and each island has

193
00:26:16,600 --> 00:26:26,280
decided in the books, it believe it or not, each island has 500 small islands and in the

194
00:26:26,280 --> 00:26:34,400
middle of this universe there is the Mount Siniru or Meiru and it is not visible to human

195
00:26:34,400 --> 00:26:42,720
beings and surrounding the Meiru and this mountain, great mountain, there are seven

196
00:26:42,720 --> 00:26:52,600
constantly mountains and lower mountains, seven and in between these seven there is water,

197
00:26:52,600 --> 00:27:00,680
so they are called inner oceans or something, there is the outer ocean and then there

198
00:27:00,680 --> 00:27:09,480
are inner ocean, seven inner oceans and they are the high, the Siniru is it is 4000

199
00:27:09,480 --> 00:27:16,880
leagues and then the first toilet, the highest mountain, the surrounding mountain is half

200
00:27:16,880 --> 00:27:23,040
that high and then next one is half that height and so on, but if you want to draw according

201
00:27:23,040 --> 00:27:33,120
to scale it is very difficult, I tried it but I cannot, because you have to draw a big drawing

202
00:27:33,120 --> 00:27:48,360
not on small paper, because half height, half height, half height, half height, half

203
00:27:48,360 --> 00:27:51,920
and that is called one, one wall cycle, let us say one universe and suns and moon are

204
00:27:51,920 --> 00:28:02,400
said to go round this, round the Meiru, the middle, middle mountain and it is half height and

205
00:28:02,400 --> 00:28:22,960
so the sun and moon are only 24, 42,000 leagues from the earth and the size of the sun

206
00:28:22,960 --> 00:28:29,720
is 50 leagues and the size of the moon is 49 leagues, there is only one league difference

207
00:28:29,720 --> 00:28:36,720
between the size of the moon and size of the sun and then the stars and planets go

208
00:28:36,720 --> 00:28:48,280
round them and it is said that when the sun goes round the Meiru, then it throws a

209
00:28:48,280 --> 00:28:56,760
shell right and so when three continents are light one continent is dark and that is night

210
00:28:56,760 --> 00:29:01,760
and the others are day.

211
00:29:01,760 --> 00:29:13,200
So only one quarter of the universe is dark and the three three three, the other three

212
00:29:13,200 --> 00:29:24,080
fires are light at one time and the, there are four great islands and the south island

213
00:29:24,080 --> 00:29:35,840
is called Jambutiba and it is, it is a place where borders, borders and all white men

214
00:29:35,840 --> 00:29:50,840
and fewer people are born and that land is described as something like white, white, white

215
00:29:50,840 --> 00:29:59,680
one, one, one side and then what about that, you know, slow beam or something, becoming

216
00:29:59,680 --> 00:30:08,840
small and they are the side, so that fits with India, so India is Jambutiba, the southern

217
00:30:08,840 --> 00:30:17,320
island, so if India is southern island then Europe should be western island and China

218
00:30:17,320 --> 00:30:26,320
or Japan should be eastern island and I think America should be in northern island because

219
00:30:26,320 --> 00:30:40,320
when it is day in Asia then it is night here, so America should be the north, north

220
00:30:40,320 --> 00:30:53,520
of Great Island which is called Hotel Acuro, so this is the cosmology of Buddhism.

221
00:30:53,520 --> 00:31:12,240
Now, there was one theme on page 220, you see the paragraph number 43, those line 1, 2,

222
00:31:12,240 --> 00:31:19,840
3, 4, 5, they are misplaced, the Wall sphere mountains line of summits plunges down

223
00:31:19,840 --> 00:31:28,360
and you can see just 280,000 lakes and towers up in like 80 and ringing one wall element

224
00:31:28,360 --> 00:31:37,400
all down in pitch and tiredly, so this line should be on page 221 just above paragraph

225
00:31:37,400 --> 00:31:47,080
44 and not here, I do not know why, why he put this here, maybe by mistake, so this line

226
00:31:47,080 --> 00:32:02,600
should be on the other page, page 2021, now here you see you find the moon's disk is 49

227
00:32:02,600 --> 00:32:11,440
leaves across and the sun's disk is 50 leaves, therefore this wall of location was known

228
00:32:11,440 --> 00:32:17,200
to him in all ways to, so he is north of walls because he has seen the wall in all ways,

229
00:32:17,200 --> 00:32:26,960
what I call it, local, we do know of the wall, know of the wall of being or physical wall

230
00:32:26,960 --> 00:32:39,080
and wall of formations, a wall of conditioned things, therefore I both call know of the walls

231
00:32:39,080 --> 00:32:50,200
and when we, when we read the subcommamentaries, we find more of these descriptions, I

232
00:32:50,200 --> 00:32:57,720
don't know where they come from, I actually not from the soldiers, but I don't know

233
00:32:57,720 --> 00:33:07,440
what they made, they really got them from some writings of Hindus, because there are such

234
00:33:07,440 --> 00:33:17,840
books in Hinduism and so they may have got them from them, but they are not important actually

235
00:33:17,840 --> 00:33:33,960
and one thing is, see one thing is, Himalayas is 500 leaves high, 500 leaves means, one

236
00:33:33,960 --> 00:33:53,840
leaf is about eight miles, so how many miles, 4000 miles high, sometimes they measure

237
00:33:53,840 --> 00:34:04,400
not by height, maybe by path, so to reach the summit, you have to, you have to, you do

238
00:34:04,400 --> 00:34:12,680
climb, say, a number of miles, say, more temple fires is how many miles high, one mile

239
00:34:12,680 --> 00:34:22,840
or two miles high, two miles high, not from the sea level, I mean measuring, like with

240
00:34:22,840 --> 00:34:29,840
the poles, like measuring by path, we say as the crow flies, direct, this is not as

241
00:34:29,840 --> 00:34:38,360
the crow flies, no, that's why we are so many miles, say, Himalayas is 500 miles high

242
00:34:38,360 --> 00:34:52,520
and so on, then you know, it may not be, how many miles high, it's about average it's

243
00:34:52,520 --> 00:35:09,640
like my side, but do you think I may refer to this, the wall as we know it, the universe

244
00:35:09,640 --> 00:35:14,480
as we know it, or if it's some kind of a mythical type of a thing because as you indicate

245
00:35:14,480 --> 00:35:25,240
that the mirror cannot be seen by human, so it may be, you know, some kind of a symbolic

246
00:35:25,240 --> 00:35:41,520
type of thing, but you know, one statement, convert things another, because it is said

247
00:35:41,520 --> 00:35:50,280
that when three islands are like, one island is dark, so following this, this wall

248
00:35:50,280 --> 00:35:57,960
is what is meant by the poloka and then our poles, because, you know, one part of the

249
00:35:57,960 --> 00:36:05,000
wall is dark when the others are light, not exactly one, one quarter, but something like

250
00:36:05,000 --> 00:36:16,600
that, maybe they are knowledge of geography is very limited in those days and then

251
00:36:16,600 --> 00:36:24,920
there may be sages who just contemplate and then talked about these things and they came

252
00:36:24,920 --> 00:36:34,080
to be accepted, you know, nobody seemed to bother about measuring the height of Himalayas

253
00:36:34,080 --> 00:36:43,400
of Mount Everest and they had no way, no means of measuring the height of mountain in

254
00:36:43,400 --> 00:36:56,040
those days, so they may be just measuring by, by path, but they are not really important

255
00:36:56,040 --> 00:37:20,040
things, okay, so next time, maybe a bubble, another plenty of business.

256
00:37:20,040 --> 00:37:37,520
Thank you.

