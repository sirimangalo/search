1
00:00:00,000 --> 00:00:10,000
Do Buddhists have prophets, like many other religions?

2
00:00:10,000 --> 00:00:14,000
Do you mean does Buddhism, I suppose?

3
00:00:14,000 --> 00:00:21,000
I'm asking questions like this. It's difficult. Buddhism

4
00:00:21,000 --> 00:00:24,000
Buddhism has many, many different things in it.

5
00:00:24,000 --> 00:00:28,000
Or many people call themselves Buddhist and do very, very strange things.

6
00:00:28,000 --> 00:00:33,000
Just as in the name of any religion people do very strange things.

7
00:00:33,000 --> 00:00:39,000
But Buddhism Buddhism, the scriptural, what we have passed down

8
00:00:39,000 --> 00:00:44,000
that we understand to be kind of how enlightened beings acted,

9
00:00:44,000 --> 00:00:49,000
does actually have some measure of prophecy

10
00:00:49,000 --> 00:00:53,000
or admit to some level of prophecy.

11
00:00:53,000 --> 00:00:56,000
Because there's an example that is given,

12
00:00:56,000 --> 00:00:59,000
the first example is of the Buddha himself, when he was a Bodhisatta,

13
00:00:59,000 --> 00:01:01,000
he had five dreams.

14
00:01:01,000 --> 00:01:06,000
And these dreams were that,

15
00:01:06,000 --> 00:01:12,000
they were images,

16
00:01:12,000 --> 00:01:15,000
they were symbols of certain things that would happen.

17
00:01:15,000 --> 00:01:20,000
Like many birds, he saw flying towards him of all different colors

18
00:01:20,000 --> 00:01:24,000
and they fell down at his feet and all turned to white,

19
00:01:24,000 --> 00:01:27,000
I believe, or saffron, I can't remember what.

20
00:01:27,000 --> 00:01:30,000
And this was that all the people who came to him

21
00:01:30,000 --> 00:01:33,000
would, from many different corners of the earth,

22
00:01:33,000 --> 00:01:36,000
many different walks of life would all come together to ordain,

23
00:01:36,000 --> 00:01:40,000
I think, or to practice his teachings, I can't remember what.

24
00:01:40,000 --> 00:01:44,000
So he had many prophetic dreams about his future.

25
00:01:44,000 --> 00:01:48,000
Or he had these five dreams about his future,

26
00:01:48,000 --> 00:01:51,000
what his future would be.

27
00:01:51,000 --> 00:01:55,000
But the more well-known example is of King,

28
00:01:55,000 --> 00:02:01,000
I believe it's King Poseinadhi, King Poseinadhi.

29
00:02:05,000 --> 00:02:11,000
The five, the 16 dreams of Poseinadhi, I believe.

30
00:02:11,000 --> 00:02:15,000
And these are quite famous in Buddhist circles,

31
00:02:15,000 --> 00:02:18,000
but as usual, I can't remember it accurately.

32
00:02:18,000 --> 00:02:21,000
And these 16 dreams are in the Jataka.

33
00:02:21,000 --> 00:02:26,000
And he had these dreams and he came to ask the Buddha about them.

34
00:02:26,000 --> 00:02:27,000
Yeah, right.

35
00:02:27,000 --> 00:02:28,000
Poseinadhi.

36
00:02:28,000 --> 00:02:31,000
Because he wasn't a very wise person.

37
00:02:31,000 --> 00:02:34,000
And so he listened to his, these brahmanas who told them

38
00:02:34,000 --> 00:02:36,000
that, oh, it means there's going to be a catastrophe.

39
00:02:36,000 --> 00:02:41,000
You have to kill all of these animals in ritual sacrifice.

40
00:02:41,000 --> 00:02:44,000
And we have to perform this ritual sacrifice for you.

41
00:02:44,000 --> 00:02:49,000
Which is, you know, this is the kind of prophecy that you see

42
00:02:49,000 --> 00:02:54,000
in cultural, religion all around the world.

43
00:02:54,000 --> 00:02:58,000
This shisterism, if that's such a word.

44
00:02:58,000 --> 00:03:05,000
It's not Buddhism, it's a bunch of con artists.

45
00:03:05,000 --> 00:03:10,000
Where people will look at your horoscope

46
00:03:10,000 --> 00:03:15,000
or will look at something that happened to you, dream,

47
00:03:15,000 --> 00:03:17,000
trying to remember what it was.

48
00:03:17,000 --> 00:03:20,000
People always come up to me with these examples of how they went

49
00:03:20,000 --> 00:03:24,000
to a religious person and ask them what this meant

50
00:03:24,000 --> 00:03:25,000
or what that meant.

51
00:03:25,000 --> 00:03:28,000
And they would always say, they'll always say,

52
00:03:28,000 --> 00:03:31,000
it means you have to build this or be,

53
00:03:31,000 --> 00:03:32,000
oh, that's right.

54
00:03:32,000 --> 00:03:33,000
This woman told me about this.

55
00:03:33,000 --> 00:03:34,000
She meant to see one teacher.

56
00:03:34,000 --> 00:03:37,000
And he told her, oh, you have very bad karma

57
00:03:37,000 --> 00:03:40,000
and so you have to build, you have to build a kuti

58
00:03:40,000 --> 00:03:42,000
for our monastery and this and this.

59
00:03:42,000 --> 00:03:45,000
And she couldn't believe because she'd just come to visit

60
00:03:45,000 --> 00:03:47,000
this monastery in the beginning.

61
00:03:47,000 --> 00:03:51,000
For the first time, I'm already here with this teacher

62
00:03:51,000 --> 00:03:53,000
giving it, telling her all these ridiculous things

63
00:03:53,000 --> 00:03:57,000
and how he liked her car or something like that.

64
00:03:57,000 --> 00:03:59,000
It was just ridiculous things she was telling me.

65
00:03:59,000 --> 00:04:01,000
Your car is quite nice.

66
00:04:01,000 --> 00:04:04,000
You know, you should get one for the monastery and so on.

67
00:04:04,000 --> 00:04:10,000
Just explaining it all the way based on prophecy

68
00:04:10,000 --> 00:04:16,000
on horoscopes and fate and fortune.

69
00:04:16,000 --> 00:04:18,000
And this is much more common.

70
00:04:18,000 --> 00:04:20,000
So this is what the Brahmins did.

71
00:04:20,000 --> 00:04:23,000
They thought to themselves, here's a way for us to make a living.

72
00:04:23,000 --> 00:04:26,000
And they said, oh, great king, you have to perform a huge sacrifice

73
00:04:26,000 --> 00:04:31,000
and very costly and we will arrange it all and so on.

74
00:04:31,000 --> 00:04:36,000
You see this so common and it's funny how actually Buddhist teachers

75
00:04:36,000 --> 00:04:39,000
can fall into this trap when throughout the suit

76
00:04:39,000 --> 00:04:42,000
does it's ridiculed so blatantly.

77
00:04:42,000 --> 00:04:45,000
And you know, they're taking the wrong example

78
00:04:45,000 --> 00:04:48,000
from the suit as you might say.

79
00:04:48,000 --> 00:04:51,000
But then he went to see the Buddha and his wife

80
00:04:51,000 --> 00:04:54,000
who was actually quite wise, Malika, I believe,

81
00:04:54,000 --> 00:04:57,000
was quite wise, told him to go see the Buddha and said,

82
00:04:57,000 --> 00:04:59,000
what are you doing talking to these ridiculous people

83
00:04:59,000 --> 00:05:02,000
who know nothing, why don't you go ask the Buddha

84
00:05:02,000 --> 00:05:03,000
what these dreams mean?

85
00:05:03,000 --> 00:05:07,000
And the Buddha told him explain to him prophecy

86
00:05:07,000 --> 00:05:09,000
something that is going to happen in the future,

87
00:05:09,000 --> 00:05:11,000
how people are going to act in the future.

88
00:05:11,000 --> 00:05:15,000
He said, these are prophetic dreams and they mean

89
00:05:15,000 --> 00:05:19,000
that in the future this is going to happen, this is going to happen.

90
00:05:19,000 --> 00:05:22,000
And he said, but don't worry, it's not going to happen in your time.

91
00:05:22,000 --> 00:05:26,000
So he was saying it to placate the king.

92
00:05:26,000 --> 00:05:29,000
And you know, if you read through it, you might think actually,

93
00:05:29,000 --> 00:05:33,000
the Buddha is kind of just using it to placate this man,

94
00:05:33,000 --> 00:05:38,000
this befuddled king, because the dreams might not actually

95
00:05:38,000 --> 00:05:43,000
mean anything, but the Buddha is using them as a means

96
00:05:43,000 --> 00:05:46,000
of explaining the truth of the future

97
00:05:46,000 --> 00:05:51,000
that the Buddha himself was able to see.

98
00:05:51,000 --> 00:05:59,000
So the existence of prophets is allowed for and admitted,

99
00:05:59,000 --> 00:06:05,000
but I would say for the most part, prophecy

100
00:06:05,000 --> 00:06:10,000
is a exalted form of shaesturism.

101
00:06:10,000 --> 00:06:15,000
People trying to make a living become a spiritual leader

102
00:06:15,000 --> 00:06:17,000
for this or that purpose.

103
00:06:17,000 --> 00:06:20,000
It's very rare to find someone who is actually prophetic.

104
00:06:20,000 --> 00:06:24,000
And I would say the people who actually do have this kind of understanding

105
00:06:24,000 --> 00:06:28,000
about the future, this kind of supervision and knowledge

106
00:06:28,000 --> 00:06:32,000
of what's going to happen in the future are not at all interested

107
00:06:32,000 --> 00:06:36,000
in going out and proclaiming it to people.

108
00:06:36,000 --> 00:06:39,000
They're much more interested in the practice of meditation

109
00:06:39,000 --> 00:06:43,000
and encouraging people in the practice of meditation.

110
00:06:43,000 --> 00:06:45,000
It's not so useful.

111
00:06:45,000 --> 00:06:48,000
I mean, it could be useful in that sense as the Buddha used it

112
00:06:48,000 --> 00:06:54,000
to explain how the future is not such a bright thing

113
00:06:54,000 --> 00:06:56,000
to look forward to and to try to cultivate

114
00:06:56,000 --> 00:07:02,000
and all of these works that we have put into effect

115
00:07:02,000 --> 00:07:07,000
and that we try to put into effect to benefit society

116
00:07:07,000 --> 00:07:13,000
and are really quite pointless because of what we first see in the future.

117
00:07:13,000 --> 00:07:15,000
I mean, it really doesn't take prophecy

118
00:07:15,000 --> 00:07:19,000
because we know it's going to happen.

119
00:07:19,000 --> 00:07:23,000
We can see the writing on the wall and eventually the earth is going to no longer exist.

120
00:07:23,000 --> 00:07:28,000
This entire solar system, galaxy, universe,

121
00:07:28,000 --> 00:07:33,000
as it is now, is totally going to be obliterated at some point in the future.

122
00:07:33,000 --> 00:07:36,000
We don't need prophecy to tell us that.

123
00:07:36,000 --> 00:07:40,000
I think prophecy is in that sense for a short-sighted

124
00:07:40,000 --> 00:07:43,000
but what would it mean to be able to prophesy something?

125
00:07:43,000 --> 00:07:46,000
It means to see something on the road ahead.

126
00:07:46,000 --> 00:07:49,000
Once you get to it, you just have to keep going.

127
00:07:49,000 --> 00:07:52,000
It's really pointless and meaningless.

128
00:07:52,000 --> 00:07:59,000
I would say a product of folk religion and cultural religion

129
00:07:59,000 --> 00:08:02,000
rather than actual spiritual practice

130
00:08:02,000 --> 00:08:07,000
because it doesn't answer the underlying questions of why.

131
00:08:07,000 --> 00:08:16,000
The broader why, why does it happen like this?

132
00:08:16,000 --> 00:08:40,000
Not what's going to happen, but why does it happen?

