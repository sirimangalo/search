1
00:00:00,000 --> 00:00:05,000
Hi, and welcome back to our study of the Dhamupada.

2
00:00:05,000 --> 00:00:12,000
Today we continue on with verse number 62, which reads as follows.

3
00:00:35,000 --> 00:00:37,000
I have sons.

4
00:00:37,000 --> 00:00:42,000
Nana Mati, I have wealth.

5
00:00:42,000 --> 00:00:44,000
Iti bala wyaniti.

6
00:00:44,000 --> 00:00:52,000
Thus the fool worries himself for his backstories.

7
00:00:52,000 --> 00:00:57,000
His kadap is concerned.

8
00:00:57,000 --> 00:01:00,000
He is distraught by these thoughts.

9
00:01:00,000 --> 00:01:07,000
Nana Mati, one's self, indeed, is not one's self.

10
00:01:07,000 --> 00:01:22,000
The kutto, kutto, dunan, how, therefore, could either sons or wealth belong to one's self.

11
00:01:22,000 --> 00:01:33,000
The story behind the verse, the story goes that there was this rich man called Ananda.

12
00:01:33,000 --> 00:01:41,000
Ananda sati, an rich man named Ananda.

13
00:01:41,000 --> 00:01:45,000
And he taught his children.

14
00:01:45,000 --> 00:01:49,000
He lived his life and taught his children to not spend anything,

15
00:01:49,000 --> 00:01:59,000
to never give away anything, because wealth is difficult to gain.

16
00:01:59,000 --> 00:02:04,000
It's hard to hold onto, and it can vanish at any time,

17
00:02:04,000 --> 00:02:06,000
so you have to work very, very hard to keep it,

18
00:02:06,000 --> 00:02:13,000
and keep every coin accounted for.

19
00:02:13,000 --> 00:02:19,000
And so over his years of cultivating wealth and obsessing over wealth,

20
00:02:19,000 --> 00:02:29,000
he came to view it as something that needed protecting.

21
00:02:29,000 --> 00:02:33,000
He got so wound up that every coin should be accounted for

22
00:02:33,000 --> 00:02:36,000
and should never give anything to anyone.

23
00:02:36,000 --> 00:02:43,000
To never give away or foolishly spend your wealth.

24
00:02:43,000 --> 00:02:46,000
Reminds us of the rich man with the pancakes,

25
00:02:46,000 --> 00:02:49,000
but this is a different man.

26
00:02:49,000 --> 00:02:58,000
It seems to be that wealth, there's a common theme in the

27
00:02:58,000 --> 00:03:03,000
polycan in a rich richness, often leading to miserliness,

28
00:03:03,000 --> 00:03:11,000
which of course you see in modern day as well.

29
00:03:11,000 --> 00:03:15,000
There are some examples of rich people who are very generous

30
00:03:15,000 --> 00:03:19,000
and kinder, and many rich people who can only think about

31
00:03:19,000 --> 00:03:21,000
how they can amass more wealth.

32
00:03:21,000 --> 00:03:22,000
You can get more and more.

33
00:03:22,000 --> 00:03:26,000
There's a little Buddha said for this reason,

34
00:03:26,000 --> 00:03:31,000
even if it were to rain gold,

35
00:03:31,000 --> 00:03:36,000
you would never get enough for even one person.

36
00:03:36,000 --> 00:03:40,000
So this is story of Anand, and not much of a story,

37
00:03:40,000 --> 00:03:44,000
because he passed away and left his fortune to his son.

38
00:03:44,000 --> 00:03:47,000
And the story is actually about his reincarnation.

39
00:03:47,000 --> 00:03:50,000
When he was reborn, he was reborn as an outcast.

40
00:03:50,000 --> 00:03:55,000
In a village of, it says around a thousand outcast

41
00:03:55,000 --> 00:04:07,000
or beggars or poor unemployed outcasts,

42
00:04:07,000 --> 00:04:09,000
or unable, of course, to get any,

43
00:04:09,000 --> 00:04:13,000
but the worst of jobs, and that they'll live either off begging

44
00:04:13,000 --> 00:04:17,000
or off slavery and slave labor, etc.

45
00:04:17,000 --> 00:04:21,000
Now, from the moment that he was conceived in his mother's womb,

46
00:04:21,000 --> 00:04:27,000
the entire village was unable to get anything

47
00:04:27,000 --> 00:04:33,000
to make even the slightest bit of money or food.

48
00:04:33,000 --> 00:04:35,000
They weren't able to beg.

49
00:04:35,000 --> 00:04:37,000
They weren't able to work.

50
00:04:37,000 --> 00:04:39,000
And the moment that he was conceived,

51
00:04:39,000 --> 00:04:41,000
he cursed the whole village.

52
00:04:41,000 --> 00:04:44,000
So the story goes.

53
00:04:44,000 --> 00:04:49,000
Some kind of group karma and work, I guess.

54
00:04:49,000 --> 00:04:54,000
And so they split the village.

55
00:04:54,000 --> 00:04:56,000
They got together and they said something must,

56
00:04:56,000 --> 00:04:58,000
something must be wrong here.

57
00:04:58,000 --> 00:04:59,000
Someone must be causing this.

58
00:04:59,000 --> 00:05:02,000
And so they split the village in half.

59
00:05:02,000 --> 00:05:06,000
And had them go in separate locations

60
00:05:06,000 --> 00:05:08,000
and go begging or go working or whatever.

61
00:05:08,000 --> 00:05:12,000
And they found that one half was able to get,

62
00:05:12,000 --> 00:05:16,000
to get by fine as per normal.

63
00:05:16,000 --> 00:05:20,000
The other group still was, as it were, cursed.

64
00:05:20,000 --> 00:05:25,000
And so they cut that group in half and then cut the other group

65
00:05:25,000 --> 00:05:29,000
in half and then half again until they got down to two families

66
00:05:29,000 --> 00:05:30,000
and they split them up.

67
00:05:30,000 --> 00:05:33,000
And they found that this woman with this pregnant woman

68
00:05:33,000 --> 00:05:35,000
was the cause of all the problems.

69
00:05:35,000 --> 00:05:41,000
So they kicked them out and sent them on their way.

70
00:05:41,000 --> 00:05:47,000
And then when he was born, his parents found the same thing

71
00:05:47,000 --> 00:05:51,000
that if they went on arms alone or if they went out working

72
00:05:51,000 --> 00:05:54,000
or whatever alone, then they could get money just fine

73
00:05:54,000 --> 00:05:59,000
and they could get by as well as could be expected.

74
00:05:59,000 --> 00:06:02,000
But if he was with them, they would all get nothing.

75
00:06:02,000 --> 00:06:06,000
So they kicked him out as well and gave him a little piece of,

76
00:06:06,000 --> 00:06:10,000
maybe a piece of pot or something.

77
00:06:10,000 --> 00:06:17,000
And said, go, you must spare for yourself.

78
00:06:17,000 --> 00:06:19,000
They kept him around until he was like seven years old

79
00:06:19,000 --> 00:06:21,000
and then they sent him off.

80
00:06:21,000 --> 00:06:22,000
Go by yourself.

81
00:06:22,000 --> 00:06:24,000
The other thing is he was very, very ugly.

82
00:06:24,000 --> 00:06:26,000
So he kind of looked like an ogre.

83
00:06:26,000 --> 00:06:34,000
And this is the horrible repercussions of being such a miser.

84
00:06:34,000 --> 00:06:39,000
And one day he was wandering around trying to get whatever

85
00:06:39,000 --> 00:06:42,000
arms he could, which of course wasn't much.

86
00:06:42,000 --> 00:06:44,000
He wandered back to his old home.

87
00:06:44,000 --> 00:06:47,000
And he saw his house and he recognized it.

88
00:06:47,000 --> 00:06:49,000
And so he just walked right in.

89
00:06:49,000 --> 00:06:51,000
And he started looking around the house,

90
00:06:51,000 --> 00:06:52,000
not quite sure why he was there,

91
00:06:52,000 --> 00:06:55,000
but I looked at him and looked very familiar and kind of like home.

92
00:06:55,000 --> 00:06:58,000
And so he was walking around and he went into one room

93
00:06:58,000 --> 00:07:01,000
and there were his grandchildren, his son's sons.

94
00:07:01,000 --> 00:07:03,000
And they freaked out.

95
00:07:03,000 --> 00:07:05,000
And they called out and they said monster.

96
00:07:05,000 --> 00:07:07,000
There was a monster in there.

97
00:07:07,000 --> 00:07:13,000
And the servants came over and beat him and threw him out

98
00:07:13,000 --> 00:07:15,000
just as the Buddha was walking by.

99
00:07:15,000 --> 00:07:17,000
And so the Buddha's walking by going on arms around

100
00:07:17,000 --> 00:07:22,000
and he sees this young beggar beaten to a pulp

101
00:07:22,000 --> 00:07:27,000
or beaten quite severely, lying on the side of the road.

102
00:07:27,000 --> 00:07:28,000
Not to a pulp.

103
00:07:28,000 --> 00:07:29,000
He's still alive.

104
00:07:29,000 --> 00:07:32,000
He's beaten up and the Buddha looks at him.

105
00:07:32,000 --> 00:07:35,000
And then he looks at Anandah, who is our Anandah,

106
00:07:35,000 --> 00:07:38,000
the rich Anandah, walking beside him.

107
00:07:38,000 --> 00:07:40,000
He turns to look at Anandah.

108
00:07:40,000 --> 00:07:49,000
Anandah knows to take this as an instigation,

109
00:07:49,000 --> 00:07:52,000
to ask the question.

110
00:07:52,000 --> 00:07:55,000
So he asks the Buddha.

111
00:07:55,000 --> 00:07:59,000
What's the story of this guy, Reverend Sir?

112
00:07:59,000 --> 00:08:03,000
And the Buddha said he used to be the great rich man Anandah.

113
00:08:03,000 --> 00:08:06,000
And this was his house.

114
00:08:06,000 --> 00:08:11,000
Anandah called the rich man's son.

115
00:08:11,000 --> 00:08:14,000
And the Buddha explained to him,

116
00:08:14,000 --> 00:08:16,000
this was your father.

117
00:08:16,000 --> 00:08:18,000
And he said, I don't believe it looks at him.

118
00:08:18,000 --> 00:08:20,000
And he's this ugly outcast.

119
00:08:20,000 --> 00:08:24,000
And he said, how can he go from a rich man to a beggar?

120
00:08:24,000 --> 00:08:26,000
And the Buddha had him go in the house

121
00:08:26,000 --> 00:08:28,000
and find out all his treasures.

122
00:08:28,000 --> 00:08:31,000
And the kid was able to actually remember where everything was

123
00:08:31,000 --> 00:08:33,000
and so he proved it.

124
00:08:33,000 --> 00:08:36,000
And then the Buddha taught this verse.

125
00:08:36,000 --> 00:08:38,000
Simple story.

126
00:08:38,000 --> 00:08:43,000
And the point here is to remind us not to be negligent.

127
00:08:43,000 --> 00:08:45,000
It's a simple lesson.

128
00:08:45,000 --> 00:08:48,000
When we think of material possessions,

129
00:08:48,000 --> 00:08:51,000
all of those things, not just put them done that

130
00:08:51,000 --> 00:08:55,000
with our sons and our wealth, but everything.

131
00:08:55,000 --> 00:09:01,000
And look at all of our belongings, all of our

132
00:09:01,000 --> 00:09:04,000
in-joyments in the material realm,

133
00:09:04,000 --> 00:09:07,000
thinking of them as me as mine,

134
00:09:07,000 --> 00:09:09,000
as somehow controllable.

135
00:09:09,000 --> 00:09:14,000
So we either hold on to them as ours to enjoy,

136
00:09:14,000 --> 00:09:16,000
or we hold on to ours to control,

137
00:09:16,000 --> 00:09:21,000
or ours to own, or ours to dwell in.

138
00:09:21,000 --> 00:09:23,000
Or our body, we think of it as ourselves,

139
00:09:23,000 --> 00:09:25,000
our house, we think of it as ourselves,

140
00:09:25,000 --> 00:09:27,000
our car, our bedroom, everything.

141
00:09:27,000 --> 00:09:30,000
All of our family, we think of them as our family,

142
00:09:30,000 --> 00:09:33,000
our friends, we think of our friends.

143
00:09:33,000 --> 00:09:40,000
And so we get caught up in this habit of

144
00:09:40,000 --> 00:09:43,000
expectation of being able to control

145
00:09:43,000 --> 00:09:45,000
of being able to rely upon,

146
00:09:45,000 --> 00:09:49,000
of being able to enjoy all of these things.

147
00:09:49,000 --> 00:09:52,000
And the Buddha said, even yourself is not yourself.

148
00:09:52,000 --> 00:09:56,000
It's funny, the more common one that people

149
00:09:56,000 --> 00:09:58,000
know is that he had to know not to,

150
00:09:58,000 --> 00:10:01,000
which is, you wonder whether one of them is actually,

151
00:10:01,000 --> 00:10:04,000
or the other one is actually,

152
00:10:04,000 --> 00:10:06,000
but anyway, the Buddha taught both ways,

153
00:10:06,000 --> 00:10:09,000
that he had to know not to himself is a refuge of self,

154
00:10:09,000 --> 00:10:13,000
which means one is one's own refuge.

155
00:10:13,000 --> 00:10:15,000
But that's referring to the force that he put on,

156
00:10:15,000 --> 00:10:18,000
and one makes a refuge by practicing

157
00:10:18,000 --> 00:10:22,000
on one's own, not relying on anyone else.

158
00:10:22,000 --> 00:10:24,000
But even that one can't control,

159
00:10:24,000 --> 00:10:29,000
one can't rely upon one's expectations,

160
00:10:29,000 --> 00:10:32,000
one can't be fulfilled in one's desires,

161
00:10:32,000 --> 00:10:38,000
and in one's demands.

162
00:10:38,000 --> 00:10:43,000
So as a result,

163
00:10:43,000 --> 00:10:49,000
itimbalo yanyati, a fool, is vexed by these thoughts.

164
00:10:49,000 --> 00:10:52,000
They think of their children, and their loved ones,

165
00:10:52,000 --> 00:10:56,000
and all of the people in their life as being controllable,

166
00:10:56,000 --> 00:11:00,000
and they're not able to control these things,

167
00:11:00,000 --> 00:11:03,000
throw these people, then they suffer.

168
00:11:03,000 --> 00:11:05,000
They're vexed, they're worried all the time,

169
00:11:05,000 --> 00:11:07,000
worried about how they might be,

170
00:11:07,000 --> 00:11:10,000
and trying to figure out ways to control people,

171
00:11:10,000 --> 00:11:11,000
ways to control their family,

172
00:11:11,000 --> 00:11:14,000
to control their friends, ways to control their employers,

173
00:11:14,000 --> 00:11:18,000
their employees, their co-workers, and so on and so on.

174
00:11:18,000 --> 00:11:22,000
How can I control people to be the way I want them to be?

175
00:11:22,000 --> 00:11:24,000
And the same goes with our done now,

176
00:11:24,000 --> 00:11:26,000
our possessions are well.

177
00:11:26,000 --> 00:11:28,000
All of our belongings, guarding our houses,

178
00:11:28,000 --> 00:11:31,000
guarding our valuables,

179
00:11:31,000 --> 00:11:33,000
guarding our possessions,

180
00:11:33,000 --> 00:11:35,000
guarding our enjoyment,

181
00:11:35,000 --> 00:11:42,000
regarding our own bodies, being careful to make sure

182
00:11:42,000 --> 00:11:46,000
that we can always enjoy the pleasure that we enjoy now.

183
00:11:46,000 --> 00:11:48,000
But none of this can be controlled,

184
00:11:48,000 --> 00:11:51,000
and when it is out of our control,

185
00:11:51,000 --> 00:11:53,000
we're vexed by it,

186
00:11:53,000 --> 00:11:56,000
especially foolish people who really believe

187
00:11:56,000 --> 00:11:58,000
that these things are going to make them happy,

188
00:11:58,000 --> 00:12:01,000
people who believe in the world bringing happiness,

189
00:12:01,000 --> 00:12:06,000
when they're being vexed quite often,

190
00:12:06,000 --> 00:12:07,000
because of their lack of wisdom,

191
00:12:07,000 --> 00:12:08,000
they're not able to see this,

192
00:12:08,000 --> 00:12:11,000
and so as a result, they constantly vexed.

193
00:12:11,000 --> 00:12:14,000
They do it again and again and again,

194
00:12:14,000 --> 00:12:16,000
doing the same thing over and over again,

195
00:12:16,000 --> 00:12:18,000
expecting different results,

196
00:12:18,000 --> 00:12:23,000
forgetting that they are constantly upset.

197
00:12:23,000 --> 00:12:24,000
The window reminds us,

198
00:12:24,000 --> 00:12:25,000
even ourselves,

199
00:12:25,000 --> 00:12:27,000
even ourselves is not ourselves.

200
00:12:27,000 --> 00:12:28,000
What this means is,

201
00:12:28,000 --> 00:12:30,000
we can't even control ourselves.

202
00:12:30,000 --> 00:12:32,000
We can make choices,

203
00:12:32,000 --> 00:12:33,000
we can have intentions,

204
00:12:33,000 --> 00:12:35,000
but we can't really control it.

205
00:12:35,000 --> 00:12:36,000
We can't say,

206
00:12:36,000 --> 00:12:39,000
I'm going to be happy all the time,

207
00:12:39,000 --> 00:12:42,000
or I'm going to be calm all the time,

208
00:12:42,000 --> 00:12:45,000
we can't say that we're never going to feel pain,

209
00:12:45,000 --> 00:12:46,000
or we're not going to get angry,

210
00:12:46,000 --> 00:12:48,000
we're not going to get upset,

211
00:12:48,000 --> 00:12:51,000
but we can't say this.

212
00:12:51,000 --> 00:12:53,000
Even about our own minds,

213
00:12:53,000 --> 00:12:56,000
even about our own being.

214
00:12:56,000 --> 00:12:59,000
So how then could we possibly do this?

215
00:12:59,000 --> 00:13:02,000
How could we possibly hold on to these things?

216
00:13:02,000 --> 00:13:06,000
How then could we expect that we're going to

217
00:13:10,000 --> 00:13:14,000
enjoy the possession of these things forever?

218
00:13:14,000 --> 00:13:17,000
How could we possibly forget that we're going to lose them all

219
00:13:17,000 --> 00:13:18,000
when we die?

220
00:13:18,000 --> 00:13:20,000
This is what Ananda Saiti does,

221
00:13:20,000 --> 00:13:23,000
I didn't realize.

222
00:13:23,000 --> 00:13:28,000
And really the most impressive part of this story

223
00:13:28,000 --> 00:13:30,000
is how quickly he was gone,

224
00:13:30,000 --> 00:13:31,000
this is what shocked his son.

225
00:13:31,000 --> 00:13:33,000
He was unable to believe that,

226
00:13:33,000 --> 00:13:34,000
just like that,

227
00:13:34,000 --> 00:13:35,000
one moment,

228
00:13:35,000 --> 00:13:36,000
the moment of death,

229
00:13:36,000 --> 00:13:38,000
turned him from a rich,

230
00:13:38,000 --> 00:13:40,000
powerful,

231
00:13:40,000 --> 00:13:44,000
influential person

232
00:13:44,000 --> 00:13:45,000
to a nobody,

233
00:13:45,000 --> 00:13:47,000
to the lowest of the low,

234
00:13:47,000 --> 00:13:50,000
an outcast of the outcast.

235
00:13:50,000 --> 00:13:52,000
But that's the truth,

236
00:13:52,000 --> 00:13:54,000
that's what the Buddha means by its not self.

237
00:13:54,000 --> 00:13:57,000
You can't even hold on to your own being,

238
00:13:57,000 --> 00:13:59,000
your own status.

239
00:13:59,000 --> 00:14:02,000
I think I'm a monk, the moment I die,

240
00:14:02,000 --> 00:14:03,000
that's gone.

241
00:14:03,000 --> 00:14:05,000
You think you're a human being,

242
00:14:05,000 --> 00:14:06,000
the moment you die,

243
00:14:06,000 --> 00:14:07,000
that's gone,

244
00:14:07,000 --> 00:14:09,000
you can be the next moment you can be an earthworm,

245
00:14:09,000 --> 00:14:11,000
or a dung beetle.

246
00:14:11,000 --> 00:14:12,000
Everything that we collect,

247
00:14:12,000 --> 00:14:14,000
everything that we hold on to,

248
00:14:14,000 --> 00:14:16,000
no security for us,

249
00:14:16,000 --> 00:14:18,000
all these people in some cultures,

250
00:14:18,000 --> 00:14:20,000
they will bury their belongings with them,

251
00:14:20,000 --> 00:14:22,000
thinking that they can bring it into their next life,

252
00:14:22,000 --> 00:14:26,000
or they'll burn certain objects.

253
00:14:26,000 --> 00:14:28,000
In Thailand and China,

254
00:14:28,000 --> 00:14:33,000
they burn these really crappy houses,

255
00:14:33,000 --> 00:14:35,000
and fake stuff and everything,

256
00:14:35,000 --> 00:14:36,000
and you think, wow,

257
00:14:36,000 --> 00:14:38,000
they're going to get a lot of fake stuff when they go to have the ideas

258
00:14:38,000 --> 00:14:40,000
that they can take that stuff with them if they burn it.

259
00:14:40,000 --> 00:14:42,000
Since it's all garbage,

260
00:14:42,000 --> 00:14:44,000
you think, well, it's not really.

261
00:14:44,000 --> 00:14:45,000
Even if it did work.

262
00:14:45,000 --> 00:14:47,000
Of course, it doesn't work.

263
00:14:47,000 --> 00:14:48,000
You can't possess,

264
00:14:48,000 --> 00:14:50,000
we don't possess anything.

265
00:14:50,000 --> 00:14:53,000
We just happen to be going in the same direction as all this stuff.

266
00:14:53,000 --> 00:14:57,000
We'll be in the same physical locale, right?

267
00:14:57,000 --> 00:15:00,000
We manage to make choices that bring all this stuff near us,

268
00:15:00,000 --> 00:15:02,000
and that's it.

269
00:15:02,000 --> 00:15:03,000
We can't do any better than that.

270
00:15:03,000 --> 00:15:05,000
We can't control.

271
00:15:05,000 --> 00:15:10,000
Even our enjoyment is fleeting and ephemeral,

272
00:15:10,000 --> 00:15:13,000
something that cannot be relied upon,

273
00:15:13,000 --> 00:15:15,000
since it's very much.

274
00:15:15,000 --> 00:15:17,000
Out of our control,

275
00:15:17,000 --> 00:15:22,000
based on causes and conditions that are far more powerful than a new one of us.

276
00:15:22,000 --> 00:15:25,000
So our reminder for us,

277
00:15:25,000 --> 00:15:27,000
and in regards to meditation,

278
00:15:27,000 --> 00:15:30,000
our reminder for us to keep our minds clear

279
00:15:30,000 --> 00:15:32,000
and to be clear about all the things that we use.

280
00:15:32,000 --> 00:15:35,000
It doesn't mean that we shouldn't be using our possessions.

281
00:15:35,000 --> 00:15:38,000
It doesn't mean we shouldn't.

282
00:15:41,000 --> 00:15:43,000
We shouldn't live,

283
00:15:43,000 --> 00:15:46,000
but it means that when we do experience even pleasure,

284
00:15:46,000 --> 00:15:50,000
or when you're eating food,

285
00:15:50,000 --> 00:15:53,000
when you're seeing beautiful things,

286
00:15:53,000 --> 00:15:55,000
the pleasure that comes from that.

287
00:15:57,000 --> 00:16:00,000
But we should see it simply as a feeling.

288
00:16:00,000 --> 00:16:02,000
We should see the experience simply as an experience.

289
00:16:02,000 --> 00:16:05,000
The experience is seeing the hearing,

290
00:16:05,000 --> 00:16:07,000
smelling, tasting, feeling.

291
00:16:07,000 --> 00:16:10,000
And to see liking as liking,

292
00:16:10,000 --> 00:16:13,000
just liking as disliking.

293
00:16:13,000 --> 00:16:15,000
And come to see that these things are not self,

294
00:16:15,000 --> 00:16:16,000
are not under control.

295
00:16:16,000 --> 00:16:18,000
Try to keep our minds clear.

296
00:16:18,000 --> 00:16:21,000
Because all of this will catch you when you die.

297
00:16:21,000 --> 00:16:23,000
And if you have great wanting,

298
00:16:23,000 --> 00:16:25,000
so someone who is very, very rich

299
00:16:25,000 --> 00:16:28,000
because they're clinging so much,

300
00:16:28,000 --> 00:16:30,000
what goes with them is the clinging.

301
00:16:30,000 --> 00:16:33,000
And that's why you're born very, very poor.

302
00:16:33,000 --> 00:16:36,000
That's why rich people are very quick to be born very poor

303
00:16:36,000 --> 00:16:38,000
because they have so much greed and attachment.

304
00:16:38,000 --> 00:16:41,000
And it's that want that goes with you.

305
00:16:41,000 --> 00:16:42,000
So you're born wanting.

306
00:16:42,000 --> 00:16:45,000
You're born in a constant state of want.

307
00:16:45,000 --> 00:16:47,000
I'm asking, well, it doesn't make you wealthy.

308
00:16:47,000 --> 00:16:50,000
Being generous and kind.

309
00:16:50,000 --> 00:16:54,000
And having a mind of surplus.

310
00:16:54,000 --> 00:16:56,000
When you think you have surplus,

311
00:16:56,000 --> 00:16:58,000
when you're content with it,

312
00:16:58,000 --> 00:17:00,000
you'll always be content.

313
00:17:00,000 --> 00:17:02,000
You'll never know the words nutty.

314
00:17:02,000 --> 00:17:04,000
When you want something,

315
00:17:04,000 --> 00:17:05,000
you'll never be discontent.

316
00:17:05,000 --> 00:17:06,000
You'll never know discontent

317
00:17:06,000 --> 00:17:08,000
because your mind is content.

318
00:17:08,000 --> 00:17:10,000
It's funny how it works that way.

319
00:17:13,000 --> 00:17:16,000
The mind that is clinging is the mind that is corrupt.

320
00:17:16,000 --> 00:17:19,000
And clinging to self is the worst type of corruption

321
00:17:19,000 --> 00:17:23,000
because it leads to all other likes and dislikes.

322
00:17:23,000 --> 00:17:25,000
So we have to keep our minds clear.

323
00:17:25,000 --> 00:17:27,000
This is why meditation is important.

324
00:17:27,000 --> 00:17:29,000
When we live our lives, however, you live your lives.

325
00:17:29,000 --> 00:17:31,000
To keep your minds clear.

326
00:17:31,000 --> 00:17:35,000
And to not cling to self either in regards to your own self

327
00:17:35,000 --> 00:17:37,000
or in regards to the things that you enjoy.

328
00:17:37,000 --> 00:17:44,000
So a nice lesson from the Dhammapada and from our friend,

329
00:17:44,000 --> 00:17:50,000
the former Anandasati will be managed to find a way out of that mist.

330
00:17:50,000 --> 00:17:54,000
So thank you all for tuning in and wish you all peace,

331
00:17:54,000 --> 00:18:18,000
happiness and freedom from suffering.

