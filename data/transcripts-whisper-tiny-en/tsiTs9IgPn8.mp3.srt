1
00:00:00,000 --> 00:00:07,160
Francesca Donatella says you just made me scared so much freedom seems dangerous

2
00:00:11,880 --> 00:00:15,920
Lucas Lucas looks scared. Yeah, are you scared of freedom?

3
00:00:19,720 --> 00:00:21,720
Well, we don't even know what the freedom is

4
00:00:21,720 --> 00:00:38,400
It's called xenophobia freedom of the end of fear of the unknown to me it makes perfect sense. It's like

5
00:00:39,920 --> 00:00:42,600
You you suppose you had a bottomless pit

6
00:00:42,600 --> 00:00:53,120
Or no, that's me. That's silly that the idea of infinity of having no bottom or of having nothing to cling to

7
00:00:53,760 --> 00:00:57,640
Right of infinite possibility. It's quite scary, you know

8
00:00:58,520 --> 00:01:02,120
There's no reference point. There's there's no guiding point, right?

9
00:01:03,240 --> 00:01:05,240
And it's scary to have no

10
00:01:05,920 --> 00:01:08,080
no

11
00:01:08,080 --> 00:01:16,320
No reference point nothing nothing to fall back. I mean, it's really easy to believe in Jesus Christ, right? Okay, that's easy. Right. Bye. I've got it

12
00:01:17,360 --> 00:01:19,680
There you've got a reference point. Buddhism

13
00:01:21,480 --> 00:01:23,480
In the sense that I'm

14
00:01:23,480 --> 00:01:25,480
Explaining it doesn't have a reference point

15
00:01:27,480 --> 00:01:34,080
But you know, and I made the video I did qualify that because there is one no nibana is a reference point

16
00:01:34,080 --> 00:01:41,480
So you strive to strive towards the bond that here and now it would be also the

17
00:01:42,200 --> 00:01:44,200
reference point, right? It's another good one

18
00:01:45,800 --> 00:01:51,600
So yeah, you don't need purpose to have a reference point. You don't need to be going the another thing in mind

19
00:01:51,600 --> 00:01:55,920
So you don't need to be going somewhere to have a reference point, right? So

20
00:01:57,920 --> 00:02:01,760
Well, yes, we have freedom and and that makes you scared to do anything

21
00:02:01,760 --> 00:02:08,360
But in the sense Buddhism is not about doing something. It's about coming back to in a sense not doing anything

22
00:02:09,120 --> 00:02:11,120
So yeah, the present moment is an excellent

23
00:02:12,480 --> 00:02:14,480
It's an excellent

24
00:02:14,600 --> 00:02:16,600
point that

25
00:02:18,800 --> 00:02:24,680
We have we have something to cling to it's just in a sentence. It's the present moment

26
00:02:24,680 --> 00:02:26,680
Yeah

27
00:02:28,960 --> 00:02:30,960
Hi

28
00:02:30,960 --> 00:02:32,960
Lucas

29
00:02:32,960 --> 00:02:37,600
Yeah, I just want to refer to it one more time. There was a film

30
00:02:38,040 --> 00:02:40,840
He also for her called Seneca

31
00:02:41,440 --> 00:02:43,440
And if you said that

32
00:02:43,440 --> 00:02:46,200
But if you still have a fear in you

33
00:02:46,880 --> 00:02:49,040
Yeah, you're definitely I'm not free

34
00:02:49,040 --> 00:02:55,480
Scary of the shears, then you could become a free or know the freedom

35
00:02:56,040 --> 00:02:58,440
So how it could be afraid of

36
00:02:59,520 --> 00:03:01,520
Being of being free

37
00:03:02,440 --> 00:03:04,440
Is a state without

38
00:03:04,440 --> 00:03:06,440
here

39
00:03:07,040 --> 00:03:15,800
Well, there's another thing as well is that the fear itself is a reference point the fear itself is something to not cling to but quote unquote cling to in the sense of

40
00:03:15,800 --> 00:03:21,000
You're afraid of you know, you're afraid of this and it paralyzes you

41
00:03:21,560 --> 00:03:25,920
Well, don't go further than the fear, which you know, it's of course the present moment, but

42
00:03:27,720 --> 00:03:34,400
That's I think a general important point is that when you are angry about something well, the anger is the most important thing when you have

43
00:03:34,400 --> 00:03:37,240
When you're afraid of something well, the fear is the most important thing

44
00:03:37,960 --> 00:03:39,560
So

45
00:03:39,560 --> 00:03:44,600
This goes with such philosophical questions as is there is a purpose to that is there a purpose to that well

46
00:03:44,600 --> 00:03:46,960
How do you feel about the question? How do you feel about the answer?

47
00:03:47,800 --> 00:03:50,760
Makes me afraid well, and what much more important is the fear

48
00:03:52,000 --> 00:03:55,920
Welcome Troy Troy. Do you want to tell us about yourself? Where are you?

49
00:03:56,920 --> 00:03:59,200
I'm in Canada. I have two small kids here

50
00:03:59,200 --> 00:04:03,960
So I might go on mute and I bet some comes disruptive. Please let me know what part of Canada

51
00:04:03,960 --> 00:04:16,640
I'm in Toronto. Cool. I think you're getting more recently Canadian or Canadian citizens should I say I'm Canadian a yeah, I'm sorry. No, I'll be in Toronto in April

52
00:04:16,640 --> 00:04:18,640
I'm coming back. Oh, really? Yeah

53
00:04:19,640 --> 00:04:22,880
I'm gonna try to set up. I'll be living be staying in Caledan

54
00:04:23,960 --> 00:04:25,960
I'm trying to set up a center there. Oh

55
00:04:25,960 --> 00:04:33,480
Oh, actually, is there if there's any post you put up in that regard? I'll be paid attention. Yeah, you can you also join our

56
00:04:34,240 --> 00:04:42,080
If you look over my web blog, I think my latest post at utadamodatsurimongo.org is there's a link to our mailing list

57
00:04:42,080 --> 00:04:46,240
If you want if you're Canadian if you're in Canada, you want to get on our mailing list find out

58
00:04:46,760 --> 00:04:51,760
Get emails about updates and get involved and you're welcome to join that

59
00:04:51,760 --> 00:05:01,040
Thank you. Sorry. I'll put myself on mute. In fact, I probably should go. I think I'll put it here. Oh, yeah. Oh, thanks for the thanks for coming

60
00:05:07,240 --> 00:05:09,240
Hello, connect

61
00:05:12,320 --> 00:05:14,320
Yeah

62
00:05:14,320 --> 00:05:19,200
You're done recording right? No

