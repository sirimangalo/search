1
00:00:00,000 --> 00:00:26,000
Good evening, everyone, broadcasting live from Stony Creek, Ontario, August 7, 2015.

2
00:00:26,000 --> 00:00:35,000
Here we have another verse on Mehta, on a verse passage.

3
00:00:35,000 --> 00:00:38,000
This one is actually one of the more popular passages.

4
00:00:38,000 --> 00:00:45,000
It may not be very well known in the West, but in Thailand and in Asia we actually, some places they'll chant it.

5
00:00:45,000 --> 00:00:51,000
When I stayed at the Thai monastery in Los Angeles, they would chant this every day.

6
00:00:51,000 --> 00:01:15,000
Subscribe to Mehta!

7
00:01:15,000 --> 00:01:39,660
It sounds nice in the poly.

8
00:01:39,660 --> 00:01:57,900
So here we have 11, 8th, 8th, 8th, 8th, 9th, 9th, 8th, 7th, 11th.

9
00:01:57,900 --> 00:01:59,440
So I saw people are asking, what are they?

10
00:01:59,440 --> 00:02:00,520
11.

11
00:02:00,520 --> 00:02:06,160
Because in English it's not clear, but if you click on the link to the poly, if you're using

12
00:02:06,160 --> 00:02:13,120
Firefox and you have the digital poly reader installed and on top of that you

13
00:02:13,120 --> 00:02:22,560
click on the link it will take you to the poly and if you read that you can see in

14
00:02:22,560 --> 00:02:27,480
the second paragraph it's clear of course you have to also know poly. A lot of

15
00:02:27,480 --> 00:02:37,640
apps. So kang supati one dreams or sleeps in happiness because it's not one is

16
00:02:37,640 --> 00:02:46,880
not bothered by guilt or remorse, hatred or fear.

17
00:02:46,880 --> 00:03:00,280
So kang party bhudshati one wakes up with happily so does the wake up feeling

18
00:03:00,280 --> 00:03:05,720
guilty or bad about the things that they've done either the papa kang supinang

19
00:03:05,720 --> 00:03:16,600
bhudshati one doesn't see any evil dreams because one's mind is clear. Love is

20
00:03:16,600 --> 00:03:32,320
like this pure vibe that keeps your mind on a on a wavelength on a positive

21
00:03:32,320 --> 00:03:44,400
wavelength. So the bad brain waves can't arise in the bad mental activity is

22
00:03:44,400 --> 00:03:51,320
quite it's quite just because the mental activity is unimpositable. I mean

23
00:03:51,320 --> 00:03:55,840
there's probably a more organic way to explain it but basically it's it's

24
00:03:55,840 --> 00:04:06,080
like that it's like a good vibes and good vibrations and have the effect of

25
00:04:06,080 --> 00:04:15,920
focusing one's one's state of mind. So one doesn't see bad dreams either.

26
00:04:15,920 --> 00:04:22,340
Manusan and Biyohoti one is dear to humans.

27
00:04:22,340 --> 00:04:35,120
Up Manusan and Biyohoti one is dear to non-humans. Non-humans is usually in

28
00:04:35,120 --> 00:04:40,920
modern times the day they use the word to mean like ghosts or evil spirits but

29
00:04:40,920 --> 00:04:47,520
I'm gonna say could mean any non-human could mean animals. People who have love are

30
00:04:47,520 --> 00:04:52,840
very good with animals. People who have made that. You can often tell someone

31
00:04:52,840 --> 00:05:00,560
someone's love by how good they are with animals. How good they are with children.

32
00:05:00,560 --> 00:05:07,200
How good they are with animals. Usually you can't tell how good they are with

33
00:05:07,200 --> 00:05:14,240
ghosts or angels or so. It looks so easy.

34
00:05:14,240 --> 00:05:23,200
But the angels guard the person. They're guarded by angels. Angels see fit to

35
00:05:23,200 --> 00:05:28,720
guard because they appreciate the goodness of the person. They try to make

36
00:05:28,720 --> 00:05:33,800
sure that no harm comes to this being. Apparently even though we can't

37
00:05:33,800 --> 00:05:48,200
normally see them this apparently happens. Nasa-agiywa-wisan-wa-sak-tang-wa-kamati.

38
00:05:48,200 --> 00:06:08,160
Fire poison and swords do not affect one. Do not affect one. They can't penetrate

39
00:06:08,160 --> 00:06:19,040
it. It's the meaning. Not for that person to unge and not into that person to

40
00:06:19,040 --> 00:06:24,040
fire and poison and weapons enter. So a person would love if they have this.

41
00:06:24,040 --> 00:06:29,320
Remember we're talking here about Jitou-i-mouti which means a state of Jhana of

42
00:06:29,320 --> 00:06:35,880
intense super kind of super mundane concentration. It's still mundane but it's

43
00:06:35,880 --> 00:06:42,160
beyond the central realm. So there's a state of vibration that is so intense

44
00:06:42,160 --> 00:06:49,120
that it actually takes one out of this sphere. And so the idea is that weapons

45
00:06:49,120 --> 00:06:53,320
don't even penetrate. You can get hit with a sword or shot with a gun. They talk

46
00:06:53,320 --> 00:06:58,280
about this in Thailand. It's possible for there's been cases where people were

47
00:06:58,280 --> 00:07:04,360
monks or meditators were shot with guns and it didn't hit them somehow.

48
00:07:04,360 --> 00:07:11,320
I mean the truth of it is something else but this is the idea. This is something

49
00:07:11,320 --> 00:07:16,760
apparently the Buddha said. Poison, if you're full of love there's a strength of

50
00:07:16,760 --> 00:07:27,360
mind to it. So how many do we have? One, so kang-su-pati, dreams, sleeps in happiness,

51
00:07:27,360 --> 00:07:35,200
two wakes in happiness, three no evil dreams, four dear to humans, five dear to

52
00:07:35,200 --> 00:07:43,360
non-humans, six angels, a garden, two person, seven these things don't hurt

53
00:07:43,360 --> 00:07:53,880
a person. Tu-wa-tang-ji-tang samadhi, number eight, one gets a well-established

54
00:07:53,880 --> 00:07:59,080
concentration. Mine concentrates a Tu-wa-tang, it's quickly. Mine becomes

55
00:07:59,080 --> 00:08:03,800
concentrated quickly because one doesn't have to deal with negativity. The

56
00:08:03,800 --> 00:08:07,720
mind is full of positivity wishing only good things.

57
00:08:07,720 --> 00:08:25,400
positivity supports the mind. 9. Mukovano, we've received a tea when there's a radiant complexion.

58
00:08:25,400 --> 00:08:35,880
Number 10. One dies with it. Asamu-ho-galang-karate, one makes one's time, means

59
00:08:35,880 --> 00:08:45,000
dies, makes an end to one's time. Asamu-l-ho, without confusion and confused

60
00:08:45,000 --> 00:08:49,480
because the mind is so sharp and strong and focused, clearly aware of what is

61
00:08:49,480 --> 00:08:58,120
good. And if one doesn't further develop insight meditation,

62
00:08:58,120 --> 00:09:07,560
but the wheat genet, wheat genet, one doesn't develop further. Brahmalok-pago-ho-tang,

63
00:09:07,560 --> 00:09:14,360
one is set to be a right to arise in the Brahbalok.

64
00:09:14,360 --> 00:09:22,160
So, yeah, love is a great thing. Love is awesome, it has these benefits. But

65
00:09:22,160 --> 00:09:26,080
there's clearly a limit to those benefits. There's no how great they are.

66
00:09:26,080 --> 00:09:35,040
Finally, there's a provision that it can only lead so far.

67
00:09:35,040 --> 00:09:42,000
It leads to big born as a god or to arise in the Brahmalok, which is still

68
00:09:42,000 --> 00:09:50,800
finite and still a part of samsara. Nonetheless, it's great and it's quite

69
00:09:50,800 --> 00:09:54,840
helpful in the practice of insight meditation. Today it's fortuitous or

70
00:09:54,840 --> 00:10:01,360
serendipitous or it is serendipitous. That this verse comes today. Today we

71
00:10:01,360 --> 00:10:12,280
were invited to for lunch at a restaurant in Aaron Mills, I think, or some

72
00:10:12,280 --> 00:10:19,480
more near, some more Mississauga, Toronto area. And I wasn't really keen to

73
00:10:19,480 --> 00:10:25,440
go, I said, I got a call and said, hey, you want to go, I said, no, I'm not really.

74
00:10:25,440 --> 00:10:28,800
Oh, I think I know what it was. I knew it was there was a monk who'd visited

75
00:10:28,800 --> 00:10:33,360
here and he's such a kind monk. Someone who's so full of love, you could just get

76
00:10:33,360 --> 00:10:38,080
us feeling for how kind he is. Well, if I don't go, it's going to be

77
00:10:38,080 --> 00:10:44,080
disappointing to him. And you see how great his love is able to manipulate

78
00:10:44,080 --> 00:10:48,720
people without trying, he manipulates me and the one thing to go because I

79
00:10:48,720 --> 00:10:54,040
wouldn't want to disappoint someone so full of love. So I went and it turned out

80
00:10:54,040 --> 00:11:02,480
it was a meeting of all the many of the monks who had helped organize or been

81
00:11:02,480 --> 00:11:08,760
involved in the celebration of Weisak in that's in celebration square in

82
00:11:08,760 --> 00:11:16,160
Mississauga in May. So I don't have any pictures I don't think, but there's lots of

83
00:11:16,160 --> 00:11:20,360
pictures I'm sure up on Facebook by now. If you check out Bhandi Serna Paula's

84
00:11:20,360 --> 00:11:27,360
page, I bet he's got some up. But it was awesome. There was so much just in the

85
00:11:27,360 --> 00:11:31,760
short time that we were together for lunch, maybe two hours. There was such

86
00:11:31,760 --> 00:11:40,360
love and just kindness and you get a sense of how great it is to be in good

87
00:11:40,360 --> 00:11:47,400
company. How great it is to be with people who have this love and this caring.

88
00:11:47,400 --> 00:11:51,240
And then you can also feel where there's not so much love. Like there are times

89
00:11:51,240 --> 00:11:57,920
where the famines arise and so you get a sense of that person who's not

90
00:11:57,920 --> 00:12:03,520
really in the spirit, you know, that kind of thing. And you can taste you can tell

91
00:12:03,520 --> 00:12:13,480
that you can taste the difference, kind of different flavor to it. So there's no

92
00:12:13,480 --> 00:12:19,280
one, no, never underestimate the power, the greatness of love. It's a wonderful

93
00:12:19,280 --> 00:12:25,200
thing and brings lots of benefits. So that's the dumb of her today. Now we can

94
00:12:25,200 --> 00:12:29,600
get into some questions. Does anybody have any questions? I'm going to stipulate

95
00:12:29,600 --> 00:12:32,640
that they should probably be about meditation because I don't want to get

96
00:12:32,640 --> 00:12:40,400
overwhelmed here. So let's try to keep them to meditation question.

97
00:12:40,400 --> 00:13:02,400
What do you do if you have a bad dream but cannot wake up?

98
00:13:02,400 --> 00:13:12,960
The problem of dreaming is there's no mindfulness, the variability that's

99
00:13:12,960 --> 00:13:20,280
required to grasp to grasp what's happening and to experience it clearly and

100
00:13:20,280 --> 00:13:28,160
to realize this is a dream is generally lacking. As soon as you realize it's a

101
00:13:28,160 --> 00:13:34,560
dream you wake up, I think there are people I guess who have lucid dreams. But in

102
00:13:34,560 --> 00:13:42,480
that case, it's much less a dream. I don't know so much about lucid dreaming.

103
00:13:49,600 --> 00:13:57,360
Anyway, I don't know. I'd probably wake up. It's not that big of a deal. It

104
00:13:57,360 --> 00:14:01,040
happens. It's not much you can do about it. The more you meditate the less you

105
00:14:01,040 --> 00:14:07,760
dream, certainly the less bad dreams you have. Prevention, prevent yourself from

106
00:14:07,760 --> 00:14:16,160
having the bad dreams in the first place. They're generally a sign of guilt or

107
00:14:16,160 --> 00:14:29,920
other negative mind stays.

108
00:14:37,840 --> 00:14:41,040
So if you're listening, if you're watching on YouTube and you want to know what's

109
00:14:41,040 --> 00:14:45,200
going on here, we're actually asking and we're doing the question thing in

110
00:14:45,200 --> 00:14:52,960
another place. We're doing questions at meditation.ceremungalow.org and so I'm

111
00:14:52,960 --> 00:14:57,280
not answering questions on YouTube right now. I'm reading questions.

112
00:14:57,280 --> 00:15:02,080
People are posting in our chat, chat box, our shout box over at

113
00:15:02,080 --> 00:15:07,920
meditation.ceremungalow.org. But I'm also broadcasting simultaneously on

114
00:15:07,920 --> 00:15:18,320
YouTube. Just because there's lots of people on YouTube.

115
00:15:19,360 --> 00:15:24,240
We recommend practice of the ability to lucid dreaming. Well, no.

116
00:15:24,240 --> 00:15:28,000
No, I teach based on a specific meditation practice. I think there's a link.

117
00:15:28,000 --> 00:15:32,160
Yes, there's a link to the booklet up near the top of the page. So if you click on

118
00:15:32,160 --> 00:15:40,880
that link, you'll see what I recommend.

119
00:15:54,000 --> 00:15:57,520
Any advice for countering the attachment to the idea of progress in the

120
00:15:57,520 --> 00:16:03,440
meditation practice or such, desire acceptable. It's acceptable, I'd say, in the

121
00:16:03,440 --> 00:16:07,840
beginning. It's in the end, you do away with it.

122
00:16:11,360 --> 00:16:20,080
Beginning, you're doing everything wrong. In the end, you're just mindful.

123
00:16:20,080 --> 00:16:29,040
There's no countering. We're not in charge here that we can counter.

124
00:16:29,040 --> 00:16:34,720
We're just trying to dodge the bullets. Duck and weave, duck and weave.

125
00:16:34,720 --> 00:16:39,920
Not exactly duck and weave, like avoid, but be in the right place in the right

126
00:16:39,920 --> 00:16:46,000
situation. Experience every experience with the right frame of mind, with a clear

127
00:16:46,000 --> 00:16:51,600
frame of mind. It's like dodging bullets, in a sense,

128
00:16:51,600 --> 00:17:00,080
dodge all the problems. It takes a lot of maneuvering flexibility.

129
00:17:05,760 --> 00:17:10,160
Are there any landmarks besides Nimbana that one will continue

130
00:17:10,160 --> 00:17:18,080
on the right path in the next life? No, no, no. Yeah, there's one and it's called

131
00:17:18,080 --> 00:17:23,200
julesso dapana. If one retains at least the second stage of knowledge,

132
00:17:23,200 --> 00:17:29,360
the tradition goes that such a person will not be born in a bad state in

133
00:17:29,360 --> 00:17:34,640
the next life. Just the reaching the second, so understanding about it,

134
00:17:34,640 --> 00:17:39,280
seeing cause and effect, practicing meditation to the point where you

135
00:17:39,280 --> 00:17:42,160
experience cause and effect, and you're able to see

136
00:17:42,160 --> 00:17:46,080
how the body and the mind work together and how good deeds lead to.

137
00:17:46,080 --> 00:17:52,400
Good results, bad deeds or bad mind states lead to bad results.

138
00:17:58,240 --> 00:18:03,440
How much control does a novice have over how much formal practice they can do?

139
00:18:03,440 --> 00:18:07,760
Or how much can a novice expect to be able to formally practice?

140
00:18:07,760 --> 00:18:12,880
I guess it sounds like you have the idea that Buddhism is somehow

141
00:18:12,880 --> 00:18:21,520
monolithic, homogenous. Every monastery you go to is different.

142
00:18:21,520 --> 00:18:26,400
You can't ask me that unless you're talking about coming here toward you.

143
00:18:26,400 --> 00:18:36,240
So, ordination is a vehicle. It depends very much what you do with it and where you go with it.

144
00:18:38,160 --> 00:18:42,640
And so yeah, it depends also on your teachers. Maybe they make you sweep out the

145
00:18:42,640 --> 00:18:49,280
latrines every day. So we sweep the forest. In Thailand, they like to sweep the

146
00:18:49,280 --> 00:18:52,400
forest. It's just one of the most absurd things to

147
00:18:52,400 --> 00:18:59,280
we have a very different view from over here and they'll sweep out the

148
00:18:59,280 --> 00:19:03,840
entire forest. So there's no more leaves on the ground and then they'll burn out the leaves.

149
00:19:06,160 --> 00:19:09,280
Keeps the snakes away and the scorpions I guess.

150
00:19:09,280 --> 00:19:24,000
Well, if you came to ordain here, you would have lots and lots of time to meditate conceivably.

151
00:19:27,680 --> 00:19:31,920
Because there's not much else to do. I mean, you could learn poly. You could have to do

152
00:19:31,920 --> 00:19:43,360
polycourses and study you can do. But you certainly would have time to do courses. You can be

153
00:19:43,360 --> 00:19:45,920
expected to do meditation courses regularly.

154
00:19:53,440 --> 00:19:57,280
When you recommend to meditate right before going to bed for preventing bad dreams, yeah,

155
00:19:57,280 --> 00:20:08,800
it's a good recommendation. Can you talk about tensions in the body, especially in the jaw?

156
00:20:08,800 --> 00:20:12,240
Is it okay to let the jaw relax and have the mouth hang open when meditating?

157
00:20:13,120 --> 00:20:17,040
Yeah, sure. Your mouth dries. It tends to dry out if you do that.

158
00:20:18,880 --> 00:20:22,000
You can let it relax, but maybe relaxed and closed. It's better.

159
00:20:22,000 --> 00:20:30,720
There's nothing wrong with tension. As long as you're able to maintain it.

160
00:20:30,720 --> 00:20:58,560
Yeah, insects are a tough one. When I was in Sri Lanka,

161
00:21:01,680 --> 00:21:13,520
I had termites in my cave, and they got into my books and my tent. They actually ate a

162
00:21:13,520 --> 00:21:18,960
hole through the bottom of the tent. There's a mosquito tent that I was sleeping in

163
00:21:19,920 --> 00:21:24,000
to get at the books that were inside the tent. They weren't interested in the plastic,

164
00:21:24,000 --> 00:21:27,520
but it got in their way, so they just ate a hole through the bottom of the tent.

165
00:21:27,520 --> 00:21:43,600
And so I had to find a way to get them out of the cave. So what I did is I funneled them all

166
00:21:43,600 --> 00:21:51,600
into a jar and then I took a funnel, another funnel. I went outside and found a place where the

167
00:21:51,600 --> 00:21:58,160
termites were, and I opened a little hole in it, and I funneled them all into the different termites.

168
00:22:06,880 --> 00:22:13,200
Sorry, what's the question about the repass in a retreat? Don't see the question exactly.

169
00:22:13,200 --> 00:22:24,320
Hopefully I'd have a pro tip. I don't really know what a pro tip is. I mean, I don't recommend,

170
00:22:24,320 --> 00:22:29,280
I can't recommend meditation courses in other traditions, so unless it's in our tradition,

171
00:22:30,160 --> 00:22:33,120
it's not really much I can say. You have to talk to people in that tradition.

172
00:22:33,120 --> 00:22:46,880
Except to say that it's probably a good thing when I can't say too much about other traditions.

173
00:22:56,000 --> 00:22:56,960
Okay, well that's it.

174
00:22:56,960 --> 00:23:07,680
Thank you all for tuning in then, and keep up the practice. We'll see you again tomorrow.

175
00:23:07,680 --> 00:23:37,520
All right, good night.

