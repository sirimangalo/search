1
00:00:00,000 --> 00:00:07,560
something to put a send and we should often remind ourselves of this one of

2
00:00:07,560 --> 00:00:18,940
those pithy sayings that last long after the person who said them is gone one

3
00:00:18,940 --> 00:00:22,440
such saying and you can't say whether it was the Buddha who first said it

4
00:00:22,440 --> 00:00:30,400
is probably the sort of thing that you hear from many people but may may

5
00:00:30,400 --> 00:00:37,080
well have been one of those sayings that came from the Buddha originally is

6
00:00:37,080 --> 00:00:48,000
kano whoma apatanga don't let the moment pass you by

7
00:00:48,000 --> 00:00:59,120
a moment won't you go here is accusative

8
00:00:59,120 --> 00:01:13,120
Ma Upa Tenga means, don't let me the moment not overcome you or go beyond you.

9
00:01:13,120 --> 00:01:23,120
May the moment not pass you by, don't let the moment pass you by.

10
00:01:23,120 --> 00:01:33,120
The moment moves really quickly, you know, life moves quickly.

11
00:01:33,120 --> 00:01:40,120
When we look back, it's frightening how quickly it's all gone, though.

12
00:01:40,120 --> 00:01:46,120
Sometimes people get in the start and are nostalgic thinking about where their lives are gone,

13
00:01:46,120 --> 00:01:58,120
where their lives are gone, disappeared in the infinity of the past.

14
00:01:58,120 --> 00:02:05,120
How quickly the days and weeks and months and years go by.

15
00:02:05,120 --> 00:02:17,120
And even that is not the least of it.

16
00:02:17,120 --> 00:02:21,120
That's not the end of it.

17
00:02:21,120 --> 00:02:32,120
Even our own life, our whole life is just a brief moment in time.

18
00:02:32,120 --> 00:02:49,120
There was a story of an angel, a couple who were in heaven.

19
00:02:49,120 --> 00:02:57,120
And the wife passed away from heaven and was born as a human being.

20
00:02:57,120 --> 00:03:04,120
In the morning, one fine morning they were out for all looking and enjoying themselves in heaven,

21
00:03:04,120 --> 00:03:07,120
and the wife suddenly passed away.

22
00:03:07,120 --> 00:03:12,120
Getting rid of those stories, one of these famous Buddhist stories.

23
00:03:12,120 --> 00:03:15,120
And the memory is so bad.

24
00:03:15,120 --> 00:03:25,120
And so the woman was born as a young girl and grew up.

25
00:03:25,120 --> 00:03:42,120
And remembered her past life as an angel and realized that she had lost her glory in heaven.

26
00:03:42,120 --> 00:03:47,120
And so she worked all her life to try and do good deeds.

27
00:03:47,120 --> 00:03:59,120
She got married, had children, and she was about 60 or 70, I think, when she finally passed away.

28
00:03:59,120 --> 00:04:06,120
And she was born right away in heaven again, except that was after noon.

29
00:04:06,120 --> 00:04:09,120
It was the afternoon of the same day.

30
00:04:09,120 --> 00:04:15,120
So the husband sees her and says, oh, where did you go this morning?

31
00:04:15,120 --> 00:04:21,120
And she had spent a whole lifetime down in on earth.

32
00:04:21,120 --> 00:04:24,120
That's how it is.

33
00:04:24,120 --> 00:04:27,120
That they say the difference between heaven and earth.

34
00:04:27,120 --> 00:04:40,120
And of course the different levels of heaven and so on are yet longer and longer in length.

35
00:04:40,120 --> 00:04:48,120
One human lifetime is just a day in the lower heavens.

36
00:04:48,120 --> 00:04:52,120
And in the higher heavens, one day in those heavens is only,

37
00:04:52,120 --> 00:04:58,120
your one lifetime in those heavens is only a day in the higher heavens or so on.

38
00:04:58,120 --> 00:05:02,120
More and more insanely long.

39
00:05:02,120 --> 00:05:08,120
The point where you're in the Brahma world, in the God world, God realm, they forget.

40
00:05:08,120 --> 00:05:12,120
They forget that they were born in these realms and they think they're eternal.

41
00:05:12,120 --> 00:05:15,120
It's so long.

42
00:05:15,120 --> 00:05:21,120
I just have no ability to recollect back that far.

43
00:05:21,120 --> 00:05:25,120
So they think that they're eternal.

44
00:05:25,120 --> 00:05:29,120
Very easy to think your eternal when you live so long.

45
00:05:29,120 --> 00:05:34,120
Very difficult to see impermanence suffering in non-self.

46
00:05:34,120 --> 00:05:40,120
Impossible, they say in these realms it's impossible to realize the Dhamma.

47
00:05:40,120 --> 00:05:48,120
Because there's no ability to comprehend the three characteristics or something like that.

48
00:05:48,120 --> 00:05:55,120
The point being time moves pretty quickly.

49
00:05:55,120 --> 00:06:01,120
Most so quickly that you think it actually really stands still.

50
00:06:01,120 --> 00:06:08,120
It's like a paradox. It gets so fast that you think it's really meaningless.

51
00:06:08,120 --> 00:06:12,120
I think how long we've lived our lives and it's so pointless.

52
00:06:12,120 --> 00:06:27,120
It's on nothing such a blink in the stream of time.

53
00:06:27,120 --> 00:06:37,120
Given that time is eternal, you think well, actually it doesn't matter whether it gets this moment at all

54
00:06:37,120 --> 00:06:40,120
because there's more moments after it.

55
00:06:40,120 --> 00:06:51,120
It's eternal than there's no reason to concern ourselves.

56
00:06:51,120 --> 00:06:58,120
Which is kind of true. You don't have to practice meditation.

57
00:06:58,120 --> 00:07:02,120
You don't have to cultivate good deeds.

58
00:07:02,120 --> 00:07:07,120
You can see this woman who did great deeds and was born in heaven and then lost it all.

59
00:07:07,120 --> 00:07:12,120
And then she had to spend her whole life just to get it all again.

60
00:07:12,120 --> 00:07:18,120
Her whole life was so much work and effort just to get back in the afternoon.

61
00:07:18,120 --> 00:07:26,120
It was like nothing to her husband who had looked for her during the day and then the afternoon she came back.

62
00:07:26,120 --> 00:07:29,120
Where did she go this evening? Where did she go this morning?

63
00:07:29,120 --> 00:07:32,120
She had only been gone a short time.

64
00:07:32,120 --> 00:07:42,120
Meaning, her whole life was not meaningless, but really very insignificant.

65
00:07:42,120 --> 00:07:53,120
And of course, even having done all those good deeds, her stay in heaven would not be permanent.

66
00:07:53,120 --> 00:08:06,120
But there are some reasons to argue against this idea that we should take things easy and let things go and not worry about this blood concern ourselves about

67
00:08:06,120 --> 00:08:17,120
making use of the time that we have. The first one is that we're born as human beings, which is not a overly common occurrence.

68
00:08:17,120 --> 00:08:23,120
Very difficult for ordinary animals to be born as human.

69
00:08:23,120 --> 00:08:27,120
Much easier for them to go round and round.

70
00:08:27,120 --> 00:08:30,120
Being born in animals is not difficult.

71
00:08:30,120 --> 00:08:40,120
It doesn't take brains, it doesn't take goodness, it doesn't take anything, any sort of special quality.

72
00:08:40,120 --> 00:08:48,120
So you have to do as be stupid. If you have no intelligence, no wisdom, if you don't care about good things,

73
00:08:48,120 --> 00:08:56,120
very easy to be born as an animal.

74
00:08:56,120 --> 00:09:00,120
But to be born as a human is a very special type of animal.

75
00:09:00,120 --> 00:09:08,120
Humans can go against their own genes, go against their own programming.

76
00:09:08,120 --> 00:09:18,120
Humans have the ability that in ordinary animals rarely, if ever takes a bit.

77
00:09:18,120 --> 00:09:28,120
And that is to reprogram themselves.

78
00:09:28,120 --> 00:09:45,120
To question their own intention and to reprogram their habits and their other behaviors.

79
00:09:45,120 --> 00:09:53,120
Humans have the potential to become totally free from bad habits, from unholes from mind states.

80
00:09:53,120 --> 00:10:02,120
The ordinary animals don't have it.

81
00:10:02,120 --> 00:10:06,120
So who knows where we're going to go in the next life?

82
00:10:06,120 --> 00:10:17,120
If we do good deeds, we'll maybe go to heaven, or maybe born against a human being, but who knows?

83
00:10:17,120 --> 00:10:31,120
If we also do bad deeds, so if we're lazy and careless and don't concern ourselves about doing good deeds, let our bad deeds show as well.

84
00:10:31,120 --> 00:10:41,120
And who knows where we'll go? We might wind up in great suffering and get caught up in the trap.

85
00:10:41,120 --> 00:10:53,120
Which, again, is all fine and good because maybe billions and billions and trillions of years from now, or lifetimes from now,

86
00:10:53,120 --> 00:10:57,120
we might have another chance to be born a human being. So it's all good.

87
00:10:57,120 --> 00:11:14,120
That's how you want to live. But given the choice, it does make sense that we should capitalize on the opportunity that we have to be born a human being.

88
00:11:14,120 --> 00:11:24,120
The second is not only we're born a human being, but we're born in the time when Buddhism is to be found in the world.

89
00:11:24,120 --> 00:11:31,120
So we take this from granted because Buddhism is very easy to... Well, we take it from granted because we're good at taking things for granted.

90
00:11:31,120 --> 00:11:35,120
We take for granted whatever we have. This is why the world is such a mess.

91
00:11:35,120 --> 00:11:43,120
We have such great luxury and opulence in the human realm that we've taken it from granted, and now it's disappearing.

92
00:11:43,120 --> 00:11:52,120
The trees are disappearing, the weather is getting bad, the air is becoming poisonous, the water is poisonous.

93
00:11:52,120 --> 00:12:09,120
This is what we're taking for granted. This is the animal side of us. This parasitic nature of existence that you see in animals as well.

94
00:12:09,120 --> 00:12:17,120
The only reason animals don't do it because they're not smart enough. They don't have the brains to take over the way we do.

95
00:12:17,120 --> 00:12:27,120
It's not that they're more conscious of it. In fact, they can see how animals do do when they have the chance.

96
00:12:27,120 --> 00:12:36,120
How prone they are to overrunning and destroying and wreaking havoc on the environment.

97
00:12:36,120 --> 00:12:43,120
The only reason they can is that they don't have the same intellectual capabilities as humans do.

98
00:12:43,120 --> 00:12:51,120
We take a lot for granted and so we lose it. We take it for granted so we don't care for it.

99
00:12:51,120 --> 00:13:00,120
We do the same with the Buddhist teaching. We take it for granted and so we don't care for it and slowly it's wasting away.

100
00:13:00,120 --> 00:13:15,120
Often all people care about our very simple teachings of the Buddha or stories or only interested in it philosophically or intellectually.

101
00:13:15,120 --> 00:13:23,120
In many Buddhist countries they're only interested in memorizing it so they can teach it to others.

102
00:13:23,120 --> 00:13:30,120
But we're not caring for it or we're less caring for it. And of course we've always been this way.

103
00:13:30,120 --> 00:13:43,120
Even in the Buddhist time people didn't really care for the dhamma or they should have and so it deteriorated.

104
00:13:43,120 --> 00:13:52,120
But the point here is it's still here and there's still our people caring for the dhamma, practicing it.

105
00:13:52,120 --> 00:14:02,120
Concerning themselves with cultivating the dhamma within them and becoming practitioners of the dhamma.

106
00:14:02,120 --> 00:14:15,120
We're all fortunate enough to have this greatness, this goodness, this truth in the world, this teaching in the world.

107
00:14:15,120 --> 00:14:27,120
And it's not going to last. We'll know how much longer it will last. The story goes that we've got another 2500 years, which is of course a very little time.

108
00:14:27,120 --> 00:14:34,120
Especially considering the rest of it might be spend as a dog or horse or a dhamma beetle.

109
00:14:34,120 --> 00:14:43,120
We don't know where we're going next. But now we have it and if we don't capitalize on this opportunity that we've got,

110
00:14:43,120 --> 00:14:49,120
it's going to be over sooner than we can think, sooner than we know it.

111
00:14:49,120 --> 00:14:55,120
This is why I'm always encouraging people to ordain. I mean, it seems like a no brainer to me.

112
00:14:55,120 --> 00:15:01,120
It's not like I'm saying everyone should ordain or it's the way of the world to ordain.

113
00:15:01,120 --> 00:15:08,120
But luckily about this very special opportunity, this is like a once in a bazillion year opportunity.

114
00:15:08,120 --> 00:15:15,120
Who knows when we'll get this chance again to be born a human being in the time of the Buddha?

115
00:15:15,120 --> 00:15:31,120
Come, let's put our heart into this. Let's work together and try to purify our minds, try to get rid of these evil and wholesome unskillful tendencies that exist inside of us.

116
00:15:31,120 --> 00:15:37,120
Let's work on this. Let's put our hearts into it.

117
00:15:37,120 --> 00:15:49,120
It's the best chance we have later on if we know what comes next. But now we have this.

118
00:15:49,120 --> 00:16:01,120
The Buddha's teaching here is something that is perfect and pure and really and truly leads to peace, happiness, and freedom from suffering.

119
00:16:01,120 --> 00:16:19,120
So this is the second reason. The third reason is because we are actually have the opportunity.

120
00:16:19,120 --> 00:16:25,120
We have the chance to practice the Buddha's teaching.

121
00:16:25,120 --> 00:16:37,120
So in many cases, there is no opportunity even for people who, well for all the beings who are born in the world.

122
00:16:37,120 --> 00:16:44,120
Very few of them are engaged actively in the practice of the Buddha's teaching.

123
00:16:44,120 --> 00:16:54,120
So many people are caught up in other religions, for example, which we would consider to be wrong view because they are caught up in views and beliefs.

124
00:16:54,120 --> 00:17:03,120
They are caught up in, they are dedicated to beliefs like, if you believe in X, you will go to heaven.

125
00:17:03,120 --> 00:17:15,120
If you don't believe in X, you will go to hell. If you do these rituals, you will be free for eternity and so on.

126
00:17:15,120 --> 00:17:23,120
So many people are of the belief that when you die there is nothing. When you die the mind stops.

127
00:17:23,120 --> 00:17:41,120
All of these we would consider to be wrong view because they are based on speculation or faith or brainwashing in many cases.

128
00:17:41,120 --> 00:17:54,120
So it is rare to find people who get or appreciate the idea of not just scientific investigation, but personal scientific investigation.

129
00:17:54,120 --> 00:18:06,120
Investigation of your own reality, not just reality on a impersonal level, but your own existence, your own experience.

130
00:18:06,120 --> 00:18:12,120
It is very rare to find.

131
00:18:12,120 --> 00:18:19,120
But it is rare to find the opportunity even when we even for those people who want to.

132
00:18:19,120 --> 00:18:24,120
So first of all, it is the opportunity of having your mind set on it.

133
00:18:24,120 --> 00:18:35,120
Second of all, the opportunity of being physically located or physically situated in all aspects.

134
00:18:35,120 --> 00:18:43,120
Such that you can take advantage of this right view.

135
00:18:43,120 --> 00:18:52,120
This is a good intention to practice the number.

136
00:18:52,120 --> 00:18:56,120
So here we are well situated.

137
00:18:56,120 --> 00:19:07,120
We have the texts, we have the thoughts in our mind, the intentions in our mind.

138
00:19:07,120 --> 00:19:11,120
We even have what looks to be a community of meditator.

139
00:19:11,120 --> 00:19:13,120
People interested in meditation.

140
00:19:13,120 --> 00:19:25,120
So we have here daily the chance to at least do some basic group meditation and practice and study.

141
00:19:25,120 --> 00:19:31,120
Dedication to the number.

142
00:19:31,120 --> 00:19:44,120
So there is another reason to take this moment seriously is that we have not only is the Buddha's teaching in the world, but it is here for us.

143
00:19:44,120 --> 00:19:46,120
It is right with us.

144
00:19:46,120 --> 00:19:49,120
All we have to do is start practicing.

145
00:19:49,120 --> 00:20:03,120
All of the factors are in place. We are not starving, we are not in debt or slave slaves to.

146
00:20:03,120 --> 00:20:08,120
We are not enslaved to anyone.

147
00:20:08,120 --> 00:20:12,120
We are not in danger of our lives or our health.

148
00:20:12,120 --> 00:20:19,120
We have all these qualities and then moreover we have a meditation group.

149
00:20:19,120 --> 00:20:23,120
We have a group of people who are of like mind.

150
00:20:23,120 --> 00:20:28,120
We have a place and we have the opportunity to practice.

151
00:20:28,120 --> 00:20:30,120
This is the third reason.

152
00:20:30,120 --> 00:20:39,120
The fourth reason is that while we have taken the opportunity and here we are right, we are ready to practice.

153
00:20:39,120 --> 00:20:43,120
We are ready to do this.

154
00:20:43,120 --> 00:20:44,120
This practice.

155
00:20:44,120 --> 00:20:52,120
So the fourth reason is it is not exactly a reason, but the fourth is how great this moment is.

156
00:20:52,120 --> 00:20:58,120
Why this moment is special is because we have actually not only got the opportunity but we have taken it.

157
00:20:58,120 --> 00:21:03,120
So many people have the opportunity to come here tonight for example.

158
00:21:03,120 --> 00:21:12,120
Many people don't but certainly there are people who know about it and have for some reason rather decided not to become.

159
00:21:12,120 --> 00:21:15,120
There will always be this.

160
00:21:15,120 --> 00:21:29,120
We will always be at times we will be lazy or get sidetracked by other concerns or other attachments.

161
00:21:29,120 --> 00:21:34,120
There are desires that make us want to do other things.

162
00:21:34,120 --> 00:21:38,120
But here we have taken this opportunity and we are here together.

163
00:21:38,120 --> 00:21:47,120
So all that we have left is to make use of it and to practice.

164
00:21:47,120 --> 00:21:48,120
So this is what we are doing.

165
00:21:48,120 --> 00:21:53,120
I mean goodness is not just the Buddha's teaching is not just the practice of meditation.

166
00:21:53,120 --> 00:22:10,120
All that we have done tonight is dedication to the Buddha's teaching and something that we should be happy about or glad about or encouraged by the fact that we have taken this opportunity.

167
00:22:10,120 --> 00:22:11,120
Here we are.

168
00:22:11,120 --> 00:22:15,120
We come in and we don't say bad things to each other.

169
00:22:15,120 --> 00:22:17,120
We don't do bad things.

170
00:22:17,120 --> 00:22:19,120
We don't even think bad thoughts.

171
00:22:19,120 --> 00:22:24,120
We come in and we are respectful and we pay respect to the Buddha.

172
00:22:24,120 --> 00:22:34,120
We offer in sense candles, flowers to the Buddha.

173
00:22:34,120 --> 00:22:42,120
We make offerings to the Buddha out of respect and even from that very act it is a very special thing.

174
00:22:42,120 --> 00:22:47,120
Just thinking of someone who is so pure and perfect.

175
00:22:47,120 --> 00:22:51,120
It puts our mind towards the idea of being pure and perfect.

176
00:22:51,120 --> 00:22:55,120
This is why the Buddha said Bodja, Japoja, Niyana.

177
00:22:55,120 --> 00:23:03,120
When you pay respect or homage to those who are worthy of homage, this is a great blessing.

178
00:23:03,120 --> 00:23:09,120
Because whoever you pay homage to, this is the sort of person that you become.

179
00:23:09,120 --> 00:23:16,120
Whoever you put up as your role model, of course this is who you wind up being more and more light.

180
00:23:16,120 --> 00:23:24,120
When we think about the Buddha and we pay respect to the Buddha, this is putting him up as the ideal.

181
00:23:24,120 --> 00:23:29,120
Whoever this person was, we never met him, but we know this is our ideal.

182
00:23:29,120 --> 00:23:34,120
A person who is free from defilement, a person who is perfectly wise.

183
00:23:34,120 --> 00:23:42,120
A person who has a great compassion to help other beings.

184
00:23:42,120 --> 00:23:57,120
This is the ideal that we maybe don't strive completely for, but we work as close as we can to become a disciple of this great being.

185
00:23:57,120 --> 00:24:03,120
And then we begin chanting, even the chanting, some people may be wonder why we do chanting.

186
00:24:03,120 --> 00:24:07,120
It may be difficult, especially if you don't understand what's being said.

187
00:24:07,120 --> 00:24:13,120
When you don't understand the chanting, you often think it's maybe just some ritual and we don't know why we're doing it.

188
00:24:13,120 --> 00:24:16,120
Maybe it seems like it has one point for some people.

189
00:24:16,120 --> 00:24:19,120
Maybe that's a reason why they don't think to come.

190
00:24:19,120 --> 00:24:23,120
They think that chanting is not really useful.

191
00:24:23,120 --> 00:24:25,120
But then you think these are the words of the Buddha.

192
00:24:25,120 --> 00:24:32,120
So when you hear in God-no-hu-ma-upa-tanga, even just hearing that, that's the words of the Buddha.

193
00:24:32,120 --> 00:24:38,120
It's a great blessing, especially when you know this is the words of the Buddha.

194
00:24:38,120 --> 00:24:42,120
Even if you don't know what's being said, there are these stories of a frog.

195
00:24:42,120 --> 00:24:49,120
For example, it was killed while he was listening to Buddha's teaching, or these bats that fell from the roof of a cave,

196
00:24:49,120 --> 00:24:55,120
while they were listening to monks chanting, they had been done there.

197
00:24:55,120 --> 00:25:05,120
These cases of beings who became, who was minds became pure, not knowing what this was, but knowing that it was somehow holy or somehow special,

198
00:25:05,120 --> 00:25:12,120
and becoming glad and pacified by the words by the speech.

199
00:25:12,120 --> 00:25:23,120
There's something very powerful to the Buddha's teaching, especially when it's in Pali, in a language which was at least very close to what the Buddha actually spoke.

200
00:25:23,120 --> 00:25:42,120
So this is something that helps to purify and to pacify our minds and to help give rise to gladness and peace in the mind.

201
00:25:42,120 --> 00:25:44,120
It's very useful for meditation.

202
00:25:44,120 --> 00:25:53,120
And then we get to listen to the chanting of the Buddha's teaching, especially when you do know what's being said.

203
00:25:53,120 --> 00:25:58,120
So by memorizing the Buddha's teaching, then you always have this in your mind.

204
00:25:58,120 --> 00:26:15,120
It's an idea of being homage to those who are worthy of Oment, and so many other, how many blessings, 37 or something, and any many blessings, 38.

205
00:26:15,120 --> 00:26:29,120
I say, even a jambal, and an panditan, and 30, even a association, not associating with fools, association with the wife.

206
00:26:29,120 --> 00:26:34,120
This is a great passing.

207
00:26:34,120 --> 00:26:58,120
And so on and so many, just the mangalasuta alone is an incredible teaching for my teacher said, if society, if the world had large knew about these blessings and followed in practice, just these basic principles of life, and there would be no conflict in the world.

208
00:26:58,120 --> 00:27:07,120
Just this one two days, it's very, very powerful, so many things in there, maybe not everything, but so many things.

209
00:27:07,120 --> 00:27:11,120
We can see the Buddha's wisdom just in this tutorial alone.

210
00:27:11,120 --> 00:27:14,120
That's why we chanted so often.

211
00:27:14,120 --> 00:27:17,120
It's a good one to remember.

212
00:27:17,120 --> 00:27:20,120
If you can't remember it, you can go by the first letter.

213
00:27:20,120 --> 00:27:23,120
This is how I do it, this is how I can remember.

214
00:27:23,120 --> 00:27:47,120
Remember the first syllable, and then you get it, especially when you, this is how people memorize things.

215
00:27:47,120 --> 00:27:55,120
You find ways to, you find patterns, and it keeps them in the mind.

216
00:27:55,120 --> 00:27:59,120
So for example, a little off topic here, but it's useful.

217
00:27:59,120 --> 00:28:04,120
For example, this isn't quite how I do it, but so I always remember the first one.

218
00:28:04,120 --> 00:28:06,120
Everybody remembers the first one.

219
00:28:06,120 --> 00:28:08,120
Ah, we're saving that.

220
00:28:08,120 --> 00:28:12,120
But the second and the third one, P.B. is peanut butter.

221
00:28:12,120 --> 00:28:18,120
You remember, I don't do that, I never thought of that till now, but that's one way of remembering it.

222
00:28:18,120 --> 00:28:21,120
Because it's so ridiculous that you'll always remember, forget that, no.

223
00:28:21,120 --> 00:28:22,120
A peanut butter.

224
00:28:22,120 --> 00:28:25,120
So you think a peanut butter sandwich, that's how it starts.

225
00:28:25,120 --> 00:28:29,120
I'm sorry, it's not very respectful, but this is how you memorize things.

226
00:28:29,120 --> 00:28:36,120
The second one I do use is B.M.

227
00:28:36,120 --> 00:28:44,120
I don't know if I want to go into it, it's ridiculous, but you figure I would be, B.M. is Bank of Montreal for me, I think.

228
00:28:44,120 --> 00:28:47,120
And then M.D. is a doctor.

229
00:28:47,120 --> 00:28:51,120
So I remember M.D. is a doctor, and so on and so on.

230
00:28:51,120 --> 00:28:53,120
It's very, very tricks that you use.

231
00:28:53,120 --> 00:28:57,120
This is how I learned the manga to sit there.

232
00:28:57,120 --> 00:29:02,120
Anyway, totally off track it, off track, off topic.

233
00:29:02,120 --> 00:29:07,120
But this is, if you learn about how people memorize things, you have to use,

234
00:29:07,120 --> 00:29:13,120
you have to use mnemonic devices, they're called.

235
00:29:13,120 --> 00:29:16,120
Point being, then we have this in our minds.

236
00:29:16,120 --> 00:29:19,120
We have the manga listed with us at all times.

237
00:29:19,120 --> 00:29:20,120
We don't need books.

238
00:29:20,120 --> 00:29:25,120
We can think of it, and we can use it when we're in times of distress.

239
00:29:25,120 --> 00:29:34,120
Even E.T.P. so-and-so-and-so-and-so-and-so-patipano, even just these three are great.

240
00:29:34,120 --> 00:29:35,120
Very powerful.

241
00:29:35,120 --> 00:29:36,120
These are the words of the Buddha.

242
00:29:36,120 --> 00:29:38,120
This isn't someone made up later.

243
00:29:38,120 --> 00:29:46,120
The Buddha himself said, when you're afraid, you can use these, and they're very powerful.

244
00:29:46,120 --> 00:29:53,120
Just by chanting this when you're afraid, or when you have trouble, or when you're confused.

245
00:29:53,120 --> 00:29:55,120
My teacher said, you can even shorten it.

246
00:29:55,120 --> 00:29:59,120
He said, if you just think, Buddha may not hold.

247
00:29:59,120 --> 00:30:00,120
Dhamma may not hold.

248
00:30:00,120 --> 00:30:03,120
Sanko may not hold.

249
00:30:03,120 --> 00:30:05,120
The Buddha is my refuge.

250
00:30:05,120 --> 00:30:07,120
The Dhamma is my refuge.

251
00:30:07,120 --> 00:30:09,120
The Sangha is my refuge.

252
00:30:09,120 --> 00:30:14,120
He said, this is, just remember this, wherever you go, keep this with you.

253
00:30:14,120 --> 00:30:16,120
It's a great protection.

254
00:30:16,120 --> 00:30:22,120
Like in the Buddha's time, there's people with the Buddhists who would say,

255
00:30:22,120 --> 00:30:25,120
the Buddha's time, the Buddha's time.

256
00:30:25,120 --> 00:30:29,120
So one time this woman, she tripped and she said,

257
00:30:29,120 --> 00:30:34,120
the Buddha's time, the Buddha's time, something like that.

258
00:30:34,120 --> 00:30:38,120
There's a whole story involved with that one.

259
00:30:38,120 --> 00:30:42,120
But it's apparently something that people would use as a protection.

260
00:30:42,120 --> 00:30:45,120
The Buddha's time.

261
00:30:45,120 --> 00:30:52,120
And then they also used Namobu Daya and Namo Daya and Namo Sangha.

262
00:30:52,120 --> 00:30:59,120
These kind of things, ways of shortening the homage to the Buddha's time, and the Sangha very powerful.

263
00:30:59,120 --> 00:31:07,120
The most powerful is the Buddha's actual word, Ttipi Sol bhagavavam, and the Buddha's time.

264
00:31:07,120 --> 00:31:10,120
Because it means something.

265
00:31:10,120 --> 00:31:14,120
And thinking about someone who's very pure and teachings that are very pure.

266
00:31:14,120 --> 00:31:18,120
And the Sangha who is very pure.

267
00:31:18,120 --> 00:31:21,120
Then we come and listen to the Dhamma.

268
00:31:21,120 --> 00:31:23,120
This is another great, wholesome thing.

269
00:31:23,120 --> 00:31:25,120
Very difficult to listen to the Dhamma.

270
00:31:25,120 --> 00:31:29,120
It's difficult for us to get to this situation, this position.

271
00:31:29,120 --> 00:31:33,120
This situation is opportunity that we have to listen to the Dhamma.

272
00:31:33,120 --> 00:31:39,120
Not easy to find for all the reasons I already mentioned it.

273
00:31:39,120 --> 00:31:43,120
Dogs and cats can't listen to the Dhamma.

274
00:31:43,120 --> 00:31:47,120
They can listen that they can't understand the Dhamma.

275
00:31:47,120 --> 00:31:52,120
But difficult also to bear with it, because it's not exciting.

276
00:31:52,120 --> 00:31:59,120
It doesn't appeal to our desires, it's not pleasurable all the time.

277
00:31:59,120 --> 00:32:12,120
And you have to sit still and you have to bear with pains and hunger and desires in the mind of boredom in the mind of versions of doubts in the mind.

278
00:32:12,120 --> 00:32:19,120
So many different things in the mind that you have to bear with just in order to hear the Dhamma.

279
00:32:19,120 --> 00:32:24,120
Even in the time of the Buddha, many people had trouble listening to the Dhamma.

280
00:32:24,120 --> 00:32:32,120
Based on their departments that were in the mind.

281
00:32:32,120 --> 00:32:38,120
So it's something that even just listening to the Dhamma is a kind of a training, especially when you use it as a meditation practice.

282
00:32:38,120 --> 00:32:45,120
So you can even, at the moment of hearing the Dhamma, you can apply the Dhamma to the teaching.

283
00:32:45,120 --> 00:32:48,120
Go to the talk.

284
00:32:48,120 --> 00:32:52,120
You hear sound and the sound is arising and ceasing.

285
00:32:52,120 --> 00:32:56,120
When you say to yourself hearing, hearing, you can use the Dhamma talk.

286
00:32:56,120 --> 00:32:59,120
Because it's such a good chance.

287
00:32:59,120 --> 00:33:02,120
There's nothing else you can do, you can't go anywhere.

288
00:33:02,120 --> 00:33:10,120
If you use it to reflect on your own experience and when we say, don't let the moment pass you by, you actually don't.

289
00:33:10,120 --> 00:33:19,120
You take the moment up and you are mindful of the moment watching your likes and dislikes, your wants and your version.

290
00:33:19,120 --> 00:33:27,120
Your pains and your pleasure being aware and studying yourself.

291
00:33:27,120 --> 00:33:35,120
Turning the Dhamma inward, not just taking it for an intellectual exercise, but actually studying yourself.

292
00:33:35,120 --> 00:33:40,120
Using it as a study tool.

293
00:33:40,120 --> 00:33:43,120
And of course, finally the meditation practice itself.

294
00:33:43,120 --> 00:33:55,120
So this is what we really take as the pinnacle of our meeting together is the actual meditation practice.

295
00:33:55,120 --> 00:34:05,120
Because in the end it's true that you don't have to hurry, you don't have to worry about all the time that you've lost or all that.

296
00:34:05,120 --> 00:34:09,120
Or what's going to happen in the future?

297
00:34:09,120 --> 00:34:14,120
The present moment is eternal, it's always here.

298
00:34:14,120 --> 00:34:20,120
The question is not whether we're going to have the opportunity to learn about the present moment.

299
00:34:20,120 --> 00:34:25,120
Because the moment the question is whether we will and whether we'll know how to.

300
00:34:25,120 --> 00:34:30,120
How long has it taken us to get to this point where we actually know how to approach?

301
00:34:30,120 --> 00:34:42,120
We have a teaching on how to approach the present moment and how to study the present moment and how to straighten out our minds in regards to the present moment.

302
00:34:42,120 --> 00:34:44,120
And how long will this last?

303
00:34:44,120 --> 00:34:47,120
How long will we have this opportunity?

304
00:34:47,120 --> 00:34:53,120
Very uncertain, we have no idea it could be gone tomorrow.

305
00:34:53,120 --> 00:34:57,120
And then that's it for us.

306
00:34:57,120 --> 00:35:03,120
So for this reason the Buddha said it can no hold my al-Pachagas.

307
00:35:03,120 --> 00:35:18,120
Today is when we should make effort, we should do our work today.

308
00:35:18,120 --> 00:35:25,120
Kojamya Maranang Suve, who knows whether that will be even tomorrow.

309
00:35:25,120 --> 00:35:30,120
Maybe that will come tomorrow.

310
00:35:30,120 --> 00:35:39,120
So in that note, let's begin our practice and we can begin to take this moment seriously and use it to seriously.

311
00:35:39,120 --> 00:36:03,120
Learn about ourselves, study ourselves.

