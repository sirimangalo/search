1
00:00:00,000 --> 00:00:03,120
Well, and welcome back to Ask a Monk.

2
00:00:03,120 --> 00:00:08,960
Now I'd like to answer a question about pain in meditation.

3
00:00:08,960 --> 00:00:16,680
When you feel a great amount of pain during your meditation, is it necessary to sit

4
00:00:16,680 --> 00:00:20,160
still and ensure that you never move?

5
00:00:20,160 --> 00:00:22,720
If a person moves, is there something wrong with that?

6
00:00:22,720 --> 00:00:28,000
Or is the point of meditation to be able to not move?

7
00:00:28,000 --> 00:00:31,280
Is it wrong practice if you move your body?

8
00:00:31,280 --> 00:00:35,360
And this is a very simple question with a very simple answer, but it actually has some profound

9
00:00:35,360 --> 00:00:36,360
implications.

10
00:00:36,360 --> 00:00:41,440
So I'm going to take a little bit of time to answer it in some detail.

11
00:00:41,440 --> 00:00:46,280
The simple answer first is, yes indeed, you can move.

12
00:00:46,280 --> 00:00:50,000
There's nothing intrinsically wrong with moving your body.

13
00:00:50,000 --> 00:00:51,720
You know, we move it all the time.

14
00:00:51,720 --> 00:00:56,960
The problem here, of course, that we have to understand, is that moving based on pain

15
00:00:56,960 --> 00:00:59,720
creates a version towards the pain.

16
00:00:59,720 --> 00:01:04,400
Now that's very all very well in theory.

17
00:01:04,400 --> 00:01:09,160
You shouldn't move because if you move, you're moving based on a version.

18
00:01:09,160 --> 00:01:15,120
But it can actually go the other way where you crush your version to the pain and you

19
00:01:15,120 --> 00:01:17,120
force yourself to sit.

20
00:01:17,120 --> 00:01:25,200
In fact, creating more tension and more stress because there's the forcing.

21
00:01:25,200 --> 00:01:31,280
There's the not complying with nature.

22
00:01:31,280 --> 00:01:35,880
And in fact, sometimes nature dictates that it's necessary to move.

23
00:01:35,880 --> 00:01:39,760
The appropriate reaction, the physical reaction, is to move.

24
00:01:39,760 --> 00:01:43,160
Once you get on in the meditation, this will change.

25
00:01:43,160 --> 00:01:45,280
First of all, there will be less pain.

26
00:01:45,280 --> 00:01:50,760
Second of all, it will bring less stress and tension and less of a desire to move.

27
00:01:50,760 --> 00:01:55,240
The pain will become just another sensation and the mind won't say something's wrong or

28
00:01:55,240 --> 00:01:56,480
feel that something's wrong.

29
00:01:56,480 --> 00:02:01,000
It will just see it as a sensation and be content with it and not upset by it.

30
00:02:01,000 --> 00:02:03,000
So you won't need to move.

31
00:02:03,000 --> 00:02:05,480
Now in the beginning, this isn't the case.

32
00:02:05,480 --> 00:02:10,600
In the beginning, it brings more stress and tension and really great problems.

33
00:02:10,600 --> 00:02:15,800
So what we should do is be mindful of it.

34
00:02:15,800 --> 00:02:21,760
And when it first comes, we should certainly try to be as mindful of it as possible and

35
00:02:21,760 --> 00:02:27,560
not move without immediately moving our bodies because this is a great teacher for us.

36
00:02:27,560 --> 00:02:29,640
And this is what I'd like to get into.

37
00:02:29,640 --> 00:02:38,080
But the basic technique is to say to ourselves, pain, pain, pain, just quietly, not out

38
00:02:38,080 --> 00:02:42,920
loud, but in the mind, just focusing on the pain and reminding ourselves, hey, this is just

39
00:02:42,920 --> 00:02:43,920
pain.

40
00:02:43,920 --> 00:02:44,920
There's nothing wrong with it.

41
00:02:44,920 --> 00:02:46,520
It's not a negative experience.

42
00:02:46,520 --> 00:02:50,640
There's nothing intrinsically bad about pain.

43
00:02:50,640 --> 00:02:54,520
In terms of the universe really doesn't care whether you're in pain or not.

44
00:02:54,520 --> 00:02:58,920
And so it's not something that is intrinsically negative.

45
00:02:58,920 --> 00:03:02,120
What is negative is your negative reactions to it.

46
00:03:02,120 --> 00:03:06,520
Your anger, your frustration, your thinking that it's bad, that it's a problem that you

47
00:03:06,520 --> 00:03:07,920
have to do something about it.

48
00:03:07,920 --> 00:03:12,680
So when you see it simply as pain and all of that goes away, this is really the truth.

49
00:03:12,680 --> 00:03:17,040
But something that you should try, these videos are not meant to just be, oh, that sounds

50
00:03:17,040 --> 00:03:18,040
nice.

51
00:03:18,040 --> 00:03:19,040
I agree with that.

52
00:03:19,040 --> 00:03:21,840
And then you go home or go back to Facebook or whatever.

53
00:03:21,840 --> 00:03:23,640
These are something you should put into practice.

54
00:03:23,640 --> 00:03:29,880
So when I say this, try it and find out for yourself because it really has wonderful

55
00:03:29,880 --> 00:03:31,520
consequences.

56
00:03:31,520 --> 00:03:36,240
Any kind of pain that you come up with in your life suddenly, it's no longer a great problem

57
00:03:36,240 --> 00:03:38,280
or a great difficulty.

58
00:03:38,280 --> 00:03:43,880
It's something that you can deal with with mindfulness and clarity and wisdom and not have

59
00:03:43,880 --> 00:03:46,320
to suffer from.

60
00:03:46,320 --> 00:03:50,320
Once that becomes too much, in the beginning that's very difficult to do and you're not

61
00:03:50,320 --> 00:03:54,720
really mindful you're mostly a pain pain and you're really angry about it anyway.

62
00:03:54,720 --> 00:04:00,480
So saying pain isn't really acknowledging, it's reinforcing the anger and the hate.

63
00:04:00,480 --> 00:04:05,200
So when it gets to that point where it's overwhelming and you feel like you have to move,

64
00:04:05,200 --> 00:04:08,680
then just move mindfully, you can lift your leg when moving, you're hand moving,

65
00:04:08,680 --> 00:04:16,480
say to yourself, moving, placing or grasping, lifting, moving, placing or you can just move

66
00:04:16,480 --> 00:04:22,200
your leg without, you know, if you're sitting in this position, just lifting, moving,

67
00:04:22,200 --> 00:04:23,200
placing.

68
00:04:23,200 --> 00:04:27,240
Just do it mindfully and it becomes a meditation because eventually our whole life should

69
00:04:27,240 --> 00:04:32,280
become meditation, we should try to be as mindful as we can in our daily lives.

70
00:04:32,280 --> 00:04:36,200
When we eat, we're mindful, when we drink, we're mindful, everything we do, we know what

71
00:04:36,200 --> 00:04:37,200
we're doing.

72
00:04:37,200 --> 00:04:40,760
We move our hands, we'll be moving, moving and shaking.

73
00:04:40,760 --> 00:04:42,600
When we talk, we should know that we're talking.

74
00:04:42,600 --> 00:04:48,440
What is this feeling of my lips moving, even to that extent you can be mindful.

75
00:04:48,440 --> 00:04:52,600
When you're listening you can say hearing, hearing and acknowledging the sound and you'll

76
00:04:52,600 --> 00:04:56,560
find that you really understand the meaning much better and you get less caught up in

77
00:04:56,560 --> 00:04:59,760
your own emotions and judgments and so on.

78
00:04:59,760 --> 00:05:02,960
So this is the basic technique.

79
00:05:02,960 --> 00:05:09,760
Now the theory behind pain and how it's such a good teacher is that eventually pain

80
00:05:09,760 --> 00:05:17,760
is one of the most, the best teachers that we have because pain is something that we don't

81
00:05:17,760 --> 00:05:18,760
like.

82
00:05:18,760 --> 00:05:25,600
It's our aversion, it's our problem, you know, it's if it weren't for pain, if it weren't

83
00:05:25,600 --> 00:05:31,440
for things like pain, then we would never need to practice meditation, we would never

84
00:05:31,440 --> 00:05:33,720
think to come and practice meditation.

85
00:05:33,720 --> 00:05:34,720
Why wouldn't we?

86
00:05:34,720 --> 00:05:38,760
It's the point of wasting all this time when we can be out enjoying life.

87
00:05:38,760 --> 00:05:42,840
But the problem is that in our life it's not all fun and games and in fact our clinging

88
00:05:42,840 --> 00:05:47,880
leads us to have suffering because we don't like certain things where we're not content,

89
00:05:47,880 --> 00:05:52,000
we're thinking about the good things and it's the one a bad thing comes it makes us angry

90
00:05:52,000 --> 00:05:54,720
because it's not what we want and so on.

91
00:05:54,720 --> 00:06:03,520
So what we're basically doing here is learning how to live with it and how to see it

92
00:06:03,520 --> 00:06:07,800
for what it is and see it simply as another experience, not as a bad thing and not seeing

93
00:06:07,800 --> 00:06:10,280
other things as good things.

94
00:06:10,280 --> 00:06:16,040
And so the theory here is that most people understand, it has to do with how people

95
00:06:16,040 --> 00:06:18,880
understand suffering.

96
00:06:18,880 --> 00:06:26,280
An ordinary person understands suffering to be, and suffering here is of course the big elephant

97
00:06:26,280 --> 00:06:32,600
in the room, it's what we as Buddhists deal with, it's what we talk about, what we teach

98
00:06:32,600 --> 00:06:36,800
and yet always have a very difficult, great amount of difficulty talking about.

99
00:06:36,800 --> 00:06:41,800
But this is because most people understand suffering to be the pain, suffering to be

100
00:06:41,800 --> 00:06:46,640
a feeling, this is what is a dukowade and dukowade and that means dukowade is suffering,

101
00:06:46,640 --> 00:06:52,400
so we understand dukowade to be a way to be a feeling that you get, you get this feeling

102
00:06:52,400 --> 00:06:56,040
that's dukowade, that's suffering, you get that feeling that's suffering, you get another

103
00:06:56,040 --> 00:06:58,160
feeling and it's not suffering.

104
00:06:58,160 --> 00:07:07,600
So we categorize our experiences, this one is suffering, it's not suffering and as a result

105
00:07:07,600 --> 00:07:16,920
our means of overcoming suffering, of being free from suffering is to find a way to only

106
00:07:16,920 --> 00:07:20,320
have these experiences and to never have these experiences.

107
00:07:20,320 --> 00:07:24,800
This doesn't that sound normal, that sounds yeah, that's how we get rid of suffering, that's

108
00:07:24,800 --> 00:07:29,120
because that's what we're told, that's what we're taught and it's totally false, it's

109
00:07:29,120 --> 00:07:34,240
totally wrong, it's the wrong way to deal with suffering, but this is how we do, why is

110
00:07:34,240 --> 00:07:40,400
it wrong, okay so you have pain in the leg, you move your leg and then you have pain in

111
00:07:40,400 --> 00:07:44,080
the back and so then you have to stretch your back and then you have pain in your head

112
00:07:44,080 --> 00:07:49,120
and so you have to take a pill for the pain and you have pain here, pain there and you

113
00:07:49,120 --> 00:07:54,040
have to take another pill or you have to get an operation and so on and so on and all this

114
00:07:54,040 --> 00:07:58,280
time you're developing more and more aversion to the pain until eventually it becomes

115
00:07:58,280 --> 00:08:03,840
totally overwhelming. When a person takes a pill for a headache, really what they're doing

116
00:08:03,840 --> 00:08:08,480
is reaffirming their aversion towards the pain and as a result it's going to be worse

117
00:08:08,480 --> 00:08:14,120
next time it's going to, yeah, it's going to be worse and worse and worse, not because

118
00:08:14,120 --> 00:08:19,040
the pain changes, but because our attitude towards it is reaffirmed as being this is bad,

119
00:08:19,040 --> 00:08:23,320
this is bad, this is bad bad bad, until it becomes totally unbearable.

120
00:08:23,320 --> 00:08:29,840
These things are not static, craving is not static, it builds, it turns into a habit, it

121
00:08:29,840 --> 00:08:37,960
changes our whole vibration in that direction and it changes the world around us as well,

122
00:08:37,960 --> 00:08:43,400
anger does as well, aversion does as well, we become totally averse to these things and

123
00:08:43,400 --> 00:08:52,000
unable to bear them. So what would we come to see and the reason why we come to practice

124
00:08:52,000 --> 00:08:57,800
meditation is the second type of suffering, the Buddha said in the Dukasabhava, somehow

125
00:08:57,800 --> 00:09:06,240
means a reality of suffering, that suffering is a fact, you can't escape it, it's reality,

126
00:09:06,240 --> 00:09:10,880
it's a part of reality and this is like getting old is a part of reality, getting sick

127
00:09:10,880 --> 00:09:20,480
is a part of it, dying is a part of reality. So these methods of overcoming suffering or

128
00:09:20,480 --> 00:09:31,920
these ways of avoiding the unpleasant part of reality are temporary, are ineffective because

129
00:09:31,920 --> 00:09:36,240
it's there, it's a part of life, it's not going to go away, you're not going to be to

130
00:09:36,240 --> 00:09:40,000
remove it, you're not going to get rid of sickness, you're not going to get rid of old

131
00:09:40,000 --> 00:09:45,000
age, you're not going to get rid of death, these things are going to come to you and

132
00:09:45,000 --> 00:09:49,720
it's this realization, when we have a situation that we can't overcome, people who

133
00:09:49,720 --> 00:09:54,760
have migraine headaches, they've taken every pill and nothing works, people who are

134
00:09:54,760 --> 00:10:00,760
mourning a person who passed away, they can drink or whatever, but nothing works and

135
00:10:00,760 --> 00:10:04,960
they're still sad, they're still thinking about the person, nothing works. When a person

136
00:10:04,960 --> 00:10:09,440
gets to this point, this is most often when they begin to practice meditation, when

137
00:10:09,440 --> 00:10:13,120
they get to the point where they realize they're totally on the wrong path, when they

138
00:10:13,120 --> 00:10:20,960
realize that this reaffirmation of greed, anger, delusion is totally dragging them down in

139
00:10:20,960 --> 00:10:26,040
the wrong path and they're not helping, their reactions towards suffering, their ways of

140
00:10:26,040 --> 00:10:31,720
dealing with suffering are ineffective. This is the second type of suffering, it's a very

141
00:10:31,720 --> 00:10:35,320
important realization because that's what leads us, that's what leads to the

142
00:10:35,320 --> 00:10:38,880
conclusion that we have to do something, people say why do you meditate with the point

143
00:10:38,880 --> 00:10:43,720
wasting your time? Well, those people have not yet realized, this is what they

144
00:10:43,720 --> 00:10:48,400
haven't yet realized. For a person who has realized that you can't really escape it,

145
00:10:48,400 --> 00:10:53,720
you can say I go out and party and I'm happy, I've tried it and I have no, I'm not able

146
00:10:53,720 --> 00:10:58,800
to escape suffering in the way you pretend or you think you're able to. My

147
00:10:58,800 --> 00:11:03,520
experience is that suffering is a part of life, it's something that we either

148
00:11:03,520 --> 00:11:11,000
learn to deal with or we suffer more and more and more. It gets worse and worse and worse.

149
00:11:11,000 --> 00:11:15,600
So we find a way, so this is when we come to practice meditation. When we come to

150
00:11:15,600 --> 00:11:20,400
practice meditation we realize the third truth, the third type of suffering, the

151
00:11:20,400 --> 00:11:26,800
third way of understanding this word suffering and this is that suffering is

152
00:11:26,800 --> 00:11:33,240
inherent in all things, just as heat is inherent in fire, you know, there's no

153
00:11:33,240 --> 00:11:40,440
fire that is not hot. Fire is hot, that's, well, in a conventional sense, fire is

154
00:11:40,440 --> 00:11:46,480
hot. By the same token, all things in the world, anything that arises is

155
00:11:46,480 --> 00:11:52,640
suffering and what we mean by this is just as fire is hot if you hold on to it

156
00:11:52,640 --> 00:11:58,400
and burns you. When something arises and you cling to it, it makes you suffer.

157
00:11:58,400 --> 00:12:02,520
Why this happens basically what I've been saying, when it's something good you

158
00:12:02,520 --> 00:12:06,560
cling to it as good and then it disappears. When it disappears, you're unhappy,

159
00:12:06,560 --> 00:12:10,400
you're looking for it, because there's still the one thing, there's still the

160
00:12:10,400 --> 00:12:14,920
clinging in your mind and in fact you cultivate it and develop it and it comes

161
00:12:14,920 --> 00:12:20,760
up again and again, I want this, I want this, I like this, when it's gone, the

162
00:12:20,760 --> 00:12:25,360
craving doesn't go away, the experience is gone and who knows when it'll come

163
00:12:25,360 --> 00:12:30,200
back. If it's an unpleasant experience, the craving, a version go away, go away

164
00:12:30,200 --> 00:12:35,120
and pushing it away with this method and that makes it worse and so on. When we

165
00:12:35,120 --> 00:12:40,520
practice meditation, we come to realize that, no, these things are not really going

166
00:12:40,520 --> 00:12:43,360
to make me happy, clinging to good things is not going to make me happy, why?

167
00:12:43,360 --> 00:12:48,640
Because they come and they go, they arise and they cease. My happiness depends on

168
00:12:48,640 --> 00:12:53,000
that, how can I ever hope to be happy? If say my happiness depends on this person,

169
00:12:53,000 --> 00:12:57,440
you come to realize that that person is only experiences, you hear something

170
00:12:57,440 --> 00:13:00,840
and that's that person, you see something and that's that person, you think of

171
00:13:00,840 --> 00:13:05,240
something and that's that person. But all of those experiences are impermanent

172
00:13:05,240 --> 00:13:09,080
and they're uncertain, you don't know whether they're going to say something

173
00:13:09,080 --> 00:13:12,720
good to you, maybe someone you love says something bad to you and you suffer

174
00:13:12,720 --> 00:13:16,880
from it and the realization that it's not sure, there's no person there that's

175
00:13:16,880 --> 00:13:20,560
wonderful, there's a bunch of experiences waiting to happen and those

176
00:13:20,560 --> 00:13:24,920
experiences will not all be pleasant when the person dies if they're a loved one

177
00:13:24,920 --> 00:13:29,880
and that will be a very unpleasant thing. This realization we come to through

178
00:13:29,880 --> 00:13:33,800
our meditation practice, even just watching our own body, watching the rise

179
00:13:33,800 --> 00:13:39,480
and falling on the stomach, coming to see that every single thing that arises

180
00:13:39,480 --> 00:13:44,600
has to pass away, it comes and it goes, coming to see that we do it even with

181
00:13:44,600 --> 00:13:49,200
our stomach, rising and falling, oh, now it's smooth and then we like it and then

182
00:13:49,200 --> 00:13:53,120
suddenly it changes and it's rough and uncomfortable and we can't really find

183
00:13:53,120 --> 00:13:58,000
it and we're angry and upset. Even this very stupid simple object of the

184
00:13:58,000 --> 00:14:02,040
stomach can teach us everything we need to know about reality because it

185
00:14:02,040 --> 00:14:08,320
shows us our mind, it shows us our the way we project and we relate to things

186
00:14:08,320 --> 00:14:13,600
and make more out of things than they actually are. Through the meditation

187
00:14:13,600 --> 00:14:18,640
practice we'll come to break down people, break down things and experiences

188
00:14:18,640 --> 00:14:25,760
into individual phenomenon that arise and sees and come and go and so as a

189
00:14:25,760 --> 00:14:30,560
result we see that that none of them are pleasant or none of them are

190
00:14:30,560 --> 00:14:35,280
satisfying and this is what it means by suffering that our happiness should

191
00:14:35,280 --> 00:14:38,880
never depend on these things. It depends on a person that person doesn't

192
00:14:38,880 --> 00:14:44,880
exist, it's just experiences coming to see that our relationship with the

193
00:14:44,880 --> 00:14:48,640
person is just through experiences which are never going to satisfy us.

194
00:14:48,640 --> 00:14:51,440
They're only going to feel that they can possibly do is bring more

195
00:14:51,440 --> 00:14:57,600
clinging and craving and dissatisfaction and the need for more and so on and we

196
00:14:57,600 --> 00:15:02,240
will never truly be at peace with ourselves. People think that they will but you

197
00:15:02,240 --> 00:15:05,520
can look at those people and see that they're not really truly at peace with

198
00:15:05,520 --> 00:15:09,920
themselves and through the meditation we come to see this and so we come to

199
00:15:09,920 --> 00:15:14,800
gain true peace for ourselves. As you as you see these things you know you'll see

200
00:15:14,800 --> 00:15:17,840
things arising even just the stomach coming and going and you see the

201
00:15:17,840 --> 00:15:21,520
clinging and how useless it is to cling to it to be this way or that way

202
00:15:21,520 --> 00:15:25,280
because it's changing. You see it coming and going and that it's not under your

203
00:15:25,280 --> 00:15:29,600
control you see impermanence suffering on self these three characteristics.

204
00:15:29,600 --> 00:15:35,200
As this goes on the fourth type of suffering or the fourth way of understanding

205
00:15:35,200 --> 00:15:38,800
suffering comes to you and that is as the truth and this is what we call

206
00:15:38,800 --> 00:15:42,480
you know you hear about the noble truth the noble truth of suffering

207
00:15:42,480 --> 00:15:45,920
and this is the realization that we're hoping for this realization

208
00:15:45,920 --> 00:15:52,000
it's not it's not a good it's not satisfying these are not going to

209
00:15:52,000 --> 00:15:56,240
make me happy. This realization is the fourth type of

210
00:15:56,240 --> 00:16:00,480
way of understanding suffering is really the consummation of the Buddha's

211
00:16:00,480 --> 00:16:02,640
teaching it's this you know starting to see things

212
00:16:02,640 --> 00:16:05,760
things coming and going on this isn't satisfying that it

213
00:16:05,760 --> 00:16:09,440
you just realize at some point it this isn't the way to find happiness it's

214
00:16:09,440 --> 00:16:12,480
not intellectual of course I'm just you know putting words to it but it's

215
00:16:12,480 --> 00:16:16,880
suddenly a boom the mind just says no and the mind gives

216
00:16:16,880 --> 00:16:21,920
that the mind lets go and at that point there's freedom the mind

217
00:16:21,920 --> 00:16:25,520
is released and you can enter you enter into this state of

218
00:16:25,520 --> 00:16:29,680
of of total freedom which really there's no experience at all there's no

219
00:16:29,680 --> 00:16:32,560
seeing, hearing, smelling, tasting, feeling or even thinking

220
00:16:32,560 --> 00:16:36,400
you don't even aware it's like kind of like falling asleep but it's

221
00:16:36,400 --> 00:16:39,200
total peace and when you come back you know

222
00:16:39,200 --> 00:16:43,840
you really wow I just totally let go you know there was no

223
00:16:43,840 --> 00:16:49,440
arising of anything at that point and that this is you know we call

224
00:16:49,440 --> 00:16:53,520
Nirvana this realization and a person starts to realize this and

225
00:16:53,520 --> 00:16:57,120
looking around and seeing that their happiness can come from anything

226
00:16:57,120 --> 00:17:01,040
outside of themselves and so they cultivate this more and more and more

227
00:17:01,040 --> 00:17:03,840
and start to see the truth more and more and more and

228
00:17:03,840 --> 00:17:08,160
and eventually are able to become totally free from suffering

229
00:17:08,160 --> 00:17:11,760
so with the point being that it's this realization

230
00:17:11,760 --> 00:17:14,880
whether we talk about Nirvana and so on and this

231
00:17:14,880 --> 00:17:18,560
see for yourself no Buddha's teaching is to see for yourself

232
00:17:18,560 --> 00:17:22,320
but the point which I think everyone can understand is that we we have to see

233
00:17:22,320 --> 00:17:25,120
this reality we have to really grasp this

234
00:17:25,120 --> 00:17:29,040
that we're not going to find happiness in the things outside of ourselves

235
00:17:29,040 --> 00:17:34,240
and that these phenomena that arise

236
00:17:34,240 --> 00:17:38,880
there there there's no good that can come from clinging

237
00:17:38,880 --> 00:17:42,960
even in terms of aversion there's no good that comes from

238
00:17:42,960 --> 00:17:47,280
wanting to escape it or wanting to even force yourself to sit still

239
00:17:47,280 --> 00:17:50,560
in this case so it's a very simple question with a very simple

240
00:17:50,560 --> 00:17:54,720
answer but here the profound implications is to do a suffering that help us to

241
00:17:54,720 --> 00:17:58,480
understand why it is that we might want to sit through it sometimes

242
00:17:58,480 --> 00:18:01,600
and if we can get that through through our head then

243
00:18:01,600 --> 00:18:04,160
you know sometimes we can sit through it we say you know

244
00:18:04,160 --> 00:18:07,680
yeah I want to move but it's not really a big deal it's not going to kill me

245
00:18:07,680 --> 00:18:11,200
and I'm going to learn something here and as you do that you'll see how your mind

246
00:18:11,200 --> 00:18:14,880
works you see the craving and the aversion

247
00:18:14,880 --> 00:18:19,120
and you see how the phenomenon works and how it arises and ceases and see

248
00:18:19,120 --> 00:18:22,560
that there's no good that comes from being angry about it or upset about it you

249
00:18:22,560 --> 00:18:25,680
don't feel happier it doesn't solve the problem it creates more

250
00:18:25,680 --> 00:18:29,360
aversion moving your foot doesn't solve the problem it's a temporary

251
00:18:29,360 --> 00:18:33,200
solution to kind of ease the pressure when it's too much for you

252
00:18:33,200 --> 00:18:36,320
but eventually it's it's you you're just going to you know see

253
00:18:36,320 --> 00:18:39,680
why would I move my foot again I've moved my foot 10 times already it didn't

254
00:18:39,680 --> 00:18:42,160
solve the problem and eventually you give that up

255
00:18:42,160 --> 00:18:45,520
and you say well I'll just sit through it that's what happens in meditation when

256
00:18:45,520 --> 00:18:47,680
you go to an intensive course in the beginning

257
00:18:47,680 --> 00:18:50,640
and pain here you say okay I can fix that and you put a pillow under here and

258
00:18:50,640 --> 00:18:53,360
then this one then you put a pillow under here and then here you put a pillow

259
00:18:53,360 --> 00:18:56,560
and here you put a pillow until eventually you just say

260
00:18:56,560 --> 00:19:00,000
this it's it's not working it's not solving the problem

261
00:19:00,000 --> 00:19:04,320
and you give that and that's the realization that we're striving for you

262
00:19:04,320 --> 00:19:08,480
realize that more and more eventually you'll realize that that's the truth

263
00:19:08,480 --> 00:19:12,800
that that's the truth of reality and you'll let go and not cling

264
00:19:12,800 --> 00:19:19,120
so this is my discussion of the truth of suffering in the Buddhist teaching

265
00:19:19,120 --> 00:19:22,880
thanks for tuning in and I wish you all the best that you're all able to put

266
00:19:22,880 --> 00:19:27,040
this into practice and and try your best to become patient

267
00:19:27,040 --> 00:19:30,640
and be able to overcome your attachments and emotions

268
00:19:30,640 --> 00:19:35,200
and suffering and find true peace happiness and freedom from suffering

269
00:19:35,200 --> 00:19:54,960
so all the best

