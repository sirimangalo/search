1
00:00:00,000 --> 00:00:06,680
Okay, good morning.

2
00:00:06,680 --> 00:00:16,680
Broadcasting lies.

3
00:00:16,680 --> 00:00:25,680
Tomorrow I'm off to the airport.

4
00:00:25,680 --> 00:00:34,720
I've been near the airport, spent tomorrow, last day in Bangkok, and then early morning

5
00:00:34,720 --> 00:00:38,760
Saturday to Sri Lanka.

6
00:00:38,760 --> 00:00:50,560
I'm told that the internet is good in Sri Lanka, I might be able to even do a

7
00:00:50,560 --> 00:01:01,240
long radio this weekend, let's see, going to be busy, it looks like there's an

8
00:01:01,240 --> 00:01:13,160
whole schedule set up for Sri Lanka, teaching every day, almost every day.

9
00:01:13,160 --> 00:01:20,520
Meditation is just like any other work that we undertake, it's got a lot

10
00:01:20,520 --> 00:01:39,480
in common with training the body, and that's how it should be seen, if you want real

11
00:01:39,480 --> 00:01:51,760
results from meditation lasting results, it's the difference, it's like the

12
00:01:51,760 --> 00:02:05,400
difference between getting drunk or taking drugs to please the body, to make

13
00:02:05,400 --> 00:02:14,680
you feel good, versus training the body to really make you good, make it work

14
00:02:14,680 --> 00:02:32,040
good, healing the body of its sicknesses, cultivating strength and abilities.

15
00:02:32,040 --> 00:02:56,800
Meditation is also a lot like other mental trainings, training to be a lawyer or an

16
00:02:56,800 --> 00:03:17,800
accountant or a doctor, training in chess or training, mathematics, sciences,

17
00:03:17,800 --> 00:03:26,120
engineering, it's similar, it's using the same building blocks, so the

18
00:03:26,120 --> 00:03:36,120
strategies all have to be the same, and there's one strategy that stands out as

19
00:03:36,120 --> 00:03:50,200
sort of the key to success, the key to achievement to progress, it's called the

20
00:03:50,200 --> 00:04:02,200
path to idhi, or the things that lead to idhi, idhi meaning power or strength or

21
00:04:02,200 --> 00:04:16,280
accomplishment, success, before things that lead to success in any sphere, not

22
00:04:16,280 --> 00:04:25,600
just in meditation, useful for us to remember, really all you need to succeed for

23
00:04:25,600 --> 00:04:38,240
things, for general principles, called the for idhi pata, if you don't have these

24
00:04:38,240 --> 00:04:51,440
you can succeed, if you have them succeed, they are essential, and in order we

25
00:04:51,440 --> 00:05:09,200
have chanda means, somewhere between contentment with the activity and interest in the activity,

26
00:05:09,200 --> 00:05:34,200
it's not quite desire, if you want to do something that's not necessarily helpful, it can

27
00:05:34,200 --> 00:05:43,160
actually get in your way because you're too eager, it can actually be too keen, but maybe

28
00:05:43,160 --> 00:05:50,880
that's the word keen, being keen about something, you do have to be keen, it's not

29
00:05:50,880 --> 00:05:57,400
exactly desire, but for some reason or other you're keen on doing it, in some cases it's

30
00:05:57,400 --> 00:06:08,120
keen because it brings you pleasure like keen on eating food or keen on romance or sexuality,

31
00:06:08,120 --> 00:06:15,640
keen on drugs and alcohol, keen on candy, but it doesn't have to be that, it can be keen

32
00:06:15,640 --> 00:06:22,360
on working, keen on helping other people, keen on cultivating goodness in yourself, keen

33
00:06:22,360 --> 00:06:33,120
on on anything, but if you're not keen about something, success is most generally outside

34
00:06:33,120 --> 00:06:41,480
of your grasp, very difficult to achieve success when you are not keen on what you're

35
00:06:41,480 --> 00:06:50,200
doing, it's possible to succeed, it's just going against the grain, it's like the oil in

36
00:06:50,200 --> 00:06:59,840
your, the motor oil, the oil in your motor, without it, it's very hard, it's hard on

37
00:06:59,840 --> 00:07:06,600
the system, because you don't want to do what you do, meditation of course is the worst

38
00:07:06,600 --> 00:07:13,840
with this, if you don't want to meditate, it becomes a chore, you have a problem, this

39
00:07:13,840 --> 00:07:18,640
is a problem, but obviously a very common problem, it's easy to get to the point, especially

40
00:07:18,640 --> 00:07:23,400
in insight meditation where you're dealing with suffering, where you don't really want

41
00:07:23,400 --> 00:07:31,720
to meditate anymore, you're not happy, you're not comfortable doing it anymore, but the

42
00:07:31,720 --> 00:07:36,600
neat thing about insight meditation is that that can become a meditation, and if you're

43
00:07:36,600 --> 00:07:43,440
clever, if you really know what you're doing, if you really understand what your principles

44
00:07:43,440 --> 00:07:49,280
of, the basic principles of insight meditation, then it's not a barrier, because that's

45
00:07:49,280 --> 00:07:55,680
a meditation, you meditate on the disliking, you meditate on the boredom and the frustration,

46
00:07:55,680 --> 00:08:06,160
paying in the suffering, and you find there's no excuse, you don't need excuses not to

47
00:08:06,160 --> 00:08:20,440
do it, you don't have any reason for your disliking, for your aversion, you may find

48
00:08:20,440 --> 00:08:31,440
this doesn't really get in the way, and if I'm contentment, it's very hard to want to meditate

49
00:08:31,440 --> 00:08:38,920
after a while, because it's not something you can cling to, no meditation is an activity,

50
00:08:38,920 --> 00:08:46,160
insight meditation is not something you can cling to.

51
00:08:46,160 --> 00:08:49,840
You can't think about meditation, if you really understand it, and if I'm practicing

52
00:08:49,840 --> 00:08:56,280
you, you can't think about it and say, boy, I want to do that, you can't feel attraction

53
00:08:56,280 --> 00:09:01,360
towards it, once you understand it, because it's nothing, there's nothing, there's no

54
00:09:01,360 --> 00:09:09,520
there's no object of attraction, there's only the consideration, those things that are

55
00:09:09,520 --> 00:09:17,040
undesirable, and when you think about them, you're mind of course requires, and yet the

56
00:09:17,040 --> 00:09:24,600
interest comes, the more you practice, the more interested you are in practicing, it's

57
00:09:24,600 --> 00:09:34,320
not even practicing so much as returning to a state of clarity of mind, cleaning away

58
00:09:34,320 --> 00:09:49,800
the garbage, ceasing to engage in the cultivation of unwholesiveness, ceasing to engage

59
00:09:49,800 --> 00:09:56,800
in evil in that which causes suffering for doing others.

60
00:10:20,800 --> 00:10:29,800
So anyway, something for us to keep in mind that we need to be interested in, keen

61
00:10:29,800 --> 00:10:34,800
on what they're doing, keen on meditation, so something to be cultivated.

62
00:10:34,800 --> 00:10:41,040
So as a sign, if you're not keen on it, don't just dismiss that and keep going and

63
00:10:41,040 --> 00:10:48,960
plugging away anyway, don't judge through it, you have to actually work through that,

64
00:10:48,960 --> 00:10:56,960
the drudgery, the dissatisfaction with the meditation, meditate on that, it's important

65
00:10:56,960 --> 00:11:02,080
because you won't get anywhere without being keen about it, without reading yourself

66
00:11:02,080 --> 00:11:10,960
without aversion, just going against the grain all the time.

67
00:11:10,960 --> 00:11:18,320
Number two is effort, when you need effort in order to succeed, this is of course true

68
00:11:18,320 --> 00:11:24,800
in the world, if you don't work hard, you don't get anywhere.

69
00:11:24,800 --> 00:11:28,480
In the dhamma as well, if you don't work at it, you don't get anywhere, you need the

70
00:11:28,480 --> 00:11:38,280
effort to rid yourself of unwholesome states, you need the effort to prevent wholesome

71
00:11:38,280 --> 00:11:47,280
states from arising, the effort to cultivate wholesome states, and the effort to maintain

72
00:11:47,280 --> 00:11:57,280
and protect wholesome states that have already risen, these are the four right efforts

73
00:11:57,280 --> 00:12:02,120
in Buddhism.

74
00:12:02,120 --> 00:12:08,320
In a way, it's kind of a different sort of effort from what we would normally understand

75
00:12:08,320 --> 00:12:11,800
by the term with the effort, it's just pushing harder and harder, and that's not really

76
00:12:11,800 --> 00:12:21,240
what it means, it means working certainly, but working in a specific way, working to maintain

77
00:12:21,240 --> 00:12:31,000
wholesomeness and stay away from unwholesome states.

78
00:12:31,000 --> 00:12:32,000
That's all.

79
00:12:32,000 --> 00:12:35,240
It doesn't mean pushing harder, it doesn't mean meditating hours and hours and hours

80
00:12:35,240 --> 00:12:48,720
and hours, it can be a good thing, if it's done properly, it's a wonderful thing.

81
00:12:48,720 --> 00:12:52,840
But there has to be a company by right effort, not wrong effort, you can't just push

82
00:12:52,840 --> 00:13:08,720
and push and push yourself, you go crazy, right effort is a specific type of effort, the

83
00:13:08,720 --> 00:13:19,840
effort to be pure of mind, effort taken to be clear in mind, and you need that if you

84
00:13:19,840 --> 00:13:25,240
don't have effort, your mind will be, you won't really be mindful, really meditating,

85
00:13:25,240 --> 00:13:33,840
you can walk and sit for hours and hours and it won't be any good, and put effort.

86
00:13:33,840 --> 00:13:38,960
Of course, you just need effort just to actually meditate, it takes effort to get up early

87
00:13:38,960 --> 00:13:45,240
in the morning, takes effort to sit up straight, it takes effort to walk back and forth

88
00:13:45,240 --> 00:13:50,600
without stopping, it just takes, it does take effort, if you don't have it, that's going

89
00:13:50,600 --> 00:13:57,000
to be a hindrance.

90
00:13:57,000 --> 00:14:03,960
So as with anything, this is part of our success, how to find success.

91
00:14:03,960 --> 00:14:11,400
The third intibanda is called jita, jita means mind, so it's actually a curious usage

92
00:14:11,400 --> 00:14:20,240
of the word mind, but it means keeping in mind, if you want to succeed at anything, the

93
00:14:20,240 --> 00:14:27,440
third quality that you need is to keep it in mind, not lose sight of the act of the

94
00:14:27,440 --> 00:14:40,880
goal, do not lose sight of the act, do not forget your mission, do not forget your goals,

95
00:14:40,880 --> 00:14:45,920
do not lose track of them, do not get sidetracked by other things, do not get distracted

96
00:14:45,920 --> 00:14:56,880
by other things, and this is of course nowhere more important than in meditation,

97
00:14:56,880 --> 00:15:06,760
where the object is the mind itself, the object is reality, it's the practice of the

98
00:15:06,760 --> 00:15:23,520
mind, it involves the mind, so keeping the mind on the task is the whole thing, keeping

99
00:15:23,520 --> 00:15:29,000
it in mind takes on a whole new meaning in meditation, because that's the essence of meditation,

100
00:15:29,000 --> 00:15:37,040
when your mind isn't on the meditation, you can't progress at all whatsoever, perhaps most

101
00:15:37,040 --> 00:15:44,240
crucial before, because it is the practice itself, the practice is keeping in mind that

102
00:15:44,240 --> 00:15:49,720
in the world, in the world the affairs take your mind off of it, sometimes it's fine

103
00:15:49,720 --> 00:15:59,200
because it's worth running by itself, your business will continue, even your physical

104
00:15:59,200 --> 00:16:05,200
activities, you can be lifting weights and let your mind wander, you just have to keep

105
00:16:05,200 --> 00:16:12,280
your mind on it when it's necessary, meditation isn't like that, meditation is obviously

106
00:16:12,280 --> 00:16:19,700
requires this to a much greater degree, but it still involves keeping your mind

107
00:16:19,700 --> 00:16:24,800
on the fact that you, it's time to do, do sitting meditation, time to do walking and

108
00:16:24,800 --> 00:16:39,120
sitting, time to, okay, not to be mindful in all your activities when you're eating, reminding

109
00:16:39,120 --> 00:16:43,960
yourself that you have, remembering that you should be mindful when you eat chewing, chewing,

110
00:16:43,960 --> 00:16:49,200
swallowing, tasting, when you're showering, when you're washing your clothes, whatever

111
00:16:49,200 --> 00:16:55,280
you're doing, the ability to remember, to remind yourself, ah, I should be mindful of

112
00:16:55,280 --> 00:17:01,000
this, remind yourself and keep yourself in the present moment, this is what has been

113
00:17:01,000 --> 00:17:18,820
completed without this meditation will never succeed, one hundred percent. Number three, number four is called V-Mungsau,

114
00:17:18,820 --> 00:17:30,980
V-Mungsau means out or, ah, to divide it, now I would say guess better, since out,

115
00:17:30,980 --> 00:17:37,780
mungsau, mungsau, I'm not even sure where that comes from, but V-Mungsau means the ability

116
00:17:37,780 --> 00:17:47,900
to discriminate, it's another word for wisdom, but discrimination is what is used to

117
00:17:47,900 --> 00:17:53,620
because that's what it's, the aspect of wisdom that's important is the ability not just

118
00:17:53,620 --> 00:18:02,980
to keep your mind on something, but to, ah, be clever and figure out ways and, and

119
00:18:02,980 --> 00:18:13,060
refine your, your attention, refine your, find your, find your, and adjust your activity.

120
00:18:13,060 --> 00:18:19,900
So obviously, so obviously quite important in all, activities were successes, the goal

121
00:18:19,900 --> 00:18:27,060
when we were trying to succeed at something, it's not just enough to, it's, well, it's

122
00:18:27,060 --> 00:18:32,860
true in some spheres, it might just be enough to plaw it along, but in most cases, you

123
00:18:32,860 --> 00:18:40,540
need some, it wouldn't be enough just to have the first three without having this wisdom

124
00:18:40,540 --> 00:18:46,260
to be able to adjust and discriminate and refine your activity.

125
00:18:46,260 --> 00:18:55,540
So, in, at work, finding better ways to do things, making the right decisions in business,

126
00:18:55,540 --> 00:19:05,820
in family and society, in, in school, being clever, studying, being clever, choosing

127
00:19:05,820 --> 00:19:18,460
the right topics and having the right tools and, in general, just being able to be efficient

128
00:19:18,460 --> 00:19:25,380
having the wisdom to, just to see when you're being inefficient and so on, to see when

129
00:19:25,380 --> 00:19:36,420
you're actually on the wrong path, practicing incorrectly, working incorrectly, working

130
00:19:36,420 --> 00:19:37,420
improperly.

131
00:19:37,420 --> 00:19:43,460
So then we'll see it to see a better way, and then to have wisdom, to not just plaw

132
00:19:43,460 --> 00:19:50,620
along, but to do it with wisdom, so you, you do it right, and you're able to adjust your

133
00:19:50,620 --> 00:20:00,620
flexible mistakes with the, the meditation, this is also obviously a very important aspect

134
00:20:00,620 --> 00:20:07,620
of the practice of the path, and that you can't just do walking, sitting, walking, sitting,

135
00:20:07,620 --> 00:20:16,700
you think you're going to become enlightened, you need to be clear about the object,

136
00:20:16,700 --> 00:20:24,340
your mind has to be clear, your mind has to be, you know, on the right path, in the

137
00:20:24,340 --> 00:20:32,020
right way, in the right place.

138
00:20:32,020 --> 00:20:36,260
So when the stomach is rising, your mind has to be aware of the stomach rising, it goes

139
00:20:36,260 --> 00:20:48,220
around, it really, it doesn't, shouldn't really need to be said, but your mind has to be there.

140
00:20:48,220 --> 00:20:52,380
Well that's, that's Jita, I know you know that your mind has to be there, but that you

141
00:20:52,380 --> 00:21:01,660
have to be clear about nature of your practice, you have to be able to adjust your

142
00:21:01,660 --> 00:21:07,500
practice, you have to be able to see when you're doing things right and doing things wrong,

143
00:21:07,500 --> 00:21:15,700
you have to be able to, for example when you're walking and you find yourself very distracted,

144
00:21:15,700 --> 00:21:20,100
you have to be, discriminate, you have to be able to see that walking isn't helping

145
00:21:20,100 --> 00:21:27,060
you right now, so you sit down, you're sitting down and you feel very tired, drowsing

146
00:21:27,060 --> 00:21:38,660
off, then you have to discriminate, you have to be, discriminate, you have to be clever

147
00:21:38,660 --> 00:21:48,220
and decide to walk instead, you have to find ways like on and on when he was, when he

148
00:21:48,220 --> 00:21:56,220
became enlightened, he was doing walking all night and he didn't get anywhere, and so

149
00:21:56,220 --> 00:22:02,860
last moment he decided, he realized that he'd probably better lie down because he had

150
00:22:02,860 --> 00:22:08,940
too much effort, and so he lay down and right away he became enlightened, this kind

151
00:22:08,940 --> 00:22:15,180
of thing, it actually can happen, it's quite, quite possible that you just adjust something,

152
00:22:15,180 --> 00:22:20,460
you just have the wisdom to adjust your practice, to be able to catch the things that

153
00:22:20,460 --> 00:22:27,900
you're not noting, realize that when you're struggling to have the wisdom to catch what

154
00:22:27,900 --> 00:22:33,740
it is that's causing problems, catch what it is that's holding you back, you can't just

155
00:22:33,740 --> 00:22:38,740
force yourself to practice properly, you can't force the practice to succeed, it's not

156
00:22:38,740 --> 00:22:48,580
like any other work in that way, any wisdom, you have to find the way through wisdom,

157
00:22:48,580 --> 00:22:57,620
then yeah, yeah, but he's so jutting, you become pure through wisdom, you can discriminate

158
00:22:57,620 --> 00:23:03,940
this very important, not just walks it, walks it, walks it for hours and hours, you can

159
00:23:03,940 --> 00:23:10,580
do it for lifetimes and not get anywhere if you're not doing it properly, mind isn't

160
00:23:10,580 --> 00:23:17,060
in the right place and you're not adjusting, constantly adjusting your practice, constantly

161
00:23:17,060 --> 00:23:25,620
flexible, it's got this is most apparent in regards to the layers of practice and the types

162
00:23:25,620 --> 00:23:32,300
of experience, in the way that a meditator figures out, in a sense figures out an experience

163
00:23:32,300 --> 00:23:40,620
or figures out an engram, a type of mental activity, figures out how to be mindful of

164
00:23:40,620 --> 00:23:46,340
something and then they stick to that and they try to do that again and again and again

165
00:23:46,340 --> 00:23:52,820
and that's not how meditation works because every moment is going to be different, you're

166
00:23:52,820 --> 00:23:59,140
always changing, even your changing is changed, even your changing is changed, you have

167
00:23:59,140 --> 00:24:08,140
to remember that, it's never going to be predictable, that's the truth of it, it's not

168
00:24:08,140 --> 00:24:13,900
just that it comes again and again and again but that it's always changing and if you're

169
00:24:13,900 --> 00:24:21,020
just trying to use the old wave without having we mung sound, without being able to break

170
00:24:21,020 --> 00:24:26,860
apart and see the truth behind the current experience, understand the current experience,

171
00:24:26,860 --> 00:24:34,780
you have a very hard type of doing the same thing over a little bit of information and

172
00:24:34,780 --> 00:24:44,540
the ability to understand the current experience, you know, it's uniqueness, so there

173
00:24:44,540 --> 00:24:49,260
you go, those are the four independent, they're very useful, something for us to think

174
00:24:49,260 --> 00:24:59,020
about during our practice, keep in mind so that our practice may succeed, thank you all

175
00:24:59,020 --> 00:25:06,020
for tuning in and may all of your practice, everyone's practice succeed, be well.

