1
00:00:00,000 --> 00:00:18,320
Good evening, everyone, broadcasting live Friday the 13th, Friday May 13th, today's quote

2
00:00:18,320 --> 00:00:38,360
It's about good things, good things that are not good, there are things in this world

3
00:00:38,360 --> 00:00:56,320
that we think are good, they turn out to be dangerous, dar uno, dar una means wicked or harsh

4
00:00:56,320 --> 00:01:02,360
cruel.

5
00:01:02,360 --> 00:01:14,440
What is it in this world that is harsh and cruel, dangerous?

6
00:01:14,440 --> 00:01:41,000
What is his three things, lamba, game, sakara, praise, and see the sea loca, fame, sakara is

7
00:01:41,000 --> 00:01:53,440
honor when we went through this, sakara is when people hold the esteem you, sea loca

8
00:01:53,440 --> 00:02:04,600
is fame, lamba is the game, these things are very bad, not the sort of things you think

9
00:02:04,600 --> 00:02:33,480
of is bad, it's problematic, why are these considered to be bad, but as I said, some people

10
00:02:33,480 --> 00:02:47,600
I can see in their mind, they would not tell a lie, they would not knowingly tell a lie

11
00:02:47,600 --> 00:03:12,440
from gold or jewels, golden ball, golden ball filled with pieces of silver, no, maybe, right.

12
00:03:12,440 --> 00:03:24,080
There are people who would not normally do evil things, they don't think they're naturally

13
00:03:24,080 --> 00:03:33,400
pure, this is the way of worldly goodness, you see people who are just naturally wholesome

14
00:03:33,400 --> 00:03:39,400
and good and kind.

15
00:03:39,400 --> 00:03:46,840
And then he says, by that same person I've seen telling lies, because his heart was obsessed

16
00:03:46,840 --> 00:03:54,760
by gains honor and fame, though I've been corrupted, and there are like asops, fables

17
00:03:54,760 --> 00:04:04,000
about that sort of thing, someone becomes, you know, the classic story or someone loses

18
00:04:04,000 --> 00:04:17,120
sight, with a sight of what's good, becomes blinded by gains honor and fame.

19
00:04:17,120 --> 00:04:24,880
Good things blind us, there's a danger in too much, undeserved, but often talked about

20
00:04:24,880 --> 00:04:40,560
undeserved, when someone isn't capable, or isn't ready, or isn't worthy of gain, praise,

21
00:04:40,560 --> 00:04:52,000
fame, or anything else good, you know, it's another reason why nature is such a useful

22
00:04:52,000 --> 00:05:03,160
environment to meditate in, because it's quite harsh, so it forces you to be patient

23
00:05:03,160 --> 00:05:14,760
and present, it's easy when things become, when we live in the world like in Asia, when

24
00:05:14,760 --> 00:05:25,040
we went to meditate, it could be quite cold, and it's a colder than it is here now, but

25
00:05:25,040 --> 00:05:34,280
here we turn on the heat when it gets below 20 degrees, when we have hot water showers,

26
00:05:34,280 --> 00:05:39,760
we couldn't imagine taking a cold water shower, this kind of thing, it's easy to become

27
00:05:39,760 --> 00:05:51,840
complacent and lazy, when you actually live in nature, if you've ever lived in nature,

28
00:05:51,840 --> 00:06:05,480
it forces you to be content, to accept, to let go, because you can't run away, if you

29
00:06:05,480 --> 00:06:09,560
don't have a hot water shower, then you have to take cold water shower, you know, if you're

30
00:06:09,560 --> 00:06:14,040
live in the forest, you don't even have a hot water, a cold water shower, you have to bathe

31
00:06:14,040 --> 00:06:23,040
in the river, or not bathe at all for days, so why we go on arms round, because it's

32
00:06:23,040 --> 00:06:31,640
a way of being content, you know, with whatever food there is easily and freely available

33
00:06:31,640 --> 00:06:41,720
for charity, bringing ourselves from the complacency of choosing and getting what we want

34
00:06:41,720 --> 00:06:52,240
all the time, so why we sleep on the floor, it's why we give up money and give up luxury,

35
00:06:52,240 --> 00:06:59,760
because these things are dangerous, where all the good things are problematic, and this

36
00:06:59,760 --> 00:07:04,840
is another reason why many people are unable to progress and the practice, are unable

37
00:07:04,840 --> 00:07:10,120
to even think of meditation, practice as a positive thing, there's many people in this

38
00:07:10,120 --> 00:07:17,400
world who couldn't imagine or reason for meditating, right, why, why would you waste your

39
00:07:17,400 --> 00:07:28,400
time, you could be enjoying life, right, because these people enjoy life, they have pleasant

40
00:07:28,400 --> 00:07:35,240
lives and they have good, they have gains that other people do not, there are people who

41
00:07:35,240 --> 00:07:40,320
think that all you have to do is think positively and good things come to you, and there's

42
00:07:40,320 --> 00:07:46,720
something to not being depressed and negative, but it's also quite clear that some people

43
00:07:46,720 --> 00:07:59,440
just have better luck and better situation than others, and they become blind and forgetful.

44
00:07:59,440 --> 00:08:04,320
There's the story of Sakka, the King of the Gods, and became a Sotabana, so he really

45
00:08:04,320 --> 00:08:09,640
understood the Dhamma, but then one day Mughalana went to check on him and found that

46
00:08:09,640 --> 00:08:15,280
he was complacent, he had this beautiful palace and he showed Mughalana this palace

47
00:08:15,280 --> 00:08:26,560
and Mughalana used his magical powers to shake the palace up with an earthquake, and Sakka

48
00:08:26,560 --> 00:08:35,600
became afraid and he said, you've become complacent, right, and this gain of you, it's

49
00:08:35,600 --> 00:08:50,680
gain of yours as an angel, these good things, we're like angels in many ways, we have such

50
00:08:50,680 --> 00:09:01,440
luxury in this world, we just have cooked food, all the things that we have that we take

51
00:09:01,440 --> 00:09:09,920
for granted, cooked food, soft clothes, that we can actually wear cotton on our bodies

52
00:09:09,920 --> 00:09:17,440
to ward off heat and cold, that we have walls and a roof to keep out the insects and

53
00:09:17,440 --> 00:09:24,360
the elements that we have medicine, so we don't have to put up with our illness, we can

54
00:09:24,360 --> 00:09:33,120
just, whenever we're sick, for whatever reason we can take a pill and it goes away, very

55
00:09:33,120 --> 00:09:49,720
easy to get lost in these things and not realize the limited nature of our luxury and

56
00:09:49,720 --> 00:10:01,040
our contentment, it's easy to be content when things are good, when you have happy things

57
00:10:01,040 --> 00:10:09,040
come to you, we practice meditation to be ready for anything, to be invincible, it's

58
00:10:09,040 --> 00:10:18,920
not a reason to be proud that you can deal with, that you can enjoy life, so you can

59
00:10:18,920 --> 00:10:19,920
do it.

60
00:10:19,920 --> 00:10:26,400
It's not a reason to be proud of when you're famous or when you're rich or when you're

61
00:10:26,400 --> 00:10:33,120
honored by others, something to be concerned about, now I will become complacent, these

62
00:10:33,120 --> 00:10:40,000
good things will make me forget about the potential for bad things, when the bad things

63
00:10:40,000 --> 00:10:50,440
come out completely unprepared, very important part aspect of Buddhism is our relationship

64
00:10:50,440 --> 00:11:02,920
with worldly good things, it's a very important point in leading us to take the meditation

65
00:11:02,920 --> 00:11:09,920
practice seriously, it's our relationship with worldly happiness, worldly pleasure, worldly

66
00:11:09,920 --> 00:11:14,800
good things, the worldly dumbness and reminding ourselves that these are not permanent,

67
00:11:14,800 --> 00:11:28,760
that the good and the bad are unpredictable, unreliable and sustainable, okay, anyway that's

68
00:11:28,760 --> 00:11:39,880
our dhamma for this evening, if we have any questions, good morning, thanks for the broken

69
00:11:39,880 --> 00:11:50,760
gong teaching, glad you appreciated it, just to remind her I asked a couple of questions,

70
00:11:50,760 --> 00:12:02,640
well I don't see them, if they're before last night then I think they're lost.

71
00:12:02,640 --> 00:12:15,400
What should you do when other people's suffering spills onto you, it's interesting how

72
00:12:15,400 --> 00:12:19,920
you say spills onto you, you might have to give me an example but I can talk about some

73
00:12:19,920 --> 00:12:28,920
of the ways it spills onto you, no, when you feel sympathetic for other people's suffering

74
00:12:28,920 --> 00:12:36,520
is one way or when people get you caught up in their own problems is another way but

75
00:12:36,520 --> 00:12:41,520
you can't actually, suffering can't actually spread, suffering is one thing that can't

76
00:12:41,520 --> 00:12:50,880
spread, you can't suffer because someone else is suffering, it's very easy to suffer

77
00:12:50,880 --> 00:12:55,760
when someone else is suffering with these reasons, right, someone gets in trouble, suppose

78
00:12:55,760 --> 00:13:05,280
someone accumulates gambling debts or something and then their whole family suffers,

79
00:13:05,280 --> 00:13:17,840
suppose someone is very ill, then their whole family suffers for another reason from sadness,

80
00:13:17,840 --> 00:13:28,320
from loss, people taking problems out on you, right, so suppose someone is, apparently when

81
00:13:28,320 --> 00:13:34,960
people are ill with cancer they can be very hard to deal with rather than becoming

82
00:13:34,960 --> 00:13:44,000
sober they become more intoxicated or more caught up in themselves, more self-absorbed

83
00:13:44,000 --> 00:13:56,240
and crazy, you know, become very self-centered, me, me, me, poor me, something like that

84
00:13:56,240 --> 00:14:03,200
and it causes problems for other people or they're angry at you for not appreciating

85
00:14:03,200 --> 00:14:12,240
them that kind of thing, but not appreciating their problems and you have to deal with it

86
00:14:12,240 --> 00:14:20,160
is with everything else, with mindfulness, don't let other people see we're very connected

87
00:14:20,160 --> 00:14:28,160
with others, we react very quickly to other people's problems, so it's a reason why we go on what

88
00:14:28,160 --> 00:14:35,600
we call retreat, right, you retreat because being around people all day is tough, you know,

89
00:14:35,600 --> 00:14:44,960
being around worldly people is very hard on your mind, very difficult for your meditation practice,

90
00:14:44,960 --> 00:14:50,560
I mean you could call it the advanced practice, right, when you actually have to deal with people

91
00:14:50,560 --> 00:14:59,760
meditating here in the monastery, it's a training, like boxing in a boxing gym, you know,

92
00:14:59,760 --> 00:15:06,800
you go to the gym when you train, but when you step into the ring it gets real and you have to put

93
00:15:06,800 --> 00:15:19,040
it all to use. Feeling sad for other people is a big problem, I think. People think compassion

94
00:15:19,040 --> 00:15:25,040
is feeling sad for others, it's not really, it shouldn't be moved by other people's suffering,

95
00:15:25,840 --> 00:15:30,880
not in that way, sorry, that's maybe wrong because you shouldn't be moved, but moved to help,

96
00:15:31,680 --> 00:15:38,160
not moved to sadness, you shouldn't be upset by it, so it doesn't mean not in that sense,

97
00:15:38,160 --> 00:15:43,360
it shouldn't be sad because of other people's suffering, and that's people will often use that

98
00:15:43,360 --> 00:15:47,760
sort of thing to their advantage, pour me and they want everyone to feel sad for them and then it

99
00:15:47,760 --> 00:15:54,800
drains everyone else and it doesn't actually help anyone, right, but you should help others,

100
00:15:54,800 --> 00:16:02,320
and in fact you can help others, because of your contentment through the meditation practice,

101
00:16:02,320 --> 00:16:06,880
when you have fewer needs of your own, you're much better able to help others,

102
00:16:07,840 --> 00:16:15,600
you talk about other people abusing that in terms of using you and expecting you to help them,

103
00:16:15,600 --> 00:16:22,240
I think for enlightened being there's a certain sense of willingness to let people use you,

104
00:16:23,440 --> 00:16:27,760
certain willingness to help others and to not worry so much about yourself because

105
00:16:27,760 --> 00:16:31,600
you don't get upset about things, you can be used and it doesn't bother you,

106
00:16:33,040 --> 00:16:37,520
you don't mind when other people want you to help them, it's just easier to go along with

107
00:16:37,520 --> 00:16:43,440
them so you just do it, and you can do things that most you're stronger, in fact invincible,

108
00:16:43,440 --> 00:16:50,960
it's an enlightened being in our hunk, nothing bothers them, so they're able to help and work

109
00:16:50,960 --> 00:17:02,880
for the benefit of others, untirely, untirely, so those are just some thoughts on that,

110
00:17:04,000 --> 00:17:08,160
ultimately you have to be mindful and it's going to depend on the situation,

111
00:17:08,160 --> 00:17:14,400
and depend on your own abilities, you shouldn't pretend that you're enlightened and try to act

112
00:17:14,400 --> 00:17:19,280
like an hour a hunt, you should sometimes protect yourself and sometimes you have to retreat,

113
00:17:22,800 --> 00:17:29,520
is eating meat considered killing, no it's not, you know, eating meat is considered eating,

114
00:17:29,520 --> 00:17:32,320
chewing, nothing dies when you eat meat,

115
00:17:32,320 --> 00:17:41,200
have all the Buddhas been men in born in India, apparently so,

116
00:17:43,520 --> 00:17:50,560
but not just men there, it's a very special being, a Buddha is born as a very special being,

117
00:17:50,560 --> 00:17:56,560
but yes is male, doesn't mean that women are inferior, although there is that kind of a

118
00:17:56,560 --> 00:18:00,800
connotation, unfortunately I don't know what to say about that, maybe it's not true, maybe

119
00:18:00,800 --> 00:18:06,720
there are women Buddhist, but there's a sense that the Buddha form is male, it's masculine,

120
00:18:06,720 --> 00:18:12,080
the same goes for tomorrow, which is the evil angel, it's also a male form,

121
00:18:14,240 --> 00:18:20,800
so it's a sense I think of the male being more powerful and I know that's kind of sexist or it's

122
00:18:20,800 --> 00:18:26,400
outright sexist, so I don't know what to say about that, maybe it's not true, it wouldn't put too

123
00:18:26,400 --> 00:18:32,640
much stock in it, it's not like it's an important teaching or anything, but yeah curiously they

124
00:18:32,640 --> 00:18:38,960
seem, according to the commentaries, to have been all male born in India, and India seems to come

125
00:18:39,840 --> 00:18:46,480
again and again, each universe has an India apparently, take that as you will,

126
00:18:46,480 --> 00:18:56,480
in your booklet under the section on sitting practice, you're right, one should investigate

127
00:18:56,480 --> 00:19:04,160
sensations other than the breath, after some minutes, what if the mind stays easily with the breath,

128
00:19:05,280 --> 00:19:10,160
should one stick with the breath or pick other sensations, if the mind is with the breath,

129
00:19:10,160 --> 00:19:14,800
then with the stomach, you focus on the stomach rise involved, but as soon as pain arises,

130
00:19:14,800 --> 00:19:21,120
focus on the pain, as soon as happiness arises, focus on happiness, as soon as calm arises,

131
00:19:21,680 --> 00:19:29,760
focus on the calm, maybe the book is poorly written, I don't know, shouldn't say you should look

132
00:19:29,760 --> 00:19:34,880
for feelings, but if there's feelings, you should focus on them, because that means your mind

133
00:19:34,880 --> 00:19:36,480
is already no longer on the stomach.

134
00:19:36,480 --> 00:19:46,880
Yes, he actually shook the palace using a single toe, someone who knows their texts,

135
00:19:47,680 --> 00:19:53,280
he touched it, so Sakha look, he said, look at this beautiful majestic palace and it's

136
00:19:53,280 --> 00:20:00,560
blunder and it's magnificent, it's awesomeness, and Mogulana touched it with his big toe and

137
00:20:00,560 --> 00:20:06,480
it shook as though it was going to fall apart, and Sakha was horrified and said,

138
00:20:06,480 --> 00:20:12,880
maybe this grand palace of his is about to collapse, and Mogulana shook his head, and he

139
00:20:12,880 --> 00:20:19,680
are being negligent, this is not permanent, this is not stable, this is not awesome, this is nothing.

140
00:20:24,160 --> 00:20:29,600
It's the broken gong analogy used for people with preconceptions about you or a situation that

141
00:20:29,600 --> 00:20:36,480
happened, the broken gong is when you don't react, it means don't react, that's all, it's quite

142
00:20:36,480 --> 00:20:47,120
simple, I wouldn't read too much into it, it shouldn't be choose when to say no that is wrong,

143
00:20:47,760 --> 00:20:52,160
guess that would be right at the time, I mean you wouldn't use the exclamation points, I think

144
00:20:52,160 --> 00:20:56,480
it's the point, you wouldn't be upset about it, but you might say that's wrong,

145
00:20:56,480 --> 00:21:03,760
but the broken gong means there's something missing, a gong is unable to ring,

146
00:21:04,960 --> 00:21:08,800
that's actually kind of a negative thing, right, we think of a gong ringing as a good thing,

147
00:21:08,800 --> 00:21:14,960
but no there's something missing from the mind of an enlightened being, they might say no,

148
00:21:14,960 --> 00:21:30,800
that's wrong, but there's no upset, there's no reaction, what do the incentive for its continuance

149
00:21:30,800 --> 00:21:36,480
via factory farming and consuming it, yes, but that's not immoral, that doesn't make it immoral,

150
00:21:36,480 --> 00:21:45,040
it may make it immoral in a intellectual sense, but eating meat is just chewing, chewing,

151
00:21:45,040 --> 00:21:49,600
chewing, nothing dies as you do it, and that's where morality is in Buddhism, because otherwise

152
00:21:50,400 --> 00:21:54,800
you can never win, you can never be truly moral, because you don't know whether the food you're

153
00:21:54,800 --> 00:22:02,480
eating is pesticides that they use, you don't know, driving and you get on the bus and you pay a bus

154
00:22:02,480 --> 00:22:08,640
token, well more simply you get in your car and you drive and you know your car is going to hit

155
00:22:08,640 --> 00:22:12,560
insects, right, well then you're responsible for the killing of the insects, okay, so then you take

156
00:22:12,560 --> 00:22:17,920
a bus, well you're still paying for the bus, you're contributing to the dead insects on the windshield,

157
00:22:18,800 --> 00:22:25,600
etc, etc, you can never be truly moral if your morality is based on an intellectual concept of

158
00:22:25,600 --> 00:22:34,320
this is supporting this, this is supporting that, it's not like that, morality is the intentional

159
00:22:35,680 --> 00:22:43,200
or the abstention from intentional acts of direct harm, so if you tell someone, you know,

160
00:22:43,200 --> 00:22:49,760
go kill me a chicken or something or kill me a fish, then you're not guilty of murder,

161
00:22:50,320 --> 00:22:54,400
but you're guilty of telling someone to commit murder, which is still a very harmful thing,

162
00:22:54,400 --> 00:23:04,640
it's the state of your mind, you know, you don't want to be killed and yet you act in a way to

163
00:23:05,840 --> 00:23:11,520
with the intention to bring about death, you don't do that when you eat, you don't have the intention

164
00:23:11,520 --> 00:23:17,440
to kill this thing, you also don't have an intention that people should go out and do it more,

165
00:23:17,440 --> 00:23:25,120
go out and kill, kill more, I know people look at this as a yeah, but yeah, I don't, don't overthink

166
00:23:25,120 --> 00:23:29,680
things, keep it simple, otherwise you get lost,

167
00:23:35,360 --> 00:23:40,240
what is the benefit of not eating afternoon, well it's just a line in the sand, there's not

168
00:23:40,240 --> 00:23:45,920
there's nothing magical about noon, but you, you know, you need a certain amount of energy for

169
00:23:45,920 --> 00:23:51,120
the day, a certain amount of calories and nutrients for the day, you use those during the day,

170
00:23:51,120 --> 00:23:57,760
so eating in the morning gives you that, it means what do I need to survive, I need food,

171
00:23:57,760 --> 00:24:02,960
if I eat in the evening it's not very useful, but if I eat in the morning that helps me get

172
00:24:02,960 --> 00:24:09,200
through the day and I live and I have energy, so we only eat in the morning for that reason and

173
00:24:09,200 --> 00:24:17,120
the artificial line in the sand is dawn to midday, which is reasonable, doesn't mean it's immoral

174
00:24:17,120 --> 00:24:27,600
to eat after midday, but it's a reasonable, a reasonable convention, I mean a lot of the rules are

175
00:24:27,600 --> 00:24:34,000
just that, they're not necessary, they're just rules, and that's the thing is they're not

176
00:24:34,000 --> 00:24:40,480
thou shalt not, and then this is evil there, I undertake not to, so there's things that people

177
00:24:40,480 --> 00:24:52,560
undertake, you undertake them because they're useful, whoa, there's a big one, recently came off

178
00:24:52,560 --> 00:25:09,280
a medication, causes delirium, any tips on fear about dying incorrectly to let go or concentrate or

179
00:25:09,280 --> 00:25:23,120
observe during these overwhelming experiences, well make sure you don't have these things in

180
00:25:23,120 --> 00:25:28,720
these states in your mind, right, it's fearsome, and there's no way around it if you die with

181
00:25:28,720 --> 00:25:43,680
these evil in your heart, you're going to a bad place. I know in fear itself is a very unwholesome

182
00:25:43,680 --> 00:25:48,560
state, so it's something that you have to deal with, you die with fear in your mind, that's

183
00:25:48,560 --> 00:25:54,640
not a good thing, that's one of the prime reasons for going to a hellish state or a ghostly

184
00:25:54,640 --> 00:26:02,000
state, not a good state, now ordinary fear won't send you to hell, but but intense fear can,

185
00:26:03,520 --> 00:26:07,760
make you more afraid, right, oh my gosh, you said my fear is going to leave me to hell,

186
00:26:10,080 --> 00:26:14,800
well good, you shouldn't see how silly you're being in the end, sorry, I don't mean to truly

187
00:26:14,800 --> 00:26:23,360
realize it, but in the end it's just fear, you know me, we make more out of these things than they

188
00:26:23,360 --> 00:26:28,480
are, that's really the problem, if you're mindful of your fear, then it's no longer has power over

189
00:26:28,480 --> 00:26:35,600
you, my mindfulness meditation, how should you do it, what is the tips, well have you read my

190
00:26:35,600 --> 00:26:45,440
booklet on how to meditate, I think you have, that's about it, read my booklet, learn how to meditate,

191
00:26:45,440 --> 00:26:57,040
practice that way, will help you deal with such things as fear and anxiety and so on,

192
00:26:59,120 --> 00:27:04,960
how long should you focus on your feelings, well and as long as they last is good, but if after a

193
00:27:04,960 --> 00:27:10,880
long time they don't go away then you can ignore them and come back to the stomach rising and

194
00:27:10,880 --> 00:27:18,480
falling, but you can stay with them, we're just trying to see them as they are, to see that they're

195
00:27:18,480 --> 00:27:23,600
not under our control, trying to learn to be objective, it's more about our state of mind than

196
00:27:23,600 --> 00:27:29,440
the actual experience, so whatever you focus on, trying to cultivate this objective experience,

197
00:27:30,240 --> 00:27:33,680
it doesn't matter what you focus on, if the feelings always there, just stay with it,

198
00:27:33,680 --> 00:27:41,840
good object, I have had some serious brain injuries in the past, would it affect my meditation,

199
00:27:41,840 --> 00:27:51,200
potentially, potentially not, I wouldn't worry about it, do what you can, as best you can,

200
00:27:51,200 --> 00:27:56,720
that's all you can do, we can only start where we are, we're not all going to become enlightened

201
00:27:56,720 --> 00:28:03,520
in this lifetime, but we take a step in that direction, it becomes our direction, you know,

202
00:28:04,240 --> 00:28:09,200
we can turn ourselves in the right direction, we'll begin on the right path,

203
00:28:13,520 --> 00:28:18,240
you see so much more of it right before death than you realize you have, yeah, I'll bet,

204
00:28:18,240 --> 00:28:24,000
I don't think I've ever been in a situation where I was thinking I was going to die,

205
00:28:24,000 --> 00:28:29,360
I mean it was in a car accident once, that was kind of scary, and I remember doing rock climbing

206
00:28:29,360 --> 00:28:34,560
and when I got to the top, I tightened myself in to be let down, and halfway down I realized

207
00:28:34,560 --> 00:28:40,160
the knot that was holding me was just a single knot, I had mistighted it, that was kind of

208
00:28:40,160 --> 00:28:44,960
freaky, it's 60 feet up, but realizing it was about to slip loose and fall to my death,

209
00:28:44,960 --> 00:28:54,800
but no, I've never been in such a situation, but talking to people who have, they can imagine,

210
00:28:54,800 --> 00:28:59,520
and as a meditator you can imagine because you get these feelings, these things come up,

211
00:28:59,520 --> 00:29:04,320
that people talk about coming up when you die, and so I'm in a situation so great for preparing

212
00:29:04,320 --> 00:29:10,320
you for death because you've been there, seen it all, you know, you've gone through the

213
00:29:10,320 --> 00:29:20,800
dregs and the depths of your mind, have you ever read the book? I, the four agreements, no, I haven't.

214
00:29:25,280 --> 00:29:31,760
There's a way to Sooka refer to Nibanda, some very good questions, very difficult ones,

215
00:29:31,760 --> 00:29:37,600
I have no idea, a way to Taksooka, let's look up the word, I don't even know that word,

216
00:29:37,600 --> 00:29:45,760
it sounds like, sorry, put this, and when he was asked, the amakai thing, no, the amakai, the

217
00:29:45,760 --> 00:29:53,840
monkey amakai asked, sorry, put that, how is it that Nibanda can be happiness? There's no feeling,

218
00:29:55,360 --> 00:30:00,480
there's no feeling, how can it be happiness, right? Nibanda said it's precisely because there's no

219
00:30:00,480 --> 00:30:14,480
feeling that it's happening, so what's a way to die? No, I don't know how way to die, I don't,

220
00:30:14,480 --> 00:30:21,680
it's not in the dictionary, but a way to die is unf, is felt, so a way to die, I don't know,

221
00:30:21,680 --> 00:30:31,680
you have to give me context, I could search for it,

222
00:30:38,640 --> 00:30:47,680
it's not even in the, it's not even in the, it's not even in the, the bitticus,

223
00:30:47,680 --> 00:30:53,680
the nikai is, so there, maybe it's in the commentaries or something, you have to give me some

224
00:30:53,680 --> 00:31:01,040
context, way did that means experienced, a way did that means you haven't experienced it, so

225
00:31:02,240 --> 00:31:11,600
I don't think it refers to Nibanda, but it's a way the nia probably, or it's without way in the

226
00:31:11,600 --> 00:31:27,280
nia, what are you doing, are you traveling, what are you doing, are you traveling again soon

227
00:31:27,280 --> 00:31:32,160
with the robes, the robes are in Bangkok, we're going to pick them up when I get to Bangkok,

228
00:31:32,160 --> 00:31:36,880
travel to Chiang Mai with them, I have to get a ticket to Chiang Mai somehow,

229
00:31:36,880 --> 00:31:44,480
have we worked that out, it's Robin here, you have to work that out at some point,

230
00:31:44,480 --> 00:31:49,440
the ticket to Chiang Mai, maybe I'll just take a bus, a bus is so much cheaper,

231
00:31:50,240 --> 00:31:58,640
probably much more environmentally friendly as well, yeah, so I'll be leaving here May 31st

232
00:31:58,640 --> 00:32:06,560
and I'll be back again July 1st, I think.

233
00:32:15,360 --> 00:32:21,440
Wasanka you can search for it on the DPR, I can search for it and they can find it in the

234
00:32:21,440 --> 00:32:28,320
commentaries maybe, way data means where does that, that's very common, where do we see that,

235
00:32:28,320 --> 00:32:38,560
way data, way data, if it's very common, the word way data on this, the body uses that a lot,

236
00:32:41,120 --> 00:32:44,640
I can't think off the top of my head, one of the main suit does that's the word way data,

237
00:32:44,640 --> 00:32:55,520
way data boys should be experienced that kind of thing, weekly interviews during June,

238
00:32:55,520 --> 00:33:01,680
probably, I'm going to try my best to have internet wherever I go, I guess I can't say for sure,

239
00:33:01,680 --> 00:33:08,080
but I'll try my best, I'll have to work it out with Satanya and Bangkok, I don't know if I have

240
00:33:08,080 --> 00:33:14,560
my SIM card here or if it's still working, I don't remember how that goes, I have a not a Thai

241
00:33:14,560 --> 00:33:21,680
number, a Thai cell phone number which would give me internet there, Wi-Fi is still not that big of a

242
00:33:21,680 --> 00:33:29,120
thing there and anyway I wouldn't have my own Wi-Fi work where I'm going, a lot of people use

243
00:33:29,120 --> 00:33:43,520
mobile internet, okay enough, that was lots of questions, let's not overdo it, save some questions

244
00:33:43,520 --> 00:33:49,600
for tomorrow, tomorrow I might, tomorrow I won't be here, I don't think, I'm going to give a talk

245
00:33:49,600 --> 00:34:00,160
in Mississauga, at a funeral, or not even a funeral, some kind of memorial service which

246
00:34:01,600 --> 00:34:07,520
seems somewhat unbutous to me, I'm not really convinced that memorial services are terribly

247
00:34:07,520 --> 00:34:16,560
butous, dead is dead, the person's born again, move on, I don't know, not convinced, it seems to

248
00:34:16,560 --> 00:34:22,160
me somehow clinging to the past, right, death is something that's already happened, you know, the

249
00:34:22,160 --> 00:34:31,200
person moved on, why can't you, but not something I've thought deeply about, anyway it's a good

250
00:34:31,200 --> 00:34:38,320
opportunity to teach the dumbness of whatever, anyway, so probably not be here tomorrow evening,

251
00:34:38,960 --> 00:34:44,400
but we'll see, it could be, no, I won't be back in time, and I missed next, I'm going to

252
00:34:44,400 --> 00:34:53,520
miss tomorrow, so have a good night, wishing you all the best, be well, you know, we've got video

253
00:34:53,520 --> 00:35:23,360
up here, and I'll turn that off.

