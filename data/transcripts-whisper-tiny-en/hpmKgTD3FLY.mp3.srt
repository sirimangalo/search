1
00:00:00,000 --> 00:00:07,400
Okay, what should we do to stay in the middle path?

2
00:00:07,400 --> 00:00:19,120
Truth is, technically speaking, technically speaking, and some people aren't going to

3
00:00:19,120 --> 00:00:26,200
like this, I don't think, but technically speaking, we're not and we never are in the middle

4
00:00:26,200 --> 00:00:35,480
path until the moment, maybe two to three thought moments, if I remember correctly.

5
00:00:35,480 --> 00:00:37,880
No that's not even true.

6
00:00:37,880 --> 00:00:47,840
One thought moment, only for one thought moment, and the maximum time a person can be in the

7
00:00:47,840 --> 00:00:57,600
middle path, ever in their entire existence in Samsara, is four thought moments.

8
00:00:57,600 --> 00:01:02,440
Once when they become a sotapana, once when they become a saki-dakami, once when they become

9
00:01:02,440 --> 00:01:08,680
an anangami, and once when they become an anangami, if I'm not mistaken, and it seems quite

10
00:01:08,680 --> 00:01:14,960
unlikely that I am, technically speaking, we're only on the eightfold noble path, which

11
00:01:14,960 --> 00:01:22,760
is the Buddha's definition of the middle way, what was I saying with that said, we're

12
00:01:22,760 --> 00:01:27,760
only on the eightfold noble path for four thought moments.

13
00:01:27,760 --> 00:01:33,240
The only time that we're on the eightfold noble path is the moment before, the first

14
00:01:33,240 --> 00:01:41,800
moment of realizing the bandana, that's called the maga, maga nyanana.

15
00:01:41,800 --> 00:01:49,120
That's the sotapati maga, sotapati maga-chitra, we're going to listen abhidamana, we

16
00:01:49,120 --> 00:01:54,760
have to memorize it all, how does it go?

17
00:01:54,760 --> 00:02:13,720
I forget how it goes now, sukha, sukha, sahagata, no, pitti sukha, pitti sukha, aikagata, sahitang,

18
00:02:13,720 --> 00:02:26,000
sahagata, pitti maga-chitang, that's the name of the first one, pitta kavitara, pitti sukha,

19
00:02:26,000 --> 00:02:35,560
aikagata, sahitang, sahitang, sahagata, pitti maga-chitang, that's the first jana-chita for a

20
00:02:35,560 --> 00:02:46,520
sotapana, if a sotapana becomes sotapana from the first jana, then that is their jita,

21
00:02:46,520 --> 00:02:57,640
as five factors, vita kavichara, sukha, pitti aikagata, and it's called the sotapati maga,

22
00:02:57,640 --> 00:03:09,440
the path of mind, and so on, and so there's four for each, or four for each of the janas,

23
00:03:09,440 --> 00:03:19,720
we've got this rock, five for each of the janas in the Abhidhama, five janas, and four

24
00:03:19,720 --> 00:03:24,720
for each of the types of holy ones.

25
00:03:24,720 --> 00:03:31,480
So maximum one can go through the misfour times, go through the maga-chitang four times,

26
00:03:31,480 --> 00:03:35,240
but each one is a different maga-chitang.

27
00:03:35,240 --> 00:03:36,240
So what does maga mean?

28
00:03:36,240 --> 00:03:42,760
Maga means the moment, in that sense, maga means the moment when you attain perfect

29
00:03:42,760 --> 00:03:54,160
balance, and perfectly balance the faculties, perfect, it's called anuloma, what we're

30
00:03:54,160 --> 00:04:03,160
trying to do until we get to that point is conform, this is called anuloma, the word

31
00:04:03,160 --> 00:04:09,840
anuloma is an interesting word that I've talked about this before, anuloma means anumines

32
00:04:09,840 --> 00:04:17,040
according to or in line with or following, the following is probably the most direct translation,

33
00:04:17,040 --> 00:04:24,360
it's anuloma is apparently, if I'm not mistaken, it's this line, the grain, loma means

34
00:04:24,360 --> 00:04:31,240
grain, I have looked this up, but it's been a while, and according to my understanding,

35
00:04:31,240 --> 00:04:37,200
loma means grain, so anuloma means with the grain, they're translated into the visudim

36
00:04:37,200 --> 00:04:45,160
in the path of purification as conformity, but it means going with the grain, so that means

37
00:04:45,160 --> 00:04:51,320
conformity actually, better, easier to understand, literally going with the grain, following

38
00:04:51,320 --> 00:05:01,680
the grain, but the meaning is conforming, conforming to the dhamma or the truth or reality,

39
00:05:01,680 --> 00:05:11,400
conforming to goodness, conforming to wisdom, conforming to the buddha's teaching, and

40
00:05:11,400 --> 00:05:16,240
so in our practice of meditation, we're coming to see things clear, we're not on the

41
00:05:16,240 --> 00:05:22,920
noble path, as we see things clear and clear and clear, eventually we let go, eventually

42
00:05:22,920 --> 00:05:31,680
all of the extremes disappear, we let go of them, and I think this is the best answer

43
00:05:31,680 --> 00:05:37,080
to your question, this part of your question is that you don't stay in the middle path,

44
00:05:37,080 --> 00:05:44,600
you get there, and you get there by letting go, think of it as a person holding on like

45
00:05:44,600 --> 00:05:48,840
this, you can't see both my arms, but I'm holding on with both arms, so I'm being pulled

46
00:05:48,840 --> 00:05:54,400
this way, and I'm being pulled that way, because I'm clinging, but eventually I see

47
00:05:54,400 --> 00:05:59,520
that this is just causing me to be completely dizzy and off balance, and I let go with

48
00:05:59,520 --> 00:06:04,360
this arm, and I let go with this arm, and when I do that, what happens?

49
00:06:04,360 --> 00:06:11,240
Suddenly I'm no longer dizzy, I'm no longer swaying, I'm balanced, and so this is what

50
00:06:11,240 --> 00:06:17,400
we eventually do, and eventually we get so in the middle, it's kind of like we're in

51
00:06:17,400 --> 00:06:24,200
a tube, say, we're in this tube, and we're clinging to the walls, we got all these tentacles

52
00:06:24,200 --> 00:06:32,760
going out, eventually we let go, and we just slip out of the tube, right, we're stopping

53
00:06:32,760 --> 00:06:42,040
our progress, we're blocking our escape, it's like we're in those cartoons, we're trying

54
00:06:42,040 --> 00:06:48,040
to push the one guy out the door, and he holds on or something, he puts his legs on either

55
00:06:48,040 --> 00:06:52,920
corner, and he's on the door jam, and they're trying to push him out the door.

56
00:06:52,920 --> 00:06:57,680
That's what we're doing, and eventually you let go of this, you let go of that, you tuck

57
00:06:57,680 --> 00:07:04,680
your legs in, and you go through the door, that's really what it means to retain the

58
00:07:04,680 --> 00:07:09,520
the noble path, that's really what our practice is all about, there's so many things

59
00:07:09,520 --> 00:07:13,360
that we're clinging to, and that's really, it's not a secret, it's not strange, wow,

60
00:07:13,360 --> 00:07:18,160
this mystical thing called nibano, or the noble eightfold path, it just means you know,

61
00:07:18,160 --> 00:07:25,680
now you can go, now you're free, now you're not stuck anymore, so sorry to break it to

62
00:07:25,680 --> 00:07:30,960
you, that you're really not following the middle way, unless you've already become an

63
00:07:30,960 --> 00:07:39,960
arachant, or at least so dependent, but most likely you're in the stage of anuloma, an

64
00:07:39,960 --> 00:07:47,760
anuloma is two senses, one is where we're conforming, this is the, I don't remember, I think

65
00:07:47,760 --> 00:07:54,280
this is called anuloma nyana, but the technical anuloma nyana is when we conform, and

66
00:07:54,280 --> 00:07:59,560
conform is that moment where you tuck your arms and tuck your legs in where you suddenly

67
00:07:59,560 --> 00:08:07,280
fit, right, oh now he fits through the door, right, like Winnie the Pooh, and when he,

68
00:08:07,280 --> 00:08:11,440
they stopped him from eating honey, right, he was stuck in a rabbit store, and finally

69
00:08:11,440 --> 00:08:17,320
he fit, that one moment was when they can finally throw him out and into the tree with

70
00:08:17,320 --> 00:08:25,040
all the onions, good metaphor for nyana, we need a pooh, remember the story, stuck in the

71
00:08:25,040 --> 00:08:34,320
door, then he gives up his honey, right, boom, he was flying into the honey tree, and suddenly

72
00:08:34,320 --> 00:08:44,000
he's in nyana, right, but the point is there, suddenly you fit, and that's the moment

73
00:08:44,000 --> 00:08:49,960
that is in another sense called anuloma nyana, the moment, the knowledge of conformity

74
00:08:49,960 --> 00:08:59,560
or the wisdom that is marked by conformity, where finally you fit, you get it, suddenly

75
00:08:59,560 --> 00:09:07,320
you're in line, suddenly there's, there's, or you've at least, you've let go for the

76
00:09:07,320 --> 00:09:13,920
time being, you've let go enough so that the mind can escape, the mind can be free,

77
00:09:13,920 --> 00:09:20,920
and that's the moment when the mind drops into nyana, at the most first moment when one

78
00:09:20,920 --> 00:09:27,920
realizes nyana, this is called manga jita or manga nyana, just for one moment, when

79
00:09:27,920 --> 00:09:31,560
people wonder, well why is it just one moment, why couldn't it be two moments, it could

80
00:09:31,560 --> 00:09:38,400
never be two moments, the point being, it's totally technical, this is really a, probably

81
00:09:38,400 --> 00:09:48,200
a, in many, for many people, it's a totally un, what's the word, see, have lost on

82
00:09:48,200 --> 00:09:59,840
my language, it's a, well terrible, a terrible explanation, and for most people it's, it's

83
00:09:59,840 --> 00:10:07,520
not what they wanted to hear, inadequate is the word, it's inadequate explanation, because,

84
00:10:07,520 --> 00:10:10,920
you know, we hear about this noble path and, and the Buddha using it metaphor, using

85
00:10:10,920 --> 00:10:18,160
it in a very much more conventional sense, but here we have this excellent teaching in

86
00:10:18,160 --> 00:10:21,440
the abidama about how to break it up, technically, when do you want to know the path

87
00:10:21,440 --> 00:10:26,360
for one moment, why, because what is the function of the noble path?

88
00:10:26,360 --> 00:10:35,680
The path is called the dukaniro da gami nipatipada, the path which leads one to the cessation

89
00:10:35,680 --> 00:10:41,400
of suffering, what is it that leads one to the cessation of suffering, it's the realization

90
00:10:41,400 --> 00:10:48,960
of nibanda, it's the realization of sotapanda, and the Buddha said sotapatam sotapatimanga,

91
00:10:48,960 --> 00:10:57,320
or the person who is on the path to become a sotapanda, but there's two explanations

92
00:10:57,320 --> 00:11:01,720
here, I'll get to the second one in a second, but technically speaking, that moment is

93
00:11:01,720 --> 00:11:08,480
called path, because it's the moment that changes you, it's the moment that removes

94
00:11:08,480 --> 00:11:14,120
suffering, let's, let's, to be, to be perfectly clear, we'll talk about the, at the Arahant

95
00:11:14,120 --> 00:11:24,680
level, when one reaches Arahatamanga jita, at that moment, suffering is ended, and there

96
00:11:24,680 --> 00:11:31,440
is no more clinging in the mind, no more delusion, so the person can never fall back into

97
00:11:31,440 --> 00:11:37,240
suffering ever again, if you say, well what about the second one, the second moment,

98
00:11:37,240 --> 00:11:45,600
so suppose the person enters into nibana, niroda, or however, for a day, the rest of

99
00:11:45,600 --> 00:11:52,840
the day is called palanyan, it doesn't have the same effect as maganyan, because technically

100
00:11:52,840 --> 00:11:59,080
speaking it does nothing, what's done, the first moment did it, right, so technically speaking

101
00:11:59,080 --> 00:12:04,400
the rest of the moments are just fruit, palanyan, they don't have any effect, technically

102
00:12:04,400 --> 00:12:11,440
speaking, they don't cross, because they don't cross the barrier, they don't cross the

103
00:12:11,440 --> 00:12:16,640
line, there's that line that said, well here you don't understand, and then boom suddenly

104
00:12:16,640 --> 00:12:22,160
you understand, well one moment is enough, you saw nibana, okay, but people can stand

105
00:12:22,160 --> 00:12:28,960
it for a day or even a week, they say, so that's the technical definition of what

106
00:12:28,960 --> 00:12:34,440
is maganyan, no that's called the ariyamanga, there is something else that is called

107
00:12:34,440 --> 00:12:43,960
pubangamanga, and where this fits in technically speaking, I'm a terrible scholar to say

108
00:12:43,960 --> 00:12:57,560
the least, but pubangamanga is the time before we attain the maganyan, where we are trying

109
00:12:57,560 --> 00:13:06,320
to conform to the full noble path, so we're on a path, but it's called the pubangamanga,

110
00:13:06,320 --> 00:13:13,440
and it is said that there are eight factors to the pubangamanga, and those are the same

111
00:13:13,440 --> 00:13:23,000
eight factors, a right view, right thought, right speech, right action, right livelihood,

112
00:13:23,000 --> 00:13:27,520
right effort, right concentration, right mindfulness, they're all the same, but it

113
00:13:27,520 --> 00:13:37,600
's not the middle way, it's not the noble path, noble eight-fold path, not technically speaking,

114
00:13:37,600 --> 00:13:46,840
so probably the most important part of what I've just said, and I've said a lot, is that

115
00:13:46,840 --> 00:13:51,400
you don't try to stay on the middle way, you try to come closer to it and eventually

116
00:13:51,400 --> 00:14:00,560
attain perfect balance, perfectly balanced mind, it doesn't come from trying to do a balancing

117
00:14:00,560 --> 00:14:04,640
act, it comes from letting go of the extremes, that's the other thing I wanted to say,

118
00:14:04,640 --> 00:14:08,720
when the Buddha talked about the middle way, and he talked about the two extremes, what

119
00:14:08,720 --> 00:14:18,520
he said is a nupagama, he didn't go to those two extremes, duet may be kuehantapabajitin

120
00:14:18,520 --> 00:14:28,760
and the savitabha, I forget it, anyway he said, I didn't go to these two extremes,

121
00:14:28,760 --> 00:14:37,880
but a ubo ante anupagama, neither of the two extremes I went to them, that's how I attained

122
00:14:37,880 --> 00:14:55,400
the middle way, that's how you find the middle way, that's how you find the middle way.

