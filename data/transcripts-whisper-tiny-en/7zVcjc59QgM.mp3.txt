Living in everyone, broadcasting live, May 11th.
Today's topic is friend, friendship, friends.
That's from the Sigilawad as it then.
Exortation or the teaching to Sigilah or Singalika,
depending on how you say it.
That's why they considered to be the definitive, definitive source for
people.
Worldly,
worldly right conduct and
department and practice.
So how to practice the dhamma in the world, living in the world.
So a lot of it is fairly, I guess, relatively superficial,
in the sense that there's much of it isn't about deep,
spiritual truths.
But it's all very dhamma.
It's about very righteous, very good,
about living a good life, living an honest life,
living a successful life.
Finding happiness in this life, happiness in the next life.
And cultivating spirituality, even as a lay person.
This is one of the better-known passages from it.
It's about the four types of people who are called
Amitamitapati Rupa.
Amitamitam means they are not friends.
Amitamitapati Rupa means they appear to be friends.
They have a parent,
sort of the...
They're like friends.
So there should be known as appearing to be friends,
but not friends.
And then we have the four types of people who are called
Amitasu Suhanda.
Amitamitam means they are friends.
Suhanda means they are real friends.
I don't know where that word comes from.
Suhanda.
And so we need about these passages.
It does go into some detail about each of the four,
but firstly step back and let's talk about this distinction.
Because the interesting point being made here
is a questioning that we must question
analyze and apply wisdom to friendship
and that this is an important aspect of our spiritual practice.
And what makes a person a friend?
Is it the fact that you have known them for a long time?
Is it the fact that they bring you pleasure or joy?
Is it the fact that even gauge in amusement or entertainment with them?
And all of these are reasons why we choose friends.
And too often you hear people lamenting the relationship
that they have with their friends,
but yet still trying to make it work,
because why?
Because he or she is my friend.
Which is curious, because if the person is a
friend through and through corrupt individual,
why are they your friend?
You see, so the reason why we pick friends
or why we consider people to be our friends is
often someone arbitrary.
I remember in high school a very interesting time
in a sort of a sick and twisted way,
because it could be friends in a group
and then suddenly one person would become ostracized
and forced out of the group,
just because it just seemed seemingly on a whim,
suddenly everyone shunned someone.
I remember this.
I was never on the receiving end of it.
But I remember being part of that.
And the cleats and really bizarre looking back
and wondering why?
Why did we pick the people we picked?
Often it's mob mentality,
very scary.
And the friends that I chose were often
for how excited they, you know,
we choose friends for how excited we become around
them often because we look up to them
or we envy them,
sometimes because they provide us simply
with entertainment or we consider them to be witty.
And why is even usually just smart or witty
or sometimes there's an amount of competition involved.
People who we compete with in friendly competition
remember when I returned back from having meditated
I lost most of my own friends.
I tried to hook up with them,
but I was only really interested in bringing them
to come and practice meditation,
which they generally weren't interested in.
And I was doing a poor job of being human relationships
very much indoctrinated at the time.
But yeah, this is the key is
the importance of right friendship.
You know, the most important thing for us
is our inner practice,
but the second most important thing is
got to be friendship.
Because it's your place
in what it means to be a being, you know,
in sentience where you place yourself
on the scale of sentience of the positive
or the negative, the good or the bad.
It's very much to do with your friends.
If you place yourself in a group of friends,
group of people who are committed to unwholesumness,
that's where you place yourself
in your state of being.
So it's your external, it's your environment,
it's the external.
But the internal is all about your own meditation,
and that doesn't matter who you're surrounded by.
But the external is that which supports
and that which nourishes your spiritual practice.
And so surrounding ourselves with whoever
is really a poor way of going about it.
But it gives us some fairly clear guidelines
and a sharp sort of distinction.
How should we go about making friends
or choosing friends, choosing who we associate with?
Now, it's important to understand
that it doesn't refer to who we should be friendly towards.
I think that's fairly obvious,
but sometimes we confuse the two.
Just because someone you don't consider
someone worthy of friendship doesn't mean you shouldn't be friendly to them.
I think because of distinction,
because I don't think everyone should be worthy of our friendship.
I've eventually considered worthy of our friendship.
Some people are simply not worthy of it.
I don't know if that sounds somewhat arrogant or cold or so.
Maybe it's the wrong way of placing it worthy unworthy
is not maybe not the best word.
I think in a technical sense, it does make sense.
Like, it's not worth having this friendship.
This friendship isn't good for either of you.
This friendship isn't something that is beneficial.
There's no benefit, therefore it's not worth it.
That's all I really mean.
Don't mean to put ourselves above others.
Just there's no benefit to it.
If all you do is drag each other down,
not a good thing.
You know, having letting evil people
take advantage of you is very bad for them, right?
So it's not something you want to cultivate
not just for your own benefits for theirs as well.
So the four types of English,
these are pretty good.
I better not think I'll get mixed up.
So there's the greedy person.
That's not the right translation.
Anyway, the greedy person, the one who speaks,
but does not act.
He's not translating directly.
And yet that to a hero.
One who...
One who looks only for their own benefit
to something like that.
I'm not quite sure.
One key, Paramo, one who is only entirely
saying, the Tappo.
Means they say good things, but that's it.
Paramo, I mean, that's it.
They extend what she is speech.
You're only a daily talk of good game.
Anopia, Bani, one who speaks.
One who speaks, one who flatters.
Right, Anopia, Bani, one who speaks to flattery in the flatter.
And number four, Abaya, Sahayo.
But the squander.
Abaya, Sahayo, one who is your companion in ruin.
One who leans you to ruin.
So these four.
The person who is only out for their own game.
The one who is only good for...
Only good for empty promises, that kind of thing.
The person who is flatter, the flatter.
And the companion in ruin.
And then he gives four reasons for each.
So the greedy one, the one who is only looking for their own.
They're greedy, they give little and ask much.
Anyway, you can read through these.
They're in the Degan Nikaya, the Siegel of Adasupa.
Degan Nikaya, number 31.
Definitely worth reading.
They're going to go into it, just talking a little bit about friendship and good friends.
The four kinds of good friends, people who should be known as friends.
Let's go to the Pali.
Pakaaru, one who does, one who helps.
Samana Sukanduku, one who is the same and happiness and suffering.
Atakai, one who...
One who speaks, one who speaks, one who speaks, one who is beneficial.
Anukampako, one who is compassionate.
That's sympathetic.
Yeah, sympathetic.
These are four real friends.
So the first one, let's talk at least a little bit explaining these, right?
So the first one is the greedy one.
This is the kind of person who just is only your friend for their own benefit.
Maybe they borrow money from you, or maybe they mooch off of you.
They're only in it because of the benefit it is to them.
The material benefit that you give them.
Not beneficial for either of you, really.
One who speaks, this is a person who talks about it, it says in the description.
They talk about things they've done for you in the past.
They talk about things they'll do for you in the future.
But they never do anything.
They don't do anything to help others.
But they're always bragging about how much they've done for you in the past.
Or how much they'll do for you in the future.
Once had a man, when I was in Thailand, he just lied through his teeth.
It was really impressive.
He told us he had all this money coming to him, and he was going to give us $250,000 to start a monastery.
And then he just up and disappeared.
So he got me to help him with a bunch of stuff.
And after I realized that was it, he was just buttering me up with all these flat-out lies.
These people actually do exist.
Call on artists.
Anopia Bahani, one who flatters you.
Yes, I've been through this.
I've been to people.
When I was in Sri Lanka this happened, it was incredible.
This man, oh, he was such a flatter.
I didn't.
That really didn't get to me.
But I had no clue that he was a bad person because I was given into his care by a monk who
was very, very famous, fairly well-known monk in the US.
And it was really bizarre finding out that people were giving him donations for me.
And he was, he was squandering among women and drink and gambling.
Incredible.
He had a long story.
But he was definitely a flatter.
It was very dangerous because it's easy to get a big head when people flatter you.
And once you have this, you're objectivity.
Abaya Sahayo.
This is a person who leads you to ruins.
This is the kind of person we often become friends with.
There was an interesting thing coming back and looking at people who I knew and realizing
that many of the people who I had focused my energy on were exactly the type of people
who were, you know, detrimental leading to ruin.
And people who liked to drink, the people who liked to, you know, people who were obsessed
with sensuality, sexuality, that kind of thing.
And a lot of the people who I had ignored and have marginalized because they were boring
because they were straight, you know, on the straight and narrow.
Looking back and looking at them, they really became, wow, these are the kind of people
I would have liked to have stayed in contact with.
Some of them I have actually gotten back in contact with.
They weren't good people.
And that's the thing is we often, our friends have friends not because the good they do
for us because of how they reaffirm or they assuage our guilty feelings at our indulgence.
And it's because they indulge with us.
And so we're not the only one in there for.
We're, we're less concerned about our own wholesomeness.
We have someone else doing it as well.
Can't be all that bad, right?
And the four who are real friends.
It actually helps you, you know, friends who help each other who, you know, provide actual
support.
You know, somebody is your friend when they think to help you and they think to do something
to your benefit when they guard you and they, they got your back, you know.
Samana Sukaduko is someone that doesn't abandon you in times of trouble.
When the going gets hard, it gets, going gets tough.
That's when, you know, you're real friends when they stick with you.
They don't abandon you and you're in, in difficulty.
You know, this person's no fun anymore.
Now he's all got all these problems.
Let's go find people who don't have all these problems so they can have a happy time again, you know.
Now someone who sticks with you in happy times and in unhappy times.
Now the other people are like that when things get good.
When things are bad, they stick with you.
When things get good, they become intoxicated with their pleasure and they treat you poorly.
Atakayi is someone who speaks, speaks benefit.
It speaks meaningful things.
This is a person who you definitely have to stick with.
This is like the Buddha.
Buddha is the greatest friend because he speaks that which is beneficial, that which is helpful.
Stay with such people.
People who say good things, people who instruct or advise or remind you of what's good and what's bad.
Even if it hurts sometimes, sometimes it's unpleasant to be around people who point out your faults.
But you should consider them as pointing a buried treasure. It's far more valuable.
Anukampako is one who sympathizes. One who sympathizes is one who wishes to help.
One who is compassionate when you're in suffering.
You try to find a way for you to get out of suffering.
Someone who is in tune with you when you need a friend to listen.
When you need someone to listen to you, they listen.
When you need someone to support, they're there to support you.
They sympathize.
Right?
The sad earless fortunes or joys is in your good fortune or strains others from speaking ill of you and he commands those who speak rather.
A little bit about friendship, definitely worth reading the whole system.
We got some questions here from last night.
Wasn't Anne under being altruistic by tending to the Buddha's needs.
Potentially altruistic.
But he was only a sort upon this.
He didn't ask to be a Buddha's attendant.
He was asked to do it.
He was doing it out of gratitude.
But my point about altruism is that people are altruistic because it pleases them to be altruistic.
If being altruistic was thoroughly repulsive to you, how could you be altruistic?
It was thoroughly unpleasing to you.
So what we do, we do because it pleases us.
So it's not really altruistic.
We help others because it pleases us to do so.
We may not say it and we'll say no, that's not why we do it.
But it is.
We do it because it has a positive association in our minds.
Still for our own benefit, that's the thing.
Due to also having unhealthy thinking and views, I noticed that it was difficult to make progress.
That we're practicing when and or how can I assess that I'm wholesome and guarded well enough to start incitementation again.
Having unhealthy thinking and views, well thinking isn't unhealthy.
This is the problem. If you judge your thoughts, that's the problem, not the thoughts themselves.
So try and let the thought speed just acknowledge the thinking, thinking, I mean, it's a process.
You should never put aside incitementation unless you're insane.
You know, then you've got to become unsafe.
If you can't be mindful period, then you can't practice.
But as long as you can my teacher said, if you can show someone a cup of water and a cup of rice.
And if they can tell the difference, they can practice incitementation.
Difficult to make progress.
You say it was difficult to make progress.
Absolutely, it's difficult to make progress.
If it was easy, we'd all be enlightened.
That's not a reason to stop.
In fact, it's a sign that you're probably doing something right.
That's if it was very, very easy.
You might want to be suspicious.
Not always. Some people have easy practice, but for the most part, it should be difficult because it challenges you.
So just because you're banging your head against the wall, it feels like banging your head against the wall.
It doesn't mean you're doing it wrong.
At very least, you're learning that banging your head against the wall hurts.
That's very much a part of what our practice is.
Learning how much it hurts to bang their heads against the wall.
So having the future, we don't even think to do it.
And then we have someone named Melinda teaching here.
Always wary of people coming on here and teaching because I don't know it.
They're teaching what I teach.
So everything here should be taken with a grain of salt.
And we don't endorse the views of anyone who posts.
We should have a little disclaimer somewhere.
Are there advantages or disadvantages to meditating outside a nature?
Who is this?
Looks familiar.
Former academic.
I've answered this before actually.
A couple of times, I think.
So nature, I, my view, is that nature is beneficial not for what it is, but for what it isn't.
Nature is about the most recognizable or comfortable environment for human being to be in,
because it's still very much in our psyche.
That's why when we go to nature, we feel peaceful because it doesn't have all this stuff that is not familiar.
So because nature is free from all of this jarring stimuli,
therefore I think there is a benefit to practicing in nature.
I mean, the Buddha was clear about it.
It seems to be, you know, it's not the kind of thing you want to put too much emphasis on obviously,
because in the end, it's all just seeing hearings, not tasting, feeling, thinking, but nature makes it easier,
which may or may not be a good thing, but in the beginning is generally a good thing.
And it makes your practice go quicker and makes it stronger, so generally a good,
not because of what it is, but because of what it is.
Because it isn't jarring, it isn't stress-inducing, so it makes you calm quite quickly.
Considering taking refuge in the five percent,
what considering or change, consideration should I make, consider to these arrangements after formulating?
I've already made arrangements for cremation of our bodies.
I wouldn't worry about the body.
The body's just ashes to ashes and dust and dust, whatever that means.
It's just dust and the wind.
But good for you, we're taking refuge in five percent.
I mean, it's a bit...
I mean, the kind of the cynic in me wants to say,
there really isn't a ceremony.
I want to ruin this whole thing for y'all, because taking refuge means taking refuge.
Do you take refuge?
Listen to the Buddhist teaching, follow it, and appreciate it.
And the five precepts, do you keep them?
But over time, there has evolved to be a ceremony.
So there is a ceremony.
And I'm happy to do it.
In fact, we do it every week when we have our super study class,
or we should be manga study class.
We just haven't had any of these classes, but for that class,
we've been doing it every time before we do the class.
Now, like in Thailand, when they come for...
When they come to offer lunch to the monks,
often the monks will give them the five precepts and the three refuges.
They'll do a ceremony.
I mean, it's done every day in Buddhist monasteries in Thailand.
In Sri Lanka, also for Sri Lankans, when they offer lunch to the monks,
monks give them the five precepts.
The three refuges.
So it's not that big of a deal, but there is a ceremony
and it's something you can do anytime.
I don't...
I mean, I see a lot of Westerners really making a big deal out of it
and telling me about other people who did the ceremony.
I'm like, well, you know, do that every day in Thailand.
I don't know people that once a week or whenever they do it,
some people chanted every day.
I used to chant the five precepts and the eight precepts.
I chanted the eight precepts on the impulse of the day.
And the five precepts regularly.
That was so wonderful for me,
because I had broken all the precepts, right?
And to just have this power where now I was finally doing something
that I could see was right.
I could know was right.
It's pretty powerful.
I'd say just go ahead and do it from the most part.
I don't think that was quite your question.
I wouldn't worry about those arrangements.
How close to Chan, CNC and Tai Chi?
I don't know what you're saying.
Sorry.
It's not a very good...
Well, well-worded question, statement sentence.
Do we need to see the rising and falling
and the stomach in terms of the primary element?
If so, does it become Dhamma Nupasana at that point
instead of Kaheh Nupasana?
Sanka, you're overthinking things as usual.
You're overthinking things to mind.
Rising and falling is the wild doctor.
It's stiffness, that's M.
You can't help but see it.
It is.
It's like, should we see a tiger as a tiger?
When you see a tree, should you see the tree in it?
It is a tree.
You don't have to go looking for the tree.
This is why I'm a dad, so it is.
That's the problem in Dhamma Nupasana.
It's a really difficult one to explain because it's much more about...
It's much more about what it is.
That's highly unexplanatory, but it's about them being groups.
It's not about the individual part, so the five aggregates.
It's about the concept of the five aggregates, the teaching, the Dhamma, the five aggregates.
Five aggregates as Dhamma is different from the five aggregates.
As Dhamma, it's an idea, the idea of that we are in the original self,
that we are just made up of the five aggregates.
I mean, that's not even entirely comprehensive,
but the Dhamma is very hard to explain for me.
I've never had a really satisfactory explanation,
which makes me feel that it's actually not something that can be explained
homogenously.
The different aspects of Dhamma refer to different things
and different meanings of the word Dhamma even,
but they all are teachings of the Buddha.
Dhamma Nupasana is much more...
I like to translate Dhamma and there is teachings.
So when you're practicing Satipatana, you need to keep these things in mind as teachings,
because they play an important role in your meditation.
That's about the best I can do with Dhamma Nupasana.
There is a sipto where the Buddha instructs Mogulana
and how to overcome drowsiness.
Mogulana is practicing the sineless concentration.
It's just the same as seeing the impermanence
that the noting practice leads to, yes, sineless.
Sineless is anmita.
It's not anmita.
It would be animitta.
Animitta is a sign.
Animitta means no sign.
It means no warning, basically.
There's no precursor.
There's nothing that bad.
I can't think of the word foreshadow or another thing
that gives you an indication that something's going to happen.
Remember the word?
So it refers to impermanence, because things don't have warning,
they change chaotically.
You can't predict, you can't expect.
So trying to fix, trying to hold on is a sure recipe for disaster and suffering.
Are you trying to ask how close we are to these things?
Are we close to being chan, dharma realm?
Because that's none of us, none of that is us.
I would recommend that you read my booklet on how to meditate
because that's where you start.
And then you can take it from there and see what you think.
Anyway, let's stop there then.
Thank you all.
Have a good night.
We will probably see you tomorrow.
Are you seeing me tomorrow?
Thank you.
