1
00:00:00,000 --> 00:00:06,920
Okay, can you tell me about the Tenri Pasa Nupa Qieleisa?

2
00:00:06,920 --> 00:00:21,480
Mavi Pasa Nupa Qieleisa are good things that get in your way.

3
00:00:21,480 --> 00:00:32,680
They are fruit of the practice that stops you from progressing, and it comes down to the basic

4
00:00:32,680 --> 00:00:40,560
principle that the practice is the only thing that leads to progress.

5
00:00:40,560 --> 00:00:48,160
Any result that you attain, that result is not what leads to progress, so the mistake

6
00:00:48,160 --> 00:01:06,960
that people make is that somehow the result is the practice, or it may not even be so explicit.

7
00:01:06,960 --> 00:01:16,800
They might just like the pleasant sensations or the positive experiences.

8
00:01:16,800 --> 00:01:24,320
So by either taking it as the right practice, taking these good things as being in and

9
00:01:24,320 --> 00:01:32,160
of themselves through the path to progress, or by simply clinging to them, one gets stuck,

10
00:01:32,160 --> 00:01:35,120
obviously because one stops practicing.

11
00:01:35,120 --> 00:01:39,680
This is an important principle because everyone goes through this.

12
00:01:39,680 --> 00:01:43,400
If you're practicing correctly, you're going to come to the Vipasa Nupa Qieleisa, if not

13
00:01:43,400 --> 00:01:47,480
one then another, then altogether there are ten of them.

14
00:01:47,480 --> 00:01:53,680
You might even add a couple on, I don't know why there are ten, but they're a good set.

15
00:01:53,680 --> 00:02:00,800
And so see if I can rattle through them to beat the rapture.

16
00:02:00,800 --> 00:02:07,400
So you might feel this charge in your body.

17
00:02:07,400 --> 00:02:12,440
Some people find themselves shaking back and forth, some people laugh, some people cry,

18
00:02:12,440 --> 00:02:25,080
some people even yawn, you feel goose flesh, you feel light, you feel heavy, so on.

19
00:02:25,080 --> 00:02:36,920
So rapture is one, Obas, which means light, so people will see lights or colors or pictures.

20
00:02:36,920 --> 00:02:43,080
And yana, people will come to understand things about their life or even understand things

21
00:02:43,080 --> 00:02:44,080
about reality.

22
00:02:44,080 --> 00:02:57,360
So they'll start to see impermanence suffering in non-self and they'll start to understand this.

23
00:02:57,360 --> 00:03:04,680
Then faith, a person might get a great amount of confidence in themselves or faith in their

24
00:03:04,680 --> 00:03:10,080
teacher and faith in the practice and faith in the Buddha and so on and start to think

25
00:03:10,080 --> 00:03:17,000
about how great this practice all is and how great they are and how strong their mind

26
00:03:17,000 --> 00:03:19,160
is and so on.

27
00:03:19,160 --> 00:03:27,240
Pasa D, they might become tranquil, suddenly they have a sense of quiet.

28
00:03:27,240 --> 00:03:32,920
Quiet is probably the most common one, especially among people who don't have teachers.

29
00:03:32,920 --> 00:03:37,040
Whenever I go to give a talk somewhere, among people who have been practicing meditation,

30
00:03:37,040 --> 00:03:41,800
there's almost always some, when answering questions, there's almost always someone who

31
00:03:41,800 --> 00:03:47,720
asks the question, what do I do when nothing arises, when I get to the point where there's

32
00:03:47,720 --> 00:03:54,160
nothing to be mindful of and the answer is actually quite easy, but people can get stuck

33
00:03:54,160 --> 00:04:00,760
in this state for years without adequate teaching, simply because logically there's

34
00:04:00,760 --> 00:04:05,360
nothing to be mindful of, but there is, there's the quiet too, this is called Pasa

35
00:04:05,360 --> 00:04:07,960
D, it's Jaitasika.

36
00:04:07,960 --> 00:04:15,560
So, Pasa D, then energy, some people will have, be full of energy and be able to do

37
00:04:15,560 --> 00:04:23,400
walk in meditation all day, all night, they'll find themselves supercharged with no

38
00:04:23,400 --> 00:04:27,400
never getting tired.

39
00:04:27,400 --> 00:04:32,040
Carefulness, people will find that they're able to catch everything, acknowledge this,

40
00:04:32,040 --> 00:04:38,320
acknowledge that, and they'll feel good about their mindfulness.

41
00:04:38,320 --> 00:04:44,120
Happiness, so some meditators will feel very happy.

42
00:04:44,120 --> 00:04:53,240
Equanimity, some meditators will feel totally neutral and unperturbed with no liking or

43
00:04:53,240 --> 00:04:59,880
disliking in number 10, attachment, so they'll become attached to any one of the first

44
00:04:59,880 --> 00:05:04,920
nine, or they'll become attached to seeing, hearing, smelling, taste, and feeling, thinking,

45
00:05:04,920 --> 00:05:10,320
something arises that they become attached to.

46
00:05:10,320 --> 00:05:13,720
These are the 10 Vipasa Nupikinis.

47
00:05:13,720 --> 00:05:21,800
And for the most part, they're actually based on positive qualities of mind, I guess

48
00:05:21,800 --> 00:05:25,840
the key is the number 10, they're clinging to it, but it's also the mistaking.

49
00:05:25,840 --> 00:05:36,640
This is the stage in the Risudimaga, it calls manga, manga, nyana, dasunamisudhi, knowledge,

50
00:05:36,640 --> 00:05:46,200
yana, dasunamisudhi, purification, based on knowledge and vision of what is and what

51
00:05:46,200 --> 00:05:48,040
is not the path.

52
00:05:48,040 --> 00:05:56,520
So it's the struggle in the meditator's mind to determine that these things actually

53
00:05:56,520 --> 00:06:02,160
are not the path and it's either through some simple instruction by the teacher or through

54
00:06:02,160 --> 00:06:06,960
their own reflection and their own experimentation, realizing that these things don't

55
00:06:06,960 --> 00:06:07,960
lead to anything.

56
00:06:07,960 --> 00:06:13,040
As I said, sometimes all it takes is one word of advice and then the person is able

57
00:06:13,040 --> 00:06:22,920
to overcome days, weeks, years of being stuck at this stage simply by explaining to them

58
00:06:22,920 --> 00:06:27,800
that this is a Vipasa Nupikinis, it's something that shouldn't be clung to and it's halting

59
00:06:27,800 --> 00:06:32,960
their progress, not because of it intrinsically, but because they're not continuing to

60
00:06:32,960 --> 00:06:44,480
practice, so that's the 10 Vipasa Nupikinis.

61
00:06:44,480 --> 00:07:10,560
Thank you.

