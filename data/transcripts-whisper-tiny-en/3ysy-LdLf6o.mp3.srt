1
00:00:00,000 --> 00:00:05,000
What is your recommendation for dealing with an angry person lashing out at you?

2
00:00:05,000 --> 00:00:12,000
Do you ignore them and go straight to paying attention to the rising and falling?

3
00:00:12,000 --> 00:00:15,000
Yeah, no, never do that.

4
00:00:15,000 --> 00:00:22,000
Rising and falling, there's nothing special about the rising and falling of the abdomen.

5
00:00:22,000 --> 00:00:29,000
There's one thing special, it's quite easy and coarse and obvious.

6
00:00:29,000 --> 00:00:33,000
So it's child's plane.

7
00:00:33,000 --> 00:00:39,000
And so it is, I remember when I first started, and I did this exact same thing.

8
00:00:39,000 --> 00:00:43,000
This exact thing when people were yelling at me and I didn't know what to do.

9
00:00:43,000 --> 00:00:46,000
I just closed my eyes and went back to the rising and falling.

10
00:00:46,000 --> 00:01:01,000
It doesn't really work because you've got this intense headache and tension and stress because this person is making you all upset.

11
00:01:01,000 --> 00:01:08,000
So you're not actually cultivating objectivity, you're not actually meditating.

12
00:01:08,000 --> 00:01:11,000
You should never ignore anything.

13
00:01:11,000 --> 00:01:14,000
The point is, don't ignore the present moment.

14
00:01:14,000 --> 00:01:19,000
The present moment at that moment is this person lashing out at you.

15
00:01:19,000 --> 00:01:26,000
So you can close your eyes, you can keep your eyes open, but what you should be focusing is on where your mind is.

16
00:01:26,000 --> 00:01:33,000
If your mind is not interested, if you're so highly developed that you're not interested, all this person is lashing out at me,

17
00:01:33,000 --> 00:01:37,000
okay, well back to my meditation, then absolutely that's fine.

18
00:01:37,000 --> 00:01:41,000
If it doesn't bother you, your mind doesn't even go out there.

19
00:01:41,000 --> 00:01:47,000
You know the person's there, you say yourself hearing, hearing, and then just ignore them and go back to your rising and falling.

20
00:01:47,000 --> 00:01:49,000
It's just like birds chirping and fine.

21
00:01:49,000 --> 00:02:01,000
But if it upsets you, or if you're at all interested in it, and it's highly unlikely that you're able to really put this out of your mind unless you are highly developed.

22
00:02:01,000 --> 00:02:10,000
So your mind, even if it's not upset, it's going to be going back to focusing on what this person is saying, all the nasty things they're saying about you.

23
00:02:10,000 --> 00:02:15,000
In which case that's what you should be focusing on saying to yourself, hearing, hearing.

24
00:02:15,000 --> 00:02:21,000
And once your mind does let go of it, if at times your mind is no longer interested in it, or the person stops yelling,

25
00:02:21,000 --> 00:02:24,000
you can just go back to your rising and falling.

26
00:02:24,000 --> 00:02:30,000
You should also be noting the emotions, if it does give rise to emotional turmoil, like you're upset or angry,

27
00:02:30,000 --> 00:02:35,000
or if you find it funny, or if you feel somehow egotistical.

28
00:02:35,000 --> 00:02:40,000
Often we feel proud of ourselves that we're able to not argue back, and you should watch that as well.

29
00:02:40,000 --> 00:02:44,000
Not think you're better than the person because they're lashing out.

30
00:02:44,000 --> 00:02:55,000
All this person is such a low spiritual, low spirituality, such a low being.

31
00:02:55,000 --> 00:03:00,000
They can't even control their temper, that kind of thing.

32
00:03:00,000 --> 00:03:01,000
You should watch that as well.

33
00:03:01,000 --> 00:03:26,000
You should watch all of these things and be mindful and replace the judgements with objectivity and mindfulness.

