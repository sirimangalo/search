1
00:00:00,000 --> 00:00:07,000
Okay, we are live.

2
00:00:07,000 --> 00:00:13,000
Just make sure we're live correctly.

3
00:00:13,000 --> 00:00:14,000
Test.

4
00:00:14,000 --> 00:00:15,000
Okay.

5
00:00:15,000 --> 00:00:18,000
Well, you can hear me in the way.

6
00:00:18,000 --> 00:00:27,000
So today we're going to do another dumbapada.

7
00:00:27,000 --> 00:00:31,000
Let's get this all set up properly.

8
00:00:31,000 --> 00:00:35,000
One day, did you want to have the earbuds in during the dumbapada?

9
00:00:35,000 --> 00:00:42,000
No.

10
00:00:42,000 --> 00:00:48,000
Say that's why I need a team.

11
00:00:48,000 --> 00:00:56,000
Okay, I think we're ready to go.

12
00:00:56,000 --> 00:01:01,000
Hello and welcome back to our study of the dumbapada.

13
00:01:01,000 --> 00:01:07,000
Today we continue on with first number 107, which reads as follows.

14
00:01:07,000 --> 00:01:35,000
And one might in a being, you know,

15
00:01:35,000 --> 00:01:40,000
and whatever being might,

16
00:01:40,000 --> 00:01:45,000
a ging-bari-kaa-jari when they live in the forest,

17
00:01:45,000 --> 00:01:47,000
tending to a fire.

18
00:01:47,000 --> 00:01:53,000
So it may live in a forest for a hundred years.

19
00:01:53,000 --> 00:01:58,000
A kang-ja-bawi-tatana misses the same as yesterday's first.

20
00:01:58,000 --> 00:02:04,000
Muhu-tam-poo-jee-fun should pay homage to one of the developed themselves.

21
00:02:04,000 --> 00:02:07,000
Just for one moment.

22
00:02:07,000 --> 00:02:10,000
Sayyewa-poo-jana-sayu.

23
00:02:10,000 --> 00:02:13,000
This poo-jah is greater.

24
00:02:13,000 --> 00:02:19,000
Greater than the one who for a hundred years practices,

25
00:02:19,000 --> 00:02:23,000
this sacrifice, fire sacrifice.

26
00:02:23,000 --> 00:02:32,000
Now, not a terribly meaningful verse for those of us who aren't familiar

27
00:02:32,000 --> 00:02:35,000
with living in the forest, tending to a fire.

28
00:02:35,000 --> 00:02:38,000
But we can generalize and talk about it.

29
00:02:38,000 --> 00:02:40,000
The story as well is quite similar to yesterday.

30
00:02:40,000 --> 00:02:42,000
It's almost identical.

31
00:02:42,000 --> 00:02:46,000
The difference here is the, sorry, put down instead of going to talk to his uncle.

32
00:02:46,000 --> 00:02:51,000
He goes and talks to his nephew.

33
00:02:51,000 --> 00:02:54,000
And he asks his nephew, so nephew.

34
00:02:54,000 --> 00:02:58,000
His nephew is also a Brahman, his whole family, of course, was Brahman.

35
00:02:58,000 --> 00:03:03,000
He says, what sort of King Brahmana-kusulankarosi

36
00:03:03,000 --> 00:03:07,000
would sort of wholesomeness or good deeds do you do?

37
00:03:07,000 --> 00:03:10,000
And so he says, oh, every month.

38
00:03:10,000 --> 00:03:13,000
Ma-se-ma-se-a-kani-kung-pasun.

39
00:03:13,000 --> 00:03:21,000
Every month, I have been having killed a passu livestock.

40
00:03:21,000 --> 00:03:27,000
Like a pet of some sort, some kind of domesticated animal or some animal.

41
00:03:27,000 --> 00:03:31,000
A sheep may be a goat, probably.

42
00:03:31,000 --> 00:03:34,000
A ging-bri-bri-tarami.

43
00:03:34,000 --> 00:03:39,000
I use it to tend to a fire or burn it in a fire.

44
00:03:39,000 --> 00:03:42,000
Maybe make a burnt offering.

45
00:03:42,000 --> 00:03:46,000
It sounds like he just burns it in a fire.

46
00:03:46,000 --> 00:03:50,000
Kind of an Abrahamic sort of practice.

47
00:03:50,000 --> 00:03:55,000
It's what the Jewish people would do.

48
00:03:55,000 --> 00:03:57,000
It's very similar.

49
00:03:57,000 --> 00:04:05,000
But fire, sacrifice seems to have originally been burning goats and so on.

50
00:04:05,000 --> 00:04:12,000
All over the world, somehow this was a big thing to do.

51
00:04:12,000 --> 00:04:22,000
And then he says, for what purpose do you do that?

52
00:04:22,000 --> 00:04:25,000
And he says, oh, same as before.

53
00:04:25,000 --> 00:04:30,000
Oh, for the Brahmalokamago kiri.

54
00:04:30,000 --> 00:04:37,000
So I have heard or it is said that this is the path to the Brahmaloka.

55
00:04:37,000 --> 00:04:40,000
And sorry, but it says, who told you that?

56
00:04:40,000 --> 00:04:41,000
Same as yesterday.

57
00:04:41,000 --> 00:04:43,000
Same as last time.

58
00:04:43,000 --> 00:04:46,000
And again, it was my teachers.

59
00:04:46,000 --> 00:04:48,000
But the first one, it was the last one.

60
00:04:48,000 --> 00:04:52,000
I see to kind of get a sense that his teachers were these naked ascetics.

61
00:04:52,000 --> 00:04:58,000
Last time, Randy was giving every month, giving a thousand pieces of money worth of food or whatever.

62
00:04:58,000 --> 00:05:04,000
Maybe even just money directly to these naked ascetics.

63
00:05:04,000 --> 00:05:08,000
And so one would think that they were probably his teachers who were teaching in this.

64
00:05:08,000 --> 00:05:13,000
Now, in this case, the question is, why are his teachers teaching such things?

65
00:05:13,000 --> 00:05:20,000
And there's various reasons, and we can extrapolate this to sort of generalize in terms of religion.

66
00:05:20,000 --> 00:05:23,000
Why do people teach such ridiculous things?

67
00:05:23,000 --> 00:05:27,000
I mean, it's understandable why people believe them.

68
00:05:27,000 --> 00:05:32,000
We just tend to believe things based on tradition or based on the authority of our teachers,

69
00:05:32,000 --> 00:05:37,000
even though we ourselves have no sense of the causal relationship.

70
00:05:37,000 --> 00:05:43,000
So this is why religions do such wide array of practices,

71
00:05:43,000 --> 00:05:46,000
because we're just told to by our priests or by our leaders.

72
00:05:46,000 --> 00:05:52,000
The question is, why do the leaders teach such things?

73
00:05:52,000 --> 00:05:55,000
Sometimes it's just for gain.

74
00:05:55,000 --> 00:05:57,000
I mean, I've seen it even in Buddhism.

75
00:05:57,000 --> 00:06:05,000
People come to the monasteries, the temples, and they ask the teacher,

76
00:06:05,000 --> 00:06:08,000
what should I do? Someone passed away, what should I do?

77
00:06:08,000 --> 00:06:11,000
And I've seen teachers, I've seen monks.

78
00:06:11,000 --> 00:06:14,000
You can just see they're the wheels turning in their head,

79
00:06:14,000 --> 00:06:19,000
kind of in a bit of panic, like, oh, scrambling to find something

80
00:06:19,000 --> 00:06:23,000
that looks or sounds kind of mystical.

81
00:06:23,000 --> 00:06:28,000
It really comes down to that, just making something up on the spur of the moment

82
00:06:28,000 --> 00:06:34,000
to appease people's desire for some ritual, something to solve their problems.

83
00:06:34,000 --> 00:06:48,000
Sri Damika, a very wonderful sort of populist Buddhist monk and teacher from Sri Lanka.

84
00:06:48,000 --> 00:06:52,000
He said, someone asked him about these emulates that monks give out.

85
00:06:52,000 --> 00:06:56,000
And he said, well, they have absolutely no meaning, but he said sometimes,

86
00:06:56,000 --> 00:07:02,000
sometimes for people who are new or who don't really have a good understanding of cause and effect,

87
00:07:02,000 --> 00:07:05,000
you have to give them something, so he took a path.

88
00:07:05,000 --> 00:07:10,000
He says, you see this? I want you to keep it, hold onto it, and it will keep you safe.

89
00:07:10,000 --> 00:07:12,000
He says, sometimes you have to do that.

90
00:07:12,000 --> 00:07:17,000
It's sort of like a dumbo in his feather, if you ever saw the Disney movie.

91
00:07:17,000 --> 00:07:21,000
Sometimes people need their magical feather.

92
00:07:21,000 --> 00:07:29,000
So there is a defense of it, but I don't think it's a very strong defense,

93
00:07:29,000 --> 00:07:35,000
especially when it becomes one's main, absolutely no defense,

94
00:07:35,000 --> 00:07:38,000
when it becomes one's main religious practice.

95
00:07:38,000 --> 00:07:45,000
And not only is there no defense, but it's a blame-worthy

96
00:07:45,000 --> 00:07:49,000
when it involves unholesome, clearly unholesome acts.

97
00:07:49,000 --> 00:07:55,000
It's one thing to tell people that offering food to a statue

98
00:07:55,000 --> 00:08:01,000
is going to be to their benefit or pouring water on the root of a tree

99
00:08:01,000 --> 00:08:03,000
is going to do this or do that.

100
00:08:03,000 --> 00:08:05,000
I mean, that's kind of innocent.

101
00:08:05,000 --> 00:08:11,000
And who knows, maybe the angels will get involved and help out with it,

102
00:08:11,000 --> 00:08:15,000
if they know there's this interesting thing.

103
00:08:15,000 --> 00:08:21,000
Buddhists in Sri Lanka are very keen to pour water

104
00:08:21,000 --> 00:08:25,000
at the roots of the Bodhi tree, so the pour water,

105
00:08:25,000 --> 00:08:29,000
to water the Bodhi tree that go around it, it's very important ceremony.

106
00:08:29,000 --> 00:08:34,000
And they do this with the understanding that it leads to pregnancy.

107
00:08:34,000 --> 00:08:37,000
And it apparently has some measure of success.

108
00:08:37,000 --> 00:08:41,000
I mean, it's anecdotal and scientists may be able to study it

109
00:08:41,000 --> 00:08:45,000
and find that in fact, there is no correlation.

110
00:08:45,000 --> 00:08:49,000
But it's an interesting idea that there might be some correlation

111
00:08:49,000 --> 00:08:52,000
because how would that relate to the Buddhist teaching?

112
00:08:52,000 --> 00:08:54,000
It's certainly not a Buddhist teaching.

113
00:08:54,000 --> 00:08:56,000
That's such a thing as possible.

114
00:08:56,000 --> 00:08:58,000
And yet we have in the Dhammapada stories.

115
00:08:58,000 --> 00:09:00,000
Remember the first story that we did.

116
00:09:00,000 --> 00:09:03,000
I don't know if I actually brought that and brought up the whole backstory.

117
00:09:03,000 --> 00:09:05,000
But this guy does basically that.

118
00:09:05,000 --> 00:09:12,000
He goes to a tree and he cleans up around this great tree in the forest

119
00:09:12,000 --> 00:09:17,000
and puts up banners and flags and a wall around it to protect it.

120
00:09:17,000 --> 00:09:21,000
And then makes a promise to the tree that if he gets a son or a daughter,

121
00:09:21,000 --> 00:09:25,000
he'll come back and do pay great homage and respect to the tree.

122
00:09:25,000 --> 00:09:31,000
And sure enough, his wife gives birthright quite quickly after that.

123
00:09:31,000 --> 00:09:34,000
So people from Sri Lanka have told me that this actually works.

124
00:09:34,000 --> 00:09:38,000
And I was trying to figure out exactly how it might work in reality

125
00:09:38,000 --> 00:09:40,000
because there has to be a causal relationship.

126
00:09:40,000 --> 00:09:43,000
If it can't just be magic, there's no such thing.

127
00:09:43,000 --> 00:09:49,000
And it's not really accepted in Buddhism.

128
00:09:49,000 --> 00:09:53,000
So the idea that somehow you could do some ritual and that then it could work.

129
00:09:53,000 --> 00:09:57,000
The only way it could work, and I'm thinking it actually could,

130
00:09:57,000 --> 00:09:59,000
is if the angels got involved,

131
00:09:59,000 --> 00:10:05,000
saying because there's got to be something to do with angels hanging out at the Bodhi tree.

132
00:10:05,000 --> 00:10:12,000
And if the lay people come to the Bodhi tree and they do this

133
00:10:12,000 --> 00:10:16,000
and they make a sincere wish, and they have a sincerely good heart,

134
00:10:16,000 --> 00:10:19,000
it's kind of like the angels can look down and say,

135
00:10:19,000 --> 00:10:20,000
oh, those are nice people.

136
00:10:20,000 --> 00:10:22,000
Well, my lifespan is almost up.

137
00:10:22,000 --> 00:10:26,000
I think maybe they even hang out looking for parents.

138
00:10:26,000 --> 00:10:29,000
And when the angels know that their lifespan is almost up,

139
00:10:29,000 --> 00:10:33,000
they try to find suitable parents to go and be reborn.

140
00:10:33,000 --> 00:10:34,000
I mean, something like that.

141
00:10:34,000 --> 00:10:37,000
Somehow the angels, the day was get involved.

142
00:10:37,000 --> 00:10:42,000
But I give that only as an example of how there might be some causal relationship,

143
00:10:42,000 --> 00:10:46,000
but there has to be something like that, or else it's just ridiculous.

144
00:10:46,000 --> 00:10:49,000
And so for the most part it is, especially,

145
00:10:49,000 --> 00:10:52,000
it's beyond ridiculous when it comes down to killing animals,

146
00:10:52,000 --> 00:10:56,000
killing your fellow living beings, and saying that somehow the past,

147
00:10:56,000 --> 00:10:57,000
the Brahma world.

148
00:10:57,000 --> 00:11:00,000
So Sariputur rightly says to him,

149
00:11:00,000 --> 00:11:04,000
he says, who taught you your teachers and he says your teachers haven't looked through.

150
00:11:04,000 --> 00:11:09,000
And so the reason why the teachers might have been teaching it may have just been,

151
00:11:09,000 --> 00:11:14,000
because you see, if you sound like you know what you're doing,

152
00:11:14,000 --> 00:11:17,000
and if you set up all these complex rituals, people think,

153
00:11:17,000 --> 00:11:20,000
oh, well, this person we need him,

154
00:11:20,000 --> 00:11:23,000
because he leads us in these very important rituals,

155
00:11:23,000 --> 00:11:27,000
and only he knows the right rituals when in fact they just make them up.

156
00:11:27,000 --> 00:11:30,000
And so I think that's what you get.

157
00:11:30,000 --> 00:11:33,000
I'm pretty sure that's what you get in a lot of the...

158
00:11:33,000 --> 00:11:38,000
the rituals at the time of the Buddha, and even after the Buddha.

159
00:11:38,000 --> 00:11:42,000
But definitely very much before the Buddha came around.

160
00:11:42,000 --> 00:11:47,000
Like we've studied these in university when I was taking Indian religion many years ago.

161
00:11:47,000 --> 00:11:52,000
And some of the rituals are just silly, like they take...

162
00:11:52,000 --> 00:11:55,000
It seems like, originally they were horrific.

163
00:11:55,000 --> 00:11:58,000
These horrific sacrifices a lot of killing.

164
00:11:58,000 --> 00:12:03,000
And so they would take a goat up to the altar and cut its head off,

165
00:12:03,000 --> 00:12:06,000
and there was something about burying a life turtle under the altar,

166
00:12:06,000 --> 00:12:11,000
and just burying it alive and killing it and suffocating it under the altar.

167
00:12:11,000 --> 00:12:17,000
I mean, for no reason, I mean, why would you bury a life turtle under a stone altar?

168
00:12:17,000 --> 00:12:19,000
But it was an important part of building the altar,

169
00:12:19,000 --> 00:12:25,000
and then there was butter that had to be poured into the fire and that kind of thing.

170
00:12:25,000 --> 00:12:30,000
And it evolved, so that eventually was probably with the advent of Buddhism,

171
00:12:30,000 --> 00:12:36,000
Jainism, and a lot of the movements that were anti-torture, or anti-cruelty.

172
00:12:36,000 --> 00:12:41,000
Eventually, by the time we came to the form that we study it today,

173
00:12:41,000 --> 00:12:44,000
what they do is they bring a goat to the altar,

174
00:12:44,000 --> 00:12:47,000
and they have to show the altar to the goat.

175
00:12:47,000 --> 00:12:50,000
Like the goat has to see the altar, and then they take the goat away.

176
00:12:50,000 --> 00:12:55,000
There's a sacrificial pole, there's a special wooden pole that has to be there as well,

177
00:12:55,000 --> 00:12:59,000
and they have to bring the goat and show the goat that pole.

178
00:12:59,000 --> 00:13:02,000
So as long as the goat sees the pole, they've done enough,

179
00:13:02,000 --> 00:13:04,000
and they take the goat away.

180
00:13:04,000 --> 00:13:07,000
And you can imagine some guys saying, oh, well, we can't kill them anymore.

181
00:13:07,000 --> 00:13:11,000
Well, let's just tell everyone, oh, it's enough that the goat sees the pole.

182
00:13:11,000 --> 00:13:14,000
I mean, it's something that they just come up with,

183
00:13:14,000 --> 00:13:18,000
say, oh, no, no, people say, well, we don't really want to kill the goat.

184
00:13:18,000 --> 00:13:23,000
Oh, well, it's okay as long as he sees the post, somehow that's meaningful.

185
00:13:23,000 --> 00:13:25,000
It's no longer about killing.

186
00:13:25,000 --> 00:13:31,000
So, I mean, good on them that their ridiculous sacrifices have become innocent.

187
00:13:31,000 --> 00:13:34,000
That's one step in the right direction.

188
00:13:34,000 --> 00:13:37,000
The other thing is they, instead of burying a turtle under the altar,

189
00:13:37,000 --> 00:13:40,000
they take a lump of butter, and they bury it under the altar.

190
00:13:40,000 --> 00:13:43,000
It's a substitute for a live turtle.

191
00:13:43,000 --> 00:13:47,000
It's a good thing for all the poor little turtles.

192
00:13:47,000 --> 00:13:53,000
But definitely, innocent doesn't mean beneficial worthwhile.

193
00:13:53,000 --> 00:13:58,000
And in this guy's case, he's got real problems with his killing,

194
00:13:58,000 --> 00:14:02,000
every month killing a living being and feeding it to the fire.

195
00:14:02,000 --> 00:14:04,000
Like if it was just butter, he was feeding to the fire,

196
00:14:04,000 --> 00:14:07,000
then he could say, well, that's kind of innocent.

197
00:14:07,000 --> 00:14:09,000
But even still, it's not the way to the Brahma world.

198
00:14:09,000 --> 00:14:14,000
It doesn't have any connection with being reborn as a Brahma, as a God.

199
00:14:14,000 --> 00:14:20,000
So, it takes him to see the Buddha, and the Buddha,

200
00:14:20,000 --> 00:14:24,000
he tells the Buddha what he does, and the Buddha says,

201
00:14:24,000 --> 00:14:28,000
you can do that for a hundred years, every month.

202
00:14:28,000 --> 00:14:33,000
And it wouldn't be worth the thousandth part,

203
00:14:33,000 --> 00:14:40,000
if he were to just pay respect to an enlightened being for one moment.

204
00:14:40,000 --> 00:14:43,000
So again, talking about homage.

205
00:14:43,000 --> 00:14:48,000
And the Buddha specifies my students.

206
00:14:48,000 --> 00:14:51,000
So it is a bold claim.

207
00:14:51,000 --> 00:14:56,000
And as I said last time, it seems like a bit of a bias claim at first blush,

208
00:14:56,000 --> 00:14:58,000
because anyone could say that.

209
00:14:58,000 --> 00:15:01,000
Well, of course, everyone's going to say their own students.

210
00:15:01,000 --> 00:15:04,000
But it has to be true at some point.

211
00:15:04,000 --> 00:15:06,000
If your students are enlightened,

212
00:15:06,000 --> 00:15:09,000
and if your people really are,

213
00:15:09,000 --> 00:15:12,000
if you're talking about a group of people who really are enlightened,

214
00:15:12,000 --> 00:15:17,000
then the Buddha wasn't afraid of making these bold and sort of bragging claims,

215
00:15:17,000 --> 00:15:20,000
boastful claims.

216
00:15:20,000 --> 00:15:23,000
Because if you look at it another way, of course,

217
00:15:23,000 --> 00:15:26,000
it's leaving you wide open to attack.

218
00:15:26,000 --> 00:15:29,000
And the Buddha fully welcomed what he's saying,

219
00:15:29,000 --> 00:15:31,000
he's really, it's called the lion's roar.

220
00:15:31,000 --> 00:15:34,000
He was really putting it out there.

221
00:15:34,000 --> 00:15:38,000
He's making a claim, a boast, and a challenge.

222
00:15:38,000 --> 00:15:43,000
Prove me wrong.

223
00:15:43,000 --> 00:15:47,000
It's claiming something very, very boldly.

224
00:15:47,000 --> 00:15:51,000
And that's really what the purpose and the meaning there is.

225
00:15:51,000 --> 00:15:55,000
That's not the Buddha bragging or forget gain of any sort.

226
00:15:55,000 --> 00:15:58,000
There's no sense that the Buddha even had any need.

227
00:15:58,000 --> 00:16:00,000
Even if you don't follow Buddhism,

228
00:16:00,000 --> 00:16:05,000
there was no real evidence to support that.

229
00:16:05,000 --> 00:16:12,000
And he was well taken care of and well supported.

230
00:16:12,000 --> 00:16:17,000
But what he's doing is instead making just this bold claim

231
00:16:17,000 --> 00:16:20,000
that could then be challenged.

232
00:16:20,000 --> 00:16:23,000
And of course, the Brahmin would have to be impressed by that

233
00:16:23,000 --> 00:16:25,000
because he couldn't challenge it.

234
00:16:25,000 --> 00:16:28,000
And looking at the Buddhist monks, he would have to agree that,

235
00:16:28,000 --> 00:16:32,000
well, yes, indeed, there's something special about many of these

236
00:16:32,000 --> 00:16:34,000
monks who have developed themselves.

237
00:16:34,000 --> 00:16:36,000
And so that's what the verse actually says.

238
00:16:36,000 --> 00:16:39,000
But we've got that to one, if you pay homage to one,

239
00:16:39,000 --> 00:16:44,000
or to those who are of developed self.

240
00:16:44,000 --> 00:16:47,000
So again, not to get too much into it.

241
00:16:47,000 --> 00:16:49,000
I think the more important aspect of this,

242
00:16:49,000 --> 00:16:51,000
that differentiates it from the last one,

243
00:16:51,000 --> 00:16:54,000
is talking about sacrifice, which I've done.

244
00:16:54,000 --> 00:16:58,000
But I think we can extrapolate that to talk about religious

245
00:16:58,000 --> 00:17:01,000
practices in general.

246
00:17:01,000 --> 00:17:03,000
It's not enough that we have a religious practice.

247
00:17:03,000 --> 00:17:06,000
And I think there's a lot of this in the world.

248
00:17:06,000 --> 00:17:08,000
Just because something is a spiritual practice or a religious

249
00:17:08,000 --> 00:17:12,000
practice doesn't make it really all that valuable.

250
00:17:12,000 --> 00:17:15,000
And certainly an understanding that the different

251
00:17:15,000 --> 00:17:18,000
spiritual practices have different values.

252
00:17:18,000 --> 00:17:22,000
So it's not to say that either you're practicing to become

253
00:17:22,000 --> 00:17:25,000
enlightened or you're doing useless things.

254
00:17:25,000 --> 00:17:28,000
There are certain religious practices that are valuable,

255
00:17:28,000 --> 00:17:30,000
but just not as valuable.

256
00:17:30,000 --> 00:17:34,000
So feeding of sacrificial fire is pretty useless.

257
00:17:34,000 --> 00:17:36,000
But practicing loving kindness, for example,

258
00:17:36,000 --> 00:17:37,000
is quite valuable.

259
00:17:37,000 --> 00:17:42,000
Practicing charity, giving money to the poor is valuable.

260
00:17:42,000 --> 00:17:45,000
It's less valuable than, well, for example,

261
00:17:45,000 --> 00:17:48,000
paying homage to one who deserves it.

262
00:17:48,000 --> 00:17:53,000
But even paying homage to one who is worthy of homage,

263
00:17:53,000 --> 00:17:58,000
is far less valuable than actually becoming worthy of respect

264
00:17:58,000 --> 00:18:00,000
to yourself.

265
00:18:00,000 --> 00:18:03,000
My teacher said that he said, going to see an enlightened

266
00:18:03,000 --> 00:18:07,000
being, or paying homage to an enlightened being,

267
00:18:07,000 --> 00:18:11,000
is not worth the smallest part of becoming practicing to become

268
00:18:11,000 --> 00:18:13,000
an enlightened being yourself.

269
00:18:13,000 --> 00:18:17,000
It's in a very famous talk that he gave on the force at

270
00:18:17,000 --> 00:18:20,000
Deepatana.

271
00:18:20,000 --> 00:18:25,000
But yeah, he made that very important statement.

272
00:18:25,000 --> 00:18:30,000
Yeah, it's good, but it's not worth the smallest part.

273
00:18:30,000 --> 00:18:36,000
It's far less good than actually practicing to become one

274
00:18:36,000 --> 00:18:37,000
yourself.

275
00:18:37,000 --> 00:18:39,000
So it's a matter of degrees.

276
00:18:39,000 --> 00:18:42,000
And that's something for us to keep in mind in our

277
00:18:42,000 --> 00:18:46,000
spiritual practice, that some people will put a lot of

278
00:18:46,000 --> 00:18:49,000
emphasis on chanting, some people put a lot of emphasis

279
00:18:49,000 --> 00:18:51,000
on charity or social work.

280
00:18:51,000 --> 00:18:54,000
And it's all about our priorities.

281
00:18:54,000 --> 00:18:57,000
Some people put a lot of emphasis on study.

282
00:18:57,000 --> 00:18:59,000
And in the end, we have to ask ourselves,

283
00:18:59,000 --> 00:19:00,000
what is our goal?

284
00:19:00,000 --> 00:19:03,000
And we have to do those things to engage,

285
00:19:03,000 --> 00:19:08,000
put primer emphasis on those things that lead us to that goal.

286
00:19:08,000 --> 00:19:11,000
For just giving and giving and giving, what is the purpose of

287
00:19:11,000 --> 00:19:11,000
that?

288
00:19:11,000 --> 00:19:15,000
If we're just chanting and chanting or this or that,

289
00:19:15,000 --> 00:19:18,000
what is the purpose of all these things?

290
00:19:18,000 --> 00:19:21,000
And on the other hand, if we do have a set purpose,

291
00:19:21,000 --> 00:19:27,000
and we do things for that purpose,

292
00:19:27,000 --> 00:19:29,000
we have to differentiate between those things that actually

293
00:19:29,000 --> 00:19:32,000
lead to the purpose.

294
00:19:32,000 --> 00:19:35,000
But then there are wide array of things that can help us,

295
00:19:35,000 --> 00:19:39,000
that can actually be a benefit to help us realize our goals.

296
00:19:39,000 --> 00:19:41,000
Charity can be useful for meditation.

297
00:19:41,000 --> 00:19:45,000
Morality, of course, is essential for meditation.

298
00:19:45,000 --> 00:19:49,000
These kind of things study is also quite important.

299
00:19:49,000 --> 00:19:52,000
But putting them in context.

300
00:19:52,000 --> 00:19:57,000
So many religious practices have to be taken as a support

301
00:19:57,000 --> 00:20:00,000
rather than a main goal or a main practice,

302
00:20:00,000 --> 00:20:02,000
a main focus.

303
00:20:02,000 --> 00:20:04,000
Anyway, things to consider.

304
00:20:04,000 --> 00:20:10,000
It's just some of the ideas that arise based on this verse.

305
00:20:10,000 --> 00:20:13,000
And the importance of quality rather than quantity.

306
00:20:13,000 --> 00:20:15,000
You can do a lot, a lot of deeds,

307
00:20:15,000 --> 00:20:18,000
but it doesn't make any of them good or worthwhile.

308
00:20:18,000 --> 00:20:22,000
You can work for 100 years, work very, very hard,

309
00:20:22,000 --> 00:20:24,000
and have nothing to show for it.

310
00:20:24,000 --> 00:20:27,000
Or meaningless things to show for it.

311
00:20:27,000 --> 00:20:30,000
Whereas you can just, for one moment, do the right thing

312
00:20:30,000 --> 00:20:34,000
and have it worse, far more than those 100 years.

313
00:20:34,000 --> 00:20:38,000
That's basically what space I know very useful teaching.

314
00:20:38,000 --> 00:20:40,000
Anyway, so that's the demo fata for tonight.

315
00:20:40,000 --> 00:20:50,000
Thank you for tuning in, wishing you all the best.

316
00:20:50,000 --> 00:20:55,000
So that's it.

317
00:20:55,000 --> 00:20:57,000
Just give me a second.

318
00:20:57,000 --> 00:21:01,000
I can have it because it has to convert.

319
00:21:01,000 --> 00:21:14,000
There's my production crew.

320
00:21:14,000 --> 00:21:41,000
So on to the demo portion, the general demo portion of the night.

321
00:21:41,000 --> 00:21:44,000
Does anyone have any questions?

322
00:21:44,000 --> 00:21:46,000
There are questions.

323
00:21:46,000 --> 00:21:49,000
Dear Bande, there is only one thousand.

324
00:21:49,000 --> 00:21:56,000
Sorry, just give me a second.

325
00:21:56,000 --> 00:22:00,000
Turn some things off.

326
00:22:00,000 --> 00:22:04,000
Does that have that test, doesn't it?

327
00:22:04,000 --> 00:22:20,000
And now one more thing.

328
00:22:20,000 --> 00:22:42,000
There's only one demo group near where I live.

329
00:22:42,000 --> 00:22:45,000
I've meditated with the group maybe five times now.

330
00:22:45,000 --> 00:22:47,000
I like the people there, but I'm not sure

331
00:22:47,000 --> 00:22:50,000
if you're going there has been good for my practice.

332
00:22:50,000 --> 00:22:53,000
My original hope was that there would be a few people on the

333
00:22:53,000 --> 00:22:57,000
similar path as I've been set on by your videos and Mahasi books.

334
00:22:57,000 --> 00:23:00,000
I don't want to complain or anything.

335
00:23:00,000 --> 00:23:03,000
But unfortunately, it's become obvious that all the people in the group

336
00:23:03,000 --> 00:23:07,000
either have only mindfulness-based stress reduction goal or just the

337
00:23:07,000 --> 00:23:10,000
simple goal of cultivating calmness.

338
00:23:10,000 --> 00:23:13,000
Obviously, I'm not saying there's anything wrong with all those things,

339
00:23:13,000 --> 00:23:17,000
but I think the practice can go so much deeper than that as you often talk about.

340
00:23:17,000 --> 00:23:21,000
I feel like the few times I've opened up a little bit about my understanding of

341
00:23:21,000 --> 00:23:26,000
insect meditation, which is the understanding that I've cultivated from your videos.

342
00:23:26,000 --> 00:23:31,000
Everybody has found the things I say really intimidating and contrary to what

343
00:23:31,000 --> 00:23:34,000
they would like to think the practice is about.

344
00:23:34,000 --> 00:23:38,000
But as I said, I really like the people and kind of get intoxicated by their company,

345
00:23:38,000 --> 00:23:41,000
which in a way pulls me away from the practice.

346
00:23:41,000 --> 00:23:43,000
Should I keep going there?

347
00:23:43,000 --> 00:23:46,000
And the next time I go, what would be the best things to say,

348
00:23:46,000 --> 00:23:49,000
talk about to the group, or should I just stay quiet?

349
00:23:49,000 --> 00:23:52,000
Sorry, Robin, for making you read such a long question.

350
00:23:52,000 --> 00:23:54,000
Best wishes.

351
00:23:56,000 --> 00:23:59,000
It's a good question though.

352
00:23:59,000 --> 00:24:05,000
I mean, this from the sounds of it, I think you may be a little bit over-reacting.

353
00:24:05,000 --> 00:24:07,000
I mean, it's just impersonal.

354
00:24:07,000 --> 00:24:11,000
I don't actually know, because based on what you're saying,

355
00:24:11,000 --> 00:24:14,000
you could be over-reacting, you could be under-reacting.

356
00:24:14,000 --> 00:24:18,000
You could be, you know, they could actually be lynching you every time you say it,

357
00:24:18,000 --> 00:24:21,000
but you're just playing it down.

358
00:24:21,000 --> 00:24:26,000
But most likely, I mean, in most cases, we get two gung-ho and maybe

359
00:24:26,000 --> 00:24:30,000
push a little too far and talk a little bit, a little bit,

360
00:24:30,000 --> 00:24:36,000
too much stress on things that are scary like Nirvana and non-attachment

361
00:24:36,000 --> 00:24:38,000
and that kind of thing.

362
00:24:38,000 --> 00:24:41,000
But you don't really have to do, because really,

363
00:24:41,000 --> 00:24:44,000
that it's all about reducing stress.

364
00:24:44,000 --> 00:24:48,000
And it's just a corollary that if you reduce it enough,

365
00:24:48,000 --> 00:24:50,000
it eventually all goes away.

366
00:24:50,000 --> 00:24:53,000
You see, I mean, it's all about how you approach it

367
00:24:53,000 --> 00:25:02,000
and how you present it.

368
00:25:02,000 --> 00:25:09,000
Now, it doesn't seem to, so it doesn't seem to me to be a valid problem

369
00:25:09,000 --> 00:25:12,000
if people are just interested in reducing stress.

370
00:25:12,000 --> 00:25:19,000
All you have to do is be clear about how deep that goes

371
00:25:19,000 --> 00:25:23,000
and help people to see that, well, you know, if your goal is to do stress,

372
00:25:23,000 --> 00:25:26,000
well, look at that stress that you still have.

373
00:25:26,000 --> 00:25:29,000
That's the sign that you're really not going quite deep enough.

374
00:25:29,000 --> 00:25:33,000
So, hey, maybe this practice will help you go deeper,

375
00:25:33,000 --> 00:25:36,000
but even still, you don't really do that.

376
00:25:36,000 --> 00:25:38,000
I mean, I never really would point out to someone,

377
00:25:38,000 --> 00:25:40,000
hey, look, your practice is inadequate.

378
00:25:40,000 --> 00:25:43,000
It would more be like if they come to you and say, you know,

379
00:25:43,000 --> 00:25:46,000
I'm not sure why, but my practice isn't.

380
00:25:46,000 --> 00:25:48,000
I'm not able to get deeper in my practice.

381
00:25:48,000 --> 00:25:51,000
Then I would say, well, maybe you could try this practice,

382
00:25:51,000 --> 00:25:53,000
maybe it works better for you.

383
00:25:53,000 --> 00:25:56,000
It would only be if they approach you.

384
00:25:56,000 --> 00:26:00,000
You don't really try to point out other people's faults and flaws.

385
00:26:00,000 --> 00:26:05,000
Now, what might become a problem is if people are only superficially interested in the Dhamma,

386
00:26:05,000 --> 00:26:08,000
that will show itself in other ways.

387
00:26:08,000 --> 00:26:12,000
Like, I've heard at some of these meditation groups,

388
00:26:12,000 --> 00:26:15,000
not MBSR, but other groups that are similar.

389
00:26:15,000 --> 00:26:18,000
They'll do a meditation day, and then afterwards they'll have alcohol,

390
00:26:18,000 --> 00:26:21,000
they'll sit around and drink.

391
00:26:21,000 --> 00:26:28,000
Well, that's a real note, no, that's absolutely stay away from that group.

392
00:26:28,000 --> 00:26:32,000
I mean, maybe not stay away, but I've put some serious caution.

393
00:26:32,000 --> 00:26:34,000
Yes, probably stay away.

394
00:26:34,000 --> 00:26:37,000
That'll certainly not go out with them after the meditation,

395
00:26:37,000 --> 00:26:44,000
but at the very least, make your, make it known that you disapprove.

396
00:26:44,000 --> 00:26:49,000
That's not in line with meditative goals,

397
00:26:49,000 --> 00:26:54,000
but they may be frivolous, they may sit around chatting about frivolous things,

398
00:26:54,000 --> 00:26:56,000
and that is a problem.

399
00:26:56,000 --> 00:26:58,000
So, that's what I would focus on more.

400
00:26:58,000 --> 00:27:01,000
And if you can focus on those things and just say,

401
00:27:01,000 --> 00:27:07,000
you know, there's, I think it's important that,

402
00:27:07,000 --> 00:27:12,000
or even not even go so far, but just bring up these teachings.

403
00:27:12,000 --> 00:27:16,000
Hey, could we, do you guys want to study the Buddha's teaching on morality

404
00:27:16,000 --> 00:27:19,000
and on ethics and that kind of thing?

405
00:27:19,000 --> 00:27:24,000
And that, in it, I think, is enough to push people to be more serious

406
00:27:24,000 --> 00:27:27,000
about their practice once they see the importance of morality,

407
00:27:27,000 --> 00:27:35,000
the importance of non-provolity, or how do we say it, for the listeners?

408
00:27:35,000 --> 00:27:37,000
Once they're forced to do that, they'll say,

409
00:27:37,000 --> 00:27:40,000
oh, then I guess I have to step up my game if the Buddha required

410
00:27:40,000 --> 00:27:44,000
more than just us sitting around all day,

411
00:27:44,000 --> 00:27:48,000
because it requires you to be mindful, mindful of your desires,

412
00:27:48,000 --> 00:27:52,000
your aversion, your delusions.

413
00:27:52,000 --> 00:27:54,000
So, I don't know, I mean, that's a little bit,

414
00:27:54,000 --> 00:27:58,000
that's some of my thoughts on it, just to give you maybe some insight.

415
00:27:58,000 --> 00:28:05,000
Maybe that helped.

416
00:28:05,000 --> 00:28:07,000
But the other thing is, don't be too critical.

417
00:28:07,000 --> 00:28:09,000
We often are very, very critical of,

418
00:28:09,000 --> 00:28:15,000
we're very easy to get critical of people who are not as into what we're doing,

419
00:28:15,000 --> 00:28:21,000
or maybe even more not as understanding.

420
00:28:21,000 --> 00:28:23,000
I don't know if it's much understanding,

421
00:28:23,000 --> 00:28:25,000
because it's easy, at least in the beginning,

422
00:28:25,000 --> 00:28:28,000
to look down upon people, not look down exactly,

423
00:28:28,000 --> 00:28:34,000
but become overly parental.

424
00:28:34,000 --> 00:28:37,000
I don't know what the right word is.

425
00:28:37,000 --> 00:28:40,000
Can't think of it, but something like that,

426
00:28:40,000 --> 00:28:43,000
or we want, or we feel like we're in a position,

427
00:28:43,000 --> 00:28:46,000
condescending that becomes, even though we don't realize

428
00:28:46,000 --> 00:28:49,000
that we become quite condescending.

429
00:28:59,000 --> 00:29:02,000
It was just a little bit of a follow-up to that.

430
00:29:02,000 --> 00:29:05,000
I don't want to sound like I'm better than the other people in the group.

431
00:29:05,000 --> 00:29:07,000
I just think my approach to the practice is better,

432
00:29:07,000 --> 00:29:09,000
due to the effort I've been putting in,

433
00:29:09,000 --> 00:29:11,000
to try to understand your books and videos,

434
00:29:11,000 --> 00:29:36,000
and if you sign a dot book.

