1
00:00:00,000 --> 00:00:15,000
And when I try to remind myself to be mindful when listening to someone, I actually lose track of what the other person is saying, because then I'm not focused on the whole, but just the parts of what is being said.

2
00:00:15,000 --> 00:00:23,000
How can I be mindful in conversation and also be able to process what the other person is like?

3
00:00:23,000 --> 00:00:52,000
I've practiced that myself where I've been in conversation situations and some of these waffling on and I'm just noting, you know, listening, listening or hearing, whichever one you want to use to note.

4
00:00:54,000 --> 00:01:02,000
It depends on the conversation. If you're in direct communication with someone, I think it's a little bit harder to do.

5
00:01:02,000 --> 00:01:10,000
But if you're in a group conversation and there are different people talking back and forth, you're not really involved.

6
00:01:10,000 --> 00:01:28,000
It's more possible, but if we press an experience, it's quite a task to communicate in one to one and note in that you're hearing.

7
00:01:28,000 --> 00:01:43,000
It could make probably regulate the note in. Do not say in your mind, hearing, hearing, hearing over and over again, just note that you're hearing and then listen to the words as the person talks.

8
00:01:43,000 --> 00:01:53,000
And then if you're answering a question, then answer the question and then, yeah, it's tough.

9
00:01:53,000 --> 00:02:19,000
I don't personally think or find that the noting itself gets in the way of the understanding, but there's a lot more that goes on when you attempt to note, you can find yourself suppressing or forcing the mind into certain states.

10
00:02:19,000 --> 00:02:37,000
You find it difficult because your mind is accustomed to reacting, so you find yourself reacting and then trying to repress the reactions or reacting to the reactions.

11
00:02:37,000 --> 00:02:54,000
And so because you're breaking it up and because it's actually a real task, because it's not something that is comfortable for someone who's not proficient in it, it's easy to get off track.

12
00:02:54,000 --> 00:03:10,000
So suddenly you'll start to think about something else and lose track of what's being said, you'll start to think about the meditation, oh, am I doing it correctly or even am I understanding what they're saying, oh, I'm not understanding what they're saying and suddenly you lose it.

13
00:03:10,000 --> 00:03:29,000
But the noting itself still leaves room for processing. It doesn't stop the mind from the brain or the mind from processing the meaning of the words.

14
00:03:29,000 --> 00:03:35,000
Not necessarily.

15
00:03:35,000 --> 00:03:59,000
So if your intention is to understand what is being said, you can still couple that with mindfulness, so your mind will love it and then you process what is being said and then your mind back about what they're saying next and process it.

16
00:03:59,000 --> 00:04:14,000
But yeah, it's a question of what your intention is. If your intention is just to meditate and as I said, just let them rattle on and say hearing hearing hearing hearing.

17
00:04:14,000 --> 00:04:39,000
Many times you don't really need to know what they've said, unless it's an important conversation, then it's a question of priorities while your priority is to process what they're saying. So don't give that up, don't dedicate yourself wholly to being mindful and noting the mental event of processing and cancel that as well as on.

18
00:04:39,000 --> 00:04:47,000
Don't don't don't get yourself too focused on the actual sound there so that you lose the mental aspect.

19
00:04:48,000 --> 00:05:00,000
The key of meditation is to purify the mind, purify the processing, the mind not to stop it, not even to stop the mind from thinking, but to purify that process.

20
00:05:00,000 --> 00:05:12,000
So the thinking is only thinking, they need to mutton to reset it so that there's no reaction to it, you understand it for what it is.

21
00:05:13,000 --> 00:05:25,000
So it's possible to get into a state where you just know it is sound, but it's also possible to know it just as concepts and thoughts in the mind, this means this means that.

22
00:05:25,000 --> 00:05:30,000
That's even our hands do that, and they're always mindful.

23
00:05:34,000 --> 00:05:45,000
But yeah, if you focus too much on it, especially when it's not fulfilling your practice, it can be problematic.

24
00:05:45,000 --> 00:06:00,000
So it doesn't mean to stop, I would say do it back and forth and try to, yes, of course be mindful. You should certainly not stop being mindful in conversation, but you might want to alternate it.

25
00:06:00,000 --> 00:06:20,000
So not always noting, let yourself think, let yourself process, even let yourself react because you can't stop the reactions and when the reactions come, just watch the reactions as well, be mindful of them if you're angry to angry, angry, liking, liking, so on.

26
00:06:20,000 --> 00:06:31,000
And then go back to listening. But I think the real problem is not the mindfulness, it's the over focusing and repressing.

27
00:06:32,000 --> 00:06:37,000
And I was in that state after I finished my first course, which didn't really go very well.

28
00:06:38,000 --> 00:06:43,000
One of the teachers was talking to me, and I just said, I can't understand what you're saying.

29
00:06:43,000 --> 00:06:52,000
I was really in a bad way, because it's really realizing how messed my mind up and was messed up my mind.

30
00:06:52,000 --> 00:07:13,000
Just messed up my mind.

