1
00:00:00,000 --> 00:00:07,600
Hi, in this video, I'll teach you how to meditate on the most important thing in the whole

2
00:00:07,600 --> 00:00:12,160
universe, yourself.

3
00:00:12,160 --> 00:00:19,400
Meditating on yourself means watching the things you do, say, and think, and making sure

4
00:00:19,400 --> 00:00:26,760
that you are always doing good deeds, speaking good speech, and thinking good thoughts.

5
00:00:26,760 --> 00:00:31,520
When you meditate on yourself, you will be able to tell whether what you are doing is

6
00:00:31,520 --> 00:00:38,960
good or bad, whether it is hurting yourself or other people, or whether it is helping.

7
00:00:38,960 --> 00:00:44,640
This way, you will be able to make sure that you are only making yourself and other people

8
00:00:44,640 --> 00:00:50,080
happy rather than unhappy.

9
00:00:50,080 --> 00:00:58,080
The easiest part of ourselves to watch is the body, so that's where we start meditating.

10
00:00:58,080 --> 00:01:04,360
Sit in a chair or on the floor, or even lie down, and focus your mind on the position

11
00:01:04,360 --> 00:01:06,880
your body is in.

12
00:01:06,880 --> 00:01:12,360
If you are sitting down, say in your mind, sitting.

13
00:01:12,360 --> 00:01:17,800
If you are lying down, say in your mind, lying.

14
00:01:17,800 --> 00:01:24,800
You can meditate like this as long as you like, just repeating to yourself, sitting,

15
00:01:24,800 --> 00:01:37,680
sitting, sitting, or lying, lying, lying.

16
00:01:37,680 --> 00:01:43,520
Another way of watching the body is to focus on the stomach as it rises and falls when

17
00:01:43,520 --> 00:01:46,600
you breathe in and out.

18
00:01:46,600 --> 00:01:51,760
If you can't feel it, you can just put your hand on your stomach, and focus on the feeling

19
00:01:51,760 --> 00:01:56,320
of the stomach, rising and falling against your hand.

20
00:01:56,320 --> 00:02:08,600
When the stomach rises, say in your mind, rising, and when it falls, think falling, rising,

21
00:02:08,600 --> 00:02:18,080
falling, rising, falling.

22
00:02:18,080 --> 00:02:24,320
You can meditate like this for as long as you like.

23
00:02:24,320 --> 00:02:31,960
The second part of ourselves to watch is the feelings, pain, happiness, and calm.

24
00:02:31,960 --> 00:02:40,280
When you feel pain, you can focus on the pain, and say pain, pain, pain.

25
00:02:40,280 --> 00:02:47,880
When you feel happy, you can think to yourself, happy, happy, happy.

26
00:02:47,880 --> 00:02:56,880
And when you feel calm, you can think to yourself, calm, calm, calm.

27
00:02:56,880 --> 00:03:01,840
Meditating like this helps you let your feelings go so you don't worry about them when

28
00:03:01,840 --> 00:03:03,720
they change.

29
00:03:03,720 --> 00:03:07,360
When you feel pain, you don't have to get upset by it.

30
00:03:07,360 --> 00:03:13,880
And when you feel happy or calm, you don't have to cling to it.

31
00:03:13,880 --> 00:03:18,320
The third part of ourselves to watch is our thoughts.

32
00:03:18,320 --> 00:03:24,840
Whenever you start to think about the past or the future, good thoughts or bad thoughts,

33
00:03:24,840 --> 00:03:31,280
you can just think to yourself, thinking, thinking, and bring your mind back to what is

34
00:03:31,280 --> 00:03:34,560
really going on.

35
00:03:34,560 --> 00:03:37,680
The fourth part of ourselves is our emotions.

36
00:03:37,680 --> 00:03:46,480
These are things like wanting and liking, anger and frustration, sadness and fear, drowsiness,

37
00:03:46,480 --> 00:03:51,960
worry, restlessness, doubt, and confusion.

38
00:03:51,960 --> 00:03:57,840
Whatever emotion comes up, just meditate on it, like anything else, so that it doesn't

39
00:03:57,840 --> 00:04:02,080
get out of control.

40
00:04:02,080 --> 00:04:09,640
Just think to yourself, liking, or angry, or sad, or confused, or whatever, depending

41
00:04:09,640 --> 00:04:15,000
on what emotion comes to your mind.

42
00:04:15,000 --> 00:04:25,400
The fifth part of ourselves is our senses, seeing, hearing, smelling, tasting, and feeling.

43
00:04:25,400 --> 00:04:30,600
You can meditate on any of these whenever you see something, even with your eyes closed.

44
00:04:30,600 --> 00:04:36,720
When you hear a sound, when you smell something good or bad, when you taste whatever it

45
00:04:36,720 --> 00:04:43,640
is you're eating or drinking, or when you feel hot, cold, hard or soft on some part of

46
00:04:43,640 --> 00:04:46,960
the body.

47
00:04:46,960 --> 00:04:58,480
Think in your mind, seeing, seeing, hearing, hearing, smelling, smelling, tasting, tasting,

48
00:04:58,480 --> 00:05:05,040
or feeling, feeling, whenever you experience one of these things.

49
00:05:05,040 --> 00:05:10,600
So to repeat, there are five parts of ourselves that we can meditate on.

50
00:05:10,600 --> 00:05:18,600
Our body, our feelings, our thoughts, our emotions, and our senses.

51
00:05:18,600 --> 00:05:22,560
Now if you're ready, we can try to put them all together.

52
00:05:22,560 --> 00:05:24,880
Start by meditating on your body.

53
00:05:24,880 --> 00:05:32,600
Focus on the position, saying, sitting, sitting, or lying, lying, or focus on the stomach,

54
00:05:32,600 --> 00:05:41,520
saying, rising, falling as it moves, then whenever feelings, thoughts, emotions, or senses

55
00:05:41,520 --> 00:05:46,080
arise, meditate on them instead.

56
00:05:46,080 --> 00:05:49,400
Once they are gone, go back to meditating on the body.

57
00:05:49,400 --> 00:05:50,400
Ready?

58
00:05:50,400 --> 00:06:04,000
Try now.

59
00:08:50,400 --> 00:08:58,800
Well done.

60
00:08:58,800 --> 00:09:03,600
If you keep practicing like this every day, you will see that you know yourself better and

61
00:09:03,600 --> 00:09:09,640
better every day, and you will get better and better at doing, saying, and thinking,

62
00:09:09,640 --> 00:09:14,480
the things that bring you and other people happiness and peace.

63
00:09:14,480 --> 00:09:19,160
And if you've been watching all the videos in this series, you now know just about everything

64
00:09:19,160 --> 00:09:22,280
you need to practice meditation.

65
00:09:22,280 --> 00:09:27,320
Don't be afraid to come back and watch all these videos again to help remind you of how

66
00:09:27,320 --> 00:09:30,440
to meditate whenever you forget.

67
00:09:30,440 --> 00:09:35,640
Thank you for watching and I hope your meditation practice makes you happy and healthy

68
00:09:35,640 --> 00:09:37,720
and free from all suffering.

69
00:09:37,720 --> 00:09:56,320
Bye for now.

