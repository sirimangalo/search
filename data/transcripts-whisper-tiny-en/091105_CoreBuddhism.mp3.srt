1
00:00:00,000 --> 00:00:25,240
No matter what, I don't know what they are saying, I welcome everyone to this dumb attack.

2
00:00:25,240 --> 00:00:44,000
They thought I would talk about how we look at the Buddha's teaching, how we look at Buddhism,

3
00:00:44,000 --> 00:01:00,320
when there are many people who want to say this or that about the Buddha's teaching, about Buddhism in general.

4
00:01:00,320 --> 00:01:10,120
Often when we look at Buddhism, we don't see it as a religion.

5
00:01:10,120 --> 00:01:19,080
And while we may not, because it's not like any other system on earth,

6
00:01:19,080 --> 00:01:37,000
we might consider to be a religion. Most of them focus on the worship of a God, or God's, or goddesses.

7
00:01:37,000 --> 00:01:54,280
Focusing on blind faith or focusing on different things, none of which are similar to the goal of Buddhism or the practice of Buddhism.

8
00:01:54,280 --> 00:02:09,440
And yet Buddhism has had a profound effect on the world, much similar in scope and in nature as other religions.

9
00:02:09,440 --> 00:02:23,320
The number of people who claim to follow Buddhism, and the devotion which people give to this system and calling it a religion,

10
00:02:23,320 --> 00:02:29,440
make it often look very similar to other systems which we might call religion.

11
00:02:29,440 --> 00:02:39,680
Personally, I'm of the view that it should be called a religion, but not for the reasons that we might normally assume,

12
00:02:39,680 --> 00:02:53,280
normally we understand religion to be a system which binds us to God or binds us to something greater than ourselves.

13
00:02:53,280 --> 00:03:01,280
The word religion, of course, coming from a Latin root meaning to bind or so on.

14
00:03:01,280 --> 00:03:10,280
And so there was one monk who cleverly said, in that case Buddhism is not a religion at all.

15
00:03:10,280 --> 00:03:21,280
Because the Buddha taught us to give up all binds, and the Buddha himself claimed that in his enlightenment he was free from all attachments.

16
00:03:21,280 --> 00:03:37,280
And so being bound to anything either heavenly or worldly is not in line with the Buddha's teaching.

17
00:03:37,280 --> 00:03:51,280
But there's another way to look at this word religion, we understand it to be, if we understand it to be from coming from the root to bind.

18
00:03:51,280 --> 00:03:57,280
We can consider that it means simply that we're bound to certain practices.

19
00:03:57,280 --> 00:04:15,280
And as I've said before, this isn't an obligation or a core state, it comes from our own heart when we take up these practices.

20
00:04:15,280 --> 00:04:23,280
And say to ourselves that these practices are essential, and so we dedicate our lives to them.

21
00:04:23,280 --> 00:04:33,280
We say I would rather some people even to the extent that they would rather die than break these practices.

22
00:04:33,280 --> 00:04:43,280
For instance, the practice of morality, there are people who do to their understanding and their dedication to the Buddha's teaching.

23
00:04:43,280 --> 00:04:51,280
They would rather die before they would kill someone else or just from their own practice

24
00:04:51,280 --> 00:04:55,280
and their understanding of the way things work.

25
00:04:55,280 --> 00:05:03,280
So they are bound to certain practices according to the Lord Buddha's teaching or in line with the Lord Buddha's teaching.

26
00:05:03,280 --> 00:05:07,280
Even to the extent that sometimes these practices may not be essential.

27
00:05:07,280 --> 00:05:20,280
So for instance, in the case of becoming a monk or a nun, they might adhere to practices which relate specifically to communal harmony or are simply functional.

28
00:05:20,280 --> 00:05:28,280
And really not essential in it of themselves, for instance, not touching money and so on.

29
00:05:28,280 --> 00:05:39,280
Not essential, but they will keep these rules religiously, we might say.

30
00:05:39,280 --> 00:05:49,280
So on this question, I would say that Buddhism very much is a religion in this sense.

31
00:05:49,280 --> 00:05:55,280
In the sense that, as I've said, I gave a talk on what it means to be Buddhist.

32
00:05:55,280 --> 00:06:03,280
It means to take the Buddha, the Dhamma, the Sangha's are refuge and to keep the five precepts.

33
00:06:03,280 --> 00:06:14,280
And basically what I tried was trying to say is that it's not that we have any obligation to keep these, but if we don't keep them,

34
00:06:14,280 --> 00:06:17,280
then we have no right to be to call ourselves Buddhists.

35
00:06:17,280 --> 00:06:20,280
And for some people this is fine, they don't want to be called Buddhists.

36
00:06:20,280 --> 00:06:32,280
But for anyone who does keep these, one could, and they keep them because of the exhortation of the Buddha, or they have learned to keep these from the Buddha,

37
00:06:32,280 --> 00:06:44,280
they agree with everything with the things that the Buddha said, and they follow according to the Buddha's teaching, and they take the Buddha and his teaching as their guiding light.

38
00:06:44,280 --> 00:06:47,280
Then this person would say would be a Buddhist.

39
00:06:47,280 --> 00:06:59,280
And it's simply a designation, it doesn't have to exist, but there is a difference for between someone who takes the Buddha's teaching as their guiding light and someone who does not.

40
00:06:59,280 --> 00:07:05,280
Another person might change their mind and go on another path and so on.

41
00:07:05,280 --> 00:07:13,280
This isn't exactly what I was hoping to talk about today, I just wanted to start here and say, what is Buddhism?

42
00:07:13,280 --> 00:07:20,280
Because you don't have to look at Buddhism as a religion, and you don't have to see it in this way, but

43
00:07:20,280 --> 00:07:31,280
I would say if you're really going to gain results and progress in the Buddha's teaching, it does have to have a certain level of religiosity.

44
00:07:31,280 --> 00:07:45,280
This isn't something that we're just doing as a hobby where we take time out of our day to meditate, or we take time out of our day to chanting, or this or that.

45
00:07:45,280 --> 00:08:00,280
It's the goal of our existence, or it's the higher purpose, is the realization of those things that the Buddha taught us to develop and to realize.

46
00:08:00,280 --> 00:08:29,280
Because if you just take it sort of as a hobby or something, or something external to your main goal in life or so on, then you can't expect that it's going to have the great results that we would hope for, or that one would expect from someone who was taking it to a religious level.

47
00:08:29,280 --> 00:08:35,280
And so what I wanted to talk today actually is, what is most important?

48
00:08:35,280 --> 00:08:40,280
What is it that we expect to get and hope to get and should hope to get from the Buddha's teaching?

49
00:08:40,280 --> 00:08:55,280
I'd like to talk about what is the most important thing about Buddhism, because there's a lot of different ideas about what is important and what we should be focusing on and so on.

50
00:08:55,280 --> 00:09:11,280
There are many issues in Buddhism, many people are debating the issue of whether women should be reinstated and so on.

51
00:09:11,280 --> 00:09:24,280
And there are various ideas, most people agree that it would be nice to have the opportunity, but they're not sure how to go about it, given the technical difficulties and so on.

52
00:09:24,280 --> 00:09:47,280
And then there's a lot of people who are against it for regions of discrimination, mostly the men, but also some women who think that it's not their place to ordain because they are, I think it's what you call, reverse discrimination or I can't remember.

53
00:09:47,280 --> 00:09:53,280
There's a term for that where you oppress yourself because you've been taught to and so on.

54
00:09:53,280 --> 00:09:57,280
But mostly it's a sexism thing.

55
00:09:57,280 --> 00:10:07,280
And so there's a lot of argument about this now and people are up in arms and banishing each other and creating.

56
00:10:07,280 --> 00:10:10,280
Also a big kerfuffle about this.

57
00:10:10,280 --> 00:10:22,280
Some people are going ahead and helping women to ordain, other people are reprimanding them and separating out and looks like it's going to be a big argument.

58
00:10:22,280 --> 00:10:33,280
And it makes you wonder, well, what is really important here and why we should be so upset about it on either side.

59
00:10:33,280 --> 00:10:43,280
Why if some people want to go ahead and give this opportunity to others, should we be so angry and upset about it?

60
00:10:43,280 --> 00:10:54,280
And I think the answer is because people like to get angry and upset and they're not really dedicated themselves to a religious practice of the Buddhist teaching.

61
00:10:54,280 --> 00:11:12,280
And on the other side, you might wonder whether there's a bit too much enthusiasm to ordain or to reinstate the lineage of female monks and so on.

62
00:11:12,280 --> 00:11:18,280
I mean, just because it's not the goal to become a monk, the goal is of course to become enlightened.

63
00:11:18,280 --> 00:11:26,280
And if it comes to the point where it just becomes an issue of equal rights or so on, then not that that's not a good thing,

64
00:11:26,280 --> 00:11:39,280
but it's going to be a hard battle to fight and one might be weary by the time it's over and too weary to perhaps practice meditation.

65
00:11:39,280 --> 00:11:48,280
So, not that I'm not supportive of this addressing this issue, but I think it's important that we look at what's most important.

66
00:11:48,280 --> 00:12:00,280
And the Buddha laid out what's most important in his teaching. He said, some people, they come and they ordain simply because of the prestige or the honor of ordaining.

67
00:12:00,280 --> 00:12:22,280
And of course, this applies not necessarily just to ordaining his monks or nuns, but when we come to a monastery or when we come to practice at a meditation center, some people come just for the for the material gain that comes along with it.

68
00:12:22,280 --> 00:12:30,280
So, you often see homeless people coming into the monastery, this is at the extreme end.

69
00:12:30,280 --> 00:12:51,280
Or you see monks who ordain and then sit around not doing anything because it can be so luxurious in Buddhist countries or places or Buddhism flourishes because the lay people are so faithful that they support these monks to just sit around and do nothing all day.

70
00:12:51,280 --> 00:13:11,280
And so, it's easy to become lazy and once you get this great honor and it is a great honor to be even to be in a meditation center in a monastery, but most importantly to become a monk or a nun, it's a great honor.

71
00:13:11,280 --> 00:13:30,280
And sometimes you wonder whether this is, whether people are actually able to see through this and you see these groups of monastics who seem to just be content and living the monastic life and living the good life.

72
00:13:30,280 --> 00:13:43,280
A way from the stresses and the worries of society, which is a great thing really, but in and of itself it's not the most important thing.

73
00:13:43,280 --> 00:13:57,280
So, you see communities of monastics who are mostly concerned about monastic harmony and building a monastery and having rules and regulations and meetings and organization.

74
00:13:57,280 --> 00:14:07,280
You know, these monks who have all this real interest in making their own robes and making their own balls and making this and making that.

75
00:14:07,280 --> 00:14:18,280
Which are all fine and noble things, but you wonder whether it's somehow become the focus in many ways and so monasticism in and of itself becomes the focus.

76
00:14:18,280 --> 00:14:30,280
For meditators, it's only worth noting to the extent that sometimes we can become kind of lazy because of the we don't have anything to do.

77
00:14:30,280 --> 00:14:39,280
It's important that we understand when we come to a meditation center that we're quite privileged, just as we would be privileged if we had ordained as a monk or a nun.

78
00:14:39,280 --> 00:14:48,280
And that we should put our heart into our practice and I'm not trying to suggest that anything that our meditators here are not doing that.

79
00:14:48,280 --> 00:14:52,280
It's just this is my role as a teacher to remind us always.

80
00:14:52,280 --> 00:14:56,280
It's something that we already know, but that we should keep in mind at all times.

81
00:14:56,280 --> 00:14:59,280
And as a monk indeed, we have to keep in mind at all times.

82
00:14:59,280 --> 00:15:08,280
Every time we walk on arms, we get this free food that comes from the faith of the laypeople and we have to feel this guilt and this shame.

83
00:15:08,280 --> 00:15:21,280
When we know that we're probably not living up to the expectations which of being a monk or being an ordained monk or nun.

84
00:15:21,280 --> 00:15:37,280
So it's something that should drive us on and we should always be concerned that we are actually living up to our expectations of that are placed upon us.

85
00:15:37,280 --> 00:15:54,280
So some people just for this honor and this gain, this material comfort will come into the Buddhist dispensation and become monks, become nuns and so on.

86
00:15:54,280 --> 00:15:56,280
It might get lazy because of that.

87
00:15:56,280 --> 00:16:02,280
Because it does come, it's just a matter of how you use it.

88
00:16:02,280 --> 00:16:15,280
In the second, for some people they're able to get beyond that and they're able to at least undertake moral precepts.

89
00:16:15,280 --> 00:16:23,280
And they're able to undertake the very basic Buddhist practice of morality.

90
00:16:23,280 --> 00:16:40,280
And this is also a very common thing where you see people very intent on keeping the moral rules and getting so obsessed with minor rules and regulations and thinking themselves to be a great monk or a great nun because of their ability to keep the rules.

91
00:16:40,280 --> 00:16:53,280
As a result of all this it's easy to become arrogant, sort of looking down on other monks and nuns that don't keep the rules and so on.

92
00:16:53,280 --> 00:17:09,280
Or as meditators we might do walking and sitting meditation, we might keep these precepts for meditators and then think of ourselves as something high up because we can do this that other people can't and we think that we are something special.

93
00:17:09,280 --> 00:17:18,280
And it may be that we are special, it may be that it's not an easy thing to keep these rules, it's not an easy thing to walk and sit in meditation for many hours a day.

94
00:17:18,280 --> 00:17:28,280
But the conceit is not something that will, it's not something of any good, any use to us, it's a hindrance to our practice, something we have to let go of.

95
00:17:28,280 --> 00:17:34,280
When we practice meditation we have to practice to let go and to give up.

96
00:17:34,280 --> 00:17:47,280
So when we start to become conceited and think that now I'm a meditator and now I'm good at this, this is not a beneficial thing.

97
00:17:47,280 --> 00:18:03,280
For some people they're able to go beyond this and they're also able to practice concentration, to practice to develop their minds and their minds actually are able to calm down.

98
00:18:03,280 --> 00:18:13,280
And this is also a very common stumbling block for people that when they get to this level of peace and calm they think that they've made it and that's all they have to do.

99
00:18:13,280 --> 00:18:24,280
So you see these monks who were able to sit in the forest and meditate for hours and days and they think they're enlightened and their students think they're enlightened.

100
00:18:24,280 --> 00:18:34,280
But then every so often the defilements get the better of them and they do or they say something that's just ridiculous and then everyone has to try to find excuses for them and so on.

101
00:18:34,280 --> 00:18:41,280
And the truth is because they've just gotten intoxicated by this level of concentration which they've developed.

102
00:18:41,280 --> 00:18:44,280
And again not to say that concentration is a bad thing.

103
00:18:44,280 --> 00:18:48,280
All of these things that would explain to us being good things.

104
00:18:48,280 --> 00:19:01,280
But how he looked at it is a person who just comes for the material benefit, material comfort that one can find in a monastery or meditation center.

105
00:19:01,280 --> 00:19:09,280
It's like a person who goes into the forest looking for the heart wood of a tree, looking for wood to build a house or so on.

106
00:19:09,280 --> 00:19:22,280
To build a house you need this strong heart wood core of the tree and so they go around looking at the trees and then they grab some leaves.

107
00:19:22,280 --> 00:19:27,280
They grab some leaves off the trees and come out of the forest and say now I've got the heart wood.

108
00:19:27,280 --> 00:19:46,280
I'm going to say it's just like that sort of person. It's kind of ridiculous actually. There's nothing you can do with those leaves and it's amazing that someone could mistake the core of the tree to these leaves for these leaves or mistake these leaves for the core of the tree.

109
00:19:46,280 --> 00:20:01,280
A person who, a person who simply developed morality and didn't go any further would be like someone who went into the forest looking for heart wood and came out with branches.

110
00:20:01,280 --> 00:20:12,280
The person who is simply content with this level of focus and concentration, this calm state of mind that lasts for a long time.

111
00:20:12,280 --> 00:20:20,280
It would be like a person who went looking for a heart wood and came out with simply bark.

112
00:20:20,280 --> 00:20:31,280
Then a little bit of said but some people go even further than this. They have this great level of concentration and they're not intoxicated by it. They understand it's benefited but they also are able to go past it.

113
00:20:31,280 --> 00:20:47,280
This is important to meditation that even when good things arise that we're able to overcome them because as we start to see the longer we practice that even these good things are impermanent and the more we cling to them the more suffering that's going to come when they're not there.

114
00:20:47,280 --> 00:20:58,280
They're going to create this imbalance in our perception where we see them as good and see other things as bad or we're not able to stand with unpleasant situations.

115
00:20:58,280 --> 00:21:10,280
But some people are even able to go farther than this but to say then they're able to practice insight meditation to see clearly.

116
00:21:10,280 --> 00:21:16,280
And they're actually able to see clearer than before. They practice and they practice and their minds become clearer and clearer.

117
00:21:16,280 --> 00:21:21,280
And they're able to understand things on a rudimentary level.

118
00:21:21,280 --> 00:21:28,280
Some people can understand impermanence. Some people understand suffering. Some people understand non-self.

119
00:21:28,280 --> 00:21:33,280
They see everything changing. They see their mood swinging. Now they're happy, overjoyed.

120
00:21:33,280 --> 00:21:40,280
Everything's wonderful and now they're miserable and they want to do something drastic and they're depressed or so on.

121
00:21:40,280 --> 00:21:48,280
Up and down and up and down. One day they might be totally calm or one practice. They might be totally calm in the next practice.

122
00:21:48,280 --> 00:21:57,280
They might be totally distracted. They see something good common and they hold on to it and then it disappears and they get upset.

123
00:21:57,280 --> 00:22:07,280
And so they see how this changing of moods goes on and actually their own mind is always changing. It's not stable.

124
00:22:07,280 --> 00:22:15,280
And they say, oh, now I understand impermanence. Now I understand the Buddha's teaching and they say, yes, now I understand meditation. Now I'm a meditator.

125
00:22:15,280 --> 00:22:22,280
Yes, now I know all there is to know and so on.

126
00:22:22,280 --> 00:22:27,280
Some people understand suffering. They sit in meditation and they have all this pain.

127
00:22:27,280 --> 00:22:32,280
But maybe then they say to themselves, pain, pain, they watch it and then it goes away.

128
00:22:32,280 --> 00:22:36,280
And eventually they're able to understand the pain to the point where it doesn't bother them anymore.

129
00:22:36,280 --> 00:22:39,280
When it comes up, they say pain, pain and it disappears quickly.

130
00:22:39,280 --> 00:22:43,280
So they say, oh, now I understand suffering.

131
00:22:43,280 --> 00:22:49,280
Now I understand that there's nothing worth holding on to and they don't cling anymore to the unpleasant feelings.

132
00:22:49,280 --> 00:22:57,280
Some people are already able to understand oneself and so when they start to practice they try to force things.

133
00:22:57,280 --> 00:23:04,280
They try to force their stomach. They try to force the movements of their feet. They try to force their mind to stay on the object.

134
00:23:04,280 --> 00:23:10,280
And as they practice, they see that the more they force things, the more suffering that comes to them.

135
00:23:10,280 --> 00:23:13,280
Their stomach gets all tied up and not.

136
00:23:13,280 --> 00:23:29,280
Their walking becomes erratic, their minds become stressed and distressed and anxious and headache comes and so on.

137
00:23:29,280 --> 00:23:37,280
And so they let go and they stop holding on, they stop forcing and they're able to just observe and sometimes observe very nicely.

138
00:23:37,280 --> 00:23:40,280
Very, very, very well.

139
00:23:40,280 --> 00:23:49,280
But some people get to a very superficial understanding of these things and stop there and then this conceit arises.

140
00:23:49,280 --> 00:24:00,280
Maybe they've been practicing for some time and they think now I know everything and maybe sometimes they even think they know more than their teachers and so on.

141
00:24:00,280 --> 00:24:19,280
And they start to become conceited about it and they think there's no one light and there's no one but me who can understand like this or they think they're special and they want to go out and tell everyone else and explain to everyone else and teach everyone else and so on.

142
00:24:19,280 --> 00:24:33,280
And this wanting to teach other people is usually a sign that we're not there yet because the more you practice, the more you are able to do away with this desire to show everyone else how wrong they are and so on.

143
00:24:33,280 --> 00:24:38,280
You become more and more interested in your own mind and not interested in other people.

144
00:24:38,280 --> 00:24:52,280
Just focus more on your own problems and you'll find the real sign of a good teacher from what I've seen and this is looking at the teachers that I've seen.

145
00:24:52,280 --> 00:25:05,280
They're not so eager to have you come and be their student but they do have the sense of eagerness to help you and to make you happy and make you comfortable.

146
00:25:05,280 --> 00:25:14,280
So they will always be looking out for your welfare and benefit but they don't pull you in through the door they only open it for you.

147
00:25:14,280 --> 00:25:23,280
What I've seen of teachers that they're not trying to convince other people to come to practice their way and so on but they're always leaving the door open.

148
00:25:23,280 --> 00:25:28,280
They're always trying to keep the door open and when you walk in they're always welcoming.

149
00:25:28,280 --> 00:25:40,280
When you come to practice they're always going to give you what you need at that time but not try to convince you that you have to come back tomorrow and you have to come back the next day and pull you in and so on.

150
00:25:40,280 --> 00:26:03,280
So this is even when we do get some kind of insight and understanding of the Buddha's teaching then we have to be careful that we don't become over enthusiastic about it because it can give rise to ego it can give rise to defilement and in the end we should simply look at our own practice and try to understand more about our own defilements.

151
00:26:03,280 --> 00:26:15,280
So it's easy to get derailed at all stages of the path I guess that's the point here from the lowest point lowest.

152
00:26:15,280 --> 00:26:24,280
From the most basic point and all the way up it's easy to get off track at any point along the way.

153
00:26:24,280 --> 00:26:39,280
And the Lord would have said someone who even to this extent that they gain wisdom but not perfect understanding. They don't get to the point where they actually are released from suffering where the mind enters into this cessation of suffering.

154
00:26:39,280 --> 00:27:00,280
But I said this is like a person who goes in looking for heart wood and comes out with wood but it's not the heart wood. It's only this pulpy stuff on the outside of the tree. So it's good, it's still not useful, it's still not enough to build a house, it's still enough to make anything out of.

155
00:27:00,280 --> 00:27:24,280
And so the Lord would have was trying to say that the heart wood of Buddhism would be compared to a very large tree than the heart wood of Buddhism is this enlightenment, this perfect understanding, the realization which comes with intensive practice and a final emancipation where the mind becomes free.

156
00:27:24,280 --> 00:27:35,280
And it comes to see that actually everything in the world is like this. It's just a moment in time where the mind suddenly gets it and suddenly says there's nothing in the world worth clinging to.

157
00:27:35,280 --> 00:27:45,280
And let's go. It's not something intellectual where we say no, nothing's worth clinging to. It's a sudden realization where the mind just let's go.

158
00:27:45,280 --> 00:27:59,280
It's like the world just disappears for a moment because the mind is no longer clinging, it's no longer seeking, it's no longer grasping. It's like you've just disconnected from the matrix you could say, just for a moment.

159
00:27:59,280 --> 00:28:11,280
And that moment is just enough to give you this super mundane realization so that from then on you look at the world in a whole new way.

160
00:28:11,280 --> 00:28:39,280
The world looks somehow lifeless from then on. And as when practices and continues to create this realization or give rise to this realization in this letting go, the less and less interest one has in external things in addiction to things in the world, to the objects of the sense and so on.

161
00:28:39,280 --> 00:28:56,280
Until finally one is able to give up all addiction and all attachment based on these realizations based on this momentary glimpse or this reoccurring glimpse until it becomes our whole vision, our whole understanding.

162
00:28:56,280 --> 00:29:18,280
And this is really the heart would of Buddhism is this super mundane realization where we actually just actually let go of everything in the mind leaves behind the world even just for a moment, but sometimes for longer periods of time and eventually even for hours or days.

163
00:29:18,280 --> 00:29:28,280
And so when we approach questions, I mean the question relevant to the past couple of days that I've been discussing on the internet is the question of women's ordination.

164
00:29:28,280 --> 00:29:44,280
And personally I've got nothing against the whole idea. I've had many students ordain us eight precept nuns, I've even had one nonwearing rope similar to that of a monk and following me on arms.

165
00:29:44,280 --> 00:30:02,280
And then not even following me on arms, but going on arms on her own and I think she's doing that up until this day and having all of the villagers be very keen and happy and giving her just as much food as they gave me and not seeing the difference.

166
00:30:02,280 --> 00:30:25,280
So to me that's really how I look at it, whether the whole question of whether they're going to be fully ordained nuns, it's actually kind of repulsive in a sense because it's such an addiction to this recognition and ego and it's the same feeling I get from a lot of monks that they're so attached to the idea of being a monk and I probably am guilty of it as well.

167
00:30:25,280 --> 00:30:31,280
It's very easy to get attached to such a romantic notion of being a monk.

168
00:30:31,280 --> 00:30:45,280
But I think in the end it's something that you have to let go and it's an impediment on the path, it's an impediment in the practice and it's certainly not by any estimation that goal of the Buddha's teaching.

169
00:30:45,280 --> 00:30:49,280
The goal of course is as I've laid it out.

170
00:30:49,280 --> 00:30:57,280
And so whether people can really become ordained or not really become ordained and have recognition in this way or that way,

171
00:30:57,280 --> 00:31:02,280
the word monk means one who stays alone, the word nun means one who is nothing.

172
00:31:02,280 --> 00:31:06,280
So we should stay alone and be nothing.

173
00:31:06,280 --> 00:31:11,280
And it's nice to have a community that is supportive of that.

174
00:31:11,280 --> 00:31:16,280
In the end it's not that we are this or are that. It's what we aren't.

175
00:31:16,280 --> 00:31:18,280
That's more important.

176
00:31:18,280 --> 00:31:21,280
Most importantly we aren't attached to anything.

177
00:31:21,280 --> 00:31:26,280
We aren't attached to being anything or having or gaining anything.

178
00:31:26,280 --> 00:31:35,280
And so when we approach Buddhism, when we approach the idea of becoming a monk or becoming a nun,

179
00:31:35,280 --> 00:31:39,280
it's really only an expedient towards the goal.

180
00:31:39,280 --> 00:31:43,280
It's for the purpose of the realization of freedom from suffering.

181
00:31:43,280 --> 00:31:50,280
It's not so that we can then go ahead and live our lives with all these rules and be a good monk or be a good nun.

182
00:31:50,280 --> 00:31:54,280
There's really no such thing and there's no benefit there in.

183
00:31:54,280 --> 00:32:03,280
In the end we should just be a good meditator and be a good Buddhist even in the sense that we are able to realize

184
00:32:03,280 --> 00:32:07,280
the teachings of the Lord would have realized the fruit of the meditation practice.

185
00:32:07,280 --> 00:32:10,280
And so we should always keep this in mind of what our goal is.

186
00:32:10,280 --> 00:32:12,280
Our goal is to practice.

187
00:32:12,280 --> 00:32:17,280
And as long as we can be in a situation where this is possible, where this is facilitated,

188
00:32:17,280 --> 00:32:23,280
everything else should be sort of secondary in this idea of some kind of equality

189
00:32:23,280 --> 00:32:25,280
or recognition.

190
00:32:25,280 --> 00:32:28,280
I think it's one of those things that should be left behind.

191
00:32:28,280 --> 00:32:32,280
In fact, I can sometimes be healthy for the ego.

192
00:32:32,280 --> 00:32:34,280
I think I'm not against equality.

193
00:32:34,280 --> 00:32:44,280
I think we should all be treated equal for sure, at least in terms of our worth as human beings.

194
00:32:44,280 --> 00:32:46,280
Sometimes it can be good for the ego.

195
00:32:46,280 --> 00:32:50,280
When we put our teachers up on a pedestal and we have to bow down to them

196
00:32:50,280 --> 00:32:55,280
or even sometimes we have to bow down to people we don't think are worthy of it.

197
00:32:55,280 --> 00:32:57,280
I've had to do the same thing.

198
00:32:57,280 --> 00:33:01,280
I've had to bow down to many monks that I was thinking at the time, boy.

199
00:33:01,280 --> 00:33:09,280
This is a real crap deal to have to bow down to this guy.

200
00:33:09,280 --> 00:33:15,280
But you do it and you let go of your ego and you try to let go of this idea that I'm better than him.

201
00:33:15,280 --> 00:33:20,280
And then you just do it as a matter of course.

202
00:33:20,280 --> 00:33:23,280
It's like that.

203
00:33:23,280 --> 00:33:26,280
It's not like that.

204
00:33:26,280 --> 00:33:31,280
But this is the ideas to become free and to let go.

205
00:33:31,280 --> 00:33:40,280
And if we find ourselves holding on, we can be sure that we're not practicing the Buddha's teaching.

206
00:33:40,280 --> 00:33:46,280
And one thing we have to realize is we're living in an imperfect world and it can't always be the way we want.

207
00:33:46,280 --> 00:33:49,280
I certainly have had to learn that as a monk.

208
00:33:49,280 --> 00:33:51,280
I think of a monk as being like this or like that.

209
00:33:51,280 --> 00:33:55,280
I want to be this way or that way and I expect all other monks to be like that.

210
00:33:55,280 --> 00:33:57,280
And they aren't.

211
00:33:57,280 --> 00:34:02,280
And that can be very depressing and can make you very angry at times.

212
00:34:02,280 --> 00:34:07,280
I've met with lots of monks who were pretty good monks, but they ended up disrobing out of anger

213
00:34:07,280 --> 00:34:14,280
as all the other monks were in their eyes terrible monks and they felt disheartened.

214
00:34:14,280 --> 00:34:25,280
And I think it's sad that we should be so unappealing as a group to people who really have a good intention.

215
00:34:25,280 --> 00:34:31,280
But half the problem is our perception of other people and are looking at them and saying,

216
00:34:31,280 --> 00:34:37,280
aren't they the way we think we want them to be? Why aren't they the way we think they should be?

217
00:34:37,280 --> 00:34:41,280
Instead of looking at ourselves and saying, how can I become the way I know I should be?

218
00:34:41,280 --> 00:34:43,280
I'm trying to change who we are.

219
00:34:43,280 --> 00:34:46,280
Because this is how Buddhism thrives.

220
00:34:46,280 --> 00:34:53,280
It thrives from personal investigation, developing one's own self, not trying to be something for other people,

221
00:34:53,280 --> 00:34:59,280
but trying to be something for oneself, to be free,

222
00:34:59,280 --> 00:35:03,280
to let go of one's own addictions and one's own attachments.

223
00:35:03,280 --> 00:35:05,280
So just some thoughts.

224
00:35:05,280 --> 00:35:08,280
I hope it's relevant to the meditation practice that we know where we're going,

225
00:35:08,280 --> 00:35:19,280
but it's also relevant on a more broad level in terms of how Buddhism should approach certain issues that are going to come up.

226
00:35:19,280 --> 00:35:28,280
I mean the issues of living in the forest or the issues of the issues of becoming a monk or a nun.

227
00:35:28,280 --> 00:35:33,280
And most important how, you know, what is our religious, what our religious practice should be.

228
00:35:33,280 --> 00:35:36,280
It shouldn't be all about externalities.

229
00:35:36,280 --> 00:35:40,280
I think most of us are aware of this, but it's easy to get off track.

230
00:35:40,280 --> 00:35:43,280
And so this is what I'd like to offer today.

231
00:35:43,280 --> 00:35:45,280
Let's let them talk.

232
00:35:45,280 --> 00:36:00,280
So thank you all for tuning in and hope to see you here next time.

