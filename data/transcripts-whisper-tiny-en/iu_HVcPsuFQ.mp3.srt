1
00:00:00,000 --> 00:00:08,480
Peter. Right. First question, I've heard the expression meditate on this as in the problem

2
00:00:08,480 --> 00:00:13,440
or issue you're experiencing. I don't understand the same, please could you explain?

3
00:00:16,480 --> 00:00:23,040
Well, I think the word meditate is quite the useful word. We don't use the word that often

4
00:00:23,040 --> 00:00:31,040
in Pali. We don't use the word that means meditate, but meditate means to, to, in some sense,

5
00:00:34,880 --> 00:00:39,680
fix in the mind. So I think that the closest Pali word or Pali term would be

6
00:00:39,680 --> 00:00:47,200
Yoni so Manasika to set up in the mind with wisdom or Yoni so means to the root of it, to get to the

7
00:00:47,200 --> 00:00:56,400
root of the, of the object in the mind. So it's actually an interesting word. Yoni means the womb

8
00:00:56,400 --> 00:01:07,280
or the seed, the, the, the, the very essence of it. Yoni so means to the essence. Manas means the mind,

9
00:01:07,280 --> 00:01:16,320
Kaara or Manasim means in the mind. Kaara means karma to do. So set up in the mind at the very root of it.

10
00:01:18,080 --> 00:01:25,280
But it's a general term that means to consider to, to mull over. So the day cart is famous for his

11
00:01:25,280 --> 00:01:35,360
book, The Meditation. So and this idea of, of considering a problem or so on. So it's a general

12
00:01:35,360 --> 00:01:41,520
word that means to consider something or to reflect on something, to try to find an answer to the

13
00:01:41,520 --> 00:01:48,960
problem. I can only, from personally, can only speak of the meditation that the type of meditation,

14
00:01:48,960 --> 00:01:55,520
the type of consideration of an issue or a problem that I'm familiar with. And that is really

15
00:01:56,160 --> 00:02:02,720
in regards to the meaning of this word, Yoni so Manasikaara means to set up the essence of the problem

16
00:02:02,720 --> 00:02:08,240
in your mind or the essence of the issue. And the essence of all issues, all problems,

17
00:02:08,240 --> 00:02:15,280
all situations, all experience, all, all, all of our lives is a moment to moment experience.

18
00:02:15,280 --> 00:02:19,600
It's the moment to moment physical and mental states that we're experiencing even now.

19
00:02:21,120 --> 00:02:27,120
So when you have an issue with someone, when you're fighting with someone,

20
00:02:27,120 --> 00:02:34,640
the essence of the experience is the physical and mental states that arise and cease.

21
00:02:34,640 --> 00:02:43,920
There's no you, there's no them, there's no problem. The essence of it is emotions, thoughts,

22
00:02:46,000 --> 00:02:50,880
experiences of physical stimuli, like hearing when the person's yelling at you,

23
00:02:50,880 --> 00:02:58,320
feeling when they're punching you and so on, or whatever the situation is. If you're sick,

24
00:02:58,320 --> 00:03:04,880
then there's the feelings associated with the sickness. And so from a Buddhist point of view,

25
00:03:06,000 --> 00:03:10,960
the answer to your question is meditating on a problem is to break it down into its

26
00:03:10,960 --> 00:03:16,000
constituent parts, break it down into what's really there. Because this idea of an entity,

27
00:03:16,000 --> 00:03:22,160
a problem, the idea of a problem, an issue of an entity of some sort is an illusion. It doesn't

28
00:03:22,160 --> 00:03:29,040
really exist. And that's the atomic nature of a problem, atomic in the sense that you can't

29
00:03:29,040 --> 00:03:33,840
divide it, you can't break it up, makes it impossible to remove. Once it is,

30
00:03:35,600 --> 00:03:40,160
then you have a problem, then there's nothing you can do about it. All you can do is run away

31
00:03:40,160 --> 00:03:48,640
from it. The fact that you can solve the problem shows that the problem itself doesn't exist,

32
00:03:48,640 --> 00:03:54,720
there's only the associated states, which can be altered. When you break it up into the states,

33
00:03:54,720 --> 00:04:00,000
you see that there's actually no problem at all, because pain is just pain, hearing is just hearing,

34
00:04:01,040 --> 00:04:05,920
thinking is just thinking, liking is just liking, just liking, just liking. When you meditate in

35
00:04:05,920 --> 00:04:13,680
this way, consider the reality. So, when you have pain, you consider it as pain, you reflect upon

36
00:04:13,680 --> 00:04:19,280
it, as simply being pain. So, the only so monastic atom is not as mean or as mine, not as good,

37
00:04:19,280 --> 00:04:24,480
not as bad, not even giving rise to any thoughts about it, except that this is pain.

38
00:04:29,360 --> 00:04:33,760
Then the problems, then you see that there is no problem.

39
00:04:33,760 --> 00:04:40,480
Then you become invincible in any situation, whatever problems arise,

40
00:04:41,520 --> 00:04:45,280
you can deal with them. You realize that there is no problem. When a confrontation arises,

41
00:04:46,480 --> 00:04:50,800
you're able to see, well, what is the problem with the confrontation? When yelling at you,

42
00:04:51,760 --> 00:04:56,480
hitting you or so on. It's just pain. The story of this monk who was

43
00:04:56,480 --> 00:05:04,000
caught by a tiger, there were these monks off in the forest, and 30 of them, and they promised

44
00:05:04,000 --> 00:05:12,560
that they would practice meditation. Meditation together, 30 monks, and they wouldn't talk to each

45
00:05:12,560 --> 00:05:18,320
other, and after 15 days on the opposite of the day, the full moon day, they would come together

46
00:05:18,320 --> 00:05:25,760
and meet and do their monastic duties and recite the dhamma and so on. After 15 days,

47
00:05:25,760 --> 00:05:32,400
only 15 of them came back, and they're looking around where the other 15 of them had no idea.

48
00:05:32,400 --> 00:05:38,560
What happened was every day for 15 days from the empty moon to the full moon,

49
00:05:38,560 --> 00:05:45,600
this tiger made off with one of the monks. This is the story, or tigers, maybe it was more than one

50
00:05:45,600 --> 00:05:51,040
that's hard to believe that tiger could eat them, human being every day, but something like that.

51
00:05:51,040 --> 00:05:58,000
Then they made a promise that if someone gets eaten, because what they realized is that every

52
00:05:58,000 --> 00:06:01,920
monk who got eaten could have shouted out to the other monks, and they could have come and helped,

53
00:06:01,920 --> 00:06:06,480
but they had made a promise not to open their mouths, not to talk to, not to say anything.

54
00:06:06,480 --> 00:06:10,720
Even when they were being caught by the tiger, they refused to open their mouth,

55
00:06:12,000 --> 00:06:16,320
and they said, okay, so now we have to make a rule. If someone gets caught by a tiger,

56
00:06:16,320 --> 00:06:21,280
you have to call us, and we'll scare him away. Same thing happened, one of the monks was caught by

57
00:06:21,280 --> 00:06:25,840
a tiger, and they couldn't catch him. The monks shouted out, and they couldn't catch the tiger.

58
00:06:26,480 --> 00:06:30,320
Made off of them, so what they did is they told the monks just to be mindful. They said,

59
00:06:30,320 --> 00:06:35,440
remember, this is your last chance, just be mindful. And the story says that he became an

60
00:06:35,440 --> 00:06:39,920
arah hunt, but I'm not sure how anyone would really know that he did become an arah hunt, but that's

61
00:06:39,920 --> 00:06:47,280
the story. So even in such extreme situations where you have extreme pain where you're dying,

62
00:06:47,280 --> 00:06:53,760
because in the end we all will face the inevitability of death. In the end we have no choice. In the

63
00:06:53,760 --> 00:07:00,080
end we have to meditate on our problems. We have to find some way out, and then this is the

64
00:07:00,080 --> 00:07:05,840
Buddhist way out to focus on things as they are. I don't know if you have anything to add to that.

65
00:07:05,840 --> 00:07:19,600
Yes, there is another aspect, I think, or I experienced on the meditation cushion, that even

66
00:07:19,600 --> 00:07:32,240
when you don't have the yoni saumas manasikara, or when you are not considering something,

67
00:07:32,240 --> 00:07:39,280
but you sit on your meditation cushion in deep meditation in a course or at home,

68
00:07:40,320 --> 00:07:47,760
and you have something in your mind, or you have had something in your mind which required a

69
00:07:47,760 --> 00:07:56,160
solution or an answer or something like that. Then even without thinking about it, when you just

70
00:07:56,160 --> 00:08:06,560
do the meditation practice and observe the rising and the falling of the abdomen and all the

71
00:08:06,560 --> 00:08:18,000
other things that could be observed, then it might happen that the solution comes by itself,

72
00:08:18,000 --> 00:08:30,240
the answer to your question, or what you were thinking about before you meditated, finds it

73
00:08:30,240 --> 00:08:52,080
an answer by itself, just by sitting there and not thinking of it.

