1
00:00:00,000 --> 00:00:29,000
You have been morning yesterday in the study group, we studied the Salekasupta and the

2
00:00:29,000 --> 00:00:48,000
Salekasupta the Buddha describes meditation, meditation that, according to enlightened

3
00:00:48,000 --> 00:01:01,000
senses, considered or called Dita-dhamma-soka-rihara.

4
00:01:01,000 --> 00:01:13,000
We are dwelling, Soka-dwelling and happiness.

5
00:01:13,000 --> 00:01:28,000
Dita-dhamma-dita means seeing dhamma just means like a nature.

6
00:01:28,000 --> 00:01:47,000
So, Dita-dhamma means sealable or of a nature to be seen.

7
00:01:47,000 --> 00:01:52,000
Visible, immediately visible is the meaning.

8
00:01:52,000 --> 00:02:06,000
Meaning is that meditation, certain types of meditation are for the purpose of being happy

9
00:02:06,000 --> 00:02:18,000
now here, immediately visible.

10
00:02:18,000 --> 00:02:22,000
You don't have to wait for the future to be happy.

11
00:02:22,000 --> 00:02:36,000
Meditation is a peaceful abiding, we are a happy state of being.

12
00:02:36,000 --> 00:02:40,000
But he distinguished that from other types of practice.

13
00:02:40,000 --> 00:02:52,000
What's interesting about the Salekasupta is that he seems to be limiting his praise or appreciation

14
00:02:52,000 --> 00:02:58,000
of this sort of meditation.

15
00:02:58,000 --> 00:03:08,000
And that's because Buddhism goes beyond just being happy, not to diminish the importance

16
00:03:08,000 --> 00:03:15,000
of being happy, not even the importance of just being happy.

17
00:03:15,000 --> 00:03:22,000
But that is all it is.

18
00:03:22,000 --> 00:03:30,000
In the Sanghiti Suta, in the Dikkani Kaya, we read about this word again.

19
00:03:30,000 --> 00:03:47,000
In the context of four types of samadhi-bhana, this helps us to see within context,

20
00:03:47,000 --> 00:03:49,000
where do we place?

21
00:03:49,000 --> 00:04:14,000
So, there's no question that happiness is a part of this.

22
00:04:14,000 --> 00:04:25,000
There's no question that happiness is a positive thing, something wished for, desired for,

23
00:04:25,000 --> 00:04:35,000
something it's reasonable to be inclined towards.

24
00:04:35,000 --> 00:04:49,000
Anyone should ever think there's something wrong with being happy.

25
00:04:49,000 --> 00:04:57,000
Especially the type of happiness involved with meditation, because it's very far removed from

26
00:04:57,000 --> 00:05:03,000
any kind of lust or passion or addiction.

27
00:05:03,000 --> 00:05:14,000
There are, you could say, people who get addicted to the peace and calm, but it's not like an addiction to sensuality.

28
00:05:14,000 --> 00:05:18,000
Because when the time you're in it, you're not thinking bad thoughts.

29
00:05:18,000 --> 00:05:27,000
It's not like you have to steal it from someone else to be greedy or manipulate others for it.

30
00:05:27,000 --> 00:05:32,000
You're not thinking harmful thoughts towards yourself or anyone.

31
00:05:32,000 --> 00:05:36,000
Your mind is very pure, that's why there's happiness.

32
00:05:36,000 --> 00:05:44,000
For many people, this is all meditation, means this is all they hope to get from meditation.

33
00:05:44,000 --> 00:05:47,000
And it's a good thing.

34
00:05:47,000 --> 00:05:54,000
The problem is, of course, that it's not the best thing.

35
00:05:54,000 --> 00:06:03,000
It's not the perfect solution to life's problems.

36
00:06:03,000 --> 00:06:06,000
Because you can't always be meditating.

37
00:06:06,000 --> 00:06:11,000
Some people who meditate like this actually do get in conflict with other people,

38
00:06:11,000 --> 00:06:17,000
because they want to meditate all the time, and other people are angry or upset,

39
00:06:17,000 --> 00:06:26,000
because of broken commitments and so on, expectations of engagement and so on.

40
00:06:26,000 --> 00:06:31,000
If someone does become addicted to it, they want to meditate all the time.

41
00:06:31,000 --> 00:06:34,000
They can really get in conflict with others.

42
00:06:34,000 --> 00:06:42,000
Because this meditation doesn't actually help you understand how to deal with conflict.

43
00:06:42,000 --> 00:06:55,000
It's quite possible that you can see it in arrogance and so on.

44
00:06:55,000 --> 00:06:57,000
Leave me alone.

45
00:06:57,000 --> 00:06:59,000
You don't understand.

46
00:06:59,000 --> 00:07:02,000
You're non-meditators.

47
00:07:02,000 --> 00:07:08,000
How could you know what it's like?

48
00:07:08,000 --> 00:07:12,000
Yes, you're coming at me.

49
00:07:12,000 --> 00:07:13,000
You had big nerds.

50
00:07:13,000 --> 00:07:22,000
Only I know you don't know, sort of thing.

51
00:07:22,000 --> 00:07:24,000
Happiness is good, but it's temporary.

52
00:07:24,000 --> 00:07:26,000
You have to believe it.

53
00:07:26,000 --> 00:07:28,000
You can't always be happy.

54
00:07:28,000 --> 00:07:30,000
You can't always be meditating.

55
00:07:30,000 --> 00:07:35,000
Even if you were always meditating, that would be a temporary thing.

56
00:07:35,000 --> 00:07:40,000
And by temporary, I mean, it could last for millions and millions of years

57
00:07:40,000 --> 00:07:44,000
if you were reborn in the Brahma world.

58
00:07:44,000 --> 00:07:46,000
It's still temporary.

59
00:07:46,000 --> 00:07:49,000
It's still not a solution because

60
00:07:49,000 --> 00:07:54,000
you still don't understand the nature of reality

61
00:07:54,000 --> 00:08:00,000
you still haven't understood suffering, causes of suffering.

62
00:08:00,000 --> 00:08:03,000
There's something more profound in it.

63
00:08:03,000 --> 00:08:06,000
Happiness doesn't have any goal.

64
00:08:06,000 --> 00:08:09,000
It doesn't lead to anything.

65
00:08:09,000 --> 00:08:13,000
It doesn't go anywhere from there.

66
00:08:13,000 --> 00:08:19,000
Happiness doesn't lead to happiness.

67
00:08:19,000 --> 00:08:21,000
So it's limited.

68
00:08:21,000 --> 00:08:25,000
And it disappears when it comes and it comes.

69
00:08:25,000 --> 00:08:31,000
So it's the first, it's considered the first type of meditation of samadhi bhavana.

70
00:08:31,000 --> 00:08:38,000
The second type is called nyana dasana, patilambaya,

71
00:08:38,000 --> 00:08:45,000
patilabha.

72
00:08:45,000 --> 00:08:51,000
And the acquiring of nyana and dasana.

73
00:08:51,000 --> 00:08:53,000
Nyana means knowledge.

74
00:08:53,000 --> 00:08:59,000
Dasana means vision.

75
00:08:59,000 --> 00:09:03,000
Some people actually do see things.

76
00:09:03,000 --> 00:09:07,000
It's actually quite common when someone undertakes

77
00:09:07,000 --> 00:09:11,000
intensive meditation.

78
00:09:11,000 --> 00:09:15,000
Often they will even be sitting with their eyes closed

79
00:09:15,000 --> 00:09:19,000
and feel like there is light shining in the window.

80
00:09:19,000 --> 00:09:21,000
Sometimes it even feels like the roof is gone

81
00:09:21,000 --> 00:09:23,000
and the sun is shining down.

82
00:09:23,000 --> 00:09:27,000
Some meditators open their eyes and check whether someone opened a door

83
00:09:27,000 --> 00:09:33,000
or something turned on a light.

84
00:09:33,000 --> 00:09:39,000
Some meditators see colors and pictures, visions.

85
00:09:39,000 --> 00:09:42,000
Some of them are very clear.

86
00:09:42,000 --> 00:09:44,000
Some meditators see things far away.

87
00:09:44,000 --> 00:09:47,000
You have clairvoyance.

88
00:09:47,000 --> 00:09:54,000
Could be clairaudience doesn't have to be visual.

89
00:09:54,000 --> 00:09:58,000
Sometimes we have visions of memories in the past.

90
00:09:58,000 --> 00:10:01,000
Some people even have predictions of the future.

91
00:10:01,000 --> 00:10:04,000
They see things that haven't happened yet.

92
00:10:04,000 --> 00:10:12,000
And only to find them happen later on.

93
00:10:12,000 --> 00:10:16,000
Likewise, many meditators have nyana and knowledge.

94
00:10:16,000 --> 00:10:18,000
They'll learn things.

95
00:10:18,000 --> 00:10:22,000
They'll learn, they'll come to understand things

96
00:10:22,000 --> 00:10:26,000
about themselves, about their situation in life,

97
00:10:26,000 --> 00:10:30,000
answers to their problems.

98
00:10:30,000 --> 00:10:53,000
There are many sort of extra-sensory benefits to meditation.

99
00:10:53,000 --> 00:11:01,000
There are meditations that are especially designed for the purpose of gaining

100
00:11:01,000 --> 00:11:04,000
such knowledge and vision.

101
00:11:04,000 --> 00:11:11,000
Seeing things far away, seeing past lives, remembering past lives,

102
00:11:11,000 --> 00:11:17,000
reading people's minds, sort of thing.

103
00:11:17,000 --> 00:11:27,000
And again, this isn't exactly the sort of thing we're aiming for in meditation.

104
00:11:27,000 --> 00:11:39,000
But really both of these two goals or benefits of meditation show the sort of uniqueness of this practice,

105
00:11:39,000 --> 00:11:47,000
this activity we call meditation, samadhi bhavana.

106
00:11:47,000 --> 00:11:54,000
And it's that it directly affects the mind.

107
00:11:54,000 --> 00:12:00,000
It's not an indirect source of happiness or an indirect source of knowledge or vision.

108
00:12:00,000 --> 00:12:02,000
It's direct.

109
00:12:02,000 --> 00:12:09,000
It is literally the development of the mind.

110
00:12:09,000 --> 00:12:12,000
It's not an indirect development of the mind.

111
00:12:12,000 --> 00:12:17,000
That's exactly what we're doing.

112
00:12:17,000 --> 00:12:26,000
And so you might say that, as I said, that happy states are not the real goal.

113
00:12:26,000 --> 00:12:37,000
The deeper state is to be had and that the sort of knowledge and vision is also not the deepest goal.

114
00:12:37,000 --> 00:12:40,000
But that's really not the point we've been talking about them.

115
00:12:40,000 --> 00:12:55,000
It's much more to reflect or reaffirm for ourselves that this is the way this is the result and the nature of the mind.

116
00:12:55,000 --> 00:13:01,000
Because ultimately, everything we do in meditation brings happiness.

117
00:13:01,000 --> 00:13:05,000
We can talk about deep tatama vi, dita dama suka vihara.

118
00:13:05,000 --> 00:13:08,000
It's the sort of immediate happiness.

119
00:13:08,000 --> 00:13:18,000
But all through the course of meditation, it's all involved in bringing about happiness.

120
00:13:18,000 --> 00:13:26,000
As we gain insight and understanding about reality, that makes us happier.

121
00:13:26,000 --> 00:13:32,000
Just having a clear state of mind clears up so much suffering.

122
00:13:32,000 --> 00:13:36,000
And of course, learning things, knowledge and vision.

123
00:13:36,000 --> 00:13:42,000
While it goes beyond just seeing lights or reading people's minds or something,

124
00:13:42,000 --> 00:13:49,000
how about reading our own minds, understanding our own minds?

125
00:13:49,000 --> 00:13:55,000
How about seeing more clearly our habits?

126
00:13:55,000 --> 00:14:01,000
Knowledge and vision is a very important intrinsic part of any meditation practice.

127
00:14:01,000 --> 00:14:05,000
Many things, many different ways you can see and know.

128
00:14:05,000 --> 00:14:14,000
But it ultimately has, it's an intrinsic part of meditation, the knowing in the scene.

129
00:14:14,000 --> 00:14:21,000
Vipasana, we even call insight, or seeing clearly we call this practice.

130
00:14:21,000 --> 00:14:27,000
Vipasana.

131
00:14:27,000 --> 00:14:36,000
Vipasana does in that happiness, meditation brings happiness.

132
00:14:36,000 --> 00:14:41,000
Vipasana brings knowledge and vision.

133
00:14:41,000 --> 00:14:54,000
The third benefit of meditation, or type of meditation, maybe, is sati-sampajana.

134
00:14:54,000 --> 00:14:58,000
Sati-sampajana-ya.

135
00:14:58,000 --> 00:15:06,000
Samwata-ti, for the purpose of sati, which we translate as mindfulness,

136
00:15:06,000 --> 00:15:17,000
and sampajana, which is often translated as clear comprehension.

137
00:15:17,000 --> 00:15:30,000
Sati is the state of confronting the object, objectively, without judgment.

138
00:15:30,000 --> 00:15:37,000
In essence, not forgetting it, or not losing sight of it.

139
00:15:37,000 --> 00:15:44,000
Not getting caught up in judgment or reaction or extrapolation.

140
00:15:44,000 --> 00:15:48,000
Remembering the actual experience.

141
00:15:48,000 --> 00:15:52,000
Samwajana is the knowledge that comes when you do that.

142
00:15:52,000 --> 00:15:57,000
It's the knowledge of the experience.

143
00:15:57,000 --> 00:16:00,000
So, the text mentions three things.

144
00:16:00,000 --> 00:16:02,000
The first is we deny.

145
00:16:02,000 --> 00:16:06,000
We know about feelings.

146
00:16:06,000 --> 00:16:10,000
Painful feelings, pleasant feelings, neutral feelings.

147
00:16:10,000 --> 00:16:13,000
All three of these are important.

148
00:16:13,000 --> 00:16:24,000
Object of meditation practice, because sati-sampajana, for the purpose of keeping the mind objective,

149
00:16:24,000 --> 00:16:30,000
keeping the mind in a pure state, in a clear state.

150
00:16:30,000 --> 00:16:33,000
We cultivate this clear state of mind.

151
00:16:33,000 --> 00:16:38,000
Every time we say to ourselves, pain, pain, or happy, happy,

152
00:16:38,000 --> 00:16:47,000
we're able to experience the feeling just as it is.

153
00:16:47,000 --> 00:16:53,000
The happy feelings can lead very quickly to addiction,

154
00:16:53,000 --> 00:16:59,000
if we're not clear in the mind.

155
00:16:59,000 --> 00:17:01,000
There's nothing wrong with pleasure, as I said,

156
00:17:01,000 --> 00:17:04,000
no one would argue that happiness is a problem.

157
00:17:04,000 --> 00:17:07,000
There's no problem with happiness.

158
00:17:07,000 --> 00:17:11,000
But with craving or desire, lust, all of these.

159
00:17:11,000 --> 00:17:14,000
There's a real cause for concern,

160
00:17:14,000 --> 00:17:21,000
because it can lead us to act and speak in ways that are to our own

161
00:17:21,000 --> 00:17:24,000
and other people's detriment.

162
00:17:24,000 --> 00:17:28,000
They're just not conducive to happiness, to feud further happiness.

163
00:17:28,000 --> 00:17:36,000
The addiction to happiness is ironically not a very happy thing.

164
00:17:36,000 --> 00:17:39,000
Sati-sampajana, you wonder, why is this?

165
00:17:39,000 --> 00:17:41,000
What's the use of this?

166
00:17:41,000 --> 00:17:45,000
It's a very profound beneficial state,

167
00:17:45,000 --> 00:17:50,000
or beneficial pair of qualities.

168
00:17:50,000 --> 00:17:59,000
Sati-sampajana.

169
00:17:59,000 --> 00:18:07,000
As well, if you're not mindful of pain, it can lead to great suffering.

170
00:18:07,000 --> 00:18:12,000
Neutral feelings lead to great suffering, mostly from delusion.

171
00:18:12,000 --> 00:18:16,000
Neutral feelings breed states of calm,

172
00:18:16,000 --> 00:18:26,000
and feelings of control, controlling the mind, controlling and stable.

173
00:18:26,000 --> 00:18:32,000
In a sense of stability, like everything is going to be okay.

174
00:18:32,000 --> 00:18:41,000
That can be quite misleading, considering how unpredictable things are.

175
00:18:41,000 --> 00:18:46,000
So even the addiction to neutral feelings, calm feelings can be quite dangerous.

176
00:18:46,000 --> 00:18:50,000
Oh, problematic, anyway.

177
00:18:50,000 --> 00:18:58,000
If we're not objective, if we're not clear in the mind, it can be a breeding ground for delusion and arrogance.

178
00:18:58,000 --> 00:19:05,000
You can see attachment, all of these.

179
00:19:05,000 --> 00:19:09,000
After we'd another second one is sunya.

180
00:19:09,000 --> 00:19:11,000
Sunya is unlike we'd announce our perceptions of things.

181
00:19:11,000 --> 00:19:15,000
We'd announce just experiences.

182
00:19:15,000 --> 00:19:25,000
Sunya, how you conceive of it, is it loud, allowed noise, quiet?

183
00:19:25,000 --> 00:19:28,000
People, my voice is too quiet.

184
00:19:28,000 --> 00:19:31,000
I can't hear you.

185
00:19:31,000 --> 00:19:35,000
It's also a cause for concern when you react to things.

186
00:19:35,000 --> 00:19:37,000
It's too loud.

187
00:19:37,000 --> 00:19:39,000
Someone was saying recently to me about dogs barking,

188
00:19:39,000 --> 00:19:41,000
and normally they get very angry,

189
00:19:41,000 --> 00:19:45,000
and they just didn't get angry.

190
00:19:45,000 --> 00:19:47,000
They said hearing, hearing.

191
00:19:47,000 --> 00:19:51,000
And they said, normally I would go and yell at this person.

192
00:19:51,000 --> 00:19:53,000
Those dogs were barking.

193
00:19:53,000 --> 00:19:55,000
But I didn't.

194
00:19:55,000 --> 00:19:59,000
And I just said hearing you.

195
00:19:59,000 --> 00:20:18,000
Sunya, the Buddha said,

196
00:20:18,000 --> 00:20:30,000
blocks any sort of outpouring of the mind where the mind would get caught up in something, get lost in something.

197
00:20:30,000 --> 00:20:35,000
And the third that is mentioned in the text is vidaka.

198
00:20:35,000 --> 00:20:39,000
Vidaka here, I guess, means thoughts.

199
00:20:39,000 --> 00:20:44,000
Watching them arise and see, normally we get caught up in them.

200
00:20:44,000 --> 00:20:48,000
So after sunya, after perception, you perceive something this way or that way,

201
00:20:48,000 --> 00:20:50,000
then you have thoughts.

202
00:20:50,000 --> 00:20:55,000
I should go and yell at this person whose dogs are barking and so on.

203
00:20:55,000 --> 00:20:56,000
This is right.

204
00:20:56,000 --> 00:20:57,000
This is wrong.

205
00:20:57,000 --> 00:20:58,000
I don't deserve this.

206
00:20:58,000 --> 00:20:59,000
I don't deserve that.

207
00:20:59,000 --> 00:21:00,000
I deserve this.

208
00:21:00,000 --> 00:21:02,000
I deserve that.

209
00:21:02,000 --> 00:21:07,000
Any kind of thoughts that might then instigate action or

210
00:21:07,000 --> 00:21:21,000
karma, a mental, volition, mental reaction or reactions in the mind.

211
00:21:21,000 --> 00:21:29,000
Our thoughts as any meditator can tell you our thoughts are a real cause for concern.

212
00:21:29,000 --> 00:21:36,000
If we're not mindful and clear in the mind, you can lead us down very much to the wrong path.

213
00:21:36,000 --> 00:21:59,000
When we get caught up in delusion and feedback loops of worry, anger, fear, addiction.

214
00:21:59,000 --> 00:22:08,000
If you think about how devastating and tortuous bad memories are, worries about the future,

215
00:22:08,000 --> 00:22:14,000
all of these things have vidaka as a base thought when you think about something.

216
00:22:14,000 --> 00:22:25,000
The more you think about it, the more susceptible you are to reaction and become obsessive about things.

217
00:22:25,000 --> 00:22:37,000
When you say to yourself, thinking, thinking, you free yourself from that.

218
00:22:37,000 --> 00:22:39,000
We're thinking is just thinking.

219
00:22:39,000 --> 00:22:45,000
There's no cause for reaction, judgment.

220
00:22:45,000 --> 00:22:53,000
So Satisampaganya is really just a, that's why we put so much emphasis on it as a practice because it's

221
00:22:53,000 --> 00:23:00,000
just the most practical way of living your life.

222
00:23:00,000 --> 00:23:15,000
It allows you to live and to be without the danger of evil or suffering.

223
00:23:15,000 --> 00:23:25,000
So, states of mind that cause stress and suffering.

224
00:23:25,000 --> 00:23:34,000
The fourth type of samadhi Babana or in fact, aspect or benefit of samadhi Babana,

225
00:23:34,000 --> 00:23:46,000
all of samadhi Babana is called as samadhi kaya samadhi.

226
00:23:46,000 --> 00:23:53,000
The asava are the taints or the defilements in the mind.

227
00:23:53,000 --> 00:24:03,000
The defilements are just anything that involves reactivity and leads to suffering.

228
00:24:03,000 --> 00:24:11,000
The defilement is something that is a cause for suffering.

229
00:24:11,000 --> 00:24:17,000
It involves the suffering, something that increases stress in the mind, suffering.

230
00:24:17,000 --> 00:24:26,000
It increases bad behavior that hurts others or hurts ourselves.

231
00:24:26,000 --> 00:24:36,000
Kaya kaya kaya kaya means the destruction.

232
00:24:36,000 --> 00:24:45,000
For the destruction of the asava.

233
00:24:45,000 --> 00:24:50,000
The destruction of the asava.

234
00:24:50,000 --> 00:25:03,000
In our minds or in our character we have both arisen defilements, meaning we are angry sometimes.

235
00:25:03,000 --> 00:25:16,000
But on a deeper level we also have the tendency, the susceptibility, the inclination to get angry.

236
00:25:16,000 --> 00:25:20,000
It is not just an arisen thing, sometimes angry sometimes not enough.

237
00:25:20,000 --> 00:25:27,000
We also have a difference in terms of our susceptibility to getting angry.

238
00:25:27,000 --> 00:25:34,000
Some people are very quick to get angry or greedy at desirous.

239
00:25:34,000 --> 00:25:45,000
See something beautiful, one person might be unmoved, another person might be driven by lust to obtain the object of beauty.

240
00:25:45,000 --> 00:25:59,000
Or pleasure.

241
00:25:59,000 --> 00:26:28,000
And so Satya Sampajani allows us to see this, but the real release or the real liberation comes about not as the practice whereby we are not reacting to things.

242
00:26:28,000 --> 00:26:40,000
Because Satya keeps us from reacting, so there is no arising anger or greed or delusion when you have Satya.

243
00:26:40,000 --> 00:26:56,000
The real liberation comes about from what follows as a result of Satya Sampajani, and that is wisdom and understanding, clarity.

244
00:26:56,000 --> 00:27:05,000
It is not the same as you practice, you have no greed or anger or delusion.

245
00:27:05,000 --> 00:27:13,000
Otherwise we could just practice tranquility, meditation, calm meditation, and that would be enough.

246
00:27:13,000 --> 00:27:25,000
Now the special quality of Satya Sampajani is that not only does it keep us from defilement, but it allows us to see more clearly.

247
00:27:25,000 --> 00:27:34,000
What do we see? We see that everything inside of us and in the world around us is impermanent, momentary, arising and ceasing.

248
00:27:34,000 --> 00:27:43,000
That the people, places and things that we think of as existing in the world are actually made up of moments of experience.

249
00:27:43,000 --> 00:28:08,000
That everything inside of us and in the world around us is therefore what we call unsatisfying, suffering is the word that they use, but it means unable to be a source of true gratification and satisfaction.

250
00:28:08,000 --> 00:28:23,000
Unable because it is momentary, not sukha, something that is momentary can't really be in any way useful or good.

251
00:28:23,000 --> 00:28:27,000
It just means it is meaningless and useless.

252
00:28:27,000 --> 00:28:31,000
You can't use something that is here and gone in a moment.

253
00:28:31,000 --> 00:28:44,000
You can't depend upon it, you can't rely upon it, you can't think of it as being your source for happiness and peace and pleasure.

254
00:28:44,000 --> 00:28:58,000
And that everything inside of us and in the world around us is not me, it's not mine, it's not a self or a soul or an entity in and of itself. It's just an experience, everything.

255
00:28:58,000 --> 00:29:09,000
Everything is just experiences made up of moments.

256
00:29:09,000 --> 00:29:17,000
When you see this, it's different from not being greedy, angry, deluded.

257
00:29:17,000 --> 00:29:33,000
It eats away at the tendency, the inclination, the proclivity to become greedy, angry and deluded.

258
00:29:33,000 --> 00:29:50,000
It changes the way we look at things, right? That's the whole point, is that our liking and disliking or partiality and my reactions to things stress and worry.

259
00:29:50,000 --> 00:30:09,000
It's all very much based on how we look at things, how we perceive things. This is worrisome, fearsome, desirable, unpleasant, undesirable.

260
00:30:09,000 --> 00:30:26,000
Those perceptions, those perceptions are based very much on concepts of stability, satisfaction, control, cell phone, all the things that I said we come to see are not real.

261
00:30:26,000 --> 00:30:42,000
When we come to see everything is unpredictable and momentary and uncontrollable and so on.

262
00:30:42,000 --> 00:30:51,000
We weaken and eventually lose our inclination to react. Why would you get angry about things when they're momentary?

263
00:30:51,000 --> 00:31:08,000
Because you're getting angry about just concepts, just ideas, whatever it is underlying the thing you're getting angry is already gone.

264
00:31:08,000 --> 00:31:37,000
And so this cuts off. This allows us to attenuate and eventually cut off in the sense of really just changing our minds, making a shift where the mind lets go, ultimately completely letting go and experiencing complete freedom, release from any kind of inclination or attachment to.

265
00:31:37,000 --> 00:31:47,000
To some sorrow, to things that are ultimately suffering or unsatisfying.

266
00:31:47,000 --> 00:32:00,000
And this is the ultimate goal. This is the final goal and ultimate reason why we practice samadhi Babana.

267
00:32:00,000 --> 00:32:08,000
But as you can see all four of these provide descriptions of parts or aspects of the practice.

268
00:32:08,000 --> 00:32:26,000
All meditation is pleasant and the best meditation is pleasant in the sense of leading to happiness or involving the cultivation of happiness over suffering over stress.

269
00:32:26,000 --> 00:32:39,000
It involves knowledge and vision, of course. So we're describing you're seeing more clearly knowing things we didn't know before, understanding things we didn't understand before.

270
00:32:39,000 --> 00:33:08,000
It involves sati sampajanya, which is the engagement with reality and a wholesome, productive, skillful, beneficial way. And it involves freeing us from our bad habits, our wrong perceptions, wrong understandings, wrong in the sense of our understanding's perceptions, inclinations that let us deciphering, let us distress.

271
00:33:08,000 --> 00:33:15,000
Let us to evil and wholesome states that hurt us and hurt others.

272
00:33:15,000 --> 00:33:23,000
When we free ourselves from those, we have accomplished the goal.

273
00:33:23,000 --> 00:33:37,000
So these are the four samadhi Babana, four aspects, types, for ways of looking at samadhi.

274
00:33:37,000 --> 00:33:44,000
That's the demo for this morning. Thank you.

