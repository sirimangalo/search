1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Damabana.

2
00:00:05,000 --> 00:00:14,000
Today we continue on with verses 131 and 132, which read as follows.

3
00:00:14,000 --> 00:00:21,000
Sukhakama, Nibudani, Yodandena, Weingseti.

4
00:00:21,000 --> 00:00:28,000
At the Nosa, Kameh Sano, Bejesso, Nalabatei, Sukhal.

5
00:00:28,000 --> 00:00:34,000
Sukhakama, Nibudani, Yodandena, Hingseti.

6
00:00:34,000 --> 00:00:41,000
At the Nosa, Kameh Sano, Bejesso, Babatei, Sukhal.

7
00:00:41,000 --> 00:00:50,000
It's a twin pair of verses there saying the same thing, but in opposition.

8
00:00:50,000 --> 00:00:59,000
So, 131 means Sukhakama, Nibudani, those beings that desire happiness.

9
00:00:59,000 --> 00:01:08,000
Yodandena, Weingseti, who harms them with a stick or with a weapon.

10
00:01:08,000 --> 00:01:21,000
At the Nosa, Kameh Sano, for this, for, for, by one,

11
00:01:21,000 --> 00:01:29,000
by one who is seeking for happiness, meaning the person doing the harming.

12
00:01:29,000 --> 00:01:33,000
So, a person harming someone who is who wishes for happiness, harming them,

13
00:01:33,000 --> 00:01:40,000
and being, harming beings that wish for happiness, they themselves seeking happiness.

14
00:01:40,000 --> 00:01:50,000
Bejesso, Nalabatei, Sukhal, in the future they do not attain happiness.

15
00:01:50,000 --> 00:01:53,000
And 132 is the opposite.

16
00:01:53,000 --> 00:01:58,000
If someone does not harm others with a stick, the other beings that seek happiness,

17
00:01:58,000 --> 00:02:12,000
and wish for happiness, one happiness, one self seeking happiness, one does find happiness in the future.

18
00:02:12,000 --> 00:02:18,000
So, this is the, this is the dhamma for today.

19
00:02:18,000 --> 00:02:25,000
The story behind these verses, it's regarding a group of boys

20
00:02:25,000 --> 00:02:29,000
who, the Buddha of Saun, is way to Sabhati.

21
00:02:29,000 --> 00:02:32,000
So, the Buddha lived in Jetawana for many years.

22
00:02:32,000 --> 00:02:41,000
So, it's like, you can still go and see the ruins of the ancient monastery in India.

23
00:02:41,000 --> 00:02:45,000
And you can walk from, it's actually quite a ways,

24
00:02:45,000 --> 00:02:49,000
but you can walk from Jetawana to Sabhati.

25
00:02:49,000 --> 00:02:57,000
And in Sabhati, there are two kind of pyramid type things.

26
00:02:57,000 --> 00:03:04,000
Probably, maybe they were round at one point, but they're kind of square kind of pyramid type things

27
00:03:04,000 --> 00:03:13,000
that are said to be the monuments to Angulimala and Nathapindika.

28
00:03:13,000 --> 00:03:20,000
And if you go up on top of the stupa, you can see the walls of Sabhati.

29
00:03:20,000 --> 00:03:25,000
You can see the ruined city.

30
00:03:25,000 --> 00:03:35,000
And so, when the Buddha was on his way into Sabhati, he saw a bunch of boys beating a snake with a stick.

31
00:03:35,000 --> 00:03:43,000
It's an interesting sort of a humanizing story that the Buddha meets with a bunch of children

32
00:03:43,000 --> 00:03:45,000
and how he teaches children.

33
00:03:45,000 --> 00:03:48,000
Versus the commentary again.

34
00:03:48,000 --> 00:03:52,000
We don't have any evidence that this actually happened,

35
00:03:52,000 --> 00:03:56,000
but it's an interesting look into how the Buddha behaved

36
00:03:56,000 --> 00:03:59,000
or how people remember him.

37
00:03:59,000 --> 00:04:02,000
The things people remember him to have done.

38
00:04:02,000 --> 00:04:08,000
So, he asked them, what are you doing?

39
00:04:08,000 --> 00:04:14,000
Kumaraka, a young man, King Garotha, what are you doing?

40
00:04:14,000 --> 00:04:19,000
Ahingbante, dandikin, pahrama.

41
00:04:19,000 --> 00:04:31,000
It's a snake, then the Buddha said, King Garuna, for what reason?

42
00:04:31,000 --> 00:04:40,000
They said, dang Sanambayanabante.

43
00:04:40,000 --> 00:04:45,000
We're afraid of its bite, then we're closer.

44
00:04:45,000 --> 00:04:50,000
The Buddha shook his head and said, dang, he shook his head.

45
00:04:50,000 --> 00:04:56,000
Thome, you think to yourself, Thome, you think to yourself,

46
00:04:56,000 --> 00:05:01,000
at the nose, who can carry Sama, we will make happiness for ourselves.

47
00:05:01,000 --> 00:05:04,000
We will bring ourselves happiness.

48
00:05:04,000 --> 00:05:09,000
And for that reason, Imam Baharanta, you beat the stick.

49
00:05:09,000 --> 00:05:14,000
You beat the snake with the stick.

50
00:05:14,000 --> 00:05:18,000
But he said, you will not be nibata, nibata, tani,

51
00:05:18,000 --> 00:05:21,000
and whatever places you are born.

52
00:05:21,000 --> 00:05:28,000
Sukalabi, no, nibata, you will not be one zwati, nibata.

53
00:05:28,000 --> 00:05:40,000
At the nose, sukambati, tani, tani, pahrama, pahari, tum, nibata, tani.

54
00:05:40,000 --> 00:05:53,000
When it's not able to, the beating with the stick does not go for,

55
00:05:53,000 --> 00:05:58,000
I don't quite get that.

56
00:05:58,000 --> 00:06:05,000
Basically, beating others doesn't need to happiness.

57
00:06:05,000 --> 00:06:10,000
And so he taught this verse.

58
00:06:10,000 --> 00:06:20,000
So simple, I mean, it ties in a lot with, ties in a lot with the past two stories.

59
00:06:20,000 --> 00:06:25,000
And the past two verses about one should not hurt others,

60
00:06:25,000 --> 00:06:29,000
oneself not wishing harm.

61
00:06:29,000 --> 00:06:33,000
But the curious thing here is the logic of the boy is,

62
00:06:33,000 --> 00:06:35,000
because it seems like sound logic, right?

63
00:06:35,000 --> 00:06:42,000
We are afraid of the bite of the snake.

64
00:06:42,000 --> 00:06:44,000
But of course, if you think for a moment about it,

65
00:06:44,000 --> 00:06:48,000
the snake wasn't actually, most likely,

66
00:06:48,000 --> 00:06:52,000
it wasn't actually a danger to the boy.

67
00:06:52,000 --> 00:06:55,000
We just found the snake and they thought, oh, he might bite us,

68
00:06:55,000 --> 00:06:58,000
so we better kill it.

69
00:06:58,000 --> 00:07:00,000
But the snake wasn't looking to bite a human,

70
00:07:00,000 --> 00:07:05,000
the snake was probably looking for a smaller animal to kill.

71
00:07:05,000 --> 00:07:08,000
Not to say snakes aren't vicious evil animals,

72
00:07:08,000 --> 00:07:15,000
but I mean, we do recognize that as a,

73
00:07:15,000 --> 00:07:17,000
at least a small evil.

74
00:07:17,000 --> 00:07:23,000
I mean, fair evil of snakes killing other animals and biting them.

75
00:07:23,000 --> 00:07:28,000
But it wasn't doing harm to the boys.

76
00:07:28,000 --> 00:07:33,000
And so it's a good example of our tendency

77
00:07:33,000 --> 00:07:38,000
to focus more on a cure, focus more on the cure

78
00:07:38,000 --> 00:07:42,000
to a problem than the cause of a problem.

79
00:07:45,000 --> 00:07:47,000
So there's a good example of that,

80
00:07:47,000 --> 00:07:52,000
but it's an important delusion or error

81
00:07:52,000 --> 00:07:57,000
or error that we make.

82
00:07:57,000 --> 00:08:02,000
It was a problem that leads to errors and judgment.

83
00:08:02,000 --> 00:08:04,000
So take these boys, for example.

84
00:08:04,000 --> 00:08:08,000
They think the problem is this.

85
00:08:08,000 --> 00:08:11,000
They think the cause of their problem is the snake, right?

86
00:08:11,000 --> 00:08:13,000
The snake is causing problems.

87
00:08:13,000 --> 00:08:17,000
And so immediately, they look for a solution.

88
00:08:17,000 --> 00:08:21,000
They don't consider and they don't...

89
00:08:21,000 --> 00:08:26,000
They look closely at the cause or what the real problem is.

90
00:08:26,000 --> 00:08:30,000
And so they focus on the solution and immediately beat the snake.

91
00:08:30,000 --> 00:08:33,000
If I get rid of the snake, then my problem will be over,

92
00:08:33,000 --> 00:08:36,000
but they haven't understood the problem.

93
00:08:36,000 --> 00:08:38,000
And in fact, the real problem is not the snake.

94
00:08:38,000 --> 00:08:39,000
It's the fear.

95
00:08:39,000 --> 00:08:40,000
That's what they say.

96
00:08:40,000 --> 00:08:41,000
We're afraid you will bite us.

97
00:08:41,000 --> 00:08:44,000
And so without investigating and seeing what is the real problem,

98
00:08:44,000 --> 00:08:46,000
whereas an adult would look at it and would say,

99
00:08:46,000 --> 00:08:48,000
well, don't be afraid of the snake.

100
00:08:48,000 --> 00:08:50,000
It's not gonna harm you, right?

101
00:08:50,000 --> 00:08:54,000
It's no reason to be afraid of it.

102
00:08:54,000 --> 00:08:56,000
But we do this with so many different things.

103
00:08:56,000 --> 00:09:04,000
We take the status of mental illness in the West.

104
00:09:04,000 --> 00:09:09,000
As soon as we have a problem like depression or anxiety,

105
00:09:09,000 --> 00:09:12,000
we're very quick to find a pill.

106
00:09:12,000 --> 00:09:16,000
That will give us a solution without looking at the cause.

107
00:09:16,000 --> 00:09:21,000
Without really looking and seeing what's the problem.

108
00:09:21,000 --> 00:09:23,000
I think I can't cope.

109
00:09:23,000 --> 00:09:27,000
I can't handle.

110
00:09:27,000 --> 00:09:30,000
And in fact, the experiences that we have aren't the problem.

111
00:09:30,000 --> 00:09:34,000
It's our reactions to them.

112
00:09:34,000 --> 00:09:36,000
And a doctor will tell you, of course,

113
00:09:36,000 --> 00:09:37,000
that if you want to cure a sickness,

114
00:09:37,000 --> 00:09:39,000
you have to find the cause.

115
00:09:39,000 --> 00:09:40,000
You have to look at the cause.

116
00:09:40,000 --> 00:09:42,000
Well, the same should go with the mind.

117
00:09:42,000 --> 00:09:49,000
The same should go with our addictions and our versions.

118
00:09:49,000 --> 00:09:53,000
Our mental problems.

119
00:09:53,000 --> 00:09:56,000
Because when you look at what's causing you problems,

120
00:09:56,000 --> 00:10:00,000
when you examine it, you find that really the answer

121
00:10:00,000 --> 00:10:02,000
is not in finding a cure.

122
00:10:02,000 --> 00:10:06,000
The cure is not in fixing things.

123
00:10:06,000 --> 00:10:09,000
The cure is learning to see things differently.

124
00:10:09,000 --> 00:10:17,000
So let's go and release yourself from the very idea

125
00:10:17,000 --> 00:10:24,000
that there's a problem in the first place.

126
00:10:24,000 --> 00:10:40,000
And so the real thing, the real deal is our...

127
00:10:40,000 --> 00:10:45,000
I mean, the real problem is not that we seek for a cure.

128
00:10:45,000 --> 00:10:46,000
It's not that we don't understand.

129
00:10:46,000 --> 00:10:48,000
So we don't understand the problem.

130
00:10:48,000 --> 00:10:53,000
And we're inconsistent in our solution.

131
00:10:53,000 --> 00:10:56,000
So some of our ideas might be good.

132
00:10:56,000 --> 00:11:00,000
Some of our ideas might be bad.

133
00:11:00,000 --> 00:11:05,000
We're unable to be consistently successful

134
00:11:05,000 --> 00:11:07,000
in solving our problems,

135
00:11:07,000 --> 00:11:10,000
because we don't look closely at the cause.

136
00:11:10,000 --> 00:11:14,000
So it's just this idea that in Buddhism,

137
00:11:14,000 --> 00:11:16,000
we do focus much more on the cause.

138
00:11:16,000 --> 00:11:19,000
We're not immediately looking to solve our problems.

139
00:11:19,000 --> 00:11:21,000
And that's sort of the difference

140
00:11:21,000 --> 00:11:24,000
between some at the meditation and we pass in the meditation.

141
00:11:24,000 --> 00:11:27,000
Some at the meditation, you're looking for a quick fix.

142
00:11:27,000 --> 00:11:29,000
Doesn't solve the problem.

143
00:11:29,000 --> 00:11:32,000
With we pass in the meditation, you put aside the cure.

144
00:11:32,000 --> 00:11:37,000
And the fix in terms of being happy and being peaceful.

145
00:11:37,000 --> 00:11:41,000
And in fact, insight meditation can be quite unpleasant

146
00:11:41,000 --> 00:11:45,000
and chaotic, stressful even.

147
00:11:45,000 --> 00:11:51,000
As you're putting it aside, in favor of looking at the problem.

148
00:11:51,000 --> 00:11:53,000
When you have pain, you look at the pain.

149
00:11:53,000 --> 00:11:58,000
Well, that's not pleasant.

150
00:11:58,000 --> 00:12:03,000
But the focus on the cause instead of the cure

151
00:12:03,000 --> 00:12:06,000
means that you're able to solve problems

152
00:12:06,000 --> 00:12:12,000
to come up with solutions that are actually beneficial.

153
00:12:12,000 --> 00:12:14,000
These boys were looking to be happy.

154
00:12:14,000 --> 00:12:17,000
But it points this out quite clearly.

155
00:12:17,000 --> 00:12:21,000
At the no-sooka mesa, no, they were seeking happiness.

156
00:12:21,000 --> 00:12:22,000
That's why they were doing it.

157
00:12:22,000 --> 00:12:24,000
They were doing it to bring happiness.

158
00:12:24,000 --> 00:12:26,000
They had the right intention.

159
00:12:26,000 --> 00:12:28,000
Their minds weren't clear.

160
00:12:28,000 --> 00:12:32,000
They hadn't studied the problem and the situation

161
00:12:32,000 --> 00:12:37,000
to the extent that they would really understand what the problem was.

162
00:12:37,000 --> 00:12:40,000
And the Buddha goes into the Buddha sees things

163
00:12:40,000 --> 00:12:42,000
beyond what most of us can see.

164
00:12:42,000 --> 00:12:45,000
So it's not only is it going to be a problem for your minds now.

165
00:12:45,000 --> 00:12:48,000
Well, you're going to, when you're reborn,

166
00:12:48,000 --> 00:12:50,000
you're going to get beaten by a stick.

167
00:12:50,000 --> 00:12:51,000
You're going to be in trouble.

168
00:12:51,000 --> 00:12:54,000
You're going to go to hell.

169
00:12:54,000 --> 00:12:59,000
Lots of bad things can happen.

170
00:12:59,000 --> 00:13:02,000
So for our meditation practice,

171
00:13:02,000 --> 00:13:06,000
we spend our time studying causes.

172
00:13:06,000 --> 00:13:08,000
We don't concern ourselves so much

173
00:13:08,000 --> 00:13:10,000
with the peace and the happiness.

174
00:13:10,000 --> 00:13:15,000
Really, really kind of the beauty of the practice.

175
00:13:15,000 --> 00:13:19,000
We're not dependent on results.

176
00:13:19,000 --> 00:13:21,000
It's much more about the process.

177
00:13:21,000 --> 00:13:23,000
And when happiness does come,

178
00:13:23,000 --> 00:13:25,000
because it does come from inside.

179
00:13:25,000 --> 00:13:26,000
Make no mistake,

180
00:13:26,000 --> 00:13:31,000
insight meditation is designed to bring peace, happiness, freedom from suffering.

181
00:13:31,000 --> 00:13:34,000
But when it does come, we're not attached to it.

182
00:13:34,000 --> 00:13:35,000
We're not dependent on it.

183
00:13:35,000 --> 00:13:39,000
We kind of just chalk it up to good results

184
00:13:39,000 --> 00:13:42,000
and keep going back into the bad stuff.

185
00:13:42,000 --> 00:13:43,000
It's like when you're cleaning house.

186
00:13:43,000 --> 00:13:44,000
When the room's clean,

187
00:13:44,000 --> 00:13:47,000
you leave and go find a dirty room.

188
00:13:47,000 --> 00:13:48,000
You do that until you're clean.

189
00:13:48,000 --> 00:13:50,000
That's how you clean your house.

190
00:13:50,000 --> 00:13:52,000
That's how you clean your mind.

191
00:13:52,000 --> 00:13:54,000
Don't sit there and marvel at the clean room

192
00:13:54,000 --> 00:13:56,000
and forget about all the rest.

193
00:13:56,000 --> 00:13:58,000
Well, at least this room's clean.

194
00:13:58,000 --> 00:13:59,000
Maybe I'll just live in this room

195
00:13:59,000 --> 00:14:03,000
and not live in the rest of the rooms in the house.

196
00:14:03,000 --> 00:14:06,000
So in meditation, we focus on the bad stuff mostly.

197
00:14:06,000 --> 00:14:07,000
It's important that we do.

198
00:14:07,000 --> 00:14:09,000
I think that's proper to say.

199
00:14:09,000 --> 00:14:11,000
Don't focus on the good stuff.

200
00:14:11,000 --> 00:14:12,000
Don't worry about it.

201
00:14:12,000 --> 00:14:14,000
Focus on the bad stuff.

202
00:14:14,000 --> 00:14:17,000
Including how we react to good things.

203
00:14:17,000 --> 00:14:20,000
It's not all pain and suffering,

204
00:14:20,000 --> 00:14:22,000
but there's greed and addiction as well.

205
00:14:22,000 --> 00:14:26,000
But on either side,

206
00:14:26,000 --> 00:14:28,000
focusing on what's wrong,

207
00:14:28,000 --> 00:14:29,000
just like a doctor would,

208
00:14:29,000 --> 00:14:32,000
just like someone cleaning house would.

209
00:14:32,000 --> 00:14:34,000
A doctor focuses on the sickness,

210
00:14:34,000 --> 00:14:36,000
tries to find the cause.

211
00:14:36,000 --> 00:14:45,000
It can give you medication to suppress the symptoms

212
00:14:45,000 --> 00:14:47,000
and even cure the sickness.

213
00:14:47,000 --> 00:14:54,000
Like if a person has abused their body,

214
00:14:54,000 --> 00:14:56,000
the doctor can fix the symptoms.

215
00:14:56,000 --> 00:15:00,000
But if you go back and abuse your body again,

216
00:15:00,000 --> 00:15:03,000
what good is it to cure the symptoms?

217
00:15:03,000 --> 00:15:04,000
Even if it's a cure.

218
00:15:04,000 --> 00:15:08,000
You just do the same thing over again.

219
00:15:08,000 --> 00:15:10,000
So that's more important,

220
00:15:10,000 --> 00:15:11,000
is it to find the cause?

221
00:15:11,000 --> 00:15:14,000
And they say an ounce of prevention is worth a pound of cure,

222
00:15:14,000 --> 00:15:15,000
right?

223
00:15:15,000 --> 00:15:20,000
That's what I'm saying for this reason.

224
00:15:20,000 --> 00:15:22,000
So we're not so much concerned with results.

225
00:15:22,000 --> 00:15:24,000
This is a problem meditators have.

226
00:15:24,000 --> 00:15:29,000
They're worried that their limitations are not comfortable.

227
00:15:29,000 --> 00:15:32,000
Is this meditation actually any good?

228
00:15:32,000 --> 00:15:35,000
Where you should find results is in the fixing and the solving.

229
00:15:35,000 --> 00:15:38,000
Sorry, you're not in the fixing and the solving.

230
00:15:38,000 --> 00:15:42,000
In the moment of addressing,

231
00:15:42,000 --> 00:15:46,000
not to fixing, but the addressing.

232
00:15:46,000 --> 00:15:53,000
When you address the experience and purify the moment,

233
00:15:53,000 --> 00:15:56,000
where you look at the experience in a new way.

234
00:15:56,000 --> 00:16:00,000
When you have pain, you look at it at just pain.

235
00:16:00,000 --> 00:16:04,000
When you have thoughts, you look at them as just thoughts.

236
00:16:04,000 --> 00:16:06,000
Even your emotions learning to look at them

237
00:16:06,000 --> 00:16:11,000
and see them just as emotions and not reacting to them.

238
00:16:11,000 --> 00:16:15,000
So that's the dumb bada versus for today.

239
00:16:15,000 --> 00:16:18,000
We're now at up to 132,

240
00:16:18,000 --> 00:16:20,000
wishing you all the best.

241
00:16:20,000 --> 00:16:36,000
Have a good name.

