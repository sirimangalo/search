WEBVTT

00:00.000 --> 00:11.600
Why is there so much emphasis on following which tradition or which lineage of praxing Buddhism?

00:11.600 --> 00:17.440
If the dharma is taught by Gautama Buddha, should we only emphasize and refer as Gautama

00:17.440 --> 00:22.840
Buddha teaching instead of lineage in tradition?

00:22.840 --> 00:27.120
May I start with a short command on it?

00:27.120 --> 00:38.840
This is because we have defiled minds, just that we need to be right, that we have the wanting

00:38.840 --> 00:43.760
to be right, the wanting to know better than others.

00:43.760 --> 00:54.960
This is how a lineage and tradition evolves, that some people meet and say, let's do it that

00:54.960 --> 01:02.480
way, and everything who does otherwise is out of that lineage, is out of that tradition.

01:02.480 --> 01:08.000
And then they start their own tradition and their own lineage according to what they think

01:08.000 --> 01:17.160
is right, and the thing is that they insist on both of course, both groups or all the

01:17.160 --> 01:26.920
groups that come up insist that they are right.

01:26.920 --> 01:38.760
So since the Buddha, Gautama Buddha is not living anymore, there is no being that can say,

01:38.760 --> 01:46.400
this is the truth, this is what has been said, this is what the Buddha said.

01:46.400 --> 01:56.160
So but now are many, many people with defiled, more or less defiled minds there, telling

01:56.160 --> 02:01.680
this has been said, that has been said, and want to be right.

02:01.680 --> 02:09.640
So that's why there are lineages and lineages and traditions.

02:09.640 --> 02:21.120
If we could agree on what were the Buddha's teaching or what were the practice of meditation

02:21.120 --> 02:29.040
that the Buddha really told to practice, then there would probably be only one tradition

02:29.040 --> 02:31.000
and one lineage.

02:31.000 --> 02:41.680
I'd like to add a couple of things, and then as it goes a little bit beyond that, because

02:41.680 --> 02:48.960
you have to understand, like in reference, for example, the Tibetan Buddhism, many aspects

02:48.960 --> 02:52.640
of Tibetan Buddhism, I don't know about all of it, and I always don't like to talk about

02:52.640 --> 02:57.320
even a country's Buddhism, but many of the types of Buddhism that you find in Tibet don't

02:57.320 --> 03:06.960
even refer so much to the teachings of the Buddha, or about following the teaching the

03:06.960 --> 03:15.680
45 years of the Buddha's teaching, they will instead strive to become Buddhas by themselves,

03:15.680 --> 03:17.160
and still call this Buddhism.

03:17.160 --> 03:26.680
So it even goes beyond simply disagreement over what the Buddha said, it's disagreement over

03:26.680 --> 03:30.040
in what way we relate to the Buddha.

03:30.040 --> 03:36.440
And so you might say, well, that's just because of defilements, but then it really goes

03:36.440 --> 03:37.440
by.

03:37.440 --> 03:51.080
So at the very least, there's room on a subjective level to suggest that there are different

03:51.080 --> 03:53.440
ways of approaching the Buddha.

03:53.440 --> 03:58.000
So you can say, you talk about the Gautama Buddha's teaching, well, some people don't

03:58.000 --> 03:59.400
see that as the most important.

03:59.400 --> 04:04.640
They take Gautama's Buddha's example as the core of Buddhism, and not even, no, and actually

04:04.640 --> 04:07.120
many of them will not even refer to Gautama Buddha.

04:07.120 --> 04:12.040
In fact, there are schools of Buddhism that don't refer so much, anyway, to him, they refer

04:12.040 --> 04:15.880
to Avalokitesh for a Buddha.

04:15.880 --> 04:21.960
Amitafas in China, they call him, who is in the Western Pure Land, who, if you chant his

04:21.960 --> 04:25.760
name with the pure mind, he'll be born in his realm and become a Buddha there.

04:25.760 --> 04:30.440
So, I mean, someone from our tradition might just say, well, that's just because of the

04:30.440 --> 04:33.800
defilements of those people and the ignorance that they say that.

04:33.800 --> 04:38.240
But subjectively, there is room for different interpretations.

04:38.240 --> 04:43.840
And so on one level, someone can say, well, yes, the Buddha taught all this, but rather

04:43.840 --> 04:48.440
than follow his teachings, I'm going to become a Buddha.

04:48.440 --> 04:53.720
And they might, therefore, start a tradition, encouraging other people to do that and

04:53.720 --> 04:58.040
saying, yeah, that's all the kombudas together, which seems to be the case in certain

04:58.040 --> 05:13.680
instances, where this is even more clearly the case is within a harmonious, sort of

05:13.680 --> 05:19.480
super tradition.

05:19.480 --> 05:20.120
So, within a group of people who follow the same text, who follow the same teachings

05:20.120 --> 05:29.040
and who can agree on what are the Buddha's teachings, I would say, I would submit the argument

05:29.040 --> 05:33.560
that it still might be possible to have different traditions and lineages, like you might

05:33.560 --> 05:41.720
consider Sariputas lineage to be a specific type of practice that was suitable for people

05:41.720 --> 05:48.680
who had high intelligence and Mogulana's lineage for those people who were more inclined

05:48.680 --> 05:49.680
towards magical powers.

05:49.680 --> 05:54.800
You know, there's the story of the Buddha upon Gijakuta, Vulture's peak, and looking down

05:54.800 --> 05:56.560
and watching the monks go and arms round.

05:56.560 --> 06:04.800
And he said, you see those monks following Sariputta, that's because they're all really

06:04.800 --> 06:07.160
engaged in the development of wisdom.

06:07.160 --> 06:10.960
And you see those monks following Mogulana, that's because the reason they follow him

06:10.960 --> 06:16.200
is because they are very keen on magical powers.

06:16.200 --> 06:20.240
And you see those ones following Ananda, that's because they're all, those monks are all

06:20.240 --> 06:25.200
Bajusutta, they're all ones who are keen on memorizing the texts.

06:25.200 --> 06:30.720
And you see those ones following Devadatta, that's because they're all corrupt and evil.

06:30.720 --> 06:37.800
And so he said something like birds of a feather flock together.

06:37.800 --> 06:47.680
And so, while you could consider that, for example, in our tradition, we call, even let's

06:47.680 --> 06:52.360
say within the Mahasi Sayada tradition, there's group of people who follow the same type

06:52.360 --> 06:53.360
of practice.

06:53.360 --> 06:58.120
Because you could make the argument that we don't find anything wrong with, say, Adjan

06:58.120 --> 07:07.680
Chaz, tradition, they have a perfectly adequate and successful interpretation of the Buddha

07:07.680 --> 07:10.800
's teaching or tradition of practice based on the Buddha's teaching.

07:10.800 --> 07:11.800
We could say that.

07:11.800 --> 07:12.800
I don't really know.

07:12.800 --> 07:15.400
Could be the case, maybe not.

07:15.400 --> 07:18.720
We certainly don't attack them or say they're wrong.

07:18.720 --> 07:23.400
And we say they're still the same, they take the same body of texts.

07:23.400 --> 07:28.920
And they seem to be practicing in a way that is in line with those texts.

07:28.920 --> 07:35.520
But even within the Mahasi Sayada group, every monastery that you go to, not even every country,

07:35.520 --> 07:40.760
every monastery that you go to, even within a monastery, within the monastery that I say

07:40.760 --> 07:47.600
that, every teacher in that monastery had a different way of teaching.

07:47.600 --> 07:51.040
And you could cynically say that it's because of the defilements of all of them, but

07:51.040 --> 07:55.440
one, all but my teacher, everybody else is just because they have defilements.

07:55.440 --> 08:00.800
But I don't think it necessarily has to be the case because the other argument was that

08:00.800 --> 08:04.240
it has to do with the needs of the students or the nature of the students.

08:04.240 --> 08:08.520
But it can also have to do with the, I would look at the teacher.

08:08.520 --> 08:12.680
So for example, the teacher of Tuchabotilla, a really good example.

08:12.680 --> 08:14.000
How did he teach Tuchabotilla?

08:14.000 --> 08:19.400
This monk who thought he was the best of the best because he could memorize the whole

08:19.400 --> 08:20.400
to Pitika?

08:20.400 --> 08:24.000
He said, go run to the pond.

08:24.000 --> 08:27.480
And so he had this guy with all his robes on, go and run to the middle of pond and floating

08:27.480 --> 08:29.840
there in the pond and he'll come back out.

08:29.840 --> 08:32.120
And then he sent them back down into the pond again.

08:32.120 --> 08:33.120
Why?

08:33.120 --> 08:34.120
Because he was seven years old.

08:34.120 --> 08:35.120
He was seven year old.

08:35.120 --> 08:36.120
I think he was an Arahant.

08:36.120 --> 08:42.120
This seven year old Arahant, but this is how they think they, they run to the pond.

08:42.120 --> 08:48.240
And then he's taught him in the middle of the pond about, but this, uh, there's Antil.

08:48.240 --> 08:50.280
And the lizard goes into the Antil.

08:50.280 --> 08:52.280
What do you do to get it out?

08:52.280 --> 08:56.200
Plug up all the holes but one and then watch that hole and grab it.

08:56.200 --> 08:59.920
And he said in the same way, this is how you watch your mind.

08:59.920 --> 09:04.920
You close all the other senses and just watch the mind wherever the mind goes.

09:04.920 --> 09:09.040
The means don't worry about what you see, what you hear, what you smell.

09:09.040 --> 09:10.040
Just look at where the mind is.

09:10.040 --> 09:13.160
If the mind goes to the eye, then you just see your hearing.

09:13.160 --> 09:17.960
So you close them all and you're only concerned with where the mind is going, not with

09:17.960 --> 09:20.280
anything else.

09:20.280 --> 09:27.360
So, so totally different way of teaching from, from, you might say another Arahant.

09:27.360 --> 09:29.680
The technique would be quite different.

09:29.680 --> 09:36.080
So you have, you know, some, some people in Thailand who, we have this practice of mindful

09:36.080 --> 09:40.640
frustration and my teacher teaches it one way, but I went to another center and they teach

09:40.640 --> 09:41.640
in another way.

09:41.640 --> 09:43.040
They have this really kind of crazy.

09:43.040 --> 09:45.920
I mean, I don't, I don't go for it to me, it seems crazy, but they have another way of

09:45.920 --> 09:46.920
doing mindful frustration.

09:46.920 --> 09:48.320
There's nothing wrong with it.

09:48.320 --> 09:52.800
It's kind of crazy, but it's still, it's a lot, I did it and I was doing it and you can

09:52.800 --> 09:55.320
be mindful just the same.

09:55.320 --> 09:57.680
We do these different parts of the walking step.

09:57.680 --> 10:01.240
And some people say you do lifting, placing, lifting straight up.

10:01.240 --> 10:04.280
Some people say you lift forward and then place it straight down.

10:04.280 --> 10:12.960
So there's a million different ways of, of, of practicing the teaching and probably as

10:12.960 --> 10:18.560
Paul and you said, many of them just come from defilements, but there is certainly room

10:18.560 --> 10:29.320
for a subjective reason, which is that simply because we're, we're all, we're all coming

10:29.320 --> 10:37.200
from different places that all, even all, I'm not, not to speak of us students, but to

10:37.200 --> 10:43.200
speak of, say, the hypothetical hour hunts, those people who are, who are fully enlightened,

10:43.200 --> 10:45.120
they're still quite different.

10:45.120 --> 10:50.800
Even Buddhas are different in certain respects, most respects are the same, but some Buddhas

10:50.800 --> 10:53.320
don't teach the party mocha, for example.

10:53.320 --> 10:57.760
So would you say, so, but you, you can't say that, that it's because of the defilements of

10:57.760 --> 11:02.000
that Buddha that he didn't teach the party mocha is because of his situation and because

11:02.000 --> 11:07.360
of the students and that came to him and so on.

11:07.360 --> 11:09.560
What is the same will be certain concepts.

11:09.560 --> 11:14.000
So the, what will be based on defilements is whether they're actually teaching based on

11:14.000 --> 11:18.840
the four noble truths, but to just sum upada, whether they're, they're clear in terms

11:18.840 --> 11:19.840
of causing the fact.

11:19.840 --> 11:24.240
So if someone starts to say that, know the Buddha taught that our hunts have angry

11:24.240 --> 11:31.800
anger and delusion, then you say defilements, and you can check that one out, this is wrong.

11:31.800 --> 11:35.040
But if someone says, well, you should prostrate like this, or you should prostrate like

11:35.040 --> 11:39.120
that, or you should do Anapana Santi, or you should, you know, you should watch at

11:39.120 --> 11:42.000
the nose, or you should watch at the stomach, or someone.

11:42.000 --> 11:50.280
There's room for all of that, I think, in the differences of, we saw what it was, or

11:50.280 --> 11:56.520
Wasana, which means the things that we bring into this life.

11:56.520 --> 12:09.280
Maybe even the, the religions of, of a country before Buddhism came to that country still

12:09.280 --> 12:19.040
have a, an influence on, on how it developed, like in Tibet there, I heard was kind

12:19.040 --> 12:32.880
of ghost religion with shamanism called, and that, that, that there still has an influence

12:32.880 --> 12:43.200
and in China, there was something different going on, so I think this can be taken in, in

12:43.200 --> 13:10.440
consideration as well.

