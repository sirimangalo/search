1
00:00:00,000 --> 00:00:05,000
Is violence ever justified in Buddhism?

2
00:00:05,000 --> 00:00:13,000
Yes, I think it is justified in certain cases.

3
00:00:13,000 --> 00:00:20,000
First, it's justified in self-defense, to an extent,

4
00:00:20,000 --> 00:00:29,000
to a limited degree, someone is threatening your life,

5
00:00:29,000 --> 00:00:34,000
then you might do violence to them, to stop them.

6
00:00:34,000 --> 00:00:37,000
Without the intention to hurt them,

7
00:00:37,000 --> 00:00:43,000
the intention to stop what they're doing.

8
00:00:43,000 --> 00:00:52,000
I would say this is not a harmful act.

9
00:00:52,000 --> 00:00:54,000
It's actually a beneficial act.

10
00:00:54,000 --> 00:00:59,000
If it's in certain cases possible to stop someone

11
00:00:59,000 --> 00:01:03,000
without doing violence,

12
00:01:03,000 --> 00:01:08,000
without intending to bring harm to them.

13
00:01:08,000 --> 00:01:10,000
It simply just stops them.

14
00:01:10,000 --> 00:01:15,000
So example, you might knock them out, for example.

15
00:01:15,000 --> 00:01:19,000
Suppose someone is attacking you

16
00:01:19,000 --> 00:01:21,000
or attacking someone else who hit them over the head

17
00:01:21,000 --> 00:01:23,000
and knock them out.

18
00:01:23,000 --> 00:01:29,000
You could say that's violent, but it wasn't a bad thing

19
00:01:29,000 --> 00:01:33,000
for them to do that, for you to do that.

20
00:01:33,000 --> 00:01:36,000
Putting people in jail might be seen as a violent act.

21
00:01:36,000 --> 00:01:38,000
You might think, well, in certain cases,

22
00:01:38,000 --> 00:01:40,000
it's beneficial both to them and to other people

23
00:01:40,000 --> 00:01:42,000
to put them in jail.

24
00:01:42,000 --> 00:01:46,000
As an example, there's obviously broader implications

25
00:01:46,000 --> 00:01:48,000
with things like jail, especially.

26
00:01:48,000 --> 00:01:52,000
But ostensibly, there's nothing wrong with it.

27
00:01:52,000 --> 00:01:56,000
Now, I don't have any real backing for that claim.

28
00:01:56,000 --> 00:02:02,000
It might be that it's totally a false claim.

29
00:02:02,000 --> 00:02:05,000
But the backing that we have is that monks

30
00:02:05,000 --> 00:02:11,000
are allowed to hit someone in self-defense.

31
00:02:11,000 --> 00:02:17,000
This is actually allowed for monks to perform violent acts

32
00:02:17,000 --> 00:02:21,000
in self-defense, to the extent necessary to get away

33
00:02:21,000 --> 00:02:26,000
from the person, the aggressor.

34
00:02:26,000 --> 00:02:31,000
So whether or not intrinsically the mind is defiled

35
00:02:31,000 --> 00:02:34,000
by doing the act, I mean, you might at the very least say

36
00:02:34,000 --> 00:02:37,000
that this is still an attachment to one's own health

37
00:02:37,000 --> 00:02:41,000
and well-being, which an arahant might not have.

38
00:02:41,000 --> 00:02:44,000
So you might say that an arahant wouldn't perform

39
00:02:44,000 --> 00:02:47,000
such acts in self-defense, but I'm not convinced.

40
00:02:47,000 --> 00:02:50,000
It seems to me that it's even to that extent

41
00:02:50,000 --> 00:02:55,000
it's still, I don't have an answer, a definite answer

42
00:02:55,000 --> 00:02:58,000
in my mind, because it seems to me it could be done simply

43
00:02:58,000 --> 00:03:01,000
because it's proper.

44
00:03:01,000 --> 00:03:04,000
Simply because it is the proper response

45
00:03:04,000 --> 00:03:08,000
to a certain experience, the natural response.

46
00:03:08,000 --> 00:03:10,000
And it might be unnatural to allow oneself

47
00:03:10,000 --> 00:03:13,000
to be killed, for example.

48
00:03:13,000 --> 00:03:19,000
But for sure, even if it is intrinsically defiling

49
00:03:19,000 --> 00:03:23,000
to act in such a way, it's certainly not defiling to the extent

50
00:03:23,000 --> 00:03:36,000
that it is rejected in Buddhism or forbidden.

51
00:03:36,000 --> 00:03:39,000
Now, the other way that violence might,

52
00:03:39,000 --> 00:03:41,000
so I actually talk about them.

53
00:03:41,000 --> 00:03:42,000
So one way is in self-defense.

54
00:03:42,000 --> 00:03:45,000
The other way is that it might actually help the individual.

55
00:03:45,000 --> 00:03:49,000
Now, as I said, I don't have a doctrinal backing

56
00:03:49,000 --> 00:03:53,000
for this exactly, and it's a bit sketchy,

57
00:03:53,000 --> 00:03:58,000
but it seems to me that if you can be sure,

58
00:03:58,000 --> 00:04:03,000
or if you're clear in the mind that it's either

59
00:04:03,000 --> 00:04:06,000
in self-defense or for the benefit of the person

60
00:04:06,000 --> 00:04:10,000
to whom you're doing violence, then I think there is room

61
00:04:10,000 --> 00:04:15,000
for the view that it is justified.

62
00:04:15,000 --> 00:04:18,000
Not saying that it is because I'm not the Buddha

63
00:04:18,000 --> 00:04:22,000
who can justify it, but I'm also not the Buddha

64
00:04:22,000 --> 00:04:24,000
who can denounce or forbid it.

65
00:04:24,000 --> 00:04:27,000
So there is still an opening there.

66
00:04:27,000 --> 00:04:31,000
But it's never justified to hurt other people

67
00:04:31,000 --> 00:04:37,000
for your own benefit or to hurt other people

68
00:04:37,000 --> 00:04:40,000
of greed, anger, or delusion,

69
00:04:40,000 --> 00:04:46,000
to hurt other people outside of these two reasons,

70
00:04:46,000 --> 00:04:48,000
to actually harm someone,

71
00:04:48,000 --> 00:04:56,000
to actually do something to make their life less,

72
00:04:56,000 --> 00:05:01,000
less peaceful.

73
00:05:01,000 --> 00:05:06,000
This is never justified,

74
00:05:06,000 --> 00:05:08,000
never to.

