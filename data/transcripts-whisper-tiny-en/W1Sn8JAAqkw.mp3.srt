1
00:00:00,000 --> 00:00:08,000
Peter asking, I came across a teaching that involves concentrating on the 32 parts of the body individually.

2
00:00:08,000 --> 00:00:18,000
Is this a method you would suggest? If it is, would it be possible to create a video similar to the how to meditate videos?

3
00:00:18,000 --> 00:00:26,000
I did a video explaining one way of understanding this sort of meditation. It's the protective meditation.

4
00:00:26,000 --> 00:00:34,000
This video I did a long time ago, or some years ago about the protective meditations.

5
00:00:34,000 --> 00:00:42,000
These four protective meditations, and these are things that in addition to the practice of insight meditation practice of trying to see things clearly,

6
00:00:42,000 --> 00:00:49,000
you can use conceptual objects from time to time to support your insight meditation, your practice of insight meditation.

7
00:00:49,000 --> 00:00:57,000
So, the first one is mindfulness of the Buddha. Thinking about the Buddha, just remembering the Buddha will give you confidence.

8
00:00:57,000 --> 00:01:06,000
The second one is mindfulness. The second one is I don't know the order, but anyway.

9
00:01:06,000 --> 00:01:12,000
The next one is loving kindness. So, the cultivation of love for all beings.

10
00:01:12,000 --> 00:01:18,000
So, at the end of our meditations will often send loving kindness and dedicate the merit to other people.

11
00:01:18,000 --> 00:01:29,000
From time to time sending good thoughts to the person people you love, to the people that you hate, and so on, to try to level out your thoughts about them.

12
00:01:29,000 --> 00:01:36,000
And to release and to be free from your thoughts of anger, your thoughts of attachment, your thoughts of worry about people.

13
00:01:36,000 --> 00:01:44,000
The third one is Asubha, which is this one that you're talking about, where a person reflects on the disgusting nature of the body, repulsive nature of the body parts.

14
00:01:44,000 --> 00:01:56,000
This helps to give up lust, helps us to give up our attachments to sexuality, our attachments to our bodies, attachments to food, and so on.

15
00:01:56,000 --> 00:02:07,000
And this helps to just to give up greed in general. So, loving kindness, meditation to give up anger, this one to give up lust or greed.

16
00:02:07,000 --> 00:02:20,000
And the fourth one is death, mindfulness of death. So, from time to time to consider the inevitability of death, and that we don't know whether death might come tomorrow or might come in the next moment.

17
00:02:20,000 --> 00:02:36,000
Reflecting on this from time to time helps to give you encouragement in the practice. So, this is how I would encourage people to use these meditations because I don't teach a practice that includes intensive development of the 32 parts of the body.

18
00:02:36,000 --> 00:02:46,000
You're welcome to undertake it. I would recommend undertaking it with a qualified teacher because given that those are conceptual objects, it's easy to get off track.

19
00:02:46,000 --> 00:03:00,000
There was a story I think of some monks who developed this and became totally averse to their existence and to their life and wanted to kill themselves.

20
00:03:00,000 --> 00:03:07,000
So, for the wrong people, it can be the wrong sort of meditation, or it can be a meditation that can have potential consequences.

21
00:03:07,000 --> 00:03:18,000
That should be carefully developed by the teacher or carefully observed by the teacher. Now, I don't teach this sort of meditation.

22
00:03:18,000 --> 00:03:25,000
So, I could think about doing a video for it, but I probably wouldn't in the end or decide not to.

23
00:03:25,000 --> 00:03:33,000
I might think to do a video for all four of them, but I did a video for all four of them, and it wasn't guided. It was just explaining how to do it.

24
00:03:33,000 --> 00:03:39,000
It might be nice to do a guided meditation, including all four of them, or one for each of the four.

25
00:03:39,000 --> 00:03:53,000
With the emphasis on the fact that this isn't meant to be a full-time meditation, it's just something that from time to time can be useful in various situations.

26
00:03:53,000 --> 00:04:04,000
So, we have a question about the 32 parts of the body when we recommend it.

