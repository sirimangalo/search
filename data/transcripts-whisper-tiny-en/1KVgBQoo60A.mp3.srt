1
00:00:00,000 --> 00:00:14,000
Okay, good evening, everyone.

2
00:00:14,000 --> 00:00:25,480
Welcome to our evening demo.

3
00:00:25,480 --> 00:00:33,480
The last night, one of the things that came up was friendship.

4
00:00:33,480 --> 00:00:42,480
Friendship was a good friendship with the evil, good friends, bad friends.

5
00:00:42,480 --> 00:00:51,480
That in due to spend a little bit of time talking about friendship, according to Buddhism.

6
00:00:51,480 --> 00:00:58,480
Friendship's a curious thing.

7
00:00:58,480 --> 00:01:08,480
It's a friendship in many cases.

8
00:01:08,480 --> 00:01:18,480
Something that misleads us from right and wrong, from good and bad.

9
00:01:18,480 --> 00:01:23,480
We find ourselves caught up with people because they're my friend.

10
00:01:23,480 --> 00:01:25,480
It's a curious phrase.

11
00:01:25,480 --> 00:01:30,480
It's a curious excuse or reason for doing something.

12
00:01:30,480 --> 00:01:38,480
Now if we mean that we owe them something dead or gratitude perhaps.

13
00:01:38,480 --> 00:01:41,480
That's reasonable.

14
00:01:41,480 --> 00:01:48,480
But it tends to be more than that we identify with individuals.

15
00:01:48,480 --> 00:01:56,480
For a Buddhist perspective, it's quite possible that we've had some connection coming from past lives.

16
00:01:56,480 --> 00:01:58,480
With certain people.

17
00:01:58,480 --> 00:02:06,480
You notice how sometimes you're just unable to make a connection with people.

18
00:02:06,480 --> 00:02:12,480
I think that someone would like to get to know, but maybe they don't have anything to do with you.

19
00:02:12,480 --> 00:02:20,480
Look at someone and there's just no connection.

20
00:02:20,480 --> 00:02:23,480
Just faces in the crowd.

21
00:02:23,480 --> 00:02:25,480
When you talk to them, there's no connection.

22
00:02:25,480 --> 00:02:33,480
But other people, you find yourself caught up with them perhaps your whole life.

23
00:02:33,480 --> 00:02:41,480
And you identify with them as being a friend.

24
00:02:41,480 --> 00:02:46,480
Even with a debt of gratitude though.

25
00:02:46,480 --> 00:02:55,480
I don't think there's any reason we should let that color the way we relate to others.

26
00:02:55,480 --> 00:03:01,480
Let people drag us down the wrong path just because they've done good for us in the past.

27
00:03:01,480 --> 00:03:06,480
We don't want to abandon them.

28
00:03:06,480 --> 00:03:07,480
Certainly not the right way.

29
00:03:07,480 --> 00:03:08,480
It doesn't help us.

30
00:03:08,480 --> 00:03:13,480
It doesn't help them.

31
00:03:13,480 --> 00:03:25,480
I even spend all our time in energy trying to help an individual who doesn't want to be helped.

32
00:03:25,480 --> 00:03:31,480
They're going down a destructive path and spend all your energy trying to dissuade them.

33
00:03:31,480 --> 00:03:47,480
Even when the evidence clearly shows that they're not to be dissuaded from their path.

34
00:03:47,480 --> 00:03:50,480
Friendship is taken quite seriously.

35
00:03:50,480 --> 00:04:02,480
It's something important in Buddhism, something you have to see in its proper light.

36
00:04:02,480 --> 00:04:06,480
Friendship is two things.

37
00:04:06,480 --> 00:04:10,480
First of all, friendship involves friendliness.

38
00:04:10,480 --> 00:04:12,480
And ideally we're friendly towards everyone.

39
00:04:12,480 --> 00:04:15,480
It would be nice if we could all be friends.

40
00:04:15,480 --> 00:04:18,480
There's no question about that.

41
00:04:18,480 --> 00:04:21,480
We should be a good friend.

42
00:04:21,480 --> 00:04:23,480
Really to everyone.

43
00:04:23,480 --> 00:04:30,480
And having the attitude of being friendly towards all people I think is perfectly valid.

44
00:04:30,480 --> 00:04:41,480
There's never a reason to be cruel or mean or unkind to others.

45
00:04:41,480 --> 00:04:50,480
Sometimes your kindness takes the form of limiting your engagement with people for their benefit,

46
00:04:50,480 --> 00:05:00,480
for your benefit, for the general goodness of you both.

47
00:05:00,480 --> 00:05:02,480
But it should never be cruel or mean.

48
00:05:02,480 --> 00:05:11,480
And sometimes karma and past life relationships are most of us.

49
00:05:11,480 --> 00:05:22,480
It means we have to be involved with people who wouldn't otherwise be inclined to be involved with.

50
00:05:22,480 --> 00:05:28,480
And in fact, that makes it all the more important to be kind and to be friendly.

51
00:05:28,480 --> 00:05:34,480
It'll be a source for constant or common conflict.

52
00:05:34,480 --> 00:05:42,480
If we have bad karma with someone, our inclination is to constantly be in conflict with them.

53
00:05:42,480 --> 00:05:46,480
Then that should be a very important part of our meditation practice.

54
00:05:46,480 --> 00:05:49,480
It's learning to overcome that conflict.

55
00:05:49,480 --> 00:05:58,480
Learning to change the course of our journey and some sorrow so that we don't get caught up.

56
00:05:58,480 --> 00:06:11,480
And the same cycles of vengeance and conflict that we were caught up in before.

57
00:06:11,480 --> 00:06:19,480
But the second thing is that it's important to be careful.

58
00:06:19,480 --> 00:06:30,480
Who is a company and who is a friendship we cultivate.

59
00:06:30,480 --> 00:06:36,480
For some people we be a good friend and that can be great if we have something to provide to them.

60
00:06:36,480 --> 00:06:44,480
And if they are keen to improve through our friendship, that's great.

61
00:06:44,480 --> 00:06:53,480
Then there are people who seek only to drag us down.

62
00:06:53,480 --> 00:07:02,480
Who is very nature is detrimental to themselves and to those who would call them friends.

63
00:07:02,480 --> 00:07:18,480
And those people who have to be careful, who can be compassionate and kind, but should never consider them as proper friends.

64
00:07:18,480 --> 00:07:32,480
So the Buddha talked about in the Deganikaya, he talked about different types of friends, those who appear to be friends, but are not really friends.

65
00:07:32,480 --> 00:07:40,480
And these are the most common mistaken friends that we come across.

66
00:07:40,480 --> 00:07:56,480
If someone is unfriendly and someone is clearly not your friend, you're not likely to see them as your friend, but there are ways by which, as I said, we mistakenly come into friendship.

67
00:07:56,480 --> 00:08:20,480
And so the Buddha outlined a number of these, he talked about the friend who, while he only takes the taker.

68
00:08:20,480 --> 00:08:42,480
So people who are, from people who are constantly after you, for benefit, for their own benefit.

69
00:08:42,480 --> 00:08:52,480
And you can see this, you can see the insincereity and the mental illness involved.

70
00:08:52,480 --> 00:08:56,480
It's quite pitiful, really.

71
00:08:56,480 --> 00:09:07,480
When people are not able to give, it's a common mental illness.

72
00:09:07,480 --> 00:09:13,480
I phrase it in this way, because normally we think, well, that person is just selfish and mean and we get very upset at them.

73
00:09:13,480 --> 00:09:15,480
But you really shouldn't be.

74
00:09:15,480 --> 00:09:31,480
If you should pity such a person, a person who constantly wants, who constantly takes.

75
00:09:31,480 --> 00:09:44,480
They take everything to give you a little useless things, useless help, and expect much more in response.

76
00:09:44,480 --> 00:09:45,480
They help you.

77
00:09:45,480 --> 00:09:49,480
When they do help you, they help you out of fear.

78
00:09:49,480 --> 00:09:55,480
They do at home, not because they're keen at all or interested at all in your benefit.

79
00:09:55,480 --> 00:10:05,480
Because they know if they feel that they have to, in order to get, in order to get from you.

80
00:10:05,480 --> 00:10:10,480
And the mooch, someone to be pitied.

81
00:10:10,480 --> 00:10:12,480
But pitied by giving them whatever they want.

82
00:10:12,480 --> 00:10:20,480
Of course, the worst thing you can do for someone, anyone is giving them whatever they want.

83
00:10:20,480 --> 00:10:26,480
They understand that this is a person who needs a different kind of help.

84
00:10:26,480 --> 00:10:29,480
Probably need to be cut off.

85
00:10:29,480 --> 00:10:32,480
It's only a person that drags you down.

86
00:10:32,480 --> 00:10:39,480
You don't help them by giving them everything.

87
00:10:39,480 --> 00:10:42,480
The second one is a person who talks.

88
00:10:42,480 --> 00:10:53,480
The entire day they see the pool, which means they're only good in the speech.

89
00:10:53,480 --> 00:11:03,480
They never actually do anything friendly or helpful to their friends.

90
00:11:03,480 --> 00:11:07,480
They talk about good deeds they've done for you in their past.

91
00:11:07,480 --> 00:11:11,480
They remind you of all the good things they've done for you.

92
00:11:11,480 --> 00:11:16,480
And they promised Grandma Promise is about the future.

93
00:11:16,480 --> 00:11:20,480
It's funny, there was a man who did this to me in Thailand.

94
00:11:20,480 --> 00:11:22,480
We were in Thailand.

95
00:11:22,480 --> 00:11:29,480
This was then how many years ago.

96
00:11:29,480 --> 00:11:31,480
Not so many years ago.

97
00:11:31,480 --> 00:11:34,480
And they all talked such a good game.

98
00:11:34,480 --> 00:11:40,480
He told me about all the millions of dollars that he used to have and that he was going to have.

99
00:11:40,480 --> 00:11:42,480
I promised.

100
00:11:42,480 --> 00:11:43,480
I helped him.

101
00:11:43,480 --> 00:11:47,480
I was helping him get into being a monk.

102
00:11:47,480 --> 00:11:48,480
He was a temporary monk.

103
00:11:48,480 --> 00:11:51,480
And he said, oh, when I just rode, I'm going to make all this money.

104
00:11:51,480 --> 00:11:55,480
And I'm going to give you a field due of meditation center in Canada.

105
00:11:55,480 --> 00:11:57,480
And talk to him.

106
00:11:57,480 --> 00:11:59,480
And I only really have believed him.

107
00:11:59,480 --> 00:12:00,480
But I thought he was sincere.

108
00:12:00,480 --> 00:12:04,480
I just thought, oh yeah, well, who knows the future.

109
00:12:04,480 --> 00:12:05,480
But it's like great.

110
00:12:05,480 --> 00:12:07,480
Well, if it happens, it happens.

111
00:12:07,480 --> 00:12:13,480
But turns out he was just, it was a tonal scam, really.

112
00:12:13,480 --> 00:12:16,480
He was really just, I don't know.

113
00:12:16,480 --> 00:12:22,480
I was bizarre, because I never ended up giving him and didn't have anything great to give him.

114
00:12:22,480 --> 00:12:30,480
But, you know, in the end, he just turned out to be his way of using me.

115
00:12:30,480 --> 00:12:35,480
And it seems like the thing sort of thing that he does, really interesting case.

116
00:12:35,480 --> 00:12:38,480
Beautiful, really.

117
00:12:38,480 --> 00:12:48,480
It's unpleasant that people get in this state where they will manipulate their fellow humans.

118
00:12:48,480 --> 00:12:51,480
I mean, this is, of course, common for me living in the monastery.

119
00:12:51,480 --> 00:12:55,480
I don't see it nearly as often as most.

120
00:12:55,480 --> 00:13:02,480
But we get so what's sort of inured to it, or in desensitized to it.

121
00:13:02,480 --> 00:13:06,480
When we think it's about, you know, just the way people behave, it's how to, you know,

122
00:13:06,480 --> 00:13:20,480
you can't get by in the world without it, which is really unfortunate, because it's such a insane way to live.

123
00:13:20,480 --> 00:13:27,480
How can you possibly think that this is the right way to be the right way to go?

124
00:13:27,480 --> 00:13:33,480
Clearly, people who don't have a sense of clarity of mind.

125
00:13:33,480 --> 00:13:40,480
If some people just seem to be unable, even though they might pick up their practice of meditation,

126
00:13:40,480 --> 00:13:44,480
they'd be unable to straighten out their minds.

127
00:13:44,480 --> 00:13:57,480
It's like those Nigerian print scams.

128
00:13:57,480 --> 00:14:04,480
They'd say, give me some money, and I'll give you a lot more.

129
00:14:04,480 --> 00:14:13,480
I mean, our first inclination is to get angry at these people, but it's just so incredibly sad and pitiful that they would.

130
00:14:13,480 --> 00:14:15,480
No, they did that way.

131
00:14:15,480 --> 00:14:23,480
The third is the flatter.

132
00:14:23,480 --> 00:14:26,480
This should be recognizable to many of you.

133
00:14:26,480 --> 00:14:32,480
This is a person who tells you what a great friend you are, and how wonderful you are, and praises you.

134
00:14:32,480 --> 00:14:36,480
And somehow thinks that that's worth something, and therefore they should get something in return, right?

135
00:14:36,480 --> 00:14:38,480
Usually that's how this goes.

136
00:14:38,480 --> 00:14:43,480
They feel like flattering you, by flattering you.

137
00:14:43,480 --> 00:14:50,480
Well, even just flattering you, that makes them a good friend.

138
00:14:50,480 --> 00:14:54,480
So they'll praise your good deeds, they'll praise your evil deeds.

139
00:14:54,480 --> 00:14:58,480
The good things you do, they'll praise is very dangerous.

140
00:14:58,480 --> 00:15:03,480
It's clearly a sign of some mental imbalance or some problem.

141
00:15:03,480 --> 00:15:05,480
You might even recognize this in yourselves.

142
00:15:05,480 --> 00:15:16,480
Sometimes flattering others, feeling that it makes us a better friend, feeling that somehow that'll make them like us more.

143
00:15:16,480 --> 00:15:18,480
It doesn't mean you shouldn't compliment people.

144
00:15:18,480 --> 00:15:27,480
In fact, it is a great thing to appreciate other people's good deeds, but it should only be their good deeds, right?

145
00:15:27,480 --> 00:15:34,480
You know you're a flatter, or you know someone's a flatter when they praise the bad things you do.

146
00:15:34,480 --> 00:15:43,480
When they praise you to your face and disparage you behind your back, it's a sign that they're just a flatter.

147
00:15:43,480 --> 00:15:48,480
You know, there's no substance or any wisdom behind what they say.

148
00:15:48,480 --> 00:15:57,480
There's no goodness behind it either.

149
00:15:57,480 --> 00:16:08,480
The fourth is the person who...

150
00:16:08,480 --> 00:16:19,480
The person who follows you or leads you or is your fellow in...

151
00:16:19,480 --> 00:16:28,480
Your fellow in ruin.

152
00:16:28,480 --> 00:16:31,480
This is the person who...

153
00:16:31,480 --> 00:16:42,480
The friend that we can all remember having was our friend in crime or partnering crime.

154
00:16:42,480 --> 00:16:45,480
When we get drunk they get drunk, they encourage us to get drunk.

155
00:16:45,480 --> 00:16:50,480
We encourage them to get drunk and they encourage us.

156
00:16:50,480 --> 00:17:10,480
And we go wasting our energy and our lives away in gambling and drinking and carrying on friends in debauchery.

157
00:17:10,480 --> 00:17:18,480
That's probably the one that's the worst, the most dangerous. It's what we see probably most often where...

158
00:17:18,480 --> 00:17:26,480
Friendship leads the friends to ruin and lead each other down the wrong path.

159
00:17:26,480 --> 00:17:49,480
It's very dangerous. It's a problem. It's a danger because it's very easy to pick up such friends.

160
00:17:49,480 --> 00:17:56,480
How do we know? And this is the power of friendship. Friendship is about encouraging each other.

161
00:17:56,480 --> 00:18:09,480
It's very easy to get a friendship, to cultivate a friendship, to have friends who will lead you and encourage you to lead them down the wrong path.

162
00:18:09,480 --> 00:18:18,480
And if and when you decide that you've got a moral compass and you start to get an idea of what's right and what's wrong, what's good and what's bad,

163
00:18:18,480 --> 00:18:24,480
to your benefit and to your detriment. And this is something that shakes up a lot of friendships.

164
00:18:24,480 --> 00:18:30,480
I mean, practicing meditation is sure to shake up old friendships.

165
00:18:30,480 --> 00:18:34,480
Because you start to realize that you weren't helping each other.

166
00:18:34,480 --> 00:18:38,480
You weren't bringing happiness, peace, freedom from suffering to each other.

167
00:18:38,480 --> 00:18:53,480
You're only entangling each other up with in conflict and evil for black and a better word.

168
00:18:53,480 --> 00:19:01,480
So those are bad friends. Now positively, the Buddha offered some advice on where how to find good friends.

169
00:19:01,480 --> 00:19:09,480
And gave a list of four good friends. First is someone who's helpful.

170
00:19:09,480 --> 00:19:16,480
And this is a great friend. Someone who supports you.

171
00:19:16,480 --> 00:19:25,480
When you don't notice something's wrong, they protect you.

172
00:19:25,480 --> 00:19:32,480
They protect you. They protect your life. They protect your possessions.

173
00:19:32,480 --> 00:19:38,480
They will be conscious of your benefit.

174
00:19:38,480 --> 00:19:45,480
I mean, that's a rare thing, a great thing. Something to be emulated, something to be respected.

175
00:19:45,480 --> 00:19:53,480
Someone who looks after you, someone who thinks of you and your benefit and your detriment.

176
00:19:53,480 --> 00:20:04,480
And in times of danger, they are a refuge person who you can go to when you have problems.

177
00:20:04,480 --> 00:20:12,480
Someone who goes the extra mile when you ask for help, they give more than what was asked.

178
00:20:12,480 --> 00:20:22,480
They're the sign of a good friend. The sign of someone who is really rare and to be emulated, to be praised, to be associated.

179
00:20:22,480 --> 00:20:30,480
Not just because of what you can get from them, but because this is someone with a good heart that should be emulated.

180
00:20:30,480 --> 00:20:38,480
And if friends, any two friends are like this, then they both of course gain.

181
00:20:38,480 --> 00:20:43,480
They're always helping each other. We both gain.

182
00:20:43,480 --> 00:20:50,480
The second is someone who is unmoved in good times and bad.

183
00:20:50,480 --> 00:20:56,480
Because you've got the fair weather friend, I think, is the word someone who, in good times, they're with you.

184
00:20:56,480 --> 00:21:06,480
But when things go wrong, someone who doesn't stick with you and then go and get stuff.

185
00:21:06,480 --> 00:21:10,480
Someone who doesn't stick with you when problems arise.

186
00:21:10,480 --> 00:21:14,480
Well, this is a person who's not like that.

187
00:21:14,480 --> 00:21:21,480
A person who tells you they're secrets, cards your secrets.

188
00:21:21,480 --> 00:21:26,480
Who does not let you down in misfortune.

189
00:21:26,480 --> 00:21:33,480
Someone who is with you through thick and thin.

190
00:21:33,480 --> 00:21:36,480
Who might even sacrifice their life for you.

191
00:21:36,480 --> 00:21:40,480
And that's sort of a friend that is hard to find.

192
00:21:40,480 --> 00:21:46,480
Someone who cares for you.

193
00:21:46,480 --> 00:21:49,480
Someone who has this wish to see you happy.

194
00:21:49,480 --> 00:21:55,480
And that's friendship is for, right?

195
00:21:55,480 --> 00:22:00,480
That's what true friendship should be for, not like any relationship.

196
00:22:00,480 --> 00:22:05,480
No, what can I get from this person? That doesn't work.

197
00:22:05,480 --> 00:22:13,480
Any two friends who are inclined to wonder what the other person can give to them.

198
00:22:13,480 --> 00:22:22,480
We'll be a friendship of always lack or wanting.

199
00:22:22,480 --> 00:22:26,480
Whereas if both friends are thinking, what can I do for this person?

200
00:22:26,480 --> 00:22:36,480
How can I make them happy? What can I do to bring good to them?

201
00:22:36,480 --> 00:22:38,480
Then both gain.

202
00:22:38,480 --> 00:22:45,480
The third is a friend who points out what is good.

203
00:22:45,480 --> 00:22:53,480
This is the best friend, number three here, but it's really the one that in Buddhism we talk about a good friend.

204
00:22:53,480 --> 00:23:00,480
I mean, someone who points out good, someone who teaches or instructs advisors you.

205
00:23:00,480 --> 00:23:04,480
Someone who you can go to for advice.

206
00:23:04,480 --> 00:23:08,480
Has a good head on their shoulders and much to say about right and wrong,

207
00:23:08,480 --> 00:23:13,480
good and bad, that is a benefit as a value.

208
00:23:13,480 --> 00:23:18,480
Perhaps not someone who preaches, but someone who is good for giving advice.

209
00:23:18,480 --> 00:23:23,480
Someone who has their head on straight, who knows.

210
00:23:23,480 --> 00:23:27,480
Who has things to tell.

211
00:23:27,480 --> 00:23:30,480
They keep you from wrongdoing.

212
00:23:30,480 --> 00:23:37,480
They support you in doing good. They give you encouragement when you do good things.

213
00:23:37,480 --> 00:23:39,480
They tell you things you don't know.

214
00:23:39,480 --> 00:23:55,480
That's a rare friend. Someone who was able to teach you to give you knowledge that you didn't have.

215
00:23:55,480 --> 00:24:05,480
Finally, they lead you to happiness. They lead you on to it as good.

216
00:24:05,480 --> 00:24:14,480
It's the measure of wisdom. So the measure of a friend who is wise

217
00:24:14,480 --> 00:24:22,480
is that the things they tell you bring you peace and happiness, freedom from suffering.

218
00:24:22,480 --> 00:24:29,480
And the fourth type of true friend is the sympathetic friend.

219
00:24:29,480 --> 00:24:32,480
One who sympathizes with you.

220
00:24:32,480 --> 00:24:37,480
You don't rejoice at your misfortune. They rejoice in your good fortune.

221
00:24:37,480 --> 00:24:42,480
Someone who has mudita. True mudita.

222
00:24:42,480 --> 00:24:46,480
Not to someone who says it, but someone who is really happy when they hear.

223
00:24:46,480 --> 00:24:49,480
Someone who you know when you tell them good things that you've done,

224
00:24:49,480 --> 00:24:53,480
good things that have happened to you that they're happy for you.

225
00:24:53,480 --> 00:24:58,480
They stop others who speak against you and someone says something bad about you.

226
00:24:58,480 --> 00:25:09,480
They protect you, they correct them. And when others speak in praise, they agree they commend such people.

227
00:25:09,480 --> 00:25:21,480
It's a fairly simple and it's not a terribly deep teaching, but I think it's important we go over various aspects of Buddhism.

228
00:25:21,480 --> 00:25:28,480
I mean, this is a very practical teaching. I think one of the many of that most of us can relate to.

229
00:25:28,480 --> 00:25:35,480
But the deeper teaching I think is about how this mind, how the mind works in cultivating friendship,

230
00:25:35,480 --> 00:25:43,480
both being friendly and in clinging to people, identifying this is my friend,

231
00:25:43,480 --> 00:25:46,480
which is, of course, problematic. Of course, it's great to be friendly.

232
00:25:46,480 --> 00:25:53,480
It's good, it's wholesome to be friendly towards all, but to cling to someone just because they're a friend.

233
00:25:53,480 --> 00:26:03,480
It's never a good reason for any. It's never a good reason in any way, at any rate, in any case.

234
00:26:03,480 --> 00:26:11,480
Even good people, we shouldn't cling to them just because they're a friend.

235
00:26:11,480 --> 00:26:17,480
They cultivate their friendship, because it's for the ultimate game,

236
00:26:17,480 --> 00:26:22,480
cultivating peace, happiness, freedom from suffering.

237
00:26:22,480 --> 00:26:49,480
So, there you go a little bit on friendship. Thank you all for tuning in.

