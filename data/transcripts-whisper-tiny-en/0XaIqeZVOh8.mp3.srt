1
00:00:00,000 --> 00:00:04,500
Hello and welcome back to our study of the Dhamapada.

2
00:00:04,500 --> 00:00:11,500
Today we continue with verse 226, which reads as follows.

3
00:00:35,500 --> 00:00:41,500
For those who are always awake,

4
00:00:41,500 --> 00:00:45,500
training both day and night,

5
00:00:45,500 --> 00:00:51,500
and for those intent on nibana,

6
00:00:51,500 --> 00:00:58,500
the defilements of the mind come to an end.

7
00:00:58,500 --> 00:01:06,500
This verse was taught in response to a discussion between the Buddha

8
00:01:06,500 --> 00:01:10,500
and a slave woman named Bhuna.

9
00:01:10,500 --> 00:01:13,500
A servant woman or slave, I guess,

10
00:01:13,500 --> 00:01:16,500
I'm not quite sure how you should translate it.

11
00:01:16,500 --> 00:01:19,500
I don't know that there were...

12
00:01:19,500 --> 00:01:21,500
I guess there were slaves at that time.

13
00:01:21,500 --> 00:01:27,500
It was more of an indentured servitude perhaps,

14
00:01:27,500 --> 00:01:30,500
or there was definitely class problems,

15
00:01:30,500 --> 00:01:34,500
and so on, low-class people were...

16
00:01:34,500 --> 00:01:37,500
They turned themselves over as servants,

17
00:01:37,500 --> 00:01:40,500
and I don't know how it worked anyway.

18
00:01:40,500 --> 00:01:46,500
It wasn't a perfect society by any means.

19
00:01:46,500 --> 00:01:52,500
But this woman was assigned to a great amount of work.

20
00:01:52,500 --> 00:01:55,500
There was a festival, perhaps I don't know.

21
00:01:55,500 --> 00:02:00,500
She was assigned to grind up rice into flour,

22
00:02:00,500 --> 00:02:05,500
or some sort of rice-based work.

23
00:02:05,500 --> 00:02:07,500
And so she had to work late into the night,

24
00:02:07,500 --> 00:02:10,500
and it was tiresome.

25
00:02:10,500 --> 00:02:13,500
Late at night in Rajagaha,

26
00:02:13,500 --> 00:02:16,500
Rajagaha has a city that is surrounded by mountains.

27
00:02:16,500 --> 00:02:19,500
So late at night, while she was working,

28
00:02:19,500 --> 00:02:23,500
she looked over and she saw up on the mountain

29
00:02:23,500 --> 00:02:27,500
and Kijakuta, Vulture's Peak,

30
00:02:27,500 --> 00:02:32,500
lights along the traveling along the path,

31
00:02:32,500 --> 00:02:37,500
up and down the mountain.

32
00:02:37,500 --> 00:02:42,500
And it was the monks after listening to the Buddha teach,

33
00:02:42,500 --> 00:02:45,500
or listening to whoever was teaching that day.

34
00:02:45,500 --> 00:02:52,500
Late at night, they would return back to their good day,

35
00:02:52,500 --> 00:02:55,500
or return back to their tent,

36
00:02:55,500 --> 00:02:58,500
or the tree, perhaps, that they were staying under,

37
00:02:58,500 --> 00:03:03,500
and they had a light and go a long way.

38
00:03:03,500 --> 00:03:05,500
But she didn't know this,

39
00:03:05,500 --> 00:03:06,500
and she thought to herself,

40
00:03:06,500 --> 00:03:10,500
wow, I wonder why they're up so late,

41
00:03:10,500 --> 00:03:13,500
and I'm up because I have all this work to do.

42
00:03:13,500 --> 00:03:17,500
Must be some sickness or something wrong.

43
00:03:17,500 --> 00:03:19,500
Must be something wrong.

44
00:03:19,500 --> 00:03:22,500
I wonder why they're up so late.

45
00:03:22,500 --> 00:03:24,500
And thought nothing more of it.

46
00:03:24,500 --> 00:03:26,500
And then in the morning,

47
00:03:26,500 --> 00:03:29,500
she got together some of the rice dust

48
00:03:29,500 --> 00:03:31,500
that was left over from her work,

49
00:03:31,500 --> 00:03:34,500
and took it up in her hand,

50
00:03:34,500 --> 00:03:36,500
poured some water over it,

51
00:03:36,500 --> 00:03:41,500
and made it into a rice potty,

52
00:03:41,500 --> 00:03:43,500
and put it in the stove,

53
00:03:43,500 --> 00:03:46,500
and the charcoal to heat it.

54
00:03:46,500 --> 00:03:48,500
And prepared her meal that way,

55
00:03:48,500 --> 00:03:53,500
and then walked out of her place of residence,

56
00:03:53,500 --> 00:03:56,500
and maybe was going to work,

57
00:03:56,500 --> 00:03:58,500
I don't know.

58
00:03:58,500 --> 00:04:00,500
And she met the Buddha.

59
00:04:00,500 --> 00:04:02,500
The Buddha was going on arms around,

60
00:04:02,500 --> 00:04:05,500
and she thought to herself,

61
00:04:05,500 --> 00:04:07,500
I have this offering,

62
00:04:07,500 --> 00:04:11,500
I could give this to this monk who appears to be very,

63
00:04:11,500 --> 00:04:14,500
very much worth supporting.

64
00:04:14,500 --> 00:04:15,500
I could do that,

65
00:04:15,500 --> 00:04:17,500
and that would be a great thing for me to do,

66
00:04:17,500 --> 00:04:20,500
but there's no way he'd accept it.

67
00:04:20,500 --> 00:04:23,500
And the Buddha stopped and looked at her,

68
00:04:23,500 --> 00:04:27,500
and so she held out this course,

69
00:04:27,500 --> 00:04:31,500
rice dust, really the worst food,

70
00:04:31,500 --> 00:04:34,500
lowest grade food you could do.

71
00:04:34,500 --> 00:04:36,500
You could make.

72
00:04:36,500 --> 00:04:38,500
And the Buddha held out his bowl.

73
00:04:38,500 --> 00:04:40,500
She put the food in his bowl

74
00:04:40,500 --> 00:04:41,500
and thought to herself,

75
00:04:41,500 --> 00:04:44,500
well, he's accepted it out of kindness to me.

76
00:04:44,500 --> 00:04:46,500
That's very kind of him too,

77
00:04:46,500 --> 00:04:49,500
to accept my offering.

78
00:04:49,500 --> 00:04:51,500
There's no way he's going to eat it.

79
00:04:51,500 --> 00:04:53,500
He'll give it to someone else,

80
00:04:53,500 --> 00:04:56,500
and so she followed along behind him,

81
00:04:56,500 --> 00:04:59,500
and watched, and the Buddha went to a tree,

82
00:04:59,500 --> 00:05:01,500
stopped under the tree, sat down,

83
00:05:01,500 --> 00:05:03,500
and started eating her rice cake.

84
00:05:03,500 --> 00:05:05,500
And so she went up to him,

85
00:05:05,500 --> 00:05:11,500
and she sat down and waited for him to finish.

86
00:05:11,500 --> 00:05:14,500
And they got to talking,

87
00:05:14,500 --> 00:05:17,500
and she remarked to him, she said,

88
00:05:17,500 --> 00:05:19,500
so I'm up late at night

89
00:05:19,500 --> 00:05:23,500
because of all the work and all the duress I'm under,

90
00:05:23,500 --> 00:05:26,500
to get this work completed.

91
00:05:26,500 --> 00:05:29,500
What's up with you?

92
00:05:29,500 --> 00:05:34,500
There must be something wrong with the monks

93
00:05:34,500 --> 00:05:37,500
that they're up, so they.

94
00:05:37,500 --> 00:05:38,500
And the Buddha said,

95
00:05:38,500 --> 00:05:39,500
well, it's true that you're up

96
00:05:39,500 --> 00:05:42,500
because of your stress that you're under,

97
00:05:42,500 --> 00:05:45,500
but my students,

98
00:05:45,500 --> 00:05:47,500
they are up all night,

99
00:05:47,500 --> 00:05:52,500
because that's the work that they are doing,

100
00:05:52,500 --> 00:05:55,500
because they are intent upon

101
00:05:55,500 --> 00:05:58,500
refreeing themselves from suffering.

102
00:05:58,500 --> 00:06:03,500
And we taught this verse.

103
00:06:03,500 --> 00:06:06,500
So I think what this story reminds us

104
00:06:06,500 --> 00:06:11,500
or teaches us the lesson it has.

105
00:06:11,500 --> 00:06:13,500
It's in the juxtaposition

106
00:06:13,500 --> 00:06:18,500
between the worldly exertion

107
00:06:18,500 --> 00:06:23,500
and the way of life of a Buddhist meditator.

108
00:06:23,500 --> 00:06:27,500
It reminds us that meditation isn't just a hobby,

109
00:06:27,500 --> 00:06:32,500
but I think a lot of Buddhists,

110
00:06:32,500 --> 00:06:38,500
of course, a lot of people who practice Buddhist meditation,

111
00:06:38,500 --> 00:06:46,500
never realize the potential

112
00:06:46,500 --> 00:06:51,500
for practicing day and night,

113
00:06:51,500 --> 00:06:54,500
training themselves not only as a hobby

114
00:06:54,500 --> 00:06:59,500
or as a sort of a side exercise,

115
00:06:59,500 --> 00:07:01,500
but as a truly, as a way of life,

116
00:07:01,500 --> 00:07:05,500
as a way of exerting themselves.

117
00:07:05,500 --> 00:07:08,500
We think of meditation differently

118
00:07:08,500 --> 00:07:11,500
from work that we do,

119
00:07:11,500 --> 00:07:13,500
or ambitions that we have,

120
00:07:13,500 --> 00:07:18,500
life goals and a life's work.

121
00:07:18,500 --> 00:07:21,500
It's rare to find someone who thinks of Buddhism

122
00:07:21,500 --> 00:07:26,500
or Buddhist meditation as a life work.

123
00:07:26,500 --> 00:07:29,500
And so the idea of staying awake all day and all night

124
00:07:29,500 --> 00:07:33,500
and doing something like meditating all day and all night

125
00:07:33,500 --> 00:07:34,500
seems crazy.

126
00:07:34,500 --> 00:07:37,500
I've suggested that to some people

127
00:07:37,500 --> 00:07:39,500
and they just thought it was the most

128
00:07:39,500 --> 00:07:41,500
crazy thing to suggest

129
00:07:41,500 --> 00:07:44,500
that one might stay up all night meditating.

130
00:07:44,500 --> 00:07:46,500
And yet we do stay up all night

131
00:07:46,500 --> 00:07:51,500
for other reasons. We'll stay up all night partying

132
00:07:51,500 --> 00:07:54,500
or studying, working

133
00:07:54,500 --> 00:07:58,500
or leave and stay up all night suffering

134
00:07:58,500 --> 00:08:01,500
if we're an insomnia,

135
00:08:01,500 --> 00:08:04,500
or if we have some grievance,

136
00:08:04,500 --> 00:08:09,500
people who have lost loved ones might stay up all night.

137
00:08:09,500 --> 00:08:14,500
So we stay up all night for all sorts of different reasons.

138
00:08:14,500 --> 00:08:19,500
We pull in all nighter for our work.

139
00:08:19,500 --> 00:08:23,500
And so the idea that meditation, practice,

140
00:08:23,500 --> 00:08:26,500
practice of Buddhism, training in the Buddhist teaching

141
00:08:26,500 --> 00:08:29,500
should be similar as an important idea

142
00:08:29,500 --> 00:08:33,500
that we should understand and appreciate.

143
00:08:33,500 --> 00:08:36,500
Sadajagaramanana,

144
00:08:36,500 --> 00:08:39,500
for those who are awake always.

145
00:08:39,500 --> 00:08:44,500
It really is an ideal

146
00:08:44,500 --> 00:08:48,500
that we strive for in our practice

147
00:08:48,500 --> 00:08:51,500
to get to the point where we can be awake

148
00:08:51,500 --> 00:08:53,500
day and night,

149
00:08:53,500 --> 00:08:56,500
where we can be mindful.

150
00:08:56,500 --> 00:09:03,500
As of course sleep is a way of resetting the mind.

151
00:09:03,500 --> 00:09:06,500
And if your mind is engaged in activities

152
00:09:06,500 --> 00:09:11,500
that sell it, that increase its stress and tension

153
00:09:11,500 --> 00:09:14,500
and wind it up,

154
00:09:14,500 --> 00:09:17,500
then sleep is a way of coming back to an ordinary

155
00:09:17,500 --> 00:09:18,500
and normal state.

156
00:09:18,500 --> 00:09:20,500
And so it feels quite relieving.

157
00:09:20,500 --> 00:09:22,500
And so we really like sleep.

158
00:09:22,500 --> 00:09:25,500
But for a meditator, it's quite the opposite.

159
00:09:25,500 --> 00:09:28,500
Meditation is a means of unwinding.

160
00:09:28,500 --> 00:09:30,500
It's a means of untying.

161
00:09:30,500 --> 00:09:32,500
Winding down the mind.

162
00:09:32,500 --> 00:09:34,500
And sleep actually winds it back up

163
00:09:34,500 --> 00:09:39,500
because all of our ordinary and familiar habits

164
00:09:39,500 --> 00:09:43,500
reassert themselves through dreams

165
00:09:43,500 --> 00:09:47,500
and through just through the relaxed state of sleep.

166
00:09:47,500 --> 00:09:51,500
And so we find the progress we've gained is often reset.

167
00:09:51,500 --> 00:09:53,500
And so practicing day and night,

168
00:09:53,500 --> 00:09:56,500
because of the nature of meditation,

169
00:09:56,500 --> 00:09:59,500
it's actually a better thing for the mind

170
00:09:59,500 --> 00:10:02,500
than for the body even than sleep.

171
00:10:02,500 --> 00:10:07,500
It can be.

172
00:10:07,500 --> 00:10:11,500
That's, I think, the lesson of the story

173
00:10:11,500 --> 00:10:18,500
that we should treat meditation as a life's work.

174
00:10:18,500 --> 00:10:20,500
Something we have to put our whole heart in.

175
00:10:20,500 --> 00:10:23,500
If you really want to gain results,

176
00:10:23,500 --> 00:10:26,500
it's something you have to decide is going to be

177
00:10:26,500 --> 00:10:29,500
a part of your life, not just a hobby

178
00:10:29,500 --> 00:10:32,500
or not just an escape,

179
00:10:32,500 --> 00:10:35,500
but something that you take as a training.

180
00:10:35,500 --> 00:10:37,500
And so that's what the verse starts to teach.

181
00:10:37,500 --> 00:10:40,500
The verse has four parts.

182
00:10:40,500 --> 00:10:43,500
The first is in relation to being always awake.

183
00:10:43,500 --> 00:10:45,500
The second is in regards to the training,

184
00:10:45,500 --> 00:10:48,500
training day and night.

185
00:10:48,500 --> 00:10:51,500
So the Buddha taught three types of training,

186
00:10:51,500 --> 00:10:54,500
or three aspects, three parts to the training.

187
00:10:54,500 --> 00:10:56,500
The first one is sea laddis,

188
00:10:56,500 --> 00:10:59,500
which means ethical behavior.

189
00:10:59,500 --> 00:11:04,500
The second is samadhi, which means focus or concentration.

190
00:11:04,500 --> 00:11:08,500
And the third is spanya, which means wisdom.

191
00:11:08,500 --> 00:11:11,500
And there's a sense that the first leads to the second,

192
00:11:11,500 --> 00:11:13,500
the second leads to the third,

193
00:11:13,500 --> 00:11:16,500
but also a sense that this is the three aspects of our training.

194
00:11:16,500 --> 00:11:19,500
When we train, when we practice mindfulness,

195
00:11:19,500 --> 00:11:22,500
we're training in all three.

196
00:11:22,500 --> 00:11:25,500
So sea laddis, the first one, ethics.

197
00:11:25,500 --> 00:11:28,500
It refers, of course, to not breaking precepts,

198
00:11:28,500 --> 00:11:33,500
so we should always be keeping ethical precepts

199
00:11:33,500 --> 00:11:35,500
that we don't do this, so we don't do that,

200
00:11:35,500 --> 00:11:37,500
not just sometimes,

201
00:11:37,500 --> 00:11:39,500
not just when it's convenient.

202
00:11:39,500 --> 00:11:41,500
I won't kill when it's convenient,

203
00:11:41,500 --> 00:11:43,500
but when there are mosquitoes.

204
00:11:43,500 --> 00:11:45,500
Well, we'll take a break from that.

205
00:11:45,500 --> 00:11:46,500
I can't do that.

206
00:11:46,500 --> 00:11:50,500
Taking a break is, especially for ethics,

207
00:11:50,500 --> 00:11:52,500
it misses the point of ethics,

208
00:11:52,500 --> 00:11:57,500
because it's not so much about the body,

209
00:11:57,500 --> 00:11:59,500
as it is about what it does for your mind.

210
00:11:59,500 --> 00:12:01,500
It helps you focus.

211
00:12:01,500 --> 00:12:03,500
And if you're only ethical when it's convenient,

212
00:12:03,500 --> 00:12:05,500
you're not really ethical at all.

213
00:12:05,500 --> 00:12:11,500
You're not changing the mind in an appreciable degree.

214
00:12:11,500 --> 00:12:15,500
But when you are ethical, when you do guard the mind,

215
00:12:15,500 --> 00:12:19,500
or guard the body and speech with the mind,

216
00:12:19,500 --> 00:12:22,500
then it does change, it does focus your mind,

217
00:12:22,500 --> 00:12:27,500
because the avenues by which the mind would get lost

218
00:12:27,500 --> 00:12:30,500
and get sidetracked are shut off.

219
00:12:30,500 --> 00:12:33,500
And so it involves not just keeping precepts,

220
00:12:33,500 --> 00:12:38,500
but also guarding our behavior when you walk ethical walking,

221
00:12:38,500 --> 00:12:44,500
means being with the walking, being objective as you walk.

222
00:12:44,500 --> 00:12:47,500
Ethical speaking means being present when you speak,

223
00:12:47,500 --> 00:12:49,500
when you speak, being aware of what you're saying,

224
00:12:49,500 --> 00:12:52,500
and being aware of the emotions,

225
00:12:52,500 --> 00:12:58,500
guarding the mind as it approaches the physical door

226
00:12:58,500 --> 00:13:04,500
and the verbal door.

227
00:13:04,500 --> 00:13:09,500
The second training samadhi often is translated as concentration,

228
00:13:09,500 --> 00:13:11,500
but focus maybe makes more sense,

229
00:13:11,500 --> 00:13:14,500
because it's about focusing,

230
00:13:14,500 --> 00:13:19,500
not just focusing in, but also coming into focus.

231
00:13:19,500 --> 00:13:22,500
As your more ethical, your mind will start to write itself.

232
00:13:22,500 --> 00:13:26,500
Samadhi has a meaning, etymologically, of balance,

233
00:13:26,500 --> 00:13:28,500
I think.

234
00:13:28,500 --> 00:13:32,500
It's like the word same, it's like a balance,

235
00:13:32,500 --> 00:13:36,500
when you're not unscentred or unfocused,

236
00:13:36,500 --> 00:13:40,500
when you get to the point where everything starts to settle.

237
00:13:40,500 --> 00:13:43,500
This is why tranquility meditation is such a useful,

238
00:13:43,500 --> 00:13:48,500
such a useful base for practice, because it settles the mind.

239
00:13:48,500 --> 00:13:54,500
But mindfulness and mindfulness of experience

240
00:13:54,500 --> 00:13:57,500
drives straight to the point,

241
00:13:57,500 --> 00:14:01,500
because it straightens out our crookedness.

242
00:14:01,500 --> 00:14:04,500
It straightens out all the disruptions.

243
00:14:04,500 --> 00:14:08,500
It settles all of our disruptions, unties all the knots in the mind,

244
00:14:08,500 --> 00:14:12,500
because normally when we experience, for example, pain

245
00:14:12,500 --> 00:14:17,500
or bad memories or loud noises,

246
00:14:17,500 --> 00:14:19,500
we react.

247
00:14:19,500 --> 00:14:22,500
We're tied to them, we're caught up by them,

248
00:14:22,500 --> 00:14:24,500
and this disturbs the mind.

249
00:14:24,500 --> 00:14:27,500
It focuses our mind.

250
00:14:27,500 --> 00:14:32,500
So as we become more mindful, our mind focuses and stabilizes.

251
00:14:32,500 --> 00:14:35,500
And that's necessary in order to see clearly,

252
00:14:35,500 --> 00:14:40,500
just like with a camera, in order to see you have to focus it,

253
00:14:40,500 --> 00:14:43,500
just like a pool of water,

254
00:14:43,500 --> 00:14:46,500
if you are a pot of water,

255
00:14:46,500 --> 00:14:48,500
if you stir it up,

256
00:14:48,500 --> 00:14:51,500
or if it's boiling or something,

257
00:14:51,500 --> 00:14:54,500
you can't see until everything settles to the bottom.

258
00:14:54,500 --> 00:14:57,500
And once it settles, then you can see what's in order.

259
00:14:57,500 --> 00:15:00,500
Entied pools, when the ocean tide comes in,

260
00:15:00,500 --> 00:15:06,500
it gets muddy, but then when it settles, then you can see.

261
00:15:06,500 --> 00:15:12,500
So when the mind is riled up, when the mind is disturbed,

262
00:15:12,500 --> 00:15:17,500
when the mind is caught up by all the emotions and attachments,

263
00:15:17,500 --> 00:15:21,500
reactions that are a part of our ordinary,

264
00:15:21,500 --> 00:15:26,500
non-meditative life, we can't see anything clearly.

265
00:15:26,500 --> 00:15:29,500
And so just as the first leads to the second,

266
00:15:29,500 --> 00:15:33,500
the second leads to the third, which is punya.

267
00:15:33,500 --> 00:15:36,500
Punya meaning wisdom.

268
00:15:36,500 --> 00:15:39,500
And wisdom is in Buddhism,

269
00:15:39,500 --> 00:15:43,500
not very similar to what we normally think of as wisdom

270
00:15:43,500 --> 00:15:46,500
in non-meditative circles,

271
00:15:46,500 --> 00:15:49,500
when we talk about wisdom, we think of something intellectual.

272
00:15:49,500 --> 00:15:54,500
But wisdom really means becoming more familiar with reality.

273
00:15:54,500 --> 00:15:57,500
It's not about studying intellectually,

274
00:15:57,500 --> 00:15:59,500
anything that we might call reality.

275
00:15:59,500 --> 00:16:02,500
It's about studying firsthand,

276
00:16:02,500 --> 00:16:06,500
so that we become more familiar.

277
00:16:06,500 --> 00:16:07,500
What does that mean?

278
00:16:07,500 --> 00:16:09,500
It means becoming more familiar with pain,

279
00:16:09,500 --> 00:16:16,500
because familiarity removes or at least reduces step by step

280
00:16:16,500 --> 00:16:22,500
our reactivity, something that is so familiar to us,

281
00:16:22,500 --> 00:16:25,500
doesn't have the same power over us,

282
00:16:25,500 --> 00:16:30,500
something that we are constantly just reacting to,

283
00:16:30,500 --> 00:16:34,500
trying to avoid shying away from.

284
00:16:34,500 --> 00:16:36,500
And so by focusing on pain,

285
00:16:36,500 --> 00:16:38,500
by focusing on the bad memories,

286
00:16:38,500 --> 00:16:41,500
by focusing on the sound that disturbs us.

287
00:16:41,500 --> 00:16:43,500
We become more familiar with it.

288
00:16:43,500 --> 00:16:46,500
We become more familiar with our reactions.

289
00:16:46,500 --> 00:16:49,500
And whereas a result able to see the reactions

290
00:16:49,500 --> 00:16:51,500
that are positive and beneficial

291
00:16:51,500 --> 00:16:53,500
and those that are unbeneficial,

292
00:16:53,500 --> 00:16:56,500
we're able to see firsthand what it is

293
00:16:56,500 --> 00:16:58,500
that's causing us stress and suffering.

294
00:16:58,500 --> 00:17:00,500
And as it becomes more familiar,

295
00:17:00,500 --> 00:17:03,500
we become wiser.

296
00:17:03,500 --> 00:17:05,500
It means we know more clearly.

297
00:17:05,500 --> 00:17:09,500
We understand more clearly.

298
00:17:09,500 --> 00:17:12,500
The results of our behaviors,

299
00:17:12,500 --> 00:17:16,500
the results of our reactions.

300
00:17:16,500 --> 00:17:18,500
And so as a result, we change.

301
00:17:18,500 --> 00:17:21,500
And the wisdom is about change, really.

302
00:17:21,500 --> 00:17:23,500
The process of acquiring wisdom

303
00:17:23,500 --> 00:17:25,500
is about abandoning all the wrong behaviors,

304
00:17:25,500 --> 00:17:28,500
all the misunderstandings,

305
00:17:28,500 --> 00:17:38,500
all the self-harming tendencies that we have.

306
00:17:38,500 --> 00:17:57,500
This then refers to the goal.

307
00:17:57,500 --> 00:17:59,500
So when you say we train,

308
00:17:59,500 --> 00:18:02,500
we can talk about meditation as a training.

309
00:18:02,500 --> 00:18:05,500
One of the biggest challenges

310
00:18:05,500 --> 00:18:13,500
to doing that is understanding why you might be doing that.

311
00:18:13,500 --> 00:18:16,500
We understand that we would party all night

312
00:18:16,500 --> 00:18:19,500
because of the pleasure, the excitement.

313
00:18:19,500 --> 00:18:22,500
We understand that we would study all night

314
00:18:22,500 --> 00:18:25,500
because of the grades that we might get

315
00:18:25,500 --> 00:18:28,500
because of the eventual job that we might get.

316
00:18:28,500 --> 00:18:32,500
We work all night because of the money, the success,

317
00:18:32,500 --> 00:18:38,500
the results that we can see tangible results.

318
00:18:38,500 --> 00:18:41,500
And often it's quite daunting

319
00:18:41,500 --> 00:18:45,500
or challenging for meditators

320
00:18:45,500 --> 00:18:53,500
to find reason

321
00:18:53,500 --> 00:18:55,500
to commit themselves to meditation,

322
00:18:55,500 --> 00:18:59,500
to see the benefits of the meditation.

323
00:18:59,500 --> 00:19:03,500
Which is kind of curious because the benefits are very similar

324
00:19:03,500 --> 00:19:07,500
to any other type of training or work that you might undertake.

325
00:19:07,500 --> 00:19:11,500
They're very much related to the actual nature of the work.

326
00:19:11,500 --> 00:19:17,500
If you study all night,

327
00:19:17,500 --> 00:19:19,500
you know more about the subject.

328
00:19:19,500 --> 00:19:23,500
If you work in a factory all night,

329
00:19:23,500 --> 00:19:30,500
you get factory products.

330
00:19:30,500 --> 00:19:33,500
If you party all night, well you get a hangover.

331
00:19:33,500 --> 00:19:38,500
But no, you also get the excitement and the pleasure.

332
00:19:38,500 --> 00:19:43,500
I think with meditation we often look too far.

333
00:19:43,500 --> 00:19:52,500
We think of results as being some spiritual, magical, mystical,

334
00:19:52,500 --> 00:19:59,500
high and airy fairy.

335
00:19:59,500 --> 00:20:02,500
Like up in the sky,

336
00:20:02,500 --> 00:20:09,500
sort of like there will be a big sign you've made it or something.

337
00:20:09,500 --> 00:20:12,500
When in fact meditation is just a training.

338
00:20:12,500 --> 00:20:14,500
Nibana, when we talk about nibana,

339
00:20:14,500 --> 00:20:18,500
it's often thought of as this very special thing

340
00:20:18,500 --> 00:20:21,500
that you would really just be walking into

341
00:20:21,500 --> 00:20:25,500
a capital city or a kingdom or walking into heaven.

342
00:20:25,500 --> 00:20:30,500
It would be like arriving at the pearly gates of heaven or something.

343
00:20:30,500 --> 00:20:34,500
But nibana is the result of training.

344
00:20:34,500 --> 00:20:38,500
When you train yourself what happens, you become trained.

345
00:20:38,500 --> 00:20:43,500
When you see clearly, when you observe,

346
00:20:43,500 --> 00:20:50,500
when you cleanse the mind, clear off the lens of perception,

347
00:20:50,500 --> 00:20:57,500
you see more clearly what you get, you get clarity.

348
00:20:57,500 --> 00:21:01,500
So nibana is more of a function of the mind than anything.

349
00:21:01,500 --> 00:21:05,500
Nibana means release or freedom.

350
00:21:05,500 --> 00:21:10,500
It literally means extinguishing,

351
00:21:10,500 --> 00:21:14,500
extinguishing, putting out the fires.

352
00:21:14,500 --> 00:21:20,500
So it's directly related to our observation and appreciation

353
00:21:20,500 --> 00:21:24,500
and cultivation of understanding of the fires.

354
00:21:24,500 --> 00:21:30,500
The fires in the mind, the fire of green, the fire of anger,

355
00:21:30,500 --> 00:21:33,500
the fire of delusion.

356
00:21:33,500 --> 00:21:37,500
Nibana is not something that we attain,

357
00:21:37,500 --> 00:21:41,500
something that we get to if we travel long and long.

358
00:21:41,500 --> 00:21:45,500
Because you're thinking of it that way as a place

359
00:21:45,500 --> 00:21:46,500
that you might get to.

360
00:21:46,500 --> 00:21:49,500
The place has nothing to do with the journey.

361
00:21:49,500 --> 00:21:53,500
And so when we think like that, we have no idea that we're getting

362
00:21:53,500 --> 00:21:55,500
close to the journey, right?

363
00:21:55,500 --> 00:21:56,500
Close to the destination.

364
00:21:56,500 --> 00:22:01,500
When someone is driving a car, you suppose you're driving to a city.

365
00:22:01,500 --> 00:22:03,500
Until you get to the outskirts of the city,

366
00:22:03,500 --> 00:22:05,500
you don't know whether you're going the right way.

367
00:22:05,500 --> 00:22:07,500
And that's often, I think, our meditators feel,

368
00:22:07,500 --> 00:22:10,500
because they separate the journey from the destination.

369
00:22:10,500 --> 00:22:12,500
And it's not like that.

370
00:22:12,500 --> 00:22:14,500
When you lift weights,

371
00:22:14,500 --> 00:22:16,500
you're not trying to move the weights somewhere.

372
00:22:16,500 --> 00:22:18,500
So that by the end, you look and you say,

373
00:22:18,500 --> 00:22:20,500
boy, those weights got all the way over there.

374
00:22:20,500 --> 00:22:22,500
Or all the way up there.

375
00:22:22,500 --> 00:22:25,500
I kept lifting them until they got to heaven.

376
00:22:25,500 --> 00:22:26,500
Absolutely not.

377
00:22:26,500 --> 00:22:28,500
You lift the weights and lift the weights

378
00:22:28,500 --> 00:22:30,500
and you turn around and you say,

379
00:22:30,500 --> 00:22:31,500
they're still where they started.

380
00:22:31,500 --> 00:22:33,500
They haven't moved at all.

381
00:22:33,500 --> 00:22:36,500
It's kind of like that, which is absurd, of course.

382
00:22:36,500 --> 00:22:40,500
Because you're not lifting weights to move them.

383
00:22:40,500 --> 00:22:43,500
You're lifting weights to strengthen the body.

384
00:22:43,500 --> 00:22:48,500
And likewise, you're cultivating the,

385
00:22:48,500 --> 00:22:53,500
you're exerting the mind in order to strengthen it.

386
00:22:53,500 --> 00:22:56,500
And to polish and to cleanse it.

387
00:22:56,500 --> 00:22:59,500
You clean you when you, when you clean the house,

388
00:22:59,500 --> 00:23:01,500
nothing magical happens.

389
00:23:01,500 --> 00:23:03,500
You just end up with a cleaner house.

390
00:23:03,500 --> 00:23:07,500
When you cleanse the mind,

391
00:23:07,500 --> 00:23:09,500
there's nothing magical that happens.

392
00:23:09,500 --> 00:23:12,500
Often meditators are excited by sights.

393
00:23:12,500 --> 00:23:15,500
They'll see bright lights or colors or pictures.

394
00:23:15,500 --> 00:23:18,500
Maybe they'll hear sounds.

395
00:23:18,500 --> 00:23:20,500
Maybe they'll feel bliss or rapture.

396
00:23:20,500 --> 00:23:22,500
And they'll think, well, maybe that's it.

397
00:23:22,500 --> 00:23:24,500
And none of those are it.

398
00:23:24,500 --> 00:23:26,500
But it is a clarity of mind.

399
00:23:26,500 --> 00:23:28,500
The mind becomes more clear.

400
00:23:28,500 --> 00:23:33,500
It sees things closer to as they really are.

401
00:23:33,500 --> 00:23:38,500
The understanding of the things that we cling to,

402
00:23:38,500 --> 00:23:40,500
how not worth clinging to,

403
00:23:40,500 --> 00:23:42,500
that which we feel is permanent,

404
00:23:42,500 --> 00:23:45,500
stable, lasting is actually not.

405
00:23:45,500 --> 00:23:48,500
So the letting go of the things that we cling to,

406
00:23:48,500 --> 00:23:50,500
that which we think of as satisfying,

407
00:23:50,500 --> 00:23:52,500
this will satisfy me.

408
00:23:52,500 --> 00:23:54,500
We find out it's not.

409
00:23:54,500 --> 00:23:57,500
And just the general realization that

410
00:23:57,500 --> 00:23:59,500
there's nothing satisfying.

411
00:23:59,500 --> 00:24:03,500
Those things that we crave are not actually going to satisfy us.

412
00:24:03,500 --> 00:24:10,500
There's no benefit in striving for them and craving for them.

413
00:24:10,500 --> 00:24:13,500
The things that we think of as me and mine under my control

414
00:24:13,500 --> 00:24:18,500
are not me nor mine nor under my control.

415
00:24:18,500 --> 00:24:21,500
All of which leads us to let go.

416
00:24:21,500 --> 00:24:25,500
It leads us to free ourselves from our attachments,

417
00:24:25,500 --> 00:24:31,500
from our stress, from our suffering.

418
00:24:31,500 --> 00:24:35,500
Atangachantia, so someone who trains

419
00:24:35,500 --> 00:24:40,500
and who realizes, who frees themselves,

420
00:24:40,500 --> 00:24:44,500
they will be free from all the filaments of the mind.

421
00:24:44,500 --> 00:24:45,500
That's really the goal.

422
00:24:45,500 --> 00:24:48,500
If you want to ask whether you've made progress,

423
00:24:48,500 --> 00:24:51,500
you just have to ask how is your mind doing?

424
00:24:51,500 --> 00:24:54,500
Is your mind seeing more clearly?

425
00:24:54,500 --> 00:24:57,500
Try and get a sense of what it is that you're actually doing.

426
00:24:57,500 --> 00:24:59,500
When you say to yourself, pain, pain,

427
00:24:59,500 --> 00:25:02,500
what is it? What are you doing?

428
00:25:02,500 --> 00:25:04,500
And that might not be clear in the beginning,

429
00:25:04,500 --> 00:25:06,500
but it should be fairly clear as you go,

430
00:25:06,500 --> 00:25:09,500
that what you're doing is reminding yourself

431
00:25:09,500 --> 00:25:12,500
and you're cultivating this capacity

432
00:25:12,500 --> 00:25:14,500
to remember things as they are,

433
00:25:14,500 --> 00:25:16,500
to see pain as pain.

434
00:25:16,500 --> 00:25:17,500
In order to say pain,

435
00:25:17,500 --> 00:25:19,500
you have to know that it's pain,

436
00:25:19,500 --> 00:25:22,500
which we think, well, of course, I know it's pain,

437
00:25:22,500 --> 00:25:25,500
but you don't. We do for a second, and then we're gone.

438
00:25:25,500 --> 00:25:27,500
When you say to yourself, pain,

439
00:25:27,500 --> 00:25:33,500
you're cultivating the perspective that pain is pain,

440
00:25:33,500 --> 00:25:34,500
rather than that's a problem,

441
00:25:34,500 --> 00:25:35,500
I have to fix it.

442
00:25:35,500 --> 00:25:36,500
What am I going to do?

443
00:25:36,500 --> 00:25:40,500
Take some medicine or get a massage or something.

444
00:25:40,500 --> 00:25:44,500
So it's a change, and it cultivates objectivity

445
00:25:44,500 --> 00:25:47,500
and a familiarity, because it brings you actually closer

446
00:25:47,500 --> 00:25:51,500
to the experience and keeps you with the experience.

447
00:25:51,500 --> 00:25:54,500
So you ask yourself, what am I doing?

448
00:25:54,500 --> 00:25:55,500
What is it doing?

449
00:25:55,500 --> 00:25:57,500
And you see that that's what it's doing.

450
00:25:57,500 --> 00:26:01,500
Then you can only guess that without doubt,

451
00:26:01,500 --> 00:26:03,500
the results will be similar.

452
00:26:03,500 --> 00:26:07,500
Okay, I'm seeing things more clearly as they are.

453
00:26:07,500 --> 00:26:09,500
I'm more becoming more familiar with things.

454
00:26:09,500 --> 00:26:11,500
I'm going to have a better perspective of things,

455
00:26:11,500 --> 00:26:12,500
and you can look there.

456
00:26:12,500 --> 00:26:14,500
You can see there.

457
00:26:14,500 --> 00:26:17,500
Am I less reactive towards these things?

458
00:26:17,500 --> 00:26:24,500
Am I more objective about my experience,

459
00:26:24,500 --> 00:26:28,500
more peaceful in my interactions with things?

460
00:26:28,500 --> 00:26:30,500
That's where you see results.

461
00:26:30,500 --> 00:26:33,500
Of course, the thrust of the verse and the story

462
00:26:33,500 --> 00:26:40,500
is that really the most likely way of attaining

463
00:26:40,500 --> 00:26:48,500
a real result is again, to treat Buddhist practice as a life goal,

464
00:26:48,500 --> 00:26:51,500
not as a hobby, not as something you do

465
00:26:51,500 --> 00:26:56,500
and it's convenient, but something that you engage in day and night.

466
00:26:56,500 --> 00:27:03,500
Sadha, jalgaramana, for those who are always awake.

467
00:27:03,500 --> 00:27:06,500
So that's the demapada for today.

468
00:27:06,500 --> 00:27:10,500
Thank you all for listening.

