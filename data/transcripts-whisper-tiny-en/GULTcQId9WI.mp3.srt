1
00:00:00,000 --> 00:00:02,000
Okay, go ahead.

2
00:00:02,000 --> 00:00:06,000
Hi, Bumpy. When noting various sock thoughts, such as rising and falling,

3
00:00:06,000 --> 00:00:12,000
is it better to simply draw awareness to the thought or to state the word in one's mind?

4
00:00:12,000 --> 00:00:15,000
Draw awareness to the thought.

5
00:00:15,000 --> 00:00:19,000
I don't know what the meaning is of either of those, really.

6
00:00:25,000 --> 00:00:28,000
I don't know how you would draw your awareness to the thought.

7
00:00:28,000 --> 00:00:32,000
Oh, wait. Yeah, no. I don't know how you would do that.

8
00:00:40,000 --> 00:00:42,000
I wouldn't worry about such things.

9
00:00:42,000 --> 00:00:44,000
I wouldn't worry about such things.

10
00:00:44,000 --> 00:00:48,000
The problems that you have with the noting and with the practice

11
00:00:48,000 --> 00:00:52,000
simply come from inexperienced.

12
00:00:52,000 --> 00:00:55,000
Eventually you figure it out. You get it.

13
00:00:55,000 --> 00:00:59,000
You realize what the benefit is, what the purpose is.

14
00:00:59,000 --> 00:01:06,000
You see what leads you to clarity of mind and what leads to suffering.

15
00:01:06,000 --> 00:01:12,000
And so your acknowledgement naturally becomes beneficial.

16
00:01:12,000 --> 00:01:16,000
And you can just get better. I was like, kind of like playing tennis.

17
00:01:16,000 --> 00:01:19,000
You'll know when the ball goes over the net.

18
00:01:19,000 --> 00:01:21,000
I always bring up tennis because I had a horrible time.

19
00:01:21,000 --> 00:01:25,000
Tennis was like a nightmare for me, learning how to play.

20
00:01:25,000 --> 00:01:29,000
All these rules about how to move your racket, how to lift the ball, and I can never hit it.

21
00:01:29,000 --> 00:01:32,000
You'll know when it goes over the net.

22
00:01:32,000 --> 00:01:35,000
So you'll know when it works.

23
00:01:35,000 --> 00:01:39,000
And in the beginning, it's just a lot of practice and testing and figuring it out.

24
00:01:39,000 --> 00:01:41,000
With any training, it's like that.

25
00:01:41,000 --> 00:01:43,000
Well, not with any training.

26
00:01:43,000 --> 00:01:48,000
With trainings like this, any intricate refined training.

27
00:01:48,000 --> 00:01:51,000
And this is like the most refined training.

28
00:01:51,000 --> 00:01:55,000
It's really something, a skill that you have to work at maybe for years.

29
00:01:55,000 --> 00:01:59,000
I mean, the point is, when you're truly mindful,

30
00:01:59,000 --> 00:02:05,000
the moment when you're truly mindful is the moment when you attain the eight-fold noble path.

31
00:02:05,000 --> 00:02:13,000
And that moment is the door to cessation.

32
00:02:13,000 --> 00:02:18,000
That's the moment where you realize Nirvana.

33
00:02:18,000 --> 00:02:21,000
So up until that point, you're really just practicing,

34
00:02:21,000 --> 00:02:23,000
refining, honing your skill.

35
00:02:23,000 --> 00:02:27,000
You're still not doing it right until you get to that moment

36
00:02:27,000 --> 00:02:31,000
where you're actually doing it right and you actually see clearly.

37
00:02:31,000 --> 00:02:47,000
So up until that point, we're practicing.

