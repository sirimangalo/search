1
00:00:00,000 --> 00:00:11,000
Okay, first question, are there recommended strategies for dealing with frustration beyond simple noting?

2
00:00:11,000 --> 00:00:20,000
I work in the technology sector and I'm dealing with technical difficulties on regular basis and frustration often arises.

3
00:00:20,000 --> 00:00:26,000
Well, I'm with you on this one.

4
00:00:26,000 --> 00:00:44,000
And I think the point is to understand that we're dealing with inanimate objects.

5
00:00:44,000 --> 00:00:55,000
But not the point, but it's an easy solution is to remind yourself that you shouldn't expect anything more.

6
00:00:55,000 --> 00:01:03,000
There's the Buddha's teaching on how to deal with frustration, and that's what he uses this phrase.

7
00:01:03,000 --> 00:01:18,000
It's actually a difficult phrase to translate, but it means something like, what more could I expect from this?

8
00:01:18,000 --> 00:01:24,000
This has been dealing with people, if you're dealing with people who are cause of frustration.

9
00:01:24,000 --> 00:01:34,000
That can be quite difficult because they're animate, and they actually can be malevolent.

10
00:01:34,000 --> 00:01:51,000
And I know this is difficult to understand, difficult to get your head around, but no matter how much suffering they cause, computers are not malevolent.

11
00:01:51,000 --> 00:02:00,000
They may seem like it, it may appear that the computers and the technology and the whole universe in fact is out to get you.

12
00:02:00,000 --> 00:02:03,000
And in fact, we do this all the time.

13
00:02:03,000 --> 00:02:09,000
We've done it since the beginning of time, the beginning of humanity.

14
00:02:09,000 --> 00:02:14,000
When it rained, we would think, oh, we did something right.

15
00:02:14,000 --> 00:02:17,000
When it didn't rain, we would think, oh, the gods are angry with us.

16
00:02:17,000 --> 00:02:23,000
When lightning struck, and when there was a forest fire, we would think, oh, it must be the gods.

17
00:02:23,000 --> 00:02:25,000
Something we've done wrong.

18
00:02:25,000 --> 00:02:37,000
And so we apply, we anthropomorphize every situation.

19
00:02:37,000 --> 00:02:46,000
Now, in Buddhism, it's really no different whether you're talking about a human being or whether you're talking about an inanimate object.

20
00:02:46,000 --> 00:02:49,000
There is no entity, and there is no being there.

21
00:02:49,000 --> 00:02:54,000
And it's all simply impersonal experiences.

22
00:02:54,000 --> 00:03:04,000
It states that arise and sees when a person, when you're dealing with an animate object, a person, or an animal, or so on,

23
00:03:04,000 --> 00:03:11,000
you're still only dealing with sight, sound, smells, tastes, feelings, and thoughts that you yourself have.

24
00:03:11,000 --> 00:03:15,000
You can never actually experience the person.

25
00:03:15,000 --> 00:03:25,000
And so the Buddha reminded us, remind ourselves, when you're dealing with a person who is a cause of anger, and frustration, and even hatred,

26
00:03:25,000 --> 00:03:28,000
that this is the way the person is.

27
00:03:28,000 --> 00:03:32,000
That person is just like that, and we shouldn't expect any more.

28
00:03:32,000 --> 00:03:36,000
But withinanimate objects, I think it gets a lot easier.

29
00:03:36,000 --> 00:03:39,000
We can see how ridiculous we're being.

30
00:03:39,000 --> 00:03:46,000
And the common example everyone gives is that when you stub your toe on something, you find yourself kicking that object.

31
00:03:46,000 --> 00:03:49,000
You're hurting your toe even more, or getting angry at the object.

32
00:03:49,000 --> 00:03:56,000
When you hit your head on the table, you get angry at the table, or on the doorframe, or so on.

33
00:03:56,000 --> 00:04:04,000
When you're walking in Asia, when you have these short doors, and you're a tall person, and you smash your head into the doorframe,

34
00:04:04,000 --> 00:04:08,000
the first instinct is to get angry at the door.

35
00:04:08,000 --> 00:04:17,000
And we use this as an example to help people realize how ridiculous we are, and how our anger is irrational.

36
00:04:17,000 --> 00:04:22,000
It has no basis in logical reason.

37
00:04:22,000 --> 00:04:25,000
There's no reason for us to get angry.

38
00:04:25,000 --> 00:04:27,000
We make the reason up afterwards.

39
00:04:27,000 --> 00:04:29,000
We're angry, then we're angry at the door.

40
00:04:29,000 --> 00:04:37,000
Then we start to think about who made the door, and who is the foolish person who designed this door, and didn't make it tall enough,

41
00:04:37,000 --> 00:04:39,000
and we start to get angry at them.

42
00:04:39,000 --> 00:04:49,000
Then we think this is racism, and they don't have any idea that foreigners are taller, or something like that.

43
00:04:49,000 --> 00:04:57,000
For example, and I think this equally applies to computers.

44
00:04:57,000 --> 00:05:02,000
We get frustrated and angry at the computer.

45
00:05:02,000 --> 00:05:11,000
You do some programming, making the website do this or that, and then it doesn't do what I want.

46
00:05:11,000 --> 00:05:14,000
Then the instinct is to get angry at the website, at the computer.

47
00:05:14,000 --> 00:05:17,000
Why aren't you doing what I want was wrong with you?

48
00:05:17,000 --> 00:05:23,000
You curse and swear.

49
00:05:23,000 --> 00:05:33,000
I would say you benefit in the way that the Buddha said from reminding yourself that this is the way it is.

50
00:05:33,000 --> 00:05:36,000
It couldn't be any other way.

51
00:05:36,000 --> 00:05:43,000
There's obviously some cause that is making it this way, and reminding yourself is another method.

52
00:05:43,000 --> 00:05:48,000
I mean, obviously the greatest method is to be mindful of the anger and the frustration.

53
00:05:48,000 --> 00:05:55,000
And so, apart from that, I would say one more thing in that is that, unless you're an anigami,

54
00:05:55,000 --> 00:06:01,000
you're going to get frustrated, and rather than get frustrated at your frustration or feel guilty about it,

55
00:06:01,000 --> 00:06:05,000
learn to look at it and say, oh, there I was frustrated.

56
00:06:05,000 --> 00:06:16,000
Remember that we're just beginning on this profound and incredible journey that it can be even many lifetimes long.

57
00:06:16,000 --> 00:06:23,000
We're not here to destroy anger here and now, necessarily.

58
00:06:23,000 --> 00:06:25,000
We're here to start to learn about it.

59
00:06:25,000 --> 00:06:31,000
And the first step to overcoming anger is to learn about it, to appreciate it and to say,

60
00:06:31,000 --> 00:06:33,000
wow, look at what I've got inside.

61
00:06:33,000 --> 00:06:38,000
I've got this viciousness that makes me kick my computer, or smash my computer screen, right?

62
00:06:38,000 --> 00:06:44,000
It's comics that you see of people smashing their computers, and so on.

63
00:06:44,000 --> 00:06:49,000
Anyway, that's some thoughts on it, I hope.

64
00:06:49,000 --> 00:06:54,000
Yeah, I have something more to say to it.

65
00:06:54,000 --> 00:07:07,000
The problem I think is when we take things personal, that the computer or silver or the door or the table does something to us,

66
00:07:07,000 --> 00:07:12,000
or if we think another person does something to us,

67
00:07:12,000 --> 00:07:18,000
and we take it personal, and it is actually not personal,

68
00:07:18,000 --> 00:07:23,000
especially not if it's something from technology.

69
00:07:23,000 --> 00:07:28,000
The computer doesn't have any intention to do something to you.

70
00:07:28,000 --> 00:07:34,000
It just does what it does.

71
00:07:34,000 --> 00:07:42,000
I think it helps a lot to send metal, to have metal thoughts,

72
00:07:42,000 --> 00:07:50,000
and to be happy for yourself to have the chance to learn some more patience,

73
00:07:50,000 --> 00:07:56,000
that is what helps me around in such situations.

74
00:07:56,000 --> 00:07:58,000
And love to your computer.

75
00:07:58,000 --> 00:08:00,000
Is that interesting?

76
00:08:00,000 --> 00:08:07,000
No, but a heart on your computer, or by it's something, by some flowers.

77
00:08:07,000 --> 00:08:12,000
You could, really, because if you start to think of your computer in a nice way,

78
00:08:12,000 --> 00:08:16,000
and those people who put those little dolls on their computer screen,

79
00:08:16,000 --> 00:08:21,000
buy your computer something nice, it really works.

80
00:08:21,000 --> 00:08:28,000
These are techniques we can have to, they're psychological tools to help us to feel differently about.

81
00:08:28,000 --> 00:08:34,000
And if that doesn't work, then you still have the chance to learn patience with it.

82
00:08:34,000 --> 00:08:59,000
So you should be grateful for that chance, even if this is very difficult.

