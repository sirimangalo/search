1
00:00:00,000 --> 00:00:08,360
T.J. has said that we can judge things based on our wisdom. When something is going on

2
00:00:08,360 --> 00:00:15,800
in the world that we judge is wrong or bad, for example, what is going on in Tibet, how

3
00:00:15,800 --> 00:00:21,920
do we know when and how to properly intervene, or is it better to just let things exist

4
00:00:21,920 --> 00:00:28,760
as they are not trying to influence them, even if we know it's wrong and may hurt someone?

5
00:00:28,760 --> 00:00:40,480
Well, judging based on wisdom, I personally, when I said that probably wasn't thinking

6
00:00:40,480 --> 00:00:55,120
of things like the Chinese occupation of the territory or the state of Tibet, in fact, I don't

7
00:00:55,120 --> 00:01:01,720
know too much weight on this at all, really, and wisdom doesn't judge so much. I was

8
00:01:01,720 --> 00:01:05,160
just probably just giving you, if I, the only time I think that I've talked about this

9
00:01:05,160 --> 00:01:13,240
is when, yeah, this abidama teacher told me that judging can be based on wisdom. And I have

10
00:01:13,240 --> 00:01:18,800
to concur that you can judge that certain things are wrong, but I don't think it's the judging

11
00:01:18,800 --> 00:01:25,680
in the sense that you're thinking of it. You wouldn't judge in the sense of thinking,

12
00:01:25,680 --> 00:01:31,120
I got to do something about that, or that's a real problem, because you've given up

13
00:01:31,120 --> 00:01:37,200
any thought of problems, and enlightened being would be able to judge in the sense of

14
00:01:37,200 --> 00:01:42,640
saying, that's bad, that's good in the sense of, or not even that's bad, that's good.

15
00:01:42,640 --> 00:01:46,640
This will lead to this, and that will lead to that. So this will lead to suffering, and

16
00:01:46,640 --> 00:01:51,520
that will lead to happiness, or this will lead to nothing, and this will lead to something,

17
00:01:51,520 --> 00:01:59,120
and so on, out of knowledge. They will be aware of how defilements lead to suffering, how

18
00:01:59,120 --> 00:02:03,400
craving leads to suffering. And in fact, I would say they would give up, and this was

19
00:02:03,400 --> 00:02:10,240
maybe a controversial thing to say. I would say they would give up such causes like freeing

20
00:02:10,240 --> 00:02:18,800
a nation from another nation, and so on. I'll tell a story that I told. It's not

21
00:02:18,800 --> 00:02:28,080
in my story, but the connection that I made in regards to Thailand, when there were difficulties,

22
00:02:28,080 --> 00:02:37,760
and I don't know if there's still art with Muslim groups, or trying to occupy or take over parts

23
00:02:37,760 --> 00:02:48,320
of Southern Thailand. And there was debate over whether to fight, or whether to give some

24
00:02:48,320 --> 00:02:59,520
control over to the Muslim people. And I looked at the Jataka, because if you want the ultimate

25
00:02:59,520 --> 00:03:10,000
example, we have the Bodhisatta, who, as a king, he gave up his kingdom. He was besieged

26
00:03:10,000 --> 00:03:18,960
by a foreign power. This other king came to, to besiege that his city to attack and conquer

27
00:03:18,960 --> 00:03:23,920
his kingdom. And so the minister said, it came and said, should we fight? Should we attack them?

28
00:03:23,920 --> 00:03:32,960
And he said, fight, attack. What are you talking about? If they want the kingdom, let them have it.

29
00:03:32,960 --> 00:03:38,320
And it's quite a difficult teaching, and some people would say, well, that just applies to the

30
00:03:38,320 --> 00:03:46,400
Bodhisatta, but I'm not sure that it does. The only thing that comes from animosity, from

31
00:03:46,400 --> 00:03:57,200
enmities, is more enmity, and more struggle, and more suffering. The issue in Tibet, or in any

32
00:03:57,200 --> 00:04:17,520
case of occupation, or territorialism, conquering, or power struggles, war, and so on.

33
00:04:17,520 --> 00:04:24,160
From a organizational point of view, you could say, this is wrong, this is right, this is good,

34
00:04:24,160 --> 00:04:30,320
this is bad, but it really has nothing to do with the Buddha's teaching, or with the path to

35
00:04:30,320 --> 00:04:36,160
the freedom from suffering. Freedom from suffering is not an organizational thing. It's something

36
00:04:36,160 --> 00:04:42,960
that you undertake on your own, that you undertake for your own self. And so the only judging

37
00:04:42,960 --> 00:04:50,000
that should go on in terms of the practice is what mine states in your mind are wholesome,

38
00:04:50,000 --> 00:04:57,280
and what mine states are unwholesome. You shouldn't, even in your practice, in terms of the practice,

39
00:04:57,280 --> 00:05:05,360
for freedom from suffering. Even consider the mine states of other people, whether they are wholesome,

40
00:05:05,360 --> 00:05:12,080
or unwholesome, which is said, nah pray san vilomani nah pray san katagatana. We should not concern

41
00:05:12,080 --> 00:05:19,920
ourselves with the faults of others, or the deeds, or undone, the deeds done are undone by others.

42
00:05:19,920 --> 00:05:31,200
We should concern ourselves with our own. So to put it simply, talking about these things as

43
00:05:31,200 --> 00:05:39,520
good or bad issues in the world as good or bad is only on a conceptual level. It has nothing to

44
00:05:39,520 --> 00:05:46,240
do with the Buddha's teaching. And Arahan, of course, does away with the world, or any interest in

45
00:05:46,240 --> 00:05:54,400
the world. And so they wouldn't fight for such causes. If you want, all we can say from a Buddhist

46
00:05:54,400 --> 00:06:00,480
point of view is that, well, if you get angry about it, that's an unwholesome thing. And if you let

47
00:06:00,480 --> 00:06:06,720
go of something, then that's a wholesome thing. If you help people to become to find peace,

48
00:06:06,720 --> 00:06:12,240
then that's a good thing. But if you cause conflict with other people, then that's a bad thing.

49
00:06:12,240 --> 00:06:19,840
On an experiential level. So the judging that we concern ourselves with is experiential moment-to-moment.

50
00:06:24,000 --> 00:06:35,120
The problem here is that we ourselves have defiled minds. But mentioned it already. So sometimes

51
00:06:35,120 --> 00:06:46,800
we want to get involved and we want to intervene because of our own defilements and then we get

52
00:06:46,800 --> 00:06:58,560
entangled and then we get angry and things get out of control. So it is really important to

53
00:06:58,560 --> 00:07:12,080
consider very carefully why do we get involved there. And what is what we can do? What is the good

54
00:07:12,800 --> 00:07:22,080
thing that we can do? I heard of monks burning themselves or people burning themselves. So

55
00:07:22,080 --> 00:07:34,080
this is killing. And killing can only happen when there is a huge amount of aggression and anger.

56
00:07:34,800 --> 00:07:50,640
So it might be understandable that people whose country has been intruded and is violated all

57
00:07:50,640 --> 00:07:58,880
the time that there is that anger. But especially when there is not the possibility as it was

58
00:07:58,880 --> 00:08:15,600
earlier to practice meditation and live a more spiritual life. So all these actions come from

59
00:08:15,600 --> 00:08:26,800
defiled minds and I don't think it helps the case. It makes it worse because no peace can

60
00:08:26,800 --> 00:08:38,320
can arise out of this because this is just hatred. So when one gets involved and when one wants

61
00:08:38,320 --> 00:08:50,560
to intervene, it is very important to check on the intentions and to check on the

62
00:08:50,560 --> 00:09:20,400
the the the defilements that there's usually there's some wanting or not wanting.

