1
00:00:00,000 --> 00:00:04,640
Good evening and welcome back to our study of the Dhamapada.

2
00:00:04,640 --> 00:00:10,520
Tonight we continue on with verse number 95, which reads as follows.

3
00:00:10,520 --> 00:00:23,000
Pratavisamoh, no virajati. In the key lupamoh tadisubhato, rahado api takadamoh,

4
00:00:23,000 --> 00:00:36,760
san sara nambavantita, you know, which means, like the earth, one is not disturbed,

5
00:00:36,760 --> 00:00:47,240
like the, or just as the, just as the earth is not disturbed, is not, just like the earth

6
00:00:47,240 --> 00:00:57,040
that is undisturbed, or just like, just as the earth is not undisturbed, is not obstructed,

7
00:00:57,040 --> 00:01:09,400
whatever that means, just as a foundation pillar in the key lup, just like an

8
00:01:09,400 --> 00:01:19,960
indikila or a foundation post, is such a one of, is such a one of good, good behavior

9
00:01:19,960 --> 00:01:31,000
or good deeds, subhato. I guess this is translated, he is undisturbed, like the earth,

10
00:01:31,000 --> 00:01:38,920
that's what it is. Pratavisamoh, no virajati, he is undisturbed like the earth, one who is

11
00:01:38,920 --> 00:01:55,600
of good behavior or good manners, is like a foundation post, is firmly, you know, well-founded

12
00:01:55,600 --> 00:02:06,360
unshakable, rahado api takadamoh, like a lake that has become unmuddied, that is clear

13
00:02:06,360 --> 00:02:17,080
and free from, from cloudiness, kandama is mud, samsara nabavantita, you know, there

14
00:02:17,080 --> 00:02:26,560
is no wandering on, there is no round of samsara, there is no, no transcendent migration,

15
00:02:26,560 --> 00:02:34,700
from one life to the next born old sick and die, being born old, getting old, getting

16
00:02:34,700 --> 00:02:52,960
sick and dying, again and again for such a person. So, like the earth, it is unobstructed,

17
00:02:52,960 --> 00:03:13,840
entroubled maybe, founded like a foundation post, like a lake that is clear and unmuddied,

18
00:03:13,840 --> 00:03:22,200
for such a person, there is no more becoming. This is a description of sari puta, so the

19
00:03:22,200 --> 00:03:31,340
story is about a famous incident that occurred in regards to sari puta, sari puta got

20
00:03:31,340 --> 00:03:39,000
the idea that he would go off on a wandering tour, so we had one story where the Buddha

21
00:03:39,000 --> 00:03:45,140
went off on a wandering tour and makaspa turned back, so the story about makaspa, here is

22
00:03:45,140 --> 00:03:55,100
a story about sari puta, and it seems that sari puta was having monks to go with him,

23
00:03:55,100 --> 00:04:04,840
talking to monks about going with him or staying behind, and there was one monk who sari

24
00:04:04,840 --> 00:04:14,200
puta didn't know by name or his name doesn't even show up, and sari puta was addressing

25
00:04:14,200 --> 00:04:20,640
monks by name, and this monk thought to himself, oh, sari puta will address me by name

26
00:04:20,640 --> 00:04:35,960
and he never did, sari puta just said, okay, and the rest of you do this or do that. He didn't

27
00:04:35,960 --> 00:04:42,280
call him, he didn't call him out, which it seems like such a small thing really, but it

28
00:04:42,280 --> 00:04:49,160
reminds me when I was a new monk, and it's a story that I'll always remember, when I

29
00:04:49,160 --> 00:05:08,840
returned to stay with my teacher as a monk, I always wanted him to remember my name, that's

30
00:05:08,840 --> 00:05:14,440
what it was, I was always kind of disappointed that he didn't remember my name, and he would

31
00:05:14,440 --> 00:05:21,320
always ask, what's his name, and when I told him my name, he said, no, what kind of a name

32
00:05:21,320 --> 00:05:28,680
is that? I said, it's going to be fun to me actually, and I could never remember my name

33
00:05:28,680 --> 00:05:35,280
every time I go to see him, what's this monk, where did he or a day? You're a day

34
00:05:35,280 --> 00:05:41,800
in with you, because after I ordained, I left and went back to Canada for a year, it was

35
00:05:41,800 --> 00:05:53,200
an odd situation, I stayed with a monk in Canada, and then there was this big bruha about

36
00:05:53,200 --> 00:06:02,560
the international department, and I kind of caused a ruckus demanding that, and demanding

37
00:06:02,560 --> 00:06:08,160
something, or I was put in a position, because there was a, I was all politics, and there were

38
00:06:08,160 --> 00:06:17,440
two groups, the monastery was sort of split between those who wanted the international

39
00:06:17,440 --> 00:06:21,840
students to go with a group of laypeople, and those who wanted the international students

40
00:06:21,840 --> 00:06:29,840
to practice with the monks through a translator, and anyway, I was being put on one side,

41
00:06:29,840 --> 00:06:33,320
and there was another monk being put on the other side, and we were the two people, and

42
00:06:33,320 --> 00:06:37,080
I said, this is splitting up the song, I say, I refuse to do it, and I went in front

43
00:06:37,080 --> 00:06:42,680
of Edgentong, and the other monk yelled at me and said, who do you think you are?

44
00:06:42,680 --> 00:06:49,040
Anyway, I caused, I felt, and then Edgentong scolded me as well, and said, this isn't splitting

45
00:06:49,040 --> 00:06:53,800
up the song, but this is trying to make things work, and he said, do you have wrong thought

46
00:06:53,800 --> 00:07:01,800
he scolded me for it, and I got in, got in, you know, there were stories going around

47
00:07:01,800 --> 00:07:08,760
the monastery about my bad behavior, I was quite adamant, and I guess kind of you could

48
00:07:08,760 --> 00:07:13,440
say kind of arrogant about it, you know, kind of how Westerners tend to be when we think

49
00:07:13,440 --> 00:07:19,120
something's not right, we get up and say in front of everybody, and from then on he never

50
00:07:19,120 --> 00:07:30,080
forgot, I knew, so yeah, I know how that is, but after that I wasn't so, it was kind

51
00:07:30,080 --> 00:07:35,280
of ruined for me after that because I know why he remembered my name, not for the best

52
00:07:35,280 --> 00:07:42,040
reason, but again it goes back to how monks can get a little bit crazy, you know, when

53
00:07:42,040 --> 00:07:52,520
your life is so simple, I mean for most people in the world they have a very extreme outputs

54
00:07:52,520 --> 00:07:57,360
for pleasure, you know, so you have all sorts of complex activities if you want to go to

55
00:07:57,360 --> 00:08:03,840
a movie, or if you want to go dancing, or if you want to go to the opera, or I don't

56
00:08:03,840 --> 00:08:10,720
know what people do, you want to go to a rave, they still do those, or you can do drugs

57
00:08:10,720 --> 00:08:15,320
or that kind of thing, but for monks there's very little of an outlet, and so you tend

58
00:08:15,320 --> 00:08:24,160
to get, you tend to get petty, and this is an example of this monk being very petty

59
00:08:24,160 --> 00:08:31,040
because there was, his ego didn't have much to react to, so a simple sliding and not calling

60
00:08:31,040 --> 00:08:36,160
him by name, set him off, and for then on he would hear a grudge against Harry Putin,

61
00:08:36,160 --> 00:08:40,840
and you hear about this a lot, monks who had grudges, they tend to be recognized, again

62
00:08:40,840 --> 00:08:47,960
in Harken's back to my own experience, I can verify this, what is it, I don't know if

63
00:08:47,960 --> 00:08:52,920
there is even an adage for something like the greasy wheel, the squeaky wheel gets

64
00:08:52,920 --> 00:09:00,560
the grease, but that's supposed to be a good thing, you know, sometimes it's better

65
00:09:00,560 --> 00:09:06,520
to go unknown, because the reason's for becoming known, this monk got in the Dhamapada

66
00:09:06,520 --> 00:09:14,160
for his, and they didn't even put his name in, insult to injury, they put him here and

67
00:09:14,160 --> 00:09:18,600
they refused to include his name, or just, you know, refused to remember his name, I don't

68
00:09:18,600 --> 00:09:28,120
know, or just forgot it, I'd get thrown down, I'd go to the saina, apakatobiku, whatever

69
00:09:28,120 --> 00:09:34,480
that means, apakatob, unknown, right, so they refused to remember, they just, no one knew

70
00:09:34,480 --> 00:09:40,200
his name, or it was unknown by Sariput, I guess, so Sariput, it didn't call him a

71
00:09:40,200 --> 00:09:46,920
by name, anyway, being petty, and now he's remembered forever immortalized in the Dhamapada

72
00:09:46,920 --> 00:09:57,040
for it, so then on top of that, to add insult to injury, not at all really, but,

73
00:09:57,040 --> 00:10:05,120
you know, I'm talking to get even more, let's say, to be even worse about this whole situation,

74
00:10:05,120 --> 00:10:15,240
he'd be even more petty to an extreme degree, as Sariput, as they were preparing, Sariput

75
00:10:15,240 --> 00:10:25,680
to walk past him, and the corner of his robe touched the upset monk on the ear or something.

76
00:10:25,680 --> 00:10:33,480
And the monk, either getting angry about it, or just using it as an excuse, started

77
00:10:33,480 --> 00:10:39,720
going around and telling people, or he went straight to the Buddha, maybe, and told

78
00:10:39,720 --> 00:10:50,680
the Buddha, what do you think he said to the Buddha, he said, yeah, he went straight to

79
00:10:50,680 --> 00:10:59,720
the Buddha, and said, Reverend Sir Venerable Sariput, that doubtless thinking to himself,

80
00:10:59,720 --> 00:11:08,120
I am your chief disciple, struck me a blow that almost broke my ear drum or something,

81
00:11:08,120 --> 00:11:14,960
and some part of my ear, and then without even asking forgiveness, he set out on his

82
00:11:14,960 --> 00:11:21,000
arms, set out for arms, so it was before the arms run.

83
00:11:21,000 --> 00:11:29,400
Did you imagine the goal of this guy going to the Buddha to inform upon Sariput, who had

84
00:11:29,400 --> 00:11:30,400
done nothing wrong?

85
00:11:30,400 --> 00:11:40,600
I mean, the amount of ignorance you need to do that is pretty astounding, and yet, I mean,

86
00:11:40,600 --> 00:11:47,280
it happens, people get blinded, but as we'll see, it doesn't last long, it can't last

87
00:11:47,280 --> 00:11:48,280
long.

88
00:11:48,280 --> 00:11:53,960
There's such purity, and as we'll see in regards to how Sariput responds, there's such

89
00:11:53,960 --> 00:12:01,600
a profundity to be, to these beings that they can't last, eventually he asks forgiveness.

90
00:12:01,600 --> 00:12:10,400
So all the monks hear about this, the Buddha calls someone, go and tell Sariput to come,

91
00:12:10,400 --> 00:12:17,960
and all the other monks finding out about this, they gather together, and Mogalana, went

92
00:12:17,960 --> 00:12:24,040
around to all the monks, and said, come, come and see, you want to see something neat,

93
00:12:24,040 --> 00:12:30,880
see how Sariput deals with this guy, and so all the monks came out to listen, and the

94
00:12:30,880 --> 00:12:39,400
Buddha said, so Sariput to this monk says that you have struck him, and without even

95
00:12:39,400 --> 00:12:48,160
apologizing, went away, you hit him, and then to find out what Sariput says, we actually

96
00:12:48,160 --> 00:12:50,120
have to go to the Anguttarani kaya.

97
00:12:50,120 --> 00:13:01,000
This is actually one story that is supported by the actual canonical texts.

98
00:13:01,000 --> 00:13:12,320
Remember, the stories we're reading are sort of recreations, recreated from the verses,

99
00:13:12,320 --> 00:13:18,240
so they may have been exaggerated, and more likely they've been exaggerated, perhaps

100
00:13:18,240 --> 00:13:30,240
some might even say made up, and just based on folklore, but regardless, they are not

101
00:13:30,240 --> 00:13:39,840
canonical, whether they're true or not, but here we have one that is based on Anguttarani

102
00:13:39,840 --> 00:13:46,840
kaya book of nines, as the Buddha says, nine things.

103
00:13:46,840 --> 00:13:55,000
He doesn't say I didn't hit the guy, instead, he says, one who has not established

104
00:13:55,000 --> 00:14:20,440
mindfulness directed to the body in regards to his own body, let's see how he says that.

105
00:14:20,440 --> 00:14:35,560
So one who is not mindful of the body, and this can be in one of various ways, but you

106
00:14:35,560 --> 00:14:41,400
could relate it back to our practice, and say one who is not aware when they're walking,

107
00:14:41,400 --> 00:14:47,360
that they know they're walking, not aware of the movements of their body, where their

108
00:14:47,360 --> 00:14:55,920
hands are, not aware of when they, where their mind is, and not objective about their

109
00:14:55,920 --> 00:15:02,920
actions, such a person could very easily strike someone out of anger.

110
00:15:02,920 --> 00:15:08,560
But the point is, when you're mindfulness, you actually are unable to get angry.

111
00:15:08,560 --> 00:15:15,760
When you're truly mindful, as an aura hunt is always, it's not possible for you to strike

112
00:15:15,760 --> 00:15:19,800
someone, and that's what's very Buddha says.

113
00:15:19,800 --> 00:15:24,400
And he gives nine similes as to why it wouldn't be possible, or as to what it's like

114
00:15:24,400 --> 00:15:30,080
to be someone who is mindful of the body, who is aware of the movements of the body, who

115
00:15:30,080 --> 00:15:34,920
would know when they were raising their fist and would be fully mindful, and thus unable

116
00:15:34,920 --> 00:15:45,600
to give rise to the rage required, the cruelty required, the hatred required to hurt someone.

117
00:15:45,600 --> 00:15:52,880
And so he says, just as water, just as you can wash impure things in water, and the water

118
00:15:52,880 --> 00:16:00,160
is not repulsed by it, the water doesn't get upset by that.

119
00:16:00,160 --> 00:16:06,600
He said, my mind is like water without enmity or ill will towards anyone, no matter what

120
00:16:06,600 --> 00:16:08,800
they do.

121
00:16:08,800 --> 00:16:14,040
Just as fire burns impure things, but it's not upset by it.

122
00:16:14,040 --> 00:16:24,720
So too, I dwell like fire, not upset when people, whatever they may say are due to me.

123
00:16:24,720 --> 00:16:33,560
Just as air blows upon impure things, and it's not repulsed by that.

124
00:16:33,560 --> 00:16:41,000
So too, I can meet with anything, come in contact with anyone and not be upset.

125
00:16:41,000 --> 00:16:44,560
My mind I dwell with a mind like air.

126
00:16:44,560 --> 00:16:48,800
I dwell with a mind like a duster, a dust rag, I guess.

127
00:16:48,800 --> 00:16:55,640
Dust rag is used to clean up all sorts of rag, it's used to clean up all sorts of things,

128
00:16:55,640 --> 00:17:00,960
and yet the rag is not repelled.

129
00:17:00,960 --> 00:17:08,640
And then just as an outcast boy or girl, when I walk, I think of myself as an outcast

130
00:17:08,640 --> 00:17:22,120
boy or girl holding a bull and wandering around for arms or for charity, begging, he

131
00:17:22,120 --> 00:17:27,560
says, that's how I see myself, I put myself on that level, when I go for arms, when

132
00:17:27,560 --> 00:17:34,000
I walk, when I live, because an outcast boy or girl in India, they would be, they're

133
00:17:34,000 --> 00:17:39,520
not allowed to work, they're not allowed to do so many things, they have to live in

134
00:17:39,520 --> 00:17:47,720
special areas or had to, this is an ancient time.

135
00:17:47,720 --> 00:17:53,200
Just as a bull with his horns cut, my old well tamed and well trained, so a bull without

136
00:17:53,200 --> 00:18:05,040
its horns isn't going to attack anyone, and then I am like this bull without horns,

137
00:18:05,040 --> 00:18:11,360
I have no horns with which to attack, meaning he can't even, he couldn't hit someone

138
00:18:11,360 --> 00:18:15,480
even if, well, if he wanted to do because he couldn't want to, it could never happen,

139
00:18:15,480 --> 00:18:20,200
it's not possible, he's not capable of it, it's not that he doesn't even want to, it's

140
00:18:20,200 --> 00:18:24,520
he's not capable of it.

141
00:18:24,520 --> 00:18:31,440
And number eight, just as a woman or a man, a young woman or man, think of a young woman,

142
00:18:31,440 --> 00:18:36,320
a young man who like to dress up, like to be clean, like to put on perfumes, like to

143
00:18:36,320 --> 00:18:39,880
put on makeup.

144
00:18:39,880 --> 00:18:45,320
Imagine if such a person had a carcass of a snake, a carcass of a dog, or a carcass of

145
00:18:45,320 --> 00:18:53,000
a human being, slung around their neck, you can imagine, put a carcass of a dog around

146
00:18:53,000 --> 00:18:57,160
your neck, what they would think of that.

147
00:18:57,160 --> 00:19:03,480
And he said, he says, Monday, just in that way, I am repelled, humiliated, and disgusted

148
00:19:03,480 --> 00:19:07,760
by this foul body, interesting wording, no?

149
00:19:07,760 --> 00:19:13,360
I mean, he has no, it's not even, you have to take that kind of with a bit of license

150
00:19:13,360 --> 00:19:19,880
because I think it's unfair to say he's disgusted by it, disgusted would be, you know,

151
00:19:19,880 --> 00:19:25,280
an upset, but he's certainly not upset, he just has no thought that there's anything

152
00:19:25,280 --> 00:19:34,280
good inside it, I mean, the body is made up of all sorts of icky things.

153
00:19:34,280 --> 00:19:43,320
And number nine, seeing as he does that, just as a person might carry around a

154
00:19:43,320 --> 00:19:50,800
cracked bowl, or imagine someone with a garbage bank full of, from a restaurant, from behind

155
00:19:50,800 --> 00:20:00,680
a restaurant with fat and, and refuse and so on, with a leaky garbage bank.

156
00:20:00,680 --> 00:20:06,880
This is a perforated bowl, a cracked bowl with, with fat in it that oozes and drips,

157
00:20:06,880 --> 00:20:13,040
and then the body is like this with all sorts of holes in it that oozes and drip and sweat

158
00:20:13,040 --> 00:20:16,080
and smell.

159
00:20:16,080 --> 00:20:24,920
And so he said, one who is in conclusion, one who is not established mindfulness directed

160
00:20:24,920 --> 00:20:29,280
to the body in regards to his own body might strike a fellow monk here and then set

161
00:20:29,280 --> 00:20:31,120
out onto her without apologizing.

162
00:20:31,120 --> 00:20:37,680
So it's called the lion's rock, it's one of the many lion's roars that we hear about.

163
00:20:37,680 --> 00:20:40,680
This one is by Sariput.

164
00:20:40,680 --> 00:20:48,160
And so it's kind of impressive to read and then Sariput has no qualms about putting this

165
00:20:48,160 --> 00:20:54,440
monk in his place, so to speak, has no, it's interesting that this came right after

166
00:20:54,440 --> 00:20:55,440
criticism.

167
00:20:55,440 --> 00:21:01,000
Yesterday we were talking about criticism and reading through it today made me think,

168
00:21:01,000 --> 00:21:03,480
okay, this is the emulation required.

169
00:21:03,480 --> 00:21:09,400
This is what we have to work towards to be like Sariput who's so mindful that he has no

170
00:21:09,400 --> 00:21:20,520
ill will and so this is the, this is Sariput's argument as to why that such a thing

171
00:21:20,520 --> 00:21:26,000
wouldn't be done.

172
00:21:26,000 --> 00:21:34,000
So you think that would be enough and it was enough, the monk immediately was shaken and

173
00:21:34,000 --> 00:21:37,960
got up on his hands and knees and prostrated himself at the foot of the Buddha and sent

174
00:21:37,960 --> 00:21:39,680
please forgive me.

175
00:21:39,680 --> 00:21:45,920
I was stupid, I was wrong, I transmitted a transgression that I so foolishly stupidly

176
00:21:45,920 --> 00:21:51,560
and unskillfully slandered the venerable suit, sorry Buddha, on grounds that were untrue,

177
00:21:51,560 --> 00:22:02,000
baseless false, and the Buddha acknowledged that and then he turns the Sariput and he says

178
00:22:02,000 --> 00:22:06,640
Sariput forgive him before his head explodes.

179
00:22:06,640 --> 00:22:13,440
Apparently it was a thing, there is a thing, if you insult someone who is enlightened

180
00:22:13,440 --> 00:22:19,480
can get you into some serious head exploding.

181
00:22:19,480 --> 00:22:26,280
And so if it was, as if it wasn't enough Sariput puts the icing on the cake by saying,

182
00:22:26,280 --> 00:22:34,520
I will pardon him, no, because of what he has said or I think it's, I will pardon him

183
00:22:34,520 --> 00:22:43,080
if he says that to me, right, if he asked me forgiveness, meaning he hasn't yet asked

184
00:22:43,080 --> 00:22:45,840
the Buddha, the asked Sariput forgiveness, he asked the Buddha forgiveness.

185
00:22:45,840 --> 00:22:50,640
So the point is, Sariput has nothing to forgive, has not no pardon to give until the

186
00:22:50,640 --> 00:22:54,840
monk says, sorry, not the, sorry Buddha needs it, but he's saying, well if he came

187
00:22:54,840 --> 00:23:00,040
to me, of course, I apologize, alright, forgive him, I have no hard feelings.

188
00:23:00,040 --> 00:23:08,640
And then he says, and then, and let him pardon me as well, let him, let him forgive me

189
00:23:08,640 --> 00:23:12,720
as well, meaning for anything I might have done to him.

190
00:23:12,720 --> 00:23:19,200
This is actually quite standard, when you, when you apologize this to a monk, we do this

191
00:23:19,200 --> 00:23:23,720
after we'll apologize to each other and then I apologize to this monk and the monk turns

192
00:23:23,720 --> 00:23:34,720
around and apologize this to me, we actually have a, we have at the end of the opening ceremony,

193
00:23:34,720 --> 00:23:40,720
we do this, opening and closing ceremonies for a meditator, we have this ceremony of asking

194
00:23:40,720 --> 00:23:44,120
forgiveness of the teacher and then the teacher turns around and asks forgiveness of

195
00:23:44,120 --> 00:23:47,440
us in this tradition.

196
00:23:47,440 --> 00:23:54,600
So this is an example for us to emulate, then we switch back to the Dhamapada story,

197
00:23:54,600 --> 00:24:03,120
then apparently the Buddha, the monks started commenting on this and were terribly impressed

198
00:24:03,120 --> 00:24:10,040
and in awe and reverence for Sariput and his ability to just put this guy in his place,

199
00:24:10,040 --> 00:24:21,840
but to not be at all upset or disturbed or angry towards the other monk and the teacher

200
00:24:21,840 --> 00:24:28,440
heard what they were saying and came and said to the, no, it's not, it's not unusual,

201
00:24:28,440 --> 00:24:33,960
it's not hard to understand, he said, it's impossible for Sariput and people like him

202
00:24:33,960 --> 00:24:41,040
to have hatred and Sariput does mind is like the great earth, it's like an in the Kila,

203
00:24:41,040 --> 00:24:50,400
like a foundation post, like a pool of still water and then he taught this verse.

204
00:24:50,400 --> 00:24:56,840
So how this relates to us, again it's about emulating this, these are qualities to emulate,

205
00:24:56,840 --> 00:25:02,880
we should read the whole suit and the Anguatar book of nines for when people accuse us

206
00:25:02,880 --> 00:25:10,680
of things that we didn't do or even of things that we didn't do, I guess it's harder

207
00:25:10,680 --> 00:25:16,480
when you didn't do it and you didn't do something, and you notice that Sariput and in general

208
00:25:16,480 --> 00:25:24,640
you'll notice that it's not that they don't refute it, they do, but they do so without

209
00:25:24,640 --> 00:25:28,800
anger and they do it for the purpose of sending the record straight so people don't get

210
00:25:28,800 --> 00:25:40,840
the wrong idea, they don't do it so that it makes them look, for sure.

211
00:25:40,840 --> 00:25:50,680
And so we emulate these qualities, we emulate the imagery that Sariput had talked about,

212
00:25:50,680 --> 00:25:55,000
but most importantly we emulate them in their practice, mindfulness of the body, it's a huge

213
00:25:55,000 --> 00:26:01,920
one, it's mindfulness is the huge one, but he's talking about mindfulness of the body

214
00:26:01,920 --> 00:26:08,000
is because we're referring to the body, he hits someone, so he points out it's just

215
00:26:08,000 --> 00:26:16,040
a funny thing to think about because I'm so mindful with my body, it's not really possible

216
00:26:16,040 --> 00:26:23,160
that such a thing, watching, observing his own actions, which is the proper way to deal

217
00:26:23,160 --> 00:26:28,160
with a accusation, someone accuses you of something, and you know, just say, I would never

218
00:26:28,160 --> 00:26:33,800
do that, you reflect and say, could I do something, did I do that, could I do that?

219
00:26:33,800 --> 00:26:40,160
And he reluctantly said, it's not possible because I'm so mindful all the time, how

220
00:26:40,160 --> 00:26:43,000
could you do that if you're mindful?

221
00:26:43,000 --> 00:26:50,680
This is the key, our theory is and the power of mindfulness is that you can't be angry

222
00:26:50,680 --> 00:26:57,920
and mindful at the same time, when you're clearly aware and objectively aware, the objectivity

223
00:26:57,920 --> 00:27:07,320
overcomes any anger or any greed, and you just are, you're just present.

224
00:27:07,320 --> 00:27:15,600
Makes you like the earth undisturbed, untroubled, the earth is, I mean it's the imagery

225
00:27:15,600 --> 00:27:23,760
of the big, the great earth, Mahapatoui, the earth as a planet, that does have earthquakes,

226
00:27:23,760 --> 00:27:33,000
but there's pretty much undisturbed, just sits there, it's totally grounded, I could say.

227
00:27:33,000 --> 00:27:38,840
And like a foundation post, they use this Indikila to mean something, Kila means a post

228
00:27:38,840 --> 00:27:45,120
that Indai is of Indra, it's an expression, it means the foundation of a building, the first

229
00:27:45,120 --> 00:27:49,800
post that sort of keeps the building up I guess.

230
00:27:49,800 --> 00:27:58,800
And so it's the most stable part of a house, someone who is firm and unmoved, you know,

231
00:27:58,800 --> 00:28:02,680
so if someone calls them an asking him, they don't get upset, if someone says nice things

232
00:28:02,680 --> 00:28:06,320
to them, they don't get pleased by it.

233
00:28:06,320 --> 00:28:10,080
And things come, they're not elated, bad things come, they're not depressed, they live

234
00:28:10,080 --> 00:28:16,520
at peace, and they live in the world without being of the world, without being moved

235
00:28:16,520 --> 00:28:23,000
by the world, without falling under the power of samsara, they become like a pool of

236
00:28:23,000 --> 00:28:30,440
free of water, still pool of water free from mud, you can think of mud as all of the mental

237
00:28:30,440 --> 00:28:37,480
defilements, this is what we're aiming for and we pass in as to have a clear mind.

238
00:28:37,480 --> 00:28:45,720
Such a person, the rounds of existence do not exist, some sara does not exist, there is

239
00:28:45,720 --> 00:28:51,800
no samsara, samsara, nabhavantitad, you know.

240
00:28:51,800 --> 00:28:58,760
Some sara means wandering on, they have nowhere left to wander, they have no desires

241
00:28:58,760 --> 00:29:08,120
or no lessons left to learn, no goals yet to achieve, they've done it all, or they've

242
00:29:08,120 --> 00:29:11,480
risen above it all, they've gone beyond it all.

243
00:29:11,480 --> 00:29:18,600
Anyway, so that's the dhamapana for tonight, nice story, always nice to hear about sari

244
00:29:18,600 --> 00:29:19,600
Buddha.

245
00:29:19,600 --> 00:29:23,320
He has other stories like this, there's another story about a monk actually coming

246
00:29:23,320 --> 00:29:28,800
and hitting him over the, hitting him on the back, sari Buddha was wandering on arms and

247
00:29:28,800 --> 00:29:32,600
this brahmana I think, he'd heard that the sari Buddha was like this, probably heard

248
00:29:32,600 --> 00:29:36,800
from this story and he thought, wow, wonder if it's true, how could it be possible that

249
00:29:36,800 --> 00:29:40,640
this guy doesn't get upset no matter what happens and he said, well, let's test him

250
00:29:40,640 --> 00:29:46,920
and so he took a stick, he wanders up, walks up behind sari put down his arms and smacks

251
00:29:46,920 --> 00:29:54,600
him on the back and sari put it just turns around and looks at him and then turns and

252
00:29:54,600 --> 00:30:01,880
goes on his way and then the brahmana ends up apologizing to sari put that, see imagine

253
00:30:01,880 --> 00:30:07,440
just walking up and smacking him, he said it's really true, he doesn't get angry, he doesn't

254
00:30:07,440 --> 00:30:13,520
get upset no matter what happened, so there's them, if you're really interested in sari

255
00:30:13,520 --> 00:30:22,280
put that you should read, there's an English, on the internet, there's an article, you

256
00:30:22,280 --> 00:30:27,200
can also, I think it's in great disciples of the Buddha, but it's also under the life of

257
00:30:27,200 --> 00:30:31,400
sari put, I think it was originally published in the Buddhist publication society, but

258
00:30:31,400 --> 00:30:38,160
I think it's on the internet, very much worth reading, because very much worth emulating,

259
00:30:38,160 --> 00:30:43,760
sari put down mokalana and all the great disciples, if you want to know what a true Buddhist

260
00:30:43,760 --> 00:30:53,920
is like, what are Buddhists like when they go all the way, it's pretty impressive, so that

261
00:30:53,920 --> 00:31:01,480
in mind, this is what we work towards to be impressive, not exactly, but to be free, to

262
00:31:01,480 --> 00:31:12,800
be pure, to be untroubled, and that's impressive, so thank you all for tuning in, wishing

263
00:31:12,800 --> 00:31:38,880
you all good practice, and have a good night.

