1
00:00:00,000 --> 00:00:07,000
Okay, so here's a question which has some interesting

2
00:00:07,000 --> 00:00:13,000
interesting aspects to it. So the question is I'm new here

3
00:00:13,000 --> 00:00:18,000
and have an interest in monastic life. How should I proceed?

4
00:00:18,000 --> 00:00:22,000
I think it's interesting is because it's a very general question.

5
00:00:22,000 --> 00:00:26,000
So we can explore it in many different ways.

6
00:00:26,000 --> 00:00:30,000
I don't know if you have anything. You want me to start it?

7
00:00:30,000 --> 00:00:36,000
No, no, go ahead. I will say something right now.

8
00:00:36,000 --> 00:00:38,000
Okay.

9
00:00:38,000 --> 00:00:44,000
So the first thing that hits me is, well, this is a very general question.

10
00:00:44,000 --> 00:00:47,000
Not quite clear. I'm new here.

11
00:00:47,000 --> 00:00:52,000
This has been your new to Buddhism or just new to our community or just new to

12
00:00:52,000 --> 00:00:57,000
monk radio. But I think more important than that is

13
00:00:57,000 --> 00:01:01,000
talking about having an interest in monastic life.

14
00:01:01,000 --> 00:01:06,000
Because it goes without saying, I think that it's easy to be interested in something.

15
00:01:06,000 --> 00:01:11,000
And not have it pan out or not have it become anything.

16
00:01:11,000 --> 00:01:19,000
It's not really, it's not really the best way to begin your

17
00:01:19,000 --> 00:01:24,000
career as a Buddhist or as a spiritual person,

18
00:01:24,000 --> 00:01:27,000
is to be interested in monastic life.

19
00:01:27,000 --> 00:01:31,000
We have to be clear what is the goal and what is the aim of our practice.

20
00:01:31,000 --> 00:01:35,000
Well, one thing I've tried to be fairly clear about in our centers that

21
00:01:35,000 --> 00:01:41,000
we're not really interested or we're not really trying to attract people

22
00:01:41,000 --> 00:01:44,000
with an interest in monastic life.

23
00:01:44,000 --> 00:01:49,000
We're trying to attract people with an interest in meditation,

24
00:01:49,000 --> 00:01:52,000
with an interest in becoming free from suffering.

25
00:01:52,000 --> 00:01:57,000
And so it's a little bit broader than just the monastic life.

26
00:01:57,000 --> 00:02:03,000
Because a person living an ordinary life can still

27
00:02:03,000 --> 00:02:06,000
practice meditation and can still develop themselves and come to

28
00:02:06,000 --> 00:02:08,000
understand the truth of life.

29
00:02:08,000 --> 00:02:11,000
You don't have to put on robes, you don't have to take up an

30
00:02:11,000 --> 00:02:15,000
omnisable and go walking through the village.

31
00:02:15,000 --> 00:02:21,000
The monastic life is simply a convention or a

32
00:02:21,000 --> 00:02:28,000
artifice that was developed and perfected by the Buddha,

33
00:02:28,000 --> 00:02:33,000
meant to allow people who were interested in how to keen interest

34
00:02:33,000 --> 00:02:36,000
in meditation in developing themselves,

35
00:02:36,000 --> 00:02:39,000
in dedicating their lives to the development of the mind

36
00:02:39,000 --> 00:02:42,000
and the purification of the mind.

37
00:02:42,000 --> 00:02:49,000
As a way for such people to see their way through,

38
00:02:49,000 --> 00:02:53,000
in the practice and to be free from all of the obstacles

39
00:02:53,000 --> 00:02:56,000
and distractions.

40
00:02:56,000 --> 00:03:04,000
So I say that it sometimes is the case where people focus too much

41
00:03:04,000 --> 00:03:08,000
energy on just being among or on living the monastic life,

42
00:03:08,000 --> 00:03:12,000
thinking that somehow they can escape their problems and go

43
00:03:12,000 --> 00:03:17,000
and live on a mountain in peace and bliss.

44
00:03:17,000 --> 00:03:20,000
There's this kind of idyllic idea.

45
00:03:20,000 --> 00:03:22,000
But when you get to the mountain, when you get to the forest,

46
00:03:22,000 --> 00:03:24,000
you see that it's not quite as idyllic as you think.

47
00:03:24,000 --> 00:03:28,000
And in fact, it comes with great hardship of its own.

48
00:03:28,000 --> 00:03:32,000
And it can lead to the certain

49
00:03:32,000 --> 00:03:34,000
defilements of its own as well.

50
00:03:34,000 --> 00:03:36,000
It's possible to become complacent as a monk

51
00:03:36,000 --> 00:03:39,000
and to get caught up in the monk's life itself

52
00:03:39,000 --> 00:03:43,000
and to lose sight of the goal of practicing meditation.

53
00:03:43,000 --> 00:03:48,000
If your goal to start out with was not the practice of meditation,

54
00:03:48,000 --> 00:03:53,000
it's easy to develop specific monastic defilements,

55
00:03:53,000 --> 00:03:56,000
where you become lazy or you become conceited,

56
00:03:56,000 --> 00:03:59,000
you can become attached to fame,

57
00:03:59,000 --> 00:04:03,000
if you become a teacher and you can become attached to study

58
00:04:03,000 --> 00:04:09,000
and intellectual activity if you become a study monk and so on.

59
00:04:09,000 --> 00:04:15,000
So your interest in a monastic life is not so interesting to

60
00:04:15,000 --> 00:04:18,000
or it's something that I would at least caution you about

61
00:04:18,000 --> 00:04:21,000
because why do you want to become a monastic?

62
00:04:21,000 --> 00:04:24,000
This is what you should be able to answer for yourself

63
00:04:24,000 --> 00:04:26,000
at the very beginning.

64
00:04:26,000 --> 00:04:29,000
Before I would ask how you should proceed,

65
00:04:29,000 --> 00:04:32,000
I would say ask yourself why become a monastic

66
00:04:32,000 --> 00:04:36,000
because monastic life is not a valid goal.

67
00:04:36,000 --> 00:04:38,000
So if your goal is to become a monk,

68
00:04:38,000 --> 00:04:41,000
then I would say you're shooting a bit low.

69
00:04:41,000 --> 00:04:43,000
That's the first thing I would say.

70
00:04:43,000 --> 00:04:47,000
I don't know if you can continue that and I'll come back to it.

71
00:04:47,000 --> 00:04:53,000
Yes, I can just say I agree so far with every word you said.

72
00:04:56,000 --> 00:04:58,000
Anything to add?

73
00:04:58,000 --> 00:05:05,000
In general, I think it's important to have

74
00:05:05,000 --> 00:05:13,000
apart from the high goal of becoming free from suffering.

75
00:05:13,000 --> 00:05:17,000
You should find a place and more important

76
00:05:17,000 --> 00:05:23,000
than that you should find the teacher you want to ordain with.

77
00:05:23,000 --> 00:05:28,000
I think that is the most important of everything.

78
00:05:28,000 --> 00:05:34,000
The person you are ordaining with will be your guidance

79
00:05:34,000 --> 00:05:37,000
for the next couple of years,

80
00:05:37,000 --> 00:05:40,000
for men in general five years.

81
00:05:40,000 --> 00:05:43,000
For women, it's a little bit different.

82
00:05:43,000 --> 00:05:55,000
It's two years with six precepts and then you stay with a preceptor

83
00:05:55,000 --> 00:06:00,000
for another two years after the full ordination.

84
00:06:00,000 --> 00:06:14,000
But then the teacher is kind of say that it's not easy to understand.

85
00:06:14,000 --> 00:06:20,000
It's a very deep bond that is happening between disciple

86
00:06:20,000 --> 00:06:27,000
and teacher or preceptor and ordain student.

87
00:06:27,000 --> 00:06:32,000
It's like it's deeper like father and son

88
00:06:32,000 --> 00:06:35,000
or mother and daughter or father and daughter.

89
00:06:35,000 --> 00:06:42,000
It's a very intensive relationship

90
00:06:42,000 --> 00:06:49,000
and one should be very clear that

91
00:06:49,000 --> 00:06:53,000
one is going to live in a community,

92
00:06:53,000 --> 00:07:02,000
maybe large or small, but one has to learn to give up

93
00:07:02,000 --> 00:07:07,000
certain personal preferences.

94
00:07:07,000 --> 00:07:12,000
One has to give up a certain ways of life.

95
00:07:12,000 --> 00:07:20,000
And for some teachers or in some places it's easy to do so

96
00:07:20,000 --> 00:07:25,000
and in other places or with other teachers you just can't.

97
00:07:25,000 --> 00:07:30,000
You won't because the chemistry is not right.

98
00:07:30,000 --> 00:07:41,000
There is just no good understanding between disciple and teacher.

99
00:07:41,000 --> 00:07:51,000
You run away. This is the most important.

100
00:07:51,000 --> 00:07:56,000
The teacher should be really a person of your confidence,

101
00:07:56,000 --> 00:07:58,000
of your trust.

102
00:07:58,000 --> 00:08:05,000
The second important is to choose where a place where you can live.

103
00:08:05,000 --> 00:08:15,000
Some people can't live in the forest or other people can't live in the city.

104
00:08:15,000 --> 00:08:20,000
So you should know what you can do.

105
00:08:20,000 --> 00:08:24,000
And if a city noise is bothering you,

106
00:08:24,000 --> 00:08:28,000
but not so much the insects,

107
00:08:28,000 --> 00:08:31,000
then you better off to the forest.

108
00:08:31,000 --> 00:08:36,000
So these are all things you should know before you

109
00:08:36,000 --> 00:08:39,000
think of ordaining.

110
00:08:39,000 --> 00:08:42,000
Of course then what Bantayutadamu already said,

111
00:08:42,000 --> 00:08:44,000
I don't have to repeat.

112
00:08:44,000 --> 00:08:47,000
That is really the most important.

113
00:08:47,000 --> 00:08:49,000
You have to have the goal.

114
00:08:49,000 --> 00:08:54,000
And in the Buddhist time it was the goal to become enlightened.

115
00:08:54,000 --> 00:08:59,000
That was the one and only reason to become a monastic.

116
00:08:59,000 --> 00:09:02,000
But the other thing is to realize that,

117
00:09:02,000 --> 00:09:04,000
I think I kind of mentioned it,

118
00:09:04,000 --> 00:09:09,000
but just to be clear that not only is becoming among not the goal,

119
00:09:09,000 --> 00:09:12,000
but it's also not,

120
00:09:12,000 --> 00:09:15,000
I should not say it wasn't even in the Buddha time,

121
00:09:15,000 --> 00:09:19,000
as romantic as it can be made out to me.

122
00:09:19,000 --> 00:09:22,000
It's not like in the movies as they say.

123
00:09:22,000 --> 00:09:32,000
So there are a lot of things that people find difficult about the monastic life,

124
00:09:32,000 --> 00:09:36,000
not just the insects, but having to put up with cold food

125
00:09:36,000 --> 00:09:38,000
and not the food that you want to put up with.

126
00:09:38,000 --> 00:09:40,000
Having to go on arms around people,

127
00:09:40,000 --> 00:09:43,000
some people even get a verse to that.

128
00:09:43,000 --> 00:09:51,000
The life, you really need to have developed this path in the past.

129
00:09:51,000 --> 00:09:54,000
I think becoming among is not an easy thing.

130
00:09:54,000 --> 00:09:58,000
One of the reasons why I started answering the way I did

131
00:09:58,000 --> 00:10:03,000
is because we hear this quite often people who want to become a monk

132
00:10:03,000 --> 00:10:06,000
and even fly all the way out here to become a monk,

133
00:10:06,000 --> 00:10:10,000
only to turn around and realize that they're not ready for it,

134
00:10:10,000 --> 00:10:13,000
that it's not for them.

135
00:10:13,000 --> 00:10:15,000
So from a Buddhist point of view,

136
00:10:15,000 --> 00:10:20,000
it really seems to be the case that a person has to have developed.

137
00:10:20,000 --> 00:10:25,000
This in the past has to have developed a sincere wish

138
00:10:25,000 --> 00:10:28,000
in a past life to get to this state.

139
00:10:28,000 --> 00:10:30,000
I suppose maybe that's putting it too high.

140
00:10:30,000 --> 00:10:32,000
It's not like it's some profound thing,

141
00:10:32,000 --> 00:10:35,000
but the meaning is that for anything, it's like this.

142
00:10:35,000 --> 00:10:37,000
Why do some people become lawyers?

143
00:10:37,000 --> 00:10:38,000
Some people become doctors?

144
00:10:38,000 --> 00:10:43,000
Why do some people incline towards intellectual activities

145
00:10:43,000 --> 00:10:47,000
and some people incline towards physical activities and so on?

146
00:10:47,000 --> 00:10:52,000
It's all has to do with the person's past inclinations.

147
00:10:52,000 --> 00:10:55,000
A person really has to have the past inclination

148
00:10:55,000 --> 00:10:57,000
to lead them to become a monk.

149
00:10:57,000 --> 00:11:01,000
I think it's an important part of it.

150
00:11:01,000 --> 00:11:06,000
So don't take it lightly and don't really be sure in yourself

151
00:11:06,000 --> 00:11:10,000
and moreover, dedicate yourself to meditation first

152
00:11:10,000 --> 00:11:13,000
because normally what happens is people have never done an

153
00:11:13,000 --> 00:11:15,000
event in intensive meditation course and they think

154
00:11:15,000 --> 00:11:18,000
I'll become a monk, I'll go and meditate for the rest of my life.

155
00:11:18,000 --> 00:11:20,000
They come and do a month of meditation and realize,

156
00:11:20,000 --> 00:11:23,000
whoa, there's much more inside than they thought.

157
00:11:23,000 --> 00:11:25,000
They miss their parents, they miss their family,

158
00:11:25,000 --> 00:11:27,000
they miss their friends, they've got a lot more attachments

159
00:11:27,000 --> 00:11:28,000
than they thought.

160
00:11:28,000 --> 00:11:31,000
So it's not that they dislike the idea of becoming a monk now

161
00:11:31,000 --> 00:11:33,000
but they realize that they're just not ready for it.

162
00:11:33,000 --> 00:11:37,000
Many people say I miss my family and they run back home.

163
00:11:37,000 --> 00:11:38,000
That kind of thing.

164
00:11:38,000 --> 00:11:41,000
It's not something to be taken lightly.

165
00:11:41,000 --> 00:11:44,000
It's because more than becoming a doctor or a lawyer,

166
00:11:44,000 --> 00:11:46,000
it is at the core of your being.

167
00:11:46,000 --> 00:11:49,000
The meditation practice and the path that we're following

168
00:11:49,000 --> 00:11:53,000
is very much going to change who you are.

169
00:11:53,000 --> 00:11:54,000
Who you were.

170
00:11:54,000 --> 00:11:56,000
Who you were.

171
00:11:56,000 --> 00:11:57,000
Okay.

172
00:11:57,000 --> 00:12:00,000
There's a question related to it.

173
00:12:00,000 --> 00:12:01,000
Continue.

174
00:12:01,000 --> 00:12:02,000
Go ahead.

175
00:12:02,000 --> 00:12:06,000
Bantapalanyani, who was your ordaining teacher.

176
00:12:06,000 --> 00:12:10,000
If we may know, yes, you may know, I think.

177
00:12:10,000 --> 00:12:16,000
My Pavatini, which is the name for the female preceptor,

178
00:12:16,000 --> 00:12:21,000
is Ayatata Aloka from North California.

179
00:12:21,000 --> 00:12:23,000
I was ordained in America.

180
00:12:23,000 --> 00:12:28,000
And the witnessing on the male sangha side

181
00:12:28,000 --> 00:12:35,000
was Bantapuna Ratana from the Babana society,

182
00:12:35,000 --> 00:12:40,000
also in America.

