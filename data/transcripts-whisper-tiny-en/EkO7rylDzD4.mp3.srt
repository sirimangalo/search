1
00:00:00,000 --> 00:00:07,920
Emma asked, she had a talk with a friend whose Muslim and the friend told her that Nirvana

2
00:00:07,920 --> 00:00:14,760
and Heaven are just the same, meaning that people who do good things will be rewarded.

3
00:00:14,760 --> 00:00:18,680
I think she's wrong, but I was not able to find good arguments.

4
00:00:18,680 --> 00:00:21,360
Can you help?

5
00:00:21,360 --> 00:00:33,040
I think they should answer this one, because one of us will possibly say something in

6
00:00:33,040 --> 00:00:38,640
accurate about this, but it's a kind of technical question.

7
00:00:38,640 --> 00:00:44,120
I want to be perfectly orthodox.

8
00:00:44,120 --> 00:00:49,360
I mean, there are many ways of answering this question.

9
00:00:49,360 --> 00:00:57,560
The Buddha did use the word sugati to describe both of or sagga even, sagga which means

10
00:00:57,560 --> 00:01:07,200
Heaven, but to describe Nirvana as well, or it seems anyway, that he would include it

11
00:01:07,200 --> 00:01:11,960
in a good destination, so kind of lumping them together.

12
00:01:11,960 --> 00:01:22,480
The idea of Heaven, the idea of Nirvana, but so you know, I mean, if you want to avoid an

13
00:01:22,480 --> 00:01:25,080
argument, you could just answer it in that way.

14
00:01:25,080 --> 00:01:29,360
You could say to the Muslim friend, well, yes, you know, that's the point, really, you

15
00:01:29,360 --> 00:01:35,920
do good, you get good, you do evil, you get evil, you get bad coming to you.

16
00:01:35,920 --> 00:01:36,920
And why?

17
00:01:36,920 --> 00:01:43,200
This Muslim friend is probably not going to be convinced by you that, oh, yes, there is

18
00:01:43,200 --> 00:01:48,080
no self, and there is no God, and there is no entity whatsoever, and everything that arises

19
00:01:48,080 --> 00:01:52,760
ceases and it's impermanence offering and non-self, and therefore we should let go of it

20
00:01:52,760 --> 00:01:57,000
and see things as just experiential in the present moment, give up past, give up future,

21
00:01:57,000 --> 00:02:01,800
et cetera, et cetera, probably not going to get very far with them.

22
00:02:01,800 --> 00:02:09,480
So rather than take them all the way to Nirvana or understanding a deep understanding of

23
00:02:09,480 --> 00:02:13,600
the difference between Nirvana and Heaven, well, encourage them in that, yes, do good deeds,

24
00:02:13,600 --> 00:02:18,600
and as a result, you know, encourage them in doing meditation, because there's actually

25
00:02:18,600 --> 00:02:25,040
Buddhism only adds one aspect to the teaching of other religions, and that's the purification

26
00:02:25,040 --> 00:02:26,360
of the mind.

27
00:02:26,360 --> 00:02:31,980
So the Buddha taught that there are three aspects to Buddhism, and Samapapasa, Akhara

28
00:02:31,980 --> 00:02:39,000
Nang, not doing any evil, Kuzla Supasampada, they're becoming full of good or the fulfillment

29
00:02:39,000 --> 00:02:46,520
of goodness, of wholesomeness, Satchitapario dapanang, and the purification of the mind.

30
00:02:46,520 --> 00:02:52,400
So it's really the purification of the mind that enters into a different route, because

31
00:02:52,400 --> 00:02:57,520
you might even say that Nibana is beyond good and evil, and the Buddha said, in the

32
00:02:57,520 --> 00:03:04,960
Nambapada, he said, this is an anara hand has gone beyond and given up good and evil.

33
00:03:04,960 --> 00:03:11,440
This is where why they fly free like a bird.

34
00:03:11,440 --> 00:03:21,400
But the point of that I would recommend for the Muslim is that it is to encourage them

35
00:03:21,400 --> 00:03:27,160
in the good deed of meditation as well, so to help them to see that if they want to do

36
00:03:27,160 --> 00:03:32,160
good deeds, it really has to come from a good mind, because even Islam agrees with such

37
00:03:32,160 --> 00:03:37,880
things that if your mind is full of anger, then it's a bad deed, if your mind is full

38
00:03:37,880 --> 00:03:40,760
of greed, then it's a bad deed, and so on.

39
00:03:40,760 --> 00:03:45,560
And they do have this rudimentary understanding of the importance of the mind.

40
00:03:45,560 --> 00:03:52,160
So for example, I've heard a teaching, I'm assuming it's orthodox Islam, that there was

41
00:03:52,160 --> 00:03:58,880
a story of a Christian in a Muslim, and they were fighting, and the Islam was winning,

42
00:03:58,880 --> 00:04:02,720
and he was about to kill the Christian, and then the Christian spit on him.

43
00:04:02,720 --> 00:04:07,440
And when the Christian spit on him, he got angry, and so he put his sword away, and he

44
00:04:07,440 --> 00:04:11,640
said, I can't kill you because I have anger in my mind.

45
00:04:11,640 --> 00:04:20,240
So yeah, if you're ever getting killed by a Muslim in soldier, just make a angry, I mean,

46
00:04:20,240 --> 00:04:23,720
it shows how rude I mean, because obviously from Buddhist point to be we don't agree

47
00:04:23,720 --> 00:04:33,280
with that, you can't never kill without anger, without some cruelty in the mind.

48
00:04:33,280 --> 00:04:37,680
But that's really the point, and so helping them to see, you know, we're not asking

49
00:04:37,680 --> 00:04:42,120
to become Buddhist, we're just asking you to, we're just encouraging you to become a good

50
00:04:42,120 --> 00:04:43,120
person.

51
00:04:43,120 --> 00:04:53,920
So really if you want to be full of good deeds, and avoiding evil deeds, the performance

52
00:04:53,920 --> 00:04:58,560
of these deeds and the abstention from certain deeds isn't really enough, because the

53
00:04:58,560 --> 00:05:03,280
only way to really perform good deeds and to abstain from bad deeds is to purify your mind,

54
00:05:03,280 --> 00:05:08,000
to have a good source of your deeds.

55
00:05:08,000 --> 00:05:11,440
And there's a way to avoid argument and a way to encourage people in meditation, because

56
00:05:11,440 --> 00:05:20,240
ultimately the only way to overcome people's views is through meditation, and ultimately

57
00:05:20,240 --> 00:05:26,400
that's all they are is there's some belief, some view that is really this, it's part

58
00:05:26,400 --> 00:05:32,760
of this whole conceptual layer that is such a thin layer of our experience and of our

59
00:05:32,760 --> 00:05:33,960
minds.

60
00:05:33,960 --> 00:05:40,960
So you'll find that Christians, Muslims, Jews, people from any religion or any walk of life

61
00:05:40,960 --> 00:05:50,320
within a week of meditating, they can discard any number of views, it's in fact much less

62
00:05:50,320 --> 00:05:55,320
the views and more the addictions and defilements that tend to persist.

63
00:05:55,320 --> 00:06:02,280
If you just give them straight, straight, straight repass an insight meditation in line

64
00:06:02,280 --> 00:06:07,280
with the four foundations of mindfulness, if they practice it, in about a week they'll

65
00:06:07,280 --> 00:06:13,800
start to ease up on this sort of thing and not really care, unless you make it a point

66
00:06:13,800 --> 00:06:16,760
of controversy with them, you say, no, if you want to practice meditation, you have

67
00:06:16,760 --> 00:06:20,440
to give them God, if you want to practice meditation, you have to give them Heaven, you have

68
00:06:20,440 --> 00:06:25,000
to understand the difference between the Abana, if you start requiring these things and

69
00:06:25,000 --> 00:06:31,240
arguing with them about them and making it clear to them that there's something important

70
00:06:31,240 --> 00:06:34,600
here that you're getting wrong, then they're going to just cling harder and harder

71
00:06:34,600 --> 00:06:42,880
to them and then it could be a lifetime or a million, you know, thousands of lifetimes

72
00:06:42,880 --> 00:06:49,920
and they'll never get it, they'll never stop clinging because there's the conflict.

73
00:06:49,920 --> 00:06:54,720
So that's the first way I would answer this question is to avoid that sort of conflict.

74
00:06:54,720 --> 00:07:01,280
There was even there was a Iranian, Iranian man, I don't know if he's here tonight, but

75
00:07:01,280 --> 00:07:06,280
he was asking my teacher about this because he had to go back to Iran and he was afraid

76
00:07:06,280 --> 00:07:12,920
that, you know, it'd be very difficult to teach meditation there because the Iranian people

77
00:07:12,920 --> 00:07:21,600
are, or because the government will even put him in jail or even execute it for converting

78
00:07:21,600 --> 00:07:27,600
to another religion and for trying to convert other people to religion then, not believing

79
00:07:27,600 --> 00:07:34,440
in God and my teacher said to him, well, let's just say it's the teaching of the Lord.

80
00:07:34,440 --> 00:07:40,760
This is just the teaching of the Lord, he's using Thai language to explain it and so

81
00:07:40,760 --> 00:07:47,600
the word basically Lord and so he said because it's the law of nature, it's reality,

82
00:07:47,600 --> 00:07:54,120
it's the Lord, or you could say it's because it's the Lord Buddha, but basically he was

83
00:07:54,120 --> 00:07:58,080
saying it comes from the universe, it comes from the Creator.

84
00:07:58,080 --> 00:08:06,200
So the point being to just encourage people in meditation, don't worry about who it belongs

85
00:08:06,200 --> 00:08:12,440
to or what are the words that you use and so on.

86
00:08:12,440 --> 00:08:22,160
But if you're interested in the difference between Heaven and Nirvana, it's that Heaven

87
00:08:22,160 --> 00:08:26,320
is a myth, well, it's basically Heaven is a myth and Nirvana is reality.

88
00:08:26,320 --> 00:08:27,320
Why?

89
00:08:27,320 --> 00:08:36,960
Because Heaven, the concept of Heaven is intrinsically based on a risen phenomenon, the idea

90
00:08:36,960 --> 00:08:40,240
of Heaven is that there is something that has arisen.

91
00:08:40,240 --> 00:08:53,200
And so it's describing a series of experiences with a term, with a conceptual name, namapanyati.

92
00:08:53,200 --> 00:08:59,120
So you're in Heaven and you're experiencing the glory of God and so on and you're praising

93
00:08:59,120 --> 00:09:06,160
God for eternity and there's all the virgins or whatever, the myths or whatever the truth

94
00:09:06,160 --> 00:09:11,520
is because there's some controversy about the use of the word virgin, it might just mean

95
00:09:11,520 --> 00:09:15,640
women, young women or something.

96
00:09:15,640 --> 00:09:20,600
But at any rate up in Heaven, the idea is that there is an experience and those experiences

97
00:09:20,600 --> 00:09:23,000
are impermanent suffering in itself.

98
00:09:23,000 --> 00:09:26,800
That's reality and that's the reality that ordinarily we can't see.

99
00:09:26,800 --> 00:09:32,960
So calling it Heaven, calling it whatever you like is saying nothing, it's still changing.

100
00:09:32,960 --> 00:09:35,720
Every moment is changing and therefore it can't be stable.

101
00:09:35,720 --> 00:09:44,840
There's no force that can keep it static because it's changing.

102
00:09:44,840 --> 00:09:47,360
Nibana on the other hand is free from arising.

103
00:09:47,360 --> 00:09:50,120
There is none of that experience arising.

104
00:09:50,120 --> 00:09:54,200
There's no seeing, no hearing, no smelling, no tasting, no feeling of thinking.

105
00:09:54,200 --> 00:09:58,360
Everything that would arise has ceased and has ceased without remainder.

106
00:09:58,360 --> 00:10:06,560
It's like the ultimate rest, the ultimate cessation, the ultimate freedom from suffering.

107
00:10:06,560 --> 00:10:11,960
It's the only possible alternative to a risen phenomenon.

108
00:10:11,960 --> 00:10:18,760
It's not some special state of a risen phenomenon or a risen experience, it's the only exception.

109
00:10:18,760 --> 00:10:25,280
Everything else that you could possibly ever imagine that could possibly arise in subject

110
00:10:25,280 --> 00:10:32,960
to the cessation, the young ginti samudeva dambung sabandamiron dambung, whatever reality,

111
00:10:32,960 --> 00:10:40,400
whatever thing really and truly arises, that thing must really and truly cease and from

112
00:10:40,400 --> 00:10:42,680
one moment to the next.

113
00:10:42,680 --> 00:10:49,320
So the only way out of that, the only exception that could ever exist in reality, logically

114
00:10:49,320 --> 00:10:54,560
and experientially, is cessation, this state of cessation.

115
00:10:54,560 --> 00:10:59,320
Now if you don't want to attain that, because there was actually this question was asked

116
00:10:59,320 --> 00:11:07,120
on our Q&A forum this yesterday or something and I was just answering it, and the person

117
00:11:07,120 --> 00:11:11,360
said, well there's someone who said, they're so afraid of this idea of cessation because

118
00:11:11,360 --> 00:11:13,600
what will happen to their self, right?

119
00:11:13,600 --> 00:11:19,880
It's the cessation of self, it's annihilation of self.

120
00:11:19,880 --> 00:11:24,240
And that's a whole other issue about the non-existence of self and so on.

121
00:11:24,240 --> 00:11:29,360
If you want to go and read that question, it's quite interesting.

122
00:11:29,360 --> 00:11:34,040
But so it's one thing to say that you don't want to attain it and of course there's various

123
00:11:34,040 --> 00:11:39,600
issues with that and how to help someone to see that there's nothing being annihilated,

124
00:11:39,600 --> 00:11:43,560
there's nothing existing in the first place, it's just those things that arise that

125
00:11:43,560 --> 00:11:50,320
have ceased, they don't arise again and it doesn't occur any arising.

126
00:11:50,320 --> 00:12:11,320
But just to see that this is the only alternative of cessation of suffering and so whether

127
00:12:11,320 --> 00:12:15,960
you want to realize it or not, it is the only alternative, it's the only thing that could

128
00:12:15,960 --> 00:12:22,520
possibly exist outside of that and the great thing about it is that it actually does lead

129
00:12:22,520 --> 00:12:29,280
to peace, happiness and freedom from suffering and it is actually far preferable to any

130
00:12:29,280 --> 00:12:36,800
other experience, it's in a whole other level of peace and happiness and because of its

131
00:12:36,800 --> 00:12:46,760
actual nature of providing freedom from this incessant arising and it actually is true

132
00:12:46,760 --> 00:12:51,880
peace and happiness and the person who realizes it has no doubt, that's why I sold up

133
00:12:51,880 --> 00:12:56,160
on that is free from doubt because they have none of this doubt about whether it's useful

134
00:12:56,160 --> 00:13:02,600
or not useful, they've come to realize it and so they understand what is of true benefit

135
00:13:02,600 --> 00:13:08,720
and that's the cessation of suffering so help in some way that answers your question.

