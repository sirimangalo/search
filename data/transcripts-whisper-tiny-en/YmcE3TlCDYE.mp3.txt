Good evening, everyone. Welcome to our evening done this session.
So last night we talked about mindfulness. We thought it would be good to follow that
up with the other half of the equation, which is Vipasana.
Once the other half of the equation is we describe this technique, the tradition that I follow
as Sati Patana Vipasana. Mahasi Sayyodha was maybe one of the first to use that phrase.
He used it in his book called Sati Patana Vipasana.
Whenever you take a course with my teacher in Thailand, you'll have you actually memorize a phrase.
Vipasana, Nainya, Sati Patana. Vipasana in line with the four Sati Patana, four foundations
of mindfulness. One body, two feelings, three mind, four dumbah.
You repeat that. So you get it through your head.
What are we practicing here?
Vipasana is an interesting word, or it's interesting what is said about it.
I often remember being told that Vipasana, you can't find Vipasana in the teptica.
A monk once told me that.
Now, for just looking for the word Vipasana, you can, of course, find that word in the teptica.
It's in the Buddha's words.
That you don't have the various explanations and the frameworks that have been made up to describe and teach Vipasana.
You don't have that really in the Buddha's words, or what we have of the Buddha's words.
But Vipasana is a concept, or as a thing.
It's very much a part of the Buddha's teaching.
I think it's fairly easy if you have your familiar with the texts to see why the later traditions have stressed this word.
They've used this word as a catch phrase to describe what we're practicing.
Nowadays, we hear about Vipasana meditation or insight meditation.
As I've said, it's not really that accurate. You don't practice Vipasana.
You practice Satipatana, you practice mindfulness.
But we call it Vipasana meditation, or if we call it Vipasana meditation, because this is what happens when you practice mindfulness.
You come, you call it Vipasana, you come to see clearly.
So anyone who says that this isn't, this is a later invention, or it's not something the Buddha came up with.
It doesn't really have a familiarity with the texts or hasn't really thought deeply, but what the texts are actually saying.
Vipasana comes up quite often, but more than that, you find again and again these phrases whenever the Buddha is talking about, or often when he's talking about cultivating wisdom or seeing things as they are.
He'll say things like, a dita nanwakami and a patikankay and a gatan. But jupyana jayodamang tata tata vipasati.
Don't go back to the past, don't worry about the future.
What's in the past is gone, what's in the future, hasn't come.
Whatever arises in the present moment in front of you, see that clearly. Vipasati.
Again, this word Vipasati, Vipasati is the verb, means he or she sees clearly, or however you want to translate Vipasana.
Blind is this world, and the Buddha and the local, this world is blind, very few see clearly.
That's what mindfulness is supposed to do.
Again and again, you have the Buddha talking about seeing clearly or seeing with wisdom.
When we talk about what is Vipasana, we often simplify it by saying Vipasana means to see three things.
Because the Buddha said, sabbay, sankara, anitya, di, yada, panya, yapasati, whoever sees with wisdom.
When one sees with wisdom that all formations are impermanent, sabbay, sankara, anitya.
Sabbay, sankara, dukha, all formations are dukha, which means suffering or unsatisfying as well.
Sabbay, dhamma, anitya, all dhammas, all realities formed or unformed or not self.
So, it's actually a figure of speech, and it makes sense that this wouldn't be something the Buddha used chiefly.
The Buddha used the word, or phrases, or the construct, panya, or panjana, tea.
But it means that panjana means to know panjana means fully.
So, more often, the Buddha uses the word, no.
Vipasana means, or pasana is a word that is when you see something, when you look at something and you see it.
But what's useful about it, of course, even in some ways more than wisdom, is that it reminds us that this isn't intellectual.
You have to see with wisdom, but you have to see for yourself another verb the Buddha uses is, satchikaroti.
Satchikaroti means to see for oneself, to know independently of belief or rationalization, logic, to really know for oneself, to experience really.
But so, what do we mean when we talk about Vipasana? What it refers to?
So, in brief, it refers to seeing the three characteristics, impermanence, suffering, non-self.
This is what Vipasana meditation should show you, and it's the way it's phrased often without much explanation.
You can make this teaching a little confusing. It's not like you're going to look, and you're going to say, oh, look, impermanence jumps out at you.
What it means is that our attachment to people places things, to pleasant experiences, our aversion to unpleasant experience, it comes from misunderstanding about reality.
We take certain things as stable, constant, lasting.
We take them as satisfying, or fixable.
We have the idea that by clinging, by fretting, by obsessing over things, we can find happiness.
If we work with our experiences, if we fix them just right, we can find happiness.
And we believe that there is substance, and there is control, and there is a self, and a soul, and a possessor.
All these things having to do with self, which is really a complex set of views.
But it relates to the idea that there is some essence to things, like people actually existing.
Or like bodies being under our control. I can move my arms and so on.
I think we're in control, because look at me, I'm moving my arms.
And so all of these turn out to be based on a poor grasp of reality.
And when you focus on reality, moment to moment, you start to say it's not quite the way we think it is.
That both the body and the mind are very much impermanent, and the world around us, very much impermanent.
We went through this today in our study group.
I think that was a really good section that we went through, because details.
What do you mean by impermanence?
This comes and it's gone.
When the next thing comes, that doesn't come.
The last thing doesn't come over. It's gone.
Every moment, when you're hungry, you experience the physical reality,
it's very harsh and unpleasant and ugly.
Everything is the physical form, it's unpleasant.
When you're satiated, everything is wonderful, and the physical form is comfortable.
It's pleasing.
It says, when you're aware of this, you can see how reality changes from moment to moment,
and how we experience it one moment.
It's very different from how we experience it the next.
Impermanence.
Start to see that, thereby we can't find satisfaction or real happiness
by clinging to things or by trying to fix things.
Because they're impermanent, but also because third, they're not self.
They're not under control.
So you think it's you moving your arms whenever you want.
While trying to try watching a simple movement, and we watch the stomach,
rising and falling.
Beginning, we think I'm the one making it rise and fall,
but as you watch and watch.
And when you walk, and you think I am walking, I'm making myself walk.
But when you really watch and see what's going on, you see, oh, I see.
There's actually just this interplay between body and mind, moments of mind's mind and body state.
There's no self involved.
And there you come to see how constructed and artificial it always.
This is the sort of thing that you start to let go.
When you see clearly in this way,
you start to see how the word sankara would actually mean.
It means that our reality is constructed.
It's like an artificial, it's like a robot.
We are like robots, constructs, and you become disenchanted
at the moment that they do okay.
So the second thing that we pass in a means is not just the three characteristics.
It also refers to this process of realization that comes as you start to see the three characteristics.
So it's a real shift because, you know,
much of what I'm saying might sound foreign to many of you.
This is the kind of thing that should sound wrong.
I mean, the idea that the body isn't under control,
the idea that things aren't stable,
the idea that you can't find satisfaction or happiness in the world.
This shouldn't sound quite wrong to us.
I mean, it's normal for that to be the case
because our ordinary understanding of reality is very different.
We think, you know, some sara is wonderful.
So much, so much to be found, so much to be sought out and gained.
But as you make this process so when you first step through the door
and you start to see things clearly, we pass in by using mindfulness,
by being objective, you know, you're not indoctrinating yourself
or taking up a perverse practice or a cultish practice.
You're just trying to see things objectively when you are perfectly
and strictly objective and honest with yourself.
Why does it change?
And so you go through a series of knowledges which, you know,
they're very much the same.
They go in the same line for everyone,
and that's why you can document them.
You can talk about the stages of insight.
Some people are, I think, rather skeptical.
It's always quite skeptical until I saw how it works.
You know, everyone is different and they won't all manifest the same,
but it's quite interesting how the mind goes through a progression,
but it's basically the progression of moving from seeing things as stable,
satisfying, and controllable to see them as an impermanent,
unsatisfying, and controlled.
And so it starts by seeing this discreetness of experience
as they arise and see if this one doesn't continue on to the next one.
And then you start to investigate that,
and the mind starts to appreciate this discreetness,
especially the cessation aspect.
And the mind starts to realize that given that this discreetness,
it means nothing lasts, it means nothing stays.
There's nothing that is a stable refuge,
and it starts to kind of obsess or really observe the cessation
that begins to see how unstable reality is.
It can be quite frightening at times,
but the mind starts to wake up.
The meditator begins to alert, become alert to the danger,
like standing on, walking on thin ice.
I thought this was a solid platform.
This body, this reality.
As you start to see everything cease,
you start to wonder, what can I hold on to?
And the mind is grasping for something to cling to.
And you begin to see that nothing's worth clinging to.
You see how the clinging is causing you stress and suffering.
It's causing you stress and suffering because nothing,
nothing that you're clinging to is stable, satisfying,
or controllable.
And that's when Nibida comes as you start to become disenchanted.
And as you become disenchanted, you start to feel trapped.
You really feel this constructed nature,
this artificial nature, everything feels like a trap.
Your body is a trap, the mind is a trap.
You feel like you're caught in the cage.
When you start to incline away, like a snake,
you may use this simile of a snake,
shedding off its skin.
The skin starts to feel kind of dead.
A fake, no?
And then one strives.
It's the higher stages of knowledge, and then one is on the path.
And there's no question if they continue where they're going to go.
Because at that point, they've got this unshakable conviction
based on strong observation.
Until eventually they become completely equanimous.
And the final stage of insight,
where we get to the peak of Fipasana,
is when you really see clearly, and you see that everything,
nothing is worth clinging.
You use clinging to.
You see everything simply as experiences that arise.
And that's the last of the Fipasana stages,
where you have perfect equanimity.
Doesn't mean you enter into a state where you're perfectly calm.
It means everything you experience, you see clearly as arising.
And ceasing, not as good or bad or me or mine.
But you become more and more refined in your observation.
There's no more reacting, no very little liking or disliking.
Until eventually, it's like this feedback loop
where it gets stronger and stronger.
And just because it's more and more refined,
you need mindfulness to build up such strength
that it's like a sonic boom,
the sound waves building up, building up, building up.
Because you're seeing clear and clear and clear.
To the point where there's the epiphany moment
where one sees the four noble truths.
One sees everything.
Everything in some sorrow.
The desire is worth clinging to.
And one thus turns away from it.
And one the mind withdraws.
It's like it drops away.
Completely relinquishes its hold on some sorrow
and enters into an Obama cessation.
Which is, of course, the ultimate goal of epipassana.
Epipassana is not for epipassana.
So our goal is not our actual goal isn't to see clearly.
Seeing clearly is only the stepping stone for becoming free.
Epipassana is this freedom from suffering.
That it's very hard to describe or understand.
It's the kind of thing you really have to experience for yourself,
even for just a moment.
And it changes you.
It changes you something drastic.
Such peace and relief.
And you find that you're all the things that you cling to.
Not all of them, but many of them.
You relinquish your hold.
You relinquish your ambition
for getting, for obtaining, for being,
for fixing, for controlling, for some sorrow.
You've given up your attachment or you've reduced it.
So that's epipassana.
Epipassana is this practice.
Epipassana is really why we focus on it.
It is because it's this progression.
It comes from the practice of mindfulness.
Mindfulness, we put first.
It's what we practice.
But what happens when you're mindful,
it's not just, oh, now I'm mindful.
Now I'm going to live my life in a better way.
No, you're going to change quite a bit.
Change the process of change is vipassana
when you guys you come to see clearly.
Well, many things about you and your life will change for the better.
The none for the worse.
It's not like you have to doubt about it.
In fact, it's not something that,
it's not the kind of change that you have to decide on.
It doesn't take a decision because it's all clear.
It's all unequivocally unarguably right.
The change is from your view through seeing clearly
without a doubt, proper course of action.
So after you've seen clearly, for someone to say,
are you sure?
That doubt doesn't arise in the mind.
You're absolutely sure because you've seen
far better than any argument or any text or any teaching could do.
You've seen clearly the right and the wrong way.
You've seen that which leads to happiness and that which leads to suffering.
That's what it means to see clearly.
That's what vipassana is.
And that's the demo for tonight.
So thank you all for tuning in.
We'll show you all the best.
