1
00:00:00,000 --> 00:00:22,560
Okay, what is the difference between Vipas and Adjana's and some of Adjana's?

2
00:00:22,560 --> 00:00:26,800
Is there an advantage of doing straightens out or some of that to be passed in every

3
00:00:26,800 --> 00:00:27,800
way first time?

4
00:00:27,800 --> 00:00:36,200
You just shouldn't think about these things, I'm not criticizing you, but this is where

5
00:00:36,200 --> 00:00:44,400
we are now, and so many people ask this question, it's really the most important question.

6
00:00:44,400 --> 00:00:50,600
We need to see the truth of reality, we have to eventually see the four noble truths,

7
00:00:50,600 --> 00:00:58,600
and they're very clearly to do only with the experience of reality as it is.

8
00:00:58,600 --> 00:01:06,600
Now how you get to that experience is the subject of a great amount of debate in modern times.

9
00:01:06,600 --> 00:01:11,320
The ancient commoners don't seem to have this sort of debate, they have different ways

10
00:01:11,320 --> 00:01:17,520
that you can go about it, but the thing is eventually you have to get to this point.

11
00:01:17,520 --> 00:01:23,440
If you want to do all sorts of mental gymnastics and get all sorts of magical powers

12
00:01:23,440 --> 00:01:31,800
in the meantime, then there are ways of doing that.

13
00:01:31,800 --> 00:01:37,320
If you want to first explore, I mean, there are so many different ways, if you want to

14
00:01:37,320 --> 00:01:44,520
become a Buddha, then man, you've got your cutout for you, if you want to become a private

15
00:01:44,520 --> 00:01:50,120
Buddha, then you've still got a lot of work cutout for you, incredible amount of work.

16
00:01:50,120 --> 00:01:54,360
If you want to become one of the Buddha's chief disciples, like if you look at the Buddha's

17
00:01:54,360 --> 00:02:01,280
two chief disciples, Mogulana took seven days to become an Arhan, Sariputta took 14 days

18
00:02:01,280 --> 00:02:04,720
to become an Arhan.

19
00:02:04,720 --> 00:02:08,880
So first of all the question, why did Sariputta take longer than Mogulana, Sariputta

20
00:02:08,880 --> 00:02:15,280
is the most excellent in wisdom, apart from the Buddha?

21
00:02:15,280 --> 00:02:19,720
And then on top of that, why did both of them take longer than all of their friends?

22
00:02:19,720 --> 00:02:22,600
Because the friends who came with them all got enlightened right away, the Buddha taught

23
00:02:22,600 --> 00:02:30,960
something in Bhum, they all became Arhan's, sitting there, listening to the Buddha talk.

24
00:02:30,960 --> 00:02:32,560
So why did it take longer?

25
00:02:32,560 --> 00:02:39,760
Because they had higher aspirations, Sariputta had exceptional aspirations, it's amazing

26
00:02:39,760 --> 00:02:43,440
that in 14 days they could accomplish what he did.

27
00:02:43,440 --> 00:02:52,680
But he came to understand reality in a profound way, if you read the Buddha that says

28
00:02:52,680 --> 00:03:01,720
it was seeing things one by one, he was able to go through everything, all parts of reality.

29
00:03:01,720 --> 00:03:05,480
But you don't really need, you don't really need to do all of that.

30
00:03:05,480 --> 00:03:13,280
So it's totally up to the individual, Kondanya, the Buddha's first disciple, the first person

31
00:03:13,280 --> 00:03:15,960
to realize the truth of the Buddha's teaching.

32
00:03:15,960 --> 00:03:19,520
He made a wish in a past life that he should be the first person to realize the Buddha's

33
00:03:19,520 --> 00:03:21,160
teaching.

34
00:03:21,160 --> 00:03:22,160
How did he do it?

35
00:03:22,160 --> 00:03:24,680
He would give away the first of everything.

36
00:03:24,680 --> 00:03:27,800
He gave away the first rice when it was unripe.

37
00:03:27,800 --> 00:03:32,840
He gave away the first rice when it was ripe on the stock, and then he gave away the

38
00:03:32,840 --> 00:03:37,320
first harvest of the rice.

39
00:03:37,320 --> 00:03:45,200
He gave away the first threshing of the rice, the first polishing of the rice, and so

40
00:03:45,200 --> 00:03:46,200
on.

41
00:03:46,200 --> 00:03:51,080
He always gave away the first, and then he made a determination that he should therefore

42
00:03:51,080 --> 00:03:54,600
become to see the truth first.

43
00:03:54,600 --> 00:04:00,720
His brother at the same time ended up giving like at the very end when it was all finished.

44
00:04:00,720 --> 00:04:02,800
He said, I'm not going to do it.

45
00:04:02,800 --> 00:04:08,240
Then at the end he gave away gifts of his rice, and then he made a determination, but

46
00:04:08,240 --> 00:04:12,920
then let me be the last person to realize the Buddha's teaching, and he was.

47
00:04:12,920 --> 00:04:19,920
He turned up to be Subaddha, who was the last person just to learn the Buddha's teaching

48
00:04:19,920 --> 00:04:26,600
from the Buddha's own mouth.

49
00:04:26,600 --> 00:04:30,960
So there are, you have to recognize the different paths.

50
00:04:30,960 --> 00:04:35,200
There's even a sutra that we always quote, and it's always subject of so much debate,

51
00:04:35,200 --> 00:04:40,120
where these guys said, we don't have magical powers, we don't have all these Arupa

52
00:04:40,120 --> 00:04:42,360
Janas.

53
00:04:42,360 --> 00:04:46,320
All we have is the basic Janas required for attaining insight.

54
00:04:46,320 --> 00:04:53,320
We are Pandya, we Muddhi, and so what the commentary says is, well, there's two, even

55
00:04:53,320 --> 00:04:57,440
the Buddha said, actually, there are two kinds of, we Muddhi, two kinds of people who

56
00:04:57,440 --> 00:04:58,440
become free.

57
00:04:58,440 --> 00:05:03,600
Pandya, we Muddhi, and the Jita, we Muddhi, Jita, we Muddhi, they do some at the first, and

58
00:05:03,600 --> 00:05:06,640
then continue on with Vipasana.

59
00:05:06,640 --> 00:05:15,600
Pandya, we Muddhi, they do straight Vipasana, and the Jana that they get is the bare minimum.

60
00:05:15,600 --> 00:05:23,920
So, and the commentaries are very clear on this, there's really no controversy here,

61
00:05:23,920 --> 00:05:28,840
except that now people don't accept this, and they say, no, no, it's not, throw out

62
00:05:28,840 --> 00:05:34,520
the commentaries, right, and reinterpret what the Buddha said in a new way.

63
00:05:34,520 --> 00:05:40,160
So you're asking me, I'm going to explain to you about this whole samatha Janasana,

64
00:05:40,160 --> 00:05:41,160
Vipasana, Jana.

65
00:05:41,160 --> 00:05:45,200
According to the commentaries, according to my understanding, but other people will

66
00:05:45,200 --> 00:05:47,960
certainly disagree with me.

67
00:05:47,960 --> 00:05:51,800
Okay, let's talk about Jana.

68
00:05:51,800 --> 00:05:54,840
There are three kinds of Jana.

69
00:05:54,840 --> 00:05:58,440
The first kind of Jana is samatha Jana.

70
00:05:58,440 --> 00:06:04,520
The second kind of Jana is called, loosely called Vipasana Jana.

71
00:06:04,520 --> 00:06:08,960
They're actually, they have different names, but let's call them these ones for now.

72
00:06:08,960 --> 00:06:13,440
The third kind is called Lokutura Jana, and this is important.

73
00:06:13,440 --> 00:06:19,960
There are actually three kinds of Jana.

74
00:06:19,960 --> 00:06:30,240
The first kind, samatha Jana is, the commentary term is a Ramanupani Jana-toh, a person

75
00:06:30,240 --> 00:06:39,080
meditates on an Ramana, which means an object, but here specifically it's referring to

76
00:06:39,080 --> 00:06:42,760
an conceptual object, the samatha object.

77
00:06:42,760 --> 00:06:57,160
So there are the standard 40 objects, the 10 casinas, the 10 load sameness, contemplations,

78
00:06:57,160 --> 00:07:11,560
kasinsib asubasib, the 10 annusatis, the Buddha, the Dhamma, the Sangha, and so on.

79
00:07:11,560 --> 00:07:23,280
Then the four Brahmavihanas, the four Arupaka-matanas, then mindfulness of the falsiveness

80
00:07:23,280 --> 00:07:31,280
of eating, hari-patikudasanya, and dhatumana-sika, that's 40 altogether.

81
00:07:31,280 --> 00:07:40,200
Focusing on any one of these 40, or something similar, is to develop samatha Jana.

82
00:07:40,200 --> 00:07:46,680
It's based on a concept, so it's something that isn't going to lead to insight directly.

83
00:07:46,680 --> 00:07:54,240
You can't focus on the Buddha, say, and gain insight, based on thinking about the Buddha.

84
00:07:54,240 --> 00:08:00,320
You can't focus on a white disc and expect to gain insight, just thinking of the color

85
00:08:00,320 --> 00:08:01,640
white.

86
00:08:01,640 --> 00:08:02,640
Why?

87
00:08:02,640 --> 00:08:05,280
Because these things are conceptual.

88
00:08:05,280 --> 00:08:08,800
The Buddha actually is an exception, because the Buddha is, when we think Buddha, we're

89
00:08:08,800 --> 00:08:10,120
thinking of the mind-state.

90
00:08:10,120 --> 00:08:16,400
So Buddha can actually lead you to think about the mind-states, and therefore you see

91
00:08:16,400 --> 00:08:20,120
them arising and ceasing, and can lead to insight.

92
00:08:20,120 --> 00:08:22,600
It doesn't actually lead to Jana.

93
00:08:22,600 --> 00:08:26,120
According to the commentaries, Buddha knows that the mindfulness of the Buddha can't lead

94
00:08:26,120 --> 00:08:32,520
to the Jana's, because it's, well, they say, because the Buddha is a subject that's very

95
00:08:32,520 --> 00:08:36,200
deep, but also because it's dealing with mind-states.

96
00:08:36,200 --> 00:08:43,920
Mind-states aren't permanent, they're impermanence-affering and unsaid and uncontrollable.

97
00:08:43,920 --> 00:08:47,400
Therefore, it can't lead to Jana.

98
00:08:47,400 --> 00:08:49,840
This is the key here.

99
00:08:49,840 --> 00:08:54,040
The only way you can enter into a summit of Jana is if you focus on something that is going

100
00:08:54,040 --> 00:08:59,320
to be stable, and if you're focusing on something that is stable, you'll never see impermanence

101
00:08:59,320 --> 00:09:00,320
suffering and unsaid.

102
00:09:00,320 --> 00:09:02,640
So there's a distinction here.

103
00:09:02,640 --> 00:09:06,360
It's very simple, actually, and it's very clear, and if you don't listen to all these

104
00:09:06,360 --> 00:09:11,720
people who like to make trouble, it really makes a lot of sense, but when you start listening

105
00:09:11,720 --> 00:09:16,600
to them and then doubting and so on and then it, you know, if you're looking for trouble,

106
00:09:16,600 --> 00:09:18,960
you'll find it.

107
00:09:18,960 --> 00:09:27,600
We pass in a Jana, it's called by the commentaries, Lakanupani Jana, though, a person meditates

108
00:09:27,600 --> 00:09:29,480
on the characteristics.

109
00:09:29,480 --> 00:09:32,080
Lakana means characteristic.

110
00:09:32,080 --> 00:09:40,000
And here referring to the three characteristics of a phenomenon, the Samanilakana, the

111
00:09:40,000 --> 00:09:49,120
Lakanupani that the characteristics that are common to all real things.

112
00:09:49,120 --> 00:09:56,160
So you're meditating here, and you have Jana, but it's Jana based on reality, and you're

113
00:09:56,160 --> 00:10:03,880
focusing on an experience, a momentary experience that is arising and then ceasing, coming

114
00:10:03,880 --> 00:10:13,080
and going, and you're seeing impermanence, suffering, and on self.

115
00:10:13,080 --> 00:10:17,520
This Jana is necessary to become enlightened because it allows you to let go.

116
00:10:17,520 --> 00:10:21,320
It's the only way one can enter into the Banda.

117
00:10:21,320 --> 00:10:28,640
Samaneta meditation, as far as, you know, it seems so blatantly obvious that Samaneta meditation

118
00:10:28,640 --> 00:10:31,800
was practiced before the time of the Buddha.

119
00:10:31,800 --> 00:10:36,240
There were people gaining Jana left right in the center.

120
00:10:36,240 --> 00:10:40,000
And there are Buddhist teachers who say, this is not true, which is incredible for me

121
00:10:40,000 --> 00:10:52,480
to think that somehow the Jana's could be, you know, somehow people missed this state of

122
00:10:52,480 --> 00:10:57,000
peace and tranquility, of worldly peace and tranquility.

123
00:10:57,000 --> 00:11:00,320
And the peace and tranquility that they were describing, no, no, that was in Jana, it was

124
00:11:00,320 --> 00:11:02,880
something else.

125
00:11:02,880 --> 00:11:12,400
When, you know, in all religions, you have this kind of incredible peace and tranquility.

126
00:11:12,400 --> 00:11:18,480
The Hindus are experts in the Jana's, I mean, it's, it's, it's really a no brain or

127
00:11:18,480 --> 00:11:26,560
Jana is just, your mind becomes fixed on something, on a concept, that's Samaneta Jana.

128
00:11:26,560 --> 00:11:31,680
So what is really new is this idea of Yipasana, which is very clear in the Buddha's teaching,

129
00:11:31,680 --> 00:11:37,320
that you're trying to see often based on the, the tranquility of mind that you have that

130
00:11:37,320 --> 00:11:42,440
allows you to see clearly, but you're trying to see the, the nature of reality as being

131
00:11:42,440 --> 00:11:49,280
unsatisfying, as being stressful, not worth clinging to.

132
00:11:49,280 --> 00:11:55,520
And because you, you see that, then you let go, and when you let go, you're free.

133
00:11:55,520 --> 00:11:58,280
When you're free, you say, I'm free.

134
00:11:58,280 --> 00:12:06,840
So Yipasana Jana leads to the third type of Jana, which is, which is Lokutra Jana.

135
00:12:06,840 --> 00:12:12,920
So the reason why these monks, who said that they were panewimuti, the reason why they

136
00:12:12,920 --> 00:12:20,720
said, they said they had never developed a local, the, sorry, the Arupa Jana's, these were

137
00:12:20,720 --> 00:12:26,680
Alahans, these were enlightened beings, who claimed to never have realized the Arupa Jana,

138
00:12:26,680 --> 00:12:31,760
the formless Jana's, a part of the Samaneta Jana's.

139
00:12:31,760 --> 00:12:36,280
And people say, well, that means they must have practiced Samaneta Jana, because they

140
00:12:36,280 --> 00:12:39,760
only say that they didn't reach the Arupa Jana's.

141
00:12:39,760 --> 00:12:46,360
So what about the Rupa Jana's, the first four of the Samaneta Jana's?

142
00:12:46,360 --> 00:12:51,960
Well, the point is that what they're saying there, the reason why they have to say that

143
00:12:51,960 --> 00:12:58,160
they've realized the, the first, the Rupa Jana's, is because everyone has to reach

144
00:12:58,160 --> 00:13:03,440
a Jana, a technical state of Jana.

145
00:13:03,440 --> 00:13:10,080
This is why this, this Suta is so very carefully worded to not talk about the realization

146
00:13:10,080 --> 00:13:12,640
of the first Jana's.

147
00:13:12,640 --> 00:13:19,200
Because everyone has to realize a Jana, which is the Locutra Jana.

148
00:13:19,200 --> 00:13:25,440
If one enters into Nibana from the first Jana, this is the first Jana, this is the first

149
00:13:25,440 --> 00:13:26,440
Locutra Jana.

150
00:13:26,440 --> 00:13:30,480
If one enters from the second, it's the second Locutra Jana, third, it's the third, fourth,

151
00:13:30,480 --> 00:13:31,720
it's the fourth.

152
00:13:31,720 --> 00:13:35,520
If you enter from Vipasana, you're entering directly into the first Jana, but you still

153
00:13:35,520 --> 00:13:40,880
have to, it still is called the first Jana, but it's the first Locutra Jana.

154
00:13:40,880 --> 00:13:44,520
Confused, it's a bit confusing.

155
00:13:44,520 --> 00:13:54,160
If you don't study, if you don't study Abhidhamma, but Abhidhamma, it lays it out quite clearly.

156
00:13:54,160 --> 00:13:59,560
Anyway, I mean, it's, it's not really an important point.

157
00:13:59,560 --> 00:14:00,920
We practice to see clearly.

158
00:14:00,920 --> 00:14:04,040
I mean, you have to practice pretty intensely.

159
00:14:04,040 --> 00:14:09,000
I think the argument that many people would make is that people who advocate Vipasana

160
00:14:09,000 --> 00:14:13,440
meditation aren't really meditating, and I would agree with that to an extent that we

161
00:14:13,440 --> 00:14:21,080
often become lazy and intellectual, and we just sit around talking and thinking about it.

162
00:14:21,080 --> 00:14:25,640
And these people go off and do some pretty heavy meditation and congratulations and good

163
00:14:25,640 --> 00:14:26,640
for them.

164
00:14:26,640 --> 00:14:29,240
We should be doing heavy meditation as well.

165
00:14:29,240 --> 00:14:35,160
But the point being, you can do heavy meditation and, and just do Vipasana without doing

166
00:14:35,160 --> 00:14:36,720
the summit apart.

167
00:14:36,720 --> 00:14:40,840
I mean, there are people who became enlightened just listening to what the Buddha said, just

168
00:14:40,840 --> 00:14:42,920
listening to the Buddha taught.

169
00:14:42,920 --> 00:14:47,960
Now did they have to do summit to meditation and have all these theories about what that

170
00:14:47,960 --> 00:14:53,040
means and how they still have to do summit to meditation or whatever.

171
00:14:53,040 --> 00:14:54,440
It's all ludicrous.

172
00:14:54,440 --> 00:14:59,600
The important thing is that you see things clearly.

173
00:14:59,600 --> 00:15:04,160
So the, the only advantage I can see of doing straight inside first is that it's probably

174
00:15:04,160 --> 00:15:07,400
all things considered a little bit quicker.

175
00:15:07,400 --> 00:15:15,200
Because you're not worried about all the, all the trappings, you're not worried about magical

176
00:15:15,200 --> 00:15:23,360
powers or the enjoyment of the pleasure of the Janas of tranquility.

177
00:15:23,360 --> 00:15:27,320
You just want to see things clearly and quickly.

178
00:15:27,320 --> 00:15:31,880
So it could potentially be a lot quicker because you avoid all of the, you don't have

179
00:15:31,880 --> 00:15:39,520
all of the other ideas that you want this or that you want this, this piece and this happiness,

180
00:15:39,520 --> 00:15:40,760
worldly piece and happiness.

181
00:15:40,760 --> 00:16:09,920
You want to go for the alt.

