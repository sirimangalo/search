1
00:00:00,000 --> 00:00:08,660
And Masaki asked for someone who has an aspiration for a certain career and is also strongly

2
00:00:08,660 --> 00:00:16,980
thinking about ordaining is a career aspiration considered an attachment.

3
00:00:16,980 --> 00:00:43,260
I don't know that there really is a question, I mean, it's like trying any sort of dual

4
00:00:43,260 --> 00:00:48,780
intention, I mean, do you think somehow that it's possible to have a certain career

5
00:00:48,780 --> 00:00:56,700
and still ordain or you know, they still go towards ordaining, they're really heading

6
00:00:56,700 --> 00:01:02,380
in the opposite direction and they're totally incompatible, there's nothing compatible

7
00:01:02,380 --> 00:01:05,020
about them.

8
00:01:05,020 --> 00:01:10,700
So the career aspiration may not be considered, you don't have to consider an attachment

9
00:01:10,700 --> 00:01:15,620
and say the desire to ordain is a good one, but the desire to have a career is the bad

10
00:01:15,620 --> 00:01:16,620
one.

11
00:01:16,620 --> 00:01:20,180
All you have to say is that there are two different paths, if you want to get a career

12
00:01:20,180 --> 00:01:26,260
then go have a career, if you want to ordain ordain or they're both attachments really,

13
00:01:26,260 --> 00:01:30,780
I mean, they're both desires, really, they're both intentions in the mind, the intention

14
00:01:30,780 --> 00:01:35,340
to ordain the intention to have a career, you have to decide for yourself which is going

15
00:01:35,340 --> 00:01:43,420
to lead to the goals that you wish for, which are going to lead you to true peace happiness

16
00:01:43,420 --> 00:01:45,780
and freedom from suffering.

17
00:01:45,780 --> 00:01:52,140
So rather than even considered an attachment, we have to ask whether it's meaningful

18
00:01:52,140 --> 00:02:03,220
or meaningless because a career is in the unmeaningless, ordination in the end is meaningless,

19
00:02:03,220 --> 00:02:07,980
but ordination will allow you to attain that, which is meaningful, which is the cessation

20
00:02:07,980 --> 00:02:09,500
of suffering.

21
00:02:09,500 --> 00:02:15,140
So whatever you can do, whether it be ordaining or whether it just be simply practicing

22
00:02:15,140 --> 00:02:20,180
meditation, the point is to find that which is useful, the point isn't our attachment,

23
00:02:20,180 --> 00:02:26,260
the point is to work in such a way that you can find that which has true meaning, which

24
00:02:26,260 --> 00:02:49,500
is happiness and freedom from suffering, could maybe add, when you have the desire for ordination

25
00:02:49,500 --> 00:03:01,660
and the idea to become enlightened in this life or in future life, then I wouldn't call

26
00:03:01,660 --> 00:03:14,380
going for a career, not an attachment, but an obstacle, a great hindrance, sometimes this

27
00:03:14,380 --> 00:03:25,900
is called Mara, which keeps you away or gets in your way of getting closer to the goal

28
00:03:25,900 --> 00:03:27,220
of enlightenment.

29
00:03:27,220 --> 00:03:36,980
Once you have this goal, everything that keeps you away from it, I would call Mara, Mara

30
00:03:36,980 --> 00:03:48,860
not as the evil dual, like a person, but a mind state that goes off track, a mind that

31
00:03:48,860 --> 00:04:15,980
is not focused enough on the goal to get there.

