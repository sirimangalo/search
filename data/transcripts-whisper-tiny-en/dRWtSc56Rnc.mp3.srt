1
00:00:00,000 --> 00:00:07,000
Good evening, everyone. Welcome to our evening gathering.

2
00:00:14,000 --> 00:00:18,000
Tonight, I promise to talk about the middle way.

3
00:00:18,000 --> 00:00:34,000
Question was asked about what exactly the middle way means.

4
00:00:34,000 --> 00:00:40,000
I think it's important to talk about, first of all, because it's often

5
00:00:40,000 --> 00:00:53,000
this understood easily abused, but also because it does in many ways

6
00:00:53,000 --> 00:01:02,000
capture the essence of what we're trying to do if understood properly.

7
00:01:02,000 --> 00:01:07,000
It's a little bit misleading when we talk about the middle way,

8
00:01:07,000 --> 00:01:16,000
because it seems to sound like there's a happy medium in between two things

9
00:01:16,000 --> 00:01:25,000
where you don't go extreme on anything, but on the other hand you don't give up anything.

10
00:01:25,000 --> 00:01:33,000
And that's not generally speaking if I can speak for the Buddha, which of course is

11
00:01:33,000 --> 00:01:38,000
a frog with some danger.

12
00:01:38,000 --> 00:01:43,000
It's not generally how the middle way is understood.

13
00:01:43,000 --> 00:01:47,000
Take, for example, the first teaching of the Buddha, this is the most famous,

14
00:01:47,000 --> 00:01:58,000
the most well-known usage of this idea of the middle way.

15
00:01:58,000 --> 00:02:10,000
So, first of all, some background we're talking about.

16
00:02:10,000 --> 00:02:21,000
It's a talk given to five ascetics, five

17
00:02:21,000 --> 00:02:31,000
Hindu or Indian ascetics who had left Brahman society because they, like many before them,

18
00:02:31,000 --> 00:02:41,000
thought to themselves and realized that there was something quite wrong about the indulgence

19
00:02:41,000 --> 00:02:49,000
in central pleasures, so religion that was associated with sensuality

20
00:02:49,000 --> 00:02:54,000
where the priests and the

21
00:02:54,000 --> 00:03:04,000
supplicants or the applicants, those who practiced the sacrificial rituals

22
00:03:04,000 --> 00:03:06,000
that they really weren't getting anything out of it.

23
00:03:06,000 --> 00:03:13,000
So there was this movement in the time of the Buddha to leave behind

24
00:03:13,000 --> 00:03:18,000
and turn away from central pleasure with the realization that it was an addiction.

25
00:03:18,000 --> 00:03:23,000
It was something that kept you tied to Samsara.

26
00:03:23,000 --> 00:03:28,000
It was a strong, sort of mystical tradition.

27
00:03:28,000 --> 00:03:36,000
A lot of talk about rising above and getting beyond Samsara.

28
00:03:36,000 --> 00:03:41,000
Different ideas of what it meant, about to see through the illusion of sensuality

29
00:03:41,000 --> 00:03:50,000
and how to break free from the bonds of sensuality.

30
00:03:50,000 --> 00:04:04,000
And so what you had was a strong broad movement to go to the other extreme.

31
00:04:04,000 --> 00:04:06,000
Of course, it wasn't thought of this way.

32
00:04:06,000 --> 00:04:11,000
It was when you reject something and so the other thing is the right thing.

33
00:04:11,000 --> 00:04:21,000
The opposite for lack of a better word is the right thing.

34
00:04:21,000 --> 00:04:27,000
And so you had a lot of people in the time of the Buddha torturing themselves

35
00:04:27,000 --> 00:04:33,000
inflicting pain upon themselves, thinking that if pleasure is the wrong way

36
00:04:33,000 --> 00:04:38,000
then of course the right way must be whatever the opposite is pain.

37
00:04:38,000 --> 00:04:45,000
So if you torture yourself, you could free yourself from any desire for sensuality.

38
00:04:45,000 --> 00:04:52,000
It's kind of odd when you think about it, but there's a non- sort of logic to it.

39
00:04:52,000 --> 00:04:55,000
You beat it out of yourself.

40
00:04:55,000 --> 00:04:58,000
It's a moment we think this when we punish people.

41
00:04:58,000 --> 00:05:04,000
We punish children who are following their cravings by hitting them.

42
00:05:04,000 --> 00:05:08,000
We think if we give them pain somehow it'll make them want pleasure less.

43
00:05:08,000 --> 00:05:15,000
It's kind of an odd sort of reasoning, but we have this sort of reasoning.

44
00:05:15,000 --> 00:05:20,000
Say, if you hurt someone, have some reason be less inclined towards pleasure,

45
00:05:20,000 --> 00:05:24,000
which of course seems to be the opposite.

46
00:05:24,000 --> 00:05:28,000
It makes one want to rethink the whole idea of punishment,

47
00:05:28,000 --> 00:05:30,000
but it's of course our end-to-punishment.

48
00:05:30,000 --> 00:05:33,000
It's a very bad karma.

49
00:05:41,000 --> 00:05:44,000
But so this was the wrong view.

50
00:05:44,000 --> 00:05:46,000
This was the wrong idea.

51
00:05:46,000 --> 00:05:49,000
And when the Buddha came to teach the middle ways,

52
00:05:49,000 --> 00:05:54,000
then Vaimi, Bhikkhuvai, Anda, there are these two Anda.

53
00:05:54,000 --> 00:05:58,000
These two Anda is like the end of something, the extreme.

54
00:05:58,000 --> 00:06:02,000
It doesn't usually mean extreme, but it's used in that way here.

55
00:06:02,000 --> 00:06:10,000
It means the extremity, the very end of some things.

56
00:06:10,000 --> 00:06:16,000
They have two ends of the spectrum, basically.

57
00:06:16,000 --> 00:06:21,000
And so it sounds like he's saying that somewhere in the between there's a happy medium,

58
00:06:21,000 --> 00:06:25,000
where you don't go overboard with sensuality.

59
00:06:25,000 --> 00:06:31,000
But on the other hand, you don't torture yourself too much.

60
00:06:31,000 --> 00:06:38,000
And so we find meditators often taking up this view of a moderate view.

61
00:06:38,000 --> 00:06:43,000
It's okay to engage in some addictions.

62
00:06:43,000 --> 00:06:47,000
If you're addicted to chocolate, it's okay to enjoy the chocolate.

63
00:06:47,000 --> 00:06:49,000
It's just some moderation.

64
00:06:49,000 --> 00:06:52,000
Just don't have too much alcohol.

65
00:06:52,000 --> 00:06:54,000
Some people go to the extent.

66
00:06:54,000 --> 00:06:57,000
Buddhist go to the extent of moderation in alcohol.

67
00:06:57,000 --> 00:07:02,000
Don't have enough to get drunk, just enough to get a little happy.

68
00:07:02,000 --> 00:07:09,000
A little librarians, I suppose.

69
00:07:09,000 --> 00:07:14,000
And don't push yourself too hard, but you have to push yourself.

70
00:07:14,000 --> 00:07:20,000
You have to force yourself to do things, because who wants to really spend weeks and months meditating?

71
00:07:20,000 --> 00:07:22,000
Don't push yourself to do it.

72
00:07:22,000 --> 00:07:24,000
Force yourself a little bit, but not too much.

73
00:07:24,000 --> 00:07:29,000
We maybe don't use the word force, but it was sense that you have to push yourself.

74
00:07:29,000 --> 00:07:32,000
Don't push yourself too hard, moderation.

75
00:07:32,000 --> 00:07:35,000
It's not at all really what the Buddha meant.

76
00:07:35,000 --> 00:07:40,000
We can see that from the texts.

77
00:07:40,000 --> 00:07:48,000
It's sensuality of any of any sorts is useless.

78
00:07:48,000 --> 00:07:50,000
Not just useless, it's problematic.

79
00:07:50,000 --> 00:07:52,000
It's wrong.

80
00:07:52,000 --> 00:07:53,000
Indulgence.

81
00:07:53,000 --> 00:07:55,000
It's not sensuality, exactly.

82
00:07:55,000 --> 00:07:58,000
Of course, experiencing sensuality is not wrong.

83
00:07:58,000 --> 00:08:03,000
Looking at something beautiful is not a problem, but indulging in it in the sense of enjoying it,

84
00:08:03,000 --> 00:08:13,000
appreciating it, liking it, liking it, is the point.

85
00:08:13,000 --> 00:08:25,000
The Buddha where the Buddha said he made a nupagama means nupagama means not approaching these two,

86
00:08:25,000 --> 00:08:32,000
not going to these two extremes.

87
00:08:32,000 --> 00:08:42,000
The Buddha found the middle way.

88
00:08:42,000 --> 00:08:46,000
We think somewhere in the middle is where you have to find.

89
00:08:46,000 --> 00:08:49,000
Maybe it's a very fine line, but it's somewhere in between these two.

90
00:08:49,000 --> 00:08:51,000
When in fact, it really isn't.

91
00:08:51,000 --> 00:08:54,000
If you look at what the Buddha talked about as being the middle way,

92
00:08:54,000 --> 00:08:57,000
it has nothing to do with either of the others.

93
00:08:57,000 --> 00:09:04,000
It's actually exposed as a mere artifice of the Buddha.

94
00:09:04,000 --> 00:09:07,000
If you understand in the context, it makes sense.

95
00:09:07,000 --> 00:09:10,000
He was trying to say, look, you've gone to the other extreme,

96
00:09:10,000 --> 00:09:12,000
and it's just as useless as the other extreme.

97
00:09:12,000 --> 00:09:14,000
They're both useless, as what he was saying.

98
00:09:14,000 --> 00:09:16,000
Forget about those two.

99
00:09:16,000 --> 00:09:23,000
Let's try a third alternative, actually, to one extent.

100
00:09:23,000 --> 00:09:31,000
To some extent, though, it feels very much like being in the middle.

101
00:09:31,000 --> 00:09:36,000
As with others, another will go through a couple of other ways that it can be.

102
00:09:36,000 --> 00:09:42,000
You can understand or it was used in the polycan and in the commentary.

103
00:09:42,000 --> 00:09:44,000
But it's very much in the middle.

104
00:09:44,000 --> 00:09:46,000
It's not just a third alternative.

105
00:09:46,000 --> 00:09:49,000
We found another extreme to go to.

106
00:09:49,000 --> 00:10:04,000
In a sense, it's the extreme of being free from something,

107
00:10:04,000 --> 00:10:13,000
from any sort of extremism, if that makes any sense.

108
00:10:13,000 --> 00:10:16,000
So if you think about what the aidful noble path,

109
00:10:16,000 --> 00:10:21,000
what the middle way is, the aidful noble path, if you look at it.

110
00:10:21,000 --> 00:10:26,000
It's very much about not this nor that or anything, really.

111
00:10:26,000 --> 00:10:33,000
So when sensuality comes up, it's not about any sort of moderation.

112
00:10:33,000 --> 00:10:40,000
But the other hand, it's not about rejection or avoidance or self-toucher.

113
00:10:40,000 --> 00:10:43,000
So in a sense, it is bright in that fine line in the middle.

114
00:10:43,000 --> 00:10:46,000
There's nothing to do with the other two.

115
00:10:46,000 --> 00:10:50,000
When you experience something beautiful, for example.

116
00:10:50,000 --> 00:10:54,000
When there's nothing beautiful about what you're seeing,

117
00:10:54,000 --> 00:10:57,000
the beautiful, ugly, is really meaningless.

118
00:10:57,000 --> 00:11:02,000
It's all very much in our minds, according to our past habits

119
00:11:02,000 --> 00:11:08,000
and our genetic makeup and so on.

120
00:11:08,000 --> 00:11:16,000
So the middle way is really the more simple experiencing objective experience.

121
00:11:16,000 --> 00:11:19,000
When you see something to be aware that you're seeing it,

122
00:11:19,000 --> 00:11:23,000
when you feel pain to be aware that you're feeling pain.

123
00:11:23,000 --> 00:11:30,000
So this idea that somehow you should avoid this other idea of,

124
00:11:30,000 --> 00:11:33,000
this is the opposite thing in fact.

125
00:11:33,000 --> 00:11:39,000
So on the other hand, some people will say, you shouldn't see beautiful things.

126
00:11:39,000 --> 00:11:42,000
Take down all your, anything that could be beautiful.

127
00:11:42,000 --> 00:11:46,000
If there's music, plug your ears.

128
00:11:46,000 --> 00:11:51,000
Food, make sure you have only plain food, nothing delicious.

129
00:11:51,000 --> 00:11:53,000
Nothing that could be considered delicious.

130
00:11:53,000 --> 00:11:58,000
Don't ever eat chocolate, that would be, that would be bad.

131
00:11:58,000 --> 00:12:06,000
You know, the other end, don't ever expose yourself to pain.

132
00:12:06,000 --> 00:12:10,000
So, you have people who would say, you know,

133
00:12:10,000 --> 00:12:13,000
sitting through pain is a torch off torture.

134
00:12:13,000 --> 00:12:15,000
It wasn't very much against with the Buddha taught.

135
00:12:15,000 --> 00:12:17,000
Mahasi Sayada goes on about this guy here.

136
00:12:17,000 --> 00:12:21,000
He goes on and on about it.

137
00:12:21,000 --> 00:12:24,000
Not on and on but he explains it quite well.

138
00:12:24,000 --> 00:12:33,000
He says that these well-meaning meditation teachers will guide their students

139
00:12:33,000 --> 00:12:37,000
to always avoid pain when pain comes up to change position,

140
00:12:37,000 --> 00:12:40,000
always be in a comfortable position, so you never experience pain.

141
00:12:40,000 --> 00:12:43,000
But he says, it's quite misguided.

142
00:12:43,000 --> 00:12:49,000
The Buddha, in fact, is quite clearly one of the texts that we have.

143
00:12:49,000 --> 00:12:57,000
He says that you should be willing to bear with pain, even if it kills you.

144
00:12:57,000 --> 00:13:00,000
Even deathly pain.

145
00:13:00,000 --> 00:13:08,000
A great catalyst for enlightenment.

146
00:13:08,000 --> 00:13:16,000
So, the middle way in this sense, I mean, it's just talking about a way that is free from any kind of reaction.

147
00:13:16,000 --> 00:13:21,000
A way that is free from any kind of ambition or drive or exertion.

148
00:13:21,000 --> 00:13:25,000
It's in fact effortless.

149
00:13:25,000 --> 00:13:29,000
The reason why meditation seems like such a chore is because we're so lazy,

150
00:13:29,000 --> 00:13:34,000
because we're so much to the indulgent side.

151
00:13:34,000 --> 00:13:39,000
Or because we're forcing it, we're practicing self-torture.

152
00:13:39,000 --> 00:13:41,000
We meditate, we force our minds.

153
00:13:41,000 --> 00:13:43,000
No, stay with the stomach.

154
00:13:43,000 --> 00:13:44,000
No, stay with the foot.

155
00:13:44,000 --> 00:13:46,000
No, don't go wandering.

156
00:13:46,000 --> 00:13:49,000
Don't get greedy, don't kidding.

157
00:13:49,000 --> 00:13:53,000
We fall to the extremes.

158
00:13:53,000 --> 00:13:56,000
We fall to the streams because we're not present.

159
00:13:56,000 --> 00:14:00,000
We're not in the center, in the middle.

160
00:14:00,000 --> 00:14:05,000
So it's not exactly a spectrum.

161
00:14:05,000 --> 00:14:09,000
It's not that you take everything to moderation.

162
00:14:09,000 --> 00:14:13,000
It's freedom from them both, which puts you right in the center, which you centered.

163
00:14:13,000 --> 00:14:14,000
Pure.

164
00:14:14,000 --> 00:14:17,000
I mean, if you think purity, what is purity?

165
00:14:17,000 --> 00:14:23,000
It's the freedom from defilement.

166
00:14:23,000 --> 00:14:33,000
Clarity is the freedom from being clouded, being obscure, being blurred.

167
00:14:33,000 --> 00:14:35,000
What is right?

168
00:14:35,000 --> 00:14:37,000
You look at the eight full noble path.

169
00:14:37,000 --> 00:14:44,000
It's all about right, some entity right view.

170
00:14:44,000 --> 00:14:48,000
And right view is really, for the most part,

171
00:14:48,000 --> 00:14:53,000
just not having any wrong view.

172
00:14:53,000 --> 00:14:56,000
It's about having views of reality.

173
00:14:56,000 --> 00:15:01,000
What is nature of reality?

174
00:15:01,000 --> 00:15:05,000
There's nothing, it's not like the middle of the right view

175
00:15:05,000 --> 00:15:08,000
is just knowledge of the form of the truth, which there's nothing.

176
00:15:08,000 --> 00:15:10,000
It's not like that's moderate.

177
00:15:10,000 --> 00:15:12,000
It's in between two things.

178
00:15:12,000 --> 00:15:15,000
It's just freedom from any other kind of view.

179
00:15:15,000 --> 00:15:17,000
And so on.

180
00:15:17,000 --> 00:15:22,000
If you look at the eight full noble path, you can clearly see it's not at all about moderation.

181
00:15:22,000 --> 00:15:29,000
It's about being right and pure in the sense of not having anything that diverts you.

182
00:15:29,000 --> 00:15:35,000
It takes you to an extreme.

183
00:15:35,000 --> 00:15:40,000
The other way that the middle way is used is,

184
00:15:40,000 --> 00:15:42,000
what's used in different ways.

185
00:15:42,000 --> 00:15:50,000
Another way it's abused is to give the idea that the Buddha didn't teach non-self.

186
00:15:50,000 --> 00:15:53,000
Or that non-self doesn't mean there is no self.

187
00:15:53,000 --> 00:15:56,000
And in fact, it doesn't mean there is no self.

188
00:15:56,000 --> 00:16:00,000
It's important to nuance this and be careful.

189
00:16:00,000 --> 00:16:04,000
So people say, well, because it doesn't mean there is no self.

190
00:16:04,000 --> 00:16:07,000
The Buddha never, as far as I know, said that.

191
00:16:07,000 --> 00:16:10,000
He certainly didn't deny it.

192
00:16:10,000 --> 00:16:14,000
But as I've said many times, this is because of the nature of reality.

193
00:16:14,000 --> 00:16:15,000
There is no self.

194
00:16:15,000 --> 00:16:19,000
It's just a statement that doesn't make any sense.

195
00:16:19,000 --> 00:16:22,000
It's like saying there is no cat.

196
00:16:22,000 --> 00:16:27,000
Well, cats are not things that exist and neither are selves.

197
00:16:27,000 --> 00:16:29,000
So you say, well, cats don't exist.

198
00:16:29,000 --> 00:16:30,000
What do you mean?

199
00:16:30,000 --> 00:16:31,000
They have a cat.

200
00:16:31,000 --> 00:16:33,000
So it's confusing.

201
00:16:33,000 --> 00:16:35,000
A cat is an abstraction.

202
00:16:35,000 --> 00:16:38,000
It's on a whole other level of reality.

203
00:16:38,000 --> 00:16:42,000
Non-self is dealing with reality.

204
00:16:42,000 --> 00:16:48,000
So the way the Buddha explained it, people are saying that when you die,

205
00:16:48,000 --> 00:16:52,000
the soul continues on.

206
00:16:52,000 --> 00:16:55,000
When the body dies, the soul enters another body.

207
00:16:55,000 --> 00:16:57,000
The soul is eternal.

208
00:16:57,000 --> 00:17:00,000
And other people would say, no, when you die,

209
00:17:00,000 --> 00:17:02,000
there is a cessation.

210
00:17:02,000 --> 00:17:03,000
There's nothing.

211
00:17:03,000 --> 00:17:05,000
There's no experience, nothing.

212
00:17:05,000 --> 00:17:08,000
And the Buddha said, these are two extreme views.

213
00:17:08,000 --> 00:17:13,000
These two views are wrong view.

214
00:17:13,000 --> 00:17:18,000
So he teaches dependent origination,

215
00:17:18,000 --> 00:17:19,000
which is in the middle.

216
00:17:19,000 --> 00:17:20,000
Why is it in the middle?

217
00:17:20,000 --> 00:17:27,000
Well, because it describes a causal chain of experience.

218
00:17:27,000 --> 00:17:32,000
And then it doesn't rely upon the conceptual abstraction

219
00:17:32,000 --> 00:17:35,000
of there being a self or something.

220
00:17:35,000 --> 00:17:39,000
Something that would be totally outside of the realm

221
00:17:39,000 --> 00:17:44,000
of experiential reality that we could call itself.

222
00:17:44,000 --> 00:17:49,000
So people get all worried or confused about this idea of

223
00:17:49,000 --> 00:17:52,000
non-self or did the Buddha actually say there was a self?

224
00:17:52,000 --> 00:17:56,000
And they really said, let go of all that stuff.

225
00:17:56,000 --> 00:17:58,000
The Buddhism is really an innocuous.

226
00:17:58,000 --> 00:18:01,000
He just didn't say anything, really.

227
00:18:01,000 --> 00:18:07,000
What he pointed out was that we have all these attachments to self.

228
00:18:07,000 --> 00:18:13,000
We have ego, we cling to things as being me as mine and so on.

229
00:18:13,000 --> 00:18:18,000
And so he advocated in giving up of self view,

230
00:18:18,000 --> 00:18:23,000
giving up of any view relating to self.

231
00:18:23,000 --> 00:18:26,000
I hope I don't sound like I'm trying to say that there is any sort of self.

232
00:18:26,000 --> 00:18:28,000
It's not.

233
00:18:28,000 --> 00:18:32,000
There's very much on the side of being non-self.

234
00:18:32,000 --> 00:18:35,000
That any way we could conceive of a self,

235
00:18:35,000 --> 00:18:38,000
any conceiving that might go on,

236
00:18:38,000 --> 00:18:40,000
that might arise in the mind.

237
00:18:40,000 --> 00:18:42,000
It's just an arisen phenomenon.

238
00:18:42,000 --> 00:18:44,000
This belief or this thought,

239
00:18:44,000 --> 00:18:46,000
hey, maybe there's a self.

240
00:18:46,000 --> 00:18:49,000
I feel like there's a self.

241
00:18:49,000 --> 00:18:51,000
All of that is problematic.

242
00:18:51,000 --> 00:18:56,000
It's a cause for stress and suffering because then there's clinging.

243
00:18:56,000 --> 00:18:59,000
And so he taught what he saw, what we can all clearly see

244
00:18:59,000 --> 00:19:04,000
that there is causal relationships between phenomena,

245
00:19:04,000 --> 00:19:06,000
between experiences.

246
00:19:06,000 --> 00:19:08,000
There's very much a middle way.

247
00:19:08,000 --> 00:19:13,000
It's sort of a view that it should not be considered extreme,

248
00:19:13,000 --> 00:19:15,000
but it is.

249
00:19:15,000 --> 00:19:19,000
This idea that any view of self could be problematic.

250
00:19:19,000 --> 00:19:21,000
It sounds very much like there is no self

251
00:19:21,000 --> 00:19:24,000
and then you think, well, what about myself?

252
00:19:24,000 --> 00:19:29,000
I was just reading the Wikipedia page on the middle way.

253
00:19:29,000 --> 00:19:34,000
And it claims that in terabyte of Buddhism,

254
00:19:34,000 --> 00:19:38,000
there's the idea that an arrow hunt upon passing

255
00:19:38,000 --> 00:19:41,000
neither exists nor non-exists.

256
00:19:41,000 --> 00:19:43,000
It says the polycannon says that.

257
00:19:43,000 --> 00:19:46,000
It doesn't give us citations.

258
00:19:46,000 --> 00:19:49,000
I'd like to have someone go in there and put a citation

259
00:19:49,000 --> 00:19:53,000
missing or something notation because

260
00:19:53,000 --> 00:19:55,000
pretty sure that's not what it says.

261
00:19:55,000 --> 00:19:58,000
I remember in the polycannon it says

262
00:19:58,000 --> 00:20:03,000
someone asks, does an arrow hunt exist after they pass away?

263
00:20:03,000 --> 00:20:05,000
No, that's not the case.

264
00:20:05,000 --> 00:20:07,000
Is it that they don't exist when they pass away?

265
00:20:07,000 --> 00:20:08,000
No, that's not the case.

266
00:20:08,000 --> 00:20:11,000
I mean, that's not proper to say.

267
00:20:11,000 --> 00:20:14,000
Well, do they both exist and not exist?

268
00:20:14,000 --> 00:20:15,000
No, that's not proper to say.

269
00:20:15,000 --> 00:20:17,000
Do they neither exist nor not exist?

270
00:20:17,000 --> 00:20:19,000
It says in Wikipedia.

271
00:20:19,000 --> 00:20:22,000
And the answer is no, that's not the case either.

272
00:20:22,000 --> 00:20:25,000
Because arrow hunt is just a concept.

273
00:20:25,000 --> 00:20:28,000
Existence is not like that existence is experience.

274
00:20:28,000 --> 00:20:34,000
All that exists is moments of experience.

275
00:20:34,000 --> 00:20:40,000
So sometimes these things that the Buddha said

276
00:20:40,000 --> 00:20:45,000
and sometimes it was just to avoid it because people get so confused

277
00:20:45,000 --> 00:20:50,000
because they're living in this intellectual or conceptual reality

278
00:20:50,000 --> 00:20:54,000
where I exist and there are people in places and things.

279
00:20:54,000 --> 00:21:01,000
We're very much extreme in this way.

280
00:21:01,000 --> 00:21:04,000
They've gone to an extreme of delusion where we believe

281
00:21:04,000 --> 00:21:11,000
that things exist where we have caught up in our conceptions

282
00:21:11,000 --> 00:21:19,000
or our sunya, we recognize things and it morphs into this belief

283
00:21:19,000 --> 00:21:23,000
for this conception of things as existing

284
00:21:23,000 --> 00:21:27,000
as having entities.

285
00:21:27,000 --> 00:21:38,000
A real middle way is based on the three characteristics

286
00:21:38,000 --> 00:21:42,000
and it's very much about not not not not.

287
00:21:42,000 --> 00:21:45,000
Not permanent, not stable, not lasting.

288
00:21:45,000 --> 00:21:49,000
Not pleasant, not satisfying, not happiness.

289
00:21:49,000 --> 00:21:53,000
Not me, not myself.

290
00:21:53,000 --> 00:21:58,000
What it means is we have all these attachments to things

291
00:21:58,000 --> 00:22:03,000
as being me and mine, as being pleasant, as being a source of happiness

292
00:22:03,000 --> 00:22:08,000
and as being stable, as being a refuge.

293
00:22:08,000 --> 00:22:11,000
We just learned today on the Visudhi manga, we got to this.

294
00:22:11,000 --> 00:22:16,000
One of the best passages I think are one of the most important passages

295
00:22:16,000 --> 00:22:22,000
I think in the Visudhi manga, not for the aspect of the Buddha's teaching

296
00:22:22,000 --> 00:22:26,000
that it describes, but just how well it describes the three characteristics.

297
00:22:26,000 --> 00:22:29,000
I mean, these are so important.

298
00:22:29,000 --> 00:22:32,000
So it gives 40 ways that come from, I think,

299
00:22:32,000 --> 00:22:35,000
the Patisambhida manga or maybe even earlier.

300
00:22:35,000 --> 00:22:40,000
40 ways by what you can understand, the three characteristics.

301
00:22:40,000 --> 00:22:43,000
We read through that. I encourage everyone.

302
00:22:43,000 --> 00:22:47,000
You can look it up. It's in the beginning or the halfway through,

303
00:22:47,000 --> 00:22:53,000
maybe the chapter on manga, manga, nyana, doesn't always seem to be purification

304
00:22:53,000 --> 00:22:58,000
by knowledge and vision of what is the path and what is not the path.

305
00:22:58,000 --> 00:23:01,000
When one first begins to grasp what is the path,

306
00:23:01,000 --> 00:23:03,000
what the path is this.

307
00:23:03,000 --> 00:23:08,000
The path is seeing through our illusions, our ignorance,

308
00:23:08,000 --> 00:23:14,000
seeing through our conception of permanence,

309
00:23:14,000 --> 00:23:21,000
stability and satisfaction and control

310
00:23:21,000 --> 00:23:24,000
to see impermanence suffering in non-self.

311
00:23:24,000 --> 00:23:28,000
As you start to grasp this, you make that shift

312
00:23:28,000 --> 00:23:32,000
when you start upon the noble path.

313
00:23:32,000 --> 00:23:34,000
That's really the middle way.

314
00:23:34,000 --> 00:23:39,000
When a person begins to see impermanence suffering in non-self,

315
00:23:39,000 --> 00:23:42,000
meaning they begin to give up or they begin to get a glimpse

316
00:23:42,000 --> 00:23:47,000
of what it means to let go and how one actually goes about letting go.

317
00:23:47,000 --> 00:23:51,000
It's quite simple as you start to see impermanence,

318
00:23:51,000 --> 00:23:54,000
you give up permanence or you give up those things

319
00:23:54,000 --> 00:23:58,000
that you were clinging to because you thought they were stable.

320
00:23:58,000 --> 00:24:01,000
As you start to see suffering, you give up those things

321
00:24:01,000 --> 00:24:03,000
that you thought were bringing you happiness.

322
00:24:03,000 --> 00:24:06,000
That weren't actually bringing you happiness.

323
00:24:06,000 --> 00:24:08,000
As you have started to see non-self,

324
00:24:08,000 --> 00:24:11,000
you give up those things that you thought were you and yours

325
00:24:11,000 --> 00:24:15,000
and belong to you.

326
00:24:15,000 --> 00:24:20,000
Once that clinging ceases, then suffering ceases,

327
00:24:20,000 --> 00:24:22,000
the suffering and the stress which comes

328
00:24:22,000 --> 00:24:27,000
from the ignorance and the delusion.

329
00:24:27,000 --> 00:24:30,000
So the middle way is very much just a simplification.

330
00:24:30,000 --> 00:24:33,000
I'm giving up those things that one might call extreme,

331
00:24:33,000 --> 00:24:37,000
and often call extreme because they end up being opposite.

332
00:24:37,000 --> 00:24:39,000
This is no good, so let's do this.

333
00:24:39,000 --> 00:24:41,000
This is no good, let's do this.

334
00:24:41,000 --> 00:24:43,000
There's this argument over the two,

335
00:24:43,000 --> 00:24:46,000
and the Buddha's just as neither is really good.

336
00:24:46,000 --> 00:24:49,000
In terms of our meditation, it does feel very much

337
00:24:49,000 --> 00:24:56,000
like being in the middle because you give up your desires

338
00:24:56,000 --> 00:24:59,000
and you give up your versions.

339
00:24:59,000 --> 00:25:02,000
Because you're patient with pleasant things.

340
00:25:02,000 --> 00:25:04,000
You're patient with unpleasant things, obviously.

341
00:25:04,000 --> 00:25:07,000
But you're also patient with pleasant things.

342
00:25:07,000 --> 00:25:10,000
When something pleasant comes, you have to be just as patient

343
00:25:10,000 --> 00:25:14,000
in the sense of not jumping for it.

344
00:25:14,000 --> 00:25:18,000
I think of something you want chocolate

345
00:25:18,000 --> 00:25:24,000
to find the patience and the ability to be with the experience

346
00:25:24,000 --> 00:25:32,000
wanting, wanting, for example, or liking or feeling feeling.

347
00:25:32,000 --> 00:25:35,000
As much as you are for pain, so when you feel unpleasant,

348
00:25:35,000 --> 00:25:38,000
sensation, just be pain, pain, or disliking,

349
00:25:38,000 --> 00:25:41,000
disliking, and you just be with them.

350
00:25:41,000 --> 00:25:43,000
That's really the path.

351
00:25:43,000 --> 00:25:48,000
When you experience disliking, you're not trying to shut it off

352
00:25:48,000 --> 00:25:52,000
or stop it, but you're also not going with it.

353
00:25:52,000 --> 00:25:56,000
When you're angry, when you're bored, when you're frustrated,

354
00:25:56,000 --> 00:25:58,000
and when you want something, you're not trying to shut it off.

355
00:25:58,000 --> 00:26:01,000
Say, no, bad, that's a problem.

356
00:26:01,000 --> 00:26:04,000
But you're also not going with it.

357
00:26:04,000 --> 00:26:05,000
You're objective.

358
00:26:05,000 --> 00:26:10,000
You say like King, like King, or wanting, let it go.

359
00:26:10,000 --> 00:26:14,000
Let it come, let it go.

360
00:26:14,000 --> 00:26:16,000
So there you go.

361
00:26:16,000 --> 00:26:20,000
There's a sort of abbreviated moderate.

362
00:26:20,000 --> 00:26:22,000
There's a moderate talk.

363
00:26:22,000 --> 00:26:28,000
Moderate length talk on the middle lane.

364
00:26:28,000 --> 00:26:30,000
So thank you all for tuning in.

365
00:26:30,000 --> 00:26:54,000
I'll show you all the best.

