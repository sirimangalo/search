1
00:00:00,000 --> 00:00:13,760
Okay, good evening, everyone.

2
00:00:13,760 --> 00:00:25,760
Welcome to our live broadcast, here in Second Life, in the Deer Park, and here in Hamilton,

3
00:00:25,760 --> 00:00:45,480
Ontario at our International Meditation Center.

4
00:00:45,480 --> 00:01:15,320
Now the sound is no good, is there an agreement on that, the sound is no good?

5
00:01:15,320 --> 00:01:23,920
Space and blurry, I don't know what that means.

6
00:01:23,920 --> 00:01:53,280
Is it peeking out, clipping, as I'm going to do?

7
00:01:53,280 --> 00:02:01,680
Testing one, two, test.

8
00:02:01,680 --> 00:02:31,520
It's fine here.

9
00:02:31,520 --> 00:02:43,200
Is it too loud, maybe?

10
00:02:43,200 --> 00:02:57,520
Alright, let me know if it's still really bad.

11
00:02:57,520 --> 00:03:23,520
So tonight I thought I'd talk about truths, symbolism, were interested in truth.

12
00:03:23,520 --> 00:03:41,200
Truth is an interesting concept, and I'm learning something quite interesting in university,

13
00:03:41,200 --> 00:03:49,720
just starting to realize that what we see in the history of Western civilization is an

14
00:03:49,720 --> 00:03:57,240
attempt at finding the truth, a failed attempt at finding the truth, a failed attempt

15
00:03:57,240 --> 00:04:04,640
at enlightenment, and even called this movement the enlightenment movement, it was the,

16
00:04:04,640 --> 00:04:09,440
and I don't really, I'm not an expert on it of course, so I may be getting it all wrong,

17
00:04:09,440 --> 00:04:16,400
but it seems there was a concerted effort to find the truth, and it failed.

18
00:04:16,400 --> 00:04:33,120
For the most part, I mean it may have succeeded for certain individuals who found Buddhism,

19
00:04:33,120 --> 00:04:52,280
but it didn't work for the world for society for most people, and so then we entered into

20
00:04:52,280 --> 00:05:02,640
and we've entered now into this post-enlightenment phase, or post-modernist phase, where people

21
00:05:02,640 --> 00:05:15,360
have come to the conclusion that there's no such thing as truth, truth is just relative,

22
00:05:15,360 --> 00:05:32,680
or truth isn't so important, what's important is relationships, heuristics, the individuals

23
00:05:32,680 --> 00:05:44,320
placed, the individuals' thoughts and feelings and impulses, which are important in Buddhism,

24
00:05:44,320 --> 00:05:52,800
and to some extent this deconstruction of artificial truth is important, there's most

25
00:05:52,800 --> 00:05:59,200
of what people have postulated to be true in the past when the history of humanity,

26
00:05:59,200 --> 00:06:03,640
most of what we postulated to be true turns out to not be true or to be only true in

27
00:06:03,640 --> 00:06:09,960
a relative sense, in a conventional sense, like Paris is the capital of France, kind of

28
00:06:09,960 --> 00:06:26,440
true, not craving is the cause of suffering, kind of true.

29
00:06:26,440 --> 00:06:35,240
And this goes quite deep, the sorts of things we think of as true are related to our conception

30
00:06:35,240 --> 00:06:48,600
of what's normal, so we follow, we get lulled into this concept that somehow being human

31
00:06:48,600 --> 00:06:58,360
is normal, we put humanity somewhere at the center and we think this is true, or this

32
00:06:58,360 --> 00:07:11,440
is real, and we privilege it over any other conceivable reality, of course we have a general

33
00:07:11,440 --> 00:07:23,240
sense of skepticism towards when we hear about angels and gods and so on.

34
00:07:23,240 --> 00:07:32,840
And it's reasonable, I mean there are reasonable aspects of that just skepticism, because

35
00:07:32,840 --> 00:07:35,440
we don't see angels and gods, right?

36
00:07:35,440 --> 00:07:57,760
And we don't see how realms.

37
00:07:57,760 --> 00:08:04,200
But we go beyond that and we think of humanity as somehow or our situation, whatever situation

38
00:08:04,200 --> 00:08:09,720
when we think of it as somehow real and somehow normal, and in fact it would be quite

39
00:08:09,720 --> 00:08:16,600
surprising if the human realm were the only realm, considering how odd it is, how bizarre

40
00:08:16,600 --> 00:08:27,440
is the human realm, how unnatural and artificial, how contrived, how specific, not even

41
00:08:27,440 --> 00:08:34,000
if you believe that the brain gives rise to consciousness, the idea that consciousness

42
00:08:34,000 --> 00:08:43,560
wouldn't have sprouted up in other ways that are more reasonable, less specific, it's

43
00:08:43,560 --> 00:08:49,880
kind of arrogant for us to think of humanity, and religion has done this, many religions

44
00:08:49,880 --> 00:08:56,320
have done this, thinking that God created man in the image of God, meaning we are somehow

45
00:08:56,320 --> 00:09:07,240
perfect, which is so far from the truth, if our appendix doesn't show that, you don't

46
00:09:07,240 --> 00:09:15,760
have to look far to see the imperfections of humanity.

47
00:09:15,760 --> 00:09:25,520
And so all of that is not the truth, the truth is quite simple.

48
00:09:25,520 --> 00:09:34,120
We have what we call the four noble truths, and we consider them to be absolute, and we

49
00:09:34,120 --> 00:09:38,680
consider certain things to be absolute truth.

50
00:09:38,680 --> 00:09:44,360
Things like seeing, seeing is absolutely seeing, there's no question, you can doubt whether

51
00:09:44,360 --> 00:09:52,120
seeing is seeing, you can doubt what you're seeing, am I really seeing a deer there in

52
00:09:52,120 --> 00:09:58,080
the side of this deer park, or is it just pixels, am I really seeing this computer in

53
00:09:58,080 --> 00:10:05,080
front of me, or am I actually tied in LinkedIn to some sophisticated virtual reality that

54
00:10:05,080 --> 00:10:13,240
makes me think I'm Buddhist monk, all of that is up for debate, but what isn't up for

55
00:10:13,240 --> 00:10:19,360
debate is that seeing is seeing, hearing is hearing, or you don't even like that going

56
00:10:19,360 --> 00:10:28,400
that far experience is experience, like Descartes said, go, go, go, zoom, there is thinking,

57
00:10:28,400 --> 00:10:36,000
you can't deny that, you can't be tricked into thinking that you're thinking, because

58
00:10:36,000 --> 00:10:45,920
you need to think in order to think.

59
00:10:45,920 --> 00:10:50,800
And so the first noble truth is, in some ways, just an expression of that, that seeing

60
00:10:50,800 --> 00:11:03,480
is seeing, hearing is hearing, that reality is what it is, it exists.

61
00:11:03,480 --> 00:11:07,600
So we talk about the first noble truth in being in terms of suffering, I mean that's the

62
00:11:07,600 --> 00:11:13,400
important quality of reality, but it's also just talking about understanding reality.

63
00:11:13,400 --> 00:11:16,600
The first number of truth isn't just that there is suffering, it's about understanding

64
00:11:16,600 --> 00:11:29,960
the nature of reality, but in yea means to understand completely, thoroughly, and that's

65
00:11:29,960 --> 00:11:42,480
the practice we undertake, in an unbiased way to see what's really true about reality,

66
00:11:42,480 --> 00:11:46,960
about seeing, and hearing, and smelling, and tasting, and feeling, and thinking about

67
00:11:46,960 --> 00:11:53,960
the body and the mind, what is the true nature of the body and the mind, what is the

68
00:11:53,960 --> 00:12:07,920
true nature of our experience, of who we are, and we begin by seeing body and mind and

69
00:12:07,920 --> 00:12:14,920
we start to see cause and effect, begin to see how the body and the mind work together

70
00:12:14,920 --> 00:12:25,520
to create all sorts of clinging, kamatana, bhavatana, we bhavatana, kamatana, wanting

71
00:12:25,520 --> 00:12:33,840
for central pleasures, craving for beautiful sights, beautiful sounds, pleasant sense and

72
00:12:33,840 --> 00:12:43,040
tastes and feelings, and thoughts, or maybe not thoughts, the first five anyway, those

73
00:12:43,040 --> 00:12:53,200
are under kamas, sensuality, are addiction to the physical world, are addictions in the physical

74
00:12:53,200 --> 00:13:01,360
world, addictions to sights and sounds and thoughts, bhavatana, are addiction to becoming

75
00:13:01,360 --> 00:13:05,360
one thing to be rich, one thing to be famous, one thing to be born as an angel or a

76
00:13:05,360 --> 00:13:17,120
god or even as a human, one thing to be born in heaven, one thing to, one thing to get

77
00:13:17,120 --> 00:13:25,600
a car, a house, one thing to have a family, one thing for something, anything could

78
00:13:25,600 --> 00:13:38,960
be anything to arise, to be, we bhavatana, one thing for something not to be, not to be fat,

79
00:13:38,960 --> 00:13:49,920
not to be thin, not to be tall, not to be short, not to be white or black, do not be stuck

80
00:13:49,920 --> 00:14:04,880
in a terrible job or to be stuck in a relationship, not to be this, not to be that, we bhavatana,

81
00:14:04,880 --> 00:14:16,800
one thing something not to be, all of those three are what leads to suffering, they would

82
00:14:16,800 --> 00:14:23,320
make, would cause problem, like they arise from not understanding the first noble truth,

83
00:14:23,320 --> 00:14:31,920
from understanding the truth of reality, because as you look at your experience, this is

84
00:14:31,920 --> 00:14:36,600
what you're seeing throughout the meditation course and throughout the meditation practice,

85
00:14:36,600 --> 00:14:46,480
you're seeing that it's just not worth clinging to anything, you're struggling and

86
00:14:46,480 --> 00:14:53,920
torturing yourself, trying to find something stable and satisfying, controllable, and

87
00:14:53,920 --> 00:14:59,960
all you're seeing is everything is impermanent and unsatisfying, uncontrollable, you're

88
00:14:59,960 --> 00:15:09,560
seeing that the nature of things don't, don't lend themselves to happiness through clinging,

89
00:15:09,560 --> 00:15:20,800
it's more like eating poison, every time you cling or it's clinging to a hot fire,

90
00:15:20,800 --> 00:15:44,640
you only burn up,

91
00:15:44,640 --> 00:15:54,640
I don't know how to block all those messages, sorry.

92
00:16:14,640 --> 00:16:25,200
And so this is our truth, this is the truth of Buddhism that craving leads to suffering

93
00:16:25,200 --> 00:16:31,880
and the converse is true that as you start to understand the nature of reality and understand

94
00:16:31,880 --> 00:16:40,400
that there's no satisfaction that comes from clinging, there's only stress and you start

95
00:16:40,400 --> 00:17:01,360
to relax, you start to lose your cravings, you start to give up, you give up your desires

96
00:17:01,360 --> 00:17:14,440
for sensuality or desires for becoming, your desires for unbecoming, and with the cessation

97
00:17:14,440 --> 00:17:32,040
of craving there is the cessation of suffering, with the cessation of craving there is

98
00:17:32,040 --> 00:17:39,280
the cessation of suffering, if you ever want to understand Nibana, this is the understanding

99
00:17:39,280 --> 00:17:44,080
of Nibana, it's not something scary or alien.

100
00:17:44,080 --> 00:17:50,200
All it means is that when you see something is causing you suffering, you can't help

101
00:17:50,200 --> 00:18:07,880
but let it go, letting go is happiness, letting go is peace, so there's no sense of making

102
00:18:07,880 --> 00:18:15,240
a mistake in falling into enlightenment, it's not an artificial state, it's not something

103
00:18:15,240 --> 00:18:18,480
you create, it comes about by pure wisdom and insight.

104
00:18:18,480 --> 00:18:24,560
If you don't understand these four normal truths, even just the first normal truth, you

105
00:18:24,560 --> 00:18:29,560
don't understand it perfectly, then don't worry about it, you're not going to ever let

106
00:18:29,560 --> 00:18:37,200
go until you truly understand the nature of reality.

107
00:18:37,200 --> 00:18:44,400
As it is, not as you want it to be or as you convince yourself that it is, but as it actually

108
00:18:44,400 --> 00:18:50,960
is through watching it repeatedly, through using the four sati bhatana to see the body,

109
00:18:50,960 --> 00:19:02,480
to see the feelings, as feelings to see the mind as mind, to see the dhammas as dhammas.

110
00:19:02,480 --> 00:19:15,120
Only this leads to, leads to freedom, and so that's the fourth normal truth, the fourth

111
00:19:15,120 --> 00:19:22,040
truth, if you read about the four normal truth, the fourth normal truth is the path, the

112
00:19:22,040 --> 00:19:27,560
path we have the eight full normal path and that describes it as its characteristics,

113
00:19:27,560 --> 00:19:34,080
but in terms of practice, the Buddha related it to a space on the four sati bhatana.

114
00:19:34,080 --> 00:19:42,920
So right mindfulness is being the key, and you develop right view, right thought, and

115
00:19:42,920 --> 00:19:51,040
that's wisdom, you develop right speech, right action, right livelihood, that's morality,

116
00:19:51,040 --> 00:19:57,000
and right effort, right mindfulness, right concentration.

117
00:19:57,000 --> 00:20:06,880
That's concentration or focus.

118
00:20:06,880 --> 00:20:11,640
These are the noble truths, this isn't all I wanted to say tonight, I wanted to talk

119
00:20:11,640 --> 00:20:16,880
about truth, but these fit in, these are the core of Buddhism, and this is the core of

120
00:20:16,880 --> 00:20:26,560
the Buddhist practice leading to enlightenment, they're the truths that we focus on.

121
00:20:26,560 --> 00:20:31,720
And so they fit into this larger conversation of what is truth, and so just to reiterate,

122
00:20:31,720 --> 00:20:41,000
let's be clear that being human is not truth, your name is not truth, these are truths

123
00:20:41,000 --> 00:20:49,560
but they're conventional truths, they don't actually mean anything in ultimate reality.

124
00:20:49,560 --> 00:21:06,280
Sultan isn't truth, second life isn't truth, this computer certainly isn't truth.

125
00:21:06,280 --> 00:21:12,200
And so all we are, we're not humans, there's this saying that's fairly interesting, not

126
00:21:12,200 --> 00:21:17,680
quite Buddhist, but interesting, we aren't human beings on a spiritual journey, we are

127
00:21:17,680 --> 00:21:28,480
spiritual beings on a human journey, it's along the right track, but we wouldn't be so concerned

128
00:21:28,480 --> 00:21:34,920
about the whole spiritual beings idea, because that's not really real either, but when

129
00:21:34,920 --> 00:21:41,640
you drill right down to what's truly real, and this is how you have to begin to, this

130
00:21:41,640 --> 00:21:46,560
is how you have to begin in order to understand concepts like rebirth, concepts like heaven

131
00:21:46,560 --> 00:21:54,880
and hell, to understand how reality works, you have to start by removing the idea that

132
00:21:54,880 --> 00:22:06,040
you're human, this pernicious sense of identity and identification as being a human, and

133
00:22:06,040 --> 00:22:16,120
I have 10 fingers and 10 toes right, since we were born, from the day we were born,

134
00:22:16,120 --> 00:22:22,440
we've begun to, or even before we're born in the womb, we have cultivated a strong sense

135
00:22:22,440 --> 00:22:32,640
of identity, I have 10 fingers and 10 toes, arms and legs, we reinforce this throughout

136
00:22:32,640 --> 00:22:41,840
our lives, but none of this is truth, none of this is who we really are, and so we have

137
00:22:41,840 --> 00:22:46,440
to deconstruct that and get to its real and that seeing, hearing, smelling, tasting, feeling

138
00:22:46,440 --> 00:22:51,680
and thinking, and that doesn't change whether you're born in hell or heaven or whatever,

139
00:22:51,680 --> 00:22:58,920
the nature of your reality becomes through whatever cause and effect that you create,

140
00:22:58,920 --> 00:23:11,520
you know, all that's real is just a six senses, body and mind, physical and mental, that's

141
00:23:11,520 --> 00:23:21,040
the truth, so there you go, there's some demo for tonight, just ended by saying it

142
00:23:21,040 --> 00:23:29,120
behooves us all to try and understand this, not just intellectually, but to quite simply

143
00:23:29,120 --> 00:23:35,640
study reality, study our own experience, we've got all the tools we need here inside,

144
00:23:35,640 --> 00:23:45,640
focus on that and cultivate understanding inside, okay, thank you, that's the demo for tonight.

145
00:24:05,640 --> 00:24:21,240
If anybody has questions, I'm happy to feel the questions.

146
00:24:21,240 --> 00:24:47,360
Thank you.

147
00:24:47,360 --> 00:24:59,480
All right. Go ahead. I've heard of the untying of knots and purification of the depths

148
00:24:59,480 --> 00:25:05,480
of the mind thus can suppress or over or cover negativity be expected to be upsurged in

149
00:25:05,480 --> 00:25:15,960
order to be cleansed. Yeah. Well, that's not sort of this idea of suppression is suppression

150
00:25:15,960 --> 00:25:20,480
isn't real. You're not actually suppressing something deep down in your brain. This isn't

151
00:25:20,480 --> 00:25:32,840
a physical thing. What you're doing is cultivating habits of reactivity that avoid the problem.

152
00:25:32,840 --> 00:25:39,520
So it's not about dragging things up. It's just about learning about cause and effect and

153
00:25:39,520 --> 00:26:06,440
changing your habits of reactivity. There's no cleansing in that sense. So the knots that

154
00:26:06,440 --> 00:26:14,680
are being referred to are just naughty behavior, not the habits, meaning not the, I guess,

155
00:26:14,680 --> 00:26:21,600
in the other sense of the other spelling of the word bad habits. Those are knots. And so

156
00:26:21,600 --> 00:26:26,760
you retrain your habits, your ways of reacting, ways of trying to avoid things, which

157
00:26:26,760 --> 00:26:37,160
feels like suppression, but it's actually just avoidance or reaction, redirection. There's

158
00:26:37,160 --> 00:26:41,640
meditation on solely the rising and falling of the abdomen, a viable practice to engage

159
00:26:41,640 --> 00:26:47,040
in for insight. Well, yeah, I mean, that's what we practice. I don't think you'd ask

160
00:26:47,040 --> 00:26:54,240
that if you, if you were one of us, so I'm going to suggest that you may be reed my booklet

161
00:26:54,240 --> 00:27:05,120
on how to meditate. Although, I guess, no, if you're just asking that not focusing on

162
00:27:05,120 --> 00:27:12,680
anything else, maybe this is actually asking just the abdomen. That's what it is asking.

163
00:27:12,680 --> 00:27:19,520
Actually, maybe I misread it. So yes, if you just use the abdomen, there's a problem there

164
00:27:19,520 --> 00:27:25,160
because you're ignoring large tracts of your experience. And so that doesn't really work.

165
00:27:25,160 --> 00:27:31,120
The abdomen's just an example. It's just a sort of a basic, extra training exercise. And

166
00:27:31,120 --> 00:27:35,320
a base to always fall back on, but reality is much broader than that. And if you don't

167
00:27:35,320 --> 00:27:41,200
incorporate all four city patana, you could work, but it's more than likely going to lead

168
00:27:41,200 --> 00:27:51,320
you into trouble as you ignore stuff and allow bad habits to fester. I want to ensure that

169
00:27:51,320 --> 00:27:56,200
the entirety of my mind is purified. Well, then you should say wanting, wanting, and

170
00:27:56,200 --> 00:28:05,120
stop having such thoughts. Don't worry like that. Just worry about what's real here

171
00:28:05,120 --> 00:28:11,480
and now and try and learn about your experiences as they happen. Don't worry about purifying

172
00:28:11,480 --> 00:28:16,880
all of the mind. The mind isn't something like that that exists as an entity. All you have

173
00:28:16,880 --> 00:28:21,440
is a reality as it arises and ceases in the present moment. And that's where you should

174
00:28:21,440 --> 00:28:31,080
focus your attention and your effort. Is that is where habits are formed and are reaffirmed

175
00:28:31,080 --> 00:28:40,680
or rejected and taken apart piece by piece. Meditating on the rising and falling so the

176
00:28:40,680 --> 00:28:47,040
press things are where the roots of suffering be eradicated. We focus on the four city

177
00:28:47,040 --> 00:28:53,240
patana, the roots of suffering will be eradicated. I'd encourage you to take up the practice

178
00:28:53,240 --> 00:29:02,640
of the four city patana, not just the rising and falling of the abdomen. So those are

179
00:29:02,640 --> 00:29:13,000
two questions that are on our site. If anyone has any live questions, please don't be shy.

180
00:29:13,000 --> 00:29:17,200
Lust and greed are not part of ultimate reality because they can broken down. No lust and

181
00:29:17,200 --> 00:29:28,880
greed are both part of ultimate reality. You can also note lasting, lasting or greed. I mean,

182
00:29:28,880 --> 00:29:40,560
they're just synonyms, honestly. I mean, you could argue that lust refers to complex. It's

183
00:29:40,560 --> 00:29:45,280
a little more complex. So there may be multiple realities associated or you might just say

184
00:29:45,280 --> 00:29:52,080
well lust refers to really intense, one thing or craving. But I mean, there's other ones

185
00:29:52,080 --> 00:30:01,440
like fear or depression which are a little more complicated. So sometimes you wouldn't want

186
00:30:01,440 --> 00:30:05,800
to say depressed. You'd want to say disliking. You wouldn't want to say fear also. You'd

187
00:30:05,800 --> 00:30:12,280
want to say disliking or worrying. Break it up into more. But it's not really important

188
00:30:12,280 --> 00:30:18,360
to go that deep afraid afraid. It's fine. It's good. Depressed. Depressed is fine. It's

189
00:30:18,360 --> 00:30:26,800
just they're just words. But some emotions are more complex than others. We give names

190
00:30:26,800 --> 00:30:36,360
to complex habits when in fact they can be broken down to simpler moments of reality.

191
00:30:36,360 --> 00:30:43,480
I don't think lust is one of them. Lust is just a specific type of greed. In the sense

192
00:30:43,480 --> 00:30:49,880
of greeting after, desiring after something. Because you don't lust after becoming famous.

193
00:30:49,880 --> 00:31:09,400
For example, you lust after sensuality generally. So just a strong, sensual desire generally.

194
00:31:09,400 --> 00:31:17,920
Welcome. Okay, well, if there are no questions, I'd like to thank everyone for coming

195
00:31:17,920 --> 00:31:26,760
out and God to see people keeping up following and listening to the audio or other people

196
00:31:26,760 --> 00:31:33,000
I think listening to the audio stream. If it's up, I hope it's up. I think it's up.

197
00:31:33,000 --> 00:31:37,240
And also those people on YouTube tuning in. The great thing about Second Life, I'm not

198
00:31:37,240 --> 00:31:41,960
sure how long we're going to be doing this, but while we're doing it, here is that people

199
00:31:41,960 --> 00:31:47,320
can just hop on from anywhere around the world. Problem, of course, is there's limited

200
00:31:47,320 --> 00:31:53,360
number of people can come and it's limited by your willingness and your ability to find

201
00:31:53,360 --> 00:31:59,720
your way into Second Life down the rabbit hole. So to speak. Maybe one day we'll go back

202
00:31:59,720 --> 00:32:06,200
to YouTube if I can ever get that worked out again. And on the meantime, thank you

203
00:32:06,200 --> 00:32:12,720
for tuning in. Wish you all good practice and peace, happiness, freedom from suffering.

204
00:32:12,720 --> 00:32:42,560
Good night.

