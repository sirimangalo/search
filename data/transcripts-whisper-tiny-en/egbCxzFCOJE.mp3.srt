1
00:00:00,000 --> 00:00:04,580
Hello and welcome back to our study of the Dhamupada.

2
00:00:04,580 --> 00:00:11,760
Today we continue on with verse 115, which reads as follows.

3
00:00:11,760 --> 00:00:32,720
This is the last in our series of verses about how it is better to live one day

4
00:00:32,720 --> 00:00:37,280
and see the truth, then to live a hundred years and not see the truth.

5
00:00:37,280 --> 00:00:44,480
Here the word is Dhammamutamal, so better it is it to live one day seeing the Dhammamut

6
00:00:44,480 --> 00:00:53,160
among the highest Dhamma, or the pinnacle of Dhammas, then to the hundred years and never

7
00:00:53,160 --> 00:01:03,200
see, never get to the top, never find the ultimate truth that you could try the Dhamma

8
00:01:03,200 --> 00:01:08,280
Dhammutamal is an ultimate truth.

9
00:01:08,280 --> 00:01:14,760
So this story is about, this verse is told in regards to the story about Bahu putikah,

10
00:01:14,760 --> 00:01:29,000
Bahu putikah, a woman who had many Bahu children putah, so she had seven sons and seven

11
00:01:29,000 --> 00:01:38,360
daughters and she was well off, she was she had she was wealthy and her children all got

12
00:01:38,360 --> 00:01:46,520
married, her sons found wives, her daughters found husbands and they lived happily and

13
00:01:46,520 --> 00:01:56,600
at peace with each other and then it happened that her husband passed away and she

14
00:01:56,600 --> 00:02:04,240
became the owner, the the possessor of all of his property, so she was wealthy at that

15
00:02:04,240 --> 00:02:09,520
point and she started to get older and her children grew up and I guess had children

16
00:02:09,520 --> 00:02:21,360
of their own and they started to tell her that she should hand over control of her wealth

17
00:02:21,360 --> 00:02:29,320
to them, let them look after her wealth, so divvy it up, it divided up between the 14 children

18
00:02:29,320 --> 00:02:33,400
and let them take care of her, they would be able because she didn't she wouldn't have

19
00:02:33,400 --> 00:02:40,080
to manage her own finances and so on, look after things, so give everything up to her

20
00:02:40,080 --> 00:02:47,600
children, let them look after because they would of course look after her and she thought

21
00:02:47,600 --> 00:02:53,360
herself well of course would need have I of wealth, my children will look after me, so

22
00:02:53,360 --> 00:02:58,200
she split it up and there's a word, if you're reading along with the English I have to

23
00:02:58,200 --> 00:03:05,400
explain because it's translated wrongly, says she split it into half which was confusing

24
00:03:05,400 --> 00:03:14,200
to me since there's 14 of them, but it says maje binditwa, maje means in the middle, so

25
00:03:14,200 --> 00:03:22,400
it's either in midst of them, she split it up among them or she split it evenly, maje

26
00:03:22,400 --> 00:03:32,480
means evenly, so she split it into 14 parts is quite clear, seven sons and seven

27
00:03:32,480 --> 00:03:42,680
daughters, but the nature of wealth is an interesting thing and from the time at first

28
00:03:42,680 --> 00:03:52,040
she was well taken care of by all of her sons and daughters and their families, but then

29
00:03:52,040 --> 00:04:01,920
eventually, but not eventually after some short time, she would go to one of her sons

30
00:04:01,920 --> 00:04:09,240
houses and they would support her for a while and then they would start to complain,

31
00:04:09,240 --> 00:04:16,080
so what did she give us more of her wealth, that we have to take care of her more than

32
00:04:16,080 --> 00:04:20,840
everyone else, and she would hear this sort of thing and feeling embarrassed, she would

33
00:04:20,840 --> 00:04:29,200
go to another of her children's houses, but it became a point where they began to fight,

34
00:04:29,200 --> 00:04:37,280
they began to bicker, they began to complain, anytime she relied upon any of them for support,

35
00:04:37,280 --> 00:04:41,600
they would resent it, thinking, why isn't she going to someone else's house, why are

36
00:04:41,600 --> 00:04:45,840
we the ones who have to take care of her, she only gave us a portion?

37
00:04:45,840 --> 00:04:50,800
This is when you, what happens when you bring money into the picture, it's why I would

38
00:04:50,800 --> 00:04:57,600
never encourage, obviously I would never do myself, but also never encourage people to

39
00:04:57,600 --> 00:05:07,240
mix the dhamma with money, like charging for it or even if your intention is pure,

40
00:05:07,240 --> 00:05:12,200
trying to make a living off of something, it's just mixing two things, you know, family

41
00:05:12,200 --> 00:05:19,960
and money, when a person dies and then the inheritance is to be split up, it can create

42
00:05:19,960 --> 00:05:27,360
bitter and terrible enemies out of siblings, you know, this happens often, I hear about

43
00:05:27,360 --> 00:05:37,960
how horrible it becomes, when people feel unfairly treated or they're fighting and they

44
00:05:37,960 --> 00:05:44,680
sue each other over there, inheritance or ridiculous things like this, money has the potential

45
00:05:44,680 --> 00:05:50,960
to destroy a lot of things, not by itself, by itself it's just paper and numbers and metal

46
00:05:50,960 --> 00:05:57,520
and that kind of thing, wealth is just staff, but the significance and the whole that has

47
00:05:57,520 --> 00:06:04,080
the concept of wealth, the concept of power, the concept of affluence and all the comfort

48
00:06:04,080 --> 00:06:11,640
that it promises, you know, is intoxicating and it destroys people, so these people literally

49
00:06:11,640 --> 00:06:19,520
are absolutely drove their mother to poverty because she suddenly had no support from

50
00:06:19,520 --> 00:06:24,640
many of them, rather than being, better than thinking of her as their mother, they just

51
00:06:24,640 --> 00:06:30,560
thought of her as the person who gave us one-fourteenth of her wealth and suddenly this

52
00:06:30,560 --> 00:06:36,560
became more important than the fact that she was their mother, which is absurd, but that

53
00:06:36,560 --> 00:06:42,440
sort of thing does happen in the world, crazy as it sounds.

54
00:06:42,440 --> 00:06:48,440
And so she got fed up, she looked at this and she lost all desire to be involved in

55
00:06:48,440 --> 00:06:57,760
this terrible mess of her family and so she became a nun, and that's her story, it's

56
00:06:57,760 --> 00:07:05,920
actually not so related to the verse, but this is the story of a very wonderful story of

57
00:07:05,920 --> 00:07:11,640
Behoputika, I'm in a terrible story, but wonderful in the sense of how she overcame it,

58
00:07:11,640 --> 00:07:22,080
how she persevered, and kind of remarkable in regards to how terrible people can be,

59
00:07:22,080 --> 00:07:27,080
to such a nice person, a person who ostensibly was a very good and kind and caring mother

60
00:07:27,080 --> 00:07:33,440
who was naive enough to think that her children were as nice as she and would take care

61
00:07:33,440 --> 00:07:43,760
of her. So she became a nun and went to practice, and she said to herself, I'm become

62
00:07:43,760 --> 00:07:52,120
a nun in old age, and so I should devote myself to heedfulness, and so she decided that

63
00:07:52,120 --> 00:07:57,680
she would spend the whole night in meditation, not sleeping, and so she would do walking

64
00:07:57,680 --> 00:08:05,560
meditation, and she was doing walking meditation all night by holding on to a tree branch

65
00:08:05,560 --> 00:08:18,000
or a fallen tree and doing walking back and forth. And she made a determination to herself,

66
00:08:18,000 --> 00:08:25,280
I will fulfill the dhamma of the Buddha, the dhamma of the teacher, I will fulfill the

67
00:08:25,280 --> 00:08:38,240
teacher's teaching. I will do the dhammas of the psalmana of the psalmana of the psalmana

68
00:08:38,240 --> 00:08:45,640
shamma, or a recluse, and she made a determination to set that adhesi, the dhamma, the dhamma,

69
00:08:45,640 --> 00:08:53,080
what kari sami, I will do the dhamma that is taught by the teacher. I will perform the

70
00:08:53,080 --> 00:08:59,480
dhammas, so she was meticulous in this, and the Buddha found out about this very simple

71
00:08:59,480 --> 00:09:04,360
story, that the Buddha found out about this and taught her this verse and said, indeed

72
00:09:04,360 --> 00:09:13,320
it's better to see the dhamma for one moment than to live a hundred years and never see it.

73
00:09:13,320 --> 00:09:19,080
And as he taught her, she focused her mind and she was delighted with his teaching, and

74
00:09:19,080 --> 00:09:26,520
so it gave her encouragement, and based on that encouragement, she was able to become an

75
00:09:26,520 --> 00:09:37,720
hour-a-hump, and become late, and see the dhamma of the highest dhamma. Now this one is really,

76
00:09:37,720 --> 00:09:42,520
probably the most powerful of these ones that we've been looking at now, maybe not most

77
00:09:42,520 --> 00:09:49,160
instructive, because it deals with nibana, which is not all that useful for a meditator to consider,

78
00:09:50,040 --> 00:09:55,000
being ineffable, not something you can talk about or think about, and in fact I think it

79
00:09:55,000 --> 00:10:01,320
scares a lot of new meditators. I think what will happen to me if I attain that, so it's more

80
00:10:01,320 --> 00:10:07,160
like the result of letting go. That's not something to be considered too much, but the point, the

81
00:10:07,160 --> 00:10:20,280
powerful power of this verse is in the fact that this is the clearest truth, the clearest

82
00:10:20,280 --> 00:10:29,480
version of this verse that you could live countless lifetimes, and it's all meaningless,

83
00:10:29,480 --> 00:10:35,400
it's all never reaches this highest dhamma, that it's possible to live countless lifetimes

84
00:10:35,400 --> 00:10:40,840
without ever reaching the pinnacle, and that's how we live our lives, and it's an important

85
00:10:40,840 --> 00:10:46,440
point to make, and Buddhism is really geared towards attaining something that we've never attained

86
00:10:46,440 --> 00:10:53,080
in all our lives, in all the realms of samsara, something that we've missed. We are like the

87
00:10:53,080 --> 00:10:59,000
plains dwellers. We see the mountain up high, or maybe see the foot of the mountain, but never

88
00:10:59,000 --> 00:11:16,840
venture to its peak, and the key here is that it's better categorically preferable because

89
00:11:18,040 --> 00:11:24,280
without seeing the highest dhamma, you do just run around on the plains, and you're born again

90
00:11:24,280 --> 00:11:31,800
and again and again, and it's defining moment when you attain nipana, because at that moment

91
00:11:31,800 --> 00:11:43,240
you've changed the course of your existence, and you've shifted categorically to the realm

92
00:11:43,240 --> 00:11:51,720
of clarity and vision and understanding, whereas before, or most people we just run around based

93
00:11:51,720 --> 00:12:11,320
on instincts and habits and partialities, and culture, and what we're used to

94
00:12:13,320 --> 00:12:18,360
without ever really waking up and examining or looking at what we're doing to ourselves,

95
00:12:18,360 --> 00:12:23,640
we just follow and chase after things that are pleasurable, run away from things that are

96
00:12:24,840 --> 00:12:32,120
unpleasant, but seeing nipana change is that it wakes you up, and you see clearly and do

97
00:12:35,240 --> 00:12:41,880
you're able to know when you're doing to yourself, you're able to see the desire and see the

98
00:12:41,880 --> 00:12:49,880
aversion, and as a result you're able to clean it out once and for all, so that's the highest

99
00:12:49,880 --> 00:13:02,120
dhamma to change, to change, because no matter how much we change, without seeing the highest

100
00:13:02,120 --> 00:13:07,800
dhamma, it's always the same. You become a better person sometimes, and then you become a worse person,

101
00:13:07,800 --> 00:13:15,320
back and forth, and we just go around and around. There's no, it's kind of like being in the ocean,

102
00:13:16,440 --> 00:13:24,120
in the ocean there are no landmarks, there are no signs, there is nothing that stands out,

103
00:13:24,120 --> 00:13:28,520
nothing that is exceptional, it's all just water, no matter where you go, you can say, I'm here,

104
00:13:28,520 --> 00:13:34,040
I'm here, I'm here, and then it's all just ocean, but if you compare to dry land, that's kind of

105
00:13:34,040 --> 00:13:40,600
goically different, when our lives as human beings, as beings, it's just like being in a great ocean,

106
00:13:41,400 --> 00:13:45,400
it's all just water, no matter what you become, you become a king, while then you die,

107
00:13:46,600 --> 00:13:52,280
become worm food, you can become a great motivational speaker, you can become the king,

108
00:13:52,280 --> 00:13:57,240
the politician, you can become a religious leader and teach people good things,

109
00:13:58,760 --> 00:14:03,080
and then in the end it just wiped out, you can teach people to become good people, and then

110
00:14:03,080 --> 00:14:08,120
they're good people for a while, and then they die, and they forget about it, and they might

111
00:14:08,120 --> 00:14:17,400
become bad people again. So we have this categorical change, we have something that is beyond that,

112
00:14:17,960 --> 00:14:22,840
and that is the dumplings among. So absolutely, this is the one that makes the most impression,

113
00:14:23,560 --> 00:14:29,400
I think. If you see it just for one moment, all it takes is the senior banner for one moment,

114
00:14:29,400 --> 00:14:36,840
and then it changes completely how you look at the universe, you wake up, you feel like you were

115
00:14:36,840 --> 00:14:42,600
asleep before, dead and now you're alive, and so the sort of things that people say once they've

116
00:14:42,600 --> 00:14:51,000
seen the banner. So that's all, that's the teaching for today, something that's useful for

117
00:14:51,000 --> 00:15:00,120
our meditation, and that it emphasizes and it reminds us of the emphasis on the banner,

118
00:15:01,320 --> 00:15:09,400
and it reminds us of how important it is, and how great it is. So it's not something to be feared

119
00:15:09,400 --> 00:15:17,400
certainly, that's something to be realized, it's the highest damba, that we have only two alternatives

120
00:15:17,400 --> 00:15:23,080
to run around chasing after mundane damba, mundane truths, which are mostly relative,

121
00:15:25,400 --> 00:15:31,640
or we can strive for the ultimate truth, the truth that doesn't change, the truth that is

122
00:15:31,640 --> 00:15:46,360
unstable, that is unchanging, that is stable, and satisfying, not controllable, but not south,

123
00:15:46,360 --> 00:15:56,680
but peace. So that's the damba pad of her today, thank you all for tuning in, wishing you all the

124
00:15:56,680 --> 00:16:06,680
best.

