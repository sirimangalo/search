1
00:00:00,000 --> 00:00:06,880
The hindrances, introduction. The first set of damas discussed in the Satipatana-suta are the hindrances.

2
00:00:07,520 --> 00:00:13,680
Before all other damas, for someone who is practicing Satipatana, which is mindfulness,

3
00:00:13,680 --> 00:00:19,120
the first meditation object outside of the body, feelings, and the mind, is the hindrances.

4
00:00:19,840 --> 00:00:24,240
They are taught first because they are strongest and most persistent in new meditators.

5
00:00:24,240 --> 00:00:30,240
Our ordinary relationship with our state of mind is problematic because it is backwards.

6
00:00:30,800 --> 00:00:35,600
When we like or want something, we take it as an indication that we need to get that something,

7
00:00:36,160 --> 00:00:38,560
that we should obtain the object of our desire.

8
00:00:40,080 --> 00:00:45,600
When we dislike or our angry at something, we take it as an indication that we should do whatever we

9
00:00:45,600 --> 00:00:51,840
can to get rid of the object of our aversion. When we are tired, we take it as an indication

10
00:00:51,840 --> 00:00:57,040
that we should sleep. When we are distracted or restless, we take it as a sign that we should get up

11
00:00:57,040 --> 00:01:04,080
and do something, anything. This is not intellectual, it is just how we react. Our whole being is

12
00:01:04,080 --> 00:01:09,520
conditioned to react in this way. When we are confused or when we have doubts about something,

13
00:01:09,520 --> 00:01:15,280
we take it as an indication that we should stop what we are doing and reject it. We live our lives,

14
00:01:15,280 --> 00:01:21,360
trying to fit the world around our expectations and personality. How often do we make decisions

15
00:01:21,360 --> 00:01:26,960
based on our likes and dislikes? When we think that it is natural to do so, that if you do not

16
00:01:26,960 --> 00:01:32,240
like something, why would you tolerate it? And if you like something, we think of course that is

17
00:01:32,240 --> 00:01:38,720
where you should focus your effort. Unfortunately, the results of this are not as positive as we

18
00:01:38,720 --> 00:01:44,240
might think. When you chase after what you want and what you like, you find yourself bound tighter

19
00:01:44,240 --> 00:01:49,120
and tighter to those things that you like and your happiness becomes more and more dependent on them.

20
00:01:49,120 --> 00:01:54,800
This is how the addiction cycle works. When we are angry or reversed to something, we become

21
00:01:54,800 --> 00:02:00,080
vulnerable to it. We become more and more averse, fragile or vulnerable to its arising.

22
00:02:00,720 --> 00:02:06,080
We also become more and more susceptible to anger, disappointment, and displeasure when it arises

23
00:02:06,080 --> 00:02:12,960
or when it cannot be removed. If we just sleep whenever we are tired, contrary to popular opinion,

24
00:02:12,960 --> 00:02:18,240
that does not actually give us more energy. There is a real problem with this for meditators who

25
00:02:18,240 --> 00:02:23,280
come to practice mindfulness. Because psychologically, they have this idea that if you are tired,

26
00:02:23,280 --> 00:02:28,800
you should sleep and that sleep will somehow make you less of a tired person. It is actually

27
00:02:28,800 --> 00:02:34,560
somewhat the opposite. The more quickly you give into sleep, the more habitually prone to

28
00:02:34,560 --> 00:02:39,920
drowsiness you become. Of course, your body does need sleep based on your daily physical and

29
00:02:39,920 --> 00:02:45,280
mental activities. But if you are very mindful, you need far less sleep and are far less susceptible

30
00:02:45,280 --> 00:02:51,200
to drowsiness than most people. If you are restless and you just get up and find something

31
00:02:51,200 --> 00:02:56,640
to occupy yourself with, you become prone to restlessness. You become more distracted by nature.

32
00:02:57,600 --> 00:03:02,160
As with the other hindrances, we react to our state of mind and try to fit the world around it

33
00:03:02,160 --> 00:03:07,760
to appease it, rather than learning to have our state of mind fit the world. We need to see how

34
00:03:07,760 --> 00:03:12,720
these hindrances are actually the biggest reason for us not being able to fit in with the world

35
00:03:12,720 --> 00:03:18,960
properly or live without stress and suffering. If we are always doubting, then we just become

36
00:03:18,960 --> 00:03:23,760
someone who doubts everything. We can never accomplish anything of any great significance

37
00:03:23,760 --> 00:03:28,560
because doubt arises habitually and we find ourselves doubting even things that present

38
00:03:28,560 --> 00:03:34,720
themselves right in front of us and are clearly observable. This happens for meditators. They will

39
00:03:34,720 --> 00:03:39,760
achieve positive results in their practice, but later doubt what they achieved. One day, they will

40
00:03:39,760 --> 00:03:44,800
have great results and be sure the practice is beneficial and the next day they forget those results

41
00:03:44,800 --> 00:03:51,680
and doubt whether the practice has any use whatsoever. The states of mind discussed above are hindrances.

42
00:03:51,680 --> 00:03:56,880
There are positive good mind states, but there are also bad harmful mind states.

43
00:03:56,880 --> 00:04:01,920
In this practice, we don't consider our normal states of mind as proper cause for action,

44
00:04:01,920 --> 00:04:07,760
speech or even thought. We instead focus on cultivating those states of mind that lead naturally

45
00:04:07,760 --> 00:04:14,000
to wholesome speech, deeds and thought. This is why we focus on our mind states as objects of

46
00:04:14,000 --> 00:04:19,440
meditation. Rather than allowing our mind states to guide us, we guide ourselves back to the states

47
00:04:19,440 --> 00:04:25,920
of mind themselves to study them and understand their true nature. Another common way people deal

48
00:04:25,920 --> 00:04:31,680
with troublesome mind states is to reject them, suppress them, or try to avoid them. One might think,

49
00:04:31,680 --> 00:04:37,920
desire is bad. Okay, I will just get really upset every time I want something. Anger is bad.

50
00:04:37,920 --> 00:04:42,560
Well, I will feel really bad about that and then I will get angry at the fact that I am angry.

51
00:04:43,120 --> 00:04:46,240
If I am tired, then I will try to force myself to stay awake,

52
00:04:46,240 --> 00:04:51,680
entire myself out by working really hard. If I am distracted, then I will force and exert myself

53
00:04:51,680 --> 00:04:56,800
with lots of effort. I will apply the effort to suppress the effort, as if you can somehow

54
00:04:56,800 --> 00:05:03,280
stop yourself from being distracted. One might think, if I have doubt, then I will just force myself

55
00:05:03,280 --> 00:05:08,800
to believe, which of course is the best way to cultivate long term doubt. It does not get rid of

56
00:05:08,800 --> 00:05:13,840
the doubt at all. Instead, in the long term, it just makes you realize how you never really

57
00:05:13,840 --> 00:05:19,840
understood or had any good basis for confidence. It prevents you from gaining true confidence.

58
00:05:20,480 --> 00:05:25,680
What helps you to gain true confidence and overcome all problematic mind states is honest and

59
00:05:25,680 --> 00:05:28,960
objective observation of the states of mind themselves.

60
00:05:30,720 --> 00:05:35,360
The Buddha said the same thing about the hindrances as he did about all other aspects of our

61
00:05:35,360 --> 00:05:41,280
experience in the Satyapatana Sutta, that we should be mindful of them. We should see how they

62
00:05:41,280 --> 00:05:47,680
arise and cease. We should watch them, observe them, and study them. When the mind is full of anger,

63
00:05:47,680 --> 00:05:52,560
we should know that it is a mindful of anger. When the mind is full of lust or passion,

64
00:05:52,560 --> 00:05:58,400
we should know that it is a mindful of passion. If you see something beautiful or attractive

65
00:05:58,400 --> 00:06:04,960
that will give rise to liking, you note seeing and the liking will not arise. If you note this

66
00:06:04,960 --> 00:06:11,280
process, you are able to understand cause and effect. You are able to see what leads to what.

67
00:06:11,280 --> 00:06:16,800
You can see how desire clouds and colors the mind. You can see how anger inflames the mind

68
00:06:16,800 --> 00:06:25,040
and drowsiness stifles the mind. The five hindrances in brief are liking, disliking, drowsiness,

69
00:06:25,040 --> 00:06:31,520
distraction, and doubt. These five states of mind are the most important aspects of the dharma to

70
00:06:31,520 --> 00:06:36,640
focus on as a beginner meditator because in the beginning you are easily overwhelmed by them.

71
00:06:37,440 --> 00:06:42,400
They will arise throughout your practice, but in the very beginning the hindrances will truly

72
00:06:42,400 --> 00:06:48,720
obstruct your practice. A new meditator has to be vigilant about the hindrances. If your practice

73
00:06:48,720 --> 00:06:54,800
is not going well, there are only five reasons, liking or wanting, disliking, which includes

74
00:06:54,800 --> 00:07:01,440
boredom, fear, depression, and sadness, drowsiness, or tiredness, distraction, or worry, or the

75
00:07:01,440 --> 00:07:08,000
inability to focus the mind on the task at hand and doubt or confusion. The hindrances are the

76
00:07:08,000 --> 00:07:14,080
only reasons why you really fail and why meditation practice can ever be difficult. If the hindrances

77
00:07:14,080 --> 00:07:19,200
do not arise, then you will be able to be mindful of anything no matter what happens.

78
00:07:20,000 --> 00:07:26,000
Without the hindrances, the mind is flexible and when experiences change, one can be fully focused

79
00:07:26,000 --> 00:07:32,080
on the new experiences. When the hindrances do not arise, it is very hard to accept change.

80
00:07:32,080 --> 00:07:37,120
It is very hard to be open. It is very hard to be flexible in the face of impermanence,

81
00:07:37,120 --> 00:07:47,120
suffering, or non-self in the face of unmet expectations.

