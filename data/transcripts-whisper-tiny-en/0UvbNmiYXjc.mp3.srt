1
00:00:00,000 --> 00:00:07,000
Good evening and welcome back to our study of the Damupanda.

2
00:00:07,000 --> 00:00:15,000
Today we continue on with verse 151, which reads as follows.

3
00:00:15,000 --> 00:00:22,000
This is a very important study of the Damupanda.

4
00:00:22,000 --> 00:00:29,000
The Damupanda is a very important study of the Damupanda.

5
00:00:29,000 --> 00:00:36,000
The Damupanda is very important.

6
00:00:36,000 --> 00:00:56,440
Which means, indeed, just as the chariot of a king, beautiful chariot and well decorated

7
00:00:56,440 --> 00:01:09,600
ages, even the king's chariot gets old, falls apart.

8
00:01:09,600 --> 00:01:28,600
Ato, likewise, Sarirampi Charangupiti, this body too, even this body, goes to old age.

9
00:01:28,600 --> 00:01:36,600
Satancha Damona Charangupiti, but the good dhamma never gets old.

10
00:01:36,600 --> 00:02:00,520
Ato, Charirampi Charangupiti, the peaceful, the peaceful, indeed, that is called peaceful, by

11
00:02:00,520 --> 00:02:29,240
the wise, and indeed, it is called peaceful, by the wise.

12
00:02:29,240 --> 00:02:42,920
So this verse was taught in relation to one of these famous stories, it is a story I

13
00:02:42,920 --> 00:02:47,400
actually told recently on a meditation retreat in New York.

14
00:02:47,400 --> 00:02:54,760
I remember some in response to a question.

15
00:02:54,760 --> 00:03:07,840
The story goes, this is the story of Queen Malika, Malika was Queen in Sawati, the Queen

16
00:03:07,840 --> 00:03:11,680
of Pissanadi.

17
00:03:11,680 --> 00:03:19,520
Pissanadi was a supporter of the Buddha, though he wasn't enlightened, even at the lower

18
00:03:19,520 --> 00:03:34,480
levels, he was still an ordinary human being with all of his faults and wrinkles.

19
00:03:34,480 --> 00:03:37,400
And his Queen was the same, but they were supporters of the Buddha.

20
00:03:37,400 --> 00:03:47,960
They did many good things, and many good things towards the Buddha and towards his religion.

21
00:03:47,960 --> 00:03:55,060
Very awkward a bit to these two, if the tradition is anything to go by, as they supported

22
00:03:55,060 --> 00:04:02,440
Buddhism in those early stages.

23
00:04:02,440 --> 00:04:12,200
The story is, Queen Malika was in general a really good person, but again, she had her

24
00:04:12,200 --> 00:04:20,160
feelings, she was just an ordinary world thing, and one day she was in the bathhouse.

25
00:04:20,160 --> 00:04:29,960
And the King was up on his, up in his, in the palace looking down.

26
00:04:29,960 --> 00:04:38,360
And as she was bathing, she finished bathing, washing her torso in her face and whatever,

27
00:04:38,360 --> 00:04:45,360
and she bent down to wash her legs, and a dog came into the, there was a dog, her dog was

28
00:04:45,360 --> 00:04:46,760
sitting there.

29
00:04:46,760 --> 00:04:51,400
And this dog came up to her, and we don't have the details of exactly what happened,

30
00:04:51,400 --> 00:05:13,040
but it says that it started to sport with her, and she let it continue.

31
00:05:13,040 --> 00:05:31,520
As Adamasantavang got to be began to do, that which was unwholesome intercourse.

32
00:05:31,520 --> 00:05:39,760
Yes, as Adamas, I'm not quite sure what that means.

33
00:05:39,760 --> 00:05:47,720
Anyway, Santavas is to do with sexual intercourse, so it started to engage, and she let

34
00:05:47,720 --> 00:05:52,800
it, she let it continue.

35
00:05:52,800 --> 00:06:00,360
And the King saw her, and so when she got back to the palace, the King was ready to

36
00:06:00,360 --> 00:06:10,680
banish her.

37
00:06:10,680 --> 00:06:13,480
Nasa was silly.

38
00:06:13,480 --> 00:06:19,800
What's silly is, what's silly is, I think a very bad word.

39
00:06:19,800 --> 00:06:25,640
Nasa means, nasa is as close as you get to the swear word in Bali.

40
00:06:25,640 --> 00:06:32,200
Nasa means, it's imperative, it's like, go to hell kind of thing, nasa it means to

41
00:06:32,200 --> 00:06:43,400
pair it one parishes, to be destroyed, nasa is like a command, get lost, when you

42
00:06:43,400 --> 00:06:47,440
perish basically.

43
00:06:47,440 --> 00:06:54,720
And the Queen asked him, what's wrong, what did I do, and he said I saw you in the bathroom

44
00:06:54,720 --> 00:07:05,240
with that dog, how could you be so disgusting and nasa, go to go away.

45
00:07:05,240 --> 00:07:12,120
And the Queen says, what are you talking about, she's thinking quick on her face, she says,

46
00:07:12,120 --> 00:07:15,720
I did no such thing, and he says, I saw you, and he says, what do you mean, she says,

47
00:07:15,720 --> 00:07:21,040
what do you see, I mean, I was up on the on the balcony and I looked down into the bathhouse

48
00:07:21,040 --> 00:07:26,680
and I saw you sporting with this dog, and the Queen says, I did no such thing, she said

49
00:07:26,680 --> 00:07:35,920
to your majesty, you must know that there's a strange nature of that bathhouse, that anyone

50
00:07:35,920 --> 00:07:44,280
who goes in there appears to be double, and he said, what are you talking about, you

51
00:07:44,280 --> 00:07:49,640
utter false, you're telling a lie, because if you don't believe me, you go in and I'll

52
00:07:49,640 --> 00:07:56,960
watch you, and this is the thing about the same thing, the commentary says, he was such

53
00:07:56,960 --> 00:08:09,320
a simpleton as to believe what she said, and so he went into the bathhouse and she goes,

54
00:08:09,320 --> 00:08:16,960
your majesty, why are you sporting with that goat, and he says, I am not, he said, I can

55
00:08:16,960 --> 00:08:23,080
see you, look at you with that goat, and so he believes, somehow he believes that it was

56
00:08:23,080 --> 00:08:30,680
all just an optical vision, anyway, the point of the story, and why it's actually quite

57
00:08:30,680 --> 00:08:38,800
interesting to us, is that she felt quite guilty about this, she was such a good person,

58
00:08:38,800 --> 00:08:45,720
but she felt guilty, she's thinking to herself, how can I face, how can I go and face

59
00:08:45,720 --> 00:08:50,080
the Buddha, and he'll know right away, that I was lying, and he'll know what I did with

60
00:08:50,080 --> 00:09:04,760
the dog, and he'll know how evil and corrupt I am in, at heart, and I lied to the king,

61
00:09:04,760 --> 00:09:08,160
and if I go see the Buddha, he'll know right away, and so she felt very guilty, and she

62
00:09:08,160 --> 00:09:21,000
tried to avoid the Buddha, from that point on she was tormented with guilt, for having

63
00:09:21,000 --> 00:09:28,520
this one time, given into some indiscretion.

64
00:09:28,520 --> 00:09:32,080
As a result, when she died, she wasn't thinking about all the good needs that she had done,

65
00:09:32,080 --> 00:09:36,080
she wasn't thinking about all the wonderful things and all the support, but sanity was

66
00:09:36,080 --> 00:09:49,760
responsible for this great gift, there was the gift beyond compare where they, I think

67
00:09:49,760 --> 00:09:57,280
it was where they were fighting over, the townspeople, the city were competing with the

68
00:09:57,280 --> 00:10:03,600
king to give the greatest gift, there was something I get around, the vicinity was responsible,

69
00:10:03,600 --> 00:10:08,360
and Malik was right there with him doing all these good needs supporting the Buddha, and

70
00:10:08,360 --> 00:10:17,600
always going to listen to the Buddha's teaching and trying their best to understand it,

71
00:10:17,600 --> 00:10:24,960
that she forgot all that, and when she died as a result, she died not long after, she went

72
00:10:24,960 --> 00:10:39,240
to hell, she was reborn in hell, as a result not of the evil deed itself, but of the

73
00:10:39,240 --> 00:10:52,440
guilt, and the torment, and the negative mind states that she had gotten obsessed in,

74
00:10:52,440 --> 00:11:02,440
and the sanity was much aggrieved, overcome with grief, and after performing the funeral

75
00:11:02,440 --> 00:11:06,840
rights, he thought to himself, I'll go to the teacher, the Buddha, and ask him where

76
00:11:06,840 --> 00:11:13,520
she's been reborn, thinking to himself, well at least that will give me some comfort

77
00:11:13,520 --> 00:11:19,840
if I know where she is, then I can think well of her and be happy knowing that she's

78
00:11:19,840 --> 00:11:26,360
in a good place, so he goes to the Buddha, and the Buddha thought to himself, and if he

79
00:11:26,360 --> 00:11:35,560
asks me, he's not the sort of person to be an understanding about such a situation, he's

80
00:11:35,560 --> 00:11:44,520
more likely to just believe me, and perhaps even lose faith in Buddhism, she's done such

81
00:11:44,520 --> 00:11:51,640
good things, why is she then born in hell, why is the Buddha saying such things, how could

82
00:11:51,640 --> 00:11:52,640
it be possible?

83
00:11:52,640 --> 00:11:56,640
Does it mean that giving all these gifts and supporting Buddhism and listening to Buddhist

84
00:11:56,640 --> 00:12:06,120
teaching is, I'm no benefit, and so he made a determination, the Buddha made a determination

85
00:12:06,120 --> 00:12:14,160
in his mind, such that the king would not ask where his queen had been reborn, and

86
00:12:14,160 --> 00:12:20,240
sure enough, the king forgot, it was the power of determination, if you make a determination

87
00:12:20,240 --> 00:12:29,520
and your will is strong enough, there's some magic to it, and so he came and he sat down

88
00:12:29,520 --> 00:12:36,240
and he listened to the Buddha and the Buddha, gave him some teachings, he listened attentively

89
00:12:36,240 --> 00:12:41,680
and when he left, just as he kind of the monastery, he remembered, oh, I forgot, I was

90
00:12:41,680 --> 00:12:47,200
going to ask him about Queen Malika, I went back to the palace, he thought, will never

91
00:12:47,200 --> 00:12:51,400
mind, I'll go back tomorrow and see him again, and again, he went back to the Buddha,

92
00:12:51,400 --> 00:12:59,280
again, the Buddha makes him forgotten, it does this for seven days, and at the end of

93
00:12:59,280 --> 00:13:11,600
the seventh day, Queen Malika leaves hell and is reborn in heaven, to see that in fact,

94
00:13:11,600 --> 00:13:19,520
one of the good heaven, one of the really high heaven, and on the eighth morning, the

95
00:13:19,520 --> 00:13:24,080
Buddha, before the king, can come, the Buddha goes to the palace, goes for arms around standing

96
00:13:24,080 --> 00:13:31,440
outside of the king's residence, the king hears that the Buddha is there and he comes

97
00:13:31,440 --> 00:13:37,000
down and he takes the Buddha's ball and he feeds him with royal food and sits him down

98
00:13:37,000 --> 00:13:50,040
and he asks him, on the eighth day, reverence her, please tell me where Queen Malika

99
00:13:50,040 --> 00:13:57,480
is reborn and he says, oh, in the world of to see that, in the to see that heaven, and

100
00:13:57,480 --> 00:14:02,080
he says, if she had not been born in there, how could anyone be born there?

101
00:14:02,080 --> 00:14:09,080
She was such a good person, but of course she was born there, and so wherever she sat,

102
00:14:09,080 --> 00:14:18,400
wherever she stood, she was always thinking about good deeds, she cared not for else but

103
00:14:18,400 --> 00:14:29,440
to give gifts and be a good person, and then he started to feel sad and he said, ever

104
00:14:29,440 --> 00:14:35,400
since she went to the old, the other world, ever since she left this world, my own person

105
00:14:35,400 --> 00:14:42,080
has been non-existent, I had been nothing, nothing doubt her, and this is where the Buddha

106
00:14:42,080 --> 00:14:50,400
taught this verse and said, oh, great king, just like this, he asks him, he looks at

107
00:14:50,400 --> 00:14:54,560
the chariot and he says, who's chariot is that, and I guess there were three chariots or

108
00:14:54,560 --> 00:15:01,560
something, he's in the palace, so he sees this old chariot and he says, who's this that,

109
00:15:01,560 --> 00:15:06,920
that's my grandfather, and who's this that, oh, that's my father, and what about this

110
00:15:06,920 --> 00:15:17,000
new one here, oh, that's mine, and he said, like, just like those chariots, the old ones

111
00:15:17,000 --> 00:15:25,840
get old, stop using them, they break down, in the same way that body breaks down and it's

112
00:15:25,840 --> 00:15:34,240
subject to old age, sickness and death, and then he taught this verse, basically saying

113
00:15:34,240 --> 00:15:46,920
that we all die, what doesn't die is goodness, what doesn't die is truth, what doesn't

114
00:15:46,920 --> 00:15:58,040
die is the dumb, so there's two reasons why this story is useful to us, of course the

115
00:15:58,040 --> 00:16:02,040
reason for the verse, or the actual verse itself relates to death, we've been talking quite

116
00:16:02,040 --> 00:16:08,720
a bit about this, remember this is the believer in the Jhara Bhagat, the old age chapter,

117
00:16:08,720 --> 00:16:18,840
so there's a lot about how we get old, which is important, so we don't make these plans

118
00:16:18,840 --> 00:16:26,120
thinking that we're going to live forever and have short-sighted ambitions for power and

119
00:16:26,120 --> 00:16:33,640
money and pleasure that can't last, that can't satisfy, that only set us up for disappointment

120
00:16:33,640 --> 00:16:41,400
and stress when we get old and sick and actually leave it all behind, but the other interesting

121
00:16:41,400 --> 00:16:47,400
thing is in regards to the story, this is a useful story to think of guilt, and guilt

122
00:16:47,400 --> 00:16:54,520
is something we also have to let go of, it's most curious about this story, it's not really

123
00:16:54,520 --> 00:17:00,360
the verse at all, it's this idea of guilt and it's a common problem for meditators, especially

124
00:17:00,360 --> 00:17:06,400
in the West I think, we're good at feeling guilty and hating ourselves for things that

125
00:17:06,400 --> 00:17:14,240
we've done, we're feeling inadequate, it's not something you see such a problem with in Asia,

126
00:17:14,240 --> 00:17:21,120
but in regard to it's a problem that meditators, all meditators face, because we, to some

127
00:17:21,120 --> 00:17:26,280
extent, think that torturing yourself over bad things is somehow useful, it's a bit of

128
00:17:26,280 --> 00:17:34,680
a defense mechanism, we instead of actually trying to better ourselves, miss somehow think

129
00:17:34,680 --> 00:17:43,160
that if we hate, if we hate what we've done enough, we'll never want to do it again, that

130
00:17:43,160 --> 00:17:52,400
somehow by hating what you wanted to do, you can drive out the desire, so Malika did, she thought

131
00:17:52,400 --> 00:17:57,240
if she hated herself enough, if she felt guilty and bad about what she'd done enough, the

132
00:17:57,240 --> 00:18:03,840
anger would consume the lust, would consume whatever it was that drove her to do such

133
00:18:03,840 --> 00:18:04,840
things.

134
00:18:04,840 --> 00:18:14,320
In fact, it's a foolish, it's like trying to dig out one thorn with another thorn,

135
00:18:14,320 --> 00:18:20,080
you get a thorn in your side and then you stick another jab, another thorn in there trying

136
00:18:20,080 --> 00:18:27,880
to get it out, you don't get a thorn out with a thorn, you just hurt yourself, more

137
00:18:27,880 --> 00:18:33,120
you're liable to get the second one stuck in there, greed isn't solved by anger and that's

138
00:18:33,120 --> 00:18:40,920
really what guilt is, feeling bad, feeling upset about what you've done, so it's important

139
00:18:40,920 --> 00:18:44,160
to be mindful, I mean she would have had no problem if she'd learned to be mindful

140
00:18:44,160 --> 00:18:50,720
of her guilt and mindful of what she'd done, it isn't something that should have centered

141
00:18:50,720 --> 00:18:56,200
hell and as you can see it was only for seven days but it wasn't even the deed itself,

142
00:18:56,200 --> 00:19:04,480
not even the lying which people lie, unless they become compulsive and habitual liars, it's

143
00:19:04,480 --> 00:19:10,920
not likely to have great consequences but the guilt, there's a story in one of the, I think

144
00:19:10,920 --> 00:19:21,640
in the jotica maybe, of a monk who, as he was going down the river on a boat, he broke

145
00:19:21,640 --> 00:19:26,760
a blade of grass on the shore which is against the rules and he felt really guilty about

146
00:19:26,760 --> 00:19:31,520
it, but he couldn't find anyone to confess it to her so he died, not having confessed

147
00:19:31,520 --> 00:19:35,840
his offense, confessing is something we do as monks, we tell someone, hey I broke a

148
00:19:35,840 --> 00:19:41,960
rule, here I'm letting you know and I try not to do it again, if we don't do that we've

149
00:19:41,960 --> 00:19:50,400
got an offense against us that we have to eventually confess and he felt so guilty about

150
00:19:50,400 --> 00:19:59,720
it he went to hell just for that, so again a reminder that it's not the deans, doing

151
00:19:59,720 --> 00:20:07,280
something is never bad, never evil, it's the intention, so you have the intention to

152
00:20:07,280 --> 00:20:11,440
commit the deed but then you have the intention also to be angry and upset about it and

153
00:20:11,440 --> 00:20:20,440
hate yourself because of it, the meditators, it's a good thing for us to remember, all

154
00:20:20,440 --> 00:20:28,240
of these, the mind is chief and it's the mind which leads us to heaven, it's the mind

155
00:20:28,240 --> 00:20:36,480
which leads us to hell, no question about it, it's actually quite a scary thing and again

156
00:20:36,480 --> 00:20:40,640
an important reason for us to think about old age sickness and death and we don't really

157
00:20:40,640 --> 00:20:49,560
know where we're going, we may very well do lots of good deeds during our life and then

158
00:20:49,560 --> 00:20:57,440
when we die be so upset that we wind up in hell for at least a short time, so our determination

159
00:20:57,440 --> 00:21:01,200
should be not to be born here or born there, it should be the set ourselves on the

160
00:21:01,200 --> 00:21:09,920
good, atasamap anititya, etamungalamutum, the greatest blessing is not to get good things

161
00:21:09,920 --> 00:21:22,400
but to set yourself on the right path because good comes from good, so there you go, that's

162
00:21:22,400 --> 00:21:26,920
the dumb pad that verse for today, thank you for tuning in, have a good night.

