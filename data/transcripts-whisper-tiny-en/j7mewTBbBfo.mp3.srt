1
00:00:00,000 --> 00:00:06,920
How can one know what actions thoughts are wholesome or unwholesome when all

2
00:00:06,920 --> 00:00:11,640
definitions and grounds for morality are quite individual and relative? A

3
00:00:11,640 --> 00:00:17,960
person psyche might be at ease with stealing lying or killing and it can be

4
00:00:17,960 --> 00:00:26,520
done selfless thing. Are you sure? Well you're asking a question what you're

5
00:00:26,520 --> 00:00:32,960
presuming something? You're presuming what we call relative truths I think

6
00:00:32,960 --> 00:00:40,120
or relative morality when it's a debate. Many people believe in absolute

7
00:00:40,120 --> 00:00:46,880
morality. Mostly theists and religious people believe in absolute morality and

8
00:00:46,880 --> 00:00:52,880
they usually give God as a reason. Now Buddhism has, I've answered this question

9
00:00:52,880 --> 00:01:02,680
before Buddhism has a much more mundane definition or argument in favor of

10
00:01:02,680 --> 00:01:08,480
absolute morality and that is the definition of morality being that the

11
00:01:08,480 --> 00:01:16,400
definition of an immoral act as something that leads to suffering. So

12
00:01:16,400 --> 00:01:25,280
Buddhism has a utilitarian view of morality. Something is immoral if it causes

13
00:01:25,280 --> 00:01:32,440
suffering. Now Buddhism also has an absolute view of morality in the sense that

14
00:01:32,440 --> 00:01:43,440
there is the claim that certain mind states inevitably lead to suffering

15
00:01:43,440 --> 00:01:50,960
inevitably have as their result unpleasantness, stress, suffering. They can't

16
00:01:50,960 --> 00:01:58,000
be avoided. So this is why Buddhism and mind states not actions because

17
00:01:58,000 --> 00:02:03,960
actions are very ethically variable. It's uncertain. Killing is potentially

18
00:02:03,960 --> 00:02:09,880
ethically very variable because it can be done unintentionally. If you step

19
00:02:09,880 --> 00:02:12,760
on an ant, didn't know the ant was there, we don't consider it to be an

20
00:02:12,760 --> 00:02:17,880
unethical act. If you knew the ant was there, if you want to kill the ant even

21
00:02:17,880 --> 00:02:22,400
though you don't actually do it, we still consider it unethical. The thought

22
00:02:22,400 --> 00:02:28,000
is unwholesome because it's going to change you, it will create the habit of

23
00:02:28,000 --> 00:02:34,400
being violent, contribute to the habit and and we'll lead to all sorts of

24
00:02:34,400 --> 00:02:44,720
unpleasantness as a result. So we disagree that all definitions and grounds

25
00:02:44,720 --> 00:02:49,720
were morality or individual and relative. We do have an individual

26
00:02:49,720 --> 00:02:53,720
definition and grounds for morality but we don't consider it to be

27
00:02:53,720 --> 00:03:04,040
relative. We consider it to be absolute because there are certain mind states that are

28
00:03:04,040 --> 00:03:10,840
unethical by their very nature and that that leaned invariably to

29
00:03:10,840 --> 00:03:15,200
suffering to stress and those are the things we consider to be unethical. So we

30
00:03:15,200 --> 00:03:24,200
believe and also we understand through experience, through introspection,

31
00:03:24,200 --> 00:03:33,200
through observation, that things like anger, greed, arrogance, conceit, can

32
00:03:33,200 --> 00:03:38,480
never be of any use. The mind states are never valuable, never beneficial,

33
00:03:38,480 --> 00:03:44,800
are always harmful. If it could be minuscule, the result could be minuscule

34
00:03:44,800 --> 00:03:51,440
but they are invariably negative and therefore unethical in the Buddhist

35
00:03:51,440 --> 00:03:57,440
sense. So a person's psyche might be at ease with stealing lying or killing and it

36
00:03:57,440 --> 00:04:03,320
can be done selflessly. I disagree that it can be done selflessly unless it's

37
00:04:03,320 --> 00:04:11,000
done unintentionally. A person who is truly selfless in a Buddhist sense will not

38
00:04:11,000 --> 00:04:16,600
kill because selflessness also means not seeing another person's self. It

39
00:04:16,600 --> 00:04:20,400
doesn't mean altruism. It means really and truly not clinging to the idea of

40
00:04:20,400 --> 00:04:25,360
entities. If you don't cling to entities, you don't kill. You don't have reason to

41
00:04:25,360 --> 00:04:34,040
kill. Stealing and lying also will not be performed by someone who has given up

42
00:04:34,040 --> 00:04:39,120
the idea of self. A person who does these things will never be at ease, can

43
00:04:39,120 --> 00:04:45,040
never truly be at ease. This is the claim because what when you watch, you see

44
00:04:45,040 --> 00:04:48,840
the person's psyche when they're lying, it's twisted. Lying is twisting

45
00:04:48,840 --> 00:04:58,040
reality. Killing is harming and it's disrupting. Stealing is again,

46
00:04:58,040 --> 00:05:24,720
and harming is corrupt. It's a corrupt act and so it's done with negative people who are able to do these things with ease are because it's so familiar and they're so steeped in corruption. So the first time an ordinary person when they steal or kill for the first time, it shakes them up. It disturbs them because the

47
00:05:24,720 --> 00:05:50,720
people would have said it's like a clean person, a person who is faceted just in cleanliness, getting some dirt on their fingers or getting some dirt on their clothes. They're quite quick to clean it up. They're horrified by it. But a person who is steeped and soiled in dirt and fills wouldn't be at all bothered by getting something on their shirt or body.

48
00:05:50,720 --> 00:06:17,720
And it's the case with unwholesomeness. People who are steeped in unwholesomeness eventually become a new call in your door or in desensitized to it. But it's building up. There's so much, there's so much salt in the ocean if you drop a bit of salt you don't notice it. But the salt content hasn't increased. It's just so salty already you don't notice.

