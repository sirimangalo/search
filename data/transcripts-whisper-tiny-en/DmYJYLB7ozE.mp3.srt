1
00:00:00,000 --> 00:00:07,000
I'm 18 and feel that I'm too young to follow the monk path and work towards

2
00:00:07,000 --> 00:00:11,400
Nirvana. Wouldn't it be best for me to follow the materialistic path now

3
00:00:11,400 --> 00:00:18,280
temporarily until I reach old age before walking the Nirvana path as then I'd

4
00:00:18,280 --> 00:00:33,180
be more mature? Right. First of all, many parts of this question. First of all,

5
00:00:33,180 --> 00:00:37,640
there's no need to ever walk the monk path. If you're really serious about it, then

6
00:00:37,640 --> 00:00:46,600
the monk path is a great one. Second of all, there's a miss, I think there's a

7
00:00:46,600 --> 00:01:01,680
misleading premise here that somehow physical maturity or age somehow implies

8
00:01:01,680 --> 00:01:07,480
better practice. I think in certain aspects of the practice are

9
00:01:07,480 --> 00:01:17,800
benefited by physical maturity in the sense of greater experience of the world. But a

10
00:01:17,800 --> 00:01:27,240
great portion or a great part of the path is actually hindered by age because

11
00:01:27,240 --> 00:01:32,880
especially in someone who has spent most of their life following the materialistic

12
00:01:32,880 --> 00:01:41,520
paths. So the materialistic path, if I understand it, if you're meaning it as

13
00:01:41,520 --> 00:01:49,880
I understand it, is an unwholesome one. It's one that leads to clinging, it

14
00:01:49,880 --> 00:01:56,400
leads to conflict, it leads to delusion and attachment and identification.

15
00:01:56,400 --> 00:02:00,800
All sorts of bad stuff. And so when that becomes your habit over

16
00:02:00,800 --> 00:02:05,600
throughout your life, the idea that that would somehow make you a better

17
00:02:05,600 --> 00:02:15,880
meditator is unreasonable and unlikely. Now, if you are talking about not

18
00:02:15,880 --> 00:02:21,120
becoming a monk for a long time, the idea that of practicing meditation and

19
00:02:21,120 --> 00:02:25,520
doing good deeds now to slowly, slowly cultivate enough goodness to become a

20
00:02:25,520 --> 00:02:29,680
monk, well, that's a different argument, but I still don't think it holds because

21
00:02:29,680 --> 00:02:34,760
being a monk is even more difficult than practicing as a lay person, physically,

22
00:02:34,760 --> 00:02:41,680
and as a lifestyle, a person who has lived their whole life not used to the

23
00:02:41,680 --> 00:02:47,920
monastic discipline. The life of being a monk will have a very difficult time in

24
00:02:47,920 --> 00:02:54,680
their old age, practicing all of the many rules, eating only once a day and so

25
00:02:54,680 --> 00:03:03,200
on. It doesn't get easier because you've waited a long time to begin it. So

26
00:03:03,200 --> 00:03:07,280
even if it's about becoming a monk, absolutely, the Buddha himself recommended

27
00:03:07,280 --> 00:03:11,560
that young people, it's for young people to do. Or dating when you're 20 is a

28
00:03:11,560 --> 00:03:19,520
perfect time, the best time. Even if it's difficult at first, for a person who's

29
00:03:19,520 --> 00:03:26,600
able to practice it, it's really the best time of one's life because one's

30
00:03:26,600 --> 00:03:33,320
able to grow up as a monk and grow up and surrounded by the dhamma, surrounded by

31
00:03:33,320 --> 00:03:38,000
the Buddha's teaching. So certainly no benefit to walking the materialistic path.

32
00:03:38,000 --> 00:03:42,400
I don't think any benefit if you want to become a monk for waiting until

33
00:03:42,400 --> 00:03:49,600
you're very old or old at all, really. 18 while you have to be 20 to ordain, and

34
00:03:49,600 --> 00:03:54,600
you might want to wait a little bit longer, but certainly don't fall, ever fall in

35
00:03:54,600 --> 00:03:59,480
the materialistic path. It's not useful for anyone in any situation. But as for

36
00:03:59,480 --> 00:04:03,600
following the lay person's paths, isn't that reasonable? But don't think that

37
00:04:03,600 --> 00:04:07,360
it's going to make it easier to become a monk when you're old. It just might

38
00:04:07,360 --> 00:04:11,640
make you a bad monk when those monks are just sits in their room and watch

39
00:04:11,640 --> 00:04:14,560
the television all day.

