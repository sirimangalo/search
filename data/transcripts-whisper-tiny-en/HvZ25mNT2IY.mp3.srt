1
00:00:00,000 --> 00:00:05,000
How does the attainment of the higher paths such as becoming a Sakadakami

2
00:00:05,000 --> 00:00:10,000
and the fruits differ from the attainment of stream entry?

3
00:00:10,000 --> 00:00:15,000
They differ based on the defilements that they cut.

4
00:00:15,000 --> 00:00:25,000
And it is difficult really to match practice and theory

5
00:00:25,000 --> 00:00:30,000
because in theory it seems to be a very definite boom

6
00:00:30,000 --> 00:00:32,000
like a signpost thing where you can now see

7
00:00:32,000 --> 00:00:35,000
now you're a Sakadakami, now you're an Anakami

8
00:00:35,000 --> 00:00:38,000
that is totally different from the practice,

9
00:00:38,000 --> 00:00:40,000
like you're practicing practicing practicing practicing practicing in poems,

10
00:00:40,000 --> 00:00:42,000
suddenly you're a Sotapana.

11
00:00:42,000 --> 00:00:47,000
I'll not say Sotapana, that is actually quite a profound shift

12
00:00:47,000 --> 00:00:51,000
but from a Sotapana to a Sakadakami, for example,

13
00:00:51,000 --> 00:00:55,000
in practice, I would say the safest thing

14
00:00:55,000 --> 00:01:00,000
both from our practical experience and still keeping with the theory

15
00:01:00,000 --> 00:01:07,000
is it's simply a marker, like a marker on a thermostat

16
00:01:07,000 --> 00:01:10,000
that when you get to a certain level,

17
00:01:10,000 --> 00:01:12,000
when the temperature gets to a certain level,

18
00:01:12,000 --> 00:01:17,000
you can see that it's past that line or a gas tank

19
00:01:17,000 --> 00:01:19,000
because you're emptying all the defilements out

20
00:01:19,000 --> 00:01:21,000
so when it gets to a certain line, you can say,

21
00:01:21,000 --> 00:01:23,000
now it's gone three quarters of a tank,

22
00:01:23,000 --> 00:01:26,000
now it's half a tank, and now it's quarter of a tank

23
00:01:26,000 --> 00:01:29,000
because in between Sotapana and Sakadakami,

24
00:01:29,000 --> 00:01:33,000
there are still the realizations of Nimana.

25
00:01:33,000 --> 00:01:36,000
One practice is again and again and realizes

26
00:01:36,000 --> 00:01:40,000
attains the fruit of the first path again.

27
00:01:40,000 --> 00:01:44,000
One can never realize the path of the Sotapana,

28
00:01:44,000 --> 00:01:50,000
the Sotapana, Sotapatimaga, again, because that's just a technical name

29
00:01:50,000 --> 00:01:52,000
for the first realization of Nimana.

30
00:01:52,000 --> 00:01:55,000
The next realization and subsequent realizations

31
00:01:55,000 --> 00:02:03,000
are Polanyan, and so you can continue to go back to Polanyan,

32
00:02:03,000 --> 00:02:07,000
which will continue to weaken the defilements.

33
00:02:07,000 --> 00:02:15,000
But until one reaches a certain unverifiable state,

34
00:02:15,000 --> 00:02:18,000
you can assume the Buddha, a certain unrecognizable state,

35
00:02:18,000 --> 00:02:25,000
where there's not enough defilements left to cause you

36
00:02:25,000 --> 00:02:32,000
to be born more than once as a human being or as an angel.

37
00:02:32,000 --> 00:02:35,000
Then you can say, you're a Sakadakami, but it's very difficult.

38
00:02:35,000 --> 00:02:38,000
You're not a Buddha, how could you know what that line is?

39
00:02:38,000 --> 00:02:40,000
An anangami will be more easy to see,

40
00:02:40,000 --> 00:02:45,000
because you get to the point where you have eradicated all

41
00:02:45,000 --> 00:02:57,000
Kamaraga and Batika, all greed or all lust and diversion.

42
00:02:57,000 --> 00:03:01,000
And Arahad is, I would say, easier to see,

43
00:03:01,000 --> 00:03:08,000
because at the realization of Arahad, there is no defilements,

44
00:03:08,000 --> 00:03:13,000
and one realizes this one when comes out of the Polanyan,

45
00:03:13,000 --> 00:03:16,000
one is able to see what defilements are left,

46
00:03:16,000 --> 00:03:18,000
because the mind is very pure at that point,

47
00:03:18,000 --> 00:03:21,000
like clear water, and one is able to see clearly

48
00:03:21,000 --> 00:03:24,000
what defilements are left, what defilements are gone,

49
00:03:24,000 --> 00:03:30,000
and so what remains to be done.

50
00:03:30,000 --> 00:03:34,000
But the actual realization is the same.

51
00:03:34,000 --> 00:03:37,000
So if you want to forget about all four of these things,

52
00:03:37,000 --> 00:03:42,000
you just say the realization, the subsequent,

53
00:03:42,000 --> 00:03:48,000
and continue repeated realization of Nibana,

54
00:03:48,000 --> 00:03:50,000
cuts off more and more defilements,

55
00:03:50,000 --> 00:03:53,000
and you just have these markers until they're all gone,

56
00:03:53,000 --> 00:04:00,000
and then it's Arahad.

