1
00:00:00,000 --> 00:00:14,000
Today it's my turn again to give the talk.

2
00:00:14,000 --> 00:00:22,000
Today's talk I thought would be on love.

3
00:00:22,000 --> 00:00:31,000
Last night we got a call from the British Broadcasting Corporation.

4
00:00:31,000 --> 00:00:36,000
They called the BBC.

5
00:00:36,000 --> 00:00:43,000
And left a comment on a news article about Burma.

6
00:00:43,000 --> 00:00:47,000
And they got back to me and asked if they could call me.

7
00:00:47,000 --> 00:01:01,000
I was quite surprised and they called and talked with some different people on live on the BBC.

8
00:01:01,000 --> 00:01:10,000
On and off we're almost half an hour.

9
00:01:10,000 --> 00:01:23,000
And I think this is an appropriate reason to connect this with the talk on love.

10
00:01:23,000 --> 00:01:36,000
Because what we find in the world nowadays are many situations

11
00:01:36,000 --> 00:01:46,000
where we find ourselves as global citizens as citizens of various nations

12
00:01:46,000 --> 00:01:53,000
as members of various religious organizations.

13
00:01:53,000 --> 00:02:04,000
In this case we find ourselves as Buddhists being confronted with situations where we are forced to

14
00:02:04,000 --> 00:02:18,000
find something to say we're forced to take a position.

15
00:02:18,000 --> 00:02:24,000
It's very easy for us simply to say well we're meditators.

16
00:02:24,000 --> 00:02:30,000
The world doesn't matter to us.

17
00:02:30,000 --> 00:02:36,000
And not say anything at all.

18
00:02:36,000 --> 00:02:41,000
And in some ways this is most appealing.

19
00:02:41,000 --> 00:02:46,000
If indeed we have no connection with the outside world.

20
00:02:46,000 --> 00:02:53,000
And there's no one calling us on the phone.

21
00:02:53,000 --> 00:03:06,000
Then most likely the most comfortable thing for us to do is simply just continue meditating.

22
00:03:06,000 --> 00:03:15,000
But when we undertake to become meditators on a long-term basis

23
00:03:15,000 --> 00:03:25,000
it comes to be a part of our daily life.

24
00:03:25,000 --> 00:03:33,000
Or our daily life comes to be a part of being meditation practice.

25
00:03:33,000 --> 00:03:37,000
And we find ourselves confronted with situations where we have to teach.

26
00:03:37,000 --> 00:03:44,000
Where we have to teach meditation we have to teach the Buddha's teaching.

27
00:03:44,000 --> 00:03:53,000
And we start to see that meditation actually is a much bigger subject than simply walking and sitting.

28
00:03:53,000 --> 00:04:05,000
The ways of explaining about meditation about freedom from suffering are actually a very broad subject.

29
00:04:05,000 --> 00:04:08,000
When we're talking to people who have never meditated before,

30
00:04:08,000 --> 00:04:11,000
perhaps we never think of meditating.

31
00:04:11,000 --> 00:04:18,000
We have to use different words, different phrases, and start at different levels.

32
00:04:18,000 --> 00:04:32,000
But we cannot simply close our eyes and continue practicing because we find ourselves funded by these people.

33
00:04:32,000 --> 00:04:42,000
We find ourselves building a sense of love for these people.

34
00:04:42,000 --> 00:04:52,000
Now, I'll get into the idea of love in just a second, but just a little bit more about the situation which we're talking about in Burma.

35
00:04:52,000 --> 00:05:09,000
It appears that the situation has become so repressive and so terrible or uncomfortable for the people of Burma.

36
00:05:09,000 --> 00:05:15,000
That they can take it no longer than they are willing to die.

37
00:05:15,000 --> 00:05:29,000
They're willing to be killed rather than continue to face the horrible repression and suffering conditions.

38
00:05:29,000 --> 00:05:57,000
We might want to say that these people are simply suffering from their karma and should be quiet and continue to suffer and wait for the end to come or wait for the end of their suffering to come.

39
00:05:57,000 --> 00:06:00,000
But it's probably not wise to say that.

40
00:06:00,000 --> 00:06:08,000
It probably doesn't take into account the manufacturers of the situation that most likely the end of suffering is here,

41
00:06:08,000 --> 00:06:12,000
and this is how the end of their suffering is coming from many people.

42
00:06:12,000 --> 00:06:22,000
That bad karma which is running its course is now coming to an end.

43
00:06:22,000 --> 00:06:31,000
And there is a change coming perhaps for the better for some people perhaps for the worst for some people.

44
00:06:31,000 --> 00:06:51,000
But the important point and the important topic for myself and I think for all of us is the position which the Buddhist monks involved in the protests should be taking.

45
00:06:51,000 --> 00:06:59,000
And this is where I like to start to talk about the idea of love.

46
00:06:59,000 --> 00:07:10,000
Because the Buddhist monks have taken the stance in Burma wishing for love for their to be loved.

47
00:07:10,000 --> 00:07:17,000
And perhaps love is a bad word but it's important to use it as a title because I'd like to talk about the different kinds of love.

48
00:07:17,000 --> 00:07:26,000
But here they mean loving kindness or kindness which springs from love.

49
00:07:26,000 --> 00:07:33,000
And so we see pictures and hears, hears and chanting from the monks chanting the Mehta Supta,

50
00:07:33,000 --> 00:07:46,000
the discourse on loving kindness, wishing for their to be sympathy and caring and love among the people of Burma rather than repression and authoritarianism and so on.

51
00:07:46,000 --> 00:07:55,000
And personally though I could certainly be wrong.

52
00:07:55,000 --> 00:08:05,000
I don't think this is a bad thing to see monks even taking to the street to let it be known.

53
00:08:05,000 --> 00:08:19,000
To announce or to proclaim to the people in the people in the people in public in Burma even to the ordinary people.

54
00:08:19,000 --> 00:08:30,000
To proclaim to them the truth and proclaim that which is wrong to be wrong that which is right to be right.

55
00:08:30,000 --> 00:08:46,000
And I think to this extent proclaiming denouncing hatred and stinginess and miserliness for what it is.

56
00:08:46,000 --> 00:09:02,000
And wishing for people to be more loving and more caring at the people in charge might be more caring for their citizens.

57
00:09:02,000 --> 00:09:08,000
But I think it's important that it stops there.

58
00:09:08,000 --> 00:09:16,000
The teaching I would like to talk about today is the teaching on love.

59
00:09:16,000 --> 00:09:26,000
And I think this is the most powerful weapon which we have and which the people of Burma now have.

60
00:09:26,000 --> 00:09:47,000
One of the most powerful weapons that we will have in dealing with enemy forces, in dealing with people who are antagonistic towards us.

61
00:09:47,000 --> 00:09:59,000
And we hear around the world of people or nations of those people in power getting ready to impose sanctions on Burma.

62
00:09:59,000 --> 00:10:12,000
And of course we have sanctions years and years and years we already have sanctions towards Burma.

63
00:10:12,000 --> 00:10:21,000
And I don't have anything to say really on that. I think it's a fairly political subject which I would rather stay away from.

64
00:10:21,000 --> 00:10:38,000
But I think a much more important and a much more beneficial thing right now is to examine the situation in Burma, examine the situations in our own societies and our own lives and our own families.

65
00:10:38,000 --> 00:10:49,000
And come to see that this world is very much lacking in the virtue of loving kindness.

66
00:10:49,000 --> 00:10:54,000
What we need for Burma now is love.

67
00:10:54,000 --> 00:11:00,000
What we need for each other is a sense of love.

68
00:11:00,000 --> 00:11:13,000
And surely it is not something which will change things overnight. It probably won't stop people in Burma from being killed.

69
00:11:13,000 --> 00:11:17,000
But then you can't stop people from dying.

70
00:11:17,000 --> 00:11:24,000
In the end everyone whether they're killed or run over by a car or a day of old age in the end we all die.

71
00:11:24,000 --> 00:11:35,000
This is only a short part of our trip. If we die of good wound maybe next life will come back and live a long and happy life.

72
00:11:35,000 --> 00:11:51,000
If we live a long and pleasurable life this time it can be that next life we will be born in a repressive situation ourselves based on our good or bad deeds.

73
00:11:51,000 --> 00:12:06,000
But if we wish for these things to disappear from the world then the most important thing for us is to develop these qualities of mind such as loving kindness.

74
00:12:06,000 --> 00:12:12,000
And so today I'd like to talk about love given this is an introduction.

75
00:12:12,000 --> 00:12:23,000
So that we may all work to develop it in our own hearts for the benefit of not only ourselves but also the world around us.

76
00:12:23,000 --> 00:12:27,000
And the idea of love it's a very dangerous subject.

77
00:12:27,000 --> 00:12:34,000
You can get angry at someone for not being loving.

78
00:12:34,000 --> 00:12:41,000
You can get angry at someone for telling you that you're not loving enough.

79
00:12:41,000 --> 00:12:48,000
We can be very tricky with the word love. We can kill ourselves.

80
00:12:48,000 --> 00:12:53,000
Over love we can kill other people over love.

81
00:12:53,000 --> 00:13:02,000
And so we see we have many different kinds of love.

82
00:13:02,000 --> 00:13:12,000
And often this subject comes up when I talk about detachment.

83
00:13:12,000 --> 00:13:20,000
When you give the talk on detachment of people who are very little interested in Buddhism maybe only curious.

84
00:13:20,000 --> 00:13:27,000
One of the first things they'll ask is what about all the people I love.

85
00:13:27,000 --> 00:13:33,000
I love my children you want me to be to tell them.

86
00:13:33,000 --> 00:13:51,000
And this question, this very question is a very good way to explain to people how love can be a good thing or can be a bad thing.

87
00:13:51,000 --> 00:13:57,000
And so I always say that your attachment to your children is not love.

88
00:13:57,000 --> 00:14:01,000
Your attachment to your children is attachment.

89
00:14:01,000 --> 00:14:06,000
Your love for them is your love for them. Your attachment to them is your attachment to them.

90
00:14:06,000 --> 00:14:15,000
The very big thing in your heart towards your children or towards your family or towards your friends.

91
00:14:15,000 --> 00:14:22,000
This very big thing which makes the heart expand, which makes the heart grow, this is called love.

92
00:14:22,000 --> 00:14:36,000
And that very little thing which contracts the heart, which defiles the heart, which makes the heart grow.

93
00:14:36,000 --> 00:14:43,000
This makes the heart suffer. This is called attachment.

94
00:14:43,000 --> 00:14:54,000
And the difference can be understood in terms of the expression, an expression of love and an expression of attachment.

95
00:14:54,000 --> 00:15:01,000
Whereas we call some things love actually in reality it's nothing to do with love, it has only to do with attachment.

96
00:15:01,000 --> 00:15:08,000
Or it has to do with love in an English sense but not in terms of a Buddhist sense.

97
00:15:08,000 --> 00:15:15,000
Or perhaps the word love is simply just a bad word, a two-general word.

98
00:15:15,000 --> 00:15:19,000
But the expression of love and the expression of attachment are quite different.

99
00:15:19,000 --> 00:15:27,000
The expression of attachment is, may you make me happy.

100
00:15:27,000 --> 00:15:36,000
The expression of love is, may I make you happy.

101
00:15:36,000 --> 00:15:46,000
So when we do something or we say something or we even think something, wishing for another person to be happy, this is called love.

102
00:15:46,000 --> 00:15:53,000
This is not attachment.

103
00:15:53,000 --> 00:15:57,000
And so a detached person can clearly be very full of love.

104
00:15:57,000 --> 00:16:01,000
A person who has no attachment to any person in the world can be the most loving person in the world.

105
00:16:01,000 --> 00:16:02,000
Why?

106
00:16:02,000 --> 00:16:09,000
Because in their heart there is no, what have you got for me?

107
00:16:09,000 --> 00:16:12,000
There's no, be this way.

108
00:16:12,000 --> 00:16:14,000
If you be this way I'll be happy.

109
00:16:14,000 --> 00:16:16,000
If you be that way I'll be unhappy.

110
00:16:16,000 --> 00:16:17,000
Do this for me.

111
00:16:17,000 --> 00:16:18,000
Don't do that for me.

112
00:16:18,000 --> 00:16:24,000
Don't do that for me.

113
00:16:24,000 --> 00:16:36,000
A person who is very detached, it's clear that their heart will shrink and their heart will become rotten.

114
00:16:36,000 --> 00:16:40,000
Even to the point where people kill themselves.

115
00:16:40,000 --> 00:16:45,000
Where they destroy other people, kill other people.

116
00:16:45,000 --> 00:16:50,000
Always saying, make me happy, make me happy.

117
00:16:50,000 --> 00:16:56,000
We can see the result if you have a relationship for instance, any relationship between two people.

118
00:16:56,000 --> 00:16:59,000
Two people who are full of attachment.

119
00:16:59,000 --> 00:17:02,000
If two people are full of attachment they're always asking from each other.

120
00:17:02,000 --> 00:17:05,000
One thing from each other.

121
00:17:05,000 --> 00:17:08,000
So how much does each person get zero?

122
00:17:08,000 --> 00:17:11,000
Whether one gets anything.

123
00:17:11,000 --> 00:17:19,000
If you have a relationship between two people who are 100% giving,

124
00:17:19,000 --> 00:17:21,000
then both get 100%.

125
00:17:21,000 --> 00:17:23,000
Look at everything.

126
00:17:23,000 --> 00:17:25,000
I give you my 100%.

127
00:17:25,000 --> 00:17:27,000
You give me your 100%.

128
00:17:27,000 --> 00:17:32,000
Neither of us means anything and we both get everything.

129
00:17:32,000 --> 00:17:36,000
It's very hard for people to see and they ask these questions.

130
00:17:36,000 --> 00:17:38,000
They criticize Buddhism.

131
00:17:38,000 --> 00:17:41,000
They tell people to be cold and detached.

132
00:17:41,000 --> 00:17:46,000
Very dry and colorless life.

133
00:17:46,000 --> 00:17:50,000
And then you watch them running around like crazy people and you think,

134
00:17:50,000 --> 00:17:55,000
well, if that's color, I'll take black and white.

135
00:17:55,000 --> 00:18:03,000
I'll take white and give up the black.

136
00:18:03,000 --> 00:18:07,000
But we find our lives full of color.

137
00:18:07,000 --> 00:18:14,000
We find our lives full of warmth, kindness, love, satisfaction, peace, happiness.

138
00:18:14,000 --> 00:18:18,000
All the things which the people in the world strive for,

139
00:18:18,000 --> 00:18:21,000
proclaim as the ultimate virtues.

140
00:18:21,000 --> 00:18:24,000
They write it on their, they tattoo it on their arms.

141
00:18:24,000 --> 00:18:29,000
They write in their known books.

142
00:18:29,000 --> 00:18:33,000
They write poems and sing songs.

143
00:18:33,000 --> 00:18:38,000
They watch movies, read books.

144
00:18:38,000 --> 00:18:44,000
They try their, their utmost to bringing this love into their lives.

145
00:18:44,000 --> 00:18:50,000
But they get all mixed up and they come to attach more than actually love.

146
00:18:50,000 --> 00:18:57,000
They're unable to find the way to, to love.

147
00:18:57,000 --> 00:19:02,000
And so I think a crucial point,

148
00:19:02,000 --> 00:19:07,000
which we have to understand as people and as meditators.

149
00:19:07,000 --> 00:19:11,000
Is that in the Lord Buddha's teaching the best way to,

150
00:19:11,000 --> 00:19:16,000
for a person to be loving.

151
00:19:16,000 --> 00:19:22,000
And to do most efficient, the most fruitful path

152
00:19:22,000 --> 00:19:26,000
to become a loving kind person.

153
00:19:26,000 --> 00:19:29,000
It's not simply the practice of love,

154
00:19:29,000 --> 00:19:34,000
the practice of wishing good things for other people.

155
00:19:34,000 --> 00:19:44,000
The greatest path to find love is the path of gaining wisdom,

156
00:19:44,000 --> 00:19:48,000
gaining understanding.

157
00:19:48,000 --> 00:19:51,000
Because we see the world going around,

158
00:19:51,000 --> 00:19:53,000
loving, loving,

159
00:19:53,000 --> 00:19:57,000
but it's mixed with attachment.

160
00:19:57,000 --> 00:19:59,000
All the time loving, all the time loving.

161
00:19:59,000 --> 00:20:03,000
But without any sense of wisdom or understanding,

162
00:20:03,000 --> 00:20:06,000
without any mindfulness,

163
00:20:06,000 --> 00:20:08,000
we see people who, we say,

164
00:20:08,000 --> 00:20:11,000
oh, that person loves their dog.

165
00:20:11,000 --> 00:20:15,000
Boy, she really loves her dog.

166
00:20:15,000 --> 00:20:22,000
And the next moment we see the person hitting their dog.

167
00:20:22,000 --> 00:20:26,000
We see the mind is full of good things and bad things

168
00:20:26,000 --> 00:20:32,000
and it only knows its own inherent character.

169
00:20:32,000 --> 00:20:37,000
We know how to do things the way we have always done things.

170
00:20:37,000 --> 00:20:39,000
Sometimes good, sometimes bad,

171
00:20:39,000 --> 00:20:41,000
good mixed with bad.

172
00:20:41,000 --> 00:20:45,000
Love mixed with affection or attachment.

173
00:20:45,000 --> 00:20:51,000
And so we're unable to be true, to be truly helpful for people.

174
00:20:51,000 --> 00:20:54,000
There's a song which goes,

175
00:20:54,000 --> 00:20:59,000
this meditation teacher once pointed out to us.

176
00:20:59,000 --> 00:21:03,000
He said,

177
00:21:03,000 --> 00:21:06,000
Sting once, Sting once said,

178
00:21:06,000 --> 00:21:11,000
this song, if you love someone, set them free.

179
00:21:11,000 --> 00:21:13,000
I thought about that a lot,

180
00:21:13,000 --> 00:21:17,000
but really it's a very clever thing to say.

181
00:21:17,000 --> 00:21:21,000
It's not just a good song there.

182
00:21:21,000 --> 00:21:23,000
Sometimes song lyrics are not good.

183
00:21:23,000 --> 00:21:25,000
Don't give good advice, I think.

184
00:21:25,000 --> 00:21:28,000
But in this case, I think this song there gives good advice.

185
00:21:28,000 --> 00:21:33,000
If you love someone, set them free.

186
00:21:33,000 --> 00:21:38,000
When you love someone and then you give them

187
00:21:38,000 --> 00:21:44,000
gifts, you know, on their birthday or on holidays,

188
00:21:44,000 --> 00:21:48,000
this is really a lot more out of affection.

189
00:21:48,000 --> 00:21:54,000
You want to see them happy and when they're happy you feel happy.

190
00:21:54,000 --> 00:22:01,000
You like to rejoice with them and laugh and have fun and so on.

191
00:22:01,000 --> 00:22:04,000
Because so much of the rest of your life is full of

192
00:22:04,000 --> 00:22:13,000
dreariness and boredom and depression and so many different things.

193
00:22:13,000 --> 00:22:19,000
That you rejoice in these festive occasions.

194
00:22:19,000 --> 00:22:24,000
But if you really want to help someone, giving them gifts in this and that,

195
00:22:24,000 --> 00:22:27,000
this is very nice and useful and a good thing,

196
00:22:27,000 --> 00:22:30,000
a good, very good practice to keep up.

197
00:22:30,000 --> 00:22:32,000
In Thailand, on people's birthdays,

198
00:22:32,000 --> 00:22:34,000
on someone's birthday, you don't give them gifts.

199
00:22:34,000 --> 00:22:37,000
They give you gifts.

200
00:22:37,000 --> 00:22:43,000
At Jen's birthday this year was a little bit different than most years.

201
00:22:43,000 --> 00:22:45,000
There was some,

202
00:22:45,000 --> 00:22:48,000
that was very, very structured this year.

203
00:22:48,000 --> 00:22:51,000
And I'm not sure what their, the reasoning was,

204
00:22:51,000 --> 00:22:57,000
but Adjan didn't give out gifts to everyone who came to see him right away.

205
00:22:57,000 --> 00:22:59,000
He gave out gifts afterwards.

206
00:22:59,000 --> 00:23:03,000
So I managed to slip in near the end and get some of his gifts,

207
00:23:03,000 --> 00:23:08,000
but I think the meditators weren't able to get the gifts he had to give us.

208
00:23:08,000 --> 00:23:12,000
A picture and some other things.

209
00:23:12,000 --> 00:23:16,000
But what we did see probably is some of the,

210
00:23:16,000 --> 00:23:18,000
the monks who were coming in,

211
00:23:18,000 --> 00:23:21,000
and we saw the refrigerators being carted away at Jen,

212
00:23:21,000 --> 00:23:24,000
giving away 189 refrigerators.

213
00:23:24,000 --> 00:23:27,000
189 refrigerators.

214
00:23:27,000 --> 00:23:32,000
As well as robes and fans.

215
00:23:32,000 --> 00:23:37,000
Every year it's, this year perhaps the biggest gift giving yet.

216
00:23:37,000 --> 00:23:51,000
Probably some of the monks flying from Bangkok wondering how they're going to get their fridge back to Bangkok.

217
00:23:51,000 --> 00:23:53,000
But these are, these are good things to do.

218
00:23:53,000 --> 00:23:59,000
But in the end what makes Adjan our teachers so famous and so well loved.

219
00:23:59,000 --> 00:24:02,000
It's not his giving of gifts.

220
00:24:02,000 --> 00:24:04,000
It's his giving of the highest gift.

221
00:24:04,000 --> 00:24:06,000
It's his giving of the gift of freedom.

222
00:24:06,000 --> 00:24:09,000
This is the meditation practice which we follow.

223
00:24:13,000 --> 00:24:16,000
The greatest thing which we can do for the world.

224
00:24:16,000 --> 00:24:20,000
Whether it be in reference to a specific country.

225
00:24:20,000 --> 00:24:25,000
And Burma is not the only situation in the world where people are suffering.

226
00:24:25,000 --> 00:24:29,000
Even in very well developed countries there are many people suffering.

227
00:24:29,000 --> 00:24:32,000
There are rich people suffering horribly.

228
00:24:32,000 --> 00:24:34,000
Horrible suffering.

229
00:24:34,000 --> 00:24:36,000
Rich people.

230
00:24:36,000 --> 00:24:38,000
People well off.

231
00:24:38,000 --> 00:24:42,000
People taking antidepressant drugs.

232
00:24:42,000 --> 00:24:46,000
People in upper class families taking antidepressant.

233
00:24:48,000 --> 00:24:51,000
There are countries in the world where people don't have enough food to eat.

234
00:24:51,000 --> 00:24:54,000
Worse than Burma I would think.

235
00:24:54,000 --> 00:24:56,000
But I don't know.

236
00:24:56,000 --> 00:25:05,000
There have been situations in every country of the world where people have suffered horribly.

237
00:25:05,000 --> 00:25:12,000
The Cambodian people how much worse they're suffering was than the suffering of Burma now.

238
00:25:14,000 --> 00:25:20,000
So much so that when I talked to a young monk at the time and it was probably my own fault.

239
00:25:20,000 --> 00:25:24,000
I was talking to a Cambodian monk about Cambodia.

240
00:25:24,000 --> 00:25:29,000
And he was talking about all the horrors and tragedy that I couldn't just, I just couldn't understand.

241
00:25:29,000 --> 00:25:32,000
And I was willing to accept, yes, I can't understand.

242
00:25:32,000 --> 00:25:35,000
But I said, well it's really all just their bad karma.

243
00:25:35,000 --> 00:25:38,000
And he got his face turned red.

244
00:25:40,000 --> 00:25:42,000
And he got very upset.

245
00:25:42,000 --> 00:25:53,000
The suffering in this world is quite incredible.

246
00:25:53,000 --> 00:25:58,000
It's something that we often don't see because our situation is so wonderful.

247
00:25:58,000 --> 00:26:05,000
We get depressed when we lose a job or when we fail a test.

248
00:26:05,000 --> 00:26:13,000
When we lose a girlfriend, people will kill themselves when they lose a girlfriend.

249
00:26:13,000 --> 00:26:16,000
These people in the world don't have enough food to eat.

250
00:26:16,000 --> 00:26:20,000
People who see their children taken away from them.

251
00:26:20,000 --> 00:26:24,000
Children who see their parents killed before their eyes.

252
00:26:24,000 --> 00:26:26,000
Tortured before their eyes.

253
00:26:26,000 --> 00:26:33,000
What this world needs is love.

254
00:26:33,000 --> 00:26:42,000
It means to develop the wisdom which can allow people to love one another and express kindness towards each other.

255
00:26:42,000 --> 00:26:45,000
All of us need this love.

256
00:26:45,000 --> 00:26:48,000
All of us we need this wisdom.

257
00:26:48,000 --> 00:26:58,000
So I got a fairly harsh email this morning from someone who's a good friend of mine.

258
00:26:58,000 --> 00:27:10,000
Maybe not harsh but hard email telling me I had to stand up with my brothers in Burma and solidarity.

259
00:27:10,000 --> 00:27:16,000
And I thought about it and I replied, but after I replied I thought more about it.

260
00:27:16,000 --> 00:27:23,000
And I realized really I am standing up in solidarity with my brothers in Burma.

261
00:27:23,000 --> 00:27:27,000
Because my brothers in Burma are off meditating in the forest.

262
00:27:27,000 --> 00:27:36,000
The young monks in Rangoon are doing something very noble, I'm sure.

263
00:27:36,000 --> 00:27:45,000
Perhaps some of them are losing their mindfulness and becoming less than noble.

264
00:27:45,000 --> 00:27:48,000
And I think that's the key is that we will see in the future.

265
00:27:48,000 --> 00:27:53,000
In the near future we will see a lot of suffering because people are losing their mindfulness.

266
00:27:53,000 --> 00:28:00,000
They are losing their sense of love because they want something which they can't get.

267
00:28:00,000 --> 00:28:07,000
And that goal might be very noble and might be very wonderful.

268
00:28:07,000 --> 00:28:16,000
But the goal is meaningless if your means are heading you in another direction.

269
00:28:16,000 --> 00:28:28,000
The greatest thing about the monks, the Buddhist monk is his moral, the Buddhist nun, his or her moral in superiority.

270
00:28:28,000 --> 00:28:30,000
We are not in the world.

271
00:28:30,000 --> 00:28:35,000
We can comment on the world, we can teach the world because we are not involved.

272
00:28:35,000 --> 00:28:41,000
We can talk on Burma because we don't take sides and say the junta is bad.

273
00:28:41,000 --> 00:28:49,000
People of Burma deserve to be free, democracy is we don't take sides.

274
00:28:49,000 --> 00:28:56,000
We explain to people all people that we are all people.

275
00:28:56,000 --> 00:28:59,000
People who repress these are people.

276
00:28:59,000 --> 00:29:04,000
Perhaps maybe they have families, maybe they have good things inside them.

277
00:29:04,000 --> 00:29:09,000
They can be very, very evil people and suddenly they can change.

278
00:29:09,000 --> 00:29:17,000
Maybe in a past life they were repressed, oppressed, persecuted, and they got angry about it.

279
00:29:17,000 --> 00:29:21,000
So now they came back and they want revenge.

280
00:29:21,000 --> 00:29:23,000
This is ordinary people.

281
00:29:23,000 --> 00:29:29,000
We have so much bitterness and cruelty inside of us that we have to do away with.

282
00:29:29,000 --> 00:29:41,000
All people, whether we are in Thailand or Burma, I know that there are great many people who understand this.

283
00:29:41,000 --> 00:29:46,000
Whether they be on the streets, chanting, loving kindness, wishing for loving kindness,

284
00:29:46,000 --> 00:29:58,000
or whether they be in a meditation center, practicing to develop real and true loving kindness in their hearts.

285
00:29:58,000 --> 00:30:06,000
At this point is the most important point in regards to the problems which exist in this world.

286
00:30:06,000 --> 00:30:14,000
When we can learn to have love and compassion, enjoy.

287
00:30:14,000 --> 00:30:22,000
When we can have the wisdom which is able to seek clearly all things inside of ourselves in the world around us,

288
00:30:22,000 --> 00:30:29,000
this is when we can truly help the world.

289
00:30:29,000 --> 00:30:34,000
For this reason we come to, for this and many reasons,

290
00:30:34,000 --> 00:30:37,000
we come to practice in passing the meditation.

291
00:30:37,000 --> 00:30:45,000
Something which helps our own heart, builds patience and forbearance,

292
00:30:45,000 --> 00:30:57,000
removes lust and greed and addiction and hatred, delusion and conceit and so many bad things,

293
00:30:57,000 --> 00:31:03,000
stinginess, jealousy, which exist in our hearts.

294
00:31:03,000 --> 00:31:05,000
But it also helps the world.

295
00:31:05,000 --> 00:31:14,000
It gives us the tools to support other people and be a real guide for other people.

296
00:31:14,000 --> 00:31:21,000
It's something which is done immeasurable benefit to the world in many ways that the world doesn't even understand.

297
00:31:21,000 --> 00:31:32,000
You may never know that the teachings of the Lord Buddha are timeless and they are very present here and now.

298
00:31:32,000 --> 00:31:43,000
It's only a matter of whether people practice them or would rather go out and shout and yell and scream.

299
00:31:43,000 --> 00:32:00,000
And fight and build to protest and all around the world people are looking for solutions to the problems.

300
00:32:00,000 --> 00:32:03,000
For never a problem comes out they look for a solution.

301
00:32:03,000 --> 00:32:06,000
But they never look at the biggest problem.

302
00:32:06,000 --> 00:32:11,000
The problem is inside of our own hearts and it's always been here.

303
00:32:11,000 --> 00:32:17,000
The situation in Burma is not something that came up two weeks ago.

304
00:32:17,000 --> 00:32:23,000
The situation in this world is something which we have all had a part in and we continue to have a part in.

305
00:32:23,000 --> 00:32:29,000
As long as we have greed, anger and delusion in our hearts.

306
00:32:29,000 --> 00:32:40,000
I would like for all of us if we couldn't take this opportunity today to practice especially thinking of suffering of the people.

307
00:32:40,000 --> 00:32:48,000
In the world it can be especially thinking about the people in Burma.

308
00:32:48,000 --> 00:32:58,000
But to take time to take a little bit of time out of our practice to send our love to all beings.

309
00:32:58,000 --> 00:33:02,000
To extend our love immeasurably to all beings.

310
00:33:02,000 --> 00:33:21,000
It can be a support for our own practice and it will be a support for the world and encouragement for the world to practice and develop themselves in the right direction.

311
00:33:21,000 --> 00:33:32,000
This is my talk giving a little bit of understanding and direction for us in regards to love.

312
00:33:32,000 --> 00:33:38,000
That sort of love is a beneficial sort of love is perhaps unbeneficial.

313
00:33:38,000 --> 00:33:54,000
So, it's all for today.

