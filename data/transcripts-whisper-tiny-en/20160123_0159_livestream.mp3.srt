1
00:00:00,000 --> 00:00:23,000
Good evening everyone, broadcasting live, January 22nd, 2016, I was everyone tonight.

2
00:00:23,000 --> 00:00:47,000
Thank you for watching.

3
00:00:47,000 --> 00:00:52,000
Thank you for watching.

4
00:00:52,000 --> 00:01:09,000
And again, for the next question.

5
00:01:09,000 --> 00:01:16,000
Do you think activities as a main person need concentration on your activities and concentrating

6
00:01:16,000 --> 00:01:17,000
on mind?

7
00:01:17,000 --> 00:01:22,000
Don't understand.

8
00:01:22,000 --> 00:01:31,000
Maybe the grammar is just not right, but you have to focus on your daily activities.

9
00:01:31,000 --> 00:01:36,000
And so you can't meditate in South India.

10
00:01:36,000 --> 00:01:42,000
And if you were able to be mindful throughout your daily activities, then you'd be in our

11
00:01:42,000 --> 00:01:43,000
100 pretty much.

12
00:01:43,000 --> 00:01:46,000
And so don't worry about that.

13
00:01:46,000 --> 00:01:49,000
Just be a little bit mindful if you can.

14
00:01:49,000 --> 00:01:58,000
And I'll help you with your daily activities.

15
00:01:58,000 --> 00:02:20,000
Okay, here's a big question.

16
00:02:20,000 --> 00:02:22,000
Seventeen years old.

17
00:02:22,000 --> 00:02:25,000
Should I learn more from the normal videos?

18
00:02:25,000 --> 00:02:36,000
Yeah, I mean the kids videos a lot of adults have talked about how they're useful for adults

19
00:02:36,000 --> 00:02:44,000
as well.

20
00:02:44,000 --> 00:02:52,000
The point of the children's videos was to give a understanding of why we're doing it.

21
00:02:52,000 --> 00:03:01,000
Those kids have a hard time understanding why we would want to focus on ourselves.

22
00:03:01,000 --> 00:03:03,000
So it's a buildup to it.

23
00:03:03,000 --> 00:03:08,000
So I mean that's useful for adults as well to really get a sense that this kind of

24
00:03:08,000 --> 00:03:16,000
meditation, where you actually repeat a word, is it's probably the most ancient and common

25
00:03:16,000 --> 00:03:22,000
sort of meditation technique.

26
00:03:22,000 --> 00:03:27,000
And you can see why I mean if you use those kids videos for kids, a three-year-old, I've

27
00:03:27,000 --> 00:03:31,000
gotten feedback that a three-year-old is able to understand it.

28
00:03:31,000 --> 00:03:34,000
Apparently they can't become enlightened.

29
00:03:34,000 --> 00:03:41,000
You have to be seven years old or older to become enlightened.

30
00:03:41,000 --> 00:03:48,000
But I don't know, that's what the commentary say, I think.

31
00:03:48,000 --> 00:03:53,000
But regardless, there's benefit.

32
00:03:53,000 --> 00:04:01,000
So I mean those videos do work for kids, but you're welcome to watch them.

33
00:04:01,000 --> 00:04:09,000
I wouldn't put too much focus, obviously, on any of the videos for kids except the last one.

34
00:04:09,000 --> 00:04:21,000
But the last one is sort of a hesitantly optimistic.

35
00:04:21,000 --> 00:04:26,000
I'm trying to say, it's what I'd like to present to people.

36
00:04:26,000 --> 00:04:34,000
And I'm hesitant about it because it turns the force sati batani into five,

37
00:04:34,000 --> 00:04:40,000
which obviously, who am I to start having new enumerations?

38
00:04:40,000 --> 00:04:48,000
But practically speaking, having five groups, the body, the feelings, the thoughts,

39
00:04:48,000 --> 00:04:55,000
the emotions and the senses give you five groups, five ways of looking at experience.

40
00:04:55,000 --> 00:04:58,000
That's quite useful, I think.

41
00:04:58,000 --> 00:05:01,000
So lay out in that video it's probably useful.

42
00:05:01,000 --> 00:05:07,000
But you can tell, like my tone is for kids, it's not for a 17-year-old for sure.

43
00:05:07,000 --> 00:05:15,000
So I'd focus more on the booklet and videos.

44
00:05:15,000 --> 00:05:21,000
And if you have time to do a meditation course, you can do one of our online meditation courses

45
00:05:21,000 --> 00:05:23,000
between that.

46
00:05:23,000 --> 00:05:27,000
And we can meet every week, talk about your meditation if you've got questions.

47
00:05:27,000 --> 00:05:37,000
It's pretty simple, it's not like we talk for a long time necessarily, but we need you through the steps.

48
00:05:37,000 --> 00:05:49,000
Our quote today is about alcohol.

49
00:05:49,000 --> 00:05:58,000
And the quality that comes from it.

50
00:05:58,000 --> 00:06:04,000
Whoever follows the dumbness should not drink or encourage others to drink.

51
00:06:04,000 --> 00:06:09,000
Knowing in that intoxication is the result, because of intoxication, the full commits

52
00:06:09,000 --> 00:06:12,000
evil deeds and makes others negligent to me.

53
00:06:12,000 --> 00:06:22,000
So avoid this root of wrong, it's all they like, only by full.

54
00:06:42,000 --> 00:07:01,000
The full commits evil deeds.

55
00:07:01,000 --> 00:07:24,000
That's the thing really is alcohol isn't inherently a problem, being intoxicated isn't inherently a problem.

56
00:07:24,000 --> 00:07:32,000
But it's an interesting substance, substance is an interesting.

57
00:07:32,000 --> 00:07:38,000
I was talking today about psychedelics.

58
00:07:38,000 --> 00:07:48,000
I mean, just taking a substance in and of itself the act of taking a substance is problematic.

59
00:07:48,000 --> 00:07:56,000
But when you take a substance for the purpose of not all substances are the same,

60
00:07:56,000 --> 00:08:01,000
when you take a substance for the purpose of actually removing mindfulness,

61
00:08:01,000 --> 00:08:06,000
removing your ability to be concerned, to be alert,

62
00:08:06,000 --> 00:08:20,000
it destroys your inhibitions, for those inhibitions are the reason.

63
00:08:20,000 --> 00:08:24,000
You don't become less inhibited because you drink alcohol,

64
00:08:24,000 --> 00:08:29,000
except during time when you drink alcohol.

65
00:08:29,000 --> 00:08:41,000
Or maybe you do become less inhibited, the reason we have inhibitions is because we can discern what we're doing is incorrect,

66
00:08:41,000 --> 00:08:45,000
whether or inhibitions are proper or improper.

67
00:08:45,000 --> 00:08:53,000
The variability to have inhibitions is to know right from wrong, to be discerning about what you're doing.

68
00:08:53,000 --> 00:09:04,000
That type of faculty has gone along with many faculties that are going to be willing to drive with God and that kind of thing.

69
00:09:04,000 --> 00:09:11,000
Depth perception, I don't know, all sorts of things, it's poison, alcohol is poison.

70
00:09:11,000 --> 00:09:22,000
We were talking about psychedelics today, and I admitted that psychedelics have the potential to open your mind,

71
00:09:22,000 --> 00:09:25,000
broaden your horizon.

72
00:09:25,000 --> 00:09:33,000
Psychedelics are interesting in that psilocybin anyway, and apparently this guy was arguing that LSD as well,

73
00:09:33,000 --> 00:09:38,000
but psilocybin anyway doesn't do what we think it does.

74
00:09:38,000 --> 00:09:41,000
I mean, there's a study anyway, there was a study,

75
00:09:41,000 --> 00:09:48,000
almost whether it's actually right, but apparently psilocybin doesn't increase brain activity.

76
00:09:48,000 --> 00:09:55,000
What we think of these things is they must induce some sort of hyperactivity in the brain, right?

77
00:09:55,000 --> 00:09:59,000
That's why we hallucinate, and apparently that's not what happens.

78
00:09:59,000 --> 00:10:03,000
They actually inhibit brain activity.

79
00:10:03,000 --> 00:10:22,000
There is a reduction of brain activity under the influence of hallucinogens.

80
00:10:22,000 --> 00:10:27,000
And so I was saying, as interesting as that may be,

81
00:10:27,000 --> 00:10:54,000
it gives the idea that it might come from the fact that the mind is freer to, it's not restrained by the senses.

82
00:10:54,000 --> 00:11:12,000
And so there's hallucinations, but the important point is,

83
00:11:12,000 --> 00:11:19,000
there's a potential to open up your mind to give you a new perspective.

84
00:11:19,000 --> 00:11:24,000
But I said, I mean, a car accident will give you a new perspective.

85
00:11:24,000 --> 00:11:31,000
It's nothing about the actual act of doing the drugs or getting in a car accident.

86
00:11:31,000 --> 00:11:41,000
It's not to say that the car accident is good, but anything that takes you out of your comfort zone being taken out of your comfort zone is a good wake-up call.

87
00:11:41,000 --> 00:11:48,000
It's not to say that that which takes you out of...

88
00:11:48,000 --> 00:12:02,000
So that would take you out of your comfort zone as it is in any way good and beneficial.

89
00:12:02,000 --> 00:12:07,000
We're even getting alcohol poisoning would probably sober you up, right?

90
00:12:07,000 --> 00:12:14,000
Getting put in the hospital for alcohol, please.

91
00:12:14,000 --> 00:12:26,000
But what was more interesting to me is the idea of the fact of just the fact of taking substances is problematic.

92
00:12:26,000 --> 00:12:37,000
I mean, you can see it with alcohol, your intention is to do something silly, to lose whatever mindfulness you might naturally have.

93
00:12:37,000 --> 00:12:45,000
But just the intention, like for in the case of psilocybin or in the case of a hallucinogen,

94
00:12:45,000 --> 00:12:48,000
your whole reason for taking why are you taking this chemical?

95
00:12:48,000 --> 00:12:51,000
I mean, if you're accident, you're just at the nurse's nose.

96
00:12:51,000 --> 00:12:57,000
There's nothing to it, but when you intentionally take it,

97
00:12:57,000 --> 00:13:03,000
your understanding that leads you to do it is a sort of a karma for sure.

98
00:13:03,000 --> 00:13:08,000
I mean, it's a habit, what that means. It's going to influence your outlook.

99
00:13:08,000 --> 00:13:16,000
It's a part of your outlook on life. It's taking drugs, taking substances.

100
00:13:16,000 --> 00:13:23,000
And so if you repeatedly take hallucinogens, or if you clearly do alcohol,

101
00:13:23,000 --> 00:13:29,000
it's going to affect who you are. I mean, putting aside the effects of the actual drugs,

102
00:13:29,000 --> 00:13:34,000
it's actually another interesting point because you can argue that hallucinogens

103
00:13:34,000 --> 00:13:39,000
are even alcohol are good for you.

104
00:13:39,000 --> 00:13:45,000
But you know, that's anecdotal evidence.

105
00:13:45,000 --> 00:13:49,000
This is a clear case of where anecdotal evidence falls very, very short,

106
00:13:49,000 --> 00:13:53,000
because you'd have to look scientifically. I mean, in the case of meditation,

107
00:13:53,000 --> 00:13:56,000
we all say meditations helpful for you, but they say, well, that's anecdotal.

108
00:13:56,000 --> 00:14:00,000
Except it turns out to be true that when you do study it,

109
00:14:00,000 --> 00:14:04,000
all indicators show that meditation is beneficial.

110
00:14:04,000 --> 00:14:06,000
But that's fairly obvious.

111
00:14:06,000 --> 00:14:09,000
The person takes alcohol and thinks, well, it's good for me,

112
00:14:09,000 --> 00:14:12,000
and it helps me loosen up.

113
00:14:12,000 --> 00:14:19,000
I think what a smart intellectual people would not take that very seriously,

114
00:14:19,000 --> 00:14:31,000
because if you think of when people get accustomed to drinking alcohol,

115
00:14:31,000 --> 00:14:35,000
that's not say alcoholics, but even just accustomed to drinking alcohol.

116
00:14:35,000 --> 00:14:38,000
So it doesn't make them better people.

117
00:14:38,000 --> 00:14:41,000
It doesn't make them happier people.

118
00:14:41,000 --> 00:14:44,000
I suppose many people do believe that.

119
00:14:44,000 --> 00:14:47,000
If you don't take alcohol, you're just going to...

120
00:14:47,000 --> 00:14:50,000
Well, the thing is you're going to have to deal with your problems, right?

121
00:14:50,000 --> 00:14:53,000
That's the whole thing.

122
00:14:53,000 --> 00:14:56,000
But I mean, even hallucinogens,

123
00:14:56,000 --> 00:15:00,000
to say that if you were to continue to do hallucinogens,

124
00:15:00,000 --> 00:15:08,000
that would somehow be a beneficial regimen of spiritual activity,

125
00:15:08,000 --> 00:15:13,000
I think 90% sure that's that.

126
00:15:13,000 --> 00:15:18,000
I mean, 90% sure, anyway, that there are side effects to it,

127
00:15:18,000 --> 00:15:24,000
apart from just the intention to take it, which, as I say, is I think the key to it.

128
00:15:24,000 --> 00:15:28,000
If your spirituality involves taking chemicals,

129
00:15:28,000 --> 00:15:31,000
it's a misunderstanding of how the mind works,

130
00:15:31,000 --> 00:15:36,000
because it's like these ideas of enlightening other beings,

131
00:15:36,000 --> 00:15:40,000
like transmitting the dumbness so that other people become enlightened.

132
00:15:40,000 --> 00:15:45,000
My end of Buddhism has this idea that you can enlighten others.

133
00:15:45,000 --> 00:15:51,000
A Buddha can enlighten you just by force kind of.

134
00:15:51,000 --> 00:15:54,000
Well, maybe they don't.

135
00:15:54,000 --> 00:15:57,000
But I think some schools do.

136
00:15:57,000 --> 00:15:59,000
At any rate, I suppose there is this idea.

137
00:15:59,000 --> 00:16:01,000
And I think a lot of people do have this idea

138
00:16:01,000 --> 00:16:05,000
that there are ways by which you can become enlightened.

139
00:16:05,000 --> 00:16:08,000
You can take a shortcut.

140
00:16:08,000 --> 00:16:11,000
And so this person I was talking with,

141
00:16:11,000 --> 00:16:14,000
he was always, and I'm all about shortcuts.

142
00:16:14,000 --> 00:16:17,000
And so what I was trying to argue was,

143
00:16:17,000 --> 00:16:22,000
the whole act of taking shortcuts is a problem.

144
00:16:22,000 --> 00:16:24,000
Not dealing with your issues.

145
00:16:24,000 --> 00:16:30,000
It's not natural, it's not real.

146
00:16:30,000 --> 00:16:35,000
And I think that's an interesting key point that it can never be possible

147
00:16:35,000 --> 00:16:39,000
by very nature taking a shortcut can be possible.

148
00:16:39,000 --> 00:16:42,000
The mind isn't the body.

149
00:16:42,000 --> 00:16:48,000
So the idea that somehow affecting the body can fix the mind

150
00:16:48,000 --> 00:16:51,000
is I think intrinsically.

151
00:16:51,000 --> 00:16:55,000
I don't mean to say that it's just hard or we can't do it.

152
00:16:55,000 --> 00:16:57,000
It's intrinsically impossible.

153
00:16:57,000 --> 00:17:05,000
But it's very nature trying to change the brain fails.

154
00:17:05,000 --> 00:17:09,000
I mean, I guess what I would say more is,

155
00:17:09,000 --> 00:17:13,000
the individual's act of trying to change their own brain

156
00:17:13,000 --> 00:17:16,000
to fix the problem by changing their own brain

157
00:17:16,000 --> 00:17:17,000
is going to affect them.

158
00:17:17,000 --> 00:17:22,000
It's going to be a remarkably problematic idea.

159
00:17:22,000 --> 00:17:27,000
So interesting ideas around alcohol is pretty obvious to a meditator.

160
00:17:27,000 --> 00:17:30,000
You can't be mindful, you can't progress in meditation

161
00:17:30,000 --> 00:17:33,000
if you're engaging in drinking alcohol.

162
00:17:33,000 --> 00:17:35,000
It's interesting teaching at the university

163
00:17:35,000 --> 00:17:37,000
because I'm assuming many of these people drink alcohol.

164
00:17:37,000 --> 00:17:42,000
I don't know what percent, but I've got a lot of karma to pay back

165
00:17:42,000 --> 00:17:47,000
when I was in residence in my first year of university.

166
00:17:47,000 --> 00:17:50,000
They were handing out these pamphlets

167
00:17:50,000 --> 00:17:54,000
that said, if your friends can't have fun without drinking,

168
00:17:54,000 --> 00:17:56,000
then maybe you need new friends.

169
00:17:56,000 --> 00:17:58,000
And so I cut out the word without it

170
00:17:58,000 --> 00:18:02,000
and put it up on our door.

171
00:18:02,000 --> 00:18:05,000
So it said if your friends can't have fun drinking,

172
00:18:05,000 --> 00:18:09,000
maybe you need new friends.

173
00:18:09,000 --> 00:18:12,000
And I got my roommate.

174
00:18:12,000 --> 00:18:14,000
He was a nice guy from...

175
00:18:14,000 --> 00:18:15,000
He was a nice guy.

176
00:18:15,000 --> 00:18:16,000
I haven't seen him long time.

177
00:18:16,000 --> 00:18:20,000
But from Sue St. Marie, Northern Ontario, where I'm the same,

178
00:18:20,000 --> 00:18:29,000
same latitude as me, North Northern Ontario.

179
00:18:29,000 --> 00:18:33,000
And I got him to start drinking alcohol.

180
00:18:33,000 --> 00:18:36,000
Yeah.

181
00:18:36,000 --> 00:18:52,000
Wasn't a great time.

182
00:18:52,000 --> 00:18:55,000
Anyway, so tomorrow 3 p.m. eastern time,

183
00:18:55,000 --> 00:18:58,000
to a 12 p.m. second lifetime.

184
00:18:58,000 --> 00:19:03,000
I'll be on the Buddha Center.

185
00:19:03,000 --> 00:19:06,000
I guess we'll meet in the main hall.

186
00:19:06,000 --> 00:19:08,000
That's the easiest place to find me.

187
00:19:08,000 --> 00:19:13,000
There's a nice place in the forest that they have.

188
00:19:13,000 --> 00:19:18,000
It's probably a little harder for people to find.

189
00:19:18,000 --> 00:19:22,000
Maybe once we get settled, we'll move to the Deer Park that they have.

190
00:19:22,000 --> 00:19:25,000
I haven't seen the Deer Park's like right now,

191
00:19:25,000 --> 00:19:30,000
but I'm assuming there's still a place there.

192
00:19:30,000 --> 00:19:35,000
I was flying around the Buddha Center this afternoon.

193
00:19:35,000 --> 00:19:40,000
And secondly, if you have a tire's connection, I'll fly.

194
00:19:40,000 --> 00:19:45,000
I'm just trying to remember how the whole system works.

195
00:19:45,000 --> 00:19:51,000
I want to move my character, how to move the camera.

196
00:19:51,000 --> 00:19:56,000
I spent five, ten minutes wandering around.

197
00:19:56,000 --> 00:20:01,000
So yeah, I'll see you guys tomorrow.

198
00:20:01,000 --> 00:20:02,000
Audio's a problem.

199
00:20:02,000 --> 00:20:05,000
Second life isn't flawless or it wasn't flawless before,

200
00:20:05,000 --> 00:20:07,000
but it's often because of people's hardware.

201
00:20:07,000 --> 00:20:10,000
It's just difficult to get everything working.

202
00:20:10,000 --> 00:20:14,000
Some people may not hear, but you're not expected to use audio.

203
00:20:14,000 --> 00:20:19,000
You just need good speakers or a headphone and you have to turn your mic off.

204
00:20:19,000 --> 00:20:22,000
That's the big problem.

205
00:20:22,000 --> 00:20:29,000
You can mute them or ban them, I don't know what they do.

206
00:20:29,000 --> 00:20:33,000
Yeah, SLT doesn't mean Sri Lankan time.

207
00:20:33,000 --> 00:20:36,000
SLT means second life time.

208
00:20:36,000 --> 00:20:42,000
Actually, it doesn't mean Sri Lankan time as well, but not for us.

209
00:20:42,000 --> 00:20:48,000
So anyway, guess that's all for tonight.

210
00:20:48,000 --> 00:20:54,000
See you all tomorrow afternoon or any of you who are able to get on second life.

211
00:20:54,000 --> 00:21:19,000
Have a good night everybody.

