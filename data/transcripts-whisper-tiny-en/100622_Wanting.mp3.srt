1
00:00:00,000 --> 00:00:21,000
When we look at the four noble truths, the core of the Buddha's teaching, we look at the

2
00:00:21,000 --> 00:00:27,040
second noble truth and we see that the cause of suffering, what we're trying to eliminate

3
00:00:27,040 --> 00:00:34,040
is craving or wanting.

4
00:00:34,040 --> 00:00:43,640
And so this question comes up time and again as to how we can expect to make any progress

5
00:00:43,640 --> 00:00:56,480
in meditation when it requires some form of wanting just to practice meditation.

6
00:00:56,480 --> 00:01:07,440
And so we get into this tricky situation of wanting not wanting to be free from wanting.

7
00:01:07,440 --> 00:01:13,040
And it really is an important dilemma because as we often see in much of our practice

8
00:01:13,040 --> 00:01:20,400
we do want and expect a great many things.

9
00:01:20,400 --> 00:01:28,920
And by building up all of these expectations we often find ourselves putting a stumbling

10
00:01:28,920 --> 00:01:33,760
block in front of us.

11
00:01:33,760 --> 00:01:46,160
It gets very, very difficult to progress because we're so dead set on some idea that

12
00:01:46,160 --> 00:01:55,080
we've created in our mind of enlightenment or nibana or freedom from suffering, freedom from

13
00:01:55,080 --> 00:01:56,080
craving.

14
00:01:56,080 --> 00:02:03,920
You know it's a very hard pill to swallow, it's the idea that we should somehow not want

15
00:02:03,920 --> 00:02:06,520
anything.

16
00:02:06,520 --> 00:02:12,800
And so we often swallow this pill thinking it's got to be good for us.

17
00:02:12,800 --> 00:02:20,600
And yet knowing deep down inside that we bitterly resent this medicine, we don't even

18
00:02:20,600 --> 00:02:28,960
want the cure, we couldn't imagine a life without wanting.

19
00:02:28,960 --> 00:02:36,000
And yet we force it on ourselves and we create this idea of how we think it must somehow

20
00:02:36,000 --> 00:02:42,600
be wonderful to be free from clinging, free from craving.

21
00:02:42,600 --> 00:02:49,520
And it falls so far short of the reality of freedom from suffering that it becomes sort

22
00:02:49,520 --> 00:02:59,680
of a means of tricking oneself or deluding oneself.

23
00:02:59,680 --> 00:03:09,160
Sort of akin to in other religions how you, in many religions we push ourselves to

24
00:03:09,160 --> 00:03:15,160
believe some people they push themselves to believe something, it's the same sort of faith

25
00:03:15,160 --> 00:03:23,040
based practice where you're practicing out of this belief that it's somehow going to

26
00:03:23,040 --> 00:03:26,880
be like this or this or this.

27
00:03:26,880 --> 00:03:28,680
And we as a result we miss the point.

28
00:03:28,680 --> 00:03:34,840
And this is well discussed in the Buddhist texts, as I mentioned before, the first thing

29
00:03:34,840 --> 00:03:41,960
the Buddha said is that well you do need some sort of intention to practice and just

30
00:03:41,960 --> 00:03:48,080
as you need an intention to come here to listen to me teach, you need an intention just

31
00:03:48,080 --> 00:03:57,320
to get to our center and then it takes intention to lift yourself up to practice.

32
00:03:57,320 --> 00:04:01,560
But on the other hand in many cases the Buddha has explained that it's easy to overreach

33
00:04:01,560 --> 00:04:11,080
the goal by setting the goal up and working towards something that's totally impossible,

34
00:04:11,080 --> 00:04:20,920
expecting ourselves to be perfectly mindful and not as a result, not practicing, not appreciating

35
00:04:20,920 --> 00:04:27,240
the small steps we're making, not being able to see them because they're so small.

36
00:04:27,240 --> 00:04:35,880
We're expecting big steps and big insights and big realizations.

37
00:04:35,880 --> 00:04:41,800
And so we fail to see the benefits that are coming step by step.

38
00:04:41,800 --> 00:04:47,080
And if we were to think about it correctly we could see that over a course of many, many

39
00:04:47,080 --> 00:04:59,800
days small steps are actually quite productive.

40
00:04:59,800 --> 00:05:05,880
And so I just wanted to explain a little bit about from my understanding what it is that

41
00:05:05,880 --> 00:05:10,720
we should be wanting and how we should relate to want in regards to our Buddhist practice,

42
00:05:10,720 --> 00:05:15,240
how we should understand the practice in regards to wanting and in regards to overcoming

43
00:05:15,240 --> 00:05:24,240
hindrances and the hindrance of expectation, for example.

44
00:05:24,240 --> 00:05:31,240
There's a story in the Topitika of this angel that comes to see the Buddha and asks him

45
00:05:31,240 --> 00:05:37,480
where one can find the beginning, where one can find the end of the universe or the end

46
00:05:37,480 --> 00:05:45,760
of the world.

47
00:05:45,760 --> 00:05:56,600
And the story goes that he had in a previous life been an ascetic and he had practiced

48
00:05:56,600 --> 00:06:07,120
tranquility meditation, concentration based, meditation, focusing on a concept or whatever.

49
00:06:07,120 --> 00:06:16,200
And he was able to, as a result, leave his body and through astral travel, he was able

50
00:06:16,200 --> 00:06:27,680
to fly at incredible speeds and say he could cross the ocean in one step.

51
00:06:27,680 --> 00:06:33,400
And so he flew through the universe trying to find the end.

52
00:06:33,400 --> 00:06:43,120
He flew with his mind and he flew for a hundred years till the span of his life was ended

53
00:06:43,120 --> 00:06:56,240
after a hundred years and he died not getting to the end of the universe.

54
00:06:56,240 --> 00:07:04,120
Just a result of his good practices, he was born as an angel and he came to see the Buddha

55
00:07:04,120 --> 00:07:08,560
and asked him, he said, you know, this is the case.

56
00:07:08,560 --> 00:07:22,360
Where can one find the end of the universe?

57
00:07:22,360 --> 00:07:23,360
It's interesting.

58
00:07:23,360 --> 00:07:30,120
There's a very clear parallel between this sort of idea and modern physics and I've

59
00:07:30,120 --> 00:07:37,080
really started looking at this in a sort of a broad way, the goals of physics, the goals

60
00:07:37,080 --> 00:07:43,200
of intellectual investigation in general, which are often derided in Buddhism but it's

61
00:07:43,200 --> 00:07:51,360
never really hit me quite as strong as it has in the past couple of days just thinking

62
00:07:51,360 --> 00:08:07,760
about it that there's really no intrinsic benefit to be gained from the vast majority

63
00:08:07,760 --> 00:08:14,200
of what we do as human beings and just thinking back to the times of living in caves

64
00:08:14,200 --> 00:08:23,080
and being hunters and gathering gatherers and whatever foragers looking at the animals

65
00:08:23,080 --> 00:08:33,200
in ancient times before the humans came around and looking at the Buddhist texts and

66
00:08:33,200 --> 00:08:42,680
how they describe ancient times as being much less hostile, of course whether this is true

67
00:08:42,680 --> 00:08:48,720
or not, there's a perspective but according to the Buddhist texts in ancient times

68
00:08:48,720 --> 00:09:03,200
it would have been far less hostile in nature, far less survival of the fittest or in some

69
00:09:03,200 --> 00:09:08,720
ways more of a peaceful place or in many places may be peaceful, maybe just in certain

70
00:09:08,720 --> 00:09:22,840
places, human beings were maybe a lot more peaceful and we see we look at scientific advancement

71
00:09:22,840 --> 00:09:28,720
in terms of finding the beginning of the universe, finding the end of the universe, understanding

72
00:09:28,720 --> 00:09:38,160
the totality of the universe, finding the smallest particles, coming to understand all

73
00:09:38,160 --> 00:09:48,600
of these things and it just, it seems in an incredible waste of time and effort that

74
00:09:48,600 --> 00:09:58,920
we're spending on these activities where we could be doing things like meditating as

75
00:09:58,920 --> 00:09:59,920
an example.

76
00:09:59,920 --> 00:10:06,240
It's something that comes to one who meditates particularly that the billions and billions

77
00:10:06,240 --> 00:10:14,280
of dollars and time and effort, really the whole goal of the human race seems to be to

78
00:10:14,280 --> 00:10:24,800
advance in ways that are pointless, to have this great technology we've become so enamored

79
00:10:24,800 --> 00:10:30,840
with and just the examination of whether or not this is indeed progress, to have iPhones

80
00:10:30,840 --> 00:10:45,480
and computers, to have wireless internet, we talk about how much progress it is and

81
00:10:45,480 --> 00:10:51,840
we don't look at the ramifications and the problems that it brings.

82
00:10:51,840 --> 00:11:02,080
I was remembering the words of Mahasizayanda in one of his books that I had read that he

83
00:11:02,080 --> 00:11:10,960
said, if you've never seen something you can't be attracted to it and at the time that

84
00:11:10,960 --> 00:11:16,560
it seemed somehow wise but I didn't quite understand it or it didn't understand the truth

85
00:11:16,560 --> 00:11:25,280
of it, it seemed a bit strange or perhaps easy to understand but not really meaningful

86
00:11:25,280 --> 00:11:29,920
but he spent some time dwelling on this and explaining how if you've never seen something

87
00:11:29,920 --> 00:11:34,480
you can never become attracted to it and I thought, well, maybe you can create it in

88
00:11:34,480 --> 00:11:42,600
your mind or so on but what you can see through meditation is actually the mind is really

89
00:11:42,600 --> 00:11:48,680
only attracted to the things that it's seen before, the things that it remembers as pleasing

90
00:11:48,680 --> 00:11:57,720
as bringing happiness and it struck me, it finally occurred to me how meaningful and

91
00:11:57,720 --> 00:12:08,240
how important this statement is and how it really gives so much importance to the reclusive

92
00:12:08,240 --> 00:12:19,200
life, the meditative life and how it points to such a great danger with material progress

93
00:12:19,200 --> 00:12:25,200
because if we didn't have all and the obvious point is if we didn't have all of these things

94
00:12:25,200 --> 00:12:37,920
we wouldn't want for them, we wouldn't crave for them and so what happens is we

95
00:12:37,920 --> 00:12:42,960
look for new things, we look for something new, not really knowing what it is and when we

96
00:12:42,960 --> 00:12:49,880
sit down and meditate we don't crave for it but when we make it suddenly it's this incredible

97
00:12:49,880 --> 00:12:56,560
new thing and when someone sees it or even just hears about it and creates the mental

98
00:12:56,560 --> 00:13:04,480
image in their mind they want it but once they see it or hear it or smell or taste that

99
00:13:04,480 --> 00:13:15,320
this new thing is more advanced, more intense and more immediate pleasure, it takes

100
00:13:15,320 --> 00:13:20,520
hold of their mind, it becomes something that they must have.

101
00:13:20,520 --> 00:13:25,720
The story Mahasizaya gave is something a little bit different, he was explaining about this

102
00:13:25,720 --> 00:13:32,040
man who refused to get married, he had been reborn from the Brahma world where of course

103
00:13:32,040 --> 00:13:36,200
there's no sensuality and so when he was born he didn't want to get married and his

104
00:13:36,200 --> 00:13:41,400
parents were quite upset by this and so they pushed and pushed and pushed him until finally

105
00:13:41,400 --> 00:13:47,400
he went to a goldsmith, he was quite rich and he went to a goldsmith and had him fashion

106
00:13:47,400 --> 00:13:57,080
a beautiful statue of a woman under his direction, fashioned this out of gold or I'll see

107
00:13:57,080 --> 00:14:03,400
fashion did himself I can't remember but it was the model of the perfect woman and he

108
00:14:03,400 --> 00:14:08,840
said to his parents if you can find a woman who is as beautiful as this then I will

109
00:14:08,840 --> 00:14:17,800
marry her and so they sent it around to all the villages and all the towns in the country

110
00:14:17,800 --> 00:14:21,840
and eventually they found some woman, they went to one village and the people said why

111
00:14:21,840 --> 00:14:27,640
is that statue of, why are you, why are you dragging around that statue of our headmen's

112
00:14:27,640 --> 00:14:33,280
daughter and they said you have a daughter who looks like you have a girl who looks like

113
00:14:33,280 --> 00:14:40,640
this and so they brought this people to see this woman in terms of that she did looking

114
00:14:40,640 --> 00:14:45,800
exactly a very close to the statue and so they went back and told this man and suddenly

115
00:14:45,800 --> 00:14:54,080
for the first time it just came up from inside of him, suddenly he had this romantic

116
00:14:54,080 --> 00:15:01,080
attraction towards this woman and so they called her to live with him but on the way

117
00:15:01,080 --> 00:15:10,640
she died it's an interesting story, she was so fragile, some very beautiful people can

118
00:15:10,640 --> 00:15:16,920
often or attractive people can be often quite fragile especially in India they may have

119
00:15:16,920 --> 00:15:23,960
been had interesting ideas at the time of beauty, nowadays you see these people who become

120
00:15:23,960 --> 00:15:31,320
anorexic just to be beautiful according to standards of beauty and so on the way to meet

121
00:15:31,320 --> 00:15:41,040
him she died of fatigue or of the just the wear and tear of the travel and so he became

122
00:15:41,040 --> 00:15:49,600
totally distraught and sane with this desire to have this woman that he could no longer

123
00:15:49,600 --> 00:15:55,320
have and it was something that someone that he had never even seen so this is where

124
00:15:55,320 --> 00:15:59,240
Mahasi said the truth is you can never become really attracted to something that you've

125
00:15:59,240 --> 00:16:06,880
never seen and so that is an interesting thing that his attraction was perhaps to the

126
00:16:06,880 --> 00:16:12,960
golden statue in one sense but on the other hand it was an attraction to an idea when

127
00:16:12,960 --> 00:16:20,560
we create these ideas in our mind and so he had this idea in his mind of this woman

128
00:16:20,560 --> 00:16:25,120
but anyway talking here specifically about getting quite far off track but talking specifically

129
00:16:25,120 --> 00:16:32,560
about our attempts to find the limits of the universe to find the entirety of the

130
00:16:32,560 --> 00:16:45,560
universe you know the final unified theory and so on is leading to an incredible amount

131
00:16:45,560 --> 00:16:52,000
of addiction and attraction and aversion and repulsion and it's leading to wars and

132
00:16:52,000 --> 00:17:01,200
it's leading to just the intensity of society our need for more and more of this technology

133
00:17:01,200 --> 00:17:08,560
for more and more knowledge for more and more worldly advancement and so some of it's

134
00:17:08,560 --> 00:17:17,640
just for pure sensuality but a lot of it's in the name of scientific advancement and really

135
00:17:17,640 --> 00:17:23,200
so what if we get the unified theory then we'll be able to make better bombs or whatever

136
00:17:23,200 --> 00:17:31,360
who knows maybe we'll be even able to go to distant stars and other planets to start

137
00:17:31,360 --> 00:17:37,800
new colonies but you know it's it's kind of in one sense you think boy I hate to be the

138
00:17:37,800 --> 00:17:45,680
poor planet that God has that had to be invaded by people like us talk about space and

139
00:17:45,680 --> 00:17:52,720
invaders because we haven't done the important work of course and so anyway the Buddha

140
00:17:52,720 --> 00:18:01,760
to this angel who came to him and said where is the end of the universe the Buddha said

141
00:18:01,760 --> 00:18:05,880
you can't find the end of the world the end of the universe there in that way he said

142
00:18:05,880 --> 00:18:10,840
I've tried it before I tried that before myself flying away to the end of the universe

143
00:18:10,840 --> 00:18:15,480
and died before the end of the universe came before it got to the end of the universe he said

144
00:18:15,480 --> 00:18:20,880
it's impossible you can't find the end of the universe in that way and what he said

145
00:18:20,880 --> 00:18:27,000
and this is very important for dealing with here he said the end of the universe is within

146
00:18:27,000 --> 00:18:37,960
this six foot frame within this body he must aming of that power within this being that

147
00:18:37,960 --> 00:18:48,520
is six feet tall and two feet wide and one foot thick this is this is where you find the

148
00:18:48,520 --> 00:18:54,680
beginning of the universe the beginning of the world the end of the world you find the

149
00:18:54,680 --> 00:19:02,440
cause for the world you find the path that leads to the end of the universe and so why

150
00:19:02,440 --> 00:19:08,520
I think this is important to the idea of wanting to the idea of expectation of striving

151
00:19:08,520 --> 00:19:15,360
in practice is that I want to make the point that what we're striving for is to not

152
00:19:15,360 --> 00:19:22,920
strive what we're reaching for is to not reach so our practice should have a quality

153
00:19:22,920 --> 00:19:28,800
of just practicing we're not practicing for anything we're practicing it's the meditation

154
00:19:28,800 --> 00:19:36,920
itself that is the point the meditation is the is the path and what's it a path do it's

155
00:19:36,920 --> 00:19:42,640
a path to to the end of the to the goal which is also here within ourselves it's leading

156
00:19:42,640 --> 00:19:55,520
us here this is perhaps the most important point is that what we want is to be to be here

157
00:19:55,520 --> 00:20:01,480
and to be now to be in the present so when we're sitting to to actually know that we're

158
00:20:01,480 --> 00:20:09,600
sitting to have our mind with the sitting to walk to know that we walk to be free from

159
00:20:09,600 --> 00:20:16,000
the wanting to be free from the mind that takes us into the past and takes us into the

160
00:20:16,000 --> 00:20:25,280
future so that when something good disappears we yearn for it in this case so that we don't

161
00:20:25,280 --> 00:20:32,640
stop yearning for it we can do away with the yearning so that when we're having to undertake

162
00:20:32,640 --> 00:20:42,720
something trying difficult we're able to do away with the the idea of when it's going

163
00:20:42,720 --> 00:20:49,600
to be over when it's going to be done our existence becomes infinite becomes if you will

164
00:20:49,600 --> 00:21:01,400
permanent our our being the our life becomes permanent you can say because we've done

165
00:21:01,400 --> 00:21:10,240
away with the future we've done away with the paths we come back to the present moment

166
00:21:10,240 --> 00:21:16,480
what we're we're reaching for is it's inside of ourselves what we're aiming for is right

167
00:21:16,480 --> 00:21:21,800
here and right now so the meditation is not to attain anything it's to let go of everything

168
00:21:21,800 --> 00:21:34,520
if you want to if you want to be happy there's a there's a there's this poem that comes

169
00:21:34,520 --> 00:21:41,880
from the jatakas that the jatakas the past life stories of the Buddha were all translated

170
00:21:41,880 --> 00:21:48,120
into English about over a hundred years ago and at the time it was it was sort of

171
00:21:48,120 --> 00:21:54,840
in vogue to to translate and rearrange the words to make poetry so there's these very poetic

172
00:21:54,840 --> 00:22:02,760
verses from the poly verses and this one verse goes for every desire that is let go a happiness

173
00:22:02,760 --> 00:22:10,520
is one he who would all happiness have must with all last be done and so you can say that

174
00:22:10,520 --> 00:22:20,760
say Buddhist idiom or Buddhist motto Buddhist verse that every everything we let go of

175
00:22:20,760 --> 00:22:25,800
every everything we are able to be free from when we're really and truly free from it

176
00:22:25,800 --> 00:22:36,200
we're really and truly let go we we we become happy to become at peace

177
00:22:36,200 --> 00:22:44,440
and so our aim is is is here is now is the default is to get back to a default state

178
00:22:44,440 --> 00:22:48,920
the state that we thought was the default state you know the way we were born and raised

179
00:22:48,920 --> 00:22:53,920
the old way before we started meditation meditation we thought well that was the default

180
00:22:53,920 --> 00:22:57,880
and now we're doing something special actually it's it's very special what we're doing

181
00:22:57,880 --> 00:23:01,480
but it's special because it's bringing us back to normal

182
00:23:01,480 --> 00:23:07,320
it's bringing us back to who we think we are we think we're ordinary people until we come

183
00:23:07,320 --> 00:23:15,560
to meditate and then we realize how crazy we are how messed up we are it's it can be discouraging

184
00:23:15,560 --> 00:23:20,680
sometimes meditators often feel a lot of self-loathing because they start to see that they think

185
00:23:20,680 --> 00:23:27,480
they must be a terrible person terrible meditator or terrible person usually that's something

186
00:23:27,480 --> 00:23:32,280
we've carried around with us is this self-loathing but in meditation it can come out quite strong

187
00:23:34,440 --> 00:23:40,280
until we start to realize that that this is the the ordinary state of human beings it's not

188
00:23:40,280 --> 00:23:49,640
very not very normal let me see that what we thought was was who we are was actually this

189
00:23:49,640 --> 00:24:03,560
this built up formed created state constructed it's a it's a artificial construct

190
00:24:04,520 --> 00:24:10,600
are being our entity who we are and all of the many attributes we give to our self is

191
00:24:10,600 --> 00:24:19,480
totally artificial and in many cases chaotic and unsustainable in the cause of a great amount

192
00:24:19,480 --> 00:24:26,600
of suffering for us so this is why when we walk we seem to do these stupid things like

193
00:24:26,600 --> 00:24:32,680
just saying walking walking or stepping right stepping up I mean it seems like such a silly thing to

194
00:24:32,680 --> 00:24:37,400
do and it doesn't seem like it's going to get anywhere and that's really the point is to stop us

195
00:24:37,400 --> 00:24:45,800
from from going places to retrain our minds to be the way we we think we already are we think we are

196
00:24:45,800 --> 00:24:51,240
here we think you know we say yesterday I was there today I was here this morning I was this and

197
00:24:51,240 --> 00:25:00,360
that but as we all know our minds are are certainly never certainly hardly ever there our minds

198
00:25:00,360 --> 00:25:09,080
are all over the place in most cases we're trying to show this to ourselves we're trying to see

199
00:25:09,080 --> 00:25:16,920
what what what a mess we're making by running all over the place we're trying to see the

200
00:25:18,440 --> 00:25:26,040
the dangers in this or the disadvantages the problem with the problems that are inherent in

201
00:25:28,280 --> 00:25:35,640
not being present there was once a king that came to see the Buddha and he has

202
00:25:35,640 --> 00:25:41,960
he has them why is it that these monks he said he can't believe it he it's like four meals a day

203
00:25:43,240 --> 00:25:47,400
and he can't believe these monks who he'd only one meal a day why they're so radiant

204
00:25:47,400 --> 00:25:55,080
why they are able to keep their color and and their vigor and their health why they're even

205
00:25:55,080 --> 00:26:05,160
seem radiant more alive than than people who eat all the food they want and enjoy all the

206
00:26:05,160 --> 00:26:11,400
essential pleasures they want and so on and then going to give another verse when I don't think

207
00:26:11,400 --> 00:26:21,320
I can remember it oh yeah for the past I do not for the past they do not mourn or for the future

208
00:26:21,320 --> 00:26:37,160
weep they take the present as it comes and thus their color keep and then something about

209
00:26:37,160 --> 00:26:44,760
lusting after the future or some less worrying after the past or someone few some uncertain future

210
00:26:44,760 --> 00:26:54,040
future deed dries again dries the young men's vigor up just as when you cut a fresh green

211
00:26:54,040 --> 00:27:00,360
read and this is a paraphrase but it it sums up a very important passage of the Lord Buddha

212
00:27:00,360 --> 00:27:06,600
the Buddha's teaching and the passage is when you when you cut just as when you cut grass

213
00:27:06,600 --> 00:27:15,880
it becomes uprooted from the from the earth and dries up in the same way when a person is lost in

214
00:27:15,880 --> 00:27:24,200
the past and lost in the future is always looking at the time and meditation and always thinking

215
00:27:24,200 --> 00:27:29,320
about what's going to come next and what we're going to do after this or thinking about the past

216
00:27:29,320 --> 00:27:39,000
what went before what's finished already it dries you up why because you're uprooted from reality

217
00:27:41,000 --> 00:27:45,560
and this is another important point is that the only thing the only reality that is truly real

218
00:27:45,560 --> 00:27:51,080
the only thing that is truly real in this in this universe is is the present moment here and now

219
00:27:51,080 --> 00:28:00,280
nothing else in this universe is real nothing else in your existence is real but the present

220
00:28:00,280 --> 00:28:06,520
moment the past is gone in fact the past you could say it never really existed it was it's just

221
00:28:06,520 --> 00:28:11,960
the present moment the changing of the present moment and the effect that it has on our minds and

222
00:28:11,960 --> 00:28:21,560
our bodies and the echoes that come as a result of it it's like when you when you shout in the canyon

223
00:28:21,560 --> 00:28:29,160
and then you hear the echo and you ask yourself is that echo in the past you know because I

224
00:28:29,160 --> 00:28:33,160
shouted it's in the past already and you say that's that's the that's my voice in the past

225
00:28:34,520 --> 00:28:39,240
or when I record my voice here and and put it up on the internet and someone listens to it it's

226
00:28:39,240 --> 00:28:45,160
it's like they're listening to the past but we know this isn't true that it only has an effect

227
00:28:45,880 --> 00:28:51,560
it changes everything we do changes the present moment so instead of yesterday I gave this talk

228
00:28:51,560 --> 00:28:58,680
and today it's on the internet it's the present moment is changing so that my voice becomes

229
00:29:00,520 --> 00:29:05,880
a reality in some somebody's computer and the sound coming out of some speakers

230
00:29:05,880 --> 00:29:11,720
and the future of course is even more ephemeral we think about

231
00:29:13,560 --> 00:29:18,760
or even more insubstantial we think about the future as being like this or that and then the

232
00:29:18,760 --> 00:29:24,280
future there's this meeting or the future there's this problem or that problem and it becomes

233
00:29:24,280 --> 00:29:31,560
a it's whole entity suddenly we give it a cell we give it a persona and it's like this big

234
00:29:31,560 --> 00:29:40,840
blob that's hanging over us and yet all that is really is our own thoughts and and we can see

235
00:29:40,840 --> 00:29:46,120
this we can catch yourself in this when we have our we catch yourself saying I'll say

236
00:29:46,120 --> 00:29:50,280
they'll say this I'll say this I'll say this I'll say this and I'll do this and that

237
00:29:50,280 --> 00:29:53,720
and the other thing when I get back then I'm going to this and this and this

238
00:29:55,720 --> 00:30:00,280
and nine times out of ten when we get back it's totally different from what we thought and we

239
00:30:00,280 --> 00:30:04,360
never have a chance to say the things we're going to say we say them and then it doesn't have

240
00:30:04,360 --> 00:30:10,840
the intended response because it's totally artificial we want to do all these things and so we're

241
00:30:10,840 --> 00:30:16,840
not ready for the real trials or tests that we have to face

242
00:30:21,560 --> 00:30:27,400
and so we can see how we're totally it was totally an artificial creation we can often be afraid

243
00:30:27,400 --> 00:30:33,400
of meeting someone we think oh that person's really angry at me I'm you know when they see them

244
00:30:33,400 --> 00:30:37,160
then they're going to get angry at me and then when they come then there's no problem

245
00:30:41,080 --> 00:30:47,800
when we we do something and then we think our parents are going to yell at us and they don't

246
00:30:47,800 --> 00:30:55,240
yell at us or so we got really angry at someone they got really angry at us we had a fight

247
00:30:55,240 --> 00:30:58,680
the next time we see them all of a sudden they're not angry anymore and if we're still angry at

248
00:30:58,680 --> 00:31:03,560
them and we yell at them the next time they'll get angry at us again this often happens

249
00:31:04,840 --> 00:31:09,400
you get angry each other someone gives in and comes back and wants to be friends again the

250
00:31:09,400 --> 00:31:14,520
other person's still angry so the first person goes away and gets angry then the second person

251
00:31:15,240 --> 00:31:20,760
realizes oh that person wanted to want to make up and so they stop being angry

252
00:31:20,760 --> 00:31:26,280
you know the first person's angry again the first person comes back and so on and so on and so

253
00:31:26,280 --> 00:31:38,920
finally it's irreconcilable so I hope this is sort of in some way useful it's a difficult thing

254
00:31:38,920 --> 00:31:44,520
to explain because it's all semantics we're talking about wanting and talking about goals we're

255
00:31:44,520 --> 00:31:54,440
talking about putting out effort and how to balance all of these things and yet it's pretty

256
00:31:54,440 --> 00:31:58,840
clear in the Buddha's teaching that even things like effort is just the balancing of your

257
00:31:58,840 --> 00:32:05,720
faculties we effort which we're looking for is not to push push push or make it more and more

258
00:32:05,720 --> 00:32:13,640
difficult for ourselves sort of on the contrary it's to find this perfect balance where we're not

259
00:32:13,640 --> 00:32:17,800
giving rise to unwholesome states and when they do come up we're getting rid of them

260
00:32:19,320 --> 00:32:26,520
we're not we're developing wholesome states and then we're cultivating and nurturing them

261
00:32:26,520 --> 00:32:32,440
when they do a rise and that's a pretty simple process I mean it's it's not easy it's simple

262
00:32:33,320 --> 00:32:42,280
it's simple and very very difficult there's not much to it and this is often the case for

263
00:32:42,280 --> 00:32:49,800
Western meditators that we have to admit that we overanalyze and we expect much much more from

264
00:32:49,800 --> 00:32:57,800
things than they actually are and it's hard it's incomprehensible in fact that walking is just

265
00:32:57,800 --> 00:33:07,480
walking that the walking meditation really has no there's no implication it's just walking

266
00:33:07,480 --> 00:33:13,480
that doesn't mean anything to us right because we're building spaceships and flying to the moon

267
00:33:13,480 --> 00:33:21,400
and the atom bombs and blowing things up and so on we're developing cures for cancer and

268
00:33:21,400 --> 00:33:26,520
AIDS and so on and so on and saying we'll rid the we'll rid the world of disease and sickness

269
00:33:27,560 --> 00:33:33,240
and the Buddhists are here smirking and saying man he's never gonna you know you're never

270
00:33:33,240 --> 00:33:38,120
going to come up you're never going to end sickness if you keep cutting up rats and feeding them

271
00:33:38,120 --> 00:33:45,080
poison and so on many people don't realize what goes on in universities and laboratories

272
00:33:45,880 --> 00:33:50,440
they sort of sort of have an idea that it must be going on but they don't realize how

273
00:33:50,440 --> 00:33:55,240
prevalent it is I mean I don't even know the extent of it but from what I've heard it's

274
00:33:55,240 --> 00:34:02,680
quite atrocious to think of the millions and millions of animals that are tortured every day

275
00:34:03,640 --> 00:34:09,560
in the name of health in the name of well-being in the name of physical happiness

276
00:34:11,000 --> 00:34:19,720
and from a Buddhist point of view it's abominable and it's ludicrous to think that

277
00:34:19,720 --> 00:34:26,680
some of this is going to save lives or bring physical health because it totally warps

278
00:34:26,680 --> 00:34:31,560
they beat the the minds of the people who do it and so you look at these scientists and they

279
00:34:31,560 --> 00:34:36,760
look like rats in the cage and they end up spending all their time in laboratories and they're

280
00:34:36,760 --> 00:34:42,440
just rats in the cage at the end and of course when they die then they become rats in the cage

281
00:34:42,440 --> 00:34:58,120
and so in the end often we have to take a step back and you know as human beings and

282
00:35:00,440 --> 00:35:05,080
kind of look and see yes we're making progress but in what direction and certainly in terms

283
00:35:05,080 --> 00:35:08,920
of Buddhism Buddhism it's not considered progress at all unless you're coming back to

284
00:35:08,920 --> 00:35:16,040
ground zero unless you're coming back to the here and the now the place that you keep leaving

285
00:35:16,840 --> 00:35:21,880
the place where you were born and where you started unless you can come back to keep

286
00:35:21,880 --> 00:35:27,480
unless you're coming back to that place you're not getting anywhere you can progress as far

287
00:35:27,480 --> 00:35:32,680
and as wide as you like and you'll ever reach any meaningful goal

288
00:35:32,680 --> 00:35:40,920
and so for us meditators this is even more important we have to get this and understand that

289
00:35:40,920 --> 00:35:51,000
we're not trying to go anywhere we're not trying to become anything we're trying to look at the

290
00:35:51,000 --> 00:35:58,840
things that arise and see the knots that we've tied ourselves up in and through

291
00:35:58,840 --> 00:36:13,080
protracted methodical patient and energetic practice to simply untide these knots

292
00:36:14,680 --> 00:36:20,120
it's something that takes wisdom and insight and detailed introspection you know just as

293
00:36:20,120 --> 00:36:26,360
with a knot you have to really focus on the knot and you have to pull and prod you can't just pull

294
00:36:26,360 --> 00:36:30,440
it all out at once you can't just cut it and make it go away

295
00:36:35,080 --> 00:36:41,160
you have to pull and prod and and just like a big knot not one pull is going to get rid of it

296
00:36:41,160 --> 00:36:47,400
but pulling and prodding and even the biggest knot after many hours or minutes or hour hopefully

297
00:36:47,400 --> 00:36:54,520
not hours but even big knots you can untide something that looked totally untiable just by

298
00:36:54,520 --> 00:36:59,640
pulling and pulling and pulling and prodding and prodding you can untide so many things so many

299
00:36:59,640 --> 00:37:06,200
knots so our practice should reflect this that we're we're not trying for anything and we're

300
00:37:06,200 --> 00:37:11,480
not pushing for anything we're just experimenting we're just looking we're gaining knowledge about

301
00:37:11,480 --> 00:37:19,560
ourselves it's not something that you can just run that and and break open and something that you

302
00:37:19,560 --> 00:37:25,080
really have to look deeply at and say okay these are the problems that i have in my mind the

303
00:37:25,080 --> 00:37:31,480
things that are weighing me down causing me stress and suffering and looking at them when we

304
00:37:31,480 --> 00:37:36,520
practice it can often be unpleasant well why is it unpleasant? walking back and forth should

305
00:37:36,520 --> 00:37:43,320
not be unpleasant sitting still should not be unpleasant it's not boring it's not uninteresting

306
00:37:44,120 --> 00:37:47,640
if you're bored there's a problem with your mind there's a problem with the way you're

307
00:37:47,640 --> 00:37:53,000
looking at the situation this is the Buddhist point of view that there's nothing in the world

308
00:37:53,000 --> 00:38:01,080
that's boring you've judged it you've made a judgment that this is undesirable or unsatisfying

309
00:38:01,800 --> 00:38:10,600
this is unacceptable and so as a result you become bored you become angry you become frustrated

310
00:38:10,600 --> 00:38:19,800
so i hope this is in some way given some shed some light or given some ideas on how we

311
00:38:19,800 --> 00:38:25,720
should approach our practice and kind of made it maybe a little bit more easier to practice

312
00:38:26,760 --> 00:38:34,040
many of these things i've talked about before but it's nice to put them into a speech format

313
00:38:34,040 --> 00:38:41,880
and so here it is that's the demo for today now we'll continue on with our meditation first mindful

314
00:38:41,880 --> 00:39:05,240
frustration then walking in and today

