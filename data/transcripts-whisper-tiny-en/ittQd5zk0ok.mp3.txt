Hi, welcome back to Ask a Monk.
Today I will be answering a question about how to become a monk.
I get this question a lot and I believe someone recently asked it on the Google Moderator page.
Being a non-ethnic Buddhist, finding it difficult to find a place or a way to become or how to get started on the path to become a monk.
So I'm just going to go through some of the things that some of the steps that one has to take or I would say are useful to take or a part of becoming a monastic.
The first one is to prepare yourself obviously.
A lot of details that you have to take care of making sure that you're not in debt.
Making sure that you don't have any unfinished business or bad relationships that are going to come back and haunt you, say with your parents or with your loved ones.
That once you go on practice and stay at a monastery are going to haunt you and are going to be a cause for you to change your mind and realize that you don't have unfinished business you have to take care of.
There are many cases of people who go to a monastery with the intention of staying for months or years or even their whole life and after a week or two, change their mind and leave.
Not because of the monastery but because they have unfinished business.
So taking care of your business, all of these externalities and it's important to maybe do a little bit of research into some of the things that are required for you to become a monk.
For instance, being free from debt and there are many technicalities that are going to be required if you're going to be accepted as a monk.
The other thing is to prepare yourself mentally.
So I would never even think about becoming a monk if you haven't started practicing meditation and it may even be a good idea to go to a meditation course before you think about becoming a monk.
On the other hand, it might be a part of you becoming a monk. Go to the monastery, start to meditate, spend some time meditating and then become a monk.
As long as you're prepared that when you go to the center you're going to, your intention is to become ordained.
So finding a meditation center that is also a monastery where there's the option of ordaining. The whole deal with ethnicity to me is a red herring. I wouldn't worry about your ethnicity and not being this or that ethnicity because I would say one should avoid monasteries that are culturally based.
Unless of course you happen to belong to that culture.
But even then, even if you do belong to the culture, I have some sort of bias against these sort of centers because they don't tend to teach very pure or deep Buddhism.
They tend to have minimal meditation, practice and sort of a kind of a mixed understanding of the Buddha's teaching where they mix it with the culture of their whatever country and it becomes more of a social gathering or community center.
Which is good for that culture but is certainly not good for Buddhist practice because it tends to lean to ceremonies, celebrations and a lot of things that are actually antithetical to the practice of the Buddha's teaching.
So avoid centers like that. Try to find a meditation center which also happens to be a monastery rather than the other way around. A monastery which has some mention of meditation. It should be a monastery where one of the first things you read on the home page or one of the links on the home pages to meditation.
In terms of saying, oh, we do five, ten minutes of meditation here or there or we can teach you this. But they actually have meditation courses or residential stays for meditators.
Because that is an indication that their emphasis is on meditation rather than ordination.
And ordination is seen as an extension of the meditation practice for the purpose of practicing meditation on the long term.
And once you find a place, then it's not much that can be said because you're going to have to just follow their rules and regulations.
Ask them what it's going to take for you to ordain. Make sure that there's the opportunity. Try to be careful of places that say, oh, well, yes. Try to avoid the question.
If you ask, can you become a monk or monastic? And they avoid the questions and come and meditate. It's often a sign that no, they won't let you ordain, but they want you to come and meditate.
And they want you to put aside the idea of ordaining. Try to get a positive answer there from the get goes so that you're not disappointed later on.
Once you get to the center, don't have any expectations once you've got an assurance that if you practice for such and such a time, they're going to let you ordain or once they see that you practice is proper.
They're going to let you ordain and just go with it. Try to forget about the future. That's going to be the most important thing in becoming a monk is to forget about time.
But the idea of one month, one year, how long you've been here, how long you have to stay and so on. Try to be present and go step by step.
And don't be in a hurry to ordain. Try to get your meditation practice solidified first. Once you're set in the meditation practice, then it shouldn't be a problem for you to ordain and you'll be much better prepared.
Once you've become a monk, the quest doesn't end there, of course. It only starts there.
And the biggest problem for monks is the difficulty in adapting to the lifestyle and the difficulty in being patient with the monastic system.
Especially nowadays when many things are different, of course, from in the Buddha's time and monks don't always just live up in the forest in peace and quiet.
There's a lot of problems. People are less patient and have shorter attention spans and so on.
You're going to be dealing with all sorts of different kinds of people. The most important thing once you become a monk to make note of is your own assessment of your practice, assessment of your progress as a monk.
And from my own practice, my own history and watching other people who become a monk, it seems like in about the first month of being a monk, you've already got this idea that you're the best monk out there or you're a perfect monk or you're an excellent monk.
And it goes like this, after a monk you think you're perfect, after about a year, you start to realize that you weren't perfect, but now you're good, now you're after a year, you're a really good monk.
And then after about five years, you realize that you weren't a very good monk, but now you're a passable monk.
And then after I would say about ten years, you start to let go of the idea and finally say to yourself, I'm just a monk.
And so it sort of goes downhill and this is a losing of your ego.
So the reason why people fail often is not because they feel that they are unable to do it.
It's that they feel that they're too good for everybody else.
And everyone else is practicing incorrectly, only I'm practicing correctly.
And they find themselves unable to deal with other people's defilements.
And the people around them are causing problems and they don't see their own problems.
And so they feel like Buddhism is just a sham or there's no one practicing correctly.
Buddhism is very important to be patient and to understand that yes, the people around us, the other monastics may not be perfect monks.
But are they trying?
And I've seen monks with a lot of problems.
And I think what helped me through it and helped me to let go of it was answering the question, are they really trying?
And to see that actually they are trying and they're trying to help themselves.
They're trying to be better people.
You should never discount someone's efforts just because they do and say bad things sometimes.
We all get angry, we all get upset, we all have problems and that's why we're here to practice.
And the other thing about being a monk is keeping to yourself, not getting busy with groups, not getting busy with society.
And I think that's one of the big problems that has entered into the monastic life is people like to sit around joking and chatting and talking rather than staying to yourself.
A monk ultimately is someone who stays by themselves and that's what you should keep in mind that you shouldn't be seeking out groups or seeking out company.
You should be seeking out solitude, seeking out quiet with a practice of meditation.
And well that's about it, I hope that's useful for anyone interested in becoming a monk and if you're like more detailed information, you're welcome to contact me.
I could probably point out a couple of places that I think are still open to the idea of ordaining westerners.
And of course in the not so distant future, I'll be starting the process here to start up a monastery in the California area.
Okay, so thanks for tuning in and all the best.
