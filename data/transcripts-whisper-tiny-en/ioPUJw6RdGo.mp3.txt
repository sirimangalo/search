I have a lot of fear inside me. I feel inferior and superior to others at the same time and I cannot make
friends easily because I feel everyone criticizes me. I cannot feel intimacy with others. Does meditating help to
diminish my ego? Thank you. Yes, meditation does help to diminish your ego. You don't have fear, you don't feel
inferior and superior. It's not that you can't make friends easily and you don't feel that everybody
criticizes me. It's not that you cannot feel intimacy with others. What's happening is fear arises.
Fear arises, you identify with it, you think I am afraid and therefore, oh my gosh, I've got a
problem. Now, if you just saw it as fear, you'd be able to watch it arise and cease and you'd see,
oh, it came by itself and it disappeared by itself. It wasn't really mine at all and you feel
quite free because it's like looking at a blazing raging fire from afar. It's hot and that's bad but
I'm not standing in it. You can't feel inferior and superior to someone at the same time that's
impossible but at certain times feelings of superiority arise, at certain time feelings of
inferior or derives. Now, these are a product of a sense of attachment to self. It's called
conceit and it's unwholesome but it's not something that you should get upset about. It's
something that you should see clearly and see the uselessness of. See, that is to your detriment
to think such thing. One thing to feel intimacy is also a problem so the desire to feel
intimacy, you should watch when that arises and really it's misleading to think that we can
never be intimate with anyone. We're always an island. You never share other people's thoughts,
really, you never share their space. You just get really really close. That's the best you can hope
but you're never in someone else's experience. We're not halves of a whole or complete beings.
This we can do is knock. Don't worry about intimacy. Meditation will help. The whole question
is wrong and it's right. I'm not really picking on you but I want to help you to see that it's wrong
because this is the kind of question anyone would ask and other people have asked.
But it's not my ego. You've already made a mistake there. You can't ask someone,
how do I diminish my ego? Does this help diminish my ego? The fact that you say my ego is
our is egotistical. It's a funny way of phrasing.
And semantically it's not a problem but it hints to me that you do have
ego problems like most people do in the sense. Not that you're egotistical but in the sense
that you identify with things. You think of things as me as mine. And it's not your ego. In fact,
there is no ego whatsoever. You go as just a concept that we use to talk about
the set of experiences that arise, that are associated with identification and comparison and
and view of self.
