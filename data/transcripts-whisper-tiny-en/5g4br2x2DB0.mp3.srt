1
00:00:00,000 --> 00:00:14,920
How did you come to the practice of meditation, how did it change your life?

2
00:00:14,920 --> 00:00:23,680
So I first came across the concept of meditation when I was about 14, I was looking from

3
00:00:23,680 --> 00:00:29,000
school and there was this poster which said, land to achieve in a piece.

4
00:00:29,000 --> 00:00:38,000
I thought, oh, that sounds really nice, but I was much more interested with playing

5
00:00:38,000 --> 00:00:44,480
with my friends, so I never actually pursued that.

6
00:00:44,480 --> 00:00:51,160
But I remember it very distinctly, I thought this sounds very interesting, who doesn't

7
00:00:51,160 --> 00:01:01,320
know, in a piece, right? But it wasn't until I was about 19, I experienced this very severe

8
00:01:01,320 --> 00:01:10,280
sort of depression. I was coming into adulthood and there was so much confusion. I was

9
00:01:10,280 --> 00:01:21,680
just thinking about careers, relationships, meaning of life, purpose of life. And I was

10
00:01:21,680 --> 00:01:28,160
thoroughly overwhelmed. So I thought, well, it must be something out there that can help

11
00:01:28,160 --> 00:01:38,640
me. I must have just Google, which is very wonderful that we can, you know, we can

12
00:01:38,640 --> 00:01:44,760
learn about meditation through the internet. So and through this, I came across one

13
00:01:44,760 --> 00:02:00,960
term for the moon, Venabouajan brown. And from then on, I started to practice mindfulness

14
00:02:00,960 --> 00:02:12,320
of breathing. I did it really have proper instructions and I was sort of just swinging it.

15
00:02:12,320 --> 00:02:19,680
But I think it was a form of some other practice. I felt I could calm myself when I needed

16
00:02:19,680 --> 00:02:29,760
to, after a long day of work, or if I was just feeling upset, I could sit down and feel

17
00:02:29,760 --> 00:02:40,400
good again. So it became this crush. And I think within a few years, I didn't feel this

18
00:02:40,400 --> 00:02:50,520
depression anymore that I once had felt. However, something was bothering me. I thought,

19
00:02:50,520 --> 00:03:02,280
well, when I'm sitting, I feel this, this niceness, but during everyday activities when

20
00:03:02,280 --> 00:03:09,920
I'm at work or interacting with family and friends, a lot of negative emotions come up

21
00:03:09,920 --> 00:03:22,840
within me. And I wish I could just take this meditation everywhere. So I was very, I was

22
00:03:22,840 --> 00:03:30,120
very like desperate actually, just to find something which would do without. Because I

23
00:03:30,120 --> 00:03:41,360
put what, what would is it if it's only localized calm? I even tried maybe what they

24
00:03:41,360 --> 00:03:47,880
call carcinoma mutation. Well, I'd visualise a kind of flame and try to maintain that throughout

25
00:03:47,880 --> 00:03:53,640
the day. But it seemed like I was just trying to patch things together and I didn't have

26
00:03:53,640 --> 00:04:04,400
a well thought out detailed way of doing it. So I such that walking meditation, I thought

27
00:04:04,400 --> 00:04:09,040
walking is simple. Surely there's a way to walk and meditate that someone's explained quite

28
00:04:09,040 --> 00:04:18,840
well. And Bantayutadam or Bikush, you know, tend up in these results. And I was really

29
00:04:18,840 --> 00:04:27,040
glad there was a sense of relief because I thought, okay, now I can, I can, I can have

30
00:04:27,040 --> 00:04:40,560
someone who knows what they're talking about instruct me properly. And it's and this

31
00:04:40,560 --> 00:04:49,280
particular way of practice that we pass on our practice with Bantayutadam or Bikush is

32
00:04:49,280 --> 00:04:58,200
changed my life in the sense of the calm that I was really wanting to take over with

33
00:04:58,200 --> 00:05:07,920
me. I learnt to stop looking for that. I realised, you know, it wasn't quite the point and

34
00:05:07,920 --> 00:05:16,240
there was something much more deeper than the sense of calm that I was, I was chasing,

35
00:05:16,240 --> 00:05:24,560
I was dependent on.

36
00:05:24,560 --> 00:05:32,000
Why do you think the meditation centre project is important? What courses have you taken

37
00:05:32,000 --> 00:05:38,400
through civil and gather and what did you get out of it?

38
00:05:38,400 --> 00:05:47,760
Well, unfortunately for me, I have the opportunity to go on retreat and be instructed

39
00:05:47,760 --> 00:05:56,000
in person by Bantayutadam or Bikush. However, what I've done is the at home meditation

40
00:05:56,000 --> 00:06:06,720
course and this course I felt has been tremendously helpful. Now, right now, we're in

41
00:06:06,720 --> 00:06:15,680
the middle of a pandemic. There's this virus which is taking away people's lives and

42
00:06:15,680 --> 00:06:23,640
it's a deeply, deeply troubled world, but I have never felt less worried and more grounded

43
00:06:23,640 --> 00:06:35,280
than I do now. And that's because of this at home meditation course kind of guiding me

44
00:06:35,280 --> 00:06:46,680
to relate to a world which is troubled in a different sense. So that's, I think that's

45
00:06:46,680 --> 00:06:59,720
what I can say about that. But with that in mind, if a meditation course at home can

46
00:06:59,720 --> 00:07:07,240
do that imagine what a meditation course in a centre, in a supportive environment with

47
00:07:07,240 --> 00:07:17,400
a teacher in person, imagine what that would to. It's literally something which would

48
00:07:17,400 --> 00:07:39,640
be amazing to put into reality. So that's my take on that.

49
00:07:39,640 --> 00:07:57,000
It's been really good for me in terms of how responsible I am and the amount of stress

50
00:07:57,000 --> 00:08:08,040
that I experience. Because when you are having mindfulness throughout the day, you're

51
00:08:08,040 --> 00:08:15,040
watching over yourself, you are able to adhere to your moral principles, to your duties,

52
00:08:15,040 --> 00:08:24,760
to your responsibilities, much more consistently. And it's so hard to forget them. So, I

53
00:08:24,760 --> 00:08:42,160
guess it would say my life has been more successful. That's for sure.

54
00:08:42,160 --> 00:08:52,760
In a related sense, the practice has made life better for those around me because they

55
00:08:52,760 --> 00:09:00,440
no longer have to help me with my issues. They don't have to support me as much financially

56
00:09:00,440 --> 00:09:07,160
and emotionally because I'm much more of a contributor. I help them more and I need less

57
00:09:07,160 --> 00:09:13,840
help from them which means that they can focus on their lives and enjoy their lives more.

58
00:09:13,840 --> 00:09:26,800
You become this much more reliable and effective person in your circle of people that you know.

59
00:09:26,800 --> 00:09:33,440
What has been challenging about your path?

60
00:09:33,440 --> 00:09:48,320
Then one of the things that really challenged me was learning about more aspects of myself

61
00:09:48,320 --> 00:09:55,520
than I had ever thought about. With some other practice, I wasn't aware of all these

62
00:09:55,520 --> 00:10:03,240
feelings and thoughts that I have that come up within me and what was really challenging

63
00:10:03,240 --> 00:10:09,880
was for me to learn about all these aspects of myself because it can be really distressing

64
00:10:09,880 --> 00:10:25,880
to see the full spectrum of unwholesome, unwholesomeness that's within you. How much of a terrible

65
00:10:25,880 --> 00:10:35,320
person you are capable of being. I found that distressing challenging because when I looked

66
00:10:35,320 --> 00:10:45,240
at it and even now when I looked at it, it's a process of also where I react to it when

67
00:10:45,240 --> 00:10:53,320
I don't like what I'm seeing and I don't really want to look at it. That's been challenging

68
00:10:53,320 --> 00:11:00,520
but what's even more challenging than that is remembering to be mindful. I think

69
00:11:00,520 --> 00:11:11,720
Bante, it's a double bit cool, says that mindfulness study means to remember but I deeply

70
00:11:11,720 --> 00:11:17,040
feel remembering to remember it's really challenging. There are so many things that we are

71
00:11:17,040 --> 00:11:28,000
addicted to, food, entertainment. The feelings that we get from other people and it's

72
00:11:28,000 --> 00:11:37,600
so much easier to escape using those and to consistently look at yourself and learn about

73
00:11:37,600 --> 00:11:45,320
yourself. You have to it very hard to be consistently mindful, but it is challenging in

74
00:11:45,320 --> 00:12:00,800
that sense. Remembering to remember, it's a very challenging process.

75
00:12:00,800 --> 00:12:14,480
So, the Mangala community has many features to it. I've got this wonderful discord

76
00:12:14,480 --> 00:12:20,040
sample where people can meet up and read and ask questions. It's got another platform

77
00:12:20,040 --> 00:12:29,000
asked through Mangala.com to ask questions and it's really wonderful in the sense that

78
00:12:29,000 --> 00:12:34,040
when you hear other people asking questions, you learn a lot from their questions and of

79
00:12:34,040 --> 00:12:40,880
course you also get to ask questions yourself. So, it's a massive learning process and

80
00:12:40,880 --> 00:12:49,400
it's the kind of things that you learn and they really are life-changing. Now, the community

81
00:12:49,400 --> 00:12:57,600
itself is also very welcoming. I've been in other spiritual communities before but in

82
00:12:57,600 --> 00:13:02,960
the student Mangala community, you really get the sense that people are not separated.

83
00:13:02,960 --> 00:13:08,440
Like, what I mean is, in other communities, I felt like things like gender, ethnicity,

84
00:13:08,440 --> 00:13:15,920
race, age, matters. You know, you get people bending together because of those similarities

85
00:13:15,920 --> 00:13:23,200
but in the student Mangala community, these characteristics that normally separate people,

86
00:13:23,200 --> 00:13:32,320
you really get a sense that they're not relevant. So, it's a very healthy balance and

87
00:13:32,320 --> 00:13:47,120
supportive spiritual family. Yeah, and people that are always so helpful, that's really

88
00:13:47,120 --> 00:13:56,960
nice and... Yeah, you really get the sense that you're not, you're not welcome or inconvenient

89
00:13:56,960 --> 00:13:59,800
in any fashion.

90
00:13:59,800 --> 00:14:08,120
Is there anything else you'd like to share?

91
00:14:08,120 --> 00:14:17,000
I guess I could say to anybody who's watching this out of curiosity or just considering

92
00:14:17,000 --> 00:14:24,120
taking up this way of practice that it does not have to try it. It might be very surprised

93
00:14:24,120 --> 00:14:39,920
about where it takes you. So, that's all I can say about that. Thank you for giving

94
00:14:39,920 --> 00:15:09,760
me the opportunity to share some of what I know and my perspective, I'll do.

