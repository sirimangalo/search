1
00:00:30,000 --> 00:00:50,760
Good evening everyone, tonight's Saturday, we're having our English session.

2
00:00:50,760 --> 00:00:56,320
And we have three people, so far tuned in on the internet and hopefully the mic is fixed

3
00:00:56,320 --> 00:01:07,840
last time the mic was using the wrong mic, so I'll just give a short talk and then

4
00:01:07,840 --> 00:01:16,160
we'll start meditating.

5
00:01:16,160 --> 00:01:23,840
The meditation we're practicing is the very specific Buddhist meditation.

6
00:01:23,840 --> 00:01:32,840
There are many types of Buddhist meditation and it's complicated by the fact that we take

7
00:01:32,840 --> 00:01:42,800
our meditation from the wide diversity of text.

8
00:01:42,800 --> 00:01:52,680
But if we return back to those texts which claim to be the things that Buddha said during

9
00:01:52,680 --> 00:01:59,720
his life, we see a real pattern in the Buddhist teaching.

10
00:01:59,720 --> 00:02:10,320
So in all traditions we have, all traditions of Buddhism we have a built up tradition of meditation

11
00:02:10,320 --> 00:02:11,320
techniques.

12
00:02:11,320 --> 00:02:17,840
In the tradition that I follow they have 40 different techniques it's just for quieting your

13
00:02:17,840 --> 00:02:25,520
mind, 40 different meditation objects and then it gets very complicated.

14
00:02:25,520 --> 00:02:33,320
In every meditation tradition, in every Buddhist tradition they have countless or a

15
00:02:33,320 --> 00:02:40,200
great diversity of meditation techniques.

16
00:02:40,200 --> 00:02:47,120
But as I said when we go back to these original text we have some basic fundamentals.

17
00:02:47,120 --> 00:03:15,640
In terms of how we can construct a truly Buddhist meditation practice.

18
00:03:15,640 --> 00:03:23,200
And what we see in the original teachings or those earlier teachings is that the Buddha

19
00:03:23,200 --> 00:03:41,720
was very very clear about the specific meditation practice we call mindfulness.

20
00:03:41,720 --> 00:03:51,040
It was always stressing this practice that whereas he would teach to the individual people

21
00:03:51,040 --> 00:03:57,080
in different things, when he came right out and said what is the path of practice that

22
00:03:57,080 --> 00:04:02,720
one should undertake in order to become free from suffering.

23
00:04:02,720 --> 00:04:09,320
He always came back to the practice of mindfulness and specifically the four foundations

24
00:04:09,320 --> 00:04:14,280
of mindfulness.

25
00:04:14,280 --> 00:04:19,920
So the Buddha didn't just say be mindful because when we hear be mindful and we hear

26
00:04:19,920 --> 00:04:27,840
people teaching mindfulness meditation, we still don't get a clear idea of how to practice

27
00:04:27,840 --> 00:04:45,360
and so mindfulness meditation becomes quite varied in its forms in the way we practice.

28
00:04:45,360 --> 00:04:51,800
But the Buddha was not so general, he was quite specific in terms of how you practice mindfulness.

29
00:04:51,800 --> 00:04:55,760
So we don't need to create our own meditation practice.

30
00:04:55,760 --> 00:05:00,280
It's quite useful when we go back to these ancient texts and really practice something

31
00:05:00,280 --> 00:05:06,120
that we can be sure or more or less sure was if not taught by the Buddha because we have

32
00:05:06,120 --> 00:05:11,120
no way of knowing for sure whether this was the Buddha's teaching or not.

33
00:05:11,120 --> 00:05:15,360
At any rate it's quite clear that this is an ancient technique around the time that the

34
00:05:15,360 --> 00:05:22,800
Buddha was said to live in by all external appearances.

35
00:05:22,800 --> 00:05:25,800
This is what the Buddha taught.

36
00:05:25,800 --> 00:05:29,800
So we have this teaching called the four foundations of mindfulness and it's always important

37
00:05:29,800 --> 00:05:32,800
for us to come back to this.

38
00:05:32,800 --> 00:05:37,400
This is really the basis of the Buddha's meditation practice.

39
00:05:37,400 --> 00:05:44,120
How we know this is because not only the Buddha teach it and not only did he stress

40
00:05:44,120 --> 00:05:52,120
that this practice and repeatedly encourage people to practice the four foundations of mindfulness.

41
00:05:52,120 --> 00:06:02,600
But he also gave clear instruction as to the benefits and the purpose and the real nature

42
00:06:02,600 --> 00:06:12,000
of the four foundations of mindfulness in terms of calling it the aka-yana-manga.

43
00:06:12,000 --> 00:06:19,600
And the aka-yana-manga has been veryously translated as the direct path, even the only way

44
00:06:19,600 --> 00:06:30,640
the one way or path that only goes in one direction, it's the sure path, as many meanings.

45
00:06:30,640 --> 00:06:35,000
But the Buddha never said this about any other meditation technique.

46
00:06:35,000 --> 00:06:39,520
He was very specific about the four foundations of mindfulness that this is the direct

47
00:06:39,520 --> 00:06:43,680
way to become free from suffering.

48
00:06:43,680 --> 00:06:52,080
And he said specifically that this is the direct way for five goals, five aims that we hope

49
00:06:52,080 --> 00:06:59,600
to achieve.

50
00:06:59,600 --> 00:07:05,800
So first of all, before I go into the aims of mindfulness meditation, just to explain briefly

51
00:07:05,800 --> 00:07:10,440
for those who don't know what are the four foundations of mindfulness.

52
00:07:10,440 --> 00:07:22,720
Four foundations of mindfulness simply mean those four groups of objects that are valid

53
00:07:22,720 --> 00:07:27,040
to use in the development of mindfulness.

54
00:07:27,040 --> 00:07:34,920
When we're trying to create this clear awareness, this bare understanding of things as they

55
00:07:34,920 --> 00:07:41,840
are, we can use any one of these four objects.

56
00:07:41,840 --> 00:07:46,400
And basically it's one way of describing reality or categorizing reality, breaking it up

57
00:07:46,400 --> 00:07:58,560
in the parts to make it easy to acknowledge, easy to catch, to label when something arises,

58
00:07:58,560 --> 00:08:02,360
we know where it fits in, and so we know how to acknowledge it.

59
00:08:02,360 --> 00:08:08,040
And the first one is body, and this refers to any movement of the body or any posture

60
00:08:08,040 --> 00:08:09,520
of the body.

61
00:08:09,520 --> 00:08:14,240
When we're sitting, this is part of the body, when we bend, as part of the body, when

62
00:08:14,240 --> 00:08:18,600
we stand up, this is part of the body.

63
00:08:18,600 --> 00:08:22,720
When our belly rises, this is the body.

64
00:08:22,720 --> 00:08:32,440
Even when we feel hot or cold, this is part of the body.

65
00:08:32,440 --> 00:08:43,200
When we walk, when we eat, when we talk, when we go to the washroom, when we brush our

66
00:08:43,200 --> 00:08:50,640
teeth or so on, when we eat food, this is all part of the body.

67
00:08:50,640 --> 00:08:55,600
This is all, comes under the body, so being aware of these things is the first and quite

68
00:08:55,600 --> 00:09:01,760
important meditation practice, and we can do this even when we're not meditating.

69
00:09:01,760 --> 00:09:08,760
The second is the feelings, when we're mindful of happy feelings, unhappy feelings, pleasant

70
00:09:08,760 --> 00:09:20,440
sensations, unpleasant sensation, calm feelings, peaceful feelings.

71
00:09:20,440 --> 00:09:27,320
The third one is the mind being aware and recognizing thoughts for what they are.

72
00:09:27,320 --> 00:09:33,000
When we're distracted, when our minds are focused, when we have good thoughts or bad thoughts,

73
00:09:33,000 --> 00:09:42,760
thinking about the past or future, just knowing that it's a thought.

74
00:09:42,760 --> 00:09:49,560
And the fourth one is the dhammas or the rest of the stuff, those other things that are

75
00:09:49,560 --> 00:09:52,080
going to come up in the practice.

76
00:09:52,080 --> 00:09:58,200
Dhamma really means the teaching of the Buddha, but here the Buddha used this as a category

77
00:09:58,200 --> 00:10:04,480
to put together everything else that's going to come up, or to lead one further on the

78
00:10:04,480 --> 00:10:13,440
path towards the dhamma to incorporate the teaching into one's practice.

79
00:10:13,440 --> 00:10:16,360
So first we have the teaching on the hindrances.

80
00:10:16,360 --> 00:10:20,600
This is a very important teaching that we're going to have to use in our observation of

81
00:10:20,600 --> 00:10:21,600
reality.

82
00:10:21,600 --> 00:10:24,320
These are things we're going to get in the way of our practice.

83
00:10:24,320 --> 00:10:30,400
So when we have liking or disliking, drowsiness, distraction, doubt, all of these we have

84
00:10:30,400 --> 00:10:38,280
to be very clear to see what they are and to not follow after, to not encourage and to

85
00:10:38,280 --> 00:10:42,080
not develop.

86
00:10:42,080 --> 00:10:47,200
Next one is the senses, seeing earrings, smelling, tasting, feeling, thinking, being mindful

87
00:10:47,200 --> 00:10:49,440
of these as well.

88
00:10:49,440 --> 00:10:54,280
And so on, there are many teachings that will come in useful and we can develop as

89
00:10:54,280 --> 00:10:57,640
in practice.

90
00:10:57,640 --> 00:11:00,160
These four are the four foundations.

91
00:11:00,160 --> 00:11:07,200
Mindfulness is just the recognition of things as they are, is the bare recognition of

92
00:11:07,200 --> 00:11:08,360
things as they are.

93
00:11:08,360 --> 00:11:13,880
So people ask, you know, I know when I feel pain, I know that I'm feeling pain, why

94
00:11:13,880 --> 00:11:20,160
do I have to say to myself, pain, pain, pain, pain, why do I have to practice meditation?

95
00:11:20,160 --> 00:11:23,800
Why can't I just sit there and know that I'm in pain?

96
00:11:23,800 --> 00:11:27,720
And the point is that when you know that you're in pain, there's an idea that I am in pain

97
00:11:27,720 --> 00:11:29,920
and it's mine.

98
00:11:29,920 --> 00:11:35,880
And so we attached to it as being a bad thing, as being a problem for me.

99
00:11:35,880 --> 00:11:38,440
If it's yours, then you have a problem.

100
00:11:38,440 --> 00:11:41,840
If pain really were yours, then you would have a problem.

101
00:11:41,840 --> 00:11:47,560
But since the pain is not yours, there's no reason to understand it in this way.

102
00:11:47,560 --> 00:11:50,920
It's something that arises in ceases.

103
00:11:50,920 --> 00:11:56,280
It's not yours in any way, shape, or form.

104
00:11:56,280 --> 00:11:57,880
We have to change our minds.

105
00:11:57,880 --> 00:12:08,840
We have to actually adjust our reaction, our understanding of the object, so that we simply

106
00:12:08,840 --> 00:12:09,840
see it for what it is.

107
00:12:09,840 --> 00:12:11,400
And this is what meditation is for.

108
00:12:11,400 --> 00:12:16,120
It's the adjusting of the mind, straightening of the mind.

109
00:12:16,120 --> 00:12:19,720
The mind is not straight by nature, it's crooked.

110
00:12:19,720 --> 00:12:25,720
We're always trying to get away or weasel our way out of things, so that we don't

111
00:12:25,720 --> 00:12:36,680
have to suffer, so that we don't have to deal with difficult situations.

112
00:12:36,680 --> 00:12:42,200
Our mind is not straight, and we have to work to straighten it, so this is why we say

113
00:12:42,200 --> 00:12:50,880
to ourselves, pain, pain, pain, it's to teach ourselves, and then it's just pain.

114
00:12:50,880 --> 00:12:58,680
So mindfulness is a bare recognition, recognizing something just for what it is, not just

115
00:12:58,680 --> 00:13:03,560
recognizing it and then going on and liking or disliking it, but stopping yourself at the

116
00:13:03,560 --> 00:13:12,040
bare awareness, the simple knowledge of the object for what it is.

117
00:13:12,040 --> 00:13:18,480
This is what it means to be mindful.

118
00:13:18,480 --> 00:13:22,720
So why did the Buddha say we should practice these four things, or this practice?

119
00:13:22,720 --> 00:13:26,920
The Buddha said, when you walk, you should know I'm walking.

120
00:13:26,920 --> 00:13:27,920
This is walking.

121
00:13:27,920 --> 00:13:33,880
When we say walking, we remind ourselves that we're walking, that it's just walking,

122
00:13:33,880 --> 00:13:41,320
it's not good, it's not bad, and not letting our minds wander, not letting our minds make

123
00:13:41,320 --> 00:13:43,640
more of anything than what it is.

124
00:13:43,640 --> 00:13:47,680
When we feel a pain, we know that there's a pain, and when we're thinking, we know that

125
00:13:47,680 --> 00:13:52,560
we're thinking, we know it for what it is.

126
00:13:52,560 --> 00:13:55,120
Why did the Buddha say that we have to do this?

127
00:13:55,120 --> 00:13:59,880
Buddha said, but he said, timataya, aniseeto, javihara ti.

128
00:13:59,880 --> 00:14:11,280
We dwell simply knowing something, simply being mindful, specifically, of the object

129
00:14:11,280 --> 00:14:14,120
for what it is.

130
00:14:14,120 --> 00:14:19,280
Why do we do this, the Buddha said, it leads to five benefits, or five goals, the five

131
00:14:19,280 --> 00:14:23,640
goals, and Buddha said, the five aims of the Buddha's teaching.

132
00:14:23,640 --> 00:14:31,320
And these are first of all, it purifies the mind.

133
00:14:31,320 --> 00:14:35,160
This is really the biggest reason why we practice meditation.

134
00:14:35,160 --> 00:14:38,160
I would say the defining point of Buddhism.

135
00:14:38,160 --> 00:14:43,080
And I would say Hinduism talks something about purifying the mind, and that's possibly

136
00:14:43,080 --> 00:14:49,600
because of the intertwined nature between the Buddha's teaching and Hinduism growing up

137
00:14:49,600 --> 00:14:50,600
together.

138
00:14:50,600 --> 00:14:58,600
They're like an older and a younger brother, older and younger siblings.

139
00:14:58,600 --> 00:15:03,560
So they share some of the same qualities, but the defining factor of Buddhism is the purification

140
00:15:03,560 --> 00:15:04,560
of the mind.

141
00:15:04,560 --> 00:15:07,720
That's the core of Buddhism.

142
00:15:07,720 --> 00:15:13,520
The Buddha went into great detail about what our mental impurities, how to practice, to

143
00:15:13,520 --> 00:15:19,640
purify the mind, and what is required to be considered pure of mind.

144
00:15:19,640 --> 00:15:23,680
It's not just that we sit quietly and we don't get angry or greedy.

145
00:15:23,680 --> 00:15:28,520
It's that we fully understand everything around of us so that we will never become angry

146
00:15:28,520 --> 00:15:34,720
or greedy again, that we will never see things as more than what they are.

147
00:15:34,720 --> 00:15:48,720
We'll never mistake things for being other than what they are.

148
00:15:48,720 --> 00:15:54,320
And so this is the most important reason for us to practice mindfulness, and this is

149
00:15:54,320 --> 00:15:56,720
what we have to agree with to practice Buddhism.

150
00:15:56,720 --> 00:16:01,560
To say that we're Buddhist, practicing Buddha's teaching, we have to get our minds around

151
00:16:01,560 --> 00:16:04,880
this idea of the purity of mind, this is what we're striving for.

152
00:16:04,880 --> 00:16:10,200
We're not going to sit there just to feel happy for a while, we're not coming to practice

153
00:16:10,200 --> 00:16:17,080
to gain magical powers or read people's minds or see heaven, see hell, fly through

154
00:16:17,080 --> 00:16:20,440
the air, whatever.

155
00:16:20,440 --> 00:16:25,720
We're not practicing to see special sights.

156
00:16:25,720 --> 00:16:27,640
I read something today.

157
00:16:27,640 --> 00:16:36,400
These people were asking this meditation teacher a monk, you know, what's wrong with

158
00:16:36,400 --> 00:16:41,480
loosen it, loosen it, loosen the genetic drugs.

159
00:16:41,480 --> 00:16:45,240
When we take these drugs, it heightens our visions.

160
00:16:45,240 --> 00:16:57,360
We actually see things clearer and it allows us to gain these visions even even more

161
00:16:57,360 --> 00:17:02,480
profoundly, and the monk turned and looked at them and said, you don't need drugs to

162
00:17:02,480 --> 00:17:09,920
hallucinate, you're hallucinating already.

163
00:17:09,920 --> 00:17:17,600
And I think that separates what we're doing from what the ordinary person who's never

164
00:17:17,600 --> 00:17:23,040
had any instruction in Buddhism, practices when they go to meditate.

165
00:17:23,040 --> 00:17:25,240
They're just looking to hallucinate.

166
00:17:25,240 --> 00:17:29,920
And it's because they hallucinate already, they don't see things from what they are.

167
00:17:29,920 --> 00:17:33,960
They don't understand that this is meaningless, that whatever we gain in our meditation

168
00:17:33,960 --> 00:17:38,680
we're just going to lose again.

169
00:17:38,680 --> 00:17:44,520
And the exception being wisdom, this once we understand that this is true and whatever

170
00:17:44,520 --> 00:17:51,920
we gain we lose, then that wisdom is something we'll never lose, we'll never cling to

171
00:17:51,920 --> 00:17:52,920
anything.

172
00:17:52,920 --> 00:18:00,400
We'll be able to see things from what they are.

173
00:18:00,400 --> 00:18:05,400
So meditation practice purifies the mind, this is the most important, because when we're

174
00:18:05,400 --> 00:18:09,600
saying to ourselves, pain, pain, pain, we're not upset about the pain.

175
00:18:09,600 --> 00:18:14,600
When we're upset and we say to ourselves, upset, upset, upset, we're not upset about

176
00:18:14,600 --> 00:18:20,520
the upset, wherever we catch ourselves, whatever our mind is doing right now we stop

177
00:18:20,520 --> 00:18:23,680
it.

178
00:18:23,680 --> 00:18:36,120
We change the mind from this clinging, running around in circles, the cycle of suffering.

179
00:18:36,120 --> 00:18:40,520
We break free from the cycle, this is why we meditate.

180
00:18:40,520 --> 00:18:45,320
The truth is, once your mind is pure, you don't need anything else.

181
00:18:45,320 --> 00:18:48,280
There's no other, there's four more benefits, but really they're meaningless once your

182
00:18:48,280 --> 00:18:53,440
mind is pure, because whatever I'm going to say next has to come from the purity of the

183
00:18:53,440 --> 00:18:54,440
mind.

184
00:18:54,440 --> 00:18:58,440
There's nothing that is, there's no positive thing that can't be had with the purity

185
00:18:58,440 --> 00:19:00,360
of the mind.

186
00:19:00,360 --> 00:19:06,760
When your mind is pure, you're happy, you're at peace, you're free, you never fall back

187
00:19:06,760 --> 00:19:13,640
into suffering.

188
00:19:13,640 --> 00:19:23,240
But the question remains, if we just said, you become pure, it still doesn't make much

189
00:19:23,240 --> 00:19:24,240
sense.

190
00:19:24,240 --> 00:19:27,240
I mean, if that were the only thing that came from the practice, you just become pure,

191
00:19:27,240 --> 00:19:31,960
but you still have all sorts of suffering and so on.

192
00:19:31,960 --> 00:19:36,200
We have to explain what happens when you become pure, so there are four other benefits.

193
00:19:36,200 --> 00:19:41,840
It's not that purity is in and out, it's just that when you become pure there's more

194
00:19:41,840 --> 00:19:43,840
to it than that.

195
00:19:43,840 --> 00:19:51,740
The second one is that you do away with all sorts of mental distress, all sorts of

196
00:19:51,740 --> 00:19:55,840
mental sicknesses.

197
00:19:55,840 --> 00:20:03,440
The greatest thing about meditation is that it heals the mind.

198
00:20:03,440 --> 00:20:06,320
It's like medication.

199
00:20:06,320 --> 00:20:16,560
It heals the body, meditation heals the mind, it's a parallel.

200
00:20:16,560 --> 00:20:22,120
And so people who suffer from depression, people who suffer from anxiety, people who suffer

201
00:20:22,120 --> 00:20:35,880
from sadness, people who suffer from anger and so on, hatred.

202
00:20:35,880 --> 00:20:40,120
Whatever mental sickness, people who even take drugs or kill themselves because of these

203
00:20:40,120 --> 00:20:44,680
mental sicknesses, depression is a mental illness.

204
00:20:44,680 --> 00:20:48,440
I had a friend once who said, told me she was clinically depressed.

205
00:20:48,440 --> 00:20:54,800
I got very upset at her when she told me she was clinically depressed because even all

206
00:20:54,800 --> 00:20:58,160
that means is she went to a clinic and they told her she was depressed.

207
00:20:58,160 --> 00:21:04,160
Of course doctors want to tell you you're clinically depressed because then suddenly it's

208
00:21:04,160 --> 00:21:06,360
a sickness.

209
00:21:06,360 --> 00:21:11,760
But it's always a sickness, we all have sickness, sickness just means not well.

210
00:21:11,760 --> 00:21:15,600
If you're a little bit sick, you're still sick, you're sick a little bit, it's still

211
00:21:15,600 --> 00:21:16,840
a sickness.

212
00:21:16,840 --> 00:21:22,640
Getting anger and according to Buddhism is a sickness.

213
00:21:22,640 --> 00:21:30,400
But here we're talking about all of the labels that we put on our mental development,

214
00:21:30,400 --> 00:21:37,160
the sicknesses, depression, people are depressed and they take medicine for it, they'll

215
00:21:37,160 --> 00:21:38,760
kill themselves for it.

216
00:21:38,760 --> 00:21:45,120
People are insomniac and so they'll take medication for it and they'll destroy themselves

217
00:21:45,120 --> 00:21:51,160
by stressing over their stress and not being able to sleep.

218
00:21:51,160 --> 00:21:57,320
And meditation clears all of this up, it may take a long time and it's certainly a gradual

219
00:21:57,320 --> 00:22:00,600
process.

220
00:22:00,600 --> 00:22:05,360
But I guarantee if you practice meditation, simply seeing things for what they are and really

221
00:22:05,360 --> 00:22:10,840
get this concept of seeing things just for what they are.

222
00:22:10,840 --> 00:22:16,480
Even just in a week or two weeks if you're practicing a little bit every day, you'll

223
00:22:16,480 --> 00:22:22,160
see a profound change in your outlook, you'll still get stressed, you'll still get depressed.

224
00:22:22,160 --> 00:22:30,040
But it's no longer a monster, it's still an enemy, it's still an adversary, but it's

225
00:22:30,040 --> 00:22:36,520
no longer your master, you're no longer a slave to the emotion, you just know that you're

226
00:22:36,520 --> 00:22:40,880
depressed and you let it go, let it be as best you can.

227
00:22:40,880 --> 00:22:46,960
And you get better and better at this and you realize that it's only a matter of time

228
00:22:46,960 --> 00:22:54,240
before you can make yourself understand that this depression, this stress, this anxiety,

229
00:22:54,240 --> 00:23:02,560
this sadness disappears.

230
00:23:02,560 --> 00:23:07,480
If you stick with it, if you understand this, that it's we're not going to change our

231
00:23:07,480 --> 00:23:12,520
minds overnight, but we're going to change the way we look at things overnight.

232
00:23:12,520 --> 00:23:17,560
We're going to change our path overnight, so that whereas before we were always making

233
00:23:17,560 --> 00:23:22,120
our problems worse, now we're going to start making them better, and they won't be better

234
00:23:22,120 --> 00:23:29,400
overnight, but we've changed our entire way of looking at things and our paths from

235
00:23:29,400 --> 00:23:34,960
becoming more sick to becoming well.

236
00:23:34,960 --> 00:23:42,920
The third benefit gets a little bit more specific.

237
00:23:42,920 --> 00:23:48,280
Specifically speaking, meditation according to the Buddha, the four foundations of mindfulness

238
00:23:48,280 --> 00:23:55,400
leads to the end of physical and mental suffering.

239
00:23:55,400 --> 00:24:03,640
So not only talking about the gross mental sicknesses, illnesses, all suffering is done

240
00:24:03,640 --> 00:24:09,800
away with the Buddha through this meditation practice, whether it be physical suffering

241
00:24:09,800 --> 00:24:10,800
or mental suffering.

242
00:24:10,800 --> 00:24:20,680
And you can verify this on a basic level by yourself through the meditation practice.

243
00:24:20,680 --> 00:24:27,040
I was mentioning to one of the meditators today about his shoulders, he said his shoulders

244
00:24:27,040 --> 00:24:30,640
had pain, and I was explaining to him, you know, acknowledge it will go away.

245
00:24:30,640 --> 00:24:36,360
And he really didn't, I don't think he didn't.

246
00:24:36,360 --> 00:24:42,040
What wasn't really, you know, sure, you know, understand it, because it's something new.

247
00:24:42,040 --> 00:24:53,160
And then I said, have you ever been to Jesus, and he said, yes, I said, yes, I said,

248
00:24:53,160 --> 00:24:56,000
this is just like that.

249
00:24:56,000 --> 00:24:58,000
Because we don't want to sit through the pain, right?

250
00:24:58,000 --> 00:25:01,120
We feel when pain comes in our shoulders, it's something's wrong.

251
00:25:01,120 --> 00:25:08,240
I have to sit against the wall, maybe I have to stop meditating, maybe it's too long.

252
00:25:08,240 --> 00:25:12,360
But it's an amazing thing that when you sit through it, if you just watch the pain and

253
00:25:12,360 --> 00:25:23,160
let yourself be with it, you're able to change your bodily makeup, you're able to change

254
00:25:23,160 --> 00:25:32,920
your physical composition, the way your muscles work, your muscles are healed themselves

255
00:25:32,920 --> 00:25:33,920
in a way.

256
00:25:33,920 --> 00:25:40,520
There's the reason why we tense up, why we're in pain is because we keep tensing our muscles

257
00:25:40,520 --> 00:25:43,120
through our stress, we keep shifting them.

258
00:25:43,120 --> 00:25:48,320
We don't notice it, and we're always tensing and tensing and tensing, and so we're building

259
00:25:48,320 --> 00:25:56,480
up physical tension and also mental discontent, mental aversion.

260
00:25:56,480 --> 00:26:04,560
This is bad, shifting, this is worse, shifting, this is terrible, shifting until it becomes

261
00:26:04,560 --> 00:26:06,920
a real problem.

262
00:26:06,920 --> 00:26:11,320
Once we work that out, many of the pains in our bodies disappear and never come back.

263
00:26:11,320 --> 00:26:16,840
It's not to say we're not going to feel pain, but it's really a whole other playing

264
00:26:16,840 --> 00:26:22,840
field.

265
00:26:22,840 --> 00:26:30,800
Obviously once we have no mental defilements or anger in our minds, no judgements in our minds,

266
00:26:30,800 --> 00:26:35,960
and there's no mental suffering.

267
00:26:35,960 --> 00:26:39,920
But physical suffering can be overcome as well through the practice, and ultimately when

268
00:26:39,920 --> 00:26:44,680
we pass away, according to Buddhist doctrine, a person who is still clinging will have

269
00:26:44,680 --> 00:26:50,040
to be born again, they continue on, they're carried on by their clinging, there's still

270
00:26:50,040 --> 00:26:56,320
fuel left to create further existence, but a person without clinging will not come back

271
00:26:56,320 --> 00:27:00,000
to be born.

272
00:27:00,000 --> 00:27:05,520
And so technically speaking, it is clearly a way to overcome physical suffering because

273
00:27:05,520 --> 00:27:10,680
in the end we don't have to come back and live this life again and again and again and

274
00:27:10,680 --> 00:27:14,760
again again.

275
00:27:14,760 --> 00:27:21,560
The fourth benefit, the fourth aim of the practice, is to attain the right path.

276
00:27:21,560 --> 00:27:27,800
So the Buddha said that the four foundations of mindfulness is the way to find the right path,

277
00:27:27,800 --> 00:27:37,320
the right method or means of living one's life, and this is really a bold statement.

278
00:27:37,320 --> 00:27:44,160
One of the things people don't like about the translation of A.K. and the manga as the

279
00:27:44,160 --> 00:27:52,840
only way is that they see that there are many ways always to the same goal, just as

280
00:27:52,840 --> 00:28:01,200
there are many paths to the same city or so on, many roads leading to the same destination.

281
00:28:01,200 --> 00:28:05,880
And there was a Burmese monk who said, yes, well there's many roads, but they're

282
00:28:05,880 --> 00:28:08,560
all roads.

283
00:28:08,560 --> 00:28:14,960
You can't drive a car through the ditch and expect to get to your destination.

284
00:28:14,960 --> 00:28:19,240
And I think this is a good explanation of how we can justify saying mindfulness is the

285
00:28:19,240 --> 00:28:22,840
right path, it's the right way to live your life.

286
00:28:22,840 --> 00:28:26,640
Because we're not telling you that being a monk is the right way to live your life

287
00:28:26,640 --> 00:28:31,560
or being a Buddhist is the right way to live your life.

288
00:28:31,560 --> 00:28:36,520
So we are saying that being mindful is the right way to live your life, seeing things

289
00:28:36,520 --> 00:28:39,520
for what they are is the right way to live your life.

290
00:28:39,520 --> 00:28:43,600
This is a profound teaching I think, it's throwing out the idea that you have to become

291
00:28:43,600 --> 00:28:48,200
a monk or you have to become this or that to live your life correctly.

292
00:28:48,200 --> 00:28:54,520
Some people will say, if you're not in our group, you're living your life wrong and

293
00:28:54,520 --> 00:28:56,480
you'll go to hell.

294
00:28:56,480 --> 00:29:04,840
Once you subscribe to our teachings, you will go to heaven just by saying agreeing with

295
00:29:04,840 --> 00:29:10,000
what we teach, being a part of our group, who identifying yourself is this or that.

296
00:29:10,000 --> 00:29:14,440
In Buddhism, that's not the case, the Buddha didn't say, this is the right way to live

297
00:29:14,440 --> 00:29:17,080
your life, that's the right way to live your life.

298
00:29:17,080 --> 00:29:21,360
He said, mindfulness is the right way to live your life and you can be anything you want.

299
00:29:21,360 --> 00:29:30,600
You can be a lawyer, a doctor, a businessman, you can be a king or a president, you can

300
00:29:30,600 --> 00:29:34,720
be a politician, whatever.

301
00:29:34,720 --> 00:29:38,560
If you're mindful, you're living your life correctly.

302
00:29:38,560 --> 00:29:43,880
And so, yes, it's being specific and saying, this is the right way and otherwise is the

303
00:29:43,880 --> 00:29:48,840
wrong way, but it's also being very general and saying, anyhow, you want to live your life

304
00:29:48,840 --> 00:29:52,520
just be mindful.

305
00:29:52,520 --> 00:29:56,160
It's the right way to live our life because then we see everything clearly.

306
00:29:56,160 --> 00:29:59,200
When people talk to us, we see clearly what's going on in their minds.

307
00:29:59,200 --> 00:30:04,720
When they say nasty things to us, we see there's something going on in their minds rather

308
00:30:04,720 --> 00:30:07,960
than being offended and upset by it.

309
00:30:07,960 --> 00:30:12,040
We're not afraid of people, we're not worried about them, we're not feeling, when

310
00:30:12,040 --> 00:30:17,520
people try to assert themselves, power tripping over us or so on.

311
00:30:17,520 --> 00:30:27,480
We don't feel threatened, we don't feel any need to accommodate ourselves to other people.

312
00:30:27,480 --> 00:30:41,200
So we're able to live our lives in a much more harmonious way, we're not slaves to our

313
00:30:41,200 --> 00:30:45,280
emotions and we're not slaves to other people.

314
00:30:45,280 --> 00:30:55,440
And as a result, we can meet with great success in business, in our society, in our families.

315
00:30:55,440 --> 00:30:59,840
We're able to be a great support for people rather than being a burden.

316
00:30:59,840 --> 00:31:05,280
And so, however we live our lives, we do it successfully.

317
00:31:05,280 --> 00:31:13,480
And the fifth benefit is that meditation leads to freedom or release, and this is Nirvana.

318
00:31:13,480 --> 00:31:21,840
I think probably the best translation of Nirvana I can think of is release, because it's

319
00:31:21,840 --> 00:31:25,320
often translated as freedom or explained as being freedom.

320
00:31:25,320 --> 00:31:30,720
But freedom is like there was something else oppressing you or holding you captive.

321
00:31:30,720 --> 00:31:39,920
Release means it has a double meaning, because in Buddhism you let go, you're released

322
00:31:39,920 --> 00:31:52,880
from your attachment, we're like holding on to ourselves.

323
00:31:52,880 --> 00:32:01,440
We're only imprisoned by our own attachment, and now we say it's like a bird clinging

324
00:32:01,440 --> 00:32:07,520
to the side of the cliff, you have a bird clinging to the side of the cliff afraid that

325
00:32:07,520 --> 00:32:10,440
it's going to fall.

326
00:32:10,440 --> 00:32:14,280
This is how we are assuming beings, we're like a bird on the side of the cliff, holding

327
00:32:14,280 --> 00:32:21,560
on for dear life, because if it lets go of things, it's going to fall to its death.

328
00:32:21,560 --> 00:32:26,160
Meditation is letting go, and realizing that you're not going to fall, you're going to

329
00:32:26,160 --> 00:32:30,800
fly, realizing that we will let go of things, we're not going to lose everything, we're

330
00:32:30,800 --> 00:32:36,720
going to gain everything, when we don't worry anymore, when we don't stress over things,

331
00:32:36,720 --> 00:32:40,640
everything comes to us.

332
00:32:40,640 --> 00:32:46,640
We have these examples of the Buddha in his past lives, how he gave up his kingdom, gave

333
00:32:46,640 --> 00:32:51,240
up his riches, gave up everything, and it all came back to him, just like a boomerate.

334
00:32:51,240 --> 00:32:55,480
He was like giving everything away, and he was even more rich and more powerful than

335
00:32:55,480 --> 00:32:57,680
before.

336
00:32:57,680 --> 00:33:02,080
And you might think this is just a fairy tale or just a means of indoctrinating people

337
00:33:02,080 --> 00:33:12,640
than the art of giving, but if you try it, you'll see it's 100% spot on true.

338
00:33:12,640 --> 00:33:16,440
My students, they were talking about we're going to give this big donation in September

339
00:33:16,440 --> 00:33:17,440
in Thailand.

340
00:33:17,440 --> 00:33:24,040
We're going to give 84 sets of robes, costs a little bit over $9,000, and they were

341
00:33:24,040 --> 00:33:30,920
saying, why don't we save up money at, if we do this, I want to help you, but we'd

342
00:33:30,920 --> 00:33:37,520
rather wait and help you build a monastery, and we've only got limited funds, and I said,

343
00:33:37,520 --> 00:33:42,320
the limited funds come from our worry, come from our stress.

344
00:33:42,320 --> 00:33:47,440
I said, this world is full of money, it's full of riches.

345
00:33:47,440 --> 00:33:56,000
When you open up, when you open yourselves up to the generosity, when your heart is

346
00:33:56,000 --> 00:34:03,000
why open-wide, you'll only gain, you'll only stand to gain when you give, and so we don't

347
00:34:03,000 --> 00:34:08,560
have to worry about the future or building a monastery or so, we'll give now, we do good

348
00:34:08,560 --> 00:34:21,160
things now, and more comes to us than ever before, the more we give up, the more we have,

349
00:34:21,160 --> 00:34:27,320
if only the world would understand this, there would be no poverty, there would be no

350
00:34:27,320 --> 00:34:33,240
famine, there would be no problems in the world, if people understood how to share, it's

351
00:34:33,240 --> 00:34:40,280
like what they teach you in kindergarten, you know, only we know how to share, but here

352
00:34:40,280 --> 00:34:45,680
more specifically giving up, not just sharing things in the sense that we still like them

353
00:34:45,680 --> 00:34:49,920
and we just want, and we're going to give up some of it, but we want some for ourselves.

354
00:34:49,920 --> 00:34:55,360
It means giving up everything, it's not a physical thing, it's not material giving up,

355
00:34:55,360 --> 00:35:02,120
it's giving up our attachments, not needing things to be other than what they are, and

356
00:35:02,120 --> 00:35:08,720
in the end giving up the whole universe, giving up everything, freeing ourselves from

357
00:35:08,720 --> 00:35:16,400
the fire of suffering, we're like sitting with fire all around us, and we're going to

358
00:35:16,400 --> 00:35:22,200
set the eyes on fire, the ears on fire that noses on fire, the tongue is on fire, it's

359
00:35:22,200 --> 00:35:29,720
on fire with craving, it's on fire with likes and dislikes, and it's on fire with impermanence,

360
00:35:29,720 --> 00:35:34,600
with the changes, if there's nothing left.

361
00:35:34,600 --> 00:35:39,840
But the thing about fire is if you don't, what we learn is the kid is if you don't touch

362
00:35:39,840 --> 00:35:47,200
it, it doesn't burn you, you have to make the movement, if you don't grab onto the fire

363
00:35:47,200 --> 00:35:54,000
it can burn you, if you don't come into contact with you, it can't make you suffer, and

364
00:35:54,000 --> 00:35:59,040
this is what the Buddha's meaning of the word suffering, Buddha said everything in the world

365
00:35:59,040 --> 00:36:07,760
is suffering, everything is suffering, basically, everything that arises is suffering.

366
00:36:07,760 --> 00:36:14,760
And that doesn't mean that sitting here is, seeing, hearing, smelling is a painful thing,

367
00:36:14,760 --> 00:36:20,400
what he meant is it's fire, when you grab it, when you hold onto it, when you need it,

368
00:36:20,400 --> 00:36:27,760
when you want it like it, hate it, so on, it can only make you suffer, there's nothing

369
00:36:27,760 --> 00:36:34,240
you can do with it that's going to make you happy, there's no adjustments you can make

370
00:36:34,240 --> 00:36:40,040
to the universe that are going to make it better. It's the adjusting, the messing, the forcing,

371
00:36:40,040 --> 00:36:46,960
the controlling that causes all the problems for us and for the world.

372
00:36:46,960 --> 00:36:58,640
And so Nervane is the real goal here, to let go of the fire, and to fly free like a bird

373
00:36:58,640 --> 00:37:08,640
rather than clinging on, the worried that we're going to fall. So that's the teaching

374
00:37:08,640 --> 00:37:12,640
for tonight, we'll look a little bit more extended, but that's a fairly important topic.

375
00:37:12,640 --> 00:37:18,640
I hope we've been able to broadcast that, and hopefully it'll be up on YouTube as well,

376
00:37:18,640 --> 00:37:23,640
so now we'll get back to our meditation practice, first mindful frustration and then we'll

377
00:37:23,640 --> 00:37:30,640
walk over to you today.

