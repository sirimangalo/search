1
00:00:00,000 --> 00:00:23,000
Vico said, in the how to meditate videos, you mentioned that in the beginning you can place your hand on the stomach if you can't feel the rising and falling, but is that advisable only for new meditators, or can it be done later also, or is it possibly considered a crutch and therefore not advisable?

2
00:00:23,000 --> 00:00:52,000
Sometimes crutches are advisable when you need them, so use it as a crutch when you don't feel your breath but not out of a deep state of concentration. It might happen that your body became so still and your mind becomes so still that you don't...

3
00:00:52,000 --> 00:01:17,000
Notice your breath anymore, it's going in and out, so subtle that you don't... you can't find it anymore, so in that case it would be really unadvisable to take the crutch and put your hand because by that you would go out of the deep concentrated mind state.

4
00:01:17,000 --> 00:01:35,000
But if for some reason you are too nervous or too agitated or your breath is going too fast for you to even be mindful to concentrate somehow, then you can put your hand back on the stomach and feel where your breath is going.

5
00:01:35,000 --> 00:02:04,000
Be mindful to even be mindful to concentrate somehow, then you can put your hand back on the stomach and feel where it is, feel that you calm down a little bit because often an agitated mind makes the body nervous and the breath fast and weigh around a fast breath.

6
00:02:04,000 --> 00:02:10,000
Can make the mind agitated because it's working together.

7
00:02:10,000 --> 00:02:33,000
And then just to qualify what the videos were, why I mentioned in the videos is really for new meditators because whether or not you do use it later on, the point for a new meditator is that new meditator specifically, I've never spent much time looking at their breath at their abdomen.

8
00:02:33,000 --> 00:02:50,000
So it's not a familiar object of attention. It's something that the mind is not used to focusing on. It's something that the mind is not able to even conceive of how it should be or where it should be or what it should be.

9
00:02:50,000 --> 00:03:02,000
So in order to give your mind an example or an idea of what the rising and falling is like, you put your hand on your stomach and you feel that rise.

10
00:03:02,000 --> 00:03:17,000
What often happens to new meditators is that they force so much the stomach that they actually make the stomach rise when the breath goes out and fall when the breath comes in.

11
00:03:17,000 --> 00:03:23,000
When the breath comes in the body, they let it out again.

12
00:03:23,000 --> 00:03:30,000
So it's a question that will ask meditators often in the beginning is the rising on the in breath or on the out breath.

13
00:03:30,000 --> 00:03:42,000
And surprisingly enough, many people will say the rising is on the out breath because that's when they let go, right? They're tensing up and then they let go.

14
00:03:42,000 --> 00:03:48,000
Which means that they really have no clue about what the breath is.

15
00:03:48,000 --> 00:03:57,000
So I used to do this to my Thai students. Some of them who would be quite living quite stressful lives.

16
00:03:57,000 --> 00:04:15,000
They're coming from reporting and saying, so this is the rising and the rising and the falling is the rising on the in breath or the out breath. And the rising is on the out breath.

17
00:04:15,000 --> 00:04:44,000
So if you have trouble answering that question in the beginning, it's a sign that you're not familiar enough or your mind is not familiar enough with the breath and also that you have an incredible amount of stress which most of us have if we haven't ever undertaken meditation breath.

18
00:04:44,000 --> 00:04:54,000
And so you need to jumpstart your mind and really give your mind a head start in understanding or in perceiving the rising and falling.

19
00:04:54,000 --> 00:05:06,000
You can even not just put your hands on your stomach. You can also lie on your back and then for sure you'll find that there's no way that the rising is on the out breath.

20
00:05:06,000 --> 00:05:15,000
The rising is on the in breath and the breath goes in. The stomach will naturally rise if you watch a baby that so it happens and if you lie down in your back, that's how it happens.

21
00:05:15,000 --> 00:05:26,000
The point of the beginner meditator using the hand is to become familiar with it which doesn't apply later on, but there could be other reasons why you might do that.

22
00:05:26,000 --> 00:05:42,000
Other reasons why you might even lie down in order to get more comfortable. If you have a lot of stress lying down can be a good way to help your your mind become more relaxed and more balanced.

23
00:05:42,000 --> 00:05:58,000
Okay.

