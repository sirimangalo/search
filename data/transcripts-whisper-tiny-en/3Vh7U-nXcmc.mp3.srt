1
00:00:00,000 --> 00:00:13,000
Do Buddhists believe in qi as Taoist do, and if so is there a method of generating this energy in oneself through meditation, etc.

2
00:00:14,000 --> 00:00:18,000
I feel like I've been seeking protection through meditation.

3
00:00:18,000 --> 00:00:28,000
Till I realize that I shouldn't be seeking protection, but maybe rather generating this power within myself.

4
00:00:29,000 --> 00:00:31,000
Do you think Paul?

5
00:00:34,000 --> 00:00:39,000
Protection through meditation, I think is definitely possible.

6
00:00:40,000 --> 00:00:43,000
I find it protects me a lot.

7
00:00:43,000 --> 00:00:52,000
It keeps me on even qi, so it uses the amount of suffering like I've been.

8
00:00:54,000 --> 00:00:59,000
I'm not sure if Buddhist believe in qi, I don't think I personally believe in qi.

9
00:01:04,000 --> 00:01:06,000
Some of the years...

10
00:01:06,000 --> 00:01:19,000
In some of the martial arts, they have some people teach this qi as some kind of energy, and some of the doctors just look at it as blood flow.

11
00:01:20,000 --> 00:01:28,000
I've seen that talk before where it's not a mystical energy, but it's a blood flowing in your body.

12
00:01:28,000 --> 00:01:37,000
But yeah, I don't ask for the energy part, I don't think that would be more of a Buddhist view.

13
00:01:38,000 --> 00:01:42,000
It's almost like you're saying it's a soul, or like a entity.

14
00:01:43,000 --> 00:01:53,000
All of these questions, all of these questions that start the Buddhists believe should be answered as a case three dominant.

15
00:01:53,000 --> 00:02:03,000
There's a wonderful video teaching that is, I think, quite famous out there, talking to you in Malaysia, I think Singapore or Malaysia, I'm not sure.

16
00:02:04,000 --> 00:02:09,000
Where he's talking about, because he wrote a book called What Buddhists Believe.

17
00:02:10,000 --> 00:02:20,000
And he said I was interviewed once by someone about this book, and he said, so you've written this book, What Buddhists Believe, so tell me, what do Buddhists believe?

18
00:02:20,000 --> 00:02:23,000
And he said, absolutely nothing.

19
00:02:24,000 --> 00:02:27,000
And the man said, then why did you write this book?

20
00:02:28,000 --> 00:02:36,000
And he said, and I told him, that's exactly why I wrote this book, to show you that there's nothing to believe.

21
00:02:37,000 --> 00:02:42,000
You don't need to believe Buddhists taught Buddhism, the Buddha taught to know, to understand.

22
00:02:42,000 --> 00:02:51,000
But I think we shouldn't miss this point that Buddhists don't believe anything, because literally it's very important.

23
00:02:52,000 --> 00:03:02,000
This kind of question is kind of awkward for a Buddhist, because we're like, well, how do you explain to this person that that's not what Buddhism is about?

24
00:03:02,000 --> 00:03:12,000
It's not about yes or no answers to questions of belief, it's about seeing the truth of mundane reality.

25
00:03:13,000 --> 00:03:22,000
It's about letting go rather than taking on some sort of belief in something external to the six senses.

26
00:03:22,000 --> 00:03:31,000
Buddhism is in fact about reducing reality to ultimate experience, to seeing hearing, smelling, tasting, feeling thinking.

27
00:03:32,000 --> 00:03:40,000
It's about getting rid of concepts like chi or even the breath, for example.

28
00:03:41,000 --> 00:03:50,000
The reason why we focus on the breath is to get rid of the concept of the breath, to come to see it as just physical and mental experiences, but the breath itself doesn't exist.

29
00:03:50,000 --> 00:04:03,000
So things like the chakras, things like chi, and so on and so on, and so on. Do you believe in this? Do you believe in God? Do you believe in ghosts? Do you believe in God?

30
00:04:03,000 --> 00:04:20,000
Goodness, don't believe in anything. We make every effort to stop believing in absolutely everything. And being facetious, because of course, the answer is yes, we believe in God, yes, we believe in ghosts, yes, we believe in X, Y, and Z.

31
00:04:20,000 --> 00:04:34,000
When I do, it's the same as the question of astrology. I mean, what you're asking is like, wow, now I have to have an opinion on this, now I have to go out and study chi, energy, and give you an answer, because it's not Buddhism.

32
00:04:34,000 --> 00:04:59,000
There's nothing Buddhist about me as to whether I believe in chi or not. It has more to do with whether I've investigated it and have any reason, totally apart from Buddhism to believe in it, because Buddha didn't teach this sort of thing, the Buddha taught, as I said, to give up our beliefs, to give up our obsession with concepts and to come to see ultimate reality for what it is.

33
00:04:59,000 --> 00:05:07,000
So, the only reason I might believe in chi is because I had studied it, which wouldn't have come about in Buddhism.

34
00:05:10,000 --> 00:05:20,000
So, I guess what I'd like to impress is an understanding of exactly what the Buddha taught and what Buddhism focuses on, so that people will not have to ask these sorts of questions.

35
00:05:20,000 --> 00:05:29,000
Because it's understandable, you think, wow, look at the Buddha's teaching is so big, I wonder if anywhere in there he taught this, or anywhere in there he taught that.

36
00:05:30,000 --> 00:05:46,000
But take it from me, don't take it from me in any basic understanding of the Buddha's teaching, or what it should give you, or what you should learn from Buddhism, is that it just doesn't apply.

37
00:05:46,000 --> 00:06:06,000
It has no place in the Buddha Dam, so, could chi exist, yeah, sure, I mean the Buddha may have even, I've never come across the Buddha, but he may have even talked about it, but it's not, I guess the point is that it's not really a question about Buddhism, isn't that, isn't that, is that it?

38
00:06:06,000 --> 00:06:18,000
Whether chi energy exists and you can use it for this or that, it's like asking whether nuclear power exists and whether you can use it for this or that, it's nothing to do with the path.

39
00:06:18,000 --> 00:06:37,000
So, I'm not trying to criticize the answer, I can understand where these questions come from, I'm trying to criticize the question, just trying to help to get back on.

