1
00:00:00,000 --> 00:00:02,400
Hi, welcome back to Ask a Monk.

2
00:00:02,400 --> 00:00:08,360
Today's question comes from Michael Art, who asks, how quickly should I switch between

3
00:00:08,360 --> 00:00:14,880
the different things, feelings, breathing, and so on that calls for my attention when meditating?

4
00:00:14,880 --> 00:00:20,440
In your how to meditate videos, you're doing it quite slowly, but is this only for demonstration

5
00:00:20,440 --> 00:00:21,440
purposes?

6
00:00:21,440 --> 00:00:27,800
No, I don't think it is just for demonstration purposes.

7
00:00:27,800 --> 00:00:34,320
It's not important how many times you say the word or that you catch everything.

8
00:00:34,320 --> 00:00:37,840
It's not important that everything that comes up, you have a label for it.

9
00:00:37,840 --> 00:00:41,960
What's important is that your mind is in this place, your mind is in this zone, your

10
00:00:41,960 --> 00:00:47,720
mind is clearly aware of things, and this word that we use is just to pull the mind

11
00:00:47,720 --> 00:00:56,840
back to that reality, to fix the mind, to straighten the mind, and keep the mind straight

12
00:00:56,840 --> 00:00:59,360
and aware of things as they are.

13
00:00:59,360 --> 00:01:03,880
You only need to do acknowledge maybe one time every second, and it's not a rhythmic thing,

14
00:01:03,880 --> 00:01:08,520
it's kind of just comfortable at a comfortable pace.

15
00:01:08,520 --> 00:01:15,560
So when you feel pain, just say to yourself, pain, pain, and it should be comfortable, there

16
00:01:15,560 --> 00:01:19,640
should be no feeling that you're saying it quickly, or you're saying it slowly, it should

17
00:01:19,640 --> 00:01:21,640
be just saying it.

18
00:01:21,640 --> 00:01:24,640
If you get the idea that you're saying it quickly, then yeah, probably it's too quick.

19
00:01:24,640 --> 00:01:27,640
If you get the idea that you're saying it slowly, probably it's too slow.

20
00:01:27,640 --> 00:01:32,320
It should be so natural that you don't even notice the speed, that there's no sense of

21
00:01:32,320 --> 00:01:34,240
slower or fast.

22
00:01:34,240 --> 00:01:41,080
It should be neutral, it should be just saying it when you feel, when you're thinking,

23
00:01:41,080 --> 00:01:47,000
thinking, thinking, thinking, just as you would normally speak, when you speak to someone,

24
00:01:47,000 --> 00:01:52,000
you speak at a certain rhythm, the acknowledgement should be done in that manner.

25
00:01:52,000 --> 00:02:00,080
As far as switching from object to object, this is also not productive in the meditation

26
00:02:00,080 --> 00:02:04,000
practice, it's not productive to try to go, okay, now I'm hearing, okay, now I'm seeing,

27
00:02:04,000 --> 00:02:10,640
okay, now it's on, pick one thing and focus on it, look at it clearly and just use it as

28
00:02:10,640 --> 00:02:14,880
a tool to train your mind to see things for what they are.

29
00:02:14,880 --> 00:02:17,720
It doesn't matter which thing you use, whatever's there in front of you, whatever's

30
00:02:17,720 --> 00:02:21,880
clearest, whatever your mind takes as the object.

31
00:02:21,880 --> 00:02:25,880
So suppose now I'm listening to the sounds outside, then I just focus on the sounds,

32
00:02:25,880 --> 00:02:28,320
I don't have to pay attention to everything else.

33
00:02:28,320 --> 00:02:32,840
When you're walking with your foot, you also feel your knee moving, your leg straightening,

34
00:02:32,840 --> 00:02:36,600
the tension and the movement in the back, the shifting and so on, but you don't have to

35
00:02:36,600 --> 00:02:41,280
acknowledge all of those things, the reason that you are aware of all of those is because

36
00:02:41,280 --> 00:02:46,760
you're saying step being right or lifting your foot, lifting, placing or so on, because

37
00:02:46,760 --> 00:02:51,040
you're focusing on the foot, you then are aware of everything as it arises quite clearly

38
00:02:51,040 --> 00:03:00,520
and you don't have to catch everything as long as you're keeping your mind in this mode,

39
00:03:00,520 --> 00:03:05,120
bringing your mind back again and again, until finally the mind starts to look at things

40
00:03:05,120 --> 00:03:08,920
naturally in this way, because that's what's going to happen.

41
00:03:08,920 --> 00:03:12,800
Once you develop this to a great extent, you'll find that it just comes naturally, you're

42
00:03:12,800 --> 00:03:13,800
just aware of things.

43
00:03:13,800 --> 00:03:18,120
When you move moving, when you brush your teeth, brushing, when you eat your foot chewing,

44
00:03:18,120 --> 00:03:25,400
the scooping, chewing, swallowing and so on, and you don't need to use the words.

45
00:03:25,400 --> 00:03:29,280
The word is the training that we do in meditation and if you keep training yourself and

46
00:03:29,280 --> 00:03:34,400
training, you'll get to such a high level that your mind just lets go and you're able

47
00:03:34,400 --> 00:03:40,200
to see things clearly and it's such a letting extreme letting go that the mind actually

48
00:03:40,200 --> 00:03:46,760
leaves behind the experiential world and enters into a state of Nubana through this practice.

49
00:03:46,760 --> 00:03:54,880
So the state that we're aiming for is not this constant labeling, but the labeling is what

50
00:03:54,880 --> 00:04:01,600
increases our awareness and if we can use the labeling again and again and again and clearing

51
00:04:01,600 --> 00:04:07,200
our mind to a higher and higher degree, we can eventually be able to let go of everything

52
00:04:07,200 --> 00:04:12,960
and so that our mind is able to experience true freedom.

53
00:04:12,960 --> 00:04:18,840
So not quickly, just try to do it naturally, that's the short answer.

54
00:04:18,840 --> 00:04:35,640
Thanks for the question, keep practicing.

