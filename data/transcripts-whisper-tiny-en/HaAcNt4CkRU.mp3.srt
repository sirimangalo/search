1
00:00:00,000 --> 00:00:04,880
Hello, welcome back to Ask a Monk.

2
00:00:04,880 --> 00:00:14,120
Today I will be talking about dealing with terminally ill patients, hospice care, and some

3
00:00:14,120 --> 00:00:24,960
of the ways that I would think one can approach people in this condition and help them

4
00:00:24,960 --> 00:00:28,800
through the practice of the meditation.

5
00:00:28,800 --> 00:00:40,520
The first thing to note or to discuss is in regards to general practices in terms

6
00:00:40,520 --> 00:00:47,320
of teaching meditation, whether it's terminally ill, it doesn't just relate to this specific

7
00:00:47,320 --> 00:00:52,520
group of people, but in any case when you're teaching meditation there are certain

8
00:00:52,520 --> 00:00:57,040
principles that you have to keep in mind.

9
00:00:57,040 --> 00:01:03,320
The first one is that any time that you're teaching meditation you should also be mindful

10
00:01:03,320 --> 00:01:04,320
yourself.

11
00:01:04,320 --> 00:01:13,640
That's probably the most important principle to keep in mind that it's not simply, or

12
00:01:13,640 --> 00:01:23,200
it's not good enough to simply know the principles and to have book theory.

13
00:01:23,200 --> 00:01:29,520
In order to teach, in order to be effective, in order to really be able to respond and interact

14
00:01:29,520 --> 00:01:36,400
with the patient, the student, one has to be practicing.

15
00:01:36,400 --> 00:01:41,760
So at the time when you're teaching, at the time when you're speaking even, you should

16
00:01:41,760 --> 00:01:45,720
try to remind yourself of the things that you're saying.

17
00:01:45,720 --> 00:01:55,360
So otherwise you fall into all sorts of biases and emotions and you become stressed

18
00:01:55,360 --> 00:02:02,640
yourself when the person doesn't respond favorably, or you can get caught up in your

19
00:02:02,640 --> 00:02:12,480
own emotions, go overboard, you can become overconfident and overbearing, and it becomes

20
00:02:12,480 --> 00:02:18,240
difficult to remain objective and impartial.

21
00:02:18,240 --> 00:02:24,880
And in part, the truth, I mean the truth is something that is very difficult to understand

22
00:02:24,880 --> 00:02:33,480
and very difficult to keep in mind, it's easy for us to slip off into illusion and fantasy

23
00:02:33,480 --> 00:02:39,360
and give the wrong advice when we're teaching we want to give something useful, we want

24
00:02:39,360 --> 00:02:47,640
to be appreciated and so if we're not careful, we can easily slip into bias and prejudice

25
00:02:47,640 --> 00:02:55,520
and find ourselves just saying something that sounds good or that it's going to give

26
00:02:55,520 --> 00:03:02,560
a favorable response from our listeners, which you may not always be in line with the truth.

27
00:03:02,560 --> 00:03:09,360
It's something that sounds good, a feel-good sort of philosophy, it's not good enough

28
00:03:09,360 --> 00:03:17,400
and that leads to the second point that in general I would recommend for anyone who's

29
00:03:17,400 --> 00:03:21,440
going to teach meditation besides practicing by yourself.

30
00:03:21,440 --> 00:03:30,040
Have it clearly in mind that your approach to it should be a meditation, it should be

31
00:03:30,040 --> 00:03:37,160
a meditative interaction with the person and the only thing that you're giving to the person

32
00:03:37,160 --> 00:03:39,160
should be information.

33
00:03:39,160 --> 00:03:45,280
You shouldn't be giving them yourself, it shouldn't be about you or your guru status,

34
00:03:45,280 --> 00:04:01,760
you shouldn't be giving them an image or a presentation of some charismatic or impressive

35
00:04:01,760 --> 00:04:02,760
personality.

36
00:04:02,760 --> 00:04:12,000
The whole idea of a guru ship or impressing something on your students is quite dangerous

37
00:04:12,000 --> 00:04:23,240
and it doesn't have the desired effect, you can impress someone emotionally and I've

38
00:04:23,240 --> 00:04:29,360
tried this or if you try it you can see for yourself the results, the result is people

39
00:04:29,360 --> 00:04:33,480
have great faith in you but they don't really understand what you're saying, they put

40
00:04:33,480 --> 00:04:41,840
more emphasis on the messenger rather than the message and so it's easy to get caught

41
00:04:41,840 --> 00:04:48,680
up in this as well, giving yourself as the message and pumping people up and giving them

42
00:04:48,680 --> 00:04:55,000
confidence and encouragement but not really giving them any skills, much more useful than

43
00:04:55,000 --> 00:05:02,920
giving an image or confidence or encouragement and being a support for people to lean on

44
00:05:02,920 --> 00:05:06,600
is helping them be a support for yourself, that's I guess the third thing I would say is

45
00:05:06,600 --> 00:05:12,720
that you're not there to be a support for them, you're there to teach them how to support

46
00:05:12,720 --> 00:05:21,360
themselves which is very often overlooked, we're much better at or much more inclined

47
00:05:21,360 --> 00:05:26,800
to be a support for others and have them lean on us and depend on us which as I'm going

48
00:05:26,800 --> 00:05:33,800
to explain is really a part of the problem, the dependency, once they can be strong which

49
00:05:33,800 --> 00:05:39,680
is really the problem that we face because we've gone our whole lives without building

50
00:05:39,680 --> 00:05:45,360
ourselves up without strengthening our minds and giving ourselves the ability to deal

51
00:05:45,360 --> 00:05:52,000
with difficulty and so when it comes we're still children, we act like young children,

52
00:05:52,000 --> 00:05:58,560
we can cry, grown adults will cry and moan and just want to take drugs and medication

53
00:05:58,560 --> 00:06:10,200
and so on, so what you should be there to give is information, now the point about meditating

54
00:06:10,200 --> 00:06:14,040
is that you'll give the right information and the information you give will be unbiased

55
00:06:14,040 --> 00:06:21,240
impartial and will be a service to the person, like you're there as a book or you're

56
00:06:21,240 --> 00:06:30,440
there as a kind of like a coach, someone to stop you when you're going in the wrong direction

57
00:06:30,440 --> 00:06:35,000
and push you in the right direction and encourage you, but because you're mindful because

58
00:06:35,000 --> 00:06:42,320
you're there, you're present, you're in the present moment, you're able to respond and

59
00:06:42,320 --> 00:06:48,480
you're able to catch the emotions of the other person and you're able to see the state

60
00:06:48,480 --> 00:06:54,680
of their mind feel and experience it and react appropriately and interact with the situation

61
00:06:54,680 --> 00:07:02,800
without tangling your own emotions up in it, so stay mindful but don't give impressions

62
00:07:02,800 --> 00:07:10,240
given information and continue to give information and constantly give unbiased information

63
00:07:10,240 --> 00:07:15,520
telling people the results of this act, not saying them don't do this, don't do that

64
00:07:15,520 --> 00:07:20,000
but you know, explaining to them why it's better to do this and better to do that and

65
00:07:20,000 --> 00:07:25,280
so on, so these are general principles that I would follow in terms of the meditation

66
00:07:25,280 --> 00:07:33,720
first, be mindful, second try to give information rather than some kind of feeling because

67
00:07:33,720 --> 00:07:37,120
it's the information that they will use and it's the technique that they will use, it

68
00:07:37,120 --> 00:07:43,360
has to come from their feeling, it has to be their emotion, it has to be their volition

69
00:07:43,360 --> 00:07:49,960
and their courage in themselves, they have to be looking for it, if you are pushing the

70
00:07:49,960 --> 00:07:56,520
meditation on them and they're not there, it's not coming from their heart, they'll

71
00:07:56,520 --> 00:08:06,240
do it to keep you there, to impress you and so on and when you're gone and even when

72
00:08:06,240 --> 00:08:10,080
you're there, it won't be from the heart and it won't have the desired results, so you

73
00:08:10,080 --> 00:08:17,680
have to be, you have to be step back and let them let their heart come forth, let their

74
00:08:17,680 --> 00:08:30,720
intention come forth and thirdly that is the point is to make themselves reliant, so by

75
00:08:30,720 --> 00:08:38,400
stepping back, by just giving them information and not bringing your own ego into the equation,

76
00:08:38,400 --> 00:08:44,360
you allow them to step up to the plate and take their own future into their hands.

77
00:08:44,360 --> 00:08:51,080
But this is more pronounced I think with people who are in great suffering, first of

78
00:08:51,080 --> 00:08:56,840
all the one thing I'd say about people who are in great suffering, there's an advantage

79
00:08:56,840 --> 00:09:01,440
and a disadvantage that they have, the advantages they see things that many of us don't

80
00:09:01,440 --> 00:09:07,720
see, they are experiencing the reason for practicing meditation, we practice one of the

81
00:09:07,720 --> 00:09:19,280
great reasons anyway is because of the eventuality or the danger that we all face that

82
00:09:19,280 --> 00:09:24,360
we might be in this situation at some point, well they're in it and they can see the need

83
00:09:24,360 --> 00:09:28,720
to meditate or the need to do something, they're looking for a way out of suffering, they

84
00:09:28,720 --> 00:09:33,880
have the suffering that many of us are blind to, we forget exists and so as a result

85
00:09:33,880 --> 00:09:41,800
we have no strength or fortitude of mind and when it comes, we like them, like people

86
00:09:41,800 --> 00:09:47,600
who are in it already, we're unable to deal with it, so they're looking for it, this

87
00:09:47,600 --> 00:09:52,240
is the advantage is that the people who are suffering greatly at the end of their lives,

88
00:09:52,240 --> 00:10:01,360
people who are terminally ill, they're not looking to pass the time or to seek entertainment

89
00:10:01,360 --> 00:10:04,880
and so on, they have a problem and they're looking to fix it, they're looking to find

90
00:10:04,880 --> 00:10:11,440
the solution, so they can be a really good audience in this sense and this is why I often

91
00:10:11,440 --> 00:10:16,520
giving, in this case giving information is often enough, but it has to be confident information

92
00:10:16,520 --> 00:10:21,480
and you have to be able to give it in a way that they understand, you're not just giving

93
00:10:21,480 --> 00:10:25,560
them a book to read or you're not just reading a book to them, you're explaining to them

94
00:10:25,560 --> 00:10:29,000
and you're going through it, I'll try to explain basically what the sort of things that

95
00:10:29,000 --> 00:10:31,440
I would teach.

96
00:10:31,440 --> 00:10:35,240
Now the disadvantage with teaching people who are terminally ill and so on is because

97
00:10:35,240 --> 00:10:46,280
you're often fighting with an alternative or a different way of treatment, so the treatment

98
00:10:46,280 --> 00:10:57,680
in hospitals, which quite often has to do with medication, will very often get in the way

99
00:10:57,680 --> 00:11:04,480
and it's going to be something that you'll have to work around and it's always going

100
00:11:04,480 --> 00:11:10,200
to lessen the effects of the meditation, if people who are on medication who have a very

101
00:11:10,200 --> 00:11:19,160
difficult time, medication based on or a medication of the sort that is meant to relieve

102
00:11:19,160 --> 00:11:27,400
pain or dull the pain is a really hindrance to meditation practice because not only does

103
00:11:27,400 --> 00:11:36,360
it dull the mind and muddle the mind but it also reinforces the avoidance of the difficulty

104
00:11:36,360 --> 00:11:44,560
just like alcohol or recreational drugs, it's a form of escape and so not only does

105
00:11:44,560 --> 00:11:50,800
it hurt the body and affect our brain's ability to process information but it also affects

106
00:11:50,800 --> 00:11:56,080
the mind's willingness and ability to deal with pain and suffering, it sends you on the

107
00:11:56,080 --> 00:12:03,120
wrong direction, so I would say one of the first things that you should explain to people

108
00:12:03,120 --> 00:12:08,160
who are on medication or who are in this position, hopefully people who haven't yet decided

109
00:12:08,160 --> 00:12:12,680
what form of treatment they're going to take, is explaining the differences in the treatment

110
00:12:12,680 --> 00:12:21,640
that yes, medication is something that is going to solve the problem in the short term

111
00:12:21,640 --> 00:12:29,080
but in the long term it's going to lead to a dependence and a addiction and the only way

112
00:12:29,080 --> 00:12:36,400
to have it effectively work is to drug you up to the point where you're unconscious because

113
00:12:36,400 --> 00:12:40,600
it's going to have less and less of effect, your body is going to become more tolerant

114
00:12:40,600 --> 00:12:45,360
to the drugs and so you'll need more and more and so on, the pain is going to become

115
00:12:45,360 --> 00:12:51,120
more intense and your aversion to the pain will become more intense, I had a direct experience,

116
00:12:51,120 --> 00:12:56,280
my grandmother was quite ill and they'd drug her up in a nursing home to the point where

117
00:12:56,280 --> 00:13:01,800
she couldn't even recognize people because this was their way of dealing with it, it was

118
00:13:01,800 --> 00:13:07,960
laziness and it was sloppiness and it was negligence, when she had pain and she complained

119
00:13:07,960 --> 00:13:12,680
about it they just increased her medication rather than going to see a doctor so they just

120
00:13:12,680 --> 00:13:19,080
increased it to the point where she'd still had no pain but she was more or less unconscious

121
00:13:19,080 --> 00:13:23,640
to the world around her and when this was realized they took her off the medication but

122
00:13:23,640 --> 00:13:30,400
then she was an incredible suffering, she wasn't able to deal with this pain that she had

123
00:13:30,400 --> 00:13:38,840
been learning and teaching herself to avoid, to run away from, to stop, to escape through

124
00:13:38,840 --> 00:13:45,680
the medication and as a result she was in great and terrible suffering that was very

125
00:13:45,680 --> 00:13:54,600
difficult for her to deal with it, so we should explain this and try to make it clear

126
00:13:54,600 --> 00:14:02,800
that there are alternatives that it's actually in our better interest to come to terms

127
00:14:02,800 --> 00:14:07,640
with the pain and to die with a clear mind if we're going to die that our minds should

128
00:14:07,640 --> 00:14:13,280
be clear we should know what we're doing, we should have the ability to find closure

129
00:14:13,280 --> 00:14:20,400
with our relatives and so on with a clear and alert mind and give confidence that there

130
00:14:20,400 --> 00:14:28,800
is another way and explain that basically the theory of the meditation and so here goes

131
00:14:28,800 --> 00:14:33,880
exactly the sorts of things that I would say to them, I would talk to them, get right to

132
00:14:33,880 --> 00:14:42,320
the point about pain and say that this is going to be our training in the meditation

133
00:14:42,320 --> 00:14:48,080
practice, to deal with the pain or to approach the pain and to change the way we look

134
00:14:48,080 --> 00:15:01,560
at the pain, so you explain first about the nature of feelings that the physical feeling

135
00:15:01,560 --> 00:15:07,880
is actually not really the problem, the problem is our inability to accept it, the fact

136
00:15:07,880 --> 00:15:14,240
that it bothers us and explain to people how when you actually look at the pain and when

137
00:15:14,240 --> 00:15:18,320
you're actually there with it, you can see that there's two things, there's the pain

138
00:15:18,320 --> 00:15:23,520
and then there's your aversion to the pain and these are quite separate and encourage

139
00:15:23,520 --> 00:15:30,400
people to look at it, to examine it and to become comfortable with it, explaining that

140
00:15:30,400 --> 00:15:36,320
as you look at the pain and as you examine it, you're able to see that it's something that

141
00:15:36,320 --> 00:15:43,080
comes and goes, it actually has no effect on the mind, the problem is that we've developed

142
00:15:43,080 --> 00:15:50,160
this wrong idea that there's something bad about the pain based on our worries and our

143
00:15:50,160 --> 00:16:00,160
feeling that somehow it's going to lead to injury or it means something more significant

144
00:16:00,160 --> 00:16:06,880
in terms of an illness or causing death, so eventually to the point where any little pain

145
00:16:06,880 --> 00:16:12,480
causes suffering in our mind, we can explain to people that we're actually causing the

146
00:16:12,480 --> 00:16:21,680
suffering ourselves by our aversion to the pain, so we explain to people the relationship

147
00:16:21,680 --> 00:16:26,720
between the experience and the suffering, how in the beginning there is the tension and

148
00:16:26,720 --> 00:16:33,920
the pressure in the body, the body being in a specific position and the pressure build

149
00:16:33,920 --> 00:16:39,880
up and the stiffness build up and so on, and then there arises this painful feeling,

150
00:16:39,880 --> 00:16:44,000
now once there arises the painful feeling, the mind picks it up and starts to run away

151
00:16:44,000 --> 00:16:52,080
with it, now it depends how the state of mind of the person, how much you want to go

152
00:16:52,080 --> 00:16:56,600
into detail, but at the very least you should be able to explain to them that the first

153
00:16:56,600 --> 00:17:01,120
of all that the feelings are going to be our focus, that what, you know, make clear to

154
00:17:01,120 --> 00:17:05,240
them what we're going to be dealing with, what the meditation is designed to do is design

155
00:17:05,240 --> 00:17:11,120
to help us to work through the feelings and to come to let go of them and actually overcome

156
00:17:11,120 --> 00:17:18,280
them to the point that they don't bother us, and explaining how we're going to do that,

157
00:17:18,280 --> 00:17:23,240
that we're going to actually focus on the pain and try to remind ourselves the nature

158
00:17:23,240 --> 00:17:29,120
of the pain, you know, it's just a pain or a feeling, and so we use this word that we

159
00:17:29,120 --> 00:17:35,200
have, explained to them the technique of meditation, you know, the word mantra, if they've

160
00:17:35,200 --> 00:17:38,960
ever heard of this, or if not explained to them what a mantra is, it's something that

161
00:17:38,960 --> 00:17:44,880
helps you focus on an object, something that helps you to come to see only the object

162
00:17:44,880 --> 00:17:54,720
and to keep other things from interfering, from distracting you, so to keep yourself focused

163
00:17:54,720 --> 00:17:56,520
on a single object.

164
00:17:56,520 --> 00:18:03,560
Now what that does is keeps you focused on simply on the pain, now reminding people that

165
00:18:03,560 --> 00:18:08,400
the pain is not the problem, once you're focused on just the sensation for itself, what

166
00:18:08,400 --> 00:18:13,280
you come to realize is that it's actually not a negative experience, it's not something

167
00:18:13,280 --> 00:18:18,960
unpleasant, and what doesn't have a chance to come in is this judgment, this disliking,

168
00:18:18,960 --> 00:18:25,000
this upset about the pain, we're going to be able to separate the feelings of upset and

169
00:18:25,000 --> 00:18:31,960
stress and anger and frustration about the sadness from the actual pain itself and just

170
00:18:31,960 --> 00:18:35,920
experience the pain for what it is, and you can reassure them that it works, tell them

171
00:18:35,920 --> 00:18:39,520
that, you know, you've done this and so on, and they should try it for themselves if it

172
00:18:39,520 --> 00:18:45,920
doesn't work, they can, they're welcome to stop and try the medication, so basically saying

173
00:18:45,920 --> 00:18:49,560
to people that, you know, we want to try this as an alternative and see what you think

174
00:18:49,560 --> 00:18:57,120
and if it doesn't work, you've always got the medication to go back up on as a backup.

175
00:18:57,120 --> 00:19:01,560
Now once you've explained this, we can, you know, this is the theory, this is something

176
00:19:01,560 --> 00:19:05,080
that they have to keep in mind, then you can start them on something simpler, say, you

177
00:19:05,080 --> 00:19:09,840
know, this mantra, this word that we use, the idea is to allow us to see things clearly,

178
00:19:09,840 --> 00:19:16,600
so we have to, we teach people to start practicing it and find a simple object to practice

179
00:19:16,600 --> 00:19:17,840
it on.

180
00:19:17,840 --> 00:19:23,160
The basic object of our contemplation is many of you are aware is the stomach, and this

181
00:19:23,160 --> 00:19:28,520
is used very useful for people who are in bed, lying down, when you're lying down and

182
00:19:28,520 --> 00:19:33,840
you're relaxed, you'll find that the stomach is quite evident and easy to follow, and

183
00:19:33,840 --> 00:19:39,680
so we watch it rising, falling, rising, falling, and keep our mind on it, and we have

184
00:19:39,680 --> 00:19:44,840
this word that allows our mind to focus only on the stomach, when it rises, say to yourself

185
00:19:44,840 --> 00:19:51,680
rise, when it falls, say to yourself, and your mind will focus on that object and that

186
00:19:51,680 --> 00:19:58,840
object only, if it won't, nothing else will come into distract it, or your mind won't

187
00:19:58,840 --> 00:20:02,080
be flitting here and there, your mind will become focused and concentrated, and then

188
00:20:02,080 --> 00:20:08,720
for a time, you'll find that you're able to find peace and clarity of mind.

189
00:20:08,720 --> 00:20:15,000
Now as you do that, you'll find that from time to time, pain arises, and especially

190
00:20:15,000 --> 00:20:20,320
if you're terminial, if you have a sickness and so on, you'll find that that is sharp

191
00:20:20,320 --> 00:20:22,800
and unpleasant sensation to rise.

192
00:20:22,800 --> 00:20:26,920
Now it's at that point, and you can lead people through this, you can have them start

193
00:20:26,920 --> 00:20:31,240
to watch the stomach and then they start to explain as they're practicing, as they're

194
00:20:31,240 --> 00:20:35,680
watching the stomach, explain to them about the sorts of things that might arise, the

195
00:20:35,680 --> 00:20:41,440
most prominent being the feelings, the pain, and so on, and say when the pain arises,

196
00:20:41,440 --> 00:20:45,680
try, we can always go back to the medication, reassuring them that they don't, and this

197
00:20:45,680 --> 00:20:51,280
isn't going to be, we're not trying to torture them, but reassure them to try first to

198
00:20:51,280 --> 00:20:58,720
acknowledge pain, and just remind themselves of the pain and keep their minds on the pain.

199
00:20:58,720 --> 00:21:03,800
Because the truth is it's only when their mind slips away from the pain into judgment,

200
00:21:03,800 --> 00:21:12,080
into aversion, into dislike, or of the pain, that it begins to give rise to thoughts

201
00:21:12,080 --> 00:21:18,480
of how to get away and how to change it, how to find some way, any way to escape the situation.

202
00:21:18,480 --> 00:21:23,280
As long as you're focusing on the situation, seeing it for what it is, it doesn't even

203
00:21:23,280 --> 00:21:26,800
enter your mind that you have to change the situation, it doesn't enter your mind that

204
00:21:26,800 --> 00:21:28,920
this is a bad situation.

205
00:21:28,920 --> 00:21:34,320
There's no room for that, your mind is full of, fully aware, it's fully focused on the

206
00:21:34,320 --> 00:21:35,320
object itself.

207
00:21:35,320 --> 00:21:40,640
You don't have to explain all this to them, but you know, have them try it, have them

208
00:21:40,640 --> 00:21:45,840
see for themselves whether this is true or not, what the result is.

209
00:21:45,840 --> 00:21:50,480
I tried this with a woman who had stomach cancer, and she had had it for about seven years,

210
00:21:50,480 --> 00:21:55,440
she was in her final stages, and so she was on some pretty heavy pain medication, and

211
00:21:55,440 --> 00:21:58,560
right before she was going to take it, I said, well, let's try this, and this is exactly

212
00:21:58,560 --> 00:21:59,560
what I did with her.

213
00:21:59,560 --> 00:22:04,640
I led her through the watching the stomach rising, falling, and then she was suddenly

214
00:22:04,640 --> 00:22:07,840
this pain arose in her stomach, and she wanted to take her medication, and they said,

215
00:22:07,840 --> 00:22:14,640
well, try this first, you know, see what it does, say to yourself pain, and so she closed

216
00:22:14,640 --> 00:22:22,160
her eyes, and she tried, and I guess she tried it, she said to herself pain, and then

217
00:22:22,160 --> 00:22:33,440
she fell asleep, she passed out into unconscious, probably because she had not been sleeping

218
00:22:33,440 --> 00:22:37,400
very well due to the pain and the suffering, but as a result, she didn't have to take the

219
00:22:37,400 --> 00:22:41,840
medication at that time, now I didn't have time to stay with her, and she obviously

220
00:22:41,840 --> 00:22:47,280
would have stayed on the medication, but if you can keep up with this, if you can help

221
00:22:47,280 --> 00:22:55,640
people through it, and explain to people, again, and again, and keep encouraging them

222
00:22:55,640 --> 00:22:59,080
through it, and leading them through it, and meditating with them and saying, let's try

223
00:22:59,080 --> 00:23:05,680
it again, eventually you can, if not wean them off the medication, you can at least help

224
00:23:05,680 --> 00:23:12,560
them to keep the medication out of minimum, or only when it's incredibly severe, and

225
00:23:12,560 --> 00:23:20,320
in fact, as I said, these sorts of people are prime, can be prime material for the meditation

226
00:23:20,320 --> 00:23:26,360
practice, and can gain great insights and wisdom, they can actually mitigate some of the

227
00:23:26,360 --> 00:23:31,000
effects of illness, and some people are able to overcome the illness as a result of the

228
00:23:31,000 --> 00:23:36,520
easing up of the tension and the body's better ability to heal itself, but at the very

229
00:23:36,520 --> 00:23:44,880
least they'll be able to deal with the end of life, and they should find that, especially

230
00:23:44,880 --> 00:23:49,320
with the help of someone who's done it before, and who's encouraging them through it,

231
00:23:49,320 --> 00:23:57,480
and reminding them about these things, that they, you know, their whole attitude will

232
00:23:57,480 --> 00:24:04,760
change, and they will become much, you know, in many ways a new person, much better

233
00:24:04,760 --> 00:24:10,680
able to deal with the difficulties of the problem, so I think that's my cue to finish,

234
00:24:10,680 --> 00:24:18,520
I'd like to thank you all for listening, for tuning in, and I hope this helps too, and

235
00:24:18,520 --> 00:24:22,840
I hope that people are able to use this, for helping people in these sorts of situations,

236
00:24:22,840 --> 00:24:38,440
so thanks for tuning in, all of that.

