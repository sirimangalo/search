One of my afraid of women, police officers, why am I afraid of women, it's not a fear,
it's a sort of awareness.
And I say that sort of tongue in cheek.
But, you know, we're talking about people who maybe are not Buddhist
or who are not stable in their mind.
Or, you know, you could say ordinary people, but they can be
sometimes a little bit extreme on the side of attachment.
And you get a lot of these.
So you find people who cling to you.
And that can be quite difficult as a monk.
You know, you've got to be nice to people and kind,
but sometimes you can be really, really kind to people
and they can fall into some kind of attachment towards you.
And I suppose the other thing is just how, you know,
you get in so much trouble as a monk.
Like sometimes you'd be alone in a place with a woman
and someone sees you and suddenly the word gets around
that you're sleeping with some with a woman.
And so on.
So you have to be really careful.
That's the first thing.
The second thing.
Why am I afraid of police officers?
Well, I'm living in America now.
I'm not living in a Buddhist country.
And so not to say anything.
Actually, the problem isn't really police officers.
It's more people who, you know, for whatever reason,
through fear of the unknown or through just overall paranoia,
caution, I don't know.
You can often get overreact towards strange things,
like things that seem strange to them like a monk.
So last year I was put in jail.
Someone called the police and claimed that I had,
I was walking around in the forest playing with myself
or opening my robes up and showing my private parts to people.
And by the time I talked to the police,
it had escalated into this story where I had run around
showing, you know, opening my robes
and showing my private parts to a man and his daughters.
And I just crazy stuff.
And there was a whole crowd of people that apparently saw me.
I was trying to think if maybe I had gone unconscious
or something.
The killer turns out to be the innocent guy
because he did it while he was in some kind of state.
So I was thinking, wow, you know, I mean, is it possible
that I could have happened?
But what I was doing, I was meditating off in the forest,
but it happened to be near a public beach.
You know, they in the coast of California,
by the ocean, they have these wonderful parks,
but it's also near a public beach.
I didn't go to the beach.
But there was, you know,
there's been many, many, many example
or many cases up there of streakers
because there's a new just colony up the road
or something like that.
So they get these kind of things all the time,
apparently, which it was totally unbeknownst to me.
I was just looking for a nice place to meditate.
And I happened to make a video.
I had set my camera up.
I think that's what this guy saw.
He saw me walking around looking for a good place
to set up my video,
and I must have looked like a crazy person
walking around in circles.
And so I spent a nice evening in the local,
the state.
I think the state pen attention.
I don't know what it was.
Some kind of state prison, just in a holding cell,
until someone bailed me out.
It was freaky.
After that, I've had quite a phobia of police officers.
And just recently, I was stopped by a police officer here.
On my arms round, because he'd had someone call in
and ask me who I was and what I was holding
and what I was hiding underneath my robes
because I carry this big bowl around.
So that's the second thing I'm afraid of.
But I suppose those aren't real fears.
I think something that I could really say
that I'm a fear of is fear of failure
that I recognize this in myself.
I don't think it's totally unhealthy.
I think some sort of fear of failure
is good to propel you on.
But it's the idea that somehow I might lose my grasp
on the path.
So I'm often watching myself
and concerned that some of the things
that I have to engage in like teaching
might get in the way or take time away from my own practice.
And so I often worry about that.
Because it's something that really used to devastate me.
Something that was really a phobia of mine
would be disrobing.
When I ordained 18 monks
ordained on the same day at the same time,
we had to ordain 18 of us three at a time,
six times, and I was in the last group.
And then after I ordained,
I left Thailand to go back to Canada.
When I came back to Thailand again,
I was the only one left out of 18.
The rest of them had all disrobed.
So the survival rate is very low.
And most of them are ordaining for whatever reason,
just for cultural reasons.
Still, you see some pretty powerful monks disrobing.
And so I've never had any thought
that I want to disrobed at all,
never, not once in my almost 10 years as a monk.
And yet, everyone's saying, oh, you'd be careful.
And you never know.
And suddenly it comes up.
And you see these people who've been monks for 20 years.
And then suddenly they want to disrobed.
So I used to have this dream.
I'd wait, you know those dreams where you're in school.
And you're not wearing clothes.
This is a common psychological dream
that Freud or Yang or whoever talked about.
It's a common phobia people have.
You're going to school, or you're going to work,
and you're not wearing your underwear.
Well, the dream I had would be that I'd been going out.
And I was in clothes.
I was wearing clothes.
I wasn't wearing a monk's robe.
And I was devastated because it meant that I disrobed.
It meant that I was no longer a monk.
And I was freaking out.
And just so sad and so upset about that.
And then I'd wake up and realize, oh, I'm still a monk.
Still, I didn't disrobed.
And that happened several times in the first couple of years
that I was a monk.
Because it's something that's very important to me.
And to lose it, just this fear that people instill in you
that watch out, you never know.
You're going to suddenly want to disrobe
and you're not going to be able to.
I don't know.
It's a long haul.
You're talking about a training that last years.
We're talking about a lifelong training.
So the point is, you don't know what could happen.
So it's something to always be on the watch out for.
So that's something I think that could be close to be called a fear.
Certainly was a fear fear in the beginning.
I suppose I'm not so afraid of it now.
But, well, there's the question and there's my answer.
So I hope that's useful.
That's the latest installment in life in a day.
Have a month.
