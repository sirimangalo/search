WEBVTT

00:00.000 --> 00:03.040
Hello, welcome back to Ask a Monk.

00:03.040 --> 00:07.760
Today's question comes from Pedro C, who asks,

00:07.760 --> 00:10.880
people tend to think one can learn from errors.

00:10.880 --> 00:13.760
However, only seldom this is true.

00:13.760 --> 00:15.560
What makes us learn from mistakes,

00:15.560 --> 00:19.240
and when do we ignore these lessons?

00:19.240 --> 00:20.040
Good question.

00:23.360 --> 00:28.560
The Buddha was pretty clear about this sort of thing

00:28.560 --> 00:34.040
that it's not true that a person who suffers

00:34.040 --> 00:37.240
is necessarily going to be any wiser than a person

00:37.240 --> 00:38.840
who does not suffer so much.

00:38.840 --> 00:41.760
A person who makes a lot of mistakes

00:41.760 --> 00:45.160
is not necessarily going to learn more.

00:45.160 --> 00:47.840
However, we're all quite aware that it's

00:47.840 --> 00:51.400
a chance to learn something, because obviously,

00:51.400 --> 00:53.920
the learning from one's mistakes is how you avoid

00:53.920 --> 00:57.040
making them in the future.

00:57.040 --> 00:59.720
The difference is why it's attention, something

00:59.720 --> 01:02.400
called the Oni so Manasikara.

01:02.400 --> 01:09.120
Manasikara means to keep in mind or to pay attention

01:09.120 --> 01:12.400
to Manas means in the mind.

01:12.400 --> 01:15.880
Cara means to make Manasi in the mind.

01:15.880 --> 01:20.760
Cara to make, to establish in the mind wisely,

01:20.760 --> 01:24.920
or to set your mind on something with wisdom.

01:24.920 --> 01:31.920
Oni so literally translates, going back to the origin.

01:31.920 --> 01:35.160
So it means when something occurs,

01:35.160 --> 01:38.800
when something happens, the ability to examine it

01:38.800 --> 01:42.240
and reflect upon it, and to see it clearly,

01:42.240 --> 01:48.920
which obviously is the practice of meditation

01:48.920 --> 01:53.240
when you are meditating, the word meditation,

01:53.240 --> 01:55.280
that's really what it means.

01:55.280 --> 01:58.800
It means the measurement, the examination,

01:58.800 --> 02:02.240
the analysis, and the understanding

02:02.240 --> 02:05.120
of a situation, of a reality.

02:05.120 --> 02:08.440
So when we make a mistake, when we say something wrong,

02:08.440 --> 02:12.560
when we do something wrong, we can see what are the causes.

02:12.560 --> 02:14.720
So we can see what are the effects.

02:14.720 --> 02:22.000
We can see what it was that led us to fall into the mind

02:22.000 --> 02:26.160
states that caused us to do something wrong.

02:26.160 --> 02:30.200
The interesting thing is we can often see

02:30.200 --> 02:32.840
how many of the things that we thought were mistakes

02:32.840 --> 02:34.560
in the sense that we did something wrong

02:34.560 --> 02:39.400
are actually not really mistakes per se.

02:39.400 --> 02:43.720
For example, you might forget an appointment,

02:43.720 --> 02:48.480
or you might make a mistake in calculating,

02:48.480 --> 02:54.880
or in you might give a speech and say the wrong thing.

02:54.880 --> 03:02.200
And it may be simply a physical failure,

03:02.200 --> 03:05.760
where the brain, the memory fails you,

03:05.760 --> 03:14.480
or the organic nature of our being is in that way.

03:14.480 --> 03:17.400
And so I think it's important to understand

03:17.400 --> 03:23.400
that oftentimes we, instead of learning from our mistakes,

03:23.400 --> 03:27.440
actually feel guilty about those situations

03:27.440 --> 03:30.120
that are not necessarily our fault.

03:30.120 --> 03:33.280
And I think this is an important part

03:33.280 --> 03:35.000
of learning from our mistakes as well,

03:35.000 --> 03:38.280
that the most important point isn't that you

03:38.280 --> 03:40.360
made a mistake.

03:40.360 --> 03:43.480
The important point is the clear awareness

03:43.480 --> 03:45.840
and understanding of the situation.

03:45.840 --> 03:50.160
So if anger arises and you agree to arises

03:50.160 --> 03:52.440
and you've delusion and ego and so on,

03:52.440 --> 03:56.120
arises and it's the ability to catch this and to see it

03:56.120 --> 04:00.080
and to adjust the mind and to create wisdom

04:00.080 --> 04:03.240
and to remove those states, if something happens

04:03.240 --> 04:05.560
that causes people to get angry at you

04:05.560 --> 04:12.560
or give rise to ego and self-righteousness and so on,

04:12.560 --> 04:14.720
self-righteousness and so on, in other people

04:14.720 --> 04:17.600
where they say this person, did this terrible thing

04:17.600 --> 04:21.600
or this person is lazy or this person is incompetent

04:21.600 --> 04:26.600
or so on, then oftentimes it's not our fault.

04:27.240 --> 04:31.000
Oftentimes this is the nature of reality.

04:31.000 --> 04:33.600
So it's important on the one hand

04:33.600 --> 04:38.400
to be able to discriminate between what is truly a mistake

04:38.400 --> 04:41.000
and what is simply a part of life.

04:41.000 --> 04:43.480
You can't please everyone all the time

04:43.480 --> 04:46.400
and it can be that with the best intentions,

04:46.400 --> 04:51.400
you said something that insulted or angered other people,

04:52.360 --> 04:54.360
it could be even that you said something right

04:54.360 --> 04:55.640
and people didn't like it.

04:55.640 --> 04:57.160
It could be that you said something wrong

04:57.160 --> 05:00.960
but you didn't mean it or you weren't clear enough

05:00.960 --> 05:03.280
or you used the wrong words or so on.

05:04.920 --> 05:09.600
And so important not to be too hard on ourselves,

05:09.600 --> 05:12.480
but on the other hand, when we do make mistakes,

05:12.480 --> 05:16.200
when we do give rise to greed, anger, delusion,

05:16.200 --> 05:19.320
and so on, of jealousy, sin, genus or whatever,

05:19.320 --> 05:21.680
something that leads us to do something

05:21.680 --> 05:26.680
that intentionally causes suffering for ourselves or others.

05:26.680 --> 05:28.920
This is the point, this is what is really a mistake

05:28.920 --> 05:33.920
and learning from it, it doesn't simply mean

05:34.800 --> 05:39.160
not doing it again, it means coming to understand

05:39.160 --> 05:42.760
the situation because too often we think

05:42.760 --> 05:45.000
learning from a mistake is simply hating,

05:45.000 --> 05:46.360
getting angry at ourselves and saying,

05:46.360 --> 05:48.960
don't do that ever again, don't do that ever again.

05:48.960 --> 05:52.440
And it may lead us to be able to avoid

05:52.440 --> 05:57.440
the certain situation simply by brute force of will,

05:59.760 --> 06:03.320
but it won't change the reasons why we did that,

06:03.320 --> 06:05.240
which are the greed, the anger, the delusion,

06:05.240 --> 06:07.400
the unwholesomeness and the mind.

06:07.400 --> 06:10.680
Once we come to see that this is causing suffering,

06:10.680 --> 06:13.360
we won't do it anymore, as I've said before,

06:13.360 --> 06:17.360
there's no one who intentionally causes suffering,

06:17.360 --> 06:21.400
we do it out of the idea that it's somehow going

06:21.400 --> 06:25.160
to bring happiness, that it's going to bring benefit to us,

06:25.160 --> 06:29.040
that it's going to somehow bring happiness to us.

06:29.040 --> 06:31.960
And once we realize that it's actually causing us suffering,

06:31.960 --> 06:36.360
we won't engage in it, we'll realize that there's no benefit.

06:36.360 --> 06:38.600
Once we realize there's no benefit,

06:38.600 --> 06:40.280
this is learning from your mistakes,

06:40.280 --> 06:42.400
so that's from a Buddhist point of view,

06:42.400 --> 07:07.120
so I hope that helps, thanks for the question.

