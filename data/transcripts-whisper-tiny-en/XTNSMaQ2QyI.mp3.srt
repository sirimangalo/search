1
00:00:00,000 --> 00:00:06,000
But it's feeling confused and experiencing the dark night of soul, of the soul, a common phase.

2
00:00:06,000 --> 00:00:11,000
The point where you realize I don't know anything and it pushes you to investigate how things actually are.

3
00:00:11,000 --> 00:00:23,000
Okay, so to say again, I don't know that you necessarily come to the realization, I don't know anything.

4
00:00:23,000 --> 00:00:38,000
But I think the sentiment there is the realization that you're wrong with very many things and you're wrong about the core nature of reality, which is, I think, a very important phase in meditation.

5
00:00:38,000 --> 00:00:45,000
You might even say it's the beginning phase, it's where you begin to cultivate wisdom.

6
00:00:45,000 --> 00:01:04,000
The very beginning is to attain right view, you know, mundane right view, you might say, where you give up your views that are out of line with reality on a conceptual level at least or on an intellectual level.

7
00:01:04,000 --> 00:01:19,000
It's really the beginning of meditation practice. You spend a few days bashing your head against the wall until finally you realized reality is not what you thought.

8
00:01:19,000 --> 00:01:25,000
Not finally, you begin to realize, I would say that comes throughout your practice.

9
00:01:25,000 --> 00:01:39,000
It humbles you. This is really the great thing about realizing you're wrong is the devastation or the devastating effect it has on your ego.

10
00:01:39,000 --> 00:01:44,000
What kills the ego most is realizing that you're wrong.

11
00:01:44,000 --> 00:01:57,000
You've never done that where you were so sure and you're arguing about something so that him and you knew that you were right. And you're so you're 100% sure that you're right. In your mind, the link has been made between fact and belief.

12
00:01:57,000 --> 00:02:01,000
You know that your belief is in mind with fact and it turns out that it's not.

13
00:02:01,000 --> 00:02:05,000
That's an amazing experience.

14
00:02:05,000 --> 00:02:15,000
It's freeing. Yeah. Yeah, it's it's horrific really.

15
00:02:15,000 --> 00:02:22,000
You know, if you never are challenged to the point where you realize that you're wrong, that's the most, that's the scariest thing.

16
00:02:22,000 --> 00:02:29,000
When you see people who are so sure of their beliefs.

17
00:02:29,000 --> 00:02:38,000
And they're never able to see when they're wrong. That's the scary thing.

18
00:02:38,000 --> 00:02:41,000
That's what's most scary.

19
00:02:41,000 --> 00:02:54,000
It's liberating to be it's liberating to be able to accept that you're wrong to know when you're wrong to have that power of mind to actually see that you were wrong.

20
00:02:54,000 --> 00:02:57,000
It's not easy to do.

21
00:02:57,000 --> 00:03:00,000
It's like pulling yourself up by your bootstraps.

22
00:03:00,000 --> 00:03:05,000
Something that you would think would be impossible.

23
00:03:05,000 --> 00:03:11,000
Looking at your looking in your own eyes kind of thing without a mirror.

24
00:03:11,000 --> 00:03:19,000
So I don't know about Dark Knight of Soul. I know that's something that one group, one Buddhist group talks about.

25
00:03:19,000 --> 00:03:31,000
But the idea that when you realize that you're wrong, how that pushes you to investigate how things actually are.

26
00:03:31,000 --> 00:03:34,000
I think you're actually talking about something different.

27
00:03:34,000 --> 00:03:41,000
So what I'm going to say is for me, it's more like this, where rather than realizing you don't know anything.

28
00:03:41,000 --> 00:03:50,000
Through the understanding of things actually as they are, you come to realize that you're wrong.

29
00:03:50,000 --> 00:04:14,000
It's the other way around when you investigate when you come to realize that you're wrong.

