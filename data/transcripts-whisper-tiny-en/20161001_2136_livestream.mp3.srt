1
00:00:00,000 --> 00:00:18,160
Can we do walking and then meditation on death during sitting?

2
00:00:18,160 --> 00:00:23,360
You can do whatever you want, it's a free world.

3
00:00:23,360 --> 00:00:48,960
I'm sorry, it's your oyster, it's not one I teach, but unless you're referring to momentary death, in which case that's what we do, we focus on the momentary born and birth and death, birth and death, every moment.

4
00:00:48,960 --> 00:00:56,680
When a monk becomes ill and decides to accept their possible death instead of seeking treatment, would they be making bad karma?

5
00:00:56,680 --> 00:01:07,640
Not necessarily, karma is momentary, so if they have moments of greed, anger or delusion, that would be bad karma.

6
00:01:07,640 --> 00:01:18,800
There's nothing inherently, there's no inherent necessity, it doesn't necessitate those mind states to refuse treatment necessarily.

7
00:01:18,800 --> 00:01:32,240
There are probably situations where it would, where you'd just be being stubborn, because people were trying to treat you, or there's simple treatment, and you refused it.

8
00:01:32,240 --> 00:01:48,640
I think if there are cases where that would be stubbornness and ego and delusion, attachment, maybe aversion to life, that could be a shame.

9
00:01:48,640 --> 00:01:56,640
You're all caught up on questions on the website, Dante, I just have a simple one, are you still going to New York City next weekend?

10
00:01:56,640 --> 00:01:57,640
Yes.

11
00:01:57,640 --> 00:02:03,640
Let's see, you have two events that are open to people to attend?

12
00:02:03,640 --> 00:02:09,640
Yes, on the 8th and the 9th, it's kind of one event I think, but two days, I'm not sure actually.

13
00:02:09,640 --> 00:02:22,640
There'll be only different people, different days, but yeah, two day, day meditation courses.

14
00:02:22,640 --> 00:02:36,640
On the 8th and the 9th, I walked over New York City, and then I'll be at my father's, so I might just take a week off broadcasting.

15
00:02:36,640 --> 00:02:43,640
But I have to be back Thursday, because this is a special six day.

16
00:02:43,640 --> 00:02:58,640
It's interesting.

17
00:02:58,640 --> 00:03:04,640
I may have to return to the monastery on the 10th, because it's during the rains, right?

18
00:03:04,640 --> 00:03:11,640
I can't just leave.

19
00:03:11,640 --> 00:03:15,640
I may have to come back on the 10th, so then I can go for another seven day.

20
00:03:15,640 --> 00:03:18,640
I only have seven days periods that we can leave.

21
00:03:18,640 --> 00:03:31,640
I'll go another seven day period to see my father, which is also an exception to that.

22
00:03:31,640 --> 00:03:44,640
Where can we get more information about your New York City that's it?

23
00:03:44,640 --> 00:03:50,640
I go on Facebook, they have something called Buddhist Insights.

24
00:03:50,640 --> 00:03:53,640
I think it's a much more...

25
00:03:53,640 --> 00:03:56,640
Look at Buddhist Insights in New York.

26
00:03:56,640 --> 00:04:01,640
Yeah, I think you put it on your page as well.

27
00:04:01,640 --> 00:04:06,640
I think I did.

28
00:04:06,640 --> 00:04:16,640
I'm pretty sure.

29
00:04:16,640 --> 00:04:21,640
Yeah, it's called Buddhist Insights in New York, New York.

30
00:04:21,640 --> 00:04:26,640
Look them up, and you can see what they're doing.

31
00:04:26,640 --> 00:04:35,640
They're trying to start a meditation center, trying to find a home.

32
00:04:35,640 --> 00:05:00,640
And they'll be advertising it in the coming days.

