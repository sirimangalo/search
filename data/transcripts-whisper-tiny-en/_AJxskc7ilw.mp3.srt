1
00:00:00,000 --> 00:00:08,000
How can we reconcile our personal relationships, those with spouses and children especially,

2
00:00:08,000 --> 00:00:19,000
which the Buddha teaches are an obstacle to enlightenment?

3
00:00:19,000 --> 00:00:37,000
Well, this relates back to the idea of where our practice begins and the difference between our progress in practice and our idea of the practice,

4
00:00:37,000 --> 00:00:44,000
because we generally get this idea that the Buddha taught us to give up all of these things,

5
00:00:44,000 --> 00:00:53,000
which he did. But when we get that idea in our minds, our minds of course start to play all sorts of tricks on us,

6
00:00:53,000 --> 00:00:58,000
and tell us what a horrible and teaches would a horrible thing that would be.

7
00:00:58,000 --> 00:01:05,000
And so we either have to repress our intuition, which tells us how great these things are,

8
00:01:05,000 --> 00:01:12,000
or we have to deny the Buddha's teaching and give it up.

9
00:01:12,000 --> 00:01:23,000
So we have this conflict inside. We truly believe in our hearts that these relationships are important.

10
00:01:23,000 --> 00:01:28,000
Well, ordinary people do. There are different levels as the Buddha talked about.

11
00:01:28,000 --> 00:01:33,000
There's the level of views, so people who truly believe that these are beneficial.

12
00:01:33,000 --> 00:01:39,000
Then there's the people who have seen through those views and come to see that they're actually not beneficial.

13
00:01:39,000 --> 00:01:43,000
They're still core to them, or nothing intrinsically beneficial about them.

14
00:01:43,000 --> 00:01:49,000
But they still think that they're beneficial. It still comes to them at some times that,

15
00:01:49,000 --> 00:01:53,000
hey, this seems beneficial. It seems like a good thing.

16
00:01:53,000 --> 00:02:02,000
Or maybe they think, boy, I'm so glad that I have a wife or a husband or a children or so on.

17
00:02:02,000 --> 00:02:05,000
They still think like this.

18
00:02:05,000 --> 00:02:10,000
And then there's the people who have even done the way with the thoughts, but they still give rise to the feelings.

19
00:02:10,000 --> 00:02:15,000
The sun, yeah. When they see their children, they think, still think those are my children,

20
00:02:15,000 --> 00:02:24,000
and when they see their husband or wife, this is my husband and my wife, and they think they feel happy about that.

21
00:02:24,000 --> 00:02:31,000
And so this is where we're at, and this is where the conflict comes from, because we have this idea of how the Buddha,

22
00:02:31,000 --> 00:02:35,000
we know that the Buddha taught that these are bad, and yet we still have these.

23
00:02:35,000 --> 00:02:42,000
We still either believe them or we think them or we feel them, and generally it's all three for most people.

24
00:02:42,000 --> 00:02:48,000
So the way to deal with this, the reconciliation of this is to think of the Buddha's teaching,

25
00:02:48,000 --> 00:02:51,000
as I've said before, think of it like a sweater.

26
00:02:51,000 --> 00:02:57,000
The Buddha's pointing out this loose thread on your sweater,

27
00:02:57,000 --> 00:03:05,000
or maybe not the Buddha's pointing out, the Buddha's pointing out the idea of suffering,

28
00:03:05,000 --> 00:03:11,000
and we can look at our lives, and we can see some loose thread in our life,

29
00:03:11,000 --> 00:03:17,000
that no matter what we believe about things like relationships, we can see suffering.

30
00:03:17,000 --> 00:03:21,000
So in our relationship, we see suffering.

31
00:03:21,000 --> 00:03:26,000
You can ask yourself, are all of my relationships perfectly harmonious?

32
00:03:26,000 --> 00:03:35,000
And when you say the answer is no, we have no trouble agreeing that that disharmony is a problem.

33
00:03:35,000 --> 00:03:46,000
And this is totally a part from whether the relationship is useful or not useful as beneficial as leading to happiness or so on.

34
00:03:46,000 --> 00:03:49,000
Whether our attachment to those people,

35
00:03:49,000 --> 00:03:58,000
or we can see that we have certain attachments or certain problems in our mind that are giving rise to suffering,

36
00:03:58,000 --> 00:04:01,000
regardless of the overall nature of the relationship.

37
00:04:01,000 --> 00:04:07,000
So we deal with those, and this is like we start pulling on this loose thread in our sweater.

38
00:04:07,000 --> 00:04:18,000
And what happens is, eventually, if you go far enough, you trace the causes right back to the root of our attachment and our partiality to certain individuals.

39
00:04:18,000 --> 00:04:29,000
But you do that practically. For me to say this to you, it's not meant so that you then go back and think about it,

40
00:04:29,000 --> 00:04:32,000
or you adopt this view.

41
00:04:32,000 --> 00:04:34,000
The point is to stop thinking like this.

42
00:04:34,000 --> 00:04:43,000
To stop thinking that your relationships are a problem or getting in the way of your relationship, getting in the way of your progress.

43
00:04:43,000 --> 00:04:51,000
And deal with those parts of your relationship that are clearly causing your suffering.

44
00:04:51,000 --> 00:05:03,000
This is in regards to the idea of our reconciling our idea that these relationships are positive.

45
00:05:03,000 --> 00:05:09,000
On a practical side, there's still the question, which may be the question that you're asking,

46
00:05:09,000 --> 00:05:19,000
of what to do then, what to do once you realize that these are getting in the way of enlightenment,

47
00:05:19,000 --> 00:05:24,000
that these are causing you difficulty.

48
00:05:24,000 --> 00:05:30,000
But I still think you have to look at it more practically and see that it's not the people,

49
00:05:30,000 --> 00:05:35,000
and it's not the fact that you have to be around these people that is causing you the problem.

50
00:05:35,000 --> 00:05:43,000
It's still our moment-to-moment time-to-time, occasional arising of defilement.

51
00:05:43,000 --> 00:05:45,000
And those don't arise all the time.

52
00:05:45,000 --> 00:05:49,000
Sometimes these people will not cause a suffering or stress,

53
00:05:49,000 --> 00:05:56,000
or will not give us occasion to do nasty or say nasty things, or think nasty things.

54
00:05:56,000 --> 00:06:02,000
But on occasion they do. On occasion their arises and their mind nasty thoughts.

55
00:06:02,000 --> 00:06:11,000
We say nasty things, and we do nasty things, and we do nasty actions based on our relationship with other people.

56
00:06:11,000 --> 00:06:15,000
This is really where we have to approach the problem.

57
00:06:15,000 --> 00:06:22,000
And the unraveling of the sweater is not only in our mind.

58
00:06:22,000 --> 00:06:28,000
You'll find that your whole life unravels, your family changes,

59
00:06:28,000 --> 00:06:35,000
some members of your family or your social circle might leave or drift apart.

60
00:06:35,000 --> 00:06:41,000
In fact, if you get far enough, you might eventually naturally drift apart from all the people that you know,

61
00:06:41,000 --> 00:06:44,000
or many or most of the people that you know.

62
00:06:44,000 --> 00:06:49,000
But the point I'm trying to make is that that comes naturally.

63
00:06:49,000 --> 00:06:54,000
And it doesn't necessarily have to be a break.

64
00:06:54,000 --> 00:06:58,000
There may come a time where you decide you want to break.

65
00:06:58,000 --> 00:07:01,000
You say I've had enough, or I need a break, and I'm going to take a vacation,

66
00:07:01,000 --> 00:07:04,000
take some time to go and do meditation on my own.

67
00:07:04,000 --> 00:07:09,000
Travel to Sri Lanka and go and stay in some monks cave.

68
00:07:09,000 --> 00:07:14,000
For example, but it doesn't necessarily have to come like that.

69
00:07:14,000 --> 00:07:23,000
And through constant and diligent practice and applying yourself to the Buddhist teaching,

70
00:07:23,000 --> 00:07:27,000
things will naturally change.

71
00:07:27,000 --> 00:07:34,000
It naturally changes in your mind, so you don't ever have to feel some conflict inside your attachment to your family

72
00:07:34,000 --> 00:07:37,000
and your attachment to the Buddhist teaching.

73
00:07:37,000 --> 00:07:44,000
And also so that you don't have to worry or fret about your surroundings,

74
00:07:44,000 --> 00:07:51,000
because our surroundings will change based on the state of our mind.

75
00:07:51,000 --> 00:07:54,000
They might not change as quickly as our mind.

76
00:07:54,000 --> 00:07:59,000
So our mind might begin to say this is not how I want to live my life,

77
00:07:59,000 --> 00:08:04,000
and you're still stuck in a life, in living that life.

78
00:08:04,000 --> 00:08:12,000
But through patience and through a clear understanding of where you want to head,

79
00:08:12,000 --> 00:08:14,000
it will come in time.

80
00:08:14,000 --> 00:08:17,000
The real problem is that we're generally impatient,

81
00:08:17,000 --> 00:08:20,000
and we lose sight of the fact that everything changes.

82
00:08:20,000 --> 00:08:25,000
And we haven't had enough experience in life, because life is quite short,

83
00:08:25,000 --> 00:08:30,000
to show us that, you know, tomorrow things might change totally,

84
00:08:30,000 --> 00:08:32,000
and a week from now and a month from now,

85
00:08:32,000 --> 00:08:36,000
and really every year is different from the last and so on.

86
00:08:36,000 --> 00:08:42,000
So that things are not, you know, the point being that we don't see impermanence clearly enough,

87
00:08:42,000 --> 00:08:51,000
we don't realize that this situation that we find ourselves in is not static,

88
00:08:51,000 --> 00:08:54,000
and it will change, and eventually we might,

89
00:08:54,000 --> 00:08:56,000
well, it will change eventually based on our mind.

90
00:08:56,000 --> 00:09:00,000
So if we're set on meditation eventually our lives become more meditative.

91
00:09:00,000 --> 00:09:16,000
We hope that helps.

