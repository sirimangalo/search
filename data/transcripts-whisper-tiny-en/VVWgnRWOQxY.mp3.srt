1
00:00:00,000 --> 00:00:11,000
I try to be as equanimus as possible, but then after a few minutes, I get so agitated that I have to open my eyes and move any advice.

2
00:00:11,000 --> 00:00:18,000
Stop trying. The more you try, the less peaceful you will become.

3
00:00:18,000 --> 00:00:25,000
So you're learning something. You're learning that forcing things is a cause of suffering, is a cause of stress.

4
00:00:25,000 --> 00:00:33,000
Trying to be equanimus. You should accept the agitation, and once you can accept the agitation, it won't be a cause for you to open your eyes.

5
00:00:33,000 --> 00:00:40,000
You'll become bored with it, disenchanted with it, and you won't fuss or worry or be concerned by it.

6
00:00:40,000 --> 00:00:52,000
Learn to meditate on the agitation, learn to meditate on your anger, meditate on your greed, learn to meditate on whatever comes up, and you'll find that the hindrance is a rise less and less.

7
00:00:52,000 --> 00:00:58,000
And you're able to be more equanimus as a result rather than as a practice.

