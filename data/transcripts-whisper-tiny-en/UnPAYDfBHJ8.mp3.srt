1
00:00:00,000 --> 00:00:12,960
Okay, hello everyone, I'm back.

2
00:00:12,960 --> 00:00:25,280
Here to record the third part of my series of videos on Buddhism and Nature.

3
00:00:25,280 --> 00:00:35,920
So it's daytime, we have a later video.

4
00:00:35,920 --> 00:00:49,440
In this part, I wanted to talk about nature in a different sort of way than the last

5
00:00:49,440 --> 00:00:53,120
two videos.

6
00:00:53,120 --> 00:01:01,400
So if you recall, the first two ways you can think of nature or two of the ways, the

7
00:01:01,400 --> 00:01:10,000
two ways I talked about are, first of all, in terms of being something different from

8
00:01:10,000 --> 00:01:16,240
the human, the artificial we might say.

9
00:01:16,240 --> 00:01:26,400
So if anything human is considered artificial, then what is not human is nature.

10
00:01:26,400 --> 00:01:37,760
And the second way, the second video was about human nature and the idea of what is nature

11
00:01:37,760 --> 00:01:44,680
in that debate over nature and nature versus nurture, which I didn't mention, but would

12
00:01:44,680 --> 00:01:55,800
have been something good to mention.

13
00:01:55,800 --> 00:02:00,840
I can see here, I'm on my phone, but I can see your comments still, they pop up on my

14
00:02:00,840 --> 00:02:06,160
screen, it's kind of neat.

15
00:02:06,160 --> 00:02:10,640
So I'm flying to date of Florida, that's when I'm doing it, this time of day, that's

16
00:02:10,640 --> 00:02:19,720
not before I leave, I'll do this video that I've been dragging my feet on a bit.

17
00:02:19,720 --> 00:02:29,400
So I guess the first thing I'd mention, something that I think I mentioned in the other

18
00:02:29,400 --> 00:02:38,040
videos is that those two ways of thinking about nature are not really nature according

19
00:02:38,040 --> 00:02:45,460
to Buddhism, they're not the ultimate nature.

20
00:02:45,460 --> 00:02:59,640
So this third way of thinking about nature, I would define it as nature is that which exists

21
00:02:59,640 --> 00:03:07,440
independent of fabrication, which is kind of similar to the idea of that which is

22
00:03:07,440 --> 00:03:16,560
not artificial, but I like the word fabricate fabrication here, and I'll try to explain why.

23
00:03:16,560 --> 00:03:21,960
So what is it that's free from fabrication?

24
00:03:21,960 --> 00:03:36,440
In modern times, most people would, I think, those who are unfamiliar with or are not in

25
00:03:36,440 --> 00:03:44,840
line with Buddhist ideas of nature, would say that it's the world around us, it's the

26
00:03:44,840 --> 00:03:59,680
physical world, right, it's the universe, the atomic and subatomic particles, the quantum

27
00:03:59,680 --> 00:04:09,200
fields, it's the people, the places, the things, not the people I guess, but yeah, it's

28
00:04:09,200 --> 00:04:14,680
even the people in the places and things, all of that is nature, reality is another word

29
00:04:14,680 --> 00:04:19,880
they might use, although that's kind of as some spiritual connotations that they might

30
00:04:19,880 --> 00:04:30,160
not like, so what is nature, nature is the third person impersonal world, independent

31
00:04:30,160 --> 00:04:39,440
of experience, of course because fabrication they would say is all what we experience, what

32
00:04:39,440 --> 00:04:48,840
we experience is all made up of fabrication, what is not fabricated is that which is outside

33
00:04:48,840 --> 00:04:53,600
of experience.

34
00:04:53,600 --> 00:05:03,920
And there's some argument that to be made there against that, like the argument from the

35
00:05:03,920 --> 00:05:18,760
point of view of magic or extra sensory perceptions, so if someone says that there are

36
00:05:18,760 --> 00:05:27,000
these things that exist out there, what I can say, well what if there's a being who is

37
00:05:27,000 --> 00:05:35,360
tricking us into thinking that there is something out there, there are these things out there

38
00:05:35,360 --> 00:05:42,160
and they might say that well you can independently verify that those things exist, I might

39
00:05:42,160 --> 00:05:47,160
be able to be tricked but I can ask other people and they'll tell me the same thing,

40
00:05:47,160 --> 00:05:56,520
they say well what if this being is really special and is tricking all of us and they might

41
00:05:56,520 --> 00:06:02,040
say wow we have these instruments and machines but ultimately I've even talked to scientists

42
00:06:02,040 --> 00:06:10,240
who say we can't really deny the possibility that God exists because no matter what laws

43
00:06:10,240 --> 00:06:23,080
or theories we have, ultimately they can all just be tricks that God is playing on us.

44
00:06:23,080 --> 00:06:28,640
This is the problem that Renee Descartes wrestled with by the way, I bring him up a lot,

45
00:06:28,640 --> 00:06:33,960
you've probably heard me some of you, many of you talk about him, I just found it kind

46
00:06:33,960 --> 00:06:40,640
of neat that he was saying something, I'm not a follower or I don't agree with many of the

47
00:06:40,640 --> 00:06:47,640
things he said but I thought it was interesting, this very famous saying of his cogito ergosum,

48
00:06:47,640 --> 00:06:53,320
he wrestled with this, he said how do we know any of this, how do I know anything,

49
00:06:53,320 --> 00:07:01,720
I mean his idea was how do I know I exist, what can I say about myself and he realized

50
00:07:01,720 --> 00:07:07,560
there was something he could say about himself that's cogito, cogito means I cognize, ergo

51
00:07:07,560 --> 00:07:18,680
therefore soon I am, why because you can't be tricked into thinking that you're thinking.

52
00:07:18,680 --> 00:07:26,600
So there's this debate about whether this stuff outside actually exists, I think scientists

53
00:07:26,600 --> 00:07:32,560
rightfully so tended to dismiss the arguments against it saying, first of all we have no

54
00:07:32,560 --> 00:07:39,040
evidence to suggest that there is such a being, we have no reason to think that there's

55
00:07:39,040 --> 00:07:49,160
such a being and based on the law of parsimony, parsimony, aw comes razor and we have to

56
00:07:49,160 --> 00:07:59,080
say it's wrong, it's wrong to suggest something extraneous without any reason, exceptional

57
00:07:59,080 --> 00:08:06,040
claims require exceptional evidence, that's the idea and that's right, there is no reason

58
00:08:06,040 --> 00:08:11,040
to think that this stuff around us doesn't exist, there's no reason it would be silly

59
00:08:11,040 --> 00:08:18,160
to suggest there is such a being, suggesting God is a silly idea, it's just silly.

60
00:08:18,160 --> 00:08:33,440
You can believe in God, but it's no reason, it's like Russell said, Bertrand Russell said,

61
00:08:33,440 --> 00:08:41,640
I could postulate a teapot orbiting the sun, you can't disprove it, how are you going to

62
00:08:41,640 --> 00:08:45,400
find out if there isn't one, how are you going to prove to me that there isn't one,

63
00:08:45,400 --> 00:08:57,320
he said God is like that, no evidence for such a teapot and so it appears quite reasonable

64
00:08:57,320 --> 00:09:05,120
to suggest that that which is beyond fabrication is this impersonal world that physics

65
00:09:05,120 --> 00:09:14,320
has come up with, classical physics, of course then we get into the fact that quantum

66
00:09:14,320 --> 00:09:20,600
physics defies the laws of physics, goes against classical physics, shows that classical

67
00:09:20,600 --> 00:09:29,960
physics is only an approximation, it's only makeshift and kind of sort of shakes up this

68
00:09:29,960 --> 00:09:38,400
idea that there is some reality out there, many of the theories behind quantum physics

69
00:09:38,400 --> 00:09:47,400
rely upon the idea that the physical reality requires observation to exist, a measurement,

70
00:09:47,400 --> 00:09:56,320
so there are other ways to measure, but it appears to be something, some room for experience,

71
00:09:56,320 --> 00:10:04,000
but that's not really, there's a problem that is missed in all of this discussion and

72
00:10:04,000 --> 00:10:10,960
that's where Buddhism comes in, now I think it's difficult if you haven't done any

73
00:10:10,960 --> 00:10:21,320
meditation to appreciate the argument, so I want to preface everything by that, because

74
00:10:21,320 --> 00:10:45,560
if you haven't spent time with experience, then your immediate habit is to see everything

75
00:10:45,560 --> 00:10:53,000
as impersonal, external things, like you look at a chair and immediately, you see it

76
00:10:53,000 --> 00:11:03,000
as a chair, it was wrong with that, I want to point out what's the problem with that,

77
00:11:03,000 --> 00:11:18,600
the problem with saying that that chair is outside of fabrication, is that psychologically,

78
00:11:18,600 --> 00:11:22,720
what's happening, let's not take out the word psychology psychologically for a moment,

79
00:11:22,720 --> 00:11:45,160
it's happening when you see the chair and recognize that it's a chair, is that it's

80
00:11:45,160 --> 00:11:54,440
at least a two-step process in the main there are two steps going on, there's the experience

81
00:11:54,440 --> 00:12:08,000
of seeing, and then there's the mental process which identifies that chair as a chair,

82
00:12:08,000 --> 00:12:16,480
there's in fact more going on, there's the process of relating the chair to other chairs

83
00:12:16,480 --> 00:12:24,520
that you've seen, first of all, that's called sunya, sunya means, this is like that,

84
00:12:24,520 --> 00:12:31,680
this is like, this goes with that, so you see a chair and there's a, like this, there's

85
00:12:31,680 --> 00:12:42,040
this process of comparing it with other things, that's like this, that's like those things

86
00:12:42,040 --> 00:12:51,760
I've seen, those things are all chairs, therefore this is a chair, two-step process,

87
00:12:51,760 --> 00:13:02,440
there's fabrication involved, do you see, and that's significant, the important thing to

88
00:13:02,440 --> 00:13:13,400
understand is that significant, and what I think much of science fails to appreciate

89
00:13:13,400 --> 00:13:24,320
is the effect, the consequences of having a mental paradigm, a psychological paradigm,

90
00:13:24,320 --> 00:13:34,000
an ontological paradigm, I guess, as how you say it, based on this impersonal external

91
00:13:34,000 --> 00:13:43,360
world, it's not even a question anymore of who's right, it's a question of the consequences,

92
00:13:43,360 --> 00:13:59,720
the effects of having such a complex experience, or a way of living, where everything

93
00:13:59,720 --> 00:14:24,560
you experience is filtered through this process of fabrication, mental fabrication, and

94
00:14:24,560 --> 00:14:39,560
so from a Buddhist perspective, it's unnatural, this complicated, complex, fabrication-based

95
00:14:39,560 --> 00:14:58,700
state of reality, because ultimately, it leads to questions about what is real, the teapot

96
00:14:58,700 --> 00:15:14,360
circling the earth, circling the sun, God, or the chair, psychologically, they have all

97
00:15:14,360 --> 00:15:37,240
the same processes, when it rains, when it rains and you say, oh, that's because of evaporation

98
00:15:37,240 --> 00:15:52,800
and condensation and warm and cold air and so on, I don't know how it works, but

99
00:15:52,800 --> 00:16:01,000
you're fabricating, you could say, you're engaging in belief statements, you're engaging

100
00:16:01,000 --> 00:16:10,760
in the process of belief, because what is belief, belief is a mental fabrication, belief

101
00:16:10,760 --> 00:16:22,480
is taking an experience and extrapolating on it, trying to understand it, so when I engage

102
00:16:22,480 --> 00:16:32,360
in this process of saying, that rain is because of this, I'm engaging in a certain mental

103
00:16:32,360 --> 00:16:35,040
process.

104
00:16:35,040 --> 00:16:38,560
All scientific theories are like this, right, if you have a scientific theory about how

105
00:16:38,560 --> 00:16:46,000
things happen and what is the truth of things and what are things made of, you're engaging

106
00:16:46,000 --> 00:16:55,960
in a process of extrapolation and there is mental fabrication going on, and you're creating

107
00:16:55,960 --> 00:17:03,760
these ideas in your mind.

108
00:17:03,760 --> 00:17:12,360
When you say, oh, it's God, why is it raining, God is making a rain, maybe God is crying,

109
00:17:12,360 --> 00:17:26,520
right, people might say, and you're doing the same thing, in science, they call this

110
00:17:26,520 --> 00:17:39,240
idea the difference between justified true belief and just belief, you have belief, well,

111
00:17:39,240 --> 00:17:52,400
not all belief is justified or true, but psychologically it's the same, it's still belief

112
00:17:52,400 --> 00:18:06,120
and the process is still the same, it's complex and it's abstract and it's this abstraction

113
00:18:06,120 --> 00:18:16,920
that allows for, as you can, as you can, I think tell that leads to misunderstanding, it's

114
00:18:16,920 --> 00:18:35,320
the wrong view, that opens the door to wrong view and opens the door to clinging and opens

115
00:18:35,320 --> 00:18:57,320
the door to arguing and debating, opens the door to suffering, won't remember.

116
00:18:57,320 --> 00:19:18,680
And in opposition to all of this, opposition to the way of extrapolating things and fabricating,

117
00:19:18,680 --> 00:19:27,080
we do almost instantaneously, when you see a chair, immediately see it as a chair, and

118
00:19:27,080 --> 00:19:34,360
you look around the room and you say, okay, this room is so big and so on, without realizing

119
00:19:34,360 --> 00:19:40,000
that all of that is at least two steps, three steps, four steps, often more, you like it,

120
00:19:40,000 --> 00:19:52,040
you dislike it, these are the kinds of things you can react to, you have views about it,

121
00:19:52,040 --> 00:20:05,160
and all of this is in contrast to something that's more natural, more simple and more pure,

122
00:20:05,160 --> 00:20:14,440
and that is indirect and unadulterated experience.

123
00:20:14,440 --> 00:20:21,000
So what Buddhism would say is truly natural, true nature, truly natural, let's say, because

124
00:20:21,000 --> 00:20:30,520
in English that makes the most sense, is experience.

125
00:20:30,520 --> 00:20:37,120
Whatever we can say about the world outside of us, the universe, the stars, and the cosmos,

126
00:20:37,120 --> 00:20:50,960
it's all fine to talk about that, but it's not natural, it's more artificial,

127
00:20:50,960 --> 00:20:59,320
than simply to experience, not really in a judgmental way, there's nothing wrong with

128
00:20:59,320 --> 00:21:05,080
all of this, I mean we use it practically, the Buddha talked about the stars and the heavens

129
00:21:05,080 --> 00:21:12,800
and so on, but he tried to make clear that there's something quite different from all

130
00:21:12,800 --> 00:21:18,240
of that, and that is experience, seeing, hearing, smelling, tasting, feeling, thinking,

131
00:21:18,240 --> 00:21:23,760
why did the Buddha mention these senses so much?

132
00:21:23,760 --> 00:21:32,960
If you're unfamiliar with these ideas, it seems like, why is he talking about such obvious

133
00:21:32,960 --> 00:21:43,880
banal things, like the senses, the same thing, Descartes saw the same thing,

134
00:21:43,880 --> 00:21:55,760
it's very important, cognition, experience, consciousness, this is why meditation works,

135
00:21:55,760 --> 00:22:04,520
this is how meditation works, how mindfulness works, mindfulness in the context of

136
00:22:04,520 --> 00:22:21,600
the present moment means non-fabrication, non-interpreted, interpretative experience,

137
00:22:21,600 --> 00:22:29,560
it's in the same way we talk about this colloquial English, like don't forget who you

138
00:22:29,560 --> 00:22:38,120
are, when we tell some way, in old times anyway, they would tell someone who is inferior

139
00:22:38,120 --> 00:22:43,120
to them, they would say, they can figure, stay, you should, don't forget yourself, you

140
00:22:43,120 --> 00:22:52,560
forget yourself, and we use this word forget in terms of forgetting something important,

141
00:22:52,560 --> 00:23:02,800
not in the past, but about the present, about your situation, like if you're in a library

142
00:23:02,800 --> 00:23:07,080
and you forget that you're in a library, right, you haven't forgotten something in the

143
00:23:07,080 --> 00:23:13,680
past, you've forgotten something about the present, and you start shouting, oh, I forgot

144
00:23:13,680 --> 00:23:20,120
we're in a library, you see, this is why setty, the word we translate as mindfulness

145
00:23:20,120 --> 00:23:28,040
actually should be translated as remembrance, that's what it literally means, and it means

146
00:23:28,040 --> 00:23:40,800
to remember the nature of the natural part of the experience, seeing, hearing, not that

147
00:23:40,800 --> 00:23:46,400
you're in a library, that is mindfulness, that is remembrance, but it's already conceptual,

148
00:23:46,400 --> 00:23:57,000
seeing that you're in a library, it's already a fabrication, a complex one, it involves

149
00:23:57,000 --> 00:24:08,680
lots of ideas that lead to you to believe you're in a library, and so when we say to

150
00:24:08,680 --> 00:24:20,080
ourselves, and we're seeing, hearing, hearing, feeling, feeling, we're reminding ourselves,

151
00:24:20,080 --> 00:24:34,520
we are reinforcing, augmenting, encouraging the experience that is single step, that is

152
00:24:34,520 --> 00:24:43,320
not complex, that doesn't leave room for views, opinions, that isn't psychologically

153
00:24:43,320 --> 00:24:54,400
indeterminate, right, because as I said, you believe in why it's raining, is indeterminate,

154
00:24:54,400 --> 00:25:02,920
you can't be sure, there is an uncertainty, there is an abstract nature to it, the reason

155
00:25:02,920 --> 00:25:15,200
why it's uncertain is because it's abstract, but you can't be tricked into thinking that

156
00:25:15,200 --> 00:25:22,640
you're seeing, for example, it's just not possible, and you could create some view that

157
00:25:22,640 --> 00:25:29,600
yes it is possible, but that doesn't matter, it's not the fact that you can, it's the reason

158
00:25:29,600 --> 00:25:36,920
why it's not the fact that you can't, it's the reason why you can't, because it's primary,

159
00:25:36,920 --> 00:25:47,640
because it's part of nature, so from a perspective of views, that's, I think, important,

160
00:25:47,640 --> 00:25:52,760
it's still very theoretical, I don't know how impressive this is for people to hear,

161
00:25:52,760 --> 00:25:57,840
but I think it's an important step, but it's not the most important step, it's not the

162
00:25:57,840 --> 00:26:11,000
most important part of nature, because the ramifications of the view that experience

163
00:26:11,000 --> 00:26:22,160
is the most natural thing, that it is nature, open, the ramifications are that it opens

164
00:26:22,160 --> 00:26:39,640
the door to suffering ethics, let's say those two really, suffering and ethics as a part

165
00:26:39,640 --> 00:26:49,760
of reality, because experience is not like physics, the physical reality has no ethics,

166
00:26:49,760 --> 00:26:56,840
there's no suffering, what is suffering, can a rock suffer, what would it mean to say that

167
00:26:56,840 --> 00:27:04,280
a physical entity suffers, it would say well suffering is just a chemical reaction in the

168
00:27:04,280 --> 00:27:15,080
brain, right, something like that, electrical, stimulus, nervous system, from experience

169
00:27:15,080 --> 00:27:22,160
that's absurd, from a point of view of experience, pain is suffering, right, loss is suffering,

170
00:27:22,160 --> 00:27:27,960
not getting what you want is suffering, I think if you don't understand this and you read

171
00:27:27,960 --> 00:27:32,520
the four noble truths, you wonder why are these so important, okay, I get it suffering,

172
00:27:32,520 --> 00:27:39,360
but why is the Buddha so fixated on this, and it's a bit foreign, I think, it's okay

173
00:27:39,360 --> 00:27:47,120
as a spiritual path, but we don't realize how significant these four noble truths are,

174
00:27:47,120 --> 00:27:57,440
these are intrinsic to reality, reality involves suffering, it's not just an impersonal thing

175
00:27:57,440 --> 00:28:04,760
where yes there's seeing and hearing and so on, there's also suffering, why is there suffering

176
00:28:04,760 --> 00:28:16,200
because there's ethics, there are good and bad qualities of mind, of experience, when

177
00:28:16,200 --> 00:28:26,800
you react to something you react with greed or anger or delusion and suffering is the result,

178
00:28:26,800 --> 00:28:34,600
and so there's not only an experiential ontological framework, there's also what we

179
00:28:34,600 --> 00:28:41,640
have called so tear theological framework, so tearological involves, it refers to the concept

180
00:28:41,640 --> 00:28:50,920
of salvation, right, I think, if I'm getting these terms, right, the idea that it's not

181
00:28:50,920 --> 00:28:59,680
just about, okay, now we know what exists and we've done the job, yeah, there is a sort

182
00:28:59,680 --> 00:29:10,800
of a path, you know, inherent in reality and that's the path out of suffering, it's not

183
00:29:10,800 --> 00:29:16,280
something that you have to believe in or that you have to subscribe to Buddhism or it's

184
00:29:16,280 --> 00:29:23,920
not something specific to Buddhism, you see, it's specific to experience, experience involves

185
00:29:23,920 --> 00:29:38,040
pain, suffering, loss, it involves ethics and unethical behavior, unethical states.

186
00:29:38,040 --> 00:29:43,800
And so this is what Buddhism, this is where Buddhism exists, this is nature according

187
00:29:43,800 --> 00:29:53,480
to Buddhism, nature is experiential, it's ethical and it revolves around the concept

188
00:29:53,480 --> 00:30:01,840
of suffering, not how can we suffer more or everything's suffering, it's about understanding

189
00:30:01,840 --> 00:30:09,080
and freeing yourself from suffering, the Buddha, people ask the Buddha what did he teach

190
00:30:09,080 --> 00:30:17,560
in a time and again he said, I teach only four things, what is suffering, what is the cause

191
00:30:17,560 --> 00:30:22,760
of suffering, what is the cessation of suffering and what is the path that leads to the

192
00:30:22,760 --> 00:30:34,520
cessation of suffering, these are the fundamental questions to experience and experiences

193
00:30:34,520 --> 00:30:54,320
the fundamental building block of nature, so there you know, that's nature according to

194
00:30:54,320 --> 00:30:59,400
Buddhism from my point of view of course, many people might argue and have different ideas

195
00:30:59,400 --> 00:31:10,000
but that's video three and I'm thinking I'll start making some Q&A videos, so I'm going

196
00:31:10,000 --> 00:31:15,880
to take the top voted questions on our meditation site, not here, if you ask questions

197
00:31:15,880 --> 00:31:22,640
on YouTube you won't get answers, sorry, but if you join our meditation community and

198
00:31:22,640 --> 00:31:30,160
up vote, the questions there I'll start, I will take into account the up votes, I'm

199
00:31:30,160 --> 00:31:38,440
not going to be controlled by them, but I'll look at questions that are meaningful and

200
00:31:38,440 --> 00:31:44,000
questions that I haven't answered before a thousand times and I'm going to try, we'll

201
00:31:44,000 --> 00:31:51,080
see how it goes, try to answer them, but that's all for now, thank you all for tuning

202
00:31:51,080 --> 00:31:57,480
in, we have 61 viewers at the moment so that's pretty awesome for an impromptu video, thank

203
00:31:57,480 --> 00:32:06,960
you all for being patient and keeping up, stop arguing in the comment section, there

204
00:32:06,960 --> 00:32:11,160
should be, you should emulate those people who are not commenting because it means they're

205
00:32:11,160 --> 00:32:21,040
listening closely, 64 now, awesome, okay have a good day everyone, I'll see you from

206
00:32:21,040 --> 00:32:28,040
Florida next, have a good day, I don't know how to end this, there we are.

