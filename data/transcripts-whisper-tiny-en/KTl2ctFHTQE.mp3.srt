1
00:00:00,000 --> 00:00:05,680
Okay, good evening, everyone.

2
00:00:05,680 --> 00:00:13,840
So I'm going to try to do more regular broadcasts.

3
00:00:13,840 --> 00:00:20,640
Here's an attempt at something like a daily broadcast.

4
00:00:20,640 --> 00:00:24,800
It would be a daily answering of questions, I think.

5
00:00:24,800 --> 00:00:27,200
So let me know if the sound is no good.

6
00:00:27,200 --> 00:00:30,400
If you can't hear me, I'm trying this little mic here.

7
00:00:30,400 --> 00:00:36,720
And maybe it's better sound, I don't know.

8
00:00:36,720 --> 00:00:40,960
I can see comments, so feel free to comment.

9
00:00:40,960 --> 00:00:45,200
But when listening to the dhamma, one thing to keep in mind is,

10
00:00:45,200 --> 00:00:48,480
it's not about chatting, you know.

11
00:00:48,480 --> 00:00:51,840
Chatting the comment section shouldn't be seen as a sort of,

12
00:00:51,840 --> 00:00:54,800
hey, let's chat while he's giving the talk.

13
00:00:54,800 --> 00:01:00,080
This isn't just because it's the internet.

14
00:01:00,080 --> 00:01:02,640
It doesn't mean we should act.

15
00:01:02,640 --> 00:01:03,440
Internet-ish.

16
00:01:07,360 --> 00:01:13,680
So these videos are probably for now at least going to be answering questions.

17
00:01:13,680 --> 00:01:16,160
So we've got all these questions on our meditations,

18
00:01:16,160 --> 00:01:18,160
and I'm not going to be answering questions in the comment.

19
00:01:18,160 --> 00:01:25,920
So please don't post comments or questions there.

20
00:01:30,160 --> 00:01:37,520
But another thing is that last night, I talked about the idea of having a sort of a live question

21
00:01:37,520 --> 00:01:42,960
where people could call me and ask, what could go wrong and sure enough in the middle of the night?

22
00:01:42,960 --> 00:01:50,640
Some people saw the video and said, oh, I think I'll call him.

23
00:01:50,640 --> 00:01:55,680
And so I had people calling me on Hangouts in the middle of the night,

24
00:01:55,680 --> 00:01:59,520
which I didn't quite expect, but probably should have.

25
00:01:59,520 --> 00:02:01,760
So I think we'll go back to the drawing board on that one.

26
00:02:05,360 --> 00:02:09,360
But the night's question is about avoidance.

27
00:02:09,360 --> 00:02:16,240
The question was about avoiding a friend who had betrayed the person.

28
00:02:17,360 --> 00:02:24,000
And their feeling was that they weren't strong enough to deal with the person,

29
00:02:24,000 --> 00:02:25,200
to avoid getting upset.

30
00:02:27,600 --> 00:02:33,200
And as a result, they wonder whether it's better to avoid this person

31
00:02:33,200 --> 00:02:35,440
while they become stronger than a new meditator.

32
00:02:35,440 --> 00:02:39,040
So it's a good general question.

33
00:02:39,040 --> 00:02:40,400
What about avoidance?

34
00:02:41,760 --> 00:02:43,280
And I've talked about this before.

35
00:02:44,240 --> 00:02:51,840
It comes in the Sabasa, there's a short, but useful guideline there.

36
00:02:53,520 --> 00:02:56,640
It's called Peri Wajana pahatamada.

37
00:02:58,240 --> 00:03:03,840
The defilements are the taints that you get rid of, the purification or the purity of mine

38
00:03:03,840 --> 00:03:09,360
that comes about from avoiding things, because if you didn't avoid them,

39
00:03:11,120 --> 00:03:13,120
unwholesome states would arise.

40
00:03:16,720 --> 00:03:19,440
So let's put this in three parts.

41
00:03:21,600 --> 00:03:26,320
What the Sabasa is that advises you to avoid.

42
00:03:27,520 --> 00:03:31,120
So there's a avoiding on principle.

43
00:03:31,120 --> 00:03:37,200
Let's call that. And the avoiding on principle, in this case, I would say,

44
00:03:37,200 --> 00:03:39,760
relates to the fact that the person is evil.

45
00:03:40,960 --> 00:03:44,400
Is of evil intentions? You know, this person betrays other people.

46
00:03:45,600 --> 00:03:48,320
They're the sort of person who is not to be trusted.

47
00:03:49,040 --> 00:03:53,440
It's not really a condemnation of the person, but it's an awareness and an understanding.

48
00:03:53,440 --> 00:04:01,920
And that might change, but even with an apology,

49
00:04:03,680 --> 00:04:07,840
you forget you forgive, but I think forgiving and forgetting is not,

50
00:04:07,840 --> 00:04:09,040
should not always be the same.

51
00:04:10,240 --> 00:04:12,800
First people have, we do things based on habits.

52
00:04:12,800 --> 00:04:17,120
So if a person has betrayed you, I think you're within your rights,

53
00:04:17,120 --> 00:04:22,080
and it's generally a good thing to do, to be wary of that person.

54
00:04:22,080 --> 00:04:25,360
Don't take their counsel or take their counsel with the grain of salt.

55
00:04:26,400 --> 00:04:27,440
Don't seek them out.

56
00:04:29,600 --> 00:04:38,560
And as far as associating with them, be wary and consider that being in their presence

57
00:04:38,560 --> 00:04:42,560
has the potential for getting caught up in their unwholesome.

58
00:04:42,560 --> 00:04:51,280
And so on principle, avoiding people who, you know, to the degree that you

59
00:04:53,520 --> 00:04:56,880
are reasonable in understanding that they've got bad intentions.

60
00:04:58,560 --> 00:05:03,520
But that says nothing about your reactions to them.

61
00:05:03,520 --> 00:05:13,760
And so the idea here is that a person's evilness or level of evil potential, let's say,

62
00:05:16,000 --> 00:05:21,600
is a source of fairly strong and understandable reaction.

63
00:05:23,200 --> 00:05:29,200
So it's of all things that's much more likely to give rise to unwholesomeness.

64
00:05:29,200 --> 00:05:37,440
It's on a level different from the unwholesomeness that comes from an objective experience.

65
00:05:37,440 --> 00:05:43,680
Let's say, stubbing your toe on a rock, for example, or feeling cold, feeling hot,

66
00:05:45,360 --> 00:05:53,040
hearing a sound, seeing an image or an object, and so on.

67
00:05:53,040 --> 00:06:00,320
So this first avoidance, definitely avoid evil people.

68
00:06:00,960 --> 00:06:06,320
It doesn't mean you run away from them, or you see them and you get all freaked out or anything.

69
00:06:06,320 --> 00:06:14,880
It means you have a rational understanding that, that you incorporate into this rational

70
00:06:15,520 --> 00:06:17,120
sense of what's right to do, right?

71
00:06:17,120 --> 00:06:20,000
Because a lot of the questions I get and a lot of the questions we have are,

72
00:06:20,000 --> 00:06:22,880
what's the right thing to do in a certain situation?

73
00:06:24,640 --> 00:06:26,960
And there's no easy answer to that.

74
00:06:26,960 --> 00:06:34,160
There are a lot of factors that come into play, but the main point is don't let your emotions

75
00:06:34,160 --> 00:06:35,520
get involved, right?

76
00:06:35,520 --> 00:06:39,520
It shouldn't just be, I don't like this, I'm going to leave, not on this level.

77
00:06:40,560 --> 00:06:44,560
There's a level in which we do things just because they're right to do, or we don't do things

78
00:06:44,560 --> 00:06:49,920
just because they're wrong to do. It's the first level. So the second level,

79
00:06:51,600 --> 00:07:00,160
where we avoid things that we can't handle, and this is already covered under the first one

80
00:07:00,160 --> 00:07:03,760
in this case, but it's worth pointing out the difference here, right?

81
00:07:04,800 --> 00:07:10,240
There are qualities about a person that lead us to

82
00:07:10,240 --> 00:07:20,080
unwholesomeness. And so in this example, a person who's betrayed you, just being in their presence

83
00:07:20,080 --> 00:07:24,080
might freak you out or make you upset and make you remember all the bad things that they didn't

84
00:07:24,080 --> 00:07:31,600
do. That's not necessarily a reason to avoid a reason to do anything, to say, well, this is not

85
00:07:31,600 --> 00:07:36,560
good, I shouldn't be in their presence because I get upset, just seeing them, right?

86
00:07:36,560 --> 00:07:45,520
Because that's in the realm of experience. The same sort of thing can arise from being around

87
00:07:45,520 --> 00:07:54,400
someone who's not doing anything unwholesomeness, but whose behavior or appearance or so on gives

88
00:07:54,400 --> 00:07:59,680
rise to unwholesomeness in you. So it could be because they're eating too loud, right? I make a

89
00:07:59,680 --> 00:08:04,480
clicking noise in my job when I eat sometimes, and my father's called me out on it.

90
00:08:04,480 --> 00:08:11,680
It gets upset about it. Maybe kids playing or something or a person's voice is shrill.

91
00:08:12,960 --> 00:08:18,640
Maybe you don't like the way I talk. I don't like the fact that I'm a white person teaching

92
00:08:18,640 --> 00:08:25,280
Buddhism or so. So many things that we give rise to. I mean, it's cold out here and I'm shaking.

93
00:08:25,280 --> 00:08:27,840
Maybe you don't like the fact that I'm shaking. Maybe I don't like the fact.

94
00:08:28,960 --> 00:08:32,480
My hand is shaking here. This phone is shaking because of how cold it is.

95
00:08:32,480 --> 00:08:37,680
That's just a feeling. That's just the body. Maybe you don't like it.

96
00:08:40,320 --> 00:08:45,760
So where does that become something that you should say? Well, I can't handle this, and I admit

97
00:08:45,760 --> 00:08:52,640
that this is my feeling, but I shouldn't handle it. And that's really the crux of the question

98
00:08:52,640 --> 00:09:02,000
as it was asked. And I think I don't have a perfect, you know, I don't have a well-based

99
00:09:02,000 --> 00:09:08,400
answer here, but but a guideline in the sort of ballpark area, I would say, is where it leads to

100
00:09:10,400 --> 00:09:17,760
do a change in your actions. So suppose this person has betrayed me and I hear them talking to

101
00:09:17,760 --> 00:09:22,160
other people, maybe just talking or I just see them sitting there and it makes me want to do

102
00:09:22,160 --> 00:09:28,320
something or at least scowl at them. And maybe when I when I talked to them, I just I know I'm

103
00:09:28,320 --> 00:09:35,680
going to say mean things or or or angry things. If it leads you to do unwholesumness,

104
00:09:36,480 --> 00:09:45,200
I think definitely there's a line that's crossed there and where you should say that's where I

105
00:09:45,920 --> 00:09:51,200
in that case I shouldn't just avoid this situation. I mean, that's where when you're in an argument

106
00:09:51,200 --> 00:09:58,960
with someone, I think, well, many people say this, but I think in Buddhism, it applies that if

107
00:09:58,960 --> 00:10:03,520
you're angry with someone, if you just are angry when you're in a conversation with someone,

108
00:10:03,520 --> 00:10:10,480
you should walk away. It should end the conversation. I'm sorry, I have to go and take time to deal

109
00:10:10,480 --> 00:10:15,840
with the anger. Not not then to go in your room and and and fume about how awful that person is,

110
00:10:15,840 --> 00:10:22,480
but to go into your room and and consider how how awful you are, right? How how how wrong

111
00:10:23,280 --> 00:10:27,600
states of a reason in you, how the anger has arisen in you and that and deal with that.

112
00:10:28,240 --> 00:10:33,680
Not beating yourself up over it, but deal with it. Because you can't effectively deal with it

113
00:10:33,680 --> 00:10:40,000
if you're getting, if you're expressing, if you're if you're committing unwholesum speech

114
00:10:40,000 --> 00:10:55,280
or or or or at. But I think for the most part, we should be we should be willing to engage with

115
00:10:55,280 --> 00:11:02,960
our unwholesum mind states. So someone, this person who's betrayed you, you're in their in their

116
00:11:02,960 --> 00:11:11,120
presence and it freaks you out. It makes you scared, worried, anxious or angry and sad. Any

117
00:11:11,120 --> 00:11:17,120
number of things, all of those things are interesting and useful and and potentially productive

118
00:11:17,120 --> 00:11:24,240
in terms of helping us understand them and and and and become stronger than them so that they don't

119
00:11:25,120 --> 00:11:28,960
hurt us in the future. And so where we shouldn't avoid, I think

120
00:11:28,960 --> 00:11:38,160
not all the time, but but we should be open to where it where we were confident that it's just

121
00:11:38,160 --> 00:11:43,040
going to be a mental state. I'm just going to be miserable around this person, right? I'm not going

122
00:11:43,040 --> 00:11:48,400
to say anything or I don't have to communicate with them, but I'm miserable and I'm unhappy

123
00:11:48,400 --> 00:11:53,680
because that's interesting. You know, that's something that we can learn about, that we can study,

124
00:11:53,680 --> 00:11:58,480
that we can overcome and we can become free from. And you could, you know, some people might

125
00:11:58,480 --> 00:12:05,920
even say it's a good thing to to to you know, that's a good opportunity. I think there's room for

126
00:12:05,920 --> 00:12:10,080
that. I don't mean you should go out of your way to find things that make you upset, but

127
00:12:11,920 --> 00:12:17,360
I saw I wouldn't go that far, but certainly when you when when as a natural part of your day,

128
00:12:17,360 --> 00:12:23,760
there's no other reason to not be there, but you're put in a situation where you have to

129
00:12:23,760 --> 00:12:30,000
be in proximity to such a person or any anything like that, any situation, the clicking, the

130
00:12:30,000 --> 00:12:35,120
person eating that's their jaws clicking or the kids playing or the loud noises or even loud

131
00:12:35,120 --> 00:12:40,960
music. If you have no other reason not to be there, then that shouldn't usually be a reason.

132
00:12:40,960 --> 00:12:47,360
I mean, certainly an environment in which you can and should meditate and can and should study

133
00:12:47,360 --> 00:12:51,920
your reactions, your likes and your dislikes. And it would be the same if it was someone you

134
00:12:51,920 --> 00:12:58,080
were your attracted to. Should I avoid people who are who are a little beautiful or who are

135
00:12:58,080 --> 00:13:07,200
sexually attractive or physically attractive, right? Should I avoid going going near delicious food

136
00:13:07,200 --> 00:13:14,480
because I might eat too much? Well, I mean, there's the point. If it's something that would cause

137
00:13:14,480 --> 00:13:21,680
you to do something, then I think there's an important point where you say, I'll just avoid that

138
00:13:22,640 --> 00:13:28,720
because I know all the bad things are say bad things. But if it's just going to make you really,

139
00:13:28,720 --> 00:13:35,920
really crave or if it's just going to make you really, really upset, then those are things

140
00:13:35,920 --> 00:13:41,360
that generally you can deal with. Now, so if it's extreme, and if you feel like it's extreme,

141
00:13:41,360 --> 00:13:48,400
then by all means, if you want my advice, retreat, step back, absolutely. But you don't have to

142
00:13:49,440 --> 00:13:54,480
and a big part of our practices learning that we don't have to do that. People talk about panic

143
00:13:54,480 --> 00:14:00,480
attacks and anxiety is not something you have to run away from. A panic attack is just a name

144
00:14:00,480 --> 00:14:07,600
for panic that snowballs and mindfulness unwinds that. It goes in the opposite direction.

145
00:14:07,600 --> 00:14:17,920
So you can apply it when you're panicking. And I think that's about all I have to say about that,

146
00:14:17,920 --> 00:14:24,320
that's the answer. So there's what you avoid on principle. There's what you avoid because it crosses

147
00:14:24,320 --> 00:14:29,760
a line where it's too much to deal with and you're either going to do or say something bad or

148
00:14:29,760 --> 00:14:36,240
you just feel like it's too much. And then there's what you can deal with just because it brings

149
00:14:36,240 --> 00:14:42,160
up buttonholes and it's in your mind. It doesn't mean it's a bad situation, one that you can't

150
00:14:42,160 --> 00:14:47,600
meditate in because unwholesumness is to some extent something you can meditate on learn about

151
00:14:47,600 --> 00:14:53,440
and overcome and change to the point that you're in the situation without the unwholesumness arising.

152
00:14:56,560 --> 00:15:01,200
So there you go. There's the demo for tonight. Again, I'm not answering questions in the comments.

153
00:15:01,200 --> 00:15:06,880
I see all your questions. And as I said in the beginning, this is not about chatting. You're supposed

154
00:15:06,880 --> 00:15:15,360
to be listening. You're supposed to be sitting there quietly listening to the demo. So keep that in mind.

155
00:15:15,920 --> 00:15:20,800
Maybe you think at this monk doesn't know what he's talking about. That's fine. But I think the

156
00:15:20,800 --> 00:15:25,920
best answer there would be just to not come in lesson. But if you think this monk, the teacher has

157
00:15:25,920 --> 00:15:37,200
something to listen to, best is to not talk to. Peace everyone. Have a good night. I don't know.

158
00:15:37,200 --> 00:15:41,840
I have no promises that it's going to be every night or when it's going to be. I'd like to do a

159
00:15:41,840 --> 00:15:47,120
little earlier. I can do it in daylight outside. But also European people can have a chance to

160
00:15:47,120 --> 00:15:55,040
get involved. Although this is all on YouTube, this is going to be empty. So everyone wants to see it.

