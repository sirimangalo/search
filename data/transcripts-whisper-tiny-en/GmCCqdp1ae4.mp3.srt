1
00:00:00,000 --> 00:00:11,760
Hello, today I might do some time to answer a question which is asked quite often by meditators

2
00:00:11,760 --> 00:00:23,040
who are undertaken practice under my guidance or also from meditators, people who have

3
00:00:23,040 --> 00:00:30,320
come to be interested in meditation via the internet and people who have watched my videos

4
00:00:30,320 --> 00:00:33,400
on how to meditate asked.

5
00:00:33,400 --> 00:00:41,120
They've asked in many different forms but in the end it comes down to I think can be

6
00:00:41,120 --> 00:00:50,320
all summed up under one question and this is are all meditations the same or is the meditation

7
00:00:50,320 --> 00:00:56,280
which I teach something which can be used with other meditations, it's a congruous

8
00:00:56,280 --> 00:01:04,840
with other meditative styles which is something which can be applied to other meditation

9
00:01:04,840 --> 00:01:12,600
techniques and thus the question are all meditations do they all follow along the same

10
00:01:12,600 --> 00:01:23,400
line and so I would categorize meditation into two broad categories and the first category

11
00:01:23,400 --> 00:01:32,520
is what we call tranquility meditation it's meditation which helps the mind to become

12
00:01:32,520 --> 00:01:40,920
peaceful and calm without any understanding of necessarily understanding about ourselves

13
00:01:40,920 --> 00:01:49,920
of the world around us it's simply any method which brings the mind to feel peaceful

14
00:01:49,920 --> 00:01:54,960
and calm it could be reading the Bible or it could be reciting the name of Jesus or

15
00:01:54,960 --> 00:02:01,760
the name of Buddha it could be looking at a candle flame or reciting the mantra ohm or any

16
00:02:01,760 --> 00:02:06,480
number of different meditations which exist out there today which we see in the various

17
00:02:06,480 --> 00:02:13,720
religious traditions and outside of religious tradition as well the other type of meditation

18
00:02:13,720 --> 00:02:19,960
is called vipasana meditation or insight meditation means meditation which leads the

19
00:02:19,960 --> 00:02:24,840
person who practices it to see clearly about the things inside them and the things in the

20
00:02:24,840 --> 00:02:31,440
world around them and it doesn't necessarily lead the person to feel peaceful and calm during

21
00:02:31,440 --> 00:02:36,880
the time they're practicing it in fact when people who are practicing to see clearly often

22
00:02:36,880 --> 00:02:43,640
have to touch upon things which make them feel uncomfortable things which they would emotionally

23
00:02:43,640 --> 00:02:50,720
rather not touch which their feeling tells them is something that they should not touch

24
00:02:50,720 --> 00:02:55,880
something that they should avoid and should run away from but they can see over time that

25
00:02:55,880 --> 00:03:00,360
by running away from these things it only gets worse and the problem is never actually

26
00:03:00,360 --> 00:03:07,120
healed so when they reach this point they decide to take insight meditation and this

27
00:03:07,120 --> 00:03:12,440
is meditation which leads the person to see clearly about oneself and about the world

28
00:03:12,440 --> 00:03:19,440
around oneself so how are these two different meditations to be distinguished the first

29
00:03:19,440 --> 00:03:26,160
way to distinguish these two types of meditation is that by the object which they take

30
00:03:26,160 --> 00:03:31,800
now I said about insight meditation that it gives you understanding about ourselves in

31
00:03:31,800 --> 00:03:38,560
the world around us so the object for insight meditation has to be ourselves and the

32
00:03:38,560 --> 00:03:43,400
world around us those things which exist for real inside of ourselves and those things

33
00:03:43,400 --> 00:03:49,000
which exist for real in the world around us and they also have to be real things for

34
00:03:49,000 --> 00:03:58,880
instance we can't focus on our human in the human body we can't focus on our swimming

35
00:03:58,880 --> 00:04:04,200
bladder which we don't have or our gills or something in the world around us we couldn't

36
00:04:04,200 --> 00:04:10,040
focus on a horned rabbit a rabbit with horns or something it has to be something which

37
00:04:10,040 --> 00:04:16,080
is real and when it all comes down to it we focus on the things which are ultimately

38
00:04:16,080 --> 00:04:21,560
real if we want to get down to what is ultimately good and ultimately bad and understand

39
00:04:21,560 --> 00:04:26,600
the difference and come to do away with the things which are no good inside of us which

40
00:04:26,600 --> 00:04:32,560
have causes for suffering for us we have to focus on what is ultimately real so even all

41
00:04:32,560 --> 00:04:38,440
of our organs or so on we have to instead focus on defenses the things which we can experience

42
00:04:38,440 --> 00:04:43,600
as we experience them in the present moment this is the object of insight meditation

43
00:04:43,600 --> 00:04:50,960
in a tranquility meditation the distinction is that tranquility meditation takes a concept

44
00:04:50,960 --> 00:04:58,080
as the object in tranquility meditation you can't take an ultimate reality as an object

45
00:04:58,080 --> 00:05:03,280
for the simple reason that it won't make you calm and this is because three things one

46
00:05:03,280 --> 00:05:07,960
it is impermanent it's changing all the time if you're focusing on something that's changing

47
00:05:07,960 --> 00:05:11,640
you don't feel peaceful and calm right the way you tend to be upset and try to look

48
00:05:11,640 --> 00:05:17,040
instead for something which is steady which is sure and unfortunately there's nothing

49
00:05:17,040 --> 00:05:22,360
in this world which is steady and which is sure so we pick a concept instead we pick

50
00:05:22,360 --> 00:05:32,640
a person or a deity or a idol or we pick a conceptual object which we create in our mind

51
00:05:32,640 --> 00:05:36,880
the other the next thing is that when we take the world around us as our object it is

52
00:05:36,880 --> 00:05:41,960
unsatisfying and if we're going to make the mind peaceful and calm we have to find something

53
00:05:41,960 --> 00:05:47,000
which is satisfying and unfortunately in the world in ourselves and in the world around

54
00:05:47,000 --> 00:05:52,320
us there's nothing real which is completely satisfying because it's impermanent there's

55
00:05:52,320 --> 00:05:58,320
nothing which we can see last everything is changing when we see things it comes and goes

56
00:05:58,320 --> 00:06:04,640
when we hear things it comes it become and go and so on and so on and finally we can't

57
00:06:04,640 --> 00:06:10,360
take things which are real as the object in the practice of tranquility meditation

58
00:06:10,360 --> 00:06:15,600
because things which are real are not under our control and if we're going to feel peaceful

59
00:06:15,600 --> 00:06:19,480
and calm we have to find things which are under our control now unfortunately there's

60
00:06:19,480 --> 00:06:24,800
nothing which is real in this world in ourselves or in the world around us which is under

61
00:06:24,800 --> 00:06:29,800
our control we can't force our body to be the way we wanted or our mind to be the way

62
00:06:29,800 --> 00:06:35,120
we wanted we can't certainly can't force the world around us to be the way we wanted

63
00:06:35,120 --> 00:06:40,720
and so if we're going to feel simply feel peace and calm sit down and find some space

64
00:06:40,720 --> 00:06:45,680
where we feel peaceful and calm we have to take a conceptual object as our meditation

65
00:06:45,680 --> 00:06:51,680
object now what I teach and what I've been trying to teach here is maybe unfortunately

66
00:06:51,680 --> 00:06:58,680
for some people inside meditation is not tranquility meditation so if those people looking

67
00:06:58,680 --> 00:07:04,360
for tranquility meditation a way to just feel peaceful and calm during the time that you're

68
00:07:04,360 --> 00:07:10,160
sitting and temporarily and something which you have to leave behind when you stand up

69
00:07:10,160 --> 00:07:15,560
and go back to your life then this is probably not for you but for people looking to come

70
00:07:15,560 --> 00:07:20,120
to understand themselves and the world around them and come to ultimately be free from

71
00:07:20,120 --> 00:07:26,120
stress and worry and to ultimately find real peace and real happiness both in meditation

72
00:07:26,120 --> 00:07:33,120
and in their normal everyday life then perhaps inside meditation is for them the second

73
00:07:33,120 --> 00:07:38,560
way to discern the difference between these two types of meditation is in the results

74
00:07:38,560 --> 00:07:44,600
so tranquility meditation you will feel peaceful and calm temporarily and when you stop practicing

75
00:07:44,600 --> 00:07:50,160
you'll feel the effects fade away and you'll go back to your everyday normal life without

76
00:07:50,160 --> 00:07:57,680
really having gained any verifiable benefit until you go back to practice again and so

77
00:07:57,680 --> 00:08:03,440
much you practice so much you keep your state of peace and calm but inside meditation

78
00:08:03,440 --> 00:08:08,280
the result is understanding understanding about things which are suffering and the ability

79
00:08:08,280 --> 00:08:14,280
to stay with things which are cause for suffering and stress and upset so that when these

80
00:08:14,280 --> 00:08:19,360
things arise in our in ourselves or in the world around us in our daily life we're able

81
00:08:19,360 --> 00:08:24,480
to deal with them we're able to deal with them in a rational a peaceful a calm in a patient

82
00:08:24,480 --> 00:08:31,600
way without having to get down on a meditation that because the object of our meditation

83
00:08:31,600 --> 00:08:38,920
is everyday real life and so we come to understand and we come to become comfortable even

84
00:08:38,920 --> 00:08:47,080
with excruciating pain or intensely difficult situation this is a second way to distinguish

85
00:08:47,080 --> 00:08:54,360
between these two types of meditations so in brief I'd like to suggest this propose

86
00:08:54,360 --> 00:09:00,200
this as a answer to the question first of all what is it that I'm teaching when I say

87
00:09:00,200 --> 00:09:06,520
how to meditate and how this might differ or how to distinguish this type of meditation

88
00:09:06,520 --> 00:09:12,760
from other types of meditation which you might find in the world today when I've gotten

89
00:09:12,760 --> 00:09:19,560
some some questions about other types of meditation and I have to say I'm not up on very

90
00:09:19,560 --> 00:09:24,920
many different meditation techniques if you want to explain to me clearly what a certain

91
00:09:24,920 --> 00:09:33,080
meditation technique is and ask me how does this fit into the Buddhist scheme of what is

92
00:09:33,080 --> 00:09:39,880
this type of meditation and that type of meditation then I could try to answer and you're

93
00:09:39,880 --> 00:09:44,840
certainly welcome to contact me on YouTube or on my web blog or via email if you have

94
00:09:44,840 --> 00:09:50,440
my email or so on this one and I'd be happy to answer if you have any other questions please

95
00:09:50,440 --> 00:09:56,520
ask and happy to do these videos thank you for the great responses and I hope to hear from

96
00:09:56,520 --> 00:10:16,360
you again I hope you're back

