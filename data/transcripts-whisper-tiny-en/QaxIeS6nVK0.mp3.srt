1
00:00:00,000 --> 00:00:29,280
Good evening everyone, welcome to another evening, another day, I just keep coming

2
00:00:29,280 --> 00:00:55,920
now, yesterday's gone, today comes, I bet at the end of today there will be another day.

3
00:00:55,920 --> 00:01:12,920
The term for the universe in Buddhism is samsara, samsara means to wander on, so they're

4
00:01:12,920 --> 00:01:40,120
a problem with my sound, to wander on, to do nang lariya sajanang yatabutang edasana, they're

5
00:01:40,120 --> 00:01:57,320
not seeing the four noble truths, samsari tang diy gamutang nang dasudaswey vajadis.

6
00:01:57,320 --> 00:02:05,000
So we're not seeing these four noble truths, samsari tang diy gamutang nang, there is

7
00:02:05,000 --> 00:02:20,120
this wandering on for a long time, to put in mildly, diga long, this long wandering

8
00:02:20,120 --> 00:02:34,000
tasudaswey vajadisu, from birth to birth, being born, dying here we're born there, dying

9
00:02:34,000 --> 00:02:55,360
from there we're born somewhere else, this is the reality that we're faced with, we're

10
00:02:55,360 --> 00:03:08,560
unfortunately privileged only to a very short memory, and so we're unable to see truths

11
00:03:08,560 --> 00:03:23,520
like the four noble truths, we don't have this science, this knowledge, and so it's

12
00:03:23,520 --> 00:03:32,120
not merely by chance that the buddhas enlightenment came after seeing the rounds of rebirth,

13
00:03:32,120 --> 00:03:49,440
understanding the repetitive and infinite repetition of rebirth, that he saw where our ambitions

14
00:03:49,440 --> 00:04:03,920
lead us, where our defilements drag us, where our addictions keep us tied into the wheel

15
00:04:03,920 --> 00:04:20,560
of samsari, like a dog chasing its tail, that's what we are, it's quite depressing really,

16
00:04:20,560 --> 00:04:27,560
all this existence, it's not how we want to look at existence, life is sacred, there is

17
00:04:27,560 --> 00:04:33,560
great joy to be had, this is what we want to say, what we want to see, that kind of teaching

18
00:04:33,560 --> 00:04:52,480
would generally go over quite a bit better than the depressing teachings of repetition but

19
00:04:52,480 --> 00:04:59,000
the interesting reality is our acceptance of suffering, the four noble truths having all

20
00:04:59,000 --> 00:05:07,200
to do with suffering and clinging and addiction, our acceptance and our understanding

21
00:05:07,200 --> 00:05:17,240
of suffering is actually the only thing that brings true happiness, through thoroughly

22
00:05:17,240 --> 00:05:29,440
understanding, you see because we're not actually happy and we can't actually find happiness,

23
00:05:29,440 --> 00:05:38,040
you think that this round of rebirth is capable of satisfying you, that if you get what

24
00:05:38,040 --> 00:05:45,640
you want, if you have what you want, you set up your life in such a way as to provide

25
00:05:45,640 --> 00:05:57,880
for all of your desires, you can somehow be satisfied, we're not actually happy, there's

26
00:05:57,880 --> 00:06:03,760
this philosophical question about whether it would be better to be a pig satisfied in

27
00:06:03,760 --> 00:06:17,680
it and it's die or Socrates, who is for philosophers is one of the wisest men in history,

28
00:06:17,680 --> 00:06:26,120
meaning to be a wise person but suffer and be made to drink poison now, the interesting

29
00:06:26,120 --> 00:06:31,640
thing of course about Socrates is that he was at peace when he died according to the

30
00:06:31,640 --> 00:06:46,440
books anyway but the real point is that wisdom itself, true wisdom which doesn't just

31
00:06:46,440 --> 00:06:53,600
mean knowledge but it means the knowledge of which knowledge, the knowledge to differentiate

32
00:06:53,600 --> 00:07:08,040
between knowledge is, which is useful, which is useless, true knowledge is the true cause

33
00:07:08,040 --> 00:07:14,240
of happiness, that the pig can never be truly happy, you might look at the pig and say oh

34
00:07:14,240 --> 00:07:22,520
that pig looks happy, when the same goes for humans you look around you and the human

35
00:07:22,520 --> 00:07:32,400
world appears to be full of happiness, everywhere you turn open up an advertisement full

36
00:07:32,400 --> 00:07:43,960
of smiles, turn on the television watch a commercial smiles, most of our favorite television

37
00:07:43,960 --> 00:07:53,200
shows are they used to be anyway full of smiles, problems that could be solved and villains

38
00:07:53,200 --> 00:08:06,120
that could be vanquished, heroes that would live happily ever after, this is the story

39
00:08:06,120 --> 00:08:13,360
that we tell ourselves and if it were true then all would be fine, it would be good for

40
00:08:13,360 --> 00:08:21,320
us to be positive about the world but unfortunately it's not true, even those people who

41
00:08:21,320 --> 00:08:37,040
will swear up and down that they're happy would be shocked if they ever tried to practice

42
00:08:37,040 --> 00:08:44,760
meditation to realize how truly unhappy they actually are, truly unsatisfied and unable

43
00:08:44,760 --> 00:08:52,960
to simply be with their own mind, to taste their own mind, we meditation really is the

44
00:08:52,960 --> 00:09:01,480
true test right, if you're so happy let's test, we'll sit here together quietly and we'll

45
00:09:01,480 --> 00:09:15,160
measure our happiness, we'll measure our minds, we'll put ourselves in the petri dish

46
00:09:15,160 --> 00:09:20,600
and we'll examine our state of mind and we'll see that those people who claim to be so

47
00:09:20,600 --> 00:09:28,360
happy are full of stress and dissatisfaction and clinging and their happiness is very much

48
00:09:28,360 --> 00:09:39,000
dependent on people, places and things, specific people, places and things, concepts, experiences

49
00:09:39,000 --> 00:10:02,280
without those they can't be, please let alone happy, so Buddhism doesn't make grand walk

50
00:10:02,280 --> 00:10:14,040
claims about religious truths or anything, it's not about proselytizing or renting about

51
00:10:14,040 --> 00:10:21,600
the evil of defilements and so on, Buddhism doesn't create a whole new religion, that's

52
00:10:21,600 --> 00:10:28,200
not really the idea, the Buddha was, they call themselves a wee bhajawadi and someone who

53
00:10:28,200 --> 00:10:38,040
just looks at things and tells it like it is, is able to see through the, or see in detail

54
00:10:38,040 --> 00:10:43,760
just like physical scientists or material scientists are able to split the atom and find

55
00:10:43,760 --> 00:10:55,960
subatomic particles and so on, the Buddha was able to do the same with the mind, so the

56
00:10:55,960 --> 00:11:02,600
Buddha didn't create something, it's not Buddhism isn't like this, the Buddhism I practice

57
00:11:02,600 --> 00:11:11,520
anyway, it doesn't have a heaven with a Buddha God in it, granting all your wishes, Buddhism

58
00:11:11,520 --> 00:11:19,080
is just describing reality and pointing out some aspects of reality that are not terribly

59
00:11:19,080 --> 00:11:29,000
obvious or clear to us, it's such a wonderful thing to take someone through a meditation

60
00:11:29,000 --> 00:11:34,960
course because you see their eyes opening day by day, you see them swallowing this bitter

61
00:11:34,960 --> 00:11:42,040
pill of truth and they're finally honest with themselves, I mean then isn't that the

62
00:11:42,040 --> 00:11:49,040
greatest thing when you're finally honest with yourself, when you stop fooling yourself

63
00:11:49,040 --> 00:12:04,040
when you stop pretending to be something, when you finally have the opportunity to just

64
00:12:04,040 --> 00:12:13,240
better yourself and you see all your ego and all your tricks and all your mental constructs

65
00:12:13,240 --> 00:12:26,360
you've created over your life and through the practice you free yourself, it's quite

66
00:12:26,360 --> 00:12:43,960
simple, so tonight's topic then is samsara, seeing through the delusion of samsara,

67
00:12:43,960 --> 00:12:49,640
the narrative that we story that we tell ourselves about how great the world is and how

68
00:12:49,640 --> 00:13:09,480
we can find all of our desires, if only we try harder, if only we cling more, cling tighter

69
00:13:09,480 --> 00:13:30,200
to the things we love, strive after them, meditation isn't about going somewhere, it's

70
00:13:30,200 --> 00:13:37,160
the opposite of this wandering on, it's the opposite of samsara, stopping, right, you know

71
00:13:37,160 --> 00:13:42,760
the story of Angulimala or this man who, if you ever watch the time movie it's a really

72
00:13:42,760 --> 00:13:50,760
good rendition that subtitles are terrible, but at one point I was trying to redo the

73
00:13:50,760 --> 00:14:02,640
subtitles but never got finished, but in the in the Thai version which is an adaptation,

74
00:14:02,640 --> 00:14:08,160
he thinks if he kills all these people he'll become so the supreme being and become one

75
00:14:08,160 --> 00:14:14,960
with God kind of thing, here's this voice and things that Brahma is telling him to kill

76
00:14:14,960 --> 00:14:20,960
all these people, so he kills them thinking he's liberating them and it evolves in the beginning

77
00:14:20,960 --> 00:14:26,360
he's killing evil people and then in the end he realizes everyone has evil in them and

78
00:14:26,360 --> 00:14:35,560
he just starts killing everyone and finally he meets the Buddha and the Buddha can

79
00:14:35,560 --> 00:14:43,040
try his through magical power to not be caught and Angulimala chasing after him running

80
00:14:43,040 --> 00:14:52,600
through the forest, he yells out, stop, stop, you're content, nothing but and the Buddha

81
00:14:52,600 --> 00:15:07,120
says, allow Anguliman, we've already stopped, I've already stopped Angulimala, turn

82
00:15:07,120 --> 00:15:23,880
it out, he couldn't, it's you who should stop, the meditation is about stopping, when

83
00:15:23,880 --> 00:15:30,360
you create karma again and again, hopefully not by killing human beings but all the karma

84
00:15:30,360 --> 00:15:39,720
that we create, this is the wandering on, this is the proliferation and becoming, every

85
00:15:39,720 --> 00:15:50,840
ambition you embark upon adds to your journey, adds length to your journey, lengthens

86
00:15:50,840 --> 00:16:00,600
your wandering on, perpetuates the wandering, the cycle and so we're not doing anything

87
00:16:00,600 --> 00:16:07,200
special in meditation, we're just stopping, let's stop and at least take stock of where

88
00:16:07,200 --> 00:16:17,280
we're headed, try and learn a little bit about the reasons why we do things, the motivations

89
00:16:17,280 --> 00:16:27,400
behind our ambitions and better ourselves and purify our intentions and so on, but is

90
00:16:27,400 --> 00:16:37,680
a incredibly noble, the noble path, the nobility of this, the grace and the perfection,

91
00:16:37,680 --> 00:16:44,200
this is why people make these statues, how many human beings can say they have statues,

92
00:16:44,200 --> 00:16:53,040
this grandwami for them, they're the only other person I can think of let me see, I mean

93
00:16:53,040 --> 00:16:59,800
Jesus is the other one right, but even Jesus doesn't have such a, and this is the outspring

94
00:16:59,800 --> 00:17:06,840
the outpouring of such faith that people get by hearing and learning and appreciating,

95
00:17:06,840 --> 00:17:12,040
like if you've ever seen in Buddhist societies, when people come to meditation they're

96
00:17:12,040 --> 00:17:20,720
just, whether they be rich or poor, how it changes their life and how much faith and confidence

97
00:17:20,720 --> 00:17:27,280
they get, even here in the West, you know, how many people come to the meditation and

98
00:17:27,280 --> 00:17:37,680
just are in awe of the greatness and the purity of this path.

99
00:17:37,680 --> 00:17:45,680
So on that note, I'd like to express my appreciation first and foremost for those meditators

100
00:17:45,680 --> 00:17:53,640
who make it all the way here and undertake intensive meditation practice, it's you who are

101
00:17:53,640 --> 00:18:04,320
truly coming to a stop, making a stand, establishing yourself well in the present moment.

102
00:18:04,320 --> 00:18:10,600
One night we have another meditator on his way, he called me from the Toronto airport,

103
00:18:10,600 --> 00:18:19,720
coming from America, he's kind of, well he's been trying to, he's never left his country

104
00:18:19,720 --> 00:18:30,280
before and he's rather young, I think, but incredibly intent on getting here, so he's

105
00:18:30,280 --> 00:18:38,560
in Canada somewhere, on his way.

106
00:18:38,560 --> 00:18:44,040
And then to all of you who come out to listen to the dhamma, listening to the dhamma is something

107
00:18:44,040 --> 00:18:54,840
unique, uniquely special in the world, this opportunity to learn not just another philosophy

108
00:18:54,840 --> 00:19:03,480
or another, one more way of understanding the world, but to learn the way, to understand

109
00:19:03,480 --> 00:19:18,360
the world, the way to see through our delusions and to free ourselves from all suffering.

110
00:19:18,360 --> 00:19:29,280
So much appreciation to you all, and that's all I have to say, thank you for coming out,

111
00:19:29,280 --> 00:19:34,320
if you have any questions, I'm happy to answer.

112
00:19:34,320 --> 00:19:40,040
And the bell rings, that means we've got a visitor, it's one of you guys let him in and

113
00:19:40,040 --> 00:19:50,960
I'll take a perfect timing, this is probably the young guy, that's okay, let the men

114
00:19:50,960 --> 00:20:09,040
that you guys can go ahead, although you can go ahead, you don't have to, he made it.

115
00:20:09,040 --> 00:20:12,760
Javan.

116
00:20:12,760 --> 00:20:19,820
Javan, how do you pronounce your name?

117
00:20:19,820 --> 00:20:27,480
Javan, come on in, have a seat for a second, I'm just answering questions and welcome,

118
00:20:27,480 --> 00:20:30,920
glad you made it.

119
00:20:30,920 --> 00:20:38,520
Any questions, there's no questions, I'm going to introduce our new meditator to the

120
00:20:38,520 --> 00:20:50,120
Didn't dongle Malakil because he wanted to offer a garland of fingers to his teacher?

121
00:20:50,120 --> 00:20:51,600
No, no, that's not it.

122
00:20:51,600 --> 00:21:02,640
Now the original text is, I think, with the commentaries is that his teacher, he was his

123
00:21:02,640 --> 00:21:08,480
favorite, he was the teacher's pet and so the other students were jealous and they

124
00:21:08,480 --> 00:21:16,960
contrived to turn the teacher against dongle Malakil and so they convinced the teacher that

125
00:21:16,960 --> 00:21:20,000
dongle Malakil was having an affair with the teacher's wife.

126
00:21:20,000 --> 00:21:29,160
Now this is how the Thai version sticks to this more or less and he gets caught in a sort

127
00:21:29,160 --> 00:21:30,320
of a comprum.

128
00:21:30,320 --> 00:21:33,840
It's a little bit more complicated, it's actually, you know, it's really well done

129
00:21:33,840 --> 00:21:39,400
in the Thai version except for all the killing, it's just full of, most of the movie is just

130
00:21:39,400 --> 00:21:45,240
about blood and gore but the actual story behind it is really, really well done.

131
00:21:45,240 --> 00:21:50,360
But sticking to the original story, so they convinced this guy and so the teacher gets

132
00:21:50,360 --> 00:21:56,520
convinced and wants to, as a result, destroy Angulimala so he says to Angulimala that

133
00:21:56,520 --> 00:22:00,600
in order to graduate, he has to, something like in order to graduate, he has to bring him

134
00:22:00,600 --> 00:22:07,040
a thousand, or he has to kill a thousand people and so what Angulim, the finger thing

135
00:22:07,040 --> 00:22:12,360
is just in order to count them because he, how's he going to count a hundred while he

136
00:22:12,360 --> 00:22:20,720
cuts the thumb maybe off of each, off one hand of each person he kills and he makes a necklace

137
00:22:20,720 --> 00:22:25,880
so he can count, that's how he can count a thousand.

138
00:22:25,880 --> 00:22:32,160
So he becomes known as Angulimala, now the actual texts are quite sparse, they don't have

139
00:22:32,160 --> 00:22:37,960
a lot of, I don't have any of this commentary, it's just, there's a story of an abandoned

140
00:22:37,960 --> 00:22:44,920
called Angulimala who cuts people's fingers off but the commentaries have all sorts of

141
00:22:44,920 --> 00:22:51,000
stories about him and then there's a suit, giving a story after when Angulimala is already

142
00:22:51,000 --> 00:23:02,360
among, I guess that's the Angulimala suit, right, no, it's, yeah, so, you know, the

143
00:23:02,360 --> 00:23:24,200
his garland of fingers wasn't the gift to his teacher, that's just a count, no, the whole

144
00:23:24,200 --> 00:23:27,960
killing people because he wanted to send them to heaven, that was the Thai version, really

145
00:23:27,960 --> 00:23:34,600
well done, it's really, I mean it's not canonical but it's a really good story, I don't

146
00:23:34,600 --> 00:23:40,800
know if you've ever seen the short video I did with clips of it, there's a really good

147
00:23:40,800 --> 00:23:50,240
section that I put together and put on YouTube a long time ago, take a look at it and

148
00:23:50,240 --> 00:23:58,400
get a sort of a taste of what the movie's like, there was a lot of controversy surrounding

149
00:23:58,400 --> 00:24:04,360
the film because it wasn't, it wasn't quite canonical and there was a bit of a romance

150
00:24:04,360 --> 00:24:13,120
between Angulimala and the wife, she ends up following him into the jungle and they escaped

151
00:24:13,120 --> 00:24:19,760
together but they never, it's very careful not to actually have them be in a romantic

152
00:24:19,760 --> 00:24:35,960
relationship and he eventually becomes a monk of course, okay, I'll let you all watch

153
00:24:35,960 --> 00:24:46,720
that video and I'm gonna sneak out the back door and go and meet my new meditator, so

154
00:24:46,720 --> 00:24:53,720
have a good night everyone.

