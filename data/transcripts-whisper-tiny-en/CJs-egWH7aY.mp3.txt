Yeah.
You're not aware of it.
Sure, do you remember what there is?
.
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
..
No Sunday, Kinko, Akkali, Ko
Hey, if I was to go, when I go,
But that time we didn't move in your evening
No, I'm gonna give you down your evening
But I'm sorry, no, I'm gonna tell you
Yeah, just don't have the time to eat
Just don't have enough time
But two fun nights a year somehow
And one time so bad
19 years, I don't know
And some homies, I don't know
Don't move me, I don't know
18 a side to 18
Oh, do me, take your mom better than
But some homies, I'm gonna tell you
I'm gonna tell you, I'm gonna tell you
Don't move me, Akkali, don't know
So I'm gonna tell you, I'm gonna tell you
So, part people, no, my God, go
So, look at some, go
When you're part people, no, my God, go
So, look at some, go
Yeah, part people, no, my God, go
So, look at some, go
So, all these times have blocks
I don't know what that's going
Yeah, see down dead, oh, in the
So, look at some, your God, oh,
For it's a fool, go down
It's a log, oh, oh, so
Oh, there it goes
Come!
Oh, what, what, oh, pop
That, what, what happen
Was crystals, or odor
fruits, what happen
Yes, I'm a lady, she knows what's about you, we'll tell you, I'm a girl, but I'm a girl,
I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl,
I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl,
I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl, I'm a girl,
then ma'am.
...
And then, uh...
Okay.
Okay, so that's okay with chanting.
Okay, okay.
Okay, okay.
Okay, okay.
Okay, okay.
Okay, okay.
Okay, okay.
Okay.
I need someone to
Do you know what to do to start recording?
Oh.
Do you know the red button?
Yeah.
so you can stop it and try it again.
I need you to sit there because you're going to have to turn it off when you're finished.
Okay, I have some more.
I'm going to turn the screen back so you can look at it.
Okay, when once you've started recording, just give me the thumbs up, so I'm sure it's done.
That light is in it.
Did you just, it shouldn't have been in it?
Oh, you mean the stairs?
Oh, yeah, it's fine.
Okay, welcome back to our study of the double palette.
Today we continue on with verse number 50, which reads as follows.
Not very similar, manin, but it's some guitar guitar.
I don't know how big he got any guitar guitar guitar.
Which means.
Not very similar, with all manin.
One shouldn't concern one's house, one's house with.
The faults of others, not very similar, you know, not the faults.
Not very similar, not very similar, not the faults of others, not the deans that they have done or the things they have left under.
At the normal level, okay, a bunch of concerns, one's house or.
No concern, one's house, one's house, I don't know.
Indeed, one should concern one's house with one's house.
Katani and Katani in terms of what one has done and what is what one has not done.
In English, one shouldn't concern yourself with the deans and the omissions and the faults of others.
If you concern yourself with the things that you have done and the things that you have done.
So rather, it's actually rather general teaching that can be applied in different ways,
in different senses, but the sense here is actually a little bit specific in terms of the story.
So which goes as follows.
There was once a woman living in Salaty, in the time of the Buddha, who looked after a certain naked ascetic.
One of these.
Requeses who decided religious practitioners, followed this idea that through torturing one's house,
that's through abstaining from any form of comfort, any form of physical comfort, one could.
They had the defound, burn away the defound, so they had the spread of the defounds where they would torture themselves, or by burning themselves.
It's difficult to see.
From the hardship, there's a sort of dappo which means a feat.
The real and the true problem, or the true cause of suffering, is passion, is lust, is essential desire.
And so the torturing, the undertaking of things that are unpleasant is a means of abstaining from this sort of attachment.
But of course the Buddha pointed out that there's another kind of attachment that is the aversion that comes from torturing yourself.
Or this repression that comes from abstaining from things that's cultivating this forceful will of denying yourself.
Extreme practices, when you never have the opportunity for the arising, sensuality, and you can't understand.
So you go to the extreme wasting away, which they don't have to do.
They don't have to make it, but they don't have to starve themselves.
When you starve yourself, you have very little of the defound, it's a rather little addiction or attachment to rise, and so it seems like you're actually burning it up.
But of course this has nothing to do with the underlying causes of the attachment, and the addiction which is ignorance and delusion.
Just a misunderstanding.
Or belief that you're going to find satisfaction is wrong with these, which can only be eradicated through wisdom.
So of course it's not the right framework within to free ones.
This is sort of the common thing that you'll find.
Without having understanding of the Buddha, it would be rather that you find a huge wide range of religious practices like this in the world.
All because people are not, simply because people are, these beings are not enlightened, so they come up with an idea based on their whim, based on what suits them.
Often just based on a whim, something that strikes them as right at a given time, or something that strikes their fancy.
And you can start more religions this way, where people think that they really believe something, but actually it just comes about by chance in the mind.
This is where the Buddha was very good at seeing through.
And he would ask tough questions like, well, why do you feel, is that really an objective practice that you follow?
What good really doesn't do it to go naked? It's a really intrinsic benefit to it. It's not just, I believe going naked is going to make me enlightened, or I believe starving myself.
It has to be based on causing the fact that has to really do what you say it's going to do.
So they would have all these crazy beliefs, like if you do this practice, you would have it, but similar to religious traditions today, and they have these beliefs. If you, for your whole life, you go naked, as a naked aesthetic, do it as a practice.
Then you'd be born in heaven, or you could even become enlightened or something.
If you starve yourself to death, this is a common practice in certain religions in India, from the traditions you starve yourself to death, you're assured of a place in heaven.
And of course, there's no logical reason to leave such a thing, does it purify your mind, does it starve yourself? I think it's highly debatable in fact, I would say, certainly doesn't purify your mind, not even of itself.
Purification of the mind has to come about through qualities of mind that can't come about through bodily status.
If a person who starts himself and still clings to wrong views and wrong beliefs, there's nothing about starving yourself that eradicates these.
So, anyway, this was the standard practice in India, and so there was this, this aesthetic, who was looked after, they said like a son, she looked after this aesthetic like he was her son, and so he would always come to her house and receive the finest of offerings.
And he got kind of, I guess it was a starving himself, certainly wasn't according to the story.
But he was certainly going naked, and so that was seen as something very difficult to do, because most people have difficulty wondering around naked, especially in India full of force flyers and dear flies,
all sorts of stick creatures, and that crafters, if you're naked. So, it is actually quite painful, of course, under the hot sun, naked, and so on.
And having to deal with the cold, having to deal with the wind, having to deal with the heat, and the sun.
And without wearing a single piece of clothing. And on top of that, the shame that comes in the embarrassment that comes from being naked, it's not an easy thing, so it is a pretty impressive practice, and this is what impressed this woman.
But, of course, the fly in the oatment was that everybody else in Salut, you are more and more people in style with you are going to date the one that I've listened to the Buddhist teaching.
And so there was less of an inclination to practice, or to all of these naked settings, and to entertain them and to feed them.
So, they, their wealth and their luxury started to diminish, their fame started to diminish, that we were just going to see the Buddha.
And this woman, of course, she heard about all her neighbors going to see the Buddha, and so she thought, well, maybe I should go in here when he has the same sense.
You know, in India it wasn't, they didn't have religions in that time, it wasn't, I belong to this religion, I belong to that religion, so people would go to different teachers and ask them the same questions and try to get teachings from different teachers.
There was no sense of this religion, that religion, there was only, what does this teacher teach, what does that teacher teach?
And so it would be like the following of this teacher, the following of that teacher, but people wouldn't identify with this religion or that religion, not so much.
And so she didn't think it would be real, you know, you wouldn't go here this sad, this religious holy man.
And so she thought, well, I should ask my revered teacher.
And this is the naked aesthetic, and so she went to him and she asked, she said, don't you know, look, I hear this, there's someone who got the minds and wanted to teach her and wonder what you think, should I, would it be okay if I went to teach?
And the naked aesthetic, of course, hasn't eradicated his defilements and starts to get a little bit concerned because he's heard what happened to his buddies, all the other naked aesthetics.
So when their students went to listen to it, someone who moved to my head to say, somehow he had this magic that converted them, I think talked about the Buddha's converting magic.
They wouldn't entertain the thought that his teaching could be any good, but he thought he had some kind of magic that allowed him to convert their students.
Kind of, you know, hypnosis or like a love potion or something.
And so he became quite distressed, you know, proceeding, let it show, and he said, no, no, no, no, no.
Try to be common, said, no, no, no, no, go see him.
He teaches the wrong thing, he teaches bad things.
He's just misleading people, it's kind of things.
Of course, he didn't really understand the Buddha's teaching, didn't probably have heard the Buddha's teaching, probably just heard rumors of the Buddha taught and so and so.
You have no reason to say, I've just decided the fact that he was afraid of his own, well, he has own likelihood.
But he managed to dissuade this woman from going, and yet she, you know, kept hearing about how great this, I'm going to go to the teaching was and probably
some students and snippets of what he taught.
And so she, you know, again and again with the pastor that they can study.
You know, look, what do you think of this teaching and person couldn't answer?
And so she would ask, you know, look, I really want to go see this.
Someone to go to the man's and do what he has to say.
No, no, no, don't go wrong, it's bad, it's evil.
And so finally, she kind of got frustrated and said, well, there's no reason why I shouldn't hear the teaching.
So if he's not going to let me go to see the Buddha, then I'll just invite the Buddha to come to see me.
And so she told her son to go to invite the Buddha for a meal the next day.
So her son agrees and is on his way to the monastery to see the Buddha, but he thinks,
I wonder what they wonder with our teacher naked aesthetic.
It's naked aesthetic, we'll think of this.
Some of you want to start talking about it.
Anyway, he goes to see the naked aesthetic.
Hey, so where are you going? I mean, my mother wants me to go to bite to someone and go to my food to,
for the meal for more money.
And he's, oh, don't, don't invite him. He's wrong, that's wrong, that's bad.
He's going to take away all of our, he's going to take away my life.
He's like, don't, don't go to, here's what you do.
Don't, don't go tell someone and go, don't go tell this, go to my, don't go and invite him.
And just go home and tell your mother that you invited him.
And then when he doesn't show up tomorrow morning, the two of us will split the food together.
All of the good food that she's given off.
If you don't go out and give you an equal share of whatever food she's made for Buddha.
And he's like, oh, I can't do that.
Kids got a little bit of scruples. He says, my mother will yell at me if I do that.
I can't do that.
So if they make some pleats, he says, no, no, don't go, don't go, don't go, it's wrong.
It's going to destroy me, please.
And, and, really got, I'm upset at this point.
And the son, son keeps refusing and says, no, no, I have to go, I don't go.
No mother might yell at me and he says, okay, well do this.
So go and invite to someone and go with them.
But don't tell them where you live.
And don't tell them you live.
Don't tell them that straight.
Don't tell them your head hurts.
Don't tell them even what part of the city live.
And just go and invite them for tomorrow's meal.
And then run off in a different direction.
As though you, you lived in that direction.
Don't run off in a totally different direction.
And so he goes to the, he goes to the Mark J.
Devanan is a assistant, assistant, but they actually want to invite you
to put more money.
But meal tomorrow morning.
And then indeed he runs off in the wrong direction.
Goes to see the said egg tells them what he does and great.
So tomorrow morning, we'll share this.
Share the meal.
There's no way in the entire.
The son will go to go find your house.
Which of course is not true.
The commentary points out quite clearly that the Buddha,
this is the one thing that the Buddha knows is the paths that we live everywhere.
And it says the Buddha, when he became enlightened under the Bodhi tree.
And you see, but nothing.
Good guy.
All of the paths, all paths became clear to him.
He knew that he knows the path to hell.
The path to the animal realm.
The path to the hungry ghosts.
The human realm.
The path to the angels.
The path to become a God.
And the path to the Bible.
How could he not know the path to this woman's house?
The word is a little bit of a different kind of path.
And most of us even know we practice meditation.
We don't never get this kind of knowledge of the Buddha that is able to find people's houses, for example.
But the Buddha had such powers and was able to,
you know, the point being that his knowledge was able to encompass the whole universe.
I think that I understand such deep things as the arising and the passing of the Ming.
So on the night, it was enlightenment.
One of the knowledge was he was able to see beings.
Like actually in his mind, watch games dying, watch them being born,
watch them coming, watch them going, and watch them go according to their desserts.
He was able to visualize what to look to oversee the process of birth and death.
So he was able to see the paths that went to this realm and that realm.
But just that power of mind was such that he could then apply it in any direction.
He chose kind of like psychics.
And in fact, this is a common phenomenon that it's claimed by psychics is this ability to remote viewing.
And in fact, there's online courses you can take or tutorials on how to practice remote viewing.
So that someone, you have a friend and you send them somewhere and then you think about,
where are they?
You start to question, where are they?
And for some people vision, start to emerge.
You're able to see things that they're seeing.
You know, of course, this is like a firefly, the Buddha was like the blazing sun.
His wisdom is known.
It's so clear. It's precise. He didn't have to use a friend. He just knew where my husband's house was without any difficulty.
So there's no problem in that thing.
And these two two miscreants are sitting there waiting for their just desserts and then the Buddha shows up.
It's down and is served by the woman.
He says breakfast.
And the aesthetic is hiding in behind.
He sees the Buddha coming and doesn't want to meet the Buddha and just hiding in an inner chamber.
And the woman comes out and she serves the Buddha with all this great food.
And the Buddha's finished, the Buddha starts to give a talk.
And he starts to describe the benefits of this woman's deeds and all the good that she's gained through offering food to a holy person and so on.
And she's excited and she's pleased by this and so she says,
I'm glad to hear that kind of thing.
And the aesthetics in there, sitting in the back and he hears this woman just falling for the Buddha's tricks he thinks.
He comes out, he storms out, he starts abusing this woman and he calls her hide.
A witch and all these horrible names and starts abusing her and starts abusing the Buddha.
And she calls them both such terrible names and so on.
Commentator doesn't go into detail.
But this upsets the woman quite violently because this was her teacher who she thought was a holy man here to see him put on such a display and to abuse her.
And really, part of it's not just out of shame for what he did, but it's out of a feeling that she had done something wrong, of course, because when you trust someone when you put your face so much in someone.
And then to have them so violently or aggressively attack you.
And it cannot make you doubt even the truth and doubt what's right and make you wonder what you did wrong if you're guilty and so on.
And so she was quite moved and distressed by this and trying to think how quickly you know what to do, I mean that she just lost her soul support her teacher.
And I'm appreciating she's coming to his bad graces.
And so she wasn't able to listen to the rest of the Buddhist sermon and discussion.
The Buddha realized this and asked me, are you not able to listen to my talk, you're not able to be able to focus and be attention to someone.
Just so disturbed by that, by my teachers' words, by his actions, by his behavior.
I can't concentrate at all.
And the Buddha said, oh, you don't worry about that.
Don't worry about his behavior.
One shouldn't pay attention to the needs and the things that other people do and the things that other people say,
one should look at one's own needs and the things that one is done.
And this is where you need this verse, not putting something on my head, some get back, and so on.
So it actually is for a fairly specific,
very specific situation or circumstance.
It's the idea of not letting people get to you when they attack you,
when other people do and say things that harm you, that upset you.
Do not let other people's deeds upset you.
But it's the first, I think, as with many of the verses, it goes far beyond the actual circumstances of the story.
So the most obvious one that comes to mind is when we compare ourselves with others.
Not very some, you know, man, where we look at the faults of others, where we criticize others.
And you can see that in this story, it's rather because the Buddha is somehow indirectly criticizing the society.
I'm not saying, don't be like him.
You don't like the way he acted, but don't be like him and more of him.
And criticize him. You're like him criticizing you, don't go back and criticize him.
Meaning that this is, his behavior is far worse, don't descend into that terrible behavior.
And this is, of course, the most important aspect of this verse is the criticizing others, finding fault in others.
And the funny thing about finding fault in others is it's often because we see fault in ourselves.
Often because we're unsure of ourselves, or if it's always because we're unsure of ourselves.
A person who is sure of themselves has no need to seek out the faults of others.
They might point out for information, say, or even criticize in as far as offering information or insight into a person's behavior,
whether it's good or bad, but they have no intention or desire to seek out the faults of others.
They have no fear or no concern or a person being better than them.
And they have no fear of themselves being worse than others.
They have no sense of it because they're confident in their purity and their behavior in their goodness and their righteousness.
But so you see this in weak people, people who are weak beloved and attack others.
Even though they put on this air on being righteous and self-righteous and being right,
it's because of their own instability of mind that they have to attack others.
When you might as stable and you might as clear, you have no reason to worry about them.
But it is an interesting twist that I just put on in this story is that even more than that,
we shouldn't rely on the opinions of others.
We shouldn't be so worried about whenever people insult us,
whether people say bad things to us.
That shouldn't be a defining factor in how we see ourselves.
So someone insults you as this setting.
There's no reason for this to be the defining factor in how we view ourselves.
And this is often real problem.
It's a problem directly here when we do receive insults and people attack us and how much this hurts us.
How much we let this affect us.
There's also this ever-present sort of societal influence where we have this self-image that we have to portray.
We have to be someone more worried about how other people think.
There's a teacher I'm worried about how I viewed by my students and how people on YouTube are going to view this video.
And I think I would like it.
There's this concern that we have for other people's engagement of us.
Now, praise all of us is crippling.
And it's to be discouraged.
It makes us lose focus.
It stops us from achieving our goals.
It stops us from being happy.
It stops us from finding peace.
So much stress comes just from this.
We call this conceit, comparing ourselves to others, comparing ourselves in terms of seeing ourselves as less than others, seeing ourselves greater than better than others, trying to be equal to others and comparing ourselves as to.
Am I equal to them? Am I good enough for them?
Are they better than me?
These kind of concerns that we have.
This is on the level of what the Buddha was saying, which is actually at a deeper level.
And there's yet another level that isn't really mentioned here, and that's in terms of, although it might be, you might find out that it's seen in this teacher's behavior, how concerned he was for his student.
And this is in terms of worrying about other people's needs.
I'm worrying about, so he was worried about this woman.
I mean, you might actually cynically say that this guy was just worried about his own livelihood.
And I think to some extent that's probably true, but it still points to the fact that he was worried about her behavior, concerns about her behavior.
And this is another important application of this verse where we worry about the wellbeing of others.
We spend all our time obsessing about the happiness of others, trying to make other people happy, trying to please them, or trying to instruct them even trying to.
And keep them on what we think is the right path for them, offering them advice and teachings and pushing ourselves on pushing our opinions on them, worrying about other people.
In some sense, it's much easier to do than to worry about yourself.
This is, there's another, there's two teachings that parallel this verse and one of them is the teaching of what it gave on these two acrobats.
There was a father and a son and a father to acrobats.
And the father said to the son, look, you get up on my shoulders and watch, you watch my feet and I'll look at all, I'll take care of your feet.
And you watch where I'm going and I'll watch where you're going and we'll perform our tricks that way and the, the stuns are crazy, that's not how acrobats perform.
You have to, when you walk, you watch your feet, I'll get up on your shoulders and I'll look after my feet and you just watch for your feet.
And what I said, this is, this is how we should see our relationship with others.
Pay attention to our own step, pay attention to our own deeds and not concern ourselves.
We're doing their own job and we're doing, we'll be performing properly.
The problem is that we don't concern ourselves with ourselves and concern ourselves with others.
And so the other teaching is the one in the Mahabaga that describes the, right, the time just after the Buddha's enlightenment where meets this group of young men who had hired a prostitute or some sort of
an escort and something like they had 29 of them and girlfriends wives, but one of them didn't.
And so they got her, they got him an escort and some kind of courtesan.
And so they went up into the forest and they were playing and then they all kind of took a rest or a CST in the forest and this, this courtesan, she, while they were asleep, she stole all their, their jewels or else they were swimming or something.
She grabbed all their jewels and ran off of them.
So they came back and, or they woke up, I can't remember what it was and found that their jewels, all their, their possessions were gone.
And they realized that this courtesan had stolen them and so they started chasing after looking for them trying to find him.
And then they met the Buddha at first, the Buddha happened to be in the forest.
He was heading to heading back to Rajagahap, or Uruvayla first.
And so he stopped by because they knew he'd meet these guys and wanted to meet them first.
They came up with a Buddha and they said, hey, have you seen this woman? Have you seen a woman in the forest?
But it's a woman to you.
What's this woman to you?
They said, she stole all of our jewels and all of our possessions and stuff.
So we're looking for him.
They said, well I'm telling you a man.
That is a greater benefit to you.
What is better for you?
That you should find this woman or that you should find your, that you should seek out this woman or you, that you should seek out yourselves.
Alright, go ahead and get started teaching.
But it's one that we always refer back to, seek out yourselves, seek yourself.
The ways that go waste, go waste the deep.
Yeah, go waste it is this verb, let's use many times when in many places when we go to, to describe the search, where we search for things that are, yeah,
subject to old age sickness, death, defiantly, and so on.
So this is like kind of like the, you have in the Bible where Jesus said, you know, let the dead break your dead or,
one has to lead, you have to leave behind your family and so on.
Once you're giving them this sort of spiritual push to help them to see what's really important.
And he said, well why don't you, why are you so concerned about worldly things?
Why does this, why does it, why is it that worldly things?
A lot of things, so much.
The things of other people, the things that you can't take with you anyway.
Why don't you look for yourselves?
And so this is, there's another sense in terms of worrying about the deeds of others,
worrying about what they've done wrong and trying to correct, trying to amend other people's deeds or fix other people's problems.
And all of these, of course, I'm very much to do with meditation.
It has to do with the difference between concepts and reality.
Reality is, in some sense, solipsistic, we exist in our own universes.
We never really experience other people's universes.
We only come into contact with other people's universes and experience them indirectly.
And so all that's real is our own experience.
All that we can understand is being real and existing.
Is this right here now, or seeing, or hearing, it's really interesting.
And so, part of the, that which takes us out of this reality is our engagement with other people, our engagement with the
goings on other people.
This is why it doesn't work to watch someone else's foot.
You can watch rhyme going and I watch where you're going and everything will come.
This is a level of a layer of abstraction that destroys any ability to function.
And so that seems like a silly example, but it actually is directly, or it's on the same level as trying to fix other people's problems.
I think it's so easy to fix other people's problems.
This is not something.
The point is that it may indirectly help them, but it takes taking you out of reality.
It doesn't lead to happiness.
It doesn't lead you to peace here that you just more and more and more try to fix other people's problems.
And because that's dealing on the level of concepts, it's something that never ends.
It has no satisfaction.
It doesn't lead you to find true peace and happiness.
It just goes on forever.
Your mind becomes more and more distracted with other people.
And as I said, because it's easier, it's much easier to deal in concepts.
Much easier to give other people advice because giving advice is the easy part.
The difficult part is following the advice and practicing according to the advice.
So meditation helps as it helps us on the level of not thinking the needs of other people seriously
in terms of getting upset by them, even to the extent of the well-being of others.
So we work always for well-being and never work for the suffering of others.
But we don't have this sense of stress and suffering.
What other people follow to get with the suffering of other people?
When people who love dying pass away or act in a way that is dissatisfied,
an act in a way that is unpleasant for us, which, of course, is the big deal with our relations with other people.
The reason why we suffer from others is because we expect them to act in a certain way,
like this woman expected her teacher to act.
Obviously expected him to be kind and caring and flattering as he would be,
as long as he was the one getting the praise, the praise as a teacher.
But then as soon as he felt threatened, he attacked her,
which, of course, is something that is upsetting after being so accustomed to such flattering,
pleasing, honey-coated words.
So I think I'm going to speak from this aesthetic.
So the attachment that really exists,
another example of the attachment to pleasure and attachment to the positive side of life,
he meant that leads to an inability to accept more displeasing aspects of experience,
which is, of course, it would directly work confronting and meditation,
helping us to see through this and to experience things just as they are,
and by tapping this inside ourselves.
So that when other people abuse us or scold us or insult us or manipulate us,
we don't get upset as a result of the things they do and say,
we're not worried about what other people think.
This is another level.
We realize that it really has,
but we realize generally that the most important thing is our happiness.
And we get a clearer sense of this,
and a sense of what it is that's causing us suffering.
It's our need to experience certain states and not experience certain other states.
For example, our need to receive praise.
So we try to live up to other people's expectations.
This woman felt like she had not lived up to her teacher's expectations.
And that's a result that was kind of a feeling of suffering for her.
Because we get such pleasure when people pray since when they say,
oh, what a good person you are about, what a smart and kind of.
What a funny beauty, intelligent person you are,
and a beautiful person who are.
So we expect all of these things.
And we expect this kind of praise.
And so we work hard for it.
We think that that's satisfying.
And as a result of a misunderstanding,
the experience and thinking that happiness can be found in external things and things.
And thinking that there's somehow some sort of accumulative satisfaction that can begin from this.
Which is after it, and those will result are upset when we don't get it.
And in moral, where it helps us to let go of the.
The problems of other people are not worrying so much about their wellbeing.
It's under their progress is their problems.
And meditation helps us to see things from our own perspective.
And to realize that everyone has to follow, has to watch their own feet.
It has to concern themselves with their own wellbeing and their own good deeds.
Not worrying so much about how others behave.
So this is kind of what you see through the meditation practice.
The more you meditate, the more you realize.
The more you focus on all of the reality.
The more you realize that there's no benefit that comes from stress,
from worrying, from concern, from other people, from the deeds.
It only puts you in dependency.
And this is kind of the wonderful thing you realize in meditation that you find a means of becoming invincible.
You find a means of not being subject to being vulnerable to the abuse of a giver to the wounds of others.
Which is very difficult.
It's something that we would never, we would never even think possible.
If we're not practice meditation, because we live on this conventional level, I think it's impossible to avoid.
Other people saying bad things to you, therefore it's impossible to not be affected by it.
Because we think there's something intrinsically bad about bad speech and bad deeds.
And about disheartress dissonance and so on.
Through the meditation, we realize this isn't so that it's only our reactions to things that are the problem.
And that, in fact, no one can ever hurt us or process suffering unless we are vulnerable, unless we make ourselves vulnerable.
And so it's for the meditation through seeing the difference between reality and concepts and experience and mental formation.
We live on a level of experience, when someone says something to us, we know that someone's saying something to us.
When we think something, we know we're thinking something.
So we never have any of these concepts, or we never fall prey to many of these concepts, but, oh, this person did that to me, this person did this to me.
It was quite a simple concept, we just live in reality.
When you hear something, it's just hearing from me.
When you dislike something, it's just disliking.
When you like something, it's liking.
So you slowly work yourself out of it.
In the end, you stop liking things or something, disliking things.
The fine happiness inside, you find peace inside.
And you experience things as they are of how you would like them to be or how to be.
How you believe them to be.
You really understand them as they are.
There's no projection, there's no extrapolation.
There's nothing superfluous or not chemical additives.
It's all natural.
You'll experience reality as it is.
And so that, like, it harm you, it's like my teacher once said, if someone calls you a buffalo,
just look, turn around and see if you have a tail.
If you have a tail, you're not a buffalo.
Because in Thailand, it's a real insult to become a buffalo.
It's not really something you should be upset about or concerned about.
And this is an epitomizer sort of the concept here.
If something's true, you accept it as true.
If something's false, you accept it as false.
If something's right, you accept it as wrong.
But there's no reason to get upset about it.
If someone calls you a buffalo, you just know that it's false.
I mean, why would you get upset?
What reason do you have?
What benefit comes from getting upset?
And this is really the point.
Is without imagination practice, we do think that there's some benefit to clinging.
And we think there's some benefit to getting upset.
If you think there's some satisfaction that comes from all of this,
from comparing ourselves to others, from trying to live up to other people's expectations,
from worrying about other people's deeds and misdeeds.
All of which is false.
Through the meditation practice, we come to see that this is not the truth of reality.
But none of this is real in our history.
And there's no satisfaction or happiness that comes from that.
So that's the verse for today.
And that's the story that goes behind it.
Thank you all for tuning in and watching us and continuing to keep up with our slow progress through the Bhupada.
Please tune in next time.
I hope that this has been beneficial and useful and helps all of you to progress on the path and find peace, happiness,
and freedom from suffering. Thank you for tuning in.
Have a good day.
Okay.
So, we're going to have narrow meditation.
Can you turn off the mic?
There's you have to open it up.
Push the button, hold it down.
And then turn off the camera as the dial on the right.
The big dial on top.
Just turn that in.
Okay.
Okay.
Okay.
And turn off the camera as the slide is from.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
Okay.
