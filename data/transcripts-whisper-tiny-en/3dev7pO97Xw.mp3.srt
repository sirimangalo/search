1
00:00:00,000 --> 00:00:06,500
For me, when concentration is good, phenomena appear in a certain flux centered in the heart region.

2
00:00:06,500 --> 00:00:10,500
Any advice on avoiding delusion and attachment around this?

3
00:00:10,500 --> 00:00:18,000
For example, the mind sometimes reduces images to represent the experience, since it's a very spatial experience.

4
00:00:18,000 --> 00:00:28,000
Well, studying the abidhamma could be one fairly crass answer.

5
00:00:28,000 --> 00:00:33,000
To the point is just to break things down into what really exists.

6
00:00:33,000 --> 00:00:36,000
So when you see something, they're seeing.

7
00:00:36,000 --> 00:00:41,000
And the point is to get to the point where an image is just an image.

8
00:00:41,000 --> 00:00:48,000
And the way to avoid delusion is to stop the associations.

9
00:00:48,000 --> 00:00:51,000
That means this and that means that.

10
00:00:51,000 --> 00:00:54,000
The heart region is one more.

11
00:00:54,000 --> 00:00:59,000
For example, if it's your case, people will start to feel tension or so on

12
00:00:59,000 --> 00:01:02,000
or the heart beating irregularly and they'll become afraid.

13
00:01:02,000 --> 00:01:07,000
They'll think, oh, I'm going to die. I'm going to have a heart attack or so on.

14
00:01:07,000 --> 00:01:15,000
The point is to avoid any sort of projection in this way.

15
00:01:15,000 --> 00:01:18,000
Other people will have an experience.

16
00:01:18,000 --> 00:01:21,000
For example, here, this is the third eye region.

17
00:01:21,000 --> 00:01:23,000
They say, oh, my third eye is opening up.

18
00:01:23,000 --> 00:01:26,000
And I get a lot of this because people actually believe in the third eye.

19
00:01:26,000 --> 00:01:30,000
They'll say, I had a third eye opening experience and so on.

20
00:01:30,000 --> 00:01:37,000
And it's total, well, it's valid in one sense. I mean, they're having some experience.

21
00:01:37,000 --> 00:01:41,000
But there's the proliferation, the idea of the chakras.

22
00:01:41,000 --> 00:01:45,000
We don't teach chakras because that's an extrapolation. The chakras don't exist.

23
00:01:45,000 --> 00:01:51,000
There may be some activity arise in a certain region, but that's just activity arising.

24
00:01:51,000 --> 00:01:56,000
It's a feeling or it's a sight or it's a sound or whatever it is.

25
00:01:56,000 --> 00:02:01,000
Anyway, if you read the Buddha's teaching, it actually seems boring and dull and dry because

26
00:02:01,000 --> 00:02:06,000
it's trying to bring us to the point where we can see what really is there.

27
00:02:06,000 --> 00:02:09,000
And that's just seeing, hearing, smelling, tasting, feeling and thinking.

28
00:02:09,000 --> 00:02:11,000
That's the truth of reality.

29
00:02:11,000 --> 00:02:15,000
The best way to avoid delusion is to focus on things like this.

30
00:02:15,000 --> 00:02:19,000
The five aggregates, the six senses.

31
00:02:19,000 --> 00:02:26,000
And the four satipatana.

