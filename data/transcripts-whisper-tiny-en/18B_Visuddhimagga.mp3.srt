1
00:00:00,000 --> 00:00:12,720
I don't know why he made this mistake, so some say it should be in front, okay then

2
00:00:12,720 --> 00:00:35,720
it's a long quotation from the particular, how let's go to, next page 294, it trains

3
00:00:35,720 --> 00:00:43,220
thus I shall breathe in, I shall breathe out, experiencing the whole body, now he trains

4
00:00:43,220 --> 00:00:49,340
thus I shall breathe in, making known, making plain, the beginning, middle and end of the

5
00:00:49,340 --> 00:00:54,520
entire in-Breath body, and I shall breathe out, making known, making plain, the beginning,

6
00:00:54,520 --> 00:01:01,440
middle and end of the entire out-breath body, thus he trains, making them known, making

7
00:01:01,440 --> 00:01:08,080
him playing in this way, he both breathes in and breathes out with consciousness associated

8
00:01:08,080 --> 00:01:14,760
with knowledge or understanding. That is why it is that he trains thus, I shall breathe

9
00:01:14,760 --> 00:01:27,600
in and shall breathe out. Now, yes. Now, first, the English translation is experiencing

10
00:01:27,600 --> 00:01:34,960
the whole body. But the explanation given in the commentary is not experiencing, but making

11
00:01:34,960 --> 00:01:43,560
clear, making known, making playing. That means trying to see the breath clearly. The

12
00:01:43,560 --> 00:01:50,480
word, the body word, but it's how it really can mean to experience. But here, it is

13
00:01:50,480 --> 00:02:01,280
explained as meaning to make clear, to make evidence. But there is a traditional hand

14
00:02:01,280 --> 00:02:09,040
among others, say, the whole body. I will come to that. How come to that? First, about

15
00:02:09,040 --> 00:02:18,120
experiencing, the word experiencing. So, making clear, or making playing, the whole breath

16
00:02:18,120 --> 00:02:27,160
body. Now, we count with the breath body. The body word is just kaya. Kaya can kaya

17
00:02:27,160 --> 00:02:38,800
means just body. Now, the commentary explained it is in breath body or breath body. Body

18
00:02:38,800 --> 00:02:46,040
here means not the whole body, but just the breath. The breath is here, calls body. Now,

19
00:02:46,040 --> 00:02:53,880
the word, the body word kaya means group. The body is a group of different parts. So,

20
00:02:53,880 --> 00:03:03,960
the breath is also the group of particles, small, small particles of matter. So, the breath

21
00:03:03,960 --> 00:03:11,520
is also called kaya. So, here, by the word kaya is meant the breath body, not the whole

22
00:03:11,520 --> 00:03:21,520
body. So, in the commentary explains this way. And the commentary, this commentary is

23
00:03:21,520 --> 00:03:28,800
based upon the patisambita maga, just mentioned. So, in the patisambita maga, also it is

24
00:03:28,800 --> 00:03:40,840
explained as meaning, breath body, in breath and out breath. But now, there are people who

25
00:03:40,840 --> 00:03:52,100
said it must mean the whole body. And say, they strip the body, they look through the

26
00:03:52,100 --> 00:04:00,520
body or all over the body for sensations. Now, observing the sensations in the body is

27
00:04:00,520 --> 00:04:09,360
not against Vipasa, not against the teachings of the Buddha. But, if you say that observing

28
00:04:09,360 --> 00:04:16,040
the sensations going through the body is according to this passage, then you are wrong.

29
00:04:16,040 --> 00:04:23,560
This passage has to do with breathing meditation. Breathing meditation means meditation,

30
00:04:23,560 --> 00:04:29,960
taking breathing as an object. If you look for sensation, if you observe sensation, you

31
00:04:29,960 --> 00:04:36,800
are no longer watching the breath. So, it is not no longer a breathing meditation, mindfulness

32
00:04:36,800 --> 00:04:44,200
of breathing meditation. It becomes other kind of contemplation on the body, or it may be

33
00:04:44,200 --> 00:04:51,760
even sensations. I mean, contemplation on feelings. So, in practice, you can do the

34
00:04:51,760 --> 00:05:03,160
sweeping of the body. If you want to, if you have gained a sudden degree of concentration,

35
00:05:03,160 --> 00:05:11,560
you can watch the sensations in the body. It is not against Vipasa. But, it is not

36
00:05:11,560 --> 00:05:20,080
according to this passage. Because, this passage is shown in with regard to breathing

37
00:05:20,080 --> 00:05:26,560
meditation. And this passage is more samata meditation. Yes, it is sweeping of this samata

38
00:05:26,560 --> 00:05:30,760
would be Vipasa. It could be Vipasa meditation.

39
00:05:30,760 --> 00:05:44,880
I still find the part to think of breathing meditation as samata rather than Vipasa. Because,

40
00:05:44,880 --> 00:05:52,840
the object is changing. No, in breathing, in samata meditation, breathing meditation as

41
00:05:52,840 --> 00:06:01,320
samata meditation, the object doesn't change. Because, it is just the nose. You keep the

42
00:06:01,320 --> 00:06:08,360
mind at the tip of the nose. But, the object of meditation is breath, in breath and out

43
00:06:08,360 --> 00:06:14,400
breath. So, you don't pay attention to other objects. And then, you try to give your mind

44
00:06:14,400 --> 00:06:22,920
on the object. That's why, next week, we will find that you have to make count in one, out one,

45
00:06:22,920 --> 00:06:28,720
in two, out two, and so on. So, you take the Vipasa object only. Only the breath is

46
00:06:28,720 --> 00:06:39,520
object. So, it is a mata meditation. So, to be aware of the body at all, it would have to

47
00:06:39,520 --> 00:06:53,280
be the least Vipasa. That's why. So, we will do the body at the beginning. The first

48
00:06:53,280 --> 00:07:02,560
four, the first foundation of mind, the second foundation of feeling. So, this has something

49
00:07:02,560 --> 00:07:12,000
to do. I mean with the question I made with this Vipasa. Can you talk about the body?

50
00:07:12,000 --> 00:07:19,840
Yeah, when you talk about the body, it means the breath body. Not the whole body. And also,

51
00:07:20,800 --> 00:07:28,480
here, seeing or being aware of the beginning, middle and end of the breath. That means giving

52
00:07:28,480 --> 00:07:36,320
your mind here, and when it goes in and goes out, first, this point. And you try to see this

53
00:07:36,320 --> 00:07:44,640
is beginning, middle, and beginning, middle, and like that. So, we cannot say that this

54
00:07:44,640 --> 00:07:58,880
Vipasa means the whole body. And this Vipasa is the kaya means the whole body. And then to

55
00:07:58,880 --> 00:08:05,840
one, only the beginning may be clear. And to another, the middle may be clear, or the end

56
00:08:05,840 --> 00:08:13,360
may be clear. But to another, all stages may be clear. So, we should be like that person. That is

57
00:08:13,360 --> 00:08:22,800
what is explaining paragraph 172. And 173, he says, he trains. He strives. He endeavors in this way.

58
00:08:23,280 --> 00:08:28,880
Or else, there was strength here. And one such as this is training in the higher body.

59
00:08:28,880 --> 00:08:34,720
His consciousness. Now, here, his concentration. Not consciousness. His concentration is training

60
00:08:34,720 --> 00:08:40,320
in higher consciousness. And his understanding is training in higher understanding.

61
00:08:40,320 --> 00:08:47,520
So, he trains and repeats, develops, repeatedly practice these three kinds of training on that object,

62
00:08:47,520 --> 00:08:51,600
by means of that mindfulness, by means of that attention. That is how the meaning should be

63
00:08:51,600 --> 00:09:00,000
regarded here. You know, the three trainings. Actually, three trainings are morality,

64
00:09:00,880 --> 00:09:07,200
concentration, and wisdom of pioneer. They are called three training. Here, they are called

65
00:09:07,200 --> 00:09:16,000
three higher training. They are the same as the other one, and just a little bit different. So,

66
00:09:16,000 --> 00:09:28,560
here, virtue, I mean, the particular strength is called higher virtue here, training in the higher

67
00:09:28,560 --> 00:09:38,640
virtue. And the genres are called training in higher consciousness. And enlightenment is called

68
00:09:38,640 --> 00:09:45,600
training in higher understanding or wisdom. So, training in higher consciousness means

69
00:09:45,600 --> 00:10:01,360
attainment of genres and other, we call stomach parties. So, in this method, Buddha said,

70
00:10:01,360 --> 00:10:09,840
he trains and so on. So, here, in the first part of the system, that is, numbers one and two,

71
00:10:10,400 --> 00:10:15,200
he should only breathe in and breathe out and not do anything else at all. And it is only

72
00:10:15,200 --> 00:10:19,920
afterwards that he should apply himself to the arousing of knowledge and so on. Consequently,

73
00:10:19,920 --> 00:10:26,320
the present tense is used here in the first text. He knows, I breathe in, he knows, I breathe out.

74
00:10:26,880 --> 00:10:32,880
So, just, he is to know that he is breathing in and breathing out, breathing in, breathing

75
00:10:32,880 --> 00:10:40,320
out, just that. But in this method, he must apply himself or he must train himself. He must make

76
00:10:40,320 --> 00:10:48,720
effort, special effort, to make clear the beginning, the middle and end of each in breath and

77
00:10:48,720 --> 00:10:55,520
out breath. That is why here, the future tense is used. I shall breathe in, I shall breathe out.

78
00:10:57,280 --> 00:11:08,400
So, that means there is some effort involved here, not just being mindful of in breath and out

79
00:11:08,400 --> 00:11:18,880
breath. In addition to being mindful of in breath and out breath, he has to know, to clearly see

80
00:11:18,880 --> 00:11:21,840
the beginning, the middle and end of the breath.

81
00:11:21,840 --> 00:11:37,760
Now, the next is for, I shall breathe in, I shall breathe out, tranquillising the bodily formations.

82
00:11:39,040 --> 00:11:46,640
Now, tranquillising the bodily formations does not mean that you must deliberately tranquillise

83
00:11:46,640 --> 00:11:55,040
the breath. You must deliberately make the breath subtle. You cannot do the actually.

84
00:11:57,680 --> 00:12:04,480
What this man is, just pay attention to the breath and as your mind comes down,

85
00:12:05,520 --> 00:12:11,840
then the breath will become more and more subtle. That is what is meant here.

86
00:12:11,840 --> 00:12:20,720
So, you cannot make them subtle by just breathing very softly or something at it.

87
00:12:22,160 --> 00:12:24,960
But because it will come with practice, you don't have to do it.

88
00:12:27,280 --> 00:12:33,760
Now, bodily formation, the body formation is used here and you know what that means?

89
00:12:36,000 --> 00:12:37,200
What is body formation?

90
00:12:37,200 --> 00:12:42,080
The breath. The breath. The breath is called here, bodily formation.

91
00:12:44,720 --> 00:12:54,640
That is why it is difficult to correctly understand in some places, because the same word is used

92
00:12:54,640 --> 00:13:01,840
in different meaning. Now, the body word is gaiya sankara. I think you are familiar with the word

93
00:13:01,840 --> 00:13:08,560
gaiya sankara. So, gaiya sankara is translated as bodily formation.

94
00:13:09,920 --> 00:13:18,160
But I don't know what formation means. The act of forming or something which is formed both?

95
00:13:20,800 --> 00:13:22,160
Something that is formed.

96
00:13:22,160 --> 00:13:23,280
Oh, something that is formed?

97
00:13:24,480 --> 00:13:28,640
But it could also mean formation of a circle.

98
00:13:28,640 --> 00:13:36,320
Yes, yes, yes. So, here it means something that is formed. So, bodily formation means bodily

99
00:13:36,320 --> 00:13:49,600
formed. That means the breath is said to be caused by mind. There are four causes of material

100
00:13:49,600 --> 00:14:01,600
properties, gamma, mind, climate and food. So, mind is one of the causes of material properties.

101
00:14:02,320 --> 00:14:06,720
Now, the breathing in and breathing out is said to be caused by mind.

102
00:14:07,920 --> 00:14:12,400
Although it is caused by mind, the breathing in and out is caused by mind.

103
00:14:12,400 --> 00:14:19,440
It needs the physical body to arise. If there is no body, there can be no breathing at all.

104
00:14:20,400 --> 00:14:27,280
So, it is described as formed by the body. That means formed with the help of the body.

105
00:14:28,800 --> 00:14:34,400
So, I think in the footnote, bodily formation, the imprint and out breath,

106
00:14:35,440 --> 00:14:40,720
for although it is consciousness oriented. So, although it is caused by consciousness,

107
00:14:40,720 --> 00:14:45,760
mind. It is nevertheless called body with formation since its existence is bound up with the

108
00:14:47,200 --> 00:14:53,920
not gamma boundary. It is bound up with the physical body, not gamma bond, not necessarily

109
00:14:53,920 --> 00:15:02,240
gamma bond. So, physical body and it is formed with that as the means.

110
00:15:02,240 --> 00:15:09,440
But, if there is no physical body, there can be no breath at all. So, it is said to be formed by

111
00:15:10,880 --> 00:15:16,720
made by or maybe conditioned by the physical body. So, it is called bodily formation.

112
00:15:18,960 --> 00:15:25,360
Is there a body there that is not, is there any kind of body that is not gamma born?

113
00:15:25,360 --> 00:15:37,440
There are material properties which are caused by gamma. For example, the sensitivity in the eye,

114
00:15:37,440 --> 00:15:45,440
sensitivity in the ear and so on. They are caused by gamma and voice. I mean,

115
00:15:45,440 --> 00:15:55,120
sound of voice is caused by consciousness of mind as well as by some other cause.

116
00:15:58,480 --> 00:16:08,720
When a rock hits against something, then there is noise. That is caused by not caused by mind.

117
00:16:08,720 --> 00:16:16,400
But, when I speak, then my voice is caused by mind. So, there are four causes of material

118
00:16:16,400 --> 00:16:23,440
properties and since breath is a material property, it must have a cause and according to

119
00:16:23,440 --> 00:16:28,240
a bit of my, it is caused by consciousness. So, it is called consciousness originated.

120
00:16:28,240 --> 00:16:34,000
Although, it is caused by consciousness, it is not called consciousness formation here,

121
00:16:34,000 --> 00:16:40,560
but it is called for bodily formation because it has to depend on the body, physical body for,

122
00:16:40,560 --> 00:16:46,320
it is arising. So, it is not necessarily become a born body, but it is physical body.

123
00:16:52,960 --> 00:17:02,080
And so, when, before we practice meditation or if we do not practice meditation,

124
00:17:02,080 --> 00:17:09,520
our breathing is said to be grows, but when we sit down and practice meditation and try to

125
00:17:09,520 --> 00:17:16,400
descend, descend the breathing, then it becomes more and more subtle.

126
00:17:22,560 --> 00:17:30,240
It will become so subtle that he has to investigate whether they exist or not. Sometimes yogis

127
00:17:30,240 --> 00:17:37,600
are alarmed afraid because they said they had stopped breathing at all and they wanted,

128
00:17:39,440 --> 00:17:43,680
whether they are living at all, whether they are alive at all.

129
00:17:45,760 --> 00:17:56,000
So, the breathing is different from other objects of meditation, like a synapse, a synapse,

130
00:17:56,000 --> 00:18:07,920
or dead bodies and so on. Other objects become clearer with the development of concentration.

131
00:18:08,480 --> 00:18:15,920
The better your concentration becomes, the clearer these objects or these images of these objects

132
00:18:15,920 --> 00:18:23,760
become, but it is opposite with the breath. The better you get concentration, the more subtle it

133
00:18:23,760 --> 00:18:33,760
becomes and the more difficult it is to see. So, you have to do apply effort and understanding

134
00:18:34,880 --> 00:18:42,800
so that you do not lose it. So, it will become so subtle that you wonder whether you are breathing

135
00:18:42,800 --> 00:18:55,760
at all or whether you have stopped breathing. So, it will reach to such a state. Now, why is that?

136
00:18:55,760 --> 00:19:02,000
Because previously at the time, when he has still not discerned, that means when he has not practiced,

137
00:19:02,000 --> 00:19:08,080
when he does not practice meditation, there is no concern in him, no reaction. No reaction really,

138
00:19:08,080 --> 00:19:16,080
if you see the words, paragraph 1 and 7 to 8, no reaction really means no adverting, no thinking

139
00:19:16,080 --> 00:19:22,240
of that. No attention, no reviewing to the effect that I am progressively externalizing,

140
00:19:22,240 --> 00:19:27,600
it grows our body with formation. But when he has discerned, there is. So, his body

141
00:19:27,600 --> 00:19:33,040
with formation at the time when he has discerned is subtle in comparison with that at the time

142
00:19:33,040 --> 00:19:42,640
when he has not. So, the breathing becomes subtle, more and more subtle with the growth of concentration.

143
00:19:42,640 --> 00:19:50,000
And you do not have to make them subtle, you cannot make them subtle at all. So, but you just

144
00:19:50,800 --> 00:19:58,800
keep your mind on the breath and try to discern it, it's eternally to see clearly the beginning,

145
00:19:58,800 --> 00:20:05,920
the middle and the end of it. And then, when you reach the next stage, they will become very

146
00:20:05,920 --> 00:20:12,560
subtle. So, you do not have to make them subtle, but they will become subtle. So, you have

147
00:20:12,560 --> 00:20:18,720
to train yourself, I shall breathe in from realizing the body with formation, but not really

148
00:20:18,720 --> 00:20:30,720
different from realizing them, but just they become from quill, they become subtle. And then,

149
00:20:30,720 --> 00:20:44,240
in the paragraph 179, they become the relative subtlety is given. In designing,

150
00:20:44,240 --> 00:20:50,800
the meditation subject, the formation is gross. And it is subtle by comparison in the first

151
00:20:50,800 --> 00:21:02,240
chana access. That means, when you first practice meditation, the breath is gross. And when you reach

152
00:21:02,240 --> 00:21:09,440
to the access concentration, it becomes subtle. And then, by comparison, it is gross at that

153
00:21:09,440 --> 00:21:18,080
stage of access concentration, but it is subtle at the stage of first chana. And then, in the

154
00:21:18,080 --> 00:21:24,800
first chana and the second chana access, it is gross. And in the second chana, it is subtle and so on.

155
00:21:24,800 --> 00:21:33,360
So, grossness and subtlety are compared with the chana, and then, a neighborhood of chana,

156
00:21:33,360 --> 00:21:46,960
and then, next chana. So, in the beginning, it is gross. And when you reach the neighborhood

157
00:21:46,960 --> 00:21:53,360
concentration, it is subtle. Then, at the neighborhood concentration, it is gross. At the first

158
00:21:53,360 --> 00:21:59,760
chana, it is subtle. Then, at the first chana, it is gross. It is subtle in the neighborhood of

159
00:21:59,760 --> 00:22:05,520
second chana. And then, it is subtle in the second chana and so on. So, subtleness is

160
00:22:07,200 --> 00:22:13,840
described with the development of meditation or concentration.

161
00:22:15,600 --> 00:22:23,120
And there are two opinions. The first one is the opinion of the diga and some yukta reciders.

162
00:22:23,120 --> 00:22:29,840
And the second is the magima reciders, but it is just a little bit different.

163
00:22:32,640 --> 00:22:38,960
Now, you know there are nikayas or collections. The collections of

164
00:22:40,400 --> 00:22:47,280
long suddas, collection of medium suddas, and collection of, that's a miscellaneous suddas,

165
00:22:47,280 --> 00:23:03,280
small, small suddas, and kindred suddas. Now, there are monks who made special study of one collection.

166
00:23:07,120 --> 00:23:15,440
And they have some opinion that may be different from those who specialize in another collection.

167
00:23:15,440 --> 00:23:22,160
And so on. So, there is difference between these teachers and so on, these reciders. So, diga

168
00:23:22,160 --> 00:23:27,120
and sinewter reciders think this way, and the magima reciders think the other way.

169
00:23:30,640 --> 00:23:36,880
But in the case of insight, that was in the case of samatama meditation. But in the inside

170
00:23:36,880 --> 00:23:41,600
meditation, the bodily formation occurring at the time of not descending is gross. And

171
00:23:41,600 --> 00:23:47,600
in descending the primary elements, it is by comparison sudda and so on. So, this this

172
00:23:47,600 --> 00:23:56,240
paragraph describes the development in vipassana meditation. It is gross in the preceding stage,

173
00:23:56,240 --> 00:23:58,480
and then sudda in succeeding stage and so on.

174
00:23:58,480 --> 00:24:14,400
And then a long collision from the pitysa vida maga.

175
00:24:14,400 --> 00:24:34,560
So, actually we have come to the end of the first full medams. You remember the first full medams.

176
00:24:34,560 --> 00:24:49,120
What is one? You know, you breathe in long when you breathe long, and you breathe

177
00:24:49,120 --> 00:25:06,720
out long when you breathe out long. So, long breaths, short breaths, and then what?

178
00:25:06,720 --> 00:25:12,880
Making clear the whole the whole breath body, and then transcorizing the breath body. So,

179
00:25:12,880 --> 00:25:19,520
be this full medams. But this partain to samatama meditation, and so they will be explained data.

180
00:25:22,960 --> 00:25:33,280
And the other groups of full have to do with jana and also with the

181
00:25:35,280 --> 00:25:39,840
other types of conditions of mindfulness. They will be explained later.

182
00:25:39,840 --> 00:25:51,040
But it's still samatama meditation. They can be samata,

183
00:25:53,600 --> 00:26:03,040
and they can be vipassana, depending on how you practice. You practice on the breath,

184
00:26:03,040 --> 00:26:12,320
and you get jana, and then you dwell on happiness or pity there, and if you observe pity,

185
00:26:12,320 --> 00:26:19,040
as in permanence, then you can feel pastana. It will describe later.

186
00:26:19,040 --> 00:26:40,960
Now, for the body deformations meaning breathing and breathing out, please see paragraph 181.

187
00:26:40,960 --> 00:26:54,480
There, the text from pity samata maga is cold. And in the text, I shall breathe in,

188
00:26:54,480 --> 00:26:59,360
shall breathe out, think about losing the body deformation. What are the body deformation?

189
00:26:59,920 --> 00:27:06,240
Long breaths, out breaths, experiencing the whole body, belong to the body.

190
00:27:06,240 --> 00:27:12,480
These things being bound up with the body are body deformations. This is the explanation

191
00:27:13,200 --> 00:27:20,000
of the word body deformations. They are called body deformations because they are bound up with the body.

192
00:27:20,000 --> 00:27:37,440
They belong to the body, without body they cannot arise. So, body deformations mean in breath and out breaths.

193
00:27:37,440 --> 00:27:48,640
Did you read the footnotes too?

194
00:27:48,640 --> 00:28:06,960
So, it is difficult to understand. So, I cannot go through that in detail, but on page 294,

195
00:28:06,960 --> 00:28:16,560
in the footnotes, above 1, 2, 3, 4, 5, 6, 7 lines, what is meant is this,

196
00:28:19,360 --> 00:28:26,160
the contemplation of the body as and so on and so on. It is very difficult to understand that

197
00:28:26,160 --> 00:28:37,360
translation. What is meant is this, the contemplation of the body as and in breath and out breath

198
00:28:37,360 --> 00:28:43,440
body as stated and of the physical body that is its material support, which is not contemplation

199
00:28:43,440 --> 00:28:58,400
of permanence, etc., in the body whose individual essence is impermanent, etc., and so on and so on.

200
00:28:59,360 --> 00:29:04,720
And maybe new translation here. So, if you are interested,

201
00:29:04,720 --> 00:29:11,760
you may ask a copy from Michael.

202
00:29:34,720 --> 00:29:47,200
I think it is a little better, it is way out better, you know.

203
00:29:51,360 --> 00:29:59,600
It is so pretty and nice and great out. Okay, then next week, from

204
00:29:59,600 --> 00:30:03,520
paragraph 186.

205
00:30:08,960 --> 00:30:14,400
So, once again, next week, from the last week, until we start, we send you again into the line.

206
00:30:14,400 --> 00:30:34,480
Next week, oh no, two weeks, and we have two more weeks, two weeks, so

207
00:30:34,480 --> 00:30:50,080
page 2992, 311, not many days, so next class is going to be the

208
00:30:50,080 --> 00:30:57,920
method of development of the first one. Yeah, and then the last class will

209
00:30:57,920 --> 00:31:08,000
be on work commentary, continue third three days at the threat that the phase 311,

210
00:31:08,000 --> 00:31:30,720
and paragraph 231, to the end of the chapter. So, two weeks more.

