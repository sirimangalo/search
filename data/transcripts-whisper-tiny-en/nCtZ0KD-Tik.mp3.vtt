WEBVTT

00:00.000 --> 00:06.080
Welcome, everyone, to find Wednesday morning in the Deer Park.

00:09.920 --> 00:15.760
Today's talk will be about becoming Buddhist.

00:25.600 --> 00:27.600
And it's an interesting topic.

00:27.600 --> 00:35.040
There are actually many Buddhist teachers out there who recommend not to become Buddhist.

00:36.240 --> 00:40.800
And maybe not many, but there are some teachers out there who

00:46.960 --> 00:53.360
who believe that the word Buddhism is in itself a problem

00:53.360 --> 00:59.440
and is taking away from the purity of the teaching.

01:02.000 --> 01:08.160
So I'd like to qualify and I'm not going to deny that there's a danger in labels.

01:10.320 --> 01:16.160
But I'm going to qualify what I mean and try to explain why it might actually be a benefit to us

01:16.160 --> 01:23.920
to take on the label or the role or the identification of a Buddhist.

01:26.720 --> 01:30.960
First of all, what is meant by a Buddhist? There are three ways of approaching the Buddhist teaching.

01:33.520 --> 01:40.160
Three sorts of paths you can take when you approach what we understand to be Buddhism.

01:40.160 --> 01:47.120
And the first is to become a fully enlightened Buddha.

01:49.920 --> 01:58.960
This path is obviously the most difficult and it's also the most noble and exceptional.

01:58.960 --> 02:14.080
To become a Buddha takes an incredibly long time and it depends on one's skill and imperfections

02:14.080 --> 02:16.400
as to whether one will even come close to attaining it.

02:19.280 --> 02:21.920
They say that to become a perfectly enlightened Buddha

02:21.920 --> 02:35.360
and you have to be willing to swim through an ocean of red-hot coals.

02:36.880 --> 02:41.520
If suppose there were an ocean the size of the Pacific Ocean

02:42.480 --> 02:47.920
and instead of water it were full of red-hot coals and you knew that you could swim through it without

02:47.920 --> 02:57.280
dying but you'd still feel all the pain and suffering. If you had what it took to become a Buddha

02:57.280 --> 03:03.760
you wouldn't think twice to cross that ocean in order to become a Buddha.

03:06.800 --> 03:13.040
It takes an incredible determination and you might even say it takes an incredible amount of luck

03:13.040 --> 03:19.680
because it's something that takes such a long time that the chances of you maintaining your dedication

03:20.800 --> 03:26.320
for such an uncountable length of time is quite difficult. This isn't to discourage people from

03:26.320 --> 03:34.080
that path. It's maybe to instill some measure of realism because it seems like there's a lot

03:34.080 --> 03:40.320
of people who take on the vow to become a fully enlightened Buddha without really understanding what

03:40.320 --> 03:49.040
it entails and without really taking on those practices and that sort of dedication which is going

03:49.040 --> 03:59.040
to give one even a slight chance of getting there. The second path is to become enlightened for one

03:59.040 --> 04:11.040
itself but not teach and so what makes a Buddha a fully enlightened Buddha special is that first of

04:11.040 --> 04:20.000
all he's able to realize here she is able to realize the truth for themselves without anyone

04:20.000 --> 04:26.640
having to explain it to them. This is the extent of their perfection. They don't need a teacher.

04:26.640 --> 04:34.960
They're so highly developed that they're able to look at reality and to see it for what it is

04:34.960 --> 04:42.400
without any instruction. But the second thing is that they're also then able to teach it to

04:42.400 --> 04:50.880
other people. Not only do they free themselves from all attachment, all clinging, all suffering,

04:50.880 --> 05:00.480
but they understand clearly how they did it and how anyone else can do it and they're able to

05:00.480 --> 05:07.520
see the path of any other person as to what it's going to take to get them to that stage

05:09.680 --> 05:16.000
and so they can teach others and they can teach others perfect with perfect accuracy

05:16.000 --> 05:22.560
such that they know exactly what it's going to take to that person to develop.

05:25.760 --> 05:30.480
Now the second type here is called the Pachaykabuddha private Buddha or one who becomes

05:30.480 --> 05:36.720
enlightened for themselves and this sort of person is special as well because they're still

05:36.720 --> 05:46.240
able to realize the truth by themselves. These are many of the great seers that have been in the past.

05:47.280 --> 05:56.880
A good example I think could be Lao Tzu who was the founder of Daoism but he wasn't the founder

05:56.880 --> 06:04.560
in the sense of actually going on teaching anyone. He just left behind a text or a set of teachings

06:04.560 --> 06:10.720
which in the end aren't really teachings, they're just principles and then he left the world

06:13.040 --> 06:20.160
and so these beings can arise at any time and they go into the forest and they're able to

06:20.160 --> 06:28.000
realize the truth but it's a different sort of realization. They let go, they see things clearly

06:28.000 --> 06:41.840
but they don't have this design in terms of understanding the method to bring people to this

06:41.840 --> 06:46.640
realization just like all of us when we practice, often we practice according to a teacher

06:47.520 --> 06:53.360
and we're even less able than the Pachaykabuddha to teach anyone else.

06:53.360 --> 06:58.880
We were able to practice by ourselves and we're able to follow the instruction of the teacher

06:58.880 --> 07:03.760
but we're not able to lead other people to the same realization because we don't have the

07:03.760 --> 07:11.840
knowledge of the teacher. So Pachaykabuddha becomes enlightened but then doesn't teach others.

07:13.520 --> 07:19.840
The third type what I would identify with as a Buddhist or it's the type of Buddhists that I'm

07:19.840 --> 07:26.960
going to be talking about here is a person who is not able in their present condition to realize

07:26.960 --> 07:36.320
the truth for themselves but they take on either the Buddha or one of his enlightened disciples

07:36.320 --> 07:44.480
or someone who is knowledgeable about the teachings of the Buddha as a teacher and as a result

07:44.480 --> 07:58.320
they're able to realize the truth having been instructed. This is the most common. The difference

07:59.280 --> 08:07.120
between the enlightened Buddha and the fully enlightened Buddha and one of his disciples

08:08.320 --> 08:13.680
it really boils down to this. A fully enlightened Buddha to become a fully enlightened Buddha,

08:13.680 --> 08:19.920
you not only have to give up everything, you also have to understand everything, know everything.

08:21.280 --> 08:25.120
You have to know everything, you have to know nothing that no stone left unturned,

08:25.120 --> 08:31.440
you have to have come to understand the entire universe. To be a disciple of the Buddha,

08:31.440 --> 08:37.840
you don't have to know everything but you have to let go of everything. You have to understand

08:37.840 --> 08:45.520
enough about the universe to realize that there's nothing worth clinging to. When you do that,

08:45.520 --> 08:52.160
you become free from suffering. Once it's the realization is the same on a practical level

08:53.440 --> 08:58.000
because you no longer subject to attachment and addiction and suffering.

09:00.080 --> 09:04.800
The fully enlightened Buddha has the advantage in terms of being able to teach others and

09:04.800 --> 09:15.280
to the breadth of the knowledge is incomparable. Today I'm just going to be talking about this

09:15.280 --> 09:23.680
third type, how to become a follower of the Buddha. The benefits, as I see it, of taking on this

09:23.680 --> 09:35.120
label, given that there are potential disadvantages of self identification and creating some sort of

09:35.120 --> 09:46.880
sense of self or so on, are that by determining in your mind by making a determination that

09:46.880 --> 09:54.400
you're going to follow the Buddha's teaching, that the Buddha and Buddhism is your path.

10:01.680 --> 10:13.200
It's something that protects you. You gain both internal and external protection.

10:13.200 --> 10:25.040
When you become protected from evil spirits, you become protected from misfortune.

10:27.840 --> 10:35.040
You become protected by Buddhists, by the Buddhist religion. You become protected internally

10:35.040 --> 10:45.120
and externally. Evil spirits, there are malevolent forces in the world. There are

10:46.480 --> 10:56.160
both humans and non-humans who, through mischief, through genuine malevolence,

10:56.160 --> 11:07.440
will try to harm us. Having taken the Buddha and his teachings and the Sangha as our refuge,

11:08.800 --> 11:17.440
having become a Buddhist or one who follows the Buddha's teaching, we have this strength of

11:17.440 --> 11:26.320
mind and we have this, it's like entering the mafia, you have protection.

11:29.600 --> 11:39.440
It's to make a crude comparison because there are also benevolent forces in the universe,

11:39.440 --> 11:49.280
both humans and non-humans. The most benevolent are the Buddhists, those who follow the Buddha's

11:49.280 --> 11:57.760
teaching because of the purity of the teaching, the fact that the Buddha focused and stressed

11:58.800 --> 12:04.560
the purification of the mind. There are many forces out there that protect the Buddha's teaching,

12:04.560 --> 12:11.200
seeing that as a beneficial force in the world, and not just talking about spirits, but also

12:11.200 --> 12:22.320
human beings who see the benefit of the Buddha's teaching. In fact, and many times non- Buddhists

12:22.320 --> 12:28.960
will protect Buddhists and I've seen this. There are both sides, there are human beings

12:28.960 --> 12:36.160
who obviously through their ignorance or through their bigotry, they're actually malevolent

12:36.160 --> 12:41.360
towards the Buddha's teaching, towards Buddhism, and they see monks, they feel hostility.

12:43.760 --> 12:51.600
I was arrested last year and put in jail by someone who thought or was either trying to have

12:51.600 --> 12:59.760
me, trying to put me in trouble or else genuinely thought I was something I wasn't thought I

12:59.760 --> 13:07.280
was a streaker or so on. You get this, but you also get other people who are not Buddhist,

13:07.280 --> 13:17.760
but see the genuine sincerity, the purity, the love and compassion that is expressed by Buddhists

13:17.760 --> 13:21.120
and wish to protect them.

13:28.880 --> 13:35.120
And this is one of the benefits I would say of identifying yourself as a Buddhist.

13:35.120 --> 13:40.880
It's one of those, it's maybe similar to identifying yourself as a Canadian. We always have this

13:40.880 --> 13:49.200
joke when we go traveling. As Canadians, we always put the Canadian flag on our backpack.

13:51.200 --> 13:58.080
And this is because Canadians were, I don't know anymore, but they were for a long time respected

13:58.880 --> 14:07.360
rightfully or not rightfully as nice people, good people. And so we'd generally be given

14:07.360 --> 14:18.240
an easier time if they were accosted by police or so on. So what happened then is Americans when

14:18.240 --> 14:24.720
they would go abroad, they would also put a Canadian flag on their backpacks. And if anyone

14:24.720 --> 14:32.000
asked they would tell them they were Canadian. It's maybe something like that. This is sort of

14:32.000 --> 14:39.600
one of the lower more base benefits of becoming a Buddhist, but it certainly is there.

14:40.400 --> 14:48.560
And protection is quite useful being protected from danger. Obviously when we're trying to develop

14:48.560 --> 15:00.480
ourselves, they say that when you take the Buddha as your refuge, evil spirits will not dare

15:00.480 --> 15:06.480
to come near you because they know the danger of attacking someone who is a follower of the Buddha.

15:08.880 --> 15:15.680
There is an inherent danger for spirits because it's very easy for them to

15:17.520 --> 15:23.360
switch from one state to another. A human being doesn't evil deed and it's very slow to bring

15:23.360 --> 15:31.120
results, but a spirit does evil deeds or good deeds, it's very quick to bring results.

15:34.800 --> 15:38.080
The next benefit is that it brings happiness in this life.

15:38.080 --> 15:52.560
When we become Buddhist, it brings a great state of peace and surety confidence to us.

15:54.640 --> 16:02.160
It eases our minds. We don't have to think about what is right and what is wrong. We accept

16:02.160 --> 16:07.840
the teachings of the Buddha. We've studied them and we're sure in our minds and we have this

16:07.840 --> 16:16.560
great confidence. We know where we stand. So it's like when the Christians, they always tell you

16:16.560 --> 16:23.280
and when you have a problem, you just ask yourself what would Jesus do? This is the Christian

16:23.280 --> 16:29.040
answer. For Buddhist, we say, well, what would the Buddha do? What would a Buddhist do? What would

16:29.040 --> 16:37.840
a follower of the Buddha do? This is generally useful as well. As I said, it brings us peace and

16:37.840 --> 16:45.840
happiness. It's something that calms and tranquilizes our mind. When we practice meditation,

16:47.920 --> 16:53.120
we feel somehow like we are a part of the group. We're walking with the Buddha.

16:53.120 --> 17:05.520
We're following after the Buddha. And then the next benefit related to that is it brings happiness

17:05.520 --> 17:13.520
in our next life. Because of the surety of mind, the confidence that we have and the peace

17:13.520 --> 17:24.000
and tranquility that exists in our minds from accepting the Buddha as our teacher. When we pass away,

17:27.200 --> 17:30.320
we can be sure to be born in a good place, in a good way.

17:36.480 --> 17:41.120
But the real reason I would say, and probably the best reason, the only sort of reason that is

17:41.120 --> 17:50.960
of immediate benefit or immediate purpose for us is that considering that the Buddha's teaching

17:50.960 --> 18:02.480
is so pure and that there's no part of it that is detrimental to us, that taking on the Buddha

18:02.480 --> 18:12.080
as our refuge or taking on Buddhism as our path leads to purity of mind, leads to purity of

18:12.080 --> 18:19.680
mind and the dedication to the practice. And this is obviously the most important because our

18:20.480 --> 18:27.520
intention, our path as Buddhists is to practice, is to develop our minds. And so taking the

18:27.520 --> 18:36.320
Buddha as our refuge and reminding ourselves of the qualities of the Buddha and determining

18:36.320 --> 18:41.440
that this is our path gives us the strength of mind to continue in the practice.

18:42.400 --> 18:49.600
When the going gets tough, we remember the example that the Buddha said and we reaffirm in our

18:49.600 --> 18:55.760
minds that we are followers of the Buddha. It gives us confidence. It gives us reassurance that

18:55.760 --> 19:05.840
we're a good person, that we're someone who has done a good thing in terms of

19:07.360 --> 19:12.320
you know, put placing our trust and our confidence in the Buddha. And by following this path,

19:12.320 --> 19:14.800
which leads us to freedom from suffering.

19:18.800 --> 19:22.400
So I would say it's generally a good thing. It's obviously not the best thing and it's not a

19:22.400 --> 19:28.000
replacement for meditation. Let's say there's a lot of people out there who are Buddhist

19:29.120 --> 19:39.120
quote unquote and don't practice and tend to get this overconfidence that you know it's

19:39.120 --> 19:46.400
you know I'm Buddhist and I've been Buddhist from birth and that's somehow that means something,

19:46.400 --> 19:54.480
somehow that is going to protect one. And I would say you know having said that it is a protection,

19:54.480 --> 20:03.760
it is a support. It's a fairly weak protection in the face of the defilements that exist in our minds.

20:03.760 --> 20:09.600
If that's our only protection is this identification as a Buddhist, then we're not likely to be

20:09.600 --> 20:18.080
protected in any way from the evils in our mind. It's simply a support. It's a basic practice

20:18.080 --> 20:23.520
that encourages us, that tranquilizes our minds. When I was practicing meditation, when I first

20:23.520 --> 20:28.960
went to practice meditation, I didn't wasn't even interested in Buddhism. I had no intention to

20:28.960 --> 20:34.160
become a Buddhist. But they made us do this ceremony where we you know we take the Buddha as

20:34.160 --> 20:40.240
our refuge and so I followed after and I was reading the words reading the script again while I

20:40.240 --> 20:46.240
was meditating. They wouldn't let us read. So all we had is this little booklet and I just kept

20:46.240 --> 20:54.880
reading it over and over again in the chanting. And so when the practice got really difficult

20:54.880 --> 21:08.160
and suddenly I was faced with this overwhelming pain and suffering from just the realization

21:08.160 --> 21:17.840
of the insanity that existed in my mind and what I had done to myself and how I was so on the

21:17.840 --> 21:29.600
wrong path with drugs and alcohol and women and music and so much that was really destroying my

21:29.600 --> 21:36.480
peace of mind and had been doing so for many years. I remember waking up at three in the morning

21:36.480 --> 21:41.680
and just saying okay I get ready to start and as soon as I sat down to start meditating everything

21:41.680 --> 21:47.520
came back again. Everything from the last day was right there again waiting for me. It hadn't gone

21:47.520 --> 21:53.760
anywhere it hadn't disappeared with my sleep and just freaking out and realizing you know I

21:53.760 --> 22:05.440
I can't do this anymore and I had no way nowhere to go I had no no refuge no nothing to hold on to

22:05.440 --> 22:15.040
nothing to pull myself up with. And so I went walking out of my out of my hut looking you know I

22:15.040 --> 22:20.000
not looking actually just wandering aimlessly and then I saw off in this bamboo salad they had

22:20.000 --> 22:26.880
this Buddha image and the light was on somebody left the light on on night and as soon as I saw

22:26.880 --> 22:33.840
it I just just drawn to it and I walked over and and and I felt down in front of the Buddha

22:33.840 --> 22:39.280
and I prostrated down and I started chanting according to this booklet and taking refuge in the

22:39.280 --> 22:53.280
Buddha. I suppose that sounds a lot like a Christian tale of what do you call newborn again being

22:53.280 --> 22:59.360
born again and you know I don't think it's that dissimilar and it's certainly not a state of

22:59.360 --> 23:05.280
enlightenment but it was something that really helped me in my practice it helped me to continue

23:05.280 --> 23:10.400
meditation so if you want to meditate if that was my intention is to meditate then I would say that

23:10.400 --> 23:17.840
was a real benefit for me because it gave me this you know basic reassurance that I wasn't alone

23:17.840 --> 23:25.280
I had someone you know to hold on to well I was like a child just learning to walk

23:25.280 --> 23:35.200
and it certainly it's certainly not the the final solution but for for newcomers it can be a very

23:35.200 --> 23:42.160
great thing to do a great benefit and and helps to calm and reassure your mind in the beginning.

23:45.040 --> 23:49.600
Okay so the next thing is what does it mean to be a Buddhist and how do you become a Buddhist

23:49.600 --> 23:59.200
and it's quite simple. As a Buddhist we're not really interested in identifying ourselves as

23:59.200 --> 24:12.800
anything but there are certain qualifications that one has to obtain one has to take the three

24:12.800 --> 24:19.360
refuges to be considered a Buddhist you have to take the Buddha as your refuge the idea is as a

24:19.360 --> 24:26.480
Buddhist we say that the Buddha is our leader though he's passed away long long ago we still

24:26.480 --> 24:33.040
you know understand that this this person who taught all of these wonderful things is our leader

24:33.040 --> 24:42.480
and we we take him and and singularly him as our leader. The second is is the dhamma his teachings

24:42.480 --> 24:47.840
we take his teachings as our teaching as the teachings we are going to follow we take we say that

24:47.840 --> 24:55.680
these are the teachings that we're going to follow this is our path and third we we take the

24:55.680 --> 25:02.640
teachers or them the enlightened disciples of the Buddha as as our refuge the people who have passed

25:02.640 --> 25:21.280
on the Buddha's teaching up until this time this is this is what I meant by you know having the

25:21.280 --> 25:27.520
protection having a support because these things are something that we can always reflect on

25:27.520 --> 25:33.600
something that we can always remember something that we can use to support ourselves when they're

25:33.600 --> 25:40.480
going to get stuff when when we're in difficulty it's not a central practice again I don't want

25:40.480 --> 25:45.840
to be able to get this idea that we're like a faith-based religion where we we focus on worshiping

25:45.840 --> 25:51.760
these things or something but the support is is undeniable the support that these things give to our

25:51.760 --> 26:08.080
mind and so my my teacher he said when you're when you're in danger when you when you're

26:09.360 --> 26:16.000
traveling anywhere when you're going on a trip or something and you're worried and you're not sure

26:16.000 --> 26:26.080
you know whether there'll be danger or misfortune or so on you can just remind yourself of the

26:26.080 --> 26:29.680
Buddha and say that the Buddha is your refuge the dumb is your refuge the song is your refuge

26:29.680 --> 26:36.960
and it it strengthens your mind and it can actually change the course of events because it creates

26:36.960 --> 26:49.280
some sort of some sort of strength and power in your mind and perhaps even is a means of seeking

26:49.280 --> 26:56.800
protection from from the angels and the guardian spirits who who may also be Buddhist or

26:56.800 --> 27:03.520
appreciate the Buddhist teaching so we have this mantra actually that will say to ourselves that

27:03.520 --> 27:08.560
just as the Buddha is my refuge the dumb is my refuge the song is my refuge Buddha may not

27:08.560 --> 27:19.200
or Tamil may not those uncle may not all and we repeat that to ourselves the second thing that

27:19.200 --> 27:30.480
that's required as a Buddhist is that we keep the five precepts and this is where people tend to

27:30.480 --> 27:36.640
have a lot of trouble I would say most people are at least superficially able to

27:38.000 --> 27:44.400
accept the Buddha the dumb and the sangha as good things and as their refuge even though they

27:44.400 --> 27:49.440
might not quite know what that means most for many Buddhists don't really know what the dummy is

27:49.440 --> 27:56.800
don't don't have a strong understanding of of what is the teaching of the Buddha and and also

27:56.800 --> 28:02.880
don't have a way of distinguishing between those those teachers or those Buddhists who could be

28:02.880 --> 28:08.000
considered sangha who actually practice according to the Buddha's teaching so there are a lot of

28:08.000 --> 28:16.080
Buddhists out there who follow after teachers who maybe are fortune telling or you know offering

28:16.080 --> 28:24.080
ambulance or you know teaching public school or whatever many things which are not in line with

28:24.080 --> 28:29.520
the Buddha's teaching and would make one thing that this is not really a part of the sangha

28:29.520 --> 28:35.600
these people are not really followers or passing on the Buddha's teaching

28:40.480 --> 28:44.320
but so people are able at least superficially to accept this it's very difficult for people to

28:44.320 --> 28:55.040
accept the five precepts and this makes it a little bit difficult to become a Buddhist and obviously

28:55.040 --> 29:03.040
it that makes sense because Buddhism is not an easy path it's not an easy thing to become enlightened

29:03.040 --> 29:08.960
to become free from suffering we're not talking about something you know there's no okay become a

29:08.960 --> 29:16.800
Buddhist sign up in your your your your set for life this isn't one of those religions it's a

29:16.800 --> 29:22.720
very difficult thing and and this is perhaps why why seeking out a support might be maybe beneficial

29:22.720 --> 29:30.880
because we have no illusions about the difficulty that of the task at hand it's not something where

29:30.880 --> 29:40.720
everyone can be said to succeed you might fail you might end this life not having gained any

29:41.680 --> 29:49.600
lasting benefit or or not having gained not having become enlightened to any degree

29:51.040 --> 29:54.320
it's possible that you could practice for some time and follow away from it

29:54.320 --> 30:04.960
and so you know really we we need all the help we can get the benefit of becoming Buddhist

30:10.080 --> 30:17.600
but we also have to take on very some some fairly difficult precepts people always ask about the

30:17.600 --> 30:22.560
five precepts you know they're really necessary especially you know they're not drinking alcohol

30:22.560 --> 30:27.600
and taking drugs and really I think this is a terrible terrible question to ask and I think it's

30:27.600 --> 30:32.400
ridiculous that people always say oh well if you want to have one glass of wine it's not really that

30:32.400 --> 30:38.800
but it's really I mean come on people if you if you can't give up such a simple thing like

30:38.800 --> 30:47.360
like in toxicants drinking poison drinking rotten grapes you know you know it is it really

30:47.360 --> 30:54.880
you know is it really fair for us to to to to to wine and complain about such a small thing

30:54.880 --> 31:03.360
when what we're looking at here is such a profound and incredibly difficult path that if you

31:03.360 --> 31:18.000
can't even give up these basic acts these these basic immoral immoral undertaking it's not it

31:18.000 --> 31:22.400
doesn't speak well for your ability to do away with the very subtle to found

31:27.280 --> 31:31.520
and I think this this is easy to see for people who really practice meditation that if you're

31:31.520 --> 31:36.000
serious about meditation it's it's not really that difficult for you to give these things up

31:36.000 --> 31:42.480
this is what is required this is what the Buddha recommended not not even really recommended

31:42.480 --> 31:47.360
but he was he was fairly categorical in terms of you know if you don't keep these you're not

31:47.360 --> 31:53.840
really going to progress in your practice these are the these are the the basic

31:53.840 --> 32:02.240
moral principles of a Buddhist meditator or Buddhist practitioner if you can't even keep these

32:02.240 --> 32:10.560
it's it's very difficult to see it anyway for you to to progress and so it's it's often a scene

32:10.560 --> 32:25.760
as a making a concession or you know giving something up in in favor for our practice

32:26.800 --> 32:31.520
we we want to go out drinking we want to drink wine we want to be a social drinker or so on

32:32.160 --> 32:37.520
maybe we want to kill we want to steal we want to cheat we want to lie and in some ways we can

32:37.520 --> 32:46.880
we can verify we can justify very small bad deeds but this is a sacrifice that we make

32:48.720 --> 32:54.480
and and in the end it isn't a sacrifice at all we see that we were wrong to once we practice we

32:54.480 --> 32:58.720
did it we were wrong to think that there was any good that could come from any of these things

33:00.080 --> 33:05.120
even just in terms of drinking people who who would drink a little bit and say oh I'm just doing

33:05.120 --> 33:11.200
it to be social and to fit in and so on actually what you're saying there is that you're doing

33:11.200 --> 33:16.080
it so that you don't have to challenge other people's beliefs you know suppose your friends are

33:16.080 --> 33:20.320
alcoholics or they really do get drunk while you just drink just so that they don't feel bad

33:21.040 --> 33:27.840
basically what you're saying you drink so that they they're able to to retain their sense that

33:27.840 --> 33:34.240
drinking is okay which of course it isn't and as a Buddhist it certainly isn't as a Buddhist

33:34.240 --> 33:40.800
we have you know we make this determination that this is the opposite of a clear and sober

33:40.800 --> 33:50.720
and and pure state of mind and so you're doing an incredible disservice to the Buddhist

33:50.720 --> 33:56.720
teach to Buddhism by encouraging these people and you're wasting a very precious

33:56.720 --> 34:08.160
chance to to change the world to say to these people I'm sorry I don't drink I think I believe

34:08.160 --> 34:20.400
that drinking is a cost for for a muddled state of mind this is an enemy to clear it to

34:20.400 --> 34:28.480
clarity of mind my practice is to purify my mind and so I don't do they say no if that makes them

34:28.480 --> 34:33.520
angry then then then here you go this is something they have to look at in themselves and something

34:33.520 --> 34:37.200
you have to look at why are you hanging out with people who get angry when you talk about clarity

34:37.200 --> 34:42.880
of mind and purity of mind when you talk about meditation and all your friends start to you know

34:42.880 --> 34:50.480
get upset and and and get bored and disinterested looks on their faces why are you with these

34:50.480 --> 34:59.680
people if you can somehow explain to them to the extent that they're able to accept and eventually

34:59.680 --> 35:07.520
come to practice good but otherwise it's not much of an excuse to say well my friends like to drink

35:07.520 --> 35:13.040
and therefore I should drink with them you know it probably means that you're hanging out with the wrong people

35:17.040 --> 35:25.600
so the five precepts we don't kill we don't steal we don't cheat meaning to commit adultery

35:25.600 --> 35:35.280
or or break up other relationships for in romantic affairs we don't lie and we don't take drugs

35:35.280 --> 35:43.520
and alcohol and those things that intoxicate the mind and this is basically what it means to

35:43.520 --> 35:48.960
become a Buddhist when you when you take these two things the three refuges you accept that the Buddha

35:49.680 --> 35:56.640
his teachings and the Sangha the teachers who have passed on his teachings are your refuge

35:56.640 --> 36:08.880
are your your your point of reference and when you take the five precepts as rules for your life

36:10.480 --> 36:13.920
this is really what it means to become a Buddhist

36:13.920 --> 36:31.360
and just as a side note I made a video on YouTube recently that is a ceremony for taking

36:31.360 --> 36:44.640
the three refuge and the five precepts so if in case anyone is interested in becoming a Buddhist

36:44.640 --> 37:05.040
you're welcome to look it up and here's the link

37:14.640 --> 37:29.920
so I thought that was something useful to talk about this ceremony on YouTube is is a way of

37:29.920 --> 37:38.400
reaffirming these principles that I've talked about it's a ceremony it's a ritual and what it does

37:38.400 --> 37:44.240
is it reaffirms in your mind it's just like our meditation practice when we watch the rising we reaffirm

37:44.240 --> 37:49.440
for ourselves this is the rising this is the falling of the abdomen we feel pain we reaffirm it

37:49.440 --> 37:54.960
for what it is and it has a power when we do this ceremony it has a power in our minds it reaffirms

37:54.960 --> 38:01.840
in our minds it strengthens our our vow our intention and it has a purpose it has an effect

38:01.840 --> 38:11.360
I would say the effect is beneficial because it strengthens our ability to to carry out our practice

38:12.720 --> 38:17.120
so that's what I wanted to talk about today I think that should be something generally useful for

38:17.120 --> 38:22.560
people and there's a support for our meditation practice I haven't really gotten into meditation

38:22.560 --> 38:32.400
in these past couple of weeks and I promised that I would but maybe I'll say that I'd like to

38:32.400 --> 38:40.880
teach a session record a session on on basic meditation practice just a 10-15 minute session

38:40.880 --> 38:46.240
where all we it's a guided meditation that I can then use for people to use at home when they

38:46.240 --> 38:52.400
want to learn to meditate in a very short time and get a basic understanding of meditation it's

38:52.400 --> 39:18.640
maybe I'll do that anyway thank you all for coming if you have any questions I'm happy to answer them

