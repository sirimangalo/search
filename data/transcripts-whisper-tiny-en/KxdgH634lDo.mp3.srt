1
00:00:00,000 --> 00:00:10,000
In terms of generating karma, what difference is there between making people happy and relieving people suffering?

2
00:00:10,000 --> 00:00:38,000
Yeah, well, on a mundane level or a conceptual level, there is a difference, bringing people happy feelings, for example,

3
00:00:38,000 --> 00:00:46,000
is quite different from freeing them from suffering, but on an ultimate level, there is no difference.

4
00:00:46,000 --> 00:00:50,000
There is no difference between happiness and freedom from suffering.

5
00:00:50,000 --> 00:00:58,000
Happiness, peace, peace, happiness and freedom from suffering are all the same thing.

6
00:00:58,000 --> 00:01:14,000
It is important to understand. It is important to note that happy feelings are not the goal.

7
00:01:14,000 --> 00:01:23,000
The peaceful feelings are not the goal, because they are impermanent, and they are part of the flux.

8
00:01:23,000 --> 00:01:29,000
They come and they go, and they might very well be replaced by something else.

9
00:01:29,000 --> 00:01:33,000
If the goal were those things, then the goal would be something that is impermanent.

10
00:01:33,000 --> 00:01:39,000
And moreover, it would be a goal that is subject to attachment.

11
00:01:39,000 --> 00:01:52,000
So it would be a goal that can still be clung to, and it would be a goal that wasn't capable of removing your clinging.

12
00:01:52,000 --> 00:02:00,000
It would still lead to suffering when the feeling is disappeared.

13
00:02:00,000 --> 00:02:06,000
So it is actually, if you are talking about that kind of happiness, and it is actually the opposite.

14
00:02:06,000 --> 00:02:13,000
It is to give up the attainment of happy feelings or sensations.

15
00:02:13,000 --> 00:02:23,000
That that truly does for you from suffering, and as long as there is seeking out these states, some kind of preference for these states.

16
00:02:23,000 --> 00:02:37,000
Thinking of them as the goal, then there will always be suffering, following it, because they don't last, and because of the nature of clinging that it necessitates disappointment.

17
00:02:37,000 --> 00:02:45,000
It takes you out of balance, so it creates stress.

