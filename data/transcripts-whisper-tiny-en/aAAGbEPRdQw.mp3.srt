1
00:00:00,000 --> 00:00:02,840
Hey, and welcome back to us a month.

2
00:00:02,840 --> 00:00:07,840
The day's question comes from Tulu Flux, who asks,

3
00:00:07,840 --> 00:00:12,240
Hi, I find it easy to be compassionate to beings that are weaker,

4
00:00:12,240 --> 00:00:14,340
less fortunate than me.

5
00:00:14,340 --> 00:00:19,200
Compassion is less forthcoming from me for those better often me.

6
00:00:19,200 --> 00:00:22,080
I understand this is wrong practice on my part.

7
00:00:22,080 --> 00:00:27,200
How do I separate compassion from PD?

8
00:00:27,200 --> 00:00:35,840
Well, first of all, as I said before in one of my videos,

9
00:00:35,840 --> 00:00:42,240
there are really two ways of approaching the whole love, compassion,

10
00:00:42,240 --> 00:00:46,320
sympathy, and equanimity thing.

11
00:00:46,320 --> 00:00:49,520
These are meditations in their own right,

12
00:00:49,520 --> 00:00:54,240
and you can practice them to develop these things if you feel yourself lacking in them.

13
00:00:54,240 --> 00:01:00,800
But they are not considered, not in the tradition that I follow,

14
00:01:00,800 --> 00:01:07,200
to be crucial or essential parts of the practice.

15
00:01:07,200 --> 00:01:12,000
And the reason being that we look at it the other way.

16
00:01:12,000 --> 00:01:15,040
The development of wisdom is the most important.

17
00:01:15,040 --> 00:01:22,560
And a person who is wise necessarily has love, compassion,

18
00:01:22,560 --> 00:01:25,920
sympathy, and equanimity, these states

19
00:01:25,920 --> 00:01:29,760
are the natural state of being.

20
00:01:29,760 --> 00:01:33,920
Not in the necessarily in the active sense,

21
00:01:33,920 --> 00:01:38,000
though they arise spontaneously in someone who is wise,

22
00:01:38,000 --> 00:01:46,800
they existed all times in the sense of not being angry or upset

23
00:01:46,800 --> 00:01:52,000
or cruel or jealous or disturbed.

24
00:01:52,000 --> 00:01:56,800
So they exist in their neutral state of being always friendly,

25
00:01:56,800 --> 00:02:00,960
of being always helpful, and so on.

26
00:02:00,960 --> 00:02:06,640
So they spring naturally and in their active state,

27
00:02:06,640 --> 00:02:09,840
as well, of a person who is enlightened, it comes easily.

28
00:02:09,840 --> 00:02:15,040
It's very easy to be actively loving and kind of compassionate.

29
00:02:15,040 --> 00:02:19,840
So as a part of the path or the development of oneself,

30
00:02:19,840 --> 00:02:25,280
they should be used in tandem with insight meditation.

31
00:02:25,280 --> 00:02:28,480
I wouldn't ever recommend to just develop compassion,

32
00:02:28,480 --> 00:02:30,640
or just develop loving kindness.

33
00:02:30,640 --> 00:02:33,680
If you're not developing insight, then you're still

34
00:02:33,680 --> 00:02:38,400
developing something based on delusion, and it's kind too.

35
00:02:38,400 --> 00:02:39,920
It's just going to lead your own encircles

36
00:02:39,920 --> 00:02:44,400
and best it can lead you to a very focused and calm state

37
00:02:44,400 --> 00:02:50,640
of being, you can lead you to be a kind and compassionate being

38
00:02:50,640 --> 00:02:55,040
on a conventional level, but for a temporary period of time.

39
00:02:55,040 --> 00:02:58,480
It's not based on wisdom, whereas with wisdom,

40
00:02:58,480 --> 00:03:01,280
you never, these things never go away.

41
00:03:01,280 --> 00:03:05,200
So I would, just to get that clear, I've said that before,

42
00:03:05,200 --> 00:03:09,760
but you don't have to worry so much about your inability

43
00:03:09,760 --> 00:03:13,680
to develop compassion, because it comes naturally through the development

44
00:03:13,680 --> 00:03:16,400
of wisdom, that's much more important.

45
00:03:16,400 --> 00:03:20,560
But it can be a useful addition, a practice that we use,

46
00:03:20,560 --> 00:03:24,000
and it's important to understand compassion

47
00:03:24,000 --> 00:03:28,560
since it is a quality of mind of wise people.

48
00:03:28,560 --> 00:03:34,160
So your observation that it's easier to be compassionate

49
00:03:34,160 --> 00:03:38,000
towards beings who are weaker or less fortunate than you

50
00:03:38,000 --> 00:03:42,240
is correct, and that's according to the texts.

51
00:03:42,240 --> 00:03:46,400
One of the texts that I always draw my answers from,

52
00:03:46,400 --> 00:03:49,840
or quite often, draw my answers from is this book.

53
00:03:49,840 --> 00:03:56,080
It's called the Path of Purification, and there's the author.

54
00:03:56,080 --> 00:03:58,640
This edition, it's an ancient text.

55
00:03:58,640 --> 00:04:04,880
It was written, they say, about 1,500 years ago,

56
00:04:04,880 --> 00:04:08,080
and it's based on even earlier sources.

57
00:04:08,080 --> 00:04:13,200
This edition is printed in Taiwan by these guys,

58
00:04:13,200 --> 00:04:17,040
and it's distributed for free.

59
00:04:17,040 --> 00:04:19,600
Now, as a monk, they send me a whole bunch of them,

60
00:04:19,600 --> 00:04:22,560
but I don't know whether they'd send these to ordinary people,

61
00:04:22,560 --> 00:04:24,480
or how that works.

62
00:04:24,480 --> 00:04:28,000
But it's the corporate body of the Buddha educational foundation.

63
00:04:28,000 --> 00:04:33,520
I recommend this book for a lot of the questions that I'm being asked.

64
00:04:33,520 --> 00:04:36,320
If you have these sorts of questions that are

65
00:04:36,320 --> 00:04:41,920
taken to do with early Buddhism or Orthodox basics of Buddhism,

66
00:04:41,920 --> 00:04:43,600
then this is an excellent book.

67
00:04:43,600 --> 00:04:49,440
So in this book, it talks about the four states,

68
00:04:49,440 --> 00:04:58,640
these four states of compassion, and let's see if I can find it here.

69
00:04:58,640 --> 00:05:17,120
They have their limits, and they have their...

70
00:05:17,120 --> 00:05:27,280
Yeah, here we are.

71
00:05:27,280 --> 00:05:33,040
Okay, so compassion, for someone who wants to develop compassion,

72
00:05:33,040 --> 00:05:38,080
the first thing they should do is focus on someone who is suffering.

73
00:05:38,080 --> 00:05:40,880
This is correct, this is because it's easy.

74
00:05:40,880 --> 00:05:46,000
Now, it doesn't mean that compassion is this feeling of pity towards a person,

75
00:05:46,000 --> 00:05:50,080
but this seeing someone suffering is the easiest way

76
00:05:50,080 --> 00:05:52,480
to evoke these feelings.

77
00:05:52,480 --> 00:05:55,920
It's because it's natural in us, because it's something that is very easy.

78
00:05:55,920 --> 00:05:59,680
If you see someone, and here's the example,

79
00:05:59,680 --> 00:06:03,840
someone who is unsightly reduced to utter misery,

80
00:06:03,840 --> 00:06:07,840
with hands and feet cut off, sitting in the shelter for the helpless,

81
00:06:07,840 --> 00:06:10,800
with a pot placed before him, with a mass of maggots,

82
00:06:10,800 --> 00:06:13,600
oozing from his arms and legs and moaning.

83
00:06:13,600 --> 00:06:16,400
Compassion should be felt for him in this way.

84
00:06:16,400 --> 00:06:18,960
This being has indeed been reduced to misery,

85
00:06:18,960 --> 00:06:21,520
if only he could be free from the suffering,

86
00:06:21,520 --> 00:06:24,320
because here it's really easy to see the person suffering,

87
00:06:24,320 --> 00:06:30,080
and it's really easy to give to understand and to feel pity for the person.

88
00:06:30,080 --> 00:06:35,200
Now, this is only one part of what the meaning of the word compassion,

89
00:06:35,200 --> 00:06:39,520
and the text goes on to talk about how you can evoke the same feelings towards

90
00:06:39,520 --> 00:06:47,280
someone who's not suffering, very easily to evoke it towards someone who is an evil person.

91
00:06:47,280 --> 00:06:52,400
That's generally someone who is unpleasant,

92
00:06:52,400 --> 00:06:55,280
someone who you don't like, someone who is under ear to you,

93
00:06:55,280 --> 00:06:57,680
someone who you have a problem with,

94
00:06:57,680 --> 00:07:01,280
and you think they're doing all sorts of terrible bad things.

95
00:07:01,280 --> 00:07:05,280
For that sort of person, it's also quite easy.

96
00:07:05,280 --> 00:07:08,000
It's relatively easy to develop compassion for.

97
00:07:08,000 --> 00:07:10,480
You can start with someone you love, but eventually

98
00:07:10,480 --> 00:07:13,040
you develop it towards someone you don't like,

99
00:07:13,040 --> 00:07:16,880
because you know that in the future they're going to fall into suffering.

100
00:07:16,880 --> 00:07:20,480
But this is not the essential quality of compassion.

101
00:07:20,480 --> 00:07:24,320
Compassion is arises.

102
00:07:25,920 --> 00:07:32,560
It's characteristic, is promoting the aspect of a laying suffering.

103
00:07:32,560 --> 00:07:36,000
It function, its function, resides in not varying other suffering,

104
00:07:36,000 --> 00:07:38,880
and it's manifested as non-cruelty.

105
00:07:38,880 --> 00:07:44,880
So the manifestation, the way compassion appears,

106
00:07:44,880 --> 00:07:48,560
or the way it expresses itself, is by not being cruel.

107
00:07:48,560 --> 00:07:52,240
And this comes about through understanding suffering.

108
00:07:52,240 --> 00:07:56,720
When you see someone who's in terrible amount of suffering is a yeah, I get it,

109
00:07:56,720 --> 00:08:01,040
and I've suffered, and I understand these course types of suffering.

110
00:08:01,040 --> 00:08:03,200
But when you practice insight meditation,

111
00:08:03,200 --> 00:08:11,520
and this is why it's so important to have it develop it in tandem with insane meditation,

112
00:08:13,520 --> 00:08:17,600
you are able to understand much more clearly what gives rise to suffering.

113
00:08:17,600 --> 00:08:19,920
So when you see someone who's doing evil deeds,

114
00:08:19,920 --> 00:08:23,040
you feel paid for them, because you know that they're going through.

115
00:08:24,320 --> 00:08:27,120
And moreover, because you've experienced suffering,

116
00:08:27,120 --> 00:08:28,480
you don't wish it for anyone.

117
00:08:28,480 --> 00:08:31,680
So your actions reflect this, your speech reflects this.

118
00:08:31,680 --> 00:08:35,280
You wouldn't want to do something that gives rise to suffering in others.

119
00:08:35,280 --> 00:08:38,960
And this is the most important part of suffering, this non-cruelty.

120
00:08:38,960 --> 00:08:45,600
It's actually a negative state, it's in the sense of not being an expression.

121
00:08:45,600 --> 00:08:49,520
It's the absence of something, the absence of cruelty.

122
00:08:49,520 --> 00:08:52,480
Whenever you speak with someone, it's free from the barbs.

123
00:08:52,480 --> 00:08:54,080
It's free from the poison.

124
00:08:54,080 --> 00:08:56,400
You never have this desire to hurt other people.

125
00:08:56,400 --> 00:08:59,840
And this is what I meant by enlightened people,

126
00:09:01,360 --> 00:09:07,120
though not always constantly working to alleviate people's suffering.

127
00:09:07,120 --> 00:09:11,680
They have compassion in the sense of not wanting to hurt others.

128
00:09:11,680 --> 00:09:14,000
And so even if they don't do anything,

129
00:09:14,000 --> 00:09:19,040
they have this at the times when they're sleeping, at the times when they're not involved

130
00:09:19,040 --> 00:09:23,360
with other beings, they still have this sense of not wanting to hurt others.

131
00:09:23,360 --> 00:09:28,480
And there's this absence of the desire to hurt others.

132
00:09:28,480 --> 00:09:33,600
So what you're really trying to accomplish here when you look at someone who's suffering,

133
00:09:33,600 --> 00:09:38,320
when you think of someone who's doing evil deeds or so on,

134
00:09:38,320 --> 00:09:39,760
is this state of non-cruelty?

135
00:09:39,760 --> 00:09:42,560
You can get to the point where you would never want to hurt other beings,

136
00:09:42,560 --> 00:09:45,600
where you're no longer angry when people do other things.

137
00:09:45,600 --> 00:09:48,480
You feel sorry for them, that they're going to suffer.

138
00:09:48,480 --> 00:09:51,840
And you never feel irritated and you never feel cruel.

139
00:09:51,840 --> 00:09:53,520
You never want to hurt others.

140
00:09:53,520 --> 00:09:57,280
This is the meaning of what is compassion.

141
00:09:57,280 --> 00:09:59,600
And it comes directly from wisdom.

142
00:09:59,600 --> 00:10:04,320
So rather than trying to develop it as according to this book,

143
00:10:04,320 --> 00:10:10,960
which it does give you very detailed method in terms of its development,

144
00:10:10,960 --> 00:10:14,480
I would recommend this a higher practice to develop wisdom

145
00:10:14,480 --> 00:10:19,680
and to give up cruelty, to be able to be free from cruelty.

146
00:10:19,680 --> 00:10:23,200
And in that way, whenever you see people suffering,

147
00:10:23,200 --> 00:10:27,920
because of your understanding of suffering and your natural aversion

148
00:10:27,920 --> 00:10:33,440
towards causing suffering in others, you will shy away from hurting others.

149
00:10:33,440 --> 00:10:36,800
And you'll feel compassion right away and want to help people

150
00:10:36,800 --> 00:10:39,040
be free from suffering because you understand it,

151
00:10:39,040 --> 00:10:40,640
because you've experienced it.

152
00:10:40,640 --> 00:10:47,840
And because you know how you have a very keen sense of the truth of suffering.

153
00:10:47,840 --> 00:10:56,160
OK, so in this sense, the state of compassion

154
00:10:56,160 --> 00:11:00,080
has no bearing on whether someone is bad off, someone is well off.

155
00:11:00,080 --> 00:11:05,360
It's a state of non-cruelty, a state of wishing

156
00:11:05,360 --> 00:11:10,040
to help people who are in suffering and not wishing to hurt people who are not.

157
00:11:10,040 --> 00:11:13,040
It's our interaction that is totally in harmony and is

158
00:11:13,040 --> 00:11:17,840
free from this evil evil that is for most of us a reality,

159
00:11:17,840 --> 00:11:21,360
where we do hurt other people, where we do try to get the better of them,

160
00:11:21,360 --> 00:11:24,160
where we do things that are going to cause them.

161
00:11:24,160 --> 00:11:27,280
Suffering sometimes just out of absolute cruelty,

162
00:11:27,280 --> 00:11:31,840
sometimes because we want something and we're not afraid to hurt others

163
00:11:31,840 --> 00:11:33,440
to get the things that we want.

164
00:11:33,440 --> 00:11:37,200
So that is my understanding of compassion.

165
00:11:37,200 --> 00:11:43,840
And I think it's very much based on the truth of meditation practice.

166
00:11:43,840 --> 00:11:45,440
OK, thanks for the question.

167
00:11:45,440 --> 00:12:15,280
Keep practicing.

