1
00:00:00,000 --> 00:00:03,480
But here's one about meditation, particularly.

2
00:00:03,480 --> 00:00:09,360
I know the difference between vipassana and samata, but not in the following situation.

3
00:00:09,360 --> 00:00:12,760
Sometimes in vipassana, you can stay with your breath for a while.

4
00:00:12,760 --> 00:00:15,080
Are you practicing samata then?

5
00:00:15,080 --> 00:00:16,800
Because you're staying with one object then.

6
00:00:16,800 --> 00:00:18,680
You're still aware of the rising and falling.

7
00:00:18,680 --> 00:00:24,240
I understand that when you are too deep, too deep in breath and you're in absorption,

8
00:00:24,240 --> 00:00:26,600
then you are practicing samata.

9
00:00:26,600 --> 00:00:32,880
No, this isn't the difference between what we call samata meditation and vipassana meditation

10
00:00:32,880 --> 00:00:35,920
in the sense that they are two different meditations.

11
00:00:35,920 --> 00:00:40,840
It's possible to say that at that moment you have samata because your mind is tranquil.

12
00:00:40,840 --> 00:00:45,400
At that moment your mind is not distracted, so you can say, well, that's samata.

13
00:00:45,400 --> 00:00:51,240
But based on the viseu-di-maga, based on the common areas, based on even I think the patisam-di-maga,

14
00:00:51,240 --> 00:00:53,600
samata is when you're focusing on a concept.

15
00:00:53,600 --> 00:00:57,400
It doesn't matter how long you're with the object, it matters what the object is.

16
00:00:57,400 --> 00:01:02,680
If the object is a concept and you're creating mindfulness about the object, you're clearly

17
00:01:02,680 --> 00:01:11,360
seeing the object as being what it is, like the Buddha or color or even the breath, seeing

18
00:01:11,360 --> 00:01:16,720
the breath as a concept going in and going out because we know that's not the actual experience

19
00:01:16,720 --> 00:01:19,360
of it, then that's samata.

20
00:01:19,360 --> 00:01:22,760
If you're still aware of the rising and falling at the moment when you're aware of the

21
00:01:22,760 --> 00:01:28,600
rising or the falling, you have the potential to develop viseu-di-maga in meditation.

22
00:01:28,600 --> 00:01:33,240
It doesn't mean that you are practicing viseu-di-maga, but if you see it clearly as rising

23
00:01:33,240 --> 00:01:38,360
or you see it clearly as falling, you're cultivating the force at the patina, and the force

24
00:01:38,360 --> 00:01:43,600
at the patina allowed the arising of the rising of viseu-di-maga.

25
00:01:43,600 --> 00:01:46,000
So that's considered viseu-di-maga.

26
00:01:46,000 --> 00:01:51,520
If you're focusing on something else, not outside of the five aggregate, so a concept,

27
00:01:51,520 --> 00:01:54,280
it's samata.

28
00:01:54,280 --> 00:02:00,880
Are you deriving this from like the viseu-di-maga?

29
00:02:00,880 --> 00:02:01,880
Yep.

30
00:02:01,880 --> 00:02:02,880
Send the commentary.

31
00:02:02,880 --> 00:02:22,080
Okay.

