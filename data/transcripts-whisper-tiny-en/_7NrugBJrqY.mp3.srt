1
00:00:00,000 --> 00:00:13,520
Good evening, everyone broadcasting live May 30th.

2
00:00:13,520 --> 00:00:17,400
This will be our last broadcast for a while.

3
00:00:17,400 --> 00:00:25,240
Maybe I'll do some audio broadcasts from Asia, but probably no video broadcasts for a lot.

4
00:00:25,240 --> 00:00:43,240
I'm heading off to Thailand, Sri Lanka, and maybe even Taiwan, but I'll be back in June 29th, so for a whole month.

5
00:00:43,240 --> 00:01:02,240
I'm going to see my teacher, just to let him know about our new monastery, our new center here, to get his blessing, to offer him some robes,

6
00:01:02,240 --> 00:01:22,240
and make a stop in Sri Lanka, just because it's Sri Lanka, and do some teaching in Sri Lanka as well, I guess.

7
00:01:22,240 --> 00:01:31,240
And then come back, and there's lots of people already signed up for meditation courses in July and August, so lots to do over the summer.

8
00:01:31,240 --> 00:01:42,240
Today's quote is about bathing.

9
00:01:42,240 --> 00:01:49,240
And this quote comes from the what?

10
00:01:49,240 --> 00:02:10,240
Discourse on the simile of the cloth, which is a rather famous suit among Buddhists that talks about purity.

11
00:02:10,240 --> 00:02:27,240
It's an important simile that a clean cloth, a pure cloth, can be dyed in any color or dye, and it takes the dye of what, it takes the color well.

12
00:02:27,240 --> 00:02:33,240
It's beautiful no matter what color you dye it, and you dip it in.

13
00:02:33,240 --> 00:02:39,240
But a dirty cloth, an impure cloth.

14
00:02:39,240 --> 00:02:55,240
Likewise, it doesn't matter what dye you dip it in, it's not going to be beautiful, it doesn't matter what color.

15
00:02:55,240 --> 00:03:05,240
What is the cause? Why is that? Because of the purity of the cloth, and the mind is the same.

16
00:03:05,240 --> 00:03:34,240
And it gives this simile a sange lite, a sugati, particular kha.

17
00:03:34,240 --> 00:03:48,240
An unpleasant future, particular kha without doubt, is to be had without doubt.

18
00:03:48,240 --> 00:04:04,240
Kiteya sange dite for a mind that is undefiled, that is pure. Happiness is future without doubt.

19
00:04:04,240 --> 00:04:15,240
Life is unsure. I do want me to be done. Think of all the changes in our lives, all the changes we've gone through.

20
00:04:15,240 --> 00:04:22,240
All the uncertainty we've had to face.

21
00:04:22,240 --> 00:04:39,240
And without a guide, without a compass of some sort, features very much uncertain.

22
00:04:39,240 --> 00:04:48,240
And seeing this and knowing this, we try to find, we try to direct and control our futures.

23
00:04:48,240 --> 00:04:58,240
But mostly, the way we work it is based on what we want, based on our preconceived desires and ideas.

24
00:04:58,240 --> 00:05:06,240
And that doesn't ever make us happy.

25
00:05:06,240 --> 00:05:15,240
We try to get happiness, but trying to get happiness doesn't make us happy.

26
00:05:15,240 --> 00:05:21,240
Well, it does just make us one happiness more.

27
00:05:21,240 --> 00:05:26,240
So, if there's nothing wrong with wanting happiness, right?

28
00:05:26,240 --> 00:05:35,240
If theoretically wanting happiness is the right idea, everyone should want to be happy.

29
00:05:35,240 --> 00:05:45,240
I can open the window if you want.

30
00:05:45,240 --> 00:05:48,240
We know about it the wrong way.

31
00:05:48,240 --> 00:05:53,240
We would have had purity for a very important reason.

32
00:05:53,240 --> 00:06:02,240
What she states in this, she makes clear, which is why we teach the concept of karma.

33
00:06:02,240 --> 00:06:11,240
So, to make us unhappy, to love karma is to help us find happiness, to provide that compass.

34
00:06:11,240 --> 00:06:14,240
You don't have to worry about so many things in life.

35
00:06:14,240 --> 00:06:16,240
You don't have to worry about what you're going to get.

36
00:06:16,240 --> 00:06:18,240
It's going to come your way.

37
00:06:18,240 --> 00:06:19,240
It's going to change.

38
00:06:19,240 --> 00:06:25,240
Our lives are always going to change.

39
00:06:25,240 --> 00:06:29,240
All we have to worry about is the source.

40
00:06:29,240 --> 00:06:37,240
We can't control, we can't be sure what's going to come to us in life.

41
00:06:37,240 --> 00:06:40,240
We can be sure of one thing.

42
00:06:40,240 --> 00:06:44,240
If the mind is pure, we'll be happy in the future.

43
00:06:44,240 --> 00:06:48,240
If the mind is impure, we will suffer from it.

44
00:06:48,240 --> 00:06:54,240
We'll be upset, we'll be unhappy, we'll have expectations that are unmet.

45
00:06:54,240 --> 00:07:00,240
We'll have disappointment and displeasure.

46
00:07:00,240 --> 00:07:04,240
But if the mind is pure, we will do good things for ourselves.

47
00:07:04,240 --> 00:07:06,240
We will do good things to help others.

48
00:07:06,240 --> 00:07:09,240
We will make good friends wherever we go.

49
00:07:09,240 --> 00:07:11,240
We don't know who those friends will be.

50
00:07:11,240 --> 00:07:20,240
We won't be able to choose necessary.

51
00:07:20,240 --> 00:07:25,240
But we will choose those who we meet.

52
00:07:25,240 --> 00:07:30,240
We will choose to associate with those who we meet who are pure.

53
00:07:30,240 --> 00:07:35,240
If our minds are impure, we will choose to associate with those who are impure.

54
00:07:35,240 --> 00:07:43,240
We'll hurt ourselves, we'll hurt each other.

55
00:07:43,240 --> 00:07:54,240
Goodness, purity, all these things that we fail often to grasp, to grasp all those.

56
00:07:54,240 --> 00:07:57,240
We're right in front of us.

57
00:07:57,240 --> 00:08:03,240
We all know what goodness is, we all know what purity is.

58
00:08:03,240 --> 00:08:05,240
We fail to see it.

59
00:08:05,240 --> 00:08:09,240
We fail to grasp.

60
00:08:09,240 --> 00:08:14,240
The Buddha gives this discourse and he talks about related things.

61
00:08:14,240 --> 00:08:17,240
It's a very good discussion, very much worth reading.

62
00:08:17,240 --> 00:08:28,240
But then he gets to this quote and he talks about how one becomes pure through insight meditation and so on.

63
00:08:28,240 --> 00:08:42,240
But then he says, I am going to speak a little bit.

64
00:08:42,240 --> 00:08:57,240
This is called because this is called a big who is based by the internal living.

65
00:08:57,240 --> 00:09:04,240
He uses the word bathing specifically because there's this one guy sitting in his audience.

66
00:09:04,240 --> 00:09:08,240
So he wants to reach out to.

67
00:09:08,240 --> 00:09:16,240
He says, he knows that this guy is going to, as the potential to become in mind.

68
00:09:16,240 --> 00:09:18,240
So he specifically mentions this.

69
00:09:18,240 --> 00:09:31,240
And right away, this Brahman, Sundarik and Bharadwaja asks the Buddha the question he says.

70
00:09:31,240 --> 00:09:55,240
He says, what's with the Bahoka river Brahman?

71
00:09:55,240 --> 00:10:02,240
He says, what's with the Bahoka river Brahman?

72
00:10:02,240 --> 00:10:06,240
What's so special about it?

73
00:10:06,240 --> 00:10:13,240
What is, what will the Bahoka river do?

74
00:10:13,240 --> 00:10:15,240
What is it?

75
00:10:15,240 --> 00:10:18,240
What good is it?

76
00:10:18,240 --> 00:10:28,240
And he shrugs and says, what is it?

77
00:10:28,240 --> 00:10:32,240
What is it?

78
00:10:32,240 --> 00:10:37,240
What is it?

79
00:10:37,240 --> 00:10:51,240
He says, don't look at some of that means the world, I think it means it's actually a word spelling.

80
00:10:51,240 --> 00:10:54,240
It should be local.

81
00:10:54,240 --> 00:11:01,240
The Bahoka river is understood as agreed to some of that.

82
00:11:01,240 --> 00:11:11,240
It's some of that means it's agreed to be.

83
00:11:11,240 --> 00:11:16,240
It's starting to be liberating.

84
00:11:16,240 --> 00:11:37,240
It's starting to be, it's starting to be for goodness by many people.

85
00:11:37,240 --> 00:11:47,240
It's starting to remove to wash away some Bahoka means evil demons.

86
00:11:47,240 --> 00:11:53,240
Evil deeds that I've been done and it's not to wash them away.

87
00:11:53,240 --> 00:11:55,240
This was a thing in India.

88
00:11:55,240 --> 00:12:01,240
Using this sort of language is important to address the idea that it's still prevalent in India

89
00:12:01,240 --> 00:12:08,240
that the rivers will wash away your evil demons, wash away your impurities.

90
00:12:08,240 --> 00:12:11,240
And again, talking about faith, faith does this.

91
00:12:11,240 --> 00:12:13,240
People have faith in this.

92
00:12:13,240 --> 00:12:17,240
I actually cultivate wholesomeness because they have this faith.

93
00:12:17,240 --> 00:12:22,240
Unfortunately, they cultivate also a lot of unwholesomeness with their wrong views,

94
00:12:22,240 --> 00:12:28,240
which causes them to ignore the importance of actually getting beyond things like faith.

95
00:12:28,240 --> 00:12:32,240
Because faith is good, not nearly enough.

96
00:12:32,240 --> 00:12:37,240
Especially when it's in the wrong thing.

97
00:12:37,240 --> 00:12:43,240
So Buddha shakes his head, I assume, and I don't think he would shake his head.

98
00:12:43,240 --> 00:12:49,240
Buddha says, all these rivers do nothing.

99
00:12:49,240 --> 00:13:06,240
If one is constantly consistent, it's forever as one is ever to be in these rivers.

100
00:13:06,240 --> 00:13:16,240
All these Bahoka river, the adi-kaka river, Gai river, Sundarika river.

101
00:13:16,240 --> 00:13:22,240
Canna come more on the subject, he can't purify black kama.

102
00:13:22,240 --> 00:13:25,240
Evil beings.

103
00:13:25,240 --> 00:13:29,240
King Sundarika Karysety, what will the Sundarika river do?

104
00:13:29,240 --> 00:13:32,240
King Bhagaya, Payaga.

105
00:13:32,240 --> 00:13:34,240
What will the Payaga river do?

106
00:13:34,240 --> 00:13:40,240
What will the Bahoka river do?

107
00:13:40,240 --> 00:13:50,240
Wearing kata, kim bisang naran, nahinan sudhi, papa kaminam.

108
00:13:50,240 --> 00:13:52,240
Kim bisang.

109
00:13:52,240 --> 00:14:00,240
It's like a translation.

110
00:14:00,240 --> 00:14:04,240
Kim bisang is cruel deeds, I think cruel.

111
00:14:04,240 --> 00:14:11,240
They cannot purify an evil doer, a man who's done cruel and brutal deeds.

112
00:14:11,240 --> 00:14:13,240
So what is true purity?

113
00:14:13,240 --> 00:14:22,240
Sundarseva is Sanda, Balugu, Payagu, Sunda's supposed to sanda.

114
00:14:22,240 --> 00:14:29,240
One pure and hard has ever more the feast of spring the holy day.

115
00:14:29,240 --> 00:14:43,240
Payagu is a special holy day for brahmanas on the moon of Payagu.

116
00:14:43,240 --> 00:14:45,240
Payagu nah.

117
00:14:45,240 --> 00:14:48,240
They have a day of purification.

118
00:14:48,240 --> 00:14:53,240
It's a special purification day and probably an Hinduism they still

119
00:14:53,240 --> 00:15:03,240
consider it to be a day of purification.

120
00:15:03,240 --> 00:15:08,240
One fair and act, one pure and hard brings his virtue to perfection.

121
00:15:08,240 --> 00:15:14,240
It is here brahmana that you should be to make yourself a refuge for all beings.

122
00:15:14,240 --> 00:15:22,240
If you speak no false hood nor work harm for living beings nor take what is offered not.

123
00:15:22,240 --> 00:15:27,240
With faith and free from avarice would need for you to go to Gaya.

124
00:15:27,240 --> 00:15:33,240
For any well will be your Gaya.

125
00:15:33,240 --> 00:15:37,240
King Gaya, Kasi, Gaya, Nganpa.

126
00:15:37,240 --> 00:15:53,240
I think Tamika gets it better than Bodhi.

127
00:15:53,240 --> 00:15:59,240
I'm not sure when Bodhi translates to what's that.

128
00:15:59,240 --> 00:16:09,240
The Bhana means well, your well will be your Gaya.

129
00:16:09,240 --> 00:16:15,240
Meaning the water in your well is the same with Gaya.

130
00:16:15,240 --> 00:16:18,240
It's the same water.

131
00:16:18,240 --> 00:16:23,240
It means that you have Gaya everywhere you go.

132
00:16:23,240 --> 00:16:26,240
This idea of purification.

133
00:16:26,240 --> 00:16:31,240
A lot of strange ideas of purification, a lot of people believe in the importance of purifying

134
00:16:31,240 --> 00:16:32,240
the body.

135
00:16:32,240 --> 00:16:41,240
It's funny how much emphasis we put on the body.

136
00:16:41,240 --> 00:16:46,240
Healthy body, healthy mind, they say.

137
00:16:46,240 --> 00:16:53,240
Which arguably there is some benefit to having a healthy body or taking care of your body.

138
00:16:53,240 --> 00:16:57,240
But it's funny that no one thinks to actually make the mind healthy and make the mind pure.

139
00:16:57,240 --> 00:17:02,240
It's not true, but we put far less emphasis on making the mind healthy.

140
00:17:02,240 --> 00:17:05,240
The mind we want just to get what we want.

141
00:17:05,240 --> 00:17:10,240
Even our spirituality tends to be based on seeking pleasure.

142
00:17:10,240 --> 00:17:12,240
Seeking happiness.

143
00:17:12,240 --> 00:17:18,240
Which, you know, they said, it stands really good to want to be happy.

144
00:17:18,240 --> 00:17:20,240
And what he wants to suffer.

145
00:17:20,240 --> 00:17:23,240
The problem is one thing isn't enough.

146
00:17:23,240 --> 00:17:26,240
And if your focus is happiness, you'll never be happy.

147
00:17:26,240 --> 00:17:28,240
It's funny, huh?

148
00:17:28,240 --> 00:17:32,240
Your focus is trying to find happiness.

149
00:17:32,240 --> 00:17:35,240
Your focus is the happiness.

150
00:17:35,240 --> 00:17:40,240
It's a problem because happiness doesn't need to happiness.

151
00:17:40,240 --> 00:17:44,240
Happiness might lead to...

152
00:17:44,240 --> 00:17:50,240
It's not happiness that leads to happiness.

153
00:17:50,240 --> 00:17:53,240
It's just like it's not suffering that leads to suffering.

154
00:17:53,240 --> 00:17:55,240
It's goodness that leads to happiness.

155
00:17:55,240 --> 00:18:01,240
It's evil that leads to suffering.

156
00:18:01,240 --> 00:18:04,240
This is why we don't have to worry about our future.

157
00:18:04,240 --> 00:18:06,240
We can't be sure what it's going to be like.

158
00:18:06,240 --> 00:18:11,240
Sometimes that's disconcerting to know that our future might.

159
00:18:11,240 --> 00:18:15,240
Not be what we thought it was going to be.

160
00:18:15,240 --> 00:18:19,240
So we have to reassess and re-adjust,

161
00:18:19,240 --> 00:18:22,240
re-evaluate what's important about life.

162
00:18:22,240 --> 00:18:24,240
It's not the situation.

163
00:18:24,240 --> 00:18:27,240
It's not whether I go to Asia or not.

164
00:18:27,240 --> 00:18:32,240
It's not whether I have a meditation center here in Hamilton or somewhere else.

165
00:18:32,240 --> 00:18:35,240
It's not whether I get this job or that job.

166
00:18:35,240 --> 00:18:40,240
It's not whether I have these friends or these families.

167
00:18:40,240 --> 00:18:44,240
Family.

168
00:18:44,240 --> 00:18:46,240
It's about your mind.

169
00:18:46,240 --> 00:18:49,240
It's about your outlook.

170
00:18:49,240 --> 00:18:52,240
It's about what you bring to the situation.

171
00:18:52,240 --> 00:18:54,240
How you approach it.

172
00:18:54,240 --> 00:18:55,240
How you live.

173
00:18:55,240 --> 00:18:56,240
How you are.

174
00:18:56,240 --> 00:18:57,240
Who you are.

175
00:18:57,240 --> 00:18:58,240
What you are.

176
00:18:58,240 --> 00:19:03,240
Are you pure in mind with good intentions for yourself and others?

177
00:19:03,240 --> 00:19:09,240
Or corrupt in mind with evil intentions for yourself and others?

178
00:19:09,240 --> 00:19:11,240
This is what we have to look at.

179
00:19:11,240 --> 00:19:14,240
This is what meditation is all about.

180
00:19:14,240 --> 00:19:17,240
It's about cultivating this purity.

181
00:19:17,240 --> 00:19:22,240
Striving to be pure in heart, pure in mind.

182
00:19:22,240 --> 00:19:25,240
So that's our quote for tonight.

183
00:19:25,240 --> 00:19:27,240
It's about bathing.

184
00:19:27,240 --> 00:19:30,240
We should all be sure to bathe internally.

185
00:19:30,240 --> 00:19:40,240
Let's see if we have some questions.

186
00:19:40,240 --> 00:19:44,240
I have a question about karma.

187
00:19:44,240 --> 00:19:49,240
We have a bunch of green people, which is always nice to see.

188
00:19:49,240 --> 00:19:50,240
Most of you are green.

189
00:19:50,240 --> 00:19:51,240
Thank you.

190
00:19:51,240 --> 00:19:53,240
That's appreciated.

191
00:19:53,240 --> 00:19:54,240
Immeditate together.

192
00:19:54,240 --> 00:19:57,240
Sometimes it's a hardship to have to meditate at the same time.

193
00:19:57,240 --> 00:20:03,240
I know for many people it's not possible because we're a global middle of the night for some people.

194
00:20:03,240 --> 00:20:04,240
I understand.

195
00:20:04,240 --> 00:20:12,240
But that people make the effort to come and meditate together is very much appreciated.

196
00:20:12,240 --> 00:20:14,240
So blame and good, bad.

197
00:20:14,240 --> 00:20:17,240
Aren't these concepts just an excuse to judge yourself or others?

198
00:20:17,240 --> 00:20:21,240
The thing about karma is it's not that it's a good thing.

199
00:20:21,240 --> 00:20:25,240
It's not that we were happy that it's this way.

200
00:20:25,240 --> 00:20:29,240
It's that man that's a real kind of a real bummer that it's this way.

201
00:20:29,240 --> 00:20:40,240
It's a bummer that we have to now deal with the things that we've done in the past.

202
00:20:40,240 --> 00:20:47,240
It's a bummer that we have to feel the effects of karma and be great if we didn't have to.

203
00:20:47,240 --> 00:20:55,240
I mean I suppose in terms of the world now it would be horrific because we kill each other and do whatever we want.

204
00:20:55,240 --> 00:21:02,240
But it's not that way.

205
00:21:02,240 --> 00:21:07,240
I guess it's how you look at it.

206
00:21:07,240 --> 00:21:15,240
It's actually quite liberating to know that if you get mugged that it's actually because you mugged people in the past.

207
00:21:15,240 --> 00:21:19,240
It's not always, it's hardly ever quite so simple.

208
00:21:19,240 --> 00:21:23,240
It's hardly ever anywhere near as simple as that.

209
00:21:23,240 --> 00:21:24,240
They're often trends.

210
00:21:24,240 --> 00:21:26,240
So why didn't that be great to know?

211
00:21:26,240 --> 00:21:27,240
Oh wow.

212
00:21:27,240 --> 00:21:28,240
And that's what Buddhist do.

213
00:21:28,240 --> 00:21:31,240
When they get mugged they say, it's probably my karma.

214
00:21:31,240 --> 00:21:34,240
I probably mugged that person in the past.

215
00:21:34,240 --> 00:21:37,240
It's not so gross over simplification.

216
00:21:37,240 --> 00:21:44,240
It ignores the fact that the person, the other person is cultivating new karma.

217
00:21:44,240 --> 00:21:46,240
But it's quite freeing.

218
00:21:46,240 --> 00:21:50,240
It's again gets back to not worrying so much about your circumstances.

219
00:21:50,240 --> 00:21:56,240
And getting a sense that all of that is or much of that is preordained.

220
00:21:56,240 --> 00:21:59,240
Much of our circumstances is going to be beyond our control.

221
00:21:59,240 --> 00:22:02,240
Things are going to happen that we didn't foresee and couldn't foresee.

222
00:22:02,240 --> 00:22:07,240
A lot of that's just because of how we've situated ourselves.

223
00:22:07,240 --> 00:22:13,240
The situation we've got ourselves in.

224
00:22:13,240 --> 00:22:19,240
I think I need Robin back on here to point the questions out to me.

225
00:22:19,240 --> 00:22:23,240
No meditation leads someone to be immune to hostility and society.

226
00:22:23,240 --> 00:22:26,240
It helps someone become a better version of themselves.

227
00:22:26,240 --> 00:22:27,240
Yeah, I saw that.

228
00:22:27,240 --> 00:22:31,240
But this was a person who hadn't meditated with us.

229
00:22:31,240 --> 00:22:32,240
Yikes.

230
00:22:32,240 --> 00:22:34,240
Where did everyone go?

231
00:22:34,240 --> 00:22:41,240
So I didn't want to answer because I figured if they started to meditate they'd get the answer.

232
00:22:41,240 --> 00:22:51,240
Because I think the answer is simply yes.

233
00:22:51,240 --> 00:22:55,240
Where the weekly practice interview happened during June.

234
00:22:55,240 --> 00:23:01,240
Should the time is going to be wonky, but I think the times are okay for me.

235
00:23:01,240 --> 00:23:05,240
I think one of them is like five, thirty or something in Sri Lanka.

236
00:23:05,240 --> 00:23:11,240
Luckily, Thailand is 12 hours or 11 hours from me, time-wise.

237
00:23:11,240 --> 00:23:13,240
That's Sri Lanka's a little bit.

238
00:23:13,240 --> 00:23:16,240
It's just going to mean they'll have interviews at 5.30am.

239
00:23:16,240 --> 00:23:18,240
Starting at 5.30am.

240
00:23:18,240 --> 00:23:20,240
Those are the evening interviews.

241
00:23:20,240 --> 00:23:23,240
We'll be at 5.30am, which is fine.

242
00:23:23,240 --> 00:23:26,240
Of course.

243
00:23:26,240 --> 00:23:29,240
The time is going to be all strange anyway.

244
00:23:29,240 --> 00:23:35,240
I know how the brain is when we fly halfway across the world.

245
00:23:35,240 --> 00:23:37,240
It'll be okay now.

246
00:23:37,240 --> 00:23:40,240
Wow, we got a huge list today.

247
00:23:40,240 --> 00:23:44,240
Most that's 30 of the people meditated here.

248
00:23:44,240 --> 00:23:49,240
Some people more than once.

249
00:23:49,240 --> 00:23:58,240
Awesome.

250
00:23:58,240 --> 00:23:59,240
Okay.

251
00:23:59,240 --> 00:24:01,240
That's all for the questions.

252
00:24:01,240 --> 00:24:07,240
I'm going to say goodnight and wishing you all the best for those of you who rely on

253
00:24:07,240 --> 00:24:14,240
YouTube to get these teachings while you might have to come on over to meditation.ceremongo.org.

254
00:24:14,240 --> 00:24:20,240
I'm guessing some of it's going to be audio for the next month.

255
00:24:20,240 --> 00:24:23,240
We won't have so much video.

256
00:24:23,240 --> 00:24:29,240
I'm not bringing my video camera to my age.

257
00:24:29,240 --> 00:24:31,240
Maybe someone else will record something.

258
00:24:31,240 --> 00:24:42,240
Some talks I give, but otherwise I can just have to be patient.

259
00:24:42,240 --> 00:24:47,240
Heck, there's so many videos on YouTube already.

260
00:24:47,240 --> 00:24:50,240
Just go watch some of you.

261
00:24:50,240 --> 00:24:57,240
Anyway, I'll see you all the best having a good night.

