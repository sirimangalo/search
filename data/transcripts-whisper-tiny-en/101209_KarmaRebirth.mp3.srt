1
00:00:00,000 --> 00:00:08,080
All right then. Welcome everyone. Today's talk will be about two important concepts in

2
00:00:08,080 --> 00:00:17,080
Buddhist teaching. These are karma and rebirth. Now, I see these are important concepts.

3
00:00:17,080 --> 00:00:24,120
And I think this is a debated statement. Many people seem to think that karma and rebirth

4
00:00:24,120 --> 00:00:30,920
have nothing to do with the Buddhist teaching, especially the teaching on rebirth. There's

5
00:00:30,920 --> 00:00:39,040
a lot of belief that the Buddha taught only for one life and nothing to do with the idea

6
00:00:39,040 --> 00:00:47,240
of rebirth. Really, these sorts of debates, I think, center around our attachment to

7
00:00:47,240 --> 00:01:02,400
views, our opinions and views of what the Buddha taught, rather than actual understanding

8
00:01:02,400 --> 00:01:08,640
of reality. Because when you practice the Buddha's teaching, it really makes no difference

9
00:01:08,640 --> 00:01:18,280
as to what is one's opinion or the idea of what it was that the Buddha taught Buddha taught

10
00:01:18,280 --> 00:01:27,680
or so on. And whether the Buddha taught about rebirth, about the past lives or future

11
00:01:27,680 --> 00:01:37,880
lives, there's no argument that arises because through the practice you come to see for

12
00:01:37,880 --> 00:01:46,880
yourself the reality of things. And what we come to see when we practice the Buddha's

13
00:01:46,880 --> 00:01:55,640
teaching and understand the reality of things is that really in a way the Buddha didn't

14
00:01:55,640 --> 00:02:11,680
teach karma and rebirth in their standard form. Because the word karma means action.

15
00:02:11,680 --> 00:02:29,960
And the doctrine of karma means the belief that every action that we do has an effect on

16
00:02:29,960 --> 00:02:39,040
our happiness and our unhappiness. Either that or certain actions when performed haven't

17
00:02:39,040 --> 00:02:53,040
effect on our happiness and our unhappiness in and of themselves. And this was the doctrine

18
00:02:53,040 --> 00:03:02,880
that was understood to be the truth by many people before the time of the Buddha. And

19
00:03:02,880 --> 00:03:16,240
it was taught by the brahmanas, the priests. And the reason it was taught by them it seems

20
00:03:16,240 --> 00:03:22,400
is because this was a very profitable thing to teach. When you teach people that certain

21
00:03:22,400 --> 00:03:33,240
ritual actions are to their benefit for either warding off evil or bringing blessings

22
00:03:33,240 --> 00:03:43,840
and so on. And those actions were actions that required a priest to perform. Then it was

23
00:03:43,840 --> 00:03:50,800
a great way to make your living. And so this evolved into an entire doctrine based on

24
00:03:50,800 --> 00:04:01,280
these sacrifices that the brahmanas did. It was also prior to the establishment of the

25
00:04:01,280 --> 00:04:10,160
priestly caste or group. It seems it was a part of the nomadic culture of the audience.

26
00:04:10,160 --> 00:04:16,920
These people who came in slaughtered all the natives in India. When they won the battle

27
00:04:16,920 --> 00:04:23,920
they would always offer something to their god, to Indra. And you can see this of course

28
00:04:23,920 --> 00:04:35,960
in all nomadic or native culture and not native tribal cultures. This idea of sacrifice.

29
00:04:35,960 --> 00:04:48,400
You can see it in Judaism, in the Old Testament, the Hebrew Bible. This idea that some

30
00:04:48,400 --> 00:04:53,280
certain activities had benefit. And the Buddha taught against this. This was the teaching

31
00:04:53,280 --> 00:05:05,480
that the Buddha had to replace. Because obviously it's not for those of us who have an intellectual

32
00:05:05,480 --> 00:05:11,880
or a scientific understanding of reality, we can see and we can verify for ourselves through

33
00:05:11,880 --> 00:05:19,920
experimentation, through research, through empirical observation that this isn't true,

34
00:05:19,920 --> 00:05:28,760
that these actions don't actually have any intrinsic benefit. And so with the Buddha

35
00:05:28,760 --> 00:05:33,560
taught as karma, and he just borrowed the word, he really didn't teach karma at all.

36
00:05:33,560 --> 00:05:41,880
He taught intention. Jaitana. And he said that what these people are talking about with

37
00:05:41,880 --> 00:05:49,320
the word karma is actually the truth of it is that it is the job, it is the role of

38
00:05:49,320 --> 00:06:01,200
Jaitana of one's intention, one's relation. When one has wholesome intentions, it doesn't

39
00:06:01,200 --> 00:06:10,240
matter what one, what one does, one's mind has wisdom in it or has the desire to sacrifice

40
00:06:10,240 --> 00:06:18,840
or love in it or so on. And everything one does with that mind, based on that mind,

41
00:06:18,840 --> 00:06:25,680
the result of that mind, the result of that karma, that mental action, will inevitably

42
00:06:25,680 --> 00:06:35,800
be positive. And so this is the Buddha's doctrine of karma. It's a very simple doctrine

43
00:06:35,800 --> 00:06:46,560
and it's very scientific. Because in understanding both karma and rebirth, we have to understand

44
00:06:46,560 --> 00:06:50,920
the nature of reality according to the Buddha, according to the Buddha's teaching.

45
00:06:50,920 --> 00:07:01,040
That the Buddha, again, didn't talk about reality as something out there. He talked about

46
00:07:01,040 --> 00:07:06,160
two kinds of reality. This conceptual reality that we talk about the universe, you might

47
00:07:06,160 --> 00:07:11,120
even talk about second life as a conceptual reality. In the same way that the world around

48
00:07:11,120 --> 00:07:16,360
us is a conceptual reality. So we can talk about the Deer Park, we can talk about the

49
00:07:16,360 --> 00:07:22,360
hall, the meditation hall up there, the store, the hot spring, the mountains, but these

50
00:07:22,360 --> 00:07:30,120
are all just concepts. They don't really exist. But we use them to refer to, refer these

51
00:07:30,120 --> 00:07:39,080
places to other people. And we do the same with the world around us. We refer to countries

52
00:07:39,080 --> 00:07:49,320
and cities and places. This three-dimensional reality that is out there. And has actually

53
00:07:49,320 --> 00:07:59,280
been shown to be only merely conceptual by modern physics. That reality can only be thought

54
00:07:59,280 --> 00:08:08,200
of in terms of events, in terms of experience. And this is very much the Buddha's teaching

55
00:08:08,200 --> 00:08:16,760
that all of reality is dependent on the mind. There's the physical side of reality, but

56
00:08:16,760 --> 00:08:25,000
it is only encompassed by experience. The ultimate reality of it is the experience of it.

57
00:08:25,000 --> 00:08:30,080
When we experience the physical reality around us seeing and hearing and smelling and

58
00:08:30,080 --> 00:08:50,680
tasting and feeling and thinking, this is what's real. And so our experience of reality

59
00:08:50,680 --> 00:09:01,120
will define the results that we receive. If we choose to direct our minds in a certain

60
00:09:01,120 --> 00:09:08,720
direction, that's the reality that the reality that arises will be based on that action,

61
00:09:08,720 --> 00:09:14,360
on that intention. When we talk about karma, it's not something magical or mystical

62
00:09:14,360 --> 00:09:18,560
where someone kills someone and then you sit around waiting for them to be hit by lightning

63
00:09:18,560 --> 00:09:26,920
or something. Or even sit around waiting for them to go to smug in the fact that they're

64
00:09:26,920 --> 00:09:37,840
going to go to hell or something. It's actually not a cut and dry doctrine in that way.

65
00:09:37,840 --> 00:09:41,520
You can't say that if a person does all sorts of bad things, they're necessarily going

66
00:09:41,520 --> 00:09:53,640
to go to hell. That's unscientific. Because it's a simplistic sort of magical pseudo-scientific

67
00:09:53,640 --> 00:09:59,800
understanding or belief, this idea that certain things automatically lead you to certain

68
00:09:59,800 --> 00:10:08,280
results. And that's unscientific. We can say that certain intentions incline in a certain

69
00:10:08,280 --> 00:10:12,760
direction, but other than that, unless we can somehow take into account all the other

70
00:10:12,760 --> 00:10:22,400
factors involved, our intention, our past intentions, the direction we're heading and so on,

71
00:10:22,400 --> 00:10:29,480
that we can't really make those kinds of claims as to what a certain act is going to

72
00:10:29,480 --> 00:10:35,560
affect it's going to have. It's like weather. You can look at a certain weather and pattern

73
00:10:35,560 --> 00:10:41,480
and say, oh, this is going to create a different weather pattern. And then the results

74
00:10:41,480 --> 00:10:46,200
come out completely different because there are so many different factors involved that

75
00:10:46,200 --> 00:10:52,760
you can't possibly take into account. You can only offer probabilities unless you have

76
00:10:52,760 --> 00:10:59,200
some super-advanced understanding of all of the different variables and factors involved

77
00:10:59,200 --> 00:11:11,920
in the process. So the teaching of karma really is that basically in a basic understanding

78
00:11:11,920 --> 00:11:21,360
is that bad things have a bad effect on our lives. That's why they're bad. Certain

79
00:11:21,360 --> 00:11:30,840
things, certain intentions will have a bad result or will incline towards a bad result.

80
00:11:30,840 --> 00:11:38,960
Certain things will incline towards a good result. Will it lead to our benefit?

81
00:11:38,960 --> 00:11:42,160
And so if we understand karma in this way, we can really understand the details of

82
00:11:42,160 --> 00:11:45,280
the doctrine. We can understand how it works. And we can see that it's actually not a

83
00:11:45,280 --> 00:11:51,600
cut-and-dry thing where all we can say is that certain things are unadvisable because

84
00:11:51,600 --> 00:11:59,360
they're dangerous. They have the potential to lead to our suffering.

85
00:11:59,360 --> 00:12:08,080
So first on a phenomenological or an experiential level, there are four kinds of karma.

86
00:12:08,080 --> 00:12:14,080
This is based on our every moment of the experience, every karma that we perform. When

87
00:12:14,080 --> 00:12:24,480
we get angry, when we have greed in our minds, when we give rise to these unals and states,

88
00:12:24,480 --> 00:12:35,000
they will have four results. One kind will actually give rise to a specific result.

89
00:12:35,000 --> 00:12:41,280
So certain acts that we perform will give a result. Sometimes when you do something bad

90
00:12:41,280 --> 00:12:46,760
or you do something good, right away you experience the result. Actually, this is generally

91
00:12:46,760 --> 00:12:54,720
the case that most of the things we do do have a immediate result. And it's just that

92
00:12:54,720 --> 00:13:00,080
we don't see it or we don't understand the relationship between the two. For instance, when

93
00:13:00,080 --> 00:13:05,200
you're generous with someone, you give them something or you offer your time to them or

94
00:13:05,200 --> 00:13:09,760
your kind words or so on, you find the right way you feel good about yourself. You feel

95
00:13:09,760 --> 00:13:17,200
calm, you feel happy, you feel proud in a sense, or you feel good about what you've done.

96
00:13:17,200 --> 00:13:26,880
And that's the direct result that comes from the good karma. But certain times we'll

97
00:13:26,880 --> 00:13:34,040
do something and we're expecting a result of a certain sort and we don't get that result.

98
00:13:34,040 --> 00:13:37,840
In fact, sometimes it isn't the case where we give something to someone. Sometimes we can

99
00:13:37,840 --> 00:13:44,720
give or be generous towards someone or do something good in a very one of the various ways.

100
00:13:44,720 --> 00:13:55,200
And get a bad result. We give something to someone and they become upset because of it.

101
00:13:55,200 --> 00:14:01,560
Maybe we don't give them what they wanted or you give a person on the street, give them

102
00:14:01,560 --> 00:14:07,680
a dollar bill and they get angry and they wanted more or so on. Maybe they give them

103
00:14:07,680 --> 00:14:13,600
one dollar and then all of a sudden their friend comes and wants another dollar or so on.

104
00:14:13,600 --> 00:14:18,640
And you feel upset by it.

105
00:14:18,640 --> 00:14:28,760
So only certain types of karma will give a direct result. Other types of karma have

106
00:14:28,760 --> 00:14:38,320
the effect of supporting the right arising of results. So sometimes we'll do something

107
00:14:38,320 --> 00:14:43,080
and it doesn't give us a good result right away or it doesn't give us a bad result right

108
00:14:43,080 --> 00:14:50,280
away. But as we perform the same action again and again or repeatedly perform wholesome

109
00:14:50,280 --> 00:14:58,080
around wholesome actions, the accumulation of the evil deeds or the good deeds leads to the

110
00:14:58,080 --> 00:15:04,760
result of the action, leads to the bad result or the good result. This is called supportive

111
00:15:04,760 --> 00:15:11,840
karma. Sometimes it's required to do the same act again and again or to perform multiple

112
00:15:11,840 --> 00:15:21,840
acts in order to bring about a result. If supporting factors aren't present then sometimes

113
00:15:21,840 --> 00:15:26,560
our good deeds won't come to result to good results.

114
00:15:26,560 --> 00:15:31,680
So the point is that we shouldn't feel upset when good deeds don't always lead to good

115
00:15:31,680 --> 00:15:43,200
results or feel also not be negligent when our bad deeds don't always lead to bad results.

116
00:15:43,200 --> 00:15:53,200
Sometimes they're just waiting for the accumulation of bad deeds. Sometimes all it takes

117
00:15:53,200 --> 00:16:00,200
is one like the straw that broke the camel's back.

