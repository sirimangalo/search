1
00:00:30,000 --> 00:00:46,400
Well, that's what you do with the knowing is the difference.

2
00:00:46,400 --> 00:00:52,480
A dog has knowing, but then right after the knowing, there is identification with it and

3
00:00:52,480 --> 00:00:58,000
there's a whole bunch of delusion and ignorance and all sorts of unwholesome mind states

4
00:00:58,000 --> 00:01:00,000
that prevent it.

