1
00:00:00,000 --> 00:00:04,640
Hi, so welcome back to Ask a Monk.

2
00:00:04,640 --> 00:00:09,600
Today's question is from the semi-G123.

3
00:00:09,600 --> 00:00:14,240
I find it difficult to clear my mind of thought as they always pop into my head and I can't

4
00:00:14,240 --> 00:00:15,240
control it.

5
00:00:15,240 --> 00:00:19,920
When I try to be aware of my breathing but not control it, this is also very hard because

6
00:00:19,920 --> 00:00:23,000
I become conscious and I automatically do so.

7
00:00:23,000 --> 00:00:26,160
Any tips?

8
00:00:26,160 --> 00:00:33,840
Actually this explanation is a clear indication that you're practicing correctly.

9
00:00:33,840 --> 00:00:38,480
And what I mean by that is you're starting to see the way your mind works.

10
00:00:38,480 --> 00:00:42,320
You're starting to see that the nature of the mind is to automatically control things.

11
00:00:42,320 --> 00:00:43,720
It'll try to control everything.

12
00:00:43,720 --> 00:00:48,480
We try to control every part of our experience to make sure that it is the way that we

13
00:00:48,480 --> 00:00:52,160
want it to be and not be the way that we don't want it to be.

14
00:00:52,160 --> 00:00:57,520
So the first part of your question or the first part of your problem as far as thought

15
00:00:57,520 --> 00:01:01,080
is popping into your head and not being able to control it shows that you still have

16
00:01:01,080 --> 00:01:03,360
the idea that we're trying to control things.

17
00:01:03,360 --> 00:01:04,360
And this is natural.

18
00:01:04,360 --> 00:01:06,360
This is the way that we look at things.

19
00:01:06,360 --> 00:01:08,480
We think we have to control everything.

20
00:01:08,480 --> 00:01:13,360
So when thought enters the mind that's wrong, trying to stop it is of course our natural

21
00:01:13,360 --> 00:01:14,360
reflex.

22
00:01:14,360 --> 00:01:17,680
And that's the wrong way to approach things.

23
00:01:17,680 --> 00:01:18,720
That's what we're starting to realize.

24
00:01:18,720 --> 00:01:20,920
You're starting to realize that you can't control it.

25
00:01:20,920 --> 00:01:28,240
There's no way that you could possibly stop your mind from thinking except temporarily

26
00:01:28,240 --> 00:01:33,240
if you practice tranquility meditation to suppress the thoughts temporarily.

27
00:01:33,240 --> 00:01:36,320
But that is never a permanent solution.

28
00:01:36,320 --> 00:01:41,600
And in the end, this is why we teach people to let go and to accept the experience.

29
00:01:41,600 --> 00:01:46,160
So then when you try to go the other way at the same time, so on the one side you're

30
00:01:46,160 --> 00:01:48,440
saying you're trying to control your thoughts and can't do it.

31
00:01:48,440 --> 00:01:51,560
On the other side you're saying you're trying not to control your breath but you can't

32
00:01:51,560 --> 00:01:53,560
do that either.

33
00:01:53,560 --> 00:01:57,960
So this is the fight between the two ways of looking at things, you know, trying to control

34
00:01:57,960 --> 00:01:59,240
and trying not to control.

35
00:01:59,240 --> 00:02:01,240
The correct way is to not control.

36
00:02:01,240 --> 00:02:06,960
But even trying to not control is a form of trying to control, you know, forcing yourself

37
00:02:06,960 --> 00:02:08,600
to not force things.

38
00:02:08,600 --> 00:02:11,840
So there's no way out of that.

39
00:02:11,840 --> 00:02:16,600
The only way to become free from this is to see clearly that trying to control things is

40
00:02:16,600 --> 00:02:17,640
a problem.

41
00:02:17,640 --> 00:02:21,640
And as you watch the breath rising, falling and you see yourself controlling it again and

42
00:02:21,640 --> 00:02:22,880
again and again.

43
00:02:22,880 --> 00:02:25,360
And you're forced to start over again and again and again and again and again and force

44
00:02:25,360 --> 00:02:32,000
to deal with the frustration, forced to deal with the impatience, the suffering involved

45
00:02:32,000 --> 00:02:35,320
with controlling, involved with forcing the breath.

46
00:02:35,320 --> 00:02:39,160
Your mind was slowly realized, it's like you're teaching yourself as you would teach

47
00:02:39,160 --> 00:02:44,160
a child by direct experience, learning the hard way that this is the wrong way to deal

48
00:02:44,160 --> 00:02:45,160
with things.

49
00:02:45,160 --> 00:02:50,040
Slowly let go and you're slowly naturally the breath will come by itself and there will

50
00:02:50,040 --> 00:02:51,120
be no more controlling.

51
00:02:51,120 --> 00:02:53,400
You can't stop yourself from controlling.

52
00:02:53,400 --> 00:03:01,680
All you can do is see clearly, which you get by this clear thought, that clinging to things

53
00:03:01,680 --> 00:03:04,840
and forcing things is a cause for suffering.

54
00:03:04,840 --> 00:03:11,800
It's something that is painful, is stressful, and is unhealthy.

55
00:03:11,800 --> 00:03:12,800
Okay.

56
00:03:12,800 --> 00:03:13,800
So I hope that helps.

57
00:03:13,800 --> 00:03:14,800
Good work.

58
00:03:14,800 --> 00:03:17,480
This is the first step in meditation.

59
00:03:17,480 --> 00:03:20,840
The first step in letting go is to see that you're clinging.

60
00:03:20,840 --> 00:03:25,320
And I would encourage you to look carefully and see what your emotions are during the time

61
00:03:25,320 --> 00:03:28,760
that you feel like your breath is being forced.

62
00:03:28,760 --> 00:03:32,600
Are you angry, are you frustrated, are you stressed?

63
00:03:32,600 --> 00:03:37,760
What is your reaction to this when you're forcing things and try not to get frustrated

64
00:03:37,760 --> 00:03:41,760
and think that you're trying not to think that you know somehow your practice is wrong.

65
00:03:41,760 --> 00:03:49,680
Try to be patient and use it as a reason to build up patients in your practice and to

66
00:03:49,680 --> 00:03:52,560
try to look at things and let things be the way they are.

67
00:03:52,560 --> 00:03:54,960
When you're forcing things, let yourself force them.

68
00:03:54,960 --> 00:03:58,720
You know, see that you're forcing it, feel the pain of forcing it, feel the suffering, and

69
00:03:58,720 --> 00:04:01,040
just come to learn about that experience.

70
00:04:01,040 --> 00:04:05,800
So that in the future you can be clear that when you force things, this is what happens.

71
00:04:05,800 --> 00:04:06,800
Okay.

72
00:04:06,800 --> 00:04:08,480
Again, keep up the good work.

73
00:04:08,480 --> 00:04:10,320
Glad to hear that someone's actually meditating.

74
00:04:10,320 --> 00:04:12,840
Hmm.

