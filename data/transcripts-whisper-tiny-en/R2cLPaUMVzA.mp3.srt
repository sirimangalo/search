1
00:00:00,000 --> 00:00:07,280
Bhante, so in short all I have to do is mindfully know everything that arises in my mind

2
00:00:07,280 --> 00:00:12,240
and even if I don't know anything else about the dhamma, I'd be able to achieve navanda.

3
00:00:12,240 --> 00:00:16,520
So maybe I should not bother with anything else but mindfulness.

4
00:00:16,520 --> 00:00:21,960
Sounds good, no?

5
00:00:21,960 --> 00:00:24,480
It could theoretically work.

6
00:00:24,480 --> 00:00:30,080
The Buddha said, A. K. Anoyang bhui mango mindfulness is the one-way path or even the only

7
00:00:30,080 --> 00:00:37,400
path depending on how you interpret that.

8
00:00:37,400 --> 00:00:51,440
But a simple answer is it only really works if you've got a very strong will to do that.

9
00:00:51,440 --> 00:00:56,160
And I would say therefore a very strong wholesomeness already in your mind.

10
00:00:56,160 --> 00:01:02,280
I would say if you're probably, you're already enlightened, let's say, or you have a teacher

11
00:01:02,280 --> 00:01:08,480
who can guide you through all the mistakes that you're going to make, all the times when

12
00:01:08,480 --> 00:01:14,920
you're actually not doing that.

13
00:01:14,920 --> 00:01:24,040
But I would say for all intents and purposes, that's a fairly good outlook.

14
00:01:24,040 --> 00:01:28,440
I don't see, there's not anything really wrong with the idea.

15
00:01:28,440 --> 00:01:35,600
If you fix that in your mind that mindfulness is really the key here.

16
00:01:35,600 --> 00:01:44,800
You won't go that far wrong, it's just that while you're not at a state of good mindfulness

17
00:01:44,800 --> 00:01:50,200
you're going to make lots of mistakes, right?

18
00:01:50,200 --> 00:01:56,000
Because you're not yet there, you're potentially going to kill and steal and lie and cheat

19
00:01:56,000 --> 00:02:05,160
and even more likely, engage in things that are going to lead you down the wrong path.

20
00:02:05,160 --> 00:02:09,560
You're going to misinterpret mindfulness, you're going to not realize that you're not

21
00:02:09,560 --> 00:02:12,920
actually being mindful, think that you're being mindful and you're actually forcing,

22
00:02:12,920 --> 00:02:19,920
you're actually being partial and training the mind in a certain way and cultivating

23
00:02:19,920 --> 00:02:28,320
some kara's in the mind, artificial constructs, artificial habits, doing all sorts of unwholesome

24
00:02:28,320 --> 00:02:32,520
things because you're not really mindful, you don't really know how to be mindful.

25
00:02:32,520 --> 00:02:40,640
You can only truly be mindful once you've cultivated, once you understand, once you've

26
00:02:40,640 --> 00:02:45,200
practiced and become proficient in it.

27
00:02:45,200 --> 00:02:51,520
So until you get there, the question is how are you going to get there on your own?

28
00:02:51,520 --> 00:02:57,120
If you don't have a teacher, really the only way to do it is to read a lot and to make

29
00:02:57,120 --> 00:03:06,560
sure that you're not falling into any of the many pitfalls that are discussed in the

30
00:03:06,560 --> 00:03:12,240
pitika. But if you have a teacher on the other hand, you just do your mindfulness and they

31
00:03:12,240 --> 00:03:16,640
will remind you, they'll tell you, okay, you have to keep these rules, so you'll keep

32
00:03:16,640 --> 00:03:17,640
those rules.

33
00:03:17,640 --> 00:03:20,880
They'll tell you, no, you have to stop this, you have to stop that or you're being mindful

34
00:03:20,880 --> 00:03:21,880
of this.

35
00:03:21,880 --> 00:03:25,320
That's not really being mindful and so on and so on and then they'll be able to catch

36
00:03:25,320 --> 00:03:29,840
you and bring you back to being mindful, but absolutely mindfulness is the key.

37
00:03:29,840 --> 00:03:35,120
Mindfulness is kind of like the thread through the through necklace.

38
00:03:35,120 --> 00:03:40,720
So if the necklace is, the brief one is just morality, concentration, and wisdom, mindfulness

39
00:03:40,720 --> 00:03:42,240
is the thread that goes through the mall.

40
00:03:42,240 --> 00:03:48,880
It starts in the morality, so true morality comes through mindfulness.

41
00:03:48,880 --> 00:03:53,920
Once you have morality that it cultivates concentration, your mind starts to get focused,

42
00:03:53,920 --> 00:03:59,680
but that comes through even more mindfulness and through being mindful in a concentrated

43
00:03:59,680 --> 00:04:05,040
state, leads to wisdom, but mindfulness is what you use from beginning to end.

44
00:04:05,040 --> 00:04:08,160
Mostly the most important one.

45
00:04:08,160 --> 00:04:16,760
All the rest is just guidance to keep you on track when you fall off, when you stop really

46
00:04:16,760 --> 00:04:35,880
being mindful.

