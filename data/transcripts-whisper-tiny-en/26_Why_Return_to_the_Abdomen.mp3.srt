1
00:00:00,000 --> 00:00:03,440
Why return to the abdomen?

2
00:00:03,440 --> 00:00:11,200
Why should one return to the abdomen rather than moving from one object to another as they arise?

3
00:00:11,200 --> 00:00:17,200
It is not intrinsically wrong to note one object after another randomly.

4
00:00:17,200 --> 00:00:22,560
In practice, however, this tends to lead to a sense of anticipation.

5
00:00:22,560 --> 00:00:28,720
Waiting for the next object to arise is detrimental to objective awareness.

6
00:00:28,720 --> 00:00:35,760
A meditator is therefore instructed to note one object until it disappears,

7
00:00:35,760 --> 00:00:41,760
then return to the abdomen until another object takes one's attention.

8
00:00:41,760 --> 00:00:48,000
As the movements of the abdomen are reliably present, they provide a good base to observe

9
00:00:48,000 --> 00:01:17,840
the present, reality, at all times.

