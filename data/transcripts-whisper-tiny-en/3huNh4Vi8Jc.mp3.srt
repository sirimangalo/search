1
00:00:00,000 --> 00:00:06,000
How do I increase Vria? How do I increase Vria from the five strings?

2
00:00:06,000 --> 00:00:16,000
The goal or the focus should not be to develop Vria effort, energy.

3
00:00:16,000 --> 00:00:24,000
The focus should be to develop mindfulness because mindfulness is the balancing faculty.

4
00:00:24,000 --> 00:00:28,000
So you shouldn't ever worry about any one of the other four.

5
00:00:28,000 --> 00:00:38,000
To develop mindfulness, the five sadha confidence, effort, concentration, and wisdom will become balanced.

6
00:00:38,000 --> 00:00:48,000
Morality, concentration, wisdom will be created and the eightfold of the path to be created and enlightenment will arise based on mindfulness.

7
00:00:48,000 --> 00:00:57,000
Vria should be balanced with some ad, some ad, which is concentration.

8
00:00:57,000 --> 00:01:00,000
If you have too much concentration, you'll become drowsy.

9
00:01:00,000 --> 00:01:04,000
If that's the case, then you should focus on the drowsiness and be mindful of it.

10
00:01:04,000 --> 00:01:25,000
You're mindful of it, you'll clear up the drowsiness and you'll find that your Vria is increased.

