Good evening, we're continuing on with our study of the Dhamapada.
Today we look at verse 204, which reads as follows.
Arogya paramalabha sent to Thi paramandhanam.
We sasa paramaya nyati nibanam paramansukha, which means
Arogya, freedom from sickness, paramalabha is the greatest gain.
Santuti contentment is the highest wealth.
We sasa paramaya nyati, which sasa means something like trust or confidence,
is the greatest relative.
And nibana is the highest happiness.
So this was taught in regards to King Paseenidhi, the course of the kingdom.
The story goes that he was overweight.
Overweight is perhaps a problematic term because it requires a judgment to say that something is underweight or overweight.
But in a conventional sense one might say he was and he said he was because he complained about it.
But more than that he ate too much, regardless of his weight, he ate too much.
And why can we say it was too much without saying it's a judgment?
Because he complained about it being too much.
Well, he didn't say actually, he came to the Buddha, he ate, they say he would eat.
What is it?
A bushel, a bushel of rice.
I don't know how big bushels were with the actual measurement ones, but he had a lot of rice and curries and so on in proportion.
Too much, he ate too much.
And why it was too much?
Because after he was done eating he would feel very tired.
And so one day he ate and then went to the monastery and he couldn't even sit down because he knew if he sat down he'd fall asleep.
So the Buddha was teaching and he started walking back and forth.
And then what he really wanted to do was lie down and fall asleep, but he knew he couldn't.
So he laid out, he sat down and tried his best to stay awake.
And the Buddha looked at him and said, great king, are you not well?
And he said, no, it's not that, I hate too much.
I hate so much, I'm tired now.
I can't stay awake and he said, I don't know what to do.
And the Buddha said, oh, and he gave him a verse to recite.
And it's actually a Dhammapada verse, 324.
A way is a way, we'll get to it eventually.
But it basically said, it was talking about the disadvantages of indolence, of laziness, of overeating.
How such a person goes to suffering.
And he had him, remember this verse, but Pazainadi was actually quite...
Well, he was tired at the time, but we think perhaps he was a little bit dull as well.
And he said he couldn't remember it.
Probably it was more because he was tired, but anyway, the Buddha said to his nephew.
He called his nephew, the King Pazainadi's nephew.
He taught him the verse and he said, when Pazainadi is eating, just before he finished his eating,
you recite him this verse and he'll stop eating.
And you can keep cutting away at his food that way.
And sure enough, it's a short story.
Sure enough, they went away and his nephew Sudasana would recite this verse to Pazainadi
and Pazainadi came back to the Buddha and he remarked upon this.
He said, it's incredible, Venerable, sir.
I'm a new man.
I'm no longer drowsy or tired, but I'm also no longer cranky because that verse has sort of taught me
any contentment in the mind.
And so he talked about all the good things that it brought that he was kinder.
And when he heard this verse, it made him want to give away rather than take for himself.
And so he gave away lots of charity.
They say, when he heard this verse, I guess he would give to Sudasana.
I don't know. It says he gave out thousands of gold coins.
And he talked about how he was no longer fighting with his relatives.
His nephew, he was now very much indebted and grateful to his nephew.
He just talked about generally.
And most importantly, not eating so much, he was thinner.
And he could go on hunts again with his head.
It's a little bit eye opening to hear him tell the Buddha that he could hunt again.
But I don't know exactly what that means because it was something about chasing horses.
I don't think it was just about killing.
And the Buddha said, yes, great king, it's true that these are great things that you've gained.
And then he said, this not having a healthy body, he said, it's a great gain.
Great gain.
And being content with less is a great wealth.
And having trustworthy relatives is a great thing, great relatives to have having this nephew, for example.
And then he said, bat nibana is the highest happiness.
And then he taught this verse basically.
So there's a mixture here of conventional and ultimate sort of shallow and deep teachings.
And it's not to be little shallow teachings.
We should practically, conventionally, we should have what we might refer to as shallow teachings.
Meaning they just meaning that they don't, in and of themselves, bring about enlightenment.
They're not on the level of insight that's going to lead us to be free from suffering.
But they are practically applicable and helpful, supportive of our practice.
And the first one that comes from, of course, the story, this idea of overeating.
Because it's something that you find in meditation.
It's a concern of people who want to come and meditate that they're told they're only going to be eating in the morning.
And that's scary, worrisome.
But then you come and practice and you find that it's actually the easiest role to keep.
But in fact, overeating becomes a bit of an issue because you find after you've eaten your drowsy.
And it's an issue without such strict rules.
It's an issue because I've been in places where they didn't have such strict rules.
Where in the evening they would allow milk or even yogurt or whatever.
And you'll find that by drinking or eating, really having solid, something solid, even a solid is milk.
You find that it takes energy because your body is using energy to process it.
Of course, it gives you some kind of energy.
But if you're not using it, it just stores the energy as fat or whatever.
And so it is a concern for meditators.
I think less when you're staying here, but still something you have to be mindful of.
Of course, more importantly, I think is the mindfulness of eating.
You know, being taught as a verse every being reminded of this verse every day.
It was not just about remembering.
It was not just about realizing the disadvantages of eating too much.
It's about being mindful and realizing when you've eaten enough.
It's an important part of, it's a practical part of meditation that it teaches you when it's enough.
Really in all things.
But food is a really obvious example.
You find it's very hard to overeat when you're mindful because you're aware, you're present and objectively observing.
And you can feel and know when you've had enough.
So deep and more than that becomes more difficult.
As far as the verse goes, though, we can each of the four parts has an important lesson for us.
And so we'll take them apart.
That's really where the core of the lesson comes from for our purposes.
The first part, Rogia, Rogia meaning sickness, illness, disease.
In Buddhism, there are four types of disease.
And so we have disease in a conventional sense and disease in an ultimate sense.
But the categorization is that there are four.
The first type of sickness comes from the environment.
You could say things like the other weather, but airborne bacteria and so on.
Viruses, heat and cold can affect us.
Radiation, even the sun.
And the second is from food, where you ingest food and drink and make you sick.
The third is kamma, which means deeds, but here it is specifically referring to bad things you've done in the past.
Past lives can make you sick.
Even probably this is more referring to past life karma that has made you born with an illness or disability or so on.
And finally sicknesses that come from the mind.
The first two are physical.
The first two are things where they don't have so much control over.
But practically speaking, we can find the lesson in all of these.
It is practically speaking important to remember that our environment impacts us.
And being mindful of what we engage in, it's important to be mindful about food.
It is important to have a healthy body.
Of course, it's important equally not to obsess over the body.
But you can put some weight on these things and say,
yes, I want to be healthy, of course, because a healthy body is useful for you.
A much more important from a Buddhist perspective is the deeper sort of ultimate level of sickness,
which comes from the mind.
So karma, of course, comes from the mind that's in past lives.
We've done bad things.
It's a good lesson to us.
And a helpful reminder to us of the dangers of doing bad deeds.
That it corrupts your mind, yes, but it also can have disastrous consequences on your body
and your future life and future existence.
People who are born ugly with disfigured features or said to mean an evil in past lives.
Who have short lives, who are sick, even though they might live a long time
or are sick their whole lives.
It's not shameful.
It's not something that you would say, oh, that's a terrible person because of these sicknesses or so on.
It's something as a lesson for us that we can take as a lesson.
It helps us to understand and have a more just understanding of the world.
This isn't because God decided that I should be sick or it isn't chance.
It isn't just bad luck on my part.
That there's some order to the universe.
And most important, it should scare the heck out of us in regards to doing bad deeds in the future.
I think for our purposes, though, the fourth one is where we find real wisdom and deep insight
and real application in our practice, the sickness that comes from the mind.
And so practically on a conventional shallow sort of level you could talk about
the ways that the body suffers from the mind, and this is certainly true.
Even just being angry, just generally being angry can have an effect on the body.
Greedy lustful states have an effect on the body.
Delusion has an effect on the body as well.
The man who's arrogant or all puffed up can have great effect on the body.
There's even some, and I'm not sure how credible it is, but there's some reference to the idea that the fluids in the body relate to
the three defilements.
So blood is related to anger.
I think flam is related to delusion.
And they can't remember which is related to what the third one would be.
Pushed maybe, I don't know.
So there are stories of people who got so angry that they vomited blood.
They've adopted, I think, as one, and stories of people getting nosebleeds because of anger and so on.
And certainly when you meditate you off, some people find that their nose is running a lot,
or they have a lot of flam coming up, and there can be some relation there.
None of this, I think, is very deep.
It's a bit interesting as Buddhists, but much more interesting is not talking about our bodies too much.
Not being too concerned, worried, or obsessed with the health of the body.
Because, of course, the health of the mind is much more important.
There's a much more powerful impact on our lives, on our happiness.
And so the ultimate level of sickness, which is related to gain.
Because if you say it's great, gain to be physically well, that's true.
It's the best physical gain, I think, to be physically well.
And you could say, even your rich and powerful, but if you're sick, it doesn't mean anything.
It's a very important teaching.
But it's not so deep as talking about mental health, because you can be physically well,
as physically healthy as well. But if your mind is corrupt, you'll never be happy either.
You can have everything. You can be healthy and strong and fit in good shape, as they say.
And still be very unhappy because of your mind.
And so really that's what we're about here.
When they talk in modern parlay, in modern talk, they talk about mental illness.
And I think it's important to point out how Buddhism says things a little bit differently.
Because making something categorically a mental illness is very dangerous.
Even if doctors don't mean to, when you call something an illness, you say,
yes, this person has this illness and this person doesn't.
You both trivialize the problems that this person might have.
And you reify the problems that this person has.
And by reify, I mean, you turn it into a thing.
And then what are you going to do with this thing?
I've talked about this before. It's not something you can deal with.
What do you do with depression?
It wants it to thing. What do you do with it?
In Buddhism, we talk about experiences, habits.
And if you work in terms of that, you can change.
You can alter these things, but you can't.
If someone has clinical depression, it's not like a possession that you have to carry on your back or in your pocket.
Everyone has greed, anger, and delusion to varying degrees.
For some people, it's become so intense that they get in a loop where they have no clue about how to deal with it, how to overcome it.
But it doesn't make it any different categorically or functionally different from anybody else's mentalist.
Now, not to trivialize that, of course.
For some people, it's so radically different from an ordinary person.
But even the word ordinary is reifying some kind of category.
It's not a cat, it's not categorical.
All of us have mental illness, and people who have been recognized as clinically having mental illnesses.
Have the same problems and the same resources and the same path to follow in terms of ameliorating their situation.
Bettering themselves.
So this is what our meditation, it's a really good way of describing what our meditation is about.
The insight meditation is about learning to see things clearly.
And it's not so much, and I've said this before.
It's not so much about what you see when we talk about seeing clearly.
But what is it I'm going to see clearly?
Yes, we can talk about that.
But that's not so much important. The important is the state of seeing clearly.
So we talk about understanding and wisdom, and people say,
what is it the thing that we're going to learn?
What are we going to understand expecting some kind of theory?
And that's really not the point that there's something there.
But much more important and much more true to Buddhism is the state of understanding.
The perspective where you're no longer trying to control and fix and claim your understanding.
It's like if someone comes to you with a problem or someone has done something,
and you have choices, you can condemn them.
You can side with them or you can understand. You can be understanding.
When we deal with people, we talk about being understanding.
And that's I think an important aspect of how we look at wisdom in Buddhism.
That's why mindfulness is so important and so much our practice.
I would say, don't think about wisdom as something that you're going to get
or suddenly you'll realize some theory or profound idea.
Wisdom, my teacher would say, when you know that the stomach is rising, that's wisdom.
It very much is. If we were in that state where we knew the stomach was rising,
that was the epitome, that was how we looked at everything.
We would be enlightened.
The problem is that we don't.
We have so much delusion and ignorance that we react to things.
We judge things and we're ill, we're sick.
So when we free ourselves from that, that's the greatest game.
That's the first one, we'll move along.
The second one is another very good way of talking about the path.
So there's this sort of shallow level, sent to deep and among Tannan.
On a conventional level, it's very important, a very Buddhist to talk about contentment.
Contentment is the greatest wealth.
We talk a lot about consumerism and greed and how you'll never always have what you want
and seeking out things and even experiences.
It's just going to lead to more discontent.
You'll get what you want and you'll want it more and you'll set yourself up for disappointment.
Greater and greater disappointment when you can't get what you want.
So of all the wealth that we could ever have, contentment is the highest.
And we have to differentiate even here, I think, because the word contentment is used and abused.
Many people will call themselves content and why are they content because they've got everything they want.
And that's possible for some time, right?
It's possible to be very content when you're rich and have a stable job or social life or family or...
It's very, very possible when things are on the up.
But that's not considered contentment in Buddhism.
There's a deeper level of contentment, not the deepest level, I think.
But, well, there's a deeper level.
And the deeper level, of course, is to be content with anything.
Sun to tea. It's a good word. Two tea means...
It could mean something like joy or excitement or so on.
It really means something like happiness. Let's say joy.
And Sun means here it implies the idea of what you already have.
Sun means what's there, kind of. Sun to tea means happy with one's own possession.
But it doesn't mean happy with, as I said, certain things.
It means happiness with things as they are.
And the only way to really have that is when you're happy with everything or with anything.
Sun to tea, meaning unconnected to conditions, unconnected to environment.
And that's much more difficult, of course.
It's easy to be easier to be content when you have things.
It's not that easy. Some people are distal discontent.
They can have everything and be...
In fact, most people are.
In fact, I would say it's quite fake to think of contentment as...
I have all these things and I'm content. Why should I do anything?
You could argue well, you might lose everything.
But even when you have everything you could possibly want, there's always craving for more.
And the only way is to jump from one thing to another because that's how the brain works.
It means new stimuli. It means greater stimuli.
And so you have to vary your experience.
If you try to always be happy with things, like get the things that make you happy, you find it doesn't work.
As you grow older, this is why people are tempering their desires because they realize
or if they don't realize they become addicts and go that way and can get terrible.
But if they're temperate, otherwise it doesn't work.
That's the brain won't make you happy. It's only capable of a certain amount of happiness before it needs more stimuli.
How the brain chemicals work.
But much more difficult.
But much more difficult, but much more sustainable and attainable is the true contentment.
A place where you could really be content is when you're content with anything or nothing.
Content when you get what you want or need, content when you don't get what you want or even need.
And it's really not all or nothing, I suppose, but it's a direction.
And the direction we take in meditation, we don't come here and say, okay, day one, I'm going to let go of everything.
But as you practice, you let go of more and more, and you become more and more content.
You need less and less to give you happiness.
Your happiness becomes less and less dependent on experience.
You see impermanence.
Everything is changing, so you become less expecting that things should be this way or that way.
You see suffering, so you become less clingy, because you see when you cling, you just dress yourself up.
You become less controlling, you see things are not self.
You control them less, because you see controlling leads to suffering.
And as a result, you become better able to be at peace, touched by the vicissitudes of life.
Your mind is unshaken, the Buddha said.
That's unto tea.
Santo tea contentment is a very good way of describing Buddhism, if described correctly.
I understood in this way.
The third one, we sassaparamaniati.
We sassa is a bit difficult to translate, I think, trust confidence,
but confidence in the specific sense of taking someone in confidence,
having confidence in someone or having their confidence.
It's a specific use of the word in English.
Trust, in Thai, they actually say something more like familiarity,
but it all points to a connection with someone that is based on trust,
based on familiarity.
And the Buddha said this is the greatest relative,
and someone you can trust, someone you can depend upon,
someone who you have this kind of relationship with.
And the commentary makes it clear that even if your mother,
even your mother and father, if they don't have this quality,
you can't trust them, depend upon them.
If you don't have this kind of close relationship where you depend on each other,
then even your mother and father aren't really relatives.
They're not very good relatives.
And he said, but even if someone is not related to you,
the commentary says, even if they're not related to you,
they have these qualities, you can consider them a true relative.
Now, this of all of them, I think, is a fairly,
what we might call shallow teaching.
It's useful, it's practical.
It's good to remember, but the deeper teaching,
there is a deeper teaching here, and that's related to this idea
that someone could be a relative without being a relative,
which in itself isn't a deep teaching,
but it relates and it points to the difference between convention and reality.
Because relatives, of course, are a convention.
If you think about it, we're all probably related by blood generation after generation.
There weren't two original human beings.
However, it happened.
But more importantly, what does it mean to be related at all?
If you had a mother and father who you never met somehow,
they died when you were born, or even before you were born,
and your parents died somehow as you were being born kind of thing?
There's one of the monks was like this,
his name, I can't remember his name, it'll come to me,
but it was one monk who his mother died while he was still in her stomach,
and they were burning the body.
Because he had such good karma, he didn't die.
They burned the body, and the guy was using his stick to turn the body.
They have to turn it because it doesn't burn unless you keep poking it.
He scraped the eyebrow of this baby,
and he got a name based on that.
There's an old story.
And he found it here, this baby crying, and he pulled it out.
And I think the king raised him or something.
I think I might have been Kumarakas, actually.
Kumaram, because he was a prince.
In this case, means prince, because of the king raised him.
No, that wasn't Kumarakas, but that's another story.
Someone knows, I'm sure.
Is that Bakul? I know.
One of the monks, one of the famous monks.
But it can happen if you have no parents,
or if you were separated from your parents at birth, I guess,
is a better story than what would it mean to say you were related to them?
You could argue there's karma involved,
but more important and more useful for us is people who
can depend upon people who are close to.
And Buddhism we talk about, Buddhist cultures,
they'll talk about something called
Dhammanyati.
Dhammanyati means a relative in the Dhamma.
Entirely, transport is a nyati, Dhamma.
But the deeper teaching there is that this relates to everything,
that we have many conventions about things we think are good and right,
and about our duties and relationships.
Relationships, not just with people, but with places and things,
like being Canadian, for example.
Even me being a monk is just a convention.
Being a man, being a woman.
Many times Mara would come to the biquinis and tell them,
what do you think you're doing?
You're just a woman, and how can you practice?
Because of course, women were not seen very highly at that time.
Time, I think.
And they would say, there's no woman here.
There's no woman, there's no man.
What room is there for any of that?
And so this is a good example.
It's not exactly I think with the Buddha was teaching,
but it does make us think a deeper sense about sort of pulling ourselves
out of the conventional, even human, being human and saying,
10 fingers, 10 toes and two eyes and so on.
What it means to be human, it's all just convention.
It's just circumstance.
Our life, the birth, all the age, death, not to mention things like getting a job
or going to school, all of these conventions,
having a relative, a mother and a father.
I mean, there are not things we should discard out of hand,
but they have to be put in their place.
And we have to understand that on a deeper level.
There's this old, this new age saying, we are stardust.
And there is some truth, of course, to that.
It puts things in perspective to realize that we are stardust.
All of our atoms originally came from stars.
Anyway, I think that that's a good interesting part of that teaching.
Useful for us to remember.
Much of insight meditation, mindfulness is about taking us out of our conventional
ideas that are all contrived so that we can see more clearly what really is.
And the fourth teaching, if you read the verse, it sounds like he's just listing off for things.
But if you read the story, the story does point something out important that these others
three may be the case.
It may be true that all of these are true.
It's true that these are all the case.
But none of them are the highest happiness, being free from illness,
unless you take it on a deeper level, having contentment again,
unless you take it on a very deep level.
Even if you do, these states are still mundane.
And that the highest happiness is still nibana.
The ultimate happiness.
And this again gives us direction.
It's not that we have to discard everything and only think of nibana.
Of course, we really shouldn't think much about nibana at all.
But it does give us a direction.
Because nibana means cessation.
It means freedom from suffering.
It means letting go.
It has all these connotations and characteristics that give us direction
and remind us of where we're headed.
That this isn't just something of if I practice meditation or be free from sickness
because my body will feel healthy or so on.
I'll be more content and I'll be able to live a more content life.
It's much more profound in that.
And much more direct and simple.
It's just about peace.
It's about happiness.
It really is about happiness.
It's about the fact that happiness can't come from clinging to things.
Happiness comes from letting go.
It can't come from things that we get, things that come to us.
It can't come from things.
Happiness comes from the wisdom to see nothing is worth clinging to.
Happiness comes from letting go.
The letting go that comes from wisdom.
I've talked about nibana before, but in this verse there's a particular reminder.
Not just putting nibana with everything else.
All these other things may be good.
None of them get to the real point.
That nibana is the highest happiness.
These may be the highest other things.
And they're good and important.
But ultimately nothing is better.
Nothing is more direct.
Nothing can be said to be greater happiness.
Then nibana or letting go.
Being free from suffering.
So that's the Dhammapana for tonight.
Thank you all for listening.
