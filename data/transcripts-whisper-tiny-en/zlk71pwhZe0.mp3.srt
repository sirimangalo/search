1
00:00:00,000 --> 00:00:14,520
We're not casting live November 11th, so it's an especially Buddhist day-to-day, or is that

2
00:00:14,520 --> 00:00:15,520
only in Canada?

3
00:00:15,520 --> 00:00:18,080
Is it a special day in America as well?

4
00:00:18,080 --> 00:00:20,320
Today's Veterans Day in America.

5
00:00:20,320 --> 00:00:22,240
Oh, you call it Veterans Day.

6
00:00:22,240 --> 00:00:25,560
Well, we have a better name for it, a very Buddhist name.

7
00:00:25,560 --> 00:00:28,240
Today is Remembrance Day.

8
00:00:28,240 --> 00:00:44,840
I mean, it's to remember Veterans, but yeah, probably shouldn't steal it, but it's in

9
00:00:44,840 --> 00:00:45,840
Remembrance.

10
00:00:45,840 --> 00:00:46,840
It's a good word.

11
00:00:46,840 --> 00:00:53,680
It's good to remember not to forget, and on Buddhism it has a bit of a different connotation.

12
00:00:53,680 --> 00:01:18,040
Okay, we're doing them up at the, we'll have to turn some stuff on, test, mic is good.

13
00:01:18,040 --> 00:01:27,280
The old mic, one that failed on me before, because the new one's failing now, so tomorrow

14
00:01:27,280 --> 00:01:28,760
we're going to have a different layout.

15
00:01:28,760 --> 00:01:34,800
I think we're going to move into the outer room, which has a nicer, nicer, robin's idea

16
00:01:34,800 --> 00:01:38,400
that we move into the outer room.

17
00:01:38,400 --> 00:02:04,480
The outer room has a nicer atmosphere, so we don't have to use this fake white.

18
00:02:04,480 --> 00:02:09,480
Okay, ready.

19
00:02:09,480 --> 00:02:16,160
Hello, everyone, and welcome back to our study of the Damapada.

20
00:02:16,160 --> 00:02:22,800
Today we continue on with first number 108, which reads as follows.

21
00:02:22,800 --> 00:02:48,720
It's actually a bit difficult, because the grammar doesn't really work in English.

22
00:02:48,720 --> 00:02:59,840
So whatever, whatever sacrifice or worship there is in the world.

23
00:02:59,840 --> 00:03:08,520
If for 100 years, so I'm going to put one desire in Bungya, desire in merit, should perform

24
00:03:08,520 --> 00:03:15,000
such a sacrifice or such a worship, and yes, it's actually a very, very similar verse

25
00:03:15,000 --> 00:03:19,800
and story to the last two.

26
00:03:19,800 --> 00:03:26,760
Subampitang tatubagameti doesn't come to a quarter of the value.

27
00:03:26,760 --> 00:03:30,320
All of that doesn't come to a quarter of the value.

28
00:03:30,320 --> 00:03:35,800
The hundred years of sacrifice or worship that one pays.

29
00:03:35,800 --> 00:03:46,680
Ambiwadana ojugate suseyo for the greater, in comparison to the greater, Ambiwadana

30
00:03:46,680 --> 00:03:55,480
or respect or reverence, that one pays to, and today our word is ujugata, in regards

31
00:03:55,480 --> 00:04:01,280
to those who are ujugata, gone to or become straight.

32
00:04:01,280 --> 00:04:10,920
Those who have gained rectitude, moral or ethical or mental straightness, uprightness,

33
00:04:10,920 --> 00:04:15,840
who's who are upright.

34
00:04:15,840 --> 00:04:25,760
So same story, we have Saripuddha, and it's becoming apparent that he was really instrumental

35
00:04:25,760 --> 00:04:33,240
in helping a lot of his relatives and now his friends become enlightened.

36
00:04:33,240 --> 00:04:43,440
So now we have Saripuddha's friend, Sahaya Kambramana, in Brahmin, who was a friend of Saripuddha,

37
00:04:43,440 --> 00:04:45,840
friend of Saripuddha's from before.

38
00:04:45,840 --> 00:04:50,200
And so Saripuddha went to him and asked him, do you do any good needs?

39
00:04:50,200 --> 00:04:57,240
And he says, oh yes, what do you do? Oh, I offer sacrificial slaughter.

40
00:04:57,240 --> 00:05:01,000
And of course Saripuddha doesn't see that as being a wholesome demon.

41
00:05:01,000 --> 00:05:12,280
This is really about the crux of these three stories, is the idea that our concept of good

42
00:05:12,280 --> 00:05:22,280
deeds is often quite misled. And this even occurs in Buddhism, you'll see Buddhists performing

43
00:05:22,280 --> 00:05:32,360
deeds that they think are good deeds when in fact they are, perhaps useless, you know.

44
00:05:32,360 --> 00:05:43,040
So the case of, I've told this story before, but once we had a Katina ceremony where I was

45
00:05:43,040 --> 00:05:50,040
staying, and all the villagers came together to do the Katina ceremony, and so what they

46
00:05:50,040 --> 00:05:56,520
did is they had this very important, I was in the rural area, so it was there where people

47
00:05:56,520 --> 00:05:59,120
didn't have a lot of money.

48
00:05:59,120 --> 00:06:06,080
And there was one man who had built a house in the area from Bangkok, and he was quite rich.

49
00:06:06,080 --> 00:06:11,240
And so he got his rich friends from Bangkok to come up, and they put together a bunch of

50
00:06:11,240 --> 00:06:17,000
money, and they put the money up on a tree, and they brought this money tree to the monastery,

51
00:06:17,000 --> 00:06:22,000
and they wanted to do a Katina ceremony. Well, it turns out half of them were drunk, they

52
00:06:22,000 --> 00:06:31,440
had been drinking, and the funniest part was, and what sort of made it a bit ridiculous,

53
00:06:31,440 --> 00:06:37,000
was that they didn't have a single robe with watch to do the Katina. So they brought this

54
00:06:37,000 --> 00:06:40,520
money tree, thinking, well that's Katina, Katina has a money tree, because that's really

55
00:06:40,520 --> 00:06:45,520
when it's become, it's all about money trees. Trees that have, or literally the made

56
00:06:45,520 --> 00:06:54,240
of money, the leaves are folded up, bits of money, or just money sticking up on sticks.

57
00:06:54,240 --> 00:07:02,520
And so I said, okay, already the Katina, what do we do? And I said, well where's the robe?

58
00:07:02,520 --> 00:07:09,360
And they didn't have a robe. So I said, well, can we borrow one of yours? And I said, okay,

59
00:07:09,360 --> 00:07:16,720
I took off, I actually took off, anyway, and I want to talk about it, I'm sure it wasn't, yeah,

60
00:07:16,720 --> 00:07:25,120
you know, point being, not really what I would call wholesome engagement, especially considering

61
00:07:25,120 --> 00:07:30,080
the alcohol involved. And the fact that after it was all over, they took the money back,

62
00:07:30,080 --> 00:07:35,920
and some of the gifts that were supposed to go to the monastery, they took back for the village,

63
00:07:35,920 --> 00:07:46,960
and it was really, it was a bit of a system that they had set up. And this happens in Buddhist

64
00:07:46,960 --> 00:07:51,680
circles sometimes, where we lose sight of why are we doing this? I think what we're doing it,

65
00:07:51,680 --> 00:08:02,080
often they're doing it just because they're excited to have their village and their community grow.

66
00:08:02,080 --> 00:08:07,920
So the money goes to the community, I think, the stuff goes for the community.

67
00:08:12,560 --> 00:08:17,280
And they don't realize the great benefit to actually giving a gift. And in fact, they don't

68
00:08:17,280 --> 00:08:22,320
really, they're really interested in giving gifts. On the other hand, there was that very same

69
00:08:22,320 --> 00:08:31,680
one, same village ended up being quite awesome about giving arms. They had another funny aspect

70
00:08:31,680 --> 00:08:37,280
of being there. I was staying with an old monk, the two old monks. So this was later when I'm

71
00:08:37,280 --> 00:08:40,720
staying with a second old monk, but I was staying with a first old monk deeper in the forest.

72
00:08:42,720 --> 00:08:51,280
And he told me, when you go on, he said, well, you can go on arms around to the village,

73
00:08:51,280 --> 00:08:58,800
but you won't get anything. You might get some dry noodles or canned fish, but that's about it.

74
00:08:58,800 --> 00:09:03,760
It won't be enough to eat, but you can go. And it was three and a half kilometers walk along a dirt

75
00:09:03,760 --> 00:09:11,280
gravel road that was actually somewhat painful to walk. But I did walk three and a half kilometers,

76
00:09:11,280 --> 00:09:16,000
and often had to walk three and a half kilometers back, but not always. Sometimes someone

77
00:09:16,560 --> 00:09:23,440
going to the park, there was a national park near where the monastery was, would give us a ride back,

78
00:09:23,440 --> 00:09:34,960
or give me or eventually ask when I had fallen. But the great thing, once I went and they could see

79
00:09:34,960 --> 00:09:48,000
that I was interested in meditation and wasn't some crabby old monk. They were really keen on

80
00:09:48,000 --> 00:09:52,720
and eventually the whole village began to give. And they really got the sense of the greatness,

81
00:09:52,720 --> 00:09:56,800
people who I think had never really gotten into it. And there was one man who was a really

82
00:09:56,800 --> 00:10:01,280
devout Buddhist and very interested in the dhamma and very knowledgeable about the dhamma,

83
00:10:01,280 --> 00:10:05,520
and we used to talk about the dhamma. And he was really impressed. And he said, you know, this is

84
00:10:05,520 --> 00:10:11,280
a great thing that people who I've never seen these people give before are now into giving.

85
00:10:11,280 --> 00:10:16,400
So there was that. But that is really what it's about. It's the difference,

86
00:10:16,400 --> 00:10:23,200
this versus are about the difference between spiritual practices. We have this idea of spiritual

87
00:10:23,200 --> 00:10:28,560
practice in many Buddhist cultures of pain, respect to gods or angels, and they've started

88
00:10:28,560 --> 00:10:34,160
making up angels. They've taken taking them from Hindu myths like Ganesha, people worship this

89
00:10:36,080 --> 00:10:45,120
monkey. I know, sorry, the elephant, god, and they have all these myths about Ganesha and Hanuman

90
00:10:45,120 --> 00:10:51,520
in this same village. There was a woman who claimed to be an avatar. An avatar is a big thing

91
00:10:51,520 --> 00:11:02,080
in Thailand anyway. An avatar for Hanuman. And of course, Hanuman is this legend. It's a story,

92
00:11:02,080 --> 00:11:08,560
really. I mean, there's not even any ancient religious texts, I think, that have Hanuman

93
00:11:08,560 --> 00:11:15,040
in them. It's this tale of the Ramayana, which is a fairly modern tale. And yet in India as well,

94
00:11:15,040 --> 00:11:21,360
they worship Hanuman or they seem to. They have monkeys, monkeys statues in front of their fields

95
00:11:21,360 --> 00:11:30,000
to ward away evil demons and stuff. But yeah, these sort of things, not worth a quarter of the

96
00:11:30,000 --> 00:11:40,160
day. She says, not a not a fourth part, not chatumbagamity, chatumbagamity. They don't

97
00:11:40,160 --> 00:11:43,760
come to a quarter. They don't even come to a thousandth part as we were talking about in the

98
00:11:43,760 --> 00:11:51,440
early ones. Anyway, so he tells him this. It's a little bit different. He takes him. He says,

99
00:11:52,400 --> 00:11:57,520
he says, look, you have to come to to see the teacher. And he takes him to see the teacher.

100
00:11:57,520 --> 00:12:02,160
And this time he actually says, one tape, please tell this man the way to the brown

101
00:12:02,160 --> 00:12:07,760
man world. But the Buddha says the same thing. Buddha asks him what he does. And he says,

102
00:12:08,480 --> 00:12:15,680
you could do that for a year. And yet it would be not be worth the smallest piece of offering.

103
00:12:17,200 --> 00:12:19,680
And he says something also a bit different.

104
00:12:19,680 --> 00:12:40,240
So if you were to give, if you were instead to give to the great to the populace, if you're

105
00:12:40,240 --> 00:12:46,720
instead to give to poor people, for example, that would be, that was one thing would be a greater

106
00:12:46,720 --> 00:12:51,600
value. So he distinguishes there. And it's an interesting point because here the Buddha

107
00:12:51,600 --> 00:12:58,560
sort of seems to be advocating giving of arms. I'd have to, that's what the English translation says,

108
00:12:58,560 --> 00:13:06,800
and that looks like what the palli says. But it's something to do with giving gifts to

109
00:13:06,800 --> 00:13:11,760
worldly people. It's much better because he's slaughtering animals, which of course is actually

110
00:13:11,760 --> 00:13:21,680
unwholesome. But then he goes on to saying, persona jithina, mama savakana, one dantana,

111
00:13:23,120 --> 00:13:27,680
to pay respect to my disciples, what is this?

112
00:13:33,360 --> 00:13:39,520
Yeah, the kusilajita, the the wholesome mind that comes is far more powerful.

113
00:13:39,520 --> 00:13:46,800
I mean, I think he's quite understating the truth here because honestly sacrificing animals is

114
00:13:46,800 --> 00:13:53,920
not only a portion, it's not only a fraction as good. It's actually harmful. There's nothing

115
00:13:53,920 --> 00:14:00,560
good about offering slaughtered and slaughtering animals as an offering. So the Buddha has, I think,

116
00:14:00,560 --> 00:14:05,200
being a little bit, going a little bit easy on him because probably if he were to say,

117
00:14:05,200 --> 00:14:11,680
you're actually that's actually an unwholesome thing, then they wouldn't perhaps set the guy

118
00:14:11,680 --> 00:14:18,480
off and make him upset at the Buddha. So he couches it a bit nicer. He doesn't say how much less

119
00:14:18,480 --> 00:14:25,040
good it is than a quarter. But it's at least not, it's less than a quarter is good.

120
00:14:26,400 --> 00:14:32,000
It's actually harmful. And that's a true of many spiritual practices. So again, as I've

121
00:14:32,000 --> 00:14:38,880
talked about, we have to be aware of the true benefit of our spiritual practices. We can't just

122
00:14:38,880 --> 00:14:45,120
think I'm doing this and this is what people tell me to do. This is spiritual. Therefore,

123
00:14:45,120 --> 00:14:52,000
it's good. Spiritual practice can actually be harmful. This was a spiritual practice that obviously

124
00:14:52,000 --> 00:15:01,520
was offering killing animals in the name of God. Sometimes we take people who have the idea that

125
00:15:01,520 --> 00:15:06,560
taking drugs is spiritual. And I'm not going to go out and say that that's harmful, but

126
00:15:07,360 --> 00:15:13,200
my suspicion is that it has a great potential for harm if not being outright harmful. I mean,

127
00:15:13,200 --> 00:15:18,000
I think a lot of people would say that taking even psychedelic drugs and hoping that it somehow

128
00:15:18,000 --> 00:15:23,200
is beneficial as a spiritual practice. I think people, some people would say that that is outright

129
00:15:23,200 --> 00:15:28,240
harmful. I'm not sure that I'd go quite so far because there is an argument to be made that it

130
00:15:28,240 --> 00:15:34,800
opens up your mind to alternate states of reality. But I think there's a lot of delusion involved,

131
00:15:34,800 --> 00:15:41,120
like the idea that those states have some meaning or purpose or value when in fact they're all

132
00:15:41,120 --> 00:15:49,760
messed up with delusion and I've been there done that. And it's just a mess of our mind

133
00:15:49,760 --> 00:16:00,320
and what our mind can come up with when it's doped up and it's high. So these kind of things.

134
00:16:00,320 --> 00:16:07,440
And you compare that, for example, taking drugs as a spiritual practice to a Buddhist spiritual

135
00:16:07,440 --> 00:16:12,480
practice of giving charity or of even just holding your hands up and paying respect to someone

136
00:16:12,480 --> 00:16:19,600
who's worthy of respect. Who would think of that? It shows how far we, many people

137
00:16:19,600 --> 00:16:25,520
who are from true spiritual practice. Maybe people say, oh, taking drugs that's a spiritual practice

138
00:16:25,520 --> 00:16:34,240
or doing expansive rituals or offering copious sacrifices or this kind of thing.

139
00:16:35,600 --> 00:16:41,840
Doing very complex or very mystical things, maybe dancing. People say in the West, we become very

140
00:16:41,840 --> 00:16:47,680
hedonistic with our spirituality. So maybe people say group orgies and karmic sex and that kind of

141
00:16:47,680 --> 00:16:53,280
thing, these are spiritual. And contrast that to the Buddha's idea of what true spirituality is,

142
00:16:53,280 --> 00:16:59,680
holding your hands up and paying respect. That is far more spiritual. So such a mundane

143
00:16:59,680 --> 00:17:06,880
seeming thing, but far more spiritual and far more beneficial, even just for a moment and a hundred

144
00:17:06,880 --> 00:17:16,480
years of dancing, spiritual and dancing or spiritual music or spiritual sex, drugs, you know,

145
00:17:16,480 --> 00:17:17,920
all these things.

146
00:17:22,400 --> 00:17:28,480
So for someone looking for punya, which we all are, punya is goodness. Even if we're just set on

147
00:17:28,480 --> 00:17:36,480
meditation, it's important not to become self-centered, you know, in a sense of me, me, me,

148
00:17:36,480 --> 00:17:41,920
I want to become spiritual enlightened. Enlightenment is about letting go,

149
00:17:41,920 --> 00:17:47,840
it's a lot of self-sacrifice. When enlightened being would be able to give up anything,

150
00:17:47,840 --> 00:17:54,480
if someone asks them for something, they would have no sense of self, of helping themselves.

151
00:17:54,480 --> 00:18:00,480
You have no qualms there and they consider what is proper to do and they do it without any

152
00:18:00,480 --> 00:18:05,920
attachment, without any greed. And they're very respectful. They honor those who have helped

153
00:18:05,920 --> 00:18:10,080
them. They're grateful to those who have helped them. That kind of thing. And they're very down

154
00:18:10,080 --> 00:18:18,400
to earth. So noble spirituality is like this. Well, it's spiritual. Well, paying respect to those

155
00:18:18,400 --> 00:18:23,760
who are worthy of respect, giving gifts to those who are worthy of gifts, and mostly just being

156
00:18:23,760 --> 00:18:32,240
aware of being here, being present, being with mundane reality, understanding truth, true reality,

157
00:18:32,800 --> 00:18:38,480
and not being concerned with mysticism or altered states of existence or that kind of thing,

158
00:18:38,480 --> 00:18:43,280
astral travel, anyone who's obsessed with astral travel. I mean, it's not to say enlightened

159
00:18:43,280 --> 00:18:49,440
people can't practice it, but people who are striving for that are really missing the point.

160
00:18:51,280 --> 00:18:58,880
Mahasi Sayyada tells a story of a woman who he knew who was working very hard to try and

161
00:18:58,880 --> 00:19:06,000
see out of her ears. This was she was trying to gain the spiritual ability to see out of her

162
00:19:06,000 --> 00:19:13,280
ears. And he said, and this one, her eyes worked perfectly fine. So we get often on the wrong path,

163
00:19:14,240 --> 00:19:24,320
true spirituality is heart is often easy to mistake for ordinary reality. I would say that enlightened

164
00:19:24,320 --> 00:19:29,120
people are what we think we are. We all think of ourselves as living normally. We think of ourselves

165
00:19:29,120 --> 00:19:37,440
as normal, but we're not. Our defilements take us away from what we think we are. Take us away from

166
00:19:37,440 --> 00:19:44,480
mundane reality. So I've made much of this very simple verse, which is very much like the other two

167
00:19:44,480 --> 00:19:50,320
verses, and I promise the next one is different, still still along the same vein, but it's a totally

168
00:19:50,320 --> 00:19:58,480
different context and story, everything. And it's a very, actually one of them, probably the, if not,

169
00:19:58,480 --> 00:20:07,280
one of, one of the, if not the most famous verses in the double pad are coming up next, next time.

170
00:20:08,160 --> 00:20:16,080
And then after that, we have the story of Sankitya, which is a very nice story, and so on and so on.

171
00:20:16,080 --> 00:20:20,320
Not all the stories are long. Someone were very short, as you can see. Some of them are very long.

172
00:20:21,440 --> 00:20:27,040
I think the longest ones have already gone by, so we're not going to have great storytelling.

173
00:20:29,040 --> 00:20:35,840
Not all of it is going to be great stories, but always something new and some new message to be

174
00:20:35,840 --> 00:20:41,040
found in the Dhamapada. And of course, the verses themselves are things you can reflect upon.

175
00:20:41,040 --> 00:20:47,600
And what is important, this is the Sasa-waga, which is a thousand, so it's contrasting single things

176
00:20:47,600 --> 00:20:53,840
to thousands of things for the most part. A thousand of something useless, better is one thing

177
00:20:53,840 --> 00:21:00,640
that is useful. So that's the Dhamapada for tonight. Thank you all for tuning in.

178
00:21:00,640 --> 00:21:13,920
Keep practicing. Okay, hopefully that turned out.

179
00:21:13,920 --> 00:21:27,840
Now I have to turn some things off.

180
00:21:31,680 --> 00:21:35,520
It's like I had it on mute. It just asked me if I wanted to turn mute off.

181
00:21:36,240 --> 00:21:39,600
If I had it on mute the whole time, would it be funny?

182
00:21:39,600 --> 00:21:44,720
No, the comments from the chat panel are that the audio is great, but the camera did go out of

183
00:21:44,720 --> 00:21:50,160
focus a little bit on the YouTube. But that audio is different from this audio. The audio

184
00:21:50,160 --> 00:22:14,400
is amazing. I don't think it was like, it couldn't have been on mute, but that would be funny as well.

185
00:22:21,120 --> 00:22:29,680
You know, I could do, for the Dhamapada portion, I don't really have to record the video.

186
00:22:29,680 --> 00:22:35,120
We could just do the audio stream for that part and start doing the live hangout.

187
00:22:36,800 --> 00:22:44,000
After I do the Dhamapada, because I mean my record, then otherwise I have it recorded on YouTube twice.

188
00:22:44,000 --> 00:22:49,280
Maybe I still have the videos be entirely different, right?

189
00:22:52,320 --> 00:22:56,880
Could you say that again, monthly or to do the Dhamapada and then do the hangout afterwards?

190
00:22:57,760 --> 00:23:02,480
Yeah, just do the audio stream during the Dhamapada, so people could listen.

191
00:23:03,280 --> 00:23:04,160
Hopefully I want it.

192
00:23:04,160 --> 00:23:13,040
And if they want to watch, they can wait. And then we could put the, you know, if you could get

193
00:23:14,480 --> 00:23:17,920
do you see the link in the bottom right corner? It says links?

194
00:23:18,960 --> 00:23:22,480
No, that's only on the page of the person who creates the hangout.

195
00:23:22,720 --> 00:23:30,560
Okay, so I can put the link in the, when people know to go there,

196
00:23:30,560 --> 00:23:33,280
maybe we'll think about doing that in the future.

197
00:23:37,360 --> 00:23:38,800
Are you, am I echoing you?

198
00:23:40,000 --> 00:23:40,800
I don't think so.

199
00:23:43,680 --> 00:23:50,000
Okay, then I don't need you loud, so I can even turn you down,

200
00:23:51,360 --> 00:23:53,840
because that's not coming anymore.

201
00:23:53,840 --> 00:23:59,440
So, camera, camera's okay, a little bit out of focus.

202
00:24:00,560 --> 00:24:03,600
That's a little blurry. It kind of goes in and out.

203
00:24:07,120 --> 00:24:09,760
Okay, questions? Do we have questions?

204
00:24:09,760 --> 00:24:11,040
We have questions, yes.

205
00:24:12,320 --> 00:24:17,120
Why do monks chant? I ask because I have recently been shown a breathing technique

206
00:24:17,120 --> 00:24:22,400
where you breathe in through your nose and then exhale through your mouth, making an a sound.

207
00:24:22,400 --> 00:24:27,040
This technique has been scientifically proven to benefit the nervous system.

208
00:24:27,680 --> 00:24:31,200
How exactly I don't know, but it got me thinking about monks chanting.

209
00:24:31,840 --> 00:24:35,600
Has the West finally caught up with what the East has known for centuries?

210
00:24:36,160 --> 00:24:38,640
Can you repeat that? It's been scientifically wild.

211
00:24:39,360 --> 00:24:42,560
Scientifically proven to benefit the nervous system.

212
00:24:42,560 --> 00:24:48,240
Okay, right away, I know there's a problem. Science never ever ever proves anything.

213
00:24:48,240 --> 00:24:52,640
Science can't prove anything. It can only disprove.

214
00:24:53,440 --> 00:24:55,600
Why I say it because you have to be careful.

215
00:24:56,800 --> 00:24:59,280
I don't think this isn't quite your question, but

216
00:25:00,720 --> 00:25:06,160
be very careful when you believe claims that someone says something is scientifically proven.

217
00:25:06,160 --> 00:25:10,560
You can provide evidence in favor of something, but you can't have to prove it.

218
00:25:10,560 --> 00:25:21,280
So you can show what appears to be a link.

219
00:25:22,480 --> 00:25:29,440
And usually with science, usually science involves taking lots of people and putting them through

220
00:25:29,440 --> 00:25:36,320
a trial and measuring the benefits. So then you have questions about how it was performed,

221
00:25:36,320 --> 00:25:41,040
whether it was a double blind, so whether there was bias involved or

222
00:25:43,360 --> 00:25:50,160
whether there was some sort of... Because studies show that

223
00:25:50,160 --> 00:25:54,640
placebo's are beneficial to people.

224
00:25:54,640 --> 00:25:57,760
placebo meaning medicine that is just sugar, it's not medicine.

225
00:25:57,760 --> 00:26:02,960
But when it's given to people, so as a depression medication, for example,

226
00:26:02,960 --> 00:26:13,360
placebo's work, I think it was they work as good as drugs or something, sometimes even better.

227
00:26:13,360 --> 00:26:25,920
No, placebo's don't but noxibos do. Noxibos being non-drugs, fake drugs that also have unpleasant

228
00:26:25,920 --> 00:26:32,560
side effects. So if a drug had unpleasant side effects, it actually did better perform better,

229
00:26:33,200 --> 00:26:40,960
I think, than certain drugs or something. I mean, just ridiculous studies that showed

230
00:26:42,080 --> 00:26:50,880
that anoxibo performed better than the actual depression drugs.

231
00:26:50,880 --> 00:27:01,200
Because people thought they were taking real drugs. So the question is how much of a benefit

232
00:27:01,200 --> 00:27:04,480
and how reliable is it to say that it's beneficial?

233
00:27:06,080 --> 00:27:10,720
That being said, your question is different, right? Your question is why do monks do chanting?

234
00:27:11,600 --> 00:27:16,480
How are you related exactly to your... Can you clarify that for me, Robin? How is it related to

235
00:27:16,480 --> 00:27:22,880
this idea of saying, ah, or something? Yes, he was recently shown a breathing technique where

236
00:27:22,880 --> 00:27:28,320
you breathe in through your nose and then exhale. How does that, how does that, the idea is that

237
00:27:28,320 --> 00:27:32,400
the act of chanting might be beneficial to your nervous system, is that the point?

238
00:27:32,400 --> 00:27:38,800
I think so, yes. Well, that's not why we practice. We're not so worried about our nervous system,

239
00:27:38,800 --> 00:27:50,640
not really at all anyway, actually. We chant in order to one express respect and veneration

240
00:27:50,640 --> 00:27:57,360
and remembrance of the Buddha, but we also chant to remember the dhamma, so that's to remember

241
00:27:57,360 --> 00:28:01,520
the teachings of the Buddha, so we chant and recite in order to keep them in mind.

242
00:28:01,520 --> 00:28:08,960
Let's see, those are the two main reasons. There's another reason that people often chant

243
00:28:08,960 --> 00:28:14,720
today and probably most Buddhist chanting is done today as a protection because people want this

244
00:28:14,720 --> 00:28:21,680
kind of physical and mundane protection, which is okay, it's just somewhat

245
00:28:24,560 --> 00:28:30,240
well clingy, really, we're both clingy if you're worried about, but they people are rightly worried

246
00:28:30,240 --> 00:28:34,800
and know they have all these worries about protecting their belongings and stuff and their life

247
00:28:34,800 --> 00:28:43,200
and their health, and so we're able to, it's kind of a crucial way, you tell people while these

248
00:28:43,200 --> 00:28:47,600
teachings, if you remember these teachings, it'll be good for you, and so they memorized,

249
00:28:47,600 --> 00:28:52,160
they recite these teachings, but actually the teachings will teach them how to be good people,

250
00:28:52,160 --> 00:29:00,480
it's sort of a basic means of giving people an introduction to goodness, even though they're doing it,

251
00:29:01,200 --> 00:29:09,600
too, as a means for worldly gain for the most part, like those people who pay respect to the

252
00:29:09,600 --> 00:29:21,760
Bodhi tree in order to get pregnant. How about chanting when you don't understand what you're saying,

253
00:29:21,760 --> 00:29:26,400
is that still a benefit? I mean, if you're doing it for the right reason, right, if you're going to

254
00:29:26,400 --> 00:29:34,880
get a heart full of goodness, like there was this, a gentong always talks about this, this bird

255
00:29:34,880 --> 00:29:42,560
that went to stay with the Bhikunis, and they had it memorized api, which means bones, bones,

256
00:29:42,560 --> 00:29:52,240
I didn't even know what it was saying, but it kept reciting it, and it was so powerful that

257
00:29:52,240 --> 00:29:58,640
this eagle came and grabbed it, and when it grabbed it, the bird said, Ati Ati had spoken

258
00:29:58,640 --> 00:30:06,080
at that, and the eagle was shocked, somehow by the power of the word or something,

259
00:30:06,080 --> 00:30:13,600
in the Bhikunis sort of thought that this was indicative of the power of good words,

260
00:30:16,480 --> 00:30:22,720
and somehow that's in the commentary to the Sati Patanasu. Atjantong always says that the

261
00:30:22,720 --> 00:30:29,600
bird went to heaven, but I don't think the actual suit says that, or the comment or

262
00:30:29,600 --> 00:30:34,240
anyway says that. I don't know if there's a place where it does say that the bird went to heaven,

263
00:30:34,240 --> 00:30:39,360
but that would be a better story, and I think that's why he tells it that the bird went to heaven

264
00:30:39,360 --> 00:30:45,760
because it had memorized this word, even though it didn't know what it was saying. So Atjantong always

265
00:30:45,760 --> 00:30:49,840
gets us to memorize the four Sati Patanas, and then he said, Now you're as good as animals,

266
00:30:49,840 --> 00:30:54,640
you're all going to go to heaven. It is all these animals who didn't understand what they were

267
00:30:54,640 --> 00:30:59,440
saying, but they still went to heaven, so all of you are going to heaven, but you're human being,

268
00:30:59,440 --> 00:31:04,880
so you can do better than that, and so now we're going to teach you the meaning, and he gives

269
00:31:04,880 --> 00:31:13,840
this talk again, and it often says this. So he's very clear, and it's quite clear that if your heart

270
00:31:13,840 --> 00:31:18,080
is in the right place, it doesn't matter whether you understand. He also would bring up that story

271
00:31:18,080 --> 00:31:23,040
when talking about us back when I didn't understand, and whenever there were meditators coming

272
00:31:23,040 --> 00:31:26,640
to listen to the Dhamma talk, he always wanted the Western meditators to come and listen to the

273
00:31:26,640 --> 00:31:31,200
Thai Dhamma talk. He said, Even if you don't understand, you're sitting there, and you have a

274
00:31:31,200 --> 00:31:35,440
respectful mind, and you know that what's being said is Dhamma is the Buddha's teaching,

275
00:31:36,400 --> 00:31:42,560
so good things come, you're good at heaven.

276
00:31:46,400 --> 00:31:49,840
Hello, Bhante, why do we know sitting if it's considered a concept?

277
00:31:49,840 --> 00:32:00,000
That's a good point, because sitting is a description of ultimate realities, or can be seen as a

278
00:32:00,000 --> 00:32:05,200
description of ultimate realities. When you say sitting, you become aware of something present,

279
00:32:06,080 --> 00:32:13,040
which is the feeling of sitting or the experience of sitting. Sometimes you won't notice that

280
00:32:13,040 --> 00:32:17,280
you're sitting, there's no feeling or there's no sense when you're really still,

281
00:32:17,280 --> 00:32:21,600
but it comes from ultimate realities.

282
00:32:28,400 --> 00:32:33,680
During the sitting meditation is the focus to be on the feeling in the body, or noticing the action.

283
00:32:37,600 --> 00:32:43,680
It's the feeling, I mean the action is only apparent because of the feeling, so it's the feeling.

284
00:32:43,680 --> 00:32:47,760
Physical sensation.

285
00:32:54,880 --> 00:32:56,560
And that's all the questions for tonight.

286
00:32:59,200 --> 00:33:05,600
Okay, well then, have a good night. Thanks everyone for tuning in.

287
00:33:06,800 --> 00:33:10,880
34 of yours on YouTube. Hello, everyone. Thank you for tuning in.

288
00:33:10,880 --> 00:33:16,640
It's always nice to see that people are still interested in this.

289
00:33:18,000 --> 00:33:23,680
I might have more going on in the future. I've thought about just thinking about doing

290
00:33:24,880 --> 00:33:29,920
maybe a once a week, a talk on something a bit more directly related to meditation.

291
00:33:31,680 --> 00:33:37,680
That might come in the new year because it might be quitting university in January,

292
00:33:37,680 --> 00:33:40,560
in order to do more teaching.

293
00:33:42,720 --> 00:33:50,400
So there's, I could add more slots to the weekly meetings because it's full mostly.

294
00:33:53,520 --> 00:33:58,640
Anyway, for now this is what we've got, so thank you for tuning in. Thanks Robin for your help.

295
00:33:58,640 --> 00:33:59,680
Thank you, Robin.

296
00:33:59,680 --> 00:34:09,680
Wonderful. Good night.

