1
00:00:00,000 --> 00:00:15,000
This is my first talk back in second life, and it's good to be back, a little bit unprepared.

2
00:00:15,000 --> 00:00:29,000
I was just confirmed yesterday that I'd be giving a talk today, so I am happy to be back,

3
00:00:29,000 --> 00:00:37,000
and it's good to see so many people interested in such things as this.

4
00:00:37,000 --> 00:00:43,000
Now, the reason I thought I'd talk today about Peace, Love and Happiness

5
00:00:43,000 --> 00:00:50,000
is specifically because this place is what it is that second life

6
00:00:50,000 --> 00:01:03,000
is a place that we come to get these very worthwhile qualities

7
00:01:03,000 --> 00:01:08,000
that we come here looking for peace, and we come here looking for love,

8
00:01:08,000 --> 00:01:11,000
we come here looking for happiness.

9
00:01:11,000 --> 00:01:17,000
So this isn't exactly the way I would frame a talk about Buddhism,

10
00:01:17,000 --> 00:01:22,000
but I think it's useful for all of us because when you give a talk,

11
00:01:22,000 --> 00:01:25,000
you give a talk for a certain audience.

12
00:01:25,000 --> 00:01:29,000
So we come to second life, we're looking for peace, or we come here,

13
00:01:29,000 --> 00:01:34,000
and this is why we stay, or this is what we gain from being here,

14
00:01:34,000 --> 00:01:39,000
we gain peace of mind that comes from getting away from our lives,

15
00:01:39,000 --> 00:01:43,000
and we realize that it's maybe an escape,

16
00:01:43,000 --> 00:01:49,000
but it does give us this quality of being free from the troubles

17
00:01:49,000 --> 00:01:55,000
and the cares of everyday life, being free from the stress.

18
00:01:55,000 --> 00:02:01,000
We have this peace, you know, I think this is obvious to all of us as we sit here

19
00:02:01,000 --> 00:02:10,000
in the Deer Park, where most of us in our everyday life

20
00:02:10,000 --> 00:02:14,000
would probably be very hard pressed to find a Deer Park,

21
00:02:14,000 --> 00:02:18,000
nearby, of course there may be some who have it,

22
00:02:18,000 --> 00:02:27,000
but for many of us this is a welcome break from city life, and so on.

23
00:02:27,000 --> 00:02:32,000
Actually it's funny, the owl in the background here has the same sound as an owl,

24
00:02:32,000 --> 00:02:35,000
that's just outside my door every night.

25
00:02:35,000 --> 00:02:43,000
So actually you can find this sort of peace in everyday life,

26
00:02:43,000 --> 00:02:50,000
but second life kind of gives us this quick and easy escape

27
00:02:50,000 --> 00:03:02,000
into nature and into peaceful, magical, or otherwise liberating environment.

28
00:03:02,000 --> 00:03:10,000
But it's important to understand, and as Buddhist we probably all are thinking that

29
00:03:10,000 --> 00:03:13,000
this sort of peace is a very external sort of peace,

30
00:03:13,000 --> 00:03:18,000
and also a very temporary sort of peace.

31
00:03:18,000 --> 00:03:24,000
It's something that can actually get in the way of our spiritual progress

32
00:03:24,000 --> 00:03:26,000
if we become addicted to it.

33
00:03:26,000 --> 00:03:34,000
Why? Because it becomes a contrast to real life.

34
00:03:34,000 --> 00:03:38,000
It becomes a contrast to our everyday situation,

35
00:03:38,000 --> 00:03:48,000
which is often full of stressful, unpleasant experiences.

36
00:03:48,000 --> 00:03:53,000
And, you know, it will be nice to be able to just slip into the computer

37
00:03:53,000 --> 00:03:56,000
and not come out.

38
00:03:56,000 --> 00:03:59,000
Like that movie Avatar, I didn't watch it, but I heard this.

39
00:03:59,000 --> 00:04:06,000
They learned how to slip into their avatars and not come back.

40
00:04:06,000 --> 00:04:08,000
We don't have the technology here yet.

41
00:04:08,000 --> 00:04:16,000
I think Lyndon Labs is working on it, but at the moment it's still not possible.

42
00:04:16,000 --> 00:04:18,000
And so we have to go back to our real life.

43
00:04:18,000 --> 00:04:22,000
And this isn't a problem, it shouldn't be a problem.

44
00:04:22,000 --> 00:04:28,000
For Buddhists it certainly shouldn't be considered a problem because we have a way out.

45
00:04:28,000 --> 00:04:33,000
We have this escape, we have this liberation.

46
00:04:33,000 --> 00:04:41,000
But it's not something that comes from running away from situations.

47
00:04:41,000 --> 00:04:45,000
It comes from letting go of them, it comes from freeing ourselves.

48
00:04:45,000 --> 00:04:50,000
From the stresses, from the problems.

49
00:04:50,000 --> 00:04:56,000
See, the only reason that things in our everyday life are giving us this impetus

50
00:04:56,000 --> 00:05:03,000
to get away and to escape into an alternate reality

51
00:05:03,000 --> 00:05:08,000
is because of our inability to understand and our inability to accept

52
00:05:08,000 --> 00:05:13,000
and our inability to live with them, our inability to find peace

53
00:05:13,000 --> 00:05:16,000
in the situation.

54
00:05:16,000 --> 00:05:27,000
It comes from our approach, our reaction to the experiences, to our everyday lives.

55
00:05:27,000 --> 00:05:36,000
So Buddhism is something that, while it doesn't detract from these escapes,

56
00:05:36,000 --> 00:05:43,000
many Buddhist meditations are a form of escape, of temporary vacation from everyday reality.

57
00:05:43,000 --> 00:05:48,000
And you can even leave your body in certain Buddhist meditations

58
00:05:48,000 --> 00:05:54,000
or fly away to heaven or go to see faraway places.

59
00:05:54,000 --> 00:06:01,000
You can enter into visualization exercises where suddenly your mind expands

60
00:06:01,000 --> 00:06:06,000
to visualize the whole of the cosmos.

61
00:06:06,000 --> 00:06:09,000
And these are not considered wrong by any means.

62
00:06:09,000 --> 00:06:12,000
But Buddhism then uses that as a stepping stone.

63
00:06:12,000 --> 00:06:17,000
And it can use a place like Second Life as a stepping stone because we come here.

64
00:06:17,000 --> 00:06:20,000
And we have great peace.

65
00:06:20,000 --> 00:06:23,000
Our minds are not distracted.

66
00:06:23,000 --> 00:06:35,000
We're not disturbed by stressful experiences or stressful interactions.

67
00:06:35,000 --> 00:06:39,000
And so it gives us a chance to slow down and to start to think

68
00:06:39,000 --> 00:06:45,000
and start to open up our minds and see things more objectively.

69
00:06:45,000 --> 00:06:48,000
And look at how our mind works.

70
00:06:48,000 --> 00:06:55,000
So we use all of these experiences not considered to be a obstacle to practice.

71
00:06:55,000 --> 00:06:58,000
They're a stepping stone.

72
00:06:58,000 --> 00:07:03,000
Then we would take a place like this and we start to look at our own minds.

73
00:07:03,000 --> 00:07:05,000
We forget about what's going on.

74
00:07:05,000 --> 00:07:07,000
We don't pay attention to our troubles.

75
00:07:07,000 --> 00:07:12,000
Let's look at what's going on in our minds and look at how our minds work.

76
00:07:12,000 --> 00:07:17,000
And if we're good at this, if we really put our hearts into this,

77
00:07:17,000 --> 00:07:24,000
we can really gain the tools that are necessary to bring the piece back into our daily lives

78
00:07:24,000 --> 00:07:26,000
so that it's not just peaceful here.

79
00:07:26,000 --> 00:07:31,000
But then when the little stresses come up, we don't turn them into big stresses

80
00:07:31,000 --> 00:07:39,000
when interactions with other people or situations start to make us stressed out.

81
00:07:39,000 --> 00:07:40,000
We're able to understand them.

82
00:07:40,000 --> 00:07:44,000
We're able to understand what's going on in our minds.

83
00:07:44,000 --> 00:07:50,000
This is the essence of Buddhist practice.

84
00:07:50,000 --> 00:08:02,000
This sort of meditation we call it, it means investigating the experience.

85
00:08:02,000 --> 00:08:05,000
So here we're all sitting here and we haven't left reality behind.

86
00:08:05,000 --> 00:08:08,000
We haven't left our minds back in real life.

87
00:08:08,000 --> 00:08:10,000
We brought them with us.

88
00:08:10,000 --> 00:08:13,000
And so you can ask yourself what's going on in my mind right now.

89
00:08:13,000 --> 00:08:19,000
Maybe some of you are happy to be here and happy to be listening to a talk on.

90
00:08:19,000 --> 00:08:20,000
Buddhism.

91
00:08:20,000 --> 00:08:26,000
Maybe some of you are unhappy and think that what I'm saying is a whole crock of bull.

92
00:08:26,000 --> 00:08:36,000
And so there will be all sorts of judgments going on in the mind.

93
00:08:36,000 --> 00:08:48,000
And these are really at the core of our stress, our problems, the core of our suffering.

94
00:08:48,000 --> 00:09:04,000
The reason why we suffer in life is because of the extrapolation and making more of things than they actually are judging.

95
00:09:04,000 --> 00:09:10,000
Turning mountains into mountains.

96
00:09:10,000 --> 00:09:17,000
So that when someone comes and says something to us then it becomes a grievance towards ourselves.

97
00:09:17,000 --> 00:09:30,000
Or when we see something or we come in contact with something pleasurable then it becomes something that we must have and we chase after it.

98
00:09:30,000 --> 00:09:46,000
So Buddhism is really the practice of acceptance, of understanding that allows us to accept, that allows us to experience, that allows us to live without judging.

99
00:09:46,000 --> 00:09:51,000
And so we do this by penetrating into the nature of the experience.

100
00:09:51,000 --> 00:09:55,000
So when we're sitting here and there arises a happy feeling, we know that it's a happy feeling.

101
00:09:55,000 --> 00:10:03,000
We don't say this is good, this is bad, we don't say this is me, this is mine or I like this or let it stay or make it go or so on.

102
00:10:03,000 --> 00:10:05,000
We just know that we're happy.

103
00:10:05,000 --> 00:10:08,000
When we're unhappy, we know that we're unhappy.

104
00:10:08,000 --> 00:10:11,000
When we like something, we know that we like something.

105
00:10:11,000 --> 00:10:16,000
We penetrate into the reality, we start to figure out how our mind works.

106
00:10:16,000 --> 00:10:22,000
We start to see the relationships between mind states so that when we like something then there arises an addiction to it.

107
00:10:22,000 --> 00:10:27,000
When there's a happy feeling, it goes rise to liking.

108
00:10:27,000 --> 00:10:30,000
When there's an unhappy feeling, it goes rise to disliking.

109
00:10:30,000 --> 00:10:33,000
When there's disliking then we get angry and so on.

110
00:10:33,000 --> 00:10:35,000
When we get angry then we suffer.

111
00:10:35,000 --> 00:10:47,000
And so learning how our mind works, coming to bring about this change that allows us to accept things which we can then take into our daily life.

112
00:10:47,000 --> 00:10:51,000
So this is the Buddhist idea of where we find true peace.

113
00:10:51,000 --> 00:10:55,000
It's not simply in getting away for a short time.

114
00:10:55,000 --> 00:11:04,000
Getting away for a short time is considered to be helpful and often necessary to bring about the change.

115
00:11:04,000 --> 00:11:07,000
But we have to make use of this.

116
00:11:07,000 --> 00:11:12,000
There are many people who come to second life and obviously don't find any sort of enlightenment.

117
00:11:12,000 --> 00:11:17,000
I think this is maybe there's only a few of them.

118
00:11:17,000 --> 00:11:24,000
But I imagine there are a few people here in second life who don't find enlightenment.

119
00:11:24,000 --> 00:11:28,000
Maybe more than a few.

120
00:11:28,000 --> 00:11:31,000
There's probably a lot of people who come and don't find peace.

121
00:11:31,000 --> 00:11:36,000
They find rather excitement and addiction and so on.

122
00:11:36,000 --> 00:11:43,000
So this is the sort of how I would approach the idea of coming to find peace,

123
00:11:43,000 --> 00:11:44,000
coming to get away.

124
00:11:44,000 --> 00:11:48,000
And if we really want to get away we have to be prepared to face.

125
00:11:48,000 --> 00:11:56,000
Be prepared to overcome rather than push away and chase away and run away.

126
00:11:56,000 --> 00:12:07,000
Next is the idea of love and I think this any talk about virtual reality of this sort has to talk about has to incorporate the idea of love.

127
00:12:07,000 --> 00:12:17,000
Because I think it's a big part of what the internet would have spurred on the internet or funded the internet.

128
00:12:17,000 --> 00:12:30,000
Certainly a big part of what has funded second life is this idea of love, this idea of relationships with other beings, with other people.

129
00:12:30,000 --> 00:12:38,000
And I've touched on this before but I'm sure there are many people who haven't heard it and it's always worth repeating.

130
00:12:38,000 --> 00:12:53,000
That we often confuse love, this idea of wanting other beings to be happy with the idea of attachment, with the concept of attachment.

131
00:12:53,000 --> 00:13:04,000
So we say I love that person or I love this person but often it's simply a physical or even a mental attachment to the idea that they're going to make us happy.

132
00:13:04,000 --> 00:13:09,000
And these are really two very very very different emotions.

133
00:13:09,000 --> 00:13:12,000
Attachment is something that I would consider to be negative.

134
00:13:12,000 --> 00:13:16,000
It says I want you to make me happy.

135
00:13:16,000 --> 00:13:18,000
Or how are you going to make me happy?

136
00:13:18,000 --> 00:13:29,000
Or it expects some level of take and love on the other hand is always saying how can I make you happy?

137
00:13:29,000 --> 00:13:50,000
And so if we look around this room around this campfire and we can ask ourselves when we came in here and as we started talking to each other or even what was going through our minds as we started looking around at each other.

138
00:13:50,000 --> 00:14:03,000
What were we thinking? Were we thinking how can I make these people happy? What can I do to make them really feel good to make them really feel at peace, to make them really feel really feel alive?

139
00:14:03,000 --> 00:14:06,000
Or were we thinking what am I going to get out of this?

140
00:14:06,000 --> 00:14:15,000
And we should be able to see that we generally have a combination of these two but that they are very very different.

141
00:14:15,000 --> 00:14:32,000
Attachment is something which shrinks which contracts the spirit which contracts the mind into a sort of clingy shriveled up state which is not really happy at all.

142
00:14:32,000 --> 00:14:39,000
It's not really at peace and it's certainly not a state of love.

143
00:14:39,000 --> 00:14:49,000
And yet this is often most of the time all that we get out of a place like Second Life because we fail to go the next step.

144
00:14:49,000 --> 00:15:03,000
We fail to take it to a more unadulterated sort of love where we are looking at people and thinking gosh this person has suffering.

145
00:15:03,000 --> 00:15:12,000
And this person is stressed out. This person has problems. What can I do to make them happy? What can I do to bring them peace?

146
00:15:12,000 --> 00:15:20,000
And this is why it's always nice to be a teacher because you always have this chance to give someone happiness.

147
00:15:20,000 --> 00:15:29,000
And of course the problem with that is the more we want to teach the more attachment we have thinking that it's going to bring us happiness and how.

148
00:15:29,000 --> 00:15:41,000
And so we try to go out and teach people and give them all sorts of advice and so on and we find that they usually don't take kindly to our advice because they can see that we're only doing it for our own benefit.

149
00:15:41,000 --> 00:15:50,000
It's not easy being a teacher. It's not easy really giving people something useful. I don't claim to be good at it.

150
00:15:50,000 --> 00:16:01,000
But I think with practice I've at least been able to give people something. So I think this is a very important sort of love that we can have for each other.

151
00:16:01,000 --> 00:16:07,000
This idea of giving people something. We look at people and we try to give them something.

152
00:16:07,000 --> 00:16:12,000
We try to do what we can for them. Doesn't mean we have to go around proselytizing.

153
00:16:12,000 --> 00:16:18,000
I hate people who proselytize. I've done it a lot myself in the past. It's often involuntary.

154
00:16:18,000 --> 00:16:24,000
And it's certainly not out of love. It's out of this attachment and this greed.

155
00:16:24,000 --> 00:16:32,000
If you really want to help people, you have to be honest and sincere and you have to really have love in your heart.

156
00:16:32,000 --> 00:16:42,000
See, it's not something that we all have. Most of us have to work really hard at developing real and true love.

157
00:16:42,000 --> 00:16:49,000
But of course, it's so much more rewarding, infinitely more rewarding than attachment.

158
00:16:49,000 --> 00:16:53,000
You can go through second life. You can go through the internet. I used to, when I was a teenager,

159
00:16:53,000 --> 00:17:02,000
and you can go looking for all sorts of enticing things on various levels of morality.

160
00:17:02,000 --> 00:17:13,000
And you can ask yourself, you can be promiscuous. You can be indulgent. You can become addicted to so many different things.

161
00:17:13,000 --> 00:17:21,000
And then I ask yourself how much happiness, how much contentment, how much peace, how much freedom from suffering it's brought to you.

162
00:17:21,000 --> 00:17:34,000
And you can see how really it just makes you crazy. It just brings more and more suffering.

163
00:17:34,000 --> 00:17:42,000
And so considering how important this is for most people and how it's often the impetus,

164
00:17:42,000 --> 00:17:47,000
one of the real impetus is for coming to a place like this.

165
00:17:47,000 --> 00:17:55,000
I think it's important that we not stop just at trying to get what we want all the time,

166
00:17:55,000 --> 00:18:01,000
but trying to give other people something that is really useful for them.

167
00:18:01,000 --> 00:18:06,000
And the Buddha was pretty clear, actually, which surprises many people.

168
00:18:06,000 --> 00:18:11,000
He was very clear that one should always strive for one's own benefit.

169
00:18:11,000 --> 00:18:17,000
He said one should never put another person's benefit above one's own.

170
00:18:17,000 --> 00:18:24,000
And you can think of how shocking this might be considering how selfless Buddhism is supposed to be.

171
00:18:24,000 --> 00:18:32,000
But here's the key is that you can say that you love people because of how great it makes you feel,

172
00:18:32,000 --> 00:18:38,000
or you give to people, or you share with others, or you do good things for how great it makes you feel.

173
00:18:38,000 --> 00:18:42,000
But the reason that it makes you feel so great is because it's actually helping others.

174
00:18:42,000 --> 00:18:49,000
And you know that, because it's pure, it's the purity of mind that comes from love that is such a great thing.

175
00:18:49,000 --> 00:19:00,000
And so the point being that going around and chasing after central pleasures is not looking up for your best interest.

176
00:19:00,000 --> 00:19:04,000
It's not really making you happy.

177
00:19:04,000 --> 00:19:16,000
And if someone really wants to look up for their best interest, then they should open up their minds and open up their hearts to this state of pure love,

178
00:19:16,000 --> 00:19:21,000
and really try to help people to give people something useful for themselves.

179
00:19:21,000 --> 00:19:27,000
So this I think is an important point that we should consider on the idea of love.

180
00:19:27,000 --> 00:19:42,000
The final overarching theme that I'd like to discuss today bring up is the idea of happiness.

181
00:19:42,000 --> 00:19:53,000
And happiness is probably the most contentious I've had arguments about in the meaning of the word happiness.

182
00:19:53,000 --> 00:20:00,000
And many people think it seems that happiness is something that is totally arbitrary.

183
00:20:00,000 --> 00:20:06,000
That what makes me happy is possibly not what's going to make you happy.

184
00:20:06,000 --> 00:20:16,000
And therefore, I guess the implication is that

185
00:20:16,000 --> 00:20:24,000
you can never come up with a satisfactory or happiness is something that we should never really approach or discuss,

186
00:20:24,000 --> 00:20:34,000
or try to strive towards or formalize into a sort of a system that brings us happiness.

187
00:20:34,000 --> 00:20:39,000
And nonetheless, I'd argue that happiness can be broken up.

188
00:20:39,000 --> 00:20:47,000
It can be at least broken up into two categories.

189
00:20:47,000 --> 00:20:54,000
And the one category is that category that says that you have to agree that for everyone to each their own, that happiness is really a subjective experience.

190
00:20:54,000 --> 00:20:56,000
So you can call this subjective happiness.

191
00:20:56,000 --> 00:21:00,000
But the sort of happiness is the happiness that is external to ourselves.

192
00:21:00,000 --> 00:21:04,000
It's the happiness that depends on objects.

193
00:21:04,000 --> 00:21:14,000
And why this is subjective, why it is so impossible to pin down what's going to make someone happy.

194
00:21:14,000 --> 00:21:23,000
Because this sort of happiness is totally dependent on our experience and our conditioning, not just in this life.

195
00:21:23,000 --> 00:21:38,000
But you can confine to this life, but actually even over countless lifetimes that we've been running through some sorrow that we've been running around.

196
00:21:38,000 --> 00:21:41,000
And you can see this when we come to Second Life.

197
00:21:41,000 --> 00:21:50,000
If you were to click, I suppose, even in this circle, this looks like a fairly conservative, comparatively conservative group of people.

198
00:21:50,000 --> 00:21:57,000
There were a couple of half naked women sitting here, and I don't see any today, so a bit relieved as a monk.

199
00:21:57,000 --> 00:22:05,000
Maybe they knew it was a monk giving the talks or they put clothes on or didn't come or something.

200
00:22:05,000 --> 00:22:10,000
But if you look at the profiles of the various people, you can see that to each their own, it's really true.

201
00:22:10,000 --> 00:22:15,000
There's a lot of people who come here for fairly exotic pleasures.

202
00:22:15,000 --> 00:22:27,000
And if you look at the histories of those people, if you look at their background, you can see the relationship very clearly that.

203
00:22:27,000 --> 00:22:35,000
To each their own, we develop these likes and these predilections over time.

204
00:22:35,000 --> 00:22:46,000
And this is really, I think, what the majority of people in the world are hooked upon. It's probably why most people come to Second Life.

205
00:22:46,000 --> 00:22:50,000
It's why most people surf the internet. And I think we're all guilty of it.

206
00:22:50,000 --> 00:22:58,000
Until we become perfectly enlightened, we still have this attachment to sensuality when we see beautiful things we chase after them.

207
00:22:58,000 --> 00:23:14,000
And so we come to a place like this looking for sensual pleasure, wanting to see beautiful things, wanting to hear beautiful sounds and smells and tastes and feelings and thoughts.

208
00:23:14,000 --> 00:23:28,000
Now, this kind of happiness, of course, especially from a Buddhist perspective, is limited and is adulterated or it's problematic.

209
00:23:28,000 --> 00:23:38,000
It's something which is not the be-all and end-all of happiness that you can't just get more and subsequently get more happiness.

210
00:23:38,000 --> 00:23:52,000
And I've talked about why this is before as well, and this isn't a particular Buddhist teaching, but if you look at the biology of addiction, you can see how true it is.

211
00:23:52,000 --> 00:24:11,000
That when we get the things that we like, when we come in here to Second Life and we see and we hear and we smell and we chase after these beautiful experiences or beautiful phenomena.

212
00:24:11,000 --> 00:24:31,000
When we get them, there's this chemical reaction that goes on in the brain when it knows that it's got what it wants. It releases certain chemicals and these chemicals, bring about a sensation which is interpreted by the mind as pleasurable, the mind then likes it and start the cycle again looking for more of the same.

213
00:24:31,000 --> 00:24:44,000
Except what happens in the brain is that it's kind of like a stretching, it doesn't exactly stretch, but it's like when you stretch a piece of cloth or something and eventually get stretched out of shape.

214
00:24:44,000 --> 00:24:55,000
This is sort of what happens to the brain that the brain starts to get stretched out of shape, but it means it's not able to produce the same amount of chemical.

215
00:24:55,000 --> 00:25:03,000
Next time you get what you want, there's less of the chemical, less of the sensation, and then the mind doesn't feel as happy.

216
00:25:03,000 --> 00:25:09,000
It doesn't feel as happy because it's not getting what it wanted, which is this chemical reaction.

217
00:25:09,000 --> 00:25:31,000
And you see how this plays out that in the beginning it's just an innocent sort of looking and then it has to be getting something more intense, maybe first we see a picture and we think, oh, that's beautiful and then we have to see the video and then we think, oh, that's beautiful and so on and so on.

218
00:25:31,000 --> 00:25:42,000
And so, until finally it just gets to the point where it just says with the drug addict, you wind up in a state of withdrawal not being able to get what you want and you have a choice.

219
00:25:42,000 --> 00:25:56,000
You can either start working your way back, climbing your way back to a more ordinary state of physical and mental activity, or you go on until finally something breaks.

220
00:25:56,000 --> 00:26:02,000
You break down and suffer tragically.

221
00:26:02,000 --> 00:26:04,000
We see this with drug addicts.

222
00:26:04,000 --> 00:26:12,000
We can also see it with ourselves when we're in our everyday lives, or here in second life, and we just keep looking.

223
00:26:12,000 --> 00:26:17,000
We just keep chasing after these things.

224
00:26:17,000 --> 00:26:35,000
So this is not considered to be ultimate and true happiness, and as was said earlier, a comment someone made was that these things are especially happiness is very difficult to gain in space time reality.

225
00:26:35,000 --> 00:26:54,000
And I think this is very true that as long as our happiness is based on something external to ourselves, something ephemeral, something that comes and goes and we're never going to hope to be able to be satisfied by it.

226
00:26:54,000 --> 00:27:08,000
So again, I'm going to encourage everyone based on the Buddhist teachings to go to the next step and to ask yourself what is really and truly going on that it is that makes us happy.

227
00:27:08,000 --> 00:27:27,000
Well, what we're seeing is that our happiness is dependent on things like the brain, the chemicals that come and go in the brain, which in turn is dependent on things that we see and that we hear and that we smell and that we taste that we feel and that we think.

228
00:27:27,000 --> 00:27:40,000
And if we were to somehow be able to pull ourselves away from that to let go of that, let's say, and just be happy to be happy in and of ourselves.

229
00:27:40,000 --> 00:27:50,000
To have our happiness not dependent on something that is impermanent, something that is coming and going to actually find that which is stable.

230
00:27:50,000 --> 00:27:59,000
And that which is permanent, that which is lasting.

231
00:27:59,000 --> 00:28:16,000
And I think we'd all agree that we'd have then a happiness which was not only truly happy in the sense of being able to satisfy us, but also universal, where it being stable, it being secure that everyone would have to agree that this is the greatest happiness.

232
00:28:16,000 --> 00:28:32,000
And so in Buddhism, this is really what we're striving for. It's not some specific point that we at instantaneously attain or we struggle very hard and then somehow poof we arise there.

233
00:28:32,000 --> 00:28:35,000
It's something which comes gradually.

234
00:28:35,000 --> 00:28:52,000
And the way it works is as we slowly let go of those things that we thought we're bringing us happiness and that are bringing us only more and more addiction and taking us further and further away from a stable state of existence.

235
00:28:52,000 --> 00:29:11,000
We start to attain this, this very thing. We start to attain something that we thought we already had. You know, most of us think that we're ordinary individuals and we have a fairly stable state of mind, which is secure and permanent and lasting and so on.

236
00:29:11,000 --> 00:29:36,000
But once we start to look closer, we can see that actually the average person is in many ways not dissimilar from an insane individual, a person who has, in fact we could say we all in some way have mental illness in the sense of our minds not being totally and perfectly healthy, being totally in purpose.

237
00:29:36,000 --> 00:30:05,000
And most of us think of ourselves as having good mental health and yet we would all agree we would all admit that we have things which are a sign of mental instability have these stresses, we have these worries, we have these fears, we have all of the neurotic characteristics of an insane individual to a greater or lesser extent.

238
00:30:05,000 --> 00:30:14,000
Of course for most of us it's to a lesser extent.

239
00:30:14,000 --> 00:30:30,000
But what Buddhism seeks to achieve is a greater and greater state of stability and equilibrium.

240
00:30:30,000 --> 00:30:38,000
Equanimity in our minds where we no longer chase after good things and no longer run away from bad things.

241
00:30:38,000 --> 00:30:55,000
Where we're able to see things as they are and through the meditation as we start to penetrate and start to look and start to accept and start to acknowledge things just for what they are not saying oh that's bad or oh that's good.

242
00:30:55,000 --> 00:31:06,000
That we start to let go and our minds start to settle down and our minds start to see clearly start to give up start to let go and start to just be.

243
00:31:06,000 --> 00:31:11,000
And once we're able to just be then we are truly happy.

244
00:31:11,000 --> 00:31:26,000
Then we're in a state where no matter what comes whether we're in second life or out chasing whatever in the world running the rat race and living among such great external suffering.

245
00:31:26,000 --> 00:31:31,000
We can still be at great peace and happiness internally.

246
00:31:31,000 --> 00:31:54,000
And of course this is may sound very much pie in the sky and hard to achieve and so on but this is why I say it's a very it's it's not something that is like a mountain peak where you have to get to the top before you experience the happiness it's like the climbing up the mountain that we can do in our daily lives.

247
00:31:54,000 --> 00:32:01,000
And so whereas being totally and perfectly happy all the time might be unattainable for most of us.

248
00:32:01,000 --> 00:32:18,000
We can at the very least see very clear solid concrete results on a day to day basis as we practice if you practice for one day then you can see that you've gained one days worth of happiness.

249
00:32:18,000 --> 00:32:33,000
Or you've gained one one step further a days worth of steps towards this goal of being free from suffering and being at true peace and happiness.

250
00:32:33,000 --> 00:32:40,000
And I think these are all related the idea of peace is of course very much tied in with the idea of happiness.

251
00:32:40,000 --> 00:32:54,000
Love in Buddhism as I said is is not exactly core. The point being that love is something that comes from being pure it's something that comes from being at peace with yourself.

252
00:32:54,000 --> 00:33:04,000
When you're truly happy and truly at peace with yourself then you have no need for other people to make you happy and all you can think of when people come is how you can make them happy.

253
00:33:04,000 --> 00:33:10,000
You see that there's suffering and you can feel the stress that's in them that is absent in you.

254
00:33:10,000 --> 00:33:27,000
And so you're able to give them this rope so to speak and pull them you're able to send them the line and bring them explained to them or to lead them in your direction simply by calling to them and showing them the way.

255
00:33:27,000 --> 00:33:49,000
So I think these three things are going to be very useful in terms of explaining and kind of opening people up to a higher and a more pure and more undulterated experience of each of them.

256
00:33:49,000 --> 00:33:57,000
So that's what I thought I would talk about today again as I said I'm a little bit unprepared so I hope it wasn't.

257
00:33:57,000 --> 00:34:08,000
I'm a little rusty I suppose but I thank you all for coming and I hope it's been at least a bit enlightening for at least some of you.

258
00:34:08,000 --> 00:34:12,000
Thank you all for coming and hope to see you next time.

259
00:34:12,000 --> 00:34:22,000
If you have any questions I'm happy to take them now.

