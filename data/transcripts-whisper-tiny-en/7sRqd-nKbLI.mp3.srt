1
00:00:00,000 --> 00:00:04,080
Hello and welcome back to our study at the Dhamabanda.

2
00:00:04,080 --> 00:00:10,760
Tonight we continue on with verse number 77, which reads as follows.

3
00:00:10,760 --> 00:00:29,520
Over the Nusasayya, Asabajh and Niwariya, satanhis opiyo hote, asatan hote apiyo, which means

4
00:00:29,520 --> 00:00:47,080
instruct and teach and prevent or forbid in regards to what is harmful, it is Asabajh

5
00:00:47,080 --> 00:00:50,960
means they are not beautiful.

6
00:00:50,960 --> 00:00:56,840
If you do this, satanhis opiyo hote, such a person, in fact it's not saying you do

7
00:00:56,840 --> 00:01:05,760
it is a one should teach, one should preach, one should instruct, one should admonish, and

8
00:01:05,760 --> 00:01:13,520
one should forbid in regards to what is harmful and what is not beautiful.

9
00:01:13,520 --> 00:01:20,640
Not beautiful as a way of saying, not wholesome, not related to goodness.

10
00:01:20,640 --> 00:01:30,400
The one who does so will be pure, will be beloved, satan, of those who are mindful or

11
00:01:30,400 --> 00:01:33,800
of the good, maybe just means of the good.

12
00:01:33,800 --> 00:01:42,360
Asatan hote apiyo and they will be not beloved, unbeloved, they will be not loved, not

13
00:01:42,360 --> 00:01:49,000
like by the bad, by evil asatan.

14
00:01:49,000 --> 00:01:55,680
So this was actually quite an interesting verse and another one of these aspects of the

15
00:01:55,680 --> 00:02:02,880
Buddha's teaching that teaches its unique piece of the puzzle of the Buddha's teaching.

16
00:02:02,880 --> 00:02:08,960
But it was told in regards to two disciples, we are told of the two chief disciples,

17
00:02:08,960 --> 00:02:18,320
who are Sariput and Morganana, and even though they had ordained from the two chief disciples

18
00:02:18,320 --> 00:02:28,400
themselves, they were allergy, they were without shame, they were bad books.

19
00:02:28,400 --> 00:02:39,840
The Buddha said, even though a spoon, even though a spoon dwells in the soup, stays

20
00:02:39,840 --> 00:02:47,560
with the soup, we put a spoon in a cup of soup, we had this verse on it anyway.

21
00:02:47,560 --> 00:02:54,120
Even in the cup of soup it can be there for a hundred years, it will never taste the soup.

22
00:02:54,120 --> 00:03:02,520
And so it is with someone who is foolish and doesn't absorb the teachings of the wise,

23
00:03:02,520 --> 00:03:08,240
they can be surrounded by wise people, even be right next to Sariput and Morganana, and

24
00:03:08,240 --> 00:03:19,040
what we wouldn't do it to have that opportunity and waste the opportunity.

25
00:03:19,040 --> 00:03:21,440
They just wasted it.

26
00:03:21,440 --> 00:03:22,440
What did they waste it doing?

27
00:03:22,440 --> 00:03:29,920
Well, apparently they went off with their followers and planted flowers and flowering trees

28
00:03:29,920 --> 00:03:34,480
and many other things besides, but they did all sorts of things that are considered to

29
00:03:34,480 --> 00:03:39,680
be waste of time and more over harmful to one's spiritual development because they

30
00:03:39,680 --> 00:03:47,880
cultivate this sense of desire and attachment and passion and so on, and yes, planting

31
00:03:47,880 --> 00:03:50,880
flowers does create attachment.

32
00:03:50,880 --> 00:03:57,400
Our spirituality goes higher than beauty, it goes beyond beauty, it goes to the ultimate,

33
00:03:57,400 --> 00:04:03,960
the highest, which is objectivity, which is this universal disability to understand

34
00:04:03,960 --> 00:04:09,800
the universe, which is quite beautiful in itself, but it has nothing to do with the appreciation

35
00:04:09,800 --> 00:04:12,280
of the smell of the flowers.

36
00:04:12,280 --> 00:04:25,200
So they went off and did this and word came back to the Buddha because they were acting

37
00:04:25,200 --> 00:04:29,760
inappropriately and they were probably doing things to ingratiate themselves with the

38
00:04:29,760 --> 00:04:36,400
laypeople, so it made it hard for proper monks to live there because of course ordinary

39
00:04:36,400 --> 00:04:41,680
people who were probably not well versed in the Buddha's teaching, they would appreciate

40
00:04:41,680 --> 00:04:45,760
the monks would pick flowers and bring them to the laypeople and people like, oh, look

41
00:04:45,760 --> 00:04:49,400
at these people bringing us flowers, that must be what monks do.

42
00:04:49,400 --> 00:04:53,840
So when the monks came with their downcast eyes, they would say, who are these guys?

43
00:04:53,840 --> 00:04:58,200
They don't have flowers, they aren't fun, they aren't singing and dancing and playing

44
00:04:58,200 --> 00:05:06,920
with our children and talking to us about the weather and so on and so on.

45
00:05:06,920 --> 00:05:10,760
And they wouldn't run errands, the monks would run errands for them because the monks

46
00:05:10,760 --> 00:05:15,880
didn't have jobs and that's a big no-no, for very much the same reason, well for many

47
00:05:15,880 --> 00:05:23,040
reasons, but one because the monks were practicing meditation that are expected to run

48
00:05:23,040 --> 00:05:25,200
errands for the laypeople.

49
00:05:25,200 --> 00:05:30,680
And there's a sense that you have to work for the food that people give you, I'm giving

50
00:05:30,680 --> 00:05:35,240
you this food, you better send this letter to this person or do this for me or do that

51
00:05:35,240 --> 00:05:36,240
for me.

52
00:05:36,240 --> 00:05:46,400
And then when are they going to practice meditation or study the Buddha's teachings and so on?

53
00:05:46,400 --> 00:05:49,960
So they did all these things that made it difficult for other monks to live there because

54
00:05:49,960 --> 00:05:54,720
they just couldn't compete with this and people weren't well versed enough in the

55
00:05:54,720 --> 00:05:59,160
Buddha's teaching to understand that it wasn't really of any benefit to have these monks

56
00:05:59,160 --> 00:06:04,480
running errands for them to bring them flowers.

57
00:06:04,480 --> 00:06:09,280
But the Buddha called, sorry put them in Mogulana, he heard about this and called them

58
00:06:09,280 --> 00:06:12,360
up and he said, go teach them.

59
00:06:12,360 --> 00:06:18,840
And if they don't listen to you, kick them up, expel them from the order.

60
00:06:18,840 --> 00:06:26,760
And if they do listen to you, then bring them back and teach them because often it's

61
00:06:26,760 --> 00:06:28,920
just because of a poor leader.

62
00:06:28,920 --> 00:06:38,600
And if you get the wrong person following you follow the wrong person, see, you can imagine

63
00:06:38,600 --> 00:06:43,480
they had like 500 followers each I think and you can imagine how that happens.

64
00:06:43,480 --> 00:06:48,200
Hey, I'm a student of, oh, yes, I'm a student of, sorry, Buddha.

65
00:06:48,200 --> 00:06:53,760
I'm a student of Mogulana and I'm like, oh, wow, you must be really good meditator.

66
00:06:53,760 --> 00:06:55,640
You must be a great teacher.

67
00:06:55,640 --> 00:06:58,920
Let's follow you.

68
00:06:58,920 --> 00:07:01,560
It makes me think of in modern days, we get this as well.

69
00:07:01,560 --> 00:07:07,280
I'm a student of Agentong, Siri, Mungalo and people, oh, yes, you must be a great monk

70
00:07:07,280 --> 00:07:11,200
because he's a great teacher, but it doesn't always hold true.

71
00:07:11,200 --> 00:07:17,280
I think sometimes people will use this to their advantage, they'll be, they'll use a

72
00:07:17,280 --> 00:07:23,160
teacher's name and they find this in spirituality, especially Buddhism, it's quite common.

73
00:07:23,160 --> 00:07:29,960
I'm a student of the Dalai Lama, who you must be, well, doesn't mean anything, really.

74
00:07:29,960 --> 00:07:36,840
Agentong doesn't mean many, many, many students have varying degrees of enlightenment.

75
00:07:36,840 --> 00:07:42,680
So don't go by such things, but you have to at least take such things with a grain of salt

76
00:07:42,680 --> 00:07:47,560
because it may tell you their tradition, but it probably doesn't tell you their spiritual

77
00:07:47,560 --> 00:07:50,000
level of spirituality.

78
00:07:50,000 --> 00:07:57,320
It may, if they're bragging about it a lot, it may show that not a very high spirituality.

79
00:07:57,320 --> 00:08:02,760
But at any rate, so there were many, many monks following these two corrupt monks and many

80
00:08:02,760 --> 00:08:06,000
of them would have just been misled, like the lay people into thinking that that cell

81
00:08:06,000 --> 00:08:08,360
monks should have.

82
00:08:08,360 --> 00:08:14,120
And so the two chief disciples were instructed to go and they went to see and did exactly

83
00:08:14,120 --> 00:08:22,720
that before they went, they would explain to them why or how they should treat this situation.

84
00:08:22,720 --> 00:08:42,480
I'm going to graduate up and up.

85
00:08:42,480 --> 00:08:47,920
basically what the Tama Padha quote says, it says, those who are not wise, those who are

86
00:08:47,920 --> 00:08:56,400
not Pandita, you will become unbeloved to them, and undeered to them, a Manapo, unpleasing.

87
00:08:56,400 --> 00:09:04,760
It will be displeasing to them, but Pandita and Anapana, but for the wise ones, you will

88
00:09:04,760 --> 00:09:15,280
be holding Manapo, you will be dear, and one will be dear, one will be pleasing to them,

89
00:09:15,280 --> 00:09:17,680
and then he told his verse.

90
00:09:17,680 --> 00:09:20,720
So that's the story, and then they went, and some of them listened, some of them didn't,

91
00:09:20,720 --> 00:09:27,400
some of them left them the monastic life thinking, what, you got a meditate, you got to

92
00:09:27,400 --> 00:09:31,600
keep all these rules, that's not what I signed up for, they left, and some of them were

93
00:09:31,600 --> 00:09:36,080
expelled.

94
00:09:36,080 --> 00:09:38,320
So how does this relate to our practice?

95
00:09:38,320 --> 00:09:43,520
Well, the first interesting aspect is in regards to what is proper practice and what

96
00:09:43,520 --> 00:09:47,760
is not.

97
00:09:47,760 --> 00:09:55,160
Ambudism is a very, fairly, it is a straight and narrow path, there's not a lot of leeway,

98
00:09:55,160 --> 00:09:56,600
not a lot of wiggle room.

99
00:09:56,600 --> 00:10:02,800
Like, you can't somehow pretend that planting flowers is part of the dumber, though people

100
00:10:02,800 --> 00:10:06,200
I think try to.

101
00:10:06,200 --> 00:10:09,240
Listening to music, these kind of things, I mean, that's very straight, and there are

102
00:10:09,240 --> 00:10:16,320
most spirituality, includes music, includes beauty, art, these kind of things, but not

103
00:10:16,320 --> 00:10:20,880
the Buddhist path, because we're going so much higher than that, it's like a science

104
00:10:20,880 --> 00:10:26,560
really, which I think will probably turn some people off, but that's a, that's a,

105
00:10:26,560 --> 00:10:33,280
the way this quote, what this quote shows us, you know, a satan, he, a satan hoat, the

106
00:10:33,280 --> 00:10:38,760
up, you know, it will not be beloved, it will not be pleasing to those who are not mindful

107
00:10:38,760 --> 00:10:50,400
or not, head in towards true, the truth, because we require an absolute objectivity.

108
00:10:50,400 --> 00:10:54,680
In order to understand the truth, you have to be strict with yourself, you have to be

109
00:10:54,680 --> 00:11:01,280
clear that you're going to be objective, a flower can be no more, of no more value to

110
00:11:01,280 --> 00:11:05,680
you than a pile of dumber, because you want to understand the truth of them.

111
00:11:05,680 --> 00:11:11,880
If you turn your nose up at the pile of dung and stick your nose in the flower, forget

112
00:11:11,880 --> 00:11:14,680
about being objective, forget about understanding the truth.

113
00:11:14,680 --> 00:11:15,680
It's not possible.

114
00:11:15,680 --> 00:11:22,080
Same goes with music, music and the sound of nails on the chalkboard have to be an equal

115
00:11:22,080 --> 00:11:28,080
value, you can't value, you can't place value in such a sense, because that's subjective,

116
00:11:28,080 --> 00:11:34,480
it's not objective, it doesn't, it forbids you, prevents you from seeing the truth.

117
00:11:34,480 --> 00:11:39,320
But those who understand this, those who are wise and are really keen on, on truth and

118
00:11:39,320 --> 00:11:44,680
understanding, they will appreciate, they will appreciate such things, they'll be interested

119
00:11:44,680 --> 00:11:47,680
in such things.

120
00:11:47,680 --> 00:11:54,640
That's important, it's important thing to make known at the get-go, so people don't have

121
00:11:54,640 --> 00:12:01,600
these wrong ideas and end up bad nothing or end up being disappointed and being upset

122
00:12:01,600 --> 00:12:08,480
that they were tricked into being objective.

123
00:12:08,480 --> 00:12:13,800
But the other thing is in regards to instruction, and it's something that we have to

124
00:12:13,800 --> 00:12:19,640
remember ourselves, sometimes when we don't like the instruction, it's not the instructor's

125
00:12:19,640 --> 00:12:26,360
fault, sometimes it is, sometimes the instructor is teaching that which is harmful.

126
00:12:26,360 --> 00:12:32,080
Of course this wouldn't have been the case with Sarriput and Woldenana, but it can be

127
00:12:32,080 --> 00:12:36,760
that a teacher can teach something that's harmful, which is what's wrong with this idea

128
00:12:36,760 --> 00:12:42,520
that all teachers are equal, or all teachings are, sorry, all teachings are equal, so people

129
00:12:42,520 --> 00:12:48,920
will say, all religions teach the same thing, which is not really true, because our religion

130
00:12:48,920 --> 00:12:54,280
could easily be made up that teaches terrible things, same as with teachers.

131
00:12:54,280 --> 00:13:03,720
But this quote, so it's kind of, it's unspoken here, but the opposite holds true that

132
00:13:03,720 --> 00:13:09,880
that which is harmful, that which leads one the way from the truth, leads one to delusion

133
00:13:09,880 --> 00:13:16,560
would be quite pleasing to those who are asetam, those who are not on a good path.

134
00:13:16,560 --> 00:13:23,960
And it would be pleasing, it would be displeasing to those who are on the right path.

135
00:13:23,960 --> 00:13:31,120
And so, how do I know what's right then?

136
00:13:31,120 --> 00:13:36,120
This is the danger of just taking things based on your likes and dislikes, the whole

137
00:13:36,120 --> 00:13:41,920
idea of follow your heart, that we often ascribe to in the West, follow your heart,

138
00:13:41,920 --> 00:13:49,880
or listen to your gut feeling, there's no reason to listen to your stomach or your intestines

139
00:13:49,880 --> 00:13:53,320
for that matter.

140
00:13:53,320 --> 00:14:00,200
Or your heart for that matter, and it's interesting, the same saying exists in Asia and

141
00:14:00,200 --> 00:14:06,600
Thailand, it's damn jai, damn jai means follow your heart, but the funny thing is, damn jai

142
00:14:06,600 --> 00:14:13,720
is usually considered to be a criticism, no, and then damn jai does not, or it can be used

143
00:14:13,720 --> 00:14:19,160
to say, oh that person just follows their heart all the time, as an insult, not an insult

144
00:14:19,160 --> 00:14:25,360
criticism, because they don't use wisdom, they don't use mindfulness or discernment, they

145
00:14:25,360 --> 00:14:30,120
just follow their heart, which we think, oh that's a good thing, follow your heart.

146
00:14:30,120 --> 00:14:42,840
Not a good thing, or they'll say, if they just follow the hearts of their children,

147
00:14:42,840 --> 00:14:46,400
which is also a problem, from a parent, a point of view, you can't just follow the

148
00:14:46,400 --> 00:14:51,640
hearts of your children, heart here means emotional or the mind or the intention of

149
00:14:51,640 --> 00:14:52,640
the person.

150
00:14:52,640 --> 00:14:56,920
So you can't just follow your intentions, you have to use discernment, in fact you shouldn't

151
00:14:56,920 --> 00:15:01,760
trust yourself, we've gotten ourselves into this mess, any suffering you have, any mental

152
00:15:01,760 --> 00:15:09,000
problems, any bad habits, you have addictions, what do you did it to yourself, why would

153
00:15:09,000 --> 00:15:13,760
you trust your mind, why would you listen to your mind, the untrained mind is your worst

154
00:15:13,760 --> 00:15:22,920
enemy, what is that, plus that is that the trained mind is your greatest friend, but

155
00:15:22,920 --> 00:15:26,640
that's the truth is you have to train your mind.

156
00:15:26,640 --> 00:15:31,400
And this verse doesn't give us indication of any indication of how to know what is the

157
00:15:31,400 --> 00:15:37,880
truth and what is not, it's not talking about that, it's talking about specifically it's

158
00:15:37,880 --> 00:15:41,880
talking about when you know the truth, don't be afraid to say it, and don't be worried

159
00:15:41,880 --> 00:15:46,320
that people are going to be upset, that's the third thing that this does, but before

160
00:15:46,320 --> 00:15:52,080
we get to that, just talking about, it tells you not to follow your heart, not to follow

161
00:15:52,080 --> 00:15:56,560
what you like and dislike, that's what I wanted to point out, that's the second point,

162
00:15:56,560 --> 00:16:03,000
is that you have to follow your wisdom, you have to follow knowledge, you can't circumvent

163
00:16:03,000 --> 00:16:07,240
that, there's no gut feeling it's going to tell you it's right or not, you have to

164
00:16:07,240 --> 00:16:12,880
see it clearly for yourself, and until you see it clearly, you're very distrustful of

165
00:16:12,880 --> 00:16:19,640
your heart and your mind, because they will trick you, they're not yours, they're based

166
00:16:19,640 --> 00:16:27,920
on causes and effects that are complex, sometimes helpful, sometimes harmful, sometimes

167
00:16:27,920 --> 00:16:33,480
helpful, but helpful in leading you towards the dead end, so it seems, oh, this is working

168
00:16:33,480 --> 00:16:40,880
just fine and then you smash up against them all, bricks, metaphorically speaking.

169
00:16:40,880 --> 00:16:46,120
But the third point is when you do know the truth, which is what the Buddha is really

170
00:16:46,120 --> 00:16:52,200
saying, oh, dayyana sasaya, teach, instruct, don't be afraid, just because people don't

171
00:16:52,200 --> 00:16:59,080
like it, but the Buddha often said, don't teach, if you know the person is not going

172
00:16:59,080 --> 00:17:03,320
to listen, sometimes you have to be careful if you're just going to upset them, but I

173
00:17:03,320 --> 00:17:07,720
think the point here is that there was a whole group of monks and it was like what you

174
00:17:07,720 --> 00:17:17,200
call a triage, a triage in medicine, you know, don't worry too much about those who

175
00:17:17,200 --> 00:17:21,960
can't say, don't worry about those who can't say, and if you don't do anything, all

176
00:17:21,960 --> 00:17:26,600
these monks are lost, all these meditators are lost, if you do something, well, some of

177
00:17:26,600 --> 00:17:33,640
them will listen, so think about those ones, and the rest you just have to let go, and so

178
00:17:33,640 --> 00:17:38,080
the Buddha told them, in this specific case, to go and teach all these people, it doesn't

179
00:17:38,080 --> 00:17:41,000
mean go out and preach to people and then when they get angry, say, oh, that's your

180
00:17:41,000 --> 00:17:46,280
fault, you're just a useless person, no, there's a time and a place to teach, and if you're

181
00:17:46,280 --> 00:17:52,160
teaching and people aren't getting anything out of it, then you are at fault, as a teacher,

182
00:17:52,160 --> 00:17:56,360
it means either you're teaching is terrible, or it means you're teaching it, you don't

183
00:17:56,360 --> 00:18:04,040
understand yet how to teach, who to teach, when to teach, so don't always just go and teach,

184
00:18:04,040 --> 00:18:10,240
but it means don't be afraid, and if a person doesn't understand the teaching, don't

185
00:18:10,240 --> 00:18:17,680
give up the teaching, just understand that, well, I'm not perfect, so I can't know exactly

186
00:18:17,680 --> 00:18:22,600
who's ready to understand the teaching, obviously that person wasn't, so it doesn't mean

187
00:18:22,600 --> 00:18:28,360
I shouldn't give up, there's no reason to think that the teaching is at fault, or the

188
00:18:28,360 --> 00:18:33,000
truth is any different, it doesn't mean it's not true, it just means that person isn't

189
00:18:33,000 --> 00:18:39,400
ready for it, potentially, but again, you have to be sure, you can't just say, well, I think

190
00:18:39,400 --> 00:18:49,160
this is right, and this is my teaching, and therefore they all can get lost if they don't

191
00:18:49,160 --> 00:18:55,160
like that, no, don't follow your heart, follow the truth, so interesting teaching quite useful

192
00:18:55,160 --> 00:19:04,360
for meditators, and one more verse, so thank you all for tuning in, that's what I teach

193
00:19:04,360 --> 00:19:34,200
you today, keep practicing and be well.

