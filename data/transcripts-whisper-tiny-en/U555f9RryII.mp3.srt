1
00:00:00,000 --> 00:00:02,840
So you start recording, and you start.

2
00:00:02,840 --> 00:00:06,400
Basically, what we want to know is what you feel

3
00:00:06,400 --> 00:00:08,680
you gain from the practice.

4
00:00:08,680 --> 00:00:12,560
What's different now that you finish the course

5
00:00:12,560 --> 00:00:15,520
from your feeling before you get started the course,

6
00:00:15,520 --> 00:00:18,760
or before you started the course?

7
00:00:18,760 --> 00:00:26,760
Well, a lot of things changed, actually.

8
00:00:26,760 --> 00:00:28,720
You know, before I came here, well, actually,

9
00:00:28,720 --> 00:00:31,680
the reason is why I came here is because I was usually

10
00:00:31,680 --> 00:00:34,560
feeling anxious.

11
00:00:34,560 --> 00:00:37,760
And it wasn't sort of like strong.

12
00:00:37,760 --> 00:00:42,080
And I knew that there was a subtle sense of underlying anxiety

13
00:00:42,080 --> 00:00:43,200
inside of me.

14
00:00:43,200 --> 00:00:48,240
And there are bouts of feelings of anger and depression.

15
00:00:48,240 --> 00:00:49,760
You know, things that people usually feel.

16
00:00:49,760 --> 00:00:51,240
But I wasn't really OK with it.

17
00:00:51,240 --> 00:00:56,760
I realized that I wasn't going to live a life

18
00:00:56,760 --> 00:00:59,640
where I just had to cope with things constantly.

19
00:00:59,640 --> 00:01:02,960
I wanted to be not to cope with those things.

20
00:01:02,960 --> 00:01:07,600
I wanted to live and coexist peacefully with my emotions.

21
00:01:07,600 --> 00:01:11,360
I didn't want to view them as if I was against them,

22
00:01:11,360 --> 00:01:14,000
or as if there were some sort of enemy.

23
00:01:19,840 --> 00:01:21,000
I came here to meditate.

24
00:01:21,000 --> 00:01:26,400
And one of the things that one of the major things

25
00:01:26,400 --> 00:01:29,840
that changed me was that the fact that I couldn't do anything,

26
00:01:29,840 --> 00:01:32,400
which is basically, that was the hardest thing

27
00:01:32,400 --> 00:01:35,000
that I have ever done around me speaking,

28
00:01:35,000 --> 00:01:37,040
that I had to do nothing.

29
00:01:37,040 --> 00:01:39,040
Because at home, you have all these things

30
00:01:39,040 --> 00:01:42,000
that you can do.

31
00:01:42,000 --> 00:01:45,280
You go to school, you go to work, you do volunteer,

32
00:01:45,280 --> 00:01:50,120
you hang out with friends, at home, you have your computer.

33
00:01:50,120 --> 00:01:53,920
And basically, that takes away everything.

34
00:01:53,920 --> 00:01:58,080
And you have all these sort of distractions.

35
00:01:58,080 --> 00:02:01,720
And so when you have emotional problems,

36
00:02:01,720 --> 00:02:03,840
you don't really have the time or have the attention

37
00:02:03,840 --> 00:02:08,960
to really scrutinize them in a way, to really confront them,

38
00:02:08,960 --> 00:02:12,120
and see what they really are in their truest nature.

39
00:02:12,120 --> 00:02:14,440
And so when you come here, that's

40
00:02:14,440 --> 00:02:17,840
the reason why it's so difficult for me to do nothing.

41
00:02:17,840 --> 00:02:23,960
Is that I have all these emotions, all of these negative thoughts

42
00:02:23,960 --> 00:02:27,920
that I never thought, I never knew that they were so serious.

43
00:02:27,920 --> 00:02:34,520
I never knew that they were that extreme, in a sense.

44
00:02:34,520 --> 00:02:41,000
And so you spend time here, and sometimes they come up,

45
00:02:41,000 --> 00:02:43,600
sometimes they go.

46
00:02:43,600 --> 00:02:47,800
But the thing you realize is that you start to adapt

47
00:02:47,800 --> 00:02:49,200
to this certain kind of environment.

48
00:02:49,200 --> 00:02:53,920
You adapt to the fact that since that there's nothing

49
00:02:53,920 --> 00:02:57,080
you can do, you might as well be OK with it.

50
00:02:57,080 --> 00:03:00,760
I think that's the biggest thing that I learned here,

51
00:03:00,760 --> 00:03:05,640
is that once you have absolutely no distractions,

52
00:03:05,640 --> 00:03:10,160
nothing you take you away from, what's

53
00:03:10,160 --> 00:03:14,520
the raw form of yourself.

54
00:03:14,520 --> 00:03:19,320
And you really start to confront who you are as a person.

55
00:03:19,320 --> 00:03:23,200
You start to realize there's nothing really bad about it.

56
00:03:23,200 --> 00:03:27,120
I realized I don't know why I was running away from them.

57
00:03:27,120 --> 00:03:33,760
I don't know why I was trying to distract myself,

58
00:03:33,760 --> 00:03:36,480
clinging on to positive thoughts,

59
00:03:36,480 --> 00:03:39,600
clinging on to positive emotions and experiences

60
00:03:39,600 --> 00:03:43,360
so that I don't have to experience those negative ones.

61
00:03:43,360 --> 00:03:49,320
Since I came here, I realized in meditation,

62
00:03:49,320 --> 00:03:54,280
I've sat through some of my worst emotions and thoughts ever.

63
00:03:54,280 --> 00:03:59,360
Just feelings of shame and guilt and sadness and anger

64
00:03:59,360 --> 00:04:01,640
and extreme panic attacks.

65
00:04:01,640 --> 00:04:08,880
I wanted to just punch a wall or do something.

66
00:04:08,880 --> 00:04:11,160
Many times, I wanted to run away to home

67
00:04:11,160 --> 00:04:14,680
and just distract myself again.

68
00:04:14,680 --> 00:04:19,120
But you can do that, and that's one of the best things.

69
00:04:19,120 --> 00:04:20,960
But you sit through them eventually.

70
00:04:20,960 --> 00:04:24,760
And you realized that I'm still here,

71
00:04:24,760 --> 00:04:30,240
that despite the suffering that you go through,

72
00:04:30,240 --> 00:04:32,440
despite the discomfort and negativity

73
00:04:32,440 --> 00:04:36,800
that you experience, you're still here.

74
00:04:36,800 --> 00:04:39,360
And you take a look around, you're reality is still here.

75
00:04:39,360 --> 00:04:41,560
Reality hasn't changed.

76
00:04:41,560 --> 00:04:43,880
All that has changed is what's inside of you.

77
00:04:43,880 --> 00:04:47,920
Inside of you, it constantly fluctuates.

78
00:04:47,920 --> 00:04:53,560
And so you learn to be mindful.

79
00:04:53,560 --> 00:04:58,240
And you learn to go, really.

80
00:04:58,240 --> 00:05:02,160
You realize that your emotions and your thoughts

81
00:05:02,160 --> 00:05:07,760
are not really there, they're not really that harmful.

82
00:05:07,760 --> 00:05:12,040
Whether they're good or not, they're just natural.

83
00:05:12,040 --> 00:05:13,600
They're like nature.

84
00:05:13,600 --> 00:05:16,760
They're like the weather as I was thinking the other day.

85
00:05:16,760 --> 00:05:19,280
Sometimes we range, sometimes it doesn't.

86
00:05:19,280 --> 00:05:21,600
It doesn't mean that you have to get upset every single time

87
00:05:21,600 --> 00:05:22,640
you're in a ring.

88
00:05:22,640 --> 00:05:24,840
You can just go outside and say, oh, it's raining.

89
00:05:24,840 --> 00:05:25,340
So what?

90
00:05:25,340 --> 00:05:27,040
The wall might be sunny or not.

91
00:05:27,040 --> 00:05:27,520
It doesn't matter.

92
00:05:27,520 --> 00:05:29,920
You just enjoy the moment.

93
00:05:29,920 --> 00:05:36,960
And so that's one of the biggest things that I've ever learned.

94
00:05:36,960 --> 00:05:42,680
And it made me have this sense of fearlessness

95
00:05:42,680 --> 00:05:49,200
that I could go into several situations

96
00:05:49,200 --> 00:05:51,560
that I wasn't able to before, because I

97
00:05:51,560 --> 00:05:54,880
was afraid of the thoughts and emotions

98
00:05:54,880 --> 00:05:57,160
that would generate.

99
00:05:57,160 --> 00:06:00,160
I wasn't those experiences and situations.

100
00:06:00,160 --> 00:06:03,600
But now, since I've spent so many days realizing the fact

101
00:06:03,600 --> 00:06:06,200
that my emotions fluctuate and the worst way

102
00:06:06,200 --> 00:06:09,360
to deal with them is to cling on to positive experiences

103
00:06:09,360 --> 00:06:13,560
and to avoid negative ones, because ultimately,

104
00:06:13,560 --> 00:06:14,960
you can't really control them.

105
00:06:14,960 --> 00:06:16,360
You can't control your thoughts.

106
00:06:16,360 --> 00:06:18,920
You can't control your emotions.

107
00:06:18,920 --> 00:06:23,400
They're just a part of whatever's happening.

108
00:06:23,400 --> 00:06:25,040
I don't know whatever really is happening.

109
00:06:25,040 --> 00:06:28,760
Sometimes I'm moody.

110
00:06:28,760 --> 00:06:30,000
Sometimes I'm not.

111
00:06:30,000 --> 00:06:31,920
Sometimes I feel anxiety.

112
00:06:31,920 --> 00:06:32,680
Sometimes I'm not.

113
00:06:32,680 --> 00:06:36,280
But the content of those things, it's irrelevant.

114
00:06:36,280 --> 00:06:39,000
That's what I've learned.

115
00:06:39,000 --> 00:06:44,840
You start to realize there are certain actions that you make.

116
00:06:44,840 --> 00:06:49,920
But the content of those things, there's

117
00:06:49,920 --> 00:06:53,480
sort of an illusion that you create for yourself.

118
00:06:53,480 --> 00:06:58,040
Everyone has stories and histories and dramas

119
00:06:58,040 --> 00:07:00,000
about their lives.

120
00:07:00,000 --> 00:07:02,840
But they're not part of reality.

121
00:07:02,840 --> 00:07:04,560
That's what I've learned.

122
00:07:08,960 --> 00:07:13,200
It's about, it's like when I was saying that it fluctuates.

123
00:07:13,200 --> 00:07:15,480
Whatever's anxiety fluctuates, it changes.

124
00:07:15,480 --> 00:07:18,160
But whatever's outside, it's still there.

125
00:07:18,160 --> 00:07:24,600
And you realize that, and as you become more mindful,

126
00:07:24,600 --> 00:07:29,280
you start to realize that, oh, I'm just thinking.

127
00:07:29,280 --> 00:07:31,600
I'm just feeling things.

128
00:07:31,600 --> 00:07:35,640
It doesn't really matter.

129
00:07:35,640 --> 00:07:38,360
Well, it matters because you want to sort of gravitate

130
00:07:38,360 --> 00:07:39,920
towards the peacefulness.

131
00:07:39,920 --> 00:07:45,960
But you do so in a way that you just naturally gravitate

132
00:07:45,960 --> 00:07:47,800
towards peacefulness.

133
00:07:47,800 --> 00:07:53,400
As you don't become involved in the thoughts

134
00:07:53,400 --> 00:07:56,480
and emotions, especially the contents of them,

135
00:07:56,480 --> 00:07:57,440
and the details of them.

136
00:08:01,120 --> 00:08:06,600
And another thing that I learned here

137
00:08:06,600 --> 00:08:13,760
was that before I arrived here, I was thinking to myself,

138
00:08:13,760 --> 00:08:15,320
while I'm going on a spiritual journey

139
00:08:15,320 --> 00:08:19,640
to achieve happiness, to get it.

140
00:08:19,640 --> 00:08:21,440
I'm so proud of myself.

141
00:08:21,440 --> 00:08:22,240
I wanted to do so.

142
00:08:22,240 --> 00:08:23,080
And I came here.

143
00:08:23,080 --> 00:08:26,120
And that's one of the biggest hindances

144
00:08:26,120 --> 00:08:29,920
that blog my path for getting, or not getting,

145
00:08:29,920 --> 00:08:33,680
but just simply being with happiness, being with peacefulness,

146
00:08:33,680 --> 00:08:37,840
is that I wanted to obtain it.

147
00:08:37,840 --> 00:08:41,760
I wanted, it's like an item.

148
00:08:41,760 --> 00:08:44,920
It's like, I have my education out, my family.

149
00:08:44,920 --> 00:08:46,640
I have my happiness.

150
00:08:46,640 --> 00:08:48,920
That's how I viewed it.

151
00:08:48,920 --> 00:08:52,000
And it doesn't work out that way.

152
00:08:52,000 --> 00:08:54,200
It never works out that way.

153
00:08:54,200 --> 00:09:00,280
And happiness is not the, I don't think,

154
00:09:00,280 --> 00:09:02,920
once you get on the pursuit of happiness,

155
00:09:02,920 --> 00:09:07,280
happiness is not in your region anymore.

156
00:09:07,280 --> 00:09:14,880
I think it's once you start to begin to be open to your suffering.

157
00:09:14,880 --> 00:09:18,880
When you become open to your negativity,

158
00:09:18,880 --> 00:09:20,880
that when you're not afraid anymore,

159
00:09:20,880 --> 00:09:24,880
we're not trying to avoid these kind of situations

160
00:09:24,880 --> 00:09:29,880
and only try to get these ones, like happiness.

161
00:09:29,880 --> 00:09:35,880
It's when you embrace all sorts of experiences.

162
00:09:35,880 --> 00:09:39,880
It's when you realize that all experiences

163
00:09:39,880 --> 00:09:41,880
are just basically experiences.

164
00:09:41,880 --> 00:09:43,880
They're basically phenomena.

165
00:09:43,880 --> 00:09:49,880
And then that's when you experience a sense of peacefulness.

166
00:09:49,880 --> 00:09:54,880
That's when you, because when I was trying to meditate,

167
00:09:54,880 --> 00:10:00,880
I was always telling myself, you have to get to a state,

168
00:10:00,880 --> 00:10:03,880
a specific meditative state.

169
00:10:03,880 --> 00:10:05,880
You have to do that.

170
00:10:05,880 --> 00:10:07,880
Then you can experience happiness.

171
00:10:07,880 --> 00:10:10,880
Because then, if you don't get into meditative state,

172
00:10:10,880 --> 00:10:11,880
you don't get wisdom.

173
00:10:11,880 --> 00:10:14,880
If you don't realize things that you came here,

174
00:10:14,880 --> 00:10:16,880
you wasted your time.

175
00:10:16,880 --> 00:10:18,880
And that was an obstacle.

176
00:10:18,880 --> 00:10:21,880
That's why I couldn't do it for so long.

177
00:10:21,880 --> 00:10:27,880
And it's when I realize that I can't control everything anymore.

178
00:10:27,880 --> 00:10:29,880
I can't dominate.

179
00:10:29,880 --> 00:10:35,880
I just have to be that you start to realize

180
00:10:35,880 --> 00:10:39,880
that peacefulness just arises naturally.

181
00:10:39,880 --> 00:10:41,880
That you're okay.

182
00:10:41,880 --> 00:10:43,880
That even if you feel anxiety,

183
00:10:43,880 --> 00:10:45,880
even if you feel anger, sadness,

184
00:10:45,880 --> 00:10:48,880
there's a sense of peacefulness in terms of,

185
00:10:48,880 --> 00:10:50,880
I don't know how to describe it,

186
00:10:50,880 --> 00:10:52,880
but it's like you're okay.

187
00:10:52,880 --> 00:10:54,880
And you're not,

188
00:10:54,880 --> 00:10:59,880
it's also like a form of detachment from your emotions.

189
00:10:59,880 --> 00:11:00,880
You just feel it.

190
00:11:00,880 --> 00:11:08,880
But you're not actually there to be resistant of it.

191
00:11:08,880 --> 00:11:11,880
Except it.

192
00:11:11,880 --> 00:11:15,880
Except it's like you accept a friend who's sad.

193
00:11:15,880 --> 00:11:18,880
You don't try to push it or push it away.

194
00:11:18,880 --> 00:11:20,880
You accept another person.

195
00:11:20,880 --> 00:11:25,880
And I realize it's always easier to accept someone else

196
00:11:25,880 --> 00:11:27,880
to accept yourself.

197
00:11:27,880 --> 00:11:29,880
So when you become mindful,

198
00:11:29,880 --> 00:11:33,880
you view your emotions as something that's not part of yourself.

199
00:11:33,880 --> 00:11:37,880
And I think that's why it's so easy to accept them.

200
00:11:37,880 --> 00:11:40,880
It's because once they're not part of yourself,

201
00:11:40,880 --> 00:11:42,880
you can just see them for who they are.

202
00:11:42,880 --> 00:11:45,880
You can give compassion to them,

203
00:11:45,880 --> 00:11:47,880
embrace them, be open to them, whatever,

204
00:11:47,880 --> 00:11:54,880
but it's much easier to just let go of that personal connection.

205
00:11:54,880 --> 00:11:56,880
But just to realize that they're just natural.

206
00:11:56,880 --> 00:11:57,880
I mean, we're all human beings.

207
00:11:57,880 --> 00:11:59,880
We experience emotions.

208
00:11:59,880 --> 00:12:02,880
You can't be peaceful all the time.

209
00:12:02,880 --> 00:12:05,880
And once you accept that,

210
00:12:05,880 --> 00:12:08,880
you can't be peaceful.

211
00:12:08,880 --> 00:12:13,880
I think that's the irony behind that.

212
00:12:13,880 --> 00:12:17,880
But it took me a lot of patience,

213
00:12:17,880 --> 00:12:19,880
which is another thing that I learned.

214
00:12:19,880 --> 00:12:21,880
A lot of patience.

215
00:12:21,880 --> 00:12:23,880
You know, in the first few days,

216
00:12:23,880 --> 00:12:24,880
you tie down a little,

217
00:12:24,880 --> 00:12:26,880
tell me that patience is your best friend.

218
00:12:26,880 --> 00:12:29,880
Apparently, I was too impatient to listen to his advice.

219
00:12:29,880 --> 00:12:31,880
But over time, there's nothing you can do.

220
00:12:31,880 --> 00:12:33,880
You have to be patient.

221
00:12:33,880 --> 00:12:36,880
And to be honest,

222
00:12:36,880 --> 00:12:38,880
the conditions here are not the best conditions.

223
00:12:38,880 --> 00:12:40,880
I was living in a cave.

224
00:12:40,880 --> 00:12:45,880
Lots of mosquitoes.

225
00:12:45,880 --> 00:12:47,880
Sounds like I'm whining,

226
00:12:47,880 --> 00:12:48,880
which I am,

227
00:12:48,880 --> 00:12:52,880
but because I was brought up in modern,

228
00:12:52,880 --> 00:12:54,880
more developed countries,

229
00:12:54,880 --> 00:12:55,880
so I was not used to it.

230
00:12:55,880 --> 00:12:58,880
But it's because of those annoyances

231
00:12:58,880 --> 00:13:01,880
that you start to adapt to certain situations

232
00:13:01,880 --> 00:13:04,880
that you force yourself to be more patient.

233
00:13:04,880 --> 00:13:06,880
Otherwise, you can't survive.

234
00:13:06,880 --> 00:13:09,880
You know, I think naturally your mind

235
00:13:09,880 --> 00:13:15,880
is sort of, it tries to be more peaceful.

236
00:13:15,880 --> 00:13:19,880
And, you know, in whatever situation,

237
00:13:19,880 --> 00:13:22,880
because that's the way that you have to survive.

238
00:13:22,880 --> 00:13:25,880
And so as a result,

239
00:13:25,880 --> 00:13:29,880
you just learn to be okay.

240
00:13:29,880 --> 00:13:32,880
You learn to not to slap yourself in the face

241
00:13:32,880 --> 00:13:34,880
over and over and over again.

242
00:13:34,880 --> 00:13:37,880
Because you can see it clearly.

243
00:13:37,880 --> 00:13:39,880
And you didn't realize that before,

244
00:13:39,880 --> 00:13:42,880
because you never had the time to observe it.

245
00:13:42,880 --> 00:13:45,880
But now you have the time to observe it,

246
00:13:45,880 --> 00:13:48,880
to actually sit down and train your mind

247
00:13:48,880 --> 00:13:49,880
and sharpen your mind

248
00:13:49,880 --> 00:13:52,880
and let you make your mind see things more clearly.

249
00:13:52,880 --> 00:13:55,880
You start to see

250
00:13:55,880 --> 00:13:58,880
links between thoughts and emotions

251
00:13:58,880 --> 00:14:02,880
and between things that's going on internally.

252
00:14:02,880 --> 00:14:05,880
And you can see so clearly that when something happens

253
00:14:05,880 --> 00:14:06,880
and when you think about something,

254
00:14:06,880 --> 00:14:09,880
it's literally like you're slapping yourself in the face.

255
00:14:09,880 --> 00:14:12,880
And that's, you know,

256
00:14:12,880 --> 00:14:15,880
in times outside of meditation,

257
00:14:15,880 --> 00:14:18,880
you do something bad like you trip on the ground

258
00:14:18,880 --> 00:14:19,880
and you slap yourself.

259
00:14:19,880 --> 00:14:20,880
You literally slap yourself in the face.

260
00:14:20,880 --> 00:14:21,880
Nobody would do that.

261
00:14:21,880 --> 00:14:22,880
Why would you do that?

262
00:14:22,880 --> 00:14:23,880
You might get angry,

263
00:14:23,880 --> 00:14:26,880
but people don't know that being angry

264
00:14:26,880 --> 00:14:28,880
is like it's hurting yourself.

265
00:14:28,880 --> 00:14:29,880
It's hurting your own mind.

266
00:14:29,880 --> 00:14:31,880
It's like hurting yourself physically.

267
00:14:31,880 --> 00:14:33,880
But I think it's even worse

268
00:14:33,880 --> 00:14:35,880
because hurting your mind,

269
00:14:35,880 --> 00:14:36,880
it's your well-being.

270
00:14:36,880 --> 00:14:37,880
It's everything.

271
00:14:37,880 --> 00:14:39,880
That it's your existence.

272
00:14:39,880 --> 00:14:41,880
Everything outside of your mind,

273
00:14:41,880 --> 00:14:45,880
it's just sort of an environment.

274
00:14:45,880 --> 00:14:48,880
And so yeah,

275
00:14:48,880 --> 00:14:49,880
so once you sort of,

276
00:14:49,880 --> 00:14:54,880
and that's how I really start to be okay with my emotions,

277
00:14:54,880 --> 00:14:58,880
is that you start to realize how to be kind to yourself.

278
00:14:58,880 --> 00:15:05,880
You start to realize how to just accept things,

279
00:15:05,880 --> 00:15:06,880
accept yourself.

280
00:15:06,880 --> 00:15:08,880
Because before coming here,

281
00:15:08,880 --> 00:15:13,880
I realized I was very critical of my thoughts,

282
00:15:13,880 --> 00:15:14,880
very judgmental.

283
00:15:14,880 --> 00:15:16,880
I'll constantly tell myself,

284
00:15:16,880 --> 00:15:17,880
oh, you shouldn't do this.

285
00:15:17,880 --> 00:15:18,880
You shouldn't think this.

286
00:15:18,880 --> 00:15:19,880
You shouldn't feel this.

287
00:15:19,880 --> 00:15:21,880
This isn't good.

288
00:15:21,880 --> 00:15:24,880
You should feel ashamed.

289
00:15:24,880 --> 00:15:26,880
You should feel anxious.

290
00:15:26,880 --> 00:15:28,880
You need to change it.

291
00:15:28,880 --> 00:15:30,880
You need to make a drastic change.

292
00:15:30,880 --> 00:15:32,880
Otherwise, it's bad for you.

293
00:15:32,880 --> 00:15:33,880
Otherwise,

294
00:15:33,880 --> 00:15:36,880
you will lose career opportunities.

295
00:15:36,880 --> 00:15:38,880
You will lose relationship opportunities.

296
00:15:38,880 --> 00:15:43,880
You would lose things that society would grant you

297
00:15:43,880 --> 00:15:46,880
if your personality isn't like this.

298
00:15:46,880 --> 00:15:48,880
And so that train me

299
00:15:48,880 --> 00:15:52,880
to think in a way that I have to be a certain person.

300
00:15:52,880 --> 00:15:54,880
So I will not accept parts of myself.

301
00:15:54,880 --> 00:15:55,880
But once you come here,

302
00:15:55,880 --> 00:15:57,880
you start to learn that

303
00:15:57,880 --> 00:16:01,880
that's how dangerous that is to you.

304
00:16:01,880 --> 00:16:06,880
That you're really unhappy living like that.

305
00:16:06,880 --> 00:16:09,880
It's really unhappy.

306
00:16:09,880 --> 00:16:12,880
It's an imprisonment.

307
00:16:12,880 --> 00:16:18,880
You're a prisoner to society.

308
00:16:18,880 --> 00:16:21,880
Society tells you that you need to feel like this.

309
00:16:21,880 --> 00:16:25,880
And it's basically telling you don't accept yourself.

310
00:16:25,880 --> 00:16:27,880
You have to be like this.

311
00:16:27,880 --> 00:16:29,880
Otherwise, you can't be happy.

312
00:16:29,880 --> 00:16:31,880
In order to be happy,

313
00:16:31,880 --> 00:16:36,880
you have to not accept yourself.

314
00:16:36,880 --> 00:16:38,880
Which is contradictive.

315
00:16:38,880 --> 00:16:43,880
And I came here and I realized

316
00:16:43,880 --> 00:16:45,880
whether or not I think about bad things,

317
00:16:45,880 --> 00:16:47,880
whether or not I feel bad emotions

318
00:16:47,880 --> 00:16:49,880
or positive anyone.

319
00:16:49,880 --> 00:16:51,880
They just come and they go.

320
00:16:51,880 --> 00:16:54,880
They never last forever.

321
00:16:54,880 --> 00:16:56,880
Sometimes I will feel extreme,

322
00:16:56,880 --> 00:16:58,880
just anger or extreme depression

323
00:16:58,880 --> 00:17:01,880
or isolation or loneliness.

324
00:17:01,880 --> 00:17:04,880
And at that very moment,

325
00:17:04,880 --> 00:17:07,880
I will always think that they will last forever.

326
00:17:07,880 --> 00:17:09,880
I will always think that.

327
00:17:09,880 --> 00:17:11,880
I always think, oh, this is the worst emotion.

328
00:17:11,880 --> 00:17:13,880
I wish you would just go away.

329
00:17:13,880 --> 00:17:15,880
Like I will believe in never.

330
00:17:15,880 --> 00:17:17,880
Like I will believe that it doesn't go away.

331
00:17:17,880 --> 00:17:20,880
But eventually, it always does.

332
00:17:20,880 --> 00:17:23,880
And I think that's the best opportunity

333
00:17:23,880 --> 00:17:24,880
is that when you do nothing,

334
00:17:24,880 --> 00:17:26,880
you realize that it does go away.

335
00:17:26,880 --> 00:17:28,880
When you do something,

336
00:17:28,880 --> 00:17:30,880
you don't realize that.

337
00:17:30,880 --> 00:17:32,880
You're tension is something else.

338
00:17:32,880 --> 00:17:35,880
And you're always kept in this illusion

339
00:17:35,880 --> 00:17:37,880
that when you have bad emotions,

340
00:17:37,880 --> 00:17:39,880
it's like a red alert.

341
00:17:39,880 --> 00:17:41,880
You have to do something.

342
00:17:41,880 --> 00:17:44,880
Otherwise, it'll just keep coming in

343
00:17:44,880 --> 00:17:46,880
and destroy everything.

344
00:17:46,880 --> 00:17:51,880
But with patience and with mindfulness

345
00:17:51,880 --> 00:17:55,880
and with the opportunity to have no distractions

346
00:17:55,880 --> 00:17:59,880
but to fully introspect

347
00:17:59,880 --> 00:18:02,880
and look at yourself and confront yourself.

348
00:18:02,880 --> 00:18:05,880
Sometimes I feel like this is it.

349
00:18:05,880 --> 00:18:07,880
I felt my happiness and ready to go.

350
00:18:07,880 --> 00:18:08,880
I don't need to be here.

351
00:18:08,880 --> 00:18:10,880
I know all the answers.

352
00:18:10,880 --> 00:18:11,880
And the next day,

353
00:18:11,880 --> 00:18:12,880
I feel extremely.

354
00:18:12,880 --> 00:18:16,880
Just reach the other end of the emotional scale.

355
00:18:16,880 --> 00:18:18,880
I feel just hopelessness.

356
00:18:18,880 --> 00:18:21,880
And I feel like I'm not going to be here.

357
00:18:21,880 --> 00:18:23,880
I'm not going to be here.

358
00:18:23,880 --> 00:18:24,880
I'm not going to be here.

359
00:18:24,880 --> 00:18:26,880
And the next day, I feel extremely.

360
00:18:26,880 --> 00:18:30,880
Just reach the other end of the emotional scale.

361
00:18:30,880 --> 00:18:33,880
I feel hopelessness, frustration,

362
00:18:33,880 --> 00:18:36,880
like everything I've learned was lost.

363
00:18:36,880 --> 00:18:39,880
Sometimes I think to myself,

364
00:18:39,880 --> 00:18:40,880
this is too difficult for me.

365
00:18:40,880 --> 00:18:42,880
I would never reach that stage of happiness.

366
00:18:42,880 --> 00:18:46,880
I would just always be imprisoned by

367
00:18:46,880 --> 00:18:49,880
negativity and suffering.

368
00:18:49,880 --> 00:18:51,880
This is ridiculous.

369
00:18:51,880 --> 00:18:54,880
I was sleeping in a cave

370
00:18:54,880 --> 00:18:57,880
with more than I think

371
00:18:57,880 --> 00:19:00,880
than 100 mosquito bites by now,

372
00:19:00,880 --> 00:19:04,880
with a chance of actually getting some diseases

373
00:19:04,880 --> 00:19:08,880
and sleeping less than eight hours,

374
00:19:08,880 --> 00:19:11,880
which is actually...

375
00:19:11,880 --> 00:19:13,880
Actually, it isn't that bad right now,

376
00:19:13,880 --> 00:19:17,880
but at first it was too much of a challenge for me.

377
00:19:17,880 --> 00:19:21,880
But you realize whether if it's positive experiences

378
00:19:21,880 --> 00:19:23,880
or negative experiences,

379
00:19:23,880 --> 00:19:25,880
nothing stays forever.

380
00:19:25,880 --> 00:19:28,880
So why bother trying to do those things

381
00:19:28,880 --> 00:19:30,880
that make you feel like

382
00:19:30,880 --> 00:19:33,880
you've reached a certain stage where you're just fully content?

383
00:19:33,880 --> 00:19:36,880
Because that stage, I don't think that stage

384
00:19:36,880 --> 00:19:39,880
ever exists in society anyway.

385
00:19:39,880 --> 00:19:42,880
Society tells you that you do these things

386
00:19:42,880 --> 00:19:44,880
and once you reach them,

387
00:19:44,880 --> 00:19:46,880
there's this eternal happiness

388
00:19:46,880 --> 00:19:48,880
and you just reach a and then you just content.

389
00:19:48,880 --> 00:19:49,880
That's your ticket.

390
00:19:49,880 --> 00:19:51,880
But that never happens.

391
00:19:51,880 --> 00:19:53,880
You do all those things,

392
00:19:53,880 --> 00:19:56,880
but internally, you don't know the truth.

393
00:19:56,880 --> 00:20:00,880
You still feel this harmony

394
00:20:00,880 --> 00:20:05,880
and this gap and this sense of lack.

395
00:20:05,880 --> 00:20:09,880
And it's when you realize

396
00:20:09,880 --> 00:20:14,880
that certain emotions and thoughts,

397
00:20:14,880 --> 00:20:18,880
they're just not satisfying, in a sense.

398
00:20:18,880 --> 00:20:23,880
And that's the thing, they're not satisfying.

399
00:20:23,880 --> 00:20:27,880
So once you stay here for a while,

400
00:20:27,880 --> 00:20:29,880
you let go of some things

401
00:20:29,880 --> 00:20:33,880
because you realize the things that you're clinging onto

402
00:20:33,880 --> 00:20:35,880
back home.

403
00:20:35,880 --> 00:20:37,880
I'm sure they're good,

404
00:20:37,880 --> 00:20:41,880
but they're also using up a lot of your attention and energy.

405
00:20:41,880 --> 00:20:43,880
They're not necessary.

406
00:20:43,880 --> 00:20:47,880
That are not ultimately conducive to your happiness.

407
00:20:47,880 --> 00:20:51,880
They don't add any more happiness.

408
00:20:51,880 --> 00:20:53,880
They don't really make it eternal.

409
00:20:53,880 --> 00:20:56,880
In fact, they actually add more addiction

410
00:20:56,880 --> 00:20:58,880
because the more you experience them, the more you want it

411
00:20:58,880 --> 00:21:00,880
because you get used to.

412
00:21:00,880 --> 00:21:03,880
Some of the things that I would do at home

413
00:21:03,880 --> 00:21:06,880
or back in the West, I would do them

414
00:21:06,880 --> 00:21:08,880
and I'm sure they're making me happy,

415
00:21:08,880 --> 00:21:11,880
but I would expect them to make me happier the next time.

416
00:21:11,880 --> 00:21:14,880
And it's just a constant cycle

417
00:21:14,880 --> 00:21:15,880
and I would get tired of them

418
00:21:15,880 --> 00:21:17,880
and I move on.

419
00:21:17,880 --> 00:21:22,880
And that's kind of like a tug of war of life.

420
00:21:22,880 --> 00:21:26,880
You're just happiness

421
00:21:26,880 --> 00:21:28,880
and you try to pull it towards you

422
00:21:28,880 --> 00:21:29,880
but then it keeps on going away

423
00:21:29,880 --> 00:21:30,880
and trying to pull it towards you

424
00:21:30,880 --> 00:21:32,880
and keeps on going away.

425
00:21:32,880 --> 00:21:36,880
You never just sort of relax.

426
00:21:36,880 --> 00:21:38,880
You always want something for yourself

427
00:21:38,880 --> 00:21:41,880
and you're resistant to something else that you don't want.

428
00:21:41,880 --> 00:21:43,880
Once you come here, you just let go.

429
00:21:43,880 --> 00:21:45,880
You just tell yourself, stop playing the game.

430
00:21:45,880 --> 00:21:48,880
You don't want to play the tug of war anymore

431
00:21:48,880 --> 00:21:50,880
or just whatever.

432
00:21:50,880 --> 00:21:53,880
And it's not a sense of carelessness or apathy

433
00:21:53,880 --> 00:21:55,880
but it's a sense of clarity.

434
00:21:55,880 --> 00:21:58,880
You're really clear on the true nature

435
00:21:58,880 --> 00:22:01,880
of your emotions and thoughts.

436
00:22:01,880 --> 00:22:06,880
And you realize what's good for you,

437
00:22:06,880 --> 00:22:08,880
what's good for others,

438
00:22:08,880 --> 00:22:11,880
what's bad for you and what's bad for others.

439
00:22:11,880 --> 00:22:15,880
And that's it.

440
00:22:15,880 --> 00:22:21,880
So those are the things that I've learned majorly.

441
00:22:21,880 --> 00:22:28,880
And I really hope that I don't forget them

442
00:22:28,880 --> 00:22:34,880
when I go back home, which I don't think I will

443
00:22:34,880 --> 00:22:36,880
because a lot of the things that I learned

444
00:22:36,880 --> 00:22:40,880
it's too major for me to forget.

445
00:22:40,880 --> 00:22:43,880
It's too big for me to forget.

446
00:22:43,880 --> 00:22:47,880
It's, I think it's already been internalized

447
00:22:47,880 --> 00:22:50,880
inside of me.

448
00:22:50,880 --> 00:22:54,880
The fact that emotions come and go,

449
00:22:54,880 --> 00:22:57,880
thoughts come and go to have that,

450
00:22:57,880 --> 00:22:59,880
to actually have that experience.

451
00:22:59,880 --> 00:23:04,880
It's much more different than to read about it in a book

452
00:23:04,880 --> 00:23:06,880
because I knew about it.

453
00:23:06,880 --> 00:23:09,880
I knew everything that I know now

454
00:23:09,880 --> 00:23:11,880
before I came here.

455
00:23:11,880 --> 00:23:14,880
Like everything I'm telling you now,

456
00:23:14,880 --> 00:23:19,880
I had the ability to tell you back home before anything happened

457
00:23:19,880 --> 00:23:21,880
because I had read about it.

458
00:23:21,880 --> 00:23:22,880
I knew everything that,

459
00:23:22,880 --> 00:23:25,880
I knew the basics of Buddhism.

460
00:23:25,880 --> 00:23:30,880
I knew the basic results that you get from meditation.

461
00:23:30,880 --> 00:23:33,880
I read a lot about philosophy, about society.

462
00:23:33,880 --> 00:23:38,880
I knew a lot of things about how to live a good life.

463
00:23:38,880 --> 00:23:41,880
But, obviously, it's different.

464
00:23:41,880 --> 00:23:42,880
Something is different here.

465
00:23:42,880 --> 00:23:44,880
You get the actual experience.

466
00:23:44,880 --> 00:23:47,880
I mean, to know something intellectually,

467
00:23:47,880 --> 00:23:49,880
you don't really know it.

468
00:23:49,880 --> 00:23:50,880
You just have the information,

469
00:23:50,880 --> 00:23:52,880
but you don't internalize it.

470
00:23:52,880 --> 00:23:54,880
You don't digest it completely.

471
00:23:54,880 --> 00:23:56,880
You can just regurgitate just from memory.

472
00:23:56,880 --> 00:23:58,880
But when you actually experience it,

473
00:23:58,880 --> 00:24:06,880
you sort of merge sort of fusion

474
00:24:06,880 --> 00:24:08,880
with that knowledge.

475
00:24:08,880 --> 00:24:10,880
You embody that knowledge now.

476
00:24:10,880 --> 00:24:16,880
You're the personification of what you've learned.

477
00:24:16,880 --> 00:24:20,880
So, yeah, I mean,

478
00:24:20,880 --> 00:24:25,880
I would really urge the people who are really interested in Buddhism,

479
00:24:25,880 --> 00:24:27,880
not to just read about it.

480
00:24:27,880 --> 00:24:29,880
Because you can read a million books about Buddhism,

481
00:24:29,880 --> 00:24:31,880
know every single little thing.

482
00:24:31,880 --> 00:24:34,880
But I will guarantee you that the results that you get from reading it

483
00:24:34,880 --> 00:24:37,880
will not be as good as someone who never knew anything about Buddhism,

484
00:24:37,880 --> 00:24:41,880
but just meditated for 20 days or even 10 days

485
00:24:41,880 --> 00:24:44,880
and had a direct experience of what you read.

486
00:24:44,880 --> 00:24:47,880
That's the most important thing I've learned.

487
00:24:47,880 --> 00:24:49,880
One of the most important things I've learned is that

488
00:24:49,880 --> 00:24:51,880
you have to get direct training.

489
00:24:51,880 --> 00:24:54,880
You have to get meditation.

490
00:24:54,880 --> 00:24:59,880
And you have to have a teacher to guide you,

491
00:24:59,880 --> 00:25:03,880
which I was fortunate enough to be with a very good one.

492
00:25:03,880 --> 00:25:08,880
And, yeah, that's basically it.

493
00:25:08,880 --> 00:25:15,880
That's basically the answer that I would give in terms of what I've learned

494
00:25:15,880 --> 00:25:19,880
and the experiences and the things I've gained here.

495
00:25:19,880 --> 00:25:35,880
So, yeah, thank you for listening in, if you are.

