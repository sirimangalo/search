1
00:00:00,000 --> 00:00:12,000
Well, when I ask a question in the class, even when I don't people stare at me very hard and make me nervous, any advice, what should I note?

2
00:00:12,000 --> 00:00:18,000
I never had a problem with that. I don't understand how you can have a problem.

3
00:00:18,000 --> 00:00:31,000
Because you would take a terrible therapist. It's just doesn't exist. It doesn't exist for me. How does it exist for you?

4
00:00:31,000 --> 00:00:41,000
Well, let's see. I'll just stick to you there.

5
00:00:41,000 --> 00:00:50,000
Anyway, I'll just say nervous nervous. If you're nervous in class, you're nervous.

6
00:00:50,000 --> 00:00:59,000
So it's like, it might be difficult to be mindful about it.

7
00:00:59,000 --> 00:01:06,000
I mean, if you can go this way to be mindful about it or you just try it.

8
00:01:06,000 --> 00:01:16,000
Hey, just don't be afraid of making mistakes or just, that's the way it is.

9
00:01:16,000 --> 00:01:21,000
That's who you are, how you look like, how you speak.

10
00:01:21,000 --> 00:01:25,000
So, you know, it shouldn't get problems.

11
00:01:25,000 --> 00:01:28,000
I'm not being mindful of it.

12
00:01:28,000 --> 00:01:35,000
Or be mindful about it. I'll just give it my advice. Be mindful about it. You can investigate it.

13
00:01:35,000 --> 00:01:37,000
You might get enlightened.

14
00:01:37,000 --> 00:01:48,000
You might get enlightened and understand what's the cause and how is it?

15
00:01:48,000 --> 00:01:59,000
Well, there's no, it's not reasonable. There is no don't worry about it.

16
00:01:59,000 --> 00:02:04,000
There is no actually recently behind being nervous because someone is looking at you.

17
00:02:04,000 --> 00:02:11,000
So just telling them to not worry about it. I mean, they'll be able to not worry about it.

18
00:02:11,000 --> 00:02:20,000
No, I don't know. I'm just telling him that it's actually true.

19
00:02:20,000 --> 00:02:24,000
It's like, try to not on a piece of paper.

20
00:02:24,000 --> 00:02:32,000
Actually, why you feel nervous? Why should you?

21
00:02:32,000 --> 00:02:42,000
What's the reason for it? Some logical points.

22
00:02:42,000 --> 00:02:45,000
Because there's no logic behind it. Why should you be afraid?

23
00:02:45,000 --> 00:02:51,000
I can't get it. I mean, I made a mistake. I made a mistake.

24
00:02:51,000 --> 00:02:55,000
But I don't think that's going to help him, Lucas.

25
00:02:55,000 --> 00:02:59,000
I think he has to really see for himself that it's pointless.

26
00:02:59,000 --> 00:03:04,000
And the only way to do that, I think, is to be mindful of it.

27
00:03:04,000 --> 00:03:07,000
I think writing down and saying, oh, it's absurd.

28
00:03:07,000 --> 00:03:11,000
He isn't really going to make him see that it's absurd because it's all mental activity.

29
00:03:11,000 --> 00:03:14,000
It's not really realizing, oh, this is absurd.

30
00:03:14,000 --> 00:03:17,000
He has to look at it and see what he's doing to himself.

31
00:03:17,000 --> 00:03:24,000
I think that seems to be what's shown through meditation.

32
00:03:24,000 --> 00:03:28,000
Simply telling yourself.

33
00:03:28,000 --> 00:03:32,000
Well, but sometimes it's like that. You just sometimes you can't figure.

34
00:03:32,000 --> 00:03:37,000
Sometimes you might not be confident. That's normal, so it's probable.

35
00:03:37,000 --> 00:03:41,000
Well, I think the important thing is seeing that it's normal,

36
00:03:41,000 --> 00:03:46,000
realizing that it's actually just happening like that. It's not bad.

37
00:03:46,000 --> 00:03:51,000
No, well, if you don't become confident, just tell them, hey, I don't feel confident.

38
00:03:51,000 --> 00:03:52,000
I'll tell you something.

39
00:03:52,000 --> 00:03:53,000
He's open about it.

40
00:03:53,000 --> 00:03:55,000
The thing is to tell yourself.

41
00:03:55,000 --> 00:03:59,000
I don't feel confident, and then you accept it.

42
00:03:59,000 --> 00:04:03,000
Or just ask them for like, what are you looking at?

43
00:04:03,000 --> 00:04:07,000
That's maybe a big thing about anxiety and nervousness.

44
00:04:07,000 --> 00:04:11,000
Is, no.

45
00:04:11,000 --> 00:04:27,000
I missed what he said, so obviously very.

