1
00:00:00,000 --> 00:00:21,840
I would like to talk about the 10 Uphukilesa, which is something which meditators just keep

2
00:00:21,840 --> 00:00:36,600
in mind when they first undertake to practice along the path of vipassana.

3
00:00:36,600 --> 00:00:43,200
For people who practice samata meditation or meditation, which is simply for the purpose

4
00:00:43,200 --> 00:00:52,360
of calming the mind, meditation which simply for making the mind focused and fixed

5
00:00:52,360 --> 00:00:56,960
and not for the purpose of continuing on to insight.

6
00:00:56,960 --> 00:01:09,920
People who spend a lot of time attaining the states of strong concentration.

7
00:01:09,920 --> 00:01:21,520
These people are much more likely to experience very intense states of what we call the Uphukilesa.

8
00:01:21,520 --> 00:01:34,120
Uphukilesa the way they talk about it is something good which blocks goodness.

9
00:01:34,120 --> 00:01:41,760
The time when the meditator has reached what they call the fourth stage of insight knowledge.

10
00:01:41,760 --> 00:01:49,280
For a new meditator, this is often the fourth of the fifth day.

11
00:01:49,280 --> 00:01:55,720
For the first time the meditator will generally experience some good things,

12
00:01:55,720 --> 00:02:00,760
there are good things which block one from attaining goodness.

13
00:02:00,760 --> 00:02:10,280
Goodness means greater good.

14
00:02:10,280 --> 00:02:18,640
These are things which are called kusala, they are called wholesomeness, they are good

15
00:02:18,640 --> 00:02:19,640
things.

16
00:02:19,640 --> 00:02:29,160
They are things which lead to happiness, lead to peace in the here and now, but they are

17
00:02:29,160 --> 00:02:38,200
called Uphukilesa because they can create a barrier, a block or can become a hindrance

18
00:02:38,200 --> 00:02:43,080
in the practice of the past and the meditation.

19
00:02:43,080 --> 00:02:53,760
When many good things arise, the meditator becomes attached, the stuck in these nice

20
00:02:53,760 --> 00:03:03,960
things which have arisen and loses their interest or gives up their practice of seeing

21
00:03:03,960 --> 00:03:11,520
the impermanence of these phenomena.

22
00:03:11,520 --> 00:03:17,560
When the meditator gives up the mind gives up the meditation object instead, dwells

23
00:03:17,560 --> 00:03:26,840
in their enjoyment of these good states, the practice of insight is inhibited.

24
00:03:26,840 --> 00:03:33,160
The meditator's progress is inhibited when it's not able to reach the higher stages of

25
00:03:33,160 --> 00:03:40,280
knowledge, the fifth, the sixth and so on, because the mind still has attachment.

26
00:03:40,280 --> 00:03:47,440
So altogether the Uphukilesa are something which meditators have to watch out for.

27
00:03:47,440 --> 00:04:03,160
Not to be afraid of, but to keep an eye out for and to use as a sign, a reason to reflect

28
00:04:03,160 --> 00:04:12,560
in one's mind whether one is enjoying attached to looking for seeking out in these good

29
00:04:12,560 --> 00:04:20,720
states, these nice meditation states, altogether in the age and text there are ten.

30
00:04:20,720 --> 00:04:25,960
You could branch it out and say there are many more, but they generally come under ten

31
00:04:25,960 --> 00:04:26,960
headings.

32
00:04:26,960 --> 00:04:32,200
These are all the good things which can come which are not really in the end of the highest

33
00:04:32,200 --> 00:04:33,200
value.

34
00:04:33,200 --> 00:04:35,520
They're good things, but only in the worldly sense.

35
00:04:35,520 --> 00:04:41,040
They can't lead one to freedom from suffering by themselves and if one becomes attached

36
00:04:41,040 --> 00:04:44,520
to them, they can be actually, become a hindrance.

37
00:04:44,520 --> 00:04:58,000
The first one is Opasa or illumination, then this is easily, an easy explanation is bright

38
00:04:58,000 --> 00:05:04,440
lights or any sort of brightness that arises in the mind.

39
00:05:04,440 --> 00:05:10,560
When one sees bright lights or colors or pictures, sometimes like a movie, sometimes

40
00:05:10,560 --> 00:05:24,760
like a kaleidoscope, sometimes like a bright flash, lasting bright light or color, there

41
00:05:24,760 --> 00:05:36,640
are many kinds of imagery we can call Opasa, they all come under the heading of illumination.

42
00:05:36,640 --> 00:05:41,120
So if we're truly practicing the best and it shouldn't come as a surprise that this is not

43
00:05:41,120 --> 00:05:47,600
what we're aiming for, but for people who have begun to practice, they often are looking

44
00:05:47,600 --> 00:05:53,760
for such things, for they've heard other people practicing other types of meditation,

45
00:05:53,760 --> 00:05:58,840
talk about these things, they might be seeking them out and when they arise the meditator

46
00:05:58,840 --> 00:06:10,480
becomes pleased by them, becomes attached to them, comes to enjoy them, comes to feel comfortable

47
00:06:10,480 --> 00:06:16,720
with them, when they're not there, the meditator might feel agitated and that makes it difficult

48
00:06:16,720 --> 00:06:23,600
for one to look at reality objectively, because one is always trying to practice in such

49
00:06:23,600 --> 00:06:30,800
a way that these phenomena arise and it's not able to open oneself up to the full spectrum

50
00:06:30,800 --> 00:06:36,800
of bad things as well, unpleasant things as well, be able to deal with unpleasant situation.

51
00:06:36,800 --> 00:06:44,080
The Opasa is the first one and we acknowledge this one as seeing, as it's one of the

52
00:06:44,080 --> 00:06:51,440
senses the Buddha said, when seeing he knows I am seeing, and we say to ourselves seeing,

53
00:06:51,440 --> 00:07:00,840
we see, we make this clear awareness in our mind, I am seeing, not emphasizing the eye,

54
00:07:00,840 --> 00:07:07,080
emphasizing the scene, and in English we don't have to use the eye, and in probably it's

55
00:07:07,080 --> 00:07:12,760
necessary to conjugate the verb, but that means I am seeing, but the emphasis is not

56
00:07:12,760 --> 00:07:25,400
on the eye, so we're simply seeing seeing, and we say seeing, seeing until the sight comes

57
00:07:25,400 --> 00:07:29,800
away, we're reminding ourselves, set demons and reminding, we're reminding ourselves

58
00:07:29,800 --> 00:07:34,520
all the time that this is only seen, it's not a bright light, it's not a color, it's not

59
00:07:34,520 --> 00:07:39,680
this, it's not all of these things arise in the mind, it's actually just seeing, it's

60
00:07:39,680 --> 00:07:50,640
light in the eye, in the mind, we acknowledge in this way we have no attachment, we find

61
00:07:50,640 --> 00:07:56,160
we take no comfort or satisfaction in this object, and we continue on with our meditation,

62
00:07:56,160 --> 00:08:02,000
we see that it also is something which is arising in season, we see that it's not me as

63
00:08:02,000 --> 00:08:10,760
the mind, it's not the mind, it's the first one, the second one, beat the rapture, rapture

64
00:08:10,760 --> 00:08:18,960
there are, in the text there are five kinds of rapture, you want to sort it out however

65
00:08:18,960 --> 00:08:26,840
you might, it means anything where the mind becomes so full of ecstasy or concentration,

66
00:08:26,840 --> 00:08:35,080
the mind becomes overful, and starts to spill out like the charge or like a full glass

67
00:08:35,080 --> 00:08:39,320
of water, when you feel the glass up with water, if you keep pouring it starts to pour

68
00:08:39,320 --> 00:08:48,200
over, so beauty is this pouring over, this outpouring of rapture, and it comes in different

69
00:08:48,200 --> 00:08:57,360
ways, if it comes in just the little flashes, flashes, sometimes it can come just in one flash,

70
00:08:57,360 --> 00:09:07,320
where suddenly the meditator feels like a shock over the meditator, feels a goose flash

71
00:09:07,320 --> 00:09:16,320
just for just for an instant, it can come in the form of light, where the meditator feels

72
00:09:16,320 --> 00:09:21,920
very light, suddenly there's this static electricity in the meditator, feels like they're

73
00:09:21,920 --> 00:09:27,000
going to float, they're charged and going to float off the ground, some people feel like

74
00:09:27,000 --> 00:09:31,920
they're actually floating, some people apparently actually do float, and they've been

75
00:09:31,920 --> 00:09:40,880
up to you to see what it is, sometimes it go comes in waves, like goose flash in waves

76
00:09:40,880 --> 00:09:51,160
or laughter, or crying, many people cry for hours, and don't know the reason, sometimes

77
00:09:51,160 --> 00:09:59,160
it comes very subtly, sublimely, when the body is shaking, rocking back and forth, many

78
00:09:59,160 --> 00:10:04,960
different ways, all in all it's this is called rapture, but there are many ways of

79
00:10:04,960 --> 00:10:10,560
acknowledging, you could just say to yourself, rapture rapture, but it's not very specific, it's

80
00:10:10,560 --> 00:10:15,680
important that you know that this is what we call pete or rapture, know how it arises,

81
00:10:15,680 --> 00:10:20,840
to help use the word rapture to understand it, but when you acknowledge, you acknowledge

82
00:10:20,840 --> 00:10:27,600
according to the existential phenomenon, the realities which are there, when you're

83
00:10:27,600 --> 00:10:35,920
shaking, you say do self swaying, you know, swaying, when you're floating, you say do self

84
00:10:35,920 --> 00:10:41,560
floating, you're like, when you cry, say crying, crying, when you laugh, laughing, few

85
00:10:41,560 --> 00:10:47,680
yawn, some people yawn, yawn, yawn, yawning, when you feel goose flash, you can

86
00:10:47,680 --> 00:10:56,320
say feeling or so, we have to acknowledge in this way the mind won't attach to the sensations,

87
00:10:56,320 --> 00:11:00,840
again, these things are not bad, and we're not trying to, of course, make them go away,

88
00:11:00,840 --> 00:11:08,360
we're trying to shake the attachment which will arise, cut away the attachment so that

89
00:11:08,360 --> 00:11:16,920
there's a clean, clean contact, when the mind contacts the object, it's like when you

90
00:11:16,920 --> 00:11:21,360
have electricity, you need a clean contact, or else you get sparks and you get all sorts

91
00:11:21,360 --> 00:11:31,520
of undesired, you know, crystal electrical short, short circuits, but when you have a

92
00:11:31,520 --> 00:11:37,320
clean contact, when the mind is only knowing this is seeing, this is hearing and so on,

93
00:11:37,320 --> 00:11:46,880
this is feeling, then there arises no attachment, no rust, no sparks, this is the second

94
00:11:46,880 --> 00:11:53,240
is called BT, number three, and I'm probably not going in the right order here, but I'll

95
00:11:53,240 --> 00:12:06,840
get through 10 of them at home, but Sati is quiet or tranquility, they say, it's when the

96
00:12:06,840 --> 00:12:14,760
mind is something like the mind stops, the activity, the busyness in the mind just suddenly

97
00:12:14,760 --> 00:12:20,640
stops, or slow stops, maybe even the rising and the falling of the abdomen is not clear,

98
00:12:20,640 --> 00:12:25,760
and there's nothing to acknowledge, and the meditators will often come and say, oh, Jan,

99
00:12:25,760 --> 00:12:33,640
there's nothing to acknowledge, they have no meditation object, what am I to do, it's

100
00:12:33,640 --> 00:12:40,080
all quiet, it's silent, they just feel quiet, well then you teach your job is very easy

101
00:12:40,080 --> 00:12:48,280
to point the words back in the meditators, basic, well say quiet, quiet, quiet, as long

102
00:12:48,280 --> 00:12:53,040
as there's consciousness there and there's something, the consciousness is taking something

103
00:12:53,040 --> 00:12:59,280
as an object, when we feel quiet, it's taking quiet as an object, so if we are not mindful

104
00:12:59,280 --> 00:13:07,680
quiet, quiet, the mind will become attached to it, become comfortable in it, then we

105
00:13:07,680 --> 00:13:13,760
start, become addicted to it, always seeking out and wanting quiet, of course there's

106
00:13:13,760 --> 00:13:17,760
nothing wrong with being quiet and it doesn't mean that your meditation has to stop, but

107
00:13:17,760 --> 00:13:22,600
because meditators feel like their meditation objects have disappeared, they do stop

108
00:13:22,600 --> 00:13:27,680
acknowledging and they just enjoy the calm, when you feel calm, it's necessary for

109
00:13:27,680 --> 00:13:35,880
the meditator to create a clear thought, saying calm, calm, when feels quiet,

110
00:13:35,880 --> 00:13:48,280
quiet, quiet, this is number three, positive, again, that is number four, it means knowledge,

111
00:13:48,280 --> 00:13:55,600
so when the meditator reaches the fourth stage of knowledge, their mind starts to become

112
00:13:55,600 --> 00:14:00,760
very sharp, and they're able to see impermanence, they're able to see suffering, they're

113
00:14:00,760 --> 00:14:04,880
able to see non-self, they're able to see all of the Buddha's teachings, if they study

114
00:14:04,880 --> 00:14:09,920
the Buddha's teachings, suddenly it all becomes clear to them, all the Buddha's teaching

115
00:14:09,920 --> 00:14:16,880
becomes very clear and they're able to understand and think about the wind, the suddhvidhamma,

116
00:14:16,880 --> 00:14:21,720
everything is clear in their minds, if they've never studied the Buddha's teaching then

117
00:14:21,720 --> 00:14:25,800
they start thinking about other things, thinking about problems in their life, how to solve

118
00:14:25,800 --> 00:14:32,160
these problems, suddenly they can solve all their problems, family problems, situations at

119
00:14:32,160 --> 00:14:38,040
home, suddenly they start thinking, or if they're scholars and this were that subject, suddenly

120
00:14:38,040 --> 00:14:42,120
they'll start working on all of these different problems, mathematicians will suddenly have

121
00:14:42,120 --> 00:14:47,200
all these math problems, they're just working out in their heads, physicists suddenly

122
00:14:47,200 --> 00:14:54,880
still have all these physics problem and so on, writers suddenly write a book in their head,

123
00:14:54,880 --> 00:15:01,760
their eyes is yana, it's knowledge, all of this knowledge is only looking at, for those who see

124
00:15:01,760 --> 00:15:05,680
impermanence suffering in non-self, if they're versed in the Buddha's teaching, they often

125
00:15:05,680 --> 00:15:11,120
at this point think to themselves that they've become enlightened because of their knowledge,

126
00:15:11,120 --> 00:15:17,880
this is something important for a meta-haters to understand, many of these, these upiculates

127
00:15:17,880 --> 00:15:23,720
that when they arise, cause the meditator who is new at the practice of insight to think

128
00:15:23,720 --> 00:15:27,120
to themselves that because of these things when they see bright lights, they think they're

129
00:15:27,120 --> 00:15:36,160
enlightened, this happened even to a great teacher in the central Thailand and he would

130
00:15:36,160 --> 00:15:43,200
have his students focus on their light and focus on a bright light and they send it through

131
00:15:43,200 --> 00:15:49,440
the body and in this way they come to see all these shapes, Buddha images and when

132
00:15:49,440 --> 00:15:56,080
they see a Buddha image they think they're enlightened and he was, he was a very famous

133
00:15:56,080 --> 00:16:00,280
teacher, he's still very famous even though he's long passed away and he went to what

134
00:16:00,280 --> 00:16:08,000
Mahatad to study Vipassana, why he went is not clear, it seems like he was a nice gesture

135
00:16:08,000 --> 00:16:14,360
on his part that he should, as a great teacher, as a famous teacher, go to see another

136
00:16:14,360 --> 00:16:18,760
meditate, meditation center, could have been because what Mahatad had had become very

137
00:16:18,760 --> 00:16:24,480
famous and this idea of Vipassana was something that he had never studied.

138
00:16:24,480 --> 00:16:29,200
So he went to what Mahatad to see their practice, their practice and he explained to his

139
00:16:29,200 --> 00:16:34,440
teacher how he saw this light and of course he took it to mean that he was enlightened,

140
00:16:34,440 --> 00:16:41,040
mean that he had reached enlightenment and his teacher said, oh very good, very good, this

141
00:16:41,040 --> 00:16:45,760
is an important trick for a teacher, is that it's very important that when these things

142
00:16:45,760 --> 00:16:51,600
arise you don't say, oh no, you better be careful, those are Upa Kiva Hisa and get the

143
00:16:51,600 --> 00:16:57,480
meditator all worried, oh Pity, you have rapture, you're shaking, oh, it's very dangerous,

144
00:16:57,480 --> 00:17:00,920
I've heard people say this, it's very dangerous, you have to become, well you make the

145
00:17:00,920 --> 00:17:05,520
meditator paranoid, of course these things are not dangerous, they're dangerous if you

146
00:17:05,520 --> 00:17:12,320
become paranoid and crazy about them, just like anything when you become paranoid and

147
00:17:12,320 --> 00:17:18,280
crazy you go crazy, meditators especially if a meditator becomes paranoid and their teacher

148
00:17:18,280 --> 00:17:22,280
encourages them to become paranoid, it's going to be very dangerous, it's important

149
00:17:22,280 --> 00:17:29,360
that we encourage the meditators and say that when things are good, say it's good because

150
00:17:29,360 --> 00:17:34,120
the meditator feels that it's good, they feel good about it, so you're not saying anything

151
00:17:34,120 --> 00:17:39,200
wrong when you say this is a good thing, you look at Lisa, how good thing, oh very good,

152
00:17:39,200 --> 00:17:47,200
I said Rupa, talking to it because there's a very senior monk who said, oh you have to

153
00:17:47,200 --> 00:17:53,800
say to yourself, seeing, seeing, seeing, and so he went away and he came back the next

154
00:17:53,800 --> 00:18:02,040
day and he said, oh now I see monks, he said, so I see a room full of monks and he somehow

155
00:18:02,040 --> 00:18:06,880
thought that this was connected with his enlightenment, but what's happening is his very

156
00:18:06,880 --> 00:18:11,000
concentrated sphere of light which he had been focusing on for so many years was starting

157
00:18:11,000 --> 00:18:16,960
to break up, breaking into this image or that image, so he was still holding on even

158
00:18:16,960 --> 00:18:22,120
his image was changing and when he saw these monks he said, oh very good, very good,

159
00:18:22,120 --> 00:18:28,080
you say seeing, he said yes, he has to keep saying to yourself, seeing, seeing, seeing,

160
00:18:28,080 --> 00:18:36,800
the next day he came back and he was shaking and he said to his teacher, this is, he

161
00:18:36,800 --> 00:18:48,920
said, I feel, I feel like I have no hope, I have no refuge, he said I'm old now and

162
00:18:48,920 --> 00:18:53,840
very shortly I'm going to die and I've got nothing, he said, when he said seeing, seeing

163
00:18:53,840 --> 00:19:02,720
what he thought was permanent, this bright light which was permanent, disappeared and

164
00:19:02,720 --> 00:19:09,160
when it disappeared he became shocked, like he couldn't find his refuge anymore, this

165
00:19:09,160 --> 00:19:14,840
thing which he would depend upon and she had worked and worked and worked, as soon as he

166
00:19:14,840 --> 00:19:20,440
stopped working it, disappeared and he slipped through to the higher stages of knowledge

167
00:19:20,440 --> 00:19:26,000
seeing, Pankayan, Paayayana, Atina, Vaayana very quickly, so he was able to say to his teacher

168
00:19:26,000 --> 00:19:29,480
he felt like he had no refuge, these things which he thought was permanent or impermanent

169
00:19:29,480 --> 00:19:36,480
leads to Pankayana, the cessation, Paayayana of danger, seeing these things that are no refuge,

170
00:19:36,480 --> 00:19:44,800
these things which we are holding on to are like, like water in our hand, slipping through

171
00:19:44,800 --> 00:19:51,880
our hand or something like ice, when it's whole it feels something that you can hold

172
00:19:51,880 --> 00:20:08,680
on to but as you hold it, it turns into water and disappear and so it's important that

173
00:20:08,680 --> 00:20:12,400
when any of these things arise that we are very clear with ourselves that these things

174
00:20:12,400 --> 00:20:18,040
are not the state which we are trying to attain and anyway this which I was talking about

175
00:20:18,040 --> 00:20:21,160
Pankayana, this is another one which we have to be careful about, if your knowledge is simply

176
00:20:21,160 --> 00:20:26,640
thinking, considering mentally, this is an insight knowledge, insight knowledge is something

177
00:20:26,640 --> 00:20:37,680
which comes in a flash and it's a realization like a aha, like a, oh, like a shock or waking

178
00:20:37,680 --> 00:20:49,160
up or hearing or seeing something, when you see a golden treasure or you see a snake, there's

179
00:20:49,160 --> 00:20:53,240
the story of the man who caught a fish in his hand, when he pulled it out of the water

180
00:20:53,240 --> 00:20:59,520
he saw it with a snake, at the moment he saw it with a snake, he let go, insight knowledge

181
00:20:59,520 --> 00:21:04,880
comes like this, you see, like you see, you know like you see, this is why the Buddha

182
00:21:04,880 --> 00:21:08,640
is at knowing and seeing because you know like you see, when you know it's like, oh my

183
00:21:08,640 --> 00:21:14,200
god, this snake, what he thought was you is, it's not you, he thought was a fish, it's

184
00:21:14,200 --> 00:21:20,560
a snake, let it go.

185
00:21:20,560 --> 00:21:38,840
Number five, we have pakaha, pakaha is pakaha means really a, pakaha means effort, some

186
00:21:38,840 --> 00:21:43,560
meditators have great effort and they can practice all day all night all day all night,

187
00:21:43,560 --> 00:21:49,200
they feel like they could go, practice forever and as long as the energy lasts, they had

188
00:21:49,200 --> 00:21:54,280
one meditator who went running around the monastery, she just couldn't sit still, she suddenly

189
00:21:54,280 --> 00:21:58,480
had all this energy and she went walking, walking, walking, she even said to herself,

190
00:21:58,480 --> 00:22:05,240
and all the people that, this was at Deoisutab, which is a very tourist, it's a tourist

191
00:22:05,240 --> 00:22:10,880
destination, so all the tourists watching this crazy walking, walking, walking, walking

192
00:22:10,880 --> 00:22:18,200
around the monastery, in the middle of the day with hundreds of people, just a thought is some

193
00:22:18,200 --> 00:22:29,800
a little bit strange, and this is another thing to watch out for, in the end, sometimes

194
00:22:29,800 --> 00:22:34,320
the meditators who have this experience will overestimate themselves, and will push themselves

195
00:22:34,320 --> 00:22:39,240
hard, too hard, in the beginning and then in the later stages they're unable to practice

196
00:22:39,240 --> 00:22:44,120
they have to take a break, or they find it hard to continue, and they get set back, because

197
00:22:44,120 --> 00:22:47,640
they get caught up in their energy, I've heard of many stories of monks who think they

198
00:22:47,640 --> 00:22:53,720
became enlightened because suddenly they're not tired, practicing all day all night, no liking,

199
00:22:53,720 --> 00:23:00,640
disliking, no delusion, not delusion still, but it's delusions very hard to see, so if you

200
00:23:00,640 --> 00:23:07,280
don't see it, you think you're enlightened, simply because of energy, and be careful, and

201
00:23:07,280 --> 00:23:13,080
it's a sign of progress when the meditator feels energy, but it can simply just be a sign

202
00:23:13,080 --> 00:23:24,040
of strong concentration, or fixed concentration, and it's temporary, it's not something

203
00:23:24,040 --> 00:23:31,120
which lasts, when we feel effort, we have to say to ourselves energy, energy, or feeling,

204
00:23:31,120 --> 00:23:40,560
it's an agitated, restless, it's important to know all of these, there's nothing wrong

205
00:23:40,560 --> 00:23:44,760
when this arises, and we're not up, we shouldn't be upset when it arises, but we should

206
00:23:44,760 --> 00:23:51,200
not either be attached to it, number five, number six, anti-mocas, anti-mocas, another word

207
00:23:51,200 --> 00:23:59,440
for confidence, means the meditators, sure, completely sure about the meditation practice,

208
00:23:59,440 --> 00:24:04,480
any meditators at this point will tell the teacher they want to become a monk or a nun,

209
00:24:04,480 --> 00:24:07,840
they start thinking about their family and thinking about how all their family members

210
00:24:07,840 --> 00:24:14,080
have to come and practice this, because this is the way, because the sign of the meditator

211
00:24:14,080 --> 00:24:20,680
is realized something, but the problem with all of these things is, I said, with confidence,

212
00:24:20,680 --> 00:24:25,440
or with any of these things, that the meditator then takes is the mean that they've become

213
00:24:25,440 --> 00:24:29,400
enlightened, confidence is a big one, the meditator has great confidence,

214
00:24:29,400 --> 00:24:38,400
even though they might not think I'm enlightened, they tend to become lax, this is how

215
00:24:38,400 --> 00:24:42,720
our mind works when we're worried, when we come here, we're all stressed and worried

216
00:24:42,720 --> 00:24:48,520
and we've seen so much suffering, that it pushes us, and we're so scared that we should

217
00:24:48,520 --> 00:24:52,960
have to go back and face all of that suffering, we should have to go back and yell at people

218
00:24:52,960 --> 00:24:59,640
and have people yell at us and not be able to deal with it, that when confidence arises

219
00:24:59,640 --> 00:25:06,200
and we feel like we've gained something, all of that impulse to practice or impetus

220
00:25:06,200 --> 00:25:11,760
to practice melts away, and it can be very hard at that point to find impetus, find reason

221
00:25:11,760 --> 00:25:22,880
to practice, the meditator often becomes lax at that point, so it fear and worry and

222
00:25:22,880 --> 00:25:27,280
all of these things which sometimes bring us stress, which bring us to practice, can be helpful

223
00:25:27,280 --> 00:25:34,280
in the beginning, but in the end it has to go beyond that, has to come to become wisdom,

224
00:25:34,280 --> 00:25:42,480
to see that the life which we let is simply not desirable, and have the same feeling or

225
00:25:42,480 --> 00:25:46,400
the same understanding but a different feeling, so that we're no longer stressed or scared

226
00:25:46,400 --> 00:25:52,240
or worried about going back, we just don't see the point, we don't wish to go back to

227
00:25:52,240 --> 00:26:00,600
that way, we see the danger but we don't get upset about the danger, and so when confidence

228
00:26:00,600 --> 00:26:07,720
arises it doesn't change our practice, it doesn't stop us in practicing, we have to be careful

229
00:26:07,720 --> 00:26:13,280
when this arises that you're still continuing, so you're confident and you're sure of yourself

230
00:26:13,280 --> 00:26:20,680
but are you continuing, even the anigami, this group of anigami meditators, they have

231
00:26:20,680 --> 00:26:27,120
gotten so far, so they almost, almost perfectly enlightened, almost free from the balance,

232
00:26:27,120 --> 00:26:30,600
and a lot of Buddha came and told them they were all being negligent because they'd

233
00:26:30,600 --> 00:26:39,720
stopped practicing, and so they all continued and became our aham, this is Santa, Santa

234
00:26:39,720 --> 00:26:50,040
or we've got Adimokha, the third one is Upatana, Upatana means establishing, in this case

235
00:26:50,040 --> 00:26:54,920
it means it's another word for mindfulness, so at this point the meditator feels like

236
00:26:54,920 --> 00:26:58,120
they're truly mindful, everything that comes, they're able to grasp it, they're able

237
00:26:58,120 --> 00:27:03,080
to grab everything, everything, everything that comes arises, see, see, see, see, from

238
00:27:03,080 --> 00:27:09,640
the mind is able to be, to grasp everything, and this even goes for things in the future

239
00:27:09,640 --> 00:27:13,600
in the past, if the meditator starts thinking about the past, they're able to remember,

240
00:27:13,600 --> 00:27:19,040
remember, remember, then this happened, and this happened, this is this, this is this, in

241
00:27:19,040 --> 00:27:25,640
the future, thinking this, and this, and this arises, the ability to remember, and to remember

242
00:27:25,640 --> 00:27:31,920
things we have to do in the future and so on, but even in the present moment it can arise

243
00:27:31,920 --> 00:27:36,800
that our mindfulness is very sharp, and we're able to catch everything, and this is a

244
00:27:36,800 --> 00:27:40,360
good thing, there's nothing wrong, but when the meditator becomes attached to this, it

245
00:27:40,360 --> 00:27:45,320
starts to feel like, yeah, I'm really mindful, when they start to think like this,

246
00:27:45,320 --> 00:27:51,680
then they stop being mindful, and they start getting lax and feeling like, ah, I've

247
00:27:51,680 --> 00:27:56,800
come, many people when they reach the stage, they say, I've got what I came for, and

248
00:27:56,800 --> 00:28:01,760
if they're impulse for coming, it's not strong enough, they'll go right back to their

249
00:28:01,760 --> 00:28:05,920
lives, that would die up again, at the fourth stage of knowledge, they'll often leave,

250
00:28:05,920 --> 00:28:11,880
feeling like they've gotten what they came for, and truly they have gotten what they came

251
00:28:11,880 --> 00:28:17,320
for, but the problem is they didn't come for enough, and when they go back they'll have

252
00:28:17,320 --> 00:28:23,320
to fall back into suffering, it's not sure where they will go, they might fall into evil

253
00:28:23,320 --> 00:28:28,480
states and become bad people, because their minds are still not clear, they might fall

254
00:28:28,480 --> 00:28:32,720
into bad companionship, they might fall into great suffering, not being able to deal with

255
00:28:32,720 --> 00:28:56,760
that great suffering, this is 15% here, and we have Opie, the meditator feels calm, it feels

256
00:28:56,760 --> 00:29:01,960
neutral, and then it feels neutral towards everything, whereas before in the third stage

257
00:29:01,960 --> 00:29:06,440
of knowledge or lower, meditator was agitated, and sometimes very upset about the pain and

258
00:29:06,440 --> 00:29:10,680
the aching and the bad things which arise, when they reach this stage, the mind becomes

259
00:29:10,680 --> 00:29:16,680
very peaceful, the mind is not agitated, everything which arises simply passes away, the

260
00:29:16,680 --> 00:29:23,840
meditator is not upset by that, because this knowledge is still weak, it's still a bit

261
00:29:23,840 --> 00:29:30,560
concentration based, and not completely insight based, the meditator is still not able

262
00:29:30,560 --> 00:29:38,320
to let go, the meditator feels themselves enjoying or satisfied by this equanimity, and

263
00:29:38,320 --> 00:29:44,040
they like this stage, because they like this stage they will attach to, and they attach

264
00:29:44,040 --> 00:29:51,920
to it, and they won't be able to be mindful calm, calm, and same as with positive, and

265
00:29:51,920 --> 00:29:56,840
number nine is happiness, and the meditator feels great happiness arise, they will become

266
00:29:56,840 --> 00:30:04,520
attached to this, and they will stop acknowledging.

267
00:30:04,520 --> 00:30:10,880
And the tenth one is simply the attachment itself, it's included here as being a base for

268
00:30:10,880 --> 00:30:17,960
the other nine, so the other nine become uppakilesa, become defilements of insight or hindrance

269
00:30:17,960 --> 00:30:24,040
is imperfect inside, because of this tenth one, and you continue the attachment, and

270
00:30:24,040 --> 00:30:29,120
you have attachment to any of the other nine, or even attachment to anything that arises

271
00:30:29,120 --> 00:30:35,880
at the sixth senses, so we see we hear, we smell, we taste, when these things arise,

272
00:30:35,880 --> 00:30:41,960
when there is nicotine, when there is the attachment to them, they become uppakilesa,

273
00:30:41,960 --> 00:30:47,000
so it's important for meditators to watch out for these things, the good things which

274
00:30:47,000 --> 00:30:54,920
block us from goodness, the block goodness, the good things which have the effect of

275
00:30:54,920 --> 00:31:01,080
hindering the meditator's practice, because the meditator becomes attached and partial

276
00:31:01,080 --> 00:31:05,880
to these things, and of course we pass in as the practice of becoming even partial, so

277
00:31:05,880 --> 00:31:14,440
that we can come to judge things like a true, like a judge, we're judging cases, we judge

278
00:31:14,440 --> 00:31:22,280
fairly, we judge people fairly, we judge situations fairly, we judge phenomena fairly,

279
00:31:22,280 --> 00:31:31,600
and justly, according, and objectively according to their nature, so this is something

280
00:31:31,600 --> 00:31:36,200
which meditators, it's very important for meditators, especially ones going on to practice

281
00:31:36,200 --> 00:31:44,400
and then practice should be aware, so for this reason they've chosen this topic for today's

282
00:31:44,400 --> 00:32:14,240
done, and that's all for today.

