1
00:00:00,000 --> 00:00:06,680
Are there any particular books by Mahasi Sayadal? You would recommend? I was

2
00:00:06,680 --> 00:00:13,440
considering buying progress of insight, the treaties on sati patana meditation.

3
00:00:13,440 --> 00:00:19,720
Yeah, progress of insight is not one I'd recommend. I might teach you even for

4
00:00:19,720 --> 00:00:24,800
bids people to read. Not for bids, but recommends people not to read it

5
00:00:24,800 --> 00:00:32,280
until you've done some intensive meditation. It doesn't mean you should have

6
00:00:32,280 --> 00:00:37,220
actually done a foundation course with a month or something in meditation in

7
00:00:37,220 --> 00:00:41,080
intensive meditation under teacher before you start to read the progress of

8
00:00:41,080 --> 00:00:50,320
insight. It can actually hinder your progress if you thought to be able to

9
00:00:50,320 --> 00:00:55,480
hinder your progress if you read it. So Mahasi Sayadal in the beginning, he thought

10
00:00:55,480 --> 00:00:59,240
that would probably be the case, but when he gets to the end of it, he says

11
00:00:59,240 --> 00:01:02,880
something like, well, it might be useful for beginners as well. And so I'm in

12
00:01:02,880 --> 00:01:06,200
agreement with that. It could be useful, but it's certainly not the one I would

13
00:01:06,200 --> 00:01:11,720
recommend. I used to, I had that printed up and I used to give it out. You don't

14
00:01:11,720 --> 00:01:16,040
need to buy it by the way. You can download it from the internet. Just Google

15
00:01:16,040 --> 00:01:25,280
progress of insight, Mahasi, and you can just print it out. I shouldn't have said that.

16
00:01:25,280 --> 00:01:33,800
I'm a copyright. No, no, it seems great. It is free. It's more like we're not

17
00:01:33,800 --> 00:01:42,280
supposed to encourage people to read this one. So I would print it up and give it

18
00:01:42,280 --> 00:01:46,760
to people when they finish the advanced course with us. I think after they

19
00:01:46,760 --> 00:01:50,320
finish the advanced course, or when they started to do some sort of teacher

20
00:01:50,320 --> 00:01:54,800
training, when they first started that, we would have them read the progress of

21
00:01:54,800 --> 00:02:02,080
insight, kind of recommended or required reading at that point. But you know,

22
00:02:02,080 --> 00:02:05,520
everyone thinks they're advanced. Everyone starts to feel like they're an

23
00:02:05,520 --> 00:02:12,040
advanced level and goes out and reads it anyway. So what I would recommend, the

24
00:02:12,040 --> 00:02:15,720
ones I owe, anything by Mahasi, I have a really, he's just so brilliant and

25
00:02:15,720 --> 00:02:23,280
clear and articulate and well translated. There's just so much good out there.

26
00:02:23,280 --> 00:02:26,040
Every time I pick up one of his books, I can never finish it because I just

27
00:02:26,040 --> 00:02:34,560
want to go and meditate. I'm very particular in my own reading.

28
00:02:34,560 --> 00:02:41,360
Mahasi Sayad is one of the only things that I personally feel the need to

29
00:02:41,360 --> 00:02:45,840
to read more and more. I would recommend his discourse on

30
00:02:49,200 --> 00:02:53,360
Bill, can you turn off the video? I'm getting an echo from you.

31
00:02:53,360 --> 00:02:58,880
Okay, better now. The discourse on dependent or

32
00:02:58,880 --> 00:03:08,480
imagination. The silica suit. They have one friend who highly recommends the

33
00:03:08,480 --> 00:03:17,080
silica suit to everyone. Yeah, it's very good. And it's available free online. I

34
00:03:17,080 --> 00:03:22,960
think both of those are on email dot, email dot org. Have you ever gone to meet

35
00:03:22,960 --> 00:03:33,040
Biko Paisela? He's somewhere in order. If you guys should look him up and see if

36
00:03:33,040 --> 00:03:38,440
he's still alive, he doesn't like me particularly. No, he has problems with me.

37
00:03:38,440 --> 00:03:42,960
No, doesn't like me, but he's taken issue with some of the things I've done.

38
00:03:42,960 --> 00:03:56,560
He's on email dot org as his website. And he got kind of upset at me for this

39
00:03:56,560 --> 00:04:02,360
copyright issue and he said, you know, you should take down what you said and you

40
00:04:02,360 --> 00:04:08,560
should respect people's copyrights and stuff. And I basically said no, but we've

41
00:04:08,560 --> 00:04:13,080
kind of been like that with apparently he has that kind of relationship with

42
00:04:13,080 --> 00:04:21,320
people. I shouldn't say things like that. But yeah, go to email dot org, go to

43
00:04:21,320 --> 00:04:28,160
static dot serimongolo dot org, friend slash Mohasi. I put a bunch of stuff up

44
00:04:28,160 --> 00:04:31,840
there or even go to the, well, that's a better place, but a lot of those were

45
00:04:31,840 --> 00:04:36,720
just taken from Mahasi dot org dot mm or something like that.

46
00:04:36,720 --> 00:04:41,280
That's the Myanmar Mahasi site. There's so many good ebooks of Mahasi stuff.

47
00:04:41,280 --> 00:04:46,000
You don't need to buy anything. Just go and download it on, print it up, worst case,

48
00:04:46,000 --> 00:04:52,400
print it up. I put it on your ebook reader. Everyone has this put it on your tablet,

49
00:04:52,400 --> 00:05:03,000
right? I think I've got about 40 Mahasi books. I'm trying to work way, way

50
00:05:03,000 --> 00:05:10,480
through on an ebook, on an ebook reader or iPad or something. Yeah, I'm on the

51
00:05:10,480 --> 00:05:21,600
drama. We hire discourse. That's a big one. Yeah, there's so much for sure. You

52
00:05:21,600 --> 00:05:24,360
can learn everything you need to know about Buddhism. I think we're reading

53
00:05:24,360 --> 00:05:28,040
his stuff. Well, of course, besides reading the typical, you have to read the

54
00:05:28,040 --> 00:05:33,800
typical as well, but you don't need to go anywhere else in my opinion, except

55
00:05:33,800 --> 00:05:40,080
Mahasi said and stuff. You've read everything by him. You're like, said, I don't

56
00:05:40,080 --> 00:06:06,000
know, but maybe that's going too far. But certainly indispensable.

