1
00:00:00,000 --> 00:00:15,160
understand?

2
00:00:15,160 --> 00:00:25,800
Sir,

3
00:00:25,800 --> 00:00:44,360
I will talk about a discourse given by the Buddha and it is found in the collection of medium

4
00:00:44,360 --> 00:00:58,800
length discourses and this discourse is about the taking heritage.

5
00:00:58,800 --> 00:01:10,880
The name of this discourse is called Dhamma Daya Dha Sutta.

6
00:01:10,880 --> 00:01:17,880
You know Buddha's teachings are divided into three main divisions.

7
00:01:17,880 --> 00:01:25,600
The first one is the rules and regulations for monks and nuns.

8
00:01:25,600 --> 00:01:34,920
And the third division is higher teachings of Buddhist psychology and philosophy.

9
00:01:34,920 --> 00:01:44,560
But the second division, which is the largest, contains the discourses given by the

10
00:01:44,560 --> 00:01:55,960
Buddha to both lay people and monks and they are the popular teachings of the Buddha and

11
00:01:55,960 --> 00:02:07,040
most people know, what most people know about Buddhism comes from these discourses.

12
00:02:07,040 --> 00:02:20,040
And as you know, the majority of these discourses begin with the usual words, A-1-B-Sutam,

13
00:02:20,040 --> 00:02:37,520
does have a heart and in almost every Suddha, an information about that Suddha was given,

14
00:02:37,520 --> 00:02:46,120
where this Suddha was taught and on what occasion it was taught and to whom it was taught.

15
00:02:46,120 --> 00:02:54,120
So most discourses contain these informations.

16
00:02:54,120 --> 00:03:02,520
Take for example the discourse on the benefits of being a recluse.

17
00:03:02,520 --> 00:03:12,560
Now that Suddha was taught by the Buddha as a response to the question put to him by King

18
00:03:12,560 --> 00:03:19,640
H. Atasadhu. That is a fairly long discourse and so it is included in the collection

19
00:03:19,640 --> 00:03:21,600
of long discourses.

20
00:03:21,600 --> 00:03:30,520
Those of you are familiar with the blessing discourse, monk Alasudha.

21
00:03:30,520 --> 00:03:40,880
So monk Alasudha was given by the Buddha, delivered by the Buddha because a deity came

22
00:03:40,880 --> 00:03:46,560
to him during the night and asked him to teach him the blessings.

23
00:03:46,560 --> 00:03:52,920
So it was at the request of their deity that Buddha taught the blessings.

24
00:03:52,920 --> 00:04:02,560
And there are other Suddhas where no occasion for delivering the discourse was given.

25
00:04:02,560 --> 00:04:07,840
So Buddha just talked to the monks.

26
00:04:07,840 --> 00:04:11,520
And this Suddha belongs to that second category.

27
00:04:11,520 --> 00:04:21,680
So nobody asked the Buddha to deliver this discourse and there was no occasion for delivering

28
00:04:21,680 --> 00:04:30,560
this discourse, but Buddha just came and then talked to the monks.

29
00:04:30,560 --> 00:04:42,400
The daily schedule of the Buddha, the first part of the night was reserved for monks.

30
00:04:42,400 --> 00:04:50,880
So during that time monks went to him, asked him questions or requested him to give a meditation

31
00:04:50,880 --> 00:04:52,720
subject and so on.

32
00:04:52,720 --> 00:05:01,840
So that time that period was reserved for monks and so it must be at such a time, such

33
00:05:01,840 --> 00:05:12,200
a period that Buddha delivered this discourse because he and also during the time of the

34
00:05:12,200 --> 00:05:20,800
Buddha monks assembled in the Dhamma hall and maybe waited for the Buddha and they always

35
00:05:20,800 --> 00:05:24,720
have a seat prepared for him.

36
00:05:24,720 --> 00:05:37,400
So he just went into the assembly and then called the monks and then taught what he wanted

37
00:05:37,400 --> 00:05:39,200
to teach.

38
00:05:39,200 --> 00:05:52,000
This Buddha first got the attention of the monks by calling them old monks and the monks

39
00:05:52,000 --> 00:05:58,800
always responded by the words Bhadante, it is like the word Bhante.

40
00:05:58,800 --> 00:06:06,400
So they responded as your reverence and then Buddha taught.

41
00:06:06,400 --> 00:06:20,960
First thing Buddha said was monks be my heirs in Dhamma, not my heirs in material things.

42
00:06:20,960 --> 00:06:34,000
That means be the takers of Dhamma heritage and not the takers of material heritage.

43
00:06:34,000 --> 00:06:44,440
So by this sentence, Buddha wanted his disciples to take the Dhamma heritage, not the material

44
00:06:44,440 --> 00:06:47,960
heritage from him.

45
00:06:47,960 --> 00:06:58,880
Buddha also gave reasons why he wanted his disciples to be heirs in Dhamma and not heirs

46
00:06:58,880 --> 00:07:02,240
in material things.

47
00:07:02,240 --> 00:07:16,320
And then he again taught or exalted them to be heirs in Dhamma with an example.

48
00:07:16,320 --> 00:07:28,480
Then after teaching with an example, Buddha got up and went back to his chamber and when

49
00:07:28,480 --> 00:07:37,040
Buddha left, the Venerable Saribuddha continued the teaching.

50
00:07:37,040 --> 00:07:47,280
So this Buddha contains teachings of two teachers of the Buddha and of the Venerable Saribuddha.

51
00:07:47,280 --> 00:07:50,640
His disciples to be heirs in Dhamma.

52
00:07:50,640 --> 00:07:54,080
Now what is the Dhamma heritage?

53
00:07:54,080 --> 00:07:55,920
What does Dhamma mean here?

54
00:07:55,920 --> 00:08:02,720
Now we meet the Vadhamma again.

55
00:08:02,720 --> 00:08:12,600
It tells us that there are two kinds of Dhamma here and two kinds of material things.

56
00:08:12,600 --> 00:08:22,320
Two kinds of Dhamma are the real Dhamma or the direct Dhamma and indirect Dhamma or the

57
00:08:22,320 --> 00:08:27,840
real Dhamma and roundabout Dhamma.

58
00:08:27,840 --> 00:08:34,760
And the material things also are of two kinds, the direct material things and indirect

59
00:08:34,760 --> 00:08:41,120
material things or the real material things and roundabout material things.

60
00:08:41,120 --> 00:08:46,400
Then Buddha was very emphatic, very clear.

61
00:08:46,400 --> 00:08:51,840
He does not say just be my heirs in Dhamma.

62
00:08:51,840 --> 00:08:59,000
He also said be not my heirs in material things.

63
00:08:59,000 --> 00:09:01,520
So he is very specific.

64
00:09:01,520 --> 00:09:13,440
So now we must understand the Dhamma, the direct Dhamma and the indirect Dhamma is of the realization

65
00:09:13,440 --> 00:09:23,320
of the four noble truths or for the eradication of mental defilements.

66
00:09:23,320 --> 00:09:35,360
And when a person gets enlightenment, then at that moment of enlightenment a type of consciousness

67
00:09:35,360 --> 00:09:42,000
arises in his mind and that type of consciousness is called the path consciousness.

68
00:09:42,000 --> 00:09:52,480
And that consciousness has the power to destroy mental defilements at the same time taking

69
00:09:52,480 --> 00:09:55,480
nibbana as object.

70
00:09:55,480 --> 00:10:03,000
And that path consciousness is immediately followed by fruit consciousness.

71
00:10:03,000 --> 00:10:08,320
It is said that there are two or three moments of fruit consciousness following one

72
00:10:08,320 --> 00:10:12,080
moment of path consciousness.

73
00:10:12,080 --> 00:10:24,080
And there are four types of path consciousness and four types of fruit consciousness.

74
00:10:24,080 --> 00:10:28,560
And this take nibbana as object.

75
00:10:28,560 --> 00:10:35,640
So four types of path consciousness, four types of fruit consciousness and the object

76
00:10:35,640 --> 00:10:47,680
nibbana, came to be called the nine supermunding states or nine supermunding dhammas.

77
00:10:47,680 --> 00:10:59,520
So these are the heritage Buddha wanted to leave for his disciples.

78
00:10:59,520 --> 00:11:07,680
These are called heritage because when the disciples attain these types of consciousness

79
00:11:07,680 --> 00:11:18,680
or these stages of enlightenment, they get free from mental defilements and so they get

80
00:11:18,680 --> 00:11:28,320
free from suffering or they get free from rebirth in the samsara.

81
00:11:28,320 --> 00:11:41,600
And doesn't get the first path and the first fruit consciousness, he is called a sotapana

82
00:11:41,600 --> 00:11:44,000
or stream entrant.

83
00:11:44,000 --> 00:11:54,640
So a stream entrant eradicates some mental defilements altogether once and for all.

84
00:11:54,640 --> 00:12:04,960
And since he has eradicated the mental defilements that would bring him down to four

85
00:12:04,960 --> 00:12:12,400
waffle states, he will not be reborn in these four waffle states.

86
00:12:12,400 --> 00:12:25,640
And also there will be only seven more lives for him to go under he attained final passing

87
00:12:25,640 --> 00:12:29,120
away.

88
00:12:29,120 --> 00:12:39,440
He is called a once returner, no, right, Sagada Gama, yeah, he is called a once returner.

89
00:12:39,440 --> 00:12:43,760
That means he will return to this human world only once.

90
00:12:43,760 --> 00:12:54,000
That means he has only two more rebuts, one rebut in the celestial realm and back one

91
00:12:54,000 --> 00:12:56,600
back to the human world.

92
00:12:56,600 --> 00:13:02,160
So he has only two more rebuts before he attained nibana.

93
00:13:02,160 --> 00:13:12,320
Nibana does not eradicate any mental defilements, but he makes the remaining mental

94
00:13:12,320 --> 00:13:19,760
defilements thinner or weaker.

95
00:13:19,760 --> 00:13:33,880
It doesn't reach as the third stage, the third path and fruit and consciousness.

96
00:13:33,880 --> 00:13:41,640
That means he will not return to this world of human beings and lower celestial beings.

97
00:13:41,640 --> 00:13:46,920
So he will be reborn in the world of Brahma.

98
00:13:46,920 --> 00:14:00,080
So he will have rebut as a Brahma and he may have only one rebut as a Brahma or 2, 3, 4,

99
00:14:00,080 --> 00:14:04,040
or 5 rebut as Brahma.

100
00:14:04,040 --> 00:14:17,560
And this non-returner eradicates sensual desire and ill will.

101
00:14:17,560 --> 00:14:23,400
He doesn't reach as the fourth stage, he is called an Arahan.

102
00:14:23,400 --> 00:14:30,880
So an Arahan eradicates all the remaining mental defilements.

103
00:14:30,880 --> 00:14:40,480
Because he has eradicated all the mental defilements, there is no more rebut for him.

104
00:14:40,480 --> 00:14:53,280
Be my heirs in Dhamma, he wanted us to attain one of these states.

105
00:14:53,280 --> 00:15:04,920
The four fruits and Nibana altogether are called Tarakdhamma here.

106
00:15:04,920 --> 00:15:23,120
The indirect dhamma is the meritorious deeds done with the expectation of getting

107
00:15:23,120 --> 00:15:27,920
out of this round of reburts.

108
00:15:27,920 --> 00:15:34,400
Now when we do meritorious deeds, sometimes we direct our minds to getting out of this

109
00:15:34,400 --> 00:15:42,800
round of reburts and sometimes people may turn their minds to just getting the worldly

110
00:15:42,800 --> 00:15:45,760
benefits, worldly results.

111
00:15:45,760 --> 00:15:53,800
So the meritorious deeds done with the intention of getting out of this round of reburts

112
00:15:53,800 --> 00:16:00,400
is called here indirect dhamma.

113
00:16:00,400 --> 00:16:12,760
Because these meritorious deeds we do with the intention of getting out of the samsara

114
00:16:12,760 --> 00:16:23,040
will ultimately or gradually take us to the realisation of the direct dhamma.

115
00:16:23,040 --> 00:16:28,360
So they are not the direct dhamma, but they can lead us to the direct dhamma.

116
00:16:28,360 --> 00:16:31,040
So they are called indirect dhamma.

117
00:16:31,040 --> 00:16:38,480
Be my heirs in dhamma, he included this indirect dhamma in the dhamma.

118
00:16:38,480 --> 00:16:50,840
So by this exhortation Buddha wanted us to do meritorious deeds with the intention of getting

119
00:16:50,840 --> 00:17:00,720
out of samsara and ultimately to gain enlightenment.

120
00:17:00,720 --> 00:17:12,400
Some meritorious deeds we must be careful, careful that we make aspiration for getting

121
00:17:12,400 --> 00:17:22,720
out of this samsara or for realisation of nevana and not for worldly gains such as longevity

122
00:17:22,720 --> 00:17:27,080
or prosperity or beauty and others.

123
00:17:27,080 --> 00:17:34,840
So when meritorious deeds are done with the intention of getting out of this samsara,

124
00:17:34,840 --> 00:17:44,480
these meritorious deeds can gradually lead us to the realisation of the true dhamma.

125
00:17:44,480 --> 00:17:58,240
Make offerings when you keep precepts, when you offer flowers, perfumes and others to the Buddha

126
00:17:58,240 --> 00:18:02,440
and others.

127
00:18:02,440 --> 00:18:11,200
Then you should direct your mind to nevana or the realisation of the four noble truths.

128
00:18:11,200 --> 00:18:22,600
And also the jhanas people attained through the practice of samsara meditation.

129
00:18:22,600 --> 00:18:30,840
Also when they are directed to the attainment of nevana, then they can lead those two that

130
00:18:30,840 --> 00:18:32,320
attainment.

131
00:18:32,320 --> 00:18:42,040
But if people practice and get jhanas with the intention of worldly benefits, then they

132
00:18:42,040 --> 00:18:50,480
will get worldly benefits only and not the ultimate benefit of getting out of this samsara.

133
00:18:50,480 --> 00:18:53,160
And so material things.

134
00:18:53,160 --> 00:18:58,720
First one is the direct material thing and second the indirect material thing.

135
00:18:58,720 --> 00:19:08,800
So here material things means the four requisites that monks receive.

136
00:19:08,800 --> 00:19:18,000
Now there are four requisites for monks and that they are food, clothing, dwelling place

137
00:19:18,000 --> 00:19:20,000
and medicine.

138
00:19:20,000 --> 00:19:34,440
Because they are necessary because they are, I recall that, they conducive to the

139
00:19:34,440 --> 00:19:43,640
well-being of monks, they are called requisites, they are the ones that monks need to survive.

140
00:19:43,640 --> 00:19:51,640
So without food monks cannot survive, without clothing, without robes, without dwelling

141
00:19:51,640 --> 00:19:55,560
place or without medicine, they cannot survive.

142
00:19:55,560 --> 00:19:59,680
And so these four things are called the requisites for monks.

143
00:19:59,680 --> 00:20:05,920
But actually these four things are requisites not only for monks, for labor will also.

144
00:20:05,920 --> 00:20:08,880
You also need these four, so four requisites.

145
00:20:08,880 --> 00:20:13,560
You need food, you need clothing, you need a house to live in and you need medicine.

146
00:20:13,560 --> 00:20:23,200
So these are the necessary things for survival of human beings.

147
00:20:23,200 --> 00:20:33,920
Now here the material things means these four requisites that monks get.

148
00:20:33,920 --> 00:20:38,800
These requisites to the monks.

149
00:20:38,800 --> 00:20:46,520
These three people, but here these requisites are called Buddha's heritage.

150
00:20:46,520 --> 00:20:51,040
That means Buddha's things, Buddha's possessions.

151
00:20:51,040 --> 00:21:02,160
It is because Buddha allowed monks to accept the requisites offered by lay people and

152
00:21:02,160 --> 00:21:07,040
Buddha allowed lay people to offer requisites to monks.

153
00:21:07,040 --> 00:21:14,400
So although these requisites are actually given by lay people, these are called Buddha's

154
00:21:14,400 --> 00:21:17,480
things, a Buddha's heritage.

155
00:21:17,480 --> 00:21:28,080
The right material thing is, again, meritorious deeds, meritorious deeds done with the

156
00:21:28,080 --> 00:21:35,000
intention of getting just worldly benefits.

157
00:21:35,000 --> 00:21:46,840
So if you do a meritorious deed, and wish for, say, longevity of beauty, of rebirth

158
00:21:46,840 --> 00:21:58,000
in human realm or in the realm of divas and so on, then merit, that meritorious deed

159
00:21:58,000 --> 00:22:02,800
is called indirect material things.

160
00:22:02,800 --> 00:22:11,720
Because although they are meritorious deeds, not things, they lead ultimately to getting

161
00:22:11,720 --> 00:22:14,720
these material things.

162
00:22:14,720 --> 00:22:22,720
They lead ultimately to getting wealth, possessions and beauty, longevity and so on.

163
00:22:22,720 --> 00:22:34,520
And so they are called, here, indirect material things, four kinds of heritage, two

164
00:22:34,520 --> 00:22:40,520
damas, damah heritage and two material heritage.

165
00:22:40,520 --> 00:22:51,280
And among these four, Buddha exerted us to take the damah heritage only and not the material

166
00:22:51,280 --> 00:22:57,240
heritage.

167
00:22:57,240 --> 00:23:09,600
And why did Buddha want his disciples to be heirs and damah not heirs in material things?

168
00:23:09,600 --> 00:23:20,120
Buddha said, I have compassion for you, thinking in what way my disciples will be heirs

169
00:23:20,120 --> 00:23:23,480
in damah and not heirs in material things.

170
00:23:23,480 --> 00:23:29,280
So the main thing is Buddha said, I have compassion for you.

171
00:23:29,280 --> 00:23:41,240
That means if you go after material things, material as heritage, then you will suffer.

172
00:23:41,240 --> 00:24:01,760
If you go for the material things only, then you will do anything, you will break rules

173
00:24:01,760 --> 00:24:13,960
and you will use means, whether just or unjust or you will use means forbidden by the

174
00:24:13,960 --> 00:24:20,240
Buddha to get material things, especially for monks.

175
00:24:20,240 --> 00:24:28,280
And so when you get material things and you will be attached to them and so the attachment

176
00:24:28,280 --> 00:24:33,400
to the material, material things will take you down to the four waffle states.

177
00:24:33,400 --> 00:24:52,320
And even though you are not attached to these material things, because you use unlawful means

178
00:24:52,320 --> 00:24:59,200
to acquire these material things, you will go down to four waffle states.

179
00:24:59,200 --> 00:25:08,920
So the part of material gains is actually the part to suffering.

180
00:25:08,920 --> 00:25:16,240
And so Buddha had compassion for his monks and so he exhorted them not to follow the

181
00:25:16,240 --> 00:25:23,880
part of material gains, but to follow the part of damah.

182
00:25:23,880 --> 00:25:31,600
So this is one reason why Buddha wanted his disciples to be heirs in damah because he had

183
00:25:31,600 --> 00:25:41,400
compassion for them and he did not want them to go astray and to resort to means that

184
00:25:41,400 --> 00:25:45,200
of forbidden by him.

185
00:25:45,200 --> 00:25:54,400
Another reason why he exhorted monks to be heirs in damah and that is if monks are heirs

186
00:25:54,400 --> 00:26:02,920
in material things and not in damah, then they will be censured by people.

187
00:26:02,920 --> 00:26:11,880
They will be reproached, saying although these monks are the disciples of the Buddha,

188
00:26:11,880 --> 00:26:18,920
they do not take the advice of the Buddha and they go after material things and so on.

189
00:26:18,920 --> 00:26:28,440
So Buddha said you will be reproached if you are heirs not in damah, but in material things.

190
00:26:28,440 --> 00:26:37,000
And I will also be reproached because people say this Buddha, he claimed to be an omniscient

191
00:26:37,000 --> 00:26:48,280
one, but he is unable to teach his disciples to be heirs in damah.

192
00:26:48,280 --> 00:26:59,920
So both the monks and the Buddha will be reproached if the monks become the heirs in material

193
00:26:59,920 --> 00:27:09,520
things and not in damah, continued if you are heirs in damah and not in material things

194
00:27:09,520 --> 00:27:14,640
and you will not be reproached and I will not be reproached either.

195
00:27:14,640 --> 00:27:19,600
So you become heirs in damah not in material things.

196
00:27:19,600 --> 00:27:31,280
Another taught with an example, the Buddha said suppose, I have eaten, I mean Buddha said

197
00:27:31,280 --> 00:27:41,280
suppose I have eaten and I finished eating and there is some unsfood left over and that

198
00:27:41,280 --> 00:27:57,880
is to be thrown away and suppose two monks arrived who are hungry and weak, then I will

199
00:27:57,880 --> 00:28:05,280
tell them monks have finished eating and there is some left over and that is to be thrown

200
00:28:05,280 --> 00:28:19,360
away, you may eat it if you like, if you don't eat it then I will throw it away where

201
00:28:19,360 --> 00:28:32,240
there are no green plants and trees or I will drop it into water where there is no life.

202
00:28:32,240 --> 00:28:41,040
That means I would have to throw it away if you don't eat, my thing in this way.

203
00:28:41,040 --> 00:28:48,240
Now Buddha said that he had eaten and there is something left over and he said if I do

204
00:28:48,240 --> 00:28:58,120
not, if we do not eat it and he will throw it away or float it on the water.

205
00:28:58,120 --> 00:29:08,120
So there is a food and I am weak and I am hungry.

206
00:29:08,120 --> 00:29:17,160
But Buddha said that we are not to be heirs in material things but to be heirs in damah.

207
00:29:17,160 --> 00:29:32,720
So following that advice he would avoid eating that food and thus the night and day hungry

208
00:29:32,720 --> 00:29:36,200
and weak, so that is the first monk.

209
00:29:36,200 --> 00:29:49,360
So he will not, he will not take the food because taking the food means, taking the material

210
00:29:49,360 --> 00:29:52,680
things heritage.

211
00:29:52,680 --> 00:29:57,440
Now the second monk he thought differently.

212
00:29:57,440 --> 00:30:03,960
Now there is a food, Buddha had eaten and there is the food left over and if I do not

213
00:30:03,960 --> 00:30:13,440
eat it, Buddha will throw it away and so why not eat the food so that I can get rid

214
00:30:13,440 --> 00:30:20,480
of hunger and weakness and then I will spend the night and day in comfort.

215
00:30:20,480 --> 00:30:32,680
So he took the food and Buddha said among these two monks, the one who did not eat the

216
00:30:32,680 --> 00:30:33,680
food.

217
00:30:33,680 --> 00:30:45,480
But thus the day and night is more, more praiseworthy.

218
00:30:45,480 --> 00:30:55,800
Now Buddha did not blame the one who ate the food but he said the monk who did not eat

219
00:30:55,800 --> 00:31:08,080
the food but avoid eating the food and spend the night with hunger and weakness.

220
00:31:08,080 --> 00:31:14,880
So that monk is more a praiseworthy.

221
00:31:14,880 --> 00:31:24,200
So Buddha monks are praiseworthy but the first monk is more praiseworthy than the others

222
00:31:24,200 --> 00:31:30,000
because when they passed the night and day they did not pass idling, they passed the day

223
00:31:30,000 --> 00:31:32,600
and night in meditation.

224
00:31:32,600 --> 00:31:41,480
So Buddha, both were good monks but Buddha said the first monk is more praiseworthy than

225
00:31:41,480 --> 00:31:44,760
the second monk.

226
00:31:44,760 --> 00:31:56,840
So why was the first monk more praiseworthy than the second one?

227
00:31:56,840 --> 00:32:07,200
His practice of refusing to eat the food will help him in the future to practice fewness

228
00:32:07,200 --> 00:32:17,000
of wishes to practice contentment, to practice scraping away mental defilements and to

229
00:32:17,000 --> 00:32:32,720
be easy to support and to arouse energy when he is discouraged or when he is low in spirits.

230
00:32:32,720 --> 00:32:39,320
And monk is tormented by thoughts of getting more and more.

231
00:32:39,320 --> 00:32:51,160
So when such thoughts arise in him, he could admonish himself saying, you even give up

232
00:32:51,160 --> 00:32:56,520
eating the food and you spend the night hungry and weary at that time.

233
00:32:56,520 --> 00:33:09,840
So why can you not resist this, I recall that, to having many wishes?

234
00:33:09,840 --> 00:33:15,400
So how can you not resist the having many wishes?

235
00:33:15,400 --> 00:33:23,880
And also he may sometimes become not content with what he has, he may want more things

236
00:33:23,880 --> 00:33:25,400
for himself.

237
00:33:25,400 --> 00:33:33,320
And then he can admonish himself like saying himself, you even relinquish eating food on

238
00:33:33,320 --> 00:33:40,760
that occasion and why not can you be content this time and so on.

239
00:33:40,760 --> 00:33:56,360
So he can use that practice or that incident as a means for admonishing him when he is

240
00:33:56,360 --> 00:34:09,960
attacked by such thoughts of desire and thoughts of discontentment or thoughts of mental

241
00:34:09,960 --> 00:34:11,720
defilements.

242
00:34:11,720 --> 00:34:19,480
So that is why this monk is said to be more praiseworthy than the other monk.

243
00:34:19,480 --> 00:34:31,480
And to have few wishes to be content, to scrape mental defilements away and to be easy

244
00:34:31,480 --> 00:34:37,720
to support and to be energetic.

245
00:34:37,720 --> 00:34:51,320
So when a monk wants to act against this training or against this advice, then he can

246
00:34:51,320 --> 00:35:03,280
admonish himself if he had avoided eating that food left over by the Buddha.

247
00:35:03,280 --> 00:35:11,880
That is why Buddha said he was more praiseworthy than the other monk.

248
00:35:11,880 --> 00:35:21,280
So monks are trained to be content and to be easy to support and that is very important

249
00:35:21,280 --> 00:35:23,480
quality in a monk.

250
00:35:23,480 --> 00:35:31,640
So if a monk wants this and that and he is not satisfied with just what he has, he causes

251
00:35:31,640 --> 00:35:37,880
suffering to himself as well as to those who supports him.

252
00:35:37,880 --> 00:35:47,440
So monks should be those who are easy to support, that means who are content with what

253
00:35:47,440 --> 00:35:54,080
they are given and also they are to be content with what they already have.

254
00:35:54,080 --> 00:35:59,960
So these are the two qualities, especially these two qualities monks have to develop.

255
00:35:59,960 --> 00:36:06,640
So Buddha exhorted his disciples to be heirs in Dhamma and not to be heirs in material

256
00:36:06,640 --> 00:36:16,040
things and how many reasons did he get?

257
00:36:16,040 --> 00:36:24,720
Because he had confession for monks, because if they follow the path of material things,

258
00:36:24,720 --> 00:36:27,480
then they will suffer in full vocal stage.

259
00:36:27,480 --> 00:36:35,640
And so he had confession for the monks and so he exhorted them to be heirs in Dhamma and

260
00:36:35,640 --> 00:36:39,200
not to be heirs in material things.

261
00:36:39,200 --> 00:36:49,600
And the second reason he gave was if you don't follow the path of Dhamma, if you are not

262
00:36:49,600 --> 00:36:56,040
heirs in Dhamma, then you will be reproached and I will also be reproached.

263
00:36:56,040 --> 00:36:58,200
So that is a second reason.

264
00:36:58,200 --> 00:37:11,520
And the third is if you follow the path of Dhamma, if you are not an heir or to material

265
00:37:11,520 --> 00:37:20,800
things, then you can admonish yourself leader when thoughts of greed, thoughts of craving

266
00:37:20,800 --> 00:37:27,000
and others come to torment your mind.

267
00:37:27,000 --> 00:37:34,440
So for these three reasons, Buddha exhorted his disciples to be heirs in Dhamma and not

268
00:37:34,440 --> 00:37:36,240
in material things.

269
00:37:36,240 --> 00:37:44,280
Okay, Buddha did not want us to be heirs in material things.

270
00:37:44,280 --> 00:37:53,760
But does that mean that we must not accept any requisites at all?

271
00:37:53,760 --> 00:37:56,720
Without the requisites, we cannot survive.

272
00:37:56,720 --> 00:38:02,880
So we have to accept, we have to have these requisites.

273
00:38:02,880 --> 00:38:12,120
So I think what Buddha meant here was not to accept requisites at all, but to practice

274
00:38:12,120 --> 00:38:20,080
contentment, to practice fewness of wishes with regard to these requisites and not to be

275
00:38:20,080 --> 00:38:32,920
attached to these requisites and not to try to get more and more requisites.

276
00:38:32,920 --> 00:38:43,360
Because when a person or a monk wants more and more, then he will resort to any means,

277
00:38:43,360 --> 00:38:49,600
not only lawful means, but unlawful means also he will resort to.

278
00:38:49,600 --> 00:38:59,040
And so I think here Buddha wanted us to practice restraint with regard to the requisites,

279
00:38:59,040 --> 00:39:05,280
but not to get requisites at all, then that is impossible.

280
00:39:05,280 --> 00:39:09,800
What heritage we will take?

281
00:39:09,800 --> 00:39:24,320
There are four choices, two Dhamma and two material things, which ones do you choose?

282
00:39:24,320 --> 00:39:31,760
Or four?

283
00:39:31,760 --> 00:39:37,760
So the first priority should be the first one, the Dhamma.

284
00:39:37,760 --> 00:39:53,600
And if not that the first one, then the second one, the meritorious deeds with inclination

285
00:39:53,600 --> 00:39:57,600
to get out of this samsara.

286
00:39:57,600 --> 00:40:03,360
And the third one is not for you, it is for monks only.

287
00:40:03,360 --> 00:40:05,880
And what about the fourth one?

288
00:40:05,880 --> 00:40:19,920
Doing meritorious deeds with the intention of getting worldly results.

289
00:40:19,920 --> 00:40:22,640
Now the best thing is the first one, right?

290
00:40:22,640 --> 00:40:26,400
And the next best thing is the second one.

291
00:40:26,400 --> 00:40:37,760
And then the best thing after that is, I think the fourth one, now once my preceptor,

292
00:40:37,760 --> 00:40:45,880
the not Mahasizyara, he was another teacher, my preceptor, he was a very famous preacher.

293
00:40:45,880 --> 00:41:03,960
So in one of his preachings, he said, even if you cannot do the meritorious deeds

294
00:41:03,960 --> 00:41:13,200
in climbing towards the getting out of samsara, you may even do the last one.

295
00:41:13,200 --> 00:41:19,360
Because it is better than doing no merit at all.

296
00:41:19,360 --> 00:41:30,880
And he gave us a analogy, suppose you are drowning, and there is a carcass of an animal

297
00:41:30,880 --> 00:41:34,200
near you, what will you do?

298
00:41:34,200 --> 00:41:40,440
You will hold onto it, or you will let yourself drown.

299
00:41:40,440 --> 00:41:50,200
So when you are drowning in the samsara, even that may be good for you, if there is no

300
00:41:50,200 --> 00:42:07,080
other thing, although the marriage with the intention of getting out of samsara is to be

301
00:42:07,080 --> 00:42:16,040
preferred, sometimes if we cannot get that type of merit, then we may get the other type

302
00:42:16,040 --> 00:42:25,160
of merit, because this merit which leads us back into the samsara.

303
00:42:25,160 --> 00:42:34,600
But it will lead us to better existences of better lives, and so we can change ourselves

304
00:42:34,600 --> 00:42:47,560
when we get to the better lives, and we can acquire meritorious deeds with the intention

305
00:42:47,560 --> 00:42:52,040
of getting out of samsara.

306
00:42:52,040 --> 00:43:13,840
So since the food one is also a type of merit, it is preferable to not doing merit at all.

307
00:43:13,840 --> 00:43:23,800
Having merit with the intention of getting worldly results is better than not doing merit

308
00:43:23,800 --> 00:43:29,320
at all, that is what I said.

309
00:43:29,320 --> 00:43:39,920
So now we are practicing Vipasana meditation, so what heritage are we taking?

310
00:43:39,920 --> 00:43:47,880
The first one, we are trying to take the first one, right?

311
00:43:47,880 --> 00:43:56,960
And the second one, I think Vipasana is also a meritorious deed, and when we practice Vipasana,

312
00:43:56,960 --> 00:44:04,840
we direct our minds to getting rid of samsara, right, to getting rid of mental defilements.

313
00:44:04,840 --> 00:44:12,080
So I think we are taking the second heritage also.

314
00:44:12,080 --> 00:44:24,120
So I think we are the beautiful followers, beautiful disciples of the Buddha.

315
00:44:24,120 --> 00:44:31,560
Buddha concluded his short discourse with the very words he said in the beginning, that

316
00:44:31,560 --> 00:44:41,040
is, monks, be my heirs in Dhamma and not my heirs in material things.

317
00:44:41,040 --> 00:44:54,720
Because I have compassion for you thinking in what way my disciples will be heirs in

318
00:44:54,720 --> 00:44:59,200
Dhamma and not my heirs in material things.

319
00:44:59,200 --> 00:45:09,600
So he concluded, I think it is to emphasize the taking of Dhamma heritage.

320
00:45:09,600 --> 00:45:18,440
So he concluded his short talk with the very words he said in the beginning, is the

321
00:45:18,440 --> 00:45:28,120
Buddha rose from his seat and went into his dwelling, so he caught up and left.

322
00:45:28,120 --> 00:45:34,320
So when the Buddha left, it was the responsibility of the chief disciple to continue his

323
00:45:34,320 --> 00:45:35,320
teaching.

324
00:45:35,320 --> 00:45:44,120
So Saribhoda took up the teaching and gave a talk to the monks.

325
00:45:44,120 --> 00:45:58,680
Why the Buddha left, the reason why the Buddha left was not given.

326
00:45:58,680 --> 00:46:06,040
But in some discourses, the reason was given.

327
00:46:06,040 --> 00:46:21,240
Because the Buddha got up and left saying, I have an ache in my back, so I want to relax.

328
00:46:21,240 --> 00:46:24,120
And then he would leave.

329
00:46:24,120 --> 00:46:33,240
But here Buddha did not see anything, so he didn't suffer any back ache here.

330
00:46:33,240 --> 00:46:44,120
To give chance to the venerable Saribhoda to teach the monks, so he left.

331
00:46:44,120 --> 00:46:55,520
The venerable Saribhoda addressed the monks saying, friend monks, or friend monks, then

332
00:46:55,520 --> 00:46:57,480
they replied, friend.

333
00:46:57,480 --> 00:47:03,600
Now, there is a difference between the Buddha and the disciples.

334
00:47:03,600 --> 00:47:08,600
When the Buddha addressed the monks, the Buddha just said, oh monks.

335
00:47:08,600 --> 00:47:11,840
And then they would say, when the Buddha saw.

336
00:47:11,840 --> 00:47:20,200
But when he decided, like Saribhoda addressed the monks, he said, friends, monks, not

337
00:47:20,200 --> 00:47:21,200
just monks.

338
00:47:21,200 --> 00:47:23,520
So he used a word friend.

339
00:47:23,520 --> 00:47:28,600
And then they responded with the word friend, again, old friend.

340
00:47:28,600 --> 00:47:38,640
So during the time of the Buddha, monks call each other, let's say, friend in Pali Ausu.

341
00:47:38,640 --> 00:47:51,960
So whether the monk is older than him or younger than him, they would address him as friend.

342
00:47:51,960 --> 00:47:56,080
But after the death of the Buddha, it was changed.

343
00:47:56,080 --> 00:48:03,080
And a young monk never called an older monk friend, we call Bhante of anable Sara.

344
00:48:03,080 --> 00:48:08,360
And the older monk or young monk Ausu, that means a friend.

345
00:48:08,360 --> 00:48:18,760
So during the time of the Buddha, this different form of address was not introduced yet.

346
00:48:18,760 --> 00:48:24,480
So only after the death of the Buddha, actually, the Buddha gave instructions to monks that

347
00:48:24,480 --> 00:48:31,760
after my passing away, you should address the younger monks, you should address older monks

348
00:48:31,760 --> 00:48:37,520
by the word Bhante and older monks, you should address the younger monks by the word Ausu.

349
00:48:37,520 --> 00:48:42,120
That means the Venablesa and friend, yeah, that's in Vinaya.

350
00:48:42,120 --> 00:48:50,560
So here, the Venable Saribo said, friends monks and then they said, friends, that there

351
00:48:50,560 --> 00:48:55,640
may be those who are younger than the Venable Saribo in the audience, but they just say,

352
00:48:55,640 --> 00:48:56,640
friend.

353
00:48:56,640 --> 00:49:00,160
So I got the term, no.

354
00:49:00,160 --> 00:49:19,000
And then the Venable Saribo continued the Dhamma talk, teaching them about the three kinds

355
00:49:19,000 --> 00:49:34,920
of seclusion and then telling them the evil states that are to be abandoned and also

356
00:49:34,920 --> 00:49:45,680
telling them that there is a middle way for the abandonment of these mental defilements.

357
00:49:45,680 --> 00:49:59,400
So we will hear Saribo there tomorrow, what he said.

