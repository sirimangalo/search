1
00:00:00,000 --> 00:00:07,200
noting emotions. In the case of emotions, is it okay to just note feeling or is it better

2
00:00:07,200 --> 00:00:14,160
to label the specific emotion? Emotions are complex entities, they generally contain

3
00:00:14,160 --> 00:00:21,040
fleeting mental component and a more lasting physical effect. It is important to separate

4
00:00:21,040 --> 00:00:27,440
these two respects in order to clearly see the reality of the experience. For example,

5
00:00:27,440 --> 00:00:34,480
anxiety exists only momentarily in the mind, but sometimes appears to persist due to the physical

6
00:00:34,480 --> 00:00:41,440
reaction triggered by it. The actual emotion, like anxiety, disliking, liking, fear,

7
00:00:41,440 --> 00:00:49,120
worry, etc, should be noted by name for the brief moment that arises. The physical aspect of

8
00:00:49,120 --> 00:00:59,120
each should be distinguished as being simply feeling or in certain cases pain, pleasure or calm.

