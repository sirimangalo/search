1
00:00:00,000 --> 00:00:12,380
Okay, good evening, everyone. Welcome to our live broadcast, broadcasting live in second

2
00:00:12,380 --> 00:00:24,060
life, live on our website via audio and recording for upload to YouTube. And of course

3
00:00:24,060 --> 00:00:34,360
live here in Hamilton to the live studio audience. We're overfull. We now have seven

4
00:00:34,360 --> 00:00:41,280
people staying in this house. No, do we? We have six people right now and tonight we're

5
00:00:41,280 --> 00:00:52,360
going to have a seventh, supposed to be coming. We're overfull. We just can't turn people

6
00:00:52,360 --> 00:01:03,240
away. Just can't keep people. They're breaking down the doors to get in. That's how passionate

7
00:01:03,240 --> 00:01:08,880
people become. Religion, religion is such a powerful thing. Know the word religion has

8
00:01:08,880 --> 00:01:15,420
such a bad reputation or such a specific reputation that makes a lot of people uneasy

9
00:01:15,420 --> 00:01:21,020
when they hear the word. So I think it's important to reform the word. Religion means taking

10
00:01:21,020 --> 00:01:29,940
things seriously. So tonight's talk is about someone who, well not actually so much about

11
00:01:29,940 --> 00:01:41,920
him hopefully, but it involves the discussion with someone who took Buddhism quite seriously,

12
00:01:41,920 --> 00:01:49,560
took the Buddha's teachings quite seriously. And he stands out as someone who is remarkable

13
00:01:49,560 --> 00:02:02,880
in terms of taking the Buddha's teaching seriously. He was designated by the Buddha as foremost

14
00:02:02,880 --> 00:02:09,800
of the Buddha's disciples who went forth, left the home life out of faith, out of confidence.

15
00:02:09,800 --> 00:02:20,940
So I had a religious sense of religiosity. His name was Raktapala. You know the story of

16
00:02:20,940 --> 00:02:34,320
Raktapala. It comes in the Gmanikaya, so it's 82. It's called Raktapala. So the story

17
00:02:34,320 --> 00:02:40,560
goes, Raktapala, heard the Buddha's teaching and realized that it was quite difficult

18
00:02:40,560 --> 00:02:46,440
for him to practice it while living at home. He thought to himself, wow, the sort of

19
00:02:46,440 --> 00:02:51,360
teaching isn't the kind of thing you can do or surrounded by sensuality and caught up

20
00:02:51,360 --> 00:02:59,760
by daily affairs of the quote unquote real world. It's not the real world, but what people

21
00:02:59,760 --> 00:03:07,640
would call the real world. The ordinary mundane contrived artificial world of society that

22
00:03:07,640 --> 00:03:18,000
we've put together to help us achieve our sensual, our goals of sensual pleasure. So useful

23
00:03:18,000 --> 00:03:28,280
for becoming enlightened, too busy, to caught up in the fountain. So you decided you wanted

24
00:03:28,280 --> 00:03:36,280
to go forth. He wanted to become a monk, but he asked the Buddha to ordain him and the

25
00:03:36,280 --> 00:03:42,800
Buddha asked, do you have your parents permission? And he said, no, I don't have my parents

26
00:03:42,800 --> 00:03:47,480
permission. But I said, well, I don't ordain people who don't have their parents permission.

27
00:03:47,480 --> 00:03:53,440
And so I had to go back to get his parents permission to make a long story short. His

28
00:03:53,440 --> 00:04:03,040
parents didn't give permission. And he laid down on the floor. He pleaded in bed with them,

29
00:04:03,040 --> 00:04:08,160
but eventually he laid down on the floor and said, I'm not going to eat or drink or do

30
00:04:08,160 --> 00:04:17,040
anything. I'm not going to get up off this floor until you allow me to ordain. And

31
00:04:17,040 --> 00:04:21,400
so they waited him out for a while, but then he made good on this threat and he just

32
00:04:21,400 --> 00:04:27,240
lay there and started starving to death. And they called his friends to try and convince

33
00:04:27,240 --> 00:04:30,240
him. And his friends came over, talked to him and then went and talked to his parents

34
00:04:30,240 --> 00:04:35,800
and said, look, he's pretty serious about this. How about this? If you let him ordain,

35
00:04:35,800 --> 00:04:42,720
at least you'll get to see him alive. You can go and visit him. But if you don't let him

36
00:04:42,720 --> 00:04:48,320
ordain, he's going to die. Then you won't see him. Then he'll be gone. So let him

37
00:04:48,320 --> 00:04:53,720
eat. And the story goes on and on. But tonight I didn't want to talk so much about his

38
00:04:53,720 --> 00:05:03,840
story as interesting as inspiring as it is. Later on in his life, he was living, I can't

39
00:05:03,840 --> 00:05:14,000
remember where he was living, but he went somewhere and met with a king. And King asks

40
00:05:14,000 --> 00:05:23,520
him, I can see if I can find a corabi. Yeah, okay. I was going to say corabi. Yeah. What

41
00:05:23,520 --> 00:05:26,720
was the name of this king on the name of the place where the king lived? I think he was

42
00:05:26,720 --> 00:05:37,840
actually the name of the place. And the king came to see him and he said, they're at

43
00:05:37,840 --> 00:05:43,840
Apollo. He said, you know, I know people who leave the home life and go to the forest to

44
00:05:43,840 --> 00:05:53,200
do their religious thing. And they do it for one of four reasons. They do it because they've

45
00:05:53,200 --> 00:06:02,600
got an old. They do it because they've gotten sick. They do it because they've lost wealth or

46
00:06:02,600 --> 00:06:12,760
they do it because they've lost their relatives. So someone who was old can no longer work

47
00:06:12,760 --> 00:06:27,840
and is feeble and can't find any sort of solace or status or work or activity in the world

48
00:06:27,840 --> 00:06:32,800
of young people and so they go off into the forest and become a religious person. This

49
00:06:32,800 --> 00:06:37,440
actually happens even in Buddhism. Old people become monks and it's somewhat troublesome

50
00:06:37,440 --> 00:06:42,000
because they have to get sick. And that's the second one is people get sick and then they

51
00:06:42,000 --> 00:06:48,080
want to go off to the forest while there's not much they can do in India at the time. Nowadays

52
00:06:48,080 --> 00:06:52,360
we just put them in old age homes and forget about them. That same sort of thing. They didn't

53
00:06:52,360 --> 00:07:04,760
have old age soldiers sent them off to the forest to live or to die. Or someone who loses

54
00:07:04,760 --> 00:07:09,080
wealth and become impoverished and have to become a beggar and have to live under a tree

55
00:07:09,080 --> 00:07:22,520
or in a ditch or something. Also come and then also happens now. Or throughout loss of relatives

56
00:07:22,520 --> 00:07:28,520
relatives who maybe took care of the person or maybe just out of grief they would realize

57
00:07:28,520 --> 00:07:35,920
the suffering of life and leave the world and he said, but you are young, you're healthy.

58
00:07:35,920 --> 00:07:42,560
You come from a good family and they're all still alive. What was it that caused you

59
00:07:42,560 --> 00:07:50,360
to leave home? Why did you leave behind the world? It's a good question with all the wonderful

60
00:07:50,360 --> 00:07:54,440
things that we have in the world. Why do we leave it? Why do we come here? Why do you come

61
00:07:54,440 --> 00:08:02,120
here to torture yourself? Even just to meditate people who go to the monasteries, temples,

62
00:08:02,120 --> 00:08:11,240
churches, when something bad happens? So it's remarkable to see someone who has their

63
00:08:11,240 --> 00:08:19,640
whole life ahead of them. Couldn't be capable of so much in the world and says, why

64
00:08:19,640 --> 00:08:22,760
we're at the palace, parents wouldn't allow him. They were quite upset that he wanted

65
00:08:22,760 --> 00:08:29,320
to throw away his wonderful future. All the wonderful and good things he can do, right?

66
00:08:29,320 --> 00:08:38,920
I wanted to do this useless thing on becoming a reckless, becoming a Buddhist monk. How

67
00:08:38,920 --> 00:08:47,040
useless they thought. And right to the palace, the right to the palace answers is well

68
00:08:47,040 --> 00:08:54,160
known in the Buddhist dispensation. He claims that it comes from the Buddha. I'm not sure

69
00:08:54,160 --> 00:08:58,480
if we actually have somewhere where the Buddha said this, we very well may have, but

70
00:08:58,480 --> 00:09:03,600
right to the palace the one is known for giving it. He says it comes from the Buddha. He said

71
00:09:03,600 --> 00:09:12,440
there are these four dhammodesa. And today it comes from this, this means to to indicate

72
00:09:12,440 --> 00:09:21,720
dikka and desa, it's the same. So the things that Buddha pointed out, four dhammodesa, four

73
00:09:21,720 --> 00:09:31,560
indicators, dhamma indicators. And when these were pointed out to me, I realized that I

74
00:09:31,560 --> 00:09:40,440
had to leave. I had to do something. I think these are quite useful, not just for encouraging

75
00:09:40,440 --> 00:09:45,960
people to take up meditation or take up Buddhism, but also to encourage us in our practice

76
00:09:45,960 --> 00:09:52,040
and to remind us why we're here when we get discouraged. And to sort of focus our attention

77
00:09:52,040 --> 00:10:01,440
on what's really important, clarify and keep us on track. So the first is Upaneeti

78
00:10:01,440 --> 00:10:17,400
Loco Adhu. The world is uncertain, the world is unstable. So all these good things, I mean

79
00:10:17,400 --> 00:10:21,880
just the fact that those losses exist, you don't need to experience the loss to know that

80
00:10:21,880 --> 00:10:30,200
that's a part of life and to know that it's a danger and to start to question, what am

81
00:10:30,200 --> 00:10:37,200
I doing here? Wasting my time when I certainly won't be prepared for for old age, for sickness,

82
00:10:37,200 --> 00:10:49,760
for loss, for death, and the uncertainty of life, all the things that we hold dear, even

83
00:10:49,760 --> 00:10:59,040
our own selves, even our own families, our own situation, everything about us. It's unpredictable.

84
00:10:59,040 --> 00:11:15,840
It's chaotic. It's the first reason why one would go off and become a monk or become

85
00:11:15,840 --> 00:11:28,640
or go off in practice meditation. The second one is Atana Ologo, Anabhi Saru.

86
00:11:28,640 --> 00:11:43,160
This world has no refuge, no master or guardian protector. And the King questions him and

87
00:11:43,160 --> 00:11:50,400
all these. He says, what do you mean? It seems very constant. Here I'm the King and life

88
00:11:50,400 --> 00:11:57,480
is very constant for me. They say, well, do you remember when you were young and you could

89
00:11:57,480 --> 00:12:05,000
shoot an arrow or a rhino horse? Can you do that now? In permanent, you see? Old age comes

90
00:12:05,000 --> 00:12:09,920
to us all seeing this and knowing that this is the case.

91
00:12:09,920 --> 00:12:13,840
And then he says, well, what about this? No protector. What do you mean? I've got elephants,

92
00:12:13,840 --> 00:12:19,000
I've got warriors, I've got lots and lots of refuge in protectors. He said, well, what

93
00:12:19,000 --> 00:12:22,920
if you get sick, suppose you get really sick, your elephant's going to protect you from

94
00:12:22,920 --> 00:12:28,640
the sickness? Is your family, your warriors are going to stand around you and keep the sickness

95
00:12:28,640 --> 00:12:38,480
away? No, there's nobody who can protect you. When we're young, this is one of the big

96
00:12:38,480 --> 00:12:43,960
shadowing of youth, the realization that our parents can't protect us from suffering is

97
00:12:43,960 --> 00:12:58,960
a shock of growing up. Realization that suffering is, that we are vulnerable to suffering

98
00:12:58,960 --> 00:13:02,360
and that we're protected from it. We're not sheltered from it. There's nothing that

99
00:13:02,360 --> 00:13:23,040
I can shelter for, that's from it. Number three, a sakoloko, sambangpahaya, gaman yandhi. This world

100
00:13:23,040 --> 00:13:33,920
is not an owner. There's no ownership. There's nothing of its own. It goes, having abandoned

101
00:13:33,920 --> 00:13:42,280
everything, the kings and what do you mean? I can go with everything, full jewels and

102
00:13:42,280 --> 00:13:49,520
we have so much that is ours, right? We've got our cars and our houses and our families

103
00:13:49,520 --> 00:13:56,760
and our friends and our iPhones and our computers and everything. That's so much, it's

104
00:13:56,760 --> 00:14:04,480
ours. We go with it. No, you don't go with any of it. He says, you know whether you're

105
00:14:04,480 --> 00:14:09,040
going to be king in the next life, whether you're going to be human in the next life

106
00:14:09,040 --> 00:14:16,080
to be even enjoy the riches, maybe you'll be born a hungry ghost guarding your own treasure

107
00:14:16,080 --> 00:14:33,680
or maybe you'll be born an ant or a termite living in the wood wall of the palace. Not

108
00:14:33,680 --> 00:14:39,240
just in the next life. All of our possessions, right? We can't hold on to them. We can't

109
00:14:39,240 --> 00:14:46,960
hold on to them. Let this be mine forever. Let this car, this house, or this iPhone. Let

110
00:14:46,960 --> 00:14:55,760
it be mine. Even our friends and family and even our own bodies don't really belong to

111
00:14:55,760 --> 00:15:00,600
us. We're borrowing them. We're borrowing them because we don't know when they're going

112
00:15:00,600 --> 00:15:07,480
to be taken back. We're borrowing them. It's like we've stolen them and we're just waiting

113
00:15:07,480 --> 00:15:13,840
for the police to come and arrest us. Wait until we get caught. It's like we're living

114
00:15:13,840 --> 00:15:19,880
on stolen time. So we've got to get as much pleasure and use out of it. Joy ride in this

115
00:15:19,880 --> 00:15:30,880
body that we have and then death comes and catches us and wham. Takes it away. We don't know

116
00:15:30,880 --> 00:15:37,440
when that's going to be. We're just trying to run away for on the lamb. We're on the

117
00:15:37,440 --> 00:16:00,880
run. Death is on our heels. It's a good analogy. Number four, uno, loco, atito, tanhara dasu. Una.

118
00:16:00,880 --> 00:16:11,920
Una is the same. That's where the word want, want and own from the same place. Una means

119
00:16:11,920 --> 00:16:28,240
wanting. This world is wanting is lacking. Atita. Unsatisfied. Tanhara dasu. The slave dasu

120
00:16:28,240 --> 00:16:40,800
has a slave of them of craving. Una means, what do you mean? I can satisfy myself any time.

121
00:16:40,800 --> 00:16:50,880
I get whatever I want. I'm always satisfied. I eat and that I'm satisfied. Una says,

122
00:16:50,880 --> 00:16:55,920
well, what if you heard about another kingdom that was weak but had lots of resources

123
00:16:55,920 --> 00:17:03,040
and they told you that it was ready for the taking. What would you do? I'd conquer it.

124
00:17:03,040 --> 00:17:12,240
What about another one and another one? I never have enough. People just said, it could

125
00:17:12,240 --> 00:17:22,640
rain gold, it could rain gold or precious jewels and the shower of them would never be enough

126
00:17:22,640 --> 00:17:28,480
for a human, for one being. It's a nature of craving. The very nature of craving

127
00:17:28,480 --> 00:17:43,120
aside its habitual, its ever-increasing. It's self-perpetuating or self-enforcing or it feeds itself.

128
00:17:43,120 --> 00:17:53,040
It becomes stronger the more you engage in it. It's a very scary thing. We're always unsatisfied.

129
00:17:55,280 --> 00:18:04,800
We can never be satisfied. Seeing this many people leave home when they realize how

130
00:18:04,800 --> 00:18:10,400
unsatisfied they are. They can eat and eat and all their gaining is more and more craving.

131
00:18:10,400 --> 00:18:17,120
It's one of the big things you realize here is how attached we are to diversion,

132
00:18:18,000 --> 00:18:27,600
how unable we are just to be with ourselves, just to have the patience to be such a simple thing.

133
00:18:27,600 --> 00:18:32,160
They go that easy, but isn't it ridiculous how we can't just be,

134
00:18:32,160 --> 00:18:47,440
we can be ourselves. We have to do, make, create, get. It's quite surprising because I think

135
00:18:47,440 --> 00:18:54,480
we generally have this idea that we can just be that we always are. We're not good at

136
00:18:54,480 --> 00:19:04,800
arming. We're not good at being. We have to do. We have to get so much craving and aversion

137
00:19:06,480 --> 00:19:15,360
of so many in-built reactions to everything. So realizing this, realizing all four of these things,

138
00:19:15,360 --> 00:19:23,760
this is what needs one to go forth, to become a monk, to practice meditation. This is the

139
00:19:23,760 --> 00:19:32,800
encouragement, the reminder to all of us that we're here for a reason. We're here to figure this

140
00:19:32,800 --> 00:19:40,560
out. We're going to solve this problem. The problem of impermanence and that the world, any goal,

141
00:19:40,560 --> 00:19:49,120
we might attain any object we might acquire. Anything we might become will always be

142
00:19:49,120 --> 00:20:00,640
unstable, uncertain impermanence. There's nothing that can keep us from the vicissitudes in life,

143
00:20:00,640 --> 00:20:04,640
there's no protection, there's no refuge, there's nowhere we can go, nothing we can do

144
00:20:04,640 --> 00:20:21,680
to avoid the vicissitudes in life. There's all of our possessions can protect us, all of our

145
00:20:24,480 --> 00:20:31,040
friends and family. We leave them all behind. And all the things we hold dear and all the things

146
00:20:31,040 --> 00:20:38,400
we crave and love and cling to are not ours. They'll be gone. We'll be gone from them soon enough.

147
00:20:42,640 --> 00:20:47,600
And finally, that craving, we're slaves of craving, slaves of desire.

148
00:20:49,440 --> 00:20:53,840
We are not masters of our desire. Our desires are not our possession that we can control.

149
00:20:53,840 --> 00:21:00,240
They are the masters where we are the slave. Very scary reality. And the scariest, these scary realities

150
00:21:01,760 --> 00:21:07,040
encourage us because there is something that can protect us. There is a refuge and that's the

151
00:21:07,040 --> 00:21:15,200
number that's the truth, reality, seeing things as they are. Once we learn how to just be

152
00:21:15,200 --> 00:21:28,400
when we become content, we become stable, we become invincible. There's no coming or going,

153
00:21:29,760 --> 00:21:31,360
and we fear ourselves from suffering.

154
00:21:35,680 --> 00:21:39,920
So there you go. There's the dhamma for tonight. Fine teaching,

155
00:21:39,920 --> 00:21:47,360
and then on by Radhapana, Buddhist monk with the greatest faith and the greatest confidence

156
00:21:48,560 --> 00:21:53,920
in going forth. No doubt in his mind. He would have done anything, he would have died

157
00:21:54,480 --> 00:22:00,240
just to do what we're doing. It's a good example of

158
00:22:00,240 --> 00:22:07,520
a religiosity and how wonderful and powerful it can be.

159
00:22:07,520 --> 00:22:37,360
So there you go. Thank you for tuning in. Have a good night.

