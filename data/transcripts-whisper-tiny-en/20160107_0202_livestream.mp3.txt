Okay. Good evening. Good evening, everyone. I don't know when exactly it starts recording,
but broadcasting live January 6th, 2016. Back on with our weekly meditations at McMaster and we're
going to keep trying with them with three times a week actually. And next week we'll have our five
minute meditation lesson. I've got someone helping me out with that, hopefully. I have to test
it. I haven't done a five minute meditation. I've never taught meditation in five minutes. Not
formally. So I'll have to figure out what's my five minute. What's my elevator pitch? Because
what they call it. So we're going to set up. We're going to set up mats and sorry I didn't hear you.
You can practice with us. It's a five minute. Good idea.
Okay. So the basis of meditation is about objectivity. I don't know if I want to get too
philosophical because they shut out. But it's we're trying to see things as they are.
We're trying to see things objectively without reacting to them. So what we're going to do
is we're going to remind ourselves, in general, this is this. So pain is pain. Thoughts are
thoughts. Anger is anger. Depression is depression. And we're not going to react to anything.
Because you see when you react to things, this multiplies them and continues them and they
snowball out of control. So when you experience pain, if you could just experience an
ex pain, it doesn't end up bothering you. If you, when you feel angry and then you say angry,
you remind yourself that you're angry and it doesn't turn into a fight or a problem. It doesn't
make you want to do bad things or say bad things. If you have a thought or someone shouting at you
and you just see it as a thought or as a sound, just as the experience of it, then it won't hurt
you. How many minutes is that? I'm sorry, I was hoping. Well, the last 10 years had a counter on
your side of the hangout. I'm sorry. That was about a minute, I think, right? A couple minutes.
Okay, so then, so here's what we're going to do. Close your eyes.
And maybe I'm going to tell them to close your eyes. First, I have to say how we do it. So
how we do this is just simply reminding ourselves. You pick a word that reminds you what's going on.
Like for instance, if you feel pain, you remind yourself just with one word, pain.
And that becomes your meditation. You'd say to yourself, pain, pain. I don't think I'm having
a close to your eyes yet. I think this is still eyes open. I made a mistake. So is it pain, pain?
If you have a thought, you just say thinking, thinking, if you hear a sound like all there's
noise of these people, because we're right in the middle of the hallway, really. It's kind of funny.
Is it hearing hearing?
And so this is the skill that we're trying to cultivate. So the technique that I'm going to show
you to do this is to train you to do this. So we pick something that's neutral. We'll pick our breath.
Focus on the breath. When your breath comes into your body, your stomach will rise. When your breath
goes out of the body, your stomach will fall. If you don't feel it, you can put your hand there.
But you just say to yourself, rise, see, fall. You can close your eyes and do it. You don't say it out loud.
You just remind yourself in your mind, rise. And that trains you to do that. Once you can do that,
then you begin to extrapolate it into, apply it in the rest of your life. So let's try that now.
And then we'll have to do that. After a minute or so, probably have one minute left,
then I'll say, okay, and then, and now, apply the same concept of seeing your stomach move clearly.
Apply it to things that really matter. Like when you feel pain, remind yourself pain,
when you have thoughts, remind yourself thought thinking thinking. When you have emotions,
remind yourself angry, frustrated, bored, sad, worried. When you want something wanting,
when you like something like kings. When you hear sounds, hearing, hearing, seeing, seeing.
And see, the thing is, we don't have a time limit. It's whatever they want to get up. If they
want to sit there and talk to me for a while, well, I think that's fine. But I'm not sure how many
mats to have. I think we'll have to experiment because I think it's going to be too noisy for
people to be too far away. So it might be like me and two other people at a time. Any more than
that, and it might get too hard for them to hear. But I think we're going to set up a nice carpet,
rug or something. And, and real sitting mats, and then tell people to take their shoes off.
Are we going to do that? I don't think there's any way around getting to take their shoes or boots off.
Or maybe the yoga mats, if a carpet is hard to come by.
Right. We could just put out a few yoga mats and just leave it at that, right?
Yeah, we'd be some for four yoga mats together or something. The other thing is how to get
all this to the university. You know, the sign. I guess it's not really
carryable at that point. See, we're supposed to have a locker. I have to tomorrow I'm going to
find out about our locker. Why we don't have a locker for our club? Because every club should have
a locker. But we don't. Yet that I know. Anyway, that's future plans here. So that seems
doable, no? Yes, and I'm your timing after I realized that I wasn't timing it for you.
That was two minutes. That was only two minutes. So what was two minutes?
The from the point that I realized that from the point that I hadn't been timing it
from there to the end, it was two minutes. So I think you were under five minutes altogether
because the whole I can have was only two minutes. Good. Did I leave anything out? No?
All right. People will have questions probably about any of the changes. It's misleading.
The sign is misleading. False advertising. It's only a two minute meditation.
No, that gets the more time to actually meditate. Yeah. Yeah, it's part of the lesson.
That's going to be great. Funny helps. I got to get someone to take pictures.
Maybe we'll make a video of it. Put it on YouTube.
I should go on the silhouette actually. I should talk to the silhouette the McMaster University
newspaper and the radio as well. Put on a press release. That's what I'll do.
We have to put on a press release. We did this for the meditation. We did this for the
piecewalk. And they interviewed me on radio.
So questions huh? We have questions. This one wasn't exactly directed to you but it's probably
a good one to answer anyway. What I don't know what I don't know yet is. Why do you call him
Bante instead of you to Dama? Yeah, but don't answer this one like every week.
No, it just seems that way. I have answered it for this new people all the time. Sorry.
There are only people all the time. New people joining this group all the time.
But those questions you think people can answer amongst each other.
Someone else can answer it. Okay. Bante, I was very glad to hear that you get to go see your
teacher again. Would it be possible for us to come together as a community and put something
together for you to offer to Ajahn Tom? And if so, what would be appropriate? Thank you, Bante.
That'd be great. I'm not really thinking that I'm going to go. It was, you know, it's been bugging
me that I haven't talked to him about this new monastery but it's not really worth flying across
the world to do it. It's not, you know, it's not like I've got a free ticket to go so it would
have to be paid for which is a considerable expense.
So yeah, absolutely. If I go, let's, for sure, let's keep that in mind but it probably won't be.
I don't know. We'll see because there's talk that they might support summer or some of the ticket
anyway. I'll see if it's really, there's a legitimate conference going on as well.
But the only real reason to go would be to talk to my teacher and he already knows about the place.
I just haven't told him myself, haven't talked to him, haven't gotten his instructions in his
blessing and all that.
Did the book in now, why do you never exist?
I think the word, I think why is problematic. It's a question that doesn't, it's like asking an
innocent man why why why he beats his wife. It's like asking why does a tree exist?
What is the purpose of a tree? It's a ridiculous question. It's a sort of a
theistic question really because the only answer is God.
It's a very human thing to want to find a purpose for things.
We differentiated ourselves from ordinary animals by our ability to cultivate tools, right?
So the idea of things having a purpose and if things don't have a purpose we destroy them or we
get rid of them, we remove them. We've had a very utilitarian bent to us in the sense.
So there's no reason to think that anything has a purpose.
I mean I guess if the question were instead, how is it that the universe exists?
I don't know whether the Buddha knew or not, but again I think it's probably a bad question.
It's incomprehensible to us that there shouldn't be an answer to that, but I think that's our
problem. I think it's just the way we think. I have anxiety issues and once I've become too anxious
about something for a few days continuously, it will get harder for me to meditate and eventually
I stop meditating until the thing I'm worried about is subsided. How can I make sure I don't
stop meditating even when I'm anxious about something? I should meditate on the anxiety. Look up
some of the videos I've done on anxiety particularly because it's an interesting one. Anxiety
it's fairly easy to see that a lot of it's just physical and physical aspects of anxiety aren't
anxiety. If you go to video.series it is a video, what is the website?
Yes. Video.surimonglo.org we got a whole bunch of my videos categorized and I think anxiety is
under mental problems or something. Yes and the other thing you can do is lie down to lying meditation
but definitely meditate on all the aspects of what you call anxiety because much of it is physical
in the anxiety only lasts a moment and if you're patient and keep noting anxiety then you're
doing very well. You're learning a lot, you're studying the anxiety and you're meditating so it
doesn't actually interrupt your meditation if it becomes a part of your meditation.
The thing that you're anxious about, note that thing, thinking, thinking over there.
How do I balance my mind between light terms and higher states of awareness through meditation?
I look around me and see people unaware of life itself and they seem to be in a permanent state
of sleep. I'm finding this very difficult. People seem to be more focused on self gain rather than
local gain. Well there's no balancing them, they're their opposites. The dhamma goes in the opposite
direction of material gain. So the conflict that you face is to choose in the end one or the other.
But I mean that's in the end if you decide that you want to become a monk or something.
What exactly does reflection of an action mean in the core of today? Thanks.
We have to read the whole suit there. Reflection here means before you do something,
before you do something you should reflect. Will this be to my detriment or to the detriment of others?
Will this be a harmful thing that I'm about to do? So then you shouldn't do it. If not then go ahead
and do it. The same well you were doing it and the same after you have done it was that thing
that I did. Harmful or beneficial and if it is harmful then you should not do it again.
Well you're doing it if you realize it's harmful you should stop. So reflecting that way.
I mean it's a this is a talk to his son who is a young boy and it's very fatherly.
It's a unique dialogue that these two have father and son.
If you are mindful of what is, I'm sorry, if you are mindful of what it is neutral and with no
desire, if you accept things for what they are, is there any reason to help others? Is there any
reason to change things? Not really. Except that it's the right thing to do. It's the natural
thing to do. We're not stones. We don't just sit around doing nothing. So part of your equanimity
is what appears to be compassionate and I think you could even argue that it is compassionate
but it's just a word. I mean you act in the right way. So enlightened beings appear to be
very compassionate but some people think well they're not really compassionate because they don't
really care. They don't really care but the natural thing to do is to help others.
Just as a Christian prays the Lord's prayer on a daily basis. Do you have any Buddhist texts
which you read reflect on daily aside from your YouTube dialogue videos? Yeah there's
reflect on the Buddha, the Dhamma and the Sangha. That's the most common.
So we have reflections about the qualities of the Buddha, the qualities of the Dhamma, the qualities
of the Sangha. And then there are other Dhammas that are to be reflected upon daily. There's a list
of five for lay people. There's a list of ten for monks. They're called the Abinha, Pachawek, and a Dhamma.
So what are they? They are. I will get old, I will get sick, I will die, I will lose everything
I have, I can't quite remember. Oh and I'm heir to my Dhamma. Look at the chanting guy. This is
Taravada chanting guy in the house. I believe a person who wishes to ordain must have the
approval of parents. Is that necessary for a grown adult? Would if the parents don't agree?
Yes, technically that's necessary of a grown adult. If the parents don't agree, then you
starve yourself until they give in. Or threaten to throw yourself from a clip or something like that.
Maybe that's a bit of a bit far. But there's something about that.
Monks who actually did that, he threatened to kill themselves. But they're starving yourself.
They work.
I don't know. Technically you can't become a monk. Sorry.
And you can go and stand in front of their front door, sit on their porch until they give in.
Does that happen a lot? Because people ask that question a lot. The parents don't agree.
If your parents are evangelist Christians, they don't.
I mean, many, many people who are not Buddhist would forbid their children even when they get older.
My grandmother would never give consent. Although sometimes it's interesting,
they'll just say, do whatever you want kind of thing. And they're like, ooh.
Is there much difference between a monk from Thailand and the teaching they learn from
a monk from Nepal? Well, every monk teaches differently. But there's also different kinds of Buddhism.
If the parent is deceased, I assume their approval is not needed.
But what if that was just a comment? But what if you knew the parents did not approve and then they
passed away? And you knew that that was not understood? As long as they're dead.
As long as they're dead, it's okay if they never would have gone for it.
They're alive. Yeah. No, I mean, it's not about disrespecting them. It's about making them feel bad.
Okay. Dead. They're gone. In fact, it's kind of an interesting rule because the Buddha
only gave the rule out of respect for his father, who then passed away. But he instated it as a
rule. And a lot of the times the reason the Buddha instated it isn't just the only reason he
instated it, like the impetus. So it's not to say that he didn't agree with his father.
I think you could argue that they're extenuating circumstances, but sorry, put it in,
sorry, put the ordained his nephew without his nephew's parents consent. He said,
they have wronged you. I'm his father.
The suggestion, that a question, but a suggestion to find a way to remind how you do
relate to this page. I think you have like a template set up on for YouTube videos. Don't you
want that? I mean, really, I'm going to add it to every video. No, but isn't there a template
that you can use for your videos? I don't know. Maybe. I mean, I just copy paste copy paste.
I do it for all the down the part of videos, but yeah, I'm not going to do it for every day
in the down the video. No, I thought there because I thought with your older videos they
have so much information. I know why don't think that you probably were typing that in every
time. I just assumed there was some sort of a template. But I mean, you were just copying
pasting from. Well, I haven't saved in the Firefox extension, but it's not like it does it for
me itself. I have to copy it in. I see. Again, I can try to put that in as a comment so people
know because people still do ask questions on the YouTube. I need to comment. There you go.
Someone just say, Hey, if you want to ask questions, go here. Yeah, I do put that in one
of this. Okay, we don't want this place overrun. Small is okay. We'll grow up too soon as it is.
Okay. Yeah, the East Buddhism in East Asia is, it looks really good. It had just had a class today.
The professors, his specialty is Indian Buddhism, which is neat because that's my interest, of
course. But in Indian monasticism, his thesis he's doing is PhD on. So I feel better about it
than before because I wasn't really that keen on East Asian Buddhism, but he sounds like someone
who will be interesting to talk to. And he's a really gregarious sort of person. So he certainly
will keep it interesting. Some people can't teach. Nice people, but they're too timid or they're too
anxious or too anxious to please. So sometimes they, it's all, the people make these, the
professor will make these silly jokes and they just don't know how to be funny and they try to be
funny. And it's just, just teach, you know, it's cool or we don't need to be entertained. Some
people do I suppose. And it's intimidating. You get up there and all these students with glassy eyes
and slouching or right on their phones. It's really intimidating when you're in a
front of a room and everyone looks bored about what you're saying. So if you're not hard, if you're not
strong, you really just have to ignore your audience sometime. I do large classes? Yeah, pretty large.
This one is a third year class, but it's a cross-listed with arts and science, which is neat
because arts and science, which was what I was in years ago and it's this really special program
that McMaster for Keners, for people who are just looking to learn. So we got a bunch of upper-year
art side people, which is awesome. And tomorrow I think I'm switching into a third, another third
year piece studies class because today I had a class on piece and popular culture, but I'm afraid
it sounds like it's going to be a lot of music and movies and I think that's not really appropriate.
Trying to think, could I, could I justify that? Yeah, I don't think so.
It's also new. It's also a newspaper. So I was thinking, well maybe
we're looking at newspapers and newspaper articles. I don't think so. I think piece studies is
going to turn out to be not as interesting as I thought it was. It's very much about
external piece, you know? Not so much about inner piece.
Like people that go to protesting, it's really angry protesting about piece. But too,
and also like brokering piece, piece talks, there's all that kind of stuff.
Like piece corpse, whatever that is, piece corpse, piece corpse.
Piece corpse, yeah.
Like maybe we have the blue, we used to have the blueberries, the piece, piece,
piece, and they called, piecekeepers.
I'm, I'm, I'm thinking religious studies actually be more interesting for me.
I thought it wouldn't, but I'm kind of keen on, I'm religious studies now.
Is that all our questions?
Just one more. Did you hear that from Ted Explante?
No, they, let me see. They just closed applications on January 4th, so
after two days ago. So I think it's still early. I'm not holding my breath. I mean,
not that it's a big deal, but I'm not really hopeful because I don't probably fit the mold of
a student and it's supposed to be student based. So I don't know, maybe they like it, maybe they
don't. The other thing I was supposed to be about change and I didn't talk about change in my
application. I realized that afterwards. I forgot that I should try to tie it into their, the paradox
of change. It'll be neat. I've got lots of work to do as it is.
Yeah, it certainly is about change. I mean, introducing technology to ancient teachings is,
that's all changed. But yeah, that's right. I think it's close enough that they can imply it,
but I probably should at least use the word change.
Okay, that's all done. Good night. Thanks for joining us.
Thank you. Thank you, Robin. Good night.
