1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Tamil Father.

2
00:00:05,000 --> 00:00:12,000
Today we look at verse 173 which reads as follows.

3
00:00:12,000 --> 00:00:18,000
Yes, a papan, kattang, kamang, kusalena, padeet.

4
00:00:18,000 --> 00:00:19,000
Padeet.

5
00:00:19,000 --> 00:00:31,000
So, mang, lo, kamabaset, abhupa, abhapa, mouto, vatandimah, which means

6
00:00:31,000 --> 00:00:39,000
whose evil deeds, whose karma, the evil deeds

7
00:00:39,000 --> 00:00:45,000
that they have done is covered over by wholesomeness.

8
00:00:45,000 --> 00:00:52,000
Such a person lights up this world, illuminates this world,

9
00:00:52,000 --> 00:01:00,000
just like the moon coming out from behind a cloud.

10
00:01:00,000 --> 00:01:02,000
So, it should sound very familiar,

11
00:01:02,000 --> 00:01:07,000
very similar to the last verse.

12
00:01:07,000 --> 00:01:12,000
We have the opposite, two opposite lines here.

13
00:01:12,000 --> 00:01:16,000
One covers over evil with wholesomeness.

14
00:01:16,000 --> 00:01:22,000
That's like the moon when it comes out from behind a cloud,

15
00:01:22,000 --> 00:01:26,000
the uncovering of the moon.

16
00:01:26,000 --> 00:01:33,000
What it means is the change of a person.

17
00:01:33,000 --> 00:01:40,000
A person may have done very bad deeds, but when they do good,

18
00:01:40,000 --> 00:01:42,000
it's the change.

19
00:01:42,000 --> 00:01:45,000
It's going from dark to light.

20
00:01:45,000 --> 00:01:51,000
So, the story here, it's a very famous story.

21
00:01:51,000 --> 00:01:54,000
It's in reference to Angulimala.

22
00:01:54,000 --> 00:01:59,000
I'm sure many of you are familiar with the name Angulimala.

23
00:01:59,000 --> 00:02:06,000
It's a famous figure in Buddhism and Buddhist culture.

24
00:02:06,000 --> 00:02:09,000
Angulimala killed many people.

25
00:02:09,000 --> 00:02:13,000
Some sources say 999 people.

26
00:02:13,000 --> 00:02:16,000
His goal was to kill a thousand,

27
00:02:16,000 --> 00:02:24,000
and his mother came to find him and he was going to kill her.

28
00:02:24,000 --> 00:02:30,000
And then the Buddha intervened.

29
00:02:30,000 --> 00:02:34,000
The story in the midgimani kaya just says that the Buddha went to see him.

30
00:02:34,000 --> 00:02:37,000
He was going to kill all these people.

31
00:02:37,000 --> 00:02:39,000
He was killing all these people,

32
00:02:39,000 --> 00:02:42,000
and he just wouldn't stop until the Buddha walked to see him.

33
00:02:42,000 --> 00:02:45,000
He converted him, and it became a monk.

34
00:02:45,000 --> 00:02:47,000
It's the real story.

35
00:02:47,000 --> 00:02:54,000
Even evil, bloodthirsty, serial killer,

36
00:02:54,000 --> 00:03:01,000
abandoned, could change his ways.

37
00:03:01,000 --> 00:03:05,000
But this verse is actually not so much about the story.

38
00:03:05,000 --> 00:03:14,000
It's about the fact that he became enlightened.

39
00:03:14,000 --> 00:03:18,000
He spoke verse 172, which is the verse we look at last week.

40
00:03:18,000 --> 00:03:19,000
This is what it says.

41
00:03:19,000 --> 00:03:23,000
The opal bepamantitwapachas, on opamantiti.

42
00:03:23,000 --> 00:03:27,000
That's the famous, the more famous verse, I think.

43
00:03:27,000 --> 00:03:30,000
One who was negligent before,

44
00:03:30,000 --> 00:03:33,000
but it's no longer negligent.

45
00:03:33,000 --> 00:03:39,000
Such a one lights up the world.

46
00:03:39,000 --> 00:03:47,000
But 173 is more closely related to the Angulimala Sutta.

47
00:03:47,000 --> 00:03:53,000
As it refers to this shocking turn of events,

48
00:03:53,000 --> 00:03:56,000
that Angulimala, such an evil person,

49
00:03:56,000 --> 00:03:58,000
could not just become a monk,

50
00:03:58,000 --> 00:04:00,000
which is from our complete itself,

51
00:04:00,000 --> 00:04:03,000
but could actually become an arrow on.

52
00:04:03,000 --> 00:04:07,000
And so the monks didn't have any idea that this was possible.

53
00:04:07,000 --> 00:04:10,000
They saw Angulimala ordained,

54
00:04:10,000 --> 00:04:12,000
and of course that was quite a remarkable thing.

55
00:04:12,000 --> 00:04:20,000
Even the king was shocked to hear that Angulimala had become a monk.

56
00:04:20,000 --> 00:04:23,000
People wouldn't believe that anything good would come of it.

57
00:04:23,000 --> 00:04:27,000
They would attack him when he went for arms, beating him,

58
00:04:27,000 --> 00:04:31,000
throwing rocks at him and sticks at him.

59
00:04:31,000 --> 00:04:34,000
So the monks saw this, and it was remarkable,

60
00:04:34,000 --> 00:04:39,000
but no one ever thought that he'd become anywhere near enlightened,

61
00:04:39,000 --> 00:04:41,000
not after all the evil.

62
00:04:41,000 --> 00:04:43,000
So they asked when he passed away,

63
00:04:43,000 --> 00:04:45,000
maybe he was killed.

64
00:04:45,000 --> 00:04:46,000
I don't know.

65
00:04:46,000 --> 00:04:50,000
I think the story says that he was beaten bloody,

66
00:04:50,000 --> 00:04:53,000
but he became enlightened,

67
00:04:53,000 --> 00:04:57,000
and then passed away into...

68
00:04:57,000 --> 00:04:59,000
I know he became enlightened,

69
00:04:59,000 --> 00:05:04,000
and he got beaten up, and after being beaten up,

70
00:05:04,000 --> 00:05:08,000
he spoke some verses and passed away.

71
00:05:08,000 --> 00:05:13,000
So he may have been beaten so severely that he died.

72
00:05:13,000 --> 00:05:15,000
The monks asked where he was reborn.

73
00:05:15,000 --> 00:05:17,000
They went to see the Buddha,

74
00:05:17,000 --> 00:05:22,000
and as was often there, they asked the Buddha,

75
00:05:22,000 --> 00:05:33,000
where was Angulimala reborn?

76
00:05:33,000 --> 00:05:35,000
And the Buddha said,

77
00:05:35,000 --> 00:06:00,000
I know Angulimala, but I knew Buddha would be going to the Buddha.

78
00:06:00,000 --> 00:06:08,000
Having killed so many people, he could possibly become a unbound.

79
00:06:08,000 --> 00:06:10,000
And they said, yes.

80
00:06:10,000 --> 00:06:12,000
And the Buddha said, yes.

81
00:06:12,000 --> 00:06:15,000
In the past, not even one...

82
00:06:15,000 --> 00:06:17,000
And this is an important part of the story,

83
00:06:17,000 --> 00:06:20,000
not even one good friend.

84
00:06:20,000 --> 00:06:26,000
He found not even one good friend.

85
00:06:26,000 --> 00:06:28,000
That's why he did so much evil.

86
00:06:28,000 --> 00:06:33,000
But after having gained a good friend,

87
00:06:33,000 --> 00:06:36,000
he became diligent.

88
00:06:36,000 --> 00:06:40,000
He lost his negligence,

89
00:06:40,000 --> 00:06:45,000
and completely obliterated the evil with the good.

90
00:06:45,000 --> 00:06:50,000
And then he taught the verse.

91
00:06:50,000 --> 00:06:54,000
So I think the story of Angulimala teaches us many things.

92
00:06:54,000 --> 00:06:56,000
It's a long story, but we're not going to go into that,

93
00:06:56,000 --> 00:07:00,000
because this verse isn't so much about the story,

94
00:07:00,000 --> 00:07:02,000
but it teaches us, I think,

95
00:07:02,000 --> 00:07:05,000
mainly two things.

96
00:07:05,000 --> 00:07:12,000
First, that we shouldn't be discouraged about our own state,

97
00:07:12,000 --> 00:07:14,000
coming into the practice.

98
00:07:14,000 --> 00:07:18,000
Some people would think, oh, I'm too far gone.

99
00:07:18,000 --> 00:07:20,000
I've done bad things.

100
00:07:20,000 --> 00:07:24,000
How could I possibly practice to become a knight?

101
00:07:24,000 --> 00:07:28,000
So I always think, well, look at Angulimala.

102
00:07:28,000 --> 00:07:32,000
Often it's true that in many cases a very bad person

103
00:07:32,000 --> 00:07:34,000
is not going to get far in the practice.

104
00:07:34,000 --> 00:07:38,000
It's very difficult.

105
00:07:38,000 --> 00:07:41,000
But on the other hand, it's not so much about your past.

106
00:07:41,000 --> 00:07:43,000
It's about your present.

107
00:07:43,000 --> 00:07:46,000
And doing lots of evil deeds is going to have consequences,

108
00:07:46,000 --> 00:08:00,000
but much of the evil is in external consequences,

109
00:08:00,000 --> 00:08:05,000
not in the state of the mind.

110
00:08:05,000 --> 00:08:10,000
All you have to deal with, and it's really not that complicated

111
00:08:10,000 --> 00:08:14,000
to deal with the emotions in your own mind,

112
00:08:14,000 --> 00:08:18,000
the kind of your own mind, the chaos in your own mind.

113
00:08:18,000 --> 00:08:22,000
And when you deal with that, well, the consequences don't end.

114
00:08:22,000 --> 00:08:25,000
It's possible you might still be arrested for your evil deeds.

115
00:08:25,000 --> 00:08:35,000
It's possible you might be subject to revenge and so on.

116
00:08:35,000 --> 00:08:39,000
But maybe that's another thing that it teaches us.

117
00:08:39,000 --> 00:08:43,000
It's not to be so concerned about external consequences.

118
00:08:43,000 --> 00:08:48,000
We often mix the two external and the internal.

119
00:08:48,000 --> 00:08:51,000
They become discouraged by external consequences.

120
00:08:51,000 --> 00:08:54,000
I do all these good things and bad things still happen to me.

121
00:08:54,000 --> 00:09:00,000
And we're not so concerned about the external bad, concerned about

122
00:09:00,000 --> 00:09:03,000
we're not really concerned about happiness.

123
00:09:03,000 --> 00:09:06,000
Because happiness doesn't need to happiness.

124
00:09:06,000 --> 00:09:10,000
If your concern is always with happiness and freedom from suffering,

125
00:09:10,000 --> 00:09:14,000
you're never really going to be free from suffering because

126
00:09:14,000 --> 00:09:18,000
such a state, such concerns, don't actually make you happy

127
00:09:18,000 --> 00:09:21,000
or free of from suffering.

128
00:09:21,000 --> 00:09:22,000
It's goodness.

129
00:09:22,000 --> 00:09:23,000
It's gusala.

130
00:09:23,000 --> 00:09:26,000
The body uses the word here, gusala in the gusala.

131
00:09:26,000 --> 00:09:28,000
Awesomeness, goodness.

132
00:09:28,000 --> 00:09:32,000
We should always be concerned with goodness.

133
00:09:32,000 --> 00:09:42,000
Best beful of goodness, best beful of goodness.

134
00:09:42,000 --> 00:09:51,000
Best beful of goodness.

135
00:09:51,000 --> 00:09:55,000
But the other lesson, the other lesson that the Buddha mentioned,

136
00:09:55,000 --> 00:09:59,000
the Buddha brings up here that I think is really the crux.

137
00:09:59,000 --> 00:10:03,000
And the Buddha really hits the heart of the matter.

138
00:10:03,000 --> 00:10:09,000
It's how easy it is to fall on the wrong path without a good friend.

139
00:10:09,000 --> 00:10:13,000
Of course, the word good friend, Kali and the mitta.

140
00:10:13,000 --> 00:10:15,000
In Buddhism, it refers to the teacher.

141
00:10:15,000 --> 00:10:18,000
It refers generally to the Buddha.

142
00:10:18,000 --> 00:10:21,000
Because people did not meet the Buddha,

143
00:10:21,000 --> 00:10:25,000
or we could say more generally with the Buddha's teaching,

144
00:10:25,000 --> 00:10:28,000
because of that they fall into.

145
00:10:28,000 --> 00:10:32,000
All sorts of evil.

146
00:10:32,000 --> 00:10:37,000
When you don't have someone who can set you down the right path,

147
00:10:37,000 --> 00:10:39,000
like now we have the Buddha,

148
00:10:39,000 --> 00:10:43,000
and we still have the Buddha through his teachings.

149
00:10:43,000 --> 00:10:46,000
When you don't have that,

150
00:10:46,000 --> 00:10:49,000
sometimes we blame, we blame evil people.

151
00:10:49,000 --> 00:10:50,000
We look at them and they say,

152
00:10:50,000 --> 00:10:52,000
what a terrible evil person.

153
00:10:52,000 --> 00:10:54,000
And sometimes often times,

154
00:10:54,000 --> 00:10:56,000
in fact, you could say all the time,

155
00:10:56,000 --> 00:11:02,000
the only reason people do evil is through delusion.

156
00:11:02,000 --> 00:11:07,000
It's not that anyone wants to be an evil person,

157
00:11:07,000 --> 00:11:09,000
or not exactly.

158
00:11:09,000 --> 00:11:18,000
It's not that anyone purposefully and knowingly wants evil.

159
00:11:18,000 --> 00:11:21,000
It's that they think somehow that some good will come out of being evil,

160
00:11:21,000 --> 00:11:23,000
doing evil things.

161
00:11:23,000 --> 00:11:27,000
And oftentimes there's not even a sense that it's evil,

162
00:11:27,000 --> 00:11:29,000
killing.

163
00:11:29,000 --> 00:11:32,000
For many people, killing is a good thing.

164
00:11:32,000 --> 00:11:37,000
Recently, what there was a politician in America

165
00:11:37,000 --> 00:11:42,000
who said, just today, what did he say?

166
00:11:42,000 --> 00:11:45,000
He said, we should kill all the drug dealers.

167
00:11:45,000 --> 00:11:53,000
And first of all, for many people,

168
00:11:53,000 --> 00:11:55,000
I think that they resonate,

169
00:11:55,000 --> 00:11:56,000
that resonates with it.

170
00:11:56,000 --> 00:11:59,000
They say, yeah, yeah, kill bad people.

171
00:11:59,000 --> 00:12:02,000
Wanting to shoot others,

172
00:12:02,000 --> 00:12:04,000
as in America, a lot of people have guns,

173
00:12:04,000 --> 00:12:06,000
and don't really,

174
00:12:06,000 --> 00:12:08,000
there are many people who don't really have a problem with.

175
00:12:08,000 --> 00:12:11,000
There was one, a talk to one vet,

176
00:12:11,000 --> 00:12:13,000
a veteran of the war,

177
00:12:13,000 --> 00:12:15,000
and he had a lot of mental issues,

178
00:12:15,000 --> 00:12:17,000
but he said, he told me,

179
00:12:17,000 --> 00:12:21,000
he said, someone asked him what he missed most about being a sergeant.

180
00:12:21,000 --> 00:12:23,000
He was a sergeant,

181
00:12:23,000 --> 00:12:27,000
and some part of the armed forces,

182
00:12:27,000 --> 00:12:30,000
he said, what he missed most about it having retired,

183
00:12:30,000 --> 00:12:33,000
and he said, being able to kill people.

184
00:12:35,000 --> 00:12:38,000
There's people that exist in the world.

185
00:12:38,000 --> 00:12:44,000
Now, there are various varying degrees of delusion,

186
00:12:44,000 --> 00:12:47,000
but it's all delusion.

187
00:12:47,000 --> 00:12:51,000
When people think that stealing is not wrong,

188
00:12:51,000 --> 00:12:54,000
or, you know, this story about Mark Zuckerberg,

189
00:12:54,000 --> 00:12:56,000
this had a Facebook,

190
00:12:56,000 --> 00:12:58,000
I don't know if I should mention names,

191
00:12:58,000 --> 00:13:00,000
but this guy,

192
00:13:00,000 --> 00:13:02,000
very famous guy who said,

193
00:13:02,000 --> 00:13:05,000
he decided to start killing very influential

194
00:13:05,000 --> 00:13:07,000
because of how famous he is.

195
00:13:07,000 --> 00:13:11,000
He decided to start killing his own animals,

196
00:13:11,000 --> 00:13:14,000
so he went and he killed a goat on his own,

197
00:13:14,000 --> 00:13:16,000
because he thought that was better,

198
00:13:16,000 --> 00:13:19,000
that was the best thing to do if you're going to eat the meat.

199
00:13:19,000 --> 00:13:22,000
Like he couldn't have just become a vegetarian, right?

200
00:13:22,000 --> 00:13:25,000
It's like, if you feel that strongly about it,

201
00:13:25,000 --> 00:13:28,000
why is it better somehow to kill yourself?

202
00:13:32,000 --> 00:13:34,000
A lot of delusion,

203
00:13:34,000 --> 00:13:37,000
I should go on and on about the types of delusion.

204
00:13:37,000 --> 00:13:40,000
Just the five precepts, you know?

205
00:13:40,000 --> 00:13:48,000
I always remark, if I had some inkling of killing being wrong,

206
00:13:48,000 --> 00:13:52,000
or that drugs and alcohol didn't open your mind

207
00:13:52,000 --> 00:13:58,000
or make you a happier, more fun-loving person,

208
00:13:58,000 --> 00:14:01,000
but in fact, dulled your mind

209
00:14:01,000 --> 00:14:09,000
and led to a greater emotional turmoil.

210
00:14:09,000 --> 00:14:12,000
That's what I had said.

211
00:14:12,000 --> 00:14:19,000
Drugs and alcohol, that's fundamentally wrong for humans to engage in.

212
00:14:19,000 --> 00:14:23,000
Maybe not, but it seems to me that if there had been more instruction,

213
00:14:23,000 --> 00:14:28,000
I might have avoided much of the suffering in my life,

214
00:14:28,000 --> 00:14:37,000
much of the evil.

215
00:14:37,000 --> 00:14:39,000
As for what the verse teaches us,

216
00:14:39,000 --> 00:14:42,000
it has a bit of a different lesson.

217
00:14:42,000 --> 00:14:46,000
The verse itself is talking about the possibility

218
00:14:46,000 --> 00:14:49,000
of mitigating evil through good.

219
00:14:49,000 --> 00:14:51,000
This is a good question.

220
00:14:51,000 --> 00:14:56,000
How does someone become pure

221
00:14:56,000 --> 00:14:59,000
when their mind is so full of evil?

222
00:14:59,000 --> 00:15:02,000
And it doesn't so much evil.

223
00:15:02,000 --> 00:15:06,000
And so we talk about different kinds of karma,

224
00:15:06,000 --> 00:15:09,000
and the Buddha reaffirms that this is the case,

225
00:15:09,000 --> 00:15:16,000
and I think if you look both at external circumstances,

226
00:15:16,000 --> 00:15:21,000
how our deeds and our acts and our minds

227
00:15:21,000 --> 00:15:26,000
that affects the world around us,

228
00:15:26,000 --> 00:15:29,000
and how our actions affect our own minds.

229
00:15:29,000 --> 00:15:32,000
You can see that it's more complicated

230
00:15:32,000 --> 00:15:39,000
than just do good, get good, do evil, get evil.

231
00:15:39,000 --> 00:15:42,000
I mean, that simply put is how karma works,

232
00:15:42,000 --> 00:15:46,000
but the more complex formulas

233
00:15:46,000 --> 00:15:49,000
that there are four types of karma.

234
00:15:49,000 --> 00:15:53,000
There's karma that creates results,

235
00:15:53,000 --> 00:15:55,000
good or bad.

236
00:15:55,000 --> 00:15:57,000
If you do a good deed,

237
00:15:57,000 --> 00:15:59,000
something good comes from it internally

238
00:15:59,000 --> 00:16:01,000
and become a better person with you.

239
00:16:01,000 --> 00:16:05,000
And externally, people appreciate you.

240
00:16:05,000 --> 00:16:08,000
You do a bad deed likewise.

241
00:16:08,000 --> 00:16:10,000
But there are other types of karma.

242
00:16:10,000 --> 00:16:13,000
Some karma is just supportive.

243
00:16:13,000 --> 00:16:16,000
Some aspects of some deeds that you do.

244
00:16:16,000 --> 00:16:19,000
Sometimes you do a good deed,

245
00:16:19,000 --> 00:16:22,000
and it supports some...

246
00:16:22,000 --> 00:16:24,000
So you do one good deed,

247
00:16:24,000 --> 00:16:26,000
and then you do another good deed,

248
00:16:26,000 --> 00:16:28,000
and people recognize the first one

249
00:16:28,000 --> 00:16:30,000
because of the second one,

250
00:16:30,000 --> 00:16:33,000
or the first one bears fruit,

251
00:16:33,000 --> 00:16:37,000
even mentally, because of the power of the second one.

252
00:16:37,000 --> 00:16:41,000
It's repeated actions affect each other.

253
00:16:41,000 --> 00:16:44,000
So supportive karma is a thing.

254
00:16:44,000 --> 00:16:49,000
And then there's a reductive karma

255
00:16:49,000 --> 00:16:52,000
that reduces the effect of other karma.

256
00:16:52,000 --> 00:16:55,000
So you do something good and something good.

257
00:16:55,000 --> 00:16:58,000
Something good comes.

258
00:16:58,000 --> 00:17:02,000
Maybe you're very nice to people and they appreciate you.

259
00:17:02,000 --> 00:17:05,000
But then you do something bad.

260
00:17:05,000 --> 00:17:09,000
And your reputation is reduced,

261
00:17:09,000 --> 00:17:11,000
even though you maybe you could be.

262
00:17:11,000 --> 00:17:15,000
Sometimes someone does very good deeds all their life,

263
00:17:15,000 --> 00:17:17,000
and then one bad mistake,

264
00:17:17,000 --> 00:17:23,000
and their reputation is ruined.

265
00:17:23,000 --> 00:17:27,000
And the fourth is nullifying karma,

266
00:17:27,000 --> 00:17:30,000
karma that destroys other karma.

267
00:17:30,000 --> 00:17:33,000
And that's what he's talking about here.

268
00:17:33,000 --> 00:17:37,000
That, especially internally,

269
00:17:37,000 --> 00:17:40,000
there's so much you can do to change

270
00:17:40,000 --> 00:17:46,000
your mind,

271
00:17:46,000 --> 00:17:49,000
change the landscape of your mind.

272
00:17:49,000 --> 00:17:51,000
All these habits that we have,

273
00:17:51,000 --> 00:17:59,000
all these qualities that make up who we are.

274
00:17:59,000 --> 00:18:02,000
As much we can do to change that.

275
00:18:02,000 --> 00:18:07,000
Meditation is the prime example.

276
00:18:07,000 --> 00:18:14,000
It's quite shocking for people who are familiar with,

277
00:18:14,000 --> 00:18:16,000
for friends, for relatives,

278
00:18:16,000 --> 00:18:20,000
of someone who goes to do a meditation course.

279
00:18:20,000 --> 00:18:21,000
Because when they come back,

280
00:18:21,000 --> 00:18:23,000
they can be quite a changed individual.

281
00:18:23,000 --> 00:18:25,000
And many things about them are different.

282
00:18:25,000 --> 00:18:31,000
And they're quite shocking.

283
00:18:31,000 --> 00:18:36,000
And all those years of building up habits have suddenly been erased

284
00:18:36,000 --> 00:18:38,000
or reduced, usually not erased,

285
00:18:38,000 --> 00:18:45,000
but some bad habits are erased.

286
00:18:45,000 --> 00:18:47,000
But throughout the meditation course,

287
00:18:47,000 --> 00:18:49,000
this is a big part of what we're doing.

288
00:18:49,000 --> 00:18:58,000
It's a good explanation of the work that we do in meditation.

289
00:18:58,000 --> 00:19:02,000
They work on nullifying our bad habits.

290
00:19:02,000 --> 00:19:07,000
I mean, nullifying really is not about covering up,

291
00:19:07,000 --> 00:19:11,000
which is the word that they use in the Buddha used in the verse.

292
00:19:11,000 --> 00:19:17,000
But it's about smothering them, really.

293
00:19:17,000 --> 00:19:25,000
Extinguishing them, I guess, is the best way to explain it.

294
00:19:25,000 --> 00:19:31,000
Simply by gaining the clarity of mind that was missing,

295
00:19:31,000 --> 00:19:34,000
you know, you light up the world.

296
00:19:34,000 --> 00:19:37,000
This illuminating imagery of lighting up the world,

297
00:19:37,000 --> 00:19:40,000
like the moon coming up from behind the clouds,

298
00:19:40,000 --> 00:19:45,000
is apt because you realize that all the people that you did

299
00:19:45,000 --> 00:19:48,000
was just because of ignorance.

300
00:19:48,000 --> 00:19:51,000
That you were totally blind.

301
00:19:51,000 --> 00:19:54,000
That you didn't have a good reason for doing any of the bad things

302
00:19:54,000 --> 00:19:56,000
that you did.

303
00:19:56,000 --> 00:20:00,000
And simply didn't know any better.

304
00:20:00,000 --> 00:20:03,000
But you knew wrong.

305
00:20:03,000 --> 00:20:07,000
We're given all these wrong views by all sorts of sources.

306
00:20:07,000 --> 00:20:10,000
What our parents do, what our friends do,

307
00:20:10,000 --> 00:20:14,000
what our society tells us, what our religions tell us

308
00:20:14,000 --> 00:20:19,000
often religions have bad ideas about killing and stealing and so on.

309
00:20:19,000 --> 00:20:32,000
Not stealing, but many religions condone various forms of violence.

310
00:20:32,000 --> 00:20:36,000
I mean, I think violence towards animals is an obvious one.

311
00:20:36,000 --> 00:20:39,000
But many religions will be against killing humans,

312
00:20:39,000 --> 00:20:45,000
but then have no problems or even in favor of killing animals.

313
00:20:45,000 --> 00:20:52,000
And often in fairly inhumane ways,

314
00:20:52,000 --> 00:20:57,000
so many sources of wrong view.

315
00:20:57,000 --> 00:20:59,000
And many of us, I think,

316
00:20:59,000 --> 00:21:04,000
this resonates, this idea that if only I'd had a good friend,

317
00:21:04,000 --> 00:21:07,000
we feel good now that we feel blessed now.

318
00:21:07,000 --> 00:21:10,000
We feel we found a refuge.

319
00:21:10,000 --> 00:21:17,000
Now that we've found a good friend in the Buddha,

320
00:21:17,000 --> 00:21:24,000
so we can light up the world.

321
00:21:24,000 --> 00:21:26,000
So that's the number part of her tonight.

322
00:21:26,000 --> 00:21:42,000
Thank you all for tuning in. Have a good night.

