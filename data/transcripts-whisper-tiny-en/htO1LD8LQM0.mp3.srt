1
00:00:00,000 --> 00:00:04,000
What do you guide students in a self-retreat over internet?

2
00:00:04,000 --> 00:00:12,000
I moved to Suriname since 10 months and there are no retreats here.

3
00:00:12,000 --> 00:00:17,000
Yes, if the person was dedicated, I think I'd like to say

4
00:00:17,000 --> 00:00:23,000
the person would have to kind of make some kind of commitment.

5
00:00:23,000 --> 00:00:35,000
We don't have to promise anything, but they should understand the commitment and make their best effort to stick with it.

6
00:00:35,000 --> 00:00:41,000
Even if it gets difficult or even if they feel like some kind of doubt arising or so on.

7
00:00:41,000 --> 00:00:50,000
Because it's quite a bit of effort on our part to set it up.

8
00:00:50,000 --> 00:00:54,000
I've done that before and then the meditators just disappears on.

9
00:00:54,000 --> 00:00:56,000
It's not the same as having them here with you.

10
00:00:56,000 --> 00:01:00,000
But I have also done it and people have stuck with it and gotten really good results out of it.

11
00:01:00,000 --> 00:01:06,000
So it's sure we can do it. I've done it over Skype before.

12
00:01:06,000 --> 00:01:14,000
But it has to be someone who's dedicated and says, yes, I'm going to take the time, put aside some time.

13
00:01:14,000 --> 00:01:24,000
Especially if they want to do it somewhat intensively, like if they have the time to devote strictly to meditation practice.

14
00:01:24,000 --> 00:01:48,000
Then we can meet every day and talk about meditation. That's possible.

