1
00:00:00,000 --> 00:00:03,640
Go back to the structure of the first chapter.

2
00:00:03,640 --> 00:00:14,160
I mean, the detail contents, table of contents of the first chapter and the handout.

3
00:00:14,160 --> 00:00:25,160
So this chapter deals with what is translated as virtue or in Pali, it is Sila.

4
00:00:25,160 --> 00:00:36,160
So the first thing mentioned there is a stanza from the Sainu Danika, as an introductory talk.

5
00:00:36,160 --> 00:00:43,040
And then the other explained what the part of purification is.

6
00:00:43,040 --> 00:00:49,160
And along with the commentary on the stanza quoted at the beginning of the book.

7
00:00:49,160 --> 00:00:59,840
And then the other goes on to explain Sila or virtue on what is Sila, what is virtue.

8
00:00:59,840 --> 00:01:08,640
And then he explained that pollution is virtue, mental factors are virtue, restraint is virtue,

9
00:01:08,640 --> 00:01:11,560
and non-transcrustion is virtue.

10
00:01:11,560 --> 00:01:20,880
And this he explained according to a text called Batisambida Maga.

11
00:01:20,880 --> 00:01:28,000
And in connection with restraint, the other explained five kinds of restraint.

12
00:01:28,000 --> 00:01:36,800
And they are given in paragraph 18.

13
00:01:36,800 --> 00:01:49,400
And then after explaining what virtue is, what is taken to be virtue according to the teachings of Tira Vada,

14
00:01:49,400 --> 00:01:58,720
the other goes on to explain the meaning of the what Sila in Pali.

15
00:01:58,720 --> 00:02:08,240
Why it is called Sila, why virtue is called Sila in Pali, and the explanation given is,

16
00:02:08,240 --> 00:02:15,200
it is called Sila because it could ordinate the bodily and verbal actions.

17
00:02:15,200 --> 00:02:24,800
And also itself as a foundation for the acquisition of wholesome states.

18
00:02:24,800 --> 00:02:30,120
So the two basic meanings of the word are given there.

19
00:02:30,120 --> 00:02:38,320
The one is coordinating and the other is the basis of holding up.

20
00:02:38,320 --> 00:02:54,760
And then the other gives the characteristic function manifestation and the approximate cause of Sila or virtue.

21
00:02:54,760 --> 00:03:09,560
It is customary with Tira Vada, orders that whenever there is something to be studied or to be understood,

22
00:03:09,560 --> 00:03:20,480
they explain that thing to be understood with reference to its characteristic, its function,

23
00:03:20,480 --> 00:03:24,680
its manifestation, and its approximate cause.

24
00:03:24,680 --> 00:03:33,240
So in Abidama also, every theme, I mean the Chaita and 52 Chaitasikas, each of them,

25
00:03:33,240 --> 00:03:40,040
are explained with reference to characteristic and so on.

26
00:03:40,040 --> 00:03:53,360
So the characteristics and others of Sila or virtue are given in paragraphs 20 to 22.

27
00:03:53,360 --> 00:04:05,760
And next, the other gives the benefits of virtue, benefits of having pure virtue or pure Sila.

28
00:04:05,760 --> 00:04:15,800
And then he describes the different kinds of virtue which takes up the rest of the first chapter.

29
00:04:15,800 --> 00:04:22,360
And we reached to the end of characteristic and so on last week.

30
00:04:22,360 --> 00:04:42,480
So this week we will begin with the benefits of virtue, paragraphs 23, 24 and so on.

31
00:04:42,480 --> 00:04:55,120
And this paragraph gives us quotations from the Soodas, first from Angutra Nigaya.

32
00:04:55,120 --> 00:04:59,560
Do you see the words A, 5 and 1?

33
00:04:59,560 --> 00:05:06,200
So A means Angutra Nigaya, is the name of one of the five collections.

34
00:05:06,200 --> 00:05:17,720
So the translation of it is called gradual sayings and there are five volumes of them and the copies are in the library here.

35
00:05:17,720 --> 00:05:29,080
And then further down towards the bottom you see D 286 and D means D G and Nigaya.

36
00:05:29,080 --> 00:05:37,280
This is another collection and the translation of this Nigaya is called dialogues of the Buddha.

37
00:05:37,280 --> 00:05:40,720
There are three volumes.

38
00:05:40,720 --> 00:05:52,720
And then a little down you see M1 33, M means Majima Nigaya, another division of Soodas.

39
00:05:52,720 --> 00:06:01,440
And the translation of this Nigaya is called middle length sayings.

40
00:06:01,440 --> 00:06:05,040
I hope you have read all these.

41
00:06:05,040 --> 00:06:07,560
These are the benefits of virtue.

42
00:06:07,560 --> 00:06:18,200
And then on the next page, beginning with further more, the other gave the same benefits of virtue in verse 4.

43
00:06:18,200 --> 00:06:32,920
And towards the bottom, about six lengths from the bottom of the verses, there is a saying with dread of self-blame and the like.

44
00:06:32,920 --> 00:06:38,800
So virtue entirely does a way with dread of self-blame and the like.

45
00:06:38,800 --> 00:06:44,120
Now self-blame and the like, what are the others?

46
00:06:44,120 --> 00:07:09,160
The benefits of being virtuous or benefits of having cure or good stiller, as these are 4 and the dangers of being unvirtuous or dangers of having bed and sealer are also 4.

47
00:07:09,160 --> 00:07:15,120
So these four are first self-blame or self-reproach, that is the first.

48
00:07:15,120 --> 00:07:27,720
So when our moral contact is in pure, the first blame comes from ourselves because we only know what we did.

49
00:07:27,720 --> 00:07:36,040
So the first they are self-reproach or self-blame and then the blame of others or the approach of others.

50
00:07:36,040 --> 00:07:46,040
So if we do not keep our rules probably, if we break our precepts, then other people will also blame us.

51
00:07:46,040 --> 00:07:51,840
So there is the danger of blame of others or others reproach.

52
00:07:51,840 --> 00:07:57,120
And the third disadvantage is punishment.

53
00:07:57,120 --> 00:08:12,200
If we do not keep precepts and we may commit crimes, we may kill beings or we may kill human beings, we may steal and so on.

54
00:08:12,200 --> 00:08:16,520
And as a result of this, we will get punishment in this life.

55
00:08:16,520 --> 00:08:24,440
So punishment is another danger of being in pure moral conduct.

56
00:08:24,440 --> 00:08:37,600
And the last one is the unhappy destiny, that means rebirth in the four woeful states of full states of loss.

57
00:08:37,600 --> 00:08:47,480
Rebirth in hell, rebut in animal kingdom, rebut as hungry ghosts, and rebut as another kind of ghosts.

58
00:08:47,480 --> 00:08:58,680
So these are the four disadvantages of being in pure in moral conduct and the opposite of them are the four benefits.

59
00:08:58,680 --> 00:09:17,680
So what you entirely does away with dread of self-blame means, what you entirely does away with the danger of these four namely self-reproach, others approach, punishment, and unhappy destiny.

60
00:09:17,680 --> 00:09:24,680
And then the other gives kinds of virtue.

61
00:09:24,680 --> 00:09:35,880
Western people often say that peace and people are front of numbers, they want to give one, two, three, four, and so on.

62
00:09:35,880 --> 00:09:44,560
So here what you explain as of one kind of two kinds, three kinds, four kinds, five kinds.

63
00:09:44,560 --> 00:09:52,880
And this first the other gives just the list of them and then they are explained in detail.

64
00:09:52,880 --> 00:10:00,280
So one kind is just, there is only one one stealer or one virtue according to the characteristic.

65
00:10:00,280 --> 00:10:17,680
So when we take into account the characteristic, there is only one virtue which is the characteristic of coordinating bodily and mental, I mean bodily and verbal actions and holding up people.

66
00:10:17,680 --> 00:10:23,280
Then there are two kinds, three kinds and so on, you can read the list.

67
00:10:23,280 --> 00:10:32,880
And on page 11, the paragraph 16, I want to make some changes there.

68
00:10:32,880 --> 00:10:45,080
As now the others explaining four kinds of virtue, that's natural virtue, customary virtue, and necessary virtue.

69
00:10:45,080 --> 00:10:50,080
I think we should say ingrained virtue instead of necessary virtue.

70
00:10:50,080 --> 00:10:57,880
The probably what here is dumbata, that means the way it is, that means just ingrained virtue.

71
00:10:57,880 --> 00:11:03,480
And the last one is virtue due to previous causes.

72
00:11:03,480 --> 00:11:08,880
Here previous causes means causes in previous lives.

73
00:11:08,880 --> 00:11:14,080
Some people are virtuous because they were virtuous in their past lives too.

74
00:11:14,080 --> 00:11:26,680
So this being virtuous is carried over from past lives to their full kinds of them.

75
00:11:26,680 --> 00:11:40,080
And in the detailed explanation, what's the bottom of paragraph 26?

76
00:11:40,080 --> 00:11:48,080
Here in keeping is accomplished by faith and energy, avoiding by faith and mindfulness.

77
00:11:48,080 --> 00:11:55,280
Now, this paragraph explains two kinds of virtue. One is keeping and one is avoiding.

78
00:11:55,280 --> 00:12:03,280
Keeping means following the advice that this should be done.

79
00:12:03,280 --> 00:12:08,080
And avoiding means following the advice that this should not be done.

80
00:12:08,080 --> 00:12:12,080
So most precepts are, this should not be done.

81
00:12:12,080 --> 00:12:16,080
So you must not kill, you must not steal and so on.

82
00:12:16,080 --> 00:12:21,080
Sometimes you should do this or you should do this, that.

83
00:12:21,080 --> 00:12:28,080
And if we do not do that, we come to some kind of unvirtuousness.

84
00:12:28,080 --> 00:12:30,080
So there are two kinds.

85
00:12:30,080 --> 00:12:34,080
And the keeping is accomplished by faith and energy.

86
00:12:34,080 --> 00:12:46,080
Because there are some, we call them rules, saying that monks must do this or they are the monastery or something like that.

87
00:12:46,080 --> 00:12:53,080
And this kind of sealer or virtue is accomplished by faith.

88
00:12:53,080 --> 00:13:01,080
If you have faith or confidence in the Buddha, if you have energy or effort,

89
00:13:01,080 --> 00:13:04,080
then you can keep that sealer.

90
00:13:04,080 --> 00:13:07,080
But avoiding is by faith.

91
00:13:07,080 --> 00:13:15,080
Now here, in this translation, it is said by faith and mindfulness.

92
00:13:15,080 --> 00:13:21,080
But in the Burmese edition, Burmese Pali edition,

93
00:13:21,080 --> 00:13:29,080
and also in the Pali text society's edition, there is no word for mindfulness.

94
00:13:29,080 --> 00:13:36,080
And so we should strike out two words here, and mindfulness.

95
00:13:36,080 --> 00:13:40,080
We should just say avoiding is accomplished by faith.

96
00:13:40,080 --> 00:13:44,080
Only when you have faith, you have confidence in the rules and in the Buddha.

97
00:13:44,080 --> 00:13:48,080
Can you keep yourself from breaking rules?

98
00:13:48,080 --> 00:13:55,080
So it is through faith and not by mindfulness.

99
00:13:55,080 --> 00:14:03,080
And the text, mindfulness is not supported by the commentary on this book.

100
00:14:03,080 --> 00:14:05,080
We call it sub-commentary.

101
00:14:05,080 --> 00:14:15,080
So in the sub-commentary, also only the word faith or the Pali word sada is explained and not mindfulness or safety.

102
00:14:15,080 --> 00:14:20,080
So we should say here, avoiding is accomplished by faith.

103
00:14:20,080 --> 00:14:25,080
If you have faith, if you have confidence in the Buddha and in his teaching,

104
00:14:25,080 --> 00:14:35,080
and you can follow the rules that prohibit you to do something.

105
00:14:35,080 --> 00:14:49,080
Because you have to do something when you follow the rules, this should be done.

106
00:14:49,080 --> 00:14:55,080
Suppose there are duties to be done to our teachers.

107
00:14:55,080 --> 00:15:04,080
Say we must get up early in the morning before the teacher gets up and we must offer him water for washing his faith.

108
00:15:04,080 --> 00:15:10,080
And if there is something to eat, we must offer him and we must do some other tools for him and so on.

109
00:15:10,080 --> 00:15:14,080
So these are called the first kind of keeping.

110
00:15:14,080 --> 00:15:24,080
So if we do not have faith and if we do not have make effort, then we will not accomplish that kind of sealer.

111
00:15:24,080 --> 00:15:36,080
So their faith and energy, both are required, but refining from something needs only faith.

112
00:15:36,080 --> 00:15:43,080
Not necessarily mindfulness.

113
00:15:43,080 --> 00:15:56,080
And in the next paragraph, that of good behavior and that of the beginning of the life of purity.

114
00:15:56,080 --> 00:16:05,080
They are the terms in Pali, directly translated right here.

115
00:16:05,080 --> 00:16:14,080
And about four lines down, this is a term for the virtue that is livelihood as eight.

116
00:16:14,080 --> 00:16:24,080
And on the second line of this page, there is a term for virtue other than that which has livelihood as eight.

117
00:16:24,080 --> 00:16:32,080
And the eight are given in the footnote.

118
00:16:32,080 --> 00:16:46,080
Refraining from three wrong bodily actions, refraining from four rubber wrong actions and refraining from wrong livelihood.

119
00:16:46,080 --> 00:16:50,080
So they constitute eight precepts.

120
00:16:50,080 --> 00:16:58,080
These eight precepts are different from the eight precepts to be mentioned a little later.

121
00:16:58,080 --> 00:17:07,080
And these eight precepts are called those that have livelihood as eight.

122
00:17:07,080 --> 00:17:15,080
The others are different.

123
00:17:15,080 --> 00:17:18,080
So this is a term for virtue that has livelihood as eight.

124
00:17:18,080 --> 00:17:26,080
It is the initial stage of the path because it has arguably to be purified in the prior stage two.

125
00:17:26,080 --> 00:17:29,080
I think there is no word for two in the original Pali.

126
00:17:29,080 --> 00:17:32,080
So we should leave out two.

127
00:17:32,080 --> 00:17:42,080
This sealer has to be accomplished before we practice, before we take up the beginning of the life of purity.

128
00:17:42,080 --> 00:17:49,080
That means before we practice meditation, we have to purify our moral conduct.

129
00:17:49,080 --> 00:17:57,080
So it has to be accomplished or purified in the prior stage of the path.

130
00:17:57,080 --> 00:18:08,080
The path here means the practice of meditation leading to the enlightenment.

131
00:18:08,080 --> 00:18:20,080
And in paragraph 29, four kinds of sealers are mentioned and they are two kinds of dependence.

132
00:18:20,080 --> 00:18:24,080
Dependence through craving and dependence through false views.

133
00:18:24,080 --> 00:18:34,080
Actually craving itself is dependence here and false views themselves are dependence here.

134
00:18:34,080 --> 00:18:38,080
Because sometimes we have attachment, we have loba.

135
00:18:38,080 --> 00:18:41,080
So we want to be reborn in a better world.

136
00:18:41,080 --> 00:18:43,080
We want to be reborn as a deva.

137
00:18:43,080 --> 00:18:45,080
I mean, celestial being.

138
00:18:45,080 --> 00:18:50,080
We want to be reborn as human beings and a good family, something like that.

139
00:18:50,080 --> 00:18:57,080
So we practice sealer so that we may be reborn in a better existence.

140
00:18:57,080 --> 00:19:04,080
And that sealer is dependent on craving or attachment.

141
00:19:04,080 --> 00:19:08,080
Because we have attachment to this life, we do something so that we may be reborn there.

142
00:19:08,080 --> 00:19:17,080
So craving itself is dependence, not dependence through craving, but dependence as craving we may say.

143
00:19:17,080 --> 00:19:25,080
And sometimes we have false view that beautification is through virtuous conduct.

144
00:19:25,080 --> 00:19:36,080
That means we can become purified through virtue only and that is a false view.

145
00:19:36,080 --> 00:19:41,080
Because we have to practice virtue first and then concentration and wisdom.

146
00:19:41,080 --> 00:19:51,080
So virtue alone by itself will not help us to become enlightened, to become purified.

147
00:19:51,080 --> 00:19:58,080
If you can't hear means purification of mind in the form of enlightenment.

148
00:19:58,080 --> 00:20:08,080
So enlightenment can be gained only through practice of Ibhasana meditation, only through practice of wisdom.

149
00:20:08,080 --> 00:20:16,080
And that practice of wisdom is possible only through only when there is concentration.

150
00:20:16,080 --> 00:20:25,080
And concentration can be built only on the purification of moral conduct.

151
00:20:25,080 --> 00:20:38,080
But moral purification of moral conduct alone by itself cannot lead us to emancipation or enlightenment.

152
00:20:38,080 --> 00:20:41,080
So that is a false view.

153
00:20:41,080 --> 00:20:47,080
And through this false view we practice in sealer.

154
00:20:47,080 --> 00:20:56,080
And so the false view is here dependent for the practice of sealer.

155
00:20:56,080 --> 00:21:05,080
So there are two kinds.

156
00:21:05,080 --> 00:21:25,080
Let us go back a little, a little up, this page.

157
00:21:25,080 --> 00:21:31,080
Do you understand the double code about the middle of the first paragraph?

158
00:21:31,080 --> 00:21:35,080
Or what is included in the double code?

159
00:21:35,080 --> 00:21:40,080
The bikhus and bikhus party mokha.

160
00:21:40,080 --> 00:21:43,080
Page 12.

161
00:21:43,080 --> 00:21:48,080
Now the rules for bikhu means monks.

162
00:21:48,080 --> 00:21:51,080
And bikhus means an answer.

163
00:21:51,080 --> 00:21:57,080
So rules for monks and nuns are technically called by the mokha.

164
00:21:57,080 --> 00:22:02,080
So what party mokha is given in the translation?

165
00:22:02,080 --> 00:22:10,080
Since there are two, they are called double code.

166
00:22:10,080 --> 00:22:21,080
And there are 227 rules in the rules for monks and 311 rules in the rules for nuns of bikhunis.

167
00:22:21,080 --> 00:22:31,080
So women have more rules than monks have.

168
00:22:31,080 --> 00:22:39,080
And then those are those belong to the beginning of the life of purity.

169
00:22:39,080 --> 00:22:44,080
And that included in the duty set out in the countercars of the vigna.

170
00:22:44,080 --> 00:22:51,080
You know there are three piquas.

171
00:22:51,080 --> 00:22:53,080
And the first is vigna piqua.

172
00:22:53,080 --> 00:22:58,080
Vigna piqua consists of rules for monks and nuns and so on.

173
00:22:58,080 --> 00:23:11,080
Now vigna piqua is composed as the 227 rules and 311 rules for monks and nuns.

174
00:23:11,080 --> 00:23:16,080
And then stories leading to the laying down of these rules.

175
00:23:16,080 --> 00:23:21,080
And then some explanations of the words in the rules.

176
00:23:21,080 --> 00:23:26,080
And they are called sota vigna.

177
00:23:26,080 --> 00:23:30,080
And then there are some other books.

178
00:23:30,080 --> 00:23:43,080
Which are called kandaka, kandaka means just sections.

179
00:23:43,080 --> 00:23:49,080
And in the kandaka, mostly rules pertaining to the keeping are given.

180
00:23:49,080 --> 00:23:57,080
So there are kandaka's like how to perform the ordination ceremony.

181
00:23:57,080 --> 00:24:05,080
And what qualifications must one have in order to become, in order to be ordained and so on.

182
00:24:05,080 --> 00:24:17,080
And in another section, the duties which are to be performed to our teachers or to our pupils

183
00:24:17,080 --> 00:24:20,080
and to the guest monks and so on are mentioned.

184
00:24:20,080 --> 00:24:23,080
So these are called kandaka's.

185
00:24:23,080 --> 00:24:34,080
So that included in the duty set out in the kandaka's is that of good behavior.

186
00:24:34,080 --> 00:24:39,080
There are two books containing these kandaka's.

187
00:24:39,080 --> 00:24:48,080
One is called great kandaka and the other is called small or lesser kandaka.

188
00:24:48,080 --> 00:25:01,080
And those are very interesting books if you're interested in something like a social conditions during the time of the Buddha.

189
00:25:01,080 --> 00:25:11,080
What things, what utensils or what things they use, how they make houses and all these things.

190
00:25:11,080 --> 00:25:20,080
Because as the number of monks grow, there are more and more problems.

191
00:25:20,080 --> 00:25:23,080
And those monks are like children.

192
00:25:23,080 --> 00:25:27,080
Sometimes they just went to the Buddha and asked him what to do.

193
00:25:27,080 --> 00:25:32,080
Now, monks are to prepare, die for themselves.

194
00:25:32,080 --> 00:25:42,080
So they have to boil some box of the tree or some pieces of wood from which we can get kala.

195
00:25:42,080 --> 00:25:45,080
And then it over boil, overflow.

196
00:25:45,080 --> 00:25:49,080
And then they went to the Buddha and reported it to him.

197
00:25:49,080 --> 00:25:51,080
And the die over from what to do.

198
00:25:51,080 --> 00:25:55,080
And Buddha said, use something, use a filter or something.

199
00:25:55,080 --> 00:25:58,080
Turn down the front.

200
00:25:58,080 --> 00:26:03,080
So these books are interesting.

201
00:26:03,080 --> 00:26:07,080
So the duties and other things contain.

202
00:26:07,080 --> 00:26:13,080
And those are called here the virtue that is of good behavior.

203
00:26:13,080 --> 00:26:22,080
And those that are set out in the kandaka's.

204
00:26:22,080 --> 00:26:34,080
And page 13, paragraph 32, in the seven diet, all virtues subject to kangas is mundane.

205
00:26:34,080 --> 00:26:39,080
I hope you understand what subject to is.

206
00:26:39,080 --> 00:26:45,080
What is subject to kangas?

207
00:26:45,080 --> 00:26:49,080
Kangas means mental defilements.

208
00:26:49,080 --> 00:26:53,080
So subject to mental defilements.

209
00:26:53,080 --> 00:27:09,080
And subject to mental defilements really means the object of mental defilements.

210
00:27:09,080 --> 00:27:24,080
So that the virtue, which is object of mental defilements is called mundane, and that which is not is called super mundane.

211
00:27:24,080 --> 00:27:32,080
And next paragraph, in the first of the triads, the inferior, the medium and superior.

212
00:27:32,080 --> 00:27:41,080
The inferior is produced by inferior zeal, purity of consciousness, energy, or inquiry.

213
00:27:41,080 --> 00:27:46,080
Now here, you have to understand these four.

214
00:27:46,080 --> 00:27:52,080
zeal, zeal, zeal is the polywater for zeal is chanda.

215
00:27:52,080 --> 00:27:58,080
You have met the word chanda among the 52 mental factors.

216
00:27:58,080 --> 00:28:02,080
And it is translated there as coordination.

217
00:28:02,080 --> 00:28:07,080
So zeal chanda means a will to do.

218
00:28:07,080 --> 00:28:11,080
The mere will to do is called chanda.

219
00:28:11,080 --> 00:28:14,080
It is not desire.

220
00:28:14,080 --> 00:28:16,080
It is not attachment.

221
00:28:16,080 --> 00:28:18,080
So it is just a will to do.

222
00:28:18,080 --> 00:28:19,080
You want to do something.

223
00:28:19,080 --> 00:28:21,080
So you want to pick up a book.

224
00:28:21,080 --> 00:28:25,080
And that will to pick up a book is called chanda.

225
00:28:25,080 --> 00:28:30,080
So when that sea or chanda or coordination is strong, I mean,

226
00:28:30,080 --> 00:28:31,080
he are not strong.

227
00:28:31,080 --> 00:28:34,080
It is called inferior.

228
00:28:34,080 --> 00:28:45,080
And there are four kinds of, in partly they are called adipity, dominating factors.

229
00:28:45,080 --> 00:28:56,080
And please read a manual of a bit of a chapter 7 for these four kinds of adipities.

230
00:28:56,080 --> 00:29:05,080
Chapter 7, page 334 and 338.

231
00:29:05,080 --> 00:29:14,080
These are the four that are called pre-dominating factors because they

232
00:29:14,080 --> 00:29:21,080
arise with other mental concomitants, but one of them always,

233
00:29:21,080 --> 00:29:24,080
I mean, one of them predominate.

234
00:29:24,080 --> 00:29:29,080
Sometimes zeal predominate, sometimes consciousness predominate and so on.

235
00:29:29,080 --> 00:29:33,080
So they are called dominating factors.

236
00:29:33,080 --> 00:29:37,080
So the polywater zeal is chanda.

237
00:29:37,080 --> 00:29:43,080
And purity of consciousness, not necessarily purity of but just consciousness there.

238
00:29:43,080 --> 00:29:51,080
So energy or inquiry, inquiry really means knowledge or understanding,

239
00:29:51,080 --> 00:29:54,080
not really inquiring into something.

240
00:29:54,080 --> 00:30:02,080
The polywater inquiry is, we mantra, we mantra, we eye,

241
00:30:02,080 --> 00:30:06,080
M-A-M-S-A.

242
00:30:06,080 --> 00:30:13,080
And we mantra is translated as inquiry.

243
00:30:13,080 --> 00:30:17,080
The basic meaning of we mantra is inquiry.

244
00:30:17,080 --> 00:30:19,080
That is correct.

245
00:30:19,080 --> 00:30:25,080
But it is a synonym for polywapanya understanding.

246
00:30:25,080 --> 00:30:31,080
So here, sometimes understanding or knowledge or wisdom predominate.

247
00:30:31,080 --> 00:30:40,080
So when these four, actually one of these four are inferior,

248
00:30:40,080 --> 00:30:43,080
then the seala is called inferior seala.

249
00:30:43,080 --> 00:30:48,080
One day of medium, then they are called medium and one day of superior,

250
00:30:48,080 --> 00:30:50,080
they are called superior and so on.

251
00:30:50,080 --> 00:30:56,080
So please reach a manual of a bit of a chapter 7,

252
00:30:56,080 --> 00:31:08,080
page 334 and 338 for these four kinds of dominating factors.

253
00:31:08,080 --> 00:31:16,080
That motivated by craving, the purpose of which is to enjoy continued existence is inferior.

254
00:31:16,080 --> 00:31:23,080
Actually, what it means here is to enjoy better existence

255
00:31:23,080 --> 00:31:30,080
and to enjoy wealth or something in that existence.

256
00:31:30,080 --> 00:31:35,080
Not continued existence, but to enjoy a better existence

257
00:31:35,080 --> 00:31:43,080
and to enjoy some things there.

258
00:31:43,080 --> 00:31:49,080
And in paragraph 35, for third line,

259
00:31:49,080 --> 00:31:54,080
that practiced by the magnanimous ordinary man.

260
00:31:54,080 --> 00:31:57,080
What is that?

261
00:31:57,080 --> 00:32:03,080
Magnanimous ordinary man.

262
00:32:03,080 --> 00:32:07,080
The polywap is put to dinner.

263
00:32:07,080 --> 00:32:15,080
Put to dinner means a person who has not reached any of the stages of enlightenment.

264
00:32:15,080 --> 00:32:20,080
So we call him ordinary person.

265
00:32:20,080 --> 00:32:23,080
The usual translation for third word is waddling.

266
00:32:23,080 --> 00:32:28,080
So ordinary waddling.

267
00:32:28,080 --> 00:32:39,080
Magnanimous actually means he has good more of habit.

268
00:32:39,080 --> 00:32:45,080
And also he is radical.

269
00:32:45,080 --> 00:32:49,080
He is well read.

270
00:32:49,080 --> 00:32:53,080
That's a good question.

271
00:32:53,080 --> 00:32:56,080
Do you translate directly?

272
00:32:56,080 --> 00:33:03,080
He has hurt well because during the time of the Buddha and some time

273
00:33:03,080 --> 00:33:05,080
after there were no books.

274
00:33:05,080 --> 00:33:16,080
So whatever you learn, you learn from hearing someone.

275
00:33:16,080 --> 00:33:26,080
Now, I hope you have read these pages, right?

276
00:33:26,080 --> 00:33:35,080
I will be picking the places where I want to give you some more information or some explanation.

277
00:33:35,080 --> 00:33:38,080
So we go to page 15, paragraph 40.

278
00:33:38,080 --> 00:33:44,080
Now, in the second letter, there are training precepts announced for bikhus to keep

279
00:33:44,080 --> 00:33:49,080
irrespective of what is announced for bikhu needs.

280
00:33:49,080 --> 00:33:55,080
What do you understand by that?

281
00:33:55,080 --> 00:34:15,080
You know, I told you there are 227 rules for monks and 311 for nuns.

282
00:34:15,080 --> 00:34:23,080
Now, there are rules which are common to both monks and nuns.

283
00:34:23,080 --> 00:34:26,080
And there are others which are not common.

284
00:34:26,080 --> 00:34:38,080
Now, monks must keep 227 rules and also some of the rules laid down for nuns.

285
00:34:38,080 --> 00:34:40,080
That is what is meant here.

286
00:34:40,080 --> 00:34:50,080
So monks' precepts really means not only 227 rules but some other rules which are

287
00:34:50,080 --> 00:34:55,080
originally meant for nuns but which they also have to keep.

288
00:34:55,080 --> 00:35:04,080
For example, there is no rule in 227 rules that monks must not sing or not dance or not

289
00:35:04,080 --> 00:35:09,080
go to shoes and so on.

290
00:35:09,080 --> 00:35:21,080
No rules in 227 rules for monks but there is that rule for bikhu needs.

291
00:35:21,080 --> 00:35:33,080
So monks must also keep that rule although it is not included in the 227 rules.

292
00:35:33,080 --> 00:35:44,080
So when you say rules or monks' rules we mean 227 plus some other rules which they must

293
00:35:44,080 --> 00:35:50,080
keep although they are not in the rules for monks.

294
00:35:50,080 --> 00:35:56,080
The same rules for nuns, there are some rules which are common to both monks and nuns

295
00:35:56,080 --> 00:35:59,080
and so they must keep those rules too.

296
00:35:59,080 --> 00:36:04,080
So that is what is meant here.

297
00:36:04,080 --> 00:36:06,080
It is translated poorly because it is not right.

298
00:36:06,080 --> 00:36:13,080
The translation is I think out of it is a little article.

299
00:36:13,080 --> 00:36:18,080
So the meaning here is that.

300
00:36:18,080 --> 00:36:32,080
No, it is not correct.

301
00:36:32,080 --> 00:36:53,080
But you understand, right?

302
00:36:53,080 --> 00:37:05,080
Because in addition to some rules for bikhu needs in addition to their own rules for monks.

303
00:37:05,080 --> 00:37:14,080
So maybe including some rules for bikhu needs that are not for bikhu needs.

304
00:37:14,080 --> 00:37:20,080
So the same with the rules for bikhu needs.

305
00:37:20,080 --> 00:37:28,080
And then for Libibu here, the ten precepts of virtue for male and female novices,

306
00:37:28,080 --> 00:37:36,080
or no, they are not for novices, are the virtue of the not fully admitted.

307
00:37:36,080 --> 00:37:43,080
Now the five training precepts, ten when possible as a permanent undertaking.

308
00:37:43,080 --> 00:37:52,080
Here also the translation is a little incorrect.

309
00:37:52,080 --> 00:37:56,080
Now the five training precepts are permanent training.

310
00:37:56,080 --> 00:37:59,080
They are called in Pali nithya cila.

311
00:37:59,080 --> 00:38:02,080
Nithya means you know the word anithya, in permanent.

312
00:38:02,080 --> 00:38:04,080
So nithya is permanent.

313
00:38:04,080 --> 00:38:07,080
So they are called permanent cila.

314
00:38:07,080 --> 00:38:17,080
It means if you claim yourself to be a follower of the Buddha, then you must keep these five precepts.

315
00:38:17,080 --> 00:38:20,080
So they must be with you always.

316
00:38:20,080 --> 00:38:23,080
So they are the permanent undertaking.

317
00:38:23,080 --> 00:38:28,080
But ten are to be undertaken when you can.

318
00:38:28,080 --> 00:38:41,080
So the five training precepts as a permanent undertaking, ten when possible, and eight as the factors of Uposita.

319
00:38:41,080 --> 00:38:44,080
And not day, just Uposita.

320
00:38:44,080 --> 00:38:48,080
Now the word Uposita in Pali can mean two things.

321
00:38:48,080 --> 00:38:54,080
The observance and the day of observance.

322
00:38:54,080 --> 00:38:58,080
But here the day is not meant.

323
00:38:58,080 --> 00:39:01,080
The eight as the factors of Uposita means the eight precepts.

324
00:39:01,080 --> 00:39:09,080
Five precepts, the five permanent precepts, plus not eating after midday,

325
00:39:09,080 --> 00:39:13,080
not using flowers, perfumes and so on.

326
00:39:13,080 --> 00:39:23,080
And also not singing, dancing and not using luxurious beds and seeds.

327
00:39:23,080 --> 00:39:28,080
So these eight precepts are called Uposita precepts.

328
00:39:28,080 --> 00:39:37,080
And at my retreat, I have, the yogis take eight precepts.

329
00:39:37,080 --> 00:39:42,080
So the most important thing is not eating after midday.

330
00:39:42,080 --> 00:39:46,080
So these eight precepts are called Uposita.

331
00:39:46,080 --> 00:39:51,080
So if it is Uposita, for labiburg, we mean these eight precepts.

332
00:39:51,080 --> 00:39:54,080
But Uposita for months is different.

333
00:39:54,080 --> 00:40:05,080
Uposita for months is just recitation of the 227 rooms in an assembly.

334
00:40:05,080 --> 00:40:15,080
So labiburg should take five precepts, always keep five precepts, precepts always.

335
00:40:15,080 --> 00:40:21,080
And if they can, they can take eight precepts on certain days of the month.

336
00:40:21,080 --> 00:40:35,080
The usual days are full moon day, new moon day, and the eighth of each half, half month.

337
00:40:35,080 --> 00:40:40,080
We grew by lunar month, and the month is divided into two halves.

338
00:40:40,080 --> 00:40:44,080
The bright half and the dark half.

339
00:40:44,080 --> 00:40:48,080
So the middle of the bright half is the eighth day.

340
00:40:48,080 --> 00:40:53,080
And also the middle of the dark half is also the eighth day.

341
00:40:53,080 --> 00:40:57,080
So at least four days in a month.

342
00:40:57,080 --> 00:41:08,080
And that is how people in Buddhist countries now like in Burma, Sri Lanka, and Thailand,

343
00:41:08,080 --> 00:41:14,080
observe Uposita. They go to mostly they go to monasteries, and keep eight precepts,

344
00:41:14,080 --> 00:41:21,080
and stay the whole day at the monastery hearing the talks,

345
00:41:21,080 --> 00:41:29,080
offering some food or something to the monks, and practicing meditation.

346
00:41:29,080 --> 00:41:37,080
And in Thailand and Laos and Cambodia, they even spend the night at the monastery.

347
00:41:37,080 --> 00:41:43,080
And they go back to the Google Home, only next morning.

348
00:41:43,080 --> 00:41:46,080
Here we do a full moon ceremony.

349
00:41:46,080 --> 00:41:51,080
That's the best, the earliest basis of this is why we do a full moon ceremony,

350
00:41:51,080 --> 00:41:56,080
and we should do the pensions and give you precepts that comes later to that.

351
00:41:56,080 --> 00:41:59,080
So we have a ceremony, we're not from a full moon.

352
00:41:59,080 --> 00:42:01,080
I see.

353
00:42:01,080 --> 00:42:04,080
So that is called Uposita.

354
00:42:04,080 --> 00:42:08,080
So for a male and female leaf, all of us are the virtue of lady.

355
00:42:08,080 --> 00:42:18,080
But for monks, Uposita means, as I said, assembling at one place,

356
00:42:18,080 --> 00:42:23,080
and then one of the monks recites the roots of party mocha,

357
00:42:23,080 --> 00:42:29,080
and the other monks create respectful attention to his recitation.

358
00:42:29,080 --> 00:42:33,080
You know, there were no books in the olden days.

359
00:42:33,080 --> 00:42:37,080
And so to make them not forget their rooms,

360
00:42:37,080 --> 00:42:41,080
they remember their rooms is done in this way.

361
00:42:41,080 --> 00:42:46,080
So monks run these rooms by heart,

362
00:42:46,080 --> 00:42:51,080
and one of the monks recites the roots and others pay attention to his recitation.

363
00:42:51,080 --> 00:42:57,080
It is still done in Burma and Thailand and also in Ceylon.

364
00:42:57,080 --> 00:43:08,080
So this is called Uposita of monks.

365
00:43:08,080 --> 00:43:15,080
Now, the food traded teetrat from paragraph 42.

366
00:43:15,080 --> 00:43:26,080
From paragraph 42, the other explains the full kinds of sila or the full kinds of virtue.

367
00:43:26,080 --> 00:43:31,080
Often mentioned in the sodas,

368
00:43:31,080 --> 00:43:38,080
and these four monks are the important kinds of virtue.

369
00:43:38,080 --> 00:43:44,080
And these will be explained in more detail than the others.

370
00:43:44,080 --> 00:43:49,080
And first, they also give the text from the sodas.

371
00:43:49,080 --> 00:43:54,080
Say, it would dwells restraint with the party mocha restraint,

372
00:43:54,080 --> 00:43:57,080
possessed of the proper contact and resort,

373
00:43:57,080 --> 00:43:59,080
and seeing fear in the slightest fault,

374
00:43:59,080 --> 00:44:03,080
he's trained himself by undertaking the precepts of training.

375
00:44:03,080 --> 00:44:06,080
It's virtue of party mocha restraint.

376
00:44:06,080 --> 00:44:08,080
So this is one kind of sila.

377
00:44:08,080 --> 00:44:12,080
virtue of party mocha restraint simply means

378
00:44:12,080 --> 00:44:15,080
keeping the party mocha rules,

379
00:44:15,080 --> 00:44:19,080
keeping the 227 or the 311 rules,

380
00:44:19,080 --> 00:44:24,080
keeping them unbroken.

381
00:44:24,080 --> 00:44:26,080
And this will be explained later.

382
00:44:26,080 --> 00:44:32,080
And the next Bee Burograph shows the restraint

383
00:44:32,080 --> 00:44:35,080
of the sense faculties that means

384
00:44:35,080 --> 00:44:39,080
restraint of the eyes, ears, and so on.

385
00:44:39,080 --> 00:44:43,080
This is actually not strictly speaking,

386
00:44:43,080 --> 00:44:48,080
it is not sila because it is not keeping any rules,

387
00:44:48,080 --> 00:44:58,080
but since when you have restraint of sense faculties,

388
00:44:58,080 --> 00:45:00,080
you are sila.

389
00:45:00,080 --> 00:45:05,080
Your virtue is practically pure.

390
00:45:05,080 --> 00:45:08,080
So it is included in the sila.

391
00:45:08,080 --> 00:45:11,080
And it is on seeing a visible object with the eye,

392
00:45:11,080 --> 00:45:14,080
he apprehends neither the signs nor the particulars

393
00:45:14,080 --> 00:45:17,080
through which if you left the eye faculty

394
00:45:17,080 --> 00:45:19,080
unguarded evil and unprofitable states

395
00:45:19,080 --> 00:45:22,080
of covered thusness and grief might inflate him.

396
00:45:22,080 --> 00:45:24,080
He enters above the way of its restraint.

397
00:45:24,080 --> 00:45:25,080
He guards the eye faculty

398
00:45:25,080 --> 00:45:29,080
and takes the restraint of the eye faculty and so on.

399
00:45:29,080 --> 00:45:33,080
In brief what it means is

400
00:45:33,080 --> 00:45:36,080
when you see something,

401
00:45:36,080 --> 00:45:40,080
you try not to get acusala from it.

402
00:45:40,080 --> 00:45:44,080
That is what is meant by the restraint of the senses.

403
00:45:44,080 --> 00:45:47,080
So you see a beautiful thing,

404
00:45:47,080 --> 00:45:51,080
and if you have no wise attention

405
00:45:51,080 --> 00:45:55,080
then you will get attached to the thing

406
00:45:55,080 --> 00:45:57,080
and there will be acusala.

407
00:45:57,080 --> 00:46:01,080
So you try to keep yourself from getting acusala,

408
00:46:01,080 --> 00:46:04,080
getting unwholesome thoughts

409
00:46:04,080 --> 00:46:08,080
by actually by the practice of mindfulness.

410
00:46:08,080 --> 00:46:12,080
The practice of mindfulness is just to prevent

411
00:46:12,080 --> 00:46:20,080
attachment to these objects.

412
00:46:20,080 --> 00:46:28,080
So it does not mean that we are to close our eyes

413
00:46:28,080 --> 00:46:31,080
or to close our ears and not look at them

414
00:46:31,080 --> 00:46:34,080
or not listen to do something at all.

415
00:46:34,080 --> 00:46:35,080
It does not mean that.

416
00:46:35,080 --> 00:46:38,080
We will be seeing things and we will be hearing

417
00:46:38,080 --> 00:46:45,080
and

418
00:47:08,080 --> 00:47:11,080
hearing

419
00:47:11,080 --> 00:47:14,080
and hearing

420
00:47:14,080 --> 00:47:18,080
and hearing

421
00:47:18,080 --> 00:47:22,080
and hearing

422
00:47:22,080 --> 00:47:27,080
and hearing

423
00:47:27,080 --> 00:47:53,080
and hearing

