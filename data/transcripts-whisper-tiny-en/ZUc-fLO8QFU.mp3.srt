1
00:00:00,000 --> 00:00:13,000
Hi, today I'll be talking about the top five reasons why everyone should practice meditation.

2
00:00:13,000 --> 00:00:17,000
And this is in a accompaniment to my videos on how to meditate.

3
00:00:17,000 --> 00:00:24,000
Sort of as an encouragement for people who are unsure as to whether meditation is really of use to them.

4
00:00:24,000 --> 00:00:30,000
So counting down, today I'll be giving the number five reason why everyone should practice meditation.

5
00:00:30,000 --> 00:00:37,000
And the number five reason why everyone should practice meditation is that meditation purifies the mind.

6
00:00:37,000 --> 00:00:43,000
Now many people might not understand how this is a useful thing or what it means to have a pure mind.

7
00:00:43,000 --> 00:00:49,000
But actually we understand the meditative tradition that this is the real reason why we're practicing meditation.

8
00:00:49,000 --> 00:00:53,000
Because when the mind is purified it brings all the other benefits.

9
00:00:53,000 --> 00:00:56,000
So what does it mean to have a purified mind?

10
00:00:56,000 --> 00:01:01,000
Well, we understand that our minds are full of many different conflicting mind states.

11
00:01:01,000 --> 00:01:04,000
Some of them bring us happiness and some of them bring us suffering.

12
00:01:04,000 --> 00:01:08,000
And the ones that we call impure are the ones that bring us suffering.

13
00:01:08,000 --> 00:01:14,000
They either cause us to do bad deeds for other people, towards other people, or do things that hurt ourselves.

14
00:01:14,000 --> 00:01:21,000
We understand these to be positive emotions which are unwholesome or things which lead to suffering.

15
00:01:21,000 --> 00:01:24,000
The positive emotions are emotions that greed one thing.

16
00:01:24,000 --> 00:01:27,000
So we call them positive but actually they lead to addiction.

17
00:01:27,000 --> 00:01:32,000
And once we're addicted then it gives rise to a great deal of suffering.

18
00:01:32,000 --> 00:01:37,000
So the meditation helps us to overcome positive emotions which are in the end actually negative.

19
00:01:37,000 --> 00:01:39,000
Like positive reactions to things.

20
00:01:39,000 --> 00:01:45,000
And it also helps us to overcome negative reactions such as anger and disliking all sorts of hatred.

21
00:01:45,000 --> 00:01:51,000
Things which lead us to real and true mental suffering.

22
00:01:51,000 --> 00:01:56,000
It also leads us to overcome sort of neutral emotions where we don't judge something as good or bad,

23
00:01:56,000 --> 00:01:58,000
but we come to associate with it.

24
00:01:58,000 --> 00:02:00,000
We say that it's me or it's mine.

25
00:02:00,000 --> 00:02:02,000
We hold on to things as our possessions.

26
00:02:02,000 --> 00:02:10,000
We hold on to ideas and concepts, views, conceit, all sorts of ideas which lead us to hold ourselves higher than others

27
00:02:10,000 --> 00:02:14,000
or put ourselves down having low self-esteem and so on.

28
00:02:14,000 --> 00:02:23,000
And these we would say are three general characteristics of unwholesome mind states or things which we could call impure, impurities in the mind.

29
00:02:23,000 --> 00:02:28,000
Now meditation helps us to overcome these at the moment when we're practicing, when we're making the clear thought.

30
00:02:28,000 --> 00:02:32,000
Because these things arise in relation to an experience.

31
00:02:32,000 --> 00:02:41,000
So when we see something we like it or we don't like it or maybe we create some kind of idea of self-righteousness or self-esteem or so on

32
00:02:41,000 --> 00:02:44,000
either positive or negative.

33
00:02:44,000 --> 00:02:51,000
At the moment when we're meditating when we see something we simply remind ourselves that it's seeing that this is an experience of seeing.

34
00:02:51,000 --> 00:02:54,000
When we feel pain we remind ourselves that this is pain.

35
00:02:54,000 --> 00:02:57,000
We simply create the clear thought as pain.

36
00:02:57,000 --> 00:03:02,000
And when we do this our minds are unable to go the next step as liking or disliking.

37
00:03:02,000 --> 00:03:07,000
Our minds have fixed on the ultimate reality of the experience as simply what it is.

38
00:03:07,000 --> 00:03:10,000
So it's not good, it's not bad, it's simply what it is.

39
00:03:10,000 --> 00:03:15,000
This is what it means to create a clear thought and this is the first benefit is that it purifies the mind.

40
00:03:15,000 --> 00:03:18,000
It helps us to get rid of anger, it helps us to get rid of addiction.

41
00:03:18,000 --> 00:03:28,000
And it helps us to get rid of all sorts of conceit and views and all sorts of unpleasant, unskillful unwholesome mind states.

42
00:03:28,000 --> 00:03:31,000
So that's the first reason why everyone should practice meditation.

43
00:03:31,000 --> 00:03:41,000
Thank you for joining me and please look forward to the rest of my videos in the future.

