1
00:00:00,000 --> 00:00:06,000
Hi, so the next question comes from Stiltson.

2
00:00:06,000 --> 00:00:13,000
Does contemplation of the universe and the strong possibility of alien life relate to awareness of mind and meditation?

3
00:00:13,000 --> 00:00:21,000
I have often had a clear thought when I realized that the events of Earth are irrelevant compared to the scale of the universe.

4
00:00:21,000 --> 00:00:35,000
In short, no. And to explain, there are lots of things that can help your meditation practice.

5
00:00:35,000 --> 00:00:50,000
For instance, as a completely unrelated example, for people who are caught up in lust, it's good for them to contemplate the loathsome side of the body, the negative, the fact that the body smells the body,

6
00:00:50,000 --> 00:01:03,000
it becomes greasy and so on to contemplate the various parts of the body to see that what we're attracted to is actually just a physical process.

7
00:01:03,000 --> 00:01:19,000
For people who are very angry as a rule, it's good for them to think of other beings with love and to project loving kindness towards all beings, especially towards the people that they have problems with.

8
00:01:19,000 --> 00:01:28,000
These are generally useful meditation practices which can aid in the meditation practice on a preliminary level.

9
00:01:28,000 --> 00:01:32,000
But they don't have to do it inside. They don't bring about real insight.

10
00:01:32,000 --> 00:01:39,000
And neither does something like realizing the insignificance of your actions and so on and so on.

11
00:01:39,000 --> 00:01:44,000
They have a benefit, but they aren't a replacement for meditation themselves.

12
00:01:44,000 --> 00:01:47,000
They don't play an integral part in the meditation.

13
00:01:47,000 --> 00:01:57,000
There's an innumerable things you could do that that mind,

14
00:01:57,000 --> 00:02:10,000
experimental exercises you could undertake that would support your meditation because of their very nature in terms of reducing the ego, in terms of putting things in perspective and so on.

15
00:02:10,000 --> 00:02:20,000
And they're all useful and they're all recommendable, but it's important not to mix them up and to think, well that's meditation and now you're on the right path and that's enough.

16
00:02:20,000 --> 00:02:29,000
Meditation deals with the next level. It deals with things on a phenomenological level where you're dealing with experience as it comes and goes.

17
00:02:29,000 --> 00:02:34,000
And that can only occur inside yourself. It can only occur wherever the mind is.

18
00:02:34,000 --> 00:02:40,000
When the mind is focused at the eye, wisdom has to arise at the eye, when it's at the ear, when it's at the nose.

19
00:02:40,000 --> 00:02:46,000
Whatever sense that you're aware of at that moment, that's where wisdom arises.

20
00:02:46,000 --> 00:02:53,000
And so at the moment where you're contemplating this, you've got one level of wisdom, something that seems to you to be wise.

21
00:02:53,000 --> 00:02:55,000
And it certainly is a wise thought.

22
00:02:55,000 --> 00:03:02,000
But meditation goes the next step and has you examine that thought itself and say, what's involved with his thought.

23
00:03:02,000 --> 00:03:15,000
You've come to see that even in that thought, there's quite likely a sort of attachment where you think, oh that's good and you're very proud of yourself and you feel very good about it and it makes you feel happy, it makes you feel peace.

24
00:03:15,000 --> 00:03:25,000
And meditation is examining these things as well and saying, oh I'm attached to this, oh I have, you know this is making me think about that and so on and so on.

25
00:03:25,000 --> 00:03:41,000
And to break those apart and to come to let go of even that attachment. The Buddha said, we should not hold on to good things, good mind stains, good thoughts, good ideas, much, much more so than we should not hold on to bad things.

26
00:03:41,000 --> 00:03:47,000
He said, even the good ones we shouldn't hold on to, so how much more should we let go of bad things.

27
00:03:47,000 --> 00:03:57,000
The point is that we're expected or our practice is to not hold on to anything, even a good idea.

28
00:03:57,000 --> 00:04:02,000
Because it had its benefit and served its purpose and now it's gone.

29
00:04:02,000 --> 00:04:10,000
So even those ideas, they can be beneficial but know they're not a part of meditation per se.

30
00:04:10,000 --> 00:04:19,000
So it's important to keep a clear line between what is meditation practice, what is empirical understanding.

31
00:04:19,000 --> 00:04:23,000
I guess this is another way of explaining that that's not empirical.

32
00:04:23,000 --> 00:04:33,000
That's a theory that you have the idea that or it's equivalent to a theory where you say there's alien life and well theoretically that means that I'm insignificant and so on.

33
00:04:33,000 --> 00:04:40,000
But it's not the same as actually examining reality and seeing, it's not that you're insignificant, it's that you don't exist.

34
00:04:40,000 --> 00:04:45,000
The you, the eye doesn't exist and that's what comes from direct meditation practice.

35
00:04:45,000 --> 00:05:08,000
Okay, hope that helps to clear things up. Thanks for the question.

