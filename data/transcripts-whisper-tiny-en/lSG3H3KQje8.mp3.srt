1
00:00:00,000 --> 00:00:13,000
T.J. asks, can you explain the similarities and or differences between a monk, a meditator, and a yogi?

2
00:00:13,000 --> 00:00:41,000
A monk is a fully ordained male or female person, monks, male monks to have 227 precepts to keep, female monks have 311 precepts to keep.

3
00:00:41,000 --> 00:00:55,000
They are generally living in monasteries and living according to the Buddhist principles.

4
00:00:55,000 --> 00:01:14,000
Not all do teach dhama, not all practice the dhama even, but monks, male or female should have as their goal, as their highest goal to become enlightened in this life.

5
00:01:14,000 --> 00:01:32,000
A meditator in the yogi is basically the same, I would say to my understanding, people who come here to our place, to our monastery, are called meditators or yogis.

6
00:01:32,000 --> 00:01:44,000
I have eight precepts as long as they hear and try as good as they can to live almost like monks.

7
00:01:44,000 --> 00:01:55,000
They don't have so many rules and they are not required to have a knowledge about them in detail.

8
00:01:55,000 --> 00:02:07,000
But an interested yogi can really try to, without having the precepts, try to keep more than the eight precepts.

9
00:02:07,000 --> 00:02:24,000
The meditator and the yogi are both people who come to stay in meditation center or in monastery and practice meditation or maybe meditator could be distinguished as somebody

10
00:02:24,000 --> 00:02:33,000
who stays at home and practices there, I don't know if that's...

11
00:02:33,000 --> 00:02:40,000
Now I was just thinking about my favorite example, this monk who...

12
00:02:40,000 --> 00:02:48,000
I will have lots of stories about him, but one of the stories about one of the monks at Jamtanga was a little bit...

13
00:02:48,000 --> 00:03:00,000
Well, he had an anger problem. At one point, during our relationship, he came up to me and started abusing me.

14
00:03:00,000 --> 00:03:04,000
I can't remember what he said, it's been so long, I've forgotten the story.

15
00:03:04,000 --> 00:03:09,000
But he must have said something like your evil and he always said, you think you're God, you think you're God.

16
00:03:09,000 --> 00:03:18,000
No, it's because I stayed in the office with the computer and so I said, no, you're not allowed to come in at any hour of the day to use the computer because this is my meditation room as well.

17
00:03:18,000 --> 00:03:26,000
So I said we had many hours and so he thought I was controlled freak.

18
00:03:26,000 --> 00:03:35,000
Anyway, but he had all these things and I remember he must have said something fairly significant and he said, how can you...

19
00:03:35,000 --> 00:03:41,000
He said something about... because I wasn't talking to him, I wasn't just listening and not answering.

20
00:03:41,000 --> 00:03:44,000
And he said, how can you not talk to me? I'm a monk.

21
00:03:44,000 --> 00:03:48,000
And I looked at him and I said, you're not a monk.

22
00:03:48,000 --> 00:03:52,000
And he walked away, stormed off.

23
00:03:52,000 --> 00:03:57,000
But a half an hour later, he came back with some Vinaya book to show me.

24
00:03:57,000 --> 00:04:05,000
And he said, I want you to confess, if you confess your offense, you've...

25
00:04:05,000 --> 00:04:10,000
No, you have to go on probation. This is what he said. You have to go on probation.

26
00:04:10,000 --> 00:04:17,000
What do you mean? He said, you've committed a sangha disaster, which is an offense that requires probation.

27
00:04:17,000 --> 00:04:25,000
I said, how so? He said, you have accused me of committing a bara jika offense, an offense that requires you,

28
00:04:25,000 --> 00:04:30,000
or that makes you no longer a monk. He said, the only way that I use, you said that I'm not a monk.

29
00:04:30,000 --> 00:04:36,000
Therefore, the only way, and the only way that I cannot be a monk is if I've committed a bara jika.

30
00:04:36,000 --> 00:04:43,000
This is really... I haven't committed a bara jika. Therefore, you have no basis to accuse me.

31
00:04:43,000 --> 00:04:46,000
And because you have no basis to accuse me, and because...

32
00:04:46,000 --> 00:04:50,000
He didn't think he said if you're... because you're angry, but he said, because you've done it,

33
00:04:50,000 --> 00:04:58,000
obviously, that's a sangha disaster offense. This is his legal argument.

34
00:04:58,000 --> 00:05:02,000
And I said to him, that's not what I meant when I said, you're not a monk.

35
00:05:02,000 --> 00:05:10,000
I said, according to the Buddha's teaching, nahi papajito, purupagati, samanohoti, parang vihaita yanto.

36
00:05:10,000 --> 00:05:17,000
I even quoted it for him. I said, you know, vatapati monk and the Buddha said, one is not a...

37
00:05:17,000 --> 00:05:26,000
a recluse, papatita, someone who has gone forth, so in novice or a monk, or even a recluse, another tradition.

38
00:05:26,000 --> 00:05:29,000
Who abuses and reviles others.

39
00:05:29,000 --> 00:05:34,000
And one is not a samanat, a shaman, or a...

40
00:05:34,000 --> 00:05:40,000
It's the same, meaning one who has gone forth, one who has left the world.

41
00:05:40,000 --> 00:05:44,000
But on vihaita yanto, who scolds others, or who abuses others.

42
00:05:44,000 --> 00:05:49,000
And, oh, he was really... I really got him with that, because he likes these kind of arguments.

43
00:05:49,000 --> 00:05:53,000
And so he came back with a tray of flowers and apologized to me.

44
00:05:53,000 --> 00:05:58,000
So, all I wanted to say, I mean, that's the answer that I would have given as Paulini gave it.

45
00:05:58,000 --> 00:06:04,000
But there is some other way that you can answer this question, and that is to talk about what is a monk.

46
00:06:04,000 --> 00:06:08,000
A monk is the word comes from mono, which means alone or one.

47
00:06:08,000 --> 00:06:14,000
You know, the significance of being one who stays alone, one who is...

48
00:06:14,000 --> 00:06:19,000
Has the Buddha said, a karat, a karat, who is...

49
00:06:19,000 --> 00:06:27,000
Delights in staying alone, pantancha, sainas, and who delights in a secluded dwelling.

50
00:06:27,000 --> 00:06:33,000
A meditator is one who meditates, one who, as we answered before, is one who considers things,

51
00:06:33,000 --> 00:06:37,000
who ponders things, who does some sort of investigating the truth of things.

52
00:06:37,000 --> 00:06:41,000
He uses yoni soma nasi kara to get to the essence of the experience,

53
00:06:41,000 --> 00:06:45,000
so the essence of the problem, the essence of life.

54
00:06:45,000 --> 00:06:48,000
And the yogi is one who practices yoga.

55
00:06:48,000 --> 00:06:51,000
No yoga is an interesting word.

56
00:06:51,000 --> 00:07:04,000
Yoga means some sort of practice, or some sort of development or cultivation.

57
00:07:04,000 --> 00:07:08,000
Now, it's one who has...

58
00:07:08,000 --> 00:07:15,000
If I'm not wrong, it's one who has engaged in spiritual development.

59
00:07:15,000 --> 00:07:19,000
So actually, all three of them are one and the same,

60
00:07:19,000 --> 00:07:22,000
and it's not by wearing a robe that one becomes a monk.

61
00:07:22,000 --> 00:07:25,000
The Buddha is in one sense.

62
00:07:25,000 --> 00:07:30,000
In one sense, you can say that, as I said to this monk,

63
00:07:30,000 --> 00:07:33,000
I really got him. I said, you're not a monk.

64
00:07:33,000 --> 00:07:38,000
And actually, I don't think I was thinking clearly when I said it.

65
00:07:38,000 --> 00:07:40,000
I was probably upset at him.

66
00:07:40,000 --> 00:07:43,000
But then I did come back with a good reply.

67
00:07:43,000 --> 00:07:45,000
Because there are two kinds of bhikkhu.

68
00:07:45,000 --> 00:07:47,000
There are two kinds of bhikkhu.

69
00:07:47,000 --> 00:07:49,000
There's the sutta bhikkhu and the vinaya bhikkhu.

70
00:07:49,000 --> 00:07:53,000
Vinaya bhikkhu is one who has ordained and has a colossal member of precepts.

71
00:07:53,000 --> 00:07:56,000
The sutta bhikkhu is one who sees the danger,

72
00:07:56,000 --> 00:08:03,000
and some who sees the danger in clinging, the danger in creating,

73
00:08:03,000 --> 00:08:06,000
in becoming things, in having ambitions, in having goals,

74
00:08:06,000 --> 00:08:11,000
in having future thoughts about the future and intentions.

75
00:08:11,000 --> 00:08:16,000
Vayangikatiti, samsare, vayangikatiti bhikkhu.

76
00:08:16,000 --> 00:08:21,000
One is considered a bhikkhu because they see the vayaya in the ikati.

77
00:08:21,000 --> 00:08:30,000
They see the si ikati, the vayaya, the danger, the vayaya in samsare.

78
00:08:30,000 --> 00:08:32,000
That's why they're called a bhikkhu.

79
00:08:32,000 --> 00:08:34,000
So you can play with these words.

80
00:08:34,000 --> 00:08:37,000
The Buddha called himself a Brahman.

81
00:08:37,000 --> 00:08:41,000
He said a Brahman is someone who is, if I can remember this,

82
00:08:41,000 --> 00:08:47,000
bhikkhu, bhikkhu, bhikkhu.

83
00:08:47,000 --> 00:08:51,000
The bhikkhu means external, so he uses Brahman comes from bhikkhu.

84
00:08:51,000 --> 00:08:55,000
He says there's something like that, who has expelled evil.

85
00:08:55,000 --> 00:08:57,000
This is why they're called a Brahman.

86
00:08:57,000 --> 00:08:59,000
Samana is because they have santimana.

87
00:08:59,000 --> 00:09:01,000
They have a peaceful mind.

88
00:09:01,000 --> 00:09:05,000
And so a yogi, he would find a meaning for the word yogi.

89
00:09:05,000 --> 00:09:10,000
It's really all just words.

90
00:09:10,000 --> 00:09:13,000
The answer you gave is correct and probably the answer that was requested,

91
00:09:13,000 --> 00:09:16,000
but just thought it was an interesting opportunity to talk about.

92
00:09:16,000 --> 00:09:19,000
What's really important, what makes one a monk is that they stay alone.

93
00:09:19,000 --> 00:09:25,000
If a person who calls themselves a monk goes cavorting around with lay people

94
00:09:25,000 --> 00:09:33,000
or even with other monks and gets caught up with useless pursuits and socializing.

95
00:09:33,000 --> 00:09:37,000
Specifically, then they can't be called a monk, even if they do have all the rules

96
00:09:37,000 --> 00:09:42,000
and are correctly certified and so on.

97
00:09:42,000 --> 00:09:46,000
One can't be a meditator just because one stays in a meditation center

98
00:09:46,000 --> 00:09:50,000
or even just because one practice is walking and walks back and forth

99
00:09:50,000 --> 00:09:51,000
and sits for a long time.

100
00:09:51,000 --> 00:09:53,000
You have to actually be meditating.

101
00:09:53,000 --> 00:09:55,000
You have to actually be considering.

102
00:09:55,000 --> 00:10:00,000
And you can't be a yogi if you're not dedicated if you're just because you're

103
00:10:00,000 --> 00:10:06,000
wearing this or that clothing or you've taken on some precepts.

104
00:10:06,000 --> 00:10:14,000
A yogi as someone who is constantly involved who is engaged in the practice.

105
00:10:14,000 --> 00:10:37,000
So just more thoughts.

