Good evening, everyone broadcasting live November 1st, 2015
and
Non-damepada nights have now taken on a bit of a
And the word is a power. They're less colorful, less lively, but sure what we're
going to do now that I've decided not to do them above every night. Maybe we won't do
anything. Maybe I'll say hello. Thank you all for practicing. I guess I can take
the time now to talk about our online courses. It's not like there's any good
in advertising them unless I start to get a little bit more vigilant. We'll see.
Actually, it's not really about that. There's about 28 slots and 24 of them
have been claimed so far right now. And that goes up and down, but 24 is about as high
as we've gotten and pretty much everyone comes out as expected and is practicing
diligently at least an hour or two a day. So that's really been, it's become sort of the
highlight for me. It's a real focus because the biggest problem that I've had in sort
of sharing this meditation technique is that I've always felt unable to share more than
the first step. So everyone's been thinking, well, this is it. This is the meditation that
I'm supposed to do for the rest of my life. If I follow this tradition, it's just stepping
right, stepping left, rising, falling. It's a very simple technique, but there's many
of the people who have started taking the course have found it gets quite complex actually.
It's quite involved. And there's quite a bit to it, not just the adding of extra elements
to the practice, but there's also the interview process. And there's even more there's
the next step which will be having people come to do intensive courses, which of course
is the best, but also the most difficult to arrange for us to arrange and for the individuals
to arrange to actually get out here. But at least what we're doing is, well, besides
giving an introduction to the practice, we're also reducing the amount of time that
you would have to spend practicing intensively to finish the course. So my idea was that
if you finish, if you do this for a while, we could finish the foundation course intensively
in about a week. And to get, so to get through, to get that far would probably take
12 to 14 weeks, maybe more, maybe more. So you'd finish most of the, you'd get all the
technique down and then you'd come here and put it into practice intensively for about
a week and you could pretty easily finish the course. So there's still four slots available.
If someone wants to take Tuesday morning or Thursday afternoon or there's one on Saturday
morning, that's now available. Apart from that, we have 24 people to make it just great.
And you have to keep track of everyone. I don't know. I just asked them every time. What
did I give you last time? I don't keep track of anything that you're lying. No heavens.
Has anyone else done this that you know of in this tradition to hold online courses or is
this kind of groundbreaking? In this tradition, I think it's new. But our tradition isn't
all that huge. I don't know another traditions. It's not something that I think is very well
set up, very commonly set up where you'd have this kind of online system. I do know that
people have done right. So if you're just talking about the having online courses, I do
know people even in this tradition, another monk under my teacher is probably the one who
gave me this idea because he does this or he's done this for a long time where people would
meet, we would talk to him once a week and he'd leave them through the course. It's from
Israel. As far as I know, he still does this. So he could, he'd benefit from this kind
of set up. He can set him up on website. There's a little bit of tension, you know, because
the whole international group of people that people I started with are not, they're not
comfortable working with me because I had a falling out with their head guy many years
ago. I think there was a question. Are you able to get on the site?
No, it's coming up on my phone, just not on my PC, but it's just one of those days technologically.
That's strange. It is, yes, especially. Maybe there's a cable cut between Connecticut
and Ontario. No, the hangouts coming in just fine. And all the other websites are coming
up just fine.
We're going through Google that one. Oh, no, this is in Pennsylvania, the servers in Pennsylvania.
That's just a question of where the server is. There might be a problem.
But it's coming in on the phone. Perfect, like, I have found a Buddhist gathering in
my town. They tend to meditate twice a week there. I'd like to go, but they practice
karma, a meditation. And I'm not too enthusiastic about it. What do you think is it worth
going?
The problem we have here is no one hears. If you're listening to the audio, you don't hear
a robin. So, just talking to me.
Okay. I'm reading the question on the chat panel. So, you'll have to read it there.
Yeah, I know. I saw it. So, there's a group practicing karma,
palm meditation, not too enthusiastic about it. So, it's worth going. I don't know anything
about karma, palm meditation. I mean, I can't actually, I don't think this was actually
directed at me. It's kind of, what do you guys think? So, I'm just one of the guys, but
you're one of the guys with a little more informed opinion. So, that's a special to get
an answer for you. I don't have an informed opinion about karma pi. I mean, I guess I
can, I don't really have that much of an informed opinion on going to other meditation
groups. I know that when I have done it, I would just do my own meditation because it's
not like they have thought police that are making sure you do their meditation, but
it can also be a little bit awkward. I remember when I was first starting in this technique,
I would only do this, and I went to a Dharma group, and they did walking meditation in
the circle. And I was kind of rude about it. I just went off into, well, it wasn't rude.
I went off into a corner and did my own walking meditation back and forth. But I think,
you know, if that were to happen today, I certainly wouldn't go off and do my own walking
meditation. I would walk with them in the circle because they walked together, they walked
around the room, in a line, you know. So I wouldn't get as stubborn as I was. But for the
most part, you can do your own meditation, do a little bit of their meditation if you
like. It's nice to go in a group to be in a group, but I think in the end, you might
find it more trouble than it's worth, unless you're like their meditation technique.
Yeah, I've done the same going to different groups, but just done my own, not with the walking.
I do walk in a circle with them, but as far as the sitting, I do my own because as
he said, no one knows what's in your mind. Not no one. Most people don't know what's in
your mind. Never know. You might find someone who can read your mind. Well, then they'll
have probably a look on their face because they'll know that you're not doing the meditation
that they told you to. So you'll be able to tell, I guess.
Hi, Monday. Sometimes when I try to know I get caught in the train of thought and then
I automatically go back to noting my walking or whatever. And then I recognize I was thinking
or fantasizing in the past, maybe five or ten seconds ago. Do I note that or just move
on and try to catch current things arising?
Trying to know that I get caught in the train of thought and then I automatically go back
to noting my walking or whatever. Then recognize I was thinking, I see. Yeah, I mean, you
shouldn't automatically go back to noting walking. You should try to catch the thought
and say thinking. You should stop walking and say thinking thinking. Or you can just ignore
it. And again, if you recognize it, you can. I mean, walking, you can, to some extent,
just leave it alone. With all, keep it simpler than worrying about this just in general.
If anyone thing is taking you away significantly from the walking, then that's when you'd
want to really definitely stop and start acknowledging it. But to some extent, and so judgment
call on your part, you can do it either way. You don't have to stop to note every single
thing, but normally would stop and put our feet together and then say thinking, thinking
we're distracted, distracted.
All right. Well, originally I just intended this to be a sort of come online and say hello
kind of thing. So we don't have to sit around here for long periods of time. It's just a
way to connect with people. Here we have somebody. Please put a question tag at the beginning
of your posts like Patrick O'Just did. Sorry, a post with a cue, colon space.
Greetings from Singapore, Bante. Could you kindly explain the differences between awareness,
release and discernment, release, as mentioned in the sutas? Do you think they both lead
to total unbinding? I don't know what awareness, release and discernment, release are.
I think you're talking about Jetowi Muti and Banyai Muti. Jetowi Muti is, it really, it depends
exactly who you're talking to. Jetowi Muti involves summit meditation. Banyai Muti involves
Vipas in meditation. Now you might say that Jetowi Muti is only summit meditation, so it's entering
into the mundane channels. And Banyai Muti is the attainment of Nibana, or you can say
Banyai Muti is the attainment of Nibana without the jhanas, without the mundane jhanas.
And Jetowi Muti is the attainment of Nibana, with also having attained the summit of jhanas.
So there was a group of monks in the time of the Buddha who, another monk who became
our hands, and the monk came up to them and asked them, said, oh, so do you have all these magical powers and
have you attained the formless jhanas and so on. And they said, no, and they said, they said,
well, how could that be? And they said, well, we're Banyai Muti. We Muti is freedom and Banyai
I guess is discernment. It's wisdom is more common translation. But they're just terms and you
have to see how they're used. So Banyai Muti said to is thought, I think, by the commentaries to
mean that they didn't practice some of the meditation. They only practiced through Bhasa.
And it's curious, it's curious that in that, in that he doesn't ask, and they don't deny having
attained the first four jhanas. And we argued about this, some years back, there was a debate
about what this meant on one of the internet forums back when I was involved with them.
And someone was saying, trying to explain that away and say, well, just because they didn't,
it doesn't mean they actually attained it. But I said, well, honestly, they have to
entertain the first four jhanas because they attained them when they realized Nibana.
So that's why, in my mind, that's the reason why they couldn't ask about that because
absolutely, if you become an orahad, you have to have attained jhanas. It's just a super mundane
jhan. But you don't, you haven't attained, or you don't have to attain the arupa jhanas or magical
powers because those are specific to samata jhanas. So it's a bit of a technical question.
Then a less technical question. Can monks keep juice overnight?
Monks can keep juice until dawn. As soon as dawn arises, the time when you would then be able
to eat solid food, the, except for the opinion is that the juice has to be thrown out,
or used for other purposes, whatever those might be.
So if it's offered to you with your lunch, you can keep it throughout the afternoon and evening.
Well, there's some debate, there's some talk about how if you accept it for food, then you have
to eat it, but I don't think that applies to juice. But so if your intention is to have it in the
morning when you receive it, and then you decide to, instead, I'll have it in the afternoon,
there's something about that where it's a problem, but I'm not entirely sure how that,
that applies. I don't really pay much attention to that. If you keep salt as a medicine,
you can't then the next day sprinkle it on your food. But that doesn't have anything to do with
your intention. That's just you've kept it more than a day, so using it as food isn't allowed.
But using salt as a medicine, you can keep salt for your whole life. So yeah, as long as
it wouldn't work anyway, but yeah, juice is something that even if you get it in the morning,
you can keep it until the next morning.
Is that just something that you find personally helpful to you with going the whole afternoon?
Yeah, juice is great. It's the answer to this, and it's the happy medium, because a lot of
Buddhists who have who keep the role, not to eat in the evening, actually don't keep the
role, because they have a lot of soy milk and milk and even more, even heavier things,
like they were trying to argue to have this soft tofu drinks, like it's a ginger,
sweet ginger drink with soft soft tofu in it. Like a smoothie? No, it's chunks of very soft tofu,
the chunks of them in the ginger drink, it's a Thai thing. It's too sweet, but otherwise it's
pretty good. I mean, having ginger and tofu is great, but probably a bit too sweet to be healthy.
Because I think just in general, in the volunteer group, we're trying to figure out ways to
to support you as best as possible. So if we can figure out a way to have juice there that's
offered to you, and then you could keep it through the afternoon or evening that would,
that sounds like it would be a good thing. Try to figure that out.
I mean, if someone comes in the morning and offers a juice drink as well, then I can keep that
for the afternoon. But I can also, because people have already given me the meals in advance,
through the gift cards, I can go to Martin's potentially and get a juice there.
Do they have something good there? Yeah, they have orange and apple juice. I think it's
it's overpriced, but I'm sure it's overpriced, but that's okay though.
I'm working on these Spanish subtitles. There's a program to edit subtitles by
files and once I finish the first file, I'll send it to your mail.
Not a question, but
Okay, well, thank you for working on the subtitles.
Okay, so you can't, I guess you can't. That means you can't upload them yourself. You've
looked and kept. Let's see.
Well, no matter what work is done, it would have to go through your YouTube channel for it to
reach its intended audience. There's a YouTube subtitles add-on, that's probably
a bit. You cannot add a subtitle, track to someone else's video. You just have to share it.
So, I have to be responsible to put this update on.
Yes, Fernando says he can't, but he can send them to you.
And then a question from Anthony,
Dante, if a layperson is watching over food in the monastery for a joke or night,
you have not broken a rule, right? Well, monastery fridges are, I mean, technically, it has to be
a little bit more organized than that. There has to be a room in the monastery. I mean, maybe even
a building. I don't know how. If you want to get really technical, but at least a room that is
designated as a, I think it's called a kapiyagara or something of room for the kapiyat,
or something like that. I don't know, I can't remember the exact poly term, but it's a place where
the lay people, and it belongs to the lay people. So, the stuff in there is not for the monks.
So, if I had juice in my room, if someone kept juice in my room, then I couldn't drink it,
even if they offered it to me. There was a refrigerator in my room, but we consider the kitchen
to be off limits to me. All the stuff in the kitchen is not mine. All the food and all the
any of the drinks and any of the stuff, except the ice cubes. I had to make ice cubes a couple
nights ago, because no one had no one made ice cubes. That's an important first aid item.
We didn't have any. Are you listening, I don't know? Did someone enjoy themselves?
I did. I injured myself. I was putting up a screen. A couple of the guys came from the Buddhism
Association, came by on Sunday, Saturday, yesterday, Friday, maybe Friday, and they wanted to watch
a Buddhist movie or a Buddhist theme movie. I think they ended up watching just in order. I don't
know what they ended up watching. I came down and it was in the middle of actually, I don't
know what to say. It was in the middle of something that probably wasn't appropriate for a Buddhist
one. Anyway, I think probably we're going to have to talk about if we want to do that again,
but definitely if we have Buddhist movies, I'm happy that they come. I think they just didn't have
a sense of what was a good Buddhist movie. Don't worth watching, but they asked if they could come
over and do some meditation and watch a Buddhist movie. That's great. So I put up the screen
from them in the basement, but as I was putting up the screen, I pinched my finger.
Pretty bad, actually. I'm surprised they didn't get all blood blister.
But I immediately went and shoved it on. It's placed it against something really, really
cold in the freezer, probably the thing that creates the ice. I just left it there for a bit.
Well, I looked for ice cubes and couldn't find anything. Hopefully everyone is listening,
but you can always take water, right? Even if it's from the kitchen.
Yeah. No, and I made ice cubes. There's ice cubes there. I mean, I filled up the ice
cube trays. They just weren't full. Yeah, they evaporate after a while if no one uses them.
That's always the case in my freezer. They're always empty ice cube trays because they evaporate.
Yeah, good to know all the possibilities of being able to send support and have it be useful to you.
For clarification, I mean in the case that the layperson intends to offer the food the next
morning, is that okay? It doesn't matter whether the layperson intends to offer it, it matters
where it's kept. So I kept outside of the monastic. If food or anything is kept in the monastic
quarters, that's not allowed. The monastic. It's called anti something that kept inside.
So at your monastery, you would consider the kitchen to belong to the meditators.
We haven't officially designated the kitchen as being the special room, but that's sort of just a
given. In fact, I guess I could say the only part of this that this really the monastic part
is the upstairs. So I would suggest that that is off the mitts, keeping stuff upstairs is not allowed,
or if it's allowed, but I couldn't partake of it.
And when food has been sent for the meditators, for example, the breakfast that Tina will send
some some dry foods for breakfast and things, and if the meditator was eating breakfast and
wish to prepare breakfast and offer it to you, is that allowed? Yeah, the meditators as long as the
food is not kept in my not kept in my dwelling, not kept by me and not cooked by me. Those are the
three aspects that are not allowed. That's good to know.
Okay, enough. No. I think we just sit here and people will start to
try and think up questions to throw at me. If people had questions, they would have asked them
I know. No, I know meditation questions, but I was really trying to get a little more information
on what's proper as far as offering food to you.
And I think now that's not a question, that's a comment. Okay, tomorrow we'll have Kundalakacy,
the story of Kundalakacy, to say, Tamapad and Ex Tamapad first. See, when we go without it for a
couple of days, then everybody's looking forward to it. So it's more special. It's more special.
Okay, thank you everyone. Thanks for having me for helping out.
Thank you very much. Good evening.
