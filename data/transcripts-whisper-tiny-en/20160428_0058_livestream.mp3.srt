1
00:00:00,000 --> 00:00:26,000
Good evening, everyone, broadcasting live April 27, 2016.

2
00:00:26,000 --> 00:00:42,000
Tonight's quote from the Senyutani Kaya is, again, not the Buddha himself.

3
00:00:42,000 --> 00:00:49,000
This is a man named Jita.

4
00:00:49,000 --> 00:00:56,000
Jita was a rather remarkable person, not a monk.

5
00:00:56,000 --> 00:00:59,000
He was a rich man.

6
00:00:59,000 --> 00:01:13,000
And he was declared by the Buddha to be preeminent among laymen who preached the dhamma.

7
00:01:13,000 --> 00:01:25,000
And so there's a whole section of the Jita Senyut is a record of conversations between him and monks.

8
00:01:25,000 --> 00:01:41,000
So he actually cleared up some debates between the monks, and that's what this quote doesn't actually show.

9
00:01:41,000 --> 00:01:45,000
So the quote is giving an example.

10
00:01:45,000 --> 00:01:55,000
And it sounds like he's repeating something that the monks already know.

11
00:01:55,000 --> 00:01:58,000
But the truth is that the monks were divided.

12
00:01:58,000 --> 00:02:01,000
These elder monks, in fact.

13
00:02:01,000 --> 00:02:08,000
And the question is actually an interesting question for meditators.

14
00:02:08,000 --> 00:02:17,000
The question is whether, let me read the poly.

15
00:02:17,000 --> 00:02:34,000
Senyut Janantiwa, I will say, Senyut Janiya Damatiwa, Senyut Janah, fetter or a bond or a bond.

16
00:02:34,000 --> 00:02:51,000
And the dhammas which are bound, the dhammas which are joined, conjoined.

17
00:02:51,000 --> 00:02:56,000
Are these two concepts?

18
00:02:56,000 --> 00:03:25,000
Are they one in, are they separate and different in meaning end in letter?

19
00:03:25,000 --> 00:03:49,000
So the meaning is you've got the bond, the fetter, that which binds things.

20
00:03:49,000 --> 00:03:53,000
And then you've got the things that are bound together.

21
00:03:53,000 --> 00:03:56,000
The question is, it's quite a deep question actually.

22
00:03:56,000 --> 00:04:00,000
The question is whether these are one and the same and just different in name.

23
00:04:00,000 --> 00:04:04,000
And some of the elders thought that the things that were bound together, the things that

24
00:04:04,000 --> 00:04:12,000
are bound together are actually the bind and some thought they were separate.

25
00:04:12,000 --> 00:04:14,000
And then Jita heard about this.

26
00:04:14,000 --> 00:04:16,000
And so he went to see the monks.

27
00:04:16,000 --> 00:04:28,000
He said, Tsutametan Monday, I've heard that many monks are arguing about this, or split

28
00:04:28,000 --> 00:04:30,000
on this matter.

29
00:04:30,000 --> 00:04:40,000
And he says, they say, A1 Gahapati, yes, householder, it is so.

30
00:04:40,000 --> 00:04:47,000
And Jita says that they're separate.

31
00:04:47,000 --> 00:04:54,000
He says, Nanataji, one Nanami Anjana, they're different in letter and different in meaning.

32
00:04:54,000 --> 00:05:03,000
And then he gives the simile of a black cow and a white cow, and they're tied together

33
00:05:03,000 --> 00:05:13,000
by a rope, and wherever the white one, the white cow pulls, the black cow has to go as well.

34
00:05:13,000 --> 00:05:18,000
So the question is whether the black cow is better is holding the white cow or the white

35
00:05:18,000 --> 00:05:20,000
cow is holding the black cow.

36
00:05:20,000 --> 00:05:24,000
And the monks say, well, no, the rope is holding the both.

37
00:05:24,000 --> 00:05:33,000
And he says in the same, just the same, the eye is not a light.

38
00:05:33,000 --> 00:05:41,000
Visions are not a feather for the eye, nor is the eye a feather for visions.

39
00:05:41,000 --> 00:05:47,000
So it's not because we see things that just seeing things is not a feather.

40
00:05:47,000 --> 00:05:51,000
It's not getting caught up.

41
00:05:51,000 --> 00:05:54,000
You don't get attached just because you're seeing, basically.

42
00:05:54,000 --> 00:05:59,000
And this is very deep and sort of, these are the kind of things that they would debate.

43
00:05:59,000 --> 00:06:07,000
And it goes to show how deep was their thought process, what they were thinking about.

44
00:06:07,000 --> 00:06:14,000
And here, when you hear something, it's not the sound that is the bond.

45
00:06:14,000 --> 00:06:22,000
And the clinging isn't in the sound.

46
00:06:22,000 --> 00:06:24,000
When you smell, taste, feel, think, none of these are the problem.

47
00:06:24,000 --> 00:06:29,000
Very simply, it's basically what we talk about in meditation.

48
00:06:29,000 --> 00:06:33,000
And just because you experience something, this isn't the problem.

49
00:06:33,000 --> 00:06:38,000
And when I was giving five-minute meditation lessons, this is how I started off, I said,

50
00:06:38,000 --> 00:06:46,000
this meditation is based on the premise that it's not our experiences that cause us suffering.

51
00:06:46,000 --> 00:06:49,000
It's our reactions to them.

52
00:06:49,000 --> 00:06:52,000
So it's the craving.

53
00:06:52,000 --> 00:07:09,000
So it is our journey in the past, they really are the ones who are here in the past,

54
00:07:09,000 --> 00:07:17,000
in the past, out there.

55
00:07:17,000 --> 00:07:33,000
That's the bond. That's what binds them, and that's the bind in this case.

56
00:07:33,000 --> 00:07:39,000
So even when you feel pain, it's not the pain, that's the problem.

57
00:07:39,000 --> 00:07:42,000
When someone's yelling at you, it's not the yelling, it's the problem.

58
00:07:42,000 --> 00:07:47,000
Bad thoughts. Thoughts are not bad. They're just thoughts.

59
00:07:47,000 --> 00:07:52,000
In fact, quite neutral. We once had a monk in Thailand.

60
00:07:52,000 --> 00:08:01,000
I don't know if I've recently mentioned him, but I know it was in New York, I was just talking about him.

61
00:08:01,000 --> 00:08:09,000
He told me, he was really crazy.

62
00:08:09,000 --> 00:08:14,000
He thought we were all conspiring against him. It was interesting.

63
00:08:14,000 --> 00:08:17,000
When they came up to me and said,

64
00:08:17,000 --> 00:08:21,000
they're talking about me over the loudspeakers. They're saying,

65
00:08:21,000 --> 00:08:27,000
and they said, well, I really don't think any said, oh, you are in with them.

66
00:08:27,000 --> 00:08:32,000
He really thought we were all conspiring to drive him crazy.

67
00:08:32,000 --> 00:08:35,000
At the monastery, it was quite interesting.

68
00:08:35,000 --> 00:08:39,000
But he wants to send to me. He said, I'm having these thoughts.

69
00:08:39,000 --> 00:08:41,000
I said, well, they're just thoughts. He said, oh, no.

70
00:08:41,000 --> 00:08:46,000
They're just the most horrible thoughts that you could possibly have.

71
00:08:46,000 --> 00:08:49,000
You know, and the end of thought is a thought.

72
00:08:49,000 --> 00:08:52,000
And he drove himself crazy. He ended up cutting his wrists and

73
00:08:52,000 --> 00:08:56,000
plating himself on fire. It all sorts of crazy things.

74
00:08:56,000 --> 00:08:58,000
Ended up disrobing.

75
00:08:58,000 --> 00:09:01,000
But we do this.

76
00:09:01,000 --> 00:09:04,000
We all do this to some extent. We beat ourselves up over our thoughts.

77
00:09:04,000 --> 00:09:06,000
Don't think that can't think that.

78
00:09:06,000 --> 00:09:08,000
I'm so evil for thinking that.

79
00:09:08,000 --> 00:09:11,000
And actually, you're not. The thoughts are just thoughts.

80
00:09:11,000 --> 00:09:13,000
That's not where evil comes from.

81
00:09:18,000 --> 00:09:22,000
So actually, Jitha was teaching them something here.

82
00:09:22,000 --> 00:09:27,000
He was teaching them, and so they don't say, oh, very good, very good.

83
00:09:27,000 --> 00:09:34,000
Very good, very good. Like patronizing, they say, wow,

84
00:09:34,000 --> 00:09:37,000
love, ha, ting, ha, ting, sula, sula, dong ting.

85
00:09:37,000 --> 00:09:39,000
It's a great game for you, householder.

86
00:09:39,000 --> 00:09:43,000
That you have this deep.

87
00:09:43,000 --> 00:09:49,000
You know, that you have the eye of wisdom.

88
00:09:49,000 --> 00:10:01,000
You have that by you, the eye of wisdom goes to the deep.

89
00:10:03,000 --> 00:10:08,000
You have a deep eye of wisdom in the deep teaching of the Buddha's.

90
00:10:08,000 --> 00:10:10,000
Deep words of the Buddha's.

91
00:10:10,000 --> 00:10:12,000
Something like that.

92
00:10:12,000 --> 00:10:20,000
So yeah, the bondage.

93
00:10:20,000 --> 00:10:25,000
What is it that binds things then?

94
00:10:25,000 --> 00:10:28,000
We start with ignorance.

95
00:10:28,000 --> 00:10:33,000
The base of all attachment is ignorant.

96
00:10:33,000 --> 00:10:37,000
It's just not seeing clearly ignorance isn't something hard to understand.

97
00:10:37,000 --> 00:10:42,000
It just means you didn't see it clearly enough.

98
00:10:42,000 --> 00:10:48,000
And that leads to misconception.

99
00:10:48,000 --> 00:10:57,000
It leads us to like things that are not worth liking, and it leads to habits of preference.

100
00:10:57,000 --> 00:10:59,000
Because we're just guessing.

101
00:10:59,000 --> 00:11:05,000
You know, when we grow up, we do this even in this life to some extent.

102
00:11:05,000 --> 00:11:13,000
When you ask a kid, which one they want, in many cases it's all the same.

103
00:11:13,000 --> 00:11:16,000
When you ask a meditator, it's hard to ask.

104
00:11:16,000 --> 00:11:18,000
Would you like this?

105
00:11:18,000 --> 00:11:20,000
Or would you like that?

106
00:11:20,000 --> 00:11:24,000
Not really able to make a decision because there's no reason to pick one over the other.

107
00:11:24,000 --> 00:11:26,000
But we do.

108
00:11:26,000 --> 00:11:34,000
We build up likes and dislikes often just by chance.

109
00:11:34,000 --> 00:11:36,000
Because of that's how the chips fall.

110
00:11:36,000 --> 00:11:39,000
That's where our life leads us.

111
00:11:39,000 --> 00:11:43,000
We acquire tastes.

112
00:11:43,000 --> 00:11:47,000
You know, some of its genetics, some of its organic.

113
00:11:47,000 --> 00:11:57,000
But some of its just random.

114
00:11:57,000 --> 00:12:04,000
And so the ignorance is a breeding ground.

115
00:12:04,000 --> 00:12:05,000
It's like darkness.

116
00:12:05,000 --> 00:12:12,000
Ignorance is a direct parallel to the darkness.

117
00:12:12,000 --> 00:12:13,000
It's mental darkness.

118
00:12:13,000 --> 00:12:21,000
So if you know anything about darkness, not only is it impossible to see what is right,

119
00:12:21,000 --> 00:12:25,000
but it's a breeding ground of all sorts of things.

120
00:12:25,000 --> 00:12:28,000
It's a breeding ground of bacteria.

121
00:12:28,000 --> 00:12:32,000
It's a breeding ground of viruses.

122
00:12:32,000 --> 00:12:37,000
I don't know if bacteria gets rotten things.

123
00:12:37,000 --> 00:12:39,000
Things rot in the dark.

124
00:12:39,000 --> 00:12:40,000
Mold grows.

125
00:12:40,000 --> 00:12:46,000
Fungus grows in the dark.

126
00:12:46,000 --> 00:12:50,000
And so all these rotten things in our minds grow based on our ignorance.

127
00:12:50,000 --> 00:12:58,000
And all it takes is to shine a light in, when you shine a powerful light in the darkness disappears.

128
00:12:58,000 --> 00:13:02,000
So you don't have to worry about getting rid of the ignorance.

129
00:13:02,000 --> 00:13:06,000
But the bad things start to shrivel up as well.

130
00:13:06,000 --> 00:13:13,000
The rotten things start to dry up and wither away.

131
00:13:13,000 --> 00:13:18,000
Because they rely on the darkness for support.

132
00:13:18,000 --> 00:13:22,000
So how do we shine this light in?

133
00:13:22,000 --> 00:13:27,000
It's a little more complicated than just shining a light, a little bit more complicated.

134
00:13:27,000 --> 00:13:30,000
Because there's different aspects.

135
00:13:30,000 --> 00:13:44,000
And I did a video if you know my video on pornography and pornography and masturbation and addiction in general.

136
00:13:44,000 --> 00:13:56,000
It was that there are, for a long time, had this concept of three aspects to an addiction.

137
00:13:56,000 --> 00:14:07,000
The object, which is seeing, hearing, smelling, tasting, feeling, or thinking.

138
00:14:07,000 --> 00:14:21,000
That's the first thing.

139
00:14:21,000 --> 00:14:25,000
The second thing is the pleasure that comes from it.

140
00:14:25,000 --> 00:14:28,000
You could do the same with pain as well.

141
00:14:28,000 --> 00:14:33,000
When you see something and it makes you happy, when you hear something, it makes you happy.

142
00:14:33,000 --> 00:14:37,000
So there's that aspect of it.

143
00:14:37,000 --> 00:14:45,000
And then the third is the desire, which is not the feeling, but it's this attraction, like a magnet,

144
00:14:45,000 --> 00:14:54,000
a clinging, like a pressure in the mind and the stickiness.

145
00:14:54,000 --> 00:14:59,000
And going back and forth between those three, this is what I talked about in this video.

146
00:14:59,000 --> 00:15:03,000
And I found this in the Buddha's teaching.

147
00:15:03,000 --> 00:15:08,000
The Buddha said, some teachers teach one or the other, but the best teacher teaches all three.

148
00:15:08,000 --> 00:15:16,000
They teach us the base, teaches the feeling, teaches the attachment.

149
00:15:16,000 --> 00:15:20,000
So this is how we deal with addiction specifically.

150
00:15:20,000 --> 00:15:28,000
If you want to deal with your attachment, go back and forth between these three.

151
00:15:28,000 --> 00:15:32,000
Every time it comes up, just be methodical, systematic.

152
00:15:32,000 --> 00:15:34,000
And you start to change those habits.

153
00:15:34,000 --> 00:15:37,000
You'll start to override this finger.

154
00:15:37,000 --> 00:15:43,000
It's because you'll see that, oh, yeah, well, there really isn't anything desirable about that at all.

155
00:15:43,000 --> 00:15:50,000
Eventually, once your wisdom gets stronger, you just don't have any desire for the things you used to desire.

156
00:15:50,000 --> 00:16:00,000
And look at it, and you rightly see that it's not worth desiring.

157
00:16:00,000 --> 00:16:04,000
So that's the demo for tonight.

158
00:16:04,000 --> 00:16:10,000
Do you have any questions?

159
00:16:10,000 --> 00:16:17,000
You guys can go ahead.

160
00:16:17,000 --> 00:16:25,000
Is there anything you can say about mindfulness of falling asleep?

161
00:16:25,000 --> 00:16:29,000
A similar experience to dying would find it very difficult.

162
00:16:29,000 --> 00:16:31,000
Well, you can try.

163
00:16:31,000 --> 00:16:35,000
You know, you try to be mindful up until the moment you fall asleep.

164
00:16:35,000 --> 00:16:45,000
If you're really good, you can know whether you fell asleep on the rising or the falling.

165
00:16:45,000 --> 00:16:51,000
You know, you just be mindful until you fall asleep.

166
00:16:51,000 --> 00:16:54,000
It's not something you should worry about or strive for.

167
00:16:54,000 --> 00:16:55,000
You just work at it.

168
00:16:55,000 --> 00:16:59,000
When you lie down at night, you know, try to take it as lying meditation.

169
00:16:59,000 --> 00:17:02,000
Don't just fall asleep.

170
00:17:02,000 --> 00:17:05,000
But it works better when you're on a meditation course.

171
00:17:05,000 --> 00:17:23,000
And life, of course, it's very difficult to be that mindful.

172
00:17:23,000 --> 00:17:47,000
So that's it.

173
00:17:47,000 --> 00:18:02,000
Any other questions?

174
00:18:02,000 --> 00:18:20,000
I think there's a bit of a lag between what I say and what comes up in the chat.

175
00:18:20,000 --> 00:18:25,000
Anyway, you have questions.

176
00:18:25,000 --> 00:18:38,000
Thank you.

177
00:18:38,000 --> 00:19:03,000
Thank you.

