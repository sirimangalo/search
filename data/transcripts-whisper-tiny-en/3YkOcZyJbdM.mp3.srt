1
00:00:00,000 --> 00:00:20,560
Okay, good evening everyone, welcome to our evening dhamma, we have questions and answers

2
00:00:20,560 --> 00:00:21,560
tonight.

3
00:00:21,560 --> 00:00:34,840
I'm using a new microphone, so hopefully this is a little louder.

4
00:00:34,840 --> 00:00:40,440
Without further ado, we'll get right into questions.

5
00:00:40,440 --> 00:00:45,240
How do you know the difference between having right view and being judgmental?

6
00:00:45,240 --> 00:00:55,640
Okay, let's take the question first, we'll deal with your story separately, or let's

7
00:00:55,640 --> 00:01:14,600
make sure this interface is working, okay, it is right view and being judgmental.

8
00:01:14,600 --> 00:01:18,120
So what right view is, why I want to take this separately from your example is because

9
00:01:18,120 --> 00:01:24,040
I don't think your example has much to do with, well, I understand what you're getting

10
00:01:24,040 --> 00:01:28,040
at, but let's be clear about what right view is.

11
00:01:28,040 --> 00:01:36,600
Right view is dealing with belief, so your belief that something is true, that something

12
00:01:36,600 --> 00:01:44,680
is right, true, I guess, is of course the most simple way of understanding right view and

13
00:01:44,680 --> 00:01:52,640
Buddhism, it's belief in for truth, and the for noble truth, right, belief in suffering,

14
00:01:52,640 --> 00:01:57,480
the cause of suffering, the cessation of suffering, and the path which leads to the cessation

15
00:01:57,480 --> 00:02:03,640
of suffering, so I don't know if it's being pedantic or nitpicking, but I'd like to be

16
00:02:03,640 --> 00:02:12,000
clear about terms, otherwise, the question, it becomes doubt arises more easily if you're

17
00:02:12,000 --> 00:02:21,520
not clear about your terms, so your example is being invited to a wedding, and as a result

18
00:02:21,520 --> 00:02:25,680
of practice, I have no interest in drinking, dancing, beautifying yourself anymore, so you

19
00:02:25,680 --> 00:02:30,880
don't want to go but feel obliged to, how do you know if this is a result of your practice

20
00:02:30,880 --> 00:02:37,720
or is it my ego using the path as an excuse not to go?

21
00:02:37,720 --> 00:02:48,080
So the only way is to be sure about situations like this is to understand the moments

22
00:02:48,080 --> 00:02:56,080
of experience involved, not wanting to go somewhere, it's just words and it's a phrase

23
00:02:56,080 --> 00:03:02,640
that we use in languages, of course, limited, but if you actually dislike, if disliking arises

24
00:03:02,640 --> 00:03:08,080
in the mind, an aversion arises in the mind when you think about going, then that's

25
00:03:08,080 --> 00:03:11,800
a state of aversion and that's wrong.

26
00:03:11,800 --> 00:03:19,360
But I would say that's just aversion, as far as ego, I guess there's the idea that you

27
00:03:19,360 --> 00:03:25,400
might be trying to convince yourself, use it as an excuse which I don't know that

28
00:03:25,400 --> 00:03:33,560
exactly call it ego, I'd call it manipulate, manipulation, manipulating yourself, lying

29
00:03:33,560 --> 00:03:39,600
to yourself basically, way of justifying, I don't exactly see it as ego, but it's a kind

30
00:03:39,600 --> 00:03:46,800
of delusion for sure, you convince yourself that, yeah, yeah, it's just because I'm a

31
00:03:46,800 --> 00:03:51,360
meditator that I don't want to go, I think that's common actually meditator from meditator

32
00:03:51,360 --> 00:04:00,920
to make excuses for themselves, because they know that something is wrong, but they find

33
00:04:00,920 --> 00:04:05,120
a way to say, oh, well, it's just because of this, it's just because of that.

34
00:04:05,120 --> 00:04:10,680
It's not terrible, it's a defense mechanism, but of course, ideally you wouldn't be

35
00:04:10,680 --> 00:04:16,120
doing that, you'd just be aware of your state of dislike, and in a light in person would

36
00:04:16,120 --> 00:04:21,760
not dislike going to a wedding, they would be clear about their stance on drinking and

37
00:04:21,760 --> 00:04:30,360
have no interest in drinking alcohol, but I think in certain cases, in an Harahand as

38
00:04:30,360 --> 00:04:35,040
a whole other issue, but someone who is a Sotapana who would never drink alcohol might

39
00:04:35,040 --> 00:04:42,560
still very well go to a wedding, even beautifying themselves, just to fit in, unless they're

40
00:04:42,560 --> 00:04:49,200
among, of course, but if you're a lay person, you know, I think it would think beautifying

41
00:04:49,200 --> 00:04:56,960
in the sense of wearing a simple dress or a towel or a suit or something like that, some

42
00:04:56,960 --> 00:05:03,720
clothes that we're going to fit in and we're going to cause unnecessary distress, it doesn't

43
00:05:03,720 --> 00:05:10,560
seem like a problem, yeah, I mean, to that extent, as far as beautifying, I can see how

44
00:05:10,560 --> 00:05:17,040
there would be a problem to mend about putting on excessive makeup or makeup at all, really.

45
00:05:17,040 --> 00:05:23,080
Makeups are an interesting subject, a type people to ask about this, and a type people

46
00:05:23,080 --> 00:05:30,760
ask me about this, and I can see an argument for saying, hey, makeup is just wrong to

47
00:05:30,760 --> 00:05:35,800
wear it at all, for as a Buddhist, but of course, it's not against the five precepts,

48
00:05:35,800 --> 00:05:41,880
but if you think about it as the eight precepts, it's really not a good thing, but then

49
00:05:41,880 --> 00:05:46,960
there's the argument of fitting in, which I think applies here in the wedding, you might

50
00:05:46,960 --> 00:05:56,240
just as well try and blend in and not make anyone get angry at you, but I don't think

51
00:05:56,240 --> 00:06:03,480
it really has to do with wrong view or ego, as I said, though perhaps there's something

52
00:06:03,480 --> 00:06:10,120
I'm not quite clear on, either way, the most important is to be aware of moments, moments

53
00:06:10,120 --> 00:06:19,440
of consciousness, in this case, not wanting to do something, something's wrong with

54
00:06:19,440 --> 00:06:43,040
the interface, and I know I'm logged out to, where's our IT team, this isn't working?

55
00:06:49,440 --> 00:07:12,440
Computer's A. So I click on a button, and it starts to load, and it doesn't ever get

56
00:07:12,440 --> 00:07:18,200
anywhere, and then I refresh the page, and it's done what it was supposed to do, but only

57
00:07:18,200 --> 00:07:26,720
after I refresh the page, why is that?

58
00:07:26,720 --> 00:07:31,320
Is it possible to have extreme mood swings from meditating a lot, some days I feel extraordinarily

59
00:07:31,320 --> 00:07:36,600
blissful, everything is wonderful, and then the other days I feel sad and depressed, could

60
00:07:36,600 --> 00:07:39,280
it perhaps be a mental illness?

61
00:07:39,280 --> 00:07:46,360
Well, sadness and depression are by Buddhist definition, mental illnesses in and of themselves,

62
00:07:46,360 --> 00:07:51,200
any time you're sad, even a little bit, that's mental illness, the mind is not healthy

63
00:07:51,200 --> 00:08:00,160
in that moment, that mind is unhealthy for one way of describing it.

64
00:08:00,160 --> 00:08:06,040
On the other hand, feeling blissful can lead to mental illness as well of attachment to

65
00:08:06,040 --> 00:08:12,760
it liking it, I would say extreme mood swings are often, I mean, they can just be old

66
00:08:12,760 --> 00:08:20,360
past habits, but they're more likely to be caused by appreciation of the blissful states,

67
00:08:20,360 --> 00:08:26,960
the wonderful states, the states that you like, and when you like them a lot, the lack

68
00:08:26,960 --> 00:08:40,520
of them or the ordinary states become unbearable, and that leads to depression, sadness

69
00:08:40,520 --> 00:08:42,280
and so on.

70
00:08:42,280 --> 00:08:51,000
So try to let go of your attachment to both of them, to good states, bad states, if you want

71
00:08:51,000 --> 00:08:57,600
to feel experienced, true peace, true happiness, it can't be dependent on anything, because

72
00:08:57,600 --> 00:09:03,600
your feelings are impermanent, and what you're experiencing is impermanence, the fact that

73
00:09:03,600 --> 00:09:09,000
nothing lasts forever and once it's gone, all you're left with is your expectations, if

74
00:09:09,000 --> 00:09:14,000
you have expectations that it will stay and when it's gone, then you suffer.

75
00:09:40,000 --> 00:10:03,880
That's probably how many I should check this, something wrong with the token, I have no idea

76
00:10:03,880 --> 00:10:16,800
what that means.

77
00:10:16,800 --> 00:10:24,520
Firefox can't establish a connection to the server at wssmeditation.ceremungalow.org

78
00:10:24,520 --> 00:10:51,320
I'm going to answer these, but I'll stop pushing buttons, who needs buttons, right?

79
00:10:51,320 --> 00:10:55,360
There's a very advanced meditator see reality as if watching a movie frame by frame each

80
00:10:55,360 --> 00:11:00,040
frame being a jitter.

81
00:11:00,040 --> 00:11:09,360
It's not exactly every jitter, because it takes many jitter to form one experience, and

82
00:11:09,360 --> 00:11:15,760
so one would experience, would see experience as moment by moment, but to see each moment

83
00:11:15,760 --> 00:11:20,720
is not possible because the seeing aspect is only a part of the process.

84
00:11:20,720 --> 00:11:29,120
The experiencing, the awareness of the mind itself, or what's going on, the part where mindfulness

85
00:11:29,120 --> 00:11:34,280
comes into play is only part of the experience, so it's not each jitter, but it is possible

86
00:11:34,280 --> 00:11:43,200
to, it's more complicated than what you're suggesting, because the awareness is part of

87
00:11:43,200 --> 00:11:53,800
the experience, so awareness arises and mindfulness arises, but it's only ever a part of

88
00:11:53,800 --> 00:12:04,600
the process, but when certainly sees a rising and ceasing, these moments of experience,

89
00:12:04,600 --> 00:12:17,040
as it is of experience coming and going, okay, some brainstem surgery to remove a

90
00:12:17,040 --> 00:12:25,800
lesion, or medication, is it okay to start meditating after surgery on medication, yeah,

91
00:12:25,800 --> 00:12:33,160
not on medication, but I mean, it's okay to meditate, but don't expect, you probably,

92
00:12:33,160 --> 00:12:40,320
I would expect that benefits, depends on what kind of medication, if it's brain medication

93
00:12:40,320 --> 00:12:44,680
then you might have a problem, you might have a problem, I mean, of course, but it

94
00:12:44,680 --> 00:12:53,480
always meditate, no matter what situation, but yeah, it's just like, yes, it's fine, mindfulness

95
00:12:53,480 --> 00:13:01,600
is always good, sattin chukuang bikowai sambhatikamudam, mindfulness I tell you, monks is always

96
00:13:01,600 --> 00:13:07,760
beneficial.

97
00:13:07,760 --> 00:13:12,120
How do lay meditators deal with cutting down on sleep and feeling energetic the next

98
00:13:12,120 --> 00:13:13,120
day?

99
00:13:13,120 --> 00:13:22,720
Well, they would meditating many hours a day and be trained to have a clear mind, trained

100
00:13:22,720 --> 00:13:36,440
to be constantly in a state of equanimity, freedom from stress, because there's no reaction,

101
00:13:36,440 --> 00:13:41,440
so as a result, there's need for much less sleep.

102
00:13:41,440 --> 00:13:44,800
How should one read a write without losing mindfulness?

103
00:13:44,800 --> 00:13:49,560
Well, when you're reading your mind is engaged in a different process, now you can intermittently

104
00:13:49,560 --> 00:13:57,480
be mindful, but there's also other activities going on, like reflection and processing

105
00:13:57,480 --> 00:14:04,680
of the text, so it's not that you can be mindful no matter what you do, I would say,

106
00:14:04,680 --> 00:14:13,160
because reading is a different mind process, at the very least it's very difficult, once

107
00:14:13,160 --> 00:14:17,840
you're very trained, of course, you can mindfulness concern to be a part of it, processing

108
00:14:17,840 --> 00:14:22,560
your emotions using mindfulness, your reactions, your thoughts about it, being aware of

109
00:14:22,560 --> 00:14:30,560
how it affects you, just like anything, you'd be aware of what's going on and be mindful

110
00:14:30,560 --> 00:14:39,480
what's going on, and the trains of thought being mindful of them thinking, thinking,

111
00:14:39,480 --> 00:14:46,920
reflecting, isn't necessary to sleep mindfully, or can mindfulness prevent one from falling

112
00:14:46,920 --> 00:14:52,320
asleep, it can prevent you from falling asleep, it's possible that you stay up later

113
00:14:52,320 --> 00:14:58,960
because your mindful, because mindfulness can give you energy, it can at times, just focusing

114
00:14:58,960 --> 00:15:03,720
your attention, it can give you energy, but if you sleep mindfully, the thing with that

115
00:15:03,720 --> 00:15:10,040
is, your mind is already in the sort of a focused state, and so the sleep is much more

116
00:15:10,040 --> 00:15:19,880
peaceful generally, with practice, much more productive, because your mind is already set

117
00:15:19,880 --> 00:15:28,080
up, you've set the tone for the sleep, is it possible to meditate too much, if so how

118
00:15:28,080 --> 00:15:34,680
does one know if one is meditating too much, it's not possible to be too mindful,

119
00:15:34,680 --> 00:15:39,080
but you know if you're talking about just sitting and meditating, if you do a lot of

120
00:15:39,080 --> 00:15:50,200
it, it can become stressful, if you're not actually being mindful, as a practitioner, you're

121
00:15:50,200 --> 00:15:54,960
practicing, you're learning how to be mindful, and so a lot of the time you're probably

122
00:15:54,960 --> 00:16:02,000
not mindful, and that can get quite stressful as it builds up, so now I would say it's

123
00:16:02,000 --> 00:16:10,440
something you want to work up to, and over time, when you're in the meditation center

124
00:16:10,440 --> 00:16:17,000
or in a monastery, over time you'll meditate more and meditate until your mindful and

125
00:16:17,000 --> 00:16:26,400
practicing all the time, night and day, is it okay to observe and note the heart be

126
00:16:26,400 --> 00:16:32,840
dispart of the practice, yes, that's fine, you have a large gap between the falling and

127
00:16:32,840 --> 00:16:38,240
the next rising, well what we would normally tell people to do then is to say rising,

128
00:16:38,240 --> 00:16:46,760
falling, sitting, rising, falling, sitting, but noting the heartbeat is fine, it's not

129
00:16:46,760 --> 00:17:03,640
something that we would use as a basic meditation object, but note it when you feel it, and

130
00:17:03,640 --> 00:17:07,720
then whenever else arises in the gap you can also note it, but rising, falling, sitting

131
00:17:07,720 --> 00:17:13,800
is a common one, that's what we would teach people as part of our meditation course.

132
00:17:13,800 --> 00:17:19,480
Do you think that using mantras and vipasana takes some amount of attention away from the

133
00:17:19,480 --> 00:17:20,480
actual experience?

134
00:17:20,480 --> 00:17:23,480
Didn't we have this question recently?

135
00:17:23,480 --> 00:17:28,680
I mean it purposefully takes something away from the experience, it stops you from seeing

136
00:17:28,680 --> 00:17:34,160
the details, it stops you from getting caught up in the details, Buddha said, that's

137
00:17:34,160 --> 00:17:40,240
right I was in one of those, the Buddha said, na nimita gahi nana bhyanjana gahi

138
00:17:40,240 --> 00:17:49,360
one doesn't grasp at the particular signs or the particular, the experience, so seeing

139
00:17:49,360 --> 00:17:55,960
is just seeing, so I don't think it's fair to say it takes any attention away from the

140
00:17:55,960 --> 00:18:04,560
actual experience, it's meant to remind us what it is that we're actually experiencing,

141
00:18:04,560 --> 00:18:13,920
so that we don't get carried away by some extrapolation or some thought about it.

142
00:18:13,920 --> 00:18:19,920
Okay, but the last question in a session, no, it never happened, so these questions are

143
00:18:19,920 --> 00:18:29,800
all from that, we're doing something very strange happen with it in my computer, so it's

144
00:18:29,800 --> 00:18:37,320
not very strange that something should happen, this computer is always something happening.

145
00:18:37,320 --> 00:18:43,240
Should I distinguish between nama rupa and seeing, hearing, touching, tasting and smelling?

146
00:18:43,240 --> 00:18:50,080
You shouldn't actively consciously do that, you should just try and be mindful seeing,

147
00:18:50,080 --> 00:18:58,680
seeing, hearing, hearing, but you'll experience nama and rupa, there'll be awareness that

148
00:18:58,680 --> 00:19:07,480
you're aware, which is the nama and there'll be the awareness of the visual, which is rupa.

149
00:19:07,480 --> 00:19:14,840
That's not too important to discriminate or think about, let that happen by itself, just

150
00:19:14,840 --> 00:19:16,880
try and note.

151
00:19:16,880 --> 00:19:25,240
Okay, I'm not going to answer this one, meditate, how to understand that pleasant feeling

152
00:19:25,240 --> 00:19:30,600
is not good or positive, or similarly unpleasant feeling is not bad or negative.

153
00:19:30,600 --> 00:19:35,080
Well, the real question is, why do we think pleasant feelings are positive and why do

154
00:19:35,080 --> 00:19:38,720
we think unpleasant feelings are negative?

155
00:19:38,720 --> 00:19:42,480
There's no need to understand how they're not that, because they're not that.

156
00:19:42,480 --> 00:19:47,400
The fact that we think this sort of, this feeling is positive and this feeling is negative,

157
00:19:47,400 --> 00:19:48,400
is absurd.

158
00:19:48,400 --> 00:19:52,880
I mean, there's no rational reason for it.

159
00:19:52,880 --> 00:19:56,800
It's a message that we like at the side that we have a desire for one and a version

160
00:19:56,800 --> 00:19:59,600
to the other.

161
00:19:59,600 --> 00:20:04,440
So all we're doing in meditation is seeing that it's meaningless.

162
00:20:04,440 --> 00:20:12,760
And not only meaningless, but harmful, is partiality, sets us up for suffering.

163
00:20:12,760 --> 00:20:15,680
Sometimes I can see what lust is just a passing feeling.

164
00:20:15,680 --> 00:20:20,600
I can see how stupid I was to get attracted to Buddha beauty, but other times lust takes

165
00:20:20,600 --> 00:20:23,160
over me at that time, I'm not able to be mindful.

166
00:20:23,160 --> 00:20:26,360
It's my mind playing tricks with me, or is it something else?

167
00:20:26,360 --> 00:20:30,360
Yes, so the mind is complicated, and so you're seeing that there is no mind, and this

168
00:20:30,360 --> 00:20:36,040
is part of what is meant by non-self, is that there's no you who is this way or that

169
00:20:36,040 --> 00:20:37,040
way.

170
00:20:37,040 --> 00:20:44,800
There's only habits that are built up and change over time.

171
00:20:44,800 --> 00:20:48,840
And so through meditation, you're changing your habits, you're building a positive habits,

172
00:20:48,840 --> 00:20:57,080
beautiful habits, clear, conscious, objective habits that are going to lead you in the

173
00:20:57,080 --> 00:21:05,000
direction of letting go to the point where you can become free from suffering.

174
00:21:05,000 --> 00:21:10,000
Find it difficult to observe the rising and falling.

175
00:21:10,000 --> 00:21:16,280
Well difficulty is good in meditation, but if it's too subtle in the beginning, I mean

176
00:21:16,280 --> 00:21:21,960
it's just because your attention is not sharp enough.

177
00:21:21,960 --> 00:21:27,400
I mean subtle is not a problem, but not in non-existent would be a problem, but subtle

178
00:21:27,400 --> 00:21:32,040
is fine, you just know the subtle movement, it'll become more clear, it should become

179
00:21:32,040 --> 00:21:38,520
more clear as you press you carry on with the practice.

180
00:21:38,520 --> 00:21:43,280
The abdomen is not terribly important, it's just a good object.

181
00:21:43,280 --> 00:21:49,000
You can also just not sitting, sitting this on, not the feelings, not everything else

182
00:21:49,000 --> 00:21:50,400
as well.

183
00:21:50,400 --> 00:21:58,160
Okay, we have two questions from a meditator here who wanted to have these questions be

184
00:21:58,160 --> 00:22:01,680
public I think.

185
00:22:01,680 --> 00:22:08,080
Is it possible to make bad karma with good intentions but followed by a wrong choice?

186
00:22:08,080 --> 00:22:14,240
I think the point is that there is no wrong choice in the sense of the action.

187
00:22:14,240 --> 00:22:28,040
What is wrong is always the moment of intention, the state of mind.

188
00:22:28,040 --> 00:22:36,960
So an Arahan could still do something that results in or seemingly results in suffering,

189
00:22:36,960 --> 00:22:42,600
but they had no bad intention, that's where the karma is, karma is in your bad intention,

190
00:22:42,600 --> 00:22:53,600
so your bad states of mind that are one of three things that are associated with greed, anger,

191
00:22:53,600 --> 00:23:02,880
or delusion, or some mix of greed and anger, greed and delusion, or anger and delusion.

192
00:23:02,880 --> 00:23:10,000
They all have delusion in them, greed, anger, or delusion.

193
00:23:10,000 --> 00:23:20,240
Yeah, karma is not the action, that's why the word karma is actually probably not the

194
00:23:20,240 --> 00:23:26,120
best word, the Buddha just adapted it, but he said karma is your mental emotions, it's

195
00:23:26,120 --> 00:23:40,400
much like Jesus said, it's not whether you commit adultery, it's whether you have desire

196
00:23:40,400 --> 00:23:47,320
in the mind or that sort of thing, I said it's the mind that is basically the same thing.

197
00:23:47,320 --> 00:23:51,080
I'm not saying that Jesus was like Buddha because I don't believe he was, but I think

198
00:23:51,080 --> 00:23:58,640
that's an interesting aspect of Christianity.

199
00:23:58,640 --> 00:24:01,240
Could you explain the importance of walking meditation?

200
00:24:01,240 --> 00:24:04,840
Why should it be as long as sitting?

201
00:24:04,840 --> 00:24:10,040
Why before sitting?

202
00:24:10,040 --> 00:24:16,880
And not after, often people just consider it like a time for relaxing after meditation.

203
00:24:16,880 --> 00:24:24,960
Okay, so the Buddha, this is actually in my booklet, I think, I hope, but the Buddha mentioned

204
00:24:24,960 --> 00:24:30,320
five or listed five benefits of walking meditation.

205
00:24:30,320 --> 00:24:37,760
The first is that it gives you strength to walk, which was important, everyone walked

206
00:24:37,760 --> 00:24:40,120
back then.

207
00:24:40,120 --> 00:24:44,920
The second is that it gives you patience to work.

208
00:24:44,920 --> 00:24:53,800
So it's useful in a practical sense, just for living your daily life, walking meditation

209
00:24:53,800 --> 00:24:59,160
is great because it's monotonous, it's a monotonous activity that builds endurance and

210
00:24:59,160 --> 00:25:02,160
builds patience and builds stamina.

211
00:25:02,160 --> 00:25:11,680
And so any other monotonous work that you have to do is improve because of it.

212
00:25:11,680 --> 00:25:16,880
The third is it allows you to digest food, sitting around all day is not good for your

213
00:25:16,880 --> 00:25:23,080
digestion, walking is, I guess, better for your digestion, makes sense.

214
00:25:23,080 --> 00:25:27,840
Number four, it leads to, it helps work out, work out many diseases.

215
00:25:27,840 --> 00:25:34,720
There are a lot of people who talk about meditators who talk about their health improving

216
00:25:34,720 --> 00:25:37,680
through walking meditation.

217
00:25:37,680 --> 00:25:42,720
But the fifth reason is probably more to the point, and the fifth reason is that the Buddha

218
00:25:42,720 --> 00:25:49,000
said that the concentration that comes, or the focus that comes from walking meditation,

219
00:25:49,000 --> 00:25:54,320
the lasts a long time, is long lasting, that's how he put it.

220
00:25:54,320 --> 00:26:01,880
So there's a sort of a strength and a lasting, a staying power to the concentration that

221
00:26:01,880 --> 00:26:08,280
is useful for when you go to do sitting meditation.

222
00:26:08,280 --> 00:26:15,320
So we do walking first because it's actually a preparation for the sitting meditation.

223
00:26:15,320 --> 00:26:18,760
You find there's quite a difference when you haven't done walking and when you have done

224
00:26:18,760 --> 00:26:21,600
walking and just go to do sitting.

225
00:26:21,600 --> 00:26:25,600
You've done the walking first, your mind is already in quite a good state and the sitting

226
00:26:25,600 --> 00:26:30,520
is more immediately beneficial.

227
00:26:30,520 --> 00:26:32,600
Why do we do the same amount of walking and sitting?

228
00:26:32,600 --> 00:26:35,280
I don't suppose it's exactly necessary.

229
00:26:35,280 --> 00:26:40,120
My teacher is quite a stickler for this.

230
00:26:40,120 --> 00:26:42,480
All the Buddha said is to do them in alternation.

231
00:26:42,480 --> 00:26:43,480
He said, do walking.

232
00:26:43,480 --> 00:26:47,640
I mean, the Buddha clearly said, do walking and sitting meditation.

233
00:26:47,640 --> 00:26:52,960
A lot of people don't pay attention to that, but the only time that I know of when he

234
00:26:52,960 --> 00:26:59,240
talked about when he gave a regimen, a daily regimen was clearly to do walking and

235
00:26:59,240 --> 00:27:10,120
sitting in alternation for 20 hours a day besides eating, I suppose.

236
00:27:10,120 --> 00:27:13,880
But there is no mention of one being as long as the other.

237
00:27:13,880 --> 00:27:21,360
The idea of making them as long as each other, I mean, I think it's reasonable because

238
00:27:21,360 --> 00:27:28,400
of their, because they're different and because it makes sure, it ensures that you're

239
00:27:28,400 --> 00:27:30,760
not partial to one or the other.

240
00:27:30,760 --> 00:27:34,040
You'll find that at times you're partial to walking at other times you're partial to

241
00:27:34,040 --> 00:27:39,520
sitting and if you was up to you to decide how much you were going to do, you would be

242
00:27:39,520 --> 00:27:47,800
very hard to break free from that partiality or that judgment, judging nature.

243
00:27:47,800 --> 00:27:56,160
So keeping them equal is sort of a force, it's artificial, which is good because it forces

244
00:27:56,160 --> 00:28:04,360
you not to follow your heart, not to follow your partialities, which is a very much part

245
00:28:04,360 --> 00:28:11,000
of the test, the practice of challenging yourself, forcing you to experience things that

246
00:28:11,000 --> 00:28:15,960
you don't like so that you can rather than change them, you can change your reaction to

247
00:28:15,960 --> 00:28:16,960
them.

248
00:28:16,960 --> 00:28:25,600
Okay, and that's all the questions for tonight.

249
00:28:25,600 --> 00:28:26,600
Thank you all for coming out.

250
00:28:26,600 --> 00:28:32,880
I really appreciate that everyone is so interested in meditation and Buddhism.

251
00:28:32,880 --> 00:28:33,880
I apologize.

252
00:28:33,880 --> 00:28:38,960
I do delete some of them in questions and people might feel that that's unfair, but some

253
00:28:38,960 --> 00:28:45,520
of the questions are sometimes questions are too technical and I'm not really keen to

254
00:28:45,520 --> 00:28:51,160
delve into the technical aspects because I don't find them as helpful.

255
00:28:51,160 --> 00:29:05,160
Sometimes they're questions I've answered, sometimes they're not terribly practical or

256
00:29:05,160 --> 00:29:13,400
applicable to the practice, okay, so that's all for tonight, thank you all for tuning

257
00:29:13,400 --> 00:29:14,400
in.

258
00:29:14,400 --> 00:29:29,400
Have a good night.

