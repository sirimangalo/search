1
00:00:00,000 --> 00:00:09,480
Is the mind different from the physical brain totally different but let's

2
00:00:09,480 --> 00:00:12,960
approach this a little bit differently. Let's be a little bit more specific here.

3
00:00:12,960 --> 00:00:20,040
What do we mean by the mind and what do we mean by the brain? Because if you

4
00:00:20,040 --> 00:00:28,400
relate back to what I was just saying neither one exists because neither one has

5
00:00:28,400 --> 00:00:35,400
anything to do with experience the mind is just a concept and the brain is just a

6
00:00:35,400 --> 00:00:41,080
concept. Reality is experience something that we have to it's very

7
00:00:41,080 --> 00:00:46,160
difficult it's not near impossible I would say because of our training in

8
00:00:46,160 --> 00:00:56,280
modern society where so brainwashed I mean it's it's it's not like there's

9
00:00:56,280 --> 00:01:00,040
anyone brainwashing us but we've been so indoctrinated this may be a good

10
00:01:00,040 --> 00:01:05,560
word we've we've caught ourselves up so much in this idea of three-dimensional

11
00:01:05,560 --> 00:01:15,160
space of external reality and so on and funny thing is to read in quantum

12
00:01:15,160 --> 00:01:20,000
physics how none of its really valid how our our belief in the existence of a

13
00:01:20,000 --> 00:01:28,200
three-dimensional universe is really quite invalid and and all we can we can

14
00:01:28,200 --> 00:01:37,440
say about reality is based on experiment based on on observations from

15
00:01:37,440 --> 00:01:42,840
Buddhist point of view based on experience so I think that should answer your

16
00:01:42,840 --> 00:01:48,840
question I don't think I really have to spell it out because then then you

17
00:01:48,840 --> 00:01:56,200
know what part of those two things is in reality if you see a brain you cut

18
00:01:56,200 --> 00:02:01,000
someone's head open and you see a brain that's seeing if you touch the brain

19
00:02:01,000 --> 00:02:10,200
that's feeling and so you can never experience you can never experience the

20
00:02:10,200 --> 00:02:15,280
brain that which means the brain is not in any sense the word that I in the

21
00:02:15,280 --> 00:02:23,680
sense of word that I would understand it I would understand it to be real now

22
00:02:23,680 --> 00:02:36,040
the deal with the mind maybe a little more difficult to understand but when we

23
00:02:36,040 --> 00:02:40,120
think of reality in terms of well you can't even say it like that you really

24
00:02:40,120 --> 00:02:44,680
have to practice meditation to understand this one because only through the

25
00:02:44,680 --> 00:02:48,640
practice of meditation will you be able to break up this continuity which

26
00:02:48,640 --> 00:02:54,240
creates the idea of a self of a soul of a mind when you when you be able to

27
00:02:54,240 --> 00:03:00,400
break it up you can see that actually one mind has often nothing to do with the

28
00:03:00,400 --> 00:03:04,360
next mind right when you lost your train of thought I lost my train of thought

29
00:03:04,360 --> 00:03:10,320
there this is the mind is just it's like getting steamrolled by another mind

30
00:03:10,320 --> 00:03:13,720
because suddenly the mind is thinking about something different and you've lost

31
00:03:13,720 --> 00:03:22,240
that old one if there were a mind you think there would be a way to or that

32
00:03:22,240 --> 00:03:25,200
that other thought would still be there so there this sort of thing wouldn't

33
00:03:25,200 --> 00:03:32,280
happen but when you practice meditation you will see the mind arising and

34
00:03:32,280 --> 00:03:35,560
see you see thinking about one thing suddenly thinking about another thing

35
00:03:35,560 --> 00:03:42,400
totally unrelated and and not continuing one from the other so you don't

36
00:03:42,400 --> 00:03:46,960
aware it arose from the the best you can guess is that it arose from nowhere

37
00:03:46,960 --> 00:03:52,920
you know where did this mind this thought come from it seems to have appeared

38
00:03:52,920 --> 00:03:58,160
out of nothing and that's really the most accurate description you can give is

39
00:03:58,160 --> 00:04:04,600
that these things arise based on nothing so the idea of a mind is a red

40
00:04:04,600 --> 00:04:10,720
herring it's it's it's of fallacy the truth is an experience which has a

41
00:04:10,720 --> 00:04:15,280
mental portion and as a physical portion not always a physical because it can be

42
00:04:15,280 --> 00:04:22,680
thoughts but it's it's a experience that incorporate something that could be

43
00:04:22,680 --> 00:04:27,560
called mental and incorporate something often incorporate something that

44
00:04:27,560 --> 00:04:31,600
could be called physical that's all all we can really say the idea of the brain

45
00:04:31,600 --> 00:04:44,000
and and and so on is is just just a concept

46
00:04:44,000 --> 00:04:59,440
nothing to add here

