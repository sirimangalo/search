1
00:00:00,000 --> 00:00:11,440
importance of metta. How important is metta meditation? It is not something I have done often, and I'm not even certain, I know how to do it correctly.

2
00:00:13,440 --> 00:00:19,360
If you want a really good discussion or explanation of all four of the Brahmavi Harras,

3
00:00:19,360 --> 00:00:28,160
I would recommend a book authored by Mahasi Sayada called Brahmavi Haradhamma. It is quite extensive and gives detailed instructions

4
00:00:28,160 --> 00:00:34,560
on how to develop loving kindness. There are two ways of understanding metta. You can use it as a

5
00:00:34,560 --> 00:00:42,400
support for your meditation, or you can use it to develop samatha, tranquility meditation, which can lead to the first three

6
00:00:42,400 --> 00:00:48,640
nanas. If you want to use metta to develop samatha, this should be done with a qualified teacher

7
00:00:48,640 --> 00:00:55,200
who can help lead you to the nanas. We often practice metta after we pass in a meditation

8
00:00:55,200 --> 00:01:00,640
as a means of extending the wholesome states of mind, cultivated during the pastana practice.

9
00:01:01,200 --> 00:01:06,800
You can also practice metta before we pass in a practice to clear up anger or aversion.

10
00:01:07,360 --> 00:01:12,480
You can also practice it during the pastana practice. When you are overwhelmed with anger,

11
00:01:12,480 --> 00:01:18,400
you can use metta to help set your mind straight. Metta helps with straightening out your mind because

12
00:01:18,400 --> 00:01:23,840
anger and hate are based on this understanding. Metta helps remind you that relating to people

13
00:01:23,840 --> 00:01:30,240
with aversion is wrong. Metta practice leads you to not just to give up the anger, but to give up

14
00:01:30,240 --> 00:01:36,800
the delusion that says, this person is bad. When you send metta, you develop the right view that

15
00:01:36,800 --> 00:01:42,480
it is proper for all beings to be happy. So you are not just developing love, but you are also

16
00:01:42,480 --> 00:01:48,720
developing right view. It is similar to how focusing on the various parts of the body does not just

17
00:01:48,720 --> 00:01:54,960
lead to giving up bodily lust, but it also leads to giving up the wrong view that this body is

18
00:01:54,960 --> 00:02:24,800
beautiful.

