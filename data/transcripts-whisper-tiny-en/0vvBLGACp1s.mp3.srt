1
00:00:00,000 --> 00:00:04,800
Okay, we're live.

2
00:00:04,800 --> 00:00:10,440
So, today we continue on with the audio 30-8ths in the sub-10.

3
00:00:10,440 --> 00:00:12,760
Where we left off last night.

4
00:00:12,760 --> 00:00:19,240
It's going to probably take about 4 sessions to finish this one.

5
00:00:19,240 --> 00:00:29,280
So, again, the format is that we will be reading the poly, chanting the poly, and then

6
00:00:29,280 --> 00:00:32,280
reading the English and discussing it.

7
00:00:32,280 --> 00:00:37,720
So, if you're just interested in your study part, you can skip ahead to that.

8
00:00:37,720 --> 00:00:39,720
And that's if you're listening.

9
00:00:39,720 --> 00:00:45,800
If you're watching this later on on YouTube, and if you're live here, you can ask questions

10
00:00:45,800 --> 00:00:47,120
on my YouTube channel.

11
00:00:47,120 --> 00:00:54,320
There should be a place to ask questions or comments or whatever.

12
00:00:54,320 --> 00:01:15,680
And otherwise, we'll just sit back for a couple of minutes, we will be starting with a couple

13
00:01:15,680 --> 00:01:24,160
minutes, we will be starting with the screen share.

14
00:01:24,160 --> 00:01:27,520
See if you can!

15
00:01:27,520 --> 00:01:41,080
Actually, at six minutes, people can join us.

16
00:01:41,080 --> 00:02:10,520
So we made it from the start, so today we're starting at the, what is at this quest for enlightenment?

17
00:02:10,520 --> 00:02:37,740
So euahatashabauatuhvai angles samas samudha samummatas sam

18
00:02:37,740 --> 00:02:42,020
He said to us, a manu, a tana,

19
00:02:42,020 --> 00:02:46,840
jatidamos, a manu, jatidamam ye,

20
00:02:46,840 --> 00:02:52,200
vapari sami, a tana, jadamos,

21
00:02:52,200 --> 00:02:58,040
a manu, jadamam ye, vapari sami,

22
00:02:58,040 --> 00:03:01,920
a tana, jadidamos, a manu,

23
00:03:01,920 --> 00:03:06,600
jadidamam ye, vapari sami,

24
00:03:06,600 --> 00:03:10,400
jadamamamamam Namo,

25
00:03:10,400 --> 00:03:14,600
janamamamamya, vapari sami,

26
00:03:14,600 --> 00:03:17,940
a tana, soka,

27
00:03:17,940 --> 00:03:20,800
moh saamanas ukadam manoa,

28
00:03:20,800 --> 00:03:23,080
vapari sami,

29
00:03:23,080 --> 00:03:24,920
jadamASana.

30
00:03:24,920 --> 00:03:27,900
jadamasamamamas Freedom moh saamas

31
00:03:27,900 --> 00:03:32,580
jadamma, jadammamya.

32
00:03:32,580 --> 00:03:43,580
Thus I may hand me away, tada was he king, nuko, hana, tada, nuko, hana, tada, mos, a mano, jada,

33
00:03:43,580 --> 00:03:56,060
tada, mani, rapadi, sami, a tada, nada, rada, mos, a mano, pei, yada, mos, a mano, marada,

34
00:03:56,060 --> 00:04:06,860
mos, a mano, so cada, mos, a mano, tada, nas, aunki, lee, sada, mos, a mano, aunki, sada, mos, aunki, lee,

35
00:04:06,860 --> 00:04:17,380
sada, mani, rapadi, sami, yana, nana, nana, nana, jada, jada, mos, a mano, jada, jada,

36
00:04:17,380 --> 00:04:33,460
zh, tada, wapadi, hotna, you got kmani, Śvana, precise, ejana, jada, mus, a mano, jada,

37
00:04:33,460 --> 00:04:25,680
m

38
00:04:25,680 --> 00:04:36,840
mv A, d, haWanki, tada, jada, m sucks, aunki, ladi, d,enos, kana, mss, alla, mwaRhadi,

39
00:04:36,840 --> 00:04:42,200
tada, urana, nuko, kana, lappa- political ay san, genius, deh ra Ji hots, dam,

40
00:04:42,200 --> 00:04:47,320
tada, mmetal, tsani, number five, jada, lappa, deh ra, ha doh apparatus,

41
00:04:47,320 --> 00:04:56,360
I'm a man of the adi dame, adi dame, the one we did, but I'm the dean, and the Taran yoga,

42
00:04:56,360 --> 00:05:04,960
came on the bottom, but he said, yeah, then I'm a man of the most, I'm a man, no, I'm a

43
00:05:04,960 --> 00:05:14,000
man of the adi dame, the one we did, but I'm a man of Taran yoga, came on the bottom,

44
00:05:14,000 --> 00:05:25,200
and I'm a man of the adi dame, the one we did, but I'm a man of the adi dame, the one we did,

45
00:05:25,200 --> 00:05:34,160
I'm a man of Taran yoga, came on the bottom, but I'm a man of the adi dame, I'm a man of the

46
00:06:04,160 --> 00:06:15,640
I don't want to get to what taught me no way as a kama kana, mata, p2, nana, zumuk dana,

47
00:06:15,640 --> 00:06:23,960
vu dantana, gesama, zumu, hale, zu kaya, zu kaya, zu kaya, zu kaya, zu kaya, nini,

48
00:06:23,960 --> 00:06:30,960
vata, nia, jada, ditva, agarasma, anagari, nampa, jing, sari,

49
00:06:30,960 --> 00:06:37,960
sari, sari, vampa, jitos, samma, no king, kuk nay, zu kaya, zu kaya, wisti,

50
00:06:37,960 --> 00:06:49,960
anotaramsanti, vara pamma, valis, samma, no yena, rara, kamma, hote, nupasangrami,

51
00:06:49,960 --> 00:07:03,960
kavasangamidwara, kala, mani, tara, vu, jun, icha, nahi, nahi, mahi, mahi, mahi,

52
00:07:03,960 --> 00:07:08,960
mahi, mahi, mahi, mahi, mahi, mahi, mahi, jahi, jahi, jahi, tahi,

53
00:07:08,960 --> 00:07:16,960
a one-wood, tahi, bhi, kahi, rara, ru, kala, mahi, tara, vu, jahi,

54
00:07:16,960 --> 00:07:27,960
vu, hara, da, yasma, da, giso, a yanda, mo, ya, vu, vu, riso, nahi, rasa, vu, sari,

55
00:07:27,960 --> 00:07:36,960
vam, za, ya, nambin, ya, sari, kavwa, hupasam, baju, tobi, a da, ya, ti,

56
00:07:36,960 --> 00:07:45,960
soko, hambi, kawi, nahi, let's see, wakib, mahi, wadam, mam, bati, a phonin,

57
00:07:45,960 --> 00:07:53,960
soko, hambi, kawi, da, wadakini, waho, tappa, tambi, nahi, nahi, nahi, nahi,

58
00:07:53,960 --> 00:08:08,960
vu, tappa, nahi, nahi, nahi, nahi, nahi, wadam, yi, jahi, nahi, mahi, jahi, nahi,

59
00:09:38,380 --> 00:09:44,020
Alarasaywakala masahatiri yayam

60
00:09:44,020 --> 00:09:53,180
Ma'yam bakiri yayam nakouarasaywakala masahatisatim

61
00:09:53,180 --> 00:09:56,300
Ma'yam bhatisatim

62
00:09:56,300 --> 00:10:01,020
nakouarasaywakala masahatisam

63
00:10:01,020 --> 00:10:13,020
nakouarasaywakala masahatipanya ma'yam bhatipanya

64
00:10:13,020 --> 00:10:19,340
yayam nuna ham yayam ma'lala rokahala moo saiyan

65
00:10:19,340 --> 00:10:42,620
Ma'yam moo saiyan

66
00:10:42,620 --> 00:10:46,260
Ma'yam moo saiyan

67
00:10:46,260 --> 00:10:53,260
Ma'yam moo saiyan

68
00:10:53,260 --> 00:10:58,260
Ma'yam nakouarasayatim

69
00:10:58,260 --> 00:11:05,260
Ma'yam moo saiyan

70
00:11:05,260 --> 00:11:10,260
Ma'yam moo saiyan

71
00:11:10,260 --> 00:11:17,260
saiyan

72
00:11:17,260 --> 00:11:23,260
Ma'yam moo saiyan

73
00:11:23,260 --> 00:11:25,260
saiyan

74
00:11:25,260 --> 00:11:29,260
Ma'yam moo saiyan

75
00:11:29,260 --> 00:11:37,260
Ma'yam moo saiyan

76
00:11:37,260 --> 00:11:44,260
saiyan

77
00:11:44,260 --> 00:11:52,260
Ma'yam moo saiyan

78
00:11:52,260 --> 00:11:58,260
Ma'yam moo saiyan

79
00:11:58,260 --> 00:12:05,260
Ma'yam moo saiyan

80
00:12:05,260 --> 00:12:12,260
Ma'yam moh saiyan

81
00:12:12,260 --> 00:12:19,260
Ma'yam moo saiyan

82
00:12:19,260 --> 00:12:26,260
Ma'yam moo saiyan

83
00:12:26,260 --> 00:12:33,260
Ma'yam moo saiyan

84
00:12:33,260 --> 00:12:42,580
and the mandana. See, yankwanda, mandana. See, tarma, hankwanda, mandana.

85
00:12:42,580 --> 00:12:50,820
Iti adi. So, hankwanda. It's so too hankwanda, it's so hankwanda, it's so hankwanda.

86
00:12:50,820 --> 00:12:56,020
Iti adani. I was so bovasanta, yankwarna.

87
00:12:56,020 --> 00:13:11,940
I was so bovasanta, and I was so bovasanta, and I was so bovasanta.

88
00:13:11,940 --> 00:13:24,460
I was so bovasanta, and it's so hankwanda, it's so hankwanda.

89
00:13:24,460 --> 00:13:27,660
It is so kangwanda.

90
00:13:27,660 --> 00:13:39,820
and I don't know what they're saying.

91
00:13:40,280 --> 00:13:51,680
But then people tell us they've got something, so we've got everything,

92
00:13:51,680 --> 00:14:21,680
Oh God God God God God God God God God Son God Son God God Son God God Son God God Son God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God God

93
00:14:21,680 --> 00:14:25,320
In its titles, I first saw him sing the maw throttle

94
00:14:25,240 --> 00:14:42,760
It was

95
00:14:42,780 --> 00:14:50,240
a small hour of fame

96
00:16:50,240 --> 00:16:56,960
He said that my hand, but he said that Nakorama say,

97
00:16:56,960 --> 00:17:02,400
well, who see we are young, but he we are young.

98
00:17:02,400 --> 00:17:10,640
Nakorama say, well, who sees a thing, my hand, but he said the Nakorama say,

99
00:17:10,640 --> 00:17:15,680
well, who sees a man in my hand, but he said,

100
00:17:15,680 --> 00:17:22,580
Nakorama say, well, who sees a man in my hand, but he said,

101
00:17:22,580 --> 00:17:24,280
yeah, no, no, no, no, no.

102
00:17:24,280 --> 00:17:26,420
No, no, no, no, no, no, no.

103
00:17:26,420 --> 00:17:26,860
Ah, no, no.

104
00:17:26,860 --> 00:17:28,900
Yes, yes, yes.

105
00:17:28,900 --> 00:17:30,000
Yes, yes, yes.

106
00:17:30,000 --> 00:17:32,960
Who put some banjary her on me?

107
00:17:32,960 --> 00:17:35,080
He probably is.

108
00:17:35,080 --> 00:17:37,040
Yes, yes, yes, no.

109
00:17:37,040 --> 00:17:38,340
Yes, yes.

110
00:17:38,340 --> 00:17:39,340
Yes, yes.

111
00:17:39,340 --> 00:17:40,000
Yes.

112
00:17:40,000 --> 00:17:40,760
Yes.

113
00:17:40,760 --> 00:17:41,560
Yes, yes, yes.

114
00:17:41,560 --> 00:17:42,560
Yes, yes.

115
00:17:42,560 --> 00:17:43,600
Yes.

116
00:17:43,600 --> 00:17:44,600
Yes, yes.

117
00:17:44,600 --> 00:17:45,600
Yes.

118
00:18:45,600 --> 00:19:12,600
Lava, no I was sobra,ned unto. A was so ye, m

119
00:20:39,000 --> 00:20:41,940
When he was a 9 YESA in yesterday!

120
00:20:42,020 --> 00:20:43,080
Tiny Stretch!

121
00:20:46,680 --> 00:21:02,300
He doesn't Mr.

122
00:21:02,300 --> 00:21:02,640
He don't prefer

123
00:21:04,640 --> 00:21:05,840
Nothing.

124
00:21:05,840 --> 00:21:12,840
Here we didn't need the light actually here.

125
00:21:12,840 --> 00:21:18,400
Okay, so now we're on to the English.

126
00:21:18,400 --> 00:21:22,280
This is, we're only going to get through the two teachers.

127
00:21:22,280 --> 00:21:28,520
So the buddy said they had two teachers, the first one was Alara, kind of put that and

128
00:21:28,520 --> 00:21:35,520
the other one was going to cut out of it.

129
00:21:35,520 --> 00:21:41,280
Because before my enlightenment while I was still only an unenlightened buddy

130
00:21:41,280 --> 00:21:46,440
sata, I too, being myself subject to birth, sat what was also subject to birth,

131
00:21:46,440 --> 00:21:50,520
being myself subject to aging, sickness, death sorrow and defilement, I

132
00:21:50,520 --> 00:21:54,520
sought while it was also subject to aging, sickness, death sorrow and defilement.

133
00:21:54,520 --> 00:22:01,180
Then I consider this, why being myself subject to birth, do I seek what is also

134
00:22:01,180 --> 00:22:03,720
subject to birth?

135
00:22:03,720 --> 00:22:08,320
Why being myself subject to aging, sickness, death sorrow and defilement, do I seek what

136
00:22:08,320 --> 00:22:12,240
is also subject to aging, sickness, death sorrow and defilement?

137
00:22:12,240 --> 00:22:17,320
Suppose that being myself subject, subject to birth, having understood the danger and

138
00:22:17,320 --> 00:22:21,400
what is subject to birth, I seek the unborn supreme security from bondage,

139
00:22:21,400 --> 00:22:26,440
bond, suppose that being myself subject to aging, sickness, death sorrow and

140
00:22:26,440 --> 00:22:30,480
defilement, having understood danger and what is subject to aging, sickness, death

141
00:22:30,480 --> 00:22:35,360
sorrow and defilement, I seek the un-aging, unalien, deathless, sorrowless and

142
00:22:35,360 --> 00:22:43,560
un-defile, supreme security from bondage, bond, all right, so here he, it's

143
00:22:43,560 --> 00:22:48,040
suggesting that there's not much new here, this is just recap of the material

144
00:22:48,040 --> 00:22:54,880
above that admitting that in the past he was caught up in seeking up some

145
00:22:54,880 --> 00:23:02,880
sorrow and then he decided that he would seek out the noble goal, so he would

146
00:23:02,880 --> 00:23:14,920
undertake the noble search, the area for his and then later while still young, a

147
00:23:14,920 --> 00:23:21,040
black-haired man endowed with a blessing of youth in the prime of life, though my

148
00:23:21,040 --> 00:23:25,240
mother and father wished otherwise and wept with tearful faces, I shaved off my

149
00:23:25,240 --> 00:23:29,480
hair and beard, put it on the yellow robe, I went forth from the home life and

150
00:23:29,480 --> 00:23:35,080
the homelessness. It's an interesting passage here just as a sight, that's

151
00:23:35,080 --> 00:23:39,680
I don't know because we normally have these stories of the bodies that

152
00:23:39,680 --> 00:23:45,240
are leaving, you know the stories, stories of the bodies that the leaving have

153
00:23:45,240 --> 00:23:48,840
him leaving in the middle of the night, right, sneaking off in the middle of the night,

154
00:23:48,840 --> 00:23:59,120
he checks in on his wife and Raoula and without waking them he leaves, but

155
00:23:59,120 --> 00:24:05,480
here we have kind of the idea that so there's the insinuation that this one

156
00:24:05,480 --> 00:24:11,480
doesn't jive with that story, that he left them while they were crying, but it

157
00:24:11,480 --> 00:24:20,160
may just be some sort of poetic, I think it's just a brief, poetic statement,

158
00:24:20,160 --> 00:24:29,560
but I think if you look at the pile, you just want to see here, I'm not the

159
00:24:29,560 --> 00:24:36,480
pitunan asumukan and dantankis, well they were, well they had tears and my

160
00:24:36,480 --> 00:24:47,400
mother and father had tears in their eyes and were weeping, so there's the idea

161
00:24:47,400 --> 00:24:49,680
that they were weeping while he was leaving, which wouldn't have been

162
00:24:49,680 --> 00:24:52,960
possible if he was sneaking away the night, but I don't think he could take

163
00:24:52,960 --> 00:24:59,040
anything about it, anyway, just an interesting. So yeah, the story goes that he

164
00:24:59,040 --> 00:25:03,760
thought about this birth old age sickness and death, right, and we have the

165
00:25:03,760 --> 00:25:11,280
classic story of how he saw these things when he left the palace and realized

166
00:25:11,280 --> 00:25:16,880
that birth old age sickness and death were all something that he was subject to

167
00:25:16,880 --> 00:25:22,840
himself, and so he figured that he should seek out the greater search, and this

168
00:25:22,840 --> 00:25:27,720
is I think a common theme that goes through people's minds and leads them to

169
00:25:27,720 --> 00:25:32,000
Buddhism, I know it's part of what led me to the spiritual life, there's this

170
00:25:32,000 --> 00:25:36,120
idea that we all have to die, there's this realization that death is, is

171
00:25:36,120 --> 00:25:41,440
everywhere, and there's no escaping at them to try to find somewhere, because

172
00:25:41,440 --> 00:25:46,720
it leaves a lot of questions, why do we die? What's going on here? How did I fall

173
00:25:46,720 --> 00:25:51,960
into this trap? Where am I? It's like waking up in a prison cell and not knowing

174
00:25:51,960 --> 00:25:59,520
where you are, or waking up in an insane asylum or something, not having to

175
00:25:59,520 --> 00:26:03,920
clue how you got there, having no memories, and just wondering how did I get

176
00:26:03,920 --> 00:26:10,200
myself in this mess, and realizing you have to find a way out, there's got to be

177
00:26:10,200 --> 00:26:18,840
something better. So he shaved off his hair and beard, actually he didn't shave

178
00:26:18,840 --> 00:26:24,160
it off according to the stories he used to sword to cut it off, and I'll maybe

179
00:26:24,160 --> 00:26:30,400
be the most the same thing if the sword sharp enough, I don't know, but on the

180
00:26:30,400 --> 00:26:37,880
yellow robe, which is apparently given to him by a brahma god, got the car on

181
00:26:37,880 --> 00:26:45,200
me, but I can't remember. The third just treated rules of someone. I can't remember,

182
00:26:45,200 --> 00:26:51,080
but fourth from home life in the homelessness, right? So the first thing he did,

183
00:26:51,080 --> 00:26:55,000
he didn't go to practice austerity is right away, first he had these two

184
00:26:55,000 --> 00:26:59,120
teachers, so let's figure out what these two teachers said. Having gone forth

185
00:26:59,120 --> 00:27:03,000
because in search of what is wholesome, seeking the supreme status of blind

186
00:27:03,000 --> 00:27:08,960
peace, I went to Alara Kalama and said to him friend Kalama, I want to lead the

187
00:27:08,960 --> 00:27:16,920
holy life in this dhamma in the end discipline. Alara Kalama replied, the

188
00:27:16,920 --> 00:27:20,960
venerable one may stay here. The dhamma is such that a wise man could soon

189
00:27:20,960 --> 00:27:25,160
enter upon and abide in it, realizing for himself to direct knowledge his

190
00:27:25,160 --> 00:27:30,280
own teacher's doctrine. I soon quickly learned that dhamma, as far as merely

191
00:27:30,280 --> 00:27:33,920
presiding in rehearsal of this teaching went, I could speak with knowledge

192
00:27:33,920 --> 00:27:39,520
and assurance, and I claimed I know and see, and there were others who did likewise.

193
00:27:39,520 --> 00:27:45,920
Meaning here with no one see I guess is that I know and I see the meaning of

194
00:27:45,920 --> 00:27:52,320
this teaching, so I understand this teaching, but it still is clear that there

195
00:27:52,320 --> 00:27:57,120
are two levels to teaching, right? So he's still on the mere lip writing, lip

196
00:27:57,120 --> 00:28:08,840
reciting in rehearsal, so he memorized it. So this is an important aspect of the

197
00:28:08,840 --> 00:28:15,120
practice is the study part, which people often want to skip over and just get

198
00:28:15,120 --> 00:28:17,840
right into practicing and this is where it gets you into trouble. If you just

199
00:28:17,840 --> 00:28:22,520
focus on the practical aspect, it's very easy to get lost and so the good

200
00:28:22,520 --> 00:28:26,520
amount of study of an even memorization of the meditation technique is

201
00:28:26,520 --> 00:28:32,560
invaluable. This is why monks will spend a lot of time studying the text, it's

202
00:28:32,560 --> 00:28:38,600
why we study these texts, memorizing them, but sound knowledge of the text is

203
00:28:38,600 --> 00:28:45,200
very very useful, if not important. It's very important, I have a sound knowledge

204
00:28:45,200 --> 00:28:50,840
unless you're under a teacher who can just lean you along and guide you through

205
00:28:50,840 --> 00:28:55,760
it, but like memorizing the study, the four foundations of mindfulness should

206
00:28:55,760 --> 00:29:01,040
be a minimum, that kind of thing. So here he's memorizing this guy's doctrine,

207
00:29:01,040 --> 00:29:06,960
all are of all of those doctrine. I considered it is not through mere faith

208
00:29:06,960 --> 00:29:13,400
alone, all are calama declares, by realizing for myself with direct knowledge, I

209
00:29:13,400 --> 00:29:20,720
enter a pipe, enter upon and abide in this dhamma. Certainly a lot of calama

210
00:29:20,720 --> 00:29:26,040
abides, knowing and seeing this dhamma. Then I went to halara, calama, and asked

211
00:29:26,040 --> 00:29:30,360
him, bring calama, and what way do you declare that by realizing for yourself

212
00:29:30,360 --> 00:29:35,120
which direct knowledge you enter upon and abide in this dhamma, and reply to

213
00:29:35,120 --> 00:29:46,640
declare the base of nothingness? So I think the question here, the meaning of the

214
00:29:46,640 --> 00:29:51,840
question is what do you mean to say? What does it mean to say that you have

215
00:29:51,840 --> 00:29:55,880
realized something basically? What does it mean to say that you by realizing for

216
00:29:55,880 --> 00:29:58,560
his own direct knowledge you enter and abide in this dhamma? What does that mean

217
00:29:58,560 --> 00:30:04,200
to say that you abide in some dhamma? And it says what I mean by that is I abide

218
00:30:04,200 --> 00:30:09,080
in the base of nothingness. So the base of nothingness is the seventh

219
00:30:09,080 --> 00:30:14,800
attainment, seventh of the worldly attainment. The first four being the four form

220
00:30:14,800 --> 00:30:22,480
janas when you're focusing on an object of form like a light or a concept of

221
00:30:22,480 --> 00:30:29,240
some sort and an element or whatever, some concept in your mind that you

222
00:30:29,240 --> 00:30:35,920
fix the mind on. And then you get into the base of infinite space and then

223
00:30:35,920 --> 00:30:41,560
infinite consciousness and then nothingness, which is the third immaterial

224
00:30:41,560 --> 00:30:47,840
because they've discarded the material object. But it's an exalted attainment,

225
00:30:47,840 --> 00:30:50,800
but the point that we're going to make here is in the end, he's going to say

226
00:30:50,800 --> 00:30:56,200
that's not the way to nibana. So it's exalted, but not the way. So he tried, but he

227
00:30:56,200 --> 00:31:01,320
tries it out first to see whether it's what he's looking for. Right? This was his

228
00:31:01,320 --> 00:31:03,360
teacher at the time.

229
00:31:03,360 --> 00:31:11,800
Yeah. Yes. He calls them out. So friend, well, friend is what they called, he's called

230
00:31:11,800 --> 00:31:16,280
each other. In the beginning, the monks called each other. I was just being

231
00:31:16,280 --> 00:31:20,640
his friend. He read their teachers. It's interesting that he called his

232
00:31:20,640 --> 00:31:27,600
teacher out. So I guess that's just how it was. I consider not only

233
00:31:27,600 --> 00:31:34,480
Arada, Alara, Kalama has faith, energy, mindfulness, concentration, and wisdom. I, too,

234
00:31:34,480 --> 00:31:39,080
have faith, energy, mindfulness, concentration, wisdom. Suppose I endeavor to

235
00:31:39,080 --> 00:31:44,120
realize the down with the Alara, Kalama, declares the enterprise, enters

236
00:31:44,120 --> 00:31:48,600
upon and abides in, realizing for himself, which direct knowledge.

237
00:31:48,600 --> 00:31:57,880
And that funny, you're asking how you were talking behind you, checking that, yeah, cool, cool.

238
00:31:57,880 --> 00:32:04,320
So these are the five faculties, in case you're wondering where he pulls these from.

239
00:32:04,320 --> 00:32:09,200
These are very common, the Buddha taught. Five faculties talk about balancing faith

240
00:32:09,200 --> 00:32:12,520
with wisdom, bouncing energy with concentration. You actually have to balance them

241
00:32:12,520 --> 00:32:21,080
all with them all, and mindfulness is the one that balances them. But anyway, he's

242
00:32:21,080 --> 00:32:26,160
saying, so, Arada has all of these, but I have these as well. So if he can reach them,

243
00:32:26,160 --> 00:32:32,280
we can reach this base of nothingness, I can do it.

244
00:32:32,280 --> 00:32:37,840
I soon quickly entered upon and abided in that dhamma, realizing for myself

245
00:32:37,840 --> 00:32:44,080
with direct knowledge. Then I went to Alara, Kalama, and asked him, bring Kalama, is it

246
00:32:44,080 --> 00:32:48,400
in this way that you declare that you enter upon and abide in this dhamma by

247
00:32:48,400 --> 00:32:54,000
realizing for yourself with direct knowledge? That is the way, friend. It is in

248
00:32:54,000 --> 00:32:57,760
this way, friend, that I also enter upon and abide in this dhamma by realizing

249
00:32:57,760 --> 00:33:02,360
for myself with direct knowledge. It is a gain for us, friend. It is a great gain

250
00:33:02,360 --> 00:33:06,760
for us that we have such an incredible one for our companion in the holy life. So

251
00:33:06,760 --> 00:33:12,620
the dhamma and I declare that I enter upon and abide in by realizing for myself

252
00:33:12,620 --> 00:33:18,120
with direct knowledge is the dhamma that you enter upon and abide in yourself,

253
00:33:18,120 --> 00:33:22,560
the dhamma that you enter upon and abide in by realizing for yourself with direct

254
00:33:22,560 --> 00:33:25,800
knowledge. And the dhamma that you enter upon the

255
00:33:25,800 --> 00:33:26,800
that you enter.

256
00:33:26,800 --> 00:33:30,240
And then by the way, I realize it's a huge subject to acknowledge.

257
00:33:30,240 --> 00:33:35,820
Is the dominant that I declare, I enter upon and abide by realizing for myself what direct

258
00:33:35,820 --> 00:33:38,040
knowledge smells so.

259
00:33:38,040 --> 00:33:43,240
So you know that the dominant and I, you know the number that I know and I know the

260
00:33:43,240 --> 00:33:47,520
dominant that you know as I am so are I am so are you.

261
00:33:47,520 --> 00:33:52,640
As you are so am I, come friends, let us now lead this community together.

262
00:33:52,640 --> 00:33:59,800
So the point, you see in Polly it sounds a lot better, but it's a standard phrase, why

263
00:33:59,800 --> 00:34:05,200
it's so awkward and hard to really get your mind around this.

264
00:34:05,200 --> 00:34:11,840
Is that this passage, enter, enter upon and abide in X by realizing for oneself with

265
00:34:11,840 --> 00:34:12,840
direct knowledge.

266
00:34:12,840 --> 00:34:16,560
It's just a stock phrase.

267
00:34:16,560 --> 00:34:26,160
It actually should be having realized for oneself, have you realized the dhamma for oneself

268
00:34:26,160 --> 00:34:30,320
with direct knowledge, one enters and abides in it.

269
00:34:30,320 --> 00:34:33,720
That's how the Polly, the Polly phrases, but it's a stock phrase.

270
00:34:33,720 --> 00:34:39,280
So in order to understand this, you have to say, all he's asking is you claim to enter

271
00:34:39,280 --> 00:34:43,840
an abide in the dhamma by realizing it for yourself with direct knowledge.

272
00:34:43,840 --> 00:34:49,560
Is this the way, you know this way and he explains how he went into it and how he entered

273
00:34:49,560 --> 00:34:59,480
upon the sphere of nothingness or the base of nothingness.

274
00:34:59,480 --> 00:35:01,520
And Allah does a great thing.

275
00:35:01,520 --> 00:35:02,840
So does the other guy.

276
00:35:02,840 --> 00:35:11,520
Oh, daka, no, I don't know daka, they both say, well it's wonderful, it's great.

277
00:35:11,520 --> 00:35:18,680
I have no jealousy, no sense of feeling threatened.

278
00:35:18,680 --> 00:35:24,760
They welcome him, and not only are they welcome him, but they ask him to lead the community

279
00:35:24,760 --> 00:35:32,680
together and totally make him an equal to them, which is heartening, because in many

280
00:35:32,680 --> 00:35:41,240
cases, the leader might feel threatened, they want to be little and actually fear that

281
00:35:41,240 --> 00:35:49,640
someone who might do this, but the Buddha makes clear that it's a lot harder.

282
00:35:49,640 --> 00:35:55,920
Thus Allah, my teacher, placed me his pupil on an equal footing with himself and awarded

283
00:35:55,920 --> 00:36:01,320
me the highest honor, but it occurred to me this dhamma does not lead to disenchantment,

284
00:36:01,320 --> 00:36:06,800
to dispassion, to cessation, to peace, to direct knowledge, to enlightenment, to nibana,

285
00:36:06,800 --> 00:36:12,160
only to reappearance in the base of nothingness, not being satisfied with that dhamma, disappointed

286
00:36:12,160 --> 00:36:13,560
with it, I left.

287
00:36:13,560 --> 00:36:20,720
There's a famous teacher and apparently more than one famous teacher that is going around,

288
00:36:20,720 --> 00:36:31,280
they're going around trying to say that this, what Allah and Uddhaka taught, has nothing

289
00:36:31,280 --> 00:36:40,360
to do with the jhanas, but the commentary is quite clear that it is, right, as Bhikkhu

290
00:36:40,360 --> 00:36:47,240
Bodhi explains, it's the seven attainments, so it includes, in order to get to the base

291
00:36:47,240 --> 00:36:50,120
of nothingness, you have to enter into the jhanas.

292
00:36:50,120 --> 00:36:57,600
So what he's saying here is that this, the jhanas and all of that are not enough.

293
00:36:57,600 --> 00:37:05,240
If you go in this direction, especially the Arupa jhanas which only lead to reappearance

294
00:37:05,240 --> 00:37:15,280
and as a brahma, that he'd want to be born as a god, they take one away from the realm

295
00:37:15,280 --> 00:37:22,560
of understanding the condition, and understanding the experiential world, because they

296
00:37:22,560 --> 00:37:29,400
fix one in a trance of contemplation of nothingness, where the mind is fixed on the idea

297
00:37:29,400 --> 00:37:34,840
of there being nothing, not the kinti.

298
00:37:34,840 --> 00:37:40,240
And it doesn't lead to dispassion, because one can't understand change, one can't see,

299
00:37:40,240 --> 00:37:44,040
one's unable to see the impermanence of this, because it's so long and it's so stable,

300
00:37:44,040 --> 00:37:48,520
so it feels stable, it feels satisfying, and it feels controllable.

301
00:37:48,520 --> 00:37:50,840
There's nothing uncontrollable about it.

302
00:37:50,840 --> 00:37:58,360
It's very stable, very certain, very reliable, only because we're not able to see over

303
00:37:58,360 --> 00:38:02,920
the long term, we're not able to see that in the future it's going, it could follow

304
00:38:02,920 --> 00:38:08,080
up from underneath us at any time, which of course it could, and will eventually.

305
00:38:08,080 --> 00:38:17,240
So some of the meditation for this reason is highly deceptive, if one isn't approaching

306
00:38:17,240 --> 00:38:22,040
it in the right way, and doesn't use it as a basis for insight, and doesn't actually examine

307
00:38:22,040 --> 00:38:28,560
the experience of tranquility, as a basis of insight.

308
00:38:28,560 --> 00:38:33,160
Again and again, I get meditators who come and have a very difficult time pulling themselves

309
00:38:33,160 --> 00:38:38,040
away from these peaceful states, until finally they realize that there's,

310
00:38:38,040 --> 00:38:42,680
so much underneath the states that they're just covering up, and that these states

311
00:38:42,680 --> 00:38:48,320
can never actually change that, can never actually lead to disenchantment dispatch.

312
00:38:48,320 --> 00:38:54,280
One has to actually go underneath or examine these and to see, to break through them,

313
00:38:54,280 --> 00:39:01,640
to break through the calm and see reality as it is.

314
00:39:01,640 --> 00:39:06,520
The calm is useful as a base and as something that you can go back to, but it's not

315
00:39:06,520 --> 00:39:10,600
enough to lead one to an environment.

316
00:39:10,600 --> 00:39:12,520
So not being satisfied with that, he left.

317
00:39:12,520 --> 00:39:16,680
Doesn't mean that genres are bad, it doesn't mean these, these genres are bad, but

318
00:39:16,680 --> 00:39:21,880
it wasn't enough, so he had to go and forge his own way.

319
00:39:21,880 --> 00:39:25,760
Still in search because of what is wholesome, seeking the supreme status of blind

320
00:39:25,760 --> 00:39:30,920
peace, went to udakha, rama, puta, and said to him, friend, I want to lead the holy

321
00:39:30,920 --> 00:39:35,280
life in this dharma and discipline, you know, he says exactly the same things I don't

322
00:39:35,280 --> 00:39:41,420
think we have to read this all, but one thing we should point out is that udakha, rama,

323
00:39:41,420 --> 00:39:50,000
puta, wasn't teacher, so he memorizes it, but here, he's saying rama declared, you see.

324
00:39:50,000 --> 00:39:58,960
So udakha hasn't actually realized this, udakha is not rama, udakha was the son of rama,

325
00:39:58,960 --> 00:40:07,600
rama had passed away, who dakha is it? Yeah, but it seems that udakha hadn't realized

326
00:40:07,600 --> 00:40:12,920
that, so he's, so it's a little bit different, he's not actually talking around udakha

327
00:40:12,920 --> 00:40:20,360
and as a result, what does udakha do? He doesn't make him an equal, he says, so you know

328
00:40:20,360 --> 00:40:23,480
the dama that rama, rama, rama, rama, rama, rama, rama, rama, rama, rama, rama, rama, rama,

329
00:40:23,480 --> 00:40:27,600
come now friend, lead the community, so he doesn't say lead it with me, he says lead it

330
00:40:27,600 --> 00:40:31,520
and I'll be your student, does udakha around, puta my companion, the whole life placed

331
00:40:31,520 --> 00:40:35,760
me in the position of a teacher, recorded me the highest on him, so actually letting his

332
00:40:35,760 --> 00:40:42,560
student become the teacher, which is highly, well, a facing, to say the least, that he is

333
00:40:42,560 --> 00:40:47,440
incredibly humble, and willing to let his student become the teacher once his students

334
00:40:47,440 --> 00:40:52,880
are past him, which is an example for us all. And as he did the same thing before, too,

335
00:40:52,880 --> 00:41:02,240
we were the Bodhisatta, said, called him a friend, and then up there he, the udakha, called

336
00:41:02,240 --> 00:41:08,560
the Bodhisatta, venerable one, when he first met him, so maybe he's just, what is a call

337
00:41:08,560 --> 00:41:16,520
when you make a rule that applies to the past? Yeah, maybe he's just updating, updating

338
00:41:16,520 --> 00:41:31,400
the quotes, right. Anyway, that's our study for today. Peace by peace, we've gotten

339
00:41:31,400 --> 00:41:35,640
through two quarters of it, half of it, thank you all for tuning in, if any of you actually

340
00:41:35,640 --> 00:41:40,760
did tune in, and if you're watching this on YouTube, later I hope that some of you's

341
00:41:40,760 --> 00:41:47,760
doing a post, beautiful honor, thank you, thank you, and good night.

