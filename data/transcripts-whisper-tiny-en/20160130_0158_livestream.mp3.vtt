WEBVTT

00:00.000 --> 00:28.000
Good evening, everyone, broadcasting live January 29th, 2016.

00:28.000 --> 00:49.000
Today's quote, today's quote is about fairly mundane things.

00:49.000 --> 00:57.000
If you're noticing a pattern, a lot of these quotes are Buddhism light.

00:57.000 --> 01:11.000
It's a bit worrisome because it's easy to get complacent if you think of Buddhism as a social philosophy to help people live well in the world.

01:11.000 --> 01:26.000
It's easy to become complacent thinking you can just be generous and do good deeds in the world, and that's enough.

01:26.000 --> 01:34.000
Not that this is not a Buddhist teaching, it's just an emphasis on worldly aspects of the teaching.

01:34.000 --> 01:45.000
It's not for our purposes, it's problematic because we're all here interested in the core teachings of the Buddha.

01:45.000 --> 01:55.000
It's hard to get to the core teachings if you keep talking about good teachings, but good basic teachings like charity.

01:55.000 --> 02:06.000
Living a good household life doesn't talk about how to address the issues.

02:06.000 --> 02:17.000
But one thing this quote does mention is to be modest about your wealth and to not get upset when it goes away.

02:17.000 --> 02:30.000
And so that at least hints at a mental development because it's easy to say don't get upset when you lose the things you love,

02:30.000 --> 02:33.000
but it's really impossible in practice.

02:33.000 --> 02:39.000
If you love something, you will be upset when it goes away. You can't stop that.

02:39.000 --> 02:43.000
When you don't get what you want, there's a yearning for it.

02:43.000 --> 03:00.000
When the yearning is unsatisfied, you're suffering.

03:00.000 --> 03:15.000
And so this begins to hint at the need for practice.

03:15.000 --> 03:21.000
To make your mind such that when you're touched by the vicissitudes of life that changes in life,

03:21.000 --> 03:34.000
the impermanence of life touched by good and evil touched by pleasant and unpleasant.

03:34.000 --> 03:48.000
When the mind doesn't waver, when the mind doesn't clean, doesn't push or pull,

03:48.000 --> 03:58.000
we have to find the practice that leads us to this.

03:58.000 --> 04:01.000
The theory is quite simple.

04:01.000 --> 04:05.000
That experience itself isn't capable of causing a suffering.

04:05.000 --> 04:09.000
It's our reactions to our experiences that cause us suffering.

04:09.000 --> 04:15.000
If we didn't react, if we saw things objective, we wouldn't cling to them.

04:15.000 --> 04:20.000
If we saw them objectively, we wouldn't suffer from them.

04:20.000 --> 04:27.000
Someone's yelling at you. It's only sound until you let it get to you.

04:27.000 --> 04:32.000
If you have great pain, it's only pain until you let it get to you.

04:32.000 --> 04:40.000
Hot and cold and hard and soft.

04:40.000 --> 04:48.000
Amazing what as a meditator you can bear and you can endure without suffering.

04:48.000 --> 04:58.000
When I first went to meditate, we had to sleep on the hardwood floor, which is the very thing that.

04:58.000 --> 05:03.000
I mean, it's not like I was in great hardship, but this was one thing that I remember.

05:03.000 --> 05:09.000
If you don't have much fat on you, sleeping like that is very painful.

05:09.000 --> 05:16.000
But you know, one night I slept under a park bench and it was really comfortable on the cement.

05:16.000 --> 05:25.000
In bitter cold as well.

05:25.000 --> 05:27.000
There was a story behind that.

05:27.000 --> 05:36.000
But I always tell this story about this park bench because there was so much.

05:36.000 --> 05:44.000
So I was really complicated situation and people were pushing and bullying.

05:44.000 --> 05:49.000
And I just walked out and went and stayed under a park bench.

05:49.000 --> 05:59.000
And it felt so great because there was no, nobody, no, I didn't have to.

05:59.000 --> 06:12.000
Concerned myself with politics or opinions and telling people that the point here is.

06:12.000 --> 06:15.000
It's all in the mind.

06:15.000 --> 06:21.000
Some people.

06:21.000 --> 06:30.000
But it said even in a comfortable bed, a comfortable chair in great misery, great suffering, great stress.

06:30.000 --> 06:38.000
Their minds are filled with lust or filled with anger, with the delusion.

06:38.000 --> 06:40.000
There's three bad things.

06:40.000 --> 06:44.000
There's three things the poison are right.

06:44.000 --> 06:52.000
Our minds are full of them.

06:52.000 --> 06:55.000
It doesn't matter what comforts we feel, comforts we get.

06:55.000 --> 07:02.000
We're still suffering.

07:02.000 --> 07:04.000
So our practice is quite simple.

07:04.000 --> 07:13.000
We just remind ourselves of what things are, keeps us objective.

07:13.000 --> 07:19.000
When you do it, you can see some people doubt this technique and that doubt gets in a way.

07:19.000 --> 07:22.000
They don't give it an honest try.

07:22.000 --> 07:33.000
As soon as you give it an honest try, you can see in that moment the clarity, the mind is clear, your mind, pure.

07:33.000 --> 07:36.000
It's amazing teaching people five-minute meditation.

07:36.000 --> 07:41.000
I bet a lot of them will forget about it, but you give them one moment.

07:41.000 --> 07:44.000
You give them that minute.

07:44.000 --> 07:47.000
Where are they?

07:47.000 --> 07:54.000
They come out of like a one-minute meditation where we do it at the end, just about one minute.

07:54.000 --> 07:58.000
And they rave about it, because they've seen it.

07:58.000 --> 08:00.000
They've seen that moment.

08:00.000 --> 08:04.000
It takes us one moment.

08:04.000 --> 08:10.000
So when you doubt about the practice, when you're not sure, you should scold yourself.

08:10.000 --> 08:13.000
Because if you've been practicing it all, you've seen the moment.

08:13.000 --> 08:15.000
You've seen that moment.

08:15.000 --> 08:17.000
You've had that one moment.

08:17.000 --> 08:20.000
So later when you doubt, is this really helping?

08:20.000 --> 08:23.000
You should call yourself foolish for thinking.

08:23.000 --> 08:27.000
If you call yourself out on it, absolutely.

08:27.000 --> 08:33.000
When they trick ourselves, is this really good?

08:33.000 --> 08:43.000
It's like a person, this happens, you know, when a person gets.

08:43.000 --> 08:52.000
When we get free from the illness, and then we become reckless again.

08:52.000 --> 08:58.000
We get a terrible illness, and we suffer terribly.

08:58.000 --> 09:04.000
So when we get free from it, we act as though it never happened.

09:04.000 --> 09:09.000
We know to admit our vulnerability to the illness.

09:09.000 --> 09:17.000
We don't work to try to change our, to train our mind.

09:17.000 --> 09:29.000
We're very good at forgetting.

09:29.000 --> 09:34.000
We should try to practice remembering.

09:34.000 --> 09:38.000
And so if you want to know real progress in the practice, think about the moments.

09:38.000 --> 09:46.000
In this moment, when I mindful, see the result.

09:46.000 --> 09:49.000
It takes us a moment.

09:49.000 --> 09:52.000
So that's a little bit for tonight.

09:52.000 --> 10:02.000
Tomorrow at 3 p.m. Eastern Time, mid-12 p.m., mid-day, second lifetime.

10:02.000 --> 10:05.000
I'll be giving it a talk.

10:05.000 --> 10:10.000
So in second life, at the Buddha Center, so you know what that is.

10:10.000 --> 10:12.000
See you there.

10:12.000 --> 10:14.000
Have a good night.

10:14.000 --> 10:17.000
Thank you.

