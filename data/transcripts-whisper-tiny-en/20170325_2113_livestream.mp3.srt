1
00:00:00,000 --> 00:00:02,520
And the things that we thought brought us happiness

2
00:00:02,520 --> 00:00:07,360
when we apply mindfulness in it's the only way

3
00:00:07,360 --> 00:00:11,120
where you can break through the veil of ignorance,

4
00:00:11,120 --> 00:00:14,560
to see things in a whole new way.

5
00:00:14,560 --> 00:00:17,560
It's quite eye-opening.

6
00:00:20,840 --> 00:00:23,360
So that's what the Buddha says in the first section.

7
00:00:23,360 --> 00:00:25,440
And then he starts talking about the various,

8
00:00:25,440 --> 00:00:29,280
he goes through each of the four satevatana in various ways,

9
00:00:29,280 --> 00:00:34,280
by which one can practice satevatana.

10
00:00:34,280 --> 00:00:37,360
At the end of most of the sections he says,

11
00:00:37,360 --> 00:00:41,040
I think all of the sections he says,

12
00:00:41,040 --> 00:00:43,720
for example, for the body he says,

13
00:00:43,720 --> 00:00:47,560
one sees ityajatangwa, kayekayana, pasi-vihanatis,

14
00:00:47,560 --> 00:00:50,160
one dwells in regards to the body,

15
00:00:50,160 --> 00:00:53,240
seeing it as body, seeing body.

16
00:00:53,240 --> 00:00:58,240
And then one sees the arising of body and the ceasing of body.

17
00:01:02,760 --> 00:01:05,200
One watches, meaning,

18
00:01:05,200 --> 00:01:08,120
one is really aware of the experience from beginning to end.

19
00:01:08,120 --> 00:01:11,440
I mean, this is really what makes this an experiential practice.

20
00:01:11,440 --> 00:01:13,600
It's not about knowing, okay, now I'm walking,

21
00:01:13,600 --> 00:01:16,240
it's about really experiencing the beginning

22
00:01:16,240 --> 00:01:18,720
of a movement in the end of a movement.

23
00:01:18,720 --> 00:01:23,720
That's the sumo daya-dhamma-nupasi or waya-dhamma-nupasi.

24
00:01:27,920 --> 00:01:29,960
And then at the end he says something that I went,

25
00:01:29,960 --> 00:01:32,520
I went really quite interested in this.

26
00:01:32,520 --> 00:01:37,520
It's atikayotiwapanasatipatupatitahoti.

27
00:01:37,520 --> 00:01:40,520
One establishes mindfulness just to the extent,

28
00:01:40,520 --> 00:01:44,280
atikayo, there is the body, or this is body.

29
00:01:44,280 --> 00:01:48,280
I mean, really it is what it is, it's really the point.

30
00:01:48,280 --> 00:01:52,720
It is body, but that thing is what it is.

31
00:01:52,720 --> 00:01:55,840
Body is body, walking is walking,

32
00:01:57,320 --> 00:01:58,160
and so on.

33
00:01:58,160 --> 00:02:00,440
In the sampaganya-pa-pa-pa, he says,

34
00:02:00,440 --> 00:02:02,640
kachantu-wa-kachami-tibajana.

35
00:02:02,640 --> 00:02:07,640
When going, one knows I am walking, I am walking, kachami.

36
00:02:09,040 --> 00:02:10,760
Polygrammar is different from English,

37
00:02:10,760 --> 00:02:12,560
so one word means I am walking.

38
00:02:12,560 --> 00:02:14,560
In English, we just say walking, walking.

39
00:02:15,640 --> 00:02:17,080
But in Poly, you say kachami,

40
00:02:17,080 --> 00:02:19,560
which also has the first person,

41
00:02:19,560 --> 00:02:24,560
pronoun, or first person subject embedded in it.

42
00:02:30,440 --> 00:02:35,600
And then Yavadeva-nyanamataya-pa-ti-sati-mataya,

43
00:02:35,600 --> 00:02:40,480
just for knowledge, just enough for knowledge,

44
00:02:40,480 --> 00:02:44,480
and pa-ti-sati, which means quite sure.

45
00:02:44,480 --> 00:02:47,920
But he used to like this word, but he said,

46
00:02:47,920 --> 00:02:51,360
he means just mindfulness, just being mindful.

47
00:02:52,640 --> 00:02:54,640
This is the whole seeing things as they are.

48
00:02:54,640 --> 00:02:56,520
It's not adding anything.

49
00:02:57,560 --> 00:03:00,280
So instead of saying, this is good, this is bad,

50
00:03:00,280 --> 00:03:02,800
this is me, this is mine.

51
00:03:02,800 --> 00:03:04,560
We say, this is this.

52
00:03:04,560 --> 00:03:07,400
And if you're aware of that, that moment is mindfulness.

53
00:03:07,400 --> 00:03:14,200
If you're aware of things as they are.

54
00:03:14,200 --> 00:03:17,000
Oh, I missed something from the last section.

55
00:03:17,000 --> 00:03:18,040
There's another important thing

56
00:03:18,040 --> 00:03:19,880
that I'm going to fit with what I'm going to say next.

57
00:03:19,880 --> 00:03:24,520
But the Buddha says, we need a low-gay, a bhijad-doma-na-sang.

58
00:03:24,520 --> 00:03:26,680
But it's just an important dimension.

59
00:03:26,680 --> 00:03:34,000
By practicing sati, we are overcoming the ideas

60
00:03:34,000 --> 00:03:36,760
to overcome judgment, really, liking

61
00:03:36,760 --> 00:03:40,160
and disliking, because when you don't have agendongi,

62
00:03:40,160 --> 00:03:41,840
he picks this apart.

63
00:03:41,840 --> 00:03:47,960
So this section, for that passage, it says,

64
00:03:47,960 --> 00:03:53,680
one puts aside, liking and disliking.

65
00:03:53,680 --> 00:03:56,360
Greed and anger, really, is what it's saying.

66
00:03:56,360 --> 00:04:01,200
And then the question is, what about delusion?

67
00:04:01,200 --> 00:04:03,640
Because there are three basis for unholciveness

68
00:04:03,640 --> 00:04:06,920
in their greed, anger, and delusion.

69
00:04:06,920 --> 00:04:08,480
Why isn't delusion here?

70
00:04:08,480 --> 00:04:34,520
Well, that's what I'm going to define.

