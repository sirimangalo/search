1
00:00:00,000 --> 00:00:11,080
And we will lead you and encourage you to lead them down the wrong path, and if and when

2
00:00:11,080 --> 00:00:15,920
you decide that you've got a moral compass and you start to get an idea what's right

3
00:00:15,920 --> 00:00:22,000
and what's wrong, what's good and what's bad, what's to your benefit and to your detriment.

4
00:00:22,000 --> 00:00:26,800
This is something that shakes up a lot of friendships, practicing meditation is sure to

5
00:00:26,800 --> 00:00:35,280
shake up old friendships as you start to realize that you weren't helping each other.

6
00:00:35,280 --> 00:00:39,160
You weren't bringing happiness, peace, freedom from suffering to each other, we are only

7
00:00:39,160 --> 00:00:53,640
entangling each other up with in conflict and evil for black or better word.

8
00:00:53,640 --> 00:00:59,120
So those are bad friends, now positively the Buddha offers some advice on how to find

9
00:00:59,120 --> 00:01:07,400
good friends and give a list of four good friends.

10
00:01:07,400 --> 00:01:16,560
First to someone who's helpful, and this is a great friend, someone who supports you.

11
00:01:16,560 --> 00:01:25,640
And when you don't notice something's wrong, they protect you.

12
00:01:25,640 --> 00:01:45,360
They protect you, they protect you, they protect your life, they protect your princess.

