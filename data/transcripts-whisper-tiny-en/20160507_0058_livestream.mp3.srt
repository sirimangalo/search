1
00:00:00,000 --> 00:00:25,240
Good evening everyone broadcasting live May 6th.

2
00:00:25,240 --> 00:00:39,320
And days quote, days quote is somewhat curious.

3
00:00:39,320 --> 00:00:47,200
And not quite, well we could body translation of course is preferred, but the word isn't

4
00:00:47,200 --> 00:00:59,320
quite false, it's a weepity, you know, weepity is failure, failings as big a body translation.

5
00:00:59,320 --> 00:01:20,840
So weepity and some, somebody, weepity and somebody, weepity means lacking some, somebody

6
00:01:20,840 --> 00:01:37,960
means attaining our failings and our attainments.

7
00:01:37,960 --> 00:01:47,680
So the Buddha says, and it's kind of, I wouldn't put too much on this because the context

8
00:01:47,680 --> 00:01:53,120
of the quote is that they would not just laugh at it and they would not, of course, with

9
00:01:53,120 --> 00:02:04,400
the Buddha's cousin who, well, he came to the Buddha and he said, he said, no, the interval

10
00:02:04,400 --> 00:02:06,560
sure you're getting old.

11
00:02:06,560 --> 00:02:15,000
So why don't you pass the, the sun gone to me, all the the sun gone and the Buddha said to

12
00:02:15,000 --> 00:02:20,960
him, I wouldn't even hand it over to Sariput and Mogulana, let alone some insignificant

13
00:02:20,960 --> 00:02:28,640
little person like you, or he said something really mean to him, actually kind of.

14
00:02:28,640 --> 00:02:33,240
Because David that is of control and so there's only one way it's going to end in the

15
00:02:33,240 --> 00:02:39,040
end the Buddha has to be firm, it's rare because he's not usually like this, but with David

16
00:02:39,040 --> 00:02:42,720
that they had to be firm at all times.

17
00:02:42,720 --> 00:02:52,800
And so David that got very upset and he tried to scheme up a way to, to take control of

18
00:02:52,800 --> 00:02:53,800
the sun gone.

19
00:02:53,800 --> 00:03:00,280
And so he came to the Buddha later and asked him to instate five rules for monks that

20
00:03:00,280 --> 00:03:08,480
they should always live in the forest, that they should only eat vegetarian food and

21
00:03:08,480 --> 00:03:13,920
things that actually were contentious and seemed reasonable, but the Buddha couldn't enforce

22
00:03:13,920 --> 00:03:19,720
and we stopped short of forcing monks even though he praised living in the forest and

23
00:03:19,720 --> 00:03:24,840
even obviously eating meat as problematic and there are types of meat that absolutely

24
00:03:24,840 --> 00:03:29,960
shouldn't be in the country.

25
00:03:29,960 --> 00:03:36,840
So he refused and they would use that as a grounds to start a schism.

26
00:03:36,840 --> 00:03:44,000
He got monks on his side saying, hey, the Buddha clearly doesn't know what he's doing, but

27
00:03:44,000 --> 00:03:49,240
the monks he got on his side were actually new monks, their monks who didn't know better.

28
00:03:49,240 --> 00:03:54,320
So they hadn't studied their mind, they hadn't really learned the intricacies of monastic

29
00:03:54,320 --> 00:04:04,320
life and so they thought his David that was the more serious than the Buddha, it seemed

30
00:04:04,320 --> 00:04:11,120
to be actually kind of lax and indulgent.

31
00:04:11,120 --> 00:04:17,120
And so they went with David Datta, but later the Buddha had Sariput and Mogulana go

32
00:04:17,120 --> 00:04:19,880
to see them and it's kind of funny how it turned out.

33
00:04:19,880 --> 00:04:26,040
This is half, this Suta is actually before that, but later on, Sariput and Mogulana went

34
00:04:26,040 --> 00:04:31,720
to see David Datta and David is at, oh look, here come the Buddha's too cheap disciples,

35
00:04:31,720 --> 00:04:38,120
they're joining us as well and Sariput and Mogulana didn't say anything and so David

36
00:04:38,120 --> 00:04:45,480
Datta said, Sariput and Mogulana you teach the monks now I'm tired and so he pretended

37
00:04:45,480 --> 00:04:49,320
to be like the Buddha where the Buddha would say you teach and the Buddha would lie down

38
00:04:49,320 --> 00:04:58,360
and listen mindfully but instead David Datta lay down and fell asleep and so Sariput and

39
00:04:58,360 --> 00:05:03,440
Mogulana taught the monks the truth and converted them all and they all went back to

40
00:05:03,440 --> 00:05:09,960
see the Buddha, they went back to be with the Buddha once they realized what was right

41
00:05:09,960 --> 00:05:16,240
and what was wrong and David Datta's sort of second in command kicked David Datta in

42
00:05:16,240 --> 00:05:22,840
the chest to wake him up and was very angry and said look, look at what's happened

43
00:05:22,840 --> 00:05:28,840
to you, I told you to be careful of these two and you didn't listen but he kicked him

44
00:05:28,840 --> 00:05:33,280
in the chest and it wounded him and that ended up in I think being fatal that was what

45
00:05:33,280 --> 00:05:43,520
killed him in the end, he got sick after that, cough, dumb blood but so this quote is

46
00:05:43,520 --> 00:05:50,360
somewhere during this whole fiasco when David Datta has just left and so the Buddha kind

47
00:05:50,360 --> 00:05:57,800
of to introduce the topic, he says it's appropriate, it's from time to time, gali in

48
00:05:57,800 --> 00:06:05,600
a column to talk about one's own failings, let's talk about for too much a way, keep

49
00:06:05,600 --> 00:06:19,920
that to consider, it is good that they be considered reflected upon and it's good to reflect

50
00:06:19,920 --> 00:06:28,880
upon the failings of others and you have to take this with a grain of salt, it's important

51
00:06:28,880 --> 00:06:34,040
not to get obsessed with the faults of others because the Buddha has said of course, not

52
00:06:34,040 --> 00:06:41,320
Braisang, we know Ma'alina, Braisang Gattagatam, don't worry about when you become obsessed

53
00:06:41,320 --> 00:06:46,200
with other people's faults but so here he's cautious, he says gali in a column from

54
00:06:46,200 --> 00:06:51,800
time to time, he's useful, because if you look at David Datta as an example and if you

55
00:06:51,800 --> 00:06:57,560
talk about him and think about him, that's not how I want to be, that's something that

56
00:06:57,560 --> 00:07:05,320
leads to stress and suffering and he says it's good from time to time to think of your

57
00:07:05,320 --> 00:07:11,160
own attainments and attainments of others but then he gets on to the meat of the sutta

58
00:07:11,160 --> 00:07:20,240
which is actually more interesting, he says, well he says David Datta is going to hell,

59
00:07:20,240 --> 00:07:35,040
that's what he says, wait where is it, there are these 8 by 8, let me read the English

60
00:07:35,040 --> 00:07:43,880
because he was overcome and obsessed by 8 bad conditions, David Datta is bound for the

61
00:07:43,880 --> 00:07:48,600
plane of misery, bound for hell and he would remain there for an eon, so the point

62
00:07:48,600 --> 00:07:53,720
is to introduce, he's now going to talk about David Datta's feelings, what David Datta

63
00:07:53,720 --> 00:08:01,440
did wrong and these are interesting because these 8 things are a list of things that

64
00:08:01,440 --> 00:08:11,200
we have to be not obsessed with, we cannot let overcome us, the first is gain, some people

65
00:08:11,200 --> 00:08:20,240
are obsessed with gain, wanting money, wanting possessions, wanting to get things and when

66
00:08:20,240 --> 00:08:29,720
you get obsessed with that it overpowers you to inflames the mind, overcome by loss when

67
00:08:29,720 --> 00:08:39,720
you don't get what you want, when you lose what you like, fame, number 3 is fame,

68
00:08:39,720 --> 00:08:46,920
when people are obsessed with being famous people who post YouTube videos, they can be

69
00:08:46,920 --> 00:08:56,160
obsessed with people's, how many subscribers they have or so on, we go on Facebook

70
00:08:56,160 --> 00:09:03,160
or obsessed by how many likes we get, kind of thing, by disrepute the opposite of fame,

71
00:09:03,160 --> 00:09:14,520
you know, obsessed when people have a bad image of you or when you become infamous or when

72
00:09:14,520 --> 00:09:24,480
no one knows who you are, by honor, I'm not sure what the context here of honor is, look

73
00:09:24,480 --> 00:09:43,300
at this, Sakara, right, yes, Sakara is similar to fame but it's, when people present

74
00:09:43,300 --> 00:09:54,960
you with things like respect and gifts and praise and honor, that's where the way it gets

75
00:09:54,960 --> 00:10:04,260
toward honor, Sakara means doing rightly, people are obsessed with other people, praising

76
00:10:04,260 --> 00:10:21,140
them and honoring them and rewarding them, that kind of thing, number 7 by evil desires,

77
00:10:21,140 --> 00:10:27,080
some obsessed by evil desires, they were that they had the most evil desires, they wanted

78
00:10:27,080 --> 00:10:31,400
to kill the Buddha, he wanted to become, he ended up trying to kill the Buddha, he so

79
00:10:31,400 --> 00:10:37,840
wanted to be head of the monks, head of the Sangha, so badly, that he did evil things,

80
00:10:37,840 --> 00:10:44,860
he was jealous of the Buddha, he was covetous of the Buddha's position and then kind of

81
00:10:44,860 --> 00:10:55,520
thing and number 8, bad friendship, so David that, David that was actually the bad friendship

82
00:10:55,520 --> 00:11:02,520
himself, he ended up making friends with King Bimissara's son, who ended up trying, ended

83
00:11:02,520 --> 00:11:19,840
up killing his father, Magatasatukim was actually able to kill his father, but association

84
00:11:19,840 --> 00:11:26,720
with bad friends means if you're on a bad path, it's not a good idea to find people

85
00:11:26,720 --> 00:11:32,600
who have your own faults, we say birds of a feather flock together, well that's not

86
00:11:32,600 --> 00:11:39,040
always wise, if you have faults then maybe it's better to associate with people who don't

87
00:11:39,040 --> 00:11:44,600
have those faults, they can teach you something, they can remind you, they can challenge

88
00:11:44,600 --> 00:11:55,800
you, but we tend the other way, we tend to flock towards birds with the same faults, we

89
00:11:55,800 --> 00:12:04,600
tend to flock towards, if we are angry sort of person, we tend to find solace in other

90
00:12:04,600 --> 00:12:09,440
people who are angry, we don't seem to have a place for around people who are not angry,

91
00:12:09,440 --> 00:12:14,320
we feel guilty all the time, we feel bad, so it's actually easier to be around angry people,

92
00:12:14,320 --> 00:12:18,240
which is of course the most dangerous thing because you're just going to become more of

93
00:12:18,240 --> 00:12:24,640
an angry person, greed, if you're a greedy person, shouldn't be around other addicts

94
00:12:24,640 --> 00:12:30,440
or other greedy people, if you're deluded, if you're full of arrogance and conceit and

95
00:12:30,440 --> 00:12:37,640
so on, you should be surrounded by humble people, you should go to seek out humble people

96
00:12:37,640 --> 00:12:46,280
that teach you humility, and then he says it is good for a beaker to overcome these

97
00:12:46,280 --> 00:12:57,680
ape things whenever they arise, overcome, see what the word he uses for overcome,

98
00:12:57,680 --> 00:13:07,960
abbaebu, abbaebu, and the commentary talks about abbaebu, this is an interesting word

99
00:13:07,960 --> 00:13:19,960
to, against it just means overcome, to be above, commentary says abbaebu, abbaebu,

100
00:13:19,960 --> 00:13:34,800
to subjugate, to conquer, to conquer these, conquer these things, these are our enemies,

101
00:13:34,800 --> 00:13:39,280
they would not, there wasn't the enemy, abbaebu wasn't his enemy, his enemy was the

102
00:13:39,280 --> 00:13:49,200
these eight things, this is obsession for these eight things I should say, because of course

103
00:13:49,200 --> 00:13:54,520
gain is not a problem, loss is not a problem, it's not these things that are the problem

104
00:13:54,520 --> 00:13:59,520
except for evil wishes, but the real problem is our obsession and this goes actually

105
00:13:59,520 --> 00:14:05,120
with even our emotions, if you're an angry person, that's not the biggest problem, the

106
00:14:05,120 --> 00:14:11,560
biggest problem is your obsession with it when you let it consume you, when you have

107
00:14:11,560 --> 00:14:17,040
the desire, that's not the biggest problem, the biggest problem is you don't, if you fail

108
00:14:17,040 --> 00:14:29,120
to address it, fail to rise above it, to overcome it, when you have drowsiness, when

109
00:14:29,120 --> 00:14:35,320
you have distraction, when you have doubt, you have doubt about the Buddha's teaching or

110
00:14:35,320 --> 00:14:41,680
the practice, the doubt isn't the biggest problem that you let it get to you, even these

111
00:14:41,680 --> 00:14:48,760
things, they're bad, but they really become bad when you follow them, when you chase them,

112
00:14:48,760 --> 00:14:56,200
when you make much of them, and so with these eight things it's really the obsession

113
00:14:56,200 --> 00:15:05,680
with them, that David Atta wasn't able to see them clearly, that's smart, no, and then

114
00:15:05,680 --> 00:15:14,280
he says, why, why should you not let them consume you? And he says, because there will

115
00:15:14,280 --> 00:15:27,560
be a, we got a burilah, which is a fever, burning, those taints, distressful and feverish

116
00:15:27,560 --> 00:15:33,720
that might arise in one who has not overcome gain and so on, do not occur in one who

117
00:15:33,720 --> 00:15:46,440
has overcome it, so your mind doesn't become inflamed, distress, stressed, fatigued, overwhelmed,

118
00:15:46,440 --> 00:16:01,320
the asoa, these outpourings, emotions and the defilement, those distress, they don't arise

119
00:16:01,320 --> 00:16:11,280
when you overcome them, and when you come to see them, they are and discard them as an

120
00:16:11,280 --> 00:16:19,160
unproductive. Whether I breathe in, you should train yourself, that's smart, you have

121
00:16:19,160 --> 00:16:27,280
difficulty. A1 seeking the bong that should be trained us, we will dwell having overcome

122
00:16:27,280 --> 00:16:36,040
abibu, yeah, abibu, yeah, we had Islam, we will dwell having overcome gain and loss

123
00:16:36,040 --> 00:16:51,840
and pain and infamate, honor and dishonor, and evil desires, and evil friends, papi, chattang,

124
00:16:51,840 --> 00:17:06,640
papi, metatam, evil friendship, so good list, good list, it's almost the 8th Loki, the last

125
00:17:06,640 --> 00:17:16,760
two would be happiness and suffering, dukan, suka, instead of papi, chattam, papi, papi,

126
00:17:16,760 --> 00:17:31,640
you know. Anyway, there's our dhamma for tonight, don't be like Davidata, don't let

127
00:17:31,640 --> 00:17:53,960
these things overcome, yeah. Any questions? You can stay for a second, you can go.

128
00:17:53,960 --> 00:18:09,200
Okay, so we've got a couple of questions first, I want to say here's, have you metatamatis

129
00:18:09,200 --> 00:18:24,600
people? Have I showed you? This is Michael, Michael's living here now, he's the steward

130
00:18:24,600 --> 00:18:32,200
here for a while, just finished his advanced course, so he's done both courses, so we'll

131
00:18:32,200 --> 00:18:37,360
have to do an interview with Michael to see what he thinks of the practice, see how it helped

132
00:18:37,360 --> 00:18:51,400
him, if it helped him, and Sunday we're going to miss a saga together, and then I have

133
00:18:51,400 --> 00:19:00,400
to go to New York, we're going to drive to New York, you will drive to New York, would

134
00:19:00,400 --> 00:19:05,880
you be able to drive to New York? How is it about crossing the border back and forth,

135
00:19:05,880 --> 00:19:12,600
there's no problem, is there? What did they say when you came in? Did they ask you or make

136
00:19:12,600 --> 00:19:16,600
a problem? They didn't make trouble for you? They couldn't sit, I'll tell them that,

137
00:19:16,600 --> 00:19:23,800
wait for an hour, an hour. I mean, you know, when I mentioned your day and even, and then

138
00:19:23,800 --> 00:19:29,480
I was staying in a lot of scary, you know, they have many questions about that. Like suspicious

139
00:19:29,480 --> 00:19:36,400
are just curious. I don't know if they should talk about it. You probably should give

140
00:19:36,400 --> 00:19:43,520
people letters in the future, letters of recommendation, letters of invitation. Oh, weird.

141
00:19:43,520 --> 00:19:49,680
Yeah, they did ask for that. Yeah, I think we've had this issue before, I just sent a letter

142
00:19:49,680 --> 00:19:55,320
of invitation to someone in the Ukraine wants to come and meditate, so hopefully that works

143
00:19:55,320 --> 00:20:01,520
well. So there's this monk I've been talking about, he wants to go visit Bukubodi, so he

144
00:20:01,520 --> 00:20:06,880
wants me to go with him, and he was going to drive, and I said, well, I've got to drive.

145
00:20:06,880 --> 00:20:18,800
We've got a Prius, right? It's a good mileage, so probably he'll pay for gas. But yeah,

146
00:20:18,800 --> 00:20:31,000
so that's a week and a half away, the 17th Sunday, we're going to Mississauga for waysack,

147
00:20:31,000 --> 00:20:41,880
waysack, okay. So questions Larry says, during sitting meditation, can more than one hindrance

148
00:20:41,880 --> 00:20:51,840
be recognized concurrently? Yeah, you won't have them both at once. You can't have a

149
00:20:51,840 --> 00:20:57,960
virgin end craving concurrently. It feels like that because over time you say, wow, I was both

150
00:20:57,960 --> 00:21:03,880
angry and greedy, but you can't have them both at the exact same moment. So concurrently

151
00:21:03,880 --> 00:21:08,480
in the broad sense, sure, but in the absolute sense, no, in the absolute sense there's

152
00:21:08,480 --> 00:21:21,600
one at a time, so whichever one is clear in that moment, just focus on it. Doesn't really

153
00:21:21,600 --> 00:21:27,600
matter, I wouldn't worry about which is whichever one is clear in whatever moment, focus

154
00:21:27,600 --> 00:21:33,640
on it. You don't have to catch them all. But again, you can't have all of them at once.

155
00:21:33,640 --> 00:21:39,240
Except for the fact that a virgin to something is kind of like desire for it not to be,

156
00:21:39,240 --> 00:21:44,280
right? So you can argue that it's actually in that same moment, it's the same thing. You're

157
00:21:44,280 --> 00:21:50,440
saying the same thing. You dislike something means you want it to be gone. So it's not

158
00:21:50,440 --> 00:22:01,000
quite craving or it is some people call that we bow at and desire for non-existence.

159
00:22:01,000 --> 00:22:08,620
But based on kara dukha, does that hold true only for unenlightened beings? No. No, it

160
00:22:08,620 --> 00:22:29,600
is not. They are dukha. Dukha is there. A characteristic means they are unsatisfying. It

161
00:22:29,600 --> 00:22:37,280
means they are not good. It's the intrinsic characteristic. It doesn't mean that they are painful

162
00:22:37,280 --> 00:22:45,080
or they when they arise cause one mental suffering or even physical suffering. That's

163
00:22:45,080 --> 00:22:56,560
not what dukha means here. Dukha means unsatisfying or unable to satisfy, unable to bring

164
00:22:56,560 --> 00:23:01,320
happiness and not happiness. Remember these three characteristics are in opposition of what

165
00:23:01,320 --> 00:23:08,280
we think things are. We think things are stable, we think things are satisfying or

166
00:23:08,280 --> 00:23:15,920
able to satisfy us or productive of true happiness. And we think things are ours or me

167
00:23:15,920 --> 00:23:28,080
or mine. We can control them. So this is just realizing that they're not that, that they're

168
00:23:28,080 --> 00:23:32,760
and not Nietzsche, that they're not suka, that they're not at that. That's what these three

169
00:23:32,760 --> 00:23:47,120
things mean. They're actually a Nietzsche's dukha on that. Can you go back to using the

170
00:23:47,120 --> 00:23:52,200
red room? Wow, there's someone who actually liked that red? No, I can't go back to using

171
00:23:52,200 --> 00:24:16,360
the red room. Just liking, just liking. Sadu means good. Sadu just means good. Sadu

172
00:24:16,360 --> 00:24:26,120
can, in Hinduism, sadu is a, or an India of sadu means a, it's a word also for a holy

173
00:24:26,120 --> 00:24:33,600
person, like a monk or a recliff. So they call us sadu's. They would call me a sadu.

174
00:24:33,600 --> 00:24:38,080
But it literally means, at least in Pali, I don't know in Sanskrit, but in Pali it's

175
00:24:38,080 --> 00:24:49,400
just used to mean good sadu. It's an exclamation. It means it is good. When something is good,

176
00:24:49,400 --> 00:25:00,640
you say sadu. That thing is sadu. And Pali would say, tongue sadu. It is good. Like that.

177
00:25:00,640 --> 00:25:09,760
But then like sadu, it was used in the sutur, right? Sadu bikho, a bikhu, Upanang, laabang, abibuya, abibuya.

178
00:25:09,760 --> 00:25:23,200
We hurry. It is good if, or it would be good if one were to dwell like anything. But

179
00:25:23,200 --> 00:25:29,280
isn't suffering only mental? No, there's two kinds of suffering. There's physical suffering

180
00:25:29,280 --> 00:25:33,640
in mental suffering. But that's, again, not what this means. I don't know. I think there's

181
00:25:33,640 --> 00:25:40,320
a lag, so I'm not sure if you, if that was after my explanation or before it. But, again,

182
00:25:40,320 --> 00:25:45,880
that's not what Dukha means here. Dukha means not happiness or not conducive to happiness.

183
00:25:45,880 --> 00:26:06,000
It's not productive of happiness. Where is Robyn? Robyn's here. I was doing video questions.

184
00:26:06,000 --> 00:26:12,680
I was forcing people to, trying to get people to come on the hangout. So, didn't have any,

185
00:26:12,680 --> 00:26:24,560
need for someone to read. I think there's a way at some time and then we just stopped.

186
00:26:24,560 --> 00:26:29,360
Everything changes. This is what you have to realize. Don't get attached to things as they

187
00:26:29,360 --> 00:26:41,440
are. I try to change the format around, shake things up. Anyway, yes, that's all for

188
00:26:41,440 --> 00:26:49,520
tonight. Thank you all for tuning in. Tomorrow, oh, another thing is tomorrow at the university.

189
00:26:49,520 --> 00:26:56,760
There's, it's called May at Mac. May at Mac, it's the yearly McMaster Open House. And

190
00:26:56,760 --> 00:27:08,160
the Peace Studies Department has a booth and they've asked some of us to go and talk to

191
00:27:08,160 --> 00:27:15,280
new students about the Peace Studies Department. I'll take it as an opportunity to get

192
00:27:15,280 --> 00:27:21,720
people to sign up for the Peace Club as well. But, I'll be interesting. I'll go as a Buddhist

193
00:27:21,720 --> 00:27:28,280
monk to go and promote the Peace Studies Department. I think that'll be good. I do believe

194
00:27:28,280 --> 00:27:34,400
in it as, I'm not sure how, obviously I don't. Studies is not my, these kind of studies

195
00:27:34,400 --> 00:27:39,360
is not my priority, but I want to encourage them and support them. I mean, anyone who's

196
00:27:39,360 --> 00:27:47,680
talking about Peace Studies in Peace is just all good in my book. So I'm happy to promote

197
00:27:47,680 --> 00:28:05,040
it. Anyway, have a good night, everyone. Okay, you can stay up here for questions if you

198
00:28:05,040 --> 00:28:14,040
want. You can also leave after. Okay. And we can do a, from time to time we can do a dual

199
00:28:14,040 --> 00:28:20,880
monk radio if you want to come on. Like you can sit here with, you can have a mic in

200
00:28:20,880 --> 00:28:25,560
the center or something and then you just have four, you see the ones with four of us?

201
00:28:25,560 --> 00:28:51,920
That was a lot of fun. We had a whole table of us in Sri Lanka for a while.

