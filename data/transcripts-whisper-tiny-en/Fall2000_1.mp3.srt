1
00:00:00,000 --> 00:00:19,440
Today, I will explain to you the four foundations of mindfulness in brief.

2
00:00:19,440 --> 00:00:28,160
You have been practicing foundations of mindfulness for a long time, and so you need to

3
00:00:28,160 --> 00:00:35,360
know the four foundations. In order for you to remember the four foundations, I have

4
00:00:35,360 --> 00:00:46,640
you recite this passage every morning after taking that precepts. I have explained this

5
00:00:46,640 --> 00:01:02,080
passage here many times, and so for many people it may be what they already know. But

6
00:01:02,080 --> 00:01:13,520
there are new people also, and so it will be good to give the explanation of this first passage

7
00:01:13,520 --> 00:01:29,200
from the discourse called the great discourse on the foundations of mindfulness. This passage

8
00:01:29,200 --> 00:01:39,840
in the discourse explains the four foundations of mindfulness in brief. On the word,

9
00:01:39,840 --> 00:01:55,200
translated as foundation of mindfulness is Satipatana. And Satipatana is defined and explained

10
00:01:55,200 --> 00:02:07,280
in the commentary as the mindfulness which is firmly established. Mindfulness which goes to the

11
00:02:07,280 --> 00:02:23,280
object with force which rushes to the object and then covering the object and being well

12
00:02:23,280 --> 00:02:31,040
established. So that is what is called Satipatana or the foundations of mindfulness.

13
00:02:31,040 --> 00:02:42,000
So actually foundation of mindfulness means mindfulness which is founded which is firmly established.

14
00:02:45,680 --> 00:02:56,800
The characteristic of mindfulness is described as not wobbling. That means not floating on the

15
00:02:56,800 --> 00:03:09,920
surface. So the mindfulness must go deep into the object, not just float on the object.

16
00:03:11,440 --> 00:03:24,640
The commentaries explain that mindfulness must be not like a ball or something that floats on the

17
00:03:24,640 --> 00:03:34,080
object but it must go deep into the object. It must sink into the object as a rock sinks into the

18
00:03:34,080 --> 00:03:46,160
water. So when we practice mindfulness, we try to establish a family on the object of our choice

19
00:03:46,160 --> 00:03:56,320
and also on the objects that become prominent at the present moment.

20
00:04:02,080 --> 00:04:12,080
Bora began with the words, this is the only way because and so on. So in this words, there is a

21
00:04:12,080 --> 00:04:31,040
what Biku. Biku is translated as monk but Biku has more meaning than a monk. Now the what

22
00:04:31,040 --> 00:04:45,440
Biku normally means one who begs. So those who begs are called Biku's but the monks do not

23
00:04:45,440 --> 00:04:56,560
beg like the beggars do. So when the monks beg they just stand at the door saying nothing

24
00:04:56,560 --> 00:05:04,720
and then lay people will come out of the houses and offer something to them or sometimes they may

25
00:05:05,440 --> 00:05:14,400
be asked to pass on. So they did not say anything but they accept what is given to them and that

26
00:05:14,400 --> 00:05:29,360
is called the Arian begging or noble begging. There is another meaning of the what Biku and that is

27
00:05:30,160 --> 00:05:38,560
one who sees danger in the round of reburts. So according to that definition,

28
00:05:38,560 --> 00:05:49,760
not only monks but also lay people who see danger in the round of reburts can be called Biku's.

29
00:05:55,120 --> 00:06:04,640
That is why I use the what Biku's rather than monks here because if we use the what monk for the

30
00:06:04,640 --> 00:06:15,840
Pauli what Biku some people may think that this discourse is for monks only. But when we have the

31
00:06:15,840 --> 00:06:26,640
what Biku without translation then we can say that this suta or this discourse is not for monks

32
00:06:26,640 --> 00:06:43,440
only but for all those who see danger in the round of reburts. The reason why Biku's or monks are

33
00:06:43,440 --> 00:06:53,760
always addressed by the Buddha was that Buddha lived in a monastery and he lived with monks.

34
00:06:53,760 --> 00:07:02,240
So whenever he wanted to give a talk he addressed the monks naturally. There may be some lay people

35
00:07:02,240 --> 00:07:14,800
but the monks are the principal receivers of his teachings and so the Buddha always used what Biku

36
00:07:14,800 --> 00:07:24,640
or when he wanted to call them Biku away. And according to the commentary,

37
00:07:26,880 --> 00:07:35,920
many lay people in that area where this discourse was given and that is called the kingdom of

38
00:07:35,920 --> 00:07:47,280
Guru. It is somewhere near modern Delhi. So the commentary explains that many lay people also

39
00:07:47,280 --> 00:07:54,960
practice the four foundations of mindfulness. If they meet together and ask what foundation of

40
00:07:54,960 --> 00:08:04,880
mindfulness are you practicing and if a person answers no I practice nothing then he is scolded.

41
00:08:04,880 --> 00:08:13,680
So when somebody says that I practice the contemplation on the body or on the feelings then they

42
00:08:13,680 --> 00:08:25,680
would praise him. So according to that explanation many lay people also practiced the foundations

43
00:08:25,680 --> 00:08:45,040
of mindfulness. In this sentence Buddha said this is the only way. Now the Pali word for this

44
00:08:45,040 --> 00:09:00,320
is ikāya no. This word ikāya no has meanings other than the only way but I like this explanation

45
00:09:00,320 --> 00:09:13,520
and so I retain the only way. Now there are many people who do not want Buddha to say this is the

46
00:09:13,520 --> 00:09:21,200
only way. So according to them there must be other ways also to reach Nibana. But

47
00:09:25,120 --> 00:09:37,600
through practice we can prove that the translation the only way is a correct translation of the

48
00:09:37,600 --> 00:09:52,640
Pali word ikāya no and it is preferable to other translations because so long as you have

49
00:09:52,640 --> 00:10:03,760
mindfulness. So long as mindfulness is standing as a guard at the doors of the eyes, ears and so

50
00:10:03,760 --> 00:10:15,200
on. No undesirable unwholesome mental states can enter your mind. So long as there is mindfulness

51
00:10:15,760 --> 00:10:23,440
they cannot enter your mind and so mindfulness is the only way to prevent them from entering your

52
00:10:23,440 --> 00:10:33,360
mind and making your mind contaminated or in other words to purify your mind. So the translation

53
00:10:33,360 --> 00:10:48,560
the only way is I think preferable here. In the Dhamabada Buddha said more explicitly. So there

54
00:10:48,560 --> 00:11:00,960
he said this alone is the way and there is no other. So following that statement in the Dhamabada

55
00:11:00,960 --> 00:11:12,880
the translation of the word ikāya no as the only way is according to the wishes of the Buddha.

56
00:11:15,840 --> 00:11:25,520
If it is to purify the mind it must be mindfulness but mindfulness can be practiced in many ways

57
00:11:25,520 --> 00:11:37,600
even in the discourse on the foundations of mindfulness itself Buddha taught 21 different ways of

58
00:11:37,600 --> 00:11:46,240
practicing mindfulness. So mindfulness can take many forms but whatever form it takes it must be

59
00:11:46,240 --> 00:11:57,680
mindfulness and it must not be otherwise. That is why mindfulness is said to be the only way.

60
00:11:57,680 --> 00:12:17,120
At outset at the beginning of this discourse Buddha gave us five benefits that we can get from the

61
00:12:17,120 --> 00:12:30,400
practice of mindfulness. The first is the purification of beings. Now this is the only way for the

62
00:12:30,400 --> 00:12:41,840
purification of beings. That means when mindfulness is practiced it will lead to the purification of

63
00:12:41,840 --> 00:12:49,680
beings and purification of beings means purification of the mind of beings not the physical body.

64
00:12:50,320 --> 00:12:56,800
So for the purification of physical body you don't need to practice mindfulness meditation.

65
00:12:57,440 --> 00:13:05,760
So here purification means purification of mind. So in order to purify the mind of

66
00:13:05,760 --> 00:13:19,440
unwholesome mental states we practice mindfulness. So this is one benefit we can get from the

67
00:13:19,440 --> 00:13:31,040
practice of mindfulness. Mind is said to be purified when there are no unwholesome

68
00:13:31,040 --> 00:13:44,000
mental states in the mind. Now attachment, greed, hate, anger, delusion, pride, jealousy and so

69
00:13:44,000 --> 00:13:53,760
on they are all called unwholesome mental states. So they contaminate the mind. So long as they

70
00:13:53,760 --> 00:14:02,160
are in the mind the mind is said to be in pure but by the practice of mindfulness when one is

71
00:14:02,160 --> 00:14:10,800
able to to prevent them from arising in the mind or entering the mind then the mind is said to

72
00:14:10,800 --> 00:14:26,320
be pure. So for the purification of the mind we practice mindfulness. Now the second benefit

73
00:14:26,320 --> 00:14:35,360
Buddha's stage it was overcoming of sorrow and limitation. So if you want to overcome sorrow

74
00:14:35,360 --> 00:14:50,640
and limitation we must practice mindfulness. Here sorrow means just sorrow and limitation means

75
00:14:51,680 --> 00:15:01,600
crying aloud. So when people are sorry they may just be sorry or they may cry and they may see

76
00:15:01,600 --> 00:15:09,600
some things when they are crying and so that is called lamentation. Both sorrow and lamentation

77
00:15:09,600 --> 00:15:21,040
can be overcome by the practice of mindfulness. The mindfulness practice is to treat with mindfulness

78
00:15:21,040 --> 00:15:33,760
every object that arises in the mind. Now when there is sorrow we can practice mindfulness of that

79
00:15:33,760 --> 00:15:45,680
sorrow or we can take that sorrow as the object of our attention and we try to pay attention to it

80
00:15:45,680 --> 00:16:00,160
or to make mental notes of it and by that way sorrow will disappear. It is important that

81
00:16:01,040 --> 00:16:10,640
when you have sorrow and want to overcome it then you take sorrow itself as the object of your

82
00:16:10,640 --> 00:16:20,640
attention and not the cause of sorrow. Now it is important that we understand the cause of sorrow

83
00:16:21,280 --> 00:16:28,960
and sorrow itself. So long as we take the cause of sorrow as object sorrow will increase.

84
00:16:28,960 --> 00:16:40,000
But once you turn your mind to sorrow itself at that moment the cause of sorrow disappears from your mind

85
00:16:40,560 --> 00:16:50,000
and so when the cause disappears the result which is sorrow must also disappear. So this is the

86
00:16:50,000 --> 00:17:00,720
mindfulness method to deal with not only sorrow but other undesirable mental states.

87
00:17:00,720 --> 00:17:21,360
On the third benefit is the disappearance of pain and grief. Now pain here means physical pain

88
00:17:21,360 --> 00:17:34,720
and grief means mental pain. Now here by the practice of mindfulness physical pain may not disappear

89
00:17:37,120 --> 00:17:48,800
but by the practice of mindfulness you will understand pain more clearly and you will see that

90
00:17:48,800 --> 00:17:59,040
pain also is impermanent it comes and goes and so when you realize the impermanence of pain

91
00:17:59,680 --> 00:18:10,000
you are able to live with it you are able to accept it. So it will no longer give you much trouble.

92
00:18:10,000 --> 00:18:20,080
So although physical pain may not disappear if you practice mindfulness you will be able to live

93
00:18:20,080 --> 00:18:35,520
with pain and grief is mental pain mental pain means grief disappointment discouragement

94
00:18:35,520 --> 00:18:51,520
and fear and many other mental states. So grief can be overcome or grief can disappear

95
00:18:52,320 --> 00:19:00,560
by the practice of mindfulness. It is more or less the same as dealing with sorrow. So when there

96
00:19:00,560 --> 00:19:08,560
is grief when there is mental pain you try to be mindful of that pain it's mental pain itself and

97
00:19:09,920 --> 00:19:18,000
when your concentration becomes strong then you will be able to overcome it and it will disappear.

98
00:19:20,880 --> 00:19:27,440
So by the practice of mindfulness physical pain may or may not disappear

99
00:19:27,440 --> 00:19:33,520
but grief or mental pain will surely disappear

100
00:19:44,800 --> 00:19:54,160
and the next benefit is reaching the noble path. Now the word path should be understood correctly.

101
00:19:54,160 --> 00:20:02,080
Here it is written with a capital P.

102
00:20:06,080 --> 00:20:18,240
In Pali it is called Magha. Magha arises at the moment of enlightenment.

103
00:20:18,240 --> 00:20:28,560
Suppose a person is practicing Vipasana meditation and he will go through different stages of

104
00:20:28,560 --> 00:20:40,320
Vipasana meditation one by one and when his Vipasana has become mature then a time will come

105
00:20:40,320 --> 00:20:51,040
when a type of consciousness arises in his mind and that type of consciousness he has never

106
00:20:51,040 --> 00:21:01,680
experienced before and that type of consciousness can eradicate the mental defilements once and

107
00:21:01,680 --> 00:21:11,520
for all. So that consciousness is called path consciousness and to reach that consciousness

108
00:21:13,280 --> 00:21:20,320
or to have that consciousness arises in one's mind and one needs to practice mindfulness.

109
00:21:20,320 --> 00:21:35,040
So the practice of mindfulness will ultimately lead the practitioner to reaching the noble path.

110
00:21:38,800 --> 00:21:48,320
The last benefit is the realization of Nipana. So when path or Magha consciousness arises

111
00:21:48,320 --> 00:22:00,160
it takes Nipana as object. So Nipana is the extinction of mental defilements and the extinction

112
00:22:00,160 --> 00:22:13,200
of suffering. So path consciousness arises taking Nipana as object. So to take Nipana as object

113
00:22:13,200 --> 00:22:31,760
is called realization of Nipana. So Buddha pointed out the benefits that we will get if we practice

114
00:22:31,760 --> 00:22:37,840
mindfulness meditation. Now these five benefits are actually just one,

115
00:22:37,840 --> 00:22:48,480
purification of mine. So when mine is totally pure there can be no sorrow or limitation

116
00:22:49,200 --> 00:23:00,000
and there can be no grief and when mine is totally pure it reaches a stage of path consciousness

117
00:23:00,000 --> 00:23:08,160
and when path consciousness arises it takes Nipana as object and at that moment the mind is

118
00:23:08,160 --> 00:23:16,960
totally pure again. So the benefit we will get from the practice of foundations of mindfulness

119
00:23:16,960 --> 00:23:38,880
is just total purification of mind. And what is this only way Buddha said this is the four foundations

120
00:23:38,880 --> 00:23:45,760
of mindfulness. So the four foundations of mindfulness or the practice of mindfulness is the only way

121
00:23:45,760 --> 00:23:54,960
to achieve purification of mind and so on. And what are the four? Here in this teaching,

122
00:23:54,960 --> 00:24:03,520
because in this teaching means in the dispensation of the Buddha, a big good dwells contemplating

123
00:24:03,520 --> 00:24:12,720
the body and the body, ardent, clearly comprehending and mindful, removing covetousness and grief

124
00:24:12,720 --> 00:24:20,080
in the world. And this is the first foundation of mindfulness and the others are second, third and

125
00:24:20,080 --> 00:24:20,480
fourth.

126
00:24:33,920 --> 00:24:42,560
So the four foundations of mindfulness are described here by way of person or by way of

127
00:24:42,560 --> 00:24:50,320
a monk practicing it. So the first foundation of mindfulness is contemplating, contemplation,

128
00:24:50,960 --> 00:24:59,600
contemplating the body in the body. Now the word contemplating is the translation of the

129
00:24:59,600 --> 00:25:15,520
Paliwa, a new persona. A new persona means repeatedly seeing, repeatedly observing. So the English

130
00:25:15,520 --> 00:25:24,640
word contemplating may mean some other thing. But here, contemplating means watching it repeatedly

131
00:25:24,640 --> 00:25:30,720
seeing it repeatedly and contemplating the body in the body means contemplating the body

132
00:25:30,720 --> 00:25:37,600
in the body and not contemplating the feeling in the body and so on. So the word body is repeated

133
00:25:37,600 --> 00:25:44,000
here to show that the contemplation must be precise.

134
00:25:44,000 --> 00:25:53,360
The body in the body and not feeling in the body, consciousness in the body and so on.

135
00:25:56,560 --> 00:26:05,920
The word body here does not necessarily mean the physical body only or the whole physical body

136
00:26:05,920 --> 00:26:19,680
only. The body here means a group, a mess of different things. Now if you go to the details in the

137
00:26:19,680 --> 00:26:28,320
discourse, you will find that the breathing in and breathing out are called body and post chance of

138
00:26:28,320 --> 00:26:39,760
the body or call body and so on. So by the word body here, we must understand the group of

139
00:26:40,640 --> 00:26:49,680
the combination of different parts, not necessarily the whole physical body.

140
00:26:49,680 --> 00:27:04,160
So when you are mindful of the breath, you are practicing the contemplation of the body

141
00:27:04,160 --> 00:27:17,200
in the body. And when we practice Buddha taught that we must be ardent, clearly comprehending

142
00:27:17,200 --> 00:27:28,480
and mindful. So by these words, Buddha showed us the components of the practice. So when we

143
00:27:28,480 --> 00:27:35,680
practice mindfulness, we must have these components arise in our minds.

144
00:27:35,680 --> 00:27:58,960
So the first component is described by the word ardent. That means the bikhu or the yogi makes effort.

145
00:27:58,960 --> 00:28:09,840
So by ardent, Buddha meant that we must make effort to be mindful. Without effort, we cannot be mindful.

146
00:28:14,720 --> 00:28:23,280
And in the commentaries, it is explained that the thought has the ability

147
00:28:23,280 --> 00:28:37,280
to dry up or to heat up the mental defilements. So that means when there is effort, when we make effort,

148
00:28:37,920 --> 00:28:45,120
we can drive away or we can burn the mental defilements.

149
00:28:45,120 --> 00:28:58,240
Now, the Pali word for this English word is arta-p. An arta-b comes from the word arta-pa, which means

150
00:28:58,240 --> 00:29:12,240
heat. So a person who has heat is called arta-b and heatia means effort. So the effort has the ability

151
00:29:12,240 --> 00:29:21,840
to heat or to burn the mental defilements. When you read the passage in Pali,

152
00:29:23,040 --> 00:29:33,120
you can feel the meaning of the word arta-b as heating up the mental defilements.

153
00:29:33,120 --> 00:29:45,040
But in the English translation, you cannot get that kind of meaning. The first component Buddha

154
00:29:45,840 --> 00:29:56,560
mentioned here is effort or making effort. And the second is clearly comprehending.

155
00:29:56,560 --> 00:30:09,280
So a yogi, when he practices mindfulness, here mindfulness of the body, he must clearly comprehend

156
00:30:09,280 --> 00:30:20,240
the body. He must clearly see the body. That means he must see correctly. And clear comprehension

157
00:30:20,240 --> 00:30:33,520
here means not just seeing or understanding. But understanding that there are just the parts

158
00:30:33,520 --> 00:30:45,600
of the body, there are just the mind that is aware of the body, and there is the body,

159
00:30:45,600 --> 00:30:55,600
and there are only these two things going on at every moment. And there is nothing which we can call

160
00:30:56,720 --> 00:31:10,480
a soul or a self or arta in this body. So clearly comprehending means seeing the things

161
00:31:10,480 --> 00:31:23,680
correctly, seeing that they are just the manifestation of the different aspects of

162
00:31:25,520 --> 00:31:35,200
and the mind and body. And there is nothing apart from mind and body which we can call an agent

163
00:31:35,200 --> 00:31:44,160
or a person or a soul or a self. So understanding that way is called clear comprehension.

164
00:31:44,800 --> 00:31:56,720
So that will come only after some time of meditation. It will not come at the very beginning of

165
00:31:56,720 --> 00:32:08,160
the practice. But with practice, a yogi will be able to see in this way that there is nothing we can

166
00:32:08,160 --> 00:32:20,240
call a person or a soul or a self, apart from the mind and the matter that is functioning at the moment.

167
00:32:20,240 --> 00:32:31,040
When you try to be mindful of the breads and when you see that there are the breads

168
00:32:31,920 --> 00:32:40,880
and the mind that is mindful of the breads and nothing more than you are said to have clear

169
00:32:40,880 --> 00:32:55,840
comprehension. And the third component would have mentioned is mindfulness. So a yogi practices

170
00:32:55,840 --> 00:33:05,760
mindfulness and mindfulness means full awareness of the object. Not a simple awareness,

171
00:33:05,760 --> 00:33:18,320
not a superficial awareness, but a deep going awareness. And that full awareness of the object is

172
00:33:18,320 --> 00:33:29,680
what we call mindfulness. Now if you look at this sentence, you will find that the first is

173
00:33:29,680 --> 00:33:42,000
effort and the second is understanding and the third is mindfulness. Sometimes we cannot follow the

174
00:33:42,000 --> 00:33:50,880
order of the words in the discourse because from the practical point of view,

175
00:33:50,880 --> 00:34:04,720
mindfulness must come after hardened. You make effort and you are mindful. And your mindfulness

176
00:34:04,720 --> 00:34:14,320
will improve with practice and only after some practice, only after your mindfulness has become

177
00:34:14,320 --> 00:34:24,240
firm and mature that you will see things clearly or that clear comprehension can come.

178
00:34:24,880 --> 00:34:34,880
So although the order of the words here is the effort first, understanding second and mindfulness

179
00:34:34,880 --> 00:34:44,400
third, in practice we must understand that effort first and then them is mindfulness and

180
00:34:45,440 --> 00:34:58,480
third clear comprehension. There is a statement in the commentaries which says the order of the words

181
00:34:58,480 --> 00:35:06,400
is one thing and the order of the meaning is another. So that means sometimes we do not follow

182
00:35:06,400 --> 00:35:12,400
the order of words, but we follow the order of the meaning or order of what actually happens.

183
00:35:13,040 --> 00:35:25,040
So here we must understand that first there is effort and then mindfulness and then clear

184
00:35:25,040 --> 00:35:41,520
comprehension. And here as this sentence stands, there is one component missing.

185
00:35:45,040 --> 00:35:52,240
When you practice mindfulness, your mind will become concentrated.

186
00:35:52,240 --> 00:36:02,080
Only when your mind is concentrated, can you see clearly? Can you have clear comprehension?

187
00:36:02,960 --> 00:36:14,640
So concentration is also an important component in the practice, but Buddha did not mention

188
00:36:14,640 --> 00:36:25,360
concentration here. So the sub commentary explains that by mindfulness we must also take

189
00:36:26,000 --> 00:36:35,040
concentration. So following the sub commentary, we get four components here.

190
00:36:35,040 --> 00:36:49,760
Effort, mindfulness, concentration and understanding. Now there is one more component

191
00:36:52,560 --> 00:37:02,960
not mentioned here and that is faith or confidence. Now we need to have confidence

192
00:37:02,960 --> 00:37:11,760
in the teachings of the Buddha. We need to have confidence in practice and we need to have

193
00:37:11,760 --> 00:37:21,040
confidence in ourselves. If we do not have confidence, if we do not have devotion towards the

194
00:37:21,040 --> 00:37:35,200
Buddha, we will not practice at all. So although confidence is not functioning actively when we

195
00:37:35,200 --> 00:37:46,640
practice mindfulness, it is still in the background. So with confidence, we get five components.

196
00:37:46,640 --> 00:37:58,480
So confidence, effort, mindfulness, concentration and understanding. And these are the five that are

197
00:37:58,480 --> 00:38:10,640
called controlling faculties. By these words are done clearly comprehending and mindful,

198
00:38:10,640 --> 00:38:19,920
Buddha showed us the components of the practice. And by the next expression,

199
00:38:19,920 --> 00:38:43,760
removing covetousness and grief in the world, Buddha showed us the mental state to be abandoned.

200
00:38:43,760 --> 00:38:53,760
Towards removing covetousness and grief in the world, Buddha showed us what is removed,

201
00:38:54,640 --> 00:39:01,760
what are removed or what are abandoned by the practice of mindfulness.

202
00:39:01,760 --> 00:39:14,560
Or when we practice mentalness, we remove, we abandon, or we prevent covetousness and grief

203
00:39:14,560 --> 00:39:24,800
from arising in our minds. And you don't have to make special effort to remove covetousness

204
00:39:24,800 --> 00:39:35,600
and grief. When you are mindful, when your mind is concentrated and when you see things clearly,

205
00:39:36,480 --> 00:39:46,800
then you are at the same time removing, that means preventing covetousness and grief to arise in your mind.

206
00:39:46,800 --> 00:40:00,000
So at every moment of your practice, at every moment of mindfulness, at every moment of clear

207
00:40:00,000 --> 00:40:12,320
comprehension, you are removing the covetousness and grief. Now the original words for covetousness

208
00:40:12,320 --> 00:40:23,200
and grief are abigia and dominoes. Now the word abigia translated here is covetousness.

209
00:40:23,200 --> 00:40:44,320
Primarily means the desire to possess something that other people possess. Suppose it

210
00:40:44,320 --> 00:40:56,080
doesn't have a new car. Then if another person wants to get that new car for his own,

211
00:40:57,680 --> 00:41:09,840
then he is set to have abigia or covetousness. So abigia in this sense is not just attachment or desire

212
00:41:09,840 --> 00:41:21,440
or craving, but it is the desire to possess the possessions of other people. Say, you have a good car

213
00:41:21,440 --> 00:41:28,880
and I want to get that car for myself and that is covetousness. But here in this sentence,

214
00:41:29,920 --> 00:41:37,200
covetousness or abigia is used not in this narrow sense. It means just

215
00:41:37,200 --> 00:41:48,320
attachment or craving or desire. If the word grief also you must understand not just grief,

216
00:41:48,320 --> 00:42:07,840
but it will hate anger, disappointment, depression, discouragement and so on. Now there are five

217
00:42:07,840 --> 00:42:19,040
mental hindrances and among these five, these two are included covetousness and grief or attachment

218
00:42:19,760 --> 00:42:27,920
and ill will and these two are the more powerful hindrances than the other three.

219
00:42:28,640 --> 00:42:35,600
And so when these two are mentioned, we must understand that the others are also mentioned.

220
00:42:35,600 --> 00:42:45,760
So by the words removing covetousness and grief in the world really means removing all the five

221
00:42:45,760 --> 00:42:58,640
mental hindrances. So as you practice mindfulness, you remove these mental hindrances. So when the

222
00:42:58,640 --> 00:43:05,920
mental hindrances are removed, you get concentration. So when you get concentration, you see things

223
00:43:05,920 --> 00:43:18,800
clearly. Now in this sentence that the translation removing is important because in many other

224
00:43:18,800 --> 00:43:35,280
translations, they said, having removed or having abandoned or having overcome. But the

225
00:43:36,080 --> 00:43:47,360
translating the poly word with having is not correct here because if we say

226
00:43:47,360 --> 00:43:56,800
having removed covetousness and grief, it will, it may mean or it will mean that we must first

227
00:43:56,800 --> 00:44:04,560
remove covetousness and grief and then practice mindfulness. But actually, we practice mindfulness

228
00:44:04,560 --> 00:44:13,120
to get rid of or to remove covetousness and grief. If we have already removed covetousness and

229
00:44:13,120 --> 00:44:23,120
grief, we don't need to practice mindfulness. We already have achieved our goal. So the poly word here

230
00:44:24,480 --> 00:44:34,160
used here, although it can mean having removed or whatever, here it does not have that sense.

231
00:44:34,160 --> 00:44:48,400
So here it means that the removing and being mindful occur at the same time. So while he is

232
00:44:48,400 --> 00:44:56,640
mindful, he is removing covetousness and grief. So he doesn't have to make a special effort

233
00:44:56,640 --> 00:45:03,360
to remove covetousness and grief. As he goes along, he is removing covetousness and grief.

234
00:45:03,360 --> 00:45:13,040
And not after removing covetousness and grief, but he removed covetousness and grief as he

235
00:45:13,040 --> 00:45:22,880
go along with his practice of mindfulness. So by this short statement,

236
00:45:22,880 --> 00:45:33,520
Buddha taught us what to do when we practice mindfulness meditation and what

237
00:45:35,680 --> 00:45:46,960
mental stage we can remove or we can prevent as we go along the practice of mindfulness.

238
00:45:46,960 --> 00:45:58,240
I thought I could finish explaining the four foundations of mindfulness today, but as it is,

239
00:45:58,240 --> 00:46:23,520
we will have to wait until tomorrow for the explanation of the foundations of mindfulness.

