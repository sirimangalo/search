1
00:00:00,000 --> 00:00:07,000
Is drinking coffee breaking one of the precepts since it has so much caffeine?

2
00:00:07,000 --> 00:00:13,000
No, but there are a lot of unwholesome things that don't break the precepts.

3
00:00:13,000 --> 00:00:18,000
I think cigarettes are probably borderline, but probably not breaking the precepts,

4
00:00:18,000 --> 00:00:25,000
because they don't impair your mind to the extent that would be necessary to call it pamada tana.

5
00:00:25,000 --> 00:00:32,000
If you call it cigarettes, pamada tana grounds for negligence or drunkenness,

6
00:00:32,000 --> 00:00:39,000
then so many different things would also be grounds for negligence.

7
00:00:39,000 --> 00:00:47,000
People say, well, the type of people are trying to, this type group of meditators are trying to convince me

8
00:00:47,000 --> 00:00:53,000
that gambling was breaking the fifth precept, because gambling makes you negligent.

9
00:00:53,000 --> 00:00:57,000
So they translate pamada as negligent, but it's so clear that what is being meant here,

10
00:00:57,000 --> 00:00:59,000
it's not negligent, it says drunkenness.

11
00:00:59,000 --> 00:01:06,000
If gambling truly makes you drunk so that you don't, so that you can't see the cards in front of you,

12
00:01:06,000 --> 00:01:09,000
then okay, gambling is a pamada tana, but it's not.

13
00:01:09,000 --> 00:01:14,000
Gambling gamblers can be very clever, and their minds are very sharp, but it's an addiction.

14
00:01:14,000 --> 00:01:22,000
Not all addictions. If that were in the fifth precept, then having, having sexual intercourse

15
00:01:22,000 --> 00:01:26,000
would be in the fifth precept, because having sexual intercourse is intoxicating,

16
00:01:26,000 --> 00:01:31,000
and yet it's not even, it's not even the third precept, and so on.

17
00:01:31,000 --> 00:01:42,000
So I've said this before that the five precepts aren't all inclusive.

18
00:01:42,000 --> 00:01:49,000
They're not comprehensive, they're a guideline, and they're five things which a enlightened being would never do,

19
00:01:49,000 --> 00:01:52,000
but there's a lot more things that an enlightened being wouldn't do.

20
00:01:52,000 --> 00:01:56,000
To answer your question directly, that isn't sort of a direct answer,

21
00:01:56,000 --> 00:02:00,000
but to talk about what is caffeine, see, we don't have meditators.

22
00:02:00,000 --> 00:02:05,000
I don't let meditators drink caffeine, but it's a more subtle reason.

23
00:02:05,000 --> 00:02:08,000
I don't believe it's going to make them drunk, because it's not going to make them drunk,

24
00:02:08,000 --> 00:02:12,000
but it gives an artificial amount of energy.

25
00:02:12,000 --> 00:02:16,000
I used to drink coffee, a lot of mung string coffee.

26
00:02:16,000 --> 00:02:19,000
It's what they have, because they can't eat in the evening, so they have coffee,

27
00:02:19,000 --> 00:02:24,000
and they're not allowed to have milk in it or cream in it, so they take this artificial cream,

28
00:02:24,000 --> 00:02:32,000
the coffee made, coffee made, it's the Thai word, and they put coffee made in there, in their coffee.

29
00:02:32,000 --> 00:02:41,000
Lots of it, because it's oil protein, I think.

30
00:02:41,000 --> 00:02:47,000
And it gets you wired, right? I was staying with this forest monk, ostensibly a good monk,

31
00:02:47,000 --> 00:02:51,000
and I thought, okay, I'll maybe I'll come and stay with this group.

32
00:02:51,000 --> 00:02:56,000
It's this other group of monks in Thailand, a small group who are sort of broken away,

33
00:02:56,000 --> 00:03:00,000
or it's a political thing, but now they've started their own group,

34
00:03:00,000 --> 00:03:08,000
or they've imported something from Burma or something like that, and they have their own lineage.

35
00:03:08,000 --> 00:03:11,000
And I thought, okay, maybe I'll come and live with these guys, that'd be great.

36
00:03:11,000 --> 00:03:15,000
And we started talking, and I said, yeah, sometimes we practice meditation on ideas,

37
00:03:15,000 --> 00:03:17,000
and yeah, yeah, meditation on night, that's great.

38
00:03:17,000 --> 00:03:21,000
That's how all of our teachers became enlightened, and he has this book,

39
00:03:21,000 --> 00:03:25,000
where this teacher's in our own, this teacher's in our own, and he's talking.

40
00:03:25,000 --> 00:03:30,000
So what we did, I said, well, that's practice on night then,

41
00:03:30,000 --> 00:03:35,000
and what he did is he had like five cups of coffee, maybe not.

42
00:03:35,000 --> 00:03:40,000
He had a lot of coffee, and he just started talking and talking and talking,

43
00:03:40,000 --> 00:03:47,000
and I realized that probably his group of monks is not for me.

44
00:03:47,000 --> 00:03:51,000
I've realized that pretty much any group of monks is not for me,

45
00:03:51,000 --> 00:03:54,000
so that's why I live on night on night.

46
00:03:54,000 --> 00:03:58,000
But it's an artificial energy, and when it's gone,

47
00:03:58,000 --> 00:04:02,000
what I found from drinking coffee is that when it's gone,

48
00:04:02,000 --> 00:04:07,000
you actually have less energy, or at the very least you're right back where you started,

49
00:04:07,000 --> 00:04:10,000
and it's even less able to deal with it.

50
00:04:10,000 --> 00:04:11,000
This is with everything.

51
00:04:11,000 --> 00:04:15,000
If you would take painkillers, then you become less able to deal with the pain.

52
00:04:15,000 --> 00:04:19,000
If you have a upper, what is it?

53
00:04:19,000 --> 00:04:22,000
Just what do you call an upper?

54
00:04:22,000 --> 00:04:28,000
Stimulant, then you have a harder time dealing when you're depressed,

55
00:04:28,000 --> 00:04:32,000
when you have the downside, or when you're tired,

56
00:04:32,000 --> 00:04:37,000
when your mind is unclear, you might become less clear as a result.

57
00:04:37,000 --> 00:04:42,000
So for that reason, it's a very bad thing.

58
00:04:42,000 --> 00:04:47,000
Another thing about coffee, and it goes even further, or it shows this sort of thing,

59
00:04:47,000 --> 00:04:50,000
how it's dangerous.

60
00:04:50,000 --> 00:04:53,000
I've seen two meditators go crazy.

61
00:04:53,000 --> 00:04:57,000
Neither one was my student, so don't get thinking.

62
00:04:57,000 --> 00:05:02,000
But I watched them.

63
00:05:02,000 --> 00:05:05,000
I watched them when they went crazy,

64
00:05:05,000 --> 00:05:07,000
and I actually had to help deal with them.

65
00:05:07,000 --> 00:05:10,000
I spent four nights in a mental hospital with one of them.

66
00:05:10,000 --> 00:05:15,000
It turns out both of them were drinking coffee all night.

67
00:05:15,000 --> 00:05:20,000
They were instructed by their teachers to practice meditation day and night,

68
00:05:20,000 --> 00:05:22,000
walking and sitting.

69
00:05:22,000 --> 00:05:24,000
How are we going to do that?

70
00:05:24,000 --> 00:05:28,000
And they both had the same bright idea that string 10 cups of coffee,

71
00:05:28,000 --> 00:05:30,000
or whatever.

72
00:05:30,000 --> 00:05:34,000
To that extent, it was like eight cups of coffee, I think.

73
00:05:34,000 --> 00:05:39,000
And just no question.

74
00:05:39,000 --> 00:05:41,000
My mind is to why they went crazy.

75
00:05:41,000 --> 00:05:44,000
Of course, other reasons that helped it,

76
00:05:44,000 --> 00:05:47,000
but I think without that coffee,

77
00:05:47,000 --> 00:05:52,000
they probably wouldn't have done a lot better with the craziness.

78
00:05:52,000 --> 00:05:54,000
Because you do, you have craziness,

79
00:05:54,000 --> 00:05:57,000
and at this time, could you imagine having had 10 cups of coffee

80
00:05:57,000 --> 00:05:59,000
and then trying to deal with it?

81
00:05:59,000 --> 00:06:01,000
You can't rationally deal with it.

82
00:06:01,000 --> 00:06:06,000
You'd be smashed your head off the wall, you really would.

83
00:06:06,000 --> 00:06:09,000
So that's why we don't have coffee,

84
00:06:09,000 --> 00:06:11,000
or that's an extreme reason why.

85
00:06:11,000 --> 00:06:14,000
It's a good reason to tell meditators at the beginning,

86
00:06:14,000 --> 00:06:15,000
not to drink coffee at all,

87
00:06:15,000 --> 00:06:20,000
so that they don't get the idea that that's a viable way.

88
00:06:20,000 --> 00:06:23,000
It means of staying awake.

89
00:06:23,000 --> 00:06:25,000
But on a general level,

90
00:06:25,000 --> 00:06:27,000
it's because it's artificial,

91
00:06:27,000 --> 00:06:29,000
and it creates this artificial,

92
00:06:29,000 --> 00:06:31,000
the crunch, it creates,

93
00:06:31,000 --> 00:06:34,000
yeah, look at people, why people drink coffee?

94
00:06:34,000 --> 00:06:36,000
Because it becomes a crunch,

95
00:06:36,000 --> 00:06:38,000
and they need the boost, they need the stimulant.

96
00:06:38,000 --> 00:06:41,000
In all ways, we're trying to come back to nature.

97
00:06:41,000 --> 00:06:45,000
This is why I talk against practicing yoga for meditators.

98
00:06:45,000 --> 00:06:47,000
Some people get all up in arms,

99
00:06:47,000 --> 00:06:52,000
because you don't see monkey's practicing yoga.

100
00:06:52,000 --> 00:06:55,000
It's not intrinsic to reality.

101
00:06:55,000 --> 00:06:58,000
There's nothing wrong with it.

102
00:06:58,000 --> 00:06:59,000
If you want to practice yoga,

103
00:06:59,000 --> 00:07:00,000
it's a whole tradition of itself.

104
00:07:00,000 --> 00:07:02,000
There's a philosophy behind it, power to you.

105
00:07:02,000 --> 00:07:03,000
It's not what we do.

106
00:07:03,000 --> 00:07:07,000
What we do is coming back to ordinary reality,

107
00:07:07,000 --> 00:07:10,000
totally ordinary, totally natural.

108
00:07:10,000 --> 00:07:13,000
So there's nothing wrong with drinking coffee,

109
00:07:13,000 --> 00:07:15,000
but reliance on it,

110
00:07:15,000 --> 00:07:19,000
which comes naturally from drinking it.

111
00:07:19,000 --> 00:07:21,000
It's an unnatural state.

112
00:07:21,000 --> 00:07:27,000
It's a reliance on a particular type of experience,

113
00:07:27,000 --> 00:07:30,000
a partiality of you to that experience

114
00:07:30,000 --> 00:07:54,000
that makes it harder for you to deal with some other experience.

