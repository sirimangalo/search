1
00:00:00,000 --> 00:00:26,000
Okay. Now the live stream should be up. Technology. Technology and religion. What a combination.

2
00:00:26,000 --> 00:00:33,000
Anyway, the point is I'd like to offer you the opportunity to get religious.

3
00:00:33,000 --> 00:00:41,000
You don't have to. I think that's important, is that I'm not going to push you, but I want to be clear that that's the topic of tonight.

4
00:00:41,000 --> 00:00:54,000
To potentially challenge some of our secular, secularist attachments, our unwillingness to commit ourselves.

5
00:00:54,000 --> 00:01:00,000
And willingness to let go and fully submerge ourselves in an experience.

6
00:01:00,000 --> 00:01:06,000
We're often afraid of religion, and rightly so. Religion can be pretty terrible.

7
00:01:06,000 --> 00:01:12,000
That's not the problem of the word religion or the category of religion.

8
00:01:12,000 --> 00:01:17,000
I think that's the point, is that we can come up with Buddhist religion and we have.

9
00:01:17,000 --> 00:01:25,000
And this is religion that, as Buddhists, we can, we find palatable.

10
00:01:25,000 --> 00:01:40,000
It's religion. It shows that the problem is not with the category of religious activity where you fully immerse yourself and fully commit yourself to something.

11
00:01:40,000 --> 00:01:46,000
The whole point is committing yourself to the right thing, which is of course the scary thing. How do you know what's right?

12
00:01:46,000 --> 00:01:51,000
So I'm going to go through it, and I think for many people this isn't objectionable.

13
00:01:51,000 --> 00:02:15,000
For those of you who are Buddhists or Buddhist practitioners, we have what we call a ceremony to take on or to enter into the meditation practice.

14
00:02:15,000 --> 00:02:25,000
And this is what I went through when I first started meditating and I've helped countless people and lots and lots of people through this ceremony.

15
00:02:25,000 --> 00:02:35,000
I've stopped doing it since I've been in Canada, but not for any particular reason except that it's been, we've been busy setting up the center.

16
00:02:35,000 --> 00:02:44,000
It's the kind of thing you really need a staff. You need people to help you with, to really do a proper ceremony.

17
00:02:44,000 --> 00:02:56,000
So I'm going to go through the concept of the opening ceremony or the ceremony to receive the meditation practice.

18
00:02:56,000 --> 00:03:13,000
It comes actually from the Visudimanga and the tradition, the ancient tradition on the sorts of things you need to set yourself in or settle yourself in before you practice meditation.

19
00:03:13,000 --> 00:03:31,000
So it's not just arbitrary. It's based, I guess, you could say much, very much on the Arakakamatana, the types of meditations that protect and support insight meditation.

20
00:03:31,000 --> 00:03:34,000
And you should recognize some of the meds we go through it.

21
00:03:34,000 --> 00:03:46,000
The first thing you do though, this is a very old tradition, it's from the Visudimanga and probably before, you give yourself up to the Buddha.

22
00:03:46,000 --> 00:03:53,000
You really do, Buddhists actually do this. I'd like to invite you all to think about this.

23
00:03:53,000 --> 00:03:58,000
Mahasi Sayyatta gives a good explanation in one of his books.

24
00:03:58,000 --> 00:04:10,000
But how giving yourself up to the Buddha is a psychological support. It's putting your faith or your well-being in the hands of the Buddha.

25
00:04:10,000 --> 00:04:18,000
That's why we take refuge in the Buddha because we think of the Buddha as someone great, someone powerful.

26
00:04:18,000 --> 00:04:29,000
It's the same sort of psychological benefit of putting your trust in God. Of course, there's no God up there and there's certainly no Buddha up there who's going to hear your prayers.

27
00:04:29,000 --> 00:04:41,000
It's not the point. The point is the psychological, well, the commitment in your mind.

28
00:04:41,000 --> 00:04:50,000
And so we actually, we recite in Pali, the words are ima hang bhagava ata bhavang tumha kang pani chachami.

29
00:04:50,000 --> 00:05:04,000
Let me recite that and then translate it. It means, ima hang bhagava ata bhavang.

30
00:05:04,000 --> 00:05:17,000
I give this, or blessed one, I give this ata bhava, this self, this being that of my being.

31
00:05:17,000 --> 00:05:26,000
I entrust it into your care. I hand it over to you.

32
00:05:26,000 --> 00:05:44,000
So in the Visudhi Magha, in description of this determination, it describes the sort of people who are ideal for practicing meditation and the kind of people who are ready to die for the meditation.

33
00:05:44,000 --> 00:05:53,000
They're ready to sacrifice their own lives. They have such great faith and such great conviction.

34
00:05:53,000 --> 00:06:06,000
That's the best sort of meditator it says. I'm going to give you a good argue that too much faith can be a bad thing if the person can get stuck in the wrong path and just go with it.

35
00:06:06,000 --> 00:06:32,000
But the ability to give yourself up and to detach yourself from your ordinary predilections, your ordinary personality, meaning give yourself up, take your whole being and just plunk it down in the realm of Buddhism.

36
00:06:32,000 --> 00:06:37,000
So we make this determination, we say, I give myself up to you or blessed one.

37
00:06:37,000 --> 00:06:45,000
It's very religious now. I think it's interesting, it's interesting not the Buddhism can be very religious if you let it.

38
00:06:45,000 --> 00:06:53,000
And I think safely so because we're not deviating from the path, we're just intensifying it.

39
00:06:53,000 --> 00:07:10,000
Intensification can be scary, but it's not always wrong. As long as the principles are true are stick to what is right.

40
00:07:10,000 --> 00:07:15,000
So we're not deviating from the path by placing our trust in the Buddha.

41
00:07:15,000 --> 00:07:27,000
We're just intensifying it and really committing ourselves and saying, okay, I really do trust the Buddha.

42
00:07:27,000 --> 00:07:39,000
It sounds very religious, it sounds like what Christians would say, but there's a power to it and a good power in this case.

43
00:07:39,000 --> 00:07:48,000
And then secondly, you do the same thing with your teacher. So the teacher will be sitting at the front and you will say to the teacher,

44
00:07:48,000 --> 00:08:03,000
I give this, I give this being over to you or teacher.

45
00:08:03,000 --> 00:08:09,000
And trust it in your care.

46
00:08:09,000 --> 00:08:23,000
The main part of this is overcoming the doubts and skepticism and that really are inevitably in the final form of their hindrance.

47
00:08:23,000 --> 00:08:37,000
In the beginning, of course, you want to be circumspect and investigate the teacher, but to some extent, at least to some extent, you want to give yourself over to the experience.

48
00:08:37,000 --> 00:08:49,000
Now you can keep in the back of your mind, don't you don't give up your rational observation and analysis of whether this is actually good for you.

49
00:08:49,000 --> 00:08:58,000
But you put some trust in both the Buddha and in your teacher, and it's great.

50
00:08:58,000 --> 00:09:11,000
It's important psychologically, because too often we have meditators come and they're full of doubts about the practice and I think it's useful to give yourself up to things.

51
00:09:11,000 --> 00:09:24,000
And if you're not, unless you're terribly blind, I mean most of us are intelligent people and eventually you'll find out whether your trust was misplaced or not.

52
00:09:24,000 --> 00:09:32,000
But if you're constantly doubting what you're doing, you never have that experience.

53
00:09:32,000 --> 00:09:42,000
I remember not saying this is how you have to go about it, but it can be useful to give yourself up to the religion.

54
00:09:42,000 --> 00:09:44,000
So that's the second thing.

55
00:09:44,000 --> 00:09:47,000
The third thing you do is you ask for the kamatana.

56
00:09:47,000 --> 00:09:49,000
You actually make a request.

57
00:09:49,000 --> 00:09:55,000
We don't go around pushing meditation on others, but it was fairly careful about this, right?

58
00:09:55,000 --> 00:10:03,000
And from the very beginning, he wasn't even going to teach, he was in a sense you could say stubborn.

59
00:10:03,000 --> 00:10:16,000
Sticking to his guns, he made a determination that he would just pass away without teaching anyone unless someone came and asked him to teach.

60
00:10:16,000 --> 00:10:28,000
And so we take this stance of letting go ourselves, not preaching, not proselytizing, not trying to convert everyone.

61
00:10:28,000 --> 00:10:31,000
We leave the door open.

62
00:10:31,000 --> 00:10:46,000
And this is very much in line with the principles of renunciation and letting go, not clinging.

63
00:10:46,000 --> 00:10:49,000
So we require the meditators to come and ask for this.

64
00:10:49,000 --> 00:10:56,000
Another aspect of this is making it clear that this is something that is being given to you.

65
00:10:56,000 --> 00:11:02,000
It's not something you deserve or we have an obligation to give you.

66
00:11:02,000 --> 00:11:07,000
This isn't the right, so you've got many meditation centers and they charge you for it.

67
00:11:07,000 --> 00:11:15,000
And I've talked several times about how dangerous and problematic that is because then you feel entitled.

68
00:11:15,000 --> 00:11:18,000
The whole relationship is different.

69
00:11:18,000 --> 00:11:26,000
I can tell you to stay up all night or I can tell you to do all sorts of crazy things.

70
00:11:26,000 --> 00:11:31,000
But I can tell you to...

71
00:11:31,000 --> 00:11:36,000
I can push the meditators because I'm not getting paid for this.

72
00:11:36,000 --> 00:11:39,000
I don't have any responsibility to coddle you.

73
00:11:39,000 --> 00:11:43,000
It really is my way or the highway.

74
00:11:43,000 --> 00:11:45,000
Does that make sense?

75
00:11:45,000 --> 00:11:53,000
We're quite flexible, but there's an important power and it's a religious power.

76
00:11:53,000 --> 00:12:05,000
This coming and asking, beseeching, beseeching the teacher.

77
00:12:05,000 --> 00:12:21,000
And then my memory is a bit rusty as to the order, but I think the next thing we do.

78
00:12:21,000 --> 00:12:40,000
So we're ready to receive the kamatana.

79
00:12:40,000 --> 00:12:43,000
We first have to now establish our minds.

80
00:12:43,000 --> 00:12:56,000
We've already started, but we now get into certain meditations that are going to support us and come us enough to be able to accept the meditation properly.

81
00:12:56,000 --> 00:13:00,000
And so the next one is, I think, made that.

82
00:13:00,000 --> 00:13:07,000
If I've got the order right, I probably don't.

83
00:13:07,000 --> 00:13:14,000
No, we'll go with it.

84
00:13:14,000 --> 00:13:17,000
The next thing would be made that.

85
00:13:17,000 --> 00:13:21,000
So you say to yourself, ahang sukito humi, may I be happy?

86
00:13:21,000 --> 00:13:25,000
Well, that's why we're doing this, right?

87
00:13:25,000 --> 00:13:28,000
And then you can expand upon that.

88
00:13:28,000 --> 00:13:36,000
May I be free from suffering, may I be free from enmity, free from sickness, free from grief?

89
00:13:36,000 --> 00:13:48,000
And then you say sabbi, sattas, sukita, huntu, may all beings be happy.

90
00:13:48,000 --> 00:13:50,000
This is the important part.

91
00:13:50,000 --> 00:14:04,000
This is where you begin to settle your karmic debts, settle your cycles of revenge, your grudges, your feuds,

92
00:14:04,000 --> 00:14:10,000
your conflicts with other people, settle your affairs.

93
00:14:10,000 --> 00:14:16,000
They make a big deal about apologizing to people, especially enlightened beings.

94
00:14:16,000 --> 00:14:24,000
If there's any enlightened being or even spiritual, spiritually advanced person who you've wronged,

95
00:14:24,000 --> 00:14:29,000
who you've maligned, you should go and apologize to them.

96
00:14:29,000 --> 00:14:35,000
And there's great emphasis placed on this because it can be a hindrance to your path.

97
00:14:35,000 --> 00:14:42,000
And this is part of that, wishing happiness, settling your thoughts towards all other beings,

98
00:14:42,000 --> 00:14:48,000
whether you've had grudges or problems, conflicts with them in the past, you settle all that here.

99
00:14:48,000 --> 00:14:58,000
Put it all aside, you leave that baggage outside the door, and you enter into the temple, the temple of Buddhism.

100
00:14:58,000 --> 00:15:01,000
Right? Very religious.

101
00:15:01,000 --> 00:15:11,000
Give up all your grudges.

102
00:15:11,000 --> 00:15:13,000
And then it starts to turn serious.

103
00:15:13,000 --> 00:15:14,000
We contemplate death.

104
00:15:14,000 --> 00:15:16,000
Adhuang me, jibitang.

105
00:15:16,000 --> 00:15:19,000
My life is unstable.

106
00:15:19,000 --> 00:15:28,000
Dhuang marinang, death is certain, death is for sure.

107
00:15:28,000 --> 00:15:33,000
jibitang me, aniyatang, my life is uncertain.

108
00:15:33,000 --> 00:15:44,000
Aniyatang marinang, or marinang me atang, death only is certain.

109
00:15:44,000 --> 00:15:55,000
As again, you can see the pattern here, we've sort of dealt with the Buddha, and often this will be accompanied by recollections of the Buddha as well.

110
00:15:55,000 --> 00:15:57,000
And then we've made that now, death.

111
00:15:57,000 --> 00:16:05,000
This is one of the protective kamatanas, because this one, death gives you conviction, and gives you encouragement.

112
00:16:05,000 --> 00:16:12,000
It reminds you, oh yeah, that's why I'm doing this, because all of those defilements I have that I'm building up,

113
00:16:12,000 --> 00:16:15,000
and all the attachments and addictions.

114
00:16:15,000 --> 00:16:26,000
They're just leading me straight to death, and the danger of death is that it determines your birth, your next birth.

115
00:16:26,000 --> 00:16:35,000
If you've done bad, if you've done good, you'll get good.

116
00:16:35,000 --> 00:16:40,000
So we spend some time reflecting on death, at least reminding ourselves.

117
00:16:40,000 --> 00:16:45,000
Every time we begin the meditation, this is something that we do, we should do.

118
00:16:45,000 --> 00:16:51,000
And again, we've been negligent here at our new center, so we'll have to figure out some...

119
00:16:51,000 --> 00:16:57,000
Eventually, some way to incorporate this religious aspect into it.

120
00:16:57,000 --> 00:17:01,000
Maybe.

121
00:17:01,000 --> 00:17:09,000
At least, I think we can make it optional, so if someone wants to do it, we could have it available and we can do the ceremony for anyone who wants to do it.

122
00:17:09,000 --> 00:17:14,000
There's also a closing ceremony, they're quite beautiful.

123
00:17:14,000 --> 00:17:18,000
The last thing you do is ask forgiveness from each other formally.

124
00:17:18,000 --> 00:17:21,000
So anyway, we'll get to that.

125
00:17:21,000 --> 00:17:25,000
So after death, then it gets into the actual...

126
00:17:25,000 --> 00:17:29,000
Okay, focusing on insight meditation.

127
00:17:29,000 --> 00:17:32,000
You make a recollection.

128
00:17:32,000 --> 00:17:42,000
You have to think about it.

129
00:17:42,000 --> 00:18:01,000
Which loosely translates into that path by which all Buddhas and all their enlightened disciples have traveled in order to become enlightened.

130
00:18:01,000 --> 00:18:12,000
And enlightened is the path of the four Satipatana, the aikayana manga of the four Satipatana.

131
00:18:12,000 --> 00:18:14,000
That's a recollection we do.

132
00:18:14,000 --> 00:18:20,000
We remind ourselves this path that I'm following, this path that I'm undertaking now.

133
00:18:20,000 --> 00:18:27,000
This is the path taken by all Buddhas in there and anyone who wants to become enlightened has to follow the path of Satipatana.

134
00:18:27,000 --> 00:18:31,000
This is the path I'm entering upon, it's an ancient path.

135
00:18:31,000 --> 00:18:36,000
It's a hallowed path, it's holy.

136
00:18:36,000 --> 00:18:44,000
It is the right way, the one way, the only way.

137
00:18:44,000 --> 00:18:51,000
It is the way to enlightenment.

138
00:18:51,000 --> 00:18:57,000
And so we gain courage and encouragement in this way.

139
00:18:57,000 --> 00:19:02,000
And conviction and sort of a sense of severity, right?

140
00:19:02,000 --> 00:19:04,000
This is not some hobby that we're undertaking.

141
00:19:04,000 --> 00:19:06,000
It's not a game that we're playing.

142
00:19:06,000 --> 00:19:25,000
This path, this is the path of the Buddhas.

143
00:19:25,000 --> 00:19:39,000
And then I'm missing something.

144
00:19:39,000 --> 00:19:47,000
I've got it here somewhere, let's go look at it.

145
00:19:47,000 --> 00:20:07,000
Anyway, if there is something missing, I'll try and find it.

146
00:20:07,000 --> 00:20:29,000
This is a fairly religious thing, but it relates well to people who have accepted Buddhism as their religion because we pay respect to the Buddha.

147
00:20:29,000 --> 00:20:48,000
We pay respect to the Dhamma, we pay respect to the Sangha, we have this relationship psychologically with the Buddhas, our leader and someone who we revere, bringing flowers, candles and incense to the Buddha statues and so on.

148
00:20:48,000 --> 00:20:57,000
Thinking fondly and reverentially towards the Buddha, bowing down before the Buddha, et cetera.

149
00:20:57,000 --> 00:21:16,000
And so this last one, it says, through this practice of the Dhamma, in order to realize the Dhamma, I pay homage to the triple gem to the Buddha, the Dhamma and the Sangha, to three refuges.

150
00:21:16,000 --> 00:21:33,000
And the neat thing about this is it taps into the, or ties into the Buddhist idea that the best way to revere someone, the best way to pay respect to anyone to the Buddha especially, is to practice its teachings.

151
00:21:33,000 --> 00:21:36,000
This is the true way you honor a Buddha.

152
00:21:36,000 --> 00:21:47,000
Buddha said this before he passed away, he said, flowers and candles and incense, that's not the best way to honor a Buddha.

153
00:21:47,000 --> 00:21:50,000
And so here's our religious practice.

154
00:21:50,000 --> 00:22:01,000
We can consider that another great thing we're doing is we are promoting Buddhism, we are supporting Buddhism.

155
00:22:01,000 --> 00:22:16,000
We are engaged in the continuation, the prolongation of the life of the Buddha's teaching in our practice of Satyipatana.

156
00:22:16,000 --> 00:22:30,000
Ananda was the one I think he said after the Buddha passed away, he said, this is it, whether or not people practice the four Satyipatana will determine whether the Buddha's ass in the last so long time.

157
00:22:30,000 --> 00:22:48,000
And so here we pay the greatest and the highest respect to the Buddha through the practice of the four Satyipatana.

158
00:22:48,000 --> 00:23:02,000
There would also be in the ceremony there would have been before this, the taking of the eight precepts, of course, there's actually a formal ceremony for taking the eight precepts and then after this, the last thing that we would do is ask forgiveness from each other.

159
00:23:02,000 --> 00:23:21,000
So all the meditators would bow down to the teacher and say,

160
00:23:21,000 --> 00:23:42,000
oh, teacher, chariya, pamadina, dua rata yena, kattang, sabanga, paradang, all any wrongdoing that I have done through the three doors.

161
00:23:42,000 --> 00:24:04,000
That means by way of body, by way of speech, by way of thought, out of negligence, out of a lack of religions or a lack of religiosity.

162
00:24:04,000 --> 00:24:16,000
And then the teacher says, a hang wu, kamami, tumhahi bhi mahi, kamita bong.

163
00:24:16,000 --> 00:24:26,000
I forgive you, you as well should forgive me for such, for any such thing.

164
00:24:26,000 --> 00:24:35,000
And this is important because even throughout the meditation course, meditators can often get very angry at their teacher and that's okay.

165
00:24:35,000 --> 00:24:44,000
They can have bad thoughts and they can make them teacher's life miserable or more difficult anyway.

166
00:24:44,000 --> 00:24:58,000
And so at the beginning of the course and at the end of the course, you as forgiveness, the teacher also is not perfect.

167
00:24:58,000 --> 00:25:00,000
May say the wrong thing may teach the wrong thing, may even present a poor example of the practice.

168
00:25:00,000 --> 00:25:07,000
And so they ask forgiveness from the students for not being perfect.

169
00:25:07,000 --> 00:25:14,000
It's a great way to clear the air. All in all, it's quite a beautiful ceremony, very religious.

170
00:25:14,000 --> 00:25:32,000
And so the argument I want to make, the whole point of this is to present the concept of religion to Buddhists or to people practicing Buddhist meditators as not something that should be discounted or discarded.

171
00:25:32,000 --> 00:25:41,000
It could be, you can avoid it or minimize the importance.

172
00:25:41,000 --> 00:25:47,000
But you can also take it up that it could be something palatable to a Buddhist.

173
00:25:47,000 --> 00:25:58,000
We don't have to say that Buddhism is not a religion and that religious activity has no place in Buddhism because obviously there are millions of Buddhists around the globe who would disagree.

174
00:25:58,000 --> 00:26:09,000
But to some extent we consider ourselves a bit elitist as being people who focus on true Buddhism, right?

175
00:26:09,000 --> 00:26:19,000
There's a lot of Buddhists out there who don't really practice on the level that we would consider proper Buddhist practice.

176
00:26:19,000 --> 00:26:28,000
And I think it's easy to get arrogant and think, you know, all of that silly stuff is just culture, that's not real Buddhism.

177
00:26:28,000 --> 00:26:34,000
And I think we throw the baby out with a bathwater to some extent.

178
00:26:34,000 --> 00:26:41,000
The Buddha was, I think, quite religious and promoted a sense of religiosity.

179
00:26:41,000 --> 00:26:58,000
And there's definitely a long tradition in the meditation tradition that I follow of taking it quite seriously and making it quite religious in this way, really devoting yourself to it in a very real, truly religious sort of way.

180
00:26:58,000 --> 00:26:59,000
So there you go.

181
00:26:59,000 --> 00:27:05,000
I think this is something that I've never done before is go through in detail the opening ceremony in a video.

182
00:27:05,000 --> 00:27:13,000
So now we have my take on Buddhist religiosity.

183
00:27:13,000 --> 00:27:37,000
That's the number for tonight. If anyone has any questions, I'm happy to answer.

184
00:27:37,000 --> 00:27:44,000
What do I think I'm doing past life regressions? You better go.

185
00:27:44,000 --> 00:27:55,000
Sorry, I just wait for the meditators to actually go back and meditate before we get into the speculative stuff.

186
00:27:55,000 --> 00:28:01,000
Past life regressions. I don't think about them so much.

187
00:28:01,000 --> 00:28:20,000
I think I'm going to take a pass on that one as being one of those things I don't think about and therefore don't have an answer.

188
00:28:20,000 --> 00:28:30,000
Why don't I have my meditators listen to questions because you all have speculative questions that are just going to make them think too much.

189
00:28:30,000 --> 00:28:36,000
I think we're on the internet. You have to remember and so people ask me questions about crazy stuff.

190
00:28:36,000 --> 00:28:40,000
Stuff that I don't want my meditators to worry about.

191
00:28:40,000 --> 00:28:46,000
These people are doing eight, ten, twelve hours of meditation a day.

192
00:28:46,000 --> 00:28:51,000
Last thing they need is to be talking about politics.

193
00:28:51,000 --> 00:28:56,000
One of my meditators doesn't even know what their president is.

194
00:28:56,000 --> 00:29:07,000
She's from America and she doesn't yet know.

195
00:29:07,000 --> 00:29:12,000
You've rewarded it. What is a good way to remove doubts about rebirth?

196
00:29:12,000 --> 00:29:18,000
I could have entertained that aspect of it, but I'm still not that interested in it.

197
00:29:18,000 --> 00:29:23,000
If you have doubts about rebirth, you should say doubting, doubting.

198
00:29:23,000 --> 00:29:43,000
I've talked about, I think it's important to recognize the flaw in our thinking that after death nothing is the null hypothesis, meaning that it's the default.

199
00:29:43,000 --> 00:29:52,000
There's no reason to think that except if you're a physicalist, which is already taking something you have to take on belief.

200
00:29:52,000 --> 00:30:03,000
There's no reason to be a physicalist rather than a biosentrist or a phenomenon of a phenomenologist.

201
00:30:03,000 --> 00:30:09,000
Positiveist maybe even.

202
00:30:09,000 --> 00:30:18,000
If you have doubts about rebirth, I would just say doubting and letting go.

203
00:30:18,000 --> 00:30:23,000
That's the real point is I wouldn't concern too much about rebirth.

204
00:30:23,000 --> 00:30:28,000
You accept, and I think that's a part of religion as you accept these things provisionally.

205
00:30:28,000 --> 00:30:35,000
In the back of your mind you say, well, this may all turn out to be wrong, but I'm going to give it an honest try.

206
00:30:35,000 --> 00:30:45,000
You'll be clear with yourself that there's no reason.

207
00:30:45,000 --> 00:30:59,000
There's no better evidence for after death nothing than there is for after death's rebirth or birth again.

208
00:30:59,000 --> 00:31:11,000
Because what we see now is a continuation of the mind to say that this, for some reason, stops at death is a claim.

209
00:31:11,000 --> 00:31:19,000
You have to provide backing for that, and the backing of course is the idea that the brain is the source of the mind, but that's never been proven.

210
00:31:19,000 --> 00:31:24,000
There's no, there's no conclusive evidence.

211
00:31:24,000 --> 00:31:39,000
In fact, there's counter evidence that shows that the mind seemingly can be active when the brain is not.

212
00:31:39,000 --> 00:31:43,000
I know I've talked about all that, lots and lots.

213
00:31:43,000 --> 00:31:49,000
It's interesting stuff to talk about it certainly within the realm of what I should be talking about.

214
00:31:49,000 --> 00:31:56,000
I don't mean to dismiss the question of why in the heck should we believe in rebirth?

215
00:31:56,000 --> 00:31:59,000
But I have talked about it quite a bit.

216
00:31:59,000 --> 00:32:08,000
When we're overcome with lust and desire for a person, do we simply become aware of the lusting and know that until it reduces an intensity?

217
00:32:08,000 --> 00:32:22,000
Yeah, not just the lust, you'll note the seeing and the thinking and the feeling, the pleasure of it, because there's usually a pleasure associated with lust.

218
00:32:22,000 --> 00:32:24,000
You'll note all of that.

219
00:32:24,000 --> 00:32:25,000
It's certainly not easy.

220
00:32:25,000 --> 00:32:33,000
It's not like that suddenly magically going to make all your problems go away, but if you get good at it, you can change your life.

221
00:32:33,000 --> 00:32:37,000
And you can certainly weaken and eventually give up.

222
00:32:37,000 --> 00:32:41,000
There's a really ridiculous attachment to things that are really disgusting.

223
00:32:41,000 --> 00:32:53,000
The human body is certainly not full of sugar and spice and all things nice.

224
00:32:53,000 --> 00:32:58,000
What is the difference between wanting to end samsara and Vibhavatana?

225
00:32:58,000 --> 00:33:03,000
Someone asked me this just this morning actually.

226
00:33:03,000 --> 00:33:09,000
Is it right to say this for like a couple that brought me food this morning asked me about Vibhavatana?

227
00:33:09,000 --> 00:33:18,000
Is it right to say that the first case is simply coastal agenda as opposed to green?

228
00:33:18,000 --> 00:33:26,000
I would say wanting to end samsara is at least dangerously close to Vibhavatana.

229
00:33:26,000 --> 00:33:33,000
I would say wanting to end samsara is not necessarily wholesome.

230
00:33:33,000 --> 00:33:36,000
I didn't be inclined to think of it as unwholesome.

231
00:33:36,000 --> 00:33:41,000
Very subtly unwholesome, but at the kind of unwholesome that gets you born in the Brahma realms.

232
00:33:41,000 --> 00:33:43,000
The formless realms.

233
00:33:43,000 --> 00:33:47,000
Vibhavatana.

234
00:33:47,000 --> 00:33:50,000
It's not about not wanting to be reborn in samsara.

235
00:33:50,000 --> 00:33:56,000
That's just an intellectual outlook and it's sort of an aversion to escapism.

236
00:33:56,000 --> 00:34:02,000
Buddhist practice is about understanding samsara, seeing it clearly.

237
00:34:02,000 --> 00:34:07,000
And the result is not an aversion to it or desire to be free from it.

238
00:34:07,000 --> 00:34:16,000
The result is an ambivalence and disdain but a disregard for it.

239
00:34:16,000 --> 00:34:25,000
A complete and utter weiriness or disinterest in samsara.

240
00:34:25,000 --> 00:34:32,000
See, the only reason that samsara continues is because we're interested, because we like it or we dislike it.

241
00:34:32,000 --> 00:34:37,000
But the goal is to see it, to understand it.

242
00:34:37,000 --> 00:34:40,000
Because when you understand it, you see this is useless.

243
00:34:40,000 --> 00:34:44,000
Absolutely and utterly without any purpose whatsoever.

244
00:34:44,000 --> 00:34:51,000
You just start to get more and more disenchanted as the word.

245
00:34:51,000 --> 00:34:53,000
And peaceful as a result.

246
00:34:53,000 --> 00:35:00,000
And finally, there's perfect peace.

247
00:35:00,000 --> 00:35:03,000
These emotions are so powerful and they completely hijack your mind.

248
00:35:03,000 --> 00:35:08,000
I've tried the meditation on the disgustiness of the body, but it doesn't help that much.

249
00:35:08,000 --> 00:35:12,000
A good one is take the body parts of the person that you find attractive

250
00:35:12,000 --> 00:35:15,000
and visualize cutting them up.

251
00:35:15,000 --> 00:35:18,000
Visualize slicing them up into pieces.

252
00:35:18,000 --> 00:35:20,000
I mean, that's temporary.

253
00:35:20,000 --> 00:35:22,000
That's just like putting a bandaid over it.

254
00:35:22,000 --> 00:35:26,000
It still doesn't stop the festering.

255
00:35:26,000 --> 00:35:27,000
Nope.

256
00:35:27,000 --> 00:35:30,000
That's why the Satipatana are the only way.

257
00:35:30,000 --> 00:35:37,000
The only way to overcome things like lust is to see the lust clearly as it is

258
00:35:37,000 --> 00:35:42,000
and all the aspects of it, the pleasure and the experience and the thoughts

259
00:35:42,000 --> 00:35:45,000
you have about it.

260
00:35:45,000 --> 00:35:46,000
Because it really does.

261
00:35:46,000 --> 00:35:50,000
Once you see it clearly, you become disenchanted.

262
00:35:50,000 --> 00:35:52,000
You lose weiraga.

263
00:35:52,000 --> 00:35:53,000
You lose your raga.

264
00:35:53,000 --> 00:35:58,000
Lust.

265
00:35:58,000 --> 00:35:59,000
Yeah.

266
00:35:59,000 --> 00:36:05,000
Well, if you want to, if you want to overcome lust,

267
00:36:05,000 --> 00:36:08,000
stay away from people who you lust after.

268
00:36:08,000 --> 00:36:10,000
It's a good sort of...

269
00:36:10,000 --> 00:36:12,000
I mean, that's just artificial.

270
00:36:12,000 --> 00:36:13,000
It doesn't solve the problem.

271
00:36:13,000 --> 00:36:14,000
It's not the answer.

272
00:36:14,000 --> 00:36:17,000
But it's a good sort of stopgap measure.

273
00:36:17,000 --> 00:36:20,000
That's why people go to meditation centers.

274
00:36:20,000 --> 00:36:24,000
I remember there was one we were doing courses before in Stony Creek.

275
00:36:24,000 --> 00:36:28,000
And there are these paintings on the walls.

276
00:36:28,000 --> 00:36:30,000
And one of them is of the daughters of Mara.

277
00:36:30,000 --> 00:36:32,000
And of course they painted it.

278
00:36:32,000 --> 00:36:36,000
Where you can actually see their breasts.

279
00:36:36,000 --> 00:36:38,000
Originally, I was there when he was painting it.

280
00:36:38,000 --> 00:36:40,000
And he originally painted it.

281
00:36:40,000 --> 00:36:43,000
It was like 17 years ago.

282
00:36:43,000 --> 00:36:47,000
He originally painted it with these women bare chested.

283
00:36:47,000 --> 00:36:49,000
And then he...

284
00:36:49,000 --> 00:36:50,000
They happen to complain.

285
00:36:50,000 --> 00:36:54,000
And so he put some sort of gauze sort of...

286
00:36:54,000 --> 00:36:58,000
He usually painted a gauze over them like a...

287
00:36:58,000 --> 00:37:02,000
When he called some kind of semi-transparent cloth.

288
00:37:02,000 --> 00:37:06,000
And anyway, there was one of our meditators who came

289
00:37:06,000 --> 00:37:08,000
and we were giving spots.

290
00:37:08,000 --> 00:37:10,000
And we gave him the spot right under this thing.

291
00:37:10,000 --> 00:37:12,000
And he came to me off frustrated and said,

292
00:37:12,000 --> 00:37:14,000
of course I get put right under that.

293
00:37:14,000 --> 00:37:18,000
I have to do my walking and sitting meditation right under these

294
00:37:18,000 --> 00:37:22,000
sexually provocative pictures of these women.

295
00:37:22,000 --> 00:37:27,000
But yeah, you kind of want to avoid that sort of thing.

296
00:37:27,000 --> 00:37:48,000
We could set up a channel ground in second life.

297
00:37:48,000 --> 00:37:52,000
We could have models, pictures.

298
00:37:52,000 --> 00:37:53,000
They should.

299
00:37:53,000 --> 00:37:56,000
We're centers that have a channel ground.

300
00:37:56,000 --> 00:38:01,000
You know, place with models of dead bodies with blood coming out

301
00:38:01,000 --> 00:38:04,000
and past coming out.

302
00:38:04,000 --> 00:38:05,000
And pictures.

303
00:38:05,000 --> 00:38:08,000
You could just sit there and look at pictures of cancers,

304
00:38:08,000 --> 00:38:12,000
growths and pictures of cut-up bodies.

305
00:38:12,000 --> 00:38:15,000
And if you do a Google search for cancer,

306
00:38:15,000 --> 00:38:17,000
it's quite interesting.

307
00:38:17,000 --> 00:38:20,000
The Google image search for different kinds of cancer.

308
00:38:20,000 --> 00:38:44,000
Their breast cancer is probably a good one.

309
00:38:44,000 --> 00:38:49,000
That's an interesting point about virtual reality.

310
00:38:49,000 --> 00:38:54,000
You know, you can do, I mean, obviously in horrific ways,

311
00:38:54,000 --> 00:38:57,000
you can do things that you would never do in reality.

312
00:38:57,000 --> 00:38:58,000
But here's an interesting one.

313
00:38:58,000 --> 00:38:59,000
The Buddhist one.

314
00:38:59,000 --> 00:39:02,000
We could actually set up a virtual reality channel ground.

315
00:39:02,000 --> 00:39:04,000
And we wouldn't have to kill people.

316
00:39:04,000 --> 00:39:12,000
We wouldn't have to find dead bodies.

317
00:39:12,000 --> 00:39:15,000
How would that work?

318
00:39:15,000 --> 00:39:17,000
I think pictures would be the best.

319
00:39:17,000 --> 00:39:20,000
You could have a room full of pictures to contemplate.

320
00:39:20,000 --> 00:39:23,000
Photos.

321
00:39:23,000 --> 00:39:26,000
I don't think trying to make an actual decomposing corpse.

322
00:39:26,000 --> 00:39:28,000
Maybe it would work.

323
00:39:28,000 --> 00:39:31,000
If we had a corpse that was scripted to decompose

324
00:39:31,000 --> 00:39:37,000
when you click on it, slowly, slowly decompose.

325
00:39:37,000 --> 00:39:39,000
Dar has a channel ground.

326
00:39:39,000 --> 00:39:48,000
They see she's forward thinking.

327
00:39:48,000 --> 00:39:51,000
Dar has been conspicuously absent.

328
00:39:51,000 --> 00:40:03,000
I understand she's gone away or something.

329
00:40:03,000 --> 00:40:05,000
It's funny how every time I'm giving talks,

330
00:40:05,000 --> 00:40:10,000
now the sim turns dark as I give the talk.

331
00:40:10,000 --> 00:40:14,000
It's quite the visual.

332
00:40:14,000 --> 00:40:21,000
Okay, well, thank you.

333
00:40:21,000 --> 00:40:25,000
You've been a wonderful audience.

334
00:40:25,000 --> 00:40:29,000
It's always nice to meet up here.

335
00:40:29,000 --> 00:40:32,000
I've got the regulars and for those of you tuning in on YouTube.

336
00:40:32,000 --> 00:40:34,000
Thank you for tuning in.

337
00:40:34,000 --> 00:40:39,000
Please share the videos, subscribe to the channel, et cetera, et cetera.

338
00:40:39,000 --> 00:40:42,000
And then there's also those of you who might be listening in on audio.

339
00:40:42,000 --> 00:40:43,000
Thank you.

340
00:40:43,000 --> 00:40:54,000
Thank you for your patient attendance and your aspiration towards goodness.

341
00:40:54,000 --> 00:41:03,000
Have a good night, everyone.

342
00:41:03,000 --> 00:41:08,000
Thank you.

