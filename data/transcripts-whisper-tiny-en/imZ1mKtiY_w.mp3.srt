1
00:00:00,000 --> 00:00:26,000
Good evening everyone, welcome to our evening dhamma.

2
00:00:26,000 --> 00:00:44,000
We have two new meditators here, two meditators here now, one is left.

3
00:00:44,000 --> 00:00:50,000
We're just winding down in a little over a week.

4
00:00:50,000 --> 00:00:54,000
I'll be heading to Florida.

5
00:00:54,000 --> 00:01:01,000
I'll be back in the new year.

6
00:01:01,000 --> 00:01:17,000
So last night there was this idea that the ocean, the ocean is profound, deep, great.

7
00:01:17,000 --> 00:01:29,000
And yet compared to the dhamma, it's quite insignificant.

8
00:01:29,000 --> 00:01:33,000
So tonight I thought I'd expand upon that.

9
00:01:33,000 --> 00:01:43,000
Let's talk about significance.

10
00:01:43,000 --> 00:01:46,000
Let me start with ourselves.

11
00:01:46,000 --> 00:01:50,000
Let's start with this life, this human being.

12
00:01:50,000 --> 00:01:58,000
Let me call I, who has been born and is going to die.

13
00:01:58,000 --> 00:02:01,000
And has what some people would call endless possibilities.

14
00:02:01,000 --> 00:02:10,000
We're just kind of a joke because we have very little possibilities as a human being.

15
00:02:10,000 --> 00:02:17,000
We're rather insignificant in the whole scheme of things.

16
00:02:17,000 --> 00:02:25,000
We can't even fly most of us.

17
00:02:25,000 --> 00:02:31,000
We can't walk through walls.

18
00:02:31,000 --> 00:02:34,000
We can't travel through the cosmos.

19
00:02:34,000 --> 00:02:55,000
We can't even learn more than a handful of languages before we die and forget them all.

20
00:02:55,000 --> 00:02:59,000
So we have all these plans and ambitions in there.

21
00:02:59,000 --> 00:03:04,000
So pitiful and insignificant, meaningless.

22
00:03:04,000 --> 00:03:09,000
Can imagine the angels and gods up and having shaking their heads or laughing at us.

23
00:03:09,000 --> 00:03:13,000
Maybe they make games out of us.

24
00:03:13,000 --> 00:03:15,000
Maybe we're like entertainment for them.

25
00:03:15,000 --> 00:03:18,000
Like these reality shows.

26
00:03:18,000 --> 00:03:26,000
But there is a significant number of angels who use us as their live reality shows.

27
00:03:26,000 --> 00:03:29,000
Or is it the Kardashians?

28
00:03:29,000 --> 00:03:31,000
Wasn't there this show about...

29
00:03:31,000 --> 00:03:33,000
Or didn't Ozzie Osbourne do his show?

30
00:03:33,000 --> 00:03:36,000
I hear about these and of course watch them.

31
00:03:36,000 --> 00:03:40,000
But you think that's interesting.

32
00:03:40,000 --> 00:03:45,000
Imagine how the angels are up in heaven and all their different heavens.

33
00:03:45,000 --> 00:03:48,000
So is what they say about Mara.

34
00:03:48,000 --> 00:03:50,000
Where the live.

35
00:03:50,000 --> 00:03:56,000
What is this? What is the term reality? Reality television?

36
00:03:56,000 --> 00:04:00,000
Where the reality show for Mara?

37
00:04:00,000 --> 00:04:04,000
The Parini Mita Wasawati Day was.

38
00:04:04,000 --> 00:04:10,000
The angels that delight in the creations of others.

39
00:04:10,000 --> 00:04:12,000
That's all we are.

40
00:04:12,000 --> 00:04:15,000
We're just rats in a cage.

41
00:04:15,000 --> 00:04:21,000
They probably perform experiments on us as well.

42
00:04:21,000 --> 00:04:33,000
Maybe from time to time pretend to be aliens and beam down and spook the heck out of us.

43
00:04:33,000 --> 00:04:35,000
The angels...

44
00:04:35,000 --> 00:04:39,000
There's one story that of course deals with this really.

45
00:04:39,000 --> 00:04:44,000
The angels hear about how short a life humans have.

46
00:04:44,000 --> 00:04:48,000
This one angel died in the morning and then the afternoon came back.

47
00:04:48,000 --> 00:04:50,000
Our disappeared in the morning and the afternoon came back.

48
00:04:50,000 --> 00:04:52,000
And they asked where she'd been.

49
00:04:52,000 --> 00:04:55,000
And she said, oh, I died and I was born as a human.

50
00:04:55,000 --> 00:04:58,000
And I lived 67 years as a human being.

51
00:04:58,000 --> 00:05:03,000
Before dying again and being born again in the afternoon.

52
00:05:03,000 --> 00:05:14,000
And they were shocked at how short the life of a human is comparatively.

53
00:05:14,000 --> 00:05:16,000
And we're so egotistical about it.

54
00:05:16,000 --> 00:05:21,000
So I think how profound is this human life?

55
00:05:21,000 --> 00:05:23,000
This body is a temple.

56
00:05:23,000 --> 00:05:32,000
This body that's full of urine and feces and all things terrible.

57
00:05:32,000 --> 00:05:35,000
And we're so proud of it, right?

58
00:05:35,000 --> 00:05:49,000
We primp and primate and we fret and worry about size and shape and color, proportions.

59
00:05:49,000 --> 00:05:54,000
We cover it, we uncover it, we cover end uncover it,

60
00:05:54,000 --> 00:06:04,000
and it's suggestively to try to attract each other this body.

61
00:06:04,000 --> 00:06:13,000
So human beings are rather insignificant.

62
00:06:13,000 --> 00:06:17,000
So then we have the angels, but even the angels are pretty pathetic and insignificant.

63
00:06:17,000 --> 00:06:27,000
Angels, apparently they go to war with each other, they still argue and they have questions and debates.

64
00:06:27,000 --> 00:06:31,000
They're still afraid of death.

65
00:06:31,000 --> 00:06:38,000
They still have the potential to perform all sorts of unwholesome deans and go to hell.

66
00:06:38,000 --> 00:06:48,000
They might live thousands, I don't know, millions of years, even probably millions.

67
00:06:48,000 --> 00:06:58,000
That wasn't anyway.

68
00:06:58,000 --> 00:07:03,000
But even the angels, even the angel realms come to an end.

69
00:07:03,000 --> 00:07:08,000
And whatever, apparently they spend all their time throwing flowers around,

70
00:07:08,000 --> 00:07:13,000
picking flowers from their beautiful flower trees and singing and dancing and carrying on,

71
00:07:13,000 --> 00:07:22,000
and all sorts of other useless pastimes, meaningless, pointless, frivolous.

72
00:07:22,000 --> 00:07:31,000
And in their gardens, all they end up growing is greed and attachment and partiality and aversion and ego,

73
00:07:31,000 --> 00:07:37,000
pathetic.

74
00:07:37,000 --> 00:07:42,000
Insignificant.

75
00:07:42,000 --> 00:07:48,000
So even the angels are insignificant, but what about the gods, no?

76
00:07:48,000 --> 00:07:56,000
Well the gods, gods will live millions, billions of years, I don't know, probably billions.

77
00:07:56,000 --> 00:08:00,000
Imagine living billions of years.

78
00:08:00,000 --> 00:08:04,000
So here we are talking about this ocean.

79
00:08:04,000 --> 00:08:10,000
Talking about the Pacific Ocean, the oceans of earth, this earth, this planet.

80
00:08:10,000 --> 00:08:12,000
How huge?

81
00:08:12,000 --> 00:08:19,000
Consider the scale of this planet, how insignificant one human being is on the face of the planet.

82
00:08:19,000 --> 00:08:26,000
But how pitifully insignificant is the earth in the face of the universe?

83
00:08:26,000 --> 00:08:36,000
There's so much out there beyond what just, beyond just what NASA and scientists,

84
00:08:36,000 --> 00:08:46,000
like Stephen Hawking or Hocking, are capable of comprehending aliens.

85
00:08:46,000 --> 00:08:54,000
Aliens are the least of what it would exist in the universe.

86
00:08:54,000 --> 00:09:01,000
There's so much, so much potential, not as a human being, but beyond the human.

87
00:09:01,000 --> 00:09:07,000
So much potential that we as human beings will never experience, not in this life.

88
00:09:07,000 --> 00:09:21,000
And everyone can't even fathom for the most part.

89
00:09:21,000 --> 00:09:30,000
And so there's this idea in spirituality, even in science, of the vastness of the universe,

90
00:09:30,000 --> 00:09:34,000
of the greatness of the universe.

91
00:09:34,000 --> 00:09:43,000
At the level of Godhood, well surely there there's some significance.

92
00:09:43,000 --> 00:09:59,000
God's are pitiful and insignificant as well. In fact, all of some sorrows, pitifully insignificant.

93
00:09:59,000 --> 00:10:05,000
The whole of this universe, no matter how big it gets.

94
00:10:05,000 --> 00:10:11,000
When compared with the dhamma, it is completely insignificant.

95
00:10:11,000 --> 00:10:20,000
It can show me the horsehead nebula, you can show me a black hole or dark matter.

96
00:10:20,000 --> 00:10:31,000
Apparently now they're working on quantum computers that are neither here nor there.

97
00:10:31,000 --> 00:10:40,000
Pitiful, meaningless, insignificant, inconsequential.

98
00:10:40,000 --> 00:10:45,000
It's the incredible nature of the dhamma that it goes beyond all this,

99
00:10:45,000 --> 00:10:51,000
no matter how incredibly complex and seemingly profound the universe might become.

100
00:10:51,000 --> 00:11:12,000
It's all pitifully insignificant, meaningless, pointless, useless, inconsequential.

101
00:11:12,000 --> 00:11:21,000
When we look at the Buddha taught, when we compare it to all this, this whole universe,

102
00:11:21,000 --> 00:11:28,000
and all of its glory and profundity, we see something quite different.

103
00:11:28,000 --> 00:11:33,000
We see a different focus, a different dimension if you will.

104
00:11:33,000 --> 00:11:41,000
It's the dimension of happiness and the dimension of suffering.

105
00:11:41,000 --> 00:11:49,000
It doesn't matter how profound the universe is in the end it all boils down to happiness or suffering.

106
00:11:49,000 --> 00:12:05,000
It boils down to the fact that reality is comprised of impermanent unsatisfying and uncontrollable phenomena.

107
00:12:05,000 --> 00:12:23,000
It doesn't matter how many there are, how complex they are, how varied they are.

108
00:12:23,000 --> 00:12:29,000
They are all governed by this love, nature, love, reality.

109
00:12:29,000 --> 00:12:38,000
It doesn't matter whether you travel the whole of the universe or be born as a god or an angel.

110
00:12:38,000 --> 00:12:43,000
It's no different than if you stay in your room and meditate.

111
00:12:43,000 --> 00:13:00,000
There's still only impermanence unsatisfying and uncontrollable phenomena.

112
00:13:00,000 --> 00:13:10,000
Nothing worth clinging to, nothing worth striving for, nothing worth nothing worth anything.

113
00:13:10,000 --> 00:13:16,000
This is why the Buddha said something quite, I suppose, depressing sounding.

114
00:13:16,000 --> 00:13:23,000
All of existence, no existence, not even the smallest bit of existence is worth anything.

115
00:13:23,000 --> 00:13:30,000
It's like feces if you have the smallest bit of feces, it's still feces.

116
00:13:30,000 --> 00:13:38,000
It's something that's hard for us to swallow because we're so enamored by and caught up in experience.

117
00:13:38,000 --> 00:13:45,000
What else is there, right?

118
00:13:45,000 --> 00:13:48,000
So we've got a lot of learning to do.

119
00:13:48,000 --> 00:13:59,000
Another aspect of this is how deeply we have our claws or how deeply some sara has its claws in us.

120
00:13:59,000 --> 00:14:08,000
And deeply intertwined how deeply who we are, who we are is a part of the problem.

121
00:14:08,000 --> 00:14:11,000
It's a part of some sara.

122
00:14:11,000 --> 00:14:20,000
It's causing us suffering, stress.

123
00:14:20,000 --> 00:14:30,000
So the Buddha's teaching is about freeing ourselves from this system, freeing ourselves from ourselves really.

124
00:14:30,000 --> 00:14:36,000
I just got a lighter this evening from someone telling me a friend of ours.

125
00:14:36,000 --> 00:14:46,000
I didn't care for a bit of a station to Afghanistan or something who has been following my teachings online.

126
00:14:46,000 --> 00:15:12,000
It sort of made me think of this whole idea as well of how small the universe is, how connected we all are.

127
00:15:12,000 --> 00:15:33,000
I mean that's really the great part of the universe is how easy it is to make this connection and to find the Buddha's teaching even from all around the globe.

128
00:15:33,000 --> 00:15:40,000
How easy it is for angels to come and to hear the Buddha's teaching as well.

129
00:15:40,000 --> 00:16:02,000
Because of the nature of the world as being somehow connected, not so big.

130
00:16:02,000 --> 00:16:20,000
But the greatest lesson from all of this is to see through our ambitions and our even our path in life, whatever path we've chosen outside of the search for freedom from suffering.

131
00:16:20,000 --> 00:16:26,000
It has to be suspect because it's caught up in things that can't satisfy us.

132
00:16:26,000 --> 00:16:40,000
It's caught up in impermanent phenomena. It's caught up in this terrible, terribly useless and meaningless universe that we find ourselves in.

133
00:16:40,000 --> 00:16:57,000
So I'm previously trying to be challenging here. I'm imagining that this video is probably going to get the most downvotes in a while. I always check on YouTube to see upvotes downvotes.

134
00:16:57,000 --> 00:17:11,000
I think this one's probably going to get a lot of downvotes. It's challenging. The Buddha said, most Mahasi Sayyadas, as most people are interested. He has this nice quote where he says, most people are interested in the number for this reason.

135
00:17:11,000 --> 00:17:26,000
I can boldly say this. I mean it sounds awful. I would think for most people this sounds very hard to swallow if not outright unpleasant.

136
00:17:26,000 --> 00:17:46,000
But this can be said because we have something. We have a very powerful truth and that is the key to freedom from suffering.

137
00:17:46,000 --> 00:17:58,000
The powerful truth is that you can cling to some sorrow you want, but you can't deny that you're suffering. You may not think that it's some sorrow that's causing into the universe or your goals or ambitions.

138
00:17:58,000 --> 00:18:05,000
But you know that you are suffering. It doesn't take much to see that.

139
00:18:05,000 --> 00:18:17,000
And so we lay this bold claim that you're suffering because of your ambitions, not because you can't realize them because you have them in the first place.

140
00:18:17,000 --> 00:18:26,000
It's quite simple. I mean if you let go, if you stop trying to be be or find or become something,

141
00:18:26,000 --> 00:18:37,000
and we would suffering could come to you. If nothing has a hold on you, like I wish this, I wish that, I want this, I want that. If you had none of that, how could you suffer?

142
00:18:37,000 --> 00:18:46,000
Where could all this stress come from? All this stress. All this stress we have. So much stress, right?

143
00:18:46,000 --> 00:19:02,000
Nothing at a hold on us. There was nothing that we clung to. If we did see this universe just as meaningless. Not in a bad way, exactly, but in a meaningless way.

144
00:19:02,000 --> 00:19:19,000
It's like when you look at a rock, you don't think, wow, that rock has purpose. You think, no, it's just a rock. It doesn't bother you. But you don't cling to it. The problem is here we are clinging to this rock.

145
00:19:19,000 --> 00:19:40,000
We cling to all sorts of meaningless, useless things. All we have to do is look down and see, wow, this is just a meaningless piece of stone. There's no benefit coming from my clinging to it. In fact, I'm hurting myself.

146
00:19:40,000 --> 00:19:57,000
That's the practice. That's what we do is to actually look, not have theories and views, like all of this made that sound quite theoretical and it involves theories, it involves people's theories and views, it challenges them.

147
00:19:57,000 --> 00:20:10,000
But we can boldly say that we can boldly black up back up these claims. Come see, look, if you look, this is what you'll see. Won't be pleasant.

148
00:20:10,000 --> 00:20:24,000
It won't be something, it won't be what you want to see most likely, but it's what's real. It's what's there. You can't deny the fact, the truth.

149
00:20:24,000 --> 00:20:33,000
Anyway, I thought that would be an interesting food for thought for you all tonight.

150
00:20:33,000 --> 00:20:38,000
That's our dumber for this evening.

151
00:20:38,000 --> 00:21:00,000
Anybody has any questions? I can answer them.

152
00:21:00,000 --> 00:21:13,000
Here's one from the online site. I have realized that from meditation, I have created a lot of clinging and of meditative compulsively feeling that I have to meditate. That's creating a great subtle aversion.

153
00:21:13,000 --> 00:21:20,000
This may be hard to answer directly, but it would be would it be advised to take a break from meditation or resume shortly or would also be appropriate.

154
00:21:20,000 --> 00:21:28,000
It would be appropriate to note the aversion. If you don't like to meditate, just say disliking, disliking.

155
00:21:28,000 --> 00:21:37,000
Meditate on it. That's normal. Meditation is terribly uncomfortable a lot of the time, so it's quite normal for you to start to dislike it.

156
00:21:37,000 --> 00:21:40,000
That's just your mind complaining.

157
00:21:40,000 --> 00:21:51,000
If we see the manga talks about this, it says, when you want to teach a baby ox, you have to take it away from its mother, tie it to a stake, and just let it run.

158
00:21:51,000 --> 00:22:06,000
It will scream and it will pull against the rope, but if you have a strong rope and a strong wooden stake, you can keep it and eventually it will get tired.

159
00:22:06,000 --> 00:22:14,000
So let the mind go. Meditation isn't painful. It isn't unpleasant, but our mind wants such pleasant things.

160
00:22:14,000 --> 00:22:24,000
So just let it go. It's different from torturing yourself. Just keep going, keep meditating. You're not torturing yourself. Your mind is just whining and complaining.

161
00:22:24,000 --> 00:22:30,000
It'll get over it.

162
00:22:30,000 --> 00:22:37,000
I mean, if you stick with it, it will.

163
00:22:37,000 --> 00:22:43,000
So that was it, one question.

164
00:22:43,000 --> 00:22:49,000
If there are no other local questions,

165
00:22:49,000 --> 00:22:55,000
I think I'll just say goodnight.

166
00:22:55,000 --> 00:23:01,000
Can one invite angels to listen to the dhamma and how? That's common for people to invite.

167
00:23:01,000 --> 00:23:07,000
It's a common Buddhist thing. When a monk would give a dhamma talk, a formal talk,

168
00:23:07,000 --> 00:23:10,000
they will invite the angels to come.

169
00:23:10,000 --> 00:23:16,000
Nowadays, when monks give blessings, they'll invite the angels to come and listen.

170
00:23:16,000 --> 00:23:26,000
So we have this chant.

171
00:23:26,000 --> 00:23:54,000
This is the time for listening to the dhamma.

172
00:23:54,000 --> 00:24:01,000
I've always been a part of the dhamma.

173
00:24:01,000 --> 00:24:10,000
I've always been a part of the dhamma.

174
00:24:10,000 --> 00:24:16,000
Imagine the angels, the Buddhist angels are having their dhamma talks and dhamma discourses,

175
00:24:16,000 --> 00:24:21,000
dhamma discussions, and they're probably still coming down to earth til they find the good monks.

176
00:24:21,000 --> 00:24:28,000
There's probably lots of angels around our gentong.

177
00:24:28,000 --> 00:24:31,000
I would think.

178
00:24:31,000 --> 00:24:41,000
Never saw any lighting up, a chamtong, like Taitavana or dhamma.

179
00:24:41,000 --> 00:24:46,000
Okay, let's call it a night then. Thank you all for coming out.

180
00:24:46,000 --> 00:24:53,000
I wish you all a good practice.

