1
00:00:00,000 --> 00:00:13,680
Okay, so tonight we're answering a question about whether anything can take precedent

2
00:00:13,680 --> 00:00:15,680
over meditation.

3
00:00:15,680 --> 00:00:26,920
The question was in regard to sleep and just a very common question.

4
00:00:26,920 --> 00:00:30,960
What to do when you're too tired to meditate?

5
00:00:30,960 --> 00:00:41,480
The story of someone who has children and busy job, busy life, and they find themselves falling

6
00:00:41,480 --> 00:00:42,480
asleep.

7
00:00:42,480 --> 00:00:45,440
They're in a bit of a dilemma.

8
00:00:45,440 --> 00:00:55,800
They want to meditate, but they need to sleep, and so how do you resolve this?

9
00:00:55,800 --> 00:01:01,280
The question was, can there sometimes be, can something sometimes be more important than

10
00:01:01,280 --> 00:01:02,760
meditation?

11
00:01:02,760 --> 00:01:10,760
Take precedent.

12
00:01:10,760 --> 00:01:12,360
And so that's the general question.

13
00:01:12,360 --> 00:01:18,000
The specific question was about sleep, but we're going to answer the general and I'll

14
00:01:18,000 --> 00:01:21,960
try and answer the sleep on this part of it.

15
00:01:21,960 --> 00:01:27,440
So the answer of whether or not anything is more important than meditation.

16
00:01:27,440 --> 00:01:42,600
The answer is, no, maybe, and yes, I'm going to answer it three ways.

17
00:01:42,600 --> 00:01:47,680
No, nothing is more important than meditation.

18
00:01:47,680 --> 00:01:52,280
Like by meditation, you mean the path that leads to freedom from suffering, then no, nothing

19
00:01:52,280 --> 00:01:54,840
is more important.

20
00:01:54,840 --> 00:02:03,400
And the significance there, the important lesson that that answer, is to remind us that

21
00:02:03,400 --> 00:02:09,400
we have to step outside of our conventional sense of what's important.

22
00:02:09,400 --> 00:02:16,640
We build up false, let's say, artificial ideas of what's important.

23
00:02:16,640 --> 00:02:22,800
And by use the word false, because often you have to call them false.

24
00:02:22,800 --> 00:02:35,520
We pick up artificially, and when examined deeply, things that are no important, and we

25
00:02:35,520 --> 00:02:38,760
say that they're important.

26
00:02:38,760 --> 00:02:43,720
Religions to this, in the sense of so many things that they say are important Buddhism,

27
00:02:43,720 --> 00:02:51,880
but Buddhists are guilty of this, groups of Buddhists, I've been as a monk, I've been told

28
00:02:51,880 --> 00:02:58,000
many times by many different groups, what's important, and sometimes it is, and sometimes

29
00:02:58,000 --> 00:03:06,040
you can see how non-important, non-essential things are, can become to be deemed essential.

30
00:03:06,040 --> 00:03:14,880
A good example, I think, is something like politics.

31
00:03:14,880 --> 00:03:25,840
Because politics starts out, in a person's mind, in a good way, start out being concerned

32
00:03:25,840 --> 00:03:33,560
about something, let's say greed, take a really good example.

33
00:03:33,560 --> 00:03:37,560
A good example would be someone who's concerned about greed, and then they start focusing

34
00:03:37,560 --> 00:03:45,600
on how someone is greedy, or some group of people is greedy, let's say the rich are greedy.

35
00:03:45,600 --> 00:03:53,520
So then they champion, they hear some politician who says, we should tax the rich people,

36
00:03:53,520 --> 00:03:59,280
and they follow that person, and eventually it becomes less and less about the issue and

37
00:03:59,280 --> 00:04:03,640
more and more about winning an election, for example, and what I mean is following your

38
00:04:03,640 --> 00:04:13,240
party and getting caught up in fighting, in whole other issue of political systems and human

39
00:04:13,240 --> 00:04:25,040
society and losing sight of what's really important.

40
00:04:25,040 --> 00:04:31,400
This doesn't answer the question of whether you should sleep or meditate, but it's important

41
00:04:31,400 --> 00:04:44,280
to remember that there are many things that are going to fall by the wayside, and we realize

42
00:04:44,280 --> 00:04:50,800
how inconsequential they are in relation to meditation, in relation to freedom from suffering.

43
00:04:50,800 --> 00:05:00,160
Because ultimately the political party wins, even whether society becomes pure or not,

44
00:05:00,160 --> 00:05:05,120
it isn't going to make a lot of difference if our own minds are solid in the process.

45
00:05:05,120 --> 00:05:10,360
Anger is a really more than greed, perhaps anger is more clear.

46
00:05:10,360 --> 00:05:19,280
We see how awful some people are and we want to keep we want to, and the hatred and evil,

47
00:05:19,280 --> 00:05:22,720
it's the wickedness that people have to each other, and so we take up a political cause

48
00:05:22,720 --> 00:05:29,360
and we get very angry ourselves, and if what you're doing involves this corruption of

49
00:05:29,360 --> 00:05:36,080
mind of greed, anger, and delusion where you build up someone else as the enemy, and

50
00:05:36,080 --> 00:05:43,080
instead are creating all these enemies inside yourself, you've missed the point.

51
00:05:43,080 --> 00:05:49,240
So that's the broader context of nothing being more important than meditation, but

52
00:05:49,240 --> 00:05:54,160
it doesn't relate to the question of things like sleep, because a really good example

53
00:05:54,160 --> 00:06:02,120
is Jakobala, that there are many things that we're going to have to compare to meditation

54
00:06:02,120 --> 00:06:08,400
as being important, compared to enlightenment as being important, and realize that sometimes

55
00:06:08,400 --> 00:06:12,560
we have to make sacrifices.

56
00:06:12,560 --> 00:06:19,560
Jakobala, the story, goes that he didn't sleep for three months, and that wouldn't have

57
00:06:19,560 --> 00:06:29,200
been a problem except that he got sick, he had an eye sickness, and apparently had to

58
00:06:29,200 --> 00:06:33,080
take this medicine lying down, and because he didn't lie down he couldn't take the medicine

59
00:06:33,080 --> 00:06:40,560
in his eyes, and he ended up becoming blind.

60
00:06:40,560 --> 00:06:46,520
And at the moment he turned blind, they say, he became enlightened, and so he became

61
00:06:46,520 --> 00:06:57,120
known as Jakobala, Jakobala means one who guards their eyes, and it's a play on words,

62
00:06:57,120 --> 00:07:08,960
or whatever, there's a humor there, I think, or irony or whatever, because he didn't

63
00:07:08,960 --> 00:07:16,360
protect his eyes, he spent three months destroying his eyes, but the eye he protected was

64
00:07:16,360 --> 00:07:22,920
his dhamma eye, his eyes were opened at the end of three months, he became enlightened,

65
00:07:22,920 --> 00:07:29,000
he became enlightened, and with his eyes open at the same moment that he became irrevocably

66
00:07:29,000 --> 00:07:46,840
in line, there's a saying that goes, we give up, we give up a lot of our time to make

67
00:07:46,840 --> 00:07:54,880
money, we'll give up a lot of things like our leisure time, our free time, our meditation

68
00:07:54,880 --> 00:08:04,280
time even, when we give up our time to make money, but when it comes time to, when we're

69
00:08:04,280 --> 00:08:11,480
sick, if it comes down to losing an organ or something, we'll give up all our money

70
00:08:11,480 --> 00:08:26,360
to save an arm, a leg, an organ, our heart, our liver, that sort of thing, but when it comes

71
00:08:26,360 --> 00:08:33,960
to saving our life, we'll even give up an organ, give up an arm, give up a leg, if it's

72
00:08:33,960 --> 00:08:49,120
necessary to save our life, save the body as a whole, but, and here's perhaps where we

73
00:08:49,120 --> 00:08:56,520
fail, we shouldn't be willing to even give up our lives in favor of our mind and

74
00:08:56,520 --> 00:09:09,880
favor of our mental health, because a life lived unmindfully, a life of evil is a far less

75
00:09:09,880 --> 00:09:22,560
value and a death, and that is mindful, death in Buddhism isn't really all that consequential,

76
00:09:22,560 --> 00:09:31,960
meaning it's not final, generally, unless you're enlightened, you'll be born again, but

77
00:09:31,960 --> 00:09:35,680
if your mind is sullied, it doesn't matter how long you live or how long you put off

78
00:09:35,680 --> 00:09:46,760
death, if you continue to be unmindful, then you're just building up a worse and worse death

79
00:09:46,760 --> 00:09:55,160
in a worse and worse rebirth.

80
00:09:55,160 --> 00:10:01,720
So things like sleep, sometimes you'll have to sacrifice them, even your physical health,

81
00:10:01,720 --> 00:10:02,880
sometimes you have to sacrifice.

82
00:10:02,880 --> 00:10:08,160
I don't think it's all that common, I don't generally see meditators getting sick or so on,

83
00:10:08,160 --> 00:10:12,800
but there are always going to be things money is a very good example.

84
00:10:12,800 --> 00:10:18,720
So you think, well, if I'm mindful, I'll be able to be successful in my job, it's true,

85
00:10:18,720 --> 00:10:24,840
but you won't want to do it, you won't have any desire to engage in the cutthroat practices

86
00:10:24,840 --> 00:10:29,720
necessary to get raises and get good jobs, and you'll be much more inclined to quit

87
00:10:29,720 --> 00:10:40,400
your job and with a simple life, living on a subsistence income.

88
00:10:40,400 --> 00:10:45,520
So that's the no, really, that when it comes to things like sleep, there really should

89
00:10:45,520 --> 00:10:51,920
be no contest, even if it means that to some extent you may not be as healthy because

90
00:10:51,920 --> 00:11:02,120
there's an argument that sleep is good for your health, physically.

91
00:11:02,120 --> 00:11:08,480
The second answer may be, well, maybe things like sleep are good for your mental health as

92
00:11:08,480 --> 00:11:13,080
well, but certainly doctors will tell you that it's true.

93
00:11:13,080 --> 00:11:24,680
I think the issue with sleep is that it's hard to reconcile the experience of an intensive

94
00:11:24,680 --> 00:11:35,880
meditation practice with the results of scientific clinical studies on sleep.

95
00:11:35,880 --> 00:11:45,240
And I think the reason is because we're talking about a different state of mind if you're

96
00:11:45,240 --> 00:11:52,000
living a stressful life with a stressful job and a stressful family and so on, you're going

97
00:11:52,000 --> 00:11:58,200
to need more sleep, but you hear about old people who don't meditate, perhaps, who live

98
00:11:58,200 --> 00:12:05,120
simple lives, retired lives, who don't sleep a lot, and I would imagine it's just naturally

99
00:12:05,120 --> 00:12:08,400
because they don't need it, because it's a meditator.

100
00:12:08,400 --> 00:12:14,440
In the times when your mind is quite focused and peaceful and the idea of sleeping eight

101
00:12:14,440 --> 00:12:31,360
hours would be ludicrous and just seem so far out of what's beneficial or useful.

102
00:12:31,360 --> 00:12:37,600
But there are things that, in instances where you do have to take care of the body, for

103
00:12:37,600 --> 00:12:44,400
example, and where do things do take precedence over meditation only because you need them

104
00:12:44,400 --> 00:12:49,840
to support your meditation practice, you need them to support your spiritual growth.

105
00:12:49,840 --> 00:12:55,920
Food, for example, there was once an example of someone listening to the Buddha's teaching

106
00:12:55,920 --> 00:13:00,960
and before he gave the talk, he told people to give this man a poor beggar to give him

107
00:13:00,960 --> 00:13:02,400
some food.

108
00:13:02,400 --> 00:13:07,680
He told the monks and the laypeople to give him some food because there's no way he could

109
00:13:07,680 --> 00:13:11,880
listen to the teaching if he hadn't eaten.

110
00:13:11,880 --> 00:13:19,880
This is more related to the practical reality of individuals that if you don't have food,

111
00:13:19,880 --> 00:13:23,640
it's not that you couldn't be mindful, it's that you probably won't be, you'll be thinking

112
00:13:23,640 --> 00:13:26,440
out constantly about food.

113
00:13:26,440 --> 00:13:30,080
So there's often things you have to take care of sleep as another example, it's very hard

114
00:13:30,080 --> 00:13:39,800
to begin to learn how to even learn how to be mindful if you're constantly falling asleep.

115
00:13:39,800 --> 00:13:46,040
So this is why answering questions about, for example, this question, should I sleep

116
00:13:46,040 --> 00:13:47,040
or should I meditate?

117
00:13:47,040 --> 00:13:49,320
Suppose it was posed that way.

118
00:13:49,320 --> 00:13:55,680
A hard to answer because they depend very much on your circumstance and that shouldn't

119
00:13:55,680 --> 00:13:56,680
be that surprising.

120
00:13:56,680 --> 00:14:04,120
Obviously our lives are complicated, but another important aspect of that is that an important

121
00:14:04,120 --> 00:14:09,760
part of meditation practice is learning for yourself.

122
00:14:09,760 --> 00:14:11,200
What is the correct response?

123
00:14:11,200 --> 00:14:18,560
What is the correct relationship with your experience with your present experience?

124
00:14:18,560 --> 00:14:23,320
When I'm in this situation, what is the proper state?

125
00:14:23,320 --> 00:14:29,640
So being able to answer your own problems and questions is a very important part of the

126
00:14:29,640 --> 00:14:34,880
cultivation because I can tell you, yes, right now you should sleep, but that won't in

127
00:14:34,880 --> 00:14:42,480
any way be significant in helping you understand that you should sleep at certain times

128
00:14:42,480 --> 00:14:47,280
or that you should meditate at certain times.

129
00:14:47,280 --> 00:14:50,360
It can help to guide you if you get too far off.

130
00:14:50,360 --> 00:15:04,360
That's what a teacher is for to rein you in and say, look, sleep less or if I'm full.

131
00:15:04,360 --> 00:15:09,600
But it's important to understand, first of all, that the path to enlightenment or freedom

132
00:15:09,600 --> 00:15:13,360
from suffering is much more complicated than just meditate.

133
00:15:13,360 --> 00:15:19,440
It's going to be much because much of our personality is going to get in the way and

134
00:15:19,440 --> 00:15:28,200
twist things around and lead us to practice incorrectly and so on and so on.

135
00:15:28,200 --> 00:15:38,800
Learning to know when and how and balancing all of our different activities is important.

136
00:15:38,800 --> 00:15:42,120
So could sleep be better than meditation at certain times I think so.

137
00:15:42,120 --> 00:15:46,400
I think it might help you be more mindful.

138
00:15:46,400 --> 00:15:54,240
But at certain times no, at certain times even when you're very tired and you would find

139
00:15:54,240 --> 00:16:01,520
that meditation is quite beneficial and so ultimately you have to see the path to enlightenment

140
00:16:01,520 --> 00:16:04,320
is a learning process.

141
00:16:04,320 --> 00:16:05,320
It's not magic.

142
00:16:05,320 --> 00:16:13,040
It's not like, okay, I do meditation every day and after so many days I become enlightened.

143
00:16:13,040 --> 00:16:15,720
Meditation is a tool that's helping you to see yourself more clearly.

144
00:16:15,720 --> 00:16:20,200
So sleep is a part of you, part of yourself.

145
00:16:20,200 --> 00:16:24,960
So meditation, mindfulness will help you learn about that part of yourself.

146
00:16:24,960 --> 00:16:25,960
What is the nature of it?

147
00:16:25,960 --> 00:16:28,120
Where is it causing me problems?

148
00:16:28,120 --> 00:16:29,920
Where is it helping me?

149
00:16:29,920 --> 00:16:34,760
How am I progressing spiritually and what's getting in the way of my spiritual progress?

150
00:16:34,760 --> 00:16:45,000
That's the, it's called, we monks have this ability to direct and adjust your mind.

151
00:16:45,000 --> 00:16:48,360
So that's the maybe part.

152
00:16:48,360 --> 00:16:57,880
The no part, the yes part, where we can categorically say something is more important

153
00:16:57,880 --> 00:17:10,320
always than meditation is mindfulness and this might seem somewhat flippant but it's not

154
00:17:10,320 --> 00:17:12,280
really.

155
00:17:12,280 --> 00:17:17,880
The significance here is that we often put too much significance on meditation and not

156
00:17:17,880 --> 00:17:19,840
enough on mindfulness.

157
00:17:19,840 --> 00:17:25,440
So we do so many hours of meditation a day, minutes of meditation a day and that's supposed

158
00:17:25,440 --> 00:17:26,440
to be significant.

159
00:17:26,440 --> 00:17:28,560
It's really not.

160
00:17:28,560 --> 00:17:37,400
Minutes are not at all significant moments or significant and mindfulness works in moments.

161
00:17:37,400 --> 00:17:42,280
Very common example of how this works in practice is when we don't want to meditate.

162
00:17:42,280 --> 00:17:47,000
We develop an aversion towards it because it's stressful often having to face your mind

163
00:17:47,000 --> 00:17:53,400
having to taste the your own mental states.

164
00:17:53,400 --> 00:17:56,000
And so we develop an aversion to it.

165
00:17:56,000 --> 00:18:00,720
This is a common problem I have with meditators, especially in the online, the correspondence

166
00:18:00,720 --> 00:18:02,440
course we do.

167
00:18:02,440 --> 00:18:07,040
We do online where people meet once a week, I have people, people are doing this right

168
00:18:07,040 --> 00:18:17,280
now, they sign up on the internet and once a week we meet, we talk over the internet.

169
00:18:17,280 --> 00:18:21,040
But often they feel like they don't like, they aren't enjoying it, they don't want to

170
00:18:21,040 --> 00:18:22,040
meditate.

171
00:18:22,040 --> 00:18:31,920
It's hard to drag themselves through that and when that's the case it's much better

172
00:18:31,920 --> 00:18:36,840
for you to not concern yourself with actually meditating and you have to realize that

173
00:18:36,840 --> 00:18:46,200
you've stopped being mindful because the disliking is the object of meditation.

174
00:18:46,200 --> 00:18:50,600
The best way to deal with disliking the meditators to meditate on the disliking, forget

175
00:18:50,600 --> 00:18:56,400
about doing your walking and sitting, being mindful of disliking is going to be so much

176
00:18:56,400 --> 00:19:02,360
more beneficial and letting go of the concept because that's what happens.

177
00:19:02,360 --> 00:19:06,800
We build up meditation like a thing like suddenly this is meditation and we don't

178
00:19:06,800 --> 00:19:07,800
like it.

179
00:19:07,800 --> 00:19:10,720
You have to do that because otherwise where is meditation?

180
00:19:10,720 --> 00:19:17,280
If I sit like this, where is the meditation?

181
00:19:17,280 --> 00:19:21,320
Meditation is a concept that we build up in our mind, time is a concept, we say one hour

182
00:19:21,320 --> 00:19:29,160
or thirty minutes, ten minutes even, one minute even and then we apply, oh no, I have

183
00:19:29,160 --> 00:19:34,800
to sit so many more minutes, we apply our emotions to that, disliking it.

184
00:19:34,800 --> 00:19:46,280
Sleep is another good example here, so we think of it as a dichotomy either I do my meditation

185
00:19:46,280 --> 00:19:50,000
or I sleep and it really should never be like that, never.

186
00:19:50,000 --> 00:19:54,360
You should really, if you're intent on mindfulness, you should never go to sleep.

187
00:19:54,360 --> 00:20:01,400
It should always be about lying down mindfully and when you fall asleep you fall asleep,

188
00:20:01,400 --> 00:20:05,040
you're trying to be mindful instead of saying I'm going to lie down to sleep, I'm going

189
00:20:05,040 --> 00:20:11,680
to lie down mindfully and when you're lying, wanting to lie down, wanting to lie down,

190
00:20:11,680 --> 00:20:15,720
wanting to lie, and when you're lying down you can even watch the stomach rise and falling

191
00:20:15,720 --> 00:20:20,840
or you can just say we're lying and lying and you will fall asleep, it's not that hard

192
00:20:20,840 --> 00:20:28,120
to fall asleep, it's much easier when you're not stressed about wanting to fall asleep and

193
00:20:28,120 --> 00:20:29,120
I guess that's another part.

194
00:20:29,120 --> 00:20:33,480
I don't think that was related to the person asking the question, but it often is we're

195
00:20:33,480 --> 00:20:40,320
a person that really wants to sleep and is insomnia because of their feeling of stress

196
00:20:40,320 --> 00:20:49,720
and pressure over the need to sleep, that changes a lot when it's similar to the focusing

197
00:20:49,720 --> 00:20:54,360
on needing to meditate, needing to sleep is the same, you build up a stress associated

198
00:20:54,360 --> 00:20:58,880
with a concept of sleeping, when you can just lie down and be mindful and fall asleep

199
00:20:58,880 --> 00:21:03,480
quite quickly.

200
00:21:03,480 --> 00:21:08,360
So it's something more important than meditation, absolutely mindfulness is more important

201
00:21:08,360 --> 00:21:14,640
than what we might call meditation and all the qualities that come with mindfulness,

202
00:21:14,640 --> 00:21:21,720
Celas, samadhi, panya, morality, concentration, wisdom, these are what's important.

203
00:21:21,720 --> 00:21:27,560
How many minutes or hours or days or months or years, or lifetimes you do of meditation

204
00:21:27,560 --> 00:21:36,840
is inconsequential, ultimately not inconsequential because of the capacity when you're doing

205
00:21:36,840 --> 00:21:40,880
the meditation, when you're doing meditation, you're capacity to be mindful is much more

206
00:21:40,880 --> 00:21:42,680
important.

207
00:21:42,680 --> 00:21:49,120
But the format is the formula, the walking and the sitting is not the real work.

208
00:21:49,120 --> 00:21:54,080
That's much more important is that moments of mindfulness and so within an hour, how many

209
00:21:54,080 --> 00:21:58,600
moments of mindfulness there where that's where the real change comes because that's

210
00:21:58,600 --> 00:22:02,400
where the training comes, that's where the growth comes.

211
00:22:02,400 --> 00:22:10,040
So there you go, that's an answer to question of whether something is, is anything more

212
00:22:10,040 --> 00:22:11,960
important than meditation?

213
00:22:11,960 --> 00:22:19,840
I think that answers the sleep one fairly well, I can't answer the specific situation,

214
00:22:19,840 --> 00:22:24,320
like, should you sleep or when should you sleep or give you a regimen?

215
00:22:24,320 --> 00:22:28,000
And again, I think that goes back to the second part where you have to figure out for yourself

216
00:22:28,000 --> 00:22:33,920
and it's important as a part of your spiritual growth that you gain the wisdom to understand

217
00:22:33,920 --> 00:22:41,240
what's most important and when is it important to do this and important to do that?

218
00:22:41,240 --> 00:22:45,320
So there you go, thank you for the question and thank you for listening.

