1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhamapada.

2
00:00:05,000 --> 00:00:13,000
Today we continue on with 1st number 90, which reads as follows.

3
00:00:13,000 --> 00:00:31,000
Kata Dino, Viso Kasa, Vipumotasa, somebody, Sabagantha, Pajinasa, Parilaho, Nvijati, which means

4
00:00:31,000 --> 00:00:40,000
Kata Dino, one who has completed their journey, completed the going.

5
00:00:40,000 --> 00:00:49,000
Viso Kasa, one who is free from sorrow.

6
00:00:49,000 --> 00:00:56,000
Vipumotasa, somebody, one who is liberated everywhere.

7
00:00:56,000 --> 00:01:04,000
There is everywhere free, everywhere they go.

8
00:01:04,000 --> 00:01:16,000
Sabagantha, Pajinasa, one who has abandoned all fetters, all forms of clinging, every object of attachment.

9
00:01:16,000 --> 00:01:22,000
Parilaho, Nvijati, no fever can be found.

10
00:01:22,000 --> 00:01:44,000
Kata Dino, Viso Kasa, very beautiful little verse.

11
00:01:44,000 --> 00:01:47,000
It talks about the fever.

12
00:01:47,000 --> 00:01:51,000
Parilaho is often translated as fever.

13
00:01:51,000 --> 00:01:57,000
That's probably etymologically fairly correct, burning.

14
00:01:57,000 --> 00:02:07,000
But in this specific instance, because of the story and because of the context of the verse itself,

15
00:02:07,000 --> 00:02:12,000
it's not a fever, it's a mental fever, a mental burning.

16
00:02:12,000 --> 00:02:18,000
So we would translate it as distress or fever of Pajin.

17
00:02:18,000 --> 00:02:23,000
But literally, it means burning, I think.

18
00:02:23,000 --> 00:02:28,000
Parilaho becomes Parilaho.

19
00:02:28,000 --> 00:02:34,000
So the story, this was told in regards to a question posed by Divaka.

20
00:02:34,000 --> 00:02:36,000
It was a doctor.

21
00:02:36,000 --> 00:02:39,000
And the doctor who looked after the Buddha.

22
00:02:39,000 --> 00:02:42,000
And his story is not told here.

23
00:02:42,000 --> 00:02:48,000
I can't remember if it's actually told in the demo pod, I don't think so.

24
00:02:48,000 --> 00:02:50,000
Maybe in the Jatakai it occurs.

25
00:02:50,000 --> 00:02:56,000
But in the Vinaya, definitely you find his story.

26
00:02:56,000 --> 00:03:03,000
He was a master doctor, an exceptional medicine man.

27
00:03:03,000 --> 00:03:11,000
They say that he had the final task given to him by his teacher

28
00:03:11,000 --> 00:03:22,000
was to stake out a 16 kilometer by 16 kilometer square,

29
00:03:22,000 --> 00:03:27,000
a piece of land, which is a league, a square league,

30
00:03:27,000 --> 00:03:35,000
a new unit of land, and bring to his teacher every plant

31
00:03:35,000 --> 00:03:46,000
in the entire square league that had no medicinal properties.

32
00:03:46,000 --> 00:03:48,000
And this was the task.

33
00:03:48,000 --> 00:03:51,000
So he set out to accomplish this task.

34
00:03:51,000 --> 00:03:54,000
Came back sometime later and said to the teacher,

35
00:03:54,000 --> 00:03:56,000
there are no such plants.

36
00:03:56,000 --> 00:03:57,000
There are no plants in that 16.

37
00:03:57,000 --> 00:04:01,000
I couldn't find any plants that were without medicinal properties.

38
00:04:01,000 --> 00:04:05,000
And that's how his teacher knew that he passed because he was able

39
00:04:05,000 --> 00:04:10,000
to identify medicinal properties in all plants.

40
00:04:10,000 --> 00:04:11,000
It's the story.

41
00:04:11,000 --> 00:04:13,000
So he was exceptional.

42
00:04:13,000 --> 00:04:18,000
He got, he caused a little bit of trouble.

43
00:04:18,000 --> 00:04:20,000
Or he got into a little bit of trouble.

44
00:04:20,000 --> 00:04:21,000
Not of his own.

45
00:04:21,000 --> 00:04:22,000
Not on his own.

46
00:04:22,000 --> 00:04:25,000
He was actually an awesome, wonderful person.

47
00:04:25,000 --> 00:04:30,000
And he donated a mango grove to the Buddha.

48
00:04:30,000 --> 00:04:33,000
And you can still go and see the place that's supposed to be

49
00:04:33,000 --> 00:04:37,000
where this mango grove was right at the bottom of Vulture's Peak

50
00:04:37,000 --> 00:04:42,000
because Vulture's Peak was difficult to go up constantly to take care

51
00:04:42,000 --> 00:04:43,000
of the Buddha.

52
00:04:43,000 --> 00:04:46,000
So in order to make it easier for him to take care of the Buddha

53
00:04:46,000 --> 00:04:50,000
when the Buddha was sick, he would, and also to make it easier

54
00:04:50,000 --> 00:04:54,000
for the Buddha to go on arms round when the Buddha was ill.

55
00:04:54,000 --> 00:04:57,000
He gave him this mango grove at the bottom of the hill,

56
00:04:57,000 --> 00:05:01,000
at the bottom of the mountain.

57
00:05:01,000 --> 00:05:06,000
But as he was looking after the Buddha, he also took on the role

58
00:05:06,000 --> 00:05:09,000
of looking after the monks.

59
00:05:09,000 --> 00:05:15,000
And as a result of taking, being so much attention,

60
00:05:15,000 --> 00:05:18,000
taking so much time to look after the sangha,

61
00:05:18,000 --> 00:05:20,000
his other patients started to miss him.

62
00:05:20,000 --> 00:05:25,000
And so people who were sick would actually ordain his monks

63
00:05:25,000 --> 00:05:34,000
just to try and get this level of care.

64
00:05:34,000 --> 00:05:36,000
And this was actually the reason why the Buddha

65
00:05:36,000 --> 00:05:38,000
instated the role that someone who is sick,

66
00:05:38,000 --> 00:05:45,000
someone who has this sickness, has any terrible sickness,

67
00:05:45,000 --> 00:05:47,000
shouldn't be allowed to ordain.

68
00:05:47,000 --> 00:05:49,000
There are other reasons for it.

69
00:05:49,000 --> 00:05:52,000
It's problematic, but it's one of those things

70
00:05:52,000 --> 00:05:54,000
that leads people to ordain for the wrong reasons.

71
00:05:54,000 --> 00:05:57,000
And this happens in Buddhist countries

72
00:05:57,000 --> 00:05:59,000
where they become lax about this.

73
00:05:59,000 --> 00:06:02,000
And really, these people become a burden

74
00:06:02,000 --> 00:06:07,000
on the monastic sangha rather than actually promoting Buddhism

75
00:06:07,000 --> 00:06:10,000
and doing good things.

76
00:06:10,000 --> 00:06:13,000
There's a few stories about him.

77
00:06:13,000 --> 00:06:17,000
But this story actually concerns Devadatta

78
00:06:17,000 --> 00:06:19,000
in tangentially.

79
00:06:19,000 --> 00:06:22,000
Because the story goes that

80
00:06:22,000 --> 00:06:26,000
Devadatta was trying all sorts of ways to kill the Buddha.

81
00:06:26,000 --> 00:06:28,000
Devadatta was the Buddha's cousin,

82
00:06:28,000 --> 00:06:31,000
and he got upset and jealous of the Buddha,

83
00:06:31,000 --> 00:06:34,000
and he wanted to be the leader of the sangha,

84
00:06:34,000 --> 00:06:39,000
just kind of an all-around mean and nasty sort of fellow.

85
00:06:39,000 --> 00:06:46,000
And so he tried, he sent an elephant

86
00:06:46,000 --> 00:06:49,000
after the Buddha, and the elephant,

87
00:06:49,000 --> 00:06:51,000
the Buddha tamed the elephant.

88
00:06:51,000 --> 00:06:53,000
It was his mad elephant that was supposed to trample the Buddha

89
00:06:53,000 --> 00:06:57,000
and the Buddha tamed him with loving kindness.

90
00:06:57,000 --> 00:07:00,000
He sent some archers after the Buddhism,

91
00:07:00,000 --> 00:07:02,000
some mercenaries after the Buddha,

92
00:07:02,000 --> 00:07:05,000
and they all converted to become monks.

93
00:07:05,000 --> 00:07:08,000
And finally, he just got fed up,

94
00:07:08,000 --> 00:07:11,000
and as the Buddha was coming down from Vulture's peak,

95
00:07:11,000 --> 00:07:13,000
he dropped a rock,

96
00:07:13,000 --> 00:07:16,000
or a big boulder on the Buddha, trying to crush him

97
00:07:16,000 --> 00:07:20,000
from up on high.

98
00:07:20,000 --> 00:07:22,000
And as the rock was falling down,

99
00:07:22,000 --> 00:07:25,000
it hit some sort of outcropping,

100
00:07:25,000 --> 00:07:27,000
of course, because you can't kill the Buddha.

101
00:07:27,000 --> 00:07:28,000
It just doesn't happen.

102
00:07:28,000 --> 00:07:29,000
This karma is too good.

103
00:07:29,000 --> 00:07:31,000
But a little splinter,

104
00:07:31,000 --> 00:07:34,000
a small splinter broke off

105
00:07:34,000 --> 00:07:35,000
to the rest of the rock,

106
00:07:35,000 --> 00:07:36,000
veered out of the way,

107
00:07:36,000 --> 00:07:38,000
and didn't come close to the Buddha,

108
00:07:38,000 --> 00:07:41,000
but a small splinter came and hit the Buddha's foot.

109
00:07:41,000 --> 00:07:44,000
And Jiwaka looked after the Buddha,

110
00:07:44,000 --> 00:07:46,000
the Buddha lay down,

111
00:07:46,000 --> 00:07:48,000
and Jiwaka bandaged him up.

112
00:07:48,000 --> 00:07:54,000
I put some polters on the wound.

113
00:07:54,000 --> 00:07:56,000
I think the commentary has tried to say

114
00:07:56,000 --> 00:07:58,000
that he didn't actually bleed,

115
00:07:58,000 --> 00:08:00,000
because it's impossible to make the Buddha bleed,

116
00:08:00,000 --> 00:08:05,000
but it's about the translation as to what exactly is meant to happen anyway.

117
00:08:05,000 --> 00:08:12,000
The Buddha was injured,

118
00:08:12,000 --> 00:08:15,000
and he put some herbs on it,

119
00:08:15,000 --> 00:08:16,000
something strong that would take the pain away

120
00:08:16,000 --> 00:08:17,000
and make the swelling go down,

121
00:08:17,000 --> 00:08:19,000
but it was quite strong.

122
00:08:19,000 --> 00:08:21,000
And then Jiwaka had to go into the city

123
00:08:21,000 --> 00:08:25,000
to look after his patients in the city.

124
00:08:25,000 --> 00:08:28,000
And he was late.

125
00:08:28,000 --> 00:08:30,000
I was looking after some rich person,

126
00:08:30,000 --> 00:08:31,000
or maybe even the king,

127
00:08:31,000 --> 00:08:32,000
and he was late coming back,

128
00:08:32,000 --> 00:08:34,000
and they closed the gates to the city

129
00:08:34,000 --> 00:08:36,000
before he could get out.

130
00:08:36,000 --> 00:08:38,000
And so he had to stay all night in the city,

131
00:08:38,000 --> 00:08:40,000
and he was worried all night,

132
00:08:40,000 --> 00:08:42,000
and concerned,

133
00:08:42,000 --> 00:08:44,000
because he had to go back

134
00:08:44,000 --> 00:08:48,000
and take this polters off of the Buddha's ankle,

135
00:08:48,000 --> 00:08:50,000
or else it would cause harm.

136
00:08:50,000 --> 00:08:52,000
It would actually cause harm to the Buddha,

137
00:08:52,000 --> 00:08:58,000
and so he was unable to let the Buddha know.

138
00:08:58,000 --> 00:09:02,000
Fortunately, that's what it's the benefit of having

139
00:09:02,000 --> 00:09:05,000
all sorts of supernatural powers.

140
00:09:05,000 --> 00:09:07,000
The Buddha was able to discern

141
00:09:07,000 --> 00:09:10,000
that it was time to take the polters off

142
00:09:10,000 --> 00:09:13,000
and had Ananda help him take it off,

143
00:09:13,000 --> 00:09:19,000
and waited there for Jiwaka to come.

144
00:09:19,000 --> 00:09:21,000
So no harm done.

145
00:09:21,000 --> 00:09:23,000
But harm was done to Jiwaka,

146
00:09:23,000 --> 00:09:24,000
who was worrying and fretting,

147
00:09:24,000 --> 00:09:26,000
who was distressed.

148
00:09:26,000 --> 00:09:29,000
Now, Jiwaka was actually a sotapana.

149
00:09:29,000 --> 00:09:31,000
I think at this time he was already a sotapana,

150
00:09:31,000 --> 00:09:33,000
I'm not sure of the actual timeline.

151
00:09:33,000 --> 00:09:35,000
You have to look deeper into that.

152
00:09:35,000 --> 00:09:38,000
But I think he was already a sotapana at this point.

153
00:09:38,000 --> 00:09:41,000
So he was already an area of Bughalah,

154
00:09:41,000 --> 00:09:44,000
but he was still subject to distress.

155
00:09:44,000 --> 00:09:50,000
And so he friended about this for the long time

156
00:09:50,000 --> 00:09:53,000
that he was kept in the city.

157
00:09:53,000 --> 00:09:55,000
And finally, when the gates opened in the morning,

158
00:09:55,000 --> 00:09:58,000
he ran over, ran,

159
00:09:58,000 --> 00:10:03,000
sprinted his way over to Vulture's Peak,

160
00:10:03,000 --> 00:10:06,000
and came to the Buddha and asked him,

161
00:10:06,000 --> 00:10:08,000
I'm sorry, then, an herbal surah,

162
00:10:08,000 --> 00:10:10,000
that I wasn't able to come,

163
00:10:10,000 --> 00:10:13,000
were you distressed by the pain?

164
00:10:13,000 --> 00:10:16,000
Were you distressed by your illness?

165
00:10:16,000 --> 00:10:18,000
And that's where this verse comes from.

166
00:10:18,000 --> 00:10:21,000
Jiwaka asked him the simple question,

167
00:10:21,000 --> 00:10:23,000
and the Buddha said,

168
00:10:23,000 --> 00:10:28,000
for one who has gone to the end,

169
00:10:28,000 --> 00:10:32,000
who is free from surah,

170
00:10:32,000 --> 00:10:35,000
one who is free everywhere,

171
00:10:35,000 --> 00:10:39,000
gone to live, who has become liberated everywhere,

172
00:10:39,000 --> 00:10:43,000
who has abandoned all ties,

173
00:10:43,000 --> 00:10:46,000
Pernilahoh, Naveetati.

174
00:10:46,000 --> 00:10:49,000
There is no fever to be found.

175
00:10:49,000 --> 00:10:52,000
No distress to be found.

176
00:10:52,000 --> 00:10:55,000
The word fever is too ambiguous,

177
00:10:55,000 --> 00:10:58,000
but when it never becomes distress.

178
00:10:58,000 --> 00:11:00,000
So this was, I think last night,

179
00:11:00,000 --> 00:11:03,000
we had the question whether an arahand can suffer,

180
00:11:03,000 --> 00:11:07,000
whether it can actually be called suffering,

181
00:11:07,000 --> 00:11:09,000
when an arahand feels pain,

182
00:11:09,000 --> 00:11:10,000
because they don't have,

183
00:11:10,000 --> 00:11:12,000
and this is what they don't have.

184
00:11:12,000 --> 00:11:14,000
The translation that we've got in the book actually

185
00:11:14,000 --> 00:11:16,000
says it translates,

186
00:11:16,000 --> 00:11:17,000
but it's suffering,

187
00:11:17,000 --> 00:11:20,000
but that's probably not accurate.

188
00:11:20,000 --> 00:11:23,000
It's again too ambiguous,

189
00:11:23,000 --> 00:11:24,000
too general.

190
00:11:24,000 --> 00:11:26,000
The specific meaning is,

191
00:11:26,000 --> 00:11:29,000
is burning up,

192
00:11:29,000 --> 00:11:31,000
so it's often referring to the body,

193
00:11:31,000 --> 00:11:34,000
but here it means burning up in the mind,

194
00:11:34,000 --> 00:11:37,000
where you distressed.

195
00:11:37,000 --> 00:11:39,000
Did it distress you?

196
00:11:39,000 --> 00:11:40,000
Upset?

197
00:11:40,000 --> 00:11:42,000
Did it upset you?

198
00:11:42,000 --> 00:11:43,000
Were you upset by it?

199
00:11:43,000 --> 00:11:45,000
Were you distressed by it?

200
00:11:45,000 --> 00:11:47,000
Were you vexed by it?

201
00:11:47,000 --> 00:11:56,000
And the Buddha said there is no vexation for one who has become free.

202
00:11:56,000 --> 00:11:58,000
And so that's the answer to that question

203
00:11:58,000 --> 00:12:02,000
that they do actually feel dukawed in the body,

204
00:12:02,000 --> 00:12:04,000
but they're not vexed by it.

205
00:12:04,000 --> 00:12:07,000
The very important point that all of us as meditators,

206
00:12:07,000 --> 00:12:09,000
hopefully by this time have heard

207
00:12:09,000 --> 00:12:11,000
or have come to be familiar with,

208
00:12:11,000 --> 00:12:13,000
but it's a point that has to be made,

209
00:12:13,000 --> 00:12:16,000
especially for people who are new to the meditation.

210
00:12:16,000 --> 00:12:20,000
The difference between not only physical suffering,

211
00:12:20,000 --> 00:12:23,000
but external conditions,

212
00:12:23,000 --> 00:12:27,000
like the state of Giwaka, who was stuck in the city,

213
00:12:27,000 --> 00:12:31,000
was incredibly vexed and disturbed and distressed.

214
00:12:31,000 --> 00:12:33,000
He had a good heart,

215
00:12:33,000 --> 00:12:36,000
but he wasn't helping the Buddha by being vexed and distressed,

216
00:12:36,000 --> 00:12:38,000
by being upset.

217
00:12:38,000 --> 00:12:43,000
And in fact, he was simply doing damage to his own mind.

218
00:12:43,000 --> 00:12:46,000
And so to some extent,

219
00:12:46,000 --> 00:12:50,000
the Buddha was using this as a lesson to Giwaka as well,

220
00:12:50,000 --> 00:12:53,000
not only to reassure him,

221
00:12:53,000 --> 00:12:56,000
but also to remind him, not to get upset.

222
00:12:56,000 --> 00:12:59,000
I wasn't upset. Why did you get upset?

223
00:13:02,000 --> 00:13:05,000
And so this is because of the difference between the physical,

224
00:13:05,000 --> 00:13:08,000
the external and our reactions.

225
00:13:08,000 --> 00:13:10,000
Pain doesn't have to be a bad thing.

226
00:13:10,000 --> 00:13:14,000
Ex crocheting pain doesn't have to be a bad thing.

227
00:13:14,000 --> 00:13:18,000
Situations don't have to cause us anger and frustration and upset

228
00:13:18,000 --> 00:13:23,000
or boredom or worry or fear.

229
00:13:23,000 --> 00:13:28,000
When people do things to us, when others hurt us,

230
00:13:28,000 --> 00:13:33,000
we do the worst thing when we respond with anger.

231
00:13:33,000 --> 00:13:37,000
When someone gets angry at us and we respond with anger,

232
00:13:37,000 --> 00:13:40,000
we are the worst.

233
00:13:40,000 --> 00:13:46,000
We are creating the worst evil, what have said.

234
00:13:48,000 --> 00:13:53,000
But this is a clear indication of the difference between

235
00:13:53,000 --> 00:13:58,000
our experiences and our reactions to them.

236
00:13:58,000 --> 00:13:59,000
So what do we have?

237
00:13:59,000 --> 00:14:02,000
One who has gone to the end means,

238
00:14:02,000 --> 00:14:03,000
actually I don't know,

239
00:14:03,000 --> 00:14:07,000
we've got Adino, it's translated as one who has gone to the end of the journey.

240
00:14:07,000 --> 00:14:09,000
Well, that's what they say here,

241
00:14:09,000 --> 00:14:12,000
but let's see exactly what it means.

242
00:14:12,000 --> 00:14:14,000
Adino.

243
00:14:14,000 --> 00:14:19,000
One who has gone the distance.

244
00:14:19,000 --> 00:14:22,000
So who has completed the path.

245
00:14:22,000 --> 00:14:26,000
I think it meditators asking how you know how far you're progressing

246
00:14:26,000 --> 00:14:29,000
and it's all this idea of the path.

247
00:14:29,000 --> 00:14:33,000
I'm often cynical about whether we should really focus too much

248
00:14:33,000 --> 00:14:35,000
on the idea of a path.

249
00:14:35,000 --> 00:14:40,000
It's misleading, I think, you know.

250
00:14:40,000 --> 00:14:44,000
They could find to talk about the idea of a path,

251
00:14:44,000 --> 00:14:47,000
but when we are concerned about our progress,

252
00:14:47,000 --> 00:14:49,000
it can become quite obsessive.

253
00:14:49,000 --> 00:14:51,000
As we're worried about where we are,

254
00:14:51,000 --> 00:14:56,000
wondering when we're going to get there, that kind of thing.

255
00:14:56,000 --> 00:15:01,000
On the other hand, having the idea of the path is a reminder

256
00:15:01,000 --> 00:15:05,000
that we can't be complacent, that we do have to walk and journey.

257
00:15:05,000 --> 00:15:09,000
But it's important as a teacher and as a meditator

258
00:15:09,000 --> 00:15:12,000
to not be too focused on progress.

259
00:15:12,000 --> 00:15:17,000
So this idea of the path or the journey is poetic

260
00:15:17,000 --> 00:15:20,000
and it's useful to know that there is a journey.

261
00:15:20,000 --> 00:15:22,000
But when you're walking a journey,

262
00:15:22,000 --> 00:15:24,000
you don't want to be the person in the backseat saying,

263
00:15:24,000 --> 00:15:27,000
are we there yet, are we there yet?

264
00:15:27,000 --> 00:15:31,000
That part of the mind should be silenced.

265
00:15:31,000 --> 00:15:33,000
The walking has to continue.

266
00:15:33,000 --> 00:15:36,000
All you have to do is put one foot in front of the other.

267
00:15:36,000 --> 00:15:40,000
You know that there's a path and you follow it.

268
00:15:40,000 --> 00:15:42,000
It's much simpler than we think.

269
00:15:42,000 --> 00:15:44,000
We think of it as being some kind of,

270
00:15:44,000 --> 00:15:49,000
like a paved road with road signs telling you

271
00:15:49,000 --> 00:15:51,000
10 kilometers left or something like that,

272
00:15:51,000 --> 00:15:53,000
but it's not really.

273
00:15:53,000 --> 00:15:57,000
A path we've never gone and the path that has no clear

274
00:15:57,000 --> 00:16:01,000
signs that we can distinguish because we've never,

275
00:16:01,000 --> 00:16:06,000
you know, we have nothing, no frame of reference.

276
00:16:06,000 --> 00:16:08,000
So our job is yes to follow the path,

277
00:16:08,000 --> 00:16:12,000
but that simply means putting one foot in front of the other.

278
00:16:12,000 --> 00:16:15,000
Metaphorically speaking.

279
00:16:15,000 --> 00:16:16,000
We soak as that.

280
00:16:16,000 --> 00:16:18,000
So in the light and being has no soak,

281
00:16:18,000 --> 00:16:20,000
no sadness, no sorrow.

282
00:16:20,000 --> 00:16:23,000
They don't long for anything.

283
00:16:23,000 --> 00:16:29,000
They don't pine away at loss.

284
00:16:29,000 --> 00:16:33,000
They don't worry about losing the things that they have.

285
00:16:33,000 --> 00:16:36,000
No soak, no sadness.

286
00:16:36,000 --> 00:16:39,000
We promote us as somebody in all directions,

287
00:16:39,000 --> 00:16:42,000
on all sides everywhere.

288
00:16:42,000 --> 00:16:48,000
We promote free, liberated,

289
00:16:48,000 --> 00:16:51,000
completely liberated.

290
00:16:51,000 --> 00:16:55,000
Saba Gantapahinas having abandoned or destroyed

291
00:16:55,000 --> 00:17:00,000
or gotten rid of all

292
00:17:00,000 --> 00:17:07,000
vines or ties, severed all ties.

293
00:17:07,000 --> 00:17:12,000
So people talk a lot about detachment,

294
00:17:12,000 --> 00:17:17,000
how Buddhism is a clear indication

295
00:17:17,000 --> 00:17:22,000
of how Buddhism does encourage

296
00:17:22,000 --> 00:17:27,000
the idea of detachment.

297
00:17:27,000 --> 00:17:30,000
But people get the wrong idea that it somehow translates

298
00:17:30,000 --> 00:17:33,000
into a sense of

299
00:17:33,000 --> 00:17:36,000
or a state of zombie-like,

300
00:17:36,000 --> 00:17:39,000
nothingness or meaninglessness.

301
00:17:39,000 --> 00:17:43,000
All it means is to not cling to things as they go by

302
00:17:43,000 --> 00:17:49,000
because the problem is we have this idea that things exist

303
00:17:49,000 --> 00:17:54,000
constantly or in some stable form.

304
00:17:54,000 --> 00:17:57,000
And so we're talking about shying away from them.

305
00:17:57,000 --> 00:17:59,000
That people think non-attachment means

306
00:17:59,000 --> 00:18:00,000
it's there.

307
00:18:00,000 --> 00:18:01,000
Don't go near it.

308
00:18:01,000 --> 00:18:02,000
But that's not it.

309
00:18:02,000 --> 00:18:04,000
It's not there actually.

310
00:18:04,000 --> 00:18:05,000
What is there is a moment.

311
00:18:05,000 --> 00:18:06,000
And that arises.

312
00:18:06,000 --> 00:18:07,000
And see, that's fine.

313
00:18:07,000 --> 00:18:11,000
Be as close and as clearly aware of that as you can.

314
00:18:11,000 --> 00:18:14,000
That's the point.

315
00:18:14,000 --> 00:18:17,000
But when it's gone, don't go with it.

316
00:18:17,000 --> 00:18:20,000
Don't let it pull you into the past.

317
00:18:20,000 --> 00:18:24,000
Don't let thoughts about the future pull you into the future.

318
00:18:24,000 --> 00:18:26,000
You end up like this pulling being pulled

319
00:18:26,000 --> 00:18:29,000
to both sides past and future.

320
00:18:29,000 --> 00:18:32,000
And you're never really here and now.

321
00:18:32,000 --> 00:18:34,000
That's what non-attachment means.

322
00:18:34,000 --> 00:18:39,000
It would make sense if there were things here that were stable

323
00:18:39,000 --> 00:18:43,000
to attach to them because you could depend upon them.

324
00:18:43,000 --> 00:18:45,000
But there's nothing like that.

325
00:18:45,000 --> 00:18:48,000
Nothing is of that sort.

326
00:18:48,000 --> 00:18:52,000
Everything arises and ceases comes and goes uncertain.

327
00:18:52,000 --> 00:18:54,000
Even all the possessions that we have,

328
00:18:54,000 --> 00:18:57,000
that we think we have constantly throughout our lives,

329
00:18:57,000 --> 00:19:02,000
the people who are with us.

330
00:19:02,000 --> 00:19:05,000
The reason we're so sad is because we take them as

331
00:19:05,000 --> 00:19:08,000
some stable entity that can be controlled.

332
00:19:08,000 --> 00:19:12,000
And that is long lasting and so on.

333
00:19:12,000 --> 00:19:17,000
And so we bind ourselves to them with this attachment.

334
00:19:17,000 --> 00:19:20,000
Non-attachment or detachment.

335
00:19:20,000 --> 00:19:23,000
Non-attachment is a better way of phrasing it.

336
00:19:23,000 --> 00:19:26,000
Non-attachment means experiencing.

337
00:19:26,000 --> 00:19:28,000
But experiencing as experience.

338
00:19:28,000 --> 00:19:31,000
Seeing people as individual experiences.

339
00:19:31,000 --> 00:19:33,000
Seeing things as individual experiences.

340
00:19:33,000 --> 00:19:35,000
Because that's what they are.

341
00:19:35,000 --> 00:19:37,000
That's what you really have.

342
00:19:37,000 --> 00:19:39,000
You say you have a car when you don't see it,

343
00:19:39,000 --> 00:19:42,000
when you don't hear it, when you're not in front of it,

344
00:19:42,000 --> 00:19:44,000
when it's not in front of you.

345
00:19:44,000 --> 00:19:47,000
How can you say that you even have people in your life

346
00:19:47,000 --> 00:19:49,000
when they're not here and now?

347
00:19:49,000 --> 00:19:51,000
All you have is a thought that arises and ceases,

348
00:19:51,000 --> 00:19:53,000
even when they're with you.

349
00:19:53,000 --> 00:19:57,000
All you have is seeing, hearing, smelling, tasting, feeling

350
00:19:57,000 --> 00:20:00,000
and thinking.

351
00:20:00,000 --> 00:20:05,000
So non-attachment just means seeing things as they are

352
00:20:05,000 --> 00:20:08,000
and being with things as and being with

353
00:20:08,000 --> 00:20:10,000
there's people around you, there's people around you.

354
00:20:10,000 --> 00:20:13,000
There's no one around you, there's no one around you.

355
00:20:13,000 --> 00:20:16,000
It's not pining away after something that isn't there

356
00:20:16,000 --> 00:20:20,000
or clinging to something that is.

357
00:20:20,000 --> 00:20:25,000
For such a person who is of this sort, there is no fever.

358
00:20:25,000 --> 00:20:28,000
So what it means is they have no likes or dislikes,

359
00:20:28,000 --> 00:20:32,000
they have no vexation and no upset

360
00:20:32,000 --> 00:20:34,000
when things don't go as expected

361
00:20:34,000 --> 00:20:37,000
because they have no expectations.

362
00:20:37,000 --> 00:20:40,000
They're perfectly at peace and perfectly flexible.

363
00:20:40,000 --> 00:20:44,000
I think flexible is a very important aspect.

364
00:20:44,000 --> 00:20:49,000
It's just naturally flexible because they have no preference.

365
00:20:49,000 --> 00:20:54,000
They are content the way things are, however they are.

366
00:20:54,000 --> 00:20:58,000
So that's the dumbapada for tonight.

367
00:20:58,000 --> 00:21:03,000
Thank you all for tuning in and keep practicing.

368
00:21:03,000 --> 00:21:05,000
And be well.

