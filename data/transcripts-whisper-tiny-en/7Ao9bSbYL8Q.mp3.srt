1
00:00:30,000 --> 00:00:49,600
Okay, good evening, everyone, welcome to evening dhamma.

2
00:00:49,600 --> 00:01:09,200
Alright, we're looking at the sabasa asupta again, the second of the seven means of overcoming the defilements in the mind.

3
00:01:09,200 --> 00:01:16,800
And so again, all seven of these means come together to form a comprehensive practice, which

4
00:01:16,800 --> 00:01:23,920
maybe at the end we can just summarize again, recap. But the first one was dasana, which is

5
00:01:25,440 --> 00:01:36,240
what we're doing to come to see the truth, to see reality, to see the folly of our ways and

6
00:01:36,240 --> 00:01:49,840
to see the danger and the disadvantages of clinging to things that ultimately lead to more suffering.

7
00:01:55,280 --> 00:02:00,480
And to see freedom really, to come to see what it means to be free from suffering.

8
00:02:00,480 --> 00:02:10,000
Somewhere, the second one is somewhere which translate as restraint or restraining.

9
00:02:13,520 --> 00:02:26,160
So these are the, or this is the abandoning of the asua through restraint.

10
00:02:26,160 --> 00:02:38,560
The restraint is an interesting word. It kind of implies a sort of

11
00:02:44,560 --> 00:02:51,840
wanting to do something but not doing it. You restrain yourself against your will generally.

12
00:02:51,840 --> 00:02:58,000
It's not a bad word. I think it's not a bad translation, but we have to be careful.

13
00:02:59,440 --> 00:03:06,720
There are five kinds of somewhere, five kinds of restraint according to the texts,

14
00:03:09,040 --> 00:03:15,440
and the commentaries, I think, see the somewhere through, through morality.

15
00:03:15,440 --> 00:03:24,480
This would be, this would be against your will, against your instinct. You want to do something,

16
00:03:24,480 --> 00:03:37,280
you want to kill, you want to steal, you want to lie and cheat, but through the existence of

17
00:03:37,280 --> 00:03:46,000
precepts, through the existence of rules and religion really, you stop yourself.

18
00:03:48,960 --> 00:03:53,440
And you can think about why that is, maybe it's because you have faith in the Buddha,

19
00:03:54,800 --> 00:04:00,800
maybe it's because you have faith in the practice and

20
00:04:00,800 --> 00:04:10,320
maybe it's because you have seen some truth and

21
00:04:13,760 --> 00:04:24,960
because of that, truth gives you confidence and makes you comfortable keeping the rules.

22
00:04:24,960 --> 00:04:32,240
It could just be because you're told, hey, if you want to progress in this teaching,

23
00:04:32,240 --> 00:04:39,120
you have to keep these rules. And so you want to give this teaching a shot, so you keep the rules.

24
00:04:40,720 --> 00:04:52,000
See, that is a fairly poor way of restraining, but it is practically effective in the short term.

25
00:04:52,000 --> 00:05:00,080
And in this context, we're talking about restraint of the senses, so morality does help

26
00:05:00,080 --> 00:05:12,640
you restrain your senses in a very roundabout way. So if you keep yourself from getting caught up

27
00:05:12,640 --> 00:05:23,600
in unwholesomeness, while then you're restraining the senses in a sense, that you're

28
00:05:26,160 --> 00:05:39,200
not going to experience a lot of distracting and problematic states.

29
00:05:39,200 --> 00:05:49,520
If you don't, if you don't, well, the five precepts may be a little bit extreme, but if you keep

30
00:05:49,520 --> 00:05:56,720
the eight precepts, for example, then you don't engage in entertainment. Because you don't engage

31
00:05:56,720 --> 00:06:03,200
in entertainment, you're actually restraining your senses from seeing very pleasant things and hearing

32
00:06:03,200 --> 00:06:11,360
very pleasant sounds. So it's roundabout, but it is a sort of a restraint. When you take only to

33
00:06:11,360 --> 00:06:18,080
eat in the morning, then you're restraining the tongue, right, the pleasure that comes from taste,

34
00:06:19,040 --> 00:06:26,400
you're restraining that. And you can see this quite clearly as you meditate in the evening,

35
00:06:26,400 --> 00:06:34,480
feel hungry or have cravings for delicious food, you can see how that sort of restraint works.

36
00:06:35,440 --> 00:06:41,120
It's sort of withdrawal that we go through in the meditation course.

37
00:06:44,240 --> 00:06:48,800
And you start to see how addicted we are through the senses,

38
00:06:48,800 --> 00:06:58,080
the things that really don't actually satisfy us. Because then in the morning, you get the food

39
00:06:58,080 --> 00:07:05,760
and you realize it's not really that, you know, it's kind of a, it's a fever the Buddha says here.

40
00:07:05,760 --> 00:07:20,320
Parilaha means fever or heat, or just distress burning really, and literally it means burning.

41
00:07:23,200 --> 00:07:26,400
We burn, we burn with a desire, right?

42
00:07:26,400 --> 00:07:40,240
So see the samwara, then there's sati samwara is the real one. Sati samwara is,

43
00:07:41,520 --> 00:07:49,680
wasati samwara is only temporary as well, right? Sati samwara is what we're doing during the

44
00:07:49,680 --> 00:07:54,640
meditation practice. It's one part of our practice, one aspect of it. Because you need to

45
00:07:54,640 --> 00:08:04,720
restrain yourself in order to see clearly. It's like the focusing. If you just allow your senses to

46
00:08:04,720 --> 00:08:12,960
go free, then you get clouded by desire and diversion. When you say to yourself seeing, seeing,

47
00:08:15,360 --> 00:08:22,320
you're you're restraining the mind. It's a different kind of a restraint of the senses,

48
00:08:22,320 --> 00:08:28,240
because here you're not concerned with what the ICs. You can see all the beautiful things

49
00:08:28,240 --> 00:08:32,080
and all the terrible things you like.

50
00:08:37,360 --> 00:08:42,960
You're restraining through filtering out the defilements. So when you see the beautiful thing,

51
00:08:42,960 --> 00:08:53,200
it's just seeing. It's a different kind of restraint. It's like putting a muzzle on a dog

52
00:08:54,320 --> 00:09:01,440
and take the dog out, but it's muzzle or it's like declawing a cat, which is apparently very painful,

53
00:09:01,440 --> 00:09:07,200
so don't do that. Well, it's interesting, if you did, if you could declaw a cat,

54
00:09:07,200 --> 00:09:12,720
it might make the cat more wholesome. I don't know about that whole thing.

55
00:09:14,160 --> 00:09:21,040
But if you declaw the cat, then the cat can do all the things a cat does and doesn't get into

56
00:09:21,040 --> 00:09:27,760
as much mischief. It gets into the mischief, but doesn't, right? You see, you're taking the

57
00:09:27,760 --> 00:09:42,080
poison, you're taking away the problem. So such as somewhat, this is a big part of what we do,

58
00:09:43,040 --> 00:09:48,640
it's important because it allows you to see your experiences clearly.

59
00:09:50,080 --> 00:09:53,280
It allows you to see the things that you cling to objectively.

60
00:09:53,280 --> 00:10:04,720
When you do that, when you accomplish that, the third one is yana samura, which comes from sati.

61
00:10:06,320 --> 00:10:11,520
And this isn't theoretical or something you have to believe. This is really an incredible

62
00:10:11,520 --> 00:10:17,840
truth that when you do restrain the mind through see that and sati,

63
00:10:17,840 --> 00:10:28,080
their rise is yana, their rise is knowledge. Come to see some things about your situation that

64
00:10:28,080 --> 00:10:33,040
we're not clear before, that the things that you cling to are not worth clinging to.

65
00:10:34,960 --> 00:10:40,080
That these experiences of seeing and hearing and smelling, really anything that we

66
00:10:40,960 --> 00:10:47,520
like or want, when we start to be objective about it, we come to see they're not worth liking

67
00:10:47,520 --> 00:10:52,160
or wanting or that we dislike, right? Pain, for example.

68
00:10:56,400 --> 00:11:06,800
All of the thoughts and memories or plans and dreams that would vex and burn, consume our minds

69
00:11:06,800 --> 00:11:17,760
with the burning of passion and aversion, the fires of defilement.

70
00:11:19,600 --> 00:11:24,000
And you start to see them objectively, then you're free yourself from this.

71
00:11:26,720 --> 00:11:33,280
So yana samura, the knowledge of that these things are not worth clinging to, the knowledge

72
00:11:33,280 --> 00:11:42,400
that our defilements are just burning. It's sort of a restraint because it's the most important

73
00:11:42,400 --> 00:11:48,720
one. I mean, really, yana samura is a true restraint. It's the restraint of an enlightened

74
00:11:48,720 --> 00:11:57,920
being or just a meditator, really. When you start to see clearly, then there's no question about

75
00:11:57,920 --> 00:12:05,280
whether you'll engage in problematic activities, whether you'll get caught up in the senses,

76
00:12:07,040 --> 00:12:12,480
whether you'll cause yourself suffering, cause suffering for others, because knowledge

77
00:12:13,840 --> 00:12:17,360
wisdom has shown you. Hey, that only leads to suffering.

78
00:12:20,800 --> 00:12:25,760
It's a wonderful thing that true knowledge, true wisdom does for you from

79
00:12:25,760 --> 00:12:33,760
it does make you disinclined to cause suffering for yourselves or others.

80
00:12:36,480 --> 00:12:43,680
So sati samura, sati samura, yana samura, kanti samura is another one.

81
00:12:44,560 --> 00:12:54,240
kanti samura, kanti samura, these are two other kinds of restraints. So kanti is

82
00:12:54,240 --> 00:13:01,760
restraint through patience. It's another, I think you could call it another important aspect

83
00:13:01,760 --> 00:13:08,800
of the practice. I mean, it really, it relies upon mindfulness, but mindfulness is a sort of

84
00:13:09,360 --> 00:13:17,760
patience. I guess how you differentiated here is this sense of putting up with something.

85
00:13:17,760 --> 00:13:25,440
I think it comes very much in a company and by mindfulness. If you really want to have patience,

86
00:13:25,440 --> 00:13:34,800
true patience, you need mindfulness. But the sense of seeing things that are attractive and not

87
00:13:34,800 --> 00:13:43,280
being attracted to them, that's patience. I mean, I think it may be in a more simple way, it just

88
00:13:43,280 --> 00:13:48,320
means you want something, but you don't go after it. You may be patient with the wanting.

89
00:13:50,480 --> 00:13:55,200
And that maybe relates a lot more to being mindful of reactions.

90
00:13:57,280 --> 00:14:03,760
So you see something and then there's the liking of the seeing. You see something beautiful,

91
00:14:03,760 --> 00:14:07,760
you like it. But being mindful of the liking, that's a sort of a patience.

92
00:14:07,760 --> 00:14:14,720
It's a restraint of the senses in a different way in that you're not actually restraining the

93
00:14:14,720 --> 00:14:25,760
senses. You've kind of missed the boat and you've gone on to liking or disliking the

94
00:14:25,760 --> 00:14:30,960
defilements of a risen, but then you're patient with them. So instead of you like something,

95
00:14:30,960 --> 00:14:37,920
you want something and you chase after it, or you dislike something, and so you try and destroy it,

96
00:14:37,920 --> 00:14:46,080
or run away from it. You know, you're patient with it. Which is, I think, even that is still

97
00:14:46,080 --> 00:14:51,680
important. But much more important is learning what we call anolomycacante, which comes from

98
00:14:51,680 --> 00:15:01,840
Saty and Banya. You'd be mindful and you gain wisdom and there's this conforming patient.

99
00:15:03,280 --> 00:15:13,840
It's just a general sense of not reacting to things. And you are able to bear things

100
00:15:13,840 --> 00:15:24,480
that would normally make you stressed because you've come to see that they're not worth clinging

101
00:15:24,480 --> 00:15:34,160
to. There's not worth getting stressed or upset about them. And video somewhere might be this

102
00:15:34,160 --> 00:15:44,720
sort of intense suppressing, where you just clench your teeth, and grin and bear it,

103
00:15:46,400 --> 00:15:57,680
grin and bear it. And just buckle down and bear it. Use effort to restrain your senses.

104
00:15:57,680 --> 00:16:07,040
Maybe in the worst case, when someone is, when there's a sound that's going to make you upset

105
00:16:07,040 --> 00:16:14,160
and so you just try and block it out. There's a sight that you want to see or don't want to see

106
00:16:14,160 --> 00:16:21,520
as you close your eyes. You won't see it. Or maybe there's some situation and you run away from it.

107
00:16:21,520 --> 00:16:30,080
I don't know exactly what media someone has in different kind ways of using effort to

108
00:16:31,040 --> 00:16:39,680
restrain your senses. Restrained of the senses is, I mean many ways it's just another way of talking

109
00:16:39,680 --> 00:16:48,800
about mindfulness practice as I said. The real point here is that this is our

110
00:16:48,800 --> 00:17:02,400
boundary. This is our border between reality and illusion. The border between

111
00:17:02,400 --> 00:17:09,520
ultimate reality and conceptual reality. Beyond these six senses, there's only concepts.

112
00:17:11,040 --> 00:17:17,360
And that's not saying anything about reality in the sense of what really exists. Again, getting

113
00:17:17,360 --> 00:17:30,640
back to this philosophical paradigm shift. It's simply that psychologically

114
00:17:32,160 --> 00:17:42,480
there's a difference in terms of how we react to reality. There's a difference between reacting

115
00:17:42,480 --> 00:17:47,600
on this side of the border and that side of the border, meaning within the six senses and outside

116
00:17:47,600 --> 00:17:57,600
of the six senses. When you focus on an experience in the six senses, the mind that has implications

117
00:17:57,600 --> 00:18:05,760
on the mind that are different from the implications or the effects that come from focusing on a

118
00:18:05,760 --> 00:18:16,160
concept. When you focus on a concept, you get caught up in abstracts, people, places, things,

119
00:18:16,160 --> 00:18:27,360
ideas, philosophy, and all sorts of complications. You get caught up in your own

120
00:18:27,360 --> 00:18:37,840
ideas of how things are. Reality becomes twisted. It's twistable, malleable. There's imagination.

121
00:18:37,840 --> 00:18:44,080
So you look at a person and you can think, boy, that person's really a good person,

122
00:18:44,960 --> 00:18:53,280
good friend of mine. They're just the best person in the world. Then suddenly they do something

123
00:18:53,280 --> 00:19:00,960
terrible and you're all disappointed and shocked because you had this idea about reality.

124
00:19:03,280 --> 00:19:14,880
This is why concepts are problematic. They don't actually play out in reality, whereas experiences do.

125
00:19:14,880 --> 00:19:30,320
When you focus on experiences, the mind learns something. It learns the mechanics,

126
00:19:31,600 --> 00:19:39,600
quantum mechanics or physics and mechanics of physics.

127
00:19:39,600 --> 00:19:49,200
But there's the mental mechanics, the mechanics of the mind and the mechanics of experience.

128
00:19:50,640 --> 00:19:56,240
You see how experience works and there is a mechanics to it. There are causes and effects.

129
00:19:58,160 --> 00:20:03,760
You see when you do this, this happens. When this happens and there's this reaction and there's

130
00:20:03,760 --> 00:20:15,360
that result. So restraint of the senses in the most important sense, it means staying within the

131
00:20:15,360 --> 00:20:24,240
senses. During the meditation course, we try to stay within the senses. Our intention is to try and

132
00:20:24,240 --> 00:20:36,320
learn the fundamentals of reality from an experiential point of view. We're trying to understand our

133
00:20:36,320 --> 00:20:43,760
psychology, understand our minds, understand our experience. Because any other understanding is

134
00:20:43,760 --> 00:20:51,440
problematic. When we talk about understanding, we're talking about me, mine, from a personal point

135
00:20:51,440 --> 00:20:58,640
of view, not in the sense of self or so or that kind of thing. What that means is that knowledge

136
00:20:58,640 --> 00:21:06,000
based on concepts or impersonal knowledge is kind of ridiculous because that knowledge,

137
00:21:06,000 --> 00:21:15,200
the effects of it are felt by oneself. So knowledge from an experiential point of view is

138
00:21:15,200 --> 00:21:25,120
quite different from intellectual knowledge and this idea that I'm giving you now. True knowledge

139
00:21:25,120 --> 00:21:37,280
comes from experience and so it's very important to guard the senses in these many different

140
00:21:37,280 --> 00:21:43,360
ways. There are different ways to guard the senses but ultimately as a means of keeping you within

141
00:21:43,360 --> 00:21:51,840
the realm of the senses. One of the most important things the Buddha said is let's

142
00:21:51,840 --> 00:21:58,480
seeing, be seeing, let hearing, be hearing, let sensing, be sensing, let thinking, be thinking.

143
00:22:00,880 --> 00:22:07,920
Thinking especially, I mean most of our senses nowadays, most of our central experience is actually

144
00:22:07,920 --> 00:22:18,800
mental. We think a lot. Human beings have very strong sixth sense in terms of the thought process.

145
00:22:18,800 --> 00:22:24,640
It's the sixth sense. So when we talk about the senses, we're not just talking about the physical

146
00:22:24,640 --> 00:22:30,480
world around us. We're also talking about the realm of our thoughts. Thinking about the past,

147
00:22:30,480 --> 00:22:39,600
thinking about the future, fantasizing, thinking about ourselves, philosophizing, dreaming,

148
00:22:42,560 --> 00:22:48,640
all of this is sensual. It's the sense. It's the sense of thought. Let it just be thought.

149
00:22:51,520 --> 00:22:57,680
See when we start to focus on the content then our mind gets fixed, focused on a

150
00:22:57,680 --> 00:23:05,840
on a thing that doesn't really exist and it's malleable and we can turn it into something that

151
00:23:05,840 --> 00:23:18,720
seems, makes us think, we're in control and gives entities, gives essence to things. Hey,

152
00:23:18,720 --> 00:23:25,120
that person and they're really good or hey this food and it's really delicious and then you

153
00:23:25,120 --> 00:23:32,800
eat it and it's actually kind of disappointing and boy this makes me happy and get all sorts of ideas.

154
00:23:38,080 --> 00:23:45,760
Reality and concept are two very different things. So you stay focused on senses then you see

155
00:23:45,760 --> 00:23:58,320
what's really happening. You're able to see experience, you're able to see reality. Not sure.

156
00:23:58,320 --> 00:24:03,680
And obviously all this, it's only words. You have to experience it for yourself.

157
00:24:05,840 --> 00:24:13,040
But guarding the senses is a very important part of our practice. It's one aspect of meditation and

158
00:24:13,040 --> 00:24:20,800
it's in a practical sense like in terms of walking around not looking at everything, not staring,

159
00:24:20,800 --> 00:24:32,880
not letting your senses wander. When you see, don't just look at it. Even just guarding your eyes.

160
00:24:34,080 --> 00:24:39,920
It's very useful in meditation and a big meditation center is recommended to not look around,

161
00:24:39,920 --> 00:24:45,680
not even make eye contact. Some meditation centers don't allow eye contact. That's a pretty hard

162
00:24:45,680 --> 00:24:51,280
car. But for this reason it's a means of guarding the senses even in a roundabout way. It doesn't

163
00:24:51,280 --> 00:25:01,040
really ultimately fix the problem but can be useful. Sort of as a support for your practice.

164
00:25:01,040 --> 00:25:16,160
Because then as the Buddha says, the bother, the vexation and burning that comes from

165
00:25:19,360 --> 00:25:24,800
comes from not guarding the senses. You just avoid it all and your practice is simpler,

166
00:25:24,800 --> 00:25:32,320
easier, more efficient until you can

167
00:25:35,840 --> 00:25:45,760
restrain your mind through knowledge and then you have ultimate efficiency and purity and peace.

168
00:25:45,760 --> 00:25:56,000
Perfect restraint without effortless restraint really because there's no effort needed when

169
00:25:56,000 --> 00:26:07,520
you know something is cause for suffering. You don't go there. So many different kinds of restraint,

170
00:26:07,520 --> 00:26:16,080
but restraint is good. Buddha said, sadhoo, sadhoo, I can't remember the problem, but it's like

171
00:26:17,360 --> 00:26:20,880
good is restraint in general. That's basically what he said.

172
00:26:23,920 --> 00:26:25,360
So that's the dumb offer tonight.

173
00:26:25,360 --> 00:26:38,320
Go for some questions. I'm not going to answer all questions. There's a lot of questions and

174
00:26:39,280 --> 00:26:46,080
going to be a little bit vigilant. Unfortunately, I'm not just going to answer speculative.

175
00:26:46,080 --> 00:26:57,520
I think speculative is out. Someone says, hey, I wonder, yeah, not really good enough.

176
00:27:00,560 --> 00:27:03,920
Much more interested in, hey, I'm having this problem. Can you help me?

177
00:27:05,440 --> 00:27:09,360
Okay. So I want to order the polycan in series.

178
00:27:11,680 --> 00:27:15,840
Some of the books seem to be connected there. If you want to order the polycan in series,

179
00:27:15,840 --> 00:27:21,520
you should order the four nikayas, which is the digany kaya, the long discourses,

180
00:27:22,160 --> 00:27:28,000
the midgimani kaya, the middle-length discourses, the angutranikaya, the numbered,

181
00:27:29,760 --> 00:27:36,000
it calls them the numbered discourses, and the sanutany kaya is the connected discourses,

182
00:27:36,000 --> 00:27:41,440
the numerical discourses, and then the connected discourses. That's where you should order if you

183
00:27:41,440 --> 00:27:47,840
want. He didn't do the digany kaya at someone else, but it's also the same publishers. He might as well

184
00:27:47,840 --> 00:27:57,520
get the digany kaya as well. So that's the four you should get. Let me know over that. I mean,

185
00:27:57,520 --> 00:28:00,960
there'll be a lot of overlap because a lot of the suit dessert in different places, but

186
00:28:02,240 --> 00:28:05,760
that's the full set. I mean, there's the fifth nikaya is the fifth

187
00:28:05,760 --> 00:28:12,640
this suit set, but it's a whole bunch of little books and the translators are

188
00:28:14,000 --> 00:28:20,000
well, it's hard to get a full set of the kutakany kaya. It's not that necessary either.

189
00:28:22,960 --> 00:28:31,600
But the demo pad is in there, the jatakaya, the odana, the iti vu takaya. Those four are

190
00:28:31,600 --> 00:28:39,760
pretty good books. Good to get translations in those. Okay, and there's two questions. There's another

191
00:28:39,760 --> 00:28:46,560
question here. I had a thought. Well, I'm not interested. Sorry.

192
00:28:52,640 --> 00:28:57,840
Wasn't a question. It was a thought, and then it was, what do you think? I think it's little as

193
00:28:57,840 --> 00:29:06,160
possible. Apart from being mindful, is there anyone, anything one could attempt to counter complacency

194
00:29:06,160 --> 00:29:14,480
that sets in when the practice is actually going well? You don't have to try and run away from

195
00:29:14,480 --> 00:29:19,760
complacency. I mean, complacency is just the thing you're saying to me, what's actually going on

196
00:29:19,760 --> 00:29:24,400
in the mind is what you have to worry about. The thing is the mind changes very quickly.

197
00:29:24,400 --> 00:29:29,120
You know, things are going good. The mind is in different state. Then when things are going bad,

198
00:29:29,120 --> 00:29:33,360
when things are going bad, it's in a quite different state from when things are going good.

199
00:29:33,360 --> 00:29:41,360
And then there's, there's many, many different states of mind. And we're, we're not quick enough

200
00:29:42,160 --> 00:29:46,640
to catch the next state. They're going to get caught up. And it's learning how to,

201
00:29:46,640 --> 00:29:54,000
how to catch up, how to keep up with the mind and with the changes of the mind.

202
00:29:55,920 --> 00:30:01,280
Don't, don't, don't get, don't get, make it too complicated. There's the force at the baton and

203
00:30:01,280 --> 00:30:09,520
whatever is there, be mindful of it. We say, well, I don't want to be mindful. What does that mean?

204
00:30:09,520 --> 00:30:17,760
I mean, are you aren't mindful? Well, why? What, what's going on in your mind?

205
00:30:21,200 --> 00:30:26,160
We among size a good one to reflect on what's going on in the mind that is preventing you from

206
00:30:26,160 --> 00:30:30,960
being mindful. I can't stop thinking about a world. I didn't, we do this one.

207
00:30:30,960 --> 00:30:40,320
It wasn't, didn't we have this question before? I think I answered that one. When pursuing a romantic

208
00:30:40,320 --> 00:30:46,560
interest, one cannot help but be attached to that person's presence. It's a form of desire and

209
00:30:46,560 --> 00:30:51,040
clinging, but then do, how then do we love without clinging?

210
00:30:51,040 --> 00:31:03,120
Mindfully, I mean, love doesn't require clinging. Love is, it's totally different.

211
00:31:06,640 --> 00:31:13,920
It's a different thought process. Love is like, I want you to be happy or I wish good for you

212
00:31:13,920 --> 00:31:22,240
or I think of you as a good person. Desire is, when the problem is, when you love someone,

213
00:31:22,240 --> 00:31:26,640
you're thinking about them and when you think about someone, it's very easy to be attracted to them.

214
00:31:28,080 --> 00:31:35,120
But you can't be, you know, you're generally attracted to the body and so love and,

215
00:31:36,320 --> 00:31:41,520
I mean, they're just really just words, but so why we use meta doesn't mean love, it means

216
00:31:41,520 --> 00:31:49,920
friendliness. So are you being friendly or are you attracted to the person to very different things?

217
00:31:55,680 --> 00:32:01,360
But yes, it's, it's hard because again, when you're friendly towards someone, why you're

218
00:32:03,120 --> 00:32:07,440
you're in contact with them and when you're in contact with someone, it's quite easy to become

219
00:32:07,440 --> 00:32:19,040
attracted to them if they're attracted, attractive. During a meeting in the meditation course

220
00:32:19,040 --> 00:32:24,240
between teacher and student, what is the student supposed to say or describe? What if nothing

221
00:32:24,240 --> 00:32:27,440
has changed from the day before and they don't have anything to say about their practice?

222
00:32:30,160 --> 00:32:35,760
If nothing has changed, I mean, that's fairly rare that nothing has changed or that there's

223
00:32:35,760 --> 00:32:41,600
nothing out of the ordinary, nothing interesting to talk about, but it doesn't really matter

224
00:32:41,600 --> 00:32:46,320
the teacher will, I mean, it's the teacher's job to keep pushing the student. If nothing's

225
00:32:46,320 --> 00:32:52,880
changed, then I may be not being pushed hard enough. So you have to challenge a meditator sometimes.

226
00:32:56,000 --> 00:32:59,280
Questions are updating anymore. I wonder if that means we're

227
00:32:59,280 --> 00:33:04,640
I'm logged out.

228
00:33:18,160 --> 00:33:23,040
Is it not possible to have a shorter course for beginners? 21 days, it's not accessible for many

229
00:33:23,040 --> 00:33:31,360
people as most people only have 14 days of vacation a year. It's possible, it's just you won't

230
00:33:33,600 --> 00:33:43,920
it's not arbitrary. It's quite challenging to get where we need to go and 14 days most people

231
00:33:43,920 --> 00:33:53,120
can't do it or a lot of people can't do it. Some people could be very dedicated, you probably could.

232
00:33:53,840 --> 00:33:58,560
So a way of doing that is doing an online course the way we've been doing it. Many people actually

233
00:33:58,560 --> 00:34:04,160
have completed the foundation course in less than 14 days because they've done the online course.

234
00:34:04,160 --> 00:34:12,720
We do the online course one week, we meet once a week and in one week they do one or two hours a

235
00:34:12,720 --> 00:34:21,360
day and by going through the course that way they're able to prepare quite fairly well for it

236
00:34:22,240 --> 00:34:27,440
and then at the very least they've got the whole technique down and then they can go through

237
00:34:27,440 --> 00:34:36,000
the technique intensively in about 12 days and the other thing is you're welcome to come for 14

238
00:34:36,000 --> 00:34:41,360
days if that's how you want to do it but it's very unlikely that in 14 days I would put you

239
00:34:41,360 --> 00:34:43,840
through the whole course just it wouldn't be too much.

240
00:34:52,640 --> 00:34:57,200
Could you please provide an example to clearly understand the use of attention in the context

241
00:34:57,200 --> 00:35:04,480
of mindfulness? What is the poly term for attention? It's not a word that I really use. I think I'm

242
00:35:04,480 --> 00:35:17,120
going to not answer this one because I just wouldn't use that word. I think Yoni, so Manasika

243
00:35:17,120 --> 00:35:29,360
is maybe a good attention. Manasika might be a good translation. Attention might be a good

244
00:35:29,360 --> 00:35:39,200
translation for Manasika. It often is. Manasika is often translated as attention. Why is

245
00:35:39,200 --> 00:35:46,400
attention and why is attention? It's not really, it's not that good of a translation. Wouldn't

246
00:35:46,400 --> 00:36:04,240
worry about attention so much as we're going to just words. You need to grasp the object

247
00:36:04,240 --> 00:36:15,120
clearly and pay attention certainly a part of it but more importantly is objectivity.

248
00:36:15,120 --> 00:36:22,880
You know, experience the object as it is. It might be a good translation, a good sort of word to

249
00:36:22,880 --> 00:36:34,640
use. I think it's a bit misleading though. Or a bit vague maybe. Can you have a version to a

250
00:36:34,640 --> 00:36:40,800
pleasant feeling? It wouldn't be a pleasant feeling if you had a version towards it. You can have

251
00:36:40,800 --> 00:36:46,880
a version indirectly. You can have a version towards the idea of the pleasant feeling.

252
00:36:52,240 --> 00:36:57,280
So you dislike the idea of liking. When you think about that thing that you like,

253
00:36:58,080 --> 00:37:03,120
you dislike that. You dislike the fact that you like it and the fact that you like it isn't the liking.

254
00:37:04,080 --> 00:37:10,160
Suppose you really attracted to someone but they've dumped you and then you get angry about

255
00:37:10,160 --> 00:37:15,360
the fact that you're still in love with them that kind of thing. But it's not the love that you're

256
00:37:15,360 --> 00:37:23,760
angry. It's not the feeling that the love comes up and you get angry or you do but it's after

257
00:37:23,760 --> 00:37:32,000
the fact. You get angry about the thought. Hey, you realize I love that person and that realization

258
00:37:32,000 --> 00:37:41,760
is what makes you angry. You get angry when you realize that. So I guess you can call that a

259
00:37:41,760 --> 00:37:47,680
version to a pleasant feeling but it's not exactly when a pleasant feeling arises. We call it

260
00:37:47,680 --> 00:37:55,120
pleasant because really we call it pleasant because there's a desire attached to it.

261
00:37:55,120 --> 00:38:03,440
Yeah, that's not entirely true. But now you can't directly have a version towards it.

262
00:38:05,920 --> 00:38:14,240
If an arrow hunter or Buddha, I can answer all timers, questions. I've answered these and they

263
00:38:14,240 --> 00:38:20,000
seem to keep coming back and I don't see how useful they are. Are you an arrow hunter or Buddha

264
00:38:20,000 --> 00:38:25,920
if you are then worried about it? If you are then you wouldn't worry about it. So it's really not

265
00:38:25,920 --> 00:38:32,400
that useful of a question. I followed the eight precepts for a day and I became so exhausted and stressed.

266
00:38:34,640 --> 00:38:46,320
Well, withdrawal. Yes, it's a sort of withdrawal. That's really all there is to say about that.

267
00:38:46,320 --> 00:38:52,880
The eight precepts are designed to put you through withdrawal from the things, the stimulation.

268
00:38:52,880 --> 00:38:58,320
So now you see it's all just a drug habit. You're dealing with all these mental drugs. The eight

269
00:38:58,320 --> 00:39:07,280
precepts is a detox. There are several stories about people who had a very bad history in their lives

270
00:39:07,280 --> 00:39:11,680
before reaching enlightenment. It's going to be that the more you suffer in life, the easier it could

271
00:39:11,680 --> 00:39:18,480
be to reach enlightenment. It can be. It's a cause for people to seek out enlightenment, seeing suffering.

272
00:39:23,040 --> 00:39:31,360
I don't think it's the more you suffer the easier it is. I think seeing the consequences of

273
00:39:31,360 --> 00:39:42,160
your actions. Seeing suffering, yeah, sure, if you've got strong suffering, it can make it easier,

274
00:39:43,120 --> 00:39:52,240
quite often makes it easier. Could we be living in a computer simulation? I don't want to answer that

275
00:39:52,240 --> 00:39:57,440
question, but the fact that it brings you great anxiety is interesting and you should focus on that

276
00:39:57,440 --> 00:40:02,480
anxiety. If you're mindful of that anxiety, you'll feel better. It doesn't really matter whether

277
00:40:02,480 --> 00:40:07,280
you're in a simulation or it doesn't matter because still, whether we're in a simulation or not,

278
00:40:07,280 --> 00:40:12,880
it's still just seeing hearings behind tasting, feeling, thinking, this is what Descartes got,

279
00:40:12,880 --> 00:40:19,920
way back when Descartes said, hey, I could be tricked about everything except the fact that I'm

280
00:40:19,920 --> 00:40:27,840
conscious except this experience. So it doesn't matter whether we're in a simulation, we're not

281
00:40:27,840 --> 00:40:35,040
really, we can't be. Computer simulations don't exist. One exists as seeing hearings, smelling

282
00:40:35,040 --> 00:40:43,120
tasting, feeling okay. How did the world come to be in Buddhism? The world, the earth, it

283
00:40:43,120 --> 00:40:50,080
devolved, you know, just naturally, really. But a lot of it had to do it was in conjunction with the

284
00:40:50,080 --> 00:40:58,960
mind. Our minds became more corrupt as the earth became more corrupt. This is a corruption,

285
00:40:58,960 --> 00:41:13,200
a courseification becoming more coarse, less refined as we develop greater desires and attachments

286
00:41:13,200 --> 00:41:24,320
and so on. I may be a little bit vague. I mean, there's not really, well, we're not really

287
00:41:24,320 --> 00:41:31,040
concerned about the origin of the world, but there are some stories like that on how the earth

288
00:41:31,040 --> 00:42:00,880
came to be the way it is. Okay, so there's all the questions. Thank you all for coming out. Have a good night.

