1
00:00:00,000 --> 00:00:08,000
In the video of the talk by Ajahn Tong, he spoke of three pathways to liberation,

2
00:00:08,000 --> 00:00:15,000
something along the lines of reaching Nirvana, Nirvana, by understanding one of the three characteristics

3
00:00:15,000 --> 00:00:20,000
and each a dukka anata. Could you explain something about that?

4
00:00:20,000 --> 00:00:24,000
Aren't the three characteristics inseparably connected?

5
00:00:24,000 --> 00:00:29,000
And how are those three pathways to be understood?

6
00:00:29,000 --> 00:00:34,000
Well, I mean, I only have the texts to go by, so I can tell you what the texts say,

7
00:00:34,000 --> 00:00:37,000
and that's what they say, one of three pathways.

8
00:00:37,000 --> 00:00:49,000
But we don't even have to go there, because practically speaking, it seems quite clearly to be the case that one does become prominent.

9
00:00:49,000 --> 00:00:59,000
So the example of these paths is when we focus on the stomach,

10
00:00:59,000 --> 00:01:03,000
the stomach will change in one of three ways. Suddenly it will begin to go quickly.

11
00:01:03,000 --> 00:01:08,000
This is a sign of impermanence, and the mind enters into cessation based on that.

12
00:01:08,000 --> 00:01:13,000
Sometimes the rising and the falling will become stuck, and unpleasant.

13
00:01:13,000 --> 00:01:18,000
This is a sign of suffering, and then because of that the mind goes into cessation.

14
00:01:18,000 --> 00:01:22,000
Sometimes the rising and falling will go quite smoothly.

15
00:01:22,000 --> 00:01:26,000
Well, suddenly it suddenly appear to be going on its own without...

16
00:01:26,000 --> 00:01:31,000
to have no relationship to oneself.

17
00:01:31,000 --> 00:01:35,000
So it's the idea of non-self, the idea that this experience isn't self,

18
00:01:35,000 --> 00:01:38,000
and because of that there is the cessation.

19
00:01:38,000 --> 00:01:47,000
This can be verified by experienced meditators who will have one of these or something else arise.

20
00:01:47,000 --> 00:01:55,000
If you want to stick to the theory that the three characters sticks are inseparable,

21
00:01:55,000 --> 00:01:58,000
then you would explain it as one of them being prominent.

22
00:01:58,000 --> 00:02:05,000
And the explanation that the text gives us to why one of them appears prominent, even though all three of them are there,

23
00:02:05,000 --> 00:02:08,000
is because of one's past inclinations.

24
00:02:08,000 --> 00:02:15,000
If a person has given a lot of charity and done a lot of good deeds, for example,

25
00:02:15,000 --> 00:02:20,000
done good for other people, then they will tend to enter in...

26
00:02:20,000 --> 00:02:23,000
and that is the prominent goodness and good quality that they have,

27
00:02:23,000 --> 00:02:27,000
then they'll tend to enter in through impermanence.

28
00:02:27,000 --> 00:02:30,000
If a person has practiced tranquility meditation,

29
00:02:30,000 --> 00:02:35,000
and that's the prominent characteristic of their personality,

30
00:02:35,000 --> 00:02:40,000
this strong concentration and calm in the mind,

31
00:02:40,000 --> 00:02:43,000
then they'll enter into suffering.

32
00:02:43,000 --> 00:02:46,000
They'll tend to enter in through suffering.

33
00:02:46,000 --> 00:02:49,000
If a person has practiced a lot of...

34
00:02:49,000 --> 00:02:52,000
we pass in an insane meditation,

35
00:02:52,000 --> 00:02:55,000
and that is the defining characteristic of their personality,

36
00:02:55,000 --> 00:02:58,000
then they'll tend to enter in through non-self.

37
00:02:58,000 --> 00:03:00,000
This is what the texts say.

38
00:03:00,000 --> 00:03:05,000
I think that's a little more difficult to verify because it's hard to say which...

39
00:03:05,000 --> 00:03:09,000
you look at a meditator, it's hard to say which characteristic is defining about them,

40
00:03:09,000 --> 00:03:16,000
let alone know because of their past history and as well have to do with past lives.

41
00:03:16,000 --> 00:03:21,000
But it certainly appears to be the case practically speaking,

42
00:03:21,000 --> 00:03:27,000
that one or the other becomes evident more clear.

43
00:03:27,000 --> 00:03:36,000
I don't know that there is any teaching of the Buddha that states that they're all seen at the same time.

44
00:03:36,000 --> 00:03:42,000
I would suggest that it's perhaps not the case that all three are seen at the same time,

45
00:03:42,000 --> 00:03:49,000
and that one necessarily has to see one or the other at any given time,

46
00:03:49,000 --> 00:03:51,000
but I don't have any backing for that.

47
00:03:51,000 --> 00:03:53,000
It may be possible to see all three at once.

48
00:03:53,000 --> 00:03:59,000
It seems to me that because the mind is only capable of having one thought at one given time,

49
00:03:59,000 --> 00:04:03,000
that any given thought would have to be either this is impermanent,

50
00:04:03,000 --> 00:04:10,000
this is impermanent, this is suffering or this is non-sobs, so you could see how it comes that one or the other is realized.

51
00:04:10,000 --> 00:04:20,000
Didn't the Buddha say when you see impermanence, you also see suffering a non-self?

52
00:04:20,000 --> 00:04:28,000
I'm quite sure of that, but I think it doesn't mean that you see all three at once,

53
00:04:28,000 --> 00:04:36,000
but you enter, you see impermanence first, and through seeing this,

54
00:04:36,000 --> 00:04:41,000
you understand suffering and non-self as well,

55
00:04:41,000 --> 00:04:46,000
or if you understand non-self first,

56
00:04:46,000 --> 00:04:51,000
to its fullest extent, you will understand both others.

57
00:04:51,000 --> 00:04:59,000
But I think it's kind of a misconception, it's kind of shocking to hear this because you do think that all three of them have to be understood,

58
00:04:59,000 --> 00:05:08,000
but I don't think there's any intrinsic need to realize all three.

59
00:05:08,000 --> 00:05:11,000
The point is to let go, right?

60
00:05:11,000 --> 00:05:14,000
Any one of these three will allow you to let go.

61
00:05:14,000 --> 00:05:16,000
They are all saying the same thing.

62
00:05:16,000 --> 00:05:22,000
This is not satisfying, it's not worth clinging to.

63
00:05:22,000 --> 00:05:26,000
It's the mind that let's go, why does it let go?

64
00:05:26,000 --> 00:05:30,000
Because it sees that they're not permanent, it sees that they're not satisfying,

65
00:05:30,000 --> 00:05:33,000
or it sees that they're not controllable.

66
00:05:33,000 --> 00:05:49,000
I would say it can be any one of the three, but it is in a sense saying the same thing about them that they're meaningless.

