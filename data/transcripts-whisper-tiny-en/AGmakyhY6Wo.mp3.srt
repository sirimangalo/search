1
00:00:00,000 --> 00:00:11,000
Here's a good question. Is it true that Mahasi Sayyada said that eventually the mental

2
00:00:11,000 --> 00:00:16,240
labeling and meditation can be dropped? Very good question. It shows that we have someone

3
00:00:16,240 --> 00:00:29,120
who knows something about Mahasi Sayyada tradition and technique and all of the little

4
00:00:29,120 --> 00:00:36,120
intricacies. My answer, no, Mahasi Sayyada never said that. There's a book and I

5
00:00:36,120 --> 00:00:41,840
probably have it here in my closet that seems to suggest that. It's written in

6
00:00:41,840 --> 00:00:49,040
English. There is a Thai book that is translated in Thai that actually explicitly

7
00:00:49,040 --> 00:01:03,080
states that. There is also, I'm trying to think, I researched this because there's

8
00:01:03,080 --> 00:01:28,280
one statement where Mahasi Sayyada said, let me get it, okay.

9
00:01:28,280 --> 00:01:35,280
Maybe I don't have it. Maybe it's here somewhere. Oh, I know. It's going to be in my suitcase.

10
00:01:35,280 --> 00:01:43,760
I'm not going to find it. I won't dig it out. He says, and the Thai is actually quite good.

11
00:01:43,760 --> 00:01:48,560
The Thai explains what the Burmese is actually saying. You'd have to go to the Burmese

12
00:01:48,560 --> 00:01:55,880
and see exactly what he says. What he says is that some people criticize this technique

13
00:01:55,880 --> 00:02:03,960
of labeling, of noting, the mantra. They criticize it saying that as a result, the

14
00:02:03,960 --> 00:02:12,040
meditator will focus only on concepts or the meditator will be, the mind will be focused

15
00:02:12,040 --> 00:02:18,520
on the concepts. When you say rising, you'll be focused on a concept of the stomach

16
00:02:18,520 --> 00:02:24,920
rising as opposed to actually focusing on the YO-DAT or the air element.

17
00:02:24,920 --> 00:02:30,960
Mahasi Sayyada's answer to that is it is true where, well, it is true that in the beginning

18
00:02:30,960 --> 00:02:41,160
a meditator will see only concepts, but yet I believe is the word that he used. It's

19
00:02:41,160 --> 00:02:49,040
the word that they use in Thai. As the meditator progresses, the concept will fall away

20
00:02:49,040 --> 00:02:54,640
and all that will be left is the ultimate experience of the ultimate reality. He never

21
00:02:54,640 --> 00:03:03,400
says that the meditator will cease to use the words. Now, what the Thai does, this terrible

22
00:03:03,400 --> 00:03:09,440
Thai book does, is it goes in the beginning, where is in the beginning the meditator

23
00:03:09,440 --> 00:03:19,840
will experience only concepts and then in parenthesis, it says noting, the word or the

24
00:03:19,840 --> 00:03:30,320
noting. As the meditator progresses, they will drop the concept or they will experience

25
00:03:30,320 --> 00:03:34,520
only the ultimate reality or something, making it very clear and I think even putting

26
00:03:34,520 --> 00:03:38,960
a footnote, explaining what it means that eventually the meditator doesn't have to

27
00:03:38,960 --> 00:03:46,920
use the noting anymore, which is certainly not what is said there. The English also

28
00:03:46,920 --> 00:03:51,400
misinterprets it in that way, but looking at the Thai, you can get a sense of what he's

29
00:03:51,400 --> 00:03:56,600
actually saying in Burmese and I bet that's quite clearly what he says because the Thai

30
00:03:56,600 --> 00:04:03,200
actually puts it in parenthesis. The English gloss is it over and just says, eventually

31
00:04:03,200 --> 00:04:07,200
then the words will be given up or something like, but the word that he uses is not

32
00:04:07,200 --> 00:04:14,280
word, it's concepts and that's clearly in line with the theory that we follow, that

33
00:04:14,280 --> 00:04:19,520
in the beginning the meditator, through this practice, will be the only the concepts of

34
00:04:19,520 --> 00:04:23,320
the stomach rising and so on, but eventually the concepts will disappear and when they

35
00:04:23,320 --> 00:04:30,680
say rising, they would simply be aware of the wild dot too. So no, as far as I understand

36
00:04:30,680 --> 00:04:35,760
Mahasi Sayodhana never said that the mental labeling can be dropped, he may have elsewhere

37
00:04:35,760 --> 00:04:40,000
but it would be a great shock to me, it was a shock to me to read that in English because

38
00:04:40,000 --> 00:04:47,800
it makes no sense. Another answer that you might give to that is when the point when

39
00:04:47,800 --> 00:04:53,160
the noting can be dropped, it can be dropped at some point, it can be dropped at the point

40
00:04:53,160 --> 00:05:04,560
when the meditator reaches Anulomanyana, the 12th stage of knowledge, then the 13th is going

41
00:05:04,560 --> 00:05:10,960
to remove 14th, 15th, yeah. So the 12th stage of knowledge, because at that point the mind

42
00:05:10,960 --> 00:05:21,160
needs no assistance, at that point the mind is seen perfectly clearly the four noble

43
00:05:21,160 --> 00:05:29,160
truths, from Anulomanyana all the way to Palañana, it's seeing the four noble truths,

44
00:05:29,160 --> 00:05:34,640
and that happens automatically. So at that point, most especially when the meditator enters

45
00:05:34,640 --> 00:05:41,400
into nibana, then is meditating, you could say, is meditating on nibana or is in the

46
00:05:41,400 --> 00:05:47,440
experience of cessation, then there's no need for the noting. It can also say that an

47
00:05:47,440 --> 00:05:51,680
Arahan has no need to practice the noting because what that means is the meditator has

48
00:05:51,680 --> 00:05:59,800
no need to meditate. So whether they actually do noting or not, you'll have to ask the

49
00:05:59,800 --> 00:06:17,200
Arahan themselves. Okay, yeah, what is that question?

