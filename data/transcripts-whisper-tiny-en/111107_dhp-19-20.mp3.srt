1
00:00:00,000 --> 00:00:07,000
Today we continue on the first study of the Dhamupana, going on to verses 19 and 20.

2
00:00:07,000 --> 00:00:10,000
It's comprised of single stories.

3
00:00:10,000 --> 00:00:17,000
The verses are as follows.

4
00:00:40,000 --> 00:00:47,000
The verses are as follows.

5
00:00:47,000 --> 00:00:54,000
The verses are as follows.

6
00:00:54,000 --> 00:01:03,000
The verses are as follows.

7
00:01:03,000 --> 00:01:10,000
The verses are as follows.

8
00:01:10,000 --> 00:01:17,000
The verses are as follows.

9
00:01:17,000 --> 00:01:24,000
The verses are as follows.

10
00:01:24,000 --> 00:01:31,000
The verses are as follows.

11
00:01:31,000 --> 00:01:38,000
The verses are as follows.

12
00:01:38,000 --> 00:01:45,000
The verses are as follows.

13
00:01:45,000 --> 00:01:52,000
The verses are as follows.

14
00:01:52,000 --> 00:01:59,000
The verses are as follows.

15
00:01:59,000 --> 00:02:06,000
The verses are as follows.

16
00:02:06,000 --> 00:02:10,180
of another who counts the cows of another person.

17
00:02:10,180 --> 00:02:12,840
Nabagavasam on Yasuhoti,

18
00:02:12,840 --> 00:02:17,240
such a person doesn't partake of the blessings of the holy life,

19
00:02:17,240 --> 00:02:21,040
or the fruits of the holy life.

20
00:02:21,040 --> 00:02:23,400
And the opposing verse,

21
00:02:23,400 --> 00:02:27,240
upon P.J. Samita Masumano, a person who knows even a little bit,

22
00:02:27,240 --> 00:02:28,840
even though they know a little bit,

23
00:02:28,840 --> 00:02:32,680
or are able to speak a little bit of the truth.

24
00:02:32,680 --> 00:02:37,440
But they are Damasa, they are Anudamachari,

25
00:02:37,440 --> 00:02:41,640
a person who practices the Damma in according to the Damma,

26
00:02:41,640 --> 00:02:43,720
or in order to realize the Damma,

27
00:02:43,720 --> 00:02:46,200
and realizes the Damma.

28
00:02:46,200 --> 00:02:49,800
Raghanjedbo, Sanjat Paha, Yamohang,

29
00:02:49,800 --> 00:02:54,360
they abandon with the abandoning of delusion,

30
00:02:54,360 --> 00:02:59,320
greed, greed, anger, and delusion.

31
00:02:59,320 --> 00:03:02,560
They have clear comprehension sum up,

32
00:03:02,560 --> 00:03:06,240
Pajano, and Suimutajito,

33
00:03:06,240 --> 00:03:10,680
their mind is well freed, or well liberated.

34
00:03:10,680 --> 00:03:15,640
Anupadiya, no, not clinging to anything.

35
00:03:15,640 --> 00:03:20,000
Idda wa huranwahir, or here after things in this life,

36
00:03:20,000 --> 00:03:23,960
or in terms of the future of the future lives.

37
00:03:23,960 --> 00:03:26,400
Samba noasam on Yasuhoti,

38
00:03:26,400 --> 00:03:28,800
such a person partakes,

39
00:03:28,800 --> 00:03:31,200
is one who partakes in the holy life,

40
00:03:31,200 --> 00:03:34,000
or in the fruits of the holy life.

41
00:03:34,000 --> 00:03:36,240
So, this is word by word translation,

42
00:03:36,240 --> 00:03:38,080
basically, it means a person who,

43
00:03:38,080 --> 00:03:41,320
though they know a lot, that doesn't practice accordingly,

44
00:03:41,320 --> 00:03:45,680
and, you know, our negligent and heedless and so on,

45
00:03:45,680 --> 00:03:50,040
they don't have a part in the fruit of the teachings.

46
00:03:50,040 --> 00:03:52,320
But a person who, even though they've studied a little bit,

47
00:03:52,320 --> 00:03:55,320
and maybe know very little,

48
00:03:55,320 --> 00:03:58,000
and can explain very little,

49
00:03:58,000 --> 00:04:02,600
but still, by practicing according to the teaching,

50
00:04:02,600 --> 00:04:07,080
they become one who partakes on the fruit of the truth.

51
00:04:07,080 --> 00:04:08,920
Now, obviously, this makes sense.

52
00:04:08,920 --> 00:04:10,760
Through practicing the teachings,

53
00:04:10,760 --> 00:04:12,760
if you practice accordingly,

54
00:04:12,760 --> 00:04:14,160
and give up,

55
00:04:14,160 --> 00:04:16,720
the low bud of Samoha raga,

56
00:04:16,720 --> 00:04:21,600
lust of the anger and the moha delusion,

57
00:04:21,600 --> 00:04:23,520
someone who has clear comprehension.

58
00:04:23,520 --> 00:04:26,040
So, it's actually practicing in the Buddhist teaching

59
00:04:26,040 --> 00:04:27,600
on mindfulness,

60
00:04:27,600 --> 00:04:31,680
and he comes, Suvi Muta Chitoh has freedom in the mind.

61
00:04:34,080 --> 00:04:36,000
So, this is given in regards to a story.

62
00:04:36,000 --> 00:04:37,520
Again, I'm pulling out the commentary here,

63
00:04:37,520 --> 00:04:40,480
because it turns out that the English is quite

64
00:04:42,240 --> 00:04:44,040
negligent for Mutoh,

65
00:04:44,040 --> 00:04:48,240
the person who really translated it didn't do a very good job.

66
00:04:48,240 --> 00:04:50,880
So, in the last story, and in this story,

67
00:04:50,880 --> 00:04:53,480
I realized I'm probably gonna have to use this book

68
00:04:53,480 --> 00:04:56,520
to check every one and actually read the following,

69
00:04:56,520 --> 00:05:00,200
because it's not precise.

70
00:05:00,200 --> 00:05:02,240
The meaning is not, meaning is not as given.

71
00:05:02,240 --> 00:05:03,920
In the last one, it was actually quite important,

72
00:05:03,920 --> 00:05:06,200
because what it said is that a person

73
00:05:06,200 --> 00:05:08,360
who's reached the second stage,

74
00:05:08,360 --> 00:05:13,920
this woman who called her younger brother,

75
00:05:15,840 --> 00:05:17,640
and they say, the reason she got sick

76
00:05:17,640 --> 00:05:20,240
is because she wanted a husband,

77
00:05:20,240 --> 00:05:22,520
but she was a second academy.

78
00:05:22,520 --> 00:05:24,760
So, the idea that she would starve herself

79
00:05:24,760 --> 00:05:26,080
because she didn't have a husband,

80
00:05:26,080 --> 00:05:28,200
she was a bit ludicrous.

81
00:05:28,200 --> 00:05:30,880
She was someone who was very high in teaching,

82
00:05:30,880 --> 00:05:34,200
and that's not what actually looked at the text says.

83
00:05:34,200 --> 00:05:35,560
This one is not so important,

84
00:05:35,560 --> 00:05:39,080
but it's mixing up some parts of it,

85
00:05:39,080 --> 00:05:41,600
and it's saying that the,

86
00:05:41,600 --> 00:05:43,120
by the way, I'll read the story

87
00:05:43,120 --> 00:05:45,040
and then I'll explain what the disparity is.

88
00:05:45,040 --> 00:05:48,080
So, there were these two friends,

89
00:05:48,080 --> 00:05:49,720
inseparable friends,

90
00:05:49,720 --> 00:05:52,600
and they became monks in the Buddhist teaching.

91
00:05:52,600 --> 00:05:55,160
The older, one was older, one was,

92
00:05:55,160 --> 00:05:57,320
they weren't the same age, one older, one younger,

93
00:05:58,520 --> 00:05:59,840
and the Buddha said, so,

94
00:06:01,760 --> 00:06:04,400
tell them that there are two duties

95
00:06:04,400 --> 00:06:05,640
in the Buddhist teaching.

96
00:06:05,640 --> 00:06:06,800
When someone becomes a monk,

97
00:06:06,800 --> 00:06:07,680
they have two duties.

98
00:06:07,680 --> 00:06:10,880
One is to practice Vipasana and the others

99
00:06:10,880 --> 00:06:14,040
to study the text or to memorize the text.

100
00:06:14,040 --> 00:06:16,400
The older one said, well, being old,

101
00:06:16,400 --> 00:06:18,280
probably it's gonna be difficult for me

102
00:06:18,280 --> 00:06:20,760
to learn all of the teachings

103
00:06:20,760 --> 00:06:23,040
and to memorize them and be able to recite them.

104
00:06:23,040 --> 00:06:26,040
Can I just do the Vipasana part?

105
00:06:26,040 --> 00:06:27,040
Or he said to himself,

106
00:06:27,040 --> 00:06:28,440
I'll just do the Vipasana part.

107
00:06:28,440 --> 00:06:29,960
I'll just do the part of,

108
00:06:29,960 --> 00:06:32,040
I mean to see things clearly as they are.

109
00:06:33,240 --> 00:06:37,840
So, he took up some ragrobes.

110
00:06:37,840 --> 00:06:39,400
He went and got some rags

111
00:06:39,400 --> 00:06:41,520
and put together a set of robes,

112
00:06:41,520 --> 00:06:45,160
and went to the Buddha and asked for a meditation teaching,

113
00:06:45,160 --> 00:06:46,960
and the Buddha gave him a teaching

114
00:06:46,960 --> 00:06:50,600
in regards to the practice of Vipasana meditation,

115
00:06:50,600 --> 00:06:52,760
all the way up to Arahanshi.

116
00:06:52,760 --> 00:06:55,720
He went off into the forest and became an Arahan.

117
00:06:55,720 --> 00:06:57,400
The other one, because he was younger,

118
00:06:57,400 --> 00:07:00,240
he said, well, I'll just do the study side of things then.

119
00:07:00,240 --> 00:07:03,800
So he said, okay, I'll take up and learn all of the Buddha's teaching,

120
00:07:03,800 --> 00:07:07,000
and he actually was able to memorize all of the Tipitika.

121
00:07:07,000 --> 00:07:08,840
I believe that is what it says.

122
00:07:10,440 --> 00:07:12,920
Anyway, he was able to memorize quite a bit of the,

123
00:07:12,920 --> 00:07:15,320
if not all of the Tipitika, the Buddha's teaching.

124
00:07:17,320 --> 00:07:18,560
But he didn't practice.

125
00:07:18,560 --> 00:07:21,400
And because he was so famous from his teaching,

126
00:07:21,400 --> 00:07:23,600
he had lots and lots of students coming to see him.

127
00:07:23,600 --> 00:07:26,240
So he was a very well-known teacher,

128
00:07:26,240 --> 00:07:28,320
well-respected and lots of,

129
00:07:28,320 --> 00:07:30,400
something like 18 groups or something,

130
00:07:30,400 --> 00:07:31,560
I don't know what it says in here.

131
00:07:34,760 --> 00:07:38,040
And the one in the forest, as an Arahan,

132
00:07:38,040 --> 00:07:39,920
it so happened that a group of monks,

133
00:07:39,920 --> 00:07:41,840
they got training from the Buddha,

134
00:07:41,840 --> 00:07:42,960
and then they went off to the forest,

135
00:07:42,960 --> 00:07:45,160
and went to where he was staying.

136
00:07:45,160 --> 00:07:47,600
Practice under him and became Arahanshi as well.

137
00:07:48,840 --> 00:07:50,640
When they become Arahan,

138
00:07:50,640 --> 00:07:51,960
they asked, they went to see the elder,

139
00:07:51,960 --> 00:07:54,720
and they asked for a mission to go back and see the Buddha.

140
00:07:54,720 --> 00:07:55,840
They said, we'd like to go see the Buddha.

141
00:07:55,840 --> 00:07:59,000
The Buddha said, fine, go back and pay respect to the Buddha,

142
00:07:59,000 --> 00:08:02,400
and pay respect to the Buddha's chief disciples,

143
00:08:02,400 --> 00:08:04,440
and all of the 80 great disciples.

144
00:08:04,440 --> 00:08:06,200
And then go and pay respect to my friend,

145
00:08:06,200 --> 00:08:07,800
and tell them, you're paying respect

146
00:08:07,800 --> 00:08:09,120
in the name of your teacher.

147
00:08:10,320 --> 00:08:11,840
So they did this, they went to the Buddha,

148
00:08:11,840 --> 00:08:14,560
and so on, and they went to find his old friend,

149
00:08:14,560 --> 00:08:16,880
and said, we pay respect in the name of our teacher.

150
00:08:16,880 --> 00:08:19,920
And they said, who is your teacher?

151
00:08:19,920 --> 00:08:20,880
And he said, it's your friend,

152
00:08:20,880 --> 00:08:23,080
who you became a monk together.

153
00:08:23,080 --> 00:08:25,600
And he said, here, students, what did he teach you?

154
00:08:25,600 --> 00:08:28,120
Did he teach, could you, what could you have learned from him?

155
00:08:28,120 --> 00:08:32,400
Could you have learned even one Nikaya or one Pithika,

156
00:08:32,400 --> 00:08:34,720
or what could you possibly have learned from him?

157
00:08:36,200 --> 00:08:37,400
Because he doesn't know anything.

158
00:08:37,400 --> 00:08:40,200
He didn't study, what do you mean teacher?

159
00:08:41,960 --> 00:08:43,320
So he said this kind of thing,

160
00:08:43,320 --> 00:08:44,920
and then he thought to himself,

161
00:08:44,920 --> 00:08:47,680
when this monk comes, I'm going to ask him some questions.

162
00:08:47,680 --> 00:08:52,680
I'm going to show everyone that that is a fraud,

163
00:08:53,440 --> 00:08:55,440
really, because he can't be a teacher,

164
00:08:55,440 --> 00:08:56,400
he doesn't know anything.

165
00:08:58,760 --> 00:09:02,280
And it so happened that eventually this senior,

166
00:09:02,280 --> 00:09:04,480
the Arahant came back to where the Buddha

167
00:09:04,480 --> 00:09:06,560
and the Buddha was paid respect to the Buddha

168
00:09:06,560 --> 00:09:09,920
and went to see his old friend and sat down.

169
00:09:09,920 --> 00:09:14,920
And the younger monk, who was the study monk,

170
00:09:14,920 --> 00:09:18,560
he thought, okay, now I'll ask him some questions.

171
00:09:18,560 --> 00:09:20,560
And I'll show him and his students were there

172
00:09:20,560 --> 00:09:22,000
and the older monk students were there

173
00:09:22,000 --> 00:09:23,320
and he said, in front of all these people,

174
00:09:23,320 --> 00:09:25,960
we'll show everyone with this monk.

175
00:09:25,960 --> 00:09:27,160
That this monk knows nothing.

176
00:09:28,240 --> 00:09:29,520
And the Buddha found out about this,

177
00:09:29,520 --> 00:09:32,280
he knew what was going on and got the gist of it

178
00:09:32,280 --> 00:09:34,360
and realized that if he let this happen,

179
00:09:34,360 --> 00:09:36,440
this younger monk might go to help.

180
00:09:36,440 --> 00:09:38,160
He said, nearly young, it says in his summer

181
00:09:38,160 --> 00:09:42,000
that, I know it's on the last page, that's why I don't see him.

182
00:09:42,000 --> 00:09:44,480
He said, if I let him be, he'll go to Niriya,

183
00:09:44,480 --> 00:09:49,480
he'll go to hell, Niriya, but he might go to Niriya

184
00:09:49,480 --> 00:09:52,640
to help if I leave him, if I let this happen.

185
00:09:52,640 --> 00:09:56,560
So the Buddha went walking through the monastery

186
00:09:56,560 --> 00:09:58,120
and went to see these two monks.

187
00:09:59,160 --> 00:10:01,080
And he sat down on the seat that they had ready

188
00:10:01,080 --> 00:10:02,920
because they always have a Buddha suit ready.

189
00:10:04,560 --> 00:10:08,720
And he asked the Buddha to ask questions.

190
00:10:08,720 --> 00:10:10,440
And this is where the disparity is.

191
00:10:10,440 --> 00:10:15,440
Because what really happens is he asks first he asks the study

192
00:10:17,560 --> 00:10:19,280
monkey about the first genre.

193
00:10:20,960 --> 00:10:22,200
And the first monk can't answer.

194
00:10:22,200 --> 00:10:25,080
He asks the other monk, the monk has been practicing

195
00:10:25,080 --> 00:10:28,240
about the first genre, that monk, no problem answering.

196
00:10:28,240 --> 00:10:31,400
And he asks the second genre, same thing.

197
00:10:31,400 --> 00:10:32,760
Third genre, fourth genre.

198
00:10:32,760 --> 00:10:37,760
Although all of the eight, the four rupajanas in the four,

199
00:10:37,760 --> 00:10:42,480
rupajanas in the four, a rupajanas, he asks about all of them.

200
00:10:42,480 --> 00:10:45,400
And it says in this, the text that the study monk

201
00:10:45,400 --> 00:10:47,640
wasn't able to answer a single one of them.

202
00:10:49,800 --> 00:10:51,880
And on the other hand, the practice monk

203
00:10:51,880 --> 00:10:54,480
was able to answer every single one of them.

204
00:10:54,480 --> 00:10:56,120
He was able to answer them.

205
00:10:56,120 --> 00:11:00,200
Then he asked about the first path, the Sotapana path.

206
00:11:00,200 --> 00:11:02,240
And the study monk was unable to answer

207
00:11:02,240 --> 00:11:04,120
and his practice monk was.

208
00:11:04,120 --> 00:11:06,600
So he's asking questions that relate directly to the practice.

209
00:11:06,600 --> 00:11:09,480
So it must be something like, what is this experience?

210
00:11:09,480 --> 00:11:10,560
Like, what is that experience?

211
00:11:10,560 --> 00:11:12,400
Or what happens during this experience?

212
00:11:12,400 --> 00:11:14,120
Can you describe it to me?

213
00:11:15,040 --> 00:11:16,560
And because it wasn't in the text,

214
00:11:16,560 --> 00:11:18,560
it wasn't something you learned from heart.

215
00:11:18,560 --> 00:11:23,560
You had no knowledge of it and no answer.

216
00:11:23,720 --> 00:11:26,400
So the first, second, third, fourth.

217
00:11:26,400 --> 00:11:31,320
And each time the Buddha, when the practice monk answered

218
00:11:31,320 --> 00:11:33,600
correctly, he would hold his hands up and say sad.

219
00:11:33,600 --> 00:11:37,000
And expresses appreciation.

220
00:11:38,000 --> 00:11:42,760
And then all of the students of the study monk,

221
00:11:42,760 --> 00:11:44,480
they started muttering about this.

222
00:11:44,480 --> 00:11:47,840
And they said, why is the Buddha keep praising the younger monk?

223
00:11:47,840 --> 00:11:50,000
And they said, it isn't crazy that the Buddha keeps praising

224
00:11:50,000 --> 00:11:51,680
this monk when there was nothing.

225
00:11:51,680 --> 00:11:52,960
And the Buddha turned to them and said,

226
00:11:52,960 --> 00:11:54,960
what are you talking about?

227
00:11:54,960 --> 00:11:56,800
And they explained what they're saying.

228
00:11:56,800 --> 00:11:59,480
And the Buddha said, monks, your teacher,

229
00:11:59,480 --> 00:12:03,560
is like a person who looks after the cows of another farmer.

230
00:12:03,560 --> 00:12:05,080
He looks after them.

231
00:12:05,080 --> 00:12:06,520
He doesn't get any of the milk.

232
00:12:06,520 --> 00:12:08,040
He doesn't get the butter.

233
00:12:08,040 --> 00:12:09,480
He doesn't get the cheese.

234
00:12:09,480 --> 00:12:12,200
He doesn't get any of the fruits of the cow.

235
00:12:12,200 --> 00:12:15,040
But my son, mama put that.

236
00:12:15,040 --> 00:12:18,200
Mama put those, what he said.

237
00:12:18,200 --> 00:12:25,000
I think, I believe, is like the farmer who owns the cow.

238
00:12:25,000 --> 00:12:28,040
And then he gave these two verses.

239
00:12:28,040 --> 00:12:30,720
So the discrepancy is that the English translation

240
00:12:30,720 --> 00:12:34,000
makes it out that the John is that the study monk

241
00:12:34,000 --> 00:12:38,120
was able to answer them, which isn't the case here.

242
00:12:38,120 --> 00:12:40,400
Anyway, it's just a small point.

243
00:12:40,400 --> 00:12:42,920
More important, this is a very important one

244
00:12:42,920 --> 00:12:45,400
from practical point of view, especially

245
00:12:45,400 --> 00:12:48,840
because it details some points of practice.

246
00:12:48,840 --> 00:12:53,080
And some characteristics of a person, it doesn't practice.

247
00:12:53,080 --> 00:12:55,840
So this is a reoccurring theme in the Buddha's teaching

248
00:12:55,840 --> 00:12:59,160
that the study of the texts doesn't make one

249
00:12:59,160 --> 00:13:01,800
or a follower of the Buddha.

250
00:13:01,800 --> 00:13:03,240
Even though you can study all of this,

251
00:13:03,240 --> 00:13:08,000
and many monks will practically memorize this commentary.

252
00:13:08,000 --> 00:13:09,520
This is how they learn poly.

253
00:13:09,520 --> 00:13:10,640
They did a little bit of it.

254
00:13:10,640 --> 00:13:16,120
I learned maybe a half of this, and I tried.

255
00:13:16,120 --> 00:13:18,480
And it's a good thing to study.

256
00:13:18,480 --> 00:13:22,040
But the person who not got to hold the person doesn't practice

257
00:13:22,040 --> 00:13:24,720
accordingly, who doesn't take the teachings to heart

258
00:13:24,720 --> 00:13:26,400
and isn't mindful.

259
00:13:26,400 --> 00:13:29,480
If you know the last words of the Buddha,

260
00:13:29,480 --> 00:13:33,200
it was a pamadhi in the sampadhi.

261
00:13:33,200 --> 00:13:35,400
So don't be negligent.

262
00:13:35,400 --> 00:13:37,520
These were his last words.

263
00:13:37,520 --> 00:13:40,040
So here, a person who's being negligent

264
00:13:40,040 --> 00:13:43,560
is very much the core of what the Buddha told us not to do.

265
00:13:43,560 --> 00:13:46,480
So that means that for someone who's teaching,

266
00:13:46,480 --> 00:13:49,280
wasting their time just with teaching,

267
00:13:49,280 --> 00:13:50,800
just with spreading the dhamma together,

268
00:13:50,800 --> 00:13:54,520
so not actually practicing on their own,

269
00:13:54,520 --> 00:13:56,840
but that it's considered to be negligent.

270
00:13:56,840 --> 00:14:00,400
And of course, in their mind, they're heedless.

271
00:14:00,400 --> 00:14:02,440
They're not aware when the emotion arrives.

272
00:14:02,440 --> 00:14:05,800
And you see this in many scholarly monks

273
00:14:05,800 --> 00:14:07,320
or scholars of any type.

274
00:14:07,320 --> 00:14:11,400
You can become quite conceited and attached to their knowledge,

275
00:14:11,400 --> 00:14:18,040
and aren't able to recognize when

276
00:14:18,040 --> 00:14:20,080
Miranda would recognize the defilements and yourselves

277
00:14:20,080 --> 00:14:22,960
and aren't able to eradicate them.

278
00:14:22,960 --> 00:14:26,880
On the other hand, a person who doesn't even study anything,

279
00:14:26,880 --> 00:14:30,440
or let's say not doesn't study anything up but,

280
00:14:30,440 --> 00:14:33,400
up but means little, because the story

281
00:14:33,400 --> 00:14:36,360
goes that he studied enough to become an aura.

282
00:14:36,360 --> 00:14:38,320
This is like when someone has a map

283
00:14:38,320 --> 00:14:39,880
and one studies the route, one's going,

284
00:14:39,880 --> 00:14:41,760
one doesn't have to study over here or over there,

285
00:14:41,760 --> 00:14:44,000
one just has to study this way.

286
00:14:44,000 --> 00:14:45,360
Okay, and then I reach here and there,

287
00:14:45,360 --> 00:14:47,960
and then there's this sign and turn right and so on.

288
00:14:47,960 --> 00:14:49,840
And that's it, and don't have to worry

289
00:14:49,840 --> 00:14:52,000
about the rest of the map.

290
00:14:52,000 --> 00:14:55,320
This is what it means by up but it means enough.

291
00:14:55,320 --> 00:14:57,520
You learn just enough.

292
00:14:57,520 --> 00:15:02,760
And so this is the difference between these two months.

293
00:15:02,760 --> 00:15:05,640
One of them learned the whole map, which is useful

294
00:15:05,640 --> 00:15:06,880
because he had lots of students

295
00:15:06,880 --> 00:15:08,880
and maybe some of his students became enlightened.

296
00:15:08,880 --> 00:15:10,640
There was a story that I always tell him,

297
00:15:10,640 --> 00:15:12,480
but this monk who just did teaching

298
00:15:12,480 --> 00:15:14,640
and all of his students became enlightened.

299
00:15:14,640 --> 00:15:16,360
One of his students became an anagami

300
00:15:16,360 --> 00:15:18,560
and thought, what is our teacher?

301
00:15:18,560 --> 00:15:21,760
What the team says our teacher gave?

302
00:15:21,760 --> 00:15:23,480
And he thought about it and he realized our teacher

303
00:15:23,480 --> 00:15:25,760
is still an ordinary human being.

304
00:15:25,760 --> 00:15:28,080
So he went and he performed some magic

305
00:15:28,080 --> 00:15:31,040
and he levitated off the floor in front of his teacher.

306
00:15:31,040 --> 00:15:33,040
He said, time for a lesson,

307
00:15:33,040 --> 00:15:34,960
but he didn't mean that the way his teacher thought in his,

308
00:15:34,960 --> 00:15:37,600
I have no time for a lesson and it's so busy.

309
00:15:37,600 --> 00:15:39,080
And then he floated up in the air and he said,

310
00:15:39,080 --> 00:15:40,440
you have no time for yourself.

311
00:15:40,440 --> 00:15:42,080
I don't want to lesson from you.

312
00:15:42,080 --> 00:15:44,920
He flew out the window, apparently.

313
00:15:47,560 --> 00:15:50,920
And this monk got very distressed

314
00:15:50,920 --> 00:15:52,520
and he actually left and went

315
00:15:52,520 --> 00:15:54,560
off to practice meditation as a result.

316
00:15:56,400 --> 00:15:58,760
If you've heard the story, if you have an end of the story,

317
00:15:58,760 --> 00:16:01,520
he goes off into the forest and he's all alone.

318
00:16:01,520 --> 00:16:03,760
But he's so famous that the angels think he's,

319
00:16:05,480 --> 00:16:06,480
they think they can learn from him.

320
00:16:06,480 --> 00:16:08,320
So the angels come and they want to,

321
00:16:08,320 --> 00:16:09,600
well, he's practicing meditation.

322
00:16:09,600 --> 00:16:10,880
Let's practice accordingly.

323
00:16:11,920 --> 00:16:14,320
And so he walks and they walk and he sits and they sit.

324
00:16:14,320 --> 00:16:16,760
And then he gets so frustrated that he's not gaining

325
00:16:16,760 --> 00:16:19,240
and he sits down and starts crying.

326
00:16:19,240 --> 00:16:21,280
And suddenly this angel appears beside him

327
00:16:21,280 --> 00:16:23,120
and it's sitting there crying.

328
00:16:23,120 --> 00:16:26,640
And he said, what are you, what are you crying for?

329
00:16:26,640 --> 00:16:28,320
And then he said, well, I see you cry.

330
00:16:28,320 --> 00:16:30,760
I thought that must be the way to do it, the way to practice.

331
00:16:33,000 --> 00:16:36,680
I'm not more, and then he felt so ashamed

332
00:16:36,680 --> 00:16:39,080
that he actually started taking it seriously

333
00:16:39,080 --> 00:16:40,840
and he woke up and said, this is stupid.

334
00:16:40,840 --> 00:16:44,000
And he's getting himself a slap in the face

335
00:16:44,000 --> 00:16:46,120
and sobered up, I guess.

336
00:16:46,120 --> 00:16:50,040
And then he became an aura.

337
00:16:50,040 --> 00:16:51,480
So the key is to not be language.

338
00:16:51,480 --> 00:16:53,840
And the person who is, who is negligent

339
00:16:53,840 --> 00:16:56,000
means someone who is not being mindful

340
00:16:56,000 --> 00:16:56,840
is the good of saying.

341
00:16:56,840 --> 00:16:58,280
The person who is always mindful,

342
00:16:58,280 --> 00:17:01,800
this is what means to be vigilant or to be hateful.

343
00:17:06,440 --> 00:17:08,000
And so here, and then in the next verse,

344
00:17:08,000 --> 00:17:10,840
it talks about what are some of the requisites

345
00:17:10,840 --> 00:17:12,560
for being hateful.

346
00:17:12,560 --> 00:17:16,120
And I must say, oh, the Anudamutadi, someone who practices

347
00:17:16,120 --> 00:17:18,320
the them practices the teaching.

348
00:17:18,320 --> 00:17:20,360
So you take the four foundations of mindfulness.

349
00:17:20,360 --> 00:17:21,840
They don't sit around thinking about them

350
00:17:21,840 --> 00:17:24,400
or studying about them or teaching.

351
00:17:24,400 --> 00:17:27,600
When we're citing them, however, he take the time to go

352
00:17:27,600 --> 00:17:33,160
and find a cave or find a tree or find a hut and practice.

353
00:17:36,400 --> 00:17:37,280
And then what do you do?

354
00:17:37,280 --> 00:17:39,920
Raghan, chadhu, santja, paha, yumoham.

355
00:17:39,920 --> 00:17:43,760
This is key, because some people don't quite,

356
00:17:43,760 --> 00:17:45,760
it isn't quite clear what we're trying to do

357
00:17:45,760 --> 00:17:49,120
and what are the measurement of our practice.

358
00:17:49,120 --> 00:17:51,720
The way we measure our practice is based on the amount

359
00:17:51,720 --> 00:17:54,600
of ragha, the amount of dosa, the amount of moha,

360
00:17:54,600 --> 00:17:57,120
the greed, anger, and delusion.

361
00:17:57,120 --> 00:17:59,360
And the more of this that we're able to do away with,

362
00:17:59,360 --> 00:18:00,880
the further we've gone in the practice,

363
00:18:00,880 --> 00:18:04,440
or the further our measure of our progress

364
00:18:04,440 --> 00:18:07,200
is how much we've been able to do away with this.

365
00:18:07,200 --> 00:18:10,360
And we have less ragha, less lust, less anger,

366
00:18:10,360 --> 00:18:13,680
less delusion, and the core.

367
00:18:13,680 --> 00:18:16,800
And then the way to abandon it is in the next

368
00:18:16,800 --> 00:18:21,960
where some pajan know through the practice of samma,

369
00:18:21,960 --> 00:18:26,040
which means right, and pajan know, which means clear seeing,

370
00:18:26,040 --> 00:18:29,400
we're seeing thoroughly.

371
00:18:29,400 --> 00:18:31,800
We translate this often as clear comprehension,

372
00:18:31,800 --> 00:18:37,440
some pajan know, some pajan yeah.

373
00:18:37,440 --> 00:18:43,160
But it really means the perfect awareness of reality.

374
00:18:43,160 --> 00:18:44,720
So, in a sense, when you watch the stomach,

375
00:18:44,720 --> 00:18:46,440
it's a simple example.

376
00:18:46,440 --> 00:18:48,680
Perfectly knowing what's going on.

377
00:18:48,680 --> 00:18:51,200
This is the rise, and this is the fall.

378
00:18:51,200 --> 00:18:52,560
When you're walking, knowing that you're

379
00:18:52,560 --> 00:18:54,680
for this movie, when you're sitting,

380
00:18:54,680 --> 00:18:56,920
knowing that you're sitting, and so on.

381
00:18:56,920 --> 00:18:59,320
When you're in this awareness, when you have this full

382
00:18:59,320 --> 00:19:04,520
and complete, and samma, which means proper awareness.

383
00:19:04,520 --> 00:19:08,240
So, it means there's no, it's just full knowing.

384
00:19:08,240 --> 00:19:09,240
You know, this is rising.

385
00:19:09,240 --> 00:19:12,680
You don't like it, or just like it, or cling to it.

386
00:19:12,680 --> 00:19:15,160
And whatever experience arises, so we hear the noise.

387
00:19:15,160 --> 00:19:18,000
You're not getting upset or attracted to it.

388
00:19:18,000 --> 00:19:23,280
We see something not to be attached to it, or a post-life.

389
00:19:23,280 --> 00:19:28,800
To just rightly seeing it as it is, this is samma pajan.

390
00:19:28,800 --> 00:19:30,080
So, we mood teach it.

391
00:19:30,080 --> 00:19:33,920
So, we mood touch it, though, with a mind that is well released.

392
00:19:33,920 --> 00:19:37,320
Well, released means, and there are different kinds of release,

393
00:19:37,320 --> 00:19:40,680
the commentary talks about these, but in the next page.

394
00:19:40,680 --> 00:19:44,320
But the most important one is, it's like the covering up

395
00:19:44,320 --> 00:19:45,920
of the roof.

396
00:19:45,920 --> 00:19:47,440
You can cover it up with morality.

397
00:19:47,440 --> 00:19:49,680
You can cover it up with concentration.

398
00:19:49,680 --> 00:19:53,680
The most important is to cover, cover up the roof with wisdom.

399
00:19:53,680 --> 00:19:58,160
To create a seal with wisdom so that your mind has no,

400
00:19:58,160 --> 00:20:02,640
there's no room for it to arise, because the mind is perfectly

401
00:20:02,640 --> 00:20:06,920
airtight, perfect, complete knowledge.

402
00:20:06,920 --> 00:20:08,960
You know the truth.

403
00:20:08,960 --> 00:20:10,720
I mean, basically it means to see the bottom,

404
00:20:10,720 --> 00:20:13,440
because once you see the bottom, your mind is perfectly

405
00:20:13,440 --> 00:20:14,640
protected.

406
00:20:14,640 --> 00:20:17,440
The first time when a person becomes a salt of the bottom,

407
00:20:17,440 --> 00:20:21,480
the mind is perfectly protected from,

408
00:20:21,480 --> 00:20:26,720
or is perfectly freed from, what is it?

409
00:20:26,720 --> 00:20:32,680
Sakaya-Diti, a set of view of self, Ritzi-Kutat,

410
00:20:32,680 --> 00:20:36,320
Sila-Battaparamasa, which means attachment

411
00:20:36,320 --> 00:20:41,240
to wrong practices and wrong morality.

412
00:20:41,240 --> 00:20:44,240
So, abstaining from things that it's not necessary to abstain

413
00:20:44,240 --> 00:20:46,760
from taking on practice that are in practice

414
00:20:46,760 --> 00:20:48,520
is that are not necessary to practice.

415
00:20:48,520 --> 00:20:51,760
Thinking that by abstaining and by practicing in this way,

416
00:20:51,760 --> 00:20:53,240
or thinking that there's abstentions

417
00:20:53,240 --> 00:20:55,120
and these practices are necessary.

418
00:20:55,120 --> 00:20:57,960
You never do that because you know what is the right practice,

419
00:20:57,960 --> 00:21:02,480
and you know what leads to the result, because you've been there.

420
00:21:02,480 --> 00:21:04,720
And we teach each other, which means no doubt,

421
00:21:04,720 --> 00:21:06,640
because you've seen the bandit.

422
00:21:06,640 --> 00:21:08,700
You have no doubt about whether the bandit exists

423
00:21:08,700 --> 00:21:10,360
or a little bit of thought.

424
00:21:10,360 --> 00:21:13,400
Because you come to see the truth from yourself.

425
00:21:13,400 --> 00:21:17,520
And then when you get Sakithikami and the gami and anahana-kah,

426
00:21:17,520 --> 00:21:19,440
and you become anahana-kahana, and then the mind is

427
00:21:19,440 --> 00:21:24,520
so we move that, which means well free,

428
00:21:24,520 --> 00:21:29,080
There's no greed, no anger, no delusion, no ignorance,

429
00:21:29,080 --> 00:21:32,440
to full understanding of the formable truth.

430
00:21:32,440 --> 00:21:34,640
Everything that you experience is seen just

431
00:21:34,640 --> 00:21:35,480
for what it is.

432
00:21:35,480 --> 00:21:42,520
There's some up at Daniel, some at Daniel, so that's

433
00:21:42,520 --> 00:21:49,600
the final story of the Yamakawaga, a group of pairs,

434
00:21:49,600 --> 00:21:51,800
the first Vadha.

435
00:21:51,800 --> 00:21:55,960
And it's a reminder for us that what we are doing

436
00:21:55,960 --> 00:21:58,120
is the proper practice of the Buddha.

437
00:21:58,120 --> 00:22:00,160
We should be proud because many people,

438
00:22:00,160 --> 00:22:04,880
even people who are born Buddhist or have found Buddhism,

439
00:22:04,880 --> 00:22:08,440
Westerners who find Buddhism, will often just spend

440
00:22:08,440 --> 00:22:11,520
their life studying and talking and discussing

441
00:22:11,520 --> 00:22:15,560
and trying to appreciate intellectual and they consider

442
00:22:15,560 --> 00:22:20,720
intellectual appreciation to be the realization.

443
00:22:20,720 --> 00:22:23,280
And they miss the fruit of the holy life because they

444
00:22:23,280 --> 00:22:25,960
don't actually practice it.

445
00:22:25,960 --> 00:22:29,640
So it should feel proud, not proud and conceited,

446
00:22:29,640 --> 00:22:32,480
but you should feel confident and feel good about yourself.

447
00:22:32,480 --> 00:22:34,360
And we should remind ourselves that what we're doing

448
00:22:34,360 --> 00:22:36,440
is a good thing.

449
00:22:36,440 --> 00:22:41,120
Reminders, as means, look at the benefits that we came and not

450
00:22:41,120 --> 00:22:44,800
disparage the wisdom that we're gaining in the practice.

451
00:22:44,800 --> 00:22:46,920
It's a very moment you're gaining something,

452
00:22:46,920 --> 00:22:49,320
learning something more about your soul.

453
00:22:49,320 --> 00:22:52,160
On the other side, at the same time, it's easy to become discouraged

454
00:22:52,160 --> 00:22:55,080
because it's very difficult and you're

455
00:22:55,080 --> 00:22:57,120
experiencing things, you're dealing with things

456
00:22:57,120 --> 00:23:00,760
that you're still used to covering up and avoiding.

457
00:23:00,760 --> 00:23:04,440
So when they come up again, there's the instinct

458
00:23:04,440 --> 00:23:08,240
is to repress them to push them more and to run away from them.

459
00:23:08,240 --> 00:23:12,880
So it can be quite disheartening and some people actually

460
00:23:12,880 --> 00:23:14,640
decide they want to run away and they don't want

461
00:23:14,640 --> 00:23:16,400
to deal with it anymore.

462
00:23:16,400 --> 00:23:17,720
It's important to remind ourselves

463
00:23:17,720 --> 00:23:20,520
that this is really what we've gained already

464
00:23:20,520 --> 00:23:24,360
or what we're doing here is something quite important

465
00:23:24,360 --> 00:23:26,240
and quite incredible.

466
00:23:26,240 --> 00:23:29,920
And if you remember the goodness of what we're doing,

467
00:23:29,920 --> 00:23:34,640
be able to don't lose sight of what we've gained or what

468
00:23:34,640 --> 00:23:38,200
we're gaining because you're learning all the time about yourself,

469
00:23:38,200 --> 00:23:41,960
about how the mind works, about how reality works.

470
00:23:41,960 --> 00:23:47,240
And that's a very precious gift.

471
00:23:47,240 --> 00:23:49,680
So that's the story for today.

472
00:23:49,680 --> 00:23:51,360
And it's an absolutely good amount of time

473
00:23:51,360 --> 00:23:55,080
to finish this session.

474
00:23:55,080 --> 00:24:00,200
And tomorrow or whenever we'll get on to the second chapter.

475
00:24:00,200 --> 00:24:18,120
So thanks for tuning in and back to a little Tushman.

