1
00:00:00,000 --> 00:00:07,000
When you thought of being a monk, where you were asking yourself too much questions like us here,

2
00:00:07,000 --> 00:00:12,000
do you think we are relying too much on the idea,

3
00:00:12,000 --> 00:00:17,000
addition level of consciousness?

4
00:00:17,000 --> 00:00:21,000
Yeah, too many questions.

5
00:00:21,000 --> 00:00:27,000
Well, some questions are a little bit much, but most of the questions seem to be really,

6
00:00:27,000 --> 00:00:30,000
really well thought out, and important to people.

7
00:00:30,000 --> 00:00:33,000
I think a lot of the questions here are actually important to people.

8
00:00:33,000 --> 00:00:36,000
You guys deal with a lot more than I do.

9
00:00:36,000 --> 00:00:39,000
You have a lot more factors to deal with,

10
00:00:39,000 --> 00:00:42,000
and it makes things more complicated.

11
00:00:42,000 --> 00:00:46,000
It's a lot easier for someone on the outside to help you out with that,

12
00:00:46,000 --> 00:00:51,000
and a lot of the helping out is just to bring you back to a more simple,

13
00:00:51,000 --> 00:00:54,000
or pull you into a more simple life.

14
00:00:54,000 --> 00:00:59,000
Help you to rearrange things so that you live a more simple life,

15
00:00:59,000 --> 00:01:05,000
so that you think in a more simple way that you live in a more simple,

16
00:01:05,000 --> 00:01:09,000
you live your life more simply.

17
00:01:09,000 --> 00:01:12,000
But that's not the first question.

18
00:01:12,000 --> 00:01:14,000
It's not either question, really.

19
00:01:14,000 --> 00:01:21,000
But asking too many questions, was I asking too many questions?

20
00:01:21,000 --> 00:01:23,000
She, I don't know.

21
00:01:23,000 --> 00:01:25,000
I didn't have people to ask them.

22
00:01:25,000 --> 00:01:28,000
I wasn't really asking a lot of questions, no.

23
00:01:28,000 --> 00:01:36,000
But what I would say is that I was misled or misled.

24
00:01:36,000 --> 00:01:41,000
I was totally in the dark,

25
00:01:41,000 --> 00:01:44,000
so I didn't know, I think the problem here,

26
00:01:44,000 --> 00:01:47,000
and that's often a problem, is people think they should ask something.

27
00:01:47,000 --> 00:01:51,000
And so they come here, and they think, well,

28
00:01:51,000 --> 00:01:52,000
okay, so this is Buddhism.

29
00:01:52,000 --> 00:01:54,000
You come and ask questions of the monk,

30
00:01:54,000 --> 00:01:56,000
which is a bit of a problem.

31
00:01:56,000 --> 00:01:58,000
I mean, that's the negative side of this,

32
00:01:58,000 --> 00:02:00,000
is that people get the idea that, well,

33
00:02:00,000 --> 00:02:01,000
this is their Buddhist practice,

34
00:02:01,000 --> 00:02:04,000
is to come and ask questions.

35
00:02:04,000 --> 00:02:07,000
Because we're a general,

36
00:02:07,000 --> 00:02:09,000
when before we've practiced meditation,

37
00:02:09,000 --> 00:02:12,000
we're in the dark, we don't really know what is the path.

38
00:02:12,000 --> 00:02:14,000
I went to Thailand looking for wisdom,

39
00:02:14,000 --> 00:02:16,000
and that's what I said with the meditations,

40
00:02:16,000 --> 00:02:18,000
and I said, why are you coming to meditate?

41
00:02:18,000 --> 00:02:20,000
I said, I want to find wisdom.

42
00:02:20,000 --> 00:02:22,000
And I said, oh, that's a good thing.

43
00:02:22,000 --> 00:02:24,000
But it was absurd, because I hadn't a clue

44
00:02:24,000 --> 00:02:25,000
what wisdom was.

45
00:02:25,000 --> 00:02:30,000
And when I found, or when I began to develop wisdom,

46
00:02:30,000 --> 00:02:34,000
I turned out to be totally otherwise

47
00:02:34,000 --> 00:02:35,000
than what I thought.

48
00:02:35,000 --> 00:02:37,000
And it's a surprise when you develop,

49
00:02:37,000 --> 00:02:40,000
when you really gain wisdom.

50
00:02:40,000 --> 00:02:42,000
It totally shocks you.

51
00:02:42,000 --> 00:02:44,000
It's totally other than what you think.

52
00:02:44,000 --> 00:02:50,000
So asking questions here is incredibly limited.

53
00:02:50,000 --> 00:02:53,000
It shouldn't be ever be considered your practice,

54
00:02:53,000 --> 00:02:56,000
or it shouldn't ever be considered to solve things.

55
00:02:56,000 --> 00:02:59,000
If anything, it should be,

56
00:02:59,000 --> 00:03:02,000
if I'm answering these questions correctly,

57
00:03:02,000 --> 00:03:06,000
it should incline you more towards meditation,

58
00:03:06,000 --> 00:03:10,000
incline you more to find the answers.

59
00:03:10,000 --> 00:03:13,000
I'm not answering, I'm not giving you the answers.

60
00:03:13,000 --> 00:03:17,000
I'm trying, and hopefully, I don't know if I'm succeeding,

61
00:03:17,000 --> 00:03:22,000
but trying to lead you to encourage you

62
00:03:22,000 --> 00:03:26,000
to find the answers, to look for the answers.

63
00:03:26,000 --> 00:03:29,000
The best thing would be if people came away

64
00:03:29,000 --> 00:03:31,000
thinking, I want to go on a meditation course.

65
00:03:31,000 --> 00:03:35,000
I want to go off and really dedicate myself to this.

66
00:03:35,000 --> 00:03:38,000
It's the problem with the meditation videos,

67
00:03:38,000 --> 00:03:41,000
and even a booklet on how to meditate great.

68
00:03:41,000 --> 00:03:43,000
They've helped a lot of people,

69
00:03:43,000 --> 00:03:46,000
but the one downside,

70
00:03:46,000 --> 00:03:49,000
and it's not a reason to stop,

71
00:03:49,000 --> 00:03:53,000
but the problem is that people get the idea that that's enough.

72
00:03:53,000 --> 00:03:55,000
And so they say, if they do say,

73
00:03:55,000 --> 00:03:57,000
I'm going to go on a meditation course,

74
00:03:57,000 --> 00:04:00,000
they think it'll be just the same as practicing meditation at home.

75
00:04:00,000 --> 00:04:02,000
And then it just totally blows them away

76
00:04:02,000 --> 00:04:08,000
because it's so much, it's on a totally different level.

77
00:04:08,000 --> 00:04:10,000
So, yeah, you should never be content

78
00:04:10,000 --> 00:04:15,000
with this sort of an activity.

79
00:04:15,000 --> 00:04:17,000
This is just for people who are new,

80
00:04:17,000 --> 00:04:19,000
I would say,

81
00:04:19,000 --> 00:04:22,000
well, people who come back and have some free time

82
00:04:22,000 --> 00:04:24,000
from their meditation,

83
00:04:24,000 --> 00:04:26,000
and they want to hear the dhamma,

84
00:04:26,000 --> 00:04:30,000
listen to the dhamma, maybe even share the dhamma themselves.

85
00:04:30,000 --> 00:04:34,000
So, I would say don't ask too many questions

86
00:04:34,000 --> 00:04:38,000
and don't rely too much on the ideation level of consciousness

87
00:04:38,000 --> 00:04:40,000
whether people are or not.

88
00:04:40,000 --> 00:04:43,000
I get the feeling that people are fairly sincere here.

89
00:04:43,000 --> 00:05:07,000
So, hope that helps.

