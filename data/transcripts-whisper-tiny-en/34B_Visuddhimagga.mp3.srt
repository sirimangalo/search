1
00:00:00,000 --> 00:00:17,520
The one that's having the carver to wouldn't use the words.

2
00:00:17,520 --> 00:00:28,520
Yeah, past is that we are going to need a load of, and then Jago, and 15 days ago, and

3
00:00:28,520 --> 00:00:30,520
moved to then another year.

4
00:00:30,520 --> 00:00:35,520
Because this was, what is, what is the common relation with this?

5
00:00:35,520 --> 00:00:40,520
Same here is, all these words are the synonymous of nibana.

6
00:00:40,520 --> 00:01:09,520
So, all these words are the synonymous of nibana.

7
00:01:09,520 --> 00:01:15,520
For the ultimate sense, it is nibana that is called the noble tradition of suffering.

8
00:01:15,520 --> 00:01:22,520
But, because creving face the way and sees it on coming to death, on coming to death really

9
00:01:22,520 --> 00:01:25,520
means depending upon death.

10
00:01:25,520 --> 00:01:33,520
That is, when consciousness arises, it takes nibana as content.

11
00:01:33,520 --> 00:01:44,520
When there is nibana as content can fuck consciousness arise, and only the fuck consciousness

12
00:01:44,520 --> 00:01:47,520
abandons with mental defilements.

13
00:01:47,520 --> 00:01:55,520
So, nibana is called freedom of will, and nibana is called cessation.

14
00:01:55,520 --> 00:02:10,520
Actually, freedom of will means the cause of freedom of will, the cause of cessation.

15
00:02:10,520 --> 00:02:17,520
And, because there comes to be the giving of, etc., of death, craving on coming to death.

16
00:02:17,520 --> 00:02:23,520
And, since there is not even one kind of reliance here, it would be dependent upon,

17
00:02:23,520 --> 00:02:29,520
from among the reliance that is consisting in the cause of sense, desires, etc., it is there

18
00:02:29,520 --> 00:02:35,520
who call, hearing an out, rolling question is, letting it grow, not relying on death.

19
00:02:35,520 --> 00:02:43,520
So, all these expressions are the synonymous for nibana.

20
00:02:43,520 --> 00:02:48,520
So, nibana is not giving up actually, but the cause of giving up.

21
00:02:48,520 --> 00:02:55,520
The cause of rolling question, the cause of letting go, the cause of not relying.

22
00:02:55,520 --> 00:03:02,520
That means nibana is something, not just, not just the act of giving up, not just the

23
00:03:02,520 --> 00:03:09,520
act of rolling question, not just letting go, not just not relying, but something.

24
00:03:09,520 --> 00:03:21,520
That, that helps, for consciousness, if you give up, you will be rolling question, so on.

25
00:03:21,520 --> 00:03:27,520
It is a speech, as it is characteristic, its function is not being done, or its function

26
00:03:27,520 --> 00:03:28,520
is being done for it.

27
00:03:28,520 --> 00:03:35,520
Is it manifested as the single sinless, or is it manifested as non-iversification?

28
00:03:35,520 --> 00:03:42,520
Now, in the footnotes, the, what nibpa can tell is just explained.

29
00:03:42,520 --> 00:03:57,520
You, you, you, you, you, you later, later on.

30
00:03:57,520 --> 00:04:04,520
And also, we put not on, on coming to death, footnotes on the 16th, on PC, on 577.

31
00:04:04,520 --> 00:04:08,520
It is put to read that, and footnotes.

32
00:04:08,520 --> 00:04:14,520
The value of act is used here, used here is agama.

33
00:04:14,520 --> 00:04:33,520
So, I have got to turn it on.

34
00:04:33,520 --> 00:04:38,520
I will have to take on the video.

35
00:04:38,520 --> 00:04:43,520
No, I will read that for you.

36
00:04:43,520 --> 00:04:46,520
I will read the video.

37
00:04:46,520 --> 00:04:47,520
Yes.

38
00:04:47,520 --> 00:04:48,520
Yes.

39
00:04:48,520 --> 00:04:57,520
No.

40
00:04:57,520 --> 00:05:02,520
No.

41
00:05:02,520 --> 00:05:06,520
No discussion on diva.

42
00:05:06,520 --> 00:05:15,520
So, these are the, questions and answers, like arguments.

43
00:05:15,520 --> 00:05:21,520
One is the opponent and the other is the defender, some you like that.

44
00:05:21,520 --> 00:05:27,520
Question number one, is nibba are not exist as because it is untapply handable, like

45
00:05:27,520 --> 00:05:28,520
the players form.

46
00:05:28,520 --> 00:05:35,520
So, the opponent said there is no nibba, and nibba does not exist in it, because it cannot

47
00:05:35,520 --> 00:05:37,520
be every handable, like it, he has gone.

48
00:05:37,520 --> 00:05:40,520
So, there is no such thing as it has gone.

49
00:05:40,520 --> 00:05:43,520
So, like that, there is no nibba that at all.

50
00:05:43,520 --> 00:05:51,520
So, the answer is that it is not so, because it is every handable by the right means,

51
00:05:51,520 --> 00:05:57,520
or it is every handable by some namely the noble ones, by the right means.

52
00:05:57,520 --> 00:06:04,520
In other words, whether way, that is appropriate way, a way of virtue, concentration and understanding

53
00:06:04,520 --> 00:06:05,520
of business.

54
00:06:05,520 --> 00:06:10,520
It is like the Superman-Dame consciousness of others, which is every handable only by

55
00:06:10,520 --> 00:06:16,520
certain of the noble ones, by means of knowledge of penetration of others' minds.

56
00:06:16,520 --> 00:06:20,520
So, it is like the Superman-Dame consciousness.

57
00:06:20,520 --> 00:06:27,520
The Superman-Dame consciousness can be every hundred only by those who have attained

58
00:06:27,520 --> 00:06:34,520
enlightenment or who have attained the Superman-Dame consciousness.

59
00:06:34,520 --> 00:06:43,520
The higher the higher the noble person can know the Superman-Dame consciousness of the

60
00:06:43,520 --> 00:06:50,520
noble person.

61
00:06:50,520 --> 00:06:59,520
So, those who have reached the second stage can know the Superman-Dame consciousness

62
00:06:59,520 --> 00:07:02,520
of those of the first stage and so on.

63
00:07:02,520 --> 00:06:57,480
So, like that, in the

64
00:06:57,480 --> 00:07:22,480
Superman-Dame consciousness of those of the first stage and so on.

65
00:07:22,480 --> 00:07:29,480
But, what the foolish form there is not every hand, it is untapplichedable.

66
00:07:29,480 --> 00:07:36,480
These people will say it is untapplichedable, but it is not untapplichedable.

67
00:07:36,480 --> 00:07:40,480
That we call this, say it is.

68
00:07:40,480 --> 00:07:44,480
And then, we define our continues.

69
00:07:44,480 --> 00:07:48,480
Again, if you understand that, that's not exist, why not?

70
00:07:48,480 --> 00:07:53,480
Because it then follows that the will will be huda, and the will means the practice,

71
00:07:53,480 --> 00:07:56,480
and the practice of the noble, a full path.

72
00:07:56,480 --> 00:07:59,480
So, it will be non-sense.

73
00:07:59,480 --> 00:08:06,480
If you say there is no nibbana, for if nibbana are not existence, then it will follow

74
00:08:06,480 --> 00:08:11,480
that the right way, which includes the three advocates beginning with virtue and is tethered

75
00:08:11,480 --> 00:08:14,480
by right understanding would be huda.

76
00:08:14,480 --> 00:08:17,480
And it is not huda because it does not reach nibbana.

77
00:08:17,480 --> 00:08:22,480
Now, I don't like the word aggregates here.

78
00:08:22,480 --> 00:08:25,480
It then may make you confused.

79
00:08:25,480 --> 00:08:30,480
I just want to say group.

80
00:08:30,480 --> 00:08:36,480
Yeah, group beginning with virtue means favor samadhi and banya.

81
00:08:36,480 --> 00:08:45,480
There are described as a body, described in body as silakanda, samadhi kanda, banya kanda.

82
00:08:45,480 --> 00:08:51,480
But kanda really means not the aggregates like rupa kanda and so on.

83
00:08:51,480 --> 00:08:53,480
So, we can just include.

84
00:08:53,480 --> 00:08:58,480
So, the group beginning with virtue and is tethered by right understanding, it will be huda.

85
00:08:58,480 --> 00:09:01,480
And it is not huda because it does reach nibbana.

86
00:09:01,480 --> 00:09:04,480
So, you cannot say that there is no nibbana.

87
00:09:04,480 --> 00:09:08,480
If you say there is no nibbana, then it hurts.

88
00:09:08,480 --> 00:09:09,480
It will be meaningless.

89
00:09:09,480 --> 00:09:16,480
But, but it is not meaningless if one leads to nibbana, so there is nibbana, there is one

90
00:09:16,480 --> 00:09:17,480
to three other men.

91
00:09:17,480 --> 00:09:22,480
But, in number two, the futility of the way does not follow because what is reached is

92
00:09:22,480 --> 00:09:24,480
f chance or not existance.

93
00:09:24,480 --> 00:09:29,480
There is f chance of the first everyday, consequence of the cutting of the departments.

94
00:09:29,480 --> 00:09:38,480
So, after cutting of the department, the fire everyday do not exist.

95
00:09:38,480 --> 00:09:46,480
Then, it is not so because though there is f chance of first and future aggregates, there

96
00:09:46,480 --> 00:09:50,480
is nevertheless no reaching of nibbana, simply because of that.

97
00:09:50,480 --> 00:09:59,480
Now, here the opponent says the f chance of first aggregates to be nibbana.

98
00:09:59,480 --> 00:10:11,480
But, if the f chance of aggregates is nibbana, then the path is not not existance now.

99
00:10:11,480 --> 00:10:13,480
The futility is not existance now.

100
00:10:13,480 --> 00:10:18,480
And so, there would be nibbana.

101
00:10:18,480 --> 00:10:24,480
But there is no reaching of nibbana, simply because the path is not present now, the future

102
00:10:24,480 --> 00:10:29,480
is not present now, the path aggregates are no better than the future aggregates are no

103
00:10:29,480 --> 00:10:30,480
present.

104
00:10:30,480 --> 00:10:40,480
But, if you see the f chance of non existance of the first everyday, it is nibbana,

105
00:10:40,480 --> 00:10:42,480
then it will be otherwise.

106
00:10:42,480 --> 00:10:48,480
Then, the third question, then is the f chance of present aggregates as well nibbana, then

107
00:10:48,480 --> 00:10:55,480
the f chance of present aggregates are nibbana, could that not be nibbana?

108
00:10:55,480 --> 00:11:02,480
The opponent asks that it is not true because it is contradiction in terms.

109
00:11:02,480 --> 00:11:10,480
The present and non existance, the present cannot be absent, right?

110
00:11:10,480 --> 00:11:18,480
Because the essence is an impossibility, since if the f chance, they are non present follows.

111
00:11:18,480 --> 00:11:24,480
Besides the nibbana, our absence of present aggregates is dead, and they will follow,

112
00:11:24,480 --> 00:11:32,480
excluding the arising of the nibbana element, not arising, reaching, excluding the reaching

113
00:11:32,480 --> 00:11:39,480
of the nibbana element, which results with result of task cleaning left at the first moment,

114
00:11:39,480 --> 00:11:44,480
which has present aggregates as it supports now.

115
00:11:44,480 --> 00:11:51,480
The second part of the paragraph means that, that would end with the fourth of excluding

116
00:11:51,480 --> 00:12:00,480
the reaching of the nibbana element, with the result of task cleaning left and so on.

117
00:12:00,480 --> 00:12:07,480
That means, at the moment of task consciousness, there are aggregates, right?

118
00:12:07,480 --> 00:12:12,480
Aggregates are in existence.

119
00:12:12,480 --> 00:12:23,480
If you say, the essence of the present aggregates are nibbana, then they should be no reaching

120
00:12:23,480 --> 00:12:28,480
of nibbana at the moment of task consciousness, because at the moment of task consciousness,

121
00:12:28,480 --> 00:12:31,480
there are aggregates.

122
00:12:31,480 --> 00:12:40,480
And then they are going to be no nibbana at the moment of task consciousness.

123
00:12:40,480 --> 00:12:45,480
And then nibbana is the nibbana with the result of task cleaning left.

124
00:12:45,480 --> 00:12:50,480
That means, nibbana before death.

125
00:12:50,480 --> 00:12:53,480
There are two, two kinds of nibbana.

126
00:12:53,480 --> 00:12:54,480
We will find a data.

127
00:12:54,480 --> 00:13:01,480
The nibbana before the death of Arahan and big nibbana, one day, Arahan dies.

128
00:13:01,480 --> 00:13:08,480
So here, the nibbana, one day, and Arahan dies, and there are no, no, every game.

129
00:13:08,480 --> 00:13:10,480
So then nibbana is possible.

130
00:13:10,480 --> 00:13:13,480
But this another kind of, another nibbana is not possible.

131
00:13:13,480 --> 00:13:20,480
It should take the absence of every case to be nibbana, because aggregates are not

132
00:13:20,480 --> 00:13:27,480
absent at the moment of task consciousness.

133
00:13:27,480 --> 00:13:36,480
Then the full question, then will there be no force if it is non-presence of defalments

134
00:13:36,480 --> 00:13:38,480
that is nibbana?

135
00:13:38,480 --> 00:13:43,480
Then just being non-presence of, or non-existence of defalments.

136
00:13:43,480 --> 00:13:47,480
And if that be nibbana, then there will be no form.

137
00:13:47,480 --> 00:13:54,480
Then it is not true, because it would then follow that the noble purpose meaningless.

138
00:13:54,480 --> 00:14:01,480
For if it was so then since defalments can be not exist and also before the moment of

139
00:14:01,480 --> 00:14:06,480
the noble path, it followed that the noble path would be meaningless.

140
00:14:06,480 --> 00:14:15,480
Non-existence of the defalments, there can be non-existence of defalments

141
00:14:15,480 --> 00:14:19,480
before the path arises.

142
00:14:19,480 --> 00:14:22,480
Sometimes they don't arise, right?

143
00:14:22,480 --> 00:14:24,480
It defalments.

144
00:14:24,480 --> 00:14:30,480
Then if that is nibbana, then the path would be meaningless.

145
00:14:30,480 --> 00:14:38,480
You don't have to try to read the thought, because you always have nibbana.

146
00:14:38,480 --> 00:14:45,480
So this is what the, what you call it?

147
00:14:45,480 --> 00:14:48,480
It is full of, no, no.

148
00:14:48,480 --> 00:14:53,480
It is the opponent, right?

149
00:14:53,480 --> 00:14:57,480
The opponent and the, I don't know what you call this, defender.

150
00:14:57,480 --> 00:15:02,480
So this is one set of question and answers.

151
00:15:02,480 --> 00:15:08,480
Then question five thing, but it's not even a destruction because of the basis.

152
00:15:08,480 --> 00:15:16,480
Now here the question is on another aspect.

153
00:15:16,480 --> 00:15:19,480
Okay, okay, that is nibbana.

154
00:15:19,480 --> 00:15:27,480
But nibbana is just the destruction of, destruction of great effort and delusion.

155
00:15:27,480 --> 00:15:36,480
So there is no such, no, no separate thing is nibbana, but just be destruction.

156
00:15:36,480 --> 00:15:39,480
Just the disappearance of defalments is nibbana.

157
00:15:39,480 --> 00:15:42,480
It takes that way.

158
00:15:42,480 --> 00:15:51,480
And in part of the passage, the friend, which is the destruction of great of hate, of the illusion is nibbana.

159
00:15:51,480 --> 00:16:03,480
Then the defalments, that is not slow, because if you follow that relationship, that means I have to follow the fruit.

160
00:16:03,480 --> 00:16:06,480
Also, what's near destruction?

161
00:16:06,480 --> 00:16:14,480
For that two is describing the same way beginning that friend, which is the destruction of great, of hate, of delusion is a relationship.

162
00:16:14,480 --> 00:16:27,480
Now, the answer here is, if you follow the purposes, which is the destruction of great, of hate, or delusion is nibbana.

163
00:16:27,480 --> 00:16:39,480
Following that passage, if you say the nibbana is a mere destruction, then there is another passage in the same shoulder, which stays there.

164
00:16:39,480 --> 00:16:43,480
Their hardship is destruction.

165
00:16:43,480 --> 00:16:53,480
So, a hardship also would be mere destruction.

166
00:16:53,480 --> 00:16:57,480
So, it is not going to be taken that way.

167
00:16:57,480 --> 00:16:59,480
So, it is not true.

168
00:16:59,480 --> 00:17:12,480
We are not to take that nibbana is, not, nibbana is mere destruction, or mere disappearance of mental defalments.

169
00:17:12,480 --> 00:17:17,480
And, what is more, the father said that follow the nibbana would be temporary, etc.

170
00:17:17,480 --> 00:17:24,480
Or, if it was so, if it was a destruction of great effort and delusion, it would follow the nibbana would be temporary.

171
00:17:24,480 --> 00:17:31,480
That's the characteristic of being formed, and the obtainable regardless of right effort.

172
00:17:31,480 --> 00:17:39,480
And, precisely because of its heavy form, characteristics, it would be included in the form, and it would be burning with fire that would create, etc.

173
00:17:39,480 --> 00:17:42,480
And, because of its burning, it would follow that it was suffering.

174
00:17:42,480 --> 00:17:44,480
Nibbana would be suffering.

175
00:17:44,480 --> 00:17:50,480
Because, nibbana is the destruction of disappearance of mental defalments.

176
00:17:50,480 --> 00:17:59,480
Since, mental defalments are formed, they are burning, and they are dogha, then nibbana would also be dogha.

177
00:17:59,480 --> 00:18:05,480
So, it should not be taken that way.

178
00:18:05,480 --> 00:18:13,480
Is there no fallacy, if nibbana is that kind of destruction, subsequent to which there is no more occurrence?

179
00:18:13,480 --> 00:18:19,480
That means, suppose, you have mental defalments.

180
00:18:19,480 --> 00:18:32,480
I mean, you have mental defalments, and they don't appear again, then that destruction could be called nibbana.

181
00:18:32,480 --> 00:18:38,480
That is, what the opponent want to say.

182
00:18:38,480 --> 00:18:53,480
Suppose, it was in practice as Jana, and then, by the practice of Jana, he could put off, or he could avoid the defalments from arising.

183
00:18:53,480 --> 00:19:00,480
Not by total destruction, but just by putting them away.

184
00:19:00,480 --> 00:19:06,480
Then, after that, he gets nibbana, I mean, he gets enlightenment.

185
00:19:06,480 --> 00:19:17,480
Then, nibbana would not be at the moment of enlightenment, but at the time when he could put off the defalments.

186
00:19:17,480 --> 00:19:27,480
That is what is meant by the opponent.

187
00:19:27,480 --> 00:19:35,480
So, if they are no fallacy, if nibbana is that kind of destruction, subsequent to which there is no more occurrence.

188
00:19:35,480 --> 00:19:39,480
That is not true, because there is no such kind of destruction.

189
00:19:39,480 --> 00:19:42,480
And even if they are war, they afford that fallacy.

190
00:19:42,480 --> 00:19:49,480
That means, it would be temporary, that the characteristic of new form, and so on, but not be avoided.

191
00:19:49,480 --> 00:19:54,480
And because it would follow that the noble part was nibbana.

192
00:19:54,480 --> 00:19:58,480
For the noble part, cause that the destruction of defect.

193
00:19:58,480 --> 00:20:01,480
And that is why it is called destruction.

194
00:20:01,480 --> 00:20:05,480
And subsequent, there is no more occurrence of defect.

195
00:20:05,480 --> 00:20:10,480
So, after the part consciousness, there are no more mental defalments.

196
00:20:10,480 --> 00:20:15,480
So, the part would be called nibbana, and not nibbana itself.

197
00:20:15,480 --> 00:20:21,480
But it is because the kind of destruction calls that this inconsistent and not arising, that is nibbana.

198
00:20:21,480 --> 00:20:30,480
So, figuratively speaking, as decisive support for the part, that nibbana is called destruction, as a metaphor for it.

199
00:20:30,480 --> 00:20:34,480
Do you understand this?

200
00:20:34,480 --> 00:20:36,480
No.

201
00:20:36,480 --> 00:20:38,480
There are two things.

202
00:20:38,480 --> 00:20:44,480
Sassation, consisting a non-arising, and nibbana.

203
00:20:44,480 --> 00:21:04,480
Nibbana serves as a decisive support, simply means, let us say, as an object.

204
00:21:04,480 --> 00:21:17,480
So, since nibbana serves as a decisive support for the part, nibbana is said to be the cause.

205
00:21:17,480 --> 00:21:26,480
And, Sassation, consisting a non-arising is effect.

206
00:21:26,480 --> 00:21:42,480
But, here, nibbana is called destruction, and that is by metaphor.

207
00:21:42,480 --> 00:21:48,480
You know, in the passage called it, nibbana is described as destruction.

208
00:21:48,480 --> 00:21:57,480
So, destruction of great hatred and inclusion is nibbana.

209
00:21:57,480 --> 00:22:05,480
So, there, we have to understand that destruction is not really nibbana.

210
00:22:05,480 --> 00:22:13,480
But, nibbana is described as destruction, because it is the cause of condition for destruction.

211
00:22:13,480 --> 00:22:19,480
Nibbana serves as a condition for destruction, that is why nibbana is also called destruction,

212
00:22:19,480 --> 00:22:20,480
by a metaphor.

213
00:22:20,480 --> 00:22:25,480
So, it is not a direct, direct expression.

214
00:22:25,480 --> 00:22:34,480
There are some, there are some kinds of metaphor in this way.

215
00:22:34,480 --> 00:22:45,480
So, in a body, you see, let us say, sugar is flam.

216
00:22:45,480 --> 00:22:49,480
If you eat much sugar, you have flam.

217
00:22:49,480 --> 00:22:52,480
So, people say, sugar is flam.

218
00:22:52,480 --> 00:22:56,480
Something like, say, salt is, say, blood pressure.

219
00:22:56,480 --> 00:23:00,480
Salt is not blood pressure, but it is a cause of blood pressure.

220
00:23:00,480 --> 00:23:04,480
So, some that we say, salt is blood pressure, something like that.

221
00:23:04,480 --> 00:23:10,480
Here, nibbana is destruction, nibbana is not actually destruction, but it is a cause

222
00:23:10,480 --> 00:23:11,480
of destruction.

223
00:23:11,480 --> 00:23:13,480
It is a condition for destruction.

224
00:23:13,480 --> 00:23:17,480
That is why nibbana is explained, stated as destruction.

225
00:23:17,480 --> 00:23:20,480
So, it is not a right talk.

226
00:23:20,480 --> 00:23:25,480
That is why in the next question, why is it not stated in its form?

227
00:23:25,480 --> 00:23:29,480
That means, why does not stated directly?

228
00:23:29,480 --> 00:23:35,480
Why is it not called a cause for destruction or condition for destruction?

229
00:23:35,480 --> 00:23:40,480
Then, the answer is, because it is extremely subtle.

230
00:23:40,480 --> 00:23:49,480
So, you can go to, go to, hesitate to teach this demo, because it is so subtle.

231
00:23:49,480 --> 00:23:54,480
And so, it would not be readily understood by the listeners.

232
00:23:54,480 --> 00:24:01,480
Yes, because there is a problem here, because you say, even that is a cause of destruction,

233
00:24:01,480 --> 00:24:06,480
you need to come to be for destruction.

234
00:24:06,480 --> 00:24:12,480
A cause for means, a condition for, even that does not cause this destruction, but

235
00:24:12,480 --> 00:24:15,480
even as a condition for destruction.

236
00:24:15,480 --> 00:24:18,480
But it does not make it sound like destruction.

237
00:24:18,480 --> 00:24:27,480
This is the next step after, you know, the step after it is a higher step.

238
00:24:27,480 --> 00:24:33,480
Nibbana, we cannot say that Nibbana is past or present or future.

239
00:24:33,480 --> 00:24:39,480
So, Nibbana is some kind of, some kind of demo.

240
00:24:39,480 --> 00:24:48,480
But when fat consciousness arises, it takes Nibbana as object.

241
00:24:48,480 --> 00:24:52,480
And at the same time, it is mental defilements.

242
00:24:52,480 --> 00:24:54,480
So, destruction is there.

243
00:24:54,480 --> 00:24:59,480
Instruction of mental defilements is at the moment of fat consciousness.

244
00:24:59,480 --> 00:25:03,480
And fast consciousness can arise only when it takes Nibbana as object.

245
00:25:03,480 --> 00:25:08,480
If there is no such thing at Nibbana, then fat consciousness can not arise.

246
00:25:08,480 --> 00:25:15,480
But that is what Nibbana is said to be a condition for the destruction of mental defilements.

247
00:25:15,480 --> 00:25:25,480
Okay, not your finished sense, it is, but it is not uncreated.

248
00:25:25,480 --> 00:25:35,480
If it is so, then only when there is fat, there is the destruction of mental defilements.

249
00:25:35,480 --> 00:25:38,480
So, and it is taken as Nibbana.

250
00:25:38,480 --> 00:25:40,480
So, Nibbana is not uncreated.

251
00:25:40,480 --> 00:25:42,480
Nibbana is made by something.

252
00:25:42,480 --> 00:25:45,480
It is what the opponent says.

253
00:25:45,480 --> 00:25:49,480
That is not so because it is not a rousable by the thought.

254
00:25:49,480 --> 00:25:54,480
That means it is not produced by crack.

255
00:25:54,480 --> 00:26:01,480
It is only reachable by the thought.

256
00:26:01,480 --> 00:26:03,480
That is why it is uncreated.

257
00:26:03,480 --> 00:26:07,480
It is because it is uncreated, that it is free from aging.

258
00:26:07,480 --> 00:26:13,480
It is because of the essence of its creation, then it is aging, and then it is permanent.

259
00:26:13,480 --> 00:26:15,480
Then you say permanent.

260
00:26:15,480 --> 00:26:18,480
So, there is another.

261
00:26:18,480 --> 00:26:21,480
There is another argument.

262
00:26:21,480 --> 00:26:26,480
Then it follows the Nibbana to just a kind of permanent claim of the Adam and so on.

263
00:26:26,480 --> 00:26:31,480
I think this is not correct.

264
00:26:31,480 --> 00:26:35,480
What the opponent says here is, you say Nibbana is permanent.

265
00:26:35,480 --> 00:26:41,480
So, if Nibbana is permanent, then the Adam and etc are also permanent.

266
00:26:41,480 --> 00:26:49,480
The Adam also etc are taught in other philosophies.

267
00:26:49,480 --> 00:26:55,480
You know there are six classical Hindu philosophies.

268
00:26:55,480 --> 00:27:02,480
Maybe we call it dark darshan in Sanskrit darshan.

269
00:27:02,480 --> 00:27:14,480
In some of these philosophies, they accept the Adam as the cause of creation.

270
00:27:14,480 --> 00:27:24,480
The atoms are the real, they are taken to be indestructible to be permanent.

271
00:27:24,480 --> 00:27:38,480
So, if you say Nibbana is permanent, then the atoms also are also permanent, something like that.

272
00:27:38,480 --> 00:27:58,480
So, then it follows the Nibbana, Nibbana has a kind of permanent of the Adam.

273
00:27:58,480 --> 00:28:01,480
No, no, no, not this.

274
00:28:01,480 --> 00:28:08,480
Then the answer is that is not so because of the absence of any cause that brings about its

275
00:28:08,480 --> 00:28:10,480
narrating, here it is also not correct.

276
00:28:10,480 --> 00:28:15,480
Because of the absence of any reason, not cause.

277
00:28:15,480 --> 00:28:18,480
That can prove its truth.

278
00:28:18,480 --> 00:28:23,480
Not that brings about its arising.

279
00:28:23,480 --> 00:28:31,480
In this passage, the terms used in Hindu logic are used.

280
00:28:31,480 --> 00:28:37,480
So, you have to understand Hindu logic in order to understand these persons and be subcommendary

281
00:28:37,480 --> 00:28:38,480
on this.

282
00:28:38,480 --> 00:28:40,480
But it is very difficult.

283
00:28:40,480 --> 00:28:55,480
So, what he too means reason, if I am going to have this a logism in logic, I wrote a book

284
00:28:55,480 --> 00:28:47,480
on Hindu

285
00:28:47,480 --> 00:28:56,480
a logism in Burmese.

286
00:28:56,480 --> 00:28:59,480
And I have forgotten about that.

287
00:28:59,480 --> 00:29:02,480
So, I have to read it again.

288
00:29:02,480 --> 00:29:09,480
So, there is what is called as a logism in Hindu logic.

289
00:29:09,480 --> 00:29:22,480
And that is actually you use as something like the classical example is, there is fire

290
00:29:22,480 --> 00:29:32,480
on the body, because there is smoke.

291
00:29:32,480 --> 00:29:37,480
Wherever there is smoke, there is fire, for example, a kitchen.

292
00:29:37,480 --> 00:29:40,480
And there is smoke on the body.

293
00:29:40,480 --> 00:29:43,480
Therefore, the monitoring test is fine.

294
00:29:43,480 --> 00:29:46,480
So, these are the five stages of cell voices.

295
00:29:46,480 --> 00:29:55,480
So, in order to make other people convinced and you have to use these five steps.

296
00:29:55,480 --> 00:30:03,480
So, the first one is the statement of the fire on the body.

297
00:30:03,480 --> 00:30:10,480
Then why, somebody may ask, because there is the smoke.

298
00:30:10,480 --> 00:30:15,480
How, how, how a smoke and fire are you related?

299
00:30:15,480 --> 00:30:19,480
And he says, wherever there is smoke, there is fire.

300
00:30:19,480 --> 00:30:22,480
For example, you see the kitchen.

301
00:30:22,480 --> 00:30:25,480
Not, not kitchen in the middle of the state.

302
00:30:25,480 --> 00:30:27,480
You use cash, yes.

303
00:30:27,480 --> 00:30:31,480
And there are no smoke, there is no smoke.

304
00:30:31,480 --> 00:30:34,480
So, wherever there is smoke, there is fire.

305
00:30:34,480 --> 00:30:35,480
For example, kitchen.

306
00:30:35,480 --> 00:30:38,480
And now, there is smoke there on the monitoring.

307
00:30:38,480 --> 00:30:40,480
So, there is fire on the monitoring.

308
00:30:40,480 --> 00:30:42,480
So, this is called cell voices.

309
00:30:42,480 --> 00:30:47,480
And the second, second sentence is called he to reason.

310
00:30:47,480 --> 00:30:51,480
Why, why do you see that there is fire on the monitoring?

311
00:30:51,480 --> 00:30:53,480
Because, I see the smoke there.

312
00:30:53,480 --> 00:30:56,480
So, that sentence is called he to.

313
00:30:56,480 --> 00:31:03,480
So, that is not called, that is a reason given by, by, by the person.

314
00:31:03,480 --> 00:31:12,480
So, here, because of the absence of any reason, there is no, no reason to prove that atoms

315
00:31:12,480 --> 00:31:18,480
are permanent.

316
00:31:18,480 --> 00:31:26,480
Because nipana has permanence, then these, that is, the atom etcetera are permanent as well.

317
00:31:26,480 --> 00:31:33,480
And the next question is, because nipana is permanent, the atoms are also permanent.

318
00:31:33,480 --> 00:31:37,480
Then, the answer is what?

319
00:31:37,480 --> 00:31:39,480
That is not so.

320
00:31:39,480 --> 00:31:47,480
Because in that proposition, the characteristic of logical reason, not cause, logical reason,

321
00:31:47,480 --> 00:31:52,480
is not valid, not, does not arise.

322
00:31:52,480 --> 00:32:04,480
So, because the characteristic of logical reason, is not valid.

323
00:32:04,480 --> 00:32:12,480
In other words, you see the nipana is permanent, is not to assert a reason, why the atom etcetera

324
00:32:12,480 --> 00:32:15,480
should be permanent.

325
00:32:15,480 --> 00:32:29,480
So, because according to, to theta or the coldest, the, the permanency of atoms and others

326
00:32:29,480 --> 00:32:34,480
are not, not proved, say, the idea are not true.

327
00:32:34,480 --> 00:32:42,480
So, here to see that, because nipana is, is permanent, they are permanent.

328
00:32:42,480 --> 00:32:50,480
It is not, it is not a valid, valid, a reason, valid statement.

329
00:32:50,480 --> 00:32:58,480
So, the reason here is not valid.

330
00:32:58,480 --> 00:33:07,480
Then, they are permanent, because of the absence of the atom, as nipana is, that is not

331
00:33:07,480 --> 00:33:13,480
valid.

332
00:33:13,480 --> 00:33:14,480
Because the atom and so on have not been established as facts, there is according to

333
00:33:14,480 --> 00:33:15,480
the goodest thing.

334
00:33:15,480 --> 00:33:20,480
The, of course, that logical reason improved that, not only this, that nipana is permanent,

335
00:33:20,480 --> 00:33:27,480
it says, because it is uncreated, and there should be periods there, and we should,

336
00:33:27,480 --> 00:33:32,480
we should strike out and, and then it should begin with the capital added.

337
00:33:32,480 --> 00:33:39,480
It is immaterial, because it transcends the individual essence of manifest, another statement.

338
00:33:39,480 --> 00:33:49,480
So, questions 9, 10, and 11, in, in, in, in these question analysis, the logical,

339
00:33:49,480 --> 00:33:56,480
the logical terms they use, and we have to understand, in the logic, to, to understand these,

340
00:33:56,480 --> 00:34:04,480
you understand these, these passages clearly.

341
00:34:04,480 --> 00:34:07,480
And then, it is described as immaterial.

342
00:34:07,480 --> 00:34:13,480
So, they were nice, nipana, nipana, or arupa.

343
00:34:13,480 --> 00:34:21,480
Although, it is not consciousness, it is not mental factor, but it is nipana, it is arupa.

344
00:34:21,480 --> 00:34:29,480
And then, the Buddha's goal is fun, and has no clue, to, to, to, to learn, to learn.

345
00:34:29,480 --> 00:34:36,480
Actually, what is man here is, it is a single goal, because there is no difference in goal

346
00:34:36,480 --> 00:34:38,480
of Buddha, etcetera.

347
00:34:38,480 --> 00:34:51,480
So, the Buddha's goal of Buddha's, the Buddha's enlightenment, the Buddha's destruction of the

348
00:34:51,480 --> 00:34:55,480
the five man's and others' destruction of the five man's of the same.

349
00:34:55,480 --> 00:35:01,480
So, here, the Buddha's goal is fun, and has no clue, with the, I think that is not so good.

350
00:35:01,480 --> 00:35:11,480
So, there is, it is a single goal, because there is no difference in goal of Buddha, etcetera.

351
00:35:11,480 --> 00:35:19,480
And then, we describe the two kinds of nipana, nipana, with a result of past-winding, left,

352
00:35:19,480 --> 00:35:23,480
and nipana, without a result of past-winding, left.

353
00:35:23,480 --> 00:35:29,480
That's simply means, nipana, while the arahan is spinning, or it would be good.

354
00:35:29,480 --> 00:35:34,480
And then we were not addicted of an amount.

355
00:35:34,480 --> 00:35:38,480
Before he dies, he still has the aggregate.

356
00:35:38,480 --> 00:35:40,480
He still has his body and mind.

357
00:35:40,480 --> 00:35:45,480
So we are called the result of past cleaning.

358
00:35:45,480 --> 00:35:49,480
With the result of past cleaning means, what is it like?

359
00:35:49,480 --> 00:36:03,480
And without result of past cleaning, that means, when he dies.

360
00:36:03,480 --> 00:36:09,480
And then paragraph done before, because it can be arrived at by distinction of knowledge that

361
00:36:09,480 --> 00:36:12,480
succeeds through entire perseverance.

362
00:36:12,480 --> 00:36:18,480
And because it is the words, not word, the word of the omniscient word,

363
00:36:18,480 --> 00:36:24,480
it is not non-existent as regards individual questions in this ultimate sense.

364
00:36:24,480 --> 00:36:30,480
But this is said, people there is an unborn and unbecome and unmade and unformed.

365
00:36:30,480 --> 00:36:36,480
This is a famous message quoted by many, many people.

366
00:36:36,480 --> 00:36:41,480
So this is a section of the definition, dealing with the description of the cessation of suffering.

367
00:36:41,480 --> 00:36:50,480
That is not true.

368
00:36:50,480 --> 00:37:00,480
And I am going to end up with four noble children.

369
00:37:00,480 --> 00:37:12,480
You know, you said that our hardship, that really means, what do you want to say?

370
00:37:12,480 --> 00:37:13,480
Arahatapala.

371
00:37:13,480 --> 00:37:28,480
I mean, you know, but part of an arahan and fruit, part of arahan-shipping fruit of arahan-shipping

372
00:37:28,480 --> 00:37:38,480
fruit, you know, you don't feel oil in you.

373
00:37:38,480 --> 00:37:44,480
So we want to say that that's the same as diva and then we'll come to this.

374
00:37:44,480 --> 00:37:46,480
No, no.

375
00:37:46,480 --> 00:37:50,480
Diva is separate in Dhamma.

376
00:37:50,480 --> 00:38:04,480
It's one thing fruit is another thing.

377
00:38:04,480 --> 00:38:24,480
Yes.

378
00:38:24,480 --> 00:38:43,480
You know, you'll cross over into a digital robot and make it.

379
00:38:43,480 --> 00:38:48,480
Right.

380
00:38:48,480 --> 00:39:02,480
Right.

381
00:39:02,480 --> 00:39:12,480
Right.

382
00:39:12,480 --> 00:39:36,480
Right.

383
00:39:36,480 --> 00:39:50,480
Yeah.

384
00:39:50,480 --> 00:40:14,480
I'm not asking.

385
00:40:14,480 --> 00:40:28,480
Right.

