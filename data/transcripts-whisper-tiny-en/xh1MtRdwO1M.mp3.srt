1
00:00:00,000 --> 00:00:26,000
Okay, good evening everyone, welcome to our evening talk.

2
00:00:26,000 --> 00:00:47,000
We have a new meditator today from Winnipeg who is actually an old meditator.

3
00:00:47,000 --> 00:01:03,000
He's finally found the time to coordinate and do the full foundation course in our tradition.

4
00:01:03,000 --> 00:01:16,000
At the same time we're losing a meditator, a graduate who has just finished the foundation course

5
00:01:16,000 --> 00:01:27,000
passed with flying colors, graduated and moving on, but he'll be back.

6
00:01:27,000 --> 00:01:35,000
The dhamma is addictive, well not really is it.

7
00:01:35,000 --> 00:01:40,000
That's the problem with the dhamma actually is that it's not addictive.

8
00:01:40,000 --> 00:01:53,000
It's the opposite of clinging and so you don't create an addiction towards it.

9
00:01:53,000 --> 00:01:58,000
But you do create an attraction, it's kind of a paradox in a sense.

10
00:01:58,000 --> 00:02:08,000
You do somehow become addicted to it, addicted through wisdom.

11
00:02:08,000 --> 00:02:16,000
It appears though you're an addict, person can't stop meditating, you must be addicted to it.

12
00:02:16,000 --> 00:02:20,000
Which is just a fool's way of showing their misunderstanding.

13
00:02:20,000 --> 00:02:35,000
The inability to understand the lack of alarm to superficial, just a superficial comment.

14
00:02:35,000 --> 00:02:39,000
Wisdom is a lot like addiction in that way.

15
00:02:39,000 --> 00:02:47,000
When you know something's right, you can't avoid it.

16
00:02:47,000 --> 00:03:00,000
When you know something's wrong, you can't take part in it.

17
00:03:00,000 --> 00:03:06,000
So congratulations for that.

18
00:03:06,000 --> 00:03:14,000
I propose tonight's talk, as tonight's talk I was going to talk.

19
00:03:14,000 --> 00:03:21,000
I thought the topic would be desire, but there's a little bit more to it.

20
00:03:21,000 --> 00:03:35,000
I'm going to talk about the general concept of the path.

21
00:03:35,000 --> 00:03:44,000
Meaning the path of desire versus the path of renunciation.

22
00:03:44,000 --> 00:03:51,000
Is there a way of understanding our options?

23
00:03:51,000 --> 00:03:53,000
Because this is a true dichotomy.

24
00:03:53,000 --> 00:03:56,000
You can't, there's no in between.

25
00:03:56,000 --> 00:03:58,000
It's one or the other.

26
00:03:58,000 --> 00:04:01,000
Mahasi Sayedas says they're diametrically opposed.

27
00:04:01,000 --> 00:04:09,000
I don't know what the actual Burmese words he used are probably not exact translation, but

28
00:04:09,000 --> 00:04:14,000
this is their opposites.

29
00:04:14,000 --> 00:04:19,000
You can't do both.

30
00:04:19,000 --> 00:04:30,000
Or rather, they lead an opposite direction, so most of us find ourselves in the middle being

31
00:04:30,000 --> 00:04:40,000
pulled in both direction.

32
00:04:40,000 --> 00:04:44,000
Sometimes engaging in enjoying nice sensations.

33
00:04:44,000 --> 00:04:50,000
You might have pleasant sights or sounds or smells or tastes or feelings.

34
00:04:50,000 --> 00:04:55,000
You might engage in them repeatedly.

35
00:04:55,000 --> 00:05:19,000
But they're not complementary, it's the point.

36
00:05:19,000 --> 00:05:32,000
So the context is we had an interesting class last night on the theory of the study of religion.

37
00:05:32,000 --> 00:05:54,000
That was on the question of whether phenomena like sports, sports, loyalty, team loyalty,

38
00:05:54,000 --> 00:06:03,000
or fandom, like the Trekkies or the Jedi Knights.

39
00:06:03,000 --> 00:06:10,000
Our guest speaker was a put down Jedi Knight on the census as his religion.

40
00:06:10,000 --> 00:06:16,000
He explained why that wasn't actually the word, I think.

41
00:06:16,000 --> 00:06:27,000
Not obvious reasons for doing it, but the discussion was whether certain phenomena that appeared

42
00:06:27,000 --> 00:06:37,000
to be quite religious or at least involving a great amount of sincerity and dedication,

43
00:06:37,000 --> 00:06:44,000
whether they could be called religious.

44
00:06:44,000 --> 00:06:55,000
As I won't go into great details, an interesting conversation, a couple of points that came up were...

45
00:06:55,000 --> 00:07:09,000
Well, the main point was that they don't touch upon anything meaningful.

46
00:07:09,000 --> 00:07:19,000
Could you argue that something has to touch upon what is ultimately meaningful for it to be a religion?

47
00:07:19,000 --> 00:07:26,000
And so I mean, I was the one who posed this and I suggested that you could argue that something is only a religion

48
00:07:26,000 --> 00:07:30,000
and this is what the courts often argue.

49
00:07:30,000 --> 00:07:35,000
There's been lots of challenges to the tax exempt status of religions,

50
00:07:35,000 --> 00:07:45,000
and that it should include things like smoking marijuana or it should include vegan movements,

51
00:07:45,000 --> 00:07:49,000
veganism or raw food movements.

52
00:07:49,000 --> 00:08:06,000
It should include the Church of the Flying Spaghetti Monster, for example.

53
00:08:06,000 --> 00:08:18,000
But they said that these various movements don't have cohesive doctrine that is sincerely held regarding ultimate questions of ultimate concern.

54
00:08:18,000 --> 00:08:23,000
The meaning of life, etc.

55
00:08:23,000 --> 00:08:28,000
Which in the end I think is an unfair assessment, but that's not the point.

56
00:08:28,000 --> 00:08:39,000
The point in the end was that loyalty to a sports team may seem quite religious.

57
00:08:39,000 --> 00:08:46,000
And so I said, well, I can understand, I can accept that it might be a religion, but it's a pretty silly religion.

58
00:08:46,000 --> 00:08:48,000
It's the point.

59
00:08:48,000 --> 00:08:50,000
And this is the interesting point of it all.

60
00:08:50,000 --> 00:08:58,000
Not what is a religion and what is not a religion, but the fact that we can rank religions.

61
00:08:58,000 --> 00:09:04,000
We're not allowed to, and this is sort of the real problem with the word religion is that

62
00:09:04,000 --> 00:09:08,000
call something a religion and suddenly you have to respect it.

63
00:09:08,000 --> 00:09:15,000
Suddenly it's equal to all other religions and so the concern with allowing something to be called

64
00:09:15,000 --> 00:09:21,000
a religion is that well, then you have to respect it and who wants to think of

65
00:09:21,000 --> 00:09:31,000
fan worship of Captain Kirk as the same as the worship of Jesus Christ or Buddha or so on.

66
00:09:31,000 --> 00:09:38,000
I want to be able to distinguish, I don't think I'm setting up a straw man.

67
00:09:38,000 --> 00:09:44,000
I think many people want to distinguish because they consider religion to be something in a category of its own.

68
00:09:44,000 --> 00:09:50,000
Two e-gennarists, that's Latin.

69
00:09:50,000 --> 00:10:08,000
It's a fancy Latin phrase that means of its own type, something in its own, in a category of its own.

70
00:10:08,000 --> 00:10:19,000
And so where this is all going is the recognition that there are many paths.

71
00:10:19,000 --> 00:10:32,000
But denying that all paths lead to the same goal, that all paths are equally valid, that all paths should be considered equally,

72
00:10:32,000 --> 00:10:54,000
be given the same sort of attention.

73
00:10:54,000 --> 00:11:07,000
And so that led today during my French lesson, I was talking to my French tutor and I gave him a lesson in Buddhism at the end, trying to explain it in French and then switching to English.

74
00:11:07,000 --> 00:11:17,000
And his question was why shouldn't you engage in enjoyable things if they make you happy?

75
00:11:17,000 --> 00:11:24,000
And he was trying to struggle with it and to argue, but be careful because he didn't want to threaten my religion.

76
00:11:24,000 --> 00:11:29,000
He obviously didn't agree with me, but he didn't want to get into her that just debate.

77
00:11:29,000 --> 00:11:42,000
So he phrased how things must be or that you can't do things, because all things you do must be, must advance you as a person.

78
00:11:42,000 --> 00:11:47,000
I said, well, no, actually, it's not like that.

79
00:11:47,000 --> 00:11:53,000
It's a question of what you want out of life and where you're heading.

80
00:11:53,000 --> 00:12:09,000
I think this is what is unfamiliar to a lot of ordinary people is that there exists paths, there exist methods,

81
00:12:09,000 --> 00:12:18,000
that lead beyond this ordinary state of existence that most people are intellectually comfortable with.

82
00:12:18,000 --> 00:12:23,000
Most people are intellectually comfortable with suffering.

83
00:12:23,000 --> 00:12:28,000
They are physically or practically comfortable with it.

84
00:12:28,000 --> 00:12:38,000
They moan and complain like everybody else, but if you ask them, hey, wouldn't you like to change?

85
00:12:38,000 --> 00:12:43,000
The way the world works?

86
00:12:43,000 --> 00:12:46,000
I mean, really, if you ask them, would you like to practice meditation?

87
00:12:46,000 --> 00:12:52,000
I've got this practice where you could become completely free from all the sufferings of life.

88
00:12:52,000 --> 00:12:59,000
Most people, I think their initial reaction is, okay, suffering is okay.

89
00:12:59,000 --> 00:13:04,000
I'm okay with this.

90
00:13:04,000 --> 00:13:10,000
But really, what's saying that is the desire they have and an attachment,

91
00:13:10,000 --> 00:13:19,000
there are many attachments to pleasures and to ambitions and so on.

92
00:13:19,000 --> 00:13:25,000
And then the ego behind it, the identification, so the idea of leaving behind some sorrow,

93
00:13:25,000 --> 00:13:37,000
the idea of nibana is frightening, horrific even, abhorrent to most people, to not be born again.

94
00:13:37,000 --> 00:13:45,000
I mean, actually, many people are struggling under the delusion that we only have one life.

95
00:13:45,000 --> 00:13:51,000
And to deny this, the pleasures of this life is one life.

96
00:13:51,000 --> 00:14:09,000
How horrible would that be? What a horrific thought.

97
00:14:09,000 --> 00:14:18,000
So it's important to clarify, we're not talking about giving up happiness in order to give up suffering.

98
00:14:18,000 --> 00:14:22,000
We're not talking about an even trade here.

99
00:14:22,000 --> 00:14:30,000
These aren't on an equal footing.

100
00:14:30,000 --> 00:14:34,000
When we talk about the ordinary path of desire and diversion,

101
00:14:34,000 --> 00:14:41,000
the ordinary path of desire, aversion, anger, delusion, that most people are on.

102
00:14:41,000 --> 00:14:47,000
We have all these things and engage in them regularly.

103
00:14:47,000 --> 00:14:51,000
This is a very coarse path.

104
00:14:51,000 --> 00:15:01,000
This is a path that is fraught with problems and is working on a level of constant delusion,

105
00:15:01,000 --> 00:15:07,000
of constant confusion and darkness, lack of clarity.

106
00:15:07,000 --> 00:15:18,000
The mind for such an individual, for most individuals, is a sleep, diffused, distracted,

107
00:15:18,000 --> 00:15:22,000
muddied, muddled, all these things.

108
00:15:22,000 --> 00:15:33,000
Of course, constantly agitated, constantly excited,

109
00:15:33,000 --> 00:15:42,000
and fraught with worry and fear and anxiety and stress.

110
00:15:42,000 --> 00:15:49,000
Even when seeking out the objects of pleasure or even when attaining them,

111
00:15:49,000 --> 00:15:59,000
there is a distraction, there is lack of clarity in the mind.

112
00:15:59,000 --> 00:16:04,000
And so really all we're talking about in Buddhism is not giving anything up,

113
00:16:04,000 --> 00:16:08,000
per se, not directly.

114
00:16:08,000 --> 00:16:14,000
It's about a path that is on a higher level and higher in a very pejorative sense,

115
00:16:14,000 --> 00:16:17,000
or whatever the opposite or pejorative is.

116
00:16:17,000 --> 00:16:20,000
It really is a pejorative lower.

117
00:16:20,000 --> 00:16:29,000
That's even a phrase. It's a really, truly lower path to seek out central pleasure.

118
00:16:29,000 --> 00:16:36,000
He no gamo, pouto, pouto, genico, analeo, another sanchito,

119
00:16:36,000 --> 00:16:39,000
but called it inferior.

120
00:16:39,000 --> 00:16:46,000
Gamo, the way of the village.

121
00:16:46,000 --> 00:16:53,000
All we're doing in meditation practice is creating a higher state of consciousness,

122
00:16:53,000 --> 00:17:00,000
a greater sense of clarity.

123
00:17:00,000 --> 00:17:04,000
And through the increase of clarity, things change.

124
00:17:04,000 --> 00:17:09,000
We refine our pursuits.

125
00:17:09,000 --> 00:17:17,000
We discard the pursuits that are clearly causing us stress and suffering and problem.

126
00:17:17,000 --> 00:17:20,000
Clearly now that our minds are more clear.

127
00:17:20,000 --> 00:17:22,000
It really is that simple.

128
00:17:22,000 --> 00:17:27,000
It's not a matter of deciding, should I give up happiness in order to be free from suffering?

129
00:17:27,000 --> 00:17:29,000
It's not at all like that.

130
00:17:29,000 --> 00:17:31,000
In fact, greater happiness comes.

131
00:17:31,000 --> 00:17:40,000
Our refined, pure, exalted, blissful, wonderful sense of happiness comes.

132
00:17:40,000 --> 00:17:44,000
Of course, my meditators are all like, yeah, right.

133
00:17:44,000 --> 00:17:51,000
I'm here torturing people in Canada, and then I talk about bliss and happiness.

134
00:17:51,000 --> 00:17:53,000
Well, our graduates know.

135
00:17:53,000 --> 00:17:56,000
Our graduates can tell you.

136
00:17:56,000 --> 00:18:04,000
It's a great struggle to rise above the, rise above the dirt, the merc,

137
00:18:04,000 --> 00:18:11,000
to break free from the chains, the chains of desire.

138
00:18:11,000 --> 00:18:24,000
But it's a struggle unequivocally worthwhile.

139
00:18:24,000 --> 00:18:40,000
And this isn't just a claim that I make him, and this is really how we should understand

140
00:18:40,000 --> 00:18:44,000
the practice and how we should be clearly evident from the practice that we do.

141
00:18:44,000 --> 00:18:45,000
We're not judging.

142
00:18:45,000 --> 00:18:48,000
We're not even trying to change ourselves.

143
00:18:48,000 --> 00:18:59,000
Beyond the simple change of seeing our experiences, even our own minds, and our own habits, clearly.

144
00:18:59,000 --> 00:19:03,000
The wheat puss in the, seeing clearly.

145
00:19:03,000 --> 00:19:06,000
It's not some hard to understand concept.

146
00:19:06,000 --> 00:19:09,000
Things arise and cease.

147
00:19:09,000 --> 00:19:20,000
The ordinary state of mind is to, to fair blindly grasping and clinging to anything,

148
00:19:20,000 --> 00:19:27,000
and cultivating habits with no rhyme or reason.

149
00:19:27,000 --> 00:19:29,000
It's like turning on the light.

150
00:19:29,000 --> 00:19:36,000
When you shine the light in the darkness disappears.

151
00:19:36,000 --> 00:19:40,000
When you're able to see it quite easily.

152
00:19:40,000 --> 00:19:45,000
And suddenly, quite clear, come all the things that were previously mysterious.

153
00:19:45,000 --> 00:19:46,000
Why do I suffer?

154
00:19:46,000 --> 00:19:48,000
Why am I depressed?

155
00:19:48,000 --> 00:19:56,000
How do I free myself from desire and anger and aversion?

156
00:19:56,000 --> 00:20:05,000
I'll become clear.

157
00:20:05,000 --> 00:20:09,000
So there you go.

158
00:20:09,000 --> 00:20:14,000
I mean, that's about all I had to say tonight, just some food for thought.

159
00:20:14,000 --> 00:20:23,000
And I guess including the encouragement, not to be, not to lose, lose sight of this,

160
00:20:23,000 --> 00:20:28,000
or to lose faith, or to be caught up in the pursuit of desire,

161
00:20:28,000 --> 00:20:39,000
the pursuit of ordinary happiness,

162
00:20:39,000 --> 00:20:45,000
to engage in, and pursue, clarity of mind,

163
00:20:45,000 --> 00:20:50,000
ability to see things as they are more clearly than before.

164
00:20:50,000 --> 00:20:56,000
It allows us to refine our happiness and to create greater states of peace

165
00:20:56,000 --> 00:21:02,000
and goodness in our hearts and to free ourselves from all the troubles that come from

166
00:21:02,000 --> 00:21:06,000
evil and wholesome mind states.

167
00:21:06,000 --> 00:21:08,000
So good for you, everyone.

168
00:21:08,000 --> 00:21:11,000
Keep up the good work.

169
00:21:11,000 --> 00:21:13,000
All right, there you go.

170
00:21:13,000 --> 00:21:17,000
That's the dhamma for tonight.

171
00:21:17,000 --> 00:21:21,000
Thank you all.

172
00:21:21,000 --> 00:21:26,000
If anyone has any questions, I'm happy to answer them.

173
00:21:51,000 --> 00:21:56,000
Thank you.

174
00:22:21,000 --> 00:22:30,000
Thank you for coming, everyone.

175
00:22:30,000 --> 00:22:32,000
I guess there are no questions.

176
00:22:32,000 --> 00:22:36,000
If there are no questions, we'll say goodnight then.

177
00:22:36,000 --> 00:22:40,000
And then there's a question.

178
00:22:40,000 --> 00:22:42,000
He started by mentioning meditation as addicting,

179
00:22:42,000 --> 00:22:47,000
and so my question is when did the Buddha mention the path as being beautiful in the beginning,

180
00:22:47,000 --> 00:22:52,000
in the middle, and as we most often do not focus in the beauties of life?

181
00:22:52,000 --> 00:22:56,000
Yeah, the word isn't quite beautiful.

182
00:22:56,000 --> 00:22:57,000
It's caliana.

183
00:22:57,000 --> 00:23:05,000
Caliana means good, bright, or beautiful would be so bad.

184
00:23:05,000 --> 00:23:08,000
Super.

185
00:23:08,000 --> 00:23:10,000
Super would be beautiful.

186
00:23:10,000 --> 00:23:14,000
But caliana, I can't remember the etymology,

187
00:23:14,000 --> 00:23:17,000
but it's a specific word.

188
00:23:17,000 --> 00:23:20,000
We use it for a friend, like a caliana mitta is a beautiful friend,

189
00:23:20,000 --> 00:23:23,000
but there's nothing to do with their good looks.

190
00:23:23,000 --> 00:23:31,000
It's just a wonderful friend, an awesome friend.

191
00:23:31,000 --> 00:23:35,000
So caliana means good in the beginning,

192
00:23:35,000 --> 00:23:37,000
and that's often translated in that way.

193
00:23:37,000 --> 00:23:40,000
It's good in the beginning, good in the middle.

194
00:23:40,000 --> 00:23:40,040
It's the bhayana, brahma chari

195
00:23:45,000 --> 00:23:47,000
Thang sa bhayan jianang,

196
00:23:47,000 --> 00:23:40,440
Kayvala paripun nang par RM

197
00:23:54,440 --> 00:23:57,080
And it is the brahma chari fast, that is,

198
00:23:57,080 --> 00:24:01,060
raw complete, in both its letter and its meaning,

199
00:24:01,060 --> 00:24:03,980
meaning that all the teachings are there,

200
00:24:03,980 --> 00:24:07,000
and the meaning is perfect,

201
00:24:07,000 --> 00:24:21,000
Gave a parypundang totally and completely complete, parysudang and pure.

202
00:24:21,000 --> 00:24:26,000
The practice isn't addictive, I was trying to say it seems addictive, because people

203
00:24:26,000 --> 00:24:39,000
who engage in it, it leads you on, the more you gain, the more you are the inclination

204
00:24:39,000 --> 00:24:53,000
to gain or to strive for gain, because of wisdom, not because of attachment.

205
00:24:53,000 --> 00:25:02,000
Gave a parypundang.

206
00:25:02,000 --> 00:25:26,000
Gave a parypundang.

207
00:25:26,000 --> 00:25:55,000
I've posted a bug report for the broadcasting software that I use.

208
00:25:55,000 --> 00:26:03,000
Other people are confirming the bug, and they say just wait for the next version of this

209
00:26:03,000 --> 00:26:10,000
software to come out, unless I can figure out how to compile it, which is a mystery

210
00:26:10,000 --> 00:26:19,000
to me how that works, but there are ways of getting the software.

211
00:26:19,000 --> 00:26:27,000
Anyway, so at some point soon the point is I'll probably be doing live YouTube broadcasts again.

212
00:26:27,000 --> 00:26:34,000
Which doesn't mean we can't do second life as well, because I can always log into second life and just minimize it.

213
00:26:34,000 --> 00:26:48,000
The thing about Linux, it seems like I can port the audio to four different sources at once, which is kind of cool.

214
00:26:48,000 --> 00:26:56,000
I don't know if that happens in Windows as well, but anyway, let's end it there then.

215
00:26:56,000 --> 00:26:58,000
Thank you all for coming out.

216
00:26:58,000 --> 00:27:01,000
It's great to have a crowd every night.

217
00:27:01,000 --> 00:27:25,000
We'll try to be back here again tomorrow.

