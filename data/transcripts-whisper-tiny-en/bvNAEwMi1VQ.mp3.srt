1
00:00:00,000 --> 00:00:10,580
Adam asks, does Buddhism share any similarity with non-dualism, ideas like we are all one

2
00:00:10,580 --> 00:00:16,840
consciousness experiencing itself subjectively, or that everything is nothing and nothing

3
00:00:16,840 --> 00:00:37,400
is everything, etc. There are many different views about that. I would say there is, in Mahayana

4
00:00:37,400 --> 00:00:46,820
Buddhism, certainly they do say these things that everything is one and we are all one

5
00:00:46,820 --> 00:00:58,560
and there are other sects. There is nothing and everything is nothing and nothing is

6
00:00:58,560 --> 00:01:16,240
everything and so on. It should probably not be just so generalized because it is both

7
00:01:16,240 --> 00:01:28,500
true. It just depends on the level in which you look at it. There are certainly

8
00:01:28,500 --> 00:01:45,660
experiences of being one with everything in meditation and there are experiences of

9
00:01:45,660 --> 00:02:06,700
nothingness. I think to be fair with it to be objective, we should consider that everything

10
00:02:06,700 --> 00:02:15,940
is possible but there is only one truth in the end and that is that things are arising

11
00:02:15,940 --> 00:02:24,000
and seizing. No matter what we think about them, no matter if we think, oh we are all one

12
00:02:24,000 --> 00:02:38,860
or if we think everything is nothing, it is just thinking and it is just an experience

13
00:02:38,860 --> 00:02:54,940
and it is impermanent, so it is thinking. The argument kind of reminds me of this one

14
00:02:54,940 --> 00:03:01,620
discourse, the way we bunkers it or something, I don't remember the name of horrible, this

15
00:03:01,620 --> 00:03:07,300
scholarly stuff, but there is just one truth over these two monks. I think our monk and

16
00:03:07,300 --> 00:03:14,940
the lay person were fighting over this. It is not the same question but a similar question.

17
00:03:14,940 --> 00:03:19,940
Similar numbers question, similar in the sense that it is just numbers. One of them said

18
00:03:19,940 --> 00:03:23,660
there are three types of feelings. The Buddha has taught that there are three types of

19
00:03:23,660 --> 00:03:29,820
feelings. The monk said, no no, the Buddha taught that there are five types of weight

20
00:03:29,820 --> 00:03:34,260
and feeling and they argued back and forth and back and forth and then they went to the

21
00:03:34,260 --> 00:03:40,660
Buddha. They went to Ananda, I think and Ananda brought it to the Buddha and the Buddha

22
00:03:40,660 --> 00:03:47,660
said actually there are 108 types of feelings and I think it goes because of the past

23
00:03:47,660 --> 00:03:56,420
present future because of the six senses you can multiply it out. The idea of non-duality

24
00:03:56,420 --> 00:04:08,740
is just a theory in the concept and there is nothing to do with what is real. Why I thought

25
00:04:08,740 --> 00:04:13,300
of this is because I was thinking there are six things. We don't have one thing or two

26
00:04:13,300 --> 00:04:17,660
things. We have six things, seeing, hearing, smelling, tasting, feeling, thinking. If you

27
00:04:17,660 --> 00:04:25,580
want to say there is one thing and there is only experience or there is only reality. The

28
00:04:25,580 --> 00:04:30,340
one sense that I don't agree with things like non-dualism is the idea that Nibana is

29
00:04:30,340 --> 00:04:36,620
the same as Samsara because to me that is a fundamental misunderstanding of the difference

30
00:04:36,620 --> 00:04:45,180
between an arising and a ceasing or an arising and a non-arising. Phenomena do arrives.

31
00:04:45,180 --> 00:04:54,340
The non-arising is Nibana. The arising is Samsara. There are cases where it seems

32
00:04:54,340 --> 00:04:59,420
fundamentally untrue but the whole idea of us being one, well, you know, you could say

33
00:04:59,420 --> 00:05:04,940
that we are two or you could say that we are three or you could say that we are six. In

34
00:05:04,940 --> 00:05:09,940
the end it is what it is. It is Panyan. He says it is just experience. I don't know.

35
00:05:09,940 --> 00:05:16,660
Maybe that wasn't such an important point but the important most important point is that

36
00:05:16,660 --> 00:05:25,260
it is all just thinking. It is all just ideas and views really but whether things are

37
00:05:25,260 --> 00:05:54,340
dualistic or non-dualistic.

