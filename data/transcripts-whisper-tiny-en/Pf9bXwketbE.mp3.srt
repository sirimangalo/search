1
00:00:00,000 --> 00:00:18,000
Okay, so do the realizations of impermanence, suffering, and non-self have to be, or are they supposed to be intellectual?

2
00:00:18,000 --> 00:00:32,000
No, and in fact, in a sense, the experience of impermanence, suffering, and non-self is the absence, right?

3
00:00:32,000 --> 00:00:41,000
Because they're all negative aspects or negative characteristics.

4
00:00:41,000 --> 00:00:48,000
It's not like non-self comes out and hits you in the face.

5
00:00:48,000 --> 00:00:59,000
But what it is is it's the giving up of the belief that something is permanent.

6
00:00:59,000 --> 00:01:08,000
Let me come in your meditation that you intellectualize, or not exactly intellectualize, but the thought arises.

7
00:01:08,000 --> 00:01:11,000
Wow, this is all impermanent.

8
00:01:11,000 --> 00:01:16,000
This realized this thought arises.

9
00:01:16,000 --> 00:01:20,000
I don't like a realization, but that's not the insight.

10
00:01:20,000 --> 00:01:22,000
That's not the moment of insight.

11
00:01:22,000 --> 00:01:26,000
It often follows.

12
00:01:26,000 --> 00:01:34,000
It's preceded by a moment of insight.

13
00:01:34,000 --> 00:01:45,000
The moment of insight is the seeing for yourself, for instance, in the case of impermanence, it's seeing something cease.

14
00:01:45,000 --> 00:01:54,000
It's the continued realization or the continued observation of the cessation of phenomena.

15
00:01:54,000 --> 00:02:04,000
For suffering, it's seeing the intolerability of the phenomenon.

16
00:02:04,000 --> 00:02:09,000
Basically, in terms of seeing that the things you thought that were going to make you happy are not going to make you happy.

17
00:02:09,000 --> 00:02:12,000
You see them arising and ceasing arising and ceasing.

18
00:02:12,000 --> 00:02:16,000
You lose the idea that they are so quiet.

19
00:02:16,000 --> 00:02:29,000
In the sense, it's giving up rather than it's giving up of the opposite characteristics.

20
00:02:29,000 --> 00:02:39,000
When you see things as they are, you give up the belief in permanence in satisfaction or that things can be pleasant.

21
00:02:39,000 --> 00:02:44,000
When you cling to something that it can bring you happiness.

22
00:02:44,000 --> 00:02:47,000
Number three, giving up the idea that you can control.

23
00:02:47,000 --> 00:02:51,000
We try to control our breath with a stomach, for example.

24
00:02:51,000 --> 00:02:55,000
In the beginning, you're just going to keep controlling the stomach no matter what you do.

25
00:02:55,000 --> 00:03:03,000
You'll be practicing rising, falling, and here you are forcing the breath at every in-breath, every out-breath.

26
00:03:03,000 --> 00:03:12,000
You start to see that it's not really forcing the breath at all.

27
00:03:12,000 --> 00:03:20,000
There's this conception of the forcing, but eventually what you'll see is that all you're doing is creating stress.

28
00:03:20,000 --> 00:03:24,000
You're not actually controlling the breath. You're just creating more stress.

29
00:03:24,000 --> 00:03:27,000
The breath is arising based.

30
00:03:27,000 --> 00:03:31,000
The stomach is rising based on many conditions.

31
00:03:31,000 --> 00:03:34,000
Only one of which is the mind.

32
00:03:34,000 --> 00:03:41,000
But the mind is responsible for creating stress, which is why when you try to control it, you just feel more and more terrible.

33
00:03:41,000 --> 00:03:43,000
More and more unpleasant.

34
00:03:43,000 --> 00:03:46,000
Until finally, you realize that it's not really...

35
00:03:46,000 --> 00:03:50,000
You can't just say, because, I mean, think about it.

36
00:03:50,000 --> 00:03:51,000
This is what we don't realize.

37
00:03:51,000 --> 00:03:56,000
If you could make it...

38
00:03:56,000 --> 00:03:59,000
If it was under your control, then why can't you just say,

39
00:03:59,000 --> 00:04:02,000
okay, my breath is just going to rise smoothly.

40
00:04:02,000 --> 00:04:04,000
It's going to fall smoothly.

41
00:04:04,000 --> 00:04:07,000
That's what we think we can do.

42
00:04:07,000 --> 00:04:14,000
But it's totally off the wall in this idea.

43
00:04:14,000 --> 00:04:21,000
This idea that we can do that, because it's totally unrelated to the facts.

44
00:04:21,000 --> 00:04:24,000
Eventually, you realize this.

45
00:04:24,000 --> 00:04:26,000
It's not intellectualizing.

46
00:04:26,000 --> 00:04:27,000
I just did.

47
00:04:27,000 --> 00:04:31,000
I just had you intellectualize and say, as the Buddha did, well, if you could,

48
00:04:31,000 --> 00:04:34,000
then why can't you control it to be like this or like that?

49
00:04:34,000 --> 00:04:36,000
It's not like that at all.

50
00:04:36,000 --> 00:04:41,000
The realization is visceral.

51
00:04:41,000 --> 00:04:46,000
It's actually seeing me breath a rise and see sun its own.

52
00:04:46,000 --> 00:04:50,000
And the clear perception of non-self is when you actually see like that.

53
00:04:50,000 --> 00:04:53,000
It actually appears to you that the body is moving on its own.

54
00:04:53,000 --> 00:04:55,000
It appears to you that thoughts are arising on the road.

55
00:04:55,000 --> 00:04:57,000
You're able to see this.

56
00:04:57,000 --> 00:05:00,000
And you're able to realize that really that's all there is,

57
00:05:00,000 --> 00:05:02,000
is phenomena arising and ceasing.

58
00:05:02,000 --> 00:05:06,000
And you give up any desire to control.

59
00:05:06,000 --> 00:05:13,000
The whole idea of control just goes out the window.

60
00:05:13,000 --> 00:05:16,000
And that's when you do the realization of non-self.

61
00:05:16,000 --> 00:05:20,000
So the mind lets go and the mind lets go, then there's freedom from suffering.

62
00:05:20,000 --> 00:05:35,000
That's how it should be.

