1
00:00:00,000 --> 00:00:06,520
I really appreciate your video uploads and that you are informing us about Buddhism.

2
00:00:06,520 --> 00:00:10,960
I meditate regularly and I am an agnostic.

3
00:00:10,960 --> 00:00:17,360
To what extent supports implies Buddhism, the concept of God.

4
00:00:17,360 --> 00:00:24,680
I made a video several years ago about my understanding of God.

5
00:00:24,680 --> 00:00:32,040
God is in the same category as the self in that they can't logically exist.

6
00:00:32,040 --> 00:00:39,880
If you are talking about it intellectually, they can't possibly exist.

7
00:00:39,880 --> 00:00:42,840
Because reality is based on existence.

8
00:00:42,840 --> 00:00:47,120
That reality is only seeing, hearing, smelling, tasting, feeling, thinking that.

9
00:00:47,120 --> 00:00:48,360
That's reality.

10
00:00:48,360 --> 00:00:53,960
Even if God were to come and stand before you, it's impossible that you can be sure

11
00:00:53,960 --> 00:00:57,920
that that's God.

12
00:00:57,920 --> 00:01:00,120
You say because you're only seeing something.

13
00:01:00,120 --> 00:01:04,360
If you hear God, it's impossible that you should know that you're hearing God.

14
00:01:04,360 --> 00:01:06,000
Because it's just hearing.

15
00:01:06,000 --> 00:01:12,000
If God strikes you over that with a lightning bolt or something or lifts you up into

16
00:01:12,000 --> 00:01:18,120
the air and shows you the wonder of His creation, you still can't be sure that that's

17
00:01:18,120 --> 00:01:19,120
God.

18
00:01:19,120 --> 00:01:24,960
Because it's still only seeing, hearing, smelling, tasting, feeling and thinking.

19
00:01:24,960 --> 00:01:31,520
In this sense, you can never know that God exists in the way that people say that they

20
00:01:31,520 --> 00:01:33,240
know that God exists.

21
00:01:33,240 --> 00:01:36,760
Because what they know is an extrapolation of experience.

22
00:01:36,760 --> 00:01:38,360
They don't really know it.

23
00:01:38,360 --> 00:01:41,200
And this is very important because this is how the mind works.

24
00:01:41,200 --> 00:01:48,240
The mind will never react based on something that it doesn't know.

25
00:01:48,240 --> 00:01:55,520
It only reacts based on what it knows, what it means.

26
00:01:55,520 --> 00:02:01,880
The only way the mind can let go is to actually experience that something is unsatisfying,

27
00:02:01,880 --> 00:02:05,840
is cause for stress and suffering.

28
00:02:05,840 --> 00:02:12,320
The only way we want to become free from suffering is to see it as suffering.

29
00:02:12,320 --> 00:02:23,000
So the only way this person can come to an end of some sorrow of craving and clinging

30
00:02:23,000 --> 00:02:29,760
and striving is to see things as they are.

31
00:02:29,760 --> 00:02:33,600
And this has nothing to do with God because seeing God is just seeing.

32
00:02:33,600 --> 00:02:39,160
The soul is the same, self is the same, it has no place in experience.

33
00:02:39,160 --> 00:02:47,240
It's just a concept, an idea that comes up based on our experience that we have some

34
00:02:47,240 --> 00:02:52,800
volition that we can make choices, that at any given moment there is a decision that

35
00:02:52,800 --> 00:02:53,800
is made.

36
00:02:53,800 --> 00:03:02,040
So we therefore give rise to the idea that there is someone behind that volition, which

37
00:03:02,040 --> 00:03:07,080
is important to understand that we're not saying that there is no volition, that there

38
00:03:07,080 --> 00:03:09,240
is no decision.

39
00:03:09,240 --> 00:03:17,800
It's just that reality doesn't admit of entities like God or Self or the doer in any sense.

40
00:03:17,800 --> 00:03:23,520
It's just our understanding, the way we talk about reality, the way we conceive of reality

41
00:03:23,520 --> 00:03:29,680
is so off track that we can't understand when we say that there is the doing, but there

42
00:03:29,680 --> 00:03:30,680
is no doer.

43
00:03:30,680 --> 00:03:34,800
You know, if there is doing, there must be the doer, for example.

44
00:03:34,800 --> 00:03:40,040
Your question is about God, that's a little bit different, but same sort of answer that

45
00:03:40,040 --> 00:04:07,120
entities don't exist in reality, all that exists is experience.

