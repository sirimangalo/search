1
00:00:00,000 --> 00:00:07,080
Last night when I was meditating, I saw scary images in my mind and tingling sensation

2
00:00:07,080 --> 00:00:10,320
in my face because of being afraid.

3
00:00:10,320 --> 00:00:15,080
Maybe it's because of scary movies, but this is omitted to sorry meditating.

4
00:00:15,080 --> 00:00:24,000
So many years ago in the past, how do I overcome this being afraid and is it all in my head?

5
00:00:24,000 --> 00:00:29,200
I remember when I first meditated, I think some of you know my story, how I was watching

6
00:00:29,200 --> 00:00:32,640
the Godfather movies right before I went to practice meditation.

7
00:00:32,640 --> 00:00:36,680
I'd never seen the Godfather movie, so I watched one, two, and three all together and then

8
00:00:36,680 --> 00:00:41,760
I watched Scarface to top it up, I don't think it has much to do with it.

9
00:00:41,760 --> 00:00:49,720
But then when I was meditating, I had this experience of voice saying, you know, just totally

10
00:00:49,720 --> 00:00:54,040
out of the blue and out of nowhere, just saying, I told you not to come back here.

11
00:00:54,040 --> 00:01:01,880
That's almost exactly what it said, which is no meaning, and it was just this totally

12
00:01:01,880 --> 00:01:09,320
mean and vicious sort of voice.

13
00:01:09,320 --> 00:01:13,480
But that sort of thing didn't happen later and it certainly happened less, these kind

14
00:01:13,480 --> 00:01:18,200
of things certainly happened less and less and less as I practiced.

15
00:01:18,200 --> 00:01:24,760
One thing is to practice correctly, because on the other hand, there are meditators for

16
00:01:24,760 --> 00:01:30,040
whom these things happen more and more and more and there's no doubt about why that is,

17
00:01:30,040 --> 00:01:32,520
it's because they're encouraging them.

18
00:01:32,520 --> 00:01:35,480
They're giving them power.

19
00:01:35,480 --> 00:01:48,000
We only create, we're only tortured by those things that we give power.

20
00:01:48,000 --> 00:01:57,560
And we are only haunted by those things that we perpetuate, that we promote in ourselves.

21
00:01:57,560 --> 00:02:02,680
So I know people who have gone crazy meditating, I know one monk who tried to kill himself

22
00:02:02,680 --> 00:02:03,680
several times.

23
00:02:03,680 --> 00:02:10,440
Slid his wrists, set himself on fire, totally wrong practice, totally out to lunch.

24
00:02:10,440 --> 00:02:15,720
I think he's gotten a lot better now and he started taking some medication for it and

25
00:02:15,720 --> 00:02:21,400
so on.

26
00:02:21,400 --> 00:02:26,480
But if you're practicing correctly, these were slowly going away.

27
00:02:26,480 --> 00:02:32,320
The point is that, and it's another important answer to your question, the idea is it's

28
00:02:32,320 --> 00:02:34,840
all, is it all in my mind?

29
00:02:34,840 --> 00:02:39,320
Because when people hear voices or they see things, they wonder, am I really seeing or is

30
00:02:39,320 --> 00:02:40,320
it all in my mind?

31
00:02:40,320 --> 00:02:43,000
Am I really hearing voices or is it all in my mind?

32
00:02:43,000 --> 00:02:49,320
Because monk I'm talking about heard voices and to him and to most people, it's an important

33
00:02:49,320 --> 00:02:50,320
point.

34
00:02:50,320 --> 00:02:52,000
Is it real or is it in my mind?

35
00:02:52,000 --> 00:02:57,080
And I get used to get this a lot with the Thai people because Thai people have good concentration

36
00:02:57,080 --> 00:03:03,240
and their minds are fairly focused and so they do see a lot of bright lights and colors

37
00:03:03,240 --> 00:03:08,240
and pictures and they're asked, they always ask me, is it real?

38
00:03:08,240 --> 00:03:12,840
From a Buddhist point of view, it's a peculiar sort of question, the question of whether

39
00:03:12,840 --> 00:03:13,840
it's in your head.

40
00:03:13,840 --> 00:03:20,040
This isn't all of your question, it isn't all of the answer, but it's an important point.

41
00:03:20,040 --> 00:03:27,560
Is it all in my head, see, there's no me, so there can be no my, there's no head, so the

42
00:03:27,560 --> 00:03:34,760
question really breaks down, reality is that experience that you had.

43
00:03:34,760 --> 00:03:37,680
And why I say this, because how else can you understand reality?

44
00:03:37,680 --> 00:03:42,120
It's a big question, it's a philosophical question for philosophy.

45
00:03:42,120 --> 00:03:44,960
What is real, what is reality?

46
00:03:44,960 --> 00:03:51,160
I can't imagine any other way of understanding reality than that which is experienced or

47
00:03:51,160 --> 00:03:52,160
experience.

48
00:03:52,160 --> 00:03:57,720
And it seems to me, I'm sure to think that there could be any other definition of reality

49
00:03:57,720 --> 00:04:04,720
and of course there aren't many different definitions of reality, so the one I'm giving

50
00:04:04,720 --> 00:04:10,400
you, I hope and as I understand, is the Buddha's understanding of reality, that reality

51
00:04:10,400 --> 00:04:12,440
is experience.

52
00:04:12,440 --> 00:04:20,440
And so those faces that you saw were really a vision of spaces, you really, or there really

53
00:04:20,440 --> 00:04:26,240
was an experience unless you're lying to me, an experience of seeing faces, right?

54
00:04:26,240 --> 00:04:30,640
Am I correct the images or images?

55
00:04:30,640 --> 00:04:32,920
That was real.

56
00:04:32,920 --> 00:04:36,360
It goes along with these people who say they saw God, right?

57
00:04:36,360 --> 00:04:45,400
So practice meditation and say, I saw God and therefore religion X is the truth, I saw

58
00:04:45,400 --> 00:04:50,600
Jesus and therefore Christianity is the truth, Jesus came and talked to me.

59
00:04:50,600 --> 00:04:56,960
And this is where Buddhism is, I think should be seen as different or Buddhist meditation

60
00:04:56,960 --> 00:05:01,720
should be seen as different from a meditation of the sort that I described, where one sees

61
00:05:01,720 --> 00:05:05,000
God and becomes one with God or so.

62
00:05:05,000 --> 00:05:11,640
Because Buddhism would say, yes, there was an experience of seeing, but that's all you

63
00:05:11,640 --> 00:05:19,160
can honestly say about it without entering into speculation.

64
00:05:19,160 --> 00:05:24,520
The truth of your experience is that there was a vision, just like when I'm seeing now,

65
00:05:24,520 --> 00:05:32,080
when I look at Taurindu, there's an experience of vision, I have no idea what I'm saying.

66
00:05:32,080 --> 00:05:37,680
So as an example, when he hears my voice, there's an experience of hearing and he's looking

67
00:05:37,680 --> 00:05:40,680
at me like I'm crazy because he doesn't understand it.

68
00:05:40,680 --> 00:05:42,000
But that's the truth.

69
00:05:42,000 --> 00:05:49,520
His experience is just a sound, just like my experience of looking at him is just seeing.

70
00:05:49,520 --> 00:05:51,160
The difference is what goes on in the mind.

71
00:05:51,160 --> 00:05:56,800
He has no, no mental idea except what's this crazy guy saying, I can't make out what

72
00:05:56,800 --> 00:05:58,040
he's saying.

73
00:05:58,040 --> 00:06:00,760
And I look at him and I can make out that Taurindu.

74
00:06:00,760 --> 00:06:04,600
But this is all happening in the brain, in the mind.

75
00:06:04,600 --> 00:06:10,480
The reality of experience is still just a seeing, still just a hearing, no matter whether

76
00:06:10,480 --> 00:06:12,640
you or how you understand it.

77
00:06:12,640 --> 00:06:17,160
So why this is an important thing to explain is because that's what should define your

78
00:06:17,160 --> 00:06:18,160
meditation.

79
00:06:18,160 --> 00:06:21,080
When you see something, that should be just seeing.

80
00:06:21,080 --> 00:06:25,440
When you have a tingling sensation in your face, yes, you had a tingling sensation in your

81
00:06:25,440 --> 00:06:26,440
face.

82
00:06:26,440 --> 00:06:31,040
It also goes back to the question about how, and it's an important point that I don't

83
00:06:31,040 --> 00:06:37,360
think either was mentioned is that how we make these connections.

84
00:06:37,360 --> 00:06:44,840
So you have the pain in the body and you have the memory and you can come up with a theory

85
00:06:44,840 --> 00:06:49,160
which is totally sound to say that they are connected.

86
00:06:49,160 --> 00:06:51,240
But that theory does you know good whatsoever.

87
00:06:51,240 --> 00:06:54,520
It in fact does you harm because you've created something.

88
00:06:54,520 --> 00:07:00,280
You've created a link, you have two experiences and you've created a third one that doesn't

89
00:07:00,280 --> 00:07:01,280
exist.

90
00:07:01,280 --> 00:07:04,920
You've created a third entity which is the connection between these two and that's where

91
00:07:04,920 --> 00:07:06,320
the problem arises, right?

92
00:07:06,320 --> 00:07:10,320
That's where problem first comes into being.

93
00:07:10,320 --> 00:07:14,720
When you have just a pain and just a memory, you have no problem.

94
00:07:14,720 --> 00:07:18,720
When there's a memory, you're thinking when you have the pain, you have pain.

95
00:07:18,720 --> 00:07:21,440
When you have the tingling in your face, you have the tingling in your face, when you have

96
00:07:21,440 --> 00:07:26,800
the pictures or the images, you have images, they're seeing.

97
00:07:26,800 --> 00:07:30,960
You can read through the Buddhist Sutras and this is what you'll find and this is an incredibly

98
00:07:30,960 --> 00:07:31,960
important point.

99
00:07:31,960 --> 00:07:38,200
I think that one that is core to understanding a Buddhist thought and meditation and

100
00:07:38,200 --> 00:07:41,600
it's the basis on which we should develop our meditation.

101
00:07:41,600 --> 00:07:47,560
If you develop meditation in this way, there's no chance, assuming that you keep it in

102
00:07:47,560 --> 00:07:54,560
mind and take it to heart and do base your meditation on this idea, there's no chance that

103
00:07:54,560 --> 00:08:00,040
such things will get worse and lead to any problem that you should be afraid of.

104
00:08:00,040 --> 00:08:05,920
What you're doing when you give rise to the fear is feeding it, creating attention and

105
00:08:05,920 --> 00:08:11,840
stress and the energy that arises based on the fear is giving power to that.

106
00:08:11,840 --> 00:08:18,640
This is why I would say that creating desire, thinking that the energy is somehow going to

107
00:08:18,640 --> 00:08:24,760
help you, is really a bad idea because that energy is actually strengthening the habit

108
00:08:24,760 --> 00:08:25,760
of the desire.

109
00:08:25,760 --> 00:08:28,240
It's strengthening the habit of the fear.

110
00:08:28,240 --> 00:08:31,960
When you give rise to fear, you're only going to become more afraid next time because

111
00:08:31,960 --> 00:08:39,440
the energy is messing with the brain and it's messing with the mind.

112
00:08:39,440 --> 00:08:50,080
It has a power to it and you need the energy to hardwire the information into the brain.

113
00:08:50,080 --> 00:08:52,680
I think that's all I have to say about that.

114
00:08:52,680 --> 00:09:02,600
I want to add here that if you want to overcome that being afraid, then you have to accept

115
00:09:02,600 --> 00:09:13,920
it as what it is, as just being afraid, you have to face it.

116
00:09:13,920 --> 00:09:19,840
One thing that happens when we're afraid is that we make a big deal out of it.

117
00:09:19,840 --> 00:09:24,720
We're making more out of it than it actually really is.

118
00:09:24,720 --> 00:09:36,280
Being afraid is just a feeling of being afraid and this is nothing really special.

119
00:09:36,280 --> 00:09:49,680
It's not a real life-threatening situation or so, it's going on in the mind.

120
00:09:49,680 --> 00:10:02,760
When we see it as it is, I lost a little bit of what I wanted to say, but it's then

121
00:10:02,760 --> 00:10:10,680
much easier to let go of it when we don't make such a big deal out of it.

122
00:10:10,680 --> 00:10:17,840
When we don't take it to personal, when we don't think, oh, I'm afraid, I have to do

123
00:10:17,840 --> 00:10:23,160
something that's wrong instead of just accepting it as it is.

