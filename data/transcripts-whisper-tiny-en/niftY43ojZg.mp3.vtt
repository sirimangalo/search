WEBVTT

00:00.000 --> 00:04.340
Hi, welcome back to Buddhism 101.

00:04.340 --> 00:09.820
Today we'll be continuing to talk about morality with part two, which is called the

00:09.820 --> 00:12.680
Garden of the Census.

00:12.680 --> 00:20.600
In Buddhism, we recognize there to be six senses, and it's nothing magical or spiritual.

00:20.600 --> 00:27.400
It's just the five ordinary physical senses, seeing, hearing, smelling, tasting, feeling,

00:27.400 --> 00:31.440
and an additional six sense being just the mind, which is thinking.

00:31.440 --> 00:35.400
So seeing, hearing, smelling, tasting, feeling, thinking, these are considered to be the

00:35.400 --> 00:41.280
six doors, if you will, to experience or doors to perception.

00:41.280 --> 00:48.560
So all of our experiences occur at one or another of these doors or over these senses.

00:48.560 --> 00:55.360
The Garden of the Census simply means to prevent the mind from cultivating judgements,

00:55.360 --> 01:04.280
projections, extrapolations of reality, so subjectivity to try to keep the mind at some level

01:04.280 --> 01:09.960
of objectivity, where we experience the object as it is, because this is what will allow

01:09.960 --> 01:17.680
us to focus and to see things as they are, cultivating concentration in wisdom, which are

01:17.680 --> 01:20.200
what morality is meant to lead to.

01:20.200 --> 01:27.280
So the practice of guarding the Census is actually a much more intrinsic or essential

01:27.280 --> 01:30.360
form of morality from a Buddhist perspective.

01:30.360 --> 01:38.000
The consideration or the idea is that extrapolations, judgements, projections, these are

01:38.000 --> 01:47.200
all somehow immoral in the sense that in the specific sense that they lead more to suffering

01:47.200 --> 01:59.480
and to stress, they lead us to misunderstand or to follow our addictions and our desires

01:59.480 --> 02:04.480
and our versions and our hatreds and our fears and our worries, they tend to cultivate

02:04.480 --> 02:14.280
these negative states of mind as opposed to simply states of peace and clarity of mind.

02:14.280 --> 02:21.960
They tend to dull and to confuse and to darken the mind.

02:21.960 --> 02:32.560
So this is really the beginning or the entry level for meditation, practicing in a Buddhist

02:32.560 --> 02:39.920
sense, where we begin to pull the mind back from its judgements, from its partialities,

02:39.920 --> 02:48.920
categorizing of reality and arbitrary categories of acceptable, unacceptable, good and bad,

02:48.920 --> 02:54.120
right, wrong, mean, mind and so on, without having taken the time to really understand

02:54.120 --> 02:59.560
what is good, what is bad, what is right and what is wrong from an objective point of view.

02:59.560 --> 03:06.080
So from this point of view, morality is simply bringing the mind back to reality.

03:06.080 --> 03:09.240
When you see something to have it just be seeing, when you hear something to have it

03:09.240 --> 03:14.560
just be hearing and nothing else, when you experience something to have it just be experiencing

03:14.560 --> 03:20.880
that thing and this is accomplished by, well, it's accomplished in many ways, it's

03:20.880 --> 03:26.440
firstly accomplished by simply forcing the mind, guarding the mind in the sense of when

03:26.440 --> 03:33.400
the mind goes out, bringing it back, saying no, no, no, just forbidding the mind to

03:33.400 --> 03:39.200
go out, it can be done by clamping the mind down or repressing the desires and so on.

03:39.200 --> 03:45.160
This is a conventional sort of guarding the senses.

03:45.160 --> 03:52.040
One way is physically to prevent yourself from giving rise to likes or dislikes.

03:52.040 --> 03:58.160
So when you're walking around in the city or driving around, driving or whatever, riding

03:58.160 --> 04:04.960
in the car, to not be looking around sightseeing or trying to take in what's going on around

04:04.960 --> 04:11.080
you, you have to not allow yourself to see the things that would create desire or aversion

04:11.080 --> 04:13.800
or worry or fear or so on.

04:13.800 --> 04:17.800
It doesn't really close your eyes or lower your gaze or so on.

04:17.800 --> 04:25.640
To avoid specific situations, to not go to bars or go to not watch entertainment, entertaining

04:25.640 --> 04:31.600
shows or listen to music or so on, knowing that it somehow excites you and creates habits

04:31.600 --> 04:35.240
of addiction and attachment.

04:35.240 --> 04:41.360
But obviously this is only going to be a temporary fix and I think it's worth noting

04:41.360 --> 04:49.760
that this will never give rise to concentration or wisdom by itself.

04:49.760 --> 04:55.520
So from the Buddhist perspective, morality has to be something deeper on an experiential

04:55.520 --> 05:02.280
level where even when you see something that would potentially attract you, you are able

05:02.280 --> 05:03.760
to be objective about it.

05:03.760 --> 05:08.160
You're able to focus your mind just on the essence of the object.

05:08.160 --> 05:10.600
So again, seeing is just seeing.

05:10.600 --> 05:12.960
And this is accomplished by something that we call mindfulness.

05:12.960 --> 05:17.080
This is something that I'll get into in a later video.

05:17.080 --> 05:24.760
But for now, basically the practice of morality requires this ability to capture the

05:24.760 --> 05:26.320
object in its essence.

05:26.320 --> 05:30.920
So when you see something, you remind yourself, this is seeing, just saying to yourself

05:30.920 --> 05:31.920
seeing.

05:31.920 --> 05:35.800
When you hear something, you remind yourself hearing, when you smell smelling, when you

05:35.800 --> 05:42.040
taste tasting, when you feel feeling, when you think thinking, guarding the senses from

05:42.040 --> 05:50.000
any sort of extrapolation or projection, so the mind grasps the seeing as just an experience

05:50.000 --> 05:54.000
and nothing more grasps the experience just as it is.

05:54.000 --> 06:02.400
This is considered to be morality because it keeps the mind from any judgment that would

06:02.400 --> 06:13.200
cause one to perform any moral deeds, any speech that is performed as a result of our experiences

06:13.200 --> 06:19.520
as therefore pure, any actions that are performed as a result of our experience of the

06:19.520 --> 06:22.720
world that are therefore pure and objective.

06:22.720 --> 06:27.760
So instead of when we see something getting angry about it and speaking or acting in

06:27.760 --> 06:36.760
a way to come suffering, we're here or smell or so on, we're able to act rationally and

06:36.760 --> 06:39.040
with clarity of mind.

06:39.040 --> 06:45.720
So not much to say about it, it's quite a simple concept, but it's really the very essence

06:45.720 --> 06:50.720
of Buddhist meditation practice, this beginning of the path of cultivating morality where

06:50.720 --> 06:56.280
we begin to pull the mind back and eventually the mind will become accustomed to seeing

06:56.280 --> 06:57.280
things as they are.

06:57.280 --> 07:01.200
This will give rise to concentration and therefore wisdom to things that we'll talk about

07:01.200 --> 07:02.760
in future videos.

07:02.760 --> 07:09.400
But for now, another aspect of morality, really probably the essence of what we mean by

07:09.400 --> 07:11.960
morality in a Buddhist context.

07:11.960 --> 07:18.800
So very important and important for us to understand at the very outset so that we can get

07:18.800 --> 07:23.200
an idea of what is meant by Buddhist morality and Buddhist meditation practice.

07:23.200 --> 07:50.280
So thank you for tuning in, this is Buddhism 101.

