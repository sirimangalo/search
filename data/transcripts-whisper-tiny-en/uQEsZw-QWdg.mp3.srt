1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Damapanda.

2
00:00:05,000 --> 00:00:15,000
Today we continue on with verses 119 and 120, which read as follows.

3
00:00:15,000 --> 00:00:22,000
Today we continue on with the Damapanda.

4
00:00:22,000 --> 00:00:29,000
Today we continue on with the Damapanda.

5
00:00:29,000 --> 00:00:52,000
This is a little bit of a tongue twister because of the alliteration, which is purposeful.

6
00:00:52,000 --> 00:01:00,000
It's a play on the poly, so if you read it, you can see the words that are very similar.

7
00:01:00,000 --> 00:01:11,000
But it means even an evil-doer will see benefit or good things.

8
00:01:11,000 --> 00:01:31,000
For as long as the evil doesn't ripen, you're not able to tip up at the papal, papa, and opacity.

9
00:01:31,000 --> 00:01:44,000
When the papa, the evil ripens, then the evil-doer will see evil.

10
00:01:44,000 --> 00:01:47,000
That's verse 19 verse 20 is the opposite.

11
00:01:47,000 --> 00:01:54,000
Even a good-doer sees evil for as long as, may see evil.

12
00:01:54,000 --> 00:02:04,000
For as long as the good doesn't bear fruit, or doesn't come to fruition, doesn't ripen.

13
00:02:04,000 --> 00:02:14,000
But when the goodness ripens, then the good-doer sees goodness.

14
00:02:14,000 --> 00:02:28,000
This verse was taught in regards to the story of Anatopindika, the Buddha, one of the Buddha's chief lay disciples, clay supporters.

15
00:02:28,000 --> 00:02:37,000
So here was a man who, the word Anatopindika, Anatah means a refuge.

16
00:02:37,000 --> 00:02:49,000
Anatah means one who has no refuge. It's a word that was used to refer to poor people in the helpless.

17
00:02:49,000 --> 00:02:56,000
And pindika, or pindam means a ball of rice, or it means rice.

18
00:02:56,000 --> 00:03:11,000
And pindam and ika, or nika means one who has. So this means one who has arms, or a ball of rice, for the helpless.

19
00:03:11,000 --> 00:03:20,000
So he's the giver of arms. That's his name. That's the name he was given.

20
00:03:20,000 --> 00:03:27,000
He never would could reason, because not only did he support the Buddhist monks, but he was kind and generous.

21
00:03:27,000 --> 00:03:35,000
And he would give arms to the poor people, I guess, as well.

22
00:03:35,000 --> 00:03:49,000
And he was also quite generous. So it seems that not only did he give great gifts to, by actually buying this forest of Jaitas, a prince Jaita,

23
00:03:49,000 --> 00:04:00,000
and giving it to the Buddha as a monastery. But he also was generous with his friends and associates, it seemed.

24
00:04:00,000 --> 00:04:09,000
So over time, even though he was quite rich, over time he lent out a lot of his money.

25
00:04:09,000 --> 00:04:16,000
Huge amounts of it, really. And people didn't pay him back.

26
00:04:16,000 --> 00:04:24,000
And at the same time he had, in those days, they didn't have banks, so they would hide there.

27
00:04:24,000 --> 00:04:32,000
They would bury their valuable, and they had buried it in the ground near a river.

28
00:04:32,000 --> 00:04:45,000
And the river had overflown, and the bank had eroded, and all of a great deal of his family's wealth was swept out to sea.

29
00:04:45,000 --> 00:04:53,000
And so Anatha Pindik, whenever he went to the monastery to see the Buddha, he would never go empty-handed.

30
00:04:53,000 --> 00:04:56,000
He would always, in the morning, he would bring food.

31
00:04:56,000 --> 00:05:05,000
In the afternoon he would bring juice, and he would always bring with him gifts.

32
00:05:05,000 --> 00:05:25,000
They had religious gifts of incense and candles, and maybe not an incense, and flowers, garlands of flowers, that kind of thing.

33
00:05:25,000 --> 00:05:38,000
But even though he was starting to become actually quite impoverished, so he had lost really the bulk of his wealth, he never stopped giving.

34
00:05:38,000 --> 00:05:50,000
Even if it was just a little, he never gave it up, and so the Buddha asked him one day, are you still giving alms at your house?

35
00:05:50,000 --> 00:06:12,000
And he said, yes, Thunderbolt, sir, I'm still giving alms, but it's not as refined as it once was, as high quality as it once was.

36
00:06:12,000 --> 00:06:21,000
And the Buddha gives a quote, which is actually quite...

37
00:06:21,000 --> 00:06:25,000
I think it's not tea, tea, te, percentam, here, here we are.

38
00:06:25,000 --> 00:06:31,000
Cheetas, mihi, pani, te, that's a different quote, sorry.

39
00:06:31,000 --> 00:06:39,000
Cheetas, mihi, pani, te, Buddha, diy nang, diy nang, diy nang, nang, nang, nang, nam, nakte.

40
00:06:39,000 --> 00:06:43,000
It's a paraphrase of a different quote, which I actually think.

41
00:06:43,000 --> 00:06:48,000
I can't remember whether it was the Buddha who actually said it, but it's a very common quote.

42
00:06:48,000 --> 00:06:52,000
Nati, chit, te, percentam, mihi, apagana, madakina.

43
00:06:52,000 --> 00:07:03,000
There's no such thing as a gift, as a small gift, when it's given in faith and given just to one who deserves it.

44
00:07:03,000 --> 00:07:09,000
He says, almost the same, we look on nam and nati.

45
00:07:09,000 --> 00:07:19,000
There's no such thing as a course gift, when the gift is given out of with a pure heart, with a bright heart,

46
00:07:19,000 --> 00:07:27,000
and given to a suitable recipient.

47
00:07:27,000 --> 00:07:33,000
He went on and reassured an atapindha and said, you know what?

48
00:07:33,000 --> 00:07:38,000
He told a story of his past life, I think, about how he once tried to...

49
00:07:38,000 --> 00:07:45,000
It was very kind to people and still wasn't able to move them, but he never wavered in it.

50
00:07:45,000 --> 00:07:55,000
Anyway, he reassured or gave reassurance or encouragement to an atapindha, appreciated the fact that

51
00:07:55,000 --> 00:08:05,000
he gave, it's actually a thing in Buddhism that giving me a part of our life.

52
00:08:05,000 --> 00:08:08,000
I've been talking quite a bit about giving.

53
00:08:08,000 --> 00:08:14,000
You can see how many of these are giving, but it does bear mentioning that even though our focus is meditation,

54
00:08:14,000 --> 00:08:28,000
the practice that giving is, the Buddha said, if being new, the true benefit of giving, no one would eat a meal

55
00:08:28,000 --> 00:08:40,000
without giving a portion of it or some gift or some part of it to another person who was worthy of it.

56
00:08:40,000 --> 00:08:46,000
If there was someone there, worthy of it, then they wouldn't eat without sharing.

57
00:08:46,000 --> 00:08:50,000
No one would eat a single meal if they really knew.

58
00:08:50,000 --> 00:08:52,000
They said the way I know.

59
00:08:52,000 --> 00:08:59,000
He said it, but it's because people don't know that they don't share and they don't think to give.

60
00:08:59,000 --> 00:09:09,000
There's much made in the suit, there's about the difference between the benefit that you get in eating food or partaking of your possessions.

61
00:09:09,000 --> 00:09:15,000
And the benefit that comes from giving and sharing your possessions with others.

62
00:09:15,000 --> 00:09:23,000
It's quite a different benefit and on a different scale.

63
00:09:23,000 --> 00:09:26,000
So this was really part of the course.

64
00:09:26,000 --> 00:09:34,000
Not depending on doing something that, though hard to do, is a very typical Buddhist thing,

65
00:09:34,000 --> 00:09:42,000
you give according to your means, but giving is a part of one's spiritual life, especially as a lay person.

66
00:09:42,000 --> 00:09:50,000
But even as monks, for monks, as I've talked about how we share and give and actually have an easier time with it,

67
00:09:50,000 --> 00:09:54,000
because we don't need as much.

68
00:09:54,000 --> 00:10:01,000
We don't have as many uncertainties in our lives in the sense that,

69
00:10:01,000 --> 00:10:07,000
we don't have to worry about our car breaking down or someone breaking into our house.

70
00:10:07,000 --> 00:10:13,000
We don't have to worry about what clothes we're going to wear to work and so on.

71
00:10:13,000 --> 00:10:17,000
We don't have as many requirements placed on us.

72
00:10:17,000 --> 00:10:21,000
So giving for monks is actually in many ways easier.

73
00:10:21,000 --> 00:10:24,000
In any way, very much a part of our spiritual practice.

74
00:10:24,000 --> 00:10:29,000
So this is the setting, but the story.

75
00:10:29,000 --> 00:10:37,000
There was a goddess, or it may not goddess, who would say more like a female angel,

76
00:10:37,000 --> 00:10:41,000
living in Anatapintika's house.

77
00:10:41,000 --> 00:10:46,000
I guess in his mansion, he had a, I guess, a fairly large residence.

78
00:10:46,000 --> 00:10:57,000
And I guess at each of the four doors before gates to his residence, there was an angel.

79
00:10:57,000 --> 00:11:01,000
Anyway, one of the gates may be the gate.

80
00:11:01,000 --> 00:11:09,000
She was sort of like the spiritual guard over the gate.

81
00:11:09,000 --> 00:11:19,000
Had for a long time been concerned about Anatapintika's generosity,

82
00:11:19,000 --> 00:11:32,000
because as many earth angels would be, she was very enamored of the jewels and the valuables in his house.

83
00:11:32,000 --> 00:11:41,000
And so for a long time, she'd wanted to kind of push him towards conserving his wealth.

84
00:11:41,000 --> 00:11:45,000
And you're not giving it away because every time valuables left the house, that would be it,

85
00:11:45,000 --> 00:11:50,000
she'd never see them again, and it felt, all she ever saw was this diminishing

86
00:11:50,000 --> 00:11:56,000
pile of wealth, and in late times, later times, it diminished quite a bit.

87
00:11:56,000 --> 00:12:01,000
And so now it was mostly gone, and at this point, she couldn't bear it anymore.

88
00:12:01,000 --> 00:12:08,000
And so she decided to herself that she had, she would have to do something about this.

89
00:12:08,000 --> 00:12:13,000
And so in the middle of the night, she went to him.

90
00:12:13,000 --> 00:12:22,000
She had never felt confident, or she'd never dared to go to him before that,

91
00:12:22,000 --> 00:12:24,000
because he was rich and powerful and so on.

92
00:12:24,000 --> 00:12:26,000
But now he was the poor man.

93
00:12:26,000 --> 00:12:29,000
And so she felt comfortable going to him.

94
00:12:29,000 --> 00:12:34,000
She went in the middle of the night and stood by his bed,

95
00:12:34,000 --> 00:12:40,000
or stood in the air above his bed, and maybe glowing or so on.

96
00:12:40,000 --> 00:12:43,000
And Natapindika opened his eyes and said,

97
00:12:43,000 --> 00:12:44,000
who is that?

98
00:12:44,000 --> 00:12:51,000
And she said, it is a great, great safety,

99
00:12:51,000 --> 00:13:00,000
which means not treasure, but just a rich person.

100
00:13:00,000 --> 00:13:04,000
The goddess, I am the goddess that resides over your fourth gate.

101
00:13:04,000 --> 00:13:13,000
I am here. I am here. I am here. Come to give you an admonition. Come to give you a teaching.

102
00:13:13,000 --> 00:13:16,000
I had to add one issue.

103
00:13:16,000 --> 00:13:22,000
And he said, well then, tell me, what do you have to say?

104
00:13:22,000 --> 00:13:24,000
And here is an admonition.

105
00:13:24,000 --> 00:13:27,000
Great treasure, a great wealthy man.

106
00:13:27,000 --> 00:13:31,000
Without considering the future, you have dissipated your great wealth

107
00:13:31,000 --> 00:13:33,000
in the religion of the monk Godhama.

108
00:13:33,000 --> 00:13:36,000
Now, although you have reduced yourself to poverty,

109
00:13:36,000 --> 00:13:39,000
you still continue to give of your wealth.

110
00:13:39,000 --> 00:13:42,000
If you continue this course, in a few days,

111
00:13:42,000 --> 00:13:47,000
you will not have enough left to provide you with clothing and food.

112
00:13:47,000 --> 00:13:52,000
Of what use to you is the monk Godhama.

113
00:13:52,000 --> 00:13:55,000
Abandon your lavish giving, devote your attention to business

114
00:13:55,000 --> 00:13:58,000
and make a fortune.

115
00:13:58,000 --> 00:14:00,000
Sounds like solid advice, right?

116
00:14:00,000 --> 00:14:04,000
For those of us living in the world.

117
00:14:04,000 --> 00:14:09,000
Why would you waste your money on this bald-headed, shaved,

118
00:14:09,000 --> 00:14:11,000
this shaved, reckless?

119
00:14:11,000 --> 00:14:17,000
In fact, we often criticize people who give religious causes.

120
00:14:17,000 --> 00:14:21,000
And we criticize religious institutions as being money hungry

121
00:14:21,000 --> 00:14:24,000
because they usually just want money

122
00:14:24,000 --> 00:14:27,000
and collecting money all the time.

123
00:14:27,000 --> 00:14:30,000
I think Buddhism is very guilty of that,

124
00:14:30,000 --> 00:14:33,000
and we are even our organization,

125
00:14:33,000 --> 00:14:35,000
even does that sort of thing.

126
00:14:35,000 --> 00:14:37,000
There's a lot of talk about money,

127
00:14:37,000 --> 00:14:39,000
which is, I guess, in a way unfortunate,

128
00:14:39,000 --> 00:14:42,000
but in another way, it's important to recognize

129
00:14:42,000 --> 00:14:45,000
that it's a part of a part of spirituality

130
00:14:45,000 --> 00:14:49,000
to give up your resources to share and to let go,

131
00:14:49,000 --> 00:14:55,000
to not claim certainly not to worry about such things.

132
00:14:55,000 --> 00:15:03,000
But to use, and to put to use your resources

133
00:15:03,000 --> 00:15:09,000
for the best of causes and best benefit.

134
00:15:09,000 --> 00:15:11,000
And so not the vindictus says,

135
00:15:11,000 --> 00:15:14,000
is that the advice you want you came to give me?

136
00:15:14,000 --> 00:15:16,000
Yes, sir.

137
00:15:16,000 --> 00:15:19,000
He says, then be gone.

138
00:15:19,000 --> 00:15:22,000
Though a hundred thousand like you should try,

139
00:15:22,000 --> 00:15:25,000
you would not be able to move me from my course.

140
00:15:25,000 --> 00:15:29,000
You have said to me what you had no right to say.

141
00:15:29,000 --> 00:15:31,000
What business have you to dwell in my house,

142
00:15:31,000 --> 00:15:34,000
leave my house instantly?

143
00:15:34,000 --> 00:15:36,000
He banished her from this house,

144
00:15:36,000 --> 00:15:39,000
and because he was such a powerful person,

145
00:15:39,000 --> 00:15:42,000
and also a Sotapana,

146
00:15:42,000 --> 00:15:44,000
she was unable to...

147
00:15:44,000 --> 00:15:47,000
Angels are somehow sensitive in this way.

148
00:15:47,000 --> 00:15:49,000
She was unable to stay there.

149
00:15:49,000 --> 00:15:51,000
She just couldn't stay in the house anymore.

150
00:15:51,000 --> 00:15:56,000
I mean, however, the other angels probably wouldn't have let her stay anyway.

151
00:15:56,000 --> 00:15:58,000
So she left.

152
00:15:58,000 --> 00:16:02,000
But after leaving, she couldn't find lodging anywhere else.

153
00:16:02,000 --> 00:16:05,000
I guess even angels have difficulty.

154
00:16:05,000 --> 00:16:10,000
Probably she was at that point ostracized by the other angels,

155
00:16:10,000 --> 00:16:15,000
by the earth angels.

156
00:16:15,000 --> 00:16:18,000
And so she wandered trying to find a way,

157
00:16:18,000 --> 00:16:24,000
and she went finally to the city angel.

158
00:16:24,000 --> 00:16:33,000
There's an angel who looks after the city of Savati.

159
00:16:33,000 --> 00:16:35,000
And she explained her problem and said,

160
00:16:35,000 --> 00:16:40,000
please help me to take me to the...

161
00:16:40,000 --> 00:16:42,000
If you can come with me to the treasure

162
00:16:42,000 --> 00:16:46,000
and speak on my behalf and get him to forgive me

163
00:16:46,000 --> 00:16:49,000
for saying something,

164
00:16:49,000 --> 00:16:54,000
but the angels of the city shook his head and said,

165
00:16:54,000 --> 00:16:58,000
you said something that you had no business to say.

166
00:16:58,000 --> 00:17:00,000
Like, it's not possible for me.

167
00:17:00,000 --> 00:17:04,000
I can't help you, I'm sorry.

168
00:17:04,000 --> 00:17:06,000
So at her wit then,

169
00:17:06,000 --> 00:17:09,000
she decided to go the next step up,

170
00:17:09,000 --> 00:17:12,000
which is the four great kings that you hear about

171
00:17:12,000 --> 00:17:14,000
in Chinese mythology.

172
00:17:14,000 --> 00:17:17,000
They come from Buddhism.

173
00:17:17,000 --> 00:17:23,000
Buddhism talks about four gods that apparently look after the four directions.

174
00:17:23,000 --> 00:17:29,000
I don't know, or four angels that look after the...

175
00:17:29,000 --> 00:17:32,000
I don't know, four quarters of the earth maybe.

176
00:17:32,000 --> 00:17:38,000
Maybe the four different continents or something.

177
00:17:38,000 --> 00:17:42,000
And again, they said the same thing that they couldn't help her.

178
00:17:42,000 --> 00:17:47,000
And so finally, she went up to the Daoa Dingsa heaven,

179
00:17:47,000 --> 00:17:50,000
where she really wasn't...

180
00:17:50,000 --> 00:17:52,000
She was very low in the angel hierarchy,

181
00:17:52,000 --> 00:17:54,000
so it was like going to see the king,

182
00:17:54,000 --> 00:17:56,000
like a peasant going to see the king.

183
00:17:56,000 --> 00:17:59,000
But she managed to get an audience with Sakai,

184
00:17:59,000 --> 00:18:04,000
the king of the angels of the Daoa Dingsa heavens.

185
00:18:04,000 --> 00:18:06,000
And she told the story and said,

186
00:18:06,000 --> 00:18:08,000
I can't find... I have nowhere to live

187
00:18:08,000 --> 00:18:12,000
than I've done something really reprehensible,

188
00:18:12,000 --> 00:18:16,000
and now I wonder about without perfect protection.

189
00:18:16,000 --> 00:18:20,000
It says ironically, her strangely, children in hand.

190
00:18:20,000 --> 00:18:22,000
My angels don't have children, so it's strange.

191
00:18:22,000 --> 00:18:27,000
It must mean the other angels that are in her retinue,

192
00:18:27,000 --> 00:18:30,000
because they don't give birth as far as I know.

193
00:18:30,000 --> 00:18:34,000
Of course, who knows? I don't really know much about angels.

194
00:18:34,000 --> 00:18:44,000
And so she said, please help me to regain my privilege as a gatekeeper.

195
00:18:44,000 --> 00:18:49,000
And Sakai replied, I'm sorry, I can't help you.

196
00:18:49,000 --> 00:18:54,000
It's not possible for me to speak to the treasure on your behalf.

197
00:18:54,000 --> 00:18:57,000
However, I will tell you away.

198
00:18:57,000 --> 00:19:04,000
And she said, oh, please do. Tell me what it is.

199
00:19:04,000 --> 00:19:07,000
And here was Sakai. Sakai was quite a wise man,

200
00:19:07,000 --> 00:19:10,000
and also a Sultabana.

201
00:19:10,000 --> 00:19:13,000
He said, go assume the dress of the treasures

202
00:19:13,000 --> 00:19:16,000
steward or the rich men steward.

203
00:19:16,000 --> 00:19:18,000
And he's like a accountant.

204
00:19:18,000 --> 00:19:21,000
And take a piece of a leaf.

205
00:19:21,000 --> 00:19:24,000
You know, that's what they went right on.

206
00:19:24,000 --> 00:19:30,000
And list all the wealth he wants possessed that he lent out.

207
00:19:30,000 --> 00:19:33,000
And then go around to all the people who owe him money,

208
00:19:33,000 --> 00:19:43,000
and convince them, use your angelic influence to get them to return their...

209
00:19:43,000 --> 00:19:46,000
to make it on their debts.

210
00:19:46,000 --> 00:19:51,000
And then go and find the huge amount of wealth that has been swept out into

211
00:19:51,000 --> 00:19:55,000
the ocean that used to belong to Anatapindika.

212
00:19:55,000 --> 00:20:00,000
And on top of that, find an equal amount that's scattered throughout

213
00:20:00,000 --> 00:20:07,000
or that has no owner, you know, wealth that is lost and has no owner.

214
00:20:07,000 --> 00:20:09,000
And bring that to him.

215
00:20:09,000 --> 00:20:13,000
Take all that wealth, deposit it in his safe house,

216
00:20:13,000 --> 00:20:16,000
and then go to him and ask forgiveness.

217
00:20:16,000 --> 00:20:20,000
And so she did this.

218
00:20:20,000 --> 00:20:25,000
And she went to the... and then she came to Anatapindika and said,

219
00:20:25,000 --> 00:20:29,000
knocked on his door and he said, who is it?

220
00:20:29,000 --> 00:20:33,000
Or she stood in the air radiating light.

221
00:20:33,000 --> 00:20:34,000
Who is that?

222
00:20:34,000 --> 00:20:35,000
It is me.

223
00:20:35,000 --> 00:20:39,000
It is I, the blind stupid goddess that once dwelt over your gate.

224
00:20:39,000 --> 00:20:44,000
Pardon me the words, I once spoke to him in my blind stupidity.

225
00:20:44,000 --> 00:20:47,000
In obedience to the command of Saka, King of the Angels,

226
00:20:47,000 --> 00:20:50,000
I have recovered the wealth, your wealth,

227
00:20:50,000 --> 00:20:53,000
and filled your store room there with us.

228
00:20:53,000 --> 00:20:55,000
Have I atoned from my offense?

229
00:20:55,000 --> 00:20:58,000
I have no place wherein to lodge myself,

230
00:20:58,000 --> 00:21:04,000
and therefore I am greatly weirined.

231
00:21:04,000 --> 00:21:08,000
Anatapindika didn't... and then Anatapindika didn't accept her apology.

232
00:21:08,000 --> 00:21:10,000
He looked at her.

233
00:21:10,000 --> 00:21:13,000
He said, I know what I've got to do.

234
00:21:13,000 --> 00:21:19,000
And so he took her with him and brought her to see the Buddha.

235
00:21:19,000 --> 00:21:24,000
Because Anatapindika would have never been really likely upset with her.

236
00:21:24,000 --> 00:21:27,000
And wouldn't have held a grudge.

237
00:21:27,000 --> 00:21:35,000
But his only concern really was for goodness and for righteousness.

238
00:21:35,000 --> 00:21:40,000
And so here, his only thought was to provide some goodness for this angel,

239
00:21:40,000 --> 00:21:44,000
to give this angel, to make sure this angel got on the right track.

240
00:21:44,000 --> 00:21:46,000
It wasn't about apologizing to him.

241
00:21:46,000 --> 00:21:50,000
It was about having her understand goodness and evil,

242
00:21:50,000 --> 00:21:52,000
righteousness and righteousness.

243
00:21:52,000 --> 00:21:55,000
And so bringing her to see the Buddha,

244
00:21:55,000 --> 00:21:59,000
was all about teaching her right from wrong.

245
00:22:02,000 --> 00:22:06,000
And so they came to the Buddha.

246
00:22:06,000 --> 00:22:11,000
And Anatapindika related the story to the Buddha.

247
00:22:11,000 --> 00:22:13,000
And then the Buddha taught them both.

248
00:22:13,000 --> 00:22:18,000
And in order to admonish them, both gave these two stanzas or two verses.

249
00:22:18,000 --> 00:22:26,000
The first in regards to evil, doers who still profit until their evil comes to fruition.

250
00:22:26,000 --> 00:22:30,000
And then the same goes with goodness.

251
00:22:30,000 --> 00:22:39,000
So you could say that the first one was a bit easier to see.

252
00:22:39,000 --> 00:22:41,000
We're more liable to see that one, I think,

253
00:22:41,000 --> 00:22:45,000
and to understand that one that you can do evil deeds and still be protected from them.

254
00:22:45,000 --> 00:22:54,000
But there's no sense that you're not going to eventually be forced to pay the consequences.

255
00:22:54,000 --> 00:22:59,000
But with goodness, I think we have a little bit more trouble.

256
00:22:59,000 --> 00:23:02,000
I think for many people it's difficult to do good deeds.

257
00:23:02,000 --> 00:23:07,000
And when you undertake them, for people who aren't accustomed to doing good deeds,

258
00:23:07,000 --> 00:23:09,000
it's difficult to do them.

259
00:23:09,000 --> 00:23:15,000
And then when you've done them and don't get a result and feel like it's been for not,

260
00:23:15,000 --> 00:23:17,000
it can be quite discouraged.

261
00:23:17,000 --> 00:23:23,000
But this is another one of those verses that puts that to rest and reminds us

262
00:23:23,000 --> 00:23:29,000
that that really is the case, something that has a good result.

263
00:23:29,000 --> 00:23:32,000
Even if good deeds do have a good result.

264
00:23:32,000 --> 00:23:34,000
For many people, this isn't a certainty.

265
00:23:34,000 --> 00:23:39,000
They're not sure whether it's right to do good deeds or whether good deeds

266
00:23:39,000 --> 00:23:42,000
can have a bad result and so on.

267
00:23:42,000 --> 00:23:46,000
But even when they do, even given that they do,

268
00:23:46,000 --> 00:23:48,000
it's not always evident right away.

269
00:23:48,000 --> 00:23:51,000
It's not always clear and apparent.

270
00:23:51,000 --> 00:23:53,000
This goes for charity.

271
00:23:53,000 --> 00:23:55,000
This goes for morality.

272
00:23:55,000 --> 00:23:57,000
This goes for meditation.

273
00:23:57,000 --> 00:24:00,000
Sometimes meditation can appear to be fruitless.

274
00:24:00,000 --> 00:24:02,000
You practice, practice, practice.

275
00:24:02,000 --> 00:24:04,000
And you still get angry.

276
00:24:04,000 --> 00:24:05,000
You still get greedy.

277
00:24:05,000 --> 00:24:11,000
You still have all these problems inside.

278
00:24:11,000 --> 00:24:16,000
It's in fact not even as clear as just taking time.

279
00:24:16,000 --> 00:24:20,000
It's also the mix of good and evil.

280
00:24:20,000 --> 00:24:23,000
We've done many things that are on the wholesome.

281
00:24:23,000 --> 00:24:33,000
Then they're going to play a part and they're going to mitigate the benefit of the good deeds.

282
00:24:33,000 --> 00:24:35,000
And the same goes with evil.

283
00:24:35,000 --> 00:24:38,000
When a person does evil, if they've also done many good deeds,

284
00:24:38,000 --> 00:24:42,000
which most human beings have in order to be born as human beings,

285
00:24:42,000 --> 00:24:45,000
they're going to be protected from their evil.

286
00:24:45,000 --> 00:24:49,000
For can be for quite some time, maybe even for their whole life

287
00:24:49,000 --> 00:24:52,000
until they die and they're forced to feel the weight.

288
00:24:52,000 --> 00:24:58,000
And the full force of the evil deeds,

289
00:24:58,000 --> 00:25:01,000
in which case it leads to unpleasantness in the future lives.

290
00:25:05,000 --> 00:25:09,000
But a reminder for us, karma is not so simple

291
00:25:09,000 --> 00:25:14,000
as you do the deed and then an angel shows up and gives you a reward.

292
00:25:14,000 --> 00:25:19,000
But these are the sorts of things that are not the pinnedika

293
00:25:19,000 --> 00:25:21,000
would have been aware of as a sort upon that.

294
00:25:21,000 --> 00:25:23,000
Because having practiced in sight meditation,

295
00:25:23,000 --> 00:25:28,000
he was able to see cause and effect and understand where the true,

296
00:25:28,000 --> 00:25:30,000
what's really going on behind the scenes.

297
00:25:30,000 --> 00:25:35,000
Because we obviously can't know what the cause of our deeds is going to be.

298
00:25:35,000 --> 00:25:36,000
You will do good deeds.

299
00:25:36,000 --> 00:25:39,000
You can't know what the fruit is going to be.

300
00:25:39,000 --> 00:25:42,000
How can you know that evil is going to lead to evil?

301
00:25:42,000 --> 00:25:45,000
Because through the meditation,

302
00:25:45,000 --> 00:25:47,000
you can see what's going on behind the scenes.

303
00:25:47,000 --> 00:25:49,000
You see the underlying framework.

304
00:25:49,000 --> 00:25:55,000
And you can watch as your mind changes and as the universe changes.

305
00:25:55,000 --> 00:25:58,000
Based on your deeds, if you do good deeds,

306
00:25:58,000 --> 00:26:00,000
how it changes for the better.

307
00:26:00,000 --> 00:26:03,000
If you do evil deeds, how it changes you.

308
00:26:03,000 --> 00:26:06,000
Changes your attitude.

309
00:26:06,000 --> 00:26:09,000
And it has direct consequences on the world around you.

310
00:26:09,000 --> 00:26:12,000
You can see all of this moment to moment.

311
00:26:12,000 --> 00:26:17,000
And as you see this more and more and clearer and clearer,

312
00:26:17,000 --> 00:26:21,000
you have less and less doubt or concern about the results of your actions.

313
00:26:21,000 --> 00:26:22,000
What is it going to be?

314
00:26:22,000 --> 00:26:23,000
What's it going to bring?

315
00:26:23,000 --> 00:26:28,000
Because you know that you're planting seeds of happiness.

316
00:26:28,000 --> 00:26:30,000
Seeds of greatness and goodness.

317
00:26:30,000 --> 00:26:32,000
You don't have to worry about what is the result.

318
00:26:32,000 --> 00:26:35,000
Because you know it's going to be positive.

319
00:26:35,000 --> 00:26:40,000
That's the great, really the greatness of the Law of Karma.

320
00:26:40,000 --> 00:26:43,000
Is the certainty of it.

321
00:26:43,000 --> 00:26:46,000
You don't have to be certain what's going to happen.

322
00:26:46,000 --> 00:26:51,000
You just have to be certain the nature of that what's going to happen.

323
00:26:51,000 --> 00:26:54,000
And it's never sure what exactly is going to come of your deeds.

324
00:26:54,000 --> 00:27:02,000
But it is sure the nature of the effect of certain deeds.

325
00:27:02,000 --> 00:27:05,000
All some deeds have pleasant results.

326
00:27:05,000 --> 00:27:09,000
Peaceful, happy, and good results.

327
00:27:09,000 --> 00:27:19,000
And all some deeds have unpleasant, evil results.

328
00:27:19,000 --> 00:27:23,000
So it does have quite a bit to do with our meditation practice

329
00:27:23,000 --> 00:27:28,000
in terms of really understanding this.

330
00:27:28,000 --> 00:27:33,000
And in terms of being patient with our practice, be patient with good deeds

331
00:27:33,000 --> 00:27:35,000
that you do for others.

332
00:27:35,000 --> 00:27:38,000
Don't concern yourself with what?

333
00:27:38,000 --> 00:27:41,000
I don't see any benefit from this.

334
00:27:41,000 --> 00:27:43,000
Never think like that.

335
00:27:43,000 --> 00:27:45,000
Because you're very hard to see.

336
00:27:45,000 --> 00:27:48,000
Very hard to come back later and say, oh that's because of that.

337
00:27:48,000 --> 00:27:50,000
There's so many different factors involved.

338
00:27:50,000 --> 00:27:56,000
Look more on the actual deed and the immediate results.

339
00:27:56,000 --> 00:28:03,000
What you're sending out into the universe, the energy that you're creating.

340
00:28:03,000 --> 00:28:06,000
So very important that when you do a good deed, you let it go.

341
00:28:06,000 --> 00:28:07,000
You do the good deed.

342
00:28:07,000 --> 00:28:11,000
And when you say don't think about the results, it's almost true.

343
00:28:11,000 --> 00:28:16,000
It's just not quite true because you are very much focused on the immediate results.

344
00:28:16,000 --> 00:28:24,000
You're doing it because of the energy that it's putting out, positive energy.

345
00:28:24,000 --> 00:28:28,000
Don't concern yourself with actual results.

346
00:28:28,000 --> 00:28:32,000
And the same goes with meditation and things like morality and so on.

347
00:28:32,000 --> 00:28:34,000
Don't concern yourself.

348
00:28:34,000 --> 00:28:36,000
Why don't I feel better?

349
00:28:36,000 --> 00:28:39,000
Why do I still feel bad at times?

350
00:28:39,000 --> 00:28:41,000
And so why do I still have suffering?

351
00:28:41,000 --> 00:28:44,000
There's so many different factors involved.

352
00:28:44,000 --> 00:28:46,000
Some of them are even out of your control.

353
00:28:46,000 --> 00:28:51,000
You still feel physical pain, for example, and that kind of thing.

354
00:28:51,000 --> 00:28:54,000
But what we should look at is how the meditation is changing it.

355
00:28:54,000 --> 00:28:56,000
At the moment that we perform it.

356
00:28:56,000 --> 00:28:58,000
Let that go and do it.

357
00:28:58,000 --> 00:29:02,000
And cultivate it and then let it go and don't worry about how it's going to change your brain

358
00:29:02,000 --> 00:29:07,000
or your mind or your thoughts or the world around you.

359
00:29:07,000 --> 00:29:08,000
You should never think like that.

360
00:29:08,000 --> 00:29:14,000
Never be concerned with the more abstract results.

361
00:29:14,000 --> 00:29:19,000
Except kind of in the back of your head or as a, oh, gee, I'm a nicer person.

362
00:29:19,000 --> 00:29:22,000
Not that kind of thing, but that should never be your focus.

363
00:29:22,000 --> 00:29:26,000
In meditation, the results should be the immediate result.

364
00:29:26,000 --> 00:29:30,000
What happens when you're mindful in that moment and be content with that?

365
00:29:30,000 --> 00:29:32,000
That's how you know you're doing good.

366
00:29:32,000 --> 00:29:38,000
It's very important because otherwise you're constantly judging and wondering

367
00:29:38,000 --> 00:29:40,000
and everything is about expecting.

368
00:29:40,000 --> 00:29:43,000
I do a good deed because I expect some result.

369
00:29:43,000 --> 00:29:47,000
And you see how troublesome that is, how problematic that is

370
00:29:47,000 --> 00:29:49,000
it becomes no longer a good deed.

371
00:29:49,000 --> 00:29:53,000
It becomes more of a desire for good things to come to you.

372
00:29:53,000 --> 00:29:55,000
And meditation is the same.

373
00:29:55,000 --> 00:29:57,000
We don't meditate as a good deed anymore.

374
00:29:57,000 --> 00:30:04,000
Sometimes we meditate wanting good things to come to us, which can be an unwholesome thing.

375
00:30:04,000 --> 00:30:10,000
Instead, we should focus on the act and the moment, the benefit that comes from it.

376
00:30:10,000 --> 00:30:12,000
Because that will become real.

377
00:30:12,000 --> 00:30:13,000
That is real.

378
00:30:13,000 --> 00:30:15,000
That is here and that is now.

379
00:30:15,000 --> 00:30:19,000
So, that's the Damapada for tonight.

380
00:30:19,000 --> 00:30:23,000
Thank you all for tuning in, wishing you all a good practice.

381
00:30:23,000 --> 00:30:26,000
And peace happiness and freedom from suffering.

382
00:30:26,000 --> 00:30:49,000
Thank you.

