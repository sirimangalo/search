1
00:00:00,000 --> 00:00:25,920
Okay, good evening everyone, welcome to our evening session.

2
00:00:25,920 --> 00:00:46,560
I'm going to talk about, I'm going to talk a lot about refuge and protection.

3
00:00:46,560 --> 00:01:07,200
Basically the Buddha saw problems, he saw problems, he saw suffering, he was a pessimist,

4
00:01:07,200 --> 00:01:13,920
but we're really lucky that the Buddha saw the problems and saw suffering and that his

5
00:01:13,920 --> 00:01:25,000
concern was so much about things like refuge and protection, because if the Buddha had

6
00:01:25,000 --> 00:01:33,200
just talked about what was enjoyable in the world it wouldn't really help us at all, right?

7
00:01:33,200 --> 00:01:43,160
You don't come, practice meditation because you're really happy.

8
00:01:43,160 --> 00:01:52,320
Come to practice meditation generally because you see the problem where you want to find

9
00:01:52,320 --> 00:02:02,360
the problem, because you see the suffering.

10
00:02:02,360 --> 00:02:10,040
There's much danger in the world, there's much that we are susceptible to, simple facts

11
00:02:10,040 --> 00:02:17,320
of life, old age, sickness, death, beyond that there's so much, the ordinary person

12
00:02:17,320 --> 00:02:35,680
is so vulnerable, rich or poor, it doesn't really matter, our susceptibility, our vulnerability,

13
00:02:35,680 --> 00:02:46,880
so many kinds of suffering, daily, we wake up, from the time we wake up to the time we

14
00:02:46,880 --> 00:02:53,880
go to sleep, danger lurks, even in the form of the people around us, who knows when

15
00:02:53,880 --> 00:03:04,720
they're going to say something we don't like, do something that hurts us, even our own

16
00:03:04,720 --> 00:03:09,880
bodies, who knows what it's going to be like today, maybe today I'll have a backache

17
00:03:09,880 --> 00:03:21,120
or a headache or a stomachache, maybe I'll trip and fall or cut myself, this isn't

18
00:03:21,120 --> 00:03:26,040
even talking about when we have to go out into the world and work and meet with people

19
00:03:26,040 --> 00:03:34,120
who want to compete with us and want to manipulate us, people who try to, there are people

20
00:03:34,120 --> 00:03:43,720
who spend all their time trying to manipulate others and cheat them and hurt people who

21
00:03:43,720 --> 00:03:46,120
are bent on hurting others.

22
00:03:46,120 --> 00:03:56,440
It's amazing, know that the world is full of such things, it's enough to make you

23
00:03:56,440 --> 00:04:10,240
quite terrified of this world and so the Buddha taught about protection and I think it comes

24
00:04:10,240 --> 00:04:16,640
much from the fact that people are looking for protection, so if you have all these dangers

25
00:04:16,640 --> 00:04:31,560
in the world, you do it, you can't protect yourself and you seek out protection, where do

26
00:04:31,560 --> 00:04:42,760
we find protection, this is the question, the Buddha said, sonata bikouwe, we had at the

27
00:04:42,760 --> 00:04:54,640
dewella with protection, dewella is one who has a refuge, ma anata, not as one without

28
00:04:54,640 --> 00:05:02,600
refuge, don't just be this happy, go lucky person that just helps that everything turns

29
00:05:02,600 --> 00:05:17,960
out all right and then suffers terribly when things go wrong, be invincible, do kang bikouwe

30
00:05:17,960 --> 00:05:32,440
anata vyarati anata vyarati anata vyarati, one without a refuge, dwells in suffering.

31
00:05:32,440 --> 00:05:58,080
So, the Buddha taught the nata karana dhamma, dhamma's that, dhamma's act as protection,

32
00:05:58,080 --> 00:06:10,520
like this protection for us, so good dhamma's to learn, it provides a fairly rounded

33
00:06:10,520 --> 00:06:19,240
list of qualities, something we're thinking about, give us a really sort of well-rounded

34
00:06:19,240 --> 00:06:29,560
perspective on the practice, and it's useful to remember that it's not just mindfulness

35
00:06:29,560 --> 00:06:37,360
that protects us, mindfulness, I think is really the core, but there's much more that

36
00:06:37,360 --> 00:06:45,280
that we need to keep in mind in order to succeed, so the first thing that protects us

37
00:06:45,280 --> 00:06:51,520
is ethics, morality, of course, because if you're unethical, you're much more likely

38
00:06:51,520 --> 00:07:01,240
to lead to come across danger, if you're unethical you'll be subject to punishment,

39
00:07:01,240 --> 00:07:14,440
subject to retribution, enmity, malice, and even just the fear and guilt of being an evil

40
00:07:14,440 --> 00:07:31,760
person, a chara go chara sampana, one who knows what is suitable and what is unsuitable.

41
00:07:31,760 --> 00:07:41,800
Number two, one is a bhausuta, one that has learned much, it's much learning, coming

42
00:07:41,800 --> 00:07:48,240
to these talks every night, you'll learn more and more not just about yourself, but about

43
00:07:48,240 --> 00:07:52,000
the broader theory behind the practice.

44
00:07:52,000 --> 00:07:56,800
You guys spend all day learning about yourself, but you come here, I can't teach you about

45
00:07:56,800 --> 00:08:04,200
yourself, but I can give you some theory that will help you when you practice to learn

46
00:08:04,200 --> 00:08:05,200
about yourself.

47
00:08:05,200 --> 00:08:13,600
So to have learned much, this is very useful, so that you don't get lost, if you learn

48
00:08:13,600 --> 00:08:21,280
just a little, it's easy to get stuck because you think that what you know is everything

49
00:08:21,280 --> 00:08:27,240
and you build a small picture and think it's the big picture and it's easy to get lost

50
00:08:27,240 --> 00:08:33,760
because you have to fill in the blanks yourself and you get the wrong idea about the practice,

51
00:08:33,760 --> 00:08:44,960
when you've studied a lot, it's much harder to get lost like that.

52
00:08:44,960 --> 00:08:50,560
Number three, one has good friends.

53
00:08:50,560 --> 00:08:55,000
Good friends is interesting, we think of good friends as people who sit around and chat.

54
00:08:55,000 --> 00:09:00,880
Someone who can have long conversations with, so when you hear about this, you think,

55
00:09:00,880 --> 00:09:07,120
well, my mistake, I should be making friends with all the other meditators, maybe I'll

56
00:09:07,120 --> 00:09:11,040
go hang out in their room for a while.

57
00:09:11,040 --> 00:09:17,840
Don't do that, I catch you hanging out in each other's rooms, I'll be very disappointed.

58
00:09:17,840 --> 00:09:24,920
Well, all frown very, very severely.

59
00:09:24,920 --> 00:09:29,920
I'll try anyway.

60
00:09:29,920 --> 00:09:38,960
Now a good friend is, I mean, I'm the good friend here, if we don't become good friends,

61
00:09:38,960 --> 00:09:40,640
we don't move anymore in trouble.

62
00:09:40,640 --> 00:09:46,000
If I'm not a good friend, you're the more in big trouble.

63
00:09:46,000 --> 00:09:52,000
Good friend is one who gives you meditation practice.

64
00:09:52,000 --> 00:09:54,960
Good friend is one who supports you in your practice.

65
00:09:54,960 --> 00:10:07,920
The Buddha is our friend, I'm just an emissary, but we're lucky we have this friendship

66
00:10:07,920 --> 00:10:09,720
and we're friends with each other.

67
00:10:09,720 --> 00:10:10,960
How are we friends with each other?

68
00:10:10,960 --> 00:10:15,960
How are you friends with each other, even if you don't know each other's names?

69
00:10:15,960 --> 00:10:19,440
Because you give each other the opportunity to practice, because you don't go and hang

70
00:10:19,440 --> 00:10:20,800
out in each other's rooms.

71
00:10:20,800 --> 00:10:27,960
It's a really bad friend who spent a waste all your time chatting you up about useless

72
00:10:27,960 --> 00:10:28,960
things.

73
00:10:28,960 --> 00:10:37,440
It's a bad friend who sits in your room and asks you about your life and makes jokes

74
00:10:37,440 --> 00:10:43,480
and that's a friend who wastes your time.

75
00:10:43,480 --> 00:10:51,200
Good friend is the one who says that person's meditating, I will just let them be, give

76
00:10:51,200 --> 00:10:53,920
them the opportunity to learn about themselves.

77
00:10:53,920 --> 00:10:59,800
It's quite difficult, you know, just to live in such close quarters and not waste people

78
00:10:59,800 --> 00:11:05,800
and waste each other's time.

79
00:11:05,800 --> 00:11:11,400
Not distract each other from the practice, you're doing such a good service to each other,

80
00:11:11,400 --> 00:11:22,120
such as being such good friends to each other by not interacting, by purposefully being

81
00:11:22,120 --> 00:11:26,800
peaceful with each other.

82
00:11:26,800 --> 00:11:31,800
You all are supporting each other's practice, you think about it, think about how supportive

83
00:11:31,800 --> 00:11:38,360
it is to see other people practicing and imagine how everyone else is feeling about you

84
00:11:38,360 --> 00:11:47,000
and they see you practicing.

85
00:11:47,000 --> 00:11:56,620
Number four, one is Suwaju, Suwaju, Suwaju, Suwaju, Suwaju, Suwaju, Suwaju, Suwaju, Suwaju, Suwaju, Suwaju, Suwaju, Suwaju,

86
00:11:56,620 --> 00:12:10,600
Suwaju, Suwaju, Suwaju, Suwaju means one is easy to talk to, easy to teach.

87
00:12:10,600 --> 00:12:16,680
Easy to teach, this is a refuge because nobody wants to teach someone who's hard to teach.

88
00:12:16,680 --> 00:12:23,400
It's funny, sometimes people forget that I'm not getting paid to do this and they demand

89
00:12:23,400 --> 00:12:31,920
a view, but sometimes they assume that I have some obligation to put up with them.

90
00:12:31,920 --> 00:12:37,320
I really don't, and I don't mean for myself, I mean it's remarkable how you read in

91
00:12:37,320 --> 00:12:41,760
the ancient texts, an arrow hunt if someone came to these arrow hunts and asked them

92
00:12:41,760 --> 00:12:47,760
to teach, they'd often just say, look, you wouldn't understand if I told you anyway.

93
00:12:47,760 --> 00:12:56,560
It would just be, it would just be a tie or something for me. They just wouldn't teach people, you know?

94
00:12:56,560 --> 00:13:05,760
If they saw someone was just, how was it ready for it?

95
00:13:05,760 --> 00:13:08,760
And you think, you know, we're so used to thinking on these teachers, we shouldn't be

96
00:13:08,760 --> 00:13:17,600
compassion and they really want to teach, you know, no, no, nope.

97
00:13:17,600 --> 00:13:22,640
I'm saying this as a warning, you know, if you're hard to teach, it's quite possible

98
00:13:22,640 --> 00:13:30,240
that people just won't want to teach you.

99
00:13:30,240 --> 00:13:36,440
And nobody's under any obligation to do so.

100
00:13:36,440 --> 00:13:49,920
It's very scary, you know, but it said this is death.

101
00:13:49,920 --> 00:13:58,600
It's death, if people think, this person's not worth teaching, it's death for a monk,

102
00:13:58,600 --> 00:14:00,800
death for a Buddhist.

103
00:14:00,800 --> 00:14:06,800
They'll never be like that, always be someone who's easy to teach.

104
00:14:06,800 --> 00:14:12,240
It doesn't mean you have to agree with everything, but if you find that a teacher in

105
00:14:12,240 --> 00:14:20,640
our teaching is not for you, you ask questions, but if you really feel like you just

106
00:14:20,640 --> 00:14:25,920
can't understand and can't agree with it, then go away, go somewhere else, find another

107
00:14:25,920 --> 00:14:40,680
teacher, when you find a good teacher, or a teaching that is good, try your best to listen

108
00:14:40,680 --> 00:14:41,680
and to learn.

109
00:14:41,680 --> 00:14:46,920
Suaju means not arguing on the one hand, but it also means understanding.

110
00:14:46,920 --> 00:14:52,240
I know people, I have students sometimes who don't argue, but they don't understand either,

111
00:14:52,240 --> 00:14:56,800
and they don't even try to understand, they just say yes, yes, yes, but then they go away

112
00:14:56,800 --> 00:15:00,200
and they find they didn't even do what I told them.

113
00:15:00,200 --> 00:15:08,120
None of you, no, this isn't directed in any of you, but we all recognize a little bit

114
00:15:08,120 --> 00:15:14,600
of all of these in us, so if you recognize a little bit in you, then you're all very

115
00:15:14,600 --> 00:15:22,200
easy to teach, so don't worry about that, but it's good to know, so we can keep

116
00:15:22,200 --> 00:15:24,640
these things in mind.

117
00:15:24,640 --> 00:15:31,440
Suaju doesn't just mean being complacent, it also means really making effort to try and

118
00:15:31,440 --> 00:15:39,120
understand and listen, focus on the teaching.

119
00:15:39,120 --> 00:15:48,120
Number five is helping each other with whatever tasks there are.

120
00:15:48,120 --> 00:15:52,240
The community is a refuge for us, right?

121
00:15:52,240 --> 00:15:58,680
So supporting the community, doing the functions of the community, right?

122
00:15:58,680 --> 00:16:04,040
So this is all the cleaning that you guys are doing every day, I'm sure.

123
00:16:04,040 --> 00:16:10,440
I don't look, but I'm thinking everything, it must be very spotless because you all spend

124
00:16:10,440 --> 00:16:15,440
time every day cleaning.

125
00:16:15,440 --> 00:16:21,440
At our center, we have no cleaning people, we employ the meditators to clean up after themselves

126
00:16:21,440 --> 00:16:27,680
and clean up after each other, it's a good practice, you know?

127
00:16:27,680 --> 00:16:34,560
And here we have it, this is a refuge because this keeps the community going, it allows

128
00:16:34,560 --> 00:16:38,040
the community to exist.

129
00:16:38,040 --> 00:16:43,040
If we didn't help out, if we didn't work at it, then it would change, you know?

130
00:16:43,040 --> 00:16:48,000
If we had to employ someone or something, we never would, but suppose some meditation

131
00:16:48,000 --> 00:16:53,000
center that has employees that do this work, then they start to, you know, then what

132
00:16:53,000 --> 00:17:01,160
they do, they charge money and it goes very much downhill, I would say, when you just

133
00:17:01,160 --> 00:17:08,000
help out, when we run as a community, and you think of this as your home, and you'd

134
00:17:08,000 --> 00:17:20,200
be responsible for it, this is a refuge, this allows us to have this beautiful meditation

135
00:17:20,200 --> 00:17:30,600
community.

136
00:17:30,600 --> 00:17:39,560
Number six, one is Dhamma Kamo, Dhamma Kamo is interesting, Kama is actually desire, Kama

137
00:17:39,560 --> 00:17:52,040
is this liking, Kama is a bad thing, usually any kind of desire is bad, but Dhamma Kamo is

138
00:17:52,040 --> 00:18:01,320
different, it's a figure of speech really, but it means one who likes the Dhamma, right?

139
00:18:01,320 --> 00:18:07,080
Do you like the Buddha's teaching or do you secretly hate it and just swallowing this

140
00:18:07,080 --> 00:18:09,080
bitter pill?

141
00:18:09,080 --> 00:18:13,960
If you don't like the Buddha's teaching and you're just putting up with it because

142
00:18:13,960 --> 00:18:17,720
we give you a meditation practice, then there's a problem.

143
00:18:17,720 --> 00:18:23,600
I think sometimes we do once in a while and get people like that who aren't interested

144
00:18:23,600 --> 00:18:29,200
in Buddhism, but I just want to learn how to meditate and well, this is the only place

145
00:18:29,200 --> 00:18:38,720
they could find, something like that, or it was free or something like that.

146
00:18:38,720 --> 00:18:44,000
That's a problem, makes it very difficult to succeed.

147
00:18:44,000 --> 00:18:50,480
You know you're on the right track and you know you're powerful when the Dhamma is

148
00:18:50,480 --> 00:18:59,280
joy, enjoy is enjoyable, doesn't mean the meditation practice is not stressful and painful

149
00:18:59,280 --> 00:19:06,880
and tough, it's very tough, it challenges you to face things that you don't want to face,

150
00:19:06,880 --> 00:19:10,880
but when you do it gladly and when you listen to the Buddha's teaching and just think

151
00:19:10,880 --> 00:19:32,120
that's my cup of tea, that's a real refuge, that's a sign that you're out to succeed.

152
00:19:32,120 --> 00:19:40,480
Some people are always doubting skeptical, maybe even indifferent, okay I'll try it,

153
00:19:40,480 --> 00:19:45,200
you know that's dangerous, it's understandable, but it's a sign that they don't get

154
00:19:45,200 --> 00:19:58,680
to have this refuge of really just feeling joy adhering the Buddha's teaching, feeling

155
00:19:58,680 --> 00:20:05,280
joy at the idea of practicing meditation, that's a real refuge.

156
00:20:05,280 --> 00:20:21,440
Number seven, number seven one is a strong effort, arada video, arada video, one is energetic,

157
00:20:21,440 --> 00:20:31,320
laziness of course is our enemy, when you're energetic this is a refuge, it's a refuge

158
00:20:31,320 --> 00:20:36,480
because it means you keep going, you don't have to push hard or anything in the practice,

159
00:20:36,480 --> 00:20:42,800
energy is always a tricky one because if you push it becomes controlling, trying to control,

160
00:20:42,800 --> 00:20:48,960
real energy is mental, it's even when you're very tired, the energy to continue to be mindful,

161
00:20:48,960 --> 00:21:06,880
to the moment you fall asleep, that's a refuge, it's the effort to give up on wholesomeness

162
00:21:06,880 --> 00:21:18,880
and to cultivate wholesomeness really. Number eight, one is content, content with whatever,

163
00:21:18,880 --> 00:21:30,560
that's a refuge, discontent is a big danger, especially for a meditator, even just discontent

164
00:21:30,560 --> 00:21:36,640
with food, and a meditation centre you might get fairly simple food, maybe it's not

165
00:21:36,640 --> 00:21:45,000
the food that you're used to, it's funny hearing Buddhists complain sometimes because

166
00:21:45,000 --> 00:21:50,960
Buddhists are in very different countries, so you've got the Chinese Buddhists and you've

167
00:21:50,960 --> 00:21:54,680
got the Thai Buddhists and you've got the Sri Lankan Buddhists and the Burmese Buddhists

168
00:21:54,680 --> 00:22:03,840
and the Western Buddhists and our foods are all different, so it's fun watching us in

169
00:22:03,840 --> 00:22:13,480
each other's cultures and complaining about the food. It can be very difficult, very difficult

170
00:22:13,480 --> 00:22:22,200
for Thai people to eat Sri Lankan food or Western people to eat Thai food, sometimes

171
00:22:22,200 --> 00:22:29,200
in Western people like Thai food, but it can be difficult after a while to just eat rice

172
00:22:29,200 --> 00:22:38,080
every day, every day, if you're not used to rice, be content with your room, be content

173
00:22:38,080 --> 00:22:46,440
with this place, sometimes in the beginning a meditator might have very discontent, there's

174
00:22:46,440 --> 00:22:52,360
nothing, it's not a beautiful place, right, just this room, it's kind of like a dungeon

175
00:22:52,360 --> 00:22:59,280
down there, very easy to get discontent, you get caught up in this, right, because there's

176
00:22:59,280 --> 00:23:05,720
nothing wrong with it, it's not a dungeon and what's wrong with a dungeon, right, we

177
00:23:05,720 --> 00:23:12,840
can give it this word, give it this idea of oh this is like someone else might look at

178
00:23:12,840 --> 00:23:28,200
it and say wow, I've got my own cave, I'm a cave-dwelling meditator, I'd be very happy

179
00:23:28,200 --> 00:23:36,720
with it, cultivating contentment is important, oh it's incredibly important, contentment

180
00:23:36,720 --> 00:23:45,040
really describes the path in one way, describes the goal in one way, contentment means

181
00:23:45,040 --> 00:23:55,400
not wishing things were otherwise, contentment means objectivity, however it is, I'm still

182
00:23:55,400 --> 00:24:04,480
happy, still at peace, number nine is mindfulness of course, we have to include mindfulness

183
00:24:04,480 --> 00:24:10,720
in here, one has mindfulness, and the body uses a term that's, we don't talk about much,

184
00:24:10,720 --> 00:24:25,720
it's satin napaca, satin napaca, napaca is from napaca, napaca means like discerning, and

185
00:24:25,720 --> 00:24:30,360
there's lots of words that pair with mindfulness, and it's the way they would have to read

186
00:24:30,360 --> 00:24:37,200
too much into it, but basically what the Buddha is saying is, he's satin and it involves

187
00:24:37,200 --> 00:24:45,200
itself often with wisdom, so we have sati sampajanya, sampajanya is also wisdom, napaca,

188
00:24:45,200 --> 00:24:52,520
it's a kind of wisdom, because mindfulness allows you to grasp an object, fully grasp

189
00:24:52,520 --> 00:25:02,880
it, mindful mind is not wavering, it's not forgetful, and so it confronts the object,

190
00:25:02,880 --> 00:25:13,920
it understands the object, or it allows for the understanding to arise, and of course that's

191
00:25:13,920 --> 00:25:20,760
our greatest refuge, and the Buddha elsewhere has said that one dwells with oneself

192
00:25:20,760 --> 00:25:28,600
as a refuge by practicing the four sati patana, the four foundations of mindfulness are

193
00:25:28,600 --> 00:25:39,160
the greatest refuge, because they make you invincible, if you can cultivate that objectivity,

194
00:25:39,160 --> 00:25:43,080
then what could possibly hurt you?

195
00:25:43,080 --> 00:25:48,280
Everything in existence boils down to experience, there's nothing out there that's not

196
00:25:48,280 --> 00:25:54,680
going to be experienced, and if you can be at peace with experience, so you're not judging

197
00:25:54,680 --> 00:26:04,960
it, then you're invincible, nothing can hurt you, nothing can make you suffer, and number

198
00:26:04,960 --> 00:26:15,640
ten is wisdom, so satingos with wisdom, satipanya, nibedikaya, samma dukkaya gāminya,

199
00:26:15,640 --> 00:26:35,400
wisdom that leads to detachment, penetrating wisdom, noble penetrating and leading to the

200
00:26:35,400 --> 00:26:43,920
right destruction of suffering, so we talk a lot about wisdom, it's really the key and

201
00:26:43,920 --> 00:26:49,480
it's really the pinnacle in Buddhism, but wisdom isn't something that you're study or

202
00:26:49,480 --> 00:26:50,960
something that you hear, right?

203
00:26:50,960 --> 00:26:56,800
Wisdom means to know, wisdom is when you see that the things you thought were worth

204
00:26:56,800 --> 00:27:01,600
clinging to are not worth clinging to, doesn't even feel like wisdom is not how we think

205
00:27:01,600 --> 00:27:12,280
of wisdom in the West, but it's an awareness, it's a knowledge, like when you touch fire

206
00:27:12,280 --> 00:27:18,840
and then you know that the fire is hot, that kind of knowledge, when you know not to get

207
00:27:18,840 --> 00:27:25,720
angry because that's just silly, you know not to be greedy, when you eat and you're no

208
00:27:25,720 --> 00:27:33,360
longer attached to the food, because you've learned that food is not worth clinging to,

209
00:27:33,360 --> 00:27:44,520
greed is not a useful thing, so the Buddha said sanata bekavai bekavai bekanata, dwell as

210
00:27:44,520 --> 00:27:50,920
one with a refuge and these are ten dhammas that allow one to have a refuge to have a

211
00:27:50,920 --> 00:28:01,880
protector, protection, there you go, that's the dhamma for tonight, something useful for us,

212
00:28:01,880 --> 00:28:07,160
anything that are helpful in our practice, hopefully, thank you.

213
00:28:31,880 --> 00:29:00,560
Okay, some questions, three questions tonight.

214
00:29:00,560 --> 00:29:04,720
Did one have increasing meditation time or just be more content with the present reality

215
00:29:04,720 --> 00:29:10,240
or the ways to increase mindfulness without formal meditation?

216
00:29:10,240 --> 00:29:20,840
I want to do both, ideally, but you know, I mean, it's not magic, there's no magic formula,

217
00:29:20,840 --> 00:29:25,000
we're talking about building habits, and at the same time as you're building the habit

218
00:29:25,000 --> 00:29:31,760
of being mindful, other habits are conflicting with it, so if you're just relying on

219
00:29:31,760 --> 00:29:37,200
daily mindfulness, well the question is how mindful can you be, and in the beginning

220
00:29:37,200 --> 00:29:42,760
probably not very much, because it's a new habit, so there's no magic, there's no answer

221
00:29:42,760 --> 00:29:51,560
to your question besides that, the more you can be mindful, the more quickly the habit

222
00:29:51,560 --> 00:29:57,840
is going to be cultivated, it's completely up to you how quick you, how long it takes

223
00:29:57,840 --> 00:30:09,160
you to cultivate mindfulness, I lost one question, did I delete the wrong question or someone

224
00:30:09,160 --> 00:30:12,120
else delete it?

225
00:30:12,120 --> 00:30:18,320
One of the five precepts, not term-living things, came upon a spider, does that precept apply

226
00:30:18,320 --> 00:30:27,480
to living things, yes, it applies to animals, peoples, bugs, doesn't apply to plants,

227
00:30:27,480 --> 00:30:54,480
to sentient beings, that's it, so thank you all for coming out.

