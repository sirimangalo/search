1
00:00:00,000 --> 00:00:02,760
Welcome back to Ask a Monk.

2
00:00:02,760 --> 00:00:11,560
This question comes from Jung Taeq Singh who asks, do we need to give up all materialistic

3
00:00:11,560 --> 00:00:15,880
pleasure?

4
00:00:15,880 --> 00:00:19,760
Giving up on giving up, giving up pleasure.

5
00:00:19,760 --> 00:00:22,160
No, you don't have to give up any pleasure.

6
00:00:22,160 --> 00:00:25,080
This is really the key.

7
00:00:25,080 --> 00:00:30,880
You have to give up your attachment to the pleasure, and there's a difference.

8
00:00:30,880 --> 00:00:34,400
You can experience pleasure without being attached to it.

9
00:00:34,400 --> 00:00:35,800
That's clear.

10
00:00:35,800 --> 00:00:43,880
The body has many states, even the mind has many states, and these don't all necessarily

11
00:00:43,880 --> 00:00:49,800
stem from greed or co-exist with attachment.

12
00:00:49,800 --> 00:00:55,320
But attachment is a sort of stress, it's something that brings the mind out of its state

13
00:00:55,320 --> 00:00:57,760
of peace and happiness.

14
00:00:57,760 --> 00:01:03,400
And if you engage in attachment addiction, wanting desire, and so on, you're only going

15
00:01:03,400 --> 00:01:08,960
to become more and more miserable when you compartmentalize reality into good and bad and

16
00:01:08,960 --> 00:01:13,720
therefore don't get always what you want because you have wants.

17
00:01:13,720 --> 00:01:18,600
If you're able to accept reality for what it is, then it's not a question of whether it's

18
00:01:18,600 --> 00:01:22,440
pleasure or pain at all.

19
00:01:22,440 --> 00:01:28,320
You're able to live in harmony with the world around you and harmony with reality as

20
00:01:28,320 --> 00:01:36,240
it is, accepting, change, accepting the good and the bad and not giving labels to things

21
00:01:36,240 --> 00:01:39,360
is good or bad.

22
00:01:39,360 --> 00:01:43,640
But I think more than that, it's important to understand that you don't have to give up

23
00:01:43,640 --> 00:01:45,680
anything at all.

24
00:01:45,680 --> 00:01:48,320
Buddhism is not a dog.

25
00:01:48,320 --> 00:01:56,640
There's no theory involved in terms of you have to let go of this or let go of that.

26
00:01:56,640 --> 00:02:04,720
The point is that you're suffering and you may not realize that, for many people they

27
00:02:04,720 --> 00:02:10,720
don't realize that, they're not able to see their state of existence as having suffering

28
00:02:10,720 --> 00:02:13,560
in it.

29
00:02:13,560 --> 00:02:22,960
But you are, and we all are, and once we realize that there is suffering, then we want

30
00:02:22,960 --> 00:02:24,320
to find a way out of it.

31
00:02:24,320 --> 00:02:29,440
So we start looking and once we start looking, we realize that the cause of suffering is

32
00:02:29,440 --> 00:02:30,760
really your attachment.

33
00:02:30,760 --> 00:02:39,480
As I said, the attachments to this and to that, to pleasure the attachments to being

34
00:02:39,480 --> 00:02:47,880
in a certain existence, to being something, our attachments to not being something, these

35
00:02:47,880 --> 00:02:52,040
are a cause of suffering because we're not able to accept reality for what it is, we're

36
00:02:52,040 --> 00:02:55,560
not able to accept the experience.

37
00:02:55,560 --> 00:03:00,600
And when you realize this, you let go by yourself.

38
00:03:00,600 --> 00:03:03,280
You realize the stress that you're causing.

39
00:03:03,280 --> 00:03:08,640
When you see something as causing you stress, if you see it clearly, there's no thought

40
00:03:08,640 --> 00:03:10,800
involved at all.

41
00:03:10,800 --> 00:03:18,280
It immediately changes who you are, your way of behaving changes so that you don't go that

42
00:03:18,280 --> 00:03:19,840
way anymore.

43
00:03:19,840 --> 00:03:25,800
You would never do something when you know that, when you know, when you've verified

44
00:03:25,800 --> 00:03:32,120
for yourself, that it's causing you suffering, that's the nature of the mind.

45
00:03:32,120 --> 00:03:37,040
We only do things because we think it's going to cause us a pleasure and happiness

46
00:03:37,040 --> 00:03:41,480
and bring about our some sort of benefit.

47
00:03:41,480 --> 00:03:46,160
When we see that there's no benefit in something, and see clearly means in a meditative

48
00:03:46,160 --> 00:03:48,960
sense, we won't engage in that.

49
00:03:48,960 --> 00:03:54,800
So you don't ever have to worry about having to give something up, thinking, oh, I'm going

50
00:03:54,800 --> 00:03:56,280
to have to give up this or that.

51
00:03:56,280 --> 00:03:57,280
That's not the point.

52
00:03:57,280 --> 00:04:04,920
The point is, look closer, look clearer, give up those things that are stopping you from

53
00:04:04,920 --> 00:04:10,040
seeing clearly, of course, give up drinking, alcohol and drugs and so on.

54
00:04:10,040 --> 00:04:16,480
Because you can't see objectively with your mind in intoxicated.

55
00:04:16,480 --> 00:04:21,920
For instance, give up entertainment to a certain degree, to the degree that it's getting

56
00:04:21,920 --> 00:04:27,320
in the way of your clear investigation of reality.

57
00:04:27,320 --> 00:04:29,480
But that's just a technical issue.

58
00:04:29,480 --> 00:04:35,560
If you don't give it up, you won't be able to undertake the scientific inquiry.

59
00:04:35,560 --> 00:04:39,440
But once you've gone that far, you know, start to look and don't take my word for it,

60
00:04:39,440 --> 00:04:43,440
that you have to give up this or that or everything, and just start looking.

61
00:04:43,440 --> 00:04:48,720
And then the better you understand reality, the more you'll give up until finally you give

62
00:04:48,720 --> 00:04:56,120
up everything by yourself, there's no forcing involved or pushing or pulling at all.

63
00:04:56,120 --> 00:05:03,520
And it's wisdom, once you see things as they are, you don't want to cling because it's

64
00:05:03,520 --> 00:05:08,960
not a belief, it's not a theory, there's no happiness that comes from clinging.

65
00:05:08,960 --> 00:05:34,080
So I'll have the best hope that helped.

