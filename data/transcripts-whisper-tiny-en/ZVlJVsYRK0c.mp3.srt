1
00:00:00,000 --> 00:00:07,480
I am welcome to ask a monk. Today, I have a question from Ghani, so we

2
00:00:07,480 --> 00:00:11,680
hate you to Damo. I find it easy to meditate if there's a drowning noise in the

3
00:00:11,680 --> 00:00:15,880
background. I eventually tune it out, but it helps me start. Is this an

4
00:00:15,880 --> 00:00:26,760
efficient way to meditate? Short answer, no. Sorry. Meditation is to see things for

5
00:00:26,760 --> 00:00:30,720
what they are to understand things for what they are. When, first of all, when

6
00:00:30,720 --> 00:00:33,760
you say something makes it easy to meditate, you have to understand that's not

7
00:00:33,760 --> 00:00:39,640
necessarily a good thing. Meditation works because it's not easy, because it

8
00:00:39,640 --> 00:00:49,800
challenges you, because it pushes the mind to become straight, to stop clinging to

9
00:00:49,800 --> 00:00:58,480
things. A very good analogy is training the body, a physical work out. If it's

10
00:00:58,480 --> 00:01:02,440
easy, if you find some way to make it easy, you know, like taking all the weights

11
00:01:02,440 --> 00:01:07,760
off or so on, you don't really gain anything. It's through the difficulty of the

12
00:01:07,760 --> 00:01:12,440
practice of having to bear with things that are unpleasant and uncomfortable

13
00:01:12,440 --> 00:01:22,000
that you actually train your mind to let go. Second of all, the idea that an

14
00:01:22,000 --> 00:01:28,240
external stimulus might somehow help in meditation at any rate is really, I

15
00:01:28,240 --> 00:01:34,720
would say, a misunderstanding of what is meditation, because meditation would

16
00:01:34,720 --> 00:01:38,480
be to understand the drowning noise itself and to understand what's going on

17
00:01:38,480 --> 00:01:44,120
with your mind that makes it seem like it's making you more meditative. Does

18
00:01:44,120 --> 00:01:47,480
that mean it's bringing a peaceful state or does that mean it's making you

19
00:01:47,480 --> 00:01:53,120
happy or does that mean it's putting into sleep or so on? It's generally a case

20
00:01:53,120 --> 00:01:57,920
that it helps your mind to get into a rhythm so you don't have to think, so

21
00:01:57,920 --> 00:02:05,000
you don't have to deal with the bumps and the rock you wrote of experience,

22
00:02:05,000 --> 00:02:09,480
you're able to smooth it out so that you feel comfortable and you feel calm

23
00:02:09,480 --> 00:02:14,120
and there's a sense of liking and attachment and clinging and therefore

24
00:02:14,120 --> 00:02:20,000
eventually a state of compartmentalizing reality into this is acceptable

25
00:02:20,000 --> 00:02:23,600
and that is not acceptable, this is good and that is not good and

26
00:02:23,600 --> 00:02:28,320
therefore suffering. So it's all of these things and I get a lot of questions

27
00:02:28,320 --> 00:02:31,720
like this about, you know, is this helpful for meditation? Is that helpful

28
00:02:31,720 --> 00:02:34,360
for meditation? There's nothing really that's going to help you in your

29
00:02:34,360 --> 00:02:39,080
meditation except meditating and meditating on everything, meditating on the

30
00:02:39,080 --> 00:02:42,640
bad stuff, meditating on the good stuff, helping you to see how your mind

31
00:02:42,640 --> 00:02:49,080
reacts to things and helping to straighten your mind so that you no longer

32
00:02:49,080 --> 00:02:52,880
cling to things, you no longer judge things. You're able to see things for what

33
00:02:52,880 --> 00:03:00,960
they are. Your response to life is one of wisdom and understanding. You don't

34
00:03:00,960 --> 00:03:07,360
look at something and say, oh, that's good and I'm attracted to it. You say, oh, that is that, you know what it is and when

35
00:03:07,360 --> 00:03:13,920
you know what it is, you react accordingly, you react in a way that doesn't

36
00:03:13,920 --> 00:03:19,000
doesn't lead you to suffering, it doesn't cause you to hurt others as well.

37
00:03:19,000 --> 00:03:23,600
So it's important to understand that the meditative state is simply the

38
00:03:23,600 --> 00:03:28,080
the clear awareness of something. When you have an unfocused state of mind,

39
00:03:28,080 --> 00:03:32,880
meditation is to understand the unfocused state of mind and understanding the

40
00:03:32,880 --> 00:03:36,720
causes of it, understanding the nature of it and seeing that you'd be better

41
00:03:36,720 --> 00:03:40,520
off without it so that you never give rise to that sort of state of mind. You

42
00:03:40,520 --> 00:03:45,840
don't give rise to the causes because you know that what they're going to

43
00:03:45,840 --> 00:03:52,360
lead to. If you avoid it by, you know, a droning noise is just another drug,

44
00:03:52,360 --> 00:03:56,400
it's like, well, saying, you know, why don't you take marijuana that helps you

45
00:03:56,400 --> 00:04:02,040
to feel calm and peaceful, why don't you take alcohol because it, it makes you

46
00:04:02,040 --> 00:04:07,120
lose your, your, your suffering, your pain, but it's an addiction. You know, you

47
00:04:07,120 --> 00:04:11,200
can sit and watch TV, you can plug yourself into music and so on. And all of

48
00:04:11,200 --> 00:04:15,600
these things are going to make, give you that sense of, of, you could say of

49
00:04:15,600 --> 00:04:22,040
meditation where you're, you feel, you feel comfortable, you feel focused, but

50
00:04:22,040 --> 00:04:27,400
it's not meditation because you're not really seeing things as they are. So I

51
00:04:27,400 --> 00:04:32,160
would say the fact that you need a droning noise is a sign that something's

52
00:04:32,160 --> 00:04:39,000
wrong. It's a sign that you're not able to accept reality for what it is. You,

53
00:04:39,000 --> 00:04:42,440
you're, you're addicted to something and that's normal. That's natural for all of

54
00:04:42,440 --> 00:04:45,840
us. We're, we're in this state and, and that's what we're working on in

55
00:04:45,840 --> 00:04:50,120
our practice. So I would turn the droning noise off and figure out what's wrong

56
00:04:50,120 --> 00:04:54,360
and look at that. If you feel distracted, say to yourself distracted, distracted,

57
00:04:54,360 --> 00:05:01,720
just learn about it. If, if you feel agitated or, or whatever, just focus on, on

58
00:05:01,720 --> 00:05:06,400
the phenomenon as it is and, and come to see what are the causes and the effects,

59
00:05:06,400 --> 00:05:12,040
what is the nature of it? And eventually do away with it for real rather than

60
00:05:12,040 --> 00:05:16,920
avoiding it. Okay. So hope that helps. And I think that's a good question

61
00:05:16,920 --> 00:05:21,080
because I get a lot of questions like that. So I hope this helps. Several people

62
00:05:21,080 --> 00:05:25,800
who may be wondering about AIDS to meditation and guarantee that there isn't

63
00:05:25,800 --> 00:05:55,640
one. Okay. So thanks for, for all the questions and keeping coming.

