1
00:00:00,000 --> 00:00:08,320
Masaki has a question. This may sound like an odd question, but I'm simply curious. Sex

2
00:00:08,320 --> 00:00:13,520
in Buddhism is frowned upon and in the case of the eight and ten precepts prohibited.

3
00:00:13,520 --> 00:00:19,280
However, in the Vajrayana Buddhism, they practice tantric sex. Isn't this a breach

4
00:00:19,280 --> 00:00:34,600
of the Buddha's teaching? Well, first of all, first of all, Vajrayana Buddhism isn't

5
00:00:34,600 --> 00:00:42,120
following the Buddha's teaching, per se. It's following certain teachings that are designed

6
00:00:42,120 --> 00:00:51,320
to allow one to become a Buddha or a Bodhisattva. And so it's not quite clear. I mean, the word

7
00:00:51,320 --> 00:01:02,120
Vajrayana is coined by a certain group of Tibetan Buddhists and to describe a certain

8
00:01:02,120 --> 00:01:07,240
specific set of practices. Tantra, of course, is a Hindu concept. It's something that was

9
00:01:07,240 --> 00:01:14,240
developed by Hindu yogis who have their own ideas about enlightenment and samadhi and

10
00:01:14,240 --> 00:01:25,800
so on. And it was adopted by Mahayana Buddhists who were, you know, from a cynical

11
00:01:25,800 --> 00:01:40,000
travata point of view, it was just a bunch of fake Buddhists, brahmanas, who, yeah, that's

12
00:01:40,000 --> 00:01:51,480
okay, we can pause it there. Okay, so tantra is a Hindu concept, the idea of tantric

13
00:01:51,480 --> 00:01:59,120
sex and so on. But it's based on this Hindu idea of the energies in the body and the

14
00:01:59,120 --> 00:02:05,960
development of high states of concentration and godhood and so on. It's much more to do

15
00:02:05,960 --> 00:02:14,360
with a acceptance of samsara. And so from a cynical travata point of view, it's

16
00:02:14,360 --> 00:02:22,280
say that this is simply how it arose, that there were many brahmana or Hindu ascetics

17
00:02:22,280 --> 00:02:28,080
who saw that Buddhism was so well liked and well received and there was even a king,

18
00:02:28,080 --> 00:02:34,480
king of Soka who became Buddhist and very much favored the Buddhist who joined the order

19
00:02:34,480 --> 00:02:38,000
and started with all their, including all their heredic doctrines and that's what

20
00:02:38,000 --> 00:02:44,920
Rokhasanga happens on. And so it depends who you ask. Of course tantric Buddhists or

21
00:02:44,920 --> 00:02:52,240
Vajrayana Buddhists have their own theories about the use and the benefit of tantric

22
00:02:52,240 --> 00:02:58,640
Buddhic tantric sex. And they say, you know, it's the only, it's only allowed for initiates

23
00:02:58,640 --> 00:03:04,480
and for people who have been initiated and it's only allowed in certain situations.

24
00:03:04,480 --> 00:03:10,960
But they also drink alcohol as a tantric practice. So one thing you could say is that

25
00:03:10,960 --> 00:03:16,840
it's part of their efforts to learn everything, to see everything, to come to understand

26
00:03:16,840 --> 00:03:22,440
everything, to have a full spectrum of reality because they're trying to become a Buddha

27
00:03:22,440 --> 00:03:28,440
for themselves or something like that. So on the one hand, there's a very, very much

28
00:03:28,440 --> 00:03:34,160
a difference of opinion among Buddhists over what is correct practice. But on the other

29
00:03:34,160 --> 00:03:39,840
hand, even from within the orthodox teravat of viewpoint, there is some leeway for coming

30
00:03:39,840 --> 00:03:46,920
to understand everything. I mean, a body sat there really does. He does engage in family

31
00:03:46,920 --> 00:03:51,320
life and having children and so on. He goes through many different lives, learning many

32
00:03:51,320 --> 00:03:55,120
different things because in the end he has to come to understand everything. So it has

33
00:03:55,120 --> 00:04:01,480
to be much more well-rounded than someone who's just following a long after and reaping

34
00:04:01,480 --> 00:04:09,080
the fruits of the Buddha's teaching. So you might say that there's some leeway given

35
00:04:09,080 --> 00:04:17,600
there. As far as including something like tantric sex in the orthodox teravat of meditation

36
00:04:17,600 --> 00:04:26,280
course, it's absurd really, because there's no need to learn the evils of central desire

37
00:04:26,280 --> 00:04:31,720
or the disadvantages of them. The Buddha's already taught them. The Buddha's already

38
00:04:31,720 --> 00:04:38,080
explained those things and he's already given us a course to understand the desire that

39
00:04:38,080 --> 00:04:43,960
arises. He's given us a means of focusing on the desire itself. The means of breaking

40
00:04:43,960 --> 00:04:50,480
up the experience. So we don't have to repeatedly again and again realize the addiction

41
00:04:50,480 --> 00:05:01,760
and the attachments and so on. We don't need the energy that comes from sexual retention

42
00:05:01,760 --> 00:05:05,760
is it called? I'm not sure that he was holding back at the moment of ejaculation. I

43
00:05:05,760 --> 00:05:11,640
mean, all these theories that they have about tantric sex and so on. It really serves

44
00:05:11,640 --> 00:05:17,280
no purpose for the meditator who simply has to understand moment-to-moment experience

45
00:05:17,280 --> 00:05:23,080
for what it is. So when desire arises to understand the desire for what it is, when energy

46
00:05:23,080 --> 00:05:30,760
arises or even physical energy arises, to understand the energy for what it is. When their

47
00:05:30,760 --> 00:05:36,280
arise is the vision of something attractive, to understand it for what it is and to see

48
00:05:36,280 --> 00:05:41,160
that the attraction is one thing, the object is another and so on. It only takes knowledge

49
00:05:41,160 --> 00:05:47,120
of the four foundations of mindfulness. Tantric sex is really way out there and I would

50
00:05:47,120 --> 00:05:53,040
only say it plays some small part or could play some part in the work of a body sata who

51
00:05:53,040 --> 00:06:18,720
has to, as I said, come to understand everything.

