1
00:00:00,000 --> 00:00:05,000
Why do Muslims seem to dislike us so much?

2
00:00:05,000 --> 00:00:13,000
I've tried to explain to a Muslim friend that we do not worship idols, but it seems to fall on deaf ears.

3
00:00:13,000 --> 00:00:18,000
It's actually the two issues here.

4
00:00:18,000 --> 00:00:25,000
I guess the implication that you're making is that Muslims dislike us because we worship idols.

5
00:00:25,000 --> 00:00:30,000
And Muslim people are very much against worshiping idols.

6
00:00:30,000 --> 00:00:32,000
Well, you know what?

7
00:00:32,000 --> 00:00:35,000
The problem is we do worship idols.

8
00:00:35,000 --> 00:00:38,000
Sad to say,

9
00:00:38,000 --> 00:00:41,000
but a lot of Buddhists do worship idols.

10
00:00:41,000 --> 00:00:45,000
They think of the Buddha as a God,

11
00:00:45,000 --> 00:00:50,000
and more especially when they don't think of the Buddha as a God.

12
00:00:50,000 --> 00:00:56,000
But there are people who wear Buddha images around their necks,

13
00:00:56,000 --> 00:00:59,000
thinking that it's going to protect them.

14
00:00:59,000 --> 00:01:01,000
And that's an understatement.

15
00:01:01,000 --> 00:01:02,000
Scott, you know this, I think.

16
00:01:02,000 --> 00:01:05,000
I think this is all familiar to you.

17
00:01:05,000 --> 00:01:19,000
That there are people out there who pay millions of dollars for a single small Buddha image.

18
00:01:19,000 --> 00:01:27,000
Yeah, I'm going to get to that and make actually this quite a generalization.

19
00:01:27,000 --> 00:01:30,000
But don't worry, I'm not going to let it.

20
00:01:30,000 --> 00:01:35,000
We will get there.

21
00:01:35,000 --> 00:01:42,000
So, I mean, there is a valid criticism there that this relying to people,

22
00:01:42,000 --> 00:01:48,000
they look down upon this in a good thing.

23
00:01:48,000 --> 00:01:54,000
They say, you don't wear a Buddha image around your neck.

24
00:01:54,000 --> 00:01:55,000
That's an image of the Buddha.

25
00:01:55,000 --> 00:01:57,000
And the Thai people are all like, yeah,

26
00:01:57,000 --> 00:02:00,000
let's buying and selling these Buddha images.

27
00:02:00,000 --> 00:02:03,000
And they have this way of feeling them.

28
00:02:03,000 --> 00:02:05,000
You touch the Buddha image with your thumb.

29
00:02:05,000 --> 00:02:08,000
There was this German man who claimed to be able to do it.

30
00:02:08,000 --> 00:02:13,000
So he was showing me touching the Buddha image.

31
00:02:13,000 --> 00:02:17,000
And he was like, this one is powerful.

32
00:02:17,000 --> 00:02:18,000
This one is not powerful.

33
00:02:18,000 --> 00:02:19,000
This one has this kind of power.

34
00:02:19,000 --> 00:02:20,000
This one is that kind of power.

35
00:02:20,000 --> 00:02:23,000
And so Thai people have this word kang kang.

36
00:02:23,000 --> 00:02:29,000
I think it is kang, which means powerful kang.

37
00:02:29,000 --> 00:02:33,000
And so they know which one is which one is powerful and which one is not.

38
00:02:33,000 --> 00:02:35,000
Nowadays, I don't even know if it's still a fad,

39
00:02:35,000 --> 00:02:41,000
but when I was in Thailand, there was this disgusting fad of...

40
00:02:41,000 --> 00:02:48,000
And it wasn't even Buddha images anymore.

41
00:02:48,000 --> 00:02:49,000
It was this angel called Jetukam.

42
00:02:49,000 --> 00:02:54,000
Jetukam, which is a fictitious being that apparently that is supposed to...

43
00:02:54,000 --> 00:02:55,000
It was just made up.

44
00:02:55,000 --> 00:02:57,000
There's even a story about where it was made up.

45
00:02:57,000 --> 00:03:04,000
It was made up based on a Hindu based on not even exactly,

46
00:03:04,000 --> 00:03:09,000
but based on a Hindu deity.

47
00:03:09,000 --> 00:03:17,000
And that's supposed to guard this JT, this Pagoda in Thailand.

48
00:03:17,000 --> 00:03:23,000
And it became this huge fad and people got rich off of these things

49
00:03:23,000 --> 00:03:30,000
because people can be so dumb and would buy them.

50
00:03:30,000 --> 00:03:35,000
So even monasteries became quite wealthy by having special edition.

51
00:03:35,000 --> 00:03:39,000
It was like those pong, you remember pong?

52
00:03:39,000 --> 00:03:46,000
Those little bottle caps collecting pong?

53
00:03:46,000 --> 00:03:49,000
I think it was called pong.

54
00:03:49,000 --> 00:03:52,000
You know, it became the vegan collector's items,

55
00:03:52,000 --> 00:03:54,000
and they were these round dates.

56
00:03:54,000 --> 00:03:56,000
They looked like pong, actually.

57
00:03:56,000 --> 00:03:58,000
And you would collect them.

58
00:03:58,000 --> 00:04:02,000
And so they had special edition and all of this garbage.

59
00:04:02,000 --> 00:04:05,000
People say the same about Buddha images.

60
00:04:05,000 --> 00:04:12,000
They have this huge ceremony to bless and to christen a Buddha image

61
00:04:12,000 --> 00:04:14,000
and make it powerful.

62
00:04:14,000 --> 00:04:20,000
They put all sorts of spells over it and all sorts of stuff.

63
00:04:20,000 --> 00:04:26,000
Why I'm even talking about this is because there's really a point that has to be made.

64
00:04:26,000 --> 00:04:29,000
Yeah, I'm really kind of beating around the bush.

65
00:04:29,000 --> 00:04:34,000
But there's a point that has to be made here is that

66
00:04:34,000 --> 00:04:37,000
the...

67
00:04:37,000 --> 00:04:38,000
Well, they have...

68
00:04:38,000 --> 00:04:44,000
There's really a point there because the Buddha never taught us to make images of anything.

69
00:04:44,000 --> 00:04:46,000
You know, he wasn't critical.

70
00:04:46,000 --> 00:04:49,000
It was like, yeah, big deal, make an image, don't make an image.

71
00:04:49,000 --> 00:04:53,000
The question ever came up.

72
00:04:53,000 --> 00:04:57,000
There's a famous story and one that we always tell is that

73
00:04:57,000 --> 00:05:02,000
the first Buddha images were Greek.

74
00:05:02,000 --> 00:05:12,000
The records that we have of Buddhist India are that they never made Buddha images.

75
00:05:12,000 --> 00:05:16,000
They would never put the Buddha, and in fact they avoided it.

76
00:05:16,000 --> 00:05:20,000
So when they wanted to tell a story and they would inscribe it on rocks,

77
00:05:20,000 --> 00:05:21,000
the kings would do this.

78
00:05:21,000 --> 00:05:24,000
Instead of putting the Buddha, they would put a wheel of the dhamma

79
00:05:24,000 --> 00:05:30,000
or they would put a Bodhi leaf, or they would put just an empty.

80
00:05:30,000 --> 00:05:34,000
You know, when they want to show the Buddha on the Bodhisattva,

81
00:05:34,000 --> 00:05:38,000
writing this horse, they would put just the empty horse

82
00:05:38,000 --> 00:05:40,000
because they would never...

83
00:05:40,000 --> 00:05:44,000
It was never done.

84
00:05:44,000 --> 00:05:50,000
And so the point, the point I'm trying to make is that...

85
00:05:50,000 --> 00:05:58,000
We really have a problem there, and that we really are worshiping idols.

86
00:05:58,000 --> 00:06:01,000
And that should really stop.

87
00:06:01,000 --> 00:06:06,000
And of course, the defense that's always used that I haven't used

88
00:06:06,000 --> 00:06:13,000
is that we use the Buddha image in order to recollect the Buddha.

89
00:06:13,000 --> 00:06:22,000
Which, I think, can be a valid excuse, but it sets you up for a lot of danger,

90
00:06:22,000 --> 00:06:27,000
especially when dealing with people who believe

91
00:06:27,000 --> 00:06:33,000
grave and images are seen and punishable by death or so.

92
00:06:35,000 --> 00:06:38,000
So, okay, so let's get back, try to get back...

93
00:06:38,000 --> 00:06:48,000
relate this back to the question, because not all Muslims just like us.

94
00:06:50,000 --> 00:06:59,000
But it is true, I think, that there is definitely a bad feeling

95
00:06:59,000 --> 00:07:09,000
between what is essentially an atheistic religion

96
00:07:09,000 --> 00:07:16,000
and the atheistic religions.

97
00:07:16,000 --> 00:07:19,000
I mean, Hinduism is, I guess, an exception because in Hinduism,

98
00:07:19,000 --> 00:07:22,000
they don't exactly worship the gods.

99
00:07:22,000 --> 00:07:28,000
They interact with them, and there's general belief that one can become God.

100
00:07:28,000 --> 00:07:33,000
Judaism also doesn't have so much trouble, because in Judaism,

101
00:07:33,000 --> 00:07:37,000
they can argue with God, and God isn't...

102
00:07:37,000 --> 00:07:41,000
God is a very much part of the universe,

103
00:07:41,000 --> 00:07:48,000
but he doesn't play the central role in what it means to be Jewish.

104
00:07:48,000 --> 00:07:51,000
So, Jewish people can often come in practice meditation.

105
00:07:51,000 --> 00:07:54,000
But I think there's no beating around the bush

106
00:07:54,000 --> 00:07:57,000
that Muslims, people who are practicing Muslims,

107
00:07:57,000 --> 00:08:03,000
who have a problem with just as Christians do

108
00:08:03,000 --> 00:08:07,000
with people who don't believe in God.

109
00:08:07,000 --> 00:08:16,000
And that's aggravated when we take some graven image or idol, as you say,

110
00:08:16,000 --> 00:08:25,000
an idol as a substitute.

111
00:08:25,000 --> 00:08:27,000
And then that's the ultimate blasphemy.

112
00:08:27,000 --> 00:08:30,000
It's like putting a piece of rock on the same level

113
00:08:30,000 --> 00:08:35,000
as the most important thing in the universe, which is their God.

114
00:08:35,000 --> 00:08:37,000
So, regardless...

115
00:08:37,000 --> 00:08:40,000
I mean, obviously it's a silly thing to do for us to be

116
00:08:40,000 --> 00:08:43,000
paying so much attention to these images,

117
00:08:43,000 --> 00:08:47,000
but there's a very valid point there that it's going to get us

118
00:08:47,000 --> 00:08:52,000
into a lot of heat with those people who feel very strongly with God.

119
00:08:52,000 --> 00:08:57,000
I think, and why this is actually an important point,

120
00:08:57,000 --> 00:09:00,000
is because this is part of what helped wipe out Buddhism in India.

121
00:09:00,000 --> 00:09:02,000
When the Muslims came to Indian,

122
00:09:02,000 --> 00:09:04,000
they weren't just because they were Muslims,

123
00:09:04,000 --> 00:09:09,000
but because they were militaristic sort of people.

124
00:09:09,000 --> 00:09:12,000
But Muslim Islam told them that this was wrong,

125
00:09:12,000 --> 00:09:16,000
and so it was one of the first things to go with these idol worshipers,

126
00:09:16,000 --> 00:09:20,000
which were the Buddhists who had their Buddha images.

127
00:09:20,000 --> 00:09:25,000
Hinduism was much better, much more like a chameleon,

128
00:09:25,000 --> 00:09:28,000
much better to fit easier to fit in,

129
00:09:28,000 --> 00:09:30,000
and much harder to wipe out.

130
00:09:30,000 --> 00:09:33,000
But Buddhism is pretty easy to wipe out,

131
00:09:33,000 --> 00:09:39,000
especially when you have these Buddha images and temples and so on.

132
00:09:39,000 --> 00:09:44,000
If monks were just people wearing rags living in the forest,

133
00:09:44,000 --> 00:09:47,000
I think it'd be a lot harder to wipe them out.

134
00:09:47,000 --> 00:09:50,000
But when they're sitting ducks,

135
00:09:50,000 --> 00:09:59,000
getting fat off of donations living in opulent gold and palaces,

136
00:09:59,000 --> 00:10:04,000
not a hard target to hit.

137
00:10:04,000 --> 00:10:12,000
And I think that can be extrapolated to a more modern example.

138
00:10:12,000 --> 00:10:19,000
Buddhism is not making much headway on a monastic level

139
00:10:19,000 --> 00:10:22,000
into the rest of the world,

140
00:10:22,000 --> 00:10:27,000
because Buddhists, when they build a Buddhist monastery,

141
00:10:27,000 --> 00:10:32,000
first of all they call it a temple and they have their priests.

142
00:10:32,000 --> 00:10:36,000
And second of all, the way they go about it,

143
00:10:36,000 --> 00:10:40,000
for instance time on the stairs, they build it out of gold,

144
00:10:40,000 --> 00:10:45,000
as much gold as possible, and rich, beautiful carpets,

145
00:10:45,000 --> 00:10:50,000
and huge Buddha images and lots of ceremonies and so on.

146
00:10:50,000 --> 00:11:00,000
It's an affront really to people's sensibilities.

147
00:11:00,000 --> 00:11:05,000
People go there and they're certainly not looking for that.

148
00:11:05,000 --> 00:11:09,000
It's making it very difficult for us to bring Buddhism to people,

149
00:11:09,000 --> 00:11:11,000
people by...

150
00:11:11,000 --> 00:11:13,000
It's not just overkill.

151
00:11:13,000 --> 00:11:17,000
It's total misrepresentation of the Buddha's teaching

152
00:11:17,000 --> 00:11:20,000
because gold has no place in the monastery.

153
00:11:20,000 --> 00:11:22,000
A monk can't even touch gold.

154
00:11:22,000 --> 00:11:26,000
Touching, touching gold is an offense.

155
00:11:26,000 --> 00:11:27,000
Touching jewels.

156
00:11:27,000 --> 00:11:32,000
Touching anything, any precious material like that is an offense.

157
00:11:32,000 --> 00:11:42,000
So it's a real danger.

158
00:11:42,000 --> 00:11:47,000
And I think the Buddha images are a part of that.

159
00:11:47,000 --> 00:11:51,000
We don't need Buddha images to spread Buddhism.

160
00:11:51,000 --> 00:11:55,000
It's the Dhamma, which was spread Buddhism.

161
00:11:55,000 --> 00:12:01,000
And yes, you have the excuse that it helps us to remember the Buddha

162
00:12:01,000 --> 00:12:03,000
thinking about him.

163
00:12:03,000 --> 00:12:07,000
So I'm not saying that a Buddha image is totally negative,

164
00:12:07,000 --> 00:12:12,000
but our obsession with them and our placing them.

165
00:12:12,000 --> 00:12:19,000
Because if in that case, why not have a Buddha image placed somewhere

166
00:12:19,000 --> 00:12:23,000
in its own place instead of putting it up in front of an altar

167
00:12:23,000 --> 00:12:31,000
for you to worship and give donations to and so on?

168
00:12:31,000 --> 00:12:37,000
You have it up as a reminder or something that you can go and look at.

169
00:12:37,000 --> 00:12:39,000
Whatever.

170
00:12:39,000 --> 00:12:43,000
It's certainly not the most important thing.

171
00:12:43,000 --> 00:12:47,000
The Buddha's root bakaya was not his most important kaya,

172
00:12:47,000 --> 00:12:52,000
his most important body.

173
00:12:52,000 --> 00:12:54,000
I think that I had something else to say.

174
00:12:54,000 --> 00:13:02,000
But I think that's saying quite a bit already is that the Buddha,

175
00:13:02,000 --> 00:13:06,000
there's the danger in the Buddha image.

176
00:13:06,000 --> 00:13:10,000
I mean, it's really in a front to people who believe other things.

177
00:13:10,000 --> 00:13:16,000
And that's really the point is we don't have to be so obvious about who we are.

178
00:13:16,000 --> 00:13:24,000
As I said, a monk living in the forest really is not a threat to anyone.

179
00:13:24,000 --> 00:13:28,000
They still might go out and kill them because, oh, these people don't believe in God.

180
00:13:28,000 --> 00:13:38,000
But there's not so much of a problem there.

181
00:13:38,000 --> 00:13:41,000
The other part of the answer, I guess that I should say,

182
00:13:41,000 --> 00:13:43,000
is it's because these people have wrong views.

183
00:13:43,000 --> 00:13:50,000
And when people have wrong views, they tend to be what is a judgmental.

184
00:13:50,000 --> 00:13:52,000
But here I am judging them.

185
00:13:52,000 --> 00:13:57,000
But as people hold on to views, they believe this is right and nothing else is right.

186
00:13:57,000 --> 00:14:03,000
And so they're apt to criticize anyone who just disagrees.

187
00:14:03,000 --> 00:14:04,000
They're not just criticized.

188
00:14:04,000 --> 00:14:06,000
They might even persecute.

189
00:14:06,000 --> 00:14:08,000
And it's not just the Muslims.

190
00:14:08,000 --> 00:14:10,000
The Christians were horrible about it.

191
00:14:10,000 --> 00:14:14,000
The Spanish Inquisition, the Crusades.

192
00:14:14,000 --> 00:14:16,000
It has to do with belief.

193
00:14:16,000 --> 00:14:23,000
The stronger your belief, the less tolerant you are of people who disagree.

194
00:14:23,000 --> 00:14:27,000
So the word atheist is just a shock to people.

195
00:14:27,000 --> 00:14:43,000
It's a four-letter word.

