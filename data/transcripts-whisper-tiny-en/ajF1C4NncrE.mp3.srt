1
00:00:00,000 --> 00:00:08,280
How could I be mindful when I come across some sangas who do not present or follow the

2
00:00:08,280 --> 00:00:15,960
ways and rules of the Buddha Dhamma, because the sanga are what we learn from and look

3
00:00:15,960 --> 00:00:23,240
up to them in this Buddha sasana time?

4
00:00:23,240 --> 00:00:26,720
How could I be mindful when I come across them?

5
00:00:26,720 --> 00:00:32,280
Well, the first important thing is to be clear that such sangas exist.

6
00:00:32,280 --> 00:00:40,960
Too often we get shocking letters from people saying, I can't believe it.

7
00:00:40,960 --> 00:00:47,720
I met a monk who does X, Y, or Z. How could it be possible?

8
00:00:47,720 --> 00:01:02,360
It's important to understand that these kind of hypocrites, I suppose you would say,

9
00:01:02,360 --> 00:01:08,840
are not to be too harsh, but people who are unable to follow the teachings that they have

10
00:01:08,840 --> 00:01:16,600
taken on a bit enough more than they could chew maybe as a kind of a nice way of putting

11
00:01:16,600 --> 00:01:19,880
it.

12
00:01:19,880 --> 00:01:25,880
That they exist, and that will help with your mindfulness if you are prepared for it.

13
00:01:25,880 --> 00:01:29,200
But on the other hand, not to be too skeptical.

14
00:01:29,200 --> 00:01:34,080
The other thing that we often get is we hear lay people criticizing monks for doing silly

15
00:01:34,080 --> 00:01:38,640
things, like lay people who criticize monks for eating meat.

16
00:01:38,640 --> 00:01:39,640
Those monks eat meat.

17
00:01:39,640 --> 00:01:44,880
How could they possibly, I mean, as far as we know, the Buddha Himself ate meat, we're

18
00:01:44,880 --> 00:01:52,520
not, there's no clear case, but from the teachings that we have in our tradition, it's

19
00:01:52,520 --> 00:01:58,640
pretty clear that the Buddha was not upset at all if a monk ate meat unless the Buddha

20
00:01:58,640 --> 00:02:04,800
wasn't upset, but the Buddha wasn't against monks eating meat, provided that it had

21
00:02:04,800 --> 00:02:12,120
been not killed for them or provided that they didn't have any idea that had been killed

22
00:02:12,120 --> 00:02:13,840
for their purpose.

23
00:02:13,840 --> 00:02:18,480
For example, and you hear this in many different levels, monks who are really doing good

24
00:02:18,480 --> 00:02:24,800
things and may just not be totally dedicated to meditation, maybe they practice some meditation

25
00:02:24,800 --> 00:02:29,840
or maybe they get too busy with other things to practice enough meditation and still people

26
00:02:29,840 --> 00:02:32,920
will give them a hard time.

27
00:02:32,920 --> 00:02:37,120
His man who came to visit me recently, he said, it's really a shame to hear people criticize

28
00:02:37,120 --> 00:02:41,320
because he said, you know, you have to ask them, what could you do it?

29
00:02:41,320 --> 00:02:42,320
Could you put on those wrongs?

30
00:02:42,320 --> 00:02:45,320
It's easy for you to say, oh, this monk is bad, and that monk is bad.

31
00:02:45,320 --> 00:02:49,040
Well, why aren't you among showing them how to be a good monk, right?

32
00:02:49,040 --> 00:02:54,880
I mean, this is one thing that you have to be careful about as well, is over criticism

33
00:02:54,880 --> 00:03:00,720
because when you say that they are not following the ways and rules, well, to what extent,

34
00:03:00,720 --> 00:03:06,840
I mean, for the most part, even the really, not the really bad ones, but even some of

35
00:03:06,840 --> 00:03:15,160
the monks who I wouldn't find impressive or that I might criticize or say things about

36
00:03:15,160 --> 00:03:20,000
in private, when you look at it, they're actually doing good things.

37
00:03:20,000 --> 00:03:22,600
They actually have many good qualities to them.

38
00:03:22,600 --> 00:03:27,240
So I think that's important, and the Buddha was quite clear about this as well.

39
00:03:27,240 --> 00:03:34,280
When you come upon someone who's verbal and physical acts are not very pure, but they

40
00:03:34,280 --> 00:03:39,120
have mental good acts, so their mind is in the right place, so they have good intentions,

41
00:03:39,120 --> 00:03:43,160
they just get everything wrong, then you think, well, you know, at least they're good

42
00:03:43,160 --> 00:03:46,800
and mental in the mind, or if they have good speech but not good actually, you think,

43
00:03:46,800 --> 00:03:51,400
well, at least their speech is good, or if their action is good, or if some of their actions

44
00:03:51,400 --> 00:03:57,200
are good, then you can look at that as well and try to be impartial about it, because

45
00:03:57,200 --> 00:04:02,000
that's a very important part of what mindfulness is, being impartial.

46
00:04:02,000 --> 00:04:06,240
So the other thing is understanding that beings go according to their karma, so if these

47
00:04:06,240 --> 00:04:12,840
beings are acting in a way that is actually unwholesome, especially using the Buddha's

48
00:04:12,840 --> 00:04:19,120
teaching as a kind of a cover for that unwholesome behavior, then it's quite sad, actually.

49
00:04:19,120 --> 00:04:29,360
It's quite something that is quite shocking when you think about how they are going to

50
00:04:29,360 --> 00:04:35,240
have to suffer, so you can certainly get rid of your aversion or your anger or your frustration

51
00:04:35,240 --> 00:04:42,600
at them or your criticism towards them, when you think of how pitiful they are and how

52
00:04:42,600 --> 00:04:48,360
pitiable they are, how worthy of your pity they are, because, you know, like when you

53
00:04:48,360 --> 00:04:54,680
would have said, like, for example, when you see a man walking towards a pit full of

54
00:04:54,680 --> 00:05:01,680
burning hot embers, you think, wow, if that man continues on that path, he's going to fall

55
00:05:01,680 --> 00:05:05,160
into that pit of embers and suffer horribly.

56
00:05:05,160 --> 00:05:09,720
So what would you think, would you be angry at that man or would you think, would you

57
00:05:09,720 --> 00:05:15,160
start criticizing and look at that, why is that dumb, you know, that terrible person?

58
00:05:15,160 --> 00:05:17,480
What are they doing walking towards that pit?

59
00:05:17,480 --> 00:05:22,680
That's really the case here, if people are really immoral and doing moral deeds, then

60
00:05:22,680 --> 00:05:28,440
you should really look upon them in this way, and this is something else that helps.

61
00:05:28,440 --> 00:05:33,360
Ultimately, it just, the only real answer to your question is to be mindful, learn how

62
00:05:33,360 --> 00:05:38,960
to be mindful, learn what it means to be mindful, learn to see that the sun guy is not

63
00:05:38,960 --> 00:05:44,280
real, the sun guy is a concept, a monk is a concept, person is a concept, all of these

64
00:05:44,280 --> 00:05:50,000
things are not real, what is real is your experience moment to moment to moment, and as

65
00:05:50,000 --> 00:05:54,320
with people dying, as with everyone, that'll be the best interaction that you have with

66
00:05:54,320 --> 00:05:55,680
these people.

67
00:05:55,680 --> 00:06:00,880
You can't change them and you can't force them to change, but you can purify your own

68
00:06:00,880 --> 00:06:06,440
interactions with them, because the purer you are, the more closer you'll be to other

69
00:06:06,440 --> 00:06:13,880
pure beings, and the more able you will be to supporting good things and to supporting

70
00:06:13,880 --> 00:06:19,800
the sun guy and to supporting beneficial organizations and so on.

71
00:06:19,800 --> 00:06:23,800
The better how you will be in this world, this is why we're out here encouraging people

72
00:06:23,800 --> 00:06:28,760
to be good, because we know it depends on the people's minds, you have to develop yourself.

73
00:06:28,760 --> 00:06:43,480
We have to develop ourselves before we can help other people.

74
00:06:43,480 --> 00:06:50,240
Is it possible to say something or don't they hear?

75
00:06:50,240 --> 00:07:01,480
The sun guy is made of individuals and all these individuals do probably the best they

76
00:07:01,480 --> 00:07:15,200
can to follow the rules, but you can't do it perfect for everybody, no matter what you

77
00:07:15,200 --> 00:07:29,000
do, you will always find somebody who finds fault and look at me, for example, today

78
00:07:29,000 --> 00:07:41,560
I'm keeping the vineyard, but many people will find fault in it, because I'm not wearing

79
00:07:41,560 --> 00:07:49,320
my robes as I should wear my robes as a bikuni, but many people who see the videos now

80
00:07:49,320 --> 00:07:59,240
out in the world will say, how can she sit there without her shirt on, but in fact in the

81
00:07:59,240 --> 00:08:09,120
Buddha's time the bikinis weren't wearing blouses, so in the villa, it is not stated that the

82
00:08:09,120 --> 00:08:20,200
bikuni should wear blouses, we should wear the same kind of robes as a monk with two additional

83
00:08:20,200 --> 00:08:33,640
parts, which of which none is a blouse, so I'm doing good in the villa sense, but for

84
00:08:33,640 --> 00:08:43,360
some I might not do good, because I might hurt their feelings or something, so you can't

85
00:08:43,360 --> 00:09:10,920
do it right for everybody.

