1
00:00:00,000 --> 00:00:11,640
Welcome, today we will be studying the second half of the Mahasa-Rofamasutamujimnika

2
00:00:11,640 --> 00:00:28,440
in the 29th, as usual, the format is to chant the chant the pali, and then the

3
00:00:28,440 --> 00:00:35,440
read and discuss the English translation. So we get started right now with the pali.

4
00:01:58,440 --> 00:02:07,640
So we get started with the pali, and then the minadhyamasutamasutamasutamasutamasutamasutamasutamasutamasutamasutamasutamasutamamutamasutamamutamamutamamutamah

5
00:02:28,440 --> 00:02:40,040
So da ya see the sampadaya, tamano, tinachu, pariborna, sankapu.

6
00:02:40,040 --> 00:02:52,400
So da ya see the sampadaya, tamanu, kangzi, tinaparangwambiti, so da ya see the sampadaya,

7
00:02:52,400 --> 00:03:18,360
tamanu, tamanu, sampadaya, tamanu, tinachu, pariborna, sankapu, so da ya see the sampadaya, tamanu,

8
00:03:18,360 --> 00:03:29,280
tamanu, kangzi, tinaparangwambiti, so da ya see the sampadaya, tamanu, japina, pariborna,

9
00:03:29,280 --> 00:03:46,400
tamanu, tamanu, tamanu, tamanu, tamanu, tamanu, tamanu, pajiborna, sankapu,

10
00:04:16,400 --> 00:04:19,400
No, no, no, come, we have a tea.

11
00:04:19,400 --> 00:04:22,800
Say it happy, big, away, poor resource.

12
00:04:22,800 --> 00:04:26,400
Aratikosa, rakavisi.

13
00:04:26,400 --> 00:04:32,680
Sarapari is an anjarma, no, mahatul kasa.

14
00:04:32,680 --> 00:04:40,080
Tirtatosa, ravatul, whatikami was aran, big, unjitwa.

15
00:04:40,080 --> 00:04:45,480
Aratya pakami, yasaranti, manyama, no.

16
00:04:45,480 --> 00:04:54,800
Tameyana, jakuma, poor resource, diswa, yasaranti, no, what I am a one, poor resource.

17
00:04:54,800 --> 00:05:07,760
Nya sisaram, nya sipaguna, nya sita, chana, nya sipapari, kam, nya sisakapalasam.

18
00:05:07,760 --> 00:05:20,240
Aratikosa, rakavisi, sarapari is an anjarma, no, mahatul, rukasa.

19
00:05:20,240 --> 00:05:27,040
Tirtatosa, ravatul, whatikami was aran, big, unjitwa.

20
00:05:27,040 --> 00:05:32,320
Aratya pakantosa, ran, demonyama, no.

21
00:05:32,320 --> 00:05:46,960
Nya sisaram, nya sasaram, nya sasadhanana, nya vi sati, ti, yi, mami, wo kobiko, vi, ti, kachukulapoto, sadha,

22
00:05:46,960 --> 00:05:53,120
agarasmana, gari, young pam, vatito, ho, ti.

23
00:05:53,120 --> 00:06:01,960
O, ti, no, mita, ti, ajara, yam, aran, nya sukai, paree, vi, dukay,

24
00:06:01,960 --> 00:06:05,880
dumana, sai, yi, yi, yi, sai.

25
00:06:05,880 --> 00:06:12,480
Tukote, noyo, dukapareto, hapay, wa nam, marimasa.

26
00:06:12,480 --> 00:06:19,600
K, wa lasa, dukakantasa, ntakiri, apanya, yitati.

27
00:06:19,600 --> 00:06:28,320
So, yi, wa mam, shitos, samano, hapasa, kara, silo, kana, vinibati, ti.

28
00:06:28,320 --> 00:06:37,320
So, tingalabasakarasilokain, nya hatamano, ho, tingaparepun, nya sankapu.

29
00:06:37,320 --> 00:06:46,320
So, tingalabasakarasilokain, nya hatan, kamsetinaparangwametin.

30
00:06:46,320 --> 00:06:54,320
So, ting, alabas, a kara sea looking and a man, chetinapam and chetinapam, a dangapam,

31
00:06:54,320 --> 00:07:02,280
a dangapam, chetinapam, a tose, a man, o see the sampadana, a ditinapam, a titinapam, a

32
00:07:02,280 --> 00:07:10,640
taiya, see the sampadana, a hata, mano, o tinoh, jakoparipun, sankapu.

33
00:07:10,640 --> 00:07:39,120
So, taiya, see the sampadana, a tose, tinapam, a tose, a tose, a tose, a tose, a tose, a tose, a tose,

34
00:07:39,120 --> 00:07:41,920
Mārāja kupāry-būṇa sankā bū.

35
00:07:41,920 --> 00:07:49,720
So dāya samadhi sampādāya nātāna kānṣetī nāpaṁṁvietī.

36
00:07:49,720 --> 00:07:57,400
So dāya samadhi sampādāya nāma jatī nāpaṁma jatī nāpaṁma jatī nāpaṁma jatī nāpaṁma

37
00:07:57,400 --> 00:08:03,720
nāpaṁma jatī nāpaṁma jatī nāpaṁma nāpaṁma jatī.

38
00:09:33,720 --> 00:09:40,720
Bhajita Samano, Babasaka, Zilogana, Bini, Prateti.

39
00:09:40,720 --> 00:09:49,520
So Tehana, Babasaka, Rasito, Kainan, Atamano, Tehana, Paripun, Nasankapo.

40
00:09:49,520 --> 00:09:58,320
So Tehana, Babasaka, Rasilogainan, Atamana, Kamseti, Naparang, Vameti.

41
00:09:58,320 --> 00:10:07,600
So Tehana, Babasaka, Zilogainan, Namseti, Naparman, Jati, Naparman, Jati, Naparman, Jati,

42
00:10:07,600 --> 00:10:13,520
Naparman, Jati, Naparman, Jati, Naparman, Nasamano, Sita Sampadana, Raseti.

43
00:10:13,520 --> 00:10:21,720
So Daya, Sita Sampadaya, Atamano, Tino, Jakoparipun, Nasankapo.

44
00:10:21,720 --> 00:10:37,960
So Tehana, Tino, Parapun, Jati, Naparman, Jati, Naparman, Naparman, Jati,

45
00:10:37,960 --> 00:10:52,400
Naparman, Tosa, Mano, Sampadana, Raseti, So Tehana, Risampadaya, Atamano, Tino, Tarko, Paripun, Nasankapo.

46
00:10:52,400 --> 00:11:00,360
So Tehana, Sita, Sampadaya, Atamano, Naseti, Naparang, Vati.

47
00:11:00,360 --> 00:11:06,680
So Tehana, Sita, Naparman, Jati, Naparman, Jati.

48
00:11:06,680 --> 00:11:23,000
Naparman, Raseti, Atamano, Nana, Raseti, So Tehana, Nana, Nasamano, Tino, Jakoparipun, Nasankapo.

49
00:11:23,000 --> 00:11:37,040
So Tehana, Nasani, Nana, Tano, Kansi, Tehana, Parapun, Vati, So Tehana, Nasani, Nana, Jati, Naparman, Jati, Naparman, Jati, Naparman,

50
00:11:37,040 --> 00:12:00,520
Vati, Naparang, Jati, Pamato, Samana, Raseti, Naparang, Naparang, Naparang, Naparang, Naparang, Naparang,

51
00:12:00,520 --> 00:12:13,600
Sayyatta, Pifika, Vibhulisosa, Rati, Khosa, Ragavisi, Sarapari, Sanan, Jara, Manom, Arukas,

52
00:12:13,600 --> 00:12:20,960
Dita, Tosa, Ravatosa, Rani, Rati, Vati, Rara, Yatta, Kami, Yatta,

53
00:12:20,960 --> 00:12:32,920
Saranti, Jana, Manom, Tami, Nang, Japoparipo, Risodis, Vai, Vamwadeya, Anya, Sirota, Yambawa,

54
00:12:32,920 --> 00:12:42,040
Burisosa, Ravan, Nya, Sipa, Gung, Anya, Sita, Javan, Nya, Sipa, Parthika, Nanya, Sisaka,

55
00:12:42,040 --> 00:12:49,600
Palasam, Tata, Aya, Nambawa, Ampurisosa, Rati, Khosa, Ragavisi,

56
00:12:49,600 --> 00:13:01,560
Sarapari, Sanan, Jara, Manom, Arukas, Dita, Tosa, Ravatosa, Rani, Vati, Vai, Arukas,

57
00:13:01,560 --> 00:13:23,680
Vanti, Jana, Manom, Sari, Nasa, Rakarini, Nasa, Atam, Manom, Vai, Sati, Tati, Rami, Vakobita,

58
00:13:23,680 --> 00:13:37,160
Sanda, Agaras, Mahana, Gari, Ampur, Vati, Dita, Nami, Jara, Yama, Rani, Nasa, Kari, Vati,

59
00:13:37,160 --> 00:13:54,120
Vati, Dukas, Nasa, Nasa, Nasa, Nasa, Nasa, Nasa, Nasa, Nasa, Rati,

60
00:13:54,120 --> 00:14:12,920
Vati, Jana, Rati, Rati, Rati, Rati, Rati, Rati, Nasa, Kari, Rati, Rati, Nasa,

61
00:15:42,920 --> 00:16:12,920
So darlllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllolllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll littlellllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll

62
00:16:42,920 --> 00:17:11,920
Okay, so last week we did the first three sections of this attempt.

63
00:17:11,920 --> 00:17:15,920
So it has made up of five sections.

64
00:17:15,920 --> 00:17:21,920
The five search of people who set out looking for heart wood.

65
00:17:21,920 --> 00:17:25,920
The heart wood in the Buddhist asana and the Buddhist religion.

66
00:17:25,920 --> 00:17:30,920
They set out looking for something of benefit, of true benefit.

67
00:17:30,920 --> 00:17:37,920
And some people come away with just the branches in the leaves.

68
00:17:37,920 --> 00:17:41,920
Some come away with the outer bark, some come away with the inner bark.

69
00:17:41,920 --> 00:17:47,920
And today we're going to look at those who come away with the soft wood and those who come away with the heart wood.

70
00:17:47,920 --> 00:17:59,920
And as we learned last week, the leaves and twings, those are the game that one gets.

71
00:17:59,920 --> 00:18:11,920
The material game and the outer bark is morality, the inner bark is concentration that comes from the practice.

72
00:18:11,920 --> 00:18:22,920
So today we'll find out what the other two are.

73
00:18:22,920 --> 00:18:32,920
Here, because some clansmen goes forth out of faith from the home life in the homelessness, considering I am a victim of birth, aging, and death of sorrow,

74
00:18:32,920 --> 00:18:39,920
lamentation, pain, grief, and despair, I am a victim of suffering, a prey to suffering.

75
00:18:39,920 --> 00:18:42,920
Surely an ending of this whole mass of suffering can be known.

76
00:18:42,920 --> 00:18:47,920
When he has gone forth thus, he acquires gain, honor, and renown.

77
00:18:47,920 --> 00:18:52,920
He has not pleased with that gain, honor, and renown, and his intention is not fulfilled.

78
00:18:52,920 --> 00:18:56,920
Being diligent, he achieves the attainment of virtue.

79
00:18:56,920 --> 00:19:00,920
He is pleased with that attainment of virtue, but his intention is not fulfilled.

80
00:19:00,920 --> 00:19:03,920
Being diligent, he achieves the attainment of concentration.

81
00:19:03,920 --> 00:19:08,920
He is pleased with that attainment of concentration but his intention is not fulfilled.

82
00:19:08,920 --> 00:19:12,920
He does not count of it, lot himself and disparage others.

83
00:19:12,920 --> 00:19:16,920
He does not become intoxicated with that attainment of concentration.

84
00:19:16,920 --> 00:19:21,000
He does not grow negligent and fallen in negligence.

85
00:19:21,000 --> 00:19:24,480
Being diligent, he achieves knowledge and vision.

86
00:19:24,480 --> 00:19:28,760
He is pleased with that knowledge and vision and his intention is fulfilled.

87
00:19:28,760 --> 00:19:32,320
On account of it, he lauds himself and disparages others thus.

88
00:19:32,320 --> 00:19:37,280
I live knowing and seeing what these other bikhus live, unknowing and unseen.

89
00:19:37,280 --> 00:19:42,600
He becomes intoxicated with that knowledge and vision, grows negligent, falls into negligence,

90
00:19:42,600 --> 00:19:44,600
and being negligent, he lives in suffering.

91
00:19:44,600 --> 00:19:51,600
One thing I didn't mention yesterday, because it wasn't clear to me, that's clear now,

92
00:19:51,600 --> 00:19:55,600
is there's a distinction between you.

93
00:19:55,600 --> 00:20:06,600
So you notice that the correct response or a reaction to gain honour and honour is to not be pleased,

94
00:20:06,600 --> 00:20:10,600
not at the Manoho team.

95
00:20:10,600 --> 00:20:14,600
And to not get his intention is not fulfilled.

96
00:20:14,600 --> 00:20:25,600
The actual word here is Baripuna Sankapu, which means he doesn't give rise to the thought of fulfillment.

97
00:20:25,600 --> 00:20:28,600
Sankapu is like a thought.

98
00:20:28,600 --> 00:20:35,600
So he doesn't give rise to thoughts of fulfillment, he doesn't feel fulfilled.

99
00:20:35,600 --> 00:20:40,600
But the other ones, it changes, you see.

100
00:20:40,600 --> 00:20:45,600
At the Manoho team, he is with virtue, being pleased isn't a problem.

101
00:20:45,600 --> 00:20:50,600
It's okay to feel good about it, to feel to gain confidence,

102
00:20:50,600 --> 00:20:52,600
and in some sense feel proud of it.

103
00:20:52,600 --> 00:20:57,600
No, no, the word pride, of course, is generally used in a negative sense, especially in Buddhism.

104
00:20:57,600 --> 00:21:04,600
There's kind of a, it's just words, and there's a kind of a sense of feeling proud as a confidence booster.

105
00:21:04,600 --> 00:21:05,600
You understand?

106
00:21:05,600 --> 00:21:09,600
Not proud in terms of lauding yourself and putting other people down,

107
00:21:09,600 --> 00:21:17,600
which of course is the big problem, you're right, where you laud yourself in a spare jet.

108
00:21:17,600 --> 00:21:23,600
But there's a sense of being, and I guess, I suppose proud is obviously not the right word,

109
00:21:23,600 --> 00:21:31,600
but it's a kind of a feeling of confidence or good feeling about it, feeling good about it.

110
00:21:31,600 --> 00:21:36,600
I wouldn't even use the word, please, because of course that has negative connotations in the sense of liking it.

111
00:21:36,600 --> 00:21:48,600
But you might sort of neutral sense, say feels good about it, or is, is, is perked about, perked out by it.

112
00:21:48,600 --> 00:21:52,600
Is encouraged by it.

113
00:21:52,600 --> 00:21:54,600
Anyway, there's a difference here.

114
00:21:54,600 --> 00:22:03,600
So the point being that, that gain honor and renown are to be looked at as garbage. These are not good things.

115
00:22:03,600 --> 00:22:08,600
It's not to be understood that this is a good result, or something to be sought out,

116
00:22:08,600 --> 00:22:11,600
whereas the others actually all are to be sought out.

117
00:22:11,600 --> 00:22:14,600
So there's a distinction there, and we shouldn't mistake this.

118
00:22:14,600 --> 00:22:16,600
And some people actually go the other way with this.

119
00:22:16,600 --> 00:22:21,600
When they read this suit that, they will, or this suit, or should just like it,

120
00:22:21,600 --> 00:22:24,600
they will disdain virtue, and they will put it aside and say, it's not important.

121
00:22:24,600 --> 00:22:30,600
Just go to the end of the suit, and that says, we're at the fifth one, right?

122
00:22:30,600 --> 00:22:32,600
We're just going to be the goal.

123
00:22:32,600 --> 00:22:34,600
And put the rest of these aside.

124
00:22:34,600 --> 00:22:36,600
So you don't invert you, you don't in concentration.

125
00:22:36,600 --> 00:22:39,600
These are just the parts of the parts of the tree that you don't need, right?

126
00:22:39,600 --> 00:22:41,600
If you need heart wood, get rid of the rest of this stuff.

127
00:22:41,600 --> 00:22:43,600
So the simile falls apart there.

128
00:22:43,600 --> 00:22:45,600
And you have to look closely at what the Buddha is saying.

129
00:22:45,600 --> 00:22:48,600
He's not, it's not a direct comparison.

130
00:22:48,600 --> 00:22:54,600
With a tree, yes, you can put aside all the other parts of the tree to get it at the heart wood.

131
00:22:54,600 --> 00:22:58,600
Maybe you can't actually have to cut the tree down to get at the heart wood,

132
00:22:58,600 --> 00:23:01,600
and you get all of it, actually, isn't it?

133
00:23:01,600 --> 00:23:06,600
But you would discard the rest of it and just take the heart wood away with you as the Buddha says to you.

134
00:23:06,600 --> 00:23:10,600
But that's actually not the case in the spiritual path.

135
00:23:10,600 --> 00:23:13,600
You don't need gain honor and renown.

136
00:23:13,600 --> 00:23:19,600
It says he, when you're done for the acquires, it's all in all the cases he acquires it, but actually it's not always the case.

137
00:23:19,600 --> 00:23:26,600
Some months, some meditators, of course, never require gain honor and renown.

138
00:23:26,600 --> 00:23:28,600
So we shouldn't take this too literally.

139
00:23:28,600 --> 00:23:32,600
If people don't have a deep knowledge of the Buddha's teaching,

140
00:23:32,600 --> 00:23:40,600
they'll get the wrong idea in various ways in this way.

141
00:23:40,600 --> 00:23:44,600
But if you look closely, you can see there's a difference here.

142
00:23:44,600 --> 00:23:45,600
This one is no good.

143
00:23:45,600 --> 00:23:47,600
The rest of them are good and actually necessary.

144
00:23:47,600 --> 00:23:54,600
So you need virtue in order to gain concentration, you need concentration in order to gain knowledge and vision.

145
00:23:54,600 --> 00:24:01,600
Knowledge and vision, we're talking about in this one, is supposed to refer to magical powers.

146
00:24:01,600 --> 00:24:10,600
Now, if that's all it referred to, then it would be unnecessary.

147
00:24:10,600 --> 00:24:12,600
So you'd have something here that was not necessary.

148
00:24:12,600 --> 00:24:18,600
And so if going by the commutarial interpretation, then this one is optional.

149
00:24:18,600 --> 00:24:27,600
So the first one and the fourth one are potentially optional, but you'd have to consider that this would also possibly include

150
00:24:27,600 --> 00:24:37,600
Vipassana and Yana, because the word knowledge, of course, Yana, Yana is not just confined to the magical knowledge

151
00:24:37,600 --> 00:24:39,600
or supernatural knowledge.

152
00:24:39,600 --> 00:24:44,600
It's also used to describe and also Dasana.

153
00:24:44,600 --> 00:24:46,600
Yana, Dasana.

154
00:24:46,600 --> 00:24:49,600
These are also used to describe Vipassana.

155
00:24:49,600 --> 00:24:52,600
The word Vipassana is the same as Dasana.

156
00:24:52,600 --> 00:25:02,600
So the word vision here is the same as if you put a V on the beginning as sort of a special or in or in the sense of insight.

157
00:25:02,600 --> 00:25:11,600
It's the word insight comes from Vipassana, the in vision, inward vision or clear vision.

158
00:25:11,600 --> 00:25:18,600
So these words could also, and Yana Dasana could also refer to Vipassana in which case.

159
00:25:18,600 --> 00:25:23,600
It actually makes no sense, because in which case it would then be required.

160
00:25:23,600 --> 00:25:25,600
Because you can't get to the fifth one.

161
00:25:25,600 --> 00:25:27,600
You can't get to the final part.

162
00:25:27,600 --> 00:25:37,600
What if the Buddha is teaching without Vipassana, the knowledge and vision of Vipassana, of insight of the three into basically into the three characteristics.

163
00:25:37,600 --> 00:25:42,600
But of course, broken up into the various stages of knowledge.

164
00:25:42,600 --> 00:25:54,600
So of course, if one is pleased with magical powers, that's going to be a problem, but sometimes overlooked is being pleased with insight knowledge.

165
00:25:54,600 --> 00:26:07,600
If we interpret it that way, then we have this curious idea that you can actually become pleased with and complacent with insight knowledge, which actually does happen.

166
00:26:07,600 --> 00:26:16,600
Many people I think will read my booklet and start to practice meditation, but never actually go the next step of taking it seriously.

167
00:26:16,600 --> 00:26:22,600
And in that case, it's easy to become complacent when you start to gain some insight and some knowledge in your life gets better.

168
00:26:22,600 --> 00:26:25,600
And all of your major problems go away.

169
00:26:25,600 --> 00:26:35,600
Once life goes back to a sort of normal, sustainable semi pleasant state, when we become complacent.

170
00:26:35,600 --> 00:26:46,600
What happens with monks as well, they go and do meditation courses and don't really get anywhere and then feel complacent and never go back and continue it or never take it up or don't keep it up.

171
00:26:46,600 --> 00:26:55,600
See that in Thailand, a lot of the scholar monks will go and do courses and come out with them not really.

172
00:26:55,600 --> 00:27:04,600
I'm going to get a lot out of it, I think, but some of them don't seem to get lunch. And so they become complacent with the fact that they've done a course or something.

173
00:27:04,600 --> 00:27:15,600
I remember you, people and foreigners used to come to me, they used to, we used to give out certificates and that was a real mistake because then everybody was just doing it for the certificate.

174
00:27:15,600 --> 00:27:25,600
That's just a pretty sickening. You're high school to form, I never have to do this again. And the certificate now. Exactly.

175
00:27:25,600 --> 00:27:37,600
So as a result, even with nyanadasana, one becomes negligent. Also, nyanadasana being negligent and having suffering.

176
00:27:37,600 --> 00:27:47,600
And there's no direct suffering that comes from nyanadasana, but the arrogance and the conceit, of course, as we talked about earlier. This is what happened in the day with that, because he got actually this far.

177
00:27:47,600 --> 00:27:54,600
He gained supernaturally. He never got robust in the insight, but he gained mundane jhana and magical powers.

178
00:27:54,600 --> 00:28:16,600
And as a result, he lauded himself and disparage. And that's, of course, what this suit is all referring to today with that. It's using him as an example of this. But the suit is highly cited. This is a commonly cited suit that, of course, it puts everything in its place and puts a lot of monks in their place as well.

179
00:28:16,600 --> 00:28:25,600
So something that you hear taught in Thailand in Buddhist countries and Buddhist countries, Buddhist society.

180
00:28:25,600 --> 00:28:32,600
Oh, okay. Did we read this one yet? No, they hear it. He's similarly, of course.

181
00:28:32,600 --> 00:28:40,600
Suppose a man needing heartwood, seeking heartwood, wandering in search of heartwood, came with great treatment, standing possessed of heartwood.

182
00:28:40,600 --> 00:28:53,600
Passing over to the heartwood, he would cut off its sapwood and take it away, thinking it was heartwood. Then a man with good sight, seeing him, might say, this good man did not know the heartwood or the twigs and leaves.

183
00:28:53,600 --> 00:29:03,600
Thus, while needing heartwood, he cut off its sapwood and took it away, thinking it was heartwood. Whatever it was, this good man had to make with heartwood, his purpose will not be served.

184
00:29:03,600 --> 00:29:15,600
So, two bikus, here's some glance, when it goes forth out of faith, he lives in suffering. This biku is called one who has taken the sapwood of the holy light and stopped short with that.

185
00:29:15,600 --> 00:29:29,600
The sapwood is the heart of the tree that carries the sap of the up the tree, which I guess is the outside of the tree. Makes sense.

186
00:29:29,600 --> 00:29:35,600
And then that's the fourth one, the fourth one is the sapwood, and now we're going to get into the heartwood. This one's different, so pay attention.

187
00:29:35,600 --> 00:29:53,600
Here bikus, some glance, when goes forth out of faith from the home life and the homelessness, considering I am a victim of birth, aging and death, of sorrow, lamentation, pain, grief and despair, I am a victim of suffering, a prey to suffering. Surely an ending of this whole mass of suffering can be known.

188
00:29:53,600 --> 00:30:02,600
When he has gone forth thus, he acquires gain honor and renown. He is not pleased with that gain honor and renown, and his intention is not fulfilled.

189
00:30:02,600 --> 00:30:10,600
When he is diligent, he achieves that attainment of virtue. He has pleased with that attainment of virtue, but his intention is not fulfilled.

190
00:30:10,600 --> 00:30:20,600
When he is diligent, he achieves the containment of concentration. He is pleased without attainment of concentration, but his intention is not fulfilled.

191
00:30:20,600 --> 00:30:23,600
When he is diligent, he achieves knowledge and vision.

192
00:30:23,600 --> 00:30:27,600
He is pleased with that knowledge and vision, but his intention is not fulfilled.

193
00:30:27,600 --> 00:30:31,600
He does not, on account of it, lot himself and disparage others.

194
00:30:31,600 --> 00:30:34,600
He does not become intoxicated with that knowledge and vision.

195
00:30:34,600 --> 00:30:37,600
He does not grow negligent and fall in negligence.

196
00:30:37,600 --> 00:30:41,600
Being diligent, he attains perpetual liberation.

197
00:30:41,600 --> 00:30:45,600
And it is impossible for that beku to fall away from that perpetual deliverance.

198
00:30:45,600 --> 00:30:49,600
He is trying to find a good translation for a semi-wimoka.

199
00:30:49,600 --> 00:30:51,600
We mokka, or we mooty, I think.

200
00:30:51,600 --> 00:30:53,600
We mooty is liberation.

201
00:30:53,600 --> 00:30:56,600
A semi-amines timeless.

202
00:30:56,600 --> 00:31:01,600
A semi-amines timeless.

203
00:31:01,600 --> 00:31:05,600
A semi-amines timeless.

204
00:31:05,600 --> 00:31:07,600
A semi-amines timeless.

205
00:31:07,600 --> 00:31:10,600
I think timeless is that timeless better.

206
00:31:10,600 --> 00:31:15,600
A semi-amine mukka is a temporary liberation.

207
00:31:15,600 --> 00:31:20,600
So the Janas and Janadasana and Samadhi,

208
00:31:20,600 --> 00:31:23,600
the attainment of concentration, all of that is temporary.

209
00:31:23,600 --> 00:31:28,600
The attainment of the Loki and Janas is a semi-amine mukka.

210
00:31:28,600 --> 00:31:38,600
A semi-amine mukka, which is time-temporary liberation, temporary liberation.

211
00:31:38,600 --> 00:31:42,600
Temporary versus timeless.

212
00:31:42,600 --> 00:31:46,600
Non-temporary.

213
00:31:46,600 --> 00:31:49,600
And so it's being referred to here as the attainment of nibana.

214
00:31:49,600 --> 00:31:53,600
When someone realizes or experiences nibana,

215
00:31:53,600 --> 00:31:58,600
there is a break with samsara.

216
00:31:58,600 --> 00:32:01,600
And that liberation deversesis.

217
00:32:01,600 --> 00:32:07,600
That liberation is never lost.

218
00:32:07,600 --> 00:32:11,600
It's impossible for that people to fall away from that perpetual deliverance.

219
00:32:11,600 --> 00:32:13,600
It's like a crack in the dam.

220
00:32:13,600 --> 00:32:15,600
It can never be fixed.

221
00:32:15,600 --> 00:32:17,600
Even from the state of being a soda bonnet,

222
00:32:17,600 --> 00:32:21,600
it's only a matter of time before the dam breaks and the water is released.

223
00:32:21,600 --> 00:32:25,600
The person is released from suffering.

224
00:32:25,600 --> 00:32:27,600
So here we don't have to go through this.

225
00:32:27,600 --> 00:32:30,600
I don't think, okay, go for it.

226
00:32:30,600 --> 00:32:33,600
Suppose a man needing Harwood, seeking Harwood,

227
00:32:33,600 --> 00:32:35,600
wandering in search of Harwood, came to a great tree,

228
00:32:35,600 --> 00:32:38,600
standing possessive Harwood, and cutting off only its Harwood.

229
00:32:38,600 --> 00:32:41,600
He would take it away knowing it was Harwood.

230
00:32:41,600 --> 00:32:44,600
Then a man with good sight, seeing him might say,

231
00:32:44,600 --> 00:32:48,600
this good man knew the Harwood, the sapwood, the air bark,

232
00:32:48,600 --> 00:32:50,600
the outer bark, and the twigs and leaves.

233
00:32:50,600 --> 00:32:53,600
Thus, while needing Harwood, seeking Harwood,

234
00:32:53,600 --> 00:32:55,600
wandering in search of Harwood,

235
00:32:55,600 --> 00:32:57,600
he came to a great tree standing possessive Harwood,

236
00:32:57,600 --> 00:32:59,600
and cutting off only its Harwood.

237
00:32:59,600 --> 00:33:01,600
He took it off knowing it was Harwood.

238
00:33:01,600 --> 00:33:04,600
Whatever it was, this good man had to make with Harwood.

239
00:33:04,600 --> 00:33:06,600
His purpose will be served.

240
00:33:06,600 --> 00:33:08,600
So too, because here some glance

241
00:33:08,600 --> 00:33:10,600
when he goes forth out of faith,

242
00:33:10,600 --> 00:33:13,600
when he is diligent, he attains perpetual liberation.

243
00:33:13,600 --> 00:33:15,600
And it is impossible for the happy hoot

244
00:33:15,600 --> 00:33:17,600
to fall away from that perpetual liberation,

245
00:33:17,600 --> 00:33:20,600
deliverance, perpetual deliverance.

246
00:33:20,600 --> 00:33:22,600
So this guy's different.

247
00:33:22,600 --> 00:33:24,600
This person has actually found the Harwood.

248
00:33:24,600 --> 00:33:26,600
So looking for the Harwood,

249
00:33:26,600 --> 00:33:29,600
just like a person who is looking for Harwood

250
00:33:29,600 --> 00:33:31,600
and finds Harwood,

251
00:33:31,600 --> 00:33:35,600
who knows the difference between all the parts of a tree.

252
00:33:35,600 --> 00:33:39,600
A person in the Buddha's teaching who

253
00:33:39,600 --> 00:33:42,600
has found true deliverance.

254
00:33:42,600 --> 00:33:46,600
That's the person who understands Buddhist teaching.

255
00:33:46,600 --> 00:33:50,600
And who gets something useful out of it.

256
00:33:50,600 --> 00:33:52,600
My teacher gave a talk on this,

257
00:33:52,600 --> 00:33:55,600
and he said, we come to the Buddha's teaching.

258
00:33:55,600 --> 00:33:57,600
It's like we're under a great tree.

259
00:33:57,600 --> 00:33:59,600
We have to ask ourselves,

260
00:33:59,600 --> 00:34:01,600
what are we getting out of this?

261
00:34:01,600 --> 00:34:05,600
What are we getting out of our practice, our ordination?

262
00:34:05,600 --> 00:34:07,600
Are we getting just the gain?

263
00:34:07,600 --> 00:34:10,600
And let's place to live.

264
00:34:10,600 --> 00:34:14,600
Are we getting morality?

265
00:34:14,600 --> 00:34:16,600
Are we getting concentration?

266
00:34:16,600 --> 00:34:18,600
And so on.

267
00:34:18,600 --> 00:34:22,600
And the Buddha had something to say about

268
00:34:22,600 --> 00:34:25,600
the conclusion that we come to.

269
00:34:25,600 --> 00:34:26,600
This one.

270
00:34:26,600 --> 00:34:29,600
So this holy life, Vicus, does not have gain on her

271
00:34:29,600 --> 00:34:31,600
and renowned for its benefits,

272
00:34:31,600 --> 00:34:35,600
or the attainment of virtue for its benefits,

273
00:34:35,600 --> 00:34:38,600
or the attainment of concentration for its benefits,

274
00:34:38,600 --> 00:34:41,600
or knowledge and vision for its benefits.

275
00:34:41,600 --> 00:34:44,600
But it is this unshakable deliverance of mine

276
00:34:44,600 --> 00:34:46,600
that is the goal of this holy life.

277
00:34:46,600 --> 00:34:49,600
It's Harwood and its end.

278
00:34:49,600 --> 00:34:59,600
The attainment of our hardship is the goal.

279
00:34:59,600 --> 00:35:03,600
Being none of the others are the point.

280
00:35:03,600 --> 00:35:06,600
The person who finds those is like a person

281
00:35:06,600 --> 00:35:08,600
who has gone looking for something of value

282
00:35:08,600 --> 00:35:12,600
and has come up with something that's not valuable,

283
00:35:12,600 --> 00:35:15,600
not intrinsically valuable.

284
00:35:15,600 --> 00:35:20,600
They're actually considered, these things are considered valuable,

285
00:35:20,600 --> 00:35:24,600
but only in the sense that they lead towards the final goal.

286
00:35:24,600 --> 00:35:26,600
Of course, there are five Sara.

287
00:35:26,600 --> 00:35:29,600
This is Sara, Mahasara, Opamasutu.

288
00:35:29,600 --> 00:35:33,600
So I'll give you the five types of Sara of essence

289
00:35:33,600 --> 00:35:35,600
or of things of value.

290
00:35:35,600 --> 00:35:37,600
Then the five things of value,

291
00:35:37,600 --> 00:35:39,600
I'm not mentioned in the Suta,

292
00:35:39,600 --> 00:35:41,600
are morality, concentration, wisdom,

293
00:35:41,600 --> 00:35:46,600
release, and knowledge and vision of release.

294
00:35:46,600 --> 00:35:50,600
So even morality, concentration,

295
00:35:50,600 --> 00:35:55,600
and even morality and concentration are considered

296
00:35:55,600 --> 00:35:58,600
to be valuable,

297
00:35:58,600 --> 00:36:01,600
but only in the sense that they lead to the release

298
00:36:01,600 --> 00:36:04,600
and knowledge and vision of the age.

299
00:36:04,600 --> 00:36:07,600
So you want to read the last paragraph?

300
00:36:07,600 --> 00:36:09,600
That is what the plus one said.

301
00:36:09,600 --> 00:36:11,600
The vehicles were satisfied and delighted

302
00:36:11,600 --> 00:36:13,600
in the best one's words.

303
00:36:19,600 --> 00:36:23,600
So that's another Suta down.

304
00:36:23,600 --> 00:36:24,600
Hey, seven years today.

305
00:36:24,600 --> 00:36:26,600
Thank you all for tuning in.

306
00:36:26,600 --> 00:36:42,600
Have a good name.

