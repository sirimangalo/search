1
00:00:00,000 --> 00:00:03,200
No, welcome back to Ask a Monk.

2
00:00:03,200 --> 00:00:09,180
Next question comes from B.D. 951, who asks,

3
00:00:09,180 --> 00:00:11,160
Can you speak on the Topitica?

4
00:00:11,160 --> 00:00:15,400
I heard the oral tradition wasn't written down for many years.

5
00:00:15,400 --> 00:00:17,920
Even then, not in language of the Buddha.

6
00:00:17,920 --> 00:00:22,680
If this is the case, why are I giving over such things as did the Buddha say, as did

7
00:00:22,680 --> 00:00:30,240
the Buddha say, rights or rituals or prayers and practices, thanks.

8
00:00:30,240 --> 00:00:33,440
So talking about the Topitica.

9
00:00:33,440 --> 00:00:39,080
And specifically talking about the Topitica in terms of its exactness.

10
00:00:39,080 --> 00:00:43,560
First of all, the Topitica is supposed to have been written down in something very close

11
00:00:43,560 --> 00:00:46,560
to what the Buddha actually spoke.

12
00:00:46,560 --> 00:00:54,880
This is the language that it was memorized in, and it may have been standardized a little

13
00:00:54,880 --> 00:01:06,400
bit, even in those early times where there may be spellings or forms that were standardized

14
00:01:06,400 --> 00:01:13,360
in terms of the spelling and the harmonization of the actual words.

15
00:01:13,360 --> 00:01:20,520
But the language itself is very similar to Indian languages that you find today to

16
00:01:20,520 --> 00:01:27,000
a certain extent, and quite similar to Sanskrit, actually, but much more of a spoken language

17
00:01:27,000 --> 00:01:32,000
is the language or a prokrid that they call palli, or we call palli.

18
00:01:32,000 --> 00:01:37,120
It's also called mugada, among Buddhists, we'll call it mugada, the mugada language

19
00:01:37,120 --> 00:01:41,120
because that was the area where the Buddha lived and we believe it to be the language

20
00:01:41,120 --> 00:01:46,160
that he spoke it may not have been exactly as I said.

21
00:01:46,160 --> 00:01:50,720
The second thing is about an oral tradition, and there's something great about an oral

22
00:01:50,720 --> 00:01:55,160
tradition that is really misunderstood.

23
00:01:55,160 --> 00:01:59,240
We think that an oral tradition is somehow inferior to the written word, and that's not

24
00:01:59,240 --> 00:02:00,240
really the case.

25
00:02:00,240 --> 00:02:10,440
The mind is an incredible computer, and if used properly is much more reliable than a

26
00:02:10,440 --> 00:02:17,800
book, and this is far true.

27
00:02:17,800 --> 00:02:27,120
This is even more true when you're talking about groups of people memorizing texts.

28
00:02:27,120 --> 00:02:32,320
Now I've done some memorizing of texts, and it's true that sometimes I get it wrong,

29
00:02:32,320 --> 00:02:36,600
but I misremember something.

30
00:02:36,600 --> 00:02:41,400
Here we're talking about a language that was native to these people, this was the language

31
00:02:41,400 --> 00:02:44,560
that they spoke, so what they were memorizing.

32
00:02:44,560 --> 00:02:49,240
Maybe not exactly as I said, but what they were memorizing was something that was very

33
00:02:49,240 --> 00:02:52,720
easily memorable to them.

34
00:02:52,720 --> 00:02:59,800
Whereas I might skip a word or get a verb form wrong, I might conjugate a verb wrong in

35
00:02:59,800 --> 00:03:05,280
the wrong form, they would never do that, and skipping a word would not be as likely

36
00:03:05,280 --> 00:03:15,840
either because it wouldn't make sense, and it wouldn't fit with the standard mode of expression.

37
00:03:15,840 --> 00:03:20,960
When you're talking about groups of people, which we are, the number of Buddhist monks

38
00:03:20,960 --> 00:03:27,560
when the Buddha passed away was, I don't know, there were thousands upon thousands, I

39
00:03:27,560 --> 00:03:34,400
don't know, thousands of them anyway, and they would memorize these texts in large

40
00:03:34,400 --> 00:03:41,880
groups of people, and it's incredible to me even how just as one person, how much I can

41
00:03:41,880 --> 00:03:42,880
remember.

42
00:03:42,880 --> 00:03:48,080
I can memorize the Buddha, I can memorize pieces of this text piece by piece by piece

43
00:03:48,080 --> 00:03:50,600
until I eventually get the suta.

44
00:03:50,600 --> 00:03:55,200
Go when I go back, I've missed some things, and I go back over it and over it, and

45
00:03:55,200 --> 00:03:57,840
eventually it sticks in memory.

46
00:03:57,840 --> 00:04:00,720
There are sutas that I haven't memorized for a long time.

47
00:04:00,720 --> 00:04:07,120
There was a story of a monk who 20 years, he didn't look at the Majima Nikaya, which

48
00:04:07,120 --> 00:04:15,960
is 152, talks the Buddha gave, and 20 years later he could still recite it after not

49
00:04:15,960 --> 00:04:20,960
looking at it for 20 years, not thinking about it, he went off to practice meditation,

50
00:04:20,960 --> 00:04:28,000
and as a result of the meditation practice, they say he was able to memorize it without

51
00:04:28,000 --> 00:04:31,960
any trouble.

52
00:04:31,960 --> 00:04:37,120
There's something there, obviously it's possible, and it seems quite likely that there

53
00:04:37,120 --> 00:04:44,640
were errors that crept into the Buddha's teaching, but I don't think the oral tradition

54
00:04:44,640 --> 00:04:47,560
was really to blame.

55
00:04:47,560 --> 00:04:53,440
There's an example in the Christian tradition, if there's a man, a scholar, one of the

56
00:04:53,440 --> 00:05:03,280
greatest Bible scholars, a great Bible scholar, Bart Erman, who explains that, actually,

57
00:05:03,280 --> 00:05:08,440
we don't know what Jesus actually said because we don't have the original texts, we don't

58
00:05:08,440 --> 00:05:13,280
know if those texts are what Jesus actually said or not, but we don't have those texts,

59
00:05:13,280 --> 00:05:21,400
he said what we have are mistakes upon mistakes upon mistakes, or errors, or changes upon

60
00:05:21,400 --> 00:05:22,400
changes upon changes.

61
00:05:22,400 --> 00:05:27,800
There's more changes in the, there's more discrepancies between the different texts than

62
00:05:27,800 --> 00:05:34,560
there are words in the Bible, and he attributes this mostly, well, partially to scribal

63
00:05:34,560 --> 00:05:42,040
errors, you know, I mean, scribes are generally not meditating, they're not engaged in

64
00:05:42,040 --> 00:05:49,080
the practice of the Buddhist, or of the teaching, scribes are generally paid, or in the Buddhist

65
00:05:49,080 --> 00:05:56,760
tradition, maybe not paid, but they, they wouldn't likely be the meditation monks.

66
00:05:56,760 --> 00:06:01,520
And so as a result, there were many errors that crept into the, the Christian texts, but

67
00:06:01,520 --> 00:06:10,600
another reason was the, the, the doctrinal differences, so there were many schisms and

68
00:06:10,600 --> 00:06:12,680
they changed according to their schisms.

69
00:06:12,680 --> 00:06:16,960
That is maybe the case in Buddhism as well, because there were many different schools and

70
00:06:16,960 --> 00:06:22,040
there have been many different schools, and as a result, we do find the same discourse

71
00:06:22,040 --> 00:06:32,800
has different, and often pointedly different, you know, information where this school, it,

72
00:06:32,800 --> 00:06:38,120
it follows after their idea and that school it follows after their idea and so on.

73
00:06:38,120 --> 00:06:39,280
So that is the case.

74
00:06:39,280 --> 00:06:46,080
I think the point though, whether there may be errors and changes and, and so on in, in

75
00:06:46,080 --> 00:06:54,400
the tepitika, or not, and whether it actually is with the Buddha said or not, is that

76
00:06:54,400 --> 00:07:02,600
we have the, we have a Buddhist corpus, we have this tepitika, which is a very internally

77
00:07:02,600 --> 00:07:10,040
consistent group of texts, and should we take it literally, you know, as every piece of

78
00:07:10,040 --> 00:07:16,080
it is the Buddha's teaching? No, I think to an extent, we shouldn't, to an extent we

79
00:07:16,080 --> 00:07:24,840
should be able to apply the basic concepts that are found therein, and because what we

80
00:07:24,840 --> 00:07:31,080
have is based on, obviously, based on the teachings of the Buddha, whether or not,

81
00:07:31,080 --> 00:07:35,760
no, it'd be nice if it was all exactly with the Buddha said, and I tend to think, tend

82
00:07:35,760 --> 00:07:40,720
to, you know, for myself, I say, you know, unless you can prove to me or find some conclusive

83
00:07:40,720 --> 00:07:45,640
evidence that it's not, and I'd rather think that it is with the Buddha taught, and there

84
00:07:45,640 --> 00:07:52,760
are many people out there who try to, you know, put the texts into times and say, you

85
00:07:52,760 --> 00:07:57,760
know, this, this group of texts was later because it's in a different, you know, meter

86
00:07:57,760 --> 00:08:03,320
is different, the poetry is of a different type and so on, and they have all these theories

87
00:08:03,320 --> 00:08:09,040
that, you know, seem to me, you know, I don't know, whatever, they have their theories,

88
00:08:09,040 --> 00:08:18,720
I have my theories, or lack thereof, but the point is, is that, you know, clearly, if you

89
00:08:18,720 --> 00:08:23,280
read through the text, clearly there is wisdom here, clearly there is something that has

90
00:08:23,280 --> 00:08:29,840
something to do with Buddhism, which means with enlightenment, Buddha means one who knows

91
00:08:29,840 --> 00:08:36,160
with knowledge with wisdom, there is Buddhism in these texts, there is wisdom in enlightenment

92
00:08:36,160 --> 00:08:41,360
in these texts, and if you can get that from the text, I think that is, is really the

93
00:08:41,360 --> 00:08:42,520
most important thing.

94
00:08:42,520 --> 00:08:48,960
There are passages in there that are so profound, and, you know, there will be a point

95
00:08:48,960 --> 00:08:53,600
where the Buddha said, all you need to know, before you start to practice meditation,

96
00:08:53,600 --> 00:08:59,360
all you need to know is that nothing is worth clinging to, basically it is something that

97
00:08:59,360 --> 00:09:06,240
you find in the text, if a person learns that nothing, no dhamma, no reality, no thing

98
00:09:06,240 --> 00:09:09,960
is worth clinging to, then this is enough for them to get started.

99
00:09:09,960 --> 00:09:17,760
This is enough as far as book learning goes, this is enough as far as a basic or a precursor

100
00:09:17,760 --> 00:09:20,440
to the practice of the Buddha's teaching.

101
00:09:20,440 --> 00:09:26,920
So this is how we should look at the, the, the, tepitika, you know, for people to go about

102
00:09:26,920 --> 00:09:30,480
quoting sutas and saying, this proves that, that proves that.

103
00:09:30,480 --> 00:09:37,000
And often saying, you know, this implies, this implies that, and, and therefore dogmatically

104
00:09:37,000 --> 00:09:42,240
sticking to us of set practice, I think is really sad.

105
00:09:42,240 --> 00:09:46,600
There are many different types of practice out there based on the Buddha's teaching, and

106
00:09:46,600 --> 00:09:51,280
you'll see that if you read the tepitika, if you read the sutas, that there wasn't one

107
00:09:51,280 --> 00:09:56,960
practice that the Buddha recommended for all people.

108
00:09:56,960 --> 00:10:00,640
If you could take it literally, it wouldn't do much good to say, you know, look at this

109
00:10:00,640 --> 00:10:03,480
sutas, but it said, this is the way to enlightenment.

110
00:10:03,480 --> 00:10:07,400
Someone comes up and look at this sutas, this is the way to enlightenment.

111
00:10:07,400 --> 00:10:11,560
So it wouldn't even do good if it was.

112
00:10:11,560 --> 00:10:18,800
The important thing is the basic understanding and the premise that we can then use for

113
00:10:18,800 --> 00:10:20,760
our practice.

114
00:10:20,760 --> 00:10:24,640
There are many things in the tepitika that will guide you away from wrong practice, and

115
00:10:24,640 --> 00:10:29,560
you should get those, you should understand those, but they're very general principles.

116
00:10:29,560 --> 00:10:36,080
The principles of impermanence, of impermanence, suffering, and non-self in the sense that

117
00:10:36,080 --> 00:10:38,920
there's nothing in the world.

118
00:10:38,920 --> 00:10:44,680
There's nothing in samsara that is permanent, that is lasting, that you can cling to.

119
00:10:44,680 --> 00:10:48,600
There's nothing in samsara that is going to make you happy if you do cling to it, because

120
00:10:48,600 --> 00:10:55,200
it's impermanence, and there's nothing in samsara in the whole of existence.

121
00:10:55,200 --> 00:11:01,360
That you can say is me, is mine, that it is under my control, that you can control, that

122
00:11:01,360 --> 00:11:06,360
you can change to say, okay, it's impermanent, I'm going to make it permanent, there's

123
00:11:06,360 --> 00:11:08,960
no stability to be found.

124
00:11:08,960 --> 00:11:12,720
These three general principles, well, you'll find them throughout the tepitika, but I think

125
00:11:12,720 --> 00:11:18,400
clearly those are core ideas in the Buddha's teaching, and that has to do with the

126
00:11:18,400 --> 00:11:24,760
four noble truths, which are basically the core of the Buddha's teaching.

127
00:11:24,760 --> 00:11:28,400
You shouldn't take this as some kind of doctrine, where, yes, the Buddha teach you

128
00:11:28,400 --> 00:11:33,560
taught the four noble truths, and then you think about them, and you understand them intellectually,

129
00:11:33,560 --> 00:11:38,480
and you know how they fit into all this text, and this text, and this group of teaching

130
00:11:38,480 --> 00:11:46,720
fits in and so on, it's really useless, this is the Buddha said, like people who look

131
00:11:46,720 --> 00:11:49,120
after the cows of others.

132
00:11:49,120 --> 00:11:53,800
It's not so important to know everything, it's important to know what's important, it's

133
00:11:53,800 --> 00:11:59,200
important to know those things, and to know them well, those things that lead you to freedom

134
00:11:59,200 --> 00:12:08,600
from suffering, and intellectually, simply to understand the types of things that are going

135
00:12:08,600 --> 00:12:14,800
to help and improve your practice, what are the wrong ways of practice, the wrong courses

136
00:12:14,800 --> 00:12:19,760
of action, what are the right courses of action, what are wholesome things, what are unwholesome

137
00:12:19,760 --> 00:12:26,520
things, and this is really important, getting the general principles, so reading the tepitika

138
00:12:26,520 --> 00:12:32,360
is great, read through it, but don't look through it, trying to pick out the, why did

139
00:12:32,360 --> 00:12:36,360
the Buddha say this, why did the Buddha say that, get general principles, and then go to

140
00:12:36,360 --> 00:12:41,320
practice meditation, because what the Buddha was teaching was about reality, was about

141
00:12:41,320 --> 00:12:48,200
this, our existence, what's happening right here and now, this listening to my sound

142
00:12:48,200 --> 00:12:55,000
of your speakers, and the feeling in your chair, and the hot and heat and the cold, the

143
00:12:55,000 --> 00:13:00,040
emotions going through your mind, the intellectual process of liking what I say, disliking

144
00:13:00,040 --> 00:13:08,960
what I say, judging what I say, the ego, the delusion, and so on, all of this is what the

145
00:13:08,960 --> 00:13:15,000
Buddha taught, it's clearly there in the tepitika, and if something in the tepitika goes

146
00:13:15,000 --> 00:13:21,720
against reality, that's when you should discard it, not when it goes against what you

147
00:13:21,720 --> 00:13:28,040
believe, and I think this is what leads a lot of people to criticize and to find patterns

148
00:13:28,040 --> 00:13:32,680
of what is really the Buddha's teaching and what is not, because it's based on their views

149
00:13:32,680 --> 00:13:39,400
and their opinions, they don't like this teaching, so they throw it out, they don't believe

150
00:13:39,400 --> 00:13:47,440
this teaching is in line with their idea of what is right and so on, but there's nothing

151
00:13:47,440 --> 00:13:55,200
you can do about that, because there are as many views as there are people, you can't

152
00:13:55,200 --> 00:14:03,360
possibly make everyone agree with any teaching, even the truth, there are more people

153
00:14:03,360 --> 00:14:10,960
probably who will refuse to believe the truth than people who will believe the truth.

154
00:14:10,960 --> 00:14:17,280
Maybe not, there are definitely people who will refuse to believe the truth when given

155
00:14:17,280 --> 00:14:26,680
to them, probably I'd say more, more who will refuse to believe the truth, so don't

156
00:14:26,680 --> 00:14:34,680
go looking for a doctrine per se in the tepitika in terms of the Buddha taught this, and

157
00:14:34,680 --> 00:14:41,400
this is the truth in the text, so Buddha was against that, he said, when you know for yourself

158
00:14:41,400 --> 00:14:48,760
that these things are to your benefit, then develop them, when you know for yourself that

159
00:14:48,760 --> 00:14:54,000
certain things are for your detriment and abandon them, and that's what happens, once you

160
00:14:54,000 --> 00:14:59,960
know that something is for your detriment you will not develop it, it will only decrease and

161
00:14:59,960 --> 00:15:04,880
decline, and once you see clearly that it's not to your benefit, you will never give rise

162
00:15:04,880 --> 00:15:12,880
to it again, it will disappear, so simple, you gave one specific example here which I'm

163
00:15:12,880 --> 00:15:19,280
not really, it doesn't really mean too much to me, you know the example of whether did

164
00:15:19,280 --> 00:15:24,800
the Buddha say rights and rituals or prayers and practices, well that's really a different

165
00:15:24,800 --> 00:15:31,800
question though, that's the question of translation because those two terms come from the

166
00:15:31,800 --> 00:15:39,680
same poly passage, the same word, as Silapataparamasa, and so it's not whether the Buddha

167
00:15:39,680 --> 00:15:47,680
said A or B, it's whether one or the other is a more proper translation, both etymologically

168
00:15:47,680 --> 00:15:55,080
and in terms of the Buddha had with the Buddha meant by it, Silapataparamasa specifically

169
00:15:55,080 --> 00:16:08,280
and this is, it's only an example, but Silapatapatapatapatapamasa, Sila means refraining

170
00:16:08,280 --> 00:16:16,020
from certain things, what that means undertaking certain things or certain practices,

171
00:16:16,020 --> 00:16:22,320
so whatever you translate them as, it means, one is negative and one is positive, refraining

172
00:16:22,320 --> 00:16:31,800
and undertaking in the sense of refraining and undertaking.

173
00:16:31,800 --> 00:16:42,600
Bhara amasabara means others, or external, and amasam means clinging, or holding on to grasping.

174
00:16:42,600 --> 00:16:50,600
So it's holding on to these two things, you know, certain refraining from certain things

175
00:16:50,600 --> 00:16:54,600
and undertaking certain things, and holding on that to be important.

176
00:16:54,600 --> 00:16:59,800
Like saying, to be a good Buddhist, you have to do this, you have to light incense, you

177
00:16:59,800 --> 00:17:07,000
have to bow in this way, in this way you have to chant in this way, in this way, in this way,

178
00:17:07,000 --> 00:17:18,240
you know, it's important to practice, to do certain things, and to refrain from certain things

179
00:17:18,240 --> 00:17:23,520
that are outside of the path, so that don't have anything to do with the path.

180
00:17:23,520 --> 00:17:29,760
If someone says, you know, you have to light incense, or you have to bow, and this is the

181
00:17:29,760 --> 00:17:34,720
way to, or this is important, in the practice of enlightenment, or if someone says you have

182
00:17:34,720 --> 00:17:43,400
to refrain from eating garlic, or you have to refrain from this, or refrain from that,

183
00:17:43,400 --> 00:17:53,320
to practice the Buddha's teaching, and that has nothing to do with the Buddha's teaching,

184
00:17:53,320 --> 00:17:58,840
or there's nothing to do with reality, with understanding and realizing the truth.

185
00:17:58,840 --> 00:18:01,720
That's what is meant by Siva Bhata Paramasa.

186
00:18:01,720 --> 00:18:06,840
So the problem you have here is translation, in which translation there is going to give

187
00:18:06,840 --> 00:18:11,240
people a better understanding of the actual poly word, and as you can see, it's a complicated

188
00:18:11,240 --> 00:18:18,520
word. The best way to solve that dilemma is to learn poly, and that's a great thing to do

189
00:18:18,520 --> 00:18:25,080
as a Buddhist. It's a life goal. If you're a person who is dedicated to this path, it's

190
00:18:25,080 --> 00:18:30,920
worth it to learn poly, because any translation is only makeshift. You see, there are two examples

191
00:18:30,920 --> 00:18:38,840
which one is right. Well, neither one exactly captures those four words that make up the compound.

192
00:18:38,840 --> 00:18:47,960
So, anyway, just a food for thought and a little bit of insight for today. Thanks for the question of the best.

