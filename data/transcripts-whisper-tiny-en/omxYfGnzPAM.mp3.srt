1
00:00:00,000 --> 00:00:07,000
Can a mental illness which severely impairs one's mind, such as schizophrenia or Alzheimer's

2
00:00:07,000 --> 00:00:18,000
disease, damage or undo one's progress towards enlightenment, damage, yes, undo potentially.

3
00:00:18,000 --> 00:00:26,520
A person who has become a Sotapana will not fall back, will not unbecome a Sotapana,

4
00:00:26,520 --> 00:00:31,520
and a Sotakatakami Nangami, a person who has seen Nibana that isn't undone.

5
00:00:31,520 --> 00:00:38,520
The realisation, the attainment or the observation, the experience of Nibana is not something

6
00:00:38,520 --> 00:00:43,520
that the effects of it don't ever wear off because it's not something that arises.

7
00:00:43,520 --> 00:00:47,520
It's outside of Samsara.

8
00:00:47,520 --> 00:00:50,520
Apart from that, certainly.

9
00:00:50,520 --> 00:00:58,520
I mean, I don't have any texts to go by, but based on my own experience.

10
00:00:58,520 --> 00:01:03,520
There's a question in here someone asked about, commented on the fact that I'm always saying

11
00:01:03,520 --> 00:01:07,520
in the Buddhist tradition or according to the texts or so on.

12
00:01:07,520 --> 00:01:14,520
And so they got the idea that somehow I'm just parrotting back the tradition,

13
00:01:14,520 --> 00:01:17,520
which I suppose you could say is true, but I look at it the other way.

14
00:01:17,520 --> 00:01:20,520
I look at it like, I'm not Buddhism.

15
00:01:20,520 --> 00:01:27,520
And anyone who thinks that I'm what I think is the be-all-end-all of what the Buddha taught

16
00:01:27,520 --> 00:01:32,520
is misled.

17
00:01:32,520 --> 00:01:37,520
So for me to say, well, I think, and I believe, or to just say things,

18
00:01:37,520 --> 00:01:42,520
say my own beliefs as truth, I think that would be the travesty.

19
00:01:42,520 --> 00:01:48,520
I think going by the tradition is much more useful beneficial for people.

20
00:01:48,520 --> 00:01:59,520
Because I won't say about myself, but anyway, I guess I can say,

21
00:01:59,520 --> 00:02:06,520
I have faith in the tradition or have to say it without sounding like I'm bragging,

22
00:02:06,520 --> 00:02:14,520
but have some, no, for me, this is the truth.

23
00:02:14,520 --> 00:02:19,520
And so when I talk about the Buddhist texts,

24
00:02:19,520 --> 00:02:25,520
it's because of my appreciation and agreement with them.

25
00:02:25,520 --> 00:02:29,520
If I don't agree, I'll try to say I don't.

26
00:02:29,520 --> 00:02:31,520
Maybe just don't understand or something.

27
00:02:31,520 --> 00:02:34,520
But if I don't agree, I'll say it.

28
00:02:34,520 --> 00:02:38,520
But so in this case, I don't have any text to go by.

29
00:02:38,520 --> 00:02:45,520
So I try to improvise.

30
00:02:45,520 --> 00:02:51,520
But there definitely seems to be some leeway for the physical to affect the mental.

31
00:02:51,520 --> 00:02:54,520
Now we always say how important the mental is.

32
00:02:54,520 --> 00:02:59,520
And that sometimes gives the impression that the physical has no effect on the mental.

33
00:02:59,520 --> 00:03:01,520
That's certainly not true.

34
00:03:01,520 --> 00:03:08,520
As far as I understand, it's quite clear that the body can give rise.

35
00:03:08,520 --> 00:03:10,520
Physical can give rise to the mental.

36
00:03:10,520 --> 00:03:18,520
So the body is often productive of certain mental states and can change mental states.

37
00:03:18,520 --> 00:03:22,520
This is why drinking alcohol is actually a problem even though it's physical.

38
00:03:22,520 --> 00:03:27,520
Because it actually does affect the mind and actually causes problems for the mind.

39
00:03:27,520 --> 00:03:36,520
So the idea that these states can get in the way and can actually be supportive in the creation of unwholesomeness.

40
00:03:36,520 --> 00:03:39,520
I think you have a strong case for that.

41
00:03:39,520 --> 00:03:43,520
Now they aren't enough in and of themselves to give rise to unwholesomeness

42
00:03:43,520 --> 00:03:45,520
because that takes the mind.

43
00:03:45,520 --> 00:03:50,520
But they can certainly trigger unwholesomeness that is already present.

44
00:03:50,520 --> 00:03:52,520
What we call anusaya.

45
00:03:52,520 --> 00:03:57,520
Anusaya means latent tendencies that are triggered by stimuli.

46
00:03:57,520 --> 00:04:05,520
So schizophrenia, Alzheimer's disease, these would be triggers.

47
00:04:05,520 --> 00:04:12,520
And for a person who's not already enlightened, they would be quite damaging to the person's practice or inhibiting.

48
00:04:12,520 --> 00:04:16,520
They would definitely inhibit impair one's practice.

49
00:04:16,520 --> 00:04:22,520
Certainly it's not the case that everyone has the ability to practice. That being said,

50
00:04:22,520 --> 00:04:24,520
it doesn't mean that these people can't practice.

51
00:04:24,520 --> 00:04:29,520
It just means it will be harder for them and they shouldn't expect to have an easy go at it.

52
00:04:29,520 --> 00:04:35,520
I don't know if someone with standard classic textbook schizophrenia can become enlightened.

53
00:04:35,520 --> 00:04:36,520
I don't know that.

54
00:04:36,520 --> 00:04:40,520
I do believe that it's going to be more difficult for them.

55
00:04:40,520 --> 00:04:42,520
They have positives on their side.

56
00:04:42,520 --> 00:04:46,520
Many schizophrenias, I would say, are desperate.

57
00:04:46,520 --> 00:04:51,520
Not desperate, but are looking for something as are many of us.

58
00:04:51,520 --> 00:04:58,520
Whereas an ordinary person might not and might be complacent in a way that a schizophrenic might not.

59
00:04:58,520 --> 00:05:06,520
So there's something same with a person who has Alzheimer's, a person who has onset of Alzheimer's

60
00:05:06,520 --> 00:05:21,520
will become incredibly agitated and go clean quite strongly or seek out or pursue the meditation practice quite intensely,

61
00:05:21,520 --> 00:05:25,520
knowing that they only have so long before their mind is going to go.

62
00:05:25,520 --> 00:05:26,520
That kind of thing.

63
00:05:26,520 --> 00:05:37,520
And the actual disease itself is not likely to be detrimental and impediment in the practice.

64
00:05:37,520 --> 00:05:38,520
How much?

65
00:05:38,520 --> 00:05:40,520
That makes an interesting thing.

66
00:05:40,520 --> 00:05:42,520
It's something that we should experiment with.

67
00:05:42,520 --> 00:05:49,520
Certainly these are the sort of people who could definitely benefit.

68
00:05:49,520 --> 00:05:58,520
I don't know how to do it, but I don't know how to do it.

