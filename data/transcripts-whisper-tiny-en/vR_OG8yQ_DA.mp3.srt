1
00:00:00,000 --> 00:00:15,720
Okay, good evening everyone, welcome to our evening dharma session, tonight I'd like to talk

2
00:00:15,720 --> 00:00:29,480
about the Brahma Mihara. Brahma Mihara is translated often as divine

3
00:00:29,480 --> 00:00:38,880
abiding. I think it goes beyond that, reluctant to translate it in that way, because

4
00:00:38,880 --> 00:00:45,560
divine is what we use for heaven. It doesn't have anything to do with heaven, it has

5
00:00:45,560 --> 00:00:58,800
to do with Brahma. Nobody uses the word Brahma. Brahma means the highest, or it means

6
00:00:58,800 --> 00:01:10,440
the sacred means the ultimate in a sense, the high, the holy, so often used to mean

7
00:01:10,440 --> 00:01:17,640
holiness. So I've translated here as houses of the holy, which I thought was

8
00:01:17,640 --> 00:01:29,400
a saying, but apparently it's just a Led Zeppelin album. Anyway, Brahma Mihara

9
00:01:29,400 --> 00:01:43,560
Mihara is dwelling, or a bode, and Brahma is holy. And these are not directly

10
00:01:43,560 --> 00:01:50,320
related with Vipasana, but I think it's useful to look at how they relate to our

11
00:01:50,320 --> 00:01:59,560
practice anyway, and you'll see how we can relate them to insight meditation. The

12
00:01:59,560 --> 00:02:05,080
first thing to talk about is how these are understood in the world, often misunderstood,

13
00:02:05,080 --> 00:02:16,080
and misused in the world. We often focus too much on the four Brahma Mihara's.

14
00:02:16,080 --> 00:02:31,040
Brahma Mihara's are love, or friendliness, compassion, sympathy, or rejoicing at

15
00:02:31,040 --> 00:02:45,600
others' fortune and equanimity. But the way they're used often in the world is not

16
00:02:45,600 --> 00:02:51,760
even in a terribly wholesome way. Love, for example, or friendliness. We become attached

17
00:02:51,760 --> 00:03:00,400
to people, thinking that that's love. Our friendliness can easily become affection and

18
00:03:00,400 --> 00:03:11,720
partiality to the point where we are attached and dependent on others, and then vulnerable

19
00:03:11,720 --> 00:03:26,800
to their manipulation and vulnerable to loss and vulnerable to change. And we suffer as a result.

20
00:03:26,800 --> 00:03:40,640
Then we can get quite upset at the suffering of others, angry even. We can feel sad. We

21
00:03:40,640 --> 00:03:48,640
can feel frustrated. We can get burnt out, quite disillusioned and depressed when we think

22
00:03:48,640 --> 00:03:57,080
about how much suffering there is in the world. Not a very useful or wholesome state of

23
00:03:57,080 --> 00:04:08,680
mind. And as far as sympathetic joy, we can be happy for others, but we can also become

24
00:04:08,680 --> 00:04:36,440
intoxicated by that, become obsessed or indulgent in thinking about or rejoicing. I mean,

25
00:04:36,440 --> 00:04:41,040
the idea is when someone, something good happens to someone, and then you go out and party

26
00:04:41,040 --> 00:04:51,120
about it. It's not what is meant by rejoicing. Let's go celebrate celebrating, for example,

27
00:04:51,120 --> 00:04:56,280
celebrating, because then to just speak, it's no longer appreciation. It's about no longer

28
00:04:56,280 --> 00:05:08,480
about appreciation. It's about festivity. So we go beyond what is wholesome. And with

29
00:05:08,480 --> 00:05:15,040
equanimity, that of course, the most obvious way that this goes wrong is by not caring

30
00:05:15,040 --> 00:05:28,280
in the sense of not discerning right from wrong, good from bad. So I'm greedy. So I get

31
00:05:28,280 --> 00:05:41,560
angry. So what? The selfish, ignorant, individual, this kind of delusion, equanimity based

32
00:05:41,560 --> 00:05:47,800
on delusions, quite dangerous, because it's not really equanimity at all, but it's a sense

33
00:05:47,800 --> 00:05:54,920
of equanimity, a sense of not caring, lack of interest in your own benefit or lack of interest

34
00:05:54,920 --> 00:06:03,120
in any benefit whatsoever. So the first thing to understand is these are not what is meant,

35
00:06:03,120 --> 00:06:13,120
or understand the ways that these go wrong. When we talk about how they go right, first

36
00:06:13,120 --> 00:06:23,480
is even as insight meditators, how we put them into practice, in support of our practice,

37
00:06:23,480 --> 00:06:28,320
being friendly to each other. Even in a meditation center, we have to take care of each

38
00:06:28,320 --> 00:06:37,320
other. We have to take care of the resident. We have to clean. We have to be considerate

39
00:06:37,320 --> 00:06:50,800
of each other, replacing the toilet paper on the holder, not staying in the shower for

40
00:06:50,800 --> 00:06:56,640
long, long time, cleaning up after ourselves, cleaning up after each other. There's a lot

41
00:06:56,640 --> 00:07:04,240
of friendliness involved there, but certainly living in the world. And as a Buddhist, this

42
00:07:04,240 --> 00:07:10,400
is something for us to keep in mind, even as a beginner meditator or someone who has just

43
00:07:10,400 --> 00:07:14,960
started to learn about Buddhism. It's an important part, because it's something that

44
00:07:14,960 --> 00:07:22,600
protects you and it provides you with a sort of a guiding principle. So you know that

45
00:07:22,600 --> 00:07:25,480
when you're angry at someone, you know that there's something wrong with that, that's

46
00:07:25,480 --> 00:07:31,000
not really where we're headed. When you're trying to be friendly, you can have a sense of

47
00:07:31,000 --> 00:07:39,240
how friendly am I towards others? It can be a good way of discerning your commitment and your

48
00:07:39,240 --> 00:07:45,920
progress and your success on the path. Also, it's a good way to protect you and keep you

49
00:07:45,920 --> 00:07:51,280
on the path. Thinking that's not good, I better not be. I'm not saying those nasty things

50
00:07:51,280 --> 00:08:01,480
about others or do those nasty things to others. Being compassionate, not wishing for others

51
00:08:01,480 --> 00:08:08,640
to suffer, being joyful, trying your best to rejoice and seeing how jealous we are, what

52
00:08:08,640 --> 00:08:12,680
other people get good things and saying to yourself, you know that's not right. I should

53
00:08:12,680 --> 00:08:18,120
be happy for them. A lot of Buddhists do these sorts of things, even those that don't

54
00:08:18,120 --> 00:08:28,520
particularly set themselves in the meditation practice. An equanimity, trying not to be upset,

55
00:08:28,520 --> 00:08:36,960
trying not to be reactionary, or to other people suffering or to their causing you suffering,

56
00:08:36,960 --> 00:08:44,520
or another's harm you to not get upset by that, or another's do bad deeds, to not get

57
00:08:44,520 --> 00:08:54,760
angry, to not hate them, to try and be equanimists above all equanimists, above all remembering

58
00:08:54,760 --> 00:09:05,480
that reality is what it is. You're getting upset about it or attached to it, isn't

59
00:09:05,480 --> 00:09:15,600
it actually? Anyone's benefit? So that's how we, in a conventional sense, how we put

60
00:09:15,600 --> 00:09:20,960
them into practice. Of course, then there's the practice, and it's the meditative sense.

61
00:09:20,960 --> 00:09:27,360
You can actually meditate on these. Tomorrow, I'm asked to do a short, loving, kindness

62
00:09:27,360 --> 00:09:32,040
meditation. It's actually why I thought of this because they're having this thing called

63
00:09:32,040 --> 00:09:37,560
Buddha Day, which I'm not really sure what it means. Once a year, they've got this Buddha

64
00:09:37,560 --> 00:09:48,480
Day thing. Tomorrow's the day of the year. They want, if I was asked to take part and

65
00:09:48,480 --> 00:09:58,200
do a short, loving, kindness meditation. But you can take this up as a meditation practice,

66
00:09:58,200 --> 00:10:07,120
and then it really does lead to the Brahma realms. It does lead to the God realms. You become

67
00:10:07,120 --> 00:10:18,080
Godlike or godly or God, a God. When you engage, when you fix your mind on the quality

68
00:10:18,080 --> 00:10:23,480
of love, you fix your mind on the quality of compassion and there are ways of developing

69
00:10:23,480 --> 00:10:30,520
this. You spend days and weeks and months to the point where it's all of you and you

70
00:10:30,520 --> 00:10:44,920
have unlimited love and compassion and joy and equanimity towards all beings. But I think

71
00:10:44,920 --> 00:10:50,240
the most important way for us to think about these four things is not really in this sense

72
00:10:50,240 --> 00:10:58,120
at all. It's more in the sense that our practice of insight meditation helps us or should

73
00:10:58,120 --> 00:11:10,920
lead us to all four of these. The meditation in many ways is about, I mean, for many of

74
00:11:10,920 --> 00:11:16,400
us, a big part of it is how it's going to affect our lives in a conventional sense because

75
00:11:16,400 --> 00:11:21,000
the existence of beings, of course, is just a convention in our minds. But that's how

76
00:11:21,000 --> 00:11:29,080
we live. We go home and we have to deal with people. And so one of the great benefits of

77
00:11:29,080 --> 00:11:35,280
meditation practice is that it ideally helps you cultivate these or makes these more naturally

78
00:11:35,280 --> 00:11:46,400
a part of who you are because you free yourself from all the things that cause us to be

79
00:11:46,400 --> 00:11:59,920
nasty and mean to others and jealous and partial and causes to get upset. So our default

80
00:11:59,920 --> 00:12:07,120
is to be friendly towards others. I mean, friendliness is just reacting appropriately to

81
00:12:07,120 --> 00:12:19,480
someone who wishes something of you. Maybe your time, maybe a support material support,

82
00:12:19,480 --> 00:12:37,360
maybe psychological mental support, emotional support. And compassion when they're suffering,

83
00:12:37,360 --> 00:12:48,240
when there's a suffering finding ways, it's just natural when someone has the need of being

84
00:12:48,240 --> 00:12:59,320
let out of their suffering, being healed or cured or having their problems fixed and so

85
00:12:59,320 --> 00:13:13,040
on. But it's natural because this is the natural inclination of the mind. When you see

86
00:13:13,040 --> 00:13:24,880
things clearly, when you have an objective, a non-reactionary outlook, the obvious response

87
00:13:24,880 --> 00:13:30,640
to someone who needs your help, wishing for something positive or wishing for something

88
00:13:30,640 --> 00:13:37,400
negative to be removed from their lives, is to help them, and give them what they need

89
00:13:37,400 --> 00:13:54,240
and do to help them out of a bad situation. And so rather than really focusing all our

90
00:13:54,240 --> 00:14:01,680
time, which a lot of Buddhists do, I think, on developing these, working hard to develop

91
00:14:01,680 --> 00:14:13,200
these, we should work hard to purify our minds of the obstacles to all these things. Because

92
00:14:13,200 --> 00:14:21,720
when your mind is in a natural state, your default is to be kind, to be compassionate,

93
00:14:21,720 --> 00:14:26,200
to rejoice in others, to appreciate others. When someone does something good and they tell

94
00:14:26,200 --> 00:14:32,480
you about it or they express it to you, a natural reaction is to say that's good. It's

95
00:14:32,480 --> 00:14:40,320
only our sense of low self-worth that makes us jealous and makes us upset when others

96
00:14:40,320 --> 00:14:51,600
get good things. And of course, the natural outcome is equanimity. These are really good

97
00:14:51,600 --> 00:14:59,040
indicators of where we're going in a conventional sense. What's kind of a person is a meditator.

98
00:14:59,040 --> 00:15:10,000
What kind of a person are they? A person who has all four of these. These are qualities

99
00:15:10,000 --> 00:15:19,480
that protect our practice, but they're also qualities that come from our practice. A person

100
00:15:19,480 --> 00:15:37,240
who practices insight meditation, being mindful of experience, seeing things as they are,

101
00:15:37,240 --> 00:15:50,400
is naturally inclined to be a good person, naturally inclined to be a good friend. It's

102
00:15:50,400 --> 00:15:55,560
interesting to watch how many meditators after or hear about how many meditators after

103
00:15:55,560 --> 00:16:02,720
they do an intensive course and go home, have a real hard time fitting in. They find themselves

104
00:16:02,720 --> 00:16:08,400
struggling and they feel like meditation is actually made them a bit of a pariah or a

105
00:16:08,400 --> 00:16:16,440
social misfit, a social misfits, probably the best term. And they question this sort

106
00:16:16,440 --> 00:16:25,520
of thing and they question the benefits of it, perhaps, or they struggle to see what

107
00:16:25,520 --> 00:16:31,520
good it has been to them. One in fact, it seems to have caused a lot of stress. If a meditator

108
00:16:31,520 --> 00:16:37,760
has really had good results, this sort of doubt is not something that cuts deep, but certainly

109
00:16:37,760 --> 00:16:44,000
it cuts deep in all those who expect them to behave the way they used to behave and find

110
00:16:44,000 --> 00:17:02,560
them different, find them perhaps aloof or unresponsive, unattentive, unfriendly, even. And

111
00:17:02,560 --> 00:17:12,760
I think it's important to look at, to understand this, in this sense, to be strong in

112
00:17:12,760 --> 00:17:18,080
the results that you've gained and to stand strong. Because in fact, what you're getting

113
00:17:18,080 --> 00:17:22,800
when you go home is a whole bunch of people who don't have friendliness, who don't have

114
00:17:22,800 --> 00:17:32,080
compassion, who are used to clinging and used to crying and suffering and getting angry

115
00:17:32,080 --> 00:17:39,680
and being jealous and manipulating each other. And when you don't play along, they'll turn

116
00:17:39,680 --> 00:17:48,760
on you. It can it happens. People ordinary people, they can't take change and they certainly

117
00:17:48,760 --> 00:17:58,000
can't take people who are not willing to cling. When everyone around you is asking you

118
00:17:58,000 --> 00:18:05,400
to get excited, hey, let's go party. I got a promotion, let's go party. No, I'd rather

119
00:18:05,400 --> 00:18:13,440
just stay home. Good for you though. Make someone very angry, right? Because they expect

120
00:18:13,440 --> 00:18:17,840
you to be festive. They expect that when a good thing comes, it's not enough to just

121
00:18:17,840 --> 00:18:29,280
appreciate it. We should somehow use it as a cause to go and indulge in senseless enjoyment

122
00:18:29,280 --> 00:18:44,600
of central pleasures. So I mean, it's perhaps part of what we have to understand is that

123
00:18:44,600 --> 00:18:53,160
these are special states. It's not the ordinary states. It's not states that allow you

124
00:18:53,160 --> 00:19:00,880
to fit in with society. Society is not built on these foundations. I mean, some societies

125
00:19:00,880 --> 00:19:10,760
somewhere might theoretically be, but it's not something you find as the general state

126
00:19:10,760 --> 00:19:21,160
of affairs. Mostly society is built on competition, built on manipulation, built on attachment,

127
00:19:21,160 --> 00:19:32,560
clinging. So, meditation makes you as sort of a special individual, much more impervious

128
00:19:32,560 --> 00:19:40,400
to the sorts of suffering that go on, but much less able to engage in a system that's

129
00:19:40,400 --> 00:19:49,680
based very much on the sorts of things that we're moving away from, built on stress and

130
00:19:49,680 --> 00:20:01,280
suffering, really, the causes of stress and suffering. And so the Brahmoi Haras are really

131
00:20:01,280 --> 00:20:07,840
a state of greatness. They can be cultivated artificially by themselves and directly working

132
00:20:07,840 --> 00:20:16,960
on them or they can come from stem from greatness of enlightenment, the greatness of clarity

133
00:20:16,960 --> 00:20:24,160
of mind that comes from insight meditation. So it's useful to think about, I think, that's

134
00:20:24,160 --> 00:20:35,440
your demo for tonight. Thank you all for tuning in. And I think I'm going to have to put

135
00:20:35,440 --> 00:20:44,200
a pause on these. I'm going to have to stop broadcasting for a while. I've got a period

136
00:20:44,200 --> 00:20:52,840
of time coming up where I have other commitments. So I think next broadcast will be a

137
00:20:52,840 --> 00:21:04,600
week today on April 18th. April 18th, I should be back and they will have to figure out

138
00:21:04,600 --> 00:21:11,920
what my new schedule is going to be like. Meanwhile, if there are any questions tonight,

139
00:21:11,920 --> 00:21:23,560
please go and post them on our meditation page. How does one go about dealing with very

140
00:21:23,560 --> 00:21:29,240
talkative people such as their parents that they live with and are no longer interested

141
00:21:29,240 --> 00:21:33,880
in the subject matter of what they are talking about that feel rude to say, they are

142
00:21:33,880 --> 00:21:43,360
not interested in what they are talking about. Well, it's one thing to not be interested.

143
00:21:43,360 --> 00:21:54,120
It's another thing to be upset about, right? Because I mean, if you have ever been in the

144
00:21:54,120 --> 00:21:59,840
forest and you hear the birds talking, do you get anger at the birds for what they are

145
00:21:59,840 --> 00:22:12,520
talking about? I remember sitting with Adjantong, my teacher, and I'd sit for him with

146
00:22:12,520 --> 00:22:21,840
five, I'd sit for him, sit with him for five hours a day. Sometimes it was an hour of listening

147
00:22:21,840 --> 00:22:33,760
to some old, non-usually, because the old women would reach women usually, would go to the

148
00:22:33,760 --> 00:22:40,120
monastery as a way of retiring, of leaving home society and Thailand is a little bit,

149
00:22:40,120 --> 00:22:48,600
well, it's quite sexist, prejudiced towards women and has been traditionally. So sometimes

150
00:22:48,600 --> 00:22:53,280
the best place for them to live in their old ages in the monastery. I mean, there's more

151
00:22:53,280 --> 00:22:57,560
to it than that. Many of them are very much devoted to the practice, but some of them

152
00:22:57,560 --> 00:23:05,920
are just, seem to be somewhat devoted to chatting. And so they come and chat with Adjant

153
00:23:05,920 --> 00:23:14,600
for, it seemed like an hour, maybe half an hour, probably not an hour. But he never got

154
00:23:14,600 --> 00:23:22,760
upset, and he never tried to brush them on. I just sat there and listened and nodded sometimes

155
00:23:22,760 --> 00:23:36,960
and smiled sometimes. You have to check yourself and worry about other people. Someone

156
00:23:36,960 --> 00:23:43,000
else is talking, well, that's the experience you have, so them talking. If you don't

157
00:23:43,000 --> 00:24:02,360
like it, if it upsets you, that's also your problem, or that's your problem. Because

158
00:24:02,360 --> 00:24:09,160
I certainly wasn't as happy about all this long chatting going on as my teacher was. It's

159
00:24:09,160 --> 00:24:15,600
much harder for me to sit there and listen to jabbering on about nothing. And that was my

160
00:24:15,600 --> 00:24:26,600
problem. It's quite good practice, in fact. What is the Buddhist view on serendipity? I don't

161
00:24:26,600 --> 00:24:33,040
have really a view. I mean, we can speculate that what we call serendipity is often related

162
00:24:33,040 --> 00:24:41,920
to karma or that sort of thing, but we don't use such words like serendipity. serendipity is

163
00:24:41,920 --> 00:25:00,160
just noticing a pattern, noticing something really that it's something, some sort of coincident

164
00:25:00,160 --> 00:25:12,200
it's almost. What is coincidence is coincident. It's just, it happened that way. And

165
00:25:12,200 --> 00:25:15,800
so many times, in many ways, we're just conditioned to see those sorts of things and be

166
00:25:15,800 --> 00:25:23,160
excited by them. When all the times that there was no coincidence, we didn't notice.

167
00:25:23,160 --> 00:25:30,360
We didn't say, oh, well, that's uncerendipitous, right? I mean, it's uncerendipitous that

168
00:25:30,360 --> 00:25:36,680
you asked that question and I wasn't thinking about it. Or it's uncerendipitous that

169
00:25:36,680 --> 00:25:41,040
I arrived at the bus stop and there was no bus there. But isn't it serendipitous that

170
00:25:41,040 --> 00:25:48,240
sometimes I just get to the bus stop right as the bus is coming. And then I notice

171
00:25:48,240 --> 00:25:54,880
it. Or I was thinking about someone in the call, but many times you think about someone

172
00:25:54,880 --> 00:26:01,920
and they don't call. You know, I mean, so there can be, there can be relationship. There

173
00:26:01,920 --> 00:26:09,880
can be reasons that we don't really understand. But quite often, I think it's just

174
00:26:09,880 --> 00:26:16,640
a coincident, right? At any rate, at any rate, whether there is a cause or not a cause,

175
00:26:16,640 --> 00:26:27,200
whether it's just a random coincident or a caused coincident. It's still just a coincident.

176
00:26:27,200 --> 00:26:33,600
They call the things coincide. And that's how we look at it and put it some, we see what's

177
00:26:33,600 --> 00:26:40,120
happening for what's happening. I don't try and judge it or make stories or connections

178
00:26:40,120 --> 00:26:50,120
about it. We didn't as explained as pleasant, unpleasant and neutral, but can a feeling

179
00:26:50,120 --> 00:26:58,000
be pleasant or unpleasant without liking and disliking. Pleasant or unpleasant. What are

180
00:26:58,000 --> 00:27:02,760
there actually five feelings? And that's where the Abidama separates it out to five because

181
00:27:02,760 --> 00:27:09,760
there can be a painful bodily feeling without disliking. But there cannot be a painful

182
00:27:09,760 --> 00:27:17,440
mental feeling without disliking. On the other hand, there can be a pleasant bodily feeling,

183
00:27:17,440 --> 00:27:29,400
or the bodily feeling itself doesn't arise with liking or disliking. But a pleasant

184
00:27:29,400 --> 00:27:35,960
mental feeling can arise with liking or without liking. For example, Kuzula, the Kuzula

185
00:27:35,960 --> 00:27:43,000
Jitta's all have all can arise with so many. And there's no liking there. There's just

186
00:27:43,000 --> 00:27:49,560
a pleasant mental feeling involved. It doesn't have to be. It can also be a neutral feeling.

187
00:27:49,560 --> 00:27:57,160
But there is the potential for a wholesome mind to arise with pleasure without any liking

188
00:27:57,160 --> 00:28:05,880
of it. On the other hand, Loba agreed. The Loba Mula Jitta can arise with

189
00:28:05,880 --> 00:28:20,440
pleasure as well. So it's with liking or without liking. But a mental, a mental, unpleasant

190
00:28:20,440 --> 00:28:26,680
feeling, mental displeasure, mental unpleasant feeling, as always with disliking displeasure,

191
00:28:26,680 --> 00:28:40,040
anger, patiga. But a painful physical feeling can be without disliking. Or is I think physical

192
00:28:40,040 --> 00:28:46,160
feelings, because of how the mind, serious, serious works, physical feelings are always

193
00:28:46,160 --> 00:28:52,800
I think without, just technically, because you have to experience it first and then react mentally.

194
00:28:52,800 --> 00:28:59,840
But the physical is never liking. There's no room for liking or disliking at that point, I think.

195
00:28:59,840 --> 00:29:05,840
But in the sequence of experience, of the physical feeling, there will come a moment

196
00:29:05,840 --> 00:29:15,080
in the mind of pleasure or displeasure with liking or disliking or with wholesomeness.

197
00:29:15,080 --> 00:29:26,560
Okay, and that's all the questions. So that's all for the week. I'll be back again next

198
00:29:26,560 --> 00:29:52,240
Tuesday. Thank you all for coming up.

