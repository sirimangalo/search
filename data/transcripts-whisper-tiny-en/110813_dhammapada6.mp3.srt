1
00:00:00,000 --> 00:00:08,280
Hello, today we continue our study of the Dhamapada on with verse number 6, which goes

2
00:00:08,280 --> 00:00:24,360
parachana vijananti, maya mehta, yamamase, yajatatatat, vijananti, tato samanti, mehta gah,

3
00:00:24,360 --> 00:00:35,920
which means there are some who don't see clearly that we will all have to die, but those

4
00:00:35,920 --> 00:00:49,320
who do see this clearly, yajatatatat, vijananti, for those who do see clearly, because of that,

5
00:00:49,320 --> 00:01:00,480
they settle their quarrels, or settle their disputes.

6
00:01:00,480 --> 00:01:07,480
So people who don't see clearly or don't fully understand, yajananti, many people

7
00:01:07,480 --> 00:01:14,120
don't see this, for those who see it, see clearly that we all have to die, that nothing

8
00:01:14,120 --> 00:01:21,400
is permanent, and they're able to settle their quarrels when they realize this truth.

9
00:01:21,400 --> 00:01:28,320
This was told in regards to one of my personal favorites, which is kind of odd, but it's

10
00:01:28,320 --> 00:01:35,840
an interesting monk story, it's about a quarrel that arose between two monks who lived

11
00:01:35,840 --> 00:01:40,680
in Ghosidarama, a monastery in the Buddhist time.

12
00:01:40,680 --> 00:01:45,600
Ghosidarama, there were two monks, one was very famous for teaching the Dhamma, which is

13
00:01:45,600 --> 00:01:52,760
the meditation practice, and the things that the Buddha taught us to do, we should do.

14
00:01:52,760 --> 00:01:57,160
And another one was very famous for the Vinaya, the Buddhist teaching on the monks' rules

15
00:01:57,160 --> 00:02:01,360
and the things that one shouldn't do.

16
00:02:01,360 --> 00:02:07,640
And so they were both very famous, they each had a group of very large groups of students.

17
00:02:07,640 --> 00:02:13,120
And one day, one of the Dhamma was coming out of the washroom, and the one who taught

18
00:02:13,120 --> 00:02:17,920
the Vinaya was going into the washroom, and the one to the Vinaya looked and saw that the

19
00:02:17,920 --> 00:02:28,160
one who teaches the Dhamma had left some water in the bucket in the washroom, which you're

20
00:02:28,160 --> 00:02:33,120
not supposed to do because mosquitoes will lay their eggs.

21
00:02:33,120 --> 00:02:36,920
And so he said, hey, that's an offense, you know, to do that.

22
00:02:36,920 --> 00:02:42,080
And he said, oh, I'm sorry, I didn't realize, then please let me confess it to you.

23
00:02:42,080 --> 00:02:48,840
When there's an offense against the rules, then you have to confess and say, you know,

24
00:02:48,840 --> 00:02:53,880
promise that you will try your best not to do it again, and just make it clear that you

25
00:02:53,880 --> 00:02:56,600
are aware of the fact that you did it.

26
00:02:56,600 --> 00:03:03,120
And the one who taught the Vinaya said, but if you didn't realize it was a, or if you

27
00:03:03,120 --> 00:03:09,760
didn't do it intentionally, then it's not an offense.

28
00:03:09,760 --> 00:03:13,840
And the other monks said, OK, then, and he left without confessing it.

29
00:03:13,840 --> 00:03:19,000
And the one who sat the Vinaya, the Vinaya went around telling all his students that this

30
00:03:19,000 --> 00:03:22,480
other monk had an offense that he hadn't confessed.

31
00:03:22,480 --> 00:03:28,760
And so they went and told the students of the other teacher that this, this, this, this

32
00:03:28,760 --> 00:03:33,000
story and those monks told the teacher and the teacher said, wow, first he tells me it's

33
00:03:33,000 --> 00:03:34,000
not an offense.

34
00:03:34,000 --> 00:03:39,240
Now he says it is an offense, that, that monks a liar, and so these, his students heard

35
00:03:39,240 --> 00:03:43,520
this and they went and told the other students, say, your teacher's a liar.

36
00:03:43,520 --> 00:03:49,080
And then they told their teacher and the teacher got more angry and said nasty things

37
00:03:49,080 --> 00:03:53,720
until finally they were split up into two different groups.

38
00:03:53,720 --> 00:03:59,760
As a result of that, the, the novices and the, the bikunis, the female monks were split into

39
00:03:59,760 --> 00:04:05,000
two different groups as well, the lay people who supported the monks split up into two groups,

40
00:04:05,000 --> 00:04:10,160
the angels who guarded the monastery split up into two groups, the angels that were their

41
00:04:10,160 --> 00:04:14,560
friends who lived in the, in the sky and in, in the various heavens, they all split

42
00:04:14,560 --> 00:04:15,560
up into two groups.

43
00:04:15,560 --> 00:04:21,080
And the story goes that this, this part of the universe split up all the way up into

44
00:04:21,080 --> 00:04:28,720
the Brahma or the god realms and the gods themselves were, were split, believe it or not.

45
00:04:28,720 --> 00:04:32,720
There were some water being left in a bucket.

46
00:04:32,720 --> 00:04:39,440
Now the story goes on and on about how the Buddha tried to convince them to, to change

47
00:04:39,440 --> 00:04:44,320
their ways and they wouldn't listen and actually they said to him, oh, you would, please

48
00:04:44,320 --> 00:04:48,240
don't, don't bother yourself, we will take care of this on our own and they wouldn't

49
00:04:48,240 --> 00:04:53,040
let the Buddha even get involved and so the Buddha left and went and stayed and there's

50
00:04:53,040 --> 00:04:57,000
a story, there's a whole side story about how the Buddha stayed in the forest with an

51
00:04:57,000 --> 00:05:03,720
elephant and the elephant even food and took care of him and then after that he came back

52
00:05:03,720 --> 00:05:09,680
and then at the end he was, he, he got all these monks, what happens right, the best part

53
00:05:09,680 --> 00:05:14,000
of the, when the Buddha left, all of the lay people said, well, where's the Buddha

54
00:05:14,000 --> 00:05:15,000
gone?

55
00:05:15,000 --> 00:05:18,920
They said, oh, we, we told them not to get involved with our quarrel and so he, he left

56
00:05:18,920 --> 00:05:23,680
and all the lay people got very angry and upset and said, how could you, you know, now

57
00:05:23,680 --> 00:05:28,720
the, we don't have a, they put the opportunity to see the Buddha because of you and so

58
00:05:28,720 --> 00:05:33,800
they stopped giving food to the monks because of that the monks were at a very hard time

59
00:05:33,800 --> 00:05:40,880
eating, getting arms food and just surviving and they, they, they went and they apologize

60
00:05:40,880 --> 00:05:44,720
and the people said, well, you have to get on a apology from the, get forgiveness from

61
00:05:44,720 --> 00:05:49,080
the Buddha and because it was the rain season, they couldn't do that so they had the

62
00:05:49,080 --> 00:05:53,520
way three months and they were very, very hard up for three months.

63
00:05:53,520 --> 00:05:59,760
And after this, the Buddha came back and then he put all the, put the monks who were

64
00:05:59,760 --> 00:06:05,040
quarreling in a special place and didn't let any of the other monks get involved with them.

65
00:06:05,040 --> 00:06:07,400
When people came and asked, where are those quarreling monks, the Buddha would always

66
00:06:07,400 --> 00:06:10,240
point to them and say, there they are, there they are.

67
00:06:10,240 --> 00:06:14,160
And they were so ashamed that eventually they came and, and asked forgiveness from the

68
00:06:14,160 --> 00:06:17,720
Buddha and found really ashamed of what they had done.

69
00:06:17,720 --> 00:06:20,560
And that's when the Buddha told this verse.

70
00:06:20,560 --> 00:06:27,320
Now the verse has, I think, two important lessons that we can draw from it and the first

71
00:06:27,320 --> 00:06:32,120
one is in regards to death, which I sort of went into last time, but I'd like to talk

72
00:06:32,120 --> 00:06:33,120
a little bit more.

73
00:06:33,120 --> 00:06:40,200
The second one is about the importance of wisdom and understanding and how understanding

74
00:06:40,200 --> 00:06:47,720
is really the key in overcoming quarrels and overcoming all suffering and all unhostments

75
00:06:47,720 --> 00:06:49,840
all evil.

76
00:06:49,840 --> 00:07:00,760
So first in regards to death, it's interesting how the theory or the ideology, the teaching

77
00:07:00,760 --> 00:07:07,680
on rebirth and how the mind continues, actually makes death a more of an important event

78
00:07:07,680 --> 00:07:12,960
and this verse kind of makes that clear in a way that we might not think of the Buddha's

79
00:07:12,960 --> 00:07:14,600
telling us how important death is.

80
00:07:14,600 --> 00:07:19,160
So may you think, well, if the mind continues, then how is it really so important?

81
00:07:19,160 --> 00:07:24,480
And the way it works is, is this, the body and the mind have two, they work together,

82
00:07:24,480 --> 00:07:28,160
but they have two different, two characteristics, different characteristics.

83
00:07:28,160 --> 00:07:29,440
The mind is very quick.

84
00:07:29,440 --> 00:07:35,120
The mind in one moment can be here, the next moment can be there, it can arise and move very

85
00:07:35,120 --> 00:07:37,480
quickly from one object to the next.

86
00:07:37,480 --> 00:07:44,440
And the body on the other hand is very fixed and so our state of being in this life on

87
00:07:44,440 --> 00:07:49,560
a physical level is quite fixed, but on a mental level it's quite fluid.

88
00:07:49,560 --> 00:07:54,840
So your mind can change and if your mind becomes more and more corrupt, you can do that

89
00:07:54,840 --> 00:08:02,160
very quickly and it can change from good to evil or from evil to good relatively quickly.

90
00:08:02,160 --> 00:08:07,880
And there can be quite a disparity therefore between the physical realm and the mental realm.

91
00:08:07,880 --> 00:08:15,960
You can be surrounded by good things and get lots of good and wonderful pleasures and luxuries

92
00:08:15,960 --> 00:08:20,760
and have a very corrupt and evil mind that one would think this person doesn't deserve

93
00:08:20,760 --> 00:08:23,040
all of that luxury.

94
00:08:23,040 --> 00:08:29,800
But the truth is, this is because of the difference, the body is a course construct

95
00:08:29,800 --> 00:08:38,320
and the amount of energy that is involved in simply the creation of a human being or of

96
00:08:38,320 --> 00:08:47,400
a physical construct is immense and so it has a great power, this power of inertia, where

97
00:08:47,400 --> 00:08:49,480
you can't just stop that inertia.

98
00:08:49,480 --> 00:08:55,160
Now some people if they do really bad things or really good things, they might wind up

99
00:08:55,160 --> 00:08:57,440
cutting it off by dying.

100
00:08:57,440 --> 00:09:03,760
You hear about really good people who are killed because of their unwillingness to do

101
00:09:03,760 --> 00:09:09,800
evil and so on or die because they're not willing to do evil in order to live on and so on.

102
00:09:09,800 --> 00:09:16,000
And you of course hear about evil people who do bad things and as a result are killed

103
00:09:16,000 --> 00:09:20,920
or dying unpleasant circumstances because of their way their minds are.

104
00:09:20,920 --> 00:09:27,480
But for most people, for many people the inertia and the power of the physical prevents

105
00:09:27,480 --> 00:09:32,120
the repercussions until the moment of death.

106
00:09:32,120 --> 00:09:35,680
Now at the moment of death the body is totally reconstructed.

107
00:09:35,680 --> 00:09:40,840
There is a period that goes where the mind leaves the body, leaves the body behind and

108
00:09:40,840 --> 00:09:50,120
begins this immense activity or work required to rebuild another body or to create another

109
00:09:50,120 --> 00:09:55,400
human being starting at the moment of conception.

110
00:09:55,400 --> 00:10:01,520
Now that of course is much more mental and the nature of that is going to depend almost

111
00:10:01,520 --> 00:10:06,040
entirely on the nature of the mind or the set of the mind.

112
00:10:06,040 --> 00:10:08,920
What sort of work is the mind going to undertake?

113
00:10:08,920 --> 00:10:14,200
For most people we're caught up in this programming of creating a human being in a human

114
00:10:14,200 --> 00:10:18,960
life and so as a result will be reborn as a human being but you can be reborn in many

115
00:10:18,960 --> 00:10:25,920
different ways based on your mind and therefore the importance of, as we've seen in the

116
00:10:25,920 --> 00:10:32,240
past stories, the importance of settling our disputes in this life and settling them in

117
00:10:32,240 --> 00:10:34,680
our mind anyway.

118
00:10:34,680 --> 00:10:42,000
Sometimes we can't stop other people from being angry at us but most of greatest importance

119
00:10:42,000 --> 00:10:47,800
is that we are able to settle them in our own minds and this is the word semanti,

120
00:10:47,800 --> 00:10:56,240
semanti, semanti which means to stabilize or to tranquilize them in our minds to make it

121
00:10:56,240 --> 00:11:02,200
so that they no longer, they're no longer a source of clinging, we no longer cling, we

122
00:11:02,200 --> 00:11:12,560
no longer regurgitate and go after and recreate this in our minds.

123
00:11:12,560 --> 00:11:19,680
So the case, the problem in the case with these two sets of monks which is a real danger

124
00:11:19,680 --> 00:11:26,520
and so the Buddha pointed out to them that this is really useless, this is really futile

125
00:11:26,520 --> 00:11:31,680
and the reason people do this is because they don't see, they don't see clearly so this

126
00:11:31,680 --> 00:11:38,240
is the importance of death because it frees the mind to make choices again on a level that

127
00:11:38,240 --> 00:11:44,120
it's not free to in this life because of the inertia of its previous work and creating

128
00:11:44,120 --> 00:11:47,600
this being.

129
00:11:47,600 --> 00:11:55,640
And the second part is that importance of understanding and how understanding things like

130
00:11:55,640 --> 00:12:05,080
death is important on a meditative level, understanding the truth of birth and death as occurring

131
00:12:05,080 --> 00:12:15,080
on a momentary level is far more important and crucial in fact in freeing ourselves up

132
00:12:15,080 --> 00:12:21,560
from strife and quarrel and disputes and suffering.

133
00:12:21,560 --> 00:12:28,120
And I've said it before but it's worth reiterating that this is really all you need to

134
00:12:28,120 --> 00:12:33,520
do to overcome your suffering to overcome quarrels and so on, you don't have to go

135
00:12:33,520 --> 00:12:40,360
to apologize to the people, I mean that's always a good way to overcome it but in a general

136
00:12:40,360 --> 00:12:45,640
level in regards to our clings and regards to our cravings and regards to our attachment,

137
00:12:45,640 --> 00:12:49,280
all that needs to be done is to see it clearly.

138
00:12:49,280 --> 00:12:53,560
When you're addicted to something, you don't have to force yourself to stop or try to

139
00:12:53,560 --> 00:12:57,760
convince yourself that it's wrong or remind yourself again and again how bad you are for

140
00:12:57,760 --> 00:12:58,760
doing it.

141
00:12:58,760 --> 00:13:03,240
All you have to do is see the addiction clearly for what it is, see it objectively.

142
00:13:03,240 --> 00:13:08,000
Let it be, let it come up and when you see it for what it is, you'll let go of it.

143
00:13:08,000 --> 00:13:12,360
You'll have no desire to cling to it because you'll see that everything ceases.

144
00:13:12,360 --> 00:13:16,840
So this Buddha's teaching on death was only the introduction, it's this reminder to bring

145
00:13:16,840 --> 00:13:21,560
the monks back to this idea that everything arises and ceases because once they can see

146
00:13:21,560 --> 00:13:24,520
that we die, they can see how futile it is.

147
00:13:24,520 --> 00:13:25,520
Why are we fighting?

148
00:13:25,520 --> 00:13:26,520
What are we hoping to gain?

149
00:13:26,520 --> 00:13:32,400
We can come out on top as I said in the last video but you know what does it mean to

150
00:13:32,400 --> 00:13:40,480
be on top because eventually we all die and we're put on bottom.

151
00:13:40,480 --> 00:13:43,480
And once they start to do that then you can start to see that really why would we cling

152
00:13:43,480 --> 00:13:47,440
to anything and you start to be able to look at your clinging and see that when you cling

153
00:13:47,440 --> 00:13:51,960
to something you hold on to it it maybe brings you pleasure temporarily but then it

154
00:13:51,960 --> 00:13:59,240
disappears and what is the benefit, what is the gain that comes from that.

155
00:13:59,240 --> 00:14:05,280
When you see on a phenomenological level, experiential or moment to moment level how everything

156
00:14:05,280 --> 00:14:11,000
arises and ceases how this happiness, this pleasure that you're hoping to get is really

157
00:14:11,000 --> 00:14:18,520
actually bound up with suffering and stress because it conditions the mind to want more

158
00:14:18,520 --> 00:14:28,280
and to strive more and to be stressed and you can make yourself sick and uncomfortable.

159
00:14:28,280 --> 00:14:33,720
And well as a result of the clinging.

160
00:14:33,720 --> 00:14:39,120
So when you practice meditation when you look at the objects of awareness on a moment

161
00:14:39,120 --> 00:14:46,080
to moment basis you're able to see that everything arises and ceases.

162
00:14:46,080 --> 00:14:51,600
Simply that seeing allows you to you know it changes your whole way of looking at things

163
00:14:51,600 --> 00:14:56,960
you no longer look at things in terms of the universe in terms of us and them and I and

164
00:14:56,960 --> 00:15:04,080
me and mine and so on all of the so many of the points of reference that we we take

165
00:15:04,080 --> 00:15:17,000
in on an ordinary run of the meal frame of reference are gone or lost or given up by someone

166
00:15:17,000 --> 00:15:21,400
who takes up this practice you really change your whole way of looking at the world.

167
00:15:21,400 --> 00:15:26,240
And this is I think it's important to pick up this word in this verse.

168
00:15:26,240 --> 00:15:31,040
We John and the which is really the same word as we pass in that we pass in a means to

169
00:15:31,040 --> 00:15:36,040
see clearly we John and the means to know clearly or to understand clearly but it's really

170
00:15:36,040 --> 00:15:41,800
the same word and it has the exact same meaning and it's really the core of Buddhism

171
00:15:41,800 --> 00:15:45,840
and it's so easy when you read this verse to just skip by that and say oh yeah yeah we have

172
00:15:45,840 --> 00:15:48,600
to think about death and that's the most important.

173
00:15:48,600 --> 00:15:55,480
No the most important is the knowing the understanding when you see with death as an example

174
00:15:55,480 --> 00:16:00,600
when you see that everything ceases when you when you see you know our quarrel why are

175
00:16:00,600 --> 00:16:07,000
we quarreling or we have to die it helps you see that everything ceases when you see

176
00:16:07,000 --> 00:16:10,760
on a momentary level and I'm clinging to this thing why would I cling to it when it has

177
00:16:10,760 --> 00:16:15,200
to cease when you're able to see things arising and ceasing this is what the meaning is

178
00:16:15,200 --> 00:16:21,280
when you're able to understand clearly that there is nothing in the world that you can hold

179
00:16:21,280 --> 00:16:29,680
on to that lasts more than an instant when you're able to see that it's futile to cling

180
00:16:29,680 --> 00:16:36,440
and it's useless it's unbeneficial and actually it's a source of great stress and suffering

181
00:16:36,440 --> 00:16:42,480
by its very nature by the stress caused from clinging from wanting from craving and so on

182
00:16:42,480 --> 00:16:51,520
and the the the building and building up of more and more craving and suffering when you

183
00:16:51,520 --> 00:16:57,080
don't get what you want and so on so this is another important verse and another

184
00:16:57,080 --> 00:17:03,200
good lesson for us this is the Dhamma for today from the Dhamma Panda so thanks for

185
00:17:03,200 --> 00:17:19,320
tuning in all the best.

