1
00:00:00,000 --> 00:00:06,320
Good evening, everyone broadcasting live November 1st, 2015

2
00:00:10,040 --> 00:00:12,040
and

3
00:00:17,280 --> 00:00:21,440
Non-damepada nights have now taken on a bit of a

4
00:00:21,440 --> 00:00:31,360
And the word is a power. They're less colorful, less lively, but sure what we're

5
00:00:31,360 --> 00:00:36,800
going to do now that I've decided not to do them above every night. Maybe we won't do

6
00:00:36,800 --> 00:00:44,520
anything. Maybe I'll say hello. Thank you all for practicing. I guess I can take

7
00:00:44,520 --> 00:00:50,640
the time now to talk about our online courses. It's not like there's any good

8
00:00:50,640 --> 00:00:59,400
in advertising them unless I start to get a little bit more vigilant. We'll see.

9
00:00:59,400 --> 00:01:06,440
Actually, it's not really about that. There's about 28 slots and 24 of them

10
00:01:06,440 --> 00:01:16,920
have been claimed so far right now. And that goes up and down, but 24 is about as high

11
00:01:16,920 --> 00:01:25,920
as we've gotten and pretty much everyone comes out as expected and is practicing

12
00:01:25,920 --> 00:01:33,920
diligently at least an hour or two a day. So that's really been, it's become sort of the

13
00:01:33,920 --> 00:01:44,280
highlight for me. It's a real focus because the biggest problem that I've had in sort

14
00:01:44,280 --> 00:01:51,000
of sharing this meditation technique is that I've always felt unable to share more than

15
00:01:51,000 --> 00:01:57,360
the first step. So everyone's been thinking, well, this is it. This is the meditation that

16
00:01:57,360 --> 00:02:01,000
I'm supposed to do for the rest of my life. If I follow this tradition, it's just stepping

17
00:02:01,000 --> 00:02:06,880
right, stepping left, rising, falling. It's a very simple technique, but there's many

18
00:02:06,880 --> 00:02:12,000
of the people who have started taking the course have found it gets quite complex actually.

19
00:02:12,000 --> 00:02:17,760
It's quite involved. And there's quite a bit to it, not just the adding of extra elements

20
00:02:17,760 --> 00:02:25,480
to the practice, but there's also the interview process. And there's even more there's

21
00:02:25,480 --> 00:02:31,160
the next step which will be having people come to do intensive courses, which of course

22
00:02:31,160 --> 00:02:37,520
is the best, but also the most difficult to arrange for us to arrange and for the individuals

23
00:02:37,520 --> 00:02:43,000
to arrange to actually get out here. But at least what we're doing is, well, besides

24
00:02:43,000 --> 00:02:50,000
giving an introduction to the practice, we're also reducing the amount of time that

25
00:02:50,000 --> 00:02:56,760
you would have to spend practicing intensively to finish the course. So my idea was that

26
00:02:56,760 --> 00:03:03,960
if you finish, if you do this for a while, we could finish the foundation course intensively

27
00:03:03,960 --> 00:03:12,480
in about a week. And to get, so to get through, to get that far would probably take

28
00:03:12,480 --> 00:03:24,680
12 to 14 weeks, maybe more, maybe more. So you'd finish most of the, you'd get all the

29
00:03:24,680 --> 00:03:31,120
technique down and then you'd come here and put it into practice intensively for about

30
00:03:31,120 --> 00:03:40,640
a week and you could pretty easily finish the course. So there's still four slots available.

31
00:03:40,640 --> 00:03:46,080
If someone wants to take Tuesday morning or Thursday afternoon or there's one on Saturday

32
00:03:46,080 --> 00:03:53,840
morning, that's now available. Apart from that, we have 24 people to make it just great.

33
00:03:53,840 --> 00:04:01,360
And you have to keep track of everyone. I don't know. I just asked them every time. What

34
00:04:01,360 --> 00:04:09,880
did I give you last time? I don't keep track of anything that you're lying. No heavens.

35
00:04:09,880 --> 00:04:15,760
Has anyone else done this that you know of in this tradition to hold online courses or is

36
00:04:15,760 --> 00:04:22,680
this kind of groundbreaking? In this tradition, I think it's new. But our tradition isn't

37
00:04:22,680 --> 00:04:32,320
all that huge. I don't know another traditions. It's not something that I think is very well

38
00:04:32,320 --> 00:04:37,840
set up, very commonly set up where you'd have this kind of online system. I do know that

39
00:04:37,840 --> 00:04:42,120
people have done right. So if you're just talking about the having online courses, I do

40
00:04:42,120 --> 00:04:47,520
know people even in this tradition, another monk under my teacher is probably the one who

41
00:04:47,520 --> 00:04:52,760
gave me this idea because he does this or he's done this for a long time where people would

42
00:04:52,760 --> 00:04:57,280
meet, we would talk to him once a week and he'd leave them through the course. It's from

43
00:04:57,280 --> 00:05:07,560
Israel. As far as I know, he still does this. So he could, he'd benefit from this kind

44
00:05:07,560 --> 00:05:12,560
of set up. He can set him up on website. There's a little bit of tension, you know, because

45
00:05:12,560 --> 00:05:17,680
the whole international group of people that people I started with are not, they're not

46
00:05:17,680 --> 00:05:24,480
comfortable working with me because I had a falling out with their head guy many years

47
00:05:24,480 --> 00:05:40,880
ago. I think there was a question. Are you able to get on the site?

48
00:05:40,880 --> 00:05:47,880
No, it's coming up on my phone, just not on my PC, but it's just one of those days technologically.

49
00:05:47,880 --> 00:05:54,800
That's strange. It is, yes, especially. Maybe there's a cable cut between Connecticut

50
00:05:54,800 --> 00:06:01,080
and Ontario. No, the hangouts coming in just fine. And all the other websites are coming

51
00:06:01,080 --> 00:06:02,080
up just fine.

52
00:06:02,080 --> 00:06:08,000
We're going through Google that one. Oh, no, this is in Pennsylvania, the servers in Pennsylvania.

53
00:06:08,000 --> 00:06:11,720
That's just a question of where the server is. There might be a problem.

54
00:06:11,720 --> 00:06:21,320
But it's coming in on the phone. Perfect, like, I have found a Buddhist gathering in

55
00:06:21,320 --> 00:06:26,320
my town. They tend to meditate twice a week there. I'd like to go, but they practice

56
00:06:26,320 --> 00:06:31,240
karma, a meditation. And I'm not too enthusiastic about it. What do you think is it worth

57
00:06:31,240 --> 00:06:32,240
going?

58
00:06:32,240 --> 00:06:38,480
The problem we have here is no one hears. If you're listening to the audio, you don't hear

59
00:06:38,480 --> 00:06:42,560
a robin. So, just talking to me.

60
00:06:42,560 --> 00:06:47,040
Okay. I'm reading the question on the chat panel. So, you'll have to read it there.

61
00:06:47,040 --> 00:06:53,720
Yeah, I know. I saw it. So, there's a group practicing karma,

62
00:06:53,720 --> 00:07:03,160
palm meditation, not too enthusiastic about it. So, it's worth going. I don't know anything

63
00:07:03,160 --> 00:07:07,920
about karma, palm meditation. I mean, I can't actually, I don't think this was actually

64
00:07:07,920 --> 00:07:13,080
directed at me. It's kind of, what do you guys think? So, I'm just one of the guys, but

65
00:07:13,080 --> 00:07:19,440
you're one of the guys with a little more informed opinion. So, that's a special to get

66
00:07:19,440 --> 00:07:26,120
an answer for you. I don't have an informed opinion about karma pi. I mean, I guess I

67
00:07:26,120 --> 00:07:30,200
can, I don't really have that much of an informed opinion on going to other meditation

68
00:07:30,200 --> 00:07:35,480
groups. I know that when I have done it, I would just do my own meditation because it's

69
00:07:35,480 --> 00:07:41,320
not like they have thought police that are making sure you do their meditation, but

70
00:07:41,320 --> 00:07:44,960
it can also be a little bit awkward. I remember when I was first starting in this technique,

71
00:07:44,960 --> 00:07:50,520
I would only do this, and I went to a Dharma group, and they did walking meditation in

72
00:07:50,520 --> 00:07:55,240
the circle. And I was kind of rude about it. I just went off into, well, it wasn't rude.

73
00:07:55,240 --> 00:07:59,960
I went off into a corner and did my own walking meditation back and forth. But I think,

74
00:07:59,960 --> 00:08:05,520
you know, if that were to happen today, I certainly wouldn't go off and do my own walking

75
00:08:05,520 --> 00:08:10,360
meditation. I would walk with them in the circle because they walked together, they walked

76
00:08:10,360 --> 00:08:22,000
around the room, in a line, you know. So I wouldn't get as stubborn as I was. But for the

77
00:08:22,000 --> 00:08:25,240
most part, you can do your own meditation, do a little bit of their meditation if you

78
00:08:25,240 --> 00:08:33,320
like. It's nice to go in a group to be in a group, but I think in the end, you might

79
00:08:33,320 --> 00:08:41,080
find it more trouble than it's worth, unless you're like their meditation technique.

80
00:08:41,080 --> 00:08:46,840
Yeah, I've done the same going to different groups, but just done my own, not with the walking.

81
00:08:46,840 --> 00:08:50,720
I do walk in a circle with them, but as far as the sitting, I do my own because as

82
00:08:50,720 --> 00:08:58,520
he said, no one knows what's in your mind. Not no one. Most people don't know what's in

83
00:08:58,520 --> 00:09:04,080
your mind. Never know. You might find someone who can read your mind. Well, then they'll

84
00:09:04,080 --> 00:09:08,400
have probably a look on their face because they'll know that you're not doing the meditation

85
00:09:08,400 --> 00:09:14,760
that they told you to. So you'll be able to tell, I guess.

86
00:09:14,760 --> 00:09:20,600
Hi, Monday. Sometimes when I try to know I get caught in the train of thought and then

87
00:09:20,600 --> 00:09:26,000
I automatically go back to noting my walking or whatever. And then I recognize I was thinking

88
00:09:26,000 --> 00:09:31,400
or fantasizing in the past, maybe five or ten seconds ago. Do I note that or just move

89
00:09:31,400 --> 00:09:37,840
on and try to catch current things arising?

90
00:09:37,840 --> 00:09:41,440
Trying to know that I get caught in the train of thought and then I automatically go back

91
00:09:41,440 --> 00:09:50,320
to noting my walking or whatever. Then recognize I was thinking, I see. Yeah, I mean, you

92
00:09:50,320 --> 00:09:57,560
shouldn't automatically go back to noting walking. You should try to catch the thought

93
00:09:57,560 --> 00:10:07,960
and say thinking. You should stop walking and say thinking thinking. Or you can just ignore

94
00:10:07,960 --> 00:10:12,760
it. And again, if you recognize it, you can. I mean, walking, you can, to some extent,

95
00:10:12,760 --> 00:10:19,680
just leave it alone. With all, keep it simpler than worrying about this just in general.

96
00:10:19,680 --> 00:10:25,000
If anyone thing is taking you away significantly from the walking, then that's when you'd

97
00:10:25,000 --> 00:10:32,400
want to really definitely stop and start acknowledging it. But to some extent, and so judgment

98
00:10:32,400 --> 00:10:37,080
call on your part, you can do it either way. You don't have to stop to note every single

99
00:10:37,080 --> 00:10:42,320
thing, but normally would stop and put our feet together and then say thinking, thinking

100
00:10:42,320 --> 00:10:57,320
we're distracted, distracted.

101
00:10:57,320 --> 00:11:22,400
All right. Well, originally I just intended this to be a sort of come online and say hello

102
00:11:22,400 --> 00:11:29,920
kind of thing. So we don't have to sit around here for long periods of time. It's just a

103
00:11:29,920 --> 00:11:39,440
way to connect with people. Here we have somebody. Please put a question tag at the beginning

104
00:11:39,440 --> 00:11:48,080
of your posts like Patrick O'Just did. Sorry, a post with a cue, colon space.

105
00:11:48,080 --> 00:11:54,800
Greetings from Singapore, Bante. Could you kindly explain the differences between awareness,

106
00:11:54,800 --> 00:12:00,360
release and discernment, release, as mentioned in the sutas? Do you think they both lead

107
00:12:00,360 --> 00:12:07,560
to total unbinding? I don't know what awareness, release and discernment, release are.

108
00:12:07,560 --> 00:12:17,360
I think you're talking about Jetowi Muti and Banyai Muti. Jetowi Muti is, it really, it depends

109
00:12:17,360 --> 00:12:24,240
exactly who you're talking to. Jetowi Muti involves summit meditation. Banyai Muti involves

110
00:12:24,240 --> 00:12:29,760
Vipas in meditation. Now you might say that Jetowi Muti is only summit meditation, so it's entering

111
00:12:29,760 --> 00:12:38,480
into the mundane channels. And Banyai Muti is the attainment of Nibana, or you can say

112
00:12:38,480 --> 00:12:48,360
Banyai Muti is the attainment of Nibana without the jhanas, without the mundane jhanas.

113
00:12:48,360 --> 00:12:57,000
And Jetowi Muti is the attainment of Nibana, with also having attained the summit of jhanas.

114
00:12:57,000 --> 00:13:03,360
So there was a group of monks in the time of the Buddha who, another monk who became

115
00:13:03,360 --> 00:13:08,720
our hands, and the monk came up to them and asked them, said, oh, so do you have all these magical powers and

116
00:13:08,720 --> 00:13:13,520
have you attained the formless jhanas and so on. And they said, no, and they said, they said,

117
00:13:13,520 --> 00:13:20,880
well, how could that be? And they said, well, we're Banyai Muti. We Muti is freedom and Banyai

118
00:13:20,880 --> 00:13:28,160
I guess is discernment. It's wisdom is more common translation. But they're just terms and you

119
00:13:28,160 --> 00:13:34,240
have to see how they're used. So Banyai Muti said to is thought, I think, by the commentaries to

120
00:13:34,240 --> 00:13:38,240
mean that they didn't practice some of the meditation. They only practiced through Bhasa.

121
00:13:39,920 --> 00:13:45,120
And it's curious, it's curious that in that, in that he doesn't ask, and they don't deny having

122
00:13:45,120 --> 00:13:51,680
attained the first four jhanas. And we argued about this, some years back, there was a debate

123
00:13:51,680 --> 00:13:58,320
about what this meant on one of the internet forums back when I was involved with them.

124
00:14:00,800 --> 00:14:04,960
And someone was saying, trying to explain that away and say, well, just because they didn't,

125
00:14:04,960 --> 00:14:08,160
it doesn't mean they actually attained it. But I said, well, honestly, they have to

126
00:14:08,160 --> 00:14:12,000
entertain the first four jhanas because they attained them when they realized Nibana.

127
00:14:12,880 --> 00:14:17,600
So that's why, in my mind, that's the reason why they couldn't ask about that because

128
00:14:17,600 --> 00:14:22,240
absolutely, if you become an orahad, you have to have attained jhanas. It's just a super mundane

129
00:14:22,240 --> 00:14:28,080
jhan. But you don't, you haven't attained, or you don't have to attain the arupa jhanas or magical

130
00:14:28,080 --> 00:14:35,200
powers because those are specific to samata jhanas. So it's a bit of a technical question.

131
00:14:40,080 --> 00:14:43,600
Then a less technical question. Can monks keep juice overnight?

132
00:14:43,600 --> 00:14:51,840
Monks can keep juice until dawn. As soon as dawn arises, the time when you would then be able

133
00:14:51,840 --> 00:14:58,480
to eat solid food, the, except for the opinion is that the juice has to be thrown out,

134
00:14:59,600 --> 00:15:03,760
or used for other purposes, whatever those might be.

135
00:15:05,280 --> 00:15:09,760
So if it's offered to you with your lunch, you can keep it throughout the afternoon and evening.

136
00:15:09,760 --> 00:15:16,960
Well, there's some debate, there's some talk about how if you accept it for food, then you have

137
00:15:16,960 --> 00:15:21,440
to eat it, but I don't think that applies to juice. But so if your intention is to have it in the

138
00:15:21,440 --> 00:15:24,880
morning when you receive it, and then you decide to, instead, I'll have it in the afternoon,

139
00:15:24,880 --> 00:15:29,440
there's something about that where it's a problem, but I'm not entirely sure how that,

140
00:15:29,440 --> 00:15:42,640
that applies. I don't really pay much attention to that. If you keep salt as a medicine,

141
00:15:43,440 --> 00:15:50,480
you can't then the next day sprinkle it on your food. But that doesn't have anything to do with

142
00:15:50,480 --> 00:15:54,640
your intention. That's just you've kept it more than a day, so using it as food isn't allowed.

143
00:15:54,640 --> 00:16:01,440
But using salt as a medicine, you can keep salt for your whole life. So yeah, as long as

144
00:16:02,320 --> 00:16:06,960
it wouldn't work anyway, but yeah, juice is something that even if you get it in the morning,

145
00:16:07,600 --> 00:16:10,880
you can keep it until the next morning.

146
00:16:12,480 --> 00:16:18,160
Is that just something that you find personally helpful to you with going the whole afternoon?

147
00:16:18,160 --> 00:16:32,480
Yeah, juice is great. It's the answer to this, and it's the happy medium, because a lot of

148
00:16:32,480 --> 00:16:38,560
Buddhists who have who keep the role, not to eat in the evening, actually don't keep the

149
00:16:38,560 --> 00:16:45,600
role, because they have a lot of soy milk and milk and even more, even heavier things,

150
00:16:45,600 --> 00:16:51,040
like they were trying to argue to have this soft tofu drinks, like it's a ginger,

151
00:16:51,040 --> 00:16:59,760
sweet ginger drink with soft soft tofu in it. Like a smoothie? No, it's chunks of very soft tofu,

152
00:16:59,760 --> 00:17:06,720
the chunks of them in the ginger drink, it's a Thai thing. It's too sweet, but otherwise it's

153
00:17:06,720 --> 00:17:16,320
pretty good. I mean, having ginger and tofu is great, but probably a bit too sweet to be healthy.

154
00:17:17,040 --> 00:17:22,160
Because I think just in general, in the volunteer group, we're trying to figure out ways to

155
00:17:22,160 --> 00:17:28,000
to support you as best as possible. So if we can figure out a way to have juice there that's

156
00:17:28,000 --> 00:17:32,000
offered to you, and then you could keep it through the afternoon or evening that would,

157
00:17:32,000 --> 00:17:35,440
that sounds like it would be a good thing. Try to figure that out.

158
00:17:35,440 --> 00:17:39,680
I mean, if someone comes in the morning and offers a juice drink as well, then I can keep that

159
00:17:39,680 --> 00:17:54,160
for the afternoon. But I can also, because people have already given me the meals in advance,

160
00:17:54,160 --> 00:17:58,960
through the gift cards, I can go to Martin's potentially and get a juice there.

161
00:17:58,960 --> 00:18:06,480
Do they have something good there? Yeah, they have orange and apple juice. I think it's

162
00:18:06,480 --> 00:18:29,200
it's overpriced, but I'm sure it's overpriced, but that's okay though.

163
00:18:29,200 --> 00:18:40,080
I'm working on these Spanish subtitles. There's a program to edit subtitles by

164
00:18:40,080 --> 00:18:43,360
files and once I finish the first file, I'll send it to your mail.

165
00:18:46,960 --> 00:18:47,920
Not a question, but

166
00:18:51,360 --> 00:18:53,600
Okay, well, thank you for working on the subtitles.

167
00:18:53,600 --> 00:19:00,800
Okay, so you can't, I guess you can't. That means you can't upload them yourself. You've

168
00:19:00,800 --> 00:19:04,800
looked and kept. Let's see.

169
00:19:10,800 --> 00:19:16,320
Well, no matter what work is done, it would have to go through your YouTube channel for it to

170
00:19:16,320 --> 00:19:30,560
reach its intended audience. There's a YouTube subtitles add-on, that's probably

171
00:19:30,560 --> 00:19:37,920
a bit. You cannot add a subtitle, track to someone else's video. You just have to share it.

172
00:19:37,920 --> 00:19:52,640
So, I have to be responsible to put this update on.

173
00:19:52,640 --> 00:20:08,640
Yes, Fernando says he can't, but he can send them to you.

174
00:20:10,480 --> 00:20:13,440
And then a question from Anthony,

175
00:20:13,440 --> 00:20:17,440
Dante, if a layperson is watching over food in the monastery for a joke or night,

176
00:20:17,440 --> 00:20:25,120
you have not broken a rule, right? Well, monastery fridges are, I mean, technically, it has to be

177
00:20:25,120 --> 00:20:32,800
a little bit more organized than that. There has to be a room in the monastery. I mean, maybe even

178
00:20:32,800 --> 00:20:37,600
a building. I don't know how. If you want to get really technical, but at least a room that is

179
00:20:37,600 --> 00:20:45,760
designated as a, I think it's called a kapiyagara or something of room for the kapiyat,

180
00:20:45,760 --> 00:20:51,600
or something like that. I don't know, I can't remember the exact poly term, but it's a place where

181
00:20:51,600 --> 00:20:58,560
the lay people, and it belongs to the lay people. So, the stuff in there is not for the monks.

182
00:20:58,560 --> 00:21:04,800
So, if I had juice in my room, if someone kept juice in my room, then I couldn't drink it,

183
00:21:04,800 --> 00:21:10,480
even if they offered it to me. There was a refrigerator in my room, but we consider the kitchen

184
00:21:10,480 --> 00:21:17,280
to be off limits to me. All the stuff in the kitchen is not mine. All the food and all the

185
00:21:17,280 --> 00:21:22,560
any of the drinks and any of the stuff, except the ice cubes. I had to make ice cubes a couple

186
00:21:22,560 --> 00:21:27,040
nights ago, because no one had no one made ice cubes. That's an important first aid item.

187
00:21:28,160 --> 00:21:35,600
We didn't have any. Are you listening, I don't know? Did someone enjoy themselves?

188
00:21:35,600 --> 00:21:41,440
I did. I injured myself. I was putting up a screen. A couple of the guys came from the Buddhism

189
00:21:41,440 --> 00:21:50,720
Association, came by on Sunday, Saturday, yesterday, Friday, maybe Friday, and they wanted to watch

190
00:21:50,720 --> 00:21:55,840
a Buddhist movie or a Buddhist theme movie. I think they ended up watching just in order. I don't

191
00:21:55,840 --> 00:22:02,160
know what they ended up watching. I came down and it was in the middle of actually, I don't

192
00:22:02,160 --> 00:22:05,360
know what to say. It was in the middle of something that probably wasn't appropriate for a Buddhist

193
00:22:05,360 --> 00:22:12,960
one. Anyway, I think probably we're going to have to talk about if we want to do that again,

194
00:22:13,680 --> 00:22:17,280
but definitely if we have Buddhist movies, I'm happy that they come. I think they just didn't have

195
00:22:17,280 --> 00:22:23,200
a sense of what was a good Buddhist movie. Don't worth watching, but they asked if they could come

196
00:22:23,200 --> 00:22:28,800
over and do some meditation and watch a Buddhist movie. That's great. So I put up the screen

197
00:22:28,800 --> 00:22:32,080
from them in the basement, but as I was putting up the screen, I pinched my finger.

198
00:22:33,440 --> 00:22:36,880
Pretty bad, actually. I'm surprised they didn't get all blood blister.

199
00:22:40,640 --> 00:22:46,000
But I immediately went and shoved it on. It's placed it against something really, really

200
00:22:46,000 --> 00:22:52,320
cold in the freezer, probably the thing that creates the ice. I just left it there for a bit.

201
00:22:52,320 --> 00:23:00,320
Well, I looked for ice cubes and couldn't find anything. Hopefully everyone is listening,

202
00:23:00,960 --> 00:23:04,080
but you can always take water, right? Even if it's from the kitchen.

203
00:23:04,080 --> 00:23:08,240
Yeah. No, and I made ice cubes. There's ice cubes there. I mean, I filled up the ice

204
00:23:08,240 --> 00:23:13,280
cube trays. They just weren't full. Yeah, they evaporate after a while if no one uses them.

205
00:23:13,280 --> 00:23:20,240
That's always the case in my freezer. They're always empty ice cube trays because they evaporate.

206
00:23:23,920 --> 00:23:30,960
Yeah, good to know all the possibilities of being able to send support and have it be useful to you.

207
00:23:34,240 --> 00:23:38,880
For clarification, I mean in the case that the layperson intends to offer the food the next

208
00:23:38,880 --> 00:23:45,040
morning, is that okay? It doesn't matter whether the layperson intends to offer it, it matters

209
00:23:45,040 --> 00:23:54,240
where it's kept. So I kept outside of the monastic. If food or anything is kept in the monastic

210
00:23:54,240 --> 00:24:01,680
quarters, that's not allowed. The monastic. It's called anti something that kept inside.

211
00:24:01,680 --> 00:24:08,480
So at your monastery, you would consider the kitchen to belong to the meditators.

212
00:24:08,480 --> 00:24:16,720
We haven't officially designated the kitchen as being the special room, but that's sort of just a

213
00:24:16,720 --> 00:24:23,440
given. In fact, I guess I could say the only part of this that this really the monastic part

214
00:24:23,440 --> 00:24:31,280
is the upstairs. So I would suggest that that is off the mitts, keeping stuff upstairs is not allowed,

215
00:24:31,280 --> 00:24:33,680
or if it's allowed, but I couldn't partake of it.

216
00:24:36,400 --> 00:24:43,040
And when food has been sent for the meditators, for example, the breakfast that Tina will send

217
00:24:43,040 --> 00:24:48,560
some some dry foods for breakfast and things, and if the meditator was eating breakfast and

218
00:24:48,560 --> 00:24:54,080
wish to prepare breakfast and offer it to you, is that allowed? Yeah, the meditators as long as the

219
00:24:54,080 --> 00:25:05,200
food is not kept in my not kept in my dwelling, not kept by me and not cooked by me. Those are the

220
00:25:05,200 --> 00:25:20,400
three aspects that are not allowed. That's good to know.

221
00:25:24,640 --> 00:25:31,760
Okay, enough. No. I think we just sit here and people will start to

222
00:25:31,760 --> 00:25:39,040
try and think up questions to throw at me. If people had questions, they would have asked them

223
00:25:39,040 --> 00:25:47,680
I know. No, I know meditation questions, but I was really trying to get a little more information

224
00:25:47,680 --> 00:25:50,240
on what's proper as far as offering food to you.

225
00:25:50,240 --> 00:26:01,200
And I think now that's not a question, that's a comment. Okay, tomorrow we'll have Kundalakacy,

226
00:26:01,200 --> 00:26:08,640
the story of Kundalakacy, to say, Tamapad and Ex Tamapad first. See, when we go without it for a

227
00:26:08,640 --> 00:26:13,760
couple of days, then everybody's looking forward to it. So it's more special. It's more special.

228
00:26:14,960 --> 00:26:17,760
Okay, thank you everyone. Thanks for having me for helping out.

229
00:26:17,760 --> 00:26:23,200
Thank you very much. Good evening.

