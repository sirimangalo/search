1
00:00:00,000 --> 00:00:11,680
Okay, no matter what retina they are, so welcome everyone, today we're transmitting live

2
00:00:11,680 --> 00:00:23,160
again from what I have lost Angelus, and today I thought I would continue with the

3
00:00:23,160 --> 00:00:33,240
Dhamma Bada, the Dhamma Bada, which is the path of Dhamma, the path of truth. A classic Buddhist

4
00:00:33,240 --> 00:00:48,200
text composed of a few hundred verses on the Pithi saying, with a Pithi saying, so the Lord Buddha that were delivered on

5
00:00:48,200 --> 00:00:53,920
various occasions. So we've gone through two of them already, now we'll move on to number three,

6
00:00:53,920 --> 00:01:09,840
and number four, three, four, and five. And number three, I'll just go through them all actually,

7
00:01:09,840 --> 00:01:17,000
and we'll talk about the background and the meaning of some of them. There you go. The

8
00:01:17,000 --> 00:01:40,320
verse one goes, which means he abused me, he struck me, he defeated me, he robbed me. If any

9
00:01:40,320 --> 00:01:51,400
church, if any church this thought their hatred never ceases. And the second one is the opposite.

10
00:01:51,400 --> 00:01:56,520
Again, this is the book of Paris, the Yamma Galaga. So there's going to be always two,

11
00:01:56,520 --> 00:02:03,800
it's going to go in Paris. The fourth one is Akojimanga, which is the same as the first one.

12
00:02:03,800 --> 00:02:12,840
Akojimins, he abused me, or what he means, he struck me, a jinni means defeated, aha,

13
00:02:12,840 --> 00:02:25,640
same means robbed. But then it changes Yehjatang, nu, panai hanti, wirang tesu, pasamati, which means one who

14
00:02:25,640 --> 00:02:33,920
doesn't dwell on these thoughts, one who doesn't keep these thoughts in mind, dwelling

15
00:02:33,920 --> 00:02:41,880
over the bad things that other people have done. Wirang tesu, pasamati, their hatred ceases.

16
00:02:41,880 --> 00:02:53,000
Actually, wirah is often not translated as hatred, but rather as vengeance or enmity, this cycle

17
00:02:53,000 --> 00:03:05,640
of revenge ceases for them, enmity, the having of enemies ceases. So we live in this world trying

18
00:03:05,640 --> 00:03:13,160
to live in peace and harmony with each other. In fact, the King of the Angels, Sakka, he came

19
00:03:13,160 --> 00:03:18,360
down to see the Buddha once and asked this very famous question, people want to live in harmony

20
00:03:18,360 --> 00:03:24,440
in peace with each other. Why is it that they cannot live in harmony in peace? And the Buddha

21
00:03:24,440 --> 00:03:32,840
said, it's because of their vying for things, their jealousy and their stinginess, their

22
00:03:32,840 --> 00:03:41,120
inability to share, inability to distribute wealth and power, our inability to give up to

23
00:03:41,120 --> 00:03:48,640
let go to help other people, our jealousy and stinginess, our desires, our addiction to

24
00:03:48,640 --> 00:04:03,320
happy and unhappy feelings. And so here the Buddha is pointing out this important fact of

25
00:04:03,320 --> 00:04:08,840
our existence that for us to live in this world in harmony and in peace, we have to give

26
00:04:08,840 --> 00:04:13,880
up these thoughts that people have heard us. When someone does a bad thing to us, not to hold

27
00:04:13,880 --> 00:04:22,080
on to it, not to keep it inside. For someone who can't do that, then they'll always have enmity.

28
00:04:22,080 --> 00:04:31,000
And the summary versus the third verse is just summarizing the other two. Why is that? Because

29
00:04:31,000 --> 00:04:45,120
nahi wahrena wahreni samadhi tah kudajana wahrena jasamadhi asatamu sanantana wahrena, which

30
00:04:45,120 --> 00:04:51,640
means, let me just get the translation here so that I can get it perfect. For not by hatred,

31
00:04:51,640 --> 00:04:57,800
our hatred is ever quenched here in this world, meaning never is hatred. Kudajana means

32
00:04:57,800 --> 00:05:05,560
ever, or in any case, for not by hatred, they ever quenched. You could never quench hatred

33
00:05:05,560 --> 00:05:11,480
by hatred. By love rather, are they quenched? Actually, the word is not love and the word

34
00:05:11,480 --> 00:05:22,080
is not hatred. Nahi wahrena, not by vengeance, not by enmity is enmity quenched, ever quenched,

35
00:05:22,080 --> 00:05:32,080
by non enmity, by giving up our enmity, outwahrena means not being hateful or spiteful or

36
00:05:32,080 --> 00:05:40,440
vengeful. This is an eternal law, asatamu wa sanantana wahrena. This is not something

37
00:05:40,440 --> 00:05:45,960
that's just the Buddha taught, this is a part of nature. So first the stories behind

38
00:05:45,960 --> 00:05:54,720
these two verses, the first story is a story of a monk who ordained when he was old. He

39
00:05:54,720 --> 00:06:01,560
looked like he was an elder monk. I've experienced this before in Thailand, not personally

40
00:06:01,560 --> 00:06:07,080
so much, but I've lived with elder monks who have recently ordained because it's a common

41
00:06:07,080 --> 00:06:12,440
thing in Thailand. They can be really funny. They're kind of nice the first week or so,

42
00:06:12,440 --> 00:06:18,000
but after that, people start coming up to them and paying respect to them only because

43
00:06:18,000 --> 00:06:23,600
they think, oh, this is a senior monk. So they start to get really unbearable after

44
00:06:23,600 --> 00:06:30,960
a while and putting themselves up and suddenly getting this ego. It's a common thing that's

45
00:06:30,960 --> 00:06:38,640
in, there's a lot of examples of this in the text. There's one of this man who was shipwrecked

46
00:06:38,640 --> 00:06:47,200
and when he got back to shore, he had no clothes on. He had to swim for safety and discard

47
00:06:47,200 --> 00:06:52,240
all of his clothes. When he got to shore, he had nothing and so he just put on a, he took

48
00:06:52,240 --> 00:07:00,320
a piece of wood or like a barrel or something and sort of held that over him to hide

49
00:07:00,320 --> 00:07:05,880
his nudity. And when he was sitting by the side of the road begging and people looked

50
00:07:05,880 --> 00:07:13,240
at him and thought he maybe was a holy man. And so as a result that he became quite famous

51
00:07:13,240 --> 00:07:18,080
and people would come and give him gifts and give him food and so on. And as a result

52
00:07:18,080 --> 00:07:22,960
that he started to actually believe that he was in Arahan. He started out this idea where

53
00:07:22,960 --> 00:07:29,480
he had just been going naked for because he had no clothes out of necessity. He started

54
00:07:29,480 --> 00:07:36,480
doing it as a matter of practice so when people brought him clothes, he wouldn't take them

55
00:07:36,480 --> 00:07:39,200
and so on. And so finally someone woke him up to the fact that he was not enlightened

56
00:07:39,200 --> 00:07:43,440
and this wasn't the practice of the enlightened beings because in that time nudity was considered

57
00:07:43,440 --> 00:07:51,640
to be a valid religious practice. It was like giving up everything. And so this is what

58
00:07:51,640 --> 00:07:56,800
happened with this, this is what, what often happens with monks when they ordained old

59
00:07:56,800 --> 00:08:06,240
and this is what happened with this monk, Disa. Disa was his name. And so he would sit

60
00:08:06,240 --> 00:08:14,640
in the middle of the monastery and the meeting hall or so and this hall for rest hall.

61
00:08:14,640 --> 00:08:19,360
And when these senior monks came or you know monks would have been ordained for ten years,

62
00:08:19,360 --> 00:08:25,200
twenty years or so on. They came in, he would just sit there in the Sala and in the hall

63
00:08:25,200 --> 00:08:33,840
and you know not offer to take their bowls, take their robes and not offer to massage

64
00:08:33,840 --> 00:08:38,880
their legs. These duties that monks have to do to each other when a monk comes walking

65
00:08:38,880 --> 00:08:42,640
from far away because they would walk for long distances. You had to offer water, you had

66
00:08:42,640 --> 00:08:50,320
to take their robe, you had to take their bowl, then you had to show them where the

67
00:08:50,320 --> 00:08:56,280
duties were and where the washrooms were and so on. You had to help them out if you were

68
00:08:56,280 --> 00:09:00,360
a junior monk. If you're senior you still had to tell them where everything was but you

69
00:09:00,360 --> 00:09:06,600
don't have to massage them or bring water for washing or so on. You just have to tell

70
00:09:06,600 --> 00:09:11,360
them where everything is not them to themselves. But this was a newly ordained monk and

71
00:09:11,360 --> 00:09:16,840
so he's sitting there and these monks look at him and they think oh he must be a senior

72
00:09:16,840 --> 00:09:25,120
monk, he's not getting up at all. So they they talked to him and they they they slowly

73
00:09:25,120 --> 00:09:29,160
and around about way ask him how long he's been ordained and he said oh I've just recently

74
00:09:29,160 --> 00:09:33,280
ordained him I was ordained when I was old and they said what don't you understand don't

75
00:09:33,280 --> 00:09:38,120
you know how to do duties or the duties to incoming monks and he got really angry at them

76
00:09:38,120 --> 00:09:46,400
and called them all sorts of bad names. Because they as they started they said oh you

77
00:09:46,400 --> 00:09:50,560
don't have a clue you don't know how can you you don't know any you don't you haven't

78
00:09:50,560 --> 00:09:56,240
been trained in the proper etiquette and so on. And so he got all upset and went to the Buddha

79
00:09:56,240 --> 00:10:03,520
and the Buddha confirmed that he should have done something for these monks and and this

80
00:10:03,520 --> 00:10:07,520
has said well the reason I didn't do it is because they were they were very mean to me

81
00:10:07,520 --> 00:10:11,240
and so the Buddha then gave this verse he said if people are mean to you shouldn't ever

82
00:10:11,240 --> 00:10:19,560
hold on to it and you shouldn't ever try to get back at them for it you should never get

83
00:10:19,560 --> 00:10:23,440
upset at people for one who gets upset and this is where he said the verse for one who

84
00:10:23,440 --> 00:10:36,360
gets upset at others and holds on to this their enmity has never quenched.

85
00:10:36,360 --> 00:10:42,960
And then he told a story about the past but these two ascetics who something similar

86
00:10:42,960 --> 00:10:49,960
happened they were these two ascetics were staying in this house together and one of

87
00:10:49,960 --> 00:10:55,480
them on his way out he stepped on the other one's hair. What happened was the one one of

88
00:10:55,480 --> 00:10:59,760
them was going to lie down near the doorway and he put his pillow at one side but then

89
00:10:59,760 --> 00:11:03,800
when he went to sleep after it got dark he moved the pillow to the other side and slept

90
00:11:03,800 --> 00:11:07,600
with his head in the other direction so the other guy thought the first guy's head was

91
00:11:07,600 --> 00:11:12,560
in was facing to one direction and when he was on his way out he he stepped on the

92
00:11:12,560 --> 00:11:17,640
guy's hair thinking he was going around to the end where his feet were and the other

93
00:11:17,640 --> 00:11:24,600
ascetic got all angry and thought to himself that he would switch directions because

94
00:11:24,600 --> 00:11:29,640
this guy might step on him step on his hair when he comes back in as well. The guy goes

95
00:11:29,640 --> 00:11:33,240
out and thinks okay next time we'll go around the other side and of course the second

96
00:11:33,240 --> 00:11:38,000
ascetics turned around again trying to get to try to get out of the first guy's way and

97
00:11:38,000 --> 00:11:45,000
so this time he steps on the guy's neck and the other guy goes all angry and says

98
00:11:45,000 --> 00:11:49,280
first he stepped on my hair then he stepped on my neck and then he curses him and this

99
00:11:49,280 --> 00:11:53,520
was a big thing in ancient India where they cursed each other and curses were thought to

100
00:11:53,520 --> 00:11:59,880
have great potency but of course this guy wasn't a very devoted ascetic so it didn't

101
00:11:59,880 --> 00:12:05,600
have that much power. The other ascetic then got angry back and cursed the curse the guy

102
00:12:05,600 --> 00:12:13,880
who he'd stepped on and said then I curse you may your head split into seven pieces and

103
00:12:13,880 --> 00:12:18,600
then it goes on and on and there's the story of how he has to save the guy from his head

104
00:12:18,600 --> 00:12:23,120
is actually going to split the guy who got stepped on and got all angry about it he's

105
00:12:23,120 --> 00:12:32,280
going to have his head actually broken to seven pieces and then the king comes and the

106
00:12:32,280 --> 00:12:38,160
king is going to tries to get him to ask forgiveness look ask forgiveness for getting

107
00:12:38,160 --> 00:12:43,000
so angry at him he says no I won't ever ask forgiveness he says then your head is going

108
00:12:43,000 --> 00:12:48,560
to split in seven pieces and then they find some way around it and then the Buddha tells

109
00:12:48,560 --> 00:12:53,080
the story he says you know when people when people do things that we don't like it's very

110
00:12:53,080 --> 00:13:00,720
easy to get angry in fact it's often something you can forgive in people when they get

111
00:13:00,720 --> 00:13:06,280
angry at you sometimes we do things and people get angry at us maybe we didn't even mean

112
00:13:06,280 --> 00:13:11,800
it but they get angry and we should always consider first that this is the way of the mind

113
00:13:11,800 --> 00:13:16,760
when we get something that we don't like it's very easy for us to get angry so to

114
00:13:16,760 --> 00:13:20,320
the extent that someone gets angry at us for something we did it's not really a problem

115
00:13:20,320 --> 00:13:25,360
but the problem is when they hold on to it when they keep it in their mind and they

116
00:13:25,360 --> 00:13:30,060
dwell on it this is where evil really rises because anger is not something you can easily

117
00:13:30,060 --> 00:13:36,440
control or stop from arising you have to be really clear and aware of reality as it

118
00:13:36,440 --> 00:13:44,760
is you have to be able to see things see the experience quite clearly before you can actually

119
00:13:44,760 --> 00:13:49,480
do away with anger otherwise we all get angry we get angry at the slightest pain we get

120
00:13:49,480 --> 00:13:58,880
angry at the slightest noise we get angry at the slightest comment very easy to become angry

121
00:13:58,880 --> 00:14:04,440
but this is something that is normal it's a part of being an ordinary being where we really

122
00:14:04,440 --> 00:14:11,320
could get cause problems is when we take it to heart when we let it get to us so this

123
00:14:11,320 --> 00:14:21,200
is where the Buddha said this is where real problem comes from the second story is

124
00:14:21,200 --> 00:14:26,880
a bit different and much more famous it's one of the most famous of the Buddhist stories

125
00:14:26,880 --> 00:14:35,920
of the Damapada it's the story of the this woman and an ogre and how it started

126
00:14:35,920 --> 00:14:41,560
was this there was a man who who's mother wanted him to have a get a wife and he didn't

127
00:14:41,560 --> 00:14:47,240
want to have a wife and so he complained she complained to complain to complain until

128
00:14:47,240 --> 00:14:55,800
finally he gave in and he went to a family and and asked for their daughter in marriage

129
00:14:55,800 --> 00:14:59,240
and it turns out she was barren she couldn't give birth to children so the mother got

130
00:14:59,240 --> 00:15:05,760
upset and didn't like this and said look we need to get you another wife this is this

131
00:15:05,760 --> 00:15:15,200
after this is based on ancient this is in ancient India and so the mother went no so

132
00:15:15,200 --> 00:15:20,000
the the mother was going to get a wife and the elder wife thought oh if they go and get

133
00:15:20,000 --> 00:15:26,480
a wife how will I get along with her and so she went out and found a wife for him herself

134
00:15:26,480 --> 00:15:31,240
his first wife went out and found him a second wife and brought the second woman back and

135
00:15:31,240 --> 00:15:39,560
the second woman did give did did become pregnant but the first wife was so so scared that

136
00:15:39,560 --> 00:15:43,360
if this woman were to give birth and she would be if she would be considered useless she

137
00:15:43,360 --> 00:15:51,080
would be considered just to be a burden and she would lose all of her status so she she

138
00:15:51,080 --> 00:15:57,920
mixed some drug up in the woman in the second woman's food and caused her to give an abortion

139
00:15:57,920 --> 00:16:04,400
to have an abortion which she did is she said as soon as you as soon as you become pregnant

140
00:16:04,400 --> 00:16:09,560
let me know so I can help take care of you and this the second woman gallably did so

141
00:16:09,560 --> 00:16:16,760
and as soon as the first woman knew the first wife knew that she had become pregnant she

142
00:16:16,760 --> 00:16:19,840
mixed this drug in and caused her to have an abortion and she caused her to have an abortion

143
00:16:19,840 --> 00:16:27,160
twice in this way until finally the second wife caught on and so this time she didn't tell

144
00:16:27,160 --> 00:16:34,320
the first wife that she was pregnant until she was very close to giving birth and then

145
00:16:34,320 --> 00:16:41,120
the second the first wife found out and found some way to sneak this drug into her food

146
00:16:41,120 --> 00:16:47,960
again only this time it was too late to have an abortion and the child it died I guess

147
00:16:47,960 --> 00:16:58,160
in her stomach but it got stuck in the the birth canal and the woman realized that she

148
00:16:58,160 --> 00:17:04,800
was dying the second wife and as she was dying she cursed the first woman and said you've

149
00:17:04,800 --> 00:17:08,640
caused me to lose three of my children and now you've caused me to die and she vowed

150
00:17:08,640 --> 00:17:17,360
that in her next life she would get even with the first wife and then she died and the

151
00:17:17,360 --> 00:17:25,440
first wife the husband because when he finds out that the first wife is the cause of the

152
00:17:25,440 --> 00:17:36,880
miscarriage beats the first wife and makes her very ill causes her to become quite injured

153
00:17:36,880 --> 00:17:42,880
to the point where she dies she becomes sick and dies the second wife becomes a cat in

154
00:17:42,880 --> 00:17:49,840
that same house and the first wife becomes a chicken in the same house and because of their

155
00:17:49,840 --> 00:17:55,320
karma they become they enter into this circle of vengeance which is so common in Buddhism

156
00:17:55,320 --> 00:18:00,120
this is where we understand all of our enemies come from people wonder why there are people

157
00:18:00,120 --> 00:18:05,240
who just seem out to get us why do we meet these people and they just don't like us

158
00:18:05,240 --> 00:18:12,000
no matter what we do we can't do anything right and they're always out to get us in this

159
00:18:12,000 --> 00:18:15,560
case this is because of these sort of things where we just can't let go of this cycle

160
00:18:15,560 --> 00:18:29,640
of vengeance and so the the cat and the chicken they knew each other right away and

161
00:18:29,640 --> 00:18:33,720
the the cat in that house every time the chicken would give birth to chicks the cat would

162
00:18:33,720 --> 00:18:40,720
come and eat all eat the baby chicks and this made the hen the hen very angry and

163
00:18:40,720 --> 00:18:48,760
the next life the cat became a deer and the chicken became a panther or tiger or something

164
00:18:48,760 --> 00:18:54,560
a jungle cat and the same thing happened and they knew each other right away they would

165
00:18:54,560 --> 00:18:58,320
be in the same forest and as soon as they saw each other it was like remembering an

166
00:18:58,320 --> 00:19:05,800
old friend in this case remembering an old enemy and the jungle cat would eat the babies

167
00:19:05,800 --> 00:19:18,520
of the deer and ate the deer and so on and in their next life the final life the deer

168
00:19:18,520 --> 00:19:29,440
was born as a ogre and the the panther was born as or the jungle cat was born as a woman

169
00:19:29,440 --> 00:19:39,200
you know ogres and Buddhism demon might be another word for it it's like a spirit that

170
00:19:39,200 --> 00:19:49,880
eats children like these goblins or evil spirits evil ghosts demon I guess was the best

171
00:19:49,880 --> 00:19:58,480
word and so this is demon found ways to eat all of this women's children again and again

172
00:19:58,480 --> 00:20:07,600
first you know pretending to be someone else and so on and so finally the woman decided

173
00:20:07,600 --> 00:20:11,360
she it was time to that she would got pregnant and she or she had give it she gave birth

174
00:20:11,360 --> 00:20:14,520
to a child and she knew she had to get out of town because this ogre was going to find

175
00:20:14,520 --> 00:20:20,160
some way to eat her so they were walking along leaving town and then the ogre saw them

176
00:20:20,160 --> 00:20:28,760
on the road and as soon as they saw each other they knew right away who was who and the woman ran

177
00:20:28,760 --> 00:20:32,920
and she happened to be close to Jeta one where the Buddha was staying in the Buddha's

178
00:20:32,920 --> 00:20:39,280
monastery and so she ran into Jeta one into the Buddhist monastery thinking that maybe

179
00:20:39,280 --> 00:20:43,840
there would be some help there and the Buddha was in the middle of teaching in the midst

180
00:20:43,840 --> 00:20:50,040
of the assembly and she ran right up to the Buddha and threw the through her baby up

181
00:20:50,040 --> 00:20:53,360
through her baby over to the Buddha I think I can't remember exactly how it goes does seem

182
00:20:53,360 --> 00:20:59,400
a little bit strange and let me see what exactly what exactly happens a laid later boy at

183
00:20:59,400 --> 00:21:04,200
the feet of the Buddha she didn't place it on his lap that would be way too much and gave

184
00:21:04,200 --> 00:21:10,920
him over she I give you this child spare the life of my son now the demon wasn't able

185
00:21:10,920 --> 00:21:18,880
to enter into the the into the monastery because of the angels that protect the place but

186
00:21:18,880 --> 00:21:23,640
the teacher the Buddha called had Ananda called them called the ogre in called the demon

187
00:21:23,640 --> 00:21:30,480
in allowing her to come in and Ananda the Buddha's attendant brought her to stand before

188
00:21:30,480 --> 00:21:40,600
the Buddha and the Buddha said let her come don't let anyone stop her and then he said

189
00:21:40,600 --> 00:21:45,360
do her he said why have you done this had you not come face to face with a Buddha like

190
00:21:45,360 --> 00:21:49,760
me you would have cherished hatred towards each other for an eon like the snake and the

191
00:21:49,760 --> 00:21:56,080
mongus who trembled and quaked with enmity like the crows and the owls these are ancient

192
00:21:56,080 --> 00:22:01,560
these are references to ancient Indian fairy tales crows and owls are bitter enemies

193
00:22:01,560 --> 00:22:07,440
and the snake and the mongus are bitter enemies why do you return hatred for hatred hatred

194
00:22:07,440 --> 00:22:11,880
is quenched by love not by hatred and when he had thus spoken he pronounced the following

195
00:22:11,880 --> 00:22:19,240
stands and this is number five for not by enmity is enmity quenched in this world ever by

196
00:22:19,240 --> 00:22:29,920
not keeping enmity rather is hatred quenched this is an eternal law and then he had

197
00:22:29,920 --> 00:22:37,360
them give up and have them understand the truth of the truth of the teaching that he

198
00:22:37,360 --> 00:22:51,560
was giving so enmity is something that we we very easily fall into in this world when

199
00:22:51,560 --> 00:22:57,400
we're not meditating we can come we find that we're friends with other people we can

200
00:22:57,400 --> 00:23:02,600
be friends easily with them for a while but after some time we'll always come to sort

201
00:23:02,600 --> 00:23:09,320
of a test of our relationship we find this with husband and wife that often it's so wonderful

202
00:23:09,320 --> 00:23:14,800
the first you know I don't know how long before you're married it's wonderful once

203
00:23:14,800 --> 00:23:18,880
you become married it's wonderful for a short time and then it starts to get back to the

204
00:23:18,880 --> 00:23:25,880
practicalities and it's very easy for us to become annoyed and to hold on to this annoyance

205
00:23:25,880 --> 00:23:30,040
and to not be able to let go and to think that somehow everything should all be perfect

206
00:23:30,040 --> 00:23:36,640
and wonderful and so we're not able to get along because of our need for our attachment

207
00:23:36,640 --> 00:23:41,280
to the way things used to be or looking for something more exciting or so on and so you

208
00:23:41,280 --> 00:23:47,720
see people divorcing and getting married again and some people doing it two or three times

209
00:23:47,720 --> 00:23:53,520
four or five times always thinking that it's going to be perfect this time they're not

210
00:23:53,520 --> 00:23:59,960
realizing that the nature of things is not always the way it's not like that things cannot

211
00:23:59,960 --> 00:24:08,240
be perfect so part of our Buddhist practice is this act of forgiveness of letting go of

212
00:24:08,240 --> 00:24:12,440
our hatreds and being able to forgive each other it's considered to be a gift that you give

213
00:24:12,440 --> 00:24:19,640
to someone when you forgive them when you forgive someone it's like giving them a gift when

214
00:24:19,640 --> 00:24:24,480
we take the precepts we always think of it as a gift where we're never going to kill again

215
00:24:24,480 --> 00:24:28,840
and this is our gift that we give to the whole universe to all beings whenever we're

216
00:24:28,840 --> 00:24:33,720
going to steal again we make this resolve not to steal this is a gift we give to all beings

217
00:24:33,720 --> 00:24:39,360
we never cheat we never lie these are gifts that we give to all beings this freedom from

218
00:24:39,360 --> 00:24:45,920
us that no being need fear from us and we make a resolve that when people do bad things

219
00:24:45,920 --> 00:24:55,160
to us we take the Lord Buddha's words that even if even if bandits were to capture you

220
00:24:55,160 --> 00:25:02,080
and tie you down and proceed to saw off your arms and legs with a hacksaw well you're

221
00:25:02,080 --> 00:25:10,040
still alive the Buddha said any any person who would for this reason cherished anger towards

222
00:25:10,040 --> 00:25:17,200
those those bandits would not be a student of mine and he said remember this simile

223
00:25:17,200 --> 00:25:23,040
this is the simile of the saw this is the famous simile of the saw of the Buddha he said

224
00:25:23,040 --> 00:25:26,360
remember this simile of the saw and there should be there will be nothing which you

225
00:25:26,360 --> 00:25:32,080
cannot endure they said to that extent that anyone who is angry even when people are

226
00:25:32,080 --> 00:25:40,360
sawing hacking your arms and legs off if you church anger towards them for that reason you

227
00:25:40,360 --> 00:25:47,960
would not be a follower of the Buddha but as I said before it's not that we can't get angry

228
00:25:47,960 --> 00:25:53,440
at each other we should always be willing to accept other people's anger at us the Lord

229
00:25:53,440 --> 00:25:59,720
Buddha said to say what day in a papa yo yo kutang bhatikutchati which means it's a worse

230
00:25:59,720 --> 00:26:06,400
evil to hold on to get angry back at someone who is angry at you this is the worst evil

231
00:26:06,400 --> 00:26:14,800
of the two because it is this taking things to heart holding on to and and keeping things

232
00:26:14,800 --> 00:26:20,880
in our mind this idea of vengeance that is the real evil anger is not something we should

233
00:26:20,880 --> 00:26:26,280
repress and when it comes out we should let it come up not let it come out by saying or

234
00:26:26,280 --> 00:26:31,040
doing things but when it comes up just look at it come to understand where it came from

235
00:26:31,040 --> 00:26:35,920
and what it is and learn and see that it's actually not a good thing see clearly that

236
00:26:35,920 --> 00:26:40,040
it's not a good thing we don't have to pretend that we know it's a bad thing and repress

237
00:26:40,040 --> 00:26:47,800
it if we knew it was really a bad thing we wouldn't give rise to it when we practice

238
00:26:47,800 --> 00:26:54,440
meditation in this way simply looking at the anger looking at our dislike King and our dissatisfaction

239
00:26:54,440 --> 00:27:02,080
this is how we can really come to change our minds change the way we look at things when

240
00:27:02,080 --> 00:27:06,720
we just let the emotion be and it is an emotion let it be as it is sometimes it's hatred

241
00:27:06,720 --> 00:27:11,480
towards others sometimes it's hatred towards the situation sometimes it's hatred towards

242
00:27:11,480 --> 00:27:19,480
oneself just look at these and learn about them we don't have to reject them this is why

243
00:27:19,480 --> 00:27:28,080
in meditation we always say to ourselves angry like King wanting so on afraid or just

244
00:27:28,080 --> 00:27:33,480
remind ourselves this it is what it is most of the time when one of these emotions comes

245
00:27:33,480 --> 00:27:40,800
up we feel we have to do something about it instead of just reminding ourselves instead

246
00:27:40,800 --> 00:27:46,120
of just knowing it for what it is we we think oh now I have to I'm angry so now I have

247
00:27:46,120 --> 00:27:51,680
to say or do something or that's bad and I have to repress it we change that to say this

248
00:27:51,680 --> 00:27:57,400
is anger so it's anger so we say to ourselves angry angry and we simply know it for what

249
00:27:57,400 --> 00:28:04,720
it is nothing more nothing less so this is an important teaching of the Buddha something

250
00:28:04,720 --> 00:28:10,280
we should always keep in mind that we're never going to have an end we'll be like this

251
00:28:10,280 --> 00:28:16,040
ogre and the woman who again and again we're reborn in this cycle of vengeance until finally

252
00:28:16,040 --> 00:28:20,520
they met the Buddha and he explained to them that this isn't the way to become free from

253
00:28:20,520 --> 00:28:26,600
suffering there's no end you cannot beat someone to the point where you never have to fight

254
00:28:26,600 --> 00:28:37,760
again and then we'll see later on in this book that about that when you when you win

255
00:28:37,760 --> 00:28:42,840
there's always when someone wins there's always a loser and the loser is always looking

256
00:28:42,840 --> 00:28:49,800
to win or looking to get even so that's the dhamma that I thought I would give today

257
00:28:49,800 --> 00:28:57,920
this is from the dhamma pad that thank you all for tuning in and I hope to see you again

258
00:28:57,920 --> 00:29:11,520
next time if you have any questions I'm happy to take them but you'll have to either

259
00:29:11,520 --> 00:29:28,720
do it in world or on the website you can text in on the website or in Buddha verse

260
00:29:28,720 --> 00:29:34,640
you can find the stories of the dhamma pad online actually you can find them in Google

261
00:29:34,640 --> 00:29:40,360
books if you go to Google books and if you go to my library I don't know if there's

262
00:29:40,360 --> 00:29:44,680
anything you can find the link to you to dhamma on Google books probably search for my

263
00:29:44,680 --> 00:29:52,520
library I've got them all in my library that's where I'm taking the English passages from

264
00:29:52,520 --> 00:29:57,400
the translations are not so good for instance the translation of the second story that

265
00:29:57,400 --> 00:30:05,960
I gave today has it wrong also the first story where this this ascetic is lying in the

266
00:30:05,960 --> 00:30:17,200
doorway and the second ascetic steps on his hair in the in the translation it says when

267
00:30:17,200 --> 00:30:21,800
the first ascetic goes out the second ascetics all angry and grumbling it says okay now

268
00:30:21,800 --> 00:30:27,600
I'm going to turn around so that he can step on my hair again like as though he did

269
00:30:27,600 --> 00:30:31,240
it on purpose turning around thinking oh good now has step on my hair again then it's

270
00:30:31,240 --> 00:30:37,960
not the truth that's not what it means and in probably it is ambiguous it's using an

271
00:30:37,960 --> 00:30:50,600
operative case for the verb which could mean that he will or so he should but it certainly

272
00:30:50,600 --> 00:30:57,240
doesn't mean that in this case it means because he may I'm going to turn around because

273
00:30:57,240 --> 00:31:04,600
he may it doesn't mean that he may means he might right it means he might and this could be used

274
00:31:04,600 --> 00:31:09,800
in both senses that I'm going to do it so that because if I do that then he might then it might

275
00:31:09,800 --> 00:31:14,120
be a cause for him but the meaning is not that the meaning is I'm going to turn around because he

276
00:31:14,120 --> 00:31:19,160
might do it again and then he does he does do it again he doesn't realize that by turning around

277
00:31:19,160 --> 00:31:25,080
he's actually causing him the because the other guy smart and says okay I won't step on his

278
00:31:25,080 --> 00:31:32,360
his hair again and so they sort of it's like those dancing routines where you're trying to

279
00:31:32,360 --> 00:31:35,720
go around someone you go to the left and they go to the left as well and you go to the right

280
00:31:35,720 --> 00:31:43,480
and they go to the right but but you know they're pretty good translations overall you just

281
00:31:43,480 --> 00:31:49,480
have to read them with the grain of salt sometimes the wording is very Christian oriented I

282
00:31:49,480 --> 00:32:01,640
suppose archaic maybe is the better word but you find them on Buddhist book Google books in Buddhist

283
00:32:01,640 --> 00:32:17,080
legends it's under the title of Buddhist legends there's three volumes yeah the stories are

284
00:32:17,080 --> 00:32:22,840
really nice the jotica stories are probably nicer but if you're a if you're a dedicated Buddhist

285
00:32:22,840 --> 00:32:26,440
I think you won't mind the dumb about the stories I really enjoyed them but I know a lot of

286
00:32:26,440 --> 00:32:30,760
people would read them and say oh that's rubbish or that's children's stories or it's fables

287
00:32:31,400 --> 00:32:37,000
hence the name Buddhist legends but if you're a dedicated Buddhist in the traditional sense

288
00:32:38,920 --> 00:32:43,640
I think you you'd enjoy them I mean we're able to suspend our disbelief and a lot of these things

289
00:32:43,640 --> 00:32:50,600
and say well I don't know if that's true or not and just take it at face value and it's a

290
00:32:50,600 --> 00:32:57,000
really good read actually but comparing the two I'd say the jotica are better actually

291
00:32:57,960 --> 00:33:01,880
but the dhamapada are nice they're just maybe a little bit more

292
00:33:01,880 --> 00:33:16,200
sensational sensationalistic more exaggerated so you'll have the same story and the jotica is in

293
00:33:16,200 --> 00:33:26,120
the the dhamapada but the jotica version is much more toned down but the dhamapada because of

294
00:33:26,120 --> 00:33:33,960
its its sort of exciting nature it's sensational nature it's used in traditional Buddhist countries

295
00:33:35,800 --> 00:33:40,520
Thailand at any rate Thailand especially as basic

296
00:33:43,960 --> 00:33:50,040
a basic text for learning poly so novices learning poly will study the dhamapada

297
00:33:50,040 --> 00:33:55,880
in fact in Thailand most of the poly study is based on the dhamapada commentaries

298
00:33:57,240 --> 00:33:59,240
and verses commentaries and verses

299
00:34:04,360 --> 00:34:09,000
but they don't even realize that the commentary is a commentary it's considered to be the dhamapada

300
00:34:09,000 --> 00:34:25,480
is the stories and verses so it's a very very popular in those in Thailand anyway

