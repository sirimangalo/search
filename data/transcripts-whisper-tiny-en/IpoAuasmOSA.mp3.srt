1
00:00:00,000 --> 00:00:05,000
Hello everyone and welcome back to our study of the Damopanda.

2
00:00:05,000 --> 00:00:32,000
Today we continue on with first number 108, which reads as follows.

3
00:00:32,000 --> 00:00:37,000
It's actually a bit difficult because the grammar doesn't really work in English.

4
00:00:37,000 --> 00:00:48,000
So, whatever sacrifice or worship there is in the world.

5
00:00:48,000 --> 00:00:52,000
If for 100 years, if for 100 years,

6
00:00:52,000 --> 00:01:01,000
you pickle one desiring boonya, desiring merit, should perform such a sacrifice or such a worship.

7
00:01:01,000 --> 00:01:08,000
And yes, it's actually a very, very similar verse and story to the last two.

8
00:01:08,000 --> 00:01:15,000
Subampitang tatubagameti, it doesn't come to a quarter of the value.

9
00:01:15,000 --> 00:01:19,000
All of that doesn't come to a quarter of the value.

10
00:01:19,000 --> 00:01:24,000
The 100 years of sacrifice or worship that one pays.

11
00:01:24,000 --> 00:01:29,000
Abiwadana oju, oju got desu seyo.

12
00:01:29,000 --> 00:01:34,000
For the greater, in comparison to the greater,

13
00:01:34,000 --> 00:01:38,000
Abiwadana or respect or reverence,

14
00:01:38,000 --> 00:01:40,000
that one pays to.

15
00:01:40,000 --> 00:01:43,000
And today our word is ojugata.

16
00:01:43,000 --> 00:01:50,000
In regards to those who are ojugata gone to or become straight,

17
00:01:50,000 --> 00:01:59,000
those who have gained rectitude, moral or ethical or mental straightness,

18
00:01:59,000 --> 00:02:04,000
uprightness, those who are upright.

19
00:02:04,000 --> 00:02:08,000
So, same story, we have Saripudad.

20
00:02:08,000 --> 00:02:14,000
And it's becoming apparent that he was really instrumental

21
00:02:14,000 --> 00:02:21,000
in helping a lot of his relatives and now his friends become enlightened.

22
00:02:21,000 --> 00:02:28,000
So, now we have Saripudad's friend, Sahaya Kamramana.

23
00:02:28,000 --> 00:02:31,000
And Brahmin, who was a friend of Saripudad.

24
00:02:31,000 --> 00:02:39,000
So, Saripudad went to him and asked him, do you do any good deeds?

25
00:02:39,000 --> 00:02:42,000
And he says, oh yes, what do you do?

26
00:02:42,000 --> 00:02:45,000
Oh, I offer sacrificial slaughter.

27
00:02:45,000 --> 00:02:50,000
And of course, Saripudad doesn't see that as being a wholesome deed.

28
00:02:50,000 --> 00:02:53,000
This is really the crux of these three stories.

29
00:02:53,000 --> 00:03:06,000
This is the idea that our concept of good deeds is often quite misled.

30
00:03:06,000 --> 00:03:09,000
And this even occurs in Buddhism.

31
00:03:09,000 --> 00:03:16,000
You'll see Buddhists performing deeds that they think are good deeds when in fact they are.

32
00:03:16,000 --> 00:03:20,000
Perhaps useless.

33
00:03:20,000 --> 00:03:23,000
So, the case of...

34
00:03:23,000 --> 00:03:33,000
I've told this story before, but once we had a Katina ceremony where I was staying.

35
00:03:33,000 --> 00:03:37,000
And all the villagers came together to do the Katina ceremony.

36
00:03:37,000 --> 00:03:42,000
And so, what they did is they had this very important.

37
00:03:42,000 --> 00:03:44,000
I was in a rural area.

38
00:03:44,000 --> 00:03:47,000
So, it was a area where people didn't have a lot of money.

39
00:03:47,000 --> 00:03:53,000
And there was one man who had built a house in the area from Bangkok.

40
00:03:53,000 --> 00:03:55,000
And he was quite rich.

41
00:03:55,000 --> 00:03:58,000
And so, he got his rich friends from Bangkok to come up.

42
00:03:58,000 --> 00:04:02,000
And they put together a bunch of money and they put the money up on a tree.

43
00:04:02,000 --> 00:04:05,000
And they brought this money tree to the monastery.

44
00:04:05,000 --> 00:04:07,000
And they wanted to do a Katina ceremony.

45
00:04:07,000 --> 00:04:10,000
Well, it turns out half of them were drunk.

46
00:04:10,000 --> 00:04:14,000
They had been drinking.

47
00:04:14,000 --> 00:04:17,000
And the funniest part was.

48
00:04:17,000 --> 00:04:20,000
And it sort of made it a bit ridiculous,

49
00:04:20,000 --> 00:04:25,000
was that they didn't have a single robe with wives to do the Katina.

50
00:04:25,000 --> 00:04:27,000
So, they brought this money tree thinking,

51
00:04:27,000 --> 00:04:28,000
well, that's Katina.

52
00:04:28,000 --> 00:04:30,000
Katina is a money tree because that's really what's become.

53
00:04:30,000 --> 00:04:32,000
It's all about money trees.

54
00:04:32,000 --> 00:04:33,000
Trees that have...

55
00:04:33,000 --> 00:04:35,000
Or literally the made of money.

56
00:04:35,000 --> 00:04:39,000
The leaves are folded up bits of money.

57
00:04:39,000 --> 00:04:43,000
Or just money sticking up on sticks.

58
00:04:43,000 --> 00:04:47,000
And so I said,

59
00:04:47,000 --> 00:04:48,000
I said, okay, we're ready to Katina.

60
00:04:48,000 --> 00:04:49,000
What do we do?

61
00:04:49,000 --> 00:04:52,000
And I said, well, where's the robe?

62
00:04:52,000 --> 00:04:55,000
And they didn't have a robe.

63
00:04:55,000 --> 00:04:57,000
I said, well, can we borrow one of yours?

64
00:04:57,000 --> 00:04:59,000
And I said, well, Katina, I took off.

65
00:04:59,000 --> 00:05:01,000
I actually took off.

66
00:05:01,000 --> 00:05:02,000
And I wanted to talk about it.

67
00:05:02,000 --> 00:05:04,000
I'm sure it wasn't...

68
00:05:04,000 --> 00:05:07,000
Yeah, you know.

69
00:05:07,000 --> 00:05:12,000
Point being, not really what I would call a wholesome,

70
00:05:12,000 --> 00:05:15,000
engagement, especially considering the alcohol involved.

71
00:05:15,000 --> 00:05:18,000
And the fact that after it was all over,

72
00:05:18,000 --> 00:05:22,000
they took the money back and on some of the gifts

73
00:05:22,000 --> 00:05:24,000
that were supposed to go to the monastery,

74
00:05:24,000 --> 00:05:26,000
they took back for the village.

75
00:05:26,000 --> 00:05:28,000
It was really...

76
00:05:28,000 --> 00:05:32,000
It was a bit of a system that they had set up.

77
00:05:32,000 --> 00:05:36,000
And yeah, this happens in Buddhist circles sometimes

78
00:05:36,000 --> 00:05:39,000
where we lose sight of why are we doing this?

79
00:05:39,000 --> 00:05:40,000
I think what we're doing it,

80
00:05:40,000 --> 00:05:43,000
often they're doing it just because they're excited

81
00:05:43,000 --> 00:05:50,000
to have their village and their community grow.

82
00:05:50,000 --> 00:05:53,000
So the money goes to the community, I think.

83
00:05:53,000 --> 00:05:57,000
The stuff goes for the community.

84
00:06:01,000 --> 00:06:03,000
And they don't realize the great benefit

85
00:06:03,000 --> 00:06:05,000
to actually giving a gift.

86
00:06:05,000 --> 00:06:07,000
And in fact, they don't really...

87
00:06:07,000 --> 00:06:09,000
They aren't really interested in giving gifts.

88
00:06:09,000 --> 00:06:13,000
On the other hand, there was that very same village

89
00:06:13,000 --> 00:06:18,000
ended up being quite awesome about giving arms.

90
00:06:18,000 --> 00:06:21,000
There's another funny aspect of being there.

91
00:06:21,000 --> 00:06:25,000
I was staying with an old monk, two old monks.

92
00:06:25,000 --> 00:06:27,000
So this was later when I was staying with a second old monk.

93
00:06:27,000 --> 00:06:31,000
But I stayed with a first old monk deeper in the forest.

94
00:06:31,000 --> 00:06:38,000
And he told me when he said,

95
00:06:38,000 --> 00:06:40,000
well, you can go on arms around to the village,

96
00:06:40,000 --> 00:06:41,000
but you won't get anything.

97
00:06:41,000 --> 00:06:46,000
You might get some dried noodles or canned fish,

98
00:06:46,000 --> 00:06:47,000
but that's about it.

99
00:06:47,000 --> 00:06:49,000
It won't be enough to eat, but you can go.

100
00:06:49,000 --> 00:06:51,000
And it was three and a half kilometers.

101
00:06:51,000 --> 00:06:53,000
Walk along a dirt gravel road.

102
00:06:53,000 --> 00:06:57,000
That was actually somewhat painful to walk.

103
00:06:57,000 --> 00:07:00,000
But I did walk three and a half kilometers.

104
00:07:00,000 --> 00:07:03,000
And often had to walk three and a half kilometers back,

105
00:07:03,000 --> 00:07:04,000
but not always.

106
00:07:04,000 --> 00:07:07,000
Sometimes someone going to the park,

107
00:07:07,000 --> 00:07:11,000
there was a national park near where the monastery was.

108
00:07:11,000 --> 00:07:12,000
It would give us a ride back.

109
00:07:12,000 --> 00:07:16,000
It would give me or eventually us when I had followed.

110
00:07:20,000 --> 00:07:24,000
But the great thing, once I went and they could see

111
00:07:24,000 --> 00:07:29,000
that I was interested in meditation and wasn't

112
00:07:29,000 --> 00:07:34,000
some crabby old monk.

113
00:07:34,000 --> 00:07:38,000
They were really keen on and eventually the whole village

114
00:07:38,000 --> 00:07:39,000
began to give them.

115
00:07:39,000 --> 00:07:41,000
And they really got the sense of the greatness.

116
00:07:41,000 --> 00:07:44,000
People who I think had never really gotten into it.

117
00:07:44,000 --> 00:07:46,000
And there was one man who was a really devout Buddhist

118
00:07:46,000 --> 00:07:49,000
and very interested in the Dhamma and very knowledgeable

119
00:07:49,000 --> 00:07:51,000
about the Dhamma and we used to talk about the Dhamma.

120
00:07:51,000 --> 00:07:53,000
And he was really impressed.

121
00:07:53,000 --> 00:07:55,000
And he said, this is a great thing.

122
00:07:55,000 --> 00:07:58,000
People who I've never seen these people give before

123
00:07:58,000 --> 00:08:00,000
are now into giving.

124
00:08:00,000 --> 00:08:02,000
So there was that.

125
00:08:02,000 --> 00:08:04,000
But that's really what it's about.

126
00:08:04,000 --> 00:08:05,000
It's the differences.

127
00:08:05,000 --> 00:08:07,000
The differences are about the difference

128
00:08:07,000 --> 00:08:09,000
between spiritual practices.

129
00:08:09,000 --> 00:08:12,000
We have this idea of spiritual practice

130
00:08:12,000 --> 00:08:15,000
in many Buddhist cultures of paying respect to gods

131
00:08:15,000 --> 00:08:16,000
or angels.

132
00:08:16,000 --> 00:08:18,000
And they've started making up angels.

133
00:08:18,000 --> 00:08:21,000
They've taken them from Hindu myths like Ganesha.

134
00:08:21,000 --> 00:08:25,000
People worship this monkey.

135
00:08:25,000 --> 00:08:27,000
I know, sorry, the elephant.

136
00:08:27,000 --> 00:08:32,000
God, and they have all these myths about Ganesha

137
00:08:32,000 --> 00:08:35,000
and Hanuman in this same village.

138
00:08:35,000 --> 00:08:39,000
There was a woman who claimed to be a avatar.

139
00:08:39,000 --> 00:08:44,000
An avatar is a big thing in Thailand and Thailand anyway.

140
00:08:44,000 --> 00:08:47,000
An avatar for Hanuman.

141
00:08:47,000 --> 00:08:50,000
And of course, Hanuman is this legend.

142
00:08:50,000 --> 00:08:52,000
It's a story, really.

143
00:08:52,000 --> 00:08:56,000
There's not even any ancient religious texts.

144
00:08:56,000 --> 00:08:58,000
I think that have Hanuman in the midst,

145
00:08:58,000 --> 00:09:00,000
this tale of the Ramayana,

146
00:09:00,000 --> 00:09:02,000
which is a fairly modern tale.

147
00:09:02,000 --> 00:09:05,000
And yet in India as well, they worship Hanuman

148
00:09:05,000 --> 00:09:06,000
or they seem to.

149
00:09:06,000 --> 00:09:09,000
They have monkeys, monkey statues

150
00:09:09,000 --> 00:09:10,000
in front of their fields

151
00:09:10,000 --> 00:09:12,000
to ward away evil demons and stuff.

152
00:09:15,000 --> 00:09:17,000
But yeah, these sort of things

153
00:09:17,000 --> 00:09:19,000
not worth a quarter of the,

154
00:09:19,000 --> 00:09:24,000
as he says, not a fourth part.

155
00:09:24,000 --> 00:09:27,000
They don't come to a quarter.

156
00:09:27,000 --> 00:09:31,000
They don't even come to a thousandth part

157
00:09:31,000 --> 00:09:34,000
as we were talking about in the early one.

158
00:09:34,000 --> 00:09:36,000
Anyway, so he tells him this.

159
00:09:36,000 --> 00:09:38,000
It's a little bit different.

160
00:09:38,000 --> 00:09:41,000
He takes him, he says,

161
00:09:41,000 --> 00:09:45,000
he says, look, you have to come to see the teacher

162
00:09:45,000 --> 00:09:46,000
and he takes him to see the teacher

163
00:09:46,000 --> 00:09:48,000
and this time he actually says,

164
00:09:48,000 --> 00:09:50,000
one tape, please tell this man

165
00:09:50,000 --> 00:09:52,000
the way to the Brahma world.

166
00:09:52,000 --> 00:09:55,000
But the Buddha says the same thing.

167
00:09:55,000 --> 00:09:56,000
Buddha asks him what he does,

168
00:09:56,000 --> 00:10:00,000
and he says, you can do that for a year.

169
00:10:00,000 --> 00:10:05,000
And yet it will not be worth the smallest piece of offering.

170
00:10:05,000 --> 00:10:09,000
And he says something also a bit different.

171
00:10:09,000 --> 00:10:18,000
Loki, Mahads, and us, Dinandana.

172
00:10:18,000 --> 00:10:22,000
So if you were to give,

173
00:10:22,000 --> 00:10:26,000
if you're instead to give

174
00:10:26,000 --> 00:10:29,000
to the populace,

175
00:10:29,000 --> 00:10:32,000
if you're instead to give to poor people, for example,

176
00:10:32,000 --> 00:10:35,000
that would be, that was one thing

177
00:10:35,000 --> 00:10:36,000
would be a greater value.

178
00:10:36,000 --> 00:10:38,000
So he distinguishes there

179
00:10:38,000 --> 00:10:40,000
and it's an interesting point

180
00:10:40,000 --> 00:10:43,000
because here the Buddha sort of seems to be advocating

181
00:10:43,000 --> 00:10:46,000
giving of arms.

182
00:10:46,000 --> 00:10:48,000
That's what the English translation says

183
00:10:48,000 --> 00:10:50,000
and it looks like what the Pauli says

184
00:10:50,000 --> 00:10:53,000
but it's something to do with

185
00:10:53,000 --> 00:10:57,000
giving gifts to worldly people.

186
00:10:57,000 --> 00:10:59,000
It's much better because he's slaughtering animals

187
00:10:59,000 --> 00:11:02,000
which of course is actually unwholesome.

188
00:11:02,000 --> 00:11:05,000
But then he goes on to saying,

189
00:11:05,000 --> 00:11:09,000
Bhasana, Bhasana, Jibhana, Mamasavakana,

190
00:11:09,000 --> 00:11:14,000
one dandhana to pay respect

191
00:11:14,000 --> 00:11:17,000
to my disciples.

192
00:11:22,000 --> 00:11:23,000
Yeah.

193
00:11:23,000 --> 00:11:25,000
The Kṛṣṇa Jītana,

194
00:11:25,000 --> 00:11:29,000
the wholesome mind that comes is far more powerful.

195
00:11:29,000 --> 00:11:33,000
I mean, I think he's quite understanding the truth here

196
00:11:33,000 --> 00:11:38,000
because honestly, sacrificing animals is not only a portion,

197
00:11:38,000 --> 00:11:41,000
it's not only a fraction as good.

198
00:11:41,000 --> 00:11:42,000
It's actually harmful.

199
00:11:42,000 --> 00:11:45,000
There's nothing good about offering slaughtered

200
00:11:45,000 --> 00:11:48,000
and slaughtering animals as an offering.

201
00:11:48,000 --> 00:11:50,000
So the Buddha, as I think being a little bit

202
00:11:50,000 --> 00:11:52,000
going a little bit easy on him

203
00:11:52,000 --> 00:11:54,000
because probably if he were to say,

204
00:11:54,000 --> 00:11:57,000
you're actually that's actually an unwholesome thing

205
00:11:57,000 --> 00:12:01,000
then they wouldn't perhaps set the guy off

206
00:12:01,000 --> 00:12:03,000
and make him upset at the Buddha.

207
00:12:03,000 --> 00:12:05,000
So he couches it a bit nicer.

208
00:12:05,000 --> 00:12:10,000
He doesn't say how much less good it is than a quarter.

209
00:12:10,000 --> 00:12:15,000
But it's at least less than a quarter is good.

210
00:12:15,000 --> 00:12:17,000
It's actually harmful.

211
00:12:17,000 --> 00:12:19,000
And that's a true of many spiritual practices.

212
00:12:19,000 --> 00:12:23,000
So again, as I've talked about,

213
00:12:23,000 --> 00:12:27,000
we have to be aware of the true benefit of our spiritual practices.

214
00:12:27,000 --> 00:12:31,000
We can't just think, I'm doing this,

215
00:12:31,000 --> 00:12:33,000
and this is what people tell me to do.

216
00:12:33,000 --> 00:12:35,000
This is spiritual, and therefore it's good.

217
00:12:35,000 --> 00:12:38,000
Spiritual practice can actually be harmful.

218
00:12:38,000 --> 00:12:42,000
This was a spiritual practice that obviously was

219
00:12:42,000 --> 00:12:45,000
offering killing animals in the name of God.

220
00:12:45,000 --> 00:12:52,000
Sometimes we have the idea that taking drugs is spiritual.

221
00:12:52,000 --> 00:12:55,000
And I'm not going to go out and say that that's harmful,

222
00:12:55,000 --> 00:13:00,000
but my suspicion is that it has a great potential for harm

223
00:13:00,000 --> 00:13:02,000
if not being outright harmful.

224
00:13:02,000 --> 00:13:05,000
I think a lot of people would say that taking even psychedelic drugs

225
00:13:05,000 --> 00:13:10,000
and hoping that it somehow has been official as a spiritual practice.

226
00:13:10,000 --> 00:13:13,000
I think some people would say that that is outright harmful.

227
00:13:13,000 --> 00:13:15,000
I'm not sure that I go quite so far

228
00:13:15,000 --> 00:13:18,000
because there is an argument to be made that it opens up your mind

229
00:13:18,000 --> 00:13:21,000
to alternate states of reality.

230
00:13:21,000 --> 00:13:24,000
But I think there's a lot of delusion involved.

231
00:13:24,000 --> 00:13:29,000
I think the idea that those states have some meaning or purpose or value

232
00:13:29,000 --> 00:13:32,000
when in fact they're all messed up with delusion.

233
00:13:32,000 --> 00:13:35,000
I've been there done that.

234
00:13:35,000 --> 00:13:39,000
It's just all some mess of our mind.

235
00:13:39,000 --> 00:13:44,000
What our mind can come up with when it's doped up.

236
00:13:44,000 --> 00:13:48,000
And it's high.

237
00:13:48,000 --> 00:13:50,000
So these kind of things.

238
00:13:50,000 --> 00:13:55,000
And you compare that, for example, taking drugs as a spiritual practice

239
00:13:55,000 --> 00:13:58,000
to a Buddhist spiritual practice of giving charity

240
00:13:58,000 --> 00:14:01,000
or of even just holding your hands up and paying respect

241
00:14:01,000 --> 00:14:03,000
to someone who's worthy of respect.

242
00:14:03,000 --> 00:14:05,000
Who would think of that?

243
00:14:05,000 --> 00:14:11,000
It shows how far many people are from true spiritual practice.

244
00:14:11,000 --> 00:14:14,000
Maybe people say, oh, taking drugs as a spiritual practice

245
00:14:14,000 --> 00:14:22,000
or doing expansive rituals or offering copious sacrifices

246
00:14:22,000 --> 00:14:24,000
or this kind of thing.

247
00:14:24,000 --> 00:14:27,000
Doing very complex or very mystical things.

248
00:14:27,000 --> 00:14:28,000
Maybe dancing.

249
00:14:28,000 --> 00:14:31,000
People say, in the West we become very hedonistic

250
00:14:31,000 --> 00:14:33,000
with our spirituality.

251
00:14:33,000 --> 00:14:36,000
So maybe people say group orgies and karmic sex

252
00:14:36,000 --> 00:14:37,000
and that kind of thing.

253
00:14:37,000 --> 00:14:38,000
These are spiritual.

254
00:14:38,000 --> 00:14:42,000
I can contrast that to the Buddha's idea of what true spirituality is.

255
00:14:42,000 --> 00:14:44,000
Holding your hands up and paying respect.

256
00:14:44,000 --> 00:14:47,000
That is far more spiritual.

257
00:14:47,000 --> 00:14:51,000
So such a mundane seeming thing, but far more spiritual

258
00:14:51,000 --> 00:14:55,000
and far more beneficial, even just for a moment

259
00:14:55,000 --> 00:14:57,000
than a hundred years of dancing.

260
00:14:57,000 --> 00:15:04,000
Spiritual dancing or spiritual music or spiritual sex.

261
00:15:04,000 --> 00:15:11,000
Drugs or all these things.

262
00:15:11,000 --> 00:15:15,000
So for someone looking for Punea, which we all are,

263
00:15:15,000 --> 00:15:16,000
Punea is goodness.

264
00:15:16,000 --> 00:15:18,000
Even if we're just set on meditation,

265
00:15:18,000 --> 00:15:23,000
it's important not to become self-centered.

266
00:15:23,000 --> 00:15:25,000
In the sense of me, me, me.

267
00:15:25,000 --> 00:15:29,000
I want to become spiritually enlightened.

268
00:15:29,000 --> 00:15:31,000
Enlightenment is about letting go.

269
00:15:31,000 --> 00:15:34,000
It's a lot of self-sacrifice.

270
00:15:34,000 --> 00:15:37,000
When enlightened being would be able to give up anything,

271
00:15:37,000 --> 00:15:39,000
if someone asks them for something,

272
00:15:39,000 --> 00:15:43,000
a sense of self, of helping themselves.

273
00:15:43,000 --> 00:15:45,000
They have no qualms there.

274
00:15:45,000 --> 00:15:48,000
And they consider what is proper to do and they do it

275
00:15:48,000 --> 00:15:51,000
without any attachment, without any greed.

276
00:15:51,000 --> 00:15:53,000
And they're very respectful.

277
00:15:53,000 --> 00:15:55,000
They honor those who have helped them.

278
00:15:55,000 --> 00:15:57,000
They're grateful to those who have helped them.

279
00:15:57,000 --> 00:15:58,000
That kind of thing.

280
00:15:58,000 --> 00:16:00,000
And they're very down to earth.

281
00:16:00,000 --> 00:16:03,000
So noble spirituality is like this.

282
00:16:03,000 --> 00:16:05,000
Well, it's spiritual.

283
00:16:05,000 --> 00:16:08,000
Well, paying respect to those who are worthy of respect,

284
00:16:08,000 --> 00:16:11,000
giving gifts to those who are worthy of gifts,

285
00:16:11,000 --> 00:16:14,000
and mostly just being aware of being here,

286
00:16:14,000 --> 00:16:17,000
being present, being with mundane reality,

287
00:16:17,000 --> 00:16:21,000
understanding truth, true reality,

288
00:16:21,000 --> 00:16:26,000
and not being concerned with mysticism or altered states of existence

289
00:16:26,000 --> 00:16:27,000
or that kind of thing.

290
00:16:27,000 --> 00:16:28,000
Astral travel.

291
00:16:28,000 --> 00:16:30,000
Anyone who's obsessed with astral travel.

292
00:16:30,000 --> 00:16:33,000
I mean, it's not to say enlightened people can't practice it,

293
00:16:33,000 --> 00:16:36,000
but people who are striving for that

294
00:16:36,000 --> 00:16:40,000
are really missing the point.

295
00:16:40,000 --> 00:16:44,000
Mohasi Sayyada tells a story of a woman who he knew

296
00:16:44,000 --> 00:16:49,000
who was working very hard to try and see out of her ears.

297
00:16:49,000 --> 00:16:52,000
This was she was trying to gain the spiritual ability

298
00:16:52,000 --> 00:16:56,000
to see out of her ears.

299
00:16:56,000 --> 00:17:00,000
And he said, and this one, her eyes worked perfectly fine.

300
00:17:00,000 --> 00:17:03,000
So we get often on the wrong path.

301
00:17:03,000 --> 00:17:07,000
True spirituality is often easy to mistake

302
00:17:07,000 --> 00:17:12,000
for ordinary reality.

303
00:17:12,000 --> 00:17:15,000
I always say that enlightened people are what we think we are.

304
00:17:15,000 --> 00:17:17,000
We all think of ourselves as living normally

305
00:17:17,000 --> 00:17:19,000
we think ourselves as normal.

306
00:17:19,000 --> 00:17:21,000
But we're not.

307
00:17:21,000 --> 00:17:25,000
Our defilements take us away from what we think we are.

308
00:17:25,000 --> 00:17:28,000
Take us away from mundane reality.

309
00:17:28,000 --> 00:17:31,000
So I've made much of this very simple verse,

310
00:17:31,000 --> 00:17:34,000
which is very much like the other two verses.

311
00:17:34,000 --> 00:17:36,000
And I promise the next one is different.

312
00:17:36,000 --> 00:17:38,000
Still still along the same vein,

313
00:17:38,000 --> 00:17:42,000
but it's a totally different context and story, everything.

314
00:17:42,000 --> 00:17:45,000
And it's a very actually one of them.

315
00:17:45,000 --> 00:17:51,000
Probably the if not one of the if not the most famous

316
00:17:51,000 --> 00:17:55,000
verses in the double pad are coming up next.

317
00:17:55,000 --> 00:17:57,000
Next time.

318
00:17:57,000 --> 00:18:02,000
And then after that we have the story of Sun Geetcher,

319
00:18:02,000 --> 00:18:05,000
which is a very nice story, and so on and so on.

320
00:18:05,000 --> 00:18:06,000
Not all the stories are long.

321
00:18:06,000 --> 00:18:08,000
Some of them are very short as you can see.

322
00:18:08,000 --> 00:18:10,000
Some of them are very long.

323
00:18:10,000 --> 00:18:13,000
I think the longest ones have already gone by.

324
00:18:13,000 --> 00:18:17,000
So we're not going to have great storytelling.

325
00:18:17,000 --> 00:18:21,000
Not all of it is going to be great stories.

326
00:18:21,000 --> 00:18:24,000
But always something new and some new message

327
00:18:24,000 --> 00:18:26,000
to be found in the Dhamapanda.

328
00:18:26,000 --> 00:18:30,000
And of course the verses themselves are things you can reflect upon.

329
00:18:30,000 --> 00:18:32,000
And what is important?

330
00:18:32,000 --> 00:18:35,000
This is the Sahasoga, which is a thousand.

331
00:18:35,000 --> 00:18:39,000
So it's contrasting single things to thousands of things for the most part.

332
00:18:39,000 --> 00:18:41,000
A thousand of something useless.

333
00:18:41,000 --> 00:18:44,000
That is one thing that is useful.

334
00:18:44,000 --> 00:18:48,000
So that's the Dhamapanda for tonight.

335
00:18:48,000 --> 00:18:51,000
Thank you all for tuning in.

336
00:18:51,000 --> 00:18:56,000
Keep practicing, it'll be well.

