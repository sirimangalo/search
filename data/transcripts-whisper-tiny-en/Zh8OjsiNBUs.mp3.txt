Welcome. Today we're going to give it a tour of the meditation center in monastery.
So, we're starting at the cookie where I'm staying, which is actually a cave.
If you turn over here, you can see. This is where I've been staying.
You can see it's just a shallow cave that's been turned into a cookie.
This is one of our caves. We have three caves too, which we're using.
The third one we have to mix.
So, we'll go down here, I'll take it to the whole place.
We have an assisting who's working in the camera.
This is the Bodhi tree. This is an offspring of the original Bodhi tree in India.
I've taken from no one in another corner.
This is the second cave area.
If you turn over there, you can see.
This is where you can see the third cave, the second cave.
Then we have one meditator, our first meditator is staying in there.
Here's an outhouse, but we can make cookies all along here.
This is an idea here to pick up against the quick fall, we can make cookies.
Now we're down below the place where we're just up there, and we're heading back underneath it.
So, all these cliffs we could actually make cookies in here, but there's a pool in there.
I think there's a big lizard that goes in there.
So, again, if we go up here, we get back to the cave where we started, but I'll take you back to the other way.
So, we're going to go up here, and we're going to go up here, and we're going to go up here.
Now we're heading into the main monastery area.
This is where the kitchen is, and you may have a couple of other things.
Those are new bathrooms being built.
Here is the new kitchen and dining hall that's being built.
There's the old monk who is to run the place.
This is the main hall you can go in there, but sure is not.
This is the main hall.
Right now this is being used as everything, office, meditation hall, kitchen dining room.
So, once that dining room is finished, and another cookie is finished, we'll take everything out of here.
This will be our team just for meditation. It's still small, but it's usable.
Then I'll get your shoes.
So, this is the way, and this is where we're going into the bathroom, on the ground, and into the village, and into the main road, and back to...
Here are two rooms. This is where we want to put cement down.
This is the main hall.
This is the main hall, and this is the main hall.
So, here is the place that was offered.
Here is the new cookie that's being built.
We'll go over there in a second, but I'm going to take it in there.
I'm going to get up there.
Thank you.
So, this piece of property actually doesn't belong to us. It's a little piece of property in between, but at one point we use another pair of uses.
I'm going to clean up the rocks.
I'm going to clean it up.
We're going up.
Okay, but here we are.
This is a rubber forest. It doesn't belong to us.
It's all just forest. This is above the caves. The second cave that I took you to. This is above.
I don't think we can go right to the edge, but we'll go close and then we'll be right above that cave.
Oh, yeah.
I'm potentially it has a nice new view, but you have to clear out all.
That's a little bit. I won't take it for it. It's just part of our monitor.
I'm just going to clean it up.
I'm going to clean it up.
I'm going to clean it up.
I'm going to clean it up.
I'm going to clean it up.
I'm going to clean it up.
I'm going to clean it up.
I'm going to clean it up.
Now we're heading back up to where we started.
This is another cave that I can try and do quickly.
There you have it. That is our monastery and meditation center so far.
Not so far away from the village, the city that it's difficult to get to.
It's actually quite reachable.
I'm going to find peace and solitude to undertake the learning about ourselves.
It's open if people would like to visit.
If you're interested, send us a note and we're going to do our best.
It's not that scary.
It's actually quite civilized.
