1
00:00:00,000 --> 00:00:07,920
How do Buddhists view military service?

2
00:00:07,920 --> 00:00:11,800
This is interesting because it actually relates to the monk question I think or the answer

3
00:00:11,800 --> 00:00:18,680
does because the word military service has come to mean so many different things, right?

4
00:00:18,680 --> 00:00:23,520
You don't have to necessarily kill people to be in the military.

5
00:00:23,520 --> 00:00:28,520
Military can stop floods, for example, right?

6
00:00:28,520 --> 00:00:33,560
If the military is put to use stopping floods, that's a great thing.

7
00:00:33,560 --> 00:00:37,760
You took away all the weapons from the military, took away all of their weapons and gave

8
00:00:37,760 --> 00:00:51,880
them shovels and what sandbags, then I think it would be a good thing.

9
00:00:51,880 --> 00:00:59,520
You have a standing army that looks after people's welfare.

10
00:00:59,520 --> 00:01:12,560
But no, obviously the military is for the purpose of doing battle, causing harm, hurting

11
00:01:12,560 --> 00:01:20,760
other beings, maybe in defense, but a person who undertakes who enters the military

12
00:01:20,760 --> 00:01:26,600
is doing it with the, I believe, with the understanding and the intention that they are

13
00:01:26,600 --> 00:01:37,280
going to bring violence to other beings, either, you know, and as I said, they can think

14
00:01:37,280 --> 00:01:48,480
of it as in defense, but it's a violent profession by its very nature.

15
00:01:48,480 --> 00:01:53,600
I don't know, I guess my question would be how, in what way would you think that military

16
00:01:53,600 --> 00:01:59,560
service would be a positive thing?

17
00:01:59,560 --> 00:02:07,200
Or is there any way that you can think of it as not being a negative thing, I suppose?

18
00:02:07,200 --> 00:02:20,800
Okay, my job would be to help children of foreign countries not hate Americans, but the

19
00:02:20,800 --> 00:02:25,200
question is, if there were a war, would you be asked to pick up a gun and fight?

20
00:02:25,200 --> 00:02:27,960
I mean, isn't, I don't really know how that's what I said.

21
00:02:27,960 --> 00:02:33,040
The military has become something quite a bit more than just guys with guns, but I still

22
00:02:33,040 --> 00:02:44,560
think you have, you're given a duty to fight, are you not?

23
00:02:44,560 --> 00:02:50,200
There are many issues here, because I know that you can, being in the American military,

24
00:02:50,200 --> 00:02:57,200
for example, can be a great way, theoretically, to help people.

25
00:02:57,200 --> 00:03:06,960
You're given a salary, you're given a lot of equipment and resources, and yeah, joining

26
00:03:06,960 --> 00:03:12,320
other organizations might be a great thing, but there's something to be said about the

27
00:03:12,320 --> 00:03:15,120
clout of the American military in that sense.

28
00:03:15,120 --> 00:03:20,560
The problem is that I don't understand it fully, but as I understand, when you join

29
00:03:20,560 --> 00:03:29,640
the military, no, I guess that's not true, no, if you're intelligence and in the Air Force,

30
00:03:29,640 --> 00:03:36,360
you're not involved with killing, more like a desk charmer, but then I guess there's

31
00:03:36,360 --> 00:03:43,320
the moral question, is there a moral question of joining an organization that at its

32
00:03:43,320 --> 00:03:55,640
very nature is for the purpose of, I don't want to say, the purpose is to engage in combat

33
00:03:55,640 --> 00:04:09,600
or to, well, it's a military, are there ethical, are there ethical problems there?

34
00:04:09,600 --> 00:04:16,280
I guess I'm, I'm not, I don't have strong feelings about this, you take your job, and

35
00:04:16,280 --> 00:04:23,160
if your job is for the purpose of helping other people, and if through your job, you can

36
00:04:23,160 --> 00:04:30,080
help other people, then power to you, even if your job doesn't help other people.

37
00:04:30,080 --> 00:04:37,800
I'm not of the bent that you have to be concerned with, with anything larger than doing

38
00:04:37,800 --> 00:04:43,120
your own task, and I know people are, and there's a lot of people who have this social

39
00:04:43,120 --> 00:04:49,200
conscience that they feel like they shouldn't work for an organization, this, this is

40
00:04:49,200 --> 00:04:53,600
the same, same debate about vegetarianism, right?

41
00:04:53,600 --> 00:04:59,760
You can't eat meat because you're contributing to the killing of animals, you can't use

42
00:04:59,760 --> 00:05:07,840
Gillette razors because you're contributing to the cruelty towards animals.

43
00:05:07,840 --> 00:05:13,800
You can't buy certain stocks, you can buy certain products, you can't, you know, don't

44
00:05:13,800 --> 00:05:21,000
buy Nike, don't buy this, don't buy that, these sort of things, and I don't buy it,

45
00:05:21,000 --> 00:05:27,920
it seems to put it in buy it, and there's even, we have this wonderful example that we

46
00:05:27,920 --> 00:05:32,840
always tell, this woman, whose husband was a hunter.

47
00:05:32,840 --> 00:05:36,880
When she was young, she went to practice meditation, listened to the Buddha's teaching,

48
00:05:36,880 --> 00:05:46,200
and became a sotapana, which means she was, she was in a sense enlightened, and then

49
00:05:46,200 --> 00:05:50,720
she got married because in India they often, their marriage would have been decided for

50
00:05:50,720 --> 00:05:59,400
them, so she was married off to this man, who was a hunter, and every day she would

51
00:05:59,400 --> 00:06:04,680
clean his traps, clean the blood and the guts and whatever, the hair off of these traps,

52
00:06:04,680 --> 00:06:11,120
and put them out on the table for him, and every day he would go out and kill animals.

53
00:06:11,120 --> 00:06:16,280
And so they asked, how is this possible that she could do this?

54
00:06:16,280 --> 00:06:22,440
And the Buddha said it's because he said it's like if your hand has no cut, if you have

55
00:06:22,440 --> 00:06:28,920
no wound on your hand, you can handle poison, but if you have a wound on your hand, you

56
00:06:28,920 --> 00:06:40,320
can't handle poison, he says a little more poetically than that, but the point being

57
00:06:40,320 --> 00:06:50,200
a person has no bad intentions, the act of cleaning, cleaning blood and gore off of these

58
00:06:50,200 --> 00:06:57,120
killing machines, even to that extent it's not considered to be immoral, because it's

59
00:06:57,120 --> 00:07:03,080
not, it's the same as eating meat, the act itself is never immoral, this is the Buddha

60
00:07:03,080 --> 00:07:08,160
actually taught against the idea of karma, he said karma is not the important thing,

61
00:07:08,160 --> 00:07:12,960
it's the intention, so if the woman is intending, oh here I'm helping my husband making

62
00:07:12,960 --> 00:07:19,560
him happy and I'm doing my job, you know if I don't he might beat me kind of thing,

63
00:07:19,560 --> 00:07:26,520
that might have had a part in it, because there's some horrible things to go on out there,

64
00:07:26,520 --> 00:07:33,040
but even, you know, he's a hunter, he's not the most cultured of beings, but even without

65
00:07:33,040 --> 00:07:42,320
that, just the idea that she's doing her job and doing what is her way, and for a person

66
00:07:42,320 --> 00:07:51,080
who eats meat, they're just eating the food in order to gain the benefit, this is in

67
00:07:51,080 --> 00:07:56,480
line I think with, this is certainly in line with my understanding of the Buddha's teaching,

68
00:07:56,480 --> 00:08:10,680
because we're a deal in terms of experience, don't some monks practice some military aspects

69
00:08:10,680 --> 00:08:15,960
like shelving, I'll never live this one down, no monks, Buddhist monks don't practice

70
00:08:15,960 --> 00:08:22,440
shelving, shelving monks practice shelving, I'm sorry, they call themselves Buddhist, but

71
00:08:22,440 --> 00:08:30,640
in wood way does Kung Fu have to do it, the Buddha's teaching, it's well to each their

72
00:08:30,640 --> 00:08:39,600
own, but no, shelving monks are a very specific type of monk, and I don't know, I guess

73
00:08:39,600 --> 00:08:43,240
I just can't answer that, because it's so different from what I do, I used to practice

74
00:08:43,240 --> 00:08:51,680
karate, but that was a long time ago, karate isn't kung fu, but it's not a part of the

75
00:08:51,680 --> 00:08:57,320
Buddha's teaching for sure, those monks are doing it, I mean there's a whole history there

76
00:08:57,320 --> 00:09:02,240
of how it came about because there was war and the monks had to defend themselves or whatever,

77
00:09:02,240 --> 00:09:09,560
not sure how it all came about, anyway, question on joining the military, I say, as I said,

78
00:09:09,560 --> 00:09:13,920
take it as your job, because the American military is such a huge organization and so many

79
00:09:13,920 --> 00:09:20,240
people aren't involved with the killing part of it, but you have to be fairly careful

80
00:09:20,240 --> 00:09:33,440
there as well because it's, if you are put in a position where you have to be involved

81
00:09:33,440 --> 00:09:43,200
with or encourage the use of force or the distribution of weapons, for example, it's

82
00:09:43,200 --> 00:09:48,240
certainly something that you have to take into consideration, be careful that you don't somehow

83
00:09:48,240 --> 00:09:55,840
get conscripted and your soldier going fight, kind of thing, but you know, we're living in hard

84
00:09:55,840 --> 00:10:01,680
times if you have a chance to help people then power to you, go out there and help, because

85
00:10:01,680 --> 00:10:08,160
actually in Canada the military is mostly involved in peacekeeping, helping to bring peace or

86
00:10:08,160 --> 00:10:24,080
they were, I don't know what it's like, anyway, military.

