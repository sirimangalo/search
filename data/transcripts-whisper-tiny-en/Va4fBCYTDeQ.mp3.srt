1
00:00:00,000 --> 00:00:09,000
I find it easier to meditate in public places such as on the train going to work than by myself at home.

2
00:00:09,000 --> 00:00:11,000
Is this unusual?

3
00:00:11,000 --> 00:00:13,000
I fear of everything.

4
00:00:13,000 --> 00:00:19,000
Some people are afraid of leaving their own, some people unable to find peace at home.

5
00:00:19,000 --> 00:00:29,000
It sounds a little bit unusual in the beginning but it has to meditate in public.

6
00:00:29,000 --> 00:00:38,000
It definitely has some great advantage that when you do group meditation, for example,

7
00:00:38,000 --> 00:00:43,000
then you feel the power of the group.

8
00:00:43,000 --> 00:00:50,000
And that might happen to you when you are in a train or so.

9
00:00:50,000 --> 00:00:59,000
Something in your mind, don't want to give up when others are there.

10
00:00:59,000 --> 00:01:08,000
So you have another really other level of discipline that you might not have when you are practicing alone.

11
00:01:08,000 --> 00:01:19,000
When you are practicing alone in your room, you really need to build up all the discipline to sit down and meditate.

12
00:01:19,000 --> 00:01:27,000
From within you and in public places or in groups,

13
00:01:27,000 --> 00:01:38,000
there is something happening that you might be in the beginning that it is coming from the ego,

14
00:01:38,000 --> 00:01:41,000
that it doesn't want to give up or something like that.

15
00:01:41,000 --> 00:01:50,000
But it really is a power that comes from meditation in public places.

16
00:01:50,000 --> 00:02:10,000
Although I think it would be important to have the same intensity or the same strength of meditation and the same ability to meditate when you are alone at home.

17
00:02:10,000 --> 00:02:17,000
I mean, you can compare another level of being at home alone and being at home with a teacher.

18
00:02:17,000 --> 00:02:19,000
As I have done some Skype courses.

19
00:02:19,000 --> 00:02:22,000
And it totally changes people's meditation.

20
00:02:22,000 --> 00:02:29,000
They were just having this contact knowing that they had to meet with a teacher every day.

21
00:02:29,000 --> 00:02:39,000
When I was doing my foundation course, I actually had the idea that someone probably gave to me that our teacher was watching us.

22
00:02:39,000 --> 00:02:46,000
So he was, and of course, this is so ego, you were so egotistical that we think that he is watching us.

23
00:02:46,000 --> 00:02:52,000
And he is there with us even though there is 100 people or so in the monastery.

24
00:02:52,000 --> 00:02:59,000
And so not only do we think that he has magical powers, but we think that he is using them to take care of us particularly.

25
00:02:59,000 --> 00:03:02,000
And so I thought, here he was watching me.

26
00:03:02,000 --> 00:03:05,000
And so that was a real encouragement.

27
00:03:05,000 --> 00:03:12,000
As I would say, it's different levels. You, in the beginning, this is a useful thing.

28
00:03:12,000 --> 00:03:18,000
But eventually you don't let it be a crutch.

29
00:03:18,000 --> 00:03:22,000
This is the same as sitting on cushions.

30
00:03:22,000 --> 00:03:34,000
We tend to discourage people from buying and sitting on these croissants and marshmallows that they sit on.

31
00:03:34,000 --> 00:03:36,000
But not because there is anything wrong.

32
00:03:36,000 --> 00:03:40,000
I mean, not because we don't sympathize that in the beginning they are useful.

33
00:03:40,000 --> 00:03:43,000
But that in the long term, it's only going to become a crutch for you.

34
00:03:43,000 --> 00:03:49,000
And then you'll have trouble sitting doing meditation when you're in the airport or anywhere.

35
00:03:49,000 --> 00:03:53,000
You'll always have to lug around this croissant.

36
00:03:53,000 --> 00:04:12,000
But here you go.

