1
00:00:00,000 --> 00:00:11,360
Okay, so today we have Ian is here from the south of the border, know you're from America.

2
00:00:11,360 --> 00:00:12,360
Virginia Beach.

3
00:00:12,360 --> 00:00:19,520
I have to turn on the mic to turn the switch, kind of like a Virginia Beach, a USA, tell

4
00:00:19,520 --> 00:00:20,520
us about yourself.

5
00:00:20,520 --> 00:00:24,880
Tell us about, you don't need to know about you, you don't have to give up personal

6
00:00:24,880 --> 00:00:25,880
information.

7
00:00:25,880 --> 00:00:35,120
What I want to know is, before the practice, before the scores, and after the scores,

8
00:00:35,120 --> 00:00:40,240
what has changed.

9
00:00:40,240 --> 00:00:49,360
Before I want to say, I knew these things, but it was only an intellect, I was telling

10
00:00:49,360 --> 00:00:54,640
my friends, you're suffering, this is why you're suffering, this is why we all suffer,

11
00:00:54,640 --> 00:01:04,760
but after spending the time here, going to step by step the moment by moment, you begin

12
00:01:04,760 --> 00:01:13,960
to know exactly why it is, it's something that you can only know through direct experience

13
00:01:13,960 --> 00:01:26,920
it's, words can't really explain languages and sufficient, how do you feel now?

14
00:01:26,920 --> 00:01:41,080
That's your feeling today, been today overwhelmed, it's very calm, I feel like I can handle

15
00:01:41,080 --> 00:01:48,080
a lot more strong, strong, invincible, and some might be, I'll have you not even

16
00:01:48,080 --> 00:01:49,080
anymore.

17
00:01:49,080 --> 00:01:55,200
You also see, one of the common things, I'm not trying to put words in your mouth, but

18
00:01:55,200 --> 00:02:01,720
just to ask about it, it's also common to have a better picture of what you have yet

19
00:02:01,720 --> 00:02:04,920
to, the work you have yet to do.

20
00:02:04,920 --> 00:02:13,560
Definitely, it's been said before, but the iceberg, what I thought I had to do is, just

21
00:02:13,560 --> 00:02:22,040
the tip, there's a lot more, I see more work to be done, you know, today I was getting

22
00:02:22,040 --> 00:02:27,360
my ticket and I was, you know, having to get my password and I was getting frustrated

23
00:02:27,360 --> 00:02:36,280
with that, I was like, oh my goodness, it's still here, still, still things that need to

24
00:02:36,280 --> 00:02:56,560
be uprooted and understood, okay, thank you, and the end is actually already, I'm gonna

25
00:02:56,560 --> 00:03:23,880
do the second course, you know, expert camera skills, this is, all right, and sticking

26
00:03:23,880 --> 00:03:31,960
around, already asking about the instructor course, so we may actually have a candidate,

27
00:03:31,960 --> 00:03:36,160
first candidate here in Canada, I haven't taught an instructor course in meditation a

28
00:03:36,160 --> 00:03:37,160
long time.

29
00:03:37,160 --> 00:03:45,720
The idea of being an instructor is, well, talk about that some other time, it's not like

30
00:03:45,720 --> 00:03:52,900
you suddenly become a teacher, but you learn the technical skills necessary to need someone

31
00:03:52,900 --> 00:04:00,960
through basic practice, anyway, tonight I wanted to talk about, about this, the benefits

32
00:04:00,960 --> 00:04:11,160
of the practice, more specifically why we practice, and it's a bit of a trick question,

33
00:04:11,160 --> 00:04:19,880
actually, I'm not actually, I want to distinguish between benefits and reasons, maybe

34
00:04:19,880 --> 00:04:28,920
not even reasons, it's not quite the right word, and because we have this problem, Buddhism

35
00:04:28,920 --> 00:04:39,420
is about freeing ourselves from desire and yet, isn't that a desire, right, I mean, to some

36
00:04:39,420 --> 00:04:45,400
extent it's the problem with semantics, but to some extent it's really not, it really

37
00:04:45,400 --> 00:04:52,640
is an apt question, isn't the desire to be free from desire, also a desire, I would say

38
00:04:52,640 --> 00:05:03,180
yes it is, and therefore it's problematic, right, so we have this dilemma, then we can talk

39
00:05:03,180 --> 00:05:08,780
about the benefits of the practice, there's lots of them, and they're great, they're great

40
00:05:08,780 --> 00:05:16,760
for encouraging meditators, that's what they're good for, so we have the five reasons why

41
00:05:16,760 --> 00:05:21,720
the Buddha taught Satyipatana, what he said, what does Satyipatana, what does the practice

42
00:05:21,720 --> 00:05:30,120
of Satyi, of mindfulness, what does it lead to, leads to purity, your mind becomes more

43
00:05:30,120 --> 00:05:42,840
pure, Satyayana, we so dear, we so dear, Satyayana, we so dear, it allows you to go beyond

44
00:05:42,840 --> 00:05:52,440
mental illness, sorrow, lamentation, despair, depression, anxiety, all these problems that

45
00:05:52,440 --> 00:06:02,660
we have, it helps you overcome suffering, physical suffering and mental suffering, and it

46
00:06:02,660 --> 00:06:10,320
leads you to the right path, it leads you to the goal to freedom to nibana, so we have

47
00:06:10,320 --> 00:06:14,920
all of these, these are great reasons to practice, who wouldn't want to pure mind, right,

48
00:06:14,920 --> 00:06:17,920
who wouldn't want to be able to say that their mind is pure, you don't hear people

49
00:06:17,920 --> 00:06:24,000
saying, oh I'm so glad I'm such a corrupt individual, I don't know, maybe such people

50
00:06:24,000 --> 00:06:30,320
who do exist, I don't meet such people, I'm blessed to meet, mostly people who are very

51
00:06:30,320 --> 00:06:39,520
much interested in the pure mind, and so on, being free from suffering sounds great, but

52
00:06:39,520 --> 00:06:47,480
the problem is when you cling to it, when you even strive for it, you get stuck because

53
00:06:47,480 --> 00:06:51,760
you're clinging, and clinging is the cause of suffering, you're craving, grieving is the

54
00:06:51,760 --> 00:07:00,520
cause of suffering, at gentong always talks about four benefits, these are good four things

55
00:07:00,520 --> 00:07:05,760
to tell those of you who have finished the course, so listen up again, this is for you,

56
00:07:05,760 --> 00:07:09,920
there are four things that you get from the practice, from doing, let's say the foundation

57
00:07:09,920 --> 00:07:17,440
course, or foundation course in insight meditation, and the first is mindfulness, mindful

58
00:07:17,440 --> 00:07:23,640
ness is actually a goal, it's a great wonderful outcome, this is what I meant by invincible,

59
00:07:23,640 --> 00:07:29,840
no you're not invincible, but when you're mindful, that mind state is an invincible

60
00:07:29,840 --> 00:07:37,840
mind state, so it's an incredibly powerful tool, because suddenly there's no you, there's

61
00:07:37,840 --> 00:07:48,720
no, there's nothing to be hurt, there's no problem, problems disappear, there's only

62
00:07:48,720 --> 00:08:01,600
events, there's only experiences, you know, if I were giving a talk in real life, we wouldn't

63
00:08:01,600 --> 00:08:08,360
allow for all this chatter, it's for like in Sri Lanka, they really have all about asking

64
00:08:08,360 --> 00:08:14,240
questions during the dumb to talk, but save the chatter for after please, I don't mind

65
00:08:14,240 --> 00:08:19,200
but it's got to be distracting for the audience, it's right on second life, they're

66
00:08:19,200 --> 00:08:25,400
chattering, it's all wholesome, but let's just save the talk for afterwards, there's

67
00:08:25,400 --> 00:08:32,400
a time, galaena dhamma sawanamu, there's a time to listen to the dhamma, galaena dhamma saagacha,

68
00:08:32,400 --> 00:08:48,720
there's a time to talk about, discuss the dhamma, discussion after, so sati mindfulness

69
00:08:48,720 --> 00:08:52,720
is the first benefit, it's actually a benefit of the practice, why, because you have this

70
00:08:52,720 --> 00:09:01,040
wonderful tool, you can practice it throughout your life, any problems that come up, you

71
00:09:01,040 --> 00:09:11,480
can find challenges, conflicts, anything, work, study, relationships, physical suffering,

72
00:09:11,480 --> 00:09:19,920
mental suffering, loss, mindfulness is this wonderful tool that you get walking down the

73
00:09:19,920 --> 00:09:24,040
street, what do you do walking, walking, there's a tool that's applicable everywhere, brushing

74
00:09:24,040 --> 00:09:35,880
your teeth is brushing, brushing, the second one, sort of a corralery is happiness, there's

75
00:09:35,880 --> 00:09:41,080
a result of being mindful, you find happiness, adjunct on talks about this as heaven, going

76
00:09:41,080 --> 00:09:48,360
to heaven, but that's just one form of happiness, heaven is of course a great happiness,

77
00:09:48,360 --> 00:09:54,240
it's more general than that, there's so many happiness that come from letting go, from

78
00:09:54,240 --> 00:10:09,240
being free, from seeing clearly, from a pure mind, the third is, say Upanishaya, which means,

79
00:10:09,240 --> 00:10:20,720
you plant a seed, Upanishaya is, it means different things entirely, they talk about it

80
00:10:20,720 --> 00:10:31,960
as being what you bring into your next life, what you carry on with you, so I describe

81
00:10:31,960 --> 00:10:39,200
this as a start, the third benefit is you get a start on the path, you're no longer

82
00:10:39,200 --> 00:10:46,720
a beginner meditator, you're no longer a neophyte, you're no longer a, you're no longer

83
00:10:46,720 --> 00:10:54,640
a laddie, it's a mongol, isn't this word mongol, mongol, I haven't noticed this,

84
00:10:54,640 --> 00:11:01,640
Harry Potter is this big thing, isn't mongol, is that the word, mongol is the right word

85
00:11:01,640 --> 00:11:09,160
isn't it, here mongol's are, I'm told they're non-magic folk, it's a mongol

86
00:11:09,160 --> 00:11:16,360
is, and then there's the whatever the opposite of mongol is, I don't know, you're no longer

87
00:11:16,360 --> 00:11:23,800
one of those, you're no longer in the out crowd, it's not about belonging to the crowd,

88
00:11:23,800 --> 00:11:33,400
it's about having something wonderful, you can go and practice on your own, you can continue

89
00:11:33,400 --> 00:11:43,800
on, find other meditation groups, you are now a veteran, a graduate, you've got to start,

90
00:11:43,800 --> 00:11:48,280
and you take that with you, the point of Upanisha, Upanisha, as you take it with you into your

91
00:11:48,280 --> 00:11:55,400
next dive, everything here you forget, people, places and so on, but mindfulness is different,

92
00:11:55,400 --> 00:12:04,920
changes, changes in your core, changes who you are, it's a very deep-seated change to

93
00:12:04,920 --> 00:12:16,440
very basic habits of our being, so it changes the whole trajectory of our journey and some

94
00:12:16,440 --> 00:12:24,920
of us are, and the fourth is a finish, now it's possible through the basic course that you become a

95
00:12:24,920 --> 00:12:32,760
Sotapana, this is always possible, possible that you, through many years of practice you become a

96
00:12:32,760 --> 00:12:39,400
Sotapana, Sakadagami, Anagami, I don't hunt it's all there, it's all possible, it's just a question

97
00:12:39,400 --> 00:12:48,360
of your effort and your merit and perfection, your goodness and your perfections, how much goodness

98
00:12:48,360 --> 00:12:59,960
you have, how much stored up, purity you already have, just waiting to be used, put to use

99
00:12:59,960 --> 00:13:10,200
and actualized in the meditation practice, it could become a nightmare to this practice, so

100
00:13:10,200 --> 00:13:17,160
for very good reasons to practice, but again the problem, you know, what if you sit there saying,

101
00:13:17,160 --> 00:13:22,120
am I a Sotapana yet, how do I become a Sotapana, when am I going to become a Sotapana,

102
00:13:22,120 --> 00:13:33,320
and I want so much to become a Sotapana, that's very, that's a bad idea, as our meditators know

103
00:13:33,320 --> 00:13:40,600
when they get, at the end of the course, we have to make these resolves and man that can be

104
00:13:40,600 --> 00:13:49,080
such a, the resolve, the resolution can be such a, such a hindrance to the practice because you're

105
00:13:49,080 --> 00:13:56,200
waiting for it to happen, expecting for something special to happen, it destroys your practice,

106
00:14:01,960 --> 00:14:11,080
and so it's more than just a, a quandary, it speaks to the very nature of samsara which is the

107
00:14:11,080 --> 00:14:20,600
nature of numbana, this is where this problem comes from, samsara is cause and effect,

108
00:14:22,280 --> 00:14:26,520
everything, every part of this universe that we know it, that we know,

109
00:14:29,800 --> 00:14:33,560
is conditionally formed, sankara, sankata,

110
00:14:33,560 --> 00:14:41,480
everything that is conditioned, everything that is cause and effect,

111
00:14:43,400 --> 00:14:55,400
is samsara, nibana is unconditioned, you can't bring about nibana, you can't cause it evoking,

112
00:14:55,400 --> 00:15:04,360
cause it to arise, it doesn't arise, there is no arising of nibana, it is permanent,

113
00:15:05,320 --> 00:15:11,880
the permanence may be the wrong word, but it is stable, it is stable and dissatisfied,

114
00:15:11,880 --> 00:15:23,400
it is eternal, undying, unchanging, if it's unchanging, how could you cause it to come, cause it

115
00:15:23,400 --> 00:15:37,800
to arise, so because nibana is not cause and effect, the way we approach it, then the way we approach,

116
00:15:37,800 --> 00:15:44,040
the way we gradually work to bring ourselves closer to this, to freedom,

117
00:15:45,320 --> 00:15:50,360
has to be quite different, it can't be cause and effect, it can't be working towards some goal,

118
00:15:50,360 --> 00:15:59,640
working towards goals that samsara, this is why this emphasis on the present moment, it's a very

119
00:15:59,640 --> 00:16:05,800
good reason why we have to focus on the present moment, because we can't be about cause and effect,

120
00:16:05,800 --> 00:16:12,520
we can't be striving for some goal, even though we often use those words, when you practice,

121
00:16:12,520 --> 00:16:23,160
it can't be about that, as to be putting down the burden, giving up, giving up our ambitions.

122
00:16:25,800 --> 00:16:36,120
So when we look to our practice, we have to be much more about the quality of the practice,

123
00:16:36,120 --> 00:16:46,920
the purity, the goodness, rather than the happiness, the peace, all of the benefits that we

124
00:16:46,920 --> 00:16:56,200
talk about, even listening to meditators describe the outcome of the practice, that's great

125
00:16:56,200 --> 00:17:03,080
encouragement, and reassurance to remove your doubts, yes, this is a good path, but it won't

126
00:17:03,080 --> 00:17:08,200
help you in the practice, it can't think, am I there yet? Okay, whatever I have to do to become

127
00:17:08,200 --> 00:17:17,880
like that person, it's not how it works, and you get so caught up, hung up, held up, held back,

128
00:17:19,560 --> 00:17:25,480
when you try to look for results, where am I getting anything out of this practice, it's a few

129
00:17:25,480 --> 00:17:31,880
doubts, it's not that you couldn't see it, it wouldn't be useful if you did, when you're doing that,

130
00:17:31,880 --> 00:17:39,640
you're no longer meditating, so be about quality, quality in the present moment, it's like put your

131
00:17:39,640 --> 00:17:45,400
nose to the grindstone, don't ever look up, do your work, your bank account fills up by itself,

132
00:17:47,560 --> 00:17:51,880
the dhamma checks are what do you call auto deposit, what's the word,

133
00:17:53,800 --> 00:18:00,440
you get paid automatically, no need to receive a check, your bank account, your

134
00:18:00,440 --> 00:18:09,080
dhamma bank fills up by itself, you just do the work, direct deposit, yeah that's it,

135
00:18:11,880 --> 00:18:19,000
all right, so a little bit of talk about benefits nonetheless, it's always good to talk about

136
00:18:19,000 --> 00:18:23,320
because it's reassurance, but a reminder and a warning that this is not how we should

137
00:18:23,320 --> 00:18:30,200
approach our practice, practice should always be about the practice itself, why we practice,

138
00:18:31,160 --> 00:18:37,480
why we practice is because not practicing is a cause of suffering, why we practice is because we

139
00:18:37,480 --> 00:18:46,520
have ignorance, why we practice is because there's a problem, why we practice is

140
00:18:46,520 --> 00:18:57,080
it's very much about the rightness and the propriety and the goodness of it, rather than we do

141
00:18:57,080 --> 00:19:06,120
it for x-ball, we can't we can't always be thinking about this, we have to give up the idea of

142
00:19:06,120 --> 00:19:15,960
motivation for being motivated, our motivation if we use the word should be for purity, for

143
00:19:15,960 --> 00:19:26,920
quality, for rightness, goodness here and now, and let the results come, it's like this whole

144
00:19:26,920 --> 00:19:35,320
God thing, right? People say you have to do x or else God will be angry at you, if you do x,

145
00:19:35,320 --> 00:19:41,400
God will be pleased, I just think, you know if God's going to be angry at me isn't that his

146
00:19:41,400 --> 00:19:47,720
problem, God's going to be happy for me, is that really, you know, I really, is this really a

147
00:19:47,720 --> 00:19:55,080
carrot in the stick sort of thing, what an awful way to live, just another reason to never believe

148
00:19:55,080 --> 00:20:05,160
in any God, there and with that, I think we've covered just about everything, that's the

149
00:20:05,160 --> 00:20:15,160
time of our tonight, thank you all for tuning in, have a good night.

