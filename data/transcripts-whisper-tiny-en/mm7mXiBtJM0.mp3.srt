1
00:01:00,000 --> 00:01:11,280
Good evening, everyone.

2
00:01:11,280 --> 00:01:21,120
Welcome to evening dhamma.

3
00:01:21,120 --> 00:01:33,080
Tonight we're looking at, again, the Sabasa was such that we're looking at

4
00:01:33,080 --> 00:01:55,080
the Sabasa or the defilements that are to be abandoned through avoiding.

5
00:01:55,080 --> 00:02:05,640
And so immediately this should sound some warning, so it should raise some flags in your

6
00:02:05,640 --> 00:02:15,760
mind of flag, because mindfulness, of course, is all about non-avoiding, it's about

7
00:02:15,760 --> 00:02:19,160
facing.

8
00:02:19,160 --> 00:02:20,960
That's really one of the great things about this.

9
00:02:20,960 --> 00:02:31,280
As it takes into account what we might call conditional realities and the realities of your

10
00:02:31,280 --> 00:02:34,360
situation or situational realities.

11
00:02:34,360 --> 00:02:43,520
So the Zuta puts mindful practice at the front in the idea of seeing things clearly

12
00:02:43,520 --> 00:02:54,840
and facing things, and so Adiwa and we saw with Adiwa and the idea of bearing or enduring

13
00:02:54,840 --> 00:03:00,800
things that normally we wouldn't think to endure, and that's very much a part of insight

14
00:03:00,800 --> 00:03:05,720
meditation and the Buddha's teaching.

15
00:03:05,720 --> 00:03:12,800
But then there's this acknowledgement that clearly there are things that we should avoid.

16
00:03:12,800 --> 00:03:20,280
Of course, most of the time our avoiding is a bad thing, that's not what this is about.

17
00:03:20,280 --> 00:03:26,880
This is an acknowledgement that sometimes avoiding is a good thing.

18
00:03:26,880 --> 00:03:34,480
And there are two sorts of categories of things that we should avoid, things that absolutely

19
00:03:34,480 --> 00:03:38,080
must be avoided.

20
00:03:38,080 --> 00:03:41,440
I guess by anyone you might say.

21
00:03:41,440 --> 00:03:49,080
I guess you could say things that should be avoided because we're human, and things that

22
00:03:49,080 --> 00:03:54,480
should be avoided based on our situation is the second category.

23
00:03:54,480 --> 00:03:58,200
And this varies, and it's a little more interesting.

24
00:03:58,200 --> 00:04:00,560
The first one's not so interesting.

25
00:04:00,560 --> 00:04:08,360
I mean, it's important to lay this down because you otherwise might find people doing all

26
00:04:08,360 --> 00:04:14,520
sorts of things that are a danger to their, not only their practice, but perhaps even

27
00:04:14,520 --> 00:04:18,640
their lives.

28
00:04:18,640 --> 00:04:29,400
So the Buddha talks about a wild elephant.

29
00:04:29,400 --> 00:04:32,640
A wild elephant is something you should avoid.

30
00:04:32,640 --> 00:04:41,280
A wild animals, wild pigs, apparently wild pigs are very vicious.

31
00:04:41,280 --> 00:04:43,440
You should give them a wide birth.

32
00:04:43,440 --> 00:04:51,560
And one man tells me once about walking in Sri Lanka, and he saw a wild pig and off to

33
00:04:51,560 --> 00:04:59,880
the side of the trail, and just suddenly for no reason the pig just charged him and stabbed

34
00:04:59,880 --> 00:05:07,640
him in the leg with his tusk for her tusk.

35
00:05:07,640 --> 00:05:13,960
Wild bears in Canada, wild moose should probably avoid wild moose.

36
00:05:13,960 --> 00:05:27,200
There should be no surprise here, but both of these are all of this section really speaks,

37
00:05:27,200 --> 00:05:39,080
I think, to this idea of situation, how it's all fine and good or it's true that it's

38
00:05:39,080 --> 00:05:48,720
the core of our practice to bear with and to see things clearly and to focus on ultimate

39
00:05:48,720 --> 00:05:59,960
reality, but in order to do that, and in order to sustain that, and in order to allow

40
00:05:59,960 --> 00:06:09,080
the practice and in order for the practice to be fruitful, there are situational realities

41
00:06:09,080 --> 00:06:12,040
that we have to accept.

42
00:06:12,040 --> 00:06:18,560
Our situation in our family as a father or a mother or a sister or a brother or a child

43
00:06:18,560 --> 00:06:34,760
as a situation in society as a taxpayer or a situation in our workplace, in our situation

44
00:06:34,760 --> 00:06:46,640
as needing money and so on, and so there are restrictions, there are conditions placed,

45
00:06:46,640 --> 00:06:55,480
there are handicaps, I don't know if that's the right word, that are placed on our ability

46
00:06:55,480 --> 00:07:02,720
to practice, that are realities, the realities of our situation, and I get a lot of questions

47
00:07:02,720 --> 00:07:06,440
of that relate to this sort of thing, you know, what should I do in this situation,

48
00:07:06,440 --> 00:07:16,960
in that situation, it's very difficult to answer those questions and it's a very complicated

49
00:07:16,960 --> 00:07:27,920
world that we live in, but the point is that there generally is no answer, there's no

50
00:07:27,920 --> 00:07:36,360
cut and dried answer, this is what you should do, there's a sense of the guidelines by

51
00:07:36,360 --> 00:07:46,560
which you should live, and often it means not finding an answer that allows you to solve

52
00:07:46,560 --> 00:07:54,080
a problem, but it's much more about accepting the problem as part of your situation, right,

53
00:07:54,080 --> 00:08:00,800
like why is it that you aren't able to go off into the forest and practice meditation,

54
00:08:00,800 --> 00:08:09,280
well there are lots of reasons for that, and all of those reasons should be taken into

55
00:08:09,280 --> 00:08:15,840
account because they're part of your practice, some of them are just going to be excuses,

56
00:08:15,840 --> 00:08:21,040
but even the excuses, you have to understand why am I making excuses, why, where is this

57
00:08:21,040 --> 00:08:28,160
coming from, and it's not enough to say boy, I wish I was just staying in my room and

58
00:08:28,160 --> 00:08:36,160
meditating all the time, you have to figure out how to get there from where you are,

59
00:08:36,160 --> 00:08:43,800
and so avoiding is a necessary tool in many ways, I mean most obviously if it's a threat

60
00:08:43,800 --> 00:08:50,120
to your life, not because enlightened people have to worry about dying, but because

61
00:08:50,120 --> 00:08:55,760
unenlightened people have to worry about dying, it's our situation, acknowledging that

62
00:08:55,760 --> 00:09:04,680
we're unenlightened means not dying would be probably a good thing, not getting sick, probably

63
00:09:04,680 --> 00:09:11,640
be a good thing for many reasons, even if it just means if I get sick then I'll lose my

64
00:09:11,640 --> 00:09:21,040
job or so on, and I won't have any money, and well that will be a potential problem for

65
00:09:21,040 --> 00:09:29,360
me to continue practicing potentially, I mean maybe not, or it will, I'm not ready for

66
00:09:29,360 --> 00:09:43,360
that, right, a lot of it surrounds work because work is lively and employment, and well

67
00:09:43,360 --> 00:09:50,640
it's necessary to be employed, to the extent that you can live, and again there's a lot

68
00:09:50,640 --> 00:09:59,200
of talk about simplicity and what you need versus what you want, but to some extent

69
00:09:59,200 --> 00:10:06,320
there are situational realities is something we have to consider, and so we avoid many

70
00:10:06,320 --> 00:10:13,160
things for many reasons, and many of, oftentimes that's a good reason, they are good

71
00:10:13,160 --> 00:10:19,160
reasons, obviously for a monk there's a lot less that we have to avoid, it's a lot less

72
00:10:19,160 --> 00:10:25,680
complicated, it doesn't mean we don't have to avoid things, so there are these things

73
00:10:25,680 --> 00:10:31,800
that, well certain things that everyone has to avoid, but then the situational things

74
00:10:31,800 --> 00:10:42,000
that we have to avoid, like monks for example have to avoid sitting alone with a woman,

75
00:10:42,000 --> 00:10:54,240
monks should avoid going into bars, monks should avoid hanging out with, hanging out with

76
00:10:54,240 --> 00:11:00,240
single women, hanging out with bikunis, I'm talking, I'm sorry, bikunis should avoid

77
00:11:00,240 --> 00:11:09,960
hanging out with bikunis and of course the opposite is true, and that's obviously

78
00:11:09,960 --> 00:11:15,840
an incredibly situational, there's nothing profound about that, it's just that they get

79
00:11:15,840 --> 00:11:22,680
a bad reputation, and that's an interesting point, the Buddha acknowledges this concept

80
00:11:22,680 --> 00:11:30,600
of getting a bad reputation, it creates misunderstanding, and that is a valid reason

81
00:11:30,600 --> 00:11:44,720
to avoid things, you want to present a, a clear and unsuspicious friend, I mean there's

82
00:11:44,720 --> 00:11:52,600
a monk, but everyone does, it's a good example for people who aren't monks,

83
00:11:52,600 --> 00:12:01,480
so the idea of there's some extent being a stand-up citizen and living your life in a way

84
00:12:01,480 --> 00:12:12,520
to avoid suspicion and complication and all sorts of different kinds of friction, I think

85
00:12:12,520 --> 00:12:21,160
there's a third important point to make, and let's run out maybe not addressed so explicitly

86
00:12:21,160 --> 00:12:27,320
here is that if monk, for example, if monks do hang out with single women, well it's, it

87
00:12:27,320 --> 00:12:33,480
does increase the chances of them doing things that monks really aren't supposed to do,

88
00:12:33,480 --> 00:12:42,280
and that's a good point because as, as non-monks, there are many things we probably should

89
00:12:42,280 --> 00:12:55,160
avoid because of the potential cause for, for problems, right, really any Buddhists shouldn't

90
00:12:55,160 --> 00:13:02,480
go into a bar or should do their best, or let's say an alcoholic should avoid going into

91
00:13:02,480 --> 00:13:11,720
a bar, so Buddhists by extension should avoid things that they know are going to cause

92
00:13:11,720 --> 00:13:27,480
them to, to potentially slip, right, like, I don't know, getting anything that you get caught

93
00:13:27,480 --> 00:13:35,120
up in, can, like, for example, your phone may be, when you do meditation you want to put

94
00:13:35,120 --> 00:13:41,760
your phone on airplane mode, otherwise, chances are you're going to be distracted by the phone

95
00:13:41,760 --> 00:13:53,800
and, and the phone rings, so again avoiding is, is very much to do with, with how it

96
00:13:53,800 --> 00:13:58,840
relates to our practice and how it supports our practice, there are things we should

97
00:13:58,840 --> 00:14:07,360
avoid, and we talked about pain, but injury is probably another good example, in there

98
00:14:07,360 --> 00:14:15,080
are cases of, there are examples of people, there are people who, you know, know there

99
00:14:15,080 --> 00:14:22,680
are examples of sitting too long in an uncomfortable position and it actually has a detrimental

100
00:14:22,680 --> 00:14:29,800
effect on your body, so you have to have a sense of that as well, I mean it's usually

101
00:14:29,800 --> 00:14:40,000
for, for people who have injuries or some bodily condition, that means it's really better

102
00:14:40,000 --> 00:14:46,800
that they sit in a chair or that they sit in a specific way, because if they don't, well,

103
00:14:46,800 --> 00:14:52,640
it's going to, in the long run, be a detriment to their practice, maybe it'll be hard

104
00:14:52,640 --> 00:14:59,640
for them to walk or, however, them to sit or be harmful to their health in different

105
00:14:59,640 --> 00:15:00,880
ways.

106
00:15:00,880 --> 00:15:07,080
So it's another facet, I mean it has to be said, there's not a lot, it's not a very profound

107
00:15:07,080 --> 00:15:14,000
section, I suppose, but it's, it's necessary and it, it really gives you a sense that, yes,

108
00:15:14,000 --> 00:15:23,200
this is, this is the framework, this, it really puts out the framework and it, it points

109
00:15:23,200 --> 00:15:30,080
out the various ways in which we purify our minds, in which we do away with those things

110
00:15:30,080 --> 00:15:33,960
that cause us stress and suffering.

111
00:15:33,960 --> 00:15:38,160
One of them is by avoiding, because if we didn't avoid certain things, these certain

112
00:15:38,160 --> 00:15:48,160
things that we talk about rather would lead to stress and vexation and the fever of

113
00:15:48,160 --> 00:16:15,480
defilements, so that's the number for tonight, I will take questions.

114
00:16:15,480 --> 00:16:25,160
What to do when an earworm arises during practice, an earworm, I mean a sound, a thought

115
00:16:25,160 --> 00:16:32,440
or memory of a song will overcome, so a thought or memory of a song.

116
00:16:32,440 --> 00:16:40,280
Well you will know that you know that it's hearing, hearing, but it's so strong that

117
00:16:40,280 --> 00:16:45,520
I end up frustrated and tired, well then you should not frustrated, frustrated and tired,

118
00:16:45,520 --> 00:16:51,400
I mean this is giving you insight into your mind, the point is that it's not under your

119
00:16:51,400 --> 00:16:55,280
control and your, the frustration comes when it's not doing what you want, you want it

120
00:16:55,280 --> 00:17:00,800
to go away, you want it to not come back, you want to focus on something else, all of

121
00:17:00,800 --> 00:17:07,440
that and those expectations are the cause of stress and suffering and frustration.

122
00:17:07,440 --> 00:17:12,760
So if you're patient, you'll change that about yourself and you'll get less frustrated

123
00:17:12,760 --> 00:17:19,960
about things.

124
00:17:19,960 --> 00:17:29,680
Can you stay with the rising and falling in daily life as well as in seated meditation?

125
00:17:29,680 --> 00:17:36,760
You can if you're sitting, I mean it's not the kind of thing I'd encourage you to do

126
00:17:36,760 --> 00:17:41,680
and there's other thing that's happening, like you're eating for example, if you're

127
00:17:41,680 --> 00:17:48,360
eating you should focus on chewing and swallowing, but if you're sitting on the bus or

128
00:17:48,360 --> 00:18:03,840
something, I'm just sitting at home, and any exceptions to the sixth precept of no eating

129
00:18:03,840 --> 00:18:11,200
afternoon, I've heard some exceptions such as yogurt cheese and chocolate, all the texts

130
00:18:11,200 --> 00:18:18,360
are pretty clear, the only thing that's allowed are fruit juice, vegetable juice, root

131
00:18:18,360 --> 00:18:31,560
juice, leaf juice, that kind of thing.

132
00:18:31,560 --> 00:18:37,360
And there are other exceptions for medicine, but they're all for medicine and a lot of

133
00:18:37,360 --> 00:18:44,240
people have used those medicines, those medicinal allowances to allow lots of different

134
00:18:44,240 --> 00:18:51,600
things, like oil is allowed as a medicine, if your sick gets for sick monks, it says

135
00:18:51,600 --> 00:19:03,600
quite clearly that for sick monks, kirana nang bikunang means for sick monks, so if you're

136
00:19:03,600 --> 00:19:11,360
hungry, it doesn't really qualify as being sick.

137
00:19:11,360 --> 00:19:17,080
I'm having trouble understanding the selfless nature of mind consciousness, I realize

138
00:19:17,080 --> 00:19:23,240
it explained, it explained us without a noir, just knowing, but having trouble understanding

139
00:19:23,240 --> 00:19:30,000
how knowing can take place without a noir, the mind consciousness for me seems to be

140
00:19:30,000 --> 00:19:33,960
a last refuge for the illusion of self.

141
00:19:33,960 --> 00:19:37,800
But something you can think your way through, I mean non-self is something you have to

142
00:19:37,800 --> 00:19:44,320
see through the practice, so all this intellectual stuff that's going on in your mind

143
00:19:44,320 --> 00:19:52,680
of trying to understand is you're going about it all wrong, there's no point in trying

144
00:19:52,680 --> 00:19:57,280
to understand any of this, you should just try to practice.

145
00:19:57,280 --> 00:20:02,000
If you're mindful, the understanding will come by itself, but I wouldn't have to tell

146
00:20:02,000 --> 00:20:07,120
you anything.

147
00:20:07,120 --> 00:20:12,280
I found most of the angry thoughts and last thoughts based on past memories come to mind

148
00:20:12,280 --> 00:20:18,360
when having a shower bath.

149
00:20:18,360 --> 00:20:25,040
These thoughts make my mind try to create different imaginations, is there any relationship

150
00:20:25,040 --> 00:20:29,840
between relaxation being alone and defamence right, I see?

151
00:20:29,840 --> 00:20:40,360
Yes, potentially, because your mind is not, when you're not challenged, your mind is

152
00:20:40,360 --> 00:20:47,440
you feel comfortable, I mean it's really just habits, it's just the way we've developed

153
00:20:47,440 --> 00:20:53,520
as humans, when the going gets tough, the tough get going, there's a reason for that

154
00:20:53,520 --> 00:21:06,240
because you get into the habit hopefully of under adversity, being moving into or shifting

155
00:21:06,240 --> 00:21:16,400
into a habit of responding with a positive traits of an effort and attention and so on,

156
00:21:16,400 --> 00:21:25,200
but we get into the habit of okay now I can relax when things are good and so there's

157
00:21:25,200 --> 00:21:33,840
no cosmic or fundamental reason for it, it's just an interesting, it's not really interesting,

158
00:21:33,840 --> 00:21:39,120
it's an unfortunate habit that we generally tend to get into, when the going is not

159
00:21:39,120 --> 00:21:49,200
so tough, we relax and we let it slide and the bad habits come up, it's just the way

160
00:21:49,200 --> 00:22:03,160
we develop, it just shows your habitual nature, shows who you are, that's a person, how

161
00:22:03,160 --> 00:22:08,160
is Dhamma pride and applied and daily practice, it seems like it contains many Buddhist

162
00:22:08,160 --> 00:22:18,200
concepts, while the entrance is liking, disliking, drowsiness, distraction, seeing the senses

163
00:22:18,200 --> 00:22:30,080
seeing, hearing, smelling, tasting, feeling, thinking, and several videos you state that

164
00:22:30,080 --> 00:22:38,080
liking and wanting are the same, understand that what one wants, one likes and what one likes

165
00:22:38,080 --> 00:22:43,240
one wants, but the statement is confusing because there seems a clear difference in experiencing

166
00:22:43,240 --> 00:22:49,360
craving, judgment of a feeling it's pleasant, when it starts to satisfy, by drinking

167
00:22:49,360 --> 00:23:13,520
and agreeable, just right, well it's technically the same thing, it's technically a state

168
00:23:13,520 --> 00:23:33,640
of wanting, the liking of something is, technically it's the same, that's all I can say,

169
00:23:33,640 --> 00:23:39,080
but clearly yes, liking, it feels very much like liking and wanting feels very much like

170
00:23:39,080 --> 00:23:45,920
wanting, it's an interesting question, I wouldn't worry too much about it, if you like,

171
00:23:45,920 --> 00:23:53,800
say liking, if you want, say wanting, is one required to have their parents' permission

172
00:23:53,800 --> 00:24:02,760
in order to ordain this among, yes, one is required, what is said or written about

173
00:24:02,760 --> 00:24:13,040
mudra changing the frequency, it's not what I teach, sorry, dear beaucoup, I have been

174
00:24:13,040 --> 00:24:21,400
in appreciation, I do we pass an asamata, I'm a psychiatric, I'm on psychiatric medication

175
00:24:21,400 --> 00:24:25,880
and find them all useful, the question that occurred to me is how can I do these more

176
00:24:25,880 --> 00:24:40,240
frequently, thank you for guards and many abrasive blessings, well I mean keep it up,

177
00:24:40,240 --> 00:24:47,800
keep doing it, it's all I can say really, I mean medication is unfortunate and problematic

178
00:24:47,800 --> 00:24:57,320
because it covers up the sorts of things that we're trying to look at, this is a problem

179
00:24:57,320 --> 00:25:03,480
that everyone has, that they have to wrestle with in meditation, is that, meditation isn't

180
00:25:03,480 --> 00:25:12,160
about avoiding your problems, or it isn't about fixing your problems, it's about understanding

181
00:25:12,160 --> 00:25:23,000
your problems and the only way you can understand them is if they come up, I think we put

182
00:25:23,000 --> 00:25:30,400
too much reliance on medication, thinking that all it does is flick the switch back off

183
00:25:30,400 --> 00:25:37,480
that was wrongly turned on, it's not really the case, the situation in our minds is who

184
00:25:37,480 --> 00:25:46,480
we are, and it's unfortunate that for some people that is unbearable, but you know, meditation

185
00:25:46,480 --> 00:25:55,200
is about learning to bear it, so I think it's unfortunate that many people end up taking

186
00:25:55,200 --> 00:25:59,680
medication that probably didn't need to, and I know that there are people who of course

187
00:25:59,680 --> 00:26:06,560
need to, and I think it's unfortunate that it's much harder to get off it once you've

188
00:26:06,560 --> 00:26:16,360
already started taking it for some time, but I think it's most unfortunate that those people

189
00:26:16,360 --> 00:26:30,080
didn't find meditation and weren't able to avoid having to take psychoactive medication,

190
00:26:30,080 --> 00:26:34,520
but I can't budge on that, I mean I'm not going to say it's okay, you can become enlightened

191
00:26:34,520 --> 00:26:41,320
while you're on psychoactive medication, maybe technically it's possible, in my experience

192
00:26:41,320 --> 00:26:55,440
it seems a real serious obstacle, not my personal experience, but my experience as a teacher

193
00:26:55,440 --> 00:27:05,640
teaching people who've taken it, who are taking medication, because their practice ends

194
00:27:05,640 --> 00:27:17,200
up being flat, they don't, they aren't challenged in the way that other people are challenged.

195
00:27:17,200 --> 00:27:23,800
When they are meditating, any thoughts rise is that I am unable to mindful, again not

196
00:27:23,800 --> 00:27:31,520
a question, what is the problem, I have no control over it or hold on it, while that's

197
00:27:31,520 --> 00:27:41,400
seeing non-self, that's what you're trying to see, so good for you, the more you see

198
00:27:41,400 --> 00:27:47,160
it, the more you'll let go of your thoughts and stop trying to fix them, stop trying to

199
00:27:47,160 --> 00:27:49,240
control them.

200
00:27:49,240 --> 00:27:56,880
When will you be visiting the United States, I don't know, I'm not really inclined to

201
00:27:56,880 --> 00:28:06,120
visit the United States these days, I'll be going in December, I'll be going to Tampa,

202
00:28:06,120 --> 00:28:14,880
Florida, December 22nd, I think I'm going to already have a ticket, I'll be back January

203
00:28:14,880 --> 00:28:21,280
3rd, that's where my mother lives, I'll be going there and I'll do some teaching hopefully

204
00:28:21,280 --> 00:28:29,520
and we'll give a donation to the children's home, hopefully, how to deal with uncomfortable

205
00:28:29,520 --> 00:28:35,000
feeling, well, feelings aren't uncomfortable, there's a feeling and then you don't like

206
00:28:35,000 --> 00:28:40,240
it and that's a habitual reaction, if you read, I encourage you to read my booklet on

207
00:28:40,240 --> 00:28:46,080
how to meditate, it should give you some advice on how to deal with feelings and your reactions

208
00:28:46,080 --> 00:28:59,480
to them, you're welcome, and that's all the questions for tonight, hope I wasn't too

209
00:28:59,480 --> 00:29:14,920
hard to ask her, thank you all for tuning in, have a good night.

210
00:29:14,920 --> 00:29:31,600
.

211
00:29:31,600 --> 00:29:40,600
.

