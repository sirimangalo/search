So, that depends upon how the contemplation before the thought was done.
So, if the contemplation is done on a scientist aspect then, the thought also gets the name signless and so on.
However, I have to believe some leader, inside knowledge is expressed as follows.
It is expressed first as the white liberation by its liberation and so on.
And, it is a little confusing here because we understand that liberation is a name of path.
But, the position leader of America is telling us that the contemplation is called sinless and so on.
But, actually, it is not the contemplation, but I mean,
it is not the contemplation, but the path is called entirely we mock up liberation.
So, liberation means path and not, and not contemplation.
But, in the position, some leader of America, it is said that the contemplation is called liberation,
contemplation is called liberation and so on.
So, there is some grounds for confusion here.
So, how the path gets its name depends upon here on the contemplation,
which he told the path is attained.
So, on the next page, it is how a little reward and desire was,
and it is at the moment of noble thought that the liberation is distinguished
and that is done according to inside knowledge as way of arrival at the path.
That means, it is done by way of insight as souls.
So, we take insight or contemplation as a source for the name, which the path gets.
If the contemplation is sinless, then the path is also called sinless.
If the contemplation is desireless, then the path is also called desireless.
If the contemplation is quite less than that, it is also called five of whiteness, right of whiteness.
So, how the path gets these names depends on how the contemplation was done.
And then, it said that in paragraph 66, it becomes a condition for the classification of
noble persons in seven times.
So, we know that there are eight noble persons, right?
It's a real person, it's on noble persons.
For the first stage two, for the second stage two, third stage two and fourth stage two,
is sort of the person who is at the moment of first path.
And then the person after the first path, before the second path.
Then it was an at the second path and then another person after the second path before the third path and so on.
So, there are eight noble persons.
But here, they are different, just right in a different way.
And so, according to this, there are seven noble, seven kinds of noble persons.
And there are, these are what?
The fifth, W.D. one, two, one liberated by three.
The body witness, the full, the both ways liberated.
The fifth, the demo, there were the six, one, continued vision and one, seven, one liberated by understanding.
They are also, say, different kinds of, different kinds of noble persons mentioned in the,
so that then also in a dilemma.
And then the situation follows in the paragraph, in the center, five and so on.
In paragraph 75, you don't, I use it in number four.
He is called both ways liberated.
Well, he has reached the highest solution, that means I'll answer.
The highest solution after also reaching the immaterial journals.
I don't find the word for also in the body, in the body, in the body, in the body, in the body, in the body.
So, we should just hit it down.
But he is called both ways liberated when he has reached the highest function after reaching the immaterial journals.
That means he may practice some time meditation first, and then get the full immaterial journals.
And then he may contemplate on the full material journals or some other formations, and then he gets enlightened.
So, he, he is called liberated in both ways, when a man brings them to mind as not-self,
and having great wisdom and quality, if I don't understand it, he becomes a demodigodian, so on.
So, there are these seven kinds of arena, persons or noble persons.
And then, paragraph 76, pointing things for missions to mind as impermanent, the fifth or the is in access in him and so on.
I've already talked about that.
And then, paragraph 77 and 78 are the, what description of beauty or what explanation of the terms, like in Bali, Sata, and Lusari, Sata, would more than so on.
Now, let's go to paragraph 79.
This knowledge of accountability about formations is the same in leaning as the two kinds that receive it.
Hence the ancient set, this knowledge of accountability about formations is one, only and has three names.
And the outside, it is, it has a name of knowledge of desire for deliverance.
In the middle, it has a name knowledge of reflection.
At the end, when it has reached its combination, it is called knowledge of accountability about formations.
So, these three are actually one kind of knowledge.
But in the books, they are described as three kinds of knowledge.
So, the knowledge of accountability about formations is one, only and has three names.
So, they can be called knowledge of accountability about formations and all they can be called by their respective names.
And then, at the beginning of paragraph 80, something is missing.
And that is, it says in the text also.
So, the transfusion of that is missing. I mean, in the original, it is a parry of people done.
So, that is missing here.
So, it is said in the text also, because they will be following as a formation from the texts.
How is it that understanding of desire for deliverance of reflection and of composure as knowledge of the kinds of accountability about formations and so on?
So, it was taken from the British and Miramarra.
And then, explanation of it in paragraph 81.
Now, let us call the paragraph 82.
Furthermore, it may be understood that this is true from the following text.
For this is said, desire for deliverance and contemplation of reflection and accountability about formations.
These things are one in meaning and only the letter is different.
So, this is another quotation that shows that these three are actually one.
And next insight, leading to the emergence of the fact.
So, we have read about that, read that.
And then, in paragraph 84, there is a description of how a yogi contemplates and how he emerges.
That means how he becomes enlightened in many different ways.
So, the one is after interpreting the internal, it emerges from the internal.
After interpreting the internal, it emerges from the external.
3.
After interpreting the external, it emerges from the external.
4.
After interpreting the external, it emerges from the internal.
5.
After interpreting the material, it emerges from the material.
not self. And so on and so on. Now, here interpreting means just the same thing as I said,
contemplating, not really interpreting. So, contemplating the internal,
it emerges from the internal external and so on. So, we will read the first explanation in
paragraph 85. Now, someone just is interpreting at the start with his own internal formations.
That means he contemplates on his internal formations, his material properties in his body,
mental states and his mind and so on. So, he does his interpreting at the start with his own
internal formations. After interpreting them, he sees them. But in many times of the past,
it does not come about through seeing the better internal only. Since the external must be seen too.
So, he sees that another's aggregates as well as uncrun to formation that means in any
many things are information and painful not self. So, first he contemplates on his body and on his mind.
And then he contemplates on other persons aggregates externally. So, both internally and externally.
And at one time he completes his internal and at another time he takes the internal.
Because he cannot take internal and external at the same time. So, at one time he will be
contemplating on the formations of his own and at other times on other people's formations.
As he does so, inside joins with the path while he is comprehending the internal.
It is set of him that after interpreting the internal, it emerges from the internal. So,
in the beginning he contemplates on his own all the formations. And then he contemplates on
another person's formations. So, he does this during his meditation, sometimes contemplating on
his own and sometimes contemplating on others. And then the emergence or the path will arise
after he contemplates or after he contemplates on another's formations.
Because it can come at any moment. So, as he does so, inside joins with the path that means
path occurs while he is comprehending the internal. That means immediately after he is comprehending
the internal. It is set of him that after interpreting the internal, it emerges from the internal.
So, this is just different ways of understanding how the emergence of how the path arises.
Sometimes it may arise after the yogi has contemplated his own formations and sometimes
after contemplating on others from formations and so on.
And then, for a graph, it is seven. I think he others are not able to understand.
When he has done this interpreting in this way, all that is subject to arising is something
desaization. And so, to at the time of emergence, it is set that it emerges that one stroke
from the five aggregates. Yeah, he does not take aggregates one by
actually, he does not take the all five aggregates at one time. But, we personally knowledge
or we personally moments of consciousness, it is very quickly that they are
they are seem to be taken at one moment. But, actually one aggregate is taken by each consciousness.
He takes one and then he takes another and he takes another. He cannot take the all five
of together. But, since we was not so fast at the time that it is set that he
commemorates on them at one stroke. Actually, it cannot be that way.
And then, the other explanations are not difficult to understand. Now,
the other wants us to understand this knowledge very clearly. And so, he was 12 families.
12 families should be understood in order to explain this inside leading the emergence.
And the kinds of knowledge that proceeds and follow it. Here is the list, the bet,
the black snake, the house, the oxen, and the go. How do you say that? Go, go, go.
It is like like a ghost or the child hunger and thirst and cold and heat and darkness and by poison
too. Now, the number two should be not black snake, but cobra.
The literal translation is black snake, but it is not used in that sense, actually.
The bale what is kana saba. So, kana means black and saba means snake.
But, when these two words are joined to mean one animal, then it does not mean
the snake, which is black and color, but it means a cobra. Maybe cobras are generally black.
But, it does not matter what we did, it does cobra, which is a black snake.
And then the symbol is explained one by one. So, let's read the first symbol in the best.
There was a bet, it seems. She had alighted on in Madooka tree with five branches,
thinking, I shall find flowers or fruits here. She investigated one branch, but so
no flowers or fruits there were taking. And as with the first soul to, she tried the side of the
third the fold and the fifth part saw nothing. She thought, this tree is barren, there is nothing
worth taking here. So, she lost interest in the tree. She climbed up on a straight branch and poking
her head through a gap in the foliage. She looked upwards, flew up into the air and alighted
on another tree. Here in the Madooka tree, we regarded as like a bet. The five aggregates of
objects of cleaning are like the Madooka tree with the five branches. The Madooka tree there is
interpreting of five aggregates. It is like the best alighting on the tree. This comprehending
the materiality aggregate and seeing nothing there worth taking. Compreting the remaining
aggregates is like trying each branch and seeing nothing there worth taking, trying the rest.
It is a simple knowledge beginning with desire for deliverance after he has become
dispassionate towards the five aggregates through seeing the characteristic of
impermanence, etc. It is like her thinking. This tree is barren, there is nothing worth taking
here and losing interest. He is conformity knowledge which comes after the knowledge of
equanimity. His conformity knowledge is like her climbing up the straight branch. His change of
lineage knowledge is like her foaming ahead out and looking upwards. His path knowledge is like
her flying up into the air. His fruition knowledge is like her alighting in different trees.
And then there is another semi of the black snake. This memory has already been given
in paragraph 49. But the application of this memory here is this. Change of lineage knowledge
is like throwing the snake away. Path knowledge is like the man standing and looking back
once he had come up and getting free from him. The fruition knowledge is like his standing in the
place free from fear. He had gone away. This is the difference. So we have read that seemingly.
And I think this is to be edited that simply because there are the moments of change of lineage
and others are not explained. So we can combine these two to get a full, full semi.
And then the other semi are explained, the halves, the oxen and so on.
The last group, paragraph 102, hunger, thirst, cold, deep darkness, and by poison.
These six similes are given for the purpose of showing that one with insight that leads to
emergence, turns, inclines, and leans in the direction of the supramandang state.
So it is just to show that that person leans towards neibana or supramandang states.
That is why it was said about now.
Next page, paragraph 109. That is why it was said about what he knows and sees does
his heart retreats, retracts, and points from the three kinds of becoming, the four kinds of
generations, the five kinds of techniques, the seven states, and some consciousness, and the nine
apples of beings, his heart no longer goes out to them. So at this stage,
as you see around saying, even if you try to, if you try to send your thought to other objects,
it will not go just what will be here or on the object and not to the other objects.
Just as what the drug retreats and so on. But at this point, he is called one who walks a
look with reverence to whom it is said. And then what governs the difference in the noble
cuts and light them in fact as etc. And this is also for us to understand how the enlightenment
or how the path is governed by or determined, how the quality of the path is determined by
here, by Vipasana, and also by some others. This knowledge of accountability about formations
governs the fact that the very little keeps upon it for the more governs the difference in the
number of the noble cuts and light them in fact does. But fact does in general, like
as the mode of progress and the kind of liberation, it will explain later one by one. So while some
elders say that it is a journal used as the basis for insights leading you in margins, that governs
the difference in the number of enlightenment factors, put factors in general factors, and some say
that it is the every age, maybe object of insight that governs it, and some say that it is the
personal bank or one's own wish that governs it. Yet it is, now let us live out only. Yet it is
this preliminary insight and insight leading you in margins also, that should be understood to
govern it in the adoption also. So we put two also there. Now there are three opinions about it,
how the path gets its name. So the first group of teachers say what?
Now it would be explaining in the following paragraph, but let me tell you this, there are
persons who do not have any, any journals, who do not practice any summer summer
edition at all, but who practice people's imagination. And there are those who have journal.
But they don't use this journal as the object of the person on meditation.
They have changed journals, but when they practice people's accommodation, they do not
contemplate on the journals, but they contemplate on other, other mental and physical phenomena.
And there are others who have journals, but who do not use journal at all when they practice
meditation. So for them, the path that arises is like the first channel. That means it has all
the components of first channel. I'll tell you okay, you don't seem to understand it.
Let me say the higher states of consciousness. And journals are attained through the practice of
summertime meditation, not through the summertime meditation. Then you practice the discussion of
meditation, or you practice reading meditation, and then you get, say, journals. So the journals
are attained through the practice of summertime meditation. Now, if it wasn't who practices
with person on meditation, may have not attained any of the journals at all. He is a pure
with person on meditation. So there are no journals for it. And there is another person who has
journal, who has attained journal, but he didn't make use of journal at all when he practiced meditation.
There was a wee person on meditation. So he is more or less like the other person who has no journal.
And there is another person who uses his journal when he practices wee person on meditation.
That means he enters into journal, but after emerging from the journal, after getting out of
the journal, he does not use the journal as the object of meditation, but he uses the other
formations as the object of meditation. So the part of such a person is said to be
or said to resemble the first journal. Now, there are five factors in first journal
with takka, with jara, b-d, suka, ikka-ga-da, right? Initial application, sustained application,
joy, happiness, one-pointedness of mind. In the second journal, full factors, right? In the third
journal, three factors. In the fourth journal, two factors. In the fifth journal, two factors.
Now, one, a person who has no journal at all or who does not use jara in his practice of
wee person on meditation attained path. His path is accompanied by these five factors.
You can find these, some of these in the eight factors of path. What is the first factor?
Right understanding, right, thought, right, speech, projection, right livelihood,
right effort, right mindfulness, right concentration. So these are all there with the first path.
And so with the first path, we find with takka, and with takka is the right thought. With takka,
with jara, b-d, suka, ikka-ga-da, all. So his path resembles the first journal.
Now, there is another person who practices the jara, and then he reaches second, third,
fourth, and fifth. So if you first practice jara as a basis for wee person, and then it gets
out of that jana, and takes the janas as the object of meditation or other functions as object
of meditation, and then he gets third. Then, since he made the second jana, or third jana, or
fourth jana, or fifth jana, as the basis for wee person, now, his path resembles second, third,
fourth, fifth jana. So if his path resembles second jana, then one factor will be missing from the
eighth, eighth, noble, eighth, fourth path. That is, with takka will be missing there. And right thought
is with takka. And if it is the fourth jana, what will be missing? P-d will be missing, and P-d
is one among the seven factors of enlightenment. So there are three opinions here. One teacher,
or one group of teachers, is that the jana, we just make the basis for wee person now,
is what determines the quality of tak. Second group says that it is the jana, which is
made the object of wee person now in meditation, that determines the path. And the third group
says that it is the wish of the person, which determines the quality of the path. So the first
emphasize, the first group emphasizes on the jana, which is make the basis for wee person. That means
first he enters into first jana. And then imagine from the first jana, and contemplates on jana
or other formations. Now second group emphasizes on the jana, which are made the object of wee
person now in meditation. That means when they say he enters into first jana. And after getting
out of first jana, he can now place on the first jana, he can now place on first jana as
in harmony and so on. And then he gets third. So the second group emphasizes on the jana,
which is made the object of wee person now in meditation. Now please be clear there,
the object of wee person now and the basis of wee person. The basis of wee person now means
before practicing wee person now, he enters into this jana. And then he emerges from this jana
takes other things as an object of wee person now in meditation.
Object of wee person now in meditation means taking the jana or contemplating on that jana
as in harmony and so on. So there is a basis jana, contemplated jana. So the first group emphasizes
on the basis jana and the second group emphasizes on the contemplated jana. But the third group
emphasizes on one's own wish. It is a free country. So that does not mean that
simply by making a wish you cannot get fat. But suppose you enter into first jana and then you
make the first jana, second jana as the object of meditation. So what will be the first line?
According to the first group of teachers, your fat will be like first jana, right?
According to the second group of teachers, your fat will be like second jana.
What about the third group? They say that according to your wish, if you want the first jana,
then your fat will be that way. But if you want your fat to resemble second jana,
then it will be like second jana. So you have a try there. So that choice or that wish comes
not without jana. First you have to have jana and you enter, you make the jana the basis of
hypersonal and also you make the jana the object of hypersonal and then you choose from this.
Sometimes you wish that may my fat resemble first jana or may my fat resemble second jana.
But it is said that if a person has no such wish, then the product will resemble the higher jana.
For example, you enter into the first jana and you get out of it and you make the second jana,
the object of meditation and then the fat bothers. But if you have no wish, whatever fat comes,
I don't mind, then your fat will resemble the second jana because it is a higher one.
So the higher jana, I get the precedence there.
So in the following paragraph, you will find 5th or 7th or 6th or whatever.
So 5th or 7th means without fat, without the right thought of the jana.
And 6th and right hand factors mean without pd because in the 4th jana, pd is missing, right?
So there are these 3 or pd kinds of opinions or theories.
And the visual info that is said that even according to these teachers,
we personally can be a deciding factor also. Not only the jana, but we personally can also be a deciding factor.
And then the progress, our pre-780 and that is some people can get enlightened in a short time
without much difficulty, but some have difficulty short time and so on.
So there are different kinds of theories. Now,
if insight has from the start, only been able to suppress development with difficulty
with algorithm, with prompting, then it is called of difficult progress.
The opposite kind is called of easy progress and so on.
And find the manifestation of the path, the goal of insight is slowly affected after
development has been suppressed, then it is called of sluggish direct knowledge.
Now, here the goal of insight, the goal is not to be not the correct translation of the world.
I think the word here means stagnation.
The stagnation of insight, that means the yogi has reached the knowledge of the equanimity of all
formations and he is stuck there. He has to be doing that again and again and again.
So if he has to spend a long time in that stage before reaching the path,
then he said to be of what sluggish direct knowledge.
So the insight here is called a stagnation, right?
Still something like that because you have to be with this for a long time.
So the manifestation of path which has this stagnation of insight,
that means only after spending a long time with that knowledge there arises path.
So that is called of sluggish direct knowledge. The opposite kind is called of swift direct knowledge.
If you don't have to spend much time in that stage of knowledge, the equanimity of all formations,
and if you get to the path in a short time, then you are called of swift direct knowledge.
So this algorithm about formation stands at the arrival point, that means as the source of
giving names, as the source of name for path and gives its own name to the path in each case,
and so the path has four names according to the kind of progress. Now you will find the
phrase at the arrival point, at the arrival point. That is the literal translation of the body
one, not even quite literate. So the meaning is the source, the source for giving name and the source
for the name, or the cause for the name. Now for one degree, this brokerage is different in the
full path. Quite for another, it is the same. For Buddhas, however, the full path of easy
progress and swift direct knowledge for Buddhas, everything is the best. Likewise, in the case of
the general of the Damod, the other Sariboda, so he is as good as the Buddha, so he is all the best.
But in the other Mahamoglana's case, the first part was of easy progress and swift direct knowledge,
because he became a sort of an adjust after listening to Sariboda, like,
uttering the verse. So at the time, it was of easy progress and swift direct knowledge.
But the other sort of difficult progress and sluggish direct knowledge, because he had to spend
practicing meditation, and at one time he was sleepy and told him how to get rid of
sleepiness and so on. So for the other stages, he said to be of difficult progress and
sluggish direct knowledge. And then predominant, as with the kind of progress, so also with the
kinds of predominance, which are different in the full path for one people, and the same for another.
So before predominance is a zeal, a chanda, energy, consciousness, and inquiry, inquiry really means
understanding. And then the duration, but it has already been told how it governs the difference
in preparation. Furthermore, the path gets its name for five reasons. And then
the path gets its name as signless, as desires, as wives, and others for five reasons.
The one is going to its own nature and to what is what it possesses. Now, what is opposes?
And it's own special quality. Four going to its object and five going to the way of arrival.
The way of arrival means the source of the source of name. Now, the one going to its own
reason means only to its functioning, how it functions, how what function it does. So going to its own nature.
The explanation follows. It is accordingly about formations induces emergence by comprehending
formations as impermanent, liberation takes place with the sinus, liberation, and so on.
These are the five reasons of how the path gets its name as signless and so on.
Now, for a graph 126, but while this name is inadmissible by the
a bit of a method, it is, however, admissible by the so-called a method. The name signless
is not mentioned in a bit of. So, according to a bit of a method, there can be no signless path.
But by so-called a method, by the discourse method, it is admissible.
For, you see, by that method, change of lineage takes the name signless by making a signless
nibbana as object. Now, change of lineage precedes path. So, at the moment of change of lineage,
it takes the nibbana as object. The nibbana is described as signless desire less and white.
So, if we take the signless aspect of nibbana, and the change of lineage takes the nibbana as
object, and then the path follows that the path is named through change of lineage being
signless. So, the path is also named signless. According to Susan, so that a method, the path can
catch the name signless. But according to a bit of a no. And its solution can be called
signless too, according to the path way of arrival. So, since path is called signless,
the solution which follows it could also be called signless. So, the change of lineage
is called for the path to get the name signless. And the path is the cause for the
solution to get the name signless. So, in the Sultan of the method, this can be called signless,
or they get the name signless. But according to a bit of a method, they cannot be called signless.
Thus, we cannot have written a pane is called desireless because it arrives at the path
by writing up the depth of formation and so on. Okay, I think we will stop here. So, next time,
confirm at the knowledge, we have two more weeks, right? Two more classes, I mean? Yeah.
No, no. It is according to what is said and what is taught in a bit of it.
I mean, okay, the big, but then we talk about the genres around the, yeah, down, genres
are called in the genome. But then something is saying that, you know, saying that being in a
message on a method. A bit, a bit of a, is not, not necessarily
a genre, but it can be genre, but I don't know genre. We persona can be practiced, and mixed with
with the samata, or it can be practiced with samata. So, there are two ways, say, we
pass them a yangika and samata yangika, say, one who has a vehicle of samata and one who has
a vehicle of we pass them. So, one who has a vehicle of samata, we mean that he practices
samata, meditation first and he gets jalah or gets samadhi. And then takes that jalah
also, maybe as the object of meditation, and then practices
we personally.
I can learn both of them now in the theme that I don't
can't do, both of them are talked about in the theme.
Yeah.
No.
The both of them are taught in these courses,
so that they develop.
In one sort, and good learning, we have
Buddha talk about these two kinds of persons,
they sum it up.
Sum it up.
One who has sum it up as physical,
and the other who has three persons on that, as physical.
So one who has three persons on that, as vehicle practices
we personally.
