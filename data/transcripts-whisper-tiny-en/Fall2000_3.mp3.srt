1
00:00:00,000 --> 00:00:16,000
I thought I have finished with the passage, but there is something that I want to talk about.

2
00:00:16,000 --> 00:00:29,000
So we will go back to the phrase, removing covetousness and grief in the world.

3
00:00:29,000 --> 00:00:53,000
We understand from the two phrases, are then clearly comprehending and mindful and removing covetousness and grief in the world, that we must make effort and we must try to be mindful and when our mentalness becomes strong.

4
00:00:53,000 --> 00:01:13,000
We will see and the objects clearly, we will see the true nature of things and at the same time we remove the covetousness and grief or we remove the mental departments.

5
00:01:13,000 --> 00:01:23,000
When we understand this much, we are happy and we are pleased with ourselves.

6
00:01:23,000 --> 00:01:32,000
But ancient teachers went deeper than just this much understanding.

7
00:01:32,000 --> 00:01:54,000
The commentary pointed out to us that the phrase removing covetousness and grief in the world shows more than what we call it.

8
00:01:54,000 --> 00:01:59,000
More than it appears to us.

9
00:01:59,000 --> 00:02:16,000
The commentary said that by this phrase, Buddha pointed out to us the power of a yogi, the ability of a yogi.

10
00:02:16,000 --> 00:02:28,000
And there are two mental states that are removed, they are covetousness and grief.

11
00:02:28,000 --> 00:02:44,000
So there are two mental states that are removed and by the removing of covetousness or let us just call attachment.

12
00:02:44,000 --> 00:02:59,000
By the removing of attachment, the removing of satisfaction rooted in bodily gravitation.

13
00:02:59,000 --> 00:03:13,000
That means when our body is in good shape, when we are healthy, when we are strong and when the body is beautiful, then we are satisfied with the body.

14
00:03:13,000 --> 00:03:18,000
That is, if we do not practice mindfulness.

15
00:03:18,000 --> 00:03:31,000
If we practice mindfulness, we will be able to remove that satisfaction rooted in the bodily gratification.

16
00:03:31,000 --> 00:03:44,000
And by removing of grief, Buddha meant the removing of dissatisfaction rooted in bodily misfortun.

17
00:03:44,000 --> 00:03:57,000
That means when our body is not in good shape, when we are sick, when we have pain and when we are ugly, we are dissatisfied with our body.

18
00:03:57,000 --> 00:04:13,000
But when we practice mindfulness meditation, we are able to remove or avoid dissatisfaction rooted in bodily misfortun.

19
00:04:13,000 --> 00:04:38,000
So by the phrase removing covetousness and grief, Buddha meant the removing of satisfaction rooted in the bodily gratification and removing of dissatisfaction rooted in bodily misfortun.

20
00:04:38,000 --> 00:04:50,000
Again, by removing of covetousness or attachment, Buddha meant the removing of delight in the body.

21
00:04:50,000 --> 00:04:54,000
Now people take delight in the body.

22
00:04:54,000 --> 00:05:02,000
And taking delight in the body is more pronounced now than ever before.

23
00:05:02,000 --> 00:05:14,000
Now, there are many fitness centers in this country and people are going to these centers and taking exercises, paying a lot of money.

24
00:05:14,000 --> 00:05:20,000
And they go to these centers, not just to be healthy.

25
00:05:20,000 --> 00:05:29,000
They go there to have a beautiful body and then they are taught to love their body or to be pleased with their body.

26
00:05:29,000 --> 00:05:32,000
So there is the delight in the body.

27
00:05:32,000 --> 00:05:36,000
And the light in the body means attachment to the body.

28
00:05:36,000 --> 00:05:56,000
But when a person practices mindfulness or we pass on our meditation and when he is able to remove attachment, that means he is removing that delight in the body.

29
00:05:56,000 --> 00:06:05,000
Well, such people want to practice contemplation of the body meditation.

30
00:06:05,000 --> 00:06:20,000
Contemplation of the body means contemplating on the different parts of the body, trying to see the body as impermanent and so on, and also trying to see different parts of the body as repulsive.

31
00:06:20,000 --> 00:06:28,000
So, such people will not want to practice contemplation on the body.

32
00:06:28,000 --> 00:06:49,000
But when a person practices we pass on our meditation and is able to remove grief or aversion, that means he is able to remove resistance to contemplation of the body.

33
00:06:49,000 --> 00:06:58,000
Such a person will not refuse to practice contemplation of the body.

34
00:06:58,000 --> 00:07:11,000
That is because he is able to remove or he is able to avoid aversion regarding the body.

35
00:07:11,000 --> 00:07:37,000
And also this phrase shows that a yogi is able to avoid putting something unreal onto the objects and taking away something real from the objects.

36
00:07:37,000 --> 00:07:57,000
Now, when a yogi is able to avoid attachment, that means he does not add anything unreal to the object.

37
00:07:57,000 --> 00:08:11,000
Now, Buddha thought that the all conditions things are impermanent, suffering, non-soul or in substantial and unlovely.

38
00:08:11,000 --> 00:08:26,000
But those who do not practice meditation think that the impermanent, good soul or everlasting and lovely.

39
00:08:26,000 --> 00:08:43,000
So, if you think something to be lovely, that means you are adding something which is unreal according to the teaching of the Buddha onto the object.

40
00:08:43,000 --> 00:08:59,000
When you are able to avoid attachment to an object, then you are able to avoid adding something unreal onto the object.

41
00:08:59,000 --> 00:09:15,000
So, that is one kind of the power of a yogi.

42
00:09:15,000 --> 00:09:21,000
When a person sees an unlovely object, he does not like it.

43
00:09:21,000 --> 00:09:29,000
He has aversion to that object. That means he does not like it to be unlovely.

44
00:09:29,000 --> 00:09:32,000
He wanted it to be lovely.

45
00:09:32,000 --> 00:09:42,000
So, that means he wants to take away something that is real, that is unloveliness from the object.

46
00:09:42,000 --> 00:10:02,000
So, by being able to remove grief, a yogi is able to avoid taking something real away from the object.

47
00:10:02,000 --> 00:10:10,000
This three are described in the commentary as the power of a yogi.

48
00:10:10,000 --> 00:10:18,000
He is free from satisfaction and dissatisfaction regarding the body.

49
00:10:18,000 --> 00:10:28,000
And he is able to stand firm against delight and non-delight.

50
00:10:28,000 --> 00:10:37,000
And he is free from putting in something unreal and taking away something real.

51
00:10:37,000 --> 00:10:42,000
So, these three are called the power of a yogi.

52
00:10:42,000 --> 00:10:56,000
An a yogi who possesses these three kinds of powers is called a yogi who is accomplished in meditation.

53
00:10:56,000 --> 00:11:05,000
When we practice meditation it is very important that we do not add anything onto the object.

54
00:11:05,000 --> 00:11:10,000
And also we do not take away anything from the object.

55
00:11:10,000 --> 00:11:30,000
Now, if we think that a certain thing is permanent, then we are putting or adding the permanence which is unreal onto the object which is impermanent.

56
00:11:30,000 --> 00:11:43,000
And when we want to, we want an impermanent to be permanent, that means we want to take away the impermanency which is real from the object.

57
00:11:43,000 --> 00:11:58,000
And if we add something and if we take away something, if we add something unreal and if we take something real from the object that we will not see the object as it is.

58
00:11:58,000 --> 00:12:11,000
And so we will be carried away by liking or disliking satisfaction or dissatisfaction to light on the light regarding the objects.

59
00:12:11,000 --> 00:12:27,000
So, Vipasana gives us the power to see things as they really are, to see things without any additions, to see things without any omissions.

60
00:12:27,000 --> 00:12:45,000
We just take the objects as they are and it is very important to see things as they are so that we can avoid both attachment and aversion to the objects.

61
00:12:45,000 --> 00:13:07,000
And these powers mentioned in the commentary can be obtained only by those who practice Vipasana meditation only by those who make effort and who practice mindfulness and who comprehends the object clearly.

62
00:13:07,000 --> 00:13:23,000
So, at the same time when a yogi comprehends the objects clearly he is able to take the object without any additions or omissions.

63
00:13:23,000 --> 00:13:35,000
So, it is only a yogi, it is only a Vipasana yogi that can take the objects as it is without additions or omissions.

64
00:13:35,000 --> 00:13:59,000
And so it is called a power of the yogi and a yogi who possesses this power is actually an accomplished metadata.

65
00:13:59,000 --> 00:14:19,000
Now, we will go to the practice. So, there are some preliminary qualities to be fulfilled before a person practices meditation.

66
00:14:19,000 --> 00:14:42,000
And once a monk went to the Buddha and asked the Buddha to teach him and breathe so that after listening to the instructions given by the Buddha, he could go to a secluded place and practice meditation.

67
00:14:42,000 --> 00:14:55,000
To him the Buddha said, you should first purify the beginning of all wholesome things.

68
00:14:55,000 --> 00:15:01,000
You should first purify the beginning of all wholesome things.

69
00:15:01,000 --> 00:15:13,000
And what is the beginning of all wholesome things?

70
00:15:13,000 --> 00:15:21,000
That is very pure and the view that is straight.

71
00:15:21,000 --> 00:15:26,000
That means a view that is correct.

72
00:15:26,000 --> 00:15:45,000
When your sealer is very pure and your understanding correct, then supported and aided by sealer, you may cultivate the four foundations of mindfulness.

73
00:15:45,000 --> 00:16:04,000
So, in this discourse, Buddha thought to that monk, two things that he should practice, he should accomplish before he practice the foundations of mindfulness.

74
00:16:04,000 --> 00:16:12,000
The first is very pure sealer, the purity of moral conduct.

75
00:16:12,000 --> 00:16:21,000
And the second is the correct understanding.

76
00:16:21,000 --> 00:16:33,000
Although this advice is given to a monk, lay people also should follow this advice.

77
00:16:33,000 --> 00:16:46,000
So, lay people who want to practice meditation must fulfill these two things, the purity of moral conduct and then correct understanding.

78
00:16:46,000 --> 00:16:50,000
Now, the purity of moral conduct is important.

79
00:16:50,000 --> 00:17:06,000
It is actually a basis for practice of meditation or concentration because when the moral conduct is not pure, concentration cannot be gained.

80
00:17:06,000 --> 00:17:27,000
Because when moral conduct is not pure, the feeling of guilt or feeling of remorse can interfere with the practice.

81
00:17:27,000 --> 00:17:36,000
Say, I am not practicing meditation and my sealer is not pure.

82
00:17:36,000 --> 00:17:42,000
Although other people may think that I am pure in my sealer, I know that I am not pure and so on.

83
00:17:42,000 --> 00:17:51,000
So, he criticizes himself, he blames himself and so he is not able to concentrate on the meditation object.

84
00:17:51,000 --> 00:18:03,000
So, if one's moral conduct is not pure, one cannot hope to attain samadhi or concentration.

85
00:18:03,000 --> 00:18:17,000
So, that is why the purity of moral conduct or pure sealer is very important.

86
00:18:17,000 --> 00:18:30,000
And for lay people to purify their moral conduct is not so difficult.

87
00:18:30,000 --> 00:18:37,000
They can take the precepts and keep them and their moral conduct is pure.

88
00:18:37,000 --> 00:18:57,000
And when we look at the stories during the time of the Buddha, we see that the purity of moral conduct can be achieved just at the beginning of the practice of meditation.

89
00:18:57,000 --> 00:19:05,000
Now, that is why yogis are made to take precepts at the beginning of the practice.

90
00:19:05,000 --> 00:19:14,000
So, that is to fulfill this one requirement for the practice of meditation.

91
00:19:14,000 --> 00:19:21,000
Minimum moral requirement for lay people is keeping five precepts.

92
00:19:21,000 --> 00:19:27,000
And these five precepts are called universal precepts.

93
00:19:27,000 --> 00:19:42,000
These precepts can be found or can be taken even when there are no Buddhas or there are no teachings of the Buddha available.

94
00:19:42,000 --> 00:19:53,000
So, even outside the dispensation of the Buddha, it is said in our books that these five precepts are kept by lay people.

95
00:19:53,000 --> 00:20:00,000
So, these five precepts are the minimum moral requirement for lay people.

96
00:20:00,000 --> 00:20:15,000
And you all know the five precepts, abstention from killing, abstention from stealing, abstention from sexual misconduct, abstention from telling lies and abstention from taking in toxic ends.

97
00:20:15,000 --> 00:20:24,000
So, if you can abstain from these five, then your moral conduct is pure.

98
00:20:24,000 --> 00:20:31,000
But at this retreat or at our retreat, yogis take eight precepts.

99
00:20:31,000 --> 00:20:39,000
Taking eight precepts and keeping them is better than taking five precepts.

100
00:20:39,000 --> 00:20:55,000
So, taking eight precepts, yogis can avoid some unwholesome states and also they get more time to devote to the practice.

101
00:20:55,000 --> 00:21:02,000
So, at our retreat, eight precepts is a standard practice for all yogis.

102
00:21:02,000 --> 00:21:11,000
So, after taking the precepts, you can rest assured that your moral conduct is pure.

103
00:21:11,000 --> 00:21:19,000
And so, you have a firm basis for the practice of meditation.

104
00:21:19,000 --> 00:21:24,000
There is a question regarding the purity of moral conduct.

105
00:21:24,000 --> 00:21:35,000
How long must a person be pure in moral conduct before he can practice meditation?

106
00:21:35,000 --> 00:21:43,000
Now, many people think that moral conduct should be pure also.

107
00:21:43,000 --> 00:21:53,000
All the life of moral conduct should be pure for a long time before they can practice meditation.

108
00:21:53,000 --> 00:22:02,000
And so, there are many people who would say, first, you purify your moral conduct during a practice meditation yet.

109
00:22:02,000 --> 00:22:13,000
And they say that you have to purify your moral conduct for a long time.

110
00:22:13,000 --> 00:22:15,000
But that is not correct.

111
00:22:15,000 --> 00:22:39,000
Because when we see the stories that occur during the time of the Buddha, we know that those who are not pure in their moral conduct,

112
00:22:39,000 --> 00:22:46,000
after they met the Buddha and listened to the Dharma and got enlightenment.

113
00:22:46,000 --> 00:23:08,000
So, from these stories, we conclude, we can conclude that it is not essential for a person to be pure in moral conduct for a long time.

114
00:23:08,000 --> 00:23:12,000
Before, he practices meditation.

115
00:23:12,000 --> 00:23:20,000
Although, if his moral conduct can be pure for a long time, it is very good.

116
00:23:20,000 --> 00:23:28,000
But sometimes, people are afraid that, oh, my moral conduct is not pure.

117
00:23:28,000 --> 00:23:31,000
And so, I cannot practice meditation.

118
00:23:31,000 --> 00:23:33,000
And so, they do not practice meditation.

119
00:23:33,000 --> 00:23:37,000
And they are deprived of this opportunity.

120
00:23:37,000 --> 00:23:55,000
So, for little people, to be pure in moral conduct, it just be a few moments or a few hours or a few days.

121
00:23:55,000 --> 00:24:17,000
There are many stories to show that even those who are not pure in moral conduct for a long time can gain enlightenment.

122
00:24:17,000 --> 00:24:21,000
I will give you only two stories.

123
00:24:21,000 --> 00:24:30,000
Now, during the time of the Buddha, there was a man who was fishing.

124
00:24:30,000 --> 00:24:41,000
On that morning, when Buddha surveyed the world with his Buddha, I, he saw this person in his mind.

125
00:24:41,000 --> 00:24:53,000
So, in the morning, when that person was fishing, Buddha went that way on his arms round, along with his disciples.

126
00:24:53,000 --> 00:25:06,000
So, when Buddha got near the man, the man bowed down the fishing rod, and then paid respects to the Buddha.

127
00:25:06,000 --> 00:25:10,000
Then the Buddha asked him, what is your name?

128
00:25:10,000 --> 00:25:14,000
Then he said, my name is Mr. Pure.

129
00:25:14,000 --> 00:25:26,000
Then Buddha said, those who kill beings who inflict suffering on beings are not called pure.

130
00:25:26,000 --> 00:25:36,000
Mr. Pure, those only those who do not inflict suffering on beings are called Mr. Pure.

131
00:25:36,000 --> 00:25:47,000
So, after hearing this verse uttered by the Buddha, he said in the books that he became a sort of partner.

132
00:25:47,000 --> 00:25:49,000
Now, he was fishing.

133
00:25:49,000 --> 00:25:55,000
So, his moral conduct was not pure until he met the Buddha.

134
00:25:55,000 --> 00:26:06,000
So, after he heard the verse uttered by the Buddha, he must have made a resolution in his mind.

135
00:26:06,000 --> 00:26:09,000
I will no longer do this.

136
00:26:09,000 --> 00:26:17,000
So, when he has made this resolution in his mind, he has purified his moral conduct.

137
00:26:17,000 --> 00:26:31,000
So, he listened to the teaching of the Buddha, and actually he went through the different stages of Vipasana while he was listening to the teaching.

138
00:26:31,000 --> 00:26:35,000
And at the end of the teaching, then he reached Sotapanahood.

139
00:26:35,000 --> 00:26:37,000
He became a Sotapana.

140
00:26:37,000 --> 00:26:39,000
So, that is one story.

141
00:26:39,000 --> 00:26:46,000
So, in this story, now you know that until he met the Buddha, his moral conduct was not pure.

142
00:26:46,000 --> 00:26:52,000
The other story is about two peak pockets.

143
00:26:52,000 --> 00:27:00,000
Again, during the time of the Buddha, there were two friends who are peak pockets.

144
00:27:00,000 --> 00:27:10,000
So, one day, they went to where the Buddha was preaching to a multitude of people.

145
00:27:10,000 --> 00:27:17,000
So, with the intention of picking a pocket or two, they entered into the audience.

146
00:27:17,000 --> 00:27:25,000
And one of them was able to snatch some money from a listener.

147
00:27:25,000 --> 00:27:31,000
But the other big pocket got interested in the teachings of the Buddha.

148
00:27:31,000 --> 00:27:37,000
And so, he did not snatch anything from anybody.

149
00:27:37,000 --> 00:27:41,000
But he listened to the Dharma, he listened to the preaching.

150
00:27:41,000 --> 00:27:49,000
And at the end of that preaching, again, he became a Sotapana.

151
00:27:49,000 --> 00:28:05,000
In this story also, the one who became a Sotapana, his moral conduct was not pure until he got interested in the teaching of the Buddha.

152
00:28:05,000 --> 00:28:18,000
So, he got interested in the teaching of the Buddha, and he must have made a resolution at that moment that I will no longer do this big pocketing.

153
00:28:18,000 --> 00:28:27,000
And so, he purified his sealer while listening to the teaching of the Buddha.

154
00:28:27,000 --> 00:28:37,000
And then, he may practice meditation while sitting. And so, at the end of the teaching, he became a Sotapana.

155
00:28:37,000 --> 00:28:55,000
So, for lead people, it is easy to purify sealer or to achieve the purity of sealer.

156
00:28:55,000 --> 00:29:06,000
It is good. It would be best to be pure in sealer for a long time, to be pure in sealer for the whole of your life.

157
00:29:06,000 --> 00:29:13,000
So, I am not encouraging you to be fishermen and pick pockets.

158
00:29:13,000 --> 00:29:19,000
And then, come to the center and practice meditation.

159
00:29:19,000 --> 00:29:32,000
But I want to let people know that even if their moral conduct was not pure in the past, still they can practice meditation.

160
00:29:32,000 --> 00:29:40,000
Because they can purify their sealer right before they sit down and practice meditation.

161
00:29:40,000 --> 00:29:52,000
So, it is good to keep the moral conduct as long as you can, and it is an ideal to be pure in moral conduct.

162
00:29:52,000 --> 00:30:09,000
But even if you are not pure in moral conduct before, still you can practice meditation because you can purify your moral conduct right at the beginning of the practice.

163
00:30:09,000 --> 00:30:17,000
The purity of moral conduct is emphasized in the teachings of the Buddha.

164
00:30:17,000 --> 00:30:23,000
Because purity of moral conduct leads to non-remorse.

165
00:30:23,000 --> 00:30:28,000
Now, if your moral conduct is not pure, you have a merciful feelings.

166
00:30:28,000 --> 00:30:33,000
So, if your moral conduct is pure, you are free from remorse.

167
00:30:33,000 --> 00:30:51,000
That freedom from remorse leads to joy and joy leads to happiness, happiness leads to peacefulness, peacefulness leads to concentration and concentration leads to seeing things as they really are.

168
00:30:51,000 --> 00:30:58,000
And this ultimately leads to attainment of enlightenment.

169
00:30:58,000 --> 00:31:11,000
So, it is the moral purity of moral conduct is at the very base of the practice taught by the Buddha.

170
00:31:11,000 --> 00:31:35,000
And so, when the monk asked him to teach and breathe what he must do, Buddha said, you purify your sealer first.

171
00:31:35,000 --> 00:31:44,000
And the second requirement Buddha taught to that monk is the view that is straight.

172
00:31:44,000 --> 00:31:48,000
So, that means the correct view.

173
00:31:48,000 --> 00:32:04,000
And the correct view is explained in the commentary as the understanding that beings have come alone as their property.

174
00:32:04,000 --> 00:32:16,000
Or in other words, understanding of and belief in the law of karma.

175
00:32:16,000 --> 00:32:21,000
And they believe in law of karma.

176
00:32:21,000 --> 00:32:27,000
That does not mean blind faith in the law of karma.

177
00:32:27,000 --> 00:32:32,000
It is belief founded upon understanding.

178
00:32:32,000 --> 00:32:41,000
Now, according to the teachings of the Buddha, there is what is called karma.

179
00:32:41,000 --> 00:32:45,000
And there are the results of karma.

180
00:32:45,000 --> 00:32:53,000
Now, when you do something, a mental state arises in the mind.

181
00:32:53,000 --> 00:33:05,000
A mental state that encourages you to do the deeds that urges you to do the deeds.

182
00:33:05,000 --> 00:33:09,000
And that mental state is called volition.

183
00:33:09,000 --> 00:33:20,000
And when Buddha taught about karma, the Buddha meant the volition.

184
00:33:20,000 --> 00:33:30,000
So, that mental state that arises in the mind when you do something good or something bad is called karma.

185
00:33:30,000 --> 00:33:41,000
And that mental state has the ability or potential to give results in the future.

186
00:33:41,000 --> 00:33:51,000
Mind has great power.

187
00:33:51,000 --> 00:33:57,000
And when mind is headed by this volition, it can give results.

188
00:33:57,000 --> 00:34:06,000
Now, we look at our body.

189
00:34:06,000 --> 00:34:14,000
This body is connected with mind, it moves, it walks, it eats and so on.

190
00:34:14,000 --> 00:34:16,000
It does many activities.

191
00:34:16,000 --> 00:34:20,000
Once, mind is separated from the body.

192
00:34:20,000 --> 00:34:22,000
What happens?

193
00:34:22,000 --> 00:34:24,000
We are dead.

194
00:34:24,000 --> 00:34:30,000
So, a dead body cannot even move.

195
00:34:30,000 --> 00:34:41,000
But, a life body, a body that is alive can do many activities.

196
00:34:41,000 --> 00:34:50,000
Now, this hand is robot, this is matter.

197
00:34:50,000 --> 00:34:54,000
So, this is heavy.

198
00:34:54,000 --> 00:34:59,000
Although, this is heavy, the mind can make it move.

199
00:34:59,000 --> 00:35:04,000
Now, I have the intention to move and so there is movement of the hand.

200
00:35:04,000 --> 00:35:08,000
So, my movement of the hand is caused by my mind.

201
00:35:08,000 --> 00:35:14,000
If I do not want to move, my hand will not move.

202
00:35:14,000 --> 00:35:22,000
So, my mind has the power to make the hand move.

203
00:35:22,000 --> 00:35:30,000
Mind cannot be seen, but the hand can be seen and it is heavy compared to mind.

204
00:35:30,000 --> 00:35:36,000
But, with the power of mind, I can move my hand.

205
00:35:36,000 --> 00:35:40,000
So, mind has power.

206
00:35:40,000 --> 00:35:47,000
And among the mental states, there is one that is called volition or gamma.

207
00:35:47,000 --> 00:35:52,000
And that has the potential to give results in the future.

208
00:35:52,000 --> 00:36:04,000
And so, when the conditions are right for the gamma to give results, then the results are produced.

209
00:36:04,000 --> 00:36:11,000
So, there is what we call gamma and there is what we call the results of gamma.

210
00:36:11,000 --> 00:36:18,000
And we would just believe in this law of gamma, that is, that is, gamma and there is

211
00:36:18,000 --> 00:36:22,000
the results of gamma.

212
00:36:22,000 --> 00:36:38,000
And also, we believe that good gamma produces good results and bad gamma produces bad results.

213
00:36:38,000 --> 00:36:47,000
And for Buddhists, this second requirement is not difficult, because since we are Buddhist,

214
00:36:47,000 --> 00:36:56,000
we already have understanding of and belief in the law of gamma.

215
00:36:56,000 --> 00:37:02,000
But, what about other people who belong to other religion?

216
00:37:02,000 --> 00:37:07,000
They want to practice meditation.

217
00:37:07,000 --> 00:37:14,000
But, should we say to them, if you do not believe in the law of gamma, you cannot practice

218
00:37:14,000 --> 00:37:22,000
meditation and turn them away.

219
00:37:22,000 --> 00:37:26,000
I do not want to do that.

220
00:37:26,000 --> 00:37:30,000
So, I am using something like this.

221
00:37:30,000 --> 00:37:34,000
You should have believed in the law of course, and effect.

222
00:37:34,000 --> 00:37:37,000
Not the law of gamma.

223
00:37:37,000 --> 00:37:41,000
I think everybody can accept this, the law of cause and effect.

224
00:37:41,000 --> 00:37:45,000
Because, when there is a cause, there is an effect and effect is always caused by the cause.

225
00:37:45,000 --> 00:37:54,000
So, this relationship as cause and effect can be seen everywhere and I think everybody

226
00:37:54,000 --> 00:37:56,000
can accept this.

227
00:37:56,000 --> 00:38:02,000
So, to those who do not believe in gamma, so, we would say, okay, if you do not believe

228
00:38:02,000 --> 00:38:04,000
in gamma, it is okay.

229
00:38:04,000 --> 00:38:07,000
But, do you believe in the law of cause and effect?

230
00:38:07,000 --> 00:38:09,000
I think he will say yes.

231
00:38:09,000 --> 00:38:16,000
Then, you can practice meditation.

232
00:38:16,000 --> 00:38:38,000
So, according to this discourse given by the Buddha, there are two preliminary requirements

233
00:38:38,000 --> 00:38:42,000
before one practices as the foundations of mindfulness.

234
00:38:42,000 --> 00:38:53,000
But, there are some other things also that a person should do before he practice as

235
00:38:53,000 --> 00:38:58,000
meditation and that is asking forgiveness.

236
00:38:58,000 --> 00:39:02,000
Now, we do asking forgiveness every morning.

237
00:39:02,000 --> 00:39:17,000
That is because it is important asking forgiveness if it is very important for a person

238
00:39:17,000 --> 00:39:24,000
who is practicing meditation.

239
00:39:24,000 --> 00:39:40,000
Thank you.

