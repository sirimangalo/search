1
00:00:00,000 --> 00:00:04,800
that you can't control it because if you could, it wouldn't be dukka, obviously.

2
00:00:04,800 --> 00:00:07,720
But also that it's a bit more simple than that.

3
00:00:07,720 --> 00:00:12,400
It's not intellectual, it's just in the sense that the person gives it up.

4
00:00:12,400 --> 00:00:15,400
And at that I mean, it's not mine.

5
00:00:15,400 --> 00:00:21,520
And the one I'm aiming to do with it, it's like if a guy finds out that a woman's pregnant

6
00:00:21,520 --> 00:00:28,200
he immediately wants to disavow fatherhood, because he knows how much stress and suffering.

7
00:00:28,200 --> 00:00:31,000
A terrible example, but it is a good example.

8
00:00:31,000 --> 00:00:37,400
Now the mind feels when it realizes that this is suffering, it says only.

9
00:00:37,400 --> 00:00:41,700
At Gentong tells a funny story, it's actually from the Visudimanga, but

10
00:00:41,700 --> 00:00:46,200
of course he tells it in a more entertaining way.

11
00:00:46,200 --> 00:00:49,600
About this guy who wakes up in the morning,

12
00:00:49,600 --> 00:00:53,800
wakes up early in the morning to drive his cattle, his cow.

13
00:00:53,800 --> 00:00:59,800
So he has to, in the pitch dark, he has to go and find his cow, and then he starts beating it,

14
00:00:59,800 --> 00:01:03,400
hitting it, and driving it up to the pasture.

15
00:01:03,400 --> 00:01:07,000
And he gets up to his pasture, and just as the sun is coming up, he looks at the cow,

16
00:01:07,000 --> 00:01:10,400
and he realizes that's not my cow.

17
00:01:10,400 --> 00:01:16,800
And he gets scared because he's stolen somebody else's cow.

18
00:01:16,800 --> 00:01:18,800
There's a lot of stories like that in the Visudimanga,

19
00:01:18,800 --> 00:01:24,800
this realization that something isn't yours, something isn't.

20
00:01:24,800 --> 00:01:31,800
There isn't a goodness to your attachment to it, basically.

21
00:01:31,800 --> 00:01:37,800
There's a sort of realization that comes, you just disavow the thing once you see it clearly.

22
00:01:37,800 --> 00:01:43,800
I know that's a little even a little clearer because when you look at things you see clearly that they're non-salvents.

23
00:01:43,800 --> 00:01:47,800
That doesn't actually relate to the other characteristics.

24
00:01:47,800 --> 00:01:52,800
It's the idea of this waking up to the reality thing.

25
00:01:52,800 --> 00:01:54,800
But I wouldn't overthink that connection.

26
00:01:54,800 --> 00:01:59,800
You don't have to realize, I mean that what the cow example shows,

27
00:01:59,800 --> 00:02:05,800
you don't have to realize one, and you don't usually actually realize one from the other.

28
00:02:05,800 --> 00:02:12,800
But they do go hand in hand, realizing one is enough to have an understanding of the other.

29
00:02:12,800 --> 00:02:17,800
So meaning that you'll often, the way you'll perceive them not often,

30
00:02:17,800 --> 00:02:20,800
you'll always perceive them one by the other.

31
00:02:20,800 --> 00:02:25,800
But what it means is that understanding one is enough to already understand the other.

32
00:02:25,800 --> 00:02:30,800
Once you see that something is dukkha, it'll be very unsatisfying.

33
00:02:30,800 --> 00:02:34,800
There's no question of it being impermanent and it being non-salvents.

34
00:02:34,800 --> 00:02:38,800
And likewise, we see a thing as impermanent, and you really see that.

35
00:02:38,800 --> 00:02:45,800
The corralery is that it's suffering and non-salvents.

36
00:02:45,800 --> 00:02:48,800
So it's more like that.

37
00:02:48,800 --> 00:02:55,800
They really mean pretty much the same thing in an ultimate sense.

38
00:02:55,800 --> 00:03:00,800
What is cheetah? Is it actually actual metaphysical entity?

39
00:03:00,800 --> 00:03:04,800
Does it abide in any sense of the word?

40
00:03:04,800 --> 00:03:09,800
No, cheetah is something that arises in seestah. It refers to the moment

41
00:03:09,800 --> 00:03:13,800
where the mind experiences a thing.

42
00:03:13,800 --> 00:03:18,800
The thing which experiences the knowledge or not the thing,

43
00:03:18,800 --> 00:03:20,800
but the knowledge of something.

44
00:03:20,800 --> 00:03:23,800
The knowledge that arises in seestah is the awareness that arises in seestah.

45
00:03:23,800 --> 00:03:26,800
So when you see something, there's an awareness of the scene.

46
00:03:26,800 --> 00:03:30,800
And that awareness arises in seestah with the object.

47
00:03:30,800 --> 00:03:34,800
It's a little even a little bit more complicated than that.

48
00:03:34,800 --> 00:03:36,800
It'll arise in seestah many times for the same object,

49
00:03:36,800 --> 00:03:46,800
but that's maybe a little overly technical.

50
00:03:46,800 --> 00:03:50,800
One day, how do you define corona?

51
00:03:50,800 --> 00:03:56,800
corona is the mental bent,

52
00:03:56,800 --> 00:04:08,800
the lack of a better word, or inclination towards preventing the suffering of others.

53
00:04:08,800 --> 00:04:17,800
In an ultimate sense, it's merely the basis of it is not causing the cruelty.

54
00:04:17,800 --> 00:04:19,800
So in climbing towards,

55
00:04:19,800 --> 00:04:26,800
the climbing away from causing harm to others.

56
00:04:26,800 --> 00:04:32,800
Now it can manifest itself positively in terms of actually working

57
00:04:32,800 --> 00:04:36,800
to alleviate other people's suffering.

58
00:04:36,800 --> 00:04:41,800
So this is supposed to make that which is working towards the promoting of happiness.

59
00:04:41,800 --> 00:04:45,800
But they're in some ways, same side of the coin.

60
00:04:45,800 --> 00:04:49,800
Sorry, different sides of the same coin.

61
00:04:49,800 --> 00:04:58,800
Because freeing people from suffering also makes them happier.

62
00:04:58,800 --> 00:05:06,800
Which of the four elements is pain?

63
00:05:06,800 --> 00:05:16,800
It's not one of the four elements, but...

64
00:05:16,800 --> 00:05:19,800
I mean, way deny is mental,

65
00:05:19,800 --> 00:05:26,800
but there is something called dukka kai away dinner.

66
00:05:26,800 --> 00:05:29,800
And I think that's just a technical term.

67
00:05:29,800 --> 00:05:32,800
Because pain is actually mental.

68
00:05:32,800 --> 00:05:36,800
The feeling of pain, way deny is a mental thing.

69
00:05:36,800 --> 00:05:38,800
The body doesn't feel pain.

70
00:05:38,800 --> 00:05:41,800
So technically it would be when the body is,

71
00:05:41,800 --> 00:05:45,800
when the elements are out of balance,

72
00:05:45,800 --> 00:05:49,800
like you have an extreme amount of pressure.

73
00:05:49,800 --> 00:05:55,800
Pressure is probably the most common cause of pain.

74
00:05:55,800 --> 00:05:57,800
The physical pain.

75
00:05:57,800 --> 00:06:01,800
Too much pressure.

76
00:06:01,800 --> 00:06:04,800
So swelling will do it.

77
00:06:04,800 --> 00:06:07,800
Bending of the body.

78
00:06:07,800 --> 00:06:10,800
Pinching.

79
00:06:10,800 --> 00:06:13,800
But then the whole idea of the nerves, right,

80
00:06:13,800 --> 00:06:14,800
the nerves in the body.

81
00:06:14,800 --> 00:06:15,800
That doesn't work.

82
00:06:15,800 --> 00:06:16,800
It's the experience.

83
00:06:16,800 --> 00:06:18,800
But it's different.

84
00:06:18,800 --> 00:06:21,800
Pain and the element.

85
00:06:21,800 --> 00:06:23,800
The elements are just a cause of pain.

86
00:06:23,800 --> 00:06:32,800
They're not the pain.

87
00:06:32,800 --> 00:06:34,800
Good evening, Bante.

88
00:06:34,800 --> 00:06:36,800
Can you tell me what exactly is enlightenment

89
00:06:36,800 --> 00:06:39,800
is defined in the teravada tradition?

90
00:06:39,800 --> 00:06:42,800
It seems different schools of Buddhism have different ideas

91
00:06:42,800 --> 00:06:46,800
on what this is, which is confusing.

92
00:06:46,800 --> 00:06:48,800
I did a video on this, I think.

93
00:06:48,800 --> 00:06:50,800
That was fairly comprehensive.

94
00:06:50,800 --> 00:06:54,800
Again, I don't want to make a long answer to any of these questions.

95
00:06:54,800 --> 00:06:57,800
So you can go and hopefully try and find that

96
00:06:57,800 --> 00:07:00,800
or maybe someone in the chat.

97
00:07:00,800 --> 00:07:02,800
Those are so good about finding these videos,

98
00:07:02,800 --> 00:07:05,800
if you can find it and post it.

99
00:07:05,800 --> 00:07:10,800
But we don't actually use the word enlightenment so much.

100
00:07:10,800 --> 00:07:20,800
The word is budget, which means to awaken or to come to know,

101
00:07:20,800 --> 00:07:24,800
to wake up or awakening or to come to know,

102
00:07:24,800 --> 00:07:26,800
it is too meaning.

103
00:07:26,800 --> 00:07:28,800
But it's actually the same meaning, right?

104
00:07:28,800 --> 00:07:29,800
Because when you're on a hunt, when you're asleep,

105
00:07:29,800 --> 00:07:32,800
you don't know in a general sense.

106
00:07:32,800 --> 00:07:34,800
When you wake up, you come to know.

107
00:07:34,800 --> 00:07:37,800
That's where Buddha comes from.

108
00:07:37,800 --> 00:07:44,800
But the word enlightenment is generally used to refer to two different things.

109
00:07:44,800 --> 00:07:45,800
I would say two different.

110
00:07:45,800 --> 00:07:48,800
Let's make it simple and say there are two different aspects.

111
00:07:48,800 --> 00:07:51,800
Two different things.

112
00:07:51,800 --> 00:07:57,800
So the Buddha is enlightened in the sense that he knows everything.

113
00:07:57,800 --> 00:08:03,800
But an Arahan to follow him is enlightened in the sense that they've given up everything.

114
00:08:03,800 --> 00:08:06,800
Now Buddha also gives up everything, so that's part of his enlightenment.

115
00:08:06,800 --> 00:08:07,800
The other thing.

