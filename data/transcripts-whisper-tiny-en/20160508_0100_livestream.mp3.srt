1
00:00:00,000 --> 00:00:26,520
I'm good evening everyone, I'm not sure if that's any better, I'm broadcasting live

2
00:00:26,520 --> 00:00:44,240
me seventh. Today's quote is about water. From the Mindapanha which is

3
00:00:44,240 --> 00:00:55,440
probably I would say an underappreciated source of insight. It's much later in

4
00:00:55,440 --> 00:01:00,680
many years after the Buddha passed away, I think at least a few hundred

5
00:01:00,680 --> 00:01:14,560
maybe more. But it's very well written, very well organized and it's got much

6
00:01:14,560 --> 00:01:19,360
that is insightful about the Buddha's teaching. It's a lot of questions, it's

7
00:01:19,360 --> 00:01:27,200
the questions of King Melinda or Meniander I think. It's a Greek king. It's identified

8
00:01:27,200 --> 00:01:36,200
with a Greek king who conquered India or something. He came in contact with

9
00:01:36,200 --> 00:01:43,280
his Buddhist monks and asked them all sorts of questions and most of them

10
00:01:43,280 --> 00:01:52,880
couldn't answer the question. So they directed him to Nagasena, very well

11
00:01:52,880 --> 00:01:59,920
learned in monk. He was skilled in debate and skilled in

12
00:01:59,920 --> 00:02:08,600
liberation and skilled in answering questions. So it's many, many questions

13
00:02:08,600 --> 00:02:13,040
of a quite a large volume. It's actually available in the internet, an old

14
00:02:13,040 --> 00:02:22,880
translation, but pretty good. You can find it at sacredtextsacrid-texts.org

15
00:02:22,880 --> 00:02:31,240
worth reading. But this is about water. So comparison and he has a lot, it makes a

16
00:02:31,240 --> 00:02:38,040
lot of comparisons and he talks about Nibana, Nirvana, using a lot of

17
00:02:38,040 --> 00:02:46,440
different comparisons of similes. But he was talking about a meditator. Let's

18
00:02:46,440 --> 00:02:54,560
see if we can find the word for student of meditation. So the question is

19
00:02:54,560 --> 00:03:22,600
I think it's part of a larger text. He's asking what are the five ways

20
00:03:22,600 --> 00:03:33,640
five things, factors of water that one should, could, should grasp, should take

21
00:03:33,640 --> 00:03:46,280
up. So how should one be like water? Just want to find the word, yoga, yoga,

22
00:03:46,280 --> 00:03:55,560
whatever. Someone who is practicing yoga basically. So this is what yoga was

23
00:03:55,560 --> 00:04:01,000
how the word yoga was used. Yoga was used for, to mean exertion. But yoga,

24
00:04:01,000 --> 00:04:06,800
whatever is the word that came to me. It's probably adopted from Hinduism, but

25
00:04:06,800 --> 00:04:14,120
it was used in Buddhism to mean someone who is on the spiritual path, someone

26
00:04:14,120 --> 00:04:26,320
who is exerting themselves. We also call them yogi. So we entirely use this word

27
00:04:26,320 --> 00:04:33,640
a lot. They call meditators yogis. Yogi, they change it, put the word. Someone

28
00:04:33,640 --> 00:04:40,000
who is practicing yoga is a yogi. But it doesn't mean yoga in the modern sense.

29
00:04:40,000 --> 00:04:45,640
Yoga just means one day. Because a country where you, you, you, means you

30
00:04:45,640 --> 00:04:48,560
enjoy, which means to be connected. It's actually where I think the word

31
00:04:48,560 --> 00:04:54,840
uta comes from, from my name, uta meaning a yoke. So the ox would have a yoke and

32
00:04:54,840 --> 00:05:03,480
would be tied to some, tied to the work. So we're comparing water to such a

33
00:05:03,480 --> 00:05:08,320
person. How should such a person, someone who is undertaking the training of the

34
00:05:08,320 --> 00:05:15,320
Buddha, morality, concentration, wisdom? How should they be like water?

35
00:05:15,320 --> 00:05:25,920
Water is a good object for comparison. Why? Because water is naturally calm.

36
00:05:25,920 --> 00:05:36,520
Water is something that is still smooth as a reflection in the sense of being

37
00:05:36,520 --> 00:05:49,560
perfectly smooth and unshaken. In the same way a meditator should be like a

38
00:05:49,560 --> 00:05:54,360
pool of water, dispelling trickery, cajolery, insinuation and

39
00:05:54,360 --> 00:06:01,120
dissembling. Should be well poised, unshaken, untroubled, and quite pure by nature.

40
00:06:01,120 --> 00:06:10,120
Water to us humans, water is like the purest substance. It's what we survive on.

41
00:06:10,120 --> 00:06:18,960
We need water to survive. We're just, honey, we're just talking about water today.

42
00:06:18,960 --> 00:06:27,000
I was at McMaster University at the Peace Studies booth. We were talking to

43
00:06:27,000 --> 00:06:36,880
pretend to perspective students about the Peace Studies program. I'm talking

44
00:06:36,880 --> 00:06:42,600
about water because the Peace Studies department has gotten interested in the

45
00:06:42,600 --> 00:06:50,440
water situation. It's about social, it's the social justice aspect of Peace

46
00:06:50,440 --> 00:06:57,160
Studies. I'm not really involved in it, but so they're trying to address the

47
00:06:57,160 --> 00:07:02,080
issue like in Flint, Michigan and the issue with the First Nations people here

48
00:07:02,080 --> 00:07:10,000
and not having clean water to drink and to use. Having polluted water, having

49
00:07:10,000 --> 00:07:24,200
their water contaminated with chemicals and toxins. We're talking about

50
00:07:24,200 --> 00:07:29,920
Daoism. One of the people working at the book had quite a conversation with

51
00:07:29,920 --> 00:07:36,600
her about many things, but one of the most water, how water is, water is

52
00:07:36,600 --> 00:07:40,480
basically this, so I think she'd be interested in this quote. She probably sent it

53
00:07:40,480 --> 00:07:48,680
to her. Water is pure, water is unshaken, water is calm, water is nurturing. I

54
00:07:48,680 --> 00:07:53,280
think she would say because she's into this idea of nurturing. She sees it as

55
00:07:53,280 --> 00:07:59,600
sort of a feminine quality. So we're talking about feminism and feminine

56
00:07:59,600 --> 00:08:08,120
qualities and masculine qualities. Compassion being feminine. I mean, traditionally

57
00:08:08,120 --> 00:08:11,040
being understood, but you know, we're both, we're trying to avoid the word

58
00:08:11,040 --> 00:08:18,360
feminine and masculine, but talking about the different energies in the world.

59
00:08:18,360 --> 00:08:26,560
We live in a very quote-unquote masculine world, and so there's a certain type of

60
00:08:26,560 --> 00:08:31,480
energy that one might call masculine, hard. In the end, we use the words

61
00:08:31,480 --> 00:08:39,260
hard and soft, aggressively. Water is not tasting, so fire in water, you

62
00:08:39,260 --> 00:08:51,200
might say, but here it's water is purity. Water is clarity, water is calm. So the

63
00:08:51,200 --> 00:08:55,400
first one is this unshaken. As a meditator, we should be unshaken by the

64
00:08:55,400 --> 00:09:01,160
scissitudes of life, by the experiences of her meditation, because they'll try

65
00:09:01,160 --> 00:09:07,560
to shake you, and then they'll shake you again, and they'll shake you in new

66
00:09:07,560 --> 00:09:15,720
and different ways every all the time. The things will change, and then that

67
00:09:15,720 --> 00:09:21,320
way that they change will change. You have to be constantly, you have to be

68
00:09:21,320 --> 00:09:28,080
adaptive, inflexible. You have to be clever, and you have to be present. As soon

69
00:09:28,080 --> 00:09:37,880
as you become static, you get carried away by the vicissitudes of life.

70
00:09:41,600 --> 00:09:50,480
The second one is water is poised and naturally cool, and so yoga, what

71
00:09:50,480 --> 00:09:58,200
that I should be compassionate should be possessed of patience, should be cool in

72
00:09:58,200 --> 00:10:07,840
this way. Patients love and mercy. They should not be hotheaded, it should not be

73
00:10:07,840 --> 00:10:12,960
quick to anger, it should not be irritable. It should not be impatient, patience is

74
00:10:12,960 --> 00:10:19,280
very important, patience with yourself, patience with other people, patience.

75
00:10:19,280 --> 00:10:26,080
And patience with things you want as well, not just immediately giving up to

76
00:10:26,080 --> 00:10:44,040
being controlled by your desires. As water makes the impure pure, even so

77
00:10:44,040 --> 00:10:53,200
much, one should be pure, one should have no faults, one should not have any

78
00:10:53,200 --> 00:10:57,280
reason to be reprimanded by the wise, wise sheep, people should not be able to

79
00:10:57,280 --> 00:11:03,480
sense your one, no matter where they are in private or in public. They should be

80
00:11:03,480 --> 00:11:12,920
pure in their deeds, in their speech, as water is wanted by everyone,

81
00:11:12,920 --> 00:11:19,080
even so a meditator should be, should be the kind of person who is appreciated.

82
00:11:19,080 --> 00:11:27,560
You should work to make yourself not a burden, not a source of

83
00:11:27,560 --> 00:11:32,960
irritation for others, because this is problematic, both for others and for

84
00:11:32,960 --> 00:11:39,680
yourself to create suffering. Obviously, you should not be such a person, you should

85
00:11:39,680 --> 00:11:49,200
think of water as being, water is being valuable, as being pure. Whenever someone

86
00:11:49,200 --> 00:12:04,120
someone sees pure water, there's nothing negative about water. It's very much

87
00:12:04,120 --> 00:12:15,320
desired and appreciated. We shouldn't be like water in that way, and finally,

88
00:12:15,320 --> 00:12:21,800
water troubles no one. It's kind of the same. The first one is, people think

89
00:12:21,800 --> 00:12:26,360
positively and the next one is, people should not think negatively, don't think

90
00:12:26,360 --> 00:12:31,920
negatively of water, they think positively. So you'll go at, should I should not

91
00:12:31,920 --> 00:12:39,440
do anything wrong that makes others unhappy? Water doesn't hurt people. Still

92
00:12:39,440 --> 00:12:47,400
water doesn't. Water actually can be quite troublesome. And certain instances,

93
00:12:47,400 --> 00:12:54,400
you can drown with water. Water can scald you if it's too hot.

94
00:12:54,400 --> 00:13:05,800
But here is the connection that the comparison is with a still forest pool, calm

95
00:13:05,800 --> 00:13:14,160
water, pure water is valuable and appreciated. It's a good imagery, it's the

96
00:13:14,160 --> 00:13:18,000
kind of thing that we can think of when you want to think of what kind of a

97
00:13:18,000 --> 00:13:23,800
person you'd like to be, as a meditator, which should I be, and water is

98
00:13:23,800 --> 00:13:29,800
something. It comes easily to mind, of course, because it's so much a part of

99
00:13:29,800 --> 00:13:34,400
our life. And when you think about the purity of water and the stillness of a

100
00:13:34,400 --> 00:13:43,680
pool of water, the smoothness of the surface, you think about the cleansing

101
00:13:43,680 --> 00:13:52,760
nature of water. I think about how precious it is. So perfect it is. Something

102
00:13:52,760 --> 00:14:05,880
to aspire for, aspire towards. So a little bit of dhamma. Today, as I said, we

103
00:14:05,880 --> 00:14:11,160
were at the McMaster. And then we went to see a new house, thinking about moving

104
00:14:11,160 --> 00:14:16,040
to a new house, just to get more rooms, because we have many people wanting to

105
00:14:16,040 --> 00:14:22,160
come and meditate here. So we're looking at, we went to look at the house, we're

106
00:14:22,160 --> 00:14:27,080
talking about. We're going to try to look at several different houses and see

107
00:14:27,080 --> 00:14:33,960
which one is most suitable. One today was good. Actually, maybe a little smaller

108
00:14:33,960 --> 00:14:42,240
than this house, but more rooms. So I have to think about whether it

109
00:14:42,240 --> 00:14:47,360
might feel a little claustrophobic, because it's small. You'll have to stay here

110
00:14:47,360 --> 00:14:53,920
for a long time, maybe in any more space. You know, you pack a lot of people into

111
00:14:53,920 --> 00:15:23,760
one small house. I want to think about that. Anyway, any questions?

112
00:15:23,760 --> 00:15:28,120
What are your thoughts on the new cadam Buddhist? I don't have any thoughts on

113
00:15:28,120 --> 00:15:32,600
the new cadam, but Buddhism. I kind of like to shy away and I'm happy that I

114
00:15:32,600 --> 00:15:36,520
don't know things about many things, because otherwise, as happened with a

115
00:15:36,520 --> 00:15:46,120
lot of sutra, I tend to be somewhat opinionated. So I kind of try to avoid

116
00:15:46,120 --> 00:15:50,760
questions like, what do you think of x and thus x is something within her

117
00:15:50,760 --> 00:16:06,440
tradition? Pretty sure it's Tibetan, but yeah, if a Google search returns many

118
00:16:06,440 --> 00:16:13,080
results claiming that something is a cult, it's a good reason to be wary.

119
00:16:13,080 --> 00:16:18,440
Should start your own centers, start your own group, have people invite people

120
00:16:18,440 --> 00:16:23,240
over to your house, start a meetup, go to meetup.com and start a meditation

121
00:16:23,240 --> 00:16:29,440
group, and then you can come on our hangout, you can broadcast and they can

122
00:16:29,440 --> 00:16:36,440
lead you, you can lead you in meditation or something, give a talk to your group.

123
00:16:36,440 --> 00:16:41,280
Everyone should start groups in their own area, we can have it as big network.

124
00:16:41,280 --> 00:16:52,600
We all meditate together, we can call it the new Siri Mangaloo Buddhism, the

125
00:16:52,600 --> 00:17:20,880
Siri Mangaloo tradition. We've been doing walking meditation with our kids,

126
00:17:20,880 --> 00:17:28,920
as effective as they like to walk in a much faster pace. Probably not, I would

127
00:17:28,920 --> 00:17:35,000
say walking meditation for young kids is difficult, as they tend to be in

128
00:17:35,000 --> 00:17:44,400
patient. If you want to teach kids how to, it depends on their age I suppose,

129
00:17:44,400 --> 00:17:49,840
but absolutely they shouldn't walk fast. It's not about walking, it's about

130
00:17:49,840 --> 00:17:59,120
being aware of the movements of the feet. It's not, it's not an easy thing.

131
00:17:59,120 --> 00:18:03,360
When kids tend to be very excited and full of energy, they have a hard time

132
00:18:03,360 --> 00:18:09,600
being patient. If you can teach them patients, that'd be awesome, but walking

133
00:18:09,600 --> 00:18:15,080
meditation requires patience. That's really the question, can your kids develop

134
00:18:15,080 --> 00:18:22,560
patients, because you have to walk slowly, you need to develop patients. They're

135
00:18:22,560 --> 00:18:52,400
walking fast, because they're in patient.

136
00:18:52,400 --> 00:18:58,920
When they meditate, I feel claustrophobic. Well, have you read my booklet on how to

137
00:18:58,920 --> 00:19:04,280
meditate? You haven't, that's where I recommend you start. It might help with

138
00:19:04,280 --> 00:19:10,640
your claustrophobia. If you feel that, it might be, it's fear, right, phobia. So

139
00:19:10,640 --> 00:19:16,560
then you would say afraid, afraid, worried, worried, or anxious, anxious. But if

140
00:19:16,560 --> 00:19:26,280
you haven't read my booklet, I recommend that. Why avoid these kind. I haven't

141
00:19:26,280 --> 00:19:30,360
really done so much to answer your question. If you haven't talked about it,

142
00:19:30,360 --> 00:19:36,000
don't talk about details, because it's actually quite simple to fix if you're

143
00:19:36,000 --> 00:19:42,720
practicing the meditation as I teach, as we teach. So there's not like any

144
00:19:42,720 --> 00:19:48,480
special tips for things like claustrophobia. I mean, the thing is, these

145
00:19:48,480 --> 00:19:55,160
things don't, these conditions don't exist. You feel fear. And that's the

146
00:19:55,160 --> 00:19:59,440
key, is that there's a feeling of fear that arises. And so the practice that we

147
00:19:59,440 --> 00:20:05,600
thought we followed would be to say to yourself, afraid, afraid. But to read my

148
00:20:05,600 --> 00:20:13,280
booklet, it's sort of, I hope, I think, you know, I'm sad and fairly good detail.

149
00:20:36,600 --> 00:20:50,200
Any other questions? I'm really serious about this starting demo groups in your

150
00:20:50,200 --> 00:20:55,480
area. I think it'd be great. And we can do it like once a week. We could all meet

151
00:20:55,480 --> 00:21:05,440
online and I could give a talk. Or you could just do whatever day of the week.

152
00:21:05,440 --> 00:21:13,840
And just come on at 9 p.m. with your group. And join our hangout. Of course, I

153
00:21:13,840 --> 00:21:18,440
expose some people to don't want their meditation broadcast on the internet.

154
00:21:18,440 --> 00:21:23,720
I suppose there's that. Well, one day a week we could do a private hangout that

155
00:21:23,720 --> 00:21:28,000
wouldn't have to be broadcast on the internet. Although it's nice to have the

156
00:21:28,000 --> 00:21:36,680
internet community involved. Something to think about. I have done that though. Private

157
00:21:36,680 --> 00:21:43,320
Skype calls to groups. Like there was a group in Texas who wanted me to lead them in

158
00:21:43,320 --> 00:21:50,240
meditation once a week and have done online stuff like that. That's what I think you

159
00:21:50,240 --> 00:22:00,440
've done. Okay. And then I'll say good night. Tomorrow, Mother's Day, I'm going to miss

160
00:22:00,440 --> 00:22:08,880
a saga for the Sri Lankan. We suck a celebration. We suck a food just celebration. We

161
00:22:08,880 --> 00:22:24,320
suck a food. I mean the full moon of the month of Osaka. Anyway, have a good night.

