1
00:00:00,000 --> 00:00:02,000
Fear upon losing awareness

2
00:00:03,360 --> 00:00:05,360
Question I have a problem

3
00:00:05,760 --> 00:00:10,120
The moment I lose awareness the fears and negative emotions are attacking me

4
00:00:10,720 --> 00:00:14,200
It is impossible to maintain constant awareness all the time

5
00:00:14,920 --> 00:00:16,920
Can you give me some advice?

6
00:00:17,640 --> 00:00:18,960
Answer

7
00:00:18,960 --> 00:00:21,800
The real problem is in trying to maintain awareness

8
00:00:22,640 --> 00:00:26,240
Let's call awareness X and negative emotions. Why?

9
00:00:26,880 --> 00:00:28,880
The general statement is

10
00:00:28,880 --> 00:00:31,640
As soon as I lose X, why happens?

11
00:00:32,640 --> 00:00:35,120
The real problem is the dependency on X

12
00:00:36,000 --> 00:00:40,280
If your peace of mind depends on something, that is the crutch for you

13
00:00:41,040 --> 00:00:43,360
That thing that you depend on is the problem

14
00:00:44,000 --> 00:00:46,000
That is the basis of Buddhist practice

15
00:00:46,840 --> 00:00:52,040
You have to start by observing the need in this case. They need to be aware all the time

16
00:00:52,040 --> 00:00:58,040
And what you are seeing is that your mind is not stable, satisfying or controllable

17
00:00:58,680 --> 00:01:01,000
And this makes you think there is some kind of problem

18
00:01:02,360 --> 00:01:05,160
Actually, you are beginning to see the nature of your mind

19
00:01:05,880 --> 00:01:07,880
You are starting to see things as they are

20
00:01:09,000 --> 00:01:11,240
This is what you see when you practice mindfulness

21
00:01:11,800 --> 00:01:13,880
That you can't control even your own mind

22
00:01:13,880 --> 00:01:21,880
The attachment to some stable, constant awareness has to be dealt with before you can approach the fear and negative emotions

23
00:01:23,560 --> 00:01:27,400
If you are distracted, note, distracted, distracted

24
00:01:28,600 --> 00:01:31,320
If you want to be alert, focused and aware

25
00:01:31,960 --> 00:01:34,120
Note, wanting, wanting

26
00:01:35,480 --> 00:01:37,800
If you dislike the fact that you have lost focus

27
00:01:38,520 --> 00:01:42,280
Note, disliking, disliking, etc

28
00:01:42,280 --> 00:01:47,080
As for the fear and negativity, even these are not problems

29
00:01:47,400 --> 00:01:48,760
They are just experiences

30
00:01:50,280 --> 00:01:55,720
Seeing them as a problem will only make things worse, as you will react to them, feeding further negativity

31
00:01:56,840 --> 00:01:58,120
Start just looking at them

32
00:01:59,080 --> 00:02:00,680
Look at everything that arises

33
00:02:01,320 --> 00:02:06,280
Do not think to yourself, oh, this is not good because I am going to get afraid again

34
00:02:06,280 --> 00:02:11,320
Or what about those negative emotions that I had yesterday? What have they come back?

35
00:02:12,200 --> 00:02:14,840
When you could be focusing on what is right here and now

36
00:02:16,360 --> 00:02:18,360
Awareness is right here and now

37
00:02:19,080 --> 00:02:21,320
You can create awareness at any moment

38
00:02:21,320 --> 00:02:23,400
You cannot lose it because you did not have it

39
00:02:24,200 --> 00:02:25,880
You created in one moment

40
00:02:25,880 --> 00:02:27,880
And that is the moment that you have awareness

41
00:02:29,080 --> 00:02:33,320
Specifically, we are trying for a type of awareness that is firm and objective

42
00:02:33,320 --> 00:02:36,040
Where you simply know the object as it is

43
00:02:36,760 --> 00:02:39,160
Without any kind of judgment or extrapolation

44
00:02:40,360 --> 00:02:42,920
You can create this type of awareness at any moment

45
00:02:44,200 --> 00:02:46,520
The labeling or noting is a recognition

46
00:02:47,160 --> 00:02:49,400
It reaffirms the recognition of the object

47
00:02:50,600 --> 00:02:54,360
When you recognize something, you reaffirm that recognition with the labeling

48
00:02:55,000 --> 00:02:58,840
And you keep the mind at bare recognition without getting caught up in it

49
00:02:58,840 --> 00:03:03,320
When this is done, all negative emotions disappear in a moment

50
00:03:03,880 --> 00:03:06,280
And only the physical manifestations are left

51
00:03:07,880 --> 00:03:10,520
Negative emotions last for only a moment

52
00:03:10,520 --> 00:03:11,960
But they affect the body

53
00:03:11,960 --> 00:03:14,920
For example, you might think that you are still angry

54
00:03:14,920 --> 00:03:17,720
Because you have a headache, tension in a body

55
00:03:17,720 --> 00:03:19,640
Or a fast heartbeat, etc

56
00:03:20,440 --> 00:03:22,280
But none of those things are anger

57
00:03:22,280 --> 00:03:25,640
Rather, they are the physical manifestations caused by the anger

58
00:03:25,640 --> 00:03:30,440
You can focus on the physical manifestations and deal with them

59
00:03:30,440 --> 00:03:33,000
But the actual anger only lasts for a moment

60
00:03:34,760 --> 00:03:38,040
If you are unmindful of the physical manifestations

61
00:03:38,040 --> 00:03:41,720
Anger can arise again, creating a cycle of stress and suffering

62
00:03:42,760 --> 00:03:43,960
The same goes for fear

63
00:03:44,920 --> 00:03:47,320
Not all of what we call fear is actually fear

64
00:03:48,360 --> 00:03:52,040
Fear is a momentary mental state that causes changes in the body

65
00:03:52,040 --> 00:03:56,840
And then we associate these changes with fear and become upset about them

66
00:03:57,480 --> 00:03:59,720
Then we get more afraid and its nobles

67
00:04:01,400 --> 00:04:06,040
If you are mindful of both the emotions and the physical manifestations moment to moment

68
00:04:06,760 --> 00:04:08,680
Slowly, they will work themselves out

69
00:04:10,360 --> 00:04:14,200
Be patient, be realistic to not have expectations

70
00:04:14,760 --> 00:04:16,440
And consider that you are here to learn

71
00:04:16,440 --> 00:04:21,880
That is why what we say we are doing is to practice rather than to perfect

72
00:04:22,680 --> 00:04:24,120
You are not here to perfect

73
00:04:24,520 --> 00:04:25,720
You are here to practice

74
00:04:26,600 --> 00:04:28,680
And more importantly, you are here to learn

75
00:04:29,400 --> 00:04:32,120
It is not a magic trick where you sit and meditate

76
00:04:32,120 --> 00:04:33,960
And then poof, you become enlightened

77
00:04:35,160 --> 00:04:37,400
It is not like pulling a rabbit out of a hat

78
00:04:37,880 --> 00:04:38,600
It is learning

79
00:04:39,240 --> 00:04:40,840
Learning more about yourself

80
00:04:40,840 --> 00:04:42,440
And learning more about reality

81
00:04:42,840 --> 00:04:45,400
And that is what you have done in noticing this situation

82
00:04:45,400 --> 00:04:47,400
You really learn something

83
00:04:47,960 --> 00:04:50,360
And you are continuing to learn about something

84
00:04:50,360 --> 00:04:51,880
That you do not want to accept

85
00:04:52,360 --> 00:04:54,040
You are learning the hard truth

86
00:04:54,040 --> 00:04:55,800
That you cannot control the mind

87
00:04:57,320 --> 00:05:01,640
Trying not to feel guilty and trying not to beat yourself up is a very important part

88
00:05:02,600 --> 00:05:06,600
People often experience guilt and anger for not being a perfect meditator

89
00:05:07,640 --> 00:05:10,120
We feel the need to be perfect at everything

90
00:05:10,120 --> 00:05:22,680
So we spend most of our lives beating ourselves up because we are not perfect

