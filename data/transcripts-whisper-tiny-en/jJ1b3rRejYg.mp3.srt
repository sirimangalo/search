1
00:00:00,000 --> 00:00:05,000
Is it possible to study Buddhism without committing to one formal tradition?

2
00:00:05,000 --> 00:00:15,000
I feel that it is a form of attachment to claim one tradition is better than the other.

3
00:00:15,000 --> 00:00:21,000
But I asked the question like this, it might have been a teacher once, and I asked him,

4
00:00:21,000 --> 00:00:30,000
what defilement is this sort of judgment, where you say one thing is better than another.

5
00:00:30,000 --> 00:00:33,000
And he wouldn't accept that it was a defilement.

6
00:00:33,000 --> 00:00:36,000
And maybe he started to think that actually, you know, he's right, he said,

7
00:00:36,000 --> 00:00:39,000
because you can judge things based on wisdom.

8
00:00:39,000 --> 00:00:48,000
So, I'm not trying to deny or answer your claim at all,

9
00:00:48,000 --> 00:00:57,000
but it's important to understand, it's important to understand what we mean when we say one thing is better than another.

10
00:00:57,000 --> 00:01:07,000
Because certain two traditions could be, one tradition could be better than the other.

11
00:01:07,000 --> 00:01:17,000
For example, if some Buddhist tradition claiming to be a Buddhist tradition teaches you to

12
00:01:17,000 --> 00:01:25,000
kill other people and steal and lie and cheat and take drugs and alcohol,

13
00:01:25,000 --> 00:01:28,000
then I would say that's an inferior tradition.

14
00:01:28,000 --> 00:01:31,000
And of course, this is my opinion.

15
00:01:31,000 --> 00:01:35,000
Other people might say it's a superiority of tradition.

16
00:01:35,000 --> 00:01:45,000
But, I mean, the exam, that's just an example to point out that it is certainly,

17
00:01:45,000 --> 00:01:49,000
I think, objectively possible to say that one thing is better than another.

18
00:01:49,000 --> 00:02:04,000
And we have to be clear that Buddhism and what we call postmodernism do not always

19
00:02:04,000 --> 00:02:05,000
jive with each other.

20
00:02:05,000 --> 00:02:11,000
Postmodernism is where you can, you know, anything can be true depending on the context.

21
00:02:11,000 --> 00:02:18,000
And it's sort of become a very important part of global society, modern society,

22
00:02:18,000 --> 00:02:24,000
to the extent that whenever anyone says that this is better than that or this is true,

23
00:02:24,000 --> 00:02:30,000
objectively true, and this is objectively false and so on and so on.

24
00:02:30,000 --> 00:02:39,000
Then they become a subject of ridicule and aversion and so on.

25
00:02:39,000 --> 00:02:45,000
People don't like to hear this, that something could possibly be better than something else.

26
00:02:45,000 --> 00:02:54,000
So that being said, I don't think anyone would say that any good Buddhist teacher would have

27
00:02:54,000 --> 00:03:00,000
strong feelings that one tradition, one Buddhist tradition of the big Buddhist tradition,

28
00:03:00,000 --> 00:03:03,000
the major Buddhist traditions is better than the other.

29
00:03:03,000 --> 00:03:06,000
I was just thinking about this today actually.

30
00:03:06,000 --> 00:03:12,000
It's quite misleading to say this tradition or that tradition in terms of the big traditions.

31
00:03:12,000 --> 00:03:18,000
Like I follow Teravada, you follow Zen, you follow Mahayana and so on.

32
00:03:18,000 --> 00:03:30,000
What is important is to talk about a teaching tradition and these exist in Buddhism

33
00:03:30,000 --> 00:03:37,000
on a different level from the major traditions.

34
00:03:37,000 --> 00:03:41,000
So in the Mahayana tradition or whatever.

35
00:03:41,000 --> 00:03:46,000
I mean it actually goes a lot according to country I think.

36
00:03:46,000 --> 00:03:51,000
Then it does according to school because every school calls their, every country calls their school

37
00:03:51,000 --> 00:03:53,000
a little bit differently.

38
00:03:53,000 --> 00:03:59,000
Now that's not exactly true, but the Tibetans have their group and the Chinese have their group

39
00:03:59,000 --> 00:04:02,000
and the Japanese have their group and their fairly distinct.

40
00:04:02,000 --> 00:04:07,000
Teravada is fairly ubiquitous across various countries.

41
00:04:07,000 --> 00:04:19,000
But what you'll find in any of these groups is a incredible diversity of teachings

42
00:04:19,000 --> 00:04:21,000
from different teachers.

43
00:04:21,000 --> 00:04:26,000
There are Tibetan Buddhist teachers who teach very similar to what I teach.

44
00:04:26,000 --> 00:04:29,000
What would we practice here?

45
00:04:29,000 --> 00:04:33,000
What might teach our teachers?

46
00:04:33,000 --> 00:04:40,000
And there are Teravada Buddhists who teach things very, very much different from what we teach here.

47
00:04:40,000 --> 00:04:47,000
So to that end, I think you have to even go further.

48
00:04:47,000 --> 00:04:51,000
If you really want to progress, you have to go even further than picking a tradition.

49
00:04:51,000 --> 00:04:53,000
Like I'm Mahayana, I'm Teravada.

50
00:04:53,000 --> 00:04:59,000
And you can kind of avoid that actually because it's misleading and it's a cause only for argument really.

51
00:04:59,000 --> 00:05:05,000
Pointless and endless argument because it's meaningless.

52
00:05:05,000 --> 00:05:08,000
I'm Mahayana, I'm Teravada and so on.

53
00:05:08,000 --> 00:05:11,000
What's important is what are the teachings you follow?

54
00:05:11,000 --> 00:05:15,000
Which teacher do you follow and what are their teachings?

55
00:05:15,000 --> 00:05:23,000
I follow the Buddha. What are the teachings of the Buddha?

56
00:05:23,000 --> 00:05:26,000
I follow that.

57
00:05:26,000 --> 00:05:29,000
And I interpret them in this way and this way and this way.

58
00:05:29,000 --> 00:05:32,000
I understand them in this way and this way and this way and this way and this way.

59
00:05:32,000 --> 00:05:41,000
And that's for instance our meditation tradition based on the understanding of the Mahasi-Sayana

60
00:05:41,000 --> 00:05:52,000
and that part of the Buddha's teaching that he felt was important and sufficient to give people understanding and freedom from suffering.

61
00:05:52,000 --> 00:05:58,000
So I think you have to pick something like that if you really want to gain progress.

62
00:05:58,000 --> 00:06:02,000
You don't have to identify yourself.

63
00:06:02,000 --> 00:06:08,000
I think it is a form of attachment to identify yourself as this form of Buddhist or that form of Buddhist.

64
00:06:08,000 --> 00:06:15,000
I think what we do instead is think of what we do as Buddhism.

65
00:06:15,000 --> 00:06:18,000
And I don't think there's any problem with that really.

66
00:06:18,000 --> 00:06:22,000
I don't think that has to be an attachment and I don't think that's dangerous.

67
00:06:22,000 --> 00:06:28,000
If you say I follow the Mahasi-Sayana's teaching and to me that's Buddhism.

68
00:06:28,000 --> 00:06:38,000
Or I follow the teachings of, what to me be more exact, I follow the teachings of the Polycanon.

69
00:06:38,000 --> 00:06:47,000
And with an emphasis on the interpretation, the understanding and the instructions of Mahasi-Sayana and to me that's Buddhism.

70
00:06:47,000 --> 00:06:57,000
So in this way I accept many other practices that exist in the Polycanon that are not taught by the Mahasi-Sayana.

71
00:06:57,000 --> 00:07:00,000
But I just don't practice them.

72
00:07:00,000 --> 00:07:02,000
But it's still part of Buddhism for me.

73
00:07:02,000 --> 00:07:04,000
But that's my Buddhism.

74
00:07:04,000 --> 00:07:08,000
And I would say every tradition.

75
00:07:08,000 --> 00:07:16,000
And by tradition here I mean not the major traditions, but every group like the Goangkha tradition for example.

76
00:07:16,000 --> 00:07:20,000
It's terrifying that they have a different tradition.

77
00:07:20,000 --> 00:07:23,000
They, I would say, do the same thing.

78
00:07:23,000 --> 00:07:28,000
Now if it becomes a identification like I'm a Goangkha meditator.

79
00:07:28,000 --> 00:07:33,000
And Goangkha is better than Mahasi.

80
00:07:33,000 --> 00:07:36,000
Or the other way around I'm a Mahasi meditator.

81
00:07:36,000 --> 00:07:39,000
Mahasi's teaching is better than Goangkha's.

82
00:07:39,000 --> 00:07:44,000
As I said it's not necessarily wrong to think one thing is better than another.

83
00:07:44,000 --> 00:07:49,000
But what's actually going on there obviously is conceit.

84
00:07:49,000 --> 00:07:53,000
Because you put them together and you get this conceit that I'm better than you.

85
00:07:53,000 --> 00:07:55,000
And that's wrong.

86
00:07:55,000 --> 00:08:00,000
Comparing oneself to others is wrong because it's based on the idea of self.

87
00:08:00,000 --> 00:08:04,000
It's based on the idea of an entity.

88
00:08:04,000 --> 00:08:12,000
And so to that extent I suppose you could fall into a similar sort of defilement and delusion.

89
00:08:12,000 --> 00:08:17,000
Thinking that Mahayana is something or Theravada is something.

90
00:08:17,000 --> 00:08:20,000
I am a Theravada Buddhism and identifying yourself.

91
00:08:20,000 --> 00:08:26,000
Then it becomes conceit as well when you identify yourself as being something and thinking it's better.

92
00:08:26,000 --> 00:08:29,000
This is what happens when people identify with countries.

93
00:08:29,000 --> 00:08:33,000
This what happens when people identify themselves with Buddhism.

94
00:08:33,000 --> 00:08:43,000
I'm a Theravada Buddhist but I drink alcohol and kill mosquitoes and lion cheet and so on.

95
00:08:43,000 --> 00:08:54,000
That sort of thing. I'm not that sort of person but some people are claiming themselves to be Theravada Buddhists and don't fall into the teaching.

96
00:08:54,000 --> 00:08:59,000
And that can be a problem because then it becomes this identity.

97
00:08:59,000 --> 00:09:08,000
But I would say always to do with the self and the individual as opposed to making a judgment as to which is better or which is where it's.

98
00:09:08,000 --> 00:09:16,000
Because it's clearly possible that a meditation tradition, one meditation tradition could be better than another.

99
00:09:16,000 --> 00:09:23,000
They may all be exactly the same. They may all be just exactly as beneficial as the others.

100
00:09:23,000 --> 00:09:30,000
But it's probably more likely the case that certain traditions work better for certain people.

101
00:09:30,000 --> 00:09:35,000
And some traditions may work better for a greater number of people.

102
00:09:35,000 --> 00:09:40,000
Some traditions may only work for a very small portion of people.

103
00:09:40,000 --> 00:09:57,000
Some traditions even claim that there are meditation and monastic traditions that don't propagate their teachings because they believe that their teachings are very difficult to practice and only for a select few and so on.

104
00:09:57,000 --> 00:10:07,000
So finally to answer your question or the first part, possible to study Buddhism without committing to one form of tradition.

105
00:10:07,000 --> 00:10:12,000
Possible. Certainly possible to study in practice.

106
00:10:12,000 --> 00:10:24,000
But the problems based sort of on what I've said and the problems are that you become

107
00:10:24,000 --> 00:10:35,000
too broad based. So instead of focusing your efforts on one specific meditation practice that you find suitable for yourself.

108
00:10:35,000 --> 00:10:47,000
It's very easy to get off track. I mean the whole idea behind our understanding of Buddhism of the Buddha's teaching is that we need a teacher.

109
00:10:47,000 --> 00:10:56,000
And so if you're not following a teacher, you say you're following Buddhism. So this would mean that you're trying to follow the Buddha's teaching.

110
00:10:56,000 --> 00:11:07,000
But unless you have a broad grasp of one set of texts that claim to be the Buddha's teaching or the teachings of Buddhism,

111
00:11:07,000 --> 00:11:22,000
then you're quite likely to, first of all, if you don't have that, you're quite likely to create something of your own that's totally outside of the Buddha's teaching because you're not a Buddha yet.

112
00:11:22,000 --> 00:11:30,000
So more than likely to get off track and get on to some dead end teaching.

113
00:11:30,000 --> 00:11:42,000
If you have a knowledge of many different sets of the Buddha's teaching and try to mix and match, then you end up with Frankenstein.

114
00:11:42,000 --> 00:11:55,000
You end up with body parts everywhere. You don't end up with a solid hole because the traditions have formed because they work.

115
00:11:55,000 --> 00:12:05,000
And they have a gel and they have created some, they've become institutional because that institution has a benefit or has leads to a certain result.

116
00:12:05,000 --> 00:12:20,000
And finally, if you, even if you pick one tradition and read only the texts of that tradition, like say you read the polycan and you follow, say I'm following the Buddha, but I'm not going to follow the

117
00:12:20,000 --> 00:12:41,000
commentorial interpretation or interpretations of this group or that group, then you still have a bit of a problem because you're putting yourself in the position of creating a new interpretation of the Buddha's teaching or your own idea of what it is that the Buddha taught.

118
00:12:41,000 --> 00:13:03,000
And if you feel up to that task and while you have a lot of work to do and you have to do a lot of studying, I mean, considering the context of the Buddha's teaching, you read the Buddha's teaching and you're living in a country, maybe you're in, maybe you're from India, but even if you're from India, you're living in a time, far, far removed from the time of the Buddha.

119
00:13:03,000 --> 00:13:12,000
And here you have to interpret this and understand where the Buddha is coming from when he's giving this talk to people in India 2,500 years ago.

120
00:13:12,000 --> 00:13:20,000
So without some intermediary to explain it to you, obviously you can, many people prefer this.

121
00:13:20,000 --> 00:13:30,000
They don't like institutions, they think institutions are by the very nature wrong and that one should go directly to the source and only to the source.

122
00:13:30,000 --> 00:13:43,000
And then they try to interpret the Buddha's teachings directly and try to understand them by themselves, which certainly it's possible, but for the reasons mentioned I think it's quite a bit more difficult.

123
00:13:43,000 --> 00:14:07,000
And with that done.

