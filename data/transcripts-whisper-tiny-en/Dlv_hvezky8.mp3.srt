1
00:00:00,000 --> 00:00:05,760
Good evening. Good evening everyone. I don't know when exactly it starts recording.

2
00:00:07,840 --> 00:00:13,760
Broadcasting live January 6th, 2016.

3
00:00:18,320 --> 00:00:24,960
Back on with our weekly meditations at McMaster and we're going to keep trying with

4
00:00:24,960 --> 00:00:30,960
three times a week actually. And next week we'll have our five-minute

5
00:00:30,960 --> 00:00:34,240
meditation lesson. I've got someone helping me out with that, hopefully.

6
00:00:38,720 --> 00:00:42,480
I have to have to test it. I haven't done a five-minute meditation. I've never taught

7
00:00:42,480 --> 00:00:48,000
meditation in five minutes, not formally. So I have to figure out what's my five

8
00:00:48,000 --> 00:00:55,200
minute. What's my elevator pitch? And that's what they call it.

9
00:00:55,200 --> 00:01:01,600
You can get a set on this monthly. I'm going to set up mats and try I didn't hear you.

10
00:01:01,600 --> 00:01:07,280
You can practice with us. I'm gonna be able to five-minute.

11
00:01:07,280 --> 00:01:12,960
Good idea.

12
00:01:17,920 --> 00:01:25,280
Okay, so the basis of meditation is about objectivity. I don't know if I want to get too

13
00:01:25,280 --> 00:01:33,280
full of philosophical because they shut up. But it's we're trying to see things as they are.

14
00:01:33,280 --> 00:01:38,880
We're trying to see things objectively without reacting to them.

15
00:01:38,880 --> 00:01:46,800
So what we're going to do is we're going to remind ourselves in general this is this.

16
00:01:47,760 --> 00:01:54,320
So pain is pain. Thoughts are thoughts. Anger is anger. Depression is depression.

17
00:01:54,320 --> 00:02:00,560
And we're not going to react to anything because you see when you react to things

18
00:02:00,560 --> 00:02:07,360
this multiplies them and continues them and they snowball out of control.

19
00:02:08,080 --> 00:02:11,280
So when you experience pain, if you could just experience that that's pain,

20
00:02:12,000 --> 00:02:18,640
it doesn't end up bothering you. If you when you feel angry and angry, you remind yourself

21
00:02:18,640 --> 00:02:25,200
that you're angry and it doesn't turn into a fight or a problem. It doesn't make you want to do bad

22
00:02:25,200 --> 00:02:32,080
things or say bad things. If you have a thought or someone shouting at you and you just

23
00:02:32,080 --> 00:02:39,360
see it as a thought or a sound, just as the experience of it, then it won't hurt you. How many

24
00:02:39,360 --> 00:02:46,160
minutes is that? Oh, I'm sorry. I wasn't counting. Well, the last time you had a counter on

25
00:02:46,160 --> 00:02:51,360
your side of the hangout, I'm sorry. That was about a minute, I think, right? A couple minutes.

26
00:02:51,360 --> 00:02:59,280
Okay, so then, so here's what we're going to do. Close your eyes and let me be able to tell

27
00:02:59,280 --> 00:03:04,400
them to close your eyes. First, I have to say how we do it. So how we do this is just simply

28
00:03:04,400 --> 00:03:10,560
reminding ourselves, you pick a word that reminds you what's going on. Like for instance,

29
00:03:10,560 --> 00:03:16,640
if you feel pain, you remind yourself just with one word, pain and that becomes your meditation,

30
00:03:16,640 --> 00:03:22,960
you'd say to yourself, pain, pain. I don't think I'm having them close to their eyes yet. I think

31
00:03:22,960 --> 00:03:29,440
this is still eyes open. I made a mistake. There's a pain pain. If you have a thought, you just say

32
00:03:29,440 --> 00:03:33,600
thinking, thinking, if you hear a sound like all there's noise of these people, because we're

33
00:03:33,600 --> 00:03:38,400
right in the middle of the hallway, really. It's kind of funny. Is they hearing hearing?

34
00:03:40,720 --> 00:03:45,520
And so this is the skill that we're trying to cultivate. So the technique that I'm going to show

35
00:03:45,520 --> 00:03:51,520
you to do this is to train you to do this. So we pick something that's neutral. We'll pick our breath.

36
00:03:51,520 --> 00:03:55,600
Focus on the breath. When your breath comes into your body, your stomach will rise, when the breath

37
00:03:55,600 --> 00:03:59,520
goes out of the body or stomach will fall. If you don't feel it, you can put your hand there.

38
00:04:00,560 --> 00:04:09,840
But you just say to yourself, rise, say fall. Close your eyes into it. You don't say it out loud.

39
00:04:09,840 --> 00:04:21,360
You just remind yourself in your mind. And that trains you to do that. Once you can do that,

40
00:04:22,640 --> 00:04:30,880
then you begin to extrapolate it and apply it in your rest of your life. So let's try that now.

41
00:04:33,520 --> 00:04:38,080
And then I'll have them do that. After a minute or so, probably have one minute left,

42
00:04:38,080 --> 00:04:47,680
then I'll say, okay, and then now I need to apply the same concept of seeing your stomach move clearly.

43
00:04:48,160 --> 00:04:52,880
Apply it to things that really matter. Like when you feel pain, remind yourself pain,

44
00:04:52,880 --> 00:04:58,400
when you have thoughts, remind yourself thought thinking. You have emotions, you might yourself

45
00:04:58,400 --> 00:05:12,160
angry, frustrated, bored, sad, worried, when you want something wanting, when you like something like

46
00:05:13,520 --> 00:05:17,440
and see the thing is we don't have a time limit. It's whenever they want to get up. If they want

47
00:05:17,440 --> 00:05:22,880
to sit there and talk to me for a while, well, I think that's fine. But I'm not sure how many

48
00:05:22,880 --> 00:05:29,680
mats to have. I think we'll have to experiment because I think it's going to be too noisy for people

49
00:05:29,680 --> 00:05:35,760
to be too far away. So it might be like me and two other people at a time. And more than that,

50
00:05:35,760 --> 00:05:42,000
and they might get too hard for them to hear. And I think we're going to set up a nice carpet,

51
00:05:43,360 --> 00:05:50,240
rug or something. And real sitting mats, and then tell people to take their shoes off.

52
00:05:50,240 --> 00:05:56,240
We're going to do that. I don't think there's any way around getting to take their shoes or boots off.

53
00:05:57,680 --> 00:06:01,360
Or maybe the yoga mats, if a carpet is hard to come by.

54
00:06:02,480 --> 00:06:08,880
Right. We can just put out a few yoga mats and just leave it at that, right?

55
00:06:09,520 --> 00:06:12,720
That would be some of four yoga mats together or something.

56
00:06:14,960 --> 00:06:17,280
The other thing is how to get all this to the university.

57
00:06:17,280 --> 00:06:24,240
Is that the sign? I guess it's not really carryable at that point.

58
00:06:25,520 --> 00:06:29,760
See, we're supposed to have a locker. If the tomorrow I'm going to find out about our locker,

59
00:06:29,760 --> 00:06:33,200
why we don't have a locker for our club? Because every club should have a locker.

60
00:06:35,040 --> 00:06:38,160
But we don't. Yet, that I know.

61
00:06:40,560 --> 00:06:45,120
Anyway, that's future plans here. So that seems doable, no?

62
00:06:45,120 --> 00:06:50,560
Yes, and on your timing, after I realized that I wasn't timing it for you, that was two minutes.

63
00:06:51,440 --> 00:06:53,840
That was only two minutes, so. What was two minutes?

64
00:06:54,480 --> 00:06:56,400
The, from the point that I realized that

65
00:06:58,160 --> 00:07:02,480
from the point that I hadn't been timing it from there to the end, it was two minutes.

66
00:07:02,480 --> 00:07:07,040
So I think you were under five minutes altogether, because the whole second half was only two minutes.

67
00:07:07,840 --> 00:07:10,000
Good. Did I leave anything out?

68
00:07:10,000 --> 00:07:16,320
No. Good people will have questions probably, but I mean, that is a nice assignment.

69
00:07:16,320 --> 00:07:20,560
So it's misleading. Sign is misleading.

70
00:07:22,080 --> 00:07:24,880
False advertising. It's only a two minute meditation.

71
00:07:26,400 --> 00:07:28,960
No, that gives them more time to actually meditate.

72
00:07:29,600 --> 00:07:31,840
Yep. That's part of the lesson.

73
00:07:31,840 --> 00:07:38,800
That's going to be great. Final helps. I've got to get someone to take pictures.

74
00:07:39,840 --> 00:07:42,480
Maybe we'll make a video of it. Put it on YouTube.

75
00:07:49,760 --> 00:07:55,120
I should go on the silhouette, actually. I should talk to the silhouette of McMaster University,

76
00:07:55,120 --> 00:07:59,840
newspaper, and the radio as well. Put it on a press release. That's what I'll do.

77
00:07:59,840 --> 00:08:04,800
We have to put out a press release. Did this for the meditation? Did this for the piecewalk?

78
00:08:06,480 --> 00:08:08,080
And they interviewed me on radio.

79
00:08:17,120 --> 00:08:23,680
So questions, huh? We have questions. This one wasn't exactly directed to you,

80
00:08:23,680 --> 00:08:29,440
but it's probably a good one to answer anyway. What I don't know what I don't know yet is,

81
00:08:29,440 --> 00:08:31,680
why do you call him Bandai instead of you to demo?

82
00:08:32,800 --> 00:08:34,640
Yeah, but don't I answer this one like every week?

83
00:08:35,840 --> 00:08:37,200
No, it just seems that way.

84
00:08:40,240 --> 00:08:41,440
I haven't answered it before.

85
00:08:43,920 --> 00:08:45,200
There's new people all the time.

86
00:08:45,840 --> 00:08:46,880
Sorry, right?

87
00:08:46,880 --> 00:08:48,160
There are new people all the time.

88
00:08:49,280 --> 00:08:51,360
New people joining this group all the time.

89
00:08:51,360 --> 00:08:54,240
Some of those questions you think people can answer amongst each other.

90
00:08:56,000 --> 00:08:57,280
Someone else can answer.

91
00:08:57,280 --> 00:08:59,280
Okay,

92
00:08:59,280 --> 00:09:03,040
Bandai, I was very glad to hear that you get to go see your teacher again.

93
00:09:03,040 --> 00:09:07,760
Would it be possible for us to come together as a community and put something together for you

94
00:09:07,760 --> 00:09:12,480
to offer to Ajahn Tong, and if so, what would be appropriate? Thank you, Bandai.

95
00:09:13,200 --> 00:09:16,560
That'd be great. I'm not really thinking that I'm going to go.

96
00:09:17,760 --> 00:09:22,800
It was, you know, it's been bugging me that I haven't talked to him about this new monastery,

97
00:09:22,800 --> 00:09:28,320
but it's not really worth flying across the world to do it.

98
00:09:31,840 --> 00:09:34,880
It's not, you know, it's not like I've got a free ticket to go, so

99
00:09:36,400 --> 00:09:40,480
it would have to be paid for, which is a considerable expense.

100
00:09:44,320 --> 00:09:49,120
So yeah, absolutely. If I go, let's for sure, let's keep that in mind,

101
00:09:49,120 --> 00:09:55,440
but it probably won't be. I don't know. We'll see because there's talks that they might support

102
00:09:55,440 --> 00:10:03,600
some or some of the ticket anyway. I'll see if it's really, there's a legitimate conference

103
00:10:03,600 --> 00:10:10,800
going on as well. But the only real reason to go would be to talk to my teacher.

104
00:10:14,080 --> 00:10:17,680
And he already knows about the place. I just haven't told him myself, I haven't talked to him,

105
00:10:17,680 --> 00:10:21,920
I haven't gotten his instructions and his blessing and all that.

106
00:10:37,520 --> 00:10:39,600
Did the Buddha know why the universe exists?

107
00:10:39,600 --> 00:10:52,080
I think the word I think why is problematic, it's a question that doesn't, it's like asking an

108
00:10:52,080 --> 00:10:58,640
innocent man why why why he beats his wife. It's like asking why does a tree exist?

109
00:11:00,080 --> 00:11:06,000
What is the purpose of a tree? It's a ridiculous question. It's a sort of a

110
00:11:06,000 --> 00:11:17,120
theistic question, really, because the only answer is God. It's a very human thing to want to

111
00:11:17,120 --> 00:11:27,600
find a purpose for things. We differentiated ourselves from ordinary animals by our ability to

112
00:11:27,600 --> 00:11:32,720
cultivate tools. So the idea of things having a purpose and if things don't have a purpose,

113
00:11:32,720 --> 00:11:39,440
we destroy them or we get rid of them. We remove them. We've had a very utilitarian

114
00:11:39,440 --> 00:11:47,520
event to us in the sense. So there's no reason to think that anything has a purpose.

115
00:11:53,600 --> 00:11:58,160
I mean, I guess if the question were instead, how is it that the universe exists?

116
00:11:58,160 --> 00:12:03,040
I don't know whether the Buddha knew or not.

117
00:12:06,080 --> 00:12:08,400
But again, I think it's probably a bad question.

118
00:12:13,200 --> 00:12:21,280
It's incomprehensible to us that there shouldn't be an answer to that, but I think that's our problem.

119
00:12:21,280 --> 00:12:28,000
I think it's just the way we think.

120
00:12:29,120 --> 00:12:34,800
I have anxiety issues and once I've become too anxious about something for a few days continuously,

121
00:12:34,800 --> 00:12:38,800
it will get harder for me to meditate and eventually I stop meditating until the thing I'm

122
00:12:38,800 --> 00:12:44,160
worried about is subsided. How can I make sure I don't stop meditating even when I'm anxious about

123
00:12:44,160 --> 00:12:49,600
something? I should meditate on the anxiety. Look up some of the videos I've done on anxiety,

124
00:12:49,600 --> 00:12:56,000
particularly because it's an interesting one. Anxiety, it's fairly easy to see that a lot of it's

125
00:12:56,000 --> 00:13:04,240
just physical and physical aspects of anxiety aren't anxiety. If you go to video.serit is a video,

126
00:13:04,240 --> 00:13:09,360
what is the website? Video.serimonglo.org, we got a whole bunch of my videos

127
00:13:09,360 --> 00:13:24,240
categorized and I think anxiety is under mental problems. It doesn't think it could just lie down

128
00:13:24,240 --> 00:13:29,760
to lying meditation, but definitely meditate on all the aspects of what you call anxiety

129
00:13:29,760 --> 00:13:36,560
because much of it is physical. The anxiety only lasts a moment. If you're patient and keep

130
00:13:36,560 --> 00:13:41,840
noting anxiety, then you do very well, learning a lot, you're studying the anxiety,

131
00:13:42,400 --> 00:13:47,120
and you're meditating. So it doesn't actually interrupt your meditations. It becomes a part of your

132
00:13:47,120 --> 00:13:56,960
meditations. The thing that you're anxious about, note that thing.

133
00:13:56,960 --> 00:14:07,840
How do I balance my mind between lay terms and higher states of awareness through meditation?

134
00:14:08,480 --> 00:14:13,440
I look around me and see people unaware of life itself and they seem to be in a permanent state

135
00:14:13,440 --> 00:14:19,360
of sleep. I'm finding this very difficult. People seem to be more focused on self gain rather than

136
00:14:19,360 --> 00:14:27,920
global gain. Well, there's no balancing them. They're opposites. The dhamma goes in the opposite

137
00:14:27,920 --> 00:14:39,840
direction of material gain. So the conflict that you face, choose in the end one or the other.

138
00:14:39,840 --> 00:14:47,280
But I mean that's in the end if you decide that you want to become a monk or something.

139
00:14:55,680 --> 00:15:00,160
What exactly does reflection of an action mean in the court of today? Thanks.

140
00:15:00,160 --> 00:15:11,040
We have to read the whole truth. The flexion here means before you do something,

141
00:15:14,880 --> 00:15:20,160
before you do something, you should reflect with this to my detriment or to the detriment of others.

142
00:15:21,120 --> 00:15:26,000
Will this be a harmful thing that I'm about to do? So then you shouldn't do it.

143
00:15:26,000 --> 00:15:31,440
If not, then go ahead and do it. The same well you were doing it and the same after you have

144
00:15:31,440 --> 00:15:39,600
done it was that thing that I did. Harmful or beneficial. Is harmful? Then you should not do it

145
00:15:39,600 --> 00:15:46,560
again. While you're doing it, if you realize it's harmful, you should stop. So reflect in that way.

146
00:15:46,560 --> 00:16:00,320
This is a talk to his son who is a young boy and it's very fatherly. It's a unique

147
00:16:00,320 --> 00:16:03,600
disc dialogue that he's to have father in his own.

148
00:16:03,600 --> 00:16:16,400
If you are mindful of what is... I'm sorry, if you are mindful of what it is, neutral and with

149
00:16:16,400 --> 00:16:22,240
no desire, if you accept things for what they are, is there any reason to help others? Is there

150
00:16:22,240 --> 00:16:31,600
any reason to change things? Not really. Except that it's the right thing to do. It's

151
00:16:31,600 --> 00:16:38,720
natural thing to do. We're not stones. We don't just sit around doing nothing. So part of your

152
00:16:38,720 --> 00:16:44,320
equanimity is what appears to be compassion and I think you could even argue that it is compassion

153
00:16:44,320 --> 00:16:51,680
but it's just a word. I mean you act in the right way. It might in being appear to be very

154
00:16:51,680 --> 00:16:55,760
compassionate but some people think well they're not really compassionate because they don't

155
00:16:55,760 --> 00:17:02,800
really care. They don't really care but the natural thing to do is to help others.

156
00:17:14,080 --> 00:17:19,040
Just as a Christian praise the Lord's Prayer on a daily basis, do you have any Buddhist texts

157
00:17:19,040 --> 00:17:26,640
which you read reflect on daily, aside from your YouTube Dhamapatavidh videos?

158
00:17:28,080 --> 00:17:36,160
Yeah, there's... We reflect on the Buddha, the Dhamman, the Sangha. That's the most common.

159
00:17:37,680 --> 00:17:41,120
We have reflections of the qualities of the Buddha, the qualities of the Buddha, the qualities of the

160
00:17:41,120 --> 00:17:52,000
Buddha, the qualities of the Sangha. And then there are other Dhammas that are to be reflected

161
00:17:52,000 --> 00:17:59,040
upon daily. There's a list of five for the people. There's a list of ten for months called the

162
00:17:59,040 --> 00:18:14,800
binha pachawakana. So what are they? They are. Five will get old. I will get sick. I will die.

163
00:18:16,560 --> 00:18:21,200
I will lose everything I have. I can't quite... Oh and I'm... I'm here to my come.

164
00:18:21,200 --> 00:18:29,520
Look at the chanting guy. There's this pheromata chanting.

165
00:18:41,680 --> 00:18:47,200
I believe a person who wishes to ordain must have the approval of parents. Is that necessary

166
00:18:47,200 --> 00:18:55,600
for a grown adult? Would if the parents don't agree? Yes, technically that's necessary

167
00:18:55,600 --> 00:19:01,520
if a grown adult. The parents don't agree. Then you starve yourself until they give in.

168
00:19:05,360 --> 00:19:08,800
I'm threatened to throw yourself from a clip or something like that.

169
00:19:08,800 --> 00:19:15,600
Maybe that's a bit of a bit of a pheromata. But there's something about that.

170
00:19:17,040 --> 00:19:19,600
Monks who actually did that. He threatened to kill themselves.

171
00:19:21,280 --> 00:19:23,120
But they're starving yourself. They work.

172
00:19:23,120 --> 00:19:32,400
I think we can't become a monk. Sorry.

173
00:19:32,400 --> 00:19:54,800
And we could go and stand in front door, sit on their porch until they give in.

174
00:19:54,800 --> 00:20:01,200
Does that happen a lot? Because people ask that question a lot.

175
00:20:02,400 --> 00:20:07,200
That parents don't agree. If your parents are evangelists, Christians,

176
00:20:08,400 --> 00:20:09,760
that might be a little tough.

177
00:20:13,840 --> 00:20:16,480
Yeah, I know that I mean many, many people who are not Buddhist,

178
00:20:16,480 --> 00:20:29,280
but for been their children, even when they get older, my grandmother would never give

179
00:20:30,320 --> 00:20:37,520
consent. Although sometimes it's interesting, they'll just say do whatever you want.

180
00:20:37,520 --> 00:20:49,200
Is there much difference between a monk from Thailand and the teaching they learn

181
00:20:50,080 --> 00:20:57,040
from a monk from Nepal? Well, every monk teaches differently. But there's also different kinds of

182
00:20:57,040 --> 00:21:10,080
Buddhism. If the parent is deceased, I assume their approval is not needed.

183
00:21:11,520 --> 00:21:17,120
But what if that was just a comment? But what if you knew the parents did not approve and then they

184
00:21:17,120 --> 00:21:23,280
passed away and you knew that that was not their way? As long as they're dead. As long as they're dead,

185
00:21:23,280 --> 00:21:27,280
it's okay even if they never would have gone for it if they were alive. No, I mean, it's not about

186
00:21:27,280 --> 00:21:34,640
disrespecting them. It's about making them feel bad. Okay. Dead, they're gone. Okay. In fact, it's kind

187
00:21:34,640 --> 00:21:42,080
of an interesting rule because the Buddha only gave the rule out of respect for his father

188
00:21:43,520 --> 00:21:50,960
then passed away. But he instated it as a rule. And a lot of the times the reason the Buddha

189
00:21:50,960 --> 00:21:58,400
instated it isn't just the only reason he instated it, like impetus. So it's not to say that

190
00:21:58,400 --> 00:21:59,760
he doesn't agree with his father.

191
00:22:06,640 --> 00:22:10,560
I mean, I think you could argue that there are extenuating circumstances, but

192
00:22:12,240 --> 00:22:17,760
sorry, put it in. Sorry, put the ordained. It's nephew. Without his nephew's parents consent.

193
00:22:17,760 --> 00:22:20,720
They said they have wrong with you. I'm his father.

194
00:22:35,200 --> 00:22:40,800
The suggestion, not a question, but a suggestion to find a way to remind on YouTube the link to

195
00:22:40,800 --> 00:22:47,920
this page. I think you have, um, like a template set up on for YouTube videos, don't you,

196
00:22:47,920 --> 00:22:54,640
don't you, Dante? Yeah. I mean, really? I'm going to add it to every video.

197
00:22:55,360 --> 00:23:02,320
No, but isn't there a template that you can use for your videos? I don't know. Maybe.

198
00:23:04,080 --> 00:23:08,080
I mean, I just copy paste copy paste. I do it for all the download content videos, but

199
00:23:08,080 --> 00:23:11,360
yeah, I'm going to do it for every day of the demo.

200
00:23:12,640 --> 00:23:17,600
No, no, but I thought there because I thought with your older videos, they had so much information

201
00:23:17,600 --> 00:23:22,880
on it. I know, well, I don't think that you probably were typing that in every time.

202
00:23:22,880 --> 00:23:27,760
I just assumed there was some sort of a template, but maybe you were just copying pasting from

203
00:23:28,640 --> 00:23:35,600
Well, I have it saved in the Firefox extension. It's not like it does it for me itself. I have to

204
00:23:35,600 --> 00:23:44,560
copy it. I see. Again, I can try to put that in as a comment. So people know, because people

205
00:23:44,560 --> 00:23:52,080
still do ask questions on the YouTube, YouTube comments. So there you go. Someone just say,

206
00:23:52,080 --> 00:23:57,280
hey, if you want to ask questions, go here. Yeah, I do put that in when I notice it.

207
00:23:57,280 --> 00:24:05,280
But it's okay. I don't want this place over run. Small is okay.

208
00:24:07,840 --> 00:24:14,240
We'll grow up too soon as it is.

209
00:24:14,240 --> 00:24:26,480
How are classes so far, Monday?

210
00:24:29,280 --> 00:24:36,000
Okay. Yeah, the Buddhism in East Asia is, it looks really good. I just have a class today.

211
00:24:36,000 --> 00:24:43,680
The professor is specialty as Indian Buddhism, which is not because that's my interest in it.

212
00:24:46,560 --> 00:24:57,440
Indian monasticism is the thesis is doing this PhD. So it feels better about it than before,

213
00:24:57,440 --> 00:25:03,600
because I was really keen on East Asian Buddhism, but it sounds like someone who will be interesting

214
00:25:03,600 --> 00:25:13,600
to talk to. And he's a really gregarious sort of person. So certainly we'll keep it interesting.

215
00:25:13,600 --> 00:25:21,440
Some two people can't teach. Nice people, but they're too timid or they're too anxious or too

216
00:25:22,560 --> 00:25:28,080
anxious to please. Sometimes they thought they would make these, the professor will make

217
00:25:28,080 --> 00:25:34,800
these silly jokes. They don't know how to be funny and they try to be funny. And it's just

218
00:25:34,800 --> 00:25:38,240
teach. No, it's cool. We don't need to be entertained.

219
00:25:42,080 --> 00:25:46,640
Some people do a sports and it's intimidating. Get up there and all these students with glass

220
00:25:46,640 --> 00:25:53,120
the eyes and slouching or right down there on their phones. It's really intimidating and

221
00:25:53,120 --> 00:25:59,200
knowing when you're in front of a room and everyone looks bored about what you're saying. So if

222
00:25:59,200 --> 00:26:04,400
you're not hard, if you're not strong, you really just have to ignore your audience.

223
00:26:06,800 --> 00:26:07,840
Are they large classes?

224
00:26:10,240 --> 00:26:16,000
Yeah, large. This one is a third year class, but it's a cross list with arts and science,

225
00:26:16,000 --> 00:26:20,480
which is neat because arts and science, which was what I was in years ago, and it's this really

226
00:26:20,480 --> 00:26:28,240
special program at McMaster for teenagers. We're just looking to learn. So we've got a bunch of

227
00:26:28,240 --> 00:26:30,240
upper-year art side people. It's awesome.

228
00:26:35,680 --> 00:26:40,880
And tomorrow I think I'm switching into a third and another third year piece that is class,

229
00:26:40,880 --> 00:26:47,120
because today I had a class on piece and popular culture, but I'm afraid it sounds like it's

230
00:26:47,120 --> 00:26:52,720
going to be a lot of music and movies. No, that's not really appropriate.

231
00:26:56,080 --> 00:26:59,520
Trying to think, could I, could I justify that?

232
00:27:04,560 --> 00:27:05,920
Awesome. It's a new country.

233
00:27:05,920 --> 00:27:15,920
Yeah. It's also newspapers. So I was thinking, well, maybe we're looking at newspapers and newspaper articles.

234
00:27:17,120 --> 00:27:21,200
But I don't think so. I think piece studies is going to turn out to be not as interesting as

235
00:27:21,200 --> 00:27:28,400
I thought it was. It's very much about external piece, you know, not so much about inner piece.

236
00:27:28,400 --> 00:27:35,040
Like people that go to protesting, get really angry, protesting about piece.

237
00:27:36,400 --> 00:27:42,160
But to, and also like, brokering piece, piece talks, there's all that kind of stuff.

238
00:27:44,160 --> 00:27:48,640
Like, piece corpse, whatever that is, piece court, piece court, yeah.

239
00:27:49,760 --> 00:27:54,240
Like, in any way, we have the, we used to have the blueberries, piece, piece, piece,

240
00:27:54,240 --> 00:27:58,560
cut, these keepers.

241
00:28:03,520 --> 00:28:08,400
I'm, I'm, I'm thinking religious that it's actually more interesting, but it wouldn't,

242
00:28:11,040 --> 00:28:13,360
kind of, you know, I'm religious that it's,

243
00:28:13,360 --> 00:28:15,360
you know,

244
00:28:22,960 --> 00:28:23,920
said all our questions.

245
00:28:26,400 --> 00:28:29,360
Just one more. Did you hear back from Ted X, Monty?

246
00:28:30,720 --> 00:28:38,080
No, they. Let me see. They just closed applications on January 4th. So

247
00:28:38,080 --> 00:28:45,200
I'm just a couple days. So I think it's still early. I'm not holding my breath. I mean,

248
00:28:45,760 --> 00:28:52,880
not that's a big deal. I'm not really hopeful because I don't probably fit the mold of a student

249
00:28:52,880 --> 00:28:58,400
and it's supposed to be based. So I don't know. Maybe they like it. Maybe they don't.

250
00:28:58,400 --> 00:29:02,960
The other thing I was supposed to be about change and I didn't talk about change in my application.

251
00:29:02,960 --> 00:29:09,040
Really sad afterwards. We got that I should try to tie it into their paradox.

252
00:29:14,800 --> 00:29:15,360
Don't be neat.

253
00:29:17,760 --> 00:29:19,360
A lot of work to do as it is.

254
00:29:20,160 --> 00:29:24,880
Yeah, it certainly is about change. I mean, introducing technology to ancient teachings is

255
00:29:24,880 --> 00:29:30,240
that's all about change. But yeah, it's right. I think it's close enough that they can

256
00:29:30,240 --> 00:29:33,200
imply it, but they probably should at least use the word change.

257
00:29:38,480 --> 00:29:43,040
Okay, that's all done. Good night. Thanks for joining us.

258
00:29:43,040 --> 00:30:03,760
Thank you. Thank you, Robin. Good name.

