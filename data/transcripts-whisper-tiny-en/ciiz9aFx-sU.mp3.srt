1
00:00:00,000 --> 00:00:08,140
Is there anything you can say about the experience of wisdom maybe if it is different from other things like ignorance for example it seems so

2
00:00:08,940 --> 00:00:14,020
Vast and deep that only the present moment is the important. Well, that's wisdom

3
00:00:15,180 --> 00:00:17,180
That's that's wisdom

4
00:00:17,420 --> 00:00:19,980
Seeing that there is so much out there

5
00:00:19,980 --> 00:00:26,960
That there is that is so that is so it is so vast and deep which is really the meaning of samsara

6
00:00:29,720 --> 00:00:32,600
That only the present moment is is important. That's wisdom

7
00:00:33,360 --> 00:00:37,700
That's what comes from meditation practice, which I understand you're engaging in

8
00:00:41,700 --> 00:00:44,300
Through the practice of meditation you will give up

9
00:00:45,340 --> 00:00:47,340
your

10
00:00:47,340 --> 00:00:54,140
Extra your external activities activities that have nothing to do with the present moment. We come to see them as useless as

11
00:00:55,540 --> 00:00:57,540
unsatisfying and

12
00:00:57,900 --> 00:01:02,060
You become bored and disenchanted with him and you give them up. That's wisdom

13
00:01:03,340 --> 00:01:07,420
The experience of wisdom is the experience of letting go. It's not an intellectual thing

14
00:01:08,540 --> 00:01:10,540
some

15
00:01:10,540 --> 00:01:13,760
You might say that disenchantment is really the essence of wisdom

16
00:01:13,760 --> 00:01:19,080
It's this is giving up of our attachments to things, but it can take the form of

17
00:01:19,680 --> 00:01:24,720
Seeing three things, you know as we talk about impermanence suffering in non-self

18
00:01:25,760 --> 00:01:30,800
The problem with talking about those things is it comes to be an intellectual thing when you when you mention

19
00:01:31,120 --> 00:01:34,080
impermanence suffering in non-self to people who haven't experienced them

20
00:01:34,240 --> 00:01:37,440
It becomes intellectual the way a teacher would use

21
00:01:38,000 --> 00:01:41,680
impermanence suffering in non-self is for people who have already experienced them

22
00:01:41,680 --> 00:01:46,960
Because they're the three things that we reject about reality. We cannot accept. So you explained them

23
00:01:47,600 --> 00:01:49,600
Don't you see this is reality?

24
00:01:49,600 --> 00:01:53,600
What you're seeing is impermanence and then they say well, yeah, it's true and

25
00:01:54,880 --> 00:01:58,320
Then they because based on seeing these three things you let go

26
00:01:58,800 --> 00:02:01,680
when you see everything constantly changing and

27
00:02:02,240 --> 00:02:05,280
Never static never stopping then you give them up

28
00:02:05,280 --> 00:02:12,160
When you see things as unsatisfying is without any goal without any final

29
00:02:14,320 --> 00:02:20,720
Happiness then you give them up and you see them as uncontrollable as going on and on and incessant

30
00:02:21,520 --> 00:02:23,120
Then you give them up

31
00:02:23,120 --> 00:02:28,240
So that's wisdom. It's really quite simple and it's not something intellectual or something that you have to

32
00:02:31,040 --> 00:02:32,720
Look for

33
00:02:32,720 --> 00:02:38,800
And then you have to evoke it's something that comes naturally from seeing things as they are

