How does one maintain peaceful relationships with people for whom
un-buddhist attitudes, anger, conflict, etc. is the norm and not about to
change any time soon. Work colleagues, family, etc.
I would say to, I can't find the word now, to separate, to seclude yourself from this situation.
If you can't seclude yourself physically, then you have to do it mentally.
Sometimes it's not possible because you are working and you have the family and you have the colleagues.
So you can't get away from it. Then, when you can't get away physically,
get away mentally, try not to get involved too much. Try to note what they bring into the situation,
try to see their defilements as they are. And either you can try to just note it,
to be mindful on it, and then be mindful on your reaction. Try that your reaction is not going into the same direction,
but when there is anger coming towards you that you ward it off and stay peaceful and stay in your mental seclusion.
And the other thing is when the anger, for example, overwhelmed you, you got into a conflict with others,
then step out either physically or mentally and try to develop some whole, some mind state.
Take a deep breath and try to calm down for a second, noting your breath and know that the breath,
no matter what happens, is everything there is always there and is always the next thing that happens as long as you are alive.
So this is one thing you can kind of rely on and this is what calms me down pretty much when I have fear or when I am in a situation of anger.
I know there will always as long as I am alive be the next breath and I can just observe it.
So I can recommend to do that and maybe it has the same effect for you than it has for me, so that brings you a little bit of mental seclusion.
And the rest, but it will explain.
Yeah, I just wanted to back up the whole breath thing. The Buddha was very clear about how useful the breath is in this way. The Buddha said, breath has five benefits.
Santoja, it is peaceful. Panito, it is subtle or refined. It is refined.
I say it is secluded. It is not mixed with anything. It is pure in itself. It is just the breath.
Sukhor, Javi Harlow, at least one to dwell in happiness.
And then Upa Nupaneak was the papa gaiyak was the ladha matanaso and pranapati would move as a maintain. Whatever unholesome things have arisen.
It in the here and now does away with them or gets rid of them. It allows you to see them clearly.
To see them like specks on a pure cloth. So the breath is something that actually should be recommended for sure. I think that is really good.
What I wanted to say was dealing with something a little specific in the question that is kind of implied there.
Any expectation that we might not realize is that even in trying to have mindful relationships with people, it is still an expectation.
The expectation that we are going to have a peaceful calm relationship with these people. I have had relationships where I felt I was reasonably mindful and the other person was freaking out.
You are crazy and shouting at me. I am not trying to say that I was perfect in this situation. In my mind it really wasn't a problem that this person was freaking out.
It was a natural result of their level of stress and mental agitation. There was no good that I could do by trying to placate them.
In fact, it seemed to me the best thing for them to be able to realize and it was in fact because I was calm and this person was yelling at me and not this person, many people. I have had many relationships like this.
I can witness, as anyone ever told you they are going to hit you in their head, they are looking at them and walked away.
He really walked calmly and that made the whole situation so absurd because there was that angry monk and he had a completely different timing and a completely different timing.
You really changed the people by just being mindful and that doesn't mean that they are going to be harmonious with you. They might go crazy.
You find that in work as well, I think. You are surrounded by people who are unmindful. They will have a lot of stress because you are reminding them how uncommon they are. You are making them see their rhythm.
I didn't turn back on the monk and say, why don't you go back to your father's country. You were not born near this. Your father was not born here. Why don't you go back to your father's journey.
My father is in Jamthang. My father is in Jamthang and he sent me here to fix this place. I was not perfectly calm. I was fairly self-righteous at the time.
I felt like this was uncalled for. Some people didn't think I was there. Obviously. That was it.
I decided that that monastery wasn't suitable place. Of course, the other thing is that the Buddha said clearly don't associate with fools. As much as you can seclude yourself, do your work and go home.
We talked about this on the forum. Someone asked a fairly similar question. I think we worked quite well with the various answers. If you do your work, even if other people feel that you are not working well with them or something people feel like you are no longer communicating properly.
What you'll generally find is that the people who in charge respect you because you are no longer trying to cheat them and you are no longer lazy and you are a hard worker and you stick to the task and you are focused and concentrated.
It's not really something to worry about rather than trying to have harmonious relationships with other people. Even better than having a harmonious relationship, be a rock for these people.
There's the disharmony. You be the pillar of strength. Make yourself the good friend for them.
