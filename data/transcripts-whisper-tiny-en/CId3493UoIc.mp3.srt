1
00:00:00,000 --> 00:00:09,000
Is there any point in living a life other than a monk's life meditating primarily and trying to achieve Nirvana or enlightened men for trials?

2
00:00:13,000 --> 00:00:16,000
I mean, there's a lot of monks who live pointless lives as well.

3
00:00:19,000 --> 00:00:29,000
The point is not to be not to be a monk, but the parenthesis part meditating primarily and trying to achieve Nirvana enlightenment for true peace and happiness

4
00:00:29,000 --> 00:00:35,000
Well, my teacher said it just comes down to whether you want more suffering or less suffering.

5
00:00:35,000 --> 00:00:43,000
We're all, that's what you're saying is correct, but some people still want more suffering and some people want less suffering so they go quicker or slower towards that.

6
00:00:53,000 --> 00:00:56,000
Is there any point besides meditating?

7
00:00:56,000 --> 00:01:03,000
No, not really. I mean, no, because this is the only point that we see in Buddhism is nibana.

8
00:01:04,000 --> 00:01:05,000
Nirvana.

9
00:01:05,000 --> 00:01:11,000
So if it's not something, the other thing is, it's not just about meditating, right?

10
00:01:11,000 --> 00:01:23,000
There are many other ways you can cultivate awesome states, you can be charitable, you can be moral, you can study the Dhamma, listen to the Dhamma.

11
00:01:23,000 --> 00:01:33,000
These are all things that help to create wholesome states of mind and will have to bring your mind together and lead you closer to the goal.

