1
00:00:00,000 --> 00:00:08,800
Okay, then let's get started. Welcome everyone to a fine Sunday afternoon in

2
00:00:08,800 --> 00:00:19,840
second life. Everybody get yourself comfortable both here in virtual world and at

3
00:00:19,840 --> 00:00:30,360
home. I'd say that it's equally important that you're focused at home. It's more

4
00:00:30,360 --> 00:00:34,800
important that you're focused at home than that you're focused here. If your

5
00:00:34,800 --> 00:00:39,320
avatar here is shifting and shuffling that's not really a problem, but at

6
00:00:39,320 --> 00:00:43,840
home you should try to. If you can, while you're watching this, try to settle

7
00:00:43,840 --> 00:00:58,780
yourself down, turn Facebook off. Set yourself to busy if it helps. And I'm just

8
00:00:58,780 --> 00:01:03,720
going to talk for a little while and then if anyone has any questions, I'm happy

9
00:01:03,720 --> 00:01:21,240
to answer them. So the topic I'd like to talk about today has to do with the

10
00:01:21,240 --> 00:01:28,400
very core of the Buddha's teaching. No use beating around the bush and no use

11
00:01:28,400 --> 00:01:37,040
giving complicated roundabout or exotic teachings. Let's cut right to the

12
00:01:37,040 --> 00:01:44,000
chase. People when we talk about the core of Buddhism, we always think of the

13
00:01:44,000 --> 00:01:49,760
four noble truths. I think because it's a good summary of the Buddha's

14
00:01:49,760 --> 00:01:54,920
teaching. But there's a teaching that goes even more to the core than that and

15
00:01:54,920 --> 00:02:00,360
that's the Buddha's teaching on dependent origination. And that's what I'd

16
00:02:00,360 --> 00:02:08,080
like to talk about today. I know I've talked about it elsewhere. Can't

17
00:02:08,080 --> 00:02:12,560
remember if I've talked about it here on second life, but it's always good to

18
00:02:12,560 --> 00:02:22,360
go over things again. So we take the Buddha's teaching in its entirety as a

19
00:02:22,360 --> 00:02:27,520
instruction in the practice of meditation, in observing reality and coming

20
00:02:27,520 --> 00:02:32,440
to understand it. So when you listen to me give a talk here, it's important

21
00:02:32,440 --> 00:02:41,760
that you're mimicking or following after the example of the people who listen to

22
00:02:41,760 --> 00:02:46,520
the Buddha himself teach. And that is when they listened to the Buddha's

23
00:02:46,520 --> 00:02:53,040
teaching, they would also practice meditation. They would take it as an

24
00:02:53,040 --> 00:03:01,560
opportunity to look inside themselves and to apply the teachings as a

25
00:03:01,560 --> 00:03:06,960
reminder for how to approach reality. So here we can do the same. You don't have

26
00:03:06,960 --> 00:03:13,760
to look at your computer screen. You can close your eyes. If it helps you to

27
00:03:13,760 --> 00:03:20,960
keep on track to look at the screen fine, but try to really appreciate the

28
00:03:20,960 --> 00:03:25,720
teachings on a more practical level. Because in the time of the Buddha, many

29
00:03:25,720 --> 00:03:30,480
people, the example that they said is that they could gain states of

30
00:03:30,480 --> 00:03:34,040
realization during the time that they were listening to the Buddha's

31
00:03:34,040 --> 00:03:41,760
teaching. Simply by applying it and using it to calm the mind, to restrain the

32
00:03:41,760 --> 00:03:49,320
mind and calm the mind and to eventually understand the workings of the mind.

33
00:03:49,320 --> 00:03:58,000
They could even enter into a state of enlightenment, go into the realization of

34
00:03:58,000 --> 00:04:10,560
Nibana and so on. So take that as an example here. This teaching, especially, is

35
00:04:10,560 --> 00:04:15,200
is an incredibly useful and practical teaching, the teaching on dependent

36
00:04:15,200 --> 00:04:22,800
origination. It's the most profound, I would say the most profound statement of

37
00:04:22,800 --> 00:04:27,200
reality that exists. I can't think of anything else that I've heard more

38
00:04:27,200 --> 00:04:33,840
profound, even within the Buddha's teaching. And for me the most profound part is

39
00:04:33,840 --> 00:04:40,680
the very beginning of the teaching. And it's these three words in the poly that

40
00:04:40,680 --> 00:04:47,120
really brought light to the world. Before the uttering of these three words,

41
00:04:47,120 --> 00:04:58,080
Auitya Pacheasankara. Before the Buddha realized this, that Auitya or Ignorance is

42
00:04:58,080 --> 00:05:06,240
the cause that is a cause for the arising of formations or with ignorance as a

43
00:05:06,240 --> 00:05:19,040
cause there are rise formations. Anybody enlightened yet? Probably not that easy.

44
00:05:19,040 --> 00:05:24,600
This is a profound teaching and it's something that very difficult for us to

45
00:05:24,600 --> 00:05:30,600
understand. Very difficult to comprehend this teaching. And it often goes

46
00:05:30,600 --> 00:05:37,640
over our heads. We think of it as some sort of philosophical teaching. Ignorance

47
00:05:37,640 --> 00:05:40,920
leads to formations.

48
00:05:43,880 --> 00:05:49,480
Formations here just to explain the word formations. What we're talking about here

49
00:05:49,480 --> 00:05:56,840
is our mental formations, our ideas about things, our thoughts, what we think of

50
00:05:56,840 --> 00:06:00,440
something, our mental volition.

51
00:06:06,520 --> 00:06:13,080
When we want to hurt someone, when we want to get attained something,

52
00:06:13,080 --> 00:06:23,160
when we get angry, when we get greedy, when we get attached, addicted, when we're

53
00:06:23,160 --> 00:06:33,600
afraid or worried and so on, all of these mental states that are a reaction to

54
00:06:33,600 --> 00:06:43,360
something, these arise the Buddha said based on ignorance. And this is really a

55
00:06:43,360 --> 00:06:48,200
profound teaching that deserves the full of our attention. If we can

56
00:06:48,200 --> 00:06:55,560
understand just these three words, if we can realize this in our meditation

57
00:06:55,560 --> 00:07:06,960
practice, this is the state of enlightenment that we're looking for. And the

58
00:07:06,960 --> 00:07:14,280
problem is that for most of us, we don't think this way. When we get angry or

59
00:07:14,280 --> 00:07:19,120
when we become addicted to something, we say to ourselves or we say to other

60
00:07:19,120 --> 00:07:24,760
people, I know it's wrong. I know it's not good to get angry, but I can't help

61
00:07:24,760 --> 00:07:43,000
myself. I know it's not good to become addicted to sweet foods or so on, but I can't

62
00:07:43,000 --> 00:07:50,000
help myself. It's not that I don't know. It's that I'm unable to change it. I'm

63
00:07:50,000 --> 00:08:02,000
unable to avoid the judging, the emotion. And so the Buddha denied this. He

64
00:08:02,000 --> 00:08:05,720
denied that this is the case. He taught the exact opposite. He said, no, you

65
00:08:05,720 --> 00:08:11,680
don't know. You don't know that it's wrong. You say, yes, I know that it's

66
00:08:11,680 --> 00:08:15,040
wrong. What you really mean is that someone told you that it's wrong. You don't

67
00:08:15,040 --> 00:08:19,720
like the results that come from it, but you don't really understand that it's

68
00:08:19,720 --> 00:08:27,800
wrong. This is how we approach everything in our lives with ignorance, with a

69
00:08:27,800 --> 00:08:35,280
incredibly superficial awareness. Even imagine yourself, look at yourself,

70
00:08:35,280 --> 00:08:44,960
how you're sitting right now, listening to my talk. You're seeing things, you're

71
00:08:44,960 --> 00:08:47,400
hearing things, you're smelling, you're tasting, you're feeling, you're

72
00:08:47,400 --> 00:08:54,920
thinking. And all of this is happening very quickly. And every when you see

73
00:08:54,920 --> 00:09:01,320
something, you immediately start to judge it immediately start to assess it.

74
00:09:01,320 --> 00:09:08,400
This is beautiful. This is nice, or this is ugly, or this is terrible, horrible,

75
00:09:08,400 --> 00:09:13,440
whatever it is. When you hear something, you immediately start to judge it. Maybe

76
00:09:13,440 --> 00:09:20,680
you like the birds in the background. Maybe they're too loud, too noisy. Maybe

77
00:09:20,680 --> 00:09:26,400
that repetitive cricket noise is driving you crazy. We don't really see, and we

78
00:09:26,400 --> 00:09:33,480
don't really hear, we don't really understand the experience. And we don't

79
00:09:33,480 --> 00:09:37,680
really understand our reaction to the experience. When we see something, we

80
00:09:37,680 --> 00:09:44,400
think I see. I'm seeing this, and we think I like it, and we think it is good. We

81
00:09:44,400 --> 00:09:48,600
have all of these preconceived notions that are totally disconnected from the

82
00:09:48,600 --> 00:10:04,440
reality of the experience. They're generally bound up in our habits, our custom

83
00:10:04,440 --> 00:10:10,160
way of responding to things. We remember that certain things bring us

84
00:10:10,160 --> 00:10:14,640
pleasure, and so we respond in that manner. We think something's going to

85
00:10:14,640 --> 00:10:22,960
bring us happy, and it's a habitual response. It's the response of, I'm saying,

86
00:10:22,960 --> 00:10:31,400
an ordinary animal. So all we're trying to do in meditation is to look deeper at

87
00:10:31,400 --> 00:10:37,480
things. And when you're sitting here listening to the talk, someone walks into

88
00:10:37,480 --> 00:10:42,120
the room and starts making loud noise right away you get angry at it. Maybe

89
00:10:42,120 --> 00:10:47,240
there's a little kid making noises, yelling, pestering you in this, this or

90
00:10:47,240 --> 00:10:51,800
that, for this or that. And you're ready to get angry, get irritated, you can

91
00:10:51,800 --> 00:10:56,760
even get to the point where you want to yell at them. It's very quick, it's

92
00:10:56,760 --> 00:11:01,000
very easy to do that. And the only reason that you do it, it's not that you're a

93
00:11:01,000 --> 00:11:03,800
bad person, it's that you're ignorant. You don't really understand what

94
00:11:03,800 --> 00:11:08,120
happened. You weren't watching. You weren't clear on the experience. You

95
00:11:08,120 --> 00:11:13,360
misunderstood it. And so you followed after it. You reacted inappropriately, and

96
00:11:13,360 --> 00:11:17,240
you caught suffering for other people and for yourself. You feel guilty, you

97
00:11:17,240 --> 00:11:20,720
feel upset, you feel angry.

98
00:11:27,320 --> 00:11:31,800
So this is the most important point of the Buddha's teaching is that ignorance

99
00:11:31,800 --> 00:11:46,600
is the cause of our reactions to things, our judgments, our improper

100
00:11:46,600 --> 00:11:57,120
reactions, our improper modes of behavior, modes of responding to the

101
00:11:57,120 --> 00:12:06,040
stimulus that come to us. And so the Buddha tried to describe to us in his

102
00:12:06,040 --> 00:12:14,640
teaching, what it was that he realized, the the detailed explanation of

103
00:12:14,640 --> 00:12:17,880
what's going on, so that when we practice meditation, we can see things

104
00:12:17,880 --> 00:12:23,000
clearer. You ask, okay, so I'm ignorant, what is it that I'm ignorant of? What

105
00:12:23,000 --> 00:12:30,760
is it that I don't understand? And the truth is if you spend some time

106
00:12:30,760 --> 00:12:34,040
looking at reality, you'll see there's a lot that you don't understand. There's

107
00:12:34,040 --> 00:12:38,680
a lot that you weren't aware. You'll see that if you just took the time to

108
00:12:38,680 --> 00:12:46,880
really see what's going on when this young child is pasturing you, when this

109
00:12:46,880 --> 00:12:57,320
loud noise is bothering you, when it's too hot, when it's too cold, when you

110
00:12:57,320 --> 00:13:00,960
have pain in the body and so on. If you just took the time to look at it, you'd

111
00:13:00,960 --> 00:13:07,360
see there's so much more going on than you thought. And at the same time, the

112
00:13:07,360 --> 00:13:14,600
experience is so much less than you thought. There's nothing unpleasant about

113
00:13:14,600 --> 00:13:20,240
it at all, that we've got a totally wrong understanding of the experience. That

114
00:13:20,240 --> 00:13:25,760
surprisingly, there's nothing unpleasant about it at all. You can be an

115
00:13:25,760 --> 00:13:30,200
incredible pain. And when you really understand the pain, when you really see

116
00:13:30,200 --> 00:13:35,280
what's going on, it doesn't bother you at all. You become surprised that you

117
00:13:35,280 --> 00:13:45,520
were ever upset by it in the first place. You say to yourself, you can't

118
00:13:45,520 --> 00:13:52,000
believe that you were addicted to this. It's a epiphany of sorts. You suddenly

119
00:13:52,000 --> 00:13:57,240
realize that there's nothing wrong with reality. There's nothing wrong with

120
00:13:57,240 --> 00:14:02,680
the way things are. It is the way it is. What's wrong is the way we respond,

121
00:14:02,680 --> 00:14:16,440
the way we react to it. So the Buddha taught us to go into more detail and he

122
00:14:16,440 --> 00:14:23,840
explained what's really going on. And this is in the rest of the

123
00:14:23,840 --> 00:14:31,400
exposition on the dependent origination. So to go through it and brief what's

124
00:14:31,400 --> 00:14:44,480
really going on is that in the world, in the universe, in the ultimate

125
00:14:44,480 --> 00:14:57,120
reality, there are two things. There are two aspects of experience. And there's

126
00:14:57,120 --> 00:15:06,720
two sides of the same coin. They're distinct, but they're a pair. And these are

127
00:15:06,720 --> 00:15:16,360
the physical and the mental. In the universe, all of our experience can be

128
00:15:16,360 --> 00:15:23,040
summed up under the physical and the mental. When we see something, this is the

129
00:15:23,040 --> 00:15:27,120
light touching the eye, the eye is physical, the light is physical. When we hear

130
00:15:27,120 --> 00:15:30,840
something, this is the sound touching the ear. And these are both physical

131
00:15:30,840 --> 00:15:38,440
smells and the nose, tastes and the tongue, feelings in the body. These are all

132
00:15:38,440 --> 00:15:45,320
physical. And the mental side is the knowing of the object, the perception of

133
00:15:45,320 --> 00:15:50,120
it. When our mind is at the eye, then we see when our mind is at the ear, then

134
00:15:50,120 --> 00:15:54,240
we hear. But sometimes the ear might be there, and the sound might be there, but

135
00:15:54,240 --> 00:15:57,440
our mind is somewhere else. And so we fail to hear the things that people say to

136
00:15:57,440 --> 00:16:02,520
us. It's difficult to see if you're not really focusing, but sometimes when

137
00:16:02,520 --> 00:16:06,240
you're using the computer, you can find that. You're focusing so much on

138
00:16:06,240 --> 00:16:09,000
something that you don't hear someone talking to. You don't know what it was

139
00:16:09,000 --> 00:16:09,520
that they said.

140
00:16:09,520 --> 00:16:24,640
And these things in and of themselves are not a problem obviously. The

141
00:16:24,640 --> 00:16:32,560
mind knows the object, the object arises, the mind knows it. But what happens next

142
00:16:32,560 --> 00:16:40,600
is there arises a feeling. The body and the mind, it comes together at the eye,

143
00:16:40,600 --> 00:16:49,000
the ear, the nose, the tongue, the body, or the mind. In the mind, there's only

144
00:16:49,000 --> 00:16:53,640
the thought, there's only the mind. But we have the body and the mind coming

145
00:16:53,640 --> 00:16:57,360
together, or else just the mind thinking itself. At the moment of

146
00:16:57,360 --> 00:17:06,600
experience, there arises a feeling. And you can verify this. When you see

147
00:17:06,600 --> 00:17:12,400
something, if it's a good thing, you write away, you feel happy about it. And you

148
00:17:12,400 --> 00:17:16,440
can see this if you're really focusing on it. So for instance, when we see

149
00:17:16,440 --> 00:17:23,320
something and we say to ourselves seeing, seeing, seeing, we can catch when we

150
00:17:23,320 --> 00:17:29,360
feel happy about it. Or when we feel unhappy about it. When we hear something

151
00:17:29,360 --> 00:17:35,880
hearing, hearing, we can catch the feeling that there's a feeling first. There's a

152
00:17:35,880 --> 00:17:44,280
pleasant feeling or an unpleasant feeling or a neutral feeling. And these

153
00:17:44,280 --> 00:17:51,000
feelings in and of themselves aren't a problem either. There's no, there's nothing

154
00:17:51,000 --> 00:18:02,960
inherently unwholesome about a happy feeling or an unhappy feeling. A pleasant

155
00:18:02,960 --> 00:18:08,160
or unpleasant feeling, it's a physical response to a stimulus. And since we feel

156
00:18:08,160 --> 00:18:13,720
pain in the body, there's nothing wrong with that. There's nothing unpleasant

157
00:18:13,720 --> 00:18:19,640
about it. And nothing unwholesome about it. And by the same token, there's

158
00:18:19,640 --> 00:18:25,200
nothing unwholesome about a pleasant feeling. So many people when they hear

159
00:18:25,200 --> 00:18:33,040
that they're instructed to acknowledge the happy feelings, they get the wrong

160
00:18:33,040 --> 00:18:36,760
impression that we're trying to do away with happiness. That's wrong to feel

161
00:18:36,760 --> 00:18:41,600
happiness. And this isn't at all the case, but we want to understand the

162
00:18:41,600 --> 00:18:47,880
happiness. We want to see it for what it is. Because it's the feelings when

163
00:18:47,880 --> 00:18:54,320
unacknowledged, when misunderstood, if there's ignorance about the feeling, that

164
00:18:54,320 --> 00:18:57,720
this is what's going to give rise to craving. This is what gives rise to our

165
00:18:57,720 --> 00:19:01,400
likes and our disciplines.

166
00:19:09,320 --> 00:19:13,840
This teaching, if you have a number of practice meditation, it might seem

167
00:19:13,840 --> 00:19:20,000
quite foreign. It might seem quite even uninteresting. It's very difficult to

168
00:19:20,000 --> 00:19:23,880
understand. But this is an incredibly useful teaching when you're practicing

169
00:19:23,880 --> 00:19:30,080
meditation. Often meditators will be at a loss as to how to deal with strong

170
00:19:30,080 --> 00:19:35,160
emotions that come up. When they really are attached to something, or when

171
00:19:35,160 --> 00:19:40,120
they're really angry about something, when they're really distracted and

172
00:19:40,120 --> 00:19:51,320
unfocused, when they're worried or stressed, depressed, bored, afraid, whatever.

173
00:19:51,320 --> 00:19:56,080
And they don't know how to deal with it. And what the Buddha is doing here is

174
00:19:56,080 --> 00:19:59,760
breaking that experience up. What happens when you're angry? What happens when

175
00:19:59,760 --> 00:20:07,240
you're attached to something? And when you break it up, you can see that there's

176
00:20:07,240 --> 00:20:11,200
nothing really worth attaching to at all. When you feel happy, it's just a happy

177
00:20:11,200 --> 00:20:16,480
feeling. There's nothing positive or negative about it. It is what it is. You

178
00:20:16,480 --> 00:20:19,960
can see that when you cling to it, when you say, this is good, you're not going to

179
00:20:19,960 --> 00:20:26,440
prolong it. You're just going to create a need for it, an attachment to it. It's

180
00:20:26,440 --> 00:20:29,800
not like you can say, oh, I like this. Therefore, it's going to stay longer. It's

181
00:20:29,800 --> 00:20:37,640
going to stay longer than if I didn't like it. Because it's exactly the case

182
00:20:37,640 --> 00:20:42,640
with negative emotions that you can't make them go away just because you don't

183
00:20:42,640 --> 00:20:46,440
want them to be there. Negative experience doesn't disappear just because you

184
00:20:46,440 --> 00:20:52,040
want it to go. Positive experience doesn't stay just because you want it to

185
00:20:52,040 --> 00:20:59,600
stay. When we come to see this, we come to see the nature of these things is

186
00:20:59,600 --> 00:21:04,880
that they're impermanent. They're unsure, uncertain. They come and go according

187
00:21:04,880 --> 00:21:11,080
to their own nature, according to the causes and effects that created, or the

188
00:21:11,080 --> 00:21:12,200
causes that created them.

189
00:21:12,200 --> 00:21:36,480
And so you can pick any one of these parts. The object of your desire or the

190
00:21:36,480 --> 00:21:42,760
object of your aversion, you can pick the feeling that it gives rise to inside

191
00:21:42,760 --> 00:21:48,800
of you, or you can pick the emotion that arises. The important thing is that

192
00:21:48,800 --> 00:21:52,240
you pick it apart and see it clearly and you're focusing on something that's

193
00:21:52,240 --> 00:21:57,240
real, because just saying I'm addicted and that's that and I can't stop

194
00:21:57,240 --> 00:22:02,440
myself isn't at all useful in any way to simply say that I'm an angry

195
00:22:02,440 --> 00:22:07,920
person also isn't useful. It's not really understanding what's happening. It's

196
00:22:07,920 --> 00:22:17,280
not seeing clearly what's going on. Once you can pick it apart, if you can catch

197
00:22:17,280 --> 00:22:26,440
yourself at the emotion, at the feeling, if you feel pain or so on. Once you see

198
00:22:26,440 --> 00:22:31,720
it clearly, then there's no, you find no reason to get upset about it. You

199
00:22:31,720 --> 00:22:35,840
instead of saying this is bad, this is painful. You just say this is this. This is

200
00:22:35,840 --> 00:22:44,480
what it is. When there's pain, you know that there's pain. When there's a pleasant

201
00:22:44,480 --> 00:22:49,680
feeling, instead of getting addicted to it, suppose it's good food or or a

202
00:22:49,680 --> 00:22:57,640
beautiful sight. You're simply aware that it is what it is. It's a sight and

203
00:22:57,640 --> 00:23:05,240
it's a happy feeling that arises. And you don't see any reason to become addicted

204
00:23:05,240 --> 00:23:09,840
or attached to it. It doesn't make it last, as I said. It doesn't do you any good

205
00:23:09,840 --> 00:23:14,080
and all it does is lead to suffering when it's gone.

206
00:23:14,080 --> 00:23:31,800
Because the alternative is to live our lives as we do as ordinary people who are

207
00:23:31,800 --> 00:23:38,080
uninterested in mental development, live their lives, happy sometimes, miserable

208
00:23:38,080 --> 00:23:43,840
sometimes, even to the point where they try to kill themselves sometimes, having

209
00:23:43,840 --> 00:23:49,640
to go through incredible stress and suffering because they don't

210
00:23:49,640 --> 00:23:55,160
understand the experience of reality in front of them. It's not because they're

211
00:23:55,160 --> 00:24:00,440
situation, there's anything wrong with it, and so they don't understand what's

212
00:24:00,440 --> 00:24:07,360
happening. They don't understand the nature of their experience. And so they

213
00:24:07,360 --> 00:24:14,920
attribute it to being me and mine and they attribute the idea that it's somehow

214
00:24:14,920 --> 00:24:21,760
should be forced and controlled and changed. And so we segregate reality into

215
00:24:21,760 --> 00:24:30,080
the good and the bad, the acceptable and the unacceptable. When actually all

216
00:24:30,080 --> 00:24:37,480
there is is the physical and the mental and the feelings that arise. The problem

217
00:24:37,480 --> 00:24:43,360
that comes is when we when we react, the problem is not in the objects

218
00:24:43,360 --> 00:24:53,440
themselves. When we crave for something, when we need for something, when we

219
00:24:53,440 --> 00:24:58,880
require that things be other than what they are, or when we require that

220
00:24:58,880 --> 00:25:05,520
things stay the way they are and not change. Simply put, when we require things

221
00:25:05,520 --> 00:25:13,280
to be other than reality dictates, when reality dictates that things must

222
00:25:13,280 --> 00:25:18,840
change, and we require that it to be otherwise, this is where suffering comes

223
00:25:18,840 --> 00:25:28,200
from. We cling to it, we say it must be, we require it to be other than this.

224
00:25:28,200 --> 00:25:36,280
We're not satisfied the way things are, we have to go and seek out more. We don't

225
00:25:36,280 --> 00:25:44,320
understand and see it for what it is we think. It's unpleasant or it's bad or

226
00:25:44,320 --> 00:25:48,960
we think that this is going to make me happy if I can just attain this, get this

227
00:25:48,960 --> 00:26:06,600
or that object. And so we cling to me, we refuse to accept change, refuse to

228
00:26:06,600 --> 00:26:11,160
accept things the way they are. And this is what gives rise to suffering, this is

229
00:26:11,160 --> 00:26:21,800
what sets us on a cycle of addiction or obsession. It just may be a better

230
00:26:21,800 --> 00:26:30,080
word, needing it to be like this, needing it not to be like that. And the

231
00:26:30,080 --> 00:26:36,440
suffering that comes when it's not the way we want it to be. We don't see this

232
00:26:36,440 --> 00:26:43,280
in ordinary, everyday life, we don't see this when we're not observing, when we're

233
00:26:43,280 --> 00:26:49,640
not meditating. All we see is the suffering that comes from things not being

234
00:26:49,640 --> 00:26:58,720
the way we want. Even right now, I'm sure there's many things going on in your

235
00:26:58,720 --> 00:27:03,760
experience that are unpleasant, you know, you were trying to change them, sitting

236
00:27:03,760 --> 00:27:07,720
here, maybe it's too hot, maybe it's too cold, maybe the seat is too hard and you

237
00:27:07,720 --> 00:27:13,240
have to shift your position, maybe you don't like what I'm saying and it makes

238
00:27:13,240 --> 00:27:33,560
you upset and gives you a headache or so on. And it's our inability to see

239
00:27:33,560 --> 00:27:41,040
these things clearly, to see what's really going on that leads us to obsession

240
00:27:41,040 --> 00:27:51,720
and to suffering. Once we look at it, we see how amazing reality really is and how

241
00:27:51,720 --> 00:27:55,760
amazing mindfulness really is, simply seeing things for what they are,

242
00:27:55,760 --> 00:28:03,240
understanding things for what they are. In a moment, you can do away with any

243
00:28:03,240 --> 00:28:10,880
suffering that arises. You feel stressed, well you focus on the stress, just

244
00:28:10,880 --> 00:28:15,200
penetrate into it, what's going on here, what's happening, what does it mean to

245
00:28:15,200 --> 00:28:20,920
say, I am stressed, I'm upset, where's the eye, where's the stress, what's really

246
00:28:20,920 --> 00:28:28,760
going on? Just say to yourself, stress, stress, stress, keeping your mind with it

247
00:28:28,760 --> 00:28:34,360
and seeing it, simply for what it is. You realize there is no eye involved, there's

248
00:28:34,360 --> 00:28:40,080
only a feeling of stress that arises. When you see that there's nothing intrinsically

249
00:28:40,080 --> 00:28:47,880
wrong with this tense date, it is what it is, it's something that's arisen and

250
00:28:47,880 --> 00:29:00,120
after some time will disappear. When you want something or when you're angry

251
00:29:00,120 --> 00:29:05,560
about something, whatever the emotion is, whatever is causing you, stress and

252
00:29:05,560 --> 00:29:10,600
suffering, whatever is getting in the way of your clear understanding, your

253
00:29:10,600 --> 00:29:22,720
peace, your peaceful harmony with reality, you penetrate into it, you see it

254
00:29:22,720 --> 00:29:27,480
for what it is, you see that there's many things going on, you have happy

255
00:29:27,480 --> 00:29:33,160
feelings, you have negative unpleasant feelings, you have these states of

256
00:29:33,160 --> 00:29:42,520
greed and anger, and they come and they go, and when you can see and understand

257
00:29:42,520 --> 00:29:47,720
these things, then you can say to yourself, I know it's wrong and that's why I

258
00:29:47,720 --> 00:29:53,160
don't do it, you'll never say to yourself, again I know it's wrong and but I

259
00:29:53,160 --> 00:29:57,000
still do it, you come to realize that you really don't know what's wrong with

260
00:29:57,000 --> 00:30:02,120
it, you really don't know the true nature of the experience and why it's

261
00:30:02,120 --> 00:30:06,160
wrong to get angry, because when you really know that it's wrong to get angry or

262
00:30:06,160 --> 00:30:10,760
greedy or so on, you won't do it, when you know that it's wrong to carry out some

263
00:30:10,760 --> 00:30:17,360
behavior, when you truly have witsha or knowledge, understand the situation,

264
00:30:17,360 --> 00:30:25,960
you'll never cause suffering for yourself again, so that was the teaching that I

265
00:30:25,960 --> 00:30:32,080
thought to discuss today, hope that was useful for some people, sort of as a

266
00:30:32,080 --> 00:30:39,080
guide for where you should be going in your meditation, thanks for everyone

267
00:30:39,080 --> 00:31:05,080
for coming and if you have any questions, I'm happy to take them now.

